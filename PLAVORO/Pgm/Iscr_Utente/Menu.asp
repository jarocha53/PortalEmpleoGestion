<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%
'Response.Write ("Id. " & sIdpers & "<BR>")
%>
<!-- tabelle riferimento per modifica segmenti -->
<table width="500" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td valign="top"> 
		<TABLE width=100% bordercolor="#FFFFFF" border="1" cellspacing=1 cellpadding=0>
			<TR height=16 bordercolor="#000080">
				<form method=post name=FrmModDati action='/Pgm/Iscr_Utente/UTE_ModDati.asp'>
					<input type=hidden name="IdPers" value="<%=sIdpers%>">
					<TD align=center valign=middle class=sfondomenu><a class=tbltext0 href="javascript:document.FrmModDati.submit();" ONMOUSEOVER="window.status = ' '; return true"><u><b>Dati Anagrafici</b></u></a></TD>
				</form>
				<form method=post name=FrmModRes action='/Pgm/Iscr_Utente/UTE_ModRes.asp'>
					<input type=hidden name="IdPers" value="<%=sIdpers%>">
					<TD align=center valign=middle class=sfondomenu><a class=tbltext0 href="javascript:document.FrmModRes.submit();" ONMOUSEOVER="window.status = ' '; return true"><u><b>Residenza/Domicilio</b></u></a></TD>
				</form>
				<form method=post name=FrmModVar action='/Pgm/Iscr_Utente/UTE_ModVar.asp'>
					<input type=hidden name="IdPers" value="<%=sIdpers%>">
					<TD align=center valign=middle class=sfondomenu><a class=tbltext0 href="javascript:document.FrmModVar.submit();" ONMOUSEOVER="window.status = ' '; return true"><u><b>Dati Sociali</b></u></a></TD>
				</form>
				<form method=post name=FrmVisImmFam action='/Pgm/Iscr_Utente/UTE_VisImmFam.asp' >
					<input type=hidden name="IdPers" value="<%=sIdpers%>">
					<TD align=center valign=middle class=sfondomenu><a class=tbltext0 href="javascript:document.FrmVisImmFam.submit();" ONMOUSEOVER="window.status = ' '; return true"><u><b>Nucleo Familiare</b></u></a></TD>
				</form>
			</TR>
		</TABLE>
		<TABLE width=100% bordercolor="#FFFFFF" border="1" cellspacing=1 cellpadding=0>
			<TR height=16 bordercolor="#000080">				
				<form method=post name=FrmTitoli_Studio action='/Pgm/BilancioCompetenze/Competenze/TitoliStudio/Titoli_Studio.asp'>
					<input type=hidden name="IND_FASE" value=0>
					<input type=hidden name="IdPers" value="<%=sIdpers%>">
					<TD align=center  valign=middle class=sfondomenu><a class=tbltext0 href="javascript:document.FrmTitoli_Studio.submit();" ONMOUSEOVER="window.status = ' '; return true"><u><b>Istruzione</b></u></a></TD>
				</form>
				<form method=post name=FrmEsp_Formative action='/Pgm/BilancioCompetenze/Competenze/EspFormative/Esp_Formative.asp'>
					<input type=hidden name="IND_FASE" value=0>
					<input type=hidden name="IdPers" value="<%=sIdpers%>">
					<TD align=center  valign=middle class=sfondomenu><a class=tbltext0 href="javascript:document.FrmEsp_Formative.submit();" ONMOUSEOVER="window.status = ' '; return true"><u><b>Formazione</b></u></a></TD>
				</form>
				<form method=post name=FrmConoscLin action='/Pgm/Iscr_Utente/UTE_VisConoscenze.asp'>
					<input type=hidden name="Area_Conoscenza" value="23">
					<input type=hidden name="IdPers" value="<%=sIdpers%>">
					<TD align=center   valign=middle class=sfondomenu><a class=tbltext0 href="javascript:document.FrmConoscLin.submit();" ONMOUSEOVER="window.status = ' '; return true"><u><b>Conoscenze Linguistiche</b></u></a></TD>
				</form>	
				<form method=post name=FrmConoscInf action='/Pgm/Iscr_Utente/UTE_VisMenuConosc.asp'>
					<input type=hidden name="Area_Conoscenza" value="19">
					<input type=hidden name="IdPers" value="<%=sIdpers%>">
					<TD align=center   valign=middle class=sfondomenu><a class=tbltext0 href="javascript:document.FrmConoscInf.submit();" ONMOUSEOVER="window.status = ' '; return true"><u><b>Conoscenze Informatiche</b></u></a></TD>
				</form>	
			</TR>
		</TABLE>
		<TABLE width=100% bordercolor="#FFFFFF" border="1" cellspacing=1 cellpadding=0>
			<TR height=16 bordercolor="#000080">
				<form method=post name=FrmVisStatOcc action='/Pgm/Iscr_Utente/UTE_VisStatOcc.asp'>
					<input type=hidden name="IdPers" value="<%=sIdpers%>">
					<TD align=center valign=middle class=sfondomenu><a class=tbltext0 href="javascript:document.FrmVisStatOcc.submit();" ONMOUSEOVER="window.status = ' '; return true"><u><b>Stato Occupazionale</b></u></a></TD>
				</form>	
				<form method=post name=FrmEspro action='/Pgm/BilancioCompetenze/Competenze/Espro/Espro.asp'>
					<input type=hidden name="IND_FASE" value=0>
					<input type=hidden name="IdPers" value="<%=sIdpers%>">
					<TD align=center valign=middle class=sfondomenu><a class=tbltext0 href="javascript:document.FrmEspro.submit();" ONMOUSEOVER="window.status = ' '; return true"><u><b>Esperienze Profess.</b></u></a></TD>
				</form>				
				<form method=post name=FrmVisAutCert action='/Pgm/Iscr_Utente/UTE_VisAutCert.asp'>
					<input type=hidden name="IdPers" value="<%=sIdpers%>">
					<TD align=center valign=middle class=sfondomenu><a class=tbltext0 href="javascript:document.FrmVisAutCert.submit();" ONMOUSEOVER="window.status = ' '; return true"><u><b>Abilitazioni/Patenti</b></u></a></TD>
				</form>
				<form method=post name=FrmAlbo action='/Pgm/Iscr_Utente/UTE_AlbiOrdini.asp'>
					<input type=hidden name="IdPers" value="<%=sIdpers%>">
					<TD align=center valign=middle class=sfondomenu><a class=tbltext0 href="javascript:document.FrmAlbo.submit();" ONMOUSEOVER="window.status = ' '; return true"><u><b>Albi/Ordini</b></u></a></TD>
				</form>
				<form method=post name=FrmVisReddito  action='/Pgm/Iscr_Utente/UTE_VisReddito.asp'>
					<input type=hidden name="Prima" value="SI">
					<input type=hidden name="IdPers" value="<%=sIdpers%>">
					<TD class=sfondomenu align=center  valign=middle class=sfondomenu><a class=tbltext0 href="javascript:document.FrmVisReddito.submit();" ONMOUSEOVER="window.status = ' '; return true"><u><b>Reddito</b></u></a></TD>
				</form>
			</TR>
		</TABLE>
<%
		sSQL="SELECT STATUS_PROT,FL_INTER_LAV,FL_CONTR_LAV FROM DICHIARAZ WHERE ID_PERSONA = " & sIdpers
		set rsTipoPers = Server.CreateObject("ADODB.Recordset")
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
		set rsTipoPers = CC.Execute(sSQL)
		if not rsTipoPers.eof then
			catprot=rsTipoPers("STATUS_PROT")
			FL_INTER_LAV=rsTipoPers("FL_INTER_LAV")
			FL_CONTR_LAV=rsTipoPers("FL_CONTR_LAV")
			rsTipoPers.close
		end if
		SET rsTipoPers=nothing
		if FL_INTER_LAV = "S" then 			
%>
			<TABLE width=100% bordercolor="#FFFFFF" border="1" cellspacing=1 cellpadding=0>
				<TR height=16 bordercolor="#000080">
					<form method=post name=FrmVisProgMig action='/Pgm/Iscr_Utente/UTE_InsPrgMig.asp'>
						<input type=hidden name="IdPers" value="<%=sIdpers%>">
						<TD width="50%" class=sfondomenu align=center valign=middle><a class=tbltext0 href="javascript:document.FrmVisProgMig.submit();" ONMOUSEOVER="window.status = ' '; return true"><u><b>Progetto Migratorio</b></u></a></TD>
					<!--	<TD class=sfondomenu align=center valign=middle><a class=tbltext0><u><b>Progetto Migratorio</b></u></a></TD>-->
					</form>
				</TR>
			</TABLE>	
<%
		end if
		if FL_CONTR_LAV = "S" then 			
%>
			<TABLE width=100% bordercolor="#FFFFFF" border="1" cellspacing=1 cellpadding=0>
				<TR height=16 bordercolor="#000080">
					<form method=post name=FrmVisImPrg action='/Pgm/Iscr_Utente/UTE_VisImPrg.asp'>
						<input type=hidden name="IdPers" value="<%=sIdpers%>">
						<TD class=sfondomenu align=center valign=middle><a class=tbltext0 href="javascript:document.FrmVisImPrg.submit();" ONMOUSEOVER="window.status = ' '; return true"><u><b>Lavoratore Immigrato</b></u></a></TD>
					</form>
				</TR>
			</TABLE>	
<%
		end if
		if catprot="S" then
%>
			<TABLE width=100% bordercolor=#FFFFFF border=1 cellspacing=1 cellpadding=0>
				<TR height=16 bordercolor="#000080">
					<form method=post name=FrmVisCatProt action='/Pgm/Iscr_Utente/UTE_VisCatProt.asp'>
						<input type=hidden name="IdPers" value="<%=sIdpers%>">
						<TD class=sfondomenu align=center valign=middle><a class=tbltext0 href="javascript:document.FrmVisCatProt.submit();" ONMOUSEOVER="window.status = ' '; return true"><u><b>Categoria Protetta</b></u></a></TD>
					</form>
				</TR>
			</TABLE>	
<!--					
<%					
'					sSQL="SELECT 'S' AS PERS_HAND FROM PERS_CATPROT WHERE ID_PERSONA = " & sIdpers &_
'						" AND IND_STATUS=0 AND COD_CATPROT LIKE 'H%'"
'					set rsHandicap = CC.Execute(sSQL)
'					if not rsHandicap.eof then 
%>
						<form method=post name=FrmVisTerapie  action='/Pgm/Iscr_Utente/UTE_VisTerapie.asp'>
							<input type=hidden name="IdPers" value="<%'=sIdpers%>">
							<input type=hidden name="Prima" value="SI">
							<TD class=sfondomenu align=center  valign=middle class=sfondomenu><a class=tbltext0 href="javascript:document.FrmVisTerapie.submit();" ONMOUSEOVER="window.status = ' '; return true"><u><b>Terapie</b></u></a></TD>
						</form>
						<form method=post name=FrmVisHandicap  action='/Pgm/Iscr_Utente/UTE_VisHandicap.asp'>
							<input type=hidden name="IdPers" value="<%'=sIdpers%>">
							<TD class=sfondomenu align=center  valign=middle class=sfondomenu><a class=tbltext0 href="javascript:document.FrmVisHandicap.submit();" ONMOUSEOVER="window.status = ' '; return true"><u><b>Handicap</b></u></a></TD>
						</form>
						<form method=post name=FrmVisAssistenza  action='/Pgm/Iscr_Utente/UTE_VisAssistenza.asp'>
							<input type=hidden name="Prima" value="SI">
							<input type=hidden name="IdPers" value="<%'=sIdpers%>">
							<TD class=sfondomenu align=center  valign=middle class=sfondomenu><a class=tbltext0 href="javascript:document.FrmVisAssistenza.submit();" ONMOUSEOVER="window.status = ' '; return true"><u><b>Assistenza</b></u></a></TD>
						</form>
						<form method=post name=FrmVisReddito  action='/Pgm/Iscr_Utente/UTE_VisReddito.asp'>
							<input type=hidden name="Prima" value="SI">
							<input type=hidden name="IdPers" value="<%'=sIdpers%>">
							<TD class=sfondomenu align=center  valign=middle class=sfondomenu><a class=tbltext0 href="javascript:document.FrmVisReddito.submit();" ONMOUSEOVER="window.status = ' '; return true"><u><b>Reddito</b></u></a></TD>
						</form>
<%
'						rsHandicap.close
'					end if
'					set rsHandicap=nothing
%>
				</TR>
			</TABLE-->
<%
		end if	
'		Response.Write ("Tipo " & Session("tipopers"))
		if (Session("tipopers")="P" and clng(Session("Creator"))=clng(sIdpers)) then
'		if Session("Progetto") = "/PLAVORO" then
%>
			<TABLE width=100% bordercolor=#FFFFFF border=1 cellspacing=1 cellpadding=0>
				<TR height=16 bordercolor="#000080">
					<form method=post name=FrmDispon action='/Pgm/Iscr_Utente/UTE_VisDispon.asp'>
						<input type=hidden name="TIPO" value="DISPON">
						<input type=hidden name="IdPers" value="<%=sIdpers%>">
						<TD align=center valign=middle class=sfondomenu><a class=tbltext0 href="javascript:document.FrmDispon.submit();" ONMOUSEOVER="window.status = ' '; return true"><u><b>Disponibilitą/Vincoli</b></u></a></TD>
					</form>
					<form method=post name=FrmAutorizzazioni action='/Pgm/Iscr_Utente/UTE_Autorizzazioni.asp'>
					<input type=hidden name="IdPers" value="<%=sIdpers%>">
					<TD align=center valign=middle class=sfondomenu><a class=tbltext0 href="javascript:document.FrmAutorizzazioni.submit();" ONMOUSEOVER="window.status = ' '; return true"><u><b>Autorizzazioni</b></u></a></TD>
					</form>
					<form method=post name=FrmCand action='/Pgm/BilancioCompetenze/AnagCandidature/default.asp'>
						<input type=hidden name="IND_FASE" value=0>
						<input type=hidden name="IdPers" value="<%=sIdpers%>">
						<TD align=center valign=middle class=sfondomenu><span class=tbltext0><b><u><a class=tbltext0 href="javascript:document.FrmCand.submit();" ONMOUSEOVER="window.status = ' '; return true"><u><b>Candidature</b></u></a></TD>
					</form>
					<form method=post name=FrmVisOpLavoro action='/Pgm/Richieste/RIC_VisOpLavoro.asp' >
						<input type=hidden name="IdPers" value="<%=sIdpers%>">
						<TD class=sfondomenu align=center valign=middle align=center><a class=tbltext0 href="javascript:document.FrmVisOpLavoro.submit();" ONMOUSEOVER="window.status = ' '; return true"><u><b>Opportunitą di lavoro</b></u></a></TD>
					</form>
				</TR>	
			</TABLE>
		<%else%>
			<TABLE width=100% bordercolor=#FFFFFF border=1 cellspacing=1 cellpadding=0>
				<TR height=16 bordercolor="#000080">
					<form method=post name=FrmDispon action='/Pgm/Iscr_Utente/UTE_VisDispon.asp'>
						<input type=hidden name="TIPO" value="DISPON">
						<input type=hidden name="IdPers" value="<%=sIdpers%>">
						<TD align=center valign=middle class=sfondomenu><a class=tbltext0 href="javascript:document.FrmDispon.submit();" ONMOUSEOVER="window.status = ' '; return true"><u><b>Disponibilitą/Vincoli</b></u></a></TD>
					</form>
					<form method=post name=FrmBorsaLavoro action='/Pgm/Bacheca/BAC_RicCriteri1.asp'>
					<input type=hidden name="IdPers" value="<%=sIdpers%>">
					<TD align=center valign=middle class=sfondomenu><a class=tbltext0 href="javascript:document.FrmBorsaLavoro.submit();" ONMOUSEOVER="window.status = ' '; return true"><u><b>Borsa Lavoro</b></u></a></TD>
					</form>
					<form method=post name=FrmCand action='/Pgm/BilancioCompetenze/AnagCandidature/default.asp'>
						<input type=hidden name="IND_FASE" value=0>
						<input type=hidden name="IdPers" value="<%=sIdpers%>">
						<TD align=center valign=middle class=sfondomenu><span class=tbltext0><b><u><a class=tbltext0 href="javascript:document.FrmCand.submit();" ONMOUSEOVER="window.status = ' '; return true"><u><b>Candidature</b></u></a></TD>
					</form>
					<form method=post name=FrmVisOpLavoro action='/Pgm/Richieste/RIC_VisOpLavoro.asp' >
						<input type=hidden name="IdPers" value="<%=sIdpers%>">
						<TD class=sfondomenu align=center valign=middle align=center><a class=tbltext0 href="javascript:document.FrmVisOpLavoro.submit();" ONMOUSEOVER="window.status = ' '; return true"><u><B>Opportunitą di lavoro</b></u></a></TD>
					</form>
					<form method=post name=FrmVisContatti action='/Pgm/Richieste/RIC_VisContatti.asp' >
						<input type=hidden name="IdPers" value="<%=sIdpers%>">
						<TD class=sfondomenu align=center valign=middle align=center><a class=tbltext0 href="javascript:document.FrmVisContatti.submit();" ONMOUSEOVER="window.status = ' '; return true"><u><b>Contatti</b></u></a></TD>
					</form>
				</TR>	
			</TABLE>
			<TABLE width=100% bordercolor=#FFFFFF border=1 cellspacing=1 cellpadding=0>
				<TR height=16 bordercolor="#000080">
					<form method=post name=FrmAutorizzazioni action='/Pgm/Iscr_Utente/UTE_Autorizzazioni.asp'>
					<input type=hidden name="IdPers" value="<%=sIdpers%>">
					<TD align=center valign=middle class=sfondomenu><a class=tbltext0 href="javascript:document.FrmAutorizzazioni.submit();" ONMOUSEOVER="window.status = ' '; return true"><u><b>Autorizzazioni</b></u></a></TD>
					</form>
					<form method=post name=FrmCurr action='/Pgm/Curriculum/default.asp' >
						<input type=hidden name="IdPers" value="<%=sIdpers%>">
						<TD class=sfondomenu  align=center valign=middle align=center><a class=tbltext0 href="javascript:document.FrmCurr.submit();" ONMOUSEOVER="window.status = ' '; return true"><u><b>Curriculum</b></u></a></TD>
					</form>
					<form method=post name=FrmListUtente action='/Pgm/Fascicolo/FAS_ListUtente.asp' >
						<input type=hidden name="IdPers" value="<%=sIdpers%>">
						<TD class=sfondomenu  align=center valign=middle align=center><a class=tbltext0 href="javascript:document.FrmListUtente.submit();" ONMOUSEOVER="window.status = ' '; return true"><u><b>Fascicolo Personale</b></u></a></TD>
					</form>
					<form method=post name=FrmIscrBase2 action='/Pgm/Iscr_Utente/UTE_RilLogin.asp'>
						<input type=hidden name="txtFlag" value="<%=sRil%>">
						<input type=hidden name="IdPers" value="<%=sIdpers%>">
						<TD align=center  valign=middle class=sfondomenu><a class=tbltext0 href="javascript:document.FrmIscrBase2.submit();" ONMOUSEOVER="window.status = ' '; return true"><u><b>Rilascio Login/Password</b></u></a></TD>
					</form>	
					<form method=post name=FrmCertificazione action='/Pgm/Iscr_utente/UTE_InsCertificazione.asp' >
						<input type=hidden name="IdPers" value="<%=sIdpers%>">
						<TD class=sfondomenu  align=center valign=middle align=center><a class=tbltext0 href="javascript:document.FrmCertificazione.submit();" ONMOUSEOVER="window.status = ' '; return true"><u><b>Certificazione</b></u></a></TD>
					</form>
				</TR>
			</TABLE>
		<%
		end if
		%>
    </td>
  </tr>
</TABLE>
  <% 'if Session("Progetto") <> "/PLAVORO" then 
  'Response.Write "Sessione (" & Session("Creator") & ") - " & sIdpers
     if (Session("tipopers")<>"P" or clng(Session("Creator")) <> clng(sIdpers)) then			
       sPercorso=Request.ServerVariables("Script_Name")
       sMenu="NO"
       if sPercorso = "/Pgm/Iscr_Utente/UTE_ModDati.asp" or sPercorso = "/Pgm/Fascicolo/FAS_ListUtente.asp" then
            sMenu="SI"
       end if
       if sMenu="NO" then     
            sSQL1="SELECT NOME,COGNOME FROM PERSONA WHERE ID_PERSONA=" & sIdPers
'PL-SQL * T-SQL  
SSQL1 = TransformPLSQLToTSQL (SSQL1) 
            set rsNominativo =CC.execute(sSQL1)
            %>    
            <TABLE width=95% bordercolor=#FFFFFF border=1 cellspacing=1 cellpadding=0>					
	             <TR height="16" class="tblsfondo">
	                 <FORM>
			             <TD height="18" width="45%" align="center">
			                 <SPAN class="tbltextmenu2">
			                   <B>Informazioni relative a:&nbsp;&nbsp;</B>
			                 </SPAN>
			                 <SPAN class="textred">
			                   <B><%=rsNominativo("NOME")%>&nbsp;<%=rsNominativo("COGNOME")%></B>
			                 </SPAN>
			             </TD>
		             </FORM>	
	            </TR>
	       </TABLE>			 
			<%
          set rsNominativo = nothing
       end if
    end if
%>
