<!--#include Virtual = "/strutt_testa2.asp"-->
<!--#include Virtual = "/include/OpenConn.asp"-->

<script LANGUAGE="Javascript" src="/Include/ControlNum.inc"></script>
<script LANGUAGE="Javascript" src="/Include/ControlString.inc"></script>
<script LANGUAGE="Javascript" src="/Include/help.inc"></script>

<script LANGUAGE="Javascript">
//include del file per fare i controlli sulla numericit� dei campi
//include del file per fare i controlli sulla stringa

	function ControllaDati(frmIscri){
		//Funzione per i controlli dei campi da inserire 
		//-------- Controlli di OBBLIGATORIETA' --------
		//-------- Controlli FORMALI            --------
		//-------- Controlli di RELAZIONE       --------

		//Cognome
		document.frmIscri.txtCognome.value=TRIM(document.frmIscri.txtCognome.value)
		if ((document.frmIscri.txtCognome.value == "")||
			(document.frmIscri.txtCognome.value == " ")){
			alert("Il campo Cognome � obbligatorio!");
			document.frmIscri.txtCognome.focus() ;
			return false;
		}
		
		
		 sCognome=ValidateInputStringWithOutNumber(document.frmIscri.txtCognome.value)
		
		if  (sCognome==false){
			alert("Cognome formalmente errato.")
			document.frmIscri.txtCognome.focus() 
			return false
		}
		//Nome
		document.frmIscri.txtNome.value=TRIM(document.frmIscri.txtNome.value)
		if ((document.frmIscri.txtNome.value == "")||
			(document.frmIscri.txtNome.value == " ")){
			alert("Il campo Nome � obbligatorio!");
			document.frmIscri.txtNome.focus() ;
			return false;
		}
		
		sNome=ValidateInputStringWithOutNumber(document.frmIscri.txtNome.value)
		
		if  (sNome==false){
			alert("Nome formalmente errato.")
			document.frmIscri.txtNome.focus() 
			return false
		}
		
		//Email
		
		document.frmIscri.txtEmail.value=TRIM(document.frmIscri.txtEmail.value)
		if (document.frmIscri.txtEmail.value ==""){
			alert("Il campo Email � obbligatorio!")
			document.frmIscri.txtEmail.focus() 
			return false
		}
		
		pippo=ValidateEmail(document.frmIscri.txtEmail.value)
		
		if  (pippo==false){
			alert("Indirizzo Email formalmente errato.")
			document.frmIscri.txtEmail.focus() 
			return false
		}

		//Obbligatorieta login
	/*	document.frmIscri.txtLogin.value=TRIM(document.frmIscri.txtLogin.value)
		if (document.frmIscri.txtLogin.value == ""){
			alert("Login obbligatoria")
			document.frmIscri.txtLogin.focus() 
			return false
		}*/
		
		//Controlli su LOGIN: posso inserire solo numeri e / o lettere
		   document.frmIscri.txtLogin.value=TRIM(document.frmIscri.txtLogin.value)
		var anyString = document.frmIscri.txtLogin.value;
		  //  anyString =TRIM(anyString)
		if (document.frmIscri.txtLogin.value == ""){
			alert("Campo obbligatorio")
			document.frmIscri.txtLogin.focus() 
			return false
		}
					
		for (var i=0; i<=anyString.length-1; i++){
			if(((anyString.charAt(i) >= "A") && (anyString.charAt(i) <= "Z")) || 
				((anyString.charAt(i) >= "a") && (anyString.charAt(i) <= "z")) || 
				((anyString.charAt(i) >= "0") && (anyString.charAt(i) <= "9")))
			{
			}
			else
			{		
				document.frmIscri.txtLogin.focus();
			 	alert("Inserire nel campo Login un parametro valido!");
				return false
			}		
		}
	return true
	
}	
</script>

<!--#include Virtual = "/include/ControlDateVB.asp"-->
<!--#include Virtual = "/util/dbutil.asp"-->

<div align="center">
<center>
 <table border="0" width="520" cellspacing="0" cellpadding="0" height="81">
    <tr>
		<td width="520" background="<%=Session("Progetto")%>/images/titoli/strumenti2b.gif" height="81" valign="bottom" align="right">
			<table border="0" background width="520" height="30" cellspacing="0" cellpadding="0">
				<tr>
					<td width="100%" valign="top" align="right"><b class="tbltext1a">Modulo di Iscrizione &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></b></td>
				</tr>
			</table>
		</td>
    </tr>
</table><br>
<form action="UTE_CnfIscrB.asp" method="post" name="frmIscri" id="frmIscri">
<table border="0" cellpadding="0" cellspacing="0" width="520">
    <tr height="18">
		<td class="sfondomenu" width="35%" height="18"><span class="tbltext0"><b>&nbsp;CANDIDATO</b></span></td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
		<td valign="middle" align="right" class="tbltext1" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">(*) campi obbligatori</td>
    </tr>
    <tr>
       <td class="sfondocomm" width="52%" colspan="3">
		Per ottenere la password di accesso, inserisci i dati richiesti e premi <b>Invia</b>.<br>
		La password, assegnata dal sistema, ti verr� inviata all'indirizzo di posta elettronica che hai indicato. In seguito potrai cambiarla con una di tua scelta.
       </td> 
    </tr>   
    <tr height="5">
		<td colspan="3" class="sfondocomm">
			<a href="Javascript:Show_Help('/Pgm/help/Iscr_Utente/UTE_IscrBase')"><img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
		</td>
    </tr>
    <tr height="17">
		<td colspan="3" class="sfondocomm" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
    </tr>
 </table><br>

 <table border="0" width="520" cellpadding="1" cellspacing="1">   
    <tr>
		<td align="left" colspan="2" nowrap class="tbltext1">
			<b>&nbsp;Cognome*</b>		
		</td>
		<td align="left" colspan="2" width="80%">
			<input style="TEXT-TRANSFORM: uppercase; " size="50" class="textblacka" maxlength="50" name="txtCognome">
		</td>
    </tr>
    <tr>
		<td align="left" colspan="2" nowrap class="tbltext1">
			<b>&nbsp;Nome*</b>
		</td>
		<td align="left" colspan="2" width="80%">
			<input style="TEXT-TRANSFORM: uppercase;" size="50" class="textblacka" maxlength="50" name="txtNome">
		</td>
    </tr>
    <tr>
        <td align="left" colspan="2" class="tbltext1">
			<b>&nbsp;Email*</b>
        </td>
        <td align="left" colspan="2" width="80%">
			<input size="50" class="textblacka" maxlength="100" name="txtEmail">
        </td>
    </tr>
    <tr>
        <td align="left" colspan="2" class="tbltext1">
			<b>&nbsp;Login*</b>
        </td>
        <td align="left" colspan="2" width="80%">
            <input size="50" class="textblacka" maxlength="15" name="txtLogin" style="TEXT-TRANSFORM: uppercase;">
        </td>
    </tr>
</table>
<br>     
<table border="0" cellpadding="1" cellspacing="1" width="520" align="center">
	<tr>
        <td align="middle" colspan="2">
			<input type="image" name="Invia" src="<%=Session("Progetto")%>/images/conferma.gif" border="0" value="Conferma" onclick="return ControllaDati(this)">
        </td>
    </tr>
	<tr>
		<td>&nbsp;</td>
    </tr>
	<tr>
		<td height="2" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
    </tr>
</table>

</form>
</div>

<!--#include Virtual = "/include/closeconn.asp"-->
<!--#include Virtual="/strutt_coda2.asp"-->
