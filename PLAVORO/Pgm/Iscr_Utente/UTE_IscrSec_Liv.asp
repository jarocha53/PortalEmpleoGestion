<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include Virtual = "/strutt_testa2.asp"-->
<!--#include Virtual = "/include/OpenConn.asp"-->
<!--#include Virtual = "/include/DecCod.asp"-->


<script LANGUAGE="Javascript">

//include del file per fare i controlli sulla validit� delle date
<!--#include Virtual = "/Include/ControlDate.inc"-->

//include del file per fare i controlli sulla numericit� dei campi
<!--#include Virtual = "/Include/ControlNum.inc"-->

//include del file per fare i controlli sulla validit� delle date
<!--#include Virtual = "/Include/ControlCodFisc.inc"-->

//include del file per fare i controlli sulla stringa
<!--#include Virtual = "/Include/ControlString.inc"-->
<!--#include Virtual = "/Include/help.inc"-->

//Funzione per i controlli dei campi da inserire 
	function ControllaDati()
{
		//Cognome
		document.frmDomIscri.txtCognome.value=TRIM(document.frmDomIscri.txtCognome.value)
		if ((document.frmDomIscri.txtCognome.value == "")||
			(document.frmDomIscri.txtCognome.value == " "))
		{
			alert("Il campo Cognome � obbligatorio!")
			document.frmDomIscri.txtCognome.focus() 
			return false
		}
		
		sCognome=ValidateInputStringWithOutNumber(document.frmDomIscri.txtCognome.value)
		
		if  (sCognome==false){
			alert("Cognome formalmente errato.")
			document.frmDomIscri.txtCognome.focus() 
			return false
		}
		//Nome
		document.frmDomIscri.txtNome.value=TRIM(document.frmDomIscri.txtNome.value)
		if ((document.frmDomIscri.txtNome.value == "")||
			(document.frmDomIscri.txtNome.value == ""))
		{
			alert("Il campo Nome � obbligatorio!")
			document.frmDomIscri.txtNome.focus() 
			return false
		}
		
		sNome=ValidateInputStringWithOutNumber(document.frmDomIscri.txtNome.value)
		
		if  (sNome==false){
			alert("Nome formalmente errato.")
			document.frmDomIscri.txtNome.focus() 
			return false
		}
		//Data di Nascita

		if (document.frmDomIscri.txtDataNascita.value == "")
		{
			alert("Il campo Data di Nascita � obbligatorio!")
			document.frmDomIscri.txtDataNascita.focus() 
			return false
		}

		//Controllo della validit� della Data di Nascita
		DataNascita = document.frmDomIscri.txtDataNascita.value
		if (!ValidateInputDate(DataNascita))
		{
			frmDomIscri.txtDataNascita.focus() 
			return false
		}
		
		//Controllo della validit� della Data di Nascita
		DataNascita = frmDomIscri.txtDataNascita.value
		if (ValidateRangeDate(DataNascita,frmDomIscri.txtoggi.value)==false){
			alert("La data di nascita deve essere precedente alla data odierna!")
			frmDomIscri.txtDataNascita.focus()
			return false
		}		

		//Comune di Nascita e Provincia di Nascita obbligatori
		//e in alternativa con Nazione di Nascita
		document.frmDomIscri.txtComuneNascita.value=TRIM(document.frmDomIscri.txtComuneNascita.value)
		if ((document.frmDomIscri.txtComuneNascita.value == "")||
			(document.frmDomIscri.txtComuneNascita.value == " ")){ 
			if (document.frmDomIscri.cmbProvinciaNascita.value == ""){
				if (document.frmDomIscri.cmbNazioneNascita.value == ""){
					alert("i campi Comune e Provincia di Nascita o Nazione di Nascita sono obbligatori!")
					document.frmDomIscri.txtComuneNascita.focus() 
					return false
				}
			}	
		}
		if (document.frmDomIscri.txtComuneNascita.value != ""){ 
			if (document.frmDomIscri.cmbProvinciaNascita.value != ""){
				if(document.frmDomIscri.cmbNazioneNascita.value != ""){
					alert("Indicare solo il Comune e la Provincia di Nascita o solo la Nazione di Nascita.")
					document.frmDomIscri.txtComuneNascita.focus() 
					return false
				}
			}	
		}
		//Se Comune di Nascita � digitato, la Provincia � obbligatoria
		if (document.frmDomIscri.txtComuneNascita.value != ""){
			if (document.frmDomIscri.cmbProvinciaNascita.value == ""){
				alert("Il campo Provincia di Nascita � obbligatorio!")
				document.frmDomIscri.cmbProvinciaNascita.focus() 
				return false
			}
		}
			//Se la Provincia di Nascita � digitata, il Comune � obbligatorio
		if ((document.frmDomIscri.txtComuneNascita.value == "")||
			(document.frmDomIscri.txtComuneNascita.value == " ")){
			if (document.frmDomIscri.cmbProvinciaNascita.value != ""){
				alert("Il campo Comune di Nascita � obbligatorio!")
				document.frmDomIscri.txtComuneNascita.focus() 
				return false
			}
		}
		
		//Sesso
	   Sesso=document.frmDomIscri.cmbSesso.value
		if (Sesso == "")
		{
			alert("Il campo Sesso � obbligatorio!")
			document.frmDomIscri.cmbSesso.focus() 
			return false
		}	

		document.frmDomIscri.txtCodFisc.value=TRIM(document.frmDomIscri.txtCodFisc.value)
		if (document.frmDomIscri.txtCodFisc.value == "")
		{
			alert("Il campo Codice Fiscale � obbligatorio!")
			document.frmDomIscri.txtCodFisc.focus() 
			return false
		}
		
		//Codice Fiscale deve essere di 16 caratteri
		if (document.frmDomIscri.txtCodFisc.value.length != 16)
		{
			alert("Formato del Codice Fiscale errato!")
			document.frmDomIscri.txtCodFisc.focus() 
			return false
		}

	    //Contollo la validit� del Codice Fiscale
		Sesso = frmDomIscri.cmbSesso.value
		//alert(Sesso)
		DataNascita = frmDomIscri.txtDataNascita.value
		CodFisc = frmDomIscri.txtCodFisc.value.toUpperCase()
		
		if (!ControllCodFisc(DataNascita,CodFisc,Sesso))
		{
			alert("Codice Fiscale Errato!")
			document.frmDomIscri.txtCodFisc.focus()
			return false
		}
		return true
}
</script>

<!--#include Virtual = "/include/ControlDateVB.asp"-->
<!--#include Virtual = "/util/dbutil.asp"-->

<table border="0" width="500" cellspacing="0" cellpadding="0" height="81">
  <tr>
    <td width="520" background="<%=Session("Progetto")%>/images/titoli/strumenti2b.gif" height="81" valign="bottom" align="right">
      <table border="0" background width="260" height="30" cellspacing="0" cellpadding="0">
        <tr>
          <td width="100%" valign="top" align="right"><b class="tbltext1a">Modulo di Iscrizione&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></td>
        </tr>
      </table>
    </td>
  </tr>
</table>

<form action="UTE_CnfIscr.asp" method="post" name="frmDomIscri" id="frmDomIscri">
<br>

<table border="0" cellpadding="0" cellspacing="0" width="500">
    <tr height="18">
		<td class="sfondomenu" width="67%" height="18">
			<span class="tbltext0"><b>&nbsp;CANDIDATO</b></span></td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
		<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">(*) campi obbligatori</td>
    </tr>
    <tr>
       <td class="sfondocomm" width="100%" colspan="3"><br>
			Per accedere ai servizi del Portale � necessario essere iscritti.<br> Compila la Scheda Anagrafica con i tuoi dati personali e premi <b>Invia</b>.			<a href="Javascript:Show_Help('/Pgm/help/Iscr_Utente/UTE_IscrSec_Liv')">
			<img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>

       </td> 
    </tr>   
    <tr height="17">
		<td colspan="3" class="sfondocomm" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">
			</td>
		</td>
    </tr>
</table>
<br>

<table border="0" cellpadding="2" cellspacing="2" width="500">
   <tr>
		<td align="left" colspan="2" nowrap class="tbltext1">
			<span class="tbltext1">
				<strong>Cognome*</strong></span>
		</td>
		<td align="left" colspan="2">
				<input class="textblacka" style="TEXT-TRANSFORM: uppercase; WIDTH: 220px; HEIGHT: 22px" size="35" maxlength="50" name="txtCognome" value="<%=session("Cognome")%>">
				<input type="hidden" name="txtoggi" value="<%=ConvDateToString(Date())%>">
		</td>
    </tr>
    <tr>
		<td align="left" colspan="2" nowrap class="tbltext1">
			<span class="tbltext1">
				<strong>Nome*</strong></span>
			</td><td align="left" colspan="2" width="30%">
				<input class="textblacka" style="TEXT-TRANSFORM: uppercase; WIDTH: 220px; HEIGHT: 22px" size="35" maxlength="50" name="txtNome" value="<%=session("Nome")%>">
			</td>
    </tr>
    <tr>
		<td align="left" colspan="2" nowrap class="tbltext1">	
				<span class="tbltext1">					
				<strong>Data di Nascita*</strong>(gg/mm/aaaa)								
		</td>	
		<td align="left" colspan="2" width="60%">
				<input class="textblacka" style="TEXT-TRANSFORM: uppercase; WIDTH: 80px; HEIGHT: 22px" size="35" maxlength="10" name="txtDataNascita">			
		</td>
    </tr>
    <tr>
		<td height="2" align="left" colspan="4" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
    </tr>
    <tr>
		<td height="20" align="left" colspan="4" class="textblack">
		Inserisci qui di seguito il comune e la provincia di nascita, se sei nato in Italia
		</font>
		</td>
    </tr>
     <tr>
		<td height="2" align="left" colspan="4" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
	</tr>
   <tr>
		<td align="left" colspan="2" nowrap class="tbltext1">
				<span class="tbltext1">					
				
				<strong>Comune di Nascita</strong></span>
		</td>	
  		<td align="left" colspan="2" width="60%">
				<input class="textblacka" style="TEXT-TRANSFORM: uppercase; WIDTH: 220px; HEIGHT: 22px" size="35" maxlength="35" name="txtComuneNascita">
		</td>
    </tr>
    <tr>
        <td align="left" colspan="2" class="tbltext1">
			<span class="tbltext1">
				<strong>Provincia di Nascita</strong>
		 </span></td>
        <td align="left" colspan="2" width="60%">
				<%
				sInt = "PROV|0|" & date & "| |cmbProvinciaNascita|ORDER BY DESCRIZIONE"			
				CreateCombo(sInt)
				%>
         </td>
    </tr>
    <tr>
    <td height="2" align="left" colspan="4" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
    </tr>
    <tr>
		<td height="20" align="left" colspan="4" class="textblack">
		Se invece sei nato in un Paese straniero selezionane la denominazione  
		</font>
		</td>
    </tr>
     <tr>
		<td height="2" align="left" colspan="4" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
	</tr>

    <tr>
        <td align="left" colspan="2" class="tbltext1">
				<span class="tbltext1">
				<strong>Nazione di Nascita</strong></span>
        </td>
        <td align="left" colspan="2" width="60%">
        <%
					set rsComune = Server.CreateObject("ADODB.Recordset")

					sSQL = "SELECT CODCOM,DESCOM from COMUNE WHERE CODCOM like 'Z%' " &_
						"ORDER BY DESCOM"

'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
					rsComune.Open sSQL, CC
			
					Response.Write "<SELECT class=textblacka ID=cmbNazioneNascita name=cmbNazioneNascita><OPTION value=></OPTION>"

					do while not rsComune.EOF 
						Response.Write "<OPTION "

						Response.write "value ='" & rsComune("CODCOM") & _
							"'> " & rsComune("DESCOM")  & "</OPTION>"
							rsComune.MoveNext 
					loop
	
					Response.Write "</SELECT>"

					rsComune.Close	
				%>
        </td>
	</tr>
	<tr>
		<td align="middle" colspan="2" nowrap class="tbltext1">
			<p align="left">		
				<strong>Sesso*</strong>
				<strong>&nbsp;</strong>
			</p>
		</td>
		<td align="left" colspan="2" width="60%">
		<% select case sesso
			case "M"%>
			    <select class="textblacka" id="cmbSesso" name="cmbSesso">
					<option value="F">FEMMINILE
					<option selected value="M">MASCHILE
					</option>
				</select><%
			case "F"%>
			    <select class="textblacka" id="cmbSesso" name="cmbSesso">
					<option selected value="F">FEMMINILE
					<option value="M">MASCHILE
					</option>
				</select><%
			case else%>
			    <select class="textblacka" id="cmbSesso" name="cmbSesso">
					<option selected>
					<option value="F">FEMMINILE
					<option value="M">MASCHILE
					</option>
				</select><%
			end select%>    
		</td>
	</tr>
	<tr>
	    <td align="left" colspan="2" class="tbltext1">
	  		<span class="tbltext1">
	  		<strong>Codice Fiscale*</strong>
	  		</span>
	    </td>
	    <td align="left" colspan="2" width="60%">
	  		<input class="textblacka" style="TEXT-TRANSFORM: uppercase; WIDTH:   167px; HEIGHT: 22px" size="11" maxlength="16" name="txtCodFisc">
	    </td>
	</tr>
</table>    
<br>
<br>
<table border="0" cellpadding="1" cellspacing="1" width="500" align="center">
	 <tr>
        <td align="middle" colspan="2">
        <input type="image" name="Invia" src="<%=Session("Progetto")%>/images/conferma.gif" border="0" value="Conferma" onclick="return ControllaDati()">
        </td>
     </tr>
</table>
</form>
<!--#include virtual="/strutt_coda2.asp"-->
<!--#include Virtual = "/include/CloseConn.asp"-->
