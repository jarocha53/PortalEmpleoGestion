<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include Virtual = "/strutt_testa2.asp"-->
<!--#include Virtual = "/include/OpenConn.asp"-->
<!--#include Virtual = "/include/DecCod.asp"-->

<script LANGUAGE="Javascript">

//include del file per fare i controlli sulla validit� delle date
<!--#include virtual = "/include/SelComune.js"-->

//include del file per fare i controlli sulla validit� delle date
<!--#include Virtual = "/Include/ControlDate.inc"-->

//include del file per fare i controlli sulla numericit� dei campi
<!--#include Virtual = "/Include/ControlNum.inc"-->

//include del file per fare i controlli sulla validit� delle date
<!--#include Virtual = "/Include/ControlCodFisc.inc"-->

//include del file per fare i controlli sulla stringa
<!--#include Virtual = "/Include/ControlString.inc"-->
<!--#include Virtual = "/Include/help.inc"-->

function PulisciCom()
{
	document.frmIscri.txtComuneNascita.value = ""
	document.frmIscri.txtComNas.value = ""
}

function PulisciRes()
{
	document.frmIscri.txtComuneRes.value = ""
	document.frmIscri.txtComRes.value = ""
}

//Funzione per i controlli dei campi da inserire 
	function ControllaDati()
{
		//Cognome
		document.frmIscri.txtCognome.value=TRIM(document.frmIscri.txtCognome.value)
		if ((document.frmIscri.txtCognome.value == "")||
			(document.frmIscri.txtCognome.value == " "))
		{
			alert("Il campo Cognome � obbligatorio!")
			document.frmIscri.txtCognome.focus() 
			return false
		}
		
		sCognome=ValidateInputStringWithOutNumber(document.frmIscri.txtCognome.value)
		
		if  (sCognome==false){
			alert("Cognome formalmente errato.")
			document.frmIscri.txtCognome.focus() 
			return false
		}
		//Nome
		document.frmIscri.txtNome.value=TRIM(document.frmIscri.txtNome.value)
		if ((document.frmIscri.txtNome.value == "")||
			(document.frmIscri.txtNome.value == ""))
		{
			alert("Il campo Nome � obbligatorio!")
			document.frmIscri.txtNome.focus() 
			return false
		}
		
		sNome=ValidateInputStringWithOutNumber(document.frmIscri.txtNome.value)
		
		if  (sNome==false){
			alert("Nome formalmente errato.")
			document.frmIscri.txtNome.focus() 
			return false
		}
		//Data di Nascita

		if (document.frmIscri.txtDataNascita.value == "")
		{
			alert("Il campo Data di Nascita � obbligatorio!")
			document.frmIscri.txtDataNascita.focus() 
			return false
		}

		//Controllo della validit� della Data di Nascita
		DataNascita = document.frmIscri.txtDataNascita.value
		if (!ValidateInputDate(DataNascita))
		{
			frmIscri.txtDataNascita.focus() 
			return false
		}
		
		//Controllo della validit� della Data di Nascita
		DataNascita = frmIscri.txtDataNascita.value
		if (ValidateRangeDate(DataNascita,frmIscri.txtoggi.value)==false){
			alert("La data di nascita deve essere precedente alla data odierna!")
			frmIscri.txtDataNascita.focus()
			return false
		}		

		//Comune di Nascita e Provincia di Nascita obbligatori
		//e in alternativa con Nazione di Nascita
		document.frmIscri.txtComuneNascita.value=TRIM(document.frmIscri.txtComuneNascita.value)
		if ((document.frmIscri.txtComuneNascita.value == "")||
			(document.frmIscri.txtComuneNascita.value == " ")){ 
			if (document.frmIscri.cmbProvinciaNascita.value == ""){
				if (document.frmIscri.cmbNazioneNascita.value == ""){
					alert("i campi Comune e Provincia di Nascita o Nazione di Nascita sono obbligatori!")
					document.frmIscri.txtComuneNascita.focus() 
					return false
				}
			}	
		}
		if (document.frmIscri.txtComuneNascita.value != ""){ 
			if (document.frmIscri.cmbProvinciaNascita.value != ""){
				if(document.frmIscri.cmbNazioneNascita.value != ""){
					alert("Indicare solo il Comune e la Provincia di Nascita o solo la Nazione di Nascita.")
					document.frmIscri.txtComuneNascita.focus() 
					return false
				}
			}	
		}
		//Se Comune di Nascita � digitato, la Provincia � obbligatoria
		if (document.frmIscri.txtComuneNascita.value != ""){
			if (document.frmIscri.cmbProvinciaNascita.value == ""){
				alert("Il campo Provincia di Nascita � obbligatorio!")
				document.frmIscri.cmbProvinciaNascita.focus() 
				return false
			}
		}
			//Se la Provincia di Nascita � digitata, il Comune � obbligatorio
		if ((document.frmIscri.txtComuneNascita.value == "")||
			(document.frmIscri.txtComuneNascita.value == " ")){
			if (document.frmIscri.cmbProvinciaNascita.value != ""){
				alert("Il campo Comune di Nascita � obbligatorio!")
				document.frmIscri.txtComuneNascita.focus() 
				return false
			}
		}
		
		//Sesso
	   Sesso=document.frmIscri.cmbSesso.value
		if (Sesso == "")
		{
			alert("Il campo Sesso � obbligatorio!")
			document.frmIscri.cmbSesso.focus() 
			return false
		}	
		
		document.frmIscri.cmbCittadinanza1.value=TRIM(document.frmIscri.cmbCittadinanza1.value)
		if (document.frmIscri.cmbCittadinanza1.value == "")
		{
			alert("Il campo Cittadinanza I � obbligatorio!")
			document.frmIscri.cmbCittadinanza1.focus() 
			return false
		}
		
		if (frmIscri.cmbCittadinanza1.value == frmIscri.cmbCittadinanza2.value){
			alert("Differenziare le due cittadinanze.")
			frmIscri.cmbCittadinanza1.focus() 
			return false
		}
         
       if ((document.frmIscri.cmbProvRes.value == "")&&
           (document.frmIscri.txtComuneRes.value == "")&&
           (document.frmIscri.cmbStatoEstero.value == "")){
  	        alert("Inserire la provincia e il comune di residenza o lo stato estero")
  			    document.frmIscri.cmbProvRes.focus() 
  			    return false
		} 
		
 		if ((document.frmIscri.cmbProvRes.value != "")&&
           (document.frmIscri.cmbStatoEstero.value != "")){
  	        alert("Inserire solo la provincia di residenza o lo stato estero")
  			    document.frmIscri.cmbProvRes.focus() 
  			    return false
		} 
		
	 	if ((document.frmIscri.txtComuneRes.value != "")&&
           (document.frmIscri.cmbStatoEstero.value != "")){
  	        alert("Inserire solo il comune di residenza o lo stato estero")
  			    document.frmIscri.txtComuneRes.focus() 
  			    return false
		} 
		
		if ((document.frmIscri.cmbProvRes.value != "")&&
           (document.frmIscri.txtComuneRes.value == "")){
  	        alert("Inserire il comune di residenza")
  			    document.frmIscri.txtComuneRes.focus() 
  			    return false
		} 
		
		if ((document.frmIscri.cmbProvRes.value == "")&&
           (document.frmIscri.txtComuneRes.value != "")){
  	        alert("Inserire la provincia di residenza")
  			    document.frmIscri.cmbProvRes.focus() 
  			    return false
		} 
		
		if (document.frmIscri.cmbProvRes.value != ""){
		      document.frmIscri.txtCodFisc.value=TRIM(document.frmIscri.txtCodFisc.value)
		      if (document.frmIscri.txtCodFisc.value == "")
		         {
			      alert("Il campo Codice Fiscale � obbligatorio!")
			      document.frmIscri.txtCodFisc.focus() 
			      return false
		      }
		}
		
		if (document.frmIscri.txtCodFisc.value != ""){
		      
		      //Codice Fiscale deve essere di 16 caratteri
		      if (document.frmIscri.txtCodFisc.value.length != 16)
		         {
			      alert("Formato del Codice Fiscale errato!")
			      document.frmIscri.txtCodFisc.focus() 
			      return false
		      }

	          //Contollo la validit� del Codice Fiscale
	          var contrCom=""
		      Sesso = frmIscri.cmbSesso.value
		      DataNascita = frmIscri.txtDataNascita.value
		      CodFisc = frmIscri.txtCodFisc.value.toUpperCase()
		       if (document.frmIscri.txtComuneNascita.value != ""){
		           contrCom =document.frmIscri.txtComNas.value   
		         }
		      else{
		          contrCom = document.frmIscri.cmbNazioneNascita.value
		      }  
		      if (!ControllCodFisc(DataNascita,CodFisc,Sesso,contrCom))
		         {
			      alert("Codice Fiscale Errato!")
			      document.frmIscri.txtCodFisc.focus()
			      return false
		         }
		       
		      if (document.frmIscri.cmbTipoDoc.value != "")
		            {
			        alert("Il Tipo documento non va' inserito se � presente il codice fiscale!")
			        document.frmIscri.cmbTipoDoc.focus() 
			        return false
		        }
		        if (TRIM(document.frmIscri.txtNumDoc.value) != "")
		            {
			        alert("Il Numero non va' inserito se � presente il codice fiscale!")
			        document.frmIscri.txtNumDoc.focus() 
			        return false
		        }
		        
		        if (document.frmIscri.txtValidita.value != "")
		            {
			        alert("Valido fino al non va' inserito se � presente il codice fiscale!")
			        document.frmIscri.txtValidita.focus() 
			        return false
		        }    
		}
		if (document.frmIscri.txtCodFisc.value == ""){
		        if (document.frmIscri.cmbTipoDoc.value == "")
		            {
			        alert("Il campo Tipo documento � obbligatorio!")
			        document.frmIscri.cmbTipoDoc.focus() 
			        return false
		        }
		        if (TRIM(document.frmIscri.txtNumDoc.value) == "")
		            {
			        alert("Il campo Numero � obbligatorio!")
			        document.frmIscri.txtNumDoc.focus() 
			        return false
		        }
		        
		        if (document.frmIscri.txtValidita.value == "")
		            {
			        alert("Il campo Valido fino al � obbligatorio!")
			        document.frmIscri.txtValidita.focus() 
			        return false
		        }
		}   
		
		DataNascita = document.frmIscri.txtValidita.value
		if (document.frmIscri.txtValidita.value != ""){
		     //Controllo della validit� della Data di Nascita
		     
		     if (!ValidateInputDate(DataNascita)){
			     document.frmIscri.txtValidita.focus() 
			     return false
		     }
		}
		
		if (document.frmIscri.txtValidita.value != ""){
		     //Controllo della validit� della Data di Nascita
		     DataNascita = frmIscri.txtValidita.value
		     if (ValidateRangeDate(frmIscri.txtoggi.value,DataNascita)==false){
			    alert("La data di validita' deve essere superiore alla data odierna!")
			    frmIscri.txtValidita.focus()
			    return false
		     }
		}
		
		//Controlli su LOGIN: posso inserire solo numeri e / o lettere
		   document.frmIscri.txtLogin.value=TRIM(document.frmIscri.txtLogin.value)
		var anyString = document.frmIscri.txtLogin.value;
		  //  anyString =TRIM(anyString)
		if (document.frmIscri.txtLogin.value == ""){
			alert("Login obbligatoria")
			document.frmIscri.txtLogin.focus() 
			return false
		}
					
		for (var i=0; i<=anyString.length-1; i++){
			if(((anyString.charAt(i) >= "A") && (anyString.charAt(i) <= "Z")) || 
				((anyString.charAt(i) >= "a") && (anyString.charAt(i) <= "z")) || 
				((anyString.charAt(i) >= "0") && (anyString.charAt(i) <= "9")))
			{
			}
			else
			{		
				document.frmIscri.txtLogin.focus();
			 	alert("Inserire nel campo Login un parametro valido!");
				return false
			}		
		}
		
		if (document.frmIscri.txtPassword.value == "")
		{
			alert("Il campo Password � obbligatorio!")
			document.frmIscri.txtPassword.focus() 
			return false
		}
		
		if (document.frmIscri.txtConfPassword.value == "")
		{
			alert("Il campo Conferma Password � obbligatorio!")
			document.frmIscri.txtConfPassword.focus() 
			return false
		}
		
		if (document.frmIscri.txtPassword.value != document.frmIscri.txtConfPassword.value)
		{
			alert("Il campo Conferma Password deve essere uguale al campo Password")
			document.frmIscri.txtPassword.focus() 
			return false
		}
		
		//Email
		
		document.frmIscri.txtEmail.value=TRIM(document.frmIscri.txtEmail.value)
		if (document.frmIscri.txtEmail.value !=""){
			pippo=ValidateEmail(document.frmIscri.txtEmail.value)
		
		    if  (pippo==false){
			     alert("Indirizzo Email formalmente errato.")
			     document.frmIscri.txtEmail.focus() 
			     return false
		    }
		}    
	return true	     				
}

</script>

<!--#include Virtual = "/include/ControlDateVB.asp"-->
<!--#include Virtual = "/util/dbutil.asp"-->
<%'sCons = Request.Form("radConsenso")
  sContr = Request.Form("scontrollo")
 	
If sContr = 2 Then %> 
	<div align="center">
	  <center>
	 <table border="0" width="515" cellspacing="0" cellpadding="0" height="81">
	    <tr>
	      <td width="515" background="<%=Session("Progetto")%>/images/titoli/strumenti2b.gif" height="81" valign="bottom" align="right">
	        <table border="0" background width="220" height="30" cellspacing="0" cellpadding="0">
	          <tr>
	            <td width="100%" valign="top" align="right"><b class="tbltext1a">Modulo di Iscrizione &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></b></td>
	          </tr>
	        </table>
	      </td>
	    </tr>
	  </table><br>
	<form action="UTE_Cnfiscrizione.asp" method="post" name="frmIscri" id="frmIscri">
	<table border="0" cellpadding="0" cellspacing="0" width="515">
	    <tr height="18">
			<td class="sfondomenu" width="35%" height="18">
				<span class="tbltext0"><b>&nbsp;CANDIDATO</b></span></td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
			<td valign="middle" align="right" class="tbltext1" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">(*) campi obbligatori</td>
	    </tr>
	    <tr>
	       <td class="sfondocomm" width="57%" colspan="3">
	          Per ottenere l'iscrizione al Portale inserisci i dati richiesti e premi <b>Invia</b>.<br>
			  Potrai in seguito accedere al portale con la login e la password che hai scelto.
	          <a href="Javascript:Show_Help('/Pgm/help/Iscr_Utente/UTE_Iscrizione')"><img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
		   </td>
	    </tr>   
	   
	    <tr height="17">
			<td colspan="3" class="sfondocomm" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
	    </tr>
	 </table><br>    
	 
	<table border="0" cellpadding="2" cellspacing="2" width="515" align="center">
	   <tr>
			<td align="left" colspan="2" nowrap class="tbltext1">
				<span class="tbltext1">
					<strong>Cognome*</strong></span>
			</td>
			<td align="left" colspan="2">
					<input class="textblacka" style="TEXT-TRANSFORM: uppercase;" size="35" maxlength="50" name="txtCognome" value="<%=session("Cognome")%>">
					<input type="hidden" name="txtoggi" value="<%=ConvDateToString(Date())%>">
			</td>
	    </tr>
	    <tr>
			<td align="left" colspan="2" nowrap class="tbltext1">
				<span class="tbltext1">
					<strong>Nome*</strong></span>
				</td><td align="left" colspan="2" width="30%">
					<input class="textblacka" style="TEXT-TRANSFORM: uppercase;" size="35" maxlength="50" name="txtNome" value="<%=session("Nome")%>">
				</td>
	    </tr>
	    <tr>
			<td align="left" colspan="2" nowrap class="tbltext1">	
					<span class="tbltext1">					
					<strong>Data di Nascita*</strong>(gg/mm/aaaa)								
			</td>	
			<td align="left" colspan="2" width="60%">
					<input class="textblacka" style="TEXT-TRANSFORM: uppercase;" size="10" maxlength="10" name="txtDataNascita">			
			</td>
	    </tr> 
	    <tr>
			<td height="2" align="left" colspan="4" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
	    </tr>
	    <tr>
			<td height="20" align="left" colspan="4" class="textblack">
			Inserisci qui di seguito il comune e la provincia di nascita, se sei nato in Italia
			</font>
			</td>
	    </tr>
	   <tr>
	        <td align="left" colspan="2" class="tbltext1">
				<span class="tbltext1">
					<strong>Provincia di Nascita</strong>
			 </span></td>
	        <td align="left" colspan="2" width="60%">
				<%	sInt = "PROV|0|" & date & "| |cmbProvinciaNascita' onchange='PulisciCom()|ORDER BY DESCRIZIONE"			
					CreateCombo(sInt)  	%>
	         </td>
	    </tr> 
	  <tr>		
				<td align="left" colspan="2" nowrap class="tbltext1">
						<b>Comune di Nascita</b>
						
				</td>
				<td nowrap>
					<span class="tbltext">
					<input type="text" name="txtComuneNascita" style="TEXT-TRANSFORM: uppercase;" class="textblacka" size="35" readonly value="<%=DescrComuneResid%>">
					<input type="hidden" id="txtComNas" name="txtComNas" value="<%=COM_RES%>">
					
	<%
					NomeForm="frmIscri"
					CodiceProvincia="cmbProvinciaNascita"
					NomeComune="txtComuneNascita"
					CodiceComune="txtComNas"
					Cap="NO"
	%>
					<a href="Javascript:SelComune('<%=NomeForm%>','<%=CodiceProvincia%>','<%=NomeComune%>','<%=CodiceComune%>','<%=Cap%>')" ID="imgPunto1" name="imgPunto1" onmouseover="javascript:window.status='';return true"><img border="0" src="<%=Session("Progetto")%>/images/bullet1.gif"></a>
					</span>				
				</td>
			</tr>	
	    
	    <tr>
	    <td height="2" align="middle" colspan="4" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
	    </tr>
	    <tr>
			<td height="20" align="left" colspan="4" class="textblack">
			Se invece sei nato in un Paese straniero selezionane la denominazione  
			</font>
			</td>
	    </tr>

	    <tr>
	        <td align="left" colspan="2" class="tbltext1">
					<span class="tbltext1">
					<strong>Nazione di Nascita</strong></span>
	        </td>
	       
	        <td align="left" colspan="2" width="60%">
	        <%			set rsComune = Server.CreateObject("ADODB.Recordset")

						sSQL = "SELECT CODCOM,DESCOM from COMUNE WHERE CODCOM like 'Z%' " &_
							"ORDER BY DESCOM"

'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
						rsComune.Open sSQL, CC
				
						Response.Write "<SELECT class=textblacka ID=cmbNazioneNascita name=cmbNazioneNascita><OPTION value=></OPTION>"

						do while not rsComune.EOF 
							Response.Write "<OPTION "

							Response.write "value ='" & rsComune("CODCOM") & _
								"'> " & rsComune("DESCOM")  & "</OPTION>"
								rsComune.MoveNext 
						loop
		
						Response.Write "</SELECT>"

						rsComune.Close			%>
	        </td>
		</tr>
		 <tr>
			<td height="2" align="left" colspan="4" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
		</tr>
		<tr>
			<td align="middle" colspan="2" nowrap class="tbltext1">
				<p align="left">		
					<strong>Sesso*</strong>
					<strong>&nbsp;</strong>
				</p>
			</td>
			<td align="left" colspan="2" width="60%">
			<% select case sesso
				case "M"%>
				    <select class="textblacka" id="cmbSesso" name="cmbSesso">
						<option value="F">FEMMINILE
						<option selected value="M">MASCHILE
						</option>
					</select><%
				case "F"%>
				    <select class="textblacka" id="cmbSesso" name="cmbSesso">
						<option selected value="F">FEMMINILE
						<option value="M">MASCHILE
						</option>
					</select><%
				case else%>
				    <select class="textblacka" id="cmbSesso" name="cmbSesso">
						<option selected>
						<option value="F">FEMMINILE
						<option value="M">MASCHILE
						</option>
					</select>
			<%end select%>    
			</td>
		</tr>
		<tr>
		    <td align="left" colspan="2" class="tbltext1">
		  		<span class="tbltext1">
		  		<strong>Codice Fiscale</strong>
		  		</span>
		    </td>
		    <td align="left" colspan="2" width="60%">
		  		<input class="textblacka" style="TEXT-TRANSFORM: uppercase; " size="20" maxlength="16" name="txtCodFisc">
		    </td>
		</tr>	
	    <tr>
	        <td align="left" colspan="2" class="tbltext1">
				<span class="tbltext1">
					<strong>Cittadinanza I*</strong>
			 </span></td>
	        <td align="left" colspan="2" width="60%">
				<%	sInt = "STATO|0|" & date & "| |cmbCittadinanza1|ORDER BY DESCRIZIONE"
					CreateCombo(sInt)	%>
	         </td>
	    </tr>
	    <tr>
	        <td align="left" colspan="2" class="tbltext1">
				<span class="tbltext1">
					<strong>Cittadinanza II</strong>
			 </span></td>
	        <td align="left" colspan="2" width="60%">
				<%  sInt = "STATO|0|" & date & "| |cmbCittadinanza2|ORDER BY DESCRIZIONE"
					CreateCombo(sInt) %>
	         </td>
	    </tr>
	    <tr>
	    <td height="2" align="middle" colspan="4" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
	    </tr>
	    <tr>
			<td height="20" align="left" colspan="4" class="textblack">
			Se sei residente in Italia seleziona la provincia e il comune   
			</font>
			</td>
	    </tr>

	    <tr>
			<td height="20" align="left" colspan="4" class="tbltext1">
			<b>Residenza:</b>   
			</font>
			</td>
	    </tr>
	    
		<tr>
	        <td align="left" colspan="2" class="tbltext1">
				<span class="tbltext1">
					<strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Provincia di Residenza</strong>
			 </span></td>
	        <td align="left" colspan="2" width="60%">
					<%
					sInt = "PROV|0|" & date & "| |cmbProvRes' onchange='PulisciRes()|ORDER BY DESCRIZIONE"			
					CreateCombo(sInt)
					%>
	         </td>
	    </tr>
	     <tr>		
				<td align="left" colspan="2" nowrap class="tbltext1">
						<b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Comune di Residenza</b>
						
				</td>
				<td nowrap>
					<span class="tbltext">
					<input type="text" name="txtComuneRes" style="TEXT-TRANSFORM: uppercase;" class="textblacka" size="35" readonly value="<%=DescrComuneResid%>">
					<input type="hidden" id="txtComRes" name="txtComRes" value="<%=COM_RES%>">
					
	<%
					NomeForm="frmIscri"
					CodiceProvincia="cmbProvRes"
					NomeComune="txtComuneRes"
					CodiceComune="txtComRes"
					Cap="NO"
	%>
					<a href="Javascript:SelComune('<%=NomeForm%>','<%=CodiceProvincia%>','<%=NomeComune%>','<%=CodiceComune%>','<%=Cap%>')" ID="imgPunto1" name="imgPunto1" onmouseover="javascript:window.status='';return true"><img border="0" src="<%=Session("Progetto")%>/images/bullet1.gif"></a>
					</span>				
				</td>
			</tr>
	    <tr>
	    <td height="2" align="middle" colspan="4" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
	    </tr>
	    <tr>
			<td height="20" align="left" colspan="4" class="textblack">
			Altrimenti seleziona lo stato estero  
			</font>
			</td>
	    </tr>
	    <tr>
	        <td align="left" colspan="2" class="tbltext1">
					<span class="tbltext1">
					<strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Stato Estero</strong></span>
	        </td>
	       
	        <td align="left" colspan="2" width="60%">
	        <%
						set rsComune = Server.CreateObject("ADODB.Recordset")

						sSQL = "SELECT CODCOM,DESCOM from COMUNE WHERE CODCOM like 'Z%' " &_
							"ORDER BY DESCOM"

'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
						rsComune.Open sSQL, CC
				
						Response.Write "<SELECT class=textblacka ID=cmbStatoEstero name=cmbStatoEstero><OPTION value=></OPTION>"

						do while not rsComune.EOF 
							Response.Write "<OPTION "

							Response.write "value ='" & rsComune("CODCOM") & _
								"'> " & rsComune("DESCOM")  & "</OPTION>"
								rsComune.MoveNext 
						loop
		
						Response.Write "</SELECT>"

						rsComune.Close	
					%>
	        </td>
		</tr>
	     <tr>
			<td height="2" align="left" colspan="4" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
		</tr>
		 <tr>
			<td height="20" align="left" colspan="4" class="textblack">
			Se non possiedi il codice fiscale inserisci gli estremi di un documento di riconoscimento valido  
			</font>
			</td>
	    </tr>
		<tr>
	        <td align="left" colspan="2" class="tbltext1">
				<span class="tbltext1">
					<strong>&nbsp;Tipo Documento</strong>
			 </span></td>
	        <td align="left" colspan="2" width="60%">
					<%
					sInt = "DOCST|0|" & date & "| |cmbTipoDoc|ORDER BY DESCRIZIONE"			
					CreateCombo(sInt)
					%>
	         </td>
	    </tr>
	    <tr>
	        <td align="left" colspan="2" class="tbltext1">
					<b>&nbsp;Numero</b>
	        </td>
	        <td align="left" colspan="2" width="80%">
	             <input size="34" class="textblacka" maxlength="15" name="txtNumDoc" style="TEXT-TRANSFORM: uppercase;">
	        </td>
	    </tr>  
		<tr>
			<td align="left" colspan="2" nowrap class="tbltext1">	
					<span class="tbltext1">					
					<strong>Valido fino al</strong>(gg/mm/aaaa)								
			</td>	
			<td align="left" colspan="2" width="60%">
					<input class="textblacka" style="TEXT-TRANSFORM: uppercase;" size="10" maxlength="10" name="txtValidita">			
			</td>
	    </tr>  
	     <tr>
			<td height="2" align="left" colspan="4" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
		</tr>    
	    <tr>
	        <td align="left" colspan="2" class="tbltext1">
					<b>&nbsp;Login*</b>
	        </td>
	        <td align="left" colspan="2" width="80%">
	             <input size="34" class="textblacka" maxlength="15" name="txtLogin" style="TEXT-TRANSFORM: uppercase;">
	        </td>
	    </tr>
	    <tr>
	        <td align="left" colspan="2" class="tbltext1">
					<b>&nbsp;Password*</b>
	        </td>
	        <td align="left" colspan="2" width="80%">
	             <input type="password" size="34" class="textblacka" maxlength="15" name="txtPassword" style="TEXT-TRANSFORM: uppercase;">
	        </td>
	    </tr>
	    <tr>
	        <td align="left" colspan="2" class="tbltext1">
					<b>&nbsp;Conferma Password*</b>
	        </td>
	        <td align="left" colspan="2" width="80%">
	             <input type="password" size="34" class="textblacka" maxlength="15" name="txtConfPassword" style="TEXT-TRANSFORM: uppercase;">
	        </td>
	    </tr>  
	    <tr>
	         <td align="left" colspan="2" class="tbltext1">
					<b>&nbsp;Email
					</b>
	        </td>
	        <td align="left" colspan="2" width="80%">
					<input size="34" class="textblacka" maxlength="100" name="txtEmail">
	        </td>
	    </tr>
	   </table> 
	    
	<br><br>

	<table border="0" cellpadding="1" cellspacing="1" width="360" align="center">
		  <tr>
	          <td align="middle" colspan="2">
	             <input type="image" name="Invia" src="<%=Session("Progetto")%>/images/conferma.gif" border="0" value="Conferma" onclick="return ControllaDati(this)">
	          </td>
	      </tr>
		  <tr>
			  <td>
			     &nbsp;
			  </td>
	      </tr>
	</table>
	
	<br>
	<br>
	</form>
	</div>
<%
Else
	response.redirect "/Plavoro/Pgm/Iscr_Utente/UTE_PreIscrizione.asp"	
End If
%>
<!--#include Virtual = "/include/closeconn.asp"-->
<!--#include Virtual="/strutt_coda2.asp"-->
