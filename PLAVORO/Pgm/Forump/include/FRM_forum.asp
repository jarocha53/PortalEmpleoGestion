<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%

' // helper class for forum.

' this class is the main communication point that contains all forum-related
' functionality. you never communicate with the forum-active-x directly, 
' but always indirectly with this class.

class Forum
	' ---------------------------------------------
	' uri functions.

	' these functions should be used whenever you need a <a href="...">
	' tag. instead of specifying an asp page and adding some cryptic
	' parameters, you call one of these functions.
	' with this approach, you can centralize the url-parameters
	' in one place.
	'
	' you have to modify the asp filenames in the following functions,
	' when you change the name of the real files.

	' content page.
	public function getUriNoContent
		getUriNoContent = "nocontent.asp"
	end function

'FRM_reply.asp 
	public function getUriReplyForum
		getUriReplyForum = "FRM_reply.asp?" &toUrl
	end function

'FRM_reply.asp 
	public function getUriSendReply
		getUriSendReply = "FRM_insert_reply.asp?" &toUrl
	end function

'FRM_mail.asp 
	public function getUriReplyAuthor
		getUriReplyAuthor = "FRM_mail.asp?" &toUrl
	end function

'FRM_modify.asp  
	' list page.
	public function getUriModifyCurMsg( id )
		getUriModifyCurMsg = "FRM_modify.asp?" & _
			toUrlValue2("modify", id, "select",id)
	end function

'FRM_delete.asp
	public function getUriDeleteCurMsg( id ) 
		getUriDeleteCurMsg  = "FRM_delete.asp?" & _
			toUrlValue2("delkind","one", "del",id)
	end function

'FRM_delete.asp
	public function getUriDeleteCurMsgSubs(id)  
		getUriDeleteCurMsgSubs  = "FRM_delete.asp?" & _
			toUrlValue2("delkind","branch", "del", id)
	end function
	
'FRM_index.asp 
	public function getUriSelectMsg(id)
		getUriSelectMsg = "FRM_start.asp?" & _
			toUrlValue("select",id)
		appendAnchor getUriSelectMsg, id
	end function

'FRM_index.asp 
	public function getUriListWithForum( forum_id )
		
		getUriListWithForum = "FRM_start.asp?"& _		
			toUrlValue("forumid",forum_id)				
		'traceX toUrlValue("forumid",forum_id)
	end function

	public function getListForum( forum_id )
		getListForum = "FRM_start.asp?"& _		
			toUrlValue("forumid",forum_id)				
		'traceX toUrlValue("forumid",forum_id)
	end function

'FRM_insert_reply.asp 
	' reply page(s).
	public function getUriInsertReply
		getUriInsertReply = "FRM_reply.asp?" & toUrl
	end function

'FRM_failure.asp 
	public function	getUriReplyError( err_text )
		getUriReplyError = "FRM_failure.asp?err_txt=" & _
			Server.UrlEncode( err_text )
	end function

'FRM_index.asp
	public function getUriMain( article_id )
		sel = article_id
		getUriMain = "FRM_start.asp?" & toUrl
	appendAnchor getUriMain, article_id
	end function

'FRM_send_mail.asp 
	' mail page(s).
	public function getUriSendMail
		getUriSendMail = "FRM_send_mail.asp?" & toUrl
	end function

'FRM_failure.asp 
	public function	getUriMailError( err_text )
		getUriMailError = "FRM_failure.asp?err_txt=" & _
			Server.UrlEncode( err_text )
	end function

'FRM_insert_new.asp 
	' new page(s).
	public function getUriInsertNew
		getUriInsertNew = "FRM_insert_new.asp?" & toUrl
	end function

'FRM_failure.asp 
	public function	getUriNewError( err_text )
		getUriNewError = "FRM_failure.asp?err_txt=" & _
			Server.UrlEncode( err_text )
	end function

'FRM_Forum_insert_modify.asp 
	' modify page(s).
	public function getUriForumModify
		getUriForumModify = "FRM_Forum_insert_modify.asp?" & toUrl
	end function


'FRM_insert_modify.asp 
	' modify page(s).
	public function getUriModify
		getUriModify = "FRM_insert_modify.asp?" & toUrl
	end function

'FRM_failure.asp
	public function	getUriModifyError( err_text )
		getUriModifyError = "FRM_failure.asp?err_txt=" & _
			Server.UrlEncode( err_text )
	end function

'FRM_new.asp 
	' misc page(s).
	public function getUriNew( forum_id )
		getUriNew = "FRM_new.asp?" &_
			toUrlValue("forumid",forum_id)
	end function

'FRM_validate.asp
	public function getPubbMsg(id,list,idf) 
		getPubbMsg  = "FRM_Validate.asp?list=" & list & "&select=" & id & "&idf=" & idf			
			'toUrlValue2("delkind","branch", "del", id)
	end function


	' always call this immediately after you created
	' a new instance of this class.
	public sub init()
		
		Set m_Dic = CreateObject("Scripting.Dictionary")
		
		'db connection
		set m_conn = Server.CreateObject("ADODB.Connection")
        mp = Server.MapPath("/PLAVORO/Pgm/ForumP/database/forum.mdb")
		sgconn = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & mp & ";Mode=ReadWrite;Persist Security Info=False"
		m_conn.Open sgconn

		' read in all parameters from the current page's url.
		fromUrl( myselfComplete )
	end sub

	public sub readArticles( forum_id )
		m_Dic.Item( "forumid" ) = forum_id
	end sub

	' ---------------------------------------------
	
	' checks, whether the forum is currently in admin mode.
	' in admin mode, you can modify and delete articles.
	public function isInAdminMode()
		isInAdminMode = false

		'if Request("ad")<>"" then
		If Session("admforum") = "yes" then
			isInAdminMode = true
		end if
	end function

	' get the id of a forum from its short-name.
	' returns null if not found.
	' you may need to strengthen the sql query (the 'LIKE')
	' when you have forums with similar names.
	public function getForumId( forum_name )
		dim rs
'PL-SQL * T-SQL  
		set rs = m_Conn.Execute( "SELECT * FROM Forum WHERE Shortname LIKE '%" &forum_name& "%'" )

		if not (rs.BOF and rs.EOF) then
			getForumId = rs("ID")
		else
			getForumId = null
		end if
	end function

	' creates a forum with a long name.
	' returns the id of the forum.
	' if the forum already exists, only its it
	' is returned.
	public function createForum( forum_long_name )
		dim rs
'PL-SQL * T-SQL  
		set rs = m_Conn.Execute( "SELECT * FROM Forum WHERE Name LIKE '%" &forum_long_name& "%'" )

		if not (rs.BOF and rs.EOF) then
			createForum = rs("ID")
		else
			rs.Close

			set rs = Server.CreateObject("ADODB.Recordset")
			rs.Open "Forum", m_Conn, 2, 3

			rs.AddNew
			rs("Name")   = forum_long_name
			rs.Update
			
			createForum = rs("ID")
		end if
	end function
' ******************************************
'	EPILI 28/03/2003
	public function modifyForum( long_id )

		dim rs
'PL-SQL * T-SQL  
		set rs = m_Conn.Execute( "SELECT * FROM Forum WHERE ID = " & long_id)

		if not (rs.BOF and rs.EOF) then
			modifyForum = rs
		else
			modifyForum = 0
		end if

	end function
' ******************************************



	' returns the long name of a forum, given the id of a forum.
	public function getForumName( forum_id )
		dim rs
'PL-SQL * T-SQL  
		set rs = m_Conn.Execute( "SELECT * FROM Forum WHERE ID=" &forum_id )
		If not (rs.BOF and rs.EOF) Then
			getForumName = rs("Name")
		Else
			Response.Redirect ("FRM_start.asp")
		End if
	end function

	' gets the recordset of an article, given the id of the article.
	public function getArticleRs( article_id )
'PL-SQL * T-SQL  
		set getArticleRs = m_Conn.Execute( "SELECT * FROM Article WHERE ID=" &article_id )
	end function

	' gets the recordset of a forum, given the id of the forum.
	public function getForumRs( forum_id )
'PL-SQL * T-SQL  
		set getForumRs = m_Conn.Execute( "SELECT * FROM Forum WHERE ID=" &forum_id )
	end function

	' gets the recordset with all forums.
	public function getForumsRs()
'PL-SQL * T-SQL  
		set getForumsRs = m_Conn.Execute( "SELECT * FROM Forum" )
	end function
	
	
	' insert a child-article for an already present article.
	' set 'parent_id' to zero for inserting a root article
	' for the given forum.
	public function insertNew( parent_id, forum_id, subject, author_name, author_email, text, email_notify )

		dim rs
		set rs = Server.CreateObject("ADODB.Recordset")
		rs.Open "Article", m_Conn, 2, 3

		'------------------------------------------- 
		'assortment of the entries:
		'every entry has a position-number (position), which determines the assortment
		'of the output. The most up-to-date entry of the first level gets the lowest number.
		'The answer-entries get the first number of their parent`s position and, after a ".",
		'the next number which is sorted ascending.
		
		dim sql_pos
		dim rs_pos
		dim sql_child
		dim rs_child
		dim position
		dim last
		dim x
		dim indent

			'if it is a parent entry
			if parent_id = "0" then
				sql_pos = "SELECT * FROM Article WHERE ForumID =" &forum_id& " ORDER BY [Position]"
'PL-SQL * T-SQL  
SQL_POS = TransformPLSQLToTSQL (SQL_POS) 
				set rs_pos = m_Conn.Execute(sql_pos) 
				
				if rs_pos.Eof then
					position = "999999"
				else
					x = CLng(rs_pos("Position"))-1

					'if there are more than 999999 entries
					if x = 0 then x = 1

					while Len(x) < 6
						 x = "0" & x
					wend
					position = x
				end if
			indent = "0"

			'if it is a child entry
			else
				sql_pos = "SELECT * FROM Article WHERE ID =" & parent_id
'PL-SQL * T-SQL  
SQL_POS = TransformPLSQLToTSQL (SQL_POS) 
				set rs_pos = m_Conn.Execute(sql_pos)


				sql_child = "SELECT * FROM Article WHERE ParentID =" & parent_id & " ORDER BY Date DESC"
'PL-SQL * T-SQL  
SQL_CHILD = TransformPLSQLToTSQL (SQL_CHILD) 
				set rs_child = m_Conn.Execute(sql_child)
				indent = CLng(rs_pos("Indent"))+1

				'if there are no other childs
				if (rs_child.Bof and rs_child.Eof) then
					position = rs_pos("Position") & ".000001"

				'if there are other childs
				else
					last = Right(rs_child("Position"), 6)

					x = CStr(CLng(last)+1)
					while Len(x) < 6
						 x = "0" & x
					wend
					'if there are more than 999999 childs
					if x > 999999 then x = 999999
					position = rs_pos("Position") & "." & x
				end if
			end if
			
		'--------------------------------------------



		'--------------------------------------------
		'Database entry

		rs.AddNew
		rs("ParentID")    = parent_id
		rs("ForumID")     = forum_id
		rs("Subject")     = subject
		rs("AuthorName")  = author_name
		rs("AuthorEmail") = author_email
		rs("Date")        = Now()
		rs("Text")        = text
		rs("Notify")      = email_notify
		rs("Position")    = position
		rs("Indent")      = indent
		rs("Pubblicato")  = false
		rs.Update

		' mark the added article as selected.
		m_Dic.Item("select") = rs("ID")

		insertNew = rs("ID")
		'---------------------------------------------
	end function


	' modifes an already present article.
	public sub modifyForumData( id, shortName, name, description )
		shortName = replace (shortName,"'","''")
		name = replace (name,"'","''")
		description =  replace (description,"'","''")
	
'PL-SQL * T-SQL  
		m_Conn.Execute "UPDATE Forum SET " & _
			"Forum.ShortName='" &shortName& _
			"',Forum.Name='" &name& _
			"',Forum.Description='" &description&_
			"' WHERE Forum.ID=" & id
		dim rs
		set rs = Server.CreateObject("ADODB.Recordset")
		rs.Open "Article", m_Conn, 2, 3
	end sub



	' modifes an already present article.
	public sub modifyArticle( article_id, subject, author_name, author_email, text, email_notify )
		subject = replace (subject,"'","''")
		author_name = replace (author_name,"'","''")
		text =  replace (text,"'","''")
'PL-SQL * T-SQL  
		m_Conn.Execute "UPDATE Article SET " & _
			"Article.Subject='" &subject& _
			"',Article.AuthorName='" &author_name& _
			"',Article.AuthorEmail='" &author_email& "',Article.Text='" &text& _
			"',Article.Notify='" &email_notify& _
			"' WHERE Article.ID=" &article_id
		dim rs
		set rs = Server.CreateObject("ADODB.Recordset")
		rs.Open "Article", m_Conn, 2, 3
	end sub
	
	' pubbllica an already present article.
	public sub PubArticle( article_id )
'PL-SQL * T-SQL  
		m_Conn.Execute "UPDATE Article SET Article.Pubblicato = true WHERE Article.ID=" &article_id
			
		dim rs
		set rs = Server.CreateObject("ADODB.Recordset")
		rs.Open "Article", m_Conn, 2, 3
	end sub
	
	public sub getRispSelectMsg(article_id)	
	dim rs 
		set rs = Server.CreateObject("ADODB.Recordset")
		sql2 = "SELECT * FROM Article WHERE ParentID=" & article_id & " and pubblicato= true "
		rs.Open sql2, m_Conn, 1, 3
		Response.Write rs.recordcount
	end sub
	
	' returns the article-id of the article 
	' at the current file pointer position.
	'public function getCurID
		'getCurID = "201"
	'end function


	' deletes an article and all its childs, given the 
	' article id of this article.
	public sub deleteArticle( article_id )

	dim parent_position, forum_id
	dim rs_p, sql_p
	
	sql_p = "SELECT * FROM Article WHERE ID ="&Request("del")
'PL-SQL * T-SQL  
SQL_P = TransformPLSQLToTSQL (SQL_P) 
	set rs_p = m_Conn.Execute(sql_p)

	parent_position =	rs_p("Position")
	forum_id =			Request("forumid")

'PL-SQL * T-SQL  
		m_conn.Execute "DELETE FROM Article WHERE [Position] LIKE '%"&parent_position&"%' AND ForumID ="&forum_id

	end sub

	
	'--------------------------------------------------------------------------------
	' build the internal map of name-value pairs from a given url.
	function fromURL(url)

		dim url_element

		if InStr(url, "?") then
			
			url_element = split(url, "?")

			'if the url has no parameters
			if url_element(1) = "" then
				exit function
			'if the url has parameters
			else
				dim params
				dim item
				params = split(url_element(1), "&")
				

				dim x

				For each x in params
					if InStr(x, "=") then
						item = split(x, "=")
						
						'add key-item to object
						m_Dic.Add item(0), item(1)
					end if
				Next

			end if
		end if
	 
	end function
	



	'--------------------------------------------------------------------------------
	' returns all values of the internal map url-encoded
	' in the form "name=value&name=value&name=value&" etc.
	' you can append this directly to an url.
	
	function toUrl
		dim x,s,a,b
		a = m_Dic.Keys
		b = m_Dic.Items

		For x = 0 To m_Dic.Count -1
			s = s & a(x) & "=" & Server.URLEncode(b(x)) & "&"
		Next
		toUrl = s
		
	end function
	

	'--------------------------------------------------------------------------------
	' the same as "toUrl" except that the value of the
	' map-entry specified by "name" is replaced by "new_value".
	function toUrlValue(key,new_item)
		dim x,s,a,b

		if not m_Dic.Exists(key)then
			m_Dic.Add key, new_item
		end if

		a = m_Dic.Keys
		b = m_Dic.Items

		For x = 0 To m_Dic.Count -1
			if a(x)=key then
				s = s & a(x) & "=" & Server.URLEncode(new_item) & "&"
			else
				s = s & a(x) & "=" & Server.URLEncode(b(x)) & "&"
			end if
		Next

		toUrlValue = s
		
	end function
	

	'--------------------------------------------------------------------------------
	' the same as "toUrl" except that the value of the
	' map-entry specified by "name1" is replaced by "new_value1" and
	' the value of "name2" is replaced by "new_value2".
	function toUrlValue2( key1, new_item1, key2, new_item2 )
		
		dim x,s,a,b

		if not m_Dic.Exists(key1)then
			m_Dic.Add key1, new_item1
		end if

		if not m_Dic.Exists(key2) then
			m_Dic.Add key2, new_item2
		end if

		a = m_Dic.Keys
		b = m_Dic.Items

		For x = 0 To m_Dic.Count -1

	'		Response.Write key1 & "<br>"
	'		Response.Write new_item1 & "<br>"
	'		Response.Write key2 & "<br>"
	'		Response.Write new_item2 & "<br>"
			if a(x)=key1 then
				s = s & a(x) & "=" & Server.URLEncode(new_item1) & "&"
			elseif a(x)=key2 then
				s = s & a(x) & "=" & Server.URLEncode(new_item2) & "&"
			else
				s = s & a(x) & "=" & Server.URLEncode(b(x)) & "&"
			end if
	
		Next
		toUrlValue2 = s
	end function

	' ---------------------------------------------

	' helper function. opens the database connection.
	private function openDb()
		set openDb = Server.CreateObject("ADODB.Connection")
		openDb.Open DB, DB_ID, DB_PW
	end function

	' appends an anchor-link "#xxx" to an url.
	private sub appendAnchor( byref url, anchor_name )

		'-temp: exit until it works.
		exit sub

		' remove any '?' or '&' at the end.
		while Len(url)>0 and _
			(Right(url,1)="&" or Right(url,1)="?")
			url = delete(url, Len(url), 1)
		wend
		
		' append the anchor.
		url = url& "#" &anchor_name
	end sub

	' ---------------------------------------------

	public m_Conn		' the database-connection.
	'private m_Ax		' the connection to the active-x object.
	Private m_Dic		' scripting.dictionary
end class



' ***************************************************************************

%>
