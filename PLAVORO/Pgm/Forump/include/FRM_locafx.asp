<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include file="FRM_config.asp"-->
<!--#include file="FRM_forum.asp"-->
<!--#include file="FRM_email.asp"-->

<%	   
function fmtContent( text )
	text = fmtML( text )
	fmtContent = text
end function

' delivers url information of the current page.
private function myselfComplete
	dim protocol
	if LCase(Request.ServerVariables("HTTPS"))="on" then
		protocol = "https://"
	else
		protocol = "http://"
	end if

	myselfComplete = protocol & _
		Request.ServerVariables("HTTP_HOST") & _
		Request.ServerVariables("HTTP_URL")
end function

' delivers url information of the current page.
private function myself
	myself = Request.ServerVariables("SCRIPT_NAME")
end function

' formats a multiline (ML) string.
' replaces '\n' with '<br>'.
private function fmtML( in_str )
	if IsNull(in_str) then
		fmtML = ""
		exit function
	end if 

	dim str
	if in_str<>"" then
		str = Replace( in_str, vbCr+vbLf, vbCr )
		str = Replace( str   , vbLf     , vbCr )
		str = Replace( str   , vbCr     , "<br>" )

		fmtML = str
	else
		fmtML = in_str
	end if
end function

' as CString::Delete. returns the new string.
function Delete( str, index, count )
	dim l,r
	l = Left ( str, index-1 )
	r = Right( str, Len(str)-(index+count)+1 )

	Delete = l & r
end function
%>

<%
sub trace( str )
	Response.Write str
end sub

sub traceX( str )
	trace str
	Response.End
end sub
%>

<%
' ***************************************************************************
' execuTABLE code.

' creates an instance of the forum class on every page.
dim fh
set fh = new Forum
fh.init
%>
