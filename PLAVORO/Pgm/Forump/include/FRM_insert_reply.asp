<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%' insert_reply.asp

' check.

dim parent_id
dim subject
dim author_name
dim author_email
dim text
dim notify

parent_id    = Request("ParentID")
subject      = Request("Subject")
author_name  = Request("AuthorName")
forumid      = Request("forumid")

if isEMail(Request("AuthorEmail")) then
	notify       = Request("email_notify")
	author_email = Request("AuthorEmail")
else
	notify       = ""
	author_email = "nessun indirizzo inserito"
end if

text         = Request("Text")

if subject="" or author_name="" or text="" then
	Response.Redirect fh.getUriReplyError("Not all fields were filled in")
end if


' ***********************************************************************
' insert.

on error resume next

dim id
id = fh.insertNew( _
	parent_id, _
	Request("forumid"), _
	subject, _
	author_name, _
	author_email, _
	text, _
	notify )

if err<>0 then
	Response.Redirect fh.getUriReplyError(err.Description)
end if

' ***********************************************************************
' notify parent if wanted.

dim rs
set rs = fh.getArticleRs( parent_id )
if rs("Notify")<>"" then


if isEMail(Request("AuthorEmail")) then
	author_email = Request("AuthorEmail")
else
	author_email = rs("AuthorEMail")
end if

	' build body.
	dim body
	body = ""
	body = body & "<html><body bgcolor='#DCE0E7'><font size=3 color='#191970'>"
	body = body & "<font size=4 face='tahoma,verdana' color='#191970'>FORUM "
	body = body & "<font size=4 face='tahoma,verdana' color='#ff7f50'>'" &fh.getForumName(rs("ForumID"))& "'<br><br>" &vbCrLf	
	body = body & "<font size=2 face='tahoma,verdana' color='#191970'>Una risposta � stata aggiunta da <b>" & author_name & "</b> al tuo messaggio dal titolo: "
	body = body & "</font><font size=2 face='tahoma,verdana' color='#ff7f50'><b>'" & subject & "'</b><br>"
	body = body & "<font size=2 face='tahoma,verdana' color='#191970'><bR>Testo del messaggio:" &vbCrLf
	body = body & "<br>-------------------------------------------------------------------------------------------------------------------<bR>" &vbCrLf
	body = body & "<table border=0 width=550 ><tr><td><font size=2 face='tahoma,verdana' color='#191970'>'" &vbCrLf
	body = body & text &vbCrLf
	body = body & "'</font></td></tr></table>" &vbCrLf
	body = body & "-------------------------------------------------------------------------------------------------------------------<br>" &vbCrLf
	body = body & ANSWERTEXT &vbCrLf
	body = body & BASICURL &vbCrLf
	body = body & "" &vbCrLf


	'on error resume next
	SendEMail MAIL_SVR, MAIL_PORT, author_email, author_name, rs("AuthorEMail"), rs("AuthorName"), subject, body

	if err<>0 then
		Response.Redirect fh.getUriReplyError(" &err.Description& ")
	end if
end if


' ***********************************************************************
' >> continue.

if err<>0 then
	Response.Write err
'	Response.End	
	Response.Redirect fh.getUriReplyError(err.Description)
else

'	Response.Redirect fh.getUriMain( parent_id )
	Response.Redirect "FRM_start.asp?forumid=" & Request("forumid") & "&select=" & parent_id & "&"
end if
%>
