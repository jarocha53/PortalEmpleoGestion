<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include virtual ="/include/controlDateVB.asp"-->

<%' mail.asp %>

<script language="JavaScript"><!--
			function chkFormular() {
				if ( document.form.Subject.value=="" ) {
					alert( "E' necessario inserire il soggetto!" );
					document.form.Subject.focus();
					return false;
				}

				if ( document.form.Text.value=="" ) {
					alert( "E' necessario inserire il messaggio!" );
					document.form.Text.focus();
					return false;
				}

				if ( document.form.AuthorName.value=="" ) {
					alert( "E' necessario inserire il nome del mittente!" );
					document.form.AuthorName.focus();
					return false;
				}

				if ( document.form.AuthorEmail.value=="" ) {
					alert( "E' necessario inserire un indirizzo email!" );
					document.form.AuthorEmail.focus();
					return false;
				}
			}
		//--></script>

		<%
		' ************************************************************

		dim article_rs
		set article_rs = fh.getArticleRs(Request("select"))

		dim reply_subject
		reply_subject = article_rs("Subject")

		if InStr( reply_subject, "Re:" )<>1 then
			reply_subject = "Re: " + reply_subject
		end if

		' ************************************************************

		forumid = Request("forumid")
		idfor = Request("select")
		
		%>


	

		<!-------------------------------------->
		<!-- the message to reply to -->

		<form action="FRM_send_mail.asp?forumid=<%=forumid%>&select=<%=idfor%>" method="POST" name="form" onsubmit="return chkFormular()">

		<table border="0" cellspacing="1" cellpadding="0" width="100%" class=righina>
			<tr>
				<td width="100%" "<%= COLOR_REPLY_BG%>">
					<table cellspacing="2" cellpadding="0" width="100%">
						<tr>
							<td><%=FONT_REPLY_FORM1%>Forum:</td>
							<td width="100%"><%=FONT_REPLY_FORM1%><b><%=fh.getForumName(article_rs("ForumID"))%></b></font></td>
						</tr>
						<tr>
							<td><%=FONT_REPLY_FORM1%>Oggetto:</font></td>
							<td width="100%"><%=FONT_REPLY_FORM1%><b><%=article_rs("Subject")%></b></font></td>
						</tr>
						<tr>
							<td><%=FONT_REPLY_FORM1%>Mittente:</font></td>
							<td width="100%"><%=FONT_REPLY_FORM1%><b><%=article_rs("AuthorName")%></b></font></td>
						</tr>
						<tr>
							<td><%=FONT_REPLY_FORM1%>Data:</font></td>
							<td width="100%"><%=FONT_REPLY_FORM1%><b><%=ConvDateToString(article_rs("Date"))%></b></font></td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td "<%= COLOR_CONTENT_BG%>">
					<table cellspacing="5" cellpadding="0" width="100%">
						<tr>
							<td><%=FONT_REPLY_FORM1%><%=fmtContent(article_rs("Text"))%></font></td>
						</tr>
					</table>
				</td>
			</tr>
		</table><bR>

		<!-------------------------------------->


			<input type="hidden" name="ParentID" value="<%=Request("select")%>">

			<div align="left">
				<table border="0" cellspacing="0" width=100% cellpadding="7" class=righina>
					<tr <%=COLOR_CONTENT_BG%>>
						<td valign="middle" width=15%><%=FONT_REPLY_FORM2%>Soggetto:</font></td>
						<td valign="middle" width=85% colspan="2"><input type="text" class=frmtext2 size="40" name="Subject" value="<%=reply_subject%>"></td>
					</tr>
					<tr <%=COLOR_CONTENT_BG%>>
						<td valign="top" ><%=FONT_REPLY_FORM2%>Testo:</font></td>
						<td valign="top" colspan="2"><textarea name="Text" class=frmtext2 rows="10" cols="80"></textarea></td>
					</tr>

					<tr <%=COLOR_CONTENT_BG%>>
						<td valign="middle" ><%=FONT_REPLY_FORM2%>Nome:</font></td>
						<td valign="middle" colspan="2"><input type="text" class=frmtext2 size="40" name="AuthorName"></font></td>
					</tr>
					<tr <%=COLOR_CONTENT_BG%>>
						<td valign="middle"><%=FONT_REPLY_FORM2%>E-Mail:</font></td>
						<td valign="middle"  colspan="2"><input type="text" class=frmtext2 size="40" name="AuthorEmail"></td>
					</tr>

					<tr <%=COLOR_CONTENT_BG%>>
						<td valign="top">&nbsp;</td>
						
						<td valign="top"  colspan="2">
							<table border="0" cellspacing="0" width="70%">
								<tr>
									<td valign="top" width="48%" align="center">
										<input type="image" src="/Plavoro/images/conferma.gif" alt="Invia" name="B1" WIDTH="55" HEIGHT="40">
									</td>
									<td valign="top" width="48%" align="center">
										<a href="/Plavoro" onClick="Javascript:history.back();return false">
										<input type="image" src="/Plavoro/images/Indietro.gif" alt="Torna Indietro" id="image1" name="image1" WIDTH="55" HEIGHT="40"></a>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</div>
		</form>
