<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%' new.asp %>


		<script language="JavaScript"><!--
			function chkFormular() {
				if ( document.form.Subject.value=="" ) {
					alert( "E' necessario inserire il soggetto!" );
					document.form.Subject.focus();
					return false;
				}

				if ( document.form.Text.value=="" ) {
					alert( "E' necessario inserire il messaggio!" );
					document.form.Text.focus();
					return false;
				}

				if ( document.form.AuthorName.value=="" ) {
					alert( "E' necessario inserire il nome del mittente!" );
					document.form.AuthorName.focus();
					return false;
				}
			}
		//--></script>

		<%
		' ************************************************************

		dim rs
		set rs = fh.getForumRs(Request("forumid"))
		

		' ************************************************************

		%>

		<p><%=FONT_REPLY_FORM2%>Campi da riempire per inviare un nuovo messaggio al Forum <b><%=rs("Name")%></b>.</font></p>
		<form action="FRM_insert_new.asp?forumid=<%=Request("forumid")%>" method="post" name="form" onsubmit="return chkFormular()">

			<input type="hidden" name="forumid" value="<%=Request("forumid")%>">

			<div align="left">
				<table border="0" cellspacing="0" width="100%" cellpadding=7 class=righina>
					<tr <%=COLOR_CONTENT_BG%>>
						<td valign="middle" width=15%><%= FONT_REPLY_FORM2 %>Soggetto:</font></td>
						<td valign="middle" width=85% colspan="2"><input type="text" class=frmtext2 size="35" name="Subject"></td>
					</tr>
					<tr <%=COLOR_CONTENT_BG%>>
						<td valign="top"><%=FONT_REPLY_FORM2%>Testo:</font></td>
						<td valign="top" colspan="2"><textarea name="Text" class=frmtext2 rows="10" cols="80"></textarea></td>
					</tr>
					<tr <%=COLOR_CONTENT_BG%>>
						<td valign="middle"><%=FONT_REPLY_FORM2%>Mittente:</font></td>
						<td valign="middle" colspan="2"><input type="text" class=frmtext2 size="35" name="AuthorName" value="<%=Session("admlogin")%>"></td>
					</tr>
					<tr <%=COLOR_CONTENT_BG%>>
						<td valign="middle"><%=FONT_REPLY_FORM2%>E-Mail:</font></td>
						<td valign="middle" colspan="2"><input type="text" class=frmtext2 size="35" name="AuthorEmail"></td>
					</tr>
					<tr <%=COLOR_CONTENT_BG%>>
						<td valign="middle">&nbsp;</td>
						<td valign="middle" colspan="2"><%=FONT_REPLY_FORM2%>
							<input type="checkbox" name="email_notify" value="ja"> Vuoi essere avvertito via email se qualcuno risponde al tuo messaggio?</font>
						</td>
					</tr> 

					<tr <%=COLOR_CONTENT_BG%>>
						<td valign="top">&nbsp;</td>
						
						<td valign="top" width="100%" colspan="2">
							<table border="0" cellspacing="0" width="70%">
								<tr>
									<td valign="top" width="48%" align="center">
										<input type="image" src="/Plavoro/images/conferma.gif" alt="Invia" name="B1" WIDTH="55">
									</td>
									<td valign="top" width="48%" align="center">
										<a href="/Plavoro" onClick="Javascript:history.back();return false">
										<input type="image" src="/Plavoro/images/Indietro.gif" alt="Torna Indietro" WIDTH="55"></a>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</div>
		</form>
