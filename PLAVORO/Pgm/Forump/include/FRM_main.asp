<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include virtual ="/include/controlDateVB.asp"-->
<%
' // draw the indent graphics.

' this function draws the indent and the article-image, based on the 
' indent-level and the state of the article.
sub WriteIndent( ind_lvl, state )
	dim i
	for i=1 to ind_lvl
		Response.Write "<img border=""0"" height=""1"" width=""20"" src=""images/ind.gif"">"
	next

	if state="selected" then
		Response.Write "<img border=""0"" align=""absmiddle"" src=""images/news_selected.gif"">"
	elseif state="content" then
		Response.Write "<img border=""0"" align=""absmiddle"" src=""images/news_none.gif"">"
	else
		Response.Write "<img border=""0"" align=""absmiddle"" src=""images/news_unselected.gif"">"
	end if

	Response.Write "<img border=""0"" width=""2"" height=""2"" align=""absmiddle"" src=""images/t.gif"">"
end sub

%>

<% fh.readArticles forum_id %>
<% if Session("admforum") = "yes" then %> 
<p>
	 [<a class=frmtext6 href="FRM_ModForum.asp?modifyforum=<%=forum_id%>" target="_top"><b>Modifica del forum</b></a>]
</p>
<% end if%>
<p>[<a class=frmtext6 href="<%=fh.getUriNew(forum_id)%>" target="_top"><b>Nuovo Messaggio</b></a>]
	<% if request("select") > "" then %> 
		 &nbsp;&nbsp;&nbsp;&nbsp;[<a class=frmtext6 href="FRM_Start.asp" target="_top"><b>Torna Elenco Messaggi</b></a>]
	<% end if%>
	<% if Session("admforum") = "yes" then %> 
		 &nbsp;&nbsp;&nbsp;&nbsp;[<a class=frmtext6 href="FRM_list.asp" target="_top"><b>Messaggi da Pubblicare</b></a>]
	<% end if%>


	<%
	if Session("admforum") = "yes" then
	%>
		 &nbsp;&nbsp;&nbsp;&nbsp;[<a class=frmtext6 href="FRM_EliminaMod.asp" target="_top"><b>Gestione Moderatori</b></a>]
		 &nbsp;&nbsp;&nbsp;&nbsp;[<a class=frmtext6 href="FRM_logout.asp" target="_top"><b>Esci</b></a>]
	<%
	end if	
	%>
</p>	

<TABLE border="0" cellspacing="0" cellpadding="0" width="100%" class=righina  bgcolor=WhiteSmoke>
	<tr>
		<td width="70%" colspan=2>
			<TABLE border="0" cellspacing="0" cellpadding="2" width="100%">
				<tr>
					<td <%= COLOR_HEADER_BG %>>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Titolo&nbsp;&nbsp;</font></td>
				</tr>
			</TABLE>
		</td>
		<td width="20%">
			<TABLE border="0" cellspacing="0" cellpadding="2" width="100%">
				<tr>
					<td <%= COLOR_HEADER_BG %> >Autore&nbsp;&nbsp;</font></td>
				</tr>
			</TABLE>
		</td>
		<td width="10%" colspan=2>
			<TABLE border="0" cellspacing="0" cellpadding="2" width="100%">
				<tr>
					<td <%= COLOR_HEADER_BG %> >Data&nbsp;&nbsp;</font></td>
				</tr>
			</TABLE>
		</td>
	</tr>
	<tr><td colspan="3"><img src="images/t.gif" border="0" width="1" height="2"></td></tr>

	<%	Dim rs_entries
		Dim sql
		Dim impag
		
		Dim nActPagina
		Dim nTotPagina
		Dim nTamPagina		
		Dim nTotRecord
		
		nIdart = Request("select")
		If Request("select") = "" Then
			If Session("admforum") = "yes"  Then
				sql= "SELECT * FROM Article WHERE ForumID ="& forum_id &" And parentId=0 ORDER BY [Position]"
			Else
				sql= "SELECT * FROM Article WHERE ForumID ="& forum_id &" And parentId=0 And Pubblicato=True ORDER BY [Position]"
			End If	
		Else
			sqlInit = "SELECT * FROM Article WHERE ForumID ="& forum_id 
			sqlEnd  = ") And Pubblicato=True ORDER BY [Position]"
		
			If Session("admforum") = "yes"  Then
				sql= sqlInit & " and (parentId=" & nIdart & " or ID = " & nIdart & ") ORDER BY [Position]"
			Else
				sql= sqlInit & " and (parentId=" & nIdart & " or ID = " & nIdart & sqlEnd
			End If	
		End If
        'Response.Write sql
		Set rs_entries = Server.CreateObject("ADODB.Recordset")
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
		rs_entries.open sql, fh.m_Conn, 1, 3
		'Variabile per i numeri di messaggi da visualizzare per pagina
		If Request("select") = "" Then
			nTamPagina = 7
		Else
			nTamPagina = 50
		End If
		'Variabile per i numeri di pagina
		If Request.Form("Page") = "" Then
			nActPagina = 1
		Else
			nActPagina = Clng(Request.Form("Page"))
		End If
		
' Gestione paginazione

		
	if not rs_entries.EOF then
		
		rs_entries.PageSize = nTamPagina
		rs_entries.CacheSize = nTamPagina

		nTotPagina = rs_entries.PageCount 
 
		If nActPagina < 1 Then
			nActPagina = 1
		End If
		If nActPagina > nTotPagina Then
			nActPagina = nTotPagina
		End If
	
		rs_entries.AbsolutePage=nActPagina

	end if
	
'	Response.Write nActPagina
	nTotRecord=0
	
    if rs_entries.EOF then
		nonc = "<center><table border=0 cellspacing=1 cellpadding=0 width='80%'><tr><td align=center class=frmtext6>Non ci sono messaggi</td></tr></table><br>"

	    if request("select") > "" then
			Response.Redirect "Frm_Start.asp"
	    end if
    end if	
%>    
	<script LANGUAGE="JavaScript">
		var t = 0;
	</script>
<%
	While not rs_entries.EOF  And nTotRecord < nTamPagina 
	
	' store some decision values.
		dim parent						' whether the current article is a parent article
		dim selected					' whether the current article is selected.
		parent   = (rs_entries("ParentID")=0)
		
		selected = (CStr(Request("select")) = CStr(rs_entries("ID")))

		' design: all parent entries are bold.
		dim bold_in, bold_out
		if parent then
			bold_in  = "<b>"
			bold_out = "</b>"
		else
			bold_in  = ""
			bold_out = ""
		end if

		' design: a selected article gets other colors than
		'         an unselected article.
		dim select_color
		dim text_color
		dim content_color

		if selected then
			text_color    = COLOR_SELECTED_FG
			select_color  = COLOR_SELECTED_BG
			content_color = COLOR_CONTENT_BG
		else
			text_color    = ""
			select_color  = COLOR_NONSELECTED_BG 
			content_color = COLOR_CONTENT_BG
		end if


		dim cnt_txt
		cnt_txt = ""

		' if the forum is in admin mode, there are serveral
		' links that allows you to edit and remove articles.
		dim admin_links
		
		idn = rs_entries("ID")
		sNameFun = "Can_" & idn
%>
<script LANGUAGE="JavaScript">
function <%=sNameFun%>(id)
{
	if (confirm("Sei sicuro di voler eliminare il messaggio e tutti i suoi figli?"))
		{
			document.location.href = '<% =fh.getUriDeleteCurMsgSubs(idn) %>'
		}
}
</script>
<%		
		
		if fh.isInAdminMode then
			admin_links = ""
			admin_links = admin_links & FONT1 &"<b><em>"
			admin_links = admin_links & "&nbsp;&nbsp;&nbsp;&nbsp; "
			admin_links = admin_links & "<a title=""Modifica il messaggio"" href=""" &fh.getUriModifyCurMsg(rs_entries("ID"))& """ target=""_top"">"
			admin_links = admin_links & "<img src=""images/admin_modify.gif"" border=""0"" align=""absmiddle"">"
			admin_links = admin_links & "</a>"
			admin_links = admin_links & " "
			admin_links = admin_links & "<a title=""Cancella il messaggio e tutti i suoi figli"" href=""javascript:" & sNameFun & "(" & idn & ");"" target=""_top"">"
'			admin_links = admin_links & "<a title=""Cancella il messaggio e tutti i suoi figli"" href=""javascript:if (confirm('Sei sicuro di voler eliminare il messaggio e tutti i suoi figli?"")) " & fh.getUriDeleteCurMsgSubs(idn) & ";"" target=""_top"">"
			admin_links = admin_links & "<img src=""images/admin_delall.gif"" border=""0"" align=""absmiddle"">"
			admin_links = admin_links & "</a>"
			admin_links = admin_links & " "
			list="no"
			idf= request("forumid")
			if not rs_entries("Pubblicato") then
				if  selected Then
					admin_links = admin_links & "<a title=""Pubblica il messaggio"" class=frmtext7 href=""" &fh.getPubbMsg(rs_entries("ID"),list,idf)& """ target=""_top"">"
					admin_links = admin_links & "</i>(Pubblica!)"
					admin_links = admin_links & "</a>"
				else
					admin_links = admin_links & "<span class=frmtext6></i>(Messaggio da Pubblicare)</span>"
				end if
			end if
			admin_links = admin_links & "</em></b></font>"
		else	
			admin_links = ""
		end if %>

		<!-- specify the distance of two articles by modifing the "height" attribute -->
		<tr><td colspan="3"><img src="images/t.gif" border="0" width="1" height="9"></td></tr>
					
		<!-- ********************************************************* -->
		<!-- a selected entry -->

<%		
		if selected then %>
		<tr>
				<!-- col 1 -->
			<td width="2%">&nbsp;</td>
			<td width="70%">
					<TABLE border="0" cellspacing="0" cellpadding="1" width="100%">
						<tr>
							<td><a name="<%= rs_entries("ID")%>"></a><%=FONT1%><%WriteIndent rs_entries("Indent"), ""%></font></td>
							<td <%=select_color%> width="100%"><%=FONT1%><%=bold_in%><%= rs_entries("Subject")%><%=bold_out%>
								<%=cnt_txt%> <%=admin_links%>&nbsp;&nbsp;</font></td>
						</tr>
					</TABLE>
				</td>

				<!-- col 2 -->
				<td nowrap <%=select_color%> width="20%"><%=FONT1%><%=bold_in%><%= rs_entries("AuthorName")%><%=bold_out%>&nbsp;&nbsp;</font></td>

				<!-- col 3 -->
				<td nowrap <%=select_color%> width="10%"><%=FONT1%><%=bold_in%><%=ConvDateToString(rs_entries("Date"))%><%=bold_out%>&nbsp;&nbsp;</font></td>
			<td width="3%">&nbsp;</td>
		</tr>

			<!-- the content of a selected entry -->
			<% if CStr(Request("select"))=CStr(rs_entries("ID")) then%>
				<tr>
					<!-- col 1,2 and 3 -->
					<td width="1%">&nbsp;</td>
					<td colspan="4" width="100%"><TABLE border="0" cellspacing="1" cellpadding="0" width="100%">
						<tr>
							<td><%=FONT1%><%WriteIndent rs_entries("Indent"), "content"%></font></td>
							<td  width="100%">
								<TABLE border="0" <%=content_color%> cellspacing="1" cellpadding="2" width="100%">
								<tr >
									<td><%=FONT1%><%=fmtContent(rs_entries("Text"))%></td>
								</tr>
								</TABLE>
								<TABLE border="0" cellspacing="1" cellpadding="2" width="100%">
								<tr class=frmsfondo3>
									<td><%=FONT1%>
										[<a href="<%=fh.getUriReplyForum%>" target="_top"><%=FONT1%>Rispondi</font></a>]
										[<a href="<%=fh.getUriReplyAuthor%>" target="_top"><%=FONT1%>Invia Email Autore</font></a>]
									</td>
								</tr>
								</TABLE>
							</td>
							<td width="3%">&nbsp;</td>
						</tr>
					</TABLE>
					</td>
				</tr>
<%				end if%>

		<!-- ********************************************************* -->
		<!-- a not-selected entry -->

<%		else %>
					
			<tr >
				<!-- col 1 -->
				<td width="1%">&nbsp;</td>
				<td width="70%" >
					<TABLE border="0" cellspacing="0" cellpadding="2" width="100%">
					<tr>
						<td ><a name="<%=rs_entries("ID")%>"></a><%=FONT1%><%WriteIndent rs_entries("Indent"), ""%></font></td>
						<td <%=select_color%> width="100%"><a class="LIST" href="<%=fh.getUriSelectMsg(rs_entries("ID"))%>" target="_top"><%=FONT1%><%=bold_in%><%=rs_entries("Subject")%><%=bold_out%></a><%=cnt_txt%> <%=admin_links%>&nbsp;&nbsp;</font></td>
					</tr>
					</TABLE>
				</td>

				<!-- col 2 -->
				<td nowrap width="20%" <%=select_color%>><%=FONT1%><%=bold_in%><%=rs_entries("AuthorName")%><%=bold_out%>&nbsp;&nbsp;</font></td>

				<!-- col 3 -->
				<td nowrap width="10%" <%=select_color%>><%=FONT1%><%=bold_in%><%=ConvDateToString(rs_entries("Date"))%><%=bold_out%>&nbsp;&nbsp;</font></td>
				<td width="3%">&nbsp;</td>
			</tr>
			<tr>
				<td colspan=5 align=right valign=top>
				    <table border=0 cellpadding=0 cellspacing=0 width=100%>
						<tr><td valign=top class=frmtext2>&nbsp;&nbsp;&nbsp;<a href="<%=fh.getUriSelectMsg(rs_entries("ID"))%>" class="LIST" target="_top"><%=FONT1%><%WriteIndent rs_entries("Indent"), "content"%>Numero Risposte <%=fh.getRispSelectMsg(rs_entries("ID"))%> al messaggio</a></td></tr>
					</table>
				</td>
			</tr>
<%		end if %>

					<!-- ********************************************************* -->
<% 

		nTotRecord=nTotRecord + 1	
		rs_entries.MoveNext
		
	wend	
%>
			<% 
			if nonc > "" then 
			Response.Write "<tr><td colspan=5 align=center class=frmtext2><br>" & nonc & "</td></tr>"
			end if 
			%>

	</TABLE>
<br>
<% 
If Request("select") = "" Then
Response.Write "<TABLE border=0 width='100%'><tr>"
	If nActPagina > 1 Then
		Response.Write "<FORM Name='PageDown' Method='post' Action='FRM_Start.asp'>"
		Response.Write "<input type='hidden' name='Id' value='" & Idtema & "'>"
		Response.Write "<input type='hidden' name='Page' value='" & nActPagina-1 & "'>"
		Response.Write "</FORM>"
		Response.Write "<td align='right' width='98%'>"
		Response.Write "<a class=tbltext href='/PLAVORO' onClick='Javascript:PageDown.submit();return false'>"
		Response.Write "<img border='0' alt='Pagina precedente' src='/PLAVORO/images/precedente.gif'></a></td>"
	End If
	If nActPagina < nTotPagina Then
		Response.Write "<FORM Name='PageUp' Method='post' Action='FRM_Start.asp'>"
		Response.Write "<input type='hidden' name='Id' value='" & Idtema & "'>"
		Response.Write "<input type='hidden' name='Page' value='" & nActPagina+1 & "'>"
		Response.Write "</FORM>"
		Response.Write "<td align='right'>"
		Response.Write "<a class=tbltext href='/PLAVORO' onClick='Javascript:PageUp.submit();return false'>"
		Response.Write "<img border='0' alt='Pagina successiva' src='/PLAVORO/images/successivo.gif'></a></td>"
	End If
Response.Write "</tr></TABLE>"
End If
%>
