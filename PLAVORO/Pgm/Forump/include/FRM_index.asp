<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->


<style type="text/css">
	.LIST:link    { text-decoration:none; }
	.LIST:visited { text-decoration:none; }
	.LIST:active  { text-decoration:none; }
	.LIST:hover   { text-decoration:underline; }
</style>

<% ' // button for admin mode. 
	If Session("admforum") = "no" Then
		sqltext =  "Login e/o password non corretta"
		Response.Write "<center><p class='frmtext7'><b>" & sqltext & "</b></p></center>"
	End If
%>

<% ' // the list of all available forums.                             %>

<TABLE class="frmsfondo2" border="0" cellspacing="1" cellpadding="2" width="100%">
<tr>
	<td align=left ><%= FONT4 %>&nbsp;Forum Attivi &gt;&nbsp;&nbsp;
<%
		dim forum_rs
		set forum_rs = fh.getForumsRs

		' display all forums by iterating the recordset.
		while not forum_rs.Eof
%>
			<b><a class="LIST" href="FRM_Start.asp" title="<%=forum_rs("Description")%>"><span class="frmtext4"><%=forum_rs("Name")%></span></a></b>&nbsp;&nbsp;&nbsp;
<%
			forum_rs.MoveNext
		wend
%>
    </td>
	<td align=right ><b></b></td>	
</tr>
</table>
	
<% ' // the article-list of the current forum.                       
		' ---------------------------------------------------------
		' select the forum that is specified in the parameters
		' for that page.
		' the caller may have either specified a forum id or
		' the short-name of a forum.
		dim forum_id
		dim forum_name
		forum_id   = Request("forumid")
		forum_name = Request("forum")

		' translate forum name to forum id (if no id present).
		if forum_id="" and forum_name<>"" then
			forum_id = fh.getForumId( forum_name )
		end if

		' if no forum selected, select first as default.
		if forum_id="" then
			dim rs
			set rs = fh.getForumsRs()
			forum_id = rs("ID")
		end if
%>


	<!--#include file="FRM_header.asp"-->


<% 
		' ---------------------------------------------------------
		' 'forum_id' must be dimmed and set correctly before
		' #including main.inc.
		'
		' main.inc #includes the list for the forum specified by
		' 'forum_id'.
%>

		<!--#include file="FRM_main.asp"-->
