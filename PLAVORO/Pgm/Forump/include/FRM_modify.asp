<%' modify.asp %>

		<script language="JavaScript"><!--
			function chkFormular() {
				if ( document.form.Subject.value=="" ) {
					alert( "E' necessario inserire il soggetto!" );
					document.form.Subject.focus();
					return false;
				}

				if ( document.form.Text.value=="" ) {
					alert( "E' necessario inserire il messaggio!" );
					document.form.Text.focus();
					return false;
				}

				if ( document.form.AuthorName.value=="" ) {
					alert( "E' necessario inserire il nome del mittente!" );
					document.form.AuthorName.focus();
					return false;
				}
			}
		//--></script>
		<%
		' ************************************************************

		dim rs
		set rs = fh.getArticleRs(Request("modify"))

		' ************************************************************
		%>
		
		<%=FONT_REPLY_FORM2%><b>Modifica il messaggio</b></font>

		<!-------------------------------------->

		<form action="<%=fh.getUriModify%>" method="post" name="form" onsubmit="return chkFormular()">

			<input type="hidden" name="id" value="<%=rs("ID")%>">

			<div align="left">
				<table border="0" cellspacing="0" width="100%" cellpadding=7 class=righina>
					<tr <%=COLOR_CONTENT_BG%>>
						<td valign="middle" width=15%><%=FONT_REPLY_FORM2%>Soggetto:</font></td>
						<td valign="middle" width=85% colspan="2"><input type="text" class=frmtext2 size="35" name="Subject" value="<%=rs("Subject")%>"></td>
					</tr>
					<tr <%=COLOR_CONTENT_BG%>>
						<td valign="top"><%=FONT_REPLY_FORM2%>Testo:</font></td>
						<td valign="top" colspan="2"><textarea name="Text" class=frmtext2 rows="10" cols="80"><%=rs("Text")%></textarea></td>
					</tr>
					<tr <%=COLOR_CONTENT_BG%>>
						<td valign="top">&nbsp;</td>
						<td valign="top" colspan="2">&nbsp;</td>
					</tr>
					<tr <%=COLOR_CONTENT_BG%>>
						<td valign="middle"><%=FONT_REPLY_FORM2%>Nome:</font></td>
						<td valign="middle" colspan="2"><input type="text" class=frmtext2 size="35" name="AuthorName" value="<%=rs("AuthorName")%>"></td>
					</tr>
					<tr <%=COLOR_CONTENT_BG%>>
						<td valign="middle"><%=FONT_REPLY_FORM2%>E-Mail:</font></td>
						<td valign="middle" colspan="2"><input type="text" class=frmtext2 size="35" name="AuthorEmail" value="<%=rs("AuthorEmail")%>"></td>
					</tr>
					<tr <%=COLOR_CONTENT_BG%>>
						<%
						dim notify_checked
						if rs("Notify")<>"" and isEMail(rs("AuthorEmail"))then
							notify_checked="checked" 
						else 
							notify_checked=""
						end if
						%>
						<td valign="middle">&nbsp;</td>
						<td valign="middle" colspan="2"><%=FONT_REPLY_FORM2%>
						<input type="checkbox" name="email_notify" value="ja"> Vuoi essere avvertito via email se qualcuno risponde al tuo messaggio?</font>
						</td>
					</tr> 
					<tr <%=COLOR_CONTENT_BG%>>
						<td valign="top">&nbsp;</td>
						
						<td valign="top" width="100%" colspan="2">
							<table border="0" cellspacing="0" width="70%">
								<tr>
									<td valign="top" width="48%" align="center">
										<input type="image" src="/Plavoro/images/conferma.gif" alt="Invia" name="B1" WIDTH="55" HEIGHT="40">
									</td>
									<td valign="top" width="48%" align="center">
										<a href="/Plavoro" onClick="Javascript:history.back();return false">
										<input type="image" src="/Plavoro/images/Indietro.gif" alt="Torna Indietro" id="image1" name="image1" WIDTH="55" HEIGHT="40"></a>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</div>
		</form>