<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%' insert_modify.asp
' check.

dim id
dim subject
dim author_name
dim author_email
dim text
dim notify

id					= Request("id")
shortName           = Request("shortname")
name				= Request("name")
description			= Request("description")

if id="" or shortName="" or name="" or description="" then
	Response.Redirect fh.getUriModifyError("Non sono stati riempiti tutti i campi")
end if


' ***********************************************************************
' modify.

'on error resume next
 
fh.modifyForumData _
	id, _
	shortName, _
	name, _
	description


' ***********************************************************************
' >> continue.

if err<>0 then
	Response.Redirect fh.getUriModifyError(err.Description)
else
	Response.Redirect fh.getUriMain( id )
end if
%>
