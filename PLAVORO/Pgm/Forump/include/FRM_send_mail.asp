<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%' send_mail.asp 
%>

<%
' ***********************************************************************
' check.

dim id
dim subject
dim author_name
dim author_email
dim text

id           = Request("select")
subject      = Request("Subject")
author_name  = Request("AuthorName")
author_email = Request("AuthorEmail")
text         = Request("Text")

if subject="" or author_name="" or author_email="" or text="" then
	Response.Redirect fh.getUriMailError("non sono stati riempiti tutti i campi")
end if


' ***********************************************************************
' get the e-mail adress of the receiver

dim rs
set rs = fh.getArticleRs( id )

dim rec_email
dim rec_name

rec_email = rs("AuthorEMail")
rec_name  = rs("AuthorName")


' ***********************************************************************
' build body.

dim body
body = ""
	body = ""
	body = body & "<html><body bgcolor='#DCE0E7'><font size=3 color='#191970'>"
	body = body & "<font size=4 face='tahoma,verdana' color='#191970'>FORUM "
	body = body & "<font size=4 face='tahoma,verdana' color='#ff7f50'>'" &fh.getForumName(rs("ForumID"))& "'<br><br>" &vbCrLf	
	body = body & "<font size=2 face='tahoma,verdana' color='#191970'>Questa mail ti � stata inviata da <b>" & author_name & "</b> in risposta al tuo messaggio dal titolo: "
	body = body & "</font><font size=2 face='tahoma,verdana' color='#ff7f50'><b>'" & subject & "'</b><br>"
	body = body & "<font size=2 face='tahoma,verdana' color='#191970'><bR>Testo del messaggio:" &vbCrLf
	body = body & "<br>-------------------------------------------------------------------------------------------------------------------<bR>" &vbCrLf
	body = body & "<table border=0 width=550 ><tr><td><font size=2 face='tahoma,verdana' color='#191970'>'" &vbCrLf
	body = body & text &vbCrLf
	body = body & "'</font></td></tr></table>" &vbCrLf
	body = body & "-------------------------------------------------------------------------------------------------------------------<br>" &vbCrLf
	body = body & ANSWERTEXT &vbCrLf
	body = body & BASICURL &vbCrLf
	body = body & "" &vbCrLf


' ***********************************************************************
' send via e-mail.

on error resume next

subject = ""

SendEMail MAIL_SVR, MAIL_PORT, author_email, author_name, rec_email, rec_name, subject, body

if err<>0 then
	Response.Redirect _
		fh.getUriEmailError(JMail.ErrorMessage& " (" &err.Description& ")")
end if


' ***********************************************************************
' >> continue.

Response.Redirect fh.getUriMain( id )
%>
