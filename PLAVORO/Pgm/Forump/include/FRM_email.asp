<%
' check email
public function isEMail( sCheckEMail )
	Dim sEmail, nAtLoc

	isEMail = True
	sEmail = Trim(sCheckEmail)
	nAtLoc = InStr(1, sEmail, "@") 'Location of "@"

	If Not (nAtLoc > 1 And (InStrRev(sEmail, ".") > nAtLoc + 1)) Then
		'"@" must exist, and last "." in string must follow the "@"
		isEMail = False
	ElseIf InStr(nAtLoc + 1, sEmail, "@") > nAtLoc Then
		'String can't have more than one "@"
		isEMail = False
	ElseIf Mid(sEmail, nAtLoc + 1, 1) = "." Then
		'String can't have "." immediately following "@"
		isEMail = False
	ElseIf InStr(1, Right(sEmail, 2), ".") > 0 Then
		'String must have at least a two-character top-level domain.
		isEMail = False
	End If
end function
'*********************************************************************


' ***********************************************************************
' send an email without attachment.
' the err object is set, if an error occurs.


sub SendEMail( mail_server, mail_port, mail_from, from_name, mail_to, to_name, mail_subject, mail_body )
	Dim objMailOperator
	Dim objMailRefer
	Dim HtmlOperat	
	Dim HtmlRefer
	
	' no empty sender allowed.
	if mail_from="" then mail_from = mail_to
	if from_name="" then from_name = to_name
	
		'----- Mail per Referenze Azienda ITALIA LAVORO------
		Set objMailRefer = Server.CreateObject("CDONTS.NewMail")
		objMailRefer.BodyFormat = cdoBodyFormatHTML 
		objMailRefer.MailFormat = cdoMailFormatMIME
		objMailRefer.Body = HtmlRefer 
		objMailRefer.Send mail_from, mail_to, "Risposta al tuo messaggio nel forum", mail_body

end sub
%>