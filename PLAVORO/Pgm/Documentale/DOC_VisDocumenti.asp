<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%@ Language=VBScript %>
<!-- #include virtual="/strutt_testa2.asp" -->
<!-- #include virtual="/include/OpenConn.asp" -->

<script language="javascript"> 
function Leggi(file,titolo) {   
		url = "DOC_Leggi.asp?file=" + file + "&tit=" + titolo

		  var DochWnd=window.open(url,"Doc","toolbar=no,width=620,left=1,top=1,height=550,directories=no,location=no,status=no,statusbar=no,resizable=yes,menubar=no,scrollbars=yes");
		  if(!DochWnd.opener) DochWnd.opener=self;
		  if(DochWnd.focus!=null) DochWnd.focus();
	}
function Scarica(nfile) {   
		url = "/Pgm/Documentale/DOC_Download.asp?nfile=" + nfile
		var DochSCR=window.open(url,"Doc1","toolbar=no,width=350,left=1,top=1,height=250,directories=no,location=no,status=no,statusbar=no,resizable=no,menubar=no,scrollbars=no");
	   if(!DochSCR.opener) DochSCR.opener=self;
	   if(DochSCR.focus!=null) DochSCR.focus();
	}
</script>   

<%
Dim nIdtema
nIdtema = Request.Form("Tema")

If nIdtema <> "" Then

	Dim nActPagina, nTotPagina
	Dim nTamPagina, nTotRecord
	Dim sDirScelta, Posbarra 
	Dim sPathname, iInd
	Dim fso, f

	nTamPagina = 5

	If Request.Form("Page") = "" Then
		nActPagina = 1
	Else
		nActPagina = Clng(Request.Form("Page"))
	End If
 	
	Set rstCategorie = Server.CreateObject("ADODB.Recordset")
	SQLcategorie = "SELECT Id_Categoria, Descrizione, Percorso, Padre, Abstract, Tipo " &_
				   " FROM Categoria WHERE Id_Categoria =" & nIdtema & " AND id_categoria in (SELECT id_categoria FROM Ruolo_Categoria " &_
				   "WHERE (cod_rorga in ('XX','"& Session("Rorga") &"') AND idgruppo IS NULL) OR (idgruppo='" & Session("idgruppo") & "') )"
'PL-SQL * T-SQL  
SQLCATEGORIE = TransformPLSQLToTSQL (SQLCATEGORIE) 
	rstCategorie.open SQLcategorie,CC,1,3
	If not rstCategorie.EOF Then
		sTitolo = rstCategorie("Descrizione")
	Else
		sTitolo = "Documentazione"
	End if
%>
<script language="javascript" src="/Include/help.inc"></script>
<br>
<table BORDER="0" WIDTH="500" CELLPADDING="0" CELLSPACING="0">
	<tr height="18">
		<td class="sfondomenu" width="67%" height="18"><span class="tbltext0"><b>&nbsp;PUBBLICAZIONI</b></span></td>
		<td width="3%" background="/PLAVORO/images/tondo_linguetta.gif"></td>
		<td valign="middle" align="right" class="tbltext1" width="40%" background="/PLAVORO/images/sfondo_linguetta.gif"></td>
	</tr>
</table>

<table border="0" width="500" CELLPADDING="0" cellspacing="0">
	<tr height="2">
		<td class="sfondocomm" width="100%" colspan="2" background="/PLAVORO/images/separazione.gif"></td>
	</tr>
	<tr>
		<td align="left" class="sfondocomm" class="tbltext1">
			&nbsp;Documenti pubblicati in '<b><%=sTitolo%></b>'.
		</td>
		<td class="sfondocomm">
			<a href="Javascript:Show_Help('/Pgm/Help/Pubblicazioni/DOC_VisDocumenti')">
			<img align="right" src="/PLAVORO/images/help.gif" border="0" alt="Help" WIDTH="18" HEIGHT="18"></a>
		</td>
	</tr>
	<tr height="2">
		<td class="sfondocomm" width="100%" colspan="2" background="/PLAVORO/images/separazione.gif"></td>
	</tr>
</table>
<br>
<%

	If not rstCategorie.EOF Then

		sDirScelta = rstCategorie("Percorso")
		sPathname = Server.MapPath("\") & "/PLAVORO/Testi/Sistdoc/Documentale/" & sDirScelta

		Set rstSubCategorie = Server.CreateObject("ADODB.Recordset")
		SQLSubCategorie = "SELECT Id_Categoria, Gif_Categoria, Descrizione, Abstract FROM Categoria WHERE Padre=" & nIdtema & " And id_categoria in (SELECT id_categoria FROM Ruolo_Categoria WHERE Cod_rorga in ('XX','"& Session("Rorga") &"')) ORDER BY Descrizione"
'PL-SQL * T-SQL  
SQLSUBCATEGORIE = TransformPLSQLToTSQL (SQLSUBCATEGORIE) 
		rstSubCategorie.open SQLSubCategorie, CC, 1, 3

		Response.Write "<TABLE width=500 border=0 >"
			
		iInd = 0
		Do while not rstSubCategorie.EOF
			iInd = iInd + 1 
			Response.Write "<tr><td width='50' valign='top' align='center' rowspan='2'>"
			If isnull(rstSubCategorie("Gif_Categoria")) Or rstSubCategorie("Gif_Categoria") = " " Then
				Response.Write "<img src='/images/icons/big/cartella.gif' width='50' height='33'>"
			Else
				Response.Write "<img src='" & "/PLAVORO/images/categorie/" & rstSubCategorie("Gif_Categoria") & "' width='50' height='50'>"
			End If 
			Response.Write "</td>"
			Response.Write "<FORM name='frm" & iInd & "' method='POST' action='Doc_VisDocumenti.asp'>"
		    Response.Write "<input type='hidden' name='Tema' value='" & rstSubCategorie("Id_Categoria") & "'>" 	
			Response.Write "<td align='left' width='450' class='tbltext'>"
			Response.Write "<a class='textred' href='/PLAVORO' onClick='Javascript:frm" & iInd & ".submit();return false'>"
			Response.Write "<b>" & rstSubCategorie("Descrizione") & "</b></a></td></FORM></tr>"
			Response.Write "<tr><td align='left' width='450' class='tbltext'>" & rstSubCategorie("Abstract") & "</tr>"
			rstSubCategorie.MoveNext
		Loop
		 
		Response.Write "</TABLE><br>"
		rstSubCategorie.Close
		Set rstSubCategorie = nothing 	

		Set rstNotizie = Server.CreateObject("ADODB.Recordset")
		SQLnotizie = "SELECT Id_Notizie, Abstract, Id_Categoria, Autore, Data_ins, Nome_Notizie, Id_TipoDoc, " &_
					 "Fl_Pubblicato, Titolo FROM Notizie WHERE Id_Categoria = " & nIdtema 
		SQLnotizie = SQLnotizie & " and Fl_Pubblicato = 1 ORDER BY Id_Notizie Desc"
'PL-SQL * T-SQL  
SQLNOTIZIE = TransformPLSQLToTSQL (SQLNOTIZIE) 
		rstNotizie.open SQLnotizie, CC, 1, 3
				
		rstNotizie.PageSize = nTamPagina
		rstNotizie.CacheSize = nTamPagina

		nTotPagina = rstNotizie.PageCount
	 
		If nActPagina < 1 Then
			nActPagina = 1
		End If
		If nActPagina > nTotPagina Then
			nActPagina = nTotPagina
		End If
		
		sNomePercorso= Replace(rstCategorie("Percorso"),"\","/")
				
		sPathDir = "/PLAVORO/Testi/sistdoc/Documentale/" & sNomePercorso & "/"		

		Response.Write "<TABLE width='500' border='0' class='tblsfondo'>"
		
		If not rstNotizie.EOF Then	
			Response.Write "<tr><td>"
			rstNotizie.AbsolutePage = nActPagina
			nTotRecord=0
			While not rstNotizie.EOF And nTotRecord < nTamPagina 
				Response.Write "<TABLE width='500' border='0' class='sfondodoc'>"			
				Set rstTipo = Server.CreateObject("ADODB.Recordset")
				SQLTipo = "SELECT Id_TipoDoc, Tipo, Gif_Tipo FROM TipoDoc WHERE Id_TipoDoc=" & rstNotizie("Id_TipoDoc")
'PL-SQL * T-SQL  
SQLTIPO = TransformPLSQLToTSQL (SQLTIPO) 
				rstTipo.open SQLTipo, CC, 1, 3
						
				sPathFile = sPathname & "/" & rstNotizie("Nome_Notizie") & "." & rstTipo("Tipo")
				
				Set fso = CreateObject("Scripting.FileSystemObject")		
				Set fl = fso.GetFile(sPathFile) 
						
				Response.Write "<tr><td width='50' align='center' rowspan='3'><img src='/images/icons/big/" & rstTipo("Gif_Tipo") & "'>"
				Response.Write "</td><td align='left' width='450' class='tbltext' colspan='2'>"
							
				sPosFile = sPathDir & rstNotizie("Nome_Notizie") & "." & rstTipo("Tipo")
				sPosFile = Replace(sPosFile,"'","$")
				sTitolo = Replace(rstNotizie("Titolo"),"'","$")
				Response.Write "<a class='tbltext' href=""javascript:Leggi('" & sPosFile & "','" & sTitolo & "')""><b>" & rstNotizie("Titolo") & "</b></a></td>"
						
				If fl.Size > 1024 Then
					nDimDoc = Round(fl.Size/1024,2)
					sStrDimDoc = nDimDoc & " Kb"
				Else
					nDimDoc = fl.Size
					sStrDimDoc = nDimDoc & " bytes"
				End If
				Response.Write "</tr>"
				Response.Write "<tr><td width='450' align='left' class='tbltext' colspan='2'>" & rstNotizie("Abstract") & "</td></tr>"
				Response.Write "<tr><td class='tbltext1' width='420'>"
				Response.Write rstNotizie("Autore") & " - " & rstNotizie("Data_Ins") & " - " & sStrDimDoc & "</td>"
				Response.Write "<td align='right' width='30'><a class='tbltext' href=""javascript:Scarica('" & sPosFile & "')""><img src=""/italialavoro/images/download.gif"" border=0 alt=""download""></a></td>"
				Response.Write "</tr>"
				
				nTotRecord=nTotRecord + 1	
				rstNotizie.MoveNext
				Response.Write "</TABLE>"
			Wend
			rstTipo.Close
			Set rstTipo = nothing 
		Response.Write "</td></tr>"
		End If  
		Response.Write "</TABLE>"
		
		Response.Write "<br><TABLE border='0' width='500'>"
		Response.Write "<tr>"
			If nActPagina > 1 Then
				Response.Write "<FORM method='POST' action='Doc_VisDocumenti.asp' name='Down'>"
				Response.Write "<input type='hidden' name='Tema' value='" & nIdtema & "'>"
				Response.Write "<input type='hidden' name='Page' value='" & nActPagina-1 & "'>"
				Response.Write"</FORM>"
				Response.Write "<td align='right' width='480'><a href='/PLAVORO' onclick='Javascript:Down.submit(); return false'>"	
				Response.Write "<img border='0' alt='Pagina precedente' src='/PLAVORO/Images/precedente.gif'></a></td>"
			End If
			If nActPagina < nTotPagina Then
				Response.Write "<FORM method='POST' action='Doc_VisDocumenti.asp' name='Up'>"
				Response.Write "<input type='hidden' name='Tema' value='" & nIdtema & "'>"
				Response.Write "<input type='hidden' name='Page' value='" & nActPagina+1 & "'>"
				Response.Write"</FORM>"
				Response.Write "<td align='right'><a href='/PLAVORO' onclick='Javascript:Up.submit(); return false'>"		
				Response.Write "<img border='0' alt='Pagina successiva' src='/PLAVORO/Images/successivo.gif'></a></td>"
			End If
			If nActPagina = nTotPagina and nTotPagina = 1 Then
				Response.Write "<td align='center'><a href='/PLAVORO' onclick='Javascript:history.back(); return false'><img src='/PLAVORO/images/indietro.gif' alt='Torna indietro' border='0'></a></td>"
			End If
		Response.Write "</tr></TABLE>"

		rstNotizie.Close
		Set rstNotizie = nothing

	Else
		Response.Write "<TABLE width='500' border='0'>"
		Response.Write "<tr class='tblsfondo'><td colspan='3' width='500' class='tbltext' align='center'><b>--Non esistono documenti--</b></td></tr>"
		Response.Write "</TABLE><br>"
	End if

	rstCategorie.Close
	Set rstCategorie = nothing 
Else
	Response.Redirect "/PLAVORO/home.asp"
End If
%>

<!-- #include virtual="/include/closeconn.asp" -->
<!-- #include virtual="/strutt_coda2.asp" -->
