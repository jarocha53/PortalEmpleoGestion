
<!--#include Virtual="/strutt_testa3.asp"-->

<div align="center">
  <center>
  <table border="0" width="520" cellspacing="0" cellpadding="0" height="73">
    <tr>
      <td width="520" background="/PLAVORO/images/titoli/servizi2b.gif" height="73" valign="bottom" align="right">
        <table border="0" background width="320" height="30" cellspacing="0" cellpadding="0">
          <tr>
            <td width="100%" valign="top" align="right" class="tbltext1a"><b>Lo Scenario - Piemonte&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  </center>

<table border="0" width="520" cellspacing="0" cellpadding="0" height="73">
  <tr>
    <td width="100%"><center>
        <table border="0" width="95%" cellspacing="0" cellpadding="0">
          <tr>
            <td width="100%" align="center">
				<img src="/PLAVORO/Images/regioni/piemonte.gif" width="252" height="293" usemap="#Map" border="0"> 
				<map name="Map"> 
				  <area shape="circle" coords="170,80,6" href="/PLAVORO/testi/info/infosecondolivello/inforegioni/piemonte/info_verbania.asp" alt="Verbania" title="Verbania">
				  <area shape="circle" coords="122,123,6" href="/PLAVORO/testi/info/infosecondolivello/inforegioni/piemonte/info_biella.asp" alt="Biella" title="Biella">
				  <area shape="circle" coords="164,139,6" href="/PLAVORO/testi/info/infosecondolivello/inforegioni/piemonte/info_novara.asp" alt="Novara" title="Novara">
				  <area shape="circle" coords="91,177,6" href="/PLAVORO/testi/info/infosecondolivello/inforegioni/piemonte/info_torino.asp" alt="Torino" title="Torino">
				  <area shape="circle" coords="79,247,6" href="/PLAVORO/testi/info/infosecondolivello/inforegioni/piemonte/info_cuneo.asp" alt="Cuneo" title="Cuneo">
				  <area shape="circle" coords="141,210,6" href="/PLAVORO/testi/info/infosecondolivello/inforegioni/piemonte/info_asti.asp" alt="Asti" title="Asti">
				  <area shape="circle" coords="182,208,6" href="/PLAVORO/testi/info/infosecondolivello/inforegioni/piemonte/info_alessandria.asp" alt="Alessandria" title="Alessandria">
				  <area shape="circle" coords="156,158,6" href="/PLAVORO/testi/info/infosecondolivello/inforegioni/piemonte/info_vercelli.asp" alt="Vercelli" title="Vercelli">
				</map>
			</td>
 		  </tr>
        </table></center>
	</td>
  </tr>
  <tr>
    <td width="100%"><center>
        <table border="0" width="95%" cellspacing="0" cellpadding="0">
          <tr>
            <td width="100%"><br>
				<%
					PathFileEdit = "\PLAVORO\Testi\sistdoc\Sistema Redazionale\infosecondolivello\inforegioni\piemonte\info_piemonte.htm"
					on error resume next
					Response.Write Server.Execute(PathFileEdit)
					If err.number <> 0 Then
							Response.Write "<b class='tbltext'><br>Pagina al momento non disponibile"
							Response.Write "<p>Contattare il Gruppo Assistenza Portale Italia Lavoro<br>"
							Response.Write "all'indirizzo <a href='mailto:po-assistenza@italialavoro.it'>po-assistenza@italialavoro.it</a></p></b>"
					End If
				%>
			</td>
		  </tr>
        </table></center>
	</td>
  </tr>
 </table>
  <br>
 <table border="0" width="520">
	<tr>
		<td align="center">
			<a HREF="javascript:history.back()"><img SRC="/PLAVORO/Images/indietro.gif" border="0"></a>
		</td>
	</tr>
 </table>
</div>


<!--#include Virtual="/strutt_coda3.asp"-->