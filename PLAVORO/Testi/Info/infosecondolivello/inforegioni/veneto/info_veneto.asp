<!--#include Virtual="/strutt_testa3.asp"-->

<div align="center">
  <center>
  <table border="0" width="520" cellspacing="0" cellpadding="0" height="73">
    <tr>
      <td width="520" background="/PLAVORO/images/titoli/servizi2b.gif" height="73" valign="bottom" align="right">
        <table border="0" background width="320" height="30" cellspacing="0" cellpadding="0">
          <tr>
            <td width="100%" valign="top" align="right" class="tbltext1a"><b>Lo Scenario - Veneto&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  </center>

<table border="0" width="520" cellspacing="0" cellpadding="0" height="73">
  <tr>
    <td width="100%"><center>
        <table border="0" width="95%" cellspacing="0" cellpadding="0">
          <tr>
            <td width="100%" align="center">
				<img src="/PLAVORO/Images/regioni/veneto.gif" width="261" height="271" usemap="#Map" border="0"> 
				<map name="Map"> 
				  <area shape="circle" coords="155,70,9" href="/PLAVORO/testi/info/infosecondolivello/inforegioni/veneto/info_belluno.asp" alt="Belluno" title="Belluno">
				  <area shape="circle" coords="166,140,9" href="/PLAVORO/testi/info/infosecondolivello/inforegioni/veneto/info_treviso.asp" alt="Treviso" title="Treviso">
				  <area shape="circle" coords="97,156,9" href="/PLAVORO/testi/info/infosecondolivello/inforegioni/veneto/info_vicenza.asp" alt="Vicenza" title="Vicenza">
				  <area shape="circle" coords="123,192,8" href="/PLAVORO/testi/info/infosecondolivello/inforegioni/veneto/info_padova.asp" alt="Padova" title="Padova">
				  <area shape="circle" coords="55,180,8" href="/PLAVORO/testi/info/infosecondolivello/inforegioni/veneto/info_verona.asp" alt="Verona" title="Verona">
				  <area shape="circle" coords="131,231,9" href="/PLAVORO/testi/info/infosecondolivello/inforegioni/veneto/info_rovigo.asp" alt="Rovigo" title="Rovigo">
				  <area shape="circle" coords="154,177,10" href="/PLAVORO/testi/info/infosecondolivello/inforegioni/veneto/info_venezia.asp" alt="Venezia" title="Venezia">
				</map>		    
			</td>
 		  </tr>
        </table></center>
	</td>
  </tr>
  <tr>
    <td width="100%"><center>
        <table border="0" width="95%" cellspacing="0" cellpadding="0">
          <tr>
            <td width="100%"><br>
				<%
					PathFileEdit = "\PLAVORO\Testi\sistdoc\Sistema Redazionale\infosecondolivello\inforegioni\veneto\info_veneto.htm"
					on error resume next
					Response.Write Server.Execute(PathFileEdit)
					If err.number <> 0 Then
							Response.Write "<b class='tbltext'><br>Pagina al momento non disponibile"
							Response.Write "<p>Contattare il Gruppo Assistenza Portale Italia Lavoro<br>"
							Response.Write "all'indirizzo <a href='mailto:po-assistenza@italialavoro.it'>po-assistenza@italialavoro.it</a></p></b>"
					End If
				%>
			</td>
		  </tr>
        </table></center>
	</td>
  </tr>
 </table>
  <br>
 <table border="0" width="520">
	<tr>
		<td align="center">
			<a HREF="javascript:history.back()"><img SRC="/PLAVORO/Images/indietro.gif" border="0"></a>
		</td>
	</tr>
 </table>
</div>


<!--#include Virtual="/strutt_coda3.asp"-->