<!--#include Virtual="/strutt_testa3.asp"-->

<div align="center">
  <center>
  <table border="0" width="520" cellspacing="0" cellpadding="0" height="73">
    <tr>
      <td width="520" background="/PLAVORO/images/titoli/servizi2b.gif" height="73" valign="bottom" align="right">
        <table border="0" background width="320" height="30" cellspacing="0" cellpadding="0">
          <tr>
            <td width="100%" valign="top" align="right" class="tbltext1a"><b>Lo Scenario - Lombardia&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  </center>

<table border="0" width="520" cellspacing="0" cellpadding="0" height="73">
  <tr>
    <td width="100%"><center>
        <table border="0" width="95%" cellspacing="0" cellpadding="0">
          <tr>
            <td width="100%" align="center">
				<img src="/PLAVORO/Images/regioni/lombardia.gif" width="310" height="245" usemap="#Map" border="0"> 
				<map name="Map"> 
				  <area shape="circle" coords="146,58,6" href="/PLAVORO/testi/info/infosecondolivello/inforegioni/lombardia/info_sondrio.asp" alt="Sondrio" title="Sondrio">
				  <area shape="circle" coords="119,90,6" href="/PLAVORO/testi/info/infosecondolivello/inforegioni/lombardia/info_lecco.asp" alt="Lecco" title="Lecco">
				  <area shape="circle" coords="92,97,6" href="/PLAVORO/testi/info/infosecondolivello/inforegioni/lombardia/info_como.asp" alt="Como" title="Como">
				  <area shape="circle" coords="57,98,6" href="/PLAVORO/testi/info/infosecondolivello/inforegioni/lombardia/info_varese.asp" alt="Varese" title="Varese">
				  <area shape="circle" coords="98,133,6" href="/PLAVORO/testi/info/infosecondolivello/inforegioni/lombardia/info_milano.asp" alt="Milano" title="Milano">
				  <area shape="circle" coords="145,113,6" href="/PLAVORO/testi/info/infosecondolivello/inforegioni/lombardia/info_bergamo.asp" alt="Bergamo" title="Bergamo">
				  <area shape="circle" coords="212,187,6" href="/PLAVORO/testi/info/infosecondolivello/inforegioni/lombardia/info_mantova.asp" alt="Mantova" title="Mantova">
				  <area shape="circle" coords="157,189,6" href="/PLAVORO/testi/info/infosecondolivello/inforegioni/lombardia/info_cremona.asp" alt="Cremona" title="Cremona">
				  <area shape="circle" coords="116,170,6" href="/PLAVORO/testi/info/infosecondolivello/inforegioni/lombardia/info_lodi.asp" alt="Lodi" title="Lodi">
				  <area shape="circle" coords="75,178,6" href="/PLAVORO/testi/info/infosecondolivello/inforegioni/lombardia/info_pavia.asp" alt="Pavia" title="Pavia">
				  <area shape="circle" coords="178,137,6" href="/PLAVORO/testi/info/infosecondolivello/inforegioni/lombardia/info_brescia.asp" alt="Brescia" title="Brescia">
				</map>
			</td>
 		  </tr>
        </table></center>
	</td>
  </tr>
  <tr>
    <td width="100%"><center>
        <table border="0" width="95%" cellspacing="0" cellpadding="0">
          <tr>
            <td width="100%"><br>
				<%
					PathFileEdit = "\PLAVORO\Testi\sistdoc\Sistema Redazionale\infosecondolivello\inforegioni\lombardia\info_lombardia.htm"
					on error resume next
					Response.Write Server.Execute(PathFileEdit)
					If err.number <> 0 Then
							Response.Write "<b class='tbltext'><br>Pagina al momento non disponibile"
							Response.Write "<p>Contattare il Gruppo Assistenza Portale Italia Lavoro<br>"
							Response.Write "all'indirizzo <a href='mailto:po-assistenza@italialavoro.it'>po-assistenza@italialavoro.it</a></p></b>"
					End If
				%>
			</td>
		  </tr>
        </table></center>
	</td>
  </tr>
 </table>
  <br>
 <table border="0" width="520">
	<tr>
		<td align="center">
			<a HREF="javascript:history.back()"><img SRC="/PLAVORO/Images/indietro.gif" border="0"></a>
		</td>
	</tr>
 </table>
</div>


<!--#include Virtual="/strutt_coda3.asp"-->