<!--#include Virtual="/strutt_testa3.asp"-->

<div align="center">
  <center>
  <table border="0" width="520" cellspacing="0" cellpadding="0" height="73">
    <tr>
      <td width="520" background="/PLAVORO/images/titoli/servizi2b.gif" height="73" valign="bottom" align="right">
        <table border="0" background width="320" height="30" cellspacing="0" cellpadding="0">
          <tr>
            <td width="100%" valign="top" align="right" class="tbltext1a"><b>Lo Scenario - Toscana&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  </center>

<table border="0" width="520" cellspacing="0" cellpadding="0" height="73">
  <tr>
    <td width="100%"><center>
        <table border="0" width="95%" cellspacing="0" cellpadding="0">
          <tr>
            <td width="100%" align="center">
				<img src="/PLAVORO/Images/regioni/toscana.gif" width="235" height="244" usemap="#Map" border="0"> 
				<map name="Map"> 
				  <area shape="circle" coords="58,41,6" href="/PLAVORO/testi/info/infosecondolivello/inforegioni/toscana/info_massa_carrara.asp" alt="Massa Carrara" title="Massa Carrara">
				  <area shape="circle" coords="124,62,6" href="/PLAVORO/testi/info/infosecondolivello/inforegioni/toscana/info_pistoia.asp" alt="Pistoia" title="Pistoia">
				  <area shape="circle" coords="100,69,6" href="/PLAVORO/testi/info/infosecondolivello/inforegioni/toscana/info_lucca.asp" alt="Lucca" title="Lucca">
				  <area shape="circle" coords="136,75,6" href="/PLAVORO/testi/info/infosecondolivello/inforegioni/toscana/info_prato.asp" alt="Prato" title="Prato">
				  <area shape="circle" coords="151,92,6" href="/PLAVORO/testi/info/infosecondolivello/inforegioni/toscana/info_firenze.asp" alt="Firenze" title="Firenze">
				  <area shape="circle" coords="192,112,6" href="/PLAVORO/testi/info/infosecondolivello/inforegioni/toscana/info_arezzo.asp" alt="Arezzo" title="Arezzo">
				  <area shape="circle" coords="154,143,6" href="/PLAVORO/testi/info/infosecondolivello/inforegioni/toscana/info_siena.asp" alt="Siena" title="Siena">
				  <area shape="circle" coords="132,183,6" href="/PLAVORO/testi/info/infosecondolivello/inforegioni/toscana/info_grosseto.asp" alt="Grosseto" title="Grosseto">
				  <area shape="circle" coords="87,110,6" href="/PLAVORO/testi/info/infosecondolivello/inforegioni/toscana/info_livorno.asp" alt="Livorno" title="Livorno">
				  <area shape="circle" coords="84,87,6" href="/PLAVORO/testi/info/infosecondolivello/inforegioni/toscana/info_pisa.asp" alt="Pisa" title="Pisa">
				</map>
			</td>
 		  </tr>
        </table></center>
	</td>
  </tr>
  <tr>
    <td width="100%"><center>
        <table border="0" width="95%" cellspacing="0" cellpadding="0">
          <tr>
            <td width="100%"><br>
				<%
					PathFileEdit = "\PLAVORO\Testi\sistdoc\Sistema Redazionale\infosecondolivello\inforegioni\toscana\info_toscana.htm"
					on error resume next
					Response.Write Server.Execute(PathFileEdit)
					If err.number <> 0 Then
							Response.Write "<b class='tbltext'><br>Pagina al momento non disponibile"
							Response.Write "<p>Contattare il Gruppo Assistenza Portale Italia Lavoro<br>"
							Response.Write "all'indirizzo <a href='mailto:po-assistenza@italialavoro.it'>po-assistenza@italialavoro.it</a></p></b>"
					End If
				%>
			</td>
		  </tr>
        </table></center>
	</td>
  </tr>
 </table>
  <br>
 <table border="0" width="520">
	<tr>
		<td align="center">
			<a HREF="javascript:history.back()"><img SRC="/PLAVORO/Images/indietro.gif" border="0"></a>
		</td>
	</tr>
 </table>
</div>


<!--#include Virtual="/strutt_coda3.asp"-->