<!--#include Virtual="/strutt_testa2.asp"-->

<div align="center">
  <center>
  <table border="0" width="520" cellspacing="0" cellpadding="0" height="73">
    <tr>
      <td width="520" background="/PLAVORO/images/titoli/servizi2b.gif" height="73" valign="bottom" align="right">
        <table border="0" background width="320" height="30" cellspacing="0" cellpadding="0">
          <tr>
            <td width="100%" valign="top" align="right" class="tbltext1a"><b>Lo Scenario - Italia&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  </center>

<table border="0" width="520" cellspacing="0" cellpadding="0" height="73">
  <tr>
    <td width="100%"><center>
      <div align="center">
        <table border="0" width="95%" cellspacing="0" cellpadding="0">
			 <tr>
				<td width="100%" align="center"><font face="Verdana" size="2">
					<img SRC="/PLAVORO/Images/regioni/italiamap.gif" border="0" usemap="#Map" WIDTH="243" HEIGHT="305">

					<map name="Map"> 
					  <area shape="poly" coords="32,30,12,32,11,38,5,43,5,50,3,57,6,64,9,67,15,69,18,71,23,71,23,67,39,59,44,59,46,55,44,50,40,49,38,46,38,41,39,38,42,37,42,34,40,30,41,26,40,21,44,20,41,15" href="/PLAVORO/testi/info/infosecondolivello/inforegioni/piemonte/info_piemonte.asp" alt="Piemonte" title="Piemonte">
					  <area shape="poly" coords="9,31,7,24,9,23,13,23,16,23,19,23,23,22,25,22,26,25,28,29,26,32" href="/PLAVORO/testi/info/infosecondolivello/inforegioni/valle%252520d'aosta/info_valleaosta.asp" alt="Valle d'Aosta" title="Valle d'Aosta">
					  <area shape="poly" coords="56,20,61,18,65,18,69,18,72,18,74,16,76,12,81,11,83,14,82,18,79,22,79,25,80,29,82,32,85,33,80,38,82,42,85,45,87,50,90,53,93,57,81,58,70,53,62,51,55,51,54,58,50,61,48,57,46,52,42,50,39,48,38,43,41,41,44,35,42,31,42,22,44,21,48,26,51,24" href="/PLAVORO/testi/info/infosecondolivello/inforegioni/lombardia/info_lombardia.asp" alt="Lombardia" title="Lombardia">
					  <area shape="poly" coords="92,6,96,4,98,2,102,3,106,3,112,3,117,7,119,12,113,11,109,15,107,18,109,22,104,27,98,28,95,32,91,36,87,32,84,29,82,26,82,20,84,17,84,9,85,6,88,5" href="/PLAVORO/testi/info/infosecondolivello/inforegioni/trentino%252520alto%252520adige/info_trentino.asp" alt="Trentino Alto Adige" title="Trentino Alto Adige">
					  <area shape="poly" coords="118,13,121,13,124,16,118,19,117,22,118,30,120,34,126,38,123,40,118,43,116,46,115,51,114,55,114,59,113,61,109,58,105,58,99,59,96,60,93,57,91,53,89,49,86,46,81,42,82,40,84,36,87,37,89,38,94,38,96,35,100,31,112,13,110,15,108,19,106,24,104,27,101,29" href="/PLAVORO/testi/info/infosecondolivello/inforegioni/veneto/info_veneto.asp" alt="Veneto" title="Veneto">
					  <area shape="poly" coords="138,16,141,22,141,28,142,36,135,37,129,38,123,36,120,33,119,28,118,24,119,20,123,19,127,15,129,14,133,15" href="/PLAVORO/testi/info/infosecondolivello/inforegioni/friuli%252520venezia%252520giulia/info_friuli.asp" alt="Friuli Venezia Giulia" title="Friuli Venezia Giulia">
					  <area shape="poly" coords="54,50,60,50,64,53,68,55,75,56,82,59,88,59,96,59,104,59,110,59,113,59,113,66,112,69,112,75,112,79,114,84,116,87,118,91,113,93,106,93,98,88,91,83,85,84,81,84,75,81,71,79,67,75,65,70,60,68,54,64,53,58,52,54" href="/PLAVORO/testi/info/infosecondolivello/inforegioni/emilia%252520romagna/info_emilia.asp" alt="Emilia Romagna" title="Emilia Romagna">
					  <area shape="poly" coords="23,81,28,77,32,73,35,72,40,71,44,71,48,75,53,77,57,79,61,82,63,81,63,76,61,71,58,69,55,66,51,62,50,60,47,59,42,59,36,59,33,61,28,63,25,65,23,68,21,70,18,72,16,75,13,80,14,83,17,84,18,84" href="/PLAVORO/testi/info/infosecondolivello/inforegioni/liguria/info_liguria.asp" alt="Liguria" title="Liguria">
					  <area shape="poly" coords="66,77,70,78,73,80,78,82,84,84,88,84,92,85,95,87,98,90,101,93,104,95,107,96,108,100,106,104,106,109,102,113,100,116,99,121,98,125,97,129,94,130,89,130,86,130,83,127,81,123,76,123,75,125,74,126,69,126,65,122,61,118,62,115,66,114,68,114,70,113,72,109,70,103,69,98,68,95,68,92,68,87,66,83,65,79" href="/PLAVORO/testi/info/infosecondolivello/inforegioni/toscana/info_toscana.asp" alt="Toscana" title="Toscana">
					  <area shape="poly" coords="108,132,111,135,114,133,120,129,125,129,130,128,127,131,125,133,127,140,127,142,124,145,122,147,125,150,128,152,132,154,135,156,138,159,139,163,137,167,128,169,124,167,116,163,112,159,105,153,102,149,100,145,97,143,94,139,93,132,97,128,101,125,104,126,105,129" href="/PLAVORO/testi/info/infosecondolivello/inforegioni/lazio/info_lazio.asp" alt="Lazio" title="Lazio">
					  <area shape="poly" coords="106,94,113,94,118,93,126,97,130,101,135,106,139,112,140,117,141,123,136,127,130,126,124,121,120,117,120,110,119,107,115,103,111,99" href="/PLAVORO/testi/info/infosecondolivello/inforegioni/marche/info_marche.asp" alt="Marche" title="Marche">
					  <area shape="poly" coords="109,97,113,101,118,105,118,110,120,116,122,122,127,126,119,129,114,135,111,136,107,132,104,128,101,127,99,125,100,118,104,114,106,110,109,108,108,102" href="/PLAVORO/testi/info/infosecondolivello/inforegioni/umbria/info_umbria.asp" alt="Umbria" title="Umbria">
					  <area shape="poly" coords="144,127,144,132,147,136,150,140,154,144,158,147,161,151,157,155,154,160,151,157,145,161,142,161,139,158,136,155,132,153,129,151,126,149,125,147,127,143,127,135,129,131,134,129,136,128,139,128,141,128" href="/PLAVORO/testi/info/infosecondolivello/inforegioni/abruzzo/info_abruzzo.asp" alt="Abruzzo" title="Abruzzo">
					  <area shape="poly" coords="26,178,28,176,32,174,36,172,39,172,41,177,43,182,45,189,46,192,41,196,42,202,41,207,40,214,40,220,38,227,36,230,31,229,25,230,20,235,16,237,12,233,10,230,11,220,12,212,14,206,15,200,15,195,15,190,13,185" href="/PLAVORO/testi/info/infosecondolivello/inforegioni/sardegna/info_sardegna.asp" alt="Sardegna" title="Sardegna">
					  <area shape="poly" coords="161,152,165,155,166,159,166,165,162,168,158,172,154,171,150,170,144,167,141,164,146,160,150,159,153,161,157,157,159,154" href="/PLAVORO/testi/info/infosecondolivello/inforegioni/molise/info_molise.asp" alt="Molise" title="Molise">
					  <area shape="poly" coords="151,171,157,171,163,171,165,171,168,175,170,179,173,182,172,186,171,190,176,202,175,205,167,206,164,203,162,198,160,193,156,190,153,189,149,186,147,183,144,180,141,178,140,172,143,167,147,168" href="/PLAVORO/testi/info/infosecondolivello/inforegioni/campania/info_campania.asp" alt="Campania" title="Campania">
					  <area shape="poly" coords="173,158,183,158,190,159,187,164,190,173,196,177,207,180,213,187,222,192,228,197,233,203,238,209,236,215,228,212,223,205,217,199,212,197,206,197,204,197,198,193,197,190,194,185,190,184,186,181,182,177,177,179,170,178,168,175,166,171,167,167,170,163" href="/PLAVORO/testi/info/infosecondolivello/inforegioni/puglia/info_puglia.asp" alt="Puglia" title="Puglia">
					  <area shape="poly" coords="184,182,188,184,191,188,196,193,200,197,200,203,195,207,192,214,187,217,181,214,178,211,176,204,174,197,172,193,173,187,174,183,178,181" href="/PLAVORO/testi/info/infosecondolivello/inforegioni/basilicata/info_basilicata.asp" alt="Basilicata" title="Basilicata">
					  <area shape="poly" coords="192,215,195,217,198,222,202,228,207,232,208,238,204,242,200,246,197,251,194,258,191,262,188,268,184,270,180,271,176,271,174,267,178,262,178,257,182,253,185,250,187,247,187,243,187,239,187,233,185,225,182,220" href="/PLAVORO/testi/info/infosecondolivello/inforegioni/calabria/info_calabria.asp" alt="Calabria" title="Calabria">
					  <area shape="poly" coords="140,269,151,268,157,267,168,267,165,272,159,277,158,281,159,288,161,294,164,301,156,303,149,302,142,301,139,296,134,295,128,293,124,290,121,285,118,283,114,280,108,278,104,278,102,274,103,268,109,268,115,268,118,269,124,271,127,274,135,273,144,270" href="/PLAVORO/testi/info/infosecondolivello/inforegioni/sicilia/info_sicilia.asp" alt="Sicilia" title="Sicilia">
					</map>

				</td>
			</tr>
        </table></center>
	</td>
  </tr>
  <tr>
    <td width="100%"><center>
        <table border="0" width="95%" cellspacing="0" cellpadding="0">
          <tr>
            <td width="100%">
				<%
					PathFileEdit = "\PLAVORO\Testi\sistdoc\Sistema Redazionale\infosecondolivello\info_scenario.htm"
					on error resume next
					Server.Execute(PathFileEdit)
					If err.number <> 0 Then
							Response.Write "<b class='tbltext'><br>Pagina al momento non disponibile"
							Response.Write "<p>Contattare il Gruppo Assistenza Portale Italia Lavoro<br>"
							Response.Write "all'indirizzo <a href='mailto:po-assistenza@italialavoro.it'>po-assistenza@italialavoro.it</a></p></b>"
					End If
				%>
		  </td></tr>
        </table></center>
	</td>
  </tr>
 </table>
</div>


<!--#include Virtual="/strutt_coda2.asp"-->