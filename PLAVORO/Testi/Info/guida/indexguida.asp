<!--#include Virtual="/strutt_testa2.asp"-->


<table width="505" cellspacing="4" cellpadding="2" border="0"><tr>
<td width="505" valign="top">

<table width="505" border="1" cellspacing="1" cellpadding="0" bordercolor="#FFFFFF">
  <tr bordercolor="#000080" valign="top" align="center"> 
    <td height="16" colspan="3" class="sfondomenu"><b class="tbltext0">PERCORSO GUIDATO A INTERNET</b></td>
  </tr>
  <tr bordercolor="#000080" valign="top" align="center"> 
    <td height="16" width="33%" class="sfondomenu"><a class="tbltext0" href="/PLAVORO/Testi/Info/Guida/indexguida.asp"><b><u>Inicio</u></b></a></td>
    <td height="16" width="34%" class="sfondomenu"><a class="tbltext0" href="/PLAVORO/Testi/Info/Guida/storia.asp"><b><u>Se�as Hist�ricas</u></b></a></td>
    <td height="16" width="33%" class="sfondomenu"><a class="tbltext0" href="/PLAVORO/Testi/Info/Guida/connessione.asp"><b><u>Conectarse a Internet</u></b></a></td>
  </tr>
  <tr bordercolor="#000080" valign="top" align="center"> 
    <td height="16" width="33%" class="sfondomenu"><a class="tbltext0" href="/PLAVORO/Testi/Info/Guida/files.asp"><b><u>Tipi di file</u></b></a></td>
    <td height="16" width="34%" class="sfondomenu"><a class="tbltext0" href="/PLAVORO/Testi/Info/Guida/comunicazione.asp"><b><u>Strumenti per Comunicare</u></b></a></td>
    <td height="16" width="33%" class="sfondomenu"><a class="tbltext0" href="/PLAVORO/Testi/Info/Guida/ricerca.asp"><b><u>Ricerca su Internet</u></b></a></td>
  </tr>
  <tr bordercolor="#000080" valign="top" align="center"> 
    <td height="16" width="33%" class="sfondomenu"><a class="tbltext0" href="/PLAVORO/Testi/Info/Guida/virus.asp"><b><u>Virus Informatici</u></b></a></td>
    <td height="16" width="34%" class="sfondomenu"><a class="tbltext0" href="/PLAVORO/Testi/Info/Guida/faq.asp"><b><u>Domande e Risposte</u></b></a></td>
    <td height="16" width="33%" class="sfondomenu"><a class="tbltext0" href="/PLAVORO/Testi/Info/Guida/glossario.asp"><b><u>Piccolo Glossario</u></b></a></td>
  </tr>
</table>
<br>
<font face="verdana" color="#3399CC"> <b>Percorso guidato a Internet</b></font><br><br>
<img src="/PLAVORO/Images/Guida/internet01.jpg" align="left" hspace="6" vspace="6" WIDTH="61" HEIGHT="58">   
<font size="2" color="black" face="verdana">
L'avvento di Internet ha reso disponibili diversi tipi di risorse (documenti, immagini, filmati) e strumenti per
 navigare o comunicare a una massa veramente sterminata di persone.
<br><br>
Per sfruttare appieno le potenzialit� del mezzo � necessario per� conoscere da vicino quali generi di informazioni
 esso offre e quali sono le modalit� pi� idonee per accedervi.
<br><br>
Questa breve guida, caratterizzata da frequenti rimandi alle pagine della Rete stessa, si propone, appunto, di
 introdurvi al variegato mondo di Internet e a tutto ci� che questo contiene.

<br><br><hr><br>

<font size="2" face="verdana">
<b>Link generali per approfondire i diversi aspetti legati all'utilizzo di Internet</b>:
<br><br>

<font size="3">
<a href="http://www.laterza.it/internet/leggi/internet2000/online/" target="_blank">
www.laterza.it/internet/leggi/internet2000/online</a>
<br>
<font size="2">
Edizione on line del grande manuale della Laterza, che tratta dei diversi aspetti di Internet. Non c'� praticamente
 argomento che la guida non affronti, in maniera molto approfondita, con largo uso di esempi e illustrazioni. Un
 testo che non pu� essere ignorato da chiunque non si accontenti di una conoscenza superficiale del variegato
 mondo della Grande Rete.
<br><br>

<font size="3">
<a href="http://www.liberliber.it/biblioteca/e/electronic_frontier_foundation/guida_a_internet_della_electronic_frontier_foundation_v3_15/html/" target="_blank">
www.liberliber.it/biblioteca/guida_a_internet</a>
<br>
<font size="2">
Guida a Internet della EFF (<i>Electronic Frontier Foundation</i>), una delle pi� famose del mondo, tradotta in diverse
 lingue, tra cui anche l'italiano.
<br><br><hr>

</td></tr></table>


<!--#include Virtual="/strutt_coda2.asp"-->