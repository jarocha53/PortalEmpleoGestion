<!--#include Virtual="/strutt_testa2.asp"-->


<table width="505" cellspacing="4" cellpadding="2" border="0"><tr>
<td width="505" valign="top">

<table width="505" border="1" cellspacing="1" cellpadding="0" bordercolor="#FFFFFF">
  <tr bordercolor="#000080" valign="top" align="center"> 
    <td height="16" colspan="3" class="sfondomenu"><b class="tbltext0">PERCORSO GUIDATO A INTERNET</b></td>
  </tr>
  <tr bordercolor="#000080" valign="top" align="center"> 
    <td height="16" width="33%" class="sfondomenu"><a class="tbltext0" href="/PLAVORO/Testi/Info/Guida/indexguida.asp"><b><u>Inizio</u></b></a></td>
    <td height="16" width="34%" class="sfondomenu"><a class="tbltext0" href="/PLAVORO/Testi/Info/Guida/storia.asp"><b><u>Cenni Storici</u></b></a></td>
    <td height="16" width="33%" class="sfondomenu"><a class="tbltext0" href="/PLAVORO/Testi/Info/Guida/connessione.asp"><b><u>Connettersi a Internet</u></b></a></td>
  </tr>
  <tr bordercolor="#000080" valign="top" align="center"> 
    <td height="16" width="33%" class="sfondomenu"><a class="tbltext0" href="/PLAVORO/Testi/Info/Guida/files.asp"><b><u>Tipi di file</u></b></a></td>
    <td height="16" width="34%" class="sfondomenu"><a class="tbltext0" href="/PLAVORO/Testi/Info/Guida/comunicazione.asp"><b><u>Strumenti per Comunicare</u></b></a></td>
    <td height="16" width="33%" class="sfondomenu"><a class="tbltext0" href="/PLAVORO/Testi/Info/Guida/ricerca.asp"><b><u>Ricerca su Internet</u></b></a></td>
  </tr>
  <tr bordercolor="#000080" valign="top" align="center"> 
    <td height="16" width="33%" class="sfondomenu"><a class="tbltext0" href="/PLAVORO/Testi/Info/Guida/virus.asp"><b><u>Virus Informatici</u></b></a></td>
    <td height="16" width="34%" class="sfondomenu"><a class="tbltext0" href="/PLAVORO/Testi/Info/Guida/faq.asp"><b><u>Domande e Risposte</u></b></a></td>
    <td height="16" width="33%" class="sfondomenu"><a class="tbltext0" href="/PLAVORO/Testi/Info/Guida/glossario.asp"><b><u>Piccolo Glossario</u></b></a></td>
  </tr>
</table>
<br>
<font face="verdana" color="#3399CC"> <b>Glossario</b></font><br><br>
<font size="3" face="verdana" color="black">
<b>BROWSER</b><br>
<font size="2">
Programma che permette di visualizzare documenti Internet (testi, immagini e filmati). I browser pi� conosciuti
 attualmente sono <i>Microsoft Explorer</i> e <i>Netscape Navigator</i>.
<br><br>

<font size="3">
<b>CHAT</b><br>
<font size="2">
Luoghi d'incontro virtuali tra utenti, che possono comunicare tra loro in tempo reale utilizzando messaggi di testo.
<br><a href="/PLAVORO/Testi/Info/Guida/comunicazione.asp#chat"><b>Approfondimento</b></a>
<br><br>

<font size="3">
<b>DOMINIO</b><br>
<font size="2">
Insieme di caratteri alfanumerici utilizzati per identificare in modo univoco un sito o un computer su Internet.
 Di solito, i domini sono formati da pi� parti: il prefisso (<b>www.</b>), la denominazione vera e propria
 (es., <b><i>in-formati</i></b>) e un'estensione finale (<b>.com</b>, <b>.it</b>, <b>.net</b>, <b>.org</b>, ecc.).
In base a questo standard, un dominio pu� essere suddiviso in tre sezioni:
<br><b>I livello</b>, corrispondente all'estenzione finale
<br><b>II livello</b>, corrispondente a quella parte collocata prima del dominio di I livello, che in genere
 indica il nome vero e proprio del dominio.
<br><b>III livello</b>, la parte che precede il dominio di II livello, quindi <b><i>www.</i></b> e tutto ci� che si pu�
 mettere al suo posto.
<br><br>

<font size="3">
<b>DOWNLOAD</b><br>
<font size="2">
Termine inglese che indica l'operazione di scaricare, cio� di copiare sul proprio computer file residenti su un
 server remoto.
<br><br>

<font size="3">
<b>FAQ</b><br>
<font size="2">
Acronimo di <i>Frequently Asked Questions</i>: elenco delle domande poste con maggior frequenza con le relative
 risposte, riguardanti un determinato argomento. Si utilizzano per evitare che in un newsgroup si debba rispondere
 sempre alle stesse domande.
<br><br>

<font size="3">
<b>FORUM</b><br>
<font size="2">
Spazio virtuale d'incontro per gli utenti in rete, dove � possibile uno scambio di opinioni e commenti su un
 tema specifico.
<br><a href="/PLAVORO/Testi/Info/Guida/comunicazione.asp#forum"><b>Approfondimento</b></a>
<br><br>

<font size="3">
<b>GIF</b><br>
<font size="2">
Acronimo di <i>Graphic Interchange Format</i>, uno standard in grado di compattare un file di immagini per ridurne
 i tempi di trasmissione.
<br><br>

<font size="3">
<b>HTML</b><br>
<font size="2">
Sigla che sta per <i>HyperText Markup Language</i>, un linguaggio utilizzato per realizzare ipertesti da inserire
 in rete.
<br><br>

<font size="3">
<b>LINK</b><br>
<font size="2">
Collegamento, generalmente associato a un'immagine o a un testo, cliccando sul quale si viene condotti a
 un'altra pagina o file.
<br><br>

<font size="3">
<b>MAILING LIST</b><br>
<font size="2">
Sistema di comunicazione utilizzato in Internet per inviare una stessa email a pi� indirizzi di posta elettronica.
<br><a href="/PLAVORO/Testi/Info/Guida/comunicazione.asp#mailinglist"><b>Approfondimento</b></a>
<br><br>

<font size="3">
<b>MIDI</b><br>
<font size="2">
Standard di comunicazione ideato per scambiare informazioni e segnali di controllo tra programmi musicali per computer,
sintetizzatori e altri dispositivi elettronici. Permette di memorizzare file musicali in spazi di memoria molto
ridotti.
<br><br>

<font size="3">
<b>MODEM</b><br>
<font size="2">
Strumento che si usa per connettere un computer alla rete.
<br><br>

<font size="3">
<b>MOTORE DI RICERCA</b><br>
<font size="2">
Tipo particolare di sito che contiene enormi database di link. Ha la funzione di facilitare la ricerca di
 informazioni su Internet.
<br><a href="/PLAVORO/Testi/Info/Guida/ricerca.asp#motori"><b>Approfondimento</b></a>
<br><br>

<font size="3">
<b>MP3</b><br>
<font size="2">
E' lo standard attualmente pi� usato per lo scambio di files musicali in Internet. Il suo maggior pregio consiste
nel permettere una buona fedelt� di suono occupando uno spazio 8-10 volte minore rispetto ai normali CD audio.
<br><br>

<font size="3">
<b>NEWSGROUP</b><br>
<font size="2">
Gruppo di discussione in rete a cui si pu� accedere per leggere i messaggi sulla bacheca virtuale o pubblicare
 i propri.
<br><a href="/PLAVORO/Testi/Info/Guida/comunicazione.asp#newsgroup"><b>Approfondimento</b></a>
<br><br>

<font size="3">
<b>OPERATORI LOGICI</b><br>
<font size="2">
Istruzioni utilizzate nella ricerca avanzata su Internet (<b>AND</b>, <b>OR</b>, <b>NOT</b>).
<br><a href="/PLAVORO/Testi/Info/Guida/ricerca.asp#operatori"><b>Approfondimento</b></a>
<br><br>

<font size="3">
<b>POSTA ELETTRONICA</b><br>
<font size="2">
Sistema che permette di comunicare via Internet con altre persone tramite invio di messaggi di testo o altri tipi
 di file.
<br><a href="/PLAVORO/Testi/Info/Guida/comunicazione.asp#email"><b>Approfondimento</b></a>
<br><br>

<font size="3">
<b>SMS</b><br>
<font size="2">
Acronimo di <i>Short Message Service</i>, sistema di larghissima diffusione, utilizzato per spedire brevi messaggi
 per mezzo di un cellulare.
<br><br>

<font size="3">
<b>URL</b><br>
<font size="2">
Abbreviazione di <i>Uniform Resource Locator</i>, ovvero l'indirizzo di un sito espresso nella sua forma pi� comune.
<br><br>

<font size="3">
<b>VIRUS</b><br>
<font size="2">
Programma capace di infettare altri programmi installati in un computer, danneggiandoli in maniera pi� o meno grave.
 In genere i virsu si attivano quando il programma dove risiedono viene eseguito, ma possono anche rimanere a lungo
 inattivi in un computer senza rivelare la propria presenza.
<br><a href="/PLAVORO/Testi/Info/Guida/virus.asp"><b>Approfondimento</b></a>
<br><br>

<font size="3">
<b>WEBCAM</b><br>
<font size="2">
Telecamera di ridotte dimensioni utilizzata in genere per collegarsi a un computer e riprendere foto o filmati,
 salvandoli sotto forma di file.
<br><br>

<font size="3">
<b>WEBMASTER</b><br>
<font size="2">
Persona che si occupa del mantenimento di un sito web sia per quanto riguarda l'aspetto tecnico che rispetto
 all'organizzazione e l'aggiornamento.

<br><br><hr>
</td>
</tr>
</table>


<!--#include Virtual="/strutt_coda2.asp"-->


