<!--#include Virtual="/strutt_testa2.asp"-->


<table width="505" cellspacing="4" cellpadding="2" border="0"><tr>
<td width="505" valign="top">

<table width="505" border="1" cellspacing="1" cellpadding="0" bordercolor="#FFFFFF">
  <tr bordercolor="#000080" valign="top" align="center"> 
    <td height="16" colspan="3" class="sfondomenu"><b class="tbltext0">PERCORSO GUIDATO A INTERNET</b></td>
  </tr>
  <tr bordercolor="#000080" valign="top" align="center"> 
    <td height="16" width="33%" class="sfondomenu"><a class="tbltext0" href="/PLAVORO/Testi/Info/Guida/indexguida.asp"><b><u>Inizio</u></b></a></td>
    <td height="16" width="34%" class="sfondomenu"><a class="tbltext0" href="/PLAVORO/Testi/Info/Guida/storia.asp"><b><u>Cenni Storici</u></b></a></td>
    <td height="16" width="33%" class="sfondomenu"><a class="tbltext0" href="/PLAVORO/Testi/Info/Guida/connessione.asp"><b><u>Connettersi a Internet</u></b></a></td>
  </tr>
  <tr bordercolor="#000080" valign="top" align="center"> 
    <td height="16" width="33%" class="sfondomenu"><a class="tbltext0" href="/PLAVORO/Testi/Info/Guida/files.asp"><b><u>Tipi di file</u></b></a></td>
    <td height="16" width="34%" class="sfondomenu"><a class="tbltext0" href="/PLAVORO/Testi/Info/Guida/comunicazione.asp"><b><u>Strumenti per Comunicare</u></b></a></td>
    <td height="16" width="33%" class="sfondomenu"><a class="tbltext0" href="/PLAVORO/Testi/Info/Guida/ricerca.asp"><b><u>Ricerca su Internet</u></b></a></td>
  </tr>
  <tr bordercolor="#000080" valign="top" align="center"> 
    <td height="16" width="33%" class="sfondomenu"><a class="tbltext0" href="/PLAVORO/Testi/Info/Guida/virus.asp"><b><u>Virus Informatici</u></b></a></td>
    <td height="16" width="34%" class="sfondomenu"><a class="tbltext0" href="/PLAVORO/Testi/Info/Guida/faq.asp"><b><u>Domande e Risposte</u></b></a></td>
    <td height="16" width="33%" class="sfondomenu"><a class="tbltext0" href="/PLAVORO/Testi/Info/Guida/glossario.asp"><b><u>Piccolo Glossario</u></b></a></td>
  </tr>
</table>
<br>
<font face="verdana" color="#3399CC"> <b>Strumenti per Comunicare</b></font><br><br>
<img src="/PLAVORO/Images/Guida/email01.jpg" align="left" hspace="6" vspace="6" WIDTH="60" HEIGHT="60"> <a name="email"></a> 
      <font face="times new roman" size="5"> <b><font face="Verdana, Arial, Helvetica, sans-serif" size="3">Posta 
      elettronica (e-mail) </font></b><font face="Verdana, Arial, Helvetica, sans-serif" size="2"><br>
      Permette di inviare o ricevere messaggi a e da altre persone collegate in 
      Internet. Si tratta di un sistema molto rapido e ormai diffusissimo. Ai 
      messaggi possono essere &quot;attaccati&quot; (tramite il comando <i>allega</i>) file 
      contenenti grafici, foto e persino musiche e brevi filmati. Se non si utilizzano 
      caselle di posta messe a disposizione gratuitamente dai diversi siti e portali 
      � necessario un apposito programma per gestire tutte le operazioni (<i>Eudora</i>, 
      <i>Outlook Express</i>). <br>
      Per approfondimenti, vedi: <br>
      <br>
      <a href="http://www.laterza.it/internet/leggi/internet2000/online/testo/07_testo.htm" target="_blank"> 
      <font size="3">www.laterza.it/internet/leggi/internet2000/online/testo</font></a> 
      <br>
      Sezione del manuale on line Laterza, dedicata alla posta elettronica, con 
      molti esempi sull'utilizzo di questo strumento, corredati da illustrazioni. 
      <br>
      <br>
      <a href="http://www.harrrdito.it/guide/oe/" target="_blank"><font size="3">www.harrrdito.it/guide/oe</font></a> 
      <br>
      Guida on line all'utilizzo di <b><i>Outlook Express</i></b>, programma per 
      la gestione della posta elettronica della Microsoft. Argomenti principali: 
      come creare un account (impostazione nome, settaggio protocolli invio e 
      ricezione posta, password), smistare messaggi in cartelle, newsgroup. <br>
      <br>
      <a href="http://www.net4u.it/eudora/contents.html" target="_blank"><font size="3">www.net4u.it/eudora/contents</font></a> 
      <br>
      Guida on line all'uso del programma di posta elettronica <b><i>Eudora</i></b>. 
      <br>
      <br>
      <br>
      </font><font size="3"><font face="verdana" size="3"><font size="2"><font size="3"><font size="2"><font size="3"><font size="2">
      <hr>
      <font face="Verdana, Arial, Helvetica, sans-serif" size="2"><br>
      <img src="/PLAVORO/Images/Guida/chat03.jpg" align="left" hspace="6" vspace="6" WIDTH="61" HEIGHT="59"> <a name="chat"></a> 
      <b><font size="3">Chat</font></b> <br>
      Luoghi virtuali, costruiti dal server, che permettono a pi� utenti, collegati 
      su Internet, di comunicare tra loro in tempo reale (chattare) tramite messaggi 
      di testo. Tutto ci� che viene digitato sulla tastiera da un utente compare 
      sul video degli altri utenti connessi in quel momento, i quali possono intervenire 
      direttamente nella conversazione (sempre tramite tastiera). <br>
      <br>
      </font><font face="times new roman"><font size="5"><font size="3">
      <table width="100%">
        <tr> 
          <td width="25%" valign="middle"> <font face="Verdana, Arial, Helvetica, sans-serif" size="2"> 
            Pagina della <i>chat</i> del <b><i>Progetto IN</i></b> </font></td>
          <td width="75%"> 
            <center>
              <font face="Verdana, Arial, Helvetica, sans-serif" size="2"><img src="/PLAVORO/Images/Guida/Chat.jpg" WIDTH="350" HEIGHT="282"> 
              </font> 
            </center>
          </td>
        </tr>
      </table>
      <font face="Verdana, Arial, Helvetica, sans-serif" size="2"><br>
      <br>
      <b>Links di approfondimento</b>: <br>
      <br>
      <a href="http://www.mondochat.it/" target="_blank"><font size="3">Mondochat.it</font></a> 
      <br>
      Sito dedicato alle chat. Al suo interno si possono trovare molte rubriche 
      che spiegano come utilizzare al meglio questo strumento. Notizie e informazioni 
      utili sul mondo delle chat. <br>
      <br>
      <a href="http://digiland.iol.it/serveaiuto/faq/chat.shtml#r2" target="_blank"> 
      <font size="3">digiland.iol.it/serveaiuto/faq/chat.shtml#r2</font></a> <br>
      Domande e risposte pi� comuni (FAQ) che riguardano l'argomento <i>chat</i>, 
      messe a disposizione da Digiland. <br>
      <br>
      <br>
      </font><font face="times new roman" size="3"><font face="verdana" size="3"><font size="2"><font face="verdana" size="3"><font size="2">
      <hr>
      <font face="Verdana, Arial, Helvetica, sans-serif" size="2"><br>
      <img src="/PLAVORO/Images/Guida/newsgroup01.jpg" align="left" hspace="6" vspace="6" WIDTH="62" HEIGHT="40"> <a name="newsgroup"></a> 
      <font size="3"><b>Newsgroup</b></font> <br>
      Luoghi d'incontro virtuali organizzati in <i>gerarchie</i> che si occupano 
      di certi temi, in <i>sottogerarchie</i> che suddividono ulteriormente gli 
      argomenti trattati. <br>
      Quando si entra in un newsgroup, viene visualizzato l'elenco degli argomenti 
      gi� presenti e dei relativi messaggi. Si pu� rispondere ai messaggi esistenti 
      oppure proporre un nuovo argomento. <br>
      Ogni newsgroup ha di solito un moderatore, ossia una persona che controlla 
      i messaggi in arrivo, cancellando quelli che non sono pertinenti all'argomento 
      del forum o che usano un linguaggio sconveniente od offensivo. Per accedervi 
      c'� generalmente bisogno di un apposito programma, detto <i>newsreader</i> 
      (uno dei pi� diffusi � il <i>Free Agent</i>, che pu� essere scaricato dal 
      sito <a href="http://www.forteinc.com" target="_blank"><font size="3">www.forteinc.com</font></a> 
      ). <br>
      <br>
      <b>Links di approfondimento</b>: <br>
      <br>
      <a href="http://newsgroup.virgilio.it/newsgroup/supporto/help/conoscere.html" target="_blank"> 
      <font size="3">newsgroup.virgilio.it/newsgroup/supporto/help/conoscere</font></a> 
      <br>
      Pagina di <b><i>Virgilio</i></b> che spiega cosa sono i <i>newsgroup</i>. 
      <br>
      <br>
      <a href="http://www.punto.it/newsgroup.php" target="_blank"><font size="3">www.punto.it/newsgroup.php</font></a> 
      <br>
      I newsgroup italiani distinti per categorie o presentati in ordine alfabetico. 
      <br>
      <br>
      <a href="http://www.cilea.it/news-it/" target="_blank"><font size="3">www.cilea.it/news-it</font></a> 
      <br>
      Informazioni utili sul mondo dei <i>newsgroup</i> italiani. <br>
      <br>
      <br>
      </font><font face="times new roman"><font size="5"><font size="3"><font size="4"><font size="3"><font face="times new roman" size="3"><font face="verdana" size="3"><font size="2"><font size="3"><font size="2"><font size="3"><font size="2">
      <hr>
      <font face="Verdana, Arial, Helvetica, sans-serif" size="2"><br>
      <img src="/PLAVORO/Images/Guida/forum01.jpg" align="left" hspace="6" vspace="6" WIDTH="60" HEIGHT="60"> <a name="forum"></a> 
      <b><font size="3">Forum</font></b> <br>
      Luoghi d'incontro on line in cui un certo numero di persone possono ritrovarsi 
      per discutere di argomenti di interesse comune. Dal punto di vista pratico, 
      il forum non � altro che una pagina web (una sorta di bacheca elettronica) 
      dove si pu� inviare un messaggio che gli altri utenti leggeranno e a cui 
      potranno eventualmente rispondere. <br>
      Per partecipare a un forum non � necessario essere presenti contemporaneamente 
      ad altri utenti, come avviene, invece, nelle chat. <br>
      <br>
      </font><font face="times new roman"><font size="5"><font size="3">
      <table width="100%">
        <tr> 
          <td width="20%" valign="middle"> <font face="Verdana, Arial, Helvetica, sans-serif" size="2"> 
            Pagina dei <i>forum</i> del <b><i>Progetto IN</i></b> </font></td>
          <td width="80%"> 
            <center>
              <font face="Verdana, Arial, Helvetica, sans-serif" size="2"><img src="/PLAVORO/Images/Guida/ForumIN.jpg" WIDTH="400" HEIGHT="243"> 
              </font> 
            </center>
          </td>
        </tr>
      </table>
      <font face="Verdana, Arial, Helvetica, sans-serif" size="2"><br>
      <br>
      <b>Links di approfondimento</b>: <br>
      <br>
      <a href="http://www.html.it/dossier/17_forum/forum_02.htm" target="_blank"><font size="3">www.html.it/dossier/17_forum/forum_02</font></a> 
      <br>
      Pagina del portale <i><b>HTML.it</b></i> che spiega che cosa sono i <i>forum</i>. 
      <br>
      <br>
      <a href="http://www.freeforumzone.com/faq.asp" target="_blank"><font size="3">www.freeforumzone.com/faq.asp</font></a> 
      <br>
      Sezione di <i><b>Free Forum Zone</b></i>, comunit� virtuale di <i>forum</i>, 
      nella quale possono essere consultate le pi� comuni domande sull'argomento, 
      corredate dalle relative risposte (FAQ). <br>
      <br>
      <br>
      </font><font face="verdana" size="3"><font size="2"><font face="verdana" size="3"><font size="2">
      <hr>
      <font face="Verdana, Arial, Helvetica, sans-serif" size="2"><br>
      <img src="/PLAVORO/Images/Guida/mailinglist01.jpg" align="left" hspace="6" vspace="6" WIDTH="61" HEIGHT="58"> <a name="mailinglist"></a> 
      <b><font size="3">Mailing list</font></b> <br>
      Sistema che permette di ricevere, tramite posta elettronica, messaggi che 
      riguardano un dato argomento. Una <i>mailing list</i> pu� essere: <br>
      - <i>unidirezionale</i> (chiamata anche <i>newsletter</i>), quando si ricevono 
      periodicamente messaggi, ma non si pu� inviare messaggi agli altri utenti 
      iscritti; <br>
      - <i>bidirezionale</i>, se invece ogni utente pu� mandare messaggi che vengono 
      recapitati a tutti gli altri utenti. <br>
      <br>
      <b>Link di approfondimento</b>: <br>
      <br>
      <a href="http://www.html.it/articoli/12.htm" target="_blank"><font size="3">www.html.it/articoli/12</font></a> 
      <br>
      Pagina del portale <b><i>HTML.it</i></b> che spiega cosa sono le <i>mailing 
      list</i>. <br>
      <br>
      <br>
      </font><font face="times new roman"><font size="5"><font size="3"><font face="verdana" size="3"><font size="2">
      <hr>
      <font face="Verdana, Arial, Helvetica, sans-serif" size="2"><br>
      <b>Links di approfondimento generale</b>: <br>
      <br>
      <a href="http://www2.comune.carpi.mo.it/download/corso_net/cb_pag16.htm" target="_blank"> 
      <font size="3">www2.comune.carpi.mo.it/download/corso_net/cb_pag16</font></a> 
      <br>
      Pagina del sito della Rete Civica della citt� di Carpi, che tratta abbastanza 
      diffusamente delle <i>mailing list</i> e dei <i>newsgroup</i>, fornendo 
      anche esempi e consigli sul modo di utilizzare questi servizi. <br>
      <br>
      <a href="http://trovanotizie.iltrovatore.it/OGGISULWEB/01/12/21/126237.shtml" target="_blank"> 
      <font size="3">trovanotizie.iltrovatore.it/OGGISULWEB/01/12/21/126237</font></a> 
      <br>
      Comunit� on line che offre <i>chat</i>, <i>forum</i> e <i>newsgroup</i> 
      su argomenti diversi. <br>
      <br>
      <font color="red"> <b>Vuoi verificare le tue conoscenze sull'uso della <i>posta 
      elettronica</i>??</b> <br>
      <font color="black"> <b>clicca sul link</b>: <br>
      <br>
      <a href="http://www.lavoroinrete.it/Community/Quiz/quiz.asp?quizid=9" target="_blank"> 
      <font size="3">www.lavoroinrete.it/Community/Quiz/quiz.asp?quizid=9</font></a> 
      <br>
      Pagina del portale <b><i>Lavoroinrete.it</i></b>, che propone un test in 
      dieci domande. Nello stesso portale possono essere trovati altri test interessanti. 
      </font></font></font><font face="verdana, tahoma" size="2"><font size="3"><font size="2"><font size="3"><font size="2"><font face="times new roman" size="4" color="red"><font face="verdana, tahoma" size="2" color="black"><font size="3"><font size="2"><br>
      <br>
      <hr>
      </font>
</td>
</tr>
</table>


<!--#include Virtual="/strutt_coda2.asp"-->

