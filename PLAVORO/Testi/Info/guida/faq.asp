<!--#include Virtual="/strutt_testa2.asp"-->


<table width="505" cellspacing="4" cellpadding="2" border="0"><tr>
<td width="505" valign="top">

<table width="505" border="1" cellspacing="1" cellpadding="0" bordercolor="#FFFFFF">
  <tr bordercolor="#000080" valign="top" align="center"> 
    <td height="16" colspan="3" class="sfondomenu"><b class="tbltext0">PERCORSO GUIDATO A INTERNET</b></td>
  </tr>
  <tr bordercolor="#000080" valign="top" align="center"> 
    <td height="16" width="33%" class="sfondomenu"><a class="tbltext0" href="/PLAVORO/Testi/Info/Guida/indexguida.asp"><b><u>Inizio</u></b></a></td>
    <td height="16" width="34%" class="sfondomenu"><a class="tbltext0" href="/PLAVORO/Testi/Info/Guida/storia.asp"><b><u>Cenni Storici</u></b></a></td>
    <td height="16" width="33%" class="sfondomenu"><a class="tbltext0" href="/PLAVORO/Testi/Info/Guida/connessione.asp"><b><u>Connettersi a Internet</u></b></a></td>
  </tr>
  <tr bordercolor="#000080" valign="top" align="center"> 
    <td height="16" width="33%" class="sfondomenu"><a class="tbltext0" href="/PLAVORO/Testi/Info/Guida/files.asp"><b><u>Tipi di file</u></b></a></td>
    <td height="16" width="34%" class="sfondomenu"><a class="tbltext0" href="/PLAVORO/Testi/Info/Guida/comunicazione.asp"><b><u>Strumenti per Comunicare</u></b></a></td>
    <td height="16" width="33%" class="sfondomenu"><a class="tbltext0" href="/PLAVORO/Testi/Info/Guida/ricerca.asp"><b><u>Ricerca su Internet</u></b></a></td>
  </tr>
  <tr bordercolor="#000080" valign="top" align="center"> 
    <td height="16" width="33%" class="sfondomenu"><a class="tbltext0" href="/PLAVORO/Testi/Info/Guida/virus.asp"><b><u>Virus Informatici</u></b></a></td>
    <td height="16" width="34%" class="sfondomenu"><a class="tbltext0" href="/PLAVORO/Testi/Info/Guida/faq.asp"><b><u>Domande e Risposte</u></b></a></td>
    <td height="16" width="33%" class="sfondomenu"><a class="tbltext0" href="/PLAVORO/Testi/Info/Guida/glossario.asp"><b><u>Piccolo Glossario</u></b></a></td>
  </tr>
</table>
<br>
<font face="verdana" color="#3399CC"> <b>Domande e Risposte</b></font><br><br>
<font face="verdana" size="3" color="black">
<b>Qual � la differenza tra un <i>browser</i> e un <i>modem</i></i>?</b>
<br><br><font size="2">
Il <b><i>browser</i></b> � un programma che permette di navigare su Internet, cio� di visualizzare i diversi files
 che vi si trovano; il <b><i>modem</i></b>, invece, � un dispositivo (hardware), che rende possibile il collegamento
 tra il computer e la rete telefonica.
<br><br>

<font face="verdana" size="3" color="black">
<b>Cosa sono i <i>motori di ricerca</i>?</b>
<br><br><font size="2">
Sono siti particolari, utilizzabili gratuitamente, che permettono di cercare informazioni o file di vario tipo
 su Internet. Le modalit� di ricerca sono due: per <i><b>parole chiave</b></i>, quando si vogliono trovare siti
 su un tema ben definito; <b><i>per argomento</i></b> (o <i><b>categorie</b></i>), se si vuole approfondire,
 passando via via ad argomenti sempre pi� specifici.
<br><br>

<font face="verdana" size="3" color="black">
<b>Dovendo cercare sul web tutti i siti che parlano di cinema o di televisione, nella ricerca avanzata bisogna
 utilizzare come operatore <i>AND</i> od <i>OR</i>?</b>
<br><br><font size="2">
Nel riquadro per la ricerca si deve scrivere <i><b>cinema OR televisione</b></i>, poich� inserendo <i>AND</i> si
 otterrebbero solo quei siti che trattano di entrambi gli argomenti (e non quelli dedicati soltanto a uno di essi).
<br><br>

<font face="verdana" size="3" color="black">
<b>Qual � la differenza fondamentale tra <i>chat</i> e <i>forum</i>?</b>
<br><br><font size="2">
Le <b><i>chat</i></b> si svolgono in tempo reale, e quindi hanno bisogno della presenza contemporanea di pi� utenti.
 I <b><i>forum</i></b>, consistendo sostanzialmente in bacheche dove si scrivono messaggi, possono funzionare anche
 se gli utenti si collegano in tempi diversi al sito che li ospita.
<br><br>

<font face="verdana" size="3" color="black">
<b>E' possibile inviare filmati attraverso la posta elettronica?</b>
<br><br><font size="2">
Tramite email si pu� inviare qualsiasi tipo di file, anche se bisogna sempre tener presente che file
 eccessivamente grandi (dell'ordine dei megabyte) possono richiedere molto tempo per la trasmissione o
 addirittura saturare la casella di posta.
<br><br>

<font face="verdana" size="3" color="black">
<b>Per fissare in un'immagine web la foto di un tramonto � preferibile usare il formato <i>Gif</i> o <i>Jpeg</i>?</b>
<br><br><font size="2">
Il formato <b><i>Gif</i></b> supporta soltanto 256 colori e quindi � adatto per le immagini con poche sfumature,
 perch� occupa meno spazio di memoria. Negli altri casi, � preferibile utilizzare il formato <b><i>Jpeg</i></b>,
 anche se i suoi file hanno maggiore estensione
<br><br>

<font face="verdana" size="3" color="black">
<b>Perch� spesso i files vengono <i>compressi</i>?</b>
<br><br><font size="2">
Per ridurre lo spazio di memoria da essi occupato. In tal modo il tempo di caricamento � reso pi� veloce.
<br><br>

<font face="verdana" size="3" color="black">
<b>Cos'� lo <i>streaming</i>?</b>
<br><br><font size="2">
Si tratta di una tecnologia relativamente recente che permette di vedere (o ascoltare) file video (o audio) senza
 attendere che il file stesso sia completamente scaricato.
<br><br>

<font face="verdana" size="3" color="black">
<b>I <i>virus</i> possono trasmettersi attraverso file di immagini?</b>
<br><br><font size="2">
S�. I virus possono attaccarsi a qualsiasi tipo di file, diffondendori successivamente in altre aree della memoria
 del computer.


<br><br><hr>
</td>
</tr>
</table>


<!--#include Virtual="/strutt_coda2.asp"-->




