<!--#include Virtual="/strutt_testa2.asp"-->


<table width="505" cellspacing="4" cellpadding="2" border="0"><tr>
<td width="505" valign="top">

<table width="505" border="1" cellspacing="1" cellpadding="0" bordercolor="#FFFFFF">
  <tr bordercolor="#000080" valign="top" align="center"> 
    <td height="16" colspan="3" class="sfondomenu"><b class="tbltext0">PERCORSO GUIDATO A INTERNET</b></td>
  </tr>
  <tr bordercolor="#000080" valign="top" align="center"> 
    <td height="16" width="33%" class="sfondomenu"><a class="tbltext0" href="/PLAVORO/Testi/Info/Guida/indexguida.asp"><b><u>Inizio</u></b></a></td>
    <td height="16" width="34%" class="sfondomenu"><a class="tbltext0" href="/PLAVORO/Testi/Info/Guida/storia.asp"><b><u>Cenni Storici</u></b></a></td>
    <td height="16" width="33%" class="sfondomenu"><a class="tbltext0" href="/PLAVORO/Testi/Info/Guida/connessione.asp"><b><u>Connettersi a Internet</u></b></a></td>
  </tr>
  <tr bordercolor="#000080" valign="top" align="center"> 
    <td height="16" width="33%" class="sfondomenu"><a class="tbltext0" href="/PLAVORO/Testi/Info/Guida/files.asp"><b><u>Tipi di file</u></b></a></td>
    <td height="16" width="34%" class="sfondomenu"><a class="tbltext0" href="/PLAVORO/Testi/Info/Guida/comunicazione.asp"><b><u>Strumenti per Comunicare</u></b></a></td>
    <td height="16" width="33%" class="sfondomenu"><a class="tbltext0" href="/PLAVORO/Testi/Info/Guida/ricerca.asp"><b><u>Ricerca su Internet</u></b></a></td>
  </tr>
  <tr bordercolor="#000080" valign="top" align="center"> 
    <td height="16" width="33%" class="sfondomenu"><a class="tbltext0" href="/PLAVORO/Testi/Info/Guida/virus.asp"><b><u>Virus Informatici</u></b></a></td>
    <td height="16" width="34%" class="sfondomenu"><a class="tbltext0" href="/PLAVORO/Testi/Info/Guida/faq.asp"><b><u>Domande e Risposte</u></b></a></td>
    <td height="16" width="33%" class="sfondomenu"><a class="tbltext0" href="/PLAVORO/Testi/Info/Guida/glossario.asp"><b><u>Piccolo Glossario</u></b></a></td>
  </tr>
</table>
<br>
<font face="verdana" color="#3399CC"> <b>Cenni Storici</b></font><br><br>
<img src="/PLAVORO/Images/Guida/arpanet04.jpg" align="left" hspace="6" vspace="6" WIDTH="60" HEIGHT="60">  
<font size="2" face="verdana" color="black">
Di solito, si fa risalire l'origine di Internet al 1969, quando il Dipartimento della Difesa americano costru� un
sistema capace di garantire le comunicazioni tra le diverse basi militari del territorio nazionale. L'idea portante
 di questa struttura era che essa dovesse essere in grado di funzionare anche nel caso che alcune parti fossero state
 distrutte da un attacco nemico. Venivano infatti utilizzati computer collegati in rete tra loro, in modo che nessuno
 di loro avesse un ruolo centrale della trasmissione delle informazioni.
<br>
Lo standard di comunicazione utilizzato si dimostr� talmente efficiente che fu presto adottato anche per applicazioni
 non militari, diffondendosi gradualmente in tutto il mondo.
<br><br>
Nel 1992 il CERN di Ginevra realizz� il <i>World Wide Web</i>, un sistema capace di semplificare notevolmente la navigazione. Si
 trattava di un sistema multimediale basato sul concetto di <i>ipertesto</i>, cio� sulla possibilit� di effettuare una
 lettura non sequenziale, saltando da un punto all'altro utilizzando dei rimandi (<i>link</i>).
<br><br>
Nel 1993 nasce <i>Mosaic</i>, il primo browser con caratteristiche simili a quelli in uso oggi, che rivoluzion� il
 modo di effettuare le ricerche e di comunicare in rete. Ad esso segu� (1994) la creazione di <i>Netscape Navigator</i>.
<br><br><hr><br>

<font face="verdana, tahoma" size="2">
<b>Per avere maggiori notizie sull'origine e lo sviluppo di Internet, possono essere utili i seguenti link</b>:
<br><br>

<font size="3">
<a href="http://www.tin.it/internet/storia/index.html" target="_blank">www.tin.it/internet/storia/index.html</a><br>
<font size="2">
Breve prospettiva storica sulla nascita di Internet.
<br><br>

<font size="3">
<a href="http://www.inf.uniroma3.it/~necci/storia_internet.htm" target="_blank">www.inf.uniroma3.it/~necci/storia_internet.htm</a><br>
<font size="2">
Storia di Internet del Dipartimento di Informatica e Automazione dell'Universit� di Roma 3.
<br><br>

<font size="3">
<a href="http://www.arcanet.it/storia.html" target="_blank">www.arcanet.it/storia.html</a><br>
<font size="2">
Altro breve percorso che tratta della storia di Internet.
<br><br>

<font size="3">
<a href="http://www.laterza.it/internet/leggi/internet2000/online/testo/27_testo.htm" target="_blank">
www.laterza.it/internet/leggi/internet2000/online/testo</a><br>
<font size="2">
Pagina del manuale on line della Laterza in cui si trova una estesa trattazione della storia di Internet, dalle origini
 ai nostri giorni.
<br><br><hr>
</td>
</tr>
</table>


<!--#include Virtual="/strutt_coda2.asp"-->
