<!--#include Virtual="/strutt_testa2.asp"-->


<table width="505" cellspacing="4" cellpadding="2" border="0"><tr>
<td width="505" valign="top">

<table width="505" border="1" cellspacing="1" cellpadding="0" bordercolor="#FFFFFF">
  <tr bordercolor="#000080" valign="top" align="center"> 
    <td height="16" colspan="3" class="sfondomenu"><b class="tbltext0">PERCORSO GUIDATO A INTERNET</b></td>
  </tr>
  <tr bordercolor="#000080" valign="top" align="center"> 
    <td height="16" width="33%" class="sfondomenu"><a class="tbltext0" href="/PLAVORO/Testi/Info/Guida/indexguida.asp"><b><u>Inizio</u></b></a></td>
    <td height="16" width="34%" class="sfondomenu"><a class="tbltext0" href="/PLAVORO/Testi/Info/Guida/storia.asp"><b><u>Cenni Storici</u></b></a></td>
    <td height="16" width="33%" class="sfondomenu"><a class="tbltext0" href="/PLAVORO/Testi/Info/Guida/connessione.asp"><b><u>Connettersi a Internet</u></b></a></td>
  </tr>
  <tr bordercolor="#000080" valign="top" align="center"> 
    <td height="16" width="33%" class="sfondomenu"><a class="tbltext0" href="/PLAVORO/Testi/Info/Guida/files.asp"><b><u>Tipi di file</u></b></a></td>
    <td height="16" width="34%" class="sfondomenu"><a class="tbltext0" href="/PLAVORO/Testi/Info/Guida/comunicazione.asp"><b><u>Strumenti per Comunicare</u></b></a></td>
    <td height="16" width="33%" class="sfondomenu"><a class="tbltext0" href="/PLAVORO/Testi/Info/Guida/ricerca.asp"><b><u>Ricerca su Internet</u></b></a></td>
  </tr>
  <tr bordercolor="#000080" valign="top" align="center"> 
    <td height="16" width="33%" class="sfondomenu"><a class="tbltext0" href="/PLAVORO/Testi/Info/Guida/virus.asp"><b><u>Virus Informatici</u></b></a></td>
    <td height="16" width="34%" class="sfondomenu"><a class="tbltext0" href="/PLAVORO/Testi/Info/Guida/faq.asp"><b><u>Domande e Risposte</u></b></a></td>
    <td height="16" width="33%" class="sfondomenu"><a class="tbltext0" href="/PLAVORO/Testi/Info/Guida/glossario.asp"><b><u>Piccolo Glossario</u></b></a></td>
  </tr>
</table>
<br>
<font face="verdana" color="#3399CC"> <b>Tipi di file</b></font><br><br>
<img src="/PLAVORO/Images/Guida/software01.jpg" align="left" hspace="6" vspace="6" WIDTH="60" HEIGHT="61">  
<font size="2" face="verdana">I documenti che si trovano su Internet sono di moltissimi tipi: 
  testi, disegni e grafici, programmi, suoni e musica, immagini (fisse o animate), 
  foto e filmati. Molti di questi file hanno bisogno di appositi programmi per 
  poter essere letti o scaricati. <br>
  Per comodit� di esposizione, possiamo distinguere tra: </font><br>
  </font></p>
<table width="100%">
  <tr> 
    <td width="5%"> </td>
    <td width="95%"> <font size="2" color="black" face="Verdana, Arial, Helvetica, sans-serif"> 
      <a href="#FileCompressi">File compressi</a><br>
      <a href="#Documenti">Documenti</a><br>
      <a href="#FileMusicali">File musicali</a><br>
      <a href="#Immagini">Immagini</a><br>
      <a href="#Filmati">Filmati</a><br>
      <a href="#Programmi">Programmi</a> </font></td>
  </tr>
</table>
<font face="Verdana, Arial, Helvetica, sans-serif"><br>
<br>
</font>
<hr>
<font face="Verdana, Arial, Helvetica, sans-serif"><br>
<img src="/PLAVORO/Images/Guida/winzip01.jpg" align="left" hspace="6" vspace="6" WIDTH="60" HEIGHT="60"> 
<a name="FileCompressi"><font color="black" size="3"><b>File compressi</b></font></a> 
<font size="2">(con estensione <b><font color="black">.zip</font></b>) <br>
File ridotti come dimensione tramite un apposito programma (<i><font color="black">Winzip</font></i>) 
per rendere pi� veloce l'operazione di scaricamento. La maggior parte dei file 
contenenti testi e disegni o programmi, che troviamo su Internet, sono sotto forma 
compressa. <br>
Se non gi� presente sul computer, <i><font color="black">Winzip</font></i> pu� 
essere scaricato gratuitamente dal sito</font> <a href="http://www.volftp.it" target="_blank"><font size="3">www.volftp.it</font></a> 
<br>
<br>
<b><font color="black" size="2">Per maggiori informazioni sulle operazioni di 
scaricamento del programma o per la compressione e la decompressione dei file, 
consultare il manuale on line all'indirizzo</font></b><font size="2">: </font><br>
<br>
<a href="http://www.pokebook.it/winzip/indice.php" target="_blank"><font size="3">www.pokebook.it/winzip/indice.php</font></a> 
<br>
<br>
<br>
</font>
<hr>
<font face="Verdana, Arial, Helvetica, sans-serif"><br>
<img src="/PLAVORO/Images/Guida/documenti01.jpg" align="left" hspace="6" vspace="6" WIDTH="60" HEIGHT="60"> 
<a name="Documenti"><font color="black" size="3"><b>Documenti di testo e grafica</b></font></a>,<font size="2"> 
in diversi formati, a seconda del programma utilizzato per la compilazione (con 
estensione <b><font color="black">.doc</font></b>, <b><font color="black">.txt</font></b>, 
<b><font color="black">.rtf</font></b>, <b><font color="black">.pdf</font></b>): 
<br>
<b><font color="black">.txt</font></b> sono i documenti di testo semplice, scritti, 
ad esempio, con il blocco notes di Windows (<i><font color="black">Notepad</font></i>). 
Detti documenti non supportano alcun tipo di formattazione (margini, paragrafi, 
centrature), n� modifiche ai caratteri (dimensioni, tipologia, corsivo, grassetto, 
sottolineatura). <br>
<b><font color="black">.rtf</font></b>, file di testo formattato, che possono 
essere letti con il <i><font color="black">WorldPad</font></i> di Windows. <br>
<b><font color="black">.doc</font></b> sono i documenti scritti con il programma 
di videoscrittura <i><font color="black">Word</font></i>. <br>
<b><font color="black">PDF</font></b> (acronimo di Portable Document Format) � 
il formato pi� comune che si usa per i documenti. La sua caratteristica principale 
consiste nel conservare inalterati i caratteri, la formattazione, le immagini 
e i colori originari, indipendentemente dal software e dal sistema operativo utilizzati 
per crearlo. I documenti in formato <b><font color="black">PDF</font></b> (con 
estensione <b><font color="black">.pdf</font></b>) possono essere consultati e 
stampati con <i><font color="black">Adobe Acrobat Reader</font></i>, scaricabile 
gratuitamente dal sito </font><a href="http://www.adobe.it/products/acrobat/readstep.html" target="_blank"><font size="3">www.adobe.it/products/acrobat/readstep.html</font></a>. 
<br>
<br>
<br>
</font>

<a href="<%=Session("Progetto")%>/testi/info/guida/files.asp"><img alt="Torna all'inizio pagina" align="right" src="<%=Session("Progetto")%>/images/sopra.gif" border="0"></a>
<br>
<hr>
<font face="Verdana, Arial, Helvetica, sans-serif"><br>
<img src="/PLAVORO/Images/Guida/musica05.jpg" align="left" hspace="6" vspace="6" WIDTH="62" HEIGHT="59"> 
<a name="FileMusicali"><font color="black" size="3"><b>File musicali</b></font></a>, 
<font size="2">nei pi� comuni formati <i><font color="black">Mp3</font></i>, <i><font color="black">midi</font></i>, 
<i><font color="black">wave</font></i> o <i><font color="black">.kar</font></i>. 
<br>
<b><font color="black">Mp3</font></b>, file con estensione <b><font color="black">.mp3</font></b>. 
<br>
<b><font color="black">Midi</font></b>, con estensione <b><font color="black">.mid</font></b>. 
Standard di comunicazione ideato per far dialogare tra loro strumenti musicali 
e computer. Utilizzato anche per riprodurre musica per il ridotto spazio di memoria 
occupato. <br>
<b><font color="black">Wave</font></b>, con estensione <b><font color="black">.wav</font></b> 
<br>
<b><font color="black">.kar</font></b>, file di karaoke che, oltre a riprodurre 
i vari strumenti musicali utilizzati nel brano, visualizza anche le parole. Occorre 
un apposito programma (<i><font color="black">winkar</font></i>, <i><font color="black">vanbasco's'</font></i>, 
<i><font color="black">kar all</font></i>). <br>
<br>
Per ascoltare un CD, file <i><font color="black">midi</font></i> e <i><font color="black">wave</font></i> 
� sufficiente il lettore multimediale contenuto in Windows. Il programma <i><font color="black">Winamp</font></i>, 
scaricabile gratuitamente dal sito</font> <a href="http://www.winamp.com/" target="_blank"><font size="3">www.winamp.com</font></a> 
<font size="2">, � in grado di leggere CD, file midi e anche file Mp3. Per la 
conversione di brani di un comune CD in file Mp3, pu� essere utilizzato <i><font color="black">Audiocatalist</font></i>, 
scaricabile dal sito</font> <a href="http://www.xingtech.com" target="_blank"><font size="3">www.xingtech.com</font></a>. 
<br>
<br>
<b><font color="black" size="3">Streaming</font></b> <br>
<font size="2">Negli ultimi tempi si va sempre pi� affermando una tecnologia (valida 
sia per file audio e video) che, a differenza del normale download (dove bisogna 
attendere che il file sia completamente scaricato prima di poterlo ascoltare, 
o vedere), permette la fruizione in tempo reale dell'audiovisivo. I file <i><font color="black">streaming</font></i> 
si riconoscono di solito dall'estensione <b><font color="black">.ram</font></b> 
o <b><font color="black">.rm</font></b>. <br>
<br>
<b><font color="black">Link utili per coloro che sono interessati ai file in formato 
mp3 (molti altri potranno essere trovati utilizzando un qualsiasi motore di ricerca)</font></b>: 
</font><br>
<br>
<a href="http://www.ariete.net/mp3/software/sharing.htm" target="_blank"><font size="3">www.ariete.net/mp3/software/sharing</font></a> 
<br>
<font size="2">Sito da cui � possibile scaricare gratuitamente molti programmi 
che permettono di scambiare file musicali e video con altri utenti. Tra i programmi 
disponibili on line sono da segnalare <i><font color="black">Morpheus</font></i>, 
<i><font color="black">WinMX</font></i>, <i><font color="black">iMesh</font></i>, 
<i><font color="black">CuteMX</font></i> e <i><font color="black">Audiogalaxy</font></i>. 
Una <a href="http://www.ariete.net/mp3/faq/intro_01.htm" target="_blank"><font color="black"> 
<font size="3">sezione apposita</font></font></a> fornisce una serie di informazioni 
sui file del formato mp3: cosa sono, come si scaricano, come si ascoltano, come 
si creano, breve storia. </font><br>
<br>
<a href="http://www.musiccity.com/" target="_blank"><font size="3">www.musiccity.com</font></a> 
<br>
<font size="2">Sito da cui scaricare <i><font color="black">Morpheus</font></i>, 
uno dei pi� diffusi software del momento per scambiare file mp3 con altri utenti. 
Una delle caratteristiche interessanti del programma � la possibilit� di scaricare 
lo stesso file da diversi utenti contemporaneamente, accelerando l'operazione. 
Il programma � inoltre in grado di completare i downloads interrotti trovando 
un'altra fonte. </font><br>
<br>
<a href="http://www.ascrocco.it/home/dir/guide/morpheus.htm" target="_blank"><font size="3">www.ascrocco.it/home/dir/guide/morpheus</font></a> 
<br>
<font size="2">Guida a <i><font color="black">Morpheus</font></i>, il programma 
che permette di scambiare file mp3 e video con altri utenti. </font><br>
<br>
<a href="http://www.odisseo.pc.it/scuole/casali/multim/Pag_midi.htm" target="_blank"><font size="3">www.odisseo.pc.it/scuole/casali/multim/Pag_midi</font></a> 
<br>
<font size="2">File in formato MIDI dei pi� famosi cantanti italiani da ascoltare 
on line o scaricare gratuitamente. <br>
</font><br>
<a href="http://midifarm.com/files/midifiles" target="_blank"><font size="3">midifarm.com/files/midifiles</font></a> 
<br>
<font size="2">Pi� di 12.000 basi musicali in formato midi, da ascoltare on line. 
In inglese. </font><br>
<br>
<br>
</font>
<a href="<%=Session("Progetto")%>/testi/info/guida/files.asp"><img alt="Torna all'inizio pagina" align="right" src="<%=Session("Progetto")%>/images/sopra.gif" border="0"></a>
<br>
<hr>
<font face="Verdana, Arial, Helvetica, sans-serif"><br>
<img src="/PLAVORO/Images/Guida/immagini02.jpg" align="left" hspace="6" vspace="6" WIDTH="60" HEIGHT="59"> 
<a name="Immagini"><font color="black" size="3"><b>File di immagini</b></font></a> 
<font size="2">(fisse o animate), nei pi� comuni formati <b><font color="black">Gif</font></b> 
e <b><font color="black">Jpeg</font></b>. <br>
<b><font color="black">Gif</font></b> (estensione dei file <b><font color="black">.gif</font></b>), 
supporta soltanto 256 colori, e pu� quindi essere usato per rappresentare immagini 
senza molte sfumature. E' possibile ottenere delle animazioni visualizzando in 
successione un certo numero di immagini di questo tipo. <br>
<br>
Esempi di <i><font color="black">immagini Gif</font></i>: </font><br>
</font>
<table width="100%">
  <tr> 
    <td width="30%"> 
      <center>
        <font color="black" size="3" face="Verdana, Arial, Helvetica, sans-serif"><img src="/PLAVORO/Images/Guida/ITALY.gif" WIDTH="150" HEIGHT="90"> 
        </font> 
      </center>
    </td>
    <td width="30%"> 
      <center>
        <font color="black" size="3" face="Verdana, Arial, Helvetica, sans-serif"><img src="/PLAVORO/Images/Guida/People.gif" WIDTH="140" HEIGHT="115"> 
        </font> 
      </center>
    </td>
    <td width="20%"> 
      <center>
        <font color="black" size="3" face="Verdana, Arial, Helvetica, sans-serif"><img src="/PLAVORO/Images/Guida/lamu.gif" WIDTH="55" HEIGHT="115"> 
        </font> 
      </center>
    </td>
    <td width="20%"> 
      <center>
        <font color="black" size="3" face="Verdana, Arial, Helvetica, sans-serif"><img src="/PLAVORO/Images/Guida/ANI-GEAR.gif" WIDTH="93" HEIGHT="70"> 
        </font> 
      </center>
    </td>
  </tr>
</table>
<font face="Verdana, Arial, Helvetica, sans-serif"><br>
<b><font color="black" size="2">Jpeg</font></b><font size="2"> (estensione dei 
file <b><font color="black">.jpg</font></b>), consente di utilizzare 16 milioni 
di colori e di comprimere i file per occupare meno spazio su disco. Si usa soprattutto 
per la riproduzione di fotografie o immagini che hanno ampie sfumature di colore. 
<br>
<br>
Esempi di immagini in formato <i><font color="black">Jpeg</font></i>: </font><br>
<br>
</font>
<table width="100%">
  <tr> 
    <td width="50%"> 
      <center>
        <font color="black" size="3" face="Verdana, Arial, Helvetica, sans-serif"><img src="/PLAVORO/Images/Guida/TwinTowers.jpg" WIDTH="200" HEIGHT="151"> 
        </font> 
      </center>
    </td>
    <td width="50%"> 
      <center>
        <font color="black" size="3" face="Verdana, Arial, Helvetica, sans-serif"><img src="/PLAVORO/Images/Guida/Paesaggio.jpg" WIDTH="200" HEIGHT="150"> 
        </font> 
      </center>
    </td>
  </tr>
</table>
<font face="Verdana, Arial, Helvetica, sans-serif"><br>
<br>
<font size="3" color="black"> <b><font size="2">Siti dove possono essere scaricate 
gratuitamente immagini in formato GIF e IPEG</font></b><font size="2">: <br>
</font><br>
<a href="http://www.ultragratis.it/" target="_blank">Ultragratis</a><br>
<font size="2">GIF animate. </font><br>
<br>
<a href="http://www.gifanimate.it" target="_blank">Gif animate</a><br>
<font size="2">Immagini GIF, anche in movimento. </font><br>
<br>
<a href="http://www.animfactory.com" target="_blank">Animfactory</a><br>
<font size="2">Uno dei pi� famosi siti di GIF animate. </font><br>
<br>
<a href="http://www.sira.it/guida/multimed.htm" target="_blank">Multimed</a><br>
<font size="2">Sito dal quale scaricare immagini in formato Jpeg (<b>.jpg</b>) 
di vario tipo. </font><br>
<br>
</font></font><font face="verdana, tahoma" size="2"><font size="3"><font size="2"><font size="3"><font size="2"><font size="3"><font size="2"><font size="3"><font size="2">
<a href="<%=Session("Progetto")%>/testi/info/guida/files.asp"><img alt="Torna all'inizio pagina" align="right" src="<%=Session("Progetto")%>/images/sopra.gif" border="0"></a>
<br>
<hr>
<font color="black" size="3" face="Verdana, Arial, Helvetica, sans-serif"><br>
<img src="/PLAVORO/Images/Guida/cinema01.jpg" align="left" hspace="6" vspace="6" WIDTH="60" HEIGHT="60"> 
<a name="Filmati"><b>Filmati</b></a>, <font size="2">nei pi� comuni formati 
<b>Mpeg</b>, <b>AVI</b> e <b>QuickTime</b>, con estensioni rispettivamente: <b>.mpeg</b> 
(o <b>.mpg</b>), <b>.avi</b>, <b>.mov</b>. <br>
Per poter vedere questi filmati � sufficiente aver installato Microsoft <i>Media 
Player</i>, prelevabile, se non gi� presente sul proprio computer, dal sito</font> 
<a href="http://www.microsoft.com/windows/mediaplayer/" target="_blank">www.microsoft.com/windows/mediaplayer</a> 
. <br>
<br>
<font size="2">Anche per i filmati si va sempre pi� diffondendo la tecnologia 
<i>streaming</i>, la quale, come gi� spiegato per i file audio, consente di vedere 
il filmato senza attendere che il file sia completamente scaricato. Uno dei programmi 
pi� noti � <i>RealPlayer</i> (lo stesso che permette di ascoltare in streaming 
i file audio. Di <i>RealPlayer</i>, scaricabile del sito </font><a href="http://www.real.com" target="_blank">www.real.com</a>, 
<font size="2">esistono due versioni: una completa, a pagamento; l'altra gratuita, 
dalle prestazioni un po' pi� limitate.</font> <br>
<br>
<b><font size="2">Link utili</font></b><font size="2">: </font><br>
<br>
<a href="http://ricerca.inwind.it/oss/main/ricerca-v.jsp" target="_blank">ricerca.inwind.it/oss/main/ricerca-v</a> 
<br>
<font size="2">Sezione del portale <i>InWind</i> dedicata ai video, scaricabili 
liberamente. E' disponibile un motore interno per rendere pi� agevole la ricerca 
di ci� che interessa. </font><br>
<br>
<a href="http://www.videomusica.it/" target="_blank">Videomusica.it</a> <br>
<font size="2">Musica, videoclip e nuove tendenze musicali. Notizie, recensioni 
e classifiche. </font><br>
<br>
<a href="http://www.newmediatv.it/" target="_blank">New Media TV</a> <br>
<font size="2">Sito che trasmette in diretta eventi audio e video per mezzo della 
tecnologia <i>streaming</i>. </font><br>
<br>
<br>
</font><font face="times new roman" size="5"><font size="3"><font size="4"><font size="3"><font size="4"><font size="3"><font face="verdana, tahoma" size="2"><font size="3"><font size="2"><font size="3"><font size="2"><font size="3"><font size="2">
<a href="<%=Session("Progetto")%>/testi/info/guida/files.asp"><img alt="Torna all'inizio pagina" align="right" src="<%=Session("Progetto")%>/images/sopra.gif" border="0"></a>
<br>
<hr>
<font color="black" size="3" face="Verdana, Arial, Helvetica, sans-serif"><br>
<img src="/PLAVORO/Images/Guida/software02.jpg" align="left" hspace="6" vspace="6" WIDTH="59" HEIGHT="61"> 
<a name="Programmi"><b>Programmi di diverso tipo</b></a>, <font size="2">con 
estensione <b>.exe</b>. <br>
Oltre ai programmi citati in precedenza per comprimere e decomprimere file, ai 
file musicali o video, da Internet possono essere scaricati gratuitamente moltissimi 
altri tipi di programmi, come: giochi, programmi per gestire e-mail e newsletter, 
programmi per elaborare file di immagini o per realizzare album di fotografie 
digitali, programmi che accrescono l'efficacia del computer, programmi di protezione 
(ad esempio, gli antivirus)... </font><br>
<br>
</font><font face="times new roman" size="5"><font size="3">
<hr>
<font color="black" size="3" face="Verdana, Arial, Helvetica, sans-serif"><br>
<b><font size="2">Indirizzi da cui scaricare programmi interessanti</font></b><font size="2">: 
</font><br>
<br>
<a href="http://www.programmigratuiti.com/" target="_blank">www.programmigratuiti.com</a> 
<br>
<font size="2">Programi gratuiti scaricabili direttamente da Internet. I programmi 
possono essere ricercati tramite un motore interno, oppure utilizzando la lista 
degli argomenti presente nella <i>Homepage</i>. <br>
</font><br>
<a href="http://www.allsoftware.net" target="_blank">www.allsoftware.net</a> <br>
<font size="2">Sito dedicato interamente al software. Al suo interno troviamo 
notizie, articoli, recensioni, oltre, naturalmente, a moltissimi programmi che 
possono essere scaricati gratuitamente. </font><br>
<br>
<a href="http://download.cnet.com/" target="_blank">download.cnet.com</a> <br>
<font size="2">Sito in lingua inglese, fornitissimo di programmi di tutti i tipi, 
suddivisi in categorie per facilitarne la ricerca. E' disponibile anche un motore 
di ricerca interno che costituisce un valido aiuto per trovare ci� che interessa. 
</font></font><font color="black" size="3" face="verdana, tahoma"><br>
<br>
</font><font face="verdana, tahoma" size="2"><font size="3"><font size="2"><font size="3"><font size="2"><font size="3"><font size="2">
<a href="<%=Session("Progetto")%>/testi/info/guida/files.asp"><img alt="Torna all'inizio pagina" align="right" src="<%=Session("Progetto")%>/images/sopra.gif" border="0"></a>
<br>
<hr>
</font>
</td>
</tr>
</table>


<!--#include Virtual="/strutt_coda2.asp"-->

