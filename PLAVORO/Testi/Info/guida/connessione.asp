<!--#include Virtual="/strutt_testa2.asp"-->


<table width="505" cellspacing="4" cellpadding="2" border="0"><tr>
<td width="505" valign="top">

<table width="505" border="1" cellspacing="1" cellpadding="0" bordercolor="#FFFFFF">
  <tr bordercolor="#000080" valign="top" align="center"> 
    <td height="16" colspan="3" class="sfondomenu"><b class="tbltext0">PERCORSO GUIDATO A INTERNET</b></td>
  </tr>
  <tr bordercolor="#000080" valign="top" align="center"> 
    <td height="16" width="33%" class="sfondomenu"><a class="tbltext0" href="/PLAVORO/Testi/Info/Guida/indexguida.asp"><b><u>Inizio</u></b></a></td>
    <td height="16" width="34%" class="sfondomenu"><a class="tbltext0" href="/PLAVORO/Testi/Info/Guida/storia.asp"><b><u>Cenni Storici</u></b></a></td>
    <td height="16" width="33%" class="sfondomenu"><a class="tbltext0" href="/PLAVORO/Testi/Info/Guida/connessione.asp"><b><u>Connettersi a Internet</u></b></a></td>
  </tr>
  <tr bordercolor="#000080" valign="top" align="center"> 
    <td height="16" width="33%" class="sfondomenu"><a class="tbltext0" href="/PLAVORO/Testi/Info/Guida/files.asp"><b><u>Tipi di file</u></b></a></td>
    <td height="16" width="34%" class="sfondomenu"><a class="tbltext0" href="/PLAVORO/Testi/Info/Guida/comunicazione.asp"><b><u>Strumenti per Comunicare</u></b></a></td>
    <td height="16" width="33%" class="sfondomenu"><a class="tbltext0" href="/PLAVORO/Testi/Info/Guida/ricerca.asp"><b><u>Ricerca su Internet</u></b></a></td>
  </tr>
  <tr bordercolor="#000080" valign="top" align="center"> 
    <td height="16" width="33%" class="sfondomenu"><a class="tbltext0" href="/PLAVORO/Testi/Info/Guida/virus.asp"><b><u>Virus Informatici</u></b></a></td>
    <td height="16" width="34%" class="sfondomenu"><a class="tbltext0" href="/PLAVORO/Testi/Info/Guida/faq.asp"><b><u>Domande e Risposte</u></b></a></td>
    <td height="16" width="33%" class="sfondomenu"><a class="tbltext0" href="/PLAVORO/Testi/Info/Guida/glossario.asp"><b><u>Piccolo Glossario</u></b></a></td>
  </tr>
</table>
<br>
<font face="verdana" color="#3399CC"> <b>Connettersi a Internet</b></font><br><br>
<img src="/PLAVORO/Images/Guida/connessione03.jpg" align="left" hspace="6" vspace="6" WIDTH="61" HEIGHT="59">   
<font size="2" face="verdana" color="black">
<b>Per potersi collegare a Internet occorrono:</b><br>
- un computer
<br>
- un <i><b>browser</b></i> (programma che permette di visualizzare la maggior parte degli &quot;oggetti&quot; presenti su Internet
 (testi, ipertesti, suoni, immagini, animazioni, filmati). I due browser attualmente pi� usati sono <i>Internet
 Explorer</i> e <i>Netscape</i>;
<br>
- un <i><b>modem</b></i>, dispositivo che rende possibile il trasferimento di dati a distanza attraverso una normale linea telefonica.
<br>
- una linea telefonica (normale, ADSL, ISDN);
<br>
- un abbonamento a un <i><b>provider</b></i>, una societ� che dispone di server e di linee ad alta velocit� collegate
 costantemente alla rete.
<br><br><hr><br>

<font face="verdana, tahoma" size="2">
<b>Links per approfondire questo specifico argomento</b>:
<br><br>

<font size="3">
<a href="http://cesare.dsi.uniroma1.it/LaboratorioII/internet/node02.htm" target="_blank">
cesare.dsi.uniroma1.it/LaboratorioII/internet/node02</a>
<br>
<font size="2">
Breve guida che spiega in parole semplici ci� che serve per connettersi a Internet, non trascurando il problema
 dei costi di connessione in rete.
<br><br>

<font size="3">
<a href="http://www.polito.it/cetem/attivita/prodotti/cdrom/icfaq41p/index.htm" target="_blank">
www.polito.it/cetem/attivita/prodotti/cdrom/icfaq41p</a>
<br>
<font size="2">
Introduzione on line a Internet, strutturata secondo lo schema delle <i>FAQ</i> (domande e risposte). Il suo linguaggio
 � semplice e molto discorsivo. Se ne consiglia la lettura a chi desidera approfondire aspetti secondari e
 questioni minori connessi all'uso di Internet.
<br><br>

<font size="3">
<a href="http://www.lib.berkeley.edu/TeachingLib/Guides/Internet/FindInfo.html" target="_blank">
www.lib.berkeley.edu/TeachingLib/Guides/Internet</a>
<br>
<font size="2">
Piccola guida in lingua inglese che fornisce alcune informazioni elementari su Internet. Utile per principianti.

<br><br><hr>
</td>
</tr>
</table>


<!--#include Virtual="/strutt_coda2.asp"-->
