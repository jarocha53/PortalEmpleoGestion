<!--#include Virtual="/strutt_testa2.asp"-->


<table width="505" cellspacing="4" cellpadding="2" border="0"><tr>
<td width="505" valign="top">

<table width="505" border="1" cellspacing="1" cellpadding="0" bordercolor="#FFFFFF">
  <tr bordercolor="#000080" valign="top" align="center"> 
    <td height="16" colspan="3" class="sfondomenu"><b class="tbltext0">PERCORSO GUIDATO A INTERNET</b></td>
  </tr>
  <tr bordercolor="#000080" valign="top" align="center"> 
    <td height="16" width="33%" class="sfondomenu"><a class="tbltext0" href="/PLAVORO/Testi/Info/Guida/indexguida.asp"><b><u>Inizio</u></b></a></td>
    <td height="16" width="34%" class="sfondomenu"><a class="tbltext0" href="/PLAVORO/Testi/Info/Guida/storia.asp"><b><u>Cenni Storici</u></b></a></td>
    <td height="16" width="33%" class="sfondomenu"><a class="tbltext0" href="/PLAVORO/Testi/Info/Guida/connessione.asp"><b><u>Connettersi a Internet</u></b></a></td>
  </tr>
  <tr bordercolor="#000080" valign="top" align="center"> 
    <td height="16" width="33%" class="sfondomenu"><a class="tbltext0" href="/PLAVORO/Testi/Info/Guida/files.asp"><b><u>Tipi di file</u></b></a></td>
    <td height="16" width="34%" class="sfondomenu"><a class="tbltext0" href="/PLAVORO/Testi/Info/Guida/comunicazione.asp"><b><u>Strumenti per Comunicare</u></b></a></td>
    <td height="16" width="33%" class="sfondomenu"><a class="tbltext0" href="/PLAVORO/Testi/Info/Guida/ricerca.asp"><b><u>Ricerca su Internet</u></b></a></td>
  </tr>
  <tr bordercolor="#000080" valign="top" align="center"> 
    <td height="16" width="33%" class="sfondomenu"><a class="tbltext0" href="/PLAVORO/Testi/Info/Guida/virus.asp"><b><u>Virus Informatici</u></b></a></td>
    <td height="16" width="34%" class="sfondomenu"><a class="tbltext0" href="/PLAVORO/Testi/Info/Guida/faq.asp"><b><u>Domande e Risposte</u></b></a></td>
    <td height="16" width="33%" class="sfondomenu"><a class="tbltext0" href="/PLAVORO/Testi/Info/Guida/glossario.asp"><b><u>Piccolo Glossario</u></b></a></td>
  </tr>
</table>
<br>
<font face="verdana" color="#3399CC"> <b>Ricerca su Internet</b></font><br><br>
<b><font face="Verdana, Arial, Helvetica, sans-serif" size="2">Per comodit� 
      di esposizione, possiamo distinguere 3 casi</font></b><font face="Verdana, Arial, Helvetica, sans-serif" size="2">: 
      <br>
      <br>
      <br>
      <b>1.</b> <i><b>Ricerca diretta con indirizzo del sito</b></i> <br>
      Se si dispone dell'indirizzo esatto (<b><i>URL</i></b>) del sito da cercare 
      (ad esempio, <a href="http://www.in-formati.com/" target="_blank"> <font size="3">www.in-formati.com</font></a><font size="3"> 
      </font>), basta inserirlo nell'apposito spazio, che si trova nella parte 
      alta della pagina del browser, e verremo automaticamente condotti al sito 
      desiderato. </font><font face="Verdana, Arial, Helvetica, sans-serif"><font size="3"><font size="4"><font size="3"><br>
      <br>
      <font size="2"><br>
      <a name="motori"></a> <b>2.</b> <i><b>Utilizzo dei motori di ricerca</b></i> 
      <br>
      Se, invece, non abbiamo uno specifico indirizzo, ma vogliamo approfondire 
      un dato argomento, ovvero siamo alla ricerca di determinati file, programmi, 
      risorse grafiche o altro, dobbiamo far ricorso a particolari programmi detti 
      <i>motori di ricerca</i>, che esplorano il web alla ricerca delle pagine 
      che abbiano una qualche attinenza con quello che si sta cercando. <br>
      Per effettuare la ricerca mediante un motore, bisogna inserire una o pi� 
      parole chiave nell'apposito box. </font><br>
      <br>
      </font></font></font></font><font size="3"><font size="4"><font size="3">
      <center>
        <table>
          <tr> 
            <td> <font size="2" face="Verdana, Arial, Helvetica, sans-serif"> 
              <b>Box di ricerca di Yahoo</b>: <br>
              <br>
              <img src="/PLAVORO/Images/Guida/Yahoo01.jpg" WIDTH="400" HEIGHT="126"> <br>
              <br>
              </font></td>
          </tr>
          <tr> 
            <td> <font size="2" face="Verdana, Arial, Helvetica, sans-serif"> 
              <b>Box di ricerca di SuperEva</b>: <br>
              <br>
              <img src="/PLAVORO/Images/Guida/SuperEva01.jpg" WIDTH="400" HEIGHT="120"> </font></td>
          </tr>
        </table>
      </center>
      <font face="Verdana, Arial, Helvetica, sans-serif" size="2"><br>
      <br>
      Il risultato � di solito un certo numero di indirizzi, accompagnati da una 
      breve recensione (ricavata dal titolo e dalle prime righe del sito). <br>
      Il pi� delle volte, una ricerca condotta in questa maniera, specie se si 
      utilizzano parole generiche, dar� come risultato una lista lunghissima di 
      indirizzi che, molte volte, � di scarsa utilit�. Non � infatti pensabile 
      di visitare, uno per uno, centinaia, o magari, migliaia di siti per trovare 
      quei pochi contenenti le informazioni che ci interessano. <br>
      Immaginiamo di utilizzare il motore di ricerca <b><i>Google</i></b> inserendo 
      alcune parole molto comuni (� una prova che ognuno di voi pu� fare). Ecco 
      i risultati: </font><font face="Verdana, Arial, Helvetica, sans-serif"><br>
      <br>
      </font>
      <table width="100%" cellpadding="10">
        <tr> 
          <td width="45%" align="right"> <font face="Verdana, Arial, Helvetica, sans-serif" size="2"><b>libri<br>
            casa<br>
            auto<br>
            orologi</b> </font></td>
          <td width="55%" align="left"> <font face="Verdana, Arial, Helvetica, sans-serif" size="2"><b>509.000 
            pagine web<br>
            346.000 pagine web<br>
            244.000 pagine web<br>
            53.000 pagine web</b> </font></td>
        </tr>
      </table>
      <font face="Verdana, Arial, Helvetica, sans-serif"><br>
      <font size="2">E' chiaro che una ricerca di questo genere non ci serve assolutamente 
      a nulla. <br>
      <br>
      Per ovviare, almeno in parte, a questo inconveniente si pu� ricorrere alla 
      <i><b>ricerca avanzata</b></i>, disponibile in tutti i pi� importanti motori 
      di ricerca. <br>
      L'opzione di ricerca avanzata permette di specificare alcuni vincoli da 
      assegnare alla ricerca stessa, come la lingua e il tipo di dominio di primo 
      livello (<b>.it</b>, <b>.com</b>, <b>.net</b>, <b>.org</b>). Ma la risorsa 
      pi� importante da utilizzare nella ricerca avanzata sono i cosiddetti <i><b>operatori 
      logici</b></i>, che risultano molto efficienti nell'allargare o nel restringere 
      il campo di ricerca entro cui deve svolgersi una data interrogazione. </font><br>
      <br>
      <a name="operatori"></a> </font><font size="5" face="tahoma" color="red">
      <center>
        <font face="Verdana, Arial, Helvetica, sans-serif"> <font size="3">Operatori 
        logici</font></font><font face="Verdana, Arial, Helvetica, sans-serif"><br>
        </font><font size="3" color="black"> </font>
      </center>
      <font size="3" color="black">
      <table width="100%" border="1" cellpadding="6" bordercolor="#FFFFFF">
        <tr bordercolor="#333333"> 
          <td width="20%"> 
            <center>
              <font size="2" face="Verdana, Arial, Helvetica, sans-serif"> <b>AND</b> 
              </font> 
            </center>
          </td>
          <td width="80%"> <font size="2" face="Verdana, Arial, Helvetica, sans-serif"> 
            Indica che la ricerca deve prendere in considerazione solo i siti 
            che contengono tutte le parole indicate e non soltanto una di esse.Esempio: 
            se ci interessano i video di Jovanotti, dovremo scrivere nell'apposito 
            riquadro per la ricerca: <br>
            </font><font size="3"> 
            <center>
              <font size="2" face="Verdana, Arial, Helvetica, sans-serif"> jovanotti 
              <font color="red"> AND </font> video </font> 
            </center>
            </font></td>
        </tr>
        <tr bordercolor="#333333"> 
          <td> 
            <center>
            <font size="2" face="Verdana, Arial, Helvetica, sans-serif"> <b>OR</b> 
            </font> 
          </center>
        </td>
          <td> <font size="2" face="Verdana, Arial, Helvetica, sans-serif"> Indica 
            che la ricerca deve selezionare i siti che contengano almeno una delle 
            parole indicate. Esempio: se desidero cercare i siti dedicati ai CD 
            o alle musicassette, scriver�: </font><font size="3"> 
            <center>
            <font size="2" face="Verdana, Arial, Helvetica, sans-serif"> cd <font color="red"> 
            OR </font> musicassette </font> 
          </center>
          </font></td>
        </tr>
        <tr bordercolor="#333333"> 
          <td> 
            <center>
            <font size="2" face="Verdana, Arial, Helvetica, sans-serif"> <b>(AND) 
            NOT</b> </font> 
          </center>
        </td>
          <td> <font size="2" face="Verdana, Arial, Helvetica, sans-serif"> Esclude 
            una data parola dalla ricerca. Esempio: per cercare tra le diverse 
            marche di moto, ad eccezione della Yamaha, bisogna scrivere: </font><font size="3"> 
            <center>
            <font size="2" face="Verdana, Arial, Helvetica, sans-serif"> moto 
            <font color="red"> AND NOT </font> yamaha </font> 
          </center>
          </font></td>
        </tr>
        <tr bordercolor="#333333"> 
          <td> 
            <center>
            <font size="2" face="Verdana, Arial, Helvetica, sans-serif"> <b>NEAR</b> 
            </font> 
          </center>
        </td>
          <td> <font size="2" face="Verdana, Arial, Helvetica, sans-serif"> Seleziona 
            i siti in cui i due termini non distino tra loro pi� di 10 parole. 
            Questa vicinanza indica con buona probabilit� che i termini inseriti 
            per la ricerca appartengano alla stessa frase e quindi siano in connessione 
            tra loro. </font></td>
        </tr>
      </table>
      <font face="Verdana, Arial, Helvetica, sans-serif"><br>
      <br>
      </font></font> <font size="2" color="black" face="Verdana, Arial, Helvetica, sans-serif"> 
      <b>Per un approfondimento sul tema specifico degli <i>operatori logici</i>, 
      puoi consultare i seguenti siti</b>: <br>
      <br>
      <font size="3"> <a href="http://www.aleamanagement.com/@motori/avanzata.htm" target="_blank">www.aleamanagement.com/@motori/avanzata.htm</a> 
      <br>
      <font size="2">Brevissima guida che pu� servire come introduzione all'uso 
      degli operatori logici nella ricerca avanzata. La descrizione � accompagnata 
      da disegni ed esempi. </font><br>
      <br>
      <font size="3"> <a href="http://www.motoridiricerca.it/operatori.htm" target="_blank">www.motoridiricerca.it/operatori.htm</a> 
      <br>
      <font size="2">Guida sintetica all'uso degli operatori logici nella ricerca 
      avanzata, corredata da esempi pratici. </font><br>
      <br>
      <font size="3"> <a href="http://woodyallenitalia.tripod.com/search-bool.html" target="_blank">woodyallenitalia.tripod.com/search-bool.html</a> 
      <br>
      <font size="2">Altra piccola guida agli operatori logici, in cui viene preso 
      in considerazione anche l'uso dell'asterisco (*), delle virgolette (&quot; &quot;) 
      e delle parentesi () nella ricerca. <br>
      </font><br>
      <br>
      <font size="3"> <b><font size="2">3. <i>Ricerca tramite indici predisposti 
      (categorie)</i></font></b> <font size="2"><br>
      Effettuare la ricerca per categorie (elencate generalmente nella prima pagina 
      dei motori di ricerca e dei portali), scegliendo dall'elenco quella che 
      ha maggiori affinit� con l'argomento ricercato. Si prosegue cos� ai livelli 
      inferiori di sottocategorie, finch� non si giunge a ci� che interessa. <br>
      <br>
      Esempi di categorie tratte dal motore di ricerca Yahoo: <br>
      </font><br>
      </font></font></font></font></font><font size="2" color="black" face="verdana, tahoma"><font size="3"><font size="2"><font size="3"><font size="2"><font size="3"><font size="2"><font face="times new roman" size="3"> 
      <table width="100%">
        <tr> 
          <td width="40%" valign="middle"> <font face="Verdana, Arial, Helvetica, sans-serif" size="1"> 
            <font size="2">Cliccando su uno qualsiasi dei link principali, si 
            viene condotti alle <i>sottocategorie</i>, tra le quali dovremo ancora 
            scegliere per avvicinarci maggiormente all'argomento che ci interessa. 
            Il risultato finale � una o pi� pagine di link che portano ai siti 
            contenenti l'argomento o gli oggetti che stiamo cercando. </font></font></td>
          <td width="60%"> 
            <center>
              <font face="Verdana, Arial, Helvetica, sans-serif"><img src="/PLAVORO/Images/Guida/Yahoo03.gif" WIDTH="300" HEIGHT="257"> 
              </font> 
            </center>
          </td>
        </tr>
      </table>
      <hr>
      <font face="Verdana, Arial, Helvetica, sans-serif"><br>
      <font size="2" color="black"> <b>Links di approfondimento su <i>motori di 
      ricerca</i> in generale</b>: <br>
      <br>
      <font size="3"> <a href="http://www.motoridiricerca.it/principa.htm" target="_blank">www.motoridiricerca.it/principa.htm</a> 
      <br>
      <font size="2"> I principali motori di ricerca. Caratteristiche, confronti 
      tra motori, descrizione dei pi� importanti motori di ricerca italiani e 
      internazionali, modalit� specifiche di ricerca su Internet. <br>
      <br>
      <font size="3"> <a href="http://www.polito.it/cetem/attivita/prodotti/cdrom/icfaq41p/index.htm" target="_blank">www.polito.it/cetem</a> 
      <br>
      <font size="2"> Sezione on line, realizzata inizialmente per gli studenti 
      del Politecnico di Torino, che tratta in maniera abbastanza esaustiva l'argomento 
      delle modalit� di ricerca su Internet (utilizzo di indici di rete e motori 
      di ricerca, ricerca e-mail). La forma dell'esposizione segue lo schema delle 
      domande e delle risposte (FAQ); il linguaggio � semplice e la trattazione 
      scende spesso nei particolari. <br>
      <br>
      <font size="3"> <a href="http://www.netserve.it/motoritaliani.htm" target="_blank">www.netserve.it/motoritaliani.htm</a> 
      <br>
      <font size="2"> Caratteristiche dei pi� importanti motori di ricerca italiani 
      e internazionali. <br>
      <br>
      <font size="3"> <a href="http://212.216.174.30/internet/leggi/internet2000/online/testo/15_testo.htm" target="_blank"> 
      Internet/leggi/internet2000/online/testo/15_testo</a> <br>
      <font size="2"> Manuale completo, di media difficolt�, utile per chi desidera 
      una trattazione pi� completa sull'argomento. Nel sito si trova la descrizione 
      del funzionamento dei motori, con molti esempi pratici. Non manca una sezione 
      dedicata ai pi� diffusi motori di ricerca, con l'indicazione delle caratteristiche 
      distintive e delle diverse modalit� di funzionamento. <br>
      <br>
      <font size="3"> <a href="http://www.monash.com/spidap.html" target="_blank">www.monash.com/spidap.html</a> 
      <br>
      <font size="2"> Guida on line ai motori di ricerca: domande pi� frequenti 
      e relative risposte (FAQ), differenze tra diversi tipi di motori, come funzionano 
      i motori di ricerca (ricerca semplice e ricerca avanzata), descrizione dei 
      pi� famosi motori di ricerca. In lingua inglese. <br>
      <br>
      <font size="3"> <a href="http://www.screen.com/start/guide/searchengines.html" target="_blank">www.screen.com/start/guide/searchengines.html</a> 
      <br>
      <font size="2"> Links in lingua inglese ai principali siti di approfondimento 
      sull'argomento motori di ricerca. <br>
      <br>
      </font></font></font></font></font></font></font></font></font></font></font></font></font></font><font size="2" color="black" face="verdana, tahoma"><font size="3"><font size="2"><font size="3"><font size="2"><font size="3"><font size="2"><font size="3"><font size="2"><font size="3"><font size="2"><font size="3"><font size="2"> 
      <hr>
      <font face="Verdana, Arial, Helvetica, sans-serif"><br>
      <font color="red"> <b>Vuoi verificare la tua preparazione sull'utilizzo 
      dei <i>motori di ricerca</i>?</b> <br>
      <font color="black"> Il motore di ricerca <b><i>Altavista</i></b> mette 
      a disposizione due test, da svolgere direttamente on line, per verificare 
      le tue conoscenze sull'argomento: </font></font></font><font face="times new roman" size="4" color="red"><font size="3" color="black">
      <table width="100%">
        <tr> 
          <td width="50%"> 
            <center>
              <font size="2" face="Verdana, Arial, Helvetica, sans-serif"> <a href="http://www.altavista.wwmind.com/education2/testbase.php3" target="_blank"><b>test 
              base</b></a> </font> 
            </center>
          </td>
          <td width="50%"> 
            <center>
              <font size="2" face="Verdana, Arial, Helvetica, sans-serif"> <a href="http://www.altavista.wwmind.com/education2/testadv.php3" target="_blank"><b>test 
              avanzato</b></a> </font> 
            </center>
          </td>
        </tr>
      </table>
      <font face="Verdana, Arial, Helvetica, sans-serif" size="2"><br>
      Prima di affrontare i test, ti si consiglia comunque di leggere la <a href="http://www.altavista.wwmind.com/education2/guida/index.php3" target="_blank"> 
      <b>guida on line</b></a>, che spiega quali sono le caratteristiche specifiche 
      delle modalit� di ricerca di <b><i>Altavista</i></b>. <br>
      </font><font size="3"><font size="4"><font size="3"><br>
      <hr>
      </font>
</td>
</tr>
</table>


<!--#include Virtual="/strutt_coda2.asp"-->


