<!--#include Virtual="/strutt_testa2.asp"-->


<table width="505" cellspacing="4" cellpadding="2" border="0"><tr>
<td width="505" valign="top">

<table width="505" border="1" cellspacing="1" cellpadding="0" bordercolor="#FFFFFF">
  <tr bordercolor="#000080" valign="top" align="center"> 
    <td height="16" colspan="3" class="sfondomenu"><b class="tbltext0">PERCORSO GUIDATO A INTERNET</b></td>
  </tr>
  <tr bordercolor="#000080" valign="top" align="center"> 
    <td height="16" width="33%" class="sfondomenu"><a class="tbltext0" href="/PLAVORO/Testi/Info/Guida/indexguida.asp"><b><u>Inizio</u></b></a></td>
    <td height="16" width="34%" class="sfondomenu"><a class="tbltext0" href="/PLAVORO/Testi/Info/Guida/storia.asp"><b><u>Cenni Storici</u></b></a></td>
    <td height="16" width="33%" class="sfondomenu"><a class="tbltext0" href="/PLAVORO/Testi/Info/Guida/connessione.asp"><b><u>Connettersi a Internet</u></b></a></td>
  </tr>
  <tr bordercolor="#000080" valign="top" align="center"> 
    <td height="16" width="33%" class="sfondomenu"><a class="tbltext0" href="/PLAVORO/Testi/Info/Guida/files.asp"><b><u>Tipi di file</u></b></a></td>
    <td height="16" width="34%" class="sfondomenu"><a class="tbltext0" href="/PLAVORO/Testi/Info/Guida/comunicazione.asp"><b><u>Strumenti per Comunicare</u></b></a></td>
    <td height="16" width="33%" class="sfondomenu"><a class="tbltext0" href="/PLAVORO/Testi/Info/Guida/ricerca.asp"><b><u>Ricerca su Internet</u></b></a></td>
  </tr>
  <tr bordercolor="#000080" valign="top" align="center"> 
    <td height="16" width="33%" class="sfondomenu"><a class="tbltext0" href="/PLAVORO/Testi/Info/Guida/virus.asp"><b><u>Virus Informatici</u></b></a></td>
    <td height="16" width="34%" class="sfondomenu"><a class="tbltext0" href="/PLAVORO/Testi/Info/Guida/faq.asp"><b><u>Domande e Risposte</u></b></a></td>
    <td height="16" width="33%" class="sfondomenu"><a class="tbltext0" href="/PLAVORO/Testi/Info/Guida/glossario.asp"><b><u>Piccolo Glossario</u></b></a></td>
  </tr>
</table>
<br>
<font face="verdana" color="#3399CC"> <b>Virus Informatici</b></font><br><br>
<img src="/PLAVORO/Images/Guida/virusinformatici01.jpg" align="left" hspace="6" vspace="6" WIDTH="79" HEIGHT="82">  
<font size="2" face="verdana" color="black">
Se navighi spesso su Internet o se hai l'abitudine di scambiare programmi su floppy disk con i tuoi amici, ti
 capiter� prima o poi di fare la conoscenza con i cosiddetti v<i>irus informatici</i>, con conseguenze anche serie
 per i file che si trovano sul tuo computer. 
<br>I virus informatici sono piccoli programmi, scritti generalmente in assembler, che si diffondono tramite il trasferimento di files infetti da un computer all'altro. Tre sono le caratteristiche principali che li distinguono:
<br>- si attaccano ad altri files, non essendo capaci di essere presenti come programmi autonomi;
<br>- sono in grado di replicarsi, ossia ogni volta che vengono eseguiti possono riprodurre una parte di codice
 eseguibile uguale a se stessi, inserendolo in altri files;
<br>- hanno la capacit� di attivarsi in un dato momento, provocando la cancellazione o la modifica di alcune informazioni del sistema.
<br><br>Mentre i primi virus attaccavano soltanto file eseguibili (quelli con estensione <b><i>.com</i></b>
 o <b><i>.exe</i></b>), i virus recenti riescono ad alterare molti altri tipi di files, comprese le istruzioni
 del <i>BIOS</i> e persino di danneggiare fisicamente l'hardware.
<br><br><hr><br>

<font face="verdana, tahoma" size="2">
<b>Per saperne di pi�, visita i siti sotto indicati</b>:
<br><br>

<font size="3">
<a href="http://www.gem.it/virus/" target="_blank">www.gem.it/virus/</a>
<br>
<font size="2">
Sito dedicato interamente ai virus informatici: cosa sono, chi li costruisce, come si diffondono e cosa fanno,
 come difendersi.
<br><br>

<font size="3">
<a href="http://www.bio.unipd.it/network/virus.html" target="_blank">www.bio.unipd.it/network/virus</a>
<br>
<font size="2">
Descizione dettagliata delle varie tipologia di virus esistenti, anche dal punto di vista della loro evoluzione
 storica. Come operano, quali tipi di danno possono arrecare, in che modo ci si pu� difendere.
<br><br>

<font size="3">
<a href="http://www.bcom.it/guida/virus/" target="_blank">www.bcom.it/guida/virus/</a>
<br>
<font size="2">
Manualetto on line che insegna a evitare i danni da parte dei virus informatici.
<br><br>

<font size="3">
<a href="http://www.zdnet.it/zdnet/jumpCh.asp?idChannel=287&amp;idUser=0" target="_blank">
www.zdnet.it/zdnet</a>
<br>
<font size="2">
Manualetto on line dedicato ai virus informatici.

<br><br><hr>
</td>
</tr>
</table>


<!--#include Virtual="/strutt_coda2.asp"-->



