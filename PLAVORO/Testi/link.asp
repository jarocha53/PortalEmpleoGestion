<!--#include Virtual="/strutt_testa3.asp"-->
<%  
If Session("login") <> "" then
  iLTable = "520"
Else
  iLTable = "371"
End If
%>
<TABLE border="0" width="<%=iLTable%>" height="54" cellspacing="0" cellpadding="0">
	<tr>
		<% If Session("login") <> "" then %>
		<td width="<%=iLTable%>" valign="bottom" align="right" background="/PLAVORO/images/titoli/link2b.gif">
		<%  Else %>
		<td width="<%=iLTable%>" valign="bottom" align="right" background="/PLAVORO/images/titoli/link3b.gif">
		<%	End If	%>
			<span class=tbltext1a><b>LINK UTILI</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		</td>
	</tr>
</TABLE>
<TABLE border="0" width="<%=iLTable%>" cellspacing="3" cellpadding="3">
	<tr>
		<td class="textblack" valign="top" width="50%"><b class="textred">POLITICA e ISTITUZIONI</b><br>
			<a class="textblack" href="/PLAVORO/Testi/Info/infoprimolivello/links/politica/LK_parlmin.asp"><u>Parlamento e Ministeri</u></a>,
			<a class="textblack" href="/PLAVORO/Testi/Info/infoprimolivello/links/politica/LK_regioni.asp"><u>Regioni</u></a>,
			<a class="textblack" href="/PLAVORO/Testi/Info/infoprimolivello/links/politica/LK_provcom.asp"><u>Province e Comuni</u></a>,
			<a class="textblack" href="/PLAVORO/Testi/Info/infoprimolivello/links/politica/LK_comeur.asp"><u>ComunitÓ europea</u></a>,
			<a class="textblack" href="/PLAVORO/Testi/Info/infoprimolivello/links/politica/LK_enti.asp"><u>Enti</u></a>,
			<a class="textblack" href="/PLAVORO/Testi/Info/infoprimolivello/links/politica/LK_partpol.asp"><u>Partiti politici</u></a>, 
			<a class="textblack" href="/PLAVORO/Testi/Info/infoprimolivello/links/politica/LK_orgint.asp"><u>Organizzazioni internazionali</u></a> 
		</td>
		<td class="textblack" valign="top" width="50%"><b class="textred">AZIENDE</b><br>
			<a class="textblack" href="/PLAVORO/Testi/Info/infoprimolivello/links/aziende/LK_servaziende.asp"><u>Servizi per le aziende</u></a>,
			<a class="textblack" href="/PLAVORO/Testi/Info/infoprimolivello/links/aziende/LK_stagetir.asp"><u>Stage e tirocini</u></a>,
			<a class="textblack" href="/PLAVORO/Testi/Info/infoprimolivello/links/aziende/LK_agevfinanz.asp"><u>Agevolazioni e finanziamenti</u></a>,
			<a class="textblack" href="/PLAVORO/Testi/Info/infoprimolivello/links/aziende/LK_asscat.asp"><u>Associazioni di categoria</u></a>
		</td>
	</tr>
	<tr>
		<td class="textblack" valign="top" width="50%"><b class="textred">LAVORO</b><br>
			<a class="textblack" href="/PLAVORO/Testi/Info/infoprimolivello/links/lavoro/LK_incontrodo.asp"><u>Incontro domanda-offerta</u></a>,
			<a class="textblack" href="/PLAVORO/Testi/Info/infoprimolivello/links/lavoro/LK_mobilitans.asp"><u>MobilitÓ Nord-Sud</u></a>,
			<a class="textblack" href="/PLAVORO/Testi/Info/infoprimolivello/links/lavoro/LK_terzosett.asp"><u>Terzo settore</u></a>,
			<a class="textblack" href="/PLAVORO/Testi/Info/infoprimolivello/links/lavoro/LK_lsu.asp"><u>LSU</u></a>,
			<a class="textblack" href="/PLAVORO/Testi/Info/infoprimolivello/links/lavoro/LK_leglavoro.asp"><u>Legislazione sul lavoro</u></a>,
			<a class="textblack" href="/PLAVORO/Testi/Info/infoprimolivello/links/lavoro/LK_sindacati.asp"><u>Sindacati</u></a>,
			<a class="textblack" href="/PLAVORO/Testi/Info/infoprimolivello/links/lavoro/LK_autoimprend.asp"><u>Autoimprenditoria</u></a>
			<a class="textblack" href="/PLAVORO/Testi/Info/infoprimolivello/links/lavoro/LK_concorsipub.asp"><u>Concorsi pubblici</u></a>,
			<a class="textblack" href="/PLAVORO/Testi/Info/infoprimolivello/links/lavoro/LK_pattiterrit.asp"><u>Patti Territoriali</u></a>
		</td>
		<td class="textblack" valign="top" width="50%"><b class="textred">FORMAZIONE</b></a><br>
			<a class="textblack" href="/PLAVORO/Testi/Info/infoprimolivello/links/formazione/LK_progettoin.asp"><u>Progetto IN</u></a>,
			<a class="textblack" href="/PLAVORO/Testi/Info/infoprimolivello/links/formazione/LK_guideonline.asp"><u>Guide on line</u></a>,
			<a class="textblack" href="/PLAVORO/Testi/Info/infoprimolivello/links/formazione/LK_offertaformat.asp"><u>Offerta formativa regionale</u></a>,
			<a class="textblack" href="/PLAVORO/Testi/Info/infoprimolivello/links/formazione/LK_fondosociale.asp"><u>Fondo Sociale Europeo</u></a>,
			<a class="textblack" href="/PLAVORO/Testi/Info/infoprimolivello/links/formazione/LK_orientamento.asp"><u>Orientamento</u></a>
		</td>
	</tr>
	<tr>
		<td class="textblack" valign="top" width="50%"><b class="textred">ISTRUZIONE</b></a><br>
			<a class="textblack" href="/PLAVORO/Testi/Info/infoprimolivello/links/istruzione/LK_obbligoformat.asp"><u>Obbligo formativo</u></a>,
			<a class="textblack" href="/PLAVORO/Testi/Info/infoprimolivello/links/istruzione/LK_scuolalav.asp"><u>Scuola-lavoro</u></a>,
			<a class="textblack" href="/PLAVORO/Testi/Info/infoprimolivello/links/istruzione/LK_scuoleonline.asp"><u>Scuole on line</u></a>,
			<a class="textblack" href="/PLAVORO/Testi/Info/infoprimolivello/links/istruzione/LK_unionline.asp"><u>UniversitÓ on line</u></a>,
			<a class="textblack" href="/PLAVORO/Testi/Info/infoprimolivello/links/istruzione/LK_didattica.asp"><u>Didattica</u></a>,
			<a class="textblack" href="/PLAVORO/Testi/Info/infoprimolivello/links/istruzione/LK_riformascol.asp"><u>Riforma scolastica</u></a>,
			<a class="textblack" href="/PLAVORO/Testi/Info/infoprimolivello/links/istruzione/LK_studenti.asp"><u>Studenti</u></a>
		</td>
		<td class="textblack" valign="top" width="50%"><b class="textred">IMMIGRAZIONE</b></a><br>
			<a class="textblack" href="/PLAVORO/Testi/Info/infoprimolivello/links/immigrazione/LK_stranieriit.asp"><u>Stranieri in Italia</u></a>,
			<a class="textblack" href="/PLAVORO/Testi/Info/infoprimolivello/links/immigrazione/LK_italestero.asp"><u>Italiani all'estero</u></a>,
		</td>
	</tr>
	<tr>
		<td class="textblack" valign="top" width="50%"><b class="textred">COMPUTER E INTERNET</b></a><br>
			<a class="textblack" href="/PLAVORO/Testi/Info/infoprimolivello/links/internet/LK_motricerca.asp"><u>Motori di ricerca</u></a>,
			<a class="textblack" href="/PLAVORO/Testi/Info/infoprimolivello/links/internet/LK_risorseserv.asp"><u>Risorse e servizi</u></a>,
			<a class="textblack" href="/PLAVORO/Testi/Info/infoprimolivello/links/internet/LK_spwebmaster.asp"><u>Spazio webmaster</u></a>,
			<a class="textblack" href="/PLAVORO/Testi/Info/infoprimolivello/links/computer/LK_hardware.asp"><u>Hardware</u></a>,
			<a class="textblack" href="/PLAVORO/Testi/Info/infoprimolivello/links/computer/LK_software.asp"><u>Software</u></a>
		</td>
		<td class="textblack" valign="top" width="50%"><b class="textred">MONDO DONNA</b></a><br>
			<a class="textblack" href="/PLAVORO/Testi/Info/infoprimolivello/links/mondodonna/LK_lavorofem.asp"><u>Lavoro al femminile</u></a>,
			<a class="textblack" href="/PLAVORO/Testi/Info/infoprimolivello/links/mondodonna/LK_assocfem.asp"><u>Associazioni femminili</u></a>
		</td>
	</tr>
</TABLE>
<!--#include Virtual="/strutt_coda3.asp"-->
