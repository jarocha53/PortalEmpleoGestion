<!--#include Virtual="/strutt_testa1.asp"-->
<br><div align="center">

<table border="0" width="740" height="54" cellspacing="0" cellpadding="0">
  <tr>
    <td width="100%" valign="bottom" align="right" background="/PLAVORO/images/contattaci.gif">
    <span class="tbltext1a"><b>CONTATTACI</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span> </td>
  </tr>
</table>

<br><div align="center">
  <center>
   <table border="0" width="740" cellpadding="2">
  <tr>
	  <td width="50%" align="right">
	  <b><a class="tbltext3" href="mailto:InfoUtente@italialavoro.it"><u>Scrivici</u></b>&nbsp;</td>
			<td width="50%" align="left"><a href="mailto:InfoUtente@italialavoro.it"><img border="0" src="/Images/icons/posta.gif" WIDTH="100" HEIGHT="89"></a>
			</a></td>
	</tr>
  </table>
  <table border="0" width="740" cellpadding="2">
  <!--tr>    <td width="100%" colspan="5" align=center>&nbsp;</td>  </tr-->

    <tr>
      <td width="100%" height="33" colspan="5" align="center"><font face="Verdana" color="#3398CC" size="4"><i>Elenco
        sedi per il progetto</i></font><br>
        <br>
      </td>
    </tr>
    <tr>
      <td width="100%" height="33" colspan="5" align="center"><font face="Verdana" color="#3398CC" size="4">Sede
        Centrale</font></td>
    </tr>

    <tr>
      <td width="10%" bgcolor="#3398CC" align="left"><font color="#FFFFFF">Citt�</font></td>
      <td width="15%" bgcolor="#3398CC" align="left"><font color="#FFFFFF">Cap</font></td>
      <td width="30%" bgcolor="#3398CC" align="left"><font color="#FFFFFF">Via e Numero
          Civico</font></td>
      <td width="15%" bgcolor="#3398CC" align="left"><font color="#FFFFFF">Telefono</font></td>
      <td width="15%" bgcolor="#3398CC" align="left"><font color="#FFFFFF">Fax</font></td>
    </tr>
    <tr>
      <td width="10%" bgcolor="#F1F3F3"><span class="tbltext">Roma</span></td>
      <td width="15%" bgcolor="#F1F3F3"><span class="tbltext">00154</span></td>
      <td width="30%" bgcolor="#F1F3F3"><span class="tbltext">Viale Ostiense 131/L</span></td>
      <td width="15%" bgcolor="#F1F3F3"><span class="tbltext">06/570121</span></td>
      <td width="15%" bgcolor="#F1F3F3"><span class="tbltext">06/5754895</span></td>
    </tr>
  </table>
  </center>
</div>
<p><br>
<br>
</p>
<div align="center">
  <center>
  <table border="0" width="740" cellpadding="2">
    <tr>
      <td width="740" colspan="6">
        <p align="center"><font face="Verdana" color="#3398CC" size="4">Sedi
        Territoriali</font></td>
    </tr>
    <tr>
      <td width="10%" bgcolor="#3398CC"><font color="#FFFFFF">Regione</font></td>
      <td width="15%" bgcolor="#3398CC"><font color="#FFFFFF">Citt�</font></td>
      <td width="15%" bgcolor="#3398CC"><font color="#FFFFFF">Cap</font></td>
      <td width="30%" bgcolor="#3398CC"><font color="#FFFFFF">Via e Numero
          Civico</font></td>
      <td width="15%" bgcolor="#3398CC"><font color="#FFFFFF">Telefono</font></td>
      <td width="15%" bgcolor="#3398CC"><font color="#FFFFFF">Fax</font></td>
    </tr>
    <tr>
      <td width="10%" bgcolor="#3398CC"><font color="#FFFFFF">Abruzzo</font></td>
      <td width="15%" bgcolor="#DDDDDD"><font size="2" face="Verdana">Pescara</font></td>
      <td width="15%" bgcolor="#DDDDDD"><font size="2" face="Verdana">65124</font></td>
      <td width="30%" bgcolor="#DDDDDD"><font size="2" face="Verdana">Via
            Passolanciano, 75</font></td>
      <td width="15%" bgcolor="#DDDDDD"><font size="2" face="Verdana">085/2056821</font></td>
      <td width="15%" bgcolor="#DDDDDD"><font size="2" face="Verdana">085/68632</font></td>
    </tr>
    <tr>
      <td width="10%" bgcolor="#3398CC"><font color="#FFFFFF">Basilicata</font></td>
      <td width="15%" bgcolor="#F1F3F3"><font size="2" face="Verdana">Pomarico (MT)</font></td>
      <td width="15%" bgcolor="#F1F3F3"><font size="2" face="Verdana">75016</font></td>
      <td width="30%" bgcolor="#F1F3F3"><font size="2" face="Verdana">TRV.
            Viale Kennedy, 19</font></td>
      <td width="15%" bgcolor="#F1F3F3"><font size="2" face="Verdana">0835/551288</font></td>
      <td width="15%" bgcolor="#F1F3F3"><font size="2" face="Verdana">0835/552283</font></td>
    </tr>
    <tr>
      <td width="10%" bgcolor="#3398CC"><font color="#FFFFFF">Calabria</font></td>
      <td width="15%" bgcolor="#DDDDDD"><font size="2" face="Verdana">Rende (CS)</font></td>
      <td width="15%" bgcolor="#DDDDDD"><font size="2" face="Verdana">87036</font></td>
      <td width="30%" bgcolor="#DDDDDD"><font size="2" face="Verdana">V.le
            J.F. Kennedy, 16 Centro Commerciale &quot;Metropolis&quot;</font></td>
      <td width="15%" bgcolor="#DDDDDD"><font size="2" face="Verdana">0984/846056</font></td>
      <td width="15%" bgcolor="#DDDDDD"><font size="2" face="Verdana">0984/463790</font></td>
    </tr>
    <tr>
      <td width="10%" bgcolor="#3398CC"><font color="#FFFFFF">Campania</font></td>
      <td width="15%" bgcolor="#F1F3F3"><font size="2" face="Verdana">Napoli</font></td>
      <td width="15%" bgcolor="#F1F3F3"><font size="2" face="Verdana">80143</font></td>
      <td width="30%" bgcolor="#F1F3F3"><font size="2" face="Verdana">Centro
            Direzionale Isola F.4</font></td>
      <td width="15%" bgcolor="#F1F3F3"><font size="2" face="Verdana">081/7347711</font></td>
      <td width="15%" bgcolor="#F1F3F3"><font size="2" face="Verdana">081/7347091</font></td>
    </tr>
    <tr>
      <td width="10%" bgcolor="#3398CC"><font color="#FFFFFF">Puglia</font></td>
      <td width="15%" bgcolor="#DDDDDD">
            <p><font size="2" face="Verdana">Bari</font></p>
            <p><font size="2" face="Verdana">Lecce</font></p>
        <p>&nbsp;</td>
      <td width="15%" bgcolor="#DDDDDD">
            <p><font size="2" face="Verdana">70126</font></p>
            <p><font size="2" face="Verdana">73100</font></p>
        <p>&nbsp;</td>
      <td width="30%" bgcolor="#DDDDDD">
            <p><font size="2" face="Verdana">Via Amendola, 168</font></p>
            <p><font size="2" face="Verdana">Piazza S. Oronzo, 40</font></p>
        <p>&nbsp;</td>
      <td width="15%" bgcolor="#DDDDDD">
            <p><font size="2" face="Verdana">080/5461011</font></p>
            <p><font class="tbltext" span>0832/309012</font></p>
        <p>&nbsp;</td>
      <td width="15%" bgcolor="#DDDDDD">
            <p><font size="2" face="Verdana">080/5461062</font></p>
            <p><font size="2" face="Verdana">0832/302979</font></p>
        <p>&nbsp;</td>
    </tr>
    <tr>
      <td width="10%" bgcolor="#3398CC"><font color="#FFFFFF">Sicilia</font></td>
      <td width="15%" bgcolor="#DDDDDD">
            <p><font size="2" face="Verdana">Licata (AG)</font></p>
            <p><font size="2" face="Verdana">Palermo</font></p>
        <p>&nbsp;</td>
      <td width="15%" bgcolor="#DDDDDD">
            <p><font size="2" face="Verdana">92027</font></p>
            <p><font size="2" face="Verdana">90139</font></p>
        <p>&nbsp;</td>
      <td width="30%" bgcolor="#DDDDDD">
            <p><font size="2" face="Verdana">Via Borgonuovo, 35 (Scica)</font></p>
            <p><font size="2" face="Verdana">Via Riccardo Wagner, 4 </font></p>
        <p>&nbsp;</td>
      <td width="15%" bgcolor="#DDDDDD">
            <p><font size="2" face="Verdana">0922/770036</font></p>
            <p><font size="2" face="Verdana">091/6090660</font></p>
        <p>&nbsp;</td>
      <td width="15%" bgcolor="#DDDDDD">
            <p><font size="2" face="Verdana">0922/770036</font></p>
            <p><font size="2" face="Verdana">091/6090834</font></p>
        <p>&nbsp;</td>
    </tr>
  </table>
  </center>
</div>
<p><br>
<br>
</p>
<center>
<table border="0" width="740" cellpadding="2">
  <tr>
    <td width="740" colspan="6">
      <p align="center"><font face="Verdana" color="#3398CC" size="4">InSar</font></td>
  </tr>
  <tr>
    <td width="10%" bgcolor="#3398CC"><font color="#FFFFFF">Regione</font></td>
    <td width="15%" bgcolor="#3398CC"><font color="#FFFFFF">Citt�</font></td>
    <td width="15%" bgcolor="#3398CC"><font color="#FFFFFF">Cap</font></td>
    <td width="30%" bgcolor="#3398CC"><font color="#FFFFFF">Via e Numero
          Civico</font></td>
    <td width="15%" bgcolor="#3398CC"><font color="#FFFFFF">Telefono</font></td>
    <td width="15%" bgcolor="#3398CC"><font color="#FFFFFF">Fax</font></td>
  </tr>
  <tr>
    <td width="10%" bgcolor="#3398CC"><font color="#FFFFFF">sardegna</font></td>
    <td width="15%" bgcolor="#DDDDDD"><font size="2" face="Verdana">Cagliari</font></td>
    <td width="15%" bgcolor="#DDDDDD"><font size="2" face="Verdana">09123</font></td>
    <td width="30%" bgcolor="#DDDDDD"><font size="2" face="Verdana">Via Mameli, 228</font></td>
    <td width="15%" bgcolor="#DDDDDD"><font size="2" face="Verdana">070/288584</font></td>
    <td width="15%" bgcolor="#DDDDDD"><font size="2" face="Verdana">070/288065</font></td>
  </tr>
</table>
</center><br>
  <table border="0" width="740" cellpadding="2">
  <tr>
	  <td width="50%" align="right">
	  <b><a class="tbltext3" href="mailto:InfoUtente@italialavoro.it"><u>Scrivici</u></b>&nbsp;</td>
			<td width="50%" align="left"><a href="mailto:InfoUtente@italialavoro.it"><img border="0" src="/Images/icons/posta.gif" WIDTH="100" HEIGHT="89"></a>
			</a></td>
	</tr>
  </table>
<br><br>

<table border="0" width="650" cellspacing="0" cellpadding="0" height="81">

  <tr>
	</td>
  </tr>
 </table>

</div>
<!--#include Virtual="/strutt_coda1.asp"-->