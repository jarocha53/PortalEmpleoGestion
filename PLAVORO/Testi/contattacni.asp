<!--#include Virtual="/strutt_testa1.asp"-->
<br><div align="center">

<table border="0" width="740" height="54" cellspacing="0" cellpadding="0">

  <tr>
    <td width="100%" valign="bottom" align="right" background="/PLAVORO/images/contattaci.gif">
    <span class=tbltext1a><b>CONTATTACI</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span> </td>
  </tr>
</table>

<TABLE cellSpacing=1 cellPadding=1 width="80%">
<TBODY>
<TR>
<TD><B><FONT face="Arial, Helvetica, sans-serif" size=2>Sede 
Centrale</FONT></B></TD></TR>
<TR>
<TD><FONT face="Arial, Helvetica, sans-serif" size=2>Roma, 00154, via Ostiense, 
131/L <BR>tel: 06.570121 - fax: 06.5757220<BR><BR></FONT></TD></TR>
<TR>
<TD><B><FONT face="Arial, Helvetica, sans-serif" size=2>Ufficio di 
Rappresentanza</FONT></B></TD></TR>
<TR>
<TD><FONT face="Arial, Helvetica, sans-serif" size=2>Bruxelles, Belgio 
1210<BR>Boulevard du Roi Albert II, 5<BR>tel: +32.2.2240718 - fax: 
+32.2.2240742<BR><BR></FONT></TD></TR>
<TR>
<TD><B><FONT face="Arial, Helvetica, sans-serif" size=2><BR>Uffici 
Regionali</FONT></B></TD></TR>
<TR>
<TD>
<P><FONT face="Arial, Helvetica, sans-serif" 
size=2><BR><B>Abruzzo/Marche/Molise</B><BR>Pescara, 65124, via Passolanciano, 
75<BR>tel: 085.36159 - fax: 085.36164<BR>email: <A 
href="mailto:%20abruzzo@italialavoro.it">abruzzo@italialavoro.it</A><BR><BR><B>Basilicata</B><BR>Pomarico 
(MT), 75016, Trav. v.le Kennedy, 19<BR>tel: 0835.551288 - fax: 
0835.552283<BR>email: <A 
href="mailto:%20basilicata@italialavoro.it">basilicata@italialavoro.it</A><BR><BR><B>Calabria</B><BR>Rende 
(CS), 87036, V.le J.F. Kennedy<BR>Centro Commerciale "Metropolis", scala F/int. 
7<BR>tel: 0984.846056 - fax: 0984.463790<BR>email: <A 
href="mailto:%20calabria@italialavoro.it">calabria@italialavoro.it</A><BR><BR><B>Campania</B><BR>Napoli, 
80143, Centro Direzionale Isola F4<BR>tel: 081.7347701 - fax: 
081.7347091<BR>email: <A 
href="mailto:%20campania@italialavoro.it">campania@italialavoro.it<BR></A><BR><B>Emilia 
Romagna</B><BR>Bologna, 40131, via Saffi, 15<BR>tel: 051.5281914 - fax: 
051.523367<BR>email: <A 
href="mailto:%20emiliaromagna@italialavoro.it">emiliaromagna@italialavoro.it</A> 
<BR><BR><B>Lazio</B><BR>Roma, 00154, via Ostiense, 131/L<BR>tel: 06.57012402 - 
fax: 06.5747567<BR>email: <A 
href="mailto:%20lazio@italialavoro.it">lazio@italialavoro.it</A><BR><BR><B>Liguria</B><BR>Genova, 
16149, via De Marini, 1<BR>tel: 010.460418 - fax: 010.6450791<BR>email: <A 
href="mailto:liguria@italialavoro.it">liguria@italialavoro.it</A><BR><BR><B>Lombardia</B><BR>Milano, 
20122, p.za XXV Aprile, 2<BR>tel: 02.29098222 - fax: 02.29098227<BR>email: <A 
href="mailto:%20lombardia@italialavoro.it">lombardia@italialavoro.it</A><BR><BR><B>Piemonte</B><BR>Torino, 
10124, via Matteo Pescatore, 2<BR>tel: 011.8395730 - fax: 011.8128950<BR>email: 
<A 
href="mailto:%20piemonte@italialavoro.it">piemonte@italialavoro.it</A><BR><BR><B>Puglia</B><BR>Bari, 
70124, via Amendola, 268<BR>tel: 080.5461062 - fax: 080.5481482<BR>Lecce, 73100, 
p.za S.Oronzo, 40<BR>tel: 0832.309012 - fax: 0832.302979<BR>email: <A 
href="mailto:puglia@italialavoro.it">puglia@italialavoro.it</A><BR><BR><B>Sardegna</B><BR>Cagliari, 
09123, (c/o Insar S.p.A.) via Mameli, 228<BR>tel: 070.288584 - fax: 
070.288065<BR>email: <A 
href="mailto:%20sardegna@italialavoro.it">sardegna@italialavoro.it</A><BR><BR><B>Sicilia</B><BR>Palermo, 
90139, via Amari, 124<BR>tel: 091.6121125 - fax: 091.6121154<BR>Licata (AG), 
92022, (c/o SCICA) via Borgonuovo, 35<BR>tel e fax: 0922.770036<BR>email: <A 
href="mailto:sicilia@italialavoro.it">sicilia@italialavoro.it</A><BR><BR><B>Toscana</B><BR>Firenze, 
50129, via Don Minzoni, 35<BR>tel: 055.587239 - fax: 055.5048028<BR>email: <A 
href="mailto:toscana@italialavoro.it">toscana@italialavoro.it</A><BR><BR><B>Umbria</B><BR>Spoleto 
(PG), 06049, via Flaminia, 43<BR>tel: 0743.43744 - fax: 0743.43585<BR>email: <A 
href="mailto:umbria@italialavoro.it">umbria@italialavoro.it</A><BR><BR><B>Veneto</B><BR>Vicenza 
36100, via Leonardo da Vinci, 14<BR>tel: 0444.913814 - fax: 
0444.913358<BR>email: <A 
href="mailto:veneto@italialavoro.it">veneto@italialavoro.it</A></FONT></P>

<table border="0" width="650" cellspacing="0" cellpadding="0" height="81">

  <tr>

	</td>
  </tr>
 </table>

</div>
<!--#include Virtual="/strutt_coda1.asp"-->