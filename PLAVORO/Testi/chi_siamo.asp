<!--#include Virtual="/strutt_testa1.asp"-->
<br><div align="center">

<table border="0" width="740" height="54" cellspacing="0" cellpadding="0">

  <tr>
    <td width="100%" valign="bottom" align="right" background="/PLAVORO/images/chisiamo.gif">
    <span class=tbltext1a><b>CHI SIAMO<b></FONT>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span> </td>
  </tr>
</table>

<br><div align="center">
  <center>
    <table cellSpacing="1" cellPadding="1" width="70%">
      <tbody>
        <tr>
          <td width="50%" bgColor="#3399CC">
            <div align="center">
              <IMG height=228 src="/PLAVORO/images/Covatta.jpg" width=196 border=0>
            </div>
          </td>
          <td width="50%" bgColor="#3399CC">
            <div align="center">
              <IMG height=228 src="/PLAVORO/images/Forlani.jpg" width=196 border=0>
            </div>
          </td>
        </tr>
        <tr bgColor="#cccccc">
          <td width="50%" bgcolor="#c0c0c0">
            <div align="center">
              <font face="Verdana, Arial, Helvetica, sans-serif" size="2"><b>Luigi
              Covatta</b><br>
              Presidente</font>
            </div>
          </td>
          <td width="50%" bgcolor="#c0c0c0">
            <div align="center">
              <font face="Verdana, Arial, Helvetica, sans-serif" size="2"><b>Natale
              Forlani</b><br>
              Amministratore Delegato</font>
            </div>
          </td>
        </tr>
      </tbody>
    </table>
  </center>
</div>
<p align="center">
    <b><font face="Verdana, Arial, Helvetica, sans-serif" size="2">Consiglio di
    Amministrazione</font></b><br>
<div align="center">
  <center>
    <table cellSpacing="1" cellPadding="1" width="70%">
      <tbody>
        <tr>
          <td vAlign="top" width="50%" bgColor="#3399CC"><b><font face="Verdana, Arial, Helvetica, sans-serif" color="#ffffff" size="2">Presidente</font></b></td>
          <td width="50%" bgColor="#c0c0c0"><font face="Verdana, Arial, Helvetica, sans-serif" size="2">Luigi
            Covatta</font></td>
        </tr>
        <tr>
          <td vAlign="top" width="50%" bgColor="#3399CC"><b><font face="Verdana, Arial, Helvetica, sans-serif" color="#ffffff" size="2">Amministratore
            Delegato</font></b></td>
          <td width="50%" bgColor="#c0c0c0"><font face="Verdana, Arial, Helvetica, sans-serif" size="2">Natale
            Forlani</font></td>
        </tr>
        <tr>
          <td vAlign="top" width="50%" bgColor="#3399CC"><b><font face="Verdana, Arial, Helvetica, sans-serif" color="#ffffff" size="2">Consiglieri</font></b></td>
          <td width="50%" bgColor="#c0c0c0"><font face="Verdana, Arial, Helvetica, sans-serif" size="2">Ada
            Becchi<br>
            Cristiano Carocci<br>
            Michele Dau<br>
            Arturo Maresca<br>
            Salvatore Tatarella</font></td>
        </tr>
      </tbody>
    </table>
  </center>
</div>
<p align="center">
    <br>
    <font size="2"><b><font face="Verdana, Arial, Helvetica, sans-serif">Collegio
    Sindacale</font></b></font><br>
<div align="center">
  <center>
    <table cellSpacing="1" cellPadding="1" width="70%">
      <tbody>
        <tr>
          <td vAlign="top" width="50%" bgColor="#3399CC"><b><font face="Verdana, Arial, Helvetica, sans-serif" color="#ffffff" size="2">Presidente</font></b></td>
          <td width="50%" bgColor="#c0c0c0"><font face="Verdana, Arial, Helvetica, sans-serif" size="2">Rosa
            Altavilla</font></td>
        </tr>
        <tr>
          <td vAlign="top" width="50%" bgColor="#3399CC"><b><font face="Verdana, Arial, Helvetica, sans-serif" color="#ffffff" size="2">Sindaci
            Effettivi</font></b></td>
          <td width="50%" bgColor="#c0c0c0"><font face="Verdana, Arial, Helvetica, sans-serif" size="2">Franco
            Bucci<br>
            Giuseppe Di Giovanni</font></td>
        </tr>
      </tbody>
    </table>
  </center>
</div>
<p align="center">
    <br>
    <b><font face="Verdana, Arial, Helvetica, sans-serif" size="2">Comitato
    Scientifico Italia Lavoro</font></b><br>
<div align="center">
  <center>
    <table cellSpacing="1" cellPadding="1" width="70%">
      <tbody>
        <tr>
          <td vAlign="top" width="50%" bgColor="#3399CC"><b><font face="Verdana, Arial, Helvetica, sans-serif" color="#ffffff" size="2">Prof.
            Sergio Arzeni</font></b></td>
          <td width="50%" bgColor="#c0c0c0"><font face="Verdana, Arial, Helvetica, sans-serif" size="2">Responsabile
            Programma Leed dell'OCDE</font></td>
        </tr>
        <tr>
          <td vAlign="top" width="50%" bgColor="#3399CC"><b><font face="Verdana, Arial, Helvetica, sans-serif" color="#ffffff" size="2">Prof.
            Marco Biagi</font></b></td>
          <td width="50%" bgColor="#c0c0c0"><font face="Verdana, Arial, Helvetica, sans-serif" size="2">Professore
            di Diritto del Lavoro presso l'Universitą di Modena</font></td>
        </tr>
        <tr>
          <td vAlign="top" width="50%" bgColor="#3399CC"><b><font face="Verdana, Arial, Helvetica, sans-serif" color="#ffffff" size="2">Prof.
            Renato Brunetta</font></b></td>
          <td width="50%" bgColor="#c0c0c0"><font face="Verdana, Arial, Helvetica, sans-serif" size="2">Professore
            di Economia del Lavoro presso la II^ Universitą di Roma</font></td>
        </tr>
        <tr>
          <td vAlign="top" width="50%" bgColor="#3399CC"><b><font face="Verdana, Arial, Helvetica, sans-serif" color="#ffffff" size="2">Prof.
            Michele Colasanto</font></b></td>
          <td width="50%" bgColor="#c0c0c0"><font face="Verdana, Arial, Helvetica, sans-serif" size="2">Professore
            di Sociologia presso l'Universitą Cattolica di Milano</font></td>
        </tr>
        <tr>
          <td vAlign="top" width="50%" bgColor="#3399CC"><b><font face="Verdana, Arial, Helvetica, sans-serif" color="#ffffff" size="2">Prof.
            Carlo dell'Aringa</font></b></td>
          <td width="50%" bgColor="#c0c0c0"><font face="Verdana, Arial, Helvetica, sans-serif" size="2">Presidente
            ISFOL</font></td>
        </tr>
        <tr>
          <td vAlign="top" width="50%" bgColor="#3399CC"><b><font face="Verdana, Arial, Helvetica, sans-serif" color="#ffffff" size="2">Prof.
            Domenico De Masi</font></b></td>
          <td width="50%" bgColor="#c0c0c0"><font face="Verdana, Arial, Helvetica, sans-serif" size="2">Professore
            di Sociologia del Lavoro presso L'Universitą La Sapienza di Roma</font></td>
        </tr>
        <tr>
          <td vAlign="top" width="50%" bgColor="#3399CC"><b><font face="Verdana, Arial, Helvetica, sans-serif" color="#ffffff" size="2">Prof.
            Giuseppe De Rita</font></b></td>
          <td width="50%" bgColor="#c0c0c0"><font face="Verdana, Arial, Helvetica, sans-serif" size="2">Segretario
            Generale Censis</font></td>
        </tr>
        <tr>
          <td vAlign="top" width="50%" bgColor="#3399CC"><b><font face="Verdana, Arial, Helvetica, sans-serif" color="#ffffff" size="2">Prof.
            Giampaolo Galli</font></b></td>
          <td width="50%" bgColor="#c0c0c0"><font face="Verdana, Arial, Helvetica, sans-serif" size="2">Direttore
            Centro Studi Confindustria</font></td>
        </tr>
        <tr>
          <td vAlign="top" width="50%" bgColor="#3399CC"><b><font face="Verdana, Arial, Helvetica, sans-serif" color="#ffffff" size="2">Prof.
            Luciano Gallino</font></b></td>
          <td width="50%" bgColor="#c0c0c0"><font face="Verdana, Arial, Helvetica, sans-serif" size="2">Direttore
            Dipartimento Scienze dell'Educazione e Formazione presso l'Universitą
            di Torino</font></td>
        </tr>
        <tr>
          <td vAlign="top" width="50%" bgColor="#3399CC"><b><font face="Verdana, Arial, Helvetica, sans-serif" color="#ffffff" size="2">Prof.
            Gustavo Ghidini</font></b></td>
          <td width="50%" bgColor="#c0c0c0"><font face="Verdana, Arial, Helvetica, sans-serif" size="2">Professore
            di Diritto Industriale presso la LUISS</font></td>
        </tr>
        <tr>
          <td vAlign="top" width="50%" bgColor="#3399CC"><b><font face="Verdana, Arial, Helvetica, sans-serif" color="#ffffff" size="2">Prof.
            Paolo Leon</font></b></td>
          <td width="50%" bgColor="#c0c0c0"><font face="Verdana, Arial, Helvetica, sans-serif" size="2">Professore
            di Economia Pubblica presso la III^ Universitą di Roma</font></td>
        </tr>
        <tr>
          <td vAlign="top" width="50%" bgColor="#3399CC"><b><font face="Verdana, Arial, Helvetica, sans-serif" color="#ffffff" size="2">Dr.
            Bruno Manghi</font></b></td>
          <td width="50%" bgColor="#c0c0c0"><font face="Verdana, Arial, Helvetica, sans-serif" size="2">Esperto
            di Politica Sindacale</font></td>
        </tr>
        <tr>
          <td vAlign="top" width="50%" bgColor="#3399CC"><b><font face="Verdana, Arial, Helvetica, sans-serif" color="#ffffff" size="2">Prof.ssa
            Fiorella Kostoris Padoa Schioppa</font></b></td>
          <td width="50%" bgColor="#c0c0c0"><font face="Verdana, Arial, Helvetica, sans-serif" size="2">Presidente
            ISAE - Istituto di Studi e di Analisi Economica</font></td>
        </tr>
        <tr>
          <td vAlign="top" width="50%" bgColor="#3399CC"><b><font face="Verdana, Arial, Helvetica, sans-serif" color="#ffffff" size="2">Prof.
            Giuseppe Pennisi</font></b></td>
          <td width="50%" bgColor="#c0c0c0"><font face="Verdana, Arial, Helvetica, sans-serif" size="2">Professore
            di Finanza Pubblica presso la Scuola Superiore della Pubblica
            Amministrazione</font></td>
        </tr>
        <tr>
          <td vAlign="top" width="50%" bgColor="#3399CC"><b><font face="Verdana, Arial, Helvetica, sans-serif" color="#ffffff" size="2">Dr.
            Mario Pirani</font></b></td>
          <td width="50%" bgColor="#c0c0c0"><font face="Verdana, Arial, Helvetica, sans-serif" size="2">Editorialista
            e giornalista de "La Repubblica"</font></td>
        </tr>
        <tr>
          <td vAlign="top" width="50%" bgColor="#3399CC"><b><font face="Verdana, Arial, Helvetica, sans-serif" color="#ffffff" size="2">Prof.ssa
            Chiara Saraceno</font></b></td>
          <td width="50%" bgColor="#c0c0c0"><font face="Verdana, Arial, Helvetica, sans-serif" size="2">Professoressa
            di Sociologia della Famiglia e Pari Opportunitą presso l'Universitą
            di Torino</font></td>
        </tr>
        <tr>
          <td vAlign="top" width="50%" bgColor="#3399CC"><b><font face="Verdana, Arial, Helvetica, sans-serif" color="#ffffff" size="2">Prof.
            Gianfranco Viesti</font></b></td>
          <td width="50%" bgColor="#c0c0c0"><font face="Verdana, Arial, Helvetica, sans-serif" size="2">Professore
            di Economia Internazionale presso l'Universitą di Bari</font></td>
        </tr>
        <tr>
          <td vAlign="top" width="50%" bgColor="#3399CC"><b><font face="Verdana, Arial, Helvetica, sans-serif" color="#ffffff" size="2">Prof.
            Stefano Zamagni</font></b></td>
          <td width="50%" bgColor="#c0c0c0"><font face="Verdana, Arial, Helvetica, sans-serif" size="2">Professore
            di Economia Politica presso l'Universitą di Bologna</font></td>
        </tr>
      </tbody>
    </table>
  </center>
</div>
<!--#include Virtual="/strutt_coda1.asp"-->

