<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<HTML><HEAD>
<LINK REL=STYLESHEET TYPE="text/css" HREF="/fogliostile.css">
</HEAD>
<BODY>
<%
Function ChangeFromWordToHtml(StrCompleta)
	StrFinale = Replace(StrCompleta,"�", "'")
	StrFinale = Replace(StrFinale,"�", "e'")
	StrFinale = Replace(StrFinale,"�", "a'")
	StrFinale = Replace(StrFinale,"�", "o'")
	StrFinale = Replace(StrFinale,"�", "i'")
	StrFinale = Replace(StrFinale,"�", "u'")
	StrFinale = Replace(StrFinale,"�", "-")
	StrFinale = Replace(StrFinale,"�", """")
	StrFinale = Replace(StrFinale,"�", """")
	ChangeFromWordToHtml = StrFinale
End function

RecordsPerPagina = 10
Page = Request.QueryString("Page")

If Request.QueryString("aord") <> "" Then
	ArgOrder = Request.QueryString("aord")
	TypeOrder = Request.QueryString("tord")
Else
	ArgOrder = Request.Form("selectSort")
	TypeOrder = Request.Form("selectOrder")
End if 

SortBy = ArgOrder & TypeOrder

If TypeOrder = "[a]" Then
	NameTypeOrder = "Ascendente"
Else
	NameTypeOrder = "Discendente"
End if

Select Case ArgOrder
Case "Rank"
  NameArgOrder = "Posizione"
Case "DocTitle"
  NameArgOrder = "Titolo"
Case "Size" 
  NameArgOrder = "Dimensione"
Case "DocAuthor"
  NameArgOrder = "Autore"
End select

If Request.Form("SrchStrCtrl") <> "" Then
CompSearch = Request.Form("SrchStrCtrl")
Else
CompSearch = Request.QueryString("search")
End if 
Response.Write(CompSearch & vbcrlf)
'si crea un'istanza della classe Query
Dim Q, RS
Set Q = Server.CreateObject("ixsso.Query")
Q.SortBy = SortBy
'si fissa come criterio di ordinamento la raggiungibilit�, discendente
Q.Columns = "DocTitle, FileName, vpath, Size, Characterization, DocAuthor"
' si specificano le propriet� dei documenti che vogliamo estrarre:
' il nome del file, il suo virtual path, il tasso di raggiungibilit�.
Q.Catalog = "PlavoroDoc" 
' si definisce il catalogo su cui ricercare
Q.Query = CompSearch
'Q.MaxRecords = 100 
' si definisce il numero massimo di documenti che vogliamo siano estratti
On Error resume next
Set RS = Q.CreateRecordSet("nonsequential")
If Err <> 0  Then
	Response.Write ("ERRNUM: " & Err.number & " ErrDesc: " & Err.description)
	Response.end
End if
On Error Goto 0
' viene effettuata la ricerca e viene creato il recordset dei risultati


If Not RS.Eof Then ' sono stati trovati degli elementi
   NextRecordNumber = 1
%>
  <!-- mostra i risultati -->
  Documenti da <%=ind+1%>  a 
  <%
	If RecordsPerPagina < RS.RecordCount Then 
		If Page > 1 Then
			response.write RecordsPerPagina * Page
		Else
			response.write RecordsPerPagina
		End if
	Else
	response.write RS.RecordCount
	End if
	%> su <%= RS.RecordCount %> in ordine <%=NameTypeOrder%> per <%=NameArgOrder%>.
	</font>
	<table width="630">
	<tr class="tbllabel1">
	<td width="20">&nbsp;</td>
	<td width="550"><b>Nome del Documento</b></td>
	<td width="60"><b>Size</b></td>
	</tr>
	<% 
	'setto il NextRecordNumber
	NextRecordNumber = 1 
	For i = 1 to RecordsPerPagina
		Do while (Not RS.Eof) and (NextRecordNumber <= RecordsPerPagina)
		%>
        <tr class="tblsfondo">
			<td class="tbltext" width="20" align=center><%= ind + NextRecordNumber %></td>
			<td class="tbltext" width="550">
			<a target="_blank" href="<%'=Replace(RS("Path"),lCase(Server.MapPath("\")),"")%> <%=RS("vpath")%>" ><%= RS("Filename") %></a></td>
			<%
			If (CLng(RS("Size")) > 1024) then
				DimDoc = Round(CLng(RS("Size"))/1024,1)
				StrDimDoc = DimDoc & " Kb"
			Else
				DimDoc = CLng(RS("Size"))
				StrDimDoc = DimDoc & " bytes"
			End if
			%>
			<td class="tbltext" width="60"><%= StrDimDoc %></td>
		</tr>
		<tr class="tblsfondo">
			<td colspan=3 class="tbltext" width="630">
			<%
			TitoloFinale = Replace(RS("DocTitle"),"�", "'")
			If RS("DocTitle")<> "" Then
			'Response.Write("<B>Titolo:" & ChangeFromWordToHtml(RS("DocTitle")) & "</B><br>")
			Response.Write("<B>Titolo:" & RS("DocTitle") & "</B><br>")
			Else
			Response.Write("<B>Titolo: -- </B><br>")
			End if
			If RS("DocAuthor")<> "" Then
			Response.Write("<B>Autore:" & RS("DocAuthor") & "</B><br>")
			Else
			Response.Write("<B>Autore: -- </B><br>")
			End if
			Response.Write(ChangeFromWordToHtml(RS("Characterization"))) & "............"
			Response.Write RS("Characterization") & "............"
			%></td>
		</tr>
		<% 
		RS.MoveNext
		NextRecordNumber = NextRecordNumber +1
		Loop
	Next
Else
	Response.Write "Nessun Record Trovato"
End if


%>
</table>
<%
RS.Close
set RS = nothing
%>
</BODY></HTML>
