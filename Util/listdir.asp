<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->

<html>
<title>Files</title>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<LINK REL=STYLESHEET TYPE="text/css" HREF="/fogliostile.css">
</head>
<body>

<div align="center">
	<br><table width="570px" border=0 cellspacing=2 cellpadding=1 >
		<tr> 
			<td colspan=2 align=middle background=/images/sfmnsmall.jpg>
			<b class="titolo">List Files</b>
			</td>
		</tr>
	</table>
	<br>
<%

' DisplayFileName
Sub DisplayFileName(dirfile,fhandle,frame)
Dim newgif,linktarget
Dim fsize

	
	if fhandle.name="listdir.asp" then
	else
	b = ""
	If len(fhandle.name)>4 Then b = Ucase(Right(fhandle.name,4))
	If Left(b,1) = "." Then b = Right(b,3)
	
	Select Case b
	
	Case "TXT","DAT"	'extension files recognized

	If fhandle.size<10240 Then
		If fhandle.size=0 Then
			fsize = "0"
		Else
			fsize = FormatNumber(fhandle.size,0,0,-2)
		End If
	Else
		fsize = FormatNumber((fhandle.size+1023)/1024,0,0,-2) & "K"
	End If
	response.write "<TR>" & VBCRLF
	tstr = "<FONT FACE=" & gblFace & " SIZE=2> " & MockIcon("view") & " <A HREF='" & LCase(fhandle.name) & "' STYLE=""{text-decoration:none}"">" & LCase(fhandle.name) & "</a></FONT>"
	response.write "<TD VALIGN=""TOP"" BGCOLOR=" & gblReverse & ">" & Tstr & "</TD>" & VBCRLF
	response.write "<TD VALIGN=""TOP"" BGCOLOR=" & gblReverse & "><FONT FACE=" & gblFace & " SIZE=1>" & FormatDateTime(fhandle.datelastmodified,0) & "</FONT></TD>" & VBCRLF
	response.write "<TD VALIGN=""TOP"" BGCOLOR=" & gblReverse & "><FONT FACE=" & gblFace & " SIZE=1>" & fsize & " bytes</FONT></TD>" & VBCRLF
	response.write "</TR>" & VBCRLF	

	End Select
	
	end if 'fileman.asp

End Sub 'DisplayFileName

Function MockIcon(txt)
Dim tstr,d

	'Sorry, mac/linux users.
	tstr = "<FONT FACE=""WingDings"" SIZE=4 COLOR=" & gblGreen & ">"
	Select Case Lcase(txt)
	Case "bmp","gif","jpg","tif","jpeg","tiff"
		d = 176
	Case "doc"
		d = 50
	Case "exe","bat","bas","c","src"
		d = 255
	Case "file"
		d = 51
	Case "fldr"
		d = 48
	Case "htm","html","asa","asp","cfm","php3"
		d = 182
	Case "pdf"
		d = 38
	Case "txt","ini"
		d = 52
	Case "xls"
		d = 252
	Case "zip","arc","sit"
		d = 59
	Case "newicon"
		tstr = "<FONT TITLE=""Questo documento � stato modificato negli ultimi 14 giorni."" FACE=""WingDings"" SIZE=4 COLOR=" & gblRed & ">"
		d = 171
	Case "view"
		d = 52
	Case "edit"
		d = asc("?")
	Case "conv"
		d = 220
	Case Else
		d = 51
	End Select
	tstr = tstr & Chr(d) & "</FONT>"
	MockIcon = tstr
End Function 'mockicon


'******************MAIN


	Dim gblScriptName
	Dim gblRoot
	Dim fsDir,FileList,fn

	Dim gblNow 'server may not be local time
	gblNow = Now

	Dim gblFace,gblColor	'needs three quotes
	gblFace = """Verdana, Arial, Helvetica, sans-serif"""
	gblColor = """#008caa"""

	Dim gblRed
	gblRed = """#FF0000"""
	
	Dim gblGreen
	gblGreen = """#003843"""

	Dim gblReverse
	gblReverse = """#E0E0E0"""
	
	gblScriptName = Request.ServerVariables("Script_Name")
	gblScriptName = Mid(gblScriptName,InstrRev(gblScriptName,"/") + 1)
	gblRoot = Replace(Request.ServerVariables("Script_Name"),"/" & gblScriptName,"")
	
	fsDir=Server.MapPath(gblRoot)

	Set fso = CreateObject("Scripting.FileSystemObject")

	Set f = fso.GetFolder(fsDir)
	Set FileList = f.Files
	a = 0
	
	response.write "<TABLE ALIGN=CENTER BORDER=0 CELLPADDING=2 CELLSPACING=3 WIDTH=""90%"">"
	response.write "<TR><TD VALIGN=""BOTTOM""><FONT FACE=" & gblFace & " COLOR=" & gblGreen & " SIZE=1><B>NOME DOCUMENTO</B></FONT></TD>" & VBCRLF
	response.write "<TD VALIGN=""BOTTOM""><FONT FACE=" & gblFace & " COLOR=" & gblGreen & " SIZE=1><B>ULTIMA MODIFICA</B></FONT></TD>" & VBCRLF
	response.write "<TD VALIGN=""BOTTOM""><FONT FACE=" & gblFace & " COLOR=" & gblGreen & " SIZE=1><B>DIMENSIONE DEL FILE</B></FONT></TD>" & VBCRLF
	response.write "</TR>" & VBCRLF
	
	For Each fn in FileList
		emptyDir = FALSE
		DisplayFileName "DIR",fn,frame
	Next 'fn

	response.write "</TABLE>" & VBCRLF
	

%>

</div>
</body>
</html>
