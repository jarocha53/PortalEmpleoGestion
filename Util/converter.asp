<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%response.buffer=true%>
<!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML//EN">
<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; CHARSET=iso-8859-1">
<HTML>
<link rel="stylesheet" href="/fogliostile.css" type="text/css">
<HEAD>
<TITLE>Convert DOC/RTF->HTML/RTF (c) STE Consulting</TITLE>
</HEAD>
<BODY BGCOLOR=#FFFFFF>
<p> 
<!--#include virtual="/util/wordlib.asp"-->
<%
LOCAL_ROOT		= "d:\sviluppo\home_design_81\downloads\"
LOCAL_ROOT_IN		= "d:\sviluppo\home_design_81\downloads\"
LOCAL_ROOT_OUT		= "d:\sviluppo\home_design_81\downloads\"
URL_ROOT		= "/downloads"
URL_ROOT_OUT		= "/downloads"

f=Request.QueryString("f")
d=Request.QueryString("d")

if (len (f)>1) then
LOCAL_ROOT_IN=d
LOCAL_ROOT_OUT=d
FileName=f
set regEx = new regExp
regEx.Pattern = ".doc"   ' Set pattern.
regEx.IgnoreCase = True   ' Set case insensitivity.
regEx.Global = True   ' Set global applicability.
Fileout = regEx.Replace (f,"_doc.htm")

CreateNewHTML "DOC2000HTML",LOCAL_ROOT,LOCAL_ROOT_IN,LOCAL_ROOT_OUT,URL_ROOT,f,fileout
%> 

<input language=javascript onload="window.close(self);parent.history.go(-1);" type=image src="/images/pmeditor/close.gif" alt="Close Windows" name=Close

<%
end if

dim pathInfo
dim rbchoice 
pathInfo = Request("source")

if pathInfo = "RadioButton" then
	rbchoice = Request.Form("RadioButtonChoice")
	FileName = Request.Form("FileName")
	output = Request.Form("output")
	rbchoice = Request.Form("RadioButtonChoice")
	CreateNewHTML rbchoice,LOCAL_ROOT,LOCAL_ROOT_IN,LOCAL_ROOT_OUT,URL_ROOT,FileName,output
end if
ScriptPath = "http://" & Request.ServerVariables("SERVER_NAME")  & Request.ServerVariables("PATH_INFO")
ScriptPath = Request.ServerVariables("PATH_INFO")
Response.Write "<FORM ACTION='" & ScriptPath & "?source=RadioButton' METHOD='POST'>"
%>
</p>
<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" class="titolobianco" bgcolor="#008caa" height="35">
  <tr> 
    <td> 
      <div align="center"> Convertitore DOC/RTF in HTML/RTF</div>
    </td>
  </tr>
</table>
<br>
<table width="90%" border="0" cellspacing="5" cellpadding="0" align="center" class="tbltext">
  <tr> 
    <td width="36%"> 
      <div align="center">Seleziona il file nella lista</div>
    </td>
    <td width="31%"> 
      <div align="center"> 
        <select name="FileName" size=3  >
          <% ShowFolderList(LOCAL_ROOT_IN) %>
        </select>
      </div>
    </td>
    <td width="33%"> 
      <div align="center"> 
        <input type=SUBMIT value="Convertire" name="SUBMIT">
      </div>
    </td>
  </tr>
  <tr> 
    <td width="36%"> 
      <div align="center">Nome del file convertito</div>
    </td>
    <td width="31%"> 
      <div align="center"> 
        <input name="output" type=EDIT value="output.htm">
      </div>
    </td>
    <td width="33%"> 
      <div align="center"><a href="<%=URL_ROOT_OUT%>" class="tbltext">Vedere la cartella 
        di destinazione</a> </div>
    </td>
  </tr>
</table>
<p align="center"><span class="tbltext">Seleziona una opzione e clica sul pulsante 
  &quot;Convertire&quot; </span></p>
<table width="50%" border="1" cellspacing="1" cellpadding="1" align="center" class="tbltext">
  <tr> 
    <td>Convertire DOC in MS XHTML</td>
    <td> 
      <input name="RadioButtonChoice" type=RADIO value="DOC2HTML">
    </td>
  </tr>
  <tr> 
    <td>Convertire DOC in Compresso XHTML</td>
    <td> 
      <input name="RadioButtonChoice" type=RADIO value="DOC2HTMLCOMP">
    </td>
  </tr>
  <tr> 
    <td>Convertire DOC in HTML</td>
    <td> 
      <input name="RadioButtonChoice" type=RADIO value="DOC2000HTML" checked>
    </td>
  </tr>
  <tr> 
    <td>Convertire DOC in RTF</td>
    <td> 
      <input name="RadioButtonChoice" type=RADIO value="DOC2RTF">
    </td>
  </tr>
  <tr> 
    <td>Convertire RTF in HTML</td>
    <td> 
      <input name="RadioButtonChoice" type=RADIO value="RTF2HTML">
    </td>
  </tr>
</table>
<p align="center">
</FORM>
</p>
</BODY>
</HTML>
