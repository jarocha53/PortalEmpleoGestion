<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include virtual="/util/portallib.asp"-->
<%

'PQ 30/09/2001 
function EX_valida_link()
	dim ret
	ret = false

	Set Conn = Server.CreateObject("ADODB.Connection")
	Conn.Open StringConn
	
	dtOdierna = "to_date('" & day(now) & "/" & month(now()) & "/" & year(now()) & "','dd/mm/yyyy')"

	sql = "select ut.login from componenti_classe cc, classe cl, edizione ed, utente ut, sess_form sf "
	sql = sql + " where cl.id_edizione = sf.id_edizione and	ut.creator = cc.id_persona "
	sql = sql + " and cc.id_classe = cl.id_classe and sf.cod_sessione = 'ORI' and "
	sql = sql + dtOdierna + " between sf.dt_inisess and sf.dt_finsess and ut.login like '" & Session("login") & "'"
	
'PL-SQL * T-SQL  
UCASESQL = TransformPLSQLToTSQL (UCASESQL) 
	set rcs = Conn.Execute(ucase(sql))
	
	if not rcs.EOF  Then
		ret = true
	end if

	Set rs=nothing
	Conn.Close
	Set Conn=nothing
	EX_valida_link=ret

end function

' verifica se abilitare la voce di menu relativa al questionario di gradimento per l'orientamento
function check_quegrador()
	dim ret
	ret = false

	Set Conn = Server.CreateObject("ADODB.Connection")
	Conn.Open StringConn

	dtOdierna = "to_date('" & day(now) & "/" & month(now()) & "/" & year(now()) & "','dd/mm/yyyy')"
	
	sql = "select ut.login from componenti_classe cc, classe cl, edizione ed, utente ut, sess_form sf "
	sql = sql + " where cl.id_edizione = sf.id_edizione and	ut.creator = cc.id_persona "
	sql = sql + " and cc.id_classe = cl.id_classe and sf.cod_sessione = 'ORQ' and "
	sql = sql + dtOdierna +" between sf.dt_inisess and sf.dt_finsess and ut.login like Upper('" & Session("login") & "')"
	
'PL-SQL * T-SQL  
UCASESQL = TransformPLSQLToTSQL (UCASESQL) 
	set rcs = Conn.Execute(ucase(sql))
	
	if not rcs.EOF  Then
		ret = true
	end if
	
	'if not rcs.EOF  Then
	'	ret = true
	'end if

	Set rs=nothing
	Conn.Close
	Set Conn=nothing
	check_quegrador=ret

end function

function check_quegradfpq()
	dim ret
	ret = false

	Set Conn = Server.CreateObject("ADODB.Connection")
	Conn.Open StringConn

	dtOdierna = "to_date('" & day(now) & "/" & month(now()) & "/" & year(now()) & "','dd/mm/yyyy')"
	
	sql = "select ut.login from componenti_classe cc, classe cl, edizione ed, utente ut, sess_form sf "
	sql = sql + " where cl.id_edizione = sf.id_edizione and	ut.creator = cc.id_persona "
	sql = sql + " and cc.id_classe = cl.id_classe and sf.cod_sessione = 'ORQ' and "
	sql = sql + dtOdierna +" between sf.dt_inisess and sf.dt_finsess and ut.login like '" & Session("login") & "'"
	
'PL-SQL * T-SQL  
UCASESQL = TransformPLSQLToTSQL (UCASESQL) 
	set rcs = Conn.Execute(ucase(sql))
	
	if not rcs.EOF  Then
		ret = true
	end if

	Set rs=nothing
	Conn.Close
	Set Conn=nothing
	check_quegradfpq=ret

end function

function check_quegradfp()
	dim ret
	ret = false

	Set Conn = Server.CreateObject("ADODB.Connection")
	Conn.Open StringConn

	dtOdierna = "to_date('" & day(now) & "/" & month(now()) & "/" & year(now()) & "','dd/mm/yyyy')"
	
	sql = "select ut.login from componenti_classe cc, classe cl, edizione ed, utente ut, sess_form sf "
	sql = sql + " where cl.id_edizione = sf.id_edizione and	ut.creator = cc.id_persona "
	sql = sql + " and cc.id_classe = cl.id_classe and sf.cod_sessione in('IFP', 'EFP') and "
	sql = sql + dtOdierna +" between sf.dt_inisess and sf.dt_finsess and ut.login like '" & Session("login") & "'"
	
'PL-SQL * T-SQL  
UCASESQL = TransformPLSQLToTSQL (UCASESQL) 
	set rcs = Conn.Execute(ucase(sql))
	
	if not rcs.EOF  Then
		ret = true
	end if

	Set rs=nothing
	Conn.Close
	Set Conn=nothing
	check_quegradfp=ret

end function


'abilitazione voce di menu per questionario gradimento informatica in presenza
function check_quegradIFQ()
	dim ret
	ret = false

	Set Conn = Server.CreateObject("ADODB.Connection")
	Conn.Open StringConn

	dtOdierna = "to_date('" & day(now) & "/" & month(now()) & "/" & year(now()) & "','dd/mm/yyyy')"
	
	sql = "select ut.login from componenti_classe cc, classe cl, edizione ed, utente ut, sess_form sf "
	sql = sql + " where cl.id_edizione = sf.id_edizione and	ut.creator = cc.id_persona "
	sql = sql + " and cc.id_classe = cl.id_classe and sf.cod_sessione = 'IFQ' and "
	sql = sql + dtOdierna +" between sf.dt_inisess and sf.dt_finsess and ut.login like Upper('" & Session("login") & "')"
	
'PL-SQL * T-SQL  
UCASESQL = TransformPLSQLToTSQL (UCASESQL) 
	set rcs = Conn.Execute(ucase(sql))
	
	if not rcs.EOF  Then
		ret = true
	end if

	Set rs=nothing
	Conn.Close
	Set Conn=nothing
	check_quegradIFQ=ret

end function

'abilitazione voce di menu per questionario gradimento inglese in presenza
function check_quegradEFQ()
	dim ret
	ret = false

	Set Conn = Server.CreateObject("ADODB.Connection")
	Conn.Open StringConn

	dtOdierna = "to_date('" & day(now) & "/" & month(now()) & "/" & year(now()) & "','dd/mm/yyyy')"
	
	sql = "select ut.login from componenti_classe cc, classe cl, edizione ed, utente ut, sess_form sf "
	sql = sql + " where cl.id_edizione = sf.id_edizione and	ut.creator = cc.id_persona "
	sql = sql + " and cc.id_classe = cl.id_classe and sf.cod_sessione = 'EFQ' and "
	sql = sql + " cc.id_persona > 1 and "	
	sql = sql + dtOdierna +" between sf.dt_inisess and sf.dt_finsess and ut.login like Upper('" & Session("login") & "')"
	
'PL-SQL * T-SQL  
UCASESQL = TransformPLSQLToTSQL (UCASESQL) 
	set rcs = Conn.Execute(ucase(sql))
	
	if not rcs.EOF  Then
		ret = true
'		invioComunicazione (Conn)
	end if

	Set rs=nothing
	Conn.Close
	Set Conn=nothing
	check_quegradEFQ=ret

end function

' abilitazione voce di menu per UTENTI AMMESSI NON ANCORA IN CLASSE
' utilizzata per abilitare la voce di menu "IL MIO NETWORK" x gli studenti che fanno parte di
' una qualsiasi classe e che hanno la data di chiusura ruolo non valorizzata
' NB non si tiene conto di QUANDO gli studenti sono in classe
function check_InClasse()
	dim ret
	ret = false

	Set Conn = Server.CreateObject("ADODB.Connection")
	Conn.Open StringConn

	dtOdierna = "to_date('" & day(now) & "/" & month(now()) & "/" & year(now()) & "','dd/mm/yyyy')"
	
	sql = "select ut.login from componenti_classe cc, utente ut  "
	sql = sql + " where ut.creator = cc.id_persona "
	sql = sql + " and cc.dt_ruoloal is null " 
	sql = sql + " and ut.login like Upper('" & Session("login") & "')"
	
'PL-SQL * T-SQL  
UCASESQL = TransformPLSQLToTSQL (UCASESQL) 
	set rcs = Conn.Execute(ucase(sql))
	
	if not rcs.EOF  Then
		ret = true
	end if

	Set rs=nothing
	Conn.Close
	Set Conn=nothing
	check_InClasse=ret

end function

'abilitazione voce di menu "MODULI FORMATIVI" per utenti in formazione in presenza o a distanza
function check_InFormazione()
	dim ret
	ret = false

	Set Conn = Server.CreateObject("ADODB.Connection")
	Conn.Open StringConn
	
	dtOdierna = "to_date('" & day(now()) & "/" & month(now()) & "/" & year(now()) & "','dd/mm/yyyy')"
	
	sql = "select ut.login from componenti_classe cc, classe cl, edizione ed, utente ut, sess_form sf "
	sql = sql + " where cl.id_edizione = sf.id_edizione and cl.id_edizione = ed.id_edizione and	ut.creator = cc.id_persona "
	sql = sql + " and cc.id_classe = cl.id_classe and sf.cod_sessione in ('IFP','EFP','IFD','EFD') AND "
	sql = sql + " ut.creator > 1 AND "	
	sql = sql + " TO_DATE(TO_CHAR(SYSDATE, 'DDMMYYYY'), 'DDMMYYYY') between sf.dt_inisess and sf.dt_finsess and ut.login like '" & Session("login") & "'"
'sql = sql + dtOdierna + " between sf.dt_inisess and sf.dt_finsess and ut.login like '" & Session("login") & "'"	
	
'PL-SQL * T-SQL  
UCASESQL = TransformPLSQLToTSQL (UCASESQL) 
	set rcs = Conn.Execute(ucase(sql))
	
	if not rcs.EOF  Then
		ret = true
	end if

	Set rs=nothing
	Conn.Close
	Set Conn=nothing
	check_InFormazione=ret

end function

' pq 30/09/2001 anche gli studenti sono nella fase di formazione in presenza devono avere
' abilitata la funzione di orientamento
function valida_link()
	dim ret
	ret = false

	Set Conn = Server.CreateObject("ADODB.Connection")
	Conn.Open StringConn
	
	dtOdierna = "to_date('" & day(now) & "/" & month(now()) & "/" & year(now()) & "','dd/mm/yyyy')"

	sql = "select ut.login from componenti_classe cc, classe cl, edizione ed, utente ut, sess_form sf "
	sql = sql + " where cl.id_edizione = sf.id_edizione and	ut.creator = cc.id_persona "
	sql = sql + " and cc.id_classe = cl.id_classe " 
	sql = sql + " and (sf.cod_sessione = 'ORI' OR sf.cod_sessione = 'IFP' OR sf.cod_sessione = 'EFP') and "
	sql = sql + dtOdierna + " between sf.dt_inisess and sf.dt_finsess and ut.login like '" & Session("login") & "'"
	
'PL-SQL * T-SQL  
UCASESQL = TransformPLSQLToTSQL (UCASESQL) 
	set rcs = Conn.Execute(ucase(sql))
	
	if  Session("Creator")=6485 or Session("Creator")=6517 or Session("Creator")=7668 or Session("Creator")=8771 or Session("Creator")=10861 then
		ret=true
	else
		if not rcs.EOF  Then
			ret = true
		end if
	end if
	
	'if not rcs.EOF  Then
	'	ret = true
	'end if

	Set rs=nothing
	Conn.Close
	Set Conn=nothing
	valida_link=ret

end function


' verifica se per la persona � gi� stata inviata la comunicazione
' return code = -1 non esiste la comunicazione
'				0 esiste il testo della comunicazione ed � stato anche inviato
'				>0 esiste il testo della comunicazione ma non 
'				  � stata inviata all'utente e il valore ritornato � ID_COMUNICAZIONE

function findComunicazione (cc)
	dim sqlI, RRC, sql1, Oggi, DtDef, idComun
	dim idPersona, sTesto, sLink, iTipoCom, DFORM
	
	findComunicazione = -1

	on error resume next
	
	OGGI = "TO_DATE(TO_CHAR( SYSDATE, '''DDMMYYYY'''), '''DDMMYYYY''')"
	
	iTipoCom = 2
	sRuolo = "DI"
	sOggetto = "BONUS"

	set RRC=Server.CreateObject("ADODB.Recordset")
	sql1 = "SELECT ID_COMUNICAZIONE from COMUNICAZIONE "
    sql1 = sql1 & " where tipo_com = " & iTipoCom
    sql1 = sql1 & " and destinatari = 'QBONUS' "  
	sql1 = sql1 & " and OGGETTO = '" & sOggetto & "'"

	' verifica esistenza comunicazione relativa al bonus	
	RRC.open sql1, CC, 3
	If not RRC.EOF then
		IDCOMUN = RRC.Fields(0)
		findComunicazione = idComun
	else
		RRC.CLOSE
		set rrc = nothing
		exit function
	end if

	idPersona = session("creator")
	
	sqlCom = "select id_comunicazione from COM_PERSONA "
	sqlCom = sqlCom & " where id_comunicazione = " & idcomun
	
	rrc.open sqlCom,cc,3

	IF rrc.eof then
		rrc.close
		set rrc = nothing
		exit function
	end if
	
	findComunicazione = 0

end function ' findComunicazione



function invioComunicazione (CC)
	dim sqlI, RRC, sql1, Oggi, DtDef, idComun
	dim idPersona, sTesto, sLink, iTipoCom, DFORM
	dim iCom

	
	iCom =  findComunicazione (cc)
	' se la comunicazione � gi� stata inviata esco
	if iCom = 0 then exit function
	
	on error resume next
	
	OGGI = "TO_DATE(TO_CHAR( SYSDATE, '''DDMMYYYY'''), '''DDMMYYYY''')"
	
	CC.BeginTrans

	if iCom = -1 then
	
		idPersona = 0
		sTesto = "Si comunica che � a partire da Oggi � attivo il questionario per il bonus"
		sLink = ""
		iTipoCom = 2
		sRuolo = "DI"
		sOggetto = "BONUS"
						
		sqlI="INSERT INTO COMUNICAZIONE (MITTENTE, TESTO_COM, LINK_COM, TESTO_LINK_COM, TIPO_COM, DT_INS_COM, DT_INVIO_COM, DESTINATARI, COD_RUOLO, OGGETTO, DT_TMST)"
		sqlI = sqlI & " VALUES (" & idPersona & ",'" & sTesto & "','" & sLink & "','" & sLink & "',"& iTipoCom & "," & OGGI & "," & OGGI & ",'QBONUS','" & sRUOLO & "','" & sOGGETTO & "'," & OGGI & ")"
		'Response.Write sqlI  & "<br>"

'PL-SQL * T-SQL  
SQLI = TransformPLSQLToTSQL (SQLI) 
		cc.execute sqlI					
	
		IF err.number <> 0 then
			CC.RollbackTrans
			exit function
		end if

		set RRC=Server.CreateObject("ADODB.Recordset")
	
		sql1 = "SELECT ID_COMUNICAZIONE from COMUNICAZIONE "
		sql1 = sql1 & " where DT_INS_COM = " & OGGI
		sql1 = sql1 & " and tipo_com = " & iTipoCom
		sql1 = sql1 & " and destinatari = 'QBONUS' "  
		sql1 = sql1 & " and OGGETTO = '" & sOggetto & "'"
	
		RRC.open sql1, CC, 3
		If not RRC.EOF then
			IDCOMUN = RRC.Fields(0)
		end if
		RRC.CLOSE
	
		set rrc =nothing
	else
		IdComun = iCom
	end if

	'DTDEF = "01/01/1900 01:01:01 AM"
	DTDEF = "NULL"
	
	idPersona = session("creator")
	
	sqlI="INSERT INTO COM_PERSONA (ID_COMUNICAZIONE,ID_PERSONA, DT_RICEZIONE, FL_DELETE , DT_TMST)"
	sqlI = sqlI & " VALUES (" & IDCOMUN & "," & IDPERSONA & "," & DTDEF & ",'N'," & OGGI & ")"
	
'PL-SQL * T-SQL  
SQLI = TransformPLSQLToTSQL (SQLI) 
	cc.execute sqlI

	IF err.number <> 0  then
		CC.RollbackTrans
		on error goto 0
		exit function
	end if
		
	CC.CommitTrans	
	on error goto 0	

end function



'abilitazione voce di menu per questionario bonus inglese in presenza
function check_queBonus()
	dim ret
	ret = false

	Set Conn = Server.CreateObject("ADODB.Connection")
	Conn.Open StringConn

	dtOdierna = "to_date('" & day(now) & "/" & month(now()) & "/" & year(now()) & "','dd/mm/yyyy')"
	
	sql = "select ut.login from componenti_classe cc, classe cl, edizione ed, utente ut, sess_form sf "
	sql = sql + " where cl.id_edizione = sf.id_edizione and	ut.creator = cc.id_persona "
	sql = sql + " and cc.id_classe = cl.id_classe and ((sf.cod_sessione = 'EFP' and "
	sql = sql + " cc.id_persona > 1 and "	
	sql = sql + dtOdierna +" = sf.dt_finsess) OR "
	sql = sql + "(sf.cod_sessione = 'BON' and cc.id_persona > 1 and "
	sql = sql + dtOdierna + " BETWEEN sf.dt_inisess AND sf.dt_finsess))"
	sql = sql + "and ut.login like Upper('" & Session("login") & "')"

'PL-SQL * T-SQL  
UCASESQL = TransformPLSQLToTSQL (UCASESQL) 
	set rcs = Conn.Execute(ucase(sql))
	
	if not rcs.EOF  Then
		ret = true
'		invioComunicazione (Conn)
	end if

	Set rs=nothing
	Conn.Close
	Set Conn=nothing
	check_queBonus=ret

end function

%>












