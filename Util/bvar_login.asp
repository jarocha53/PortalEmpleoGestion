<html>

<head>
<meta http-equiv="Content-Type"
content="text/html; charset=iso-8859-1">
<meta name="GENERATOR" content="Microsoft FrontPage Express 2.0">
<title>SOLICITUD DE CLAVE</title>
</head>
<body background="../images/sfondo1.gif">

<table border="0" width="570" style="WIDTH: 570px">
    <tr>
        <td align="middle" colspan="2" background="../images/sfmnsmall.jpg">
    <script language="Javascript">
	<!--#include file ="../include/ControlCodFisc.inc"-->
	<!--#include file ="../include/ControlDate.inc"-->
	<!--#include file ="../include/ControlNum.inc"-->
	
	
	function checkField(frmpw) 
	{
     	if (frmpw.pwcognome.value == "") {
			alert("El apellido es obligatorio");
			frmpw.pwcognome.focus();
			return(false);
		}	
		if (frmpw.pwnome.value == "") {
		        alert("El nombre es obligatorio"); 
			frmpw.pwnome.focus();
			return(false);
		}

     	if (frmpw.pwdata.value == "") {
			alert("La fecha de nacimiento es obligatoria");
			frmpw.pwdata.focus();
			return(false);
		}
		
		//CONTROLLO DELLA DATA //

			
		dt = frmpw.pwdata.value
		if (!ValidateInputDate(dt)){
			frmpw.pwdata.focus() 
			return(false);
		}

		if (frmpw.pwcf.value == "") {
		    alert("El Cuil es obligatorio"); 
			frmpw.pwcf.focus()
			return(false);
		}

		cf = document.frmpw.pwcf.value.toUpperCase() 
		if (!ControllCodFisc(dt,cf,'x')) {
		    alert("El Cuil es erroneo"); 
			frmpw.pwcf.focus() 
			return(false);
		}

		if (document.frmpw.pwtelefono.value == "") {
		    alert("El campo telefono es obligatorio"); 
			document.frmpw.pwtelefono.focus();
			return(false);
		}

		 if(document.frmpw.pwtelefono.value < "0" || document.frmpw.pwtelefono.value> "9")
		  {
		  document.frmpw.pwtelefono.focus();
		  alert("Ingrese un valor num�rico para el tel�fono. ");
		  return(false);
		  } 

		var anyString = document.frmpw.pwemail.value;
	
		for ( var i=0; i<=anyString.length-1; i++ ) {
		
		 if ( ((anyString.charAt(i) >= "A") && (anyString.charAt(i) <= "Z")) || 
	        ((anyString.charAt(i) >= "a") && (anyString.charAt(i) <= "z")) || 
	        (anyString.charAt(i) >= "0") && (anyString.charAt(i) <= "9") ||
		    (anyString.charAt(i) == "@") || (anyString.charAt(i) == "."))
			{
			}
		else
			{		
			document.frmpw.pwemail.focus();
		 	alert("Ingrese para el campo E-MAil un valor valido");
			return(false);
			}
		
		
		}
	}
</script> <font color="#003843"
           
      face="Verdana, Arial, Helvetica, sans-serif"><strong>Solicitud de Clave</strong></font></td>
    </tr>
</table><BR><BR>

<table border="0" width="570" style="WIDTH: 570px">
<tr>
	<td colspan="2"><font color="#003843"         
      face="Verdana, Arial, Helvetica, sans-serif" size=2>Esta busqueda debe ser utilizada unicamente por aquellas personas que olvidaron su contrase�a.</font></td>
    </tr>
    <td colspan="2"><font color="#003843"         
      face="Verdana, Arial, Helvetica, sans-serif" size=2>Ingresa la misma informaci�n que ingreso durante la inscripci�n.</font></td>
    </tr>
</table>

<form method="post" name="frmpw"
 onsubmit="return checkField(this);" action='Inviopsw.asp'>
    <p>&nbsp;</p>
    <table border="0" cellpadding="0" cellspacing="0" width="570">
        <tr>
            <td valign="top" width="23"><font color="#f1f3f3"><IMG
            height=26 src="../images/angolosin.jpg" width=23></font></td>
            <td width="50%" bgcolor="#008caa">
      <P align=left><FONT color=ivory><font
            size="2" face="Verdana" ><strong>INFORMACION PERSONAL</strong></font><font
            size="2" face="Verdana" >&nbsp;</font></FONT></P></td>
            <td align="right" colspan="2" width="50%"
            bgcolor="#008caa"><font color="#f1f3f3" size="1" face="Verdana"><i>(* = campi obbligatori)</i></font></td>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="1" width="570"
    style="WIDTH: 570px">
        <tr>
            <td align="left" colspan="2" bgcolor="#008caa"><p
            align="left"><font color="#008caa"><strong>_</strong></font><font
              
      color="#ffffff" size="2" face="Verdana"><strong>Apellido*</strong></font><font
            color="#008caa" size="2" face="Verdana"><strong>_</strong></font></p>
            </td>
            <td align="left" colspan="2" width="60%"
            bgcolor="#f1f3f3"><p align="left">
          <input size="30"
            maxlength="50" name="pwcognome" style="TEXT-TRANSFORM: uppercase" >
        </p>
            </td>
        </tr>
        <tr>
            <td align="left" colspan="2" bgcolor="#008caa"><p
            align="left"><font color="#008caa"><strong>_</strong></font><font
              
      color="#ffffff" size="2" face="Verdana"><strong>Nombre*</strong></font><font
            color="#008caa" size="2" face="Verdana"><strong>_</strong></font></p>
            </td>
            <td align="left" colspan="2" width="60%"
            bgcolor="#f1f3f3"><p align="left">
          <input size="30"
            maxlength="50" name="pwnome" style="TEXT-TRANSFORM: uppercase" >
        </p>
            </td>
        </tr>
        <tr>
            <td align="left" colspan="2" bgcolor="#008caa"><p
            align="left"><font color="#008caa"><STRONG>_</STRONG></font><font
              
      color="#ffffff" size="2" face="Verdana"><STRONG>Fecha de Nacimiento* 
      </STRONG><FONT size=1> (dd/mm/aaaa)</FONT></font></p>
            </td>
            <td align="left" colspan="2" width="60%"
            bgcolor="#f1f3f3"><p align="left">
          <input size="10" maxlength="10" name="pwdata" style="TEXT-TRANSFORM: uppercase" >
          </p>
            </td>
        </tr>
        <tr>
            <td align="left" colspan="2" bgcolor="#008caa"><p
            align="left"><font color="#008caa"><strong>_</strong></font><font
              
      color="#ffffff" size="2" face="Verdana"><strong>Cuil*</strong></font><font
            color="#008caa" size="2" face="Verdana"><strong>_</strong></font></p>
            </td>
            <td align="left" colspan="2" width="60%"
            bgcolor="#f1f3f3"><p align="left">
          <input size="30" maxlength="16" name="pwcf" style="TEXT-TRANSFORM: uppercase">
        </p>
            </td>
        </tr>
        <tr>
            <td align="left" colspan="2" bgcolor="#008caa"><p
            align="left"><font color="#008caa"><strong>_</strong></font><font
              
      color="#ffffff" size="2" face="Verdana"><strong>Telefono*</strong></font></p>
            </td>
            <td align="left" colspan="2" width="60%"
            bgcolor="#f1f3f3"><p align="left">
          <input size="26"
            maxlength="20" name="pwtelefono" style="WIDTH:
            184px; HEIGHT: 22px" >
          </p>
            </td>
        </tr>
        <tr>
            <td align="left" colspan="2" bgcolor="#008caa"><p
            align="left"><font color="#008caa"><strong>_</strong></font><font
              
      color="#ffffff" size="2" face="Verdana"><strong>E-Mail</strong></font><font
            color="#008caa" size="2" face="Verdana"><strong>_</strong></font></p>
            </td>
            <td align="left" colspan="2" width="60%"
            bgcolor="#f1f3f3"><p align="left">
          <input size="25"
            maxlength="50" name="pwemail" style="WIDTH:
            180px; HEIGHT: 22px" >
        </p>
            </td>
        </tr>
        <tr>
            <td align="middle" colspan="2">&nbsp;</td>
            <td align="middle" colspan="2" width="60%">&nbsp;</td>
        </tr>
        <tr>
            <td align="left" colspan="2"><font face="Verdana">&nbsp;
            </font></td>
            <td align="left" colspan="2" width="60%"><p
            align="left"><font face="Verdana"><input
            type="submit" name="Invia" value="Richiedi"></font></p>
            </td>
        </tr>
    </table>
</form>

<p>&nbsp;</p>
<table style="WIDTH: 570px" width=570 border=0>
  
  <tr>
    <td class=Footer align=middle colSpan=3>
      <P align=center><FONT face=Arial size=2>Copyright � 2001-2003 
      <font color=#008caa><strong 
      >ejob-pl@ce</strong></font> Todos los derechos reservados</FONT></P></td></tr></table>
</body>
</html>
