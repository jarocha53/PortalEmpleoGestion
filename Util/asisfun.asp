<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include virtual="/util/portallib.asp"-->
<!-- #include virtual="/include/OpenConn.asp" -->
<%

Function asignaFunzioneGruppo (idf,idg,nv,vv,Conn)
		
		FieldStr=" (idgruppo,idfunzione)"
		ValueStr = " VALUES (" & idg & "," & idf & ")"
		
		if nv <> "" then 
			FieldStr=" (idgruppo,idfunzione,namevar,valore)"
			ValueStr = " VALUES (" & idg & "," & idf & ",'" & nv & "','" & vv & "')"
		end if
			
		SqlStr = "INSERT INTO gruppofun" + FieldStr + ValueStr
		
'PL-SQL * T-SQL  
UCASESQLSTR = TransformPLSQLToTSQL (UCASESQLSTR) 
 		set rstGruppoFun = Conn.Execute(UCase(SqlStr))

End Function

Function asignaFunzioneUtente (idf,idu,nv,vv,au,Conn)

		FieldStr=" (idutente,idfunzione,aut_log)"
		ValueStr = " VALUES (" & idu & "," & idf & ",'" & au & "')"
		
		if nv <> "" then 
			FieldStr=" (idutente,idfunzione,namevar,valore,aut_log)"
			ValueStr = " VALUES (" & idu & "," & idf & ",'" & nv & "','" & vv & "','" & au & "')"
		end if
				
		SqlStr = "INSERT INTO utefun " + FieldStr + ValueStr
		
'PL-SQL * T-SQL  
UCASESQLSTR = TransformPLSQLToTSQL (UCASESQLSTR) 
 		Set rstUteFun = Conn.Execute(UCase(SqlStr))

End Function

Function deleteFunzioneInUtente(idfun,Conn)
		
		SqlStr = "DELETE FROM utefun WHERE idfunzione="&idfun
'PL-SQL * T-SQL  
UCASESQLSTR = TransformPLSQLToTSQL (UCASESQLSTR) 
 		Set rstDelFunUte = Conn.Execute (ucase(SqlStr))

End Function

Function deleteFunzioneInGruppi(idfun,Conn)
		
		SqlStr = "DELETE FROM gruppofun WHERE idfunzione="&idfun
'PL-SQL * T-SQL  
UCASESQLSTR = TransformPLSQLToTSQL (UCASESQLSTR) 
		Set rstDelFunGru = Conn.Execute (ucase(SqlStr))
 		
End Function

Function deleteFunzione(idfun,Conn)
		
		SqlStr = "DELETE FROM funzione WHERE idfunzione="&idfun
'PL-SQL * T-SQL  
UCASESQLSTR = TransformPLSQLToTSQL (UCASESQLSTR) 
		Set rstDelFun = Conn.Execute (ucase(SqlStr))

End Function

Function CartellaGruppo(idgruppo,Conn)	

		SqlStr = "SELECT webpath FROM gruppo WHERE idgruppo="&idgruppo
'PL-SQL * T-SQL  
UCASESQLSTR = TransformPLSQLToTSQL (UCASESQLSTR) 
 		Set rs = Conn.Execute(ucase(SqlStr))
		
		path=rs(0)
		CartellaGruppo=right(path,len(path)-1)		
		
 		set rs=nothing
End Function

Function deleteGruppo(idgruppo,Conn)
		
		SqlStr = "DELETE FROM gruppo WHERE idgruppo=" & idgruppo
'PL-SQL * T-SQL  
UCASESQLSTR = TransformPLSQLToTSQL (UCASESQLSTR) 
 		Set rstGruppo = Conn.Execute (ucase(SqlStr))
 		
End Function

Function deleteCartella(cartella,Conn)
		dim nc
		
		SqlStr = "SELECT count(webpath) FROM gruppo WHERE webpath LIKE '/"&cartella&"'"
'PL-SQL * T-SQL  
UCASESQLSTR = TransformPLSQLToTSQL (UCASESQLSTR) 
 		Set rstWeb = Conn.Execute(ucase(SqlStr))
		nc = cint(rstWeb(0))
		
		if nc = 1 then 'la cartella e unica para este grupo
			
		dim patho,path
		patho = Request.ServerVariables("APPL_PHYSICAL_PATH") & mid(Session("Progetto"),2)
		path= patho & "\" & cartella
	
		Set fs = CreateObject("Scripting.FileSystemObject")
		if fs.FolderExists(path) then
   			fs.deleteFolder(path)	
   		end if
   		Set fs = Nothing
   		
   		end if
End Function

Function moveUtentiToGruppo(idgru,idnewgru,Conn)

		SqlStr = "UPDATE utente SET idgruppo="&idnewgru&" WHERE idgruppo="&idgru
'PL-SQL * T-SQL  
UCASESQLSTR = TransformPLSQLToTSQL (UCASESQLSTR) 
		Set rstUtente = Conn.Execute (ucase(SqlStr))
		
End Function

Function deleteGruppoUtenti(idgru,Conn)
		
		SQLUteFun = "delete from utefun where idutente in (select distinct(utefun.idutente) FROM utefun,utente where utente.idutente=utefun.idutente and utente.idgruppo="& Session("ck_idgruppo")& ")"
'PL-SQL * T-SQL  
UCASESQLUTEFUN = TransformPLSQLToTSQL (UCASESQLUTEFUN) 
		Set rstUteFun = Conn.execute(ucase(SQLUteFun))
		
		SQLUtente = "delete from utente where idgruppo="&idgru
'PL-SQL * T-SQL  
UCASESQLUTENTE = TransformPLSQLToTSQL (UCASESQLUTENTE) 
 		Set rstUtente = Conn.execute(ucase(SQLUtente))
End Function

Function deleteGruppoFunzioni(idgru,Conn)
		
		SqlStr = "DELETE FROM gruppofun WHERE idgruppo="&idgru
'PL-SQL * T-SQL  
UCASESQLSTR = TransformPLSQLToTSQL (UCASESQLSTR) 
		Set rstGruppoFun = Conn.Execute (ucase(SqlStr))

End Function

%>
