<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include virtual = "/util/portallib.asp"-->
<!--#include virtual = "/util/globalsub.asp"-->
<!--#include virtual = "/util/openconn.asp"-->

<%
	if ValidateService(Session("idpersona"),"Ricerca_Utenti_Amm",cc) = "false" then
		Response.Redirect "/util/error_login.asp"
	End if
%>

<%

Set conntemp = server.CreateObject("adodb.Connection")
'PL-SQL * T-SQL  
STRINGCONN = TransformPLSQLToTSQL (STRINGCONN) 
conntemp.open StringConn

%>
<script language=Javascript>

	function checkField(Form) {

		if (Form.SearchText.value == "") {

			alert("Es necesario ingresar al menos un parametro de busqueda");

			Form.SearchText.focus();

			return(false);

		}
		
		
	}		
</script>		
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>utente</title>
<base target="_self">
<link rel="stylesheet" type="text/css" href="/fogliostile.css">

</head>
<body onload="document.utente.SearchText.focus()">
<div align="center">

<table width=580 border=0 cellspacing=0 cellpadding=1>
<tr> 
	<td align=middle background=/images/sfmnsmall.jpg border=0>
	<b class="titolo">Ricerca Utenti</b>
	</td>
</tr>
</table>
<br>
<br>

<form method="GET" name="utente" target="_self" onSubmit="return checkField(this);">

<table border="0" cellspacing="0" cellpadding="0" width="570">
     <tr> 
     <td valign="top"  width="23"><IMG src="/images/angolosin.jpg" border=0></td>
     <td width="370" bgcolor="#008caa" align=left valign="middle" class="tbllabel1">Ricerca nei campi: cognome o nome o login</td>
     <td align="right" bgcolor="#008caa" valign="middle" colspan="2" width="177">
			<i><span class="tbllabel1">(* = dati obbligatori)</span></i>
		</td>
     
     </tr>
</table>



<table class="tblsfondo" border="0" cellpadding="2" cellspacing="2" width="570">


</div>

</tr>

<tr><td colspan='2' height="25">
<tr>

</tr>

<tr>
   <td height="25" class="tbllabel1" align="left">&nbsp;Gruppo Appartenenza</td>
   <td><input type="text" class="textred" name="gruppo" value="Utente" readonly="true"></td>
</tr>

<tr>
   <td height="25" class="tbllabel1" align="left">&nbsp;Parametro di Ricerca*</td>
   <td><input type="text" name="SearchText"value="<%=Request.querystring("Searchtext")%>" size="30"></td>
</tr>
  
  <tr>
  <tr>
  </table>
<td>
<br>
<input type="submit" value="Ricerca">

</td>
</tr>

<input type="hidden" name="cmd" value="Find">
</form>
<%

Dim actPagina
Dim totPagina
Dim tamPagina		
Dim totRecord

tamPagina=20

if Request.Querystring("pagina")="" then
	actPagina=1
else
	actPagina=Clng(Request.Querystring("pagina"))
end if

set rs=Server.CreateObject("ADODB.Recordset")
rs.CursorLocation = 3

If Request.querystring("Searchtext")<>"" Then
 If CheckIsNumber(Request.querystring("Searchtext")) = True Then
  TmpNum = Replace(Trim(Request.querystring("Searchtext")), ",", ".")
 Else
  TmpNum = "0"
 End If
 
 SqlStr = "SELECT idpersona, cognome, nome, login, email, password, idgruppo, creator " & _
          "FROM utente " & _
          "where idgruppo IN (SELECT GRUPPO.idgruppo FROM gruppo " & _
		  "WHERE desgruppo IN ('UTENTE BASE','UTENTE AMMESSO', 'UTENTE NON AMMESSO'))" & _
          " And (" & _
		  "(cognome LIKE '%"+FixDbFieldSave(Request.querystring("Searchtext"))+"%') OR " & _
		  "(nome LIKE '%"+FixDbFieldSave(Request.querystring("Searchtext"))+"%') OR " & _
		  "(login LIKE '%"+FixDbFieldSave(Request.querystring("Searchtext"))+"%') OR " & _
		  "(email LIKE '%"+FixDbFieldSave(Request.querystring("Searchtext"))+"%')) "

		SqlStr = SqlStr & " order by cognome,nome"
 
 SqlStr=ucase(SqlStr)
 
'PL-SQL * T-SQL  
SQLSTR = TransformPLSQLToTSQL (SQLSTR) 
 'Set rs = conntemp.Execute(SqlStr)
 
 rs.PageSize=tamPagina
 rs.CacheSize=tamPagina
 
'PL-SQL * T-SQL  
SQLSTR = TransformPLSQLToTSQL (SQLSTR) 
 rs.Open SqlStr, conntemp, 1, 2
 
 totPagina=rs.PageCount
 
 if actPagina < 1 then
	actPagina = 1
 end if
 if actPagina > totPagina then
	actPagina = totPagina
 end if

 if rs.EOF <> True then
 	rs.AbsolutePage=actPagina
 end if
 totRecord=0

 If (rs.EOF=True) And (Request.Querystring("cmd")="Find") Then
 %>
 <BR>
    <table class="tblsfondo"  border="0" cellpadding="2" cellspacing="0" width="570">
	<tr> 
     <td background="/images/angolosin.jpg" height="26" width="15"></td>
     <td bgcolor="#008caa" width="555" ></td>
     
     </tr>
	
	
	<tr align=center>
	<td width="15">
	</td>
	<td class="tbltext" width="555">
	<span class="size">
		Il valore ricercato non esiste!
	</span>
	</td>
	</tr>
	</table>
 <br>
 
  <%
  Else
  %>
  
<table class="tblsfondo"  border="0" cellpadding="2" cellspacing="0" width="570">
     <tr> 
     <td background="/images/angolosin.jpg" height="26" width="15"></td>
     <td bgcolor="#008caa" align=left valign="middle" class="tbllabel1">Cognome</td>
     <td bgcolor="#008caa" align=left valign="middle" class="tbllabel1">Nome</td>
     <td bgcolor="#008caa" align=left valign="middle" class="tbllabel1">Email</td>
     <td bgcolor="#008caa" align=left valign="middle" class="tbllabel1">Login</td>
     <td bgcolor="#008caa" align=left valign="middle" class="tbllabel1">Password</td>
     </tr>

<%
 End If
 
 
 
  
 While (rs.EOF <> True) And (Request.Querystring("cmd")="Find") And (totRecord < tamPagina)
  Response.write "<tr class=""tbltext"">"+CrLf
  Response.write "<td width=""20"">"+CrLf
  Response.write "</td>"
  Response.write "<td>"
  Response.write lcase(FixDbField(rs("cognome")))
  Response.write "</td>"
  Response.write "<td>"
  Response.write lcase(FixDbField(rs("nome")))
  Response.write "</td>"
  Response.write "<td>"
  Response.write lcase(FixDbField(rs("email")))
  Response.write "</td>"
  Response.write "<td>"+CrLf
  Response.write lcase(FixDbField(rs("login")))+CrLf
  Response.write "</td>"
  Response.write "<td>"
  Response.write lcase(FixDbField(rs("password")))
  Response.write "</td>"
  Response.write "</tr>"
  totRecord=totRecord + 1  
  Rs.movenext
 Wend
 rs.Close
 Set rs = Nothing
 End if
 conntemp.Close
 Set conntemp = Nothing
%>
</table>

<%
if actPagina > 1 then
	Response.Write("<A HREF=SearchUte_Amm.asp?cmd=Find&SearchText="+Request.querystring("Searchtext")+"&pagina="& actPagina-1& _
	">Pagina Precedente</A>")
end if
if actPagina > 1 and actPagina < totPagina then
	Response.Write " ------ "
end if
if actPagina < totPagina then
	Response.Write("<A HREF=SearchUte_Amm.asp?cmd=Find&SearchText="+Request.querystring("Searchtext")+"&pagina=" & actPagina+1 & _
	">Pagina Successiva</A>")
end if
%>

<br>
<br>
</body>
</html>
<!--#include virtual = "/util/closeconn.asp"-->
