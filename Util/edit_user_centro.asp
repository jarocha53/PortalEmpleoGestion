<!--#include Virtual = "/strutt_testa2.asp"-->
<!--#include Virtual ="/Include/openconn.asp"-->

<script type="text/javascript">

		//!nombres.match(/[A-Za-z ñ]+/)
	
	function validateEmail(email) {
		  var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
		  if( !emailReg.test( email ) ) {
		    return false;
		  } else {
		    return true;
		  }
	}

	function validateTextAndSpace(text){
		var textReg = /^[A-Za-z ñáéíóú]+$/i;
 		if( !textReg.test( text ) ) {
		    return false;
		} else {
			return true;
		}
	}


	$(function(){

		$( "#goToUsersList" ).button({
	      icons: {
	        primary: "ui-icon-arrowreturnthick-1-w"
	      }//,
	      //text: false
		}).click(function(){
			window.location.href = "users_list.asp"
		});




		$( "#saveNewUser" ).button({
	      icons: {
	        primary: "ui-icon-disk"
	      }//,
	      //text: false
		}).click(function(){
			var continuar = true;
			
			var nombres = $("#nombres");
			var apellidos = $("#apellidos");
			
			var email = $("#email").val();
			var nombres_text = $("#nombres").val();
			var apellidos_text = $("#apellidos").val();


			if(nombres.val() == ""){
				continuar = false;
				alert("El campo 'Nombres' es obligatorio!");
				nombres.focus();
			}else if(apellidos.val() == ""){
				continuar = false;
				alert("El campo 'Apellidos' es obligatorio!");
				apellidos.focus();
			}else if(!validateTextAndSpace(nombres_text)){
				continuar = false;
				alert("Solo se permiten caracteres alfabéticos.");
				nombres.focus();
			}else if(!validateTextAndSpace(apellidos_text)){
				continuar = false;
				alert("Solo se permiten caracteres alfabéticos.");
				apellidos.focus();
			}else if(!validateEmail(email)) { 
				continuar = false;
				alert("Email Invalido");
				$("#email").focus();
			}
			
			if(continuar){
				$("#formModificarUsuario").submit();
			}


			//return continuar;

		});
	});
</script>

<%
Dim nombres
Dim apellidos
Dim email
Dim user_name
Dim id_utente
Dim msg


On Error Resume Next
    'id_utente = CInt(Server.HTMLEncode(request("id_utente")))
    id_utente = Server.HTMLEncode(request("id_utente"))
    'response.write "---[ " & request("id_utente") & " ]---<br />"
    'response.write "---[ " & Server.HTMLEncode(request("id_utente")) & " ]---<br />"
    'response.write "---[ " & CInt(Server.HTMLEncode(request("id_utente"))) & " ]---<br />"
    'response.write "---[ " & id_utente & " ]---<br />"
	'response.end
If err <> 0 Then
	msg = "El usuario seleccionado no pertenece a su centro1111."
	response.Redirect "users_list.asp?msg="& msg
End If
    
Dim authUser_Sede
authUser_Sede = Session("creator")

id_grupo_centros = 25


sqlEditUser_Sede = "SELECT COUNT(1) AS EXISTE FROM UTENTE WHERE IDUTENTE = '" & id_utente & "' AND CREATOR = '" & authUser_Sede & "' AND IDGRUPPO = '" & id_grupo_centros & "'"
set rsEditUser_Sede = CC.Execute(sqlEditUser_Sede)

existe = 0
if not rsEditUser_Sede.EOF then	
	existe 	= rsEditUser_Sede(0) 
end if

if existe = 0 then
	msg = "El usuario seleccionado no pertenece a su centro222222."
	response.Redirect "users_list.asp?msg="& msg
end if


if request("modified") = "1" then 

	nombres = Server.HTMLEncode(request("nombres"))
	apellidos = Server.HTMLEncode(request("apellidos"))
	email = Server.HTMLEncode(request("email"))



	sqlEditarUsuarioCentro = "UPDATE UTENTE SET COGNOME = '" & apellidos & "', NOME = '" & nombres & "', EMAIL = '" & email & "' WHERE UTENTE.IDUTENTE = " & id_utente

	set rsEditarUsuarioCentro = CC.Execute(sqlEditarUsuarioCentro)

	msg = "Usuario actualizado con exito."
	'showMsg msg
	response.Redirect "users_list.asp?msg="& msg

	%>

<% elseif id_utente <> "" then
	sqlDataUser = "SELECT dbo.UTENTE.LOGIN, dbo.UTENTE.COGNOME, dbo.UTENTE.NOME, dbo.UTENTE.EMAIL, dbo.UTENTE.IDUTENTE FROM dbo.UTENTE WHERE dbo.UTENTE.IDUTENTE = " & id_utente
	set rsDataUser = CC.Execute(sqlDataUser)
	if not rsDataUser.EOF then	
		user_name 	= rsDataUser(0) 
		nombres 	= rsDataUser(2)
		apellidos 	= rsDataUser(1)
		email 		= rsDataUser(3)
	end if
end if
%>


<table border="0" width="530" cellspacing="0" cellpadding="0" height="81" >
   <tr>
     <td width="530" background="<%=Session("Progetto")%>/images/titoli/strumenti2g.gif" height="81" valign="bottom" align="right">
       <table border="0" style="width: 100%;padding-right: 20px;" height="30" cellspacing="0" cellpadding="0">
         <tr>
           
          <td width="100%" valign="top" align="right"><b class="tbltext1a">EDITAR USUARIO - CENTRO DE ATENCI&Oacute;N</span></b></td>
         </tr>
       </table>
     </td>
   </tr>
</table>
<br>

<table cellpadding="0" cellspacing="0" width="500" border="0">
	<tr height="18">
	
    <td class="sfondomenu" height="18" width="67%"> <span class="tbltext0"> <b>Editar Usuario - Centro de Atenci&oacute;n</b></span> </td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
		
    <td valign="middle" align="right" class="tbltext1" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">(*) 
      campo obligatorio</td>
		</td>
		<td valign="middle" align="right" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1"></td>
	</tr>
	<tr width="371" class="sfondocomm">
		
    <td colspan="3">Editar los datos pertenecientes al usuario.<br>
	<!--br>
	NOTA: Ingresar al menos uno de los siguientes datos: Nº de Planilla, RUC-->
    <!-- <a href="Javascript:Show_Help('/Pgm/help/Imprese/IMP_InsImpresa')"><img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a> -->     </td>
	</tr>   
	<tr height="2">
		<td colspan="3" class="sfondocomm" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
	</tr>
</table>

<br>
<br>

<form id="formModificarUsuario" name="searchForm" METHOD="POST" action="/Util/edit_user_centro.asp">
	
	<input type="hidden" Class="textblack" id="modified" name="modified" value="1" maxlength="50" />
	<input type="hidden" Class="textblack" id="id_utente" name="id_utente" value="<%=id_utente%>" maxlength="50" />

	<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
		<tr>
			<td class="sfondocomm" width="30%" align="left" colspan="2" style="font-weight:bold;font-size:15px;padding:20px 0px 20px 20px">
				<div style="border-bottom: 1px solid #160EA6;width: 95%;">Informaci&oacute;n de  Perfil:</div>
			</td>
		</tr>
		<tr>
			<td class="sfondocomm" width="30%" align="right">Nombres *</td>
			<td class="sfondocomm" width="40%" align="left">
				<input type="text" id="nombres" name="nombres" value="<%=nombres%>" style="width:70%;" />
			</td>
		</tr>
		<tr>
			<td class="sfondocomm" width="20%" align="right">Apellidos *</td>
			<td class="sfondocomm" width="40%" align="left">
				<input type="text" id="apellidos" name="apellidos"  value="<%=apellidos%>"  style="width:70%;" />
			</td>
		</tr>
		<tr>
			<td class="sfondocomm" width="20%" align="right">Email</td>
			<td class="sfondocomm" width="40%" align="left">
				<input type="text" id="email" name="email"  value="<%=email%>" style="width:70%;" />
			</td>
		</tr>
		<tr>
			<td class="sfondocomm" width="30%" align="left" colspan="2" style="font-weight:bold;font-size:15px;padding:20px 0px 20px 20px">
				<div style="border-bottom: 1px solid #160EA6;width: 95%;">Informaci&oacute;n de  Autenticaci&oacute;n:</div>
			</td>
		</tr>
		<tr>
			<td class="sfondocomm" width="20%" align="right">Nombre de Usuario *</td>
			<td class="sfondocomm" width="40%" align="left">
				<span id="nombre_usuario" style="font-weight:bold;"><%=user_name%></span>
			</td>
		</tr>
		<tr>
			<td class="sfondocomm" width="20%" align="right">Contrase&ntilde;a *</td>
			<td class="sfondocomm" width="40%" align="left">
				<a href="change_password_user_centro.asp?id_utente=<%=id_utente%>">CAMBIAR</a>
			</td>
		</tr>
		<tr>
			<td class="sfondocomm" colspan="2" style="text-align:center;">
			<br />
				<!--input type="submit" value="Registrar" /-->
				<button id="goToUsersList" type="button">VOLVER</button>
				<button id="saveNewUser" type="button">MODIFICAR</button>
			</td>
		</tr>
	</table>
</form>

<br/><br/><br/>








<!--#include virtual = "/include/closeconn.asp"-->
<!--#include Virtual="/strutt_coda2.asp"-->


