<!--#include Virtual = "/strutt_testa2.asp"-->
<!--#include Virtual ="/Include/openconn.asp"-->

<%

	Dim msg
	msg = Server.HTMLEncode(request("msg"))
 

	Dim id_gruppo_centro 
	id_gruppo_centro = 25

	sqlUsersList = "SELECT IDUTENTE,LOGIN,NOME ,COGNOME,IND_ABIL FROM UTENTE WHERE CREATOR = " & Session("creator") & " AND IDGRUPPO = " & id_gruppo_centro

	'Response.Write sqlUsersList
	set rsUsersList = CC.Execute(sqlUsersList)

%>

<table border="0" width="530" cellspacing="0" cellpadding="0" height="81" >
   <tr>
     <td width="530" background="<%=Session("Progetto")%>/images/titoli/strumenti2g.gif" height="81" valign="bottom" align="right">
       <table border="0" style="width: 100%;padding-right: 20px;" height="30" cellspacing="0" cellpadding="0">
         <tr>
           
          <td width="100%" valign="top" align="right"><b class="tbltext1a">LISTADO USUARIOS - CENTRO DE ATENCI&Oacute;N</span></b></td>
         </tr>
       </table>
     </td>
   </tr>
</table>
<br>

<table cellpadding="0" cellspacing="0" width="500" border="0">
	<tr height="18">
	
    <td class="sfondomenu" height="18" width="67%"> <span class="tbltext0"> <b>Listado Usuarios - Centro de Atenci&oacute;n</b></span> </td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
		
    <td valign="middle" align="right" class="tbltext1" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">(*) 
      campo obligatorio</td>
		</td>
		<td valign="middle" align="right" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1"></td>
	</tr>
	<tr width="371" class="sfondocomm">
    	<td colspan="3">Usuarios pertenecientes al centro de atenci&oacute;n.</td>
	</tr>   
	<tr height="2">
		<td colspan="3" class="sfondocomm" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
	</tr>

	<tr height="23" align="center" class="sfondocomm"> 
		<td colspan="3"> <br /> <b class="tbltext1" id="msg"> <%=msg%> </b><br /><br /></td>
	</tr>

	<tr height="23" style="text-align:left;" class="sfondocomm"> 
		<td colspan="3" style="font-size:8px;padding-left: 20%;"> 

			<div class="tbltext1" style="width:40%;display:inline-block;"> 
				<img src="../Images/Icons/edit_user.png" style="width:15px;height:15px;" /> Editar Usuario 
			</div>
			<div class="tbltext1" style="width:40%;display:inline-block;"> 
				<img src="../Images/Icons/enable_user.png" style="width:15px;height:15px;" /> Usuario Habilitado
			</div>
			<br />
			<div class="tbltext1" style="width:40%;display:inline-block;"> 
				<img src="../Images/Icons/change_pasword.png" style="width:15px;height:15px;" /> Cambiar Contrase&ntilde;a
			</div>			
			<div class="tbltext1" style="width:40%;display:inline-block;"> 
				<img src="../Images/Icons/disable_user.png" style="width:15px;height:15px;" /> Usuario Desabilitado
			</div>
		</td>
	</tr>


	<tr width="371">
    	<td colspan="3" style="background-color: #f1f1f3;">
    		
			<% if not rsUsersList.EOF then %>
				<br /><br />
				<table border="0" width="90%" style="margin:auto;FONT-SIZE: 10px;COLOR: navy;FONT-FAMILY: Verdana, Arial;">
					<tr class="tbltext0 tblsfondo3" style="text-align:center;">
						<td width="20%" style="border-radius:10px 0 0 0;"><b>Nombre de Usuario</b></td>
						<td width="20%"><b>Nombres</b></td>
						<td width="20%"><b>Apellidos</b></td>
						<td width="20%" style="text-align:center;border-radius:0 10px 0 0;"><b>Acciones</b></td>
					</tr>

				<%  do while not rsUsersList.EOF %>
					<tr class="tblsfondo" style="background-color: #AACDE0;">
						<td style="padding-left:10px;"><%=rsUsersList(1) %></td>
						<td style="padding-left:10px;"><%=rsUsersList(2) %></td>
						<td style="padding-left:10px;"><%=rsUsersList(3) %></td>
						<td style="text-align:center;">
							<a href="javascript:void(0)" onclick="editUser('<%=rsUsersList(0)%>')">
								<img src="../Images/Icons/edit_user.png" style="width:20px;height:20px;" />
							</a> |
							<a  href="javascript:void(0)" onclick="enableDisableUser(this,'<%=rsUsersList(0)%>','<%=rsUsersList(4)%>')">
								<% if rsUsersList(4) = "S" then %>
									<img src="../Images/Icons/enable_user.png" style="width:20px;height:20px;" />	
								<% elseif rsUsersList(4) = "N" then %>
									<img src="../Images/Icons/disable_user.png" style="width:20px;height:20px;" />
								<% end if %>
							</a> | 
							<a href="javascript:void(0)" onclick="changePassword('<%=rsUsersList(0)%>')">
								<img src="../Images/Icons/change_pasword.png" style="width:20px;height:20px;" />
							</a> 
						</td>
					</tr>
					<% rsUsersList.MoveNext %>
				<% loop %>
			<% end if%>

    	</td>
	</tr>

</table>

<script type="text/javascript">
		enableDisableUser = function(caller,id_utente,ind_abil){
			var url = "enableDisableUser.asp";
			$.post(url,{id_utente:id_utente,ind_abil:ind_abil},function(response){

				$(caller).children().eq(0).attr("src",ind_abil=="S"?"../Images/Icons/disable_user.png":"../Images/Icons/enable_user.png");
				if($(caller).attr("onclick").split(",")[2] == "'N')"){
					$(caller).attr("onclick",$(caller).attr("onclick").replace("N","S"))
				}
				else{
					$(caller).attr("onclick",$(caller).attr("onclick").replace("S","N"))
				}
			});
		}

		editUser = function (id_utente){
			location = "edit_user_centro.asp?id_utente="+id_utente;
		}

		changePassword = function(id_utente){
			location = "change_password_user_centro.asp?id_utente="+id_utente;	
		}
</script>

<br>
<br>


<!--#include virtual = "/include/closeconn.asp"-->
<!--#include Virtual="/strutt_coda2.asp"-->


