<%
' Utilizzato solo per i siti del ministero
sNomePrj = UCase(Request.Form ("NomePrj"))
if sNomePrj <> "" Then 
	Session("Progetto") = sNomePrj
End if
' 
%>
<!-- #include virtual="/include/OpenConn.asp" -->
<!-- #include virtual="/util/portallib.asp" -->
<!-- #include virtual="/util/dbUtil.asp" -->
<!-- #include virtual="/include/SysFunction.asp" -->
<%
'
' ATTENZIONE !!!!!
' in caso di aggiornamento verificare la necessit� di aggiornare 
' la pagina INTERLOGIN.ASP 
'
'--------------------------------------------------------------------------------------------------------------------------------------------------------
'---- consuelo 25/02/2004 direttive di G.Bartone---------
'     inserimento su LOG_UTE come funzionava su IN
function InserisciSessione()
	
	session("id_webserver") = Request.ServerVariables("SERVER_NAME")
	on error resume next

	err.Clear
	InserisciSessione = false
	
	sSQL = "INSERT INTO LOG_UTE (ID_SESSIONE, ID_WEBSERVER, IDUTENTE, DT_INISESS, DT_FINSESS, IDGRUPPO) " & _
		   " VALUES(" & Session.SessionId & " ,'" & session("id_webserver") & "'," & Session("IdUtente") & _
		   ", SYSDATE, NULL, " & Session("idgruppo") & ")"
	
	Errore = EseguiNoC(sSQL,CC)
	
	if Errore = "0" then
		InserisciSessione = true
	end if  		

	on error goto 0

end function
'---- FINE consuelo 25/02/2004 --------------------------



Dim Login,Passwd
Login = UCase(Request.Form ("login"))
Login = Replace(Login,"'","''")

Passwd = UCase(Request.Form ("password"))
Passwd= Replace(Passwd,"'","''")


'--=====================================================
'	Verifica usuario - Usuario Unificado MTSS
'--=====================================================
'..control = "Saltar"
control = "NoSaltar"

if control <> "Saltar" then
	Dim cmdUsuario, xStatus, xMensaje, xUsuarioId, xNombre, xApellido

	Set cmdUsuario = Server.CreateObject("ADODB.Command")
	With cmdUsuario
		.ActiveConnection = gConnSeguridad
		.CommandTimeout = 1200
		.CommandText = "spLogin"
		.CommandType = 4
		'-------------------------------------------------
		' Parametros de Entrada������� 
		'-------------------------------------------------
		.Parameters("@UserName").Value = Login
		.Parameters("@Password").Value = Passwd
		.Execute()
		'-------------------------------------------------
		' Parametros de Output
		'-------------------------------------------------
		xUsuarioId = .Parameters("@UsuarioId").value
		xNombre = .Parameters("@Nombre").value
		xApellido = .Parameters("@Apellido").value
		xStatus = .Parameters("@Status").value
		xMensaje = .Parameters("@Mensaje").value

		'-------------------------------------------------
		' Verifica si puede entrar
		'-------------------------------------------------
		if xStatus <> "1" Then
			%>
			<script>
			alert('<%=xMensaje %>')
			parent.location.href="/util/logout.asp"
			</script>
			<% 
			Response.End
		End If
	End With
End if

'-------------------------------------------------------
' Sigue con lo que estaba
'-------------------------------------------------------
Set rstUtente = Server.CreateObject("ADODB.RECORDSET")
SQLUtente = "SELECT u.idutente, u.nome, u.cognome, u.email, u.password, u.idgruppo, u.tipo_pers, u.dt_pwd, NVL(SYSDATE-u.dt_pwd,0)as giorni, " & _
			" u.creator, u.login, u.ind_abil, g.desgruppo, g.cod_rorga, g.webpath, nvl(u.id_uorg,0) id_uorg, g.cod_ruofu " & _
			" FROM utente u, gruppo g" & _
			" WHERE ltrim(rtrim(u.login)) = '" & Login & "' AND u.idgruppo = g.idgruppo "

rstUtente.open SQLUtente, CC, 3, 3
'Response.Write sqlutente


If rstUtente.EOF Then
	%>
	<script>
		alert("El usuario <%=Login%> no est� registrado.")
		parent.location.href="/util/logout.asp"
	</script>
	<% 
	Response.End
End If
	
'---------------------------------------------------------
' Para mantener compatibilidad MTSS
'---------------------------------------------------------
' La clave se toma la registrada en ITlavoro para que pase
' El acceso fue controlado antes de llegar aca
passwd = Ucase(rstUtente("password"))
'trim(passwd) = trim(passwd) and trim(rstUtente("ind_abil"))="S")

If trim(rstUtente("ind_abil"))="S"   then

	'inserimento verifica validit� password e eventuale modifica

	Session("password") = rstUtente("password")
	Session("IdUtente") = Clng(rstUtente("IdUtente"))
	nGiorni= clng(rstUtente("giorni"))
	sPagtoVisit = Session("Progetto") & "/home.asp"
	
		if isnull(rstUtente("dt_pwd")) then
			%>		
			<Script>
				 var myWindow=window.open("","w1","width=380,height=350");
					myWindow.location.href = "/util/PopPassword.asp?Tit=1"
					parent.location.href =  "<%=sPagtoVisit%>"
			</Script> 
			<%	
			rstUtente.Close
			Set rstUtente = Nothing	
	
			Response.end
		end if
		
		if nGiorni > 180 then
			Set rstUtenteDeny = Server.CreateObject("ADODB.RECORDSET")
			SQLUtenteDeny = "UPDATE UTENTE SET IND_ABIL='N' WHERE IDUTENTE=" & Session("idutente")
			rstUtenteDeny.open SQLUtenteDeny, CC, 3, 3
			%>
			<Script>
			alert("El usuario no se encuentra habilitado a acceder al sistema")
				parent.location.href="<%=sPagtoVisit%>"
			</Script>
			<%			
			rstUtenteDeny.Close
			Set rstUtenteDeny = Nothing	
			rstUtente.Close
			Set rstUtente = Nothing	
	
			Response.end
		end if	
		
		if nGiorni > 90 then
			%>		
			<Script>
				 var myWindow=window.open("","w1","width=380,height=350");
					myWindow.location.href = "/util/PopPassword.asp?Tit=2"
					parent.location.href =  "<%=sPagtoVisit%>"
			</Script> 
			<%	
			rstUtente.Close
			Set rstUtente = Nothing	
	
			Response.end
		end if
		
	
	Session("IdUtente") = Clng(rstUtente("IdUtente"))
	Session("nome") = rstUtente("nome")
	Session("cognome") = rstUtente("cognome")
	Session("email") = rstUtente("email")
	Session("idgruppo") = Clng(rstUtente("idgruppo"))
	Session("password") = rstUtente("password")
	Session("tipopers") = rstUtente("tipo_pers")
	Session("creator") = Clng(rstUtente("creator"))
	Session("login") = rstUtente("login")
	Session("gruppo") = rstUtente("desgruppo")
	Session("rorga") = rstUtente("cod_rorga") 
	Session("webpath") = rstUtente("webpath")
	Session("iduorg") = Clng(rstUtente("id_uorg"))
	Session("ruofu") = rstUtente("cod_ruofu")
	Session("Prglog") = Session("Progetto")
	
	'utilizzer� poi la session Prglog, che identifica il progetto dove ho fatto la login,
	'nei controlli all'interno di chk_init.asp
	
	if Session("rorga") <> "RC" and Session("rorga") <> "RR" then
	'Solo in locale
		Session.TimeOut = 180
	else
		Session.Timeout = 180
	end if
	
	If  Ucase(Session("Progetto")) = "/SPORT2JOB" Then
		Session.Timeout = 180
	End If
		
	Set rstGruppoFun = Server.CreateObject("ADODB.RECORDSET")
		SQLGruppoFun = "SELECT a.namevar, a.valore, b.acronimo FROM gruppofun a, funzione b WHERE (a.idfunzione=b.idfunzione and a.idgruppo=" & Session("idgruppo")& ")"
		rstGruppoFun.open SQLGruppoFun, CC, 1, 3
		
	If not rstGruppoFun.EOF Then
		Do While Not rstGruppoFun.EOF
			If rstGruppoFun("namevar") <> "" Then
				Session(rstGruppoFun("namevar")) = rstGruppoFun("valore")
				Session(rstGruppoFun("acronimo")) = addElement2ArrayFunction(rstGruppoFun("acronimo"),rstGruppoFun("namevar"),rstGruppoFun("valore"),false)

			End if
			rstGruppoFun.MoveNext
		Loop	
	End If				

	
	rstGruppoFun.Close
	Set rstGruppoFun = Nothing

	Set rstUteFun = Server.CreateObject("ADODB.RECORDSET")
		SQLUteFun = "SELECT a.namevar, a.valore, b.acronimo FROM utefun a, funzione b where (a.idfunzione=b.idfunzione and a.idutente=" & Session("IdUtente") & ")"
		rstUteFun.open SQLGruppoFun, CC, 1, 3
					
	If not rstUteFun.EOF Then
		Do While Not rstUteFun.EOF 
			If rstUteFun("namevar") <> "" Then
				Session(rstUteFun("namevar")) = rstUteFun("valore")
				Session(rstUteFun("acronimo")) = addElement2ArrayFunction(rstUteFun("acronimo"),rstUteFun("namevar"),rstUteFun("valore"),true)					
			end if
			rstUteFun.MoveNext
		Loop
	End If
	rstUteFun.Close
	Set rstUteFun = Nothing
	
	Set rstLastAccess = Server.CreateObject("ADODB.RECORDSET")
		sSQLLastAccess="SELECT DT_ACCESSO FROM UTENTE WHERE IDUTENTE =" & SESSION ("Idutente")
		rstLastAccess.open sSQLLastAccess, CC, 1, 3
		session ("LastAccess")= rstLastAccess("DT_ACCESSO")
	Set rstLastAccess = Nothing	
	
	CC.Begintrans
	
	sSQL ="UPDATE UTENTE  SET DT_ACCESSO=" & convdatetodb(Now()) & " WHERE idutente= " & session("idutente")

	Errore = EseguiNoC(sSQL,CC)
	
	If Errore="0" Then

'---- CONSUELO 25/02/2004 direttive di G.Bartone---------
'	  inserimento su LOG_UTE come funzionava su IN

		Set RRAut = Server.CreateObject("adodb.RECORDSET")
		sSQLAut = "SELECT AUT_LOG FROM GRUPPO WHERE IDGRUPPO = " & Session("idgruppo") & " AND AUT_LOG = 'S'"
		
		RRAut.Open sSQLAut, CC
		
		'verifico se l'utente appartiene ad un gruppo monitorato su LOG_UTE
		if not RRAut.eof then
			
			Session("Log") = "S"
						
   			if InserisciSessione() = true then
				CC.CommitTrans
			else
				CC.RollbackTrans
			end if
		else
			CC.CommitTrans
		end if
		
		RRAut.Close	 
		Set RRAut = nothing

'---- FINE CONSUELO 25/02/2004 direttive di G.Bartone---------

	Else
	   CC.RollbackTrans   
	End If 	    
Else

	If UCase(rstUtente("password")) <> UCase(passwd) Then
		%>
		<Script>
			alert("La contrase�a ingresada es incorrecta")
			parent.location.href="/util/logout.asp"
		</Script>
		<%
	Else
	'----> ind_abil=N ---------
		%>
		<Script>
			alert("El usuario no se encuentra habilitado a acceder al sistema")
			parent.location.href="/util/logout.asp"
		</Script>
		<%	
	End If
	Response.end
End If

rstUtente.Close
Set rstUtente = Nothing	

		if nGiorni > 79 then
			sPagtoVisit = Session("Progetto") & "/home.asp"
			nGGMancanti = 91 - nGiorni
			Session("DatePwd") = nGGMancanti
%>
				<Script>
					var myWindow=window.open("","w1","width=380,height=350");
						myWindow.location.href = "/util/PopPassword.asp?Tit=3"
						parent.location.href =  "<%=sPagtoVisit%>"
				</Script> 
<%			
				Response.end
		end if

sPagtoVisit = Session("Progetto") & "/home.asp"
Session("homePage") = sPagtoVisit 

Response.Redirect sPagtoVisit
%>
<!-- #include virtual="/include/closeconn.asp" -->
