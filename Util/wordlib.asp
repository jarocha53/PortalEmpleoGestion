<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<SCRIPT LANGUAGE=VBScript RUNAT=Server>
Function ShowFolderList(folderspec)
   Dim fso, f, f1, fc, s
   Set fso = CreateObject("Scripting.FileSystemObject")
   Set f = fso.GetFolder(folderspec)
   Set fc = f.Files
   For Each f1 in fc
   s= s & "<OPTION VALUE=" & chr(34) & f1.name & chr(34) & ">" & f1.name
   Next
   response.write s
   ShowFolderList = s
End Function

SUB CreateNewHTML(iFlag,LOCAL_ROOT,LOCAL_ROOT_IN,LOCAL_ROOT_OUT,URL_ROOT,Filein,Fileout)

Dim objWD
Dim FileFormat
Dim ConFile
Dim LockComments
Dim Password
Dim AddToRecentFiles
Dim WritePassword
Dim ReadOnlyRecommended
Dim EmbedTrueTypeFonts
Dim SaveNativePictureFormat
Dim SaveFormsData
Dim SaveAsAOCELetter
Dim DoComp
Dim DoTidy
Dim Do2000

remoteID = Request.ServerVariables("REMOTE_ADDR")
count = Session("counter")

FIleFormat=8
ConFile="conf.txt"

'Filein = Request.Form("FileName")
'Fileout = Request.Form("output")

'PL-SQL * T-SQL  
Set Executor = Server.CreateObject("ASPExec.Execute")
Executor.Application = "cmd.exe"
Executor.Parameters = " /C pskill.exe winword.exe"
Executor.TimeOut = 9000
Response.Write "WinWord Fixer: Execution starts<br>"
response.flush

'PL-SQL * T-SQL  
WINAPPANDWAIT = TransformPLSQLToTSQL (WINAPPANDWAIT) 
intResult = Executor.ExecuteWinAppAndWait
	if intResult = 0 then
		Response.Write "WinWord Fixer: Execution successful<br>"
	else
		Response.Write "WinWord Fixer: Error, the result of the call was: " & intResult & "<br>"
	end if
response.flush

Select Case iFlag

Case "DOC2HTML"
FileFormat =8

Case "DOC2HTMLCOMP"
FileFormat =8
DoComp=1

Case "DOC2HTMLCOMPTIDY"
FileFormat =8
DoComp=1
DoTidy=1

Case "DOC2000HTML"
FileFormat =8
ConFile="conf2000.txt"
DoComp=1
DoTidy=1

Case "DOC2RTF"
FileFormat =6

Case "RTF2HTML"
FileFormat =8
DoComp=1
DoTidy=1
ConFile="conf2000.txt"

End Select

Response.write "Document: " & LOCAL_ROOT_IN & Filein & " Opening<br>"
response.flush

Response.write "Converter: Instance Requested from (" & remoteID & ") counter (" & count &")<br>"
response.flush
Set objWD = GetObject(LOCAL_ROOT_IN & Filein,"Word.Document")


Response.write "Document: " & LOCAL_ROOT_IN & Filein & " Opened<br>"
response.flush

objWD.application.visible=True

LockComments = True
Password = ""
AddToRecentFiles = False
WritePassword = ""
ReadOnlyRecommended = False
EmbedTrueTypeFonts = False
SaveNativePictureFormat = True
SaveFormsData = False
SaveAsAOCELetter = False
objWD.SaveAs LOCAL_ROOT_OUT & Fileout, FileFormat, LockComments, Password, AddToRecentFiles, WritePassword,    ReadOnlyRecommended, EmbedTrueTypeFonts, SaveNativePictureFormat, SaveFormsData, SaveAsAOCELetter

Response.write "Document: Converted and Saved as "& LOCAL_ROOT_OUT & Fileout &"<br>"
response.flush

'objWD.close
objWD.application.quit
Set objWD = Nothing

Response.write "Converter Instance: Closed<br>"
response.flush

if FIleFormat=8 then

if DoComp then

'PL-SQL * T-SQL  
Set Executor = Server.CreateObject("ASPExec.Execute")
Executor.Application = "filter.exe"
Executor.Parameters = " -l -t -m -f -b -r " & LOCAL_ROOT_OUT & Fileout
Executor.TimeOut = 9000
Response.Write "XTML Filter: Execution starts on "& LOCAL_ROOT_OUT & Fileout & "<br>"
response.flush

'PL-SQL * T-SQL  
WINAPPANDWAIT = TransformPLSQLToTSQL (WINAPPANDWAIT) 
intResult = Executor.ExecuteWinAppAndWait
	if intResult = 0 then
		Response.Write "XTML Filter: Execution successful<br>"
	else
		Response.Write "XTML Filter: Error, the result of the call was: " & intResult & "<br>"
	end if
end if
response.flush


Dim MyFilePath
Dim MyFileObject
Dim MyFile
Dim TheFile
Dim laString

MyFilePath=LOCAL_ROOT_OUT & Fileout

if DoTidy=1 then

Response.write "Tidy: executi�n starts with " & Confile & " as config file <br>"
response.flush
Set FSO = Server.CreateObject("scripting.FileSystemObject")
Set TidyObj = Server.CreateObject("TidyCOM.TidyObject")
TidyObj.Options.Load LOCAL_ROOT & ConFile
TidyObj.TidyToFile MyFilePath, MyFilePath
Response.write "Tidy: executed on " & MyFilePath & " succesfully<br>"
response.flush
Set FSO=nothing
Set TidyObj =nothing

end if

Dim fso, ts,fs
Set fso = CreateObject("Scripting.FileSystemObject")
Set ts = fso.getfile(MyFilePath)
Set fs = ts.OpenAsTextStream(1,-2)
text=fs.readall
set regEx = new regExp
regEx.Pattern = "\.doc"   ' Set pattern.
   regEx.IgnoreCase = True   ' Set case insensitivity.
   regEx.Global = True   ' Set global applicability.
   text = regEx.Replace (text,".htm")

fs.close

Set ts = fso.createtextfile(MyFilePath,true)
ts.write text
ts.close
set ts=nothing
set fso=nothing

end if

Session("counter") = count + 1
count = Session("counter")
Response.Write "<A HREF=" &  URL_ROOT &"/" & Fileout
Response.Write "><H4> Use this link to view converted file from server: " & Fileout & "</a></H4>"
END SUB


</SCRIPT>
