<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->

<%
'
' ATTENZIONE !!!!!
' in caso di aggiornamento verificare la necessit� di aggiornare 
' la pagina LOGIN.ASP 
'
Response.ExpiresAbsolute = Now() - 1 
Response.AddHeader "pragma","no-cache"
Response.AddHeader "cache-control","private"
Response.CacheControl = "no-cache"
on error resume next
'********************************************
'FORZATURA PER POTER ESEGIRE L'INTER LOGIN
Progetto = "/" & Request.Form  ("txtProgetti")
'**************************************
%>

<!--#include Virtual = "/include/OpenConn.asp"-->
<!--#include virtual = "/util/portallib.asp"-->
<!--#include virtual = "/util/dbUtil.asp"-->
<!--#include virtual = "/include/SysFunction.asp"-->

<%

Dim Login,Passwd

Login = Session("login")
Login = Replace(Login,"'","''")

Passwd = UCASE(Session("password"))
Passwd = UCASE(Replace(Passwd,"'","''"))
Snome=	UCASE(Session("nome"))
Snome = UCASE(Replace(Snome,"'","''"))
Scognome=UCase(Session("cognome"))
Scognome = UCASE(Replace(Scognome,"'","''"))

'--=====================================================
'	Verifica usuario - Usuario Unificado MTSS
'--=====================================================
	Dim cmdUsuario, xStatus, xMensaje, xUsuarioId, xNombre, xApellido

	Set cmdUsuario = Server.CreateObject("ADODB.Command")
	With cmdUsuario
		.ActiveConnection = gConnSeguridadReplica
		.CommandTimeout = 1200
		.CommandText = "spLogin"
		.CommandType = 4
		'-------------------------------------------------
		' Parametros de Entrada������� 
		'-------------------------------------------------
		.Parameters("@UserName").Value = Login
		.Parameters("@Password").Value = Passwd
'PL-SQL * T-SQL  
		.Execute()
		'-------------------------------------------------
		' Parametros de Output
		'-------------------------------------------------
		xUsuarioId = .Parameters("@UsuarioId").value
		xNombre = .Parameters("@Nombre").value
		xApellido = .Parameters("@Apellido").value
		xStatus = .Parameters("@Status").value
		xMensaje = .Parameters("@Mensaje").value

		'-------------------------------------------------
		' Verifica si puede entrar
		'-------------------------------------------------
		if xStatus <> "1" Then
			%>
			<script>
			alert('<%=xMensaje %>')
			parent.location.href="/util/logout.asp"
			</script>
			<% 
			Response.End
		End If
	End With

'-------------------------------------------------------
' Sigue con lo que estaba
'-------------------------------------------------------
Set rstUtente = Server.CreateObject("ADODB.RECORDSET")
SQLUtente = "SELECT u.idutente, u.nome, u.cognome, u.email, u.password, u.idgruppo, u.tipo_pers, " & _
			" u.creator,u.id_uorg, u.login, u.ind_abil, g.desgruppo, g.cod_rorga, g.webpath, g.cod_ruofu " & _
			" FROM utente u, gruppo g" & _
			" WHERE upper(u.login) = '" & Login & "'" & _
			" and upper(u.password)='" & Passwd & "'" & _
			" and upper(u.cognome)='" & Scognome  & "'" & _
			" and upper(u.nome)='" & Snome & "'" & _
			" and u.idgruppo=g.idgruppo"
		
'PL-SQL * T-SQL  
SQLUTENTE = TransformPLSQLToTSQL (SQLUTENTE) 
rstUtente.open SQLUtente, cx, 3, 3
'Response.Write SQLUtente

%>
<FORM name="frmInterLogin">
	<input name="login" type="hidden" value="<%=login%>">
	<input name="password" type="hidden" value="<%=Passwd%>">
</FORM>
<%
If rstUtente.EOF Then 
%>
	<script>
		alert("El usuario " + document.frmInterLogin.login.value.toUpperCase() + " no est� habilitado a acceder.");
		history.back();
	</script>
<% 
	Response.End 
End If	

'---------------------------------------------------------
' Para mantener compatibilidad MTSS
'---------------------------------------------------------
' La clave se toma la registrada en ITlavoro para que pase
' El acceso fue controlado antes de llegar aca
passwd = UCase(rstUtente("password"))

If  (UCase(rstUtente("password")) = UCase(passwd) and rstUtente("ind_abil")="S") Then

	Session("Progetto") = "/" & ucase(Progetto)
	
	Session("IdUtente") = Clng(rstUtente("IdUtente"))
	Session("nome") = rstUtente("nome")
	Session("cognome") = rstUtente("cognome")
	Session("email") = rstUtente("email")
	Session("idgruppo") = Cint(rstUtente("idgruppo"))
	Session("password") = rstUtente("password")
	Session("tipopers") = rstUtente("tipo_pers")
	Session("creator") = Clng(rstUtente("creator"))
	Session("login") = rstUtente("login")
	Session("gruppo") = rstUtente("desgruppo")
	Session("rorga") = rstUtente("cod_rorga") 
	Session("webpath") = rstUtente("webpath")
	Session("Prglog") = Session("Progetto")
	Session("iduorg") = Clng(rstUtente("id_uorg"))
	Session("ruofu") = rstUtente("cod_ruofu")
	Session("menusx") = ""
	'utilizzer� poi la session Prglog,
	'che identifica il progetto dove ho fatto la login,
	'nei controlli all'interno di chk_init.asp
	
	if Session("rorga") <> "RC" and Session("rorga") <> "RR" then
		Session.TimeOut = 25 
	else
		Session.Timeout = 180
	end if

	rstUtente.Close
	Set rstUtente = Nothing	
	Set rstGruppoFun = Server.CreateObject("ADODB.RECORDSET")
		SQLGruppoFun = "SELECT a.namevar, a.valore, b.acronimo FROM gruppofun a, funzione b WHERE (a.idfunzione=b.idfunzione and a.idgruppo=" & Session("idgruppo")& ")"
'PL-SQL * T-SQL  
SQLGRUPPOFUN = TransformPLSQLToTSQL (SQLGRUPPOFUN) 
		rstGruppoFun.open SQLGruppoFun, cx, 1, 3

	If not rstGruppoFun.EOF Then
		Do While Not rstGruppoFun.EOF
			If rstGruppoFun("namevar") <> "" Then
				Session(rstGruppoFun("namevar")) = rstGruppoFun("valore")
				Session(rstGruppoFun("acronimo")) = addElement2ArrayFunction(rstGruppoFun("acronimo"),rstGruppoFun("namevar"),rstGruppoFun("valore"),false)
			End if
			rstGruppoFun.MoveNext
			
		Loop	
	End If
	rstGruppoFun.Close
	Set rstGruppoFun = Nothing

	Set rstUteFun = Server.CreateObject("ADODB.RECORDSET")
		SQLUteFun = "SELECT a.namevar, a.valore, b.acronimo FROM utefun a, funzione b where (a.idfunzione=b.idfunzione and a.idutente=" & Session("IdUtente") & ")"
'PL-SQL * T-SQL  
SQLGRUPPOFUN = TransformPLSQLToTSQL (SQLGRUPPOFUN) 
		rstUteFun.open SQLGruppoFun, cx, 1, 3

	If not rstUteFun.EOF Then
		Do While Not rstUteFun.EOF 
			If rstUteFun("namevar") <> "" Then
				Session(rstUteFun("namevar")) = rstUteFun("valore")
				Session(rstUteFun("acronimo")) = addElement2ArrayFunction(rstUteFun("acronimo"),rstUteFun("namevar"),rstUteFun("valore"),true)					
			end if
			rstUteFun.MoveNext
		Loop	
	End If
	rstUteFun.Close
	Set rstUteFun = Nothing	

	cx.Begintrans

	sSQL ="UPDATE UTENTE SET DT_ACCESSO=" & convdatetodb(Now()) &_
          " WHERE idutente=" & session("idutente")

	Errore=EseguiNoC(sSQL,cx)

	If Errore="0" Then
	   cx.CommitTrans
	Else
	   cx.RollbackTrans   
	End If      

Else
	If UCase(rstUtente("password")) <> UCase(passwd) Then
%>
		<Script>
			alert("Contrase�a incorrecta")
			//parent.location.href="/util/logout.asp"
		</Script>
<%
	Else
%>
		<Script>
			alert("No se encuentra habilitado a acceder al sistema")
			//parent.location.href="/util/logout.asp"
		</Script>
<%
	End If
	
End If

sPagtoVisit =  Session("Progetto") 

Session("homePage") = sPagtoVisit

Response.Redirect sPagtoVisit


%>



<!--#include virtual = "/include/ClosePar.asp"-->
