<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%
Buffer = True
Response.expiresAbsolute=Now()-10
Response.expires=0
Response.addHeader "pragma","no-cache"
Response.addHeader "cache-control","private"

Dim CR
Dim LF
Dim CrLf
CR=Chr(13)
LF=Chr(10)
CrLf=Chr(13)+Chr(10)
QU = Chr(34)
Qs = Chr(39)

Function LeadZero(TmpNumber)

Select Case Len(TmpNumber)
	Case 0
		LeadZero="00"
	Case 1
		LeadZero="0"+TmpNumber
	Case 2
		LeadZero=TmpNumber
End Select

End function

Function CheckIsNumber(NumberToCheck)
 CheckIsNumber = True
 NumberToCheck = Replace(NumberToCheck, ",", ".")
 If InStr(NumberToCheck, ".") > 0 Then
  PosStr = Mid(NumberToCheck, 1, InStr(NumberToCheck, ".") - 1)
  DecStr = Mid(NumberToCheck, InStr(NumberToCheck, ".") + 1, Len(NumberToCheck))
  For a = 1 To Len(PosStr)
   If Asc(Mid(PosStr, a, 1)) < 48 Then
    CheckIsNumber = False
   End If
   If Asc(Mid(PosStr, a, 1)) > 57 Then
    CheckIsNumber = False
   End If
  Next
  For a = 1 To Len(DecStr)
   If Asc(Mid(DecStr, a, 1)) < 48 Then
    CheckIsNumber = False
   End If
   If Asc(Mid(DecStr, a, 1)) > 57 Then
    CheckIsNumber = False
   End If
  Next
 Else
  For a = 1 To Len(NumberToCheck)
   If Asc(Mid(NumberToCheck, a, 1)) < 48 Then
    CheckIsNumber = False
   End If
   If Asc(Mid(NumberToCheck, a, 1)) > 57 Then
    CheckIsNumber = False
   End If
  Next
 End If
End Function

Function ISThisADate(TheDate)
 
 If IsDate(TheDate) = False Then
 	ISThisADate = False
 Else
 	ISThisADate = True
 End if
End Function

Function FixDbField(FieldToCheck)
 If IsNull(FieldToCheck)=True THen
 	FieldToCheck="&nbsp;"
 End if
 If FieldToCheck<>"&nbsp;" Then
  FieldToCheckTmp=Cstr(FieldToCheck)
  FieldToCheckTmp = Replace(FieldToCheckTmp, ",", ".")
  FieldToCheckTmp = Replace(FieldToCheckTmp, Chr(34), "�")
  'FieldToCheckTmp = Replace(FieldToCheckTmp, "'", "'")
  FixDbField = Server.HtmlEncode(Trim(FieldToCheckTmp))
 End If
End Function

Function FixDbFieldSave(FieldToCheck)
 If IsNull(FieldToCheck)=True Then
 	FieldToCheck=""
 End if
 If FieldToCheck <> "" Then
  FieldToChecktmp=Cstr(FieldToCheck)
  FieldToChecktmp = Replace(FieldToChecktmp, ",", ".")
  FieldToChecktmp = Replace(FieldToChecktmp, Chr(34), "�")
  FieldToChecktmp = Replace(FieldToChecktmp, "'", "''")
  FixDbFieldSave=Trim(FieldToChecktmp)
 End If
End Function

Function ShowTextOnPic(TextToShow)
 ShowTextOnPic = " onMouseOver=" + QU + "self.status=" + Qs + TextToShow + Qs + QU + " onMouseOut=" + QU + "self.status=" + Qs + Qs + QU + " "
End Function

Function CreateDropDownBox(DefaultValue, ItemsInList, Fieldname, dropdownsize, StyleStr, curvalue)
 TmpStr = "<select size='" + dropdownsize + "' name='" + Fieldname + "' " + StyleStr + ">"
 ItemsInList = Replace(ItemsInList, ",", ";")
 ItemsInList = Replace(ItemsInList, " ", "")
 ItemsInList = ItemsInList + ";"
 If InStr(ItemsInList, curvalue) <= 0 Then
     TmpStr=TmpStr+"<option selected value='"+curvalue+"'>"+curvalue+"</option>"
 End If
 While InStr(ItemsInList, ";") > 0
  If Mid(ItemsInList, 1, InStr(ItemsInList, ";") - 1) = DefaultValue Then
   TmpStr=TmpStr+"<option selected value='"+Mid(ItemsInList,1,InStr(ItemsInList,";")-1)+"'>"+Mid(ItemsInList,1,InStr(ItemsInList,";")-1)+"</option>"
  Else
   TmpStr=TmpStr+"<option value='"+Mid(ItemsInList,1,InStr(ItemsInList,";")-1)+"'>"+Mid(ItemsInList,1,InStr(ItemsInList,";")-1)+"</option>"
  End If
 ItemsInList = Mid(ItemsInList, InStr(ItemsInList, ";") + 1, Len(ItemsInList))
 Wend
 TmpStr = TmpStr + "</select>"
 CreateDropDownBox = TmpStr
End Function

Function CreateCheckBox(ItemsInList, Fieldname, StyleStr, curvalue)
 
 TmpStr = ""
 ItemsInList = Replace(ItemsInList, ",", ";")
 ItemsInList = Replace(ItemsInList, " ", "")
 ItemsInList = ItemsInList + ";"

 While InStr(ItemsInList, ";") > 0
  If InStr(curvalue,Mid(ItemsInList, 1, InStr(ItemsInList, ";") - 1) )>0 Then
   TmpStr=TmpStr+"<input type='checkbox' name='"+fieldname+"' checked value='"+Mid(ItemsInList,1,InStr(ItemsInList,";")-1)+"'>"+Mid(ItemsInList,1,InStr(ItemsInList,";")-1)+"  "
  Else
   TmpStr=TmpStr+"<input type='checkbox' name='"+fieldname+"' value='"+Mid(ItemsInList,1,InStr(ItemsInList,";")-1)+"'>"+Mid(ItemsInList,1,InStr(ItemsInList,";")-1)+"  "
  End If
 ItemsInList = Mid(ItemsInList, InStr(ItemsInList, ";") + 1, Len(ItemsInList))
 Wend
 CreateCheckBox= TmpStr
End Function

Function CreateYesNo(YesNoFirstChoice, YesNoSecondChoice, YesNoFirstValue,YesNoSecondValue, FieldName,CurValue, DefaultValue)
 TmpStr = "<select size='1' name='" + Fieldname + "'>"
	
 If CurValue="" Then CurValue=DefaultValue
 
 If YesNoFirstValue = CurValue Then
   TmpStr=TmpStr+"<option selected value='"+YesNoFirstValue +"'>"+YesNoFirstChoice +"</option>"
  Else
   TmpStr=TmpStr+"<option value='"+YesNoFirstValue +"'>"+YesNoFirstChoice +"</option>"
  End If

 If YesNoSecondValue = CurValue Then
   TmpStr=TmpStr+"<option selected value='"+YesNoSecondValue+"'>"+YesNoSecondChoice+"</option>"
  Else
   TmpStr=TmpStr+"<option value='"+YesNoSecondValue+"'>"+YesNoSecondChoice+"</option>"
  End If

 TmpStr = TmpStr + "</select>"
 CreateYesNo = TmpStr

End function

Function GetDateTimeResponse(FieldName)
	
	If Request.QueryString(FieldName+"_MONTH")<>"" THEN
		GetResponseDateTime =Request.QueryString(FieldName+"_MONTH")
	Else
		GetResponseDateTime ="01"
	End If
	
	If Request.QueryString(FieldName+"_DAY")<>"" THEN
		GetResponseDateTime = GetResponseDateTime+ "/" +Request.QueryString(FieldName+"_DAY")
	Else	
		GetResponseDateTime = GetResponseDateTime+ "/01"
	End if
	
	If Request.QueryString(FieldName+"_YEAR")<>"" THEN
		GetResponseDateTime = GetResponseDateTime+ "/" +Request.QueryString(FieldName+"_YEAR")
	Else
		GetResponseDateTime = GetResponseDateTime+ "/80"
	End if
	
	GetResponseDateTime = GetResponseDateTime+ " "

	If Request.QueryString(FieldName+"_HOUR")<>"" THEN
		GetResponseDateTime = GetResponseDateTime+ Request.QueryString(FieldName+"_HOUR")
	Else
		GetResponseDateTime = GetResponseDateTime+ "00"
	End if
	
	If Request.QueryString(FieldName+"_MIN")<>"" THEN
		GetResponseDateTime = GetResponseDateTime+ ":" +Request.QueryString(FieldName+"_MIN")
	Else
		GetResponseDateTime = GetResponseDateTime+ ":00"
	End if	
	If Request.QueryString(FieldName+"_SEC")<>"" THEN
		GetResponseDateTime = GetResponseDateTime+ ":" +Request.QueryString(FieldName+"_SEC")
	Else
		GetResponseDateTime = GetResponseDateTime+ ":00"
	End if
	GetDateTimeResponse = GetResponseDateTime		
End Function

Function CreateDateTimeBox(DateToShow, FieldName, DateFormat )

DateFormat =DateFormat +" "
TmpStr=""
If ISThisADate(DateToShow)=True then
 DateToShowDate = CDate(DateToShow)
Else
  DateToShowDate = CDate("01/01/80")
End if

While Len(DateFormat)>0 
	Select Case UCase(Mid(dateformat,1,2))
		Case "DD"
			TmpStr = TmpStr + "<select size='1' name='" + Fieldname + "_DAY'>"+CrLf
			For a = 1 to 31
				If a=Day(DateToShowDate) Then TmpSel=" selected" else TmpSel=""
				TmpStr=TmpStr+" <option"+TmpSel+" value='"+Trim(CStr(a))+"'>"+Trim(CStr(a))+"</option>"+CrLf
			Next
			TmpStr = TmpStr+"</select>"
			TmpStr=TmpStr+Mid(dateformat, 3,1)
			dateformat = Mid(dateformat,4,len(dateformat))
		Case "MM"
			TmpStr = TmpStr + "<select size='1' name='" + Fieldname + "_MONTH'>"+CrLf
			For a = 1 to 12
				If a=month(DateToShowDate) Then TmpSel=" selected" else TmpSel=""
				TmpStr=TmpStr+"<option"+TmpSel+" value='"+Trim(CStr(a))+"'>"+Trim(CStr(a))+"</option>"+CrLf
			Next
			TmpStr = TmpStr+"</select>"
			TmpStr=TmpStr+Mid(dateformat, 3,1)
			dateformat = Mid(dateformat,4,len(dateformat))		
		Case "YY"
			TmpStr = TmpStr + "<select size='1' name='" + Fieldname + "_YEAR'>"+CrLf
			For a = 80 to 99	
				If (a+1900)=year(DateToShowDate) Then TmpSel=" selected" else TmpSel=""
				TmpStr=TmpStr+" <option"+TmpSel+" value='"+Trim(CStr(a))+"'>"+Trim(CStr(a))+"</option>"+CrLf
			Next
			For a = 0 to 9
				If (a+2000)=year(DateToShowDate) Then TmpSel=" selected" else TmpSel=""
				TmpStr=TmpStr+" <option"+tmpSel+" value='0"+Trim(CStr(a))+"'>0"+Trim(CStr(a))+"</option>"+CrLf
			Next
			TmpStr = TmpStr+"</select>"
			TmpStr=TmpStr+Mid(dateformat, 3,1)
			dateformat = Mid(dateformat,4,len(dateformat))		
		Case "HH"
			TmpStr = TmpStr + "        <select size='1' name='" + Fieldname + "_HOUR'>"+CrLf
			For a = 0 to 9
				If a=Hour(DateToShowDate) Then TmpSel=" selected" else TmpSel=""
				TmpStr=TmpStr+" <option"+TmpSel+" value='0"+Trim(CStr(a))+"'>0"+Trim(CStr(a))+"</option>"+CrLf
			Next
			For a = 10 to 23
				If a=Hour(DateToShowDate) Then TmpSel=" selected" else TmpSel=""
				TmpStr=TmpStr+" <option"+TmpSel+" value='"+Trim(CStr(a))+"'>"+Trim(CStr(a))+"</option>"+CrLf
			Next
			TmpStr = TmpStr+"</select>"
			TmpStr=TmpStr+Mid(dateformat, 3,1)
			dateformat = Mid(dateformat,4,len(dateformat))			
		Case "MI"
			TmpStr = TmpStr + "<select size='1' name='" + Fieldname + "_MIN'>"+CrLf
			For a = 0 to 9
				If a=Minute(DateToShowDate) Then TmpSel=" selected" else TmpSel=""
				TmpStr=TmpStr+" <option"+TmpSel+" value='0"+Trim(CStr(a))+"'>0"+Trim(CStr(a))+"</option>"+CrLf
			Next
			For a = 10 to 59
				If a=Minute(DateToShowDate) Then TmpSel=" selected" else TmpSel=""
				TmpStr=TmpStr+" <option"+TmpSel+" value='"+Trim(CStr(a))+"'>"+Trim(CStr(a))+"</option>"+CrLf
			Next
			TmpStr = TmpStr+"</select>"
			TmpStr=TmpStr+Mid(dateformat, 3,1)
			dateformat = Mid(dateformat,4,len(dateformat))			
		Case "SS"

			TmpStr = TmpStr + "<select size='1' name='" + Fieldname + "_SEC'>"+CrLf
			For a = 0 to 9
				If a=Second(DateToShowDate) Then TmpSel=" selected" else TmpSel=""
				TmpStr=TmpStr+" <option"+TmpSel+" value='0"+Trim(CStr(a))+"'>0"+Trim(CStr(a))+"</option>"+CrLf
			Next
			For a = 10 to 59
				If a=Second(DateToShowDate) Then TmpSel=" selected" else TmpSel=""
				TmpStr=TmpStr+"<option"+TmpSel+" value='"+Trim(CStr(a))+"'>"+Trim(CStr(a))+"</option>"+CrLf
			Next
			TmpStr = TmpStr+"</select>"
			TmpStr=TmpStr+Mid(dateformat, 3,1)
			dateformat = Mid(dateformat,4,len(dateformat))			
		
	End select

Wend
CreateDateTimeBox = TMpStr
End function

Function SQLDate(TmpDate)

 If ISThisADate(TmpDate)=False Then TmpDate="1/1/80 00:00:00"
 ThisDate=Cdate(TmpDate)
  X = Month(TmpDate)
  Select Case X
  Case 1
   temp = "Jan"
  Case 2
   temp = "Feb"
  Case 3
   temp = "Mar"
  Case 4
   temp = "Apr"
  Case 5
   temp = "May"
  Case 6
   temp = "Jun"
  Case 7
   temp = "Jul"
  Case 8
   temp = "Aug"
  Case 9
    temp = "Sep"
  Case 10
   temp = "Oct"
  Case 11
   temp = "Nov"
  Case 12
   temp = "Dec"
  Case Else
   temp = "Error"
 End Select

 If temp <> "Error" Then
  SQLDate ="'"& temp & " " & Day(ThisDate) & " " & Year(ThisDate) 
  If InStr(TmpDate,":")>0 Then
  	SqlDate=SqlDate+ " "+LeadZero(CStr(Hour(ThisDate)))+":"+LeadZero(CStr(Minute(ThisDate)))+":"+LeadZero(CStr(Second(ThisDate)))
  End if
  SqlDate = SqlDate & "'" 
 Else
  SQLDate = "'Jan 01 1980'"
 End If
End Function

%>
