<head>
	<title> Modificar Contrase�a</title>
	<link REL="STYLESHEET" TYPE="text/css" HREF="<%=session("progetto")%>/fogliostile.css">
</head>
<script language="Javascript" src="/Include/ControlString.inc"></script>
<script language="Javascript" src="/Include/help.inc"></script>
<script language="Javascript">
	
	var op="<%=Session("password")%>";
	function NoChange() {
			self.close()
		}
	function validar(fr){
			frmPassword.oldpassword.value=TRIM(frmPassword.oldpassword.value)
		if (frmPassword.oldpassword.value == "") {
			alert("Ingresar la contrase�a actual");
			frmPassword.oldpassword.focus();
			return false;
		}
		if (frmPassword.oldpassword.value.toUpperCase()==op.toUpperCase()) {
		}else{
			alert("El campo contrase�a actual es incorrecto");
			frmPassword.oldpassword.focus();
			return false;
		}
		
		frmPassword.newpassword.value =TRIM(frmPassword.newpassword.value)
		
		if (!ChechSingolChar(frmPassword.newpassword.value,"'"))
		{
		alert ("No puede elegir una contrase�a que \ncontenga el caracter ap�strofe");
		frmPassword.newpassword.value = ""
		frmPassword.rpassword.value = ""
		frmPassword.newpassword.focus();
		return false;
		}
		if (!ValidateInputStringIsValid(frmPassword.newpassword.value))
		{
		frmPassword.newpassword.value = ""
		frmPassword.rpassword.value = ""
		frmPassword.newpassword.focus();
		return false;
		}
		if (frmPassword.newpassword.value == "") {
			alert("Ingresar la nueva contrase�a");
			frmPassword.newpassword.focus();
			return false;
		}
		if (frmPassword.newpassword.value.toUpperCase() == op.toUpperCase()) {
			alert("La nueva contrase�a debe ser distinta a la contrase�a actual.");
			frmPassword.newpassword.value = ""
			frmPassword.rpassword.value = ""
			frmPassword.newpassword.focus();
			return false;
		}
		frmPassword.rpassword.value = TRIM(frmPassword.rpassword.value)
		if (frmPassword.rpassword.value == "") {
			alert("Reingresar la nueva contrase�a");
			frmPassword.rpassword.focus();
			return false;
		}
		if (frmPassword.rpassword.value.toUpperCase() == frmPassword.newpassword.value.toUpperCase()) {
		}else{
			alert("La nueva contrase�a no coincide con la reingresada");
			frmPassword.newpassword.focus();
			return false;
		}
		if (frmPassword.newpassword.value.length < 8 ) {
					alert("La nueva contrase�a debe ser de al menos 8 caracteres");
			frmPassword.newpassword.focus();
			return false;
		}
		return true;
	}
</script>

<div align="center">
  <center>
  <%
	sID = Request.form("ident")
	Motivo = Request.QueryString("Tit")
  
	If Motivo = 3 Then
		If Session("DatePwd") = 1 Then
			StitoloInit = "Falta " & Session("DatePwd")	& " d�a "
		Else
			StitoloInit = "Faltan " & Session("DatePwd")	& " d�as "
		End If
		sTitolo= StitoloInit & " para que venza su contrase�a."
	Else
		sTitolo="Su contrase�a ha vencido y debe ser modificada."
	End If

 %>
<table border="0" width="100%" cellspacing="0" cellpadding="0">
	<tr>
		<td>
			<img src="<%=Session("Progetto")%>/images/testatina.gif">
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr class="sfondocomm">
		<td width="100%" valign="bottom" align="center">
			<table border="0" width="100%" height="30" cellspacing="0" cellpadding="0">
				<tr>
					<td width="100%" valign="top" class="textred" align="center">
						<b class="textred"><%=sTitolo%></span></b>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>

<form name="frmPassword" method="post" action="/util/changepassword.asp" onsubmit="return validar(this);">

	<input type="hidden" name="ident" value="<%=Session("idutente")%>">
	<input type="hidden" name="pswtime" value="ok">	 
	<table border="0" cellpadding="1" cellspacing="1" width="370">
		<tr>
			<td align="left" colspan="2" nowrap class="tbltext1"><b>Contrase�a en Uso</b></td>
			<td align="left" colspan="2" width="60%"><input type="password" maxLength="20" name="oldpassword" size="21"></td>
	    </tr>
	       
	    <tr>
	       <td align="left" colspan="2" nowrap class="tbltext1"><b>Nueva Contrase�a</b></td>
	       <td align="left" colspan="2" width="60%"><input type="password" maxLength="20" name="newpassword" size="21"></td>
	    </tr>  
		<tr>
			<td align="left" colspan="2" nowrap class="tbltext1"><b>Reingresar Nueva Contrase�a</b></td>
			<td align="left" colspan="2" width="60%"><input type="password" maxLength="20" name="rpassword" size="21"></td>
		</tr>
	</table> 
	<table border="0" width="100%" cellspacing="0" cellpadding="0">
		<tr>
			<td>&nbsp;</td>
		</tr>
	</table>
	<table border="0" cellpadding="1" cellspacing="1" width="370" align="center">
		<tr>
		<%If Motivo=3 Then %>	
			<td align="right" colspan="2">
				<input type="image" name="Modifica" src="<%=Session("Progetto")%>/images/conferma.gif" border="0" value="Modifica" onclick="return validar(this)">
			</td>
			<td align="left" colspan="2">
				<a href="javascript:NoChange()"><img name="NoChange" src="<%=Session("Progetto")%>/images/chiudi.gif" border="0"></a>
			</td>
		<%Else%>	
			<td align="middle" colspan="2">
				<input type="image" name="Modifica" src="<%=Session("Progetto")%>/images/conferma.gif" border="0" value="Modifica" onclick="return validar(this)">
			</td>
		<%End If%>
		</tr>
	</table>
</form>
</div>

