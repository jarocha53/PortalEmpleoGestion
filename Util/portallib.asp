<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include virtual = "/include/ControlDateVB.asp"-->
<!--#include Virtual = "/include/OpenConn.asp"-->

<% 

Response.Buffer = true

'******************************
'Param mask(name of var),tockeck(desire value of var)
'******************************

Function ValidateMask(mask,tocheck)

	Dim i,f,check,muser

	i=1

	f=Len(tocheck)

	muser=Session(mask)

	check=0

	For i=1 To f

		If mid(tocheck,i,1) = "1" Then

			If mid(muser,i,1)="1" Then

				'Nothing

			Else

				check=1

			End if

		End if

	Next

	If check = 0 Then

	 ValidateMask="true"

	else

	 ValidateMask="false"

	End if 

End Function

'******************************
'Param mask(name of var),tockeck(desire value of var)
'******************************

Function ValidateUserGroup(nomegruppo,Conn)

	Dim check,ig

	check = 0

	if nomegruppo <> "" Then

		'Set Conn = Server.CreateObject("ADODB.Connection")

		'Conn.Open StringConn

		sql = "select idgruppo from gruppo WITH (NOLOCK) where desgruppo like '"+nomegruppo+"'"

'PL-SQL * T-SQL  
' Tolta per ottimizzare le chiamate sql
'UCASESQL = TransformPLSQLToTSQL (UCASESQL) 
		set rcs = Conn.Execute(ucase(sql))

		if rcs.EOF <> "true" Then

			ig = rcs.Fields(0)

			if Session("idgruppo") <> ig then

				check = 1

			end if

		end if

		Set rs=nothing

		'Conn.Close

		'Set Conn=nothing

	end if

	If check = 0 Then

		ValidateUserGroup="true"

	else

		ValidateUserGroup="false"

	End if 

End Function

'******************************
'Param mask(name of var),tockeck(desire value of var)
'******************************

Function ValidateService(idutente,nomefunzione,Conn)

	Dim check,ig,sAutoriz,sIdFun,data
	check = 0
	if idutente = "" then
		response.redirect "/util/error_login.asp"
	end if
	
	ValidateService="true"
	exit function 
	
	sAutoriz = ""
	
	if nomefunzione <> "" or idutente <> "" Then
		'Set Conn = Server.CreateObject("ADODB.Connection")
		'Conn.Open StringConn
		sqlGruppoFun = "select idgruppo,aut_log,idfunzione from gruppofun WITH (NOLOCK) where " & _
			  "idfunzione in (select idfunzione from funzione WITH (NOLOCK) where acronimo like upper('%" & nomefunzione & "%')) and " & _ 
			  "idgruppo in (select idgruppo from utente WITH (NOLOCK) where idutente = " & Clng(idutente) & ")"
		
	'Response.Write(sqlGruppoFun)
'PL-SQL * T-SQL  
'Tolta per ottimizzare le chiamate sql
'UCASESQLGRUPPOFUN = TransformPLSQLToTSQL (SQLGRUPPOFUN) 
		set rcs = Conn.Execute(SQLGRUPPOFUN)

		if not rcs.EOF Then
			ig = Clng(rcs.Fields(0))
			sAutoriz = "" & rcs("aut_log")
			sIdFun =  rcs("idfunzione")
			if Clng(Session("idgruppo")) <> ig then
				check = 1
			end if
		else
			check = 1	
		end if
	else
		check = 1
	end if

' se il gruppo non ha associata la funzione richiesta si verifica che 
' essa sia almeno associata alla persona


	If check = 1 Then
		sqlUteFun="select idutente,aut_log,idfunzione from utefun WITH (NOLOCK) where idutente=" & Clng(Session("idutente")) & " and " & _
	     "idfunzione in (select idfunzione from funzione WITH (NOLOCK) where acronimo like upper('" & nomefunzione & "'))"
			
		
'PL-SQL * T-SQL  
'Tolta per ottimizzare le chiamate sql
'UCASESQLUTEFUN = TransformPLSQLToTSQL (UCASESQLUTEFUN) 
		set rcs = Conn.Execute(ucase(sqlUteFun))

		if not rcs.EOF Then
		    sAutoriz = "" & rcs("aut_log")
			sIdFun = rcs("idfunzione")
			check = 0
		else
			check = 1
		end if
	end if

	If check = 0 Then

		 ValidateService="true"

         if sAutoriz = "S" then 
              on error resume next
              data = now()
              sSQL = "INSERT INTO LOG_FUN(ID_WEBSERVER,ID_SESSIONE,IDUTENTE,IDFUNZIONE," &_
                     "DT_USO,DT_TMST) VALUES('" & Request.ServerVariables("SERVER_NAME") &_
                     "'," & session.SessionID & "," & idutente & "," & sIdFun &_
                     "," & convDatetoDB(now()) & "," & convDatetoDB(now()) & ")"
              on error goto 0         
         end if
       
       'activar servicios por gruppo
		sqlGruppoFunVar="select namevar,valore from gruppofun WITH (NOLOCK) " & _
		     "where idgruppo=" & Clng(Session("idgruppo")) & " and " & _
		     "idfunzione in (select idfunzione from funzione WITH (NOLOCK) where acronimo like upper('" & nomefunzione & "'))"
	
'PL-SQL * T-SQL  
'Tolta per ottimizzare le chiamate sql
'UCASESQLGRUPPOFUNVAR = TransformPLSQLToTSQL (UCASESQLGRUPPOFUNVAR) 
		set rcs = conn.Execute(ucase(sqlGruppoFunVar))
		If rcs.eof Then
			'Nothing
		else
			Do While Not rcs.eof
				If rcs("namevar") <> "" Then
					Session(rcs("namevar")) = rcs("valore")
				end if
				rcs.MoveNext
			Loop	
		End If
		
		'activar servicios por usuario
		sqlUteFunVar="select namevar,valore from utefun WITH (NOLOCK) where idutente=" & Clng(Session("idutente")) & " and " & _
		     "idfunzione in (select idfunzione from funzione WITH (NOLOCK) where acronimo like upper('" & nomefunzione & "'))"
'PL-SQL * T-SQL  
'Tolta per ottimizzare le chiamate sql
'UCASESQLUTEFUNVAR = TransformPLSQLToTSQL (UCASESQLUTEFUNVAR) 
		set rcs = conn.Execute(ucase(sqlUteFunVar))
		
		If rcs.eof Then
			'Nothing
		else
			Do While Not rcs.eof 
				If rcs("namevar") <> "" Then
					Session(rcs("namevar")) = rcs("valore")
				end if
				rcs.MoveNext
			Loop	
		End If

	else
		ValidateService="false"
	End if

	Set rcs=nothing

	'Conn.Close

	'Set Conn=nothing 

End Function

'******************************
'Param namevar
'******************************

Function GetUserVar(namevar)

	GetUserVar=Session(namevar)

End Function

'******************************
'Param namevar,valore
'******************************

Function SetUserVar(namevar,valore,Conn)

	on error resume next

	Dim idutente,idfunzione
	
	idutente = Session("idutente")

	if idutente="" then 

		exit function 

	end if

	idfunzione = "1" 'default_portal
    response.write "codigo.. XXX.."
	if Session(namevar) = "" Then

		sSQL="INSERT INTO utefun (idutente, idfunzione, namevar, valore) VALUES (" & Clng(idutente) & " , " & Clng(idfunzione) & " , '" & namevar & "', '" & valore & "')"

		Session(namevar)=valore

	Else

		sSQL="Update utefun set valore='" & valore & "' where (idutente=" & idutente & " and idfunzione=" & idfunzione & " and namevar like '" & namevar & "')"

		Session(namevar)=valore

	End If

'PL-SQL * T-SQL  
'Tolta per ottimizzare le chiamate sql
'UCASESSQL = TransformPLSQLToTSQL (UCASESSQL) 
	set rs = Conn.Execute(ucase(sSQL))

	Set rs=nothing

end function

'******************************
'Param namevar,valore,idfunzione
'******************************

Function SetUserSectionVar(namevar,valore,idfunzione,Conn)

	on error resume next

	Dim idutente

	idutente = Session("idutente")

	if idutente="" then 

		exit function 

	end if

	if idfunzione="" then 

		exit function

	end if

	if namevar="" then 

		exit function

	end if

	if Session(namevar) = "" Then

		sSQL="Insert Into utefun (idutente,idfunzione,namevar,valore) values (" & idutente & "," & idfunzione & ",'" & namevar & "','" & valore & "')"

		Session(namevar)=valore

	Else

		sSQL="Update utefun set valore='"& valore & "' where (idutente=" & idutente & " and idfunzione=" & idfunzione & " and namevar like '" & namevar & "')"

		Session(namevar)=valore

	End If

'PL-SQL * T-SQL  
'Tolta per ottimizzare le chiamate sql
'UCASESSQL = TransformPLSQLToTSQL (UCASESSQL) 
	set RS = Conn.Execute(ucase(sSQL))

	set rs=nothing

End function

'******************************
'Param 
'******************************

'Function AddUser(nome,cognome,logid,password,email,creator,Conn)

'	Dim idgruppo

'Response.Write nome & 1
'Response.Write cognome & 2
'Response.Write logid & 3
'Response.Write password & 4
'Response.Write email & 5
'Response.Write creator & 6

'	AddUser=False

'	on error resume next

'	if nome = "" then

'		addUser=false

'		exit function

'	end if

'	if cognome = "" then

'		addUser=false

'		exit function

'	end if

'	if password = "" then

'		password = Session.SessionId

'	end if

'	if email = "" then

'		email="no available"

'	end if

'	'Set Conn = Server.CreateObject("ADODB.Connection")

'	'Conn.Open StringConn

'	if logid = "" then	

'		sql = "select nvl(max(idutente),0)+1 from utente"

'	' response.write sql

'		 set rcs = Conn.Execute(ucase(sql))
        
'		id = rcs.Fields(0)

'		logid = left(nome,1) & cognome & id

'		rcs.close
		
'       set rcs = nothing
        
'	end if

'	logid = replace (logid, " ", "")

'	logid = replace (logid, "'", "")

'	logid = replace (logid, """", "")

'	if err <> 0 then

'	   exit function

'	end if

'	sql1 = 	"insert into utente (idutente,nome,cognome,password,email,idgruppo,creator,login) " & _

'		" values (" & id & ",'" & _

'		nome & "','" & _

'		cognome & "','" & _

'		password & "','" & _

'		email & "'," & _

'		20 & "," & creator & ",'" & logid & "')"
	
'      rs=Conn.Execute(ucase(sql1))
 
'	' Conn.close

'	'set Conn = nothing
	
'	if err <> 0 then

'	   exit function

'	 end if

'	AddUser=logid & "|" & Password

'End function

'******************************
'Param 
'******************************

Function AddUserGroup(nome,cognome,tipo,logid,password,email,creator,idg,progetto,uorg,cc)
	'response.write "<br>logid " & logid
	'response.write "<br>password " & password
	
 
	AddUserGroup = false

	 on error resume next
    
    'if IsNull(uorg) then
    '  uorg="*"
    'end if

    if uorg = "" then
       uorg="*"
    end if

     
	if nome = "" then
		exit function
	end if
	'nome = replace (nome, "'", "''") 

	if cognome = "" then
		exit function
	end if
	'cognome = replace (cognome, "'", "''")

	if password = "" then
		password=Session.SessionId
	end if
	
	if logid = "" then	
		exit function
	end if
	 
	'logid = replace (logid, "'", "''")
	'logid = replace (logid, """", "''")  
	 
    if idg = "" then	
		exit function
	end if
	 
    if progetto = "" then	
		exit function
	end if

	if creator = "" then	
		creator=0
	end if	
	
	select case tipo
	    case "P","S","I",""
	       tipo=tipo
	    case else
	        exit function
	end select  
	      
 '  sql = "select idutente from utente where login = '" & logid &"'"
      
'PL-SQL * T-SQL  
'Tolta per ottimizzare le chiamate sql
'UCASESQL = TransformPLSQLToTSQL (UCASESQL) 

   ' set rcs = CC.Execute(ucase(sql))
      
    
   ' if rcs.eof then

       ' err.clear
        
  		sql = " INSERT INTO UTENTE ( NOME,COGNOME,PASSWORD,TIPO_PERS,EMAIL,"
  		
  		if uorg <> "*" then 
  		sql = sql & "ID_UORG,"
  		end if 
  		sql =sql & "IDGRUPPO,LOGIN,CREATOR,DT_ISCRIZIONE,PROGETTO,IND_ABIL,DT_TMST,DT_PWD)" & _

		" VALUES ('"& ucase(Nome) & "','" & _
		 ucase(cognome) & "','" & _
		 
		 password & "','" & _
		 
         ucase(tipo) & "','" & _
         
		 email & "'," 
		
	  if uorg <> "*" then
	   
	     sql=sql & uorg & "," 
	 	 
	  end if 
	  
	    
			 

		sql=sql & idg & ",'" & ucase(logid) & "'," & creator & ",GETDATE(),'" & _
		 ucase(progetto) & "','S',GETDATE(),GETDATE())"
		'response.Write sql
'PL-SQL * T-SQL  
'Tolta per ottimizzare le chiamate sql
'SQL = TransformPLSQLToTSQL (SQL) 
'response.Write sql
		 set rs = cc.Execute(sql)	
         set rs = nothing
  'else
  '  rcs.close
	'set rcs = nothing 		
	'exit function
  'end if
    
  if err <>0 then
     exit function
  end if  

  AddUserGroup =true
  	
End function

'******************************
'No Param 
'******************************

Function logout()

	session.abandon

	Response.redirect "/copertina.htm"

End Function

'******************************
'No Param 
'******************************

Function showallvars()

dim Header, Footer

header = "<table border=""1"" align=""center"" width=""400"">"

footer = "</table>"

Response.Write("cookies" & header)

for each item in Request.Cookies 

		Response.Write("<tr><td>" & item & "</td><td>" & Request.Cookies(item) & "</td>")

		next

Response.Write(footer & "server vars" & header)

for each item in request.servervariables

Response.Write("<tr><td>" & item & "</td><td>" & Request.ServerVariables(item) & "</td></tr>")

next

response.write(footer & "session contents" & header)

for each item in session.contents

if isArray(session(item)) then

Response.Write("<tr><td>" & item & " is Array </td><td>" & showArray(Session(item)) & "</td></tr>")

else

Response.Write("<tr><td>" & item & "</td><td>" & session(item) & "</td></tr>")

end if

next

response.write(footer & "application contents" & header)

'if Len(application.contents) = 0 then

for each item in application.contents

Response.Write("<tr><td>" & item & "</td><td>" & application(item) & "</td></tr>")

next

End Function

'******************************
'No Param 
'******************************

Function ChangePassword(idutente,newpassword,Conn)

	'Set Conn = Server.CreateObject("ADODB.Connection")

	'Conn.Open StringConn

	sSQL="update utente set password='" & newpassword & "' where idutente = " & idutente

	'Response.Write sSQL

'PL-SQL * T-SQL  
'Tolta per ottimizzare le chiamate sql
'UCASESSQL = TransformPLSQLToTSQL (UCASESSQL) 
	set rs = Conn.Execute(ucase(sSQL))

	Session("password") = newpassword

	set rs = nothing

	'Conn.Close

	'Set Conn = nothing

End Function

Function MultiChangePassword(Progetto,Login,Passwd,sNome,sCognome,newpassword)%>

	<!--#include Virtual = "/include/OpenConn.asp"-->

	<%
	
		sSQL="update utente set password='" & UCase(newpassword) & "',dt_pwd=GETDATE() WHERE upper(LOGIN) = '" &	ucase(Login) & "'" &_
					" AND upper(COGNOME) ='" & UCase(sCognome) & "' AND upper(NOME) ='" & Ucase(sNome) & "'" &_
					" AND upper(PASSWORD) = '" & ucase(Passwd) & "'"

		
		cx.BeginTrans
		errore = EseguiNoc(sSQL,cx)
		
		if errore = "0" then
			cx.commitTrans
			MultiChangePassword = True
		else
			cx.RollBackTrans
			MultiChangePassword = False
		end if

		'Session("password") = newpassword

		set rs = nothing
	

	%><!--#include Virtual = "/include/ClosePar.asp"-->

<%End Function

'******************************
'Param mask(name of var),tockeck(desire value of var)
'******************************

Function PaginaGruppo(idgruppo,conn)

	Dim defaultpagina

	defaultpagina="/copertina.htm"

	if idgruppo <> "" Then

		sql = "select webpath from gruppo WITH (NOLOCK) where idgruppo="&idgruppo

'PL-SQL * T-SQL  
'Tolta per ottimizzare le chiamate sql
'UCASESQL = TransformPLSQLToTSQL (UCASESQL) 
		set rcs = Conn.Execute(ucase(sql))

		if rcs.EOF <> "true" Then

			defaultpagina = rcs.Fields(0)

		end if

		Set rs=nothing

	end if

	PaginaGruppo = defaultpagina

End Function

'******************************
'Param mask(name of var),tockeck(desire value of var)
'******************************

public Function ValidateLocalGroup(nar)

Dim find,myArray

myArray = Session(nar)

find = "false"

if isArray(myArray) then

	for i= 0 to Ubound(myArray,1)

	    if Clng(Session("idgruppo"))=myArray(i) then 
	   		
	   		find = "true"

	    end if

	Next

end if

ValidateLocalGroup  = find

end Function

'******************************
'Param mask(name of var),tockeck(desire value of var)
'******************************

Function GetSectionVar(namefunzione,namevar)

Dim MyPos,astring
Dim result
result = "false"

Dim anArray
anArray = Session(namefunzione)

if isArray(anArray) and namevar <> "" then

	For i = 0 to UBound(anArray,1)  ' achar la string que quiero encontrar

	astring=anArray(i)             ' astring donde quiero buscar la string
  	partes=split(astring,"|")

  	if partes(0) = namevar then
	  	result = partes(1)
	  	i = UBound(anArray,1)
	end if

	next
end if

GetSectionVar = result

end Function

'******************************
'Param
'******************************

Public Function StringConn()

		'via OLE DB Microsoft su Oracle

		'StringConn = "Provider=MSDAORA.1;user id=portale; password=portale; data Source=portale"		

		'via ODBC su Oracle

		'StringConn = "DSN=portale_ora; user id=portale; password=portale"

		'via OLE DB Oracle

		StringConn = "Provider=OraOLEDB.Oracle.1;user id=plavoro; password=plavoro;data Source=plavoro"
		'via ODBC su Access

	'	StringConn = "DSN=dbportale"

End Function

'******************************
'Param
'******************************

Function addElement2ArrayFunction(namefun,nombre,valor,actu)

Dim Dimensione,resulta
Dim sa,enc

if namefun <> "" then
	sa = Session(namefun)
end if

if isArray(sa) then 
	enc = -1
	Dimensione=UBound(sa,1) 
	for i=0 to Dimensione
	     if sa(i) = "" then
	        sa(i)=nombre & "|" & valor 
		resulta = sa
		enc = i
	     else 
		if actu=true then
			partes=split(sa(i),"|")
			if partes(0)=nombre then
				sa(i)=nombre & "|" & valor
				resulta = sa
				enc = i
			end if
	     	end if
	     end if
	Next
	if enc < 0 then
		Redim Preserve sa(Dimensione+1)
		sa(Dimensione+1)=nombre & "|" & valor		
		resulta = sa
	end if
else
	Dim saa(1)
	saa(0)=nombre & "|" & valor
	resulta = saa
end if
addElement2ArrayFunction = resulta
End function

'******************************
'Param
'******************************

Function showArray(anArray)
Dim ret

 ret = "<table border=0><td>"
 For i = LBound(anArray,1) to UBound(anArray, 1)
  ret = ret & "<br>" & anArray(i)
 Next
 ret = ret & "</td></table>"

showArray = ret

end function

'******************************
'Param
'******************************

Function WriteLoginInfo

	Const ForWriting = 2
	Const ForAppend = 8
	Dim fs, ListFile, folderspec,filespec

    	folderspec = Request.ServerVariables("APPL_PHYSICAL_PATH")	
    	filespec = folderspec & "util\login.log"
   
	Set fs = Server.CreateObject("Scripting.FileSystemObject")
	Set ListFile = fs.OpenTextFile(filespec, ForAppend, True)
	
	ListFile.WriteLine Session("login") & "," & Request.ServerVariables("LOCAL_ADDR") & "," & date()

	ListFile.close
	Set ListFile = Nothing
	Set fs = Nothing
	
End Function

Function InsProj(id_persona,progetto,idutente,cc)
  if idutente="" then idutente="0"
  
  sqlproj = " SELECT id_persona,cod_cproj FROM pers_proj WITH (NOLOCK) " &_
            " WHERE id_persona =" & id_persona & " and cod_cproj = '" & progetto & "' "
  
'PL-SQL * T-SQL  
'Tolta per ottimizzare le chiamate sql
'SQLPROJ = TransformPLSQLToTSQL (SQLPROJ) 
   set rsproj = cc.Execute(sqlproj)

		if not rsproj.EOF Then
		
			rsproj.close
		
			exit function
		
		else
			sqlinsproj = " INSERT INTO pers_proj(id_persona,cod_cproj,dt_iscrizione,id_utente,dt_tmst) " &_
			          " VALUES (" & id_persona & ",'" & progetto & "',getdate()," & idutente & ",getdate())"
			
'PL-SQL * T-SQL  

'Tolta per ottimizzare le chiamate sql
'SQLINSPROJ = TransformPLSQLToTSQL (SQLINSPROJ) 
            set rsinproj = cc.Execute(sqlinsproj)
		    
			Set rsinproj = Nothing       
		end if
		
    Set rsproj = Nothing
End Function


Function AddUserGroup_V(nome,cognome,tipo,logid,password,email,tipodoc,nrodoc,observ,creator,idg,progetto,uorg,cc)

    AddUserGroup_V = false

	 on error resume next
    
    'if IsNull(uorg) then
    '  uorg="*"
    'end if

    if uorg = "" then
       uorg="*"
    end if

     
	if nome = "" then
		exit function
	end if
	'nome = replace (nome, "'", "''") 

	if cognome = "" then
		exit function
	end if
	'cognome = replace (cognome, "'", "''")

	if password = "" then
		password=Session.SessionId
	end if
	
	if logid = "" then	
		exit function
	end if
	 
	'logid = replace (logid, "'", "''")
	'logid = replace (logid, """", "''")  
	 
    if idg = "" then	
		exit function
	end if
	 
    if progetto = "" then	
		exit function
	end if

	if creator = "" then	
		creator=0
	end if	
	
	select case tipo
	    case "P","S","I",""
	       tipo=tipo
	    case else
	        exit function
	end select  
	      
 '  sql = "select idutente from utente where logid = '" & logid &"'"
      
'PL-SQL * T-SQL  
'Tolta per ottimizzare le chiamate sql
'UCASESQL = TransformPLSQLToTSQL (UCASESQL) 
   ' set rcs = CC.Execute(ucase(sql))
      
    
   ' if rcs.eof then

       ' err.clear
        
  		sql = " INSERT INTO UTENTE ( NOME,COGNOME,PASSWORD,TIPO_PERS,EMAIL,"
  		
  		if uorg <> "*" then 
  		sql = sql & "ID_UORG,"
  		end if 
  		sql =sql & "IDGRUPPO,LOGIN,CREATOR,DT_ISCRIZIONE,PROGETTO,IND_ABIL,DT_TMST,TIPODOCUMENTO,NRODOCUMENTO,OBSERVACIONES)" & _

		" VALUES ('"& ucase(Nome) & "','" & _

		 ucase(cognome) & "','" & _

		 password & "','" & _
		 
         ucase(tipo) & "','" & _
         
		 email & "'," 
		
	  if uorg <> "*" then
	   
	     sql=sql & uorg & "," 
	 	 
	  end if 
	  
	    	 

		sql=sql & idg & ",'" & ucase(logid) & "'," & creator & ",GETDATE(),'" & _
		 ucase(progetto) & "','S',GETDATE()," & tipodoc & "," & nrodoc & ",'" & observ & "')"
		
		'Response.Write sql
		'Response.End 
		
'PL-SQL * T-SQL  
'Tolta per ottimizzare le chiamate sql
'SQL = TransformPLSQLToTSQL (SQL) 
		 set rs = cc.Execute(sql)	
         set rs = nothing
  'else
  '  rcs.close
	'set rcs = nothing 		
	'exit function
  'end if
    
  if err <>0 then
     exit function
  end if  

  AddUserGroup_V =true
  	
End function
%>
