<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%
Dim toplevel,fsDir,fsDir2,fsRoot,fsDiro,fsDir2o
Dim f,fso,fn
Dim Filelist

Dim DIRROOT2
DIRROOT2="d:\sviluppo\"

	Dim gblSiteName,gblSiteCode
	gblSiteName = Request.ServerVariables("SERVER_NAME")
	gblSiteCode = ""

	Dim gblNow 'server may not be local time
	gblNow = Now

	Dim gblFace,gblColor	'needs three quotes
	gblFace = """Verdana, Arial, Helvetica, sans-serif"""
	gblColor = """#008caa"""

	Dim gblRed
	gblRed = """#FF0000"""
	
	Dim gblGreen
	gblGreen = """#003843"""

	Dim gblReverse
	gblReverse = """#E0E0E0"""

	'global variables

	Dim gblTitle,gblPageText
	gblTitle = " * * * PortalMaker + Parser Interface  * * * "
	gblPageText = "&nbsp;"

	'global constants

	Dim gblScriptName
	gblScriptName = Request.ServerVariables("Script_Name")
	gblScriptName = Mid(gblScriptName,InstrRev(gblScriptName,"/") + 1)




if Request.QueryString("d") <> "" then
	fsDir = Request.QueryString("d")
	fsDiro = LCase(fsDir)
	fsDiro = replace(fsDiro,"\\","\")	
else
	fsDir = Request.ServerVariables("APPL_PHYSICAL_PATH")
	fsDiro = LCase(fsDir)
end if
if Request.QueryString("d2") <> "" then
	fsDir2 = Request.QueryString("d2")
	fsDir2o = LCase(fsDir2)
	fsDir2o = replace(fsDir2o,"\\","\")	
else
	fsDir2 = Request.ServerVariables("APPL_PHYSICAL_PATH")		
	fsDir2o = LCase(fsDir2)
end if

fsDir = replace(LCase(fsDir),"/../","/")
fsDir2 = replace(LCase(fsDir2),"/../","/")
fsRoot = LCase(Request.ServerVariables("APPL_PHYSICAL_PATH"))

Set fso = CreateObject("Scripting.FileSystemObject")

'--
' Navigate
Sub Navigate (frame)
	Dim emptyDir

	emptyDir = TRUE
	response.write "<TABLE BORDER=0 CELLPADDING=2 CELLSPACING=3 WIDTH=""100%"">"
	response.write "<TR><TD VALIGN=""TOP"">&nbsp;</TD>" & VBCRLF
	response.write "<TD COLSPAN=3><FONT FACE=" & gblFace & " COLOR=" & gblGreen & " SIZE=4><B>"

	Select Case frame
	Case "1"
		response.write "SOURCE"
	Case "2"
		response.write "TARGET"
		fsDir = fsDir2								
	end select
	
	response.write "</B></FONT></TD>" & VBCRLF
	response.write "</TR>" & VBCRLF

	' get the directory of file names
	If Lcase(fsDir) = Lcase(fsRoot) Then toplevel = TRUE
	If frame > 1 and Lcase(fsDir2)<>DIRROOT2 Then 
		toplevel = FALSE
	end if

	If toplevel Then
		parent = ""
	Else
		response.write "<TR><TD VALIGN=""TOP"" ALIGN=""RIGHT""><FONT FACE=""WingDings"" SIZE=4 COLOR=" & gblRed & ">" & chr(199) & "</FONT></TD>" & VBCRLF
		if frame = 1 then
			parent = fso.GetParentFolderName(fsDiro) & "\"		
			response.write "<TD COLSPAN=3><FONT FACE=" & gblFace & " SIZE=1><B><A TITLE=""Cliccare qui per spostare alla cartella del livello superiore."" HREF=""" & gblScriptName & "?d=" & URLSpace(parent) & "&d2=" & URLSpace(fsDir2o)& """>Cartella del livello superiore</A></B></FONT></TD></TR>" & VBCRLF
		else
			parent = fso.GetParentFolderName(fsDir2o) & "\"
			response.write "<TD COLSPAN=3><FONT FACE=" & gblFace & " SIZE=1><B><A TITLE=""Cliccare qui per spostare alla cartella del livello superiore."" HREF=""" & gblScriptName & "?d2=" & URLSpace(parent) & "&d=" & URLSpace(fsDiro)& """>Cartella del livello superiore</A></B></FONT></TD></TR>" & VBCRLF
		end if
	End If
	Set f = fso.GetFolder(fsDir)
	Set FileList = f.subFolders
	a = 0
	For Each fn in FileList
		emptyDir = FALSE
		If a = 0 Then
			a = 1
			response.write "<TR><TD VALIGN=""TOP"">&nbsp;</TD>" & VBCRLF
			response.write "<TD COLSPAN=3><FONT FACE=" & gblFace & " SIZE=1><B>" & fsDir & "</B></FONT></TD>" & VBCRLF
			response.write "</TR>" & VBCRLF
			response.write "<TR><TD VALIGN=""TOP"">&nbsp;</TD>" & VBCRLF
			response.write "<TD COLSPAN=3 VALIGN=""BOTTOM""><FONT FACE=" & gblFace & " COLOR=" & gblRed & " SIZE=1><B>NOME CARTELLA</B></FONT></TD>" & VBCRLF
			response.write "</TR>" & VBCRLF
		End If
		DisplayFileName "DIR",fn,frame
	Next 'fn

'	if frame = 1 then

	response.write "<TR><TD VALIGN=""TOP"">&nbsp;</TD>" & VBCRLF
	response.write "<TD COLSPAN=""3"" VALIGN=""BOTTOM""><FONT FACE=" & gblFace & " COLOR=" & gblRed & " SIZE=1><B>NOME DOCUMENTO</B></FONT></TD>" & VBCRLF
	response.write "</TR>" & VBCRLF

	Set filelist = f.Files
	For Each fn in filelist
		emptyDir = FALSE
		DisplayFileName "FILE",fn,frame
	Next 'fn

'	end if 'frame = 1

	If emptyDir Then
		response.write " <TR> "& VBCRLF
		response.write " <TD COLSPAN=4><FONT FACE=" & gblFace & " SIZE=1><B>" & fsDir & "</B></FONT></TD>" & VBCRLF		
		response.write " </TR>" & VBCRLF
	End If
	response.write "</TABLE>" & VBCRLF
End Sub 'Navigate

'--
' DisplayFileName
Sub DisplayFileName(dirfile,fhandle,frame)
Dim newgif,linktarget
Dim fsize

	
	If dirFile="DIR" Then
		response.write "<TR>" 
		if frame = 1 then
		linktarget = "<A HREF=""" & gblScriptName & "?d=" & URLSpace(fhandle) & "\&d2=" & URLSpace(fsDir2o)& """ TITLE=""Click here to move down a level and list the documents in this folder."">"
		else
		linktarget = "<A HREF=""" & gblScriptName & "?d2=" & URLSpace(fhandle) & "\&d=" & URLSpace(fsDiro)& """ TITLE=""Click here to move down a level and list the documents in this folder."">"
		end if
		tstr = "<FONT FACE=" & gblFace & " SIZE=2>" & linktarget & LCase(fhandle.name) & "</A></FONT>"
		response.write "<TD VALIGN=""TOP"" ALIGN=""RIGHT"">" & MockIcon("fldr")
		response.write "</TD>"
		response.write "<TD COLSPAN=3 VALIGN=""TOP"" BGCOLOR=" & gblReverse & ">" & Tstr & VBCRLF
		response.write "</TD>" & VBCRLF
		
	Else
		if fhandle.name="fileman.asp" then
		else
		newgif = ""
'		If fhandle.datelastmodified+14>gblNow Then newgif = MockIcon("newicon")
		b = ""
		If len(fhandle.name)>4 Then b = Ucase(Right(fhandle.name,4))
		If Left(b,1) = "." Then b = Right(b,3)
		
		if frame = 1 then
			Select Case b
			Case "ASP","HTM","HTML"
			newgif = newgif & " <A HREF=javascript:marca(""" & fhandle.name & ""","&frame&") TITLE=""Click here to select de file for parsing."" STYLE=""{text-decoration:none}"" >" & MockIcon("edit") & "</A>"
				tstr = "/"&replace(fsDiro,fsRoot,"")&replace(fhandle.name," ","%20")
			End Select
		end if 'frame=1

		If fhandle.size<10240 Then
			If fhandle.size=0 Then
				fsize = "0"
			Else
				fsize = FormatNumber(fhandle.size,0,0,-2)
			End If
		Else
			fsize = FormatNumber((fhandle.size+1023)/1024,0,0,-2) & "K"
		End If
		if frame = 1 then
			Select Case b
			Case "ASP","HTM","HTML"
				response.write "<TR>" & VBCRLF
				tstr = "<FONT FACE=" & gblFace & " SIZE=2><A HREF=""" & tstr & """ TITLE=""Click here to link to this document."">" & LCase(fhandle.name) & "</A></FONT>" & newgif
				response.write "<TD VALIGN=""TOP"" ALIGN=""RIGHT""></TD>" & VBCRLF
				response.write "<TD COLSPAN=""3"" VALIGN=""TOP"" BGCOLOR=" & gblReverse & ">" & Tstr & "</TD>" & VBCRLF
				response.write "</TR>" & VBCRLF			
			End Select
		else
			response.write "<TR>" & VBCRLF
			tstr = "<FONT FACE=" & gblFace & " SIZE=2>" & LCase(fhandle.name) & "</FONT>" & newgif
			response.write "<TD VALIGN=""TOP"" ALIGN=""RIGHT""></TD>" & VBCRLF
			response.write "<TD COLSPAN=""3"" HEIGHT=""24"" VALIGN=""TOP"" BGCOLOR=" & gblReverse & ">" & Tstr & "</TD>" & VBCRLF
			response.write "</TR>" & VBCRLF	
		end if 'frame=1
		end if 'fileman.asp
	End If 
End Sub 'DisplayFileName

'--
' URLspace
Function URLSpace(s)
	URLSpace = replace(replace(s,"+","%2B")," ","+")
End Function 'URLSpace

'--
' MockIcon (icon emulator)
Function MockIcon(txt)
Dim tstr,d

	'Sorry, mac/linux users.
	tstr = "<FONT FACE=""WingDings"" SIZE=4 COLOR=" & gblRed & ">"
	Select Case Lcase(txt)
	Case "bmp","gif","jpg","tif","jpeg","tiff"
		d = 176
	Case "doc"
		d = 50
	Case "exe","bat","bas","c","src"
		d = 255
	Case "file"
		d = 51
	Case "fldr"
		d = 48
	Case "htm","html","asa","asp","cfm","php3"
		d = 182
	Case "pdf"
		d = 38
	Case "txt","ini"
		d = 52
	Case "xls"
		d = 252
	Case "zip","arc","sit"
		d = 59
	Case "newicon"
		tstr = "<FONT TITLE=""Questo documento � stato modificato negli ultimi 14 giorni."" FACE=""WingDings"" SIZE=4 COLOR=" & gblRed & ">"
		d = 171
	Case "view"
		d = 52
	Case "edit"
		d = asc("?")
	Case "conv"
		d = 220
	Case Else
		d = 51
	End Select
	tstr = tstr & Chr(d) & "</FONT>"
	MockIcon = tstr
End Function 'mockicon


%>
<html>
<title>
<%=gblTitle%>
</title>
<link rel="stylesheet" href="/fogliostile.css" type="text/css">
<script language=javascript>
<!--
<%
	Response.write "var sel=""" & replace(fsDiro,"\","\\") & """;"
%>

function validar(){
	if (sel == parser.target.value) {
		alert ("Fuente = Objetivo : Prohibido")
		return false;
	}
	if (confirm("Make parse?")){
		var mw = window.open("parser.asp?source="+parser.source.value+"&target="+parser.target.value+"&recursive="+parser.recursive.checked,'w1','width=350,height=400');
		if (!mw.opener)
	        	mw.opener = self;
        }
	return false;
	
}
function marca(t,s){
	if (s==1){
		parser.source.value=sel+t;
		location.href="#top";
	}
	
}
//-->
</script>

<body>
<a name="top"></a>
<h1 align="center" class="tbllabel">PortalMaker + Parser Interface </h1>
<div align="center">
<form name=parser method="post" action="/util/parser.asp" onsubmit="return validar()">
    <table cellpadding="2" cellspacing="0" border="0" bgcolor="#FFFFFF">
      <tr height="20"> 
        <td width="40%" class="tbltext" align="right" > 
          <font face="Arial, Helvetica, sans-serif" class="tbltext">Source 
            (file|directory)</font>
        </td>
        <td colspan="2"> 
          <input type="text" size=80 name="source" value="<%=fsDiro%>" class="tbltext">
        </td>
      </tr>
      <tr height="40"> 
        <td class="tbltext" align="right" height="20"> 
          <font face="Arial, Helvetica, sans-serif" class="tbltext">Directory 
            for Output</font>
        </td>
        <td colspan="2"> 
          <input type="text" size=80 name="target" value="<%=fsDir2o%>" class="tbltext">
        </td>
      </tr>
      <tr> 
        <td class="tbltext" height="4" align="right"> 
          <font face="Arial, Helvetica, sans-serif" class="tbltext">Non Recursive 
            Directoy Parsing</font>
        </td>
        <td colspan="2" class="tbltext"> 
        <input type="checkbox" name="recursive"> Enable? 
        </td>
      </tr>
      <tr>
        <td colspan="3">
        <div align="right">
          <input type=submit value="Execute Parsing" name="submit">
        </div>
        </td>
      </tr>
    </table>
    </form>
</div>

    <table width="90%" border="1" align="center">
    <tr>
    <td width="50%" valign="top">
    	<!-- Mostrar la estructura de directorios/files origen -->
    	<%
    		Navigate 1
    	%>
    </td>
    <td valign="top">
    	<!-- Mostrar la estructura de directorios destino -->
    	<%
    		Navigate 2
    	%>
    </td>
    </tr>
    </table>
</body>
</html>
