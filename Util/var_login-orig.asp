<html>
<head>
<link REL="STYLESHEET" TYPE="text/css" HREF="<%=Session("Progetto")%>/fogliostile.css">
<title>Solicitud de Reenvio de la Contraseņa</title>
</head>
<script language="Javascript" src="/Include/ControlString.inc"></script>
<script language="Javascript" src="/Include/help.inc"></script>
<script language="Javascript">
	function checkField(frmpw){
		
		frmpw.pwcognome.value=TRIM(frmpw.pwcognome.value.toUpperCase())
		if (frmpw.pwcognome.value == "") {
			alert("El apellido es obligatorio");
			frmpw.pwcognome.focus();
			return(false);
		}	
		frmpw.pwnome.value=TRIM(frmpw.pwnome.value.toUpperCase())
		if (frmpw.pwnome.value == "") {
		        alert("El nombre es obligatorio"); 
			frmpw.pwnome.focus();
			return(false);
		}
		
		frmpw.pwLogin.value=TRIM(frmpw.pwLogin.value.toUpperCase())
     	if (frmpw.pwLogin.value == "") {
			alert("El nombre de usuario es obligatorio");
			frmpw.pwLogin.focus();
			return(false);
		}
  }	
</script>
<body topmargin="0" rightMargin="0" leftMargin="10">
<!--#include virtual ="/include/openconn.asp"-->
<table border="0" width="100%" style="WIDTH: 100%px">
    <tr>
        <td align="left" colspan="2">
			<table border="0" width="371" cellspacing="0" cellpadding="0">
				<tr>
					<td>
						<img src="<%=Session("Progetto")%>/images/mtcollogo.jpg" width="370" height="59">
					</td>
				</tr>
				<tr>
					<td width="371" valign="bottom" align="right">
						<table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
							<tr>
								<td width="100%" valign="top" align="right">
									<b class="tbltext1a">Solicitud de la contraseņa &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<table border="0" cellspacing="1" cellpadding="1" width="98%" class="tblsfondo">
				<tr class="sfondocomm">
					<td colspan="2" class="tbltext1">Esta pantalla de solicitud debe ser utilizado unicamente 
					por aquellos que olvidaron o extraviaron <strong> la CONTRASEŅA</strong>.
				
						<a href="Javascript:Show_Help('/pgm/help/Util/Var_Login')" name onmouseover="javascript:status='' ; return true">
						<img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
					</td>
			    </tr>
			</table>

			<form method="post" name="frmpw" onsubmit="return checkField(this);" action="vispsw.asp">
			<table border="0" cellpadding="1" cellspacing="1" width="100%">
			      <tr>
					<td width="5%" rowspan="3">&nbsp;</td>
			        <td colspan="2" class="tbltext1">&nbsp;<b>Apellido</b></td>
			        <td colspan="2" width="70%" align="left">
						<input size="35" maxlength="50" name="pwcognome" style="TEXT-TRANSFORM: uppercase;">
			        </td>
			      <tr>
			        <td colspan="2" class="tbltext1">&nbsp;<b>Nombre</b> </td>
			        <td colspan="2" width="70%" align="left">
						<input size="35" maxlength="50" name="pwnome" style="TEXT-TRANSFORM: uppercase;">
			        </td>
			      </tr>
			      <tr>
			        <td colspan="2" class="tbltext1">&nbsp;<b>Usuario</b></td>
			        <td colspan="2" width="70%" align="left">
						<input size="35" maxlength="50" name="pwLogin" style="TEXT-TRANSFORM: uppercase;">
			        </td>
			     </tr>
			</table>
				<!-- Pulsante di conferma -->        
			<table border="0" cellpadding="1" cellspacing="1" width="100%">
			    <tr>
			        <td align="middle" width="60%">&nbsp;</td>
			    </tr>
			    <tr>
			        <td align="center" width="60%"><p align="center">
						<input type="image" src="<%=Session("Progetto")%>/images/conferma.gif" name="Invia" value="Richiedi"></p>
			        </td>
			    </tr>
			</table>
			</form>
<!--#include virtual ="/include/closeconn.asp"-->
</body>
</html>
