<%@LANGUAGE="VBSCRIPT" CODEPAGE="1254"%>
<%
'Response.CacheControl = "no-cache"
'Response.AddHeader "Pragma", "no-cache"
'Response.Expires = -1
%>
<%
Function CheckCAPTCHA(valCAPTCHA)
	SessionCAPTCHA = Trim(Session("CAPTCHA"))
	Session("CAPTCHA") = vbNullString
	if Len(SessionCAPTCHA) < 1 then
        CheckCAPTCHA = False
        exit function
    end if
	if CStr(SessionCAPTCHA) = CStr(valCAPTCHA) then
	    CheckCAPTCHA = True
	else
	    CheckCAPTCHA = False
	end if
End Function
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-9" />
<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
<META HTTP-EQUIV="Expires" CONTENT="-1">
<title>ASP Security Image Generator (CAPTCHA) v2.0</title>
</head>

<body>
 <form id="f_example" name="f_example" method="post" action="examplecaptcha.asp">
   <table border="0" cellpadding="2" cellspacing="2" width="500">
<!-- <table width="350" height="187" border="1" align="center"> -->
    <tr>
      <td width="152" height="32">
         <span class="tbltext1">
		 <strong>C&oacute;digo de registro*</strong>
		</span>
      </td>
      <td width="182"><img src="../../../../../Util/aspcaptcha.asp" width="86" height="21" /></td>
    </tr>
<%
if Request.ServerVariables("REQUEST_METHOD") = "POST" then
	strCAPTCHA = Trim(Request.Form("strCAPTCHA"))
	response.write "Alex= " & CheckCAPTCHA(strCAPTCHA)
	if CheckCAPTCHA(strCAPTCHA) = true then
		%>
    <tr>
      <td height="37" colspan="2" align="center"><b style="color:#00CC00">OK (<%=strCAPTCHA%>)</b></td>
    </tr>		
		<%
	else
		%>
    <tr>
      <td height="37" colspan="2" align="center"><b style="color:#FF0000">Incorrecto<br />Intente de nuevo.</b></td>
    </tr>
		<%
	end if 
end if
%>
    <tr>
      <td height="66">
      			<span class="tbltext1">
				<strong>Ingrese este c&oacute;digo en la casilla*</strong>
				</span>

      </td>
      <td><input class="textblacka" name="strCAPTCHA" type="text" id="strCAPTCHA" maxlength="8" /></td>
    </tr>
    <tr>
      <td height="37" colspan="2" align="center"><input type="submit" name="Submit" value="Verificar" /></td>
    </tr>
  </table>
</form>   

</body>
</html>
