<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%			

'*****************************************************************************
'	Funciones de Login - Usar como includes
'*****************************************************************************
'============================================================================
' Verifica el login de un usuario
'	y setea las variables de session
'
'OJO: Funcion publica para ingresar al portal
'----------------------------------------------------------------------------
'Necesita:
'	Login	Nombre del usuario
'	Passwd	clave del mismo
'Devuelve	
'	Status	= "0" --> Usuario aceptado
'			  <>  	  Rechazado
'	Mensaje			Mensaje de Error
'	HTMLsalto		Direcicon del salto a paginas de control
'============================================================================
function LoginControl(Login, Passwd, Status, Mensaje, HTMLsalto)

	Login = trim(Login)

	' Control en personas
	LoginControlPersonas Login, Passwd, Status, Mensaje
	if Status = "0" then
		Login = trim(Login)
		' Armado de las varialbes del entorno del portal
		LoginEntornoPortal Login, Status, Mensaje, HTMLsalto
	end if	
	
end function


'============================================================================
' Para un usuario valido por Nro de Id 
'	Arma varialbes de session
'	Usada para interfases entre modulos
'
'OJO: Funcion publica para ingresar al portal
'----------------------------------------------------------------------------
'Necesita:
'	IdUsuario	Id del usuario
'Devuelve	
'	Status	= "0" --> Usuario aceptado
'			  <>  	  Rechazado
'	Mensaje			Mensaje de Error
'	HTMLsalto		Direcicon del salto a paginas de control
'============================================================================
function LoginControl_IdUsuario(IdUsuario, Status, Mensaje, HTMLsalto)
	dim Login, Passwd,Usuario
	Login = ""
	Passwd = "" 
	Status = "997"
	' Busca el nombre y clave del usuario solicitado
	strSQl = "Select A.UserName, A.[Password] From Usuario A where A.UsuarioID =" & IdUsuario
	'strSQl =  "Select A.UserName, A.Clave From Usuarios A where A.Usuario="  & IdUsuario
	Set rstUsuario = Server.CreateObject("ADODB.RECORDSET")
'PL-SQL * T-SQL  
' Conexi�n a SQL2000 - STRSQL = TransformPLSQLToTSQL (STRSQL) 
	rstUsuario.open strSQl, gConnSeguridadReplica, 3, 3
	if rstUsuario.recordcount > 0 then
		Login = rstUsuario("UserName")
		Login = trim(Login)
		' Arma varialbes del entorno
		LoginEntornoPortal Login, Status, Mensaje, HTMLsalto
	end if
	rstUsuario.close
	
end function


'============================================================================
' Verifica un usuario en Personas
'
'OJO : Esta funcion debe ser privada por razones de seguridad
'----------------------------------------------------------------------------
'Necesita:
'	Login	Nombre del usuario
'	Passwd	clave del mismo
'Devuelve	
'	Status	= "0" --> Usuario aceptado
'			  <>  	  Rechazado
'	Mensaje			Mensaje de Error
'	HTMLsalto		Direcicon del salto a paginas de control
'============================================================================
private function LoginControlPersonas(Login, Passwd, Status, Mensaje)
	dim Control
	
	Status = "998"
	Mensaje = ""
	Login = trim(Login)
	
	'--=====================================================
	'	Verifica usuario - Usuario Unificado MTSS
	'--=====================================================
	'..control = "Saltar"
	control = "NoSaltar"
	
	if control <> "Saltar" then
		Dim cmdUsuario, xStatus, xMensaje, xUsuarioId, xNombre, xApellido

		Set cmdUsuario = Server.CreateObject("ADODB.Command")
		With cmdUsuario
			.ActiveConnection = gConnSeguridadReplica
			.CommandTimeout = 1200
			.CommandText = "spLogin"
			.CommandType = 4
			'-------------------------------------------------
			' Parametros de Entrada������� 
			'-------------------------------------------------
			.Parameters("@UserName").Value = Login
			.Parameters("@Password").Value = Passwd
			.Parameters("@SistemaId").Value = 11
'PL-SQL * T-SQL  
			.Execute()
			'-------------------------------------------------
			' Parametros de Output
			'-------------------------------------------------
			xUsuarioId = .Parameters("@UsuarioId").value
			xNombre = .Parameters("@Nombre").value
			xApellido = .Parameters("@Apellido").value
			xStatus = .Parameters("@Status").value
			xMensaje = .Parameters("@Mensaje").value

			'-------------------------------------------------
			' Verifica si puede entrar
			'-------------------------------------------------
			if xStatus <> "1" Then
				Status ="100"
				Mensaje = xMensaje 
				exit function
			End If
		End With
	End if

	'=================================================
	' Termino Bien -> Usuario Aceptado 
	'=================================================
	Status = "0"	
	Mensaje = "Usuario Aceptado"
end function

private function LoginEntornoPortal(Login, Status, Mensaje, HTMLsalto)
'============================================================================
' Para un Usuario Validado Arma las variables de session
'	del entorno del portal
'
'OJO : Esta funcion debe ser privada por razones de seguridad
'----------------------------------------------------------------------------
'Necesita:
'	Login	Nombre del usuario
'Devuelve	
'	Status	= "0" --> Usuario aceptado
'			  <>  	  Rechazado
'	Mensaje			Mensaje de Error
'	HTMLsalto		Direcicon del salto a paginas de control
'============================================================================
	Status = "999"
	Mensaje = ""
	
	'-------------------------------------------------------
	' Debe venir el nombre del usuario
	'-------------------------------------------------------
	if len(Login) = 0 then
		Status ="1"
		Mensaje = "Usuario no Valido para el entorno del portal"
		exit function
	end if
	
	'-------------------------------------------------------
	' Sigue con lo que estaba
	'-------------------------------------------------------
		
	Set rstUtente = Server.CreateObject("ADODB.RECORDSET")
' Comienzo Modificacion M3
	'SQLUtente = "SELECT u.idutente, u.nome, u.cognome, u.email, u.password, u.idgruppo, u.tipo_pers, u.dt_pwd, NVL(SYSDATE-u.dt_pwd,0)as giorni, " & _
	'			" u.creator, u.login, u.ind_abil, g.desgruppo, g.cod_rorga, g.webpath, nvl(u.id_uorg,0) id_uorg, g.cod_ruofu " & _
	'			" FROM utente u, gruppo g" & _
	'			" WHERE ltrim(rtrim(u.login)) = '" & Ucase(Login) & "' AND u.idgruppo = g.idgruppo "

	SQLUtente = "SELECT    U.IDUTENTE, U.NOME,U.COGNOME,U.EMAIL,U.PASSWORD,U.IDGRUPPO,U.TIPO_PERS,U.DT_PWD, ISNULL(DATEDIFF(DD, U.DT_PWD, GETDATE()), 0) AS GIORNI, " & _
				" U.CREATOR, U.LOGIN, U.IND_ABIL,G.DESGRUPPO, G.COD_RORGA, G.WEBPATH, ISNULL(U.ID_UORG, 0) ID_UORG, G.COD_RUOFU  " & _
				" FROM utente u, gruppo g" & _
				" WHERE ltrim(rtrim(u.login)) = '" & Ucase(Login) & "' AND u.idgruppo = g.idgruppo "

'PL-SQL * T-SQL  
'SQLUTENTE = TransformPLSQLToTSQL (SQLUTENTE) 
' Fin Modificacion M3
	rstUtente.open SQLUtente, CC, 3, 3
	'Response.Write sqlutente
	'response.End 

	'---------------------------------------------------------------------------
	' cuando viene de proyectos, puede darse el caso que no este el usuario dado
	' de alta en ningun grupo del portal
	'----------------------------------------------------------------------------
	If rstUtente.EOF Then
		Status ="2"
		Mensaje = "El usuario " & Login & " - NO est� registrado en el Portal." & SQLUtente
		exit function
	End If
	
	'---------------------------------------------------------
	' Para mantener compatibilidad MTSS
	'---------------------------------------------------------
	' La clave se toma la registrada en ITlavoro para que pase
	' El acceso fue controlado antes de llegar aca
	passwd = Ucase(rstUtente("password"))
	'trim(passwd) = trim(passwd) and trim(rstUtente("ind_abil"))="S")

	If trim(rstUtente("ind_abil"))="S" then
		'inserimento verifica validit� password e eventuale modifica
		Session("password") = rstUtente("password")
		Session("IdUtente") = Clng(rstUtente("IdUtente"))
		' pagina de inicio
		sPagtoVisit = Session("Progetto") & "/home.asp"
		
		' control de dias de la clave	
		nGiorni= clng(rstUtente("giorni"))
		if isnull(rstUtente("dt_pwd")) then
			Status ="3"
			Mensaje = "Algo no definido"
			HTMLsalto = sPagtoVisit 
			rstUtente.Close
			Set rstUtente = Nothing	
			exit function
		end if
		
		if nGiorni > 180 then
			Set rstUtenteDeny = Server.CreateObject("ADODB.RECORDSET")
			SQLUtenteDeny = "UPDATE UTENTE SET IND_ABIL='N' WHERE IDUTENTE=" & Session("idutente")
'PL-SQL * T-SQL  
SQLUTENTEDENY = TransformPLSQLToTSQL (SQLUTENTEDENY) 
			rstUtenteDeny.open SQLUtenteDeny, CC, 3, 3

			Status ="4"
			Mensaje = "El usuario no se encuentra habilitado a acceder al sistema"
			HTMLsalto = sPagtoVisit 

			rstUtenteDeny.Close
			Set rstUtenteDeny = Nothing	
			rstUtente.Close
			Set rstUtente = Nothing	
			exit function
		end if	
		
		if nGiorni > 90 then
			Status ="5"
			Mensaje = "El usuario no se encuentra habilitado a acceder al sistema"
			HTMLsalto = sPagtoVisit 
			rstUtente.Close
			Set rstUtente = Nothing	
			exit function
		end if
	
		Session("IdUtente") = Clng(rstUtente("IdUtente"))
		Session("nome") = rstUtente("nome")
		Session("cognome") = rstUtente("cognome")
		Session("email") = rstUtente("email")
		Session("idgruppo") = Clng(rstUtente("idgruppo"))
		Session("password") = rstUtente("password")
		Session("tipopers") = rstUtente("tipo_pers")
		Session("creator") = Clng(rstUtente("creator"))
		Session("login") = rstUtente("login")
		Session("gruppo") = rstUtente("desgruppo")
		Session("rorga") = rstUtente("cod_rorga") 
		Session("webpath") = rstUtente("webpath")
		Session("iduorg") = Clng(rstUtente("id_uorg"))
		Session("ruofu") = rstUtente("cod_ruofu")
		Session("Prglog") = Session("Progetto")
	
		'utilizzer� poi la session Prglog, che identifica il progetto dove ho fatto la login,
		'nei controlli all'interno di chk_init.asp
		if Session("rorga") <> "RC" and Session("rorga") <> "RR" then
		'Solo in locale
			Session.TimeOut = 180
		else
			Session.Timeout = 180
		end if
	
		If  Ucase(Session("Progetto")) = "/SPORT2JOB" Then
			Session.Timeout = 180
		End If
		
		Set rstGruppoFun = Server.CreateObject("ADODB.RECORDSET")
			SQLGruppoFun = "SELECT a.namevar, a.valore, b.acronimo FROM gruppofun a, funzione b WHERE (a.idfunzione=b.idfunzione and a.idgruppo=" & Session("idgruppo")& ")"
'PL-SQL * T-SQL  
SQLGRUPPOFUN = TransformPLSQLToTSQL (SQLGRUPPOFUN) 
			rstGruppoFun.open SQLGruppoFun, CC, 1, 3
		
		If not rstGruppoFun.EOF Then
			Do While Not rstGruppoFun.EOF
				If rstGruppoFun("namevar") <> "" Then
					Session(rstGruppoFun("namevar")) = rstGruppoFun("valore")
					Session(rstGruppoFun("acronimo")) = addElement2ArrayFunction(rstGruppoFun("acronimo"),rstGruppoFun("namevar"),rstGruppoFun("valore"),false)
				End if
				rstGruppoFun.MoveNext
			Loop	
		End If				
		rstGruppoFun.Close
		Set rstGruppoFun = Nothing

		Set rstUteFun = Server.CreateObject("ADODB.RECORDSET")
			SQLUteFun = "SELECT a.namevar, a.valore, b.acronimo FROM utefun a, funzione b where (a.idfunzione=b.idfunzione and a.idutente=" & Session("IdUtente") & ")"
'PL-SQL * T-SQL  
SQLGRUPPOFUN = TransformPLSQLToTSQL (SQLGRUPPOFUN) 
			rstUteFun.open SQLGruppoFun, CC, 1, 3
					
		If not rstUteFun.EOF Then
			Do While Not rstUteFun.EOF 
				If rstUteFun("namevar") <> "" Then
					Session(rstUteFun("namevar")) = rstUteFun("valore")
					Session(rstUteFun("acronimo")) = addElement2ArrayFunction(rstUteFun("acronimo"),rstUteFun("namevar"),rstUteFun("valore"),true)					
				end if
				rstUteFun.MoveNext
			Loop
		End If
		rstUteFun.Close
		Set rstUteFun = Nothing
	
		Set rstLastAccess = Server.CreateObject("ADODB.RECORDSET")
			sSQLLastAccess="SELECT DT_ACCESSO FROM UTENTE WHERE IDUTENTE =" & SESSION ("Idutente")
'PL-SQL * T-SQL  
SSQLLASTACCESS = TransformPLSQLToTSQL (SSQLLASTACCESS) 
			rstLastAccess.open sSQLLastAccess, CC, 1, 3
			session ("LastAccess")= rstLastAccess("DT_ACCESSO")
		Set rstLastAccess = Nothing	
	
		CC.Begintrans
	
		sSQL ="UPDATE UTENTE  SET DT_ACCESSO=" & convdatetodb(Now()) & " WHERE idutente= " & session("idutente")
		Errore = EseguiNoC(sSQL,CC)
		If Errore="0" Then
			'---- CONSUELO 25/02/2004 direttive di G.Bartone---------
			'	  inserimento su LOG_UTE come funzionava su IN
			Set RRAut = Server.CreateObject("adodb.RECORDSET")
			sSQLAut = "SELECT AUT_LOG FROM GRUPPO WHERE IDGRUPPO = " & Session("idgruppo") & " AND AUT_LOG = 'S'"
		
'PL-SQL * T-SQL  
SSQLAUT = TransformPLSQLToTSQL (SSQLAUT) 
			RRAut.Open sSQLAut, CC
		
			'verifico se l'utente appartiene ad un gruppo monitorato su LOG_UTE
			if not RRAut.eof then
				Session("Log") = "S"
				if InserisciSessione() = true then
					CC.CommitTrans
				else
					CC.RollbackTrans
				end if
			else
				CC.CommitTrans
			end if
			RRAut.Close	 
			Set RRAut = nothing

		'---- FINE CONSUELO 25/02/2004 direttive di G.Bartone---------
		Else
		   CC.RollbackTrans   
		End If 	    
	Else
		If UCase(rstUtente("password")) <> UCase(passwd) Then
			Status ="6"
			Mensaje = "La contrase�a ingresada es incorrecta"
			HTMLsalto = "/util/logout.asp"
			rstUtente.Close
			Set rstUtente = Nothing	
			exit function
		Else
		'----> ind_abil=N ---------
			Status ="7"
			Mensaje = "El usuario no se encuentra habilitado a acceder al sistema"
			HTMLsalto = "/util/logout.asp"
			rstUtente.Close
			Set rstUtente = Nothing	
			exit function
		End If
	End If
	rstUtente.Close
	Set rstUtente = Nothing	

	if nGiorni > 79 then
		sPagtoVisit = Session("Progetto") & "/home.asp"
		nGGMancanti = 91 - nGiorni
		Session("DatePwd") = nGGMancanti

		Status ="8"
		Mensaje = "XXXX"
		HTMLsalto = sPagtoVisit
		exit function
	end if

	'=================================================
	' Termino Bien -> Usuario Aceptado 
	'=================================================
	sPagtoVisit = Session("Progetto") & "/home.asp"
	Session("homePage") = sPagtoVisit 
	
	Status = "0"	
	Mensaje = "Usuario Aceptado"
	HTMLsalto = sPagtoVisit
	
end function


'---- consuelo 25/02/2004 direttive di G.Bartone---------
'     inserimento su LOG_UTE come funzionava su IN
function InserisciSessione()
	
	session("id_webserver") = Request.ServerVariables("SERVER_NAME")
	on error resume next

	err.Clear
	InserisciSessione = false
	
	sSQL = "INSERT INTO LOG_UTE (ID_SESSIONE, ID_WEBSERVER, IDUTENTE, DT_INISESS, DT_FINSESS, IDGRUPPO) " & _
		   " VALUES(" & Session.SessionId & " ,'" & session("id_webserver") & "'," & Session("IdUtente") & _
		   ", SYSDATE, NULL, " & Session("idgruppo") & ")"
	
	Errore = EseguiNoC(sSQL,CC)
	
	if Errore = "0" then
		InserisciSessione = true
	end if  		

	on error goto 0

end function

%>
