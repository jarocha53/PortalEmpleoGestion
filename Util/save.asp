<%@ LANGUAGE = "VBSCRIPT"%>
<%response.buffer=true%>

<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include virtual="/strutt_testa2.asp"-->
<br>
<table width="500" align="center" border="0" cellspacing="2" cellpadding="1" class="sfondocomm">
	<tr> 
		<td valign="top" align="left" width="23">
			<img align="left" src="/images/gutente.gif" border="0" WIDTH="29" HEIGHT="32">
		</td>
		<td class="tbltext" align="middle" width="477"> 
			<b>File salvato correttamente.</b> <br> <br>
		</td>
	</tr>
</table>
<body>
<br>
<%
de_bug=0

'Filein = Request.Form("Page")

' ****** Modificato da P.Giovannini 24/10/2001
' ****** inserito una Replace per gestire l'invio 
' ****** di immagini interne alle news facendole 
' ****** puntare alla directory virtuale Images

If Request.Form("PathOrigGif") <> "" Then
	sPathOrigGif = Request.Form("PathOrigGif")
	sDivPathOrigGif = split(sPathOrigGif,"\")
	nProfPathGifOrig = Ubound(sDivPathOrigGif)
	sNomeGif = sDivPathOrigGif(nProfPathGifOrig)
	Select case Session("Tipo")
	case "B"
		sPathVirtGif = Session("Progetto") & "/images/bacheca/" & sNomeGif
	case "R"
		sPathVirtGif = Session("Progetto") & "/images/Redazionale/" & sNomeGif
	case "D"
		sPathVirtGif = Session("Progetto") & "/images/Doc/" & sNomeGif
	case else
		sPathVirtGif = Session("Progetto") & "/images/News/" & sNomeGif
	End select
	
	For I = 1 To Request.Form("Page").Count 
		sAppo = sAppo & Request.Form("Page")(I)
	Next
	
	Filein = Replace(sAppo,sPathOrigGif,sPathVirtGif)

Else

	For I = 1 To Request.Form("Page").Count 
		Filein = Filein & Request.Form("Page")(I)
	Next

'INIZIO AM 12/10
	Filein = Replace(Filein,"JPG","jpg")
	Filein = Replace(Filein,"GIF","gif")
'FINE AM 12/10
	
	
	nPosIMG = Instr(1,Filein,"<IMG")
	Do While nPosIMG 
		nPosbarra = InStr(nPosIMG+1,Filein,":\") 
		If nPosbarra Then
			nInitStr = nPosbarra - 1
			nPosExtGif = Instr(nPosIMG,Filein,".gif") 
			nPosExtJpg = Instr(nPosIMG,Filein,".jpg") 
			If nPosExtGif Then
				nFineStr = nPosExtGif
			End If
			If nPosExtJpg Then
				nFineStr = nPosExtJpg
			End If
			If nInitStr < nFineStr Then
				sPosGif = (nFineStr-nInitStr) + 4
				sPathOrigGif = Mid (Filein,nInitStr,sPosGif)
				sDivPathOrigGif = split(sPathOrigGif,"\")
				nProfPathGifOrig = Ubound(sDivPathOrigGif)
				sNomeGif = sDivPathOrigGif(nProfPathGifOrig)
				Select case Session("Tipo")
				case "B"
					sPathVirtGif = Session("Progetto") & "/images/bacheca/" & sNomeGif
				case "R"
					sPathVirtGif = Session("Progetto") & "/images/Redazionale/" & sNomeGif
				case "D"
					sPathVirtGif = Session("Progetto") & "/images/Doc/" & sNomeGif
				case else
					sPathVirtGif = Session("Progetto") & "/images/News/" & sNomeGif
				End select
				Filein = Replace(Filein,sPathOrigGif,sPathVirtGif)
			End if
			nPosIMG = nFineStr
		End if
		nPosIMG = Instr(nPosIMG+1,Filein,"<IMG") 
	Loop
End If

' *******************************************

Filename =  Unescape(Request.Form("File"))
sPathFileCreato = Request.Form("PathFileCreato")
aDirPathFileCreato = Split(sPathFileCreato,"\")

Dim fso, ts
Set fso = CreateObject("Scripting.FileSystemObject")

' ****************************************************
' Per la creazione di una info_.asp automaticamente.
' ****************************************************
if Request.Form("Build") <> "" then
	sParam = "&Build=Build"

	Set ts = fso.OpenTextFile(Filename)
	sStruTest = ts.ReadLine
	sStruCoda = ts.ReadLine
	ts.close

	FilenameASP = Replace(Filename,".htm",".asp")
	aFilename = Split(FilenameASP,"\")
	
	If UCase(aDirPathFileCreato(0)) = "TESTI" Then
		FilenameASP  = Server.MapPath("/") & "\" & sPathFileCreato & "\" & Right(aFilename(ubound(aFilename)),Len (aFilename(ubound(aFilename)))-1  )
	Else
		FilenameASP  = Server.MapPath("/") & Session("Progetto") &_
		"\Testi\info\infoprimolivello\" & Right(aFilename(ubound(aFilename)),Len (aFilename(ubound(aFilename)))-1  )
	End If
	
	sParam = "&Build=" & aFilename(ubound(aFilename))

	Set ts = fso.CreateTextFile(FilenameASP)
	ts.writeline (sStruTest)
	ts.WriteBlankLines	2
	
	ts.WriteLine (" <br><br><center>")
	ts.WriteLine ("<TABLE border='0' width='100%' cellspacing='0' cellpadding='0' height='81'>")
	ts.WriteLine ("<tr><td width='100%'>") 
	ts.WriteBlankLines	2
	
	ts.writeline (chr(60) & chr(37))
	
	ts.WriteBlankLines	2
	
	If UCase(aDirPathFileCreato(0)) = "TESTI" Then
		ts.writeline (" " &_
		" PathFileEdit = ""/" & Replace(sPathFileCreato,"\","/") & "/" &_
		  Replace(mid(aFilename(ubound(aFilename)),2),".asp",".htm") & """") 
	Else
		ts.writeline (" " &_
		" PathFileEdit = Session(""Progetto"") & ""/Testi/SistDoc/" & Replace(sPathFileCreato,"\","/") & "/" &_
		  Replace(mid(aFilename(ubound(aFilename)),2),".asp",".htm") & """") 
	End If
	
	
	' *********************
	' Gestione errore
	ts.WriteBlankLines	2
	ts.writeline ("on error resume next ")
'PL-SQL * T-SQL  
PATHFILEEDIT  = TransformPLSQLToTSQL ("PATHFILEEDIT") 
	ts.writeline (" Server.Execute(PathFileEdit) ")
	' *********************

	ts.WriteBlankLines	2
	ts.writeline (chr(37) & chr(62)) 

	ts.writeline ("<!-- #include virtual=""/include/MsgErrInc.asp"" -->")
	ts.WriteBlankLines	2
	ts.writeline (" </td>")
	ts.writeline ("</tr>")
	ts.writeline ("</TABLE></center>")

	ts.WriteBlankLines	2
	ts.writeline (sStruCoda)
	ts.close

end if

Set ts = fso.CreateTextFile(Filename,True)
ts.write (Filein)
ts.close

Set ts = nothing
Set fso = nothing

' ************************************************
' ****** inserito un If innestato per gestire 
' ****** la modifica e la creazione delle news
' ************************************************
If Session("Wiz") = "" Then
	
	If Session("Tipo")="N" or Session("Tipo")="D" or Session("Tipo")="R" Then

		If (Request.Form("IdNews") = "") And Session("Tipo")="R" Then
			Idcat = Request.Form("IdNews")
			
			Response.Write "<meta http-equiv='Refresh' content='2; URL=/Pgm/Documentale/Doc_InsDoc.asp?Id=" & Request.Form("Categoria") & sParam & "'>"
		Else
			If (Request.Form("IdNews") <> "") Then
				Idcat = Request.Form("IdNews")
				Response.Write "<meta http-equiv='Refresh' content='2; URL=/Pgm/Documentale/Doc_LeggiNews.asp?Nr=" & Idcat & sParam & "'>"
			Else
				Idcat = Request.Form("Categoria")
				Response.Write "<meta http-equiv='Refresh' content='2; URL=/Pgm/Documentale/Doc_InsFotoNews.asp?Id=" & Idcat & sParam & "'>"
			End If
		End If

' *******************************************
	Else 
%>
	<div align="center"><center>
	<input type="button" name="op" align="center" value="Chiudigg" onclick="javascript:window.close()" type="image" src="<%=Session("progetto")%>/images/chiudi.gif" alt="Close Windows" name="Close">
	</center></div>
<%	
	End If 
Else
' ****************************************************
' Per la creazione di una pagina che descrive il gruppo.
' ****************************************************
	Session("Wiz") = ""
	If Session("InitGruppi") <> "" Then
		Session("InitGruppi") = ""
		Response.Redirect "/Pgm/Documentale/Doc_InitGruppi.asp"
	Else
		Response.Redirect "/Pgm/Wizard/creagruppo/pag2.asp"
	End If

End If
%>

<!--#include virtual="/strutt_coda2.asp"-->
