<!--#include Virtual = "/strutt_testa2.asp"-->
<!--#include Virtual ="/Include/openconn.asp"-->

<script type="text/javascript">

	$(function(){


		$( "#goBack" ).button({
	      icons: {
	       	primary: "ui-icon-arrowreturnthick-1-w"
	      }
		}).click(function(){
			window.history.back()
		});


		$( "#saveChangePassword" ).button({
	      icons: {
	        primary: "ui-icon-locked"
	      }
		}).click(function(){
			var continuar = true;
			
			var password = $("#password");
			var rpassword = $("#rpassword");
			

			if(password.val() == ""){
				continuar = false;
				alert("El campo 'Contraseña' es obligatorio!");
				password.focus();
			}else if(rpassword.val() == ""){
				continuar = false;
				alert("El campo 'Confirmar Contraseña' es obligatorio!");
				rpassword.focus();
			}else if(password.val() != rpassword.val()){
				continuar = false;
				alert("Los campos 'Contraseña y Confirmar Contraseña' no coinciden.");
				password.focus();
			}else if(seguridad_clave(password.val())<60){
				continuar = false;
				rpassword.val("");
				password.focus();
			}
			
			if(continuar){
				$("#formCambiarPasswordUsuario").submit();
			}

		});
	});


	/**
* funcciones de verificacion para password
*/
    var numeros = "0123456789";
    var letras = "abcdefghyjklmnñopqrstuvwxyz";
    var letras_mayusculas = "ABCDEFGHYJKLMNÑOPQRSTUVWXYZ";
    tiene_numeros = function (texto) {
        for (i = 0; i < texto.length; i++)
            if (numeros.indexOf(texto.charAt(i), 0) != -1)
                return true;
        return false;
    }
    tiene_letras = function (texto) {
        texto = texto.toLowerCase();
        for (i = 0; i < texto.length; i++)
            if (letras.indexOf(texto.charAt(i), 0) != -1)
                return true;
        return false;
    }

    tiene_minusculas = function (texto) {
        for (i = 0; i < texto.length; i++)
            if (letras.indexOf(texto.charAt(i), 0) != -1)
                return true;
        return false;
    }


    tiene_mayusculas = function (texto) {
        for (i = 0; i < texto.length; i++) {
            if (letras_mayusculas.indexOf(texto.charAt(i), 0) != -1) {
                return 1;
            }
        }
        return 0;
    }

    /**
        Tiene letras y números: +30%
        Tiene mayúsculas y minúsculas: +30%
        Tiene entre 4 y 5 caracteres: +10%
        Tiene entre 6 y 8 caracteres: +30%
        Tiene más de 8 caracteres: +40%
    * */
    seguridad_clave = function (clave) {
    	var mensaje = "";
        var seguridad = 0;
        if (clave.length != 0) {
            if (tiene_numeros(clave) && tiene_letras(clave)) {
                seguridad += 30;
            } else {
                mensaje += " [numeros y letras] ";
            }
            if (tiene_minusculas(clave) && tiene_mayusculas(clave)) {
                seguridad += 30;
            } else
                mensaje += " [Mayusculas y minusculas] ";
            if (clave.length >= 4 && clave.length <= 5) {
                seguridad += 10;
            } else {
                if (clave.length >= 6 && clave.length <= 8) {
                    seguridad += 30;
                } else {
                    if (clave.length > 8) {
                        seguridad += 40;
                    }
                    else if (clave.length < 6) {
                        mensaje += " [Más de 6 caracteres] ";
                    }
                }
            }
        }
        if(mensaje != "" && seguridad < 60)
        	alert("La contraseña, debe tener: "+mensaje);
        return seguridad
    }
</script>

<%

	Dim id_utente
	
	On Error Resume Next
		'id_utente = CInt(Server.HTMLEncode(request("id_utente")))
    	id_utente = Server.HTMLEncode(request("id_utente"))
	If err <> 0 Then
		msg = "El usuario seleccionado no pertenece a su centro."
		response.Redirect "users_list.asp?msg="& msg
	End If

	Dim authUser_Sede
	authUser_Sede = Session("creator")

	id_grupo_centros = 25


	sqlEditUser_Sede = "SELECT COUNT(1) AS EXISTE FROM UTENTE WHERE IDUTENTE = '" & id_utente & "' AND CREATOR = '" & authUser_Sede & "' AND IDGRUPPO = '" & id_grupo_centros & "'"
	set rsEditUser_Sede = CC.Execute(sqlEditUser_Sede)

	existe = 0
	if not rsEditUser_Sede.EOF then	
		existe 	= rsEditUser_Sede(0) 
	end if

	if existe = 0 then
		msg = "El usuario seleccionado no pertenece a su centro."
		response.Redirect "users_list.asp?msg="& msg
	end if



	Dim user_name

	sqlUserName = "SELECT LOGIN FROM UTENTE WHERE IDUTENTE = " & id_utente
	set rsUserName = CC.Execute(sqlUserName)
	if not rsUserName.EOF then	
		user_name = rsUserName(0)
	end if

	if Server.HTMLEncode(request("change")) = "1" then 
		Dim msg

		Dim password
		password = Server.HTMLEncode(request("password"))
		Dim rpassword
		rpassword = Server.HTMLEncode(request("rpassword"))
		
		if password <> rpassword then
			msg = "Los campos no coinciden."
			response.Redirect "users_list.asp?msg="& msg
		else
			sqlCambiarPasswordUsuarioCentro = "UPDATE UTENTE SET PASSWORD = '" & password & "' WHERE UTENTE.IDUTENTE = " & id_utente

			set rsCambiarPasswordUsuarioCentro = CC.Execute(sqlCambiarPasswordUsuarioCentro)

			msg = "El registro ha sido modificado con exito."
			response.Redirect "users_list.asp?msg="& msg
		end if

	end if 

%>


<table border="0" width="530" cellspacing="0" cellpadding="0" height="81" >
   <tr>
     <td width="530" background="<%=Session("Progetto")%>/images/titoli/strumenti2g.gif" height="81" valign="bottom" align="right">
       <table border="0" style="width: 100%;padding-right: 20px;" height="30" cellspacing="0" cellpadding="0">
         <tr>
           
          <td width="100%" valign="top" align="right"><b class="tbltext1a">REGISTRAR USUARIO - CENTRO DE ATENCI&Oacute;N</span></b></td>
         </tr>
       </table>
     </td>
   </tr>
</table>
<br>

<table cellpadding="0" cellspacing="0" width="500" border="0">
	<tr height="18">
	
    <td class="sfondomenu" height="18" width="67%"> <span class="tbltext0"> <b>Registrar Usuario - Centro de Atenci&oacute;n</b></span> </td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
		
    <td valign="middle" align="right" class="tbltext1" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">(*) 
      campo obligatorio</td>
		</td>
		<td valign="middle" align="right" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1"></td>
	</tr>
	<tr width="371" class="sfondocomm">
		
    <td colspan="3">Ingresar los datos pertenecientes al nuevo usuario a registrar. Recuerde que este usuario podr&aacute; acceder a todas las opciones habilitadas para los centros de atenci&oacute;n<br>
	<!--br>
	NOTA: Ingresar al menos uno de los siguientes datos: Nº de Planilla, RUC-->
    <!-- <a href="Javascript:Show_Help('/Pgm/help/Imprese/IMP_InsImpresa')"><img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a> -->     </td>
	</tr>   
	<tr height="2">
		<td colspan="3" class="sfondocomm" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
	</tr>
</table>

<br>
<br>

<form id="formCambiarPasswordUsuario" name="searchForm" METHOD="POST" action="/Util/change_password_user_centro.asp">
	
	<input type="hidden" Class="textblack" id="change" name="change" value="1" maxlength="50" />
	<input type="hidden" Class="textblack" id="id_utente" name="id_utente" value="<%=id_utente%>" maxlength="50" />

	<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
		<tr>
			<td class="sfondocomm" width="30%" align="left" colspan="2" style="font-weight:bold;font-size:15px;padding:20px 0px 20px 20px">
				<div style="border-bottom: 1px solid #160EA6;width: 95%;">Informaci&oacute;n de  Autenticaci&oacute;n:</div>
			</td>
		</tr>
		<tr>
			<td class="sfondocomm" width="30%" align="right">Nombre de Usuario</td>
			<td class="sfondocomm" width="40%" align="left">
				<span id="user_name" style="font-weight:bold;"><%=user_name%></span>
			</td>
		</tr>
		<tr>
			<td class="sfondocomm" width="20%" align="right">Contrase&ntilde;a *</td>
			<td class="sfondocomm" width="40%" align="left">
				<input type="password" id="password" name="password"  value="<%=password%>"  style="width:70%;" />
			</td>
		</tr>
		<tr>
			<td class="sfondocomm" width="20%" align="right">Confirmar Contrase&ntilde;a *</td>
			<td class="sfondocomm" width="40%" align="left">
				<input type="password" id="rpassword" name="rpassword"  value="<%=rpassword%>"  style="width:70%;" />
			</td>
		</tr>
		<tr>
			<td class="sfondocomm" colspan="2" style="text-align:center;">
				<!--input type="submit" value="Registrar" /-->
				<button id="goBack" type="button">VOLVER</button>
				<button id="saveChangePassword" type="button">CAMBIAR</button>
			</td>
		</tr>
		<tr height="23" align="center"> 
		<td class="sfondocomm" colspan="2"> <b class="tbltext1" id="msg"> <%=msg%> </b></td>
	</tr>
	</table>
</form>

<br/><br/><br/>

<% Sub showMsg(msg) %>
	<script type="text/javascript">
		//$(function(){
			//$("#msg").html("<%=msg%>");
			alert("<%=msg%>");
		//});
	</script>
<% end sub %>






<!--#include virtual = "/include/closeconn.asp"-->
<!--#include Virtual="/strutt_coda2.asp"-->


