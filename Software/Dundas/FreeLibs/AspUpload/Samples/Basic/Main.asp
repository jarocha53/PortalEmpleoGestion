<%@ Language=VBScript %>
<% Response.Buffer = true
'******************************************************************************
'Copyright Dundas Software Ltd. 2000. All Rights Reserved.
'
'PURPOSE:			The user can upload up to 3 files to the server. If the operation
'					  is successful the user will receive a success message, if the
'					  operation fails then a failure message will be displayed.	    
'
'POST DESTINATION:  Process.asp           
'
'COMMENTS:			 Note that a maximum size of one (1) MBytes per file is allowed.			
'
'
'Dundas Software Contact Information:
'	Email:	sales@dundas.com
'   Phone:	(800) 463-1492
'			(416) 467-5100
'	Fax:	(416) 422-4801
'******************************************************************************
%>
<html>
<head>
<meta NAME="GENERATOR" Content="Microsoft Visual Studio 6.0">
</head>
<BODY color="black" bgcolor="white">
<table><tr><td><IMG border=0 height=36 src="../images/clouds.jpg" width=550></td></tr></table>
<span style="LEFT: 574px; POSITION: absolute; TOP: 20px; Z-INDEX: -1">
<a href="http://www.dundas.com">
<img align="right" border="0" src="../images/dundaslogo.gif" hspace="5" WIDTH="160" HEIGHT="18">
</a>
</span>
<br> <IMG border=0 height=50 src="../images/headline_upload.gif" width=390><br>
<p>
<IMG align=absMiddle border=0 height=14 src="../images/scenario.gif" width=82>
</p>
<font size="3"><i>This simple demo allows the user to upload up to three (3) files to the server, with a maximum
file size of one (1) MBytes per file.
<br><br>How to access form data is demonstrated as is the saving of uploaded files to memory.  Please note that a 
success or failure message will be displayed along with the user's name after the upload is processed.  
</i>
</font></P>
<center>
<form name="UploadForm" action="Process.asp" enctype="multipart/form-data" method="post" style="BORDER-BOTTOM: thin; BORDER-RIGHT-STYLE: none">
<%
'if an upload operation just occurred output success/failure message to user
if Request.QueryString("Message") <> "" then
	Response.Write "<font color=blue><b>" & Request.QueryString("Message") & "</font></b><br><br>"
end if
%>
<p style="BACKGROUND-COLOR: #6495ed; BORDER-BOTTOM: thin solid; BORDER-LEFT: thin solid; BORDER-RIGHT: thin solid; BORDER-TOP: thin solid; WIDTH: 400px">
<font color="#ffffff">&nbsp;&nbsp;UPLOAD INFORMATION</font>
<table width="400" border="0" cellPadding="3" cellSpacing="3" style="BACKGROUND-COLOR: #ffffe0" align="center">
  <tr>
	<td>Please enter your name: </td>
    <td><input type="textbox" name="txtName"></td>
  </tr>
  <tr>
    <td>File to upload #1: </td>
    <td><input NAME="File1" TYPE="file"></td></tr>
  <TR>
    <td>File to upload #2: </td>
    <td><input NAME="File2" TYPE="file"></td></TR>
  <TR>
    <td>File to upload #3: </td>
    <td><input NAME="File3" TYPE="file"></td></TR>
  </table></p>
<p>
<input type="submit" value="Upload File(s)" id="submit1" name="submit1">
<br></form></P></center>
</BODY>
</html>