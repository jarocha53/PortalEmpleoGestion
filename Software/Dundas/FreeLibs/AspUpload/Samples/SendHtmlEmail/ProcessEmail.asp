<%@ Language=VBScript %>
<% Response.Buffer = True 'enable html buffering so we can use a redirect anywhere in the page %>
<%
'*********************************** IMPORTANT ***********************************
'  A folder with a GUID for a name will be created one directory level up from this
'	ASP page's location, and is used to temporarily store uploaded files.  
'	If you want files uploaded by users to be deleted then give the default web account
'   Full Control permissions for this folder only.  Then remove the
'   commenting from the "objUpload.Files(j).Delete" line below.
'*********************************** IMPORTANT ***********************************
'*********************************************************************************
'Copyright Dundas Software Ltd. 2000. All Rights Reserved.
'
'PURPOSE:           Processes the information POSTED by SendHtmlEmail.asp, and then 
'					 attempts to send the email to the specified address(es) along
'					 with the specified objects (if any) embedded into the email's body. 
'
'CONTROLS USED:		Dundas Upload Control 2.0 and Dundas Mailer Control 1.0.
'
'COMMENTS:          Maximum amount of posted data is 1 Meg.
'					Form values are sent back to the main page using a Querystring.
'					A maximum of 1 Megabytes of form data is allowed to be uploaded
'					 to the server (the sum of the posted form data and all uploaded
'					 files). 
'
'Dundas Software Contact Information:
'	Email:	sales@dundas.com
'   Phone:	(800) 463-1492
'			(416) 467-5100
'	Fax:	(416) 422-4801
'*********************************************************************************
sub DeleteUploads
'removes any files user uploaded to server in temp directory (see section at top of page)
dim j  'counter variable
for j = 0 to objUpload.Files.Count - 1
	objUpload.Files(j).Delete
next
end sub
%>
<HTML>
<HEAD>
<META NAME="GENERATOR" Content="Microsoft Visual Studio 6.0">
</HEAD>
<BODY>
<%
dim objUpload    'instance of the Upload control
dim strTemp      'temp. string variable, used to replace carriage return/linefeed with <br> in HtmlBody property
dim objEmail     'stores a Mailer control object
dim strPath		 'set to "c:\4AA80F25-21E4-11D4-9985-0050BAD44BCD", it is the path of the storage folder
dim i            'counter variable  

'functions will throw an exception if the operation is unsuccessful, so on error resume next is used
on error resume next  

set objEmail = Server.CreateObject("Dundas.Mailer")     'Mailer object
'if Mailer control is not installed and registered then redirect user
'  to the Error.asp page.
if Err.Number <> 0 then
	Response.Redirect "../Error.asp?Error=" & server.URLEncode("You must first install and register the <b>free</b> Dundas Mailer Control 1.0.</font><br><br>Click <a href=http://www.dundas.com/index.asp?downloads/register.asp?ProdID=2>here</a> to download the Mailer control.")	
end if

set objUpload = Server.CreateObject("Dundas.Upload.2")    'Upload object

'retrieve path to dir. level one up from this page, create unique folder for
'  uploaded files temporarily saved to disk
strPath = Server.MapPath("..")
strPath = strPath & "\" & "00566F20-168D-445E-974E-A5BC0881F6A4"

objUpload.DirectoryCreate strPath  'create temp directory if it doesn't exist

'before saving data we will make sure that the sum of the form data and all uploaded
'  files does not exceed 2 Meg.
objUpload.MaxUploadSize = 2000000

'save the uploaded files to the temp directory.  This populates the Upload control's collections!    
objUpload.Save strPath 
'error trap for success/failure
if Err.Number <> 0 then
	Response.Redirect "../Error.asp?Error=" & server.URLEncode(Err.Description)
end if

'add specified To field to collection, if user did not enter a value redirect to the error page
if objUpload.Form("txtTo") <> "" then	
	objEmail.TOs.Add objUpload.Form("txtTo")
else
	'user has to enter TO field, if he/she hasn't then redirect to the error page
	Response.Redirect "../Error.asp?Error=" & server.URLEncode("You must enter a value for the TO field.")
end if

'set FromAddress property
if objUpload.Form("txtFrom") <> "" then	
	objEmail.FromAddress = objUpload.Form("txtFrom")
end if

'set the Subject property
objEmail.Subject = objUpload.Form("txtSubject")

'add specified Cc field to collection, first testing to see if 
'  user entered a value
if not IsEmpty(objUpload.Form.Item("txtCc")) then
	objEmail.CCs.Add objUpload.Form("txtCc")
end if

'add specified Bcc field to collection
if not IsEmpty(objUpload.Form.Item("txtBcc")) then
'If objUpload.Form("txtBcc") <> "" Then		
	objEmail.BCCs.Add objUpload.Form("txtBcc")
end if

'check to see if SMTP relay server has been specified, if so add to collection
if not IsEmpty(objUpload.Form.Item("txtSMTP")) then
'If objUpload.Form("txtSMTP") <> "" Then
	objEmail.SMTPRelayServers.Add objUpload.Form("txtSMTP")
end if

objEmail.HTMLBody = "<Html><Head></Head>"  'initialize html body of message
if objUpload.Files.Count > 0 then  'there is a background picture or sound specified so set the HtmlBody property 	
	'initialize the Body tag
	objEmail.HTMLBody =  objEmail.HTMLBody & "<body "
	'loop through uploaded files to be embedded, adding the appropriate tags as needed
	'	NOTE: we use the Name property of the File object to let us know what file input box the file came from
	for i = 0 to objUpload.Files.Count - 1
		if objUpload.Files(i).TagName = "txtBGPicture" then
			'set background picture and add to the HtmlEmbeddedObjs collection, first making sure that the user uploaded a valid picture file
			if instr(1,objUpload.Files(i).ContentType,"image") then	
				objEmail.HTMLEmbeddedObjs.Add objUpload.Files(i).Path,cstr(i + 1),objUpload.Files(i).OriginalPath
				objEmail.HTMLBody = objEmail.HTMLBody & "background=cid:" & cstr(i + 1) & ">"		
			else
				objEmail.HTMLBody = objEmail.HTMLBody & ">" 'close body tag if user uploaded an invalid picture
			end if
		elseif objUpload.Files(i).TagName = "txtBGSound" then 
			'check to see if user uploaded a background image, if not then we need to close the body tag
			if objUpload.Files(0).TagName = "txtBGSound" then 
				objEmail.HTMLBody = objEmail.HTMLBody & ">"
			end if	
			if instr(1,objUpload.Files(i).ContentType,"audio") then	
				'set background sound and add to the HtmlEmbeddedObjs collection, making sure user has uploaded a valid audio file
				objEmail.HTMLEmbeddedObjs.Add objUpload.Files(i).Path, Cstr(i + 1),objUpload.Files(i).OriginalPath
				objEmail.HTMLBody = objEmail.HTMLBody & "<BGSound src=cid:" & CStr(i + 1) & "></BGSOUND>"
			end if
		end if
	next 	
else
	'no embedded objects specified, so just set the body tag
	objEmail.HTMLBody = objEmail.HTMLBody & "<body>"
end if	
'now set message body input by user into the textarea element, replacing Cr/Lf with <br>,
'	and also replacing html characters with escape characters
'	NOTE: ALTERNATIVELY YOU CAN LEAVE ANY HTML CHARACTERS AS IS, SO THAT THE BODY OF THE EMAIL
'		CAN BE FORMATTED WITH HTML TAGS.  HOWEVER, CARE MUST THEN BE TAKEN SO THAT DUPLICATE TAGS DO NOT OCCUR
'		IF YOU ARE ALSO ADDING TAGS TO THE HTMLBODY PROPERTY PROGRAMMATICALLY 
strTemp = server.HTMLEncode(objUpload.Form("txtBody")) 
strTemp = Replace(strTemp,vbCrLf,"<br>")
objEmail.HTMLBody =  objEmail.HTMLBody & strTemp

'finish html body by adding closing html tags
objEmail.HTMLBody = objEmail.HTMLBody & "</body></html>"

'send the email
objEmail.SendMail

'test for success/failure
if Err.Number <> 0 then
	'an error occurred so redirect user to the error page
	dim ErrString
	ErrString = Err.Description 
	call DeleteUploads  'deletes any files uploaded by user
	set objEmail = nothing  'release resources
	set objUpload = nothing
	Response.Redirect "../Error.asp?Error=" & server.URLEncode(ErrString)
else
	'successful, so redirect back to main page, and set the QueryString variables to store form values to be sent back to main page
	dim item2(5)
	dim k
	dim Temp     
	
	'now retrieve previous form element values to pass back as a querystring
	item2(0) = objUpload.Form("txtSMTP")
	item2(1) = objUpload.Form("txtFrom")
	item2(2) = objUpload.Form("txtTo")
	item2(3) = objUpload.Form("txtCc")		
	item2(4) = objUpload.Form("txtBcc")
	item2(5) = objUpload.Form("txtSubject")
	
	call DeleteUploads  'deletes any files uploaded by user (see text at top of page!)
	
	set objEmail = nothing  'release resources
	set objUpload = nothing
	
	'redirect user back to main page with querystring used for previous form element values
	Response.Redirect "SendHtmlEmail.asp?Success=TRUE&Value1=" & server.URLEncode(item2(0)) & "&Value2=" & server.URLEncode(item2(1)) & "&Value3=" & server.URLEncode(item2(2)) & "&Value4=" & server.URLEncode(item2(3)) & "&Value5=" & server.URLEncode(item2(4)) & "&Value6=" & server.URLEncode(item2(5))
end if
%>
</BODY>
</HTML>
