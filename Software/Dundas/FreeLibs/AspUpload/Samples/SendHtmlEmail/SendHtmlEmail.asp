<%@ Language=VBScript %>
<%
'*********************************************************************************
'Copyright Dundas Software Ltd. 2000. All Rights Reserved.
'
'PURPOSE:           The user can send an html-based email to the specified
'					 recipients, and optionally specify a background audio file
'					 as well as a background picture for the email.  The uploading
'					 is handled by the Upload control, while the mailing operation
'					 is handled by the Dundas Mailer Control 1.0.  
'
'POST DESTINATION:  ProcessEmail.asp.           
'
'INPUT:				SMTP server (optional), TO recipients, CC recipients, BCC              
'					 recipients, From address, Subject of the message, the message
'					 body, optional background audio file and optional background
'					 image for the html-based email.
'
'COMMENTS:          IMPORTANT: This demo requires the Dundas Mailer Control.  
'					Maximum amount of posted data is 2 Meg.
'					A success message is displayed if the email is successfully
'					 sent.  
'					User will be redirected to the Error.asp page if the Dundas Mailer
'					  control is not installed and registered.					
'
'Dundas Software Contact Information:
'	Email:	sales@dundas.com
'   Phone:	(800) 463-1492
'			(416) 467-5100
'	Fax:	(416) 422-4801
'*********************************************************************************
%>
<html>
<head>
<meta NAME="GENERATOR" Content="Microsoft FrontPage 4.0">
</head>
<body color="black" bgcolor="white">
<table><tr><td><IMG border=0 height=36 src="../images/clouds.jpg" width=550></td></tr></table>
<a href="http://www.dundas.com">
<img align="right" border="0" src="../images/dundaslogo.gif" hspace="5" WIDTH="160" HEIGHT="18"></a>
<IMG border=0 height=50 src="../images/headline_upload.gif" width=390>
<p>
<IMG align=absMiddle border=0 height=14 src="../images/scenario.gif" width=82>
</p>
<p><font size="3"><i>This demo sends an html-based email to a specified destination address.  You can  
select a background image as well as a background sound for the email.</font></p>
<p>The Upload control is used when embedding objects into 
the email. &nbsp;Please note that the maximum amount of data to be uploaded has been set to two (2) MBytes.</I></p> 
<form action="ProcessEmail.asp" name="frmSendMail" method="post" ENCTYPE="multipart/form-data" style="BORDER-BOTTOM: thin; BORDER-RIGHT-STYLE: none">
<%
dim temp(5)  'used to populate form elements with values returned from ProcessEmail.asp
'check to see if an email was sent previously, if so output success/failure message
if Request.QueryString("Success") = "TRUE" then
	Response.Write "<center><font color=blue><B> The previous email was successfully sent to either the destination address or the SMTP relay server (if specified).</font></b></center>"
	'now grab the querystring parameter which stores the form's element values
	dim l
	for l = 1 to 6
		temp(l - 1) = Request.QueryString("Value" & l)
	next
else
'user loaded page for first time, so set temp. array to zero-length string values
	for l = 0 to 5
		temp(l) = ""
	next
end if	
%>
<center>
<p style="BACKGROUND-COLOR: #6495ed; width: 600; BORDER-BOTTOM: thin solid; BORDER-LEFT: thin solid; BORDER-RIGHT: thin solid; BORDER-TOP: thin solid"><font color="#ffffff">
&nbsp;&nbsp;SENDING AN HTML EMAIL WITH EMBEDDED OBJECTS<br></font>
<table width="600" border="0" style="BACKGROUND-COLOR: #ffffe0;" background="">  
  <TR>
  <td width="600" colspan="2" align="middle" nowrap border="1">
      <p align="center"><font face="Arial"><b><BR>
      SMTP Relay Server:</b></font>  <input name="txtSMTP" size="18" tabindex="1" value="<%=temp(0)%>"><br>
      <i>(<font size="2">Optional.&nbsp; Using a relay server
      will improve the performance of the operation</font>.)<br></i></p>
	</td></TR>
<tr>
	<td height="27" align="right" width="170" nowrap>
      <p align="right"><b>From:</b></p>
	</td>
    <td height="27" align="left" nowrap> 
      <p align="left"> <input name="txtFrom" size="50" tabindex="2" value="<%=temp(1)%>">&nbsp;
      <font color="red">*</font></p>
    </td>
</tr>
<center>
<tr>
	<td height="27" align="right" width="170" nowrap>
		<b>To:</b>
	</td>
</center>
	<td height="27" align="left" nowrap>
      <p align="left"><input name="txtTo" size="50" tabindex="3" value="<%=temp(2)%>">&nbsp;
      <font color="red">*</font>
      </p>
	</td></TR>
    <center>
<tr>
	<td height="27" align="right" width="170" nowrap>
		<b>Cc:</b>
	</td>
    <td height="27" align="left" nowrap>
		<input name="txtCc" size="50" tabindex="4" value="<%=temp(3)%>">
	</td>
</tr>
<tr>
	<td height="27" align="right" width="170" nowrap>
		<b>Bcc:</b>
	</td>
    <td height="27" align="left" nowrap>
		<input name="txtBcc" size="50" tabindex="5" value="<%=temp(4)%>">
	</td>
</tr>
<tr>
	<td height="25" align="right" width="170" nowrap>
		<b>Subject:</b>
	</td>
    <td height="25" align="left" nowrap>
		<input name="txtSubject" size="50" tabindex="6" value="<%=temp(5)%>">
	</td>
</tr>
</center>
<tr>
    <td align="right" width="170" nowrap>
      <b><font size="3">Background Picture:</font></b>
    </td>
    <td align="left" nowrap>
      <input type="file" name="txtBGPicture" size="50" tabindex="7">
    </td>
</tr>
<tr>
	<td align="right" width="170" nowrap>
      <b><font size="3">Background Sound:</font></b>
    </td>
    <td align="left" nowrap>
		<input type="file" name="txtBGSound" size="50" tabindex="8">
	</td>
</tr>
  <center>
<tr>
	<td width="600" colspan="2" align="middle" nowrap>
      <p align="center"><TEXTAREA name=txtBody rows=3 style="HEIGHT: 150px; WIDTH: 500px" tabIndex=9></TEXTAREA><BR></p>
    </td>
  </tr>
</table></center></p></center>
<p align="center"><input type="submit" value="Submit" name="cmdSubmit">
<input type="reset" value="Reset" name="cmdReset"></p></FORM>
</body>
</html>
