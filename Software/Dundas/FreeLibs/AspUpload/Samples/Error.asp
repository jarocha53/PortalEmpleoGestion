<%@ Language=VBScript %>
<html>
<head>
<meta NAME="GENERATOR" Content="Microsoft Visual Studio 6.0">
<title>Dundas Mailer Control Demo: Error Page.</title>
</head>
<body color="black" bgcolor="white">

<table><tr><td><img border="0" height="36" src="images/clouds.jpg" width="550"></td></tr></table>
<span style="LEFT: 574px; POSITION: absolute; TOP: 20px; Z-INDEX: -1">
<a href="http://www.dundas.com">
<img align="right" border="0" src="images/dundaslogo.gif" hspace="5" WIDTH="160" HEIGHT="18"></a>
</span> <img border="0" height="50" src="images/headline_upload.gif" width="390"> 
<br>
<p>The following error occurred : <font color="red"><%=Request.QueryString("Error")%></font></p>

<% if InStr(1,Request.QueryString("Error"),"You must first install",1) = 0 then %>
	<p>Please click Back on your browser and try the operation again.
<% end if %>

</body>
</html>
