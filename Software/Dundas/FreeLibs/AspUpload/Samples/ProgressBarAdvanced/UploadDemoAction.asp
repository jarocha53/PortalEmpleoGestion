<%@ENABLESESSIONSTATE=FALSE%>
<!--#include file="..\DSUpload.inc"-->
<%
'*********************************************************************************
'Copyright Dundas Software Ltd. 2000. All Rights Reserved.
'
'PURPOSE:           Processes the information POSTED by main.asp, and
'					 display the content of the Files & Form collection of the
'                    Dundas Upload control.
'
'CONTROLS USED:		Dundas Upload control.
'
'Dundas Software Contact Information:
'	Email:	sales@dundas.com
'   Phone:	(800) 463-1492
'			(416) 467-5100
'	Fax:	(416) 422-4801
'*********************************************************************************
%>
<HTML>
<HEAD>
<META NAME="GENERATOR" Content="Microsoft Visual Studio 6.0">
</HEAD>
<BODY color="black" bgcolor="white">
<table><tr><td><IMG border=0 height=36 src="../images/clouds.jpg" width=550></td></tr></table>
<span style="LEFT: 574px; POSITION: absolute; TOP: 20px; Z-INDEX: -1">
<a href="http://www.dundas.com">
<img align="right" border="0" src="../images/dundaslogo.gif" hspace="5" WIDTH="160" HEIGHT="18">
</a>
</span> <IMG border=0 height=50 src="../images/headline_upload.gif" width=390> 
<hr align="center" color="#0000ff" size="1">
<i>
<FONT size=3><br>Observe the upload operation summary below.</I> </FONT>
<%
on error resume next

'check if there is an error, if so then inform user of error. 
private sub CheckErr
	if Err <> 0 then
		' Display error message
		Response.Write "<P style='BACKGROUND-COLOR: #a52a2a'><FONT color=#ffffff>"
		Response.Write "&nbsp;&nbsp;" & "ERROR:  " & Err.description & "</FONT></P>"
		' Clear error
		Err.Clear
	end if
end sub

'create upload component instance
set objUpload = Server.CreateObject("Dundas.Upload")

objUpload.ProgressID = Request.QueryString("ProgressID")
'if the state server was on a different machine then this is where you would need to 
'  set the IP address of the machine where the state server is located
'objUpload.StateServer = "127.0.0.1"

'initialize the root & temp folders names
RootFolderName = Server.MapPath(".") & "\TempData\"
TempFolderName = RootFolderName & objUpload.GetUniqueName

'don't use unique file names in the beginning
objUpload.UseUniqueNames = false

'upload data till first uploaded file is encountered
set NextFile = objUpload.GetNextFile
call CheckErr

'set the various properties specified by the user
if IsEmpty(objUpload.Form("UniqueNames")) then
	objUpload.UseUniqueNames = false
else
	'if "unique file names" are selected set the flag
	objUpload.UseUniqueNames = true

	'change the NextFile object's name to be unique 
	if not(NextFile is nothing) then	
		NextFile.FileName = objUpload.GetUniqueName & "_" &	NextFile.FileName
	end if
end if 

'set the temp. folder name
if not(IsEmpty(objUpload.Form("FolderName"))) then
	TempFolderName = RootFolderName & objUpload.Form.Item("FolderName")
end if 

'set maximum upload size
if not(IsEmpty(objUpload.Form.Item("MaxUpload"))) then
	objUpload.MaxUploadSize = objUpload.Form.Item("MaxUpload")
	if objUpload.MaxUploadSize <> -1 then
		objUpload.MaxUploadSize = objUpload.MaxUploadSize * 1024
	end if
end if

'set the maximum number of files which can be uploaded at one time
if not(IsEmpty(objUpload.Form.Item("MaxFiles"))) then
	objUpload.MaxFileCount = objUpload.Form.Item("MaxFiles")
end if
'set the maximum file size allowed
if not(IsEmpty(objUpload.Form.Item("MaxSize"))) then
	objUpload.MaxFileSize = objUpload.Form.Item("MaxSize")
	if objUpload.MaxFileSize <> -1 then
		objUpload.MaxFileSize = objUpload.MaxFileSize * 1024
	end if
end if	

'create temp folder
objUpload.DirectoryCreate TempFolderName
call CheckErr

'upload the rest of the files one by one
do until NextFile is nothing
	'save file into the temp. folder
	NextFile.Save TempFolderName
	
	'get Nextfile object
	set NextFile = nothing
	set NextFile = objUpload.GetNextFile

	'exit the loop in case of error	
	if Err <> 0 then
		call CheckErr
		exit do
	end if
loop

'show file items collection
Response.Write "<P style='BACKGROUND-COLOR: #6495ed'><FONT color=#ffffff>"
Response.Write "&nbsp;Number of files uploaded: " & CStr(objUpload.Files.Count) & "</FONT></P>"
it = 0
for each item in objUpload.Files
	Response.Write "<TABLE border=0 cellPadding=0 cellSpacing=1 width='100%'"
	if cint(it/2.)*2 = it then
		Response.Write " style='BACKGROUND-COLOR: #ffffe0'>" 
	else
		Response.Write " style='BACKGROUND-COLOR: #ffffff'>" 
	end if 
	Response.Write "<TR><TD width='18%'>Input Tag name:</TD>"
	Response.Write "<TD>" & item.TagName & "</TD></TR>"
	Response.Write "<TR><TD>Content type:</TD>"
	Response.Write "<TD>" & item.ContentType & "</TD></TR>"
	Response.Write "<TR><TD>Size:</TD>"
	Response.Write "<TD>" & CStr(item.Size) & "</TD></TR>"
	Response.Write "<TR><TD>Original path:</TD>"
	Response.Write "<TD>" & item.OriginalPath & "</TD></TR>"
	Response.Write "<TR><TD>Server path:</TD>"
	Response.Write "<TD>" & item.Path & "</TD></TR>"
	Response.Write "</TABLE>"
	
	item.Delete		' Delete uploaded file
	
	it = it + 1
next

'show form items collection
Response.Write "<BR>"
Response.Write "<P style='BACKGROUND-COLOR: #6495ed'><FONT color=#ffffff>"
Response.Write "&nbsp;Number of form items: " & CStr(objUpload.Form.Count) & "</FONT></P>"
Response.Write "<TABLE border=0 cellPadding=0 cellSpacing=1>"
Response.Write "<TR><TD width='18%'><STRONG>Input Tag Name:</STRONG></TD>"
Response.Write "<TD width='18%'><STRONG>Value:</STRONG></TD></TR>"
it = 0
for each item in objUpload.Form
	if cint(it/2.)*2 = it then
		Response.Write "<TR width='18%' style='BACKGROUND-COLOR: #ffffe0'>" 
	else
		Response.Write "<TR width='18%' style='BACKGROUND-COLOR: #ffffff'>" 
	end if 
	Response.Write "<TD>" & item & "</TD>"
	Response.Write "<TD>" & item.Value & "</TD></TR>"
	it = it + 1
next
Response.Write "</TABLE>"

'delete temp folder
objUpload.DirectoryDelete TempFolderName, true
call CheckErr

'destroy Upload component instance
set objUpload = nothing
%> <P></P>
<P align=center><INPUT TYPE="button" VALUE="Go Back" id=GoBack name=GoBack LANGUAGE="javascript" ONCLICK="window.history.back();"></P>
</BODY>
</HTML>
