<%@ENABLESESSIONSTATE=FALSE%>
<%
'force this page to expire immediately
Response.Expires = -10000
%>
<html>
<head>
<meta NAME="GENERATOR" Content="Microsoft Visual Studio 6.0">
<meta http-equiv=refresh content="1,ProgressBar.asp?ProgressID=<%=Request.QueryString("ProgressID")%>">
<title>Upload Progress Bar</title>
</head>
<%
on error resume next

'create UploadProgress component instance
set UplProg = Server.CreateObject("Dundas.UploadProgress")

'this is where you would need to set the StateServer IP address
'   if StateServer.exe is running on a machine other than the machine 
'   where these ASP pages are stored.
'objUploadProgress.StateServer = "127.0.0.1"

'retrieve progress data from the StateServer
UplProg.ProgressID = Request.QueryString("ProgressID")
UplProg.GetProgress
Percentage = UplProg.PercentCompleted
TotalSize = UplProg.TotalSize
UploadedSize = UplProg.UploadedSize

if TotalSize = -1 or UploadedSize = -1 then
	TotalSize = 0
	UploadedSize = 0
end if

'if there are errors set the value to -1
if Err.number <> 0 then
	Percentage = -1
end if

'delete progress data when we hit 100%.  The next time GetProgress is called an
'   exception will be thrown since the Progress ID does not exist at the State Server, so
'   Percentage will be set to -1 and the window will be closed
if UplProg.PercentCompleted = 100 then
	UplProg.DeleteProgress
end if

set UplProg = nothing
%>
<script>
var val = <%=Percentage%>;
// If there are any errors - close frame
if(val == -1) {
	window.close();
}
</script>
<body>
<table border="1" width="100%">
  <tr>
    <td>
      <table ID="Prog" border="0" width="<%=Percentage%>%" bgcolor="#FF0000">
        <tr>
          <td width="100%">&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</body>
<P align="center"><FONT size="2">
<%=Percentage%>% (Uploaded <%=UploadedSize%> of <%=TotalSize%> bytes)
</FONT></P>
</html>
