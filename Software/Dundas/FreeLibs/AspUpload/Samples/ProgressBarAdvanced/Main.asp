<%@ENABLESESSIONSTATE=FALSE%>
<%
'******************************************************************************
'Copyright Dundas Software Ltd. 2000. All Rights Reserved.
'
'PURPOSE:           The user can try uploading files to the server using 
'                    different settings for the Dundas Upload control.
'
'POST DESTINATION:  UplLimitAction.asp.           
'
'Dundas Software Contact Information:
'	Email:	sales@dundas.com
'   Phone:	(800) 463-1492
'			(416) 467-5100
'	Fax:	(416) 422-4801
'******************************************************************************
Response.Expires = -10000
%>
<html>
<head>
<Script Language="javascript">
<!--
function CheckForCookiesOn()
{
//checks to see if browser has cookie support enabled,
//	if not an alert box is displayed, letting user know we need cookies
//also returns true if cookies disabled, otherwise false is returned	
	var blnCookiesSupported = false;
	
	//set the cookie
	document.cookie = 'testit' + "=" + escape('hi') + ";"
	
	//now attempt to retrieve the cookie
	var aCookie = document.cookie.split(";");
	for (var i=0; i < aCookie.length; i++)
	{ 
		var aCrumb = aCookie[i].split("=");
		if ('testit' == aCrumb[0])
		{
			blnCookiesSupported=true; 
			return false; 
		}	
		if (blnCookiesSupported==false){
			alert('For the progress bar to function correctly (for this particular demo) your browser must be set up so that it utilizes cookies.\nPlease note that cookie support is not required for the Progress Bar itself.');
			return true;
		}
	}
}
//-->
</Script>
<meta NAME="GENERATOR" Content="Microsoft Visual Studio 6.0">
</head>
<body color="black" bgcolor="white">
<table><tr><td><IMG border=0 height=36 src="../images/clouds.jpg" width=550></td></tr></table>
<span style="LEFT: 574px; POSITION: absolute; TOP: 20px; Z-INDEX: -1">
<a href="http://www.dundas.com">
<img align="right" border="0" src="../images/dundaslogo.gif" hspace="5" WIDTH="160" HEIGHT="18">
</a>
</span> <IMG border=0 height=50 src="../images/headline_upload.gif" width=390> 
<p>
<IMG align=absMiddle border=0 height=14 src="../images/scenario.gif" width=82>
</p>
<p>
<font size="3"><i>Using this demo you can upload files to the server using different settings. It demonstrates
accessing form data prior to the file upload, progress bar implementation, setting limitations on the uploading of data and the usage of unique names.</i>
</font>
</p>
<%
on error resume next

'create UploadProgress object
set UplProg = Server.CreateObject("Dundas.UploadProgress")
'set the StateServer IP address
'UplProg.StateServer = "127.0.0.1"
'retrieve a new progress ID
NewProgressID = UplProg.GetNewProgressID
set UplProg = nothing

if Err <> 0 then
	NewProgressID = -1
	'display an error message
	Response.Write "<P style='BACKGROUND-COLOR: #a52a2a'><FONT color=#ffffff>"
	Response.Write "&nbsp;&nbsp;" & "State Server Error:  " & Err.description & "&nbsp;&nbsp;Progress bar will be disabled!</FONT></P>"
end if
%>

<SCRIPT>
NewID = <%=NewProgressID%>;
function dorefresh()
{
	var CookiesOff;
	
	//if cookies are not enabled let user know that browser must be supporting cookies for this demo to run ok
	CookiesOff = CheckForCookiesOn();	
		
		
	if( NewID != -1 && String(window.document.cookie).indexOf("RefreshProgressID=TRUE") != -1 )
		window.location.reload(1);
	window.document.cookie = "RefreshProgressID=FALSE";


	if (CookiesOff == true){
	//by setting the ProgressID to -1 we make sure progress bar will not be displayed by the Upload function below	
		NewID = -1;
	}	
}
	
function upload()
{ 
	if (NewID != -1){	
	//only open progressbar.asp window if cookies are enabled, since we are using cookies
	//   to store a boolean (RefreshProgressID) to store the ProgressID (identifies this particular upload operation)
		window.document.cookie = "RefreshProgressID=TRUE";
		Param = "SCROLLBARS=no,RESIZABLE=no, TOOLBAR=no,STATUS=no,MENUBAR=no,WIDTH=400,HEIGHT=100";
		Param += ",TOP=" + String(window.screen.Height/2 - 50);
		Param += ",LEFT=" + String(window.screen.Width/2 - 200);
		window.open("ProgressBar.asp?ProgressID=<%=NewProgressID%>" , null, Param);
	}	
	
	document.UploadForm.action = "UploadDemoAction.asp?ProgressID=<%=NewProgressID%>"
	document.UploadForm.submit();
}

//make sure irregardless of how user gets here (e.g. Back button) that a new id is generated
//  every time the page is loaded. 
window.onload = dorefresh;

</SCRIPT>

<form name="UploadForm" action="UploadDemoAction.asp" encType="multipart/form-data" method="post" style="BORDER-BOTTOM: thin; BORDER-RIGHT-STYLE: none">
<p style="BACKGROUND-COLOR: #6495ed; BORDER-BOTTOM: thin solid; BORDER-LEFT: thin solid; BORDER-RIGHT: thin solid; BORDER-TOP: thin solid"><font color="#ffffff">&nbsp;&nbsp;SET UPLOAD SETTINGS</font>
<table width="100%" border="0" cellPadding="3" cellSpacing="3" style="BACKGROUND-COLOR: #ffffe0">
  <tr>
    <td width="20%">Unique file names: </td>
    <td><input ID="UniqueNames" NAME="UniqueNames" TYPE="checkbox" CHECKED></td>
    <td>Each uploaded file will be saved on the server with a unique name.</td></tr>
  <tr>
    <td>Temp. folder name: </td>
    <td><input NAME="FolderName" style="WIDTH: 94px"></td>
    <td>Server folder name for temporary file storage. You can keep it empty to use the unique folder name.</td></tr>
  <tr>
    <td>Max. upload size: </td>
    <td><input NAME="MaxUpload" VALUE="500" style="WIDTH: 94px"></td>
    <td> Maximum number of KBytes that can be uploaded. You can set it to -1 to disable the limit.</td></tr>
  <tr>
    <td>Max. file number: </td>
    <td><input NAME="MaxFiles" VALUE="2" style="WIDTH: 94px"></td>
    <td>Maximum number of files to upload. You can set it to -1 to disable the limit.</td></tr>
  <tr>
    <td>Max. file size: </td>
    <td><input NAME="MaxSize" VALUE="100" style="WIDTH: 94px"></td>
    <td>Maximum size of the file in KBytes which can be uploaded. You can set it to -1 to disable the limit.</td></tr>
    </table></p>


<p style="BACKGROUND-COLOR: #6495ed; BORDER-BOTTOM: thin solid; BORDER-LEFT: thin solid; BORDER-RIGHT: thin solid; BORDER-TOP: thin solid"><font color="#ffffff">&nbsp;&nbsp;SELECT FILES TO UPLOAD</font>
<table width="100%" border="0" cellPadding="3" cellSpacing="3" style="BACKGROUND-COLOR: #ffffe0">
  <tr>
    <td width="20%">File to upload #1: </td>
    <td><input NAME="File1" TYPE="file"></td></tr>
  <TR>
    <td>File to upload #2: </td>
    <td><input NAME="File2" TYPE="file"></td></TR>
  <TR>
    <td>File to upload #3: </td>
    <td><input NAME="File3" TYPE="file"></td></TR>
  <TR>
    <td>File to upload #4: </td>
    <td><input NAME="File4" TYPE="file"></td></TR>
    </table></p>

<p align="center"><input TYPE="button" VALUE="Upload File(s)" ONCLICK="upload()"><br></form></P>
</BODY>
</html>
