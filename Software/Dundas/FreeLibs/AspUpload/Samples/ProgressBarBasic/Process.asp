<%@ENABLESESSIONSTATE=FALSE%>
<!-- We disable maintaining session state so that more than one ASP 
         page can be processed at the same time -->
<%
'enable inline error trapping since most control methods throw an exception
'   if an error occurs
On Error Resume Next

dim objUpload	'instance of Upload component

'create Upload component
Set objUpload = Server.CreateObject("Dundas.Upload")

'we must set this to a valid ID created in main.asp so that 
objUpload.ProgressID = Request.QueryString("ProgressID")

'set maximum file size to 2 MByte
objUpload.MaxFileSize = 8000000

'save uploaded data to memory
objUpload.SaveToMemory
'check for error, and redirect back to main page with success/failure message
if Err.Number <> 0 then
	Response.Redirect "main.asp?Success=" & Err.Description
else
	Response.Redirect "main.asp?Success=The upload operation was successful."
end if

'release resources
set objUpload = nothing
%>
