<script language="VBScript" runat="server"> 


function TransformPLSQLToTSQL (sSql)
'===============================================================================
'	Conversion de sentencias PL-SQL a SQL 2005
'
'Necesita:	
'	sSql	sentencia SQL en PL-SQL
'
'Devuelve:
'	sSql	Sentencia convertida a SQL 2005	
'===============================================================================
	dim originalSQL, paginaWeb, lngRecs, IdSession, IdUsuario
    Dim Cmd
    Dim Param

	'------------------------------------------------------------
	' Coneccion la base
	'------------------------------------------------------------
	' Usa la coneccion a la base principal  
		
	'------------------------------------------------------------
	' Conversion de SQLs
	'------------------------------------------------------------
	originalSQL = sSql
	sSql = ""
	
	if len(originalSQL) > 0 then
		'---------------------------------------------
		' que pagina lo llamo
		'---------------------------------------------
		paginaWeb = Request.ServerVariables("URL")
		if len(paginaWeb)= 0 then
			paginaWeb =""
		end if
		
		'---------------------------------------------
		' ID Session del usuario 
		'---------------------------------------------
		IdSession = Session.SessionID
		if len(IdSession)= 0 then
			IdSession =""
		end if
		
		'---------------------------------------------
		' ID usuario actual
		'---------------------------------------------
			
		IdUsuario = Session("IDUtente")							
		if len(IdUsuario)= 0 then
			IdUsuario =0
		end if
		
						
		' ---- Comentarios de M3 ---------	
		'Si la consulta contiene la función de PL/SQL MONTHS_BETWEEN, 
		'invierto el orden de los parámetros en la misma
		
		'..originalSQL = reemplazo("BA.", Ucase(originalSQL), "BA..")
		'..originalSQL = reemplazo("PLAVORO.", Ucase(originalSQL), "PLAVORO..")
		' ---- Fin Comentarios de M3 ---------	
		
		'===================================================
		'	Ejecuta el comando de transformacion y el log
		'===================================================
		Set cmdGrabar = Server.CreateObject("ADODB.Command")
		With cmdGrabar
			.ActiveConnection = CC
			.CommandText = "spTransformPLSQLToTSQLext"
			.CommandType =	&H0004			' Const adCmdStoredProc = &H0004
			.CommandTimeOut = 60
			
			' Seteo de parametros de input			
			For Each Parameter In .Parameters
				If Parameter.Direction And &H0001 Then	'..Const adParamInput = &H0001
					select case mid(ucase(Parameter.Name), 2)
						case "PLSQL"
							Parameter.Value = originalSQL
						case "URL"
							Parameter.Value = paginaWeb
						case "SESSION"							
							Parameter.Value = IdSession	
						case "USUARIOID"
							Parameter.Value = cint(IdUsuario)
						case else
					end select 
				end if
			next
			
				
			' ejecuta el comando 
			.Execute lngRecs,, &H00000080	'Const adExecuteNoRecords = &H00000080

			' Obtiene los parametros de salida
			For Each Parameter In .Parameters
				'Const adParamOutput = &H0002
				'Const adParamReturnValue = &H0004
				If Parameter.Direction And (&H0002 Or &H0004) Then
					select case mid(ucase(Parameter.Name), 2)
						case "SQL"
							sSql = "" & Parameter.Value 
						case else
					end select 
				End If
			Next
		End With
		
		set cmdGrabar = nothing
	end if	 

	'-----------------------------
	' Salida
	'-----------------------------
	TransformPLSQLToTSQL = sSql
	
end function



function TransformPLSQLToTSQL2 (sSql)
	 
	originalSQL =""
	originalSQL =sSql
	
	sSQL = reemplazo("CONCAT(", Ucase(sSQL), "[dbo].[CONCAT](")
	sSQL = reemplazo("TRIM(", Ucase(sSQL), "[dbo].[TRIM](")
	sSQL = reemplazo("TO_CHAR(", Ucase(sSQL), "[dbo].[TO_CHAR](")
	sSQL = reemplazo("SYSDATE", Ucase(sSQL), "[dbo].[SYSDATE]()")
	sSQL = reemplazo("LENGTH(", Ucase(sSQL), "[dbo].[LENGTH](")
	sSQL = reemplazo("NVL(", Ucase(sSQL), "[dbo].[NVL](")
	sSQL = reemplazo("SUBSTR(", Ucase(sSQL), "[dbo].[SUBSTR](")
	sSQL = reemplazo("MONTHS_BETWEEN(", Ucase(sSQL), "DATEDIFF(MM,")
	sSQL = reemplazo("||", Ucase(sSQL), "+")
	
	sSelect = clausulaSelect(Ucase(sSql))
	'Debug
	'response.Write("<P>" + sSelect + "</P>")
	
	sFrom = clausulaFrom(Ucase(sSql))
	'Debug
	'response.Write("<P>" + sFrom + "</P>")
	
	sWhere = clausulaWhere(Ucase(sSql))
	'Debug
	'response.Write("<P>" + sWhere + "</P>")
	
	sWhere = transformOuterJoin (Ucase(sWhere))
	'Debug
	'response.Write("<P>" + sWhere + "</P>")
	
    'Modificacion
	sOrderBy = clausulaOrderBy(Ucase(sSql))

	
	If (len(sSelect) > 0) then
		sSQL = sSelect + " " + sFrom 
		If (len(sWhere) > 0) then
			'sSQL = sSQL + " WHERE " + sWhere
			sSQL = sSQL + " " + sWhere  + " " + sOrderBy
		End If
	End if

	
	TransformPLSQLToTSQL2 = sSQL	
	
	'------------------------------------------------------------
	' Graba en la tabla Log
	'------------------------------------------------------------
	'..Set ConnM3 = server.CreateObject ("ADODB.Connection")
	'..'ConnM3.open "DSN=BA;UID=sa;PWD=password;DATABASE=BA;Initial Catalog=BA;"
	'..ConnM3.open "DSN=BA2;UID=sa;PWD=sa;DATABASE=BA2;Initial Catalog=BA2;"

	' ejecuta la consulta.
    EmpleoLavoro = ""
    sSQL = reemplazo("'", Ucase(sSQL), "#")
    originalSQL = reemplazo("'", Ucase(originalSQL), "#")
	EmpleoLavoro = "INSERT INTO Log ([SQL], [PL-SQL],Hora, [URL]) VALUES ('" & sSQL & "', '" & originalSQL & "', getDate(), '" + Request.ServerVariables("URL")+ "' )"
	'Response.Write (Sql)
	
	CC.Execute (EmpleoLavoro)	
	
    '..ConnM3.Execute (EmpleoLavoro)	
	'..ConnM3.Close
	'..Set ConnM3 = Nothing
	
	'------------------------------------------------------------
	' Fin Grabacion en la tabla Log
	'------------------------------------------------------------
	
end function


function reemplazo(patron, sentencia, equivalente)

	If Instr(1, sentencia, patron, vbTextCompare) > 0 then
		reemplazo = replace(sentencia, patron, equivalente)
	Else
		reemplazo = sentencia
	End if
	
end function

function identificaString(sql, inicio, limite)

	Dim iLimite
	Dim iInicio
	
	iLimite = 0
	iInicio = 0

	iInicio = Instr(1, sql, inicio)
	if (iInicio <= 0)  then
		identificaString = ""
		exit function
	end if

	iLimite = Instr(1, sql, limite)
	if (iLimite <= 0) then
		iLimite = len(sql) + 1
	end if

	if ((iLimite - iInicio)) >= 0 then
		identificaString = mid(sql, iInicio, (iLimite - iInicio))
	end if
			
end function

function clausulaSelect(sql)

	clausulaSelect = identificaString(sql, "SELECT "," FROM ")

end function

function clausulaFrom(sql)

	clausulaFrom = identificaString(sql, "FROM "," WHERE ")

end function

function clausulaWhere(sql)

	clausulaWhere = identificaString(sql, "WHERE ","ORDER BY ")

end function

function clausulaOrderBy(sql)

	clausulaOrderBy = identificaString(sql, "ORDER BY "," HAVING BY ")

end function


function parseWhere(where)

	where = replace(where, "AND", "$")
	where = replace(where, "WHERE ", "")
	condiciones = split(where,"$")
	
	maxcounter=ubound(condiciones)
	
	parseWhere = condiciones

end function

function parseWhereCondition(whereCondition)

	whereCondition = replace(whereCondition, "=", "$")
	whereCondition = replace(whereCondition, ">=", "$")
	whereCondition = replace(whereCondition, "<=", "$")

	condiciones = split(whereCondition,"$")
	
	parseWhereCondition = condiciones
		
end function

function transformOuterJoin(where)

	comparador = ""
	retorno = ""
	
	If (Instr(1, where, "(+)") = 0) then
		transformOuterJoin = where
		exit function
	End if
		
	whereConditions = parseWhere(where)
	maxcounter=ubound(whereConditions)
	
	FOR counter=0 TO maxcounter
		
		If (Instr(1, whereConditions(counter), "(+)") > 0) Then
		
			whereCondition = parseWhereCondition(whereConditions(i))
			If (Instr(1, whereCondition(0), "(+)") > 0) Then
				comparador = "*="
						
			Else
				comparador = "=*"
			End if
			
			If (len(retorno) > 0) then
					retorno = retorno & " AND "
			End if

			retorno = retorno + whereCondition(0) + comparador + whereCondition(1)
			retorno = replace(retorno,"(+)", "")
			
		Else
		
			If (len(retorno) > 0) then
					retorno = retorno & " AND "
			End if
			retorno = retorno  + whereConditions(counter)
			
		End If

	Next

	'transformOuterJoin =  retorno
	transformOuterJoin =  "WHERE " + retorno
	
end function

</script>



