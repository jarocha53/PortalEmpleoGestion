<!--#include Virtual="/strutt_testa2.asp"-->
<TABLE border="0" width="520" cellspacing="0" cellpadding="0">
	<tr>
		<td width="520" valign="bottom" align="right" background="<%=Session("Progetto")%>/images/titoli/link.gif" height="38">
			<span class="tbltext1a"><b>LINK UTILI</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		</td>
	</tr>
</TABLE>
<br>
<TABLE border="0" width="520" cellspacing="3" cellpadding="3">
	<tr>
		<td class="textblack" valign="top" width="50%"><b class="textred">POLITICA e ISTITUZIONI</b><br>
			<a class="textblack" href="/Testi/links/politica/LK_parlmin.asp"><u>Parlamento e Ministeri</u></a>,
			<a class="textblack" href="/Testi/links/politica/LK_comeur.asp"><u>ComunitÓ europea</u></a>,
			<a class="textblack" href="/Testi/links/politica/LK_enti.asp"><u>Enti Locali</u></a>,
			<a class="textblack" href="/Testi/links/politica/LK_partpol.asp"><u>Partiti politici</u></a>, 
			<a class="textblack" href="/Testi/links/politica/LK_orgint.asp"><u>Organizzazioni internazionali</u></a>, 
			<a class="textblack" href="/Testi/links/politica/Info_entivari.asp"><u>Enti vari</u></a> 
		</td>
		<td class="textblack" valign="top" width="50%"><b class="textred">AZIENDE</b><br>
			<a class="textblack" href="/Testi/links/aziende/LK_servaziende.asp"><u>Servizi per le aziende</u></a>,
			<a class="textblack" href="/Testi/links/aziende/LK_stagetir.asp"><u>Stage e tirocini</u></a>,
			<a class="textblack" href="/Testi/links/aziende/LK_agevfinanz.asp"><u>Agevolazioni e finanziamenti</u></a>,
			<a class="textblack" href="/Testi/links/aziende/LK_asscat.asp"><u>Associazioni di categoria</u></a>
		</td>
	</tr>
	<tr>
		<td class="textblack" valign="top" width="50%"><b class="textred">LAVORO</b><br>
			<a class="textblack" href="/Testi/links/lavoro/LK_incontrodo.asp"><u>Incontro domanda-offerta</u></a>,
			<a class="textblack" href="/Testi/links/lavoro/LK_terzosett.asp"><u>Terzo settore</u></a>,
			<a class="textblack" href="/Testi/links/lavoro/LK_leglavoro.asp"><u>Legislazione sul lavoro</u></a>,
			<a class="textblack" href="/Testi/links/lavoro/LK_sindacati.asp"><u>Sindacati</u></a>,
			<a class="textblack" href="/Testi/links/lavoro/LK_autoimprend.asp"><u>Autoimprenditoria</u></a>
			<a class="textblack" href="/Testi/links/lavoro/LK_concorsipub.asp"><u>Concorsi pubblici</u></a>,
			<a class="textblack" href="/Testi/links/lavoro/LK_pattiterrit.asp"><u>Patti Territoriali</u></a>,
			<a class="textblack" href="/Testi/links/lavoro/LK_portalieu.asp"><u>Portali europei del lavoro</u></a>
			<a class="textblack" href="/Testi/links/lavoro/LK_emersione.asp"><u>Emersione e Sviluppo Locale</u></a>
		</td>
		<td class="textblack" valign="top" width="50%"><b class="textred">FORMAZIONE</b></a><br>
			<a class="textblack" href="/Testi/links/formazione/LK_guideonline.asp"><u>Corsi e manuali on line</u></a>,
			<a class="textblack" href="/Testi/links/formazione/LK_offertaformat.asp"><u>Offerta formativa regionale</u></a>,
			<a class="textblack" href="/Testi/links/formazione/LK_fondosociale.asp"><u>Fondo Sociale Europeo</u></a>,
			<a class="textblack" href="/Testi/links/formazione/LK_orientamento.asp"><u>Orientamento</u></a>,
			<a class="textblack" href="/Testi/links/formazione/Info_guideonline2.asp"><u>Guide on line</u></a>
		</td>
	</tr>
	<tr>
		<td class="textblack" valign="top" width="50%"><b class="textred">ISTRUZIONE</b></a><br>
			<a class="textblack" href="/Testi/links/istruzione/LK_obbligoformat.asp"><u>Obbligo formativo</u></a>,
			<a class="textblack" href="/Testi/links/istruzione/LK_scuolalav.asp"><u>Scuola-lavoro</u></a>,
			<a class="textblack" href="/Testi/links/istruzione/LK_scuoleonline.asp"><u>Scuole on line</u></a>,
			<a class="textblack" href="/Testi/links/istruzione/LK_unionline.asp"><u>UniversitÓ on line</u></a>,
			<a class="textblack" href="/Testi/links/istruzione/LK_didattica.asp"><u>Didattica</u></a>,
			<a class="textblack" href="/Testi/links/istruzione/LK_riformascol.asp"><u>Riforma scolastica</u></a>,
			<a class="textblack" href="/Testi/links/istruzione/LK_studenti.asp"><u>Studenti</u></a>
		</td>
		<td class="textblack" valign="top" width="50%"><b class="textred">IMMIGRAZIONE</b></a><br>
			<a class="textblack" href="/Testi/links/immigrazione/LK_stranieriit.asp"><u>Stranieri in Italia</u></a>,
			<a class="textblack" href="/Testi/links/immigrazione/LK_italestero.asp"><u>Italiani all'estero</u></a>
		</td>
	</tr>
	<tr>
		<td class="textblack" valign="top" width="50%"><b class="textred">COMPUTER E INTERNET</b></a><br>
			<a class="textblack" href="/Testi/links/computer/LK_motricerca.asp"><u>Motori di ricerca</u></a>,
			<a class="textblack" href="/Testi/links/computer/LK_risorseserv.asp"><u>Risorse e servizi</u></a>,
			<a class="textblack" href="/Testi/links/computer/LK_spwebmaster.asp"><u>Spazio webmaster</u></a>,
			<a class="textblack" href="/Testi/links/computer/LK_hardware.asp"><u>Hardware</u></a>,
			<a class="textblack" href="/Testi/links/computer/LK_software.asp"><u>Software</u></a>
		</td>
		<td class="textblack" valign="top" width="50%"><b class="textred">MONDO DONNA</b></a><br>
			<a class="textblack" href="/Testi/links/mondodonna/LK_lavorofem.asp"><u>Lavoro al femminile</u></a>,
			<a class="textblack" href="/Testi/links/mondodonna/LK_assocfem.asp"><u>Associazioni femminili</u></a>
		</td>
	</tr>
</TABLE>
<!--#include Virtual="/strutt_coda2.asp"-->
