<!--#include Virtual="/strutt_testa2.asp"-->

<div align="center">
  <center>
  <table border="0" width="520" cellspacing="0" cellpadding="0" height="38">
    <tr>
      <td width="520" background="<%=Session("Progetto")%>/images/titoli/cpi.gif" height="38" align="right">
        <table border="0" width="520" height="30" cellspacing="0" cellpadding="0">
          <tr>
            <td width="100%" valign="bottom" align="right" class="tbltext1a">
				<b>centri per l'impiego - Sicilia&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b>
			</td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  </center>

<table border="0" width="520" cellspacing="0" cellpadding="0">
  <tr>
    <td width="100%"><center>
        <table border="0" width="95%" cellspacing="0" cellpadding="0">
          <tr>
            <td width="100%" align="center">
				<img src="/Images/regioni/sicila.gif" width="343" height="195" usemap="#Map" border="0"> 
				<map name="Map"> 
				  <area shape="circle" coords="264,46,6" href="/Testi/inforegioni/sicilia/info_messina.asp" alt="Messina" title="Messina">
				  <area shape="circle" coords="132,61,6" href="/Testi/inforegioni/sicilia/info_palermo.asp" alt="Palermo" title="Palermo">
				  <area shape="circle" coords="80,79,6" href="/Testi/inforegioni/sicilia/info_trapani.asp" alt="Trapani" title="Trapani">
				  <area shape="circle" coords="188,98,6" href="/Testi/inforegioni/sicilia/info_enna.asp" alt="Enna" title="Enna">
				  <area shape="circle" coords="238,95,6" href="/Testi/inforegioni/sicilia/info_catania.asp" alt="Catania" title="Catania">
				  <area shape="circle" coords="166,108,6" href="/Testi/inforegioni/sicilia/info_caltanissetta.asp" alt="Caltanissetta" title="Caltanissetta">
				  <area shape="circle" coords="132,115,6" href="/Testi/inforegioni/sicilia/info_agrigento.asp" alt="Agrigento" title="Agrigento">
				  <area shape="circle" coords="249,137,7" href="/Testi/inforegioni/sicilia/info_siracusa.asp" alt="Siracusa" title="Siracusa">
				  <area shape="circle" coords="214,152,6" href="/Testi/inforegioni/sicilia/info_ragusa.asp" alt="Ragusa" title="Ragusa">
				</map>
 		    </td>
 		  </tr>
        </table></center>
	</td>
  </tr>
  <tr>
    <td width="100%"><center>
      <div align="center">
        <table border="0" width="95%" cellspacing="0" cellpadding="0">
          <tr>
            <td width="100%"><br>
				<%
					PathFileEdit = "\Testi\inforegioni\sicilia\info_sicilia.htm"
					on error resume next
					Server.Execute(PathFileEdit)
					If err.number <> 0 Then
						Response.Write "<b class='tbltext'><br>Pagina al momento non disponibile"
						Response.Write "<p>Contattare il Gruppo Assistenza Portale Italia Lavoro<br>"
						Response.Write "all'indirizzo <a href='mailto:po-assistenza@italialavoro.it'>po-assistenza@italialavoro.it</a></p></b>"
					End If
				%>
			</td>
		  </tr>
        </table></center>
	</td>
  </tr>
 </table>
  <br><a href="/Testi/inforegioni/info_cpi.asp"><img src="<%=Session("Progetto")%>/Images/indietro.gif" border="0"></a>

</div>


<!--#include Virtual="/strutt_coda2.asp"-->