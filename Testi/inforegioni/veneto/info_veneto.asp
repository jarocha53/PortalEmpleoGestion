<!--#include Virtual="/strutt_testa2.asp"-->

<div align="center">
  <center>
  <table border="0" width="520" cellspacing="0" cellpadding="0" height="38">
    <tr>
      <td width="520" background="<%=Session("Progetto")%>/images/titoli/cpi.gif" height="38" align="right">
        <table border="0" width="520" height="30" cellspacing="0" cellpadding="0">
          <tr>
            <td width="100%" valign="bottom" align="right" class="tbltext1a">
				<b>centri per l'impiego - Veneto&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b>
			</td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  </center>

<table border="0" width="520" cellspacing="0" cellpadding="0" height="38">
  <tr>
    <td width="100%"><center>
        <table border="0" width="95%" cellspacing="0" cellpadding="0">
          <tr>
            <td width="100%" align="center">
				<img src="/Images/regioni/veneto.gif" width="261" height="271" usemap="#Map" border="0"> 
				<map name="Map"> 
				  <area shape="circle" coords="155,70,9" href="/Testi/inforegioni/veneto/info_belluno.asp" alt="Belluno" title="Belluno">
				  <area shape="circle" coords="166,140,9" href="/Testi/inforegioni/veneto/info_treviso.asp" alt="Treviso" title="Treviso">
				  <area shape="circle" coords="97,156,9" href="/Testi/inforegioni/veneto/info_vicenza.asp" alt="Vicenza" title="Vicenza">
				  <area shape="circle" coords="123,192,8" href="/Testi/inforegioni/veneto/info_padova.asp" alt="Padova" title="Padova">
				  <area shape="circle" coords="55,180,8" href="/Testi/inforegioni/veneto/info_verona.asp" alt="Verona" title="Verona">
				  <area shape="circle" coords="131,231,9" href="/Testi/inforegioni/veneto/info_rovigo.asp" alt="Rovigo" title="Rovigo">
				  <area shape="circle" coords="154,177,10" href="/Testi/inforegioni/veneto/info_venezia.asp" alt="Venezia" title="Venezia">
				</map>
			</td>
 		  </tr>
        </table></center>
	</td>
  </tr>
  <tr>
    <td width="100%"><center>
        <table border="0" width="95%" cellspacing="0" cellpadding="0">
          <tr>
            <td width="100%"><br>
				<%
					PathFileEdit = "\Testi\inforegioni\veneto\info_veneto.htm"
					on error resume next
					Server.Execute(PathFileEdit)
					If err.number <> 0 Then
						Response.Write "<b class='tbltext'><br>Pagina al momento non disponibile"
						Response.Write "<p>Contattare il Gruppo Assistenza Portale Italia Lavoro<br>"
						Response.Write "all'indirizzo <a href='mailto:po-assistenza@italialavoro.it'>po-assistenza@italialavoro.it</a></p></b>"
					End If
				%>
			</td>
		  </tr>
        </table></center>
	</td>
  </tr>
 </table>
  <br><a href="/Testi/inforegioni/info_cpi.asp"><img src="<%=Session("Progetto")%>/Images/indietro.gif" border="0"></a>

</div>


<!--#include Virtual="/strutt_coda2.asp"-->