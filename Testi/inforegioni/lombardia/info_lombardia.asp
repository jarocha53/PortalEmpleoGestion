<!--#include Virtual="/strutt_testa2.asp"-->

<div align="center">
  <center>
  <table border="0" width="520" cellspacing="0" cellpadding="0" height="38">
    <tr>
      <td width="520" background="<%=Session("Progetto")%>/images/titoli/cpi.gif" height="38" align="right">
        <table border="0" width="520" height="30" cellspacing="0" cellpadding="0">
          <tr>
            <td width="100%" valign="bottom" align="right" class="tbltext1a">
				<b>centri per l'impiego - Lombardia&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b>
			</td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  </center>

<table border="0" width="520" cellspacing="0" cellpadding="0" height="38">
  <tr>
    <td width="100%"><center>
      <div align="center">
        <table border="0" width="95%" cellspacing="0" cellpadding="0">
          <tr>
            <td width="100%" align="center">
				<img src="/Images/regioni/lombardia.gif" width="310" height="245" usemap="#Map" border="0"> 
				<map name="Map"> 
				  <area shape="circle" coords="146,58,6" href="/Testi/inforegioni/lombardia/info_sondrio.asp" alt="Sondrio" title="Sondrio">
				  <area shape="circle" coords="119,90,6" href="/Testi/inforegioni/lombardia/info_lecco.asp" alt="Lecco" title="Lecco">
				  <area shape="circle" coords="92,97,6" href="/Testi/inforegioni/lombardia/info_como.asp" alt="Como" title="Como">
				  <area shape="circle" coords="57,98,6" href="/Testi/inforegioni/lombardia/info_varese.asp" alt="Varese" title="Varese">
				  <area shape="circle" coords="98,133,6" href="/Testi/inforegioni/lombardia/info_milano.asp" alt="Milano" title="Milano">
				  <area shape="circle" coords="145,113,6" href="/Testi/inforegioni/lombardia/info_bergamo.asp" alt="Bergamo" title="Bergamo">
				  <area shape="circle" coords="212,187,6" href="/Testi/inforegioni/lombardia/info_mantova.asp" alt="Mantova" title="Mantova">
				  <area shape="circle" coords="157,189,6" href="/Testi/inforegioni/lombardia/info_cremona.asp" alt="Cremona" title="Cremona">
				  <area shape="circle" coords="116,170,6" href="/Testi/inforegioni/lombardia/info_lodi.asp" alt="Lodi" title="Lodi">
				  <area shape="circle" coords="75,178,6" href="/Testi/inforegioni/lombardia/info_pavia.asp" alt="Pavia" title="Pavia">
				  <area shape="circle" coords="178,137,6" href="/Testi/inforegioni/lombardia/info_brescia.asp" alt="Brescia" title="Brescia">
				</map>
			</td>
 		  </tr>
        </table></center>
	</td>
  </tr>
  <tr>
    <td width="100%"><center>
      <div align="center">
        <table border="0" width="95%" cellspacing="0" cellpadding="0">
          <tr>
            <td width="100%"><br>
				<%
					PathFileEdit = "\Testi\inforegioni\lombardia\info_lombardia.htm"
					on error resume next
					Server.Execute(PathFileEdit)
					If err.number <> 0 Then
						Response.Write "<b class='tbltext'><br>Pagina al momento non disponibile"
						Response.Write "<p>Contattare il Gruppo Assistenza Portale Italia Lavoro<br>"
						Response.Write "all'indirizzo <a href='mailto:po-assistenza@italialavoro.it'>po-assistenza@italialavoro.it</a></p></b>"
					End If
				%>
			</td>
		  </tr>
        </table></center>
	</td>
  </tr>
 </table>
  <br><a href="/Testi/inforegioni/info_cpi.asp"><img src="<%=Session("Progetto")%>/Images/indietro.gif" border="0"></a>

</div>


<!--#include Virtual="/strutt_coda2.asp"-->