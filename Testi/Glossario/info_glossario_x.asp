
<!-- #include Virtual="/strutt_testa2.asp" -->
<table border="0" background="<%=Session("Progetto")%>\images\sfondocentrale.jpg" width="520" height="400" cellpadding="0" cellspacing="0">
	<tr>
		<td valign="top" align="right">
			<table border="0" background="<%=Session("Progetto")%>\images\Titoli\t_informazione.gif" width="520" height="38">
				<tr>
					<td class="titoletto" valign="bottom" align="right">
						<b>glossario operativo&nbsp;&nbsp;</b>
					</td>
				</tr>
			</table>

			<table border="0" width="520" height="38" cellpadding="0" cellspacing="0">
				<tr>
					<td class="titoletto" valign="bottom">
						<img border="0" src="<%=Session("Progetto")%>/Images/Titoli/t_immglossario.jpg" usemap="#FPMap0">
						<map name="FPMap0">
							<area shape="rect" coords="135, 20, 144, 32" href="/Testi/Glossario/info_glossario_A.asp" alt="A" title="A">
							<area shape="rect" coords="148, 19, 159, 35" href="/Testi/Glossario/info_glossario_B.asp" alt="B" title="B">
							<area shape="rect" coords="165, 17, 177, 32" href="/Testi/Glossario/info_glossario_C.asp" alt="C" title="C">
							<area shape="rect" coords="179, 20, 189, 31" href="/Testi/Glossario/info_glossario_D.asp" alt="D" title="D">
							<area shape="rect" coords="195, 20, 202, 36" href="/Testi/Glossario/info_glossario_E.asp" alt="E" title="E">
							<area shape="rect" coords="208, 20, 215, 31" href="/Testi/Glossario/info_glossario_F.asp" alt="F" title="F">
							<area shape="rect" coords="221, 22, 234, 31" href="/Testi/Glossario/info_glossario_G.asp" alt="G" title="G">
							<area shape="rect" coords="236, 22, 249, 31" href="/Testi/Glossario/info_glossario_H.asp" alt="H" title="H">
							<area shape="rect" coords="252, 21, 261, 29" href="/Testi/Glossario/info_glossario_I.asp" alt="I" title="I">
							<area shape="rect" coords="263, 20, 270, 31" href="/Testi/Glossario/info_glossario_J.asp" alt="J" title="J">
							<area shape="rect" coords="275, 21, 287, 30" href="/Testi/Glossario/info_glossario_K.asp" alt="K" title="K">
							<area shape="rect" coords="291, 20, 297, 29" href="/Testi/Glossario/info_glossario_L.asp" alt="L" title="L">
							<area shape="rect" coords="304, 20, 315, 31" href="/Testi/Glossario/info_glossario_M.asp" alt="M" title="M">
							<area shape="rect" coords="320, 22, 329, 30" href="/Testi/Glossario/info_glossario_N.asp" alt="N" title="N">
							<area shape="rect" coords="336, 20, 347, 29" href="/Testi/Glossario/info_glossario_O.asp" alt="O" title="O">
							<area shape="rect" coords="352, 21, 359, 29" href="/Testi/Glossario/info_glossario_P.asp" alt="P" title="P">
							<area shape="rect" coords="365, 22, 379, 31" href="/Testi/Glossario/info_glossario_Q.asp" alt="Q" title="Q">
							<area shape="rect" coords="382, 20, 391, 30" href="/Testi/Glossario/info_glossario_R.asp" alt="R" title="R">
							<area shape="rect" coords="397, 23, 406, 28" href="/Testi/Glossario/info_glossario_S.asp" alt="S" title="S">
							<area shape="rect" coords="410, 21, 417, 30" href="/Testi/Glossario/info_glossario_T.asp" alt="T" title="T">
							<area shape="rect" coords="423, 22, 435, 29" href="/Testi/Glossario/info_glossario_U.asp" alt="U" title="U">
							<area shape="rect" coords="440, 24, 448, 29" href="/Testi/Glossario/info_glossario_V.asp" alt="V" title="V">
							<area shape="rect" coords="454, 22, 467, 30" href="/Testi/Glossario/info_glossario_W.asp" alt="W" title="W">
							<area shape="rect" coords="475, 22, 481, 28" href="/Testi/Glossario/info_glossario_X.asp" alt="X" title="X">
							<area shape="rect" coords="487, 23, 497, 29" href="/Testi/Glossario/info_glossario_Y.asp" alt="Y" title="Y">
							<area shape="rect" coords="503, 22, 508, 29" href="/Testi/Glossario/info_glossario_Z.asp" alt="Z" title="Z">
						</map>
					</td>
				</tr>
			</table>
			<table border="0" width="520" height="38" cellpadding="0" cellspacing="0">
				<tr>
					<td align="left" class="tbltext2g"><b>X</b></td>
				</tr>
			</table>
			<table border="0" width="520" cellspacing="0" cellpadding="0" height="81">
			  <tr>
			    <td width="520"><center>
			        <table border="0" width="520" cellspacing="0" cellpadding="0">
			          <tr>
			            <td width="520">
							<%
								PathFileEdit = "\Testi\Glossario\info_glossario_X.htm"
								on error resume next
								Server.Execute(PathFileEdit)
								If err.number <> 0 Then
									Response.Write "<b class='tbltext'><br>Pagina al momento non disponibile"
									Response.Write "<p>Contattare il Gruppo Assistenza Portale Italia Lavoro<br>"
									Response.Write "all'indirizzo <a href='mailto:po-assistenza@italialavoro.it'>po-assistenza@italialavoro.it</a></p></b>"
								End If
							%>
					  </td></tr>
			        </table></center>
				</td>
			  </tr>
			</table>
		</td>
	</tr>
</table>

<!-- #include Virtual="/strutt_coda2.asp" -->