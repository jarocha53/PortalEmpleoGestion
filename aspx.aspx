<html>

<head>
<meta http-equiv="Content-Language" content="es">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>Para gestionar un Formularios de</title>
</head>

<body>

<table cellpadding="0" cellspacing="0" width="100%" height="383" id="table1">
				<!-- MSTableType="nolayout" -->
				<tr>
					<td valign="top">&nbsp;</td>
					<td valign="top">
					<p align="justify"><font color="#085584"><strong>
					<font face="Verdana" size="1">Para gestionar un </font>
					</strong><font face="Verdana" size="1">
					<span class="tbltext1"><b>Formularios de creación de Oficina 
					o de alta de usuarios del sistema ... </b></span></font></font></p>
					<ul type="square">
						<li>
						<p align="left" style="margin-top: 0; margin-bottom: 0">
						<font face="Verdana" size="1">
						<span style="background-color: #FFFFFF">Completar el formulario 
						utilizando los campos que figuran en el mismo</span></font></li>
						<li>
						<p align="left" style="margin-top: 0; margin-bottom: 0">
						<font face="Verdana" size="1">
						<span style="background-color: #FFFFFF">Imprimir y&nbsp; 
					firmar por el usuario y el supervisor de la oficina. </span>
						</font></li>
						<li>
						<p align="left" style="margin-top: 0; margin-bottom: 0">
						<font face="Verdana" size="1">
						<span style="background-color: #FFFFFF">Remitir el 
					formulario a la Gerencia de Empleo de su localidad. </span>
						</font></li>
						<li>
						<p align="left" style="margin-top: 0; margin-bottom: 0">
						<font face="Verdana" size="1">
						<span style="background-color: #FFFFFF">La Gerencia de 
						Empleo verificará y remitirá el formulario al Ministerio 
						de Trabajo, Empleo y Seguridad Social para su 
						procesamiento. </span></font></li>
					</ul>
					<p align="left" style="margin-top: 0; margin-bottom: 0">&nbsp;</p>
					<p align="center" style="margin-top: 0; margin-bottom: 0">
					<a href="formularios/DSI%20026%20-%20Solicitud%20de%20configuración%20oficina%20de%20empleo.pdf" style="text-decoration: none">
					<span style="font-size: 9pt; font-weight: 700">
					<font color="#085584">Formulario de configuración de oficina 
					de empleo y alta de usuarios</font></span></a></p>
					<p align="center" style="margin-top: 0; margin-bottom: 0">
					<span style="background-color: #FFFFFF">
					<font face="Verdana" size="1">Haga click en este link para 
					abrir el formulario </font></span></p>
					<p align="left" style="margin-top: 0; margin-bottom: 0">&nbsp;</td>
					<td valign="top" height="81">&nbsp;</td>
				</tr>
				<tr>
					<td width="1%">&nbsp;</td>
					<td width="98%" valign="top">
					<strong><font face="Verdana" size="1" color="#085584">
					Descargue el programa&nbsp; para la visualizar los 
					formularios.</font></strong><p>
					<font face="Verdana" size="1" color="#808080">El siguiente 
					vinculo </font><font face="Verdana" size="1">
							<a target="_blank" href="http://www.adobe.com">
							http://www.adobe.com</a></font><font face="Verdana" size="1" color="#808080">, le permitirá encontrar la aplicacion necesaria 
					para poder visualizar los formularios que se encuentran en 
					el sitio. La aplicacion es de descarga gratuita y permite 
					ver archivos en formato PDF, para Windows 95 
					o superior.</font></p>
					<font face="Verdana" size="1">
							<br>
							&nbsp;</font></td>
					<td height="159" width="1%">&nbsp;</td>
				</tr>
				</table>
		
</body>

</html>
