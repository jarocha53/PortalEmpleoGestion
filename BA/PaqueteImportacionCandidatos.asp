
<!--#include Virtual = "/IncludeMtss/fields.asp"-->

<!--#include virtual="/include/ObtieneSecuencia.asp"-->

<!--#include virtual="/IncludeMTSS/adovbs.asp"-->

<%
'


' sistema de Proyectos ---> habilitar estas sentencias 
'<!--#include virtual="/empleo/proyectos/inc/showreq.asp"-->

' POrtal de Empleo ---> habilitar estas sentencias 
'<!--#include virtual="/empleo/proyectos/inc/fields.asp"-->



FieldsInit()
FieldsLoadRequest
FieldsLoadRequestForm

Fields("Usuario") = Session("Usuario")

'=============================================================================
' Integracion Portal Proyectos - Asignacion de beneficiarios a proyectos 
' segun Documento 2 - 2007/02/12
'
'Necesita:
'	DondeEstoy	o sistema Portal =2, proyectos = 1
'	ID_Busqueda 
'	Id_Persona 
'	ConexionOracle 
'	ConexionSQL
'
'Devuelve: funcion = true --> OK
'	Status  --> control de estados
'	Mensaje			Mensaje de error
'
'OjO: !!! IMPORTANTE !!!
'	Este asp esta en proyectos y en el portal  
'	Si se hacen cambios hay que cambiar en los dos lados
'Ubicaciones:
'	En proyectos: 
'	En el portal: 
'=============================================================================

'---------------------------------------------------------------------------------------------
'	Interfase entre variables del ASP original del portal y los necesarios para la integracion
'---------------------------------------------------------------------------------------------
' Esta funcion se usa siempre que se da enviar un cambio de estado al candidato en una seleccion o aviso
function ProyectoAsignaCandidatoAlProyecto (DondeEstoy, ID_Busqueda, Id_Persona, ConexionOracle, ConexionSQL, Status, Mensaje)
	'FieldsInit()
	dim SQL, xID_Busqueda, xId_Persona, Seguir
	dim FinAsignacion

	xID_Busqueda = clng (ID_Busqueda)
	'xID_Busqueda = 3082
	xId_Persona = clng(Id_Persona)

	'-----------------------------------------------------------------------------	
	' pongo que hay un problema por si salta en algun lado en forma no controlada
	'-----------------------------------------------------------------------------		
	ProyectoAsignaCandidatoAlProyecto = false
	Status = "99"
	Mensaje = "Error del proceso"
	
	'==========================================================================
	' Hay que asignar algo al proyecto ? 
	'==========================================================================
	'--------------------------------------------------------------------------
	' 1) El aviso o seleccion esta asociado a un proyecto?
	' 2) Verificar que la fecha de fin de asignacion sea posterior a hoy
	' 3) Verificar si el Sistema donde se encuentra, le permite asignar gente
	'		esto sirve solo para el caso en que la funcion se llame desde el portal
	'--------------------------------------------------------------------------
	Seguir = true
	SQL = "Select RICHIESTA_SEDE.Gecal, RICHIESTA_SEDE.Programa, RICHIESTA_SEDE.Proyecto," _
			& " RICHIESTA_SEDE.DT_Fin_Asignacion As FechaFinAsignacion " _
			& " From PLAVORO.RICHIESTA_SEDE " _
			& " Where RICHIESTA_SEDE.ID_RICHIESTA = " & xId_Busqueda
	SQL = UCase(SQL)
	
	Set xRec = server.CreateObject("ADODB.RECORDSET")
	xRec.Open SQL, ConexionOracle 
		
	' Lee la cabecera del aviso o seleccion 
	' Evaluo resultados
	' 1.-Si hay registros y hay proyecto --> sigo adelante sino ve voy
	Seguir = false
	if ((xRec.EOF = true) or (isnull(xRec("Gecal").Value) = true)) then
		' se va bien
		ProyectoAsignaCandidatoAlProyecto = true
		Status = "10"
		Mensaje = "El aviso o Selección no esta relacionado con un proyecto"
		exit function	
	else
		Seguir = true
		fields("xGecal") = cint(xRec("Gecal").Value)
		fields("xPrograma") = cint(xRec("Programa").Value)
		fields("xProyecto") = cint(xRec("Proyecto").Value)
		FinAsignacion = "" & xRec("FechaFinAsignacion").Value	
	end if
	
'	Response.Write xRec("FechaFinAsignacion").Value 
'	Response.End
	xrec.close
	set xRec = nothing
		
	' Puede hacer este movimiento, este sistema? (DondeEstoy)
	if Seguir = true then		
		Dim QuienAsigna 
		Seguir = false		
		SQL = "Select formaAsignacion" _
			& " From ProgramaRRHH " _
			& " Where programa = " & fields("xPrograma")
		SQL = UCase(SQL)		
		Set xRec = server.CreateObject("ADODB.RECORDSET")
		xRec.Open SQL, ConexionSQL 		
		
		'
		' Evaluo resultados
		' Si QuienAsigna <> Sistema o QuienASigna <> 3 --> me voy.
		'
		QuienAsigna = xRec("formaAsignacion").Value 
				
		if ((xRec.EOF = true) or (isnull(QuienAsigna) = true)or (QuienAsigna <> DondeEstoy and QuienASigna <> 3) ) then
			' se va bien
			ProyectoAsignaCandidatoAlProyecto = true
			Status = "11"
			Mensaje = "No tiene permisos desde este Sistema para asignar gente a proyectos"
			exit function
		else 
			seguir = true		
		end if	
				
		xRec.close
		set xRec = nothing								
				
	end if		
		
' control de fecha de fin de asignacion 
' ESTE CONTROL SE HACE SOLO CUANDO SE VIENE DE PROYECTOS
' FECHA FIN ASIGNACION ES SIEMPRE > FECHA FIN PUBLICACION-
' SE USA PARA TOMAR CANDIDATOS DESPUES DE QUE EL AVISO NO ESTA MAS VISIBLE
'	' Falta hacer -- 
'	if xxxxxxxxx then
'		' se va mal
'		ProyectoAsignaCandidatoAlProyecto = false
'		Status = "12"
'		Mensaje = "La fecha de asignacion al proyecto esta vencida"
'		exit function	
'	end if	
	
	' control de quien hace el movimiento seguin ProyectoRRHH
	
	
	'-----------------------------------------------------------------------------
	' 3) Hay personas con estado de 'Aceptado' pendientes de asignar al proyecto? 
	' tengo la lista de personas a asignar 
	'-----------------------------------------------------------------------------
	'String sql a correr en el oracle
	
	SQL = "Select Persona.COD_FISC," _
				 & " RICHIESTA_CANDIDATO.ID_Persona, " _
				 & " PERS_DOC.COD_DOCST, " _
				 & " PERS_DOC.ID_DOC, " _
				 & " Persona.Nome, " _
				 & " Persona.SESSO, " _
				 & " Persona.DT_NASC, " _
				 & " Persona.STAT_CIV, " _
				 & " Persona.Num_Tel, " _
				 & " STATO_OCCUPAZIONALE.COD_STDIS, " _
				 & " IntermediacionEstados.AccionProyecto " _
				 & " From plavoro.RICHIESTA_CANDIDATO , plavoro.Persona , plavoro.STATO_OCCUPAZIONALE, plavoro.PERS_DOC, plavoro.IntermediacionEstados " _
				 & " Where Persona.ID_PERSONA = RICHIESTA_CANDIDATO.ID_PERSONA " _
				 & " and STATO_OCCUPAZIONALE.ID_PERSONA = RICHIESTA_CANDIDATO.ID_PERSONA " _
				 & " and PERS_DOC.ID_PERSONA = Persona.ID_PERSONA " _
				 & " and RICHIESTA_CANDIDATO.COD_ESITO_SEL = IntermediacionEstados.Estado " _
				 & " and STATO_OCCUPAZIONALE.IND_STATUS = 0 " _
				 & " and RICHIESTA_CANDIDATO.ID_RICHIESTA =" & xId_Busqueda _
				 & " and IntermediacionEstados.AccionProyecto > 0 "		

	' si tengo una persona definda --> busco solo esa
	if len(xId_Persona)> 0 and xId_Persona <> "0" then
		SQL = SQL + " and RICHIESTA_CANDIDATO.ID_PERSONA=" & xId_Persona
	end if
	SQL = UCase(SQL)
	
	Set xRec = server.CreateObject("ADODB.RECORDSET")
	xRec.Open SQL, ConexionOracle  
		
	' Si no hay registros validos --> me voy
	if xRec.BOF = true or xRec.EOF = true then
		xRec.close
		set xRec = nothing
		
		'--------------------------------------------------------------------
		' si no trae nada, entonces tengo que cambiar el estado del candidato
		' cambia a estado 17, esto no esta dinamico
		'--------------------------------------------------------------------
		if DondeEstoy  = 2 then ' solo cuando viene del portal
			
			ResultadoMovimiento = "17"
		
			sSQLUpdate = "UPDATE plavoro.RICHIESTA_CANDIDATO SET COD_ESITO_SEL=" & ResultadoMovimiento _
					& " WHERE ID_PERSONA = " & xId_Persona _
					& " AND ID_RICHIESTA = " & xID_Busqueda 
			
		'	Response.Write sSQLUpdate
		'	Response.End
			Set xRecUpdate = server.CreateObject("ADODB.RECORDSET")
			xRecUpdate.Open sSQLUpdate, ConexionOracle  
					
			Secuencia = ObtenerProximaSecuencia(xID_Busqueda,xId_Persona)

			sSQLinsert = "INSERT INTO plavoro.RICHIESTA_CANDIDATO_HIST (SECUENCIA,ID_RICHIESTA,ID_PERSONA,DT_INS,DT_TMST,COD_TIPO_INS,COD_ESITO_SEL) " _
				& " VALUES (" & Secuencia & "," _
							  & xID_Busqueda & ","_
							  & xId_Persona _
							  & ",SYSDATE,SYSDATE,'0'," _
							  & ResultadoMovimiento _
							  & ")"
			
			'Response.Write sSQLinsert
			
			Set xRecInsert = server.CreateObject("ADODB.RECORDSET")
			xRecInsert.Open sSQLinsert, ConexionOracle   	
		
		end if
		
		' se va bien
		ProyectoAsignaCandidatoAlProyecto = true
		Status = "20"
		Mensaje = "No hay candidatos para asignar"
		exit function
	end if
		
	'============================================================================
	' Hay personas para asignar al proyecto
	'
	' Tengo el recordset abierto con la lista de personas
	'============================================================================
	'-----------------------------------------------------------------------
	'FUNCION DEL ASP:
	' Trae los datos de la base ORACLE y los mete en una tabla SQL.
	'   Mete 1 o varios Candidatos del portal en una tabla SQL llamada 
	'   Richiesta_CandidatoTmp.
	'-----------------------------------------------------------------------
	'-------------------------------------------------------------------
	'	Genera un numero de importacion a usar
	'-------------------------------------------------------------------
	Fields("Gecal") = "-1"
	Fields("Programa") = "-22"  ' contador de paquetes
	Fields("NroRichiestaDatos") = "0"
		
		
	' sp para generar el numero de paquete
	Set cmdPaquete = Server.CreateObject("ADODB.Command")
	With cmdPaquete
		.ActiveConnection = ConexionSQL
		.CommandText = "ProyectoContadoresUpd" 
		.CommandType = adCmdStoredProc
		.CommandTimeOut = 1600
		FieldsSaveParameters .Parameters
		.Execute lngRecs,, adExecuteNoRecords
		FieldsLoadParameters .Parameters
		' numero de paquete asignado
		if len(Fields("ProyectoProx")) > 0 then
			Fields("NroRichiestaDatos") = Fields("ProyectoProx")		
		else
			' Error al asignar el numero de paquete
			xRec.close
			set xRec = nothing
		
			' se va mal
			ProyectoAsignaCandidatoAlProyecto = false
			Status = "100"
			Mensaje = "Problemas al asignar numero de paquete de importación"
			exit function
		end if
		
	End With
		
	'Response.Write "Abro Loop: " & Time & "<BR>"
	'-------------------------------------------------------------
	'Recorro los registro y grabo en Richiesta_CandidatoTmp
	'-------------------------------------------------------------
	Do While not xRec.EOF
		'Armo fields para pasar al store	
		' datos del aviso o seleccion y el proyecto a asignar
	
		fields("Id_Richiesta") = ID_Busqueda
		fields("Gecal") = fields("xGecal") 
		fields("Programa") = fields("xPrograma")
		fields("Proyecto") = fields("xProyecto")
		fields("AccionProyecto") = fields("AccionProyecto")
		' datos de la persona
		Fields("Cuil") = xrec("COD_FISC").Value
		Fields("ID_Persona") = xrec("ID_Persona").Value
		Fields("TipoDoc") = xrec("COD_DOCST").Value
		Fields("NumDoc") = xrec("ID_DOC").Value
		Fields("Nombre") = xrec("Nome").Value
		Fields("Sexo") = xrec("SESSO").Value
		Fields("FechaNacimiento") = xrec("DT_NASC").Value
		Fields("EstadoCivil") = xrec("STAT_CIV").Value
		Fields("Telefono") = xrec("Num_Tel").Value
		Fields("EstadoOcupacional") = xrec("COD_STDIS").Value
			
		'Response.Write "Leo Registro: " & Time & "<BR>"
		'-------------------------------------------------------------
		'Inserto los registro en la tabla temporal
		'-------------------------------------------------------------
		Set cmdGrabar = Server.CreateObject("ADODB.Command")
			With cmdGrabar
				.ActiveConnection = ConexionSQL
				.CommandText = "ElavoroImportacionSQLCandidatos" 
				.CommandType = adCmdStoredProc
				.CommandTimeOut = 1600
				FieldsSaveParameters .Parameters
				.Execute lngRecs,, adExecuteNoRecords
				FieldsLoadParameters .Parameters
			End With
		Set cmdGrabar = nothing

		'------------------------------------------------			
		' siguiente registro
		'------------------------------------------------			
		xRec.MoveNext
	Loop
	xRec.close
	set xRec = nothing

	' variables que necesita el store para armar las TMPs
	fields("DondeEstoy") = DondeEstoy
	fields("Id_Busqueda") = ID_Busqueda
	'=====================================================
	'	Armo el paquete de importacion
	'=====================================================
	'Busco los proyectos y Grabo en las Tablas: ImportacionTMP, ImporatcionesProyectosTmp y ImportacionesBeneficiariosTmp
	Set cmdGrabar = Server.CreateObject("ADODB.Command")
	With cmdGrabar
		.ActiveConnection = ConexionSQL
		.CommandText = "ElavoroImportacionesTMP" 
		.CommandType = adCmdStoredProc
		.CommandTimeOut = 1600
		FieldsSaveParameters .Parameters
		'For Each P In .Parameters
		'	Response.Write P.Name & " = " & P.Value & "<BR>"
		'Next
		'Response.end
			
		
		 .Execute lngRecs,, adExecuteNoRecords
		 FieldsLoadParameters .Parameters
	End With

	'=====================================================
	'	Salida
	'=====================================================
	
	ProyectoAsignaCandidatoAlProyecto = true
	Status = "100"
	Mensaje = "Se ha preparado a las personas para asignar al proyecto"

	Fields("Gecal") = Fields("xGecal")
	Fields("Programa") = Fields("xPrograma")	
	
	'----------------------------------------------------------
	' si estoy del lado del portal, determino si el movimiento
	' va a ser en forma directa o normal. Esto determina a que store
	' se tiene que llamar.
	' GENERA EL MOVIMIENTO DE ASIGNACION / DESASIGNACION / RESERVA
	'------------------------------------------------------------
	if DondeEstoy  = 2 then ' solo cuando viene del portal
	
		SQL =" select Movimiento from ProgramaRRHHmovimCandidatos" _ 
		   & " Where Programa = " & Fields("Programa")
		
		Set xRec = server.CreateObject("ADODB.RECORDSET")
		xRec.Open SQL, ConexionSQL 
		
		Directo = xrec("Movimiento").Value
		
		if ((xRec.EOF = true) or  (isnull(Directo) = true)) then
				xRec.close
				set xRec = nothing
				' se va bien
				ProyectoAsignaCandidatoAlProyecto = true
				Status = "23"
				Mensaje = "No esta definido el movimiento que se dese hacer"
				exit function
		 else				 
			if Directo = 1 then 'movimiento normal
				SP = "BenMovimientosImp"														  			
			else
				SP= "BenMovimientosDirectos"	'movimiento directo			
			end if
		 end if
		 
		 'llamo al sp que genera el movimiento
		 '------------------------------------
			Set cmdGrabar = Server.CreateObject("ADODB.Command")
			With cmdGrabar
				.ActiveConnection = ConexionSQL
				.CommandText = SP
				.CommandType = adCmdStoredProc
				.CommandTimeOut = 1600
				FieldsSaveParameters .Parameters
				.Execute lngRecs,, adExecuteNoRecords
				FieldsLoadParameters .Parameters				
			End With
			Set cmdGrabar = nothing 			
		
		'funcion que actualiza el estado del candidato en el portal
		
		numeroImportacion = fields("Numero")
		ProyectoActualizarEstadoCandidato numeroImportacion, ConexionOracle, ConexionSQL, Status, Mensaje

		
	end if		
				
End function



function ProyectoActualizarEstadoCandidato(numeroImportacion, ConexionOracle, ConexionSQL, Status, Mensaje)
' Esta funcion evalua el resultado de la importacion, y de acuerdo a eso actualiza
' el estado del candidato en el portal

'-------------------------------------------------------------------------
'PASOS:
'	1.- tomar los resultados de la importacion : importacionesBeneficiariosTMP
'	2.- Update sobre la tabla ProyectoRRHHPaquetes con los codigos de resultado
'		que estan en la tabla importacionesBeneficiariosTMP	
'	3.- Actulizar los estados de los candidatos en funcion a los resultados que
'		estan en la tabla ProyectoRRHHPaquetes	
'--------------------------------------------------------------------------

	'------------------------------------------------------------
	' el paso 1 y 2, lo resuelve el sp:ElavoroImportacionResultados
	'-------------------------------------------------------------
	Fields("numero") = numeroImportacion
	
	Set cmdGrabar = Server.CreateObject("ADODB.Command")
	With cmdGrabar
		.ActiveConnection = ConexionSQL
		.CommandText = "ElavoroImportacionResultados"
		.CommandType = adCmdStoredProc
		.CommandTimeOut = 1600
		FieldsSaveParameters .Parameters
		.Execute lngRecs,, adExecuteNoRecords
		FieldsLoadParameters .Parameters				
	End With
	Set cmdGrabar = nothing 			
	
	SQLresultado = "select id_richiesta,id_persona,resultado from ProyectoRRHHPaquetes" _
			& " where nroimportacion= " & numeroImportacion
	
	Set xRec = server.CreateObject("ADODB.RECORDSET")
	xRec.Open SQLresultado, ConexionSQL
	
	' Si no hay registros validos --> me voy
	if xRec.BOF = true or xRec.EOF = true then
		xRec.close
		set xRec = nothing
		
		' se va bien
		ProyectoActualizarEstadoCandidato = true
		Status = "26"
		Mensaje = "No hay candidatos para en el paquete de importacion"
		exit function
	end if
	
	Do While not xRec.EOF
		'Armo fields para pasar al store	
		resultado = xrec("resultado").Value
		id_richiesta = xrec("id_richiesta").Value
		id_persona = xrec("id_persona").Value
		'-------------------------------------------------------------
		' dependiendo del tipo de resultado actuliza el estado del 
		' candidato en el portal
		'-------------------------------------------------------------
					  
			'determinar el siguiente estado tiene que ser dinamico,
			'falta !!!!!!!!!!!!!!!!!!!!!!!!		  
			' candidato aceptado
			if resultado = 1 then
				ResultadoMovimiento = 12 'estado asignado en el portal
			end if	
			' rechazado por validacion		  		  
			if resultado = 2 then
				ResultadoMovimiento = 11 'estado asignado en el portal
			end if
			
			sSQLUpdate = "UPDATE plavoro.RICHIESTA_CANDIDATO SET COD_ESITO_SEL=" & ResultadoMovimiento & "," _
					& " Nro_Importacion = " & numeroImportacion _
					& " WHERE ID_PERSONA = " & id_persona _
					& " AND ID_RICHIESTA = " & id_richiesta 
			
			Set xRecUpdate = server.CreateObject("ADODB.RECORDSET")
			xRecUpdate.Open sSQLUpdate, ConexionOracle  
					
			Secuencia = ObtenerProximaSecuencia(id_richiesta,id_persona)

			sSQLinsert = "INSERT INTO plavoro.RICHIESTA_CANDIDATO_HIST (SECUENCIA,ID_RICHIESTA,ID_PERSONA,DT_INS,DT_TMST,COD_TIPO_INS,COD_ESITO_SEL, Nro_Importacion) " _
				& " VALUES (" & Secuencia & "," _
							  & id_richiesta & ","_
							  & id_persona _
							  & ",SYSDATE,SYSDATE,'0'," _
							  & ResultadoMovimiento & ","_
							  & numeroImportacion _
							  & ")"
			
			Set xRecInsert = server.CreateObject("ADODB.RECORDSET")
			xRecInsert.Open sSQLinsert, ConexionOracle   
			
		'------------------------------------------------			
		' siguiente registro
		'------------------------------------------------			
		
		xRec.MoveNext
	Loop	
	
	xRec.close
	set xRec = nothing


End function



%>

