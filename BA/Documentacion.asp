<HTML>
<HEAD>
<TITLE><%=Session("TIT")%> [ <%=Session("login")%> ] </TITLE>
<LINK REL=STYLESHEET TYPE="text/css" HREF='<%=session("progetto")%>/fogliostile.css'>
</head>
<body class=sfondocentro topmargin="2" leftmargin="0">       
 <%
'Session(menusx) tiene traccia della parte di menu a tendina selezionata 
If Request("S") <> "" Then
	Session("menusx") = Request("S") 
End If

'Response.Write Request("S")

	If Session("Progetto") & "*" = "*" Then
		Response.Redirect "/util/error_login.asp"
	End If	

		Session("strx") = "0"
		
		fileinc = Session("Progetto") & "/top.asp"
		'Response.Write fileinc
		
		Server.Execute fileinc
		fileinc = Session("Progetto") & "/tabinizio.asp"
		Server.Execute fileinc
		
		'Response.Write fileinc
		
%>
<table border="0" cellpadding="0" cellspacing="0" height="100%" valign="top" width="100%" align="center">
	<tr>
	    <td align="center" valign="top" colspan="4" class="sfondocentro">
         <br>
         <table width="800" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="10"></td>
            <td width="413"><span class="celeste18"> Documentación </span></td>
          </tr>
          <tr>
            <td></td>
            <td height="1" colspan="2" bgcolor="#cccccc"></td>
          </tr>
          <tr>
            <td></td>
            <td height="10" colspan="2"><BR></td>
          </tr>          
          <tr>
            <td width="10"></td>
            <td width="413"><span class="tbltext1"><b> Formularios de Alta de Oficinas y Usuarios del Portal</b></span></td>
          </tr>
                    
          <tr>
            <td></td>
            <td height="10" colspan="2"><BR></td>
          </tr>
          <tr>
  		    <td width="10"></td>
            <td height="20" colspan="2" class="notitext">
            <table width="100%"  border="0" cellspacing="0" cellpadding="0">
     
                <tr>
                  <td ALIGN="JUSTIFY" COLSPAN="3" CLASS="TBLTEXT1">
                Como gestionar su solicitud de configuración de Oficina de empleo o solicitud de usuario?
				<ol>
				<li>Completar el formulario "Solicitud de configuración oficina de empleo" o "Solicitud de usuario Portal Empleo".
				<li>Imprimir el formulario y firmar por el usuario y el supervisor de la oficina. 
				<li>Remitir el formulario a la Gerencia de Empleo del Municipio. 
				<li>La Gerencia de Empleo remitirá el formulario por correo interno del ministerio a la oficina de la USE.
				<li>La DSE recibe y aprueba el alta de la oficina o del usuario y remite el formulario a la Dirección de Sistemas Informáticos quien procesará la solicitud.
				</ol>
                  </td>
                </tr>       
 
                 <tr>
                  <td ALIGN="JUSTIFY" COLSPAN="3" CLASS="TBLTEXT1">
					<BR>
                  </td>
                </tr>             
                <tr>
                  <td><img src="/ba/images/visto.gif"></td>
                  <td>&nbsp;</td>
                  <td><span class="tbltext1"><a TARGET="_new" href="/BA/Testi/SistDoc/Doc_Ext/DSI 026 - Solicitud de configuración oficina de empleo.pdf"><B>Solicitud de configuración oficina de empleo</B></a></td>
                </tr>            
                <tr>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td width=""><img src="/ba/images/visto.gif"></td>
                  <td width="">&nbsp;</td>
                  <td width=""><span class="tbltext1"><a TARGET="_new" href="/BA/Testi/SistDoc/Doc_Ext/DSI 027 - Solicitud de usuario Portal Empleo.pdf"><B>Solicitud de usuario Portal Empleo</B></a></td>
                </tr>
                
                <tr bordercolor="White">
                  <td>&nbsp;</td>
                  <td></td>
                  <td></td>
                </tr>

				<tr><td colspan="3"><table border="2" bordercolor="Black">
                
                <tr bordercolor="White">
                  <td>&nbsp;</td>
                  <td></td>
                  <td></td>
                </tr>
                                
                <tr bordercolor="White">
                  <td colspan="3" class="tbltextv"><b>Descarga de Aplicaciones para la visualización de los formularios.</b></td>
                </tr>

                <tr bordercolor="White">
                  <td>&nbsp;</td>
                  <td></td>
                  <td></td>
                </tr>
                				
                <tr>
					<td CLASS="tbltextv" bordercolor="White" colspan="3">                
						El siguiente enlace le permitirá encontrar las aplicaciones necesarias para poder visualizar los 
						documentos e informes que se encuentran en el sitio. Las 
						aplicaciones son de descarga gratuita y permiten descomprimir 
						archivos en formato ZIP o abrir archivos en formato PDF, tanto para PC con 
						Windows 95 o superior y para Macintosh con MAC OS 8.6 o superior.
					</td>
				</tr>
                
                <tr bordercolor="White">
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                
                <tr bordercolor="White">
                  <td width=""><img src="/ba/images/visto.gif"></td>
                  <td width=""></td>
                  <td width=""><span class="tbltext1"><a TARGET="_new" href="http://www.trabajo.gov.ar/upper/ayuda.htm">
                  <B>Descargar Aplicaciones</B></a></td>
                </tr>
				</table></td></tr>               
              </table>       
              </td>
          </tr>
          <tr>
            <td></td>
            <td height="1" colspan="2"></td>
          </tr>
          <tr>
            <td colspan="3" align="center"><br><br>
            <a href="javascript:history.back();" class="textred"><b>Volver</b></a>
            </td>
            </tr>
        </table> 
        <br>
         </td>
    </tr>
<tr>
		<td height="48" width="151" bgcolor="#C84438" valign="top">
		&nbsp;</td>
		<td height="48" width="655" valign="bottom" bgcolor="#085584" colspan="3">
		<p align="center"><span class="gris11">
		<font face="Verdana" color="#FFFFFF" style="font-size: 6pt">Copyright © 
		2006 - Todos los derechos reservados<br>
		<b>Ministerio de Trabajo, Empleo y Seguridad Social de la Nación</b><br>
		Av L.eandro N. Alem 650 (C1001AAO) Ciudad Autónoma de Buenos Aires - 
		República Argentina <br>
		Sitio desarrollado por la Dirección de Sistemas Informáticos</font></span></td>
		<!--<td height="48" width="9" bgcolor="#085584" bordercolor="#E5E5E5" bordercolorlight="#E5E5E5" bordercolordark="#E5E5E5">&nbsp;</td>
		<td height="48" width="166" colspan="2" valign="top" bgcolor="#085584">
		&nbsp;</td>-->
</tr>
</table>
<%
	fileinc = Session("Progetto") & "/tabchiusura.asp"
	server.execute fileinc
%>
</center></div>
<% If Request.ServerVariables("SERVER_NAME") = "194.177.112.23" Then %>
<!--TABLE WIDTH=100% BGCOLOR=yellow BORDER=0 CELLSPACING=1 CELLPADDING=1>
	<TR>
		<TD ALIGN=CENTER><FONT COLOR=BLUE SIZE=4 FACE=VERDANA><B>Sito di Test</B></FONT></TD>
	</TR>
</TABLE-->
<% End If  %>
</BODY>
</HTML>