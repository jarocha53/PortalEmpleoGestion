
<!-- DOCTYPE html -->
<script type="text/javascript">
  $(function(){
    $("#accordion").accordion({
          heightStyle: "content"
    });  

    $( "#submenu_update_curriculum" ).menu();
    $( "#submenu_curriculum" ).menu();
    $( "#submenu_occupational_interests" ).menu();
    $( "#submenu_profile_options" ).menu();
    $( "#submenu_configurations" ).menu();
    
  })
</script>

<style type="text/css">
  #accordion h3{
    width: 167px;
  }

  #accordion h3{
    font-size: 12px;
  }

  #accordion div{
    padding: 0px;
    padding-left: 10px;
  }

  #accordion ul{
    list-style: none;
  }

  #accordion ul li{
    font-size: 11px;    
  }
</style>

<%
    'Especificación UTF-8
    'Response.CodePage = 65001
    'Response.CharSet = "utf-8"
 %>


<div id="accordion">
  <h3>Actualizar Hoja de Vida</h3><!-- (/BA/home.asp'&#44) -->
  <div>
    <ul id="submenu_update_curriculum">
      <li><a href="/Pgm/Iscr_Utente/UTE_ModDati.asp">
        <span class="ui-icon ui-icon-person"></span>Informaci&oacute;n Personal
      </a></li>
      <li><a href="/Pgm/BilancioCompetenze/Competenze/TitoliStudio/Titoli_Studio.asp">
        <span class="ui-icon ui-icon-note"></span>Formaci&oacute;n y Capacitaci&oacute;n
      </a></li>
      <li><a href="/Pgm/BilancioCompetenze/Competenze/Espro/PreEspro.asp">
        <span class="ui-icon ui-icon-wrench"></span>Experiencia Laboral
      </a></li>
      <li><a href="/Pgm/Iscr_Utente/UTE_VisAutCert.asp">
        <span class="ui-icon ui-icon-refresh"></span>Informaci&oacute;n Adicional
      </a></li>
    </ul> 
  </div>
  
  <h3>Ver Hoja de Vida</h3> 
  <div>
     <ul id="submenu_curriculum">
      <li><a href="/Pgm/Curriculum/default.asp">
        <span class="ui-icon ui-icon-search"></span>Ver Hoja de Vida
      </a></li>
    </ul>    
  </div>

  <h3>Intereses Ocupacionales</h3> 
  <div>
    <ul id="submenu_occupational_interests">
      <li><a href="/Pgm/BilancioCompetenze/AnagCandidature/default.asp">
        <span class="ui-icon ui-icon-suitcase"></span>Intereses Ocupacionales
      </a></li>
    </ul>    
  </div>

  <h3>Vacantes y Postulaciones</h3> 
  <div>
    <ul id="submenu_profile_options">
      <li><a href="/Pgm/Busqueda/BUS_VisOpLavoro.asp">
        <span class="ui-icon ui-icon-arrowthickstop-1-w"></span>Vacantes Relacionadas con su Perfil
      </a></li>
       <li><a href="/Pgm/Busqueda/BUS_VisContatti.asp">
        <span class="ui-icon ui-icon-check"></span>Postulaciones a Vacantes
      </a></li>
     </ul>    
  </div>

<!--   <h3>Vacantes Disponibles</h3> 
  <div>
    <ul id="submenu_profile_options">
      <li><a href="/Pgm/Busqueda/BUS_Busqueda.asp?Tipo=1">
        <span class="ui-icon ui-icon-arrowthickstop-1-w"></span>Vacantes Disponibles
      </a></li>
    </ul>    
  </div>
 -->
  <h3>Configuraci&oacute;n</h3>
  <div>
    <ul id="submenu_configurations">
      <li><a href="/util/password.asp">
        <span class="ui-icon ui-icon-locked"></span>Cambiar Contrase&ntilde;a
      </a></li>
      <li><a href="/util/logout.asp">
        <span class="ui-icon ui-icon-circle-close"></span>Salir
      </a></li>
    </ul>
  </div>
</div>

<!-- 
CAMBIAR CONTRASEÑA (/util/password.asp)

 
SALIR (/util/logout.asp)
-->