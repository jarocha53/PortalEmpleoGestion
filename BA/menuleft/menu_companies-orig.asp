<!-- DOCTYPE html -->
<script type="text/javascript">
  $(function(){
    $("#accordion").accordion({
          heightStyle: "content"
    });  

    $( "#submenu_empleyer_management" ).menu();
    $( "#submenu_management_and_placement" ).menu();
    $( "#submenu_community" ).menu();
    $( "#submenu_configurations" ).menu();
    
  })
</script>

<style type="text/css">
  #accordion h3{
    width: 167px;
  }

  #accordion h3{
    font-size: 12px;
  }

  #accordion div{
    padding: 0px;
    padding-left: 10px;
  }

  #accordion ul{
    list-style: none;
  }

  #accordion ul li{
    font-size: 11px;    
  }

   
 
 
</style>

<div id="accordion">
  	<h3>Gesti&oacute;n Empleador</h3><!-- (/BA/home.asp'&#44) -->
  	<div>
    	<ul id="submenu_empleyer_management">
      		<li><a href="/pgm/imprese/IMP_ModImpresa.asp">
        		<span class="ui-icon ui-icon-pencil"></span>Mis Datos
      		</a></li>
  		</ul>
	</div>
	<h3>Gesti&oacute;n de Vacantes</h3>
  	<div>
  		<ul id="submenu_management_and_placement">
        <li><a href="/Pgm/Imprese/IMP_BusquedaResultados.asp?Tipo=1&TEmp=E">
            <span class="ui-icon ui-icon-newwin"></span>Vacantes
          </a></li>
        <li><a href="/Pgm/Busqueda/BUS_Ingresa.asp?Tipo=1&TEmp=E&TipoPrestacion=1">
            <span class="ui-icon ui-icon-newwin"></span>Publicar Vacantes
          </a></li>
<!--       		<li><a href="/Pgm/Imprese/IMP_BusquedaResultados.asp?Tipo=0&TEmp=E">
        		<span class="ui-icon ui-icon-check"></span>Selecciones
      		</a></li>
 -->  		</ul>
  	</div>
  	<h3>Comunidad</h3>
  	<div>
  		<ul id="submenu_community">
  			<li><a href="/pgm/formazione/faq/faq_viscanali.asp">
        		<span class="ui-icon ui-icon-help"></span>Preguntas Frecuentes
      		</a></li>
      		<li><a href="/Pgm/Formazione/Forum/FOR_VisCanali.asp">
        		<span class="ui-icon ui-icon-comment"></span>Foros
      		</a></li>
  		</ul>
  	</div>

  	<h3>Configuraci&oacute;n</h3>
	<div>
	<ul id="submenu_configurations">
	  <li><a href="/util/password.asp">
	    <span class="ui-icon ui-icon-locked"></span>Cambiar Contrase&ntilde;a
	  </a></li>
	  <li><a href="/util/logout.asp">
	    <span class="ui-icon ui-icon-circle-close"></span>Salir
	  </a></li>
	</ul>
	</div>
</div>