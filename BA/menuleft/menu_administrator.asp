<!-- DOCTYPE html -->
<script type="text/javascript">
  $(function(){
    $("#accordion").accordion({
          heightStyle: "content"
    });




    $("#submenu_monitor").menu()
    $("#submenu_employee_center").menu()
    $("#submenu_colocation").menu()
    $("#submenu_employer").menu()
    $("#submenu_search_jobs").menu()
    $("#submenu_private_area_operator").menu()
    $("#submenu_comunity").menu()
    $("#submenu_management").menu()
    $("#submenu_configurations").menu()
  })
</script>

<style type="text/css">
  #accordion h3{
    width: 167px;
  }

  #accordion h3{
    font-size: 12px;
  }

  #accordion div{
    padding: 0px;
    padding-left: 10px;
  }

  #accordion ul{
    list-style: none;
  }

  #accordion ul li{
    font-size: 11px;    
  }
</style>

<%
    'Especificación UTF-8
    'Response.CodePage = 65001
    'Response.CharSet = "utf-8"
 %>


<div id="accordion">
  
  <h3>Buscador de Empleo</h3><!-- (/BA/home.asp'&#44) -->
  <div>
    <ul id="submenu_search_jobs">
      <li><a href="/pgm/iscr_utente/ute_ricutente.asp">
        <span class="ui-icon ui-icon-shuffle"></span>Entrevistar
      </a></li>
      <li><a href="/pgm/fascicolo/fas_RicUtente.asp">
        <span class="ui-icon ui-icon-mail-open"></span>Consultar Historia Laboral
      </a></li>
      <%if Session("creator") <> 0 then
          if session("idutente") = 31 then %>
            <li><a href="/pgm/iscr_utente/certification.asp">
              <span class="ui-icon ui-icon-arrowthickstop-1-s"></span>Descargar Certificado
            </a></li>
            <li><a href="/Pgm/precarga/precargaOferentes.asp">
              <span class="ui-icon ui-icon-arrowthickstop-1-s"></span>Precarga
            </a></li>
          <%
          end if
        end if          
      %>
    </ul> 
  </div>
  
  <h3>Empleador</h3> 
  <div>
     <ul id="submenu_employer">
      <li><a href="/pgm/imprese/IMP_VerificacionCuitEmpresa.asp?TEmp=E">
        <span class="ui-icon ui-icon-plusthick"></span>Nuevo Empleador
      </a></li>
      <li><a href="/pgm/imprese/IMP_BusquedaFiltros.asp?VieneDePagina=Busqueda&TEmp=E">
        <span class="ui-icon ui-icon-search"></span>B&uacute;squeda Empleador
      </a></li>
      <li><a href="javascript:void(1)" class="ui-state-disabled">
        Gesti&oacute;n y colocaci&oacute;n
      </a></li>
      <li><a href="/Pgm/Imprese/IMP_BusquedaFiltros.asp?VieneDePagina=Avisos&TEmp=E">
        <span class="ui-icon ui-icon-newwin"></span>Vacantes
      </a></li>
      <%if Session("creator") <> 0 then
          if session("idutente") = 31 then %>
            <li><a href="/Pgm/Imprese/IMP_BusquedaFiltros.asp?VieneDePagina=Selecciones&TEmp=E">
              <span class="ui-icon ui-icon-check"></span>Selecciones
            </a></li>
          <%
          end if
        end if          
      %>
    </ul>    
  </div>

  <h3>Centro de Empleo</h3>
  <div style="width:100%">
    <ul id="submenu_employee_center">
      <li><a href="javascript:void(1)" class="ui-state-disabled">
        Gesti&oacute;n y colocaci&oacute;n
      </a></li>
      <li><a href="/Pgm/Busqueda/BUS_Busqueda.asp?Tipo=1">
        <span class="ui-icon ui-icon-newwin"></span>Vacantes
      </a></li>
      <%if Session("creator") <> 0 then
          if session("idutente") = 31 then %>
            <li><a href="/Pgm/Busqueda/BUS_Busqueda.asp?Tipo=0">
              <span class="ui-icon ui-icon-check"></span>Selecciones 
            </a></li>
          <%
          end if
        end if          
      %>
      <li><a href="javascript:void(1)" class="ui-state-disabled">
        Gesti&oacute;n de entrevistas
      </a></li>
      <li><a href="/Pgm/Entrevistas/EntrevistasPorOperador.asp?EstadoEntrevista=20">
          <span class="ui-icon ui-icon-bullet"></span>Finalizadas  
      </a></li>
      <%if Session("creator") <> 0 then
          if session("idutente") = 31 then %>
            <li><a href="/Pgm/Entrevistas/EntrevistasPorOperador.asp?EstadoEntrevista=100">
                <span class="ui-icon ui-icon-radio-on"></span>No Finalizadas 
            </a></li>
           <%
          end if
        end if          
      %>
   </ul>
  </div>

<h3>Monitoreo</h3>
  <div>
    <ul id="submenu_monitor">
      <li>
        <a href="/Pgm/monitor/report/Statistiche/STA_ListStatistiche.asp">
          <span class="ui-icon ui-icon-signal"></span>Estad&iacute;sticas 
        </a>
      </li>
       <li>
        <a href="/Pgm/monitor/report/Reports/listReports.asp">
          <span class="ui-icon ui-icon-signal"></span>Reportes 
        </a>
      </li>
      <%if Session("creator") <> 0 then
          if session("idutente") = 31 then %>
        <li>
          <a href="/Pgm/monitor/report/Estrattore/EST_CreaEstrattore.asp">
            <span class="ui-icon ui-icon-signal"></span>Consultas 
          </a>
        </li>
          <%
          end if
        end if          
      %>
    </ul>
  </div>

<%
if Session("creator") <> 0 then
    if session("idutente") = 31 then %>
        <h3>&Aacute;rea Privada Operador</h3>
          <div>
            <ul id="submenu_private_area_operator">
              <li><a href="/pgm/ProjectSite/Rubrica/default.asp">
                <span class="ui-icon ui-icon-contact"></span>Lista de Contactos  
              </a></li>
              <li><a href="/pgm/ProjectSite/Agenda/default.asp">
                <span class="ui-icon ui-icon-calendar"></span>Agenda Privada  
              </a></li>
              <li><a href="/Pgm/Formazione/comunicazioni/com_lista.asp">
                <span class="ui-icon ui-icon-mail-closed"></span>Mensajes  
              </a></li>
            </ul>
          </div>

        <h3>Comunidad</h3>
        <div>
          <ul id="submenu_comunity">
            <li><a href="/pgm/formazione/faq/faq_viscanali.asp">
              <span class="ui-icon ui-icon-help"></span>Preguntas Frecuentes  
            </a></li>
            <li><a href="/Pgm/Formazione/Forum/FOR_VisCanali.asp">
              <span class="ui-icon ui-icon-comment"></span>Foros 
            </a></li>
          </ul>
        </div>
    <% end If
end If
    %>

    
  <% if Session("creator") <> 0 then
    if session("idutente") = 31 then %>
        <h3>Gerencia</h3>
        <div>
          <ul id="submenu_management">
            <li><a href="/Pgm/Management/reportRegisteredCompanies.asp">
              <span class="ui-icon ui-icon-help"></span>Reporte de Empresas Inscritas
            </a></li>
            <li><a href="/Pgm/Management/reportRegisteredPeople.asp">
              <span class="ui-icon ui-icon-help"></span>Reporte de Personas Inscritas
            </a></li>
            <li><a href="/Pgm/Management/reportdeals.asp">
              <span class="ui-icon ui-icon-help"></span>Reporte Perfiles Vigentes
            </a></li>
            <li><a href="/Pgm/Management/reportpublisheddeals.asp">
              <span class="ui-icon ui-icon-help"></span>Reporte Perfiles Vencidos  
            </a></li>
            <li><a href="/Pgm/Management/reportPostuEstad.asp">
              <span class="ui-icon ui-icon-help"></span>Reporte Postulados y sus Estados
            </a></li>
            <li><a href="/Pgm/Management/reportOferenSinCentro.asp">
              <span class="ui-icon ui-icon-help"></span>Reporte de oferentes sin centro asociado
            </a></li>
            <li><a href="/Pgm/Management/reportEntrevistas.asp">
              <span class="ui-icon ui-icon-help"></span>Reporte de Entrevistas
            </a></li>
            <li><a href="/Pgm/Management/reportCurCapacitacion.asp">
              <span class="ui-icon ui-icon-help"></span>Reporte de cursos de capactaci&oacute;n
            </a></li>
            <li><a href="/Pgm/Management/reportDireccionamientos.asp">
              <span class="ui-icon ui-icon-help"></span>Reporte de direccionamientos
            </a></li>
              <li><a href="/Pgm/Management/reportVacantesXgrupoocupacional.asp">
              <span class="ui-icon ui-icon-help"></span>Reporte de vacantes por grupo ocupacional
            </a></li>
            <li><a href="/Pgm/Management/reportVacantesXpuestoocupacional.asp">
              <span class="ui-icon ui-icon-help"></span>Reporte de vacantes por puesto
            </a></li>
          </ul>
        </div>
    <% end If %>
  <% end If %>

  <% if Session("creator") <> 0 then %>
    <% if session("super_admin") = 1 then %>
        <h3>Gesti&oacute;n Usuarios Centro</h3>
        <div>
          <ul id="submenu_configurations">
            <li><a href="/util/create_user_centro.asp">
                  <span class="ui-icon ui-icon-person"></span>Registrar Usuario
              </a>
            </li>
            <li><a href="/util/users_list.asp">
                  <span class="ui-icon ui-icon-folder-open"></span>Listado de Usuarios
              </a>
            </li>
          </ul>
        </div>
    <% end If %>
  <% end If %>


  <h3>Configuraci&oacute;n</h3>
  <div>
    <ul id="submenu_configurations">
      <li><a href="/util/password.asp">
        <span class="ui-icon ui-icon-locked"></span>Cambiar Contrase&ntilde;a
      </a></li>
      <li><a href="/bajaUtente.asp">
        <span class="ui-icon ui-icon-circle-arrow-s"></span>Dar de baja
      </a></li>
      <li><a href="/util/logout.asp">
        <span class="ui-icon ui-icon-circle-close"></span>Salir
      </a></li>
    </ul>
  </div>

</div>

</div>

<!-- 
CAMBIAR CONTRASEÑA (/util/password.asp)

 
SALIR (/util/logout.asp)
-->