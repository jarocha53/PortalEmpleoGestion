<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #include virtual="/strutt_testa2.asp"-->
<!--#include virtual ="/include/openconn.asp"-->
<!--#include virtual ="/util/portallib.asp"-->
<%
if ValidateService(session("idutente"),"MDD_RICDATIPERSONALI", CC) <> "true" then 
	response.redirect "/util/error_login.asp"
end if
%>
<!-- ************** Javascript Inizio ************ -->

<script LANGUAGE="Javascript">

<!--#include virtual="/Include/help.inc"-->

<!--#include virtual = "/include/SelComune.js"-->

//include del file per fare i controlli sulla validit� delle date
<!--#include virtual = "/Include/ControlDate.inc"-->

//include del file per fare i controlli sulla numericit� dei campi
<!--#include virtual = "/Include/ControlNum.inc"-->

//include del file per fare i controlli sulla validit� del codice fiscale
<!--#include virtual = "/Include/ControlCodFisc.inc"-->

//include del file per fare i controlli sulle stringhe
<!--#include virtual = "/Include/ControlString.inc"-->

var windowArea

function PulisciCom()
{
	document.frmDomIscri.txtComuneNascita.value = ""
	document.frmDomIscri.txtComune.value = ""
}

function PulisciRes()
{
	document.frmDomIscri.txtComuneRes.value = ""
	document.frmDomIscri.txtComRes.value = ""
}


//Funzione per i controlli dei campi da inserire 

//-------- Controlli di OBBLIGATORIETA' --------
//-------- Controlli FORMALI            --------
//-------- Controlli di RELAZIONE       --------

	function ControllaDati(frmDomIscri){

		//Cognome
		frmDomIscri.txtCognome.value=TRIM(frmDomIscri.txtCognome.value)
		if (frmDomIscri.txtCognome.value == ""){
			alert("Cognome obbligatorio")
			frmDomIscri.txtCognome.focus() 
			return false
		}
		//Nome
		frmDomIscri.txtNome.value=TRIM(frmDomIscri.txtNome.value)
		if (frmDomIscri.txtNome.value == ""){
			alert("Nome obbligatorio")
			frmDomIscri.txtNome.focus() 
			return false
		}
		//Data di Nascita
		DataNascita = TRIM(frmDomIscri.txtDataNascita.value)
		if (DataNascita == ""){
			alert("Data di Nascita obbligatoria")
			frmDomIscri.txtDataNascita.focus() 
			return false
		}
		//Controllo della validit� della Data di Nascita
		if (!ValidateInputDate(DataNascita)){
			frmDomIscri.txtDataNascita.focus() 
			return false
		}
		//Comune di Nascita e Provincia di Nascita obbligatori
		//e in alternativa con Nazione di Nascita
		frmDomIscri.txtComuneNascita.value=TRIM(frmDomIscri.txtComuneNascita.value)
		if (frmDomIscri.txtComuneNascita.value == ""){ 
			if (frmDomIscri.cmbProvinciaNascita.value == ""){
				if (frmDomIscri.cmbNazioneNascita.value == ""){
					alert("Comune e Provincia di Nascita o Nazione di Nascita obbligatori")
					frmDomIscri.cmbProvinciaNascita.focus() 
					return false
				}
			}
		}
       
		if (frmDomIscri.txtComuneNascita.value != ""){ 
			if (frmDomIscri.cmbProvinciaNascita.value != ""){
				if(frmDomIscri.cmbNazioneNascita.value != ""){
					alert("Indicare solo il Comune e la Provincia di Nascita oppure la Nazione di Nascita")
					frmDomIscri.cmbNazioneNascita.focus() 
					return false
				}
			}	
		}
		//Se Comune di Nascita � digitato, la Provincia � obbligatoria
		if (frmDomIscri.txtComuneNascita.value != ""){
			if (frmDomIscri.cmbProvinciaNascita.value == ""){
				alert("Provincia di Nascita obbligatoria")
				frmDomIscri.cmbProvinciaNascita.focus() 
				return false
			}
		}
		//Se la Provincia di Nascita � digitata, il Comune � obbligatorio
		if  (frmDomIscri.txtComuneNascita.value == ""){
			if (frmDomIscri.cmbProvinciaNascita.value != ""){
				alert("Comune di Nascita obbligatorio")
				frmDomIscri.txtComuneNascita.focus() 
				return false
			}
		}
		//Sesso
		if (frmDomIscri.cmbSesso.value == ""){
			alert("Sesso obbligatorio")
			frmDomIscri.cmbSesso.focus() 
			return false
		}
		//Codice Fiscale
		frmDomIscri.txtCodFisc.value = TRIM(frmDomIscri.txtCodFisc.value)
		if (frmDomIscri.txtCodFisc.value == ""){
			alert("Codice Fiscale obbligatorio")
			frmDomIscri.txtCodFisc.focus() 
			return false
		}
		//Codice Fiscale deve essere di 16 caratteri
		if (frmDomIscri.txtCodFisc.value.length != 16){
			alert("Formato del Codice Fiscale errato")
			frmDomIscri.txtCodFisc.focus() 
			return false
		}
		//Controllo la validit� del Codice Fiscale
		var contrCom=""
		DataNascita = frmDomIscri.txtDataNascita.value;
		CodFisc = frmDomIscri.txtCodFisc.value.toUpperCase();
		Sesso = frmDomIscri.cmbSesso.value;
		
		if (frmDomIscri.txtComuneNascita.value != ""){
		          contrCom =frmDomIscri.txtComune.value;   
		}
		else{
		          contrCom = frmDomIscri.cmbNazioneNascita.value
		}
		
		if (!ControllCodFisc(DataNascita,CodFisc,Sesso,contrCom)){
			alert("Codice Fiscale Errato");
			frmDomIscri.txtCodFisc.focus(); 
			return false
		}
	return true
	}

//----------------------------------------------------------------------------------------------------------------------------------------------------------	
	
</script>

<!-- ************** Javascript Fine   ************ -->

<!-- ************** ASP Inizio *************** -->

<!--#include virtual = "/include/DecCod.asp"-->
<!--#include virtual = "/include/ControlDateVB.asp"-->
<!--#include virtual = "/util/dbutil.asp"-->

<%	
'-------------------------------------------------------------------------------------------------------------------------------
Sub Titolo(sTitolo,sDescTitolo) %>
    <table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
		<tr height="18">
			<td class="sfondomenu" width="67%" height="18"><span class="tbltext0"><b>&nbsp;<%=sTitolo%></b></span></td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
			<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">(*) campi obbligatori</td>
		</tr>
	</table>
	<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
		<tr>
			<td align="left" class="sfondocomm"><%=sDescTitolo%>
				<a href="Javascript:Show_Help('/Pgm/help/ModDati/MDD_ModDatiPersonali')" onmouseover="javascript:window.status=' '; return true"><img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
			</td>
		</tr>
		<tr height="2">
			<td class="sfondocomm" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>
	<br>
<%
end sub

Sub Msgetto(Msg)
%>
	<br>
	<table align="center">
		<tr>
			<td class="tbltext3"><b><%=Msg%></b></td>
		</tr>	
	</table>
<%
End Sub

'-------------------------------------------------------------------------------------------------------------------------------

sub Inizio()
%>
	<table border="0" width="525" cellspacing="0" cellpadding="0" height="70">
		<tr>
		    <td width="530" background="<%=Session("Progetto")%>/images/titoli/cercolavoro2b.gif" height="70" align="right">
		       <br><br>
				<table border="0" width="260" height="20" cellspacing="0" cellpadding="0">
					<tr>
						<td width="100%" valign="baseline" align="center"><span class="tbltext1a"><b>Modifica Codice Fiscale</b></span></td>
					</tr>
		       </table>
			</td>
		</tr>
	</table>
	<br>	
	
	<form method="post" name="frmDomIscri" onsubmit="return ControllaDati(this)" action="MDD_CnfDatiPersonali.asp">
		<input type="hidden" name="txtoggi" value="<%=ConvDateToString(Date())%>">
 
<%
end sub

'------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

sub ImpPagina()
    dim sInt
    sPers = Request.Form ("Idpers")
    sSql = "SELECT COGNOME,NOME,DT_NASC, " &_
           "COM_NASC,PRV_NASC,STAT_NASC,COD_FISC,SESSO " &_ 
           "FROM PERSONA WHERE ID_PERSONA=" & sPers
    
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
    set rsDati = cc.execute(sSql)           

    sCommento =  "Presta molta attenzione a quanto inserisci.<br>" &_ 
		        " Ai fini della verifica del Codice Fiscale si prenderanno in considerazione:<br>" &_ 
		        " sesso, luogo e data di nascita."  
				        
    call Titolo("Informazioni Personali",sCommento)
	    %>
	    <input type="hidden" name="txtPers" value="<%=sPers%>">
 
		<table width="500" border="0" cellspacing="2" cellpadding="1">
		<tr>
			<td align="left" class="tbltext1" width="30%">
				<b>Cognome*</b>
			</td>
			<td align="left">
				<input style="TEXT-TRANSFORM: uppercase;" size="30" maxlength="30" class="textblack" name="txtCognome" value="<%=rsDati("COGNOME")%>" readonly>
			</td>
	    </tr>
	    <tr>
			<td align="left" class="tbltext1" width="30%">
				<b>Nome*</b>
			</td>
			<td align="left">
				<input style="TEXT-TRANSFORM: uppercase;" size="30" maxlength="30" class="textblack" name="txtNome" value="<%=rsDati("NOME")%>" readonly>
			</td>
	    </tr>

	    <tr>
			<td align="left" class="tbltext1" width="30%">
				<b>Data di Nascita*</b> (gg/mm/aaaa)
			</td>	
			<td align="left">
				<input style="TEXT-TRANSFORM: uppercase;" size="11" maxlength="10" class="textblack" name="txtDataNascita" value="<%=rsDati("DT_NASC")%>">
			</td>
	    </tr>
	    <tr><td>&nbsp;</td></tr>
	    <tr>
			<td colspan="4">
	            <span class="textblack">	Per <b>i nati in Italia</b> specificare provincia e comune di nascita.  		</span>  
	            <br><br>
	     	</td>
	    </tr>
	    <tr>		
			<td align="left" class="tbltext1" width="30%">
				<b>Provincia di Nascita*</b>
			</td>			
			<td align="left">				
<%
			sInt = "PROV|0|" & date & "|" & rsDati("PRV_NASC") & "|cmbProvinciaNascita' onchange='PulisciCom()|ORDER BY DESCRIZIONE"			
			CreateCombo(sInt)
			
			sComune = rsDati("COM_NASC")
				if sComune <> "" then			
					sSQL = "SELECT CODCOM,DESCOM from COMUNE WHERE CODCOM = '" & sComune & "'"
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
			        set rsComune = CC.Execute(sSQL)
			        
					If  rsComune.EOF  Then
						descComune = "Comune/Stato non codificato"
					Else   
						descComune = rsComune("DESCOM") 
					end if
					
					rsComune.Close
					
				    set rsComune = nothing
				end if			
%>
			</td>
		</tr>		
		<tr>		
			<td align="left" class="tbltext1" width="30%">
				<b>Comune di Nascita*</b>
			</td>
			<td nowrap>
				<span class="tbltext">
				<input type="text" name="txtComuneNascita" style="TEXT-TRANSFORM: uppercase;" class="textblack" size="35" readonly value="<%=DescComune%>">
				<input type="hidden" id="txtComune" name="txtComune" value="<%=sComune%>">
<%
				NomeForm="frmDomIscri"
				CodiceProvincia="cmbProvinciaNascita"
				NomeComune="txtComuneNascita"
				CodiceComune="txtComune"
				Cap="NO"
%>
				<a href="Javascript:SelComune('<%=NomeForm%>','<%=CodiceProvincia%>','<%=NomeComune%>','<%=CodiceComune%>','<%=Cap%>')" ID="imgPunto1" name="imgPunto1" onmouseover="javascript:window.status='';return true"><img border="0" src="<%=Session("Progetto")%>/images/bullet1.gif"></a>
				</span>				
			</td>
		</tr>
		<tr><td>&nbsp;</td></tr>		
		<tr>
			<td colspan="4" class="textblack">
				Per<b> i nati fuori dall'Italia</b>, invece, specificare solo la nazione di nascita.  	
				<br><br>
			</td>
		</tr>
		
	    <tr>
			<td align="left" class="tbltext1" width="30%" value="<%=rsDati("STAT_NASC")%>">
				<b>Nazione di Nascita</b>
	        </td>
			<td align="left">
				<%
				set rsComune = Server.CreateObject("ADODB.Recordset")
				sSQL = "SELECT CODCOM,DESCOM from COMUNE WHERE CODCOM like 'Z%' " &_
						"ORDER BY DESCOM"

'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
				rsComune.Open sSQL, CC
				
				%>
				<select class="textblack" ID="cmbNazioneNascita" name="cmbNazioneNascita">
				<option></option>
				<%
				do while not rsComune.EOF
				    if rsDati("STAT_NASC") = rsComune("CODCOM") then
						Response.Write "<OPTION "

						Response.write "value ='" & rsComune("CODCOM") & _
							"' selected> " & rsComune("DESCOM")  & "</OPTION>"
					
						rsComune.MoveNext 
					else
						Response.Write "<OPTION "

						Response.write "value ='" & rsComune("CODCOM") & _
							"'> " & rsComune("DESCOM")  & "</OPTION>"
					
						rsComune.MoveNext 
					end if
					
				loop

				rsComune.Close	
				%>
				</select>
	        </td>
		</tr>
		<tr><td>&nbsp;</td></tr>
		<tr>
			<td align="left" class="tbltext1" width="30%">
				<b>Sesso*</b>
	        </td>
			<td align="left">
				<select class="textblack" id="cmbSesso" name="cmbSesso">
					<option></option>
					<option value="F" <%if rsDati("SESSO") ="F" then Response.Write " selected" end if%>>FEMMINILE</option>
					<option value="M" <%if rsDati("SESSO") ="M" then Response.Write " selected" end if%>>MASCHILE</option>
					</option>
				</select>
	        </td>
	  	</tr>
	  	<tr>
			<td align="left" class="tbltext1" width="30%">
				<b>Codice Fiscale*</b>
	        </td>
			<td align="left">
				<input style="TEXT-TRANSFORM: uppercase;" size="22" maxlength="16" class="textblack" name="txtCodFisc" value="<%=rsDati("COD_FISC")%>">
	        </td>
	    </tr>
		<tr><td>&nbsp;</td></tr>
	    </table>
	<br>
	<table cellpadding="0" cellspacing="0" width="500" border="0">	
		<tr align="center">
			<td nowrap align="right"><input type="image" name="Conferma" src="<%=Session("progetto")%>/images/conferma.gif"></td value="Conferma">
			<td nowrap align="left"><a HREF="javascript:history.back()"><img SRC="<%=Session("Progetto")%>/images/indietro.gif" border="0"></td>    
		</tr>
	</table>
</form>
	
<%
end sub

sub Fine()
%>
<!--#include virtual = "/include/closeconn.asp"-->
<!--#include virtual = "strutt_coda2.asp"-->
<%
end sub
%>
<!-- ************** ASP Fine *************** -->

<!-- ************** MAIN Inizio ************ -->
<%

dim sDataIsc
dim sSQL
dim CnConn
dim rsAbil

dim nBando
dim nIdProgetto
dim sDescBando
dim sEsitoBando


dim sGraduatoria
dim sGruppo, sCommento

    Inizio()
	ImpPagina()
	Fine()
%>
<!-- ************** MAIN Fine ************ -->
