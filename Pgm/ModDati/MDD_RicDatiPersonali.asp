<!--#include Virtual = "/strutt_testa2.asp"-->
<!--#include Virtual = "/include/SysFunction.asp"-->
<!--#include Virtual = "/include/openconn.asp"-->
<!--#include virtual = "/include/DecCod.asp"-->
<!--#include virtual = "/util/portallib.asp"-->
<%
if ValidateService(session("idutente"),"MDD_RICDATIPERSONALI", CC) <> "true" then 
	response.redirect "/util/error_login.asp"
end if
%>
<script language="javascript">
//include del file per fare i controlli sulla validit� delle date
<!--#include virtual = "/Include/ControlDate.inc"-->

//include del file per fare i controlli sulla numericit� dei campi
<!--#include virtual = "/Include/ControlString.inc"-->
<!--#include virtual = "/Include/ControlNum.inc"-->
<!--#include Virtual = "/Include/ControlCodFisc.inc"-->
<!--#include virtual = "/Include/help.inc"-->

//document.frmInsCriteri.cmbValori1.length  

function ControllaDati(){


document.frmRicerca.txtNome.value=TRIM(document.frmRicerca.txtNome.value)
document.frmRicerca.txtCognome.value=TRIM(document.frmRicerca.txtCognome.value)
document.frmRicerca.txtDataN.value=TRIM(document.frmRicerca.txtDataN.value)
document.frmRicerca.txtCodFisc.value=TRIM(document.frmRicerca.txtCodFisc.value)


if ((document.frmRicerca.txtCognome.value=="")&&
	(document.frmRicerca.txtNome.value=="")&&
	(document.frmRicerca.txtDataN.value=="")&&
	(document.frmRicerca.txtCodFisc.value=="")){
	alert('Non puoi effettuare la ricerca se non compili i campi obbligatori!')
	document.frmRicerca.txtCognome.focus()
	return false
	
}	
	
if (document.frmRicerca.txtCognome.value!=""){
	var testoC
	testoC=isNaN(document.frmRicerca.txtCognome.value)
	if (testoC!=true){
		alert('Il campo Cognome non pu� contenere numeri!')
		return false
	}
}
	
	
if (document.frmRicerca.txtNome.value!=""){
	var testoN
	testoN=isNaN(document.frmRicerca.txtNome.value)
	if (testoN!=true){
		alert('Il campo Nome non pu� contenere numeri!')
		return false
	}
}
	
var sDataNascita1
sDataNascita1=document.frmRicerca.txtDataN.value
if (sDataNascita1 !=""){ 
	if (!ValidateInputDate(sDataNascita1)){
		document.frmRicerca.txtDataN.focus() 
		return false
	}

	if ((document.frmRicerca.txtNome.value=="")||
		(document.frmRicerca.txtCognome.value=="")){
		alert('Per avviare la ricerca devi inserire i dati anagrafici.')
		return false
	}
	
}
	
if ((document.frmRicerca.txtCognome.value!="")||
	(document.frmRicerca.txtNome.value!="")){
	
	if (document.frmRicerca.txtCodFisc.value!=""){
		var apice
		apice="'"
		alert('La ricerca non pu� essere effettuata inserendo contemporaneamente Dati Anagrafici e Codice Fiscale. (Consulta l'+apice+'HELP!)')
		document.frmRicerca.txtCognome.focus()
		return false	
	}

	if ((document.frmRicerca.txtCognome.value!="")&&
		(document.frmRicerca.txtNome.value=="")){
		alert('Devi inserire anche il Nome.')
		document.frmRicerca.txtNome.focus()
		return false
	}

	if ((document.frmRicerca.txtNome.value!="")&&
		(document.frmRicerca.txtCognome.value=="")){
		alert('Devi inserire anche il Cognome.')
		document.frmRicerca.txtCognome.focus()
		return false
	}

	if ((document.frmRicerca.txtNome.value=="")||
			(document.frmRicerca.txtCognome.value=="")){
		alert('Per avviare la ricerca devi inserire i dati anagrafici.')
		return false
	}
}	
		//*******ff
sCognome=ValidateInputStringWithNumber(document.frmRicerca.txtCognome.value)
if  (sCognome==false){
	alert("Cognome formalmente errato.")
	document.frmRicerca.txtCognome.focus() 
	return false
}	
		
 var anyStringC = document.frmRicerca.txtCognome.value;
 for (var i=0; i<=anyStringC.length-1; i++){
		if(((anyStringC.charAt(i) >= "A") && (anyStringC.charAt(i) <= "Z")) || 
			((anyStringC.charAt(i) >= "a") && (anyStringC.charAt(i) <= "z"))||
			(anyStringC.charAt(i) == " ")||(anyStringC.charAt(i) == "'"))
			{
		}else{		
			document.frmRicerca.txtCognome.focus();
			alert("Cognome formalmente errato.");
			return false
		}		
}

var anyStringN = document.frmRicerca.txtNome.value;
for (var i=0; i<=anyStringN.length-1; i++){
	if(((anyStringN.charAt(i) >= "A") && (anyStringN.charAt(i) <= "Z")) || 
		((anyStringN.charAt(i) >= "a") && (anyStringN.charAt(i) <= "z"))||
		(anyStringN.charAt(i) == " ")||(anyStringN.charAt(i) == "'"))
		{
	}else{		
		alert("Nome formalmente errato.")
		document.frmRicerca.txtNome.focus()
		return false
	}		
}
				
var sDataNascita
sDataNascita=document.frmRicerca.txtDataN.value
if (sDataNascita !=""){ 
	if (!ValidateInputDate(sDataNascita)){
		document.frmRicerca.txtDataN.focus() 
		return false
	}
}		
//controllo che l'anno non sia superiore a quello odierno

if(document.frmRicerca.txtDataN.value !=""){
	var intAnnoOdierno
	var sDataOdierna
	var sDataCompleta
	var intAnno

	sDataCompleta=document.frmRicerca.txtDataN.value

	intAnno=sDataCompleta.substr(6,4)

	sDataOdierna = new Date();
	intAnnoOdierno= sDataOdierna.getFullYear();

	if (intAnno >=intAnnoOdierno){
		alert ("La Data di Nascita deve essere precedente alla data odierna!")
		return false
	}
}
	
	
//Controllo la validit� del Codice Fiscale
//CodFisc = document.frmRicerca.txtCodFisc.value.toUpperCase()
//Codice Fiscale deve essere di 16 caratteri
/*if ((document.frmRicerca.txtCodFisc.value.length != 16) &&
	(document.frmRicerca.txtCodFisc.value != ""))
		{
		alert("Formato del Codice Fiscale errato.")
		document.frmRicerca.txtCodFisc.focus() 
		return false
}
*/		
/*if ((!ControllChkCodFisc(CodFisc)) &&
	(document.frmRicerca.txtCodFisc.value != "")
	{
	alert("Codice Fiscale Errato.")
	document.frmRicerca.txtCodFisc.focus()
	return false
}	
*/		
return true
}
</script>
<form name="frmRicerca" method="POST" action="MDD_LisPersonale.asp">
	<table border="0" width="525" cellspacing="0" cellpadding="0" height="70">
	   <tr>
	    <td width="530" background="<%=Session("Progetto")%>/images/titoli/cercolavoro2b.gif" height="70" align="right">
	       <br><br>
	       <table border="0" width="260" height="20" cellspacing="0" cellpadding="0">
			<tr>
	           <td width="100%" valign="baseline" align="center"><span class="tbltext1a"><b>Modifica Codice Fiscale&nbsp;&nbsp;&nbsp;</b></span></td>
	         </tr>
	       </table>
	     </td>
	   </tr>
	</table>
	<br>
	<table cellpadding="0" cellspacing="0" width="500" border="0">
	   <tr height="18">
	   	<td class="sfondomenu" height="18" width="35%">
				<span class="tbltext0"><b>&nbsp;RICERCA</b></span></td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
			<td valign="middle" align="right" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1">(*) campi obbligatori</td>
	   </tr>
	   <tr height="2">
			<td width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif">	</td>     
		</tr>
			
	      <td class="sfondocomm" width="57%" colspan="3" height="18" align="left">
	       <br>
				
			Puoi effettuare una ricerca tra le risorse al momento disponibili. <br>
			Seleziona i parametri di ricerca delle risorse, quindi premi <b>Ricerca</b>. <br>
			Tieni presente che la ricerca deve avvenire specificando obbligatoriamente i campi
			<b>Cognome</b>, <b>Nome</b> o in alternativa solo il campo <b>Codice Fiscale</b>.
			
		
				<a href="Javascript:Show_Help('/Pgm/Help/ModDati/MDD_RicDatiPersonali')" onmouseover="javascript:window.status='' ; return true">
				<img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0">
				</a>	
			</td> 
	   </tr>   
		<tr height="2">
			<!--<td colspan="3" class="sfondocomm" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">-->
			<td width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
			</td>
		</tr>
	</table>
	<br>
	<table border="0" CELLPADDING="1" CELLSPACING="2" width="500">
		
		<tr height="17">
			<td width="200" class="tbltext1">
				<b>Cognome</b>*
			</td>
			<td width="400" class="tbltext1">
				<b><input class="textblacka" type="text" name="txtCognome" style="TEXT-TRANSFORM: uppercase" size="35"></b>
			</td>
		</tr>
		<tr height="17">
			<td width="200" class="tbltext1">
				<b>Nome</b>*
			</td>
			<td width="200" class="tbltext1">
				<b><input class="textblacka" type="text" name="txtNome" style="TEXT-TRANSFORM: uppercase" size="35"></b>
			</td>
		</tr> 
		<tr height="17">
			<td width="200" class="tbltext1">
				<b>Data di Nascita</b> 
				<br>(gg/mm/aaaa)
			</td>
			<td width="400" class="tbltext1">
				<b><input class="textblacka" type="text" name="txtDataN" maxlength="10" size="10">
			</td>
		</tr>
		<tr height="17">
			<td colspan="3">
				&nbsp;
			</td>
		</tr>
		<tr height="2">
			<td width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
		</tr>
		<tr height="2">
			<td width="100%" colspan="3" background></td>
		</tr>
		<tr height="17">
			<td colspan="3">
				&nbsp;
			</td>
		</tr>
		<tr height="17">
			<td width="100" class="tbltext1">
				<b>Codice Fiscale</b>*
			</td>
			<td width="400" class="tbltext1">
				<b><input class="textblacka" type="text" name="txtCodFisc" style="TEXT-TRANSFORM: uppercase" maxlength="16" size="20">
			</td>
		</tr>		
</table>
<br>
<table border="0" cellpadding="0" cellspacing="0" width="510" align="center">
	<tr>
		<td align="middle" colspan="2">
			<input type="image" name="Invia" src="<%=Session("Progetto")%>/images/lente.gif" border="0" value="Conferma" onclick="return ControllaDati()">		
		</td>
   </tr>
	<tr>
		<td>&nbsp;</td>
   </tr>
</table>		
</form>
<!--#include Virtual="/include/closeconn.asp"-->
<!--#include Virtual="/strutt_coda2.asp"-->		

