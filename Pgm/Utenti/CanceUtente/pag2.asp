<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #include Virtual="/strutt_testa2.asp" -->
<!-- #include virtual="/include/openconn.asp" -->
<!-- #include virtual="/util/portallib.asp" -->
<!-- #include virtual="/util/globalsub.asp" -->
<!-- #include virtual="/include/ControlDateVB.asp" -->
<%
If Not ValidateService(Session("IdUtente"),"Gestione Utenti",cc) Then 
	Response.Redirect "/util/error_login.asp"
End If

 If Session("ck_idpersona") = "" Or isnull(Session("ck_idpersona")) Then 
	Session("ck_idpersona") = Request.QueryString("idutente")
 End if 
%>
<script language="Javascript">
<!--#include Virtual = "/Include/help.inc"-->

<%
if Session("Msg_Error") <> "" then
		response.write "alert('"&Session("Msg_Error")&"');"
		Session("Msg_Error")=""
  end if
%>
//-->
</script>
<br>
<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
	<tr height="18">
		<td class="sfondomenu" width="67%" height="18"><span class="tbltext0"><b>&nbsp;AMMINISTRAZIONE PORTALE - Cancellazione Utente</b></span></td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
		<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">(*) campi obbligatori</td>
	</tr>
	<tr>
		<td class="sfondocomm" width="57%" colspan="3">
		<br><p>In questa sezione l'amministratore, dopo aver visualizzato tutti i dati dell'utente prescelto, 
	    premendo il tasto Invia procederÓ alla cancellazione fisica dell'utente.</p>
		</td>
	</tr>
	<tr height="17">
		<td class="sfondocomm" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
	</tr>
</table>
<br>
<%
 If Session("ck_idpersona") <> "" Then
 
	Set rstUtente = Server.CreateObject("ADODB.RECORDSET")
		SQLUtente = "SELECT idutente,cognome,nome,email,login, password,idgruppo,creator,tipo_pers,dt_iscrizione,dt_accesso,progetto,ind_abil FROM utente WHERE idutente=" & Session("ck_idpersona") 
		SQLUtente = UCase(SQLUtente)
'PL-SQL * T-SQL  
SQLUTENTE = TransformPLSQLToTSQL (SQLUTENTE) 
		rstUtente.open SQLUtente,CC,1,3	
		rstUtente.MoveFirst
		
		Session("ck_cognome") = (rstUtente("cognome"))
		Session("ck_nome") = (rstUtente("nome"))
		Session("ck_email") = (rstUtente("email"))
		Session("ck_login") = (rstUtente("login"))
		Session("ck_password") = (rstUtente("password"))
		Session("ck_idgruppo") = (rstUtente("idgruppo"))
		Session("ck_creator") = (rstUtente("creator"))
		Session("ck_tipo_pers") = (rstUtente("tipo_pers"))
		Session("ck_dt_iscrizione") = ConvDateToString(rstUtente("dt_iscrizione"))
		Session("ck_dt_accesso") = ConvDateToString(rstUtente("dt_accesso"))
		Session("ck_progetto") = (rstUtente("progetto"))
		Session("ck_ind_abil") = (rstUtente("ind_abil"))
		    
		rstUtente.close
		Set rstUtente=nothing           
%>
<form method="post" action="pag3.asp" name="utente">
<table border="0" cellspacing="0" cellpadding="0" width="500">
	<tr height="30" class="sfondocomm"> 
		<td width="480" align="left" valign="middle"><b>Dati Utente</b></td>
		<td><a href="Javascript:Show_Help('/Pgm/help/Utenti/CanceUtente/Pag2')" name onmouseover="javascript:status='' ; return true">
		     <img align="left" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
		</td>
	</tr>
</table>
<table border="0" cellpadding="2" cellspacing="2" width="500">
	<tr height="30">
	   <td height="25" class="tbltext1" align="left">&nbsp;<b>Cognome*</b></td>
	   <td><input type="text" name="cognome" class="textblack" value="<%=Session("ck_cognome")%>" size="50" readonly></td>
	</tr>
	<tr>
		<td height="25" class="tbltext1" align="left">&nbsp;<b>Nome*</b></td>
		<td><input type="text" name="nome" size="50" class="textblack" value="<%=Session("ck_nome")%>" readonly></td>
	</tr>
	<tr>
		<td height="25" class="tbltext1" align="left">&nbsp;<b>Email</b></td>
		<td><input type="text" name="email" size="50" class="textblack" value="<%=Session("ck_email")%>" readonly></td>
	</tr>
	<tr>
		<td height="25" class="tbltext1" align="left">&nbsp;<b>Login*</b></td>
		<td><input type="text" name="login" size="50" class="textblack" value="<%=Session("ck_login")%>" readonly></td>
	</tr>
	<tr>
		<td height="25" class="tbltext1" align="left">&nbsp;<b>Password*</b></td>
		<td><input type="text" name="password" size="50" class="textblack" value="<%=Session("ck_password")%>" readonly></td>
	</tr>
	<tr>
		<td height="25" class="tbltext1" align="left">&nbsp;<b>Gruppo*</b></td>
<%
			Set rstGruppo = Server.CreateObject("ADODB.RECORDSET")
				SQLGruppo = "SELECT idgruppo,desgruppo FROM gruppo WHERE idgruppo=" & Session("ck_idgruppo") & " ORDER BY desgruppo" 
				SQLGruppo = UCase(SQLGruppo)
	
'PL-SQL * T-SQL  
SQLGRUPPO = TransformPLSQLToTSQL (SQLGRUPPO) 
				rstGruppo.open SQLGruppo,CC,1,3
				Session("ck_desgruppo")= (rstGruppo.fields(1))  
%>
		<td><input type="text" name="idgruppo" size="50" class="textblack" Value="<%=Session("ck_desgruppo")%>" readonly></td>
	</tr>
	<tr>
		<td height="25" class="tbltext1" align="left">&nbsp;<b>Creator</b></td>
		<td><input type="text" size="22" class="textblack" value="<%=Session("ck_creator")%>" readonly></td>
	</tr>
	<tr>
		<td height="25" class="tbltext1" align="left">&nbsp;<b>Data Iscrizione</b></td>
		<td><input type="text" size="11" class="textblack" value="<%=Session("ck_dt_iscrizione")%>" name="dtiscrizione" readonly></td>
	</tr>
	<tr>
		<td height="25" class="tbltext1" align="left">&nbsp;<b>Data Ultimo Accesso</b></td>
		<td><input type="text" size="11" class="textblack" value="<%=Session("ck_dt_accesso")%>" name="dtaccesso" readonly></td>
	</tr>
	<tr>
		<td height="25" class="tbltext1" align="left">&nbsp;<b>Progetto</b></td>
		<td><input type="text" size="40" class="textblack" value="<%=Session("ck_progetto")%>" name="progetto" readonly></td>
	</tr>
	<tr>
		<td height="25" class="tbltext1" align="left">&nbsp;<b>Abilitazione</b></td>
		<td><input type="text" size="1" class="textblack" value="<%=Session("ck_ind_abil")%>" name="indabil" readonly></td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2" align="center">
			<input type="hidden" name="oldcognome" value>
			<a href="pag1.asp"><img name="oi" border="0" alt="Indietro" src="<%=Session("Progetto")%>/images/indietro.gif"></a>
			<input type="image" alt="Invia" src="<%=Session("Progetto")%>/images/conferma.gif" name="conferma">
		</td>
	</tr>
    <tr>
		<td colspan="2">&nbsp;</td>
	</tr>	
</table>
<%
End if
%> 
<br>
<table border="0" cellpadding="2" cellspacing="1" width="500">
	<tr>
	   <td width="420"></td>	
	   <td height="25" class="sfondocomm" align="center"><b>Step 3/5</b></td>
	</tr>
</table>
</form>
<!-- #include virtual="/include/closeconn.asp" -->
<!-- #include Virtual="/strutt_coda2.asp" -->
