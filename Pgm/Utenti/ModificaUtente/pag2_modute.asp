<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #include virtual="/strutt_testa2.asp" -->
<!-- #include virtual="/include/openconn.asp" -->
<!-- #include virtual="/util/portallib.asp" -->
<!-- #include virtual="/include/ControlDateVB.asp" -->
<!-- #include virtual="/util/globalsub.asp" -->
<%
If Not ValidateService(Session("IdUtente"),"Gestione Utenti",cc) Then 
	Response.Redirect "/util/error_login.asp"
End If

' If Session("ck_idpersona") = "" Or isnull(Session("ck_idpersona")) Then 
	Session("ck_idpersona") = Request.QueryString("idutente")
' End if 
%>
<script language="Javascript">
<!--#include virtual="/include/controlString.inc"-->
<!--#include Virtual = "/Include/help.inc"-->	
	function checkField(frm,app){
	
		var chk="",app1="";
		
	        if (TRIM(frm.cognome.value)==""){
				alert("El campo Apellido  es obligatorio");
				frm.cognome.focus();
				return false;
			}
	
			if (TRIM(frm.nome.value)==""){
				alert("El campo Nombre es obligatorio");
				frm.nome.focus();
				return false;
			}
			
			if (TRIM(frm.email.value) == "") {
		       alert("El campo Email es obligatorio");
		        frm.email.focus();
		         return(false);
			}
			if (!ValidateEmail(frm.email.value)){
		       alert("El campo Email tiene formato erroneo!");
		       frm.email.focus();
		       return false
			}
			
			if (TRIM(frm.password.value)==""){
				alert("El campo Password es obligatorio");
				frm.password.focus();
				return false;
			}
	
			if (TRIM(frm.password.value)==""){
				alert("El campo Password es obligatorio");
				frm.password.focus();
				return false;
			}
	
		   if (frm.password.value.length < 8){
				alert("La nueva password debe ser de al menos 8 caracteres");
				frm.password.focus();
				return false;
			}
			
			if (frm.idgruppo.value==0){
				alert("El campo Grupo  es obligatorio");
				frm.idgruppo.focus();
				return false;
			}
		
			/*if (frm.id.value==0){
				alert("El campo Oficina de Trabajo  es obligatorio");
				frm.idgruppo.focus();
				return false;
			}*/
		
				
			if (frm.idgruppo.value != TRIM(app)){
				if (confirm("Atención! Modificando el grupo al que pertenece el usuario, y sera eliminado de la función actual. Continuar?")){
					return true;
					chk="ok";
				}
				else{
					frm.idgruppo.focus();
					return false;
				}
			}
			
			
			
			if (chk='ok'){ 
				return confirm("Confirma la modificación del Usuario?");
			}
			
	}
</script>	
<%
if Session("Msg_Error") <> "" then
		response.write "alert('"&Session("Msg_Error")&"');"
		Session("Msg_Error")=""
  end if
%>


<br>
<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
	<tr height="18">
		<td class="sfondomenu" width="67%" height="18"><span class="tbltext0"><b>&nbsp;Configuración del Sistema - Modificar Usuario</b></span></td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
		<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">(*) campo obligatorio</td>
	</tr>
	<tr>
		<td class="sfondocomm" width="57%" colspan="3">
		<br><p>
		En esta sección es posible efectuar la modificación de los datos de un usuario.<br>             
		El campo Apellido, Nombre y Email no puede modificarse en el caso de que Creador sea distinto de cero.
		
		<a href="Javascript:Show_Help('/Pgm/help/Utenti/Modificautente/pag2_modute/')" name onmouseover="javascript:status='' ; return true">
		<img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></p>
		</td>
	</tr>
	<tr height="2">
		<td class="sfondocomm" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
	</tr>
</table>
<br>
<%

 If Session("ck_idpersona") <> "" Then
 
	Set rstUtente = Server.CreateObject("ADODB.RECORDSET")
		SQLUtente = "SELECT idutente,cognome,nome,email,login,password,idgruppo,creator,tipo_pers,dt_iscrizione,dt_accesso,progetto,ind_abil,id_uorg FROM utente  WHERE idutente=" & Session("ck_idpersona")
		'Response.Write Session("ck_idpersona")
		
		 
		SQLUtente = UCase(SQLUtente)
'PL-SQL * T-SQL  
SQLUTENTE = TransformPLSQLToTSQL (SQLUTENTE) 
		rstUtente.open SQLUtente,CC,1,3	
		rstUtente.MoveFirst
		
		Session("ck_cognome") = (rstUtente("cognome"))
		Session("ck_nome") = (rstUtente("nome"))
		Session("ck_email") = (rstUtente("email"))
		Session("ck_login") = (rstUtente("login"))
	    Session("ck_password") = (rstUtente("password"))
		Session("ck_idgruppo") = (rstUtente("idgruppo"))
		Session("ck_creator") = (rstUtente("creator"))
		Session("ck_idOficina") = (rstUtente("creator"))
		Session("ck_tipo_pers") = (rstUtente("tipo_pers"))
		Session("ck_sid_uorg") = rstUtente("id_uorg") & "0"
		if Session("ck_sid_uorg") <> 0 then
		   Session("ck_sid_uorg") = rstUtente("id_uorg")
		end if
		Session("ck_dt_iscrizione") = ConvDateToString(rstUtente("dt_iscrizione"))
		If rstUtente("dt_accesso") <> "" Then
		 Session("ck_dt_accesso") = ConvDateToString((rstUtente("dt_accesso")))
		Else
		 Session("ck_dt_accesso") = ""
		End if
		Session("ck_progetto") = (rstUtente("progetto"))
		Session("ck_ind_abil") = (rstUtente("ind_abil"))
		

		if Session("ck_ind_abil") = "S"  then
			Svalue = "selected"
			Nvalue = ""
		else
			Svalue = ""
			Nvalue = "selected"
		end if 
 
 		rstUtente.close
		Set rstUtente=nothing          

		
%>
<form method="post" action="pag2fun_modute.asp" name="utente" onsubmit="return checkField(this,'<%=Session("ck_idgruppo")%>');">
<!--table border="0" cellspacing="0" cellpadding="0" width="500">	<tr height="30" class="sfondocomm"> 		<td width="480" align="left" valign="middle"><b>Modifica Utente</b></td>		<td align="right" valign="middle" colspan="2"><img src="<%=Session("Progetto")%>/images/help.gif" border="0"></td>  	</tr></table-->

<table border="0" cellpadding="2" cellspacing="2" width="500">
	<tr>
		<td></td>
		<td height="25" class="tbltext1" align="Left">&nbsp;<b><u>Datos Personales del Usuario</b></u></td>
	</tr>
	<tr height="30">
	   <td height="25" class="tbltext1" align="left">&nbsp;<b>Apellido*</b></td>
	   <td>
	 
<% If CLng(Session("ck_creator")) = 0 Then %>
		<input type="text" name="cognome" class="textblack" value="<%=Session("ck_cognome")%>" size="50">
<% Else %>
		<input type="text" name="cognome" class="textblack" value="<%=Session("ck_cognome")%>" size="50" readonly>
<% End If %>
		</td>
	</tr>
	<tr>
		<td height="25" class="tbltext1" align="left">&nbsp;<b>Nombre*</b></td>
		<td>
<% If  CLng(Session("ck_creator")) = 0 Then %>
		<input type="text" name="nome" size="50" class="textblack" value="<%=Session("ck_nome")%>">
<% Else %>
		<input type="text" name="nome" size="50" class="textblack" value="<%=Session("ck_nome")%>" readonly>
<% End If %>
		</td>
	</tr>
	<tr>
		<td height="25" class="tbltext1" align="left">&nbsp;<b>Email*</b></td>
		<td>
<% If  CLng(Session("ck_creator")) = 0 Then %>		
		<input type="text" name="email" size="50" class="textblack" value="<%=Session("ck_email")%>">
<% Else %>
		<input type="text" name="email" size="50" class="textblack" value="<%=Session("ck_email")%>" readonly >
<% End If %>		
		</td>
	</tr>
	
	<tr>
		<td height="25" class="tbltext1" align="left">&nbsp;<b>Usuario</b></td>
		<td><input type="text" name="login" size="50" class="textgray" value="<%=Session("ck_login")%>" readonly></td>
	</tr>
	<tr>
		<td height="25" class="tbltext1" align="left">&nbsp;<b>Contraseña*</b></td>
		<td>
			<input type="Hidden" name="vecchiaPWD" size="50" value="<%=Session("ck_password")%>">
			<input type="password" name="password" size="50"  value="<%=Session("ck_password")%>">
		</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td></td>
		<td height="25" class="tbltext1" align="Left">&nbsp;<b><u>Datos para el Portal</u></b></td>
	</tr>
	
	<!--tr>
		<td height="25" class="tbltext1" align="left">&nbsp;<b>Oficina de Trabajo*</b></td>
		<td>
			<select name="idOficina" class="textblack">
			<option value="0"> </option>
<%
				Set rstOficina = Server.CreateObject("ADODB.RECORDSET")
				SQLOficina = "Select Sede_IMPRESA.Id_Sede, Sede_IMPRESA.DESCRIZIONE, TADES.DESCRIZIONE as PRV From IMPRESA,Sede_IMPRESA,TADES Where IMPRESA.ID_IMPRESA = Sede_IMPRESA.ID_IMPRESA and TADES.CODICE = Sede_IMPRESA.PRV and COD_TIMPR = '03' and TADES.NOME_TABELLA = 'PROV' and estado = 1 Order By TADES.DESCRIZIONE,Sede_IMPRESA.DESCRIZIONE" 
    			SQLOficina = UCase(SQLOficina)
'PL-SQL * T-SQL  

SQLOFICINA = TransformPLSQLToTSQL (SQLOFICINA) 
				rstOficina.open SQLOficina,CC,1,3
				
				While rstOficina.EOF <> True
				  Leng = 0  
				  Response.Write "<option value="& rstOficina("Id_Sede")
				  If cstr(Session("ck_idOficina")) =  cstr(rstOficina("Id_Sede")) Then
					 Response.Write " selected"
				  End If
'				  Leng = len(rstOficina("DESCRIZIONE")) - 14
'				  Response.Write ">" & rstOficina("PRV") & "-" & right(rstOficina("DESCRIZIONE"),Leng) & "</option>"

				  Response.Write ">" & rstOficina("PRV") & "-" & rstOficina("DESCRIZIONE") & "</option>"

				  rstOficina.MoveNext
				Wend  
				
				'set rstOficina=nothing		
				
				set rstOficina=nothing			
		
%>
			</select>
		</td>
	</tr-->
	<tr>
		<td height="25" class="tbltext1" align="left">&nbsp;<b>Grupo*</b></td>
		<td>
			<select name="idgruppo" class="textblack">
			<option value="0"> </option>
<%
			Set rstGruppo = Server.CreateObject("ADODB.RECORDSET")
				SQLGruppo = "SELECT idgruppo,desgruppo FROM gruppo ORDER BY desgruppo" 
				SQLGruppo = UCase(SQLGruppo)
'PL-SQL * T-SQL  
SQLGRUPPO = TransformPLSQLToTSQL (SQLGRUPPO) 
				rstGruppo.open SQLGruppo,CC,1,3
				
				While rstGruppo.EOF <> True
				  Response.Write "<option value="& rstGruppo("idgruppo")
				  If cstr(Session("ck_idgruppo")) =  cstr(rstGruppo("idgruppo")) Then
					 Response.Write " selected"
				  End If
				  Response.Write ">" & rstGruppo("desgruppo") & "</option>"
				  rstGruppo.MoveNext
				Wend  
				
							
		
%>
			</select>
		</td>
	</tr>
	<tr>
		<td height="25" class="tbltext1" align="left">&nbsp;<b>Creador</b></td>
		<td><input type="text" size="22" class="textgray" value="<%=Session("ck_creator")%>" name="creator" readonly></td>
	</tr>
	<tr>
		<td height="25" class="tbltext1" align="left">&nbsp;<b>Fecha Inscripción</b></td>
		<td><input type="text" size="11" class="textgray" value="<%=Session("ck_dt_iscrizione")%>" name="dtiscrizione" readonly></td>
	</tr>
<% If Session("ck_dt_accesso") <> "" Then %>
	<tr>
		<td height="25" class="tbltext1" align="left">&nbsp;<b>Fecha Ultimo Acceso</b></td>
		<td><input type="text" size="11" class="textgray" value="<%=Session("ck_dt_accesso")%>" name="dtaccesso" readonly></td>
	</tr>
<% End If %>
	<tr>
		<td height="25" class="tbltext1" align="left">&nbsp;<b>Proyecto</b></td>
		<td><input type="text" size="40" class="textgray" value="<%=Session("ck_progetto")%>" name="progetto" readonly></td>
	</tr>
	<tr>
		<td height="25" class="tbltext1" align="left">&nbsp;<b>Habilitación</b></td>
		<td>
			<select name="indabil" class="textblack">
				<option value="S" <%=Svalue%>>S</option>
				<option value="N" <%=Nvalue%>>N</option>
			</select>
		</td>	
	</tr>
	
		<tr>
	    <td height="25" class="tbltext1">&nbsp;<b>Unidad Funcional</b></td>
	   
	   	<td><select name="cmbUorg" class="textblack">
			<option value="0"> </option>
<%
            SQLUorg = "SELECT ID_UORG,DESC_UORG FROM UNITA_ORGANIZZATIVA ORDER BY DESC_UORG"
'PL-SQL * T-SQL  
SQLUORG = TransformPLSQLToTSQL (SQLUORG) 
			Set rstUorg1= CC.execute(SQLUorg)
             
				While rstUorg1.EOF <> True
				  Response.Write "<option value="& rstUorg1("id_uorg")
				  
				  If cstr(Session("ck_sid_uorg")) =  cstr(rstUorg1("id_uorg")) Then
					 Response.Write " selected"
				  End If
				  Response.Write ">" & rstUorg1("desc_uorg") & "</option>"
				  rstUorg1.MoveNext
				Wend  
%>  
		
	</select>
<%				
	set rstUorg1=nothing
				
%>
			
		</td>
	</tr>
		<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2" align="center">
			<input type="hidden" name="oldcognome" value>		
			<a href="javascript:history.go(-1)"><img name="ti" border="0" alt="Indietro" src="<%=Session("Progetto")%>/images/indietro.gif"></a>
			<input type="image" alt="Invia" src="<%=Session("Progetto")%>/images/conferma.gif" name="conferma">
		</td>
	</tr>
    <tr>
		<td colspan="2">&nbsp;</td>
	</tr>	
</table>

<%
End if
%> 
<!--
<br>
<table border="0" cellpadding="2" cellspacing="1" width="500">
	<tr>
	   <td width="420"></td>	
	  
	</tr>
</table>
-->
</form>
<!-- #include virtual="/include/CloseConn.asp" -->
<!-- #include virtual="/strutt_coda2.asp" -->
