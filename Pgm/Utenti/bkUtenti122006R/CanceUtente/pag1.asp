<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #include Virtual="/strutt_testa2.asp" -->
<!-- #include virtual="/include/openconn.asp" -->
<!-- #include virtual="/util/portallib.asp" -->
<!-- #include virtual="/util/globalsub.asp" -->
<%
If Not ValidateService(Session("IdUtente"),"Gestione Utenti",cc) Then 
	Response.Redirect "/util/error_login.asp"
End If

	Session("ck_idpersona")=""
	Session("ck_cognome")=""
	Session("ck_nome")=""
	Session("ck_email")=""
	Session("ck_login")=""
	Session("ck_password")=""
	Session("ck_idgruppo")=""
	Session("ck_creator")=""	
%>
<script>
<!--#include Virtual = "/Include/help.inc"-->


<%
	if Session("Msg_Error") <> "" then
		response.write "alert('" & Session("Msg_Error") & "');"
		Session("Msg_Error")=""
	end if
%>
function checkField(Form) {

	if (Form.SearchText.value == "") {
		alert("E' necessario inserire almeno un parametro di ricerca");
		Form.SearchText.focus();
		return(false);
		}
		
}		
</script>
<body onload="document.utente.SearchText.focus()">
<br>
<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
	<tr height="18">
		<td class="sfondomenu" width="67%" height="18"><span class="tbltext0"><b>&nbsp;Configuraci�n del Sistema - Eliminar Usuario</b></span></td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
		<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
	</tr>
	<tr>
		<td class="sfondocomm" width="57%" colspan="3">
		<p>En esta secci�n el administrador, ingresando un par�metro de busqueda en el campo de texto, podr� visualizar el nombre de uno m�s usuario.</p>
		Posteriormente, individualizando el usuario que quiere eliminarse podran verse sus datos presionando sobre el campo apellido.
		</td>
	</tr>
	<tr height="17">
		<td class="sfondocomm" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
	</tr>
</table>
<br>
<br>
<form method="GET" name="utente" target="_self" onSubmit="return checkField(this);">
<table border="0" width="500" class="sfondocomm">
	<tr height="30">
		<td width="480" align="left" valign="middle">&nbsp;<b>Busqueda del usuario para eliminar</b></td>
		<td><a href="Javascript:Show_Help('/Pgm/help/Utenti/CanceUtente/Pag1')" name onmouseover="javascript:status='' ; return true">
		     <img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a></td>
		      <!--td align="right" valign="middle"><img src="<%=Session("Progetto")%>/images/help.gif" border="0"></td-->  
	</tr>   
</table>
<br>
<table border="0" cellpadding="0" cellspacing="1" width="500">
	<tr>
	    <td colspan="4" class="tbltext1">Busqueda del campo: apellido</td>
	</tr>
	<tr>
		<td height="25" class="tbltext1" align="left">&nbsp;Parametro de Busqueda</td>
		<td>
			<input type="text" class="textblack" name="SearchText" value="<%=Request.QueryString("Searchtext")%>" size="30">
		</td>
		<td>
			<input type="image" alt="Invia" border="0" name="Invia" src="<%=Session("Progetto")%>/images/conferma.gif">
		</td>
	</tr>
</table>   
</form>  
<%
Dim iActPagina
Dim iTotPagina
Dim iTamPagina		
Dim iTotRecord

iTamPagina=20

If Request.Querystring("pagina")="" Then
	iActPagina = 1
Else
	iActPagina = Clng(Request.Querystring("pagina"))
End If

If Request.QueryString("Searchtext") <> "" Then
	If CheckIsNumber(Request.QueryString("Searchtext")) = True Then
		  TmpNum = Replace(Trim(Request.QueryString("Searchtext")), ",", ".")
	Else
		  TmpNum = "0"
	End If
 
	Set rstUtente = Server.CreateObject("ADODB.RECORDSET")
	    SQLUtente = "SELECT a.idutente, a.cognome, a.nome, a.login, a.email, a.password, a.idgruppo, b.desgruppo, a.creator " & _
	   				"FROM utente a, gruppo b " & _
	   				"WHERE a.idgruppo = b.idgruppo AND (" & _
	   				"(cognome LIKE '" & FixDbFieldSave(Request.QueryString("Searchtext")) & "%') OR " & _
	   				"(a.idgruppo=" & TmpNum & ")) ORDER BY a.cognome,a.nome"
	    SQLUtente = UCase(SQLUtente)
'PL-SQL * T-SQL  
SQLUTENTE = TransformPLSQLToTSQL (SQLUTENTE) 
	    rstUtente.open SQLUtente,CC,1,3
	    
	    rstUtente.PageSize=iTamPagina
	    rstUtente.CacheSize=iTamPagina
		 
	iTotPagina=rstUtente.PageCount
 
	If iActPagina < 1 Then
		iActPagina = 1
	End If

	If iActPagina > iTotPagina Then
		iActPagina = iTotPagina
	End If

	If rstUtente.EOF <> True Then
		rstUtente.AbsolutePage=iActPagina
	End If
	iTotRecord=0

	If (rstUtente.EOF=True) Then
 %>
	<table border="0" cellspacing="2" cellpadding="1" width="500" align="center">
		<tr align="center">
			<td class="tbltext3">El valor buscado no existe!</td>
		</tr>
 <% Else %>  
	<table border="0" cellpadding="2" cellspacing="1" width="500">
		<tr class="sfondocomm"> 
			<td align="left" height="26" valign="middle"><b>Apellido</b></td>
			<td align="left" valign="middle"><b>Nombre</b></td>
			<td align="left" valign="middle"><b>Usuario</b></td>
		</tr>
<% End If  
	While (rstUtente.EOF <> True)  And (iTotRecord < iTamPagina) 
	 Response.write "<tr class=tblsfondo>"+CrLf
	 Response.write "<td class=tbltext1>"
	 Response.write "<A href = 'pag2.asp?idutente="+CStr(rstUtente("idutente"))+"'>"
	 Response.write "<font color='#ff0000'>"+FixDbField(rstUtente("cognome"))
	 Response.write "</a>"
	 Response.write "</td>"
	 Response.write "<td class=tbltext1>"
	 Response.write lcase(FixDbField(rstUtente("nome")))
	 Response.write "</td>"
	 Response.write "<td class=tbltext1>"+CrLf
	 Response.write lcase(FixDbField(rstUtente("login")))+CrLf
	 Response.write "</td></tr>"
	 iTotRecord=iTotRecord + 1  
	 rstUtente.movenext
	Wend
	rstUtente.Close
	Set rstUtente = Nothing
	 Response.write "</TABLE><br>"
End if
Response.Write "<TABLE border=0 width=500>"
Response.Write "<tr>"
If iActPagina > 1 Then
	Response.Write("<td align=right width=480><a href=pag1.asp?SearchText=" & Request.querystring("Searchtext") & "&pagina=" & iActPagina-1 & _
	"><img border=0 src=" & Session("Progetto") & "/images/precedente.gif></a></td>")
End If
If iActPagina > 1 And iActPagina < iTotPagina Then
	Response.Write " ------ "
End If
If iActPagina < iTotPagina Then
	Response.Write("<td align=right><a href=pag1.asp?SearchText=" & Request.querystring("Searchtext") & "&pagina=" & iactPagina+1 & _
	"><img border=0 src=" & Session("Progetto") & "/images/successivo.gif></a></td>")
End If
Response.Write "</tr></TABLE><br>"
%>
<table border="0" cellpadding="2" cellspacing="1" width="500">
	<tr>
	   <td width="420"></td>	
	   <% If Request.QueryString("Searchtext") <> "" Then %>
	   <td height="25" class="sfondocomm" align="center"><b>Paso 2/5</b></td>
	   <% Else %>
	   <td height="25" class="sfondocomm" align="center"><b>Paso 1/5</b></td>
	   <% End If %>
	</tr>
</table>
<!-- #include virtual="/include/closeconn.asp" -->
<!-- #include Virtual="/strutt_coda2.asp" -->
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   


