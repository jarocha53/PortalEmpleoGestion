<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #include Virtual="/strutt_testa2.asp" -->
<!-- #include virtual="/util/portallib.asp" -->
<!-- #include virtual="/include/openconn.asp" -->
<!-- #include virtual="/util/globalsub.asp" -->
<%
If Not ValidateService(Session("IdUtente"),"Gestione Utenti",cc) Then 
	Response.Redirect "/util/error_login.asp"
End If

	Session("ck_idpersona")=""
	Session("ck_cognome")=""
	Session("ck_nome")=""
	Session("ck_email")=""
	Session("ck_login")=""
	Session("ck_password")=""
	Session("ck_idgruppo")=""
	Session("ck_creator")=""	
%>
<script>
<!--#include Virtual = "/Include/help.inc"-->
<%
	if Session("Msg_Error") <> "" then
		response.write "alert('" & Session("Msg_Error") & "');"
		Session("Msg_Error")=""
	end if
%>
function checkField(Form) {

	if (Form.SearchText.value == "") {
		alert("E' necessario inserire almeno un parametro di ricerca");
		Form.SearchText.focus();
		return(false);
		}
		
}		
</script>
<body onload="document.utente.SearchText.focus()">
<br>
<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
	<tr height="18">
		<td class="sfondomenu" width="67%" height="18"><span class="tbltext0"><b>&nbsp;Configuración del Sistema - Modificación Usuario</b></span></td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
		<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
		
	</tr>
	<tr>
		<td class="sfondocomm" width="57%" colspan="3">
		<br>
		<p>En esta sección el administrador, ingresando un parametro de busqueda en el campo de texto, podra consultar los nombres de uno o mas usuarios.</p>
		Posteriormente, una vez individualizado el usuario que desea modificar, presionando sobre su nombre puede efectuar la modificación.
		  <a href="Javascript:Show_Help('/Pgm/help/Utenti/Modificautente/pag1_modute/')" name onmouseover="javascript:status='' ; return true">
		<img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0">
		</td>
	</tr>
	<tr height="2">
		<td class="sfondocomm" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
	</tr>
</table>
<br>
<br>
<form method="GET" name="utente" target="_self" onSubmit="return checkField(this);">
<br>
<table border="0" cellpadding="0" cellspacing="1" width="500">
	<tr>
		<td height="25" class="tbltext1" align="left" width="150">&nbsp;<b>Apellido para la busqueda</b></td>
		<td width="250">
			<input type="text" style="TEXT-TRANSFORM: uppercase;" class="textblack" name="SearchText" value="<%=ucase(Request.QueryString("Searchtext"))%>" size="40">
		</td>
		<td width="100">
			<input type="image" alt="Invia" border="0" name="Invia" src="<%=Session("Progetto")%>/images/lente.gif">
		</td>
	</tr>
</table>   
</form>  
<%
Dim nActPagina
Dim nTotPagina
Dim nTamPagina		
Dim nTotRecord

nTamPagina=20

If Request.Querystring("pagina")="" Then
	nActPagina = 1
Else
	nActPagina = Clng(Request.Querystring("pagina"))
End If

If Request.QueryString("Searchtext") <> "" Then
	If CheckIsNumber(Request.QueryString("Searchtext")) = True Then
		  TmpNum = Replace(Trim(Request.QueryString("Searchtext")), ",", ".")
	Else
		  TmpNum = "0"
	End If
 
	Set rstUtente = Server.CreateObject("ADODB.RECORDSET")
	    SQLUtente = "SELECT a.idutente, a.cognome, a.nome, a.login, a.email, a.password, a.idgruppo, b.desgruppo, a.creator " & _
	   				"FROM utente a, gruppo b " & _
	   				"WHERE a.idgruppo = b.idgruppo AND (" & _
	   				"(cognome LIKE '" & FixDbFieldSave(Request.QueryString("Searchtext")) & "%') OR " & _
	   				"(a.idgruppo=" & TmpNum & ")) ORDER BY a.cognome,a.nome"
	    SQLUtente = UCase(SQLUtente)
'PL-SQL * T-SQL  
SQLUTENTE = TransformPLSQLToTSQL (SQLUTENTE) 
	    rstUtente.open SQLUtente,CC,1,3
	    
	    rstUtente.PageSize = nTamPagina
	    rstUtente.CacheSize = nTamPagina
		 
	nTotPagina = rstUtente.PageCount

	If nActPagina < 1 Then
		nActPagina = 1
	End If

	If nActPagina > nTotPagina Then
		nActPagina = nTotPagina
	End If

	If rstUtente.EOF <> True Then
		rstUtente.AbsolutePage = nActPagina
	End If
	nTotRecord=0

	If (rstUtente.EOF=True) Then
 %>
	<table border="0" cellspacing="2" cellpadding="1" width="500" align="center">
		<tr align="center">
			<td class="tbltext3">El valor buscado no existe!</td>
		</tr>
 <% Else %>  
	<table border="0" cellpadding="2" cellspacing="1" width="500">
		<tr class="sfondocomm"> 
			<td align="left" height="26" valign="middle"><b>Apellido</b></td>
			<td align="left" valign="middle"><b>Nombre</b></td>
			<td align="left" valign="middle"><b>Usuario</b></td>
			<td align="left" valign="middle"><b>Email</b></td>
			<td align="left" valign="middle"><b>Grupo</b></td>
		</tr>
<% End If 

	While (rstUtente.EOF <> True)  And (nTotRecord < nTamPagina) 
	 
	 Response.write "<tr class=tblsfondo>"
	 Response.write "<td class=tbltext1>"
	 Response.write "<A class=tblagg href='pag2_modute.asp?idutente=" & CStr(rstUtente("idutente")) & "'>"
	 Response.write FixDbField(rstUtente("cognome"))
	 Response.write "</a></td>"
	 Response.write "<td class=tbldett>" & LCase(FixDbField(rstUtente("nome")))& "</td>"
	 Response.write "<td class=tbldett>" & LCase(FixDbField(rstUtente("login"))) & "</td>"
	 Response.write "<td class=tbldett>" & LCase(FixDbField(rstUtente("email"))) & "</td>"
	 Response.write "<td class=tbldett>" & LCase(FixDbField(rstUtente("desgruppo"))) & "</td></tr>"
	 
	 nTotRecord=nTotRecord + 1  
	 rstUtente.movenext
	Wend
	rstUtente.Close
	Set rstUtente = Nothing
	 Response.write "</TABLE><br>"
End if
Response.Write "<TABLE border=0 width=500>"
Response.Write "<tr>"

If nActPagina > 1 Then
	Response.Write("<td align=right width=480><a href=pag1_modute.asp?SearchText=" & Request.querystring("Searchtext") & "&pagina=" & nActPagina-1 & _
	"><img border=0 alt='Pagina precedente' src=" & Session("Progetto") & "/images/precedente.gif></a></td>")
End If
If nActPagina < nTotPagina Then
	Response.Write("<td align=right><a href=pag1_modute.asp?SearchText=" & Request.querystring("Searchtext") & "&pagina=" & nActPagina+1 & _
	"><img border=0 alt='Pagina successiva' src=" & Session("Progetto") & "/images/successivo.gif></a></td>")
End If
Response.Write "</tr></TABLE><br>"
%>
<table border="0" cellpadding="2" cellspacing="1" width="500">
<% If Request.QueryString("Searchtext") <> ""  Then%>

	<tr>
		<td colspan="2" align="center">
			<input type="hidden" name="oldcognome" value>
			<a href="pag1_modute.asp"><img name="oi" border="0" alt="Indietro" src="<%=Session("Progetto")%>/images/indietro.gif"></a>
		</td>
	</tr>
	<tr>
<%end if	 %>
	   <td width="420"></td>
	   <% If Request.QueryString("Searchtext") <> "" Then %>
	   <td height="25" class="sfondocomm" align="center"><b>Paso 2/3</b></td>
	   <% Else %>
	   <td height="25" class="sfondocomm" align="center"><b>Paso 1/3</b></td>
	   <% End If %>
	</tr>
</table>
<!-- #include virtual="/include/closeconn.asp" -->
<!-- #include Virtual="/strutt_coda2.asp" -->
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   


