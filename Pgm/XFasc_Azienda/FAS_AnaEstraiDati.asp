<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%

'Response.Write "ute=" & nAnaSede & "<br>"
'Response.Write "imp=" & nAnaIdImpresa & "<br>"
bDbAllineato= True
sAnasql="SELECT Descrizione,Indirizzo,Comune,Prv, Cap, Num_Tel_sede, Fax_sede, E_mail_sede, Ref_Sede " &_
	" FROM Sede_impresa WHERE ID_Sede=" & clng(nAnaSede)

	
'PL-SQL * T-SQL  
SANASQL = TransformPLSQLToTSQL (SANASQL) 
set RRAna = CC.Execute(sAnasql)

if not RRAna.eof  then
 	sAnaDescr		= RRAna("Descrizione")
 	sAnaIndirizzo	= RRAna("Indirizzo")
 	sAnaComune		= DescrComune(RRAna("Comune"))
 	sAnaProv		= DecCodVal("PROV", 0, Date(), RRAna("Prv"), 1)
 	sAnaComune		= sAnaComune & " (" & RRAna("Prv") & ")"
  	nAnaCap			= RRAna("Cap")
  	nAnaTel			= RRAna("Num_Tel_Sede")
  	nAnaFax			= RRAna("Fax_sede")
  	sAnaEmail		= RRAna("E_mail_sede")
  	sAnaRef			= RRAna("Ref_Sede")
	 	
 	if Trim(sAnaComune)	= "" or isnull(sAnaComune)then
 		sAnaComune	= " - "
 	end if
 	if Trim(sAnaIndirizzo)= "" or isnull(sAnaIndirizzo) then
 		sAnaIndirizzo = " - "
 	end if
 	if Trim(sAnaProv) = "" or isnull(sAnaProv) then
 		sAnaProv = " - "
 	end if
 	if nAnaCap = "" or isnull(nAnaCap) or not isnumeric(nAnaCap) then
 		nAnaCap = " - "
 	end if
 	if nAnaTel = "" or isnull(nAnaTel) or not isnumeric(nAnaTel)then
 		nAnaTel = " - "
 	end if
 	if nAnaFax = "" or isnull(nAnaFax) or not isnumeric(nAnaFax)then
 		nAnaFax = " - "
 	end if
 	if Trim(sAnaEmail) = "" or isnull(sAnaEmail) then
 		sAnaEmail = " - "
 	end if
 	if Trim(sAnaRef) = "" or isnull(sAnaRef) then
 		sAnaRef = " - "
 	end if
	 
else
	bDbAllineato = false 
end if 

RRAna.Close()
	
if bDbAllineato then
	sAnasql = " SELECT I.Rag_Soc, I.Cod_Forma, S.Denominazione As Settore, I.Dt_Cost, I.Cap_Soc, I.Cod_Valuta, "  &_
			" I.Desc_Att As Attivita, I.Num_Isc_Reg, I.Num_Inps, I.Part_Iva, I.Cod_Fisc, I.Dt_Cess_Attivita, " &_
			" I.Cod_Cess_Attivita, I.SitoWeb, I.Cod_Timpr" &_
			" FROM Impresa I, Settori S " &_
			" WHERE I.Id_settore = S.Id_Settore And Id_Impresa = " & clng(nAnaIdImpresa)

'PL-SQL * T-SQL  
SANASQL = TransformPLSQLToTSQL (SANASQL) 
	set RRAna = CC.Execute(sAnasql)

	if not RRAna.Eof then
		sAnaSettore		= RRAna.Fields("Settore")
		sAnaAttivita	= RRAna.Fields("Attivita")
		sAnaSitoWeb		= RRAna.Fields("SitoWeb")
		nAnaPartIva		= RRAna.Fields("Part_Iva")
		nAnaIscReg		= RRAna.Fields("Num_Isc_Reg")
		sAnaCodFisc		= RRAna.Fields("Cod_Fisc")
		nAnaInps		= RRAna.Fields("Num_Inps")
		nAnaCapSoc		= RRAna.Fields("Cap_Soc")
		sAnaDtCess		= ConvDateToString(RRAna.Fields("DT_Cess_Attivita"))
		sAnaDtCost		= ConvDateToString(RRAna.Fields("Dt_Cost"))
	 	sAnaCessAtt		= RRAna.Fields("Cod_Cess_Attivita")
	 	sAnaForma		= RRAna.Fields("Cod_Forma")
	 	sAnaValuta		= RRAna.Fields("Cod_Valuta")
	 	sAnaTimpr		= RRAna.Fields("Cod_Timpr")

		if Trim(sAnaSitoWeb)	= "" or isnull(sAnaSitoWeb) then
	 		sAnaSitoWeb	= " - "
	 	end if
		if Trim(sAnaCodFisc)	= "" or isnull(sAnaCodFisc) then
	 		sAnaCodFisc= " - "
	 	end if
		if nAnaIscReg	= "" or isnull(nAnaIscReg) or not isnumeric(nAnaIscReg) then
	 		nAnaIscReg = " - "
	 	end if
		if nAnaInps	= "" or isnull(nAnaInps) or not isnumeric(nAnaInps) then
	 		nAnaInps	= " - "
	 	end if
		if nAnaCapSoc	= "" or isnull(nAnaCapSoc) or not isnumeric(nAnaCapSoc) then
	 		nAnaCapSoc = " - "
	 	end if	
		if not isDate(sAnaDtCess) then
	 		sAnaDtCess = " - "
	 	end if
		if not isDate(sAnaDtCost) then
	 		sAnaDtCost	= " - "
	 	end if
	 	if Trim(sAnaCessAtt) = "" or isnull(sAnaCessAtt) then
	 		sAnaCessAtt= " - "
	 	else
			sAnaCessAtt= DecCodVal("AZVAR", "0", Date(), RRAna.Fields("Cod_Cess_Attivita"), 1)
	 	end if
	 	if Trim(sAnaForma)	= "" or isnull(sAnaForma) then
	 		sAnaForma	= " - "
	 	else
			sAnaForma	= DecCodVal("FGIUR", "0", Date(), RRAna.Fields("Cod_Forma"), 1)
	 	end if
	 	if Trim(sAnaValuta)	= "" or isnull(sAnaValuta) then
	 		sAnaValuta = "  "
	 	else
			sAnaValuta = DecCodVal("VALUT", "0", Date(), RRAna.Fields("Cod_Valuta"), 1)
	 	end if
		if Trim(sAnaTimpr)	= "" or isnull(sAnaTimpr) then
	 		sAnaTimpr	= " - "
	 	else
			sAnaTimpr	= DecCodVal("TIMPR", "0", Date(), RRAna.Fields("Cod_Timpr"), 1)
	 	end if

	' La ragione sociale dell'azienda viene indicata nella forma "Ragione sociale Impresa" + "Forma giuridica" + "Sede"
  		sAnaRagSoc		= RRAna.Fields("Rag_Soc") & " " & sAnaForma & " (" & Ucase(sAnaDescr) & ")"		
	else
		bDbAllineato = False
	end if
	RRAna.Close()
End if

set RRAna = nothing  

%>

