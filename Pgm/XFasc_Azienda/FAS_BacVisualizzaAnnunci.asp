<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<% 
		Dim nIdRic, sIdSede, RR, sql
		Dim sTipoArea, sTipoProf, sDtAl
		Dim sql1, RR1		'recordset delle persone che soddisfano i parametri
		Dim bCImpiego, bAInterin, bBlank
		
		sIdsede	 = Request ("Rif")
		bCImpiego	= false
		bAInterin	= false
		
		if Session("Creator") = "" then
			Messaggio("P�gina momentaneamente no disponible")
		else 
'Prima di eseguire la query sulle tabelle delle richieste verifico se chi accede all'applicazione
' � un centro per l'impiego o un'agenzia interinale

		set RR = Server.CreateObject("ADODB.Recordset")		
		SQL= "SELECT I.COD_TIMPR TipoImpresa FROM SEDE_IMPRESA SI, IMPRESA I WHERE I.ID_IMPRESA = SI.ID_IMPRESA AND SI.ID_SEDE=" & Session("Creator") & " AND I.COD_TIMPR IN('01','02','03')" 

'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
		RR.Open SQL,CC,3
		
		if not RR.EOF then
			select case  RR.Fields("TipoImpresa")
				case "03"
					bCImpiego = true
				case else
					bAInterin = true
			end select
		end if
		
		RR.Close

		sql = "Select ASe.ID_ANNUNCIO, ASe.ID_SETTORE, ASe.ID_AREAPROF, " &_
				"ASe.ID_FIGPROF, ASe.COD_REGIONE, ASe.PRV, ASe.STAT_LAV, ASe.DT_FIN_VAL, " &_
				"S.DENOMINAZIONE AS Settore, AP.DENOMINAZIONE AS Area, " &_  
				"FP.DENOMINAZIONE AS figura " &_
				"FROM ANNUNCIO_SEDE ASe, SETTORI S, AREE_PROFESSIONALI AP, FIGUREPROFESSIONALI FP " &_
				"WHERE ASe.ID_SETTORE = S.ID_SETTORE AND " &_ 
				"AP.ID_AREAPROF = ASe.ID_AREAPROF AND " &_
				"FP.ID_FIGPROF = ASe.ID_FIGPROF AND " &_ 
				"ASe.ID_Sede =" & sIdSede & " AND " &_
				"ASe.FL_PUBBLICATO = 'S' " &_
				"Order by ASe.DT_FIN_VAL"

		'Response.Write sql
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
		RR.Open sql,CC,3
	
		If RR.eof then
			Messaggio("No se encontraron avisos publicados por esta empresa.")
			RR.close
			set RR = nothing
			Exit Sub
		end if
		
		do until RR.EOF
			' Dati dell'annuncio		
			
			sTipoProf		= RR("figura")
			sTipoArea		= RR("Area")
			sDtAl			= RR("dt_fin_val")
			
			sRegioneDesc	= DecCodVal("REGIO", "0", "", RR("COD_REGIONE"), 1)
			sRegioneDesc	= lCase(sRegioneDesc)			
			
			sProvDesc		= DecCodVal("PROV", "0", "", RR("PRV"), 1)
			sProvDesc		= lCase(sProvDesc)
			
			sStatoDesc		= DecCodVal("STATO", "0", "", RR("STAT_LAV"), 1)
			sStatoDesc		= lCase(sStatoDesc)

			sSettoreDesc	= RR("Settore")
			sSettoreDesc	= lCase(sSettoreDesc)

			nIdArea			= RR("ID_AREAPROF")		
			nIdProfilo		= RR("ID_FIGPROF")
			nIdAnnuncio		= RR("ID_ANNUNCIO")	
%>	
			<br>

			<table WIDTH="96%" BORDER="0" CELLPADDING="1" CELLSPACING="1" align="center">
			<tr>
				<td width="2%">
					&nbsp;
				</td>
		        <td class="tbltext1" width="35%">
					<b>Sector de referencia</b>
				</td>
		        <td class="textblack">
					<b><%=sSettoreDesc%></b>
				</td>
			</tr>
			<tr>
				<td>
					&nbsp;
				</td>
		        <td class="tbltext1">
					<b>Area profesional</b>
				</td>
		        <td class="textblack"> 
					<b><%=sTipoArea%></b>
				</td>
			</tr>
			<tr>
				<td>
					&nbsp;
				</td>
		        <td class="tbltext1">
					<b>Perfil Profesional</b>
				</td>
		        <td class="textblack"> 
					<b><%=sTipoProf%></b>
				</td>
			</tr>
			<tr>
				<td>
					&nbsp;
				</td>
		        <td class="tbltext1">
					<b>Departamento</b>
				</td>
		        <td class="textblack"> 
					<b>
				<%	if sProvDesc <> "" and not isnull(sProvDesc) then
						Response.Write sProvDesc
				  	else
				  		Response.Write " - "	
					end if
				%>
					</b>
				</td>
			</tr>
			<tr>
				<td>
					&nbsp;
				</td>
		        <td class="tbltext1">
					<b>Region</b>
				</td>
		        <td class="textblack"> 
					<b>
				<%	if sRegioneDesc <> "" and not isnull(sRegioneDesc) then
						Response.Write sRegioneDesc
				  	else
				  		Response.Write " - "	
					end if
				%>
					</b>
				</td>
			</tr>
			<tr>
				<td>
					&nbsp;
				</td>
		        <td class="tbltext1">
					<b>Pa�s</b>
				</td>
				<td class="textblack"> 
					<b>
				<%	if sStatoDesc <> "" and not isnull(sStatoDesc) then
						Response.Write sStatoDesc
				  	else
				  		Response.Write " - "	
					end if
				%>
					</b>
				</td>
			</tr>
			<tr>
				<td>
					&nbsp;
				</td>
		        <td class="tbltext1">
					<b>Valido hasta el</b>
				</td>
		        <td class="textblack"> 
					<b><%=sDtAl%></b>
				</td>
			</tr>
			</table>
			<br>	
<%			'Dati Candidati contattati
			
			sql1 = "select FLOOR(MONTHS_BETWEEN(SYSDATE,P.DT_NASC)/12) As Eta, P.id_persona, P.prv_res, P.Cognome As Cognome, " &_ 
				" P.Nome, P.Cod_Fisc As CF, P.com_res, SO.Id_Sede As Id_Interin, SO.Id_CImpiego As ID_Impiego, " &_ 
				" SO.cod_stdis  As categoria, to_char(RC.dt_ins, 'DD/MM/YYYY') As dataIns, RC.cod_esito_sel, " &_
				" RC.COD_TIPO_INS " &_
				" from PERSONA P, STATO_OCCUPAZIONALE SO, RICHIESTA_CANDIDATO RC " &_ 
				" where P.id_persona = SO.id_persona(+)" &_
				" And SO.ind_status(+) = 0" &_
				" And RC.id_persona = P.id_persona " &_
				" and RC.id_richiesta = " & nIdAnnuncio &_
				" and RC.cod_tipo_sel ='0' " &_
				" order by P.id_persona"

			set RR1 = server.CreateObject("ADODB.recordset")
'PL-SQL * T-SQL  
SQL1 = TransformPLSQLToTSQL (SQL1) 
			RR1.Open sql1,CC,1
		
			'Response.Write "vany"
			'Response.End 
			
			if not RR1.EOF then
%>				<br>
				<table width="96%" border="0" cellpadding="1" cellspacing="2" align="center">
					<tr height="23" class="sfondocommaz"> 
					<td align="center" width="63">
						<b class="tbltext1"></b>
					</td>
					<td align="center" width="80">
						<b class="tbltext1">Apellido y Nombre</b>
					</td>
					<td align="center" width="70">
						<b class="tbltext1">Cuil</b>
					</td>
					<td align="center" width="100"> 
						<b class="tbltext1">T�tulos de Estudio</b>
					</td>
					<td align="center" width="80"> 
						<b class="tbltext1">Residencia</b>
					</td>
					<td align="center" width="120"> 
						<b class="tbltext1">Estado</b>
					</td>
					<td align="center" width="60"> 
						<b class="tbltext1">Proximidad</b>
					</td>
					<td align="center" width="80"> 
						<b class="tbltext1">Fecha envio <br> comunicaci�n</b>
					</td>
					<td align="center" width="90"> 
						<b class="tbltext1">Exito</b>
					</td>
				</tr>	
<%			
				Dim RRT, sqlT 'recordset per titolo di studio

				
				do until RR1.Eof 
						sqlT = "Select cod_liv_stud As titolo From TISTUD " &_ 
							" where id_persona (+)=" & RR1.fields("id_persona") &_ 
							" And cod_stat_stud (+)= 0 " &_
							" Order by AA_stud desc, dt_tmst desc"
							
'PL-SQL * T-SQL  
SQLT = TransformPLSQLToTSQL (SQLT) 
					Set RRT = CC.Execute(sqlT)

					if RRT.Eof then
						sTitolo = "-"
					else
						RRT.Movefirst
						sTitolo = RRT.fields("titolo")
					end if
					
					'Response.Write stitolo 
					'Response.End 
			
					RRT.Close
					set RRT = nothing
					sqlT = ""	
					
					bBlank = true
					if bCImpiego then
						if not isnull(RR1.Fields("Id_Impiego")) then
							if clng(RR1.Fields("Id_Impiego")) = clng(Session("Creator")) then
								bBlank = false
							end if
						end if
					end if
					if bAInterin then
						if not isnull(RR1.Fields("ID_Interin")) then
							if clng(RR1.Fields("ID_Interin")) = clng(Session("Creator")) then
								bBlank = false
							end if
						end if
					end if
%>				
				<tr height="23" class="tblsfondo">
					<td align="middle" class="TBLTEXT1">
						<b class="tbltext1">
							<%=RR1.fields("id_persona")%>
						</b>
					</td>
					<td align="left" class="textblack">
<%						if not bBlank then
							Response.Write RR1.Fields("Cognome") & "&nbsp;" & RR1.Fields("Nome")  
						else
							Response.Write "**********"
						end if
%>					</td>
					<td align="left" class="textblack">
<%						if not bBlank then
							Response.Write RR1.Fields("CF") 
						else
							Response.Write "**********"
						end if
%>
					</td>
					<td align="left" class="textblack">
					
<%
					if sTitolo <> "-" then
						sTitolo = lcase(DecCodVal("TSTUD",0,"",sTitolo,""))
					end if
					Response.Write sTitolo
%>
					</td>
					<td align="left" class="textblack">
						<%  
							sComune = DescrComune(RR1.fields("com_res"))
							if  sComune <> "0" then
								Response.Write sComune & "<br>&nbsp; (" & RR1.fields("prv_res") & ")"
							else
								Response.Write "-"
							end if	
						%> 
					</td>
					<td align="left" class="textblack">
					<%	if RR1.fields("categoria") <> "" then
							Response.Write  lcase(DecCodVal("STDIS",0,"",RR1.fields("categoria"),""))
						else
							Response.Write "-"
						end if
					%> 
					</td>
					<td valign="center" align="left" class=textblack>
<%		
						sqlGrado = "SELECT GRADO_FP_PERS FROM PERS_FIGPROF WHERE ID_PERSONA = " &_
								CLng(RR1.Fields("ID_PERSONA")) & " AND ID_FIGPROF = " &_
								clng(nIdProfilo) & " AND IND_STATUS = 0"
'PL-SQL * T-SQL  
SQLGRADO = TransformPLSQLToTSQL (SQLGRADO) 
						set RRGrado = CC.Execute(sqlGrado)

						if RRGrado.EOF then
							sGrado = 0
						else
							sGrado = RRGrado("GRADO_FP_PERS")
						end if
						RRGrado.close
						set RRGrado = nothing
	
						if sGrado > "0" then	
							Response.Write" " & sGrado & "%"
						else
							Response.Write " - "
						end if
%>					</td>
					<td width="80" align="left" class="textblack"> 
						&nbsp;<%=RR1.fields("dataIns")%>
					</td>
					<td valign="center" width="90" align="left"> 
						<table border="0" width="90" cellspacing="0" cellpadding="0">
							<tr>
								<td class="textblack">
							<%if (RR1.fields("cod_esito_sel") <> "") then
								Response.Write DecCodVal("ESSEL",0,date, RR1.fields("cod_esito_sel"), 1)						 
							else 
								' Se la persona si � autocandidata occorre
								' che far vedere il curriculum.
								if RR1.fields("cod_tipo_ins") = 1 then
									Response.Write "INTERESSATO ALL'OFFERTA </td>"
								end if	
						  
							end if%>
								</td>
							</tr>
						</table>
					</td>
				</tr>
<%				RR1.Movenext
			
			loop
			
			RR1.Close
			set RR1 = nothing
%>				
			</table>
			<br>
<%
		else
			Messaggio("No se encontr� ning�n postulante para este aviso")
		end if
%>
		<table WIDTH="96%" BORDER="0" CELLPADDING="0" CELLSPACING="0" align="center">
			<tr height="2">
				<td class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif">
				</td>
			</tr>		
		</table>

<%		RR.MoveNext
		loop

		RR.Close
		set RR = nothing
		End if
%>
