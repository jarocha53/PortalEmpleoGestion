<!--#include virtual="/util/portallib.asp"-->
<!--#include virtual="/strutt_testa2.asp"-->
<!--#include virtual="/include/openconn.asp"-->
<!--#include Virtual = "/include/SelAreaTerrBandi.asp"-->


<script type="text/javascript" src="/../../js/jquery.inputmask.js"></script>
<script type="text/javascript" src="/../../js/jquery.inputmask.regex.extensions.js"></script>
<%
	if ValidateService(session("idutente"),"IMP_VISIMPRESA",cc) <> "true" AND ValidateService(session("idutente"),"INGRESAREMPRESA",cc) <> "true" then 
		response.redirect "/util/error_login.asp"
	end if
	
	sTimpr=Request.Form ("cmbCodTimpr")
	
TEmp = request("TEmp")
'Response.Write(temp)

Session("FaltaCarga") = "N"

if (not isnull(TEmp)) and (TEmp <> "") then
	TEmp = ucase(TEmp)
end if 

dim Tit1p
dim Tit1s
dim nombre

if temp = "E" then 
	Rol = 1
	tit1s = "Empleador"
	tit1p = "Empleadores"
	nombre="Nombre o Raz�n <br> Social"
else
	Rol = 2
	tit1s = "Prestador"
	tit1p = "Prestadores"
	nombre="Instituci�n/Organismo"
end if

%>

<script language="javascript">
<!--#include virtual = "/include/SelComune.js"-->
<!--#include virtual = "/include/SelDepto.js"-->
<!--#include virtual = "/include/SelLoc.js"-->
<!--#include virtual = "/include/SelFormaJuridica.js"-->

//include del file per fare i controlli sulla validit� delle date
<!--#include virtual="/Include/ControlDate.inc"-->

//include del file per fare i controlli sulla numericit� dei campi
<!--#include virtual="/Include/ControlNum.inc"-->
<!--#include virtual="/Include/ControlString.inc"-->
<!--#include virtual="/Include/help.inc"-->

// SM - controllo RUC
<!--#include virtual="/Include/ControlloRuc.inc"-->
//SM fine

	$(function(){
	
		 //$('#tin').mask({mask:"999-999-9999",clearEmpty: false});

		$("#cmbTipoDoc").change(function(){
			$("#ni").val("");

			var cmbTipoDoc_val = $("#cmbTipoDoc").val();
		 	
		 	if(cmbTipoDoc_val == "NI1"){
				$("#ni").inputmask(
					//'Regex' ,  {  regex :  "[a-z]" }
				  	"999999999[-9]]",{ "clearIncomplete": true, "showMaskOnFocus": true }
				); 
		 	}else{
		 		$('#ni').inputmask('remove');
		 	}
		});


		 	

		//$.ui.mask.definitions['~'] = "[+-]";
		//$.ui.mask.defaults.allowPartials = true; //used to allowPartials on all instances of mask() on the page.
		//$("#tin").mask({mask: "##-#######"});
	
	});
	
	
	validaExistenciaEmpresa = function (){
	    cuit = document.FrmVerificacionCuitEmpresa.txtCodFis.value  //ruc
		tipdoc = document.FrmVerificacionCuitEmpresa.cmbTipoDoc.value
		
		//$("#validateCodFis").show();
		$.post("consultaExistenciaEmpresa.asp",{ tipDoc: tipdoc, numero: cuit },
		function(response){
		//$("#validateCodFis").hide();
		if(response == "no"){
			ValidarIdentificadores();//passValidateEmpresa = false;
		}else{
		  if(confirm("�la empresa existe, desea editarla?"))
			editarEmpresa();
		}
			
		});
	}


	function ValidarIdentificadores(){
		
		//renae = document.FrmVerificacionCuitEmpresa.txtNumIscReg.value  //nro de planilla
		cuit = document.FrmVerificacionCuitEmpresa.txtCodFis.value  //ruc
		tipdoc = document.FrmVerificacionCuitEmpresa.cmbTipoDoc.value
		//regice = document.FrmVerificacionCuitEmpresa.txtNumInps.value
		razon = document.FrmVerificacionCuitEmpresa.txtRagSoc.value
	    

/*
		var longitudDoc = $("#ni").val().length;
		var tipoDoc =  $("#cmbTipoDoc").val();
		if(tipoDoc == "NI" && longitudDoc != 11){
			alert("Long invalida");	
		}
		
*/

		/*if (FrmVerificacionCuitEmpresa.cmbTipoDoc.value == ""){
			alert("El Tipo de Documento es requerido");
			return false;
		}
				
		if (FrmVerificacionCuitEmpresa.txtCodFis.value == ""){
			alert("El N�mero de Identificaci�n es obligatorio");
			FrmVerificacionCuitEmpresa.txtCodFis.focus();
			return false;
		}*/


		FrmVerificacionCuitEmpresa.txtRagSoc.value = TRIM(FrmVerificacionCuitEmpresa.txtRagSoc.value)		
		if (FrmVerificacionCuitEmpresa.txtRagSoc.value == "") 
		{
			if(document.FrmVerificacionCuitEmpresa.TEmp.value == "P")
				alert("El campo Instituci�n/Organismo es obligatorio");
			else	                          
				alert("El campo Nombre o Raz�n Social es obligatorio");

			//maxlength
			FrmVerificacionCuitEmpresa.txtRagSoc.focus(); 
			return false;
		}	
			
		/*	FrmVerificacionCuitEmpresa.txtNumIscReg.value = TRIM(FrmVerificacionCuitEmpresa.txtNumIscReg.value)		
		if (!FrmVerificacionCuitEmpresa.txtNumIscReg.value == "")	
			{
				if (eval(FrmVerificacionCuitEmpresa.txtNumIscReg.value) == 0)
					{
						alert("El campo Planilla no puede ser cero")
						FrmVerificacionCuitEmpresa.txtNumIscReg.focus()
						return false
					}
				
				if (FrmVerificacionCuitEmpresa.txtNumIscReg.value != "")
				{
					if (!IsNumIscReg(FrmVerificacionCuitEmpresa.txtNumIscReg.value))
					{
						FrmVerificacionCuitEmpresa.txtNumIscReg.focus()
						return false				
					}
				}
			}
		*/	
			// 05/06/2008 - Se Planilla � valorizzato allora deve essere lungo 15 caratteri
		/*	if ((FrmVerificacionCuitEmpresa.txtNumIscReg.value.length != 15 && FrmVerificacionCuitEmpresa.txtNumIscReg.value.length != 0))
					// SM fine
					{
				
						alert("El campo Planilla debe ser de 15 caracteres")
						FrmVerificacionCuitEmpresa.txtNumIscReg.focus(); 
						return false;
					}	
			
		*/	
			/* DAMIAN 31/10/2007 - SE QUITA OBLIGATORIEDAD		
			FrmVerificacionCuitEmpresa.txtNumInps.value = TRIM(FrmVerificacionCuitEmpresa.txtNumInps.value)		
		if (!FrmVerificacionCuitEmpresa.txtNumInps.value == "")
			{
				if (FrmVerificacionCuitEmpresa.txtNumInps.value != "")
				{
			
					if (!IsNum(FrmVerificacionCuitEmpresa.txtNumInps.value))
					{
						alert("El valor del campo REGICE  debe ser num�rico")
						FrmVerificacionCuitEmpresa.txtNumInps.focus() 
						return false
					}	
					
				}
			}	
			FIN 31/10/2007 - SE QUITA OBLIGATORIEDAD */
			
		// SM inizio - inseriti controlli per il RUC
		
			
			
		if (FrmVerificacionCuitEmpresa.txtCodFis.value != ""){
			blank = " ";
		
			if (!ChechSingolChar(FrmVerificacionCuitEmpresa.txtCodFis.value,blank)){
				// ao 18/09/2007 sostituito cuit/cuil con ruc
				alert("El N�mero de Identificaci�n no puede contener espacios en blanco")
				FrmVerificacionCuitEmpresa.txtCodFis.focus();
				return false;
			}

			//SM inizio
			
			/*if ((FrmVerificacionCuitEmpresa.txtCodFis.value.length != 12))
			// SM fine
			{
		
				alert("El N�mero de Identificaci�n debe ser de 12 caracteres")
				FrmVerificacionCuitEmpresa.txtCodFis.focus(); 
				return false;
			}*/	
			/* 1/11/2007 DAMIAN	- SE QUITA VERIFICACION
			if (!ValidateInputStringWithNumber(FrmVerificacionCuitEmpresa.txtCodFis.value))
			{
		
				alert("El campo Ruc es err�neo")
				FrmVerificacionCuitEmpresa.txtCodFis.focus()
				return false;
			}
			 FIN 1/11/2007  */

			var cmbTipoDoc_val = $("#cmbTipoDoc").val();
			 	
		 	if(cmbTipoDoc_val != "NI"){
				if (!IsNum(FrmVerificacionCuitEmpresa.txtCodFis.value))
				{
					alert("El N�mero de Identificaci�n debe ser num�rico")
					FrmVerificacionCuitEmpresa.txtCodFis.focus()
					return false;
				}
			}/*else if($("#ni").val().length != 10){
				alert("El N�mero de Identificaci�n se encuentra incompleto, o excede el n�mero de digitos de un Nit[10]");
				$("#ni").focus()
				return false;
			}*/


			//SM inizio
			
			// 1/11/2007 DAMIAN	- SE QUITA VERIFICACION
					
			//if (!ControlloRuc(eval(FrmVerificacionCuitEmpresa.txtCodFis.value)))
			//	{
			//		FrmVerificacionCuitEmpresa.txtCodFis.focus()
			//		return false;
			//	}
			//	FIN 1/11/2007  
			
			//SM fine
					
		}else{
			//if ((FrmVerificacionCuitEmpresa.txtNumIscReg.value == "") && (FrmVerificacionCuitEmpresa.txtNumInps.value == ""))	
			if (FrmVerificacionCuitEmpresa.txtNumIscReg.value == "")	
			{
			//	alert("El campo Cuit es obligatorio.");
				alert("El N�mero de Identificaci�n es obligatorio.");
					return false;
			}
		}
		
					
		
		TEmp = document.FrmVerificacionCuitEmpresa.TEmp.value
		
		//if ((renae != "") || (regice != "") || (cuit != ""))
		if ((tipdoc != "") || (cuit != ""))		
		{
			razon = razon.replace("&","chrecommerciale");
			myUrl = "ValidarIdentificadores.asp?TEmp="+TEmp+"&Tipo=IE&razon="+razon+"&tipdoc="+tipdoc+"&cuit="+cuit
			//myUrl = "ValidarIdentificadores.asp?TEmp="+TEmp+"&Tipo=IE&razon="+razon+"&renae="+renae+"&cuit="+cuit
			//document.location.href = "ValidarIdentificadores.asp?TEmp="+TEmp+"&Tipo=IE&razon="+razon+"&renae="+renae+"&regice="+regice+"&cuit="+cuit
			//alert(myUrl);
			document.location.href = myUrl;
		}
		else
		{
			alert("Debe completar alg�n campo a validar");
			return false;
		}
	}
	
	function editarEmpresa(){
	
		cuit = document.FrmVerificacionCuitEmpresa.txtCodFis.value  //ruc
		tipdoc = document.FrmVerificacionCuitEmpresa.cmbTipoDoc.value
		razon = document.FrmVerificacionCuitEmpresa.txtRagSoc.value
		TEmp = document.FrmVerificacionCuitEmpresa.TEmp.value
		
		//if ((renae != "") || (regice != "") || (cuit != ""))
		if ((tipdoc != "") || (cuit != ""))		
		{
			razon = razon.replace("&","chrecommerciale");
			myUrl = "ValidarIdentificadores.asp?TEmp="+TEmp+"&Tipo=IE&razon="+razon+"&tipdoc="+tipdoc+"&cuit="+cuit
			//myUrl = "ValidarIdentificadores.asp?TEmp="+TEmp+"&Tipo=IE&razon="+razon+"&renae="+renae+"&cuit="+cuit
			//document.location.href = "ValidarIdentificadores.asp?TEmp="+TEmp+"&Tipo=IE&razon="+razon+"&renae="+renae+"&regice="+regice+"&cuit="+cuit
			//alert(myUrl);
			document.location.href = myUrl;
		}
		else
		{
			alert("Debe completar alg�n campo a validar");
			return false;
		}
	}

	function IsNumIscReg(sNumIscReg) {
		var t
		
		for (t=0; t<8;t++) {
			if (!IsNum(sNumIscReg.charAt(t))) {
				//	alert("El campo N�mero RENAE debe ser num�rico")
					alert("El campo N�mero de planilla  debe ser num�rico")
					return false
			}
		}
		return true
	}

</script>
<!--#include virtual="/util/dbutil.asp"-->
<!--#include virtual="/include/ControlDateVB.asp"-->
<!--#include virtual="/include/DecCod.asp"-->

<table border="0" width="530" cellspacing="0" cellpadding="0" height="81">
   <tr>
     <td width="530" background="<%=Session("Progetto")%>/images/titoli/strumenti2g.gif" height="81" valign="bottom" align="right">
       <table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
         <tr>
           
          <td width="100%" valign="top" align="right"><b class="tbltext1a">Gesti&oacute;n de
            <%=tit1s%> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></b></td>
         </tr>
       </table>
     </td>
   </tr>
</table>
<br>

<table cellpadding="0" cellspacing="0" width="500" border="0">
	<tr height="18">
	
    <td class="sfondomenu" height="18" width="67%"> <span class="tbltext0"> <b>Gesti�n de 
      <%=tit1s%> </b></span> </td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
		
    <td valign="middle" align="right" class="tbltext1" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">(*) 
      campo obligatorio</td>
		</td>
		<td valign="middle" align="right" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1"></td>
	</tr>
	<tr width="371" class="sfondocomm">
		
    <td colspan="3">Ingresar los datos correspondientes al <%=tit1s%>. <br>
	Los datos ingresados en esta pantalla no podr�n modificarse en el formulario de ingreso. <br>
	<!--br>
	NOTA: Ingresar al menos uno de los siguientes datos: N� de Planilla, RUC-->
    <!-- <a href="Javascript:Show_Help('/Pgm/help/Imprese/IMP_InsImpresa')"><img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a> -->     </td>
	</tr>   
	<tr height="2">
		<td colspan="3" class="sfondocomm" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
	</tr>
</table>

<br>
<br>
<form name="FrmVerificacionCuitEmpresa" action="" method="post">
	<table border="0" cellpadding="0" cellspacing="1" width="500">
        <tr>
			<td align="middle" colspan="2" nowrap>
				<p align="left"> <span class="tbltext1"><strong><%=nombre + " "%></strong></span></p>
			</td>
            <td align="left" colspan="2"> 
				<p align="left">
					<input style="TEXT-TRANSFORM:uppercase" class="textblack" size="50" maxlength="50" name="txtRagSoc">					
				</p>
            </td>
		</tr>	
			
        <!--JHAMON <tr>
			<td align="left" colspan="2">
				<p align="left">
					<span class="tbltext1"><b>Nro de Planilla</b></span>					
				</p>
            </td>            
			<td align="left" colspan="2"> 
			
				<p align="left">
				<input style="TEXT-TRANSFORM:uppercase" class="textblack" size="20" maxlength="15" name="txtNumIscReg">			
				</p>
            </td>
        </tr>      
        -->
        <td align="left" class="tbltext1"><strong></strong> <span class="tbltext1"><strong>Tipo Documento*</strong> </span></td>
	      <td align="left" colspan="2"><%
	         
'	         sSQL = "SELECT COD_DOCST,ID_DOC,DT_FIN_VAL,DT_TMST FROM PERS_DOC" 

'			set	rsDocumento= CC.execute(sSQL)
	         
							sInt = "DOCST|0|" & date & "||cmbTipoDoc| and valore in('02','03') ORDER BY DESCRIZIONE"
							
							CreateCombo(sInt)
				%>          
				</td>
      </tr>  

        <tr>
			<td align="left" colspan="2">
				<p align="left">
				<!-- ao 1/19/2007 sostituito cuit/cuil con ruc -->
				<span class="tbltext1"><b>N�mero*
				<%'if TEmp = "E" then 
				'Response.Write "*"
				'end if %>
				 </b></span></p>
            </td>  
              
			<td align="left" colspan="2"> 
				<p align="left">
					<input id="ni" style="TEXT-TRANSFORM:uppercase" class="textblack" size="15" maxlength="15" name="txtCodFis">
				</p>
            </td>
        </tr>
        <!-- DAMIAN 31/10/2007 SE QUITA REGICE
        <tr>
            <td align="left" colspan="2">
				<p align="left">
				   <span class="tbltext1"><b>Nro. REGICE </b></span>
				</p>
            </td>            
			<td align="left" colspan="2"> 
				<p align="left">
				   <input style="TEXT-TRANSFORM:uppercase" class="textblack" size="16" maxlength="6" name="txtNumInps">				
		        </p>
            </td>
		</tr>
		
		<tr>
            <td align="right" colspan="4">
				<input type="button" class="my" name="BTNVALIDAR" value="Validar Regice, Renae y RUC" onclick="validaExistenciaEmpresa()">
            </td>
		</tr>
		FIN 31/10/2007 -->
        <tr>
            <td align="center" colspan="4">
				<input type="button" class="my" name="BTNVALIDAR" value="Enviar" onclick="validaExistenciaEmpresa()">
            </td>
		</tr>
		
	</table>
	<input type="hidden" value="<%=TEmp%>" name="TEmp">
</form>
<!--#include virtual="/strutt_coda2.asp"-->
<!--#include virtual="/include/closeconn.asp"-->