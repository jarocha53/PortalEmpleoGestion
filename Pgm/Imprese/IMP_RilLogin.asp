<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include Virtual = "/strutt_testa2.asp"-->
<!--#include Virtual = "/include/OpenConn.asp"-->
<!--#include Virtual = "/include/DecCod.asp"-->
<!--#include Virtual = "/include/ControlDateVB.asp"-->
<!--#include Virtual = "/util/dbutil.asp"-->

<script LANGUAGE="Javascript">

//include del file per fare i controlli sulla numericit� dei campi
<!--#include Virtual = "/Include/ControlNum.inc"-->
<!--#include Virtual = "/Include/ControlString.inc"-->
//include del file per fare i controlli sulla stringa
<!--#include Virtual = "/Include/ControlString.inc"-->
<!--#include virtual = "/Include/help.inc"-->

	
	
	//Funzione per i controlli dei campi 
	function ControllaDati(){
// -------- CONTROLLO VALORI INSERITI -------------------------		

/*
 Parte di controlli sui campi del referente
*/
		document.frmIscri.txtCognome.value=TRIM(document.frmIscri.txtCognome.value)
		sCognome=ValidateInputStringWithOutNumber(document.frmIscri.txtCognome.value)
		if  (sCognome==false){
			alert("Apellido formalmente err�neo.")
			document.frmIscri.txtCognome.focus() 
			return false
		}
			
		if (frmIscri.txtCognome.value == ""){
			alert("El campo apellido es obligatorio!")
			frmIscri.txtCognome.focus() 
			return false
		}
		
		document.frmIscri.txtNome.value=TRIM(document.frmIscri.txtNome.value)
		sNome=ValidateInputStringWithOutNumber(document.frmIscri.txtNome.value)
		if  (sNome==false){
			alert("Nombre formalmente err�neo.")
			document.frmIscri.txtNome.focus() 
			return false
		}		
		if (frmIscri.txtNome.value == ""){
			alert("El campo nombre es obligatorio!")
			frmIscri.txtNome.focus() 
			return false
		}
		
		frmIscri.sTelefono.value = TRIM(frmIscri.sTelefono.value)

		if (frmIscri.sTelefono.value != "")
		{
			if (!IsNum(frmIscri.sTelefono.value)) {
				alert("El campo Tel�fono debe ser num�rico")
				frmIscri.sTelefono.focus() 
				return false
			}
			if (eval(document.frmIscri.sTelefono.value) == 0){
				alert("El n�mero de tel�fono no puede ser cero")
				document.frmIscri.sTelefono.focus()
				return false
			}
		}
		
		/*if (document.frmIscri.cmbTipoDocP.value == "")
		{
		    alert("El campo tipo de documento es obligatorio!")
			document.frmIscri.cmbTipoDocP.focus() 
			return false
		}
		if (TRIM(document.frmIscri.txtNumDoc.value) == "")
		{
		    alert("El campo n�mero de Documento es obligatorio!")
			document.frmIscri.txtNumDoc.focus() 
			return false;
		 }
		 else
		 {
			if (!IsNum(TRIM(document.frmIscri.txtNumDoc.value)))
			{
			     alert ("El campo debe ser num�rico");
				  return false;
			}
		 }*/
		 
		if (document.frmIscri.cmbTipoDocP.value != "")
		{
			if (TRIM(document.frmIscri.txtNumDoc.value) != "")
			{
				if (!IsNum(TRIM(document.frmIscri.txtNumDoc.value)))
				{
				    alert ("El campo debe ser num�rico");
				    document.frmIscri.txtNumDoc.focus()
					return false;
				}
				// if (document.frmIscri.txtNumDoc.value.length >10)
				// {
				// 	alert("El n�mero de documento no debe superar los 10 caracteres");
				// 	return false;
				// }
			 }
			 else
			 {
				alert("Si seleccion� el tipo de documento debe ingresar el n�mero");
				document.frmIscri.txtNumDoc.focus()
				return false;
			 }
		}
		else
		{
			if (TRIM(document.frmIscri.txtNumDoc.value) != "")
			{
				alert("Debe seleccionar primero el tipo de documento");
				document.frmIscri.txtNumDoc.focus()
				return false;
			 }		
		}
		
		
		
		
		//if (frmIscri.elements[2].selectedIndex == 0) {
		/*if (frmIscri.cmbRefRuo.selectedIndex == 0){
			alert ("El campo cargo es obligatorio")
			//frmIscri.elements[2].focus()
			frmIscri.cmbRefRuo.focus()
			return false
		}*/
		
		
		if (frmIscri.cmbRefRuo.value == ""){
			alert ("El campo cargo es obligatorio")
			//frmIscri.elements[2].focus()
			frmIscri.cmbRefRuo.focus()
			return false
		}
		
		//Controlli su LOGIN e PW: posso inserire solo numeri e / o lettere
		if (frmIscri.lgpw.value == "S"){
			var anyString = document.frmIscri.txtLogin.value;
			if ((frmIscri.txtLogin.value == "") && (frmIscri.txtPW.value != "")){
				alert("El campo Usuario es obligatorio")
				frmIscri.txtLogin.focus() 
				return false
			}
			
			if ((frmIscri.txtLogin.value != "") && (frmIscri.txtPW.value == "")){
				alert("El campo Contrase�a es obligatorio")
				frmIscri.txtPW.focus() 
				return false
			}
			
			if ((frmIscri.txtLogin.value != "") && (frmIscri.txtRePW.value == "")){
				alert("El campo Reingresar contrase�a es obligatorio")
				frmIscri.txtRePW.focus() 
				return false
			}
			
			if (trim(frmIscri.txtPW.value).length < 8){
				alert("El campo Contrase�a debe ser de al menos 8 caracteres")
				frmIscri.txtPW.focus() 
				return false
			}
			
			if (frmIscri.txtPW.value != frmIscri.txtRePW.value){
				alert("El campo Reingresa contrase�a es diferente a la contrase�a")
				frmIscri.txtRePW.focus() 
				return false
			}
			
			//non permette la cancellazione di login e password
			if ((frmIscri.txtLogin.value == "") && (frmIscri.lgold.value != "")){
				alert("El campo Usuario es obligatorio")
				frmIscri.txtLogin.focus() 
				return false
			}

			for (var i=0; i<=anyString.length-1; i++){
				if(((anyString.charAt(i) >= "A") && (anyString.charAt(i) <= "Z")) || 
					((anyString.charAt(i) >= "a") && (anyString.charAt(i) <= "z")) || 
					((anyString.charAt(i) >= "0") && (anyString.charAt(i) <= "9")) ||
					(anyString.charAt(i) == ".") || (anyString.charAt(i) == "-") ||
					(anyString.charAt(i) == "_"))
				{
				}
				else
				{		
					document.frmIscri.txtLogin.focus();
				 	alert("Ingrese en el campo usuario un valor v�lido");
					return false
				}		
			}

			var anyString = document.frmIscri.txtPW.value;

			for (var i=0; i<=anyString.length-1; i++){
				if(((anyString.charAt(i) >= "A") && (anyString.charAt(i) <= "Z")) || 
					((anyString.charAt(i) >= "a") && (anyString.charAt(i) <= "z")) || 
					((anyString.charAt(i) >= "0") && (anyString.charAt(i) <= "9")) ||
					(anyString.charAt(i) == ".") || (anyString.charAt(i) == "-") ||
					(anyString.charAt(i) == "_"))
				{
				}
				else
				{		
					document.frmIscri.txtPW.focus();
				 	alert("Ingrese en el campo Contrase�a un valor v�lido");
					return false
				}		
			}
		}
		
		//Email
		
		document.frmIscri.txtMail.value=TRIM(document.frmIscri.txtMail.value)
		
		//se inseriti login e pw � obbligatira l'email GB
		if ((frmIscri.txtLogin.value != "") && (document.frmIscri.txtMail.value == "")){
			alert("La direcci�n de correo electr�nico es obligatoria si asign� del Usuario y Contrase�a")
			frmIscri.txtPW.focus() 
			return false
		}
		
		if (document.frmIscri.txtMail.value != "")
		{
			appoEmail=ValidateEmail(document.frmIscri.txtMail.value)
		
			if  (appoEmail==false)
			{
				alert("Direcci�n de correo electr�nico err�neo")
				document.frmIscri.txtMail.focus() 
				return false
			}	
		}
			
	///agregado
	
	
			//if (document.frmIscri.txtCodFisc.value == ""){
		

		//}  
		
		/*if (document.frmIscri.txtobservaciones.value == "")
		{
			alert("Debe completar el campo observaciones");
			return false;
		}*/
		
		/*if (document.frmIscri.txtobservaciones.value.length >900)
		{
			alert("No puede superar los 900 caracteres");
			return false;
		}*/
		///fin agregado	
		
		/* S'attivano i campi protetti*/
		document.frmIscri.txtCognome.disabled = false
		document.frmIscri.txtNome.disabled = false
		document.frmIscri.cmbTipoDocP.disabled = false
		document.frmIscri.txtNumDoc.disabled = false
		document.frmIscri.txtMail.disabled = false
		
		return true
	}	



	//Cancella Dati
	function Cancella(frmIscri){
		document.frmIscri.reset();
		return false
	}

	// funciones agregadas por Damian 31/07/2008
	function ltrim(str) { 
		for(var k = 0; k < str.length && isWhitespace(str.charAt(k)); k++);
		return str.substring(k, str.length);
	}
	function rtrim(str) {
		for(var j=str.length-1; j>=0 && isWhitespace(str.charAt(j)) ; j--) ;
		return str.substring(0,j+1);
	}
	function trim(str) {
		return ltrim(rtrim(str));
	}
	function isWhitespace(charToCheck) {
		var whitespaceChars = " \t\n\r\f";
		return (whitespaceChars.indexOf(charToCheck) != -1);
	}
</script>
<% 

TEmp = request("TEmp")

if (not isnull(TEmp)) and (TEmp <> "") then
	TEmp = ucase(TEmp)
end if 


dim Tit1p
dim Tit1s
if temp = "E" then 
	tit1s = "Empleador"
	tit1p = "Empleadores"
else
	tit1s = "Prestador"
	tit1p = "Prestadores"
end if

	dim sidsede
	dim simpresa
	dim sIdContacto
	DIM sCodCargo
	dim sCodDocumento
	dim sNroDocumento
	dim sApellido
	dim sNombre
	dim sEmail
	dim sObservaciones
	dim Session_Creator
	dim IdCimpego
	dim sCodSede
	
	sidsede=Request.Form("idsede")
	sidImp=Request.Form("idimpresa")


	Session_Creator=Request.Form("Usuario")
	IdCimpego=Request.Form("IdCimpego")
	sCodSede=Request.Form("CodSede")



	if mid(Session("UserProfiles"),1,1) = "0" then
		sIsa = "0"
	else
		sIsa = mid(Session("UserProfiles"),1,2)
	end if
	   

   
   %>
   <!--#include virtual = "/include/RichLogin.asp"--><%

   
   sRil=clng(RichLogin(sidsede,"S"))


   %>

<% 
sCodImp = sIdImp


miaimpresa=0
if clng(session("creator")) >0 then
	sSQL = "SELECT ID_IMPRESA FROM SEDE_IMPRESA WHERE ID_SEDE =" & clng(session("creator"))
	'PL-SQL * T-SQL  
	SSQL = TransformPLSQLToTSQL (SSQL) 
	set rsSImpresa = CC.Execute(sSQL)

	miaimpresa=rsSImpresa("ID_IMPRESA")
end if

set rsRepresentante = Server.CreateObject("ADODB.Recordset")

     sSql="SELECT * from Contacto_sede " &_
			" where  id_sede=" & sIdSede
	'response.write sSQL
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
     rsRepresentante.open sSQL,CX,3
        
	 sApellido=""
     sNombre="" 
     sEMail=""
     sIdContacto=0
     sCodCargo=""
     sCodDocumento=""
     sNroDocumento=""
     sObservaciones=""
     sTelefono=""
     
     if not rsRepresentante.EOF then 
		sApellido=rsRepresentante("Apellido_Contacto")
		sNombre=rsRepresentante("Nombre_Contacto")
		sEMail=rsRepresentante("email_contacto")
		sIdContacto=rsRepresentante("Id_Contacto")
		sCodCargo=rsRepresentante("Id_Cargo")
		sCodDocumento=rsRepresentante("Cod_docst")
		sNroDocumento=rsRepresentante("Numero_Doc")
		sObservaciones=ltrim(rtrim(rsRepresentante("observaciones")))
		sTelefono= rsRepresentante("num_tel")
	end if
	rsRepresentante.Close
	set rsRepresentante=nothing
	
	

VieneDePagina = request("VieneDePagina")

%>




<table border="0" width="530" cellspacing="0" cellpadding="0" height="81">
   <tr>
     <td width="530" background="<%=Session("Progetto")%>/images/titoli/strumenti2g.gif" height="81" valign="bottom" align="right">
       <table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
         <tr>
           <td width="100%" valign="top" align="right"><b class="tbltext1a">Gesti�n de <%=tit1s%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></b></td>
         </tr>
       </table>
     </td>
   </tr>
</table>
<br>
<!--#include File="IMP_Label.asp"-->
<%
      set rscontactosede = server.CreateObject("adodb.recordset")
      
      ssql2="SELECT i.RAG_SOC, i.COD_TIMPR, descrizione FROM IMPRESA i" &_
	  " inner join sede_impresa si on si.id_impresa = i.id_impresa " &_
	  " inner join contacto_sede cs on cs.id_sede = si.id_sede " &_
	  " WHERE i.ID_IMPRESA=" &  sCodImp & " and si.id_sede = " & sIdSede
			
			
	'Response.Write ssql2
	
'PL-SQL * T-SQL  
SSQL2 = TransformPLSQLToTSQL (SSQL2) 
	  set rscontactosede = cc.execute(ssql2)
	  
	  
	  if not rscontactosede.EOF then
		sede = rscontactosede("descrizione")
	  end if 
	  
	  set rscontactosede = nothing
%>
<%if sede <> "" then%>
            <TABLE width=95% bordercolor=#FFFFFF border=1 cellspacing=1 cellpadding=0>					
	             <TR height="16" class="tblsfondo">
	                 <FORM id=form2 name=form2>
						<TD height="18" width="45%" align="center"> 
						  <!--SPAN class="tbltextmenu2"-->
						  
							<SPAN class="tbltext1"><b>(Contacto perteneciente a la sede: <%=sede%>)</b></span>
						  
						</TD>
		             </FORM>	
	            </TR>
	       </TABLE>	
<%end if%>	       
	       
<br>
<table cellpadding="0" cellspacing="0" width="500" border="0">
   <tr height="18">
		<td class="sfondomenu" height="18" width="67%">
			<span class="tbltext0"><b>DATOS DE LA PERSONA DE CONTACTO</b></span></td>
			
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
		<td valign="middle" align="right" class="tbltext1" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">(*) campo obligatorio</td>
    </tr>
</table>		
<table cellpadding="0" cellspacing="0" width="500" border="0">
   <tr width="371">
   <td align="left" class="sfondocomm" colspan="3" >Ingrese los datos de la persona de contacto, Usuario y Contrase�a de acceso al portal.	
<!-- 	<a href="Javascript:Show_Help('/Pgm/help/Imprese/IMP_RilLogin')"><img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
 -->	</td>
	</tr>   
	<tr height="2">
		<td colspan="3" class="SFONDOCOMMAZ" background="<%=Session("Progetto")%>/images/separazione.gif">
		</td>
	</tr>
</table><br>

<form action="IMP_CnfRichiesta.asp" method="post" name="frmIscri" id="frmIscri">
<input type="hidden" name="idimpresa" value="<%=sIdImp%>">
<input type="hidden" name="idsede" value="<%=sidSede%>">
<input type="hidden" name="idContacto" value="<%=sidContacto%>">
<input type="hidden" name="TEmp" value="<%=TEmp%>">
<input type="hidden" name="VieneDePagina" value="<%=VieneDePagina%>">



<% 
''if sRil=0 then 
%> 

<% 
	'busco el representante en tabla Contacto_sede,por codigo de sede
	
	
	'''borrar
	
if(trim(sCodImp) = trim(miaimpresa)) and trim(sApellido) <> "" then
	prot="disabled"
end if
%>


	
		<!---------- Inizio Tabella Inserimento ---------->
		<table border="0" width="350" cellpadding="2" cellspacing="1">   
		   <!--<tr>
				<td COLSPAN="2" width="500">
					<span class="tbltext1a"><b>Representante:</b></span>
				</td>
			</tr>-->
			<tr>
				<td colspan="2"></td>
			</tr>     
			<tr>
				<td align="left" class="tbltext1" nowrap><b>
					&nbsp;&nbsp;Apellido </b>*	
				</td>
				<td align="left">
					<input style="TEXT-TRANSFORM: uppercase;" <%=prot%> size="49" class="textblack" maxlength="50"  name="txtCognome" value="<%=sApellido%>">
				</td>
		   </tr> 
			<tr>
				<td align="left" class="tbltext1" nowrap><b>
					&nbsp;&nbsp;Nombre </b>*		
				</td>
				<td align="left">
					<input style="TEXT-TRANSFORM: uppercase;" <%=prot%> size="49" class="textblack" maxlength="50" name="txtNome" value="<%=sNombre%>">
				</td>
		   </tr> 	
			<tr>
				<td align="left" class="tbltext1" nowrap><b>
					&nbsp;&nbsp;Tipo de Documento</b>	
				</td>
				<td align="left">
						
				      <% if prot="disabled" then
							sInt = "DOCST|0|" & date & "|" & sCodDocumento & "|cmbTipoDocP' " & prot & " alt=' |ORDER BY DESCRIZIONE"		
							 CreateCombo(sInt)
				      else
							sInt = "DOCST|0|" & date & "|" & sCodDocumento & "|cmbTipoDocP| and valore in('01','03') ORDER BY DESCRIZIONE"		
							 CreateCombo(sInt)
				       end if
				       ''''sRefRuo = "RUOLO|0|"  &  date & "|" & scodCargo & "|cmbRefRuo| ORDER BY DESCRIZIONE"
				      
				       
				       %>
				</td>
		   </tr>  
		   
			<tr>
				<td align="left" class="tbltext1" nowrap><b>
					&nbsp;&nbsp;Nro. de Documento</b>		
				</td>
				<td align="left">
					<input size="20" class="textblack" maxlength="15"  <%=prot%> name="txtNumDoc" ID="Text1" value="<%=sNroDocumento%>">
				</td>
		   </tr>  
			
			
			
			
			
			
			<tr>
				<td align="left" class="tbltext1" nowrap><b>
					&nbsp;&nbsp;Cargo </b>*	
				</td>
				<td align="left">
				<input type="text" class="textblack" style="TEXT-TRANSFORM: uppercase;" name="cmbRefRuo" value="<%=scodCargo%>" size="40" maxlength="50">
					<%

						'sInt = "TSEDE|" & sisa & "|" & date & "|" & sCodSede & "|cmbCodSede| ORDER BY DESCRIZIONE"	
					
						'Cargo Richiamo funzione che popola la combo RUOLO
						'-----------------------------------------------	
						'sCodCargo
						
								
						'sRefRuo = "RUOLO|0|" & date & "|" & NULL & "|cmbRefRuo|ORDER BY DESCRIZIONE"
								
						'CreateCombo(sRefRuo)
						
						''sRefRuo = "RUOLO|0|"  &  date & "|" & scodCargo & "|cmbRefRuo| ORDER BY DESCRIZIONE"
						
						''CreateCombo(sRefRuo)
						
						'-----------------------------------------------				
					%>		
				</td>
		   </tr>   
			<tr>
				<td align="left" class="tbltext1" nowrap><b>
					&nbsp;&nbsp;Tel�fono </b>		
				</td>
				<td align="left">
					<input size="49" class="textblack" maxlength="100" name="sTelefono" value="<%=sTelefono%>">
				</td>
		   </tr> 			 
			<tr>
				<td align="left" class="tbltext1" nowrap><b>
					&nbsp;&nbsp;Correo Electr�nico </b>		
				</td>
				<td align="left">
					<input size="49" class="textblack" <%=prot%> maxlength="100" name="txtMail" value="<%=sEMail%>">
				</td>
		   </tr>   

			<tr>
				<td align="left" class="tbltext1" nowrap><b>
					&nbsp;&nbsp;Observaciones </b>		
				</td>
				<td align="left">
					
					<textarea OnKeyUp="JavaScript:CheckLenTxArea(document.frmIscri.txtObservaciones,document.frmIscri.textNumCaracteres,900);" name="txtObservaciones" cols="30" rows="7"><%=sObservaciones%></textarea>
					
					<input type="text" name="textNumCaracteres" size="3" value ="900" readonly>
				</td>
		   </tr>  		   


		   
		   
		     
		   <tr>
				<td>&nbsp;</td>
		   </tr>

		</table>
<br>
<%
if(trim(sCodImp) <> trim(miaimpresa)) then
		set rsLogin = Server.CreateObject("ADODB.Recordset")
		sSQL = "SELECT LOGIN, PASSWORD, IND_ABIL FROM UTENTE WHERE CREATOR =" & sidsede 
		rsLogin.open sSQL,cx,3
		
		if not rsLogin.EOF then
			lg=rsLogin("LOGIN")
			pw=rsLogin("PASSWORD")
			if rsLogin("IND_ABIL")="S" then
				ia="checked"	
			end if	
		else	
			ia="checked"	
		end if
		'response.write "lg= " & lg

%>
	<input type="hidden" name="lgpw" value="S">
	<input type="hidden" name="lgold" value=<%=lg%>>
	<table border=2>		   
			<tr>
				<td align="left" class="tbltext1" width="85" nowrap><b>
					&nbsp;&nbsp;Usuario </b>		
				</td>
				<td align="left">
					<input style="TEXT-TRANSFORM: uppercase;" size="37" maxlength="15" class="textblack" name="txtLogin" ID="Text2" value="<%=lg%>"
					<%if trim(lg)<>"" then response.write "disabled" end if%>>
				</td>
		   </tr>   
	
			<tr>
				<td align="left" class="tbltext1" width="85" nowrap><b>
					&nbsp;&nbsp;Contrase�a </b>
				</td>
				<td align="left">
					<input type='password' style="TEXT-TRANSFORM: uppercase;" size="37" maxlength="15" class="textblack" name="txtPW" ID="Text2" value=<%=pw%>>
				</td>
		   </tr>  
		   <tr>
				<td align="left" class="tbltext1" width="85" nowrap><b>
					&nbsp;&nbsp;Reingresar contrase�a </b>
				</td>
				<td align="left">
					<input type='password' style="TEXT-TRANSFORM: uppercase;" size="37" maxlength="15" class="textblack" name="txtRePW" ID="Text2" value=<%=pw%>>
				</td>
		   </tr>  
		   	<tr>
				<td align="left" class="tbltext1" width="85" nowrap><b>
					&nbsp;&nbsp;Habilitado </b>		
				</td>
				<td align="left">
					<input type="checkbox" name="chkAbil" value=1 <%=ia%>>
				</td>
		   </tr>    
	</table>
<%else%>
	<input type="hidden" name="lgpw" value="N">
	<input type="hidden" name="txtLogin" value="">
<%end if%>						
	<br>     
		<%
	
		'Response.write "Session_Creator: " & Session_Creator
		'Response.Write "<br>"
		'Response.write "IdCimpego: " & IdCimpego
		'Response.Write "<br>"
		'Response.write "sCodSede: " & sCodSede
		'Response.Write "<br>"
		'Response.write "sCodImp: " & sCodImp
		'Response.Write "<br>"
		'Response.write "miaimpresa: " & miaimpresa

		
		
		%>
		
		
	<table border="0" cellpadding="0" cellspacing="0" width="500" align="center">
		<tr>
			<td align="right">
				<a href="javascript:history.back()"><img src="<%=Session("progetto")%>/images/Indietro.gif" border="0"></a>
			</td>
			<td width="5%"></td>
			<td align="left" width="250">
				<% 
				'oculta el boton de enviar si la oficina 
				'asignada es distinta a la del operador        RIC 19-9-06
				' se � direttamente l'impresa a gestire i propri dati il bottone viene abilitato
				If (cstr(Session_Creator) = cstr(IdCimpego)) or (trim(sCodImp) = trim(miaimpresa)) then
					%>
					<%if trim(lg)<>"" then%> 
						<input type="hidden" name="txtLogin" value="<%=lg%>">
					<%end if%>
					<input type="image" name="Invia" src="<%=Session("Progetto")%>/images/conferma.gif" border="0" value="Conferma" onclick="return ControllaDati()">
					<%				
				end if 
				%>
			</td>
		</tr>
	</table>
	<!--
    <table border="0" width="500" cellpadding="1" cellspacing="1">   

    	<tr height="17"> 
			<td align="middle" class="tbltext3"><b>Ya se ha enviado el usuario y contrase�a a la empresa.</b>
			</td>
		</tr>
	</table><br>
	
	<table WIDTH="500" BORDER="0" CELLPADDING="0" CELLSPACING="0">
					<tr align="center">
						<td>
							<a href="javascript:history.back()"><img src="<%=Session("progetto")%>/images/Indietro.gif" border="0"></a>
						</td>
					</tr>
				</table>
				
	-->				
<%
' end if 
%>

</form>

<!--#include Virtual = "/include/closeconn.asp"-->
<!--#include Virtual = "/strutt_coda2.asp"-->
