<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #include Virtual="/strutt_testa2.asp" -->
<!-- #include virtual="/include/OpenConn.asp" -->
<!-- #include virtual="/include/DecCod.asp" -->

<!--*************************** INCLUDE BASE 64 FUNCTION ****************** -->
<!-- #include virtual="/include/Base64.asp" -->
<!--*********************************************************************** -->

<!--*********************************************************************** -->
<!--Busca a la persona o grupo que le queremos enviar un mensaje ' RKL-->
<!--***********************************************************************-->
<%
	'@LANGUAGE="VBSCRIPT" CODEPAGE="65001" 
	'Session.Codepage=65001
	'Response.ContentType = "text/HTML" 
	'Response.CodePage = 65001
	'Response.CharSet = "utf-8"


	'****************************************************************************************************
	'Cargando data Grupo Ocupacional
	'sSQLOcupationalGroups = "SELECT ID_AREAPROF, DENOMINAZIONE FROM AREE_PROFESSIONALI ORDER BY DENOMINAZIONE ASC"
	'set rsOcupationalGroups = CC.Execute(sSQLOcupationalGroups)
	'****************************************************************************************************	
	'Consulta Global
			
	'sSQLExist1 = "SELECT TI.ID_PERSONA, MAX(TITAD.VALORE) VALORE INTO ##TIMAS9870 " &_
	''			"FROM TISTUD TI with(nolock) " &_
	''			"INNER JOIN (SELECT * FROM TADES with(nolock)  WHERE NOME_TABELLA= 'LSTUD') TITAD " &_
     ''    				"ON TI.COD_LIV_STUD = TITAD.CODICE " &_
 	''			"GROUP BY TI.ID_PERSONA"
 	'set rsRegisteredPeople1 = CC.Execute(sSQLExist1)


	'sSQLExist2 = "SELECT P.ID_PERSONA, P.SESSO, ISNULL(TIMAS.VALORE,'ND') AS VALORE INTO ##CURSON49857 " &_
	''			"FROM PERSONA P with(nolock) LEFT JOIN  ##TIMAS9870 TIMAS ON P.ID_PERSONA = TIMAS.ID_PERSONA " &_
	''			"WHERE P.ID_PERSONA IN (SELECT DISTINCT ID_PERSONA FROM STATO_OCCUPAZIONALE with(nolock) WHERE " &_  
	''				"ID_CIMPIEGO = 211263) " &_
	''			"ORDER BY P.ID_PERSONA "
	'Set rsRegisteredPeople2 = CC.Execute(sSQLExist2)

	'sSQLExist3 = "SELECT TI.ID_PERSONA, MAX(TITAD.VALORE) VALORE INTO ##TIMAS9870 " &_
	'			"FROM TISTUD TI with(nolock) " &_
	'			"INNER JOIN (SELECT * FROM TADES with(nolock)  WHERE NOME_TABELLA= 'LSTUD') TITAD " &_
    '    			"ON TI.COD_LIV_STUD = TITAD.CODICE " &_
 	'			"GROUP BY TI.ID_PERSONA " 
 	'set rsRegisteredPeople3 = CC.Execute(sSQLExist3)
 
	'sSQLExist4 = "SELECT P.ID_PERSONA, P.SESSO, ISNULL(TIMAS.VALORE,'ND') AS VALORE INTO ##CURSON49857 " &_
	'			"FROM PERSONA P with(nolock) LEFT JOIN  ##TIMAS9870 TIMAS " &_
    '            	"ON P.ID_PERSONA = TIMAS.ID_PERSONA " &_
	'			"WHERE P.ID_PERSONA IN (SELECT DISTINCT ID_PERSONA FROM STATO_OCCUPAZIONALE with(nolock) WHERE ID_CIMPIEGO is not null) " &_
	'			"ORDER BY P.ID_PERSONA "
	'set rsRegisteredPeople4 = CC.Execute(sSQLExist4)


	centro = "" 
	if request("centro_atencion")="" then 
		centro = session("creator") 
	else 
		centro = request("centro_atencion")
	end if		


	'sSQLExist5 = "SELECT p.ID_PERSONA, " &_
	''					"p.COD_FISC AS 'IDENTIFICACION', " &_
	''					"replace(p.NOME,'&#209;','�') AS 'PRIMER NOMBRE', " &_
	''					"replace(p.SECONDO_NOME,'&#209;','�') AS 'SEGUNDO NOMBRE', " &_
	''					"replace(p.COGNOME,'&#209;','�') AS 'PRIMER APELLIDO', " &_
	''					"replace(p.SECONDO_COGNOME,'&#209;','�') AS 'SEGUNDO APELLIDO', " &_
	''					"floor(datediff(MONTH, p.DT_NASC, getdate()) / 12) as 'EDAD', " &_
	''					"case when p.SESSO='F' then 'FEMENINO' else 'MASCULINO' end as 'SEXO', " &_
	''					"dep.DESCRIZIONE AS 'DEPARTAMENTO DE RESIDENCIA', " &_
	''					"Ciudad.DESCOM AS 'CIUDAD DE RESIDENCIA', " &_
	''					"p.FRAZIONE_RES AS 'BARRIO DE RESIDENCIA', " &_
	''					"P.NUM_TEL AS 'TELEFONO FIJO', " &_
	''					"P.NUM_TEL_DOM AS 'TELEFONO CELULAR', " &_
	''					"P.E_MAIL AS 'CORREO ELECTRONICO', " &_
	''					"sim.DESCRIZIONE 'CENTRO DE ATENCION', " &_
	''					"TITAD.DESCRIZIONE 'MAXIMO NIVEL EDUCATIVO', " &_
	''					"t.DESCRIZIONE as 'SITUACION LABORAL ACTUAL',  " &_
	''					"min(convert(varchar(25), SO.DT_DICH_STATUS, 103)) AS 'FECHA DE INSCRIPCION', " &_
	''					"floor(DATEDIFF(day,SO.DT_DICH_STATUS,GETDATE())) AS 'DIAS DE INSCRIPCION', " &_
	''					"isnull(DER1.CUENTA,0) as 'INTERMEDIACION', " &_
	''					"isnull(DER2.CUENTA,0) as 'EXTERNAS - CAPACITACION', " &_
	''					"isnull(DER4.CUENTA,0) as 'INTERNAS - ENTREVISTA DE ORIENTACION INDIVIDUAL', " &_
	''					"isnull(DER6.CUENTA,0) as 'INTERNAS - TALLERES', " &_
	''					"isnull(DER8.CUENTA,0) as 'EXTERNAS - SERVICIOS DE SALUD', " &_
	''					"isnull(DER11.CUENTA,0) as 'EXTERNAS - OTROS PROGRAMAS SOCIALES' " &_
	''					"FROM stato_occupazionale so " &_
	''					"INNER join (SELECT ID_PERSONA, MAX(DT_DICH_STATUS) DT_DICH_STATUS FROM STATO_OCCUPAZIONALE WITH (NOLOCK) GROUP BY ID_PERSONA ) soUNI " &_
	''					"ON SO.ID_PERSONA = soUNI.ID_PERSONA " &_
	''					"AND SO.DT_DICH_STATUS = SOUNI.DT_DICH_STATUS " &_
	''					"RIGHT JOIN persona p with(nolock) " &_
	''					"on so.id_persona = p.id_persona " &_
	''					"left join tades t with(nolock)  " &_
	''					"on so.cod_stdis=t.CODICE and NOME_TABELLA ='STDIS'  " &_
	''					"left join SEDE_IMPRESA sim with(nolock) " &_
	''					"on sim.id_sede = so.ID_CIMPIEGO " &_
	''					"left join (select CODICE, DESCRIZIONE from tades with(nolock) where nome_tabella = 'PROV') Dep " &_
	''					"on p.PRV_RES =dep.CODICE " &_
	''					"left join (SELECT CODCOM,DESCOM from COMUNE with(nolock) ) Ciudad " &_
	''					"on p.COM_RES=Ciudad.CODCOM " &_
	''					"left join (SELECT pe.ID_PERSONA, pe.sesso, ISNULL(y.VALORE,'ND') as valore " &_
	''					"		FROM PERSONA pe with(nolock) " &_
	''					"		LEFT JOIN (select TI.ID_PERSONA, MAX(TITAD.VALORE) VALORE " &_
	''					"			from TISTUD TI with(nolock) " &_
	''					" 			INNER JOIN (SELECT * FROM TADES with(nolock)  WHERE NOME_TABELLA= 'LSTUD') TITAD " &_
	''					"   		ON TI.COD_LIV_STUD = TITAD.CODICE " &_
	''					"			GROUP BY TI.ID_PERSONA ) as y " &_
	''					"			on pe.id_persona = y.ID_PERSONA " &_
	''					"			WHERE pe.ID_PERSONA IN (SELECT DISTINCT f.ID_PERSONA FROM STATO_OCCUPAZIONALE f with(nolock) WHERE f.ID_CIMPIEGO is not null) " &_
	''					"			) C  " &_
	''					"on C.ID_PERSONA = p.ID_PERSONA  " &_
	''					"LEFT JOIN (SELECT ID_PERSONA, COD_DERIV, COUNT(ID_PERSONA) AS CUENTA FROM dbo.PERS_DERIVACION PEDE WHERE COD_DERIV = ''DER1' GROUP BY ID_PERSONA, COD_DERIV) der1 ON P.ID_PERSONA = der1.ID_PERSONA " &_
	''					"LEFT JOIN (SELECT ID_PERSONA, COD_DERIV, COUNT(ID_PERSONA) AS CUENTA FROM dbo.PERS_DERIVACION PEDE WHERE COD_DERIV = ''DER2' GROUP BY ID_PERSONA, COD_DERIV) DER2 ON P.ID_PERSONA = DER2.ID_PERSONA " &_
	''					"LEFT JOIN (SELECT ID_PERSONA, COD_DERIV, COUNT(ID_PERSONA) AS CUENTA FROM dbo.PERS_DERIVACION PEDE WHERE COD_DERIV = ''DER4' GROUP BY ID_PERSONA, COD_DERIV) DER4 ON P.ID_PERSONA = DER4.ID_PERSONA " &_
	''					"LEFT JOIN (SELECT ID_PERSONA, COD_DERIV, COUNT(ID_PERSONA) AS CUENTA FROM dbo.PERS_DERIVACION PEDE WHERE COD_DERIV = 'DER6' GROUP BY ID_PERSONA, COD_DERIV) DER6 ON P.ID_PERSONA = DER6.ID_PERSONA " &_
	''					"LEFT JOIN (SELECT ID_PERSONA, COD_DERIV, COUNT(ID_PERSONA) AS CUENTA FROM dbo.PERS_DERIVACION PEDE WHERE COD_DERIV = 'DER8' GROUP BY ID_PERSONA, COD_DERIV) DER8 ON P.ID_PERSONA = DER8.ID_PERSONA " &_
	''					"LEFT JOIN (SELECT ID_PERSONA, COD_DERIV, COUNT(ID_PERSONA) AS CUENTA FROM dbo.PERS_DERIVACION PEDE WHERE COD_DERIV = ''DER11' GROUP BY ID_PERSONA, COD_DERIV) DER11 ON P.ID_PERSONA = DER11.ID_PERSONA " &_
	''					"INNER JOIN (SELECT VALORE, DESCRIZIONE FROM TADES with(nolock) WHERE NOME_TABELLA= 'LSTUD' UNION( SELECT 'ND', 'No Disponible')) TITAD on c.valore = TITAD.valore  " 
	''				

	''					sSQLExist5 = sSQLExist5 & "WHERE so.id_cimpiego =" & centro & " " &_
	''					"AND SO.DT_DICH_STATUS >= convert(datetime,'01/05/2013 10:0:0',103) " &_
	''					"group BY p.ID_PERSONA , p.COD_FISC,p.NOME, p.SECONDO_NOME, p.COGNOME, p.SECONDO_COGNOME, floor(datediff(MONTH, p.'DT_NASC, getdate()) / 12),p.SESSO,dep.DESCRIZIONE,Ciudad.DESCOM,p.FRAZIONE_RES,P.NUM_TEL,P.NUM_TEL_DOM, P.E_MAIL, sim.DESCRIZIONE,TITAD.'DESCRIZIONE,t.DESCRIZIONE,SO.DT_DICH_STATUS,DER1.CUENTA, DER2.CUENTA, DER4.CUENTA, DER6.CUENTA, DER8.CUENTA, DER11.CUENTA,SO.'DT_DICH_STATUS  " &_
	''			"ORDER BY 'PRIMER NOMBRE', 'SEGUNDO NOMBRE', 'PRIMER APELLIDO', 'SEGUNDO APELLIDO' " 
					

	''			sSQLExist = "SET NOCOUNT ON; " & sSQLExist5

				sSQLExist = "set nocount on; select * from personasRegistradas where id_cimpiego = " & centro
				sSQLExist = sSQLExist & " ORDER BY 'PRIMER NOMBRE', 'SEGUNDO NOMBRE', 'PRIMER APELLIDO', 'SEGUNDO APELLIDO' " 

				'Response.Write sSQLExist
				
				
	'sSQLExist6 = "EXEC [dbo].[reportePersonasInscritas] " &_
	''				"SELECT	@return_value"
		set rsRegisteredPeople = CC.Execute(sSQLExist)

	
	'rsRegisteredPeople.CursorType = adOpenstatic 


	if request("search") = "search" then
		session("pagina") = 1
	end if
	rsRegisteredPeople.pagesize=10

	if rsRegisteredPeople.pagecount <> 0 then
		if session("pagina")>rsRegisteredPeople.pagecount then
				session("pagina")=rsRegisteredPeople.pagecount 'evitamos el error de reload
		end if

		' controlando avance de pagina
		
		dim direccion 
		'if request("direccion") = "" then		
	''		direccion = "Atras"
	''	else
			direccion = request("direccion")
	''	end if

		
		if session("pagina") = "" then
			session("pagina") = 1
		else

			if direccion = "Adelante" and session("pagina")<rsRegisteredPeople.pagecount then
				session("pagina") = session("pagina") + 1
			else 
				if direccion = "Atras" and session("pagina")>1 then
					session("pagina") = session("pagina") - 1
				end if	 
			end if	 
		end if

		if session("pagina")<>"" then
			rsRegisteredPeople.absolutepage=session("pagina")
		else 
			rsRegisteredPeople.absolutepage=1
		end if

		inicio=1+(session("pagina")-1)*rsRegisteredPeople.pagesize
		fin=inicio+rsRegisteredPeople.pagesize - 1
		if fin > rsRegisteredPeople.recordcount then
			fin =rsRegisteredPeople.recordcount
		end if

	end if 

	
	function roundUp(x)
	   If x > Int(x) then
	    roundup = Int(x) + 1
	   Else
	    roundup = x
	   End If
	End Function

%>

<script language="Javascript"> 
$(function(){

	$("#form_next").submit(function(){
		$("#centro_atencion_next").val($("#centro_atencion").val());
		return true;
	});


	$("#next").click(function(){
		$("#form_next").submit();		
	});

	$("#form_back").submit(function(){
		$("#centro_atencion_back").val($("#centro_atencion").val());
		return true;
	});
	

	$("#back").click(function(){
		$("#form_back").submit();
	});	


});
</script>


<form name="cerca" id="form_search" action="reportRegisteredPeople.asp" Method="Post">
	</br>
	<table border="0" width="90%" CELLPADDING="0" cellspacing="0">
		<tr>
			<td align="left" class="sfondomenu" width="199">
				<span class="tbltext0">
				<b>&nbsp;REPORTE DE PERSONAS INSCRITAS</b></span>
			</td>
			<td width="25" valign="bottom" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">
				<img border="0" src="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
			<td width="30%" valign="middle" align="right" class="tbltext1" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
		</tr>
	</table>
		
	<table border="0" width="90%" CELLPADDING="0" cellspacing="0">
	    <tr>
			<td class="sfondocomm" align="left" class="tbltext1">
			En esta secci&oacute;n se puede efectuar una b&uacute;squeda de personas inscritas.</br></br>
			</td>
			<td class="sfondocomm">
				<a href="Javascript:Show_Help('/Pgm/Help/ProjectSite/Rubrica/default/')">
				<img src="<%=Session("Progetto")%>/images/Help.gif" border="0" align="right" alt="Help">
			</td> 
	    </tr>

		<tr height="2">
			<td class="sfondocomm" width="100%" colspan="2" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>
	<table width="400" border="0" class="sfondocomm">
		<tr class="sfondocomm">
			<td colspan="2"><b>B&Uacute;SQUEDA DE INSCRITOS</b></td>		
		</tr>
		<tr style="text-align:center;">
			<td class="tbltext1" width="20%"colspan="2"><b>Centro de atenci&oacute;n</b></td>
			<td rowspan="4" width="20%">
		   		<input type="image" alt="Cerca" src="<%=Session("Progetto")%>/images/lente.gif" id="search" name="search" value="search">
			</td>
		</tr>
		<tr style="text-align:center;">
			<td class="tbltext1" colspan = "2">				
				<% sSQLAttentionCenter = "select SI.ID_IMPRESA, SI.ID_SEDE, I.RAG_SOC, SI.DESCRIZIONE AS centro " &_ 
						"from SEDE_IMPRESA SI " &_ 
						"inner join IMPRESA I ON I.ID_IMPRESA = SI.ID_IMPRESA " &_ 
						"where COD_TIMPR = '03' " %>
				<%	set rsAttentionCenter = CC.Execute(sSQLAttentionCenter) %>
			
				<select name="centro_atencion" id="centro_atencion" Class="textblack">
				<% contador=0 %>
				<!--option value=""></option-->
				<%
					Do While contador < rsAttentionCenter.RecordCount and NOT rsAttentionCenter.EOF%>
					  <% 'if request("centro_atencion")<>"" and trim(request("centro_atencion"))=cstr(rsAttentionCenter("id_sede")) then

					  if trim(centro)=cstr(rsAttentionCenter("id_sede")) then %>
						<option value=<%=rsAttentionCenter("id_sede")%> selected><%= rsAttentionCenter("centro")%></option>
					  <% else %>
					  	<option value=<%=rsAttentionCenter("id_sede")%> ><%= rsAttentionCenter("centro")%></option>
					<% end if %>
					<%
					rsAttentionCenter.MoveNext
					contador=contador+1
					Loop
				%>
				</select>
			</td>
			<TD></TD>
		</tr>
	</table>	
</form>
</br>


<% if not rsRegisteredPeople.EOF then %>
		</br></br>
		<table border="0" width="90%">
			<tr class="tbltext0 tblsfondo3" style="text-align:center;">
				<td width="25%" style="border-radius:10px 0 0 0;"><b>Identificaci&oacute;n</b></td>
				<td width="35%"><b>Nombre</b></td>
				<td width="25%"><b>Departamento Residencia</b></td>
				<td width="25%"><b>Ciudad Residencia</b></td>
				<td width="15%" style="border-radius:0 10px 0 0;"><b>Fecha de Inscripci&oacute;n</b></td>
			</tr>
		<!-- montamos el bucle para mostrar los registros -->
		<% contador=0

		Do While contador < rsRegisteredPeople.pagesize and NOT rsRegisteredPeople.EOF%>
			<tr class="tblsfondo">
				<td style="text-align:left; padding-left: 10px;"><%= rsRegisteredPeople("IDENTIFICACION")%></td>
				<td style="padding-left: 10px;"><%= rsRegisteredPeople("PRIMER NOMBRE") & " " & rsRegisteredPeople("SEGUNDO NOMBRE") & " " & rsRegisteredPeople("PRIMER APELLIDO") & " " & rsRegisteredPeople("SEGUNDO APELLIDO") %></td>
				<td style="text-align:center;"><%= rsRegisteredPeople("DEPARTAMENTO DE RESIDENCIA") %></td>
				<td style="text-align:center;"><%= rsRegisteredPeople("CIUDAD DE RESIDENCIA") %></td>
				<td style=" text-align:center;"><%= rsRegisteredPeople("FECHA DE INSCRIPCION") %></td>
			</tr>

		<% rsRegisteredPeople.MoveNext
		contador=contador+1
		Loop
		%>

		</table>

	<!-- mostramos los botones de adelante y atras segun proceda -->
	
	<table border="0" width="90%" style="margin:auto;">
		<tr class="tbltext0 tblsfondo3">
			<td width="10px" style="text-align:left; border-radius:0 0 0 10px; padding-left:10px;">
				<%if session("pagina") <> 1 then %>
					<form method="POST" action="reportRegisteredPeople.asp" id="form_back">
						<p>
							<a href="#" id="back">
								<img type="image" src="../../images/previous.png" width="24px" height="24px" style="border:0px;">
								<input type="hidden" alt="Atras" value="Atras" src="../../images/previous.png" id="imageBack" name="direccion" >
							</a>
							<input type="hidden" Class="textblack" id="centro_atencion_back" name="centro_atencion" value="<%=request("centro_atencion")%>" maxlength="50" />

						</p>
					</form>
				<%end if%>
			</td>
			<td width="40%" style="text-align:left; padding-left:10px;">Registros 
				<span style="color:#FF0000;"><%=inicio%></span> al 
				<span style="color:#FF0000;"><%=fin%></span> de un total de
				<span style="color:#FF0000;"><%=rsRegisteredPeople.recordcount%></span>
			</td>
			<td width="40%" style="text-align:right; padding-right:10px;">P&aacute;gina 
				<span style="color:#FF0000;"><%=session("pagina")%></span> de 
				<span style="color:#FF0000;"><%=rsRegisteredPeople.pagecount%></span>
			</td>
			<td width="10%" style="text-align:right;border-radius:0 0 10px 0; padding-right:10px;">
				<% if session("pagina") < rsRegisteredPeople.pagecount then %>
					<form method="POST" name="reportRegisteredPeople.asp" id="form_next">
						<p>
							<a href="#" id="next">
								<img type="image" src="../../images/next.png" width="24px" height="24px" style="border:0px;">
								<input type="hidden" alt="Adelante" value="Adelante" src="../../images/next.png" id="imageNext" name="direccion" >
							</a>
							<input type="hidden" Class="textblack" id="centro_atencion_next" name="centro_atencion" value="<%=request("centro_atencion")%>" maxlength="50" />

						</p>
					</form>
				<%end if%>
			</td>
		</tr>
	</table>

	<%
	'******************************************************* EXPORTAR A EXCEL *******************************************************
	Dim encabezadosExcel 
	encabezadosExcel = "IDENTIFICACI�N,PRIMER NOMBRE,SEGUNDO NOMBRE,PRIMER APELLIDO,SEGUNDO APELLIDO,EDAD,SEXO,DEPARTAMENTO DE RESIDENCIA,CIUDAD DE RESIDENCIA,BARRIO DE RESIDENCIA,TEL�FONO FIJO,TEL�FONO CELULAR,DIRECCI�N RESIDENCIA,CORREO ELECTR�NICO,CENTRO DE ATENCI�N,M�XIMO NIVEL EDUCATIVO,TITULO MAXIMO NIVEL EDUCATIVO,SITUACI�N LABORAL ACTUAL,FECHA DE INSCRIPCI�N,D�AS DE INSCRIPCI�N, PERFIL LABORAL"
	encabezadosExcelBase64 = Base64Encode(encabezadosExcel)

	Dim encabezadosQuery 
	encabezadosQuery = "IDENTIFICACION,PRIMER NOMBRE,SEGUNDO NOMBRE,PRIMER APELLIDO,SEGUNDO APELLIDO,EDAD,SEXO,DEPARTAMENTO DE RESIDENCIA,CIUDAD DE RESIDENCIA,BARRIO DE RESIDENCIA,TELEFONO FIJO,TELEFONO CELULAR,DIRECCION RESIDENCIA,CORREO ELECTRONICO,CENTRO DE ATENCION,MAXIMO NIVEL EDUCATIVO,TITULO MAXIMO NIVEL EDUCATIVO,SITUACION LABORAL ACTUAL,FECHA DE INSCRIPCION,DIAS DE INSCRIPCION,PERFIL"
	encabezadosQueryBase64 = Base64Encode(encabezadosQuery)

	Dim reportFilterData 
		reportFilterData = "centro_atencion:"&centro
		reportFilterDataBase64 = Base64Encode(reportFilterData)
	
	'********************************************************************************************************************************
	%>
	<!-- input type="button" id="exporttoExcel" value="Exportar a Excel" onclick="javascript:exporttoExcel()" -->
	<!-- exporttoExcelGeneric(reportName,reportTitle,reportFilterDataBase64,encabezadosExcelBase64,encabezadosQueryBase64) -->
	<input type="button" value="Exportar a Excel" onclick="javascript:exporttoExcelGeneric('reportRegisteredPeopleByCenter','REPORTE DE PERSONAS INSCRITAS','<%=reportFilterDataBase64%>','<%=encabezadosExcelBase64%>','<%=encabezadosQueryBase64%>')">



<% else%>
	<h1 class="tbltext0 tblsfondo3" style="width:80%; border-radius:10px; font-size:20px;">No se encontraron coincidencias</h1>
<% end if %>

<!-- #include Virtual="/strutt_coda2.asp" -->
<!-- #include virtual="/include/CloseConn.asp" -->