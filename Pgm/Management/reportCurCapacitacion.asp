<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #include Virtual="/strutt_testa2.asp" -->
<!-- #include virtual="/include/OpenConn.asp" -->
<!-- #include virtual="/include/DecCod.asp" -->

<!--*************************** INCLUDE BASE 64 FUNCTION ****************** -->
<!-- #include virtual="/include/Base64.asp" -->
<!--*********************************************************************** -->

<!--*********************************************************************** -->
<!--Busca a la persona o grupo que le queremos enviar un mensaje ' RKL-->
<!--***********************************************************************-->
<%
	'@LANGUAGE="VBSCRIPT" CODEPAGE="65001" 
	'Session.Codepage=65001
	'Response.ContentType = "text/HTML" 
	'Response.CodePage = 65001
	'Response.CharSet = "utf-8"


	'****************************************************************************************************
	'Cargando data Grupo Ocupacional
	'sSQLOcupationalGroups = "SELECT ID_AREAPROF, DENOMINAZIONE FROM AREE_PROFESSIONALI ORDER BY DENOMINAZIONE ASC"
	'set rsOcupationalGroups = CC.Execute(sSQLOcupationalGroups)
	'****************************************************************************************************

	ban = false
	sSQLTotal = "select count(1) totalcursos " &_
				"from persona p left join " &_
				"formazione f on p.id_persona = f.id_persona left join " &_
				"area_corso a on f.id_area = a.id_area left join " &_
			    "(select CODICE, DESCRIZIONE from tades where nome_tabella = 'PROV') dep on p.prv_res = Dep.CODICE left join " &_
			    "(SELECT CODCOM,DESCOM from COMUNE) as ciu ON p.com_res=ciu.CODCOM inner join " &_
				"(select id_persona, id_sede, max(dt_dich_status) rr from stato_occupazionale group by id_persona,id_sede) s on p.id_persona = s.id_persona "
				if request("tip_curso") <>"" then
					sSQLTotal = sSQLTotal & "where f.cod_att_finale = '" & request("tip_curso") & "' "
					ban = true
				end if
				if request("nombre") <>"" then
				  if ban then
					sSQLTotal = sSQLTotal & "AND f.cod_tcorso like '%" & request("nombre") & "%' "
				  else
				    sSQLTotal = sSQLTotal & "where f.cod_tcorso like '%" & request("nombre") & "%' "
					ban = true
				  end if
				end if
				if request("departamento") <>"" then
				  if ban then
					sSQLTotal = sSQLTotal & "AND p.prv_res = " & request("departamento") & " "
				  else
				    sSQLTotal = sSQLTotal & "where p.prv_res = " & request("departamento") & " "
					ban = true
				  end if
				end if
				if request("ciudad") <>"" then
					sSQLTotal = sSQLTotal & "AND p.com_res = '" & request("ciudad") & "' "
				end if
				if request("centro_atencion") <>"" then
				  if ban then
					sSQLTotal = sSQLTotal & "AND s.id_sede = '" & request("centro_atencion") & "' "
				  else 
				    sSQLTotal = sSQLTotal & "where s.id_sede = '" & request("centro_atencion") & "' "
				  end if
				end if
				
	'Response.Write 		sSQLTotal	&"<br/><br/>"
	set rsTotal = CC.Execute(sSQLTotal)
	
	ban = false

	'Consulta Global
	sSQLExist = "select p.cod_fisc identificacion, " &_
			    "p.cognome  apellido, " &_
			    "p.nome  nombre, " &_
			    "CONVERT(VARCHAR(23), p.dt_nasc, 103) fecha_nacimiento, " &_
			    "case when p.sesso='M' then 'Masculino' else 'Femenino' end  sexo, " &_
			    "dep.descrizione departamento, " &_
			    "ciu.descom ciudad, " &_
			    "num_tel, " &_
			    "isnull(p.e_mail,'No registra') e_mail, " &_
			    "isnull(a.DENOMINAZIONE,'No registra') area_conocimiento, " &_
			    "isnull(f.cod_tcorso,'No registra') nombre_curso, " &_
			    "case when f.cod_status_corso=0 then 'Finalizado' when f.cod_status_corso=1 then 'Incompleto' when f.cod_status_corso=2 then 'En curso' else 'No registra' end estado, " &_
			    "case when f.cod_att_finale='N' then 'Diplomado' when f.cod_att_finale='C' then 'Seminario' when f.cod_att_finale='F' then 'Curso' when f.cod_att_finale='Q' then 'Otros' when f.cod_att_finale='N' then 'Diplomado' when f.cod_att_finale='S' then 'Taller' when f.cod_att_finale='T' then 'Certificaci�n' else 'No registra' end tipo_curso " &_
				"from persona p left join " &_
				"formazione f on p.id_persona = f.id_persona left join " &_
				"area_corso a on f.id_area = a.id_area left join " &_
			    "(select CODICE, DESCRIZIONE from tades where nome_tabella = 'PROV') dep on p.prv_res = Dep.CODICE left join " &_
			    "(SELECT CODCOM,DESCOM from COMUNE) as ciu ON p.com_res=ciu.CODCOM inner join " &_
				"(select id_persona,id_sede, max(dt_dich_status) rr from stato_occupazionale group by id_persona,id_sede) s on p.id_persona = s.id_persona  "
				if request("tip_curso") <>"" then
					sSQLExist = sSQLExist & "where f.cod_att_finale = '" & request("tip_curso") & "' "
					ban = true
				end if
				if request("nombre") <>"" then
				  if ban then
					sSQLExist = sSQLExist & "AND f.cod_tcorso like '%" & request("nombre") & "%' "
				  else
				    sSQLExist = sSQLExist & "where f.cod_tcorso like '%" & request("nombre") & "%' "
					ban = true
				  end if
				end if
				if request("departamento") <>"" then
				  if ban then
					sSQLExist = sSQLExist & "AND p.prv_res = " & request("departamento") & " "
				  else
				    sSQLExist = sSQLExist & "where p.prv_res = " & request("departamento") & " "
					ban = true
				  end if
				end if
				if request("ciudad") <>"" then
					sSQLExist = sSQLExist & "AND p.com_res = '" & request("ciudad") & "' "
				end if
				if request("centro_atencion") <>"" then
				  if ban then
					sSQLExist = sSQLExist & "AND s.id_sede = '" & request("centro_atencion") & "' "
				  else 
				    sSQLExist = sSQLExist & "where s.id_sede = '" & request("centro_atencion") & "' "
				  end if
				end if
	sSQLExist = sSQLExist & "ORDER BY p.cognome"

	'response.WRITE sSQLExist
	'RESPONSE.end


	'response.Write "*********************************************************"
	'response.Write "NOMBRE EMRESA -- " & request("nombre_empresa") & "</br>"
	'response.Write "SEDE EMPRESA -- " & request("sede_empresa") & "</br>"
	'response.Write "DEPARTAMENTO -- " & request("departamento") & "</br>"
	'response.Write "CIUDAD -- " & request("ciudad") & "</br>"
	'response.Write "FECHA INSCRIPCI�N FROM -- " & request("fecha_inscripcion_from") & "</br>"
	'response.Write "FECHA INSCRIPCI�N TO -- " & request("fecha_inscripcion_to") & "</br>"
	'response.Write "*********************************************************"
		
	
	'rsCurCapacitacion.CursorType = adOpenstatic 
	set rsCurCapacitacion = CC.Execute(sSQLExist)

	if request("search") = "search" then
		session("pagina") = 1
	end if

	rsCurCapacitacion.pagesize=10

	if rsCurCapacitacion.pagecount <> 0 then
		if session("pagina")>rsCurCapacitacion.pagecount then
				session("pagina")=rsCurCapacitacion.pagecount 'evitamos el error de reload
		end if

		' controlando avance de pagina
		
		dim direccion 
		'if request("direccion") = "" then		
	''		direccion = "Atras"
	''	else
			direccion = request("direccion")
	''	end if

		
		if session("pagina") = "" then
			session("pagina") = 1
		else

			if direccion = "Adelante" and session("pagina")<rsCurCapacitacion.pagecount then
				session("pagina") = session("pagina") + 1
			else 
				if direccion = "Atras" and session("pagina")>1 then
					session("pagina") = session("pagina") - 1
				end if	 
			end if	 
		end if

		if session("pagina")<>"" then
			rsCurCapacitacion.absolutepage=session("pagina")
		else 
			rsCurCapacitacion.absolutepage=1
		end if

		inicio=1+(session("pagina")-1)*rsCurCapacitacion.pagesize
		fin=inicio+rsCurCapacitacion.pagesize - 1
		if fin > rsCurCapacitacion.recordcount then
			fin =rsCurCapacitacion.recordcount
		end if

	end if 

	
	function roundUp(x)
	   If x > Int(x) then
	    roundup = Int(x) + 1
	   Else
	    roundup = x
	   End If
	End Function

%>

<script language="Javascript"> 
$(function(){

	$("#form_next").submit(function(){
		$("#tip_curso_next").val($("#tip_curso").val());
		$("#nombre_next").val($("#nombre").val());
		$("#departamento_next").val($("#departamento").val());
		$("#ciudad_next").val($("#ciudad").val());
		$("#centro_atencion_next").val($("#centro_atencion").val());
		return true;
	});


	$("#next").click(function(){
		$("#form_next").submit();		
	});

	$("#form_back").submit(function(){
		$("#tip_curso_back").val($("#tip_curso").val());
		$("#nombre_back").val($("#nombre").val());
		$("#departamento_back").val($("#departamento").val());
		$("#ciudad_back").val($("#ciudad").val());		
		$("#centro_atencion_back").val($("#centro_atencion").val());		
		return true;
	});
	

	$("#back").click(function(){
		$("#form_back").submit();
	});	

	$("#cmbDepartamento").change(function() {
		idDepartment = $("#cmbDepartamento").val();

		var cmbDepartamento = $("#cmbDepartamento").val();
		$("#departamento").val(cmbDepartamento); 

		$.post('loadCitiesbyDeparment.asp', {"idDepartament":idDepartment}, function(data) {
  			process(data);
		}).error(function(e){
			//console.log("Falla!!!")
			//console.log(e)
			if(e.status == 200)
			{
				process(e.responseText)
			}
		});
	});

	var departamento = $("#departamento").val();
	$("#cmbDepartamento").val(departamento);
	$("#cmbDepartamento").change();
	
	$("#cmbCiudad").change(function() {
		var cmbCiudad = $("#cmbCiudad").val();
		$("#ciudad").val(cmbCiudad); 
	});
	var ciudad = $("#ciudad").val();


	process = function(data){

		if(typeof(data) == "string")
			data = JSON.parse(data.substr(0,data.length-2)+"]")

		$("#cmbCiudad").empty();
		$("#cmbCiudad").append("<option value=''></option>");
		//$("#ciudad").val("");
		//$.each(data, function(index, value) {
		for(i in data){	
			if(i != "indexOf")
				$("#cmbCiudad").append("<option value="+data[i]["CODCOM"]+">"+data[i]["DESCOM"]+"</option>")
		}
		//);
		$("#cmbCiudad").val(ciudad);
	}



});
</script>




<form name="cerca" id="form_search" action="reportCurCapacitacion.asp" Method="Post">
	</br>
	<table border="0" width="700" CELLPADDING="0" cellspacing="0">
		<tr>
			<td align="left" class="sfondomenu" width="300">
				<span class="tbltext0">
				<b>&nbsp;REPORTE DE CURSOS DE CAPACITACI&Oacute;N</b></span>
			</td>
			<td width="25" valign="bottom" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">
				<img border="0" src="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
			<td width="40%" valign="middle" align="right" class="tbltext1" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
		</tr>
	</table>
		
	<table border="0" width="700" CELLPADDING="0" cellspacing="0">
	    <tr>
			<td class="sfondocomm" align="left" class="tbltext1">
			En esta secci&oacute;n se puede efectuar una b&uacute;squeda de los cursos de capacitaci&oacute;n.</br></br>
			<!-- Puede filtrar la b&uacute;squeda ingresando el apellido y nombre del usuario. -->
			</td>
			<td class="sfondocomm">
				<a href="Javascript:Show_Help('/Pgm/Help/ProjectSite/Rubrica/default/')">
				<img src="<%=Session("Progetto")%>/images/Help.gif" border="0" align="right" alt="Help">
			</td> 
	    </tr>

		<tr height="2">
			<td class="sfondocomm" width="100%" colspan="2" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>
	<table width="700" border="0" class="sfondocomm">
		<tr class="sfondocomm">
			<td colspan="2"><b>B&Uacute;SQUEDA DE CURSOS DE CAPACITACI&Oacute;N</b></td>		
		</tr>
		<tr style="text-align:center;">
			<td class="tbltext1" width="20%" colspan="2"><b>Tipo de curso</b></td>			
			<td class="tbltext1" width="20%"><b>Nombre de curso</b></td>
			<td class="tbltext1" width="20%"></td>
			<td width="20%" rowspan="2" style="vertical-align: middle; text-align: center; margin-left: auto; margin-right: auto; align: center;">
				<div style="margin:auto; border-radius:10px; padding-top:8px; padding-bottom:8px; width:80%; text-align:center;" class="tbltext0 tblsfondo3"><b>TOTAL CURSOS</b> </br> <% response.Write rsTotal("TotalCursos") %></div>
			</td>
		</tr>
		<tr style="text-align:center;">
			<td class="tbltext1" colspan="2">
				<% sqlTipDoc = "SELECT ID_CAMPO_DESC  FROM DIZ_DATI WHERE ID_TAB='FORMAZIONE' AND ID_CAMPO='COD_ATT_FINALE'" 
				   set rst = CC.Execute(sqlTipDoc)
				   info=Rst("ID_CAMPO_DESC")
				   cantidad=split(info,"|")  
				%>
				<select name="tip_curso" class="textblack" id="tip_curso">
					<option name=""/>
				<%	for i=0 to Ubound(cantidad) step 2
						Response.Write "<option value='" & cantidad(i) & "'"
						if cantidad(i)=request("tip_curso") then Response.Write " selected"
					    Response.Write ">" & Ucase((cantidad(i+1))) & "</option>"
					next
				%>	
				</select>
				<%	Rst.Close
					set Rst=nothing
				%>
				</select>
			</td>
			<td class="tbltext1">
				<input type="text" Class="textblack" id="nombre" name="nombre" value="<%=request("nombre")%>" maxlength="50" />
			</td>
			<td class="tbltext1"><td>
			<td></td>
		
		</tr>
		<tr style="text-align:center;">
			<td class="tbltext1" width="20%"><b>Departamento</b></td>
			<td class="tbltext1" width="20%"><b>Ciudad</b></td>
			<td class="tbltext1" width="30%"><b>Centro de atenci&oacute;n</b></td>
			<td></td>
			<td rowspan="2" width="20%">
		   		<input type="image" alt="Cerca" src="<%=Session("Progetto")%>/images/lente.gif" id="search" name="search" value="search">
			</td>
		</tr>
		<tr style="text-align:center;">
			<td class="tbltext1">
				<input type="hidden" Class="textblack" id="departamento" name="departamento" value="<%=request("departamento")%>" maxlength="50" />
				<%
			   	sInt = "PROV|0|" & date & "| |cmbDepartamento' |AND VALORE = 'CO' ORDER BY DESCRIZIONE"			
				CreateCombo(sInt)
				%>
			</td>
			<td class="tbltext1">
				<input type="hidden" Class="textblack" id="ciudad" name="ciudad" value="<%=request("ciudad")%>" maxlength="50" />
				<select class="textblack" id="cmbCiudad" name="cmbCiudad" style="width: 85%;">
				<option value=""></option>				
				</select>
			</td>
			<td class="tbltext1">				
				<select name="centro_atencion" id="centro_atencion" Class="textblack">
				<% contador=0 %>
				<% sSQLAttentionCenter = "select SI.ID_IMPRESA, SI.ID_SEDE, I.RAG_SOC, SI.DESCRIZIONE AS centro " &_ 
						"from SEDE_IMPRESA SI " &_ 
						"inner join IMPRESA I ON I.ID_IMPRESA = SI.ID_IMPRESA " &_ 
						"where COD_TIMPR = '03' " %>
				<%	set rsAttentionCenter = CC.Execute(sSQLAttentionCenter) %>
				<option value=""></option>
				<%
					Do While contador < rsAttentionCenter.pagesize and NOT rsAttentionCenter.EOF%>
					  <% if request("centro_atencion")<>"" and trim(request("centro_atencion"))=cstr(rsAttentionCenter("id_sede")) then %>
						<option value=<%=rsAttentionCenter("id_sede")%> selected><%= rsAttentionCenter("centro")%></option>
					  <% else %>
					  	<option value=<%=rsAttentionCenter("id_sede")%> ><%= rsAttentionCenter("centro")%></option>
					<% end if %>
					<%
					rsAttentionCenter.MoveNext
					contador=contador+1
					Loop
				%>
				</select>
			</td>
		</tr>		
	</table>	
</form>
</br>


<% if not rsCurCapacitacion.EOF then %>  
		</br></br>
		<table border="0" width="700">
			<tr class="tbltext0 tblsfondo3" style="text-align:center;">
				<td width="12%" style="border-radius:10px 0 0 0;"><b>No. Documento</b></td>
				<td width="12%"><b>Apellido</b></td>
				<td width="12%"><b>Nombre</b></td>
				<td width="12%"><b>Area de conocimiento</b></td>
				<td width="12%"><b>Nombre del curso</b></td>
				<td width="12%" style="border-radius:0 10px 0 0;"><b>Tipo de curso</b></td>
			</tr>
		<!-- montamos el bucle para mostrar los registros -->
		<% contador=0
		Do While contador < rsCurCapacitacion.pagesize and NOT rsCurCapacitacion.EOF%>
			<tr class="tblsfondo">
				<td style="text-align:center;"><%= rsCurCapacitacion("IDENTIFICACION")%></td>
				<td><%= rsCurCapacitacion("APELLIDO") %></td>
				<td><%= rsCurCapacitacion("NOMBRE") %></td>
				<td><%= rsCurCapacitacion("AREA_CONOCIMIENTO") %></td>
				<td><%= rsCurCapacitacion("NOMBRE_CURSO") %></td>
				<td><%= rsCurCapacitacion("TIPO_CURSO") %></td>
			</tr>

		<% rsCurCapacitacion.MoveNext
		contador=contador+1
		Loop
		%>

		</table>

	<!-- mostramos los botones de adelante y atras segun proceda -->
	<table border="0" width="100%" class="tbltext0 tblsfondo3" style="margin:auto;">
		<tr>
			<td width="10px" style="text-align:left;">
				<%if session("pagina") <> 1 then %>
					<form method="POST" action="reportCurCapacitacion.asp" id="form_back">
						<p>
							<a href="#" id="back">
								<img type="image" src="../../images/previous.png" width="24px" height="24px" style="border:0px;">
								<input type="hidden" alt="Atras" value="Atras" src="../../images/previous.png" id="imageBack" name="direccion" >
							</a>
							<!-- input type="image" id="back" alt="Atras" value="Atras" src="../../images/previous.png" id="imageBack" name="direccion" width="24px" height="24px" -->
							<input type="hidden" Class="textblack" id="tip_curso_back" name="tip_curso" value="<%=request("tip_curso")%>" maxlength="50" />
							<input type="hidden" Class="textblack" id="nombre_back" name="nombre" value="<%=request("nombre")%>" maxlength="50" style="width: 85%;" />
							<input type="hidden" Class="textblack" id="departamento_back" name="departamento" value="<%=request("departamento")%>" maxlength="50" style="width: 85%;" />
							<input type="hidden" Class="textblack" id="ciudad_back" name="ciudad" value="<%=request("ciudad")%>" maxlength="50" style="width: 85%;" />
							<input type="hidden" Class="textblack" id="centro_atencion_back" name="centro_atencion" value="<%=request("centro_atencion")%>" maxlength="50" style="width: 85%;" />
						</p>
					</form>
				<%end if%>
			</td>
			<td width="40%" style="text-align:left;">Registros 
				<span style="color:#FF0000;"><%=inicio%></span> al 
				<span style="color:#FF0000;"><%=fin%></span> de un total de
				<span style="color:#FF0000;"><%=rsCurCapacitacion.recordcount%></span>
			</td>
			<td width="40%" style="text-align:right;">P&aacute;gina 
				<span style="color:#FF0000;"><%=session("pagina")%></span> de 
				<span style="color:#FF0000;"><%=rsCurCapacitacion.pagecount%></span>
			</td>
			<td width="10%" style="text-align:right;">
				<% if session("pagina") < rsCurCapacitacion.pagecount then %>
					<form method="POST" name="reportCurCapacitacion.asp" id="form_next">
						<p>
							<a href="#" id="next">
								<img type="image" src="../../images/next.png" width="24px" height="24px" style="border:0px;">
								<input type="hidden" alt="Adelante" value="Adelante" src="../../images/next.png" id="imageNext" name="direccion" >
							</a>
							<!-- input type="image" alt="Adelante" value="Adelante" src="../../images/next.png" id="imageNext" name="direccion" width="24px" height="24px" -->

							<input type="hidden" Class="textblack" id="tip_curso_next" name="tip_curso" value="<%=request("tip_curso")%>" maxlength="50" />
							<input type="hidden" Class="textblack" id="nombre_next" name="nombre" value="<%=request("nombre")%>" maxlength="50" style="width: 85%;" />
							<input type="hidden" Class="textblack" id="departamento_next" name="departamento" value="<%=request("departamento")%>" maxlength="50" style="width: 85%;" />
							<input type="hidden" Class="textblack" id="ciudad_next" name="ciudad" value="<%=request("ciudad")%>" maxlength="50" style="width: 85%;" />
							<input type="hidden" Class="textblack" id="centro_atencion_next" name="centro_atencion" value="<%=request("centro_atencion")%>" maxlength="50" style="width: 85%;" />
						</p>
					</form>
				<%end if%>
			</td>
		</tr>
	</table>

	<%
		'******************************************************* EXPORTAR A EXCEL *******************************************************
		Dim encabezadosExcel 
		encabezadosExcel = "Documento,Apellido,Nombre,Fecha Nacimiento,Sexo,Departamento,Ciudad,Tel�fono,Correo Electr�nico,Area de conocimiento,Nombre del curso,Estado,Tipo de curso"
		encabezadosExcelBase64 = Base64Encode(encabezadosExcel)

		Dim encabezadosQuery 
		encabezadosQuery = "IDENTIFICACION,APELLIDO,NOMBRE,FECHA_NACIMIENTO,SEXO,DEPARTAMENTO,CIUDAD,NUM_TEL,E_MAIL,AREA_CONOCIMIENTO,NOMBRE_CURSO,ESTADO,TIPO_CURSO"

		encabezadosQueryBase64 = Base64Encode(encabezadosQuery)

		Dim reportFilterData 
		reportFilterData = "tip_curso:"&request("tip_curso")&",nombre:"&request("nombre")&",departamento:"&request("departamento")&",ciudad:"&request("ciudad")&",centro_atencion:"&request("centro_atencion")
		reportFilterDataBase64 = Base64Encode(reportFilterData)
		'********************************************************************************************************************************

	%>

	<input type="button" value="Exportar a Excel" onclick="javascript:exporttoExcelGeneric('reportCurCapacitacion','REPORTE DE CURSOS DE CAPACITACI�N','<%=reportFilterDataBase64%>','<%=encabezadosExcelBase64%>','<%=encabezadosQueryBase64%>')">


<% else%>
	<h1 class="tbltext0 tblsfondo3" style="width:80%; border-radius:10px; font-size:20px;">No se encontraron coincidencias</h1>
<% end if %>

<!-- #include Virtual="/strutt_coda2.asp" -->
<!-- #include virtual="/include/CloseConn.asp" -->



