<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #include Virtual="/strutt_testa2.asp" -->
<!-- #include virtual="/include/OpenConn.asp" -->
<!-- #include virtual="/include/DecCod.asp" -->

<!--*************************** INCLUDE BASE 64 FUNCTION ****************** -->
<!-- #include virtual="/include/Base64.asp" -->
<!--*********************************************************************** -->

<!--*********************************************************************** -->
<!--Busca a la persona o grupo que le queremos enviar un mensaje ' RKL-->
<!--***********************************************************************-->
<%
	'@LANGUAGE="VBSCRIPT" CODEPAGE="65001" 
	'Session.Codepage=65001
	'Response.ContentType = "text/HTML" 
	'Response.CodePage = 65001
	'Response.CharSet = "utf-8"


	'****************************************************************************************************
	'Cargando data Grupo Ocupacional
	'sSQLOcupationalGroups = "SELECT ID_AREAPROF, DENOMINAZIONE FROM AREE_PROFESSIONALI ORDER BY DENOMINAZIONE ASC"
	'set rsOcupationalGroups = CC.Execute(sSQLOcupationalGroups)
	'****************************************************************************************************

	ban = false
	
	sSQLTotal = "SELECT COUNT (COD_FISC) AS TotalEmpresas " &_
				" FROM ( select * " &_
				"from ( " &_
				"select id_persona, cod_fisc, cognome, nome, com_res, prv_res " &_
				"	from persona as a " &_
				"where not EXISTS ( select * " &_
				"													from STATO_OCCUPAZIONALE as u " &_
				"												 where u.id_persona = a.id_persona)) as per " &_
				"LEFT JOIN (select CODICE, DESCRIZIONE from tades where nome_tabella = 'PROV') as Dep ON per.prv_res = Dep.CODICE " &_
				"LEFT JOIN (SELECT CODCOM,DESCOM from COMUNE) as Ciudad ON per.com_res=Ciudad.CODCOM "
				if (request("tipo_documento") <> "" and request("doc_identidad") <> "") or request("nombre") <> "" or request("apellido") <> "" or request("departamento") <> "" or request("ciudad") <> "" then
					sSQLTotal = sSQLTotal & " where "
				end if
				if request("tipDoc") <> "" and request("doc_identidad") <> "" then
					sSQLTotal = sSQLTotal & "AND per.COD_FISC like'%" & request("tipo_documento")&request("doc_identidad") & "%' "
					ban = true
				end if
				if request("nombre") <>"" then
				  if ban then
					sSQLTotal = sSQLTotal & "AND per.nome LIKE '%" & request("nombre") & "%' "
				  else
				    sSQLTotal = sSQLTotal & " per.nome LIKE '%" & request("nombre") & "%' "
				  end if
				  ban = true
				end if
				if request("apellido") <>"" then
				  if ban then
					sSQLTotal = sSQLTotal & "AND per.cognome like '" & request("apellido") & "' "
				  else 
				    sSQLTotal = sSQLTotal & " per.cognome like '" & request("apellido") & "' "
			      end if
			      ban = true
				end if
				if request("departamento") <>"" then
				  if ban then
					sSQLTotal = sSQLTotal & "AND per.prv_res = " & request("departamento") & " "
				  else 
				    sSQLTotal = sSQLTotal & " per.prv_res = " & request("departamento") & " "
				  end if
				  ban = true
				end if
				if request("ciudad") <>"" then
				  if ban then 
					sSQLTotal = sSQLTotal & "AND per.com_res = '" & request("ciudad") & "' "
				  else 
				    sSQLTotal = sSQLTotal & " per.com_res = '" & request("ciudad") & "' "
				  end if
				end if
				sSQLTotal = sSQLTotal & " ) as b "
				
	set rsTotal = CC.Execute(sSQLTotal)

    ban = false
	
	'Consulta Global
	sSQLExist = "select cod_fisc, cognome, nome, isnull(DESCRIZIONE,'NO REGISTRA') DESCRIZIONE, ISNULL(DESCOM,'NO REGISTRA') DESCOM   " &_
				"from ( " &_
				"select id_persona, cod_fisc, cognome, nome, com_res, prv_res " &_
				"	from persona as a " &_
				"where not EXISTS ( select * " &_
				"													from STATO_OCCUPAZIONALE as u " &_
				"												 where u.id_persona = a.id_persona)) as per " &_
				"LEFT JOIN (select CODICE, DESCRIZIONE from tades where nome_tabella = 'PROV') as Dep ON per.prv_res = Dep.CODICE " &_
				"LEFT JOIN (SELECT CODCOM,DESCOM from COMUNE) as Ciudad ON per.com_res=Ciudad.CODCOM " 
				if (request("tipo_documento") <> "" and request("doc_identidad") <> "") or request("nombre") <> "" or request("apellido") <> "" or request("departamento") <> "" or request("ciudad") <> "" then
					sSQLExist = sSQLExist & " where "
				end if
				if request("tipDoc") <> "" and request("doc_identidad") <> "" then
				  if ban then
					sSQLExist = sSQLExist & "AND per.COD_FISC like'%" & request("tipo_documento")&request("doc_identidad") & "%' "
				  else 
				    sSQLExist = sSQLExist & " per.COD_FISC like'%" & request("tipo_documento")&request("doc_identidad") & "%' "
				  end if
				  ban = true
				end if
				if request("nombre") <>"" then
				  if ban then 
					sSQLExist = sSQLExist & "AND per.nome LIKE '%" & request("nombre") & "%' "
				  else 
				    sSQLExist = sSQLExist & " per.nome LIKE '%" & request("nombre") & "%' "
			      end if
				  ban = true
				end if
				if request("apellido") <>"" then
				  if ban then
					sSQLExist = sSQLExist & "AND per.cognome like '" & request("apellido") & "' "
				  else 
					sSQLExist = sSQLExist & " per.cognome like '" & request("apellido") & "' "
				  end if
				  ban = true
				end if
				if request("departamento") <>"" then
				  if ban then
					sSQLExist = sSQLExist & "AND per.prv_res = " & request("departamento") & " "
				  else 
				    sSQLExist = sSQLExist & " per.prv_res = " & request("departamento") & " "
				  end if
				  ban = true
				end if
				if request("ciudad") <>"" then
				  if ban then
					sSQLExist = sSQLExist & "AND per.com_res = '" & request("ciudad") & "' "
				  else 
				    sSQLExist = sSQLExist & " per.com_res = '" & request("ciudad") & "' "
				  end if
				end if
	sSQLExist = sSQLExist & "ORDER BY per.cognome"
	
	

	'response.Write "*********************************************************"
	'response.Write "NOMBRE EMRESA -- " & request("nombre_empresa") & "</br>"
	'response.Write "SEDE EMPRESA -- " & request("sede_empresa") & "</br>"
	'response.Write "DEPARTAMENTO -- " & request("departamento") & "</br>"
	'response.Write "CIUDAD -- " & request("ciudad") & "</br>"
	'response.Write "FECHA INSCRIPCIÓN FROM -- " & request("fecha_inscripcion_from") & "</br>"
	'response.Write "FECHA INSCRIPCIÓN TO -- " & request("fecha_inscripcion_to") & "</br>"
	'response.Write "*********************************************************"


				
	
	'rsOferenSinCentro.CursorType = adOpenstatic 
	set rsOferenSinCentro = CC.Execute(sSQLExist)

	if request("search") = "search" then
		session("pagina") = 1
	end if

	rsOferenSinCentro.pagesize=10

	if rsOferenSinCentro.pagecount <> 0 then
		if session("pagina")>rsOferenSinCentro.pagecount then
				session("pagina")=rsOferenSinCentro.pagecount 'evitamos el error de reload
		end if

		' controlando avance de pagina
		
		dim direccion 
		'if request("direccion") = "" then		
	''		direccion = "Atras"
	''	else
			direccion = request("direccion")
	''	end if

		
		if session("pagina") = "" then
			session("pagina") = 1
		else

			if direccion = "Adelante" and session("pagina")<rsOferenSinCentro.pagecount then
				session("pagina") = session("pagina") + 1
			else 
				if direccion = "Atras" and session("pagina")>1 then
					session("pagina") = session("pagina") - 1
				end if	 
			end if	 
		end if

		if session("pagina")<>"" then
			rsOferenSinCentro.absolutepage=session("pagina")
		else 
			rsOferenSinCentro.absolutepage=1
		end if

		inicio=1+(session("pagina")-1)*rsOferenSinCentro.pagesize
		fin=inicio+rsOferenSinCentro.pagesize - 1
		if fin > rsOferenSinCentro.recordcount then
			fin =rsOferenSinCentro.recordcount
		end if

	end if 

	
	function roundUp(x)
	   If x > Int(x) then
	    roundup = Int(x) + 1
	   Else
	    roundup = x
	   End If
	End Function

%>

<script language="Javascript"> 
$(function(){

	$("#form_next").submit(function(){
		$("#tipo_documento_next").val($("#tipo_documento").val());
		$("#documento_next").val($("#documento").val());
		$("#apellido_next").val($("#apellido").val());
		$("#nombre_next").val($("#nombre").val());
		$("#departamento_next").val($("#departamento").val());
		$("#ciudad_next").val($("#ciudad").val());
		return true;
	});


	$("#next").click(function(){
		$("#form_next").submit();		
	});

	$("#form_back").submit(function(){
		$("#tipo_documento_back").val($("#tipo_documento").val());
		$("#documento_back").val($("#documento").val());
		$("#apellido_back").val($("#apellido").val());
		$("#nombre_back").val($("#nombre").val());
		$("#departamento_back").val($("#departamento").val());
		$("#ciudad_back").val($("#ciudad").val());		
		return true;
	});

	$("#back").click(function(){
		$("#form_back").submit();
	});	


	$("#cmbDepartamento").change(function() {
		idDepartment = $("#cmbDepartamento").val();

		var cmbDepartamento = $("#cmbDepartamento").val();
		$("#departamento").val(cmbDepartamento); 

		$.post('loadCitiesbyDeparment.asp', {"idDepartament":idDepartment}, function(data) {
  			process(data);
		}).error(function(e){
			//console.log("Falla!!!")
			//console.log(e)
			if(e.status == 200)
			{
				process(e.responseText)
			}
		});
	});

	var departamento = $("#departamento").val();
	$("#cmbDepartamento").val(departamento);
	$("#cmbDepartamento").change();
	
	$("#cmbCiudad").change(function() {
		var cmbCiudad = $("#cmbCiudad").val();
		$("#ciudad").val(cmbCiudad); 
	});
	var ciudad = $("#ciudad").val();


	process = function(data){

		if(typeof(data) == "string")
			data = JSON.parse(data.substr(0,data.length-2)+"]")

		$("#cmbCiudad").empty();
		$("#cmbCiudad").append("<option value=''></option>");
		//$("#ciudad").val("");
		//$.each(data, function(index, value) {
		for(i in data){	
			if(i != "indexOf")
				$("#cmbCiudad").append("<option value="+data[i]["CODCOM"]+">"+data[i]["DESCOM"]+"</option>")
		}
		//);
		$("#cmbCiudad").val(ciudad);
	}



});
</script>




<form name="cerca" id="form_search" action="reportOferenSinCentro.asp" Method="Post">
	</br>
	<table border="0" width="700" CELLPADDING="0" cellspacing="0">
		<tr>
			<td align="left" class="sfondomenu" width="300">
				<span class="tbltext0">
				<b>&nbsp;REPORTE DE OFERENTES SIN CENTRO ASOCIADO</b></span>
			</td>
			<td width="25" valign="bottom" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">
				<img border="0" src="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
			<td width="40%" valign="middle" align="right" class="tbltext1" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
		</tr>
	</table>
		
	<table border="0" width="700" CELLPADDING="0" cellspacing="0">
	    <tr>
			<td class="sfondocomm" align="left" class="tbltext1">
			En esta secci&oacute;n se puede efectuar una b&uacute;squeda de los oferentes sin centro asociado.</br></br>
			<!-- Puede filtrar la b&uacute;squeda ingresando el apellido y nombre del usuario. -->
			</td>
			<td class="sfondocomm">
				<a href="Javascript:Show_Help('/Pgm/Help/ProjectSite/Rubrica/default/')">
				<img src="<%=Session("Progetto")%>/images/Help.gif" border="0" align="right" alt="Help">
			</td> 
	    </tr>

		<tr height="2">
			<td class="sfondocomm" width="100%" colspan="2" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>
	<table width="700" border="0" class="sfondocomm">
		<tr class="sfondocomm">
			<td colspan="2"><b>B&Uacute;SQUEDA DE OFERENTES SIN CENTRO ASOCIADO</b></td>		
		</tr>
		<tr style="text-align:center;">
			<td class="tbltext1" width="20%"><b>TIpo de documento</b></td>			
			<td class="tbltext1" width="20%"><b>No. Documento</b></td>
			<td width="20%" rowspan="2" style="vertical-align: middle; text-align: center; margin-left: auto; margin-right: auto; align: center;">
				<div style="margin:auto; border-radius:10px; padding-top:8px; padding-bottom:8px; width:80%; text-align:center;" class="tbltext0 tblsfondo3"><b>TOTAL OFERENTES</b> </br> <% response.Write rsTotal("TotalEmpresas") %></div>
			</td>
			<td rowspan="2" width="20%">
		   		<input type="image" alt="Cerca" src="<%=Session("Progetto")%>/images/lente.gif" id="search" name="search" value="search">
			</td>
		</tr>
		<tr style="text-align:center;">
			<td class="tbltext1">
			<select name="tipo_documento" id="tipo_documento" Class="textblack">
				<% sqlTipDoc = "select * from TADES where TADES.NOME_TABELLA = 'DOCST' order by DESCRIZIONE " %>
				<% set rsTipDoc = CC.Execute(sqlTipDoc) %>
				<% contador=0
					
					%><option value=""></option><%
					Do While contador < rsTipDoc.pagesize and NOT rsTipDoc.EOF%>
					  <% if request("tipDoc")<>"" and request("tipDoc")=rsTipDoc("CODICE") then %>
						<option value=<%= rsTipDoc("CODICE")%> selected><%= rsTipDoc("DESCRIZIONE")%></option>
					  <% else %>
						<option value=<%= rsTipDoc("CODICE")%> ><%= rsTipDoc("DESCRIZIONE")%></option>
					<% end if
					rsTipDoc.MoveNext
					contador=contador+1
					Loop
				%>
				</select>
			</td>
			<td class="tbltext1">
				<input type="text" Class="textblack" id="documento" name="documento" value="<%=request("documento")%>" maxlength="50" />
			</td>
		</tr>
		<tr style="text-align:center;">
			<td class="tbltext1" width="20%"><b>Nombre</b></td>			
			<td class="tbltext1" width="20%"><b>Apellido</b></td>
			<td class="tbltext1" width="20%"><b>Departamento</b></td>
			<td class="tbltext1" width="20%"><b>Ciudad</b></td>
		</tr>
		<tr style="text-align:center;">
			<td class="tbltext1">
				<input type="text" Class="textblack" id="nombre" name="nombre" value="<%=request("nombre")%>" maxlength="50" />
			</td>
			<td class="tbltext1">
				<input type="text" Class="textblack" id="apellido" name="apellido" value="<%=request("apellido")%>" maxlength="50" />
			</td>
			<td class="tbltext1">
				<input type="hidden" Class="textblack" id="departamento" name="departamento" value="<%=request("departamento")%>" maxlength="50" />
				<%
			   	sInt = "PROV|0|" & date & "| |cmbDepartamento' |AND VALORE = 'CO' ORDER BY DESCRIZIONE"			
				CreateCombo(sInt)
				%>
			</td>
			<td class="tbltext1">
				<input type="hidden" Class="textblack" id="ciudad" name="ciudad" value="<%=request("ciudad")%>" maxlength="50" />
				<select class="textblack" id="cmbCiudad" name="cmbCiudad" style="width: 85%;">
				<option value=""></option>				
			</select>
			</td>
		</tr>
	</table>	
</form>
</br>


<% if not rsOferenSinCentro.EOF then %>
		</br></br>
		<table border="0" width="500">
			<tr class="tbltext0 tblsfondo3" style="text-align:center;">
				<td width="12%" style="border-radius:10px 0 0 0;"><b>No. Documento</b></td>
				<td width="12%"><b>Apellido</b></td>
				<td width="12%"><b>Nombre</b></td>
				<td width="12%"><b>Departamento</b></td>
				<td width="12%"><b>Ciudad</b></td>
			</tr>
		<!-- montamos el bucle para mostrar los registros -->
		<% contador=0
		Do While contador < rsOferenSinCentro.pagesize and NOT rsOferenSinCentro.EOF%>
			<tr class="tblsfondo">
				<td style="text-align:center;"><%= rsOferenSinCentro("COD_FISC")%></td>
				<td><%= rsOferenSinCentro("COGNOME") %></td>
				<td><%= rsOferenSinCentro("NOME") %></td>
				<td><%= rsOferenSinCentro("DESCRIZIONE") %></td>
				<td><%= rsOferenSinCentro("DESCOM") %></td>
			</tr>

		<% rsOferenSinCentro.MoveNext
		contador=contador+1
		Loop
		%>

		</table>

	<!-- mostramos los botones de adelante y atras segun proceda -->
	<table border="0" width="100%" class="tbltext0 tblsfondo3" style="margin:auto;">
		<tr>
			<td width="10px" style="text-align:left;">
				<%if session("pagina") <> 1 then %>
					<form method="POST" action="reportOferenSinCentro.asp" id="form_back">
						<p>
							<a href="#" id="back">
								<img type="image" src="../../images/previous.png" width="24px" height="24px" style="border:0px;">
								<input type="hidden" alt="Atras" value="Atras" src="../../images/previous.png" id="imageBack" name="direccion" >
							</a>
							<!-- input type="image" id="back" alt="Atras" value="Atras" src="../../images/previous.png" id="imageBack" name="direccion" width="24px" height="24px" -->
							<input type="hidden" Class="textblack" id="tipo_documento_back" name="tipo_documento" value="<%=request("tipo_documento")%>" maxlength="50" />
							<input type="hidden" Class="textblack" id="documento_back" name="documento" value="<%=request("documento")%>" maxlength="50" />
							<input type="hidden" Class="textblack" id="apellido_back" name="apellido" value="<%=request("apellido")%>" maxlength="50" />
							<input type="hidden" Class="textblack" id="nombre_back" name="nombre" value="<%=request("nombre")%>" maxlength="50" style="width: 85%;" />
							<input type="hidden" Class="textblack" id="departamento_back" name="departamento" value="<%=request("departamento")%>" maxlength="50" style="width: 85%;" />
							<input type="hidden" Class="textblack" id="ciudad_back" name="ciudad" value="<%=request("ciudad")%>" maxlength="50" style="width: 85%;" />


						</p>
					</form>
				<%end if%>
			</td>
			<td width="40%" style="text-align:left;">Registros 
				<span style="color:#FF0000;"><%=inicio%></span> al 
				<span style="color:#FF0000;"><%=fin%></span> de un total de
				<span style="color:#FF0000;"><%=rsOferenSinCentro.recordcount%></span>
			</td>
			<td width="40%" style="text-align:right;">P&aacute;gina 
				<span style="color:#FF0000;"><%=session("pagina")%></span> de 
				<span style="color:#FF0000;"><%=rsOferenSinCentro.pagecount%></span>
			</td>
			<td width="10%" style="text-align:right;">
				<% if session("pagina") < rsOferenSinCentro.pagecount then %>
					<form method="POST" name="reportOfernSinCentro.asp" id="form_next">
						<p>
							<a href="#" id="next">
								<img type="image" src="../../images/next.png" width="24px" height="24px" style="border:0px;">
								<input type="hidden" alt="Adelante" value="Adelante" src="../../images/next.png" id="imageNext" name="direccion" >
							</a>
							<!-- input type="image" alt="Adelante" value="Adelante" src="../../images/next.png" id="imageNext" name="direccion" width="24px" height="24px" -->

							<input type="hidden" Class="textblack" id="tipo_documento_next" name="tipo_documento" value="<%=request("tipo_documento")%>" maxlength="50" />
							<input type="hidden" Class="textblack" id="documento_next" name="documento" value="<%=request("documento")%>" maxlength="50" />
							<input type="hidden" Class="textblack" id="apellido_next" name="apellido" value="<%=request("apellido")%>" maxlength="50" />
							<input type="hidden" Class="textblack" id="nombre_next" name="nombre" value="<%=request("nombre")%>" maxlength="50" style="width: 85%;" />
							<input type="hidden" Class="textblack" id="departamento_next" name="departamento" value="<%=request("departamento")%>" maxlength="50" style="width: 85%;" />
							<input type="hidden" Class="textblack" id="ciudad_next" name="ciudad" value="<%=request("ciudad")%>" maxlength="50" style="width: 85%;" />


						</p>
					</form>
				<%end if%>
			</td>
		</tr>
	</table>

	<%
		'******************************************************* EXPORTAR A EXCEL *******************************************************
		Dim encabezadosExcel 
		encabezadosExcel = "Documento,Apellido,Nombre,Departamento,Ciudad"
		encabezadosExcelBase64 = Base64Encode(encabezadosExcel)

		Dim encabezadosQuery 
		encabezadosQuery = "COD_FISC,COGNOME,NOME,DESCRIZIONE,DESCOM"

		encabezadosQueryBase64 = Base64Encode(encabezadosQuery)

		Dim reportFilterData 
		reportFilterData = "doc_identidad:"&request("TipDoc")&request("doc_identidad")&",apellido:"&request("apellido")&",nombre:"&request("nombre")&",departamento:"&request("departamento")&",ciudad:"&request("ciudad")
		reportFilterDataBase64 = Base64Encode(reportFilterData)
		'********************************************************************************************************************************

	%>

	<input type="button" value="Exportar a Excel" onclick="javascript:exporttoExcelGeneric('reportOferenSinCentro','REPORTE DE OFERENTES SIN CENTRO ASOCIADO','<%=reportFilterDataBase64%>','<%=encabezadosExcelBase64%>','<%=encabezadosQueryBase64%>')">


<% else%>
	<h1 class="tbltext0 tblsfondo3" style="width:80%; border-radius:10px; font-size:20px;">No se encontraron coincidencias</h1>
<% end if %>

<!-- #include Virtual="/strutt_coda2.asp" -->
<!-- #include virtual="/include/CloseConn.asp" -->