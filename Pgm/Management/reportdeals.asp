<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #include Virtual="/strutt_testa2.asp" -->
<!-- #include virtual="/include/OpenConn.asp" -->
<!-- #include virtual="/include/DecCod.asp" -->

<!--*************************** INCLUDE BASE 64 FUNCTION ****************** -->
<!-- #include virtual="/include/Base64.asp" -->
<!--*********************************************************************** -->


<!--*********************************************************************** -->
<!--Busca a la persona o grupo que le queremos enviar un mensaje ' RKL-->
<!--***********************************************************************-->
<%
	'@LANGUAGE="VBSCRIPT" CODEPAGE="65001" 
	'Session.Codepage=65001
	'Response.ContentType = "text/HTML" 
	'Response.CodePage = 65001
	'Response.CharSet = "utf-8"

	'if request("empleador")<>"" then
	'	response.Write("si llego toca buscar "&request("empleador"))




	dim empleador
	empleador=Request("nombre_empleador")
	dim titulo_oferta 
	titulo_oferta = request("titulo_oferta")
	'dim cargo_equivalente 
	'cargo_equivalente = request("cargo_equivalente")
	'dim grupo_ocupacional
	'grupo_ocupacional = request("grupo_ocupacional")
	dim centro_atencion
	centro_atencion = request("centro_atencion")
	dim fecha_publicacion
	fecha_publicacion = request("fecha_publicacion")
	dim fecha_vencimiento_from
	fecha_vencimiento_from = request("fecha_vencimiento_from")
	dim fecha_vencimiento_to
	fecha_vencimiento_to = request("fecha_vencimiento_to")

	'response.Write ("----> " & request("nombre_empleador") & " <----</br></br>")
	'response.Write ("----> " & request("titulo_oferta") & " <----</br></br>")
	'response.Write ("----> " & request("cargo_equivalente") & " <----</br></br>")
	'response.Write ("----> " & request("grupo_ocupacional") & " <----</br></br>")
	'response.Write ("----> " & request("centro_atencion") & " <----</br></br>")
	'response.Write ("----> " & request("fecha_vencimiento_from") & " <----</br></br>")
	'response.Write ("----> " & request("fecha_vencimiento_to") & " <----</br></br>")

	'****************************************************************************************************
	'Cargando data Grupo Ocupacional
	'sSQLOcupationalGroups = "SELECT ID_AREAPROF, DENOMINAZIONE FROM AREE_PROFESSIONALI ORDER BY DENOMINAZIONE ASC"
	'set rsOcupationalGroups = CC.Execute(sSQLOcupationalGroups)
	'****************************************************************************************************
	

	'Cargando data Centro de Atenci�n - Listados de centros de atenci�n
	'sSQLAttentionCenter = "SELECT ID_IMPRESA, DESCRIZIONE FROM SEDE_IMPRESA ORDER BY DESCRIZIONE ASC"
	sSQLAttentionCenter = "select SI.ID_IMPRESA, SI.ID_SEDE, I.RAG_SOC, SI.DESCRIZIONE AS centro " &_ 
        "from SEDE_IMPRESA SI " &_ 
		"inner join IMPRESA I ON I.ID_IMPRESA = SI.ID_IMPRESA " &_ 
		"where COD_TIMPR = '03' "
	set rsAttentionCenter = CC.Execute(sSQLAttentionCenter)

	'Consulta Global
	sSQLExist = "SELECT richiesta_sede.id_richiesta AS Referencia," &_ 
				"im.RAG_SOC+' ('+SEDE_IMPRESA.DESCRIZIONE+')' as Empleador," &_ 
				"richiesta_sede.offer_title as 'T�tulo de la Oferta'," &_
				"f.denominazione as 'Cargo Equivalente', a.denominazione as 'Grupo Ocupacional', " &_
				"richiesta_sede.nro_vacantes as 'Numero de Vacantes', " &_
				"(SELECT COUNT(RICHIESTA_CANDIDATO_HIST.ID_RICHIESTA) AS NUMERO " &_
					"FROM RICHIESTA_CANDIDATO_HIST " &_
					"WHERE RICHIESTA_CANDIDATO_HIST.COD_ESITO_SEL IN (1,14) " &_
					"AND RICHIESTA_CANDIDATO_HIST.ID_RICHIESTA = richiesta_sede.id_richiesta " &_
					"AND ( " &_
						"SELECT top 1 rch.DT_INS " &_
						"FROM RICHIESTA_CANDIDATO_HIST rch " &_
						"WHERE rch.ID_RICHIESTA = RICHIESTA_CANDIDATO_HIST.ID_RICHIESTA " &_
									"AND rch.ID_PERSONA = RICHIESTA_CANDIDATO_HIST.ID_PERSONA " &_
						"ORDER BY rch.DT_INS DESC " &_
					") = RICHIESTA_CANDIDATO_HIST.DT_INS " &_
				") AS 'Num Postulantes', " &_
				"(SELECT count(DISTINCT(RICHIESTA_CANDIDATO_HIST.ID_PERSONA)) " &_
					"FROM RICHIESTA_CANDIDATO_HIST " &_
					"WHERE RICHIESTA_CANDIDATO_HIST.COD_ESITO_SEL IN (1,14) " &_
					"AND RICHIESTA_CANDIDATO_HIST.ID_RICHIESTA = richiesta_sede.id_richiesta) 'Num Postulantes old'," &_
				" sim.DESCRIZIONE as 'Centro de Atenci�n', " &_
				" sim.ID_SEDE as 'Id Centro de Atenci�n', " &_
				"convert(varchar(25),richiesta_sede.DT_FINPUBLICACION, 103) as 'Fecha Vencimiento', " &_
				"convert(varchar(25),richiesta_sede.DT_INGRESO, 103) as 'Fecha Inicio', " &_
				"(SELECT COUNT(RICHIESTA_CANDIDATO_HIST.COD_ESITO_SEL) AS NUMERO " &_
					"FROM RICHIESTA_CANDIDATO_HIST " &_
					"WHERE RICHIESTA_CANDIDATO_HIST.COD_ESITO_SEL IN (1,14) " &_
							"AND RICHIESTA_CANDIDATO_HIST.COD_ESITO_SEL = 1 " &_
					"AND RICHIESTA_CANDIDATO_HIST.ID_RICHIESTA = richiesta_sede.id_richiesta  " &_
					"AND ( " &_
						"SELECT top 1 rch.DT_INS " &_
						"FROM RICHIESTA_CANDIDATO_HIST rch " &_
						"WHERE rch.ID_RICHIESTA = RICHIESTA_CANDIDATO_HIST.ID_RICHIESTA " &_
									"AND rch.ID_PERSONA = RICHIESTA_CANDIDATO_HIST.ID_PERSONA " &_
						"ORDER BY rch.DT_INS DESC " &_
					") = RICHIESTA_CANDIDATO_HIST.DT_INS " &_
					"GROUP BY CAST(RICHIESTA_CANDIDATO_HIST.COD_ESITO_SEL AS int) " &_
				") AS TOTAL_COD_UNO, " &_
				"(SELECT COUNT(RICHIESTA_CANDIDATO_HIST.COD_ESITO_SEL) AS NUMERO " &_
					"FROM RICHIESTA_CANDIDATO_HIST " &_
					"WHERE RICHIESTA_CANDIDATO_HIST.COD_ESITO_SEL IN (1,14) " &_
							"AND RICHIESTA_CANDIDATO_HIST.COD_ESITO_SEL = 14 " &_
					"AND RICHIESTA_CANDIDATO_HIST.ID_RICHIESTA = richiesta_sede.id_richiesta  " &_
					"AND ( " &_
						"SELECT top 1 rch.DT_INS " &_
						"FROM RICHIESTA_CANDIDATO_HIST rch " &_
						"WHERE rch.ID_RICHIESTA = RICHIESTA_CANDIDATO_HIST.ID_RICHIESTA " &_
									"AND rch.ID_PERSONA = RICHIESTA_CANDIDATO_HIST.ID_PERSONA " &_
						"ORDER BY rch.DT_INS DESC " &_
					") = RICHIESTA_CANDIDATO_HIST.DT_INS " &_
					"GROUP BY CAST(RICHIESTA_CANDIDATO_HIST.COD_ESITO_SEL AS int) " &_
				") AS TOTAL_COD_CATORCE " &_
				"FROM SEDE_IMPRESA " &_
				"inner join IMPRESA im on im.id_impresa = SEDE_IMPRESA.id_impresa " &_
				"inner join richiesta_sede on richiesta_sede.id_sede = SEDE_IMPRESA.id_sede " &_
				"inner join FIGUREPROFESSIONALI f on richiesta_sede.id_figprof = f.id_figprof " &_
				"inner join impresa_rolesserviciostipos irs ON richiesta_sede.servicio = irs.servicio " &_
				"inner join AREE_PROFESSIONALI a on a.id_areaprof = f.id_areaprof " &_
				"inner join SEDE_IMPRESA sim on sim.id_sede = richiesta_sede.COD_OFICINA " &_
				"WHERE	richiesta_sede.DT_finpublicacion > = convert(datetime,CONVERT(varchar(10), GETDATE(), 103),103) AND richiesta_sede.FL_PUBBLICATO = 'S' " &_
					"AND richiesta_sede.cod_tipo_richiesta = 1 "
				if request("nombre_empleador") <>"" then					
					sSQLExist = sSQLExist & "AND im.RAG_SOC LIKE '%" & empleador & "%' "
				end if
				if request("titulo_oferta") <>"" then					
					sSQLExist = sSQLExist & "AND richiesta_sede.offer_title LIKE '%" & titulo_oferta & "%' "
				end if
				'if request("cargo_equivalente")<>"" then					
					'sSQLExist = sSQLExist & "AND f.denominazione LIKE '%" & cargo_equivalente & "%' "
				'end if
				'if request("grupo_ocupacional")<>"" then					
					'sSQLExist = sSQLExist & "AND a.ID_AREAPROF = " & grupo_ocupacional 
				'end if
				if request("centro_atencion")<>"" then					
					sSQLExist = sSQLExist & " AND richiesta_sede.COD_OFICINA = " & centro_atencion
				end if
				if request("fecha_publicacion")<>"" then
					sSQLExist = sSQLExist & " AND convert(datetime,convert(varchar(25),richiesta_sede.DT_INGRESO, 103), 103) = convert(datetime,convert(varchar(25),'" & fecha_publicacion & " 00:00:00', 103), 103) " 
				end if
				if request("fecha_vencimiento_from")<>"" then					
					sSQLExist = sSQLExist & " AND convert(datetime,richiesta_sede.DT_FINPUBLICACION, 103) BETWEEN convert(datetime,convert(varchar(25),'" & fecha_vencimiento_from & " 00:00:00', 103), 103) AND convert(datetime,convert(varchar(25),'" & fecha_vencimiento_to & " 00:00:00', 103),103) "
				end if
				if request("nroVacantesVsPostulantes") <> "" then

					if request("nroVacantesVsPostulantes") = "Mayor" then
						sSQLExist = sSQLExist & " AND richiesta_sede.nro_vacantes > (SELECT count(DISTINCT(RICHIESTA_CANDIDATO_HIST.ID_PERSONA)) FROM RICHIESTA_CANDIDATO_HIST WHERE RICHIESTA_CANDIDATO_HIST.COD_ESITO_SEL IN (1,14) AND RICHIESTA_CANDIDATO_HIST.ID_RICHIESTA = richiesta_sede.id_richiesta)"
					end if
					if request("nroVacantesVsPostulantes") = "Menor" then
						sSQLExist = sSQLExist & " AND richiesta_sede.nro_vacantes < (SELECT count(DISTINCT(RICHIESTA_CANDIDATO_HIST.ID_PERSONA)) FROM RICHIESTA_CANDIDATO_HIST WHERE RICHIESTA_CANDIDATO_HIST.COD_ESITO_SEL IN (1,14) AND RICHIESTA_CANDIDATO_HIST.ID_RICHIESTA = richiesta_sede.id_richiesta)"
					end if
					if request("nroVacantesVsPostulantes") = "Igual" then
						sSQLExist = sSQLExist & " AND richiesta_sede.nro_vacantes = (SELECT count(DISTINCT(RICHIESTA_CANDIDATO_HIST.ID_PERSONA)) FROM RICHIESTA_CANDIDATO_HIST WHERE RICHIESTA_CANDIDATO_HIST.COD_ESITO_SEL IN (1,14) AND RICHIESTA_CANDIDATO_HIST.ID_RICHIESTA = richiesta_sede.id_richiesta)"
					end if

				end if

				sSQLExist = sSQLExist & "Order by richiesta_sede.DT_FINPUBLICACION asc"								
				'response.Write sSQLExist
				'response.Write sSQLExist & "</br>"

	
	'response.Write "nombre_empleador --- " & request("nombre_empleador") & "</br>"
	'response.Write "titulo_oferta --- " & request("titulo_oferta") & "</br>"
	'response.Write "centro_atencion --- " & request("centro_atencion") & "</br>"
	'response.Write "fecha_publicacion --- " & request("fecha_publicacion") & "</br>"
	'response.Write "fecha_vencimiento_from --- " & request("fecha_vencimiento_from") & "</br>"
	'response.Write "fecha_vencimiento_to --- " & request("fecha_vencimiento_to") & "</br>"
	
	'rsDeals.CursorType = adOpenstatic 
	set rsDeals = CC.Execute(sSQLExist)



'Response.ContentType = "application/vnd.ms-excel"


  	'Dim ResultsString
    'Dim objFileSystemObject
    'Dim objTextStream
    'Dim filename
 
    ' grab the string, using a comma as a record separator and a tab as a record separator
    'ResultsString = rsDeals.GetString(2, , ",", vbTab)
 
    ' create a link to the local file system on the server
    'Set objFileSystemObject = Server.CreateObject("Scripting.FileSystemObject")
 
    ' Create a text file and dump the contents of the string to the file,
    ' using the file name export with the current date and time as a way to seperate out differing users
    'filename="export" & CLng(Now)
    'Set objTextStream = objFileSystemObject.CreateTextFile(filename & ".csv", 2, True)
    'objTextStream.Write(ResultsString)
 
    ' Clean up
    'objTextStream.Close
    'Set objTextStream = Nothing
    'Set objFileSystemObject = Nothing
 
    ' return a link to the file to the browser
    'Response.Write("The file <a href='" & filename &".csv'>" & filename & ".csv</a> is now available to download")


	if request("search") = "search" then
		session("pagina") = 1
	end if

	rsDeals.pagesize=10

	if rsDeals.pagecount <> 0 then
		if session("pagina")>rsDeals.pagecount then
				session("pagina")=rsDeals.pagecount 'evitamos el error de reload
		end if

		' controlando avance de pagina
		
		dim direccion 
		'if request("direccion") = "" then		
	''		direccion = "Atras"
	''	else
			direccion = request("direccion")
	''	end if

		
		if session("pagina") = "" then
			session("pagina") = 1
		else

			if direccion = "Adelante" and session("pagina")<rsDeals.pagecount then
				session("pagina") = session("pagina") + 1
			else 
				if direccion = "Atras" and session("pagina")>1 then
					session("pagina") = session("pagina") - 1
				end if	 
			end if	 
		end if


		'response.Write "pagecount----- " & rsDeals.pagecount & " -----</br>"
		'response.Write "absolutepage----- " & rsDeals.absolutepage & " -----</br>"
		'response.Write "session.pagina----- " & session("pagina") & " ----</br>"

		if session("pagina")<>"" then
			rsDeals.absolutepage=session("pagina")
		else 
			rsDeals.absolutepage=1
		end if

		inicio=1+(session("pagina")-1)*rsDeals.pagesize
		fin=inicio+rsDeals.pagesize - 1
		if fin > rsDeals.recordcount then
			fin =rsDeals.recordcount
		end if
		'rsDeals.absolutepage=3

	end if 




	'response.Write "pagecount----- " & rsDeals.pagecount & " -----</br>"
	'response.Write "absolutepage----- " & rsDeals.absolutepage & " -----</br>"
	'response.Write "session.pagina----- " & session("pagina") & " ----</br>"


	sSQLTotal = "SELECT count(DISTINCT(rs.id_richiesta)) AS 'Total', " &_
				"SUM(rs.nro_vacantes) AS 'Total Vacantes'" &_
				"FROM richiesta_sede rs " &_
				"inner join SEDE_IMPRESA si on si.id_sede = rs.id_sede " &_
				"inner join IMPRESA im on im.id_impresa = si.id_impresa " &_ 
				"inner join FIGUREPROFESSIONALI f on f.id_figprof = rs.id_figprof " &_
				"inner join impresa_rolesserviciostipos irs ON irs.servicio = rs.servicio " &_
				"inner join AREE_PROFESSIONALI a on a.id_areaprof = f.id_areaprof " &_
				"inner join SEDE_IMPRESA sim on sim.id_sede = rs.COD_OFICINA " &_
				"WHERE DT_finpublicacion > = convert(datetime,CONVERT(varchar(10), GETDATE(), 103),103) " &_
							"AND FL_PUBBLICATO = 'S' " &_ 
							"AND cod_tipo_richiesta = 1 " 
							if request("nombre_empleador")<>"" then					
								sSQLTotal = sSQLTotal & "AND im.RAG_SOC LIKE '%" & empleador & "%' "
							end if
							if request("titulo_oferta")<>"" then					
								sSQLTotal = sSQLTotal & "AND rs.offer_title LIKE '%" & titulo_oferta & "%' "
							end if
							'if request("cargo_equivalente")<>"" then					
								'sSQLTotal = sSQLTotal & "AND f.denominazione LIKE '%" & cargo_equivalente & "%' "
							'end if
							'if request("grupo_ocupacional")<>"" then					
								'sSQLTotal = sSQLTotal & "AND a.ID_AREAPROF = " & grupo_ocupacional 
							'end if
							if request("centro_atencion")<>"" then					
								sSQLTotal = sSQLTotal & "AND rs.COD_OFICINA = " & centro_atencion
							end if
							if request("fecha_publicacion")<>"" then
								sSQLTotal = sSQLTotal & " AND convert(datetime,convert(varchar(25),rs.DT_INGRESO, 103), 103) = convert(datetime,convert(varchar(25),'" & fecha_publicacion & " 00:00:00', 103), 103) " 
							end if
							if request("fecha_vencimiento_from")<>"" then					
								sSQLTotal = sSQLTotal & " AND convert(datetime,rs.DT_FINPUBLICACION, 103) BETWEEN convert(datetime,convert(varchar(25),'" & fecha_vencimiento_from & " 00:00:00', 103), 103) AND convert(datetime,convert(varchar(25),'" & fecha_vencimiento_to & " 00:00:00', 103),103) "
							end if
							if request("nroVacantesVsPostulantes") <> "" then

								if request("nroVacantesVsPostulantes") = "Mayor" then
									sSQLTotal = sSQLTotal & " AND rs.nro_vacantes > (SELECT count(DISTINCT(RICHIESTA_CANDIDATO_HIST.ID_PERSONA)) FROM RICHIESTA_CANDIDATO_HIST WHERE RICHIESTA_CANDIDATO_HIST.COD_ESITO_SEL IN (1,14) AND RICHIESTA_CANDIDATO_HIST.ID_RICHIESTA = rs.id_richiesta)"
								end if
								if request("nroVacantesVsPostulantes") = "Menor" then
									sSQLTotal = sSQLTotal & " AND rs.nro_vacantes < (SELECT count(DISTINCT(RICHIESTA_CANDIDATO_HIST.ID_PERSONA)) FROM RICHIESTA_CANDIDATO_HIST WHERE RICHIESTA_CANDIDATO_HIST.COD_ESITO_SEL IN (1,14) AND RICHIESTA_CANDIDATO_HIST.ID_RICHIESTA = rs.id_richiesta)"
								end if
								if request("nroVacantesVsPostulantes") = "Igual" then
									sSQLTotal = sSQLTotal & " AND rs.nro_vacantes = (SELECT count(DISTINCT(RICHIESTA_CANDIDATO_HIST.ID_PERSONA)) FROM RICHIESTA_CANDIDATO_HIST WHERE RICHIESTA_CANDIDATO_HIST.COD_ESITO_SEL IN (1,14) AND RICHIESTA_CANDIDATO_HIST.ID_RICHIESTA = rs.id_richiesta)"
								end if
								
							end if

				
	set rsTotal = CC.Execute(sSQLTotal)		

	
	function roundUp(x)
	   If x > Int(x) then
	    roundup = Int(x) + 1
	   Else
	    roundup = x
	   End If
	End Function


	'dim totalRegistros
	'totalRegistros = rsTotal("Total")
	'dim nroRegistrosPorPagina
	'nroRegistrosPorPagina = 2
	'dim nroPaginas
	'nroPaginas = roundUp(totalRegistros/nroRegistrosPorPagina)
	'dim pagina
	'pagina = 1

	'dim limiteInferior

	'response.Write "TotalReg -> " & totalRegistros & "</br></br>"
	'response.Write "nroRegistrosPorPagina -> " & nroRegistrosPorPagina & "</br></br>"
	'response.Write "PAg -> " & pagina & "</br></br>"
	'response.Write "nroPag -> " & nroPaginas & "</br></br>"	


	

	'Do While pagina <= nroPaginas
			'response.Write pagina & "</br></br>"
		'limiteInferior = (pagina - 1) * nroRegistrosPorPagina
			'sSQLLimits = sSQLExist & " Limit " & limiteInferior & ", " & nroRegistrosporPagina 
			'response.Write sSQLLimits & "</br></br>"

			'set rsDealsLimits = CC.Execute(sSQLLimits)	

  		'pagina = pagina + 1
	'Loop

'else
''	response.Write("Nada que buscar")
'End If
%>

<script language="Javascript"> 
$(function(){
	/* $("#cmbOcupationGroups").change(function() {
		var cmbOcupationGroups = $("#cmbOcupationGroups").val();
		$("#grupo_ocupacional").val(cmbOcupationGroups); 
	}); */

	$("#cmbAttentionCenter").change(function() {
		var cmbAttentionCenter = $("#cmbAttentionCenter").val();
		$("#centro_atencion").val(cmbAttentionCenter); 
	});
	var attention_center = $("#centro_atencion").val();
	$("#cmbAttentionCenter").val(attention_center);

	$("#cmbnroVacantesVsPostulantes").change(function() {
		var cmbnroVacantesVsPostulantes = $("#cmbnroVacantesVsPostulantes").val();
		$("#nroVacantesVsPostulantes").val(cmbnroVacantesVsPostulantes); 
	});
	var nroVacantesVsPostulantes = $("#nroVacantesVsPostulantes").val();
	$("#cmbnroVacantesVsPostulantes").val(nroVacantesVsPostulantes);

	

	
	$("#form_next").submit(function(){
			$("#nombre_empleador_next").val($("#nombre_empleador").val());
			$("#titulo_oferta_next").val($("#titulo_oferta").val());
			$("#centro_atencion_next").val($("#centro_atencion").val());
			$("#fecha_publicacion_next").val($("#fecha_publicacion").val());
			$("#fecha_vencimiento_from_next").val($("#fecha_vencimiento_from").val());
			$("#fecha_vencimiento_to_next").val($("#fecha_vencimiento_to").val());
			return true;
		});


	$("#next").click(function(){
		$("#form_next").submit();		
	});

	$("#form_back").submit(function(){
			$("#nombre_empleador_back").val($("#nombre_empleador").val());  
			$("#titulo_oferta_back").val($("#titulo_oferta").val());
			$("#centro_atencion_back").val($("#centro_atencion").val());
			$("#fecha_publicacion_back").val($("#fecha_publicacion").val());
			$("#fecha_vencimiento_from_back").val($("#fecha_vencimiento_from").val());
			$("#fecha_vencimiento_to_back").val($("#fecha_vencimiento_to").val());
			return true;
		});

	$("#back").click(function(){
		$("#form_back").submit();
	});	



	/* $("#form_next").submit(function(){
		
	});

	$("#form_back").submit(function(){
		
	});*/
	


});

	/*function open_win(url)
	{
		window.open(url)
	}*/
	/*
	function exporttoExcel(){
		$("#exporttoExcel").attr("value","Generando");
		$.post("reportDealsExporttoExcel.asp",function(data){
			if(data == "Empty")
				alert("No hay informaci�n");
			else
				window.open(data)
	    		
	  	});
	  	$("#exporttoExcel").attr("value","Exportar a Excel");
	}
	*/

</script>

<form name="cerca" id="form_search" action="reportdeals.asp" Method="Post">


	<br>
	<table border="0" width="700" CELLPADDING="0" cellspacing="0">
		<tr>
			<td align="left" class="sfondomenu" width="199">
				<span class="tbltext0">
				<b>&nbsp;REPORTE DE PERFILES VIGENTES</b></span>
			</td>
			<td width="25" valign="bottom" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">
				<img border="0" src="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
			<td width="30%" valign="middle" align="right" class="tbltext1" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
		</tr>
	</table>
		
	<table border="0" width="700" CELLPADDING="0" cellspacing="0">
	    <tr>
			<td class="sfondocomm" align="left" class="tbltext1">
			En esta secci&oacute;n se puede efectuar una b&uacute;squeda de perfiles solicitados por las empresas, cuya fecha de vencimiento es posterior a la fecha actual.</br></br>
			<!-- Puede filtrar la b&uacute;squeda ingresando el apellido y nombre del usuario. -->
			</td>
			<td class="sfondocomm">
				<a href="Javascript:Show_Help('/Pgm/Help/ProjectSite/Rubrica/default/')">
				<img src="<%=Session("Progetto")%>/images/Help.gif" border="0" align="right" alt="Help">
			</td> 
	    </tr>

		<tr height="2">
			<td class="sfondocomm" width="100%" colspan="2" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>
<table width="700" border="0" class="sfondocomm">
	<tr class="sfondocomm">
		<td colspan="2"><b>B&Uacute;SQUEDA DE PERFIL</b></td>		
	</tr>
	<tr style="text-align:center;">
		<td class="tbltext1" width="22%"><b>Empleador</b></td>
		<td class="tbltext1" width="22%"><b>Titulo Perfil</b></td>
		<!-- td class="tbltext1" width="20%"><b>Cargo Equivalente</b></td>
		<td class="tbltext1" width="20%"><b>Grupo Ocupacional</b></td -->
		<td class="tbltext1" width="22%"><b>Centro de Atenci&oacute;n</b></td>
		<td class="tbltext1" width="22%"><b>Fecha Publicaci&oacute;n</b></td>
		<td class="tbltext1" width="22%"><b>Nro Vacantes Vs Postulantes</b></td>
	</tr>
	<tr style="text-align:center;">
		<td class="tbltext1">
			<input type="text" Class="textblack" id="nombre_empleador" name="nombre_empleador" value="<%=request("nombre_empleador")%>" maxlength="50" />
		</td>
		<td class="tbltext1">
			<input type="text" Class="textblack" id="titulo_oferta" name="titulo_oferta" value="<%=request("titulo_oferta")%>" maxlength="50" />
		</td>
		<!-- td class="tbltext1">
			<input type="text" Class="textblack" name="cargo_equivalente" value="" maxlength="50" / -->
			<!-- %= request("cargo_equivalente")  % --> 
		<!--/td -->
		<!-- td class="tbltext1">
			<input type="hidden" Class="textblack" id="grupo_ocupacional" name="grupo_ocupacional" value="" maxlength="50" / -->
			<!-- %=request("grupo_ocupacional")% -->
			<!-- select class="textblack" id="cmbOcupationGroups" name="cmbOcupationGroups" style="width: 85%;">
				<option selected></option -->
				<% 'if not rsOcupationalGroups.EOF then %>
					<% 'do while not rsOcupationalGroups.EOF %>
						<!-- option value="" -->
						<!-- %=rsOcupationalGroups("ID_AREAPROF")% -->
						<% 'response.Write rsOcupationalGroups("DENOMINAZIONE") %><!-- /option -->
						<% 'rsOcupationalGroups.MoveNext %>
					<% 'loop %>
				<% 'end if %>
			<!-- /select>
		</td -->

		<td class="tbltext1">
			<input type="hidden" Class="textblack" id="centro_atencion" name="centro_atencion" value="<%=request("centro_atencion")%>" maxlength="50" />
			<select class="textblack" id="cmbAttentionCenter" name="cmbAttentionCenter" style="width: 85%;">
				<option selected></option>
				<% if not rsAttentionCenter.EOF then %>
					<% do while not rsAttentionCenter.EOF %>
						<option value="<%=rsAttentionCenter("ID_SEDE")%>"><% response.Write rsAttentionCenter("centro") %></option>
						<% rsAttentionCenter.MoveNext %>
					<% loop %>
				<% end if %>
			</select>
		</td>

		<td class="tbltext1">
			<input type="text" Class="textblack fecha" id="fecha_publicacion" name="fecha_publicacion" value="<%=request("fecha_publicacion")%>" maxlength="50" style="width: 85%;" />
		</td>
		<td class="tbltext1">
			<input type="hidden" Class="textblack" id="nroVacantesVsPostulantes" name="nroVacantesVsPostulantes" value="<%=request("cmbnroVacantesVsPostulantes")%>" maxlength="50" />
			<select class="textblack" id="cmbnroVacantesVsPostulantes" name="cmbnroVacantesVsPostulantes" style="width: 85%;">
				<option value=""></option>
				<option value="Mayor">Mayor</option>
				<option value="Menor">Menor</option>
				<option value="Igual">Igual</option>
			</select>
		</td>
	</tr>
	<tr style="text-align:center;">
		<td class="tbltext1" width="22%"><b>Fecha Vencimiento (Desde)</b></td>
		<td class="tbltext1" width="22%"><b>Fecha Vencimiento (Hasta)</b></td>
		<td width="22%" rowspan="2" style="vertical-align: middle; text-align: center; margin-left: auto; margin-right: auto; align: center;">
			<div style="margin:auto; border-radius:10px; padding-top:8px; padding-bottom:8px; width:80%; text-align:center;" class="tbltext0 tblsfondo3"><b>TOTAL PERFILES</b> </br> <% response.Write rsTotal("Total") %></div>
		</td>
		<td width="22%" rowspan="2" style="vertical-align: middle; text-align: center; margin-left: auto; margin-right: auto; align: center;">
			<div style="margin:auto; border-radius:10px; padding-top:8px; padding-bottom:8px; width:80%; text-align:center;" class="tbltext0 tblsfondo3"><b>TOTAL VACANTES</b> </br> <% response.Write rsTotal("Total Vacantes") %></div>
		</td>
		<td rowspan="2" width="12%">
		   <input type="image" alt="Cerca" src="<%=Session("Progetto")%>/images/lente.gif" id="search" name="search" value="search">
		</td>
	</tr>
	<tr  style="text-align:center;">
		<td class="tbltext1">
			<input type="text" Class="textblack fecha" id="fecha_vencimiento_from" name="fecha_vencimiento_from" value="<%=request("fecha_vencimiento_from")%>" maxlength="50" style="width: 85%;" />
		</td>
		<td class="tbltext1">
			<input type="text" Class="textblack fecha" id="fecha_vencimiento_to" name="fecha_vencimiento_to" value="<%=request("fecha_vencimiento_to")%>" maxlength="50" style="width: 85%;" />
		</td>
	</tr>
</table>
</br>
<!-- table width="700" border="0">
	<tr class="sfondocomm">
		<td colspan="2"><b>B&uacute;squeda de Oferta</b></td>
	</tr>
	<tr>
		<td class="tbltext1" width="48%">Empleador</td>
		<td width="52%">
		<input type="text" Class="textblack" name="empleador" value="<%=request(empleador)%>" maxlength="50" /></td>
	</tr>
	<tr><td colspan="2" style="background-color:red;">&nbsp;</td><tr>
	<tr>
		<td colspan="2" align="center">
		   <input type="image" alt="Cerca" src="<%=Session(Progetto)%>/images/lente.gif" id="image1" name="image1">
		</td>
	</tr>
</table -->
</form>

<% 'if request("nombre_empleador")<>"" then %>

	<% if not rsDeals.EOF then %>

		<table border="0" width="700px">
			<tr class="tbltext0 tblsfondo3" style="text-align:center;">
				<td width="12%" style="border-radius:10px 0 0 0;"><b>Referencia</b></td>
				<td width="20%"><b>Empleador</b></td>
				<td width="12%"><b>Titulo Perfil</b></td>
				<!-- td width="100"><b>Cargo Equivalente</b></td>
				<td width="50"><b>Grupo Ocupacional</b></td -->
				<td width="12%"><b>N&uacute;mero Vacantes</b></td>
				<td width="12%" colspan="2"><b>N&uacute;mero Postulantes</b></td>
				<td width="18%"><b>Centro de Atenci&oacute;n</b></td>
				<td width="12%" style="border-radius:0 10px 0 0;"><b>Fecha Publicaci&oacute;n - Vencimiento</b></td>
			</tr>


		<!-- montamos el bucle para mostrar los registros -->
		<% contador=0
		Do While contador < rsDeals.pagesize and NOT rsDeals.EOF%>
			<tr class="tblsfondo">
				<td style="text-align:center;"><%= rsDeals("Referencia")%></td>
				<td><%= rsDeals("Empleador") %></td>
				<td><%= rsDeals("T�tulo de la Oferta") %></td>
				<td style="text-align:center;"><%= rsDeals("Numero de Vacantes") %></td>
				<td style="text-align:center;" width="5%">
					<%= rsDeals("Num Postulantes") %>
				</td>
				<td style="text-align:left;" width="18%">
					Remitido: 
						<% if rsDeals("TOTAL_COD_UNO") <> 0 then%>
							<%= rsDeals("TOTAL_COD_UNO") %>
						<% else %>
							0
						<% end if %>
					</br>Autopostulado: 
						<% if rsDeals("TOTAL_COD_CATORCE") <> 0 then%>
							<%= rsDeals("TOTAL_COD_CATORCE") %>
						<% else %>
							0
						<% end if %>
				</td>
				<td><%= rsDeals("Centro de Atenci�n") %></td>
				<td><%= rsDeals("Fecha Inicio") %> - <span style="color:red;"><%= rsDeals("Fecha Vencimiento") %></td>
			</tr>

			<% rsDeals.MoveNext
			contador=contador+1
		Loop

		%>
		</table>

<!-- mostramos los botones de adelante y atras segun proceda -->


	<table border="0" width="700px" class="tbltext0 tblsfondo3">
		<tr>
			<td width="10%" style="text-align:left;">
				<%if session("pagina") <> 1 then %>
					<form method="POST" action="reportdeals.asp" id="form_back">
						<p>
							<a href="#" id="back">
								<img type="image" src="../../images/previous.png" width="24px" height="24px" style="border:0px;">
								<input type="hidden" alt="Atras" value="Atras" src="../../images/previous.png" id="imageBack" name="direccion" >
							</a>
							<!-- input type="image" id="back" alt="Atras" value="Atras" src="../../images/previous.png" id="imageBack" name="direccion" width="24px" height="24px" -->

							<input type="hidden" Class="textblack" id="nombre_empleador_back" name="nombre_empleador" value="<%=request("nombre_empleador")%>" maxlength="50" />
							<input type="hidden" Class="textblack" id="titulo_oferta_back" name="titulo_oferta" value="<%=request("titulo_oferta")%>" maxlength="50" />
							<input type="hidden" Class="textblack" id="centro_atencion_back" name="centro_atencion" value="<%=request("centro_atencion")%>" maxlength="50" />
							<input type="hidden" Class="textblack" id="fecha_publicacion_back" name="fecha_publicacion" value="<%=request("fecha_publicacion")%>" maxlength="50" style="width: 85%;" />
							<input type="hidden" Class="textblack" id="fecha_vencimiento_from_back" name="fecha_vencimiento_from" value="<%=request("fecha_vencimiento_from")%>" maxlength="50" style="width: 85%;" />
							<input type="hidden" Class="textblack" id="fecha_vencimiento_to_back" name="fecha_vencimiento_to" value="<%=request("fecha_vencimiento_to")%>" maxlength="50" style="width: 85%;" />

						</p>
					</form>
				<%end if%>
			</td>
			<td width="40%" style="text-align:left;">Registros 
				<span style="color:#FF0000;"><%=inicio%></span> al 
				<span style="color:#FF0000;"><%=fin%></span> de un total de
				<span style="color:#FF0000;"><%=rsDeals.recordcount%></span>
			</td>
			<td width="40%" style="text-align:right;">P&aacute;gina 
				<span style="color:#FF0000;"><%=session("pagina")%></span> de 
				<span style="color:#FF0000;"><%=rsDeals.pagecount%></span>
			</td>
			<td width="10%" style="text-align:right;">
				<% if session("pagina") < rsDeals.pagecount then %>
					<form method="POST" name="reportdeals.asp" id="form_next">
						<p>
							<a href="#" id="next">
								<img type="image" src="../../images/next.png" width="24px" height="24px" style="border:0px;">
								<input type="hidden" alt="Adelante" value="Adelante" src="../../images/next.png" id="imageNext" name="direccion" >
							</a>
							<!-- input type="image" alt="Adelante" value="Adelante" src="../../images/next.png" id="imageNext" name="direccion" width="24px" height="24px" -->


							<input type="hidden" Class="textblack" id="nombre_empleador_next" name="nombre_empleador" value="<%=request("nombre_empleador")%>" maxlength="50" />
							<input type="hidden" Class="textblack" id="titulo_oferta_next" name="titulo_oferta" value="<%=request("titulo_oferta")%>" maxlength="50" />
							<input type="hidden" Class="textblack" id="centro_atencion_next" name="centro_atencion" value="<%=request("centro_atencion")%>" maxlength="50" />
							<input type="hidden" Class="textblack" id="fecha_publicacion_next" name="fecha_publicacion" value="<%=request("fecha_publicacion")%>" maxlength="50" style="width: 85%;" />
							<input type="hidden" Class="textblack" id="fecha_vencimiento_from_next" name="fecha_vencimiento_from" value="<%=request("fecha_vencimiento_from")%>" maxlength="50" style="width: 85%;" />
							<input type="hidden" Class="textblack" id="fecha_vencimiento_to_next" name="fecha_vencimiento_to" value="<%=request("fecha_vencimiento_to")%>" maxlength="50" style="width: 85%;" />
						</p>
					</form>
				<%end if%>
			</td>
		</tr>
	</table>

<%
	'******************************************************* EXPORTAR A EXCEL *******************************************************
	Dim encabezadosExcel 
	encabezadosExcel = "Referencia,Empleador,Titulo Perfil,N�mero Vacantes,N�mero Postulantes,Centro de Atenci�n,Fecha Publicaci�n,Fecha Vencimiento"
	encabezadosExcelBase64 = Base64Encode(encabezadosExcel)

	Dim encabezadosQuery 
	encabezadosQuery = "Referencia,Empleador,T�tulo de la Oferta,Numero de Vacantes,Num Postulantes,Centro de Atenci�n,Fecha Inicio,Fecha Vencimiento"
	encabezadosQueryBase64 = Base64Encode(encabezadosQuery)

	Dim reportFilterData 
	reportFilterData = "empleador:"&empleador&",titulo_oferta:"&titulo_oferta&",centro_atencion:"&centro_atencion&",fecha_publicacion:"&fecha_publicacion&",nroVacantesVsPostulantes:"&request("nroVacantesVsPostulantes")&",fecha_vencimiento_from:"&fecha_vencimiento_from&",fecha_vencimiento_to:"&fecha_vencimiento_to	
	reportFilterDataBase64 = Base64Encode(reportFilterData)
	'********************************************************************************************************************************
%>
	<!-- input type="button" id="exporttoExcel" value="Exportar a Excel" onclick="javascript:exporttoExcel()" -->
	<!-- exporttoExcelGeneric(reportName,reportTitle,reportFilterDataBase64,encabezadosExcelBase64,encabezadosQueryBase64) -->
	<input type="button" value="Exportar a Excel" onclick="javascript:exporttoExcelGeneric('reportCurrentDeals','REPORTE DE PERFILES VIGENTES','<%=reportFilterDataBase64%>','<%=encabezadosExcelBase64%>','<%=encabezadosQueryBase64%>')">
	

	<% else%>
		<h1 class="tbltext0 tblsfondo3" style="width:80%; border-radius:10px; font-size:20px;">No se encontraron coincidencias</h1>
	<% end if %>

<% 'end if %>
<!-- #include Virtual="/strutt_coda2.asp" -->
<!-- #include virtual="/include/CloseConn.asp" -->