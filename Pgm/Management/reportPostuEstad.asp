<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #include Virtual="/strutt_testa2.asp" -->
<!-- #include virtual="/include/OpenConn.asp" -->
<!-- #include virtual="/include/DecCod.asp" -->

<!--*************************** INCLUDE BASE 64 FUNCTION ****************** -->
<!-- #include virtual="/include/Base64.asp" -->
<!--*********************************************************************** -->

<!--*********************************************************************** -->
<!--Busca a la persona o grupo que le queremos enviar un mensaje ' RKL-->
<!--***********************************************************************-->
<%
	'@LANGUAGE="VBSCRIPT" CODEPAGE="65001" 
	'Session.Codepage=65001
	'Response.ContentType = "text/HTML" 
	'Response.CodePage = 65001
	'Response.CharSet = "utf-8"


	'****************************************************************************************************
	'Cargando data Grupo Ocupacional
	'sSQLOcupationalGroups = "SELECT ID_AREAPROF, DENOMINAZIONE FROM AREE_PROFESSIONALI ORDER BY DENOMINAZIONE ASC"
	'set rsOcupationalGroups = CC.Execute(sSQLOcupationalGroups)
	'****************************************************************************************************


	sSQLTotal = "SELECT count(1) AS totalempresas " &_
				"FROM PERSONA A  " &_
				"LEFT OUTER JOIN STATO_OCCUPAZIONALE C ON A.ID_PERSONA = C.ID_PERSONA AND C.IND_STATUS = 0  " &_
				"INNER JOIN RICHIESTA_CANDIDATO D ON D.ID_PERSONA = A.ID_PERSONA " &_
				"INNER JOIN RICHIESTA_SEDE RS ON RS.ID_RICHIESTA = D.ID_RICHIESTA  " &_
				"WHERE  D.ID_PERSONA = A.ID_PERSONA " &_
				"and A.ID_PERSONA not in (select ID_PERSONA from PERSONAS_JOB_PROFILE where AUTHORIZATIONOFDATAVISUALIZATION =3 ) "
				if request("tipDoc") <> "" and request("doc_identidad") <> "" then
					sSQLTotal = sSQLTotal & "AND a.COD_FISC like'%" & request("tipDoc")&request("doc_identidad") & "%' "
				end if
				if request("nombre") <>"" then
					sSQLTotal = sSQLTotal & "AND a.nome LIKE '%" & request("nombre") & "%' "
				end if
				if request("apellido") <>"" then
					sSQLTotal = sSQLTotal & "AND a.cognome like '" & request("apellido") & "' "
				end if
				if request("vacante") <>"" then
					sSQLTotal = sSQLTotal & "AND D.id_richiesta = " & request("vacante") & " "
				end if
				if request("estado") <>"" then
					sSQLTotal = sSQLTotal & "AND D.COD_ESITO_SEL = '" & request("estado") & "' "
				end if
				if request("centro_atencion") <>"" then
					sSQLTotal = sSQLTotal & " and RS.COD_OFICINA = '" & request("centro_atencion") & "' "
				end if
				if request("fecini") <>"" and request("fecfin") <>"" then
					sSQLTotal = sSQLTotal & " and convert(datetime,convert(varchar(25),DT_INS, 103), 103) >= convert(datetime,convert(varchar(25),'" & request("fecini") & " 00:00:00', 103), 103) and " &_
					            " convert(datetime,convert(varchar(25),DT_INS, 103), 103) <= convert(datetime,convert(varchar(25),'" & request("fecfin") & " 00:00:00', 103), 103) "
				end if
				
	'response.WRITE sSQLTotal
	'RESPONSE.end
	set rsTotal = CC.Execute(sSQLTotal)

	'Consulta Global
	sSQLExist = "SELECT rs.ID_RICHIESTA as 'vacante', COD_FISC as identificacion, FLOOR(DATEDIFF(month, A.DT_NASC, GETDATE()) / CONVERT(FLOAT, 12)) AS Edad, " &_
			    "(select DESCRIZIONE from TADES where NOME_TABELLA = 'PROV' AND CODICE = a.PRV_RES ) AS 'Departamento', " &_
			    "(select DESCOM from COMUNE where CODCOM = a.COM_RES ) AS 'Ciudad', " &_
			    " A.nome + ' ' + A.cognome as 'Nombre', " &_
			    "A.frazione_res as Barrio, A.ind_res as direccion, A.num_tel_dom as telefono,  " &_
			    "(select DESCRIZIONE from TADES where NOME_TABELLA = 'STDIS' AND CODICE = C.COD_STDIS ) AS 'situacionlaboral', " &_
			    "CONVERT(VARCHAR(23), D.DT_INS, 103) AS 'fechapostulacion',  " &_
			    "(select DESCRIZIONE from TADES  " &_
			    " where NOME_TABELLA = 'essel' " &_
			    " AND CODICE = D.COD_ESITO_SEL " &_
			    ") AS 'ESTADOCONTACTO', " &_
			    "LOWER(isnull((Select top 1 ta.DESCRIZIONE From TISTUD ti inner join tades ta on ti.COD_LIV_STUD = ta.CODICE where id_persona = a.id_persona and ta.NOME_TABELLA = 'LSTUD' Order by AA_stud desc, dt_tmst desc),'-')) 'NivelEducativo',  " &_
				"RS.OFFER_TITLE AS 'TITOFERTA'  " &_
				"FROM PERSONA A  " &_
				"LEFT OUTER JOIN STATO_OCCUPAZIONALE C ON A.ID_PERSONA = C.ID_PERSONA AND C.IND_STATUS = 0  " &_
				"INNER JOIN RICHIESTA_CANDIDATO D ON D.ID_PERSONA = A.ID_PERSONA " &_
				"INNER JOIN RICHIESTA_SEDE RS ON RS.ID_RICHIESTA = D.ID_RICHIESTA  " &_
				"WHERE  D.ID_PERSONA = A.ID_PERSONA " &_
				"and A.ID_PERSONA not in (select ID_PERSONA from PERSONAS_JOB_PROFILE where AUTHORIZATIONOFDATAVISUALIZATION =3 ) "
				if request("tipDoc") <> "" and request("doc_identidad") <> "" then
					sSQLExist = sSQLExist & "AND a.COD_FISC like'%" & request("tipDoc")&request("doc_identidad") & "%' "
				end if
				if request("nombre") <>"" then
					sSQLExist = sSQLExist & "AND A.nome + ' ' + A.cognome LIKE '%" & request("nombre") & "%' "
				end if				
				if request("vacante") <>"" then
					sSQLExist = sSQLExist & "AND D.id_richiesta = " & request("vacante") & " "
				end if
				if request("estado") <>"" then
					sSQLExist = sSQLExist & "AND D.COD_ESITO_SEL = '" & request("estado") & "' "
				end if
				if request("centro_atencion") <>"" then
					sSQLExist = sSQLExist & " and RS.COD_OFICINA = '" & request("centro_atencion") & "' "
				end if
				if request("fecini") <>"" and request("fecfin") <>"" then
					sSQLExist = sSQLExist & " and convert(datetime,convert(varchar(25),DT_INS, 103), 103) >= convert(datetime,convert(varchar(25),'" & request("fecini") & " 00:00:00', 103), 103) and " &_
					            " convert(datetime,convert(varchar(25),DT_INS, 103), 103) <= convert(datetime,convert(varchar(25),'" & request("fecfin") & " 00:00:00', 103), 103) "
				end if
				
	sSQLExist = sSQLExist & "ORDER BY a.COGNOME"

	'response.WRITE sSQLExist
	'RESPONSE.end


	'response.Write "*********************************************************"
	'response.Write "NOMBRE EMRESA -- " & request("nombre_empresa") & "</br>"
	'response.Write "SEDE EMPRESA -- " & request("sede_empresa") & "</br>"
	'response.Write "DEPARTAMENTO -- " & request("departamento") & "</br>"
	'response.Write "CIUDAD -- " & request("ciudad") & "</br>"
	'response.Write "FECHA INSCRIPCIÓN FROM -- " & request("fecha_inscripcion_from") & "</br>"
	'response.Write "FECHA INSCRIPCIÓN TO -- " & request("fecha_inscripcion_to") & "</br>"
	'response.Write "*********************************************************"
		
	
	'rsRegisteredPostuldos.CursorType = adOpenstatic 
	set rsRegisteredPostuldos = CC.Execute(sSQLExist)

	if request("search") = "search" then
		session("pagina") = 1
	end if

	rsRegisteredPostuldos.pagesize=10

	if rsRegisteredPostuldos.pagecount <> 0 then
		if session("pagina")>rsRegisteredPostuldos.pagecount then
				session("pagina")=rsRegisteredPostuldos.pagecount 'evitamos el error de reload
		end if

		' controlando avance de pagina
		
		dim direccion 
		'if request("direccion") = "" then		
	''		direccion = "Atras"
	''	else
			direccion = request("direccion")
	''	end if

		
		if session("pagina") = "" then
			session("pagina") = 1
		else

			if direccion = "Adelante" and session("pagina")<rsRegisteredPostuldos.pagecount then
				session("pagina") = session("pagina") + 1
			else 
				if direccion = "Atras" and session("pagina")>1 then
					session("pagina") = session("pagina") - 1
				end if	 
			end if	 
		end if

		if session("pagina")<>"" then
			rsRegisteredPostuldos.absolutepage=session("pagina")
		else 
			rsRegisteredPostuldos.absolutepage=1
		end if

		inicio=1+(session("pagina")-1)*rsRegisteredPostuldos.pagesize
		fin=inicio+rsRegisteredPostuldos.pagesize - 1
		if fin > rsRegisteredPostuldos.recordcount then
			fin =rsRegisteredPostuldos.recordcount
		end if

	end if 

	
	function roundUp(x)
	   If x > Int(x) then
	    roundup = Int(x) + 1
	   Else
	    roundup = x
	   End If
	End Function

%>

<script language="Javascript"> 
$(function(){

	$("#form_next").submit(function(){
		$("#doc_identidad_next").val($("#doc_identidad").val());
		$("#nombre_next").val($("#nombre").val());
		$("#vacante_next").val($("#vacante").val());
		$("#estado_next").val($("#estado").val());
		$("#centro_atencion_next").val($("#centro_atencion").val());
		$("#fecini_next").val($("#fecini").val());
		$("#fecfin_next").val($("#fecfin").val());
		return true;
	});


	$("#next").click(function(){
		$("#form_next").submit();		
	});

	$("#form_back").submit(function(){
		$("#doc_identidad_back").val($("#doc_identidad").val());
		$("#nombre_back").val($("#nombre").val());
		$("#vacante_back").val($("#vacante").val());
		$("#estado_back").val($("#estado").val());
		$("#centro_atencion_back").val($("#centro_atencion").val());
		$("#fecini_back").val($("#fecini").val());
		$("#fecfin_back").val($("#fecfin").val());
		return true;
	});
	

	$("#back").click(function(){
		$("#form_back").submit();
	});	


});
</script>




<form name="cerca" id="form_search" action="reportPostuEstad.asp" Method="Post">
	</br>
	<table border="0" width="700" CELLPADDING="0" cellspacing="0">
		<tr>
			<td align="left" class="sfondomenu" width="300">
				<span class="tbltext0">
				<b>&nbsp;REPORTE POSTULADOS Y SUS ESTADOS</b></span>
			</td>
			<td width="25" valign="bottom" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">
				<img border="0" src="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
			<td width="40%" valign="middle" align="right" class="tbltext1" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
		</tr>
	</table>
		
	<table border="0" width="700" CELLPADDING="0" cellspacing="0">
	    <tr>
			<td class="sfondocomm" align="left" class="tbltext1">
			En esta secci&oacute;n se puede efectuar una b&uacute;squeda de los postulados.</br></br>
			<!-- Puede filtrar la b&uacute;squeda ingresando el apellido y nombre del usuario. -->
			</td>
			<td class="sfondocomm">
				<a href="Javascript:Show_Help('/Pgm/Help/ProjectSite/Rubrica/default/')">
				<img src="<%=Session("Progetto")%>/images/Help.gif" border="0" align="right" alt="Help">
			</td> 
	    </tr>

		<tr height="2">
			<td class="sfondocomm" width="100%" colspan="2" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>
	<table width="700" border="0" class="sfondocomm">
		<tr class="sfondocomm">
			<td colspan="3"><b>B&Uacute;SQUEDA DE POSTULADOS</b></td>		
		</tr>
		<tr style="text-align:center;">
			<td class="tbltext1" width="30%"><b>Tipo Documento</b></td>			
			<td class="tbltext1" width="30%"><b>Documento</b></td>
			<td class="tbltext1" width="30%"><b>Nombre</b></td>
		</tr>
		<tr style="text-align:center;">
			<td class="tbltext1">
			<select name="tipDoc" id="tipDoc" Class="textblack">
			<% sqlTipDoc = "select * from TADES where TADES.NOME_TABELLA = 'DOCST' order by DESCRIZIONE " %>
			<% set rsTipDoc = CC.Execute(sqlTipDoc) %>
			<% contador=0
				
				%><option value=""></option><%
				Do While contador < rsTipDoc.pagesize and NOT rsTipDoc.EOF%>
				  <% if request("tipDoc")<>"" and request("tipDoc")=rsTipDoc("CODICE") then %>
					<option value=<%= rsTipDoc("CODICE")%> selected><%= rsTipDoc("DESCRIZIONE")%></option>
				  <% else %>
				  	<option value=<%= rsTipDoc("CODICE")%> ><%= rsTipDoc("DESCRIZIONE")%></option>
				<% end if
				rsTipDoc.MoveNext
				contador=contador+1
				Loop
			%>
			</select>
			</td>
			<td class="tbltext1">
				<input type="text" Class="textblack" id="doc_identidad" name="doc_identidad" value="<%=request("doc_identidad")%>" maxlength="50" />
			</td>
			<td class="tbltext1">
				<input type="text" Class="textblack" id="nombre" name="nombre" value="<%=request("nombre")%>" maxlength="50" />
			</td>
		</tr>		
		<tr style="text-align:center;">
			<td class="tbltext1" width="30%"><b>Estado</b></td>			
			<td class="tbltext1" width="30%"><b>Fecha inicial</b></td>
			<td class="tbltext1" width="30%"><b>Fecha final</b></td>
		</tr>
		<tr style="text-align:center;">
			<td class="tbltext1">
				<select name="estado" id="estado" Class="textblack">
				<% sqlEstado = "select * from TADES where TADES.NOME_TABELLA = 'ESSEL' order by DESCRIZIONE " %>
				<% set rsEstado = CC.Execute(sqlEstado) %>
				<% contador=0
					
					%><option value=""></option><%
					Do While contador < rsEstado.pagesize and NOT rsEstado.EOF%>
					  <% if request("estado")<>"" and request("estado")=rsEstado("CODICE") then %>
						<option value=<%= rsEstado("CODICE")%> selected><%= rsEstado("DESCRIZIONE")%></option>
					  <% else %>
					  	<option value=<%= rsEstado("CODICE")%> ><%= rsEstado("DESCRIZIONE")%></option>
					<% end if
					rsEstado.MoveNext
					contador=contador+1
					Loop
				%>
				</select>
			</td>			
			<td class="tbltext1">
				<input type="text" Class="textblack fecha" id="fecini" name="fecini" value="<%=request("fecini")%>" maxlength="50" style="" />
			</td>
			<td class="tbltext1">
				<input type="text" Class="textblack fecha" id="fecfin" name="fecfin" value="<%=request("fecfin")%>" maxlength="50" style="" />
			</td>
		</tr>
		<tr style="text-align:center;">
			<td class="tbltext1" width="30%"><b>Centro de atenci&oacute;n</b></td>
			<td class="tbltext1" width="30%"><b>No. vacante</b></td>
			<td width="30%" rowspan="2">
		   		<input type="image" alt="Cerca" src="<%=Session("Progetto")%>/images/lente.gif" id="search" name="search" value="search">				
			</td>
		</tr>
		<tr style="text-align:center;">
			<td class="tbltext1">				
				<select name="centro_atencion" id="centro_atencion" Class="textblack">
				<% contador=0 %>
				<% sSQLAttentionCenter = "select SI.ID_IMPRESA, SI.ID_SEDE, I.RAG_SOC, SI.DESCRIZIONE AS centro " &_ 
						"from SEDE_IMPRESA SI " &_ 
						"inner join IMPRESA I ON I.ID_IMPRESA = SI.ID_IMPRESA " &_ 
						"where COD_TIMPR = '03' " %>
				<%	set rsAttentionCenter = CC.Execute(sSQLAttentionCenter) %>
				<option value=""></option>
				<%
					Do While contador < rsAttentionCenter.pagesize and NOT rsAttentionCenter.EOF%>
					  <% if request("centro_atencion")<>"" and trim(request("centro_atencion"))=cstr(rsAttentionCenter("id_sede")) then %>
						<option value=<%=rsAttentionCenter("id_sede")%> selected><%= rsAttentionCenter("centro")%></option>
					  <% else %>
					  	<option value=<%=rsAttentionCenter("id_sede")%> ><%= rsAttentionCenter("centro")%></option>
					<% end if %>
					<%
					rsAttentionCenter.MoveNext
					contador=contador+1
					Loop
				%>
				</select>
			</td>
			<td class="tbltext1">
				<input type="text" Class="textblack" id="vacante" name="vacante" value="<%=request("vacante")%>" maxlength="50" />
			</td>			
		</tr>
		<tr><td></td><td></td><td><div style="margin:auto; border-radius:10px; padding-top:8px; padding-bottom:8px; width:80%; text-align:center;" class="tbltext0 tblsfondo3"><b>TOTAL POSTULADOS</b> </br> <% response.Write rsTotal("TotalEmpresas") %></div></td></tr>
	</table>	
</form>
</br>


<% if not rsRegisteredPostuldos.EOF then %>  
		</br></br>
		<table border="0" width="700">
			<tr class="tbltext0 tblsfondo3" style="text-align:center;">
				<td width="10%" style="border-radius:10px 0 0 0;"><b>Documento</b></td>
				<td width="40%"><b>Nombre</b></td>
				<td width="10%"><b>Edad</b></td>
				<td width="10%"><b>No. Vacante</b></td>
				<td width="20%"><b>Oferta</b></td>
				<td width="10%" style="border-radius:0 10px 0 0;"><b>Estado</b></td>
			</tr>
		<!-- montamos el bucle para mostrar los registros -->
		<% contador=0
		Do While contador < rsRegisteredPostuldos.pagesize and NOT rsRegisteredPostuldos.EOF%>
			<tr class="tblsfondo">
				<td style="text-align:center;"><%= rsRegisteredPostuldos("IDENTIFICACION")%></td>
				<td><%= rsRegisteredPostuldos("NOMBRE") %></td>
				<td><%= rsRegisteredPostuldos("EDAD") %></td>
				<td><%= rsRegisteredPostuldos("VACANTE") %></td>
				<td><%= rsRegisteredPostuldos("TITOFERTA") %></td>
				<td><%= rsRegisteredPostuldos("ESTADOCONTACTO") %></td>				
			</tr>

		<% rsRegisteredPostuldos.MoveNext
		contador=contador+1
		Loop
		%>

		</table>

	<!-- mostramos los botones de adelante y atras segun proceda -->
	<table border="0" width="100%" class="tbltext0 tblsfondo3" style="margin:auto;">
		<tr>
			<td width="10px" style="text-align:left;">
				<%if session("pagina") <> 1 then %>
					<form method="POST" action="reportPostuEstad.asp" id="form_back">
						<p>
							<a href="#" id="back">
								<img type="image" src="../../images/previous.png" width="24px" height="24px" style="border:0px;">
								<input type="hidden" alt="Atras" value="Atras" src="../../images/previous.png" id="imageBack" name="direccion" >
							</a>
							<!-- input type="image" id="back" alt="Atras" value="Atras" src="../../images/previous.png" id="imageBack" name="direccion" width="24px" height="24px" -->
							<input type="hidden" Class="textblack" id="doc_identidad_back" name="doc_identidad" value="<%=request("doc_indentidad")%>" maxlength="50" />
							<input type="hidden" Class="textblack" id="nombre_back" name="nombre" value="<%=request("nombre")%>" maxlength="50" />
							<input type="hidden" Class="textblack" id="vacante_back" name="vacante" value="<%=request("vacante")%>" maxlength="50" />
							<input type="hidden" Class="textblack" id="estado_back" name="estado" value="<%=request("estado")%>" maxlength="50" />
							<input type="hidden" Class="textblack" id="centro_atencion_back" name="centro_atencion" value="<%=request("centro_atencion")%>" maxlength="50" />
							<input type="hidden" Class="textblack" id="fecini_back" name="fecini" value="<%=request("fecini")%>" maxlength="50" />
							<input type="hidden" Class="textblack" id="fecfin_back" name="fecfin" value="<%=request("fecfin")%>" maxlength="50" />
						</p>
					</form>
				<%end if%>
			</td>
			<td width="40%" style="text-align:left;">Registros 
				<span style="color:#FF0000;"><%=inicio%></span> al 
				<span style="color:#FF0000;"><%=fin%></span> de un total de
				<span style="color:#FF0000;"><%=rsRegisteredPostuldos.recordcount%></span>
			</td>
			<td width="40%" style="text-align:right;">P&aacute;gina 
				<span style="color:#FF0000;"><%=session("pagina")%></span> de 
				<span style="color:#FF0000;"><%=rsRegisteredPostuldos.pagecount%></span>
			</td>
			<td width="10%" style="text-align:right;">
				<% if session("pagina") < rsRegisteredPostuldos.pagecount then %>
					<form method="POST" name="reportPostuEstad.asp" id="form_next">
						<p>
							<a href="#" id="next">
								<img type="image" src="../../images/next.png" width="24px" height="24px" style="border:0px;">
								<input type="hidden" alt="Adelante" value="Adelante" src="../../images/next.png" id="imageNext" name="direccion" >
							</a>
							<!-- input type="image" alt="Adelante" value="Adelante" src="../../images/next.png" id="imageNext" name="direccion" width="24px" height="24px" -->

							<input type="hidden" Class="textblack" id="doc_identidad_next" name="doc_identidad" value="<%=request("doc_identidad")%>" maxlength="50" />
							<input type="hidden" Class="textblack" id="nombre_next" name="nombre" value="<%=request("nombre")%>" maxlength="50" />
							<input type="hidden" Class="textblack" id="vacante_next" name="vacante" value="<%=request("vacante")%>" maxlength="50" />							
							<input type="hidden" Class="textblack" id="estado_next" name="estado" value="<%=request("estado")%>" maxlength="50" />
							<input type="hidden" Class="textblack" id="centro_atencion_next" name="centro_atencion" value="<%=request("centro_atencion")%>" maxlength="50" />							
							<input type="hidden" Class="textblack" id="fecini_next" name="fecini" value="<%=request("fecini")%>" maxlength="50" />							
							<input type="hidden" Class="textblack" id="fecfin_next" name="fecfin" value="<%=request("fecfin")%>" maxlength="50" />							
						</p>
					</form>
				<%end if%>
			</td>
		</tr>
	</table>

	<%
		'******************************************************* EXPORTAR A EXCEL *******************************************************
		Dim encabezadosExcel 
		encabezadosExcel = "VACANTE,IDENTIFICACION,EDAD,DEPARTAMENTO,CIUDAD,NOMBRE,BARRIO,DIRECCION,TELEFONO,SITUACIONLABORAL,FECHAPOSTULACION,ESTADOCONTACTO,NIVELEDUCATIVO,TITOFERTA"
		encabezadosExcelBase64 = Base64Encode(encabezadosExcel)

		Dim encabezadosQuery 
		encabezadosQuery = "VACANTE,IDENTIFICACION,EDAD,DEPARTAMENTO,CIUDAD,NOMBRE,BARRIO,DIRECCION,TELEFONO,SITUACIONLABORAL,FECHAPOSTULACION,ESTADOCONTACTO,NIVELEDUCATIVO,TITOFERTA"

		encabezadosQueryBase64 = Base64Encode(encabezadosQuery)

		Dim reportFilterData 
		reportFilterData = "doc_identidad:"&request("TipDoc")&request("doc_identidad")&",nombre:"&request("nombre")&",vacante:"&request("vacante")&",estado:"&request("estado")&",centro_atencion:"&request("centro_atencion")&",fecini:"&request("fecini")&",fecfin:"&request("fecfin")
		reportFilterDataBase64 = Base64Encode(reportFilterData)
		'********************************************************************************************************************************

	%>

	<input type="button" value="Exportar a Excel" onclick="javascript:exporttoExcelGeneric('reportPostuEstad','REPORTE POSTULADOS Y SUS ESTADOS','<%=reportFilterDataBase64%>','<%=encabezadosExcelBase64%>','<%=encabezadosQueryBase64%>')">


<% else%>
	<h1 class="tbltext0 tblsfondo3" style="width:80%; border-radius:10px; font-size:20px;">No se encontraron coincidencias</h1>
<% end if %>

<!-- #include Virtual="/strutt_coda2.asp" -->
<!-- #include virtual="/include/CloseConn.asp" -->



