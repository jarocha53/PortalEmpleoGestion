<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #include Virtual="/strutt_testa2.asp" -->
<!-- #include virtual="/include/OpenConn.asp" -->
<!-- #include virtual="/include/DecCod.asp" -->

<!--*************************** INCLUDE BASE 64 FUNCTION ****************** -->
<!-- #include virtual="/include/Base64.asp" -->
<!--*********************************************************************** -->

<!--*********************************************************************** -->
<!--Busca a la persona o grupo que le queremos enviar un mensaje ' RKL-->
<!--***********************************************************************-->
<%
	'@LANGUAGE="VBSCRIPT" CODEPAGE="65001" 
	'Session.Codepage=65001
	'Response.ContentType = "text/HTML" 
	'Response.CodePage = 65001
	'Response.CharSet = "utf-8"


	'****************************************************************************************************
	'Cargando data Grupo Ocupacional
	'sSQLOcupationalGroups = "SELECT ID_AREAPROF, DENOMINAZIONE FROM AREE_PROFESSIONALI ORDER BY DENOMINAZIONE ASC"
	'set rsOcupationalGroups = CC.Execute(sSQLOcupationalGroups)
	'****************************************************************************************************	
	'Consulta Global
			
	'sSQLExist1 = "SELECT TI.ID_PERSONA, MAX(TITAD.VALORE) VALORE INTO ##TIMAS9870 " &_
	''			"FROM TISTUD TI with(nolock) " &_
	''			"INNER JOIN (SELECT * FROM TADES with(nolock)  WHERE NOME_TABELLA= 'LSTUD') TITAD " &_
     ''    				"ON TI.COD_LIV_STUD = TITAD.CODICE " &_
 	''			"GROUP BY TI.ID_PERSONA"
 	'set rsRegisteredPeople1 = CC.Execute(sSQLExist1)


	'sSQLExist2 = "SELECT P.ID_PERSONA, P.SESSO, ISNULL(TIMAS.VALORE,'ND') AS VALORE INTO ##CURSON49857 " &_
	''			"FROM PERSONA P with(nolock) LEFT JOIN  ##TIMAS9870 TIMAS ON P.ID_PERSONA = TIMAS.ID_PERSONA " &_
	''			"WHERE P.ID_PERSONA IN (SELECT DISTINCT ID_PERSONA FROM STATO_OCCUPAZIONALE with(nolock) WHERE " &_  
	''				"ID_CIMPIEGO = 211263) " &_
	''			"ORDER BY P.ID_PERSONA "
	'Set rsRegisteredPeople2 = CC.Execute(sSQLExist2)

	sSQLExist3 = "SELECT TI.ID_PERSONA, MAX(TITAD.VALORE) VALORE INTO ##TIMAS9870 " &_
				"FROM TISTUD TI with(nolock) " &_
				"INNER JOIN (SELECT * FROM TADES with(nolock)  WHERE NOME_TABELLA= 'LSTUD') TITAD " &_
         			"ON TI.COD_LIV_STUD = TITAD.CODICE " &_
 				"GROUP BY TI.ID_PERSONA " 
 	'set rsRegisteredPeople3 = CC.Execute(sSQLExist3)
 
	sSQLExist4 = "SELECT P.ID_PERSONA, P.SESSO, ISNULL(TIMAS.VALORE,'ND') AS VALORE INTO ##CURSON49857 " &_
				"FROM PERSONA P with(nolock) LEFT JOIN  ##TIMAS9870 TIMAS " &_
                	"ON P.ID_PERSONA = TIMAS.ID_PERSONA " &_
				"WHERE P.ID_PERSONA IN (SELECT DISTINCT ID_PERSONA FROM STATO_OCCUPAZIONALE with(nolock) WHERE ID_CIMPIEGO is not null) " &_
				"ORDER BY P.ID_PERSONA "
	'set rsRegisteredPeople4 = CC.Execute(sSQLExist4)

	sSQLExist5 = "SELECT p.ID_PERSONA, " &_
				"p.COD_FISC AS 'IDENTIFICACION', " &_
				"replace(p.NOME,'&#209;','�') AS 'PRIMER NOMBRE', " &_
				"replace(p.SECONDO_NOME,'&#209;','�') AS 'SEGUNDO NOMBRE', " &_
				"replace(p.COGNOME,'&#209;','�') AS 'PRIMER APELLIDO', " &_
				"replace(p.SECONDO_COGNOME,'&#209;','�') AS 'SEGUNDO APELLIDO', " &_
				"floor(datediff(MONTH, p.DT_NASC, getdate()) / 12) as 'EDAD', " &_
				"case when p.SESSO='F' then 'FEMENINO' else 'MASCULINO' end as 'SEXO', " &_
				"dep.DESCRIZIONE AS 'DEPARTAMENTO DE RESIDENCIA', " &_
				"Ciudad.DESCOM AS 'CIUDAD DE RESIDENCIA', " &_
				"p.FRAZIONE_RES AS 'BARRIO DE RESIDENCIA', " &_
				"P.NUM_TEL AS 'TELEFONO FIJO', " &_
				"P.NUM_TEL_DOM AS 'TELEFONO CELULAR', " &_
				"P.E_MAIL AS 'CORREO ELECTRONICO', " &_
				"sim.DESCRIZIONE 'CENTRO DE ATENCION', " &_
				"TITAD.DESCRIZIONE 'MAXIMO NIVEL EDUCATIVO', " &_
				"t.DESCRIZIONE  as 'SITUACION LABORAL ACTUAL', " &_
				"min(convert(varchar(25), SO.DT_DICH_STATUS, 103)) AS 'FECHA DE INSCRIPCION', " &_
				"floor(DATEDIFF(day,SO.DT_DICH_STATUS,GETDATE())) AS 'DIAS DE INSCRIPCION' " &_
				"FROM persona p " &_
				"inner join stato_occupazionale so on p.id_persona=so.id_persona " &_
				"inner join tades t on so.cod_stdis=t.CODICE and NOME_TABELLA ='STDIS' " &_
				"inner join SEDE_IMPRESA sim on sim.id_sede = so.ID_CIMPIEGO " &_
				"inner join (select CODICE, DESCRIZIONE from tades where nome_tabella = 'PROV') Dep " &_
						"on p.PRV_RES =dep.CODICE " &_
				"inner join (SELECT CODCOM,DESCOM from COMUNE) Ciudad on p.COM_RES=Ciudad.CODCOM " &_
				"inner join ##CURSON49857 C on C.ID_PERSONA = p.ID_PERSONA " &_
				"INNER JOIN (SELECT VALORE, DESCRIZIONE FROM TADES with(nolock) WHERE NOME_TABELLA= 'LSTUD' UNION( SELECT 'ND', 'No Disponible')) TITAD on c.valore = TITAD.valore " &_
				"WHERE so.id_cimpiego is not null " &_
				"AND (ind_status = '0' OR (ind_status = '2' " &_
				"AND P.ID_PERSONA not in ( SELECT ID_PERSONA FROM STATO_OCCUPAZIONALE WHERE p.id_persona = id_persona AND ind_status = '0' ))) " &_
				"AND SO.DT_DICH_STATUS >= convert(datetime,'01/05/2013 10:0:0',103) " &_
				"group BY p.ID_PERSONA, p.COD_FISC,replace(p.NOME,'&#209;','�') , replace(p.SECONDO_NOME,'&#209;','�'), replace(p.COGNOME,'&#209;','�'), replace(p.SECONDO_COGNOME,'&#209;','�'), floor(datediff(MONTH, p.DT_NASC, getdate()) / 12),p.SESSO, floor(DATEDIFF(day,SO.DT_DICH_STATUS,GETDATE())),dep.DESCRIZIONE,Ciudad.DESCOM,P.NUM_TEL,P.NUM_TEL_DOM, P.E_MAIL, P.FRAZIONE_RES, sim.DESCRIZIONE,t.DESCRIZIONE,TITAD.DESCRIZIONE " &_
				"ORDER BY 'PRIMER NOMBRE', 'SEGUNDO NOMBRE', 'PRIMER APELLIDO', 'SEGUNDO APELLIDO' " &_
				"DROP TABLE ##TIMAS9870 DROP TABLE ##CURSON49857 "



				sSQLExist = "SET NOCOUNT ON; " & sSQLExist3 & " " & sSQLExist4 & " " & sSQLExist5

	'sSQLExist6 = "EXEC [dbo].[reportePersonasInscritas] " &_
	''				"SELECT	@return_value"
		set rsRegisteredPeople = CC.Execute(sSQLExist)

	
	'rsRegisteredPeople.CursorType = adOpenstatic 


	if request("search") = "search" then
		session("pagina") = 1
	end if

	rsRegisteredPeople.pagesize=10

	if rsRegisteredPeople.pagecount <> 0 then
		if session("pagina")>rsRegisteredPeople.pagecount then
				session("pagina")=rsRegisteredPeople.pagecount 'evitamos el error de reload
		end if

		' controlando avance de pagina
		
		dim direccion 
		'if request("direccion") = "" then		
	''		direccion = "Atras"
	''	else
			direccion = request("direccion")
	''	end if

		
		if session("pagina") = "" then
			session("pagina") = 1
		else

			if direccion = "Adelante" and session("pagina")<rsRegisteredPeople.pagecount then
				session("pagina") = session("pagina") + 1
			else 
				if direccion = "Atras" and session("pagina")>1 then
					session("pagina") = session("pagina") - 1
				end if	 
			end if	 
		end if

		if session("pagina")<>"" then
			rsRegisteredPeople.absolutepage=session("pagina")
		else 
			rsRegisteredPeople.absolutepage=1
		end if

		inicio=1+(session("pagina")-1)*rsRegisteredPeople.pagesize
		fin=inicio+rsRegisteredPeople.pagesize - 1
		if fin > rsRegisteredPeople.recordcount then
			fin =rsRegisteredPeople.recordcount
		end if

	end if 

	
	function roundUp(x)
	   If x > Int(x) then
	    roundup = Int(x) + 1
	   Else
	    roundup = x
	   End If
	End Function

%>

<script language="Javascript"> 
$(function(){

	$("#next").click(function(){
		$("#form_next").submit();		
	});

	$("#back").click(function(){
		$("#form_back").submit();
	});	


});
</script>


<form name="cerca" id="form_search" action="reportRegisteredPeople.asp" Method="Post">
	</br>
	<table border="0" width="90%" CELLPADDING="0" cellspacing="0">
		<tr>
			<td align="left" class="sfondomenu" width="199">
				<span class="tbltext0">
				<b>&nbsp;REPORTE DE PERSONAS INSCRITAS</b></span>
			</td>
			<td width="25" valign="bottom" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">
				<img border="0" src="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
			<td width="30%" valign="middle" align="right" class="tbltext1" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
		</tr>
	</table>
		
	<table border="0" width="90%" CELLPADDING="0" cellspacing="0">
	    <tr>
			<td class="sfondocomm" align="left" class="tbltext1">
			En esta secci&oacute;n se puede efectuar una b&uacute;squeda de personas inscritas.</br></br>
			</td>
			<td class="sfondocomm">
				<a href="Javascript:Show_Help('/Pgm/Help/ProjectSite/Rubrica/default/')">
				<img src="<%=Session("Progetto")%>/images/Help.gif" border="0" align="right" alt="Help">
			</td> 
	    </tr>

		<tr height="2">
			<td class="sfondocomm" width="100%" colspan="2" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>
</form>
</br>


<% if not rsRegisteredPeople.EOF then %>
		</br></br>
		<table border="0" width="90%">
			<tr class="tbltext0 tblsfondo3" style="text-align:center;">
				<td width="25%" style="border-radius:10px 0 0 0;"><b>Identificaci&oacute;n</b></td>
				<td width="35%"><b>Nombre</b></td>
				<td width="25%"><b>Departamento Residencia</b></td>
				<td width="25%"><b>Ciudad Residencia</b></td>
				<td width="15%" style="border-radius:0 10px 0 0;"><b>Fecha de Inscripci&oacute;n</b></td>
			</tr>
		<!-- montamos el bucle para mostrar los registros -->
		<% contador=0

		Do While contador < rsRegisteredPeople.pagesize and NOT rsRegisteredPeople.EOF%>
			<tr class="tblsfondo">
				<td style="text-align:left; padding-left: 10px;"><%= rsRegisteredPeople("IDENTIFICACION")%></td>
				<td style="padding-left: 10px;"><%= rsRegisteredPeople("PRIMER NOMBRE") & " " & rsRegisteredPeople("SEGUNDO NOMBRE") & " " & rsRegisteredPeople("PRIMER APELLIDO") & " " & rsRegisteredPeople("SEGUNDO APELLIDO") %></td>
				<td style="text-align:center;"><%= rsRegisteredPeople("DEPARTAMENTO DE RESIDENCIA") %></td>
				<td style="text-align:center;"><%= rsRegisteredPeople("CIUDAD DE RESIDENCIA") %></td>
				<td style=" text-align:center;"><%= rsRegisteredPeople("FECHA DE INSCRIPCION") %></td>
			</tr>

		<% rsRegisteredPeople.MoveNext
		contador=contador+1
		Loop
		%>

		</table>

	<!-- mostramos los botones de adelante y atras segun proceda -->
	
	<table border="0" width="90%" style="margin:auto;">
		<tr class="tbltext0 tblsfondo3">
			<td width="10px" style="text-align:left; border-radius:0 0 0 10px; padding-left:10px;">
				<%if session("pagina") <> 1 then %>
					<form method="POST" action="reportRegisteredPeople.asp" id="form_back">
						<p>
							<a href="#" id="back">
								<img type="image" src="../../images/previous.png" width="24px" height="24px" style="border:0px;">
								<input type="hidden" alt="Atras" value="Atras" src="../../images/previous.png" id="imageBack" name="direccion" >
							</a>
							<!-- input type="image" id="back" alt="Atras" value="Atras" src="../../images/previous.png" id="imageBack" name="direccion" width="24px" height="24px" -->

						</p>
					</form>
				<%end if%>
			</td>
			<td width="40%" style="text-align:left; padding-left:10px;">Registros 
				<span style="color:#FF0000;"><%=inicio%></span> al 
				<span style="color:#FF0000;"><%=fin%></span> de un total de
				<span style="color:#FF0000;"><%=rsRegisteredPeople.recordcount%></span>
			</td>
			<td width="40%" style="text-align:right; padding-right:10px;">P&aacute;gina 
				<span style="color:#FF0000;"><%=session("pagina")%></span> de 
				<span style="color:#FF0000;"><%=rsRegisteredPeople.pagecount%></span>
			</td>
			<td width="10%" style="text-align:right;border-radius:0 0 10px 0; padding-right:10px;">
				<% if session("pagina") < rsRegisteredPeople.pagecount then %>
					<form method="POST" name="reportRegisteredPeople.asp" id="form_next">
						<p>
							<a href="#" id="next">
								<img type="image" src="../../images/next.png" width="24px" height="24px" style="border:0px;">
								<input type="hidden" alt="Adelante" value="Adelante" src="../../images/next.png" id="imageNext" name="direccion" >
							</a>
							<!-- input type="image" alt="Adelante" value="Adelante" src="../../images/next.png" id="imageNext" name="direccion" width="24px" height="24px" -->

						</p>
					</form>
				<%end if%>
			</td>
		</tr>
	</table>

	<%
	'******************************************************* EXPORTAR A EXCEL *******************************************************
	Dim encabezadosExcel 
	encabezadosExcel = "IDENTIFICACI�N,PRIMER NOMBRE,SEGUNDO NOMBRE,PRIMER APELLIDO,SEGUNDO APELLIDO,EDAD,SEXO,DEPARTAMENTO DE RESIDENCIA,CIUDAD DE RESIDENCIA,BARRIO DE RESIDENCIA,TEL�FONO FIJO,TEL�FONO CELULAR,CORREO ELECTR�NICO,CENTRO DE ATENCI�N,M�XIMO NIVEL EDUCATIVO,SITUACI�N LABORAL ACTUAL,FECHA DE INSCRIPCI�N,D�AS DE INSCRIPCI�N"
	encabezadosExcelBase64 = Base64Encode(encabezadosExcel)

	Dim encabezadosQuery 
	encabezadosQuery = "IDENTIFICACION,PRIMER NOMBRE,SEGUNDO NOMBRE,PRIMER APELLIDO,SEGUNDO APELLIDO,EDAD,SEXO,DEPARTAMENTO DE RESIDENCIA,CIUDAD DE RESIDENCIA,BARRIO DE RESIDENCIA,TELEFONO FIJO,TELEFONO CELULAR,CORREO ELECTRONICO,CENTRO DE ATENCION,MAXIMO NIVEL EDUCATIVO,SITUACION LABORAL ACTUAL,FECHA DE INSCRIPCION,DIAS DE INSCRIPCION"
	encabezadosQueryBase64 = Base64Encode(encabezadosQuery)

	'********************************************************************************************************************************
	%>
	<!-- input type="button" id="exporttoExcel" value="Exportar a Excel" onclick="javascript:exporttoExcel()" -->
	<!-- exporttoExcelGeneric(reportName,reportTitle,reportFilterDataBase64,encabezadosExcelBase64,encabezadosQueryBase64) -->
	<input type="button" value="Exportar a Excel" onclick="javascript:exporttoExcelGeneric('reportRegisteredPeople','REPORTE DE PERSONAS INSCRITAS',null,'<%=encabezadosExcelBase64%>','<%=encabezadosQueryBase64%>')">



<% else%>
	<h1 class="tbltext0 tblsfondo3" style="width:80%; border-radius:10px; font-size:20px;">No se encontraron coincidencias</h1>
<% end if %>

<!-- #include Virtual="/strutt_coda2.asp" -->
<!-- #include virtual="/include/CloseConn.asp" -->