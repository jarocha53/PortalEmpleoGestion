<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #include Virtual="/strutt_testa2.asp" -->
<!-- #include virtual="/include/OpenConn.asp" -->
<!-- #include virtual="/include/DecCod.asp" -->

<!--*************************** INCLUDE BASE 64 FUNCTION ****************** -->
<!-- #include virtual="/include/Base64.asp" -->
<!--*********************************************************************** -->

<!--*********************************************************************** -->
<!--Busca a la persona o grupo que le queremos enviar un mensaje ' RKL-->
<!--***********************************************************************-->
<%
	'@LANGUAGE="VBSCRIPT" CODEPAGE="65001" 
	'Session.Codepage=65001
	'Response.ContentType = "text/HTML" 
	'Response.CodePage = 65001
	'Response.CharSet = "utf-8"


	'****************************************************************************************************
	'Cargando data Grupo Ocupacional
	'sSQLOcupationalGroups = "SELECT ID_AREAPROF, DENOMINAZIONE FROM AREE_PROFESSIONALI ORDER BY DENOMINAZIONE ASC"
	'set rsOcupationalGroups = CC.Execute(sSQLOcupationalGroups)
	'****************************************************************************************************

	ban = false
	sSQLTotal = "Select count(1) AS 'TotalEntrevistas' " &_
				"From agEntrevistas agE with(nolock) " &_
				"Left Outer Join agEntrevistaEstados agEE with(nolock) " &_
				"On agEE.Estado = agE.Estado " &_
				"Left Outer join agEntrevistaTareas agET with(nolock) " &_
				"On agET.Tarea = agE.TareaUlt " &_
				"Left Outer join PLavoro.dbo.utente U with(nolock) " &_
				"On u.IDUtente = agE.Usuario " 
				if request("centro_atencion") <> "" then
					sSQLTotal = sSQLTotal & "where oficina = "& request("centro_atencion") &" "
					ban = true
				end if
				if request("fecini") <>"" and request("fecfin") <>"" then
					if ban then
						sSQLTotal = sSQLTotal & " and convert(datetime,convert(varchar(25),agE.FECHAALTA, 103), 103) >= convert(datetime,convert(varchar(25),'" & request("fecini") & " 00:00:00', 103), 103) and " &_
					            " convert(datetime,convert(varchar(25),agE.FECHAALTA, 103), 103) <= convert(datetime,convert(varchar(25),'" & request("fecfin") & " 00:00:00', 103), 103) "
					else
						sSQLTotal = sSQLTotal & " where convert(datetime,convert(varchar(25),agE.FECHAALTA, 103), 103) >= convert(datetime,convert(varchar(25),'" & request("fecini") & " 00:00:00', 103), 103) and " &_
					            " convert(datetime,convert(varchar(25),agE.FECHAALTA, 103), 103) <= convert(datetime,convert(varchar(25),'" & request("fecfin") & " 00:00:00', 103), 103) "
					end if
					
				end if
				
	'response.WRITE sSQLTotal
	'RESPONSE.end
	set rsTotal = connLavoro.Execute(sSQLTotal)

	ban = false
	'Consulta Global
	sSQLExist = "Select 	CUIL Identificacion,  " &_
				"U.nome + '-' + U.cognome as 'Entrevistador', " &_
				"ENTREVISTANRO as 'Entrevista', " &_
				"CITANRO , " &_ 
				"agEE.Descripcion as ESTADO, " &_
				"agEt.Descripcion as TAREA, " &_
				"convert(varchar,agE.FECHAALTA,103) + ' ' + convert(varchar,agE.FECHAALTA,108) as 'FECINICIO', " &_
				"FECHAULTMOD " &_
				"From agEntrevistas agE with(nolock) " &_
				"Left Outer Join agEntrevistaEstados agEE with(nolock) " &_
				"On agEE.Estado = agE.Estado " &_
				"Left Outer join agEntrevistaTareas agET with(nolock) " &_
				"On agET.Tarea = agE.TareaUlt " &_
				"Left Outer join PLavoro.dbo.utente U with(nolock) " &_
				"On u.IDUtente = agE.Usuario " 
				if request("centro_atencion") <> "" then
					sSQLExist = sSQLExist & "where oficina = "& request("centro_atencion") &" "
					ban = true
				end if
				if request("fecini") <>"" and request("fecfin") <>"" then
					if ban then
						sSQLExist = sSQLExist & " and convert(datetime,convert(varchar(25),agE.FECHAALTA, 103), 103) >= convert(datetime,convert(varchar(25),'" & request("fecini") & " 00:00:00', 103), 103) and " &_
					            " convert(datetime,convert(varchar(25),agE.FECHAALTA, 103), 103) <= convert(datetime,convert(varchar(25),'" & request("fecfin") & " 00:00:00', 103), 103) "
					else
						sSQLExist = sSQLExist & " where convert(datetime,convert(varchar(25),agE.FECHAALTA, 103), 103) >= convert(datetime,convert(varchar(25),'" & request("fecini") & " 00:00:00', 103), 103) and " &_
					            " convert(datetime,convert(varchar(25),agE.FECHAALTA, 103), 103) <= convert(datetime,convert(varchar(25),'" & request("fecfin") & " 00:00:00', 103), 103) "
					end if
					
				end if
				
				
	sSQLExist = sSQLExist & "ORDER BY ENTREVISTANRO"

	'response.WRITE sSQLExist
	'RESPONSE.end


	'response.Write "*********************************************************"
	'response.Write "NOMBRE EMRESA -- " & request("nombre_empresa") & "</br>"
	'response.Write "SEDE EMPRESA -- " & request("sede_empresa") & "</br>"
	'response.Write "DEPARTAMENTO -- " & request("departamento") & "</br>"
	'response.Write "CIUDAD -- " & request("ciudad") & "</br>"
	'response.Write "FECHA INSCRIPCIÓN FROM -- " & request("fecha_inscripcion_from") & "</br>"
	'response.Write "FECHA INSCRIPCIÓN TO -- " & request("fecha_inscripcion_to") & "</br>"
	'response.Write "*********************************************************"
		
	
	'rsRegisteredPostuldos.CursorType = adOpenstatic 
	set rsEntrevistas = connLavoro.Execute(sSQLExist)

	if request("search") = "search" then
		session("pagina") = 1
	end if

	rsEntrevistas.pagesize=10

	if rsEntrevistas.pagecount <> 0 then
		if session("pagina")>rsEntrevistas.pagecount then
				session("pagina")=rsEntrevistas.pagecount 'evitamos el error de reload
		end if

		' controlando avance de pagina
		
		dim direccion 
		'if request("direccion") = "" then		
	''		direccion = "Atras"
	''	else
			direccion = request("direccion")
	''	end if

		
		if session("pagina") = "" then
			session("pagina") = 1
		else

			if direccion = "Adelante" and session("pagina")<rsEntrevistas.pagecount then
				session("pagina") = session("pagina") + 1
			else 
				if direccion = "Atras" and session("pagina")>1 then
					session("pagina") = session("pagina") - 1
				end if	 
			end if	 
		end if

		if session("pagina")<>"" then
			rsEntrevistas.absolutepage=session("pagina")
		else 
			rsEntrevistas.absolutepage=1
		end if

		inicio=1+(session("pagina")-1)*rsEntrevistas.pagesize
		fin=inicio+rsEntrevistas.pagesize - 1
		if fin > rsEntrevistas.recordcount then
			fin =rsEntrevistas.recordcount
		end if

	end if 

	
	function roundUp(x)
	   If x > Int(x) then
	    roundup = Int(x) + 1
	   Else
	    roundup = x
	   End If
	End Function

%>

<script language="Javascript"> 
$(function(){

	$("#form_next").submit(function(){		
		$("#centro_atencion_next").val($("#centro_atencion").val());
		$("#fecini_next").val($("#fecini").val());
		$("#fecfin_next").val($("#fecfin").val());
		return true;
	});


	$("#next").click(function(){
		$("#form_next").submit();		
	});

	$("#form_back").submit(function(){		
		$("#centro_atencion_back").val($("#centro_atencion").val());
		$("#fecini_back").val($("#fecini").val());
		$("#fecfin_back").val($("#fecfin").val());
		return true;
	});
	

	$("#back").click(function(){
		$("#form_back").submit();
	});	


});
</script>




<form name="cerca" id="form_search" action="reportEntrevistas.asp" Method="Post">
	</br>
	<table border="0" width="700" CELLPADDING="0" cellspacing="0">
		<tr>
			<td align="left" class="sfondomenu" width="300">
				<span class="tbltext0">
				<b>&nbsp;REPORTE DE ENTREVISTAS</b></span>
			</td>
			<td width="25" valign="bottom" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">
				<img border="0" src="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
			<td width="40%" valign="middle" align="right" class="tbltext1" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
		</tr>
	</table>
		
	<table border="0" width="700" CELLPADDING="0" cellspacing="0">
	    <tr>
			<td class="sfondocomm" align="left" class="tbltext1">
			En esta secci&oacute;n se puede efectuar una b&uacute;squeda de entrevistas realizadas.</br></br>
			<!-- Puede filtrar la b&uacute;squeda ingresando el apellido y nombre del usuario. -->
			</td>
			<td class="sfondocomm">
				<a href="Javascript:Show_Help('/Pgm/Help/ProjectSite/Rubrica/default/')">
				<img src="<%=Session("Progetto")%>/images/Help.gif" border="0" align="right" alt="Help">
			</td> 
	    </tr>

		<tr height="2">
			<td class="sfondocomm" width="100%" colspan="2" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>
	<table width="700" border="0" class="sfondocomm">
		<tr class="sfondocomm">
			<td colspan="3"><b>B&Uacute;SQUEDA DE ENTREVISTAS</b></td>		
		</tr>
		<tr style="text-align:center;">			
			<td class="tbltext1" width="25%"><b>Fecha inicial</b></td>
			<td class="tbltext1" width="25%"><b>Fecha final</b></td>
			<td class="tbltext1" width="25%"></td>
			<td class="tbltext1" width="25%"></td>
		</tr>
		<tr style="text-align:center;">
			<td class="tbltext1">
				<input type="text" Class="textblack fecha" id="fecini" name="fecini" value="<%=request("fecini")%>" maxlength="50" style="" />
			</td>
			<td class="tbltext1">
				<input type="text" Class="textblack fecha" id="fecfin" name="fecfin" value="<%=request("fecfin")%>" maxlength="50" style="" />
			</td>
			<td>
			  <input type="image" alt="Cerca" src="<%=Session("Progetto")%>/images/lente.gif" id="search" name="search" value="search">				
			</td>
			<td>
			  <div style="margin:auto; border-radius:10px; padding-top:8px; padding-bottom:8px; width:80%; text-align:center;" class="tbltext0 tblsfondo3"><b>TOTAL ENTREVISTAS</b> </br> <% response.Write rsTotal("TotalEntrevistas") %></div>
			</td>
		</tr>
		<tr style="text-align:center;">
			<td class="tbltext1" width="30%"><b>Centro de atenci&oacute;n</b></td>
			<td colspan = "3"></td>
		</tr>
		<tr style="text-align:center;">
			<td class="tbltext1">				
				<select name="centro_atencion" id="centro_atencion" Class="textblack">
				<% contador=0 %>
				<% sSQLAttentionCenter = "select SI.ID_IMPRESA, SI.ID_SEDE, I.RAG_SOC, SI.DESCRIZIONE AS centro " &_ 
						"from SEDE_IMPRESA SI " &_ 
						"inner join IMPRESA I ON I.ID_IMPRESA = SI.ID_IMPRESA " &_ 
						"where COD_TIMPR = '03' " %>
				<%	set rsAttentionCenter = CC.Execute(sSQLAttentionCenter) %>
				<option value=""></option>
				<%
					Do While contador < rsAttentionCenter.pagesize and NOT rsAttentionCenter.EOF%>
					  <% if request("centro_atencion")<>"" and trim(request("centro_atencion"))=cstr(rsAttentionCenter("id_sede")) then %>
						<option value=<%=rsAttentionCenter("id_sede")%> selected><%= rsAttentionCenter("centro")%></option>
					  <% else %>
					  	<option value=<%=rsAttentionCenter("id_sede")%> ><%= rsAttentionCenter("centro")%></option>
					<% end if %>
					<%
					rsAttentionCenter.MoveNext
					contador=contador+1
					Loop
				%>
				</select>
			</td>
			<td colspan="3"></td>			
		</tr>		
	</table>	
</form>
</br>


<% if not rsEntrevistas.EOF then %>  
		</br></br>
		<table border="0" width="90%" style="margin:auto;">
			<tr class="tbltext0 tblsfondo3" style="text-align:center;">
				<td width="10%" style="border-radius:10px 0 0 0;"><b>Identificaci&oacute;n</b></td>
				<td width="40%"><b>Entrevistador</b></td>
				<td width="10%"><b>No. Entrevista</b></td>
				<td width="10%"><b>Sita N&uacute;mero</b></td>
				<td width="10%"><b>Estado</b></td>
				<td width="20%"><b>Tarea</b></td>
				<td width="20%"><b>Fecha inicio entrevista</b></td>
				<td width="10%" style="border-radius:0 10px 0 0;"><b>Fecha ultima modificaci&oacute;</b></td>
			</tr>
		<!-- montamos el bucle para mostrar los registros -->
		<% contador=0
		Do While contador < rsEntrevistas.pagesize and NOT rsEntrevistas.EOF%>
			<tr class="tblsfondo">
				<td style="text-align:center;"><%= rsEntrevistas("IDENTIFICACION")%></td>
				<td><%= rsEntrevistas("ENTREVISTADOR") %></td>
				<td><%= rsEntrevistas("ENTREVISTA") %></td>
				<td><%= rsEntrevistas("CITANRO") %></td>
				<td><%= rsEntrevistas("ESTADO") %></td>
				<td><%= rsEntrevistas("TAREA") %></td>
				<td><%= rsEntrevistas("FECINICIO") %></td>
				<td><%= rsEntrevistas("FECHAULTMOD") %></td>				
			</tr>

		<% rsEntrevistas.MoveNext
		contador=contador+1
		Loop
		%>

		</table>

	<!-- mostramos los botones de adelante y atras segun proceda -->
	<table border="0" width="100%" class="tbltext0 tblsfondo3" style="margin:auto;">
		<tr>
			<td width="10px" style="text-align:left;">
				<%if session("pagina") <> 1 then %>
					<form method="POST" action="reportEntrevistas.asp" id="form_back">
						<p>
							<a href="#" id="back">
								<img type="image" src="../../images/previous.png" width="24px" height="24px" style="border:0px;">
								<input type="hidden" alt="Atras" value="Atras" src="../../images/previous.png" id="imageBack" name="direccion" >
							</a>
							<!-- input type="image" id="back" alt="Atras" value="Atras" src="../../images/previous.png" id="imageBack" name="direccion" width="24px" height="24px" -->
							<input type="hidden" Class="textblack" id="centro_atencion_back" name="centro_atencion" value="<%=request("centro_atencion")%>" maxlength="50" />
							<input type="hidden" Class="textblack" id="fecini_back" name="fecini" value="<%=request("fecini")%>" maxlength="50" />
							<input type="hidden" Class="textblack" id="fecfin_back" name="fecfin" value="<%=request("fecfin")%>" maxlength="50" />
						</p>
					</form>
				<%end if%>
			</td>
			<td width="40%" style="text-align:left;">Registros 
				<span style="color:#FF0000;"><%=inicio%></span> al 
				<span style="color:#FF0000;"><%=fin%></span> de un total de
				<span style="color:#FF0000;"><%=rsEntrevistas.recordcount%></span>
			</td>
			<td width="40%" style="text-align:right;">P&aacute;gina 
				<span style="color:#FF0000;"><%=session("pagina")%></span> de 
				<span style="color:#FF0000;"><%=rsEntrevistas.pagecount%></span>
			</td>
			<td width="10%" style="text-align:right;">
				<% if session("pagina") < rsEntrevistas.pagecount then %>
					<form method="POST" name="reportEntrevistas.asp" id="form_next">
						<p>
							<a href="#" id="next">
								<img type="image" src="../../images/next.png" width="24px" height="24px" style="border:0px;">
								<input type="hidden" alt="Adelante" value="Adelante" src="../../images/next.png" id="imageNext" name="direccion" >
							</a>
							<!-- input type="image" alt="Adelante" value="Adelante" src="../../images/next.png" id="imageNext" name="direccion" width="24px" height="24px" -->
							<input type="hidden" Class="textblack" id="centro_atencion_next" name="centro_atencion" value="<%=request("centro_atencion")%>" maxlength="50" />							
							<input type="hidden" Class="textblack" id="fecini_next" name="fecini" value="<%=request("fecini")%>" maxlength="50" />							
							<input type="hidden" Class="textblack" id="fecfin_next" name="fecfin" value="<%=request("fecfin")%>" maxlength="50" />							
						</p>
					</form>
				<%end if%>
			</td>
		</tr>
	</table>

	<%
		'******************************************************* EXPORTAR A EXCEL *******************************************************
		Dim encabezadosExcel 
		encabezadosExcel = "IDENTIFICACION,ENTREVISTADOR,No. ENTREVISTA,SITA NUMERO,ESTADO,TAREA,FECHA INICIO ENTREVISTA,FECHA ULTIMA MODIFICACION"
		encabezadosExcelBase64 = Base64Encode(encabezadosExcel)

		Dim encabezadosQuery 
		encabezadosQuery = "IDENTIFICACION,ENTREVISTADOR,ENTREVISTA,CITANRO,ESTADO,TAREA,FECINICIO,FECHAULTMOD"

		encabezadosQueryBase64 = Base64Encode(encabezadosQuery)

		Dim reportFilterData 
		reportFilterData = "centro_atencion:"&request("centro_atencion")&",fecini:"&request("fecini")&",fecfin:"&request("fecfin")
		reportFilterDataBase64 = Base64Encode(reportFilterData)
		'********************************************************************************************************************************

	%>

	<input type="button" value="Exportar a Excel" onclick="javascript:exporttoExcelGeneric('reportEntrevistas','REPORTE ENTREVISTAS','<%=reportFilterDataBase64%>','<%=encabezadosExcelBase64%>','<%=encabezadosQueryBase64%>')">


<% else%>
	<h1 class="tbltext0 tblsfondo3" style="width:80%; border-radius:10px; font-size:20px;">No se encontraron coincidencias</h1>
<% end if %>

<!-- #include Virtual="/strutt_coda2.asp" -->
<!-- #include virtual="/include/CloseConn.asp" -->



