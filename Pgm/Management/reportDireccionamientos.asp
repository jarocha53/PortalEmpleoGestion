<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #include Virtual="/strutt_testa2.asp" -->
<!-- #include virtual="/include/OpenConn.asp" -->
<!-- #include virtual="/include/DecCod.asp" -->

<!--*************************** INCLUDE BASE 64 FUNCTION ****************** -->
<!-- #include virtual="/include/Base64.asp" -->
<!--*********************************************************************** -->

<!--*********************************************************************** -->
<!--Busca a la persona o grupo que le queremos enviar un mensaje ' RKL-->
<!--***********************************************************************-->
<%
	'@LANGUAGE="VBSCRIPT" CODEPAGE="65001" 
	'Session.Codepage=65001
	'Response.ContentType = "text/HTML" 
	'Response.CodePage = 65001
	'Response.CharSet = "utf-8"


	'****************************************************************************************************
	'Cargando data Grupo Ocupacional
	'sSQLOcupationalGroups = "SELECT ID_AREAPROF, DENOMINAZIONE FROM AREE_PROFESSIONALI ORDER BY DENOMINAZIONE ASC"
	'set rsOcupationalGroups = CC.Execute(sSQLOcupationalGroups)
	'****************************************************************************************************


	sSQLTotal = "select  count(1) totalPersonas " &_
				"  from stato_occupazionale so    " &_
				"  INNER join (SELECT ID_PERSONA, MAX(DT_DICH_STATUS) DT_DICH_STATUS FROM STATO_OCCUPAZIONALE WITH (NOLOCK) GROUP BY ID_PERSONA ) soUNI    " &_
				"  ON SO.ID_PERSONA = soUNI.ID_PERSONA    " &_
				"  AND SO.DT_DICH_STATUS = SOUNI.DT_DICH_STATUS    " &_
				"  right JOIN persona p with(nolock) " &_
				"   on  so.id_persona = p.id_persona " &_
				"   left join PERS_DERIVACION pw with(nolock) " &_
				"	on p.id_persona = pw.id_persona " &_
				"   inner join tades t with(nolock) " &_
				"	 on  t.codice = pw.cod_deriv " &_
				"   left join sede_impresa s " &_
				"	 on  so.id_cimpiego = s.id_sede " &_
				" where t.nome_tabella = 'DERIV' "
				if request("centro_atencion") <>"" then
					sSQLTotal = sSQLTotal & " and so.id_cimpiego = '" & request("centro_atencion") & "' "
				end if
				if request("fecini") <>"" and request("fecfin") <>"" then
					sSQLTotal = sSQLTotal & " and convert(datetime,convert(varchar(25),fecha_DERIV, 103), 103) >= convert(datetime,convert(varchar(25),'" & request("fecini") & " 00:00:00', 103), 103) and " &_
					            " convert(datetime,convert(varchar(25),fecha_DERIV, 103), 103) <= convert(datetime,convert(varchar(25),'" & request("fecfin") & " 00:00:00', 103), 103) "
				end if
				
	'response.WRITE sSQLTotal
	'RESPONSE.end
	set rsTotal = CC.Execute(sSQLTotal)

	'Consulta Global
	sSQLExist = "select  p.COGNOME, " &_
				"	P.NOME, " &_
				"	P.COD_FISC, " &_
				"	P.NUM_TEL, " &_
				"	P.E_MAIL, " &_
				"	s.descrizione CENTRO, " &_
				"	t.descrizione TIPO_DIRECCIONAMIENTO, " &_
				"	CASE WHEN pw.estado = '01' THEN 'Direccionamiento Concretado' " &_
				"		 WHEN pw.estado = '01' THEN 'Direccionamiento No Concretado' " &_
				"		 ELSE 'Volver en otra fecha' " &_
				"	END AS RES_DIRECCIONAMIENTO, " &_
				"	pw.fecha_DERIV " &_
				"  from stato_occupazionale so    " &_
				"  INNER join (SELECT ID_PERSONA, MAX(DT_DICH_STATUS) DT_DICH_STATUS FROM STATO_OCCUPAZIONALE WITH (NOLOCK) GROUP BY ID_PERSONA ) soUNI    " &_
				"  ON SO.ID_PERSONA = soUNI.ID_PERSONA    " &_
				"  AND SO.DT_DICH_STATUS = SOUNI.DT_DICH_STATUS    " &_
				"  right JOIN persona p with(nolock) " &_
				"   on  so.id_persona = p.id_persona " &_
				"   left join PERS_DERIVACION pw with(nolock) " &_
				"	on p.id_persona = pw.id_persona " &_
				"   inner join tades t with(nolock) " &_
				"	 on  t.codice = pw.cod_deriv " &_
				"   left join sede_impresa s " &_
				"	 on  so.id_cimpiego = s.id_sede " &_
				" where t.nome_tabella = 'DERIV' "
				if request("centro_atencion") <>"" then
					sSQLExist = sSQLExist & " and so.id_cimpiego = '" & request("centro_atencion") & "' "
				end if
				if request("fecini") <>"" and request("fecfin") <>"" then
					sSQLExist = sSQLExist & " and convert(datetime,convert(varchar(25),fecha_DERIV, 103), 103) >= convert(datetime,convert(varchar(25),'" & request("fecini") & " 00:00:00', 103), 103) and " &_
					            " convert(datetime,convert(varchar(25),fecha_DERIV, 103), 103) <= convert(datetime,convert(varchar(25),'" & request("fecfin") & " 00:00:00', 103), 103) "
				end if
				
	sSQLExist = sSQLExist & "ORDER BY p.COGNOME"

	'response.WRITE sSQLExist
	'RESPONSE.end


	'response.Write "*********************************************************"
	'response.Write "NOMBRE EMRESA -- " & request("nombre_empresa") & "</br>"
	'response.Write "SEDE EMPRESA -- " & request("sede_empresa") & "</br>"
	'response.Write "DEPARTAMENTO -- " & request("departamento") & "</br>"
	'response.Write "CIUDAD -- " & request("ciudad") & "</br>"
	'response.Write "FECHA INSCRIPCIÓN FROM -- " & request("fecha_inscripcion_from") & "</br>"
	'response.Write "FECHA INSCRIPCIÓN TO -- " & request("fecha_inscripcion_to") & "</br>"
	'response.Write "*********************************************************"
		
	
	'rsRegisteredDireccionamientos.CursorType = adOpenstatic 
	set rsRegisteredDireccionamientos = CC.Execute(sSQLExist)

	if request("search") = "search" then
		session("pagina") = 1
	end if

	rsRegisteredDireccionamientos.pagesize=10

	if rsRegisteredDireccionamientos.pagecount <> 0 then
		if session("pagina")>rsRegisteredDireccionamientos.pagecount then
				session("pagina")=rsRegisteredDireccionamientos.pagecount 'evitamos el error de reload
		end if

		' controlando avance de pagina
		
		dim direccion 
		'if request("direccion") = "" then		
	''		direccion = "Atras"
	''	else
			direccion = request("direccion")
	''	end if

		
		if session("pagina") = "" then
			session("pagina") = 1
		else

			if direccion = "Adelante" and session("pagina")<rsRegisteredDireccionamientos.pagecount then
				session("pagina") = session("pagina") + 1
			else 
				if direccion = "Atras" and session("pagina")>1 then
					session("pagina") = session("pagina") - 1
				end if	 
			end if	 
		end if

		if session("pagina")<>"" then
			rsRegisteredDireccionamientos.absolutepage=session("pagina")
		else 
			rsRegisteredDireccionamientos.absolutepage=1
		end if

		inicio=1+(session("pagina")-1)*rsRegisteredDireccionamientos.pagesize
		fin=inicio+rsRegisteredDireccionamientos.pagesize - 1
		if fin > rsRegisteredDireccionamientos.recordcount then
			fin =rsRegisteredDireccionamientos.recordcount
		end if

	end if 

	
	function roundUp(x)
	   If x > Int(x) then
	    roundup = Int(x) + 1
	   Else
	    roundup = x
	   End If
	End Function

%>

<script language="Javascript"> 
$(function(){

	$("#form_next").submit(function(){
		$("#centro_atencion_next").val($("#centro_atencion").val());
		$("#fecini_next").val($("#fecini").val());
		$("#fecfin_next").val($("#fecfin").val());
		return true;
	});


	$("#next").click(function(){
		$("#form_next").submit();		
	});

	$("#form_back").submit(function(){
		$("#centro_atencion_back").val($("#centro_atencion").val());
		$("#fecini_back").val($("#fecini").val());
		$("#fecfin_back").val($("#fecfin").val());
		return true;
	});
	

	$("#back").click(function(){
		$("#form_back").submit();
	});	


});
</script>




<form name="cerca" id="form_search" action="reportDireccionamientos.asp" Method="Post">
	</br>
	<table border="0" width="700" CELLPADDING="0" cellspacing="0">
		<tr>
			<td align="left" class="sfondomenu" width="300">
				<span class="tbltext0">
				<b>&nbsp;REPORTE DE DIRECCIONAMIENTOS</b></span>
			</td>
			<td width="25" valign="bottom" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">
				<img border="0" src="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
			<td width="40%" valign="middle" align="right" class="tbltext1" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
		</tr>
	</table>
		
	<table border="0" width="700" CELLPADDING="0" cellspacing="0">
	    <tr>
			<td class="sfondocomm" align="left" class="tbltext1">
			En esta secci&oacute;n se puede efectuar una b&uacute;squeda de los direccionamientos realizados a los oferentes.</br></br>
			<!-- Puede filtrar la b&uacute;squeda ingresando el apellido y nombre del usuario. -->
			</td>
			<td class="sfondocomm">
				<a href="Javascript:Show_Help('/Pgm/Help/ProjectSite/Rubrica/default/')">
				<img src="<%=Session("Progetto")%>/images/Help.gif" border="0" align="right" alt="Help">
			</td> 
	    </tr>

		<tr height="2">
			<td class="sfondocomm" width="100%" colspan="2" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>
	<table width="700" border="0" class="sfondocomm">
		<tr class="sfondocomm">
			<td colspan="3"><b>B&Uacute;SQUEDA DE DIRECCIONAMIENTOS</b></td>		
		</tr>
		<tr style="text-align:center;">
			<td class="tbltext1" width="30%"><b>Centro de atenci&oacute;n</b></td>			
			<td class="tbltext1" width="30%"><b>Fecha inicial</b></td>
			<td class="tbltext1" width="30%"><b>Fecha final</b></td>
		</tr>
		<tr style="text-align:center;">
			<td class="tbltext1">				
				<select name="centro_atencion" id="centro_atencion" Class="textblack">
				<% contador=0 %>
				<% sSQLAttentionCenter = "select SI.ID_IMPRESA, SI.ID_SEDE, I.RAG_SOC, SI.DESCRIZIONE AS centro " &_ 
						"from SEDE_IMPRESA SI " &_ 
						"inner join IMPRESA I ON I.ID_IMPRESA = SI.ID_IMPRESA " &_ 
						"where COD_TIMPR = '03' " %>
				<%	set rsAttentionCenter = CC.Execute(sSQLAttentionCenter) %>
				<option value=""></option>
				<%
					Do While contador < rsAttentionCenter.pagesize and NOT rsAttentionCenter.EOF%>
					  <% if request("centro_atencion")<>"" and trim(request("centro_atencion"))=cstr(rsAttentionCenter("id_sede")) then %>
						<option value=<%=rsAttentionCenter("id_sede")%> selected><%= rsAttentionCenter("centro")%></option>
					  <% else %>
					  	<option value=<%=rsAttentionCenter("id_sede")%> ><%= rsAttentionCenter("centro")%></option>
					<% end if %>
					<%
					rsAttentionCenter.MoveNext
					contador=contador+1
					Loop
				%>
				</select>
			</td>
			<td class="tbltext1">
				<input type="text" Class="textblack fecha" id="fecini" name="fecini" value="<%=request("fecini")%>" maxlength="50" style="" />
			</td>
			<td class="tbltext1">
				<input type="text" Class="textblack fecha" id="fecfin" name="fecfin" value="<%=request("fecfin")%>" maxlength="50" style="" />
			</td>
		</tr>
		<tr>
			<td></td>
			<td>
				<DIV ALIGN="RIGHT"><input type="image" alt="Cerca" src="<%=Session("Progetto")%>/images/lente.gif" id="search" name="search" value="search"></DIV>				
			</td>
			<td>
				<div style="margin:auto; border-radius:10px; padding-top:8px; padding-bottom:8px; width:80%; text-align:center;" class="tbltext0 tblsfondo3"><b>TOTAL DIRECCIONADOS</b> </br> <% response.Write rsTotal("totalPersonas") %></div>
			</td>
		</tr>
	</table>	
</form>
</br>


<% if not rsRegisteredDireccionamientos.EOF then %>  
		</br></br>
		<table border="0" width="700">
			<tr class="tbltext0 tblsfondo3" style="text-align:center;">
				<td width="10%" style="border-radius:10px 0 0 0;"><b>Documento</b></td>
				<td width="30%"><b>Nombre</b></td>
				<td width="30%"><b>Tipo de direccionamiento</b></td>
				<td width="30%" style="border-radius:0 10px 0 0;"><b>Respuesta direccionamiento</b></td>
			</tr>
		<!-- montamos el bucle para mostrar los registros -->
		<% contador=0
		Do While contador < rsRegisteredDireccionamientos.pagesize and NOT rsRegisteredDireccionamientos.EOF%>
			<tr class="tblsfondo">
				<td style="text-align:center;"><%= rsRegisteredDireccionamientos("cod_fisc")%></td>
				<td><%= rsRegisteredDireccionamientos("cognome") & " " & rsRegisteredDireccionamientos("nome") %></td>
				<td><%= rsRegisteredDireccionamientos("tipo_direccionamiento") %></td>
				<td><%= rsRegisteredDireccionamientos("res_direccionamiento") %></td>				
			</tr>

		<% rsRegisteredDireccionamientos.MoveNext
		contador=contador+1
		Loop
		%>

		</table>

	<!-- mostramos los botones de adelante y atras segun proceda -->
	<table border="0" width="100%" class="tbltext0 tblsfondo3" style="margin:auto;">
		<tr>
			<td width="10px" style="text-align:left;">
				<%if session("pagina") <> 1 then %>
					<form method="POST" action="reportDireccionamientos.asp" id="form_back">
						<p>
							<a href="#" id="back">
								<img type="image" src="../../images/previous.png" width="24px" height="24px" style="border:0px;">
								<input type="hidden" alt="Atras" value="Atras" src="../../images/previous.png" id="imageBack" name="direccion" >
							</a>
							<!-- input type="image" id="back" alt="Atras" value="Atras" src="../../images/previous.png" id="imageBack" name="direccion" width="24px" height="24px" -->
							<input type="hidden" Class="textblack" id="centro_atencion_back" name="centro_atencion" value="<%=request("centro_atencion")%>" maxlength="50" />
							<input type="hidden" Class="textblack" id="fecini_back" name="fecini" value="<%=request("fecini")%>" maxlength="50" />
							<input type="hidden" Class="textblack" id="fecfin_back" name="fecfin" value="<%=request("fecfin")%>" maxlength="50" />
						</p>
					</form>
				<%end if%>
			</td>
			<td width="40%" style="text-align:left;">Registros 
				<span style="color:#FF0000;"><%=inicio%></span> al 
				<span style="color:#FF0000;"><%=fin%></span> de un total de
				<span style="color:#FF0000;"><%=rsRegisteredDireccionamientos.recordcount%></span>
			</td>
			<td width="40%" style="text-align:right;">P&aacute;gina 
				<span style="color:#FF0000;"><%=session("pagina")%></span> de 
				<span style="color:#FF0000;"><%=rsRegisteredDireccionamientos.pagecount%></span>
			</td>
			<td width="10%" style="text-align:right;">
				<% if session("pagina") < rsRegisteredDireccionamientos.pagecount then %>
					<form method="POST" name="reportDireccionamientos.asp" id="form_next">
						<p>
							<a href="#" id="next">
								<img type="image" src="../../images/next.png" width="24px" height="24px" style="border:0px;">
								<input type="hidden" alt="Adelante" value="Adelante" src="../../images/next.png" id="imageNext" name="direccion" >
							</a>
							<!-- input type="image" alt="Adelante" value="Adelante" src="../../images/next.png" id="imageNext" name="direccion" width="24px" height="24px" -->							
							<input type="hidden" Class="textblack" id="centro_atencion_next" name="centro_atencion" value="<%=request("centro_atencion")%>" maxlength="50" />							
							<input type="hidden" Class="textblack" id="fecini_next" name="fecini" value="<%=request("fecini")%>" maxlength="50" />							
							<input type="hidden" Class="textblack" id="fecfin_next" name="fecfin" value="<%=request("fecfin")%>" maxlength="50" />							
						</p>
					</form>
				<%end if%>
			</td>
		</tr>
	</table>

	<%
		'******************************************************* EXPORTAR A EXCEL *******************************************************
		Dim encabezadosExcel 
		encabezadosExcel = "APELLIDO,NOMBRE,IDENTIFICACION,TELÉFONO,E MAIL,CENTRO DE ATENCIÓN,TIPO DIRECCIONAMIENTO,RESPUESTA DIRECCIONAMIENTO,FECHA DIRECCIONAMIENTO"
		encabezadosExcelBase64 = Base64Encode(encabezadosExcel)

		Dim encabezadosQuery 
		encabezadosQuery = "COGNOME,NOME,COD_FISC,NUM_TEL,E_MAIL,CENTRO,TIPO_DIRECCIONAMIENTO,RES_DIRECCIONAMIENTO,FECHA_DERIV"

		encabezadosQueryBase64 = Base64Encode(encabezadosQuery)

		Dim reportFilterData 
		reportFilterData = "centro_atencion:"&request("centro_atencion")&",fecini:"&request("fecini")&",fecfin:"&request("fecfin")
		reportFilterDataBase64 = Base64Encode(reportFilterData)
		'********************************************************************************************************************************

	%>

	<input type="button" value="Exportar a Excel" onclick="javascript:exporttoExcelGeneric('reportDireccionamientos','REPORTE DE DIRECCIONAMIENTOS','<%=reportFilterDataBase64%>','<%=encabezadosExcelBase64%>','<%=encabezadosQueryBase64%>')">


<% else%>
	<h1 class="tbltext0 tblsfondo3" style="width:80%; border-radius:10px; font-size:20px;">No se encontraron coincidencias</h1>
<% end if %>

<!-- #include Virtual="/strutt_coda2.asp" -->
<!-- #include virtual="/include/CloseConn.asp" -->



