<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->

<!-- #include Virtual="/strutt_testa2.asp" -->
<!-- #INCLUDE FILE="../Utils.asp" -->
<center>
<%

AREA = Request("ID_AREA")

Set Conn = server.CreateObject ("ADODB.Connection")
Conn.open strConn

Sql = ""
Sql = Sql &  " SELECT "
Sql = Sql &  "   FIG.ID_FIGPROF, FIG.DENOMINAZIONE FigProf, "
Sql = Sql &  "   ARE.DENOMINAZIONE Area, VAL.FL_VALID,"
Sql = Sql &  " ((SELECT GRADO_FP_PERS FROM PERS_FIGPROF "
Sql = Sql &  "  WHERE "
Sql = Sql &  "  ID_FIGPROF=FIG.ID_FIGPROF AND "
Sql = Sql &  "  ID_PERSONA=" & IDP & " AND "
Sql = Sql &  "  PERS_FIGPROF.IND_STATUS = 0 AND "
Sql = Sql &  "  DT_TMST>=(SELECT MAX(PERS_FIGPROF.DT_TMST) FROM PERS_FIGPROF WHERE ID_PERSONA= " & IDP & "))) Grado, "
Sql = Sql &  "  (SELECT Max(ID_PROG_PROF) FROM PERS_PROG_PROF WHERE ID_PERSONA = " & IDP & " AND "
Sql = Sql &  "		ID_FIGPROF = FIG.ID_FIGPROF) SiNo "
Sql = Sql &  " FROM "
Sql = Sql &  "   AREE_PROFESSIONALI ARE, "
Sql = Sql &  "   FIGUREPROFESSIONALI FIG, "
Sql = Sql &  "   VALIDAZIONE VAL"
Sql = Sql &  " WHERE "
Sql = Sql &  "   ( FIG.ID_AREAPROF=ARE.ID_AREAPROF ) AND "
Sql = Sql &  "   ( VAL.ID_VALID=FIG.ID_VALID ) AND"
Sql = Sql &  "   ( VAL.FL_VALID = 1 ) AND "
Sql = Sql &  "   ARE.ID_AREAPROF = " & AREA
Sql = Sql &  " ORDER BY upper(Grado) DESC, UPPER(FigProf)"


if Sql<>"" then


'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
	Set Rs = Conn.Execute (Sql)
	if not rs.eof then
		VV = Rs.getrows()
	else
		%>
		<script language='Javascript'>alert("ATENCION:\no se encontraron datos.");
		goToPage('visbilancio.asp')</script>
		<%
		Response.Flush
		Response.End
	End if
	Set Rs = Nothing
	Set Conn = Nothing

	Response.Write "<form name='Salva' action='SalvaDettagliArea.asp' method=POST>"
	Response.Write "<input type=hidden name='back' value='" & request("back") & "'>"


	If IsArray(VV) then
		Response.Write "<input type='hidden' value='" & ubound(VV,2) & "' name='Limite'>"

Response.Write "<center>"
Response.Write "<table border=0 width='500' CELLPADDING=0 cellspacing=0>"
		Response.Write "<tr class='tblcomm'>"
	Response.Write "<td  align=left  class=sfondomenu>"
	Response.Write "<span class='tbltext0'><b>BALANCE DE COMPETENCIAS</b></span>"
	Response.Write "</td>"
	Response.Write "<td width='25' valign='bottom' background='" & Session("Progetto")& "/images/imgConsiel/sfondo_linguetta.gif' >"
	Response.Write "<img border='0' src='" & Session("Progetto") & "/images/imgConsiel/tondo_linguetta.gif'></td>"
	Response.Write "<td width='278' valign='bottom' background='" & Session("Progetto") & "/images/imgConsiel/sfondo_linguetta.gif'>&nbsp;</td>"
		Response.Write "</tr>"
Response.Write "</table>"

Response.Write "<table border=0 width='500' CELLPADDING=0 cellspacing=0>"
		Response.Write "<tr>"
	Response.Write "<td class=sfondocomm  align='left' colspan='2' class='tbltext1'>"
	Response.Write  ucase( VV(2,0)) 
	Response.Write "<td class=sfondocomm><a href=""Javascript:Show_Help('" & Session("Progetto") & "/HelpBdc/Bilancio_Competenze/ProfiliArea.htm')"">"
	Response.Write " <img src='" & Session("Progetto")& "/images/Help.gif' border='0'  align=right alt='Help'>"
	Response.Write "</a>"
	Response.Write "</td>"
		Response.Write " </tr>"
		Response.Write "<tr>"
	Response.Write "<td colspan=3 bgcolor='#3399CC'></td>"
	Response.Write "</tr>"
		Response.Write "<tr>"
	Response.Write "<td colspan=3 height='15'></td>"
	Response.Write "</tr>"
Response.Write "</table>"


		Response.Write "<table cellspacing=0 cellpadding=0 width='500' border=0>"
		Response.write "<tr class=sfondocomm>"
		Response.Write "<td class=tbltext1 valign=top width='500' colspan='4'>"
		Response.write "<b>Listado de Figuras Profesionales del Area : " & ucase( VV(2,0)) & "</small></b></font>"
		Response.Write "</td>"
		Response.Write "</TR>"
		Response.Write "<TR>"
		Response.Write "</TR>"
		Response.Write "<TR class=sfondocomm>"
		Response.Write "<TD class=tbltext1><b>Denominación</b></TD>"
		Response.Write "<TD class=tbltext1><b>Proyecto</b></TD>"
		Response.Write "<TD class=tbltext1><b>Grado</b></font></TD>"
		Response.Write "<td align=center class=tbltext1><b>Opción</b></td>"
		Response.Write "</tr>"

		colore="#FFFFFF"
		for x = lbound(VV,2) to Ubound(VV,2)
			Response.Write chr(13) & chr(10)
			Response.Write "<tr >"
				Response.Write "<td class=tbltext1>" & VV(1,x) & "</td>"
				Response.Write "<td class=tbltext1 align='center'>"
				if clng(0 & VV(5,x)) > 0 then Response.Write "<font  color='#008000' size=1><b>SI</font></b>"
				Response.Write "&nbsp;</td>"
				Response.Write "<TD>"
				Perc = int(0 & trim(VV(4,x)))
				Slide= round(Perc/20)
				if Slide<>0 then
					
					Response.Write "<img src='" & Slide & ".gif' border=0>"
					Response.Write "</td>"
				else
					Response.Write "&nbsp;</TD>"
				end if
				Response.Write "<td align='center'>&nbsp;&nbsp;"
				Response.Write "<input Onclick='goToPage(""ReportFPOcc.asp?ID_FIGPROF=" & VV(0,x) & """)' type='Button' Value='Balance' CLASS='My'>"
				Response.write "<input type=hidden name='Id" & x & "' value='" & VV(0,x) & "'>&nbsp;"
				Response.Write "</td>"
			Response.Write "</tr>"
		next
		Response.write "<TR>"
		Response.Write "</TR>"
		Response.Write "</table>"
	end if

	Response.write "<br>"
Response.Write "<table cellspacing=0 cellpadding=0 width='500' border=0>"

	Response.write "<TR>"
	Response.Write	"<TD  align='Left'>"
	Response.Write "<input type='button' class='My' value='Volver' OnClick=""goToPage('VisBilancio.asp?Tipo=" & Tipo & "&ID_AREA=" & AREA & "')"">"
	Response.Write Chr(13) & chr(10)
	Response.Write "<input type='hidden' name='DETTAGLI' Value='" & Request.ServerVariables ("URL") & "'>"

	Response.Write "<input type='hidden' name='ID_AREA' Value='" & AREA & "'>"
	Response.Write "<input type='hidden' name='Tipo' value='" & Tipo & "'>"
	Response.write "</td>"
	Response.write "</tr>"
	Response.write "<tr>"
	Response.write "<td  bgcolor='#3399CC'></td>"
		Response.write "</tr>"
	Response.Write "</table>"

end if

Response.Write "</form>"
%>
<!-- #include Virtual="/strutt_coda2.asp" -->
