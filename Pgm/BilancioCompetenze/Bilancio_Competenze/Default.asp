<!-- #include Virtual="/strutt_testa2.asp" -->
<!-- #INCLUDE Virtual="/pgm/bilanciocompetenze/Utils.asp" -->

<table border=0 cellspacing=0 cellpadding=0 width='500' height="224" style="WIDTH: 500px; HEIGHT: 224px">
	<tr bgcolor='#f1f3f3'>
	<td valign='center' class='tbltext1' align=left bgcolor='#f1f3f3'>
      <P align=center> <strong>BALANCE DE COMPETENCIAS</strong></P>
      <P>Utilizar la funcion para confrontar la carpeta de competencias con una 
        de las figuras profesionales disponibles</P>
      <P>Se puede buscar con:</P>
      <UL>
        <LI>un simple motor de busqueda; 
        <LI>utilizando los &aacute;reas profesionales&nbsp;resultantes del balance 
          de proximidad; 
        <LI>utilizando la clasificaci&oacute;n de las incumbencias de ISTAT.</LI>
      </UL>
      <P>En cualquier momento, de ser necesario puede pasar al analisis de las 
        competencias por:</P>
      <UL>
        <LI>ingresar nuevos elementos al portafolio; 
        <LI>eliminar los elementos ingresados erroneamente; 
        <LI>eliminar los elementos que no posee; 
        <LI> modificar el nivel de posesi&oacute;n de los elementos que ha declarado.</LI>
      </UL>
      <P>Despu&eacute;s de cada modificaci&oacute;n recordar qiue se debe regenerar 
        el balance de proximidad para poner al d&iacute;a las informaciones sobre 
        los perfiles y las &aacute;reas que pueden ser objeto de candidaturas<a href="Javascript:Show_Help('/Pgm/Help/BilancioCompetenze/Bilancio_Competenze/default')"><img src="<%=Session("Progetto")%>/images/Help.gif" border='0'  align=right alt='Help'> 
        </a> </td>
     </tr>

	<tr>
		<td>&nbsp;
		</td>
	</tr>
     
<tr>
	<td align='center'>
	<a onmouseover="javascript: window.status=' '; return true" href="javascript:goToPage('VisBilancio.asp')" >
	<img src="<%=Session("Progetto")%>/images/avanti.gif" border="0" >
	<!--<input value="Avanti" type='button' OnClick="goToPage('VisBilancio.asp')" class='My'>	-->
	</a> 
	</td>
</tr>
</table>     
<!-- #include Virtual="/strutt_coda2.asp" -->
