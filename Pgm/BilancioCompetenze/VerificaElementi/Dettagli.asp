<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #Include Virtual="/strutt_testa2.asp" -->
<!-- #Include file="../Utils.asp" -->

<center>
<%

if request("Tipo")="" then 
	if Request("TipoQry")= "CONOSC" then 
		Tipo="CONOSCENZE"
		INTEST = "CONOSCENZE"
		vl = "CONOCIMIENTOS"
		AREA_CONOSCENZA=request("ID_AREA")
	END IF 
	
	if Request("TipoQry")= "CAPAC" then 
		Tipo="CAPACITA"
		INTEST = "CAPACITA'"
		vl = "CAPACIDADES"
		AREA_CAPACITA=request("ID_AREA")
	END IF
	
	if Request("TipoQry")= "COMPOR" then 
		Tipo="COMPORTAMENTI"
		INTEST="COMPORTAMENTI"
		vl = "COMPORTAMIENTOS"
		AREA_COMPORTAMENTI=request("ID_AREA")
	END IF
else
	Tipo= request("Tipo")
	AREA_COMPORTAMENTI= Request("AREA_COMPORTAMENTI")
	AREA_CONOSCENZA = Request("AREA_CONOSCENZA")
	AREA_CAPACITA = Request("AREA_CAPACITA")
end if 



Set Conn = server.CreateObject ("ADODB.Connection")
Conn.open strConn

Sql=""

	If Tipo="CONOSCENZE" then
		Sql = ""
		Sql = Sql & "SELECT PERS_CONOSC.ID_CONOSCENZA, PERS_CONOSC.COD_GRADO_CONOSC "
		Sql = Sql & "FROM PERS_CONOSC "
		Sql = Sql & "WHERE PERS_CONOSC.ID_PERSONA = " & IDP
		
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
		Set Rs = Conn.Execute (Sql)
		If not rs.eof then MM = Rs.getrows()
		Set Rs = Nothing
		
		Tipo = "CONOSCENZE"
		INTEST = "CONOSCENZE"
			
		Sql = ""
		Sql = Sql & "SELECT CONOSCENZE.ID_CONOSCENZA, CONOSCENZE.DENOMINAZIONE,AREA_CONOSCENZA.DENOMINAZIONE "
		Sql = Sql & "FROM "
		Sql = Sql & "CONOSCENZE, AREA_CONOSCENZA, VALIDAZIONE "
		Sql = Sql & "WHERE "
		Sql = Sql & "((CONOSCENZE.ID_AREACONOSCENZA = AREA_CONOSCENZA.ID_AREACONOSCENZA) AND "
		Sql = Sql & "(CONOSCENZE.ID_VALID = VALIDAZIONE.ID_VALID)) AND "
		Sql = Sql & "((VALIDAZIONE.FL_VALID = 1) OR (VALIDAZIONE.ID_PERS_LAV = " & IDP & ")) "
		Sql = Sql & " AND (CONOSCENZE.ID_AREACONOSCENZA = " & AREA_CONOSCENZA & ")"
		Sql = Sql & " ORDER BY CONOSCENZE.DENOMINAZIONE "
	
	
	End if

	If Tipo="CAPACITA" then
		Sql = ""
		Sql = Sql & "SELECT PERS_CAPAC.ID_CAPACITA, PERS_CAPAC.GRADO_CAPACITA "
		Sql = Sql & "FROM PERS_CAPAC "
		Sql = Sql & "WHERE PERS_CAPAC.ID_PERSONA = " & IDP
		
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
		Set Rs = Conn.Execute (Sql)
		if not rs.eof then MM = Rs.getrows()
		Set Rs = Nothing
		Tipo = "CAPACITA"
		INTEST = "CAPACITA'"
		
		Sql = ""
		Sql = Sql & "SELECT CAPACITA.ID_CAPACITA, CAPACITA.DENOMINAZIONE,AREA_CAPACITA.DENOMINAZIONE "
		Sql = Sql & "FROM "
		Sql = Sql & "CAPACITA, AREA_CAPACITA, VALIDAZIONE "
		Sql = Sql & "WHERE "
		Sql = Sql & "((CAPACITA.ID_AREACAPACITA = AREA_CAPACITA.ID_AREACAPACITA) AND "
		Sql = Sql & "(CAPACITA.ID_VALID = VALIDAZIONE.ID_VALID)) AND "
		Sql = Sql & "((VALIDAZIONE.FL_VALID = 1) OR (VALIDAZIONE.ID_PERS_LAV = " & IDP & "))  "
		Sql = Sql & " AND (CAPACITA.ID_AREACAPACITA = " & AREA_CAPACITA & ")"
		Sql = Sql & " ORDER BY CAPACITA.DENOMINAZIONE "
	End if

If Tipo="COMPORTAMENTI" Then
		Sql = ""
		Sql = Sql & "SELECT PERS_COMPOR.ID_COMPORTAMENTO, PERS_COMPOR.GRADO_PERS_COMPOR "
		Sql = Sql & "FROM PERS_COMPOR "
		Sql = Sql & "WHERE PERS_COMPOR.ID_PERSONA = " & IDP
		
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
		Set Rs = Conn.Execute (Sql)
		if not rs.eof then MM = Rs.getrows()
		Set Rs = Nothing
		

		Tipo = "COMPORTAMENTI"
		INTEST = "COMPORTAMENTI"
	
		Sql = ""
		Sql = Sql & "SELECT COMPORTAMENTI.ID_COMPORTAMENTO, COMPORTAMENTI.DENOMINAZIONE,AREA_COMPORTAMENTI.DENOMINAZIONE "
		Sql = Sql & "FROM "
		Sql = Sql & "COMPORTAMENTI, AREA_COMPORTAMENTI, VALIDAZIONE "
		Sql = Sql & "WHERE "
		Sql = Sql & "((COMPORTAMENTI.ID_AREACOMPORTAMENTO = AREA_COMPORTAMENTI.ID_AREACOMPORTAMENTO) AND "
		Sql = Sql & "(COMPORTAMENTI.ID_VALID = VALIDAZIONE.ID_VALID)) AND "
		Sql = Sql & "((VALIDAZIONE.FL_VALID = 1) OR (VALIDAZIONE.ID_PERS_LAV = " & IDP & "))"
		Sql = Sql & " and (COMPORTAMENTI.ID_AREACOMPORTAMENTO = " & AREA_COMPORTAMENTI & ")"
		Sql = Sql & " ORDER BY COMPORTAMENTI.DENOMINAZIONE "
End if

if Sql<>"" then


'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
	Set Rs = Conn.Execute (Sql)

	
	if not rs.eof then
		VV = Rs.getrows()
	else
		%>
		<script language='Javascript'>alert("ATENCIÓN:\nNo hay datos para los parámetros ingresados.");window.history.back()</script>
		<%
		Response.Flush
		Response.End
	End if
	Set Rs = Nothing
	Set Conn = Nothing

	Response.Write "<form name='Salva' action='SalvaDettagli.asp' method=POST>"
	Response.Write "<input type=hidden value='" & request("back") & "' name='back'>"
	Response.Write "<input type='hidden' value='"& request("IdPers") & "' name='IdPers'>"
	Response.Write "<input type='hidden' value='" & request("IND_FASE") & "' name='IND_FASE'>"
	Response.Write "<input type='hidden' value='" & Request("LivMenu") &"' name='LivMenu'>"
	Response.Write "<input type='hidden' value='NO' name='VISMENU'>"
Response.Write "<table border=0 width='500' CELLPADDING=0 cellspacing=0>"
		Response.Write "<tr class='tblcomm'>"
	Response.Write "<td  align=left  class=sfondomenu>"
	Response.Write "<span class='tbltext0'><b>" & vl & "</b></span>"
	Response.Write "</td>"
	Response.Write "<td width='25' valign='bottom' background='" & Session("Progetto")& "/images/imgConsiel/sfondo_linguetta.gif' >"
	Response.Write "<img border='0' src='" & Session("Progetto") & "/images/imgConsiel/tondo_linguetta.gif'></td>"
	Response.Write "<td width='278' valign='bottom' background='" & Session("Progetto") & "/images/imgConsiel/sfondo_linguetta.gif'>&nbsp;</td>"
		Response.Write "</tr>"
Response.Write "</table>"

Response.Write "<table border=0 width='500' CELLPADDING=0 cellspacing=0>"
		Response.Write "<tr>"
	Response.Write "<td class=sfondocomm  align='left' colspan='2' class='tbltext1'>"
	Response.Write  "Area:&nbsp;<b>" & VV(2,0) & "</b>"
	Response.Write "<td class=sfondocomm>"
	
	select case tipo
		case "CAPACITA"
		Response.Write "<a href=""Javascript:Show_Help('" & Session("Progetto") & "/HelpBdc/VerificaElementi/Dettagli1.htm')"">"
		Response.Write " <img src='" & Session("Progetto")& "/images/Help.gif' border='0'  align=right alt='Help'>"
		Response.Write "</a>"
			
		case "COMPORTAMENTI"
		Response.Write "<a href=""Javascript:Show_Help('" & Session("Progetto") & "/HelpBdc/VerificaElementi/Dettagli2.htm')"">"
		Response.Write " <img src='" & Session("Progetto")& "/images/Help.gif' border='0'  align=right alt='Help'>"
		Response.Write "</a>"
		
		case "CONOSCENZE"
		Response.Write "<a href=""Javascript:Show_Help('" & Session("Progetto") & "/HelpBdc/VerificaElementi/Dettagli.htm')"">"
		Response.Write " <img src='" & Session("Progetto")& "/images/Help.gif' border='0'  align=right alt='Help'>"
		Response.Write "</a>"

	end select

	Response.Write "</td>"
		Response.Write " </tr>"
		Response.Write "<tr>"
	Response.Write "<td colspan=3 bgcolor='#3399CC'></td>"
	Response.Write "</tr>"
Response.Write "</table>"
	
	If IsArray(VV) then
		Response.Write "<input type='hidden' value='" & ubound(VV,2) & "' name='Limite'>"
	Response.Write "<table cellspacing=0 cellpadding=0 width='500' border=0 style='font-family: Verdana; font-size: 8pt; color:#ffffff'>"
	Response.Write "<TR>"
		Response.Write "<TD class='sfondocomm' colspan='2'><b>Denominación<small></b></TD>"
		Response.Write "<td align=center class='sfondocomm'><b>0 - 1 - 2 - 3 - 4</b></td>"
	Response.Write "</tr>"
		
		if request("Limit") <> "" then
			Ini = request("Limit")
			Fin = Ini + 10
			if Ini<0 then 
				Ini=0
			end if		
		else
			Ini = 0
			Fin = 9
		end if
		
		If Fin > ubound(VV,2) then Fin = ubound(VV,2)
		If Ini < lbound(VV,2) then Ini = lbound(VV,2)

		for x = Ini to Fin
			if (x mod 2)= 0 then 
					sfondo="class='tblsfondo'" 
				else
					sfondo="class='tbltext1'"
				end if
				Response.Write "<tr " & sfondo & ">"

		Response.Write chr(13) & chr(10)
		
		
				Response.Write "<td colspan='2' width='80%' class='tbltext1'>&nbsp;" & Ucase(VV(1,x)) & "</td>"
				Response.Write "<td  align='center' class='tbltext1'>"
				Response.write "<input type=hidden name='Id" & x & "' value='" & VV(0,x) & "'>"
				Response.write "<input type=hidden name='exist" & x & "' value='" & (Mettival(MM,VV(0,x),0,0) <> "") & "'>"

					Response.Write "<input type='radio' value='' name='Grado" & x & "'"
						if Int("0" & Mettival(MM,VV(0,x),0,1)) = 0 then Response.Write " CHECKED "
					Response.Write " >"
					Response.Write "<input type='radio' value=1 name='Grado" & x & "'"
						if Int("0" & Mettival(MM,VV(0,x),0,1)) = 1 then Response.Write " CHECKED "
					Response.Write " >"
					Response.Write "<input type='radio' value=2 name='Grado" & x & "'"
						if Int("0" & Mettival(MM,VV(0,x),0,1)) = 2 then Response.Write " CHECKED "
					Response.Write " >"
					Response.Write "<input type='radio' value=3 name='Grado" & x & "'"
						if Int("0" & Mettival(MM,VV(0,x),0,1)) = 3 then Response.Write " CHECKED "
					Response.Write " >"
					Response.Write "<input type='radio' value=4 name='Grado" & x & "'"
						if Int("0" & Mettival(MM,VV(0,x),0,1)) = 4 then Response.Write " CHECKED "
					Response.Write " >"
			Response.Write "</td>"
			Response.Write "</tr>"
		next	
		Response.write "<TR>"
		Response.write "<TD colspan='3'>&nbsp"
		Response.write "</TD>"
		Response.Write "</TR>"

	end if
	
	Response.write "<br>"
		
	Response.write "<TR>"
	Response.Write	"<TD colspan=3 align='center' class='tbltext1'>"

	Response.Write "<input type='hidden' name='Ini' Value='" & Ini & "'>"
	Response.Write "<input type='hidden' name='Limit' Value='" & Fin & "'>"
	Response.Write "<input type='button' class='My' value='Cancelar' OnClick='goToPage(""DettagliCompet_1.asp?Tipo=" & Tipo & """)'>"
	
	
	'NrPag=int(ubound(VV,2)/9)
	NrPag=int((ubound(VV,2)-10)/9)
	if NrPag=0 then NrPag=1
	'PagCor=int((ini+9)/9)
	
	pippo=ubound(VV,2) - (NrPag * 10)
	
	if pippo>0 then 
		NrPag=NrPag+1
	end if
	'PagCor=int((Fin-1)/9)
	PagCor=int((Ini+1)/10)+1
	'if PagCor=0 then 
	'	PagCor=1
	'end if
	'if Fin=ubound(VV,2) then
	'	PagCor=NrPag
	'end if
	
	'Response.Write ("Da " & Ini & " a " & Fin & " <br>")

	'Response.Write "<br>"
	'Response.Write ("PagCor " & PagCor & "<br>")
	'Response.Write ("NrPag " & NrPag & "<br>")
	'Response.Write ("Array " & ubound(VV,2) & "<br>")
	'Response.Write ("Array/9 " & ubound(VV,2)/9 & "<br>")

	if PagCor<>1 then   Response.write "&nbsp;<input type='submit' Name='Indietro' value='Volver' class='My'>"
	Response.Write "&nbsp;Pag. <b>" & PagCor & "</b> de <b>" & NrPag & "</b>&nbsp;"
	if PagCor<>NrPag then  Response.Write "<input type='submit' Name='Avanti' value='Avanzar' class='My'>&nbsp;"
	Response.Write "<input type='submit' Name='Fine' value='Guardar y Salir' class='My'>&nbsp;"
	'if PagCor=NrPag then Response.Write "<input type='button' class='My' value='Aggiungi " & Lcase(Tipo) & "' OnClick=""document.location='AddElemento.asp?Tipo=" & Tipo & "'"">"

	Response.Write Chr(13) & chr(10)
	Response.Write "<input type='hidden' name='DETTAGLI' Value='" & Request.ServerVariables ("URL") & "'>"
	Response.Write "<input type='hidden' name='Ricerca' Value='" & Request("Ricerca") & "'>"
	
	Response.Write "<input type='hidden' name='CONOSCENZE' Value='"  
		IF Tipo="CONOSCENZE" then 
			Response.Write "CONOCIMIENTO"
		end if 
		Response.Write "'>"	
	Response.Write "<input type='hidden' name='AREA_CONOSCENZA' Value='" & AREA_CONOSCENZA & "'>"
	
	Response.Write "<input type='hidden' name='CAPACITA' Value='" 
		IF Tipo="CAPACITA" then 
			Response.Write "CAPACITA"
		end if 
		Response.Write "'>"		
	Response.Write "<input type='hidden' name='AREA_CAPACITA' Value='" & AREA_CAPACITA & "'>"
	
	
	Response.Write "<input type='hidden' name='COMPORTAMENTI' Value='"
		IF Tipo="COMPORTAMENTI" then 
			Response.Write "COMPORTAMENTI"
		end if 
		Response.Write "'>"
	Response.Write "<input type='hidden' name='AREA_COMPORTAMENTI' Value='" & AREA_COMPORTAMENTI & "'>"
	
	Response.Write "<input type='hidden' name='Tipo' value='" & Tipo & "'>"
	
	Response.write "</TD>"
	
	Response.write "</tr>"
	Response.write "<tr>"
		Response.write "<td colspan=3 bgcolor='#3399CC'></td>"
	Response.write "</tr>"

	Response.Write "</table>"
	
end if

Response.Write "</form>"
%>
</center>
<!-- #include Virtual="/strutt_coda2.asp" -->
