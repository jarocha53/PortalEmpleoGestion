<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->

<!-- #INCLUDE virtual="/strutt_testa2.asp" -->
<!-- #INCLUDE FILE="../../Utils.asp" -->
<center>
<form name='QUADRO' Method='POST'>
<%

Set Conn = server.CreateObject ("ADODB.Connection")
Conn.open strConn
'========================
' CAPACITA
'========================
Sql = ""
Sql = Sql & " SELECT  CAPACITA.DENOMINAZIONE, PERS_CAPAC.GRADO_CAPACITA, AREA_CAPACITA.DENOMINAZIONE "
Sql = Sql & " FROM PERS_CAPAC, CAPACITA, AREA_CAPACITA"
Sql = Sql & " WHERE "
Sql = Sql & " CAPACITA.ID_AREACAPACITA = AREA_CAPACITA.ID_AREACAPACITA AND "
Sql = Sql & " PERS_CAPAC.ID_CAPACITA = CAPACITA.ID_CAPACITA AND "
Sql = Sql & " PERS_CAPAC.ID_PERSONA = " & IDP
Sql = Sql & " ORDER BY AREA_CAPACITA.DENOMINAZIONE "

'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
Set Rs = Conn.Execute (Sql)
if not rs.eof then
	LST_CAPACITA = Rs.getrows()
end if
Set Rs = Nothing

Sql = ""
Sql = Sql & " SELECT  DENOMINAZIONE, POSSESSO  "
Sql = Sql & " FROM ELEMENTI_USR "
Sql = Sql & " WHERE SubStr(FL_TIPO,1,6)='CAPACI' AND ID_PERSONA = " & IDP
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
Set Rs = Conn.Execute (Sql)
if not rs.eof then
	LST_CAPACITA_USR = Rs.getrows()
end if
Set Rs = Nothing

'========================
'CONOSCENZE
'========================
Sql = ""
Sql = Sql & " SELECT CONOSCENZE.DENOMINAZIONE, PERS_CONOSC.COD_GRADO_CONOSC, AREA_CONOSCENZA.DENOMINAZIONE, AREA_CONOSCENZA.ID_AREACONOSCENZA "
Sql = Sql & " FROM PERS_CONOSC, CONOSCENZE,AREA_CONOSCENZA "
Sql = Sql & " WHERE "
Sql = Sql & " CONOSCENZE.ID_AREACONOSCENZA = AREA_CONOSCENZA.ID_AREACONOSCENZA AND "
Sql = Sql & " PERS_CONOSC.ID_CONOSCENZA = CONOSCENZE.ID_CONOSCENZA AND "
Sql = Sql & " PERS_CONOSC.ID_PERSONA = " & IDP
Sql = Sql & " ORDER BY AREA_CONOSCENZA.DENOMINAZIONE "
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
Set Rs = Conn.Execute (Sql)
if not rs.eof then
	LST_CONOSC = Rs.getrows()
end if
Set Rs = Nothing

Sql = ""
Sql = Sql & " SELECT  DENOMINAZIONE, POSSESSO  "
Sql = Sql & " FROM ELEMENTI_USR "
Sql = Sql & " WHERE SubStr(FL_TIPO,1,6)='CONOSC' AND ID_PERSONA = " & IDP
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
Set Rs = Conn.Execute (Sql)
if not rs.eof then
	LST_CONOSC_USR = Rs.getrows()
end if
Set Rs = Nothing

'========================
' COMPORTAMENTI
'=======================
Sql = ""
Sql = Sql & " SELECT COMPORTAMENTI.DENOMINAZIONE,PERS_COMPOR.GRADO_PERS_COMPOR, AREA_COMPORTAMENTI.DENOMINAZIONE "
Sql = Sql & " FROM PERS_COMPOR, COMPORTAMENTI, AREA_COMPORTAMENTI "
Sql = Sql & " WHERE "
Sql = Sql & " COMPORTAMENTI.ID_AREACOMPORTAMENTO= AREA_COMPORTAMENTI.ID_AREACOMPORTAMENTO AND "
Sql = Sql & " PERS_COMPOR.ID_COMPORTAMENTO = COMPORTAMENTI.ID_COMPORTAMENTO AND "
Sql = Sql & " PERS_COMPOR.ID_PERSONA = " & IDP
Sql = Sql & " ORDER BY AREA_COMPORTAMENTI.DENOMINAZIONE "
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
Set Rs = Conn.Execute (Sql)
if not rs.eof then
	LST_COMPORTAMENTI = Rs.getrows()
end if

Sql = ""
Sql = Sql & " SELECT  DENOMINAZIONE, POSSESSO  "
Sql = Sql & " FROM ELEMENTI_USR "
Sql = Sql & " WHERE SubStr(FL_TIPO,1,6) = 'COMPOR' AND ID_PERSONA = " & IDP
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
Set Rs = Conn.Execute (Sql)
if not rs.eof then
	LST_COMPOR_USR = Rs.getrows()
end if
Set Rs = Nothing


Conn.Close
Set conn = Nothing
%>


	<table border=0 width='500' CELLPADDING=0 cellspacing=0>
		<tr>
		<td align="left" colspan="3" bgcolor='#3399CC'></td>
		</tr>
		<tr height=20 valign=center >
			<td align="left" colspan="3" bgcolor='#C6DFFF'>
				<font face=Verdana color='"#003063"' size=1><b>Elenco elementi di competenza gi� Inseriti.</b></font>
			</td>
		</tr>
		<tr>
		<td align="left" colspan="3" bgcolor='#3399CC'></td>
		</tr>
	</table>
<%
Response.Write "<center>"
Response.Write "<b><small>LE CAPACITA</small></b><BR>"
Response.Write "</center>"
Response.Write "<table border=0 width=500 cellspacing=1 cellpadding=0 style='font-family: Verdana; font-size: 8pt;'>"
IF ISARRAY(LST_CAPACITA) THEN
	'Codificate nel dizionario dati
	Response.Write "<TR><TD><b><small>Area di Riferimento/Denominazione</small></b></td><td><b><small>Possesso</small></b></td></tr>"
	VAL=""
	For I = lbound(LST_CAPACITA,2) to Ubound(LST_CAPACITA,2)
		if VAL = LST_CAPACITA(2,I) then
			VAL = ""
		else
			response.write "<tr><td COLSPAN=2 bgcolor='#f1f3f3'><B><I><small>" & LST_CAPACITA(2,I) & "</small></I></B></td></TR>"
		end if
		response.write "<td>&nbsp;&nbsp;&nbsp;<small>" & LST_CAPACITA(0,I) & "</small></td>"
		response.write "<td align='center'><small>" & LST_CAPACITA(1,I) & "</small></td></tr>"
		VAL = LST_CAPACITA(2,I)
	Next
END IF
'Definite dall'utente .......
IF ISARRAY(LST_CAPACITA_USR) THEN
	FOR I=0 TO UBOUND(LST_CAPACITA_USR,2)
		response.write "<tr><td><small>" & LST_CAPACITA_USR(0,I) & "</small></td>"
		response.write "<td align='center'><small>" & LST_CAPACITA_USR(1,I) & "</small></td></tr>"
	NEXT
END IF
Response.Write "</table>"

Response.Write "<BR><HR width='530' align='center'>"

Response.Write "<center>"
Response.Write "<b><small>LE CONOSCENZE</small></b><BR>"
Response.Write "</center>"
Response.Write "<table border=0 width=500 cellspacing=1 cellpadding=0 style='font-family: Verdana; font-size: 8pt;'>"
IF ISARRAY(LST_CONOSC) THEN
	Response.Write "<TR><TD><b><small>Area di Riferimento/Denominazione</small></b></td><td><b><small>Possesso</small></b></td></tr>"
	VAL=""
	for I = lbound(LST_CONOSC,2) to Ubound(LST_CONOSC,2)
		if VAL = LST_CONOSC(2,I) then
			VAL=""
		else
			if Int(0 & LST_CONOSC(3,I)) = 20 OR Int(0 & LST_CONOSC(3,I)) = 24 then
				response.write "<tr><td COLSPAN=2><B><I>&nbsp;</I></B></td></TR>"
			else
				response.write "<tr><td COLSPAN=2 bgcolor='#f1f3f3'><B><I><small>" & LST_CONOSC(2,I) & "</small></I></B></td></TR>"
			end if
		end if
		response.write "<td>&nbsp;&nbsp;&nbsp;<small>" & LST_CONOSC(0,I) & "</small></td>"
		response.write "<td align='center'><small>" & LST_CONOSC(1,I) & "</small></td></tr>"
		VAL=LST_CONOSC(2,I)
	next
END IF
'Definite dall'Utente
IF ISARRAY(LST_CONOSC_USR) THEN
	FOR I=0 TO UBOUND(LST_CONOSC_USR,2)
		response.write "<tr><td bgcolor='#E8F6F6'><small>" & LST_CONOSC_USR(0,I) & "</small></td>"
		response.write "<td bgcolor='#E8F6F6' align='center'><small>" & LST_CONOSC_USR(1,I) & "</small></td></tr>"
	NEXT
END IF
Response.Write "</table>"
Response.Write "<BR><HR width='530' align='center'>"

Response.Write "<center>"
Response.Write "<b><small>I COMPORTAMENTI</small></b><BR>"
Response.Write "</center>"
Response.Write "<table border=0 width=500 cellspacing=1 cellpadding=0 style='font-family: Verdana; font-size: 8pt;'>"
IF ISARRAY(LST_COMPORTAMENTI) THEN

	Response.Write "<TR><TD><b><small>Area di Riferimento/Denominazione</b></small></td><td><b><small>Possesso</small></b></td></tr>"
	VAL=""
	for I = lbound(LST_COMPORTAMENTI,2) to Ubound(LST_COMPORTAMENTI,2)
		if VAL = LST_COMPORTAMENTI(2,I) then
			VAL = ""
		else
			response.write "<tr><td COLSPAN=2  bgcolor='#f1f3f3'><B><I><small>" & LST_COMPORTAMENTI(2,I) & "</small></I></B></td></TR>"
		end if
		response.write "<td>&nbsp;&nbsp;&nbsp;<small>" & LST_COMPORTAMENTI(0,I) & "</small></td>"
		response.write "<td align='center'><small>" & LST_COMPORTAMENTI(1,I) & "</small></td></tr>"
		VAL = LST_COMPORTAMENTI(2,I)
	NEXT
END IF
'Definiti dall'Utente
IF ISARRAY(LST_COMPOR_USR) THEN
	FOR I=0 TO UBOUND(LST_COMPOR_USR,2)
		response.write "<tr><td bgcolor='#E8F6F6'><small>" & LST_COMPOR_USR(0,I) & "</small></td>"
		response.write "<td bgcolor='#E8F6F6' align='center'><small>" & LST_COMPOR_USR(1,I) & "</small></td></tr>"
	NEXT
END IF
Response.Write "</table>"

Response.Write "<BR><HR width='530' align='center'>"

Response.Write "<BR>"

%>

</form>
<br>
<center>
<div id="aa"><form><input type="button" value="Indietro" onClick="javascript:history.back()" class='My'></form></div>
</center>
<!-- #INCLUDE virtual="/strutt_coda2.asp" -->
