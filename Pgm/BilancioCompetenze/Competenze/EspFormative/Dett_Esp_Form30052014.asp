<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #Include Virtual="/strutt_testa2.asp" -->
<!-- #include Virtual = "/Include/DecCod.asp" -->
<!-- #Include Virtual="/Pgm/BilancioCompetenze/Utils.asp" -->
<!-- #include Virtual = "/include/OpenConn.asp" -->
<!-- #Include Virtual="/Pgm/BilancioCompetenze/ValidaDtNascita.asp" -->
<!-- #Include Virtual="/Include/ControlDateVB.asp" -->


<script language="javascript">
	<!--
<!-- #Include Virtual=/Include/ControlString.inc -->
<!-- #Include Virtual="/Include/ControlNum.inc" -->
<!-- #Include Virtual="/Include/ControlDate.inc" -->
function valida(Obj)
{	

	Obj.COD_TCORSO.value=TRIM(Obj.COD_TCORSO.value)
	Obj.DESC_ENTE.value=TRIM(Obj.DESC_ENTE.value)
	//Obj.AA_CORSO.value=TRIM(Obj.AA_CORSO.value)
	//Obj.AA_CORSO_UltAnno.value=TRIM(Obj.AA_CORSO_UltAnno.value)
	Obj.ORE_DURATA.value=TRIM(Obj.ORE_DURATA.value)  
	Obj.DESC_CERT.value=TRIM(Obj.DESC_CERT.value)
	
	Obj.DTINICIO.value = TRIM(Obj.DTINICIO.value)
	Obj.DTFIN.value = TRIM(Obj.DTFIN.value)
	
	if (Obj.COD_TCORSO.value == "")
	{
		alert("El campo T�tulo de Estudio es obligatorio")
		Obj.COD_TCORSO.focus() 
		return false
	}

	if (!ValidateInputStringWithNumber(Obj.COD_TCORSO.value))
	{
		alert("El T�tulo de Estudio es erroneo")
		Obj.COD_TCORSO.focus() 
		return false;
	}

	if (Obj.ID_AREA.options[Obj.ID_AREA.selectedIndex].value == "")
	{
		alert("El campo Area de Referencia es obligatorio")
		Obj.ID_AREA.focus() 
		return false;
	}

	/*agregado por Damian el 31/01/2008 */
	
	if (Obj.ID_SUBAREA.options[Obj.ID_SUBAREA.selectedIndex].value == "")
	{
		alert("El campo �rea de formaci�n espec�fica es obligatorio")
		Obj.ID_SUBAREA.focus() 
		return false;
	}
	/*fin agregado por Damian 31/01/2008 */

	
//		if (Obj.COD_STATUS_CORSO.value == "")
//	{
//		alert("Il campo:\n'Conseguimento' \n � obbligatorio")
//		Obj.COD_STATUS_CORSO.focus() 
//		return false;
//	}

	
	
	   if (!ValidateInputStringWithNumber(Obj.DESC_ENTE.value))
	{
		alert("El campo Tema es erroneo")
		Obj.DESC_ENTE.focus() 
		return false;
	}
	
	if (Obj.DTINICIO.value == "")
	{
		alert("El campo Fecha de Inicio es obligatorio");
		return false;
	}

	if ((Obj.DTFIN.value == "")&&(Obj.COD_STATUS_CORSO.value == "0"))
	{
		alert("El campo Fecha de Finalizaci�n es obligatorio si el curso fue finalizado");
		return false;
	}

	if ((Obj.DTFIN.value != "")&&(Obj.COD_STATUS_CORSO.value != "0"))
	{
		alert("El campo Fecha de Finalizaci�n no puede completarse si el curso no fue finalizado");
		return false;
	}
	

	if (Obj.DTINICIO.value != "")
	{
		if (ValidateInputDate(Obj.DTINICIO.value)== false)
		{
			return false;
		}
		if (ValidateRangeDate(document.Mod_Esp_Formative.FechaNacimiento.value,Obj.DTINICIO.value)==false)
		{
			alert("La fecha de inicio debe ser mayor a la fecha de nacimiento");
			return false;
		}
	}
	
	if (Obj.DTFIN.value != "")
	{
		if (ValidateInputDate(Obj.DTFIN.value)== false)
		{
			return false;
		}
		if (ValidateRangeDate(document.Mod_Esp_Formative.FechaNacimiento.value,Obj.DTFIN.value)==false)
		{
			alert("La fecha de fin debe ser mayor a la fecha de nacimiento");
			return false;
		}
	}
	
	if ((Obj.DTINICIO.value != "")&&(Obj.DTFIN.value != ""))
	{
		if (ValidateRangeDate(Obj.DTINICIO.value,Obj.DTFIN.value)==false)
		{
			alert("La fecha de fin debe ser mayor o igual a la fecha de inicio");
			return false;
		}
	}
	
		

	/*if ((Obj.AA_CORSO.value == "") && (Obj.AA_CORSO_UltAnno.value == "")) 
	{
		alert("Debe ingresar el Ultimo a�o cursado si el curso no ha finalizado \n o bien el A�o de finalizaci�n si el curso ha finalizado")
		Obj.AA_CORSO.focus() 
		return false
	}	
	
	
	if ((!Obj.AA_CORSO.value == "") && (!Obj.AA_CORSO_UltAnno.value == "")) 
	{
		alert("El campo A�o se refiere al ultimo A�o cursado")
		
	}	
	
		
	if (!Obj.AA_CORSO.value == "") 
	{
		if (!isNum(Obj.AA_CORSO.value))
			{
				alert("El campo A�o debe ser num�rico")
				Obj.AA_CORSO.focus() 
				return false
			}
		if (Obj.AA_CORSO.value  > <%=year(Date)%>)
			{
			alert("El A�o de Finalizaci�n no puede ser superior al a�o en curso")
			Obj.AA_CORSO.focus() 
			return false;	
			}
	if (ValidaAnnoNascita(Obj.AA_CORSO.value))
		{ 
		alert("El A�o de Finalizaci�n no puede ser inferior al a�o de nacimiento") 
		Obj.AA_CORSO.focus() 
		return false; 
		} 

	if ((Obj.COD_STATUS_CORSO.value == "1") || (Obj.COD_STATUS_CORSO.value == "2"))
		{
			alert("El A�o de finalizaci�n debe ingresarse solo en el caso que se haya finalizado el curso")
			Obj.AA_CORSO.focus() 
			return false;
		}


	}	
	 

	 if (!Obj.AA_CORSO_UltAnno.value == "") 
	{
		if (!isNum(Obj.AA_CORSO_UltAnno.value))
		{
			alert("El Ultimo A�o Cursado debe ser num�rico")
			Obj.AA_CORSO_UltAnno.focus() 
			return false;
		}
		if (Obj.AA_CORSO_UltAnno.value  > <%=year(Date)%>)
		{
			alert("El Ultimo A�o Cursado no puede ser superior al a�o en curso")
			Obj.AA_CORSO_UltAnno.focus() 
			return false;	
		}
		if (ValidaAnnoNascita(Obj.AA_CORSO_UltAnno.value))
		{ 
		alert("El Ultimo A�o Cursado no puede ser inferior al a�o de nacimiento") 
		Obj.AA_CORSO_UltAnno.focus() 
		return false; 
		}
		if (!((Obj.COD_STATUS_CORSO.value == "1") || (Obj.COD_STATUS_CORSO.value == "2")))
			{
				alert("El Ultimo A�o Cursado debe ser ingresado unicamente si el curso no fue finalizado")
				Obj.AA_CORSO.focus() 
				return false;
			}
		 
	  }	*/
  
	if ((Obj.ORE_DURATA.value=="" ))
		{
			alert("El campo 'Duraci�n en Horas' es obligatorio")
			Obj.ORE_DURATA.focus() 
			return false;
	    }
			
	if (!(Obj.ORE_DURATA.value=="" ))
		{
			if (!isNum(Obj.ORE_DURATA.value))
			{
			alert("El campo 'Duraci�n en Horas' debe ser numerico")
			Obj.ORE_DURATA.focus() 
			return false;
			}
	    }

	    if (Obj.CmbCOD_STAT_FREQ.value == "")
	    {
		alert("El campo 'Pa�s donde realiz� el curso' es obligatorio")
		Obj.CmbCOD_STAT_FREQ.focus() 
		return false;
	    }
	    
/*	    LColuccia/RGuidotti:
		Commentato controllo = Rimossa obbligatoriet� del campo "Riconosciuto in".
		if (Obj.CmbCOD_STAT_RIC.value == "")
	    {
		alert("Il campo Riconosciuto in � obbligatorio")
		Obj.CmbCOD_STAT_RIC.focus()
		return false;
	    }

	    if (!ValidateInputStringWithNumber(Obj.DESC_CERT.value))
		{
		alert("El campo Descripci�n de la Certificaci�n es obligatorio")
		Obj.DESC_CERT.focus() 
		return false;
		}
*/		
		
//OBLIGATORIA LA DURACION SI ESTA EN CURSO O FINALIZO
	if ((Obj.ORE_DURATA.value=="" ))
		{
			alert("El campo duraci�n es olbigatorio")
			Obj.ORE_DURATA.focus() 
			return false;
	    }

	if ((Obj.COD_STATUS_CORSO.value == "0")||(Obj.COD_STATUS_CORSO.value == "2"))
	{
		if (Obj.ORE_DURATA.value == "")
		{	
			alert("Debe indicar la duraci�n si el estado es finalizado o en curso");
			return false;
		}
	}
	  
//FIN VALIDACION DE DURACION
}

function Recargar()
{
	
	nombre = Mod_Esp_Formative.COD_TCORSO.value
	area = Mod_Esp_Formative.ID_AREA.options[Mod_Esp_Formative.ID_AREA.selectedIndex].value
	IdPers          = Mod_Esp_Formative.IdPers.value          
	LivMenu         = Mod_Esp_Formative.LivMenu.value         
	IND_FASE        = Mod_Esp_Formative.IND_FASE.value        
	DT_NASC         = Mod_Esp_Formative.FechaNacimiento.value 
	//Backpage        = Mod_Esp_Formative.Backpage.value        
	DT_TMST         = Mod_Esp_Formative.DT_TMST.value
	ID_FORMAZIONE   = Mod_Esp_Formative.ID_FORMAZIONE.value

		
	window.navigate("Dett_Esp_Form.asp?nombre="+nombre+"&area="+area+"&IdPers="+IdPers+"&LivMenu="+LivMenu+"&IND_FASE="+IND_FASE+"&DT_NASC="+DT_NASC+"&DT_TMST="+DT_TMST+"&ID_FORMAZIONE="+ID_FORMAZIONE)
	 
}

/** +++++++++++++++++++++++++++++++++++++++++++++++++
+	@autor: IPTECHONOLOGIES							*
++++++++++++++++++++++++++++++++++++++++++++++++++++*/

	function elimina() {
		var courseName = Mod_Esp_Formative.COD_TCORSO.value
		
		if(confirm("Est� seguro que desea borrar el curso [ "+ courseName +" ]?")){
			$.get("Del_esp_form.asp",$("#Mod_Esp_Formative").serializeArray())
			.done(
				function(data){
					alert("Estudio eliminado con exito.")
						document.FrmEsp_Formative.submit();
				}
			)
			.fail(function(data){
				alert("No fue posible eliminar el Estudio.")
			})
		}
	}

/** +++++++++++++++++++++++++++++++++++++++++++++++++*/
//-->
</script>

<%


ID_FORMAZIONE =  request("Id_Formazione")


		Sql = " SELECT "
		Sql = Sql &  " FORMAZIONE.COD_TCORSO, "
		Sql = Sql &  " AREA_CORSO.DENOMINAZIONE, "
		Sql = Sql &  " FORMAZIONE.ID_AREA, "
		Sql = Sql &  " FORMAZIONE.COD_TIPO, "
		'Sql = Sql &  " FORMAZIONE.COD_IST_ER, "
		Sql = Sql &  " FORMAZIONE.DESC_ENTE, "
		Sql = Sql &  " FORMAZIONE.AA_CORSO,"
		Sql = Sql &  " FORMAZIONE.COD_STATUS_CORSO, "
		'Sql = Sql &  " FORMAZIONE.FL_CERTIFICAZIONE, "
		Sql = Sql &  " FORMAZIONE.ORE_DURATA, "
		Sql = Sql &  " FORMAZIONE.COD_ATT_FINALE, "
		Sql = Sql &  " FORMAZIONE.DESC_CERT, "
		Sql = Sql &  " FORMAZIONE.COD_STAT_FREQ, "
		Sql = Sql &  " FORMAZIONE.COD_STAT_RIC, "
		Sql = Sql &  " FORMAZIONE.ID_SETTORE, "
		Sql = Sql &  " FORMAZIONE.DT_TMST,FORMAZIONE.DTINICIO,FORMAZIONE.DTFIN, COD_TIPO_DURACION, FORMAZIONE.ID_SUBAREA "
		Sql = Sql &  " FROM FORMAZIONE, AREA_CORSO " 
		Sql = Sql &  " WHERE "
		Sql = Sql &  " (AREA_CORSO.ID_AREA=FORMAZIONE.ID_AREA) AND "
		Sql = Sql & "  FORMAZIONE.ID_PERSONA= " & IDP & " AND FORMAZIONE.ID_FORMAZIONE=" & ID_FORMAZIONE


'PL-SQL * T-SQL  
'SQL = TransformPLSQLToTSQL (SQL) 
	Set rs = cc.execute(sql)
if not rs.eof then
	COD_TCORSO			= RS("COD_TCORSO") 'Denominazione Corso
	ID_AREA  			= RS("ID_AREA") 'Area di riferimento
	COD_TIPO			= RS("COD_TIPO")'Tipologia corso
	COD_STATUS_CORSO	= RS("COD_STATUS_CORSO")	'Status Conseguimento
	DESC_ENTE	        = RS("DESC_ENTE")	'Status Conseguimento	
'	COD_IST_ER			= RS("COD_IST_ER") 'Descrizione Istituto erogatore
	COD_ATT_FINALE		= RS("COD_ATT_FINALE") 'Codice attestato finale
	ORE_DURATA			= RS("ORE_DURATA") 'Durata in ORE
	AA_CORSO			= RS("AA_CORSO") 'Anno Corso
'	FL_STAGE			= RS("FL_STAGE") 'Stage
'	FL_CERTIFICAZIONE	= RS("FL_CERTIFICAZIONE") 'Certificazione
	DESC_CERT 	        = RS("DESC_CERT") 'Certificazione
	COD_STAT_FREQ		= RS("COD_STAT_FREQ")'Stato frequenza corso
	COD_STAT_RIC		= RS("COD_STAT_RIC")'Stato in cui � riconosciuto
	ID_SETTORE		    = RS("ID_SETTORE") 'Settore Merceologico
	DT_TMST				= RS("DT_TMST") 'Data di stampa
	ID_TIPO_DURACION    = RS("COD_TIPO_DURACION")
	FECHAINICIO         = RS("DTINICIO")
	FECHAFIN			= RS("DTFIN")
	ID_SUBAREA          = RS("ID_SUBAREA")

END IF

nombre = request("nombre")
area = request("area")

if area = "" then
	area = ID_AREA
end if

	if trim(COD_STATUS_CORSO) = "1" or COD_STATUS_CORSO = "2" then 
		AA_CORSO_UltAnno = AA_CORSO
	else
		AA_CORSO_concluso = AA_CORSO
	end if	
	


	if not isnull(COD_TCORSO) then
		COD_TCORSO= REPLACE(COD_TCORSO,"'","`")
	end if

  	sub CreaComboCOD_STATUS_CORSO(Codice)
	dim stringa_frequenza,aFrequenza

		sSql="SELECT ID_CAMPO_DESC " &_
			"FROM DIZ_DATI " &_
			"WHERE " &_
			"ID_TAB='FORMAZIONE' AND ID_CAMPO='COD_STATUS_CORSO'"

'PL-SQL * T-SQL  
'SSQL = TransformPLSQLToTSQL (SSQL) 
		set Rst=CC.Execute(sSql)
		stringa_frequenza=Rst("ID_CAMPO_DESC")
		aFrequenza=split(stringa_frequenza,"|")
	%>
		<select name="COD_STATUS_CORSO" class="textblack" id="COD_STATUS_CORSO">

	<%	for i=0 to Ubound(aFrequenza) step 2
			Response.Write "<option value='" & aFrequenza(i) & "'"
			if aFrequenza(i)=Codice then Response.Write " selected"
			Response.Write ">" & Ucase((aFrequenza(i+1))) & "</option>"
		next
	%>	
	</select>
	<%	Rst.Close
		set Rst=nothing
	end sub

	sub CreaComboCOD_ATT_FINALE(Codice)
	dim stringa_frequenza,aFrequenza

		sSql="SELECT ID_CAMPO_DESC " &_
			"FROM DIZ_DATI " &_
			"WHERE " &_
			"ID_TAB='FORMAZIONE' AND ID_CAMPO='COD_ATT_FINALE'"

'PL-SQL * T-SQL  
'SSQL = TransformPLSQLToTSQL (SSQL) 
		set Rst=CC.Execute(sSql)
		stringa_frequenza=Rst("ID_CAMPO_DESC")
		aFrequenza=split(stringa_frequenza,"|")
	%>
		<select name="COD_ATT_FINALE" class="textblack" id="COD_ATT_FINALE">

	<%	for i=0 to Ubound(aFrequenza) step 2
			Response.Write "<option value='" & aFrequenza(i) & "'"
			if aFrequenza(i)=Codice then Response.Write " selected"
			Response.Write ">" & Ucase((aFrequenza(i+1))) & "</option>"
		next
	%>	
	</select>
	<%	Rst.Close
		set Rst=nothing
	end sub


%>
<center>
	<form method="post" name="Mod_Esp_Formative" id="Mod_Esp_Formative" action="Cnf_Esp_Formative.asp" OnSubmit="return valida(this)">
	
	<input type="hidden" value="<%=Request("IdPers")%>" name="IdPers">
	<input type="hidden" value="<%=Request("LivMenu")%>" name="LivMenu">
	<input type="hidden" value="<%=Request("IND_FASE")%>" name="IND_FASE">
	<input type="hidden" value="<%=DT_NASC%>" name="FechaNacimiento">
	<input type="hidden" value="<%=DT_TMST%>" name="DT_TMST">
	<input type="hidden" value="<%=ID_FORMAZIONE%>" name="ID_FORMAZIONE">
	

	<table border="0" width="500" CELLPADDING="0" cellspacing="0">
		<tr class="tblcomm">
			<td align="left" class="sfondomenu">
				<span class="tbltext0"><b>CURSO DE CAPACITACI&Oacute;N</b></span>
			</td>
			<td width="25" valign="bottom" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">
			<img border="0" src="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
			<td width="140" valign="bottom" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">&nbsp;
			</td>
		</tr>
</table>
		
		<table border="0" width="500" CELLPADDING="0" cellspacing="0">
    <tr>
		<td class="sfondocomm" align="left" class="tbltext1">
			Para modificar el curso realizado seleccione una opci&oacute;n y presione <b>Enviar</b>
			para guardar la modificaci�n. 

		
		</td> 
		<td class="sfondocomm">
		
		
		<a onmouseover="javascript: window.status= ' '; return true;" href="Javascript:Show_Help('/Pgm/Help/BilancioCompetenze/Competenze/EspFormative/Dett_Esp_Form')">
		<img src="<%=Session("Progetto")%>/images/Help.gif" border="0" align="right" alt="Help">
		</a>
		</td>
    </tr>
		<tr height="2">
		<td class="sfondocomm" width="100%" colspan="2" background="<%=Session("Progetto")%>/images/separazione.gif">
	</td>
	</tr>
</table>	

<br>

	<table border="0" width="500" cellspacing="1" cellpadding="2" style="text-align:left;">
		
		<tr>
			
        <td class="tbltext1"><b>Nombre del Curso*</td>
			<td class="tbltext1" width="60%">
				<input style="TEXT-TRANSFORM: uppercase; width:300px;" class="textblacka" maxlength="60" name="COD_TCORSO" value="<%=COD_TCORSO%>">
			</td>
		</tr>
		<tr>
			
       <td class="tbltext1"><b>Area del Conocimiento*&nbsp;</td>
			<td class="tbltext1" width="60%">

	<%
			'Sql= ""
			'Sql= Sql & " SELECT ID_AREA, DENOMINAZIONE FROM AREA_CORSO ORDER BY DENOMINAZIONE"

''vany.. condicion para que muestre solo las tipologias asociadas a la prestacion formacion profesional

		    Sql = "SELECT ac.id_area, ac.denominazione "
            Sql = Sql & "FROM area_corso ac, "
            Sql = Sql & "area_grupodetalle agd, area_grupos ag, impresa_rolesserviciostipos irst "
            Sql = Sql & "where irst.servicio = 2 and ag.areagrupo = 3 and agd.id_area = ac.id_area "
            Sql = Sql & "and agd.areagrupo = ag.areagrupo and irst.areagrupo = ag.areagrupo and ag.areaorigen = 2 "

    
'PL-SQL * T-SQL  
''SQL = TransformPLSQLToTSQL (SQL) 
		Set Rs = Cc.Execute (Sql)

		Response.Write "<SELECT  class='textblack' name='ID_AREA' onchange='Recargar()'>"
			if not rs.eof then

				DO

					Response.Write "<option value='" & RS(0) & "'" 
					if trim(area) <> "" then
						if trim(area) = trim(rs(0)) then 
							Response.Write "SELECTED"
						end if
					end if 
					Response.Write ">"
					Response.Write replace(RS(1),"�","'",1)
					Response.Write  "</option>"
					RS.MOVENEXT
				LOOP UNTIL RS.EOF
			END IF
		Set Rs = Nothing
		Response.Write "</SELECT>"
			Response.Write "</td>"
		Response.Write "</tr>"
	%>
		
 	<%
	'Comment by AEM
	'if area <> "" then   
	%>
<!-- 	<tr height="20">
        <td align="left" class="tbltext1">
			<p align="left"><b>Area de Formaci&oacute;n Espec�fica*&nbsp;</b> </p>
        </td>
        <td align="left" width="60%" bgcolor="#FFFFFF">
			<p align="left">
-->
			<%
		
			Sql= ""
			Sql= Sql & " SELECT ID_SUBAREA, DESCRIPCION FROM AREA_CORSO_SUB WHERE ID_AREA = " & CLNG(1) & " ORDER BY DESCRIPCION"
			'Comment by AEM
			'Sql= Sql & " SELECT ID_SUBAREA, DESCRIPCION FROM AREA_CORSO_SUB WHERE ID_AREA = " & CLNG(area) & " ORDER BY DESCRIPCION"

'PL-SQL * T-SQL  
'SQL = TransformPLSQLToTSQL (SQL) 
			Set Rs = CC.Execute (Sql)

			Response.Write "<SELECT class='textblack' name='ID_SUBAREA' style='visibility:hidden;'>"
			'Comment by AEM
			'Response.Write "<SELECT  class='textblack' name='ID_SUBAREA'>"
			'Response.Write "<option value=''></option>"
				if not rs.eof then
					DO
						Response.Write "<option value='" & RS(0) & "'"
						IF ID_SUBAREA <> "" THEN
							IF CSTR(ID_SUBAREA) = CSTR(RS(0)) THEN 
								Response.Write " Selected "
							end if
						end if 
						Response.Write ">"
						Response.Write replace(RS(1),"�","'",1)
						Response.Write "</option>"
						RS.MOVENEXT
					LOOP UNTIL RS.EOF
				END IF
			Set Rs = Nothing
			Response.Write "</SELECT>"

			%>
			</td>
    </tr> 
    <%  
	'Comment by AEM
	'end if
	%>  
	
<!--		<tr>
			
        <td class="tbltext1"> <b>Tipolog�a del Curso&nbsp; </td>
			<td class="tbltext1" width="60%">-->
				<%
				'LColuccia/RGuidotti:
				'Creata combo per Tipologia Corso.
				'UtCod_Tipo= "TCORS|0|" & date & "|" & COD_TIPO & "|CmbCOD_TIPO|ORDER BY DESCRIZIONE"			
				'CreateCombo(UtCod_Tipo)
				'FINE LColuccia/RGuidotti
				%>
<!--			</td>
		</tr>
-->		
		<tr>
			
        <td class="tbltext1"><b>Estado*</b></td>
			<td class="tbltext1" width="60%">
			<%CreaComboCOD_STATUS_CORSO(COD_STATUS_CORSO)%>
			</td>
		</tr>
	
		<tr height="20">
			
        <td align="left" class="tbltext1"> <b>Instituci�n que dict� el curso:</b></td>
			
			<td class="tbltext1">
				<input style="TEXT-TRANSFORM: uppercase; width:300px;" class="textblacka" maxlength="50" name="DESC_ENTE" value="<%=DESC_ENTE%>">
			</td>
		</tr>

	<tr height="15">
		<td colspan="2">
		</td>
	</tr>
	<tr>
	<td colspan="2" height="2" bgcolor="#3399cc"><td>
	</tr>
	<tr height="15">
		<td colspan="4">
		</td>
	</tr>
	
	<tr height="20">
		<td class="tbltext1" align="left"> <b>Fecha de Inicio*</b><br>
          (dd/mm/aaaa))
		</td>
		<td align="left" class="tbltext1" width="60%">
				<input class="textblacka fecha" size="11" maxlength="10" name="DTINICIO" value="<%=FECHAINICIO%>">
				&nbsp;
		</td>
    </tr>		
	<tr height="15">
		<td colspan="4">
		</td>
	</tr    
	<tr>
		<td colspan="4"> <p><span class="textblack"> Si el curso fue finalizado 
            ingresar el siguiente dato:</span> </p>
          </td>
	</tr>

	<tr height="20">
		<td class="tbltext1" align="left"> <b>Fecha de Finalizaci�n</b><br>
          (dd/mm/aaaa)
		</td>
		<td align="left" class="tbltext1" width="60%">
				<input class="textblacka fecha" size="11" maxlength="10" name="DTFIN" value="<%=FECHAFIN%>">
				&nbsp;
		</td>
    </tr>
    
    <tr>
		<td colspan="4">&nbsp;</td>
	</tr>
	
	<!--<tr>
		<td colspan="2"> <span class="textblack"> Si el curso fue completado ingresar 
          el siguiente dato:</span> </td>
	</tr>
		
			<tr height="20">
			
        <td class="tbltext1" align="left"> <b>A&ntilde;o de Finalizaci�n*</b><br>
          (aaaa)
			</td>
			<td align="left" class="tbltext1" width="60%">
				<input class="textblacka" size="11" maxlength="4" name="AA_CORSO" value="<%=AA_CORSO_concluso%>">
			</td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
	<tr>
		<td colspan="2"> <span class="textblack"> Si fue abandonado o esta en curso, completar 
          el siguiente dato</span> </td>
		</tr>
			
    <tr height="20">
		<td class="tbltext1" align="left"> <b>�ltimo a&ntilde;o Cursado*</b><br>
          (aaaa)
		</td>
		<td align="left" class="tbltext1" width="60%">
				<input class="textblacka" size="11" maxlength="4" name="AA_CORSO_UltAnno" value="--><%=AA_CORSO_UltAnno%><!--">
		</td>
    </tr>		   
-->
	<tr>
		<td colspan="2" height="2" bgcolor="#3399cc"><td>
	</tr>
	<tr height="15">
		<td colspan="2"></td>
	</tr>


	<tr height="20">
		<td align="left" colspan="1" class="tbltext1"> <b>Duraci�n en horas*:</b></td>
		<td align="left" colspan="1" width="">
				<!-- input class="textblacka" size="11" maxlength="4" name="ORE_DURATA" VALUE="<%=ORE_DURATA%>" -->
				<% CreateCombo("ESFHO|0|" & "" & "|"&ORE_DURATA&"|ORE_DURATA|ORDER BY DESCRIZIONE") %>
<!--				 <span class="tbltext1"><b>&nbsp;&nbsp;&nbsp;&nbsp; Medida en:</b></span>
				 &nbsp;&nbsp;&nbsp;&nbsp;-->
			<%	
			'CreateComboDizDati "FORMAZIONE","COD_TIPO_DURACION","COD_TIPO_DURACION","textblack","",ID_TIPO_DURACION
			%>
		</td>
    </tr>


		<tr>
			
        <td class="tbltext1"><b>Tipo de Curso</b> </td>
			<td class="tbltext1" width="60%">
				<% CreaComboCOD_ATT_FINALE(COD_ATT_FINALE) %>
			</td>
		</tr>
<!--Comment by AEM
		<tr height="20">
			
        <td align="left" class="tbltext1"> <b>Instituci�n Certificadora</b> 
          <br>
          (Ej: IRAM, ISO, etc.)
			</td>
			<td align="left" width="60%">
				<input style="TEXT-TRANSFORM: uppercase; width:300px;" class="textblacka" maxlength="50" name="DESC_CERT" VALUE="<%=DESC_CERT%>">
Se oculta este y se introduce en Hidden-->
				<input type="hidden" class="textblacka" maxlength="50" name="DESC_CERT" value="ISO">

<!--			</td>
		</tr>
-->		
        <tr>
			
        <td class="tbltext1"><b>Pa&iacute;s donde realiz&oacute; el curso*</b> </td>
			<td class="tbltext1" width="60%">
		
		<%	
			UtCod_stat_Freq= "STATO|0|" & date & "|" & COD_STAT_FREQ & "|CmbCOD_STAT_FREQ|ORDER BY DESCRIZIONE"			
			CreateCombo(UtCod_stat_Freq)
            'UtCod_stat_Freq=0
		%>
	
<!--			</td>
		</tr>
		<tr>
			
        <td class="tbltext1"> <b>Cerfiticado en</b></td>
			<td class="tbltext1" width="60%">
-->				<%
		        'Comment by AEM
				'UtCod_Stat_Ric= "STATO|0|" & date & "|" & COD_STAT_RIC & "|CmbCOD_STAT_RIC|ORDER BY DESCRIZIONE"			
				'CreateCombo(UtCod_Stat_Ric)
				UtCod_Stat_Ric=0
				%>
			</td>
		</tr>
			
		<tr>
			
<!--        <td class="tbltext1"><b>Rama de Actividad&nbsp;</b> </td>
			<td class="tbltext1" width="60%">
	
	
		<%
		Sql= ""
		Sql= Sql & " SELECT ID_SETTORE,DENOMINAZIONE FROM SETTORI ORDER BY DENOMINAZIONE"

'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
		Set Rs = Cc.Execute (Sql)
		Response.Write "<SELECT  class='textblack' name='ID_SETTORE'>"
			if not rs.eof then
					Response.Write "<option value='NULL'></option>"		
			DO
					Response.Write "<option value='" & Rs(0) & "'" 
					
						if   trim(ID_SETTORE) = trim(Rs(0)) then 
							Response.Write "SELECTED"
						end if
					
					Response.Write ">" 
					Response.Write  LEFT(RS(1),50) 
					Response.Write 	"</option>"					
					RS.MOVENEXT
				LOOP UNTIL RS.EOF
			end if
		Set Rs = Nothing
		Response.Write "</SELECT>"

		%>
	
		</td>
		</tr>
-->

            <% ID_SETTORE="Agropecuario" %>

		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>	
	</table>
	
	<table border="0" width="500" CELLPADDING="0" cellspacing="0">
				
		<tr>
			<td align="middle">
				<a href="javascript:goToPage('Esp_Formative.asp')"><img alt="Indietro" border="0" src="<%=Session("Progetto")%>/images/indietro.gif"></a>
				<input type="image" src="<%=Session("Progetto")%>/images/conferma.gif" border="0" value="Salva" id="image1" name="image1">
				<a onmouseover="window.status =' '; return true" title="Elimina" href="javascript:elimina()">
					<img border="0" src="<%=Session("Progetto")%>/images/elimina.gif">
				</a>
			</td>
		</tr>
	</table>
	</form>
</center>
<!--#include Virtual = "/include/closeconn.asp"-->
<!-- #include Virtual="/strutt_coda2.asp" -->
