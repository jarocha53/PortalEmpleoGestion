<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #Include Virtual="/strutt_testa2.asp" -->
<!-- #include Virtual = "include/DecCod.asp" -->
<!-- #Include Virtual="/Pgm/BilancioCompetenze/Utils.asp" -->
<!-- #include Virtual = "include/OpenConn.asp" -->
<!-- #Include Virtual="/Pgm/BilancioCompetenze/ValidaDtNascita.asp" -->
<!-- #Include Virtual="/Include/ControlDateVB.asp" -->


<script language="javascript">
<!--

<!-- #Include Virtual=/Include/ControlString.inc -->
<!-- #Include Virtual="/Include/ControlNum.inc"	-->
<!-- #Include Virtual="/Include/ControlDate.inc" -->

function valida(Obj)
{	
	Obj.COD_TCORSO.value=TRIM(Obj.COD_TCORSO.value)
	Obj.DESC_ENTE.value=TRIM(Obj.DESC_ENTE.value)
	Obj.COD_STATUS_CORSO.value=TRIM(Obj.COD_STATUS_CORSO.value)
	Obj.DESC_CERT.value=TRIM(Obj.DESC_CERT.value)
	
	Obj.DTINICIO.value = TRIM(Obj.DTINICIO.value)
	Obj.DTFIN.value = TRIM(Obj.DTFIN.value)

	
	//Obj.AA_CORSO.value=TRIM(Obj.AA_CORSO.value)
	//Obj.AA_CORSO_UltAnno.value=TRIM(Obj.AA_CORSO_UltAnno.value)
	Obj.ORE_DURATA.value=TRIM(Obj.ORE_DURATA.value)  
	
	if (Obj.COD_TCORSO.value == "")
	{
		alert("El campo Nombre del curso es obligatorio")
		Obj.COD_TCORSO.focus() 
		return false;
	}
	
if (!ValidateInputStringWithNumber(Obj.COD_TCORSO.value))
	{
		alert("El campo Nombre del curso es err�neo")
		Obj.COD_TCORSO.focus() 
		return false;
	}

	if (Obj.ID_AREA.options[Obj.ID_AREA.selectedIndex].value == "")
	{
		alert("El campo �rea de formaci�n es obligatorio")
		Obj.ID_AREA.focus() 
		return false;
	}
	
	/*agregado por Damian el 31/01/2008 */
	
	if (Obj.ID_SUBAREA.options[Obj.ID_SUBAREA.selectedIndex].value == "")
	{
		alert("El campo �rea de formaci�n espec�fica es obligatorio")
		Obj.ID_SUBAREA.focus() 
		return false;
	}
	/*fin agregado por Damian 31/01/2008 */
	
	if (Obj.COD_STATUS_CORSO.value == "")
	{
		alert("El campo estado es obligatorio")
		Obj.COD_STATUS_CORSO.focus() 
		return false;
	}
	

	/* 16/06/2009 Damian: Se quita la validacion
		
	   if (!ValidateInputStringWithNumber(Obj.DESC_ENTE.value))
	{
		alert("El campo acerca de es err�neo")
		Obj.DESC_ENTE.focus() 
		return false;
	}
		
	*/

	if (Obj.DTINICIO.value == "")
	{
		alert("El campo Fecha de Inicio es obligatorio");
		return false;
	}

	if ((Obj.DTFIN.value == "")&&(Obj.COD_STATUS_CORSO.value == "0"))
	{
		alert("El campo Fecha de Finalizaci�n es obligatorio si el curso fue finalizado");
		return false;
	}

	if ((Obj.DTFIN.value != "")&&(Obj.COD_STATUS_CORSO.value != "0"))
	{
		alert("El campo Fecha de Finalizaci�n no puede completarse si el curso no fue finalizado");
		return false;
	}
	

	if (Obj.DTINICIO.value != "")
	{
		if (ValidateInputDate(Obj.DTINICIO.value)== false)
		{
			return false;
		}
		if (ValidateRangeDate(document.Esp_Formative.FechaNacimiento.value,Obj.DTINICIO.value)==false)
		{
			alert("La fecha de inicio debe ser mayor a la fecha de nacimiento");
			return false;
		}
	}
	
	if (Obj.DTFIN.value != "")
	{
		if (ValidateInputDate(Obj.DTFIN.value)== false)
		{
			return false;
		}
		if (ValidateRangeDate(document.Esp_Formative.FechaNacimiento.value,Obj.DTFIN.value)==false)
		{
			alert("La fecha de fin debe ser mayor a la fecha de nacimiento");
			return false;
		}
	}
	
	if ((Obj.DTINICIO.value != "")&&(Obj.DTFIN.value != ""))
	{
		if (ValidateRangeDate(Obj.DTINICIO.value,Obj.DTFIN.value)==false)
		{
			alert("La fecha de fin debe ser mayor o igual a la fecha de inicio");
			return false;
		}
	}
	
	
	
	/*if ((Obj.AA_CORSO.value == "") && (Obj.AA_CORSO_UltAnno.value == "")) 
	{
		alert("El campo A�o de finalizaci�n o bien �ltimo a�o cursado es obligatorio")
		Obj.AA_CORSO.focus() 
		return false;
	}	
	
	if ((!Obj.AA_CORSO.value == "") && (!Obj.AA_CORSO_UltAnno.value == "")) 
	{
		alert("El a�o solo puede ingresarse en el campo A�o de finalizaci�n o bien en �ltimo a�o cursado ")
		Obj.AA_CORSO.focus() 
		return false;
	}	
		
	
	if (!Obj.AA_CORSO.value == "") 
	{
		if (!isNum(Obj.AA_CORSO.value))
		{
			alert("El campo A�o de finalizaci�n debe ser num�rico")
			Obj.AA_CORSO.focus() 
			return false;
		}
	if (Obj.AA_CORSO.value  > <%=year(Date)%>)
		{
		alert("El campo A�o de finalizaci�n no puede ser superior al a�o actual")
		Obj.AA_CORSO.focus() 
		return false;	
		}
	if (ValidaAnnoNascita(Obj.AA_CORSO.value))
		{ 
		alert("El campo A�o de finalizaci�n debe ser superior al a�o de nacimiento") 
		Obj.AA_CORSO.focus() 
		return false; 
		} 	
		if ((Obj.COD_STATUS_CORSO.value == "1") || (Obj.COD_STATUS_CORSO.value == "2"))
		{
			alert("El campo A�o de finalizaci�n debe ser indicado solo si la experiencia fue concluida")
			Obj.AA_CORSO.focus() 
			return false;
		}
	}	
	 

	 if (!Obj.AA_CORSO_UltAnno.value == "") 
	{
		if (!isNum(Obj.AA_CORSO_UltAnno.value))
		{
			alert("El campo Ultimo a�o cursado debe ser num�rico")
			Obj.AA_CORSO_UltAnno.focus() 
			return false;
		}
		if (Obj.AA_CORSO_UltAnno.value  > <%=year(Date)%>)
		{
			alert("El campo Ultimo a�o cursado no puede ser superior al a�o actual")
			Obj.AA_CORSO_UltAnno.focus() 
			return false;	
		}
		if (ValidaAnnoNascita(Obj.AA_CORSO_UltAnno.value))
		{ 
		alert("El campo Ultimo a�o cursado debe ser superior al a�o de nacimiento") 
		Obj.AA_CORSO_UltAnno.focus() 
		return false; 
		} 
		if (!((Obj.COD_STATUS_CORSO.value == 1) || (Obj.COD_STATUS_CORSO.value == 2)))
		{
		
		alert("El campo Ultimo a�o cursado debe ser indicado solo si la experiencia no fue concluida")
		Obj.AA_CORSO_UltAnno.focus() 
		return false;
		}
	 }	*/
	  
	
	if ((Obj.ORE_DURATA.value=="" ))
		{
			alert("El campo 'Duraci�n en Horas' es obligatorio")
			Obj.ORE_DURATA.focus() 
			return false;
	    }
		
	if (!(Obj.ORE_DURATA.value=="" ))
		{
			if (!isNum(Obj.ORE_DURATA.value))
			{
			alert("El campo 'Duraci�n en Horas' debe ser num�rico")
			Obj.ORE_DURATA.focus() 
			return false;
			}
	    }
	    
	    
	    if (Obj.CmbCOD_STAT_FREQ.options[Obj.CmbCOD_STAT_FREQ.selectedIndex].value == "")
	    {
		alert("El campo 'Pa�s donde Realiz� el Curso' es obligatorio")
		Obj.CmbCOD_STAT_FREQ.focus()
		return false;
	    }
	    
/*	LColuccia/RGuidotti:
	Commentato controllo = Rimossa obbligatoriet� del campo "Riconosciuto in".
	
		if (Obj.CmbCOD_STAT_RIC.options[Obj.<EM>CmbCOD_STAT_RIC</EM>.selectedIndex].value == "")
	    {
		alert("Il campo Riconosciuto in � obbligatorio")
		Obj.CmbCOD_STAT_RIC.focus()
		return false;
	    }

	    
	    if (!ValidateInputStringWithNumber(Obj.DESC_CERT.value))
		{
		alert("El campo Descripci�n de la Certificaci�n es err�neo")
		Obj.DESC_CERT.focus() 
		return false;
		}
		
*/		
		
//OBLIGATORIA LA DURACION SI ESTA EN CURSO O FINALIZO

	if ((Obj.COD_STATUS_CORSO.value == "0")||(Obj.COD_STATUS_CORSO.value == "2"))
	{
		if (Obj.ORE_DURATA.value == "")
		{	
			alert("Debe indicar la duraci�n si el estado es finalizado o en curso");
			return false;
		}
	}
//FIN VALIDACION DE DURACION

}
//-->

function Recargar()
{
	
	nombre = Esp_Formative.COD_TCORSO.value
	area = Esp_Formative.ID_AREA.options[Esp_Formative.ID_AREA.selectedIndex].value
	IdPers          = Esp_Formative.IdPers.value          
	LivMenu         = Esp_Formative.LivMenu.value         
	IND_FASE        = Esp_Formative.IND_FASE.value        
	DT_NASC         = Esp_Formative.FechaNacimiento.value 
	Backpage        = Esp_Formative.Backpage.value        
       
	

		
	window.navigate("Add_Esp_Formative.asp?nombre="+nombre+"&area="+area+"&IdPers="+IdPers+"&LivMenu="+LivMenu+"&IND_FASE="+IND_FASE+"&DT_NASC="+DT_NASC+"&Backpage="+Backpage)
	 
}
</script>


  <%
	sub CreaComboCOD_STATUS_CORSO(Codice)
	dim stringa_frequenza,aFrequenza

		sSql="SELECT ID_CAMPO_DESC " &_
			"FROM DIZ_DATI " &_
			"WHERE " &_
			"ID_TAB='FORMAZIONE' AND ID_CAMPO='COD_STATUS_CORSO'"

'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
		set Rst=CC.Execute(sSql)
		stringa_frequenza=Rst("ID_CAMPO_DESC")
		aFrequenza=split(stringa_frequenza,"|")
	%>
		<select name="COD_STATUS_CORSO" class="textblack" id="COD_STATUS_CORSO">

	<%	for i=0 to Ubound(aFrequenza) step 2
			Response.Write "<option value='" & aFrequenza(i) & "'"
			if aFrequenza(i)=Codice then Response.Write " selected"
			Response.Write ">" & Ucase((aFrequenza(i+1))) & "</option>"
		next
	%>	
	</select>
	<%	Rst.Close
		set Rst=nothing
	end sub

	sub CreaComboCOD_ATT_FINALE(Codice)
	dim stringa_frequenza,aFrequenza

		sSql="SELECT ID_CAMPO_DESC " &_
			"FROM DIZ_DATI " &_
			"WHERE " &_
			"ID_TAB='FORMAZIONE' AND ID_CAMPO='COD_ATT_FINALE'"

		'PL-SQL * T-SQL  
		SSQL = TransformPLSQLToTSQL (SSQL) 
		set Rst=CC.Execute(sSql)
		stringa_frequenza=Rst("ID_CAMPO_DESC")
		aFrequenza=split(stringa_frequenza,"|")
		%>
		<select name="COD_ATT_FINALE" class="textblack" id="COD_ATT_FINALE">

		<%	
		for i=0 to Ubound(aFrequenza) step 2
			Response.Write "<option value='" & aFrequenza(i) & "'"
			if aFrequenza(i)=Codice then Response.Write " selected"
			Response.Write ">" & Ucase((aFrequenza(i+1))) & "</option>"
		next
		%>	
		</select>
		<%	Rst.Close
		set Rst=nothing
	end sub


nombre = request("nombre")
area = request("area")

'Response.Write nombre
'Response.Write area
%>



<center>
		<form method="post" name="Esp_Formative" action="Salva_Esp_Formative.asp" OnSubmit="return valida(this)">
		<input type="hidden" value="<%=Request("IdPers")%>" name="IdPers">
		<input type="hidden" value="<%=Request("LivMenu")%>" name="LivMenu">
		<input type="hidden" value="<%=Request("IND_FASE")%>" name="IND_FASE">
		<input type="hidden" value="<%=DT_NASC%>" name="FechaNacimiento">
		<input type="hidden" value="<%=Request("Backpage")%>" name="Backpage">
		<input type="hidden" size="50" name="txtoggi" value="<%=ConvDateToString(Now())%>">

	<table border="0" width="500" CELLPADDING="0" cellspacing="0">
		<tr class="tblcomm">
			
        <td align="left" class="sfondomenu"> <span class="tbltext0"><b>CURSO DE CAPACITACI&Oacute;N </b></span></td>
			<td width="25" valign="bottom" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">
			<img border="0" src="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
			<td width="140" valign="bottom" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">&nbsp;
			</td>
		</tr>
	</table>
	
<table border="0" width="500" CELLPADDING="0" cellspacing="0">
    <tr>
		<td class="sfondocomm" align="left" class="tbltext1">
		Ingresar el Curso de Capacitaci&oacute;n. <br>
			Indicar la informaci&oacute;n relativa al curso en el cual particip&oacute;.
			<br>Complete los campos y presione <b>Enviar</b> para guardar la modificaci&oacute;n. 
			</td> 
		<td class="sfondocomm"><a onmouseover="javascript: window.status = ' '; return true; " href="Javascript:Show_Help('/Pgm/Help/BilancioCompetenze/Competenze/EspFormative/Add_Esp_Formative/Default.htm')">
			<img src="<%=Session("Progetto")%>/images/Help.gif" border="0" align="right" alt="Help"></a>
		</td>
    </tr>
 	<tr height="2">
		<td class="sfondocomm" width="100%" colspan="2" background="<%=Session("Progetto")%>/images/separazione.gif">
		</td>
	</tr>
</table>	

<table border="0" cellpadding="0" cellspacing="1" width="500" style="text-align:left;">
	<tr>
		<td colspan="2">&nbsp;</td>
	<tr>
		
	<tr height="20">
        <td align="left" colspan="2" class="tbltext1"> <b>Nombre del Curso* &nbsp;</b> 
        </td>
        <td align="left" colspan="2" width="60%">
			<input style="TEXT-TRANSFORM: uppercase;width:300px; " class="textblacka" maxlength="60" name="COD_TCORSO" value="<%=nombre%>">
        </td>
	</tr>
	
	<tr height="20">
        <td align="left" colspan="2" class="tbltext1">
			<p align="left"><b>&Aacute;rea del Conocimiento*&nbsp;</b> </p>
        </td>
        <td align="left" colspan="2" width="60%" bgcolor="#FFFFFF">
			<p align="left">

			<%
		
			'Sql= ""
			'Sql= Sql & " SELECT ID_AREA, DENOMINAZIONE FROM AREA_CORSO ORDER BY DENOMINAZIONE"

''vany.. condicion para que muestre solo las tipologias asociadas a la prestacion formacion profesional

		    Sql = "SELECT ac.id_area, ac.denominazione "
            Sql = Sql & "FROM area_corso ac, "
            Sql = Sql & "area_grupodetalle agd, area_grupos ag, impresa_rolesserviciostipos irst "
            Sql = Sql & "where irst.servicio = 2 and ag.areagrupo = 3 and agd.id_area = ac.id_area "
            Sql = Sql & "and agd.areagrupo = ag.areagrupo and irst.areagrupo = ag.areagrupo and ag.areaorigen = 2 "
            Sql = Sql & "order by ac.denominazione "

'PL-SQL * T-SQL  
'response.Write Sql
'SQL = TransformPLSQLToTSQL (SQL) 
			Set Rs = CC.Execute (Sql)

			Response.Write "<SELECT  class='textblack' name='ID_AREA' onchange='Recargar()' >"
			Response.Write "<option value=''></option>"
				if not rs.eof then
					DO
						if cstr(RS(0)) = cstr(area) then 
							Response.Write "<option value='" & RS(0) & "' selected>"
						else
							Response.Write "<option value='" & RS(0) & "'>"
						end if
						Response.Write replace(RS(1),"�","'",1)
						Response.Write "</option>"
						RS.MOVENEXT
					LOOP UNTIL RS.EOF
				END IF
			Set Rs = Nothing
			Response.Write "</SELECT>"
			Response.Write "</td>"
			%>
    </tr>
 	<%
	'Comment by AEM
	'if area <> "" then   
	%>
<!--    <tr height="20">
        <td align="left" colspan="2" class="tbltext1">
			<p align="left"><b>Area de Formaci&oacute;n Espec�fica*&nbsp;</b> </p>
        </td>
        <td align="left" colspan="2" width="60%" bgcolor="#FFFFFF">
        
			<p align="left">
-->
			<%
		
			Sql= ""
			Sql= Sql & " SELECT ID_SUBAREA, DESCRIPCION FROM AREA_CORSO_SUB WHERE ID_AREA = " & CLNG(1) & " ORDER BY DESCRIPCION"
			'Comment by AEM
			'Sql= Sql & " SELECT ID_SUBAREA, DESCRIPCION FROM AREA_CORSO_SUB WHERE ID_AREA = " & CLNG(area) & " ORDER BY DESCRIPCION"

'PL-SQL * T-SQL  
'SQL = TransformPLSQLToTSQL (SQL) 
			Set Rs = CC.Execute (Sql)

			Response.Write "<SELECT class='textblack' name='ID_SUBAREA' style='visibility:hidden;'>"
			'Comment by AEM
			'Response.Write "<SELECT class='textblack' name='ID_SUBAREA' >"
			'Response.Write "<option value=''></option>"
				if not rs.eof then
					DO
						Response.Write "<option value='" & RS(0) & "'>"
						Response.Write replace(RS(1),"�","'",1)
						Response.Write "</option>"
						RS.MOVENEXT
					LOOP UNTIL RS.EOF
				END IF
			Set Rs = Nothing
			Response.Write "</SELECT>"
			%>
			
			</td>
    </tr> 

    <% 
	'Comment by AEM
    'end if
	%>  
	  
<!-- Comment by AEM
		<tr height="20">
		<td class="tbltext1" align="left" colspan="2"> <b>Tipolog&iacute;a del 
          Curso </b></td>
		<td colspan="2">-->
		<%
			'LColuccia/RGuidotti:
			'Creata combo per Tipologia Corso.
			'UtCod_Tipo= "TCORS|0|" & date & "||CmbCOD_TIPO|ORDER BY DESCRIZIONE"			
			'CreateCombo(UtCod_Tipo)
			'FINE LColuccia/RGuidotti
			%>
<!--		</td>
	</tr>
-->		
    <tr height="20">
		<td align="left" colspan="2">
			<p align="left" class="tbltext1"><strong>Estado</strong> </td>
		<td>
		<%CreaComboCOD_STATUS_CORSO("0")%>

		</td>
    </tr>
    
    <tr height="20">
        <td align="left" colspan="2" class="tbltext1">
			<p align="left"><b>Instituci�n que dict� el curso:</b></td>
        <td align="left" colspan="2" width="60%">
			<input style="TEXT-TRANSFORM: uppercase; width:300px; " class="textblacka" maxlength="50" name="DESC_ENTE">
        </td>
	</tr>


	<tr height="15">
		<td colspan="4">
		</td>
	</tr>

	<tr>
		<td colspan="4" height="2" bgcolor="#3399cc"><td>
	</tr>
	<tr height="15">
		<td colspan="4">
		</td>
	</tr>
	<tr height="20">
		<td class="tbltext1" align="left" colspan="2"> <b>Fecha de Inicio*</b><br>
          (dd/mm/aaaa))
		</td>
		<td align="left" class="tbltext1" colspan="2" width="60%">
				<input class="textblacka" size="11" maxlength="10" name="DTINICIO">
				&nbsp;
		</td>
    </tr>		
	<tr height="15">
		<td colspan="4">
		</td>
	</tr    
	<tr>
		<td colspan="4"> <p><span class="textblack"> Si el curso fue finalizado 
            ingresar el siguiente dato:</span> </p>
          </td>
	</tr>

	<tr height="20">
		<td class="tbltext1" align="left" colspan="2"> <b>Fecha de Finalizaci�n*</b><br>
          (dd/mm/aaaa)
		</td>
		<td align="left" class="tbltext1" colspan="2" width="60%">
				<input class="textblacka" size="11" maxlength="10" name="DTFIN">
				&nbsp;
		</td>
    </tr>
    
    <tr>
		<td colspan="4">&nbsp;</td>
	</tr>
	
	<!--	
	<tr height="20">
		<td class="tbltext1" align="left" colspan="2"> <b>A�o de finalizaci�n*</b><br>
          (aaaa)
		</td>
		<td align="left" class="tbltext1" colspan="2" width="60%">
				<input class="textblacka" size="11" maxlength="4" name="AA_CORSO">
				&nbsp;
		</td>
    </tr>
    
    <tr>
		<td colspan="4">&nbsp;</td>
	</tr>		   
    
    <tr>
	    <td>
    		<tr>
				
        <td colspan="4"> <span class="textblack"> Si fue abandonado o esta en curso completar 
          el siguiente dato:</span> </td>
			</tr>			
		</td>
    </tr>
    
    <tr height="20">
		<td class="tbltext1" align="left" colspan="2"> <b>�ltimo a&ntilde;o cursado*</b><br>
          (aaaa)
		</td>
		<td align="left" class="tbltext1" colspan="2" width="60%">
				<input class="textblacka" size="11" maxlength="4" name="AA_CORSO_UltAnno">
				&nbsp;
		</td>
    </tr>
    -->
	
	<tr>
		<td colspan="4" height="2" bgcolor="#3399cc"><td>
	</tr>
	
	<tr height="15">
		<td colspan="4">
		</td>
	</tr>
	
	<tr height="20">
		<td align="left" colspan="2" class="tbltext1"> <b>Duraci�n en horas*</b></td>
		<td align="left" colspan="2" width="">
	<!--				<input class="textblacka" size="11" maxlength="4" name="ORE_DURATA">
			 <span class="tbltext1"><b>&nbsp;&nbsp;&nbsp;&nbsp; Medida en:</b></span>
				 &nbsp;&nbsp;&nbsp;&nbsp;
-->			<%	
			'CreateComboDizDati "FORMAZIONE","COD_TIPO_DURACION","COD_TIPO_DURACION","textblack","",""
					
			CreateCombo("ESFHO|0|" & "" & "|CO|ORE_DURATA|ORDER BY DESCRIZIONE")
			%>
		</td>
    </tr>	
    

	
	<tr height="20">
		<td align="left" colspan="2">
			<p align="left" class="tbltext1"> <strong>Tipo de Curso</strong></td>
		<td colspan="2">
			<%CreaComboCOD_ATT_FINALE("0")%>
		</td>
	</tr>

	
<!--Comment by AEM
	<tr height="20">
		<td align="left" colspan="2" class="tbltext1">
			<p align="left"><b>Instituci�n Certificadora</b><br>
            (Ej: IRAM, ISO, etc.)
        </td>
        <td align="left" colspan="2" width="60%">
			 <input style="TEXT-TRANSFORM: uppercase; width:300px; " class="textblacka" maxlength="50" name="DESC_CERT" > 
Se oculta este y se introduce en Hidden-->
			<input type="hidden" class="textblacka" maxlength="50" name="DESC_CERT" value="ISO">
<!--        </td>
        	</tr>
-->

                
	<tr height="20">
		<td class="tbltext1" align="left" colspan="2">
			<p align="left"><b>Pa&iacute;s donde realiz&oacute; el curso</b></td>
		<td colspan="2">
			<%
			UtCod_stat_Freq= "STATO|0|" & date & "|CO|CmbCOD_STAT_FREQ|ORDER BY DESCRIZIONE"			
			CreateCombo(UtCod_stat_Freq)
            'UtCod_stat_Freq=0
			%>
		</td>
	</tr>	
	
<!--AEM	<tr height="20">
		<td class="tbltext1" align="left" colspan="2">
			<p align="left"><b>Certificado en</b></td>
		<td colspan="2">
AEM-->			<%
			'LColuccia/RGuidotti:
			'Modificata CreateCombo, in modo da mostrare per default riga vuota.
'			UtCod_Stat_Ric= "STATO|0|" & date & "||CmbCOD_STAT_RIC|ORDER BY DESCRIZIONE"			
'			CreateCombo(UtCod_Stat_Ric)
			'FINE LColuccia/RGuidotti
			UtCod_Stat_Ric=0
			%>
		</td>
	</tr>
		
<!--AEM	<tr height="20">
		<td class="tbltext1" align="left" colspan="2">
			<p align="left"><b>Rama de Actividad</b></td>
		<td colspan="2">
			<%
			Sql= ""
			Sql= Sql & " SELECT ID_SETTORE,DENOMINAZIONE FROM SETTORI ORDER BY DENOMINAZIONE"

'PL-SQL * T-SQL  
'SQL = TransformPLSQLToTSQL (SQL) 
			Set Rs = CC.Execute (Sql)
				Response.Write "<SELECT  class='textblack' name='ID_SETTORE'>"
				Response.Write "<option value=''></option>"
				if not rs.eof then
					DO
						Response.Write "<option value='" & Rs(0) & "'>" & LEFT(RS(1),48) & "</option>"
						RS.MOVENEXT
					LOOP UNTIL RS.EOF
				end if
			Set Rs = Nothing
			Response.Write "</SELECT>"
			%>
		</td>
	</tr>
AEM-->
            <% ID_SETTORE="Agropecuario" %>
			

	
	<tr>
		<td colspan="4">&nbsp;
		</td>
	</tr>			
	
	<tr>
		<td align="middle" colspan="3">
			<a href="javascript:goToPage('Esp_Formative.asp')"><img alt="Indietro" border="0" src="<%=Session("Progetto")%>/images/indietro.gif"></a>
			<input type="image" src="<%=Session("Progetto")%>/images/conferma.gif" border="0" value="Salva" id="image1" name="image1">
		</td>
	</tr>
</table>

</form>
</center>
<!--#include Virtual = "/include/closeconn.asp"-->
<!-- #include Virtual="/strutt_coda2.asp" -->
