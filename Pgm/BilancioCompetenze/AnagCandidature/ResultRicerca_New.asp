<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #include Virtual="/strutt_testa2.asp" -->
<!-- #include Virtual = "/Include/DecCod.asp" -->
<!-- #include Virtual = "/Include/ControlDateVb.asp" -->
<!-- #Include Virtual="/Pgm/BilancioCompetenze/Utils.asp" -->
<!-- #include Virtual = "/Include/OpenConn.asp"-->

<%
if request("lDwn") = "" then
	lDwn = 0
else
	lDwn = request("lDwn")
end if
%>

<%
'=================================================================
'	Persistencia en ASP del lado del cliente 
'
'	Funciones particulares de este ASP
'=================================================================
dim TotalRegistros, PaginadoCantRenglones

PaginadoCantRenglones = 20	' Cantidad de renglones por pagina

%>
<!-- #include Virtual = "/IncludeMTSS/PersistenciaEnASPcliente.asp"-->

<SCRIPT LANGUAGE=VBScript>
<!--
'-----------------------------------------------------------
' Persistencia : evalua si un item esta seleccionado
'-----------------------------------------------------------
function RegistrosSeleccionadosVerificaSeleccion (Indice, ItemDatos)
'-----------------------------------------------------
' evalua un registro si esta seleccionado o no
'Necesita:	
'	Indice al alelmento de la lista en el form
'Devuelve:
'	funcion = true Esta seleccionado
'	ItemDatos Los datos necesarios formateados para grabar el registro
'-----------------------------------------------------
	dim EstaSeleccionado, xIndice 
	EstaSeleccionado = false
	
	xIndice = cint(Indice) - 1
	
	' el item esta seleccionado ?
	if document.forms.ResultRic.ID_FIGPROF (xIndice).checked = true then
		EstaSeleccionado = true
		' recupera el codigo de la figura profesional
		ItemDatos = document.forms.ResultRic.ID_FIGPROF(xIndice).value
	end if

	RegistrosSeleccionadosVerificaSeleccion = EstaSeleccionado	
end function

-->
</SCRIPT>

<%
'=================================================================
'	Otras funciones del ASP
'=================================================================
%>

<script language="Javascript">
<!--
	function FocusFigProf(ID_FIGPROF)
	// Apre e visualizza la finestra con la descrizione breve
	// della figura professionale
	
	{
		URL = "FocusFigProf.asp?"
		URL = URL + "ID_FIGPROF=" + ID_FIGPROF + "&VISMENU=NO"
		opt = "address=no,status=no,width=660,height=350,top=150,left=35,scrollbars=yes"
		window.open (URL,"",opt)
	}


	function SalvaAvantiIndietro(sUrl, Target)
	// --------------------------------------------------------------------------------
	// Gestisce l'eventuale visualizzazione su pi� pagine
	// e il passaggio necessario per la navigazione
	// a seguito della procedura di aggiornamento
	// --------------------------------------------------------------------------------
	
	{
	document.forms.ResultRic.action = sUrl		
	document.forms.ResultRic.IdPers.value = document.valori.IdPers.value	
	document.forms.ResultRic.IND_FASE.value = document.valori.IND_FASE.value
	document.forms.ResultRic.LivMenu.value = document.valori.LivMenu.value
	// Intercepto el llamado al paginado para guardar lo seleccionado en la pagina actual 
	RegistrosSeleccionadosRefreshForm2 ( <%=lDwn%> , "ResultRic")
	
		if (Target == "NextPage")
		{
			A = <%=lDwn%> + 20
			document.forms.ResultRic.lDwn.value = A
			document.forms.ResultRic.Forward.value = "SI"
		}
	
		if 	(Target == "Back")
		{
			A = <%=lDwn%> - 20
			document.forms.ResultRic.lDwn.value = A		
		}

		if (Target == "SaveAllArea")
		{
			document.forms.ResultRic.lDwn.value =  <%=lDwn%>
			document.forms.ResultRic.CandAll.value = "SI"
		}

		if (Target == "Save")
		{
			A = <%=lDwn%> 
			document.forms.ResultRic.lDwn.value = A
			document.forms.ResultRic.Forward.value = "SI"
		
		}

			
	document.forms.ResultRic.submit();
	}
	
	
	
	
function Prossimita(ID_FIGPROF)
{	
// Visualizza la finestra sul Gap rispetto agli elementi di competenza
// associati al profilo


		URL = "/pgm/BilancioCompetenze/Bilancio_Pross/reportfigprof.asp?"
		URL = URL + "ID_FIGPROF=" + ID_FIGPROF + "&Vismenu=NO" + "&IdPers=" + <%=IDP%> + "&VisAnag=NO"
		opt = "address=no,status=no,width=630,height=400,top=150,left=35,scrollbars=yes"
		window.open (URL,"",opt)

}

//-->
</script>


<form name="ResultRic" Method="post" OnSubmit="return CheckChanges(this)">
	<input type="hidden" name="IdPers" value>
	<input type="hidden" name="IND_FASE" value>
	<input type="hidden" name="LivMenu" value>
	<input type="hidden" name="testo" Value="<%=Request("testo")%>">
	<input type="hidden" name="ID_AREAPROF" Value="<%=Request("ID_AREAPROF")%>">
	<input type="hidden" name="ID_SETTORE" Value="<%=Request("ID_SETTORE")%>">
	<input type="hidden" name="DenArea" Value="<%=Request("DenArea")%>">
	<input type="hidden" name="DenSettore" Value="<%=Request("DenSettore")%>">
	<input type="hidden" name="lDwn" Value>
	<input type="hidden" name="CandAll" Value>
	<input type="hidden" name="InputPage" Value="<%=Request("InputPage")%>">
	<input type="hidden" name="Forward" Value>
	<input type="hidden" name="BackPage" Value="<%=Request("BackPage")%>">
	<input type="hidden" name="FigProfCandidati" value="<%=Request("FigProfCandidati")%>">
	<input type="hidden" name="AreeCandidate" Value="<%=Request("AreeCandidate")%>">
	<input type="hidden" size="50" name="txtoggi" value="<%=ConvDateToString(Date())%>">
<%			

''''Response.Write  "FigProfCandidati " & request("FigProfCandidati")

IF request("FigProfCandidati") = "True" THEN
	SQL = ""
	SQL= SQL& "SELECT "
	SQL= SQL& "FIGUREPROFESSIONALI.ID_FIGPROF, FIGUREPROFESSIONALI.DENOMINAZIONE,  "
	SQL = SQL& " FIGUREPROFESSIONALI.BREVE_DESC, "
	SQL= SQL& " PERS_FIGPROF.INCROCIO, PERS_FIGPROF.GRADO_FP_PERS, PERS_FIGPROF.DT_TMST "
	SQL = SQL& " FROM "
	SQL = SQL& "FIGUREPROFESSIONALI, PERS_FIGPROF"
	SQL = SQL& " WHERE "
	SQL = SQL& "  FIGUREPROFESSIONALI.ID_FIGPROF = PERS_FIGPROF.ID_FIGPROF "
	SQL = SQL& "  AND IND_STATUS = 0 "
	SQL = SQL& " AND INCROCIO = 'S' "
	SQL = SQL& " AND ID_PERSONA = " & IDP 
	SQL= SQL & " ORDER BY UPPER(FIGUREPROFESSIONALI.DENOMINAZIONE) "

ELSE	
	SQL = ""
	SQL= SQL& "SELECT "
	SQL= SQL& "FIGUREPROFESSIONALI.ID_FIGPROF, FIGUREPROFESSIONALI.DENOMINAZIONE,	  "
	SQL = SQL& " FIGUREPROFESSIONALI.BREVE_DESC, "
	SQL = SQL& "NVL((SELECT PERS_FIGPROF.INCROCIO FROM PERS_FIGPROF "
	SQL = SQL& " WHERE IND_STATUS = 0 "
	SQL = SQL& " AND ID_PERSONA = " & IDP 
	SQL = SQL& " AND FIGUREPROFESSIONALI.ID_FIGPROF = PERS_FIGPROF.ID_FIGPROF),'X') PERS_INCROCIO, "
	SQL = SQL& " NVL((SELECT PERS_FIGPROF.GRADO_FP_PERS FROM PERS_FIGPROF "
	SQL = SQL& " WHERE IND_STATUS = 0 "
	SQL = SQL& " AND ID_PERSONA = " & IDP 
	SQL = SQL& " AND FIGUREPROFESSIONALI.ID_FIGPROF = PERS_FIGPROF.ID_FIGPROF),0) PERS_GRADOPROSSIMITA, "
	SQL = SQL& " NVL((SELECT PERS_FIGPROF.DT_TMST FROM PERS_FIGPROF "
	SQL = SQL& " WHERE IND_STATUS = 0 "
	SQL = SQL& " AND ID_PERSONA = " & IDP 
	SQL = SQL& " AND FIGUREPROFESSIONALI.ID_FIGPROF = PERS_FIGPROF.ID_FIGPROF),'') PERS_DT_TMST "
	SQL = SQL& "  FROM "
	SQL = SQL& "FIGUREPROFESSIONALI"
	SQL = SQL& " ,VALIDAZIONE "
		
	if request("ID_AREAPROF") <> "" then
		SQL = SQL & " ,AREE_PROFESSIONALI "
	end if
	
	if request("ID_SETTORE") <> "" then
		SQL= SQL& " ,SETTORI_FIGPROF "
	end if 

	SQL= SQL & "WHERE "

	W= ""
	W = W & " ((FIGUREPROFESSIONALI.ID_VALID = VALIDAZIONE.ID_VALID "
	W = W & " AND VALIDAZIONE.FL_VALID = 1 ) OR "
	W = W & " (FIGUREPROFESSIONALI.ID_AREAPROF = 0 and VALIDAZIONE.ID_VALID = FIGUREPROFESSIONALI.ID_VALID)) "
	if request("testo") <> "" then
		W = W & "AND "
		W = W & " UPPER(FIGUREPROFESSIONALI.DENOMINAZIONE) LIKE '%" & ucase(request("testo")) & "%' "
	end if
		
	if request("ID_AREAPROF") <> "" then
		W = W & " AND "
		W = W & " FIGUREPROFESSIONALI.ID_AREAPROF = AREE_PROFESSIONALI.ID_AREAPROF "
		W = W & " AND "
		W = W & " FIGUREPROFESSIONALI.ID_AREAPROF = " & request("ID_AREAPROF")
	end if

	if request("ID_SETTORE") <> "" then
		W = W & " AND "
		W = W & "SETTORI_FIGPROF.ID_SETTORE = " & request("ID_SETTORE")
		W = W & " AND "
		W = W & "SETTORI_FIGPROF.ID_FIGPROF = FIGUREPROFESSIONALI.ID_FIGPROF "
	end if
	
	SQL= SQL & W 

	SQL= SQL & " ORDER BY  FIGUREPROFESSIONALI.DENOMINAZIONE "
'Response.Write (SQL)
END IF
	
	'==========================================
	' Ejecuta el query
	'==========================================
	Set Rs = Server.CreateObject("ADODB.Recordset")
	Rs.CursorLocation = 3	
	Rs.CursorType = 3		
	Rs.ActiveConnection = CC
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
	Rs.Open SQL
	rc= 0
	
	if not rs.eof then
		rc = rs.RecordCount
		'----------------------------------------------------------------------
		'	Primera Vez 
		'	Lee todo buscado los items seleccionados para persistencia
		'----------------------------------------------------------------------
		if PersistenciaRenglones = "" then
			RegistrosSeleccionadosAgregaItem "0", "x"

			' recorre las paginas de datos, buscando los items seleccionados para persistencia
			Puntero = 0
			Rs.MoveFirst	' se posiciona en el primer registro
			do 
				' lee los registros de una pagina 
				xRESULT = Rs.getrows(PaginadoCantRenglones )	
				if IsArray(xRESULT) = true then
					' recorre items
					for I = lbound(xRESULT,2) to Ubound(xRESULT,2)
						' busca items seleccionados
						if xRESULT(3, I) = "S"  then 	
							' obtiene el valor del campo
							ItemDatos = xRESULT(0, I)
							' agrega al vector
							RegistrosSeleccionadosAgregaItem Puntero + i + 1, ItemDatos
						end if
					next
					' siguiente pagina
					Puntero = Puntero + PaginadoCantRenglones
				end if
			loop while not rs.eof			
			' se reposiciona en el primer registro
			Rs.MoveFirst
		end if
		
		'..Response.Write PersistenciaRenglones
		
		'----------------------------------------------------------------------
		'	Siguientes veces 
		'	lee la pagina solicitada
		'----------------------------------------------------------------------
		if ldwn < 20 then ldwn = 0
		Rs.Move (ldwn)
		RESULT = Rs.getrows(20)	
	end if	
	rs.Close
	set rs = nothing
	TotalRegistros = rc
	
	'Response.Write sql
	
	%>
	<table border="0" width="500" CELLPADDING="0" cellspacing="0">
		<tr>
			<td align="left" class="sfondomenu"><b><span class="tbltext0"> 
				<b>&nbsp; 
				  <%
				  IF Request("FigProfCandidati") = "True" THEN
						Response.Write "POSTULACIONES A PUESTOS/OCUPACIONES&nbsp;"
				  ELSE
						Response.Write "POSTULACIONES A PUESTOS/OCUPACIONES&nbsp;"
						'Response.Write "POSTULACIONES A GRUPOS OCUPACIONALES"
					END IF 
					%>
				</b> 
			</td>
			<td width="80" valign="bottom" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">
				<img border="0" src="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
			<td width="20%" valign="middle" align="right" class="tbltext1" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
		</tr>
	</table>
	
	<table border="0" width="500" CELLPADDING="0" cellspacing="0">
		<tr>
			<td class="sfondocomm" align="left" class="tbltext1">
				La tabla muestra los <b>Puestos/Ocupaciones</b>
				<%if Request("FigProfCandidati") = "True" then 
					Response.Write " para las cuales se puede ingresar una postulaci�n." 
				end if%>
				Para ingresar <br>o anular una postulaci�n a un Puesto/Ocupaci�n. 
				<br>seleccionar o quitar la selecci�n de la casilla correspondiente.
			
				<%if Request("FigProfCandidati") = "True" then %>
					<br> Para ingresar una postulaci�n presionar el bot�n <b>Ingresar Postulaci�n</b>.		
				<%end if %>
			
				<% if request("inputpage")="AreaProf" then %>
					 <br> Es posible ingresar una postulaci�n a un Grupo Ocupacional sin especificar ningun Puesto
					 presionando el boton <b>Enviar</b>.
				 <%end if%>
					
			</td> 
			<td class="sfondocomm"><a onmouseover="javascript: window.status=' '; return true" href="Javascript:Show_Help('/Pgm/Help/BilancioCompetenze/AnagCandidature/ResultRicerca')">
				<img src="<%=Session("Progetto")%>/images/Help.gif" border="0" align="right" alt="Help"></a>
			</td>
		</tr>
 		<tr height="2">
			<td class="sfondocomm" width="100%" colspan="2" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>
	<br>

	<%
	if request("InputPage") = "AreaProf" then
	%>
	  <table border="0" width="500" CELLPADDING="0" cellspacing="0">
	    <tr> 
	      <td align="left"> 
	        <%if request("AreeCandidate") = "True"	then %>
				<a class="textred" onmouseover="javascript: window.status=' '; return true" href="javascript:SalvaAvantiIndietro(&quot;DelCandid.asp&quot;,&quot;DeleteAllArea&quot;)"> 
					<b>Remover Postulaciones</b>
				</a> 
	        <%else%>
				&nbsp; 
	        <%end if%>
	      </td>
	      <!--<td align="right"> <a class="textred" onmouseover="javascript: window.status=' '; return true" href="javascript:SalvaAvantiIndietro(&quot;SalvaCandidature.asp&quot;,&quot;SaveAllArea&quot;)"> 
	        <b>Candidatearse a todos los Puestos</b></a> </td>-->
	    </tr>
	  </table>
	  <br>
	<%
	end if

	if request("FigProfCandidati") = "True" then
	%>
		<b>
		<a class="textred" onmouseover="javascript: window.status=' '; return true" href="javascript:goToPage(&quot;CercaFigProf.asp&quot;)"> 
		<b>Ingresar Postulaci�n</b> </a> 
	<%
	end if

	'-------------------------------------------------------
	' Hay postulaciones para mostrar ?
	'-------------------------------------------------------
	if IsArray(RESULT) then
		Response.Write "<input type='hidden' name='limite' value='" & Ubound(RESULT,2) & "'>"
		Pag = cint(RC) mod 20		' **** CAntidad de regnlones de paginado
			if Pag> 0 then
				Pagine= 0 + int(cint(RC)/20)+1
			else
				Pagine = 0 + int(cint(RC)/20)
			end if
		Response.Write "<p>"
		Response.Write "<table width='500' border=0 cellspacing=1 cellpadding=1>"

		Response.Write "<tr class='sfondocomm'>"
		Response.Write "<td valign=top width='50%' align='right' colspan='4'>"
		Response.write "<b>Nro. Elementos:</b> " & RC  & "</b>"
		Response.Write "</td>"
		Response.Write "</tr>"
		IF request("testo") <> "" or request("ID_AREAPROF") <> "" or Request("ID_SETTORE") <> "" then
			Response.Write "<tr class=sfondocomm>"
			Response.write "<td align='left' colspan='4'><small>"
	
			if request("testo") <> "" then
				if request("testo") = " " then
					Response.Write "<b><B>Denominaci�n del registro�</B>(texto buscado):</b>&nbsp; qualsiasi " 
				else 
					Response.Write "<b>Denominaci�n del registro (texto buscado):</b>&nbsp;" & request("testo") & "<br>"
				end if
			end if
			
			if Request("ID_AREAPROF") <> "" then
				Response.Write "<b>Grupo:</b>&nbsp;" & Lcase(request("DenArea")) & "<br>"
			end if
			
			if Request("ID_SETTORE") <> "" then 
				Response.Write  "<br><b>Sector:</b>&nbsp;" & Lcase(request("DenSettore")) 
			end if
			
			Response.Write "</small></td>"
			Response.Write "</tr>"
		END IF		
		
		'--------------------------------------------------
		' Titulos de la tabla de postulaciones
		'--------------------------------------------------
		Response.Write "<tr class='sfondocomm'>"
		Response.Write "<td width='4%'>&nbsp;</td>"
		Response.Write "<td width='84%'><b>Denominaci�n</b></td>"
		Response.Write "<td WIDTH='7%' align=center><b>Proximidad</b></td>"
		Response.Write "<td WIDTH='7%'  align=center><b>Postulaci�n</b></td>"
		Response.Write "</tr>"

		' lista de postulaciones mostradas
		for I = lbound(RESULT,2) to Ubound(RESULT,2)
			Response.Write "<tr class='tblsfondo'>"
			Response.Write "<td width=5% align='center' class='tbltext1'>"
					
				if trim(Result(2,I)) <> "" then 			
					Response.Write "<b>&nbsp;<a class='tblagg'  onmouseover=""javascript: window.status=' '; return true"" href='javascript: FocusFigProf(" & RESULT(0,I) & ")' title='Visualizza Dettaglio Figura Professionale'>"
					Response.write  I + 1 + lDwn & "</b></a>&nbsp;"
				else
					Response.write  I + 1 + lDwn & "</b>"
				end if
					
				Response.Write "</td>"
				Response.Write "<td class='tbldett'>" 
				Response.Write RESULT(1,I)
				Response.Write "</td>"
				Response.Write "<td align='center' class='tbltext1' >"
						
				if RESULT(3,I) <> "X"  then 
					Response.Write "<INPUT TYPE='HIDDEN' NAME='PERS_ID_FIGPROF' "
					Response.Write "VALUE ='" & RESULT(0,I) & "'	>"
					Response.Write "<INPUT TYPE='HIDDEN' NAME='PERS_DT_TMST' "
					Response.Write "VALUE ='" & RESULT(5,I) & "'>"						
				end if	

				if RESULT(4,I) <> "0" then 
					GrafPross = ""
					GrafPross = GrafPross &	"<a href=""Javascript:Prossimita(" & RESULT(0,I)& ")"" onmouseover=""javascript:window.status=' '; return true"">" 
					GrafPross = GrafPross & "<img src='"  & Session("Progetto") & "/images/GRAF.gif' border=0>"
					Response.Write 	GrafPross	
				end if

				Response.Write "</td>"
				Response.Write "<td align='center' class='tbltext1'>"
				
				'--------------------------------------------------------
				' check de items Seleccionado  
				'--------------------------------------------------------
				Response.Write "<INPUT TYPE='CHECKBOX' NAME='ID_FIGPROF' "
				Response.Write "value='" & RESULT(0,I) &"'" 
				
				' el estado de la seleccion lo toma de los vectores de persistencia
				
				if RESULT(3,I) = "S"  then 
					Response.Write " CHECKED" 
				end if						
				Response.Write " >"

			Response.Write "</td>"
			Response.Write "</tr>"
		next
		Response.Write "</table>"			

		'---------------------------------------------------
		' Botones de Paginado de registros
		'---------------------------------------------------
		%>
		<br>
		<table border="0" width="500">		
			<tr>
				<td align="right" class="tbltext1">
					<%
					' Paginado para Atras	
					if lDwn > 1 then
					%>
						<a class="textred" onmouseover="javascript: window.status=' '; return true" href="javascript:SalvaAvantiIndietro('ResultRicerca.asp','Back')">
						<img src="<%=Session("Progetto")%>/images/precedente.gif" border="0">				
						</a>
					<%
					end if

					' Paginado para Adelante	
					if I + 1 +lDwn <= cint(RC) then
						%>
						<a class="textred" onmouseover="javascript: window.status=' '; return true" href="javascript:SalvaAvantiIndietro(&quot;ResultRicerca.asp&quot;,&quot;NextPage&quot;)">
						<img src="<%=Session("Progetto")%>/images/successivo.gif" border="0">				
						</a> 
						<%
					end if
					%>
				</td>
			</tr>
		</table>

		<%
		'---------------------------------------------------
		' Botones de comando - Atras o enviar
		'---------------------------------------------------
		' Pagina Anterior para ir para atras 
		if Request("FigProfCandidati") = "True" then
			URL = "default.asp"
		else
			URL = Request("BackPage")
		end if
		%>
		<table border="0" width="500">		
			<tr>
				<td align="right" class="tbltext1">
						<a class="textred" onmouseover="javascript: window.status=' '; return true" href="javascript:goToPage('<%=URL%>')">
						<img src="<%=Session("Progetto")%>/images/indietro.gif" border="0">				
						</a> 
				</td>			
				<td align="left">			
					<a class="textred" onmouseover="javascript: window.status=' '; return true" href="javascript:SalvaAvantiIndietro(&quot;SalvaCandidature.asp&quot;,&quot;Save&quot;)">
					<img src="<%=Session("Progetto")%>/images/Conferma.gif" border="0">
					</a> 
				</td>			
			</tr>
		</table>
		<%
	else
		'----------------------------------
		' No hay registros de postulaciones
		'----------------------------------
		If request("FigProfCandidati")="True" then
			%>
			<br>
			<br>
			<table width="500">
				<tr>
					<td align="center" class="tbltext3"> Ninguna Postulaci�n Registrada. <br>
					</td>
				</tr>
				<tr>
					<td align="center" class="tbltext1">
						<a class="textred" onmouseover="javascript: window.status=' '; return true" href="javascript:goToPage('default.asp')">
							<img src="<%=Session("Progetto")%>/images/indietro.gif" border="0">				
						</a> 
					</td>	
				</tr>
			</table>
		<%	
		else 	
		%>
			<table width="500">
				<br>
				<br>
				<tr>
					<td class="tbltext3" align="center"><b> No han sido encontrados elementos que satisfacen<br>los criterios de b&uacute;squeda especificada
						<br><br>
						</b></td>
				</tr>
				<tr>
					<td align="center" class="tbltext1">
						<a class="textred" onmouseover="javascript: window.status=' '; return true" href="javascript:history.back()">
							<img src="<%=Session("Progetto")%>/images/indietro.gif" border="0">				
						</a> 
					</td>	
				</tr>
			</table>
		<%
		end if
	end if
	%>

	<%			
	'------------------------------------------------------------------
	' Manejo de persistencia de items seleccionados	
	'------------------------------------------------------------------
	%>
	<input type="hidden" name="PersistenciaRenglones" value="<%=PersistenciaRenglones%>">
	<input type="hidden" name="PersistenciaDatos" value="<%=PersistenciaDatos%>">
	<input type="hidden" name="PersistenciaTotalRegistros" value="<%=TotalRegistros%>">
	<input type="hidden" name="PersistenciaPaginadoCantRenglones" value="<%=PaginadoCantRenglones%>">
</form>
<!--#include virtual = "/include/CloseConn.asp"-->
<!-- #include Virtual="/strutt_coda2.asp" -->
