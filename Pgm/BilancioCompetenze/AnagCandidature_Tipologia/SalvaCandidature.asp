<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #include Virtual="/strutt_testa2.asp" -->
<!--#include virtual="/include/SysFunction.asp"-->
<!--#include virtual="/include/ValInfPers.asp"-->
<!-- #include Virtual = "/Include/ControlDateVb.asp" -->
<!-- #Include Virtual="/Pgm/BilancioCompetenze/Utils.asp" -->
<!--#include Virtual = "/Include/OpenConn.asp"-->
<%
errore = 0

CC.BeginTrans
OraDate = ConvDateToDB(Now())
LIV = request("LivMenu")

dim i, j, MaxSelAntes, MaxSelAhora, Max1, Max2, Sql
dim xRegistro1, xRegistro2

'-------------------------------------------------------------
'CANDIDATURE FIGURE PROFESSIONALI AGGIORNAMENTO
'-------------------------------------------------------------
'Gestione di eventuali occorrenze gi� registrate in PERS_FIGPROF 
'Valorizzazione a IND_STATUS 1, INCORCIO N di eventuali candidature preesistenti nella pagina precedente
'Per mantenere per le candidature eventuale grado di prossimit� (vedi valorizzazione nel ciclo successivo
	'------------------------------------------------------------------------------------
	' 1) Datos de item registrados en tablas y seleccionadas en paginas
	'------------------------------------------------------------------------------------
	dim xFigurasProfEnTabla, xFigurasProfSeleccionadas, xFigurasProfSelCodigos
	
	' Lista de figuras que tenia registradas la persona en la tabla PERS_FIGPROF al entrar
	xFigurasProfEnTabla = Request("FigurasProfEnTabla")
	FigurasProfEnTabla = split (xFigurasProfEnTabla, "*")
	MaxSelAntes = ubound(FigurasProfEnTabla)
	
	' Lista de figuras actualmente seleccionadas en las paginas
	' marcas de figuras seleccionadas
	xFigurasProfSeleccionadas = Request("PersistenciaRenglones")
	FigurasProfSeleccionadas = split (xFigurasProfSeleccionadas, "*")
	MaxSelAhora = ubound(FigurasProfSeleccionadas)
	' codigos de figuras
	xFigurasProfSelCodigos = Request("PersistenciaDatos")
	FigurasProfSelCodigos = split (xFigurasProfSelCodigos, "*")
	
'..	Response.Write xFigurasProfSeleccionadas & "<br>"
'..	Response.Write xFigurasProfSelCodigos & "<br>"
	'------------------------------------------------------------------------------------
	' 2) Desasigna todas las figuras profesionales que se visualizan en la pagina llamadora (al entrar)
	'	Son las figuras del grupo ocupacional que se esta mostrando al inicio
	'	y que estaban seleccionadas y registradas en la Tabla PERS_FIGPROF al entrar  
	'------------------------------------------------------------------------------------
	for i = 0 to MaxSelAntes
		 xRegistro1 = FigurasProfEnTabla(i)
		 ' separa los campos del registro
		 Registro1 = split(xRegistro1,"_")
		 Max1 = ubound(Registro1)
		 
		 ' analiza el registro
		 if len(Registro1(0))>"0" and  Registro1(1) <> "0" then
			Sql= ""
			Sql = Sql & " UPDATE PERS_SUBTIPOLOGIA SET "
			Sql = Sql & " INCROCIO = 'N', "
			Sql = Sql & " IND_STATUS = 1 "
			Sql = Sql & " WHERE "
			Sql = Sql & " ID_SUBTIPOLOGIA = " & Registro1(0)
			Sql = Sql & " AND DT_TMST = " &  ConvDateToDB(Registro1(1))
			Sql = Sql & " AND IND_STATUS = 0 "
			Sql = Sql & " AND ID_PERSONA = " & IDP
'PL-SQL * T-SQL  
'SQL = TransformPLSQLToTSQL (SQL) 
			CC.Execute Sql
		 end if
	next

'..Response.Write "Paso 1 Fin" & "<br>"
'..Response.End

'	'---------------------------------------------
'   ' Esto se uso hasta el 4/9/2006 
'	'---------------------------------------------
'	for y = 1 to  Request("PERS_ID_FIGPROF").count
'		Sql= ""
'		Sql = Sql & " UPDATE PERS_SUBTIPOLOGIA SET "
'		Sql = Sql & " INCROCIO = 'N', "
'		Sql = Sql & " IND_STATUS = 1 "
'		Sql = Sql & " WHERE "
'		Sql = Sql & " ID_SUBTIPOLOGIA = " & request("PERS_ID_FIGPROF") (y)
'		Sql = Sql & " AND DT_TMST = " &  ConvDateToDB(Request("PERS_DT_TMST")(y))
'		Sql = Sql & " AND IND_STATUS = 0 "
'		Sql = Sql & " AND ID_PERSONA = " & IDP
'		CC.Execute Sql
'	next		
	
	'------------------------------------------------------------------------------------
	' 3) Actualiza las figuras profesionales que han sido o siguen seleccionadas esta vez
	'	y que ya existian en la tabla de PERS_FIGPROF. Hace el Update
	'------------------------------------------------------------------------------------
	' Arma lista de figuras seleccionadas que ya estaban registradas en la base 
	IdFPCheck=""
	
	' Recorre item registrados, busca si sigue seleccionado y lo actualiza
	for i = 0 to MaxSelAntes
		xRegistro2 = FigurasProfEnTabla(i)
		 
		' separa los campos del registro
		Registro2 = split(xRegistro2, "_")
		Max2 = ubound(Registro2)
		 
		' analiza el registro en la tabla 
		' a) Es un registro valido
		if len(Registro2(0)) >"0" and  Registro2(1) <> "0" then
			' b) busca si el item sigue seleccionado
			for J = 1 to MaxSelAhora
				if len(FigurasProfSeleccionadas(j)) > 0 and FigurasProfSeleccionadas(j) <> "0" then
					if clng(Registro2(0)) = clng("0" & FigurasProfSelCodigos(j)) then
						' arma un string con la lista de elementos actualizados
						' Costruzione elenco figure professionali selezionate nelle checkbox
						if len(IdFPCheck) > clng(0) then
							IdFPCheck = IdFPCheck & ","
						end if
						IdFPCheck = IdFPCheck & trim(FigurasProfSelCodigos(j))
						
						'-------------------------------------------
						' actualiza el dato en archivo
						' de items seleccinados que estaban registrados en la tabla
						'-------------------------------------------
						'Gestione di eventuali occorrenze gi� registrate in PERS_FIGPROF 
						'Valorizzazione a IND_STATUS 0 e INCROCIO S di eventuali candidature preesistenti nella pagina precedente
						'Per mantenere per le candidature eventuale grado di prossimit�			
						Sql= ""
						Sql = Sql & " UPDATE PERS_SUBTIPOLOGIA SET "
						Sql = Sql & " INCROCIO = 'S', "
						Sql = Sql & " IND_STATUS = 0, "
						Sql = Sql & " DT_TMST= " & convdatetodb(Now())
						Sql = Sql & " WHERE "
						Sql = Sql & " ID_SUBTIPOLOGIA = " & Registro2(0)
						Sql = Sql & " AND DT_TMST = " &  ConvDateToDB(Registro2(1))
						Sql = Sql & " AND IND_STATUS = 1 "
						Sql = Sql & " AND ID_PERSONA = " & IDP
'PL-SQL * T-SQL  
'SQL = TransformPLSQLToTSQL (SQL) 
						CC.Execute Sql
					
						' ya encontre lo que buscaba, salgo de este for para ejecutar el de nivel superior
						exit for
					end if
				end if
			next
		end if 
	next	

'..Response.Write "Paso 2 Fin" & "<br>"
'..Response.End
			
'	'------------------------------------------------------------------------------------
'	' Esto se uso hasta el 4/9/2006
'	'------------------------------------------------------------------------------------'
'	IdFPCheck=""
'
'	'Costruzione elenco figure professionali selezionate nelle checkbox
'		
'	if Request("ID_FIGPROF").count <> 0 and Request("PERS_ID_FIGPROF").count <> 0  then 
'		for x = 1 to Request("ID_FIGPROF").count
'			IdFPCheck = IdFPCheck & Request("ID_FIGPROF") (x) & "," 
'		next
'		if 	trim(IdFPCheck) <> "" then
'			L = len(IdFPCheck)
'			IdFPCheck = left(IdFPCheck,L-1)
'		end if
'
'		'Gestione di eventuali occorrenze gi� registrate in PERS_FIGPROF 
'		'Valorizzazione a IND_STATUS 0 e INCROCIO S di eventuali candidature preesistenti nella pagina precedente
'		'Per mantenere per le candidature eventuale grado di prossimit�			
'			
'		for y = 1 to  Request("PERS_ID_FIGPROF").count
'			Sql= ""
'			Sql = Sql & " UPDATE PERS_SUBTIPOLOGIA SET "
'			Sql = Sql & " INCROCIO = 'S', "
'			Sql = Sql & " IND_STATUS = 0, "
'			Sql = Sql & " DT_TMST= " & convdatetodb(Now())
'			Sql = Sql & " WHERE "
'			Sql = Sql & " ID_SUBTIPOLOGIA = " & request("PERS_ID_FIGPROF") (y)
'			Sql = Sql & " AND DT_TMST = " &  ConvDateToDB(Request("PERS_DT_TMST")(y))						
'			Sql = Sql & " AND IND_STATUS = 1 "
'			Sql = Sql & " AND ID_PERSONA = " & IDP
'			Sql = Sql & " AND ID_SUBTIPOLOGIA IN (" & IdFPCheck  & ")"
'			CC.Execute Sql
'		next		
'	end if	

'----------------------------------------------------------------------------------------------  
'##################### INSERIMENTO FIGURE PROFESSIONALI SELEZIONATE SINGOLARMENTE
' 4) Inserta las figuras nuevas 
'---------------------------------------------------------------------------------------------- 
IF request("CandAll") <> "SI" then
	'--------------------------------------------------------------
	' REcorre los items seleccionados, descuenta los existentes en archivo(actualizados en el paso anterior)
	' y agrega los nuevos
	'--------------------------------------------------------------
	for i = 1 to MaxSelAhora
		if len(FigurasProfSeleccionadas(i)) > 0 and FigurasProfSeleccionadas(i) <> "0" then
			if clng(FigurasProfSeleccionadas(i)) > clng(0) then
				' Ricerca se l'occorrenza relativa alla Figura Professionale demarcata � gia presente in elenco
				' con la corretta valorizzazione
				Sql= "" 
				Sql= "SELECT ID_SUBTIPOLOGIA "
				Sql= Sql & "FROM PERS_SUBTIPOLOGIA "
				Sql= Sql & "WHERE ID_SUBTIPOLOGIA=" &  FigurasProfSelCodigos(i)
				Sql= sql & " AND ID_PERSONA=" & IDP
				Sql= sql & " AND IND_STATUS = 0"
				Sql= sql & " AND INCROCIO = 'S' "

'PL-SQL * T-SQL  
'SQL = TransformPLSQLToTSQL (SQL) 
				set RsIdFigprof = CC.Execute(sql)
				if  RsIdFigprof.eof THEN
					'Quando non registrata:
					'Inserimento ID della Figura Professionale 
					SqlFigProf=""
					SqlFigProf= SqlFigProf &  "INSERT INTO "
					SqlFigProf= SqlFigProf &  " PERS_SUBTIPOLOGIA (ID_PERSONA, ID_SUBTIPOLOGIA, GRADO_SUBTIPOLOGIA_PERS, DT_TMST, ID_FASE, INCROCIO, IND_STATUS) "
					SqlFigProf= SqlFigProf &  " VALUES "
					SqlFigProf= SqlFigProf &  " (" & IDP & "," & FigurasProfSelCodigos(i) & ",0," & OraDate & ",0,'S','0')"
'PL-SQL * T-SQL  
'SQLFIGPROF = TransformPLSQLToTSQL (SQLFIGPROF) 
					CC.Execute SqlFigProf	
															
					'vData= Request.Form("txtoggi")
					'chiave =  Request("ID_FIGPROF") (x) &  "|" & sIdpers 		
					'errore = EseguiNoCTrace(UCase(Mid(Session("progetto"),2)), chiave, "PERS_FIGPROF", Session("idutente"), sIdpers, "P", "INS", SqlFigProf, 1, vData, cc)						
				end if 
				RsIdFigprof.close	
				set RsIdFigprof = nothing
			end if
		end if
	NEXT
end if


'..Response.Write "Paso 3 Fin" & "<br>"
'..Response.End

'	'------------------------------------------------------------------------------------
'	' Esto se uso hasta el 4/9/2006
'	'------------------------------------------------------------------------------------'
'	IF  request("CandAll") <> "SI" then
'				FOR x = 1 to Request("ID_FIGPROF").count
'				
'				'Ricerca se l'occorrenza relativa alla Figura Professionale demarcata � gia presente in elenco
'				' con la corretta valorizzazione
'						Sql= "" 
'						Sql= "SELECT ID_SUBTIPOLOGIA "
'						Sql= Sql & "FROM PERS_SUBTIPOLOGIA "
'						Sql= Sql & "WHERE ID_SUBTIPOLOGIA=" &  Request("ID_FIGPROF")(x)
'						Sql= sql & " AND ID_PERSONA=" & IDP
'						Sql= sql & " AND IND_STATUS = 0"
'						Sql= sql & " AND INCROCIO = 'S' "
'
'						set RsIdFigprof=CC.Execute(sql)
'					
'							if   RsIdFigprof.eof THEN
'								'Quando non registrata:
'								'Inserimento ID della Figura Professionale 
'									SqlFigProf=""
'									SqlFigProf= SqlFigProf &  "INSERT INTO "
'									SqlFigProf= SqlFigProf &  " PERS_SUBTIPOLOGIA (ID_PERSONA,ID_SUBTIPOLOGIA,GRADO_SUBTIPOLOGIA_PERS,DT_TMST,ID_FASE,INCROCIO,IND_STATUS) "
'									SqlFigProf= SqlFigProf &  " VALUES "
'									SqlFigProf= SqlFigProf &  " (" & IDP & "," & Request("ID_FIGPROF") (x) & ",0," & OraDate & ",0,'S','0')"
'									CC.Execute SqlFigProf	
'	
'										
'	'vData= Request.Form("txtoggi")
'	'chiave =  Request("ID_FIGPROF") (x) &  "|" & sIdpers 		
'	'errore = EseguiNoCTrace(UCase(Mid(Session("progetto"),2)), chiave, "PERS_FIGPROF", Session("idutente"), sIdpers, "P", "INS", SqlFigProf, 1, vData, cc)						
'
'							end if 
'
'	
'							RsIdFigprof.close	
'							set RsIdFigprof=nothing
'							
'								
'				NEXT
'				
'	
'
'		END IF				
				

'------------------------------------------------------------------------------------------------
' ############################# CANDIDATURA PER TUTTA L'AREA
' 5) Agrega todas las figuras profesionales de una area especifica
'------------------------------------------------------------------------------------------------
IF  request("InputPage") = "AreaProf" and request("CandAll") = "SI"  then  			
	'Gestione di eventuali occorrenze gi� registrate in PERS_FIGPROF 
	'Valorizzazione a IND_STATUS 0 e INCROCIO S di eventuali candidature preesistenti nella pagina precedente
	'Per mantenere per le candidature eventuale grado di prossimit�		
	Sql= ""
	Sql = Sql & " UPDATE PERS_SUBTIPOLOGIA SET "
	Sql = Sql & " INCROCIO = 'S', "
	Sql = Sql & " IND_STATUS = 0,"
	Sql = Sql & " DT_TMST= " & convdatetodb(Now())
	Sql = Sql & " WHERE "
	Sql = Sql & "  IND_STATUS = 1 "
	Sql = Sql & " AND ID_PERSONA = " & IDP
	Sql = Sql & " AND ID_SUBTIPOLOGIA IN (" 
	Sql = Sql & " SELECT ID_SUBTIPOLOGIA "
	Sql = Sql & " FROM AREA_CORSO_SUB"
	Sql = Sql & " WHERE "
	Sql = Sql & " AREA_CORSO_SUB.ID_AREA = " & Request("ID_AREAPROF") & ")"
	'Response.End 
'PL-SQL * T-SQL  
'SQL = TransformPLSQLToTSQL (SQL) 
	CC.Execute Sql

	' Selezione delle figure professionali associate alle Aree Professionali 
	' non gi� registrate,attive e candidtate in PERS_FIGPROF

	Sql = ""
	Sql = " SELECT AREA_CORSO_SUB.ID_SUBAREA"
	Sql = Sql &  " FROM AREA_CORSO_SUB "
	Sql = Sql &  " WHERE "
	Sql = Sql &  " AREA_CORSO_SUB.ID_AREA = " & request("ID_AREAPROF") 
	Sql = Sql &  " AND  AREA_CORSO_SUB.ID_SUBAREA "
	Sql = Sql &  " NOT IN (SELECT PERS_SUBTIPOLOGIA.ID_SUBTIPOLOGIA FROM PERS_SUBTIPOLOGIA "
	Sql = Sql &  " WHERE ID_PERSONA = " & IDP
	Sql = Sql &  " AND IND_STATUS = 0 "
	Sql = Sql &  " AND INCROCIO = 'S')"

	Set RsFigProf = Server.CreateObject("ADODB.Recordset")
						
	RsFigProf.CursorLocation = 3
	RsFigProf.CursorType = 3	
	RsFigProf.ActiveConnection = CC
'PL-SQL * T-SQL  
'SQL = TransformPLSQLToTSQL (SQL) 
	RsFigProf.Open Sql
	
	if not RsFigProf.eof then 
		IdFigProf = RsFigProf.getrows()
		' Salvataggio figure professionali associate all'area per cui � satta posta la candidatura    
		for x = 0 to  RsFigProf.RecordCount -1
			SqlFigProf=""
			SqlFigProf= SqlFigProf &  "INSERT INTO "
			SqlFigProf= SqlFigProf &  " PERS_SUBTIPOLOGIA (ID_PERSONA, ID_SUBTIPOLOGIA, GRADO_SUBTIPOLOGIA_PERS, DT_TMST, ID_FASE, INCROCIO, IND_STATUS) "
			SqlFigProf= SqlFigProf &  " VALUES "
			SqlFigProf= SqlFigProf &  " (" & IDP & "," & IdFigProf(0,x)  & ",0," & OraDate & ",0,'S','0')"
'PL-SQL * T-SQL  
'SQLFIGPROF = TransformPLSQLToTSQL (SQLFIGPROF) 
			CC.Execute SqlFigProf
		next   
	end if	
	RsFigProf.Close
	set IdFigProf = nothing
	set RsFigProf = nothing   
END IF 

'Response.Write sql

'------------------------------------------------------------------------------------------------
'######################################### CONTROLLO ED EVENTUALE INSERIMENTO CANDIDATURA PER AREA PROFESSIONALE 
' Se l'utente ha seguito un percorso per Area Professionale			
'------------------------------------------------------------------------------------------------
IF request("ID_AREAPROF") <> "" then
	Sql = ""
	Sql= "SELECT ID_TIPOLOGIA FROM PERS_TIPOLOGIA WHERE ID_TIPOLOGIA=" & Request("ID_AREAPROF") 
	Sql= Sql & "AND ID_PERSONA=" & IDP 
	Sql= Sql & " AND INCROCIO = 'S' "
	Sql= Sql & " AND IND_STATUS = 0"
			
	'Response.Write sql 

'PL-SQL * T-SQL  
'SQL = TransformPLSQLToTSQL (SQL) 
	set RsArea=CC.Execute(sql)	
	if  RsArea.eof then
		'se non la candidatura all'area professionale relativa alla figura professionale
		'inserisco la candidatura all'area.
		SqlAreProf=""
		SqlAreProf= SqlAreProf &  "INSERT INTO "
		SqlAreProf= SqlAreProf &  " PERS_TIPOLOGIA (ID_PERSONA,ID_TIPOLOGIA,GRADO_TIPOLOGIA_PERS,DT_TMST,ID_FASE,INCROCIO,IND_STATUS) "
		SqlAreProf= SqlAreProf &  " VALUES "
		SqlAreProf= SqlAreProf &  " (" & IDP & "," & Request("ID_AREAPROF") & ",0," & OraDate & ",0,'S','0')"

'PL-SQL * T-SQL  
'SQLAREPROF = TransformPLSQLToTSQL (SQLAREPROF) 
		CC.Execute (SqlAreProf)

		RsArea.close
		set RsArea=nothing
	end if
ELSE 
	' Se l'utente non ha seguito un percorso per Area Professionale (ricerca per testo o per settore)
	' usado desde el 4/9/2006
	IF request("InputPage") = "FigProf"   and  request("FigProfCandidati") = "" THEN 
		for i = 1 to MaxSelAhora
			if len(FigurasProfSeleccionadas(i)) > 0 and FigurasProfSeleccionadas(i) <> "0" then
				if clng(FigurasProfSeleccionadas(i)) > clng(0) then
					Sql = " SELECT DISTINCT(ID_AREA) "
					Sql = Sql & "FROM AREA_CORSO_SUB "
					Sql = Sql & " WHERE "
					Sql = Sql & " ID_SUBAREA = " & FigurasProfSelCodigos(i)
					Sql = Sql & "  AND   "
					Sql = Sql & " ID_AREA "
					Sql = Sql & " NOT IN  "
					Sql = Sql & "(SELECT ID_TIPOLOGIA AS ID_AREA "
					Sql = Sql & " FROM PERS_TIPOLOGIA "
					Sql = Sql & " WHERE "
					Sql = Sql & " IND_STATUS= 0 AND "
					Sql = Sql & " INCROCIO = 'S' AND "
					Sql = Sql & " ID_PERSONA = " & IDP & ")"

'PL-SQL * T-SQL  
'SQL = TransformPLSQLToTSQL (SQL) 
					SET rsArea=CC.execute(SQL)	
						
					if  not RsArea.eof then
						IdArea = RsArea(0) 
						'se la candidatura all'area professionale relativa alla figura professionale
						'inserisco la candidatura all'area.
						SqlAreProf=""
						SqlAreProf= SqlAreProf &  "INSERT INTO "
						SqlAreProf= SqlAreProf &  " PERS_TIPOLOGIA (ID_PERSONA,ID_TIPOLOGIA,GRADO_TIPOLOGIA_PERS,DT_TMST,ID_FASE,INCROCIO,IND_STATUS) "
						SqlAreProf= SqlAreProf &  " VALUES "
						SqlAreProf= SqlAreProf &  " (" & IDP & "," & IdArea & ",0," & OraDate & ",0,'S','0')"

'PL-SQL * T-SQL  
'SQLAREPROF = TransformPLSQLToTSQL (SQLAREPROF) 
						CC.Execute (SqlAreProf)
					end if
					RsArea.close	
					set RsArea=nothing
				end if
			end if
		next
	end if
END IF

'	'---------------------------------------------
'   ' Esto se uso hasta el 4/9/2006 
'	'---------------------------------------------
'	' Se l'utente non ha seguito un percorso per Area Professionale (ricerca per testo o per settore)
'	IF request("InputPage") = "FigProf"   and  request("FigProfCandidati") = "" THEN 
'		FOR x = 1 to Request("ID_FIGPROF").count
'			Sql = " SELECT DISTINCT(ID_AREA) "
'			Sql = Sql & "FROM AREA_CORSO_SUB "
'			Sql = Sql & " WHERE "
'			Sql = Sql & " ID_SUBAREA = " & Request("ID_FIGPROF") (x)
'			Sql = Sql & "  AND   "
'			Sql = Sql & " ID_AREA "
'			Sql = Sql & " NOT IN  "
'			Sql = Sql & "(SELECT ID_TIPOLOGIA AS ID_AREA "
'			Sql = Sql & " FROM PERS_TIPOLOGIA "
'			Sql = Sql & " WHERE "
'			Sql = Sql & " IND_STATUS= 0 AND "
'			Sql = Sql & " INCROCIO = 'S' AND "
'			Sql = Sql & " ID_PERSONA = " & IDP & ")"
'
'			SET rsArea=CC.execute(SQL)	
'			if  not RsArea.eof then
'				IdArea = RsArea(0) 
'				'se la candidatura all'area professionale relativa alla figura professionale
'				'inserisco la candidatura all'area.
'															
'				SqlAreProf=""
'				SqlAreProf= SqlAreProf &  "INSERT INTO "
'				SqlAreProf= SqlAreProf &  " PERS_TIPOLOGIA (ID_PERSONA,ID_TIPOLOGIA,GRADO_TIPOLOGIA_PERS,DT_TMST,ID_FASE,INCROCIO,IND_STATUS) "
'				SqlAreProf= SqlAreProf &  " VALUES "
'				SqlAreProf= SqlAreProf &  " (" & IDP & "," & IdArea & ",0," & OraDate & ",0,'S','0')"
'														
'				CC.Execute (SqlAreProf)
'			end if
'			RsArea.close	
'			set RsArea=nothing
'		next
'	END IF
'END IF


'------------------------------------------------------------------------------------------------
'###################### Eliminaciones VArias
'------------------------------------------------------------------------------------------------
	' ELIMINAZIONE PERS_FIGPROF OCCORRENZE non attive (IND_STATUS = 1)
	Sql = ""
	Sql = " DELETE PERS_SUBTIPOLOGIA WHERE "
	Sql = Sql &  " IND_STATUS = 1"
	Sql = Sql &  " AND"
	Sql = Sql &  " INCROCIO = 'N'"
	Sql = Sql &  " AND"
	Sql = Sql &  " ID_PERSONA =" & IDP
'PL-SQL * T-SQL  
'SQL = TransformPLSQLToTSQL (SQL) 
	cc.execute sql


	' ELIMINAZIONE PERS_AREAPROF OCCORRENZE non attive (IND_STATUS = 1)	
	Sql = ""
	Sql = " DELETE PERS_TIPOLOGIA WHERE "
	Sql = Sql &  " IND_STATUS = 1"
	Sql = Sql &  " AND"
	Sql = Sql &  " INCROCIO = 'N'"
	Sql = Sql &  " AND"
	Sql = Sql &  " ID_PERSONA =" & IDP
'PL-SQL * T-SQL  
'SQL = TransformPLSQLToTSQL (SQL) 
	cc.execute sql



'-----------------------------------------------------------------
' ***************	Resultados y salida		**********************
'-----------------------------------------------------------------
url = Request("BackPage")


'predisposizione ESEGUINOTRACE 
' al momento la funzione non � attivata

sValInf = ValInfPers(sIdpers,cc)
			Errore = sValInf	
			
if errore <> "0" then
	CC.RollbackTrans
	%>			
	<br>
	<table border="0" cellspacing="1" cellpadding="1" width="500">
		<tr align="middle">
			<td class="tbltext3"> Modificaci&oacute;n de Datos no efectuada<br>
						<%Response.Write (Errore)%> 
			</td>
		</tr>
	</table>
	<br>				
 
	<table border="0" cellpadding="0" cellspacing="1" width="500">
		<tr>
			<td align="middle" colspan="2" width="60%"><b>
				<a HREF="javascript:history.back()"><img SRC="<%=Session("Progetto")%>/images/indietro.gif" border="0">
			</td>
		</tr>
	</table>	
	<!--#include virtual = "/include/CloseConn.asp"-->
	<!-- #include Virtual="/strutt_coda2.asp" -->
	<% 

else 		
	CC.CommitTrans

	' adonde vuelve
	if request("InputPage") = "AreaProf" then 
		URL = "AreeProfCandidate.asp"
	else 
		URL = "ResultRicerca.asp"
	end if

	%>
	<!--#include virtual = "/include/CloseConn.asp"-->

	<form name="SalvaCand" Method="post" action="<%=URL%>">
		<input type="hidden" name="IdPers" value="<%=request("IdPers")%>">
		<input type="hidden" name="IND_FASE" value="<%=request("IND_FASE")%>">
		<input type="hidden" name="LivMenu" value="<%=request("LivMenu")%>">
		<input type="hidden" name="FigProfCandidati" value="True">
		<input type="hidden" name="Prest" value="<%=request("Prest")%>">
		<input type="hidden" name="PrestSeleccionada" value="<%=request("PrestSeleccionada")%>">
	</form>

	<script>
		<!--
		alert("Modificaci�n correctamente efectuada")
				document.forms.SalvaCand.submit();
		//-->
	</script>
<%
end if
%>
