<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #INCLUDE FILE="../Utils.asp" -->


<%
OraDate = ConvDateToDB(Now())
Set Conn = server.CreateObject ("ADODB.Connection")
Conn.open strConn
Conn.BeginTrans

LIV = request("LivMenu")

	If  LIV <> "SPI_2" then
		FASI = "(0,3)"
	End If
		
			
		' ##### AREE PROFESSIONALI			
		' Tutte le aree presenti sulla schermata 'Modifica Candidatura' vengono impostate con INCROCIO = N
		' C'� un solo execute perch� i dati sui quali si deve fare update vengono individuati direttamente nella
		' clausola di WHERE
		
		Sql= ""
		Sql = Sql & " UPDATE PERS_TIPOLOGIA SET "
		Sql = Sql & " INCROCIO = 'N' "
		Sql = Sql & " WHERE "
		for x = 1 to request("ID_AREAPROF").count
			DT_AREAPROF = ConvDateToDB(request("DT_AREAPROF")(x))
			Sql = Sql & " (ID_TIPOLOGIA = " & request("ID_AREAPROF")(x) & " AND "
			Sql = Sql & " DT_TMST = " & DT_AREAPROF & ") OR "
		next
		Sql = Sql & " ID_PERSONA < 0 AND " '#### Mi serve una condizione sempre false perch� il rigo di sopra mi termina con un Or 
		Sql = Sql & " ID_PERSONA = " & IDP
			
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
		Conn.Execute Sql
						

		' valorizzo a "S" tutte le aree prof.  selezionate nella checkbox
		for x = 1 to Request("ID_AREAPROF_CAND").count
		
		For j = 1 to Request("ID_AREAPROF").count
			if request("ID_AREAPROF")(j) = request("ID_AREAPROF_CAND")(x) then
				x_DT = j
			end if
		next
		DT_AREAPROF = ConvDateToDB(request("DT_AREAPROF")(x_DT))
		
			Sql= ""
			Sql = Sql & " UPDATE PERS_TIPOLOGIA SET "
			Sql = Sql & " INCROCIO = 'S', "
			Sql = Sql & " IND_STATUS ='0',"
			Sql = Sql & " DT_TMST = " & Oradate
			Sql = Sql & " WHERE ID_PERSONA = " & IDP & " AND "
			Sql = Sql & " ID_TIPOLOGIA = " & Request("ID_AREAPROF_CAND")(x) & " AND "
			Sql = Sql & " DT_TMST = " & DT_AREAPROF
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
			Conn.Execute Sql	
		next
		
		
		' ##### FIGURE PROFESSIONALI
		' Tutte le FIGURE PROFESSIONALI presenti sulla schermata 'Modifica Candidatura' vengono impostate con INCROCIO = N
		' C'� un solo execute perch� i dati sui quali si deve fare update vengono individuati direttamente nella
		' clausola di WHERE
		
		Sql= ""
		Sql = Sql & " UPDATE PERS_SUBTIPOLOGIA SET "
		Sql = Sql & " INCROCIO = 'N' "
		Sql = Sql & " WHERE "
		for x = 1 to request("ID_FIGPROF").count
			DT_FIGPROF = ConvDateToDB(request("DT_FIGPROF")(x))
			'DT_FIGPROF = "TO_DATE('" & day(request("DT_FIGPROF")(x)) & "/" & month(request("DT_FIGPROF")(x)) & "/" & year(request("DT_FIGPROF")(x)) & " " & left(TimeValue(request("DT_FIGPROF")(x)),len(TimeValue(request("DT_FIGPROF")(x)))-2) & "','DD/MM/YYYY HH:MI:SS')"
			Sql = Sql & " (ID_SUBTIPOLOGIA = " & request("ID_FIGPROF")(x) & " AND "
			Sql = Sql & " DT_TMST = " & DT_FIGPROF & ") OR "
		next
		Sql = Sql & " ID_PERSONA < 0 AND " '#### Mi serve una condizione sempre false perch� il rigo di sopra mi termina con un Or 
		Sql = Sql & " ID_PERSONA = " & IDP			
		
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
		Conn.Execute Sql
		

		' valorizzo a "S" tutte le figure prof.  selezionate nella checkbox
		for x = 1 to Request("ID_FIGPROF_CAND").count
		
		For j = 1 to Request("ID_FIGPROF").count
			if request("ID_FIGPROF")(j) = request("ID_FIGPROF_CAND")(x) then
				x_DT = j
			end if
		next
		
		DT_FIGPROF = ConvDateToDB(request("DT_FIGPROF")(x_DT))
		'DT_FIGPROF = "TO_DATE('" & day(request("DT_FIGPROF")(x_DT)) & "/" & month(request("DT_FIGPROF")(x_DT)) & "/" & year(request("DT_FIGPROF")(x_DT)) & " " & left(TimeValue(request("DT_FIGPROF")(x_DT)),len(TimeValue(request("DT_FIGPROF")(x_DT)))-2) & "','DD/MM/YYYY HH:MI:SS')"
		
			Sql= ""
			Sql = Sql & " UPDATE PERS_SUBTIPOLOGIA SET "
			Sql = Sql & " INCROCIO = 'S', "
			Sql = Sql & " IND_STATUS ='0',"
			Sql = Sql & " DT_TMST = " & Oradate
			Sql = Sql & " WHERE ID_PERSONA = " & IDP & " AND "
			Sql = Sql & " ID_SUBTIPOLOGIA = " & Request("ID_FIGPROF_CAND")(x) & " AND "
			Sql = Sql & " DT_TMST = " & DT_FIGPROF
				
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
			Conn.Execute Sql	
		next

	LogTrace ucase(mid(session("progetto"),2)),IDP, "P",IDP,"PERS_TIPOLOGIA", "" &IDP & "", "MOD"
	LogTrace ucase(mid(session("progetto"),2)),IDP, "P",IDP,"PERS_SUBTIPOLOGIA", "" &IDP & "", "MOD"
 							
Conn.CommitTrans
Conn.Close
Set conn = Nothing

If LIV = "SECONDO" then

%>
	<script>
		<!--
			goToPage("<%=Request("back")%>?LIV=<%=LIV%>")
		//-->
	</script>
<%else %>
	<script>
		<!--
			goToPage("<%=Request("back")%>")
		//-->
	</script>
<%End if %> 
