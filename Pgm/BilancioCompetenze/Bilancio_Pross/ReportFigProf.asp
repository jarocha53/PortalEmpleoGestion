<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #INCLUDE FILE="../Utils.asp" -->
<html>
<head>
<title>Reporte del Balance de Competencias</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="<%=Session("Progetto")%>/fogliostile.css" type="text/css">
<script LANGUAGE="JavaScript">
	function stp()
	{
		bb=(document.layers)?document.layers['aa']:(document.getElementById)?document.getElementById('aa').style:document.all['aa'].style;
		gg=(document.layers)?document.layers['ff']:(document.getElementById)?document.getElementById('ff').style:document.all['ff'].style;
		bb.visibility="hidden"
		gg.visibility="hidden"
		self.print();
		bb.visibility="visible"
		gg.visibility="visible"

	}
</script>
</head>
<body bgcolor="#ffffff">
<%

HelpPage = Request("HelpPage")

	select case HelpPage
		case "PROSSIMITA"
		HelpPath = "Bilancio_Pross/ReportFigProf.htm"
		case "CANDIDATURE"
		HelpPath = "Candidature/ReportFigProf.htm"
	end Select


sIdPers= request("IdPers")
Set Conn = server.CreateObject ("ADODB.Connection")
Conn.open strConn

	Sql = ""
	Sql = Sql & "SELECT "
	Sql = Sql & "COMPETENZE.DENOMINAZIONE, COMPETENZE.ID_COMPETENZA, COMPET_FP.GRADO_FP_COMP "
	Sql = Sql & "FROM "
	Sql = Sql & "COMPETENZE, COMPET_FP "
	Sql = Sql & "WHERE "
	Sql = Sql & "COMPETENZE.ID_COMPETENZA = COMPET_FP.ID_COMPETENZA "
	Sql = Sql & "AND "
	Sql = Sql & "COMPET_FP.ID_FIGPROF = " & request("ID_FIGPROF")

'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
	Set Rs = Conn.Execute (Sql)
	if not rs.eof then COMPETENZE = Rs.getrows()

	Sql = ""
	Sql = Sql & "SELECT "
	Sql = Sql & "CONOSCENZE.DENOMINAZIONE, COMPET_CONOSC.ID_COMPETENZA, COMPET_CONOSC.GRADO_COMP_CON, "
	Sql = Sql & "	(SELECT PERS_CONOSC.COD_GRADO_CONOSC FROM PERS_CONOSC "
	Sql = Sql & "	WHERE "
	Sql = Sql & "	PERS_CONOSC.ID_CONOSCENZA = CONOSCENZE.ID_CONOSCENZA AND "
	Sql = Sql & "	PERS_CONOSC.ID_PERSONA = " & sIdPers & ") GRADO_PERS "
	Sql = Sql & "FROM "
	Sql = Sql & "CONOSCENZE, COMPET_CONOSC, COMPET_FP "
	Sql = Sql & "WHERE "
	Sql = Sql & "CONOSCENZE.ID_CONOSCENZA = COMPET_CONOSC.ID_CONOSCENZA "
	Sql = Sql & "AND "
	Sql = Sql & "COMPET_CONOSC.ID_COMPETENZA = COMPET_FP.ID_COMPETENZA "
	Sql = Sql & "AND "
	Sql = Sql & "COMPET_FP.ID_FIGPROF = " & request("ID_FIGPROF")

'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
	Set Rs = Conn.Execute (Sql)
	if not rs.eof then CONOSCENZE = Rs.getrows()

	Sql = ""
	Sql = Sql & "SELECT "
	Sql = Sql & "CAPACITA.DENOMINAZIONE, COMPET_CAPAC.ID_COMPETENZA, COMPET_CAPAC.GRADO_COMP_CAP, "
	Sql = Sql & "	(SELECT PERS_CAPAC.GRADO_CAPACITA FROM PERS_CAPAC "
	Sql = Sql & "	WHERE "
	Sql = Sql & "	PERS_CAPAC.ID_CAPACITA = CAPACITA.ID_CAPACITA AND "
	Sql = Sql & "	PERS_CAPAC.ID_PERSONA = " & sIdPers & ") GRADO_PERS "
	Sql = Sql & "FROM "
	Sql = Sql & "CAPACITA, COMPET_CAPAC, COMPET_FP "
	Sql = Sql & "WHERE "
	Sql = Sql & "CAPACITA.ID_CAPACITA = COMPET_CAPAC.ID_CAPACITA "
	Sql = Sql & "AND "
	Sql = Sql & "COMPET_CAPAC.ID_COMPETENZA = COMPET_FP.ID_COMPETENZA "
	Sql = Sql & "AND "
	Sql = Sql & "COMPET_FP.ID_FIGPROF = " & request("ID_FIGPROF")

'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
	Set Rs = Conn.Execute (Sql)
	if not rs.eof then CAPACITA = Rs.getrows()

	Sql = ""
	Sql = Sql & "SELECT "
	Sql = Sql & "SETTORI.DENOMINAZIONE, SETTORI.ID_SETTORE, "
	Sql = Sql & "VALIDAZIONE.ID_VALID, VALIDAZIONE.FL_VALID, VALIDAZIONE.ID_PERS_INS "
	Sql = Sql & "FROM "
	Sql = Sql & "SETTORI, SETTORI_FIGPROF, VALIDAZIONE "
	Sql = Sql & "WHERE "
	Sql = Sql & "SETTORI_FIGPROF.ID_VALID = VALIDAZIONE.ID_VALID "
	Sql = Sql & "AND "
	Sql = Sql & "SETTORI.ID_SETTORE = SETTORI_FIGPROF.ID_SETTORE "
	Sql = Sql & "AND "
	Sql = Sql & "SETTORI_FIGPROF.ID_FIGPROF = " & request("ID_FIGPROF")

'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
	Set Rs = Conn.Execute (Sql)
	if not rs.eof then SETTORI = Rs.getrows()


	Sql = ""
	Sql = Sql & "SELECT "
	Sql = Sql & "COMPORTAMENTI.DENOMINAZIONE, COMPORTAMENTI.ID_COMPORTAMENTO, "
	Sql = Sql & "VALIDAZIONE.ID_VALID, VALIDAZIONE.FL_VALID, VALIDAZIONE.ID_PERS_INS, COMPOR_FP.GRADO_FP_COMPOR, "
	Sql = Sql & "	(SELECT PERS_COMPOR.GRADO_PERS_COMPOR FROM PERS_COMPOR "
	Sql = Sql & "	WHERE "
	Sql = Sql & "	PERS_COMPOR.ID_COMPORTAMENTO = COMPORTAMENTI.ID_COMPORTAMENTO AND "
	Sql = Sql & "	PERS_COMPOR.ID_PERSONA = " & sIdPers & ") GRADO_PERS "
	Sql = Sql & "FROM "
	Sql = Sql & "COMPORTAMENTI, COMPOR_FP, VALIDAZIONE "
	Sql = Sql & "WHERE "
	Sql = Sql & "COMPOR_FP.ID_VALID = VALIDAZIONE.ID_VALID "
	Sql = Sql & "AND "
	Sql = Sql & "COMPORTAMENTI.ID_COMPORTAMENTO = COMPOR_FP.ID_COMPORTAMENTO "
	Sql = Sql & "AND "
	Sql = Sql & "COMPOR_FP.ID_FIGPROF = " & request("ID_FIGPROF")

'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
	Set Rs = Conn.Execute (Sql)
	if not rs.eof then COMPORT = Rs.getrows()

set Rs = Nothing
Conn.Close
Set conn = Nothing

%>


<p align="left">
<center>
<div id="ff">
<input type="button" value="Imprimir" OnClick="stp()" CLASS="My">&nbsp; &nbsp;
<input type="button" value="Cancelar" OnClick="top.close()" CLASS="My">
</div>
</center>
</p>
<center>
<table border="0" cellspacing="1" cellpadding="0" width="570">
<!--tr><td align="left" class="sfondocomm"><b><small><%	'if UCASE(request("VISANAG"))<> "NO" THEN 	'	Response.write Session("Nome") & " - " & Session("Cognome")	'end if %>&nbsp;</small></tr-->
<tr><td align="left" class="sfondocomm"><b><small>Puesto para el cual esta postulado:&nbsp;<%=UCASE(request("DENOMINAZIONE"))%> </b>
<a href="Javascript:Show_Help('<%=Session("Progetto")%>/HelpBdc/<%=HelpPath%>')">
<img src="<%=Session("Progetto")%>/images/Help.gif" border="0" align="right" alt="Help">
</a>
</td></tr>
</table>
</center>
<center>
<%
	if IsArray(SETTORI) then
	Response.Write "<table border=0 width='570' cellspacing=1 cellpadding=0 >"
		Response.write "<tr>"
		Response.Write "<td>"

			Response.Write "<table border=0  cellspacing=1 width='570'>"
			Response.Write "<tr class='sfondocomm'>"
				Response.Write "<td><b><small>Rama de Actividad</small></b></td>"
			Response.Write "</tr>"
				for I = lbound(SETTORI,2) to Ubound(SETTORI,2)
					Response.Write "<tr>"
						Response.Write "<td class='tbltext1'><small>" & SETTORI(0,I) & "<small></td>"
					Response.Write "</tr>"
				next
			Response.Write "</table>"
		Response.Write "</td>"
		Response.Write "</tr>"
	Response.Write "</table>"
	end if
%>
<br>
<center>
<%
	if IsArray(COMPETENZE) then
	Response.Write "<table border=0 width='570' cellspacing=1 cellpadding=0>"
	Response.write "<tr>"
	Response.Write "<td>"

	Response.Write "<table border=0 cellspacing=1 width='570'>"
	Response.Write "<tr class='sfondocomm'>"
		Response.Write "<td Colspan=2><b><small>Competencia<small></b></td>"
		Response.Write "<td NOWRAP width='73'><b><small>Poceso</small></b></td>"
	Response.Write "</tr>"
	for i = lbound(COMPETENZE,2) to Ubound(COMPETENZE,2)
		Response.Write "<tr>"
		Response.Write "<td class='tbltext1' colspan='3'><b>&nbsp;&nbsp;<small>" & COMPETENZE(0,I) & "<small></b></td>"
		Response.Write "<td class='tbltext1' align='center'><b>&nbsp;</b></td>"
		Response.Write "</tr>"

		Response.Write "<tr class='sfondocomm'>"
		Response.Write "<td WIDTH='12'>&nbsp;</td>"
		Response.Write "<td colspan=3><b><small>Conocimiento<small></b></td>"
		Response.Write "</tr>"
		if isarray(CONOSCENZE) THEN
			for jj = lbound(CONOSCENZE,2) to Ubound(CONOSCENZE,2)
			if int(0 & COMPETENZE(1,I)) = int(0 & CONOSCENZE(1,jj)) then
				Response.Write "<tr>"
					Response.Write "<td>&nbsp;</td>"
					Response.Write "<td class='tbltext1'><small>" & CONOSCENZE(0,jj) & "</small></td>"
					if Int(0 & CONOSCENZE(3,jj)) >= Int(0 & CONOSCENZE(2,jj)) then
						Immagine = "<img src='si.gif'>"
					else
						Immagine = "<img src='no.gif'>"
					end if
					Response.Write "<td class='tbltext1' align='center'><b>&nbsp;&nbsp;" & Immagine & "</b></td>"
				Response.Write "</tr>"
			end if
			next
		END IF

		Response.Write "<tr class='sfondocomm'>"
		Response.Write "<td WIDTH='30'>&nbsp;</td>"
		Response.Write "<td colspan=3><b><small>Desempe�o<small></b></td>"
		Response.Write "</tr>"
		IF ISARRAY(CAPACITA) THEN
			for jj = lbound(CAPACITA,2) to Ubound(CAPACITA,2)
			if int(0 & COMPETENZE(1,I)) = int(0 & CAPACITA(1,jj)) then
				Response.Write "<tr>"
					Response.Write "<td>&nbsp;</td>"
					Response.Write "<td class='tbltext1'><small>" & CAPACITA(0,jj) & "</small></td>"
					if Int(0 & CAPACITA(3,jj)) >= Int(0 & CAPACITA(2,jj))then

						Immagine = "<img src='si.gif'>"
					else

						Immagine = "<img src='no.gif'>"
					end if
					Response.Write "<td align='center'><b>&nbsp;&nbsp;" & Immagine & "</b></td>"
				Response.Write "</tr>"
			end if
			next
		END IF
	next
	Response.Write "</table>"
	end if
			Response.Write "</td>"
		Response.Write "</tr>"
	Response.Write "</table>"
%>
<br>
<%
	if IsArray(COMPORT) then
	Response.Write "<table border=0 width='570' cellspacing=1 cellpadding=0 style='font-family: Verdana; font-size: 10pt; color:#000000'>"
		Response.write "<tr>"

		
		Response.Write "<td>"

			Response.Write "<table border=0 cellspacing=1 width='570' style='font-size: 8pt; font-family: verdana'>"
		
			Response.Write "<tr class='sfondocomm'>"
		
		Response.Write "<td WIDTH='30'>&nbsp;</td>"			
				Response.Write "<td><b><small>Comportamiento</small></b></td>"
		
		Response.Write "<td align='center'><b><small>Poceso</small></b></td>"
			Response.Write "</tr>"
				for I = lbound(COMPORT,2) to Ubound(COMPORT,2)
					Response.Write "<tr>"
				Response.Write "<td>&nbsp;</td>"
						Response.Write "<td class='tbltext1'>&nbsp;&nbsp;<small>" & COMPORT(0,I) & "</small></td>"
						if Int(0 & COMPORT(6,I)) >= Int(0 & COMPORT(5,I)) then
							Immangine = "<img src='si.gif'>"
						else
							Immangine = "<img src='no.gif'>"
						End if
						Response.Write "<td class='tbltext1' align='center'><b>&nbsp;&nbsp;" & Immangine & "</b></td>"
					Response.Write "</tr>"
				next
				Response.Write "</table>"
		Response.Write "</td>"
		Response.Write "</tr>"
	Response.Write "</table>"
	end if

%>
</center>
<div id="aa">
<input type="button" value="Imprimir" OnClick="stp()" CLASS="My">&nbsp; &nbsp;
<input type="button" value="Cancelar" OnClick="top.close()" CLASS="My">
</div>

</body>
</html>
