<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #INCLUDE FILE="../Utils.asp" -->
<%


Set Conn = server.CreateObject ("ADODB.Connection")
Conn.open strConn

'OraDate = "TO_DATE('" & day(date) & "/" & month(Date) & "/" & year(Date) & " " & left(time,len(time & "")-2) & "','DD/MM/YYYY HH:MI:SS')"
OraDate = ConvDateToDB(Now())

'##### SALVA IL CASO CAPACITA'

if request("Tipo") = "CAPACITA" then
Conn.BeginTrans
for x = Int(request("Ini")) to Int(request("Limit"))
	LstCampi = "ID_PERSONA,GRADO_CAPACITA,ID_CAPACITA"

	LstVal = ""
	LstVal = LstVal &  IDP & "|"
	LstVal = LstVal &  request("Grado" & x) & "|"
	LstVal = LstVal & request("ID" & x)

	Sql = ""
	if request("exist" & x) = "True" then
		Sql = QryUpd("PERS_CAPAC",LstCampi,LstVal)
		Sql = Sql & " WHERE "
		Sql = Sql & "PERS_CAPAC.ID_PERSONA = " & IDP & " AND "
		Sql = Sql & "PERS_CAPAC.ID_CAPACITA = " & request("ID" & x)
	else
		Sql = QryIns("PERS_CAPAC",LstCampi,LstVal)
	end if

	if request("Grado" & x) <> "" then
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
		Conn.Execute Sql
	else
		Sql = ""
		Sql = Sql & "DELETE PERS_CAPAC "
		Sql = Sql & " WHERE "
		Sql = Sql & "PERS_CAPAC.ID_PERSONA = " & IDP & " AND "
		Sql = Sql & "PERS_CAPAC.ID_CAPACITA = " & request("ID" & x)

'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
		Conn.Execute Sql
	end if
next
Conn.CommitTrans
end if
'##### END CASO CAPACITA'


'##### SALVA IL CASO COMPORTAMENTI
if request("Tipo") = "COMPORTAMENTI" then
Conn.BeginTrans
for x = Int(request("Ini")) to Int(request("Limit"))
	LstCampi = "ID_PERSONA,GRADO_PERS_COMPOR,ID_COMPORTAMENTO"

	LstVal = ""
	LstVal = LstVal &  IDP & "|"
	LstVal = LstVal &  request("Grado" & x) & "|"
	LstVal = LstVal & request("ID" & x)

	Sql = ""
	if request("exist" & x) = "True" then
		Sql = QryUpd("PERS_COMPOR",LstCampi,LstVal)
		Sql = Sql & " WHERE "
		Sql = Sql & "PERS_COMPOR.ID_PERSONA = " & IDP & " AND "
		Sql = Sql & "PERS_COMPOR.ID_COMPORTAMENTO = " & request("ID" & x)
	else
		Sql = QryIns("PERS_COMPOR",LstCampi,LstVal)
	end if

	if request("Grado" & x) <> "" then
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
		Conn.Execute Sql
	else
		Sql = ""
		Sql = Sql & "DELETE PERS_COMPOR "
		Sql = Sql & " WHERE "
		Sql = Sql & "PERS_COMPOR.ID_PERSONA = " & IDP & " AND "
		Sql = Sql & "PERS_COMPOR.ID_COMPORTAMENTO = " & request("ID" & x)
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
		Conn.Execute Sql
	end if
next
Conn.CommitTrans
end if
'##### END CASO COMPORTAMENTI

'##### SALVA IL CASO CONOSCENZE
if request("Tipo") = "CONOSCENZE" then
Conn.BeginTrans
for x = Int(request("Ini")) to Int(request("Limit"))
	LstCampi = "ID_PERSONA,COD_GRADO_CONOSC,DT_TMST,ID_AREACONOSCENZA,ID_CONOSCENZA"

	LstVal = ""
	LstVal = LstVal & IDP & "|"
	LstVal = LstVal & request("Grado" & x) & "|"
	LstVal = LstVal & OraDate & "|"
	if request("ID_AREACONOSCENZA") <> "" then
		LstVal = LstVal & request("ID_AREACONOSCENZA") & "|"
	else
		LstVal = LstVal & "NULL" & "|"
	end if
	LstVal = LstVal & request("ID" & x)

	Sql = ""
	if request("exist" & x) = "True" then
		Sql = QryUpd("PERS_CONOSC",LstCampi,LstVal)
		Sql = Sql & " WHERE "
		Sql = Sql & "PERS_CONOSC.ID_PERSONA = " & IDP & " AND "
		Sql = Sql & "PERS_CONOSC.ID_CONOSCENZA = " & request("ID" & x)
	else
		Sql = QryIns("PERS_CONOSC",LstCampi,LstVal)
	end if

	if request("Grado" & x) <> "" then
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
		Conn.Execute Sql
	else
		Sql = ""
		Sql = Sql & "DELETE PERS_CONOSC "
		Sql = Sql & " WHERE "
		Sql = Sql & "PERS_CONOSC.ID_PERSONA = " & IDP & " AND "
		Sql = Sql & "PERS_CONOSC.ID_CONOSCENZA = " & request("ID" & x)
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
		Conn.Execute Sql
	end if
next
Conn.CommitTrans
end if
'##### END CASO CONOSCENZE

Conn.Close
Set conn = Nothing

URL = request("back")

if request("Avanti") <> "" then
	URL = request("DETTAGLI") & "?"
	URL = URL & "Limit=" & request("Limit") & "&"
	URL = URL & "ID_AREA=" & request("ID_AREA") & "&"
	URL = URL & "Tipo=" & request("Tipo")
End if

if request("Indietro") <> "" then
	URL = request("DETTAGLI") & "?"
	URL = URL & "Limit=" & Request("Ini")-10 & "&"
	URL = URL & "ID_AREA=" & request("ID_AREA") & "&"
	URL = URL & "Tipo=" & request("Tipo")
End if
%>
<SCRIPT>
<!--
	goToPage(URL)
//-->
</SCRIPT>

