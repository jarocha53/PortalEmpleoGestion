<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #INCLUDE FILE="../Utils.asp" -->
<%


Set Conn = server.CreateObject ("ADODB.Connection")
Conn.open strConn

	Sql = ""
	Sql = Sql & "SELECT "
	Sql = Sql & "COMPETENZE.DENOMINAZIONE, COMPETENZE.ID_COMPETENZA, COMPET_FP.GRADO_FP_COMP "
	Sql = Sql & "FROM "
	Sql = Sql & "COMPETENZE, COMPET_FP "
	Sql = Sql & "WHERE "
	Sql = Sql & "COMPETENZE.ID_COMPETENZA = COMPET_FP.ID_COMPETENZA "
	Sql = Sql & "AND "
	Sql = Sql & "COMPET_FP.ID_FIGPROF = " & request("ID_FIGPROF")

'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
	Set Rs = Conn.Execute (Sql)
	if not rs.eof then COMPETENZE = Rs.getrows()

	Sql = ""
	Sql = Sql & "SELECT "
	Sql = Sql & "CONOSCENZE.DENOMINAZIONE, COMPET_CONOSC.ID_COMPETENZA, COMPET_CONOSC.GRADO_COMP_CON, "
	Sql = Sql & "	(SELECT PERS_CONOSC.COD_GRADO_CONOSC FROM PERS_CONOSC "
	Sql = Sql & "	WHERE "
	Sql = Sql & "	PERS_CONOSC.ID_CONOSCENZA = CONOSCENZE.ID_CONOSCENZA AND "
	Sql = Sql & "	PERS_CONOSC.ID_PERSONA = " & IDP & ") GRADO_PERS "
	Sql = Sql & "FROM "
	Sql = Sql & "CONOSCENZE, COMPET_CONOSC, COMPET_FP "
	Sql = Sql & "WHERE "
	Sql = Sql & "CONOSCENZE.ID_CONOSCENZA = COMPET_CONOSC.ID_CONOSCENZA "
	Sql = Sql & "AND "
	Sql = Sql & "COMPET_CONOSC.ID_COMPETENZA = COMPET_FP.ID_COMPETENZA "
	Sql = Sql & "AND "
	Sql = Sql & "COMPET_FP.ID_FIGPROF = " & request("ID_FIGPROF")

'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
	Set Rs = Conn.Execute (Sql)
	if not rs.eof then CONOSCENZE = Rs.getrows()

	Sql = ""
	Sql = Sql & "SELECT "
	Sql = Sql & "CAPACITA.DENOMINAZIONE, COMPET_CAPAC.ID_COMPETENZA, COMPET_CAPAC.GRADO_COMP_CAP, "
	Sql = Sql & "	(SELECT PERS_CAPAC.GRADO_CAPACITA FROM PERS_CAPAC "
	Sql = Sql & "	WHERE "
	Sql = Sql & "	PERS_CAPAC.ID_CAPACITA = CAPACITA.ID_CAPACITA AND "
	Sql = Sql & "	PERS_CAPAC.ID_PERSONA = " & IDP & ") GRADO_PERS "
	Sql = Sql & "FROM "
	Sql = Sql & "CAPACITA, COMPET_CAPAC, COMPET_FP "
	Sql = Sql & "WHERE "
	Sql = Sql & "CAPACITA.ID_CAPACITA = COMPET_CAPAC.ID_CAPACITA "
	Sql = Sql & "AND "
	Sql = Sql & "COMPET_CAPAC.ID_COMPETENZA = COMPET_FP.ID_COMPETENZA "
	Sql = Sql & "AND "
	Sql = Sql & "COMPET_FP.ID_FIGPROF = " & request("ID_FIGPROF")

'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
	Set Rs = Conn.Execute (Sql)
	if not rs.eof then CAPACITA = Rs.getrows()

	Sql = ""
	Sql = Sql & "SELECT "
	Sql = Sql & "SETTORI.DENOMINAZIONE, SETTORI.ID_SETTORE, "
	Sql = Sql & "VALIDAZIONE.ID_VALID, VALIDAZIONE.FL_VALID, VALIDAZIONE.ID_PERS_INS "
	Sql = Sql & "FROM "
	Sql = Sql & "SETTORI, SETTORI_FIGPROF, VALIDAZIONE "
	Sql = Sql & "WHERE "
	Sql = Sql & "SETTORI_FIGPROF.ID_VALID = VALIDAZIONE.ID_VALID "
	Sql = Sql & "AND "
	Sql = Sql & "SETTORI.ID_SETTORE = SETTORI_FIGPROF.ID_SETTORE "
	Sql = Sql & "AND "
	Sql = Sql & "SETTORI_FIGPROF.ID_FIGPROF = " & request("ID_FIGPROF")

'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
	Set Rs = Conn.Execute (Sql)
	if not rs.eof then SETTORI = Rs.getrows()


	Sql = ""
	Sql = Sql & "SELECT "
	Sql = Sql & "COMPORTAMENTI.DENOMINAZIONE, COMPORTAMENTI.ID_COMPORTAMENTO, "
	Sql = Sql & "VALIDAZIONE.ID_VALID, VALIDAZIONE.FL_VALID, VALIDAZIONE.ID_PERS_INS, COMPOR_FP.GRADO_FP_COMPOR, "
	Sql = Sql & "	(SELECT PERS_COMPOR.GRADO_PERS_COMPOR FROM PERS_COMPOR "
	Sql = Sql & "	WHERE "
	Sql = Sql & "	PERS_COMPOR.ID_COMPORTAMENTO = COMPORTAMENTI.ID_COMPORTAMENTO AND "
	Sql = Sql & "	PERS_COMPOR.ID_PERSONA = " & IDP & ") GRADO_PERS "
	Sql = Sql & "FROM "
	Sql = Sql & "COMPORTAMENTI, COMPOR_FP, VALIDAZIONE "
	Sql = Sql & "WHERE "
	Sql = Sql & "COMPOR_FP.ID_VALID = VALIDAZIONE.ID_VALID "
	Sql = Sql & "AND "
	Sql = Sql & "COMPORTAMENTI.ID_COMPORTAMENTO = COMPOR_FP.ID_COMPORTAMENTO "
	Sql = Sql & "AND "
	Sql = Sql & "COMPOR_FP.ID_FIGPROF = " & request("ID_FIGPROF")

'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
	Set Rs = Conn.Execute (Sql)
	if not rs.eof then COMPORT = Rs.getrows()

set Rs = Nothing
Conn.Close
Set conn = Nothing

%>
<!-- #include Virtual="/strutt_testa2.asp" -->

<SCRIPT LANGUAGE="JavaScript">
function stp(){
bb=(document.layers)?document.layers['aa']:(document.getElementById)?document.getElementById('aa').style:document.all['aa'].style;
bb.visibility="hidden"
self.print();
bb.visibility="visible"
}
</SCRIPT>


<body bgcolor="FFFFFF">
<!-- <%=IDP%> -->
<table border=0 cellspacing=1 cellpadding=0 width='500' style="font-family: Verdana; font-size: 12pt; color:#ffffff">
<tr><td background='../imgs/sfmnsmall.jpg' align=center><font color='#000000'><B><small><%=request("DENOMINAZIONE")%></small></B></td></tr>
</table>
<br>
<%
	if IsArray(SETTORI) then
	Response.Write "<table border=0 width='500' cellspacing=1 cellpadding=0 style='font-family: Verdana; font-size: 10pt; color:#000000'>"
	Response.write "<tr>"
	Response.Write "<td>"

	Response.Write "<table border=0 cellspacing=1 width=100% style='font-size: 8pt; font-family: verdana'>"
	Response.Write "<tr>"
		Response.Write "<td bgcolor='#f1f3f3'><b>Sector Merceol�gico</b></td>"
	Response.Write "</tr>"
		for I = lbound(SETTORI,2) to Ubound(SETTORI,2)
			Response.Write "<tr>"
				Response.Write "<td>" & SETTORI(0,I) & "</td>"
			Response.Write "</tr>"
		next
		Response.Write "</table>"
	Response.Write "</td>"
	Response.Write "</tr>"
	Response.Write "</table>"
	end if
%>
<br>
<%
	if IsArray(COMPETENZE) then
	Response.Write "<table border=0 width='500' cellspacing=1 cellpadding=0 style='font-family: Verdana; font-size: 10pt; color:#000000'>"
	Response.write "<tr>"
	Response.Write "<td>"

	Response.Write "<table border=0 cellspacing=1 width=100% style='font-size: 8pt; font-family: verdana'>"
	Response.Write "<tr>"
		Response.Write "<td bgcolor='#f1f3f3' colspan='2'><b>Competencia</b></td>"
		Response.Write "<td bgcolor='#f1f3f3' align='center'><b>Atendido</b></td>"
		Response.Write "<td bgcolor='#f1f3f3' align='center'><b>Dich.</b></td>"
		Response.Write "<td bgcolor='#f1f3f3' align='center'>&nbsp;</td>"
	Response.Write "</tr>"
	for i = lbound(COMPETENZE,2) to Ubound(COMPETENZE,2)
		Response.Write "<tr><td colspan=5>&nbsp;</td></tr>"
		Response.Write "<tr>"
		Response.Write "<td colspan='5'><b>&nbsp;&nbsp;" & COMPETENZE(0,I) & "</b></td></TR>"
		Response.Write "<tr>"
		Response.Write "<td WIDTH='12'>&nbsp;</td>"
		Response.Write "<td colspan=4><b>Conocimiento</b></td>"
		Response.Write "</tr>"
		if isarray(CONOSCENZE) THEN
			for jj = lbound(CONOSCENZE,2) to Ubound(CONOSCENZE,2)
			if int(0 & COMPETENZE(1,I)) = int(0 & CONOSCENZE(1,jj)) then
				Response.Write "<tr>"
					Response.Write "<td>&nbsp;</td>"
					Response.Write "<td bgcolor='#f1f3f3' >" & CONOSCENZE(0,jj) & "</td>"
					Response.Write "<td bgcolor='#f1f3f3' align='center'>" & CONOSCENZE(2,jj) & "</td>"
					if isnull(CONOSCENZE(3,jj)) then
						testo= "--- "
					else
						testo= CONOSCENZE(3,jj)
					end if

					if Int(0 & CONOSCENZE(3,jj)) >= Int(0 & CONOSCENZE(2,jj)) then
						img = "<img src='Blu_2.gif'>"
					else
						img = "<img src='Red_2.gif'>"
					end if
					Response.Write "<td bgcolor='#f1f3f3' align='center'>&nbsp;&nbsp;" & testo & "</td>"
					Response.Write "<td bgcolor='#f1f3f3' align='center'>&nbsp;&nbsp;" & img & "</td>"
				Response.Write "</tr>"
			end if
			next
		END IF

		Response.Write "<tr>"
		Response.Write "<td WIDTH='30'>&nbsp;</td>"
		Response.Write "<td colspan=4><b>Capacit�</b></td>"
		Response.Write "</tr>"

		IF ISARRAY(CAPACITA) THEN
			for jj = lbound(CAPACITA,2) to Ubound(CAPACITA,2)
			if int(0 & COMPETENZE(1,I)) = int(0 & CAPACITA(1,jj)) then
				Response.Write "<tr>"
					Response.Write "<td>&nbsp;</td>"
					Response.Write "<td bgcolor='#f1f3f3'>" & CAPACITA(0,jj) & "</td>"
					Response.Write "<td bgcolor='#f1f3f3' align='center'>" & CAPACITA(2,jj) & "</td>"
					if isnull(CAPACITA(3,jj)) then
						testo= "--- "
					else
						testo= CAPACITA(3,jj)
					end if
					if Int(0 & CAPACITA(3,jj)) >= Int(0 & CAPACITA(2,jj)) then
						img = "<img src='Blu_2.gif'>"
					else
						img = "<img src='Red_2.gif'>"
					end if

					Response.Write "<td align='center' bgcolor='#f1f3f3'>&nbsp;&nbsp;" & Testo & "</td>"
					Response.Write "<td align='center' bgcolor='#f1f3f3'>&nbsp;&nbsp;" & img & "</td>"
				Response.Write "</tr>"
			end if
			next
		END IF
	next
	Response.Write "</table>"
	end if
%>
<br>
<%
	if IsArray(COMPORT) then
	Response.Write "<table border=0 width='500' cellspacing=1 cellpadding=0 style='font-family: Verdana; font-size: 10pt; color:#000000'>"
	Response.write "<tr>"
	Response.Write "<td>"

	Response.Write "<table border=0 cellspacing=1 width=100% style='font-size: 8pt; font-family: verdana'>"
	Response.Write "<tr>"
		Response.Write "<td bgcolor='#f1f3f3'><b>Comportamiento</b></td>"
		Response.Write "<td bgcolor='#f1f3f3' align='center'><b>Atendido</b></td>"
		Response.Write "<td bgcolor='#f1f3f3' align='center'><b>Dich.</b></td>"
		Response.Write "<td bgcolor='#f1f3f3' align='center'>&nbsp;</td>"
	Response.Write "</tr>"
		for I = lbound(COMPORT,2) to Ubound(COMPORT,2)
			Response.Write "<tr>"
				Response.Write "<td bgcolor='#f1f3f3'>&nbsp;&nbsp;" & COMPORT(0,I) & "</td>"
				Response.Write "<td bgcolor='#f1f3f3'>&nbsp;&nbsp;" & COMPORT(5,I) & "</td>"

				if isnull(COMPORT(5,I)) then
					Testo = " -- "
				else
					Testo = Int(0 & COMPORT(5,I))
				End if

				if Int(0 & COMPORT(6,I)) >= Int(0 & COMPORT(5,I)) then
					img = "<img src='Blu_2.gif'>"
				else
					img = "<img src='Red_2.gif'>"
				End if

				Response.Write "<td bgcolor='#f1f3f3' align='center'><b>&nbsp;&nbsp;" & Testo & "</b></td>"
				Response.Write "<td bgcolor='#f1f3f3' align='center'><b>&nbsp;&nbsp;" & img & "</b></td>"
			Response.Write "</tr>"
		next
		Response.Write "</table>"
	Response.Write "</td>"
	Response.Write "</tr>"
	Response.Write "</table>"
	end if

%>

<div id="aa">
<form name='indietro'>
<input type='button' value='Volver' OnClick='goToPage("VisBilancio.asp")' CLASS='My'>
<input type='button' value='Imprimir' OnClick='stp()' CLASS='My'>
</form>
<!-- #include Virtual="/strutt_coda2.asp" -->

