<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #INCLUDE FILE="../Utils.asp" -->
<%
OLD_DT_TMST = ConvDateToDB(request("DT_TMST"))
OraDate = ConvDateToDB(Now())

Set Conn = server.CreateObject ("ADODB.Connection")
Conn.open strConn
Conn.BeginTrans 

Sql = " DELETE PERS_AZ_PROF "
Sql = Sql &  " WHERE ID_PROG_PROF = " & Request("ID_PROG_PROF")
Sql = Sql &  " AND rtrim(COD_AZIONE) = " & rtrim(Request("COD_AZIONE"))
Sql = Sql &  " AND DT_TMST = " & OLD_DT_TMST
 LogTrace ucase(mid(session("progetto"),2)),IDP, "P",IDP,"PERS_AZ_PROF", "" &IDP &    "", "DEL" 	
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
Conn.Execute Sql



	' controllo se tutte le azioni per un intero profilo (FPR) vengono cancellate
	Sql = " SELECT COUNT (*) "
	Sql = Sql &  " FROM PERS_AZ_PROF "
	Sql = Sql &  " WHERE ID_PROG_PROF = " & Request("ID_PROG_PROF")
	'Sql = Sql &  " AND DT_TMST = " & OLD_DT_TMST

'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
	Set Rs = Conn.Execute (Sql)
			VALIDA = Rs(0) 
			Rs.Close

		' se tutte le azioni per il profilo sono state cancellate viene eliminato 
		' anche il Progetto relativo (per evitare violazioni dell'indice)

		if cint(VALIDA) = 0 then 
			Sql = " DELETE FROM PERS_PROG_PROF  "
			Sql = Sql &  " WHERE ID_PROG_PROF = " & Request("ID_PROG_PROF")
				LogTrace ucase(mid(session("progetto"),2)),IDP, "P",IDP,"PERS_PROG_PROF", "" &IDP &    "", "DEL" 	
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
			Conn.Execute Sql
		end if

Conn.CommitTrans 

Conn.Close
Set conn = Nothing

QS = ""
QS = QS & "ID_FIGPROF=" & request("ID_FIGPROF") & "&" 
QS = QS & "ID_ELEMENTO=" & request("ID_ELEMENTO") & "&" 
QS = QS & "COD_ELEMENTO=" & request("COD_ELEMENTO") 


Back = Request("back")
Back = Back & "?" & QS

%>
<SCRIPT>
<!--
	goToPage('<%=Back%>')
//-->
</SCRIPT>
