<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #INCLUDE FILE="../Utils.asp" -->
<%
OraDate = ConvDateToDB(Now())
Set Conn = server.CreateObject ("ADODB.Connection")
Conn.open strConn

Conn.BeginTrans 
ID_PROG_PROG = request("ID_PROG_PROF")

COD_ELEMENTO  = REQUEST("COD_ELEMENTO")

'#### Se non esiste un'occorrenza gi� creata in PERS_PROG_PROF si crea e si deriva la chiave primaria

if request("ID_PROG_PROF") = "" then
	LstCampi = "ID_PERSONA,ID_FIGPROF,ID_ELEMENTO,COD_ELEMENTO"
		LstVal = ""
		LstVal = LstVal &  IDP & "|"
		LstVal = LstVal & request("ID_FIGPROF") & "|"
		LstVal = LstVal & request("ID_ELEMENTO") & "|"
		LstVal = LstVal & "'" & request("COD_ELEMENTO") & "'"

		Sql = ""
		Sql = QryIns("PERS_PROG_PROF",LstCampi,LstVal)
			LogTrace ucase(mid(session("progetto"),2)),IDP, "P",IDP,"PERS_PROG_PROF", "" &IDP &    "", "INS" 		

'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
		Conn.Execute Sql
		
		Sql = ""
		Sql = Sql & "SELECT max(ID_PROG_PROF) "
		Sql = Sql & "FROM PERS_PROG_PROF "
		Sql = Sql & "WHERE "
		Sql = Sql & "ID_FIGPROF = " & request("ID_FIGPROF") & " AND "
		Sql = Sql & "ID_ELEMENTO = " & request("ID_ELEMENTO")
		
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
		Set Rs = conn.Execute (Sql)
		ID_PROG_PROF = Rs(0)
		Set Rs = Nothing

else '#### Altrimenti si fa riferimento alla chiave gi� esistente
		ID_PROG_PROF = request("ID_PROG_PROF")
end if

'#### Si inserisce Il nuovo Progetto professionale

COMPLETATO = Request("COMPLETATO")
If COMPLETATO = "" Then COMPLETATO = "N"

	LstCampi = "ID_PROG_PROF,COD_AZIONE,DESCRIZIONE,OBIETTIVO,DURATA,OSSERVAZIONI,COMPLETATO,DT_TMST"

		LstVal = ""
		LstVal = LstVal &  ID_PROG_PROF & "|"
		LstVal = LstVal & "'" & request("COD_AZIONE") & "'" & "|"
		LstVal = LstVal & "'" & CleanXsql(request("DESCRIZIONE")) & "'" & "|"
		LstVal = LstVal & "'" & CleanXsql(request("OBIETTIVO")) & "'" & "|"
		LstVal = LstVal & "'" & CleanXsql(request("DURATA")) & "'" & "|"
		LstVal = LstVal & "'" & CleanXsql(request("OSSERVAZIONI")) & "'" & "|"
		LstVal = LstVal & "'" & COMPLETATO & "'" & "|"
		LstVal = LstVal & OraDate
		
		Sql = ""
		Sql = QryIns("PERS_AZ_PROF",LstCampi,LstVal)

'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
		Conn.Execute Sql
	 LogTrace ucase(mid(session("progetto"),2)),IDP, "P",IDP,"PERS_AZ_PROF", "" &IDP &    "", "INS" 	
Conn.CommitTrans 

Conn.Close
Set conn = Nothing



QS = ""
QS = QS & "ID_FIGPROF=" & request("ID_FIGPROF") & "&" 
QS = QS & "ID_ELEMENTO=" & request("ID_ELEMENTO") & "&" 
QS = QS & "COD_ELEMENTO=" & request("COD_ELEMENTO") 


Back = Request("back")
Back = Back & "?" & QS
%>

<SCRIPT>
<!--
	goToPage('<%=Back%>')
//-->
</SCRIPT>

