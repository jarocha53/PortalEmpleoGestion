<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #INCLUDE FILE="../Utils.asp" -->
<%
OraDate = ConvDateToDB(Now())

Set Conn = server.CreateObject ("ADODB.Connection")
Conn.open strConn
COMPLETATO = request("COMPLETATO")

DESCRIZIONE = ucase(CleanXsql(left(Request("DESCRIZIONE"),100)))
OBIETTIVO = ucase(CleanXsql(left(Request("OBIETTIVO"),100)))
DURATA = ucase(CleanXsql(left(Request("DURATA"),100)))
OSSERVAZIONI = ucase(CleanXsql(left(Request("OSSERVAZIONI"),100)))
OLD_DT_TMST = ConvDateToDB(request("DT_TMST"))

Conn.BeginTrans 

Sql = " UPDATE PERS_AZ_PROF "
Sql = Sql &  " SET "
Sql = Sql &  " DESCRIZIONE = '" & DESCRIZIONE & "',"
Sql = Sql &  " OBIETTIVO = '" & OBIETTIVO  & "',"
Sql = Sql &  " DURATA = '" & DURATA & "',"
Sql = Sql &  " OSSERVAZIONI = '" & OSSERVAZIONI  & "',"
Sql = Sql &  " COMPLETATO = '" & COMPLETATO & "',"
Sql = Sql &  " DT_TMST = " & OraDate
Sql = Sql &  " WHERE ID_PROG_PROF = " & Request("ID_PROG_PROF")
Sql = Sql &  " AND rtrim(COD_AZIONE) = " & rtrim(Request("COD_AZIONE"))
Sql = Sql &  " AND DT_TMST = " & OLD_DT_TMST

'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
Conn.Execute Sql
	LogTrace ucase(mid(session("progetto"),2)),IDP, "P",IDP,"PERS_AZ_PROF", "" &IDP &    "", "MOD" 		
Conn.CommitTrans 

Conn.Close
Set conn = Nothing

QS = ""
QS = QS & "ID_FIGPROF=" & request("ID_FIGPROF") & "&" 
QS = QS & "ID_ELEMENTO=" & request("ID_ELEMENTO") & "&" 
QS = QS & "COD_ELEMENTO=" & request("COD_ELEMENTO") 

QS = QS & "Back=" & Request("Back")

Back = Request("back")
Back = Back & "?" & QS
%>
<SCRIPT>
<!--
	goToPage('<%=Back%>')
//-->
</SCRIPT>
