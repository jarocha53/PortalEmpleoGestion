<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include Virtual = "/strutt_testa2.asp"-->
<!--#include Virtual = "/include/OpenConn.asp"-->
<!--#include Virtual = "/include/DecCod.asp"-->
<!--#include Virtual = "/include/ControlDateVB.asp"-->
<!--#include Virtual = "/util/dbutil.asp"-->
<!--#include Virtual = "/include/ImpostaProgetto.asp"-->


<script language="javascript" src="IMP_ControlliImp.js"></script>

<script LANGUAGE="Javascript">

//include del file per fare i controlli sulla numericit� dei campi
<!--#include Virtual = "/Include/ControlNum.inc"-->

//include del file per fare i controlli sulla stringa
<!--#include Virtual = "/Include/ControlString.inc"-->
<!--#include virtual = "/Include/help.inc"-->
<!--#include virtual = "/include/SelComune.js"-->

function PulisciRes()
{
	frmIscri.txtComune.value = ""
	frmIscri.txtComuneHid.value = ""
    frmIscri.txtCAP.value = ""
}	
	
	//Funzione per i controlli dei campi 
	function ControllaDati()
	{
	
	// -------- CONTROLLO VALORI INSERITI -------------------------		

	/*Parte di controlli sui campi del referente*/
		document.frmIscri.txtCognome.value=TRIM(document.frmIscri.txtCognome.value)
		sCognome=ValidateInputStringWithOutNumber(document.frmIscri.txtCognome.value)
		if (sCognome==false){
			alert("Cognome formalmente errato.")
			document.frmIscri.txtCognome.focus() 
			return false
		}
		if (frmIscri.txtCognome.value == ""){
			alert("Il campo Cognome � obbligatorio!")
			frmIscri.txtCognome.focus() 
			return false
		}
		
		document.frmIscri.txtNome.value=TRIM(document.frmIscri.txtNome.value)
		sNome=ValidateInputStringWithOutNumber(document.frmIscri.txtNome.value)
		if  (sNome==false){
			alert("Nome formalmente errato.")
			document.frmIscri.txtNome.focus() 
			return false
		}		
		if (frmIscri.txtNome.value == ""){
			alert("Il campo Nome � obbligatorio!")
			frmIscri.txtNome.focus() 
			return false
		}
		
		if (frmIscri.elements[2].selectedIndex == 0) {
			alert ("Il campo Ruolo � obbligatorio")
			frmIscri.elements[2].focus()
			return false
		}
		
		//Controlli su LOGIN: accetta numeri e/o lettere, punto, underscore, trattino
		document.frmIscri.txtLogin.value = TRIM(document.frmIscri.txtLogin.value)
		var anyString = document.frmIscri.txtLogin.value;
		if (frmIscri.txtLogin.value == "") {
			alert("Il campo Login � obbligatorio")
			frmIscri.txtLogin.focus() 
			return false
		}
		for (var i=0; i<=anyString.length-1; i++) {
			if(((anyString.charAt(i) >= "A") && (anyString.charAt(i) <= "Z")) || 
				((anyString.charAt(i) >= "a") && (anyString.charAt(i) <= "z")) || 
				((anyString.charAt(i) >= "0") && (anyString.charAt(i) <= "9")) ||
				(anyString.charAt(i) == ".") || (anyString.charAt(i) == "-") ||
				(anyString.charAt(i) == "_"))
			{
			}
			else
			{		
				document.frmIscri.txtLogin.focus();
			 	alert("Inserire nel campo Login un valore valido");
				return false
			}		
		}
		
		//Email
		document.frmIscri.txtMail.value=TRIM(document.frmIscri.txtMail.value)
		if (document.frmIscri.txtMail.value =="") {
			alert("Email obbligatoria")
			document.frmIscri.txtMail.focus() 
			return false
		}
		appoEmail=ValidateEmail(document.frmIscri.txtMail.value)
		if  (appoEmail==false){
			alert("Indirizzo Email formalmente errato")
			document.frmIscri.txtMail.focus() 
			return false
		}		

	/*Parte di controlli sui campi dell'impresa*/
		if (frmIscri.elements[5].selectedIndex == 0) {
			alert ("Il campo Tipologia Impresa � obbligatorio")
			frmIscri.elements[5].focus()
			return false
		}

		document.frmIscri.txtRagSoc.value=TRIM(document.frmIscri.txtRagSoc.value)
		if (frmIscri.txtRagSoc.value == ""){
			alert("Il campo Ragione sociale � obbligatorio")
			frmIscri.txtRagSoc.focus()
			return false
		}
		
		if (frmIscri.elements[7].selectedIndex == 0) {
			alert ("Il campo Tipo � obbligatorio")
			frmIscri.elements[7].focus()
			return false
		}
		
		if (frmIscri.elements[8].selectedIndex == 0) {
			alert ("Il campo Settore � obbligatorio")
			frmIscri.elements[8].focus()
			return false
		}
		
		document.frmIscri.txtDescAtt.value=TRIM(document.frmIscri.txtDescAtt.value)
		if (frmIscri.txtDescAtt.value == ""){
			alert("Il campo Attivit� � obbligatorio")
			frmIscri.txtDescAtt.focus() 
			return false
		}
		
		// partita iva o codice fiscale: uno dei due � obbligatorio
		frmIscri.txtPartIva.value = TRIM(frmIscri.txtPartIva.value);
		frmIscri.txtCodFis.value = TRIM(frmIscri.txtCodFis.value);
		if ((frmIscri.txtPartIva.value == "") && (frmIscri.txtCodFis.value == ""))
		{
			alert("E' obbligatorio inserire almeno uno dei due campi Partita iva o Codice fiscale")
			frmIscri.txtPartIva.focus() 
			return false; 
		}
				
		// controllo partita iva 
		if (frmIscri.txtPartIva.value != "")
		{
			if (!IsNum(frmIscri.txtPartIva.value))
			{
				alert("Il campo Partita iva deve essere numerico")
				frmIscri.txtPartIva.focus() 
				return false
			}
			if (frmIscri.txtPartIva.value.length != 11)
			{
				alert("La partita Iva deve essere di 11 caratteri")
				frmIscri.txtPartIva.focus() 
				return false
			}
		}
		
		// controllo codice fiscale
		if (frmIscri.txtCodFis.value != "")
		{
			blank = " ";
			if (!ChechSingolChar(frmIscri.txtCodFis.value,blank))
			{
				alert("Il campo Codice Fiscale non pu� contenere spazi bianchi")
				frmIscri.txtCodFis.focus();
				return false;
			}
			if ((frmIscri.txtCodFis.value.length != 11) && (frmIscri.txtCodFis.value.length != 16))
			{
				alert("Il campo Codice fiscale deve essere di 11 o 16 caratteri")
				frmIscri.txtCodFis.focus(); 
				return false;
			}	
			if (!ValidateInputStringWithNumber(frmIscri.txtCodFis.value))
			{
				alert("Il campo Codice fiscale � formalmente errato")
				frmIscri.txtCodFis.focus()
				return false;
			}
		}
		
	/*Parte di controlli sui campi della sede*/	
		document.frmIscri.txtDescrizione.value=TRIM(document.frmIscri.txtDescrizione.value)
		if (frmIscri.txtDescrizione.value == ""){
				alert("La Descrizione Sede � obbligatoria")
			frmIscri.txtDescrizione.focus() 
			return false
		}
		
		if (frmIscri.elements[14].selectedIndex == 0) {
			alert ("Il campo Tipo Sede � obbligatorio")
			frmIscri.elements[14].focus()
			return false
		}

		document.frmIscri.txtIndirizzo.value=TRIM(document.frmIscri.txtIndirizzo.value)
		if (frmIscri.txtIndirizzo.value == ""){
			alert("Il campo Indirizzo � obbligatorio")
			frmIscri.txtIndirizzo.focus() 
			return false
		}

		// provincia
		if (frmIscri.cmbProvRes.value == "")	{
			alert("Il campo Provincia � obbligatorio")
			frmIscri.cmbProvRes.focus()
			return false;
		}
		
		
		document.frmIscri.txtComune.value=TRIM(document.frmIscri.txtComune.value)
		if (frmIscri.txtComune.value == ""){
			alert("Il Comune � un dato obbligatorio")
			//frmIscri.txtComune.focus() 
			return false
		}
		
		//CAP obbligatorio
		if ((document.frmIscri.txtCAP.value == "")||
			(document.frmIscri.txtCAP.value == " ")){
			alert("CAP obbligatorio")
			document.frmIscri.txtCAP.focus() 
			return false
		}
		
		//CAP deve essere numerico
		if (document.frmIscri.txtCAP.value != ""){
			if (!IsNum(document.frmIscri.txtCAP.value)){
				alert("Il CAP deve essere Numerico")
				document.frmIscri.txtCAP.focus() 
				return false
			}
		}
		
		//CAP deve essere diverso da zero
		if (document.frmIscri.txtCAP.value != ""){
			if (eval(document.frmIscri.txtCAP.value) == 0){
				alert("Il CAP non pu� essere zero")
				document.frmIscri.txtCAP.focus()
				return false
			}
		}
		
		//CAP deve essere di 5 caratteri
		if (document.frmIscri.txtCAP.value != ""){
			if (document.frmIscri.txtCAP.value.length != 5){
				alert("Formato del CAP errato")
				document.frmIscri.txtCAP.focus() 
				return false
			}
		}
		
		/*if (frmIscri.elements[18].selectedIndex == 0) {
			alert ("Campo obbligatorio")
			frmIscri.elements[18].focus()
			return false
		}*/
		
		document.frmIscri.txtSedeTel.value = TRIM(document.frmIscri.txtSedeTel.value)
		if (document.frmIscri.txtSedeTel.value != ""){
			if (!IsNum(frmIscri.txtSedeTel.value)){
				alert("Il campo Telefono deve essere numerico")
				frmIscri.txtSedeTel.focus() 
				return false
			}
			if (eval(document.frmIscri.txtSedeTel.value) == 0){
				alert("Il numero di telefono non pu� essere zero")
				document.frmIscri.txtSedeTel.focus()
				return false
			}
		}			
		
		document.frmIscri.txtSedeFax.value = TRIM(document.frmIscri.txtSedeFax.value)
		if (document.frmIscri.txtSedeFax.value != ""){
			if (!IsNum(frmIscri.txtSedeFax.value)){
				alert("Il campo Fax deve essere numerico")
				frmIscri.txtSedeFax.focus() 
				return false
			}
			if (eval(document.frmIscri.txtSedeFax.value) == 0){
				alert("Il numero di fax non pu� essere zero")
				document.frmIscri.txtSedeFax.focus()
				return false
			}
		}			

		if (document.frmIscri.txtCentroImpiego.value == ""){
			alert("Il centro per l'impiego � obbligatorio")
			return false
		}			

		if (document.frmIscri.cmbProvRes.value != document.frmIscri.txtProvinciaOld.value){
			alert("Il centro per l'impiego non risiede in questa provincia")
			return false
		}			
	
		//Il referente pu� procedere con l'iscrizione solo se da il consenso
		/*if (document.frmIscri.radConsenso[1].checked)
		{
			alert("Impossibile completare la registrazione senza il consenso")
			return false			
		}*/
		
		return true
	}	


	//Cancella Dati
	function Cancella(frmIscri){
		document.frmIscri.reset();
		return false
	}
</script>

<%
'sCons = Request.Form("radConsenso")
sContr = Request.Form("scontrollo")
 	
If sContr = 2 Then%>
	<div align="center">

	<table border="0" width="520" cellspacing="0" cellpadding="0" height="81">
	   <tr>
	     <td width="520" background="<%=Session("Progetto")%>/images/titoli/strumenti2g.gif" height="81" valign="bottom" align="right">
	       <table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
	         <tr>
	           <td width="100%" valign="top" align="right"><b class="tbltext1a">Registrazione Azienda &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></b></td>
	         </tr>
	       </table>
	     </td>
	   </tr>
	</table>
	<br>

	<%
	'inizio 28/11/2003
	sFilePath = Server.MapPath("/") & session("progetto") & "/Pgm/QImprese/IMP_Cnf1Liv.asp"
	set oFileSystemObject	= Server.CreateObject("Scripting.FileSystemObject")
	oFileObj = oFileSystemObject.FileExists (sFilePath)

	'If session("progetto")="/PLAVORO" Then
	If session("progetto")=impostaProgetto or oFileObj = false Then%>
			<form action="IMP_Cnf1Liv.asp" method="post" name="frmIscri" id="frmIscri">
	<%
	Else%>
		<form action="<%=session("progetto")%>/Pgm/QImprese/IMP_Cnf1Liv.asp" method="post" name="frmIscri" id="frmIscri">
	<%
	End If
	' fine 28/11/2003
	%>
    
	<table cellpadding="0" cellspacing="0" width="520" border="0">
		<tr height="18">
			<td class="sfondomenu" height="18" width="47%">
				<span class="tbltext0"><b>&nbsp;AZIENDA</b></span></td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
			<td valign="middle" align="right" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1">(*) campi obbligatori</td>
		</tr>
		<tr>
			<td class="sfondocommaz" width="57%" colspan="3" height="18" align="left"><br><span class="tbltext">
				Per ottenere la password di accesso, inserire i dati richiesti e premere <b>Invia</b>.<br>
				La password, assegnata dal sistema, verr� inviata all'indirizzo di posta elettronica indicato.<br>
				In seguito potr� essere cambiata con una di propria scelta.		
				<a href="Javascript:Show_Help('/Pgm/help/QImprese/IMP_Iscr1Liv')"><img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a><br>
			</td> 
		</tr>   
		<tr height="18">
			<td colspan="3" class="sfondocommaz" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">
			</td>
		</tr>
	</table>
	<br>

	<!---------- Inizio Tabella Inserimento ---------->
	<table border="0" width="500" cellpadding="0" cellspacing="0">   
		<tr>
			<td class="sfondocommaz" COLSPAN="2" width="500">
				<span class="tbltext1a"><b>Referente</b></span>
			</td>
		</tr>
		<tr>
			<td colspan="2"></td>
		</tr>     
		<tr>
			<td align="left" class="tbltext1" nowrap WIDTH="100"><b>
				&nbsp;Cognome </b>*	
			</td>
			<td align="left" Width="400">
				<input style="TEXT-TRANSFORM: uppercase;" size="49" class="textblack" maxlength="50" name="txtCognome">
			</td>
		</tr> 
		<tr>
			<td align="left" class="tbltext1" nowrap><b>
				&nbsp;Nome </b>*		
			</td>
			<td align="left">
				<input style="TEXT-TRANSFORM: uppercase;" size="49" class="textblack" maxlength="50" name="txtNome">
			</td>
		</tr> 	
		<tr>
			<td align="left" class="tbltext1" nowrap><b>
				&nbsp;Ruolo </b>*	
			</td>
			<td align="left">
				<%
				'Richiamo funzione che popola la combo RUOLO
				'-----------------------------------------------			
				sRefRuo = "RUOLO|0|" & date & "|" & NULL & "|cmbRefRuo|ORDER BY DESCRIZIONE"			
				CreateCombo(sRefRuo)
				'-----------------------------------------------				
				%>		
			</td>
		</tr>   
		<tr>
			<td align="left" class="tbltext1" width="85" nowrap><b>
				&nbsp;Login </b>*		
			</td>
			<td align="left">
				<input style="TEXT-TRANSFORM: uppercase;" size="37" maxlength="15" class="textblack" name="txtLogin">
			</td>
		</tr>    
		<tr>
			<td align="left" class="tbltext1" nowrap><b>
				&nbsp;E-mail </b>*		
			</td>
			<td align="left">
				<input size="49" class="textblack" maxlength="100" name="txtMail">
			</td>
		</tr>     
		<tr>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2" class="sfondocommaz" width="350">
				<span class="tbltext1a"><b>Impresa</b></span>
			</td>
		</tr> 
		<tr>
			<td align="left" class="tbltext1" nowrap><b>
				&nbsp;Tipologia&nbsp;Impresa </b>*
			</td>
			<td align="left">
				<%
				'Richiamo funzione che popola la combo Tipologia Impresa
				'------------------------------------------------------------			
				sTimpr = "TIMPR|0|" & date & "|" & NULL & "|cmbCodTimpr|AND CODICE <> '03' ORDER BY DESCRIZIONE"			
				CreateCombo(sTimpr)
				'------------------------------------------------------------				
				%>	
			</td>
		</tr> 	   	
		<tr>
			<td align="left" class="tbltext1" nowrap><b>
				&nbsp;Ragione&nbsp;Sociale </b>*	
			</td>
			<td align="left">
				<input style="TEXT-TRANSFORM: uppercase;" size="49" class="textblack" maxlength="50" name="txtRagSoc">
			</td>
		</tr>
		<tr>
			<td align="left" class="tbltext1" nowrap><b>
				&nbsp;Tipo </b>*
			</td>
			<td align="left">
				<%
				'Richiamo funzione che popola la combo FORMA GIURIDICA(=TIPO)
				'------------------------------------------------------------			
				sTipo = "FGIUR|0|" & date & "|" & NULL & "|cmbCodForm|ORDER BY DESCRIZIONE"			
				CreateCombo(sTipo)
				'------------------------------------------------------------				
				%>	
			</td>
		</tr>
		<tr>
			<td align="left" class="tbltext1" nowrap><b>
				&nbsp;Settore </b>*
		    </td>
			<td align="left">
				<%
				'Effettuo una SELECT per popolare la combo SETTORE
				'-------------------------------------------------
				Dim rsRec
				Dim Id
				Dim Name
					
				Id = cmbCodSet
				Name = cmbCodSet
					
				'Lancia una select SQL 
				sSQL = "SELECT id_settore, denominazione FROM SETTORI ORDER BY denominazione"
					
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
				set rsRec = CC.Execute(sSQL)
					
				'Eseguo la select SQL
				'creo la Combo Box con i dati che query ha ritornato.
				Response.Write "<SELECT style='width=262px' class=textblacka ID='cmbCodSet' name='cmbCodSet'><OPTION value=></OPTION>"

				do until rsRec.EOF
					Response.Write "<OPTION "
					Response.write "value ='" & rsRec.Fields("ID_SETTORE") & _
						"'> " & rsRec.Fields("DENOMINAZIONE")  & "</OPTION>"
					rsRec.MoveNext 
				loop
				'-------------------------------------------------
				rsRec.Close
				Set rsRec = Nothing
				%>		
			</td>
		</tr>
		<tr>
			<td align="left" class="tbltext1" nowrap><b>
				&nbsp;Attivit� </b>*
			</td>
			<td align="left">
			    <input size="49" class="textblack" maxlength="255" name="txtDescAtt" style="TEXT-TRANSFORM: uppercase;">
			</td>
		</tr>
		<tr height="2">
			<td class="sfondocommaz" colspan="4" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
		</tr>
		<tr>
			<td colspan="4" class="textblack">
				 I campi <b>Partita IVA</b> e <b>Codice fiscale</b> possono essere compilati in alternativa o entrambi.
				 <br>E' comunque obbligatoria la compilazione di almeno uno dei due.
			</td>
		</tr>
		<tr>
			<td align="left" class="tbltext1" nowrap>
				<b>&nbsp;P.IVA </b>
			</td>
			<td align="left">
				<input style="TEXT-TRANSFORM: uppercase;" class="textblack" size="16" maxlength="11" name="txtPartIva">
			</td>
		</tr>
		<tr>
			<td align="left" class="tbltext1"><b>&nbsp;Codice&nbsp;fiscale<b>
			</td>
			<td align="left">
				<input style="TEXT-TRANSFORM: uppercase;" size="16" class="textblack" maxlength="16" name="txtCodFis">
			</td>
		</tr>
		<tr>
			<td align="left" class="tbltext1" nowrap><b>
				&nbsp;Sito Web</b>
			</td>
			<td align="left">
				<input style="TEXT-TRANSFORM: uppercase;" size="49" class="textblack" maxlength="50" name="txtSitoWeb">
			</td>
		</tr>
		<tr>
			<td align="middle" colspan="2">&nbsp;
			</td>
		</tr>
		<tr>
			<td class="sfondocommaz" COLSPAN="2" width="350">
				<span class="tbltext1a"><b>Sede</b></span>
			</td>
		</tr>   
		<tr>
			<td align="left" class="tbltext1" nowrap><b>
				&nbsp;Descrizione&nbsp;Sede </b>*	
			</td>
			<td align="left">
				<input style="TEXT-TRANSFORM: uppercase;" size="49" class="textblack" maxlength="50" name="txtDescrizione">
			</td>
		</tr>
		<tr>
			<td align="left" class="tbltext1" nowrap><b>
				&nbsp;Tipo Sede </b>*		
			</td>
			<td align="left">
				<%
				'Richiamo funzione che popola la combo TIPO SEDE
				'-----------------------------------------------
				sSede = "TSEDE|0|" & date & "|" & NULL & "|cmbCodSede|ORDER BY DESCRIZIONE"			
				CreateCombo(sSede)
				'-----------------------------------------------
				%>			
			</td>
		</tr>
		<tr>
			<td align="left" class="tbltext1" nowrap><b>
				&nbsp;Indirizzo </b>*		
			</td>
			<td align="left">
				<input style="TEXT-TRANSFORM: uppercase;" size="49" class="textblack" maxlength="50" name="txtIndirizzo">
			</td>
		</tr>
		<!--tr>		<td align="left" class="tbltext1" nowrap><b>			&nbsp;CAP</b> *		</td>		<td align="left">			<input style="TEXT-TRANSFORM: uppercase;" size="15" class="textblack" maxlength="5" name="txtCAP">		</td>   </tr-->
		<tr>		
			<td align="left" nowrap class="tbltext1">
					<b>&nbsp;Provincia </b>*
			</td>			
			<td align="left" width="60%">				
				<%
				sProv = "PROV|0|" & date & "|" & PRV_RES & "|cmbProvRes' onchange='PulisciRes()| ORDER BY DESCRIZIONE"			
				CreateCombo(sProv)	
				%>
			</td>
		</tr>		
		<tr>		
			<td align="left" nowrap class="tbltext1">
				<b>&nbsp;Comune </b>*
			</td>
			<td nowrap>
				<span class="tbltext">
					<input type="text" name="txtComune" style="TEXT-TRANSFORM: uppercase;" class="textblack" size="40" readonly value="<%=DescrComuneResid%>">
					<input type="hidden" id="txtComuneHid" name="txtComuneHid" value="<%=COM_RES%>">
					<%
					NomeForm="frmIscri"
					CodiceProvincia="cmbProvRes"
					NomeComune="txtComune"
					CodiceComune="txtComuneHid"
					Cap="txtCAP"
					%>
					<a href="Javascript:SelComune('<%=NomeForm%>','<%=CodiceProvincia%>','<%=NomeComune%>','<%=CodiceComune%>','<%=Cap%>')" ID="imgPunto1" name="imgPunto1" onmouseover="javascript:window.status='';return true"><img border="0" src="<%=Session("Progetto")%>/images/bullet1.gif"></a>
				</span>				
			</td>
		</tr>		
	    <tr>		
			<td align="left" nowrap class="tbltext1">
				<b>&nbsp;CAP </b>*
			</td>			
			<td align="left" width="60%">
				<input type="text" name="txtCAP" class="textblack" value="<%=CAP_RES%>" size="10" maxlength="5">
			</td>
		</tr>	
	   	<!--tr>		<td align="left" class="tbltext1" nowrap><b>			&nbsp;Comune </b>*				</td>		<td align="left">			<input style="TEXT-TRANSFORM: uppercase;" size="49" class="textblack" maxlength="50" name="txtComune">		</td>   </tr>	<tr>		<td align="left" class="tbltext1" nowrap><b>			&nbsp;Provincia </b>*			</td>		<td align="left"><%		'	sProv = "PROV|0|" & date & "|" & NULL & "|cmbProv|ORDER BY DESCRIZIONE"						CreateCombo(sProv)%>		</td>	</tr-->  
		<tr>
			<td align="left" class="tbltext1" nowrap><b>
				&nbsp;Telefono </b>	
			</td>
			<td align="left">
				<input style="TEXT-TRANSFORM: uppercase;" size="28" class="textblack" maxlength="20" name="txtSedeTel">
			</td>
		</tr>  
		<tr>
			<td align="left" class="tbltext1" nowrap><b>
				&nbsp;Fax</b>
			</td>
			<td align="left">
				<input style="TEXT-TRANSFORM: uppercase;" size="28" class="textblack" maxlength="20" name="txtSedeFax">
			</td>
		</tr>  
		<tr>
			<td align="left" class="tbltext1" nowrap><b>
				&nbsp;Centro Impiego </b>*
			</td>
			<td align="left" colspan="2" width="60%">
				<span class="tbltext">
				<%'txtCentroImpiego%>
				<input type="text" id="txtCentroImpiego" name="txtCentroImpiego" readonly class="textblack" size="40" value="<%=DescCimpiego%>"> 
				<input type="Hidden" id="txtCentroImpiegoHid" name="txtCentroImpiegoHid">
				<input type="hidden" id="txtProvinciaOld" name="txtProvinciaOld">
				<a href="Javascript:SelCentroImpiego('CENTRO_IMPIEGO','ID_CIMPIEGO','txtCentroImpiego')" onmouseover="javascript:window.status='';return true" ID="imgPunto1" name="imgPunto1"><img border="0" src="<%=Session("Progetto")%>/images/bullet1.gif"></a>
				</span>				
			</td>
	   </tr>
	   <%'END IF%>
	   <tr>
			<td align="middle" colspan="2">&nbsp;
			</td>
	   </tr>
	   <tr>
			<td align="middle" colspan="2">&nbsp;
			</td>
	   </tr>  
	</table>
	<!---------- Fine Tabella Inserimento ---------->

	<br>     
	<table border="0" cellpadding="1" cellspacing="1" width="360" align="center">
		<tr>
			<td align="middle" colspan="2">
				<input type="image" name="Invia" src="<%=Session("Progetto")%>/images/conferma.gif" border="0" value="Conferma" onclick="return ControllaDati()">
			</td>
	   </tr>
		<tr>
			<td>&nbsp;</td>
	   </tr>
	</table>

	</form>

	</div>
<%
Else
	response.redirect "/Pgm/QImprese/IMP_PreIscrizione.asp"	
End If
%>

<!--#include Virtual = "/include/closeconn.asp"-->
<!--#include Virtual = "/strutt_coda2.asp"-->
