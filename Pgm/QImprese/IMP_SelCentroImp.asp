<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #include virtual="/util/dbutil.asp" -->
<!-- #include virtual="/include/openconn.asp" -->
<!--#include Virtual = "/include/DecCod.asp"-->

<html>
<head>
<title>Centro Impiego</title>
<link REL="STYLESHEET" TYPE="text/css" HREF="<%=session("progetto")%>/fogliostile.css">

<script language="javascript">
<!-- #include virtual="/include/help.inc" -->

function controlloVal(valoreDesc,text){

	var valoreRegione;
	var valoreArea;
	
	valoreArea = opener.document.frmIscri.txtCentroImpiego.value;
	
	valoreDesc = valoreDesc.toUpperCase();

	switch (text){
			
		case "txtCentroImpiego":
			if (valoreDesc != valoreArea){
				opener.document.frmIscri.txtCentroImpiego.value = ""
				opener.document.frmIscri.txtCentroImpiegoHid.value = ""
			}
		break;	
		
	}
}

function InviaSelezione(idArea,Denominazione,sNomeCampo,sProv){
		
	controlloVal(Denominazione,sNomeCampo)
		
	var i = 0
		
	while (opener.document.frmIscri.elements[i].name != sNomeCampo){
		i++;
	}
	
	opener.document.frmIscri.txtProvinciaOld.value = sProv
	opener.document.frmIscri.elements[i+1].value = idArea
	var ciccio = Denominazione.replace("$", "'")
	opener.document.frmIscri.elements[i].value = ciccio.toUpperCase()
	self.close()
}


//function InviaProv(){
//	frmSelArea.submit();
//}

function Chiudi()
{
	self.close()
}

</script>
</head>
<%
sArea = Request.QueryString("Area")
sCampo = Request.QueryString("sTxtArea")
sNomeCampo = Request.QueryString("NomeCampo")
'sProv = Request.QueryString("cmbProv")
sProv = Request.QueryString("Provincia")
%>

<body>
<form name="frmSelArea" method="get" action"IMP_SelCentroImp.asp">

<input type="hidden" name="Area" value="<%=sArea%>">
<input type="hidden" name="sTxtArea" value="<%=sCampo%>">
<input type="hidden" name="NomeCampo" value="<%=sNomeCampo%>">
<input type="hidden" id="txtProv" name="txtProv" value="<%=sProv%>">

<table border="0" CELLPADDING="0" CELLSPACING="0" width="380">

<tr height="17">
	<td class="sfondomenu" width="67%" height="18"><span class="tbltext0">
		<b>&nbsp;SELEZIONE CENTRO IMPIEGO</b>
	</td>
	<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">
	</td>
	<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
</tr>
<tr>
	<td class="sfondocommaz" width="57%" colspan="3">
		Selezionare la provincia e scegliere il Centro per 
		l'Impiego dall'elenco che verr� visualizzato. <br>
		Premere <b>Chiudi</b> per non selezionare nulla.
		<a href="Javascript:Show_Help('/Pgm/help/Iscr_Utente/UTE_SelArea')">
		<img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0" onmouseover="javascript:window.status='' ; return true"></a> 
	</td>
</tr>
<tr height="2">
	<td class="sfondocommaz" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif">
	</td>
</tr>
</table>

<br><p class="tbltext1" align="center"><b>Selezione del Centro Impiego</b></p>

<table border="0" CELLPADDING="2" CELLSPACING="2" width="380">
<%
if sProv <> "" then
'	sSQL = "SELECT ID_CIMPIEGO,DESC_CIMPIEGO FROM CENTRO_IMPIEGO WHERE PRV='" & sProv & "' ORDER BY DESC_CIMPIEGO"
'	sSQL = "SELECT ID_SEDE,DESCRIZIONE FROM SEDE_IMPRESA A, IMPRESA B WHERE A.ID_IMPRESA=B.ID_IMPRESA AND A.COD_TIMPR='03' AND PRV='" & sProv & "' ORDER BY DESCRIZIONE"
	sSQL = "SELECT B.ID_SEDE, B.DESCRIZIONE FROM IMPRESA A, sede_impresa b WHERE a.COD_TIMPR='03' and a.id_impresa=b.id_impresa and prv='" & sProv & "' ORDER BY DESCRIZIONE"
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	Set rsArea = CC.Execute(sSQL)		
%>	
	
	<tr>
		<td colspan="2" align="center">
			<a class="textred" href="Javascript:InviaSelezione('','','<%=sNomeCampo%>')"><b>Rimuovi Selezione</b></a>
		</td>
	</tr>
<%
	if not rsArea.EOF then
		Do While Not rsArea.EOF
%>
	<tr>
		<td nowrap width="190" class="tblsfondo">
			<a class="textblack" href="Javascript:InviaSelezione('<%=rsArea(0)%>','<%=replace(rsArea(1),"'","$")%>','<%=sNomeCampo%>','<%=sProv%>')"><b><%=ucase(rsArea(1))%></b></a>
		</td>
		<%rsArea.MoveNext%>
		<td nowrap width="190" class="tblsfondo">
		<%if not rsArea.EOF then%>
			<a class="textblack" href="Javascript:InviaSelezione('<%=rsArea(0)%>','<%=replace(rsArea(1),"'","$")%>','<%=sNomeCampo%>','<%=sProv%>')"><b><%=ucase(rsArea(1))%></b></a>
				<%rsArea.MoveNext%>
		<%else%>
			&nbsp;
		<%end if%>
		</td>
	</tr>
<%
		loop
		rsArea.Close
%>
	<tr>
		<td colspan="2" align="center">
			<a class="textred" href="Javascript:InviaSelezione('','','<%=sNomeCampo%>','')"><b>Rimuovi Selezione</b></a>
		</td>
	</tr>
<%
	end if
end if
%>	
</table>
</form>

<table width="380" cellspacing="2" cellpadding="1" border="0">
	<tr align="center">
		<td>
			<a href="javascript: Chiudi()"><img src="<%=Session("progetto")%>/images/chiudi.gif" title="Chiudi la pagina" border="0" align="absBottom"></a>
		</td>
	</tr>		
</table>
</body>
</html>
<!-- #include virtual="/include/closeconn.asp" -->
