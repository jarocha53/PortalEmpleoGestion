<%
Sub Inizio()
%>
<html>
<head>
	<link REL="STYLESHEET" TYPE="text/css" HREF="<%=session("progetto")%>/fogliostile.css">
	<title>Scegli Images</title>	
</head>
<script LANGUAGE="JavaScript">
function InviaSelezione(saPath)
{	
	
	opener.otherDoc.body.innerHTML = opener.otherDoc.body.innerHTML + '<img src=' + saPath + ' border=0>'
	
	self.close()
}

if (!document.layers&&!document.all)
event="test"
function showtip2(current,e,img)
{
	if (document.all&&document.readyState=="complete")
	{
		var tab
		
		tab= '<table border=2 class=sfondomenu><tr><td><img src=' + img + ' border=0 onclick="hidetip2();"></td></tr></table>'
	
		//document.all.tooltip2.innerHTML='<img src=' + img + ' border=0>'
		document.all.tooltip2.innerHTML=tab
		document.all.tooltip2.style.pixelLeft=document.body.scrollLeft +10
		document.all.tooltip2.style.pixelTop=document.body.scrollTop +10
		document.all.tooltip2.style.visibility="visible"
	}
}
function hidetip2()
{
	if (document.all)
		document.all.tooltip2.style.visibility="hidden"
	else if (document.layers)
	{
		clearInterval(currentscroll)
		document.tooltip2.visibility="hidden"
	}
}

function Valida(frmSub)
{
	if (frmSub.txtUpload.value=="")
	{
		frmSub.txtUpload.focus()
		alert("Selezionare un file")
		return false
	}
	return true
}

function ChechMultiChar(InputString,Caratteri)
{
    var anyString = InputString;
    var anyCaratteri = Caratteri;
	for ( var i=0; i<=anyString.length-1; i++ ){
		for ( var k=0; k<=anyCaratteri.length-1; k++ ){
			if ((anyString.charAt(i) == anyCaratteri.charAt(k))) {
				return(false);
			}
		}			
	}
	return(true);
}

function NuovaCartella(sFold, sImg)
{   
	var NomeCartella = ""
	NomeCartella = window.prompt("Inserisci il nome della cartella","Nuova Cartella")
	if (NomeCartella != null)
	{
		if ((NomeCartella.length >> 0) && (ChechMultiChar(NomeCartella,"\/:*?<>|'") == true))
		{
			location.href = "REP_ScegliImages.asp?modo=3&img="+sImg+"&sNF="+sFold+NomeCartella
		}
		else
		{
			alert('Nome della cartella non consentito')
		}
	}	
}

function EliminaCartella(sImg)
{
	if (window.confirm("Confermi l'eliminazione della cartella?"))
	{
		location.href = "REP_ScegliImages.asp?modo=4&img="+sImg
	}
}
function EliminaFile(sImg,count)

{


var nPrimoEle 
	var ok
	i = 1
	ok = 0 
	nPrimoEle = 0
    nCheck = 0
	nomecheck =  nCheck + "cancimg" 
	
			while ((i<=count))
				 
			{
				if (Elfile.elements[nPrimoEle].checked){
					ok = 1;
					break
				}
				nPrimoEle = nPrimoEle + 2;
				nomecheck =  (nCheck + 1) + "cancimg" ;
				i++;
			}

	if (ok == 0) {
		alert ("Devi selezionare almeno un file")
	}
	else
	{
		if (window.confirm("Confermi l'eliminazione del/dei file/s?"))
			{
				Elfile.submit();
			}
	}
	
	
	
}	
</script>
<body>
<div id="tooltip2" style="position:absolute; visibility:hidden; color:white">

</div>
<p align="center">
<%
	dim sFunzione
	dim sTitolo
	dim sCommento
	dim bCampiObbl
	
	sFunzione = "REPOSITORY"
	sTitolo = "REPOSITORY"
	sCommento = "Selezione Immagini"
	bCampiObbl = false
	sHelp = "/Pgm/help/repository/REP_ScegliImages/"		
%>
	<script LANGUAGE="JavaScript" src="/Include/Help.inc">
</script>

<%
'Questo include permette di visualizzare la testata delle pagine;
'Nel programma chiamante occorre definire e valorizzare le seguenti variabili:
' - "sFunzione"   --> Nome della funzione
' - "sTitolo"     --> Titolo della pagina
' - "sCommento"   --> Commento della pagina
' - "bCampiObbl"  --> Flag per visualizzare la dicitura "Campi obbligatori"
' - "sHelp"       --> Nome della pagina contenente l'help (non attivo)
%>
<!--table border="0" width="520" cellspacing="0" cellpadding="0" height="81">   <tr>     <td width="400" background="<%=Session("Progetto")%>/images/titoli/strumenti2b.gif" height="81" valign="bottom" align="right">       <table border="0" width="260" height="30" cellspacing="0" cellpadding="0">         <tr>           <td width="100%" valign="top" align="right"><b class="tbltext1a"><%=sFunzione%></span></b></td>         </tr>       </table>     </td>   </tr></table-->
<br>
<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
	<tr height="18">
		<td class="sfondomenu" width="67%" height="18"><span class="tbltext0"><b>&nbsp;<%=sTitolo%></b></span></td>
		<td><img src="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
		<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">
<%			
			if bCampiObbl then
				Response.Write "(*) campi obbligatori"
			end if
%>
		</td>
	</tr>
</table>
<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
	<tr>
		<td align="left" class="sfondocomm" width="500">
			<%=sCommento%><br>
			Cliccare su un'immagine per poterla inserire nel testo, oppure su preview per vederla nelle dimensioni originali.
			Per eliminare un'immagine, selezionarla e cliccare su &quot;elimina immagini selezionate&quot;  (� possibile anche una scelta multipla). 
			Per aggiungere una nuova immagine alla lista fare &quot;Sfoglia&quot; e poi eseguire l'Upload.	
		</td>
		<td valign="bottom" class="sfondocomm" width="75">
			<%
			if sHelp="" then
			%>
			    <img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0" onmouseover="javascript:window.status='' ; return true">
			<%
			else
			%>
				<a href="Javascript:Show_Help('<%=sHelp%>')">
					<img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0" onmouseover="javascript:window.status='' ; return true">
				</a>
			<%
			end if
			%>
		</td>
	</tr>
	<tr height="2">
		<td class="sfondocomm" width="450" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif">
		</td>
	</tr>
</table>
<br>

<%
End Sub
'------------------------------------------------------------------------
Sub Fine()
%>
<br>
<table width="500" border="0">
	<tr>
		<td align="center">
			<a href="javascript:window.close()">
				<img src="<%=Session("progetto")%>/images/chiudi.gif" border="0" onmouseover="javascript:window.status='' ; return true">
			</a>
		</td>
	</tr>
</table>
</p>		
</body>
</html>
<%
End Sub
'------------------------------------------------------------------------
Function EstraiDir(sPath)
	for nPos = 1 to Len(sPath)
		if mid(sPath,nPos,1) = "\" then
			nLastPos=nPos
		end if
	next
	EstraiDir = left(sPath, nLastPos)
End Function
'------------------------------------------------------------------------
Function AccorciaStringa(sStringa, nLunghezza)
dim nLenStrDx
	if len(sStringa) <= nLunghezza then
		AccorciaStringa = sStringa
	else
		nLenStrDx = nLunghezza - 8 '8 � dato dai primi 5 caratteri + i 3 punti
		AccorciaStringa = left(sStringa,5) & "..." & right(sStringa, nLenStrDx)
	end if
End Function
'------------------------------------------------------------------------
Sub ImpostaPag()
	
dim nPos, nLastPos, folderspec
dim sPRel
dim nFile
dim sPercIcone
dim nContaDir

	on error resume next
	if sImg = "" then
		sPRel = sRoot
	else
		sPRel = EstraiDir(Replace(sImg,"/","\"))
	end if
	folderspec = server.mappath(sPRel)
	if err.number <> "0" then
		sPRel = sRoot
		folderspec = server.mappath(sPRel)
	end if
	on error goto 0
	
	Dim fso, f, f1, fc
	Set fso = CreateObject("Scripting.FileSystemObject")
	Set f = fso.GetFolder(folderspec)
	Set fc = f.Files
		Set fs = f.SubFolders
	
'S U B F O L D E R
%>
<table width="500" border="0" class="sfondocomm" cellspacing="0" cellpadding="0">
	<tr height="25">
		<td align="left" class="sfondocomm">
			Cartella selezionata: <b><%=AccorciaStringa(sprel,40)%></b>
		</td>
		<td align="center" valign="center" width="30">
<%
	sPRel = Replace(sPRel,"\","/")
	if (sPRel <> sRoot) then
	
	app = EstraiDir(Replace(mid(sprel,1,len(sprel)-1),"/","\"))
		if len(app) > 0 then 
		prec = mid(app,1,len(app)-1)
		%>
			<a href="REP_ScegliImages.asp?modo=1&amp;img=<%=prec%>" class="tblagg">
				<img src="<%=session("progetto")%>/images/UpFolder.gif" border="0" alt="Livello superiore" WIDTH="18" HEIGHT="16">
			</a>
		<%
		end if
	end if
%>
		</td>
<% 

if Session("rorga") = "RC" Then %>
		
		<td align="center" valign="center" width="30">
			<a href="javascript:NuovaCartella('<%=sPRel%>', '<%=sImg%>')">
				<img src="<%=session("progetto")%>/images/NewFolder.gif" border="0" alt="Nuova Cartella" WIDTH="19" HEIGHT="18">
			</a>
		</td>	
<%if  sprel <> "/images/repository/" then %>			
		<td align="center" valign="center" width="30">
			<a href="javascript:EliminaCartella('<%=sPrel%>')">
				<img src="<%=session("progetto")%>/images/cestino.gif" border="0" alt="Elimina Cartella">
			</a>
		</td>		
<% End If %>
	</tr>

</table>
<% End If %>

<table width="500" border="0" class="sfondocomm" cellspacing="0" cellpadding="0">
<%
	nContaDir = 0
	for each f2 in fs
		if left(f2.name,1) <> "_" then
		nContaDir = nContaDir + 1
%>
		<tr HEIGHT="1" bgcolor="White">
			<td align="left">
			</td>
		</tr>
			<tr>
				<td align="left">
					<a href="REP_ScegliImages.asp?modo=1&amp;img=<%=sprel & f2.name%>" class="tblagg">
						<img src="<%=session("progetto")%>/images/cartellasm.gif" border="0" WIDTH="17" HEIGHT="14">
						&nbsp;<%=f2.name%>
					</a>
				</td>
			</tr>
<%
		end if
	next
	if nContaDir = 0 then
%>
		<tr HEIGHT="1" bgcolor="White">
			<td align="left">
			</td>
		</tr>
		<tr HEIGHT="25">
			<td align="left" class="sfondocomm">
				(nessuna sottocartella)
			</td>
		</tr>
<%
	end if
%>
</table>
<br>
<form method="post" action="REP_ScegliImages.asp?modo=5&amp;img=<%=sprel%>" name="Elfile">
<table width="500" border="0" align="center" cellpadding="0" cellspacing="0">
	<tr>
<%
		i=0
		nFile = 0
	For Each f1 in fc
		sImmagine=replace(sprel & f1.name, "\", "/")
		if (right(f1.name,4) = ".gif") or (right(f1.name,4) = ".jpg") then
			nFile = nFile + 1
			if sImmagine = sImg then
				sAttr = " class=sfondocomm"
			else
				sAttr = ""
			end if	
%>

			<td valign="top">
				<table width="100" border="0" align="left">
					<tr>
						<td align="left" <%=sattr%>>
							<a href="javascript:InviaSelezione('<%=sImmagine%>');" border="0" class="tbldett">
								<img src="<%=sImmagine%>" width="70" height="50" border="0">
							</a>
						</td>
						<td width="30">&nbsp;</td>
					</tr>
					
					<tr>
						<td><b class="tbltext"><%=f1.name%></b>
						</td>
						<td width="30">&nbsp;</td>
					</tr>
					
					<tr>
						<td <%=sattr%>>
							<a href="Javascript:showtip2(this,event,'<%=sImmagine%>');" class="sfondocomm">preview</a>
						</td>
						<td width="30">&nbsp;</td>
					</tr>
					
					<tr>
						<td>
							<input type="checkbox" name="<%=i%>cancimg">
							<input type="hidden" name="<%=i%>pathfile" value="<%=sImmagine%>">
						</td>
						<td width="30">&nbsp;</td>
					</tr>			
				</table>
<%
			if nFile mod 4 = 0 then
%>
			</td>
		</tr>
			
		
<%
			end if
		end if
	i=i+1
	Next
	
%>
			<tr height="25" width="380"> 
				<td align="left" class="sfondocomm" colspan="4">
					Contenuto cartella: <%=fc.count%> file
				
				</td>		
<% if Session("rorga") = "RC" and fc.count > 0 then %>					
				<td width="190" class="sfondocomm" align="right">Elimina immagine/i selezionata/e</td>
				<td class="sfondocomm" colspan="4">

					<a href="javascript:EliminaFile('<%=sPrel%>','<%=nFile%>')">
						<img src="<%=session("progetto")%>/images/cestino.gif" border="0" alt="Elimina File dalla Cartella">
					</a>
					</td>
<%end IF%>				
			</tr>
	</table>
</form>
<br>
<%
End Sub
'------------------------------------------------------------------------
Sub Upload(sPImg)
if Session("rorga") = "RC" then	
%>

<table width="500" border="0" align="center">
	<tr height="25"> 
		<td align="left" class="sfondocomm">
			Upload del file
		</td>
	</tr>
	<tr>
		<td>
			<form METHOD="POST" encType="multipart/form-data" onsubmit="return Valida(this)" name="frmUpload" action="REP_ScegliImages.asp?modo=2&amp;img=<%=sPImg%>">
				<input type="File" id="txtUpload" name="txtUpload">
				<input type="submit" id="cmdUpload" name="cmdUpload" value="Upload">
				<!--input type="hidden" id="modo" name="modo" value="2">				<input type="hidden" id="img" name="img" value="<%=sPImg%>"-->
			</form>
		</td>
	</tr>
</table>

<%
End If
End Sub
'------------------------------------------------------------------------
Function SalvaFile(sPImg)

	if sPImg = "" then
		sPImg = sRoot
	end if
	
	On Error Resume Next

	Set objUpload = Server.CreateObject("Dundas.Upload.2")
	objUpload.UseUniqueNames = false
	
	PathSave = Server.MapPath("\") & replace(sPImg, "/", "\")
	objUpload.Save PathSave

	strName = objUpload.Form("txtUpload")

	Set objUpload = Nothing
	
	if Err.number = 0 then
		SalvaFile = "File correttamente salvato"
	else
		SalvaFile = "Errore durante il salvataggio: " & Err.number & ": " & Err.Description
	end if
	
End Function
'------------------------------------------------------------------------
Function CreaCartella()
dim sNuova, f

sNuova = Request.QueryString("sNF")

Set FSO = Server.CreateObject ("Scripting.FileSystemObject") 
'on error resume next 

path = Server.MapPath("\") & replace(sNuova, "/", "\")
if not fso.folderexists(path) then

	set f = fso.CreateFolder(path)

end if

set FSO = Nothing 

if Err.number = 0 then
	CreaCartella = "Cartella correttamente creata"
else
	CreaCartella = "Errore durante la creazione della cartella: " & Err.number & ": " & Err.Description
end if

End Function
'------------------------------------------------------------------------
Function EliminaCartella()

folderspec = server.MapPath(sImg)

	Dim fso, f, f1, fc, s
	Set fso = CreateObject("Scripting.FileSystemObject")
	Set f = fso.GetFolder(folderspec)
	Set fc = f.Files
	Set fs = f.SubFolders
	if fc.count = 0 and fs.count = 0 then
		fso.DeleteFolder(folderspec)
		if Err.number = 0 then
			sPRel = Replace(sImg,"\","/")
			if (sPRel <> sRoot) then
				app = EstraiDir(Replace(mid(sprel,1,len(sprel)-1),"/","\"))
				if len(app) > 0 then 
					sImg = mid(app,1,len(app)-1)
				else
					sImg = sRoot
				end if
			else
				sImg = sRoot
			end if
			
			EliminaCartella = "Cartella correttamente eliminata"
		else
			EliminaCartella = "Errore durante l'eliminazione della cartella: " & Err.number & ": " & Err.Description
		end if
	else
		EliminaCartella = "Impossibile cancellare una cartella non vuota"
	end if
End Function
'-----------------------------------------------------------------------------------------------------------------------------
Function EliminaFile(sPath)

folderspec = server.MapPath(sPath)

	Dim fso, f, f1, fc, s
	Set fso = CreateObject("Scripting.FileSystemObject")
	Set f = fso.GetFolder(folderspec)
	Set fc = f.Files
for i=0 to fc.count
check = i & "cancimg"
pathfilex = i & "pathfile"
if Request.Form(check) = "on" then
	veropath = server.MapPath("\") & Request.Form(pathfilex)
	fso.DeleteFile(veropath)
end if
next

End Function
'------------------------------------------------------------------------
'M A I N

dim sRoot 
sRoot = "/images/repository/"
sImg = Request.QueryString("img")
nModo = Request.QueryString("modo")
select case nModo
	case 1
		sImg = sImg & "/"
	case 2
		sPar = SalvaFile(sImg)
		%>
		<script language="javascript">
			alert("<%=sPar%>");
		</script>
		<%	
	case 3
		sPar = CreaCartella()
		%>
		<script language="javascript">
			alert("<%=sPar%>");
		</script>
		<%
	case 4
		sPar = EliminaCartella()
		sImg = sImg & "/"
		%>
		<script language="javascript">
			alert("<%=sPar%>");
		</script>
			<%
	case 5
		sPar = EliminaFile(sImg)
				
end select
%>
<!--#include Virtual ="/include/openconn.asp"-->
<%
Inizio()
ImpostaPag()
Upload(sImg)
%>	
<!--#include Virtual ="/include/closeconn.asp"-->
<%
Fine()
%>