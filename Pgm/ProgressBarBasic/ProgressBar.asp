<%@ENABLESESSIONSTATE=FALSE%>
<%
'force this page to expire immediately
Response.Expires = -10000
%>
<!-- We disable maintaining session state so that more than one ASP 
         page can be processed at the same time -->
<html>
<head>
<meta NAME="GENERATOR" Content="Microsoft Visual Studio 6.0">
<!--the next meta tag causes the page to refresh and also pass the relevant progress ID -->
<meta http-equiv=refresh content="1,ProgressBar.asp?ProgressID=<%=Request.QueryString("ProgressID")%>">
<title>Upload Progress Bar</title>
</head>
<%
On Error Resume Next

'create an instance of the UploadProgress component
Set objUploadProgress = Server.CreateObject("Dundas.UploadProgress")

'this is where you would need to set the StateServer IP address
'   if StateServer.exe is running on a machine other than the machine 
'   where these ASP pages are stored.
'objUploadProgress.StateServer = "127.0.0.1"

'retrieve progress data from the StateServer
objUploadProgress.ProgressID = Request.QueryString("ProgressID")
objUploadProgress.GetProgress
Percentage = objUploadProgress.PercentCompleted
TotalSize = objUploadProgress.TotalSize
UploadSize = objUploadProgress.UploadedSize

if TotalSize = -1 OR UploadSize = -1 Then
	TotalSize = 0
	UploadSize = 0
End If

'if there are errors set the percentage value to -1
'note that when we reach 100% we delete the progress data at the State Server, so
'  the next time we call GetProgress an exception will be thrown and as a result
'  this window will close
if Err.number <> 0 Then
	Percentage = -1
End If

'delete progress data from the State Server when we hit 100%
if objUploadProgress.PercentCompleted = 100 Then
	objUploadProgress.DeleteProgress
End If

Set objUploadProgress = Nothing
%>

<script>
var val = <%=Percentage%>;
// If there are any errors - close frame
if(val == -1) {
	window.close();
}
</script>
<body>
<table border="1" width="100%">
  <tr>
    <td>
      <table ID="Prog" border="0" width="<%=Percentage%>%" bgcolor="#FF0000">
        <tr>
          <td width="100%">&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</body>
<P align="center"><FONT size="2">
<%=Percentage%>% (Uploaded <%=UploadSize%> of <%=TotalSize%> bytes)
</FONT></P>
</html>
