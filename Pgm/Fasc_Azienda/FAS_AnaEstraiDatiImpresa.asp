<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%

'Response.Write "ute=" & nAnaSede & "<br>"
'Response.Write "imp=" & nAnaIdImpresa & "<br>"
bDbAllineato= True
if bDbAllineato then
	sAnasql = " SELECT I.Rag_Soc, I.Cod_Forma, S.Denominazione As Settore, I.Dt_Cost, I.Cap_Soc, I.Cod_Valuta, "  &_
			" I.Desc_Att As Attivita, I.Cod_Fisc, I.Dt_Cess_Attivita, " &_
			" I.Cod_Cess_Attivita, I.SitoWeb, I.RENAE, I.DT_MTSS, I.Cod_Timpr, " &_
			" I.NUM_BPS, I.DT_BPS, I.NUM_DGI, I.DT_DGI" &_
			" FROM Impresa I, Settori S " &_
			" WHERE I.Id_settore = S.Id_Settore And Id_Impresa = " & clng(nAnaIdImpresa)
	
	

'PL-SQL * T-SQL  
SANASQL = TransformPLSQLToTSQL (SANASQL) 
	set RRAna = CC.Execute(sAnasql)
	
	if not RRAna.Eof then
	
		sAnaSettore		= RRAna.Fields("Settore")
		sAnaAttivita	= RRAna.Fields("Attivita")
		sAnaSitoWeb		= RRAna.Fields("SitoWeb")
		'nAnaPartIva		= RRAna.Fields("Part_Iva")
		'nAnaIscReg		= RRAna.Fields("Num_Isc_Reg")
		sAnaCodFisc		= RRAna.Fields("Cod_Fisc")
		if isnull(RRAna.Fields("RENAE"))then 
			sAnaCodRenae	= " - " 
			sDTMTSS = "" 
		Else 
			sAnaCodRenae	=  cstr(RRAna.Fields("RENAE"))
			sDTMTSS = " - Valido Hasta " & (RRAna.Fields("DT_MTSS"))
		end if	
		if isnull(RRAna.Fields("NUM_BPS"))then 
			sAnaCodBPS	= " - " 
			sDT_BPS = "" 
		Else 
			sAnaCodBPS	=  cstr(RRAna.Fields("NUM_BPS"))
			sDT_BPS = " - Valido Hasta " & (RRAna.Fields("DT_BPS"))
		end if	
		if isnull(RRAna.Fields("NUM_DGI"))then 
			sAnaCodDGI	= " - " 
			sDT_DGI = "" 
		Else 
			sAnaCodDGI	=  cstr(RRAna.Fields("NUM_DGI"))
			sDT_DGI = " - Valido Hasta " & (RRAna.Fields("DT_DGI"))
		end if			
		'nAnaInps		= RRAna.Fields("Num_Inps")
		nAnaCapSoc		= RRAna.Fields("Cap_Soc")
		sAnaDtCess		= ConvDateToString(RRAna.Fields("DT_Cess_Attivita"))
		if RRAna.Fields("Dt_Cost") <> "" then sAnaDtCost = (RRAna.Fields("Dt_Cost"))
	 	sAnaCessAtt		= RRAna.Fields("Cod_Cess_Attivita")
	 	sAnaForma		= RRAna.Fields("Cod_Forma")
	 	sAnaValuta		= RRAna.Fields("Cod_Valuta")
	 	sAnaTimpr		= RRAna.Fields("Cod_Timpr")
	 	sAnaFormaJuridica	= RRAna.Fields("Cod_Forma")
	 	
	 	
	 	
	 	if sAnaTimpr="08" then
	 		sTituloActividad="Actividad Principal"
	 	end if
	 	if sAnaTimpr="10" then
	 		sTituloActividad="Jurisdicción"
	 	end if		
	 	
	 	if sAnaTimpr="05" then
	 		if sAnaFormaJuridica or  sAnaFormaJuridica="06"  or sAnaFormaJuridica="03"  then
	 			sTituloActividad="Nombre"
	 			
	 		end if
	 	end if
	 	if sAnaTimpr="09"  or  sAnaTimpr="04"  or sAnaTimpr="07" or sAnaTimpr="11" or sAnaTimpr="12" then
	 		sTituloActividad="Observaciones"
	 	end if
	 	
		if Trim(sAnaSitoWeb)	= "" or isnull(sAnaSitoWeb) then
	 		sAnaSitoWeb	= " - "
	 	end if
		if Trim(sAnaCodFisc)	= "" or isnull(sAnaCodFisc) then
	 		sAnaCodFisc= " - "
	 	end if
		'if isnull(sAnaCodRenae) then
	 	'	sAnaCodRenae= " - "
	 	'end if
		if nAnaIscReg	= "" or isnull(nAnaIscReg) or not isnumeric(nAnaIscReg) then
	 		nAnaIscReg = " - "
	 	end if
		if nAnaInps	= "" or isnull(nAnaInps) or not isnumeric(nAnaInps) then
	 		nAnaInps	= " - "
	 	end if
		if nAnaCapSoc	= "" or isnull(nAnaCapSoc) or not isnumeric(nAnaCapSoc) then
	 		nAnaCapSoc = " - "
	 	end if	
		if not isDate(sAnaDtCess) then
	 		sAnaDtCess = " - "
	 	end if
		if trim(sAnaDtCost) = "" then
	 		sAnaDtCost	= " - "
	 	end if
	 	if Trim(sAnaCessAtt) = "" or isnull(sAnaCessAtt) then
	 		sAnaCessAtt= " - "
	 	else
			sAnaCessAtt= DecCodVal("AZVAR", "0", Date(), RRAna.Fields("Cod_Cess_Attivita"), 1)
	 	end if
	 	if Trim(sAnaForma)	= "" or isnull(sAnaForma) then
	 		sAnaForma	= " - "
	 	else
			sAnaForma	= DecCodVal("FGIUR", "0", Date(), RRAna.Fields("Cod_Forma"), 1)
	 	end if
	 	if Trim(sAnaValuta)	= "" or isnull(sAnaValuta) then
	 		sAnaValuta = "  "
	 	else
			sAnaValuta = DecCodVal("VALUT", "0", Date(), RRAna.Fields("Cod_Valuta"), 1)
	 	end if
		if Trim(sAnaTimpr)	= "" or isnull(sAnaTimpr) then
	 		sAnaTimpr	= " - "
	 	else
			sAnaTimpr	= DecCodVal("TIMPR", "0", Date(), RRAna.Fields("Cod_Timpr"), 1)
	 	end if
	 	
	 	
	 	'---------------------------------------------------------------------------------
	 	' Busca las prestaciones correspondientes a la empresa
	 	sAnaPrest	= ""
	 	sqlPrest =  "SELECT rs.rol, rs.servicio, rst.descripcion FROM impresa_rolesservicios rs " &_ 
	 				"INNER JOIN impresa_rolesserviciostipos rst ON rs.servicio = rst.servicio " &_ 
	 				"WHERE rs.id_impresa = " & nAnaIdImpresa & " ORDER BY rst.descripcion "
	 	
	 	set RSPrest = CC.Execute(sqlPrest)

	 	do while not RSPrest.eof 
	 		
	 		if cint(RSPrest("ROL")) = 2 and TEmp = "P" then
	 			sAnaPrest = sAnaPrest & ucase(RSPrest("DESCRIPCION")) & "<br>"
	 			'sqlPrestServ = "SELECT id_area FROM empresa_capacitacion WHERE " &_
	 			'				"id_impresa = " & nAnaIdImpresa & " AND servicio = " & RSPrest("SERVICIO")

				sqlPrestServ =  "SELECT ac.denominazione, acs.descripcion " &_ 
								"FROM empresa_capacitacion ec " &_ 
								"	INNER JOIN " &_ 
								"	area_corso ac ON ec.id_area = ac.id_area " &_ 
								"	INNER JOIN " &_ 
								"	area_corso_sub acs ON ec.id_subarea = acs.id_subarea " &_ 
								"WHERE id_impresa = " & nAnaIdImpresa & " AND servicio = " & RSPrest("SERVICIO")

				set RSPrestServ = server.CreateObject("Adodb.recordset")
	 			
	 			'Response.Write "<br> sqlPrestServ: " & sqlPrestServ
	 			
	 			set RSPrestServ = CC.Execute(sqlPrestServ)
	 			if not RSPrestServ.eof then
	 				do while not RSPrestServ.eof
	 					sAnaPrest = sAnaPrest & "&nbsp;&nbsp;&nbsp;" &_
	 								 lcase(RSPrestServ("DENOMINAZIONE")) & ": " &_
	 								  lcase(RSPrestServ("DESCRIPCION")) & "<br>"
	 					
	 					RSPrestServ.movenext
	 				loop
	 			end if
	 			RSPrestServ.close
	 			set RSPrestServ = nothing
	 		end if

	 		if cint(RSPrest("ROL")) = 1 and TEmp = "E" then
		 		sAnaPrest = sAnaPrest & ucase(RSPrest("DESCRIPCION")) & "<br>"
				sqlPrestServ =  "SELECT ap.denominazione as AREAPROF, fp.denominazione as FIGUPROF " &_ 
								"FROM empresa_perfilemp ep " &_  
								"	INNER JOIN " &_  
								"	aree_professionali ap ON ep.id_areaprof = ap.id_areaprof " &_ 
								"	INNER JOIN " &_ 
								"	figureprofessionali fp ON ep.id_figprof = fp.id_figprof " &_ 
								"WHERE id_impresa = " & nAnaIdImpresa & " AND servicio = " & RSPrest("SERVICIO")

				set RSPrestServ = server.CreateObject("Adodb.recordset")
	 			
	 			'Response.Write "<br> sqlPrestServ: " & sqlPrestServ
	 			
	 			set RSPrestServ = CC.Execute(sqlPrestServ)
	 			if not RSPrestServ.eof then
	 				do while not RSPrestServ.eof
	 					sAnaPrest = sAnaPrest & "&nbsp;&nbsp;&nbsp;" &_
	 								 lcase(RSPrestServ("AREAPROF")) & ": " &_
	 								  lcase(RSPrestServ("FIGUPROF")) & "<br>"
	 					
	 					RSPrestServ.movenext
	 				loop
	 			end if
	 			RSPrestServ.close
	 			set RSPrestServ = nothing
	 		end if

	 		
	 		RSPrest.movenext
	 	loop 
		if sAnaPrest = "" then sAnaPrest = " - "
		
		
		
			
		
		
		
		'Response.Write "TEmp: "  & TEmp
		'Response.End 
	 	
	 	
		

	' La ragione sociale dell'azienda viene indicata nella forma "Ragione sociale Impresa" + "Forma giuridica" + "Sede"
  		'''''sAnaRagSoc		= RRAna.Fields("Rag_Soc") & " " & sAnaForma & " (" & Ucase(sAnaDescr) & ")"
  		''''sandra. saco el municipio de la descrip de razon social
  		sAnaRagSoc		= RRAna.Fields("Rag_Soc") 
	else
		bDbAllineato = False
	end if
	RRAna.Close()
	
End if

set RRAna = nothing  


'REPRESENTANTE LEGAL '''''''VANI

SET RRREPLEGAL = server.CreateObject("adodb.recordset")

sRRREPLEGAL="SELECT REPLEGAL_APELLIDO, REPLEGAL_NOMBRE, REPLEGAL_CARGO, REPLEGAL_TIPODOC, REPLEGAL_NRODOC from impresa where id_impresa = " & nAnaIdImpresa

	sApellidoRL		= ""
 	sNombreRL	= ""
 	sCargoRL = ""
 	sTipoDocContactoRL= ""
  	sNroDocContactoRL	= ""

'PL-SQL * T-SQL  
SRRREPLEGAL = TransformPLSQLToTSQL (SRRREPLEGAL) 
set RRREPLEGAL = CC.Execute(sRRREPLEGAL)

if not RRREPLEGAL.eof  then
	sApellidoRL		= RRREPLEGAL("REPLEGAL_APELLIDO")
 	sNombreRL	= RRREPLEGAL("REPLEGAL_NOMBRE")
 	sCargoRL = RRREPLEGAL("REPLEGAL_CARGO")
 	sTipoDocContactoRL= RRREPLEGAL("REPLEGAL_TIPODOC")
  	sNroDocContactoRL	= RRREPLEGAL("REPLEGAL_NRODOC")
  	
	 	
 	if Trim(sApellidoRL)	= "" or isnull(sApellidoRL)then
 		sApellidoRL	= " - "
 	end if
 
 	if Trim(sNombreRL)	= "" or isnull(sNombreRL)then
 		sNombreRL	= " - "
 	end if

 	if Trim(sCargoRL)	= "" or isnull(sCargoRL)then
 		sCargoRL	= " - "
 	end if

 	if sTipoDocContactoRL<>"" or  not(isnull(sTipoDocContactoRL)) then
 		sSqlTablas="SELECT DESCRIZIONE from tades where Nome_tabella='DOCST' AND codice='" & sTipoDocContactoRL & "'" 
		
		
'PL-SQL * T-SQL  
SSQLTABLAS = TransformPLSQLToTSQL (SSQLTABLAS) 
		set RsTades = CC.Execute(sSqlTablas)
		if  not rsTades.eof then
			sTipoDocContactoRL="" & rsTades("DESCRIZIONE")
		end if	
		rsTades.close
		set rsTades=nothing
 	end if
 	
 	if Trim(sNroDocContactoRL)	= "" or isnull(sNroDocContactoRL)then
 		sNroDocContactoRL	= " - "
 	end if
END IF 

'''''''VANI

SET RRREPLEGAL = NOTHING


%>

