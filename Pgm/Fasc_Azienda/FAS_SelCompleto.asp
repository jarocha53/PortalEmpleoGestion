<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include virtual="/util/portallib.asp"-->
<!--#include Virtual = "/include/SysFunction.asp"-->
<!--#include Virtual = "/include/DecComun.asp"-->
<!--#include Virtual = "/include/DecCod.asp"-->
<!--#include Virtual = "/include/ControlDateVB.asp"-->
<!--#include Virtual ="/Include/openconn.asp"-->
<!--#include Virtual = "/include/MuestraFiltros.asp"-->

<%
TEmp = request("TEmp")

if (not isnull(TEmp)) and (TEmp <> "") then
	TEmp = ucase(TEmp)
end if 				         

dim Tit1p
dim Tit1s
dim TipoP
dim cupo
dim nombre
dim finicio
if temp = "E" then 
	tit1s = "Empleador"
	tit1p = "Empleadores"
	tipop = "Rama de Actividad"
	'cupo = "N�mero de Puestos Requeridos"
	finicio = "Fecha de Incorporaci�n al puesto"
	nombre="Nombre o Raz�n Social"
else
	tit1s = "Prestador"
	tit1p = "Prestadores"
	tipop = "Rama de Actividad"
	'cupo = "N�mero de Vacantes Requeridos"
	finicio = "Fecha de Inicio"
	nombre="Nombre de la Instituci�n u Organismo Responsable"
end if

%>
<html>
	<head>
	<title>Imprimir Expediente del <%=tit1s%></title>
	<link REL="STYLESHEET" TYPE="text/css" HREF="<%=session("progetto")%>/fogliostile.css">

	<script language="javascript">
		<!-- #include virtual="/include/help.inc" -->
		function PrimaDellaStampa(Progetto,Oggi)
		{
			//document.frmPulsanti.style.visibility='hidden';
			//document.frmInizioPrima.style.visibility='hidden'
			lblPulsanti.innerHTML = ""
			lblPrima.innerHTML = ""
			var Tab
					
			Tab = "<table border=0 width=96% cellspacing=0 cellpadding=0 height=70 align=center>"
			Tab = Tab + "<tr>"
			Tab = Tab + "	<td colspan=2 width=96% background=" + Progetto + "/images/titoli/strumenti1g.gif height=70 valign=bottom align=left>"
			Tab = Tab + "		<table border=0 background width=95% height=30 cellspacing=0 cellpadding=0>"
			Tab = Tab + "			<tr>"
			Tab = Tab + "				<td width=95% valign=bottom align=right>"
			Tab = Tab + "					<b CLASS=tbltext1a>Expediente del <%=tit1s%></b>"
			Tab = Tab + "				</td>"
			Tab = Tab + "			</tr>"
			Tab = Tab + "		</table>"
			Tab = Tab + "	</td>"
			Tab = Tab + "</tr>"
			Tab = Tab + "<tr>"
			Tab = Tab + "	<td align=right width=93%>"
			Tab = Tab + "		<b CLASS=tbltext1>Situaci�n al </b>&nbsp;"
			Tab = Tab + "		<b CLASS=textblack>" + Oggi + "</b>"
			Tab = Tab + "	</td>"
			Tab = Tab + "	<td>&nbsp;</td>"
			Tab = Tab + "</tr>"
			Tab = Tab + "</table>"
					
			lblDopo.innerHTML = Tab
		}
		function DopoDellaStampa(Progetto, Oggi)
		{
			//document.frmPulsanti.style.visibility='visible';
			lblDopo.innerHTML = ""
					
			var Tab
			Tab = Tab = "<table border='0' CELLPADDING='0' CELLSPACING='0' width='96%'>"
			Tab = Tab +	"<tr height='17'>"
			Tab = Tab +	"	<td class='sfondomenu' width='190px' height='18'><span class='tbltext0'>"
			Tab = Tab +	"		<b>&nbsp;Expediente del <%=tit1s%></b>"
			Tab = Tab +	"	</td>"
			Tab = Tab +	"	<td width='12px' background=" + Progetto + "/images/tondo_linguetta.gif>"
			Tab = Tab +	"	</td>"
			Tab = Tab +	"	<td valign='middle' align='right' width='350px' class='tbltext1' background='" + Progetto + "/images/sfondo_linguetta.gif'></td>"
			Tab = Tab +	"</tr>"
			Tab = Tab +	"<tr>"
			Tab = Tab +	"	<td class='sfondocommaz' width='100%' colspan='3'>"
			Tab = Tab +	"		Visualizacion de toda la informaci�n relativa a la sede del <%=tit1s%> seleccionada.<br>"
			Tab = Tab +	"		Presione <b>Cerrar</b> para salir."
			Tab = Tab +	"		Presione <b>Imprimir</b> para obtener una copia impresa."
			Tab = Tab +	"	</td>"
			Tab = Tab +	"</tr>"
			Tab = Tab +	"<tr>"	
			Tab = Tab +	"	<td class='sfondocommaz' colspan='3' align=right width='100%'>"
			Tab = Tab +	"		<input type=image align=right src='/images/help.gif' border='0' onclick=Show_Help('/pgm/help/Fasc_Azienda/FAS_SelCompleto') id=image1 name=image1>"
			Tab = Tab +	"	</td>"
			Tab = Tab +	"</tr>"
			Tab = Tab +	"<tr height=2>"
			Tab = Tab +	"	<td class=sfondocommaz width='730' colspan='3' background='" + Progetto + "/images/separazione.gif'>"
			Tab = Tab +	"	</td>"
			Tab = Tab +	"</tr>"
			Tab = Tab +	"<tr>"
			Tab = Tab +	"	<td align=right colspan=3>" 
			Tab = Tab +	"		&nbsp;"
			Tab = Tab +	"	</td>"
			Tab = Tab +	"</tr>"
			Tab = Tab +	"<tr>"
			Tab = Tab +	"	<td align=right width=730 colspan=3>" 
			Tab = Tab +	"		<b CLASS=tbltext1>Situaci�n al:</b><br>"
			Tab = Tab +	"		<b CLASS=textblack>" + Oggi + "</b>"
			Tab = Tab +	"	</td>"
			Tab = Tab +	"</tr>"
			Tab = Tab +	"</table>"
					
			lblPrima.innerHTML = Tab
					
			Pul = "<table width='500' cellspacing='2' cellpadding='1' border='0'>"
			Pul = Pul + "<tr align='center' height='50'>"
			Pul = Pul + "	<td>"
			Pul = Pul + "		<input type=image src='" + Progetto + "/images/chiudi.gif' title='Cerrar la p�gina' border='0' align='center' onclick='self.close();' id='Chiudi' name='Chiudi'>&nbsp;"
			Pul = Pul + "		<input type=image src='" + Progetto + "/images/stampa.gif' title='Imprimir la p�gina' border='0' align='center' onclick='self.print();' id='Stampa' name='Stampa'>"
			Pul = Pul + "	</td>"
			Pul = Pul + "</tr>"
			Pul = Pul + "</table>"
			
			lblPulsanti.innerHTML = Pul
		}
	</script>
	</head>
<%	
'********************************
'*********** MAIN ***************

'	if ValidateService(session("idutente"),"RIC_RICRICHIESTA",cc) <> "true" then 
'		response.redirect "/util/error_login.asp"
'	end if

' Dichiarazioni delle variabili necessarie per visualizzari i dati aziendali anagrafici
Dim RRAna, nAnaSede, sAnasql,RsSedesEmpresa
Dim sAnaDescr, sAnaIndirizzo, sAnaComune, sAnaProv, nAnaCap, nAnaTel, nAnaFax, sAnaEmail, sAnaRef
Dim nAnaIdImpresa 
Dim bDbAllineato
Dim sAnaRagSoc, sAnaForma, sAnaSettore, sAnaDtCost, nAnaCapSoc, sAnaValuta, sAnaAttivita
Dim nAnaInps, nAnaPartIva, sAnaCodFisc, sAnaDtCess, sAnaCessAtt, sAnaSitoWeb, sAnaTimpr, nAnaIscReg
dim sSqlSedes
dim sApellidoContacto,sNombreContacto,sEmailContacto,sDescriDocContacto,sNroDocContacto,sDescriCargoContacto
'''''nAnaSede= Request("Rif")
nAnaIdImpresa = Request("Impresa")

%>

<%

'''''<!--#include virtual="/Pgm/Fasc_Azienda/FAS_AnaEstraiDati.asp"-->
Inizio()

'if bDbAllineato then	
'	Intestazione()
'	DatiSede()
'	DatiImpresa()
'	SituazioneImpresa()
'	Richieste()
'	Bacheca()
'else
'	Messaggio("Pagina momentaneamente no disponible.")
'end if
bDbAllineato=true
if bDbAllineato then	
	%>
		<!--#include virtual="/Pgm/Fasc_Azienda/FAS_AnaEstraiDatiImpresa.asp"-->
	<%	
	
	DatiImpresa()
	
	
	'''Intestazione()
	sSqlSedes="SELECT Id_Impresa,Id_sede from Sede_impresa WHERE ID_Impresa=" & nAnaIdImpresa
'PL-SQL * T-SQL  
SSQLSEDES = TransformPLSQLToTSQL (SSQLSEDES) 
	set RsSedesEmpresa = CC.Execute(sSqlSedes)
	
	do while not RsSedesEmpresa.eof
		nAnaSede= RsSedesEmpresa("Id_Sede")
		
		%>
		<!--#include virtual="/Pgm/Fasc_Azienda/FAS_AnaEstraiDati.asp"-->
		<%	
		DatiSede()
		DatosContactoSede()
		''DatiImpresa()
		SituazioneImpresa()
		Richieste()
		Bacheca()
		RsSedesEmpresa.movenext
	loop
		'fuori loop perch� riporta le sedi iscritte ai progetti tutte insieme
		IntestaPrj()%>
<!--#include virtual="/Pgm/Fasc_Azienda/FAS_EstraiTrayecto.asp"-->	
<%else
	Messaggio("Pagina momentaneamente no disponible.")
end if
RsSedesEmpresa.close
set RsSedesEmpresa=nothing
Fine()
	
'********************************
'********************************
'**********************************************************************************************************************************************************************************	
	Sub Inizio()
%>	
		<!--#include virtual="/Pgm/Fasc_Azienda/FAS_Inizio.asp"-->			
<% 
	End Sub
'**********************************************************************************************************************************************************************************
	Sub Intestazione()
%>
		<!--#include virtual="/Pgm/Fasc_Azienda/FAS_AnaIntestazione.asp"-->
<%	End Sub
'**********************************************************************************************************************************************************************************
	Sub DatiSede()
%>	
		<!--#include virtual="/Pgm/Fasc_Azienda/FAS_AnaVisualizzaDatiSede.asp"-->
<%
	End Sub	
'**********************************************************************************************************************************************************************************
	Sub DatosContactoSede()
%>	
		<!--#include virtual="/Pgm/Fasc_Azienda/FAS_AnaVisualizzaDatosContactoSede.asp"-->
<%
	End Sub
	
	Sub DatiImpresa()	
%>
		<!--#include virtual="/Pgm/Fasc_Azienda/FAS_AnaVisualizzaDatiImpresa.asp"-->
<%	End Sub
'**********************************************************************************************************************************************************************************
	Sub SituazioneImpresa()
%>		
		<!--#include virtual="/Pgm/Fasc_Azienda/FAS_AnaVisualizzaSituazione.asp"-->	
<%	End Sub
'**********************************************************************************************************************************************************************************
	Sub IntestaPrj()
%>
		<br>
		<table width="96%%" border="0" CELLPADDING="0" CELLSPACING="0">
			<tr height="17">
				<td class="sfondomenu" width="190" height="18"><span class="tbltext0">
					<b>&nbsp;TRAYECTO EN EL PROGRAMA</b>
				</td>
				<td width="10" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">
				</td>
				<td valign="middle" align="right" class="tbltext1" width="298" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
			</tr>
		</table>
<%	End Sub 
'**********************************************************************************************************************************************************************************
	Sub Richieste()
%>
		<br>
		<table width="96%%" border="0" CELLPADDING="0" CELLSPACING="0">
			<tr height="17">
				<td class="sfondomenu" width="190" height="18"><span class="tbltext0">
					<b>&nbsp;SELECCIONES&nbsp;-&nbsp;SEDE &nbsp;<%=sAnaDescr%></b>
				</td>
				<td width="10" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">
				</td>
				<td valign="middle" align="right" class="tbltext1" width="298" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
			</tr>
		</table>
		<!--#include Virtual = "/pgm/Fasc_Azienda/FAS_RicVisualizzaRichieste.asp"-->
<%	End Sub 
'**********************************************************************************************************************************************************************************
	Sub Bacheca()
%>
		<br>
		<table width="96%%" border="0" CELLPADDING="0" CELLSPACING="0">
			<tr height="17">
				<td class="sfondomenu" width="190" height="18"><span class="tbltext0">
					<b>&nbsp;AVISOS&nbsp;-&nbsp;SEDE &nbsp;<%=sAnaDescr%></b>
				</td>
				<td width="10" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">
				</td>
				<td valign="middle" align="right" class="tbltext1" width="298" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
			</tr>
		</table>
		<!--#include Virtual = "/pgm/Fasc_Azienda/FAS_BacVisualizzaAnnunci.asp"-->
<%	End Sub 
'**********************************************************************************************************************************************************************************	
	Sub Fine()
%>
		<br>
		<label name="lblPulsanti" id="lblPulsanti"></label>
		<br>
		</body>
		</center>
		</html>
<%	
End Sub 
'**********************************************************************************************************************************************************************************
	Sub Messaggio(Testo)
%>		
		<br><br>
		<table width="743" align="center" border="0" cellspacing="2" cellpadding="1">
			<tr height="20"> 
			  	<td class="tbltext1" align="middle" width="743">
					<b><%=Testo%></b>
				</td>
			</tr>
		</table>
		<br><br>
<%				
	End Sub
%>
<!--#include virtual="/Pgm/Fasc_Azienda/FAS_SubEstraiTrayecto.asp"-->	

<!--#include Virtual ="/Include/closeconn.asp"-->
	
