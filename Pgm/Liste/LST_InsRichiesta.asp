<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include virtual="/util/portallib.asp"-->
<!--#include Virtual = "/strutt_testa2.asp"-->
<!--#include Virtual = "/include/SysFunction.asp"-->
<!--#include Virtual ="/Include/openconn.asp"-->
<!--#include Virtual ="/Include/ControlDateVB.asp"-->
<!--#include Virtual = "/include/DecCod.asp"-->

<script LANGUAGE="Javascript">
//include del file per fare i controlli sulla validit� delle date
<!--#include virtual = "/Include/ControlDate.inc"-->

//include del file per fare i controlli sulla numericit� dei campi
<!--#include virtual = "/Include/ControlNum.inc"-->
<!--#include virtual = "/Include/help.inc"-->
<!-- #Include Virtual="/Include/ControlString.inc" -->

var windowSpiega

function Indietro() {	
	if (windowSpiega != null) {
		windowSpiega.close();	
	}
}

function Ricarica() {			
	if (windowSpiega != null) {
		windowSpiega.close();
	}
	//alert ("1-" + document.FrmPrincipale.cmbTipoArea.value);
	aAreaVal = document.FrmPrincipale.cmbTipoArea.value.split("|")
	nIdAreaVal=aAreaVal[0]
	document.FrmRicarica.Area.value = nIdAreaVal;
	document.FrmRicarica.submit();
	return true;
}

function Spiega() {	
	var nId
	var aValore
	
	if (document.FrmPrincipale.cmbTipoProf.value == "") {
		alert("Indicare una figura professionale");
		document.FrmPrincipale.cmbTipoProf.focus();
	}else{
		aValore = document.FrmPrincipale.cmbTipoProf.value.split("|")
		nId = aValore[0]
		document.FrmSpiega.id_figprof.value = nId;
		document.FrmSpiega.submit();
	}
}	 	

//inizio Am 2/12/04
/*function Validate(TheForm) {
	if (TheForm.cmbTipoArea.value == "" && TheForm.cmbTipoProf.value == "" && TheForm.cmbCittadinanza.value == ""){
		alert("Indicare almeno un criterio di ricerca");
		TheForm.cmbTipoArea.focus();
		return false;
	}
	return true;
}
*/
//Fine Am 2/12/04
		
function ApriFigProf() {
	document.FrmPrincipale.CriterioRicerca.value=TRIM(document.FrmPrincipale.CriterioRicerca.value);
   if (TRIM(document.FrmPrincipale.CriterioRicerca.value)== "") {
 		alert("Inserire un ruolo o affidarsi alla ricerca per Area professionale.")
 		document.FrmPrincipale.CriterioRicerca.focus()
 		return;
 	}
	if (!ValidateInputStringWithNumber(document.FrmPrincipale.CriterioRicerca.value)) {
		alert("Il criterio di ricerca nel campo Ruolo Ricoperto � formalmente errato")
		document.FrmPrincipale.CriterioRicerca.focus() 
		return;
	}
				
	CriterioRicerca = FrmPrincipale.CriterioRicerca.value;
		
	URL = "LST_RicFigProf.asp?VISMENU=NO" + "&RICERCA="+ CriterioRicerca 
	opt = "address=no,status=no,width=550,height=280,top=150,left=15"
	window.open (URL,"",opt)
}
</script>

<script language="javascript" src="Controlli.js">
</script>

<%
'********************************
'*********** MAIN ***************

'	if ValidateService(session("idutente"),"RIC_RICRICHIESTA",cc) <> "true" then 
'		response.redirect "/util/error_login.asp"
'	end if

	Dim sIdSede
	
	sIdsede=Request("Idsede")

	if sIdSede = "" then
		sIdSede = Session("Creator")
	end if
	
	Inizio()
	Imposta_Pag()
	
'**********************************************************************************************************************************************************************************		
	Sub Imposta_Pag()

		Dim sArea
		Dim sqlA, RRA
		Dim sqlP, RRP
		Dim sTespr
		
		sArea = Request("Area")
		
		'Response.Write ("Area " & sArea & "<br>")
		
		if not isnumeric(sArea) then
			sArea=0
		end if
		
		if isnumeric(Request("Figura")) then 
			sFigura = Request("Figura")
		else
			sFigura=0
		end if

		sqlA = "Select id_areaprof, denominazione from aree_professionali a," &_
			   " validazione b where a.id_valid=b.id_valid and fl_valid=1  order by denominazione"
		
		set RRA=Server.CreateObject("ADODB.Recordset")
		
'PL-SQL * T-SQL  
SQLA = TransformPLSQLToTSQL (SQLA) 
		RRA.Open sqlA, CC,1,2
		
		if RRA.EOF then 
			Messaggio("Non esistono Aree Professionali validate")
			exit sub
		end if
		%>
		<form method="post" name="FrmRicarica" action="LST_InsRichiesta.asp">
			<input type="hidden" name="Area" value>
			<input type="hidden" name="Figura" value>
			<input type="hidden" name="idSede" value="<%=sIdSede%>">
		</form>
		<form method="post" name="FrmSpiega" target="_New" action="/pgm/bilanciocompetenze/DettagliArea/Focusfigprof.asp">
			<input type="hidden" name="VisMenu" value="NO">
			<input type="hidden" name="id_figprof" value>
		</form>
		<!--form method="post" name="FrmPrincipale" action="LST_ElencoCandidati.asp" onsubmit="return Validate(this)"-->
		<form method="post" name="FrmPrincipale" action="LST_ElencoCandidati.asp">
			<input type="hidden" name="idSede" value="<%=sIdSede%>">
			<table WIDTH="500" BORDER="0" CELLPADDING="2" CELLSPACING="2">
				<!--Inizio Am -->
				<tr>
					<!--td class="tbltext1">						<b></b>					</td-->
					<td colspan="2" class="textblack" align="center"><b>Prioritari <input checked name="chkPriori" type="radio" value="S">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Disponibili <input name="chkPriori" type="radio" value="N">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Origine Italiana <input name="chkPriori" type="radio" value="I"></b></td>			
				</tr>
				<!--Fine Am -->
				<tr>
					<td class="tbltext1" valign="bottom" width="170">
						<b>Ruolo ricoperto</b>
					</td>
					<td WIDTH="330" colspan="2">
						<input style="TEXT-TRANSFORM: uppercase; width:250px;" type="text" class="textblack" size="40" maxlength="50" name="CriterioRicerca">
						<a href="javascript:ApriFigProf()">
							<img src="<%=Session("Progetto")%>/images/lente.gif" border="0">
						</a>		
					</td>
				</tr>
				<tr>
			        <td class="tbltext1" width="170">
						<b>Area Professionale</b>
					</td>
					<td class="textblack" ALIGN="LEFT" WIDTH="330" colspan="2"> 
						<select name="cmbTipoArea" class="textblack" onchange="javascript:return Ricarica()">						
							<option value></option>
							<%do until RRA.EOF
								if sArea <> ""  then	
									if clng(sArea) = clng(RRA.Fields("ID_AREAPROF")) then		
										Response.Write "<option value=""" & clng(RRA.Fields("ID_AREAPROF")) & "|" & ucase(RRA.Fields("Denominazione")) & """ selected>" & ucase(RRA.Fields("Denominazione")) & "</option>"
									else
										Response.Write "<option value=""" & clng(RRA.Fields("ID_AREAPROF")) & "|" & ucase(RRA.Fields("Denominazione")) & """>" & ucase(RRA.Fields("Denominazione")) & "</option>"				
									end if
								else
									Response.Write "<option value=" & clng(RRA.Fields("ID_AREAPROF")) & ">" & ucase(RRA.Fields("Denominazione")) & "</option>"	
								end if
								RRA.MoveNext
							loop
							RRA.Close
							set RRA =nothing
							sqlA =""%>					
						</select>
					</td>
				</tr>
				<%
				sqlP =	"Select id_figprof, denominazione" &_
						" from figureprofessionali a,validazione b " &_
						" where a.id_valid = b.id_valid"
						
						'if sArea > "" then
							sqlP = sqlP & " and id_areaprof=" & clng(sArea)
						'end if
						   
						sqlP = sqlP & " and fl_valid = 1 order by denominazione"
		
				set RRP=Server.CreateObject("ADODB.Recordset")
'PL-SQL * T-SQL  
SQLP = TransformPLSQLToTSQL (SQLP) 
				RRP.Open sqlP, CC, 1, 2
				%>	
				<tr>
			        <td class="tbltext1" width="170">
						<b>Profilo professionale</b>
					</td>
					<td class="textblack" width="320" align="left"> 
						<select name="cmbTipoProf" style="WIDTH: 380px" class="textblack">
							<option value></option>
							<%
							Do Until RRP.EOF
								If clng(sFigura)=clng(RRP.Fields("ID_FIGPROF")) Then%>
									<option value="<%=clng(RRP.Fields("ID_FIGPROF")) & "|" & RRP.Fields("Denominazione")%>" selected><%=RRP.Fields("Denominazione")%></option>
								<%Else%>
									<option value="<%=clng(RRP.Fields("ID_FIGPROF")) & "|" & RRP.Fields("Denominazione")%>"><%=RRP.Fields("Denominazione")%></option>
								<%End if
								RRP.MoveNext
							Loop
							RRP.Close
							set RRP =nothing
							sqlP =""%>
						</select>
					</td>
					<td width="10">	
						<a href="Javascript:Spiega()">
							<img border="0" src="<%=Session("Progetto")%>/images/bullet1.gif" alt="Descrizione del profilo professionale">
						</a>
					</td>
				</tr>
			    <tr>
			        <td class="tbltext1" width="170">
						<b>Cittadinanza</b>
					</td>
					<td class="textblack" colspan="2"> 
						<%
						sInt = "STATO|0|" & date & "|" & "" & "|cmbCittadinanza|ORDER BY DESCRIZIONE"
						CreateCombo(sInt)
						%>
					</td>
				</tr>	
				
			</table>
			<br>
			<table width="500" cellspacing="2" cellpadding="1" border="0">
				<tr align="center">
					<td>
						<input type="image" src="<%=Session("progetto")%>/images/conferma.gif" title="Conferma richiesta" name="submit" border="0" align="absBottom">
					</td>
					<td>
						<a href="javascript:history.back()">
							<img src="<%=Session("progetto")%>/images/Indietro.gif" title="Pagina precedente" border="0" align="absBottom" onclick="javascript:Indietro()">
						</a>
					</td>
				</tr>
			</table>
		</form>
		<script>
			document.FrmPrincipale.elements[2].focus();
		</script>
<%	End Sub 
'**********************************************************************************************************************************************************************************	
	Sub Messaggio(Testo)
%>		
		<br><br>
		<table width="500" align="center" border="0" cellspacing="2" cellpadding="1">
			<tr height="20"> 
			  	<td class="tbltext3" align="middle" width="500">
					<b><%=Testo%></b>
				</td>
			</tr>
		</table>
		<br><br>
<%				
	End Sub
'**********************************************************************************************************************************************************************************	
	Sub Inizio()
%>
	<table border="0" width="520" cellspacing="0" cellpadding="0" height="50">
		<tr><td>&nbsp;</td></tr>
		<tr>
			<!--td width="500" background="<%'=session("progetto")%>/images/titoli/servizi2g.gif" height="70" valign="bottom" align="right">				<table border="0" background width="500" height="23" cellspacing="0" cellpadding="0">					<tr-->
						<td width="100%" valign="top" align="right">
							<b CLASS="tbltext1a">Liste</b>
						</td>
					<!--/tr>				</table>			</td-->
		</tr>
	</table> 
	<!-- Lingetta superiore -->
	<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
		<tr height="18">
			<td class="sfondomenu" width="67%" height="18"><span class="tbltext0"><b>&nbsp;LISTE </b></span></td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
			<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
		</tr>
	</table>
	<!-- Commento -->
	<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
		<tr>
			<td align="left" class="sfondocommaz">
				Definire i criteri per la ricerca e premere <b>Invia</b>.  
				<a href="Javascript:Show_Help('/Pgm/help/Liste/LST_InsRichiesta')"><img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
			</td>
		</tr>
		<tr height="2">
			<td class="sfondocommaz" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>
<%
End Sub
%>

<!--#include Virtual ="/Include/closeconn.asp"-->
<!--#include Virtual="/strutt_coda2.asp"-->		
