<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #include virtual="/strutt_testa2.asp" -->
<!-- #include virtual="/include/openconn.asp" -->
<!-- #include virtual="/util/portallib.asp" -->
<!-- #include virtual="/util/globalsub.asp" -->
<%
If Not ValidateService(Session("IdUtente"),"Gestione Funzioni",cc) Then 
	Response.Redirect "/util/error_login.asp"
End If

	Session("ck_idfunzione")=""
	Session("ck_desfunzione")=""
%>
<script>
<!--
	function checkField(frm){
		if (frm.idfunzione.value=='0') {
			alert("E' necessario selezionare una funzione");
			frm.idfunzione.focus();
			return false;
		}
		i = frm.idfunzione.selectedIndex;
		valore = frm.idfunzione.options[i].text;
		frm.desfunzione.value=valore;
		return true;
	}
//-->
</script>
</head>
<body onload="document.funzione.idfunzione.focus();">
<br>
<TABLE border="0" CELLPADDING="0" CELLSPACING="0" width="500">
	<tr height="18">
		<td class="sfondomenu" width="67%" height="18"><span class="tbltext0"><b>&nbsp;AMMINISTRAZIONE PORTALE - Cancellazione Funzione</b></span></td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
		<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
	</tr>
	<tr height=17>
		<td class="sfondocomm" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
	</tr>
</TABLE>
<br> 
<form method="GET" action="pag1fun.asp" name="funzione" onsubmit="return checkField(this);">
<TABLE border="0" width="500" class="sfondocomm">
	<tr height="30" >
		<td width="480" align=left valign="middle">&nbsp;<b>Ricerca funzione da cancellare</b></td>
		<td align="right" valign="middle"><IMG src="<%=Session("Progetto")%>/images/help.gif" border=0></td>  
	</tr>   
</TABLE>
<br>
<TABLE border="0" cellpadding="2" cellspacing="2" width="500">
	<tr>
		<td height="25" class="tbltext1" align="left">&nbsp;Descrizione funzione</td>
		<td>  
   			<select name='idfunzione'>
   			<option value='0'></option>
<%   
			SqlStr = "SELECT idfunzione,desfunzione FROM funzione order by 2"
'PL-SQL * T-SQL  
UCASESQLSTR = TransformPLSQLToTSQL (UCASESQLSTR) 
			Set rs = CC.Execute(ucase(SqlStr))
			rs.MoveFirst
			While rs.EOF <> True 
			 	response.write "<option "
			 	if Session("ck_idfunzione")=FixDbField(rs.fields(0)) then 
			 		response.write "selected "
			 	end if
				response.write "value='" & rs.fields(0) & "'"
				response.write ">" & rs.fields(1) & "</option>"
			  rs.MoveNext
			Wend
			rs.close
			set rs=nothing
%>   
  			</select> 
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td colspan=2 align=center>
			<input type="hidden" name="desfunzione" value="">
			<input type="image" alt="Invia" border="0" name="Invia" src="<%=Session("Progetto")%>/images/conferma.gif">
		</td>
	</tr>	
</TABLE>
</FORM>

<!-- #include virtual="/include/closeconn.asp" -->
<!-- #include virtual="/strutt_coda2.asp" -->
