// -----------------------------------------------------------------------------
// Compara dos fechas
// -----------------------------------------------------------------------------

function compararfecha(fecha1, fecha2, tipo)
{
  var dia1;
  var mes1;
  var ano1;
  var dia2;
  var mes2;
  var ano2;
  var newfecha1;
  var newfecha2;
  var respuesta;

  dia1=fecha1.substr(0,2);
  mes1=fecha1.substr(3,2);
  ano1=fecha1.substr(6,4);
  dia2=fecha2.substr(0,2);
  mes2=fecha2.substr(3,2);
  ano2=fecha2.substr(6,4);

  newfecha1=parseInt(ano1 + mes1 + dia1);
  newfecha2=parseInt(ano2 + mes2 + dia2);

  respuesta=false;

  if ( tipo == '=' )
  {
    if ( newfecha1 == newfecha2 ) {respuesta=true;}
  }

  if ( tipo == '>' )
  {
      if ( newfecha1 > newfecha2 ) {respuesta=true;}
  }

  if ( tipo == '<' )
  {
        if ( newfecha1 < newfecha2 ) {respuesta=true;}
  }

  if ( tipo == '>=' )
  {

        if ( newfecha1 >= newfecha2 ) {respuesta=true;}
  }

  if ( tipo == '<=' )
  {
         if ( newfecha1 <= newfecha2 ) {respuesta=true;}
  }

  return respuesta;
}
//-----------------------------------------------------------------------
// Comprueba que la FECHA este comprendida entre FECHA1 y FECHA2
// -------------------------------------------------------------

function entrefechas(fecha, fecha1, fecha2)
{
  var respuesta;
  respuesta=false;
  if (compararfecha(fecha, fecha1, '>=') && compararfecha(fecha, fecha2, '<='))
  { respuesta=true; }
  return respuesta; 
}
// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------

//------------------------------------------------------------------------------

   function pasar_formato( fecha )
   {
       var i;
       var j;
       var d;
       var m;
       var y;
       var dd;
       var mm;
       var yyyy;
       var tmp;
       n_dd = new Number();
       n_mm = new Number();
       n_yyyy = new Number();
       fecha_actual = new Date(); 
       var fecha_retorno;
     
       fecha_retorno = '';

       barra = new RegExp( "[/]" );

       i = fecha.value.search(barra);

       if ( i == -1 )
       {
          /***********************************/
          /* Posible formato  'DD MM'        */
          /***********************************/
          i = fecha.value.search(' ');
          if ( i == -1 )
          {
              /* Error: No es el formato correcto */
              fecha_retorno = '';
              return( fecha_retorno );             
          }
          dd = fecha.value.substr(0,i);
          n_dd = dd;
          if ( isNaN( n_dd ) )
          {
              /* Error: dia incorrecto */
              fecha_retorno = '';
              return( fecha_retorno );             
          }
          mm = fecha.value.substr(i+1,fecha.value.length);
          n_mm = mm
          if ( isNaN( n_mm ) )
          {
              /* Error: mes incorrecto */
              fecha_retorno = '';
              return( fecha_retorno );             
          }
          yyyy = fecha_actual.getYear();
          n_yyyy = yyyy;
          if( dd.length == 1 )
          {
              d = '0'+dd;
          }
          else
          {
              d = dd;
          }
          if( mm.length == 1 )
          {
              m = '0'+mm; 
          }
          else
          {
              m = mm;
          }   

          fecha_retorno = d+'/'+m+'/'+yyyy;
       } 
       else
       {
          /* posibles formatos DD/MM o DD/MM/YYYY */
          tmp = fecha.value.substr(i+1,fecha.value.length);

          j = tmp.search("[/]");

          if ( j == -1 )
          {
             /* posible formato DD/MM */
             dd = fecha.value.substr(0,i);
             n_dd = dd;
             if ( isNaN( n_dd ) )
             {
                 /* Error: dia incorrecto */
                 fecha_retorno = '';
                 return( fecha_retorno );             
             }
             mm = fecha.value.substr(i+1,fecha.value.length);
             n_mm  
             if ( isNaN( n_mm ) )
             {
                 /* Error: mes incorrecto */
                 fecha_retorno = '';
                 return( fecha_retorno );             
             }
             yyyy = fecha_actual.getYear();
             if( dd.length == 1 )
             {
                 d = '0'+dd;
             }
             else
             {
                 d = dd;
             }
             if( mm.length == 1 )
             {
                 m = '0'+mm; 
             }
             else
             {
                 m = mm;
             }   

             fecha_retorno = d+'/'+m+'/'+yyyy;
          }
          else
          {
             /* posible formato DD/MM/YYYY */
             dd = fecha.value.substr(0,i);
             n_dd = dd;
             if ( isNaN( n_dd ) )
             {
                 /* Error: dia incorrecto */
                 fecha_retorno = '';
                 return( fecha_retorno );             
             }
             mm = fecha.value.substr(i+1,j);
             n_mm = mm;
             if ( isNaN( n_mm ) )
             {
                 /* Error: mes incorrecto */
                 fecha_retorno = '';
                 return( fecha_retorno );             
             }
             yyyy = fecha.value.substr(i+j+2,fecha.value.length);
             n_yyyy = yyyy;
             if ( isNaN( n_yyyy ) )
             {
                 /* Error: a�o incorrecto */
                 fecha_retorno = '';
                 return( fecha_retorno );             
             }
             if( dd.length == 1 )
             {
                 d = '0'+dd;
             }
             else
             {
                 d = dd;
             }
             if( mm.length == 1 )
             {
                 m = '0'+mm; 
             }
             else
             {
                 m = mm;
             }   

             fecha_retorno = d+'/'+m+'/'+yyyy;
          } 
       }   
       return( fecha_retorno );             
   }

//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Valida si el campo contiene una fecha valida
//------------------------------------------------------------------------------
   function EsFecha( fecha )
   {
       var a;
       var i;

       a = pasar_formato( fecha );

       if ( a == "" )
       {

          // Para dar el error en el caso de que la funci�n pasar_formato nos devuelva
          // una cadena vacia pero sea una fecha inv�lida.
          if (fecha.value != "")
          {
            alert( "La data non � corretta.");
            fecha.focus();
            fecha.select();
            return false;
          }
/*           fecha.value = ''; */
       }
       i = comprobar_fecha( a );

       if ( i == 1 )
       {
          fecha.value = a;
       }
       else if ( fecha.value.length > 0 )
       {
          alert( "La data non � corretta. ");
		  fecha.focus();
          fecha.select();
          return false;
       }
       else
       {
          return false;
       }
        
   return true;
   }


//------------------------------------------------------------------------------
   function comprobar_fecha( fecha )
   {
       var ret;
       n_d = new Number();
       n_m = new Number();
       n_y = new Number();

       n_d = fecha.substr(0,2);
       n_m = fecha.substr(3,2);
       n_y = fecha.substr(6,4);

	   if ( fecha.length != 10 )
       {
	   		return( 0 );	 
       }

       if ( n_m==1 || n_m==3 || n_m==5 || n_m==7 || n_m==8 || n_m==10 || n_m == 12 )
       {
           if ( n_d >0 && n_d <=31 )
           {
               return( 1 );
           }
           else
           {
               return( 0 );
           }
       }
       else if ( n_m==4 || n_m==6 || n_m==9 || n_m==11 )
       {
           if ( n_d >0 && n_d <=30 )
           {
               return( 1 );
           }
           else
           {
               return( 0 );
           }
       }
       else if ( n_m==2 )
       {
           if ( (n_y%4?0:(n_y%100?1:(n_y%400?0:1))) )
           {
              /* el a�o es bisiesto */
              if ( n_d>0 && n_d<=29 )
              {
                  return( 1 );
              }
              else
              {
                  return( 0 );
              }
           }
           else
           {
              if ( n_d >0 && n_d <=28 )
              {
                  return( 1 );
              }
              else
              {
                  return( 0 );
              }
           }
       }
       else
       {
            return( 0 );
       }
   }
