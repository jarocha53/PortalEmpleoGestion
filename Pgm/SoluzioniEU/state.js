
var Prov = new Array("Pescara","Chieti","L'Aquila","Teramo","Matera","Potenza","Catanzaro","Cosenza","Crotone","Reggio Calabria","Vibo Valentia","Avellino","Benevento","Caserta","Napoli","Salerno","Bologna","Ferrara","Forl�","Modena","Parma","Piacenza","Ravenna","Reggio Emilia","Rimini","Gorizia","Pordenone","Trieste","Udine","Frosinone","Latina","Rieti","Roma","Viterbo","Genova","Imperia","La Spezia","Savona","Bergamo","Brescia","Como","Cremona","Mantova","Milano","Lecco","Lodi","Pavia","Sondrio","Varese","Ancona","Ascoli Piceno","Macerata","Pesaro-Urbino","Campobasso","Isernia","Alessandria","Asti","Biella","Cuneo","Novara","Torino","Verbania","Vercelli","Bari","Brindisi","Foggia","Lecce","Taranto","Cagliari","Nuoro","Oristano","Sassari","Agrigento","Caltanissetta","Catania","Enna","Messina","Palermo","Ragusa","Siracusa","Trapani","Arezzo","Firenze","Grosseto","Livorno","Lucca","Massa Carrara","Pisa","Pistoia","Prato","Siena","Bolzano","Trento","Perugia","Terni","Aosta","Belluno","Padova","Rovigo","Treviso","Venezia","Verona","Vicenza");
var Reg = new Array("1:103","1:4","5:6","7:11","12:16","17:25","26:29","30:34","35:38","39:49","50:53","54:55","56:63","64:68","69:72","73:81","82:91","92:93","94:95","96:96","97:103");
var Strut = new Array("SPI Teramo","CPI Giulianova","SPI Potenza","SPI Genova","SPI Savona","SPI Lodi","CPI Asti","CPI Cuneo","SPI Lecce","CPI Castrano","CPI Nardo'","SPI Massa","SPI Belluno","CPI Belluno","CPI Agordo","CPI Calalo","CPI Feltre","SPI Padova","CPI Padova","CPI Camposampiero","CPI Este","CPI Monselice","CPI Piovedisacco","SPI Vicenza","CPI Vicenza","CPI Arzignano","CPI Bassano del Grappa");
var IdStrut1 = new Array("1:5","0:0","0:0","0:0","1:2")
var IdStrut2 = new Array("1:3","0:0","3:3")
var IdStrut3 = new Array("1:6","0:0","0:0","0:0","0:0","0:0")
var IdStrut4 = new Array("1:6","0:0","0:0","0:0","0:0","0:0")
var IdStrut5 = new Array("1:10","0:0","0:0","0:0","0:0","0:0","0:0","0:0","0:0","0:0")
var IdStrut6 = new Array("1:5","0:0","0:0","0:0","0:0")
var IdStrut7 = new Array("1:6","0:0","0:0","0:0","0:0","0:0")
var IdStrut8 = new Array("1:5","4:4","0:0","0:0","5:5")
var IdStrut9 = new Array("1:12","0:0","0:0","0:0","0:0","0:0","0:0","0:0","6:6","0:0","0:0","0:0")
var IdStrut10 = new Array("1:5","0:0","0:0","0:0","0:0")
var IdStrut11 = new Array("1:3","0:0","0:0")
var IdStrut12 = new Array("1:9","7:7","0:0","8:8","0:0","0:0","0:0","0:0","0:0")
var IdStrut13 = new Array("1:6","0:0","0:0","9:11","0:0","0:0")
var IdStrut14 = new Array("1:5","0:0","0:0","0:0","0:0")
var IdStrut15 = new Array("0:0","0:0","0:0","0:0","0:0","0:0","0:0","0:0","0:0")
var IdStru16 = new Array("1:11","0:0","0:0","0:0","0:0","12:12","0:0","0:0","0:0","0:0","0:0")
var IdStrut17 = new Array("1:3","0:0","0:0")
var IdStrut18 = new Array("1:3","0:0","0:0")
var IdStrut18 = new Array("1:2","0:0")
var IdStrut20 = new Array("1:8","13:17","18:23","0:0","0:0","0:0","0:0","24:27");

var selected_prov, set_reg, selected_strutt;

function SetProv(formObj1,formObj2) {
	document.search.ss.disabled = false
	var c = Reg[formObj2.selectedIndex].split(":");
	var f = parseInt(c[0]) - 1;
	var t = parseInt(c[1]) - 1;
	var prov_index = 0;
	formObj1.length = t - f + 2;
	formObj1.options[0].text = "Tutte le provincie";
	var SortProv = new Array();
	for (var i = f; i <= t; i++)
		SortProv[i-f] = Prov[i];
	SortProv.sort();
	for (var i = f; i <= t; i++) {
		formObj1.options[i-f+1].text = SortProv[i-f];
		if (SortProv[i-f] == selected_prov)
			prov_index = i-f+1;
	}
	formObj1.options[prov_index].selected = true;
	if (formObj2.value == "_all") {
		formObj1.selectedIndex = 0
		search.ss.selectedIndex = 0
	}
}

function SetReg(formObj1,formObj2) {
	document.search.ss.disabled = false
	if (formObj2.value == "_all") {
		alert("Non si puo selezionare una provincia\nsenza scegliere la regione.");
		formObj1.selectedIndex = 0;
		formObj2.focus ();
	}
	if (!set_reg) return;
	if ( formObj2.options[formObj2.selectedIndex].text == 'Tutte' ) {
		for (var i=0; i<Prov.length; i++) {
			if ( formObj1.options[formObj1.selectedIndex].text == Prov[i] ) {
				for (j=1; j<Reg.length; j++) {
					var c = Reg[j].split(":");
					if ( i>=(parseInt(c[0])-1) & i<=(parseInt(c[1])-1) ) {
						formObj2.options[j].selected = true;
						break;
					}
				}
				SetProv(formObj1,formObj2);
			}      
		}
	}
}

function CtrlStrutt(formObj1,formObj2,formObj3) {
	if ((formObj2.value == "_all") || (formObj3.value == "_all")) {
		alert("Non si puo selezionare una struttura\nsenza scegliere la regione e la provincia.");
		formObj1.selectedIndex = 0
		formObj2.focus ();
	}
}

function SetStrutt(formObj1,formObj2) {
	indregio = document.search.r.value
	if (indregio == "abruzzo") {
		var c = IdStrut1[formObj2.selectedIndex].split(":");
	}
	if (indregio == "basilicata") {
		var c = IdStrut2[formObj2.selectedIndex].split(":");
	}
	if (indregio == "calabria") {
			var c = IdStrut3[formObj2.selectedIndex].split(":");
	}
	if (indregio == "campania") {
			var c = IdStrut4[formObj2.selectedIndex].split(":");
	}
	if (indregio =="emilia_romagna") {
			var c = IdStrut5[formObj2.selectedIndex].split(":");
	}
	if (indregio == "friuli_venezia_giulia") {
			var c = IdStrut6[formObj2.selectedIndex].split(":");
	}
	if (indregio == "lazio") {
			var c = IdStrut7[formObj2.selectedIndex].split(":");
	}
	if (indregio == "liguria") {
			var c = IdStrut8[formObj2.selectedIndex].split(":");
	}
	if (indregio == "lombardia") {
			var c = IdStrut9[formObj2.selectedIndex].split(":");
	}
	if (indregio == "marche") {
			var c = IdStrut10[formObj2.selectedIndex].split(":");
	}
	if (indregio == "molise") {
			var c = IdStrut11[formObj2.selectedIndex].split(":");
	}
	if (indregio == "piemonte") {
			var c = IdStrut12[formObj2.selectedIndex].split(":");
	}
	if (indregio == "puglia") {
			var c = IdStrut13[formObj2.selectedIndex].split(":");
	}
	if (indregio == "sardegna") {
			var c = IdStrut14[formObj2.selectedIndex].split(":");
	}
	if (indregio == "sicilia") {
			var c = IdStrut15[formObj2.selectedIndex].split(":");
	}
	if (indregio == "toscana") {
			var c = IdStrut16[formObj2.selectedIndex].split(":");
	}
	if (indregio == "trentino_alto_adige") {
			var c = IdStrut17[formObj2.selectedIndex].split(":");
	}
	if (indregio == "umbria") {
			var c = IdStrut18[formObj2.selectedIndex].split(":");
	}
	if (indregio == "valle_aosta") {
			var c = IdStrut19[formObj2.selectedIndex].split(":");
	}
	if (indregio == "veneto") {
			var c = IdStrut20[formObj2.selectedIndex].split(":");
	}
	var f = parseInt(c[0]) - 1;
	var t = parseInt(c[1]) - 1;
	var prov_index = 0;
	formObj1.length = t - f + 2;
	formObj1.options[0].text = "Tutte le strutture";
	var SortProv = new Array();
	if (c[0]!= 0) {
		for (var i = f; i <= t; i++)
			SortProv[i-f] = Strut[i];
		SortProv.sort();
		for (var i = f; i <= t; i++) {
			formObj1.options[i-f+1].text = SortProv[i-f];
			if (SortProv[i-f] == selected_prov)
				prov_index = i-f+1;
		}
	
		formObj1.options[prov_index].selected = true;
		
		if (formObj2.value == "_all") {
			formObj1.selectedIndex = 0
			search.ss.selectedIndex = 0
		}
	}
	else
	{
		formObj1.disabled = true
	}	
}
function Valorizza(formObj1,formObj2) {
	formObj1.value = ""
	formObj1.value = formObj2.options[formObj2.selectedIndex].text
}

function ChkQuest(formObj1,formObj2) {
	formObj1.value = ""
	formObj1.value = formObj2.options[formObj2.selectedIndex].value
	// document.location.href = "Sol_Cerca.asp?q=" + search.quest.value
//	 document.location.href = "Sol_init.asp?s=1&q=" + search.quest.value
	search.q.value = search.quest.value;
	search.ss.value = "1";
	document.search.submit();
}

function ChkDom() {
//  document.location.href = "Sol_Cerca.asp?q=" + search.quest.value + "&r=" + search.ragg.value
//	document.location.href = "Sol_init.asp?r=" + search.ragg.value
//  document.location.href = "Sol_init.asp?s=1&q=" + search.quest.value + "&r=" + search.ragg.value
	search.q.value = search.quest.value;
	search.r.value = search.ragg.value;
	search.ss.value = "1";
	document.search.submit();
}
function Ricerca() {
	search.q.value = search.quest.value;
	search.r.value = search.ragg.value;
	search.ss.value = "1";
	return true;
}
function CheckPaese(){
//	if (search.paese.value == "Italia") {
		search.q.value = search.quest.value;
		search.r.value = search.ragg.value;
		search.ss.value = "1";
		document.search.submit();
//	}

}
function ControllaInvio(frm)
	{
	alert("ww")
		if (frm.F1.value == "") {
			alert("Non � stato selezionato alcun documento!");
			return false
			}
			else {
			frm.submit
			}	
	}
