<!--#include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include virtual="/strutt_testa2.asp"-->
<!--#include virtual="/util/dbutil.asp"-->
<!--#include virtual="/include/ControlDateVB.asp"-->
<!--#include virtual="/include/SysFunction.asp"-->
<!--#include virtual="/include/openconn.asp"-->
<!--#include virtual="/include/DecCod.asp"-->
<!--#include Virtual="/Pgm/Agenda_Entrevistas/Movimientos_Citas/Soporte/ProcCalendario.asp"-->  
<%
Dim C

Set C = New Calendario
%>
<script language="javascript">
<!--#include virtual = "/include/SelComune.js"-->
<!--#include virtual = "/include/ControlDate.inc"-->

function CambiarDatos()
{
    document.frmIngreso.action = "OFI_InsOficina.asp"
    document.frmIngreso.submit()
}
function MuestroDependencia()
{
    var texto
    texto = ""
    texto = texto + "<table width='500' class='tbltext1'>"
    texto = texto + "<tr>"
    texto = texto + "<td width='40%'><b>La OE depende de la siguiente OE</b></td>"
    texto = texto + "<td width='60%'>"
    texto = texto + "&nbsp;&nbsp;&nbsp;"
    texto = texto + "<input type='hidden' name='hidOficinaPadre' id='hidOficinaPadre'/>"
    texto = texto + "<input type='text' name='txtOficinaPadre' id='txtOficinaPadre' class='tbltext1' size='50' readonly/>"
    texto = texto + "&nbsp;&nbsp;"
    texto = texto + "<a href='javascript:SeleccionarOficinaPadre();'>"
    texto = texto + "<img border='0' src='<%=Session("Progetto")%>/images/bullet1.gif'>"
    texto = texto + "</a>"
    texto = texto + "</td>"
    texto = texto + "</tr>" 
    texto = texto + "</table>"
    lblDepende.innerHTML = texto
}
function OcultoDependencia()
{
    lblDepende.innerHTML = ""
   
}
function SeleccionarOficinaPadre()
{
    var prov
    var muni
    
    prov = document.frmIngreso.cmbProvincia.value
    muni = document.frmIngreso.txtMunicipioHid.value
    
    if (prov != "")
    {
	   windowArea = window.open ('/include/SelOficinaPadre.asp?Provincia='+prov+'&Municipio='+muni+'&NomeForm=frmIngreso'+'&Nome=txtOficinaPadre'+'&NomeHid=hidOficinaPadre','Info','width=422,height=450,Resize=No,Scrollbars=yes,status=yes')	
    }else{
        alert("Debe seleccionar al menos el Departamento");
    }
    
}

function AbrirForm(CodImp,Rol,Formulario)
{	
    if (Formulario == "Feriados")
    {
        windowSpecifico = window.open ('FormFeriados.asp?CodSede=' + CodImp + '&Rol=' + Rol, 'Info','width=540,height=450,Resize=No,status=yes,Scrollbars=yes')   
    }
    
    if (Formulario == "Servicios")
    {
        windowSpecifico = window.open ('FormServicios.asp?CodImp=' + CodImp + '&Rol=' + Rol, 'Info','width=540,height=450,Resize=No,status=yes,Scrollbars=yes')
    }
}

function ValidacionFinal()
{

	
	var IngresoHorario;
	
	if (document.frmIngreso.txtOE.value == "")
		{
		alert("El campo Nombre de la OE es obligatorio");
		return false;
		} 
	
	if (document.frmIngreso.txtEmail.value == "")
		{
		alert("El campo E-mail de la OE es obligatorio");
		return false;
		} 
	else
	{
	    if (document.frmIngreso.txtEmail.value.match("@") != "@")
	    {
	        alert("La direcci�n de email ingresada es incorrecta.")
	        return false;
	    }
	    else
	    {
            if (
            (document.frmIngreso.txtEmail.value.match(".com") != ".com")
            &&
            (document.frmIngreso.txtEmail.value.match(".gov") != ".gov")
            &&
            (document.frmIngreso.txtEmail.value.match(".org") != ".org")       
            &&
            (document.frmIngreso.txtEmail.value.match(".mil") != ".mil")         
            &&
            (document.frmIngreso.txtEmail.value.match(".edu") != ".edu") 
            &&
            (document.frmIngreso.txtEmail.value.match(".gub") != ".gub") 
            )
            {
                alert("La direcci�n de email ingresada es incorrecta");
                return false;
            }
            else
            {
           
                if (document.frmIngreso.txtEmail.value.indexOf("@") == 0)
                    {
                        alert("La direcci�n de correo ingresada es incorrecta");
                        return false;
                    }
           
               if (document.frmIngreso.txtEmail.value.match("@.") == "@.")
                    {
                        alert("La direcci�n de correo ingresada es incorrecta");
                        return false;
                    }
            }
	    }
	}
		
	if (document.frmIngreso.txtDireccion.value == "")
		{
		alert("El campo Direcci�n de la OE es obligatorio");
		return false;
		} 
		
    if (document.frmIngreso.txtDireccion.value.length < 7)
		{
		alert("El campo Direcci�n de la OE de ser de al menos 7 caracteres");
		return false;
		} 
			
	if (document.frmIngreso.cmbProvincia.value == "")
		{
		alert("El campo Departamento es obligatorio");
		return false;
		} 
	
	if (document.frmIngreso.txtMunicipioHid.value == "")
		{
		alert("El campo Municipio es obligatorio");
		return false;
		} 
	
	if (document.frmIngreso.txtCPNH.value == "")
		{
			alert("El campo C�digo Postal es obligatorio");
			return false;
		} 
	else
		{
			if (isNaN(document.frmIngreso.txtCPNH.value) == true)
				{
					alert("El C�digo Postal debe ser num�rico")
					return false;
				}
		}
	
	if (document.frmIngreso.txtCP.value == "")
		{
			alert("El campo C�digo Postal es obligatorio");
			return false;
		} 
	else
		{
			if (isNaN(document.frmIngreso.txtCP.value) == true)
				{
					alert("El C�digo Postal debe ser num�rico")
					return false;
				}
		}

	if (document.frmIngreso.txtTelContacto.value == "")
		{
			alert("El campo Tel�fono de Contacto es obligatorio");
			return false;
			} 
		else
		{
			if (isNaN(document.frmIngreso.txtTelContacto.value) == true)
				{
					alert("El Tel�fono de Contacto debe ser num�rico")
					return false;
				}
				else
				{
				    if (document.frmIngreso.txtTelContacto.value.length < 8)
				    {
    				    alert("La longitud m�nima permitida para el campo tel�fono de contacto es de 8 n�meros");
    				    return false;
				    }
				}
		}
		
	if (document.frmIngreso.txtTelAlternativo.value == "")
		{
			//alert("El campo Tel�fono Alternativo es obligatorio");
			//return false;
		} 
		else
		{
			if (isNaN(document.frmIngreso.txtTelAlternativo.value) == true)
				{
					alert("El Tel�fono Alternativo debe ser num�rico")
					return false;
				}
			else
				{
				    if (document.frmIngreso.txtTelAlternativo.value.length < 8)
				    {
    				    alert("La longitud m�nima permitida para el campo tel�fono alternativo es de 8 n�meros");
    				    return false;
				    }					
				}
		}
			
	if (document.frmIngreso.cmbTipoOficina.value == "")
		{
		alert("El campo Tipo de Oficina es obligatorio");
		return false;
		} 
			
	if ((document.frmIngreso.optEsSupervisora[0].checked == false) && (document.frmIngreso.optEsSupervisora[1].checked == false))
		{
		alert("El campo Es Supervisora es obligatorio");
		return false;
		} 
		
	if (document.frmIngreso.optEsSupervisora[1].checked == true)
	{
	    if (document.frmIngreso.hidOficinaPadre.value == "")
	    {
	        alert("Si seleccion� la opci�n 'No Es Supervisora' debe seleccionar la OE a la cual se encuentra asociada");
	        return false; 
	    }
	    
	}
	
	if (document.frmIngreso.txtFechaDesde.value == "")
		{
		alert("El campo Fecha Desde es obligatorio");
		return false;
		} 
	else
	    {
	    	if (!ValidateInputDate(document.frmIngreso.txtFechaDesde.value))
            {
	            document.frmIngreso.txtFechaDesde.focus() 
	            return false
            }
	    }
	
	if (document.frmIngreso.txtFechaHasta.value == "")
		{
		alert("El campo Fecha Hasta es obligatorio");
		return false;
		} 
	else
	    {
	    	if (!ValidateInputDate(document.frmIngreso.txtFechaHasta.value))
            {
	            document.frmIngreso.txtFechaHasta.focus() 
	            return false
            }
	    }
	
	if (ValidateRangeDate(document.frmIngreso.txtFechaDesde.value,document.frmIngreso.txtFechaHasta.value)==false){
		alert("La fecha inicial no debe ser mayor a la fecha final!")
	    return false;
	}
	
	if (ValidateRangeDate(document.frmIngreso.txtFechaActual.value,document.frmIngreso.txtFechaDesde.value)==false){
		alert("La fecha actual no debe ser mayor a la fecha inicial!")
	    return false;
	}	
		    	
	if (document.frmIngreso.txtPuestos.value == "")
		{
		alert("El campo Cantidad de Puestos es obligatorio");
		return false;
		} 
	else
		{
			if (isNaN(document.frmIngreso.txtPuestos.value) == true)
				{
					alert("La Cantidad de Puestos debe ser num�rica")
					return false;
				}
		}
		
	if (document.frmIngreso.cmbLunesInicio.value == "")
		{
		alert("El campo Lunes Hora Inicio es obligatorio");
		return false;
		} 
	
	if (document.frmIngreso.cmbLunesFin.value == "")
		{
		alert("El campo Lunes Hora Fin es obligatorio");
		return false;
		} 
	
	if (document.frmIngreso.cmbMartesInicio.value == "")
		{
		alert("El campo Martes Hora Inicio es obligatorio");
		return false;
		} 
	
	if (document.frmIngreso.cmbMartesFin.value == "")
		{
		alert("El campo Martes Hora Fin es obligatorio");
		return false;
		} 
	
	if (document.frmIngreso.cmbMiercolesInicio.value == "")
		{
		alert("El campo Miercoles Hora Inicio es obligatorio");
		return false;
		} 
	
	if (document.frmIngreso.cmbMiercolesFin.value == "")
		{
		alert("El campo Miercoles Hora Fin es obligatorio");
		return false;
		} 
	
	if (document.frmIngreso.cmbJuevesInicio.value == "")
		{
		alert("El campo Jueves Hora Inicio es obligatorio");
		return false;
		} 
	
	if (document.frmIngreso.cmbJuevesFin.value == "")
		{
		alert("El campo Jueves Hora Fin es obligatorio");
		return false;
		} 
	
	if (document.frmIngreso.cmbViernesInicio.value == "")
		{
		alert("El campo Viernes Hora Inicio es obligatorio");
		return false;
		} 
	
	if (document.frmIngreso.cmbViernesFin.value == "")
		{
		alert("El campo Viernes Hora Fin es obligatorio");
		return false;
		} 
	
	if (document.frmIngreso.cmbSabadoInicio.value == "")
		{
		alert("El campo S�bado Hora Inicio es obligatorio");
		return false;
		} 
	
	if (document.frmIngreso.cmbSabadoFin.value == "")
		{
		alert("El campo S�bado Hora Fin es obligatorio");
		return false;
		} 
	
	if (document.frmIngreso.cmbDomingoInicio.value == "")
		{
		alert("El campo Domingo Hora Inicio es obligatorio");
		return false;
		} 
	
	if (document.frmIngreso.cmbDomingoFin.value == "")
		{
		alert("El campo Domingo Hora Fin es obligatorio");
		return false;
		} 

	IngresoHorario = 0
	
    if (parseInt(document.frmIngreso.cmbLunesInicio.value) > parseInt(document.frmIngreso.cmbLunesFin.value))
    {
        alert("El horario inicial de atenci�n debe ser menor al final");
        return false;
    }else
    {
        if (parseInt(document.frmIngreso.cmbLunesInicio.value) < parseInt(document.frmIngreso.cmbLunesFin.value))
        {
            IngresoHorario = 1
        }
    }

    if (parseInt(document.frmIngreso.cmbMartesInicio.value) > parseInt(document.frmIngreso.cmbMartesFin.value))
    {
        alert("El horario inicial de atenci�n debe ser menor al final");
        return false;
    }else
    {
        if (parseInt(document.frmIngreso.cmbMartesInicio.value) < parseInt(document.frmIngreso.cmbMartesFin.value))
        {
            IngresoHorario = 1
        }        
    }    

    if (parseInt(document.frmIngreso.cmbMiercolesInicio.value) > parseInt(document.frmIngreso.cmbMiercolesFin.value))
    {
        alert("El horario inicial de atenci�n debe ser menor al final");
        return false;
    }else
    {
        if (parseInt(document.frmIngreso.cmbMiercolesInicio.value) < parseInt(document.frmIngreso.cmbMiercolesFin.value))
        {
            IngresoHorario = 1
        }  
    }
        
    if (parseInt(document.frmIngreso.cmbJuevesInicio.value) > parseInt(document.frmIngreso.cmbJuevesFin.value))
    {
        alert("El horario inicial de atenci�n debe ser menor al final");
        return false;
    }else
    {
        if (parseInt(document.frmIngreso.cmbJuevesInicio.value) < parseInt(document.frmIngreso.cmbJuevesFin.value))
        {
            IngresoHorario = 1
        }  
    }
    
    if (parseInt(document.frmIngreso.cmbViernesInicio.value) > parseInt(document.frmIngreso.cmbViernesFin.value))
    {
        alert("El horario inicial de atenci�n debe ser menor al final");
        return false;
    }else
    {
        if (parseInt(document.frmIngreso.cmbViernesInicio.value) < parseInt(document.frmIngreso.cmbViernesFin.value))
        {
            IngresoHorario = 1
        }         
    }

    if (parseInt(document.frmIngreso.cmbSabadoInicio.value) > parseInt(document.frmIngreso.cmbSabadoFin.value))
    {
        alert("El horario inicial de atenci�n debe ser menor al final");
        return false;
    }else
    {
        if (parseInt(document.frmIngreso.cmbSabadoInicio.value) < parseInt(document.frmIngreso.cmbSabadoFin.value))
        {
            IngresoHorario = 1
        }          
    }

    if (parseInt(document.frmIngreso.cmbDomingoInicio.value) > parseInt(document.frmIngreso.cmbDomingoFin.value))
    {
        alert("El horario inicial de atenci�n debe ser menor al final");
        return false;
    }else
    {
        if (parseInt(document.frmIngreso.cmbDomingoInicio.value) < parseInt(document.frmIngreso.cmbDomingoFin.value))
        {
            IngresoHorario = 1
        }           
    }
    
	if (IngresoHorario == 0)
    {
        alert("Debe seleccionar el horario de atenci�n al menos para un d�a de la semana");
        return false;
    }
    
	 
	return true;
}
</script>

<%
    '''vany 10042007

 if (FaltaCarga <> "S") then
	if isobject(Session("ObjServiciosOE")) then
		set ObjServicios = Session("ObjServiciosOE")
		
		ObjServicios.RemoveAll()
		set Session("ObjServiciosOE")  = ObjServicios
	end if
	
	if isobject(Session("ObjFeriados")) then
		set ObjPerfiles = Session("ObjFeriados")
	
		ObjPerfiles.RemoveAll()
		set Session("ObjFeriados")  = ObjPerfiles
	end if
end if
   
    sRol = request("Rol")
    sCodImp = request("CodImp")
    sCodSede = request("CodSede")
    
    if sRol = "" then 
        sRol = 3
    end if 
    
    if sCodImp = "" then
        sCodImp = 0
    end if 
    
    if sCodSede = "" then   
        sCodSede = 0 
    end if 
    
    '****Datos Basicos****' 
    sOE = request("txtOE")
    sEmail = request("txtEmail")
    sDireccion = request("txtDireccion")
    sTelContacto = request("txtTelContacto")
    sTelAlternativo = request("txtTelAlternativo")
    sProvincia = request("cmbProvincia")
    sMunicipio = request("txtMunicipioHid")
    sCodPost = request("txtCPNH")
    sTipoOficina = request("cmbTipoOficina")
 
    '****Supervisora****'
    sEsSupervisora = request("optEsSupervisora")
    sOficinaPadre = request("hidOficinaPadre")
    
       
    '****Puestos****'
    sFechaDesde = request("txtFechaDesde")    
    sFechaHasta = request("txtFechaHasta")
    sPuestos = request("txtPuestos")
    
    '****Configuracion de Agenda****' 
    sLunesInicio = request("cmbLunesInicio")
    sLunesFin = request("cmbLunesFin")

    sMartesInicio = request("cmbMartesInicio")
    sMartesFin = request("cmbMartesFin")
    
    sMiercolesInicio = request("cmbMiercolesInicio")
    sMiercolesFin = request("cmbMiercolesFin")

    sJuevesInicio = request("cmbJuevesInicio")
    sJuevesFin = request("cmbJuevesFin")

    sViernesInicio = request("cmbViernesInicio")
    sViernesFin = request("cmbViernesFin")
 
    sSabadoInicio = request("cmbSabadoInicio")
    sSabadoFin = request("cmbSabadoFin")               
 
    sDomingoInicio = request("cmbDomingoInicio")
    sDomingoFin = request("cmbDomingoFin")
    
    Session("IngresoServiciosOE") = false
    Session("IngresoFeriados") = false

%>
<br />
<br />
<%FechaActual = ConvDateToString(Date())%>
<form name="frmIngreso" action="OFI_CnfOficina.asp" method="post" onsubmit="return ValidacionFinal();">
<input type="hidden" value="<%=FechaActual%>" name="txtFechaActual" />
<table width="500" border="0" class="tbltext1">

    <tr>
        <td colspan="2">
            <b>Ingrese a continuaci�n los datos correspondientes a la OE que desea crear.</b>
            <input type="hidden" value="<%=sRol%>" name="Rol" />
            <input type="hidden" value="<%=sCodImp%>" name="CodImp" />
            <input type="hidden" value="<%=sCodSede%>" name="CodSede" />
            <br />
            <br />
            <br />
            <br />
        </td>
    </tr>
    <tr>
        <td colspan="2"><hr /></td>
    </tr>
    <tr>
        <td colspan="2"><b><u>Datos B�sicos de la Oficina</u></b><br /><br /></td>
    </tr>    
    <tr>
        <td width='40%'><b>Nombre de La OE *</b></td>
        <td width='60%'><input type="text" name="txtOE" value="<%=sOE%>" size="50" maxlength="100" class="tbltext1"/></td>
    </tr>
    <tr>
        <td><b>E-mail de la OE *</b></td>
        <td><input type="text" name="txtEmail" value="<%=sEmail%>" size="50" maxlength="100" class="tbltext1"/></td>
    </tr>
    <tr>
        <td><b>Direcci�n *</b></td>
        <td><input type="text" name="txtDireccion" value="<%=sDireccion%>" size="50" maxlength="100" class="tbltext1"/></td>
    </tr>
    <tr>
        <td><b>Departamento *</b></td>
        <td>
            <%sInt = "PROV|0|" & date & "|" & sProvincia & "|cmbProvincia' class='tbltext1' onchange='CambiarDatos()|ORDER BY DESCRIZIONE"%>			
		    <%CreateCombo(sInt)%>   
	    </td>
    </tr>
    
    <tr>
		<td>
			<b>Municipio *</b>
		</td>
		<td>
			<input type="text" name="txtMunicipio" style="TEXT-TRANSFORM: uppercase; WIDTH: 270px" class="tbltext1" size="10" readonly value>
			<input type="hidden" id="txtMunicipioHid" name="txtMunicipioHid" value>

<%
				NomeForm="frmIngreso"
				CodiceProvincia="cmbProvincia"
				NomeComune="txtMunicipio"
				CodiceComune="txtMunicipioHid"
				Cap="txtCP"
%>
			<a href="Javascript:SelComune('<%=NomeForm%>','<%=CodiceProvincia%>','<%=NomeComune%>','<%=CodiceComune%>','<%=Cap%>')" ID="imgPunto1" name="imgPunto1" onmouseover="javascript:window.status='';return true">
			
			<img border="0" src="<%=Session("Progetto")%>/images/bullet1.gif"></a>				
		</td>
	</tr>
	<tr>
	    <td>
	        <b>C.P. *</b>
	    </td>
	    <td>
	        <input type="hidden" name="txtCPNH" value>
			<input type="text" class="tbltext1" name="txtCP" value size="10" maxlength="5">
	    </td>
	</tr>
    <tr>
        <td><b>Tel�fono de Contacto: *</b></td>
        <td><input type="text" name="txtTelContacto" value="<%=sTelContacto%>" size="20" maxlength="100" class="tbltext1"/></td>
    </tr>
    <tr>
        <td><b>Tel�fono Alternativo: </b></td>
        <td><input type="text" name="txtTelAlternativo" value="<%=sTelAlternativo%>" size="20" maxlength="100" class="tbltext1"/></td>
    </tr>
    <tr>
        <td><b>Tipo de Oficina:</b></td>
        <td>
        <% 
        sqlTipoOficina = "select codice, descrizione from tades where nome_tabella = 'FGIUR' and valore='03'"
        CreateComboDesdeSQL sqlTipoOficina,"cmbTipoOficina","tbltext1",sTipoOficina,"","270",cc
        %>                 
        </td>
    </tr>   
    
    <tr>
        <td colspan="2"><hr /></td>
    </tr>
           
    <tr>
        <td colspan="2"><br />
            <a class="textred" href="javascript:AbrirForm(<%=sCodImp%>,<%=sRol%>,'Servicios');">
                <b>Prestaciones de la Oficina &nbsp;&nbsp;&nbsp;&nbsp;>></b>
            </a>   
        </td>
    </tr> 
 
    <tr>
        <td colspan="2"><hr /></td>
    </tr>
       
    <tr>
        <td><br /><b>La oficina es Supervisora? *</b></td>
        <td>
        <b>SI</b><input type="radio" name="optEsSupervisora" id="optEsSupervisora" value="S" class="tbltext1" onclick="OcultoDependencia()"/>
        <b>NO</b><input type="radio" name="optEsSupervisora" id="optEsSupervisora" value="N" class="tbltext1" onclick="MuestroDependencia()"/>
        </td>
    </tr>
</table>

 <label name="lblDepende" id="lblDepende">
 </label>  
 
 <table width="500" class="tbltext1">
    <tr>
        <td colspan="2"><hr /></td>
    </tr>
    <tr>
        <td colspan="2"><b><u>Puestos</u></b><br /><br /></td>
    </tr>
    <tr>
       <td width="40%">
            <b>Fecha Desde</b>
       </td>
       <td width="60%"> 
            <b><input type="text" name="txtFechaDesde" value="<%=sFechaDesde%>" class="tbltext1" size="15" maxlength="10"/></b>
       </td>
    </tr>     
    <tr>
       <td>
            <b>Fecha Hasta</b>
       </td>
       <td>
            <b><input type="text" name="txtFechaHasta" value="<%=sFechaHasta%>" class="tbltext1" size="15" maxlength="10"/></b>
       </td>
    </tr> 
    <tr>
       <td>
            <b>Cantidad de Puestos</b>
       </td>
       <td>
            <b><input type="text" name="txtPuestos" value="<%=sPuestos%>" class="tbltext1" size="4" maxlength="3"/></b>
       </td>
    </tr>        
    <tr>
        <td colspan="2"><hr /></td>
    </tr>
    <tr>
        <td colspan="2"><b><u>Franja Horaria para Entrevistas</u></b><br /><br /></td>
    </tr>
    <tr>
       <td>
            <b>Lunes</b>&nbsp;&nbsp;&nbsp;&nbsp;
       </td>
       <td>
            <b>H. Inicial</b>&nbsp;&nbsp;<%=C.CrearComboAgEnda("cmbLunesInicio","tbltext1","","HORAS","420,1260",sLunesInicio,61)%>
            &nbsp;&nbsp;&nbsp;&nbsp;<b>H. Final</b>&nbsp;&nbsp;<%=C.CrearComboAgEnda("cmbLunesFin","tbltext1","","HORAS","420,1260",sLunesFin,61)%>
       </td>
    </tr>    

    <tr>
       <td>
            <b>Martes</b>&nbsp;&nbsp;&nbsp;&nbsp;
       </td>
       <td>
            <b>H. Inicial</b>&nbsp;&nbsp;<%=C.CrearComboAgEnda("cmbMartesInicio","tbltext1","","HORAS","420,1260",sMartesInicio,61)%>
            &nbsp;&nbsp;&nbsp;&nbsp;<b>H. Final</b>&nbsp;&nbsp;<%=C.CrearComboAgEnda("cmbMartesFin","tbltext1","","HORAS","420,1260",sMartesFin,61)%>
       </td>
    </tr>

    <tr>
       <td>
            <b>Mi�rcoles</b>&nbsp;&nbsp;&nbsp;&nbsp;
       </td>
       <td>
            <b>H. Inicial</b>&nbsp;&nbsp;<%=C.CrearComboAgEnda("cmbMiercolesInicio","tbltext1","","HORAS","420,1260",sMiercolesInicio,61)%>
            &nbsp;&nbsp;&nbsp;&nbsp;<b>H. Final</b>&nbsp;&nbsp;<%=C.CrearComboAgEnda("cmbMiercolesFin","tbltext1","","HORAS","420,1260",sMiercolesFin,61)%>
       </td>
    </tr> 
    
    <tr>
       <td>
            <b>Jueves</b>&nbsp;&nbsp;&nbsp;&nbsp;
       </td>
       <td>
            <b>H. Inicial</b>&nbsp;&nbsp;<%=C.CrearComboAgEnda("cmbJuevesInicio","tbltext1","","HORAS","420,1260",sJuevesInicio,61)%>
            &nbsp;&nbsp;&nbsp;&nbsp;<b>H. Final</b>&nbsp;&nbsp;<%=C.CrearComboAgEnda("cmbJuevesFin","tbltext1","","HORAS","420,1260",sJuevesFin,61)%>
       </td>
    </tr>           
 
    <tr>
       <td>
            <b>Viernes</b>&nbsp;&nbsp;&nbsp;&nbsp;
       </td>
       <td>
            <b>H. Inicial</b>&nbsp;&nbsp;<%=C.CrearComboAgEnda("cmbViernesInicio","tbltext1","","HORAS","420,1260",sViernesInicio,61)%>
            &nbsp;&nbsp;&nbsp;&nbsp;<b>H. Final</b>&nbsp;&nbsp;<%=C.CrearComboAgEnda("cmbViernesFin","tbltext1","","HORAS","420,1260",sViernesFin,61)%>
       </td>
    </tr> 
  
    <tr>
       <td>
            <b>S�bado</b>&nbsp;&nbsp;&nbsp;&nbsp;
       </td>
       <td>
            <b>H. Inicial</b>&nbsp;&nbsp;<%=C.CrearComboAgEnda("cmbSabadoInicio","tbltext1","","HORAS","420,1260",sSabadoInicio,61)%>
            &nbsp;&nbsp;&nbsp;&nbsp;<b>H. Final</b>&nbsp;&nbsp;<%=C.CrearComboAgEnda("cmbSabadoFin","tbltext1","","HORAS","420,1260",sSabadoFin,61)%>
       </td>
    </tr> 
    
    <tr>
       <td>
            <b>Domingo</b>&nbsp;&nbsp;&nbsp;&nbsp;
       </td>
       <td>
            <b>H. Inicial</b>&nbsp;&nbsp;<%=C.CrearComboAgEnda("cmbDomingoInicio","tbltext1","","HORAS","420,1260",sDomingoInicio,61)%>
            &nbsp;&nbsp;&nbsp;&nbsp;<b>H. Final</b>&nbsp;&nbsp;<%=C.CrearComboAgEnda("cmbDomingoFin","tbltext1","","HORAS","420,1260",sDomingoFin,61)%>
       </td>
    </tr> 

    <tr>
        <td colspan="2"><hr /></td>
    </tr>    
    
    <tr>
        <td colspan="2"><br />
            <a class="textred" href="javascript:AbrirForm(<%=sCodSede%>,<%=sRol%>,'Feriados');">
                <b>Feriados de la Oficina &nbsp;&nbsp;&nbsp;&nbsp;>></b>
            </a>   
        </td>
    </tr>            

    <tr>
        <td colspan="2"><hr /></td>
    </tr>
    
     <tr>
        <td colspan="2" align="right"><br />
            <input type="submit" name="Enviar" value="Enviar >>" class="my"/>
        </td>
    </tr>     
    
 </table>

 </form>  
























    
		    
    <!--<tr>
        <td><b>Departamento:</b></td>
        <td> 
        <% 
        sDepartamento = request("cmbDepartamento")
        sqlDepartamento = "select distinct coddept,descripcion from Departamento where provincia = '" & sProvincia & "'"
        CreateComboDesdeSQL sqlDepartamento,"cmbDepartamento","tbltext1",sDepartamento,"onchange=CambiarDatos()","",cc   
        %>
        </td>
    </tr>
    <tr>
        <td><b>Localidad:</b></td>
        <td>
        <% 
        sLocalidad = request("cmbLocalidad")
        sqlLocalidad = "select distinct codloc,descripcion from Localidad where provincia = '" & sProvincia & "' and depto ='" & sDepartamento & "'"
        CreateComboDesdeSQL sqlLocalidad,"cmbLocalidad","tbltext1",sLocalidad,"onchange=CambiarDatos()","",cc   
        %>                 
        </td>
    </tr>
    <tr>
        <td><b>La oficina participa del seguro? *</b></td>
        <td>
        <b>SI</b><input type="radio" name="optParticipaSeguro" value="S" class="tbltext1"/>
        <b>NO</b><input type="radio" name="optParticipaSeguro" value="N" class="tbltext1"/>
        </td>
    </tr>
    <tr>
        <td><b>La oficina es una GECAL? *</b></td>
        <td>
        <b>SI</b><input type="radio" name="optEsGecal" value="S" class="tbltext1"/>
        <b>NO</b><input type="radio" name="optEsGecal" value="N" class="tbltext1"/>
        </td>
    </tr>
    <tr>
        <td><b>La oficina es una ONG? *</b></td>
        <td>
        <b>SI</b><input type="radio" name="optEsOng" value="S" class="tbltext1"/>
        <b>NO</b><input type="radio" name="optEsOng" value="N" class="tbltext1"/>
        </td>
    </tr>-->
   
<!--#include virtual="/strutt_coda2.asp"-->
<%
Set C = Nothing
%>

