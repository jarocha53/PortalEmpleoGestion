<%@ Language=VBScript %>
<%
clave = request("clave")
sCodImp=request("CodImp")
sRol = request("Rol")

if isobject(Session("ObjServiciosOE")) then
	set ObjServiciosOE = Session("ObjServiciosOE")
else
	set ObjServiciosOE = server.CreateObject("Scripting.Dictionary")
end if

'linka="BorrarPerfiles.asp?servicio=" & clave & "&Rol=" & sRol
'linkb="BorrarCapacitacion.asp?servicio=" & clave & "&Rol=" & sRol
file = "BorrarPerfiles.asp"

if clave <> "" then 
	if ObjServiciosOE.Exists(cint(clave)) then 
		ObjServiciosOE.Remove(cint(clave))
		BorrarCapacitacion(clave)
		BorrarPerfil(clave)
	end if 
else
	ObjServiciosOE.RemoveAll()
	BorrarCapacitacion("Todo")
	BorrarPerfil("Todo")
end if



Response.Redirect "FormServicios.asp?CodImp=" & sCodImp & "&Rol=" & sRol

set Session("ObjServiciosOE") = ObjServiciosOE


sub BorrarCapacitacion(clave)
	if isobject(Session("ObjCapacitacion")) then
		set ObjCapacitacion = Session("ObjCapacitacion")
	else
		set ObjCapacitacion = server.CreateObject("Scripting.Dictionary")
	end if
	
	if clave <> "Todo" then
		for each cl in ObjCapacitacion
			Identificadores=""
			Identificadores = split(cl,"_")
					
			if isarray(Identificadores) then 
				valor = Identificadores(0)
			else
				valor = ""
			end if
			
			if cstr(valor) = cstr(clave) then
				ObjCapacitacion.Remove(cl)
			end if 
		next
	else
		ObjCapacitacion.RemoveAll()
	end if
end sub


sub BorrarPerfil(clave)
	if isobject(Session("ObjPerfiles")) then
		set ObjPerfiles = Session("ObjPerfiles")
	else
		set ObjPerfiles = server.CreateObject("Scripting.Dictionary")
	end if

	if clave <> "Todo" then
		for each cl in ObjPerfiles
			Identificadores=""
			Identificadores = split(cl,"_")
					
			if isarray(Identificadores) then 
				valor = Identificadores(0)
			else
				valor = ""
			end if
			
			if cstr(valor) = cstr(clave) then
				ObjPerfiles.Remove(cl)
			end if 
		next
	else
		ObjPerfiles.RemoveAll()
	end if
end sub


%>
