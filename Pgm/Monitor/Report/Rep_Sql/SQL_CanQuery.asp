<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->

<title>Interrogazione Base Dati</title>
<link REL="STYLESHEET" TYPE="text/css" HREF="<%=Session("Progetto")%>/fogliostile.css">
<table border="0" CELLPADDING="0" CELLSPACING="0" width="450">
	<tr height="17">
		<td class="sfondomenu" width="67%" height="18"><span class="tbltext0">
			<b>&nbsp;ELIMINAZIONE QUERY</b>
		</td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">
		</td>
		<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
	</tr>
	<tr>
		<td class="sfondocomm" width="57%" colspan="3">
			 Conferma elimininazione query.
		</td>
	</tr>
	<tr height="2">
		<td class="sfondocomm" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif">
		</td>
	</tr>
</table>
<%
nRighe = Request.Form("Elem")
n=0
sErrore = True
For i = 0 To nRighe
	sChecked = Request.form("checkbox" & i)
	If sChecked = "on" Then
		If not DelFile(Request.Form("nf" & i)) Then
			sErrore = False
			exit for
		End If
	End If
Next
%>
<br><br>
<table border="0" CELLPADDING="0" CELLSPACING="0" width="450">
	<tr align="center" height="2">
	<%If sErrore Then %>
		<td class="tbltext3">Eliminazione correttamente effettuata</td>
	<%Else%>
		<td class="tbltext3">Errore durante la eliminazione</td>
	<%End If%>
	</tr>
</table>
<br><br>
<table border="0" CELLPADDING="0" CELLSPACING="0" width="450">
	<tr align="center" height="2">
		<td class="tbltext3" align="right">
		<a href="SQL_VisQuery.asp"><img src="<%=session("progetto")%>/images/indietro.gif" title="chiudi" border="0"></a>
		</td>
		<td class="tbltext3" align="left">
		<a href="Javascript:close()"><img src="<%=session("progetto")%>/images/chiudi.gif" title="chiudi" border="0"></a>
		</td>
	</tr>
</table>
<%
Function DelFile(sNomeFile)

	dim sFilePath

	DelFile = False
	sPrj = replace(Session("Progetto"),"/","\")
	sFilePath=Server.MapPath ("\") & sPrj & "\DocPers\Estrattore\"  & sNomeFile
	set oFileSystemObject	= Server.CreateObject("Scripting.FileSystemObject")
	
	If oFileSystemObject.FileExists (sFilePath) Then
		oFileSystemObject.DeleteFile (sFilePath)
		DelFile = true
	End If
	
	Set oFileSystemObject = nothing
	Set oFileCurriculum = nothing

End Function
%>
