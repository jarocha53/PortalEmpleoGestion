<!--#include virtual ="/util/portallib.asp"-->
<!--#include virtual ="/include/openconn.asp"-->
<!--#include virtual ="/Util/DBUtil.asp"-->
<!--#include virtual ="/include/DecCod.asp"-->
<!--#include virtual ="/include/ckProfile.asp"-->
	
<title>Interrogazione Base Dati</title>
<link REL="STYLESHEET" TYPE="text/css" HREF="<%=Session("Progetto")%>/fogliostile.css">

<script LANGUAGE="JavaScript" src="/Include/FunOpenWindow.js"></script>
<script language="Javascript" src="ChkQuery.js"></script>
<%
Sub ImpostaPag()
	Dim sTipo, sSql
%>		
<br>
<table border="0" CELLPADDING="0" CELLSPACING="0" width="450">
	<tr height="17">

		<td class="sfondomenu" width="67%" height="18"><span class="tbltext0">
			<b>&nbsp;VISUALIZZAZIONE QUERY </b>
		</td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">
		</td>
		<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
	</tr>
	<tr>
		<td class="sfondocomm" width="57%" colspan="3">
			Selezionare la query che si desidera visualizzare 
			oppure spuntare quelle che si vogliono eliminare.
			I file con estenzione TXT indicano le query registrate,
			mentre i file con estenzione XLS indicano i report generati.
		</td>
	</tr>
	<tr height="2">
		<td class="sfondocomm" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif">
		</td>
	</tr>
</table>
<br>			
<%	
	sPrj = replace(Session("Progetto"),"/","\")	
	sPathnameRoot = Server.MapPath("\") & sPrj & "\DocPers\Estrattore\"
	ShowFolderList(sPathnameRoot)
	aDivstr = split(sTxt,"-")
	aDtstr = split(sDtC,"-")
	nTotSubDir = Ubound(split(sTxt,"-"))
		
	If nTotSubDir <> -1 Then 
		NUM = 0
	'si verifica se l'utente ha almeno un file salvato
		For i = 0 To (nTotSubDir-1)
			aNomef = split(aDivstr(i),"_",-1,1)
			sCreator = aNomef(0)
			If right(ucase(aDivstr(i)),4) = ".TXT" And sCreator = cstr(Session("idutente")) Then
				NUM = NUM + 1
			End If 
			If right(ucase(aDivstr(i)),4) = ".XLS" And aNomef(1) = cstr(Session("idutente")) Then
				NUM = NUM + 1
			End If 
		Next
		If NUM <> 0 Then
%>
		<form name="frmReport" method="POST" ONSUBMIT="return ConrolSel()" action="SQL_CanQuery.asp">
		<table width="450px" border="0" cellspacing="1" cellpadding="1">
			<tr height="23" align="middle" class="sfondocomm">
				<td width="10">&nbsp;</td>
				<td align="center" width="170">
					 <b>Nome file &nbsp; </b> 
				</td>
				<td align="center">
					 <b>Data creazione &nbsp; </b> 
				</td>
			</tr>
<%
				n = 0
				For i = 0 To (nTotSubDir-1)
					aNomef = split(aDivstr(i),"_",-1,1)
					sNomef = aNomef(2)
					sCreator = aNomef(0)
					If (right(ucase(aDivstr(i)),4) = ".XLS") And (aNomef(1) = cstr(Session("idutente"))) Then
						
						sPathXLS = Session("Progetto")& "/DocPers/Estrattore/" & aDivstr(i)		
						n = n + 1
%>	
						<input name="nf<%=n%>" type="hidden" value="<%=aDivstr(i)%>">
						<tr height="2" class="tblsfondo">
							<td width="10"><input type="checkbox" name="checkbox<%=n%>"></td>
							<td align="center"><a class="textred" href="javascript:ScaricaReport('<%=sPathXLS%>')"><b><%=split(aDivstr(i),"_",-1,1)(2)%>_<%=split(aDivstr(i),"_",-1,1)(3)%></a></b></td>
							<td align="center" class="tbltext1"><%=aDtstr(i)%></td>
						</tr>
<%					End If
					
					If (right(ucase(aDivstr(i)),4) = ".TXT") And (sCreator = cstr(Session("idutente"))) Then
						sPathTXT = Session("Progetto")& "/DocPers/Estrattore/" & aDivstr(i)		
						n = n + 1
					%>	
						<input name="nf<%=n%>" type="hidden" value="<%=aDivstr(i)%>">
						<tr height="2" class="tblsfondo">
							<td width="10"><input type="checkbox" name="checkbox<%=n%>"></td>
							<td align="center"><a class="textred" href="javascript:ScaricaReport('<%=sPathTXT%>')"><b><%=sNomef%></a></b></td>
							<td align="center" class="tbltext1"><%=aDtstr(i)%></td>
						</tr>
<%					End If
				Next
%>				</table>
				<input name="Elem" type="hidden" value="<%=n%>">
			<br><br>
<%		Else %>	
			<table width="450" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td align="middle" class="tbltext3">
						<b>Non risulta alcuna query salvata.</b>
					</td>
				</tr>
			</table>
			<br><br><br>													
<%		End If
	Else
%>			<table width="450" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td align="middle" class="tbltext1">
						<b class="size">Non risulta alcuna query salvata.</b>
					</td>
				</tr>
			</table>
			<br><br><br>													
<%	End If	%>
			
		<table width="450" border="0" cellspacing="0" cellpadding="0">
			<tr align="center">
				<%if n <> 0 then%>&nbsp;
					<td align="right">
					<input type="image" src="<%=Session("Progetto")%>/images/Elimina.gif" title="Chiudi">
					</td></form>
						<form name="can2" method="post" action="javascript:window.close()">
					<td align="left">
						<input name="b" type="image" src="<%=Session("Progetto")%>/images/chiudi.gif" value="Chiudi">
					</td>
				<%else%>
					</form>
						<form name="can2" method="post" action="javascript:window.close()">
					<td align="center">
						<input name="b" type="image" src="<%=Session("Progetto")%>/images/chiudi.gif" value="Chiudi">
					</td>			
				<%end if%>
			</tr>
		</table>
		</form>
		
<%	End Sub

Sub ShowFolderList(folderspec)
	Dim fso, f, f1, s, sf
	Set fso = CreateObject("Scripting.FileSystemObject")
	on error resume next 
	Set f = fso.GetFolder(folderspec)
	Set sf = f.Files
	If err > 0 Then
		Response.Write "<b class='tbltext'>Non risulta presente la cartella Estrattore </b>"
		Response.End
	End If
	For Each f1 in sf
	  sTxt = sTxt & f1.name & "-"
	  sDtC = sDtC & f1.datelastmodified & "-"
	Next
  
	Set sf = nothing
	Set f = nothing
	Set fso = nothing
End Sub		

'*********************		MAIN	*******************

	Dim strConn
	Dim sTxt		'nome dei files contenuti nella directory Estrattore del progetto
	Dim sDtC		'Data di creazione dei files contenuti nella directory Estrattore del progetto
		
	If Not ValidateService(session("idutente"),"SQL_RicReport",cc) Then 
		response.redirect "/util/error_login.asp"
	End If	
		
	ImpostaPag()
%>
	<!--#include virtual ="/include/closeconn.asp"-->

