var newWindow1
	var newWindow2
		
	function VisQ()
	{	
		if (newWindow2 != null ) 
		{
			newWindow2.close()  
		}			
		funz  = 'SQL_VisQuery.asp' 
		newWindow2 = window.open(funz, 'Visualizza',"toolbar=0;location=0,directories=0,status=0,scrollbars=1,resizable=1,copyhistory=0,width=500,height=340,screenX=200,screenY200")
	}
		
	function Controlla()
	{	var sql = document.frmSelect.txtQuery.value
		if (sql == "")
			{
				alert("Scrivere il testo da salvare.");
				document.frmSelect.txtQuery.focus();
			}
		else
			{
				Salva();
			}
	}

	function replace(string,text,by) {
	// Replaces text with by in string
	    var strLength = string.length, txtLength = text.length;
	    if ((strLength == 0) || (txtLength == 0)) return string;

	    var i = string.indexOf(text);
	    if ((!i) && (text != string.substring(0,txtLength))) return string;
	    if (i == -1) return string;

	    var newstr = string.substring(0,i) + by;

	    if (i+txtLength < strLength)
	        newstr += replace(string.substring(i+txtLength,strLength),text,by);

	    return newstr;
	}
  
	function Salva()
	{	
		if (newWindow1 != null ) 
		{
			newWindow1.close()  
		}
		var nome = TRIM(document.frmSelect.txtQuery.value)
		nome =  nome.toUpperCase()
		var tipo = document.frmSelect.cboTipo.value
			
		sQry = replace(nome,"%","$");
			
		f  = 'SQL_SalvaQuery.asp?sql=' + sQry + '&tipo=' + tipo
		newWindow1 = window.open(f, 'Salva',"toolbar=0;location=0,directories=0,status=0,scrollbars=0,resizable=0,copyhistory=0,width=480,height=300,screenX=200,screenY200")
	}
		
	function Validator1()
	{ 
			
		if (TRIM(document.frmTabella.cboTabella.value) == "N" ) 
		{		alert("Inserire il nome della Tabella!");
				document.frmTabella.cboTabella.focus();
				return (false);
		}
		return (true);
	}
		
	function Validator2()
	{	var nome = TRIM(document.frmSelect.txtQuery.value)
		nome =  nome.toUpperCase()
			
		if (TRIM(document.frmSelect.txtQuery.value) == "") 
		{		alert("Inserire la query!");
				document.frmSelect.txtQuery.focus();
				return (false);
		}
		if (nome.substring(0,6) != "SELECT") 
		{		alert("Si possono eseguire solo delle select.");
				document.frmSelect.txtQuery.focus();
				return (false);
		}
		if (document.frmSelect.cboTipo.value == "N" ) 
		{		alert("Scegliere il tipo di report.");
				document.frmSelect.cboTipo.focus();
				return (false);
		}
		return (true);
	}	
