<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include Virtual = "/strutt_testa2.asp"-->
<!--#include virtual ="/util/portallib.asp"-->
<!--#include virtual ="/include/openconn.asp"-->
<!--#include virtual ="/include/ckProfile.asp"-->

<script language="Javascript" src="/include/ControlString.inc"></script>
<script language="Javascript" src="/Include/help.inc"></script>
<script language="Javascript" src="ChkReport.js"></script>

<%
	Dim fso			' object - variabile globale usata in CreateFile
	Dim	fAss		' file - variabile globale usata in CreateFile
	Dim sNomeFile	' string - variabile globale usata in CreateFile
	Dim sPath		' string - variabile globale usata in CreateFile
	Dim sprj        ' string - variabile globale usata in CreateFile
	
	If Not ValidateService(session("IdUtente"),"SQL_RicReport",cc) Then 
		response.redirect "/util/error_login.asp"
	End If
	
	sPrj = replace(Session("Progetto"),"/","\")
%>
<table border="0" width="520" cellspacing="0" cellpadding="0" height="81">
   <tr>
     <td width="500" background="<%=Session("Progetto")%>/images/titoli/strumenti2b.gif" height="81" valign="bottom" align="right">
       <table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
         <tr>
           <td width="100%" valign="top" align="right"><b class="tbltext1a">Estrattore Dati</span></b></td>
         </tr>
       </table>
     </td>
   </tr>
</table>
<br>
<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
	<tr height="17">
		<td class="sfondomenu" width="67%" height="18"><span class="tbltext0">
			<b>&nbsp;ESTRATTORE DATI </b>
		</td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
		<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
	</tr>
	<tr>
		<td class="sfondocomm" width="57%" colspan="3">
			<br>Selezionare la tabella di cui si vuole visualizzare la struttura o inserire l'istruzione di estrazione<b></b> 
			<a href="Javascript:Show_Help('/pgm/Help/Monitor/Report/Rep_Sql/SQL_RicReport')">
			<img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
		</td>
	</tr>
	<tr height="2">
		<td class="sfondocomm" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
	</tr>
</table>
<%
	sTipo = Request.QueryString("tipo")
	sSql = Request.QueryString("sql")
	sSql = replace(sSql,"$","%")	
	sNome = Request.QueryString("nome")

	If sNome <> "" Then
		CreateFile(sNome)
		WriteRecord(sSql)
		CloseFile()
	End If	
%>	
<br><br>
<table border="0" CELLPADDING="0" CELLSPACING="0" width="500" align="center">
	<tr>
		<td>
			<span class="tbltext1"><b>&nbsp;VISUALIZZAZIONE DELLA STRUTTURA DELLA TABELLA</b></span>
		</td>
	</tr>
	<tr height="2">			
		<td background="<%=Session("Progetto")%>/images/separazione.gif"></td>
	</tr>
</table><br>
<form method="post" action="SQL_VisReport.asp" name="frmTabella" onSubmit="return Validator1()">
<table width="500" border="0" align="center" cellspacing="2" cellpadding="1">
		<tr>
			<td align="center" class="tbltext" >
				<b>Tabella</b>
			</td>
			<td align="left" class="tblblack" >
<%                                                     'prima era la tab di oracle CAT         
					sqlTable = "SELECT table_name FROM information_schema.TABLES WHERE table_type in ('BASE TABLE','VIEW','SYNONYM') ORDER BY table_name"
'PL-SQL * T-SQL  
SQLTABLE = TransformPLSQLToTSQL (SQLTABLE) 
					set rsTable = CC.Execute(sqlTable)	
%>					<select name="cboTabella">
						<option value="N"></option>				
<%					Do While Not rsTable.Eof %>					
						<option value="<%=rsTable("table_name")%>"><%=rsTable("table_name")%></option>
<%						rsTable.Movenext
					Loop
					rsTable.Close
					set rsTable = nothing
%>					</select>
			</td>
			<td align="left" width="130" class="tbltext">				
				<input type="image" src="<%=Session("progetto")%>/images/conferma.gif" title="Invia" border="0" id="image1" name="image1" align="absBottom">				
				<input type="hidden" name="cboTipo" value="VIDEO">
			</td>
		</tr>
</table>
</form>
<br><br><br><br>
<table border="0" CELLPADDING="0" CELLSPACING="0" width="500" align="center">
	<tr>
		<td>
			<span class="tbltext1"><b>&nbsp;INTERROGAZIONE DELLE TABELLE</b></span>
		</td>
	</tr>
	<tr height="2">			
		<td background="<%=Session("Progetto")%>/images/separazione.gif"></td>
	</tr>
</table>
<br>
<form method="post" action="SQL_VisReport.asp" name="frmSelect" onSubmit="return Validator2()">			
<table width="500" border="0" cellspacing="2" cellpadding="1">
	<tr>
		<td align="left" colspan="3" class="tbltext" width="500">
			<b>Select da eseguire</b>
		</td>
	</tr>	
	<tr>
		<td align="left" colspan="3" class="tblblack" width="500">
			<textarea name="txtQuery" cols="60" rows="12"><%=sSql%></textarea>
		</td>	
	</tr>
	<tr>
		<td align="left" width="20%" class="tblblack">
			<a href="javascript:Controlla()"><img src="<%=Session("progetto")%>/images/conferma.gif" title="Invia" border="0" id="image1" name="image1" align="absBottom"></a>			
		</td>
		<td align="left" width="20%" class="tblblack">
			<a href="javascript:VisQ()"><img src="<%=Session("progetto")%>/images/Visualizza.gif" title="Visualizza" border="0" id="image2" name="image2" align="absBottom"></a>			
		</td>
		<td align="right" width="60%" class="tbltext">
			<b>
			Tipo di output : 
			</b>
			<select name="cboTipo">
				<option value="VIDEO" selected> A video </option>
<%					If sTipo = "FILE" Then %>
						<option value="FILE" selected> Su file </option>
<%					Else %>
						<option value="FILE"> Su file </option>
<%					End If %>
			</select>
		</td>	
	</tr>
	<tr> 
		<td colspan="3" align="middle">
			<b>&nbsp;</b>
		</td>
	</tr>
	<tr>			
		<td colspan="3" align="middle">
			<input type="image" src="<%=Session("progetto")%>/images/lente.gif" title="Esegui Query" border="0" id="image3" name="image3" align="absBottom">
		</td>
	</tr>
</table>
</form>	
<!--#include virtual ="/include/closeconn.asp"-->
<!--#include Virtual = "/strutt_coda2.asp"-->
<%
' creazione file report e scrittura testata 
Sub CreateFile (nome) 
	dim sTesto
		
	sNomeFile = Session("idutente") & "_Sql_" & nome & ".txt"
	sPath=Server.MapPath ("\") & sPrj & "\DocPers\Estrattore\"
	on error resume next 
	Set fso = CreateObject("Scripting.FileSystemObject")
	Set fAss = fso.CreateTextFile(sPath & sNomeFile,true)	' creazione file
	If err > 0 Then
		Response.Write "<b class='tbltext'>Non risulta presente la cartella Estrattore </b>"
		Response.End
	End If
'scrittura della testata del report
	fAss.WriteLine "Elaborato il " & day(Now) & "/" & month(now) & "/" & year(now) & " Ore " & hour(now) & ":" & minute(now)
		
	fAss.WriteLine ""		
	fAss.WriteLine "Report: Interrogazione base dati"
	fAss.WriteLine ""		
End Sub

' chiusura file e release memoria 
Sub CloseFile () 
	fAss.Close		
	Set fAss = nothing
	Set fso = nothing
End Sub

Sub WriteRecord(Rec)
		fAss.WriteLine Rec 
End Sub
%>
