<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include Virtual = "/strutt_testa2.asp"-->
<!--#include Virtual = "/include/openconn.asp"-->
<!--#include virtual = "include/ControlDateVB.asp"-->
  
<title>Situazione Test</title>

<!--	BLOCCO SCRIPT		-->
<!-- JavaScript immediate script -->

<script LANGUAGE="JavaScript">
<!--#include virtual = "Include/help.inc"-->

<!--#include virtual = "/Include/ControlDate.inc"-->

function Validate(TheForm){
   
	if (document.frm1.cmbDescBando.value ==""){
	    alert("Bando obbligatorio")
	    document.frm1.cmbDescBando.focus ();
	    return false
	}
	if (document.frm1.cmbProvBan.value ==""){
	    alert("Provincia obbligatoria")
	    document.frm1.cmbProvBan.focus ();
	    return false
	}
	if (document.frm1.cmbQue.value ==""){
	    alert("Test obbligatorio")
	    document.frm1.cmbQue.focus ();
	    return false
	}
	if (document.frm1.txtData.value !=""){
	    if (!ValidateInputDate(document.frm1.txtData.value)){
			document.frm1.txtData.focus(); 
			return false
		}
		if (!ValidateRangeDate(document.frm1.txtData.value,document.frm1.txtDataOdierna.value)){
				alert("La Data non deve essere superiore alla data odierna")
				document.frm1.txtData.focus(); 
				return false
		} 
	}
	return true	

}

function InviaProv(){
   document.frm1.action ="QUE_RicReport.asp" 
   document.frm1.submit();  
}		
</script>
	
	
<!--	FINE BLOCCO SCRIPT	-->
<!--	FINE BLOCCO HEAD	-->
<!--	BLOCCO ASP			-->
<%  
	Sub Inizio()
%>	
    <br>
	<table cellpadding="0" cellspacing="0" width="500" border="0">
       <tr height="18">
		   <td class="sfondomenu" height="18" width="45%">
		       <span class="tbltext0"><b>&nbsp;SITUAZIONE TEST</b></span></td>
		   <td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
		   <td valign="middle" align="right" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1">(*) campi obbligatori</td>
       
       </tr>
       <tr height="2">
		   <td width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif"></td>     
	   </tr>
       <tr width="371" class="SFONDOCOMM">
		   <td colspan="3">Controllo esito dei test effettuati.
		       <br>Premere <b>Invia</b> dopo aver selezionato i parametri di selezione.
		       <a href="Javascript:Show_Help('/Pgm/help/monitor/report/Quest/QUE_RicReport')" name="prov3" onmouseover="javascript:window.status='' ; return true"><img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
		   </td>
	   </tr>
	   <tr height="2">
		   <td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif">
		   </td>
	   </tr>
     </table>
<%
	End Sub
%>
<!--------------------------------------------------------------------------------------------------------------------------------------------------------->
<%  ' Viene creato il combo/la label con le provincie/a di competenza dell'utente se 
	
	Sub ImpostaPag()
	    dim sStrProv
		Dim sBando	
       	sBando = Request.Form("cmbDescBando") 

%>		<form method="post" action="QUE_VisReport.asp" onsubmit="return Validate(this)" name="frm1">
			<table border="0" cellspacing="0" cellpadding="3" width="500">
				 <tr height="30">
			         <td class="tbltext1" align="left" width="100">
			              <b>&nbsp;&nbsp;Bando*</b>
		             </td>
		             <td align="left" width="300">
		                <span class="tbltext3">
<%                        if sBando <> "" then
                              strcombo= session("iduorg") & "|" & sBando & "|cmbDescBando|onchange='Javascript:InviaProv()'" 
			                  CreateBandi strcombo
			              else
			                  strcombo= session("iduorg") & "||cmbDescBando|onchange='Javascript:InviaProv()'" 
			                  CreateBandi strcombo
			              end if    			
%>				          
                        </span>
		             </td>
                 </tr> 
	
		        <tr>	
			<td align="left" class="tbltext1" width="100">
			<b>&nbsp;&nbsp;Provincia*</b>
			</td>
<%          if sBando <> "" then
               sProv = SetProvBando(sBando,session("idUorg"))
			   sStrProv = ""
%>       	   <td align="left" width="300">			 	 
				  <select NAME="cmbProvBan" class="textblack">			 
				  <option>&nbsp;</option>
				  <option value="TPROV">-TUTTE LE PROVINCE-&nbsp;</option>
<%				  nProvSedi = len(sProv)
                 
			        if nProvSedi <> 0 then
	                    pos_ini = 1 
	                    for i=1 to nProvSedi/2
	                   		sTarga = mid(sProv,pos_ini,2)
	                   		sStrProv = sStrProv & "'" & sTarga & "',"										' DecCodVal(nome_tabella, isa, dt_decor, codice, flag_conn)
%>
		                   <option value="<%=sTarga%>"><%=DecCodVal("PROV", 0, "", sTarga,"")%></option>
<%	                    pos_ini = pos_ini + 2
	                    next
                    end if
%>
			         </select>
				</td>
				<input type="hidden" name="txtProv" value="<%=mid(sStrProv,1,len(sStrProv) - 1)%>">
		
<%          else  %>
                <td align="left" width="300">
                  <select NAME="cmbProvBan" class="textblack">			 
				  <option value>&nbsp;</option>
				  </select>
				</td>  
<%          end if  
%>				
		</tr>   
		<tr>
				 <td align="left" width="100" class="tbltext1" valign="center"><b>&nbsp;&nbsp;Test*&nbsp;</b></td>
<%                  set RR=server.CreateObject("ADODB.Recordset")
				      
		     sql=" SELECT ID_QUESTIONARIO,TIT_QUESTIONARIO FROM QUESTIONARIO WHERE ID_PROJOWNER ='" & mid(session("progetto"),2) & "' ORDER BY ID_QUESTIONARIO"
		   
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
		     RR.open sql, CC, 3

             if not RR.eof  then
                  RR.MoveFirst	
%>
				 <td align="left" width="300"><input type="hidden" name="pluto" value>
				  <select ID="cmbQue" name="cmbQue" class="textblack">
				   <option value selected></option>
				    
				   <%dim appoQue
				     dim sQuest
				     sQuest =""   
						Response.Write "<OPTION value ='TQUE'> " & "-TUTTI I QUESTIONARI-"  & "</OPTION>"%>
<%				     do until RR.EOF  %>
                <option value="<%=RR.fields("ID_QUESTIONARIO")%>"><%=RR.fields("TIT_QUESTIONARIO")%></option>
<%                   sQuest = sQuest & RR.fields("ID_QUESTIONARIO") & ","
                     RR.movenext
                     loop %>
 		      </select>
 		     </td>
 		     <input type="hidden" name="txtQuest" value="<%=mid(sQuest,1,len(sQuest)-1)%>">
 <% 		     end if
                 RR.close
 %>		</tr>   
		<tr>
			 <td align="left" width="100" class="tbltext1" valign="center"><b>&nbsp;&nbsp;Data esec.(gg/mm/aaaa)</b></td>
			 <td align="left">
			   <span class="tbltext1">
					<input type="text" name="txtData" size="10" maxlength="10" class="textblacka"> 
				  </span>
			 </td>
		</tr>
	</table>
			<br>
			<table border="0" cellspacing="0" cellpadding="0" width="500">
				<tr align="center">
					<td> 
						<input type="image" name="Conferma" src="<%=Session("progetto")%>/images/lente.gif">
					</td>
				</tr>
			</table>
				<input type="hidden" name="txtDataOdierna" value="<%= Date()%>"> 
				 
			</form>
<%
	End Sub
%>	
<!--------------------------------------------------------------------------------------------------------------------------------------------------------->
<!--	FINE BLOCCO ASP		-->
<!--		MAIN			-->

<%
	Dim strConn, sModo
	Dim sPrv, sCf,Rif,sEleProv
%> 
  <!--#include Virtual = "/include/DecCod.asp"-->
	<!--#include Virtual ="/Util/DBUtil.asp"-->
	<!--#include virtual ="/util/portallib.asp"-->
    <!--#include virtual = "include/SelAreaTerrBandi.asp"-->

<%
	if not (ValidateService(session("idutente"),"QUE_RICREPORT", CC)) then 
	    response.redirect "/util/error_login.asp"
    end if
	
	Inizio()	
	ImpostaPag()
%>	
    <!--#include Virtual = "/include/closeconn.asp"-->	
	<!--#include Virtual = "/strutt_coda2.asp"-->

<!--	FINE BLOCCO MAIN	-->
