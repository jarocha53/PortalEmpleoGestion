<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include Virtual = "/include/openconn.asp"-->
<!--#include virtual = "include/ControlDateVB.asp"-->
  
<title>Situazione Test</title>

<!--	BLOCCO SCRIPT		-->
<!-- JavaScript immediate script -->
<link REL="STYLESHEET" TYPE="text/css" HREF="<%=session("progetto")%>/fogliostile.css">

<script LANGUAGE="JavaScript">
<!--#include virtual = "Include/help.inc"-->
</script>
	
	
<!--	FINE BLOCCO SCRIPT	-->
<!--	FINE BLOCCO HEAD	-->
<!--	BLOCCO ASP			-->
<%  
	Sub Inizio()
%>	
    <br>
	<table cellpadding="0" cellspacing="0" width="500" border="0">
       <tr height="18">
		   <td class="sfondomenu" height="18" width="45%">
		       <span class="tbltext0"><b>&nbsp;SITUAZIONE TEST</b></span></td>
		   <td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
		   <td valign="middle" align="right" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1"></td>
       
       </tr>
       <tr height="2">
		   <td width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif"></td>     
	   </tr>
       <tr width="371" class="SFONDOCOMM">
		   <td colspan="3">Visualizzazione esito dei test effettuati.
		       <br>Premere <b>Chiudi</b> per chiudere la pagina.
		       <a href="Javascript:Show_Help('/Pgm/help/monitor/report/Quest/QUE_VisDett')" name="prov3" onmouseover="javascript:window.status='' ; return true"><img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
		   </td>
	   </tr>
	   <tr height="2">
		   <td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif">
		   </td>
	   </tr>
     </table>
<%
	End Sub
    
    sub Fine()
%>    
        <br><br>
        <table cellpadding="0" cellspacing="0" width="500" border="0">
	        <tr><td>&nbsp;</tr></td>
		    <td align="center">
				<a href="javascript:self.close()"><img src="<%=Session("progetto")%>/images/chiudi.gif" title="Chiudi la pagina" border="0" align="absBottom"></a>
			</td>
	    </table> 
 <%  end sub
 %>
<!--------------------------------------------------------------------------------------------------------------------------------------------------------->
<%  ' Viene creato il combo/la label con le provincie/a di competenza dell'utente se 
	
	Sub ImpostaPag()
	    dim sTitolo,sUtente,sQuestionario,sql
	    
	   	sTitolo = Request.QueryString ("txtTitoloQ") 
        sUtente = Request.QueryString ("txtUteQ") 
        sQuestionario = Request.QueryString ("txtidQ") 
        
        sql = "SELECT DT_ESEC,NUM_PUNTI,NUM_RISPESA,NUM_TENTATIVI,NUM_ACCESSI" &_
              " FROM RISULT_QUEST WHERE ID_PERSONA = " & sUtente &_
              " AND ID_QUESTIONARIO = " & sQuestionario

'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
        set rsQuest = cc.execute (sql)
        
        if not rsQuest.eof then
	%>      <br>
	        <table width="500" border="0" cellspacing="2" cellpadding="2">
		        <tr class="sfondocomm">
			        <td height="20"><b>Descrizione<br>Questionario</b></td>
			        <td height="20"><b>Data<br>Esecuzione</b></td>
			        <td height="20"><b>Punteggio</b></td>
			        <td height="20"><b>Risposte<br>Esatte</b></td>
			        <td height="20"><b>Numero<br>Accessi</b></td>
	    	        <td height="20"><b>Numero<br>Tentativi</b></td>
			    </tr>
			    <tr class="tblsfondo">
			        <td height="20" class="tbltext1"><%=sTitolo%></td>
			        <td height="20" class="tbltext1"><%=rsQuest("DT_ESEC")%></td>
			        <td height="20" class="tbltext1"><%=rsQuest("NUM_PUNTI")%></td>
			        <td height="20" class="tbltext1"><%=rsQuest("NUM_RISPESA")%></td>
			        <td height="20" class="tbltext1"><%=rsQuest("NUM_ACCESSI")%></td>
	    	        <td height="20" class="tbltext1"><%=rsQuest("NUM_TENTATIVI")%></td>
			    </tr>
	        </table> 
<%	    else  %>
            <br><br>
			<table width="500" cellspacing="0" cellpadding="0" border="0">
				<tr>
					<td align="center">
						<b class="tbltext1"><span class="size">
						Non risultano informazioni aggiuntive per il nominativo selezionati
						</b></span>
					</td>
				</tr>
			</table>
			<br><br><br>   
<%	    end if
        set rsQuest = nothing
        Fine()
	End Sub
%>	
<!--------------------------------------------------------------------------------------------------------------------------------------------------------->
<!--	FINE BLOCCO ASP		-->
<!--		MAIN			-->

    <!--#include Virtual = "/include/DecCod.asp"-->
	<!--#include Virtual ="/Util/DBUtil.asp"-->
	<!--#include virtual ="/util/portallib.asp"-->

<%
	if not (ValidateService(session("idutente"),"QUE_RICREPORT", CC)) then 
	    response.redirect "/util/error_login.asp"
    end if
	
	Inizio()
	ImpostaPag()
%>	
    <!--#include Virtual = "/include/closeconn.asp"-->	

<!--	FINE BLOCCO MAIN	-->
