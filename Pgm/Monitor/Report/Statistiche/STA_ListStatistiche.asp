<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include Virtual = "/strutt_testa2.asp"-->
<!--#include Virtual = "/include/openconn.asp"-->
<!--#include virtual = "/include/ControlDateVb.asp"-->
<!--#include virtual="/util/dbutil.asp"-->
<!--#include virtual="/include/DecComun.asp"-->
<!--#include Virtual = "/include/DecCod.asp"-->
<!--#include virtual = "/include/SelAreaTerrBandi.asp"-->
<%
Select Case Ucase(session("progetto"))
	Case "/ARITES"
		Depprov="Provincial"
		CEPE = "Oficina"        
    Case "/UYITES"
		Depprov="Departamental"
		CEPE = "Oficina"        
    Case "/BA"
		Depprov="Departamental"
		CEPE = "Centro de Atenci�n"
End Select
%>
<script language="javascript" src="Util.js">
</script>
<script language="javascript">
<!--#include virtual = "/Include/help.inc"-->
</script>
<%Inizio() %>
<br>
<table cellpadding="0" cellspacing="0" width="488" border="0">
	<tr height="18">
		<td class="sfondomenu" height="18" width="45%">
		<span class="tbltext0"><b>&nbsp;ESTAD&Iacute;STICAS</b></span></td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
		<td valign="middle" align="right" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1"></td>
	</tr>
	<tr height="2">
		<td width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif"></td>     
	</tr>
</table>
<table cellpadding="0" cellspacing="0" width="488" border="0">
	<tr width="371">
		<td colspan="3" class="sfondocomm" style=text-align:justify;>
			Es posible consultar un tipo de estad�stica seleccionando uno de los tipos listados a continuaci�n.
			<!--a href="Javascript:Show_Help('/Pgm/help/monitor/report/statistiche/STA_ListStatistiche')" name="prov3" onmouseover="javascript:window.status='' ; return true"><img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a-->
			Una vez seleccionada se visualizar� el resultado en pantalla y podr� imprimirse el reporte si lo desea.
			<br><br>Pueden seleccionarse filtros para visualizar un determinado &aacute;mbito territorial. <br>El resultado de la b�squeda ser� diferente seg�n el valor seleccionado en el campo <b>&Aacute;mbito Territorial.</b><!-- es decir --><br><br>		
			<%if Ucase(session("progetto")) <> "/BA" then%>
				<b>Todos</b>: corresponde a  buscar todas las personas inscritas en la plataforma <br>
			<%end if%>
			<b>Nacional</b>: permite buscar las personas registradas en todo el Pa�s <br>
			<b><%=Depprov%></b>: permite buscar las personas registradas en el departamento al que pertenece el <%=CEPE%> del operador que est� solicitando la b�squeda. <br>
			<b><%=CEPE%></b>: permite extraer s�lo las personas asociadas a la oficina de pertenencia del operador. <br>			
		</td>
	</tr>
	<tr height="2">
		<td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
	</tr>
</table>
<%
sSQL = "SELECT RAG_SOC||' ('||DESCRIZIONE||')' as Descrizione, PRV FROM SEDE_IMPRESA SI, IMPRESA I" &_
	" WHERE SI.ID_IMPRESA = I.ID_IMPRESA AND SI.ID_SEDE = " & CLng(Session("Creator"))
	
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
set rsCPI = CC.Execute(sSQL)
'Response.Write ssql & "<br>---------"
if not rsCPI.EOF then
	sDescCPI = rsCPI("Descrizione")
	PRV = rsCPI("PRV")
else
	sDescCPI = " - "
end if
rsCPI.Close
set rsCPI = nothing

sSQL = "SELECT COGNOME,NOME FROM UTENTE WHERE IDUTENTE = " & CLng(Session("idutente"))
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
set rsDatiOp = CC.Execute(sSQL)
'Response.Write ssql & "<br>---------"
if not rsDatiOp.eof then
	sNome	= rsDatiOp("NOME") & "  " & rsDatiOp("COGNOME")
else
	sNome	= ""
end if
rsDatiOp.Close
set rsDatiOp = nothing
%>
<form name="frmDatiSede" method="post">
<br>
<table cellpadding="2" cellspacing="2" width="488" border="0">
	<tr>
		<td width="20%" class="tbltext1" align="left"><b>
				 	<%=CEPE%>:
		</b></td>
		<td width="70%" class="textblack" align="left"><b><%=sDescCPI%></b></td>
	</tr>
	<tr>
		<td width="20%" class="tbltext1" align="left"><b>Funcionario :</b></td>
		<td width="70%" class="textblack" align="left"><b><%=sNome%></td>
	</tr>
	<tr><td COLSPAN="2">&nbsp;</td></tr>
	<tr height="2">
		<td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif">
		</td>
	</tr>	
	<tr>
		<input type="hidden" name="CPI" value="<%=sDescCPI%>">
		<input type="hidden" name="txtProvincia" value="<%=PRV%>">
	</tr>	
</table>
<br>
<table cellpadding="0" cellspacing="0" width="488" border="0">	
	<tr>
		<td class="tbltext1" align="center" width="100%">
			<%if ExistBando (session("iduorg"))=true then%>
			Pueden ser seleccionados filtros para visualizar un determinado &aacute;mbito territorial
			<%end if%>
		</td>	
	</tr>	
</table>
<br>
<table cellpadding="0" cellspacing="0" width="488" border="0">	
		<tr>	
		<td class="tbltext1" align="left" width="80" align="left">
			<b>&Aacute;mbito Territorial</b>
		</td>
        <td class="tbltext" align="left"  width="80" align="left">
		<Select class='textblack' name='cmbAmbito'>
			<%if Ucase(session("progetto")) <> "/BA" then%>
				<OPTION value ='-1'>Todos</OPTION>
			<%end if%>	
			<OPTION value ='1'>Nacional</OPTION>
			<OPTION value ='2'><%=Depprov%></OPTION>
			<OPTION value ='0'><%=CEPE%></OPTION>
		</Select> 
         </td>
		<td class="tbltext" align="center" width="120"><b><%'if ExistBando(session("iduorg"))=true then Response.write "Llamado" end if%></b></td>
		<td align="left">
		<span class="tbltext3">
			<%
			' Controlla se ci sono Bandi disponibili
			'StrCombo = session("iduorg") & "||CmbDescBando|'" 
			'CreateBandi StrCombo
			'Response.Write "<SELECT ID='CmbDescBando' class=textblack name='CmbDescBando''><OPTION value=></OPTION><option value='1' >OBJETIVO EMPLEO</option></select>"
			%>
        </span>
		</td>
	</tr>  
	<tr><td>&nbsp;</td>
	</tr>
</table>

<table cellpadding="2" cellspacing="2" width="488" border="0">
<% 
'Sql = "SELECT DESFUNZIONE,WEBPATHFUN FROM FUNZIONE G, GRUPPOFUN GF"
'Sql = Sql &  " WHERE G.GRUPPOFUNZIONE = 'STATISTICHE' "
'Sql = Sql &  " AND GF.IDGRUPPO = " & clng(Session("idgruppo")) &_
'" AND G.IDFUNZIONE = GF.IDFUNZIONE"

' EPILI 31/07/2002
' Prende tutti i report statistici.
'---------------------------------------------------------------------
'--comente desde aca para que no se muestren las estadisticas
'---------------------------------------------------------------------
 Sql = "SELECT DESFUNZIONE,WEBPATHFUN, GRUPPOFUNZIONE FROM FUNZIONE "
 Sql = Sql & " WHERE GRUPPOFUNZIONE LIKE 'STATISTICHE%' ORDER by GRUPPOFUNZIONE, DESFUNZIONE"


'response.Write Sql
 set rsFunStat = CC.Execute(Sql)
 i=0
 Titolo = ""
 if not rsFunStat.EOF then
	do while not rsFunStat.EOF
		if trim(Titolo) <> trim(rsFunStat("GRUPPOFUNZIONE")) then 
		Titolo=rsFunStat("GRUPPOFUNZIONE") 'trim(mid(rsFunStat("GRUPPOFUNZIONE"),12))
		%><tr><td><b class="tbltext1"><br><%=replace(MkTit(Titolo),"STATISTICHE", "ESTAD&Iacute;STICAS")%><br><br></b></a></td></tr><%
		end if
		%>		
		<input type="hidden" name="sNamPag" value="<%=rsFunStat("WEBPATHFUN")%>">
		<tr>
			<td align="left"><a onmouseover="javascript:status='' ; return true" href="Javascript:VisStatistica(<%=i%>)" ID="imgPunto1" name="imgPunto1" >
			<img border="0" src="<%=Session("Progetto")%>/images/bullet1.gif">&nbsp;
			<b class="tbltext1">
			<%=rsFunStat("DESFUNZIONE")%></b></a></td>
		</tr>
		<%
		i=i+1
		rsFunStat.MoveNext
	loop
 else
  ' Gestire errore 
end if
 rsFunStat.close
 set rsFunStat = nothing 
%>
</table>
</form>	
<%sub inizio()%>
  <table border="0" width="520" cellspacing="0" cellpadding="0" height="81">
    <tr>
      <td width="520" background="<%=Session("Progetto")%>/images/titoli/strumenti2b.gif" height="81" valign="bottom" align="right">
        <table border="0" background width="260" height="30" cellspacing="0" cellpadding="0">
          <tr>
            <td width="100%" valign="top" align="right"><b class="tbltext1a">ESTAD&Iacute;STICAS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></b></td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
<%
end sub

Function MkTit(Tl)
	for ct=1 to 30 - cint(len(Tl)/2): sep = sep & "_" : next
	MkTit = sep & " " & Tl & " " & sep
End Function
%>
<!--#include Virtual ="/Include/closeconn.asp"-->
<!--#include Virtual="/strutt_coda2.asp"-->