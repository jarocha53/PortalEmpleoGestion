<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include Virtual = "/strutt_testa2.asp"-->
<!--#include Virtual = "/include/openconn.asp"-->
<!--#include virtual = "/include/ControlDateVb.asp"-->
<!--#include virtual="/util/dbutil.asp"-->
<!--#include virtual="/include/DecComun.asp"-->
<!--#include Virtual = "include/DecCod.asp"-->
<script language="javascript" src="Util.js">
</script>
<script language="javascript">
<!--#include virtual = "/Include/help.inc"-->
</script>

<%
'---------------------------------------------
' Titulos
'---------------------------------------------
Inizio() 

'-----Pino agosto 2007 - asteriscata perch� non disponibile 
'----------------------------------------------------------------------------------------------------
'	Coneccion a Abrir: Base DataWareHouse SQL (** Se usa solo para reportes estadisticos ***)
'----------------------------------------------------------------------------------------------------
'	'Determina ServerName
'    'If Len(ServerName) = 0 Then
'	    ServerName = Request.ServerVariables("SERVER_NAME")
'	    Application("SERVER_NAME") = ServerName
'   End If
'    ServerName = Request.ServerVariables("SERVER_NAME")
'
'	Set connEmpleoDataWareHouse = Server.CreateObject("adodb.connection") ' Objeto de Conexion a la Base EmpleoProyectos
'	with connEmpleoDataWareHouse
'	    .ConnectionString = Application(ServerName & "_DataWareHouseConnectionString")	
'	    .ConnectionTimeout = Application(ServerName & "_DataWareHouseConnectionTimeout")
'	    .CommandTimeout = Application(ServerName & "_DataWareHouseCommandTimeout")
'	    .CursorLocation = Application(ServerName & "_DataWareHouseCursorLocation")
''PL-SQL * T-SQL  
'	    .Open ,Application(ServerName & "_DataWareHouseRuntimeUserName"), Application(ServerName & "_DataWareHouseRuntimePassword")
'	end with

'---------------------------------------------
' Help inicial
'---------------------------------------------
%>
<br>

<table cellpadding="0" cellspacing="0" width="488" border="0">
   <tr height="18">
		<td class="sfondomenu" height="18" width="45%">
		<span class="tbltext0"><b>&nbsp;ESTADISTICAS</b></span></td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
		<td valign="middle" align="right" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1"></td>
   </tr>
   <tr height="2">
		<td width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif"></td>     
	</tr>
   <tr width="371" class="SFONDOCOMM">
		<td colspan="3">Es posible consultar un tipo de estad�stica
			seleccionando uno de los tipos listados a continuaci�n.
			Una vez seleccionada se visualizar� el resultado en pantalla y podr� imprimirse el reporte si lo desea.
			<a href="Javascript:Show_Help('/Pgm/help/monitor/reporting/Service/ListaDeReportes')" name="prov3" onmouseover="javascript:window.status='' ; return true"><img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
		</td>
	</tr>
	<tr height="2">
		<td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif">
		</td>
	</tr>
</table>

<%
'==============================================================================
' Datos de la oficina y el operador
'==============================================================================
'-------------------------------------
' Oficina actual 
'-------------------------------------
' Oficina actual 
Oficina = CLng(Session("Creator"))
' Datos de la oficina
sSQL = "SELECT RAG_SOC||' ('||DESCRIZIONE||')' as Descrizione, PRV FROM SEDE_IMPRESA SI, IMPRESA I" &_
	" WHERE SI.ID_IMPRESA = I.ID_IMPRESA AND SI.ID_SEDE = " & Oficina
	
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
set rsCPI = CC.Execute(sSQL)
'Response.Write ssql & "<br>---------"
if not rsCPI.EOF then
	sDescCPI = rsCPI("Descrizione")
	PRV = rsCPI("PRV")
else
	sDescCPI = " - "
end if
rsCPI.Close
set rsCPI = nothing

'-------------------------------------
' Datos del operador
'-------------------------------------
sSQL = "SELECT COGNOME, NOME FROM UTENTE WHERE IDUTENTE = " & CLng(Session("idutente"))
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
set rsDatiOp = CC.Execute(sSQL)
'Response.Write ssql & "<br>---------"
if not rsDatiOp.eof then
	sNome	= rsDatiOp("NOME") & "  " & rsDatiOp("COGNOME")
else
	sNome	= ""
end if
rsDatiOp.Close
set rsDatiOp = nothing

'============================================================================
' Arma parametros para el reporte estadistico
'============================================================================
'-------------------------------------
' De los datos de la Oficina Actual
'-------------------------------------
dim Pcia, PciaDesc, Municipio, MunicipioDesc, OficinaDesc
	Pcia = 0
	PciaDesc = ""
	Municipio = 0
	MunicipioDesc = ""
	OficinaDesc = ""

' Datos de la la oficina actual
sSQL = "Select * from SEDE_IMPRESA SI WHERE SI.ID_SEDE = " & Oficina

'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
set rsOficina = CC.Execute(sSQL)
' Encontro la oficina
if not rsOficina.EOF then
	Pcia = rsOficina("PRV")
	PciaDesc = ""
	Municipio = rsOficina("Comune")
	MunicipioDesc = ""
	OficinaDesc = rsOficina("Descrizione")
	
	' Descripcion del municipio de la oficina actual
	dim rsComune 
	sSQL = "SELECT DESCOM from COMUNE WHERE CODCOM = '" & Municipio & "'"  
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	set rsComune = CC.Execute(sSQL) 
	MunicipioDesc = rsComune("DESCOM") 
	rsComune.Close 
	set rsComune = nothing 

	' Descripcion de la provincia  de la oficina actual
	PciaDesc = trim(DecCodVal("PROV", 0, "", Pcia, 0))
	
end if
rsOficina.Close
set rsOficina = nothing


'--------Pino agosto 2007 - asteriscata perch� non disponible
'----------------------------------------------------------
' Busca el parametro en las tablas del DataWareHouse 
'----------------------------------------------------------
' Busca el parametro para los reportes
'dim rsParametros
'dim ReporteParametros
'ReporteParametros = ""

' Oficina definida
'if Oficina <> 0 then
'	sSQL = "SELECT * from OficinasOlap " _
'		& " WHERE Provincia = '" &  PciaDesc & "'" _
'		& " and Municipio = '" & MunicipioDesc & "'" _
'		& " and Oficina = '" & OficinaDesc & "'"
'
'else
'	' Todas las oficinas
'	sSQL = "SELECT * from OficinasOlap " _
'		& " WHERE Provincia is null " _
'		& " and Municipio is null " _
'		& " and Oficina is null"
'end if
'' busca el valor del parametro a  pasar 
''PL-SQL * T-SQL  
'SSQL = TransformPLSQLToTSQL (SSQL) 
'set rsParametros = connEmpleoDataWareHouse.Execute(sSQL) 
'if rsParametros.Recordcount > 0 then
'	ReporteParametros = rsParametros("ParameterValue") 
'end if
'rsParametros.Close 
'set rsParametros = nothing 

'..Response.Write "*" & ReporteParametros & "*"
'..Response.End

' Cierra conexion al data
'connEmpleoDataWareHouse.Close
'set connEmpleoDataWareHouse = nothing

'-----------------------------------------
' resultados
'-----------------------------------------
'Response.Write "<br>" & PciaDesc 
'Response.Write "<br>" & MunicipioDesc 
'Response.Write "<br>" & OficinaDesc 
'Response.Write "<br>" & ReporteParametros 

%>
<form name="frmDatiSede" method="post">

<br>
<table cellpadding="2" cellspacing="2" width="488" border="0">
	<tr>
		<td width="30%" class="tbltext1"><b>Oficina de Empleo :</b></td>
		<td width="70%" class="textblack"><b><%=sDescCPI%></b></td>
	</tr>
	<!--tr>		<td colspan=2 class="tbltext1"><b>Operatore</b></td>	</tr-->	
	<tr>
		<td width="30%" class="tbltext1"><b>Operador :</b></td>
		<td width="70%" class="textblack"><b><%=sNome%></td>
	</tr>	
	<tr>
		<td COLSPAN="2">&nbsp;</td>
	</tr>
	<tr height="2">
		<td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif">
		</td>
	</tr>	
	<tr>
		<input type="hidden" name="CPI" value="<%=sDescCPI%>">
		<input type="hidden" name="txtProvincia" value="<%=PRV%>">
		<input type="hidden" name="ReporteParametros" value="<%=ReporteParametros%>">
	</tr>	
</table>
<br>

<table cellpadding="2" cellspacing="2" width="488" border="0">
<% 

'==============================================================================
' Lista las estadisiticas definidas
'==============================================================================
' Reporting service a utilizar 
PathReportingService = Application("LinkReportingService")

'------------------------------------------------------------------------------
' Arma la lista de estadisticas
'------------------------------------------------------------------------------
'Sql = "SELECT DESFUNZIONE,WEBPATHFUN FROM FUNZIONE G, GRUPPOFUN GF"
'Sql = Sql &  " WHERE G.GRUPPOFUNZIONE = 'STATISTICHE' "
'Sql = Sql &  " AND GF.IDGRUPPO = " & clng(Session("idgruppo")) &_
'" AND G.IDFUNZIONE = GF.IDFUNZIONE"

' EPILI 31/07/2002
' Prende tutti i report statistici.
Sql = "SELECT DESFUNZIONE, webPathFun FROM FUNZIONE "
Sql = Sql &  " WHERE GRUPPOFUNZIONE = 'STATISTICHE' "
Sql = Sql &  " AND DT_TMST > "  & ConvDateToDBs(date) 

'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
set rsFunStat = CC.Execute(Sql)
i=0
if not rsFunStat.EOF then
	do while not rsFunStat.EOF
		%>		
		<input type="hidden" name="sNamPag" value="<%=(PathReportingService & rsFunStat("WEBPATHFUN"))%>">
			<tr>
				<td><a onmouseover="javascript:status='' ; return true" href="Javascript:VisStatistica(<%=i%>)" ID="imgPunto1" name="imgPunto1">
				<img border="0" src="<%=Session("Progetto")%>/images/bullet1.gif">&nbsp;
				<b class="tbltext1">
				<%=rsFunStat("DESFUNZIONE")%></b></a>
				<% ' para ver los links que arma
				Response.Write "" '"<br>" & (PathReportingService & rsFunStat("WEBPATHFUN") & ReporteParametros)
				%>
				</td>
			</tr>
		<%
		' siguiente registro
		i= i + 1
		rsFunStat.MoveNext
	loop
else
	' Gestire errore 
end if
rsFunStat.close
set rsFunStat = nothing 

%>
</table>
</form>	

<%
'------------------------------------------------
' Titulos
'------------------------------------------------
sub inizio()
	%>
	  <table border="0" width="520" cellspacing="0" cellpadding="0" height="81">
	    <tr>
	      <td width="520" background="<%=Session("Progetto")%>/images/titoli/strumenti2b.gif" height="81" valign="bottom" align="right">
	        <table border="0" background width="260" height="30" cellspacing="0" cellpadding="0">
	          <tr>
	            <td width="100%" valign="top" align="right"><b class="tbltext1a">ESTADISTICAS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></b></td>
	          </tr>
	        </table>
	      </td>
	    </tr>
	  </table>

	<%
end sub

%>

<!--#include Virtual ="/Include/closeconn.asp"-->
<!--#include Virtual="/strutt_coda2.asp"-->		
