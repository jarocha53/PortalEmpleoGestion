<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%if session("progetto") <> "" then %> 
<!--#include Virtual = "/include/openconn.asp"-->
<!-- #include virtual="/include/SelezioneProvince.asp" -->
<!--#include Virtual = "/include/DecCod.asp"-->
<!--#include virtual = "/include/SelBandi.asp"-->
<!-- #include virtual="/include/ElabRegole.asp" -->


<script language="javascript" src="Util.js">
</script>

<html>

<title>Antiguedad de las inscripciones por sexo y titulo de estudio</title>
<link REL="STYLESHEET" TYPE="text/css" HREF="<%=session("progetto")%>/fogliostile.css">
<body topmargin="0" onafterprint="javascript:buttom.style.visibility='visible';" onbeforeprint="javascript:buttom.style.visibility='hidden'">
<%
dim sRegPro
	if trim(Request.QueryString("sAmb"))="" then
		Ambito = FiltroCPI(true)
	else 
		Ambito = Request.QueryString("sAmb")
	end if	
	
	'Response.Write "ambito" & Ambito
	'Response.Write Request.QueryString("CPI")
	'Response.Write Request.QueryString("sProv")
	sProv = Request.QueryString("sProv")
	
	ValProv = DecodTadesToArray("PROV",date,"CODICE='" & sProv & "'",0,0)
	sDescProv = ValProv(0,1,0)
	'Response.Write sDescProv
	'DecCodVal(nome_tabella, isa, dt_decor, codice, flag_conn
	'Response.Write DescProv
	sDesc=ValProv(0,1,1)
	DescRegione = DecCodVal("REGIO",0,date,sDesc,0)

if ucase(session("progetto"))= "/ARITES" then
	provdep = "la Provincia"
else
	provdep = "el Departamento"
end if
	
	select case(ambito)
		
		case("x")
			titolo = "Distribucion sobre " & _ 
					 "la antiguedad de las inscripciones por sexo y t�tulo de estudio de los inscriptos en: " & _
					 Request.QueryString("CPI")
		case(0)
			titolo = "Distribucion sobre " & _ 
					 "la antiguedad de las inscripciones por sexo y t�tulo de estudio de los inscriptos en: " & _
					 Request.QueryString("CPI") 
		case(1)
			titolo = "Distribucion sobre " & _ 
					 "la antiguedad de las inscripciones por sexo y t�tulo de estudio de los inscriptos en el Territorio Nacional"

		case(2)
			titolo = "Distribucion sobre " & _ 
					 "la antiguedad de las inscripciones por sexo y t�tulo de estudio de los inscriptos en " & provdep & " de " & sDescProv 

		case(3)
		    sRegPro =SetProvince(3)
			titolo = "Distribucion sobre " & _ 
					 "la antiguedad de las inscripciones por sexo y t�tulo de estudio de los inscriptos en la Region de " & DescRegione 				 					 
	end select		 

	
	if trim(Request.QueryString("sBan"))<>"" then
		titolo = titolo & " - Inscriptos a : " & DecBando(Request.QueryString("sBan"))
	end if

dim Sql,rsFasciaA
dim aFascia(1)
aFascia(0) = " <= 12"
aFascia(1) = " > 12"

inizio()

%>

<table cellpadding="0" cellspacing="0" width="488" border="0" align="center">
	<tr height="18">
		<td class="sfondomenu" height="18" width="90%">
		<span class="tbltext0"><b>ANTIGUEDAD DE LA INSCRIPCION POR SEXO Y TITULO DE ESTUDIO</b></span></td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
		<td valign="middle" align="right" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1"></td>
   </tr>
   <tr height="2">
		<td width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif"></td>     
	</tr>
   <tr width="371" class="SFONDOCOMM">
		<td colspan="3">		  
		 <%=titolo%>		
		</td>
	</tr>
	<tr height="2">
		<td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif">
		</td>
	</tr>
</table>
<br>
<table align="center" border="0">
	<tr width="500">
		<td class="textred"><b>&nbsp;situaci�n al : <%=date%></strong></td> 
	</tr>
</table>
<br>
<%
sCpi=Request.QueryString("CPI")

sFileName = Server.MapPath("/") & session("progetto") & "/DocPers/Statistiche/" &_
	Session("IdUtente") & "_ST12.xls"

sFileNameJS = session("progetto") & "/DocPers/Statistiche/" &_
	Session("IdUtente") & "_ST12.xls"

'Response.Write sFileName
set oFileSystemObject	= Server.CreateObject("Scripting.FileSystemObject")

set oFileObj = oFileSystemObject.CreateTextFile(sFileName)

' ********************************
' Scrivo la intestazione del file.
' ********************************
oFileObj.WriteLine ("Duraci�n de la inscripcion por sexo y t�tulo de estudio")
oFileObj.WriteLine ("")
oFileObj.WriteLine (titolo)
'oFileObj.WriteLine ("Resoconto distribuzione per di titolo di studio e sesso")
'oFileObj.WriteLine ("per gli iscritti a: " & sCpi)
oFileObj.WriteLine ("")


aLstud = decodTadesToArray("LSTUD",date,"",2,"0")

for i = 0 to ubound(aFascia)
oFileObj.WriteLine ("")
sFascia = Replace(aFascia(i),"<=","inferior a")

	sFascia = Replace(sFascia,">","superior a")
	sFascia = sFascia & " meses"
%><br>
		
<%	oFileObj.WriteLine ("Antiguedad de Inscripcion" & sFascia)
	oFileObj.WriteLine ("")
	oFileObj.WriteLine ("TITULO DE ESTUDIO" & chr(9) & "FEMENINOS" & chr(9) & "MASCULINOS")
%>
	<table border="0" width="488" cellspacing="2" cellpadding="2" align="center">
		<tr class="tblsfondo">
			<td width="350" class="tbltext1" colspan="3"><b>Antiguedad Inscripci�n <%=sFascia%></b></td>
		</tr>
		<tr class="tblsfondo">
			<td width="350" class="tbltext1"><b>T�tulo de estudio</b></td>
			<td width="75" class="tbltext1"><b>Femenino</b></td>
			<td width="75" class="tbltext1"><b>Masculino</b></td>
		</tr>
				
<%
	
	perSql=""			
	if trim(Request.QueryString("sBan"))<>"" then
		perSql = " AND p.ID_PERSONA IN (SELECT ID_PERSONA FROM DOMANDA_ISCR WHERE ID_BANDO='" & Request.QueryString("sBan") & "') "
	end if 
	
	for nTC = 0 to ubound(aLstud)-1

select case(ambito)
	case(0)
		'centro impiego
		SQL = "SELECT COUNT(DISTINCT p.id_persona) AS NPER,SESSO " &_
	          "FROM persona p, tistud t,stato_occupazionale so " &_
              "WHERE t.id_persona = p.id_persona and p.id_persona = so.id_persona " &_ 
              perSql & " AND t.cod_liv_stud ='" & aLstud(nTC,nTC,nTC) & "'" &_
              "and t.cod_stat_stud ='0'" &_
              "and MONTHS_BETWEEN(SYSDATE,DT_DEC_ISCR) " & aFascia(i) &_
              " AND (ind_status='0' or " &_
						"(ind_status='2' and " &_
						" ind_status not in (select ind_status from STATO_OCCUPAZIONALE WHERE p.id_persona = id_persona AND id_cimpiego= " & session("creator") & " and ind_status='0'))) " &_
						" and id_cimpiego=" & session("creator") &_
              " group by sesso"
	
	case(1)
		'territorio
		SQL = "SELECT COUNT(DISTINCT p.id_persona) AS NPER,SESSO " &_
	          "FROM persona p, tistud t,stato_occupazionale so " &_
              "WHERE t.id_persona = p.id_persona and p.id_persona = so.id_persona " &_ 
              perSql & " AND t.cod_liv_stud ='" & aLstud(nTC,nTC,nTC) & "'" &_
              "and t.cod_stat_stud ='0'" &_
              "and MONTHS_BETWEEN(SYSDATE,DT_DEC_ISCR) " & aFascia(i) &_
              " AND (ind_status='0' or " &_
						"(ind_status='2' and " &_
						" ind_status not in (select ind_status from STATO_OCCUPAZIONALE WHERE p.id_persona = id_persona AND id_cimpiego= " & session("creator") & " and ind_status='0'))) " &_
						" and id_cimpiego is not null " &_
              "group by sesso"
	case(2)
		'provincia
		SQL = "SELECT COUNT(DISTINCT p.id_persona) AS NPER,SESSO " &_
	          "FROM persona p, tistud t,stato_occupazionale so " &_
              "WHERE t.id_persona = p.id_persona and p.id_persona = so.id_persona " &_ 
              perSql & " AND t.cod_liv_stud ='" & aLstud(nTC,nTC,nTC) & "'" &_
              "and t.cod_stat_stud ='0'" &_
              "AND SO.ID_CIMPIEGO IN (SELECT ID_SEDE FROM SEDE_IMPRESA SI, IMPRESA I " &_
		      "WHERE  SI.ID_IMPRESA =  I.ID_IMPRESA AND I.COD_TIMPR = '03' AND SI.PRV='" & sProv & "')" &_                        
              "AND MONTHS_BETWEEN(SYSDATE,DT_DEC_ISCR) " & aFascia(i) &_
              " AND (ind_status='0' or " &_
						"(ind_status='2' and " &_
						" ind_status not in (select ind_status from STATO_OCCUPAZIONALE WHERE p.id_persona = id_persona AND id_cimpiego= " & session("creator") & " and ind_status='0'))) " &_
						" and id_cimpiego is not null " &_
              "group by sesso"
		    'Response.Write sql
		    
   case(3)
		'regione
		SQL = "SELECT COUNT(DISTINCT p.id_persona) AS NPER,SESSO " &_
	          "FROM persona p, tistud t,stato_occupazionale so " &_
              "WHERE t.id_persona = p.id_persona and p.id_persona = so.id_persona " &_ 
              perSql & " AND t.cod_liv_stud ='" & aLstud(nTC,nTC,nTC) & "'" &_
              "and t.cod_stat_stud ='0'" &_
              "AND SO.ID_CIMPIEGO IN (SELECT ID_SEDE FROM SEDE_IMPRESA SI, IMPRESA I " &_
		      "WHERE  SI.ID_IMPRESA =  I.ID_IMPRESA AND I.COD_TIMPR = '03' AND SI.PRV in(" &  sRegPro & "))" &_                        
              "AND MONTHS_BETWEEN(SYSDATE,DT_DEC_ISCR) " & aFascia(i) &_
              " AND (ind_status='0' or " &_
						"(ind_status='2' and " &_
						" ind_status not in (select ind_status from STATO_OCCUPAZIONALE WHERE p.id_persona = id_persona AND id_cimpiego= " & session("creator") & " and ind_status='0'))) " &_
						" and id_cimpiego is not null " &_
              "group by sesso"
              'Response.Write sql
end select

		set rsFasciaA = server.CreateObject("ADODB.recordset")
		
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
		rsFasciaA.Open Sql,cc,3
		
		rsCount = rsFasciaA.RecordCount
						
		Select case rsCount			
			
			Case "1"
				if rsFasciaA("SESSO") = "F" then    %>
					<tr>
						<td width="350" class="tbltext1"><%=aLstud(nTC,nTC+1,nTC)%></td>	
						<td width="75" class="tbltext1"><%=rsFasciaA("NPER")%></td>
						<td width="75" class="tbltext1">0</td>
					</tr>
<%					oFileObj.WriteLine (aLstud(nTC,nTC+1,nTC) & chr(9) & rsFasciaA("NPER")& chr(9) & "0")	
				else    %>
					<tr>
						<td width="350" class="tbltext1"><%=aLstud(nTC,nTC+1,nTC)%></td>	
						<td width="75" class="tbltext1">0</td>
						<td width="75" class="tbltext1"><%=rsFasciaA("NPER")%></td>
					</tr>
<%					oFileObj.WriteLine (aLstud(nTC,nTC+1,nTC) & chr(9) & "0" & chr(9) & rsFasciaA("NPER"))	
				end if				
			Case "2"   %>	
				<tr>
					<td width="350" class="tbltext1"><%=aLstud(nTC,nTC+1,nTC)%></td>	
					<td width="75" class="tbltext1"><%=rsFasciaA("NPER")%></td>
<%					nPersF = rsFasciaA("NPER") 
					rsFasciaA.MoveNext%>  
					<td width="75" class="tbltext1"><%=rsFasciaA("NPER")%></td>
				</tr>
<%					oFileObj.WriteLine (aLstud(nTC,nTC+1,nTC) & chr(9) & nPersF & chr(9) & rsFasciaA("NPER"))
			Case else  %>
				<tr>
					<td width="350" class="tbltext1"><%=aLstud(nTC,nTC+1,nTC)%></td>
					<td width="75" class="tbltext1">0</td>
					<td width="75" class="tbltext1">0</td>	
				</tr>
<%				oFileObj.WriteLine (aLstud(nTC,nTC+1,nTC) & chr(9) & "0" & chr(9) &  "0")			
		End select		
	
set rsFasciaA = nothing
	next%>
	</table>
<%
next
oFileObj.close
set oFileSystemObject = nothing
set oFileObj = nothing

erase aLstud


fine()


' *************************************************************************************
sub inizio()
%>
  <table border="0" width="488" cellspacing="0" cellpadding="0" height="81" align="center">
    <tr>
      <td width="500" background="<%=Session("Progetto")%>/images/titoli/strumenti2b.gif" height="81" valign="bottom" align="right">
        <table border="0" background width="260" height="30" cellspacing="0" cellpadding="0">
          <tr>
            <td width="100%" valign="top" align="right"><b class="tbltext1a">ESTADISTICAS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></b></td>
          </tr>
          <tr>
            <td width="100%" valign="top" align="right"><BR><BR></td>
          </tr>
        </table>
      </td>
    </tr>
  </table>

<%
end sub
' ************************************************************************************
sub Fine()
	%>
<br><br>
<label id="buttom">
	<table width="488" cellspacing="2" cellpadding="1" border="0" align="center">
		<tr align="center">
			<td>
				<a href="javascript:self.close()"><img src="<%=Session("progetto")%>/images/chiudi.gif" title="Chiudi la pagina" border="0" align="absBottom"></a>
				<a href="javascript:print()"><input type="image" src="<%=Session("progetto")%>/images/stampa.gif" title="Stampa la pagina" border="0" align="center" onclick="self.print();" id="stampa" name="stampa"></a>
			</td>
		</tr>		
		<tr align="center">
			<td>&nbsp;</td>
		</tr>		
		<tr align="center">
			<td>
				<a class="textred" href="javascript:Scarica('<%=sFileNameJS%>')"><b>Abrir reporte</b></a>
			</td>
		</tr>		
	</table>
</label>
<br>
<br>
<%end sub%>

</body>
</html>
<!--#include Virtual = "/include/CloseConn.asp"-->
<%else%>
	<script>
		alert("La sesion ha caducado")
		self.close()
	</script>
<%end if%>

