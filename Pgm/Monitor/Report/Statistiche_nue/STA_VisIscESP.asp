<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%dim diprov
IF ucase(session("progetto")) = "/ARITES" THEN 
	diprov="Provincia"
else
	diprov="Departamento"
end if
IF session("progetto") <> "" THEN%>

<!--#include Virtual = "/include/openconn.asp"-->
<!--#include Virtual = "/include/DecCod.asp"-->
<!--#include virtual = "/include/SelBandi.asp"-->
<!--#include virtual="/include/ElabRegole.asp" -->
<!--#include virtual="/include/SelezioneProvince.asp" -->

<html>
	<head>
		<title>Inscriptos por edad, sexo y <%=diprov%></title>
		<link REL="STYLESHEET" TYPE="text/css" HREF="<%=session("progetto")%>/fogliostile.css">
		<script language="javascript" src="Util.js"></script>
	</head>
	<body topmargin="0" onafterprint="javascript:buttom.style.visibility='visible';" onbeforeprint="javascript:buttom.style.visibility='hidden'">
<%
dim Sql, rsFasciaA
dim aFascia(5)

aFascia(0) = " < 18"
aFascia(1) = "BETWEEN 18 AND 29"
aFascia(2) = "BETWEEN 30 AND 49"
aFascia(3) = "BETWEEN 50 AND 65"
aFascia(4) = " > 65"

inizio()

	
	
	if trim(Request.QueryString("sAmb"))="" then
		Ambito = FiltroCPI(true)
	else 
		Ambito = Request.QueryString("sAmb")
	end if	
	
	CodProv = Request.QueryString("sProv")
	
	
	ValProv = DecodTadesToArray("PROV",date,"CODICE='" & CodProv & "'",0,0)
	sDescProv = ValProv(0,1,0)
	Desc_Val=ValProv(0,1,1)
	sDescRegio = DecCodVal("REGIO",0,date,Desc_Val,0)

	titolo =	"Recuento de la distribuci�n por franja de edad, sexo y " &_
				diprov & " de residencia de los inscriptos "
	select case(Ambito)
		case("x")
			titolo = titolo & "a: " & Request.QueryString("CPI") 
		case(0)
			titolo = titolo & "a: " & Request.QueryString("CPI") 
		case(1)
			titolo = titolo & "del Territorio Nacional"
		case(2)
			titolo = titolo & "del " & diprov & " de " & sDescProv
		case(3)
			titolo = titolo & "de la Region de " & sDescRegio
	end select
	
	if trim(Request.QueryString("sBan"))<>"" then
		titolo = titolo & " - Inscriptos a : " & DecBando(Request.QueryString("sBan"))
	end if
%>

<center>
<table cellpadding="0" cellspacing="0" width="488" border="0">
	<tr height="18">
		<td class="sfondomenu" height="18" width="75%" nowrap>
		<span class="tbltext0"><b>&nbsp;ISCRIPTOS POR FRANJA DE EDAD, SEXO Y <%=ucase(diprov)%></b></span></td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
		<td valign="middle" align="right" width="40%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1"></td>
	</tr>
	<tr>
		<td width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif"></td>     
	</tr>
	<tr width="371" class="SFONDOCOMM">
		<td colspan="3">
			<%=titolo%>
		</td>
	</tr>
	<tr height="2">
		<td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif">
		</td>
	</tr>
</table>
<br>

<table>
	<tr width="500">
		<td width="500" class="textred" align="center"><b>&nbsp;situaci�n al : <%=date%></strong></td> 
	</tr>
</table>	
<br>

<table border="0" width="488" cellspacing="2" cellpadding="2">

<%
scpi = Request.QueryString("CPI")

sFileName = Server.MapPath("/") & session("progetto") & "/DocPers/Statistiche/" &_
	Session("IdUtente") & "_ESP.xls"

sFileNameJS = session("progetto") & "/DocPers/Statistiche/" &_
				Session("IdUtente") & "_ESP.xls"
	

set oFileSystemObject	= Server.CreateObject("Scripting.FileSystemObject")

set oFileObj = oFileSystemObject.CreateTextFile(sFileName)

' ********************************
' Scrivo la intestazione del file.
' ********************************
oFileObj.WriteLine ("INSCRIPTOS POR FRANJA DE EDAD, SEXO Y " &  ucase(diprov))
oFileObj.WriteLine ("")
oFileObj.WriteLine (titolo)
oFileObj.WriteLine ("")
oFileObj.WriteLine ("")
sReport = "NO"

FOR i = 0 to ubound(aFascia)-1

	Sql =	"SELECT distinct COUNT(P.id_persona) AS NUM_PERS, PRV_RES, " &_
			"decode(SESSO,'F','FEMMINA','M','MASCHIO') AS SESSO " &_ 
			"FROM PERSONA P " &_
			"WHERE SESSO IS NOT NULL "
			
	if trim(Request.QueryString("sBan"))<>"" then
		Sql = Sql & " AND p.ID_PERSONA IN (SELECT ID_PERSONA FROM DOMANDA_ISCR WHERE ID_BANDO='" & Request.QueryString("sBan") & "') "
	end if 
	
	Sql =Sql & "AND EXISTS (" &_
				"SELECT id_persona FROM STATO_OCCUPAZIONALE SO " &_
				"WHERE p.id_persona = SO.id_persona " &_
				"AND (SO.ind_status='0' or " &_
				"		(SO.ind_status='2' and " &_
						" SO.ind_status not in (select ind_status from STATO_OCCUPAZIONALE WHERE p.id_persona = id_persona and ind_status='0'))) "
	
	select case(cint(ambito))
		case(0)		'CPI
			Sql =	Sql &	"AND SO.id_cimpiego= " & session("creator") & ") "
		case(1)		'TERRITORIO NAZIONALE---query originale
			Sql =	Sql &	"AND SO.id_cimpiego is not null) "		
		case(-1)		'TERRITORIO NAZIONALE - TUTTI
			Sql =	Sql &	") "	
		case(2)		'departamento DEL CPI
			Sql =	Sql &	"AND SO.ID_CIMPIEGO IS NOT NULL " &_
							"AND SO.ID_CIMPIEGO IN (" &_
								"SELECT SI.ID_SEDE FROM SEDE_IMPRESA SI, IMPRESA I " &_
								"WHERE SI.ID_IMPRESA = I.ID_IMPRESA " &_
								"AND SI.PRV='" & CodProv & "' " &_
								"AND I.COD_TIMPR='03')) "
		case(3)		'REGIONE DEL CPI 
			Regio	= SetProvince(ambito)
			Sql =	Sql &	"AND SO.ID_CIMPIEGO IS NOT NULL " &_
							"AND SO.ID_CIMPIEGO IN (" &_
								"SELECT SI.ID_SEDE FROM SEDE_IMPRESA SI, IMPRESA I " &_
								"WHERE SI.ID_IMPRESA = I.ID_IMPRESA " &_
								"AND SI.PRV IN (" & Regio & ") " &_
								"AND I.COD_TIMPR='03')) "	
	end select
	
	Sql =	Sql &	"AND PRV_RES IS NOT NULL " &_
					"AND FLOOR(MONTHS_BETWEEN(SYSDATE,DT_NASC)/12) " & aFascia(i) &_
					" GROUP BY PRV_RES, SESSO " & _
					" ORDER BY  PRV_RES, SESSO "

'Response.Write SQL

	set rsFasciaA = server.CreateObject("ADODB.recordset")
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
'Response.Write sql & "<br><br>"
	rsFasciaA.Open Sql,cc,3
	n = 0

	sFascia = Replace(aFascia(i),"BETWEEN","FRANJA DE")
	sFascia = Replace(sFascia,"AND","A")
	sFascia = sFascia & " A�OS"

	oFileObj.WriteLine (sFascia & chr(9) & ucase(diprov) & chr(9) & "FEMMINE" & chr(9) & "MASCHI")

%>
	<tr class="tblsfondo"><td width="150" class="tbltext1"><b><%=sFascia%></b></td>
		<td width="150" class="tbltext1"><b><%=diprov%></b></td>
		<td width="100" class="tbltext1"><b>Femenino</b></td>
		<td width="100" class="tbltext1"><b>Masculino</b></td>
	<!--TD width='100' class=tbltext1><B>Sesso</B></TD--></tr>
<%

	if not rsFasciaA.eof then
		sReport = "OK"
		do while not rsFasciaA.eof
			if n = 0  then
				sProv = DecCodVal("PROV","0", date, rsFasciaA("PRV_RES"),1)
				if sProv <> "" then
					%>

					<tr><td width="150" class="tbltext1">&nbsp;</td>
					<td width="150" class="tbltext1">
					<%				
					'sProv = DecCodVal("PROV","0", date, rsFasciaA("PRV_RES"),1)
					Response.Write sProv
					%></td>
						<!--TD width='100' class=tbltext1><%'=rsFasciaA("SESSO")%></TD-->
					<%IF rsFasciaA("SESSO") = "FEMMINA" THEN%>
						<td width="100" class="tbltext1"><%
						nNuMpersF = rsFasciaA("NUM_PERS")
						Response.Write nNuMpersF
						%></td>
						<td width="100" class="tbltext1"><%
						sProvPrec = rsFasciaA("PRV_RES")
						rsFasciaA.MoveNext
						IF NOT rsFasciaA.EOF THEN
							if sProvPrec = rsFasciaA("PRV_RES") then
								Response.write rsFasciaA("NUM_PERS")
								nNuMpersM = rsFasciaA("NUM_PERS")
							else
								Response.write "0"
								nNuMpersM = 0 
								rsFasciaA.MovePrevious
							end if
						ELSE
								nNuMpersM = 0 
								Response.write "0"
								rsFasciaA.MovePrevious
						END IF	%>
					</td>
				<%
				else
					nNuMpersF = 0 
					nNuMpersM = rsFasciaA("NUM_PERS")
				%>
					<td width="100" class="tbltext1">0</td>
					<td width="100" class="tbltext1"><%=rsFasciaA("NUM_PERS")%>
				<%end if
			  end if %>			
				</tr>
			<%else
				sProv = DecCodVal("PROV","0", date, rsFasciaA("PRV_RES"),1)
				if sProv <> "" then 
			%>
					<tr><td width="150" class="tbltext1">&nbsp;</td>
					<td width="150" class="tbltext1">
					<%
					'sProv = DecCodVal("PROV","0", date, rsFasciaA("PRV_RES"),1)
					Response.Write sProv
					%></td>
						<!--TD width='100' class=tbltext1><%'=rsFasciaA("SESSO")%></TD-->
					<%IF rsFasciaA("SESSO") = "FEMMINA" THEN%>
						<td width="100" class="tbltext1"><%
						nNuMpersF = rsFasciaA("NUM_PERS")
						Response.Write nNuMpersF
						%></td>
						<td width="100" class="tbltext1"><%
						sProvPrec = rsFasciaA("PRV_RES")
						rsFasciaA.MoveNext
						IF NOT rsFasciaA.EOF THEN
							if sProvPrec = rsFasciaA("PRV_RES") then
								Response.write rsFasciaA("NUM_PERS")
								nNuMpersM = rsFasciaA("NUM_PERS")
							else
								Response.write "0"
								nNuMpersM = 0 
								rsFasciaA.MovePrevious
							end if
						ELSE
								nNuMpersM = 0 
								Response.write "0"
								rsFasciaA.MovePrevious
						END IF
						%></td>
					<%else
						nNuMpersF = 0 
						nNuMpersM = rsFasciaA("NUM_PERS")
					%>
						<td width="100" class="tbltext1">0</td>
						<td width="100" class="tbltext1"><%=rsFasciaA("NUM_PERS")%>
					<%end if%>			
				</tr>

			<%
				end if 
			end if
			oFileObj.WriteLine (chr(9) & sProv & chr(9) & nNuMpersF & chr(9) & nNuMpersM)

			rsFasciaA.MoveNext
			n = n + 1
		loop
		
	
	else
		oFileObj.WriteLine (chr(9) & "-" & chr(9) & "0" & chr(9) & "0")
%>
	<tr><td width="150">&nbsp;</td>
		<td width="150" class="tbltext1"><b>-</b></td>
		<td width="100" class="tbltext1"><b>0</b></td>
		<td width="100" class="tbltext1"><b>0</b></td>
	<!--TD width='100' class=tbltext1><B>Sesso</B></TD--></tr>


<%
	end if
	
	rsFasciaA.Close
	set rsFasciaA = nothing

NEXT

oFileObj.close
set oFileSystemObject = nothing
set oFileObj = nothing
%>

</table>
</center>

<%
'Commentato controllo IF e sub Fine1(). 
'Sostituiti con chiamata diretta a Fine().

Fine()

'if sReport = "OK" then
'	Fine()
'else
'	Fine1()
'end if

'sub Fine1()
%>
	<!--	<center>	<br><br>	<table width="460" cellspacing="2" cellpadding="1" border="0" align="center">		<tr align="center">			<td class="tbltext3">				Nessun elemento ritrovato 			</td>		</tr>				<tr align="center">			<td>&nbsp;</td>		</tr>				<tr align="center">			<td>				<a href="javascript:self.close()"><img src="<%'=Session("progetto")%>/images/chiudi.gif" title="Chiudi la pagina" border="0" align="absBottom"></a>			</td>		</tr>			</table>	</center>	-->	
<%'end sub

' *************************************************************************************
sub inizio()
%>
  <center>
  <table border="0" width="500" cellspacing="0" cellpadding="0" height="81">
    <tr>
      <td width="500" background="<%=Session("Progetto")%>/images/titoli/strumenti2b.gif" height="81" valign="bottom" align="right">
        <table border="0" background width="260" height="30" cellspacing="0" cellpadding="0">
          <tr>
            <td width="100%" valign="top" align="right"><b class="tbltext1a">ESTADISTICAS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></b></td>
          </tr>
          <tr>
            <td width="100%" valign="top" align="right"><BR><BR></td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  </center>
<%
end sub
' ************************************************************************************

' ************************************************************************************
sub Fine()
%>
	<br><br>
	<label id="buttom">
	<table width="460" cellspacing="2" cellpadding="1" border="0" align="center">
		<tr align="center">
			<td>
				<a href="javascript:self.close()"><img src="<%=Session("progetto")%>/images/chiudi.gif" title="Chiudi la pagina" border="0" align="absBottom"></a>
				<a href="javascript:print()"><input type="image" src="<%=Session("progetto")%>/images/stampa.gif" title="Stampa la pagina" border="0" align="center" onclick="self.print();" id="stampa" name="stampa"></a>
			</td>
		</tr>
		<tr><td class="textred">&nbsp; </td></tr>
		<tr align="center">
			<td>
				<a class="textred" href="javascript:Scarica('<%=sFileNameJS%>')"><b>Abrir Reporte</b></a>
			</td>
		</tr>
		<tr align="center">
			<td>&nbsp;</td>
		</tr>	
	</table>
	</label>
<%end sub
' ************************************************************************************
%>

</body>
</html>
<!--#include Virtual = "/include/CloseConn.asp"-->

<%ELSE%>
	<script>
		alert("La sesion ha caducado")
		self.close()
	</script>
<%END IF%>
