<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include Virtual = "/include/openconn.asp"-->
<!--#include virtual = "/include/DecCod.asp"-->
<!--#include virtual = "/include/ControlDateVB.asp"-->
<!--#include virtual = "/util/portallib.asp"-->
<%
'if ValidateService(session("idutente"),"RIC_RICRICHIESTA",cc) <> "true" then 
'	response.redirect "/util/error_login.asp"
'end if
%>
<html>
<head>
<title>Situación ocupacional</title>
<link REL="STYLESHEET" TYPE="text/css" HREF="<%=session("progetto")%>/fogliostile.css">
<script LANGUAGE="Javascript">
<!--#include virtual = "/Include/help.inc"-->
</script>

<%
'********************************
'*********** MAIN ***************

'	if ValidateService(session("idutente"),"RIC_RICRICHIESTA",cc) <> "true" then 
'		response.redirect "/util/error_login.asp"
'	end if

Dim nIdPers
	
nIdPers = Request("id")
Inizio()
Imposta_pag()

'********************************
'********************************
'**********************************************************************************************************************************************************************************
Sub Imposta_Pag()
	Dim sql, RR, aStdis
	
	sql = "select a.DT_DEC_DIS, b.DESCrizione, a.COD_STDIS, a.COD_ST181" &_
		  " from STATO_OCCUPAZIONALE a, sede_impresa b " &_
		  " where a.ID_PERSONA = " & nIdPers &_
		  " And a.ID_CIMPIEGO = b.id_sede(+) " &_
		  " And a.IND_STATUS = 0"
	
	'Response.Write sql
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
	set RR = CC.Execute(sql)
	
	if not RR.Eof then
%>
		<table WIDTH="500" BORDER="0" CELLPADDING="2" CELLSPACING="2" align="center">
			<tr height="25">
			    <td class="tbltext1" width="200">
					<b>Fecha de transcurso de desocupación </b>
				</td>
			    <td class="textblack"> 
					<b>
<%					if not isnull(RR.Fields("DT_DEC_DIS")) then 
						Response.Write ConvDateToString(RR.Fields("DT_DEC_DIS"))
					else
						Response.Write " - "
					end if
%>					</b>
				</td>
			</tr>
			<tr height="25">
				<td class="tbltext1" width="200">
					<b>
<%				if 	not isnull(RR.Fields("COD_STDIS")) then		
						select case left(RR.Fields("COD_STDIS"),1)
							case "I"
								Response.Write "Ocupado por tiempo indeterminado"
							case "D"
								Response.Write "Ocupado por tiempo determinado"
							case "N"
								Response.Write "Desocupado"
							case else
								Response.Write "-"
						end Select
				end if
%>				    </b>
				</td>
			    <td class="textblack"> 
					<b>
<%					if not isnull(RR.Fields("COD_STDIS")) then

						aStdis = decodTadesToArray("STDIS",date,"CODICE='" &_
							 RR.fields("COD_STDIS") & "'",1,0)
						Response.Write 	lcase(aStdis(0,1,0))										
'						Response.Write lcase(DecCodVal("STDIS",0,"",RR.fields("COD_STDIS"),""))
					else
						Response.Write " - "
					end if
%>					</b>
				</td>
			</tr>
			<tr height="25">
			    <td class="tbltext1" width="200">
					<b>Oficina de Empleo de la inscripción </b>
				</td>
			    <td class="textblack"> 
					<b>
<%					if not isnull(RR.Fields("DESCRIZIONE")) then
						Response.Write RR.Fields("DESCRIZIONE")
					else
						Response.Write " - "
					end if
%>					</b>
				</td>
			</tr>
		</table>
		<br>	
<%	end if		
	RR.close
	set RR = nothing
	
	if aStdis(0,1,1) = "S" then
		%>
		<table WIDTH="500" BORDER="0" CELLPADDING="2" CELLSPACING="2" align="center">
			<tr height="25"><td>
		<%
'PL-SQL * T-SQL  
		Server.Execute("/PLAVORO/TESTI/SISTDOC/SISTEMA " & _
			"REDAZIONALE/INFOPRIMOLIVELLO/info_cercolavoro.htm")
'		Server.Execute("/PLAVORO/TESTI/SISTDOC/SISTEMA " & _
'			"REDAZIONALE/INFOPRIMOLIVELLO/INFO_" & aStdis(0,1,0) & ".HTM")
		%>
		</td></tr></table>
		<%
	end if
	Erase aStdis ' Distruggo l'array

	Chiudi()
			
End Sub 
'**********************************************************************************************************************************************************************************
Sub Inizio()%>
	<table width="520px" border="0" cellspacing="0" cellpadding="0" height="70" align="center">
		<tr>
			<td width="500" background="<%=session("progetto")%>/images/titoli/servizi2g.gif" valign="bottom" align="right">
				<table border="0" background width="500" height="23" cellspacing="0" cellpadding="0">
					<tr>
						<td width="100%" valign="top" align="right"><b CLASS="tbltext1a">Status
						  </b></td>
					</tr>
				</table>
			</td>
		</tr>
	</table> 
	<br>
	<table border="0" CELLPADDING="0" CELLSPACING="0" width="500" align="center">
		<tr height="18">
			<td class="sfondomenu" width="67%" height="18"><span class="tbltext0"><b>&nbsp;STATO OCCUPAZIONALE</b></span></td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
			<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
		</tr>
	</table>
	<!-- Commento -->
	<table border="0" CELLPADDING="0" CELLSPACING="0" width="500" align="center">
		<tr>
			<td align="left" class="sfondocommaz">
				Visualizacion del estado ocupacional del postulante <%=nIdPers%>.<br> 
				<br>
			</td>
		</tr>
		<tr height="2">
			<td class="sfondocommaz" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>
	<br>
<%End Sub
'**********************************************************************************************************************************************************************************
Sub Chiudi()%>
	<br>
	<table border="0" CELLPADDING="0" CELLSPACING="0" width="500" align="center">
		<tr>
			<td align="center"> 
				<a href="javascript:self.close()"><img title="Chiudi" src="<%=Session("Progetto")%>/images/chiudi.gif" border="0"></a>
			</td>
		</tr>
	</table>
<%End Sub
'**********************************************************************************************************************************************************************************	
%>
<!--#include Virtual ="/Include/closeconn.asp"-->
