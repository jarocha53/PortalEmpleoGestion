<%@ Language=VBScript %>
<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #include virtual="/strutt_testa2.asp" -->
<!-- #include virtual = "/util/portallib.asp" -->
<!-- #include virtual = "/include/OpenConn.asp" -->
<!-- #include virtual = "/include/ckProfile.asp" -->
<%
	If ((Not ValidateService(Session("IdUtente"),"Gestione Notizie",cc)) And (Not ValidateService(Session("IdUtente"),"Sistema Documentale",cc))) Then 
		response.redirect "/util/error_login.asp"
	End If

IdCat = Request.QueryString("Id")

if Request.QueryString("Dir") = "" then

	NomeDir = Request.Form("DirCreata")

	Set rstCategorie = Server.CreateObject("ADODB.Recordset")
	SQLCategoria = "SELECT Id_Categoria, Descrizione, Percorso, Padre FROM Categoria WHERE Id_Categoria=" & IdCat
'PL-SQL * T-SQL  
SQLCATEGORIA = TransformPLSQLToTSQL (SQLCATEGORIA) 
	rstCategorie.open SQLCategoria, CC, 1, 3
	If Not rstCategorie.EOF Then
		IdPadre = rstCategorie("Padre")
		If Request.Form("DirAggior") = "" Then
			DirScelta = rstCategorie("Percorso") & "\" & NomeDir
		Else
			DirScelta = Request.Form("DirAggior")
		End If
	Else
		IdPadre = 0
		DirScelta = NomeDir
	End If
	
	PathDirInit = Server.MapPath("\")
	PathDir = PathDirInit & Session("Progetto") & "\images\Categorie\"

%>
<script language="javascript" src="/Include/help.inc"></script>
<script language="Javascript">
	function CheckFile()
	{
		if (FotoForm.F1.value == "")  
		{	
			alert ("Atenci�n! Debe seleccionar una foto.");
			return false
		}
		return true
	}	
</script>
<br>
<%
'-------------selezione pagina help----------------------------
sTipoDoc=Session("Tipo")

Select Case sTipoDoc

	Case "N" sNomCart="doc_InviaImgDoc"
	
	Case "D" sNomCart="doc_InviaImgDoc_1"
	
End Select
'-------------------------------------------------------------
%>
<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
	<tr height="18">
		<td class="sfondomenu" width="67%" height="18"><span class="tbltext0"><b>&nbsp;AREA REDACCIONAL - <%=Session("Intestazione")%></b></span></td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
		<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
	</tr>
	<tr>
		<td class="sfondocomm" width="57%" colspan="3">
			<br>Para asociar una foto a la noticia utilizar el pulsante <b>Busca</b> 
			y seleccionar el archivo de la imagen en el propio computador <br>
			Presionar <b>Env�a</b> para confirmar el ingreso. <br>
			<a href="Javascript:Show_Help('/pgm/help/Documentale/<%=sNomCart%>')" name onmouseover="javascript:status='' ; return true">
			<img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0" width="18" height="18" alt="Para mayores informaciones"></a>
		</td>
	</tr>
	<tr height="17">
		<td class="sfondocomm" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
	</tr>
</table>

<form Enctype="multipart/form-data" Method="POST" Name="FotoForm" Action="DOC_InviaImgDoc.asp?Dir=<%=PathDir%>&amp;Path=<%=DirScelta%>&amp;IdPadre=<%=IdPadre%>&amp;Id=<%=IdCat%>" onSubmit="return CheckFile()">
<table BORDER="0" CELLPADDING="0" CELLSPACING="0" width="500">
	<tr class="sfondocomm">
		<td VALIGN="TOP" COLSPAN="2">
			<br><b>N.B.: La foto enviada debe satisfacer las siguientes caracter�sticas:</b>
		    <ul>
				<li>Formatos permitidos: jpg o gif.
				<li>Dimensiones: 50x50 pixel.
				<li>Tama�o m�ximo: 100 Kb.
			</ul>
		</td>
	</tr>
	<tr>
		<td WIDTH="500" colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td WIDTH="100" class="tbltext1"><b>Foto Peque�a:</b></td>
		<td WIDTH="400"><input SIZE="40" TYPE="FILE" NAME="F1"></td>
	</tr>
	<tr>
		<td VALIGN="TOP" ALIGN="CENTER" COLSPAN="2">
			<table BORDER="0" CELLPADDING="2" CELLSPACING="3" width="500">
				<td ALIGN="right" WIDTH="250">
					<input type="image" src="<%=Session("Progetto")%>/images/conferma.gif" name="avanti">
				</td>
				<td ALIGN="left"><a href="javascript:history.back()">
					<img alt="Indietro" border="0" src="<%=Session("Progetto")%>/Images/indietro.gif"></a>
				</td>
			</table>
		</td>
		</td>
	</tr>
</table>
</form>
<%
	rstCategorie.Close
	Set rstCategorie = nothing
%>
<!-- #include virtual = "/include/closeconn.asp" -->
<!-- #include virtual="/strutt_coda2.asp" -->
<%
Else

	Set upl = Server.CreateObject("Dundas.Upload.2")
		upl.UseUniqueNames = False
		upl.SaveToMemory ()

	For Each objFile in upl.Files
		SizeFileUp = objFile.size
		If SizeFileUp > 110000 Then	
			objFile.delete
			Response.Write "<TABLE width='510' border='0' cellpadding='2' cellspacing='2'>"
			Response.Write "<tr><td align='center'><img src='/images/sistdoc/Upload/FotoNOK.gif' width='400' height='260'></td></tr>"
			Response.Write "<tr><td colspan='2' class='tbltext3' align=center><b>La foto enviada tiene una dimensi�n mayor de la permitida.</b></td></tr>"
			Response.Write "<tr><td align=center><a href=javascript:history.back()>"
			Response.Write "<img alt=Atr�s border=0 src=" & Session("Progetto") & "/images/indietro.gif></a></td></tr>"
			Response.Write "</TABLE>"
			Response.End
		Else
			NomeFoto = upl.GetFileName(objFile.OriginalPath)
			sFile = Request.QueryString("Dir") & NomeFoto
				If upl.FileExists(sFile) Then
					Response.Write "<TABLE width='510' border='0' cellpadding='2' cellspacing='2'>"
					Response.Write "<tr><td align='center'><img src='/images/sistdoc/Upload/DocumentoNOK.gif' width='400' height='260'></td></tr>"
					Response.Write "<tr><td colspan='2' class='tbltext1' align='center'><B>El archivo <i class='textreda'>" & NomeFoto 
					Response.Write "</i> ya existe!</B> <br>E' possibile che l'immagine sia gi� stata inserita o che esista un'immagine differente con lo stesso nome, per evitare accidentali sostituzioni � necessario cambiare nome al file da associare a questa categoria. </td></tr>"
					Response.Write "<tr><td align='center'><a href=javascript:history.back()>"
					Response.Write "<img alt='Atr�s' border='0' src='" & Session("Progetto") & "/images/indietro.gif'></a></td></tr>"
					Response.Write "</TABLE>"
					Response.End
				Else
					objFile.SaveAs sFile
				End If
		End if
	Next		
	
		Percorso = Request.QueryString("Path")
		Set upl = Nothing
		
	If NomeFoto = "" Then
		Response.Redirect "DOC_InviaImgDoc.asp?Id=" & IdCat
	Else 
		Response.Redirect "DOC_RisInviaImgCartella.asp?Id=" & IdCat & "&Foto=" & NomeFoto & "&Path=" & Percorso & "&IdPadre=" & Request.QueryString("IdPadre")
	End If
End If
%>
