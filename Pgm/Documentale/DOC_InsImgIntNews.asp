<%@ Language=VBScript %>
<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #include virtual="/strutt_testa2.asp" -->
<!-- #include virtual="/util/portallib.asp" -->
<!-- #include virtual="/include/OpenConn.asp" -->
<!-- #include virtual="/include/ckProfile.asp" -->

<script language="javascript" src="/Include/help.inc"></script>

<%
If ((Not ValidateService(Session("IdUtente"),"Gestione Notizie",cc)) And (Not ValidateService(Session("IdUtente"),"Sistema Documentale",cc))) Then 
	response.redirect "/util/error_login.asp"
End if
%>
<!-- #include virtual="/include/CloseConn.asp" -->
<%
sFImageName = Request.QueryString("FileName")
If Request.QueryString("Up") = "" Then
	IdCat = Request.Form("Id")
%>
<%
'-------------selezione pagina help----------------------------
sTipoDoc=Session("Tipo")
'Response.Write sTipoDoc
Select Case sTipoDoc

	Case "N" sNomCart="doc_insimgintnews"
	
	Case "D" sNomCart="doc_insimgintnews_1"
	
	Case "R" sNomCart="doc_insimgintnews_2"
	
End Select
'-------------------------------------------------------------
%>
<br>
<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
	<tr height="18">
		<td class="sfondomenu" width="67%" height="18"><span class="tbltext0"><b>&nbsp;AREA REDAZIONALE - <%=Session("Intestazione")%></b></span></td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
		<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
	</tr>
	<tr>
		<td class="sfondocomm" width="57%" colspan="3"><br>
		Per associare una foto alla notizia utilizzare il pulsante <b>Sfoglia</b> e selezionare il file dell'immagine nel proprio computer. Premere <b>Invia</b> per confermare l'inserimento. <br>
		Se non si desidera inserire una foto premere direttamente <b>Invia</b> per passare alla composizione del testo.
		<a href="Javascript:Show_Help('/pgm/help/Documentale/<%=sNomCart%>')" name onmouseover="javascript:status='' ; return true">
		<img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0" width="18" height="18" alt="per maggiori informazioni"></a></td>
	</tr>
	<tr height="17">
		<td class="sfondocomm" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
	</tr>
</table>

	<script language="Javascript">
		function CheckFile()
		{
			if (document.FotoForm.F1.value == ""){
				alert("Selezionare il file.");
				document.FotoForm.F1.focus();
				return false
			}
			else {
				document.FotoForm.action = "Doc_InsImgIntNews.asp?Up='Si'&Id=<%=IdCat%>&FileName="+document.FotoForm.F1.value
			}
			document.FotoForm.submit();
			return false
		}	
	</script>

<form ENCTYPE="multipart/form-data" METHOD="POST" onsubmit="return CheckFile()" ACTION name="FotoForm">
<table BORDER="0" CELLPADDING="2" CELLSPACING="3" WIDTH="500">
	<tr>
		<td class="sfondocomm" VALIGN="TOP" COLSPAN="2">
		<% If Session("Tipo") = "R" Then 
			sDim = "200 pixel di larghezza."
		 Else 
			sDim = "300 pixel di larghezza."
		 End If %>
			<br><b>N.B.: La foto inviata deve soddisfare le seguenti caratteristiche:</b>
		    <ul>
				<li>Formati permessi: jpg o gif.
				<li>Dimensioni massima: <%=sDim%>
				<li>Grandezza massima: 100 Kb.
			</ul>
		</td>
	</tr>
	<tr>
		<td VALIGN="TOP" COLSPAN="2">&nbsp;</td>
	</tr>
	<tr>
		<td WIDTH="150" class="tbltext1"><b>Foto Interna</b></td>
		<td WIDTH="350"><input class="textblack" size="30" type="FILE" name="F1"></td>
	</tr>
	<tr>
		<td VALIGN="TOP" ALIGN="CENTER" COLSPAN="2">
			<input type="image" src="<%=Session("Progetto")%>/images/conferma.gif" name="avanti">
		</td>
	</tr>
</table>
</form>
<br>
<!-- #include virtual="/strutt_coda2.asp" -->
<%
Else

nIdTema = Request.QueryString("Id")
If Session("Tipo")="R" Then 
		sPathDir = Server.MapPath("\") & Session("Progetto") & "\images\Redazionale\"
End If
If Session("Tipo")="N" Then 
		sPathDir = Server.MapPath("\") & Session("Progetto") & "\images\News\"
End If
If Session("Tipo")="D" Then 
		sPathDir = Server.MapPath("\") & Session("Progetto") & "\images\Doc\"
End If
	sPathDir = Replace(sPathDir,"\","/")
	sExtFoto = Lcase(right(sFImageName,4))
        If sExtFoto <> ".gif" and sExtFoto <> ".jpg" Then
			Response.Write "<TABLE width='510' border='0' cellpadding='2' cellspacing='2'>"
			Response.Write "<tr><td align='center'><img src='/Images/SistDoc/Upload/DocumentoNOK.gif' width='400' height='260'></td></tr>"
			Response.Write "<tr><td colspan='2' class='tbltext1' align='center'><B>La foto &nbsp;<i class='textredb'>" & sFImageName
			Response.Write "&nbsp;</i>ha un formato non permesso.</Font></B></td></tr>"
			Response.Write "<tr><td align='center'><a href=javascript:history.back()>"
			Response.Write "<img alt='Indietro' border='0' src='" & Session("Progetto") & "/Images/indietro.gif'></a></td></tr>"
			Response.Write "</TABLE>"
			Response.End
		End If
		Set upl = Server.CreateObject("Dundas.Upload.2") 
		upl.UseUniqueNames = False				
		upl.SaveToMemory ()
		
		For Each objFile in upl.Files 
			SizeFileUp = objFile.size
			If SizeFileUp > 110000 Then	' Se dimensione superiore 100 Kb elimino il file inviato
				objFile.delete
				Response.Write "<TABLE width='510' border='0' cellpadding='2' cellspacing='2'>"
				Response.Write "<tr><td align='center'><img src='/Images/SistDoc/Upload/FotoNOK.gif' width='400' height='260'></td></tr>"
				Response.Write "<tr><td colspan='2' class='tbltext3' align=center><b>La foto inviata ha una dimensione maggiore di quella permessa.</b></td></tr>"
				Response.Write "<tr><td align=center><a href=javascript:history.back()>"
				Response.Write "<img alt='Indietro' border='0' src='" & Session("Progetto") & "/Images/indietro.gif'></a></td></tr>"
				Response.Write "</TABLE>"
				Response.End
			Else
				Pathdoc = objFile.OriginalPath
				sNomeDoc = upl.GetFileName(objFile.OriginalPath)
				sFile = sPathDir & sNomeDoc
				If upl.FileExists(sFile) Then
					Response.Write "<TABLE width='510' border='0' cellpadding='2' cellspacing='2'>"
					Response.Write "<tr><td align='center'><img src='/Images/SistDoc/Upload/DocumentoNOK.gif' width='400' height='260'></td></tr>"
					Response.Write "<tr><td colspan='2' class='tbltext1' align='center'><B>Esiste gia' una foto <i class='textreda'>" & sNomeDoc
					Response.Write "</i></Font></B></td></tr>"
					Response.Write "<tr><td align='center'><a href=javascript:history.back()>"
					Response.Write "<img alt='Indietro' border='0' src='" & Session("Progetto") & "/Images/indietro.gif'></a></td></tr>"
					Response.Write "</TABLE>"
					Response.End
				Else
					on error resume next
					objFile.SaveAs sFile
					If err.number <> 0 Then
						Response.Write "<TABLE width='510' border='0' cellpadding='2' cellspacing='2'>"
						Response.Write "<tr><td align=center>"
						Response.Write "<b class='tbltext'><br>Immagine non inviata correttamente"
						Response.Write "<p>Contattare il Gruppo Assistenza Portale Italia Lavoro<br>"
						Response.Write "all'indirizzo <a href='mailto:po-assistenza@italialavoro.it'>po-assistenza@italialavoro.it</a></p></b>"
						Response.Write "</td></tr></TABLE>"
						Response.End
					End If	
				End If
			End if
		Next
		 
		Set upl = Nothing
		
			Response.Write"<FORM id='Ritorno' method='POST' action='/pgm/documentale/Doc_Vis" & Session("Intestazione") & ".asp'>"
			Response.Write"<input type='hidden' name='Tema' value='" & nIdTema & "'>"
			Response.Write"</FORM>"	

			Response.Write "<TABLE width='510' border='0' cellpadding='2' cellspacing='2'>"
			Response.Write "<tr><td align='center'><img src='/Images/SistDoc/Upload/DocumentoOK.gif' width='400' height='260'></td></tr>"
			Response.Write "<tr><td colspan='2' class='tbltext1' align='center'><B>La foto &nbsp;<i class='textredb'>" & sNomeDoc 
			Response.Write "&nbsp;</i>Immagine inserita correttamente</B></td></tr>"

			Response.Write "<tr><td align='center'><a href=javascript:Ritorno.submit();>"
			Response.Write "<img alt='Indietro' border='0' src='" & Session("Progetto") & "/Images/indietro.gif'></a></td></tr>"
			Response.Write "</TABLE>"
			

End If
%>
