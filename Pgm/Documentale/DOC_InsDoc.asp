<%@ Language=VBScript %>
<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #include virtual="/strutt_testa2.asp"-->
<!-- #include virtual="/util/portallib.asp" -->
<!-- #include virtual="/include/OpenConn.asp" -->
<!-- #include virtual="/include/ckProfile.asp" -->
<!-- #include virtual="/include/ControlDateVB.asp" -->
<%
	if ((Not ValidateService(Session("IdUtente"),"Gestione Notizie",cc)) And (Not ValidateService(Session("IdUtente"),"Sistema Documentale",cc))) then 
		response.redirect "/util/error_login.asp"
	End if
%>
<script language="javascript" src="/Include/ControlString.inc"></script>
<script language="javascript" src="/Include/help.inc"></script>
<script LANGUAGE="javascript">
//include del file per fare i controlli sulla validit� delle date

function InitInvio() {
	InvioForm.Titolo.focus();	
}

function ControlloTitolo() {
	var i, codice_car;
	var codice = InvioForm.Titolo.value;
	var CtrlString = false;
	for (i=0; i< codice.length; i++) {
		 codice_car = codice.charAt(i)
		 if (codice_car != " ") {
			CtrlString = true;
			break;
		}
	}
}

function ControllaCheck() {

if (!TRIM(InvioForm.Titolo.value)){
	alert ("Non e' consentito l'invio della notizia senza aver scelto un titolo.");
	InvioForm.Titolo.focus();
	return false 
	}
if (InvioForm.Titolo.value.length > 100){
	alert ("Non e' consentito l'invio della news con un titolo\n pi� grande di 100 caratteri.\nAl momento e' lungo " + InvioForm.Titolo.value.length + " caratteri.")
	InvioForm.Titolo.focus();
	return false 
	}
if (InvioForm.Abstract.value.length > 400) {
	alert("L'abstract non puo' essere piu' lungo di 400 caratteri.\nAl momento �' lungo " + InvioForm.Abstract.value.length + " caratteri.");
	InvioForm.Abstract.focus();
	return false 
	}
	
if (!TRIM(InvioForm.NomeNews.value)){
	alert ("Non e' consentito l'invio senza aver dato un nome alla news.");
	InvioForm.NomeNews.focus();
	return false 
}

if (! isNaN(InvioForm.NomeNews.value)){
	alert ("Il nome della news non puo' essere solo di numeri.");
	InvioForm.NomeNews.focus();
	return false 
	}
 
Diff1 = 255 - InvioForm.LungURL.value
if ( Diff1 < InvioForm.NomeNews.value.length ){
	alert ("Nome News non valido!\nIl percorso completo della news \nsupera il limite di caratteri accettati \ndal sistema operativo.");
	InvioForm.NomeNews.focus();
	return false 
	}
	
Trovato = false 
var inputform = InvioForm.NomeNews.value
if (!ValidateInputStringIsValid(inputform)) {
	return false
}

}
function ControllaFoto() {

if (!(InvioFoto.ModFoto[0].checked) && !(InvioFoto.ModFoto[1].checked) && !(InvioFoto.ModFoto[2].checked)) {
	alert("Non e'stato selezionato quale tipo di foto si vuol modificare.")
	return false
	}
}
</script>
<%
' Controlllo se il file sia in modifica o in creazione
If Request.QueryString("Cod") = "MA" Then

	IdNews = Request.QueryString("Nr")
	
	Set rstNotizie = Server.CreateObject("ADODB.Recordset")
	
	SQLnotizie = "SELECT Id_Notizie, Titolo, Abstract, Data_Pubb, Data_Ins, Id_Categoria," &_
				 " Nome_Notizie, Autore, Fonte, Gif_HP, Gif_Notizie, Fl_Pubblicato, Id_tipodoc," &_
				 " alt_notizie, alt_hp FROM Notizie WHERE Id_Notizie=" & IdNews 
	
'PL-SQL * T-SQL  
SQLNOTIZIE = TransformPLSQLToTSQL (SQLNOTIZIE) 
	rstNotizie.open SQLnotizie, CC, 1, 3
	
	IdCat = rstNotizie("Id_Categoria")
	IdNews = rstNotizie("Id_Notizie")
	sNomeNews = rstNotizie("Nome_Notizie")
	sIdTipoNews = rstNotizie("Id_tipoDoc")
	
	Set rsttipoNotizie = Server.CreateObject("ADODB.Recordset")
	
	SQLTipoNotizie = "SELECT tipo FROM tipodoc where id_tipodoc =" & sIdTipoNews   
	
'PL-SQL * T-SQL  
SQLTIPONOTIZIE = TransformPLSQLToTSQL (SQLTIPONOTIZIE) 
	rsttipoNotizie.open SQLTipoNotizie, CC, 1, 3
	sTipoNews = rsttipoNotizie("tipo")
	
	If session("Tipo") = "R" Then
		sNomePag = mid(sNomeNews,2) & "." & sTipoNews
	Else
		sNomePag = sNomeNews & "." & sTipoNews
	End If
	
Else

	IdCat = Request.QueryString("Id")
	
	If session("Tipo") = "R" Then
		sNomePag = mid(Request.QueryString("Build"),2)
	Else
		sNomePag = "nomenews"
	End If

End if

'sNomePag = mid(Request.QueryString("Build"),2)

Set rstCategorie = Server.CreateObject("ADODB.Recordset")
SQLCategoria = "SELECT Id_Categoria, Descrizione, Percorso FROM Categoria WHERE Id_Categoria=" & IdCat
'PL-SQL * T-SQL  
SQLCATEGORIA = TransformPLSQLToTSQL (SQLCATEGORIA) 
rstCategorie.open SQLCategoria, CC, 1, 3
sPercorso = rstCategorie("Percorso")
'Response.Write sPercorso 
' Ricavo il percorso della categoria in cui si trova la notizia
PathDir = Session("DirInit") & sPercorso
PathDirImg = Session("DirInitEdit") & sPercorso
' Ricavo la directory di partenza per sapere se comune o relativa ad un progetto
aSepPath = split(rstCategorie("Percorso"),"\")
sServerName = Request.ServerVariables("Server_Name")
If Ucase(aSepPath(0)) = "TESTI" Then
 sPathLink = sServerName & replace(sPercorso,"\","/") & "/" & sNomePag
Else
 sPathLink = sServerName & Session("Progetto") & "/Testi/Info/infoprimolivello/" & sNomePag
End If
%>
<body onload="InitInvio()">
<br>
<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
	<tr height="18">
		<td class="sfondomenu" width="67%" height="18"><span class="tbltext0"><b>&nbsp;AREA REDAZIONALE - <%=Session("Intestazione")%> </b></span></td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
		<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">(*) campi obbligatori</td>
	</tr>
<% '-------------selezione pagina help----------------------------
sTipoDoc=Session("Tipo")
Select Case sTipoDoc

	Case "N" sNomCart="doc_insdoc"
	
	Case "D" sNomCart="doc_insdoc_1"
	
	Case "R" sNomCart="doc_insdoc_2"
		
End Select
'-------------------------------------------------------------
If Request.QueryString("Cod") <> "MA" Then 	
	
	If session("Tipo") <> "R" Then %>

	<tr>
		<td class="sfondocomm" width="57%" colspan="3"><br>Compilare il form con i dati di identificazione della notizia <b><%=rstCategorie("Descrizione")%></b> per inserirla.
	
<%  Else%>
	<tr>
		<td class="sfondocomm" width="57%" colspan="3"><br>Compilare il form con i dati di identificazione della pagina creata <b><%=sNomePag%></b>.<br>
		
<% End If %>
	    La pagina sara' visibile nel sito all'indirizzo:<br><b>http://<%=sPathLink%></b>
		<a href="Javascript:Show_Help('/pgm/help/Documentale/<%=sNomCart%>')" name onmouseover="javascript:status='' ; return true">
		<img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0" width="18" height="18" alt="per maggiori informazioni"></a></td>
</tr>
<%	
Else%>

	<tr>
		<td class="sfondocomm" width="57%" colspan="3"><br>Modificare gli attributi della notizia <b><%=sNomeNews%></b> e premere <b>Invia</b> per confermare le modifiche.
		La pagina sara' visibile nel sito all'indirizzo:<br><b>http://<%=sPathLink%></b>
		<a href="Javascript:Show_Help('/pgm/help/Documentale/<%=sNomCart%>')" name onmouseover="javascript:status='' ; return true">
		<img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0" width="18" height="18" alt="per maggiori informazioni"></a></td>
	</tr>	
<%
End If%>
		
	<tr height="17">
		<td class="sfondocomm" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
	</tr>
</table>
<%
 ' Controllo che il documento sia in modifica o creazione
If Request.QueryString("Cod") <> "MA" Then %>
<form Name="InvioForm" Method="post" Action="DOC_InsNews.asp" onSubmit="return ControllaCheck()">
<table width="510" BORDER="0" CELLPADDING="2" CELLSPACING="2">
	<tr>
		<td width="150" class="tbltext1"><b>Titolo*</b></td>	
		<td width="360"><input class="textblacka" Type="text" Name="Titolo" Size="50" onblur="ControlloTitolo()"></td>	
	</tr>
	<tr>
		<td width="150" class="tbltext1"><b>Abstract <br>(lunghezza max 400)</b></td>	
		<td width="360"><textarea class="textblacka" Name="Abstract" rows="4" cols="40">Inserire qui il testo</textarea></td>	
	</tr>
	<% ' EPILI 08/08/2002
	   If Session("Tipo") <> "R" then %>
	<tr>
		<td width="150" class="tbltext1"><b>Nome News*</b></td>	
		<td width="360"><input class="textblacka" Type="text" Name="NomeNews" Size="50" Maxlength="50"></td>	
	</tr>
	<% Else %>
		<input class="textblacka" Type="hidden" Name="NomeNews" Size="50" Maxlength="50" value="Redazionale">
	<% End if 
	   If Session("Tipo") <> "R" then %>
	<tr>
		<td width="150" class="tbltext1"><b>Data di Inserimento</b></td>	
		<td width="360"><input class="textgray" Type="text" Name="DataIns" Size="11" Value="<%=ConvDateToString(Date)%>" Maxlength="10" ReadOnly></td>	
	</tr>
	<tr>
		<td width="150" class="tbltext1"><b>Fonte</b></td>	
		<td width="360"><input class="textblacka" Type="text" Name="Fonte" Size="40" Maxlength="50"></td>	
	</tr>
	<% End If %>
	<tr>
		<td width="150" class="tbltext1"><b>Autore</b></td>	
		<td width="360"><input class="textblacka" Type="text" Name="Autore" Size="40" Maxlength="50" Value="<%=Session("NomeUtente")%>" readonly></td>	
	</tr>
	
	<% If Request.QueryString("Foto") <> "" Then
			aSepFoto = Split(Request.QueryString("Foto"),"$")
		If (Ubound(aSepFoto) > 1) Then
		%>
		<tr>
			<td width="150" class="tbltext1"><b>Testo foto piccola</b></td>	
			<td><input class="textblacka" Type="text" Name="txtAltFotoHp" Size="40" Maxlength="50"></td>
		</tr>
		<tr>
			<td width="150" class="tbltext1"><b>Testo foto grande</b></td>	
			<td><input class="textblacka" Type="text" Name="txtAltFotoApp" Size="40" Maxlength="50"></td>
		</tr>
		<tr>
			<td><input Type="hidden" Name="FotoHp" Value="<%=aSepFoto(1)%>"></td>
			<td><input Type="hidden" Name="FotoApp" Value="<%=aSepFoto(2)%>"></td>
		</tr>
		<%Else%>
		<tr>
			<td width="150" class="tbltext1"><b>Testo foto</b></td>	
			<td><input class="textblacka" Type="text" Name="txtAltFotoHp" Size="40" Maxlength="50"></td>
		</tr>
		<tr>
			<td><input Type="hidden" Name="FotoHp" Value="<%=aSepFoto(1)%>"></td>
		</tr>
		<%End If%>
	<% End If %>	
		<input Type="hidden" Name="LungURL" Value="<%=Len(PathDir)+1%>">
		<input Type="hidden" Name="TargetURL" Value="<%=rstCategorie("Percorso")%>">
		<input Type="hidden" Name="Categoria" Value="<%=IdCat%>">
		<input type="hidden" name="Build" value="<%=Request.QueryString("Build")%>">
	<tr>
		<td colspan="2" width="510" align="center">
			<input type="image" src="<%=Session("Progetto")%>/images/conferma.gif" Value="CreaNews" Name="CreaNews"><br>
		</td>
	</tr>
</table>
</form>
<% Else %>
<form Name="InvioForm" Method="post" Action="DOC_ModNews.asp?Nr=<%=IdNews%>" onSubmit="return ControllaCheck()">
<table width="510" BORDER="0" CELLPADDING="2" CELLSPACING="2">
	<tr>
		<td width="150" class="tbltext1"><b>Titolo*</b></td>	
		<td width="360"><input class="textblacka" Type="text" Name="Titolo" Size="50" onblur="ControlloTitolo()" Value="<%=rstNotizie("Titolo")%>"></td>	
	</tr>
	<tr>
		<td width="150" class="tbltext1"><b>Abstract <br>(lunghezza max 400)</b></td>	
		<td width="360"><textarea class="textblacka" Name="Abstract" rows="5" cols="49"><%=rstNotizie("Abstract")%></textarea></td>	
	</tr>
	
	<% If Session("Tipo") <> "R" then %>
	<tr>
		<td width="150" class="tbltext1"><b>Nome News*<b></td>	
		<td width="360"><input class="textblacka" Type="text" Name="NomeNews" Size="50" Maxlength="50" Value="<%=rstNotizie("Nome_Notizie")%>"></td>	
	</tr>
	<% Else %>
		<input class="textblacka" Type="hidden" Name="NomeNews" Size="50" Maxlength="50" value="<%=rstNotizie("Nome_Notizie")%>">
	<% End if %>
	<tr>
		<td width="150" class="tbltext1"><b>Data di Invio</b></td>	
		<td width="360"><input class="textgray" Type="text" Name="DataInvio" Size="11" Value="<%=ConvDateToString(rstNotizie("Data_Ins"))%>" Maxlength="10" readonly></td>	
	</tr>
	<tr>
		<td width="150" class="tbltext1"><b>Data di Pubblicazione</b></td>	
		<td width="360"><input class="textgray" Type="text" Name="DataPubb" Size="11" Value="<%=ConvDateToString(rstNotizie("Data_Pubb"))%>" Maxlength="10" readonly></td>	
	</tr>
	<tr>
		<td width="150" class="tbltext1"><b>Fonte</b></td>	
		<td width="360"><input class="textblacka" Type="text" Name="Fonte" Size="40" Maxlength="50" Value="<%=rstNotizie("Fonte")%>"></td>	
	</tr>
	<tr>
		<td width="150" class="tbltext1"><b>Autore</b></td>	
		<td width="360"><input class="textgray" Type="text" Name="Autore" Size="48" Maxlength="50" Value="<%=rstNotizie("Autore")%>" readonly></td>	
	</tr>
	<%if rstNotizie("Gif_HP") <> "" then%>
	<tr>
		<td width="150" class="tbltext1"><b>Testo foto piccola</b></td>	
		<td><input class="textblacka" Type="text" Name="txtAltFotoHp" value="<%=rstNotizie("alt_hp")%>" Size="40" Maxlength="50"></td>
	</tr>
	<%end if
	if rstNotizie("Gif_Notizie") <> "" then%>
	<tr>
		<td width="150" class="tbltext1"><b>Testo foto grande</b></td>	
		<td><input class="textblacka" Type="text" Name="txtAltFotoApp" value="<%=rstNotizie("alt_Notizie")%>" Size="40" Maxlength="50"></td>
	</tr>
	<%end if%>
	<tr>
		<td colspan="2" width="510" align="center">
			<input Type="image" src="<%=Session("Progetto")%>/images/conferma.gif" Name="ModificaNews"><br>
		</td>
	</tr>	
		<input Type="hidden" Name="LungURL" Value="<%=Len(PathDir)+1%>">
		<input Type="hidden" Name="TargetURL" Value="<%=rstCategorie("Percorso")%>">
		<input Type="hidden" Name="Categoria" Value="<%=IdCat%>">	
		<input Type="hidden" Name="OldFile" Value="<%=rstNotizie("Nome_Notizie")%>">
</table>
</form>
<% If Session("Tipo")="N" Then %>
<form Name="InvioFoto" Method="post" Action="DOC_ModFotoNews.asp?Nr=<%=IdNews%>" onSubmit="return ControllaFoto()">
<table width="500" BORDER="0" CELLPADDING="2" CELLSPACING="2">
	<tr class="sfondocomm">
		<td colspan="3" width="510" class="tbltext1" align="center">
		<b>Sostituzione delle foto della notizia <i><%=rstNotizie("Nome_Notizie")%></b></i>
		</td>
	</tr>
	<tr>
		<td width="140" align="center" class="tbltext1"><b>Foto Piccola</b></td>
		<td width="350" align="center" class="tbltext1"><b>Foto Grande</b></td>
		<td rowspan="2" width="10">&nbsp;</td>
	</tr>
	<tr>
	<% If isnull(rstNotizie("Gif_HP")) Then %>
		<td width="140" align="center" class="tbltext1">Non Disponibile</td>
	<% Else %>
		<td width="150" align="center"><img src="<%=PathDirImg%>\<%=rstNotizie("Gif_HP")%>" width="50" Height="50" Alt="Foto Piccola"></td>
	<% End If %>
	<% If isnull(rstNotizie("Gif_Notizie")) Then %>
		<td width="350" align="center" class="tbltext1">Non Disponibile</td>
	<% Else %>
		<td width="350" align="center"><img src="<%=PathDirImg%>\<%=rstNotizie("Gif_Notizie")%>" width="200" Height="100" Alt="Foto Grande"></td>
	<% End If %>		
	</tr>
	<tr>
		<td width="140" align="center" class="tbltext1"><b>Foto 1</b><input Type="radio" Value="1" Name="ModFoto"></td>
		<td width="350" align="center" class="tbltext1"><b>Foto 2</b><input Type="radio" Value="2" Name="ModFoto"></td>
		<td width="250" align="center" class="tbltext1"><b>Entrambe</b><input Type="radio" Value="0" Name="ModFoto"></td>
	</tr>
	<tr>
		<td colspan="3" width="510" Align="center"><input Type="image" src="<%=Session("Progetto")%>/Images/cambia.gif" Name="ModFotoApp"></td>
	</tr>
	<tr>	
		<td><input Type="hidden" Name="TargetURL" Value="<%=rstCategorie("Percorso")%>"></td>
		<td><input Type="hidden" Name="Categoria" Value="<%=IdCat%>"></td>
	</tr>
</table>
</form>
<% 
	rsttipoNotizie.close
	Set rsttipoNotizie = nothing 
	rstNotizie.Close
	Set rstNotizie = nothing 
	rstCategorie.Close
	Set rstCategorie = nothing 
	End If
End If %>
<!-- #include virtual = "/include/closeconn.asp" -->
<!--#include virtual="/strutt_coda2.asp"-->
