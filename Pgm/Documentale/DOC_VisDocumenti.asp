<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #include virtual="/strutt_testa2.asp" -->
<!-- #include virtual="/util/portallib.asp" -->
<!-- #include virtual="/include/OpenConn.asp" -->
<!-- #include virtual="/include/ckProfile.asp" -->
<!-- #include virtual="/include/ControlDateVB.asp" -->
<script language="javascript" src="/Include/help.inc"></script>
<script language="javascript">
	function Cancella(nomeform){
	if (confirm("�Est� seguro de querer eliminar el documento?")){
	   document.all.item(nomeform).submit()
	}
}
</script>
<%
If Not ValidateService(Session("IdUtente"),"Sistema Documentale",cc) Then
	response.redirect "/util/error_login.asp"
End if

Dim nActPagina
Dim nTotPagina
Dim nTamPagina
Dim nTotRecord

nTamPagina = 5

If Request.Form("Page") = "" Then
	nActPagina = 1
Else
	nActPagina = Clng(Request.Form("Page"))
End If

Idtema = Request.Form("Tema")

Dim DirScelta, Posbarra, Pathname
Dim fso, f

Set rstCategorie = Server.CreateObject("ADODB.Recordset")
SQLcategorie = "SELECT Id_Categoria, Descrizione, Percorso, Padre, Abstract, Tipo FROM Categoria WHERE Id_Categoria =" & IdTema

'Response.Write(SQLcategorie)

'PL-SQL * T-SQL
SQLCATEGORIE = TransformPLSQLToTSQL (SQLCATEGORIE)



rstCategorie.open SQLcategorie,CC,1,3
nIdCatScelta = rstCategorie("Id_Categoria")

If rstCategorie("Tipo") <> "D" Then
	Response.Redirect "/pgm/documentale/DOC_InitDocumenti.asp"
End if

DirScelta = rstCategorie("Percorso")

Pathname = Session("DirInit") & DirScelta
FileEdit = Session("DirInitEdit") & DirScelta & "/" & Session("IdUtente") & ".htm"
Set fso = CreateObject("Scripting.FileSystemObject")
on error resume next
Set f = fso.GetFolder(Pathname)
If err.number <> 0 Then
	Response.Write "<b class='tbltext'><br>En este momento no se puede visualizar la p�gina"
	Response.Write "<p>Contactar el Grupo Asistencia Portal Italia Lavoro <br>"
	Response.Write "a la direcci�n <a href='mailto:po-assistenza@italialavoro.it'>po-assistenza@italialavoro.it</a></p></b>"
	Response.End
End If
%>
<br>
	<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
		<tr height="18">
			<td class="sfondomenu" width="67%" height="18"><span class="tbltext0"><b>&nbsp;AREA REDACCIONAL - <%=Session("Intestazione")%> </b></span></td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
			<td valign="middle" align="right" class="tbltext1" width="40%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
		</tr>
		<tr>
			<td class="sfondocomm" width="57%" colspan="3">
			Para ingresar un documento ya presente en el propio computador (.doc, .xls, .zip, etc.) utilizar <b>env�o Archivo</b>. <br>
			Para modificar la descripci�n de la categor�a corriente o para eliminar la categor�a presionar sobre <b>Modifica</b> o sobre <b>Elimina</b> del men� horizontal <br>
			Para visualizar un documento presionar sobre el Nombre del File correspondiente, para modificarlo o eliminarlo utilizar los s�mbolos presentes al lado de cada documento.
			<a href="<%=Session("Progetto")%>" onclick="Javascript:Show_Help('/Pgm/help/Documentale/DOC_VisDocumenti'); return false">
			<img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0" width="18" height="18" alt="Para mayores informaciones"></a></td>
		</tr>

		<tr height="17">
			<td class="sfondocomm" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
		</tr>
	</table>
<!-- #include file="DOC_VisBarra.asp" -->
<%
Set rstSubCategorie = Server.CreateObject("ADODB.Recordset")
SQLSubCategorie = "SELECT Id_Categoria, Gif_Categoria, Descrizione, Abstract FROM Categoria WHERE Padre=" & Idtema & "ORDER BY Descrizione"
'PL-SQL * T-SQL
SQLSUBCATEGORIE = TransformPLSQLToTSQL (SQLSUBCATEGORIE)
rstSubCategorie.open SQLSubCategorie, CC, 1, 3
Response.Write "<TABLE width=500 border=0>"
Response.Write "<tr class=sfondocomm><td colspan='3' width='500'><B>Sub-argumentos:</B></td></tr>"
Response.Write "<tr class=sfondocomm><td width='50' align='center'><b>Img</b></td><td width='250' align='center'><b>Nombre Carpeta</b></td><td width='200' align='center'><b>Resumen</b></td></tr>"
If rstSubCategorie.BOF Then
	Response.Write "<tr class='tblsfondo'><td colspan='3' width='500' class='tbltext' align='center'><B>--No existen sub-argumentos--</B></td></tr></TABLE><br>"
Else
	nIndSubcat = 0
	Do While not rstSubCategorie.EOF
		nIndSubcat = nIndSubcat + 1
		If isnull(rstSubCategorie("Gif_Categoria")) Or rstSubCategorie("Gif_Categoria") = " " Then
			Response.Write "<tr class=tblsfondo><td width='50' align='center'><img src='/images/icons/cartella.gif' width='17' height='17'>"
		Else
			Response.Write "<tr class=tblsfondo><td width='50'><img src='" & Session("Progetto") & "/images/categorie/" & rstSubCategorie("Gif_Categoria")
			Response.Write "' width='50' height='50'>"
		End If
		Response.Write "</td>"
		Response.Write "<FORM name='frm" & nIndSubcat & "' method='POST' action='DOC_VisDocumenti.asp'>"
        Response.Write "<input type='hidden' name='Tema' value='" & rstSubCategorie("Id_Categoria") & "'>"

		Response.Write "<td align='left' width='250' class='tbltext'>"
		Response.Write "<a class='tbltext' href='" & Session("Progetto") & "' onClick='Javascript:frm" & nIndSubcat & ".submit();return false'>"

		Response.Write "<b>" & rstSubCategorie("Descrizione") & "</b></a></td></FORM>"
		Response.Write "<td align='left' width='200' class='tbltext'>" & rstSubCategorie("Abstract") & "</td></tr>"
		rstSubCategorie.MoveNext
	Loop
	Response.Write ("</TABLE><br>")
End if

Set rstNotizie = Server.CreateObject("ADODB.Recordset")
SQLnotizie = "SELECT Id_Notizie, Abstract, Id_Categoria, Nome_Notizie, Id_TipoDoc, " &_
	         "Fl_Pubblicato, Titolo FROM Notizie WHERE Id_Categoria = " & IdTema & "ORDER BY Id_Notizie Desc"
'PL-SQL * T-SQL
SQLNOTIZIE = TransformPLSQLToTSQL (SQLNOTIZIE)
rstNotizie.open SQLnotizie, CC, 1, 3

rstNotizie.PageSize = nTamPagina
rstNotizie.CacheSize = nTamPagina

nTotPagina = rstNotizie.PageCount

If nActPagina < 1 Then
	nActPagina = 1
End If
If nActPagina > nTotPagina Then
	nActPagina = nTotPagina
End If
NomePercorso= Replace(rstCategorie("Percorso"),"\","/")
'Response.Write("percorso" & NomePercorso)

PathDir = Session("Progetto") & "/Testi/sistdoc/Documentale/" & NomePercorso & "/"

Response.Write "<TABLE width='500' border='0'>"
%>
<tr height="18">
			<td width="100%" colspan="6" height="18" class="sfondocomm"><b>Documentos contenidos en la carpeta <i><%=UCase(rstCategorie("Descrizione"))%></i></b><br>
			La p�gina ser� visible en el portal en la direcci�n:<br>
			<b>http://<%=Request.ServerVariables("Server_Name")%><%=PathDir%>[titolo]</b></td>
	</tr>
	<%
Response.Write "<tr class='sfondocomm'><td width='30' align='center'><b>Tipo</b></td><td width='180' align='center'><b>Nombre archivo [T�tulo]</b></td><td width='40' align='center'><b>Dim.</b></td><td colspan='3'>&nbsp;</td></tr>"

If rstNotizie.BOF Then
	Response.Write "<tr class='tblsfondo'><td colspan='6' class='tbltext' align='center' width='500'><b>--No existen documentos--</b></td></tr></TABLE>"
Else

	rstNotizie.AbsolutePage = nActPagina
	nTotRecord=0
	nIndFrm = 0
	While not rstNotizie.EOF And nTotRecord < nTamPagina
		nIndFrm = nIndFrm + 1
		Response.Write "<FORM name='CanDoc" & nIndFrm & "' method='POST' action='DOC_CanFile.asp'>"
        Response.Write "<input type='hidden' name='Nr' value='" & rstNotizie("Id_Notizie") & "'>"
		Response.Write "<input type='hidden' name='P' value='Ko'>"
		Response.Write "</FORM>"
		Response.Write "<FORM name='PubDoc" & nIndFrm & "' method='POST' action='DOC_CanFile.asp'>"
        Response.Write "<input type='hidden' name='Nr' value='" & rstNotizie("Id_Notizie") & "'>"
		Response.Write "<input type='hidden' name='P' value='Ok'>"
		Response.Write "</FORM>"
		Response.Write "<FORM name='ModDoc" & nIndFrm & "' method='POST' action='DOC_ModDoc.asp'>"
        Response.Write "<input type='hidden' name='Nr' value='" & rstNotizie("Id_Notizie") & "'>"
		Response.Write "</FORM>"

		Set rstTipo = Server.CreateObject("ADODB.Recordset")
		SQLTipo = "SELECT Id_TipoDoc, Tipo, Gif_Tipo FROM TipoDoc WHERE Id_TipoDoc=" & rstNotizie("Id_TipoDoc")
'PL-SQL * T-SQL
SQLTIPO = TransformPLSQLToTSQL (SQLTIPO)
		rstTipo.open SQLTipo, CC, 1, 3

		sPathFile = Pathname & "/" & rstNotizie("Nome_Notizie") & "." & rstTipo("Tipo")
		sPathFile =Replace(sPathFile,"\","/")
		Set fl = fso.GetFile(sPathFile)

		Response.Write "<tr class='tblsfondo'><td width='30' align='center'><img src='/Images/Icons/" & rstTipo("Gif_Tipo") & "' alt='" & rstTipo("Tipo") & "'>"
		Response.Write "</td><td align='left' width='300' class='tbltext'>"

		sTitoloFile	= rstNotizie("Nome_Notizie") & "." & rstTipo("Tipo")
		sPosFile = PathDir & sTitoloFile

		sPosFile = Replace(sPosFile,"'","$")
		sTitolo = Replace(rstNotizie("Titolo"),"'","$")

		If LCase(rstTipo("Tipo")) <> "zip" and LCase(rstTipo("Tipo")) <> "ram" Then
		Response.Write "<a class='tbltext' href=""javascript:Leggi('" & sPosFile & "','" & sTitolo & "')""><b>" & rstNotizie("Titolo") & " [" & sTitoloFile & "]</b></a></td>"
		Else
		Response.Write "<a class='tbltext' href='" & sPosFile & "'><b>" & rstNotizie("Titolo") & " [" & sTitoloFile & "]</b></a></td>"
		End If

		If fl.Size > 1024 Then
			DimDoc = Round(fl.Size/1024,2)
			StrDimDoc = DimDoc & " Kb"
		Else
			DimDoc = fl.Size
			StrDimDoc = DimDoc & " bytes"
		End If

		Response.Write "<td align='left' width='70' class='tbltext'>" & StrDimDoc & "</td>"

			If Session("GruppoDoc") = 0 Then
				Response.Write "<td width='20' align='center' class='tbltext'><a href=""Javascript:Cancella('CanDoc" & nIndFrm & "')"">"
				Response.Write "<img src='/images/SistDoc/Delete.gif' width='25' height='21' border='0' alt='Elimina Documento '></a></td>"
			End If

			If cint(rstTipo("Id_TipoDoc")) = 6 Then
				Response.Write "<td width='25' align='center'><a  class='tbltext' href=DOC_LeggiNews.asp?Nr=" & rstNotizie("Id_Notizie")
			Else
				Response.Write "<td width='25' align='center'><a class='tbltext' href='" & Session("Progetto") & "' onClick='Javascript:ModDoc" & nIndFrm & ".submit();return false'"
			End If

			Response.Write "><img src='/Images/SistDoc/ModDoc.gif' width='18' height=21 border=0 alt='Modifica Documento'></a></td>"
			If cint (rstNotizie("Fl_Pubblicato")) = 0 And Session("GruppoDoc") = 0 Then
				if sVisitor = false then
				Response.Write "<td><a class=tbltext href='" & Session("Progetto") & "' onClick='Javascript:PubDoc" & nIndFrm & ".submit();return false'><img src=/Images/SistDoc/pubb.gif alt='Publica' border=0></a>"
				else
				Response.Write "<td>&nbsp;</td>"
				end if
			Else
			Response.Write "<td><img src='" & session("Progetto") & "/images/visto.gif' width='18' height='18' alt='Documento Publicado'></td>"
			End If

		Response.Write "</tr>"
		Response.Write "<tr class='tblsfondo'><td width=30 align='center' valign='top'>&nbsp;</td>"
		Response.Write "<td colspan='5' align='left' width='575' class='tbltext'>" & rstNotizie("Abstract") & "</td>"
		Response.Write "</tr>"
		nTotRecord=nTotRecord + 1
		rstNotizie.MoveNext
	Wend
	Response.Write "</TABLE>"
	rstTipo.Close
	Set rstTipo = nothing
End If

Response.Write "<TABLE border='0' width='500'>"
Response.Write "<tr>"
If nActPagina > 1 Then
	Response.Write "<FORM name='PageDown' method='POST' action='DOC_VisDocumenti.asp'>"
	Response.Write "<input type='hidden' name='Tema' value='" & Idtema & "'>"
	Response.Write "<input type='hidden' name='Page' value='" & nActPagina-1 & "'>"
	Response.Write "</FORM>"
	Response.Write "<td align='right' width='480'>"
	Response.Write "<a class=tbltext href='" & Session("Progetto") & "' onClick='Javascript:PageDown.submit();return false'>"
	Response.Write "<img border='0' alt='P�gina anterior' src='" & Session("Progetto") & "/images/precedente.gif'></a></td>"
End If
If nActPagina < nTotPagina Then
	Response.Write "<FORM name='PageUp' method='POST' action='DOC_VisDocumenti.asp'>"
	Response.Write "<input type='hidden' name='Tema' value='" & Idtema & "'>"
	Response.Write "<input type='hidden' name='Page' value='" & nActPagina+1 & "'>"
	Response.Write "</FORM>"
	Response.Write "<td align='right'>"
	Response.Write "<a class=tbltext href='" & Session("Progetto") & "' onClick='Javascript:PageUp.submit();return false'>"
	Response.Write "<img border='0' alt='P�gina sucesiva' src='" & Session("Progetto") & "/images/successivo.gif'></a></td>"
End If
Response.Write "</tr></TABLE>"
rstCategorie.Close
Set rstCategorie = nothing

rstSubCategorie.Close
Set rstSubCategorie = nothing

rstNotizie.Close
Set rstNotizie = nothing
%>
<br>
<table border="0" cellpadding="0" cellspacing="0" width="500" align="center">
	<tr align="center">
		<td>
		<a href="<%=Session("Progetto")%>" onClick="javascript:history.back();return false">
		<img src="<%=Session("progetto")%>/images/indietro.gif" border="0"></a>
		</td>
	</tr>
</table>
<!-- #include virtual="/include/closeconn.asp" -->
<!-- #include virtual="/strutt_coda2.asp" -->
