<%@ Language=VBScript %>
<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #include virtual="/strutt_testa2.asp" -->
<!-- #include virtual="/util/portallib.asp" -->
<!-- #include virtual="/include/OpenConn.asp" -->
<!-- #include virtual="/include/ckProfile.asp" -->
<!-- #include virtual="/include/ControlDateVB.asp" -->

<script language="javascript" src="/Include/help.inc"></script>
<script language="javascript">

function Cancella(nomeform){
	if (confirm("�Est� seguro de querer eliminar el documento?")){
	   document.all.item(nomeform).submit()
	}
}     
</script>
<%
If (Not ValidateService(Session("IdUtente"),"Gestione Notizie",cc)) And (Not ValidateService(Session("IdUtente"),"Sistema Documentale",cc)) Then 
	response.redirect "/util/error_login.asp"
End If
' Dato id ricavo i dati della notizia corrispondente
IdNews = Request.QueryString("Nr")
Set rstNotizie = Server.CreateObject("ADODB.Recordset")
SQLnotizie = "SELECT Id_Notizie, Titolo, Abstract, Data_Ins, Id_Categoria, Nome_Notizie, Autore, Gif_HP, Gif_Notizie, Fl_Pubblicato, Id_TipoDoc FROM Notizie WHERE Id_Notizie=" & IdNews 
'PL-SQL * T-SQL  
SQLNOTIZIE = TransformPLSQLToTSQL (SQLNOTIZIE) 
rstNotizie.open SQLnotizie, CC, 1, 3

IdTema = rstNotizie("Id_Categoria")
NomeNews = rstNotizie("Nome_Notizie")	
sIdTipoDoc = rstNotizie("Id_TipoDoc")
' Dato idcategoria ricavo il percorso dove si trova la notizia
Set rstCategorie = Server.CreateObject("ADODB.Recordset")
SQLcategorie = "SELECT Id_Categoria, Descrizione, Percorso, Tipo, Padre FROM Categoria WHERE Id_Categoria =" & IdTema
'PL-SQL * T-SQL  
SQLCATEGORIE = TransformPLSQLToTSQL (SQLCATEGORIE) 
rstCategorie.open SQLcategorie,CC,1,3

'Response.end

If Session("Tipo") <> rstCategorie("Tipo") and rstCategorie("Tipo") <> "L" Then
	Response.Redirect "/pgm/documentale/DOC_Init" & Session("Intestazione") & ".asp"
End if

Dim DirScelta, Posbarra, Pathname
Dim fso, f, fl

	DirScelta = rstCategorie("Percorso")

aSepDirScelta = split(DirScelta,"\")
' Controllo che la categoria sia comune o relativa ad un progetto 
If UCase(aSepDirScelta(0)) = "TESTI" Then
	PathDir = Server.MapPath("\") & "\" & DirScelta	
Else
	PathDir = Session("DirInit") & DirScelta
End If

PathFile = PathDir & "\" & NomeNews & ".htm"

' Controllo che il file sia autocostruito o meno 
If clng(sIdTipoDoc) = 11 then
	If 	UCase(aSepDirScelta(0)) <> "TESTI" then
		PathFile = Server.MapPath("\") & Session("Progetto") & "\Testi\SistDoc\" & DirScelta & "\" & NomeNews & ".htm"	
	Else
		PathFile = Server.MapPath("\") & "\" & DirScelta & "\" & NomeNews & ".htm"	
	End If
End if	

'response.Write PathFile 

	FileEdit= split(PathFile,Server.MapPath("\"))(1) 
	FileEdit = replace(FileEdit,"\","/")
	PathGif = Session("DirInitEdit") & DirScelta & "/" & rstNotizie("Gif_Notizie")
	PathGif = replace(PathGif,"\","/")

	Set fso = CreateObject("Scripting.FileSystemObject") 
	
' Cerco di leggere il file
	on error resume next
	Set fl = fso.GetFile(PathFile)
	If err.number <> 0 Then
		Response.Write "<b class='tbltext'><br>En este momento no se puede visualizar la p�gina"
		Response.Write "<p>Contactar el Grupo Asistencia Portal Italia Lavoro <br>"
		Response.Write "a la direcci�n <a href='mailto:assistenza@xxxxx.it'>assistenza@xxxxx.it</a></p></b>"
                Response.End
	Else
%>
		<script LANGUAGE="JAVASCRIPT">
			function HPImageOpen(image)
			{
				finimg = window.open("",'newWind','width=225,height=200,Resize=No')
				finimg.document.write("<title>Anteprima</Title>")
				finimg.document.write("<img width=200 height=100 src='" + image +"'>")
				finimg.document.write("<BR><BR><Center>")
				finimg.document.write("<input type='image' src='<%=Session("Progetto")%>/images/chiudi.gif' onclick='javascript:self.close()'>")
				finimg.document.write("</Center>")
			}

		</script>
		<br>
		<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
			<tr height="18">
				<td class="sfondomenu" width="67%" height="18"><span class="tbltext0"><b>&nbsp;AREA REDAZIONALE - <%=Session("Intestazione")%></b></span></td>
				<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
				<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
			</tr>
			<tr>
				<td class="sfondocomm" width="57%" colspan="3"><br>
				Para modificar el texto de las Noticias utilisar <b>Modifica Texto</b>.<br>
						Para modificar los atributos de las noticias utilizar <b>Modifica Atributos</b>.<br>
<%						If Session("GruppoDoc") = "0" Then %>
							Para eliminar la noticia utilizar <b>Elimina</b>.<br>
<%						End If
				
				If Session("Tipo") = "R" or ( Session("Tipo") = "N" And Session("GruppoDoc") = "0" And Cint(rstNotizie("Fl_Pubblicato")) = 0 )Then %>
				   En el momento en que se salvan los cambios el nuevo texto ser� visible en esta p�gina.<br>
				   N.B.: Para hacer visible los cambios p�blicamente en el portal es necesario publicar el documento 
				   utilizando la opci�n <b>P�blica</b>.<br>
		
<%				End If  
'-------------selezione pagina help----------------------------
				sTipoDoc=Session("Tipo")
'Response.Write sTipoDoc
				Select Case sTipoDoc

					Case "N" sNomCart="doc_legginews"
	
					Case "D" sNomCart="doc_legginews_1"
	
					Case "R" sNomCart="doc_legginews_2"
		
				End Select
'-------------------------------------------------------------
%>
			     <a href="Javascript:Show_Help('/Pgm/help/Documentale/<%=sNomCart%>')" name onmouseover="javascript:status='' ; return true">
				<img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0" width="18" height="18" alt="Para mayores informaciones"></a>
	
				</td>
			</tr>
			<tr height="17">
				<td class="sfondocomm" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
			</tr>
		</table>

		<form id="CartSup" Method="POST" Action="DOC_Vis<%=Session("Intestazione")%>.asp">
		<table border="0" width="500" CELLPADDING="0" CELLSPACING="0">
		  <input type="hidden" name="Tema" value="<%=rstCategorie("Id_Categoria")%>">
			<tr>
				<td width="500" colspan="2">
					<font face="WingDings" size="2"><%=chr(199)%></font>&nbsp;
					<b><a title="Categor�a de nivel superior" class="textred" href="<%=Session("Progetto")%>" onClick="Javascript:CartSup.submit();return false">Categor�a de nivel superior</a></b>
				</td>
			</tr>	
			<tr>
				<td class="tbltext1" width="100"><b>Ubicaci�n:</b></td>
				<td class="textblack" width="400"><%=DirScelta%></td>
			</tr>
			<tr>
				<td class="tbltext1"><b>Autor:</b></td>
				<td class="textblack"><%=LCase(rstNotizie("Autore"))%></td>
			</tr>
			<tr>
				<td class="tbltext1"><b>Creado:</b></td>
				<td class="textblack"><%=ConvDateToString(fl.DateCreated)%></td>
			</tr>
			<tr>
				<td class="tbltext1"><b>Ultimo cambio:</b></td>
				<td class="textblack"><%=ConvDateToString(fl.DateLastModified)%></td>
			</tr>
		</table>
		</form>

<%		'If not isnull(rstNotizie("Gif_Notizie")) Then %>
		<!--table border="0" CELLPADDING="2" CELLSPACING="2" width="500">
			 <tr>
				<td rowspan="2" align="left" valign="top&quot;"> 
					<a class="textred" href="javascript:HPImageOpen('<%=PathGif%>')">
					<b>Anteprima immagine Notizia</b></a>
				</td>
			 </tr>
		</table-->
<%'End If %>

		<table CELLPADDING="0" CELLSPACING="0" border="0" width="500">
			<tr>
				<td width="100%" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
			</tr>
		</table>
	
		<table CELLPADDING="0" CELLSPACING="0" border="0" width="500">
			<form name="PubDoc" method="POST" action="DOC_CanFile.asp">
				<input type="hidden" name="Nr" value="<%=IdNews%>">
				<input type="hidden" name="P" value="Ok">
			</form>
			<form name="CanDoc" method="POST" action="DOC_CanFile.asp">
				<input type="hidden" name="Nr" value="<%=IdNews%>">
				<input type="hidden" name="P" value="Ko">
			</form>
			<tr>
				<td width="23"><img src="/images/sistdoc/moddoc.gif" height="21" alt="Modifica Texto del documento">&nbsp;</td>
				<td class="tbltext1"><a class="tbltext1" href="/util/pmeditor.asp?Page=<%=FileEdit%>&amp;Id=<%=IdNews%>" title="Modifica Texto del documento"><b>Modifica Texto</b></a></td>

				<td width="23"><img src="/images/sistdoc/moddoc.gif" width="23" height="21" alt="Modifica Atributos del documento">&nbsp;</td>
				<td class="tbltext1"><a class="tbltext1" href="DOC_InsDoc.asp?Nr=<%=IdNews%>&amp;Cod=MA" title="Modifica Atributos del documento"><b>Modifica Atributos</b></a></td>
<%			
			' Il pulsante Elimina visibile solo se ...
			If Session("GruppoDoc") = 0 Then
				If (Session("Tipo") <> "R" or ucase(aSepDirScelta(1))="INFOPRIMOLIVELLO" or ucase(aSepDirScelta(1))="INCENTIVI" or ucase(aSepDirScelta(1))="AUTORIZZAZIONI" or ucase(aSepDirScelta(0))="TESTI") Then %> 
				<td width="23"><img src="/images/sistdoc/delete.gif" width="23" height="21" alt="cancella il documento">&nbsp;</td>
				<td class="tbltext1"><a class="tbltext1" href="javascript:Cancella('CanDoc')" title="cancella il documento"><b>Elimina</b></a></td>	
<%				End If
			End If
			' Il pulsante Pubblica visibile per le news
			If cint(rstNotizie("Fl_Pubblicato")) = 0 And Session("Tipo") = "N" And Session("GruppoDoc") = 0 Then%>
				<td width="23"><img src="/images/sistdoc/pubb.gif" width="23" height="21" alt="Publica el documento">&nbsp;</td>
				<td class="tbltext1"><a class="tbltext1" href="<%=Session("Progetto")%>" onClick="javascript:PubDoc.submit();return false" title="Publica el documento"><b>Publica</b></a></td>	
<%  
			End if
			' Il pulsante Pubblica visibile per il redazionale 
			If Session("Tipo") = "R" And Session("GruppoDoc") = 0 Then
%>
				<td width="23"><img src="/images/sistdoc/pubb.gif" width="23" height="21" alt="Publica el documento">&nbsp;</td>
				<td class="tbltext1"><a class="tbltext1" href="DOC_CopiaFile.asp?F=<%=PathFile%>&amp;Nr=<%=IdNews%>" title="Publica el documento"><b>Publica</b></a></td>
<%			End if %>
			</tr>				
		</table>

		<table CELLPADDING="0" CELLSPACING="0" border="0" width="500">
			<tr>
				<td width="100%" colspan="8" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
			</tr>					
		</table>
		<br>
		<table WIDTH="510" BORDER="0" CELLPADDING="0" CELLSPACING="0">
			<tr>
				<td class="sfondocomm" colspan="2"><b>Contendo de &quot;<b><%=rstNotizie("Titolo")%></b>&quot;</b></td>
			</tr>
		</table>
		<br>
		<table WIDTH="510" BORDER="0" CELLPADDING="0" CELLSPACING="0">
			<tr>
				<td colspan="2">
					<%
					on error resume next 
'PL-SQL * T-SQL  
'FILEEDIT = TransformPLSQLToTSQL (FILEEDIT) 
						Server.Execute(FILEEDIT)
						
					If err.number <> 0 Then
						Response.Write "<b class='tbltext'><br>En este momento no se puede visualizar la p�gina"
						Response.Write "<p>Contactar el Grupo Asistencia Portal Italia Lavoro<br>"
						Response.Write "a la direcci�n <a href='mailto:po-assistenza@italialavoro.it'>po-assistenza@italialavoro.it</a></p></b>"
						Response.End
					End If
					%>
				</td>
			</tr>
		</table>
<%	
	End If

	rstCategorie.Close
	Set rstCategorie = nothing 
	rstNotizie.Close
	Set rstNotizie = nothing
%>
<!-- #include virtual="/strutt_coda2.asp" -->
