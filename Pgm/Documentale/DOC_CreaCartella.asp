<%@ Language=VBScript %>
<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #include virtual="/strutt_testa2.asp" -->
<!-- #include virtual="/util/portallib.asp" -->
<!-- #include virtual="/include/OpenConn.asp" -->
<!-- #include virtual="/include/ckProfile.asp" -->

<%
	If ((Not ValidateService(Session("IdUtente"),"Gestione categorie",cc)) And (Not ValidateService(Session("IdUtente"),"Sistema Documentale",cc))) Then 
		response.redirect "/util/error_login.asp"
	End If
%>

<script language="javascript" src="/Include/help.inc"></script>
<script language="javascript" src="/Include/ControlString.inc"></script>
<script LANGUAGE="javascript">
function CtrlForm() {
var inputForm = CreaDir.Descrizione.value
if (!ValidateInputStringIsValid(inputForm)) {
	return false
}
	
if (CreaDir.AbstractDir.value.length > 160){
	alert("L'abstract non puo' essere piu' lungo di 160 caratteri.\nAl momento e' lungo " + CreaDir.AbstractDir.value.length + " caratteri.");
	CreaDir.AbstractDir.focus();
	return false 
	}

if (CreaDir.Descrizione.value == ""){
	alert ("Non e' consentito l'invio \nsenza aver dato un nome all'argomento.");
	return false 
	}

if (CreaDir.Descrizione.value.length > 50){
	alert("L'abstract non puo' essere piu' lungo di 50 caratteri.\nAl momento e' lungo " + CreaDir.Descrizione.value.length + " caratteri.");
	CreaDir.Descrizione.focus();
	return false 
	}
}
</script>

<br>
<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
	<tr height="18">
		<td class="sfondomenu" width="67%" height="18"><span class="tbltext0"><b>&nbsp;AREA EDITORIAL - <%=Session("Intestazione")%> </b></span></td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
<%		If Request.QueryString("Up")<> "Ok" Then %>		
			<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">(*) campos obligatorios</td>
<%		Else %>
			<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
<%		End If %>		
	</tr>
<%	If Request.QueryString("Up") <> "Ok" Then
 
'-------------selezione pagina help----------------------------
		sTipoDoc=Session("Tipo")
		Select Case sTipoDoc

			Case "N" sNomCart="doc_creacartella"
	
			Case "D" sNomCart="doc_creacartella_1"
	
			Case "R" sNomCart="doc_creacartella_2"
	
		End Select
'-------------------------------------------------------------	
%>

	<tr>
		<td class="sfondocomm" width="57%" colspan="3"><br>Ingreso de una Nueva Categoria: <br>
		Ingresar en el campo <b>Nombre Argumento</b> el nombre para mostrar en la lista de categorias.<br>
		Ingresar en el campo <b>Descripcion Argumento</b> la descripcion de los contenidos de la nueva categoria.
		<a href="Javascript:Show_Help('/pgm/help/Documentale/<%=sNomCart%>')">
		<img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0" width="18" height="18" alt="para mas informacion"></a></td>
	</tr>
<%	End If %>
	<tr height="17">
		<td class="sfondocomm" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
	</tr>
</table>
<% 
Dim DirCrea, IdCat, Updir
	IdCat = Request.Form("Id")
	Updir = Request.Form("Up")
If IdCat = "" Then
	IdCat = "0"
End if
sTipo = Session("Tipo")
' Se la categoria risulta capostipite ricavo la posizione sul server
If (IdCat <> 0) Then
	Set rstCategorie = Server.CreateObject("ADODB.Recordset")
	SQLCategorie = "SELECT Id_Categoria, Descrizione, Percorso, Padre, Tipo FROM Categoria WHERE Id_Categoria =" & Idcat
'PL-SQL * T-SQL  
SQLCATEGORIE = TransformPLSQLToTSQL (SQLCATEGORIE) 
	rstCategorie.open SQLCategorie,CC,1,3
	
	sTipo = rstCategorie("Tipo")
	sDirScelta = rstCategorie("Percorso")
	Response.Write "<BR><TABLE border=0 cellspacing=2 cellpadding=2 width=500>"
	Response.Write "<tr><td width=120 class=tbltext1><b>Ubicacion Actuale:</b></td>"
	Response.Write "<td width=380 class=textblack>" & sDirScelta & "</td></tr></TABLE>"
	rstCategorie.Close
	Set rstCategorie = nothing 
End If
aSepDirScelta = split(sDirScelta,"\")
' Controllo se la categoria risulta capostipite
If (Idcat <> 0) Then
' Controllo che la categoria sia comune o relativa ad un progetto
	If UCase(aSepDirScelta(0)) <> "TESTI" then
		DirCrea = Session("DirInit") & sDirScelta & "\" & Trim(Request.Form("Descrizione"))
	Else
		DirCrea = Server.MapPath("\") & "\" & sDirScelta & "\" & Trim(Request.Form("Descrizione"))
	End If
Else
		DirCrea = Session("DirInit") & Trim(Request.Form("Descrizione"))
End If
' Controllo che il percorso sia minore del massimo consentito dal sistema operativo
If len(DirCrea) < 245 Then

	If (Updir <> "Ok") Then
%>
	<form Method="POST" Action="DOC_CreaCartella.asp" id="CreaDir" onSubmit="return CtrlForm()">
	<input type="hidden" name="Id" value="<%=IdCat%>">
	<input type="hidden" name="Up" value="Ok">
	<table BORDER="0" WIDTH="500" CELLPADDING="2" CELLSPACING="2">
		<tr>
			<td WIDTH="180" class="tbltext1"><b>Nombre Argumento*:</b></td>	
			<td WIDTH="320"><input class="textblacka" type="text" name="Descrizione" size="45"></td>
		</tr>
		<tr>
			<td WIDTH="180" class="tbltext1"><b>Descripcion Argumento:</b></td>	
			<td WIDTH="320"><textarea class="textblacka" name="AbstractDir" rows="4" cols="40"></textarea></td>	
		</tr>
		<tr>
			<td colspan="2" WIDTH="500" align="center">
			  <input type="image" src="<%=Session("progetto")%>/images/conferma.gif" title="Crea nuova cartella" name="submit" border="0" align="absBottom">
			  <a href="<%=Session("Progetto")%>" onClick="javascript:history.back();return false"><input type="image" src="<%=Session("progetto")%>/images/indietro.gif" title="Indietro" border="0" align="absBottom"></a>
			</td>
		</tr>
	</table>
	</form>		
<% 
	Else

		Dim sDescrizione, sAbstract, nTipo, nCharPath
		Dim fso, fd

		Set fso = CreateObject("Scripting.FileSystemObject")
' Controllo che non esista una cartella con lo stesso nome		
		If (fso.FolderExists(DirCrea)) Then
			Response.Write "<TABLE widht='500>"
			Response.Write "<tr><td align='center'><img src='/images/sistdoc/Upload/CreazioneNotOK.gif' border='0' width='400' height='260' alt='Creazione Cartella NOT OK'></td></tr>"
			Response.Write "<tr><td align='center' class='tbltext3'><b>La cartella <i class='textredb'>" & Request.Form("Descrizione")
			Response.Write "</i> esiste gi�.</b></td></tr>"
			Response.Write "<tr><td align='center'><a href='" & Session("Progetto") & "' onClick='javascript:history.back();return false'>"
			Response.Write "<img alt='Indietro' border=0 src='" & Session("Progetto") & "/images/indietro.gif'></a></td></tr>"
			Response.Write "</TABLE>"
%>
		<!--#include virtual="/strutt_coda2.asp"-->	
<%
			Response.End
		Else

			nCharPath = len(DirCrea)
' Cerco di creare la directory	       
			If nCharPath < 245 Then 
				on error resume next
				Set fd = fso.CreateFolder(DirCrea)
				If err.number <> 0 Then
					Response.Write "<b class='tbltext'><br>Carpeta no creada"
					Response.Write "<p>Contactar el Equipo de Ayuda del Portal Empleo <br>"
					Response.Write "all'indirizzo <a href='mailto:dgutierrez@italialavoro.it'>dgutierrez@italialavoro.it</a></p></b>"
'					Response.Write dircrea
					Response.End
				End If
			Else
				nDiffChar = nCharPath - 245 
				Response.Write nCharPath & "<br>"
				Response.Write nDiffChar
				Response.Write "<TABLE widht='500'>"
				Response.Write "<tr><td align='center'><img src='/images/sistdoc/Upload/CreazioneNotOK.gif' border='0' width='400' height='260' alt='Creazione Cartella NOT OK'></td></tr>"
				Response.Write "<tr><td align='center' class='tbltext3'><b>La ruta completa de la carpeta <i class='textredb'>" & Request.Form("Descrizione")
				Response.Write "</i><br>supera el limite de caracteres permitidos por el sistema operativo.</b></td></tr>"
				Response.Write "<tr><td align='center'><a href='" & Session("Progetto") & "' onClick='javascript:history.back();return false'>"
				Response.Write "<img alt='Atras' border='0' src='" & Session("Progetto") & "/images/indietro.gif'></a></td></tr>"
				Response.Write "</TABLE>"
%>
		<!--#include virtual="/strutt_coda2.asp"-->	
<%
				Response.End
			End if
		End If
 
		If (IdCat <> 0) Then
			sPercorso = sDirScelta & "\" & Trim(Request.Form("Descrizione"))
		Else
			sPercorso = Trim(Request.Form("Descrizione"))
		End If
	
		sPercorso = Replace (sPercorso, "'","''")
		sProgetto = Mid(Session("Progetto"),2)
		sDescrizione = Trim(Request.Form("Descrizione"))
		sDescrizione = Replace (sDescrizione, "'","''")
		sAbstract = Trim(Request.Form("AbstractDir"))
		sAbstract = Replace (sAbstract, "'","''")
	
' Aggiorno il DB	  	
		SQLCat = "INSERT INTO Categoria ( Descrizione, Percorso, Padre, Tipo, Abstract, Progetto )" 
		SQLCat = SQLCat & "VALUES ('" & sDescrizione & "','" & sPercorso & "'," & IdCat & ",'" & sTipo & "','" & sAbstract & "','" & sProgetto & "')" 
	
'PL-SQL * T-SQL  
SQLCAT = TransformPLSQLToTSQL (SQLCAT) 
		Set rstInCategorie = CC.Execute(SQLCat)
		 
			SqlModAtt = "SELECT Id_Categoria FROM Categoria WHERE Percorso = '" & sPercorso & "'"
'PL-SQL * T-SQL  
SQLMODATT = TransformPLSQLToTSQL (SQLMODATT) 
			Set rstModCat = CC.Execute(SqlModAtt)
			nIdCatRoot = rstModCat("Id_Categoria")
	
			SQLRuoloCategoria = "INSERT INTO Ruolo_Categoria (Id_Categoria, Cod_Rorga) VALUES (" & nIdCatRoot & ",'" & Session("Rorga") & "')" 
'PL-SQL * T-SQL  
SQLRUOLOCATEGORIA = TransformPLSQLToTSQL (SQLRUOLOCATEGORIA) 
		    Set rstInRuoloCategoria = CC.Execute(SQLRuoloCategoria)
	
		Response.Write "<TABLE widht='500' BORDER='0'>"
		Response.Write "<tr><td align='center'><img src='/images/sistdoc/Upload/CartellaOK.gif' border='0' widht='400' height='260' alt='Creacion Carpeta OK'></td></tr>"
		Response.Write "<tr><td align='center' class='tbltext3'><b>Ha sido creada la carpeta <i class='textredb'>" & Request.Form("Descrizione")
		Response.Write "</i></b></td></tr>"
		Response.Write "</TABLE>"
' Se la categoria non risulta del tipo redazionale posso associare una foto	  		
		If Session("Tipo") <> "R" Then	
%>
			<form id="InsImg" Method="post" Action="DOC_InviaImgDoc.asp?Id=<%=IdCat%>">
			<table WIDTH="500" BORDER="0">
				<tr>
					<td width="350" class="tbltext1" align="right"><b>Desea asociar una imagen a la categoria creada?</b>&nbsp;&nbsp;</td>
					<td><input type="image" alt="Aggiungi" border="0" src="<%=Session("Progetto")%>/images/aggiungi.gif" name="b"></td>
				</tr>
				<tr>
					<td colspan="2" align="center"><input type="hidden" Name="DirCreata" Value="<%=Trim(Request.Form("Descrizione"))%>"></td>
				</tr>
			</table>
			</form>
<%	
		End If
		
		Response.Write "<Form id='FormIndietro' Method='post' Action='DOC_Vis" & Session("Intestazione") & ".asp'>"
		Response.Write "<TABLE WIDTH=500 BORDER=0>"
		If Idcat=0 Then
			Response.Write "<input type='hidden' name='Tema' value='" & nIdCatRoot & "'>" 
		Else
			Response.Write "<input type='hidden' name='Tema' value='" & IdCat & "'>" 
		End If
		Response.Write "<tr><td align=center><a href='" & Session("Progetto") & "' onClick='Javascript:FormIndietro.submit();return false'>"	
		Response.Write "<img alt=Atras border=0 src=" & Session("Progetto") & "/images/indietro.gif></a></td></tr>"
		Response.Write "</TABLE>"
	
	End If
Else

Response.Write "<TABLE widht='500'>"
Response.Write "<tr><td align='center'><img src='/images/sistdoc/Upload/CreazioneNotOK.gif' border='0' widht='400' height='260' alt='Creacion Cartpeta NOT OK'></td></tr>"
Response.Write "<tr><td align='center' class='tbltext3'><b>La ruta completa de la carpeta"
Response.Write "<br>ha alcanzado el limite  maximo de caracteres <br>permitidos por el sistema operativo.</b></td></tr>"
Response.Write "<tr><td align='center'><a href='" & Session("Progetto") & "' onClick='javascript:history.back();return false'>"
Response.Write "<img alt='Atras' border=0 src='" & Session("Progetto") & "/images/indietro.gif'></a></td></tr>"
Response.Write "</TABLE>"

End If
%>
<!-- #include virtual="/include/closeconn.asp" -->
<!-- #include virtual="/strutt_coda2.asp" -->	

