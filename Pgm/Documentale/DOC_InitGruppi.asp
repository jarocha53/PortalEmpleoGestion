<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #include Virtual="/strutt_testa2.asp" -->
<!-- #include virtual="/util/portallib.asp" -->
<!-- #include virtual="/include/OpenConn.asp" -->
<!-- #include virtual="/util/globalsub.asp" -->

<%
If Not ValidateService(Session("IdUtente"),"DOC_InitGruppi",cc) Then 
	Response.Redirect "/util/error_login.asp"
End If
%>
<script Language="JavaScript" src="/Include/help.inc"></script>

<script Language="JavaScript">
function checkField(Form){
	if (FormGruppo.Gruppo.value == 0){
	    alert ("E' necessario selezionare una funzione");
		FormGruppo.Gruppo.focus();
		return false;
		}
	else
		{
		FormGruppo.submit()
	}
}			
</script>
<br>
<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
	<tr height="18">
		<td class="sfondomenu" width="67%" height="18"><span class="tbltext0"><b>&nbsp;Configuración del Sistema - </b></span></td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
		<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
	</tr>
	<tr>
		<td class="sfondocomm" width="57%" colspan="3"><br>
			 Utilizar esta página para crear y/o modificar el contenido de la página de acceso del grupo.
		    <a href="Javascript:Show_Help('/Pgm/help/Documentale/DOC_InitGruppi')" name onmouseover="javascript:status='' ; return true">
     	     <img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0" width="18" height="18" alt="per maggiori informazioni"></a>
		</td>
	</tr>
	<tr height="17">
		<td class="sfondocomm" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
	</tr>
</table>
<br>

<form method="POST" action="DOC_VisGruppi.asp" name="FormGruppo">
<table border="0" width="500">
	<tr height="20">
		<td align="left" colspan="4" class="textblack"><b>
		Seleccionar un grupo de los existentes</b></td>
	</tr>
	<tr>
		<td height="25" class="tbltext1" align="left">&nbsp;Grupos existentes</td>
		<td>
			<select name="Gruppo" onchange="return checkField(this);">
			<option value="0"></option>
<%  
			Set rstGruppo = Server.CreateObject("ADODB.RECORDSET")
			SQLGruppo = "SELECT IDGRUPPO,DESGRUPPO FROM GRUPPO ORDER BY 2"
'PL-SQL * T-SQL  
SQLGRUPPO = TransformPLSQLToTSQL (SQLGRUPPO) 
			rstGruppo.open SQLGruppo, CC, 1, 3
			rstGruppo.MoveFirst

			While rstGruppo.EOF <> True 
			    response.write "<option value='" & rstGruppo("IDGRUPPO") & "'>" & rstGruppo("DESGRUPPO") & "</option>"
			    rstGruppo.MoveNext
			Wend
			rstGruppo.close
			set rstGruppo=nothing
%>
			</select>
		</td>
	</tr>
</table>
</form>

<!-- #include virtual="/include/closeconn.asp" -->
<!-- #include Virtual="/strutt_coda2.asp" -->
