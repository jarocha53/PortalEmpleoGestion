<!--#include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include virtual="/strutt_testa2.asp"-->
<!--#include virtual="/util/portallib.asp"-->
<!-- BLOCCO SCRIPT	-->
	<script LANGUAGE="JavaScript" src="/Include/help.inc"></script>
	<script LANGUAGE="JavaScript">
	<!--
	function controllo_date(n){		
		return true;
	}
		
	function validateIt(n)	{ 
		if (FormMod.txtDesc.value == "") {
				alert("La descripci�n del canal es obligatoria")
				FormMod.txtDesc.focus();	
				return false;
			}
			
		if (FormMod.txtAbstr.value.length > 160){
				alert("El resumen no puede superar los 160 caracteres.\nEs largo " + FormMod.txtAbstr.value.length + " caracteres");
				FormMod.txtAbstr.focus();
				return false;
			}
				
	// si controlla il numero dei ruoli selezionati per la fruizione della categoria
		var R = 0	//indicatore del numero dei ruoli
		for (var i=0 ; i<n;i++) 	{	
			if(eval("FormMod.chkRuo" + i +".checked"))
				R = R + 1;
			
			if ( eval("document.FormMod.chkIdGru" + i )) {
				nGrupElem = 0
				if ( eval("document.FormMod.chkIdGru" + i + ".length")) 
					nGrupElem = eval("document.FormMod.chkIdGru" + i + ".length")
				if (nGrupElem > 1) {
					for ( t=0; t<nGrupElem; t++ ) {
						if (eval("document.FormMod.chkIdGru" + i + "[" + t + "].checked")) 
							R = R + 1;
										
					}
				} else {
					if (eval("document.FormMod.chkIdGru" + i + ".checked"))
						R = R + 1 ;
				}
				
			}				
		}

				
		// Si controlla se l'utente vuole passare da un tipo di fruizione pubblica a una particolare
			
		if (FormMod.chkPS.value != "Pubblico"){	
		//chkPs � != Pubblico se la categoria  � pubblica
			if (R != 0 ) {
		//se l'utente seleziona uno dei canali disponibili essendo la categoria di tipo pubblico
		//la sua scelta comporter� l'eliminazione del canale pubblico dalla categoria
				if (confirm("Seleccionando el siguiente canal ser� eliminado el canal P�blico de los usuarios de esta categor�a.\n�Se desea proceder?"))
					return true;
				else
					return false;	
			} else {
				alert("No ha sido seleccionado ning�n canal");
				return false;
			}
		}
			
		if (FormMod.chkPS.value == "Pubblico") { 
		//chkPs � = priv se la categoria non � pubblica
		//se l'utente seleziona il canale pubblico essendo la categoria di tipo particolare
		//la sua scelta comporter� l'eliminazione dei canali dalla categoria
			if (FormMod.chkPN.checked) 	{
				if (confirm("Si se selecciona el canal �P�blico� ser�n eliminados todos los otros canales de uso disponibles.\n�Se desea proceder?")) 
					return true;	
				else
					return false;	
			}
		}
	}
	//-->
	</script>
<!-- FINE BLOCCO SCRIPT	-->
<!--	BLOCCO ASP			-->
<%
'**********************************************************************************
'************************** Impostazione della pagina *****************************
'**********************************************************************************

	Sub ImpostaPag()
		Dim rstCategoria, SQLCateg, n, i, SQLCountTades, rstCountTades
		Dim nID, sDescArea, sTitoloArea, sNomeCat, nProfCat, sAbstract
		dim aFrmIscr
		
		nID = Request.Form("idA")
		
		SQLCateg = "SELECT Percorso, Descrizione, Abstract FROM Categoria WHERE Id_Categoria =" & nID 	
		Set rstCategoria = Server.CreateObject("ADODB.RECORDSET")
'PL-SQL * T-SQL  
SQLCATEG = TransformPLSQLToTSQL (SQLCATEG) 
		rstCategoria.open SQLCateg, CC, 2, 2
		
		If rstCategoria.EOF Then
%>		
			<table border="0" cellspacing="0" cellpadding="0" width="500">
				<tr align="center"> 
					<td class="tbltext1">
						<b>P�gina momentaneamente no dsiponible</b>
			        </td>
				</tr>
			</table>
<%			
			Exit Sub
		Else
			nProfCat = Ubound(split(rstCategoria("Percorso"),"\"))
			sNomeCat = split(rstCategoria("Percorso"),"\")(nProfCat)
			sDescArea = rstCategoria("Descrizione")
			sDescPercorso = rstCategoria("Percorso")
			sTitoloArea = sNomeCat
			sAbstract = rstCategoria("Abstract")
		End If
		rstCategoria.close
		Set rstCategoria = nothing
		
		sCondizione = ""
		sTabella = "RORGA"
		dData	 = Date()
		nOrder   = 0
		Isa		 = 0
		
		aArray = decodTadesToArray(sTabella,dData,sCondizione,nOrder,Isa)
		n = ubound(aArray)
		
%>
		<form Name="FormMod" ACTION="DOC_CnfIscrizione.asp" METHOD="POST" onSubmit="return validateIt(<%=n%>);">
		    <input type="hidden" name="Modo" value="1">
			<input type="hidden" name="n" value="<%=n%>">
			<input type="hidden" name="IdA" value="<%=nID%>">  
			
		    <table border="0" cellspacing="2" cellpadding="1" width="500">
				<tr>
					<td align="left" class="tbltext1" width="150">
						<b>Nombre Categor�a&nbsp;</b> 
					</td>
		            <td class="textblack" colspan="2" align="left"> 
		                <b><%=sTitoloArea%></b>
		            </td>
				</tr>
				<tr> 
		            <td align="left" class="tbltext1" width="150">
						<b>Descripci�n Categor�a*&nbsp; </b>
					</td>
		            <td colspan="2"> 
						<input class="textblack" type="text" name="txtDesc" value="<%=sDescArea%>" size="50" maxlength="95">
		            </td>
				</tr>
				<tr> 
		            <td align="left" class="tbltext1" width="150">
						<b>Resumen categor�a&nbsp; </b>
					</td>
		            <td colspan="2">
						<textarea class="textblack" name="txtAbstr" rows="3" cols="60"><%=sAbstract%></textarea>
					</td>
				</tr>
			</table><br>
		<table border="0" cellspacing="2" cellpadding="1" width="500">
			<tr> 
		        <td colspan="3" height="23" align="center" class="sfondocomm" width="500" colspan="2">
					<b>Usuario de la categor�a&nbsp; </b>
				</td>
			</tr>			

		<%	If isArray(aArray) then
				for ind = 0 to (ubound(aArray) -1)
					sDisabled	 = ""
					sHiddenInput = ""

					sqlFrmIscr = "SELECT ID_CATEGORIA,IDGRUPPO FROM RUOLO_CATEGORIA " &_
					      " WHERE COD_RORGA='" & aArray(ind,ind,ind) & "'" &_
					      " AND ID_CATEGORIA =" & nId
					set rsFrmIscr = Server.CreateObject("adodb.recordset")
					
					'Response.Write sqlFrmIscr
'PL-SQL * T-SQL  
SQLFRMISCR = TransformPLSQLToTSQL (SQLFRMISCR) 
					rsFrmIscr.Open sqlFrmIscr, CC, 2, 2 
					if not rsFrmIscr.EOF then
						aFrmIscr = rsFrmIscr.GetRows() 				
					Else
						redim aFrmIscr(0,0)
						aFrmIscr(0,0) = null
					End If	
					sBotton = ""
					If not IsNull(aFrmIscr(0,0)) Then
						If aFrmIscr(1,0) <> "" Then ' E' solo relativo al rorga
							bRorga = false
						Else
							sDisabled		= " checked disabled "
							bRorga = true		
						End If
						sHiddenInput	= "<input type='hidden' name='HchkRuo" &_
							 ind & "' value='" & aArray(ind,ind,ind) & "'>"
					Else
						bRorga = true
					End If
		%>		
				<tr>
					<td align="right" class="tblsfondo" width="20">
						<input type="checkbox" name="chkRuo<%=ind%>" id="chkRuo<%=ind%>" value="<%=aArray(ind,ind,ind)%>" <%=sDisabled%>>
							<%=sHiddenInput%>
					</td>
					<td align="left" class="tblsfondo" width="480" colspan="2">
						<span class="tbltext1">&nbsp; <b><%=aArray(ind,ind+1,ind)%></b>&nbsp;</span>
					</td>
				
				</tr>
		<%			sSqlGruppo=" SELECT IDGRUPPO,DESGRUPPO,COD_RORGA FROM GRUPPO WHERE COD_RORGA = '" & aArray(ind,ind,ind) & "' ORDER BY DESGRUPPO"
					set rsRorga = Server.CreateObject("ADODB.Recordset")
'PL-SQL * T-SQL  
SSQLGRUPPO = TransformPLSQLToTSQL (SSQLGRUPPO) 
					rsRorga.open sSqlGruppo, CC, 2, 2

					sIDG = ""
					If Not rsRorga.eof Then 
						%>
						<input type="hidden" name="RorgaGru<%=ind%>" value="<%=aArray(ind,ind,ind)%>">									
						<%
						nConta = 0 
						Do While Not rsRorga.eof
							nConta = nConta + 1
							sChecked = ""				
							If not bRorga then
								for i = 0 to UBound(aFrmIscr,2)
									if clng(aFrmIscr(1,i)) = clng(rsRorga("idgruppo")) then
										sChecked = " checked disabled "
									end if
								next 
											
							End If

							nIdG = rsRorga("idgruppo")
							sDesGru = rsRorga("DESGRUPPO")%>						
							<input type="hidden" name="GruPos<%=nIdG%>" value="<%=nConta%>">									
							<tr align="right">
								<td width="10" class="tblsfondo">&nbsp;</td>
								<td align="right" class="tblsfondo" width="10">
									<input <%=sChecked%> type="checkbox" name="chkIdGru<%=ind%>" value="<%=nIdG%>">									
									<% If sChecked <> "" Then%>
										<input type="hidden" name="CheckGru<%=ind%>" value="<%=nIdG%>">									
									<% End If %>
								</td>												
								<td align="left" class="tblsfondo" width="480" nowrap>
									<span class="tbltext1">&nbsp;<%=sDesGru%>&nbsp;</span>
								</td>
							</tr>											
		<%					sIDG = sIDG & nIdG & ","
							rsRorga.MoveNext()
						Loop
		%>
							<input type="hidden" name="Gruppo<%=ind%>" value="<%=left(sIDG,len(sIDG)-1)%>">									
		<%			End If 
					rsFrmIscr.Close 
				Next
				set rsFrmIscr = nothing
				erase aArray
			End If
			
			sSqlRuolo = "SELECT ID_CATEGORIA FROM RUOLO_CATEGORIA WHERE COD_RORGA='XX' AND ID_CATEGORIA = " & nId 
			set RR = server.createobject("ADODB.recordset")
'PL-SQL * T-SQL  
SSQLRUOLO = TransformPLSQLToTSQL (SSQLRUOLO) 
			RR.Open sSqlRuolo, CC ,3
			
			If Not RR.EOF Then
		%>
				<tr>
					<td align="right" class="tblsfondo" width="20">
						<input type="checkbox" name="chkPN" value="XX" checked disabled>
						<input type="hidden" name="chkPS" value="XX">
					</td>
					<td colspan="2" align="left" class="tblsfondo" width="230"><span class="tbltext1">&nbsp; P�blico&nbsp;</span></td>
				</tr>
		<%	Else %>
				<tr>
					<td align="right" class="tblsfondo" width="20">
						<input type="checkbox" name="chkPN" value="XX">
						<input type="hidden" name="chkPS" value="Pubblico">
					</td>
					<td align="left" colspan="2" class="tblsfondo" width="230"><span class="tbltext1">&nbsp; P�blico&nbsp;</span></td>
				</tr>
		<%
			End If
			RR.Close
			set RR = nothing
			sql = ""
		%>
			</table>
			<br>	
			<table border="0" cellspacing="2" cellpadding="1" width="500">
				<tr> 
					<td align="right">
						<input type="image" src="<%=Session("Progetto")%>/images/conferma.gif">
					</td>
		</form>	
				<form method="post" name="Indietro" action="DOC_VisCategorie.asp">
					<input type="hidden" name="cat" value="<%=nID%>">
					<input type="hidden" name="des" value="<%=sDescArea%>">
					<input type="hidden" name="per" value="<%=sDescPercorso%>">
			
					<td align="left"> 
						<a href="<%=Session("progetto")%>" onclick="javascript:Indietro.submit();return false">
						<img src="<%=Session("progetto")%>/images/indietro.gif" border="0"></a>
					</td>
				</form>	
			</tr>
			</table>
		
<%
	End Sub
%>	
<!--------------------------------------------------------------------------------------------------------------------------------------------------------->
<!--	FINE BLOCCO ASP		-->
<!--		MAIN			-->
<%
	Dim strConn, Rs, sql
	Dim sDesc, sOggetto, n, nId
%>	
	<!--#include virtual ="/include/openconn.asp"-->
	<!--#include virtual ="/include/ckProfile.asp"-->
	<!--#include virtual ="/Util/DBUtil.asp"-->	
	<!--#include virtual ="/include/DecCod.asp"-->
	<!--#include virtual ="/include/sysfunction.asp"-->
<%
	If Not ValidateService(Session("IdUtente"),"USE_DOCUMENTALE",cc) Then 
		response.redirect "/util/error_login.asp"
	End If
	
	n = Request.Form("n")
	nID = request.Form("idA")
	sModo = Request.Form("Modo")
%>
<br>
<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
	<tr height="18">
		<td class="sfondomenu" width="67%" height="18"><span class="tbltext0"><b>&nbsp;Habilitaci�n documental-modifica</b></span></td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
		<td valign="middle" align="right" class="tbltext1" width="40%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
	</tr>
	<% if sModo = ""  Then %>
	<tr>
		<td class="sfondocomm" width="57%" colspan="3"><br>Usa el siguiente formato para modificar  los derechos de la categor�a
			<a href="Javascript:Show_Help('/Pgm/help/Documentale/DOC_ModIscrizione')" name onmouseover="javascript:status='' ; return true">
		  <img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0" width="18" height="18" alt="Para mayores informaciones"></a>
      
		</td>
	</tr>
	<% End If %>
	<tr height="17">
		<td class="sfondocomm" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
	</tr>
</table>
<br>
<%	
	If Not (ckProfile(Session("MASK"),2) And ckProfile(Session("MASK"),4)) Then
%>		 
		<table border="0" cellspacing="0" cellpadding="0" width="500">
			<tr align="center"> 
				<td class="tbltext1">
					<b>Usuario no habilitado.</b>
		        </td>
			</tr>
		</table>		
<%	
	Else
		ImpostaPag()
	End if
%>
<!--	FINE BLOCCO MAIN	-->
<!--#include virtual ="/include/closeconn.asp"-->
<!--#include virtual="/strutt_coda2.asp"-->
