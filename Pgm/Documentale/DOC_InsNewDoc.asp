<%@ Language=VBScript %>
<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #include virtual="/strutt_testa2.asp" -->
<!-- #include virtual="/util/portallib.asp" -->
<!-- #include virtual="/include/OpenConn.asp" -->
<!-- #include virtual="/include/ckProfile.asp" -->
<script language="javascript" src="/Include/help.inc"></script>
<%
	If ((Not ValidateService(Session("IdUtente"),"Gestione Notizie",cc)) And (Not ValidateService(Session("IdUtente"),"Sistema Documentale",cc))) Then 
		response.redirect "/util/error_login.asp"
	End if

Server.ScriptTimeout = 540

IdCat = Request.QueryString("Id")
FO = Request.QueryString("FO")
aSepNomeFO = Split(FO,"\")
sNomeFO = aSepNomeFO(Ubound(aSepNomeFO))
FO = replace(FO,"\","\\")

%>
<script>
var sPosFile
<%
	Response.Write "sPosFile =""" & FO & """;"
	Response.Write "sNomeFile =""" & sNomeFO & """;"
	Response.Write "iLunFile =""" & len(sNomeFO) & """;"
%>
	function checkFileName()
	{
		
		if (FotoForm.F1.value == "") {
			//alert("Non � stato selezionato nesssun documento")
			alert("No ha seleccionado ning�n documento")
			return false
		}
		if (iLunFile > 50) {
			//alert ("Attenzione!\nIl file che si vuole inviare non sara' accettato\n" + sNomeFile + "\nil nome del documento ha una lunghezza superiore ai 50 caratteri permessi")
			alert ("Atenci�n!\nEl archivo que se quiere enviar no ser� aceptado\n" + sNomeFile + "\nel nombre del documento tiene un largo superior a los 50 caracteres permitidos")
			return false
		}
		if (FotoForm.F1.value != sPosFile) {
			//alert("Il file che si vuole inviare\n" + sPosFile + "\ne' diverso da quello controllato precedentemente\n" + FotoForm.F1.value)
			//alert("1) " + FotoForm.F1.value + ".- 2) " + sPosFile);
			alert("El archivo que se quiere enviar\n" + sPosFile + "\nes distinto del controlado precedentemente\n" + FotoForm.F1.value)
			return false
		}else{
			return true
		}
	}
</script>
<%
If Request.QueryString("Dir") = "" Then
	Set rstCategorie = Server.CreateObject("ADODB.Recordset")
	SQLCategoria = "SELECT Id_Categoria, Descrizione, Percorso FROM Categoria WHERE Id_Categoria=" & IdCat
'PL-SQL * T-SQL  
SQLCATEGORIA = TransformPLSQLToTSQL (SQLCATEGORIA) 
	rstCategorie.open SQLCategoria, CC, 1, 3
	DirScelta = rstCategorie("Percorso") & "\"
	PathDir = replace(Session("DirInit") & DirScelta, "/", "\")
%>
<br>
<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
	<tr height="18">
		<td class="sfondomenu" width="67%" height="18"><span class="tbltext0"><b>&nbsp;AREA REDACCIONAL - <%=Session("Intestazione")%> </b></span></td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
		<td valign="middle" align="right" class="tbltext1" width="40%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
	</tr>
	<tr>
		<td class="sfondocomm" width="57%" colspan="3">
		<br>El documento seleccionado puede ser enviado, utilice el pulsante <b>Examinar</b> y seleccione nuevamente il file en el computador local. <br>Hacer click en <b>Agregar</b> para confirmar la inserci�n. <br>
		<b>N.B.:</b> El archivo no debe superar los <b>5 Mb</b>.<br>
		<a href="Javascript:Show_Help('/pgm/help/Documentale/DOC_insnewdoc')" name onmouseover="javascript:status='' ; return true">
		<img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0" width="18" height="18" alt="per maggiori informazioni"></a></td>
	</tr>
	<tr height="17">
		<td class="sfondocomm" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
	</tr>
</table>
<table BORDER="0" CELLPADDING="2" CELLSPACING="3" width="500">
	<tr>
		<td width="500" class="tbltext1"><b>Documento seleccionado:<br><%=Request.QueryString("FO")%></b></td>
	</tr>
</table>
<form ENCTYPE="multipart/form-data" onsubmit="return checkFileName()" Method="POST" Action="DOC_InsNewDoc.asp?Dir=<%=PathDir%>&amp;Id=<%=IdCat%>&amp;FO=<%=FO%>" name="FotoForm">
<table BORDER="0" CELLPADDING="2" CELLSPACING="3" width="500">
	<tr>
		<td width="100" class="tbltext1"><b>Documento:</b></td>
		<td width="400"><input size="55" type="FILE" name="F1"></td>
	</tr>
	<tr>
		<td valign="TOP" align="CENTER" colspan="2">
			<a href="javascript:history.back()"><img alt="Indietro" border="0" src="<%=Session("Progetto")%>/images/indietro.gif"></a>
			<input type="image" src="<%=Session("Progetto")%>/images/aggiungi.gif" id="image1" name="image1">
		</td>
	</tr>
</table>
</form>
<%
	rstCategorie.Close
	Set rstCategoria = nothing 
%>
<!-- #include virtual="/include/closeconn.asp" -->
<!-- #include virtual="/strutt_coda2.asp" -->
<%	
Else

	Set upl = Server.CreateObject("Dundas.Upload.2")
		upl.UseUniqueNames = False
		upl.Save Request.QueryString("Dir")

	For Each objFile in upl.Files
	
		SizeFileUp = objFile.size
		
		If SizeFileUp > 5000000 Then	
			objFile.delete
			Response.Write "<TABLE width='510' border='0' cellpadding='2' cellspacing='2'>"
			Response.Write "<tr><td align='center'><img src='/images/sistdoc/Upload/DocumentoNOK.gif' width='400' height='260'></td></tr>"
			Response.Write "<tr><td colspan='2' class='tbltext3' align=center><b>El documento enviado tiene una dimensi�n mayor a la permitida.</b></td></tr>"
			Response.Write "<tr><td align='center'><a href=javascript:history.back()"
			Response.Write "><img alt='Indietro' border='0' src='" & Session("Progetto") & "/images/indietro.gif'></a></td></tr>"
			Response.Write "</TABLE>"
			Response.End
		Else
			aPathdoc = split(objFile.OriginalPath,"\")
			nProfPathdoc = Ubound(aPathdoc)
			NomeDoc = aPathdoc(nProfPathdoc)
		End if

	Next
	
		Set upl = Nothing 
		Response.Redirect "DOC_CreaAttDoc.asp?Id=" & IdCat & "&F=" & NomeDoc
End if
%>
