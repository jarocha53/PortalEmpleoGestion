<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include Virtual="/strutt_testa2.asp"-->
<!--#include virtual="/util/portallib.asp"-->
<!--#include virtual ="/include/openconn.asp"-->
<!--#include virtual ="/include/ckProfile.asp"-->	
<!--#include virtual ="/Util/DBUtil.asp"-->

<script language="javascript" src="/Include/help.inc"></script>

<%
	If Not ValidateService(Session("IdUtente"),"USE_DOCUMENTALE",cc) Then 
		response.redirect "/util/error_login.asp"
	End If
		
	Dim rstCategorie, sql, nInd
	Dim nIdPadre, sDesPadre, sPercorso, sTitoloPagina
		
	sDesPadre=Request.Form("des")
	nIdPadre=Request.Form("cat")
	
	
	If nIdPadre <> 0 Then
	Set rstCat=Server.CreateObject("ADODB.Recordset")
		sSQLCat = "SELECT Descrizione, Percorso,Padre FROM Categoria WHERE Tipo = 'D' and id_categoria=" & nIdPadre
'PL-SQL * T-SQL  
SSQLCAT = TransformPLSQLToTSQL (SSQLCAT) 
		rstCat.open sSQLCat, CC,1
		nPadre= rstCat("Padre")
		If cint(nPadre) <> 0 Then
			Set rstCatPadre=Server.CreateObject("ADODB.Recordset")
				sSQLCat1 = "SELECT Descrizione, Percorso,Padre FROM Categoria WHERE Tipo = 'D' and id_categoria=" & nPadre
'PL-SQL * T-SQL  
SSQLCAT1 = TransformPLSQLToTSQL (SSQLCAT1) 
				rstCatPadre.open sSQLCat1, CC,1
				sPadreDesc= rstCatPadre("Descrizione")
				sPadrePath= rstCatPadre("Percorso")
		End If	
	End If		
	
	If nIdPadre  = "" Then
		nIdPadre = "0"
		sPercorso = " "
		sTitoloPagina = "Selecciona la categoría que se desea habilitar" 
	Else
		sPercorso = Request.Form("per")
		sTitoloPagina = "la categoría seleccionada es <b>" &  sDesPadre & "</b>"
		sSottoCat = "No existen sub-categorías por <b>" &  sDesPadre & "</b>"
	End If

	Set rstCategorie=Server.CreateObject("ADODB.Recordset")
	
	If Session("Ruolo") <> "" Then
		sSQLCat = "SELECT Id_Categoria, Descrizione, Gif_categoria, Abstract, Percorso FROM Categoria " &_
				  " WHERE Id_Categoria IN (SELECT Id_Categoria FROM Ruolo_Categoria " &_
				  " WHERE (Cod_Rorga='" & Session("Ruolo") & "'" &_
				  " AND Tipo = 'D' AND Padre=" & nIdPadre & ") OR (COD_RORGA ='XX' AND IDGRUPPO = " & Session("idgruppo") & "))"			
	Else
		sSQLCat = "SELECT Id_Categoria, Descrizione, Gif_categoria, Abstract, Percorso FROM Categoria " &_
				  " WHERE Tipo = 'D' AND Padre=" & nIdPadre
	End If
'PL-SQL * T-SQL  
SSQLCAT = TransformPLSQLToTSQL (SSQLCAT) 
	rstCategorie.open sSQLCat, CC,1
	
	If ckProfile(SESSION("MASK"),2) Or ckProfile(SESSION("MASK"), 4) Then
		If nIdPadre <> "0" Then 
%>
		<br>
		<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
			<tr height="18">
				<td class="sfondomenu" width="67%" height="18"><span class="tbltext0"><b>&nbsp;Habilitación documental-visualización</b></span></td>
				<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
				<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
			</tr>
			<tr>
				<td class="sfondocomm" width="57%" colspan="3"><br><%=sTitoloPagina%>.........
					<a href="Javascript:Show_Help('/Pgm/help/Documentale/DOC_VisCategorie_1')" name onmouseover="javascript:status='' ; return true">
					<img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0" width="18" height="18" alt="Para mayores informaciones"></a>
				</td>
			</tr>
			<tr height="17">
				<td class="sfondocomm" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
			</tr>
		</table>
		<br>
		<table border="0" width="500" CELLPADDING="0" CELLSPACING="0">
			<tr>
				<td width="500" colspan="2">
					<font face="WingDings" SIZE="2"><%=chr(199)%></font>&nbsp;
					<b><a title="Cliccare per tornare  all'indice delle categorie" class="textred" href="/Pgm/Documentale/DOC_VisCategorie.asp">Vuelve al índice de las categorías</a></b>
				</td>
			</tr>
		</table>
		<br>
		<table border="0" cellspacing="0" cellpadding="0" width="500">
			<form method="post" name="ModAbil<%=nInd%>" action="DOC_ModIscrizione.asp">
				<input type="hidden" name="idA" value="<%=nIdPadre%>">
			</form>
			<tr>
				<td align="center" width="30">
					<img src="/Images/SistDoc/ModDoc.gif" width="23" height="21">
				<td>
				<td align="left">
				<a href="<%=Session("Progetto")%>" onClick="javascript:ModAbil.submit();return false" class="tbltext">
					<b>Modifica la habilitación relativa a la categoría <br><%=sDesPadre%></b></a>
				<td>
			</tr>
		</table>
			<br>
<%		Else %>
		<br>
		<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
			<tr height="18">
				<td class="sfondomenu" width="67%" height="18"><span class="tbltext0"><b>&nbsp;Habilitación documental-visualización</b></span></td>
				<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
				<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
			</tr>
			<tr>
				<td class="sfondocomm" width="57%" colspan="3"><br><%=sTitoloPagina%>.........
					<a href="Javascript:Show_Help('/Pgm/help/Documentale/DOC_VisCategorie')" name onmouseover="javascript:status='' ; return true">
					<img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
				</td>
			</tr>
			<tr height="17">
				<td class="sfondocomm" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
			</tr>
		</table>
		<br>
<%		End If 
	End If
%>
	<table border="0" width="500">
		<tr class="sfondocomm">
			<td colspan="3" width="500" align="left"><b>Listado de  Categorías:</b></td>
		</tr>
		<tr class="sfondocomm">
			<td colspan="2" width="270" align="center"><b>Nombre Carpeta</b></td>
			<td align="center"><b>Descripción</b></td>
		</tr>		
<%		If Not rstCategorie.EOF Then
			nInd = 1
			rstCategorie.MoveFirst
			Do While Not rstCategorie.EOF
			
				iIdCat = rstCategorie("Id_Categoria")
				sDescrCat =  rstCategorie("Descrizione") 
				sAbstrCat = rstCategorie("Abstract")
				sGifCat = rstCategorie("Gif_Categoria")
				sPathCat = rstCategorie("Percorso")
%>
			<form method="post" name="VisAbil<%=nInd%>" action="DOC_VisCategorie.asp">
				<input type="hidden" name="cat" value="<%=iIdCat%>">
				<input type="hidden" name="des" value="<%=sDescrCat%>">
				<input type="hidden" name="per" value="<%=sPathCat%>">
			</form>
			<tr class="tblsfondo"> 
				<td width="55" align="center"> 
<%
				If Not (rstCategorie("Gif_Categoria")= " ") Then %>							
					<img Src="<%=Session("Progetto")%>/images/Categorie/<%=sGifCat%>" width="50" height="50">
<%				Else %>
					<img src="/images/Icons/cartella.gif" width="17" height="17">	
<%				End If %>	
				</td>
				<td width="225">
					<a href="<%=Session("Progetto")%>" onClick="javascript:VisAbil<%=nInd%>.submit();return false" class="tbltext">
					<b><%=sDescrCat%></b></a>
				</td>
				<td width="220" class="tbltext"><%=sAbstrCat%></td>
			</tr>
				<% 
				   nInd = nInd + 1
				   rstCategorie.movenext
			Loop 
		Else  
%>
			<tr align="center" class="tblsfondo"> 
				<td colspan="3" class="tbltext" align="center"><b><%=sSottoCat%><b></td>
			</tr>	
<%		End If %>
	</table>
	<br>
<%	If ckProfile(SESSION("MASK"),2) Or ckProfile(SESSION("MASK"), 4) Then %>
	<table border="0" width="500"> 
		<form method="post" name="InsAbil" action="DOC_InsIscrizione.asp">
				<input type="hidden" name="cat" value="<%=nIdPadre%>">
				<input type="hidden" name="per" value="<%=sPercorso%>">
		</form>
		<tr height="18" class="sfondocomm">
			<td colspan="3"><b>Administración carpetas</b></td>
		</tr>
		<tr class="tblsfondo" height="20">
			<td width="50" align="center"><img src="/images/sistdoc/Bullet1.gif"></td>
			<td width="450" colspan="2">
				<a href="<%=Session("Progetto")%>" onClick="javascript:InsAbil.submit();return false" class="tbltext">
				<b>Crea nueva Categoría</b></a>
			</td>
		</tr>
	</table>
<%	End If 
	If Request.Form ("cat") <> "" and nPadre <> "" Then
%>
	<form method="post" name="Indietro" action="DOC_VisCategorie.asp">
		<input type="hidden" name="cat" value="<%=nPadre%>">
		<input type="hidden" name="des" value="<%=sPadreDesc%>">
		<input type="hidden" name="per" value="<%=sPadrePath%>">
	</form>
	<table border="0" CELLPADDING="0" CELLSPACING="0" width="500" align="center">
		<tr align="center">
			<td>
				<a href="<%=Session("progetto")%>" onclick="javascript:Indietro.submit();return false">
				<img src="<%=Session("progetto")%>/images/indietro.gif" border="0"></a>
			</td>
		</tr>
	</table>
<%	End If %>
<!--#include virtual ="/include/closeconn.asp"-->
<!--#include Virtual="/strutt_coda2.asp"-->
