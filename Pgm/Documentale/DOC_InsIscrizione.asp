<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include virtual="/strutt_testa2.asp"-->
<!--#include virtual ="/util/portallib.asp"-->

<!-- BLOCCO SCRIPT	-->
<script LANGUAGE="JavaScript" src="/include/help.inc"></script>
<script LANGUAGE="JavaScript">
	<!--
	
	function validateIt(n)
	{
		if (FormIscr.txtCanale.value == "" ) {
			FormIscr.txtCanale.focus();
			alert("Inserire il nome del nuovo canale. ");
			return false;
		}
		
		if (FormIscr.txtDescr.value == "" )	{
			FormIscr.txtDescr.focus();
			alert("Inserire la descrizione del nuovo canale. ");
			return false;
		}
		
		if (FormIscr.txtAbstr.value.length > 160) {
			alert("L'abstract non puo' essere lungo piu' di 160 caratteri.\nE' lungo " + FormIscr.txtAbstr.value.length + " caratteri");
			FormIscr.txtAbstr.focus();
			return false 
		}
			
		var num = 0
		for (var i=0 ; i<n; i++) {	
			if ( eval("document.FormIscr.chkRuo" + i + ".checked") )
				num = num + 1;

			if ( eval("document.FormIscr.chkIdGru" + i )) {
				nGrupElem = 0
				if ( eval("document.FormIscr.chkIdGru" + i + ".length")) 
					nGrupElem = eval("document.FormIscr.chkIdGru" + i + ".length")
	
				if (nGrupElem > 1) {
					for ( t=0; t<nGrupElem; t++ ) {
						if (eval("document.FormIscr.chkIdGru" + i + "[" + t + "].checked")) 
							num = num + 1;
					}
				} else {
					if (eval("document.FormIscr.chkIdGru" + i + ".checked"))
						num = num + 1 ;
				}
			}				
		}			
		if ( (num == 0) && (FormIscr.chkPN.checked == "") ) {
			alert("Indicare almeno un gruppo di fruitori del canale inserito")
			return false;
		}			
		if ( ( num > 0 ) && (FormIscr.chkPN.checked != "") ) {
			alert("La scelta del canale Pubblico non permette di effettuare la selezione contemporanea degli altri Fruitori");
			return false;
		}
		return true
	}	
//-->
</script>
<!-- FINE BLOCCO SCRIPT	-->

<!--	BLOCCO ASP			-->
<%

'**********************************************************************************
'************************** Impostazione della pagina *****************************
'**********************************************************************************

Sub ImpostaPag()
	Dim rstCategorie, rstCountTades, rstTades 
	Dim SQLCat, SQLCountTades, SQLTades 
	Dim nCampiUtenza, i 
		
	SQLCat = "SELECT Id_Categoria FROM CATEGORIA" 	
	Set rstCategorie = Server.CreateObject("ADODB.RECORDSET")
'PL-SQL * T-SQL  
SQLCAT = TransformPLSQLToTSQL (SQLCAT) 
	rstCategorie.open SQLCat, CC, 2, 2
	
	If rstCategorie.EOF Then
%>		 
		<table border="0" cellspacing="0" cellpadding="0" width="500">
			<tr align="center"> 
				<td class="tbltext1">
					<b>Pagina momentaneamente non disponibile.</b>
		        </td>
			</tr>
		</table>
<%			
		rstCategorie.close
		Set rstCategorie = Nothing
		SQLCat = ""
		Exit Sub
	End If
		
	'VV	SQLCountTades = "SELECT Count(*) FROM TADES WHERE nome_tabella = 'RORGA' "
'PL-SQL * T-SQL  
SQLCOUNTTADES = TransformPLSQLToTSQL (SQLCOUNTTADES) 
	'	Set rstCountTades = CC.Execute(SQLCountTades)
	'	nCampiUtenza = Cint(rstCountTades.Fields(0)) + 1
	'	
	'	rstCountTades.Close
	'	Set rstCountTades = Nothing
	'	SQLCountTades = ""

	sCondizione = ""
	sTabella = "RORGA"
	dData	 = Date()
	nOrder   = 0
	Isa		 = 0
	aArray = decodTadesToArray(sTabella,dData,sCondizione,nOrder,Isa)
	n = ubound(aArray)

%>
	<form Name="FormIscr" ACTION="DOC_CnfIscrizione.asp" METHOD="POST" onSubmit="return validateIt(<%=n%>)">
		<input type="hidden" name="Action" value="INS">
		<input type="hidden" name="IdPadre" value="<%=clng(Request.Form("cat"))%>">		
		<input type="hidden" name="n" value="<%=n%>"> 
		<table border="0" cellspacing="2" cellpadding="1" width="500">
				<tr>
					<td align="left" class="tbltext1" width="150">
						<b>Nome Categoria*&nbsp;</b> 
					</td>
		            <td class="textblack" colspan="2"> 
		                <input type="text" name="txtCanale" size="50" maxlength="49" class="tbltext">
		            </td>
		        </tr>
				<tr> 
		            <td align="left" class="tbltext1" width="150">
						<b>Descrizione Categoria*&nbsp; </b>
					</td>
		            <td class="textblack" colspan="2"> 
						<input type="text" name="txtDescr" size="50" maxlength="95" class="tbltext">
		            </td>
				</tr>
				<tr> 
		            <td align="left" class="tbltext1" width="150">
						<b>Abstract Categoria&nbsp; </b>
					</td>
		            <td colspan="2">
						<textarea class="textblack" name="txtAbstr" rows="3" cols="60"></textarea>
					</td>
				</tr>
			</table>
			<table border="0" cellspacing="2" cellpadding="1" width="500">
				<tr> 
		            <td colspan="3" height="23" align="center" class="sfondocomm" width="500" colspan="2">
						<b>Fruitori della Categoria&nbsp; </b>
					</td>
				</tr> 

<%				if isArray(aArray) then
				   for i = 0 to (ubound(aArray) - 1) 
			
%>					<tr height="10">
						<td align="right" class="tblsfondo" width="20">
							<input type="checkbox" id="chkRuo<%=i%>" name="chkRuo<%=i%>" value="<%=aArray(i,i,i)%>">	
						</td>
						<td colspan="2" align="left" class="tblsfondo" width="480">	
							<span class="tbltext1">&nbsp; <b><%=aArray(i,i+1,i) %></b>&nbsp;</span>	
						</td>
					</tr>
<%					

		sSql=" SELECT IDGRUPPO,DESGRUPPO,COD_RORGA FROM GRUPPO " &_
				"WHERE COD_RORGA = '" & aArray(i,i,i) & "' ORDER BY DESGRUPPO"
			set rsRorga = Server.CreateObject("ADODB.Recordset")
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
			rsRorga.open sSql, CC, 2, 2

			sIDG = ""
			if not rsRorga.eof then 
				%>
				<input type="hidden" name="RorgaGru<%=i%>" value="<%=aArray(i,i,i)%>">									
				<%
				nConta = 0 
				do while not rsRorga.eof

					nIdG = rsRorga("idgruppo")
					sDesGru = rsRorga("DESGRUPPO")%>						
					<input type="hidden" name="GruPos<%=nIdG%>" value="<%=nConta%>">									
					<tr align="right">
						<td width="10" class="tblsfondo">&nbsp;</td>
						<td width="10" align="right" class="tblsfondo">
							<input <%=sChecked%> type="checkbox" name="chkIdGru<%=i%>" value="<%=nIdG%>">									
						</td>												
						<td align="left" class="tblsfondo" width="480" nowrap>
							<span class="tbltext1">&nbsp;<%=sDesGru%>&nbsp;</span>
						</td>
					</tr>											
<%					sIDG = sIDG & nIdG & ","
					rsRorga.MoveNext()
				loop%>
					<input type="hidden" name="Gruppo<%=i%>" value="<%=left(sIDG,len(sIDG)-1)%>">									

<%
			end if 
			rsRorga.Close 

					next
					erase aArray
				end if
%>										

		<input type="hidden" name="chkPS" value="Pubblico">
		<input type="hidden" name="PercorsoPadre" value="<%=Request.Form("per")%>">

		<tr>
			<td align="right" class="tblsfondo" width="20"><input type="checkbox" name="chkPN" value="XX">	</td>
			<td align="left" colspan="2" class="tblsfondo" width="480"><span class="tbltext1">&nbsp; PUBBLICO&nbsp;</span></td>
		</tr>	
			</table>
			<br>	
			<table border="0" cellspacing="2" cellpadding="1" width="500">
				<tr> 
					<td align="right">
						<input type="image" src="<%=Session("Progetto")%>/images/conferma.gif">
					</td>
					<td>
						<a href="<%=Session("progetto")%>" onclick="javascript:history.back();return false">
						<img src="<%=Session("progetto")%>/images/indietro.gif" border="0"></a>
					</td>
				</tr>
			</table>
			
		</form>
		
<%
End Sub
	

%>
<!--	FINE BLOCCO ASP		-->

<!--		MAIN			-->
	
	<!--#include virtual ="/include/openconn.asp"-->
	<!--#include virtual ="/include/ckProfile.asp"-->
<%
	Dim strConn, Rs, sql
	
	If Not ValidateService(Session("IdUtente"),"USE_DOCUMENTALE",cc) Then 
		response.redirect "/util/error_login.asp"
	End If
	
	sModo = Request.Form("Modo")
%>
<br>
<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
	<tr height="18">
		<td class="sfondomenu" width="67%" height="18"><span class="tbltext0"><b>&nbsp;ABILITAZIONE DOCUMENTALE-INSERIMENTO</b></span></td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
		<td valign="middle" align="right" class="tbltext1" width="40%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
	</tr>
	<tr>
		<td class="sfondocomm" width="57%" colspan="3"><br>Usa il seguente form per creare una nuova categoria con i diritti corrispondenti
		<a href="Javascript:Show_Help('/pgm/help/Documentale/DOC_InsIscrizione')">
		<img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0" width="18" height="18" alt="per maggiori informazioni"></a></td>
	</tr>
	<tr height="17">
		<td class="sfondocomm" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
	</tr>
</table><br>
<%	
	If Not (ckProfile(Session("MASK"),2) And ckProfile(Session("MASK"),4)) Then
%>		 <table border="0" cellspacing="0" cellpadding="0" width="500">
				<tr align="center"> 
					<td class="tbltext1">
						<b>Utente non abilitato.</b>
		            </td>
				</tr>
		</table>		
<%	
	Elseif sModo = ""  Then
			ImpostaPag()
		Else Inserisci()
	End If
%>	
<!--#include virtual = "/include/closeconn.asp"-->

<!--	FINE BLOCCO MAIN	-->

<!--#include virtual="/strutt_coda2.asp"-->
