<%@ Language=VBScript %>
<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #include virtual="/strutt_testa2.asp" -->
<!-- #include virtual="/util/portallib.asp" -->
<!-- #include virtual="/include/OpenConn.asp" -->
<!-- #include virtual="/include/ckProfile.asp" -->
<script language="javascript" src="/Include/help.inc"></script>
<%
If (Not ValidateService(Session("IdUtente"),"Gestione Notizie",cc)) Then 
	response.redirect "/util/error_login.asp"
End If
' Dal mask ricavo il Tipo di categoria ed i permessi
TipoCatScelta = right(Session("mask"),1)
PermGruppo = left(Session("mask"),1)

Session("Tipo") = TipoCatScelta
Session("GruppoDoc") = PermGruppo

If Session("Tipo") = "X" Then
	Response.Redirect "/util/error_login.asp"
End If
' Setto le directory di partenza
PathDirInit = Server.MapPath("\") & Session("Progetto") & "/Testi/SistDoc/Notizie/"
DirPath = Session("Progetto") 

	TipoVista = mid(Session("mask"),2,1)
	
	Session("Vista") = TipoVista
	Session("DirInit") = PathDirInit 
	Session("DirInitEdit") = DirPath & "/Testi/SistDoc/Notizie/"
	Session("Intestazione") = "News"

Dim DirScelta, Posbarra, Pathname
Dim fso, f

Set rstTipoCat = Server.CreateObject("ADODB.Recordset")
SQLTipoCat =" SELECT Id_Categoria, Descrizione, Percorso, Abstract, Gif_categoria, Padre, Tipo FROM Categoria " &_ 
			" WHERE Padre=0 And Tipo='" & TipoCatScelta & "' ORDER BY Descrizione"
'PL-SQL * T-SQL  
SQLTIPOCAT = TransformPLSQLToTSQL (SQLTIPOCAT) 
rstTipoCat.open SQLTipoCat, CC, 1, 3

Session("NomeUtente") = Session("Nome") & "&nbsp;" & Session("Cognome")
%>
<br>
<table BORDER="0" WIDTH="500" CELLPADDING="0" CELLSPACING="0">
	<tr height="18">
		<td class="sfondomenu" width="67%" height="18"><span class="tbltext0"><b>&nbsp;AREA REDAZIONALE - <%=Session("Intestazione")%> </b></span></td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
		<td valign="middle" align="right" class="tbltext1" width="40%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
	</tr>
	<tr>
		<td class="sfondocomm" width="57%" colspan="3"><br>Per inserire, cancellare o modificare una News in una categoria gi� esistente selezionare la categoria della News dall'<b>Elenco Categorie</b>. <br>
		Per aggiungere una nuova categoria di News premere su <b>Crea Nuova Categoria</b>.<br>
		<a href="Javascript:Show_Help('/Pgm/help/Documentale/DOC_InitNews')" name onmouseover="javascript:status='' ; return true">
		<img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0" width="18" height="18" alt="per maggiori informazioni"></a></td>
	</tr>
	<tr height="17">
		<td class="sfondocomm" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
	</tr>
</table>
<br>


<!-- <table BORDER="0" WIDTH="500"> -->
<table BORDER="0" WIDTH="500" CELLPADDING="1" CELLSPACING="1">

	<tr class="sfondocomm" height="18"><td colspan="4" width="500"><b>Elenco Categorie:</b></td></tr>	
	<tr height="18" class="sfondocomm">
		<td width="160" colspan="2" height="18" align="center"><b>Nome</b></td>
		<td width="260" height="18" align="center"><b>Descrizione</b></td>
		<td width="80" height="18" align="center"><b>Notizie da Pubblicare</b></td>
	</tr>
  <%
  If rstTipoCat.EOF Then
	
	Response.Write "<tr class='tblsfondo' height='18'><td width=50 colspan=2 align=center><img src=/Images/SistDoc/Bullet1.gif></td>"
	Response.Write "<td class='tbltext'><b>Nessuna categoria presente</b></td><td>&nbsp;</td></tr>" 
		
  Else
    iInd = 0
    Set rstNotizie = Server.CreateObject("ADODB.Recordset")

	 Do While Not rstTipoCat.EOF
		iInd = iInd + 1
		SQLnotizie = "SELECT Count(Id_Notizie) As NUM FROM Notizie " &_
					 " WHERE Id_Categoria=" & rstTipoCat.Fields("Id_Categoria") & " AND Fl_Pubblicato = 0" 
'PL-SQL * T-SQL  
SQLNOTIZIE = TransformPLSQLToTSQL (SQLNOTIZIE) 
		rstNotizie.open SQLnotizie, CC, 1, 3
		nNonPubblicate= rstNotizie("NUM")
		rstNotizie.Close
		
		If isnull(rstTipoCat("Gif_categoria")) Then
			Response.Write "<tr height='18' class='tblsfondo'><td width='50' align='center'><img src='/Images/SistDoc/Bullet1.gif'>"
		Else
			Response.Write "<tr class='tblsfondo'><td width='50' align='center'><img width='50' height='50' src='" & Session("Progetto") & "/Images/Categorie/" & rstTipoCat("Gif_categoria") & "'>&nbsp;"
		End If
			Response.Write "</td><FORM Name='vinews" & iInd & "' Method='post' Action='DOC_VisNews.asp'>"
			Response.Write "<input type='hidden' name='Tema' value='" & rstTipoCat("Id_Categoria") & "'>" 
			Response.Write "<td width='160'><a class='tbltext' href='" & Session("Progetto") & "' onClick='Javascript:vinews" & iInd & ".submit();return false'><b>" & rstTipoCat("Descrizione") & "</b></a></td></FORM>"
			Response.Write "<td width='260' class='tbltext'>" & rstTipoCat("Abstract") & "&nbsp;</TD>"
			Response.Write "<td width='80' class='tbltext' align=center>" 
		if clng(nNonPubblicate) <> 0 then
			Response.Write nNonPubblicate 
		else
			Response.Write "&nbsp;"
		end if
		Response.Write "</TD></TR>"
		rstTipoCat.MoveNext
	Loop
	set rstNotizie = nothing
  End If
  %>
</table>
  <br>
<table border="0" width="500" CELLPADDING="1" CELLSPACING="1">
	<tr height="18" class="sfondocomm">
		<td colspan="2"><b>Amministazione Cartelle</b></td>
	</tr>
	
	<tr class="tblsfondo" height="20">
		<td width="50" align="center"><img src="/images/sistdoc/Bullet1.gif"></td>
		<td width="450"><a class="tbltext" href="DOC_CreaCartella.asp"><b>Crea Nuova Categoria</b></a></td>
  </tr>
  <!--  <TR>    <TD class="tbllabel1"><STRONG>Ricerca Documenti</STRONG></TD>  </TR>  <TR>	<TD class="tblsfondo"><img src="/Imagesg/Bullet1.gif"><A href="ricerca.asp">Ricerca Normale</A></TD>  </TR>  <TR class="tblsfondo">	<TD><img src="/Images/Bullet1.gif"><A href="riccontesto.asp">Ricerca nel contesto</A></TD>  </TR>  -->
</table>
<%	
rstTipoCat.Close
Set rstTipoCat = nothing	
%>
<!-- #include virtual="/include/closeconn.asp" -->
<!-- #include virtual="/strutt_coda2.asp" -->

