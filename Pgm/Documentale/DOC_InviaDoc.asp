<%@ Language=VBScript %>
<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%Response.Buffer=True%>
<!-- #include virtual="/strutt_testa2.asp" -->
<!-- #include virtual="/util/portallib.asp" -->
<!-- #include virtual="/include/OpenConn.asp" -->
<script LANGUAGE="Javascript" src="/Include/ControlString.inc"></script>
<script language="javascript" src="/Include/help.inc"></script>
<%
	If ((Not ValidateService(Session("IdUtente"),"Gestione Notizie",cc)) And (Not ValidateService(Session("IdUtente"),"Sistema Documentale",cc))) Then 
		Response.Redirect "/util/error_login.asp"
	End if
%>
<script LANGUAGE="JavaScript">
		function ControllaInvio(frm)
	{
		var InputForm = frm.FILE1.value
		var aNomefile;
		
		aNomefile = InputForm.split("\\")
		
		if (!ValidateNameUploadFileIsValid(InputForm)) {
			return false
		}
	
		if (InputForm == "") {
			alert("No ha sido seleccionado ning�n documento!");
			return false
		}
		
		if (aNomefile[aNomefile.length-1].charAt(aNomefile[aNomefile.length-1].length-5) == " ") {
			alert("El documento que se quiere reenviar tiene un nombre incorrecto.\nEliminar los espacios ingresados antes del punto final");
			return false
		}

		frm.submit
	
	}
</script>
<%	
	IdCat = Request.Form("Id")
	Scelta = Request.Form("Sc")
	
If Scelta = "" Then
	Set rstCategorie = Server.CreateObject("ADODB.Recordset")
	SQLCategoria = "SELECT Id_Categoria, Descrizione, Percorso FROM Categoria WHERE Id_Categoria=" & IdCat
'PL-SQL * T-SQL  
SQLCATEGORIA = TransformPLSQLToTSQL (SQLCATEGORIA) 
	rstCategorie.open SQLCategoria, CC, 1, 3
	DirScelta = rstCategorie("Percorso") & "\"
	PathDir = replace(Session("DirInit") & DirScelta, "/", "\")
%>
<br>
<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
	<tr height="18">
		<td class="sfondomenu" width="67%" height="18"><span class="tbltext0"><b>&nbsp;AREA REDACCIONAL - <%=Session("Intestazione")%> </b></span></td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
		<td valign="middle" align="right" class="tbltext1" width="40%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
	</tr>
	<tr>
		<td class="sfondocomm" width="57%" colspan="3">
		<br>Para ingresar un documento utilizar el pulsante <b>Busca</b> y selecciona el file en el propio computador.<br>
		Antes de efectuar el ingreso es necesario controlar que el documento no est� ya presente al interior de la categor�a seleccionada, despu�s de haber seleccionado el documento presionar <b>Env�a</b> para efectuar el control. 
		<a href="Javascript:Show_Help('/pgm/help/Documentale/DOC_inviadoc')" name onmouseover="javascript:status='' ; return true">
		<img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0" width="18" height="18" alt="Para mayores informaciones"></a>
		</td>
	</tr>
	<tr height="17">
		<td class="sfondocomm" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
	</tr>
</table>

<form Method="post" Action="DOC_InviaDoc.asp" name="DocInvioForm" onsubmit="return ControllaInvio(this)">
<input type="hidden" name="Dir" value="<%=PathDir%>">
<input type="hidden" name="Id" value="<%=IdCat%>">
<input type="hidden" name="Sc" value="1">
<table BORDER="0" CELLPADDING="2" CELLSPACING="3" width="500">
	<tr>
		<td width="100" class="tbltext1"><b>Documento:</b></td>
		<td width="400"><input size="55" TYPE="FILE" NAME="FILE1"></td>
	</tr>
	<tr>
		<td valign="TOP" align="CENTER" colspan="2">
			<input type="image" src="<%=Session("Progetto")%>/images/conferma.gif" name="Conferma">
		</td>
	</tr>
</table>
</form>

<% 
	rstCategorie.Close
	Set rstCategorie = nothing
Else 
' Ricavo la posizione del file.
	sPosFileOrig = Request.Form("FILE1")
	STarget =  Request.Form("Dir")
	
	nPosBarraFinaleDoc = Instrrev (sPosFileOrig, "\")
	nLenTot = Len (sPosFileOrig)
	nDiff = nLenTot - nPosBarraFinaleDoc
	sNomeFile =  right(sPosFileOrig,nDiff)

	sNomeServer = Request.ServerVariables("Server_name")
	sStrMod = "http://" & sNomeServer
	
	sPosFile = replace(STarget,sStrMod,server.MapPath("\")) & sNomeFile
	aSepNomeFile = split(sNomeFile,".")
	sExtDoc = aSepNomeFile(Ubound(aSepNomeFile))
	
		Response.Write "<FORM Name='TipoDoc' Method='post' Action='DOC_InviaDoc.asp'>"
        Response.Write "<input type='hidden' name='Id' value='" & IdCat & "'>" 
		Response.Write "</FORM>"

	Set rstTipoDoc = Server.CreateObject("ADODB.Recordset")
		SQLTipoDoc= "SELECT Id_TipoDoc FROM TipoDoc WHERE Tipo = '" & Lcase(sExtDoc) & "'"
'PL-SQL * T-SQL  
SQLTIPODOC = TransformPLSQLToTSQL (SQLTIPODOC) 
		rstTipoDoc.open SQLTipoDoc, CC, 1, 3

	If rstTipoDoc.EOF Then
		Response.Write "<TABLE width='510' border='0' cellpadding='2' cellspacing='2'>"
		Response.Write "<tr><td align='center'><img src='/images/sistdoc/Upload/DocumentoNOK.gif' width='400' height='260'></td></tr>"
		Response.Write "<tr><td colspan='2' class=tbltext1 align='center'><B>El documento &nbsp;<br><i class='textredb'>" & sNomeFile
		Response.Write "&nbsp;</i><br>tiene un formato no permitido.</B></td></tr>"
		Response.Write "<tr><td align='center'><a class='tbltext' href='" & Session("Progetto") & "' onClick='Javascript:TipoDoc.submit();return false'>"
		Response.Write "<img alt='Atr�s' border='0' src=" & Session("Progetto") & "/images/indietro.gif></a></td></tr>"
		Response.Write "</TABLE>"
		Response.End
	End If
	
	If Len(sPosFile) < 260 Then
		If (Len(sNomeFile)-(Len(aSepNomeFile(Ubound(aSepNomeFile))) + 1)) <= 50 Then
			Set fso = CreateObject("Scripting.FileSystemObject")
			If fso.FileExists(sPosFile) Then
			    Response.Write "<TABLE width=510 border='0' cellpadding='2' cellspacing='2'>"
				Response.Write "<tr><td align='center'><img src='/images/sistdoc/Upload/DocumentoNOK.gif' width='400' height='260'></td></tr>"
				Response.Write "<tr><td colspan='2' class='tbltext1' align='center'><B>Existe ya un documento con nombre <br><i class='textreda'>" & sNomeFile
				Response.Write "&nbsp;</i></Font></B></td></tr>"
				Response.Write "<tr><td align='center'><a class='tbltext' href='" & Session("Progetto") & "' onClick='Javascript:TipoDoc.submit();return false'>"
				Response.Write "<img alt='Atr�s' border='0' src=" & Session("Progetto") & "/images/indietro.gif></a></td></tr>"
				Response.Write "</TABLE>"
				Response.End
			Else
				Response.Redirect "DOC_InsNewDoc.asp?Id=" & IdCat & "&FO=" & sPosFileOrig
			End If
		Else
				Response.Write "<TABLE width='510' border='0' cellpadding='2' cellspacing='2'>"
				Response.Write "<tr><td align='center'><img src='/images/sistdoc/Upload/DocumentoNOK.gif' width='400' height='260'></td></tr>"
				Response.Write "<tr><td colspan=2 class=tbltext1 align=center><B>El documento &nbsp;<br><i class='textredb'>" & sNomeFile
				Response.Write "&nbsp;</i><br>tiene un t�tulo m�s largo de los 50 caracteres permitidos.</B></td></tr>"
				Response.Write "<tr><td align='center'><a class='tbltext' href='" & Session("Progetto") & "' onClick='Javascript:TipoDoc.submit();return false'>"
				Response.Write "<img alt='Atr�s' border='0' src=" & Session("Progetto") & "/images/indietro.gif></a></td></tr>"
				Response.Write "</TABLE>"
		End If
	Else
		Response.Write "<TABLE width='510' border='0' cellpadding='2' cellspacing='2'>"
		Response.Write "<tr><td align='center'><img src='/images/sistdoc/Upload/DocumentoNOK.gif' width='400' height='260'></td></tr>"
		Response.Write "<tr><td align=center class=tbltext3><b>El procedimiento completo del documento <br><i class=textredb>" & sNomeFile
		Response.Write "</i><br>supera el l�mite de caracteres aceptados por el sistema operativo.</b></td></tr>"
		Response.Write "<tr><td align='center'><a class='tbltext' href='" & Session("Progetto") & "' onClick='Javascript:TipoDoc.submit();return false'>"
		Response.Write "<img alt='Atr�s' border='0' src=" & Session("Progetto") & "/images/indietro.gif></a></td></tr>"
		Response.Write "</TABLE>"
	End If
End if %>		
<!-- #include virtual="/include/closeconn.asp" -->
<!-- #include virtual="/strutt_coda2.asp" -->
