<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<center>
<table border="0" CELLPADDING="2" CELLSPACING="2" width="745">		
	<tr>
		<td align="center" class="SFONDOCOMM" nowrap width="200">
			<b>Apellido y Nombre</b>
		</td>
		<td align="center" class="SFONDOCOMM" width="130">
			<b>Sexo</b>
		</td>
		<td align="center" class="SFONDOCOMM" width="160">
			<b>Fecha de Nacimiento</b>
		</td>
		<td align="center" class="SFONDOCOMM" nowrap width="160">
			<b>Grado de Parentezco</b>
		</td>
		<td align="center" class="SFONDOCOMM" nowrap width="90">
			<b>A cargo</b>
		</td>
	</tr>

<% 
sSQL = "SELECT COGNOME, NOME, SESSO," & _
	" DT_NASC, COD_GRPAR, FL_CARICO" & _
	" FROM PERS_FAMILIARI" & _
	" WHERE ID_PERSONA=" & sUtente & _
	" ORDER BY DT_NASC, COGNOME, NOME"
 
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
Set rsFamiliari = CC.Execute(sSQL)
	
Do while not rsFamiliari.eof 	
  sCognome=rsFamiliari("COGNOME")
  sNome=rsFamiliari("NOME")
  sSesso=rsFamiliari("SESSO")
  DataNasc=rsFamiliari("DT_NASC")
  nCodParent=rsFamiliari("COD_GRPAR")
  flCarico=rsFamiliari("FL_CARICO")
			
	Select case sSesso
		case "M"
			sSesso = "MASCULINO"
		case "F"
			sSesso = "FEMENINO"
	End Select
	
	Select case flCarico
		case "S"
			flCarico = "si"
		case "N"
			flCarico = ""
	End Select
		
	DescrParent = DecCodVal("GRPAR",0,"", nCodParent,"")
	
%>
	<tr class="tblsfondo"> 
		<td align="left">
			<span class="tbldett">
			<b><%=sCognome%>&nbsp;<%=sNome%></b>
			</span>
		</td>
		<td align="left">
			<span class="tbldett">
			<%=sSesso%>
			</span>
		</td>		
		<td>
			<span class="tbldett">
			<%=DataNasc%>
			</span>
		</td>				
		<td>
			<span class="tbldett">
			<%=DescrParent%>
			</span>
		</td>
		<td align="center">
			<span class="tbldett">
			<%=flCarico%>
			</span>
		</td>
	</tr>
<%
rsFamiliari.movenext	
Loop
rsFamiliari.Close()
Set rsFamiliari = Nothing
%>
</table>
</center>
