<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%


'sSQL = "SELECT NOME,COGNOME,DT_NASC,COM_NASC," & _
'		"PRV_NASC,STAT_NASC," & _
'		"COD_FISC,STAT_CIT," & _
'		"IND_RES,FRAZIONE_RES," & _
'		"COM_RES,PRV_RES," & _
'		"CAP_RES,NUM_TEL,NUM_TEL_DOM,NUM_TEL_DOM_OBS," & _
'		"STAT_CIV,STAT_CIT2,SESSO," & _
'		"E_MAIL,AUT_TRAT_DATI,AUT_INCROCIO FROM PERSONA a, dichiaraz b" & _
'		" WHERE a.ID_PERSONA=" & sUtente &_
'		" and a.id_persona=b.id_persona"
		

sSQL = "SELECT NOME,COGNOME,DT_NASC,COM_NASC," & _
		"PRV_NASC,STAT_NASC," & _
		"COD_FISC,STAT_CIT," & _
		"IND_RES,FRAZIONE_RES," & _
		"COM_RES,PRV_RES," & _
		"CAP_RES,NUM_TEL,NUM_TEL_DOM,NUM_TEL_DOM_OBS," & _
		"STAT_CIV,STAT_CIT2,SESSO," & _
		"E_MAIL FROM PERSONA a " & _
		" WHERE a.ID_PERSONA=" & sUtente 
			


'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
set rsAnagrafica = CC.Execute(sSQL)
if not rsAnagrafica.EOF then
	NOME=rsAnagrafica("NOME")
	COGNOME=rsAnagrafica("COGNOME")
	DT_NASC=rsAnagrafica("DT_NASC")
	COM_NASC=rsAnagrafica("COM_NASC")
	PRV_NASC=rsAnagrafica("PRV_NASC")
	STAT_NASC=rsAnagrafica("STAT_NASC")
	CODIGO=rsAnagrafica("COD_FISC")
	STAT_CIT=rsAnagrafica("STAT_CIT")
	STAT_CIT2=rsAnagrafica("STAT_CIT2")
	STAT_CIV = rsAnagrafica("STAT_CIV")
	IND_RES=rsAnagrafica("IND_RES")
	FRAZIONE_RES=rsAnagrafica("FRAZIONE_RES")
	COM_RES=rsAnagrafica("COM_RES")
	PRV_RES=rsAnagrafica("PRV_RES")
	CAP_RES=rsAnagrafica("CAP_RES")
	NUM_TEL=rsAnagrafica("NUM_TEL")
	E_MAIL=rsAnagrafica("E_MAIL")
	'AUT_TRAT_DATI=rsAnagrafica("AUT_TRAT_DATI")
	'AUT_INCROCIO=rsAnagrafica("AUT_INCROCIO")
	TEL_MS = rsAnagrafica("NUM_TEL_DOM")
	OBS_TEL_MS = rsAnagrafica("NUM_TEL_DOM_OBS")
	
	SELECT CASE rsAnagrafica("SESSO")
		CASE "M"
			SESSO="MASCULINO"
		CASE "F"
			SESSO="FEMENINO"
	END SELECT

end if
rsAnagrafica.Close()
set rsAnagrafica = Nothing

if COM_NASC <> "" then
	descComNas=DescrComune(COM_NASC)
end if

'Response.Write ("Pr. *" & PRV_NASC & "*")

if PRV_NASC <> "" then
	descPrvNas = "SI"
else
	descPrvNas = "NO"
end if

'Response.Write ("Pr.2 *" & descPrvNas & "*")

if STAT_NASC <> "" then			
	descStatNas = DescrComune(STAT_NASC)
end if

if COM_RES <> "" then			
	descComRes = DescrComune(COM_RES) 
end if
if PRV_RES <> "" then
	descProvinciaResi=DescrComune(PRV_RES) 
else
	descProvinciaResi=""
end if
if PRV_RES <> "" then
	descPrvRes = "SI"
else
	descPrvRes = "NO"
end if

if STAT_CIT <> "" then
	if STAT_CIT="IT" then
		descNazione="ARGENTINA"
	else 
		descNazione = DecCodVal("STATO",0,"",STAT_CIT,"")
	end if
end if

if STAT_CIT2 <> "" then
	if STAT_CIT2= "IT" then
		descNazione2="ARGENTINA"
	else 
		descNazione2 = DecCodVal("STATO",0,"",STAT_CIT2,"")
	end if
end if

if STAT_CIV <> "" then
	descCivil = DecCodVal("STCIV",0,"",STAT_CIV,"")
end if

if not isnull(NUM_TEL) then
	if trim(cstr(NUM_TEL)) = "0" then
		NUM_TEL = ""
	end if
end if 

if not isnull(TEL_MS) then
	if trim(cstr(TEL_MS)) = "0" then
		TEL_MS = ""
	end if
end if 

%>

<table border="0" CELLPADDING="4" CELLSPACING="2" width="745">
	<tr>
			<td align="left" class="tbltext1" width="100" >
				<b>Apellido:</b>
			</td>	
			<td align="left" class="textblack" width="290">
				<b><%=COGNOME%></b>
			</td>

			<td align="left" class="tbltext1" width="100" >
				<b>Nombre:</b>
			</td>	
			<td align="left" class="textblack" width="290">
				<b><%=NOME%></b>
			</td>
	
	</tr>
	<tr>
		
		<!--Inizio Ambra controllo codice fiscale gennaio 2002 -->
		<%  		
		if trim(CODIGO)<> ""  then 		
		%> 	
			
			<td align="left" class="tbltext1" width="100" nowrap>
				<b>Documento:</b>
			</td>	
			<td align="left" class="textblack" width="290">
				<b><%=CODIGO%></b>
			</td>
			<td align="left" class="tbltext1" width="80">
				<b>Sexo:</b>
			</td>				
			<td width="275">
				<b class="textblack"><%=SESSO%></b>
			</td>
		<%else %>				       	
			<td align="left" class="tbltext1" width="100">
				<b>Sexo:</b>
			</td>
			<td align="LEFT" class="textblack" colspan="3">
			   <b><%=SESSO%></b>
			</td>
		<%end if%>
	
	</tr>
	 
	<tr>
		
		<td align="left" class="tbltext1">
			<b>Fecha de Nacimiento:</b>
		</td>
		<td align="left" class="textblack">
			<b><%=ConvDateToString(DT_NASC)%></b>
		</td>
		
		<td align="left" class="tbltext1">
			<b>Lugar de Nacimiento:</b>
		</td>
		
			<%if descComNas <> "" then%>
				<td>
					<b class="textblack"><%=ucase(descComNas)%></b>
				</td>
			<%else%>
				<td>
				<%'''28/08/2003%>
					<!--b class="textblack"><%'''=ucase(descStatNas)%></b-->
					<b class="textblack"><%=ucase(descStatNas)%>&nbsp;
						<% if descPrvNas = "SI" then %>
							(<%=ucase(PRV_NASC)%>)</b>
						<%end if%>
					<%'''FINE 28/08/2003%>
				</td>
			<%end if%>		
	</tr>
	<tr>
		<td align="left" class="tbltext1">
			<b>Ciudadan�a:</b>
		</td>	
		<td align="left" class="textblack">
			<b><%=ucase(descNazione)%></b>
		</td>
		<!--inizio Ambra controllo su seconda cittadinanza -->
		<%if trim(descNazione2)= ""  then %>
			<td align="left" class="tbltext1" colspan="2">
				<b class="textblack">&nbsp;</b>
			</td>
		<%else%>
			<tr>
			<td align="left" class="tbltext1" >
				<b>Segunda Ciudadan�a:</b>
			</td>			
			<td colspan="3">
				<b class="textblack" ><%=ucase(descNazione2)%></b>
			</td>
			</tr>
		<%end if%>
	</tr>
	
		<%
'		RAOUL: Commentata IF xch� non fa visualizzare residenza all'estero.
'		If IND_RES <> "" then 
		%>
	
	<tr>	
	<!--Inizio Ambra  inserimento tutto blocco sottostante-->	
	
		
<!--Fine Ambra  -->	

<%' if CODIGO <> ""  then%>
	 <!--td align="left" class="tbltext1"-->
<% 'ELSE	 
	Sql = "select a.COD_DOCST, a.COD_FISC from PERS_TIPO_DOC a,persona b" &_
		" where a.id_persona =" & sUtente & " and b.id_persona = a.id_persona"
		
				 
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
'Response.Write SQL
				 set rsAnag = CC.Execute(SQL)
				 if not rsAnag.eof then
					codDoc=rsAnag("COD_DOCST")
					numDoc=rsAnag("COD_FISC")      
				      
					descDoc = DecCodVal("DOCST",0,"",codDoc,"")
				 else
					codDoc=""
					numDoc=""
					descDoc=""
				 end if      
					       %>
		<tr>
				<td align="left" class="tbltext1" >
					<b>Tipo Documento:</b>
				</td>	
				<td align="left" class="textblack">
					<b><%=descDoc%></b>
				</td>	       
				<td align="left" class="tbltext1" >
					<b>N�mero:</b></td>				
				<td>
					<b class="textblack"><%=numDoc%></b>
				</td>	       		       
		</tr>
<%rsAnag.close
  set rsang = nothing		
		
'END IF%>
		<tr>

	
<td align="left" class="tbltext1">
			<b>Estado Civil:</b>
		</td>
		<td align="left">
			<b class="textblack"><%=descCivil%></b>
</td>	
		
<td align="left" class="tbltext1">
			<b>Correo El�ctronico:</b>
		</td>
		<td align="left">
			<b class="textblack"><%=(E_MAIL)%></b>
</td>	
		
	</tr>
	
	
</table>

<table>

</table>



