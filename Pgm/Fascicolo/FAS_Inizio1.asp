<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->

<script>
function PrimaDellaStampa(Progetto,Oggi,Titolo,Autorizzazione)
{
	//document.frmPulsanti.style.visibility='hidden';
	//document.frmInizioPrima.style.visibility='hidden'
	lblPulsanti.innerHTML = ""
	lblPrima.innerHTML = ""
	
	//alert(Autorizzazione)
	var Tab
	
	Tab = "<table border=0 width=745 cellspacing=0 cellpadding=0 align=center>"
	Tab = Tab + "<tr>"
	Tab = Tab + "	<td colspan=2 width=100% background=" + Progetto + "/images/titoli/strumenti1g.gif height=70 valign=bottom align=left>"
	Tab = Tab + "		<table border=0 background width=95% height=30 cellspacing=0 cellpadding=0>"
	Tab = Tab + "			<tr>"
	Tab = Tab + "				<td width=85% valign=bottom align=right>"
	Tab = Tab + "					<b CLASS=tbltext1a>" + Titolo + "</b>"
	Tab = Tab + "				</td>"
	Tab = Tab + "			</tr>"
	Tab = Tab + "		</table>"
	Tab = Tab + "	</td>"
	Tab = Tab + "</tr>"
	Tab = Tab + "<tr>"
	Tab = Tab + "	<td align=right width=100%>"
	Tab = Tab + "		<b CLASS=tbltext1>Situación al: </b>&nbsp;"
	Tab = Tab + "		<b CLASS=textblack>" + Oggi + "</b>"
	Tab = Tab + "	</td>"
	Tab = Tab + "	<td>&nbsp;</td>"
	Tab = Tab + "</tr>"
	Tab = Tab + "</table>"
	
	lblDopo.innerHTML = Tab

	if (Autorizzazione != "No") {
		var Aut
	
		Aut = "<table width='50%' cellspacing='2' cellpadding='1' border='0' align='center'>"
		Aut = Aut + "<tr align='left' height='50'>"
		Aut = Aut + "	<td class='textblack'>"
		Aut = Aut + "		<b>Autorización al manejo de los datos personales.</b>"
		Aut = Aut + "		A los sentidos de la ley 675/96, expresa el consentimiento al trato y a la comunicación "
		Aut = Aut + "		de mis datos personales de parte de " + Autorizzazione + " en los límites que de a la misma."
		Aut = Aut + "		<br><br>"
		Aut = Aut + "		Firma para la autorización"
		Aut = Aut + "		<br><br>"
		Aut = Aut + "		_________________________"
		Aut = Aut + "	</td>"
		Aut = Aut + "</tr>"	
		Aut = Aut + "</table>"
	
		lblAutorizzazioni.innerHTML = Aut
	}
}
function DopoDellaStampa(Progetto,Oggi,Titolo,WTitolo,WResto,Help,Commento)
{
	
	//document.frmPulsanti.style.visibility='visible';
	lblDopo.innerHTML = ""
	
	var Tab
	
	Tab = Tab = "<table border='0' CELLPADDING='0' CELLSPACING='0' width='98%'>"
	Tab = Tab +	"<tr height='17'>"
	Tab = Tab +	"	<td class='sfondomenu' width='45%' height='18'><span class='tbltext0'>"
	Tab = Tab +	"		<b>&nbsp;" + Titolo + "</b>"
	Tab = Tab +	"	</td>"
	Tab = Tab +	"	<td width='" + WTitolo + "' background=" + Progetto + "/images/tondo_linguetta.gif>"
	Tab = Tab +	"	</td>"
	Tab = Tab +	"	<td valign='middle' align='right' width='" + WResto + "' class='tbltext1' background='" + Progetto + "/images/sfondo_linguetta.gif'></td>"
	Tab = Tab +	"</tr>"
	Tab = Tab +	"<tr>"
	Tab = Tab +	"	<td class='sfondocommaz' width='100%' colspan='3'>"
	Tab = Tab +	"		" + Commento + "<br>"
	Tab = Tab +	"		Presionar <b>Cerrar</b> para salir."
	Tab = Tab +	"		Presionar <b>Imprimir</b> para imprimir el documento."
	Tab = Tab +	"	</td>"
	Tab = Tab +	"</tr>"
	
	
	if (Help != "")
	{
		Tab = Tab +	"<tr>"	
		Tab = Tab +	"	<td class='sfondocommaz' colspan='3' align=right width='100%'>"
		Tab = Tab +	"		<input type=image align=right src='/images/help.gif' border='0' onclick=Show_Help('" + Help + "') id=image1 name=image1>"
		Tab = Tab +	"	</td>"
		Tab = Tab +	"</tr>"
	}
	
	Tab = Tab +	"<tr height=2>"
	Tab = Tab +	"	<td class=sfondocommaz width='100%' colspan='3' background='" + Progetto + "/images/separazione.gif'>"
	Tab = Tab +	"	</td>"
	Tab = Tab +	"</tr>"
	Tab = Tab +	"<tr>"
	Tab = Tab +	"	<td colspan=3>" 
	Tab = Tab +	"		&nbsp;"
	Tab = Tab +	"	</td>"
	Tab = Tab +	"</tr>"
	Tab = Tab +	"<tr>"
	Tab = Tab +	"	<td align=right width=100% colspan=3>" 
	Tab = Tab +	"		<b CLASS=tbltext1>Situación al:</b><br>"
	Tab = Tab +	"		<b CLASS=textblack>" + Oggi + "</b>"
	Tab = Tab +	"	</td>"
	Tab = Tab +	"</tr>"
	Tab = Tab +	"</table>"
	
	lblPrima.innerHTML = Tab
	
	var Pul
	
	Pul = "<table width='500' cellspacing='2' cellpadding='1' border='0' align='center'>"
	Pul = Pul + "<tr height='50' >"
	Pul = Pul + "	<td align='center'>"
	Pul = Pul + "		<input type=image src='" + Progetto + "/images/chiudi.gif' title='Cerrar la pagina' border='0' onclick='CerrarVentana();' id='Chiudi' name='Chiudi'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
	Pul = Pul + "		<input type=image src='" + Progetto + "/images/stampa.gif' title='Imprimir la pagina' border='0' onclick='Imprimir1();'  id='Stampa' name='Stampa'>"
	Pul = Pul + "	</td>"
	Pul = Pul + "</tr>"
	Pul = Pul + "</table>"
	
	lblPulsanti.innerHTML = Pul
	lblAutorizzazioni.innerHTML = ""
	
}
function Imprimir1()
{
	document.FrmImpresion.txtimprimio.value = "S";

		v1= FrmImpresion.f.value;
		v2= FrmImpresion.Fasc.value
		v3= FrmImpresion.IdPers.value
		v6=frmImpresion.sCodFisc.value
		///v4=FrmImpresion.txtImprimio.value
		NroTarea=1
		alert("en imprimir1")
	//document.location.href = "ute_graboEstado.asp?f=" + v1 + "&Fasc=" + v2 + "&IdPers=" + v3 + "&txtImprimio=" + v4
	///document.location.href = "ute_graboEstado.asp?f=" + v1 + "&Fasc=" + v2 + "&IdPers=" + v3
	document.location.href = "ute_SeguroGraboEstado.asp?nTarea=" + NroTarea + "&IdPers=" + v3  + "&Cuil=" + v6
	///GRABA EN TABLA ASP
		
}


function Imprimir()
{
	document.FrmImpresion.txtimprimio.value = "S";
	self.print();
	
}


function AntesImprimir()
{
	alert("xxxxx")
	
}

function CerrarVentana()
{
	alert(document.FrmImpresion.txtimprimio.value);
	
	self.close();
	
	   
	
	
}

</script>

 
<%

'variabili utilizzate dalla funzione PrimaDellaStampa() per scrivere le autorizzaizoni da
'visualizzare alla fine della pagina
dim sqlSedeImpr, RRSedeImpr
dim sAutirizzazione

'if session("progetto") <> "/PLAVORO" then
if Session("creator")<> 0  and Session("tipopers")="S" then

	sqlSedeImpr = " SELECT I.Rag_Soc As Impresa, SI.Descrizione As Sede " &_
				  " FROM Impresa I, Sede_Impresa SI " &_
				  " WHERE SI.Id_Impresa = I.Id_Impresa And SI.Id_Sede = " & Session("creator")& _
				  " AND COD_TIMPR='03'"
				 
				   
'PL-SQL * T-SQL  
SQLSEDEIMPR = TransformPLSQLToTSQL (SQLSEDEIMPR) 
	set RRSedeImpr = CC.Execute(sqlSedeImpr)
	
	if not RRSedeImpr.Eof then
		sAutirizzazione = "<i>" & lcase(RRSedeImpr("Impresa") & "(" & RRSedeImpr("Sede") & ")") & "</i>"
	end if
	RRSedeImpr.close 
	set RRSedeImpr = nothing
else
	sAutirizzazione = "No"
end if

%>
<center>
<body onload="DopoDellaStampa('<%=session("progetto")%>','<%=ConvDateToString(date())%>','<%=Ucase(sTitolo)%>','<%=sWidthTitolo%>','<%=sWidthResto%>','<%=sUrlHelp%>','<%=sCommento%>')" 
		onbeforeprint="PrimaDellaStampa('<%=session("progetto")%>','<%=ConvDateToString(date())%>','<%=sTitolo%>','<%=sAutirizzazione%>')"
		onafterprint="DopoDellaStampa('<%=session("progetto")%>','<%=ConvDateToString(date())%>','<%=Ucase(sTitolo)%>','<%=sWidthTitolo%>','<%=sWidthResto%>','<%=sUrlHelp%>','<%=sCommento%>')">
	
<label name="lblDopo" id="lblDopo"></label>
<label name="lblPrima" id ="lblPrima"></label>
<br>

