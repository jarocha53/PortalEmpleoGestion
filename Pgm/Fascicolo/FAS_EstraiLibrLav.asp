<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include virtual="/include/DecComun.asp"-->
<table width="745" cellpadding="1" cellspacing="2" border="0">
	<tr height="20" class="sfondocomm"> 
		<td align="middle" width="100"><b>Experiencia</b></td>
		<td align="middle" width="200"><b>Cargo </b></td>
		<td align="middle" width="230"><b>Cargo Equivalente</b></td>
		<td align="middle" width="345"><b>Funciones</b></td>
		<td align="middle" width="200"><b>Empleador</b></td>
		<td align="middle" width="50"><b>Del</b></td>
		<td align="middle" width="50"><b>Al</b></td>
	</tr>
<%
sSQL = "SELECT COD_ESPERIENZA,DT_INI_ESP_PRO," & _
		"DT_FIN_ESP_PRO,COD_MANSIONE," & _
		"RAG_SOC,COD_FORMA, CARGO_ASOC, TAREASREALIZADAS FROM ESPRO" & _
		" WHERE ID_PERSONA=" & sUtente & _
		" ORDER BY DT_FIN_ESP_PRO DESC,DT_INI_ESP_PRO DESC"

'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
set rsEspro = CC.Execute(sSQL)

DO while not rsEspro.EOF

	COD_ESPERIENZA=rsEspro("COD_ESPERIENZA")
	COD_MANSIONE=rsEspro("COD_MANSIONE")
	RAG_SOC=rsEspro("RAG_SOC")
	COD_FORMA=rsEspro("COD_FORMA")
	DT_INI_ESP_PRO=rsEspro("DT_INI_ESP_PRO")
	DT_FIN_ESP_PRO=rsEspro("DT_FIN_ESP_PRO")	
	CARGO=rsEspro("CARGO_ASOC")
	FUNCIONES=rsEspro("TAREASREALIZADAS")

	if DT_INI_ESP_PRO <> "" then
		DT_INI = ConvDateToString(DT_INI_ESP_PRO)
	else
		DT_INI = ""
	end if	

	if DT_FIN_ESP_PRO <> "" then
		DT_FINE = ConvDateToString(DT_FIN_ESP_PRO)
	else
		DT_FINE = ""
	end if	
		
	if COD_MANSIONE <> "" then			
		'sSQL = "SELECT MANSIONE FROM CODICI_ISTAT WHERE CODICE = '" & COD_MANSIONE & "'"
		     
		ssql = "select denominazione from figureprofessionali where id_figprof = " & cod_mansione
				
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	    set rsMansione = CC.Execute(sSQL)
		if not rsMansione.eof then				   
		     descMansione = rsMansione("denominazione") 
		else
		     descMansione = COD_MANSIONE
		end if				
		rsMansione.Close
						
	    set rsMansione = nothing
	end if

	if COD_ESPERIENZA <> "" then
	   descEsperienza = DecCodVal("TESPR",0,"",COD_ESPERIENZA,"")
	end if

	if COD_FORMA <> "" then
	   decForma = DecCodVal("FGIUR",0,"",COD_FORMA,"")
	end if
%> 
	<tr class="tblsfondo"> 
		<%if DT_FINE= "" THEN %>
		<td >
			<span class="tblagg">
			<%=descEsperienza%>
			</span>
		</td>
		<%else%>
		<td>
			<span class="tbldett">
			<%=descEsperienza%>
			</span>
		</td>
		<%end if%>		
		<td >
			<span class="tbldett">
			<%=CARGO%>
			</span>
		</td>				
		<td >
			<span class="tbldett">
			<%=descMansione%>
			</span>
		</td>				
		<td >
			<span class="tbldett">
			<%=FUNCIONES%>
			</span>
		</td>				
		<td >
			<span class="tbldett">
			<%=RAG_SOC%>-&nbsp;<%=decForma%><br>
			<%' Solo per il progetto Quadri in quanto proprietario della tabella Sede_espro
				Progetto = mid(Session("Progetto"),2)
				If Progetto = "QUADRI" Then
					Set rstSede = Server.CreateObject("ADODB.Recordset")
					SQLSede = "SELECT indirizzo,comune,prv,regione FROM Sede_Espro WHERE id_persona =" & sUtente
'PL-SQL * T-SQL  
SQLSEDE = TransformPLSQLToTSQL (SQLSEDE) 
					rstSede.open SQLSede,CC,1,3
					If not rstSede.EOF Then
						sIndirizzo = rstSede("indirizzo")
						nidComune = rstSede("comune")
						sProvSede = rstSede("prv")
						Response.Write sIndirizzo
						Response.Write "&nbsp;-&nbsp;"
						Response.Write DescrComune(nidComune) 
						Response.Write "&nbsp;-&nbsp;"
						Response.Write sProvSede
					End If
				Else
					Response.Write "&nbsp;-&nbsp;"
				End If
			%>
			
			</span>
		</td>				
		<td >
			<span class="tbldett">
			<%=DT_INI%>
			</span>
		</td>				
		<td >
			<span class="tbldett">
			<%=DT_FINE%>
			</span>
		</td>				
	</tr>
<%
	rsEspro.MoveNext
loop
rsEspro.Close
set rsEspro = nothing
%>
</table>
