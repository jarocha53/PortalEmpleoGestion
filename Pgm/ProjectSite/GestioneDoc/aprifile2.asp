<%
Response.Buffer = true
DocsPath = request("DocsPath")

nfile = replace(Trim(Request("nfile")),"$","'")
IlFile = server.MapPath (DocsPath & "/" & nfile)


Set objStream = Server.CreateObject("ADODB.Stream")
objStream.Open

objStream.Type = 1
objStream.LoadFromFile IlFile

Response.AddHeader "Content-Disposition", "attachment; filename=" & nfile
Response.ContentType = "application/octet-stream"
Response.BinaryWrite objStream.Read
Response.Flush
objStream.Close
Set objStream = Nothing
%>
