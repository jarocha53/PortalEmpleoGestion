<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->


<html>
<head>
	<LINK REL=STYLESHEET TYPE="text/css" HREF='<%=Session("Progetto")%>/fogliostile.css'>
	<meta http-equiv=refresh content="1,ProgressBar.asp?ProgressID=<%=Request.QueryString("ProgressID")%>">
	<title>Trasferimento file in Corso ...</title>
</head>
<%
On Error Resume Next
Response.Expires = -1

Set objUploadProgress = Server.CreateObject("Dundas.UploadProgress")


'### Recupero dati sullo stato dell'Upload
	objUploadProgress.ProgressID = Request.QueryString("ProgressID")
	objUploadProgress.GetProgress
	Percentage = objUploadProgress.PercentCompleted
	TotalSize  = objUploadProgress.TotalSize
	UploadSize = objUploadProgress.UploadedSize

if TotalSize = -1 OR UploadSize = -1 Then
	TotalSize = 0
	UploadSize = 0
End If

if Err.number <> 0 Then
	Percentage = -1
end if

	if objUploadProgress.PercentCompleted = 100 Then
		objUploadProgress.DeleteProgress
	else
		objUploadProgress.DeleteProgress
	End If


Set objUploadProgress = Nothing
	
%>

<script language=Javascript>
<!--
var val = <%=Percentage%>;
if(val == -1)
{
	top.close();
}
//-->
</script>

<body bgcolor=FFFFFF>
<table border="1" width="100%" bordercolor="#003466">
  <tr>
    <td>
      <table ID="Prog" border="0" width="<%=Percentage%>%">
        <tr class='sfondomenu'>
          <td width="100%">&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<br>
<span Class='tbltext1' align=center>
<%=Percentage%>% (Trasferiti <%=UploadSize%> di <%=TotalSize%> bytes)
</span>
</body>
</html>
