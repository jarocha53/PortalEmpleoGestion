<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #include virtual="/strutt_testa2.asp" -->
<!-- #include virtual="/include/OpenConn.asp" -->
<!-- #include virtual="/util/portallib.asp" -->
<!-- #include virtual="/util/globalsub.asp" -->
<!-- #include virtual="/include/DecCod.asp" -->
<!-- #include virtual="/include/RuoloFunzionale.asp" -->

<script language="javascript">
<!--#include virtual = "/Include/help.inc"-->
</script>
<%

If Not ValidateService(Session("IdUtente"),"Gestione Gruppi",cc)  Then 
	Response.Redirect "/util/error_login.asp"
End If

%>
<br>
<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
	<tr height="18">
		<td class="sfondomenu" width="67%" height="18"><span class="tbltext0"><b>&nbsp;Configuración del Sistema - Listado de Grupos</b></span></td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
		<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
	</tr>
	
	<tr>
		<td class="sfondocomm" width="57%" colspan="3" height="18" align="left"><br>
			<a href="Javascript:Show_Help('/pgm/help/Gruppi/ListaGruppi/Lista_gru')"><img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>	
		</td>
	</tr>
	<tr height="17">
		<td class="sfondocomm" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
		
	</tr>	
</table>
<br>
<table border="0" cellpadding="1" cellspacing="1" width="500">
	<tr class="sfondocomm"> 
		<td align="left" height="30" valign="middle" width="50"><b>Id</b></td>
		<td align="left" valign="middle" width="300"><b>Descripción</b></td>
		<td align="left" valign="middle" width="180"><b>Identificador</b></td>   
		<td align="left" valign="middle" width="200"><b>Rol Organizativo</b></td>
		<td align="left" valign="middle" width="220"><b>Rol Funcional</b></td>
	</tr>
<%
	Set rstGruppo = server.CreateObject("ADODB.RECORDSET")

	SQLGruppo ="SELECT idgruppo, desgruppo, webpath, cod_rorga, cod_ruofu, DT_TMST " & _
	           "FROM gruppo " & _ 
	           "ORDER BY idgruppo"
	           
	SQLGruppo = UCase(SQLGruppo)
'PL-SQL * T-SQL  
SQLGRUPPO = TransformPLSQLToTSQL (SQLGRUPPO) 
	rstGruppo.open SQLGruppo, CC, 1, 3


	If rstGruppo.EOF <>True Then rstGruppo.MoveFirst
	While rstGruppo.EOF <> True 
		sDecodifica = DecCodVal("RORGA", 0, rstGruppo("DT_TMST"), rstGruppo("COD_RORGA"), "")
 %>
	<tr class="tblsfondo"> 
	
	
		
		<td class="tblDett">
			<input type="hidden" value="<%=rstGruppo("IDGRUPPO")%>" name="txtidgru">
			<%=rstGruppo("IDGRUPPO")%>
		</td>
					
		<td class="tblDett">
			<input type="hidden" value="<%=rstGruppo("DESGRUPPO")%>" name="txtdes">
			<%=rstGruppo("DESGRUPPO")%>
		</td>
					
		<td class="tblDett">
			<input type="hidden" value="<%=rstGruppo("WEBPATH")%>" name="txtpath">
			<%=mid(rstGruppo("WEBPATH"),2)%>
		</td>
		<td class="tblDett">
			<input type="hidden" value="<%=sDecodifica%>" name="txtpath">
			<%=sDecodifica%>
		</td>
		
		<td class="tblDett" align="left" width="300">
				
			<option value="<%=rstGruppo("cod_ruofu")%>"><%=DecRuolo(rstGruppo("cod_ruofu"))%></option>
		</td>
		</tr>
<%
	rstGruppo.MoveNext
	Wend
	rstGruppo.close
	Set rstGruppo = nothing		
%> 
</table>
<!-- #include virtual="/include/closeconn.asp" -->
<!-- #include virtual="/strutt_coda2.asp" -->
