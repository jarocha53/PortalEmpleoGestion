<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<html>
<head>
<title>Area</title>
<link REL="STYLESHEET" TYPE="text/css" HREF="<%=session("progetto")%>/fogliostile.css">
<script language="javascript">
<!--#include virtual="/include/help.inc" -->
<!--#include virtual = "/Include/ControlNum.inc"-->

function Campi(sCompresa) {
	var nSelTab
	var nSelCampo
	var sDecod
	
	nSelCampo	= opener.document.frmInsCriteri.elements[0].selectedIndex
	nOper		= opener.document.frmInsCriteri.elements[1].selectedIndex
	if (document.frmSelArea.txtNumValori.value == ""){
		alert("Debe indicar un valor")
		return false
	}
	if (!IsNum(document.frmSelArea.txtNumValori.value)){
		alert("Debe indicar un valor num�rico")
		return false
	}

	if (document.frmSelArea.txtNumValori.value <= 1){
		alert("Debe indicar un valor al menos mayor o igual a 2")
		return false
	}
	if (document.frmSelArea.txtNumValori.value <= 10)  {
		if (sCompresa == "0") {
			
			opener.document.location.href ="RIC_VisCriteri.asp?prVolta=No&Campo=" + 
				opener.document.frmInsCriteri.elements[0].options[nSelCampo].value +
				"&OpeLog=" + opener.document.frmInsCriteri.elements[1].options[nOper].value +
				"&IDTAB=" + opener.document.frmInsCriteri.IDTAB.value + 
				"&AREA=DIZ_DATI&SPECIFICO=DIZ_TAB&DESCTAB=" + opener.document.frmInsCriteri.DESCTAB.value + 
				"&IDRIC=" + opener.document.frmInsCriteri.IDRIC.value + 
				"&IDTAB1=" + opener.document.frmInsCriteri.IDTAB1.value +
				"&IDSEDE=" + opener.document.frmInsCriteri.IdSede.value +
				"&NumValori=" + document.frmSelArea.txtNumValori.value
		}else {
			opener.document.location.href ="RIC_VisCriteri.asp?prVolta=No&Campo=" + 
				opener.document.frmInsCriteri.elements[0].options[nSelCampo].value + sCompresa +
				"&OpeLog=" + opener.document.frmInsCriteri.elements[1].options[nOper].value +
				"&IDTAB=" + opener.document.frmInsCriteri.IDTAB.value + 
				"&AREA=DIZ_DATI&SPECIFICO=DIZ_TAB&DESCTAB=" + 
				opener.document.frmInsCriteri.DESCTAB.value +
				"&IDRIC=" + opener.document.frmInsCriteri.IDRIC.value + 
				"&IDTAB1=" + opener.document.frmInsCriteri.IDTAB1.value +
				"&IDSEDE=" + opener.document.frmInsCriteri.IdSede.value +
				"&NumValori=" + document.frmSelArea.txtNumValori.value
		}
	}else{
		alert("M�ximo 10 valores")
		return false
	}
	return true
}


function Invia()
{
	if (Campi(0)) {
		self.close()
		return true
	}
	return false
}

function Destro(e) 
{
	if (navigator.appName == 'Netscape' && 
		(e.which == 3 || e.which == 2))
		return false;
	else if (navigator.appName == 'Microsoft Internet Explorer' && 
			(event.button == 2 || event.button == 3))
		 {
			alert("Disculpe, el bot�n derecho del mouse esta deshabilitado");
			return false;
		  }
	return true;
}

// disattivazione del tasto destro sel mouse
document.onmousedown=Destro;
if (document.layers) window.captureEvents(Event.MOUSEDOWN);
window.onmousedown=Destro;

</script>
</head>
<body onload="javascript:document.frmSelArea.txtNumValori.focus();">
<form name="frmSelArea" onsubmit="return Invia()">


<table border="0" CELLPADDING="0" CELLSPACING="0" width="210" align="center">

<tr height="17">
	<td class="sfondomenu" width="67%" height="18"><span class="tbltext0">
		<b>&nbsp;N�MERO DE VALORES</b>
	</td>
	<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">
	</td>
	<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
</tr>
<tr>
	<td class="sfondocommaz" width="57%" colspan="3">
		<br>Indicar el n�mero de valores
	</td>
</tr>
<tr height="2">
	<td class="sfondocommaz" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif">
	</td>
</tr>
</table>
<br>		
<table border="0" CELLPADDING="0" CELLSPACING="0" width="210" align="center">
	<tr align="left">
	<td width="170" class="tbltext1">
		<b>Num. Valores Max(10)<b>
	</td>
	<td width="20">
		<input class="textblacka" size="2" name="txtNumValori">	
	</td>
	<td width="10">
		<input type="image" title="Invia Richiesta" src="<%=Session("progetto")%>/images/conferma.gif">
<!--		<a href="javascript:Invia()"><img src="<%'=Session("progetto")%>/images/conferma.gif" title="Chiudi la pagina"  border="0" align ="absBottom"></a>-->
	</td>
	</tr>
</table>		
<table width="210" cellspacing="2" cellpadding="1" border="0" align="center">
	<tr align="center">
		<td>
			<a href="javascript: self.close()"><img src="<%=Session("progetto")%>/images/chiudi.gif" title="Chiudi la pagina" border="0" align="absBottom"></a>
		</td>
	</tr>		
</table>
</form>

</body>
</html>
