<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include virtual="/util/portallib.asp"-->
<!--#include Virtual = "/strutt_testa2.asp"-->
<!--#include Virtual = "/include/SysFunction.asp"-->
<!--#include Virtual ="/Include/openconn.asp"-->
<!--#include Virtual ="/Include/ControlDateVB.asp"-->
<!--#include Virtual = "/include/DecCod.asp"-->
<!--#include virtual= "/util/dbutil.asp"-->


<script LANGUAGE="Javascript">
//include del file per fare i controlli sulla validit� delle date
<!--#include virtual = "/Include/ControlDate.inc"-->

//include del file per fare i controlli sulla numericit� dei campi
<!--#include virtual = "/Include/ControlNum.inc"-->
<!--#include virtual = "/Include/help.inc"-->

function Validate(TheForm)
{
	if (TheForm.cmbRichiesta.value == "")
	{
		alert("El campo tipo de b�squeda es obligatorio!");
		TheForm.cmbRichiesta.focus();
		return false;
	}
	if (TheForm.cmbTipoRicerca.value == "")
	{
		alert("El campo tipo de b�squeda es obligatorio!");
		TheForm.cmbTipoRicerca.focus();
		return false;
	}
	if (TheForm.txtNPosti.value == "")
	{
		alert("El campo n�mero de puestos requeridos es obligatorio!");
		TheForm.txtNPosti.focus();
		return false;
	}
	//Controllo numericit� dati numerici
	if (!Numerico(TheForm.txtNPosti))
	{
		return false;
	}
	if (TheForm.txtNPosti.value==0)
	{
		alert("Ingresar un valor mayor a cero.");
		TheForm.txtNPosti.focus();
		return false;
	}
	if (TheForm.txtDtDal.value == "")
	{
		alert("El campo fecha inicio de validez es obligatorio!");
		TheForm.txtDtDal.focus();
		return false;
	}
	if(!ValidateRangeDate(TheForm.txtOggi.value,TheForm.txtDtDal.value))
	{	
		alert("La fecha de inicio de validez debe ser superior a la fecha de carga de la b�squeda!")
		TheForm.txtDtDal.focus();
		return false;
	}			
	if (TheForm.txtDtAl.value == "")
	{
		alert("El campo fecha fin de validez es obligatorio!");
		TheForm.txtDtAl.focus();
		return false;
	}
	if(!ValidateRangeDate(TheForm.txtOggi.value,TheForm.txtDtAl.value))
	{	
		alert("La fecha de fin de validez debe ser superior a la fecha de carga de la b�squeda!")
		TheForm.txtDtAl.focus();
		return false;
	}			
	//Controllo delle date e dell'intervallo
	if(!intervallo(TheForm.txtDtDal,TheForm.txtDtAl))
	{
		return false;
	}
	//Controllo altri campi
	if (TheForm.txtDtAvv.value != "")
	{
		if (!ValidateInputDate(TheForm.txtDtAvv.value))
 		{
			TheForm.txtDtAvv.focus();
			return false;
		}
		if(!ValidateRangeDate(TheForm.txtDtDal.value,TheForm.txtDtAvv.value))
		{	
			alert("La fecha de carga debe ser inferior a la fecha de inicio de validez!")
			TheForm.txtDtAvv.focus();
			return false;
		}			
	}
	return true;
}
//++++++++++++++++++++		
function Numerico(campo)
{			
	var dim=campo.size;
	var anyString = campo.value;

	for ( var i=0; i<=anyString.length-1; i++ )
	{
		if ( (anyString.charAt(i) >= "0") && (anyString.charAt(i) <= "9") )
		{
		}
		else
		{
			alert("Atencion: Debe ingresar un valor num�rico!");
			campo.focus();
			return(false);
		}
	}
	return true;
}
//++++++++++++++++++++		
function intervallo(dtDal,dtAl)
{	
	if (!ValidateInputDate(dtDal.value))
 	{
		dtDal.focus();
		return false;
	}	
	if (!ValidateInputDate(dtAl.value))
 	{
		dtAl.focus();
		return false;
	}	
	if (!ValidateRangeDate(dtDal.value, dtAl.value))
	{
		dtDal.focus();
		alert("Controlar el intervalo de fechas ingresado."); 
		return false;
	}
	return true;
}

</script>
<script language="javascript" src="Controlli.js">
</script>
<br>
<%
'*********** MAIN ***************
Dim sIdSede, nIdRich
	
	nIdRich= request("idRic")
	sIdsede=Request("Idsede")
	
	if sIdSede = "" then
		sIdSede = Session("Creator")
	end if
	
	Inizio()
	Imposta_Pag()
	
'**************SUB***************		
'********************************	
Sub Imposta_Pag()
	Dim sArea
	Dim sTespr
	Dim sAreaProf
	Dim nIdAreaProf
	Dim nProfilo
	Dim sTipoProf
	Dim sTipoRic
	Dim sTipoRicerca
	Dim nPosti
%>
	<form method="post" name="FrmPrincipale" action="RIC_CnfNewRichiesta.asp" onsubmit="return Validate(this)">
		<input type="hidden" name="idSede" value="<%=sIdSede%>">
		<input type="hidden" name="idRich" value="<%=nIdRich%>">
		
		<table WIDTH="500" BORDER="0" CELLPADDING="2" CELLSPACING="2">
			<tr>
		        
      <td class="tbltext1" width="190"> <b>Grupo Ocupacional</b></td>
		        <td class="textblack" ALIGN="LEFT" WIDTH="310"> 
					<b>
						<%
						sAreaProf = request("cmbTipoArea")
						nIdAreaProf = request("idArea")
						Response.Write sAreaProf
						%>
					</b>
					<input type="hidden" name="nIdAreaProf" value="<%=nIdAreaProf%>">
					<input type="hidden" name="sAreaProf" value="<%=sAreaProf%>">
				</td>
			</tr>
			<tr>
		        
      <td class="tbltext1" width="190"> <b>Puesto</b></td>
		        <td class="textblack" width="310" align="left"> 
					<b>
						<%
						nProfilo = request("idProfilo")
						sTipoProf = request("cmbTipoProf")
						Response.Write sTipoProf
						%>
					</b>
					<input type="hidden" name="nProfilo" value="<%=nProfilo%>">
					<input type="hidden" name="sTipoProf" value="<%=sTipoProf%>">
				</td>
			</tr>
			<tr>
		        
	  <!-- <td class="tbltext1"> <b>Tipo de solicitud*</b> </td> -->
	       <td class="textblack"> 
					<%		       
	'				sTipoRic = request("cmbTipoRic")
	'				sTespr = "TESPR|0|" & date & "|" & sTipoRic & "|cmbRichiesta' style='WIDTH: 280px'|And SUBSTR(VALORE,1,1)= 'S' ORDER BY DESCRIZIONE"
	'				CreateCombo(sTespr)
				%>				
				</td>
			</tr>
			<tr>
		        
      <td class="tbltext1"> <b>Tipo de b�squeda*</b> </td>
		        <td class="textblack"> 
					<select name="cmbTipoRicerca" class="textblacka">
						<%
						sTipoRicerca = request("cmbTipoRicerca") 
						select case (sTipoRicerca)	
							case 0
						%>	
						<option value="0" selected> POR GRUPO OCUPACIONAL</option>
						<option value="1"> POR PUESTO/OCUPACION </option>
						<%
							case 1					
						%>	
						<option value="0"> POR GRUPO OCUPACIONAL</option>
						<option value="1" selected> POR PUESTO/OCUPACION</option>
						<%
						end select
						%>	
					</select>
				</td>
			</tr>
			<tr>
		        
      <td class="tbltext1"> <b>N�mero puestos requeridos*</b> </td>
		        <td class="textblack"> 
					<input class="textblacka" type="text" name="txtNPosti" size="3" maxlength="3" value>
				</td>
			</tr>
			<tr>
		        
      <td class="tbltext1"> <b>Per�odo de validez (de/al)* (dd/mm/aaaa)</b> </td>
		        <td class="textblack"> 
					<input class="textblacka" type="text" name="txtDtDal" size="10" maxlength="10">
					<b class="textblacka">/</b>
					<input class="textblacka" type="text" name="txtDtAl" size="10" maxlength="10">
				</td>
			</tr>
			<tr>
		        
      <td class="tbltext1"> <b>Fecha Incorporacion al Puesto/Ocupacion<br>
        (dd/mm/aaaa)</b> </td>
		        <td class="textblack"> 
					<input class="textblacka" type="text" name="txtDtAvv" size="10" maxlength="10">
				</td>
			</tr>
		</table>
		<br>
		<table width="500" cellspacing="2" cellpadding="1" border="0">
			<tr align="center">
				<td>
					<input type="image" src="<%=Session("progetto")%>/images/conferma.gif" title="Conferma richiesta" name="submit" border="0" align="absBottom">
				</td>
				<td>
					<a href="javascript:history.back()"><img src="<%=Session("progetto")%>/images/Indietro.gif" title="Pagina precedente" border="0" align="absBottom"></a>
				</td>
			</tr>
		</table>
		
		<input name="txtOggi" value="<%=ConvDateToString(Date())%>" type="hidden">
	</form>

<%
End Sub 

'**************SUB***************		
'********************************	
Sub Inizio()
%>
	<table border="0" width="520" cellspacing="0" cellpadding="0" height="70">
		<tr>
			<td width="500" background="<%=session("progetto")%>/images/titoli/servizi2g.gif" height="70" valign="bottom" align="right">
				<table border="0" background width="500" height="23" cellspacing="0" cellpadding="0">
					<tr>
						
          <td width="100%" valign="top" align="right"> <b>B�squeda Personal</b></td>
					</tr>
				</table>
			</td>
		</tr>
	</table> 
	<br>
	<!-- Linguetta superiore -->
	<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
		<tr height="18">
			
    <td class="sfondomenu" width="67%" height="18"><span class="tbltext0"><b>&nbsp;INGRESO 
      DE LA SELECCION</b></span></td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
			
    <td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">(*) 
      campo obligatorio</td>
		</tr>
	</table>
	<!-- Commento -->
	<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
		<tr>
			
    <td align="left" class="sfondocommaz"> Completar los datos relacionados con 
      la busqueda y luego presionar el boton enviar<a href="Javascript:Show_Help('/Pgm/help/Richieste/RIC_InsNewRichiesta')"><img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a> 
    </td>
		</tr>
		<tr height="2">
			<td class="sfondocommaz" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>
	<br>
<%
End Sub 
'********FINE SUB***************		
'*******************************
%>		
<!--#include Virtual ="/Include/closeconn.asp"-->
<!--#include Virtual="/strutt_coda2.asp"-->		
