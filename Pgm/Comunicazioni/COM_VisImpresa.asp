<!--#include virtual="/util/portallib.asp"-->
<!--#include virtual="/strutt_testa2.asp"-->
<!--#include virtual="/include/openconn.asp"-->
<!--#include virtual ="/include/ckProfile.asp"-->
<!--#include File = "DecDizDati.asp"-->

<%
'	if ValidateService(session("idutente"),"COM_VISIMPRESA",cc) <> "true" then 
'		response.redirect "/util/error_login.asp"
'	end if
%>

<script LANGUAGE="Javascript">
	<!--#include virtual="/Include/help.inc"-->
	<!--#Include Virtual="/Include/SelSedeImpresa.js" -->	
	<!--#include virtual = "/Include/ControlNum.inc"-->
	<!--#Include Virtual="/Include/ControlString.inc" -->
	<!--#include Virtual="/Include/ControlCodFisc.inc"-->
	<!--#include virtual="/Include/ControlDate.inc"-->
	
//SM include controllo Cedula de Identidad
<!--#include Virtual = "/Include/ControlloCI.inc"-->

	function ControllaDati(frmSelContratti) {

		frmSelContratti.txtPartIva.value = TRIM(frmSelContratti.txtPartIva.value);
		document.frmSelContratti.txtCodFisc.value=TRIM(document.frmSelContratti.txtCodFisc.value);
		
		if (frmSelContratti.txtPartIva.value.length == 0 && frmSelContratti.txtCodFiscAzienda.value.length == 0) {
			alert("Ingrese REGICE o Ruc de la empresa")
			frmSelContratti.txtPartIva.focus() 
			return false
		}

		if (frmSelContratti.txtPartIva.value.length > 0 && frmSelContratti.txtCodFiscAzienda.value.length > 0) {
			alert("Los campos REGICE y Ruc de la empresa son alternativos")
			frmSelContratti.txtPartIva.focus() 
			return false
		}
			
		if (frmSelContratti.txtPartIva.value.length > 0 )	{ 
			if (!IsNum(frmSelContratti.txtPartIva.value)) {
				alert("El campo REGICE debe ser num�rico")
				frmSelContratti.txtPartIva.focus() 
				return false
			}
			if (frmSelContratti.txtPartIva.value.length != 11) {
				alert("El campo REGICE debe ser de 11 caracteres")
				frmSelContratti.txtPartIva.focus() 
				return false
			}
		}

		if 	(frmSelContratti.txtCodFiscAzienda.value.length > 0 )	{
			if 	(!ValidateInputStringWithNumber(frmSelContratti.txtCodFiscAzienda.value))	{
				alert("El formato del campo Ruc de la empresa es err�neo")
				frmSelContratti.txtCodFiscAzienda.focus();
				return false;
			}
			blank = " ";
			if (!ChechSingolChar(frmSelContratti.txtCodFiscAzienda.value,blank))	{
				alert("El campo Ruc de la empresa no puede contener espacios en blanco")
				frmSelContratti.txtCodFiscAzienda.focus();
				return false;
			}
			if ((frmSelContratti.txtCodFiscAzienda.value.length != 12)) {
				alert("El Ruc de la empresa debe ser de 11")
				frmSelContratti.txtCodFiscAzienda.focus(); 
				return false;
			}
		}
	

		if (frmSelContratti.txtDescSede.value == "") {
			alert("Indicar la sede");
			frmSelContratti.txtDescSede.focus() ;
			return false;
		} 

		if (frmSelContratti.CmbContratti.value == "") {
			alert("Indicar el tipo de comunicaci�n");
			frmSelContratti.CmbContratti.focus() ;
			return false;
		} 

		if (frmSelContratti.txtCodFisc.value  == "") {
			alert("Indicar la C.I.");
			frmSelContratti.txtCodFisc.focus() ;
			return false;
		} 

// SM inizio

		//frmSelContratti.txtCodFisc.value = TRIM(frmSelContratti.txtCodFisc.value.toUpperCase())
		//CodFisc = frmSelContratti.txtCodFisc.value

/*
		if (CodFisc.length != 16) {
			alert("Formato del Codice Fiscale errato.");
			frmSelContratti.txtCodFisc.focus() ;
			return false;
		}
				
		if (!ControllChkCodFisc(CodFisc)) {
			alert("Codice Fiscale Errato.");
			frmSelContratti.txtCodFisc.focus();
			return false;
		}
*/
		
		if (document.frmSelContratti.txtCodFisc.value != "")
		{
				blank = " ";
				if (!ChechSingolChar(document.frmSelContratti.txtCodFisc.value,blank))
				{
		
					alert("El campo C.I. no puede contener espacios en blanco")
					document.frmSelContratti.txtCodFisc.focus()
					return false;
				}
				
				if ((document.frmSelContratti.txtCodFisc.value.length != 8))
				{
					alert("El campo C.I. debe ser de 8 caracteres")
					document.frmSelContratti.txtCodFisc.focus() 
					return false
				}			
				if (!ValidateInputStringWithNumber(document.frmSelContratti.txtCodFisc.value))
				{
					alert("El campo C.I.  es err�neo")
					document.frmSelContratti.txtCodFisc.focus() 
					return false
				}
				
				if (!IsNum(document.frmSelContratti.txtCodFisc.value))
				{
					alert("El campo C.I. debe ser num�rico")
					document.frmSelContratti.txtCodFisc.focus() 
					return false
				}		
		
				if (!ControlloCI(eval(document.frmSelContratti.txtCodFisc.value)))
				{
				     document.frmSelContratti.txtCodFisc.focus()
				     return false
				}
	
				
		}
		
		

	/*	if ( !ControllCodFisc('',CodFisc))
			{
				frmSelContratti.txtCodFisc.focus();
				return false;
			}*/
	
	// SM fine

		frmSelContratti.txtDtComunicazione.value = TRIM(frmSelContratti.txtDtComunicazione.value)
		if (frmSelContratti.txtDtComunicazione.value == ""){
			alert("Indicar la fecha de la comunicaci�n.");
			frmSelContratti.txtDtComunicazione.focus();
			return false;
		}

		if (!ValidateInputDate(frmSelContratti.txtDtComunicazione.value)) {
			frmSelContratti.txtDtComunicazione.focus();
			return false;
		}

		if(!ValidateRangeDate('01/01/1900',frmSelContratti.txtDtComunicazione.value)) {	
			alert("La fecha de la comunicaci�n debe ser superior al 01/01/1900")
			frmSelContratti.txtDtComunicazione.focus();
			return false;
		}			

		DataOdierna = document.frmSelContratti.txtOggi.value;
		if (ValidateRangeDate(frmSelContratti.txtDtComunicazione.value, DataOdierna)==false){
			alert("La fecha no debe ser superior a la fecha actual!");
			frmSelContratti.txtDtComunicazione.focus();
			return false;
		}

		return true;
	}

	function userfocus() {
		document.frmSelContratti.txtPartIva.focus()	
	}

	function PulisciSede() {
		document.frmSelContratti.IdSede.value = '';
		document.frmSelContratti.txtDescSede.value = '';
	}
</script>
</head>

<body background="../../images/sfondo1.gif" onload="return userfocus()">
<%
	dim CnConn
	dim rsAziende
	dim sSQL
	dim nCont
	dim sIsa
	dim sPartIva
	dim CodFiscale
	dim i
	dim nMaxElem
	dim sMask
	
	' Controllo i diritti dell'utente connesso.
	' La variabile di sessione mask � cos� formata:
	' 1 Byte abilitazione funzione Impresa
	' 2 Byte abilitazione funzione Sede_impresa
	' 3 Byte abilitazione funzione Val_impresa
	' dal 4� byte in poi si visualizzano le provincie.
		
	sMask = Session("mask") 
	' Mi definisco le variabili Diritti
	Session("Diritti")= mid(sMask,1,6)
	' Mi definisco le variabili Filtro
	Session("Filtro")= mid(sMask,7)	

	sMask = Session("Diritti")

	sPartIva = Request("txtPartIva")
	sCodFiscale = ucase(Request("txtCodFiscAzienda"))
	
	if mid(Session("UserProfiles"),1,1) = "0" then
		sIsa = "0"
	else
		sIsa = mid(Session("UserProfiles"),1,2)
	end if
%>

<table border="0" width="530" cellspacing="0" cellpadding="0" height="81">
	<tr>
		<td width="530" background="<%=Session("Progetto")%>/images/titoli/strumenti2g.gif" height="81" valign="bottom" align="right">
			<table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
				<tr>
					<td width="100%" valign="top" align="right">
						<b class="tbltext1a">Gesti�n de Comunicaciones &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b>
					</td>
				</tr>
			</table>
		</td>
   </tr>
</table>
<br>
<table cellpadding="0" cellspacing="0" width="500" border="0" align="center">
   <tr height="18">
		<td class="sfondomenu" height="18" width="67%">
			<span class="tbltext0">
				<b>&nbsp;INGRESO DE COMUNICACIONES</b>
			</span>
		</td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
		<td valign="middle" align="right" class="tbltext1" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">(*) campo obligatorio</td>
	</tr>
   <tr width="371" class="SFONDOCOMMAZ">
		<td colspan="3">
			Indicar los datos de la comunicaci�n.<br>
			Los campos <b>REGICE</b> y <b>Ruc de la empresa</b> se completan alternativamente.
			<a href="Javascript:Show_Help('/Pgm/help/Comunicazioni/COM_VisImpresa')"><align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
		</td>
	</tr>
	<tr height="2">
		<td colspan="3" class="SFONDOCOMMAZ" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
	</tr>
</table>
<br>
<form method="post" name="frmSelContratti" onsubmit="return ControllaDati(this)" action="COM_InsContratto.asp">
	<input type="hidden" id="text1" name="txtOggi" value="<%=ConvDateToString(Date())%>">
	<input type="hidden" id="text2" name="txtTipoIns" value="D">
	<table width="500" border="0" cellspacing="1" cellpadding="0">
		<tr>
			<td nowrap class="tbltext1" align="left">
				<b>REGICE</b>
			</td>
			<td nowrap class="tbltext" align="left">
				<span class="size">
					<input style="TEXT-TRANSFORM: uppercase; WIDTH: 254px;" name="txtPartIva" class="textblack" size="32" maxlength="11" onchange="Javascript:PulisciSede()">
				</span>	
			</td>
		</tr>
		<tr>
			<td nowrap class="tbltext1" align="left">
				<b>Ruc Empresa</b>
			</td>
			<td nowrap class="tbltext" align="left">
				<span class="size">
					<input style="TEXT-TRANSFORM: uppercase; WIDTH: 254px;" name="txtCodFiscAzienda" class="textblack" size="32" maxlength="12" onchange="Javascript:PulisciSede()">
				</span>
			</td>
		</tr>
		<tr>
			<td nowrap class="tbltext1" align="left">
				<b>Raz�n Social</b>
			</td>
			<td nowrap class="tbltext" align="left">
				<span class="size">
					<input style="TEXT-TRANSFORM: uppercase; WIDTH: 254px;" name="txtRazonSocial" class="textblack" size="32" maxlength="16" onchange="Javascript:PulisciSede()">
				</span>
			</td>
		</tr>
		<tr>
			<td nowrap class="tbltext1" align="left">
				<b>Sede*</b>
			</td>
			<td nowrap class="tbltext" align="left">
				<span class="size">
					<input style="TEXT-TRANSFORM: uppercase; WIDTH: 254px;" readonly name="txtDescSede" class="textblack" size="32">
				</span>
				<input type="hidden" name="IdSede">
				<%
				NomeForm = "frmSelContratti"
				NomeCfAzienda = "txtCodFiscAzienda"
				NomePiAzienda = "txtPartIva"
				NomeRS = "txtRazonSocial"
				NomeIdSede = "IdSede"
				NomeDescSede = "txtDescSede"
				%>
				<a href="Javascript:SelSedeImpresa('<%=NomeForm%>','<%=NomeCfAzienda%>','<%=NomePiAzienda%>','<%=NomeIdSede%>','<%=NomeDescSede%>','<%=NomeRS%>')" ID="imgPunto1" name="imgPunto1" onmouseover="javascript:window.status='';return true"><img border="0" src="<%=Session("Progetto")%>/images/bullet1.gif"></a>
			</td>
		</tr>
		<tr height="2">
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td nowrap class="tbltext1" align="left">
				<b>Tipo de comunicaci�n*</b>
			</td>
			<td nowrap class="tbltext" align="left">
				<%CreateComboCampoDesc "COM_SEDE","TIPO_COM","CmbContratti","",""%>	
			</td>
		</tr>
		<tr>
			<td nowrap class="tbltext1" align="left">
				<b>C.I. de la persona*</b>
			</td>
			<td nowrap class="tbltext" align="left">
				<input style="TEXT-TRANSFORM: uppercase;" size="21" maxlength="8" type="text" class="textblack" name="txtCodFisc">
			</td>
		</tr>
		<tr>
			<td nowrap class="tbltext1" align="left">
				<b>D�a de la Comunicaci�n*</b><br>(dd/mm/aaaa)
			</td>
			<td nowrap class="tbltext" align="left">
				<input class="textblack" type="text" name="txtDtComunicazione" maxlength="10" size="13">
			</td>
		</tr>
	</table>
	<br>
	<table border="0" CELLPADDING="2" CELLSPACING="2" width="500" align="center">
		<tr>
			<td align="right">
				<a href="Javascript:history.back()"><img border="0" src="<%=Session("Progetto")%>/images/indietro.gif"></a>
			</td>			
			<td align="left">
				<input type="image" src="<%=Session("Progetto")%>/images/conferma.gif" id="image1" name="image1">
			</td>
		</tr>
	</table>
</form>

<br>
<!--#include virtual ="/strutt_coda2.asp"-->
<!--#include virtual ="/include/closeconn.asp"-->