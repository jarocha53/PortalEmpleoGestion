function ChgContrattoDA(){
	document.frmContratto.txtDaContratto.value = document.frmContratto.chkContrattoDa.value
}

function ChgContrattoA(){
	document.frmContratto.txtAContratto.value = document.frmContratto.chkContrattoA.value
}

function ChkAbil(nAction){

	if (nAction == "1") {
		if (!document.frmContratto.comput.checked){
			document.frmContratto.chRiservato.disabled = true
			document.frmContratto.txtMotivo.value = ""
			document.frmContratto.txtMotivo.disabled = true
		}
		else {
			document.frmContratto.chRiservato.checked = false
			document.frmContratto.chRiservato.disabled = false
		}
	} else {
		if (!document.frmContratto.chRiservato.checked){
			document.frmContratto.txtMotivo.value = ""
			document.frmContratto.txtMotivo.disabled = true
		}else{
			document.frmContratto.txtMotivo.value = ""
			document.frmContratto.txtMotivo.disabled = false
		}
	}
		
	
}

	
function Controlli(){
	frmContratto.txtProt.value		= TRIM(frmContratto.txtProt.value)
	switch (document.frmContratto.Contratto.value){
		
		case "Assunzione":
			if (!document.frmContratto.txtMotivo.disabled)
				{
					document.frmContratto.txtMotivo.value = document.frmContratto.txtMotivo.value.toUpperCase()
				}
			var sTipoContratto;
			sTipoContratto = frmContratto.txtTipoContratto.value;
			if (sTipoContratto == "") {
				alert("Es necesario indicar la tipolog�a del contrato.");
				frmContratto.chkContratto[0].focus();
				return false;
			}

			if (frmContratto.txtDataDAl.value == "") {
				alert("Indicar la fecha de asunci�n.");
				frmContratto.txtDataDAl.focus();	
				return false;
			}else {
				if (frmContratto.txtDataDAl.value != "") {
					if (!ValidateInputDate(frmContratto.txtDataDAl.value)) {
						frmContratto.txtDataDAl.focus();	
						return false;
					}
				}
			}

			frmContratto.txtIscrMatr.value	= TRIM(frmContratto.txtIscrMatr.value)
			frmContratto.txtGrado.value		= TRIM(frmContratto.txtGrado.value)
			frmContratto.txtLivello.value	= TRIM(frmContratto.txtLivello.value)


			if (frmContratto.txtIscrMatr.value == "") {
				alert("Indicar el n�mero de matr�cula.");
				frmContratto.txtIscrMatr.focus();	
				return false;
			}

			if (frmContratto.txtFinoAl.value != "") {
				if (!ValidateInputDate(frmContratto.txtFinoAl.value)) {
					frmContratto.txtFinoAl.focus();	
					return false;
				}else{
					if (!ValidateRangeDate(frmContratto.txtDataDAl.value,frmContratto.txtFinoAl.value)) {
						alert("La fecha de fin del informe de trabajo debe ser superior a la de inicio.");
						frmContratto.txtFinoAl.focus();	
						return false;
					}
				}
			}
			if (frmContratto.cmbAreaProf.value == "") {
				alert("Indicar la categor�a de asunci�n.");
				frmContratto.cmbAreaProf.focus();	
				return false;
			}
			if (frmContratto.txtMansioni.value == "") {
				alert("Indicar la funci�n.");
				frmContratto.txtMansioni.focus();	
				return false;
			}
			if (frmContratto.txtTEconomico.value != "") {
				if (!IsNum(frmContratto.txtTEconomico.value)) {
					alert("Indicar un valor num�rico.");
					frmContratto.txtTEconomico.focus();	
					return false;
				}
			}
		
			if (frmContratto.chkLavDom.checked){
				if (frmContratto.txtIscLavDom.value == ""){
					alert("Indicar obligatoriamente el n�mero de inscripci�n al registro");
					frmContratto.txtIscLavDom.focus();
					return false;
				}
				if (frmContratto.txtIscLavDomDel.value == ""){
					alert("Indicar obligatoriamente la fecha de inscripci�n.");
					frmContratto.txtIscLavDomDel.focus();
					return false;
				}else {
					if (!ValidateInputDate(frmContratto.txtIscLavDomDel.value)){
						frmContratto.txtIscLavDomDel.focus();
						return false;
					}			
				}
				if (frmContratto.txtTipoLavorazione.value == ""){
					alert("Indicar el tipo de trabajo.");
					frmContratto.txtTipoLavorazione.focus();
					return false;
				}

			}
	
			if (frmContratto.cmbValuta.value == "" && frmContratto.txtTEconomico.value != "")	{
				alert ("Si se indica el trato econ�mico es necesario indicar tambi�n el valor");
				frmContratto.cmbValuta.focus();
				return false;
						
			}
			if (frmContratto.cmbValuta.value != "" && frmContratto.txtTEconomico.value == "")	{
				alert ("Si se indica el valor es necesario indicar tambi�n el trato econ�mico");
				frmContratto.txtTEconomico.focus();
				return false;
						
			}
			
				
			switch (sTipoContratto)	{
				case "09":
					if (frmContratto.txtDurata.value == ""){
						alert("Indicar la duraci�n del contrato.");
						frmContratto.txtDurata.focus();
						return false;
					}else {
						if (!IsNum(frmContratto.txtDurata.value)){
							alert("Indicar un valor num�rico.");
							frmContratto.txtDurata.focus();
							return false;
						}
					}
					frmContratto.txtLII.value = TRIM(frmContratto.txtLII.value)
					if (frmContratto.txtLII.value == ""){
						alert("Indicar el nivel de encuadre inicial.");
						frmContratto.txtLII.focus();
						return false;
					}
					frmContratto.txtLIF.value = TRIM(frmContratto.txtLIF.value)
					frmContratto.txtAutMinNum.value = TRIM(frmContratto.txtAutMinNum.value)
					if (frmContratto.txtAutMinNum.value == ""){
						alert("Indicar la autorizaci�n ministerial.");
						frmContratto.txtAutMinNum.focus();
						return false;
					}
					if (frmContratto.txtAutMinData.value == ""){
						alert("Indicar la fecha de autorizaci�n ministerial.");
						frmContratto.txtAutMinData.focus();
						return false;
					}else {
						if (!ValidateInputDate(frmContratto.txtAutMinData.value)){
							frmContratto.txtAutMinData.focus();
							return false;
						}			
					}
					frmContratto.txtAppNum.value = TRIM(frmContratto.txtAppNum.value)
					if (frmContratto.txtAppNum.value == ""){
						alert("Indicar la aprobaci�n de la comisi�n del departamento
						frmContratto.txtAppNum.focus();
						return false;
					}

					if (frmContratto.txtAppDel.value == ""){
						alert("Indicar la fecha de aprobaci�n de la comisi�n del departamento.");
						frmContratto.txtAppDel.focus();
						return false;
					}else {
						if (!ValidateInputDate(frmContratto.txtAppDel.value)){
							frmContratto.txtAppDel.focus();
							return false;
						}			
					}
					frmContratto.txtProgRif.value = TRIM(frmContratto.txtProgRif.value)
					if (frmContratto.txtProgRif.value == ""){
						alert("Indicar el acuerdo colectivo o proyectos de referencia.");
						frmContratto.txtProgRif.focus();
						return false;
					}
						
					if (frmContratto.txtDicDel.value == ""){
						alert("Indicar la fecha de declaraci�n.");
						frmContratto.txtDicDel.focus();
						return false;
					}else {
						if (!ValidateInputDate(frmContratto.txtDicDel.value)){
							frmContratto.txtDicDel.focus();
							return false;
						}			
					}
					break;
				case "07":

					frmContratto.txtIspLavoro.value		= TRIM(frmContratto.txtIspLavoro.value)
					frmContratto.txtCorsiPostDipl.value = TRIM(frmContratto.txtIspLavoro.value)
					frmContratto.txtTutoreNato.value	= TRIM(frmContratto.txtTutoreNato.value)
					frmContratto.txtTCognome.value		= TRIM(frmContratto.txtTCognome.value)
					frmContratto.txtTNome.value			= TRIM(frmContratto.txtTNome.value)
	
					if (frmContratto.txtIspLavoro.value == ""){
						alert("Indicar la autorizaci�n del inspector del trabajo.");
						frmContratto.txtIspLavoro.focus();
						return false;
					}
					if (frmContratto.txtILData.value == ""){
						alert("Indicar la fecha de autorizaci�n.");
						frmContratto.txtILData.focus();
						return false;
					}else {
						if (!ValidateInputDate(frmContratto.txtILData.value)){
							frmContratto.txtILData.focus();
							return false;
						}
					}
						
					if (frmContratto.txtVMData.value != ""){
						if (!ValidateInputDate(frmContratto.txtVMData.value)){
							frmContratto.txtVMData.focus();
							return false;
						}
					}
					if (frmContratto.txtDurataApp.value != ""){
						if (!IsNum(frmContratto.txtDurataApp.value)){
							alert("Indicar un valor num�rico.");
							frmContratto.txtDurataApp.focus();
							return false;
						}
					}
					if (frmContratto.txtOreCorsi.value != ""){
						if (!IsNum(frmContratto.txtOreCorsi.value)){
							alert("Indicar un valor num�rico.");
							frmContratto.txtOreCorsi.focus();
							return false;
						}
					}
					if (frmContratto.txtTutoreIl.value != ""){
						if (!ValidateInputDate(frmContratto.txtTutoreIl.value)){
							frmContratto.txtTutoreIl.focus();
							return false;
						}
					}
						
					if (frmContratto.txtTutoreCODFISC.value != ""){
						if (!ControllChkCodFisc(frmContratto.txtTutoreCODFISC.value)){
							alert("C�digo fiscal err�neo.");
							frmContratto.txtTutoreCODFISC.focus();
							return false;
						}
					}
					if (frmContratto.txtTutoreEspLav.value != ""){
						if (!IsNum(frmContratto.txtTutoreEspLav.value)){
							alert("El valor debe ser num�rico.");
							frmContratto.txtTutoreEspLav.focus();
							return false;
						}
					}
					break;
				default: 
			}

			scheck = "0"	
			sControllo = frmContratto.Controllore
			if (frmContratto.MODCASSAG.value == "S" && frmContratto.A != sControllo) {
				if (frmContratto.A.checked){
					scheck="1"
				}
				if (frmContratto.B.checked){
					scheck="1"
				}
				if (frmContratto.C.checked){
					scheck="1"
				}
				if (frmContratto.D.checked){
					scheck="1"
				}
				if (frmContratto.E.checked){
					scheck="1"
				}
				if (frmContratto.F.checked){
					scheck="1"
				}
				if (frmContratto.G.checked){
					scheck="1"
				}
				if (scheck == "0"){
					frmContratto.A.focus()
					alert("Es necesario indicar al menos un valor \n del modelo C/ASS/AG.")
					return false
				}
			}
			
			break;
		case "Cessazione":
			if (document.frmContratto.TipoCessazione.value == ""){
				alert("Es necesario seleccionar el tipo de cesaci�n.")
				frmContratto.chkCessazione[0].focus();
				return false;
			}
			if (document.frmContratto.txtDataCesDAl.value == ""){
				alert("Es necesario indicar la fecha de cesaci�n.")
				frmContratto.txtDataCesDAl.focus();
				return false;
			}else {
				if (!ValidateInputDate(frmContratto.txtDataCesDAl.value)){
					frmContratto.txtDataCesDAl.focus();
					return false;
				}			
			}
			if (document.frmContratto.txtDataCesAl.value == ""){
				alert("Es necesario indicar la fecha del informe de trabajo instaurado.")
				frmContratto.txtDataCesAl.focus();
				return false;
			}else {
				if (!ValidateInputDate(frmContratto.txtDataCesAl.value)){
					frmContratto.txtDataCesAl.focus();
					return false;
				}			
			}
			if (ValidateRangeDate(frmContratto.txtDataCesDAl.value,document.frmContratto.txtDataCesAl.value)) {
				alert("La fecha de cesaci�n no puede ser inferior/igual a la fecha de asunci�n.")
				frmContratto.txtDataCesDAl.focus();
				return false;
			}
			break;
			
		case "Trasformazione":
			if (frmContratto.txtDaContratto.value == "") 
			{
				alert("Es necesario indicar el contrato precedente.");
				frmContratto.chkContrattoDa[0].focus();
				return false;
			}
				
			if (frmContratto.txtAContratto.value == "") 
			{
				alert("Es necesario indicar el contrato precedente.");
				frmContratto.chkContrattoA[0].focus();
				return false;
			}
			if (frmContratto.txtAContratto.value == frmContratto.txtDaContratto.value) {
				alert("El contrato precende y el nuevo\contrato no pueden coincidir.");
				frmContratto.chkContrattoDa[0].focus();
				return false;
			}

			if (!frmContratto.ckDaOrPieno.checked) {
				if (frmContratto.txtDaOre.value == "") {
					alert("Si no se indica el tiempo lleno, \n indicar las horas parciales.")
					frmContratto.txtDaOre.focus();
					return false;
				}else {
					if (!IsNum(frmContratto.txtDaOre.value)) {
						alert("Indicar un valor num�rico.")
						frmContratto.txtDaOre.focus();	
						return false;
					}
				}
					
			}
			if (!frmContratto.ckaOrPieno.checked) {
				if (frmContratto.txtaOre.value == "") {
					alert("Si no se indica el tiempo lleno, \n indicar las horas parciales.")
					frmContratto.txtaOre.focus();
					return false;
				}else {
					if (!IsNum(frmContratto.txtaOre.value)) {
						alert("Indicar un valor num�rico.")
						frmContratto.txtaOre.focus();	
						return false;
					}
				}
			}
				
			if (document.frmContratto.txtDataTrasfDAl.value == ""){
				alert("Es necesario indicar la fecha de cesaci�n.")
				frmContratto.txtDataTrasfDAl.focus();
				return false;
			}else {
				if (!ValidateInputDate(frmContratto.txtDataTrasfDAl.value)){
					frmContratto.txtDataTrasfDAl.focus();
					return false;
				}			
			}

			if (document.frmContratto.txtDataTrasfAl.value == ""){
				alert("Es necesario indicar la fecha del informe de trabajo instaurado.")
				frmContratto.txtDataTrasfAl.focus();
				return false;
			}else {
				if (!ValidateInputDate(frmContratto.txtDataTrasfAl.value)){
					frmContratto.txtDataTrasfAl.focus();
					return false;
				}			
			}

			if (ValidateRangeDate(frmContratto.txtDataTrasfDAl.value,document.frmContratto.txtDataTrasfAl.value)) {
				alert("La fecha de transformaci�n no puede ser inferior/igual a la fecha de asunci�n.")
				frmContratto.txtDataTrasfDAl.focus();
				return false;
			}

			if (frmContratto.txtFinoAl.value != "") {
				if (!ValidateInputDate(frmContratto.txtFinoAl.value)) {
					frmContratto.txtFinoAl.focus();	
					return false;
				}else{
					if (ValidateRangeDate(frmContratto.txtFinoAl.value,document.frmContratto.txtDataTrasfDAl.value)){
						alert("La fecha de fin del informe no puede ser inferior/igual a la fecha de transformaci�n.")
						frmContratto.txtFinoAl.focus();
						return false;
					}
				
				}
			}

			if (frmContratto.cmbValuta.value == "" && frmContratto.txtTEconomico.value != "")	{
				alert ("Si se indica el trato econ�mico es necesario indicar tambi�n el valor");
				frmContratto.cmbValuta.focus();
				return false;
						
			}
			if (frmContratto.cmbValuta.value != "" && frmContratto.txtTEconomico.value == "")	{
				alert ("Si se indica el valor, es necesario indicar tambi�n el trato econ�mico");
				frmContratto.txtTEconomico.focus();
				return false;
						
			}				
		break;

	default:
		break;
	}
	return true;
}
function Abil(sCheckBox,sTag){
		if (frmContratto.item(sCheckBox).checked) {
			frmContratto.item(sTag).value = "";
			frmContratto.item(sTag).disabled = true;
		}else {
			frmContratto.item(sTag).value = "";
			frmContratto.item(sTag).disabled = false;
		}
		
	}
function VecchioContratto(nid)	{
		frmContratto.txtDaContratto.value = nid;
	}

function NuovoContratto(nid)	{
		frmContratto.txtAContratto.value = nid;
	}

function Cessazione(nTipoCess) {
	if (nTipoCess != 'Altro') {
		document.frmContratto.TipoCessazione.value = nTipoCess;
		document.frmContratto.txtCessazione.value = "";
		document.frmContratto.txtCessazione.disabled = true;
	}else {
		document.frmContratto.TipoCessazione.value = nTipoCess;
		document.frmContratto.txtCessazione.disabled = false;
	}
}

function SelContratto(sIdContratto)	{
	switch (sIdContratto){
		case "DOM":
			if (frmContratto.chkLavDom.checked){
				Domicilio.innerHTML = ' ' +
				'<table border="0" CELLPADDING="0" CELLSPACING="0" width="500" align="center">' +
				'	<tr>' +
				'		<TD class="tbltext1" WIDTH="215"><B>Inscripci�n registro n� *</TD>' +
				'		<TD><input class="textBlack" type="text" style="TEXT-TRANSFORM: uppercase;" name="txtIscLavDom"></TD>' +
				'	</tr>' +
				'	<tr>' +
				'		<TD class="tbltext1"><B>Del*</TD>' +
				'		<TD><input class="textBlack" type="text" style="TEXT-TRANSFORM: uppercase;" ' +
				'			name="txtIscLavDomDel" ' +
				'			size=12 maxlength=10></TD>' +
				'	</tr>' +
				'<tr> ' +
				'	<td class="tbltext1"><b>de horas medias semanales </b></td>' +
				'	<td>' +
				'		<input class="textBlack" type="text" style="TEXT-TRANSFORM: uppercase;" name="txtOreMedie"> ' +
				'	</td>' +
				'</tr>' +
				'	<tr>' +
				'		<TD class="tbltext1"><B>tipo de trabajo*</TD>' +
				'		<TD><input class="textBlack" type="text" style="TEXT-TRANSFORM: uppercase;" ' +
				'			name="txtTipoLavorazione" ' +
				'			size=12 ></TD>' +
				'	</tr></TABLE>' +
				'<table border="0" CELLPADDING="2" CELLSPACING="2" width="500" align="center">' +
				'   <tr height="2">' +
				'	  <td width="100%" ' +
				'		background="/Plavoro/images/separazione.gif"> ' +
				'	 </td>' +
				'  </tr>' +
				' </table><BR>'
			} else
			{
				Domicilio.innerHTML = ''
			}
		break;
			
    case "07": 

		frmContratto.txtTipoContratto.value = "07";
		Contratto.innerHTML =  ' ' +
		'<table border="0" CELLPADDING="0" CELLSPACING="0" width="500" align="center">' +
		'	<tr>' +
		'		<TD class="tbltext1" WIDTH="215"><B>Aut. Isp. Lav. de*</TD>' +
		'		<TD WIDTH="285"><input class="textBlack" type="text" style="TEXT-TRANSFORM: uppercase;" name="txtIspLavoro"></TD>' +
		'	</tr>' +
		'	<tr>' +
		'		<TD class="tbltext1"><B>Del*</B><BR>(dd/mm/aaaa)</TD>' +
		'		<TD><input class="textBlack" type="text" style="TEXT-TRANSFORM: uppercase;" ' +
		'			name="txtILData" ' +
		'			size=12 maxlength=10></TD>' +
		'	</tr>' +
		'	<tr>' +
		'		<TD class="tbltext1"><B>Visita m�dica con �xito positivo del*</B><BR>(dd/mm/aaaa)</TD>' +
		'		<TD><input class="textBlack" type="text" style="TEXT-TRANSFORM: uppercase;" ' +
		'			name="txtVMData" ' +
		'			size=12 maxlength=10></TD>' +
		'	</tr>' +
		'	<tr>' +
		'		<TD class="tbltext1"><B>Duraci�n de aprendizaje (meses)*</B><BR>(dd/mm/aaaa)</TD>' +
		'		<TD><input class="textBlack" type="text" style="TEXT-TRANSFORM: uppercase;" ' +
		'			name="txtDurataApp"	size=5 size=6></TD>' +
		'	</tr>' +
		'	<tr>' +
		'		<TD class="tbltext1"><B>Eventuales cursos post-diploma</B></TD>' +
		'		<TD><input class="textBlack" type="text" style="TEXT-TRANSFORM: uppercase;" ' +
		'			name="txtCorsiPostDipl"></TD>' +
		'	</tr>' +
		'	<tr>' +
		'		<TD class="tbltext1"><B>De horas</B></TD>' +
		'		<TD><input class="textBlack" type="text" style="TEXT-TRANSFORM: uppercase;" ' +
		'			name="txtOreCorsi" size=5 maxlength=6></TD>' +
		'	</tr>' +
		'	<tr>' +
		'		<TD class="tbltext1"><B>Apellido Tutor</B></TD>' +
		'		<TD><input class="textBlack" type="text" style="TEXT-TRANSFORM: uppercase;" ' +
		'			name="txtTCognome"></TD>' +
		'	</tr>' +
		'	<tr>' +
		'		<TD class="tbltext1"><B>Nombre Tutor</B></TD>' +
		'		<TD><input class="textBlack" type="text" style="TEXT-TRANSFORM: uppercase;" ' +
		'			name="txtTNome"></TD>' +
		'	</tr>' +
		'	<tr>' +
		'		<TD class="tbltext1"><B>el</B><BR>(dd/mm/aaaa)</TD>' +
		'		<TD><input class="textBlack" type="text" style="TEXT-TRANSFORM: uppercase;" ' +
		'			name="txtTutoreIl" size=12 maxlength=10></TD>' +
		'	</tr>' +
		'	<tr>' +
		'		<TD class="tbltext1"><B>Nacido en</B></TD>' +
		'		<TD><input class="textBlack" type="text" style="TEXT-TRANSFORM: uppercase;" ' +
		'			name="txtTutoreNato"></TD>' +
		'	</tr>' +
		'	<tr>' +
		'		<TD class="tbltext1"><B>C.I.</B></TD>' +
		'		<TD><input class="textBlack" type="text" style="TEXT-TRANSFORM: uppercase;" ' +
		'			name="txtTutoreCODFISC" size=17 maxlength=8></TD>' +
		'	</tr>' +
		'	<tr>' +
		'		<TD class="tbltext1"><B>Titular  (S/N)</B></TD>' +
		'		<TD><INPUT type="radio" name=rTitolare value="Si">' +
		'			<INPUT type="radio" name=rTitolare value="No" checked>' +
		'		</TD>' +
		'	</tr>' +
		'	<tr>' +
		'		<TD class="tbltext1"><B>Experiencia laboral en el sector de a�os</B></TD>' +
		'		<TD><input class="textBlack" type="text" style="TEXT-TRANSFORM: uppercase;" ' +
		'			name="txtTutoreEspLav"></TD>' +
		'	</tr>' +
		'	<tr>' +
		'		<TD class="tbltext1"><B>T�tulo</B></TD>' +
		'		<TD><input class="textBlack" type="text" style="TEXT-TRANSFORM: uppercase;" ' +
		'			name="txtTutoreQualifica"></TD>' +
		'	</tr>' +
		'	<tr>' +
		'		<TD class="tbltext1"><B>Nivel</B></TD>' +
		'		<TD><input class="textBlack" type="text" style="TEXT-TRANSFORM: uppercase;" ' +
		'			name="txtTutoreLivello"></TD>' +
		'	</tr>' +
		'</table>'
		break;

	case "09":
		frmContratto.txtTipoContratto.value = "09";
		Contratto.innerHTML =  ' ' +
		'<table border="0" CELLPADDING="0" CELLSPACING="0" width="500" align="center">' +
		'	<tr>' +
		'		<TD class="tbltext1" WIDTH="215"><B>Tipo de profesionalismo(1,2,3)</TD>' +
		'		<TD WIDTH="285" ><INPUT checked type=radio name=tipoProfes value="1"><INPUT  type=radio name=tipoProfes value="2"><INPUT type=radio name=tipoProfes value="3"></TD>' +
		'	</tr>' +
		'	<tr>' +
		'		<TD class="tbltext1"><B>Duraci�n meses*</TD>' +
		'		<TD><input class="textBlack" type="text" style="TEXT-TRANSFORM: uppercase;" ' +
		'			name="txtDurata" ' +
		'			size=6 maxlength=5></TD>' +
		'	</tr>' +
		'	<tr>' +
		'		<TD class="tbltext1"><B>Nivel de encuadramiento inicial*</TD>' +
		'		<TD><input class="textBlack" type="text" style="TEXT-TRANSFORM: uppercase;" ' +
		'			name="txtLII" ' +
		'			></TD>' +
		'	</tr>' +
		'	<tr>' +
		'		<TD class="tbltext1"><B>Nivel del encuadramiento final*</B></TD>' +
		'		<TD><input class="textBlack" type="text" style="TEXT-TRANSFORM: uppercase;" ' +
		'			name="txtLIF"></TD>' +
		'	</tr>' +
		'	<tr>' +
		'		<TD class="tbltext1"><B>Autorizaci�n del ministerio n�*</B></TD>' +
		'		<TD><input class="textBlack" type="text" style="TEXT-TRANSFORM: uppercase;" ' +
		'			name="txtAutMinNum"></TD>' +
		'	</tr>' +
		'	<tr>' +
		'		<TD class="tbltext1"><B>Del*</B><BR>(dd/mm/aaaa)</TD>' +
		'		<TD><input class="textBlack" type="text" style="TEXT-TRANSFORM: uppercase;" ' +
		'			name="txtAutMinData" size=12 maxlength=10></TD>' +
		'	</tr>' +
		'	<tr>' +
		'		<TD class="tbltext1"><B>Aprobaci�n de Comisi�n Prov. n�*</B></TD>' +
		'		<TD><input class="textBlack" type="text" style="TEXT-TRANSFORM: uppercase;" ' +
		'			name="txtAppNum"></TD>' +
		'	</tr>' +
		'	<tr>' +
		'		<TD class="tbltext1"><B>Del*</B><BR>(dd/mm/aaaa)</TD>' +
		'		<TD><input class="textBlack" type="text" style="TEXT-TRANSFORM: uppercase;" ' +
		'			name="txtAppDel" size=12 maxlength=10></TD>' +
		'	</tr>' +
		'	<tr>' +
		'		<TD class="tbltext1"><B>Acuerdo colectivo o proy. de ref.*</B></TD>' +
		'		<TD><input class="textBlack" type="text" style="TEXT-TRANSFORM: uppercase;" ' +
		'			name="txtProgRif"></TD>' +
		'	</tr>' +
		'	<tr>' +
		'		<TD class="tbltext1"><B>Declaraci�n de conformidad del*</B><BR>(dd/mm/aaaa)</TD>' +
		'		<TD><input class="textBlack" type="text" style="TEXT-TRANSFORM: uppercase;" ' +
		'			name="txtDicDel" size=12 maxlength=10></TD>' +
		'	</tr>' +
		'</Tabke><table border="0" CELLPADDING="0" CELLSPACING="0" width="500" align="center">' +
		'	<tr>' +
		'		<TD width=25><input class="textBlack" type="checkbox" ' +
		'			name="chk2460" value="Si"></TD>' +
		'		<TD width=485 class="tbltext1"><B>En los 24 meses precedentes han sido transformados a tiempo indeterminado no menos del 60% de los cfl venidos a vencimiento en el mismo per�odo.</B></TD>' +
		'	</tr>' +
		'	<tr>' +
		'		<TD colspan=2 class="tbltext1">&nbsp;</TD>' +
		'	</tr>' +
		'	<tr>' +
		'		<TD width=25><input class="textBlack" type="checkbox" ' +
		'			name="chkDichiaraz" value="Si"></TD>' +
		'		<TD width=485 class="tbltext1"><B>Declara tambi�n, que hay suspensiones del trabajo en el acto, ni, en 12 meses, son reducciones de personal con el mismo t�tulo.</B></TD>' +
		'	</tr>' +
		'</table>'
	break;
    default: 
		frmContratto.txtTipoContratto.value = "X";
		if (frmContratto.MODCASSAG.value == "S") {
			Contratto.innerHTML =  ' ' +
			'<table border="0" CELLPADDING="0" CELLSPACING="0" width="500" align="center">' +
			'	<tr>' +
			'		<TD colspan=2 class="textBlack"><B>A fin de obtener la concesi�n de las facilitaciones previstas en las normas vigentes se comunica que la presente asunci�n se encuentra entre aquellas al pie de la letra :<B></TD>' +
			'	</tr>' +
			'	<tr>' +
			'		<TD colspan=2 class="tbltext1">&nbsp;</TD>' +
			'	</tr>' +
			'	<tr>' +
			'		<TD WIDTH="25"><INPUT type=checkbox name=A value="Si"></TD>' +
			'		<TD class="tbltext1" > <B>A - Trabajador inscripto en la lista de movilidad de la regi�n &nbsp;&nbsp;<Input maxlength=20 class="textBlack" size=20 type="text" style="TEXT-TRANSFORM: uppercase;" name=txtregione> (Art. 8-25 L.23/91)<B></TD>' +
			'	</tr>' +
			'	<tr>' +
			'		<TD WIDTH="25"><INPUT type=checkbox name=B value="Si"></TD>' +
			'		<TD class="tbltext1" > <B>B - Trabajador proveniente de empresa en GIGS de al menos 6 meses y que ha gozado del trato para al menos 3 meses no continuos del art. 4 inciso 3 L.236/93</B></TD>' +
			'	</tr>' +
			'	<tr>' +
			'		<TD WIDTH="25"><INPUT type=checkbox name=C value="Si"></TD>' +
			'		<TD class="tbltext1" > <B>C - Trabajador suspendido en GIGS de m�s de 24 meses (art.8 inciso 9 L.407/1990)</B></TD>' +
			'	</tr>' +
			'	<tr>' +
			'		<TD WIDTH="25"><INPUT type=checkbox name=D value="Si"></TD>' +
			'		<TD class="tbltext1" > <B>D - Trabajador inscrito en la primera clase de las listas de colocaci�n de m�s de 24 meses (art.8 inciso 9 L.407/1990)</B></TD>' +
			'	</tr>' +
			'	<tr>' +
			'		<TD WIDTH="25"><INPUT type=checkbox name=E value="Si"></TD>' +
			'		<TD class="tbltext1" > <B>E - Trabajador en posesi�n de diploma de instituto profesional de estado o atestado de t�tulo regional (art.22 L.56/87)</B></TD>' +
			'	</tr>' +
			'	<tr>' +
			'		<TD WIDTH="25"><INPUT type=checkbox name=F value="Si"></TD>' +
			'		<TD class="tbltext1" > <B>F - Trabajadores desfavorecidos de asuntos de cooperativas sociales (art. 4 L.381/1991)</B></TD>' +
			'	</tr>' +
			'	<tr>' +
			'		<TD WIDTH="25"><INPUT type=checkbox name=G value="Si"></TD>' +
			'		<TD class="tbltext1" > <B>G - Otra hip�tesis : <input class="textBlack" size=40 type="text" style="TEXT-TRANSFORM: uppercase;" ' +
			'			name="txtIpotesi"></B></TD>' +
			'	</tr>' +
			'	<tr>' +
			'		<TD colspan=2 class="textBlack">&nbsp;</TD>' +
			'	</tr>' +
			'	<tr>' +
			'		<TD colspan=2 class="textBlack"><B>Se declara, tambi�n, que tal asunci�n no es efectuada en lugar de empleados : </B></TD>' +
			'	</tr>' +
			'	<tr>' +
			'		<TD WIDTH="25"><INPUT checked type=radio name=Sospesi value="Sospesi"></TD>' +
			'		<TD class="tbltext1"> <B> Suspendidos en CIGS</B></TD>' +
			'	</tr>' +
			'	<tr>' +
			'		<TD WIDTH="25"><INPUT type=radio name=Sospesi value="Licenziati"></TD>' +
			'		<TD class="tbltext1"> <B>Despedidos en los �ltimos 12 meses por reducci�n de personal por cualquier causa (L.407/1990)</B></TD>' +
			'	</tr>' +
			'</table>'
		}else { 
			Contratto.innerHTML =  ' ';
		}
	}
		
}
