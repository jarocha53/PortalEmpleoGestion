<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include virtual = "/util/portallib.asp"-->
<!--#include Virtual = "/strutt_testa2.asp"-->
<!--#include Virtual = "/include/SysFunction.asp"-->
<!--#include Virtual = "/Include/openconn.asp"-->
<!--#include Virtual = "/Include/ControlDateVB.asp"-->
<!--#include Virtual = "/include/DecCod.asp"-->
<!--#include Virtual = "/include/MuestraFiltros.asp"-->


<!--#include virtual = "/include/ImpostaProgetto.asp"-->
<!--#include Virtual = "/include/MuestraFiltros.asp"-->

<%
''''************************************************************************************''''
''''************MODIFICADO PARA UNIFICAR AVISOS Y SELECCIONES*******VANY****************''''
''''************************************************************************************''''
%>

<%
' ============================================================================================
' VISUALIZACION Y MODIFICACION DE SELECCIONES
'
'
'
'
'
'
'
'
'
'
'
' JavaScript														  40
' Main																 240
' Reglas de Modificacion											 540
' Postulantes, Candidatos, busqueda									1400
' sub INICIO 														1540
'
'
'
'
'
'============================================================================================
if Session("idgruppo") = 37 then
%>
	<script type="text/javascript">
		$(function(){
			//var CmbAmbito = document.getElementsByName("CmbAmbito");
			$("[name=CmbAmbito] option[selected]").html()

			//$(CmbAmbito).attr('disabled', 'disabled');

			$("#trAmbitoTerritorial").hide();
		});
	</script>
<%
end if
%>

<script LANGUAGE="Javascript">

	$(function(){

		$("#falseTodoelPais").attr("checked", true);

		$("#falseTodoelPais").change(function(){
			$("#falseTodoelPais").attr("checked", true);
			$("#trueTodoelPais").attr("checked", false);

			var checked = $("#falseTodoelPais").attr('checked');			
			if($("#falseTodoelPais").attr('checked')) {
		    	$('#cmbDepartamento').removeAttr('disabled');
		    	$("#txtMunicip").removeAttr('disabled');
			}
		});

		$("#trueTodoelPais").change(function(){
			$("#falseTodoelPais").attr("checked", false);
			$("#trueTodoelPais").attr("checked", true);

			var checked = $("#trueTodoelPais").attr('checked');
			if($("#trueTodoelPais").attr('checked')) {
				$('#cmbDepartamento').attr('disabled', 'disabled');
				$("#txtMunicip").attr('disabled', 'disabled');

				$('#cmbDepartamento').val("");
				$("#txtMunicip").val("");
				$("#cmbMunicipio").val("");
			}
		});

	});

<!--#include virtual = "/include/SelComune.js"-->

<!--#include virtual = "/Include/ControlDate.inc"-->
<!--#include virtual = "/Include/ControlNum.inc"-->
<!--#include virtual = "/Include/help.inc"-->

function VerCandidatosPorEstado(estado)
{
    document.FormCandidati.estado.value = estado;
    document.FormCandidati.submit();
}

function PulisciCom()
{
	document.FrmPrincipale.txtMunicip.value = ""
	document.FrmPrincipale.cmbMunicipio.value = ""

}
function Enviar()
{
	document.FrmPrincipale.action = 'BUS_Confirma.asp';

	if (Validate(FrmPrincipale))
	{
		document.FrmPrincipale.submit();
	}
}


function Enviar2()
{
	document.FormCandidati.action = 'BUS_Confirma.asp';

	document.FormCandidati.submit();

}


function CnfRicerca()
{
	document.FrmAR2.submit();
	return true;
}

function VisCandidati()
{
	document.frmVisCand.submit();
}

function Validate(TheForm)
{
	//CmbAmbito

	//alert(parseInt(TheForm.TxtAmbitoBase.value) );
	//alert(parseInt(TheForm.CmbAmbito.value));
	//alert(parseInt(TheForm.Tcandidatos.value));

	//vani
	if (parseInt(TheForm.TxtAmbitoBase.value) < parseInt(TheForm.CmbAmbito.value))
	{
		if (parseInt(TheForm.Tcandidatos.value) != 0)
		{
			alert("No puede seleccionar un �mbito territorial menor al ya ingresado")
			return false;
		}
	}



	if (typeof(TheForm.PersDisc) == 'checkBox')
	{
		if (TheForm.PersDisc[0].checked == true)
		{
			if (TheForm.cmbDisc.value == "")
			{
				alert("Si eligi� la opci�n SI en \n'El cargo solicitado admite postulantes con algn tipo de discapacidad' \ndebe elegir un tipo de discapacidad");
				return false;
			}
		}
	}

	if (typeof(TheForm.Obs_Disc) != 'undefined')
	{
		if (TheForm.Obs_Disc.value != "")
		{
			if (TheForm.Obs_Disc.value.length > 400)
			{
				alert("El campo observaciones no debe superar los 400 caracteres");
				return false;
			}
		}
	}

	if (TheForm.txtNPosti.value == "")
		{
			alert("El campo n�mero de puestos es obligatoro!");
			TheForm.txtNPosti.focus();
			return false;
		}
	if (TheForm.cmbMunicipio.value == "" && TheForm.cmbDepartamento.value != "")
		{
			alert("Si seleccion� un Departamento, debe seleccionar tambi�n un Municipio!");
			TheForm.cmbDepartamento.focus();
			return false;
		}
	 if (!Numerico(TheForm.txtNPosti))
		{
			return false;
		}
	 if (TheForm.txtNPosti.value==0)
		{
			alert("Ingresar un valor superior a cero.");
			TheForm.txtNPosti.focus();
			return false;
		}

	if($("#falseTodoelPais").attr('checked')) {
		if($("#cmbDepartamento").val() == ""){
			alert("El campo Departamento es obligatorio!");	
			$("#cmbDepartamento").focus();
			return false
		}
	}

	if (TheForm.cmbMunicipio.value == "" && TheForm.cmbDepartamento.value != "")
	{
		alert("Si seleccion� un Departamento, debe seleccionar tambi�n un Municipio!");
		TheForm.cmbMunicipio.focus();
		return false;
	}


	//if (TheForm.txtDtDal.value == "")
	//	{
	//		alert("El campo fecha de inicio de validez es obligatorio!");
	//		TheForm.txtDtDal.focus();
	//		return false;
	//	}
	//if(!ValidateRangeDate(TheForm.txtOggi.value,TheForm.txtDtDal.value))
	//	{
	//		alert("La data di inizio validit deve essere uguale o successiva alla data odierna!")
	//		TheForm.txtDtAl.focus();
	//		return false;
	//	}

	if (TheForm.txtDtAl.value == "")
	{
		alert("El campo env�o de candidatos de postulantes es obligatorio!");
		TheForm.txtDtAl.focus();
		return false;
	}

	if(!ValidateRangeDate(TheForm.txtOggi.value,TheForm.txtDtAl.value))
	{
		alert("El Plazo env�o de candidatos debe ser superior a la fecha actual!")
		TheForm.txtDtAl.focus();
		return false;
	}
	//Controllo delle date e dell'intervallo
	/* if(!intervallo(TheForm.txtDtDal,TheForm.txtDtAl))
		{
			return false;
		}
	*/
	//Controllo altri campi



	if (TheForm.txtDtAvv.value != "")
	{
		if (!ValidateInputDate(TheForm.txtDtAvv.value))
			{
				TheForm.txtDtAvv.focus();
				return false;
			}
		/*if(!ValidateRangeDate(TheForm.txtDtDal.value,TheForm.txtDtAvv.value))
		{
			alert("La fecha de solicitud debe ser superior a la fecha de inicio de vigencia!")
			TheForm.txtDtAvv.focus();
			return false;
		}	*/
	}
	else
	{
	         alert("Ingresar Fecha de incorporacin/Fecha de Inicio")
	         TheForm.txtDtAvv.focus();
	         return false;
	}


	// control del ingreso de duracion vacia - >se almacena en 0
	if (TheForm.txtDuracion.value == "")
	{
		TheForm.txtDuracion.value = 0
	}
	//alert("Pasa Por Aca")
	if (TheForm.cmbTipoDuracion.value == "")
	{
		TheForm.cmbTipoDuracion.value = "M"
	}

	if (typeof(TheForm.ATTIVITA) != 'undefined')
	{
		if (TheForm.ATTIVITA.value == "")
		{
			alert("Debe ingresar la Actividad Principal");
			return false;
		}
	}
	if (TheForm.TESTO.value == "")
	{
	   	alert("Debe ingresar la Descripci�n de la Vacante");
	   	return false;
	}


	//if (TheForm.sP.value == "N" && "25" == "<%=session("idgruppo")%>")
	if (TheForm.sP.value == "N" && ("25" == "<%=session("idgruppo")%>" || ("37" == "<%=session("idgruppo")%>" && TheForm.pubdirecta.value == "S")))
	{
		//alert("session('creator')= " + <%=session("creator")%>);
		//alert("centroAsoc= "+ TheForm.centroAsoc.value);
		if (TheForm.centroAsoc.value == "<%=session("creator")%>"  || "37" == "<%=session("idgruppo")%>")
		{
		   	if (confirm("Esta acci�n publicar� esta vacante. Quiere seguir adelante?"))
	         {
				
				document.frmEnvMailEmp.submit();
	         	window.onunload=function()
	         	{
	         	    window.Open(EmpresarioPublicaVacante('jhamon@mintrabajo.gov.co'));
	         	}


	        }else{
	      	   return false
	         }
	     } else{
		   	alert("�Acci�n no permitida!\n �El empresario no se encuentra asociado a su Centro de Atenci�n!");
		   	return false;	     	
	     }

    }else{
        if (TheForm.sP.value == "N") {
    	   	alert("!La publicaci�n de esta vacante entra al proceso de evaluaci�n por parte de su Centro de Atenci�n!");
        }
        
	}
	 		//alert("Pasa por aca")

	return true;
}

function Numerico(campo)
{
	var dim=campo.size;
	var anyString = campo.value;

	for ( var i=0; i<=anyString.length-1; i++ )
	{
		if ( (anyString.charAt(i) >= "0") && (anyString.charAt(i) <= "9") )
		{
		}
		else
		{
			alert("Atenci�n: Valor ingresado no numerico!");
			campo.focus();
			return(false);
		}
	}
	return true;
}

function intervallo(dtDal,dtAl)
	{
		if (!ValidateInputDate(dtDal.value))
 			{
				dtDal.focus();
				return false;
			}
		if (!ValidateInputDate(dtAl.value))
 			{
				dtAl.focus();
				return false;
			}
		if (!ValidateRangeDate(dtDal.value, dtAl.value))
			{
				dtDal.focus();
				alert("Controlar el intervalo de fechas ingresado.");
				return false;
			}
		return true;
}

function EnLoad()
{
	if (typeof(document.PersDisc) == 'checkBox')
	{
		if (document.FrmPrincipale.PersDisc[0].checked==true)
		{
		document.FrmPrincipale.cmbDisc.disabled = false;
		}
		else
		{
		document.FrmPrincipale.cmbDisc.value = "";
		document.FrmPrincipale.cmbDisc.disabled = true;
		}
	}
}
function HabDes(Valor)
{
	if (Valor == "S")
	{
	document.FrmPrincipale.cmbDisc.disabled = false;
	}
	else
	{
	document.FrmPrincipale.cmbDisc.value = "";
	document.FrmPrincipale.cmbDisc.disabled = true;
	}
}

function AlertGrupo()
{ 
   // alert (FrmPrincipale.cmbTipoRicerca.value );
	if (FrmPrincipale.cmbTipoRicerca.value == "0")
	{
    	alert("�El 'tipo de b�squeda' seleccionado permite la postulaci�n de todos los candidatos que pertenecen al 'Grupo Ocupacional'. El perfil de algunos candidatos podr�a ser lejano a lo que ud. desea encontrar. \n\nNO SER� POSIBLE CAMBIAR EL TIPO DE B�SQUEDA!");
		return false;
	}
}
</script>

<script language="javascript" src="Controlli.js">
</script>
<script language="javascript" src="ControlliBus.js">
</script>





<%
'********************************************************************************************


	Dim sModo, nIdRic
	Dim RRr, sqlr, sIdSede
	Dim sNombrePrestacion, seleccion
	Dim TAviso
	Dim nTipoProf
	Dim sTipoArea
	Dim sTipoProf
	Dim sDtDal
	Dim sDtAl
	Dim nPosti
	Dim nTipoArea
	Dim sDtAvv, TEmp
	Dim OFFER_TITLE 


	if Request.Form ("ESSEL") <> "" then
		sErrore = Rifiuta()
	end if

	sModo = Request("Modo")
	nIdRic= Request("IdRic")

	''vani adaptacion...
	if nIdRic = "" then
		nIdRic = request("Codice")
	end if

	sDt_tmst = Request("dtTmst")

	sIdsede=Request("Idsede")

	'if sIdSede = "" then
	'	sIdSede = session("Creator")
	'end if

	TEmp = request("TEmp")

	Tipo = request("Tipo")

'Response.Write "Tipo: " & Tipo

	ModificaProyecto = "S"

	if TEmp = "E" then
		sRol = 1
	elseif TEmp = "P" then
		sRol = 2
	end if

	'Response.Write "nIdRic " & nIdRic & "<br>"
	'Response.Write "TEmp: " & TEmp & "<br>"
	'Response.Write "sdt_tmst " & sdt_tmst & "<br>"
	'Response.Write "IdSede " & sIdSede & "<br>"
	'Response.Write "prima " & Request.Form ("prima")
	'Response.End 

%>
<!--#include virtual="/include/VerificaProyectos.asp"-->
<%
	ModificaProyecto = VieneDeProyectos (nIdRic)
	Imposta_Pag()

	'********************************
	'********************************
	'********************************
	'********************************
	'********************************
	'********************************
	'********************************
	'********************************

	Sub Imposta_Pag()
		Dim sql, RR
		Dim i
		Dim nTipoRic, nTipoRicerca, sTipoArea, sTipoProf, nPosti, sDtDal, sDtAl, sDtAvv, nMunicipio

		sqlr = "select COD_TIPO_RICHIESTA,OFFER_TITLE,ID_AREAPROF,ID_FIGPROF, DT_INGRESO, gecal, programa, proyecto, ID_SEDE, MUNICIPIO, " &_
				" DT_FINPUBLICACION, DT_INCORPORACION, NRO_VACANTES, DT_TMST, PERS_CARGO, PERS_DISC, PERS_TIPO_DISC, PERS_DISC_OBS, FL_PUBBLICATO,STAT_LAV,ID_SETTORE,ACTIVIDADEMPRESA,TEXTOANUNCIO,TIPOANUNCIO, GECAL, PROGRAMA, PROYECTO, DIRECCION, DURACION, DURACIONUMEDIDA, COD_TIPO_RICERCA, SERVICIO, TIPOAMBITO, COD_OFICINA, COD_PROVINCIA " &_
				"  from richiesta_sede where id_richiesta=" & nIdRic
		set RRr = Server.CreateObject("ADODB.Recordset")
		'Response.Write sqlr
		on error resume next
		Err.Clear
		'PL-SQL * T-SQL
		'SQLR = TransformPLSQLToTSQL (SQLR)
		RRr.Open sqlr,CC,1,2
		if err.number <> 0 then
			Messaggio("Ha ocurrido un error." & Err.Description)
			Indietro()
			err.Clear
			exit sub
		end if

		on error goto 0
		if not RRr.EOF then
			nTipoRic	= RRr.Fields("cod_tipo_richiesta")
			'' ******************** IPTECNOLOGIES 			 
			OFFER_TITLE	= RRr.Fields("OFFER_TITLE")
    		'' ********************************
			nTipoArea	= RRr.Fields("id_areaprof")
			nTipoProf	= RRr.Fields("id_figprof")
			nMunicipio	= RRr.Fields("MUNICIPIO")
			nPosti		= RRr.Fields("NRO_VACANTES")
			sDtDal		= ConvDateToString(RRr.Fields("dt_INGRESO"))
			sDtAl		= ConvDateToString(RRr.Fields("DT_FINPUBLICACION"))
			nTipoRicerca = RRr.Fields("COD_TIPO_RICERCA")
			sPubblica = RRr.Fields("FL_PUBBLICATO")
			'_____________________________________

			sPersCargo = RRr("Pers_Cargo")
			sPersDisc = RRr("Pers_Disc")
			sPersTipoDisc = RRr("Pers_Tipo_Disc")
			sDiscObs = RRr("Pers_Disc_Obs")
			'_____________________________________

			sDireccion = RRr("direccion")

			TEmp = request("TEmp")
			'Response.Write "TEmp: " & TEmp & "<br>"

			'******************************** MUESTRA EL AMBITO TERRITORIAL
			'vani
			NTIPOAMBITO = RRr("TIPOAMBITO")
			NCODOFICINA = RRr("COD_OFICINA")
			NCODPROVINCIA = RRr("COD_PROVINCIA")
			'**************************************************************

			Gecal = RRr("Gecal")
			Programa = RRr("Programa")
			Proyecto = RRr("Proyecto")
			sSede = RRr("ID_SEDE")



			'''PRESTACION
			sTipoPrestacion =  RRr("SERVICIO")
			'''PRESTACION

			TANUNCIO = RRr("TIPOANUNCIO")
			ATTIVITA = RRr("ACTIVIDADEMPRESA")
			TESTO = RRr("TEXTOANUNCIO")

			set RSettore = server.CreateObject("adodb.recordset")


            VariableSQL = "select DENOMINAZIONE from settori where id_settore = " & RRr("id_settore")
            'PL-SQL * T-SQL
            'VariableSQL = TransformPLSQLToTSQL(VariableSQL)

			set RSettore = cc.execute(VariableSQL)
			if not RSettore.EOF then
				sSettoreDesc = RSettore("DENOMINAZIONE")
			end if

			set RSettore = nothing

			if not IsNull(RRr.Fields("dt_incorporacion")) then
				sDtAvv	= ConvDateToString(RRr.Fields("DT_INCORPORACION"))
			else
				sDtAvv = ""
			end if
			sDT_Tmst	= RRr.Fields("dt_tmst")
			'RRr.Close
			'set RRr = nothing
		else
			Messaggio("Error en la visualizacin de la pagina")
			exit sub
		end if
		'VANI 14-06-06''''
		dim rsempr
		set rsempr = server.CreateObject("ADODB.recordset")
		IF UCASE(TANUNCIO) = "L" THEN
				SQLEMPR = ""
				SQLEMPR = SQLEMPR & "SELECT ACSE.DENOMINAZIONE AREE_PROF_DESC, AC.DENOMINAZIONE FIG_PROF_DESC "
				SQLEMPR = SQLEMPR & "FROM RICHIESTA_SEDE ASE, "
				SQLEMPR = SQLEMPR & "AREE_PROFESSIONALI ACSE, "
				SQLEMPR = SQLEMPR & "FIGUREPROFESSIONALI AC "
				SQLEMPR = SQLEMPR & "where ACSE.id_areaprof = ASE.ID_AREAPROF "
				SQLEMPR = SQLEMPR & "and AC.id_figprof = ASE.ID_FIGPROF "
				SQLEMPR = SQLEMPR & "and ACSE.ID_AREAPROF = AC.ID_AREAPROF "
				SQLEMPR = SQLEMPR & "and ASE.id_richiesta =" & nIdRic
		ELSEIF UCASE(TANUNCIO) = "C" THEN
				SQLEMPR = ""
				SQLEMPR = SQLEMPR & "SELECT DENOMINAZIONE AREE_PROF_DESC, DESCRIPCION FIG_PROF_DESC "
				SQLEMPR = SQLEMPR & "FROM RICHIESTA_SEDE ASE,      "
				SQLEMPR = SQLEMPR & "AREA_CORSO_SUB ACSE,         "
				SQLEMPR = SQLEMPR & "AREA_CORSO AC                "
				SQLEMPR = SQLEMPR & "where ASE.id_areaprof = AC.id_area   "
				SQLEMPR = SQLEMPR & "and ASE.id_figprof = ACSE.id_subarea "
				SQLEMPR = SQLEMPR & "and ACSE.id_area = AC.id_area        "
				SQLEMPR = SQLEMPR & "and ASE.id_richiesta =" & nIdRic
		END IF
		'Response.Write sqlempr
'PL-SQL * T-SQL
'SQLEMPR = TransformPLSQLToTSQL (SQLEMPR)
		set rsempr = cc.execute(sqlempr)
		if not rsempr.EOF then
			sTipoArea = rsempr("AREE_PROF_DESC")
			if (not isnull(sTipoArea)) and (sTipoArea <> "") then
				sTipoArea = ucase(sTipoArea)
			end if
			sTipoProf = rsempr("FIG_PROF_DESC")
			if (not isnull(sTipoProf)) and (sTipoProf <> "") then
				sTipoProf = ucase(sTipoProf)
			end if
		end if
		'VANI 14-06-06''''

		'si ricerca la descrizione dell'area professionale
		'sqlDecod = "select denominazione from aree_professionali where id_areaprof=" & nTipoArea

'PL-SQL * T-SQL
'SQLDECOD = TransformPLSQLToTSQL (SQLDECOD)
		'set RRDecod = CC.Execute(sqlDecod)
		'
		'sTipoArea = RRDecod.Fields(0)
		'RRDecod.Close
		'set RRDecod = nothing
		'sqlDecod = ""
		'si ricerca la descrizione della figura professionale
		'sqlDecod = "select denominazione from figureprofessionali where id_figprof=" & nTipoProf
		'
'PL-SQL * T-SQL
'SQLDECOD = TransformPLSQLToTSQL (SQLDECOD)
		'set RRDecod = CC.Execute(sqlDecod)
		'
		'sTipoProf = RRDecod.Fields(0)
		'RRDecod.Close
		'set RRDecod = nothing
		'sqlDecod = ""

		'if (cdate(ConvStringToDate(sDtAl)) => date) then

		'Response.Write TEmp


		if sTipoPrestacion <> "" then
			set RsPrestacion = server.CreateObject("adodb.recordset")

			if sRol <> "" then
				consultasPrest = "select avisotipo, avisoformato from impresa_rolesserviciostipos where rol = " & sRol & " and servicio = " & sTipoPrestacion
			else
				consultasPrest = "select avisotipo, avisoformato from impresa_rolesserviciostipos where servicio = " & sTipoPrestacion
			end if

'PL-SQL * T-SQL
'CONSULTASPREST = TransformPLSQLToTSQL (CONSULTASPREST)
		set RsPrestacion = cc.execute(consultasPrest)

		if RsPrestacion("avisotipo") = 1 then
			TAviso = "L"
		else
			TAviso = "C"
		end if

		if cint(RsPrestacion("avisoformato")) = 1 then
			FormatoEtiquetas="L"
		else
			FormatoEtiquetas="C"
		end if




	set RsPrestacion = nothing

	if sRol <> "" then
		SqlRedireccionar = ""
		SqlRedireccionar = SqlRedireccionar & "select esareaprofesional,esareacorso from impresa_rolesserviciostipos irst "
		SqlRedireccionar = SqlRedireccionar & "inner join area_grupos ag on irst.areagrupo = ag.areagrupo "
		SqlRedireccionar = SqlRedireccionar & "inner join area_origen ao on ao.areaorigen = ag.areaorigen "
		SqlRedireccionar = SqlRedireccionar & "where irst.rol = " & sRol & " and irst.servicio = " & sTipoPrestacion
	else
		SqlRedireccionar = ""
		SqlRedireccionar = SqlRedireccionar & "select esareaprofesional,esareacorso from impresa_rolesserviciostipos irst "
		SqlRedireccionar = SqlRedireccionar & "inner join area_grupos ag on irst.areagrupo = ag.areagrupo "
		SqlRedireccionar = SqlRedireccionar & "inner join area_origen ao on ao.areaorigen = ag.areaorigen "
		SqlRedireccionar = SqlRedireccionar & "where irst.servicio = " & sTipoPrestacion
	end if

	'Response.Write SqlRedireccionar

	set RsRedireccionar = server.CreateObject("adodb.recordset")

'PL-SQL * T-SQL
'SQLREDIRECCIONAR = TransformPLSQLToTSQL (SQLREDIRECCIONAR)
	set  RsRedireccionar = cc.execute(SqlRedireccionar)

	if not RsRedireccionar.EOF then
		if cstr(RsRedireccionar("esareaprofesional")) = "1" then
			Seleccion = "GP" ''grupo ocupacional / puesto
		else
			Seleccion = "TS" ''tipologia / subtipologia
		end if
	end if
end if


	nIdSede = Request("idSede")
	if nidsede <> "" then
		sSQL ="SELECT SEDE_IMPRESA.ID_SEDE,SEDE_IMPRESA.DESCRIZIONE, SEDE_IMPRESA.ID_IMPRESA" ' &_
		sSQL = sSQL + " FROM SEDE_IMPRESA WHERE SEDE_IMPRESA.ID_SEDE =" & nIdSede & " "
'PL-SQL * T-SQL
'SSQL = TransformPLSQLToTSQL (SSQL)
		set rstDescr = cc.Execute(sSQL)
		nIdImp = rstDescr("ID_IMPRESA")
		%>
		<!--#include file = "BUS_Label.asp"-->
		<%
		rstDescr.close
	end if

' ==============================================================================================
' Reglas de Visualizacion y Modificacion de datos

		miaimpresa=0
		if clng(session("creator")) >0 then
			sSQL = "SELECT ID_IMPRESA FROM SEDE_IMPRESA WHERE ID_SEDE =" & clng(session("creator"))
			'PL-SQL * T-SQL  
			SSQL = TransformPLSQLToTSQL (SSQL) 
			set rsSImpresa = CC.Execute(sSQL)

			miaimpresa=rsSImpresa("ID_IMPRESA")
		end if


	'Response.Write clng(nIdImp) & " - " &  miaimpresa
	
	select case clng(nIdImp) = clng(miaimpresa)
	case true
		'Si el usuario es consultor o mesa de ayuda no Modifica ni Gestiona
		sModifica = "S"
		sGestionaPost = "S"
	case else ' � un'operatore intermediatio (es. CPI)
		'Usuarios que Pueden Modificar o Gestionar (es. Operatore cepe)
		if not isnull(RRr("PROYECTO")) then		'Si viene de Proyectos
			sModifica = "N"
			sGestionaPost = "S"
		else
			'Response.Write "TEmp: " & TEmp & "<br>"
			' Viene de Oficina o de algun Empleador/Prestador
			if (TEmp = "E" or TEmp = "P") then ' si entra por Empleador / Prestador
				sModifica = "S"
				sGestionaPost = "S"
			else ' si entra por Gestion de la Oficina
				sGestionaPost = "S"
				if cstr(sSede) = cstr(session("creator")) then  
					'Solo Modifica  si el aviso fue publicado por la oficina
					 sModifica = "S"
				end if
			end if
		end if
	end select


	'Control y Reactivacion de Avisos Vencidos
	sReactiva = "N"
	if cdate(sDtAl)<date then
		'modificado por AEM (07/2013) para que las empresas puedan seguir gestionado despues de vencido
		'if cdate(sDtAvv)<date then
		''	sGestionaPost = "N" 
			'Response.Write "No Gestiona por que la fecha de incorporaci�n ya pas�"
		'end if
		if sModifica = "S" then
			sReactiva = "S"
			sModifica = "N"
			'Response.Write "Solo Puede Reactivar"
		else
			'Response.Write "No puede Modificar por ende  no reactiva tampoco"
		end if
	end if
'Response.Write "<BR>sGestionaPost: " & sGestionaPost
'verifica che sia l'impresa che sta gestendo l'annuncio e non gli permette di gestire i candidati
'    if cstr(sSede) = cstr(session("creator")) then  
'	    sGestionaPost = "N"
'    end if		





'Response.Write "<BR>sModifica: " & sModifica
'Response.Write "<BR>TEmp: " & TEmp
'Response.Write "<BR>Gruppo: " & session("IDGruppo")
'Response.Write "<BR>Creator: " & session("creator")
'Response.Write "<BR>NCODOFICINA: " & NCODOFICINA
'Response.Write "<BR>sSede: " & sSede
'Response.Write "<BR>Tipo: " & Tipo
'Response.Write "<BR>sGestionaPost: " & sGestionaPost
' ==============================================================================================


'FormatoEtiquetas="L"

''''vani 15-06-2006


	if FormatoEtiquetas = "L" then
		EtCant = "Trabajadores demandados para el cargo: *"
		EtFecha = "Fecha de Incorporaci�n al puesto de trabajo: *"
		TexAnuncio = "Descripci�n de la Vacante"
		duracion = "Duraci�n del trabajo"
		EtiquetaI = "Grupo Ocupacional: *"
		EtiquetaII = "Puesto/Ocupaci&oacute;n: *"
	else
		EtCant = "Participantes/Vacantes disponibles: *"
		EtFecha = "Fecha de Inicio: *"
		TexAnuncio= "Dise�o del Curso/Actividad"
		duracion = "Duraci�n del Curso/Actividad"
		EtiquetaI = "Tipolog�a: *"
		EtiquetaII = "Subtipolog�a: *"
	end if


%>


	<table border="0" width="520" cellspacing="0" cellpadding="0" height="70" align="center">
		<tr>
			<td width="500" background="<%=session("progetto")%>/images/titoli/servizi2g.gif" height="70" valign="bottom" align="right">
				<table border="0" background width="500" height="23" cellspacing="0" cellpadding="0">
					<tr>

          <!--<td width="100%" valign="top" align="right"><b CLASS="tbltext1a">Bsqueda de Personal</b></td>-->
					</tr>
				</table>
			</td>
		</tr>
	</table>

	<br>
	<!-- Lingetta superiore -->
	<table border="0" CELLPADDING="0" CELLSPACING="0" width="500" align="center">
		<tr height="18">

    <td class="sfondomenu" width="67%" height="18"><span class="tbltext0"><b>Modificaci&oacute;n
      de la Vacante</b></span></td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>

    <td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">(*)
      campo obligatorio</td>
		</tr>
	</table>
	<!-- Commento -->
	<table border="0" CELLPADDING="0" CELLSPACING="0" width="500" align="center">
<%if cstr(Tipo)= "0" then%>
  <tr class="sfondocomm">
    <td align="left" class="sfondocommaz">
		<p>Seleccione una opci&oacute;n del men&uacute; que se encuentra en la parte inferior para ingresar, modificar o eliminar un criterio de la selecci&oacute;n.
		<br>
		Presione el b&oacute;ton <strong>Enviar</strong> para guardar las modificaciones efectuadas a los criterios .
		<br>
		<%if clng(sIdSede)=clng(session("creator"))then%>
		Presionar el b&oacute;ton<strong> B&uacute;squeda</strong> para obtener la lista de candidatos.
		<br>
		<%end if %>
		<!--Presionar el boton <strong>Visualizar Filtros</strong> para visualizar los criterios de busqueda usados.
		<br-->
		Presionar el bot&oacute;n <strong>Visualizar Todos los Estados</strong> para ver todos los candidatos seleccionados.
		<br>
		Presionar sobre <%
			if Seleccion = "GP" or Seleccion = "" then
				%>la Denominaci&oacute;n del <strong>Cargo Equivalente</strong><%
			else
				%>la <strong>Subtipolog�a</strong><%
			end if %> para consultar el detalle de la misma.
			<!-- <a href="Javascript:Show_Help('/Pgm/help/BUSQUEDA/BUS_Modifica')"><img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a> -->
		</p>
    </td>
  </tr>
 <%elseif cstr(Tipo) = "1" then %>
  <tr class="sfondocomm">
    <td align="left" class="sfondocommaz">
		<p>Seleccione una opci&oacute;n del men&uacute; que se encuentra en la parte inferior para ingresar, modificar o eliminar un criterio de la vacante.
		<br>
		Presionar el bot&oacute;n <strong>enviar</strong> para guardar las modificaciones efectuadas a los criterios.
		<br>
		<%if clng(sIdSede)=clng(session("creator"))then%>
		    Presionar el bot&oacute;n<strong> Posibles Postulantes</strong> para obtener la lista de personas que se aproximan al perfil.
		    <br>
		    <!--Presionar el boton <strong>Visualizar Filtros</strong> para visualizar los criterios de busqueda usados.
		    <br-->
		    Presionar el bot&oacute;n <strong>Visualizar Postulantes</strong> para ver los postulantes.
		    <br>
		 <%end if %>   
		Presionar sobre <%
			if Seleccion = "GP" or Seleccion = "" then
				%>la Denominaci&oacute;n del <strong>Cargo Equivalente</strong><%
			else
				%>la <strong>Subtipolog&iacute;a</strong><%
			end if %> para consultar el detalle de la misma.
			<!-- <a href="Javascript:Show_Help('/Pgm/help/BUSQUEDA/BUS_Modifica')"><img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a> -->
		</p>
    </td>
  </tr>
 <%end if%>
  <tr height="2">
		<td class="sfondocommaz" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif">
		</td>
  </tr>
</table>
	<br>


<br>
	<!--Richiama la finestra di dettaglio del Profilo Professionale.Viene attivato dal link relativo.-->
	<form method="post" name="FrmSpiega" target="_New" action="/pgm/bilanciocompetenze/DettagliArea/Focusfigprof.asp">
		<input type="hidden" name="VisMenu" value="NO">
		<input type="hidden" name="id_figprof" value="<%=nTipoProf%>">
		<input type="hidden" name="TipoDeAviso" value="<%=TANUNCIO%>">
	</form>

	<form method="post" name="FrmPrincipale">
		<input type="hidden" value="<%=TEmp%>" name="TEmp">
		<input type="hidden" value="<%=Tipo%>" name="Tipo">
		

		<table border="0" width="500" cellpadding="6" cellspacing="0">
			<tr>
				<td width="130" class="tbltext1" align="left">
					<b>Tipo de Servicio:</b>
				</td>
				<td class="textblack" align="left">
					<b>	<%
						'dim rstipoanun
						'dim misql
						'dim arrtipoanun
						'dim sarrtipoanun

						'set rstipoanun = server.CreateObject("adodb.recordset")
						'misql = "Select ID_CAMPO_DESC from DIZ_DATI where ID_TAB = 'RICHIESTA_SEDE' AND ID_CAMPO = 'TIPOANUNCIO'"
'PL-SQL * T-SQL
'MISQL = TransformPLSQLToTSQL (MISQL)
						'set rstipoanun = CC.execute(misql)
						'arrtipoanun = rstipoanun("ID_CAMPO_DESC")
						'sarrtipoanun = split(arrtipoanun,"|")
						'rstipoanun.Close
						'Set rstipoanun = nothing
						'for i = lbound(sarrtipoanun) to ubound(sarrtipoanun)
						'	if sarrtipoanun(i) = rsRif("TIPOANUNCIO") then
						'		valor = ucase(sarrtipoanun(i + 1))
						'		Response.Write valor
						'		response.write "<input type='hidden' value='" & cstr(valor) & "' name='txttipoanuncio'>"
						'		exit for
						'	end if
						'next

						set rstipoanun = server.CreateObject("adodb.recordset")

						if sRol <> "" then
							misql = "select descripcion from impresa_rolesserviciostipos where rol = " & sRol & " and servicio = " & sTipoPrestacion
						else
							misql = "select rol, descripcion from impresa_rolesserviciostipos where servicio = " & sTipoPrestacion
						end if

'PL-SQL * T-SQL
'MISQL = TransformPLSQLToTSQL (MISQL)
						set rstipoanun = CC.execute(misql)
						if sRol = "" then
							sRol = rstipoanun("rol")
						end if
						Response.Write rstipoanun("descripcion")
						sNombrePrestacion = trim(rstipoanun("descripcion"))
						response.write "<input type='hidden' value='" & sTipoPrestacion & "' name='TipoPrestacion'>"

						%>
					</b>
				</td>
			</tr>

			<%
			'vani ambito territorial **************************************************************************************
			%>

			<tr id="trAmbitoTerritorial">
				<td width="130" class="tbltext1" align="left">
					<b>&Aacute;mbito Territorial</b>
				</td>
				<td class="textblack" align="left">
					<%
					Ambito = request("CmbAmbito")
					AmbitoBase = NTIPOAMBITO

					'Response.Write NTIPOAMBITO
					if request("CmbAmbito") <> "" then
						AmbitoSeleccionado = Ambito
					else
						AmbitoSeleccionado = AmbitoBase
					end if

					if sModifica = "S" then
						CreateComboDizDati "RICHIESTA_SEDE","TIPOAMBITO","CmbAmbito' 'style=cursor:hand","tbltext1","",cstr(AmbitoSeleccionado)
					else
						%><b><%
							Response.write DecCodValDizDati("RICHIESTA_SEDE","TIPOAMBITO", AmbitoBase)
							%>
							<input type="hidden" name="CmbAmbito" value="<%=AmbitoSeleccionado%>">
						<b><%
					end if

					Set rrRicCand = Server.CreateObject("ADODB.Recordset")
					Sql2 = "SELECT ID_PERSONA FROM RICHIESTA_CANDIDATO WHERE ID_RICHIESTA= " & nIdRic
					rrRicCand.Open Sql2,CC,1
					
					TotalCandidatos = rrRicCand.recordcount
					'if TotalCandidatos = 0 then
					''	response.write "<br>La vacante no tiene candidatos, y el �mbito puede cambiarse"
					'else
					''	response.write "<br>Hay " & TotalCandidatos & " candidatos. NO SE PUEDE CAMBIAR EL �MBITO"
					'end if
					rrRicCand.close
					Set rrRicCand = Nothing
					%>
					<input type="hidden" name="Tcandidatos" value="<%=TotalCandidatos%>">
					<input type="hidden" name="TxtAmbitoBase" value="<%=AmbitoBase%>">
				</td>
			</tr>
			<% if Session("idgruppo") = 37 then %>
				<script type="text/javascript">
					$(function(){
						//var CmbAmbito = document.getElementsByName("CmbAmbito");
						$("#tdShowAmbito").html($("[name=CmbAmbito] option[selected]").html())
						//$(CmbAmbito).attr('disabled', 'disabled');
						$("#trAmbitoTerritorial").hide();
					});
				</script>
				<tr>
					<td width="130" class="tbltext1" align="left">
						<b>&Aacute;mbito Territorial</b>
					</td>
					<td class="textblack" align="left" id="tdShowAmbito" style="font-weight:bold;">

					</td>


				</tr>
			<% end if %>


			<%
			'**************************************************************************************************************
			%>

			<tr>
				<td class="tbltext1" width="130" align="left">
					<b>Nro Referencia</b></td>
		        <td class="textblack" align="left">
					<b><%=nIdRic%></b>
				</td>
			</tr>
			<tr>
				<td class="tbltext1" width="130" align="left">
					<b>T&iacute;tulo Vacante</b></td>
		        <td class="textblack" align="left">
					<b><%=OFFER_TITLE%></b>
				</td>
			</tr>
		</table>
		<br>
		<table CELLPADDING="0" CELLSPACING="0" border="0" width="500">
			<tr height="2">
					<td class="sfondocommaz" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif">
					</td>
			</tr>
		</table>
		<br>



		<%
		'Datos de Proyecto **************************************************************************************
		%>

		<%
		if not isnull(RRr("PROYECTO")) then

			set rsbuscocontraparte = server.CreateObject("adodb.recordset")

			sqlbuscocontraparte = "select ica_raso as Nombre_Contraparte, Nombre as Nombre_Proyecto, DescMunicipio as Municipio, "
			sqlbuscocontraparte = sqlbuscocontraparte & "DescDepto as Departamento, DescLocalidad as Localidad, DescGecal as Gecal, DescPrograma as Programa "
			sqlbuscocontraparte = sqlbuscocontraparte & "from visProyectos where Gecal = " & RRr("GECAL") & " and Programa = " & RRr("PROGRAMA")
			sqlbuscocontraparte = sqlbuscocontraparte & " and Proyecto = " & RRr("PROYECTO")

			'Response.Write sqlbuscocontraparte

'PL-SQL * T-SQL
'SQLBUSCOCONTRAPARTE = TransformPLSQLToTSQL (SQLBUSCOCONTRAPARTE)
			set rsbuscocontraparte = connProyectos.execute(sqlbuscocontraparte)
			if not rsbuscocontraparte.eof then
			%>
				<!--<table border="0" width="500" CELLPADDING="2" CELLSPACING="2">
					<tr>
						<td class="tbltext1" width="130"><b>Datos de la Contraparte:</b></td>
						<td class="textblack"><b>Nombre: <%=trim(rsbuscocontraparte("Nombre_Contraparte"))%></b></td>
					</tr>
				</table>
				<br>
				<table CELLPADDING="0" CELLSPACING="0" border="0" width="500">
					<tr>
						<td width="100%" colspan="8" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
					</tr>
				</table>
				<br>
				<br>-->

				<table border="0" width="500" cellpadding="6" cellspacing="0">
					<tr>
						<td class="tbltext1" colspan="3"><b>Datos del Proyecto:</b></td>
					</tr>
					<tr>
						<td class="tbltext1" width="130"><b></b></td>
						<td class="tbltext1" width="100"><b>Gecal: </b></td>
				  		<td class="textblack"><b><%=Ucase(rsbuscocontraparte("gecal"))%></b></td>
					</tr>
					<tr>
						<td class="tbltext1" width="130"><b></b></td>
						<td class="tbltext1" width="100"><b>Programa: </b></td>
				  		<td class="textblack"><b><%=Ucase(rsbuscocontraparte("programa"))%></b></td>
					</tr>
					<tr>
						<td class="tbltext1" width="130"><b></b></td>
						<td class="tbltext1" width="100"><b>Proyecto: </b></td>
				  		<td class="textblack"><b><%=Ucase(RRr("proyecto"))%></b></td>
					</tr>
					<tr>
						<td class="tbltext1" width="130">&nbsp; </td>
						<td class="tbltext1" width="100"><b>Nombre:</b>	</td>
						<td class="textblack"><b><%=Ucase(rsbuscocontraparte("Nombre_Proyecto"))%></b></td>
					</tr>
					<tr>
						<td class="tbltext1" width="130"><b></b></td>
						<td class="tbltext1" width="100"><b>Organismo Responsable:</b>	</td>
						<td class="textblack"><b><%=trim(rsbuscocontraparte("Nombre_Contraparte"))%></b></td>
					</tr>
					<tr>
						<td class="tbltext1" width="130"><b></b></td>
						<td class="tbltext1" width="100"><b>Municipio:</b>	</td>
						<td class="textblack"><b><%=Ucase(rsbuscocontraparte("Municipio"))%></b></td>
					</tr>
					<tr>
						<td class="tbltext1" width="130">&nbsp; </td>
						<td class="tbltext1" width="100"><b>Departamento:</b>	</td>
						<td class="textblack"><b><%=Ucase(rsbuscocontraparte("Departamento"))%></b></td>
					</tr>
					<tr>
						<td class="tbltext1" width="130">&nbsp; </td>
				  		<td class="tbltext1" width="100"><b>Localidad:</b>	</td>
						<td class="textblack"><b><%=Ucase(rsbuscocontraparte("Localidad"))%></b></td>
					</tr>
					<!--<tr>
					<%
					sDuracion = RRr("DURACION")
					sTipoDuracion = RRr("DURACIONUMEDIDA")
					%>
					  <td class="tbltext1" width="130">&nbsp; </td>
				      <td class="textblack">
				      <br>
				      <b>Duracin: <input type="text" name="txtDuracion" value="<%=sDuracion%>" class="textblack" maxlength="3" size="3">
				      &nbsp;&nbsp;Medida en: <%CreateComboDizDati "FORMAZIONE", "COD_TIPO_DURACION", "cmbTipoDuracion", "textblack", "", sTipoDuracion%>
				      </b>
				      </td>
					</tr>-->
				</table>
				<br>
				<table CELLPADDING="0" CELLSPACING="0" border="0" width="500">
					<tr height="2">
						<td class="sfondocommaz" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
					</tr>
				</table>
				<br>
				<%
			end if
			set rsbuscocontraparte = nothing
		end if
		%>

		<%
		'Datos de la  **************************************************************************************
		%>
		<!--GRUPOS OCUPACIONALES/TIPOLOGIAS Y PUESTOS/SUBTIPOLOGIAS VANI-->

		<table border="0" width="500" cellpadding="6" cellspacing="0">
			<!--<tr height="23">
				<td width="200" class="tbltext1">
				<b>Nombre del Proyecto Original:</b>
				</td>
				<td class="textblack">
					<b><%=sNomProj%></b>
					<input type="hidden" name="txtnomproj" value="<%=sNomProj%>">
				</td>
			</tr>-->
			<%
			''''VANI 07-06-06''''

			'Response.Write sidsede
			SQLEMPR = ""
			SQLEMPR = SQLEMPR & "SELECT LC.DESCRIPCION DESCLOC,DP.DESCRIPCION DESCDEPTO "
			SQLEMPR = SQLEMPR & "FROM SEDE_IMPRESA SI "
			SQLEMPR = SQLEMPR & "INNER JOIN LOCALIDAD LC "
			SQLEMPR = SQLEMPR & "ON LC.CODLOC = SI.LOC "
			SQLEMPR = SQLEMPR & "INNER JOIN DEPARTAMENTO DP "
			SQLEMPR = SQLEMPR & "ON LC.DEPTO = DP.CODDEPT "
			SQLEMPR = SQLEMPR & "WHERE DP.PROVINCIA = SI.PRV "
			SQLEMPR = SQLEMPR & "AND SI.ID_SEDE = " & sIdsede

			'Response.Write sqlempr
'PL-SQL * T-SQL
'SQLEMPR = TransformPLSQLToTSQL (SQLEMPR)
			set rsempr = cc.execute(sqlempr)
			if not rsempr.EOF then
				sDepto = rsempr("DESCDEPTO")
				sLocalidad = rsempr("DESCLOC")
			end if

			SQLEMPR = ""
			SQLEMPR = SQLEMPR & "SELECT DESCOM FROM COMUNE CO INNER JOIN SEDE_IMPRESA SI "
			SQLEMPR = SQLEMPR & "ON CO.CODCOM = SI.COMUNE WHERE "
			SQLEMPR = SQLEMPR & "CO.PRV = SI.PRV AND SI.ID_SEDE =" & sIdsede
'PL-SQL * T-SQL
'SQLEMPR = TransformPLSQLToTSQL (SQLEMPR)
			set rsempr = cc.execute(sqlempr)

			if not rsempr.EOF then
				sMunicipio = rsempr("DESCOM")
			end if
			%>
			<!--<tr height="23">
			<td width="200" class="tbltext1">
			<b>Municipio:</b>
			</td>
			<td class="textblack">
				<b><%=sMunicipio%></b>
				<input type="hidden" name="txtmunicipio" value="<%=sMunicipio%>">
			</td>
			</tr>
			<tr height="23">
				<td width="200" class="tbltext1">
					<b>Departamento:</b>
				</td>
				<td class="textblack">
					<b><%=sDepto%></b>
					<input type="hidden" name="txtdepto" value="<%=sDepto%>">
				</td>
			</tr>
			<tr height="23">
				<td width="200" class="tbltext1">
				<b>Localidad:</b>
				</td>
				<td class="textblack">
					<b><%=sLocalidad%></b>
					<input type="hidden" name="txtlocalidad" value="<%=sLocalidad%>">
				</td>
			</tr>-->
			<!--<tr height="23">
		      <td width="130" class="tbltext1"><b>Rama de Actividad:</b></td>
					<td class="textblack">
						<b><%=sSettoreDesc%></b>
						<input type="hidden" name="txtramaact" value="<%=sSettoreDesc%>">
					</td>
			</tr>-->
			<!--<tr><td colspan="2">
			<table border="0">-->

				<tr>
					<td class="tbltext1" colspan="3">
						<b>Cargo Equivalente</b>
					</td>
				</tr>
				<tr><td><br></td></tr>

			<%' per farlo entrare sempre in questo ramo della if la successiva if � stata cambiata con una che si verifica sempre
			'if sPubblica = "S" then

			if sPubblica = "S" or "S"="S" then
				%>
				<tr>
				<td class="tbltext1" width="130" align="left"> <b><%=EtiquetaI%></b></td>
				    <td class="textblack">
						<b><%=sTipoArea%></b>
						<input type="hidden" name="txtAreaHid" value="<%=nTipoArea%>">
					</td>
				</tr>

				<tr>
					<td class="tbltext1" width="130" align="left"> <b><%=EtiquetaII%></b></td>
				    <td class="textblack">
						<a href="Javascript:FrmSpiega.submit();" title="Descripci&oacute;n del perfil profesional">
							<u><b class="textblack"><%=sTipoProf%></b></u>
							<input type="hidden" name="txtProfiloHid" value="<%=nTipoProf%>">
						</a>
					</td>
				</tr>
				<%
			else
				%>
				<tr>
					<td width="130" class="tbltext1" align="left">
						<b><%=EtiquetaI%></b>
					</td>
					<td class="textblack">
						<textarea rows="4" class="textblacka" cols="45" readonly type="text" name="txtArea"><%=sTipoArea%></textarea>
						<input type="hidden" name="txtAreaHid" value="<%=nTipoArea%>">
						<!--</td>
						<td align="left" width="60" class="tbltext1"><b>-->
						<% if Seleccion = "GP" or Seleccion = "" then %>
							<a href="Javascript:SelSpecifico('AREE_PROFESSIONALI','ID_AREAPROF','txtArea','<%=sRol%>','<%=sTipoPrestacion%>')" ID="imgPunto2" name="imgPunto2"><img border="0" src="<%=Session("Progetto")%>/images/bullet1.gif"></a>
						<% else %>
							<a href="Javascript:SelSpecifico('AREA_CORSO','ID_AREA','txtArea','<%=sRol%>','<%=sTipoPrestacion%>')" ID="imgPunto2" name="imgPunto2"><img border="0" src="<%=Session("Progetto")%>/images/bullet1.gif"></a>
						<% end if %>
					</td>
				</tr>
				<tr>
					<td width="130" class="tbltext1" align="left">
						<b><%=EtiquetaII%></b>
					</td>
					<td class="textblack">
						<textarea rows="4" class="textblacka" cols="45" readonly type="text" name="txtProfilo"><%=sTipoProf%></textarea>
						<input type="hidden" name="txtProfiloHid" value="<%=nTipoProf%>">
						<!--</td>
						<td align="left" width="60" class="tbltext1"><b>-->
						<% if Seleccion = "GP" or Seleccion = "" then %>
							<a href="Javascript:SelSpecifico('FIGUREPROFESSIONALI','ID_FIGPROF','txtProfilo','<%=sRol%>','<%=sTipoPrestacion%>')"><img border="0" src="<%=Session("Progetto")%>/images/bullet1.gif"></a>
						<% else %>
							<a href="Javascript:SelSpecifico('AREA_CORSO_SUB','ID_SUBAREA','txtProfilo','<%=sRol%>','<%=sTipoPrestacion%>')"><img border="0" src="<%=Session("Progetto")%>/images/bullet1.gif"></a>
						<% end if %>
					</td>
				</tr>
				<%
			end if
			%>
			<!--</table></td></tr>-->
			<!--<tr>
		    <td class="tbltext1" width="200"> <b>Tipo de Solicitud</b></td>
		        <td class="textblack">
					<%=lcase(DecCodVal("TESPR", 0, DATE, nTipoRic, 0))%>
				</td>
			</tr>-->
		</table>
		<%
		'Datos de la  **************************************************************************************
		%>
		<!--GRUPOS OCUPACIONALES/TIPOLOGIAS Y PUESTOS/SUBTIPOLOGIAS VANI-->
		<%'if cstr(Tipo) = "0" then%>
		<table border="0" width="500" cellpadding="6" cellspacing="0">
			<tr>
				<td class="tbltext1" width="130" align="left">
					<b>Tipo de B&uacute;squeda</b>
				</td>
		        <td class="textblack">
					<b>
					<%
					
					
				'response.write "dasdfaseasd" 
				'ntiporicerca	="1"
				if TAviso = "C" then
					tstCmb1= "Por Tipolog�a"
					tstCmb2= "Por Subtipolog�a"
				else
					tstCmb1= "Por Grupo Ocupacional"
					tstCmb2= "Por Puesto/Ocupaci&oacute;n"
				end if			
              select case nTipoRicerca
							case "0"
    'nel caso sia stata gia scelta la ricerca per Gruppo(o tipologia) non � piu consentito modificare
				Response.Write tstCmb1%>
				<input type="hidden" name="cmbTipoRicerca" value="0">
              <%case "1"%>
              <span class="tbltext">
		          <select class="textblacka" id="cmbTipoRicerca" name="cmbTipoRicerca" onchange='javascript:AlertGrupo()'>
		            <option value="1" selected="selected"><%=tstCmb2%></option>
		            <option value="0"><%=tstCmb1%></option>
		          </select>
              </span>
              <%end select
					
					
'	'					select case nTipoRicerca
'							case "0"
'								if TAviso = "C" then
'									Response.Write "Por Tipolog�a"
'								else
'									Response.Write "Por Grupo Ocupacional"
'								end if
'							case "1"
'								if TAviso = "C" then
'									Response.Write "Por Subtipolog�a"
'								else
'									Response.Write "Por Cargo Equivalente"
'								end if
'							case else
'						end select
					%>
					</b>
				</td>
			</tr>
		</table>
		<%'end if%>
		<%

		set flagsempleador = server.CreateObject("adodb.recordset")
		sqlflagsempleador = "select ID_CIMPIEGO, PUB_DIRECTA, NOM_VISIBLE from sede_impresa si, richiesta_sede rs, impresa i where i.id_impresa = si.id_impresa and rs.id_sede = si.id_sede and rs.id_richiesta = " & nIdRic

		set flagsempleador = cc.execute(sqlflagsempleador)
		if not flagsempleador.eof then
			centroAsociado=flagsempleador("ID_CIMPIEGO")
			sPubdirecta = flagsempleador("PUB_DIRECTA")
			sNomvisible = flagsempleador("NOM_VISIBLE")
		end if
		set flagsempleador = nothing

		'if sNomvisible = "N" or isnull(sNomvisible) then 

		'Datos de la Sede **************************************************************************************
		%>



		<br>
		<table CELLPADDING="0" CELLSPACING="0" border="0" width="500">
			<tr height="2">
				<td class="sfondocommaz" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif">
				</td>
			</tr>
		</table>
		<br>		

		<table border="0" width="500" cellpadding="6" cellspacing="0">
				<!--tr>
					<td class="tbltext1" colspan="3" align="left">
						<b>�Desea mostrar los datos de la empresa que publica la vacante?</b>
					</td>
				</tr-->
			<!--	<tr>		<td width=130> &nbsp;</td>	</tr>					<tr>		<td width="100%" colspan=2 background="<%=Session("Progetto")%>/images/separazione.gif"></td>	</tr>	<tr>		<td width=130> &nbsp;</td>	</tr>				-->
			<%
			set rsbuscosede = server.CreateObject("adodb.recordset")
			'if sNomvisible = "N" or isnull(sNomvisible) then
				sqlbuscosede = "select * from sede_impresa si, richiesta_sede rs, impresa i where i.id_impresa = si.id_impresa and rs.id_sede = si.id_sede and rs.id_richiesta = " & nIdRic
			'else
			'	sqlbuscosede = "select * from sede_impresa si, impresa i where i.id_impresa = si.id_impresa and si.id_sede = " & centroAsociado			
			'end if
			'Response.Write sqlbuscosede
			'response.end
		'PL-SQL * T-SQL
		'SQLBUSCOSEDE = TransformPLSQLToTSQL (SQLBUSCOSEDE)
			set rsbuscosede = cc.execute(sqlbuscosede)
			if not rsbuscosede.eof then
				sForma= DecCodVal("FGIUR", "0", "", rsbuscosede("COD_FORMA"), 1)
				'sPubdirecta = rsbuscosede("PUB_DIRECTA")
				'sNomvisible = rsbuscosede("NOM_VISIBLE")
				'response.write "centroAsociado= " & centroAsociado & "<br>"
				'response.write "session('creator')= " & session("creator") & "<br>"
				'response.write "sPubdirecta= " & sPubdirecta & "<br>"

				%>

				<input type="hidden" value="<%=centroAsociado%>" name="centroAsoc">
				<input type="hidden" value="<%=sPubdirecta%>" name="pubdirecta">

				<%

				'if sNomvisible="S" or isnull(sNomvisible) then

				%>
				<tr>
					<td class="tbltext1" colspan="3">
						<b>Datos del Solicitante:</b>
					</td>
				</tr>
				<tr>
					<td class="tbltext1" width="130" align="left">
						<b><%=EtiquetaIV%></b>
					</td>
					<td class="tbltext1" width="100" align="left">
						<b>Sede:</b>
					</td>
					<td class="textblack">
						<b><%=Ucase(rsbuscosede("descrizione"))%></b>
					</td>
				</tr>
				<tr>
					<td class="tbltext1" width="130">&nbsp; </td>
					<!--td class="tbltext1" width="100">
						<b>Empresa:</b>
					</td-->
						<td class="tbltext1" align="left"><b>
				  		<%
				  		set rstipoanun = server.CreateObject("adodb.recordset")
						misql = "select descripcion from impresa_rolestipos where rol = " & srol
						set rstipoanun = CC.execute(misql)
						Response.Write rstipoanun("descripcion") & ":"
						%>
				  	</b></td>

					<td class="textblack">
						<b><%=trim(rsbuscosede("RAG_SOC") & " (" & sForma)%>)</b>
					</td>
				</tr>
				<tr>
					<!--<td class="tbltext1"><b>Indirizzo:</b></TD>-->
					<td class="tbltext1" width="130">&nbsp; </td>
					<td class="tbltext1" width="100" align="left">
						<b>Direcci&oacute;n:</b>
					</td>
					<td class="textblack">
						<b><%=Ucase(rsbuscosede("INDIRIZZO"))%></b>
					</td>
				</tr>
				<tr>
					<!--<td class="tbltext1"><b>Comune:</b></TD>-->
					<td class="tbltext1" width="130">&nbsp; </td>
					<%
					Set rstComune = Server.CreateObject("ADODB.Recordset")

					SQLcomune = " SELECT DESCOM FROM COMUNE WHERE CODCOM = '" & rsbuscosede("COMUNE") & "'"
					rstComune.open SQLcomune, CC, 1, 3

					if not isnull(rstComune("DESCOM")) then
						DescripcionComune = ucase(rstComune("DESCOM"))
					else
						DescripcionComune = ""
					end if
					set rstComune = nothing
					%>
					<td class="tbltext1" width="100"></td>
					<td class="textblack">
<!-- 						<b>(<%=trim(rsbuscosede("CAP") & ") " & DescripcionComune)%></b>
 -->						<b><%=trim(DescripcionComune)%></b>
					</td>
				</tr>
				<!--<tr><td class="tbltext1"><b>Provincia:</b></TD>		<td class="tbltext1"> &nbsp;</TD>		<td class="textblack">( <%=Ucase(sProvDesc)%> )</td>	</tr>-->
				<%
				'end if
			else
			%>
				<tr>
					<td class="textblack" align="center">
						<b>No corresponde a un empleador/prestador</b>
					</td>
				</tr>
			<%
			end if
			set rsbuscosede = nothing
			%>
		</table>
		<%'end if%>



		<%
		'Adaptacion al formulario de intermediacion laboral *************************************************************************
		%>
		<br>
		<table CELLPADDING="0" CELLSPACING="0" border="0" width="500">
			<tr height="2">
				<td class="sfondocommaz" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
			</tr>
		</table>
		<br>

		<table border="0" width="500" cellpadding="6" cellspacing="0">
			<%'vani 15-06-2006
			if FormatoEtiquetas = "L" then
				%>
				<tr>
					<td width="130" class="tbltext1" align="left">
						<b>Personal a Cargo: </b>
					</td>
					<%
					if sModifica = "S" then
						%>
						<td class="textblack">
							<input type="radio" name="PersCargo" value="S" <%if sPersCargo = "S" then Response.Write "checked"%>>SI
							<input type="radio" name="PersCargo" value="N" <%if sPersCargo = "N" then Response.Write "checked"%>>NO
						</td>
						<%
					else
						%>
						<td class="textblack">
							<b><%if sPersCargo = "S" then Response.Write "Si" else Response.Write "No" %></b>
							<input type="hidden" name="PersCargo" value="<%=sPersCargo%>">
						</td>
						<%
					end if
					%>
				</tr>
				<%
			else
				%>
				<input type="hidden" name="PersCargo" value="">
				<%
			end if
			%>
		</table>
		<br>
		<%
		if FormatoEtiquetas = "L" then
			%>
			<table width = "500"  border="1" cellspacing="2" cellpadding="4" bordercolor="MidNightBlue">
				<tr bordercolor="white">
					<td width="130" class="tbltext1" align="left">
						<b>El cargo solicitado admite postulantes con alg&uacute;n tipo de discapacidad?*</b>
					</td>
					<td  class="textblack">
						<%
						if sModifica = "S"  then
							%>
							<input type="radio" name="PersDisc" value="S" onclick="HabDes('S')" <%if sPersDisc = "S" then Response.Write "checked"%>>SI
							<input type="radio" name="PersDisc" value="N" onclick="HabDes('N')" <%if sPersDisc = "N" then Response.Write "checked"%>>NO
							<%
						else
							%>
							<b><%if sPersDisc = "S" then Response.Write "Si" else Response.Write "No" %></b>
							<input type="hidden" name="PersDisc" value="<%=sPersDisc%>">
							<%
						end if
						%>
						&nbsp;<b>Tipo de Discapacidad?</b>&nbsp;
						<%
						if sModifica = "S"  then
							ComboDisc = "TCAPI|0|" & date & "|" & sPersTipoDisc & "|cmbDisc|ORDER BY DESCRIZIONE"
							CreateCombo(ComboDisc)
						else
							%>
							<b><%Response.Write DecCodVal("TCAPI", 0, DATE, sPersTipoDisc, 0)%></b>
							<input type="hidden" name="cmbDisc" value="<%=sPersDisc%>">
							<%
						end if

						%>
					</td>
					<!--td  class="tbltext1">
						<input type="radio" name="PersDisc" value="S" onclick="HabDes('S')" <%if sPersDisc = "S" then Response.Write "checked"%>>SI
						<input type="radio" name="PersDisc" value="N" onclick="HabDes('N')" <%if sPersDisc = "N" then Response.Write "checked"%>>NO
						&nbsp;<b>Tipo de Discapacidad?</b>&nbsp;
						<%
						ComboDisc = "TCAPI|0|" & date & "|" & sPersTipoDisc & "|cmbDisc|ORDER BY DESCRIZIONE"
						CreateCombo(ComboDisc)
						%>
					</td-->
				</tr>
				<tr bordercolor="white">
					<td width="130" class="tbltext1" align="left">
						<b>Observaciones</b>
					</td>
					<td class="textblack">
						<%
						if sModifica = "S"  then
							%>
							<textarea name="Obs_Disc" rows=7 cols=35 WRAP="ON"><%=trim(sDiscObs)%></textarea>
							<%
						else
							%>
							<b><%=trim(sDiscObs)%></b>
							<input type="hidden" name="Obs_Disc" value="<%=trim(sDiscObs)%>">
							<%
						end if
						%>
					</td>
				</tr>
			</table>
			<br>
			<%
		else
			%>
			<input type="hidden" name="PersDisc" value="N">
			<input type="hidden" name="PersDisc" value="N">
			<input type="hidden" name="cmbDisc" value="">
			<input type="hidden" name="Obs_Disc" value="">
			<%
		end if
		%>
			<!--<tr height="17" bordercolor="white">
				<td width="90" class="tbltext1">
					<b>Observaciones</b>
				</td>
				<td  class="tbltext1">
					<textarea name="Obs_Disc" rows=7 cols=40 WRAP="ON"><%=trim(sDiscObs)%></textarea>
				</td>
			</tr>-->

		<%
		'Datos de la  **************************************************************************************
		%>

		<table border="0" width="500" cellpadding="6" cellspacing="0">
				<tr>
					<td class="tbltext1" colspan="3">
						<b>Lugar de la Vacante</b><br>
						(Si no selecciona un lugar, se interpreta que las vacantes se encuentran disponibles en todo el Pa&iacute;s)
					</td>
				</tr>
				<tr><td><br></td></tr>
				<tr>
					<td width="130" class="tbltext1" align="left"><b>Vacante para todo el pa�s</td>
					<td class="tbltext1" colspan="2" align="left">
						<input type="radio" name="todoelPais" value="0" id="falseTodoelPais" /> NO
						<input type="radio" name="todoelPais" value="1" id="trueTodoelPais" /> SI
					</td>
				</tr>
			<tr>
				<td class="tbltext1" width="130" align="left">
					<b>Departamento</b>
				</td>
			    <td class="textblack" align="left">
					<%
					if len(nMunicipio)<5 then
						nMunicipio = "0" & nMunicipio
						'response.write nMunicipio
					end if
					if sModifica = "S"  then
						sInt = "PROV|0|" & date & "|" & left(nMunicipio,2) & "|cmbDepartamento' onchange='PulisciCom()|ORDER BY DESCRIZIONE"
				        CreateCombo(sInt)					
					else
						if nMunicipio <> "" then
							Set rrNDepart = Server.CreateObject("ADODB.Recordset")
							ndepart = "select descrizione from tades where nome_tabella='prov' and codice=" & left(nMunicipio,2)
							set rrNDepart = CC.execute(ndepart)
					
							nome_muni = rrNDepart("descrizione")
							rrNDepart.close
							Set rrNDepart = Nothing
						end if
						%>
						<b><%=nome_muni%></b>
						<input type="hidden" name="cmbDepartamento" value="<%=left(nMunicipio,2)%>">
						<%
					end if

					if nMunicipio <> "" then %>
						<script type="text/javascript">	
							$(function(){
								$("#falseTodoelPais").attr("checked", true);
								$("#trueTodoelPais").attr("checked", false);							
							});
						</script>
					<% else %>
						<script type="text/javascript">								
							$(function(){
								$("#falseTodoelPais").attr("checked", false);
								$("#trueTodoelPais").attr("checked", true);
								
								$('#cmbDepartamento').attr('disabled', 'disabled');
								$("#txtMunicip").attr('disabled', 'disabled');


							});
						</script>
					<% end if


					%>
				</td>
			</tr>
				
            <td width="130" class="tbltext1" align="left"><b>Municipio</b>
			</td>
	  		
      		<td class="textblack" align="left">
				<%
								dim rsComune

				sComune = nMunicipio
				
				
				if sComune <> "" then
					sSQL = "SELECT CODCOM,DESCOM from COMUNE WHERE CODCOM = '" & sComune & "'"
			        'Response.Write sSQL
'PL-SQL * T-SQL
'SSQL = TransformPLSQLToTSQL (SSQL)
			        set rsComune = CC.Execute(sSQL)

					if not rsComune.EOF then
					    descComune = rsComune("DESCOM")
					else
					    sComune =""
					    descComune =""
					end if    

					rsComune.Close

				    set rsComune = nothing
				end if%>
				
				<span class="tbltext">
				<input type="text" name="txtMunicip" id="txtMunicip" style="TEXT-TRANSFORM: uppercase;" class="textblacka" size="35" readonly value="<%=descComune%>">
				<input type="hidden" id="cmbMunicipio" name="cmbMunicipio" value="<%=sComune%>">
				
<%
				NomeForm="FrmPrincipale"
				CodiceProvincia="cmbDepartamento"
				NomeComune="txtMunicip"
				CodiceComune="cmbMunicipio"
				Cap="NO"
				if sModifica = "S"  then 
%>
				<a href="Javascript:SelComune('<%=NomeForm%>','<%=CodiceProvincia%>','<%=NomeComune%>','<%=CodiceComune%>','<%=Cap%>')" ID="A1" name="imgPunto1" onmouseover="javascript:window.status='';return true"><img border="0" src="<%=Session("Progetto")%>/images/bullet1.gif"></a>
				<%end if%>
				</span>
			</td>
		</tr>			
			
		</table>
		<table CELLPADDING="0" CELLSPACING="0" border="0" width="500">
			<tr height="2">
				<td class="sfondocommaz" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
			</tr>
		</table>

		<table border="0" width="500" cellpadding="6" cellspacing="0">
			<tr>
				<td class="tbltext1" width="130" align="left">
					<b><%=EtCant%></b>
				</td>
			    <td class="textblack" align="left">
					<%
					if sModifica = "S"  then
						%>
						<input class="textblacka" type="text" name="txtNPosti" size="3" maxlength="3" value="<%=nPosti%>">
						<%
					else
						%>
						<b><%=nPosti%></b>
						<input type="hidden" name="txtNPosti" value="<%=nPosti%>">
						<%
					end if
					%>
				</td>
			</tr>
			<tr>
				<%
				if IsNull(RRr.Fields("direccion"))then
					sDireccion = "(Informaci�n no suministrada)"
				end if
				%>
				<td width="130" class="tbltext1" align="left">
					<b>Presentarse en: </b>
				</td>
				<td class="textblack" align="left">
					<%
					if sModifica = "S"  then
						%>
						<input class="textblacka" type="text" name="txtPresentarseEn" style="width=320px" maxlength="250" value="<%=sDireccion%>">
						<%
					else
						%>
						<b><%=sDireccion%></b>
						<%
					end if
					%>
				</td>
			</tr>
			<tr>
			<%
			sDuracion = RRr("DURACION")
			sTipoDuracion = RRr("DURACIONUMEDIDA")
			if sTipoPrestacion <> "1" then
				%>
				<td class="tbltext1" width="130" align="left">
					<b><%=Duracion%>:</b>
				</td>
				<td class="textblack">
					<%
					if sModifica = "S"  then
						%>
						<b><input type="text" name="txtDuracion" value="<%=sDuracion%>" class="textblack" maxlength="3" size="3"> <!--<input type="text" name="txtDuracion" value="<%=sDuracion%>" class="textblack" readonly maxlength="3" size="3">-->
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Medida en: <%CreateComboDizDati "FORMAZIONE", "COD_TIPO_DURACION", "cmbTipoDuracion", "textblack", "", sTipoDuracion%>
						</b>
						<%
					else
						%>
						<b><%=sDuracion & " " & DecCodValDizDati("FORMAZIONE","COD_TIPO_DURACION", sTipoDuracion)%></b>
						<input type="hidden" name="txtDuracion" value="<%=sDuracion%>">
						<input type="hidden" name="cmbTipoDuracion" value="<%=sTipoDuracion%>">
						<%
					end if
					%>
				</td>
				<%
			else
				%>
				<input type="hidden" name="txtDuracion" value="0">
				<input type="hidden" name="cmbTipoDuracion" value="M">
				<%
			end if
			%>
			</tr>
			<tr>
				<td class="tbltext1" width="130" align="left">
					<b>Plazo de env&iacute;o de postulantes: *</b>
				</td>
				<td class="textblack" align="left">
					<%
					if sModifica = "S" or sReactiva = "S"  then
						%>
						<input class="textblacka fecha" type="text" name="txtDtAl" size="10" maxlength="10" value="<%=sDtAl%>">
						<%
					else
						%>
						<b><%=sDtAl%></b>
						<input type="hidden" name="txtDtAl" value="<%=sDtAl%>">
						<%
					end if
					%>
					<b>&nbsp;&nbsp; (dd/mm/aaaa)</b>
				</td>
			</tr>
			<tr>
				<td class="tbltext1" width="130" align="left">
					<b><%=EtFecha%></b>
				</td>
				<td class="textblack" align="left">
					<%
					if sModifica = "S"  then
						%>
						<input class="textblackan fecha" type="text" name="txtDtAvv" size="10" maxlength="10" value="<%=sDtAvv%>">
						<%
					else
						%>
						<b><%=sDtAvv%></b>
						<input type="hidden" name="txtDtAvv" value="<%=sDtAvv%>">
						<%
					end if
					%>
					<b>&nbsp;&nbsp; (dd/mm/aaaa)</b>
				</td>
			</tr>
		</table>
		<br>
		<table CELLPADDING="0" CELLSPACING="0" border="0" width="500">
			<tr height="2">
				<td class="sfondocommaz" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
			</tr>
		</table>
		<br>

		<%
		'Datos de la  **************************************************************************************
		%>

		<table border="0" width="500" cellpadding="6" cellspacing="0">
			<%
			if FormatoEtiquetas="L" then
				%>
				<tr>
					<td width="138" class="tbltext1" align="left">
						<b>Actividad Principal: *</b>
					</td>
					<td class="textblack" width="362">
						<%
						if sModifica = "S"  then
							%>
							<textarea NAME="ATTIVITA" ROWS="7" COLS="40"><%=ATTIVITA%></textarea>
							<%
						else
							%>
							<b><%=ATTIVITA%></b>
							<%
						end if
						%>
					</td>
				<tr height="2">
					<td width="100%" colspan="2" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
				</tr>
				<%
			end if
			%>
			<tr>
				<td width="138" class="tbltext1" align="left">
					<b><%=TexAnuncio%>: *</b>
				</td>
				<td class="textblack" width="362">
					<%
					if sModifica = "S"  then
						%>
						<textarea NAME="TESTO" ROWS="7" COLS="40"><%=TESTO%></textarea>
						<%
					else
						%>
						<b><%=TESTO%></b>
						<input type="hidden" name="TESTO" value="<%=TESTO%>">
						<%
					end if
					%>
				</td>
			</tr>
		</table>

		<br>
		<table CELLPADDING="0" CELLSPACING="0" border="0" width="500">
			<tr height="2">
				<td class="sfondocommaz" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
			</tr>
		</table>
		<br>
		<!--td colspan="2" class="textblack"> Ingresar el nmero de postulantes a mostrar,
        de no ingresar un valor se mostrarn hasta 40 postulantes.</td>

		<td class="tbltext1" width="200"> <b>Nmero a visualizar</b></td>
		<td class="textblack">
		<input class="textblacka" type="text" name="txtNom" size="10" maxlength="5">
		</td>
		</tr>-->
		<input type="hidden" value="<%=sPubblica%>" name="sP">
		<input type="hidden" name="idRic" value="<%=nIdRic%>">
		<input type="hidden" name="dtTmst" value="<%=sDt_Tmst%>">
		<input name="txtOggi" value="<%=ConvDateToString(Date())%>" type="hidden">
		<input type="hidden" name="Idsede" value="<%=sIdsede%>">
	</form>





<%' FILTROS Y DETALLES ******************************************************************************************************%>


	<%
	if sModifica = "S"  then
		EditarFiltros
		%>
		<br>
		<table width="500" cellspacing="0" cellpadding="0" border="0" align="center">
		<tr>
			<td colspan="2">
				<table border="1" bordercolor="#006699" cellpadding="8" cellspacing="0" width="520" align="center">
					<tr  bordercolor= "white">
						<td colspan="2" align="center" class="tbltext1"><b>
						DETALLES DEL OFRECIMIENTO </b><br>
						</td>
					</tr>
					<tr  bordercolor= "white">
						<td colspan="2">
							<%
							MostrarDetallesRic(nIdRic)
							%>
						</td>
					</tr>
					<form method="post" name="FormOfferte" action="BUS_ElencoOfferte.asp">
						<tr align="center"  bordercolor= "white">
					        <td width="166">&nbsp;</td>
							<input type="hidden" name="CodTipoSel" value="1">
							<input type="hidden" name="dtTmst" value="<%=sDt_Tmst%>">
							<input type="hidden" name="idRic" value="<%=nIdRic%>">
							<input type="hidden" name="idAnnuncio" value="<%=nIdRic%>">
							<input type="hidden" name="NombrePrestacion" value="<%=sNombrePrestacion%>">
							<input type="hidden" name="IdTipoAnuncio" value="<%=nIdRic%>">
							<input type="hidden" name="cmbTipoRicerca" value="<%=TAviso%>">
							<input type="hidden" name="idProfilo" value="<%=nTipoProf%>">
							<input type="hidden" name="idArea" value="<%=nTipoArea%>">
							<input type="hidden" name="cmbTipoArea" value="<%=sTipoArea%>">
							<input type="hidden" name="cmbTipoProf" value="<%=sTipoProf%>">
							<input type="hidden" name="txtDtDal" value="<%=sDtDal%>">
							<input type="hidden" name="txtDtAl" value="<%=sDtAl%>">
							<input type="hidden" name="txtMunicipio" value="<%=nMunicipio%>">
							<input type="hidden" name="txtDtAvv" value="<%=sDtAvv%>">
							<input type="hidden" name="txtNPosti" value="<%=nPosti%>">
							<input type="hidden" name="cmbTipoRic" value="<%=nTipoRic%>">
							<input type="hidden" name="Idsede" value="<%=sIdsede%>">
							<input type="hidden" name="TEmp" value="<%=TEmp%>">
							<input type="hidden" name="Tipo" value="<%=Tipo%>">
							<input type="hidden" name="seleccion" value="<%=seleccion%>">
							<input type="hidden" name="formatoetiquetas" value="<%=formatoetiquetas%>">
							<td width="166">&nbsp;</td>
						</tr>
						<tr align="center"  bordercolor= "white">
							<td colspan="2"> <a class="textred" href="javascript:FormOfferte.submit()" onmouseover="javascript: window.status= ' '; return true">
								<b>Detalle del Ofrecimiento</b></a>
							</td>
						</tr>
					</form>
				</table>
			</tr>
		</table>
		<%
	else
		'MostrarTablaFiltros(nIdRic)
		%><br><%
		'MostrarTablaDetalles(nIdRic)
	end if
	%>
	<br>



<%' CANDIDATOS Y POSTULANTES ******************************************************************************************************%>


<table width="500" cellspacing="0" cellpadding="6" border="0" align="center">
	<%
	' Visualizar Postulantes     *****************************************************
	%>
	<!--tr align="center"  bordercolor= "white">
		<form method="post" name="FormElenco" action="RIC_ElencoCriteri.asp">
			<input type="hidden" name="dtTmst" value="<%=sDt_Tmst%>">
			<input type="hidden" name="idRic" value="<%=nIdRic%>">
			<input type="hidden" name="cmbTipoRicerca" value="<%=TAviso%>">
			<input type="hidden" name="idProfilo" value="<%=nTipoProf%>">
			<input type="hidden" name="idArea" value="<%=nTipoArea%>">
			<input type="hidden" name="cmbTipoArea" value="<%=sTipoArea%>">
			<input type="hidden" name="cmbTipoProf" value="<%=sTipoProf%>">
			<input type="hidden" name="txtDtDal" value="<%=sDtDal%>">
			<input type="hidden" name="txtDtAl" value="<%=sDtAl%>">
			<input type="hidden" name="txtNPosti" value="<%=nPosti%>">
			<input type="hidden" name="txtMunicipio" value="<%=nMunicipio%>">
			<input type="hidden" name="txtDtAvv" value="<%=sDtAvv%>">
			<input type="hidden" name="cmbTipoRic" value="<%=nTipoRic%>">
			<input type="hidden" name="Idsede" value="<%=sIdsede%>">
			<input type="hidden" name="TEmp" value="<%=TEmp%>">
			<input type="hidden" name="formatoetiquetas" value="<%=formatoetiquetas%>">
			<input type="hidden" name="seleccion" value="<%=seleccion%>">
			<input type="hidden" name="NombrePrestacion" value="<%=sNombrePrestacion%>">
			<input type="hidden" name="IdTipoAnuncio" value="<%=nIdRic%>">
			<td colspan="2"> <a class="textred" href="javascript:FormElenco.submit()" onmouseover="javascript: window.status= ' '; return true"><b>Visualizar Filtros</b></a> </td>
		</form>
	</tr-->


	<%

'response.write "<br>sGestionaPost: " & sGestionaPost	
If sGestionaPost = "S" then

   'modifica inizio sg 19092007 
 '''''''''''''' if cstr(Tipo) = "0"  then
    
    	'modifica fine sg 19092007 
    	
	' Visualizar Candidatos  *******************************************************************************
	''Agregado para visualizar la cantidad de cantidatos agrupados por estado de intermediaci�n.
	''Vany.
	
	dim SQLAgrupacionEstados
	set RSAgrupacionEstados = server.CreateObject("adodb.recordset")
	
    SQLAgrupacionEstados = SQLAgrupacionEstados & "select descripcion, estado, count(*) from richiesta_candidato rc "
    SQLAgrupacionEstados = SQLAgrupacionEstados & " inner join intermediacionestados ie "
    SQLAgrupacionEstados = SQLAgrupacionEstados & " on rc.cod_esito_sel = ie.estado "
    SQLAgrupacionEstados = SQLAgrupacionEstados & " where id_richiesta = " & nIdRic
    SQLAgrupacionEstados = SQLAgrupacionEstados & " group by descripcion, estado order by descripcion asc"
	
	'response.Write sqlagrupacionestados
	'response.end
	
	set RSAgrupacionEstados = cc.execute(SQLAgrupacionEstados)
	
	''modificacion en linea 1849 y en funcion VerCandidatosPorEstado()
	
	dim HayCandidatos

'response.write "empieza aqu�"
'response.end	
	HayCandidatos = false
	
	  if not RSAgrupacionEstados.eof then 
	                ''hay candidatos..
	                HayCandidatos = true
            	    
	                'response.Write "<tr><td align='center' class='tbltext1'><b>"
	                'response.Write "<br><br>Listado de candidatos por estado de intermediaci�n.<br><br>"
	                'response.Write "</b></td></tr>"
            	    
	                response.Write "<tr><td>"
	                response.Write "<table class='tbltext1' cellpadding='3' cellspacing='4' align='center' border='1' bordercolor='navy'>"
	                response.Write "<tr bgcolor='lightblue' bordercolor='midnightblue'>"
	                response.Write "<td colspan='2' align='center'><b><u>"
	                response.Write "<br>LISTADO DE CANDIDATOS POR ESTADO DE INTERMEDIACI�N<br>"
	                response.Write "</u></b></td>"
	                response.Write "</tr>"
	                response.Write "<tr bgcolor='lightblue' bordercolor='midnightblue'>"
    	            response.Write "<td align='center'><b>"
                    response.Write "Estado de Intermediaci�n"
                    response.Write "</b></td>"
                    response.Write "<td align='center'><b>"
                    response.Write "Cantidad de Candidatos"
                    response.Write "</b></td>"
	                response.Write "</tr>"
            	    
	                RSAgrupacionEstados.movefirst

	                'response.Write "<br>session('idgruppo')= " & session("idgruppo")
            	    
	                do while not RSAgrupacionEstados.eof
	                if (session("idgruppo")="25" or ((RSAgrupacionEstados.fields("estado") = "01" or RSAgrupacionEstados.fields("estado") = "09" or RSAgrupacionEstados.fields("estado") = "13" or RSAgrupacionEstados.fields("estado") = "14") and session("idgruppo")<>"25")) and sPubblica="S" then
	                    response.Write "<tr bordercolor='lightblue'>" 
	                    response.Write "<td align='left'><a class='tbltext1' href=" & chr(34) & "javascript:VerCandidatosPorEstado('" & RSAgrupacionEstados.fields(1) & "');" & chr(34) & "><b><u>"
	                    response.Write ucase(left(RSAgrupacionEstados.fields(0),1)) & lcase(mid(RSAgrupacionEstados.fields(0),2))
	                    response.Write "</u></b></a></td>"
	                    response.Write "<td align='center'><a class='tbltext1' href=" & chr(34) & "javascript:VerCandidatosPorEstado('" & RSAgrupacionEstados.fields(1) & "');" & chr(34) & "><b><u>"
	                    response.Write RSAgrupacionEstados.fields(2)
	                    response.Write "</u></b></a></td>"
	                    response.Write "</tr>"
	                end if
	                    RSAgrupacionEstados.movenext
	                loop	    
            	    
	                response.Write "</table>"
	                response.Write "</td></tr>"

	                response.Write "<tr><td align='center' class='tbltext1'><b>"
	                'response.Write "<br><br>"
	                response.Write "</b></td></tr>"	
	            else
	                ''no hay candidatos.
            	    
	            end if
	'response.Write "<br>Good!"
	set RSAgrupacionEstados = nothing
'fine modifica sg'

'''''''''''''sg end if 
     
	''''''sg 	if cstr(Tipo) = "0" and HayCandidatos = true then
	
	if  HayCandidatos = true and sPubblica="S" then

		%>
		<tr align="center">
			<form method="post" name="FormCandidati" id="FormCandidati" action="BUS_ElencoCandidati.asp">
				<input type="hidden" name="dtTmst" value="<%=sDt_Tmst%>">
				<input type="hidden" name="idRic" value="<%=nIdRic%>">
				<input type="hidden" name="cmbTipoRicerca" value="<%=nTipoRicerca%>">
				<input type="hidden" name="idProfilo" value="<%=nTipoProf%>">
				<input type="hidden" name="NombrePrestacion" value="<%=sNombrePrestacion%>">
				<input type="hidden" name="idArea" value="<%=nTipoArea%>">
				<input type="hidden" name="cmbTipoArea" value="<%=sTipoArea%>">
				<input type="hidden" name="cmbTipoProf" value="<%=sTipoProf%>">
				<input type="hidden" name="txtDtDal" value="<%=sDtDal%>">
				<input type="hidden" name="txtDtAl" value="<%=sDtAl%>">
				<input type="hidden" name="txtMunicipio" value="<%=nMunicipio%>">
				<input type="hidden" name="txtNPosti" value="<%=nPosti%>">
				<input type="hidden" name="txtDtAvv" value="<%=sDtAvv%>">
				<input type="hidden" name="cmbTipoRic" value="<%=nTipoRic%>">
				<input type="hidden" name="Idsede" value="<%=sIdsede%>">
				<input type="hidden" name="TEmp" value="<%=TEmp%>">
				<input type="hidden" name="Tipo" value="<%=Tipo%>">
				<input type="hidden" name="formatoetiquetas" value="<%=formatoetiquetas%>">
				<input type="hidden" name="seleccion" value="<%=seleccion%>">
				<input type="hidden" name="estado" value="<%=estado%>">
				<td>
					<a class="tbltext1" href="javascript:FormCandidati.submit();" onmouseover="javascript: window.status= ' '; return true">
                    	<b><u>VISUALIZAR TODOS LOS ESTADOS >></u></b>
					</a>
				</td>
			</form>
		</tr>
		<%
		'response.Write "<br>Good!"
		if cstr(Tipo) = "1" then	
		'response.Write "<br>Good!"
		%>
		<tr align="center">
			<%'Response.Write "TEmp: " & TEmp & "<br>"
			'if TEmp = "" then
				%>
				<form method="post" name="FrmPosiblesPostulantes" action="BUS_AvvioRicerca.asp" onsubmit="return CnfRicerca()">
					<input type="hidden" name="cmbTipoRic" value="<%=nTipoRic%>">
					<input type="hidden" name="cmbTipoRicerca" value="<%=nTipoRicerca%>">
					<input type="hidden" name="idProfilo" value="<%=nTipoProf%>">
					<input type="hidden" name="idArea" value="<%=nTipoArea%>">
					<input type="hidden" name="cmbTipoArea" value="<%=sTipoArea%>">
					<input type="hidden" name="cmbTipoProf" value="<%=sTipoProf%>">
					<input type="hidden" name="txtDtDal" value="<%=sDtDal%>">
					<input type="hidden" name="txtDtAl" value="<%=sDtAl%>">
					<input type="hidden" name="txtNPosti" value="<%=nPosti%>">
					<input type="hidden" name="txtMunicipio" value="<%=nMunicipio%>">
					<input type="hidden" name="txtDtAvv" value="<%=sDtAvv%>">
					<input type="hidden" name="IdRic" value="<%=nIDRic%>">
					<input type="hidden" name="dtTmst" value="<%=sDt_Tmst%>">
					<input type="hidden" name="Idsede" value="<%=sIdsede%>">
					<input type="hidden" name="txttipoanuncio" value="<%=TAnuncio%>">
					<!--inizio 13/02/2003 -->
					<input type="hidden" name="txtNom" id="txtNom">
					<input type="hidden" name="TEmp" value="<%=TEmp%>">
					<input type="hidden" name="Tipo" value="<%=Tipo%>">
					<input type="hidden" name="formatoetiquetas" value="<%=formatoetiquetas%>">
					<!--fine 13/02/2003 -->
					<%'PartecipaPrj sIdSede
					'response.Write "sIdSede)-->" & clng(sIdSede) & "<--session(creator)-->" & session("creator")
					'viene visuallizata la possibilita di vederele le candidature solo alla OE

					'response.Write "<br>session('idgruppo')= " & session("idgruppo")

					if session("idgruppo")="25" and sPubblica="S" then
					'response.Write session("idgruppo")
					%>
					<br>
					<table border="0" cellspacing="0" width="520" align="center">
						<tr>
							<td align="center">
								<a class="textred" href="javascript:FrmPosiblesPostulantes.submit()" onmouseover="javascript: window.status= ' '; return true">
							<b>Posibles Postulantes </b>
								</a>
								<br /><br />
							</td>
						</tr>
					</table>		
					<%
					end if%>
				</form>
				<%
			'end if
			%>
			<!--<input type="image"  src="<%=Session("progetto")%>/images/conferma.gif" title="" name="submit" border="0" >-->
		</tr>



		<%
		end if
		%>

	<%
	elseif cstr(Tipo) = "1" then
	'response.write "Alex"
		
	%>
		<tr align="center">
			<%
			'if TEmp = "" then
				%>
				<form method="post" name="FrmPosiblesPostulantes" action="BUS_AvvioRicerca.asp" onsubmit="return CnfRicerca()">
					<input type="hidden" name="cmbTipoRic" value="<%=nTipoRic%>">
					<input type="hidden" name="cmbTipoRicerca" value="<%=nTipoRicerca%>">
					<input type="hidden" name="idProfilo" value="<%=nTipoProf%>">
					<input type="hidden" name="idArea" value="<%=nTipoArea%>">
					<input type="hidden" name="cmbTipoArea" value="<%=sTipoArea%>">
					<input type="hidden" name="cmbTipoProf" value="<%=sTipoProf%>">
					<input type="hidden" name="txtDtDal" value="<%=sDtDal%>">
					<input type="hidden" name="txtDtAl" value="<%=sDtAl%>">
					<input type="hidden" name="txtNPosti" value="<%=nPosti%>">
					<input type="hidden" name="txtMunicipio" value="<%=nMunicipio%>">
					<input type="hidden" name="txtDtAvv" value="<%=sDtAvv%>">
					<input type="hidden" name="IdRic" value="<%=nIDRic%>">
					<input type="hidden" name="dtTmst" value="<%=sDt_Tmst%>">
					<input type="hidden" name="Idsede" value="<%=sIdsede%>">
					<input type="hidden" name="txttipoanuncio" value="<%=TAnuncio%>">
					<!--inizio 13/02/2003 -->
					<input type="hidden" name="txtNom" id="txtNom">
					<input type="hidden" name="TEmp" value="<%=TEmp%>">
					<input type="hidden" name="Tipo" value="<%=Tipo%>">
					<input type="hidden" name="formatoetiquetas" value="<%=formatoetiquetas%>">
					<!--fine 13/02/2003 -->
					<%'PartecipaPrj sIdSede
					'response.Write "sIdSede)-->" & clng(sIdSede) & "<--session(creator)-->" & session("creator")
					'viene visuallizata la possibilita di vederele le candidature solo alla OE
					'response.write session("idgruppo")="25"
					'response.write sPubblica

					if session("idgruppo")="25" and sPubblica="S" then
					%>
					<br>
					<table border="0" cellspacing="0" width="520" align="center">
						<tr>
							<td align="center">
								<a class="textred" href="javascript:FrmPosiblesPostulantes.submit()" onmouseover="javascript: window.status= ' '; return true">
							<b>Posibles Postulantes </b>
								</a>
								<br /><br />
							</td>
						</tr>
					</table>		
					<%
					end if%>
				</form>
				<%
			'end if
			%>
			<!--<input type="image"  src="<%=Session("progetto")%>/images/conferma.gif" title="" name="submit" border="0" >-->
		</tr>

		<%
		end if
	end if
	%>
	<%
	' Visualizar Beneficiarios Asignados al Proyecto  ****************************************************************
	%>
	<%
	If sGestionaPost = "S" then
		%>
		<tr align="center"  bordercolor= "white">
			<td>
				<%
				if Proyecto <> "" then
					%>
					<a class="textred" href="javascript:FormVerProyectos.submit()" onmouseover="javascript: window.status= ' '; return true">
						<b>Visualizar Beneficiarios Asignados al Proyecto</b>
					</a>
					<%
				end if
				%>
				<form name="FormVerProyectos" method = "post" action="/Pgm/Busqueda/BUS_VerDatosProyectos.asp">
					<input type="hidden" value="<%=Gecal%>" name="Gecal">
					<input type="hidden" value="<%=Programa%>" name="Programa">
					<input type="hidden" value="<%=Proyecto%>" name="Proyecto">
					<input type="hidden" value="<%=nIdRic%>" name="Codice">
					<input type="hidden" value="<%=sIdSede%>" name="IdSede">
					<input type="hidden" name="Tipo" value="<%=Tipo%>">
					<input type="hidden" value="/pgm/Busqueda/BUS_Modifica.asp" name="Pagina">
				</form>
			<br /><br />
			</td>
		</tr>
		<%
	end if
	%>
	<table width="600" cellspacing="0" cellpadding="0" border="0" align="right">
		<tr align="center">
			<td>
	<%
	' Busqueda  ****************************************************************
	%>
	<%'Response.Write "--------" & sidsede & "--------" & session("creator") 
	If sGestionaPost = "S" then
		%>
		<%
		'select case session("Progetto")
		'case "/SUP","/SPIMM","/BDLI"
		'	nome_form="RIC_ImmAvvioRicerca.asp"
		'case else
			'nome_form="BUS_AvvioRicerca.asp"
		'end select
		'Response.Write "nTipoRicerca: " & nTipoRicerca
		if cstr(Tipo) = "0" then
		%>

<!--		<tr align="center">-->
			<%
			'sg inizio 20092007
			
			'if TEmp = "" then
			'sg fine 20092007
			'gb 052008
				%>
				<form method="post" name="FrmAR2" action="BUS_AvvioRicerca.asp" onsubmit="return CnfRicerca()">
					<input type="hidden" name="cmbTipoRic" value="<%=nTipoRic%>">
					<input type="hidden" name="cmbTipoRicerca" value="<%=nTipoRicerca%>">
					<input type="hidden" name="idProfilo" value="<%=nTipoProf%>">
					<input type="hidden" name="idArea" value="<%=nTipoArea%>">
					<input type="hidden" name="cmbTipoArea" value="<%=sTipoArea%>">
					<input type="hidden" name="cmbTipoProf" value="<%=sTipoProf%>">
					<input type="hidden" name="txtDtDal" value="<%=sDtDal%>">
					<input type="hidden" name="txtDtAl" value="<%=sDtAl%>">
					<input type="hidden" name="txtNPosti" value="<%=nPosti%>">
					<input type="hidden" name="txtMunicipio" value="<%=nMunicipio%>">
					<input type="hidden" name="txtDtAvv" value="<%=sDtAvv%>">
					<input type="hidden" name="IdRic" value="<%=nIDRic%>">
					<input type="hidden" name="dtTmst" value="<%=sDt_Tmst%>">
					<input type="hidden" name="Idsede" value="<%=sIdsede%>">
					<input type="hidden" name="txttipoanuncio" value="<%=TAnuncio%>">
					<!--inizio 13/02/2003 -->
					<input type="hidden" name="txtNom" id="txtNom">
					<input type="hidden" name="TEmp" value="<%=TEmp%>">
					<input type="hidden" name="Tipo" value="<%=Tipo%>">
					<input type="hidden" name="formatoetiquetas" value="<%=formatoetiquetas%>">
					<!--fine 13/02/2003 -->
					<%if clng(sIdSede)=clng(session("creator")) then
						 'PartecipaPrj 0					  
					  else
						 'PartecipaPrj sIdSede
					  end if	 
					'if (cdate(ConvStringToDate(sDtAl)) => date) then
					%>

					<table border="0" cellspacing="0" width="520" align="center">
						<tr>
							<td align="center">
								<input type="image" onload="EnLoad()" src="<%=Session("progetto")%>/images/lente.gif" title="" border="0" align="absBottom" id="image1" name="image1">
							</td>
						</tr>
					</table>
					<%
					'end if
					%>
				</form>
				<%
				'sg inizio 20092007
			'end if
			'sg fine 20092007
			%>
			<!--<input type="image"  src="<%=Session("progetto")%>/images/conferma.gif" title="" name="submit" border="0" >-->
		<!--</tr>-->
		<%
		elseif cstr(Tipo) = "1" then

	    ' Visualizar Postulantes     *****************************************************
	    'response.Write "sIdSede)-->" & clng(sIdSede) & "<--session(creator)-->" & session("creator")
	    'viene visuallizata la possibilita di vederele le candidature solo alla OE
	    if session("idgruppo")="25" and sPubblica="S" then%>
		    <!--<tr><td>-->

		    <table border="0" cellpadding="6" cellspacing="0" width="500">
			    <tr align="center">
			 	    <td align="center"  colspan="2">
					    <a class="textred" onmouseover="window.status=' '; return true" href="javascript:VisCandidati()">
					    <img src="<%=Session("Progetto")%>/images/lente2.jpg" border="0" title="Visualizar Postulantes">
					    </a>
			 	    </td>
			    <%'PartecipaPrj sIdSede%>
			    </tr>
		    </table>
		    <!--</td></tr>-->
		    <%end if
		end if
	end if
	%>
	
	

	<form method="post" name="frmVisCand" action="BUS_ElencoCandidati.asp">
		<input type="hidden" name="dtTmst" value="<%=sDt_Tmst%>">
		<input type="hidden" name="idRic" value="<%=nIdRic%>">
		<input type="hidden" name="cmbTipoRicerca" value="<%=nTipoRicerca%>">
		<input type="hidden" name="idProfilo" value="<%=nTipoProf%>">
		<input type="hidden" name="NombrePrestacion" value="<%=sNombrePrestacion%>">
		<input type="hidden" name="idArea" value="<%=nTipoArea%>">
		<input type="hidden" name="cmbTipoArea" value="<%=sTipoArea%>">
		<input type="hidden" name="cmbTipoProf" value="<%=sTipoProf%>">
		<input type="hidden" name="txtDtDal" value="<%=sDtDal%>">
		<input type="hidden" name="txtDtAl" value="<%=sDtAl%>">
		<input type="hidden" name="txtNPosti" value="<%=nPosti%>">
		<input type="hidden" name="txtMunicipio" value="<%=nMunicipio%>">
		<input type="hidden" name="txtDtAvv" value="<%=sDtAvv%>">
		<input type="hidden" name="cmbTipoRic" value="<%=nTipoRic%>">
		<input type="hidden" name="Idsede" value="<%=sIdsede%>">
		<input type="hidden" name="TEmp" value="<%=TEmp%>">
		<input type="hidden" name="Tipo" value="<%=Tipo%>">
		<input type="hidden" name="formatoetiquetas" value="<%=formatoetiquetas%>">
		<input type="hidden" name="seleccion" value="<%=seleccion%>">
	</form>

	<form method="post" name="frmEnvMailEmp" action="BUS_MailEmpleadores.asp">
		<input type="hidden" name="idRic" value="<%=nIdRic%>">
	</form>


	<!--<form name="frmVisCand" method="POST" action="BUS_ElencoCandidature.asp">
			<input type="hidden" name="cmbTipoRicerca" value="<%=TAviso%>">
			<input type="hidden" name="idAnnuncio" value="<%=nIDRic%>">
			<input type="hidden" name="NombrePrestacion" value="<%=sNombrePrestacion%>">
			<input type="hidden" name="idAreaProf" value="<%=nTipoArea%>">
			<input type="hidden" name="idFigProf" value="<%=nTipoProf%>">

			<input type="hidden" name="settore" value="<%=sSettoreDesc%>">
			<input type="hidden" name="area" value="<%=sTipoArea%>">
			<input type="hidden" name="figura" value="<%=sTipoProf%>">
			<input type="hidden" name="NumPosti" value="<%=nPosti%>">
			<input type="hidden" name="etichetta" value="<%=etichetta%>">
			<input type="hidden" name="visualizza" value="<%=visualizza%>">

			<input type="hidden" name="idSede" value="<%=Request("IdSede")%>">
			<input type="hidden" name="appoIdSede" value="<%=appoIdSede%>">
			<input type="hidden" name="Proyecto" value="<%=Proyecto%>">
			<input type="hidden" name="TEmp" value="<%=TEmp%>">
			<input type="hidden" name="Tipo" value="<%=Tipo%>">
	   </form>-->

<%' ENVIA Y RETROCEDE  **********************************************************************************************************************************************************%>

	<!--form method="post" name="FrmAR2" action="RIC_Ricerca.asp" onsubmit="return CnfRicerca()"-->
	<table width="500" cellspacing="0" cellpadding="0" border="0" align="center">
		<tr align="center">
			<td align="center">
				<form method="post" name="FrmAR" action="BUS_Busqueda1.asp">
					<!--<td>-->
						<%if Request("Prima") <> "" then Response.Write "<input type=hidden name=Prima value=S>"%>
						<input type="hidden" name="idsede" value="<%=sIdsede%>">
						<input type="hidden" name="modo" value="2">
						<input type="hidden" name="TEmp" value="<%=TEmp%>">
						<input type="hidden" name="Tipo" value="<%=Tipo%>">
						<!--<a href="Ric_RicRichiesta.asp" ONMOUSEOVER="window.status = ' '; return true"><img src="<%=Session("progetto")%>/images/Indietro.gif" title="Pagina precedente" border="0" align="absBottom"></a> -->
						<!--<input type="image" onload="EnLoad()" src="<%=Session("progetto")%>/images/indietro.gif" title="Pagina precedente" border="0" align="absBottom" id="image2" name="image2">-->
					<!--</td>-->
				</form>
			
            <%if sModifica = "S" OR sGestionaPost = "S" OR sReactiva = "S" then 
			        'response.Write "sono qui"
			        if session("idgruppo")="25" and sPubblica="N" then ' se sono una OE pubblico anche l'annuncio modificandolo
			        %><table width="500" cellspacing="0" cellpadding="0" border="1" bordercolor="#006699" align="left">
			        <tr align="center"><td align="center" ><b>Modifica y publica</b>
			            <a  href="javascript:Enviar();"><br><img onload="EnLoad()" src="<%=Session("Progetto")%>/images/conferma.gif" border="0" align="absBottom"></a>
			        </tr></td></table>
			        <%
			        elseif cdate(sDtAvv)<date and session("idgruppo")="37" then
			        ' Si ya se pas� la fecha de incorporaci�n, la empresa no puede modificar la vacante. El centro de atenci�n si.
			        else ' altrimenti lo modifico solo
				    %><a href="javascript:Enviar();"><img onload="EnLoad()" src="<%=Session("Progetto")%>/images/conferma.gif" border="0" align="absBottom"></a><%
	                end if%>		
	                
    			<%end if %>
			</td>
		</tr>
	</table>		 
</table>
</table>
	<%
End Sub


'sub INICIO *******************************************************************************************************************************************************************
Sub Inizio()
%>
<% 	'Response.End
	End Sub
'**********************************************************************************************************************************************************************************


	Sub Messaggio(Testo)
%>
		<br><br>
		<table width="500" align="center" border="0" cellspacing="2" cellpadding="1">
			<tr height="20">
			  	<td class="tbltext3" align="middle" width="500">
					<b><%=Testo%></b>
				</td>
			</tr>
		</table>
		<br><br>
<%
	End Sub


private sub EditarFiltros()
	%>
	<table border="1" bordercolor="#006699" cellpadding="8" cellspacing="0" width="520" align="center">
		<tr bordercolor= "white">
			<td colspan="2" align="center" class="tbltext1">
				<b>ESPECIFICACIONES DEL PERFIL SOLICITADO</b>
			</td>
		</tr>
		<br>
		<tr bordercolor= "white">
			<td colspan="2" align="center" class="tbltext1">
				<table width="500" align="center" bordercolor="LightGrey" border="1" cellspacing="0" cellpadding="4">
					<tr height="20">
						<form method="post" name="FrmC5" action="BUS_VisCriteri.asp">
							<input type="hidden" name="idTab" value="TISTUD">
							<input type="hidden" name="Area" value="LSTUD">
							<input type="hidden" name="Specifico" value="TSTUD">
							<input type="hidden" name="Condizione" value="COD_STAT_STUD">
							<input type="hidden" name="TEmp" value="<%=TEmp%>">
							<input type="hidden" name="Tipo" value="<%=Tipo%>">
							<input type="hidden" name="IdRic" value="<%=nIdRic%>">
							<input type="hidden" name="dtTmst" value="<%=sDt_Tmst%>">
							<input type="hidden" name="Idsede" value="<%=sIdsede%>">
							<td width="120" valign="middle" class="sfondomenu"> <a href="javascript:document.FrmC5.submit();" ONMOUSEOVER="window.status = ' '; return true"><u class="tbltext0"><b>Nivel Educativo</b></u></a></td>
						</form>
						<form method="post" name="FrmC9" action="BUS_VisCriteri.asp">
				  			<input type="hidden" name="idTab" value="ESPRO">
				  			<input type="hidden" name="Area" value="DIZ_TAB">
				  			<input type="hidden" name="Specifico" value="DIZ_DATI">
				  			<input type="hidden" name="Condizione" value>
							<input type="hidden" name="TEmp" value="<%=TEmp%>">
							<input type="hidden" name="Tipo" value="<%=Tipo%>">
				  			<input type="hidden" name="IdRic" value="<%=nIdRic%>">
				  			<input type="hidden" name="dtTmst" value="<%=sDt_Tmst%>">
				  			<input type="hidden" name="Idsede" value="<%=sIdsede%>">
							<td width="150" valign="middle" class="sfondomenu"> <a href="javascript:document.FrmC9.submit();" ONMOUSEOVER="window.status = ' '; return true"><u class="tbltext0"><b>Experiencia Laboral</b></u></a></td>
						</form>
						<form method="post" name="FrmC10" action="BUS_VisCriteri.asp">
							<input type="hidden" name="idTab" value="PERS_AREAPROF">
							<input type="hidden" name="Area" value="AREE_PROFESSIONALI">
							<input type="hidden" name="Specifico" value="AREE_PROFESSIONALI">
							<input type="hidden" name="Condizione" value>
							<input type="hidden" name="TEmp" value="<%=TEmp%>">
							<input type="hidden" name="Tipo" value="<%=Tipo%>">
							<input type="hidden" name="IdRic" value="<%=nIdRic%>">
							<input type="hidden" name="dtTmst" value="<%=sDt_Tmst%>">
							<input type="hidden" name="Idsede" value="<%=sIdsede%>">
							<td width="120" valign="middle" class="sfondomenu">
								<a href="javascript:document.FrmC10.submit();" ONMOUSEOVER="window.status = ' '; return true"><u class="tbltext0"><b>Intereses Ocupacionales</b></u></a>
							</td>
						</form>
						<!--VANINA 24/07/2006-->
						<!--AEM 29/04/2013 <form method="post" name="FrmC11" action="BUS_VisCriteri.asp">
							<input type="hidden" name="idTab" value="PERS_TIPOLOGIA">
							<input type="hidden" name="Area" value="AREA_CORSO">
							<input type="hidden" name="Specifico" value="AREA_CORSO">
							<input type="hidden" name="Condizione" value>
							<input type="hidden" name="TEmp" value="<%=TEmp%>">
							<input type="hidden" name="Tipo" value="<%=Tipo%>">
							<input type="hidden" name="IdRic" value="<%=nIdRic%>">
							<input type="hidden" name="dtTmst" value="<%=sDt_Tmst%>">
							<input type="hidden" name="Idsede" value="<%=sIdsede%>">
							<td width="120" valign="middle" class="sfondomenu">
								<a href="javascript:document.FrmC11.submit();" ONMOUSEOVER="window.status = ' '; return true"><u class="tbltext0"><b>&Aacute;reas de Inter&eacute;s</b></u></a>
							</td>
						</form>
						<form method="post" name="FrmC8" action="BUS_VisCriteri.asp">
				  			<input type="hidden" name="idTab" value="SEL_TAB">
				  			<input type="hidden" name="Area" value="DIZ_TAB">
				  			<input type="hidden" name="Specifico" value="DIZ_DATI">
				  			<input type="hidden" name="Condizione" value>
				  			<input type="hidden" name="IdRic" value="<%=nIdRic%>">
							<input type="hidden" name="TEmp" value="<%=TEmp%>">
							<input type="hidden" name="Tipo" value="<%=Tipo%>">
				  			<input type="hidden" name="dtTmst" value="<%=sDt_Tmst%>">
				  			<input type="hidden" name="Idsede" value="<%=sIdsede%>">
							<td width="120" valign="middle" class="sfondomenu"> <a href="javascript:document.FrmC8.submit();" ONMOUSEOVER="window.status = ' '; return true"><u class="tbltext0"><b>Varios</b></u></a></td>
				  		</form>-->
						</tr>
						<!--<tr height="20">
						<form method="post" name="FrmC1" action="BUS_VisCriteri.asp">
							<input type="hidden" name="idTab" value="PERS_CONOSC">
							<input type="hidden" name="Area" value="AREA_CONOSCENZA">
							<input type="hidden" name="Specifico" value="CONOSCENZE">
							<input type="hidden" name="Condizione" value="COD_GRADO_CONOSC">
							<input type="hidden" name="TEmp" value="<%=TEmp%>">
							<input type="hidden" name="Tipo" value="<%=Tipo%>">
							<input type="hidden" name="IdRic" value="<%=nIdRic%>">
							<input type="hidden" name="dtTmst" value="<%=sDt_Tmst%>">
							<input type="hidden" name="Idsede" value="<%=sIdsede%>">
							<td width="120" valign="middle" class="sfondomenu"> <a href="javascript:document.FrmC1.submit();" ONMOUSEOVER="window.status = ' '; return true"><u class="tbltext0"><b>Conocimiento</b></u></a></td>
						</form>
						<form method="post" name="FrmC2" action="BUS_VisCriteri.asp">
							<input type="hidden" name="idTab" value="PERS_CAPAC">
							<input type="hidden" name="Area" value="AREA_CAPACITA">
							<input type="hidden" name="Specifico" value="CAPACITA">
							<input type="hidden" name="Condizione" value="GRADO_CAPACITA">
							<input type="hidden" name="TEmp" value="<%=TEmp%>">
							<input type="hidden" name="Tipo" value="<%=Tipo%>">
							<input type="hidden" name="IdRic" value="<%=nIdRic%>">
							<input type="hidden" name="dtTmst" value="<%=sDt_Tmst%>">
							<input type="hidden" name="Idsede" value="<%=sIdsede%>">
					        <td width="120" valign="middle" class="sfondomenu"> <a href="javascript:document.FrmC2.submit();" ONMOUSEOVER="window.status = ' '; return true"><u class="tbltext0"><b>Desempe�o</b></u></a></td>
						</form>
						<form method="post" name="FrmC3" action="BUS_VisCriteri.asp">
							<input type="hidden" name="idTab" value="PERS_COMPOR">
							<input type="hidden" name="Area" value="AREA_COMPORTAMENTI">
							<input type="hidden" name="Specifico" value="COMPORTAMENTI">
							<input type="hidden" name="Condizione" value="GRADO_PERS_COMPOR">
							<input type="hidden" name="TEmp" value="<%=TEmp%>">
							<input type="hidden" name="Tipo" value="<%=Tipo%>">
							<input type="hidden" name="IdRic" value="<%=nIdRic%>">
							<input type="hidden" name="dtTmst" value="<%=sDt_Tmst%>">
							<input type="hidden" name="Idsede" value="<%=sIdsede%>">
						    <td width="150" valign="middle" class="sfondomenu"> <a href="javascript:document.FrmC3.submit();" ONMOUSEOVER="window.status = ' '; return true"><u class="tbltext0"><b>Comportamiento</b></u></a></td>
						</form>-->
						<!-- <form method="post" name="Frm4" action="RIC_VisCriteri.asp">				<input type="hidden" name="idTab" value="PERS_COMPET">				<input type="hidden" name="Area" value>				<input type="hidden" name="Specifico" value>				<input type="hidden" name="Condizione" value>					<input type="hidden" name="IdRic" value="<%'=nIdRic%>">				<input type="hidden" name="dtTmst" value="<%'=sDt_Tmst%>">				<td width="100" valign="middle" class="sfondomenu">					<!--a href="javascript:document.Frm4.submit();" ONMOUSEOVER="window.status = ' '; return true"--><!--<u class="tbltext0"><b>Competenze</b></u></a>-->
						<!-- </td>
						</form
						<form method="post" name="FrmC7" action="BUS_VisCriteri.asp">
							<input type="hidden" name="idTab" value="PERS_DISPON">
							<input type="hidden" name="Area" value="CL_VD">
							<input type="hidden" name="Specifico" value="TDISP">
							<input type="hidden" name="Condizione" value>
							<input type="hidden" name="TEmp" value="<%=TEmp%>">
							<input type="hidden" name="Tipo" value="<%=Tipo%>">
							<input type="hidden" name="IdRic" value="<%=nIdRic%>">
							<input type="hidden" name="dtTmst" value="<%=sDt_Tmst%>">
							<input type="hidden" name="Idsede" value="<%=sIdsede%>">
						    <td width="120" valign="middle" class="sfondomenu"> <a href="javascript:document.FrmC7.submit();" ONMOUSEOVER="window.status = ' '; return true"><u class="tbltext0"><b>Disponibilidad</b></u></a></td>
						</form>
						<form method="post" name="FrmC6" action="BUS_VisCriteri.asp">
							<input type="hidden" name="idTab" value="PERS_VINCOLI">
							<input type="hidden" name="Area" value="CL_VD">
							<input type="hidden" name="Specifico" value="TVINC">
							<input type="hidden" name="Condizione" value>
							<input type="hidden" name="TEmp" value="<%=TEmp%>">
							<input type="hidden" name="Tipo" value="<%=Tipo%>">
							<input type="hidden" name="IdRic" value="<%=nIdRic%>">
							<input type="hidden" name="dtTmst" value="<%=sDt_Tmst%>">
							<input type="hidden" name="Idsede" value="<%=sIdsede%>">
						    <td width="120" valign="middle" class="sfondomenu"> <a href="javascript:document.FrmC6.submit();" ONMOUSEOVER="window.status = ' '; return true"><u class="tbltext0"><b>Limitaciones</b></u></a></td>
						</form>
					</tr>-->
				</table>
			</td>
		</tr>
		<tr  bordercolor= "white">
			<td colspan="2">
				<%
				MostrarFiltros(nIdRic)
				%>
			</td>
		</tr>
	</table>
	<%
end sub

private sub PartecipaPrj(sd)
	
	if sd<>0 then
		Sql =	"SELECT id_bando, desc_bando " & _
			"FROM bando WHERE id_proj in (select id_proj_relazionato from progetto where id_proj in " & _
			" (select id_proj from bando " & _
			" where id_bando in (select id_bando from domanda_iscr_imp " & _
			" where ID_SEDE_IMPRESA  = " & sd & " AND COD_ESITO IS NULL AND ESITO_GRAD = 'S'))) " & _
			"and tipo_bando = 'P' "
	else
		Sql =	"SELECT id_bando, desc_bando " & _
			"FROM bando WHERE tipo_bando = 'P' "
	end if	
	'SQL = TransformPLSQLToTSQL (SQL)
	'Response.Write sql
	'Response.End 
	set  RsBandi = cc.execute(Sql)

	if not RsBandi.EOF then
		%>
		<table border="1" bordercolor="#006699" cellpadding="8" cellspacing="0" width="520" align="center">
			<tr bordercolor= "white">
				<td colspan="2" align="center" class="tbltext1">
					<b>LOCALIZA SOLO MIEMBROS DE LOS SIGUIENTES PROGRAMAS</b>
				</td>
			</tr>
			<br>
			
			<%i=1
			do while not RSBandi.eof%>
			<tr bordercolor= "white">
				<td colspan="2" align="center" class="tbltext1">
					<table width="500" align="center" bordercolor="LightGrey" border="0" cellspacing="0" cellpadding="4">
						<tr height="20">
							<td colspan="2" align="center" class="tbltext1">
								<input type="checkbox" name="ChkIdBando<%=i%>" value="<%=RSBandi("id_bando")%>"><b class="tbltext1"><%=RSBandi("desc_bando")%></b>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<%RSBandi.movenext
	          i= i + 1
	          loop%>
			<input type="hidden" name="NumIdBandi" value="<%=i - 1%>">
			
			<tr bordercolor= "white">
				<td colspan="2" align="center" class="tbltext1">
					<table width="500" align="center" bordercolor="LightGrey" border="0" cellspacing="0" cellpadding="4">
						<tr height="20">
							<td colspan="2" align="center" class="tbltext1">
							<%' esito_grad potrebbe asssumere i seguenti valori:
							' N=Riserva
							' S=Ammesso (vincitore)
							' NULL e cod_esito valorizzato=Escluso
							' vengono utilizzati solo i primi due per i test in quanto i non ammessi 
							' non dovranno generalmente essere estratti per la funzione di incrocio.
							%>
							<input type="radio" name="TipoPersIscr" value="T" checked>Inscriptos
							<input type="radio" name="TipoPersIscr" value="N">Suplentes
							<input type="radio" name="TipoPersIscr" value="S">Titulares
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		<%
	end if	
end sub

%>
<!--#include Virtual ="/Include/closeconn.asp"-->
<!--#include Virtual="/strutt_coda2.asp"-->
