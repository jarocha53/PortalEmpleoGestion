<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include Virtual = "/util/portallib.asp"-->
<!--#include Virtual="/strutt_testa2.asp"-->
<!--#include virtual = "/include/SysFunction.asp"-->
<!--#include virtual = "/include/OpenConn.asp"-->
<!--#include virtual="/util/dbUtil.asp"-->
<!--#include virtual = "/include/ControlDateVB.asp"-->

<div align="center">
<br>
<%
dim sIdpers
dim vData
dim vTipoDoc
dim vEnteRil
dim vDataRil
dim vComune  
dim vComRil
dim vDataScad  
dim vNazione
dim vProvRil
dim vAdattam
dim vNumDoc
dim sSQL
dim sSQL1
dim errore

vData= Request.Form("txtTMST")

sIdpers=clng(Request.Form("txtIdPers"))  
sIdDocOld=Request.Form ("txtNDocOld")
sTipoDoc= UCASE(Request.Form("txtTipoDoc"))

vEnteRil=Request.Form("cmbEnteRil") 

vDataRil = UCASE(Request.Form("txtDataRil"))
vDataScad = Request.Form("txtDataScad")

vProvRil = Request.Form("cmbProvRil") 

vComRil = Request.Form("txtComuneHid")

vNazione = Request.Form("cmbNazione")

vNumDoc =UCASE(Request.Form("txtNDoc"))
vNumDoc= Replace(server.HTMLEncode(vNumDoc), "'", "''")

chiave= sIdpers & "|" & sTipoDoc

%>
<!--#include virtual = "/include/DecComun.asp"-->
<%
Errore="0"

' aggiornamento di PERS_DOC

sSQL = "UPDATE PERS_DOC " &_
	   "SET ID_DOC='" & vNumDoc & "', " 

if vComRil <> "" then
   sSQL = sSQL &  "COM_RIL='" & vComRil & "', "
else
   sSQL = sSQL &  "COM_RIL='', "
end if  
if vProvRil <> "" then
   sSQL = sSQL & "PRV_RIL='" & vProvRil & "',"
else
   sSQL = sSQL & "PRV_RIL='', "
end if   	                
if vNazione <>"" then
   sSQL = sSQL & "COD_STATO_RIL='" & vNazione & "', "
else
   sSQL = sSQL & "COD_STATO_RIL='', "
end if
if vEnteRil <>"" then
	ssql = ssql & "COD_ENTE_RIL='" & vEnteRil  & "', " 					
else
	ssql = ssql & "COD_ENTE_RIL='', "	
end if				
if vDataRil <>"" then
	ssql = ssql & "DT_RIL_DOC=" & convdatetodb(convstringtodate(vDataRil)) & ", "
else
	ssql = ssql & "DT_RIL_DOC='', "
end if	
if vdataScad <>"" then
	ssql = ssql & "DT_FIN_VAL=" & convdatetodb(convstringtodate(vDataScad)) & ", "
else
	ssql = ssql & "DT_FIN_VAL='', "	
end if	
sSQL = sSQL &   "DT_TMST=" & convdatetodb(Now()) & _
				" WHERE ID_PERSONA = " & sIdpers &_
				" AND COD_DOCST='" & sTipoDoc &"'" &_
				" AND ID_DOC='" & sIdDocOld & "'"
					 			 						
%><!--#include virtual = "/include/ValInfPers.asp"--><%

cc.begintrans
Errore=EseguiNoCTrace(ucase(mid(session("progetto"),2)),chiave, "AUTCERT",session("idutente"),sIdpers,"P", "MOD", sSQL, 1, vData,cc)

if Errore = "0" then	
	sValInf=ValInfPers(sIdpers,cc)
    Errore=sValInf

	IF Errore="0" then
		CC.CommitTrans%>
			<form name="indietro" action="UTE_VisDocPers.asp" method="post">
				<input type="hidden" name="IdPers" value="<%=sIdpers%>">			
			</form>
			<script>
				alert("Modificación efectuada.");
				indietro.submit()
			</script>	
<%
	else	
		CC.RollbackTrans
%>		
<br>
<!--#include file="menu.asp"-->
<br>			

			<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
				<tr height="18">
					<td class="sfondomenu" width="67%" height="18"><span class="tbltext0"><b>&nbsp;DOCUMENTI PERSONALI</b></span></td>
					<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
					<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">(*) campo obligatorio</td>
				</tr>
			</table>
			<!-- Commento -->
			<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
				<tr>
					<td align="left" class="sfondocomm">
						Modificación de la información relativa al documento personal. <br>
						<a href="Javascript:Show_Help('/Pgm/help/Iscr_Utente/UTE_ModDocPers')">
						<img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
					</td>
				</tr>
				<tr height="2">
					<td class="sfondocomm" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif">
					</td>
				</tr>
			</table>	
			<br>
			<br>
			<table border="0" cellspacing="1" cellpadding="1" width="500">
				<tr align="middle">
					<td class="tbltext3">
						Modificación de datos no efectuada.
					</td>
				</tr>
				<tr align="middle">
					<td class="tbltext3">
						<%Response.Write(Errore)%> 
					</td>
				</tr>
			</table>
			<br>				
			<table border="0" cellpadding="0" cellspacing="1" width="500">
				<tr>
					<td align="middle" colspan="2" width="60%"><b>
						<a HREF="javascript:history.back()"><img SRC="<%=Session("Progetto")%>/images/indietro.gif" border="0">
					</td>
				</tr>
			</table>	
		
<%
	end if
else 
	CC.RollbackTrans
%>			
<br>
<!--#include file="menu.asp"-->
<br>
	<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
		<tr height="18">
			<td class="sfondomenu" width="67%" height="18"><span class="tbltext0"><b>&nbsp;DOCUMENTACION PERSONAL</b></span></td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
			<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">(*) campo obligatorio</td>
		</tr>
	</table>
	<!-- Commento -->
	<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
		<tr>
			<td align="left" class="sfondocomm">
				Modificación de los datos relacionados con la documentación personal. <br>
				<a href="Javascript:Show_Help('/Pgm/help/Iscr_Utente/UTE_ModDocPers')">
				<img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
			</td>
		</tr>
		<tr height="2">
			<td class="sfondocomm" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>	
	<br>
	<br>
	<table border="0" cellspacing="1" cellpadding="1" width="500">
		<tr align="middle">
			<td class="tbltext3">
				Modificación de datos no efectuada.
			</td>
		</tr>
		<tr align="middle">
			<td class="tbltext3">
				<%Response.Write(Errore)%> 
			</td>
		</tr>
	</table>
	<br>				
	<table border="0" cellpadding="0" cellspacing="1" width="500">
		<tr>
			<td align="middle" colspan="2" width="60%"><b>
				<a HREF="javascript:history.back()"><img SRC="<%=Session("Progetto")%>/images/indietro.gif" border="0">
			</td>
		</tr>
	</table>	

<%end if%>


</div>	

<!--#include Virtual = "/include/CloseConn.asp"-->	
<!--#include Virtual="/strutt_Coda2.asp"-->
