<% 
sNome = Request("Nome")
sCognome = Request("Cognome")
sLogin = Request("Login")
sPassword = Request("Password")

%>
<html>
<title>Visualizzazione dei dati di accesso</title>
<body bgColor="#f0f8ff" onafterprint="javascript:buttom.style.visibility='visible';" onbeforeprint="javascript:buttom.style.visibility='hidden';" onContextMenu="return false;">
<div=center>
<table border="0" width="95%" align="center" cellpadding="1" bgColor="#f0f8ff" cellspacing="2"><tr><td>
	<font size="3" color="#000080" face="Verdana"><br><i><b>Il Portale di ItaliaLavoro</b></i></font><br><br>
	<img SRC="/images/logo_italia_lavoro.gif" WIDTH="203" HEIGHT="78"><br><br>
</table>

<table border="0" CELLPADDING="1" CELLSPACING="2" width="95%" bgColor="#f0f8ff">
	<tr>	
		<td align="left">
			<font size="2" color="#000080" face="Verdana">
			&nbsp;&nbsp;&nbsp;&nbsp;Operatore:&nbsp;
			<b><%=session("Nome")%>&nbsp;<%=session("Cognome")%></b>
			</font>
		</td>
	</tr>
	<tr height="2">
		<td class="sfondocommaz" width="100%" background="<%=Session("Progetto")%>/images/separazione.gif">
		</td>
	</tr>
	<tr>
 		<td align="right">
 			<font size="2" color="#000080" face="Verdana">
 			Data della richiesta&nbsp;
 			<b><%=date()%></b>
 			</font>
 		</td>
 	</tr>
</table>

<table border="0" width="95%" align="center" cellpadding="1" bgColor="#f0f8ff" cellspacing="2"><tr><td>
	<font size="2" color="#000080" face="Verdana"><br>
		Gentile <%=sNome%>&nbsp;<%=sCognome%>,<br><br>ti ringraziamo
		per esserti registrato/a al Portale di ItaliaLavoro.<br>
		Ti inviamo la Login e la Password da utilizzare per entrare 
		nel Portale ed usufruire dei suoi  servizi.
	<br><br><br>
	<b>LOGIN: <%=sLogin%> </b><br><b>PASSWORD: <%=sPassword%></b>
	<br><br>A presto
	<br><i>La Redazione di ItaliaLavoro</i> ( <a href="http://www.ItaliaLavoro.it" target="_New">www.ItaliaLavoro.it</a> )<br><br></td></tr></font>
</table>
<table width="100%" cellspacing="2" cellpadding="1" border="0">
	<tr align="center">
	 <td>
		<label id="buttom">
			<a href="javascript:Chiudi()"><input type="image" src="<%=Session("progetto")%>/images/chiudi.gif" title="Chiudi la pagina" border="0" align="center" onclick="self.close();" id="chiudi" name="chiudi"></a><a href="javascript:print()"><input type="image" src="<%=Session("progetto")%>/images/stampa.gif" title="Stampa la pagina" border="0" align="center" onclick="self.print();" id="stampa" name="stampa"></a>
		</label>
	 </td>
	</tr>		
</table>
</div>
</body>
</html>
