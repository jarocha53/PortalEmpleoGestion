<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include Virtual = "/strutt_testa2.asp"-->
<!--#include Virtual = "/include/OpenConn.asp"-->
<!--#include virtual = "/include/ControlDateVb.asp"-->
<!--#include virtual = "/util/dbutil.asp"-->
<!--#include virtual = "/include/DecComun.asp"-->
<!--#include Virtual = "include/DecCod.asp"-->
<script LANGUAGE="Javascript">

//include del file per fare i controlli sulla validit� delle date
<!--#include Virtual = "/Include/ControlDate.inc"-->

//include del file per fare i controlli sulla numericit� dei campi
<!--#include Virtual = "/Include/ControlNum.inc"-->

//include del file per fare i controlli sulla stringa
<!--#include Virtual = "/Include/ControlString.inc"-->
<!--#include Virtual = "/Include/help.inc"-->

function Chiudi(){
	if (ControllaDati()== true)
	{
		
		if (confirm("Esta seguro de que quiere cerrar esta Discapacidad a la fecha actual?"))
		{
			condizione = "si";
			document.frmCatProt.txtChiudi.value = condizione;
			document.frmCatProt.submit()
		}		
	}
}			


//Controllo Validit� dei campi	
function ControllaDati(){

	DataNascita = frmCatProt.txtNasc.value
	DataOdierna = frmCatProt.txtOggi.value
	//DataIniCat = frmCatProt.txtDtIniCat.value
	DataFinCatOld = frmCatProt.txtFinCatOld.value
	DataIniCert = frmCatProt.txtDtIniCert.value
	DataFinCert = frmCatProt.txtDtFinCert.value
				
	//frmCatProt.txtDtIniCat.value  = TRIM(frmCatProt.txtDtIniCat.value)
	frmCatProt.txtDtIniCert.value = TRIM(frmCatProt.txtDtIniCert.value)
	frmCatProt.txtDtFinCert.value = TRIM(frmCatProt.txtDtFinCert.value)
    frmCatProt.txtEnteCert.value = TRIM(frmCatProt.txtEnteCert.value)
	
		
	//Codice Categoria Protetta
	if (document.frmCatProt.cmbCatProt.value == ""){
		alert("El campo Discapacidad es obligatorio!")
		document.frmCatProt.cmbCatProt.focus()
		return false		
	}
	
			
	//agregado//
	
	if (document.frmCatProt.ORIGENDISC.value == ""){
		alert ("El campo Origen de la discapacidad es obligatorio");
		return false;
	}
	
	if (document.frmCatProt.cmbSoporteUtilizado.value == ""){
		alert ("El campo Soporte Utilizado es obligatorio");
		return false;
	}		
	
	if (document.frmCatProt.REHABILITACION.value == ""){
		alert("El campo ha realizado el proceso de rehabilitaci�n es obligatorio");
		return false;
	}		
	
	if (document.frmCatProt.txtdiagnostico.value.indexOf("'") != -1)
	{
		alert("No esta permitido el ingreso del caracter '");
		return false;
	}		
	
	if(document.frmCatProt.txtdiagnostico.value != "")
	{if (IsNum(document.frmCatProt.txtdiagnostico.value))
	{
		alert("El campo no admite un valor num�rico");
		return false;
	}
	}	
	//fin agregado
	
	
	//Data Inizio Categoria Protetta
//	if (document.frmCatProt.txtDtIniCat.value == ""){
//		alert("El campo fecha de inicio de la discapacidad es obligatorio!")
//		document.frmCatProt.txtDtIniCat.focus() 
//		return false
	//}
    
    //Controllo della validit� della Data Inizio Categoria Protetta
//	if (!ValidateInputDate(DataIniCat)){
//		frmCatProt.txtDtIniCat.focus() 
//		return false
//	}
	
//	if (ValidateRangeDate(DataIniCat,DataOdierna)==false){
//		alert("La fecha de inicio de la discapacidad no debe ser mayor a la fecha actual!")
//		frmCatProt.txtDtIniCat.focus()
//	   return false
//	}
		
//	if (ValidateRangeDate(DataIniCat,DataNascita)==true){
//		alert("La fecha de inicio de la discapacidad debe ser mayor a la fecha de nacimiento!")
//		frmCatProt.txtDtIniCat.focus()
//	   return false
//	}		
   
    //controllo Ente certificatore
//	if (document.frmCatProt.txtEnteCert.value == ""){
//		alert("El campo Ente Certificador es obligatorio!")
//		document.frmCatProt.txtEnteCert.focus() 
//		return false
//	}
	
	//agregado
	if (document.frmCatProt.txtEnteCert.value.indexOf("'") != -1)
	{
		alert("No esta permitido el ingreso del caracter '");
		return false;
	}
	//fin agregado
	
	
	//Data Inizio Certificazione
	if (document.frmCatProt.txtDtIniCert.value == ""){
		alert("El campo fecha de inicio de la certificaci�n es obligatorio!")
		document.frmCatProt.txtDtIniCert.focus() 
		return false
	}

	//Controllo della validit� della Data Inizio Certificazione
	if (!ValidateInputDate(DataIniCert)){
		frmCatProt.txtDtIniCert.focus() 
		return false
	}

	//Data Inizio Certificazione non successiva data odierna -- 
	if (ValidateRangeDate(DataIniCert, DataOdierna)==false){
	    //alert("DataIniCert " + DataIniCert + "DataOdierna " + DataOdierna)
		alert("La fecha de inicio de la certificaci�n no debe ser mayor a la fecha actual!")
		frmCatProt.txtDtIniCert.focus()
	   return false
	}
    
	if (ValidateRangeDate(DataIniCert,DataNascita)==true){
		alert("La fecha de inicio de la certificaci�n no puede ser inferior a la fecha de nacimiento!")
		frmCatProt.txtDtIniCert.focus()
	   return false
	}

	if (document.frmCatProt.txtDtFinCert.value != ""){
		//Controllo della validit� della Data Fine Certificazione
		if (!ValidateInputDate(DataFinCert)){
			frmCatProt.txtDtFinCert.focus() 
			return false
			}
		}

//controllo tra Data Inizio e Fine Certificazione
	if (DataFinCert != "")
	{ 
		if (ValidateRangeDate(DataFinCert,DataIniCert)==true){
			alert("La fecha de fin de la certificaci�n no puede ser inferior a la fecha de inicio de la certificaci�n!")
			frmCatProt.txtDtFinCert.focus()
		   return false
		}
	}

//controllo tra Data Fine Categoria Protetta e Data inizio Certificazione
//var DataFinCatProtetta;
//DataFinCatProtetta= document.frmCatProt.txtFinCatOld.value
/*
if (DataFinCatOld != "") { 
	//alert("DataFinCatOld " +  DataFinCatOld);
	//alert("DataIniCat " + DataIniCat);
	//alert(ValidateRangeDate(DataFinCatOld,DataIniCat));
	if (!ValidateRangeDate(DataFinCatOld,DataIniCat)){
		alert("Non si pu� inserire una data di Inizio Categoria Protetta inferiore alla data di Fine Certificazione dell'occorrenza esistente!")
		frmCatProt.DataIniCat.focus()
		return false
	}	
}
*/	
//	if (IsNum(document.frmCatProt.txtEnteCert.value)){
//		alert("El valor no debe ser num�rico!")
//		document.frmCatProt.txtEnteCert.focus() 
//		return false
//		}		
	
	var StringCodEnte, StringCodEnte2;
	StringCodEnte = document.frmCatProt.cmbCatProt.value
	StringCodEnte2 = StringCodEnte.substr(0, 1)
	/*if ((StringCodEnte2 == "H") && (document.frmCatProt.txtPercInv.value == "")){
		alert("El campo porcentaje de discapacidad es obligatorio!")
		document.frmCatProt.txtPercInv.focus() 
		return false		
	}*/
	
	
	/*if ((StringCodEnte2 == "H") && (document.frmCatProt.txtPercInv.value < 33 ) || (document.frmCatProt.txtPercInv.value > 100 )){
		alert("El campo Porcentaje de Discapacidad no es valido! El valor debe estar en el rango de 33 a 100")
		document.frmCatProt.txtPercInv.focus() 
		return false		
	}*/

	/*if ((StringCodEnte2 != "H") && (document.frmCatProt.txtPercInv.value != "")){
		alert("El porcentaje de discapacidad debe ser indicado solo en caso de discapacidad!")
		document.frmCatProt.txtPercInv.focus() 
		return false		
	}*/
	
	
	 /*if (!IsNum(document.frmCatProt.txtPercInv.value)){
				alert("El valor debe ser num�rico!")
				document.frmCatProt.txtPercInv.focus() 
				return false
			}*/
			
	return true
}

function asigna()
{
  if (document.frmCatProt.vs.value != "")
  {
  if (typeof eval("document.frmCatProt.sel") != 'undefined')
  {
   document.frmCatProt.sel.value = document.frmCatProt.vs.value
  }
  }

}




function Redirecciono2(Valor)
{


	window.navigate("UTE_InsCatProt.asp?IdPers=" + frmCatProt.txtIdPers.value + "&CategProt=<%=request("CategProt")%>&DtaIni=<%=request("DtaIni")%>&txtDataFin=<%=request("txtDataFin")%>&sel="+Valor);
}

function Redirecciono(Valor)
{
	window.navigate("UTE_InsCatProt.asp?tipodisc=" + Valor + "&IdPers=" + frmCatProt.txtIdPers.value + "&CategProt=<%=request("CategProt")%>&DtaIni=<%=request("DtaIni")%>&txtDataFin=<%=request("txtDataFin")%>&sel=<%=request("Sel")%>");
}
</script>

<%

Dim idPers

Dim CatProt

Dim CatProtCtlr

Dim Dt_Ini_CatProt

Dim sSql

Dim rsCatProt

Dim sCondizione

Dim sIdPers

Dim DtFinCatProt
Dim CodEnteCert
Dim DtIniCert
Dim DtFinCert
Dim PercInv
Dim DtTmst

Dim IndStat

Dim sqlDtNasc

Dim rsDtNasc

Dim sDataFin


'sCondizione = "INS"
'Response.Write sIdPers

sIdPers = Request("IdPers")

CatProt = Request("CategProt")

CatProtCtlr = Mid(CatProt,1,1)

'Dt_Ini_CatProt = Request("DtaIni")

sDataFin = Request.Form ("txtDataFin")

'///////////////////////
stipodisc = request("tipodisc")
'///////////////////////
			
'sDataFin = cdate(ConvStringToDate(sDataFin)) - 1
'sDataFin = ConvDateToString(sDataFin)

sqlDtNasc = "SELECT DT_NASC FROM PERSONA WHERE ID_PERSONA = " & sIdPers

'Response.Write sqlDtNasc

'PL-SQL * T-SQL  
SQLDTNASC = TransformPLSQLToTSQL (SQLDTNASC) 
Set rsDtNasc = CC.Execute(sqlDtNasc)

Dt_Ini_CertCatProt = Request("DtaIni")

if Dt_Ini_CertCatProt <> "" then
	Dt_Ini_CertCatProt = Dt_Ini_CertCatProt
end if

'if Dt_Ini_CatProt <> "" then
'	Dt_Ini_CatProt = Dt_Ini_CatProt
'end if


'Se queste condizioni sono verificate allora significa che � una modifica		
if CatProt = "" and Dt_Ini_CatProt = "" then
	sCondizione = "INS"
else
	sCondizione = "MOD"
end if

set rsCatProt = server.CreateObject("ADODB.Recordset")


sSql = "SELECT ID_PERSONA, COD_CATPROT, IND_STATUS, DT_INI_CATPROT, " &_
		"DT_FIN_CATPROT, COD_ENTE_CERT, DT_INI_CERT, DT_FIN_CERT, PERC_INV, " &_
		"DT_TMST, ORIGEN_DISC, SOPORTE, REHABILITACION, DIAGNOSTICO FROM PERS_CATPROT WHERE ID_PERSONA = " & sIdPers & " "

'if sCondizione = "MOD" then
	'sSql = sSql & "AND COD_CATPROT = '" & CatProt & "' " 
	'sSql = sSql & "AND DT_INI_CATPROT = " & ConvDateToDbS(ConvStringToDate(Dt_Ini_CatProt)) & " "

if sCondizione = "MOD" then
	sSql = sSql & "AND COD_CATPROT = '" & CatProt & "' " 
	sSql = sSql & "AND DT_INI_CERT = " & ConvDateToDbS(ConvStringToDate(Dt_Ini_CertCatProt)) & " "

	'Response.Write sSql
end if

'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
rsCatProt.Open sSql,cc,3

if rsCatProt.eof then

	IndStat = ""
	DtFinCatProt = ""
	CodEnteCert = ""
	DtIniCert = ""
	DtFinCert = ""
	PercInv = ""
	DtTmst = ""
	Orig = ""
	Sopor = ""
	Rehabilit = ""
	Diagnostico = ""

else

	IndStat = CStr(rsCatProt("IND_STATUS"))
	
	DtFinCatProt = rsCatProt("DT_FIN_CATPROT")
	if DtFinCatProt <> "" then
		DtFinCatProt = ConvDateToString(DtFinCatProt)
	else
	    DtFinCatProt =""	
	end if
			
	CodEnteCert = rsCatProt("COD_ENTE_CERT")
	
	DtIniCert = rsCatProt("DT_INI_CERT")
	if DtIniCert <> "" then
		DtIniCert = ConvDateToString(DtIniCert)
	end if
	
	
	DtFinCert = rsCatProt("DT_FIN_CERT")
	if DtFinCert <> "" then
		DtFinCert = ConvDateToString(DtFinCert)
	end if

	PercInv = rsCatProt("PERC_INV")
	DtTmst = rsCatProt("DT_TMST")	
	Orig = rsCatProt("ORIGEN_DISC")
	Sopor = rsCatProt("SOPORTE")
	Rehabilit = rsCatProt("REHABILITACION")
	Diagnostico  = rsCatProt("DIAGNOSTICO")
end if

'Se CONDIZIONE non � vuoto allora si tratta di 
'una modifica: ricarico i dati dentro le INPUT TYPE;
'altrimenti � un INSERIMENTO
'---------------------------------------------------

%>
<!--<table border="0" width="525" cellspacing="0" cellpadding="0" height="70">	<tr>		<td width="530" background="<%=Session("Progetto")%>/images/titoli/cercolavoro2b.gif" height="70" align="right">			<br>			<br>			<table border="0" width="260" height="20" cellspacing="0" cellpadding="0">				<tr>					<td width="100%" valign="baseline" align="right"><span class="tbltext1a"><b>Assistenza Disabili&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></span></td>				</tr>			</table>		</td>	</tr></table>-->
<br>

<!--#include file="menu.asp"-->

<br>
<table cellpadding="0" cellspacing="1" width="500" border="0">
	<tr height="18">
		<td class="sfondomenu" height="18" width="45%">
			<span class="tbltext0"><b>&nbsp;DIFICULTADES PSICOFISICAS</b></span></td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
		<td valign="middle" align="right" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1">(*) campo obligatorio</td>
	</tr>
	<tr height="2">
		<td width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif">	</td>     
	</tr>
	<tr width="371" class="SFONDOCOMM">
		<td colspan="3">
		Para ingresar o modificar los datos completar los campos relacionados. <br>
		Presionar <b>enviar</b> para guardar la modificaci�n. 
		<%if sCondizione = "INS" then%>
			<a href="Javascript:Show_Help('/Pgm/help/Iscr_Utente/UTE_InsCatProt')" name="prov3" onmouseover="javascript:window.status='' ; return true"><img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
		<%else%>
			<a href="Javascript:Show_Help('/Pgm/help/Iscr_Utente/UTE_InsCatProt_1')" name="prov3" onmouseover="javascript:window.status='' ; return true"><img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
		<%end if%>
		</td>
	</tr>
	<tr height="2">
		<td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif">
		</td>
	</tr>
</table>
	
<br>
<br>
<%sel = request("Sel") 
if sel = "" then
	if CatProt <> "" then 
		sel = "2"
	end if
end if
vs = sel

%>

<form name="frmCatProt" method="post" action="UTE_CnfCatProt.asp" onsubmit="return ControllaDati()">
	<input type="hidden" id="text1" name="txtIdPers" value="<%=sIdPers%>">
	<input type="hidden" id="text1" name="txtOggi" value="<%=ConvDateToString(Date())%>">
	<input type="hidden" id="text1" name="txtNasc" value="<%=ConvDateToString(rsDtNasc("DT_NASC"))%>">
	<input type="hidden" id="text1" name="txtPercI" value="<%=CatProtCtlr%>">
	<input type="hidden" id="text1" name="txtChiudi" value="no">
	<input type="hidden" id="text1" name="txtFinCatOld" value="<%=sDataFin%>">
	<input type="hidden" name="vs" value="<%=vs%>">
	
	<table WIDTH="500" BORDER="0" CELLPADDING="0" CELLSPACING="0">
					<tr align="center">
							<td align="center" colspan="2" class="tbltext1">
							
							
<% if CatProt = "" then %>					
				<strong>Tiene alguna discapacidad?</strong>
				
				<select name="sel" onChange='Redirecciono2(this.value);'>
	<% if sel = "2" then %>
		<option value="2" selected>Si</option>
		<option value="1">No</option>	�
	<%elseif sel = "1" then%>
		<option value="2">Si</option>
		<option value="1" selected>No</option>
	<%else%>
		<option selected></option>
		<option value="2">Si</option>
		<option value="1">No</option>
	<%end if%>
	</select>
<% end if %>
							</td>
							</tr>
	</table>
	<br>
	<br>
	
	<%	
	
	'MODIFICA	
	if sCondizione = "MOD" then	
		
	%>		
		
		<input type="hidden" id="text2" size="50" name="txtFlag" value="MOD">		
		<input type="hidden" id="text2" size="50" name="txtTMST" value="<%=rsCatProt("DT_TMST")%>">
		
	<%	
	
	'INSERIMENTO	
	else		
		
	%>		
		
		<input type="hidden" id="text2" size="50" name="txtFlag" value="Ins">		
	
	<%	
	
	end if	
	
	%>		

	<input type="hidden" name="txtoggi" value="<%=ConvDateToString(Date())%>">
	<% if vs = "2" then %>
	<table border="0" cellpadding="0" cellspacing="1" width="500">
		
		
		
		<tr>
			<td align="left" colspan="2" class="tbltext1">
				<p align="left">
				<strong>&nbsp;</strong>
				<strong>Tipo de Discapacidad*</strong>
				<strong>&nbsp;</strong>
				</p>
			</td>
		    <td align="left" colspan="2" width="60%">
				<span class="textblack">
					
				<%
					
				dim CatProtCombo
							
				if sCondizione = "MOD" then 
									
					dim sDecodCatProt
									
					stipodisc = rsCatProt("COD_CATPROT")				
					sDecodCatProt = DecCodVal("TCAPI", "0", "", rsCatProt("COD_CATPROT"), 1)
					sDecodCatProt = UCase(sDecodCatProt)
									
					%>
					<input type="hidden" id="cmbCatProt" name="cmbCatProt" value="<%=rsCatProt("COD_CATPROT")%>">
					<b><%=sDecodCatProt%></b>					
					
					<%
							
				else
					
					CatProtCombo = "TCAPI|0|" & date & "|" & stipodisc & "|cmbCatProt' onChange='Redirecciono(this.value);'|ORDER BY DESCRIZIONE"			
					CreateCombo(CatProtCombo)
					
				end if
					
				%>	

				</span>
			</td>
		</tr>

		<tr height="5">
			<td></td>
		</tr>	
		
		
		

		
		
		<tr>
			<td align="left" colspan="2" class="tbltext1">
				<p align="left">
				<strong>&nbsp;</strong>
				<strong>Tipo*</strong>
				<strong>&nbsp;</strong>
				</p>
			</td>

		<td>
		<% dim mirs
		dim miarray
		dim cadena
		
		set mirs = server.CreateObject("adodb.recordset")
		
'PL-SQL * T-SQL  
		set mirs = CC.execute("Select ID_CAMPO_DESC from DIZ_DATI where ID_TAB = 'PERS_CATPROT' and ID_CAMPO = 'ORIGEN_DISC'")		
		
		if not mirs.EOF then 
			cadena = mirs("ID_CAMPO_DESC")
			miarray = split(cadena,"|")
		end if
		
		%>
		
		<select class=textblack name = "ORIGENDISC">
		<%	for y = lbound(miarray) to ubound(miarray) step 2
				%>
				<option value = '<%=miarray(y)%>' <%if miarray(y) = Orig then response.write "selected"%>><%=miarray(y+1)%></option>"
			<%next 
		%>
		</select>
		
		
		<%set mirs = nothing%>
		</td>
		</tr>
		
		
		
		<tr height="5">
			<td></td>
		</tr>	
		
		<tr>
			<td align="left" colspan="2" class="tbltext1">
				<p align="left">
				<strong>&nbsp;</strong>
				<strong>Soporte Utilizado*</strong>
				<strong>&nbsp;</strong>
				</p>
			</td>

		<td>
			<%
			
			'Response.Write sopor
			SoporteUtilizado = "SOPOR|0|" & date & "|" & Sopor & "|cmbSoporteUtilizado|AND VALORE = '" & stipodisc & "' ORDER BY DESCRIZIONE"			

			CreateCombo(SoporteUtilizado)

			%>
		</td>
		</tr>
		
		
		<tr height="5">
			<td></td>
		</tr>		

		<tr>
			<td align="left" colspan="2" class="tbltext1">
				<p align="left">
				<strong>&nbsp;</strong>
				<strong>�Ha realizado el proceso <br>&nbsp;&nbsp;de rehabilitaci�n?*</strong>
				<strong>&nbsp;</strong>
				</p>
			</td>

		<td>
			<p class=textblack>
			<input type=radio name="REHABILITACION" value="S" <%if Rehabilit = "S" then Response.Write "checked"%> >SI    
			<input type=radio name="REHABILITACION" value="N" <%if Rehabilit = "N" then Response.Write "checked"%> >NO
			</p>
		</td>
		</tr>
		
		
		<tr height="5">
			<td></td>
		</tr>




<!--		<tr>
			<td align="middle" colspan="2" nowrap class="tbltext1">
				<p align="left">
				<strong>&nbsp;</strong>
			
				<strong>Data Inizio Categoria Protetta *</strong> 
			    <strong>Fecha de inscripci�n*</strong> 
			 
				<strong>&nbsp;</strong>
				</p>
			</td>
			<td align="left" colspan="2" width="60%">
				<span class="textblack">
				<%
				'MODIFICA
				if sCondizione = "MOD" then
					%>
					    <input type="hidden" name="txtDtIniCat" style="TEXT-TRANSFORM: uppercase;" class="textblack" maxlength="10" value="<%=Dt_Ini_CatProt%>" size="12">
					<%
					    Response.Write "<b>" & Dt_Ini_CatProt & "</b>" 
				'INSERIMENTO
				else
					%>
					<input type="text" name="txtDtIniCat" style="TEXT-TRANSFORM: uppercase;" class="textblacka" maxlength="10" value size="12">						
					<%
				end if
				%>
				</span>
			</td>
		</tr>-->
			
		<%
		'NUOVO INSERIMENTO
		if sCondizione = "INS" then
		
			%>
			<!-- <tr><td align="middle" colspan="2" nowrap class="tbltext1"><p align="left"><strong>&nbsp;</strong>					<strong>Data Fine Categoria Protetta</strong>					<strong>&nbsp;</strong>					</p>				</td>				<td align="left" colspan="2" width="60%">					<input type="text" style="TEXT-TRANSFORM: uppercase;" class="textblacka" maxlength="10" name="txtDtFinCat" size="12">				</td>			</tr>			-->	
			<%
		'MODIFICA
		else
			if IndStat = "1" then 
				%>
			<tr height="5">
				<td></td>
			</tr>
				<tr>
					<td align="middle" colspan="2" nowrap class="tbltext1">
						<p align="left">
						<strong>&nbsp;</strong>
						<strong>Fecha de Cierre de la Discapacidad (dd/mm/aaaa)</strong>
						<strong>&nbsp;</strong>
						</p>
					</td>
					<td align="left" colspan="2" width="60%">
						<span class="textblack">
						<%if DtFinCatProt ="" then %>
						    <input type="text" name="txtDtFinCat" style="TEXT-TRANSFORM: uppercase;" class="textblack" maxlength="10" value="<%=DtFinCatProt%>" size="12" readonly>
						<%else
						     Response.Write "<b>" & DtFinCatProt & "</b>"						
						  end if
						%>
						</span>
					</td>
				</tr>	
				
				<%
			end if
			
		end if					
		%>		
		<tr height="5">
			<td></td>
		</tr>		
		<tr>
			<td align="left" colspan="2" class="tbltext1">
				<p align="left">
				<strong>&nbsp;</strong>
				<strong>Ente Certificador</strong>
				<strong>&nbsp;</strong>
				</p>
			</td>
		    <td align="left" colspan="2" width="60%">
				 <span class="textblack"> 
				<%
					
				if sCondizione = "MOD" then 
									
					if DtFinCatProt ="" then
					%>
					     <input type="text" name="txtEnteCert" class="textblack" value="<%=Ucase(rsCatProt("COD_ENTE_CERT"))%>" style="TEXT-TRANSFORM: uppercase;"> 
					<%else
					      Response.Write "<span class=textblack><b>" & Ucase(rsCatProt("COD_ENTE_CERT")) & "</b></span>"
					end if
				  
				else
				%>
					<input type="text" name="txtEnteCert" style="TEXT-TRANSFORM: uppercase;" span class="textblack" size="50">
					
	            <%
				end if
					
				%>				
				
				 </span> 
			</td>
		</tr>	
		<tr height="5">
			<td></td>
		</tr>			
		<tr>
			<td align="middle" colspan="2" nowrap class="tbltext1">
				<p align="left">
				<strong>&nbsp;</strong>
				<strong>Fecha de Emisi�n del Certificado <br> (dd/mm/aaaa)*</strong>
				<strong>&nbsp;</strong>
				</p>
			</td>
			<td align="left" colspan="2" width="60%">
				<span class="textblack">
				<%
				'MODIFICA
				if sCondizione = "MOD" then
					if IndStat = "1" then 
					%>
						<input type="hidden" name="txtDtIniCert" style="TEXT-TRANSFORM: uppercase;" class="textblack" maxlength="10" value="<%=DtIniCert%>" size="12" readonly>
						<b><%=DtIniCert%></b>
						<%
					else
						%>
						<input type="text" name="txtDtIniCert" style="TEXT-TRANSFORM: uppercase;" class="textblack" maxlength="10" value="<%=DtIniCert%>" size="12">
						<%					
					end if
					
				'INSERIMENTO
				else
				%>
					<input type="text" name="txtDtIniCert" style="TEXT-TRANSFORM: uppercase;" class="textblacka" maxlength="10" size="12">						
					<input type="hidden" name="hidDataFineCategoriaProtetta" value="<%=sDataFin%>">
				<%
				end if
				%>
				</span>				
			</td>
		</tr>
		<tr height="3">
			<td></td>
		</tr>		
		<tr>
			<td align="middle" colspan="2" nowrap class="tbltext1">
				<p align="left">
				<strong>&nbsp;</strong>
				<strong>Fecha de Vencimiento del Certificado <br> (dd/mm/aaaa)</strong>
				<strong>&nbsp;</strong>
				</p>
			</td>
			<td align="left" colspan="2" width="60%">
				<span class="textblack">
				<%
				'MODIFICA
				if sCondizione = "MOD" then
				   if IndStat = "1" then 
						%>
						<input type="hidden" name="txtDtFinCert" style="TEXT-TRANSFORM: uppercase;" class="textblack" maxlength="10" value="<%=DtFinCert%>" size="12" readonly>
						<b><%=DtFinCert%></b>
						<%
					else
						%>
						<input type="text" name="txtDtFinCert" style="TEXT-TRANSFORM: uppercase;" class="textblack" maxlength="10" value="<%=DtFinCert%>" size="12">
						<%					
					end if				

				'INSERIMENTO
				else
				   	%>
					<input type="text" name="txtDtFinCert" style="TEXT-TRANSFORM: uppercase;" class="textblacka" maxlength="10" size="12">						
					<%
				end if
				%>
				</span>				
			</td>
		</tr>	
		
		<%
		
		'NUOVO INSERIMENTO
		
        
		if sCondizione = "INS" then
		
		%>
			 <tr>
				<td colspan="4">
				</td>
			 </tr>
			<!-- <tr>
				<td height="2" align="left" colspan="4" background="<%=Session("Progetto")%>/images/separazione.gif">
				</td>
			</tr>
			<tr>
				<td colspan="4">
					<span class="textblack">
					<b>&nbsp;&nbsp;N.B.:</b> El campo <b>Porcentaje de Discapacidad</b> debe ser completado solo en el caso de 
					<br>&nbsp;&nbsp;de que se haya registrado una discapacidad!
					</span>
				</td>
			</tr>	
			<tr>
				<td colspan="4">
				</td>
			</tr>	-->			
			<!--<tr>
				<td align="middle" colspan="2" nowrap class="tbltext1">
					<p align="left">
					<strong>&nbsp;</strong>
					<strong>Porcentaje de Discapacidad</strong>
					<strong>&nbsp;</strong>
					</p>
				</td>
				<td align="left" colspan="2" width="60%">
					<input type="text" name="txtPercInv" style="TEXT-TRANSFORM: uppercase;" class="textblacka" maxlength="3" size="6">
				</td>			
			</tr>-->	
			<tr>
				<td align="middle" colspan="2" nowrap class="tbltext1">
					<p align="left">
					<strong>&nbsp;</strong>
					<strong>Diagn�stico</strong>
					<strong>&nbsp;</strong>
					</p>
				</td>
				<td align="left" colspan="2" width="60%">
					<input type="text" name="txtdiagnostico" style="TEXT-TRANSFORM: uppercase;" class="textblacka" maxlength="400" size="50">
				</td>			
			</tr>	


			<%
		'MODIFICA
		else
			if CatProtCtlr = "H" then 
			
				%>
				<tr height="4">
					<td></td>
				</tr>	
				 <!-- <tr><td height="2" align="left" colspan="4" background="<%=Session("Progetto")%>/images/separazione.gif">					</td>				</tr>				<tr>					<td colspan="4">						<span class="textblack">						<b>&nbsp;&nbsp;N.B.:</b> Il campo <b><STRONG>Percentuale Invalidit�</STRONG> </b> deve essere compilato solo in caso la<br>&nbsp;&nbsp;percentuale riguardi l'handicap indicato!						</span>					</td>				</tr>				<tr>					<td colspan="4">						&nbsp;					</td></tr>-->								
				<!--<tr>
					<td align="middle" colspan="2" nowrap class="tbltext1">
						<p align="left">
						<strong>&nbsp;</strong>
						<strong>Porcentaje de discapacidad</strong>
						<strong>&nbsp;</strong>
						</p>
					</td>
					<td align="left" colspan="2" width="60%">
					<% if DtFinCatProt ="" then %>
					    	<input type="text" name="txtPercInv" style="TEXT-TRANSFORM: uppercase;" class="textblacka" maxlength="3" value="<%=rsCatProt("PERC_INV")%>" size="3">
					<% else
					      Response.Write "<span class=textblack><b>" & rsCatProt("PERC_INV") & "</b></span>"
					   end if %>
					</td>
				</tr>-->




				<%
			else
				
				%>
				<!--	<tr height="3">	<td></td></tr><tr><td align="middle" colspan="2" nowrap class="tbltext1"><p align="left"><strong>&nbsp;</strong><strong>Porcentaje de discapacidad</strong><strong>&nbsp;</strong>						</p>					</td>					<td align="left" colspan="2" width="60%"><span class="textblacka">-->
						<!--<input type="HIDDEN" name="txtPercInv" style="TEXT-TRANSFORM: uppercase;" class="textblacka" maxlength="5" value="<%=rsCatProt("PERC_INV")%>">-->
						<!--<input type="hidden" name="txtdiagnostico" style="TEXT-TRANSFORM: uppercase;" class="textblacka" maxlength="400" value="<%=Diagnostico%>" size="50">-->

				<!--	<b><%=rsCatProt("PERC_INV")%></b>	</span>	</td></tr>-->				
				<%
			end if
			%>
			<tr>
					<td align="middle" colspan="2" nowrap class="tbltext1">
						<p align="left">
						<strong>&nbsp;</strong>
						<strong>Diagn�stico</strong>
						<strong>&nbsp;</strong>
						</p>
					</td>
					<td align="left" colspan="2" width="60%">
					<% if DtFinCatProt ="" then %>
					    	<input type="text" name="txtdiagnostico" style="TEXT-TRANSFORM: uppercase;" class="textblacka" maxlength="400" value="<%=Diagnostico%>" size="50">
					<% else
					      Response.Write "<span class=textblack><b>" & Diagnostico & "</b></span>"
					   end if %>
					</td>
				</tr>		
		<%end if					
		%>
		
	</table>
	<% end if %>
	<br>
	<br>
	<table border="0" cellpadding="0" cellspacing="1" width="500" align="center">
		<tr>
		   <td align="middle" colspan="2">    
				<%
				'NUOVO INSERIMENTO
				if (IndStat = "1" and DtFinCatProt <> "" and sCondizione = "INS") or (IndStat = "" and DtFinCatProt = "" and sCondizione = "INS") then
				
				%>
					<a href="javascript:document.location.href='UTE_VisCatProt.asp?IdPers=<%=sIdPers%>'"><img src="<%=Session("progetto")%>/images/Indietro.gif" border="0" onLoad='asigna();'></a>
					<%if sel="2" then %><input type="image" name="Invia" src="<%=Session("Progetto")%>/images/conferma.gif" border="0" value="Conferma" onLoad='asigna();'><% end if %> 
					<%
					else
					if (IndStat = "1" and sCondizione = "MOD") then %>
					<table WIDTH="500" BORDER="0" CELLPADDING="0" CELLSPACING="0">
							<tr align="center">
							<td>
							<a href="javascript:document.location.href='UTE_VisCatProt.asp?IdPers=<%=sIdPers%>'"><img src="<%=Session("progetto")%>/images/Indietro.gif"  border="0" onLoad='asigna();'></a>
							</td>
							</tr>
					</table>	
				<%	
				
					end if
				end if
				'MODIFICA
				if (IndStat = "0" and sCondizione = "MOD") then
				%>
			    
			         <table WIDTH="500" BORDER="0" CELLPADDING="0" CELLSPACING="0">
				         <tr align="center">
					        <td>
						        <a href="javascript:document.location.href='UTE_VisCatProt.asp?IdPers=<%=sIdPers%>'"><img src="<%=Session("progetto")%>/images/Indietro.gif" border="0" onLoad='asigna();'></a>
						        <% if sel="2" then %><input type="image" name="Invia" src="<%=Session("Progetto")%>/images/conferma.gif" border="0" value="Conferma" onLoad='asigna();'><% end if %>				
						        <a href="javascript:Chiudi()" id="cmdCancella"><img src="<%=session("Progetto")%>/images/chiudivalidita.gif" value="chiudi" border="0"></a>
					        </td>	
				         </tr>
			         </table>
	            <%
				end if
				%>
			 </td>
	    </tr>
	</table>
						
</form>
<%

rsCatProt.close

Set rsCatProt = Nothing
%>

<!--#include Virtual ="/Include/closeconn.asp"-->
<!--#include Virtual="/strutt_coda2.asp"-->
