<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include virtual="/strutt_testa2.asp" -->
<!--#include Virtual = "/include/OpenConn.asp"-->
<!--#include Virtual = "/include/ControlDateVB.asp"-->
<!--#include Virtual = "/util/portallib.asp"-->

<%
Dim sCuil, sOficina, sUsuario, sEntrevistaTarea , sNroEntrevista
'==================================================================================================
' Solicito valor de las variables del form anterior
'==================================================================================================
sIdpersona = request("Idpers")
sEntrevistaTarea = request("EntrevistaTarea")


if len(sEntrevistaTarea) = 0 then
	sEntrevistaTarea = "2"
end if

sOficina = session("Creator")
sUsuario = session("IdUtente")
sNroEntrevista = session("NroEntrevista")

'Busco el Cuil de la Persona
set rs = Server.CreateObject("ADODB.Recordset")
sSql = "SELECT COD_FISC FROM PERSONA WHERE Id_Persona =" & sIdpersona

'Response.Write sSql
'Response.End
'Ejecuto la consulta
'PL-SQL * T-SQL  
'SSQL = TransformPLSQLToTSQL (SSQL) 
rs.open sSql,CC,3
'pongo el valor del COD_FISC en la variable cuil
sCuil  = (rs("COD_FISC"))

'P�ra pruebas Hernan
'---------------------------------------------------------
'response.write "sIdpersona	 : " & sIdpersona    & "<br>"
'response.write "sOficina	 : " & sOficina    & "<br>"
'response.write "sUsuario    : " & sUsuario    & "<br>"   
'response.write "sCuil       : " & sCuil       & "<br>"  
'response.write "sEntrevistaTarea       : " & sEntrevistaTarea & "<br>"
'response.write "NroEntrevista       : " & sNroEntrevista & "<br>"    
'Response.End
'---------------------------------------------------------

'---------------------------------------------------------
'Proceso que aplica la tarea de la entrevista
'---------------------------------------------------------
	Set cmdEntrevista = Server.CreateObject("ADODB.Command")
	With cmdEntrevista
	    .ActiveConnection = ConnLavoro
		.CommandTimeout = 1200
		.CommandText = "AgEntrevistaTareaAplica"
		.CommandType = 4
		'Parametros de Input Obligatorios
		.Parameters("@Oficina").Value = sOficina
		.Parameters("@Tarea").Value = sEntrevistaTarea
		.Parameters("@CUIL").Value = sCuil
			
		if len(sNroEntrevista) > 0 then
			.Parameters("@NroEntrevista").Value = sNroEntrevista
		End if
		'Parametros de Input Opcionales
		.Parameters("@Usuario").Value = sUsuario
		
		'--------------------------------------------
		' Carga parametros
		'--------------------------------------------
		'For Each Parameter In .Parameters
		'	response.write Parameter.Name & "=" & Parameter.value
			'If Parameter.Direction And adParamInput Then
			'	Select case ucase(Mid(Parameter.Name, 2))
			'		CASE "NROCONVENIO"
			'			Parameter.value =  Nroconvenio
			'		CASE ELSE
			'			' ????
			'	END SELECT
			'End If
		'Next	
		
'PL-SQL * T-SQL  
		.Execute()
		
		'Parametros de Output
		NroEntrevista = .Parameters("@NroEntrevista")
		If len(Session("NroEntrevista")) = 0 then
				Session("NroEntrevista") = .Parameters("@NroEntrevista") 				
		End If 
		Secuencia = .Parameters("@Secuencia")			
		Status = .Parameters("@Status").value
		Mensaje = .Parameters("@Mensaje").value
		CitaNro = .Parameters("@CitaNro").value
		
		'P�ra pruebas Hernan Muestro las variables de Ouput
		'--------------------------------------------------------------
		'response.write "NroEntrevista: " & NroEntrevista  & "<br>"  
		'response.write "Secuencia: " & Secuencia  & "<br>"
		'response.write "Status: " & Status  & "<br>"
		'response.write "Mensaje: " & Mensaje  & "<br>"
		'Response.End
		'--------------------------------------------------------------
	End With

	'Proceso Ok
	If Status = 1 then
	
		Session("Msg_Error")="Cierre de Entevista aplicado correctamente"
	%>	
		
	<br>

	<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
		<tr height="18">
			<td class="sfondomenu" width="67%" height="18"><span class="tbltext0"><b>&nbsp;Control de Sistema - Entrevistas</b></span></td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
			<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">(*) </td>
		</tr>
		<tr>
			<td class="sfondocomm" width="57%" colspan="3">
			<br><p>
			Cierre de Entevista aplicado correctamente. Seleccione una opcion para continuar<br>             
					
			<a href="Javascript:Show_Help('/Pgm/help/Utenti/Modificautente/pag2_modute/')" name onmouseover="javascript:status='' ; return true">
			<img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></p>
			</td>
		</tr>
		<tr height="2">
			<td class="sfondocomm" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
		</tr>
	</table>
	<br>
	<table border="0" cellpadding="2" cellspacing="2" width="500">
		<tr height="30">
		   <td height="25" class="tbltext1" align="left">&nbsp;<b>Status:</b></td>
		   <td height="25" class="tbltext1" align="left">&nbsp;<b><%=Status%></b></td>
		</tr>
		<tr height="30">
		   <td height="25" class="tbltext1" align="left">&nbsp;<b>Mensaje:</b></td>
		   <td height="25" class="tbltext1" align="left">&nbsp;<b><%=Mensaje%></b></td>
		</tr>
	</table>
		<%
		
		'DirURL = "/BA/Home.asp?"
		'Response.Redirect DirURL
	'Redirecciono a pagina de Error
	Else
	'-------------------------------------------------------------------------------------
	' Mensaje al usuario ???
	'-------------------------------------------------------------------------------------
	%>
	<br>
	<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
		<tr height="18">
			<td class="sfondomenu" width="67%" height="18"><span class="tbltext0"><b>&nbsp;Control de Errores del Sistema - Entrevistas</b></span></td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
			<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">(*) </td>
		</tr>
		<tr>
			<td class="sfondocomm" width="57%" colspan="3">
			<br><p>
			Error al Aplicar el cierre de Entrevista.<br>             
					
			<a href="Javascript:Show_Help('/Pgm/help/Utenti/Modificautente/pag2_modute/')" name onmouseover="javascript:status='' ; return true">
			<img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></p>
			</td>
		</tr>
		<tr height="2">
			<td class="sfondocomm" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
		</tr>
	</table>
	<br>
	<table border="0" cellpadding="2" cellspacing="2" width="500">
		<tr height="30">
		   <td height="25" class="tbltext1" align="left">&nbsp;<b>Status:</b></td>
		   <td height="25" class="tbltext1" align="left">&nbsp;<b><%=Status%></b></td>
		</tr>
		<tr height="30">
		   <td height="25" class="tbltext1" align="left">&nbsp;<b>Mensaje:</b></td>
		   <td height="25" class="tbltext1" align="left">&nbsp;<b><%=Mensaje%></b></td>
		</tr>
	</table>

		<%
	End If	

%>

<!-- #include Virtual="/strutt_coda2.asp" -->
