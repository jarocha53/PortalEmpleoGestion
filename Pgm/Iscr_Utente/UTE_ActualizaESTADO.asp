<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->

<!--#include Virtual = "/include/OpenConn.asp"-->

<%

'=================================================
' Actualiza el estado del convenio
'=================================================

public function ConvenioActualiza(Tarea, IdPersona, CUIL, OficinaID, UsuarioID, NroConvenio, Mensaje)
'------------------------------------------------
' Genera o actualiza un convenio para una persona
'
'Necesita:
'	Tarea		Tarea que quiere hacer el usuario
'				1: 	Quiere Imprimir 
'				2:	Firma del convenio
'				3:	Desiste de firmar el convenio
'	IdPersona	ID de la persona en la Tabla Persona
'	CUIL		o Nro de CUIL que esta Persona.Cod_Fis
'	OficinaID	Oficina de trabajo
'	UsuarioID	Codigo de usuario
'Devuelve:
'	NroConvenio	Nro de convenio Generado o actualizado
'	Mensaje		Mensaje de Error	
'	Funcion     0: Esta todo OK 
'				>0 Error
'------------------------------------------------
	dim ExistePersona, sSQLO, Direccion
	
	ConvenioActualiza = 1	' Pone error por si se va sin control 
	NroConvenio = 0
	Mensaje = ""

	'------------------------------------------------
	' 1) Busca la persona en la tabla de Persona 
	'------------------------------------------------
	sSQLO = "SELECT ID_PERSONA, COGNOME, NOME, DT_NASC, COM_NASC, PRV_NASC,"
	sSQLO = sSQLO & " STAT_NASC, COD_FISC,"
	sSQLO = sSQLO & " STAT_CIT, STAT_CIT2, IND_RES, FRAZIONE_RES, COM_RES,"
	sSQLO = sSQLO & " PRV_RES, CAP_RES, NUM_TEL, E_MAIL, DT_TMST, DEP_NASC,"
	sSQLO = sSQLO & " LOC_NASC, DEP_RES, LOC_RES"
	sSQLO = sSQLO & " from PERSONA "
	' Busco por ID o CUIL ?
	if len(trim(IdPersona)) >0 then
		sSQLO = sSQLO & " WHERE ID_PERSONA=" & IdPersona
	else
		sSQLO = sSQLO & " WHERE Cod_Fis=" & CUIL
	end if
	
	'--------------------------------------
	' Busca a la persona
	'--------------------------------------
	
	ExistePersona = 0
	
	' abre recordset	
	set rsPers = Server.CreateObject("ADODB.Recordset")
'PL-SQL * T-SQL  
SSQLO = TransformPLSQLToTSQL (SSQLO) 
	rsPers.Open sSQLO, CC
	if rsPers.BOF = false and rsPers.EOF = False then
		' Encontre a la persona
		ExistePersona = 1
	else
		' NO existe la persona --> Se va
		Mensaje = "No Existe la Persona solicitada en Personas: NroInterno:" & IdPersona & " C.I. :" & CUIL
		ConvenioActualiza = 10
		exit function
	end if

	'====================================================
	' 2) Genero o actualizo el Convenio  
	'====================================================
	
	' Direccion para notificaciones
	Direccion = trim(rsPers!Ind_Res) & 
	if len(trim(rsPers!Frazione_Res)) > 0 then
		Direccion = Direccion & " - " & Trim(rsPers!Frazione_Res)
	end if
	if len(trim(rsPers!CAp_Res)) > 0 then
		Direccion = Direccion & " - " & Trim(rsPers!CAp_Res)
	end if
	
	sSQLO = "SeguroConveniosActualiza " & Tarea & ", '" & trim(rsPers!Cod_Fisc) & "', " _
		& ", '" & trim(rsPers!@TipoDoc) & "', " & trim(rsPers!@NroDoc) & ", " _
		& ", '" & trim(rsPers!Cognome) & "', " & trim(rsPers!Nome) & ", " _
		& ", '" & Direccion & "', " _
		& ", " & OficinaID & ", " & UsuarioID & ", 0, 0, '' " 

	' abre recordset	
	set Cmd = Server.CreateObject("ADODB.Command")
'PL-SQL * T-SQL  
SSQLO = TransformPLSQLToTSQL (SSQLO) 
	Cmd.Open sSQLO, CC
	if rsPers.BOF = false and rsPers.EOF = False then
		' Encontre a la persona
		ExistePersona = 1
	end if
	
end function


%>

<%

Public Function InicioBD()

	Hosting = "PORTALDESARROLLO"
    Application(Hosting & "_AgendaConnectionString") = "Provider=MSDASQL.1;Password=1234*;Persist Security Info=True;User ID=EmpleoLavoro;Data Source=EmpleoLavoro;Extended Properties=""DSN=EmpleoLavoro;Description=EmpleoLavoro;UID=EmpleoLavoro;APP=????4?????4?????T0???????;WSID=PC0297;DATABASE=EmpleoLavoro"";Initial Catalog=EmpleoLavoro;User Id=EmpleoLavoro;PASSWORD=1234*;"
	Application(Hosting & "_AgendaConnectionTimeout") = 60
	Application(Hosting & "_AgendaCommandTimeout") = 180
	Application(Hosting & "_AgendaCursorLocation") = 3
	Application(Hosting & "_AgendaRuntimeUserName") = "EmpleoLavoro"
	Application(Hosting & "_AgendaRuntimePassword") = "1234*"
	
	Hosting = "PORTALPRUEBA"
    Application(Hosting & "_AgendaConnectionString") = "Provider=MSDASQL.1;Password=1234*;Persist Security Info=True;User ID=EmpleoLavoro;Data Source=EmpleoLavoro;Extended Properties=""DSN=EmpleoLavoro;Description=EmpleoLavoro;UID=EmpleoLavoro;APP=????4?????4?????T0???????;WSID=PC0297;DATABASE=EmpleoLavoro"";Initial Catalog=EmpleoLavoro;User Id=EmpleoLavoro;PASSWORD=1234*;"
	Application(Hosting & "_AgendaConnectionTimeout") = 60
	Application(Hosting & "_AgendaCommandTimeout") = 180
	Application(Hosting & "_AgendaCursorLocation") = 3
	Application(Hosting & "_AgendaRuntimeUserName") = "EmpleoLavoro"
	Application(Hosting & "_AgendaRuntimePassword") = "1234*"

	Hosting = "PORTALEMPLEO"
    Application(Hosting & "_AgendaConnectionString") = "Provider=MSDASQL.1;Password=1234*;Persist Security Info=True;User ID=EmpleoLavoro;Data Source=EmpleoLavoro;Extended Properties=""DSN=EmpleoLavoro;Description=EmpleoLavoro;UID=EmpleoLavoro;APP=????4?????4?????T0???????;WSID=PC0297;DATABASE=EmpleoLavoro"";Initial Catalog=EmpleoLavoro;User Id=EmpleoLavoro;PASSWORD=1234*;"
	Application(Hosting & "_AgendaConnectionTimeout") = 60
	Application(Hosting & "_AgendaCommandTimeout") = 180
	Application(Hosting & "_AgendaCursorLocation") = 3
	Application(Hosting & "_AgendaRuntimeUserName") = "EmpleoLavoro"
	Application(Hosting & "_AgendaRuntimePassword") = "1234*"
	
	' Determina ServerName
	If Len(ServerName) = 0 Then
		ServerName = Request.ServerVariables("SERVER_NAME")
		Application("SERVER_NAME") = ServerName
	End If
	
	
	Set conn = Server.CreateObject("adodb.connection")
	
	with conn
	    .ConnectionString = Application(ServerName & "_AgendaConnectionString")
	    .ConnectionTimeout = Application(ServerName & "_AgendaConnectionTimeout")
	    .CommandTimeout = Application(ServerName & "_AgendaCommandTimeout")
	    .CursorLocation = Application(ServerName & "_AgendaCursorLocation")
'PL-SQL * T-SQL  
	    .Open ,Application(ServerName & "_AgendaRuntimeUserName"), Application(ServerName & "_AgendaRuntimePassword")
	end with

	set InicioBD = conn	
End Function

Public Function FinBD()
	Set conn = nothing
End Function


if accion<>"SAVE" THEN
	Response.Redirect "/pgm/Iscr_Utente/ute_visDichiaraz.asp?Grabado=S&f=" & sfilenamedic & "&Fasc=" & sfasc & "&IdPers=" & nIdPersona & "&FlagAna=" & sAnagrafica & "&FlagStu=" & sStudio & "&FlagOcc=" & sOccupazione
end if


'''Response.Redirect "/pgm/fascicolo/FAS_SelCompleto.asp?gRABADO='S'"


'Response.Redirect "/pgm/fascicolo/FAS_SelCompleto.asp?gRABADO='S'"
'server.transfer "/pgm/fascicolo/FAS_SelCompleto.asp"
%>
