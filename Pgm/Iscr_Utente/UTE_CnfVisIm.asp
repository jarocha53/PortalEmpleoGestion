<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--META HTTP-EQUIV="Pragma" CONTENT="no-cache"><META HTTP-EQUIV="Cache-Control" CONTENT="no-cache"-->
<%
 Response.ExpiresAbsolute = Now() - 1 
 Response.AddHeader "pragma","no-cache"
 Response.AddHeader "cache-control","private"
 Response.CacheControl = "no-cache"
%>

<!--#include Virtual = "/include/controlDateVB.asp"-->
<!--#include Virtual = "/util/portallib.asp"-->
<!--#include Virtual="/strutt_testa2.asp"-->
<!--#include Virtual = "/include/OpenConn.asp"-->
<!--#include Virtual = "/include/ControlDateVB.asp"-->
<!--#include Virtual= "/include/SysFunction.asp"-->
<!--#include Virtual = "/util/dbutil.asp"-->
<!--#include virtual = "/include/DecComun.asp"-->


</head>
<body name="myBody" onload>

<%


dim sSqlm
dim sCodOccorrenza
dim sInserModMIGR		
dim sDTingresso
dim sComuneIngresso
dim sProvIngresso
dim sMotivo
dim sStatoUE
dim sMesi
dim sIntezioneReg
dim sIntezioneProv
dim sTrasfReg
dim sComuneInteresse
dim sProvInteresse
dim sIntenzioneCameBack
dim sIntenzioneAltroStato
dim sTipoDoc
dim sRilasciato
dim sDataRil
dim sDataVal
dim sRichiesta
dim sNonRichiesta
dim sPossesso
dim sIdPers
dim sNumeroDoc
dim sTms_Migrazione
dim sTms_Doc
dim FL_Err
dim sValInf
dim sStatoDoc
dim Chiave



sIdPers=clng(Request.Form("txtIdPers"))

sCodOccorrenza=Request.Form("txtTipoOcco")
sTipoDoc= cstr(Request.Form ("cmbTipoDoc"))
 



'contiene il valore che mi dice se si tratta di u  inserimento 
'oppure di una modifica nella tabella PERS_MIGRAZIONE
sInserModMIGR= Request.Form("txtInserModMIGR")


sNumeroDoc = UCase(Request.Form ("txtNumeroDoc"))
sNumeroDoc = Replace(server.HTMLEncode(sNumeroDoc), "'", "''")


sDTingresso = ConvStringToDate(UCase(Request.Form ("txtDTingresso")))


sComuneIngresso = Request.Form ("txtComuneIngrHid")


sProvIngresso = UCase(Request.Form ("cmbProvinciaIngresso"))
sMotivo=UCase(Request.Form("cmbMotivo"))
sStatoUE = UCase(Request.Form ("cmbStato_UE"))

sMesi = UCase(Request.Form ("txtMesi"))
sMesi = Replace(server.HTMLEncode(sMesi), "'", "''")

sComuneInteresse = Request.Form ("txtComuneIntHid")


sProvInteresse = UCase(Request.Form ("cmbPrvInter"))



sRilasciato= UCase(Request.Form ("cmbEnteRilascio"))

sDataRilascio=Request.Form ("txtDataRil")
if sDataRilascio="" then
	sDataRilascio=null
else	
	sDataRilascio= ConvStringToDate(UCase(Request.Form ("txtDataRil")))
	'sDataRilascio = Replace(server.HTMLEncode(sDataRilascio), "'", "''")      
end if

	sDataVal= ConvStringToDate(UCase(Request.Form ("txtDataScad")))
	

sIntezioneProv= Request.Form("Fl1")
sIntezioneReg= Request.Form("Fl2")
sTrasfReg= Request.Form("Fl3")
sIntenzioneCameBack= Request.Form("Fl4")
sIntenzioneAltroStato= Request.Form("Fl5")
'sStatoDoc= Request.Form("Fl8")

Chiave= (  sIdPers & "|" & sDTingresso )    

sTms_Migrazione=Request.form("txtTms_Migrazione")
sTms_Migr=Request.form("txtTms_Migr")
FL_Err="0"

if sStatoUE="" then	
	sStatoUE=""
    sMesi="null"
end if


dim messaggio


 

if sInserModMIGR="1" then
	
	
		messaggio="Inserimento dei dati "
			
			CC.BeginTrans
			%>
			<!--#include virtual = "/include/ValInfPers.asp"-->
			<%
			
			
			sSQL = "INSERT INTO PERS_MIGRAZIONE " & _
				"(ID_PERSONA,DT_INGRESSO, COM_INGRESSO, PRV_INGRESSO,COD_MINGR, COD_STATO_UE, NUM_MESI_UE, " &_
				"FL_ALTROCOMUNE, FL_ALTRAPROV, FL_ALTRAREG, COM_MIGRAZIONE, PRV_MIGRAZIONE, " &_
				"FL_RITORNO, FL_PAESE_UE, DT_TMST) VALUES " &_	
				"(" & sIdPers & "," & Convdatetodbs(sDTingresso) & ", '" & sComuneIngresso & "', " &_
				" '" & sProvIngresso & "','" & sMotivo & "','" & sStatoUE & "'," & sMesi & ", " &_
				" '" & sIntezioneProv & "', '" & sIntezioneReg & "', '" & sTrasfReg & "', " &_
				" '" & sComuneInteresse & "', '" & sProvInteresse & "', " &_
				" '" & sIntenzioneCameBack & "', '" & sIntenzioneAltroStato & "', " &_
				" " &  Convdatetodb(now) & ") " 
				
			
				Errore=EseguiNoCTrace(UCASE(mid(session("progetto"),2)),Chiave,"PERS_MIGRAZIONE",session("idutente"),sIdPers,"P", "INS",sSQL, 1, now(),cc)
				
				
				
else

	messaggio="Modifica dei dati "
	
	CC.BeginTrans
			%>
			<!--#include virtual = "/include/ValInfPers.asp"-->
			<%
			sSQL = "UPDATE PERS_MIGRAZIONE SET DT_INGRESSO=" & Convdatetodbs(sDTingresso) & ",COM_INGRESSO='" & sComuneIngresso & _
			"',PRV_INGRESSO='" & sProvIngresso & "',COD_MINGR='" & sMotivo & "',COD_STATO_UE='" & sStatoUE & "', NUM_MESI_UE=" & sMesi & _
			",FL_ALTROCOMUNE='" & sIntezioneProv & "', FL_ALTRAPROV='" & sIntezioneReg & "', FL_ALTRAREG='" & sTrasfReg & _
			"',COM_MIGRAZIONE='" & sComuneInteresse & "',PRV_MIGRAZIONE='" & sProvInteresse & "',FL_RITORNO='" & sIntenzioneCameBack & "', FL_PAESE_UE='" & sIntenzioneAltroStato & _
			"',DT_TMST=" &  Convdatetodb(now) & "WHERE ID_PERSONA=" & sIdPers & "" 
					
			Errore=EseguiNoCTrace(UCASE(mid(session("progetto"),2)),Chiave,"PERS_MIGRAZIONE",session("idutente"),sIdPers,"P", "MOD",sSQL, 1, sTms_Migrazione,cc)
			               
			               

end if				
			
		
			if Errore = "0"  then
					
					'se sCodOccorrenza="PS", vuol dire che c'� soltanto una occorrenza ed � il PERMESSO DI SOGGIORNO
					if sCodOccorrenza="PS" then
					
						sSQLm = "UPDATE PERS_DOC SET COD_DOCST='" & sTipoDoc & _
										
										"',DT_RIL_DOC=" & Convdatetodbs(sDataRilascio) & _
										",ID_DOC='" & sNumeroDoc & _
										"',DT_FIN_VAL=" & Convdatetodbs(sDataVal) & _
										",PRV_RIL='" & sRilasciato & _ 
										"',COM_RIL='',COD_STATO_RIL='',DT_TMST=" & Convdatetodb(now) & "WHERE ID_PERSONA=" & sIdPers & " and cod_docst='" & sTipoDoc & "'" 
										
										
								Chiave= (  sIdPers & "|" & sNumeroDoc )
										
										Errore=EseguiNoCTrace(UCASE(mid(session("progetto"),2)),Chiave,"PERS_DOC",session("idutente"),sIdPers,"P", "MOD",sSQLm, 1, sTms_Doc,cc)

					
					else
					
							'se l'occorrenza � diversa dal permesso di soggiorno(04) oppure � nulla
							if sCodOccorrenza<>"" or sCodOccorrenza="" then

					
								'inserisco la nuova dichiarazione con il PERMESSO DI SOGGIORNO
								sSQLm = "INSERT INTO PERS_DOC (ID_PERSONA," &_
									"COD_DOCST," &_
									
									"DT_RIL_DOC," &_
									"ID_DOC," &_
									"DT_FIN_VAL," &_
									"PRV_RIL," &_
									
									"DT_TMST)values (" & sIdPers &_
									 ",'" & sTipoDoc &_
									 "'," & Convdatetodbs(sDataRilascio) &_
									 ",'" & sNumeroDoc &_
									 "'," & Convdatetodbs(sDataVal) &_
									 ",'" & sRilasciato &_
									 
									 "'," & Convdatetodb(now) & ")"    
									 
									 
									    
								
								Chiave= (  sIdPers & "|" & sNumeroDoc ) 
								
										Errore=EseguiNoCTrace(UCASE(mid(session("progetto"),2)),Chiave,"PERS_DOC",session("idutente"),sIdPers,"P", "INS",sSQLm, 1, now(),cc)
							else
					
				
					
								sSQLm = "UPDATE PERS_DOC SET COD_DOCST='" & sTipoDoc & _
										
										"',DT_RIL_DOC=" & Convdatetodbs(sDataRilascio) & _
										",ID_DOC='" & sNumeroDoc & _
										"',DT_FIN_VAL=" & Convdatetodbs(sDataVal) & _
										",PRV_RIL='" & sRilasciato & _ 
										"',DT_TMST=" & Convdatetodb(now) & "WHERE ID_PERSONA=" & sIdPers & " and cod_docst='" & sTipoDoc & "'" 
										
									
								Chiave= (  sIdPers & "|" & sNumeroDoc )
										
										Errore=EseguiNoCTrace(UCASE(mid(session("progetto"),2)),Chiave,"PERS_DOC",session("idutente"),sIdPers,"P", "MOD",sSQLm, 1, sTms_Doc,cc)
							end if
						
						end if
			
								
							dim Esito
							if Errore= "0" then	
											 sValInf=ValInfPers(sIdpers,cc)
											 Errore=sValInf	
										if Errore = "0" then
											
											Esito="correttamente effettuata"
											CC.CommitTrans
								
											%><br>
											 <!--#include file="menu.asp"-->
											<br>
											<table border="0" cellspacing="2" cellpadding="1" width="500">
															<tr align="middle">
																<td class="tbltext3">
																	<%=messaggio & Esito %>
																</td>
															</tr>
											</table>
											
								           <%
										else
													Esito="non effettuabile "
													CC.RollbackTrans
													
												 
												  %><br>
												<!--#include file="menu.asp"-->
										      <br>
										    <table border="0" cellspacing="1" cellpadding="1" width="500">
											 <tr align="middle">
												<td class="tbltext3">
													<%=messaggio & Esito%>
												</td>
											 </tr>
											<tr align="middle">
												<td class="tbltext3">
													<%Response.Write (Errore)%> 
												</td>
											</tr>
										</table>
											<br>
											<table WIDTH="500" BORDER="0" CELLPADDING="0" CELLSPACING="0">
												<tr align="center">
													<td>
														<a href="javascript:history.back()"><img src="<%=Session("progetto")%>/images/Indietro.gif" border="0"></a>
													</td>
												</tr>
											</table>
										<%
										end if			
										
										
										
										
										
							else
								Esito="non effettuabile "
								CC.RollbackTrans
								

								%><br>
								  		<!--#include file="menu.asp"-->
								        <br>
								      <table border="0" cellspacing="1" cellpadding="1" width="500">
								  	 <tr align="middle">
								  		<td class="tbltext3">
								  			<%=messaggio & Esito %>
								  		</td>
								  	 </tr>
								  	<tr align="middle">
								  		<td class="tbltext3">
								  			<%Response.Write (Errore)%> 
								  		</td>
								  	</tr>
								  </table>
								  	<br>
								  	<table WIDTH="500" BORDER="0" CELLPADDING="0" CELLSPACING="0">
								  		<tr align="center">
								  			<td>
								  				<a href="javascript:history.back()"><img src="<%=Session("progetto")%>/images/Indietro.gif" border="0"></a>
								  			</td>
								  		</tr>
								  	</table><%
							end if										
										
													
			else
			
				Esito="non effettuabile "
				CC.RollbackTrans
				
								
								
				%><br>
						<!--#include file="menu.asp"-->
				      <br>
				    <table border="0" cellspacing="1" cellpadding="1" width="500">
					 <tr align="middle">
						<td class="tbltext3">
							<%=messaggio & Esito%>
						</td>
					 </tr>
					<tr align="middle">
						<td class="tbltext3">
							<%Response.Write (Errore)%> 
						</td>
					</tr>
				</table>
					<br>
					<table WIDTH="500" BORDER="0" CELLPADDING="0" CELLSPACING="0">
						<tr align="center">
							<td>
								<a href="javascript:history.back()"><img src="<%=Session("progetto")%>/images/Indietro.gif" border="0"></a>
							</td>
						</tr>
					</table><%
				
			end if	%>
							

</form>
<!--#include VIRTUAL="/include/CloseConn.asp"-->
<!--#include Virtual="/strutt_Coda2.asp"-->
