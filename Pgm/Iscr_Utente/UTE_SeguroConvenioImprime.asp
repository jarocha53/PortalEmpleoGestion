<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%
'===============================================
'	Imprime el convenio
'===============================================
function StringDelMes(iMes)
	select case imes
		case 1
			stringdelmes="Enero" 
		case 2
			stringdelmes="Febrero"
		case 3
			stringdelmes="Marzo"
		case 4
			stringdelmes="Abril"
		case 5
			stringdelmes="Mayo"
		case 6
			stringdelmes="Junio"
		case 7
			stringdelmes="Julio"
		case 8
			stringdelmes="Agosto"
		case 9
			stringdelmes="Septiembre"							 	 				 	 				 	 
		case 10
			stringdelmes="Octubre"
		case 11
			stringdelmes="Noviembre"
		case 12
			stringdelmes="Diciembre"							 	 				 	 				 	 									 	 				 	 				 	 									 	 				 	 				 	 									 	 				 	 				 	 	
	end select
end function


function convenioEmite(Nroconvenio, sFileNameDic)
	dim obBase
	dim nombre,apellido,cuil,Documento,CitaFechaHora,OficinaLugar,Direccion,OficinaMunicipio,FechaDia
	'set obBase=new InterfaceBD
	dim sFileNameConParametros
	
	'--------------------------------------------------------------
	' Lee los datos del convenio emitido de sp SeguroConvenioLee
	'----------------------------------------------------------------
	Set Rs = Server.CreateObject("ADODB.Recordset")
	Set cmdConvenio = Server.CreateObject("adodb.command")
	
	
	with cmdConvenio
		.activeconnection = connLavoro
		.Commandtext = "SeguroConvenioLee"
		.commandtype =adCmdStoredProc 
		'--------------------------------------------
		' Carga parametros
		'--------------------------------------------
		For Each Parameter In .Parameters
			If Parameter.Direction And adParamInput Then
				Select case ucase(Mid(Parameter.Name, 2))
					CASE "NROCONVENIO"
						Parameter.value =  Nroconvenio
					CASE ELSE
						' ????
				END SELECT
			End If
		Next
		'--------------------------------------------
		' Ejecuta
		'--------------------------------------------
'PL-SQL * T-SQL  
RECORDCOUNT = TransformPLSQLToTSQL (RECORDCOUNT) 
		Set Rs = .Execute(recordcount) 
	end with	
	'--------------------------------------------
	' Devuelve parametros
	'--------------------------------------------
	IF	rs.RecordCount >0 then
		nombre="" & Rs("nombre")
		apellido="" & Rs("apellido")
		cuil="" & Rs("cuil")
		Documento="" & Rs("documento")
		CitaFechaHora="" & Rs("CitaFechaHora")
		OficinaLugar="" & Rs("OficinaLugarDesc")
		Direccion="" & Rs("Direccion")
		OficinaMunicipio="" & Rs("OficinaMunicipioDesc")
		FechaDia="" & Rs("FechaDia")
		
	end if		
	'--------------------------
	' Crea el archivo del convenio
	'--------------------------
	' Archivo .htm
	sFileNameDic = CreaDichiarazione(2, nombre, apellido,documento,cuil, citaFechaHora, OficinaLugar,OficinaMunicipio, FechaDia,direccion,NroConvenio)
	' Cerrar todo
	
	%>
	
	<script language="javascript">
	//<%=sFileNameDic%>
		//window.open("<%=sFileNameDic%>,'','toolbar=no,scrollbars=yes,status=yes'")
	ImprimeConvenio = window.open('<%=sFileNameDic%>','', 'menubar=0,width=780,height=500,top=15,left=15,Scrollbars=yes,Resize=yes')	
	
	ImprimeConvenio.print();
		
	</script>
	<%
	
end function


function CreaDichiarazione(sIdpers, Nombre, Apellido, cDocumento,Cuil,CitaFechaHora,OficinaLugar,OficinaMunicipio,dFechadelDia,Direccion,NroConvenio)
'------------------------------------------------------
'	crea el archivo del convenio
'	sIdpers = idpersona
'	sNome = nombre
'	sCognome = apellido
'	dDataNasc = Fecha de nacimiento
'	sComNasc = Comuna de nacimiento
' Devuelve:
'	Ubicacion y nombre del archivo fisico 
'------------------------------------------------------
	dim sName, xName
	dim sFilePath
	dim oFileSystemObject
	dim oFileDichiarazione
	dim sMes
	
	smes=StringdelMes(month(date))
	
	' nombre unico de archivo
	xName = sIdpers
	xName = Cuil
	
	' oficina + Id de persona
	sName = Session("Creator") & "_ConvenioSCyE_" & xName & ".htm"
	' path del archivo
	sFilePath = SERVER.MapPath("\") & ImpostaProgetto & "/DocPers/Certificazioni/" & sName
	'Response.Write " el path es " &  sfilepath
	
	
	set oFileSystemObject = Server.CreateObject("Scripting.FileSystemObject")
	set oFileDichiarazione = oFileSystemObject.CreateTextFile(sFilePath,true)
	' arma las partes del convenio
	FoglioStile(oFileDichiarazione)
	IntDicCertificazione(oFileDichiarazione)
	CorpoDicCertificazione oFileDichiarazione,Nombre,Apellido,cuil,cDocumento,CitaFechaHora,OficinaLugar,OficinaMunicipio,dFechadelDia,Direccion,smes,NroConvenio
	Privacy(oFileDichiarazione)
	TimbroFirma oFileDichiarazione,Nombre,Apellido,OficinaLugar,smes,NroConvenio
	
		
	oFileDichiarazione.close
	set oFileSystemObject = nothing
	set oFileDichiarazione = nothing
	
	CreaDichiarazione = ImpostaProgetto & "/DocPers/Certificazioni/" & sName
end function

sub FoglioStile(fso)
	fso.writeline("	<HTML>")
	fso.writeline("	<HEAD>")
	fso.writeline("	<TITLE>CONVENIO DE ADHESION</TITLE>")
	fso.writeline("	<LINK REL=STYLESHEET TYPE='text/css' HREF='" & session("progetto") & "/fogliostile.css'>")
	
	
	fso.writeline("	</head>")
	fso.writeline("	<Script language=javascript>")
	fso.writeline("	   function InfoArticolo() {")
	fso.writeline("		  if (document.title != 'Certificaci�n') {")
	fso.writeline("			window.open ('UTE_InfoArt.asp','Info','width=550,height=300,top=15,left=15,Resize=No,Scrollbars=yes,screenX=w,screenY=h')")
	fso.writeline("		  }")
	fso.writeline("	   }")
	fso.writeline("	</Script>")
end sub

sub IntDicCertificazione(fso)
	
	fso.writeline("<img src='"+ ImpostaProgetto +"/images/membreteConvenio.jpg' align=left width='200' height='140'>")
	
	fso.writeline("<table border='0' align='center' cellspacing='0' cellpadding='0' width='500' >" )
	
	fso.writeline("	<tr>" )
	fso.writeline("		<TD>&nbsp;</TD>" )
	fso.writeline("	</TR>" )
	fso.writeline("	<tr>" )
	fso.writeline("		<TD>&nbsp;</TD>" )
	fso.writeline("	</TR>" )
	
	fso.writeline("	<tr>" )
	fso.writeline("		<TD>&nbsp;</TD>" )
	fso.writeline("	</TR>" )
	fso.writeline("	<tr>" )
	fso.writeline("		<TD>&nbsp;</TD>" )
	fso.writeline("	</TR>" )
	
	fso.writeline("	<tr>" )
	fso.writeline("		<TD>&nbsp;</TD>" )
	fso.writeline("	</TR>" )
	fso.writeline("	<tr>" )
	fso.writeline("		<TD>&nbsp;</TD>" )
	fso.writeline("	</TR>" )
	
	fso.writeline("</table>" )	
	fso.writeline("<Form name=formName1><table border='0' align='center' cellspacing='0' cellpadding='0' width='500' >" )
	fso.writeline("	<tr>" )
	fso.writeline("		<TD>&nbsp;</TD>" )
	fso.writeline("	</TR>" )
	fso.writeline("</table>")
end sub

sub CorpoDicCertificazione(fso, Nombre, Apellido,cuil,cDocumento,CitaFechaHora,OficinaLugar,OficinaMunicipio,dFechadelDia,Direccion,smes,NroConvenio)
	fso.writeline("<table border='0' align='center' cellspacing='0' cellpadding='0' width='500' >" )
	fso.writeline("	<tr>" )
	fso.writeline("		<TD>&nbsp;</TD>" )
	fso.writeline("	</TR>" )
	fso.writeline("	<tr>" )
	fso.writeline("		<TD align='center'><B>Convenio de Adhesi�n</B></TD>" )
	fso.writeline("	</TR>" )
	fso.writeline("	<tr>" )
	fso.writeline("		<TD align='center'><B>al Seguro de Capacitaci�n y Empleo</B></TD>" )
	fso.writeline("	</TR>" )
	
	fso.writeline("	<tr>" )
	fso.writeline("		<TD>&nbsp;</TD>" )
	fso.writeline("	</TR>" )
	fso.writeline("	<tr>" )
	fso.writeline("	<tr>" )
	fso.writeline("		<TD align='justify'>Por la presente <I><b>" & Apellido & " " & Nombre & "</b></i> con <I><b>" & cDocumento & "</b></i> y C.U.I.L. &nbsp; N� <I><b>" & cuil & "</b></i>,&nbsp; solicito &nbsp; al &nbsp; MINISTERIO   DE  TRABAJO,  EMPLEO  Y  </TD>" )
	fso.writeline("	</TR>" )
	fso.writeline("	<tr>" )
	fso.writeline("		<TD align='justify'>SEGURIDAD  &nbsp;&nbsp; SOCIAL &nbsp;&nbsp; mi &nbsp;&nbsp; incorporaci�n &nbsp;&nbsp; al &nbsp;&nbsp; SEGURO &nbsp;&nbsp; DE </TD>" )
	fso.writeline("	</TR>" )
	fso.writeline("	<tr>" )
	fso.writeline("		<TD align='justify'>CAPACITACION  Y   EMPLEO  y desvinculaci�n al PROGRAMA JEFES DE HOGAR</TD>" )
	fso.writeline("	</TR>" )
	fso.writeline("	<tr>" )
	fso.writeline("		<TD>&nbsp;</TD>" )
	fso.writeline("	</TR>" )
	fso.writeline("	<tr>" )
	fso.writeline("		<TD align='left'><I>Por participar del SEGURO DE CAPACITACION Y EMPLEO entiendo que recibir�: </I></TD>" )
	fso.writeline("	</TR>")
	fso.writeline("	<tr>" )
	fso.writeline("		<TD>&nbsp;</TD>" )
	fso.writeline("	</TR>" )
	fso.writeline("	<tr>" )
	fso.writeline("		<TD align='left'>" )
	fso.writeline("			<UL>")
	fso.writeline("				<LI>Apoyo y asesoramiento para la b�squeda de empleo.") 
	fso.writeline("				</LI>")
	fso.writeline("				<LI>Entrenamiento y capacitaci�n para el trabajo.")
	fso.writeline("				</LI>")
	fso.writeline("				<LI>Una suma dineraria no remunerativa de DOSCIENTOS VEINTICINCO PESOS ($225) durante los primeros 18 meses y de DOSCIENTOS PESOS ($200) durante los �ltimos 6 meses de participaci�n.")
	fso.writeline("				</LI>")
	fso.writeline("				<LI>Durante el tiempo de permanencia en el SEGURO DE CAPACITACION Y EMPLEO, la acreditaci�n de dichos meses para que sean computados para mi futura jubilaci�n.")
	fso.writeline("				</LI>")
	fso.writeline("			</UL>")
	fso.writeline("     </TD>")
	fso.writeline(" </TR>")
	
	fso.writeline("	<tr>" )
	fso.writeline("		<TD>&nbsp;</TD>" )
	fso.writeline("	</TR>" )
	fso.writeline("	<tr>" )
	fso.writeline("		<TD align='left'><I>Conociendo las caracter�sticas del SEGURO DE CAPACITACION Y EMPLEO, me comprometo a: </I></TD>" )
	fso.writeline("	</TR>")
	fso.writeline("	<tr>" )
	fso.writeline("		<TD>&nbsp;</TD>" )
	fso.writeline("	</TR>" )
	fso.writeline("	<tr>" )
	fso.writeline("		<TD align='left'>" )
	fso.writeline("			<UL>")
	fso.writeline("				<LI>presentarme en la Oficina de Empleo de mi localidad toda vez que sea convocado/a;") 
	fso.writeline("				</LI>")
	fso.writeline("				<LI>participar de entrevistas y/o actividades de apoyo para buscar empleo;")
	fso.writeline("				</LI>")
	fso.writeline("				<LI>aceptar las ofertas de empleo que se me presenten, siempre que �stas sean adecuadas con mi inter�s y experiencia de trabajo;")
	fso.writeline("				</LI>")
	fso.writeline("				<LI>cumplir con aquellas actividades de formaci�n, de entrenamiento y de apoyo a la inserci�n laboral que he acordado en la Oficina de Empleo;")
	fso.writeline("				</LI>")
	fso.writeline("				<LI>informar a la Oficina de Empleo cualquier problema de salud o de otro tipo que impida asistir o realizar las actividades acordadas, para su consideraci�n y registro;")
	fso.writeline("				</LI>")
	fso.writeline("				<LI>informar a la Oficina de Empleo los cambios de domicilio,tel�fono y cualquier otro dato para mi localizaci�n.")
	fso.writeline("				</LI>")
	fso.writeline("			</UL>")
	fso.writeline("     </TD>")
	fso.writeline(" </TR>")
	fso.writeline("	<tr>" )
	fso.writeline("	<tr>" )
	fso.writeline("		<TD>&nbsp;</TD>" )
	fso.writeline("	</TR>" )
		
	fso.writeline("	<TR>" )
	fso.writeline("<tr style='PAGE-BREAK-AFTER: always'><td>&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>")
	fso.writeline("	<tr>" )
	fso.writeline("		<TD align='left'><I>Declaro tener conocimiento: </I></TD>" )
	fso.writeline("	</TR>")
	fso.writeline("	<tr>" )
	fso.writeline("		<TD>&nbsp;</TD>" )
	fso.writeline("	</TR>" )
	
	fso.writeline("	<tr>" )
	fso.writeline("		<TD align='left'>" )
	fso.writeline("			<UL>")
	
	fso.writeline("				<LI>que mi solicitud de desvinculaci�n del PROGRAMA JEFES DE HOGAR  y de incorporaci�n al SEGURO DE CAPACITACION Y EMPLEO quedar� sujeta a la aprobaci�n por parte del MINISTERIO DE TRABAJO, EMPLEO Y SEGURIDAD SOCIAL;")
	fso.writeline("				</LI>")
	fso.writeline("				<LI>que mi incorporaci�n al SEGURO DE CAPACITACION Y EMPLEO se har� efectiva desde el mes en que perciba mi primer pago correspondiente al mismo;")
	fso.writeline("				</LI>")
	fso.writeline("				<LI>que si no concurro a las citas en la Oficina de Empleo cuando soy convocado/a o no cumplo con las actividades acordadas podr� ser dado de baja del SEGURO DE CAPACITACION Y EMPLEO;")
	fso.writeline("				</LI>")
	fso.writeline("				<LI>que podr�, en el caso en que acceda a un empleo, solicitar en la Oficina de Empleo de mi localidad la Suspensi�n para dejar de percibir temporariamente la prestaci�n dineraria no remunerativa del SEGURO DE CAPACITACION Y EMPLEO.")
	fso.writeline("				</LI>")
	fso.writeline("	</TR>" )
	fso.writeline("	<tr>" )
	fso.writeline("		<TD>&nbsp;</TD>" )
	fso.writeline("	</TR>" )
	fso.writeline("			</UL>")
	fso.writeline("     </TD>")
	fso.writeline(" </TR>")
	
	
	
	fso.writeline("	<tr>" )
	fso.writeline("		<TD align='left'>Finalmente, solicito que toda documentaci�n relacionada con el SEGURO </TD>" )
	fso.writeline("	</TR>" )
	
	fso.writeline("	<tr>" )
	fso.writeline("		<TD align='left'>DE CAPACITACION Y EMPLEO me sea remitida a <b>" & direccion & "</b></TD>" )
	fso.writeline("	</TR>" )
	
	fso.writeline("	<tr>" )
	fso.writeline("		<TD>&nbsp;</TD>" )
	fso.writeline("	</TR>" )
	
	fso.writeline("	<tr>" )
	fso.writeline("		<TD align='left'>Declaro que se ha confeccionado, en esta Oficina de Empleo, mi Historia Laboral; que conozco las caracter�sticas del SEGURO DE CAPACITACION Y EMPLEO y que cumplir� con los requisitos necesarios para poder permanecer en el mismo.</TD>" )
	fso.writeline("	</TR>" )
	
	fso.writeline("	<tr>" )
	fso.writeline("		<TD>&nbsp;</TD>" )
	fso.writeline("	</TR>" )
	
	fso.writeline("	<tr>" )
		fso.writeline("		<TD align='left' >Asimismo, expreso que los datos consignados en el presente convenio son verdaderos siendo el domicilio detallado en el presente al que deber�n remitir cualquier notificaci�n e  informaci�n vinculada con el SEGURO DE CAPACITACION Y EMPLEO " & "</TD>" )
	fso.writeline("	</TR>" )
	
	fso.writeline("	<tr>" )
	fso.writeline("		<TD>&nbsp;</TD>" )
	fso.writeline("	</TR>" )
	
	fso.writeline("	<tr>" )
	fso.writeline("		<TD align='left'>Me comprometo a presentarme el d�a <b>" & citafechahora & "</b> en esta oficina de empleo sita en <b>" & oficinalugar & "</b> para conocer el resultado de mi solicitud de incorporaci�n al SEGURO DE CAPACITACION Y EMPLEO y acordar las actividades de apoyo a la inserci�n laboral.</TD>" )
	fso.writeline("	</TR>" )
		
	fso.writeline("	<tr>" )
	fso.writeline("		<TD>&nbsp;</TD>" )
	fso.writeline("	</TR>" )
	
	fso.writeline("	<tr>" )
	fso.writeline("		<TD align='left'>En la Ciudad de <b>" & oficinaMunicipio & "</b> a los <b>" & day(date) & "</b> d�as del mes de <b>" &  smes & "</b> de <b>" & year(date) & "</b> se firman dos ejemplares de un mismo tenor y a un solo efecto. " & "</TD>" )
	fso.writeline("	</TR>" )
	fso.writeline("</table>")
end sub

sub Privacy(fso)
	fso.writeline("<table border='0' align='center' cellspacing='0' cellpadding='0' width='500' >" )
	fso.writeline("	<tr>" )
	fso.writeline("		<TD  colspan=2>&nbsp;" )
	fso.writeline("		</TD>" )
	fso.writeline("	</tr>" )
	fso.writeline("	<tr>" )
	fso.writeline("		<TD  colspan=2>&nbsp;" )
	'fso.writeline("		<!--#include Virtual = ""/TESTI/AUTORIZZAZIONI/Info_Privacy.htm""-->" )
	fso.writeline("	</TD></TR>" )
	fso.writeline("	</Table>" )
end sub

sub TimbroFirma(fso, Nombre, Apellido,OficinaMunicipio,sMes,NroConvenio)
	fso.writeline("<table border='0' align='center' cellspacing='0' cellpadding='0' width='500' >" )
	fso.writeline("	<tr>" )
	fso.writeline("		<TD colspan=2>&nbsp;</TD>" )
	fso.writeline("	</TR>" )
	
	fso.writeline("	<tr>" )
	fso.writeline("		<TD  align='left'><font size=2>Firma y Aclaraci�n del Solicitante</font></TD>" )
	fso.writeline("		<TD  align='left'><font size=2>Sello, firma y aclaraci�n del entrevistador</font></TD>" )
	fso.writeline("	</TR>" )
	
	fso.writeline("	<tr>" )
	fso.writeline("		<TD align='justify'><font size=1>" & Apellido & " " & Nombre & "</font></TD>" )
	fso.writeline("		<TD align='left' ><font size=2>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; de la Oficina de Empleo</font></TD>" )
	fso.writeline("	</TR>" )
	
	fso.writeline("	<tr>" )
	fso.writeline("		<TD  colspan=2>&nbsp;" )
	fso.writeline("		</TD>" )
	fso.writeline("	</tr>" )
	
	fso.writeline("	<tr>" )
	fso.writeline("		<TD align='justify'><font size=2>N�mero de Convenio: <b>" & NroConvenio & "</b></font></TD>" )
	fso.writeline("		<TD align='left' ><font size=2></TD>" )
	fso.writeline("	</TR>" )

	fso.writeline("		<TD>&nbsp;</TD>" )
	fso.writeline("		<TD>&nbsp;</TD>" )
	fso.writeline("	</TR>" )
	
	fso.writeline("	<tr>" )
	fso.writeline("		<TD colspan=2>&nbsp;</TD>" )
	fso.writeline("	</TR></table>" )
	
	fso.writeline("</Form>")
end sub

%>

