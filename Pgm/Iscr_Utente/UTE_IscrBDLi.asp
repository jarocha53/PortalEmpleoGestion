<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%'Option Explicit 
  ' Response.ExpiresAbsolute = Now() - 1 
  ' Response.AddHeader "pragma","no-cache"
  ' Response.AddHeader "cache-control","private"
  ' Response.CacheControl = "no-cache"
%>
<!-- #include virtual="/strutt_testa2.asp"-->
<!--#include virtual ="/include/openconn.asp"-->
<!--#include virtual ="/util/portallib.asp"-->
<% 
'if ValidateService(session("idutente"),"UTE_IscrBDLi", CC) <> "true" then 
'	response.redirect "/util/error_login.asp"
'end if
%>
<!-- ************** Javascript Inizio ************ -->
<script LANGUAGE="Javascript" src="Controlli.js"></script>


<script LANGUAGE="Javascript">

<!--#include virtual="/Include/help.inc"-->

<!--#include virtual = "/include/SelComune.js"-->

//include del file per fare i controlli sulla validit� delle date
<!--#include virtual = "/Include/ControlDate.inc"-->

//include del file per fare i controlli sulla numericit� dei campi
<!--#include virtual = "/Include/ControlNum.inc"-->

//include del file per fare i controlli sulla validit� del codice fiscale
<!--#include virtual = "/Include/ControlCodFisc.inc"-->

//include del file per fare i controlli sulle stringhe
<!--#include virtual = "/Include/ControlString.inc"-->

function CambiaLabel() {
  document.frmDomIscri.cmbStatoResidenza.value=TRIM(document.frmDomIscri.cmbStatoResidenza.value)
  if (document.frmDomIscri.cmbStatoResidenza.value =="") { 
 	 document.frmDomIscri.txtLbl.value="Frazione";
 	 document.frmDomIscri.cmbProvRes.disabled=false;
 	 document.frmDomIscri.txtComuneRes.disabled=false;
 	 document.frmDomIscri.txtCAP.disabled=false;
  }
  else {
         document.frmDomIscri.txtLbl.value="Citta' estera*";
         document.frmDomIscri.cmbProvRes.disabled=true;
         document.frmDomIscri.cmbProvRes.value="";
         document.frmDomIscri.txtComuneRes.disabled=true;
         document.frmDomIscri.txtComuneRes.value="";
         document.frmDomIscri.txtComRes.value = "";
         document.frmDomIscri.txtCAP.disabled=true;
         document.frmDomIscri.txtCAP.value="";
  }
}

function PulisciCom()
{
	document.frmDomIscri.txtComuneNascita.value = ""
	document.frmDomIscri.txtComune.value = ""
}

function PulisciRes()
{
	document.frmDomIscri.txtComuneRes.value = ""
	document.frmDomIscri.txtComRes.value = ""
	document.frmDomIscri.txtCAP.value = ""
}

//----------------------------------------------------------------------------------------------------------------------------------------------------------


//Funzione per i controlli dei campi da inserire 

	function ControllaDati(frmDomIscri){
		sOggi = frmDomIscri.datasis.value;
             
		//Cognome
		frmDomIscri.txtCognome.value=TRIM(frmDomIscri.txtCognome.value)
		if (frmDomIscri.txtCognome.value == ""){
			alert("Cognome obbligatorio")
			frmDomIscri.txtCognome.focus() 
			return false
		}
		sCognome=ValidateInputStringWithOutNumber(frmDomIscri.txtCognome.value)
		
		if  (sCognome==false){
			alert("Formato de apellido err�neo.")
			frmDomIscri.txtCognome.focus() 
			return false
		}
		
		//Nome
		frmDomIscri.txtNome.value=TRIM(frmDomIscri.txtNome.value)
		if (frmDomIscri.txtNome.value == ""){
			alert("Nombre obligatorio")
			frmDomIscri.txtNome.focus() 
			return false
		}
		sNome=ValidateInputStringWithOutNumber(frmDomIscri.txtNome.value)
		if  (sNome==false){
			alert("Formato de nombre err�neo.")
			frmDomIscri.txtNome.focus() 
			return false
		}
		//Obbligatoriet� della Data di Nascita
		DataNascita = TRIM(frmDomIscri.txtDataNascita.value)
		if (DataNascita == ""){
			alert("Fecha de nacimiento obligatoria")
			frmDomIscri.txtDataNascita.focus() 
			return false
		}
		//Controllo formale della Data di Nascita
		if (!ValidateInputDate(DataNascita)){
			frmDomIscri.txtDataNascita.focus() 
			return false
		}
		//Controllo della validit� della Data di Nascita
		if (ValidateRangeDate(DataNascita,sOggi)==false){
			alert("La fecha de nacimiento no debe ser superior a la fecha actual.")
			frmDomIscri.txtDataNascita.focus()
			return false
		}	
				
		//Comune di Nascita e Provincia di Nascita obbligatori
		//e in alternativa con Nazione di Nascita
		frmDomIscri.txtComuneNascita.value=TRIM(frmDomIscri.txtComuneNascita.value)
		if (frmDomIscri.txtComuneNascita.value == ""){ 
			if (frmDomIscri.cmbProvinciaNascita.value == ""){
				if (frmDomIscri.cmbNazioneNascita.value == ""){
					alert("Indicar el lugar de nacimiento.\nMunicipio y departamento, o pa�s")
					frmDomIscri.cmbProvinciaNascita.focus() 
					return false
				}
			}
		}
       
		if (frmDomIscri.txtComuneNascita.value != ""){ 
			if (frmDomIscri.cmbProvinciaNascita.value != ""){
				if(frmDomIscri.cmbNazioneNascita.value != ""){
					alert("Indicar solo el municipio y el departamento de nacimiento o bien el pa�s de nacimiento")
					frmDomIscri.cmbNazioneNascita.focus() 
					return false
				}
			}	
		}
		//Se Comune di Nascita � digitato, la Provincia � obbligatoria
		if (frmDomIscri.txtComuneNascita.value != ""){
			if (frmDomIscri.cmbProvinciaNascita.value == ""){
				alert("Departamento de nacimiento obligatorio")
				frmDomIscri.cmbProvinciaNascita.focus() 
				return false
			}
		}
		//Se la Provincia di Nascita � digitata, il Comune � obbligatorio
		if  (frmDomIscri.txtComuneNascita.value == ""){
			if (frmDomIscri.cmbProvinciaNascita.value != ""){
				alert("Municipio de nacimiento obligatorio")
				frmDomIscri.txtComuneNascita.focus() 
				return false
			}
		}
		//Sesso
		if (frmDomIscri.cmbSesso.value == ""){
			alert("Indicar el sexo")
			frmDomIscri.cmbSesso.focus() 
			return false
		}
		
		
//***********************************************************************		
		//Codice Fiscale
		frmDomIscri.txtCodFisc.value = TRIM(frmDomIscri.txtCodFisc.value)
		if (frmDomIscri.cmbProvRes.value != ""){
		     if (frmDomIscri.txtCodFisc.value == "")
		         {
			      alert("El campo c�digo fiscal es obligatorio!")
			      frmDomIscri.txtCodFisc.focus() 
			      return false
		      }
		}		
			
		if (frmDomIscri.txtCodFisc.value != ""){
					
			//Codice Fiscale deve essere di 16 caratteri
			if (frmDomIscri.txtCodFisc.value.length != 16){
				alert("Formato de c�digo fiscal err�neo")
				frmDomIscri.txtCodFisc.focus() 
				return false
			}
			//Contollo la validit� del Codice Fiscale
			var contrCom=""
			DataNascita = frmDomIscri.txtDataNascita.value;
			CodFisc = frmDomIscri.txtCodFisc.value.toUpperCase();
			Sesso = frmDomIscri.cmbSesso.value;
				
			if (frmDomIscri.txtComuneNascita.value != ""){
		          contrCom =frmDomIscri.txtComune.value;   
			}
			else{
		          contrCom = frmDomIscri.cmbNazioneNascita.value
			}
		
			if (!ControllCodFisc(DataNascita,CodFisc,Sesso,contrCom)){
				alert("C�digo fiscal err�neo");
				frmDomIscri.txtCodFisc.focus(); 
				return false
			}
		}
		
	//***********************************************************************	
		//Cittadinanza
		if (frmDomIscri.cmbCittadinanza.value == ""){
			alert("Ciudadan�a obligatoria")
			frmDomIscri.cmbCittadinanza.focus() 
			return false
		}
		//Sesso Donna senza Posizione Militare
/*		if (frmDomIscri.cmbSesso.value == "F"){
			if (frmDomIscri.cmbLeva.value != ""){
				alert("La Posizione di Leva � obbligatoria solo per persone di sesso maschile")
				frmDomIscri.cmbLeva.focus() 
				return false
			}
		}
		//Sesso Uomo, Posizione Militare obbligatoria solo se cittadino Italiano
		if (frmDomIscri.cmbSesso.value == "M") { 
		    if (frmDomIscri.cmbCittadinanza.value == "IT"){
				if (frmDomIscri.cmbLeva.value == ""){
					alert("Posizione di Leva obbligatoria");
					frmDomIscri.cmbLeva.focus(); 
					return false;
				}
			}
			else {
					if (!frmDomIscri.cmbLeva.value == ""){
						alert("I cittadini stranieri non devono avere la posizione di leva");
						frmDomIscri.cmbLeva.focus(); 
						return false;
					}
		   }
		} */
		//Recapito Telefonico
		if (frmDomIscri.txtNumTel.value != ""){
					
			//Recapito Telefonico deve essere numerico
			if (!IsNum(frmDomIscri.txtNumTel.value)){
				alert("El n�mero telef�nico debe ser num�rico")
				frmDomIscri.txtNumTel.focus() 
				return false
			}
			//Recapito Telefonico deve essere diverso da zero
			if (eval(frmDomIscri.txtNumTel.value) == 0){
				alert("El n�mero telef�nico no puede ser cero")
				frmDomIscri.txtNumTel.focus()
				return false
			}
		}
		
	/*	frmDomIscri.txtNCompNucFam.value=TRIM(frmDomIscri.txtNCompNucFam.value); 
		if  (frmDomIscri.txtNCompNucFam.value == ""){
			frmDomIscri.txtNCompNucFam.value = "0";
		}
		if (!IsNum(frmDomIscri.txtNCompNucFam.value)){
				alert("Il numero dei componenti del nucleo familiare deve essere numerico")
				frmDomIscri.txtNCompNucFam.focus() 
				return false
			} */
			
		// numero del passaporto	
		 if (TRIM(frmDomIscri.txtNumDoc.value) == "")
		 {
			        alert("El campo n�mero de pasaporte es obligatorio.")
			        frmDomIscri.txtNumDoc.focus() 
			        return false
		 }	
		 
		 if (frmDomIscri.cmbStatRil.value=="") 
		 {
			        alert("Indicare lo Stato di rilascio del Passaporto")
			        frmDomIscri.cmbStatRil.focus() 
			        return false
		 }	

		 //Data Decorrenza del Passaporto
		 //frmDomIscri.txtDataIniValidita.value =TRIM(frmDomIscri.txtDataIniValidita.value)         
		 if (frmDomIscri.txtDataIniValidita.value == "")
		 {
			        alert("Indicare la decorrenza del Passaporto")
			        frmDomIscri.txtDataIniValidita.focus() 
			        return false
		 }        

		//Controllo della validit� della Data Decorrenza Passaporto.
		if (!ValidateInputDate(frmDomIscri.txtDataIniValidita.value)){
			frmDomIscri.txtDataIniValidita.focus() 
			return false
		}

		//Controllo della validit� della Data Scadenza Passaporto
		if (ValidateRangeDate(frmDomIscri.txtDataIniValidita.value,DataNascita)==true){
		        alert("La decorrenza del passaporto deve essere superiore alla data di nascita.")
			    frmDomIscri.txtDataIniValidita.focus() 
			    return false
		}

		//Controllo della validit� della Decorrenza Passaporto
		if (ValidateRangeDate(frmDomIscri.txtDataIniValidita.value,sOggi)==false){
		        alert("La decorrenza del passaporto non pu� essere superiore alla data odierna")
			    frmDomIscri.txtDataIniValidita.focus() 
			    return false
		}

		 //Data Scadenza del Passaporto
		 frmDomIscri.txtDataValidita.value =TRIM(frmDomIscri.txtDataValidita.value)         
		 if (frmDomIscri.txtDataValidita.value == "")
		 {
			        alert("Indicare la Scadenza del Passaporto")
			        frmDomIscri.txtDataValidita.focus() 
			        return false
		 }
		         
		 //Controllo della validit� della Data Scadenza Passaporto.
		if (!ValidateInputDate(frmDomIscri.txtDataValidita.value)){
			frmDomIscri.txtDataValidita.focus() 
			return false
		}
		 //Controllo della validit� della Data Scadenza Passaporto
		if (ValidateRangeDate(frmDomIscri.txtDataValidita.value,sOggi)==true){
		        alert("La data di scadenza del passaporto deve essere superiore alla data odierna.")
			    frmDomIscri.txtDataValidita.focus() 
			    return false
		}
		   
		//**************************************************************
		//Indirizzo di Residenza
		frmDomIscri.txtIndirizzo.value=TRIM(frmDomIscri.txtIndirizzo.value)
		if  (frmDomIscri.txtIndirizzo.value == ""){
			alert("Direcci�n obligatoria")
			frmDomIscri.txtIndirizzo.focus() 
			return false
		}
		else {
				sIndirizzo=ValidateInputIndirizzi(frmDomIscri.txtIndirizzo.value)
				if  (sIndirizzo==false){
					alert("Formato de direcci�n err�neo.")
					frmDomIscri.txtIndirizzo.focus() 
					return false
				}
		}
				
	    //Comune di Residenza , Provincia di Residenza e CAP obbligatori
		//e in alternativa con Stato di Residenza
		frmDomIscri.txtComuneRes.value=TRIM(frmDomIscri.txtComuneRes.value)
		frmDomIscri.cmbProvRes.value=TRIM(frmDomIscri.cmbProvRes.value)
		frmDomIscri.cmbStatoResidenza.value = TRIM(frmDomIscri.cmbStatoResidenza.value)
		frmDomIscri.txtCAP.value = TRIM(frmDomIscri.txtCAP.value)
		if (frmDomIscri.txtComuneRes.value == ""){ 
			if (frmDomIscri.cmbProvRes.value == ""){
				if (frmDomIscri.txtCAP.value == ""){
					if (frmDomIscri.cmbStatoResidenza.value == ""){
						alert("Comune , departamento e CAP di Residenza o Stato di Residenza obbligatori")
						frmDomIscri.cmbProvRes.focus() 
						return false
					}
				}
			}		
		}
		
		//Se Comune di Residenza � digitato, la Provincia � obbligatoria
		if (frmDomIscri.txtComuneRes.value != ""){
			if (frmDomIscri.cmbProvRes.value == ""){
				alert("Departamento de residencia obligatoria")
				frmDomIscri.cmbProvRes.focus() 
				return false
			}
		}
		//Se la Provincia di Residenza � digitata, il Comune � obbligatorio
		if  (frmDomIscri.txtComuneRes.value == ""){
			if (frmDomIscri.cmbProvRes.value != ""){
				alert("Municipio de residencia obligatorio")
				frmDomIscri.txtComuneRes.focus() 
				return false
			}
		}
			
		//CAP 
		if (frmDomIscri.cmbProvRes.value != ""){
		    //CAP  obbligatorieta'
			if (frmDomIscri.txtCAP.value == ""){
				alert("CAP obbligatorio")
				frmDomIscri.txtCAP.focus() 
				return false
			}
			//CAP deve essere numerico
			if (!IsNum(frmDomIscri.txtCAP.value)){
				alert("Il CAP deve essere Numerico")
				frmDomIscri.txtCAP.focus() 
				return false
			}
			//CAP deve essere diverso da zero
			if (eval(frmDomIscri.txtCAP.value) == 0){
				alert("Il CAP non pu� essere zero")
				frmDomIscri.txtCAP.focus()
				return false
			}
			//CAP deve essere di 5 caratteri
			if (frmDomIscri.txtCAP.value.length != 5){
				alert("Formato del CAP errato")
				frmDomIscri.txtCAP.focus() 
				return false
			}
		}
		else  {		
		        if (frmDomIscri.txtCAP.value != ""){
					alert("Departamento de residencia obligatoria")
					frmDomIscri.cmbProvRes.focus() 
					return false
				}
		}
		
		if (frmDomIscri.txtComuneRes.value != ""){ 
			if (frmDomIscri.cmbProvRes.value != ""){
			    if (frmDomIscri.txtCAP.value != ""){
					if(frmDomIscri.cmbStatoResidenza.value != ""){
						alert("Indicar solo el municipio, el departamento y el CAP de residencia o bien el Estado de residencia")
						frmDomIscri.cmbStatoResidenza.focus() 
						return false
					}
				}	
			}
		}
	    //Controllo Frazione
		frmDomIscri.txtFrazione.value=TRIM(frmDomIscri.txtFrazione.value)
	    if(frmDomIscri.cmbStatoResidenza.value != ""){
			 if (frmDomIscri.txtFrazione.value == ""){
					alert("Inserire la Citta' nel campo frazione/citta")
					frmDomIscri.txtFrazione.focus() 
					return false
			 }
	    }
	    if (frmDomIscri.txtFrazione.value != ""){
			sFrazione=ValidateInputStringWithOutNumber(frmDomIscri.txtFrazione.value)
			if  (sFrazione==false){
				alert("Frazione/Citta' formalmente errata.")
				frmDomIscri.txtFrazione.focus() 
				return false
			}
		}	
//**************************************************************************

		//Data di Iscrizione non deve essere superiore alla Data Odierna
		
		//Titolo di Studio
		if (frmDomIscri.cmbTitStud.value == ""){
			alert("T�tulo de estudio obligatorio")
			frmDomIscri.cmbTitStud.focus() 
			return false
		}
		//Anno Rilascio Titolo Studio
		if (frmDomIscri.cmbTitStud.value != "NES"){
			//Anno Rilascio Titolo obbligatorio
			if ((frmDomIscri.txtAnnoRilascio.value == "")){
				alert("El a�o de expedici�n del t�tulo es obligatorio")
				frmDomIscri.txtAnnoRilascio.focus()
				return false
			}
			//Anno Rilascio Titolo numerico
			if (!IsNum(frmDomIscri.txtAnnoRilascio.value)){
				alert("El a�o de expedici�n del t�tulo debe ser num�rico")
				frmDomIscri.txtAnnoRilascio.focus()
				return false
			}
			//Anno Rilascio Titolo deve essere di 4 caratteri
			if (frmDomIscri.txtAnnoRilascio.value.length != 4){
				alert("Indicar el a�o de expedici�n del t�tulo en el formato AAAA")
				frmDomIscri.txtAnnoRilascio.focus() 
				return false
			}
			//Anno Rilascio Titolo deve essere diverso da zero 
			if (eval(frmDomIscri.txtAnnoRilascio.value) == 0){
				alert("El a�o de expedici�n del t�tulo no puede ser cero")
				frmDomIscri.txtAnnoRilascio.focus()
				return false
			}
			//Anno Rilascio Titolo deve essere superiore all'anno di nascita
			sAnno = frmDomIscri.txtDataNascita.value
			sAnno = sAnno.substring(6,10)
			sAnno1 = frmDomIscri.txtAnnoRilascio.value
			if (!(sAnno1 > sAnno)){
				alert("El a�o de expedici�n del t�tulo debe ser superior al a�o de nacimiento")
				frmDomIscri.txtAnnoRilascio.focus()
				return false
			}
		//Anno Rilascio Titolo deve essere minore all'anno-oggi
		
			sAnno = sOggi.substring(6,10)
			//alert ("Anno sistema - anno rilascio titolo=" + sAnno)
			//anno = new Date();
			//sAnno = anno.getYear();
			if (frmDomIscri.txtAnnoRilascio.value > sAnno){
				alert("El a�o de expedici�n del t�tulo no debe ser superior al a�o corriente")
				frmDomIscri.txtAnnoRilascio.focus()
				return false
			}
			
	   }
	   else {
				frmDomIscri.txtAnnoRilascio.value = TRIM(frmDomIscri.txtAnnoRilascio.value);	
				if (!frmDomIscri.txtAnnoRilascio.value == "") {
						alert("Si no posee un t�tulo de estudio, quite el a�o de expedici�n del t�tulo");
						frmDomIscri.txtAnnoRilascio.focus()
						return false
				}		
	   }
	
	//Controllo Condizione di Appartenenza
			if (frmDomIscri.cbbAppartenenza.value == ""){
				alert("Ingrese condici�n de pertenencia")
				frmDomIscri.cbbAppartenenza.focus() 
				return false
		    }
			
	//Controllo Data Condizione di Appartenenza Dlgs. 181/2000
			if (frmDomIscri.cbbAppartenenza.value!="01" && frmDomIscri.cbbAppartenenza.value!="02"){
				if (frmDomIscri.txtDecDis.value == ""){			
					alert("El campo 'Dal' es obligatorio.")
					frmDomIscri.txtDecDis.focus() 
					return false
				}
				//Controllo della validit� dal 
				if (!ValidateInputDate(frmDomIscri.txtDecDis.value)){
					frmDomIscri.txtDecDis.focus() 
					return false
				}
			}			
		return true
	}
	
function ControllaDisocc() {
	if (document.frmDomIscri.cbbAppartenenza.value != ""){
		document.frmDomIscri.cbbDisocc.disabled = false ;		
	}
	else {
		document.frmDomIscri.cbbDisocc.value = ""
		document.frmDomIscri.cbbDisocc.disabled = true 
	}
	if ((frmDomIscri.cbbAppartenenza.value=="03")||(frmDomIscri.cbbAppartenenza.value=="04")||(frmDomIscri.cbbAppartenenza.value=="05"))
		{
			frmDomIscri.txtDecDis.disabled=false;
	}
	else {
		frmDomIscri.txtDecDis.disabled=true;
		frmDomIscri.txtDecDis.value="";
	}
		
}
//----------------------------------------------------------------------------------------------------------------------------------------------------------	
	
</script>

<!-- ************** Javascript Fine   ************ -->

<!-- ************** ASP Inizio *************** -->

<!--#include virtual = "/include/DecCod.asp"-->
<!--#include virtual = "/include/ControlDateVB.asp"-->
<!--#include virtual = "/util/dbutil.asp"-->

<%	
'-------------------------------------------------------------------------------------------------------------------------------
Sub Titolo(sTitolo,sDescTitolo) %>
    <table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
		<tr height="18">
			<td class="sfondomenu" width="67%" height="18"><span class="tbltext0"><b>&nbsp;<%=sTitolo%></b></span></td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
			<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
		</tr>
	</table>
	<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
		<tr>
			<td align="left" class="sfondocomm"><%=sDescTitolo%></td>
		</tr>
		<tr height="2">
			<td class="sfondocomm" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>
	<br>
<%
end sub

Sub Msgetto(Msg)
%>
	<br>
	<table align="center">
		<tr>
			<td class="tbltext3"><b><%=Msg%></b></td>
		</tr>	
	</table>
<%
End Sub

'-------------------------------------------------------------------------------------------------------------------------------

sub Inizio()
%>
    
    <script LANGUAGE="javascript" FOR="window" EVENT="onload">
    window.frmDomIscri.cmbStatoResidenza.value = "";
    window.frmDomIscri.cmbStatoResidenza.text = "";
    window.frmDomIscri.txtFrazione.value = "";
    window.frmDomIscri.txtFrazione.text = "";
    window.frmDomIscri.txtLbl.value = "Frazione";
    window.frmDomIscri.cmbProvRes.value = "";
    window.frmDomIscri.cmbProvRes.text = "";
    
    
    window.frmDomIscri.txtComuneRes.value = "";
    window.frmDomIscri.txtComuneRes.text = "";
	window.frmDomIscri.txtComRes.value = "";
	window.frmDomIscri.txtComRes.text = "";
	window.frmDomIscri.txtCAP.value = "";
    window.frmDomIscri.txtCAP.text = "";
    
	window.frmDomIscri.txtComuneNascita.value = "";
	window.frmDomIscri.txtComuneNascita.text = "";
	window.frmDomIscri.txtComune.value = "";
	window.frmDomIscri.txtComune.text = "";
	
	window.frmDomIscri.cbbAppartenenza.value = "";
	window.frmDomIscri.cbbAppartenenza.text = "";
	window.frmDomIscri.txtDecDis.value = "";
	window.frmDomIscri.txtDecDis.text = "";
    </script>
    
	<!--table border="0" width="520" cellspacing="0" cellpadding="0" height="81">	   <tr>	     <td width="500" background="<%=Session("Progetto")%>/images/titoli/strumenti2b.gif" height="81" valign="bottom" align="right">	       <table border="0" width="260" height="30" cellspacing="0" cellpadding="0">	         <tr>	           <td width="100%" valign="top" align="right">	           <b class="tbltext1a">Domando di iscrizione</b></td>	         			</tr>	       </table>	     </td>	   </tr>	</table-->
	<br>
	<table cellpadding="0" cellspacing="0" width="500" border="0">
		<tr height="18">
			<td class="sfondomenu" height="18" width="67%">
			<span class="tbltext0"><b>&nbsp;Acquisizione Disponibilit� al Lavoro in Italia </b></span></td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
			<td valign="middle" align="right" class="tbltext1" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">(*) campi obbligatori</td>
		</tr>
		<tr>
			<td class="sfondocomm" width="57%" colspan="3">
				Modulo di acquisizione della disponibilit� al lavoro in Italia.
		
			<a href="Javascript:Show_Help('/Pgm/help/Iscr_Utente/UTE_IscrBDLi')" onmouseover="javascript:window.status=' '; return true"><img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
			</td>
		</tr>
		<tr height="2">
			<td colspan="3" class="sfondocomm" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>
	
	<form method="post" name="frmDomIscri" onsubmit="return ControllaDati(this)" action="UTE_CnfBDLi.asp">
<%
end sub

'------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

sub ImpPagina()
    dim sInt 
%>

	    <% 
	       sCommento =  "Presta molta attenzione a quanto inserisci:<br>" &_ 
				        "Indica di seguito i tuoi dati anagrafici."  
				        
	       call Titolo("Informazioni Personali",sCommento)
	    %>
	    <!--tr>	        <td align="left" class="tbltext3" colspan="2">	        	<b>Informazioni Personali</b>			</td>	        	    </tr>	    <tr>			<td align="left" class="textblack" colspan="2">				Presta molta attenzione a quanto inserisci: 				i tuoi dati anagrafici e occupazionali verranno confrontati con 				quelli in possesso del Centro per l'Impiego a cui sei iscritto.				<br>Ogni incongruenza puo' comportare la tua esclusione dalla 				graduatoria del progetto.			</td>	    </tr-->
	
	
		<table width="500" border="0" cellspacing="2" cellpadding="1">
		<tr>
			<td align="left" class="tbltext1" width="30%">
				<b>Cognome*</b>
			</td>
			<td align="left">
				<input style="TEXT-TRANSFORM: uppercase;" size="30" maxlength="30" class="textblack" name="txtCognome">
			</td>
	    </tr>
	    <tr>
			<td align="left" class="tbltext1" width="30%">
				<b>Nome*</b>
			</td>
			<td align="left">
				<input style="TEXT-TRANSFORM: uppercase;" size="30" maxlength="30" class="textblack" name="txtNome">
			</td>
	    </tr>

	    <tr>
			<td align="left" class="tbltext1" width="30%">
				<b>Data di Nascita*</b> (gg/mm/aaaa)
			</td>	
			<td align="left">
				<input style="TEXT-TRANSFORM: uppercase;" size="11" maxlength="10" class="textblack" name="txtDataNascita">
			</td>
	    </tr>
	    <tr><td>&nbsp;</td></tr>
	    <tr>
			<td align="left" class="textblack" colspan="2">
				Para <b>el nacido en Argentina</b> especificar departamento y municipio de nacimiento.
			</td>
	    </tr>
	    
		<tr>		
			<td align="left" class="tbltext1" width="30%">
				<b>Departamento de nacimiento</b>
			</td>			
			<td align="left">				
<%
			sInt = "PROV|0|" & date & "| |cmbProvinciaNascita' onchange='PulisciCom()|ORDER BY DESCRIZIONE"			
			CreateCombo(sInt)
						
%>
			</td>
		</tr>		
		<tr>		
			<td align="left" class="tbltext1" width="30%">
				<b>Municipio de nacimiento</b>
			</td>
			<td nowrap>
				<span class="tbltext">
				<input type="text" name="txtComuneNascita" style="TEXT-TRANSFORM: uppercase;" class="textblack" size="35" readonly value="<%=DescrComuneNascita%>">
				<input type="hidden" id="txtComune" name="txtComune" value="<%=COM_NASC%>">
<%
				NomeForm="frmDomIscri"
				CodiceProvincia="cmbProvinciaNascita"
				NomeComune="txtComuneNascita"
				CodiceComune="txtComune"
				Cap="NO"
%>
				<a href="Javascript:SelComune('<%=NomeForm%>','<%=CodiceProvincia%>','<%=NomeComune%>','<%=CodiceComune%>','<%=Cap%>')" ID="imgPunto1" name="imgPunto1" onmouseover="javascript:window.status='';return true"><img border="0" src="<%=Session("Progetto")%>/images/bullet1.gif"></a>
				</span>				
			</td>
		</tr>		
		<tr>
			<td class="textblack" colspan="2">
				Para <b>el nacido fuera de Argentina</b>, en cambio, especificar solo la naci�n de nacimiento. 			</td>
		</tr>
		
	    <tr>
			<td align="left" class="tbltext1" width="30%">
				<b>Naci�n de nacimiento</b>
	        </td>
			<td align="left">
				<%
				set rsComune = Server.CreateObject("ADODB.Recordset")
				sSQL = "SELECT CODCOM,DESCOM from COMUNE WHERE CODCOM like 'Z%' " &_
						"ORDER BY DESCOM"

'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
				rsComune.Open sSQL, CC
				
				%>
				<select class="textblack" ID="cmbNazioneNascita" name="cmbNazioneNascita">
				<option></option>
				<%
				do while not rsComune.EOF 
					Response.Write "<OPTION "

					Response.write "value ='" & rsComune("CODCOM") & _
						"'> " & rsComune("DESCOM")  & "</OPTION>"
					
					rsComune.MoveNext 
				loop

				rsComune.Close	
				%>
				</select>
	        </td>
		</tr>
		<tr><td>&nbsp;</td></tr>
		<tr>
			<td align="left" class="tbltext1" width="30%">
				<b>Sesso*</b>
	        </td>
			<td align="left">
				<select class="textblack" id="cmbSesso" name="cmbSesso">
					<option selected>
					<option value="F">FEMENINO
					<option value="M">MASCULINO
					</option>
				</select>
	        </td>
	  	</tr>
	  	<tr>
			<td align="left" class="tbltext1" width="30%">
				<b>C.I.</b>
	        </td>
			<td align="left">
				<input style="TEXT-TRANSFORM: uppercase;" size="22" maxlength="16" class="textblack" name="txtCodFisc">
	        </td>
	    </tr>
		<tr>
			<td align="left" class="tbltext1" width="30%">
				<b>Ciudadan�a*</b>
	        </td>
			<td align="left">
				<%	sInt = "STATO|0|" & date & "| |cmbCittadinanza|ORDER BY DESCRIZIONE"
					CreateCombo(sInt)	%>
	        </td>
	    </tr>		
		<!--tr>			<td align="left" class="tbltext1" width="30%">				<b>Posizione di Leva</b>	        </td>			<td align="left">				<%				'sInt = "POMIL|0|" & date & "| |cmbLeva|ORDER BY DESCRIZIONE"							'CreateCombo(sInt)				%>	        </td>	    </tr-->
		<tr>
			<td align="left" class="tbltext1" width="30%">
				<b>Estado Civil</b>
	        </td>
			<td align="left">
				<%
				sInt = "STCIV|0|" & date & "| |cmbStatoCiv|ORDER BY DESCRIZIONE"			
				CreateCombo(sInt)
				%>
	        </td>
	    </tr>
			  	    
	    <tr>
			<td align="left" class="textblack" colspan="2">
			Especifique un n�mero telef�nico en el cual los operadores puedan contactarle directamente para comunicaciones urgentes.
			Indique a continuaci�n todos los n�meros que lo componen sin a�adir espacios u otros separadores. 
			</td>
	    </tr>
	    <tr>
			<td align="left" class="tbltext1" width="30%">
				<b>N�mero Telef�nico</b>
	        </td>
	        <td align="left">
				<input style="TEXT-TRANSFORM: uppercase;" size="25" maxlength="20" class="textblack" name="txtNumTel">
	        </td>
	    </tr>
	    <tr>
			<!--td align="left" class="tbltext1" width="30%">				<b>Numero Componenti nucleo familiare</b>	        </td>	        <td align="left">				<input style="TEXT-TRANSFORM: uppercase;" size="10" maxlength="8" class="textblack" name="txtNCompNucFam" Value="0">	        </td>	    </tr-->
	    <tr><td>&nbsp;</td></tr>
	    <tr>
			<td align="left" class="tbltext1" width="30%">
				<b>Passaporto numero*</b>
	        </td>
	        <td align="left">
				<input size="20" maxlength="15" class="textblack" style="TEXT-TRANSFORM: uppercase;" name="txtNumDoc">
	        </td>
	    </tr>
	    <tr>
			<td align="left" class="tbltext1" width="30%">
				<b>Rilasciato in*</b>
	        </td>
	        <td align="left" colspan="2" width="60%">
				<span class="tbltext" nowrap>
<%
					dim rsNazione
					dim descNazione
				
					sInt = "STATO|0|" & date & "||cmbStatRil|AND CODICE <> 'XX' ORDER BY DESCRIZIONE"			
					CreateCombo(sInt)				
%>	
				</span>
			</td>
	    </tr>
	    <tr>
			<td align="left" class="tbltext1" width="30%">
				<b>Decorrenza*</b>(gg/mm/aaaa)
	        </td>
	        <td align="left">
					<input style="TEXT-TRANSFORM: uppercase;" size="11" maxlength="10" class="textblack" name="txtDataIniValidita">
	        </td>
	    </tr>
	    <tr>
			<td align="left" class="tbltext1" width="30%">
				<b>Scadenza il*</b>(gg/mm/aaaa)
	        </td>
	        <td align="left">
					<input style="TEXT-TRANSFORM: uppercase;" size="11" maxlength="10" class="textblack" name="txtDataValidita">
	        </td>
	    </tr>
		<tr><td>&nbsp;</td></tr>
   </table>
     <%  
	       sCommento = "Specifica i tuoi dati di residenza."
	       call Titolo("Residenza",sCommento)%>
			
   <table width="500" border="0" cellspacing="2" cellpadding="1">
	    <tr>
			<td align="left" class="tbltext1" width="30%">
				<b>Indirizzo*</b>
	        </td>
	        <td align="left">
				<input size="30" maxlength="50" name="txtIndirizzo" class="textblack" style="TEXT-TRANSFORM: uppercase;">
	        </td>
	    </tr>
	    <tr><td>&nbsp;</td></tr>
	    <tr>
			<td class="textblack" colspan="2">
				Para <b>los residentes en el exterior</b>, completar el estado de residencia. 
			</td>
		</tr>
		
	    <tr>
			<td align="left" class="tbltext1" width="30%">
				<b>Estado de residencia</b>
	        </td>
			<td align="left">
			       
				<%
				set rsComune = Server.CreateObject("ADODB.Recordset")
				sSQL = "SELECT CODCOM,DESCOM from COMUNE WHERE CODCOM like 'Z%' " &_
						"ORDER BY DESCOM"

'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
				rsComune.Open sSQL, CC
				
				%>
				<select class="textblack" ID="cmbStatoResidenza" name="cmbStatoResidenza" onchange="CambiaLabel()">
				<option></option>
				<%
				do while not rsComune.EOF 
					Response.Write "<OPTION "

					Response.write "value ='" & rsComune("CODCOM") & _
						"'> " & rsComune("DESCOM")  & "</OPTION>"
					
					rsComune.MoveNext 
				loop

				rsComune.Close	
				%>
				</select>
	        </td>
		</tr>
		
	    <tr><td>&nbsp;</td></tr>
		<!--tr>			<td class="textblack" colspan="2">				Per <b>i residenti all'estero</b>, indicare obbligatoriamente la citta' nel campo sottostante.<br>				Per <b>i residenti in Italia</b>, invece e' facoltativo indicare la frazione nel campo sottostante. 			</td>		</tr-->	
	    <tr>
			<td align="left" class="tbltext1" width="30%">
				<!--b>Frazione/Citta' Estera</b-->
			  <input type="text" name="txtLbl" value="Frazione" size="13" ReadOnly class="tbltext1" style="font-weight: bold; border: 0px;">
				
	        </td>
	        <td align="left">
				<input size="30" maxlength="50" name="txtFrazione" class="textblack" style="TEXT-TRANSFORM: uppercase;">
	        </td>
	    </tr>
	    <tr><td>&nbsp;</td></tr>
	    <tr>
			<td align="left" class="textblack" colspan="2">
				Para <b>los residentes en Argentina</b> completar los siguientes campos.
			</td>
	    </tr>
		<tr>		
			<td align="left" class="tbltext1" width="30%">
				<b>Departamento de residencia*</b>
			</td>			
			<td align="left">				
<%
			sInt = "PROV|0|" & date & "||cmbProvRes' onchange='PulisciRes()| ORDER BY DESCRIZIONE"			
			CreateCombo(sInt)	
%>
			</td>
		</tr>		
		<tr>		
			<td align="left" class="tbltext1" width="30%">
				<b>Municipio de residencia*</b>
			</td>
			<td nowrap>
				<span class="tbltext">
				<input type="text" name="txtComuneRes" style="TEXT-TRANSFORM: uppercase;" class="textblack" size="35" readonly value="<%=DescrComuneResid%>">
				<input type="hidden" id="txtComRes" name="txtComRes" value="<%=COM_RES%>">
<%
				NomeForm="frmDomIscri"
				CodiceProvincia="cmbProvRes"
				NomeComune="txtComuneRes"
				CodiceComune="txtComRes"
				Cap="txtCAP"
%>
				<a href="Javascript:SelComune('<%=NomeForm%>','<%=CodiceProvincia%>','<%=NomeComune%>','<%=CodiceComune%>','<%=Cap%>')" ID="imgPunto1" name="imgPunto1" onmouseover="javascript:window.status='';return true"><img border="0" src="<%=Session("Progetto")%>/images/bullet1.gif"></a>
				</span>				
			</td>
		</tr>		
		<tr>
			<td align="left" nowrap class="tbltext1" width="30%">
				<b>C.A.P.*</b>
			</td>
			<td align="left">
				<input type="text" name="txtCAP" class="textblack" value="<%=CAP_RES%>" size="5" maxlength="5">
			</td>
		</tr>	    
	   
	    <!--tr>			<td height="2" align="left" colspan="2" background="<%=Session("Progetto")%>/images/separazione.gif"></td>	    </tr-->
	    
	    <tr><td>&nbsp;</td></tr>
	</table>    
	   <%
	      sCommento = "Indica di seguito le informazioni relative all'ultimo titolo di studio." 
			           
	      call Titolo("Ultimo Titolo di Studio Conseguito",sCommento)
	   %> 
	    <!--tr>	        <td align="left" class="tbltext3" colspan="2">	        	<b>Ultimo Titolo di Studio Conseguito</b>			</td>				    </tr>	    <tr>			<td align="left" colspan="2" class="textblack">			Indica di seguito le informazioni relative all'ultimo titolo di studio.			Successivamente durante la fase di orientamento potrai			completare il tuo curriculum con tutti i titoli di studio in tuo possesso.			</td>	    </tr-->
		
	 <table width="500" border="0" cellspacing="2" cellpadding="1">
		<tr>
	        <td align="left" class="tbltext1" width="30%">
				<b>T�tulo de estudio*</b>
	        </td>
			<td align="left">
				<%
				sInt = "LSTUD|0|" & date & "| |cmbTitStud| ORDER BY VALORE"			
				CreateCombo(sInt)
				%>
	        </td>
	    </tr>
		<tr>
	        <td align="left" class="tbltext1" width="30%">
				<b>A�o de expedici�n del t�tulo</b> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(aaaa)
			</td>
			<td align="left">
				<input style="TEXT-TRANSFORM: uppercase;" class="textblack" size="11" maxlength="4" name="txtAnnoRilascio">
			</td>
	    </tr>
		
	</table>

<!-- ----------------------------------------------------------- -->	    

	<table width="500" border="0" cellspacing="2" cellpadding="1">

	    <tr><td>&nbsp;</td></tr>
	</table>    
	    <%
	    '    sCommento = "Indica di seguito il tuo livello di conoscenza nei seguenti argomenti." 
			            
        '    call Titolo("Conoscenze Lingua",sCommento)
	    %>
	 	    
	<!--table width="500" border="0" cellspacing="2" cellpadding="1">		<tr class="sfondocomm">		    <td align="center" nowrap>				<b>Argomento</b>			</td>			<td align="center">				<b>Nulla</b>			</td>			<td align="center">				<b>Suff.</b>			</td>			<td align="center">				<b>Discreta</b>			</td>			<td align="center">				<b>Buona</b>			</td>			<td align="center">				<b>Ottima</b>			</td>	    </tr>		    	    <tr class="tblsfondo">			<td align="left">				<p align="left">					<span class="tbltext">&nbsp;&nbsp;&nbsp;ITALIANA</span>				</p>	        </td>	        <td align="left">				<p align="center">					<input checked type="radio" id="optItaliano" name="optItaliano" value="N">				</p>	        </td>			<td align="left">				<p align="center">					<input type="radio" id="optItaliano" name="optItaliano" value="S">				</p>	        </td>	        <td align="left">				<p align="center">					<input type="radio" id="optItaliano" name="optItaliano" value="D">				</p>	        </td>	        <td align="left">				<p align="center">					<input type="radio" id="optItaliano" name="optItaliano" value="B">				</p>	        </td>	        <td align="left">				<p align="center">					<input type="radio" id="optItaliano" name="optItaliano" value="O">				</p>	        </td>	    </tr>		<tr></tr>  	</table>	<br--> 
<%   '+++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	      sCommento = "Indicare la  'Condizione di Appartenenza Dlgs. 181/2000'<br>" 
		  sCommento = sCommento & "Indica di seguito le informazioni relative allo status occupazionale." 	           
	      call Titolo("Status Occupazionale",sCommento)
	%> 
    <table width="500" border="0" cellspacing="2" cellpadding="1">
		<tr>
	        <td align="left" class="tbltext1" width="30%">
				<b>Condizione di Appartenenza<br>Dlgs. 181/2000*</b>
	        </td>
	       <td align="left">
				
<%			    sDisabilita =""
                sInt = "ST181|0|" & date & "|" & sCategoria & "|cbbAppartenenza' " & sDisabilita & " onchange='ControllaDisocc()|" & sCondizione			
				CreateCombo(sInt)
%>																								
			</td>
		</tr>
		<tr>
			<td align="left" class="tbltext1" width="30%">
				<b>Dal</b>&nbsp;(gg/mm/aaaa)
			</td>
			<td>
				<input type="text" id="txtDecDis" name="txtDecDis" value maxlength="10" size="10" disabled>
			</td>
		</tr>
		<!--tr><td>&nbsp;</td></tr-->
		<tr>
			 <!--td align="left" class="tbltext1" width="30%">				<b>Particolare categoria<br>di appartenenza</b>							</td>			<td><%			     '           sDisabilita ="disabled"     '           sCondizione=" AND CODICE LIKE 'N%' ORDER BY DESCRIZIONE"     '           'sCondizione="  ORDER BY DESCRIZIONE"     '           sInt = "STDIS|0|" & date & "|" & CodiceStato & "|cbbDisocc' " & sDisabilita & " width='10|" & sCondizione			 '	         CreateCombo(sInt)%>																			</td-->
			<input type="hidden" name="cbbDisocc" value="N02">
		</tr>
<%	'	Response.write session("CREATOR") & "    *******************"	
			set rsDescSede = Server.CreateObject("ADODB.Recordset")
			
			sSqlSede = "SELECT descrizione,id_sede from sede_impresa " &_ 
			           " WHERE id_SEDE='" &  session("CREATOR") & "'" 
				
'PL-SQL * T-SQL  
SSQLSEDE = TransformPLSQLToTSQL (SSQLSEDE) 
			rsDescSede.Open sSqlSede, CC
				
			If Not rsDescSede.EOF Then
%>				<tr><td>&nbsp;</td></tr>
				<tr>
					<td align="left" class="tbltext1"><b>Iscritto : </b></td>
					<td><input type="text" name="iscr" size="40" value="<%=rsDescSede("descrizione")%>" READONLY style="font-weight: bold; border: 0px;"></td>
					<input type="hidden" id="iscritto" name="iscritto" value="<%=rsDescSede("id_sede")%>">
				</tr>
<%			End If
			rsDescSede.Close
			Set rsDescSede = Nothing
%>			  
    </table>	
    <br>	    
<%	 '+++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	'     sCommento = "Indicare le Aree Professionali di interesse." 
	'	 call Titolo("Candidature",sCommento)
		 
	'	 SSql = "SELECT ID_AREAPROF,DENOMINAZIONE FROM AREE_PROFESSIONALI WHERE ID_VALID = 1 ORDER BY DENOMINAZIONE"
		    
	 '    set rsQualifica = Server.CreateObject("ADODB.Recordset")
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	  '   rsQualifica.Open sSQL, CC, 3
	     
	   '  if rsQualifica.RecordCount > 0 then
	 %>    
	 		<!--table width="500" border="0" cellspacing="2" cellpadding="1">				<tr class="sfondocomm">					<td align="center" width="90%" nowrap><b>Area Professionale</b></td>					<td align="center"><b>Seleziona</b></td>				<--/tr>			  	         <%	   		'	do while not rsQualifica.EOF              %>   	            <!--tr class="tblsfondo">					<td align="left">						<p align="left">							<span class="tbltext">&nbsp;&nbsp;&nbsp;<%'=rsQualifica("DENOMINAZIONE")%></span>						</p>					</td>					<td align="left">						<p align="center">							<input TYPE="CHECKBOX" NAME="ChkAreaProf" value="<%'=rsQualifica("ID_AREAPROF")%>">						</p>					</td>  				</tr><%	         		'		rsQualifica.MoveNext		'	loop %>					</table-->  	      
<%
	 '   else
	 '        msgetto("Aree Professionali non presenti in archivio")
	 '   end if 
  
     '   rsQualifica.close:	set rsQualifica = nothing	
%>
	<!--br--> 
 <%  	 
	'     sCommento = "Indicare il tipo di rapporto preferito." 
	'	 call Titolo("Disponibilit�",sCommento)
	'	 aTipolContratto=decodTadesToArray("TDISP",Date(),"VALORE='" & sCodTipolContratto & "'",0,"0")
	'	 if not isnull(aTipolContratto(0,0,0)) then
%>		 
		 <!--table width="500" border="0" cellspacing="2" cellpadding="1">				<tr class="sfondocomm">					<td align="center" width="90%" nowrap><b>Tipologia Contratto</b></td>					<td align="center"><b>Seleziona</b></td>				</tr-->
<%		 
		'    for i=0 to Ubound(aTipolContratto)-1
%>		    
		     <!--tr class="tblsfondo">					<td align="left">						<p align="left">							<span class="tbltext">&nbsp;&nbsp;&nbsp;<%'=aTipolContratto(i,i +1,i)%></span>						</p>					</td>					<td align="left">						<p align="center">							<input TYPE="CHECKBOX" NAME="ChkTipolContratto" value="<%'=aTipolContratto(i,i,i)%>">						</p>					</td>  			 </tr><%		'    next%>	        </table-->
<%	    
	  '   else
	  '		  msgetto("Tipologie contratto non presenti in archivio.")
	  '	 end if
	  '	 Response.Write "<br>"
		 
	  '	 aTipolDatLav=decodTadesToArray("TDISP",Date(),"VALORE='" & sCodTipolDatLav & "'",0,"0")
	  '	 if not isnull(aTipolDatLav(0,0,0)) then
		   	%>		 
			<!--table width="500" border="0" cellspacing="2" cellpadding="1">				<tr class="sfondocomm">					<td align="center" width="90%" nowrap><b>Tipologia Datore di Lavoro</b></td>					<td align="center"><b>Seleziona</b></td>				</tr-->
			<%		 	    
	'	    for i=0 to Ubound(aTipolDatLav)-1
		        %>		    
		     <!--tr class="tblsfondo">					<td align="left">						<p align="left">							<span class="tbltext">&nbsp;&nbsp;&nbsp;<%'=aTipolDatLav(i,i +1,i)%></span>						</p>					</td>					<td align="left">						<p align="center">							<input TYPE="CHECKBOX" NAME="ChkTipolDatLav" value="<%'=aTipolDatLav(i,i,i)%>">						</p>					</td>  			 </tr><%		 '   next%>	        </table-->
<%	 
	 '    else
	'		 msgetto("Tipologie datori di lavoro non presenti in archivio.")
	'	 end if
	'	erase	 aTipolContratto
	'	erase    aTipolDatLav
	 %>        
     
    <br>

	<table cellpadding="0" cellspacing="0" width="500" border="0">	
		<tr align="center">
			<td nowrap align="center"><input type="image" name="Conferma" src="<%=Session("progetto")%>/images/conferma.gif"></td value="Conferma"><td>
			<!--td nowrap align="center"><a href="javascript:document.frmDomIscri.reset()"><img border="0" src="<%=Session("progetto")%>/images/annulla.gif"></td>			<td nowrap align="left"><a HREF="javascript:history.back()"><img SRC="<%=Session("Progetto")%>/images/indietro.gif" border="0"></td-->
		    <td> 
					           	               
	            <input type="hidden" name="IniIsc" value="<%=sDataIsc%>">
	            <input type="hidden" name="datasis" value="<%=sDataSis%>">
	           
	        </td>
		</tr>
	</table>
</form>
	
<%
end sub

sub Fine()
%>
<!--#include virtual = "/include/closeconn.asp"-->
<!--#include virtual = "strutt_coda2.asp"-->
<%
end sub
%>
<!-- ************** ASP Fine *************** -->

<!-- ************** MAIN Inizio ************ -->
<%
dim sDataIsc
dim sDataSis
dim sSQL
dim sCommento

dim rsQualifica
dim aTipolContratto
dim aTipolDatLav
dim sCodTipolContratto
dim sCodTipolDatLav

sDataIsc = Now()
sDataSis = ConvDateToString(Now())

sCodTipolContratto = "10"
sCodTipolDatLav = "08"

Inizio()
ImpPagina()
Fine()
%>
<!-- ************** MAIN Fine ************ -->
