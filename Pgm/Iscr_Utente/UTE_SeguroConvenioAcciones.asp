<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include Virtual = "/strutt_testa2.asp"-->
<!--#include Virtual = "/include/OpenConn.asp"-->
<!--#include virtual = "/util/portallib.asp"-->
<!--#include Virtual = "/include/DecComun.asp"-->
<!--#include Virtual = "/include/ControlDateVB.asp"-->
<!--#include Virtual = "/include/ImpostaProgetto.asp"-->
<!--#INCLUDE Virtual = "/IncludeMtss/adovbs.asp"-->
<!--#include Virtual = "/pgm/Iscr_utente/UTE_SeguroConvenioImprime.asp"-->
<!--#include Virtual = "/include/VerificaHistoriaLaboralDatos.asp"-->

<%

	

'------------------------
' Variables usadas
'------------------------
dim obDB
dim bCondicionJefes
dim rsOperatore
dim sOpeCognome
dim sOpeNome
dim sCognome
dim sNome
dim dDataNascita
dim sCodFisc
dim	sComNasc 
dim	sPrvNasc 
dim	sStatoNasc
dim sLuogoCPI
dim rsConvenios
dim sEstadoConvenio, MAILANEXOID
dim HayDatosDeConvenio

	'set obDB=new InterfaceBD

dim Tarea
dim OficinaId
dim UsuarioId
dim NroConvenio
dim Status
dim Mensaje
	
'=============================================================================
'	Lee Datos externos (de paginas llamadoras)
'=============================================================================
' Identificador de persona analizada
sIdpers = CLng(Request("IdPers"))

' Otros datos de control de tareas
Tarea  = (Request("Tarea"))
NroConvenio  = (Request("NroConvenio"))
'..Status = (Request("Status"))
'..Mensaje = (Request("Mensaje"))

' variables de session
Oficina = session("creator")
Usuario = Session("idutente")

Idpersona=sIdpers
FlAna=0	'????

'''valido el ingreso a la pagina VANI 12/07/2006

	If ValidateService(session("idutente"),"ADHESION_SEGURO",cc) <> "true" Then 
		response.redirect "/util/Error_Handler.asp?PaginaRedireccion=/pgm/iscr_utente/UTE_ModDati.asp?Idpers=" & sIdPers
	End If
	
'''valido el ingreso a la pagina VANI 12/07/2006
	

if len(Tarea)= 0 then Tarea = "0"
if len(NroConvenio)= 0 then NroConvenio = "0"
'..if len(Status)= 0 then Status = "0"
'..if len(Mensaje)= 0 then Mensaje = ""
HayDatosDeConvenio = "0"


'=============================================================================
' Lee datos necesarios 
'=============================================================================
'-------------------------------------------------------
' Datos de la oficina de trabajo 
'-------------------------------------------------------
sLuogoCPI = ""

sSQL = "SELECT COMUNE,PRV FROM SEDE_IMPRESA WHERE ID_SEDE = " & session("creator")
'PL-SQL * T-SQL  
'SSQL = TransformPLSQLToTSQL (SSQL) 
set rsLuogoCPI = cc.execute(sSQL)
if not rsLuogoCPI.eof then
	sLuogoCPI = rsLuogoCPI("COMUNE")
	if sLuogoCPI <> "" then
		sLuogoCPI = DescrComune(sLuogoCPI) 
	end if
end if
rsLuogoCPI.close
set rsLuogoCPI= nothing

'-------------------------------------------------------
' Datos del usuario actual  de Session("idutente")
'-------------------------------------------------------

sOpeCognome = " - "
sOpeNome = " - " 

sSQL = "SELECT COGNOME,NOME FROM UTENTE WHERE IDUTENTE = " & Session("idutente")
'PL-SQL * T-SQL  
'SSQL = TransformPLSQLToTSQL (SSQL) 
set rsOperatore = cc.execute (sSQL)
if not rsOperatore.eof then
	sOpeCognome = rsOperatore("COGNOME")
	sOpeNome = rsOperatore("NOME")
end if
rsOperatore.close
set rsOperatore = nothing 

'-------------------------------------------------------
' Datos de la persona analizada de la variable sIdpers
'-------------------------------------------------------
sCognome = " - "
sNome = " - " 
dDataNascita = " - " 
sCodFisc = " - " 
sComNasc = " - " 
sPrvNasc = " - " 

sSQL = "SELECT COGNOME,NOME,DT_NASC,COM_NASC,PRV_NASC,STAT_NASC,COD_FISC FROM PERSONA WHERE ID_PERSONA = " & sIdpers
'PL-SQL * T-SQL  
'SSQL = TransformPLSQLToTSQL (SSQL) 
set rsUtente = cc.execute (sSQL)
if not rsUtente.eof then
	sCognome = rsUtente("COGNOME")
	sNome = rsUtente("NOME")
	dDataNascita = ConvDateToString(rsUtente("DT_NASC"))
	sCodFisc = rsUtente("COD_FISC")
	sComNasc = rsUtente("COM_NASC")
	sPrvNasc = rsUtente("PRV_NASC")
	sStatoNasc = rsUtente("STAT_NASC")
end if
rsUtente.close
set rsUtente = nothing

'-------------------------------------------
' busca el convenio de la persona
'-------------------------------------------
set rsconv = server.CreateObject("adodb.recordset")
'PL-SQL * T-SQL  
set rsconv = connlavoro.execute ("select NroConvenio from seguroconvenios where cuil = '" & sCodFisc & "'")
if not rsconv.EOF then 
	NroConvenio = rsconv("NroConvenio")
else
	NroConvenio = 0 
end if 
set rsconv = nothing


'==========================================================================================
'	Manejo y registro del Convenio  
'	Aplica la tarea solicitada al convenio
'==========================================================================================
Status = cint(0)
Mensaje = ""

' Primera vez --> Registra el convenio
if cint(Tarea) = cint(0) then
	MAILANEXOID = 0	
	Resultado = ConvenioActualiza(0, sIdpers, "", Oficina, Usuario, NroConvenio, MAILANEXOID, Status, Mensaje)
end if

' Tareas especificas (Imprime, Firma o desiste)
if cint(Tarea) > 0 and cint(Tarea) < cint(99) then		'.. cint(Tarea) = cint(1) or cint(Tarea) = cint(2) or cint(Tarea) = cint(3) then
	Resultado= 0 : Status= 0 : Mensaje = ""
	MAILANEXOID = 0	
	
	'..Response.Write Tarea & " " & Resultado
	'..Response.End

	' Actualiza el estado del convenio
	Resultado = ConvenioActualiza(cint(Tarea), sIdpers, "", Oficina, Usuario, NroConvenio, MAILANEXOID, Status, Mensaje)

	' si termino bien aplica opciones  particulares
	if cint(Resultado) = cint(1) then
		select case cint(Tarea)
			case 1, 92, 93, 94, 95, 96, 97	' Imprimir Convenio
				convenioEmite Nroconvenio, sFileNameDic
				
			case 2	' Convenio Firmado 
				' Actualizar el estado del convenio 5 - En proceso de alta en personas
				Resultado = ConvenioActualiza(5, sIdpers, "", Oficina, Usuario, NroConvenio, MAILANEXOID, Status, Mensaje)
				
				if cint(Resultado) = cint(1) then
					MAILANEXOID = 0
					' Da el alta en EmpleoPersonas
					
					''comentado 12/12/2006 --- para analisis de alta diferida!!
					''************************************************************************''''
					''''''''Resultado = ConvenioAltaPersona(NroConvenio, Usuario, MAILANEXOID, Status, Mensaje)
					''************************************************************************''''
					
									
					'..' Actualizar el estado del convenio 6 o 7 
					'..if cint(Status) = cint(1) then		'(6)	
					'..	Resultado = ConvenioActualiza(6, sIdpers, "", Oficina, Usuario, NroConvenio, MAILANEXOID, Status, Mensaje)
					'..else					'(7)
					'..	Resultado = ConvenioActualiza(7, sIdpers, "", Oficina, Usuario, NroConvenio, MAILANEXOID, Status, Mensaje)
					'..end if
				end if
			case else	' caso no contemplado
				'.Status= -2
				'..Mensaje= "Se registr� con �xito la desici�n de la persona"  
		end select
	else
		' mensaje de errores
	end if
	' Prepara mensajes de error	
	TareaStatus = Status
	TareaMensaje = Mensaje
end if 

' Ya hizo todas las tareas pendientes 
Tarea = cint(9999)

'======================================================
'	Arma la Pagina a mostrar por partes 
'======================================================
%>

<!--#include virtual= "/pgm/fascicolo/FAS_ImpostaFlag.asp"-->
<br><!--#include file="menu.asp"--><br>

<%
'--------------------------------------------------
' Arma encabezados y ayuda
'--------------------------------------------------
Inizio()
%>

<script language="vbscript">
'===============================================================================
'	Funciones del cliente
'===============================================================================
'-------------------------------------------------------------------------------
' Desabilita el reloj de arena
'-------------------------------------------------------------------------------
sub  DeshabilitarReloj()
	document.body.style.cursor="none"
end sub

function ConvenioControl(xTarea)
'-------------------------------------------------------------------------------
'	Controla el estado de un convenio
'	La situacion de la persona
' Necesita: Tarea a realizar
'-------------------------------------------------------------------------------
	dim Status, Mensaje, NroConvenio, I, cuil, Nrotarea, Msg
	Msg = ""
	NroConvenio = 0
	cuil = ""
	
	' arma mensaje
	select case xTarea
		case 1:		Msg = "Imprimir"	' Imprimir
		case 2:		Msg = "Firmar"		' firmar
		case 3:		Msg = "Desitir"		' desiste
		case 8, 9:  Msg = "No Interesado"
		case 10:	Msg = "Modificar Decisi�n"
		case 11:	Msg = "Rectificar Decisi�n"
		case 92, 93, 94, 95, 96, 97
			Msg = "Reimpresion de Convenio"
		case else ' ???
			Msgbox "Tarea No definida " & xTarea
			exit function
	end select
	
	if Msg <> "" then
		' Muestra mensaje y pregunta de confirmacion
		select case msgbox ("Confirma " & Msg & " el Convenio?", 4, "Convenio de Adhesi�n") 
			' Dijo Aceptar --> ejecuta la tarea solicitada
			case 6	' vbYes 
				' Seteo la tarea que quiero y Me llamo a mi mismo
				document.all.Tarea.value = xTarea ' Setea la Tarea 
				' reloj de arena 
				document.body.style.cursor="wait"
				' se llama de nuevo a si mismo
				'document.formConvenio.submit
		    case else
		end select
		
	else
		' Seteo la tarea que quiero y Me llamo a mi mismo
		document.all.Tarea.value = xTarea ' Setea la Tarea 
		' reloj de arena 
		document.body.style.cursor="wait"
		' se llama de nuevo a si mismo
		'document.formConvenio.submit
	end if
	
end function

</script>


<%

'==========================================================================================
'	Presenta datos del convenio en la pagina
'==========================================================================================
	'--------------------------------------------------------------
	' Lee los datos del convenio emitido de sp SeguroConvenioLee
	'----------------------------------------------------------------
	Set Rs = Server.CreateObject("ADODB.Recordset")
	Set cmdConvenio = Server.CreateObject("adodb.command")
	
	with cmdConvenio
		.activeconnection = connLavoro
		.Commandtext = "SeguroConvenioLee"
		.commandtype = adCmdStoredProc 
		'--------------------------------------------
		' Carga parametros
		'--------------------------------------------
		For Each Parameter In .Parameters
			If Parameter.Direction And adParamInput Then
				Select case ucase(Mid(Parameter.Name, 2))
					CASE "NROCONVENIO"
						Parameter.value =  Nroconvenio
						'Response.Write nroconvenio
					CASE ELSE
						' ????
				End Select
			End If
		Next
		'--------------------------------------------
		' Ejecuta
		'--------------------------------------------
'PL-SQL * T-SQL  
'RECORDCOUNT = TransformPLSQLToTSQL (RECORDCOUNT) 
		Set Rs = .Execute(recordcount) 
	end with
		

	'==========================================================================================
	'	Busca y presenta el estado del Convenio en la pagina
	'
	' Busca convenios por el cuil de la persona (scodfisc)
	' asume que la persona puede tener solo un convenio !!!! OJO 
	' si tiene mas de uno trae cualquiera (NO deberia pasar) 
	'==========================================================================================
	'..sEstadoConvenio = 0
	If	rs.RecordCount >0 then
		%>
		<table border="0" width="500">
		   <!-- <tr>
				<td height="2" align="left" colspan="4" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
			</tr>-->
		    <tr>
				<td height="2" align="left" colspan="4"><br></td>
			 </tr>
			 <tr>
				<td colspan="4"> <span class="tbltext1"><b>Estado del Convenio de Adhesi�n</b></span>
					<br><br></td>
			</tr>           

			<tr class="sfondocomm">
				<td colspan="2" class="tbltext1"><b>Nro Convenio</b></td>
				<td align="left" colspan="2" class="tbltext1"><%=rs("NroConvenio")%></td>
			</tr>
	
			<tr class="sfondocomm">
				<td colspan="2" class="tbltext1"><b>Estado</b></td>	
				<td align="left" colspan="2" class="tbltext1"><b><%=rs("ConvenioEstadoDescCompleto")%></b></td>
			</tr>
			
			<tr class="sfondocomm">
				<td colspan="2" class="tbltext1"><b>Fecha Emisi�n</b></td>
				<td align="left" colspan="2" class="tbltext1"><%=rs("FEchaEmision")%></td>
			</tr>
			
			<tr class="sfondocomm">
				<td colspan="2" class="tbltext1"><b>Fecha Firma</b></td>
				<td align="left" colspan="2" class="tbltext1"><%=rs("FEchaFirma")%></td>
			</tr>
		</table>
		<%
	 end if 
%>

<br>

<%
'==========================================================================================
'	Presenta datos de prerequisitos para emitir el convenio
'==========================================================================================

'-------------------------------------------------------
' Cumple con los prerequisitos del seguro ???
'-------------------------------------------------------
	' Aplica validaciones centralizadas en validaicones de Entrevistas
	TipoValidacion = 2	' Validaciones del Seguro
	TareaValidacion = 2 ' Tarea de Validacion
	
	
	Resultado = VerificaEntrevistaValidacionesAplica (Oficina, TipoValidacion, 0, sIdpers, sCodFisc, TareaValidacion, Usuario, Status, Mensaje)
	iStatus = cstr(Status)
	if Resultado = 1 then
		iStatus = "OK"
		Mensaje = "La Persona Cumple con los Requisitos del Seguro"
	end if

		%>
		<table border="0" width="500">
		    <td colspan="4"> <span class="tbltext1"><b>Requisitos del Seguro de Capacitaci�n y Empleo</b></span>
				<br><br></td>
			</tr>           
			
			<tr class="sfondocomm">
				<td align="left" colspan="2" class="tbltext1"><b>Citaci�n</b></td>
				<td align="left" colspan="2"  class="tbltext1"><%=iStatus%></td>
						
			</tr>
			<tr class="sfondocomm">
				<td align="left" colspan="2" class="tbltext1"><b>Descripci�n</b></td>	
				<td align="left" colspan="2"  class="tbltext1"><%=Mensaje%></td>
			</tr>

		    <tr>
				<td colspan="4"><br></td>
			</tr>           
		</table>
		
<%
	'--------------------------------------------
	' Devuelve parametros
	'--------------------------------------------
	IF	rs.RecordCount >0 then
		HayDatosDeConvenio = "1"
  	    sEstadoConvenio = rs("ConvenioEstado")
	
		%>
		<table border="0" width="500">
		    <tr>
				<td height="2" align="left" colspan="4" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
			 </tr>
		    <tr>
				<td height="2" align="left" colspan="4"><br></td>
			 </tr>
		    <td colspan="4"><span class="tbltext1"><b>Datos de la Persona a emitir en el Convenio de Adhesi�n</b></span>
				<br><br></td>
			</tr>           
			
			<tr class="sfondocomm">
				<td colspan="2" class="tbltext1"><b>C.I.</b></td>
				<td align="left"  colspan="2" class="tbltext1"><%=rs("CUIL")%></td>
			</tr>
			<tr  class="sfondocomm">
				<td colspan="2" class="tbltext1"><b>Apellido</b></td>
				<td align="left" colspan="2" class="tbltext1"><%=rs("Apellido")%></td>
			</tr>
			<tr  class="sfondocomm">				
				<td colspan="2" class="tbltext1"><b>Nombre</b></td>	
				<td align="left" colspan="2" class="tbltext1"><%=rs("Nombre")%></td>
			</tr>
			<tr  class="sfondocomm">				
				<td colspan="2" class="tbltext1"><b>Documento</b></td>
				<td align="left" colspan="2" class="tbltext1"><%=rs("Documento")%></td>
			</tr>
			<tr  class="sfondocomm">				
				<td colspan="2" class="tbltext1"><b>Direccion Beneficiario</b></td>
				<td align="left"  colspan="2" class="tbltext1"><%=rs("Direccion")%></td>
			</tr>
			<tr  class="sfondocomm">				
				<td colspan="2" class="tbltext1"><b>Cita Acordada</b></td>
				<td align="left"  colspan="2" class="tbltext1"><%=rs("CitaFechaHora")%></td>
			</tr>
		
		    <tr>
				<td colspan="4"><br></td>
			</tr>           
		</table>
		
		<script language="JavaScript">
			
			function ApriLinkForm2()
			{
				FrmSCyE2.submit();	
			}
			
			
		</script>
		
		<% 
		'Informacion adicional para el usuario sobre el trabajador (Andrea)
		'Salto al sitio de Jefes dependiendo del servidor donde se encuentre
	'	dim salto
		dim salto2
	'	salto = "http://" & Application(ServerName & "_PortalSitioPersonas") & "/movimientospersonas/inicio.asp?recursoid=5&entrevista=1&portal=1"
		salto2= "http://" & Application(ServerName & "_PortalSitioPersonas") & "/movimientospersonas/resultsBusquedasCuiles.asp?portal=1" 
					
		%>
		
	<!--	
		<form method="post" name="FrmSCyEx" action=<%=salto%>>
			<input type="hidden" name="usuarioid" value=<%=session("idutente")%>>
			<input type="hidden" name="nivel" value="Gecal">
		</form>	-->
		 
		<form method="post" name="FrmSCyE2" action=<%=salto2%>>
			<input type="hidden" name="usuarioid" value=<%=session("idutente")%>>
			<input type="hidden" name="cuil" value=<%=sCodFisc%>>
			<input type="hidden" name="entrevista" value="1">
		</form>	
		
		<table border="0" width="500">
			<tr>
				<td colspan="4" align="right"> <!--span class="tbltext1"><b><a href= "Javascript:ApriLinkForm2()">Informaci�n Adicional&nbsp;&nbsp;&nbsp;</a></b--></td>
			<!--	<td colspan="4" align="right"> <span class="tbltext1"><b><a href= "Javascript:ApriLinkForm3()">Realizar Movimientos&nbsp;&nbsp;&nbsp;</a></b></td> -->
			</tr>
		</table><br>
	
		<%
	 end if 

	 ' cierra objetos 
	 rs.Close
	 set rs = nothing
	 set cmdConvenio = nothing
	 
'==========================================================================================
'	Arma form de opciones para el Usuario
'========================================================================================== 
%>	
<FORM method="post" action="Ute_SeguroConvenioAcciones.asp" id=formConvenio name=formConvenio onsubmit='' language="javascript">
<table border="0" width="520">
	<tr><td>
		<!-- Botones del usuario -->  	
		<table border="0" width="520">
<%
'Response.Write tarea
'Response.Write "NroConvenio_" & nroconvenio & "<br>"
'Response.Write "CUIL_" & scodfisc & "<br>"
'Response.Write "RequisitosOK_" & resultado & "<br>" 

	Botones = ""
	Set cmdConvenio = Server.CreateObject("adodb.command")
	with cmdConvenio
		'.activeconnection = obBase.InicioBD()
		.activeconnection = connlavoro
		.Commandtext = "SeguroConveniosTareasDetermina(" & NroConvenio & ",'" & sCodFisc & "'," & Resultado & ",1,null,null)"
		.commandtype = adCmdStoredProc 
		
'PL-SQL * T-SQL  
		set rsconvenio = .execute()
	end with
	
	if not rsconvenio.eof then
		Do while not rsconvenio.EOF  
			Botones = Botones & "<tr><td colspan='3'><input type='submit' class='my' name='" & rsconvenio.fields(1) & "' value='" & rsconvenio.fields(1) & "' onclick='ConvenioControl(" & rsconvenio.fields(0) & ")'></td></tr>"
			rsconvenio.movenext
		loop
	end if 

	Response.Write Botones	

%>
			<%
			'========================================
			'	Emite los mensajes de errores de la aplicacion de las tareas
			'========================================	
			if len(TareaStatus)>0 and TareaStatus <> "1" then
			%>
			<tr><td>
				<table border="0" width="520">
					<tr>
						<td colspan="3">
							<br>
							<p class="tbltext1">
							Resultados de las Tareas Solicitadas:<br>
							<b>Status:<%=TareaStatus%> - <%=TareaMensaje%></b></p>
						</td>
					</tr>
				</table>
			</td></tr>
			<%End if%>

		</table>
		</td>
	</tr>
</table>

	<input type=hidden name="IdPers" value="<%=sIdpers%>">
	<input type="hidden" value="<%=Tarea%>" name="Tarea">
	<input type="hidden" value="<%=NroConvenio%>" name="NroConvenio">

</form>
<!--#include Virtual = "/include/CloseConn.asp"-->
<!--#include Virtual = "/strutt_coda2.asp"-->

<%

'===================================================
' Funciones del Servidor
'===================================================
'-----------------------------------------
' Arma primera parte de la pagina
'	Encabezados, ayudas
'-----------------------------------------
Sub Inizio()
%>	

	<table border="0" CELLPADDING="0" CELLSPACING="0" width="500" align="center">
		<tr height="18">
			<td class="sfondomenu" width="87%" height="18"><span class="tbltext0"><b>&nbsp;CONVENIO DE ADHESION AL SEGURO DE CAPACITACION Y EMPLEO</b></span></td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
			<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
		</tr>
	</table>
	<!-- Commento -->
	
	<table border="0" CELLPADDING="0" CELLSPACING="0" width="500" align="center">
		<tr>
			<td align="left" class="sfondocomm">
				Presionando imprimir ser� emitido el convenio de adhesi�n.<br>
				Presionando Confirmar se acepta la firma del Convenio de Adhesi�n.<br>
				Presionando Desistir se desiste de la firma del Convenio
				<a href="Javascript:Show_Help('/Pgm/help/Iscr_Utente/UTE_InsCertificazione')"><img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
			</td>
		</tr>
		<tr height="2">
			<td class="sfondocommaz" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>
	<br>
	
<%End Sub%>

<%
'=================================================
' Funciones de Manejo de Convenios
'=================================================

'-------------------------------------------------
' Actualiza el Estado del convenio
'-------------------------------------------------
private function ConvenioActualiza(Tarea, IdPersona, CUIL, OficinaID, UsuarioID, NroConvenio, MAILANEXOID, Status, Mensaje)
'------------------------------------------------
' Genera o actualiza un convenio para una persona
'
'Necesita:
'	Tarea		Tarea que quiere hacer el usuario
'				0:  Registrar Convenio base
'				1: 	Quiere Imprimir 
'				2:	Firma del convenio
'				3:	Desiste de firmar el convenio
'				5, 6 y 7 actualiza estados
'	IdPersona	ID de la persona en la Tabla Persona
'	CUIL		o Nro de CUIL que esta Persona.Cod_Fis
'	OficinaID	Oficina de trabajo
'	UsuarioID	Codigo de usuario
'	MAILANEXOID	movimientos en personas
'Devuelve:
'	NroConvenio	Nro de convenio Generado o actualizado
'	Status		Codigo de Retorno 1: OK
'	Mensaje		Mensaje de Error	
'	Funcion     1: Esta todo OK 
'				<> 1 Error
'------------------------------------------------
	dim ExistePersona, sSQLO, Direccion
	dim obBase
	'set obBase=new InterfaceBD

	ConvenioActualiza = 0	' Pone error por si se va sin control 
	if not isnumeric(NroConvenio) then
		NroConvenio = 0
	end if	
	
	' Para los casos que actualiza estados, preserva el status y mensaje
	if cint(Tarea) < cint(5) or cint(Tarea) > cint(7) then
		Mensaje = ""
		status = 0
	end if 

	'------------------------------------------------
	' 1) Busca la persona en la tabla de Persona 
	'------------------------------------------------
	ExistePersona = 0
	
	sSQLO = "SELECT ID_PERSONA, COGNOME, NOME, DT_NASC, COM_NASC, PRV_NASC,"
	sSQLO = sSQLO & " STAT_NASC, COD_FISC,"
	sSQLO = sSQLO & " STAT_CIT, STAT_CIT2, IND_RES, FRAZIONE_RES, COM_RES,"
	sSQLO = sSQLO & " PRV_RES, CAP_RES, NUM_TEL, E_MAIL, DT_TMST, DEP_NASC,"
	sSQLO = sSQLO & " LOC_NASC, DEP_RES, LOC_RES"
	sSQLO = sSQLO & " from PERSONA "
	' Busco por ID o CUIL ?
	if len(trim(IdPersona)) >0 then
		sSQLO = sSQLO & " WHERE ID_PERSONA=" & IdPersona
	else
		sSQLO = sSQLO & " WHERE Cod_Fis=" & CUIL
	end if
	
	' abre recordset	
	set rsPers = Server.CreateObject("ADODB.Recordset")
'PL-SQL * T-SQL  
'SSQLO = TransformPLSQLToTSQL (SSQLO) 
	rsPers.Open sSQLO, cc
	
	if rsPers.BOF = false and rsPers.EOF = False then
		' Encontre a la persona
		ExistePersona = 1
	else
		' NO existe la persona --> Se va
		Mensaje = "No Existe la Persona solicitada en Personas: NroInterno:" & IdPersona & " C.I. :" & CUIL
		Status = 10
		ConvenioActualiza = 10
		
		'rsPers.Close
		'set rsPers = nothing
		exit function
	end if
		
	'------------------------------------------------
	' 2) Busca documentos de la persona
	'------------------------------------------------
	sSQLO = "SELECT ID_PERSONA, COD_DOCST, ID_DOC"
	sSQLO = sSQLO & " from PERS_DOC Where ID_PERSONA= " & rsPers("id_persona")
	
	' abre recordset	
	set rsDocs = Server.CreateObject("ADODB.Recordset")
'PL-SQL * T-SQL  
'SSQLO = TransformPLSQLToTSQL (SSQLO) 
	rsDocs.Open sSQLO, CC
	if rsDocs.BOF = false and rsPers.EOF = False then
		' Encontre a la persona
		ExistePersona = 1
	else
		' NO existe la persona --> Se va
		Mensaje = "La persona no tiene Documentos en PERS_DOC: NroInterno:" & rsPers("id_persona") & " C.I. :" & CUIL
		Status = 11
		ConvenioActualiza = 11

		rsPers.Close
		set rsPers = nothing
		rsDocs.Close
		set rsDocs = nothing
		exit function
	end if
	
	'------------------------------------------------
	' Direccion para notificaciones
	'------------------------------------------------
	Direccion ="" &  trim(rsPers("Ind_Res"))  
	if len(trim(rsPers("Frazione_Res"))) > 0 then
		Direccion = Direccion & " - " & Trim(rsPers("Frazione_Res"))
	end if
	if len(trim(rsPers("CAp_Res"))) > 0 then
		Direccion = Direccion & " - " & Trim(rsPers("CAp_Res"))
	end if
	
	'=====================================================================================
	' 2) Genero o actualizo el Convenio  
	'=====================================================================================
	Set cmdConvenio = Server.CreateObject("adodb.command")
	with cmdConvenio
		'.activeconnection = obBase.InicioBD()
		.activeconnection = connlavoro
		.Commandtext = "SeguroConveniosActualiza"
		.commandtype = adCmdStoredProc 
		'--------------------------------------------
		' CArga parametros
		'--------------------------------------------
		For Each Parameter In .Parameters
			If Parameter.Direction And (adParamInput or adParamOutput) Then
				'Response.Write parameter.value & " xxx " & tarea
				Select case ucase(Mid(Parameter.Name, 2))
					case "TAREA"
						Parameter.value = Tarea	
						'Response.Write "EMPIEZA"
						'Response.Write Tarea & "<br>"
					CASE "CUIL" 
						Parameter.value = "" & trim(rsPers("Cod_Fisc"))
						'Response.Write  trim(rsPers("Cod_Fisc")) & "<br>"
					CASE "TIPODOC"
						Parameter.value = "" & trim(rsDocs("Cod_DocST"))
						'Response.Write  trim(rsDocs("Cod_DocST")) & "<br>"
					CASE "NRODOC"
						Parameter.value = "" & trim(rsDocs("Id_Doc"))
						'Response.Write  trim(rsDocs("Id_Doc")) & "<br>"
					CASE "APELLIDO"
						Parameter.value = "" & trim(rsPers("Cognome"))
						'Response.Write  trim(rsPers("Cognome")) & "<br>"
					CASE "NOMBRE"
						Parameter.value = "" & trim(rsPers("Nome"))	
						'Response.Write  trim(rsPers("Nome"))	 & "<br>"
					CASE "DIRECCION"
						Parameter.value = "" & Direccion
						'Response.Write  Direccion & "<br>"
					CASE "OFICINA"
						Parameter.value = OficinaID
						'Response.Write  OficinaID & "<br>"
					CASE "USUARIO"
						Parameter.value = UsuarioID
						'Response.Write  UsuarioID & "<br>"
						
					CASE "NROCONVENIO"
						if len(NroConvenio) = 0 then 
							NroConvenio = 0
						end if
						Parameter.value = "" & NroConvenio
						'Response.Write  NroConvenio & "<br>"
					CASE "STATUS"
						if len(Status) = 0 then 
							Status = 0
						end if
						Parameter.value = Status
					CASE "MENSAJE"
						Parameter.value = "" & Mensaje	
					CASE "MAILANEXOID"
						Parameter.value = MAILANEXOID	
						'Response.Write  MAILANEXOID & "<br>"
					CASE ELSE
						' ????
				END SELECT
			End If
				'Response.Write ucase(Mid(Parameter.Name, 2)) & " = "  & Parameter.value & "<br>" ' fabio
		Next
		' Ejecuta
'PL-SQL * T-SQL  
		.Execute 0, , adexecutenorecords
		'--------------------------------------------
		' Devuelve parametros
		'--------------------------------------------
		For Each Parameter In .Parameters
			If Parameter.Direction And (adParamOutput Or adParamReturnValue) Then
				Select case ucase(Mid(Parameter.Name, 2))
					case "STATUS"
						STATUS = trim(Parameter.value)
					case "MENSAJE"
						MENSAJE = trim(Parameter.value)
					case "NROCONVENIO"
						NROCONVENIO = trim(Parameter.value)

					case else
						' ???
				end select
			End If
		Next
	end with
	
	
	' cierra objetos usados
	rsPers.Close
	rsDocs.Close
	set rsPers = nothing
	set rsDocs = nothing
	set cmdConvenio = nothing
	'obBase.finBD()
	set obBase=nothing
	
	'--------------------------------------------
	' Salida
	'--------------------------------------------
	ConvenioActualiza = Status
'	if STATUS = 1 then
'		ConvenioActualiza = 1
'	end if
	NroConvenio = NROCONVENIO
	STATUS = STATUS
	Mensaje = Mensaje
end function

private function ConvenioAltaPersona(NroConvenio, UsuarioID, MAILANEXOID, Status, Mensaje)
'------------------------------------------------
' Genera o actualiza un convenio para una persona
'
'Necesita:
'	NroConvenio	Nro de convenio Generado o actualizado
'	UsuarioID	Codigo de usuario
'	MAILANEXOID	movimientos en personas
'Devuelve:
'	Status		Codigo de Retorno 1: OK
'	Mensaje		Mensaje de Error	
'------------------------------------------------
	dim ExistePersona, sSQLO, Direccion
	dim obBase
	'set obBase=new InterfaceBD
	
	ConvenioAltaPersona = 0

	Set cmdPersonas = Server.CreateObject("adodb.command")
	with cmdPersonas
		'.activeconnection = obBase.InicioBD()
		.activeconnection = connlavoro
		.Commandtext = "SeguroConveniosPlanAlta"
		.commandtype = adCmdStoredProc 
		'--------------------------------------------
		' CArga parametros
		'--------------------------------------------
		For Each Parameter In .Parameters
			If Parameter.Direction And adParamInput Then
				Select case ucase(Mid(Parameter.Name, 2))
					CASE "USUARIO"
						Parameter.value = UsuarioID
					CASE "NROCONVENIO"
						if len(NroConvenio) = 0 then 
							NroConvenio = 0
						end if
						Parameter.value = NroConvenio
					CASE "MAILANEXOID"
						Parameter.value = MAILANEXOID	
						
					CASE "STATUS"
						if len(Status) = 0 then 
							Status = 0
						end if
						Parameter.value = Status
					CASE "MENSAJE"
						Parameter.value = "" & Mensaje	
					CASE ELSE
						' ????
				END SELECT
			End If
		Next
		' Ejecuta
'PL-SQL * T-SQL  
		.Execute 0, , adexecutenorecords
		'--------------------------------------------
		' Devuelve parametros
		'--------------------------------------------
		For Each Parameter In .Parameters
			If Parameter.Direction And (adParamOutput Or adParamReturnValue) Then
				Select case ucase(Mid(Parameter.Name, 2))
					case "STATUS"
						STATUS = trim(Parameter.value)
					case "MENSAJE"
						MENSAJE = trim(Parameter.value)
					CASE "MAILANEXOID"
						MAILANEXOID = trim(Parameter.value)
					case else
						' ???
				end select
			End If
		Next
	end with
	
	' cierra objetos	
	set cmdPersonas = nothing
	'obBase.finBD()
	set obBase=nothing
	
	'--------------------------------------------
	' Salida
	'--------------------------------------------
	
	if STATUS = 1 then
		ConvenioAltaPersona = 1
	end if
	STATUS = STATUS
	Mensaje = Mensaje
	
end function


%>





