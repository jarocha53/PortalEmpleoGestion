<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include Virtual = "/util/portallib.asp"-->
<!--#include Virtual="/strutt_testa2.asp"-->
<!--#include virtual = "/include/SysFunction.asp"-->
<!--#include virtual = "/include/OpenConn.asp"-->
<!--#include virtual="/util/dbUtil.asp"-->
<!--#include virtual = "/include/ControlDateVB.asp"-->
<!--#include Virtual = "/include/DecCod.asp"-->
<!--#include virtual = "/include/ValInfPers.asp"-->

<%
sCognome = UCase(Request.Form("txtCognome"))
sCognome = Replace(Server.HTMLEncode(sCognome), "'", "''")

sNome = UCase(Request.Form("txtNome"))
sNome = Replace(Server.HTMLEncode(sNome), "'", "''")

sCarico = Request.Form("OpbCarico")
sIdpers = Request.Form("txtIdPers")
sSesso = UCase(Request.Form("cmbSesso"))

sNascita = Request.Form("txtNascita")
sNascita = convdatetodbs(ConvStringToDate(sNascita))

cStatNasc = Request.Form("cmbStatoNasc")
cCittadin = Request.Form("cmbCittadinanza")
cGradoParent = Request.Form("cmbGradoParent")
cStatFamil = Request.Form("cmbStatoFamil")
cLivTitolo = Request.Form("cmbLivelloTitolo")
sTmst = Request.Form("txtTMST")
FlDisc = Request.Form("OpDisc")
sIdFam= Request.Form("txtIdFam")

CC.BeginTrans


sSQL = "SELECT ID_FAMILIARI FROM PERS_FAMILIARI " & _
	   "WHERE ID_PERSONA = " & sIdpers & _
	   " AND COGNOME='" & sCognome & "' AND NOME='" & sNome & "'" & _
	   " AND DT_NASC=" & sNascita & " AND SESSO='" & sSesso & "'" & _
	   " AND ID_FAMILIARI NOT IN (" & sIdFam & ")"
	   
trovato = false	

set rsFamiliari = cc.execute(sSql)
if not rsFamiliari.EOF then
	trovato = true
end if

if trovato = false then	
	
	sSqlUp = "UPDATE PERS_FAMILIARI SET ID_PERSONA=" & sIdPers & _
			 ",COGNOME='" & sCognome & "',NOME= '" & sNome & _
			 "',SESSO='" & sSesso & "',DT_NASC=" & sNascita & _
			 ",STAT_NASC='" & cStatNasc & "',STAT_CIT='" & cCittadin & _
			 "',COD_GRPAR='" & cGradoParent & "',COD_STFAM='" & cStatFamil & _
			 "',COD_LIV_STUD = '" & cLivTitolo & "',DT_TMST=" & convdatetodb(Now()) & _
			 ",FL_CARICO='" & sCarico & "',FL_DISC='" & FlDisc & "' WHERE ID_FAMILIARI = " & sIdFam

	chiave = sIdPers & "|" & sIdFam
	Errore = EseguiNoCTrace(mid(session("progetto"),2), chiave, "PERS_FAMILIARI", session("idutente"), sIdPers, "P", "MOD", sSqlUp, 1, sTmst, CC)	
	if Errore = "0" then
		sValInf = ValInfPers(sIdpers,cc)
		Errore=sValInf
		if Errore = "0" then
			CC.CommitTrans
			
%>
			<!--include file="menu.asp"-->
			<!--table border="0" cellspacing="1" cellpadding="1" width="500">				<tr align="middle">					<td class="tbltext3">						Modifica dei dati del familiare correttamente effettuato...					</td>				</tr>			</table-->
			<form name="Refresh" method="post" action="UTE_VisImmFam.asp">
 				<input type="hidden" name="IdPers" value="<%=sIdpers%>">
 				<!--meta http-equiv="Refresh" content="2;URL=javascript:Refresh.submit()"-->
 			</form>
			<script>
				alert("Modificación correctamente efectuada");
				Refresh.submit()
			</script>
<%
		else
			CC.RollbackTrans
%>
			<!--#include file="menu.asp"-->
			<br>
			<table border="0" cellspacing="1" cellpadding="1" width="500">
				<tr align="middle">
					<td class="tbltext3">
						Modificación de datos no efectuada.
					</td>
				</tr>
				<tr align="middle">
					<td class="tbltext3">
						<%Response.Write(Errore)%> 
					</td>
				</tr>
			</table>
			<br>				
			<table border="0" cellpadding="0" cellspacing="1" width="500">
				<tr>
					<td align="middle" colspan="2" width="60%"><b>
					 <a HREF="javascript:history.back()"><img SRC="<%=Session("Progetto")%>/images/indietro.gif" border="0">
					</td>
				</tr>
			</table>			
<%
		end if
	else
		CC.RollbackTrans
%>			
		<!--#include file="menu.asp"-->
		<br>
		<table border="0" cellspacing="1" cellpadding="1" width="500">
			<tr align="middle">
				<td class="tbltext3">
					Modificación de datos no efectuada.
				</td>
			</tr>
			<tr align="middle">
				<td class="tbltext3">
					<%Response.Write (Errore)%> 
				</td>
			</tr>
		</table>
		<br>				
		<table border="0" cellpadding="0" cellspacing="1" width="500">
			<tr>
				<td align="middle" colspan="2" width="60%"><b>
					<a HREF="javascript:history.back()"><img SRC="<%=Session("Progetto")%>/images/indietro.gif" border="0">
				</td>
			</tr>
		</table>	
		
<%
	end if
else
CC.RollbackTrans
%>
	<!--#include file="menu.asp"-->
	<br>
	<table border="0" cellspacing="1" cellpadding="1" width="500">
		<tr align="middle">
			<td class="tbltext3">
				Modificación de datos no efectuada. El familiar ya existe.
			</td>
		</tr>
		<tr align="middle">
			<td class="tbltext3">
				<%Response.Write (Errore)%> 
			</td>
		</tr>
	</table>
	<br>				
	<table border="0" cellpadding="0" cellspacing="1" width="500">
		<tr>
			<td align="middle" colspan="2" width="60%"><b>
				<a HREF="javascript:history.back()"><img SRC="<%=Session("Progetto")%>/images/indietro.gif" border="0">
			</td>
		</tr>
	</table>
<%
end if	
%>

<!--#include Virtual = "/include/CloseConn.asp"-->	
<!--#include Virtual="/strutt_Coda2.asp"-->
