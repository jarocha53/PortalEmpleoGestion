<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include Virtual = "/strutt_testa2.asp"-->
<!--#include Virtual = "/include/OpenConn.asp"-->
<!--#include Virtual = "/include/ControlDateVB.asp"-->

<script LANGUAGE="Javascript">
<!--#include virtual = "/include/SelComune.js"-->

//include del file per fare i controlli sulla validit� delle date
<!--#include Virtual = "/Include/ControlDate.inc"-->

//include del file per fare i controlli sulla numericit� dei campi
<!--#include Virtual = "/Include/ControlNum.inc"-->

//include del file per fare i controlli sulle stringhe
<!--#include Virtual = "/Include/ControlString.inc"-->
<!--#include Virtual = "/Include/help.inc"-->

function ControllaDati()
{
	//-------- Controlli di OBBLIGATORIETA' --------
	//-------- Controlli FORMALI            --------
	//-------- Controlli di RELAZIONE       --------
	
	if (frmInsDatiDocPers.cmbTipoDoc.value == ""){
			alert("El campo tipo de documento es obligatorio!")
			frmInsDatiDocPers.cmbTipoDoc.focus() 
			return false
		}
		
	frmInsDatiDocPers.txtNDoc.value=TRIM(frmInsDatiDocPers.txtNDoc.value)
	
	if (frmInsDatiDocPers.txtNDoc.value == ""){
		alert("El campo n�mero de documento es obligatorio!")
		frmInsDatiDocPers.txtNDoc.focus()
		return false
		}
	
	DataRil = frmInsDatiDocPers.txtDataRil.value
	DataOdierna = frmInsDatiDocPers.txtoggi.value
	DataNasc = frmInsDatiDocPers.txtDtNasc.value

	if (DataRil == ""){
		alert("Indique la fecha de expedici�n del documento!")
		frmInsDatiDocPers.txtDataRil.focus()
		return false
	}
		
	if (!ValidateInputDate(DataRil)){
		frmInsDatiDocPers.txtDataRil.focus() 
		return false
	}	
						
	if (ValidateRangeDate(DataRil,DataOdierna)==false){
		alert("La fecha de expedici�n no puede ser mayor a la fecha actual!")
		frmInsDatiDocPers.txtDataRil.focus()
	    return false
	}
	if (ValidateRangeDate(DataRil,DataNasc)==true){
		alert("La fecha de expedici�n debe ser mayor a la fecha de nacimiento!")
		frmInsDatiDocPers.txtDataRil.focus()
	    return false
	}		

//Controllo della validit� della Data di Scadenza
	
	DataScad = frmInsDatiDocPers.txtDataScad.value
			
	if (DataScad == "") {
		alert("Indique la fecha de vencimiento del documento!")
		frmInsDatiDocPers.txtDataScad.focus()
		 return false
		}			
		
	if (!ValidateInputDate(DataScad)){
		frmInsDatiDocPers.txtDataScad.focus() 
		return false
		}
		
	//DataScadenza = frmInsDatiDocPers.txtDataScad.value
	
	if (ValidateRangeDate(DataOdierna,DataScad)==false){
		alert("La fecha de vencimiento no puede ser inferior a la fecha actual!")
		frmInsDatiDocPers.txtDataScad.focus()			
	    return false
	   }
//	}
	frmInsDatiDocPers.cmbEnteRil.value=TRIM(frmInsDatiDocPers.cmbEnteRil.value)
				   
	//Comune di Nascita e Provincia di Nascita obbligatori
	//e in alternativa con Nazione di Nascita
	frmInsDatiDocPers.txtComune.value=TRIM(frmInsDatiDocPers.txtComune.value)
	
	if ((frmInsDatiDocPers.cmbProvRil.value == "")&&
		(frmInsDatiDocPers.cmbNazione.value == "")){
			alert("Indique el lugar de expedici�n del documento!")
			frmInsDatiDocPers.cmbProvRil.focus() 
			return false
		}
		
	if ((frmInsDatiDocPers.txtComune.value == "")||
		(frmInsDatiDocPers.txtComune.value == " ")){ 
		if (frmInsDatiDocPers.cmbProvRil.value == ""){
			if (frmInsDatiDocPers.cmbNazione.value == ""){
				alert("El campo municipio y departamento de expedici�n o pa�s de expedici�n es obligatorio!")
				frmInsDatiDocPers.txtComune.focus() 
				return false
			}
		}	
	}
	if (frmInsDatiDocPers.txtComune.value != ""){ 
		if (frmInsDatiDocPers.cmbProvRil.value != ""){
			if(frmInsDatiDocPers.cmbNazione.value != ""){
				alert("Indicar solo el municipio y el departamento de expedici�n o solo el pa�s!")
				frmInsDatiDocPers.txtComune.focus() 
				return false
			}
		}	
	}
	//Se Comune di Nascita � digitato, la Provincia � obbligatoria
	if (frmInsDatiDocPers.txtComune.value != ""){
		if (frmInsDatiDocPers.cmbProvRil.value == ""){
			alert("El campo departamento de expedici�n es obligatorio!")
			frmInsDatiDocPers.cmbProvRil.focus() 
			return false
		}
	}
	//Se la Provincia di Nascita � digitata, il Comune � obbligatorio
	if ((frmInsDatiDocPers.txtComune.value == "")||
		(frmInsDatiDocPers.txtComune.value == " ")){
		if (frmInsDatiDocPers.cmbProvRil.value != ""){
			alert("El campo municipio de expedici�n es obligatorio!")
			frmInsDatiDocPers.txtComune.focus() 
			return false
		}
	}
}
		
function Pulisci()
{
	frmInsDatiDocPers.cmbNazione.selectedIndex=0;
	frmInsDatiDocPers.txtComune.value = ""
	frmInsDatiDocPers.txtComuneHid.value = ""
}		

function PulEstero()
{
  if (frmInsDatiDocPers.cmbNazione.value!="") {
	frmInsDatiDocPers.cmbProvRil.selectedIndex=0;
	frmInsDatiDocPers.txtComune.value = "";
	frmInsDatiDocPers.txtComuneHid.value = ""
	}
}		

</script>

<!--#include Virtual = "/include/DecCod.asp"-->
<!--#include Virtual="/util/dbUtil.asp"-->

<%
sIdpers = Request.Form("TXT_Idpers")


sSQL = "SELECT DT_NASC FROM PERSONA WHERE ID_PERSONA=" & sIdpers
 'Response.Write sSQL 
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
set rs = CC.Execute(sSQL)

if not rs.EOF then
	dtNasc = convdatetostring(rs("DT_NASC"))
end if
rs.close
set rs = nothing
%>
<br>	
	
<!--#include file="menu.asp"-->

<br>

<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
	<tr height="18">
		<td class="sfondomenu" width="67%" height="18"><span class="tbltext0"><b>&nbsp;DOCUMENTACION PERSONAL</b></span></td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
		<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">(*) campo obligatorio</td>
	</tr>
</table>
	<!-- Commento -->
<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
	<tr>
		<td align="left" class="sfondocomm">
			Ingreso del documento personal. 
			<br>Complete el campo y presione <b>Enviar</b> para guardar la modificaci�n.
			<a href="Javascript:Show_Help('/Pgm/help/Iscr_Utente/UTE_InsDocPers')">
			<img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
		</td>
	</tr>
	<tr height="2">
		<td class="sfondocomm" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif">
		</td>
	</tr>
</table> 
		  			
<form method="post" name="frmInsDatiDocPers" onsubmit="return ControllaDati(this)" action="UTE_CnfInsDocPers.asp">
<input type="hidden" id="TXT_Idpers" name="TXT_Idpers" value="<%=sIdpers%>">
<input type="hidden" name="txtDtNasc" value="<%=dtNasc%>">

<br>		
<table border="0" cellpadding="0" cellspacing="1" width="500">
	<tr>
		<td align="middle" colspan="2" nowrap class="tbltext1" width="120">
			<p align="left">
					<strong>&nbsp;</strong>
					<strong>Tipo Documento*</strong>
					<strong>&nbsp;</strong>
			</p>
		</td>
		<td align="left" colspan="2" width="60%" width="400">
<%
			sInt = "DOCST|0|" & date & "|" & "|cmbTipoDoc|ORDER BY DESCRIZIONE"			
			CreateCombo(sInt)				
%>
		</td>
	</tr>
	<tr>
		<td align="middle" colspan="2" nowrap class="tbltext1" width="120">
			<p align="left">
				<strong>&nbsp;</strong>
				<strong>N� Documento*</strong>
				<strong>&nbsp;</strong>
			</p>
		</td>
		<td align="left" colspan="2" width="60%" width="400">
			<span class="tbltext">
				<input class="textblacka" style="TEXT-TRANSFORM: uppercase" maxlength="20" type="text" name="txtNDoc" size="25">
			</span>
		</td>
	</tr>
	<tr>     
		<td align="middle" colspan="2" nowrap class="tbltext1" width="120">
			<p align="left">
				<strong>&nbsp;</strong>
				<strong>Fecha de expedici�n*</strong><font size="1"> &nbsp;&nbsp;(gg/mm/aaaa)</font>
				<strong>&nbsp;</strong>
			</p>
		</td>	
		<td align="left" colspan="2" width="60%" width="400">
			<input type="text" style="TEXT-TRANSFORM: uppercase" maxlength="10" name="txtDataRil" size="10" class="textblacka">
			<input type="hidden" name="txtoggi" value="<%=ConvDateToString(Date())%>">
		</td>
	</tr>
	<tr>     
		<td align="middle" colspan="2" nowrap class="tbltext1" width="120">
			<p align="left">
				<strong>&nbsp;</strong>
				<strong>Valido hasta*</strong><font size="1"> &nbsp;&nbsp;(gg/mm/aaaa)</font>
				<strong>&nbsp;</strong>
			</p>
		</td>	
		<td align="left" colspan="2" width="60%" width="400">
			<input type="text" style="TEXT-TRANSFORM: uppercase" maxlength="10" name="txtDataScad" size="10" class="textblacka">
		</td>
	</tr>
	<tr>
		 <td colspan="4">
				 &nbsp;
		 </td>
	</tr>		    
	<tr>
		<td height="2" align="left" colspan="4" background="<%=Session("Progetto")%>/images/separazione.gif">
	</tr>
	 <tr>
	 	 <td colspan="4">
				 &nbsp;
		 </td>
	</tr>
	<tr>
	 	 <td colspan="4">
			<p align="left">
				<span class="textblack">
					<strong>&nbsp;</strong>
					<strong>Expedido por:</strong>
					<strong>&nbsp;</strong>
					<br>
					<br>
				</span>
			</p>
		 </td>
	</tr>			 		    		    
	<tr>
	    <td align="middle" colspan="2" nowrap class="tbltext1" width="120">
			<p align="left">
				<strong>&nbsp;</strong>
				<strong>Ente</strong>
				<strong>&nbsp;</strong>
			</p>
		</td>
	    <td align="left" colspan="2" width="60%" width="400">
	    <!--input class="textblacka" type="text" name="idente" maxlength="50" style="TEXT-TRANSFORM: uppercase;">			<span class="tbltext">			</span-->
        <%
					sInt = "ENRIL|0|" & date & "|" & "|cmbEnteRil|ORDER BY DESCRIZIONE"			
					CreateCombo(sInt)				
		%>	
        
        </td>
	</tr>
	<tr>
		<td colspan="4">
			<span class="textblack"><br>
				&nbsp;&nbsp;Indique a continuaci�n el lugar de expedici�n del documento.<br>
				&nbsp;&nbsp;<br>
				&nbsp;&nbsp;Si fue expedido por la <b>Autoridad Nacional</b> indicar:<br><br>
			</span>
		</td>
	</tr>
	<tr>
		<td align="left" colspan="2" class="tbltext1" width="120">
			<p align="left">
				<strong>&nbsp;</strong>
				<strong>Departamento </strong>
				<strong>&nbsp;</strong>
			</p>
		</td>
		<td align="left" colspan="2" width="60%" width="400" nowrap>
			<span class="tbltext">
<%
					sInt = "PROV|0|" & date & "|" & "|cmbProvRil' onchange='javascript:Pulisci()|ORDER BY DESCRIZIONE"			
					CreateCombo(sInt)				
%>	
			</span>
		</td>
	</tr>
	<tr>
		<td align="middle" colspan="2" nowrap class="tbltext1" width="120">
			<p align="left">
				<strong>&nbsp;</strong>
				<strong>Municipio </strong>
				<strong>&nbsp;</strong>
			</p>
		</td>
		<td align="left" colspan="2" width="60%" width="400">
			<span class="tbltext">
				<%if  DescrComuneResid = "0" then %>
					<input type="text" name="txtComune" style="TEXT-TRANSFORM: uppercase;" class="textblacka" size="35" readonly value="<%=""%>">
				<%else%>
					<input type="text" name="txtComune" style="TEXT-TRANSFORM: uppercase;" class="textblacka" size="35" readonly value="<%=DescrComuneResid%>">
				<%end if%>
				<input type="hidden" id="txtComuneHid" name="txtComuneHid" value="<%=CodiceComune%>">

<%
					NomeForm="frmInsDatiDocPers"
					CodiceProvincia="cmbProvRil"
					NomeComune="txtComune"
					CodiceComune="txtComuneHid"
					Cap="NO"
%>
			<a href="Javascript:SelComune('<%=NomeForm%>','<%=CodiceProvincia%>','<%=NomeComune%>','<%=CodiceComune%>','<%=Cap%>')" ID="imgPunto1" name="imgPunto1" onmouseover="javascript:window.status='';return true"><img border="0" src="<%=Session("Progetto")%>/images/bullet1.gif"></a>
			</span>					
		</td>
	</tr>
	<tr>
		<td>
			&nbsp;
		</td>
	</tr>			    
	<tr>
		<td colspan="4">
			<span class="textblack">
				&nbsp;&nbsp;o bien:
			</span>
		</td>
	</tr>
	<tr>	
		<td>
			&nbsp;
		</td>
	</tr>
	<tr>
		<td align="left" colspan="2" class="tbltext1" width="120">
			<p align="left">
				<strong>&nbsp;</strong>
				<strong>Pa�s</strong>
				<strong>&nbsp;</strong>
			</p>
		</td>   
		<td align="left" colspan="2" width="60%" width="400">
				<span class="tbltext" nowrap>
<%
					sInt = "STATO|0|" & date & "|" & "|cmbNazione' onchange='javascript:PulEstero()|AND CODICE <> 'XX' ORDER BY DESCRIZIONE"
					CreateCombo(sInt)				
%>	
				</span>
		</td>
	</tr>
	<tr>
		<td colspan="4">
			&nbsp;
		</td>
	</tr>
	<tr>
		<td colspan="4">
			&nbsp;
		</td>
	</tr>		    					
</table>				    
<table border="0" cellpadding="1" cellspacing="1" width="500">
	<tr>
		<td align="middle" colspan="2">
			<a href="javascript:history.back()"><img src="<%=Session("progetto")%>/images/Indietro.gif" border="0"></a>
				<input type="image" src="<%=Session("Progetto")%>/images/conferma.gif" border="0" value="Inserisci" onclick="return ControllaDati()" id="image1" name="image1">
		</td>
	</tr>
</table>
</form>
<!--#include virtual = "/include/CloseConn.asp"-->
<!--#include Virtual = "/strutt_coda2.asp"-->

