<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include Virtual = "/strutt_testa2.asp"-->
<!--#include Virtual = "/include/OpenConn.asp"-->
<!--#include virtual = "/include/ControlDateVb.asp"-->
<!--#include virtual = "/util/dbutil.asp"-->
<!--#include virtual = "/include/DecComun.asp"-->
<!--#include Virtual = "include/DecCod.asp"-->

<script LANGUAGE="Javascript">

//include del file per fare i controlli sulla validit� delle date
<!--#include Virtual = "/Include/ControlDate.inc"-->

//include del file per fare i controlli sulla numericit� dei campi
<!--#include Virtual = "/Include/ControlNum.inc"-->

//include del file per fare i controlli sulla validit� delle date
<!--#include Virtual = "/Include/ControlCodFisc.inc"-->

//include del file per fare i controlli sulla stringa
<!--#include Virtual = "/Include/ControlString.inc"-->
<!--#include Virtual = "/Include/help.inc"-->



//Controllo Validit� dei campi	
	function ControllaDati(){

		//Data di Inizio Assistenza
		frmAssPers.txtDtIni.value = TRIM(frmAssPers.txtDtIni.value)
		if (document.frmAssPers.txtDtIni.value == ""){
			alert("El campo fecha de inicio de asistencia es obligatorio!")
			document.frmAssPers.txtDtIni.focus() 
			return false
		}
		
		//Controllo della validit� della Data di Inizio Assistenza
		DataInizio = document.frmAssPers.txtDtIni.value
		if (!ValidateInputDate(DataInizio)){
			document.frmAssPers.txtDtIni.focus() 
			return false
		}

		DataOggi = document.frmAssPers.txtoggi.value
		//Controllo Inizio Assistenza < Fine Assistenza
		if (ValidateRangeDate(DataInizio,DataOggi)==false){
			alert("La fecha de inicio de asistencia debe ser anterior a la fecha actual!")
			frmAssPers.txtDtIni.focus()
			return false
		}					
		
		DataNascita = document.frmAssPers.txtDtNasc.value
		if (ValidateRangeDate(DataInizio,DataNascita)==true){
			alert("La fecha de inicio de asistencia debe ser posterior a la fecha de nacimiento!")
			frmAssPers.txtDtIni.focus()
		   return false
		}		
		
		//Controllo della validit� della Data di Fine Assistenza
		frmAssPers.txtDtFin.value = TRIM(frmAssPers.txtDtFin.value)
		if (document.frmAssPers.txtDtFin.value != ""){
			DataFine = document.frmAssPers.txtDtFin.value
			if (!ValidateInputDate(DataFine)){
				document.frmAssPers.txtDtFin.focus() 
				return false
			}	
		}
		
		//Codice Ente Assistenza
		if(document.frmAssPers.cmbEnteAss.value == ""){
			alert("El campo ente de asistencia es obligatorio")
			document.frmAssPers.cmbEnteAss.focus() 
			return false
		}
		
		//Controllo Inizio Assistenza < Fine Assistenza
		if (document.frmAssPers.txtDtFin.value != ""){
			if (ValidateRangeDate(DataFine,DataInizio)==true){
				alert("La fecha de fin de asistencia debe ser superior a la fecha de inicio de asistencia!")
				frmAssPers.txtDtFin.focus()
				return false
			}
			
			if (ValidateRangeDate(DataFine,DataOggi)==false){
				alert("La fecha de fin de asistencia debe ser anterior a la fecha actual!")
				frmAssPers.txtDtFin.focus()
				return false
			}					
		}
		
	return true

}

</script>

<%

Dim idPers

Dim Dt_Ini_Ass

Dim sSql

Dim rsAssPers


sIdPers = Request("Idpers")

Dt_Ini_Ass = Request("AnnoRif")



sqlDtNasc = "SELECT DT_NASC FROM PERSONA WHERE ID_PERSONA = " & sIdPers

'PL-SQL * T-SQL  
SQLDTNASC = TransformPLSQLToTSQL (SQLDTNASC) 
Set rsDtNasc = CC.Execute(sqlDtNasc)


if Dt_Ini_Ass <> "" then

	sSql = "SELECT ID_PERSONA, DT_INI_ASS, DT_FIN_ASS, COD_ENTE_ASS, DT_TMST " &_
			"FROM PERS_ASSDIS WHERE ID_PERSONA = " & sIdPers & " " &_
			"AND DT_INI_ASS = " & ConvDateToDbS(Dt_Ini_Ass) & " "

	'Response.Write sSql
	'Response.Write "<br>"
	'Response.End

'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	Set rsAssPers = CC.Execute(sSql)

end if


'Se CONDIZIONE non � vuoto allora si tratta di 
'una modifica: ricarico i dati dentro le INPUT TYPE;
'altrimenti � un INSERIMENTO
'---------------------------------------------------

%>
<!--<table border="0" width="525" cellspacing="0" cellpadding="0" height="70">	<tr>		<td width="530" background="<%=Session("Progetto")%>/images/titoli/cercolavoro2b.gif" height="70" align="right">			<br>			<br>			<table border="0" width="260" height="20" cellspacing="0" cellpadding="0">				<tr>					<td width="100%" valign="baseline" align="right"><span class="tbltext1a"><b>Assistenza Disabili&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></span></td>				</tr>			</table>		</td>	</tr></table>-->
<br>

<!--#include file="menu.asp"-->

<br>
<br>	
<table cellpadding="0" cellspacing="0" width="488" border="0">
	<tr height="18">
		<td class="sfondomenu" height="18" width="45%">
			<span class="tbltext0"><b>&nbsp;ASSISTENZA DISABILI</b></span></td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
		<td valign="middle" align="right" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1">(*) campi obbligatori</td>
	</tr>
	<tr height="2">
		<td width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif">	</td>     
	</tr>
	<tr width="371" class="SFONDOCOMM">
		<td colspan="3">
			<%
			'MODIFICA
			if Dt_Ini_Ass <> "" then
				%>		
				Modificaci�n de asistencia al discapacitado. <br>
				Complete los siguientes campos y presione <b>Enviar</b> para guardar la modificaci�n.
				<%
			else
				%>
				Ingresar nueva asistencia al discapacitado. <br>
				Complete los siguientes campos y presione <b>Enviar</b> para confirmar el ingreso.			
				<%
			end if
			%>
			<a href="Javascript:Show_Help('/Pgm/help/Iscr_Utente/UTE_ModAssPers')" name="prov3" onmouseover="javascript:window.status='' ; return true"><img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
		</td>
	</tr>
	<tr height="2">
		<td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif">
		</td>
	</tr>
</table>
<form method="post" name="frmAssPers" onsubmit="return ControllaDati(this)" action="UTE_CnfAssPers.asp">
	<input type="hidden" id="text1" name="txtIdPers" value="<%=sIdPers%>">
	<input type="hidden" id="text1" name="txtDtNasc" value="<%=ConvDateToString(rsDtNasc("DT_NASC"))%>">
	
	<%
	'MODIFICA
	if Dt_Ini_Ass <> "" then
		%>
		<input type="hidden" id="text2" size="50" name="txtFlag" value="Mod">
		<input type="hidden" id="text2" size="50" name="txtTMST" value="<%=rsAssPers("DT_TMST")%>">
		<%
	'INSERIMENTO
	else
		%>
		<input type="hidden" id="text2" size="50" name="txtFlag" value="Ins">
		<%
	end if
	%>	
	
	<input type="hidden" name="txtoggi" value="<%=ConvDateToString(Date())%>">
	<table border="0" cellpadding="0" cellspacing="1" width="500">
		<tr>
			<td align="middle" colspan="2" nowrap class="tbltext1">
				<p align="left">
				<strong>&nbsp;</strong>
				<strong>&nbsp;Fecha inicio de Asistencia*</strong> (gg/mm/aaaa)
				<strong>&nbsp;</strong>
				</p>
			</td>
			<td align="left" colspan="2" width="60%">
				<%
				'MODIFICA
				if Dt_Ini_Ass <> "" then
					%>
					<input type="hidden" readonly style="TEXT-TRANSFORM: uppercase;" class="textblacka" maxlength="10" name="txtDtIni" value="<%=ConvDateToString(rsAssPers("DT_INI_ASS"))%>" size="12">
					<span class="textblack"><b><%=ConvDateToString(rsAssPers("DT_INI_ASS"))%><b></span>
					<%
				'INSERIMENTO
				else
					%>
					<input type="text" style="TEXT-TRANSFORM: uppercase;" class="textblacka" maxlength="10" name="txtDtIni" value size="12">						
					<%
				end if
				%>				
			</td>
		</tr>
		<tr height="9">
			</td>&nbsp;</td>
		</tr>
		<tr>
		  <td align="middle" colspan="2" nowrap class="tbltext1">
		  	<p align="left">
		  		<strong>&nbsp;</strong>
		  		<strong>&nbsp;Fecha fin de Asistencia</strong> (gg/mm/aaaa)
		  		<strong>&nbsp;</strong>
		  	</p>
		  </td>
		  <td align="left" colspan="2" width="60%">
				<%
				
				Dim DtFin
				
				'MODIFICA
				if Dt_Ini_Ass <> "" then
					if rsAssPers("DT_FIN_ASS") <> "" or rsAssPers("DT_FIN_ASS") <> "//" then
						DtFin = ConvDateToString(rsAssPers("DT_FIN_ASS"))
					else
						DtFin = ""
					end if
				
					%>
					<input type="text" style="TEXT-TRANSFORM: uppercase;" class="textblack" maxlength="10" name="txtDtFin" value="<%=DtFin%>" size="12">						
					<%
				'INSERIMENTO
				else
					%>
					<input type="text" style="TEXT-TRANSFORM: uppercase;" class="textblacka" maxlength="10" name="txtDtFin" value size="12">						
					<%
				end if
				%>			  	
		  </td>
		</tr>
		<tr height="9">
			</td>&nbsp;</td>
		</tr>
		<tr>
			<td align="left" colspan="2" class="tbltext1">
				<p align="left">
				<strong>&nbsp;</strong>
				<strong>&nbsp;Ente de Asistencia*</strong>
				<strong>&nbsp;</strong>
				</p>
			</td>
		    <td align="left" colspan="2" width="60%">
				<span class="tbltext">
					
				<%
					
				dim sInt
							
				if Dt_Ini_Ass <> "" then 
													
					sInt = "ENASS|0|" & date & "|" & rsAssPers("COD_ENTE_ASS") & "|cmbEnteAss|ORDER BY DESCRIZIONE"			
											
				else
					
					sInt = "ENASS|0|" & date & "|" & "|cmbEnteAss|ORDER BY DESCRIZIONE"			
					
				end if
					
				CreateCombo(sInt)
					
				%>	

				</span>
			</td>
		</tr>		    		    		
	</table>
	<br>
	<br>
	<table border="0" cellpadding="1" cellspacing="1" width="370" align="center">
		<tr>
		    <td align="middle" colspan="2">
		    <input type="image" name="Invia" src="<%=Session("Progetto")%>/images/conferma.gif" border="0" value="Conferma" onclick="return ControllaDati()"> 
		    </td>
		 </tr>
	</table>
</form>

<%

if Dt_Ini_Ass <> "" then

	rsAssPers.close

	Set rsAssPers = Nothing

end if

%>

<!--#include Virtual ="/Include/closeconn.asp"-->
<!--#include Virtual="/strutt_coda2.asp"-->
