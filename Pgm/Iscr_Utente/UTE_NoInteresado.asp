<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%

private function ConvenioActualiza(Tarea, IdPersona, CUIL, OficinaID, UsuarioID, NroConvenio, MAILANEXOID, Status, Mensaje)
'------------------------------------------------
' Genera o actualiza un convenio para una persona
'
'Necesita:
'	Tarea		Tarea que quiere hacer el usuario
'				0:  Registrar Convenio base
'				1: 	Quiere Imprimir 
'				2:	Firma del convenio
'				3:	Desiste de firmar el convenio
'				5, 6 y 7 actualiza estados
'	IdPersona	ID de la persona en la Tabla Persona
'	CUIL		o Nro de CUIL que esta Persona.Cod_Fis
'	OficinaID	Oficina de trabajo
'	UsuarioID	Codigo de usuario
'	MAILANEXOID	movimientos en personas
'Devuelve:
'	NroConvenio	Nro de convenio Generado o actualizado
'	Status		Codigo de Retorno 1: OK
'	Mensaje		Mensaje de Error	
'	Funcion     1: Esta todo OK 
'				<> 1 Error
'------------------------------------------------
	dim ExistePersona, sSQLO, Direccion
	dim obBase
	'set obBase=new InterfaceBD

	ConvenioActualiza = 0	' Pone error por si se va sin control 
	if not isnumeric(NroConvenio) then
		NroConvenio = 0
	end if	
	
	' Para los casos que actualiza estados, preserva el status y mensaje
	if cint(Tarea) < cint(5) then
		Mensaje = ""
		status = 0
	end if 

	'------------------------------------------------
	' 1) Busca la persona en la tabla de Persona 
	'------------------------------------------------
	ExistePersona = 0
	
	sSQLO = "SELECT ID_PERSONA, COGNOME, NOME, DT_NASC, COM_NASC, PRV_NASC,"
	sSQLO = sSQLO & " STAT_NASC, COD_FISC,"
	sSQLO = sSQLO & " STAT_CIT, STAT_CIT2, IND_RES, FRAZIONE_RES, COM_RES,"
	sSQLO = sSQLO & " PRV_RES, CAP_RES, NUM_TEL, E_MAIL, DT_TMST, DEP_NASC,"
	sSQLO = sSQLO & " LOC_NASC, DEP_RES, LOC_RES"
	sSQLO = sSQLO & " from PERSONA "
	' Busco por ID o CUIL ?
	if len(trim(IdPersona)) >0 then
		sSQLO = sSQLO & " WHERE ID_PERSONA=" & IdPersona
	else
		sSQLO = sSQLO & " WHERE Cod_Fis=" & CUIL
	end if
	
	' abre recordset	
	set rsPers = Server.CreateObject("ADODB.Recordset")
'PL-SQL * T-SQL  
SSQLO = TransformPLSQLToTSQL (SSQLO) 
	rsPers.Open sSQLO, cc
	
	if rsPers.BOF = false and rsPers.EOF = False then
		' Encontre a la persona
		ExistePersona = 1
	else
		' NO existe la persona --> Se va
		Mensaje = "No Existe la Persona solicitada en Personas: NroInterno:" & IdPersona & " CUIL :" & CUIL
		Status = 10
		ConvenioActualiza = 10
		
		'rsPers.Close
		'set rsPers = nothing
		exit function
	end if
		
	'------------------------------------------------
	' 2) Busca documentos de la persona
	'------------------------------------------------
	sSQLO = "SELECT ID_PERSONA, COD_DOCST, ID_DOC"
	sSQLO = sSQLO & " from PERS_DOC|-) Where ID_PERSONA= " & rsPers("id_persona")
	
	' abre recordset	
	set rsDocs = Server.CreateObject("ADODB.Recordset")
'PL-SQL * T-SQL  
SSQLO = TransformPLSQLToTSQL (SSQLO) 
	rsDocs.Open sSQLO, CC
	if rsDocs.BOF = false and rsPers.EOF = False then
		' Encontre a la persona
		ExistePersona = 1
	else
		' NO existe la persona --> Se va
		Mensaje = "La persona no tiene Documentos en PERS_DOC: NroInterno:" & rsPers("id_persona") & " CUIL :" & CUIL
		Status = 11
		ConvenioActualiza = 11

		rsPers.Close
		set rsPers = nothing
		rsDocs.Close
		set rsDocs = nothing
		exit function
	end if
	
	'------------------------------------------------
	' Direccion para notificaciones
	'------------------------------------------------
	Direccion ="" &  trim(rsPers("Ind_Res"))  
	if len(trim(rsPers("Frazione_Res"))) > 0 then
		Direccion = Direccion & " - " & Trim(rsPers("Frazione_Res"))
	end if
	if len(trim(rsPers("CAp_Res"))) > 0 then
		Direccion = Direccion & " - " & Trim(rsPers("CAp_Res"))
	end if
	
	'=====================================================================================
	' 2) Genero o actualizo el Convenio  
	'=====================================================================================
	Set cmdConvenio = Server.CreateObject("adodb.command")
	with cmdConvenio
		'.activeconnection = obBase.InicioBD()
		.activeconnection = connlavoro
		.Commandtext = "SeguroConveniosActualiza"
		.commandtype = adCmdStoredProc 
		'--------------------------------------------
		' CArga parametros
		'--------------------------------------------
		For Each Parameter In .Parameters
			If Parameter.Direction And (adParamInput or adParamOutput) Then
				'Response.Write parameter.value & " xxx " & tarea
				Select case ucase(Mid(Parameter.Name, 2))
					case "TAREA"
						Parameter.value = Tarea	
					CASE "CUIL" 
						Parameter.value = "" & trim(rsPers("Cod_Fisc"))
					CASE "TIPODOC"
						Parameter.value = "" & trim(rsDocs("Cod_DocST"))
					CASE "NRODOC"
						Parameter.value = "" & trim(rsDocs("Id_Doc"))
					CASE "APELLIDO"
						Parameter.value = "" & trim(rsPers("Cognome"))
					CASE "NOMBRE"
						Parameter.value = "" & trim(rsPers("Nome"))	
					CASE "DIRECCION"
						Parameter.value = "" & Direccion
					CASE "OFICINA"
						Parameter.value = OficinaID
					CASE "USUARIO"
						Parameter.value = UsuarioID
						
					CASE "NROCONVENIO"
						if len(NroConvenio) = 0 then 
							NroConvenio = 0
						end if
						Parameter.value = "" & NroConvenio
					CASE "STATUS"
						if len(Status) = 0 then 
							Status = 0
						end if
						Parameter.value = Status
					CASE "MENSAJE"
						Parameter.value = "" & Mensaje	
					CASE "MAILANEXOID"
						Parameter.value = MAILANEXOID	
					CASE ELSE
						' ????
				END SELECT
			End If
				'Response.Write ucase(Mid(Parameter.Name, 2)) & " = "  & Parameter.value & "<br>" ' fabio
		Next
		' Ejecuta
'PL-SQL * T-SQL  
		.Execute 0, , adexecutenorecords
		'--------------------------------------------
		' Devuelve parametros
		'--------------------------------------------
		For Each Parameter In .Parameters
			If Parameter.Direction And (adParamOutput Or adParamReturnValue) Then
				Select case ucase(Mid(Parameter.Name, 2))
					case "STATUS"
						STATUS = trim(Parameter.value)
					case "MENSAJE"
						MENSAJE = trim(Parameter.value)
					case "NROCONVENIO"
						NROCONVENIO = trim(Parameter.value)
					case else
						' ???
				end select
			End If
		Next
	end with
	
	' cierra objetos usados
	rsPers.Close
	rsDocs.Close
	set rsPers = nothing
	set rsDocs = nothing
	set cmdConvenio = nothing
	'obBase.finBD()
	set obBase=nothing
	
	'--------------------------------------------
	' Salida
	'--------------------------------------------
	ConvenioActualiza = Status
'	if STATUS = 1 then
'		ConvenioActualiza = 1
'	end if
	NroConvenio = NROCONVENIO
	STATUS = STATUS
	Mensaje = Mensaje
end function

%>
