<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include Virtual = "/strutt_testa2.asp"-->
<!--#include Virtual = "/include/OpenConn.asp"-->
<!--#include Virtual = "/include/ControlDateVB.asp"-->

<script LANGUAGE="Javascript">
//include del file per fare i controlli sulla validit� delle date
<!--#include Virtual = "/Include/ControlDate.inc"-->

//include del file per fare i controlli sulla numericit� dei campi 
<!--#include Virtual = "/Include/ControlNum.inc"-->

//include del file per fare i controlli sulle stringhe
<!--#include Virtual = "/Include/ControlString.inc"-->
<!--#include Virtual = "/Include/help.inc"-->


function ControllaDati(frmMod){
	//-------- Controlli di OBBLIGATORIETA' --------
	//-------- Controlli FORMALI            --------
	//-------- Controlli di RELAZIONE       --------
		//Cognome
		FrmInsImmFam.txtCognome.value=TRIM(FrmInsImmFam.txtCognome.value)
		if ((FrmInsImmFam.txtCognome.value == "")||
			(FrmInsImmFam.txtCognome.value == " ")){
			alert("El campo Apellidos es obligatorio!")
			FrmInsImmFam.txtCognome.focus() 
			return false
		}
		
		sCognome=ValidateInputStringWithOutNumber(FrmInsImmFam.txtCognome.value)		
		if  (sCognome==false){
			alert("Formato de Apellidos erroneo")
			FrmInsImmFam.txtCognome.focus() 
			return false
		}	
				
		//Nome
		FrmInsImmFam.txtNome.value=TRIM(FrmInsImmFam.txtNome.value)
		if ((FrmInsImmFam.txtNome.value == "")||
			(FrmInsImmFam.txtNome.value == " ")){
			alert("El campo Nombres es obligatorio!")
			FrmInsImmFam.txtNome.focus() 
			return false
		}
		
		sNome=ValidateInputStringWithOutNumber(FrmInsImmFam.txtNome.value)		
		if  (sNome==false){
			alert("Formato de Nombres erroneo")
			FrmInsImmFam.txtNome.focus() 
			return false
			
		}	
		

		//Sesso
		if (FrmInsImmFam.cmbSesso.value == ""){
			alert("El campo sexo es obligatorio!")
			FrmInsImmFam.cmbSesso.focus() 
			return false
		}
		
		//Data di Nascita
		if (FrmInsImmFam.txtNascita.value == ""){
			alert("El campo fecha de nacimiento es obligatorio!")
			FrmInsImmFam.txtNascita.focus() 
			return false
		}
		//Controllo della validit� della Data di Nascita
		DataNascita = FrmInsImmFam.txtNascita.value
		if (!ValidateInputDate(DataNascita)){
			FrmInsImmFam.txtNascita.focus() 
			return false
		}
		
		DataNascita = FrmInsImmFam.txtNascita.value
		DataOdierna=FrmInsImmFam.txtoggi.value
		if (ValidateRangeDate(DataOdierna,DataNascita)==true){
			alert("La fecha de nacimiento debe ser anterior a la actual!")
			FrmInsImmFam.txtNascita.focus()
		    return false
		}

		
		//Stato di Nascita
		if (FrmInsImmFam.cmbNazione.value == ""){
			alert("El campo pais de nacimiento es obligatorio!")
			FrmInsImmFam.cmbNazione.focus() 
			return false
		}

				
		CognomeIns = FrmInsImmFam.txtCognome.value.toUpperCase()
		NomeIns = FrmInsImmFam.txtNome.value.toUpperCase()
		DtNascIns = FrmInsImmFam.txtNascita.value.toUpperCase()
		SessoIns = FrmInsImmFam.cmbSesso.value.toUpperCase()
		
		PersCognome = FrmInsImmFam.txtPersCognome.value.toUpperCase()
		PersNome = FrmInsImmFam.txtPersNome.value.toUpperCase()
		PersDtNasc = FrmInsImmFam.txtPersDataNasc.value.toUpperCase()
		PersSesso = FrmInsImmFam.txtPersSesso.value.toUpperCase()
		
		if ((CognomeIns == PersCognome)&&(NomeIns == PersNome)&&(DtNascIns == PersDtNasc)&&(SessoIns == PersSesso))
		{
			alert("No puede reingresar al mismo familiar!")			
			return false
		}
		
		//Cittadinanza
		if (FrmInsImmFam.cmbCittadinanza.value == ""){
			alert("El campo nacionalidad es obligatorio!")
			FrmInsImmFam.cmbCittadinanza.focus() 
			return false
		}
		
		//Grado di parentela
		if (FrmInsImmFam.cmbGrado.value == ""){
			alert("El campo parentesco es obligatorio!")
			FrmInsImmFam.cmbGrado.focus() 
			return false
		}

		if (FrmInsImmFam.cmbLivello.value ==""){
		    alert("Nivel Educativo es obligatorio!")
		    FrmInsImmFam.cmbLivello.focus()
		    return false
		}
		if ((document.FrmInsImmFam.OpbCarico[0].checked == false)&&
			(document.FrmInsImmFam.OpbCarico[1].checked == false))
		{
		    alert("La declaraci�n de familiar a cargo es obligatorio!")
		    document.FrmInsImmFam.OpbCarico[0].focus()
		    return false
		}    
	}
</script>

<!--#include Virtual = "/include/DecCod.asp"-->
<!--#include Virtual="/util/dbUtil.asp"-->

<%
sIdpers = Request.Form("ID_Persona")
PERS_COGNOME = Request.Form("txtPersCognome")
PERS_NOME = Request.Form("txtPersNome")
PERS_DT_NASC = Request.Form("txtPersDataNasc")
PERS_SESSO = Request.Form("txtPersSesso")
%>

<br>
<!--#include file="menu.asp"-->
<br>

<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
  <tr height="18"> 
    <td class="sfondomenu" width="67%" height="18"><span class="tbltext0"><b>&nbsp;FAMILIARES</b></span></td>
    <td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
    <td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">(*) 
      campo obligatorio</td>
  </tr>
</table>

<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
	<tr>
		
    <td align="left" class="sfondocomm"> Ingresar los datos del familiar y presionar 
      el bot&oacute;n enviar para guardar la informaci&oacute;n.
      
   	  <!--NUEVO AGREGADO POR F.BASANTA -->
   	  <br>
  	  En este m�dulo se deber� ingresar los datos correspondientes a las personas que conforman el  
	  <b>n�cleo familiar</b>
	  <br>
      <img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0" onclick="Javascript:window.open('ayuda_Familiares.htm','Ayuda','width=500,height=350,Resize=No,Scrollbars=yes')">

      <!--
      <a href="Javascript:Show_Help('/Pgm/help/Iscr_Utente/UTE_InsImmFam')"> 
      <img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a> 
      -->
      
    </td>
	</tr>
	<tr height="2">
		<td class="sfondocomm" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif">
		</td>
	</tr>
</table>
	
	   	
<form method="post" name="FrmInsImmFam" onsubmit="return ControllaDati(this)" action="UTE_CnfInsImmFam.asp">
	<input type="hidden" name="txtPersCognome" value="<%=PERS_COGNOME%>">
	<input type="hidden" name="txtPersNome" value="<%=PERS_NOME%>">
	<input type="hidden" name="txtPersDataNasc" value="<%=PERS_DT_NASC%>">
	<input type="hidden" name="txtPersSesso" value="<%=PERS_SESSO%>">
	
	<input type="hidden" name="txtIdPers" value="<%=sIdpers%>">
	<input type="hidden" name="txtoggi" value="<%=ConvDateToString(Date())%>">

	<table border="0" cellspacing="2" cellpadding="1" width="500">
	    <tr>
			<td align="middle" colspan="2" nowrap class="tbltext1">
				<p align="left"><strong>Apellidos*</strong></p>
			</td>
			<td align="left" colspan="2" width="60%">
				<input type="text" name="txtCognome" style="TEXT-TRANSFORM: uppercase;" maxlength="50" class="textblacka" size="35">
			</td>
	    </tr>
	    <tr>
			<td align="middle" colspan="2" nowrap class="tbltext1">
				<p align="left">
				<strong>Nombres*</strong> 
				</p>
			</td>
			<td align="left" colspan="2" width="60%">
				<span class="tbltext">
					<input type="text" name="txtNome" style="TEXT-TRANSFORM: uppercase" maxlength="50" class="textblacka" size="35">					
				</span>
			</td>
	    </tr>
	    
	    <tr>
			<td align="middle" colspan="2" nowrap class="tbltext1">
				<p align="left">
				<strong>Sexo*</strong> 
				</p>
			</td>
			<td align="left" colspan="2" width="60%">
			    <select id="cmbSesso" name="cmbSesso" class="textblack">
					<option selected>
					<option value="M">MASCULINO
					<option value="F">FEMENINO
					</option>
				</select>
			</td>
		</tr>
			    
	    <tr>
			<td align="middle" colspan="2" nowrap class="tbltext1">
				<p align="left">
				<strong>Fecha de Nacimiento*</strong><font size="1"> <BR>(dd/mm/aaaa)</font> 
				</p>
			</td>	
			<td align="left" colspan="2" width="60%">
				<input type="text" name="txtNascita" style="TEXT-TRANSFORM: uppercase" maxlength="10" class="textblacka fecha" size="10">
			</td>
	    </tr>
	    
	    <tr>
	        <td align="left" colspan="2" class="tbltext1">
				<p align="left">
				<strong>Pa�s de Nacimiento*</strong> 
				</p>
	        </td>
	        <td align="left" colspan="2" width="60%">
				<span class="tbltext">
				<%
				sInt = "STATO|0|" & date & "||cmbNazione|ORDER BY DESCRIZIONE"			
				CreateCombo(sInt)				
				%>	
				</span>
	        </td>
	    </tr>
	    
		
		<tr>
	        <td align="left" colspan="2" class="tbltext1">
				 <p align="left">
				 <strong>Nacionalidad*</strong> 
				 </p>
			</td>   
	        <td align="left" colspan="2" width="60%">
				<span class="tbltext">				
				<%
				sInt = "STATO|0|" & date & "||cmbCittadinanza|ORDER BY DESCRIZIONE"			
				CreateCombo(sInt)								
				%>	
				</span>
			</td>
		</tr>
		<tr>
			<td>
				<br>
			</td>
		</tr>
		<tr>
	        <td height="2" align="left" colspan="4" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;
				
			</td>
	    </tr>
	    <tr>
			<td align="left" colspan="2" class="tbltext1">
				<p align="left"> 
				<strong>Parentesco*</strong> 
				</p>
			</td>
			<td align="left" colspan="2" width="60%">
				<p align="left">
					<%
					'INIZIO AM 02/12/04
		
					set Rs = server.CreateObject("ADODB.recordset")
		
					sSql= "select stat_cit, stat_cit2 from persona where id_persona =" & sIdpers
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
					Rs.Open sSql,CC,1

					'IF Rs("stat_cit") = "IT" or  Rs("stat_cit2") = "IT"  THEN
					'FINE AM
						sInt = "GRPAR|0|" & date & "||cmbGrado|ORDER BY DESCRIZIONE"			
					'INIZIO AM
					'ELSE 
					'	sInt = "GRPAR|0|" & date & "||cmbGrado|AND CODICE IN ('01', '02', '03')|ORDER BY DESCRIZIONE"
					'END IF 
					rs.close	
					set rs = nothing
					'FINE AM
					CreateCombo(sInt)					
					%>
				</p>
			</td>
		</tr>	
		       <td align="left" colspan="2" class="tbltext1">
				<p align="left">
				<strong>Situaci�n Laboral</strong> 
				</p>
		    </td>
		    <td align="left" colspan="2" width="60%">
				<p align="left">
					<%
					'sInt = "STFAM|0|" & date & "||cmbStatus|ORDER BY DESCRIZIONE"			
					'CreateCombo(sInt)	
					
					sInt = "STDIS|0|" & date & "||cmbStatus|ORDER BY VALORE ASC"			
					CreateCombo(sInt)							
					%>
				</p>
		    </td>
		</tr>
		<tr>
		    <td align="left" colspan="2" class="tbltext1">
				<p align="left">
				<strong>Nivel Educativo*</strong> 
				</p>
		    </td>
		    <td align="left" colspan="2" width="60%">
				<p align="left">
					<%
					'valore="NES"
					sInt = "LSFAM|0|" & date & "|" & valore & "|cmbLivello|ORDER BY substring(VALORE,1,2)"	
					'response.write "codigo.." & sInt & ".." 
					CreateCombo(sInt)
								
					%>
				</p>
		    </td>
		</tr>
	    <tr>
			<td align="middle" colspan="2" nowrap class="tbltext1">
				<p align="left">
				<strong>El familiar esta a su Cargo?*</strong> 
				</p>
			</td>
			<td align="left" colspan="2" width="60%">
				<input type="radio" id="OpbCarico" name="OpbCarico" value="S"><span CLASS="TBLTEXT">SI</span>
				<input type="radio" id="OpbCarico" name="OpbCarico" value="N" checked><span CLASS="TBLTEXT">NO</span>
			</td>
	    </tr>
	    <tr>
			<td align="middle" colspan="2" nowrap class="tbltext1">
				<p align="left">
				<strong>El familiar tiene alg�n tipo de <br>Discapacidad?*</strong> 
				</p>
			</td>
			<td align="left" colspan="2" width="60%">
				<input type="radio" id="OpDisc" name="OpDisc" value="S"><span CLASS="TBLTEXT">SI</span>
				<input type="radio" id="OpDisc" name="OpDisc" value="N" checked><span CLASS="TBLTEXT">NO</span>
			</td>
	    </tr>
	</table>

	<br>
	
	<table border="0" cellpadding="1" cellspacing="1" width="100%">
		<tr>
			<td align="center">
				<a HREF="javascript:document.FRMIDPERSONA.submit();"><img SRC="<%=Session("Progetto")%>/images/indietro.gif" border="0"></a>
				<input type="image" src="<%=Session("Progetto")%>/images/conferma.gif" border="0" value="Inserisci" onclick="return ControllaDati(this)" id="image1" name="image1">
		    </td>
		</tr>
	</table>
</form>
	<Form name="FRMIDPERSONA" method="post" action="UTE_VisImmFam.asp">
		<input type="hidden" name="IdPers" value="<%=sIdpers%>">
	</form>
<!--#include virtual = "/include/CloseConn.asp"-->
<!--#include Virtual = "/strutt_coda2.asp"-->
