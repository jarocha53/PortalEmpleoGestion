<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include Virtual = "/strutt_testa2.asp"-->
<!--#include Virtual = "/include/OpenConn.asp"-->
<!--#include virtual = "/include/ControlDateVb.asp"-->
<!--#include virtual = "/util/dbutil.asp"-->
<!--#include virtual = "/include/DecComun.asp"-->
<!--#include Virtual = "include/DecCod.asp"-->

<!--JAVASCRIPT-->			

<script language="javascript" src="/Include/ControlString.inc">
</script>
<script LANGUAGE="Javascript">

//include del file per fare i controlli sulla validit� delle date
<!--#include Virtual = "/Include/ControlDate.inc"-->

//include del file per fare i controlli sulla numericit� dei campi
<!--#include Virtual = "/Include/ControlNum.inc"-->

//include del file per fare i controlli sulla stringa
<!--#include Virtual = "/Include/ControlString.inc"-->
<!--#include Virtual = "/Include/help.inc"-->

function recarga()
{
	document.frmRecarga.cmbResDeriv.value=TRIM(document.frmDeriv.cmbResDeriv.value)
	document.frmRecarga.submit()
	//window.navigate("UTE_InsCatProt.asp?CategProt="+document.frmCatProt.cmbCatProt.value)
}


//Controllo Validit� dei campi	
function ControlarDatos(){
    
       
    if ((frmDeriv.cmbopderiv.value == "") && (frmDeriv.txtotro.value == "")) 
    {
		alert("Debe elegir una opci�n de direccionamiento o bien completar el campo otra")
		return false;
    }
    
    if ((frmDeriv.cmbopderiv.value != "") && (frmDeriv.txtotro.value != "")) 
    {
		alert("Los campos opciones de direccionamiento y otra, son alternativos, no puede completar ambos")
		return false;
    }
    
    if (frmDeriv.txtfechaderiv.value == "")
    {
		alert("El campo fecha de direccionamiento es obligatorio");
		frmDeriv.txtfechaderiv.focus();
		return false;
    }
    
	if (ValidateRangeDate(frmDeriv.txtfechaderiv.value,frmDeriv.txtfechaactual.value)==false){
		alert("La fecha de inicio de direccionamiento no debe ser mayor a la fecha actual!")
		frmDeriv.txtfechaderiv.focus()
	   return false
	}
		
	if (frmDeriv.txtfechaderiv.value == ""){
		alert("El campo fecha de direccionamiento es obligatorio!")
		frmDeriv.txtfechaderiv.focus() 
		return false;
	}

	//Controllo della validit� della Data Inizio Certificazione
	if (frmDeriv.txtfecharesult.value != "")
	{
		if (!ValidateInputDate(frmDeriv.txtfecharesult.value)){
			frmDeriv.txtfecharesult.focus() 
			return false;
		}
	}

	//Data Inizio Certificazione non successiva data odierna -- 

	if (ValidateRangeDate(frmDeriv.txtfecharesult.value, frmDeriv.txtfechaactual.value)==false){
	    //alert("DataIniCert " + DataIniCert + "DataOdierna " + DataOdierna)
		alert("La fecha de resultado de la direccionamiento no debe ser mayor a la fecha actual!")
		frmDeriv.txtfecharesult.focus()
	   return false
	}
    
	if (ValidateRangeDate(frmDeriv.txtfechaderiv.value,frmDeriv.txtfechanac.value)==true){
		alert("La fecha de direccionamiento no puede ser inferior a la fecha de nacimiento!")
		frmDeriv.txtfechaderiv.focus()
	   return false
	}

	if (frmDeriv.txtfecharesult.value != "")
	{
		if (ValidateRangeDate(frmDeriv.txtfecharesult.value,frmDeriv.txtfechanac.value)==true){
			alert("La fecha de resultado del direccionamiento no puede ser inferior a la fecha de nacimiento!")
			frmDeriv.txtfecharesult.focus()
		   return false
		}
	}
	
	if (frmDeriv.txtfecharesult.value != ""){
		//Controllo della validit� della Data Fine Certificazione
		if (!ValidateInputDate(frmDeriv.txtfecharesult.value)){
			frmDeriv.txtfecharesult.focus() 
			return false
			}
		}
	
	//controllo tra Data Inizio e Fine Certificazione
	if (frmDeriv.txtfecharesult.value != "")
	{ 
		if (frmDeriv.txtfecharesult.value != frmDeriv.txtfechaderiv.value) 
			{
			if (ValidateRangeDate(frmDeriv.txtfecharesult.value,frmDeriv.txtfechaderiv.value)==true)
			{
			alert("La fecha de resultado del direccionamiento no puede ser inferior a la fecha de direccionamiento!")
			frmDeriv.txtfecharesult.focus()
		   return false
		   
			}
		}
	}
	
	//controllo tra Data Fine Categoria Protetta e Data inizio Certificazione
	//var DataFinCatProtetta;
	//DataFinCatProtetta= document.frmCatProt.txtFinCatOld.value
	/*
	if (DataFinCatOld != "")
	{ 
		//alert("DataFinCatOld " +  DataFinCatOld);
		//alert("DataIniCat " + DataIniCat);
		//alert(ValidateRangeDate(DataFinCatOld,DataIniCat));
		if (!ValidateRangeDate(DataFinCatOld,DataIniCat))
		{
			alert("Non si pu� inserire una data di Inizio Categoria Protetta inferiore alla data di Fine Certificazione dell'occorrenza esistente!")
			frmCatProt.DataIniCat.focus()
			return false
		}	
	}
	*/	
	/*
	if (IsNum(document.frmCatProt.txtEnteCert.value))
	{
		alert("El valor no debe ser num�rico!")
		document.frmCatProt.txtEnteCert.focus() 
		return false
	}
	*/	
}

</script>

<%
dim sIdPers
dim sqlderiv
dim rsderiv
Dim sFechaDeriv	
Dim sFechaResult
Dim sDerivacion	
dim sOtro
dim sql 
dim rs

set rs = server.CreateObject("adodb.Recordset")
			
sIdPers = Request("IdPers")
sFechaDeriv = Request("FechaDeriv")
sFechaResult = Request("FechaResult")
sDerivacion = Request("Derivacion")
sOtro = Request("Otro")
sResDeriv = Request("cmbResDeriv")

sEstado = Request ("txtResDeriv")
'sPorque = Request ("txtporque")
'sObservaciones = Request ("txtObservaciones")

'if sDerivacion = "" then 
	sql = ""
	sql = "Select * from PERS_DERIVACION where ID_PERSONA = " & sIdPers & " and FECHA_DERIV = " & ConvDateToDb(sFechaDeriv) & " "
	if sDerivacion <> "" then
		sql = sql & " and COD_DERIV  = '" & sDerivacion & "'"
	else
		ValorOtro = replace(sOtro,"#"," ")
		sql = sql & " and OTRO  = '" & ValorOtro & "'"
	end if 
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
	set rs = CC.execute(sql)
	if not rs.eof then 
		'ValorOtro = rs("OTRO")
		sPorque = rs("PORQUE")
		sObservaciones = rs("OBSERVACION")
	else
		ValorOtro = ""
		sPorque = ""
		sObservaciones = ""
	end if 
'end if 
'Response.Write sql



%>
<br>
<!--#include file="menu.asp"-->
<br>
<table cellpadding="0" cellspacing="0" width="500" border="0">
	<tr height="18">
		<td class="sfondomenu" height="18" width="45%">
			<span class="tbltext0"><b>&nbsp;DIRECCIONAMIENTO</b></span></td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
		<td valign="middle" align="right" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1">(*) campo obligatorio</td>
	</tr>
	<tr height="2">
		<td width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif">	</td>     
	</tr>
	<tr width="371" class="SFONDOCOMM">
		<td colspan="3">
		Solo pueden modificarse aquellos campos que no se vean deshabilitados.
		Para modificar los datos completar los campos que se muestran editables. <br>
		Presionar <b>enviar</b> para guardar la modificaci�n. 
		<!--<%if sCondizione = "INS" then%>
			<a href="Javascript:Show_Help('/Pgm/help/Iscr_Utente/UTE_InsCatProt')" name="prov3" onmouseover="javascript:window.status='' ; return true"><img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
		<%else%>
			<a href="Javascript:Show_Help('/Pgm/help/Iscr_Utente/UTE_InsCatProt_1')" name="prov3" onmouseover="javascript:window.status='' ; return true"><img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
		<%end if%>-->
		</td>
	</tr>
	<tr height="2">
		<td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif">
		</td>
	</tr>
</table>


<br><br>

<form name="frmRecarga" method="post" action="UTE_ModDeriv.asp"  OnSubmit="return valida(this);">
	<input type="hidden" name="IdPers" value="<%=sIdPers%>">
	<input type="hidden" name="FechaDeriv" value="<%=sFechaDeriv%>">
	<input type="hidden" name="FechaResult" value="<%=sFechaResult%>">
	<input type="hidden" name="Derivacion" value="<%=sDerivacion%>">
	<input type="hidden" name="Otro" value="<%=sOtro%>">
	<input type="hidden" name="cmbResDeriv" value="">
</form>


<form name="frmDeriv" method="post" action="UTE_CnfModDeriv.asp" onsubmit="return ControlarDatos()">
	<table border="0" cellpadding="6" cellspacing="0" width="500">

<%' Opciones de derivaci�n  -------------------------------------------------------------------------- %>  

		<tr>
			<td align="left" colspan="2" nowrap="nowrap" class="tbltext1">
				<strong>&nbsp;&nbsp;Opciones de Direccionamiento</strong>
			</td>
			<td align="left" colspan="2" width="60%">
				<% 
				'if sDerivacion <> "" then
				'	DerivCombo = "DERIV|0|" & date & "|" & sDerivacion & "|cmbopderiv|ORDER BY DESCRIZIONE"			
				'else
				'	DerivCombo = "DERIV|0|" & date & "|" & "|cmbopderiv|ORDER BY DESCRIZIONE"					
				'end if 
				'CreateComboDisabled(DerivCombo)
				if rs("COD_DERIV") <> "" then 
					set rsCodDer = Server.CreateObject("ADODB.Recordset")
					sCodDerSQL = "SELECT Descrizione FROM tades WHERE nome_tabella = 'DERIV'"
					sCodDerSQL = sCodDerSQL & " AND codice = '" & rs("COD_DERIV") & "'"
'PL-SQL * T-SQL  
SCODDERSQL = TransformPLSQLToTSQL (SCODDERSQL) 
					rsCodDer.open sCodDerSQL,CC,3
					if rsCodDer("Descrizione") <> "" then
						CODDERIV = rsCodDer("Descrizione")						
					end if
					rsCodDer.Close 
					set rsCodDer = nothing
				else
					CODDERIV = ""
				end if

				%>
				<input type="text" name="cmbopderiv" style="TEXT-TRANSFORM: uppercase;" class="textblack" maxlength="400" size="50" disabled value="<%=CODDERIV%>">
				<% if sResDeriv <> "01" then %>
				<input type="hidden" name="OpDeriv"  value="<%=rs("COD_DERIV")%>">
				<% end if %>
			</td>
		</tr>

<%' Otra --------------------------------------------------------------------------------------------- %>  
		<tr>
			<td align="left" colspan="2" nowrap="nowrap" class="tbltext1">
				<strong>&nbsp;&nbsp;Otra (especificar)</strong>
			</td>
				    
			<td align="left" colspan="2" width="60%">
				<%
				'if sDerivacion = "" then 
				'	sql = ""
				'	sql = "Select * from PERS_DERIVACION where ID_PERSONA = " & sIdPers & " and FECHA_DERIV = " & ConvDateToDb(sFechaDeriv) & " and COD_DERIV is null"
				'	if sFechaResult <> "" then
				'		sql = sql & " and FECHA_RESP = " & ConvDateToDb(sFechaResult)  
				'	end if 
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
				'	set rs = CC.execute(sql)
				'	if not rs.eof then 
				'		ValorOtro = rs("OTRO")
				'	else
				'		ValorOtro = ""
				'	end if 
				'end if 
				%>
				<input type="text" name="txtotroint" style="TEXT-TRANSFORM: uppercase;" class="textblack" maxlength="400" size="50" disabled value="<%=ValorOtro%>">
				<input type="hidden" name="txtotro" style="TEXT-TRANSFORM: uppercase;" class="textblack" maxlength="400" size="50" value="<%=ValorOtro%>">
			</td>
		</tr>

<%' Fecha de Derivaci�n ------------------------------------------------------------------------------ %>  
		<tr>
			<td align="left" colspan="2" class="tbltext1">
				<strong>&nbsp;&nbsp;Fecha de Direccionamiento*</strong>
			</td>
			<td align="left" colspan="2" width="60%" class="tbltext1">
				<input type="text" name="txtfechaderivint" style="TEXT-TRANSFORM: uppercase;" span="span" class="textblack" size="12" maxlength="10" disabled value="<%=sFechaDeriv%>">
				<a>&nbsp;&nbsp;(dd/mm/aaaa)</a>
				<input type="hidden" name="txtfechaderiv" style="TEXT-TRANSFORM: uppercase;" span="span" class="textblack" size="10" maxlength="10" value="<%=sFechaDeriv%>">
			</td>
		</tr>

<%' Resultado de la Derivaci�n ----------------------------------------------------------------------- %>  

<%
	'Response.Write "sResDeriv: " & sResDeriv
 %>  
		<tr>
			<td align="left" colspan="2" nowrap="nowrap" class="tbltext1">
				<strong>&nbsp;&nbsp;Resultado del Direccionamiento*</strong>
			</td>
			<td align="left" colspan="2" width="60%">
				<% 
				'if sDerivacion <> "" then
					ResDerivCombo = "DREST|0|" & date & "|" & sResDeriv & "|cmbResDeriv' onChange=" & chr(34) & "javascript: recarga()" & chr(34) & "|ORDER BY DESCRIZIONE"			
					%>
					<input type="hidden" name="txtResDeriv" value="<%=sResDeriv%>">
					<%
				'else
				'	ResDerivCombo = "DREST|0|" & date & "|" & "|cmbResDeriv|ORDER BY DESCRIZIONE"					
				'end if 
				CreateCombo(ResDerivCombo)
				%>
			</td>
		</tr>

<%' Fecha de Derivaci�n ------------------------------------------------------------------------------ %>  
		<%
		'if sResDeriv = "01" or sResDeriv = "03" then
		if sResDeriv = "01" then
			'if sResDeriv = "01" then
				EtiqDTfin = "Fecha Resultado Direccionamiento"
			'else
			'	EtiqDTfin = "Fecha Plazo Nueva"
			'end if
			
			%>
			<tr>
				<input type="hidden" name="OpDeriv" value="<%=sDerivacion%>">
				<td align="left" colspan="2" nowrap="nowrap" class="tbltext1">
					<strong>&nbsp;&nbsp;<%=EtiqDTfin%></strong> 
				</td>
				<td align="left" colspan="2" width="60%" class="tbltext1">
					<input type="text" name="txtfecharesult" style="TEXT-TRANSFORM: uppercase;" span="span" class="textblacka fecha" size="12" maxlength="10" value="<%=sFechaResult%>">
					<a>&nbsp;&nbsp;(dd/mm/aaaa)</a>
				</td>
			</tr>
			<%
			 ELSE %>
			
					<input type="HIDDEN" name="txtfecharesult" style="TEXT-TRANSFORM: uppercase;" span="span" class="textblack" size="12" maxlength="10" value="">

		<%End if	
	
		%>
		<%
		sql = ""
		set rs = server.CreateObject("adodb.Recordset")
		sql = "Select * from PERSONA where ID_PERSONA = " & sIdPers
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
		set rs = CC.execute(sql)
		%>
	  	<input type="hidden" id="text1" name="txtfechaactual" value="<%=ConvDateToString(Date())%>">
		<input type="hidden" id="text1" name="txtfechanac" value="<%=ConvDateToString(rs("DT_NASC"))%>">
		<input type="hidden" id="text1" name="txtIdPers" value="<%=Request.Form("IdPers")%>">
		<%
		set rs = nothing
		%>

<%' Porque ? ----------------------------------------------------------------------------------------- %>  
		
		<%
		if sResDeriv = "02" then
			%>
			<tr>
				<td align="left" colspan="2" nowrap="nowrap" class="tbltext1">
					<strong>&nbsp;&nbsp;Por Que?:</strong>
				</td>
					    
				<td align="left" colspan="2" width="60%">
					<input type="text" name="txtporque" style="TEXT-TRANSFORM: uppercase;" class="textblack" maxlength="400" size="50" value="<%=sPorQue%>">
				</td>
			</tr>
			<%
		End if	
		%>

<% 'Observaciones ----------------------------------------------------------------------------------- %>
		<tr>
			<td align="left" colspan="2" nowrap="nowrap" class="tbltext1"><b>
				&nbsp;&nbsp;Observaciones </b>		
			</td>
			
			<td align="left">
						
				<textarea OnKeyUp="JavaScript:CheckLenTxArea(document.frmDeriv.txtObservaciones,document.frmDeriv.textNumCaratteri,4000);" name="txtObservaciones" cols="30" rows="7"><%=sObservaciones%></textarea>
						
				<!--input type="text" name="txtobservaciones" size="3" value ="4000" readonly>-->
				<%
				if sObservaciones <> "" then
					largo = 4000-len(sObservaciones)
				else
					largo = 4000
				end if
				%>		 
				<input type="text" name="textNumCaratteri" value="<%=cstr(largo)%>" size="3" readonly>
			</td>
		</tr>  



<%' ENTERIOR   Y   ENVIAR ---------------------------------------------------------------------------- %>  
	</table>
	<br>
	<table CELLPADDING="0" CELLSPACING="0" border="0" width="500">
		<tr height="2">
			<td class="sfondocommaz" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
		</tr>
	</table>	
	<br>

	<table WIDTH="500" BORDER="0" CELLPADDING="0" CELLSPACING="0">
		<tr align="center">
			<td>
				<a href="javascript:document.FrmVolver.submit();"><img src="<%=Session("progetto")%>/images/Indietro.gif" border="0"></a>
				<input type="image" name="Invia" src="<%=Session("Progetto")%>/images/conferma.gif" border="0" value="Conferma"> 				
				<!--<a href="javascript:Chiudi()" id="cmdCancella"><img src="<%=session("Progetto")%>/images/chiudivalidita.gif" value="chiudi" border="0"></a>-->
			</td>	
		</tr>
	</table>
</form>
<form name="FrmVolver" action="UTE_VisDeriv.asp" method=post>
	<input type="hidden" name="IdPers" value="<%=sIdPers%>">
</form>



<!--#include Virtual ="/Include/closeconn.asp"-->
<!--#include Virtual="/strutt_coda2.asp"-->
