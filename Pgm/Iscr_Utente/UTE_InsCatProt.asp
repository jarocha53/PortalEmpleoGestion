<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%
'  DIFICULTADES PSICOFISICAS
'	Funciones: Visualizacion, Modificacion, Eliminaci�n
'	Tablas: PERS_CATPROT  Campos: id_persona, cod_catprot,  cod_ente_cert, dt_ini_cert, dt_fin_cert,
								' origen_disc, rehabilitacion, soporte, diagnostico, cod_sub_catprot, 
								' medicacion, med_frecuencia, niv_empleabilidad, autohigiene, 
								' cod_traslado, movilidad, tratam_medic, seg_soc, seg_soc_espec, 
								' otr_subsid, otr_subsid_espec, observaciones
'
'	Parametros de Entrada:	id_Persona, cod_catprot, Cod_Accion
'
'	Salida RETROCESO
'
'	Salida MODIFICAR ACT=MOD
'
'	Salida AGREGA    ACT=INS
'
'	Salida ELIMINA   ACT=DEL
'
'	Para todos los casos utiliza el mismo formulario
'		Formulario: frmCatProt   
'			Parametros: ACT, nIdAcc, nIdPers

'
'
'
%>

<!--#include Virtual = "/strutt_testa2.asp"-->
<!-- #include Virtual = "/Include/DecCod.asp" -->
<!-- #include Virtual = "/Include/OpenConn.asp" -->
<!-- #Include Virtual="/Include/ControlDateVB.asp" -->

<script language="javascript" src="/Include/ControlString.inc">
</script>
<script language="Javascript">
<!--#include virtual = "/Include/ControlDate.inc"-->
<!--#include virtual = "/include/ControlString.inc"-->
<!--#include virtual = "/Include/ControlNum.inc"-->
<!--#include virtual = "/Include/help.inc"-->

function indietro()
{
	document.frmCatProt.action='UTE_VisCatProt.asp';
	document.frmCatProt.onsubmit =''
	document.frmCatProt.submit()
}
function elimina()
{
	//alert("PASA POR ACA")
	document.frmCatProt.ACT.value = "DEL"
	document.frmCatProt.action='UTE_CnfCatProt.asp';
	document.frmCatProt.onsubmit =''
	document.frmCatProt.submit()
}

function recarga()
{
	document.frmRecarga.CategProt.value=TRIM(document.frmCatProt.cmbCatProt.value)
	document.frmRecarga.submit()
	//window.navigate("UTE_InsCatProt.asp?CategProt="+document.frmCatProt.cmbCatProt.value)
}

function valida(Obj)
{	
 	//Validacion del Tipo de Discapacidad
	//Obj.cmbCatProt.value=TRIM(Obj.cmbCatProt.value)
	if (Obj.cmbCatProt.value == "")
	{
		alert("El campo Tipo de Discapacidad es obligatorio");
		Obj.cmbCatProt.focus();
		return false
	}
	/*jhamon else
	{
		if (Obj.cmbCatProt.value != "TR")
		{
			Obj.cmbSubCatProt.value=TRIM(Obj.cmbSubCatProt.value)
			if (Obj.cmbSubCatProt.value == "")
			{
				alert("El campo Tipo de Discapacidad es obligatorio");
				Obj.cmbSubCatProt.focus();
				return false
			}
			else
			{
				if (Obj.cmbSubCatProt.value >= 90)
				{
					alert("Debe definir todo el campo Tipo de Discapacidad")
					Obj.cmbSubCatProt.focus();
					return false
				}
			}			
		}
	}jhamon*/
	
	
	
	
	
 	//Validacion del Ente Certificador
	/*jhamon
	Obj.txtEnteCert.value=TRIM(Obj.txtEnteCert.value)
	if (Obj.txtEnteCert.value == "")
	{
		alert("La presentaci�n del Certificado de Discapacidad es obligatoria, en el caso que la persona no lo presente o no est� vigente, se deber� derivarlo a trav�s de la solapa de Derivaciones para que efect�e la tramitaci�n o renovaci�n del mismo.");
		Obj.txtEnteCert.focus();
		return false
	}jhamon*/

	//Validacion de la fecha de inicio del Certificado
	/*if (Obj.txtInicioCert.value == "")
	{
		alert("El campo Fecha de Emisi�n del Certificado es obligatorio");
		Obj.txtInicioCert.focus();
		return false
	}
	else
	{
		if (ValidateInputDate(Obj.txtInicioCert.value)== false)
		{
			Obj.txtInicioCert.focus();
			return false
		}
		else
		{
			if (ValidateRangeDate(Obj.txtInicioCert.value,Obj.txtoggi.value)==false)
			{
				alert("La fecha de Emisi�n del Certificado debe ser anterior a la fecha actual")
				Obj.txtInicioCert.focus();
				return false
			}	
		}
	}
	*/
	
	//Validacion de la fecha de fin del Certificado
	/*jhamon if (Obj.txtFinCert.value == "")
	{
		alert("El campo Fecha de Vencimiento del Certificado es obligatorio");
		Obj.txtFinCert.focus();
		return false
	}
	else
	{
		if (ValidateInputDate(Obj.txtFinCert.value)== false)
		{
			Obj.txtFinCert.focus();
			return false
		}
		else
		{
			if (ValidateRangeDate(Obj.txtInicioCert.value, Obj.txtFinCert.value)==false)
			{
				alert("La fecha de Vencimiento del Certificado debe ser posterior a la fecha de Emisi�n")
				Obj.txtFinCert.focus();
				return false
			}	
			else
			{
				if (ValidateRangeDate(Obj.txtoggi.value,Obj.txtFinCert.value)==false)
				{
					alert("La fecha de Vencimiento del Certificado debe ser posterior a la fecha actual")
					Obj.txtFinCert.focus();
					return false
				}	
			}
		}
	}jhamon*/

 	//Validacion del Diagnostico
	/*jhamon
	Obj.txtDiagnostico.value=TRIM(Obj.txtDiagnostico.value)
	if (Obj.txtDiagnostico.value == "")
	{
		alert("El ingreso del diagn�stico es obligatorio.");
		Obj.txtDiagnostico.focus();
		return false
	}
	jhamon*/

 	//Validacion de la frecuencia de Medicacion
	/*jhamon
	if (Obj.txtMedicacion[0].checked == true)
	{
		//alert("esta en SI");
		if (Obj.cmbFrecuencia.value == "")
		{
			alert("Si eligi� 'SI' en la opcion Toma Medicacion, debe especificar la frecuencia")
			Obj.cmbFrecuencia.focus();
			return false
		}
	}jhamon */
 	//Validacion de la Especificacion de Seguridad Social
	/*jhamon
	Obj.txtSSocEspec.value=TRIM(Obj.txtSSocEspec.value)
	if (Obj.optSSoc[0].checked == true)
	{
		//alert("esta en SI");
		if (Obj.txtSSocEspec.value == "")
		{
			alert("Si eligi� la opci�n 'SI' en Seguridad Social debe especificarla")
			Obj.txtSSocEspec.focus();
			return false
		}
	}
	jhamon*/

 	//Validacion de la Especificacion del Subsidio
	/*jhamon
	Obj.txtSSocEspec.value=TRIM(Obj.txtSSocEspec.value)
	if (Obj.optSubsid[0].checked == true)
	{
		//alert("esta en SI");
		if (Obj.txtSubsidEspec.value == "")
		{
			alert("Si eligi� la opci�n 'SI' en Seguridad Social debe especificarla")
			Obj.txtSubsidEspec.focus();
			return false
		}
	}
	jhamon */
}


</script>
<script language="javascript" src="Controlli.js">
</script>

<%	
Dim nIdAcc, nIdPers, nCodAcc, sACT
Dim sSql , RS
'Dim nIdPers, sCatProt, sSubCatProt, sEnteCert, dInicioCert, dFinCert, sOrigenDisc
'Dim sRehabilitacion, sSoporte, sDiagnostico, sMedicacion, sFrecuenMedic, sEmpleabilidad
'Dim sAutohigiene, sTraslado, sMovilidad, sTratamiento, sSC, sSSocEspec, sSubsid, sSubsidEspec 
	

nIdPers		= Request.Form("IdPers")
sCatProt	= Request.Form("CategProt")
nCodAcc		= Request.Form("Cod_Accion")
sACT		= Request("ACT")

if nIdPers <> "" then
	nIdP = clng(nIdPers)
		
	if session("Progetto") <> "/PLAVORO" then
		sIdpers = nIdPers
		%>
		<br><!--#include virtual ="/pgm/iscr_utente/menu.asp"-->
		<%
	end if
else
	nIdPers= session("creator")
end if
Err.Clear



if sACT = "MOD" then

	sSQL = "SELECT id_persona, cod_catprot, situacion, cod_ente_cert, dt_ini_cert, dt_fin_cert, " &_
			"origen_disc, rehabilitacion, soporte, diagnostico, cod_sub_catprot, medicacion, " &_
			"med_frecuencia, niv_empleabilidad, autohigiene, cod_traslado, movilidad, tratam_medic, " &_
			"seg_soc, seg_soc_espec, otr_subsid, otr_subsid_espec, observaciones " &_
			"FROM pers_catprot " &_
			"WHERE cod_catprot = '" & sCatProt& "' AND id_persona = " & nIdPers 

	
	set RS = server.CreateObject("ADODB.recordset")
'PL-SQL * T-SQL  
'SSQL = TransformPLSQLToTSQL (SSQL) 
	RS.Open sSQL,CC

	If not rs.eof then
		'nIdPers		= RS("id_persona")
		'sCatProt		= RS("cod_catprot")
		sSubCatProt		= RS("cod_sub_catprot")
		sSituacion		= RS("situacion")
		sEnteCert		= RS("cod_ente_cert")
		dInicioCert		= RS("dt_ini_cert")
		dFinCert		= RS("dt_fin_cert")
		sOrigenDisc		= RS("origen_disc")
		sRehabilitacion	= RS("rehabilitacion")
		sSoporte		= RS("soporte")
		sDiagnostico	= RS("diagnostico")
		sMedicacion		= RS("medicacion")
		sFrecuenMedic	= RS("med_frecuencia"	 )
		sEmpleabilidad	= RS("niv_empleabilidad")
		sAutohigiene	= RS("autohigiene")
		sTraslado		= RS("cod_traslado")
		sMovilidad		= RS("movilidad")
		sTratamiento	= RS("tratam_medic")
		sSSoc			= RS("seg_soc")
		sSSocEspec		= RS("seg_soc_espec")
		sSubsid			= RS("otr_subsid")
		sSubsidEspec	= RS("otr_subsid_espec")
		sobservaciones  = RS("observaciones")
	End If 
	rs.close
	set rs=nothing
end if

%>
<br>
<table cellpadding="0" cellspacing="1" width="500" border="0">
	<tr height="18">
		<td class="sfondomenu" height="18" width="45%">
			<span class="tbltext0"><b>&nbsp;DISCAPACIDAD</b></span>
		</td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
		<td valign="middle" align="right" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1">(*) campo obligatorio</td>
	</tr>

	<tr width="371" class="SFONDOCOMM">
		<td class="sfondocomm" align="left" colspan="3" class="tbltext1">
		Para ingresar o modificar los datos complete los campos relacionados. <br>
		Presione <b>enviar</b> para guardar la informaci&oacute;n. 
		
		<!--NUEVO AGREGADO POR F.BASANTA -->		
		<br>
		<img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0" onclick="Javascript:window.open('ayuda_dificultades.htm','Ayuda','width=500,height=350,Resize=No,Scrollbars=yes')">		
		

		<%if sCondizione = "INS" then%>
<!--			
			<a href="Javascript:Show_Help('/Pgm/help/Iscr_Utente/UTE_InsCatProt')" name="prov3" onmouseover="javascript:window.status='' ; return true"><img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
-->
		<%else%>
<!--			
			<a href="Javascript:Show_Help('/Pgm/help/Iscr_Utente/UTE_InsCatProt_1')" name="prov3" onmouseover="javascript:window.status='' ; return true"><img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
-->
		<%end if%>
		</td>
	</tr>
	<tr height="2">
		<td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif">
		</td>
	</tr>
</table>

<form name="frmRecarga" method="post" action="UTE_InsCatProt.asp"  OnSubmit="return valida(this);">
	<input type="hidden" name="ACT" value="<%=sACT%>">
	<input type="hidden" name="IdPers" value="<%=nIdPers%>">
	<input type="hidden" name="CategProt" value="">
</form>

<form name="frmCatProt" method="post" action="UTE_CnfCatProt.asp"  OnSubmit="return valida(this);">
	<input type="hidden" name="ACT" value="<%=sACT%>">
	<input type="hidden" name="IdPers" value="<%=nIdPers%>">
    <input type="hidden" name="txtoggi" value="<%=ConvDateToString(Date())%>">
	<table cellpadding="0" cellspacing="16" width="530" border="0">
		<%' Tipo de Discapacidad------------------------------------------------------------%>
		<tr>
			<td align="left" class="tbltext1" width="140">
	 			<b>Tipo de Discapacidad: *</b>
	 		</td>
			<td class="textblack">
				<%
				if sACT = "MOD" then 
					dim sDecodCatProt
					sDecodCatProt = DecCodVal("TCAPI", "0", "", sCatProt, 1)
					%>
					<input type="hidden"  name="cmbCatProt" value="<%=sCatProt%>">
					<b><%=sDecodCatProt%></b>					
					<%
				else
					CatProtCombo = "TCAPI|0|" & date & "|" & sCatProt & "|cmbCatProt' onChange=" & chr(34) & "javascript: recarga()" & chr(34) & "|ORDER BY DESCRIZIONE"			
					CreateCombo(CatProtCombo)
				end if
				%>
				&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;
				
				<%
				'Response.Write "sSubCatProt: " &  sSubCatProt
				if sCatProt <> "TR" and not sCatProt = "" then 
					'Condiciones para que el valor (no definido) solo aparezca en las cargadas anteriormente RIC
					CatProtCombo = "STCAP|0|" & date & "|" & sSubCatProt & "|cmbSubCatProt' |AND valore = '" & sCatProt & "' AND codice < 90 ORDER BY DESCRIZIONE"			
					if sACT <> "INS" and cint(sSubCatProt) >= cint("90") then
						CatProtCombo = "STCAP|0|" & date & "|" & sSubCatProt & "|cmbSubCatProt' |AND valore = '" & sCatProt & "' ORDER BY DESCRIZIONE"			
					end if				
					CreateCombo(CatProtCombo)
				end if
				%>
			</td>
		</tr>
		<%' Origen de la discapacidad --------------------------------------------------------%> 
		<!--jhamon<tr>
			<td align="left" class="tbltext1">
				<b>Tipo: *</b>
			</td>
			<td>
				<%
				CreateComboDizDatiNull "PERS_CATPROT","ORIGEN_DISC","cmbOrigenDisc","textblack","",sOrigenDisc 
				%>

				<%
				'SoporteUtilizado = "SOPOR|0|" & date & "|" & sSoporte & "|cmbSoporteUtilizado|AND VALORE = '" & stipodisc & "' ORDER BY DESCRIZIONE"			
				'CreateCombo(SoporteUtilizado)
				%>
			</td>
			<td>
				<%
				dim mirs, miarray, cadena
				set mirs = server.CreateObject("adodb.recordset")
				VariableSQL = "Select ID_CAMPO_DESC from DIZ_DATI where ID_TAB = 'PERS_CATPROT' and ID_CAMPO = 'ORIGEN_DISC'"
			'PL-SQL * T-SQL
				VariableSQL = TransformPLSQLToTSQL(VariableSQL)  
				set mirs = CC.execute(VariableSQL)		
				if not mirs.EOF then 
					cadena = mirs("ID_CAMPO_DESC")
					miarray = split(cadena,"|")
				end if
				%>
				<select class=textblack name = "ORIGENDISC">
					<%
					for y = lbound(miarray) to ubound(miarray) step 2
						%>
						<option value = '<%=miarray(y)%>' <%if miarray(y) = sOrigenDisc then response.write "selected"%>><%=miarray(y+1)%></option>"
						<%
					next 
					%>
				</select>
				<%set mirs = nothing%>
			</td>
		</tr>-->
		
		<% 'Situacion -------------------------------------------------------------------------------------------%>
		<%
		if sCatProt = "VS" then
			%>
			<tr>
				<td align="left" class="tbltext1" width="140">
 					<b>Situaci�n: </b>
 				</td>
				<td>
					<%
					SituaCombo = "CAPST|0|" & date & "|" & sSituacion & "|cmbSituacion' | ORDER BY DESCRIZIONE"			
					CreateCombo(SituaCombo)
					%>
				</td>
			</tr>
			<%
		end if
		%>
		
		<% ' Soporte Utilizado --------------------------------------------------------------------------------- %>
		<%
		if sCatProt <> "VS" then
			%>
			<tr>
				<td align="left" class="tbltext1" width="140">
 					<!--jhamon<b>Soporte Utilizado: </b>jhamon-->
 				</td>
				<td  class="tbltext1">
					<!--input type="text" class="textblacka" maxlength="99" size="55" name="txtSoporte" value="<%=sSoporte%>"-->
					<%
					sSQLaux = "SELECT codice, descrizione, valore FROM tades WHERE nome_tabella = 'SOPOR' AND valore = '" & sCatProt & "' ORDER BY descrizione"
					set RSaux = server.CreateObject("ADODB.recordset")
'PL-SQL * T-SQL  
'SSQLAUX = TransformPLSQLToTSQL (SSQLAUX) 
					RSaux.Open sSQLaux,CC
					do while not RSaux.EOF 
						%>
						<input type="checkbox" class="textblack" id="chkAct<%=RSaux("codice")%>" value="<%=RSaux("descrizione")%>" <% if instr(sSoporte,RSaux("codice"))then Response.Write  " checked" %> name="chkAct<%=RSaux("codice")%>">&nbsp;<b><%=RSaux("descrizione")%></b><br>
						<%
						'Response.Write RSaux("codice")
						RSaux.MoveNext 
					loop
					RSaux.close
					set RSaux=nothing
					%>
				</td>
			</tr>
			<%
		end if
		%>
		
		<%' Certificado ---------------------------------------------------------------------------------------- %> 
  		<!--jhamon <tr>
		    <td height="2" align="left" colspan="2" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
		</tr>
		<tr>
			<td align="left" colspan="2" class="tbltext1">
				<b>Certificado de Discapacidad: </b> <br><br>
				La presentaci�n del Certificado de Discapacidad es obligatoria, en el caso que la persona no lo presente o no est� vigente, se deber� derivarlo a trav�s de la solapa de Derivaciones para que efect�e la tramitaci�n o renovaci�n del mismo.<br>
			</td>
		</tr>jhamon-->
		<% 'Ente Certificador ---------------------------------------------------------------------------------- %>
		<!--jhamon<tr>
			<td align="left" class="tbltext1" width="140">
 				<b>Ente Certificador: </b>
 			</td>
			<td>
				<input type="text" class="textblacka" maxlength="49" size="55" name="txtEnteCert" value="<%=sEnteCert%>">
			</td>
		</tr>jhamon-->
		<% 'Fecha de Emisi�n del Certificado ------------------------------------------------------------------- %>
		<!--jhamon<tr>
			<td align="left" class="tbltext1" width="140">
 				<b>Fecha de Emisi�n del Certificado: </b>
 			</td>
			<td class="tbltext1">
				<input type="text" class="textblacka" maxlength="10" size="10" name="txtInicioCert" value="<%=dInicioCert%>">
				<a>&nbsp;&nbsp;(dd/mm/aaaa)</a>
			</td>
		</tr>-->
		<% 'Fecha de Vencimiento del Certificado --------------------------------------------------------------- %>
		<!--jhamon<tr>
			<td align="left" class="tbltext1" width="140">
 				<b>Fecha de Vencimiento del Certificado: </b>
 			</td>
			<td class="tbltext1">
				<input type="text" class="textblacka" maxlength="10" size="10" name="txtFinCert" value="<%=dFinCert%>">
				<a>&nbsp;&nbsp;(dd/mm/aaaa)</a>
			</td>
		</tr>-->
		<% 'Diagnostico ---------------------------------------------------------------------------------------- %>
		<!--jhamon<tr>
			<td align="left" class="tbltext1" width="140">
 				<b>Diagnostico: </b>
 			</td>
			<td>
				<input type="text" class="textblacka" maxlength="200" size="55" name="txtDiagnostico" value="<%=sDiagnostico%>">
			</td>
		</tr>
  		<tr>
		    <td height="2" align="left" colspan="2" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
		</tr>jhamon-->
		<% ' Realizo rehabilitacion ---------------------------------------------------------------------------- %>
		<!--jhamon<tr>
			<td align="left" class="tbltext1" width="140">
 				<b>�Ha Realizado el Proceso de Rehabilitaci�n?</b>
 			</td>
			<td class="tbltext1">
				<input type=radio name="txtRehabilitacion" value="S" <%if sRehabilitacion = "S" then Response.Write "checked"%> >SI    
				<input type=radio name="txtRehabilitacion" value="N" <%if sRehabilitacion = "N" then Response.Write "checked"%> >NOjhamon-->
				<!--input type="text" class="textblacka" maxlength="99" size="55" name="txtestablecimiento" value="<%=sRehabilitacion%>"-->
			<!--jhamon</td>
		</tr>jhamon-->
		<% ' Toma Medicacion ----------------------------------------------------------------------------------- %>
		<!--jhamon <tr>
			<td align="left" class="tbltext1" width="140">
 				<b>Toma Medicaci�n: </b>
 			</td>
			<td class="tbltext1">
				<input type=radio name="txtMedicacion" value="S" <%if sMedicacion = "S" then Response.Write "checked"%> >SI    
				<input type=radio name="txtMedicacion" value="N" <%if sMedicacion = "N" then Response.Write "checked"%> >NO-->
				<!--input type="text" class="textblacka" maxlength="99" size="55" name="txtestablecimiento" value="<%=sMedicacion%>"-->
			<!--jhamon</td>
		</tr>
		-->
		<% 'Frecuencia ------------------------------------------------------------------------------------------- %>
		<!--jhamon<tr>
			<td align="left" class="tbltext1" width="140">
 				<b>Frecuencia de la Medicaci�n: </b>
 			</td>
			<td>-->
				<!--input type="text" class="textblacka" maxlength="99" size="55" name="txtTraslado" value="<%=sTraslado%>"-->
				<!--jhamon
				<%
				'jhamon CatProtCombo = "FRECU|0|" & date & "|" & sFrecuenMedic & "|cmbFrecuencia' | ORDER BY DESCRIZIONE"			
				'jhamon CreateCombo(CatProtCombo)
				%>
			</td>
		</tr>jhamon-->
		<% ' Nivel de Empleabilidad ---------------------------------------------------------------------------- %>
		<!--<tr>
			<td align="left" class="tbltext1" width="140">
 				<b>Nivel de Empleabilidad: </b>
 			</td>
			<td>
				<%
				CreateComboDizDati "PERS_CATPROT","NIV_EMPLEABILIDAD","txtEmpleabilidad","textblack","",sEmpleabilidad 
				%>
				input type="text" class="textblacka" maxlength="99" size="55" name="txtEmpleabilidad" value="<%=sEmpleabilidad%>"
			</td>
		</tr>
  		<tr>
		    <td height="2" align="left" colspan="2" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
		</tr>-->

		<% 'Autohigiene  --------------------------------------------------------------------------------------- %>
		<!--jhamon<tr>
			<td align="left" class="tbltext1" width="140">
 				<b>Autohigiene: </b>
 			</td>
			<td class="tbltext1">
				<input type=radio name="optAutohigiene" value="S" <%if sAutohigiene = "S" then Response.Write "checked"%> >SI    
				<input type=radio name="optAutohigiene" value="N" <%if sAutohigiene = "N" then Response.Write "checked"%> >NOjhamon-->
				<!--input type="text" class="textblacka" maxlength="99" size="55" name="txtestablecimiento" value="<%=sAutohigiene%>"-->
			<!--jhamon</td>
		</tr>jhamon-->

		<% 'Traslado ------------------------------------------------------------------------------------------- %>
		<!--jahmon<tr>
			<td align="left" class="tbltext1" width="140">
 				<b>Traslado: </b>
 			</td>
			<td>jhamon-->
				<!--input type="text" class="textblacka" maxlength="99" size="55" name="txtTraslado" value="<%=sTraslado%>"-->
				<%
				'jhamon CatProtCombo = "CTRAD|0|" & date & "|" & sTraslado & "|cmbTraslado' | ORDER BY DESCRIZIONE"			
				'jhamon CreateCombo(CatProtCombo)
				%>
			<!--jhamon </td>
		</tr>
jhamon-->
		<% 'Movilidad ------------------------------------------------------------------------------------------ %>
		<!--jhamon<tr>
			<%
			' Cargo "Sin Acompa�ante" como valor predeterminado
			if sMovilidad = "" then sMovilidad = "S"
			%>
			<td align="left" class="tbltext1" width="140">
 				<b>Movilidad: </b>
 			</td>
			<td>jhamon-->
				<!--input type="text" class="textblacka" maxlength="99" size="55" name="txtMovilidad" value="<%=sMovilidad%>"-->
				<%
				'jhamon CreateComboDizDatiNull "PERS_CATPROT","MOVILIDAD","cmbMovilidad","textblack","",sMovilidad
				%>
			<!--jhamon</td>
		</tr>
  		<tr>
		    <td height="2" align="left" colspan="2" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
		</tr>
        jhamon-->
		<% 'Tratamientos Medicos Actuales ---------------------------------------------------------------------- %>
		<!--jhamon<tr>
			<td align="left" class="tbltext1" width="140">
 				<b>Tratamientos M�dicos Actuales: </b>
 			</td>
			<td>
				<input type="text" class="textblacka" maxlength="99" size="55" name="txtTratamiento" value="<%=sTratamiento%>">
			</td>
		</tr>
-->
		<% 'Seguridad Social ----------------------------------------------------------------------------------- %>
		<!--jhamon<tr>
			<td align="left" class="tbltext1" width="140">
 				<b>Seguridad Social: </b>
 			</td>
			<td class="tbltext1">
				<input type=radio name="optSSoc" value="S" <%if sSSoc = "S" then Response.Write "checked"%> >SI    
				<input type=radio name="optSSoc" value="N" <%if sSSoc = "N" then Response.Write "checked"%> >NOjhamon-->
				<!--input type="text" class="textblacka" maxlength="99" size="55" name="txtestablecimiento" value="<%=sSC%>"-->
			<!--jhamon </td>
		</tr>jhamon-->

		<% 'Especificacion Seguridad Social ------------------------------------------------------------------ %>
		<!--jhamon<tr>
			<td align="left" class="tbltext1" width="140">
 				<b>Especificaci�n Seguridad Social: </b>
 			</td>
			<td>
				<input type="text" class="textblacka" maxlength="99" size="55" name="txtSSocEspec" value="<%=sSSocEspec%>">
			</td>
		</tr>jhamon-->


		<% 'Otros Subsidios ----------------------------------------------------------------------------------- %>
		<!--jhamon <tr>
			<td align="left" class="tbltext1" width="140">
 				<b>Otros Subsidios: </b>
 			</td>
			<td class="tbltext1">
				<input type=radio name="optSubsid" value="S" <%if sSubsid = "S" then Response.Write "checked"%> >SI    
				<input type=radio name="optSubsid" value="N" <%if sSubsid = "N" then Response.Write "checked"%> >NO jhamon-->
				<!--input type="text" class="textblacka" maxlength="99" size="55" name="txtestablecimiento" value="<%=sSC%>"-->
			<!--jhamon</td>
		</tr>jhamon-->

		<% 'Especificacion Otros Subsidios ------------------------------------------------------------------ %>
		<!--jhamon<tr>
			<td align="left" class="tbltext1" width="140">
 				<b>Especificaci�n Otros Subsidios: </b>
 			</td>
			<td>
				<input type="text" class="textblacka" maxlength="99" size="55" name="txtSubsidEspec" value="<%=sSubsidEspec%>">
			</td>
		</tr>
		jhamon-->
		<% 'Observaciones ----------------------------------------------------------------------------------- %>
		<!--jhamon	<tr>
				<td align="left" class="tbltext1" nowrap><b>
					&nbsp;&nbsp;Observaciones </b>		
				</td>
				<td align="left">
					
					<textarea OnKeyUp="JavaScript:CheckLenTxArea(document.frmCatProt.txtObservaciones,document.frmCatProt.textNumCaratteri,4000);" name="txtObservaciones" cols="30" rows="7"><%=sObservaciones%></textarea>
		-->			
					<!--<input type="text" name="txtobservaciones" size="3" value ="4000" readonly>-->
					 
		<!--			<input type="text" name="textNumCaratteri" value="4000" size="3" readonly>
				</td>
		   </tr>
		-->   
		  
  		<tr>
		    <td height="2" align="left" colspan="2" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
		</tr>

		<tr>
			<td colspan="2" class="tbltext1" align="center"><br>
				<input type="image" title="" src="<%=Session("Progetto")%>/images/indietro.gif" id="image1" name="image1" onclick="indietro()">&nbsp;&nbsp;
				<input type="image" title="" src="<%=Session("Progetto")%>/images/conferma.gif" id="image2" name="image2">
				<%
				if sACT <> "INS" then
					%>
					&nbsp;&nbsp;
					<input type="image" title="" src="<%=Session("Progetto")%>/images/elimina.gif" id="image3" name="image3" onclick="elimina()">
					<%
				end if
				%>
			</td>                                                                                                      
		</tr>
	</table>
</form> 

<%
'Response.Write "nIdPers: " & nIdPers & "<br>"
'Response.Write "sCatProt: " & sCatProt & "<br>"
'Response.Write "sACT: " & sACT & "<br>"
'Response.Write "sSQL: " & sSQL & "<br>"
'Response.End
%>	






<!--#include Virtual = "/include/closeconn.asp"-->
<!--#include Virtual="/strutt_coda2.asp" -->

