<!--#include Virtual = "/strutt_testa2.asp"-->
<!--#include Virtual = "/include/OpenConn.asp"-->
<!--#include Virtual = "/include/ControlDateVB.asp"-->

<%
dim sIdpers
sIdpers = Request.Form("ID_Persona")
sCodCatage=Request.Form("COD_CATAGE")
sDtIniCatage=Request.Form("DT_INI_CATAGE")
sDtFinCatage=Request.Form("DT_FIN_CATAGE")
sIndStatus=Request.Form("IND_STATUS")
DecCatage=Request.Form("DecCatage")
%>

<script LANGUAGE="Javascript">
//include del file per fare i controlli sulla validit� delle date
<!--#include Virtual = "/Include/ControlDate.inc"-->

//include del file per fare i controlli sulla numericit� dei campi
<!--#include Virtual = "/Include/ControlNum.inc"-->

//include del file per fare i controlli sulle stringhe
<!--#include Virtual = "/Include/ControlString.inc"-->
<!--#include Virtual = "/Include/help.inc"-->

function ControllaDati(frmInsCatAge){
	//-------- Controlli di OBBLIGATORIETA' --------
	//-------- Controlli FORMALI            --------
	//-------- Controlli di RELAZIONE       --------�
	
		if (frmInsCatAge.cmbTipoCatage.value == ""){
			alert("El campo categor�a es obligatorio!");
			frmInsCatAge.cmbTipoCatage.focus() ;
			return false;
		}
		
		//Controllo della validit� della Data di inizio
		DataIni = frmInsCatAge.txtDataIni.value
		
		if (DataIni == ""){
			alert("El campo fecha de inicio es obligatorio!")
			frmInsCatAge.txtDataIni.focus() 
			return false
		}
		
		if (!ValidateInputDate(DataIni)){
			frmInsCatAge.txtDataIni.focus() 
			return false
		}	
		
		DataOdierna=frmInsCatAge.txtoggi.value
		if (ValidateRangeDate(DataOdierna,DataIni)==true){
			alert("La fecha de inicio debe ser anterior a la fecha actual!")
			frmInsCatAge.txtDataIni.focus()
		    return false
		}

		//Controllo della validit� della Data di fine
		DataFine = frmInsCatAge.txtDataFine.value
		
		if (DataFine == ""){
			alert("El campo fecha de fin es obligatorio!")
			frmInsCatAge.txtDataFine.focus() 
			return false
		}
		
		if (!ValidateInputDate(DataFine)){
			frmInsCatAge.txtDataFine.focus() 
			return false
		}	
		
		if (ValidateRangeDate(DataFine,DataIni)==true){
			alert("La fecha de fin debe ser superior a la fecha de inicio!")
			frmInsCatAge.txtDataFine.focus()
		    return false
		}
		
}
</script>

<!--#include Virtual = "/include/DecCod.asp"-->
<!--#include Virtual="/util/dbUtil.asp"-->

<br>	
	
<!--#include file="menu.asp"-->

<br>

	<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
		<tr height="18">
			<td class="sfondomenu" width="67%" height="18"><span class="tbltext0"><b>&nbsp;CAGEGORIA AGEVOLATA </b></span></td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
			<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">(*) campo obligatorio</td>
		</tr>
	</table>
	<!-- Commento -->
	<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
		<tr>
			<td align="left" class="sfondocomm">
				Ingreso de la categor�a. 
				<br>Complete el campo y presione <b>Enviar</b> para guardar las modificaciones.
				<a href="Javascript:Show_Help('/Pgm/help/UTE_InsCatage.htm')" name="prov3" onmouseover="javascript:window.status='' ; return true">
				<img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
			</td>
		</tr>
		<tr height="2">
			<td class="sfondocomm" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr> 
		  			
   	<form method="post" name="frmInsCatAge" onsubmit="return ControllaDati(this)" action="UTE_CnfInsCatAge.asp?s=<%=Request.QueryString("s")%>">
		<input type="hidden" id="TXT_Idpers" name="TXT_Idpers" value="<%=sIdpers%>">
		<input type="hidden" id="DT_TMST" name="DT_TMST" value="<%=Request.Form("DT_TMST")%>">
		<table border="0" cellpadding="0" cellspacing="1" width="500">
	   		<br>
		    <tr>
				<td align="middle" colspan="2" nowrap class="tbltext1" width="120">
					<p align="left">
						<strong>&nbsp;</strong>
						<strong>&nbsp;&nbsp;Categor�a*</strong>
						<strong>&nbsp;</strong>
					</p>
				</td>
				<td align="left" colspan="2" width="60%" width="400">
					<%
					sInt = "CAGEV|0|" & date & "|" & sCodCatage & "|cmbTipoCatage|ORDER BY DESCRIZIONE"			
					if Request.QueryString("s") = 1 then
						CreateCombo(sInt)				
					else
					%>
						<font class="textblacka"><%=DecCatage%></font>
						<input type="hidden" name="cmbTipoCatage" value="<%=sCodCatage%>">
					<%end if%>					
				</td>
		    </tr>
			<tr>     
				<td align="middle" colspan="2" nowrap class="tbltext1" width="120">
					<p align="left">
						<strong>&nbsp;</strong>
						<strong>&nbsp;&nbsp;Fecha&nbsp;inicio*</strong><font size="1">&nbsp;(gg/mm/aaaa)</font>
						<strong>&nbsp;</strong>
					</p>
				</td>	
				<td align="left" colspan="2" width="60%" width="400">
					<input name="txtoggi" type="hidden" value="<%=ConvDateToString(Date())%>">
					<%if Request.QueryString("s") = 1 then %>
						<input name="txtDataIni" type="text" maxlength="10" size="10" class="textblacka">					
					<%else%>
						<font class="textblacka"><%=sDtIniCatage%></font>
						<input name="txtDataIni" type="hidden" maxlength="10" size="10" class="textblacka" value="<%=sDtIniCatage%>">
					<%end if%>
				</td>
		    </tr>
			<tr>
				<td align="middle" colspan="2" nowrap class="tbltext1" width="120">
					<p align="left">
						<strong>&nbsp;</strong>
						<strong>&nbsp;&nbsp;Fecha&nbsp;fin*</strong><font size="1">&nbsp;(gg/mm/aaaa)</font>
						<strong>&nbsp;</strong>
					</p>
				</td>
				<td align="left" colspan="2" width="60%" width="400">
					<span class="tbltext">
					<input name="txtDataFine" type="text" maxlength="10" size="10" class="textblacka" value="<%=sDtFinCatage%>">
					</span>
				</td>
		    </tr>
		</table>				    
		<table WIDTH="500">
		    <tr>
		        <td align="left" colspan="2">&nbsp;</td>
		        <td align="left" colspan="2" width="60%">&nbsp;</td>
		    </tr>
		</table>
		<br>
		<table border="0" cellpadding="1" cellspacing="1" width="500">
		<tr>
		<%if Request.QueryString("s") = 1 then %>
		    <td align="center" colspan="2">
				<input type="image" src="<%=Session("Progetto")%>/images/conferma.gif" border="0" value="Inserisci" id="image2" name="image2">
		    </td>
		<%else%>
		    <td align="right" colspan="2">
				<input type="image" src="<%=Session("Progetto")%>/images/chiudi.gif" border="0" value="Chiudi" id="image1" name="image1">
		    </td>
		    <td align="left" colspan="2">
				<input type="image" src="<%=Session("Progetto")%>/images/conferma.gif" border="0" value="Inserisci" id="image2" name="image2">
		    </td>
		<%end if%>		    
		</tr>
		</table>
	</form>		    		    
<!--#include virtual = "/include/CloseConn.asp"-->
<!--#include Virtual = "/strutt_coda2.asp"-->

