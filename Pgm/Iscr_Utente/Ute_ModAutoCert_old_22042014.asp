<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include Virtual = "/strutt_testa2.asp"-->
<!--#include Virtual = "/include/OpenConn.asp"-->
<!--#include Virtual = "/include/ControlDateVB.asp"-->

<!--#include Virtual = "/include/DecComun.asp"-->

<script LANGUAGE="Javascript">

<!--#include virtual = "/include/SelComune.js"-->

//include del file per fare i controlli sulla validit� delle date
<!--#include Virtual = "/Include/ControlDate.inc"-->

//include del file per fare i controlli sulla numericit� dei campi
<!--#include Virtual = "/Include/ControlNum.inc"-->

//include del file per fare i controlli sulla validit� del CF
<!--#include Virtual = "/Include/ControlCodFisc.inc"-->

//include del file per fare i controlli sulle stringhe
<!--#include Virtual = "/Include/ControlString.inc"-->
<!--#include Virtual = "/Include/help.inc"-->

function ControllaDati(frmMod){
	//-------- Controlli di OBBLIGATORIETA' --------
	//-------- Controlli FORMALI            --------
	//-------- Controlli di RELAZIONE       --------
	frmModDatiAutoCert.txtNDoc.value=TRIM(frmModDatiAutoCert.txtNDoc.value)
	DataRil = frmModDatiAutoCert.txtDataRil.value
	DataOdierna=frmModDatiAutoCert.txtoggi.value
	DataNasc=frmModDatiAutoCert.txtDtNasc.value
	
	if (DataRil != "") 
	{
		if (!ValidateInputDate(DataRil)){
			frmModDatiAutoCert.txtDataRil.focus() 
			return false
		}	
		if (ValidateRangeDate(DataOdierna,DataRil)==true){
			alert("La fecha de expedici�n debe ser superior a la fecha actual!")
			frmModDatiAutoCert.txtDataRil.focus()
		    return false
		}
		if (ValidateRangeDate(DataRil,DataNasc)==true){
			alert("La fecha de expedici�n debe ser superior a la fecha de nacimiento!")
			frmModDatiAutoCert.txtDataRil.focus()
		    return false
		}
	}
	
	DataScad = frmModDatiAutoCert.txtDataScad.value
	if (DataScad != "") 
	{
		if (!ValidateInputDate(DataScad)){
			frmModDatiAutoCert.txtDataScad.focus() 
			return false
		}
			
		if (ValidateRangeDate(DataScad,DataOdierna)==true){
		    alert("La fecha de vencimiento debe ser superior a la fecha actual!!")
			//alert("Data di scadenza precedente alla data odierna!")
			frmModDatiAutoCert.txtDataScad.focus()			
		    return false
		   }
	}
   frmModDatiAutoCert.idente.value=TRIM(frmModDatiAutoCert.idente.value)
	//Comune di Nascita e Provincia di Nascita obbligatori
	//e in alternativa con Nazione di Nascita
	frmModDatiAutoCert.txtComune.value=TRIM(frmModDatiAutoCert.txtComune.value)
	/*if ((frmModDatiAutoCert.txtComune.value == "")||
		(frmModDatiAutoCert.txtComune.value == " ")){ 
		if (frmModDatiAutoCert.cmbProvRil.value == ""){
			if (frmModDatiAutoCert.cmbNazione.value == ""){
				alert("Los campos municipio y provincia de expedici�n o pa�s de expedici�n son obligatorios!")
				frmModDatiAutoCert.txtComune.focus() 
				return false
			}
		}	
	}*/
	if (frmModDatiAutoCert.txtComune.value != ""){ 
		if (frmModDatiAutoCert.cmbProvRil.value != ""){
			if(frmModDatiAutoCert.cmbNazione.value != ""){
				alert("Indicar �nicamente el departamento y municipio de expedici�n o el pa�s.")
				frmModDatiAutoCert.txtComune.focus() 
				return false
			}
		}	
	}
	//Se Comune di Nascita � digitato, la Provincia � obbligatoria
	/*if (frmModDatiAutoCert.txtComune.value != ""){
		if (frmModDatiAutoCert.cmbProvRil.value == ""){
			alert("el campo provincia de expedici�n es obligatorio!")
			frmModDatiAutoCert.cmbProvRil.focus() 
			return false
		}
	}*/
	
	//Se la Provincia di Nascita � digitata, il Comune � obbligatorio
	/*if ((frmModDatiAutoCert.txtComune.value == "")||
		(frmModDatiAutoCert.txtComune.value == " ")){
		if (frmModDatiAutoCert.cmbProvRil.value != ""){
			alert("El campo municipio de expedici�n es obligatorio!")
			frmModDatiAutoCert.txtComune.focus() 
			return false
		}
	}*/
		
}
function Pulisci()
{
	frmModDatiAutoCert.txtComune.value = "";
	frmModDatiAutoCert.txtComuneHid.value = ""
}		


function PulEstero()
{
  if (frmModDatiAutoCert.cmbNazione.value!="") {
	frmModDatiAutoCert.cmbProvRil.selectedIndex=0;
	frmModDatiAutoCert.txtComune.value = "";
	frmModDatiAutoCert.txtComuneHid.value = ""
	}
}		

/** +++++++++++++++++++++++++++++++++++++++++++++++++
+	@autor: IPTECHONOLOGIES							*
++++++++++++++++++++++++++++++++++++++++++++++++++++*/

	function elimina() {
		if(confirm("Est� seguro que desea borrar la Licencia de Conducci�n?")){
			$.get("Del_AutoCert.asp",$("#frmModDatiAutoCert").serializeArray())
			.done(
				function(data){
					alert("Licencia de Conducci�n eliminada con exito.")
					document.FrmVisAutCert.submit();
				}
			)
			.fail(function(data){
				alert("No fue posible eliminar la Licencia de Conducci�n.")
			})
		}
	}

/** +++++++++++++++++++++++++++++++++++++++++++++++++*/
</script>

<!--#include Virtual = "/include/DecCod.asp"-->
<!--#include Virtual="/util/dbUtil.asp"-->

<%

dim sIDPersona
dim sCodPers 
dim sIdpers
sCodPers =Request("Codice")
sIdpers=Session("creator")
sIdpers=request("txt_idpers")
%>
<br>	
	
<!--#include file="menu.asp"-->
<br>

<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
	<tr height="18">
		<td class="sfondomenu" width="67%" height="18"><span class="tbltext0"><b>&nbsp;LICENCIAS DE CONDUCCI�N</b></span></td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
		<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">(*) campo obligatorio</td>
	</tr>
</table>
<!-- Commento -->
<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
	<tr>
		<td align="left" class="sfondocomm">
			<!--Modifica delle patenti/dichiarazioni-->
			Modificaci�n de las licencias de conducci�n ya registrados. <br>
			Presione <b>Enviar</b> para guardar la modificaci�n. 
			<a href="Javascript:Show_Help('/Pgm/help/Iscr_Utente/UTE_ModAutoCert')">
			<img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
		</td>
	</tr>
	<tr height="2">
		<td class="sfondocomm" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif">
		</td>
	</tr>
</table>	
<%
sSQL = "SELECT A.COM_RIL, A.ID_PERSONA, A.COD_PERM, A.ID_PERM, A.COD_ENTE_RIL, A.DT_RILASCIO," &_
       " A.DT_SCADENZA, A.COD_STATO_RIL, A.COM_RIL, A.PRV_RIL, A.FL_ADATTAMENTI, A.DT_TMST, P.DT_NASC " &_
       " FROM AUTCERT A, PERSONA P  WHERE A.ID_PERSONA=P.ID_PERSONA  AND A.ID_PERSONA=" & sIdpers &_ 
       " AND COD_PERM =" & sCodPers
 
'PL-SQL * T-SQL  
'SSQL = TransformPLSQLToTSQL (SSQL) 
set rsPatenti = CC.Execute(sSQL)

if rsPatenti.EOF then
%>
	
	<table border="0" cellspacing="2" cellpadding="1" width="500" align="center">
		<tr align="middle">
			<td class="tbltext3">
				<b>No es posible acceder a las informaci�n requerida</b>
			</td>
		</tr>
	</table>
	
<%else
   	sIDPersona = clng(rsPatenti("ID_PERSONA"))
   	dtNasc = convdatetostring(rsPatenti("DT_NASC"))
%>
	<form method="post" id="frmModDatiAutoCert" name="frmModDatiAutoCert" onsubmit="return ControllaDati(this)" action="UTE_CnfModAutoCert.asp">
		<input type="hidden" name="txtIdPers" value="<%=sIDPersona%>">
		<input type="hidden" name="txtCodice" value="<%=sCodPers%>">
		<input type="hidden" name="txtTMST" value="<%=rsPatenti("DT_TMST")%>">
		<input type="hidden" name="txtDtNasc" value="<%=dtNasc%>">

	<table border="0" cellpadding="0" cellspacing="1" width="500">
		<tr>
			<td align="middle" colspan="2" nowrap class="tbltext1">
				<p align="left">
					<strong>&nbsp;</strong>
					<strong>Licencia de Conducci&oacute;n</strong>
					<strong>&nbsp;</strong>
				</p>
			</td>
			<td align="left" colspan="2" width="60%">
				<span class="textblack"><b>
					<%sValore = DecCodVal("TPERM",0,DATE,rsPatenti("COD_PERM"),0)%>
					<%=sValore%>
					<input type="hidden" id="text1" name="txtTipoDoc" value="<%=sValore%>">
					</b>
				</span>
			</td>
		</tr>    
		<!--jhamon<tr>
			<td align="middle" colspan="2" nowrap class="tbltext1">
				<p align="left">
					<strong>&nbsp;</strong>
					<strong>Nro. de Registro</strong>
					<strong>&nbsp;</strong>
				</p>
			</td>
			<td align="left" colspan="2" width="60%">
				<span class="tbltext">
					<input class="textblacka" style="TEXT-TRANSFORM: uppercase" maxlength="20" type="text" name="txtNDoc" value="<%=UCase(rsPatenti("ID_PERM"))%>" size="25">
					<input type="hidden" id="text1" name="txtNDocOld" value="<%=UCase(rsPatenti("ID_PERM"))%>">
	
				</span>
			</td>
	    </tr>
		<tr>     
			<td align="middle" colspan="2" nowrap class="tbltext1">
				<p align="left">
					<strong>&nbsp;</strong>
					<strong>Fecha Emisi�n</strong><font size="1"> <br>&nbsp;&nbsp;(dd/mm/aaaa)</font>
					<strong>&nbsp;</strong>
				</p>
			</td>	
			<td align="left" colspan="2" width="60%">
				<input class="textblacka" type="text" style="TEXT-TRANSFORM: uppercase" name="txtDataRil" value="<%=ConvDateToString(rsPatenti("DT_RILASCIO"))%>" size="10">
				<input type="hidden" name="txtoggi" value="<%=ConvDateToString(Date())%>">
			</td>
	    </tr>-->
	    <tr>     
			<td align="middle" colspan="2" nowrap class="tbltext1">
				<p align="left">
					<strong>&nbsp;</strong>
					<strong>Valida hasta</strong><font size="1"> <br>&nbsp;&nbsp;(dd/mm/aaaa)</font>
					<strong>&nbsp;</strong>
				</p>
			</td>	
			<td align="left" colspan="2" width="60%">
				<input type="text" class="textblacka" style="TEXT-TRANSFORM: uppercase" name="txtDataScad" value="<%=ConvDateToString(rsPatenti("DT_SCADENZA"))%>" size="10">
			</td>
		<!--jhamon<tr>
			<td colspan="4">
				&nbsp;
			 </td>
		</tr>		    
		<tr>
			<td height="2" align="left" colspan="4" background="<%=Session("Progetto")%>/images/separazione.gif">
		</tr>
		<tr>
			<td colspan="4">
				&nbsp;
			</td>
		</tr>
		<tr>
			<td colspan="4">
				<p align="left">
					<span class="textblack">
						<strong>&nbsp;</strong>
						<strong>Concedido por:</strong>
						<strong>&nbsp;</strong>
						<br>
						<br>
					</span>
				</p>
			 </td>
		</tr>			
			
	    <tr>
			<td align="middle" colspan="2" nowrap class="tbltext1">
				<p align="left">
					<strong>&nbsp;</strong>
					<strong>Ente</strong>
					<strong>&nbsp;</strong>
				</p>
			</td>
	        <td align="left" colspan="2" width="60%">
				<input class="textblacka" type="text" style="TEXT-TRANSFORM: uppercase;" value="<%=rsPatenti("COD_ENTE_RIL")%>" name="idente" maxlength="50">
	        </td>
		</tr>	    
		<tr>
			<td align="middle" colspan="2" class="tbltext1">
				<p align="left">
					<strong>&nbsp;</strong>
					<strong>Departamento</strong>
					<strong>&nbsp;</strong>
				</p>
			</td>	
			<td align="left" colspan="2" width="60%">
	  			<span class="tbltext">
<%
					dim sProv
					sProv = rsPatenti("PRV_RIL")
					
					sInt = "PROV|0|" & date & "|" & sProv & "|cmbProvRil' onchange='javascript:Pulisci()|ORDER BY DESCRIZIONE"	
				
					CreateCombo(sInt)
				
					DescrComuneResid = DescrComune(rsPatenti("COM_RIL"))
					'Response.Write 	rsPatenti("COM_RIL")		
				if  DescrComuneResid ="0" then
					DescrComuneResid = ""				
				end if 
%>					
				</span>
			</td>
		</tr>
		<tr>
			<td align="left" colspan="2" class="tbltext1">
				<p align="left">
					<strong>&nbsp;</strong>
					<strong>Municipio</strong>
					<strong>&nbsp;</strong>
				</p>
	        </td>
	        <td align="left" colspan="2" width="60%" nowrap>
				<span class="tbltext">
						<input type="text" name="txtComune" style="TEXT-TRANSFORM: uppercase;" class="textblacka" size="35" value="<%=DescrComuneResid%>">
			
					
					<input type="hidden" id="txtComuneHid" name="txtComuneHid" value="<%=rsPatenti("COM_RIL")%>">
<%
					NomeForm="frmModDatiAutoCert"
					CodiceProvincia="cmbProvRil"
					NomeComune="txtComune"
					CodiceComune="txtComuneHid"
					Cap="NO"
					
%>
					<a href="Javascript:SelComune('<%=NomeForm%>','<%=CodiceProvincia%>','<%=NomeComune%>','<%=CodiceComune%>','<%=Cap%>')" ID="imgPunto1" name="imgPunto1" onmouseover="javascript:window.status='';return true"><img border="0" src="<%=Session("Progetto")%>/images/bullet1.gif"></a>
				</span>				
				

				
			jhamon-->	
				<!--<input type="text" class="textblacka" style="TEXT-TRANSFORM: uppercase" name="txtComune" value="<%=descComune%>" size="23">				<input type="hidden" name="txtComRil" value="<%'=sComune%>">-->
			<!--jhamon	
			</td>
		</tr>
		<tr>
			<td>
				&nbsp;
			</td>
		</tr>		
		<tr>
			<td colspan="4">
				<span class="textblack">
					&nbsp;&nbsp;Si completa los campos <b>Muncipio</b> y <b>Departamento</b> debe dejar vac�o el campo <b>Pa�s</b>.<br>&nbsp;&nbsp;Si en cambio el <b>Pa�s</b> que expidio el Permiso es distinto de Argentina, debe dejar vac�os <br>&nbsp;&nbsp;los &nbsp;campos <b>Municipio</b> y <b>Departamento</b>.
				</span>
			</td>
		</tr>
		<tr>
			<td>
				&nbsp;
			</td>
		</tr>		
		<tr>
			<td align="left" colspan="2" class="tbltext1">
				<p align="left">
					<strong>&nbsp;</strong>
					<strong>Pa�s Extranjero</strong>
					<strong>&nbsp;</strong>
				</p>
			</td>   
	        <td align="left" colspan="2" width="60%">
				<span class="tbltext" nowrap>
<%
				'	dim rsNazione
				'	dim descNazione
				'	sNazione = rsPatenti("COD_STATO_RIL")
				'
				'	sInt = "STATO|0|" & date & "|" & sNazione & "|cmbNazione' onchange='javascript:PulEstero()|ORDER BY DESCRIZIONE"			
				'	CreateCombo(sInt)				
%>	
				</span>
			</td>
		</tr>
		<tr>
			<td colspan="4">
				&nbsp;
			</td>
		</tr>
		jhamon-->
	    <!--tr>			<td height="2" align="left" colspan="4" background="<%=Session("Progetto")%>/images/separazione.gif">	    </tr-->
		<tr>
			<td colspan="4">
				&nbsp;
			</td>
		</tr>		  
	</table>

	<table border="0" cellpadding="1" cellspacing="1">
		<tr>
			<td align="middle">
				<a href="javascript:document.indietro.submit()">
					<img src="<%=Session("Progetto")%>/images/indietro.gif" border="0">
				</a>
				<input type="image" src="<%=Session("Progetto")%>/images/conferma.gif" border="0" value="Modifica" onclick="return ControllaDati(this)">
				<a onmouseover="window.status =' '; return true" title="Elimina" href="javascript:elimina()">
					<img border="0" src="<%=Session("Progetto")%>/images/elimina.gif">
				</a>	
		    </td>
		</tr>
	</table>
</form>
<form name="indietro" action="UTE_VisAutCert.asp" method="post">
	<input type="hidden" name="IdPers" value="<%=sIdpers%>">			
</form>
 <%
 
end if
rsPatenti.Close
set rsPatenti = nothing
 %>
<!--#include virtual = "/include/CloseConn.asp"-->
<!--#include Virtual = "/strutt_coda2.asp"-->
			  
