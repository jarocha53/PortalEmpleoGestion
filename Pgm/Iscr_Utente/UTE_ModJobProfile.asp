<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include Virtual = "/strutt_testa2.asp"-->
<!--#include Virtual = "/include/OpenConn.asp"-->
<!--#include Virtual = "/include/ControlDateVB.asp"-->
<!--#include Virtual = "/util/portallib.asp"-->

<script type="text/javascript">

	/********************************************************
	*   @autor: IPTECHONOLOGIES                             +
	********************************************************/

	txta_Job_Prodile__length_verify = function(e){
		$("#txta_Job_Prodile_restantes").html(2000 - $(this).val().length)
		if($(this).val().length > 1999){
			$(this).val($(this).val().substring(0,1999));
		}
	}
	
	$(function(){


		var txta_Job_Prodile = $("#txta_Job_Prodile")	
		txta_Job_Prodile.keydown(txta_Job_Prodile__length_verify)
		txta_Job_Prodile.blur(txta_Job_Prodile__length_verify)
		txta_Job_Prodile.keyup(txta_Job_Prodile__length_verify)


		$("#frmModJobProfile").submit(function(){

			//Validation 
			var monthly_salary_allowance = $("#cmbMonthlySalaryAllowance").val()
			if(monthly_salary_allowance == ""){
				alert("El campo 'Aspiración salarial mensual' es obligatorio!")
				$("#cmbMonthlySalaryAllowance").focus()
				return false
			}

			// var internship_or_work_practice = $("#cmbInternshipOrWorkPractice").val()
			// if(internship_or_work_practice == ""){
			// 	alert("El campo '¿En busqueda de ...' es obligatorio!")
			// 	$("#cmbInternshipOrWorkPractice").focus()
			// 	return false
			// }			

			var teleworking = $("#cmbTeleworking").val()
			if(teleworking == ""){
				alert("El campo '¿Interesado en el Teletrabajo?' es obligatorio!")
				$("#cmbTeleworking").focus()
				return false
			}		

			var authorization_of_data_visualization = $("#cmbAuthorizationOfDataVisualization").val()
			if(authorization_of_data_visualization == ""){
				alert("El campo 'Autorización de Visualización de Datos' es obligatorio!")
				$("#cmbAuthorizationOfDataVisualization").focus()
				return false
			}		

			//
			var fields = $("#frmModJobProfile").serializeArray()
			$.post("UTE_CnfJobProfile.asp",fields,function(data){				
				alert("Modificación de Perfil Laboral correctamente efectuada.")
				document.FrmJobProfile.submit()
			})
			return false;
		})

	})
	
function ControllaDati(frmMR){
		
		//Perfil Laboral obligatorio
/*		if (frmMR.txta_Job_Prodile.value == "")	{
			alert("Perfil Laboral obligatorio")
			frmMR.txta_Job_Prodile.focus() 
			return false
		}
*/
	}			
</script>
<!--#include Virtual = "/include/DecCod.asp"-->
<!--#include Virtual="/util/dbUtil.asp"-->
<!-- #Include Virtual="/Pgm/BilancioCompetenze/Utils.asp" -->

<%

dim sIDPersona

sIdpers=Request("Idpers")

IF sIdpers ="" then
   sIdpers=Session("creator")
end if

%>

<!--#include Virtual = "/include/VerificaModificacionDatos.asp"-->

<%
PERMITEMODIF = PuedeModificar(sIdpers)

if PERMITEMODIF = 0 then
	Session("PuedeMod") = "N"
else
	Session("PuedeMod") = "S"
end if

%>

<br>

<% 
    'Especificación UTF-8
    Response.CodePage = 65001
    Response.CharSet = "utf-8"
 
	dim rsPers


	sSQL = "SELECT ID_PERSONA,COGNOME,SECONDO_COGNOME,NOME,SECONDO_NOME,DATEDIFF(DD,DT_NASC,GETDATE())AS EDAD,DT_NASC,COM_NASC,PRV_NASC,STAT_NASC,SESSO,POS_MIL,COD_FISC,STAT_CIV,STAT_CIT,STAT_CIT2,IND_RES,FRAZIONE_RES,COM_RES,PRV_RES,CAP_RES,NUM_TEL,E_MAIL,DT_TMST,DEP_NASC,LOC_NASC,DEP_RES,LOC_RES,TIPO_LIBRETA_MILITAR,JEFE_HOGAR from PERSONA WHERE ID_PERSONA=" & clng(sIdpers)

	'PL-SQL * T-SQL
	'SSQL = TransformPLSQLToTSQL (SSQL)
	set rsPers = CC.Execute(sSQL)
	
	SESSION("ESMENOR") = "FALSE" 
	if rsPers("EDAD") < 6574 then
	    	SESSION("ESMENOR") = "TRUE" 
	END IF




	sSQLExist = "SELECT JOB_PROFILE,MONTHLYSALARYALLOWANCE,INTERNSHIPORWORKPRACTICE," &_
	   "TELEWORKING,AUTHORIZATIONOFDATAVISUALIZATION "&_
	   "FROM PERSONAS_JOB_PROFILE WHERE ID_PERSONA=" & clng(sIdpers) 

	
	'sSQLExist = TransformPLSQLToTSQL (sSQLExist) 
	set rsJobProfile = CC.Execute(sSQLExist)		

	dim JOB_PROFILE
	JOB_PROFILE = ""
	dim MONTHLYSALARYALLOWANCE 
	MONTHLYSALARYALLOWANCE = ""
	dim INTERNSHIPORWORKPRACTICE
	INTERNSHIPORWORKPRACTICE = ""
	dim TELEWORKING
	TELEWORKING = ""
	dim AUTHORIZATIONOFDATAVISUALIZATION
	AUTHORIZATIONOFDATAVISUALIZATION = ""

	if not rsJobProfile.eof then  
		JOB_PROFILE = TRIM(rsJobProfile("JOB_PROFILE"))
		MONTHLYSALARYALLOWANCE = rsJobProfile("MONTHLYSALARYALLOWANCE")
		INTERNSHIPORWORKPRACTICE = rsJobProfile("INTERNSHIPORWORKPRACTICE")
		TELEWORKING = rsJobProfile("TELEWORKING")
		AUTHORIZATIONOFDATAVISUALIZATION = rsJobProfile("AUTHORIZATIONOFDATAVISUALIZATION")
	end if



%>
<form method="post" id="frmModJobProfile" name="frmModJobProfile" onsubmit="return ControllaDati(this)" action="UTE_CnfJobProfile.asp">
	<input type="hidden" id="text1" name="txtIdPers" value="<%=rsPers("ID_PERSONA")%>">
	<input type="hidden" id="text2" size="50" name="txtTMST" value="<%=rsPers("DT_TMST")%>">
	<input type="hidden" name="txtoggi" value="<%=ConvDateToString(Date())%>">

	
	<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
		<tr height="18">
      		<td class="sfondomenu" width="67%" height="18"><span class="tbltext0"><b>&nbsp;PERFIL LABORAL</b></span></td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
      		<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">(*) campo obligatorio</td>
		</tr>
	</table>
	<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
		<tr>
      		<td align="left" class="sfondocomm"> En esta secci&oacute;n puede actualizar su Perfil Laboral.<br /></td>
		</tr>
		<tr height="2">
			<td class="sfondocomm" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
		</tr>
	</table>
	<br />

	<table border="0" cellpadding="1" cellspacing="1" width="500">
		<tr>
      		<td align="left" colspan="2" nowrap class="tbltext1" colspan="2"><b>Perfil Laboral</b></td>
  		</tr>
  		<tr>
			<td align="left" colspan="2" width="60%" style="text-align:center" colspan="2">	
				<!-- <input class="textblacka" type="text" style="TEXT-TRANSFORM: uppercase;" name="txtNTELDOMOBS" size="45" maxlength="50"> -->
				<textarea id="txta_Job_Prodile" name="txta_Job_Prodile" cols ="80%" rows="7" class="textblacka" style="TEXT-TRANSFORM: uppercase;"><%=JOB_PROFILE%></textarea>
			</td>
		</tr>
		<tr>
			<td style="text-align:right; " colspan="2" class="tbltext1">
				<p>restantes: <b><span id="txta_Job_Prodile_restantes">2000</span></b></p>	
			</td>
		</tr>	
	</table>
	<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
		<tr>
			<td height="2" align="left" colspan="2" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
		<tr>
			<td colspan="2"><br></td>
		</tr>		
			
	</table>
	<table border="0" cellpadding="1" cellspacing="1" width="500">
		<tr>    
	        <td align="left" class="tbltext1">
				<span class="tbltext1">
					<strong>Aspiración salarial mensual*</strong>
				 </span>
			</td> 
	        <td align="left">
			<%				
				sInt = "MSA|0|" & date & "|" & MONTHLYSALARYALLOWANCE & "|cmbMonthlySalaryAllowance| ORDER BY CAST(CODICE AS int)"
				CreateCombo(sInt)
			%>
	        </td> 
    	</tr>	
    	<tr>    
	        <td align="left" class="tbltext1">
				<span class="tbltext1">
					<strong>¿En b&uacute;squeda de ...</strong>
				 </span>
			</td>
	        <td align="left">
			<%
				sInt = "IOWP|0|" & date & "|" & INTERNSHIPORWORKPRACTICE & "|cmbInternshipOrWorkPractice| ORDER BY CAST(CODICE AS int)"
				CreateCombo(sInt)
			%>
	        </td> 
    	</tr>
    	<tr>    
	        <td align="left" class="tbltext1">
				<span class="tbltext1">
					<strong>¿Interesado (a) en el Teletrabajo?*</strong>
				 </span>
			</td>
	        <td align="left">
			<%				
				sInt = "TW|0|" & date & "|" & TELEWORKING & "|cmbTeleworking| ORDER BY CAST(CODICE AS int)"
				CreateCombo(sInt)
			%>
	        </td> 
    	</tr>
    	<tr>    
	        <td align="left" class="tbltext1">
				<span class="tbltext1">
					<strong>Autorización de Visualización de Datos*</strong>
				 </span>
			</td>
	        <td align="left">
			<%
				sInt = "ADV|0|" & date & "|" & AUTHORIZATIONOFDATAVISUALIZATION & "|cmbAuthorizationOfDataVisualization| ORDER BY CAST(CODICE AS int)"
				CreateCombo(sInt)
			%>
	        </td> 
    	</tr>
	</table>
	<br />
	<table border="0" cellpadding="1" cellspacing="1" width="500">
		<tr>
        	<td align="middle" colspan="2">
	        	<input type="image" src="<%=Session("Progetto")%>/images/conferma.gif" border="0" value="Modifica" id="image1" name="image1">
        	</td>
    	</tr>
	</table>	
</form>