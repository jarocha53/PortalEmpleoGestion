<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->


<!--#include Virtual = "/strutt_testa2.asp"-->
<!--#include Virtual = "/include/OpenConn.asp"-->
<!--#include Virtual = "/include/ControlDateVB.asp"-->
<!--#include Virtual = "/util/portallib.asp"-->
<script LANGUAGE="Javascript">

//include del file per fare i controlli sulla validit delle date
<!--#include virtual = "/include/SelComune.js"-->
<!--#include virtual = "/include/SelDepto.js"-->
<!--#include virtual = "/include/SelLoc.js"-->

//include del file per fare i controlli sulla validit delle date
<!--#include Virtual = "/Include/ControlDate.inc"-->

//include del file per fare i controlli sulla numericit dei campi
<!--#include Virtual = "/Include/ControlNum.inc"-->

//include del file per fare i controlli sulla validit del CF
<!--#include Virtual = "/Include/ControlCodFisc.inc"-->

//include del file per fare i controlli sulle stringhe
<!--#include Virtual = "/Include/ControlString.inc"-->
<!--#include Virtual = "/Include/help.inc"-->


/********************************************************
*   @autor: IPTECHONOLOGIES                             +
********************************************************/
$(function(){
   cmbNazione_onchange()
});

/**
* load options to cmbProvNasc by cmbNazione select value in onchange event
*/
cmbNazione_onchange = function(){
    var cod_country_selected = $("#cmbNazione").val()

    var cmbProvNasc = $("#cmbProvNasc") 
    var imgPunto1  = $("#imgPunto1")
    var txtComune = $("#txtComune")

	var selected_cmbProvNasc = cmbProvNasc.val()
    var selected_Comune_display = txtComune.parent().children().filter("input").eq(0).val()
    var selected_Comune_hidden = txtComune.parent().children().filter("input").eq(1).val()

    cmbProvNasc.html("");
    if(cod_country_selected != "CO"){
    	 cmbProvNasc.attr("disabled","disabled")
    	 imgPunto1.hide()
    	 txtComune.parent().children().filter("input").val("")
    }else{
    	cmbProvNasc.removeAttr("disabled")
    	imgPunto1.show()
    }
    

	var url_load_states_by_country_code = "../GestProgetti/Alfabeta/Selezione/Utente/AJAX_load_states_by_country_code.asp?countrycode="
    $.get(url_load_states_by_country_code+cod_country_selected,function(data){
        cmbProvNasc.html(data);
        cmbProvNasc.val(selected_cmbProvNasc)

         if(cod_country_selected == "CO"){
         	   txtComune.parent().children().filter("input").eq(0).val(selected_Comune_display)
    			txtComune.parent().children().filter("input").eq(1).val(selected_Comune_hidden)
        }
     
    })
}

/**********************************************************************************************************/

/**
* @deprecated  replaced by cmbNazione_onchange()
*/
function PulisciProv()
{
    //alert ("fsdasd")
	document.frmModDatiIscri.txtComune.value = ""
	document.frmModDatiIscri.cmbProvNasc.value = ""
	document.frmModDatiIscri.txtComune.value = ""
	document.frmModDatiIscri.txtComNasc.value = ""
}
function PulisciCom()
{
	document.frmModDatiIscri.txtComune.value = ""
	document.frmModDatiIscri.txtComNasc.value = ""
	// SM inizio
	//document.frmModDatiIscri.txtDeptoNacimiento.value = ""
	//document.frmModDatiIscri.txtDepto.value = ""
	//document.frmModDatiIscri.txtLocNacimiento.value = ""
	//document.frmModDatiIscri.txtLoc.value = ""
	// SM Fine
}

function ControllaDati(frmMod){

	//-------- Controlli di OBBLIGATORIETA' --------
	//-------- Controlli FORMALI            --------
	//-------- Controlli di RELAZIONE       --------
		//Cognome
		
		frmModDatiIscri.txtCognome.value=TRIM(frmModDatiIscri.txtCognome.value)
		if ((frmModDatiIscri.txtCognome.value == "")||
			(frmModDatiIscri.txtCognome.value == " ")){
			alert("El campo Primer Apellido es obligatorio!")
			frmModDatiIscri.txtCognome.focus()
			return false
		}
		//sCognome=ValidateInputStringWithOutNumber(frmModDatiIscri.txtCognome.value)

		//if  (sCognome==false){
		//	alert("Cognome formalmente errato.")
		//	frmModDatiIscri.txtCognome.focus()
		//	return false
		//}


		//Nome
		frmModDatiIscri.txtNome.value=TRIM(frmModDatiIscri.txtNome.value)
		if ((frmModDatiIscri.txtNome.value == "")||
			(frmModDatiIscri.txtNome.value == " ")){
			alert("El campo Primer Nombre es obligatorio!")
			frmModDatiIscri.txtNome.focus()
			return false
		}

		sNome=ValidateInputStringWithOutNumber(frmModDatiIscri.txtNome.value)

		if  (sNome==false){
			alert("Primer Nombre erroneo.")
			frmModDatiIscri.txtNome.focus()
			return false

		}

		//Data di Nascita
		if (frmModDatiIscri.txtNascita.value == ""){
			alert("El campo fecha de nacimiento es obligatorio!")
			frmModDatiIscri.txtNascita.focus()
			return false
		}

		//Controllo della validit della Data di Nascita
		DataNascita = frmModDatiIscri.txtNascita.value
		if (!ValidateInputDate(DataNascita)){
			frmModDatiIscri.txtNascita.focus()
			return false
		}

		//Controllo della validit della Data di Nascita
		DataNascita = frmModDatiIscri.txtNascita.value
		if (ValidateRangeDate(DataNascita,frmModDatiIscri.txtoggi.value)==false){
			alert("La fecha de nacimiento debe ser anterior a la actual!")
			frmModDatiIscri.txtNascita.focus()
			return false
		}

		//Comune di Nascita e Provincia di Nascita obbligatori
		//e in alternativa con Nazione di Nascita
		frmModDatiIscri.txtComune.value=TRIM(frmModDatiIscri.txtComune.value)
		/*if ((frmModDatiIscri.txtComune.value == "")||
			(frmModDatiIscri.txtComune.value == " ")){
			if (frmModDatiIscri.cmbProvNasc.value == ""){
				if (frmModDatiIscri.cmbNazione.value == ""){
					alert("Los campos Municipio y Provincia o el campo Pa�s de origen (si no es argentino) son obligatorios!")
					frmModDatiIscri.txtComune.focus()
					return false
				}
			}
		}*/

		/*if (frmModDatiIscri.txtComune.value != ""){
			if (frmModDatiIscri.cmbProvNasc.value != ""){
				if(frmModDatiIscri.cmbNazione.value != ""){
					alert("Indicar unicamente el Municipio y el Departamento o por el contrario el Pa�s de origen (si no es Colombiano)!")
					frmModDatiIscri.txtComune.focus()
					return false
				}
			}
		}*/

		//Se Comune di Nascita  digitato, la Provincia  obbligatoria
		/*if (frmModDatiIscri.txtComune.value != ""){
			if (frmModDatiIscri.cmbProvNasc.value == ""){
				alert("El campo provincia de nacimiento es obligatorio!")
				frmModDatiIscri.cmbProvNasc.focus()
				return false
			}
		}*/

		//Se la Provincia di Nascita  digitata, il Comune  obbligatorio
		/*if ((frmModDatiIscri.txtComune.value == "")||
			(frmModDatiIscri.txtComune.value == " ")){
			if (frmModDatiIscri.cmbProvNasc.value != ""){
				alert("El campo municipio de nacimiento es obligatorio!")
				frmModDatiIscri.txtComune.focus()
				return false
			}
		}*/

	    //Se la Provincia di Nascita  digitata, il depto  obbligatorio
		/*if ((document.frmModDatiIscri.txtDeptoNacimiento.value == "")||
			(document.frmModDatiIscri.txtDeptoNacimiento.value == " "))
		{
			if (document.frmModDatiIscri.cmbProvNasc.value != "")
			{
				alert("El campo Departamento de nacimiento es obligatorio!")
				document.frmModDatiIscri.txtDeptoNacimiento.focus()
				return false
			}
		}*/

	    //Se la Provincia di Nascita  digitata, il localidad  obbligatorio
		/*if ((TRIM(document.frmModDatiIscri.txtDeptoNacimiento.value) != "Sin Departamento") &&
		(document.frmModDatiIscri.txtLocNacimiento.value == ""))
		{
			if (document.frmModDatiIscri.cmbProvNasc.value != "")
			{
				alert("El campo Localidad de nacimiento es obligatorio!")
				document.frmModDatiIscri.txtLocNacimiento.focus()
				return false
			}
		}*/


		
		/********************************************************
		*   @autor: IPTECHONOLOGIES                             +
		********************************************************/
		//Validation 
		var tipo_libreta = $("#ddl_tipo_libreta").val()
		if(tipo_libreta == ""){
			alert("El campo Libreta Militar es obligatorio!")
			$("#ddl_tipo_libreta").focus()
			return false
		}

		var jefe_hogar = $("#ddl_jefe_hogar").val()
		if(jefe_hogar == ""){
			alert("El campo '�Es Jefe de Hogar?' es obligatorio!")
			$("#ddl_jefe_hogar").focus()
			return false
		}


		/**********************************************************************************************************/
		//Cittadinanza
		
		if (frmModDatiIscri.cmbCittadinanza.value == ""){
			alert("El campo Nacionalidad es obligatorio!")
			frmModDatiIscri.cmbCittadinanza.focus()
			return false
		}

		if (frmModDatiIscri.cmbCittadinanza.value != "")
		{
				if (frmModDatiIscri.cmbCittadinanza.value == frmModDatiIscri.cmbCittadinanza2.value){
					alert("Las dos Nacionalidades deben ser diferentes.")
					frmModDatiIscri.cmbCittadinanza.focus()
					return false
				}
			

			//CUIL
			//Sesso = frmModDatiIscri.cmbSesso.value
			//if (document.frmModDatiIscri.txtCodFisc.value != ""){
				//CodFisc=TRIM(frmModDatiIscri.txtCodFisc.value);
				//if ( !ControllCodFisc(Sesso,CodFisc) )
					//{
					 //  document.frmModDatiIscri.txtCodFisc.focus()
					 //  return false
				//}
			//}

			//if (document.frmDomIscri.txtCodFisc.value == ""){
			
			/* 07/11/2007 DAMIAN QUITA CAMPO
					if (document.frmModDatiIscri.cmbTipoDoc.value == "")
						{
						alert("El campo tipo de documento es obligatorio!")
						document.frmModDatiIscri.cmbTipoDoc.focus()
						return false
					}
					if (TRIM(document.frmModDatiIscri.txtNumDoc.value) == "")
						{
						alert("El campo n�mero es obligatorio!")
						document.frmModDatiIscri.txtNumDoc.focus()
						return false
					}
			 07/11/2007 FIN DAMIAN */   
				
					// SM inizio
				  /*  else
					{
						if (document.frmModDatiIscri.cmbTipoDoc.value == 1)
						{

							if (frmModDatiIscri.txtCodFisc.value.substr(2,1) == "0")
							{
								if (frmModDatiIscri.txtCodFisc.value.substr(2,2) == "00")
								{
									if (document.frmModDatiIscri.txtNumDoc.value != frmModDatiIscri.txtCodFisc.value.substr(4,6))
									{
										//alert(frmDomIscri.txtCodFisc.value.substr(3,7));
										alert("El n�mero de DNI ingresado no corresponde con el ingresado el en cuil");
										return false;
									}
								}
								else
								{
									if (document.frmModDatiIscri.txtNumDoc.value != frmModDatiIscri.txtCodFisc.value.substr(3,7))
									{
										//alert(frmDomIscri.txtCodFisc.value.substr(3,7));
										alert("El n�mero de DNI ingresado no corresponde con el ingresado el en cuil");
										return false;
									}
								}
							}
							else
							{
								if (document.frmModDatiIscri.txtNumDoc.value != frmModDatiIscri.txtCodFisc.value.substr(2,8))
								{
									alert("El n�mero de DNI ingresado no corresponde con el ingresado el en cuil");
									return false;
								}
							}
						}
					}
					
					// SM fine */
					
			//if (document.frmModDatiIscri.txtProvRes.value != ""){
			 //     document.frmModDatiIscri.txtCodFisc.value=TRIM(document.frmModDatiIscri.txtCodFisc.value)
			 //     if (document.frmModDatiIscri.txtCodFisc.value == "")
			 //        {
			//	      alert("El campo Cuil es obligatorio!")
				//      document.frmModDatiIscri.txtCodFisc.focus()
			//	      return false
			   //   }
			//}

			//if (document.frmModDatiIscri.txtCodFisc.value != ""){
			 //     document.frmModDatiIscri.txtCodFisc.value=TRIM(document.frmModDatiIscri.txtCodFisc.value)
			/*
				  //Codice Fiscale deve essere di 16 caratteri
				  if (document.frmModDatiIscri.txtCodFisc.value.length != 16)
					 {
					  alert("Formato del cuil erroneo!")
				   //   document.frmModDatiIscri.txtCodFisc.focus()
					  return false
				  }

				  //Contollo la validit del Codice Fiscale
				   var contrCom =""
				  Sesso = frmModDatiIscri.cmbSesso.value
				  DataNascita = frmModDatiIscri.txtNascita.value
				  CodFisc = frmModDatiIscri.txtCodFisc.value.toUpperCase()
				  if (document.frmModDatiIscri.txtComune.value != ""){
					   contrCom =document.frmModDatiIscri.txtComNasc.value
					 }
				  else{
					  contrCom = document.frmModDatiIscri.cmbNazione.value
				  }

				  if (!ControllCodFisc(DataNascita,CodFisc,Sesso,contrCom))
					 {
					  alert("Cuil Erroneo!")
				  //    document.frmModDatiIscri.txtCodFisc.focus()
					  return false
				  }
			 */
				 // Sesso = frmModDatiIscri.cmbSesso.value
				 // CodFisc = frmModDatiIscri.txtCodFisc.value.toUpperCase()
				//if ( !ControllCodFisc(Sesso,CodFisc) )
				//	{
				//       document.frmModDatiIscri.txtCodFisc.focus()
				 //      return false
				//	}


				   /*if (document.frmModDatiIscri.cmbTipoDoc.value != "")
						{
						alert("El tipo de documento no es necesario si se ingreso el Cuil!")
						document.frmModDatiIscri.cmbTipoDoc.focus()
						return false
					}
					if (TRIM(document.frmModDatiIscri.txtNumDoc.value) != "")
						{
						alert("El tipo de documento no es necesario si se ingreso el Cuil!")
						document.frmModDatiIscri.txtNumDoc.focus()
						return false
					}

					if (document.frmModDatiIscri.txtValidita.value != "")
						{
						alert("El campo valido hasta no se completa si se ingreso el Cuil!")
						document.frmModDatiIscri.txtValidita.focus()
						return false
					}   */
			//}

			//Sesso Uomo, Posizione Militare obbligatoria
			/*if (frmModDatiIscri.cmbSesso.value == "M"){
				if (frmModDatiIscri.cmbLeva.value == ""){
					alert("El campo posici�n de reclutamiento es obligatorio!")
					frmModDatiIscri.cmbLeva.focus()
					return false
				}
			}*/

			//Sesso Donna senza Posizione Militare
			//if (frmModDatiIscri.cmbSesso.value == "F"){
			//	if (frmModDatiIscri.cmbLeva.value != ""){
			//		alert("Il campo Posizione di Leva  obbligatorio solo per soggetti di sesso maschile.")
			//		frmModDatiIscri.cmbLeva.focus()
			//		return false
			//	}
		//	}

		//	if (document.frmModDatiIscri.txtCodFisc.value == "")
		//	{
			//        if (document.frmModDatiIscri.cmbTipoDoc.value == "")
			//        {
			//	        alert("El campo tipo de documento es obligatorio!")
			//	        document.frmModDatiIscri.cmbTipoDoc.focus()
			//	        return false
			//        }
			//        if (TRIM(document.frmModDatiIscri.txtNumDoc.value) == "")
			 //       {
			//	        alert("El campo numero es obligatorio!")
			//	        document.frmModDatiIscri.txtNumDoc.focus()
			//	        return false
			  //      }
			// }else
			//  {
					//alert (document.frmModDatiIscri.cmbTipoDoc.value);
					//return false;
			//	if (document.frmModDatiIscri.cmbTipoDoc.value == 1){
			//
			//		if (frmModDatiIscri.txtCodFisc.value.substr(2,1) == "0")
			//		{
			//			if (document.frmModDatiIscri.txtNumDoc.value != frmModDatiIscri.txtCodFisc.value.substr(3,7))
			//			{
			//				//alert(frmDomIscri.txtCodFisc.value.substr(3,7));
			//				alert("El n�mero de DNI ingresado no corresponde con el ingresado el en cuil");
			//				return false;
			//			}
			//		}else{
			//
			//			if (document.frmModDatiIscri.txtNumDoc.value != frmModDatiIscri.txtCodFisc.value.substr(2,8))
			//			{
			//				alert("El n�mero de DNI ingresado no corresponde con el ingresado el en cuil");
			//				return false;
			//			}
			//		}
			//	}
			//   }
					/*if (document.frmModDatiIscri.txtValidita.value == "")
						{
						alert("El campo valido hasta es obligatorio!")
						document.frmModDatiIscri.txtValidita.focus()
						return false
					}*/


			/*DataNascita = document.frmModDatiIscri.txtValidita.value
			if (document.frmModDatiIscri.txtValidita.value != ""){
				 //Controllo della validit della Data di Nascita

				 if (!ValidateInputDate(DataNascita)){
					 document.frmModDatiIscri.txtValidita.focus()
					 return false
				 }
			}*/

			/*if (document.frmModDatiIscri.txtValidita.value != ""){
				 //Controllo della validit della Data di Nascita
				 DataNascita = frmModDatiIscri.txtValidita.value
				 if (ValidateRangeDate(frmModDatiIscri.txtoggi.value,DataNascita)==false){
					alert("La fecha valido hasta debe ser superior a la actual!")
					frmModDatiIscri.txtValidita.focus()
					return false
				 }
			}*/

			//Email

			if (frmModDatiIscri.txtEmail.value != ""){

				 pippo=ValidateEmail(frmModDatiIscri.txtEmail.value)

				 if  (pippo==false){
					 alert("La direcci�n de email es incorrecta.")
					 frmModDatiIscri.txtEmail.focus()
					 return false
				 }
			}
		
			if ("<%=Session("PuedeMod")%>" != "N") {

				//Se non era presente utente e password e lo sta creando ora
				if (frmModDatiIscri.pswdb.value == "" && (frmModDatiIscri.txtLogin.value != "" || frmModDatiIscri.oldpassword.value != "" || frmModDatiIscri.newpassword.value !="" || frmModDatiIscri.rpassword.value !="" )){
					/*if (frmModDatiIscri.oldpassword.value != ""){
						alert("El campo contrase�a en uso no debe ser digitato!")
						frmModDatiIscri.oldpassword.focus() 
						return false
					}*/
					if (frmModDatiIscri.newpassword.value != ""){
						if (frmModDatiIscri.txtLogin.value == ""){
							alert("El campo Usuario es obligatorio si se digit� la contrase�a!")
							frmModDatiIscri.txtLogin.focus() 
							return false
						}
					}
							//Se Usuario � digitato, la password � obbligatoria
					if (frmModDatiIscri.txtLogin.value != ""){
						if (frmModDatiIscri.newpassword.value == ""){
							alert("El campo Nueva contrase�a es obligatorio si se digit� el Usuario!")
							frmModDatiIscri.newpassword.focus() 
							return false
						}
					}
					frmModDatiIscri.rpassword.value=TRIM(frmModDatiIscri.rpassword.value)
					if (frmModDatiIscri.rpassword.value == "") {
						alert("Reingresar la nueva contrase�a");
						frmModDatiIscri.rpassword.focus();
						return false;
					}
					if (frmModDatiIscri.rpassword.value.toUpperCase() == frmModDatiIscri.newpassword.value.toUpperCase()) {
					}else{
						alert("La contrase�a reingresada no coincide con la primera");
						frmModDatiIscri.newpassword.focus();
						return false;
					}
				}
				
				//frmModDatiIscri.oldpassword.value=TRIM(frmModDatiIscri.oldpassword.value)
				//
				//se aveva gi� una password ed adesso la vuole cambiare
				//alert (frmModDatiIscri.pswdb.value != "" || (frmModDatiIscri.oldpassword.value != "" || frmModDatiIscri.newpassword.value !="" || frmModDatiIscri.rpassword.value !="")); 
				
				if ( frmModDatiIscri.newpassword.value !="" || frmModDatiIscri.rpassword.value !=""){
					/*if (frmModDatiIscri.oldpassword.value == "") {
						alert("Ingresar la Contrase�a actual");
						frmModDatiIscri.oldpassword.focus();
						return false;
					}*/
					/*
					if (frmModDatiIscri.oldpassword.value.toUpperCase()==op.toUpperCase()) {
					}else{
						alert("La contrase�a ingresada no es la correcta");
						frmModDatiIscri.oldpassword.focus();
						return false;
					}*/
					
					frmModDatiIscri.newpassword.value =TRIM(frmModDatiIscri.newpassword.value)
				
					if (!ChechSingolChar(frmModDatiIscri.newpassword.value,"'"))
					{
					alert ("No es valida una contrase�a con espacios");
					frmModDatiIscri.newpassword.value = ""
					frmModDatiIscri.rpassword.value = ""
					frmModDatiIscri.newpassword.focus();
					return false;
					}

					if (!ValidateInputStringIsValid(frmModDatiIscri.newpassword.value))
					{
					frmModDatiIscri.newpassword.value = ""
					frmModDatiIscri.rpassword.value = ""
					frmModDatiIscri.newpassword.focus();
					return false;
					}
					if (frmModDatiIscri.newpassword.value == "") {
						alert("Ingresar la nueva contrase�a");
						frmModDatiIscri.newpassword.focus();
						return false;
					}
					/*if (TRIM(frmModDatiIscri.newpassword.value.toUpperCase()) == TRIM(frmModDatiIscri.pswdb.value.toUpperCase())) {
						alert("La Nueva Contrase�a debe ser diferente a la actual");
						frmModDatiIscri.newpassword.value = ""
						frmModDatiIscri.rpassword.value = ""
						frmModDatiIscri.newpassword.focus();
						return false;
					}*/
					
					frmModDatiIscri.rpassword.value=TRIM(frmModDatiIscri.rpassword.value)
					if (frmModDatiIscri.rpassword.value == "") {
						alert("Reingresar la nueva contrase�a");
						frmModDatiIscri.rpassword.focus();
						return false;
					}
					if (frmModDatiIscri.rpassword.value.toUpperCase() == frmModDatiIscri.newpassword.value.toUpperCase()) {
					}else{
						alert("La contrase�a reingresada no coincide con la primera");
						frmModDatiIscri.newpassword.focus();
						return false;
					}			
					if (frmModDatiIscri.newpassword.value.length < 6 ) {
						alert("La nueva contrase�a debe ser de al menos 6 caracteres");
						frmModDatiIscri.newpassword.focus();
						return false;
					}
					/*if (TRIM(frmModDatiIscri.oldpassword.value.toUpperCase()) != TRIM(frmModDatiIscri.pswdb.value.toUpperCase())) {
						alert("La Contrase�a ingresada no coincide con la actual");
						frmModDatiIscri.oldpassword.value = ""
						frmModDatiIscri.oldpassword.focus();
						return false;
					}*/
				}
			}
		}
	    return true
	}
	
	
</script>

<!--#include Virtual = "/include/DecCod.asp"-->
<!--#include Virtual="/util/dbUtil.asp"-->

<%

dim sIDPersona

'sIdpers=Request.QueryString("Idpers")
sIdpers=Request("Idpers")

IF sIdpers ="" then
   sIdpers=Session("creator")
end if

'response.Write sIdpers

%>

<!--#include Virtual = "/include/VerificaModificacionDatos.asp"-->

<%
PERMITEMODIF = PuedeModificar(sIdpers)

if PERMITEMODIF = 0 then
	Session("PuedeMod") = "N"
else
	Session("PuedeMod") = "S"
end if

%>

<br>
<!--#include file="menu.asp"-->
<!--	<%'agregado

            sSQL1="SELECT NOME,COGNOME FROM PERSONA WHERE ID_PERSONA=" & sIdPers
'PL-SQL * T-SQL
SSQL1 = TransformPLSQLToTSQL (SSQL1)
            set rsNominativo =CC.execute(sSQL1)

            %>
            <TABLE width=95% bordercolor=#FFFFFF border=1 cellspacing=1 cellpadding=0>
	             <TR height="16" class="tblsfondo">
	                 <FORM id=form1 name=form1>
			             <TD width="45%" height="18" align="center" bgcolor="#FFFFFF">
			                 <SPAN class="tbltextmenu2">
			                   <B>Informaci�n relativa a:&nbsp;&nbsp;</B>			                 </SPAN>
			                 <SPAN class="textred">
			                   <B><%=rsNominativo("NOME")%>&nbsp;<%=rsNominativo("COGNOME")%></B>
		                   </SPAN>
		               </TD>
		             </FORM>
	            </TR>
	       </TABLE>

			<%
          set rsNominativo = nothing
          %>
    -->
     	<br>
	<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
		<tr height="18">

    <td class="sfondomenu" width="67%" height="18"><span class="tbltext0"><b>DATOS PERSONALES</b></span></td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>

    <td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">(*)
      campo obligatorio</td>
		</tr>
	</table>

	<!-- Commento -->

	<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
		<tr>

    <td align="left" class="sfondocomm">Una vez corrobore que los datos sean correctos, presione en <b>enviar</b> para grabar la informaci&oacute;n.
      
      <!--NUEVO AGREGADO POR F.BASANTA -->
      <br>
      <img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0" onclick="Javascript:window.open('ayuda_datospersonales.htm','Ayuda','width=500,height=350,Resize=No,Scrollbars=yes')">

      <!--<a href="Javascript:Show_Help('/Pgm/help/Iscr_Utente/UTE_ModDati')" name onmouseover="javascript:status='' ; return true">
      <img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a> -->
    </td>
		</tr>
		<tr height="2">
			<td class="sfondocomm" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>

<%
CC.begintrans
InsProj clng(sIdpers),mid(session("progetto"),2),session("idutente"),CC
CC.CommitTrans
%>

<% 
dim rsPers


sSQL = "SELECT ID_PERSONA,COGNOME,SECONDO_COGNOME,NOME,SECONDO_NOME,DATEDIFF(DD,DT_NASC,GETDATE())AS EDAD,DT_NASC,COM_NASC,PRV_NASC,STAT_NASC,SESSO,POS_MIL,COD_FISC,STAT_CIV,STAT_CIT,STAT_CIT2,IND_RES,FRAZIONE_RES,COM_RES,PRV_RES,CAP_RES,NUM_TEL,E_MAIL,DT_TMST,DEP_NASC,LOC_NASC,DEP_RES,LOC_RES,TIPO_LIBRETA_MILITAR,JEFE_HOGAR from PERSONA WHERE ID_PERSONA=" & clng(sIdpers)


'PL-SQL * T-SQL
'SSQL = TransformPLSQLToTSQL (SSQL)
	set rsPers = CC.Execute(sSQL)
	
	SESSION("ESMENOR") = "FALSE" 
	if rsPers("EDAD") < 6574 then
	    	SESSION("ESMENOR") = "TRUE" 
	END IF

	set rsdepar = server.CreateObject("adodb.recordset")
	set rsloca = server.CreateObject("adodb.recordset")

	if not isnull(rsPers("DEP_NASC")) then
		sql2 = "Select * from DEPARTAMENTO WHERE PROVINCIA = '" & rsPers("PRV_NASC") & "' and CODDEPT = " & clng(rsPers("DEP_NASC"))
'PL-SQL * T-SQL
'SQL2 = TransformPLSQLToTSQL (SQL2)
		set rsdepar = CC.execute(sql2)

		if not rsdepar.eof then
			descdepto = rsdepar("DESCRIPCION")
			coddepto = rsdepar("CODDEPT")
			'provinciaarg = rsdepar("PROVINCIA")
		end if
	end if
	
	if not isnull(rsPers("LOC_NASC"))then
        if rsdepar.EOF then
            prov = 0
        else
            prov = rsdepar("PROVINCIA")
        end if

		sql3 = "Select * from LOCALIDAD WHERE PROVINCIA = " & prov & " and DEPTO = " & clng(rsPers("DEP_NASC")) & " and CODLOC = " & clng(rsPers("LOC_NASC"))


'PL-SQL * T-SQL
'SQL3 = TransformPLSQLToTSQL (SQL3)

		set rsloca = CC.execute(sql3)

		if not rsloca.eof then
			descloc = rsloca("DESCRIPCION")
			codloc = rsloca("CODLOC")
		end if
	end if
	
	sql2 = "Select LOGIN, PASSWORD from UTENTE WHERE CREATOR= " & rsPers("ID_PERSONA") 
	set rsute = CC.execute(sql2)
	if not rsute.eof then
		sLogin = rsute("LOGIN")
		sPasswd = rsute("PASSWORD")
		'response.write "<br>sLogin " & sLogin 
		'response.write "<br>sLogin " & sLogin 
	end if

	set rsdepar = nothing
	set rsloca = nothing
	set rsute = nothing
if rsPers.EOF then



%>

	<table border="0" cellspacing="2" cellpadding="2" width="500">
		<tr align="middle">

    <td class="tbltext3"> <b>No es posible ingresar a la informaci�n personal
      de la persona</b></td>
		</tr>
	</table>

<% else 
    sIDPersona = clng(rsPers("ID_PERSONA")) %>

   	<form method="post" name="frmModDatiIscri" onsubmit="return ControllaDati(this)" action="UTE_CnfModDati.asp">
		<input type="hidden" id="text1" name="txtIdPers" value="<%=rsPers("ID_PERSONA")%>">
		<input type="hidden" id="text2" size="50" name="txtTMST" value="<%=rsPers("DT_TMST")%>">
	    <input type="hidden" name="txtoggi" value="<%=ConvDateToString(Date())%>">
	<table border="0" cellpadding="2" cellspacing="2" width="500" style="text-align:left;">
	    <tr>
			<td align="middle" colspan="2" nowrap class="tbltext1">
				<p align="left"> <strong></strong> <span class="tbltext1"><strong>Primer Nombre*</strong> </span>
        <strong>&nbsp;</strong> </p>			</td>
			<td align="left" colspan="2">
				<!--<span class="tbltext">-->

				<%
					'Response.Write Session("Progetto")
					'Response.Write "<br>"
					'if Session("Progetto") = "/SPI" then
					if ucase(Session("PuedeMod")) = "N" then
				%>
						<input style="TEXT-TRANSFORM: uppercase" class="textblacka" type="text" maxlength="50" name="txtNome" value="<%=UCase(rsPers("NOME"))%>" size="35">
						
				<%
					else
				%>
						<input style="TEXT-TRANSFORM: uppercase" class="textblacka" type="text" maxlength="50" name="txtNome" value="<%=UCase(rsPers("NOME"))%>" size="35">
				<%
					end if
				%>
					<input type="hidden" id="text1" name="txtNomeOld" value="<%=UCase(rsPers("NOME"))%>">

				<!--</span>-->			</td>
	    </tr>
	    <tr>
	    		<td align="middle" colspan="2" nowrap class="tbltext1">
					<p align="left"> 
						<span class="tbltext1"><strong>Segundo Nombre</strong> </span>
        		 	</p>			
    			</td>
				<td align="left" colspan="2">
					<input type="text" style="TEXT-TRANSFORM: uppercase" class="textblacka" id="text1" name="txt_secondo_nome" size="35" maxlength="50"  value="<%=UCase(rsPers("SECONDO_NOME"))%>">
				</td>
		</tr>
	<tr>
			<td align="middle" colspan="2" nowrap class="tbltext1">
				<p align="left"> <strong></strong> <span class="tbltext1"><strong>Primer Apellido*</strong> </span>
        <strong>&nbsp;</strong> </p>			</td>
			<td align="left" colspan="2">
				<%
					'Response.Write Session("Progetto")
					'Response.Write "<br>"
					'if Session("Progetto") = "/SPI" then

					if ucase(Session("PuedeMod")) = "N" then
				%>
						<input style="TEXT-TRANSFORM: uppercase;" class="textblacka" type="text" maxlength="50" name="txtCognome" value="<%=UCase(rsPers("COGNOME"))%>" size="35">
				<%
					else
				%>
						<input style="TEXT-TRANSFORM: uppercase;" class="textblacka" type="text" maxlength="50" name="txtCognome" value="<%=UCase(rsPers("COGNOME"))%>" size="35">
				<%
					end if
				%>
				<input type="hidden" id="Hidden1" name="txtCognOld" value="<%=UCase(rsPers("COGNOME"))%>">			</td>
	    </tr>
	    <tr>
			<td align="middle" colspan="2" nowrap class="tbltext1">
				<p align="left"> <strong></strong> <span class="tbltext1"><strong>Segundo Apellido</strong> </span>
        <strong>&nbsp;</strong> </p>			</td>
			<td align="left" colspan="2">
				<%
					'Response.Write Session("Progetto")
					'Response.Write "<br>"
					'if Session("Progetto") = "/SPI" then

					if ucase(Session("PuedeMod")) = "N" then
				%>
						<input style="TEXT-TRANSFORM: uppercase" class="textblacka" type="text" maxlength="50" name="txt_secondo_cognome" value="<%=UCase(rsPers("SECONDO_COGNOME"))%>" size="35">
				<%
					else
				%>
						<input style="TEXT-TRANSFORM: uppercase;" class="textblacka" type="text" maxlength="50" name="txt_secondo_cognome" value="<%=UCase(rsPers("SECONDO_COGNOME"))%>" size="35">
				<%
					end if
				%>
				<input type="hidden" id="Hidden1" name="txtCognOld" value="<%=UCase(rsPers("COGNOME"))%>">			</td>
	    </tr>
	   <!-- <tr>
          <td align="middle" colspan="2" nowrap="nowrap" class="tbltext1">&nbsp;</td>
	      <td align="left" colspan="2">&nbsp;</td>
      </tr>-->
	    <tr>
			<td align="middle" colspan="2" nowrap class="tbltext1">
				<p align="left"> <strong></strong> <span class="tbltext1"><strong>Fecha de Nacimiento*</strong></span><font size="1"> (dd/mm/aaaa)</font> <strong>&nbsp;</strong> </p>			</td>
			<td align="left" colspan="2">
				<% 'if ucase(Session("PuedeMod")) = "N" then%>
<!-- 					<input type="text" style="TEXT-TRANSFORM: uppercase" readonly class="textblacka" name="txtNascita" value="<%=ConvDateToString(rsPers("DT_NASC"))%>" size="10" maxlength="10">			</td>
 -->				<%'else%>
					<input type="text" style="TEXT-TRANSFORM: uppercase" class="textblacka fecha" name="txtNascita" value="<%=ConvDateToString(rsPers("DT_NASC"))%>" size="10" maxlength="10">			</td>
				<%'end if%>
	    </tr>
	    <tr>
	         <td>
	            <br>	      	</td>
	    </tr>
	    <tr>
          <td height="2" align="left" colspan="4" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
      </tr>
	    <tr>
          <td align="middle" colspan="2" nowrap="nowrap" class="tbltext1"><p align="left"> <strong></strong> <span class="tbltext1"><strong>Sexo</strong></span></p></td>
	      <td align="left" colspan="2"><input type="hidden" name="cmbSesso" id="cmbSesso" value="<%=rsPers("SESSO")%>" />
              <%
              select case rsPers("SESSO")
					case "M"%>
              <span class="tbltext">
		          <select class="textblacka" id="cmbSesso2" name="cmbSesso2">
		            <option value="M" selected="selected">MASCULINO </option>
		            <option value="F">FEMENINO </option>
		          </select>
              <%'Response.Write ("<B>MASCULINO</B>")%>
              </span>
              <%case "F"%>
              <span class="tbltext">
		          <select class="textblacka" id="cmbSesso2" name="cmbSesso2">
		            <option selected="selected" value="F">FEMENINO </option>
		            <option value="M">MASCULINO </option>
		          </select>
              <%'Response.Write ("<B>FEMENINO</B>")%>
              </span>
              <%end select%>         
              </td>

      </tr>
	    <tr>
          <td colspan="4"><!--span class="textblack">			In caso la <b>Nazione di Nascita</b> fosse differente dall'Italia si deve specificare nell'apposito campo sottostante e si devono lasciare i campi <b>Comune di Nascita</b> e <b>Provincia di Nascita</b> VUOTI. Se invece la nazione di nascita &egrave; l'Italia si deve lasciare il campo <b>Nazione di Nascita</b> VUOTO.			</span-->
              <%
              
              'carica tutti gli identificativi della persona differenti dall'ultimo registrato nella tabella persona 
				sSQL = "SELECT descrizione,COD_DOCST,COD_FISC FROM PERS_TIPO_DOC, TADES WHERE ID_PERSONA=" &_
				                     clng(sIdpers) & "and COD_DOCST+COD_FISC<>'" & UCase(rsPers("COD_FISC")) &_
				                      "' and NOME_TABELLA='DOCST' and CODICE=COD_DOCST and " &_
				                      " DECORRENZA<" & ConvDateToDb(now) & "AND SCADENZA>" & ConvDateToDb(now)
			set	rsDocumento= CC.execute(sSQL)

			if not rsDocumento.EOF then%>
         </td>
      </tr>
	    <tr  class="sfondocomm">
            <td align="left" colspan="2" class="tbltext1"><strong></strong> <span class="tbltext1"><strong>Otros Documento Ingresado</strong> </span></td>
	        <td align="left" colspan="2">
	      		    <table border="1">		<%	end if %>
		
<%
	         
	                'response.write "efwwerfwaer.." & rsDocumento.EOF & "..efwwerfwaer" & ucase(Session("PuedeMod")) & "..efwwerfwaer" 
				         if  UCase(rsPers("COD_FISC")) <> "" then
    			             tipodoc=left(UCase(rsPers("COD_FISC")),2)
    			        end if
    			        ctloop=0        
				        do while not rsDocumento.EOF%>

                 	                <tr>
               	                        <td align="left" colspan="2" class=tblagg>
                	                        <%=rsDocumento("descrizione") & " - " & rsDocumento("COD_FISC")%>
				                        </td>
                	                </tr>
					        <%rsDocumento.MoveNext
					        ctloop=1
				        loop
				if ctloop=1 then%>
				        </table>      
		    </td> 
		    </td>
		</tr>
         <%end if %>
         <tr>		   
				<td align="left" colspan="2" class="tbltext1"><strong></strong> <span class="tbltext1"><strong>Libreta Militar*</strong> </span></td>
	            <td align="left" colspan="2"><%


				' PTLM => PERS_TIPO_LIBRETA_MILITAR 
			    sInt = "PTLM|0|" & date & "|" & rsPers("TIPO_LIBRETA_MILITAR") & "|ddl_tipo_libreta| "
				CreateCombo(sInt)
				%>
                      <input type="hidden" size="50" name="txtTipoDocOld" value="<%=tipodoc%>" />              

				          </td>
      </tr>  
		<tr>		   
				<td align="left" colspan="2" class="tbltext1"><strong></strong> <span class="tbltext1"><strong>Tipo Documento*</strong> </span></td>
	            <td align="left" colspan="2"><%

' di seguito impostiamo la cedula de identidad presente nella base dati nella tabella PERSONA
				         'response.Write "dsafd" & UCase(rsPers("COD_FISC"))
						if ucase(Session("PuedeMod")) = "N" then
						    sInt = "DOCST|0|" & date & "|" & tipodoc & "|proctTipoDoc| and valore in('01','03')ORDER BY DESCRIZIONE"
						    CreateComboSoloLectura(sInt)%>
						    <input type="hidden" name="cmbTipoDoc" value="<%=tipodoc%>" /><%
						else
						    'response.write "codigo.." & rsDocumento("COD_DOCST") & ".." 
						    sInt = "DOCST|0|" & date & "|" & tipodoc & "|cmbTipoDoc| and valore in('01','03') ORDER BY DESCRIZIONE"
						    CreateCombo(sInt)
						end if
				%>
                      <input type="hidden" size="50" name="txtTipoDocOld" value="<%=tipodoc%>" />              

				          </td>
      </tr>   
      <tr>
        <td align="left" colspan="2" class="tbltext1"><p align="left"> <strong></strong> <span class="tbltext1"><strong>N&uacute;mero*</strong></span> <strong>&nbsp;</strong> </p></td>
	    <td align="left" colspan="2"><span class="tbltext">
          <%if  UCase(rsPers("COD_FISC")) <> "" then
          codfisc=mid(UCase(rsPers("COD_FISC")),3) %>
          <input type="hidden" name="txtCodFiscOld" value="<%=codfisc%>" />
          <input style="TEXT-TRANSFORM: uppercase" type="text" class="textblacka" name="txtCodFisc" maxlength="14" size="14" value="<%=codfisc%>" 
                  <%'controllo se l'utente puo modificare il codice
                  if ucase(Session("PuedeMod")) = "N" then
                  response.write "readonly"
                  end if%>  
                    />
          <%
            else
          %>
          <input style="TEXT-TRANSFORM: uppercase" type="text" class="textblacka" name="txtCodFisc" maxlength="16" size="16" />
          <%end if
          %>
        </span> </td>
      </tr>
	  <tr>
          <td align="left" colspan="2" class="tbltext1">&nbsp;</td>
	      <td align="left" colspan="2">&nbsp;</td>
      </tr>
	    <tr>
          <td height="2" align="left" colspan="4" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
      </tr>
   <tr>
<!--jhamon
    <td colspan="4"> <span class="textblack"> Para los extranjeros especificar el pa�s de nacimiento</span><br>
      <br>	     	</td>
	    </tr>-->

	    <tr>
          <td align="left" colspan="2" class="tbltext1"><p align="left"> <span class="tbltext1"><strong>Pa&iacute;s de Nacimiento* </strong></span><strong></strong> </p></td>
	      <td align="left" colspan="2"><span class="tbltext">
	      
	      
	      
              
              <%
              
				sInt = "STATO|0|" & date & "|" & rsPers("STAT_NASC") & "|cmbNazione' onchange='cmbNazione_onchange()|ORDER BY DESCRIZIONE"
				CreateCombo(sInt)
				
				
				%>
          
	      <!--jhamon
            <%
				dim rsNazione
				dim descNazione
				sNazione = rsPers("STAT_NASC")
                Response.Write "jhamon" & sNazione
				set rsComune = Server.CreateObject("ADODB.Recordset")

				sSQL = "SELECT CODCOM,DESCOM from COMUNE WHERE CODCOM like 'Z%' " &_
					"ORDER BY DESCOM"

'PL-SQL * T-SQL
'SSQL = TransformPLSQLToTSQL (SSQL)
				rsComune.Open sSQL, CC

				Response.Write "<SELECT ID=cmbNazione  name=cmbNazione class=textblacka>"
				Response.Write "<OPTION></OPTION>"

				do while not rsComune.EOF
					if rsComune("CODCOM") = sNazione then
						Response.Write "<OPTION SELECTED "
						Response.write "value ='" & rsComune("CODCOM") & _
							"'> " & rsComune("DESCOM")  & "</OPTION>"
					else
						Response.Write "<OPTION "
						Response.write "value ='" & rsComune("CODCOM") & _
							"'> " & rsComune("DESCOM")  & "</OPTION>"
					end if
					rsComune.MoveNext
				loop

				Response.Write "</SELECT>"

				rsComune.Close
				

				%>
				-->
            </span>
              <input type="hidden" name="txtProvRes" value="<%=rsPers("PRV_RES")%>" />
              <% 'rsDocumento.Close
    set rsDocumento= nothing %>          </td>
      </tr>
	    <tr>
          <td align="left" colspan="2" class="tbltext1">&nbsp;</td>
	      <td align="left" colspan="2">&nbsp;</td>
      </tr>
	    <tr>

    <td colspan="4"> <span class="textblack"> Para los nacidos en Colombia ingresar
      el Departamento y Municipio de nacimiento</span><br>
      <br>	     	</td>
	    </tr>
	    <tr>
	        <td align="left" colspan="2" nowrap class="tbltext1">
				<p align="left"> <strong></strong> <span class="tbltext1"><strong>Departamento de Nacimiento </strong></span><strong>&nbsp;</strong> </p>	        </td>
	        <td align="left" colspan="2">
				<span class="tbltext">
				<%
				dim rsProv
				dim descProv
				sProv = rsPers("PRV_NASC")

				sInt = "PROV|0|" & date & "|" & rsPers("PRV_NASC") & "|cmbProvNasc' onchange='PulisciCom()| and valore = '" & rsPers("STAT_NASC") & "' ORDER BY DESCRIZIONE"
				CreateCombo(sInt)
				
				

				%>
				</span>	        </td>
	    </tr>
	    <tr>
			<td align="middle" colspan="2" nowrap class="tbltext1">
				<p align="left"> <strong></strong> <span class="tbltext1"><strong>Municipio de Nacimiento </strong></span><strong></strong> </p>			</td>
	  		<td nowrap>
	  		    <%
				dim rsComune

				sComune = rsPers("COM_NASC")

				if sComune <> "" then
					sSQL = "SELECT CODCOM,DESCOM from COMUNE WHERE CODCOM = '" & sComune & "'"
			        'Response.Write sSQL
'PL-SQL * T-SQL
'SSQL = TransformPLSQLToTSQL (SSQL)
			        set rsComune = CC.Execute(sSQL)

					descComune = rsComune("DESCOM")

					rsComune.Close

				    set rsComune = nothing
				end if%>
				<span class="tbltext">
				<input type="text" style="TEXT-TRANSFORM: uppercase" class="textblacka" name="txtComune" id="txtComune" value="<%=descComune%>" size="35" readonly>
				<input type="hidden" name="txtComNasc" value="<%=sComune%>">
<%
				NomeForm="frmModDatiIscri"
				CodiceProvincia="cmbProvNasc"
				NomeComune="txtComune"
				CodiceComune="txtComNasc"
				Cap="NO"
%>
				<a href="Javascript:SelComune('<%=NomeForm%>','<%=CodiceProvincia%>','<%=NomeComune%>','<%=CodiceComune%>','<%=Cap%>')" ID="imgPunto1" name="imgPunto1" onmouseover="javascript:window.status='';return true"><img border="0" src="<%=Session("Progetto")%>/images/bullet1.gif"></a>				</span>			</td>
	    </tr>
<!-- SM inizio
<tr>
      <td align="left" nowrap colspan="2" class="tbltext1"> <strong></strong><b>Departamento de Nacimiento</b></td>
			<td nowrap>
				<span class="tbltext">
				<input type="text" name="txtDeptoNacimiento" style="TEXT-TRANSFORM: uppercase;" class="textblacka" size="35" readonly value="<%=descdepto%>">
				<input type="hidden" name="txtDepto" value="<%=coddepto%>">

<%

		NomeForm="frmModDatiIscri"
		CodiceProvincia="cmbProvNasc"
		NomeDepto="txtDeptoNacimiento"
		CodiceDepto="txtDepto"
		CodigoDepto = ""
		CasillaLocNac ="txtLocNacimiento"
		CasillaCodLocNac ="txtLoc"
		%>
				<a href="Javascript:SelDepto('<%=NomeForm%>','<%=CodiceProvincia%>','<%=NomeDepto%>','<%=CodiceDepto%>','<%=CodigoDepto%>','<%=CasillaLocNac%>','<%=CasillaCodLocNac%>')" ID="imgPunto1" name="imgPunto1" onmouseover="javascript:window.status='';return true"><img border="0" src="<%=Session("Progetto")%>/images/bullet1.gif"></a>				</span>			</td>

		</tr>

<tr>
      <td align="left" nowrap colspan="2" class="tbltext1"> <strong></strong><b>Localidad de Nacimiento</b></td>
			<td nowrap>
				<span class="tbltext">
				<input type="text" name="txtLocNacimiento" style="TEXT-TRANSFORM: uppercase;" class="textblacka" size="35" readonly value="<%=descloc%>">
				<input type="hidden" name="txtLoc" value="<%=codloc%>">

<%
				NomeForm="frmModDatiIscri"
				CodiceProvincia="cmbProvNasc"
				NomeLoc="txtLocNacimiento"
				CodiceLoc="txtLoc"
				CodigoDepto="txtDepto"
%>
				<a href="Javascript:SelLoc('<%=NomeForm%>','<%=CodiceProvincia%>','<%=NomeLoc%>','<%=CodiceLoc%>','<%=CodigoDepto%>')" ID="imgPunto1" name="imgPunto1" onmouseover="javascript:window.status='';return true"><img border="0" src="<%=Session("Progetto")%>/images/bullet1.gif"></a>				</span>			</td>
		</tr>
	    <tr>
			<td colspan="2">&nbsp;			</td>
	    </tr>

'''  SM fine -->
		<tr>

          <td colspan="4">
  	    <tr>
          <td height="2" align="left" colspan="4" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
      </tr>
  	    <tr>
          <td align="left" colspan="2" class="tbltext1"><p align="left"> <strong></strong> <span class="tbltext1"><strong>Nacionalidad I*</strong> </span><strong>&nbsp;</strong> </p></td>
  	      <td align="left" colspan="2"><p align="left">
              <%
				sInt = "STATO|0|" & date & "|" & rsPers("STAT_CIT") & "|cmbCittadinanza|ORDER BY DESCRIZIONE"
				CreateCombo(sInt)

				%>
          </p></td>
      </tr>
  	    <tr>
          <td align="left" colspan="2" class="tbltext1"><p align="left"> <strong></strong> <span class="tbltext1"><strong>Nacionalidad II</strong></span> <strong>&nbsp;</strong> </p></td>
  	      <td align="left" colspan="2"><p align="left">
              <%
				sInt = "STATO|0|" & date & "|" & rsPers("STAT_CIT2") & "|cmbCittadinanza2|ORDER BY DESCRIZIONE"
				CreateCombo(sInt)
				%>
          </p></td>
      </tr>
		<tr>

			<td align="middle" colspan="2" nowrap class="tbltext1">&nbsp;</td>
			<td align="left" colspan="2" width="80%">&nbsp;</td>
		</tr>
	    <tr>
		    <td height="2" align="left" colspan="4" background="<%=Session("Progetto")%>/images/separazione.gif">		    </td>
	    </tr>
			</td>
		</tr>

		<tr>
          <td align="left" colspan="2" class="tbltext1"><p align="left"><span class="tbltext1"><strong>Correo Electr&oacute;nico </strong></span></p></td>
		  <td align="left" colspan="2"><!--<span class="tbltext">-->
              <%
					'Response.Write Session("Progetto")
					'Response.Write "<br>"
					if Session("Progetto") = "/SPI" then
				%>
              <input type="text" name="txtEmail" readonly="readonly" class="textblacka" maxlength="100" value="<%=rsPers("E_MAIL")%>" size="35" />
              <%
					else
				%>
              <input type="text" name="txtEmail" class="textblacka" maxlength="100" value="<%=rsPers("E_MAIL")%>" size="35" />
              <%
					end if
				%>
              <input type="hidden" id="text1" name="txtEmailOld" value="<%=rsPers("E_MAIL")%>" />
              <!--</span>-->          </td>
	  </tr>
	  <tr>
          <td align="left" colspan="2" class="tbltext1"><p align="left"> <span class="tbltext1"><strong>Estado Civil*</strong></span></p></td>
		  <td align="left" colspan="2"><p align="left">
              <%
				sInt = "STCIV|0|" & date & "|" & rsPers("STAT_CIV") & "|cmbStatoCiv|ORDER BY DESCRIZIONE"
				CreateCombo(sInt)
				%>
          </p></td>
	  </tr>
   <input type="hidden" name="txtProvRes" value="<%=rsPers("PRV_RES")%>">

	 <!--<tr>
        <td align="left" colspan="2" class="tbltext1">
			<p align="left"> <strong>&nbsp;</strong> <strong>Estado de
			<br>&nbsp;&nbsp;Reclutamiento</strong><strong>&nbsp;</strong>      </p>        </td>
        <td align="left" colspan="2" width="60%">
			<p align="left">
				<%
				'sInt = "POMIL|0|" & date & "|" & rsPers("POS_MIL") & "|cmbLeva|ORDER BY DESCRIZIONE"
				'CreateCombo(sInt)
				%>
			</p>        </td>
    </tr>-->
    <tr>
          <td align="left" colspan="2" class="tbltext1"><p align="left"> <span class="tbltext1"><strong>&#191;Es jefe de hogar?*</strong></span></p></td>
		  <td align="left" colspan="2"><p align="left">
              <%
				sInt = "EJH|0|" & date & "|" & rsPers("JEFE_HOGAR") & "|ddl_jefe_hogar|ORDER BY DESCRIZIONE"
				CreateCombo(sInt)
				%>
          </p></td>
	  </tr>
		<tr>

			<td align="middle" colspan="2" nowrap class="tbltext1">&nbsp;</td>
			<td align="left" colspan="2" width="80%">&nbsp;</td>
		</tr>


		<%if ucase(Session("PuedeMod")) <> "N" then
			'response.write "<br>sLogin " & sLogin 
		    gestlogin()
		  end if  


    'rsDocumento.Close
    set rsDocumento= nothing %>

	</table>

	<table WIDTH="500">
	    <tr>
	        <td align="left" colspan="2">&nbsp;</td>
	        <td align="left" colspan="2" width="60%">&nbsp;</td>
	    </tr>
	</table>
	<br>

	<table border="0" cellpadding="1" cellspacing="1" width="500">
	<tr>
        <td align="middle" colspan="2">
	<input type="image" src="<%=Session("Progetto")%>/images/conferma.gif" border="0" value="Modifica" onclick="return ControllaDati(this)">

        </td>
    </tr>
	</table>
 <%
end if
rsPers.Close
set rsPers = nothing

sub gestLogin%>
    <tr>    
	  <tr>
		<td height="2" align="left" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
	  </tr>
	  <tr>
		<td align="left" colspan="3" class="sfondocomm"><span class="tbltext1">Esta secci&oacute;n permite entregar credenciales (Usuario y Contrase&ntilde;a) al buscador de empleo, para facilitar su acceso al sistema v&iacute;a Web</span> </td>	 
	  </tr>
	  <tr>
		<td align="left" colspan="3" class="tbltext1"><span class="tbltext1"> <strong>&nbsp</strong></span> </td>	 
	  </tr>
	  <tr>
		<td align="left" class="tbltext1"><span class="tbltext1"> <strong>Usuario</strong></span> </td>
		<td align="left" colspan="2">
				<input class=textblacka style=TEXT-TRANSFORM: uppercase; WIDTH: 167px; size=35 maxlength=50 name=txtLogin value="<%=sLogin%>"
			<%if trim(sLogin)<>"" then response.write "disabled"%>>
		</td>
    </tr>
        <input type="hidden" id="Hidden2" name="logdb" value="<%=sLogin%>">	 
        <input type="hidden" id="Hidden3" name="pswdb" value="<%=sPasswd%>">	 
	<!--<tr>
      <td align="left" class="tbltext1">
      <b>Contrase&ntilde;a actual</b></td>
     <td align="left" colspan="2" width="60%">
        <input type="password" maxLength="20" name="oldpassword" size="21" ></td>
    </tr>-->
       
    <tr>
       <td align="left" class="tbltext1">
         <b>Nueva Contrase&ntilde;a</b></td>
       <td align="left" colspan="2" width="40%">
		  <input type="password" maxLength="20" name="newpassword" size="21"></td>
    </tr>  
   <tr>
    <td align="left" class="tbltext1"> 
      <b>Reingresar la Nueva Contrase&ntilde;a</b></td>
      <td align="left" colspan="2" width="40%">
        <input type="password" maxLength="20" name="rpassword" size="21"></td>
    </tr> <%
end sub

  %><!--#include virtual = "/include/CloseConn.asp"--><!--#include Virtual = "/strutt_coda2.asp"-->
