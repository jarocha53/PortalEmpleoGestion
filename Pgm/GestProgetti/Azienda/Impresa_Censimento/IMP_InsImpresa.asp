<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include virtual="/strutt_testa2.asp"-->
<!--#include virtual="/include/openconn.asp"-->
<!--#include virtual="/include/SelAreaTerrBandi.asp"-->
<!--#include virtual="/util/dbutil.asp"-->
<!--#include virtual="/include/ControlDateVB.asp"-->
<!--#include virtual="/include/DecCod.asp"-->
<!--#include virtual="/util/portallib.asp"-->

<%
	if ValidateService(session("idutente"),"IMP_VISCENSIMPRESA",cc) <> "true" then 
		response.redirect "/util/error_login.asp"
	end if	
%>

<meta HTTP-EQUIV="Pragma" CONTENT="no-cache"><meta HTTP-EQUIV="Cache-Control" CONTENT="no-cache">

<script language="javascript" src="ControlliImp.js"></script>
<script>
	<!--#include virtual = "/include/SelComune.js"-->

	//include del file per fare i controlli sulla validit� delle date
	<!--#include virtual="/Include/ControlDate.inc"-->

	//include del file per fare i controlli sulla numericit� dei campi
	<!--#include virtual="/Include/ControlNum.inc"-->
	<!--#include virtual="/Include/ControlString.inc"-->
	<!--#include virtual="/Include/help.inc"-->
</script>
<form name="frmImpresa" method="post" onsubmit="return ControllaDati(this)" id="frmImpresa" action="IMP_CnfImpresa.asp">

<table border="0" width="520" cellspacing="0" cellpadding="0" height="81">
   <tr>
     <td width="530" background="<%=Session("Progetto")%>/images/titoli/strumenti2b.gif" height="81" valign="bottom" align="right">
       <table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
         <tr>
           <td width="100%" valign="top" align="right"><b class="tbltext1a">Gestione Azienda &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></b></td>
         </tr>
       </table>
     </td>
   </tr>
</table>
<br>

<table cellpadding="0" cellspacing="0" width="500" border="0">
	<tr height="18">
		<td class="sfondomenu" height="18" width="67%">
			<span class="tbltext0">
				<b>GESTIONE AZIENDA</b>
			</span>
		</td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
		<td valign="middle" align="right" class="tbltext1" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">(*) campi obbligatori</td>
		</td>
		<td valign="middle" align="right" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1"></td>
	</tr>
	<tr width="371" class="SFONDOCOMM">
		<td colspan="3">Inserire i dati relativi all'azienda.
			<a href="Javascript:Show_Help('/Pgm/help/GestProgetti/Azienda/Impresa_Censimento/IMP_InsImpresa')"><img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
		</td>
	</tr>   
	<tr height="2">
		<td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif">
		</td>
	</tr>
</table>
<br><br>

	<input type="hidden" name="txtoggi" value="<%=ConvDateToString(Date())%>">
<%
	dim sIsa
	dim nProvSedi
	dim sCondizione
	dim i
	dim pos_ini
		
    sTimpr=Request.Form ("cmbCodTimpr")
    
	if mid(Session("UserProfiles"),1,1) = "0" then
		sIsa = "0"
	else
		sIsa = mid(Session("UserProfiles"),1,2)
	end if
	sTipoImp = request("cmbCodTimpr")
	
%>
	<table border="0" cellpadding="0" cellspacing="1" width="500">
		<tr>
			<td align="middle" colspan="2" nowrap>
				<p align="left">
					<span class="tbltext1" Style="CURSOR:HELP" title="Scegliere la tipologia d'impresa che corrisponde all'azienda"><b>Tipologia Impresa*<b></span>
				</p>
			</td>
			<td align="left">
<%
		'		if Session("progetto") <> "/PLAVORO" then
					sTimpr = "TIMPR|0|" & date & "|" & sTipoImp & "|cmbCodTimpr|AND CODICE <> '03' ORDER BY DESCRIZIONE"
		'		else
		'			sTimpr = "TIMPR|0|" & date & "|" & sTipoImp & "|cmbCodTimpr' onchange='InviaProv(this.value)' '|ORDER BY DESCRIZIONE"
		'		end if									
				CreateCombo(sTimpr)				
%>	
			</td>
		</tr>    
        <tr>
			<td align="middle" colspan="2" nowrap>
				<p align="left">
				<span class="tbltext1" Style="CURSOR:HELP" title="Inserire Denominazione Azienda"><b>Ragione sociale*<b></span>
				</p>
			</td>
            <td align="left" colspan="2" width="57%"> 
				<p align="left">
					<input style="TEXT-TRANSFORM:uppercase" WIDTH="220px" class="textblack" size="40" maxlength="50" name="txtRagSoc">					
				</p>
            </td>
		</tr>
        <tr>
			<td align="middle" colspan="2" nowrap>
				<p align="left">
				<span class="tbltext1" Style="CURSOR:HELP" title="Inserire forma giuridica che si adatta alla tipologia dell'attivit� esercitata."><b>Forma Giuridica*</b></span>&nbsp;
				</p>
            </td>
            
			<td align="left" colspan="2" width="57%"> 
				<p align="left">
				<span class="table">
<%
				sInt = "FGIUR|" & sIsa & "|" & date() & "| |cmbCodForm' | ORDER BY DESCRIZIONE"						
				CreateCombo(sInt)
%>
				</span>
				</p>
            </td>
		</tr>
        <tr>
			<td align="left" colspan="2">
				<p align="left">
					<span class="tbltext1" Style="CURSOR:HELP" title="Indicare classificazione delle attivit� economiche secondo estratto Istat."><b>Settore*</b></span>					
				</p>
			</td>
			
			<td align="left" colspan="2" width="57%"> 
				<p align="left">
					<span class="table">				
<%
				sSQL = "SELECT ID_SETTORE,subStr(DENOMINAZIONE,1,60) As DENOMINAZIONE from Settori ORDER BY DENOMINAZIONE"

'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
				set rsSett = CC.Execute(sSQL)
				Response.Write "<SELECT ID='cmbCodSet' name='cmbCodSet' class='textblack'><OPTION value=></OPTION>"

				do while not rsSett.EOF 

					Response.Write "<OPTION "

					Response.write "value ='" & clng(rsSett("ID_SETTORE")) & _
						"'> " & rsSett("DENOMINAZIONE")  & "</OPTION>"
					rsSett.MoveNext 
				loop
	
				Response.Write "</SELECT>"

				rsSett.Close	
				set rsSett = nothing
%>					
					</span>
				</p>
			</td>
		</tr>
        <tr>
			<td align="left" colspan="2" class="tbltext1">
				<p align="left">
					<b>Data costituzione</b> (gg/mm/aaaa)
					
				</p>
            </td>
            
			<td align="left" colspan="2" width="57%"> 
				<p align="left">
					<input style="TEXT-TRANSFORM:uppercase" class="textblack" size="16" maxlength="10" name="txtDtCost">
		        </p>
            </td>
		</tr>
        <tr>
			<td align="left" colspan="2">
				<p align="left">
					<span class="tbltext1"><b>Capitale sociale</b></span>
					
				</p>
            </td>
            
			<td align="left" colspan="2" width="57%"> 
				<p align="left">
					<input style="TEXT-TRANSFORM:uppercase" WIDTH="219px" class="textblack" size="10" name="txtCapSoc" MAXLENGTH="8">				
				</p>
            </td>
		</tr>
        <tr>
			<td align="left" colspan="2">
				<p align="left">
					<span class="tbltext1"><b>Valuta</b></span>					
				</p>
            </td>            
			<td align="left" colspan="2" width="57%"> 
				<p align="left">
				<!-- BAB-INI					<input class="textblack" type="text" name="txtEuro" size="10" maxlength="10" value="Euro" readonly>					<span class="textblack"> <b>Euro</b>					<input type="hidden" id="cmbCodVal" name="cmbCodVal" value="02">				BAB-FI -->	

<%				
				sInt = "VALUT|" & sIsa & "|" & date() & "||cmbCodVal| ORDER BY DESCRIZIONE"			
				CreateCombo(sInt)
%>
					</span>
				</p>
            </td>
		</tr>
        <tr>
            <td align="left" colspan="2">
				<p align="left">
					<span class="tbltext1" Style="CURSOR:HELP" title="Inserire breve descrizione attivit� principale dell'azienda"><b>Attivit�*</b></span>					
				</p>
            </td>            
			<td align="left" colspan="2" width="57%"> 
				<p align="left">
					<input style="TEXT-TRANSFORM:uppercase" WIDTH="219px" class="textblack" size="40" maxlength="50" name="txtDescAtt">
                </p>
            </td>
		</tr>
        <tr>
			<td align="left" colspan="2">
				<p align="left">
					<span class="tbltext1"><b>N� iscr. registro</b></span>					
				</p>
            </td>            
			<td align="left" colspan="2" width="57%"> 
				<p align="left">
				<input style="TEXT-TRANSFORM:uppercase" class="textblack" size="16" maxlength="11" name="txtNumIscReg">			
				</p>
            </td>
        </tr>
        <tr>
            <td align="left" colspan="2">
				<p align="left">
				   <span class="tbltext1"><b>Numero Inps </b></span>
				</p>
            </td>            
			<td align="left" colspan="2" width="57%"> 
				<p align="left">
				   <input style="TEXT-TRANSFORM:uppercase" class="textblack" size="16" maxlength="12" name="txtNumInps">				
		        </p>
            </td>
		</tr>
		<tr height="2">
			<td class="sfondocomm" colspan="4" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
		</tr>
		<tr>
			<td colspan="4" class="textblack">
				 I campi <b>Partita IVA</b> e <b>Codice fiscale</b> possono essere compilati in alternativa o entrambi.
				 <br>E' comunque obbligatoria la compilazione di almeno uno dei due.
			</td>
		</tr>
        <tr>
			<td align="left" colspan="2">
				<p align="left">
					<span class="tbltext1"><b>Partita IVA</b></span>
				</p>
            </td>            
			<td align="left" colspan="2" width="57%"> 
				<p align="left">					
					<input style="TEXT-TRANSFORM:uppercase" class="textblack" size="16" maxlength="11" name="txtPartIva">					
				</p>
            </td>
        </tr>
        <tr>
			<td align="left" colspan="2">
				<p align="left">
					<span class="tbltext1"><b>Codice fiscale</b></span>					 
				</p>
            </td>            
			<td align="left" colspan="2" width="57%"> 
				<p align="left">
					<input style="TEXT-TRANSFORM:uppercase" class="textblack" size="16" maxlength="16" name="txtCodFis">					
				</p>
            </td>
        </tr>
        <tr>
            <td align="left" colspan="2">
				<p align="left">
				    <span class="tbltext1"><b>Sito Web</b></span>					 
				</p>
            </td>            
			<td align="left" colspan="2" width="57%"> 
				<p align="left">
					<input style="TEXT-TRANSFORM:uppercase" WIDTH="219px" class="textblack" size="40" maxlength="255" name="txtSitoWeb">
				</p>
            </td>
        </tr>
        <tr>
			<td align="middle" colspan="2">&nbsp;</td>
            
			<td align="middle" colspan="2" width="57%">&nbsp;</td>
        </tr>
        <tr>
			<td align="left" colspan="2">
				<p align="left">
					<span class="tbltext1" Style="CURSOR:HELP" title="Inserire una breve descrizione della sede"><b>Descrizione sede*</b></span>
				</p>
			</td>			
			<td align="left" colspan="2" width="57%"> 
				<p align="left">				
					<input style="TEXT-TRANSFORM:uppercase" WIDTH="220px" class="textblack" size="40" maxlength="50" name="txtDescrizione">
				</p>
			</td>
        </tr>
        <tr>
			<td align="middle" colspan="2" nowrap>
				<p align="left">
					<span class="tbltext1" Style="CURSOR:HELP" title="Indicare che funzione si svolge nella sede indicata."><b>Tipo sede*</b></span>
				</p>		
            </td>            
			<td align="left" colspan="2" width="57%"> 
				<p align="left">
					<span class="table">
<%
					sInt = "TSEDE|" & sisa & "|" & date & "|" & sCodSede & "|cmbCodSede| ORDER BY DESCRIZIONE"			
					CreateCombo(sInt)
%>
				</p>
            </td>
        </tr>
        <tr>
			<td align="left" colspan="2">
				<p align="left">
					<span class="tbltext1"><b>Indirizzo*</b></span>
				</p>
			</td>			
			<td align="left" colspan="2" width="57%"> 
				<p align="left">				
					<input style="TEXT-TRANSFORM:uppercase" WIDTH="220px" class="textblack" size="40" maxlength="50" name="txtIndirizzo">
				</p>
			</td>
        </tr>               
        <tr>
			<td align="left" colspan="2" width="57%">
				<p align="left">
					<span class="tbltext1"><b>Provincia*</b></span>
				</p>
            </td>            
			<td align="left" colspan="2" width="57%"> 
				<p align="left">
<%				
'**** iniziare da qui le modifiche AM 12/09	****
				
				sSetProv = SetProvUorg(Session("IdUorg")) 'ricavato IDUORG
				nProvSedi = len(sSetProv)
%>	
<!--Inizio AM 12/09/2003-->
				<select NAME="cmbProv" class="textblack" onchange="javascript:Pulisci()">			 
						<option></option>
	    
						<% pos_ini = 1 	                
						    for i=1 to nProvSedi/2
						        sTarga = mid(sSetProv,pos_ini,2)															
						%>
						 <option value="<%=sTarga%>"><%=DecCodVal("PROV", 0, "", sTarga,"")%></option>
                    
						<% pos_ini = pos_ini + 2
						next%>
	             </select>
<!--Fine AM 12/09/2003-->                	
				</p>
			</td>
		</tr>
        <tr>
			<td align="left" colspan="2" nowrap class="tbltext1">
				<p align="left">
					<span class="tbltext1"><b>Comune*</b></span>
				</p>
			</td>
			<td align="left" colspan="2" width="57%">
				<span class="tbltext">
				<input type="text" style="TEXT-TRANSFORM: uppercase; WIDTH: 220px" class="textblack" name="txtComune" size="10" readonly>
				<input type="hidden" id="txtComuneHid" name="txtComuneHid">
<%
					NomeForm="frmImpresa"
					CodiceProvincia="cmbProv"
					NomeComune="txtComune"
					CodiceComune="txtComuneHid"
					Cap="txtCap"
%>
				<a href="Javascript:SelComune('<%=NomeForm%>','<%=CodiceProvincia%>','<%=NomeComune%>','<%=CodiceComune%>','<%=Cap%>')" ID="imgPunto1" name="imgPunto1" onmouseover="javascript:window.status='';return true">
				<img border="0" src="<%=Session("Progetto")%>/images/bullet1.gif"></a>
				</span>						
			</td>
		</tr>
         <tr>
			<td align="left" colspan="2">
				<p align="left">
					<span class="tbltext1"><b>CAP*</b></span>
				</p>
            </td>            
			<td align="left" colspan="2" width="57%"> 
				<p align="left">					
					<input style="TEXT-TRANSFORM:uppercase" WIDTH="82px" class="textblack" size="10" maxlength="5" name="txtCap">
					</font>
		        </p>
            </td>
        </tr>        
        <tr>
			<td align="left" colspan="2">
				<p align="left">
				    <span class="tbltext1"><b>Telefono</b></span>					
				</p>
            </td>
            <td align="left" colspan="2" width="60%">
				<p align="left">
					<span class="table">
					<input style="TEXT-TRANSFORM:uppercase" class="textblack" size="25" maxlength="20" name="txtSedeTel" value>
				</p>
            </td>
        </tr>        
        <tr>
            <td align="left" colspan="2">
				<p align="left">
				    <span class="tbltext1"><b>Fax</b></span>		
				</p>
            </td>
            <td align="left" colspan="2" width="60%">
				<p align="left">
					<span class="table">
					<input style="TEXT-TRANSFORM:uppercase" class="textblack" WIDTH="220px" size="25" maxlength="20" name="txtSedeFax" value>
				</p>
            </td>
        </tr>        
        <tr>
            <td align="left" colspan="2">
				<p align="left">
				    <span class="tbltext1"><b>E-Mail</b></span>
				</p>
            </td>
            <td align="left" colspan="2" width="60%">
				<p align="left">
					<span class="table">
					<input name="txtSedeEmail" WIDTH="220px" class="textblack" size="30" maxlength="100" value>	
							
				</p>
            </td>
        </tr>
<%
	if sTipoImp<>"03" then%>  
		<tr>
            <td align="left" colspan="2">
				<span class="tbltext1"><b>Centro Impiego*</b></span>
            </td>
				<input type="hidden" id="txtCentroImpiegoHid" name="txtCentroImpiegoHid">
				<!--<input type="hidden" id="txtProvinciaOld" name="txtProvinciaOld"> -->
			<td align="left" colspan="2" width="57%">
				<span class="tbltext">
					<input type="text" name="txtCentroImpiego" style="TEXT-TRANSFORM: uppercase; WIDTH: 220px" class="textblack" size="10" readonly>				
					<a href="Javascript:SelCentroImpiego('CENTRO_IMPIEGO','ID_CIMPIEGO','txtCentroImpiego')" onmouseover="javascript:window.status='';return true" name="imgPunto1">
					<img border="0" src="<%=Session("Progetto")%>/images/bullet1.gif" id="ImgCPI"></a>
				</span>		
			</td>
		</tr>
	</div> 
<%
	else%>
		<input type="hidden" id="txtCentroImpiego" name="txtCentroImpiego" value="nessuno"> 
		<input type="hidden" id="txtCentroImpiegoHid" name="txtCentroImpiegoHid">
		<!--<input type="hidden" id="txtProvinciaOld" name="txtProvinciaOld">-->
<%
	end if%>

<%
	'if Session("progetto") = "/PLAVORO" then%>
<!--		<tr>            <td align="left" colspan="2">&nbsp;</td>            <td align="left" colspan="2" width="57%">&nbsp;</td>        </tr>        <tr>            <td align="left" colspan="2">				<p align="left">				   <span class="tbltext1"><b>Referente*</b>(Cognome/Nome)</span>						</p>            </td>			<td align="left" colspan="2" width="57%"> 				<p align="left">					<input size="40" maxlength="50" name="txtRef" style="TEXT-TRANSFORM: uppercase; WIDTH: 220px;" class="textblack">				</p>            </td>        </tr>        <tr>			<td align="left" colspan="2">				<p align="left">				   <span class="tbltext1"><b>Ruolo*</b></span>									</p>            </td>            <td align="left" colspan="2" width="57%"> 				<p align="left">					<span class="table"><%					'Devo richiamate la Combo per il ruolo	'				sInt = "RUOLO|" & sIsa & "|" & date & "|" & sCodRuo & "|cmbRefRuo| ORDER BY DESCRIZIONE"	'				CreateCombo(sInt)%>					</span>		        </p>            </td>		</tr>        <tr>            <td align="left" colspan="2">				<p align="left">				   <span class="tbltext1"><b>Telefono</b></span>									</p>            </td>			<td align="left" colspan="2" width="57%"> 				<p align="left">					<input style="TEXT-TRANSFORM: uppercase;" class="textblack" size="25" maxlength="20" name="txtTel">				</p>            </td>        </tr>        <tr>            <td align="left" colspan="2">				<p align="left">				     <span class="tbltext1"><b>Fax</b></span>									</p>            </td>            <td align="left" colspan="2" width="57%"> 				<p align="left">										<input style="TEXT-TRANSFORM: uppercase;" class="textblack" size="25" maxlength="20" name="txtFAX">				</p>            </td>        </tr>        <tr>            <td align="left" colspan="2">				<p align="left">				     <span class="tbltext1"><b>E-Mail*</b></span>				</p>            </td>			<td align="left" colspan="2" width="57%">				<p align="left">					<input class="textblack" size="40" maxlength="100" name="txtEMail">							        </p>            </td>        </tr>		<tr>            <td align="left" colspan="2">				<p align="left">				     <span class="tbltext1"><b>Login*</b></span>									</p>            </td>            <td align="left" colspan="2" width="57%">				<p align="left">					<input style="TEXT-TRANSFORM: uppercase;" class="textblack" size="40" maxlength="15" name="txtLogin">				</p>            </td>        </tr> -->
<%
'	end if 
	
'if Session("progetto") = "/PLAVORO" then%>
<!--	<input type="hidden" name="Plavoro" value="SI">  -->
<%
'else%>
	<input type="hidden" name="Plavoro" value="NO">
<%
'end if%>
		<tr>
			<td align="middle" colspan="2">&nbsp;</td>
			<td align="middle" colspan="2" width="57%">&nbsp;</td>
		</tr>
		<tr>
			<td align="middle" colspan="2">&nbsp;</td>
            <td align="middle" colspan="2" width="57%">&nbsp;</td>
        </tr>
	</table>
    
    <table cellpadding="0" cellspacing="0" width="200" border="0">	
		<tr>
			<td align="center">
				<img src="<%=Session("Progetto")%>/images/Indietro.gif" border="0" onclick="javascript:history.back();">
			</td>

			<td align="center">
				<input type="image" name="Conferma" src="<%=Session("progetto")%>/images/conferma.gif">
			</td>
		</tr>
	</table>
</form>

<br><br><br>


<!--#include virtual="/strutt_coda2.asp"-->
<!--#include virtual="/include/closeconn.asp"-->
