<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include virtual="/util/portallib.asp"-->
<!--#include virtual="/strutt_testa2.asp"-->
<!--#include virtual="/include/openconn.asp"-->
<!--#include virtual ="/include/ckProfile.asp"-->
<%
	if ValidateService(session("idutente"),"IMP_VISCENSIMPRESA",cc) <> "true" then 
		response.redirect "/util/error_login.asp"
	end if
%>

<script LANGUAGE="Javascript">
//include del file per fare i controlli sulla numericit� dei campi
<!--#include virtual = "/Include/ControlNum.inc"-->
<!--#include virtual="/Include/help.inc"-->
<!-- #Include Virtual="/Include/ControlString.inc" -->

function ControllaDati(frmVisImpresa) {
	
	frmVisImpresa.txtDescrizione.value=TRIM(frmVisImpresa.txtDescrizione.value)
	if 	(!ValidateInputStringWithNumber(frmVisImpresa.txtDescrizione.value)) {
		alert("Il campo  Ragione sociale  � formalmente errato")
		frmVisImpresa.txtDescrizione.focus();
		return false;
	}
		
	if (frmVisImpresa.txtPartIva.value.length > 0 )	{ 
		if (!IsNum(frmVisImpresa.txtPartIva.value)) {
			alert("Il campo Partita Iva deve essere numerico")
			frmVisImpresa.txtPartIva.focus() 
			return false
		}
		if (frmVisImpresa.txtPartIva.value.length != 11) 
		{
			alert("Il campo Partita Iva deve essere di 11 caratteri")
			frmVisImpresa.txtPartIva.focus() 
			return false
		}
	}
		
	frmVisImpresa.txtCodFiscale.value=TRIM(frmVisImpresa.txtCodFiscale.value)
	if 	(frmVisImpresa.txtCodFiscale.value.length > 0 )	{
		if 	(!ValidateInputStringWithNumber(frmVisImpresa.txtCodFiscale.value))	{
			alert("Il campo Codice Fiscale � formalmente errato")
			frmVisImpresa.txtCodFiscale.focus();
			return false;
		}
		blank = " ";
		if (!ChechSingolChar(frmVisImpresa.txtCodFiscale.value,blank))	{
			alert("Il campo Codice Fiscale non pu� contenere spazi bianchi")
			frmVisImpresa.txtCodFiscale.focus();
			return false;
		}
		if ((frmVisImpresa.txtCodFiscale.value.length != 11) && (frmVisImpresa.txtCodFiscale.value.length != 16)) {
			alert("Il codice Fiscale  deve essere di 11  o 16 caratteri")
			frmVisImpresa.txtCodFiscale.focus(); 
			return false;
		}
	}
			
	document.frmVisImpresa.prVolta.value="No";
	return true;
}


function HideVariables(sUrl,idForm) {
	document.forms.item('frmRicAzien' + idForm).action = sUrl		
	document.forms.item('frmRicAzien' + idForm).submit();
}
	
function userfocus() {
	document.frmVisImpresa.txtDescrizione.focus()
}

</script>
</head>


<body background="../../../images/sfondo1.gif" onload="return userfocus()">

<%
	'on error resume next 
	dim CnConn
	dim rsAziende
	dim sSQL
	dim nCont
	dim sIsa
	dim sPrVolta
	dim sDescrizione
	dim sPartIva
	dim CodFiscale
	dim i
	dim nMaxElem
	dim sMask
	
	' Controllo i diritti dell'utente connesso.
	' La variabile di sessione mask � cos� formata:
	' 1 Byte abilitazione funzione Impresa
	' 2 Byte abilitazione funzione Sede_impresa
	' 3 Byte abilitazione funzione Val_impresa
	' dal 4� byte in poi si visualizzano le provincie.
		
	sMask = Session("mask") 
	' Mi definisco le variabili Diritti
	Session("Diritti")= mid(sMask,1,6)
	' Mi definisco le variabili Filtro
	Session("Filtro")= mid(sMask,7)	

	sMask = Session("Diritti")

	nMaxElem = 30 ' Numero massimo di imprese da visualizzare.
	
	sDescrizione= ucase(Request("txtDescrizione"))
	sDescrizione= Replace(sDescrizione,"'","''")

	sPartIva = Request("txtPartIva")
	sCodFiscale = ucase(Request("txtCodFiscale"))
	sPrVolta = Request("prVolta")
	
	if mid(Session("UserProfiles"),1,1) = "0" then
		sIsa = "0"
	else
		sIsa = mid(Session("UserProfiles"),1,2)
	end if

%>

<table border="0" width="520" cellspacing="0" cellpadding="0" height="81">
   <tr>
     <td width="530" background="<%=Session("Progetto")%>/images/titoli/strumenti2b.gif" height="81" valign="bottom" align="right">
       <table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
         <tr>
           <td width="100%" valign="top" align="right"><b class="tbltext1a">Gestione Azienda &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></b></td>
         </tr>
       </table>
     </td>
   </tr>
</table>
<br>
<table cellpadding="0" cellspacing="0" width="500" border="0" align="center">
   <tr height="18">
		<td class="sfondomenu" height="18" width="67%">
		<span class="tbltext0"><b>&nbsp;RICERCA AZIENDA</b></span></td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
		<td valign="middle" align="right" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1"></td>
   </tr>
   <tr width="371" class="SFONDOCOMM">
   <!-- bab-in    <td colspan="3">Indicare i dati per la ricerca e selezionare l'azienda che apparir�.<a href="Javascript:Show_Help('/Pgm/help/GestProgetti/Azienda/Impresa_Censimento/IMP_VisCensImpresa')"><img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a></td>   bab-fi  -->
   <td colspan="3">Indicare i dati per la ricerca ed effettuare una selezione tra i risultati visualizzati.<a href="Javascript:Show_Help('/Pgm/help/GestProgetti/Azienda/Impresa_Censimento/IMP_VisCensImpresa')"><img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a></td>
	</tr>
	<tr height="2">
		<td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif">
		</td>
	</tr>
</table>
<br>


<form method="post" name="frmVisImpresa" onsubmit="return ControllaDati(this)" action="IMP_VisCensImpresa.asp">
<input type="hidden" name="prVolta" value>

<table width="500" border="0" cellspacing="1" cellpadding="0">
		<tr align="left"> 
		<td nowrap width="75" class="tbltext1" title="Inserire Nome dell'Azienda">
			<b>Rag. Sociale</b>
		</td>
		
		
		<td nowrap width="200" align="middle" class="tbltext">
			<span class="size"><input style="TEXT-TRANSFORM: uppercase; 
				WIDTH:  254px;" name="txtDescrizione" class="textblack" size="32" maxlength="50"></span>
		</td>
		
	<tr align="left">
		<td nowrap width="75" class="tbltext1">
			<b>Part.Iva</b>
		</td>
		<td nowrap width="200" align="middle" class="tbltext">
			<span class="size"><input style="TEXT-TRANSFORM: uppercase; 
				WIDTH: 254px;" name="txtPartIva" class="textblack" size="32" maxlength="11"></span>
		</td>
	</tr>
	
		<tr align="left">
		<td nowrap width="75" class="tbltext1">
			<b>Cod.Fiscale</b>
		</td>
		<td nowrap width="200" align="middle" class="tbltext">
			<span class="size"><input style="TEXT-TRANSFORM: uppercase; WIDTH: 254px;" name="txtCodFiscale" class="textblack" size="32" maxlength="16">
		</span>
		</td>
	</tr>

	
	
</table>
<br>

<table cellpadding="0" cellspacing="0" width="300" border="0">	
	<tr align="center">
	
				
	<td nowrap><input type="image" name="Conferma" src="<%=Session("progetto")%>/images/lente.gif"></td value="Registra">
	<td nowrap><a href="javascript:document.frmVisImpresa.reset()"><img border="0" src="<%=Session("progetto")%>/images/annulla.gif" onLoad="return userfocus()"></td>			
	</tr>
</table>
</form>
	

<%
if sPrVolta = "No" then
	' Controlla la abilitazione all'inserimento
	if 	ckProfile(mid(sMask,1,2),2) then
	
	 
	
%>

	<table width="371">
		<tr> 
			<td align="center"> 
				<a class="textRed" href="IMP_InsImpresa.asp" onmouseover="javascript: window.status=' '; return true">
					<b>Inserisci nuova&nbsp;azienda</b>
				</a>
			</td>
		</tr>
	</table>
	<%end if%>
	<br>
	<table width="500" cellpadding="1" cellspacing="2" border="0">
			<!--tr height="20" class="SFONDOCOMM"> 				<td align="middle" valign="center" width="100" class="tbltext1"><b>&nbsp;Id azienda</b></td>				<td align="middle" valign="center" width="130" class="tbltext1"><b>Part.Iva</b></td>				<td align="middle" valign="center" width="130" class="tbltext1"><b>Cod.Fiscale</b></td>				<td align="middle" valign="center" width="270" class="tbltext1"><b>Azienda</b></td>			</tr-->
	<%
		nCont = 0 
	
			
		'sSQL = "SELECT ID_IMPRESA,RAG_SOC,PART_IVA, COD_FISC FROM IMPRESA WHERE ID_IMPRESA = ID_IMPRESA "	
	
		sSQL = "SELECT ID_IMPRESA,RAG_SOC,PART_IVA, COD_FISC FROM IMPRESA  "	
	
		
		'if sDescrizione <> "" then
			sSQL = sSQL + " WHERE RAG_SOC Like '%" & sDescrizione & "%'"
		'end if	
		
		if sPartIva <> "" then
			sSQL = sSQL + " AND PART_IVA='" & sPartIva & "'"
		end if
		
		if sCodFiscale <> "" then
			sSQL = sSQL + " AND COD_FISC ='" & sCodFiscale & "'"
		end if
		
		
		'if Session("progetto") <> "/PLAVORO" then 
			sSQL = sSQL + " AND COD_TIMPR <> '03'"	
		'end if
		
		sSQL = sSQL + " ORDER BY RAG_SOC "
	
	
		
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
		set rsAziende = CC.Execute(sSQL)
		'cicla fino alla fine del recor [do while not]  
		
		i = 0
		
		if not rsAziende.EOF then
		%> 
			<tr height="20" class="tblsfondo"> 
				<td align="middle" valign="center" width="100" class="tbltext1" title="Identificativo dell'azienda"><b>&nbsp;Id azienda</b></td>
				<td align="middle" valign="center" width="130" class="tbltext1"><b>Part.Iva</b></td>
				<td align="middle" valign="center" width="130" class="tbltext1"><b>Cod.Fiscale</b></td>
				<td align="middle" valign="center" width="270" class="tbltext1"><b>Azienda</b></td>
			</tr>
		<%
				
			do while not rsAziende.EOF
				i = i +1
				nCont = 1
	%>
		<form name="frmRicAzien<%=i%>" method="post">
		<input type="hidden" name="sCodImp" value="<%=rsAziende("ID_IMPRESA")%>">
			<tr class="tblsfondo"> 
			  <td align="middle" width="100">
					<span class="tblagg">
			
					<%
				' Controlla la abilitazione all'inserimento
						Response.write "<A class=""tbltext1""   onmouseover=""javascript: window.status=' '; return true""   "
						Response.Write "HREF=""  javascript: HideVariables('IMP_ModImpresa.asp','" & i & "')"">"
						Response.write "<b>" & rsAziende("ID_IMPRESA") & "</b></A>"
		
				%>
				
					</span>
				</td>
				<td width="130" align="middle">
					<span class="tbldett">
					<%Response.Write (rsAziende("PART_IVA")) %>
					</span>
				</td>

				<td width="130" align="middle">
					<span class="tbldett">
					<%Response.Write (rsAziende("COD_FISC"))  %>
					</span>
				</td>
				<td align="middle" width="270">
				<span class="tbldett">
					<%Response.Write (rsAziende("RAG_SOC")) %>
					</span>
				</td>
			</tr>
			</form>
	<% 

			if i => nMaxElem then
	%>
				</table>
				<table width="371" cellpadding="0" cellspacing="0" border="0">
					<tr align="middle">
						<td>
							<span class="tbltext1">
							<% 
								nCont = 1
								response.write ("<BR>Sono state trovati pi� di " & nMaxElem & " record modificare i parametri di ricerca")
							%>
							</span>
							<br>
							<br>
							<br><!--<table cellspacing="0" width="630">								<tr> 									<td align="Center" class="tblsfondo">										<span class="tbltext">										<b>Parola chiave indicata nel campo descrizione: </b><b>'<%=sDescrizione%>'</b></font>										</span>									</td>								</tr>							</table>-->
						</td>
					</tr>
				</table> 
	<%
				exit do
			end if
			
			rsAziende.MoveNext 
			Loop
		
		end if			
		
		rsAziende.Close 
		set rsAziende = nothing 
		
		
	%> 
	</table>
	<br>

	<% 
		if nCont = 0 then
	%>
			<table width="371" cellpadding="0" cellspacing="0" border="0">
				<tr align="middle">
					<td>
						<span class="tbltext3">
						
						<% '''LC77_24/11/2003:MODIFICATO MESSAGGIO
							response.write ("<STRONG>La ricerca non ha prodotto nessun risultato. Immettere dei nuovi criteri e ripetere la ricerca</STRONG>")
						%>
						</span>

					</td>
				</tr>
			</table> 
	<%
		end if
	end if
%> 
<!--#include virtual ="/strutt_coda2.asp"-->
<!--#include virtual ="/include/closeconn.asp"-->

