<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include virtual="/util/portallib.asp"-->
<!--#include virtual="/strutt_testa2.asp"-->
<!--#include virtual="/include/openconn.asp"-->
<!--#include virtual="/include/ckProfile.asp"-->
<!--#include virtual="/util/dbutil.asp"-->
<!--#include virtual="/include/DecCod.asp"-->
<!--#include Virtual = "/include/DecComun.asp"-->
<!--#include virtual="/include/SysFunction.asp"-->

<%
if ValidateService(session("idutente"),"IMP_VISCENSIMPRESA",cc) <> "true" then 
	response.redirect "/util/error_login.asp"
end if
%>

<script language="javascript" src="ControlliImp.js"></script>

<script LANGUAGE="Javascript">
//include del file per fare i controlli sulla validit� delle date
<!--#include virtual="/Include/ControlDate.inc"-->
<!--#include virtual = "/include/SelComune.js"-->

//include del file per fare i controlli sulla numericit� dei campi
<!--#include virtual="/Include/ControlNum.inc"-->
<!--#include virtual="/Include/ControlString.inc"-->
<!--#include virtual="/Include/help.inc"-->
	 

//Funzione per i controlli dei campi 
function ControllaDati(ModSedeImpresa)
{
	var Decorrenza
	var Scadenza

	// -------- CAMPI OBBLIGATORI -------------------------		

	frmSedeImpresa.txtDescrizione.value=TRIM(frmSedeImpresa.txtDescrizione.value)
	// Descrizione
	if (frmSedeImpresa.txtDescrizione.value == "")
	{
		alert("Il campo Descrizione � obbligatorio")
		frmSedeImpresa.txtDescrizione.focus() 
		return false
	}
	if (!ValidateInputStringWithNumber(frmSedeImpresa.txtDescrizione.value))
	{
		alert("Il campo Descrizione � formalmente errato")
		frmSedeImpresa.txtDescrizione.focus()
		return false;
	}
	
	// Tipo sede
	if (frmSedeImpresa.cmbCodSede.selectedIndex == 0) {
		alert ("Il campo Tipo sede � obbligatorio")
		frmSedeImpresa.cmbCodSede.focus()
		return false
	}
	
	// Indirizzo
	frmSedeImpresa.txtIndirizzo.value=TRIM(frmSedeImpresa.txtIndirizzo.value)
	if (frmSedeImpresa.txtIndirizzo.value == ""){
		alert("Il campo Indirizzo � obbligatorio")
		frmSedeImpresa.txtIndirizzo.focus() 
		return false
	}
	/*if (!ValidateInputStringWithNumber(frmSedeImpresa.txtIndirizzo.value))
	{
		alert("Il campo Indirizzo � formalmente errato")
		frmSedeImpresa.txtIndirizzo.focus()
		return false;
	}*/
	
	// Controllo C.A.P.
	/*if (frmSedeImpresa.txtCapRes.value == ""){
		alert("Campo obbligatorio")
		frmSedeImpresa.txtCapRes.focus() 
		return false
	}*/


	// Controllo Comune
	frmSedeImpresa.txtComune.value=TRIM(frmSedeImpresa.txtComune.value)
	if (frmSedeImpresa.txtComune.value == ""){
		alert("Il campo Comune � obbligatorio")
		frmSedeImpresa.txtComune.focus() 
		return false
	}

	// CAP
	frmSedeImpresa.txtCapRes.value = TRIM(frmSedeImpresa.txtCapRes.value)		
	if (frmSedeImpresa.txtCapRes.value == ""){
		alert("Il campo CAP � obbligatorio")
		frmSedeImpresa.txtCapRes.focus() 
		return false
	}
	if (!IsNum(frmSedeImpresa.txtCapRes.value)){
		alert("Il campo CAP deve essere numerico")
		frmSedeImpresa.txtCapRes.focus() 
		return false
	}
	if (eval(frmSedeImpresa.txtCapRes.value) == 0){
		alert("Il campo CAP non pu� essere zero")
		frmSedeImpresa.txtCapRes.focus()
		return false
	}		
	if (frmSedeImpresa.txtCapRes.value.length != 5){
		alert("Il campo CAP deve essere di 5 caratteri numerici")
		frmSedeImpresa.txtCapRes.focus() 
		return false
	}
		
	// Controllo Telefono Sede
	frmSedeImpresa.txtSedeTel.value = TRIM(frmSedeImpresa.txtSedeTel.value)
	/*if (frmSedeImpresa.txtSedeTel.value == ""){
		alert("Il campo Telefono � obbligatorio")
		frmSedeImpresa.txtSedeTel.focus() 
		return false
	}*/
	if (frmSedeImpresa.txtSedeTel.value != "")
	{
		if (!IsNum(frmSedeImpresa.txtSedeTel.value)){
			alert("Il campo Telefono deve essere numerico")
			frmSedeImpresa.txtSedeTel.focus() 
			return false
		}
		if (eval(frmSedeImpresa.txtSedeTel.value) == 0){
			alert("Il numero di telefono non pu� essere zero")
			frmSedeImpresa.txtSedeTel.focus()
			return false
		}
	}
	
	// Controllo Fax Sede
	frmSedeImpresa.txtSedeFax.value = TRIM(frmSedeImpresa.txtSedeFax.value)
	if (frmSedeImpresa.txtSedeFax.value != ""){
		if (!IsNum(frmSedeImpresa.txtSedeFax.value)){
			alert("Il campo Fax deve essere numerico")
			frmSedeImpresa.txtSedeFax.focus() 
			return false
		}
		if (eval(frmSedeImpresa.txtSedeFax.value) == 0){
			alert("Il numero di fax non pu� essere zero")
			frmSedeImpresa.txtSedeTel.focus()
			return false
		}
	}
	
	// Controllo Email Sede
	//frmSedeImpresa.txtSedeEmail.value=TRIM(frmSedeImpresa.txtSedeEmail.value)
	if (frmSedeImpresa.txtSedeEmail.value != ""){
		pippo=ValidateEmail(frmSedeImpresa.txtSedeEmail.value)
		if (pippo==false){
			alert("Indirizzo Email formalmente errato")
			frmSedeImpresa.txtSedeEmail.focus() 
			return false
		}
	}
	
	if (document.frmSedeImpresa.txtCentroImpiego.value == ""){
		alert("Il campo Centro Impiego � obbligatorio")
		return false
	}			
	
	// verificare MARCO
	if (document.frmSedeImpresa.cmbProv.value != document.frmSedeImpresa.txtProvinciaOld.value){
		alert("Il centro per l'impiego non risiede in questa provincia")
		return false
	}			

	// verificare MARCO
	// Controllo referente
	if (frmSedeImpresa.Plavoro.value == "NO"){
		return true
	}

	frmSedeImpresa.txtRef.value=TRIM(frmSedeImpresa.txtRef.value)
	if (frmSedeImpresa.txtRef.value == ""){
		alert("Il campo Referente � obbligatorio")
		frmSedeImpresa.txtRef.focus() 
		return false
	}
	if (!ValidateInputStringWithOutNumber(frmSedeImpresa.txtRef.value)) {
		alert("Il campo Referente � formalmente errato")
		frmSedeImpresa.txtRef.focus()
		return false;
	}

	//if (frmSedeImpresa.elements[10].selectedIndex == 0) {
	if (frmSedeImpresa.cmbRefRuo.selectedIndex == 0) {
		alert ("Campo Ruolo obbligatorio")
		//frmSedeImpresa.elements[10].focus()
		frmSedeImpresa.cmbRefRuo.focus()
		return false
	}

	// Controllo Telefono Sede
	frmSedeImpresa.txtTel.value=TRIM(frmSedeImpresa.txtTel.value)
	if (frmSedeImpresa.txtTel.value != ""){
		if (!IsNum(frmSedeImpresa.txtTel.value)){
			alert("Il recapito telefonico deve essere numerico")
			frmSedeImpresa.txtTel.focus() 
			return false
		}
		if (eval(frmSedeImpresa.txtTel.value) == 0){
			alert("Il numero di telefono non pu� essere zero")
			frmSedeImpresa.txtTel.focus()
			return false
		}
	}
	
	// Controllo Fax Sede
	frmSedeImpresa.txtFAX.value=TRIM(frmSedeImpresa.txtFAX.value)
	if (frmSedeImpresa.txtFAX.value != ""){
		if (!IsNum(frmSedeImpresa.txtFAX.value)){
			alert("Il campo Fax deve essere numerico")
			frmSedeImpresa.txtFAX.focus() 
			return false
		}
		if (eval(frmSedeImpresa.txtFAX.value) == 0){
			alert("Il numero di fax non pu� essere zero")
			frmSedeImpresa.txtFAX.focus()
			return false
		}
	}
	
	// Controllo Email Referente
	frmSedeImpresa.txtEMail.value=TRIM(frmSedeImpresa.txtEMail.value)
	if (frmSedeImpresa.txtEMail.value == ""){
		alert("Indirizzo Email obbligatorio")
		frmSedeImpresa.txtEMail.focus() 
		return false
	}		
	pippo=ValidateEmail(frmSedeImpresa.txtEMail.value)
	if (pippo==false){
		alert("Indirizzo Email del Referente formalmente errato")
		frmSedeImpresa.txtEMail.focus() 
		return false
	}
	
	// Controllo Login Referente
	frmSedeImpresa.txtLogin.value=TRIM(frmSedeImpresa.txtLogin.value)
	var anyString = frmSedeImpresa.txtLogin.value;
	if (frmSedeImpresa.txtLogin.value == ""){
		alert("Il campo Login � obbligatorio")
		frmSedeImpresa.txtLogin.focus() 
		return false
	}
	for (var i=0; i<=anyString.length-1; i++){
		if(((anyString.charAt(i) >= "A") && (anyString.charAt(i) <= "Z")) || 
			((anyString.charAt(i) >= "a") && (anyString.charAt(i) <= "z")) || 
			((anyString.charAt(i) >= "0") && (anyString.charAt(i) <= "9")) ||
			(anyString.charAt(i) == ".") || (anyString.charAt(i) == "-") ||
			(anyString.charAt(i) == "_"))
		{
		}
		else
		{		
			frmSedeImpresa.txtLogin.focus();
		 	alert("Inserire nel campo Login un valore valido");
			return false
		}		
	}
			
	return true
}
</script>


<%
' Controllo i diritti dell'utente connesso.
' La variabile di sessione mask � cos� formatta:
' 1 Byte abilitazione funzione Impresa
' 2 Byte abilitazione funzione Sede_impresa
' 3 Byte abilitazione funzione Val_impresa
' dal 4� byte in poi si visualizzano le provincie.
		
sMask = Session("Diritti") 
sAction = Request ("Action") 
sData = date

if mid(Session("UserProfiles"),1,1) = "0" then
	sIsa = "0"
else
	sIsa = mid(Session("UserProfiles"),1,2)
end if

sidSede = Request.Form("idSede")
sCodImp = Request.Form("sCodImp")
%>

<table border="0" width="520" cellspacing="0" cellpadding="0" height="81">
   <tr>
     <td width="530" background="<%=Session("Progetto")%>/images/titoli/strumenti2b.gif" height="81" valign="bottom" align="right">
       <table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
         <tr>
           <td width="100%" valign="top" align="right"><b class="tbltext1a">Gestione&nbsp;Azienda&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></b></td>
         </tr>
       </table>
     </td>
   </tr>
</table>
<br>

<!--#include File="IMP_Label.asp"-->
<br>

<table cellpadding="0" cellspacing="0" width="500" border="0">
	<tr height="18">
		<td class="sfondomenu" height="18" width="67%">
			<span class="tbltext0"><b>&nbsp;GESTIONE SEDE</b></span></td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
		<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">(*) campi obbligatori</td>
    </tr>
	<tr width="371" class="SFONDOCOMM">
		<td colspan="3">Modificare i dati relativi alla sede dell'azienda.
		<a href="Javascript:Show_Help('/Pgm/help/GestProgetti/Azienda/Impresa_Censimento/IMP_ModSedeImpresa')"><img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
		</td>
	</tr>   
	<tr height="2">
		<td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif">
		</td>
	</tr>
</table>

<br>
<table border="0" width="500">
     <!--'simona-->
<%     
'	if session("progetto")<>"/PLAVORO" then
%>
	<tr align="center">
		<form method="post" name="FormLogin" action="IMP_RilLogin.asp">
			<input type="hidden" name="idimpresa" value="<%=Request("sCodImp")%>">
			<input type="hidden" name="idsede" value="<%=sidSede%>">											
			<td colspan="2" align="middle">
				<a class="textred" href="javascript:FormLogin.submit()" onmouseover="javascript: window.status= ' '; return true"><b>Richiesta Login/Password</b></a>
			</td>
		</form>
	</tr>
<%
'	end if
%>
	
</table>
<br>

<%			
sSQL = "SELECT I.COD_TIMPR,S.DESCRIZIONE,S.COD_SEDE,S.INDIRIZZO,S.ID_CIMPIEGO," & _
	   "S.COMUNE,S.PRV,S.CAP,S.REF_SEDE,S.COD_RUO,S.NUM_TEL_SEDE," & _
	   "S.FAX_SEDE,S.E_MAIL_SEDE,S.NUM_TEL,S.FAX,S.E_MAIL,S.DT_TMST" & _
	   " FROM SEDE_IMPRESA S, IMPRESA I WHERE S.ID_IMPRESA=" & _
	   sCodImp & "  AND S.ID_SEDE = " & clng(sidSede) & "AND S.ID_IMPRESA=I.ID_IMPRESA"
	   
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
set rsSede = CC.Execute(sSQL)
if not rsSede.eof then
	'''Sede
	sCodTimpr = rsSede("COD_TIMPR")
	sDescrizione = trim(rsSede("DESCRIZIONE"))
	sCodSede = rsSede("COD_SEDE")
	sIndirizzo = trim(rsSede("INDIRIZZO"))
	sComune	= trim(rsSede("COMUNE"))
	sProv = rsSede("PRV")
	sDescProv = DecCodVal("PROV",0,date(),sProv,1)
	sCap = rsSede("CAP")
	sSedeTel = rsSede("NUM_TEL_SEDE")
	sSedeFax = rsSede("FAX_SEDE")
	sSedeEmail= rsSede("E_MAIL_SEDE")
	'''Referente
	sRefSede = trim(rsSede("REF_SEDE"))
	sCodRuo	= rsSede("COD_RUO")
	sNumTel	= trim(rsSede("NUM_TEL"))
	sFax = trim(rsSede("FAX"))
	sEmail = trim(rsSede("E_MAIL"))

	sDtTmst	= rsSede("DT_TMST")
	IdCimpiego = rsSede("ID_CIMPIEGO")

	if IdCimpiego <> "" then
		sSQL="SELECT DESCRIZIONE " &_
			 "FROM SEDE_IMPRESA " &_
			 "WHERE ID_SEDE = " & IdCimpiego
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
		Set rstCimpiego = CC.execute(sSQL)
		If not rstCimpiego.eof then
			DescCimpiego=rstCimpiego("DESCRIZIONE")
		End if
		rstCimpiego.Close()
	else
		DescCimpiego=""
	end if
end if
rsSede.Close 

%>
<form name="Indietro" method="post" action="IMP_VisSedeImpresa.asp">
	<input type="hidden" name="IdPers" value="<%=sIdPers%>">
	<input type="hidden" name="sCodImp" value="<%=sCodImp%>">
	<input type="hidden" name="Provincia" value="<%=sProv%>">
</form>	

<form method="post" name="frmSedeImpresa" onsubmit="return ControllaDati(this)" action="IMP_CnfSedeImpresa.asp" method="post">
	<input type="hidden" name="Action" value="Mod">
	<input type="hidden" name="DtTmst" value="<%=sDtTmst%>">
	<input type="hidden" name="sCodImp" value="<%=sCodImp%>">
	<input type="hidden" name="txtSedeImp" value="<%=sIdSede%>">
	<table border="0" cellpadding="0" cellspacing="1" width="450">		
		<tr>
   			<td align="left" colspan="2" nowrap class="tbltext1">
				<b>Descrizione*</b>
			</td>						
			<td align="left" colspan="2" width="60%">
				<input name="txtDescrizione" style="TEXT-TRANSFORM:uppercase" class="textblack" size="40" maxlength="50" value="<%=sDescrizione%>">
			</td>
		</tr>      
		<tr>
			<td align="left" colspan="2" nowrap class="tbltext1">
				<b>Tipo sede*</b>
            </td>
            <td align="left" colspan="2" width="60%" class="table">
<%
				sInt = "TSEDE|" & sisa & "|" & date & "|" & sCodSede & "|cmbCodSede| ORDER BY DESCRIZIONE"			
				CreateCombo(sInt)
%>
            </td>
		</tr>
		<tr>
			<td align="left" colspan="2" class="tbltext1">
				<b>Indirizzo*</b>
			</td>
			<td align="left" colspan="2" width="60%">
				<input name="txtIndirizzo" value="<%=sIndirizzo%>" style="TEXT-TRANSFORM:uppercase" size="40" maxlength="50" class="textblack">
			</td>
        </tr>
        <tr height="20">
            <td align="left" colspan="2" class="tbltext1">
				<b>Provincia</b>
            </td>
            <td align="left" colspan="2" width="60%" class="textblack">
				<input type="text" name="txtProv" value="<%=sDescProv%>" size="20" style="TEXT-TRANSFORM:uppercase" maxlength="10" class="textblack" readonly>					
				<input type="hidden" name="cmbProv" value="<%=sProv%>">
            </td>
        </tr>
        <tr>
            <td align="left" colspan="2" class="tbltext1">
				<b>Comune*</b>
            </td>
			<td align="left" colspan="2" width="60%" nowrap>
				<span class="tbltext">
<%
				if sComune <> "" then
					'Response.Write "1"
					'''sSQL ="Select DESCOM,PRV from Comune where CODCOM='" & sComune & "'"
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
					'''set rsComune = CC.Execute(sSQL)
%>
					<!--input type="text" name="txtComune" style="TEXT-TRANSFORM: uppercase;" class="textblack" size="40" readonly value="<%'''=rsComune("DESCOM")%>"-->
					<input type="text" name="txtComune" style="TEXT-TRANSFORM: uppercase;" class="textblack" size="40" readonly value="<%=DescrComune(sComune)%>">
<%
				else
					
%>
					<input type="text" name="txtComune" style="TEXT-TRANSFORM: uppercase;" class="textblack" size="40" readonly>
<%												
				end if	
%>

				<input type="hidden" name="txtComuneHid" value="<%=sComune%>">
<%
				NomeForm="frmSedeImpresa"
				CodiceProvincia="cmbProv"
				NomeComune="txtComune"
				CodiceComune="txtComuneHid"
				Cap="txtCapRes"
%>
				<a href="Javascript:SelComune('<%=NomeForm%>','<%=CodiceProvincia%>','<%=NomeComune%>','<%=CodiceComune%>','<%=Cap%>')" ID="imgPunto1" name="imgPunto1" onmouseover="javascript:window.status='';return true"><img border="0" src="<%=Session("Progetto")%>/images/bullet1.gif"></a>
				</span>				
			</td>
            
        </tr>
        <tr>
            <td align="left" colspan="2" class="tbltext1">
				<b>CAP*</b>
            </td>
            <td align="left" colspan="2" width="60%">
				<input name="txtCapRes" value="<%=sCAP%>" class="textblack" style="TEXT-TRANSFORM:uppercase" size="10" maxlength="5">
            </td>
        </tr>
        <tr>
            <td align="left" colspan="2" class="tbltext1">
			    <b>Telefono</b>
            </td>
            <td align="left" colspan="2" width="60%" class="table">
				<input name="txtSedeTel" class="textblack" style="TEXT-TRANSFORM:uppercase" size="25" maxlength="20" value="<%=sSedeTel%>">
            </td>
        </tr>        
        <tr>
            <td align="left" colspan="2" class="tbltext1">
			    <b>Fax</b>
            </td>
            <td align="left" colspan="2" width="60%" class="table">
				<input name="txtSedeFax" class="textblack" style="TEXT-TRANSFORM:uppercase" size="25" maxlength="20" value="<%=sSedeFax%>">
            </td>
        </tr>
        <tr>
            <td align="left" colspan="2" class="tbltext1">
			    <b>E-Mail</b>
            </td>
            <td align="left" colspan="2" width="60%" class="table">
				<input name="txtSedeEmail" class="textblack" style="TEXT-TRANSFORM:lowercase" size="40" maxlength="100" value="<%=sSedeEmail%>">
            </td>
        </tr>
<%
	if sCodTimpr <> "03" then
%>        
		<tr>
			<td align="left" class="tbltext1" nowrap colspan="2"><b>
				Centro Impiego*</b>
			</td>
			<td align="left" colspan="2" width="60%">
				<span class="table">
				<%'txtCentroImpiego%>
				<input type="text" name="txtCentroImpiego" readonly class="textblack" size="40" value="<%=DescCimpiego%>"> 
				<input type="hidden" name="txtProvinciaOld" value="<%=sProv%>">
				<input type="hidden" name="txtCentroImpiegoHid" value="<%=IdCimpiego%>">
				<a href="Javascript:SelCentroImpiegoMod('txtCentroImpiego')" onmouseover="javascript:window.status='';return true" ID="imgPunto1" name="imgPunto1"><img border="0" src="<%=Session("Progetto")%>/images/bullet1.gif"></a>
				</span>				
			</td>
		</tr>		
<%
	else
%>
		<input type="hidden" name="txtCentroImpiego" value="� un centro impiego"> 
		<input type="hidden" name="txtCentroImpiegoHid" value="CentroImpiego">		
		<input type="hidden" name="txtProvinciaOld" value="<%=sProv%>">
<%
	end if
%>		        

        <tr>
            <td align="left" colspan="2">&nbsp;</td>
            <td align="left" colspan="2" width="60%">&nbsp;</td>
        </tr>
<%
	'''LC**:8/10/03 - ripristinata visualizzazione dati referente.
	'''if Session("progetto") = "/PLAVORO" then
	if sRefSede <> "" then
%>
        <tr>
            <td align="left" colspan="2" class="tbltext1">
				<b>Referente*</b>(Cognome/Nome)
            </td>
            <td align="left" colspan="2" width="60%">
				<input name="txtRef" class="textblack" size="40" style="TEXT-TRANSFORM:uppercase" maxlength="50" value="<%=sRefSede%>" readonly>
				<input type="hidden" name="txtRefOld" value="<%=sRefSede%>">
            </td>
        </tr>
        <tr>
            <td align="left" colspan="2" class="tbltext1">
				<b>Ruolo*</b>
            </td>
            <td align="left" colspan="2" width="60%">
<%
				sInt = "RUOLO|0|" & date & "|" & sCodRuo & "|cmbRefRuo| ORDER BY DESCRIZIONE"			
				CreateCombo(sInt)
%>
            </td>
		</tr>
        <tr>
            <td align="left" colspan="2" class="tbltext1">
				<b>Telefono</b>
            </td>
            <td align="left" colspan="2" width="60%">
				<input name="txtTel" class="textblack" value="<%=sNumTel%>" style="TEXT-TRANSFORM:uppercase" size="25" maxlength="20" readonly>
            </td>
        </tr>
       <tr>
            <td align="left" colspan="2" class="tbltext1">
			    <b>Fax</b>
            </td>
            <td align="left" colspan="2" width="60%" class="table">
				<input name="txtFAX" class="textblack" style="TEXT-TRANSFORM:uppercase" size="25" maxlength="20" value="<%=sFax%>" readonly>
            </td>
        </tr>
        <tr>
            <td align="left" colspan="2" class="tbltext1">
				<b>E-Mail*</b>
            </td>
            <td align="left" colspan="2" width="60%">
				<input name="txtEMail" class="textblack" value="<%=sEmail%>" size="40" maxlength="100" readonly>
				<input type="hidden" name="txtEMailOld" value="<%=sEmail%>">
            </td>
        </tr>
        <%
        '''LC**:9/10/03 - tolta visualizzazione login.%>
'PL-SQL * T-SQL  
SSQL			'''IF RSUTENTE.EOF THEN			'''	SLOGIN=""			'''ELSE			'''	SLOGIN=RSUTENTE"LOGIN"			'''	RSUTENTE.CLOSE			'''END IF			'''SET RSUTENTE = NOTHING 			%>            <TD ALIGN="LEFT" COLSPAN="2" WIDTH="60%">				<INPUT TYPE="HIDDEN" NAME="TXTLOGINOLD" VALUE="<%=SLOGIN%>">				<INPUT NAME="TXTLOGIN" CLASS="TEXTBLACK" VALUE="<%=SLOGIN%>" STYLE="TEXT-TRANSFORM:UPPERCASE" SIZE="40" MAXLENGTH="20">            </TD>        </TR--> = TransformPLSQLToTSQL (SSQL			'''IF RSUTENTE.EOF THEN			'''	SLOGIN=""			'''ELSE			'''	SLOGIN=RSUTENTE"LOGIN"			'''	RSUTENTE.CLOSE			'''END IF			'''SET RSUTENTE = NOTHING 			%>            <TD ALIGN="LEFT" COLSPAN="2" WIDTH="60%">				<INPUT TYPE="HIDDEN" NAME="TXTLOGINOLD" VALUE="<%=SLOGIN%>">				<INPUT NAME="TXTLOGIN" CLASS="TEXTBLACK" VALUE="<%=SLOGIN%>" STYLE="TEXT-TRANSFORM:UPPERCASE" SIZE="40" MAXLENGTH="20">            </TD>        </TR-->) 
        <!--tr>            <td align="left" colspan="2" class="tbltext1">				<b>Login*</b>            </td>			<%			'''sSQL = "select login from utente where	 ind_abil='S' and tipo_pers='S' and creator=" & clng(sidSede)					'''set rsUtente = CC.Execute(sSQL)			'''if rsUtente.eof then			'''	sLogin=""			'''else			'''	sLogin=rsUtente("Login")			'''	rsUtente.close			'''end if			'''set rsUtente = nothing 			%>            <td align="left" colspan="2" width="60%">				<input type="hidden" name="txtLoginOld" value="<%=sLogin%>">				<input name="txtLogin" class="textblack" value="<%=sLogin%>" style="TEXT-TRANSFORM:uppercase" size="40" maxlength="20">            </td>        </tr-->
<%
	end if %>

</table>

<%
'if Session("progetto") = "/PLAVORO" then%>
<!--	<input type="hidden" name="Plavoro" value="SI">  -->
<%
'else%>
	<input type="hidden" name="Plavoro" value="NO">
<%
'end if%>


<br>
<table border="0" width="500">
	<tr>
		<td align="right">
			<a class="textred" href="javascript:Indietro.submit()" onMouseover="window.status = ''; return true;">
				<img src="<%=Session("Progetto")%>/images/Indietro.gif" border="0">
			</a>
        </td>
        <td width="5%"></td>
        <td align="left">
<% 
			if 	ckProfile(mid(sMask,3,2),4) then %>
				<input type="image" name="Invia" src="<%=Session("Progetto")%>/images/conferma.gif" border="0" value="Conferma" onclick="return ControllaDati(this)">
<% 
			end if 
%>
		</td>
	</tr>
</table>
    
</form>        
	
<!--#include virtual="/include/closeconn.asp"-->
<!--#include virtual="/strutt_coda2.asp"-->
