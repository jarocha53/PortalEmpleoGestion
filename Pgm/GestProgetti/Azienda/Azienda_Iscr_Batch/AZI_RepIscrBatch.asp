<%'Option Explicit 
   Response.ExpiresAbsolute = Now() - 1 
   Response.AddHeader "pragma","no-cache"
   Response.AddHeader "cache-control","private"
   Response.CacheControl = "no-cache"
%>
<!-- #include virtual="/strutt_testa2.asp"-->
<!--#include virtual = "/include/openconn.asp"-->
<!--#include virtual="/util/portallib.asp"-->
<!--#include virtual ="/include/controldatevb.asp"-->

<!-- ************** Javascript inizio ************ -->
<%
if ValidateService(session("idutente"),"AZI_REPISCRBATCH", CC) <> "true" then 
	response.redirect "/util/error_login.asp"
end if
%>

<script LANGUAGE="JavaScript">
<!--#include Virtual = "/Include/help.inc"-->

//include del file per fare i controlli sulla validit� delle date
<!--#include virtual="/Include/ControlDate.inc"-->

<!--#include virtual="/Include/ControlString.inc"-->


function VaiInizio(){
	location.href = "AZI_RepIscrBatch.asp";
}	

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

function Scarica(report){	
	
	fin=window.open(report,"","toolbar=0;location=0,directories=0,status=0,menubar=1,scrollbars=1,resizable=1,copyhistory=0,width=900,height=600,screenX=200,screenY200");
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

function ControllaDati(){

	// Controllo Date
	frmRepIscrBatchAzi.txtDtRic.value = TRIM(frmRepIscrBatchAzi.txtDtRic.value)
	
	if (frmRepIscrBatchAzi.txtDtRic.value != ""){
		if (!ValidateInputDate(frmRepIscrBatchAzi.txtDtRic.value)){
			frmRepIscrBatchAzi.txtDtRic.focus();
			return false;
		}
		if (ValidateRangeDate(frmRepIscrBatchAzi.txtDtRic.value, frmRepIscrBatchAzi.txtDtOggi.value) == false){
			alert("La data di ricerca deve essere minore della data odierna")
			frmRepIscrBatchAzi.txtDtRic.focus();
			return false;
		}				
	}
	return true;
}
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

</script>

<!-- ******************  Javascript Fine *********** -->
<!-- ************** ASP inizio *************** -->

<%
'----------------------------------------------------------------------------------------------------------------------------------------------------  
sub Msgetto(sTesto)

	%>
	<br><br>
	<table width="500" align="center" border="0" cellspacing="2" cellpadding="1">
		<tr height="20"> 
		  	<td class="tbltext3" align="middle" width="500">
				<b><%=sTesto%></b>
			</td>
		</tr>
	</table>
	<br><br>
	<%

end sub

'----------------------------------------------------------------------------------------------------------------------------------------------------  

sub Inizio()
%>
	<table border="0" width="520" cellspacing="0" cellpadding="0" height="81">
	   <tr>
	     <td width="500" background="<%=Session("Progetto")%>/images/titoli/strumenti2b.gif" height="81" valign="bottom" align="right">
	
	       <table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
	         <tr>
	             <td width="100%" valign="top" align="right">
	                 <b class="tbltext1a">Gestione Progetti</span></b>
	             </td>
	         </tr>
	       </table>
	     </td>
	   </tr>
	</table>
	<br>
	<table cellpadding="0" cellspacing="0" width="500" border="0">
		<tr height="18">
			<td class="sfondomenu" height="18" width="67%">
			<span class="tbltext0"><b>REPORT AZIENDE ISCRITTE IN MODALITA'BATCH</b></span></td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
			<td valign="middle" align="right" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1"></td>
		</tr>
		<tr width="371" class="SFONDOCOMM">
			<td colspan="3">Per la visualizzazione dei reports cliccare sui link sottostanti.
			<a href="Javascript:Show_Help('/Pgm/help/GestProgetti/Alfabeta/Selezione/SEL_RepIscrBatch')" onmouseover="javascript:window.status=' '; return true"><img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
			</td>
		</tr>
		<tr height="2">
			<td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>
	<br>
<%
end sub

'----------------------------------------------------------------------------------------------------------------------------------------------------  

sub ImpDtRicerca()
	
	dtOggi = Day(Now) & "/" & Month(Now) & "/" & Year(Now)
	'Response.Write dtOggi
	%>
	<br>    	
	<form name="frmRepIscrBatchAzi" onsubmit="return ControllaDati()" action="AZI_RepIscrBatch.asp" method="post">
		<input type="hidden" name="txtPrimaVolta" value="NO">
		<input type="hidden" name="txtDtOggi" value="<%=dtOggi%>">
		
		<table border="0" cellpadding="1" cellspacing="2" width="500">
			<tr>
				<td align="left" class="tbltext1" width="45%">
					<b>Data elaborazione</b><font size="1"> (gg/mm/aaaa)</font>
				</td>	
				<td align="left" width="55%">
					<input type="text" style="TEXT-TRANSFORM: uppercase" size="10" maxlength="10" class="textblacka" name="txtDtRic" value="<%=dtOggi%>">
				</td>
			</tr>
		</table>
		
		<br><br>
		
		<table width="500" cellspacing="2" cellpadding="1" border="0" align="center">
			<tr align="center">
				<td>
				<input type="image" src="<%=Session("progetto")%>/images/lente.gif" title="Avvia la ricerca" border="0" align="absBottom" id="image1" name="image1">
				</td>
			</tr>		
		</table>
	</form>	

	<%
end sub

'----------------------------------------------------------------------------------------------------------------------------------------------------  

sub ImpostaPag()

Dim fsDir, FileList, sFileName

dim aFileOk, indOk

	Set fso = CreateObject("Scripting.FileSystemObject")

	sPath= session("progetto") & "/DocPers/CreaLoginAziende/"
	fsDir= replace(Server.MapPath("\") & sPath, "/", "\")
'Response.Write fsDir

	Set f = fso.GetFolder(fsDir)
	Set FileList = f.Files

	if CLng(f.Files.Count) = 0 then
		Msgetto("Non ci sono report da visualizzare.")
		exit sub
	end if

	redim aFileOk(0)
	
	indOk = 0
	
	' creo un arrey che contiene la data di creazione del file(scritta nel nome_file)
	'girata in formata aaaammgg prendendo solo i file di rilascio login-password
	For Each sFileName in FileList
		if Trim(UCase(mid(sFileName.Name, 1, 3))) = "RIL" then
			if indOk > 0 then
				redim preserve aFileOk(indOk)
			end if			
			aFileOk(indOk) = mid(sFileName.Name, 20, 4) & mid(sFileName.Name, 17, 2) & mid(sFileName.Name, 14, 2)
			indOk = indOk + 1
		end if
	Next 'sFileName

	'ordinamento decrescente per data
	for y = 0 to ubound(aFileOk)
		for z = y to ubound(aFileOk)
			if aFileOk(z) > aFileOk(y) then
				appo = aFileOk(y)
				aFileOk(y) = aFileOk(z)
				aFileOk(z) = appo
			end if
		next	
	next
	%>
	<table width="500" cellspacing="2" cellpadding="1" border="0">
		<tr class="sfondocomm">
			<td class="tbltext1">
				<b>LOGIN ASSEGNATE IL</b>
			</td>
			<td class="tbltext1">
				<b>AZIENDE ASSOCIATE SENZA LOGIN IL</b>
			</td>
		</tr>
	<%
	
	'se � stata digitata una data di ricerca la giro nel formato aaaammgg
	'altrimenti metto zero per far funzionare in ogni caso la visualizzazione
	if Trim(dDtRic) <> "" then
		dDtRic = mid(dDtRic, 7, 4) & mid(dDtRic, 4, 2) & mid(dDtRic, 1, 2)
	else
		dDtRic = 0
	end if
	
	for indOk = 0 to UBound(aFileOk)
	
		dDtElab = mid(aFileOk(indOk), 7, 2) & "_" & mid(aFileOk(indOk), 5, 2) & "_" & mid(aFileOk(indOk), 1, 4)

		sNomeFileOk = "Ril_login_az_" & dDtElab & ".xls"
		sNomeFileKo = "Error_ril_login_az_" & dDtElab & ".xls"
		dDtElab = Replace(dDtElab, "_", "/")
		
		if aFileOk(indOk) >= dDtRic then
			%>	
			<tr class="sfondocomm">
				<td align="center">
					<a href="javascript:Scarica('<%=sPath & sNomeFileOk%>')" class="tblDett">
						<b><%=dDtElab%></b>
						<img border="0" src="<%=Session("Progetto")%>/images/scheda.gif" alt="Visualizza il file report">
					</a>
				</td>
				<td align="center">
					<a href="javascript:Scarica('<%=sPath & sNomeFileKo%>')" class="tblDett">
						<b><%=dDtElab%></b>
						<img border="0" src="<%=Session("Progetto")%>/images/scheda.gif" alt="Visualizza il file report">
					</a>
				</td>
			</tr>
			<%
		end if		
	next
	%>		
	</table>
	<%	
	
end sub

'-----------------------------------------------------------------------------------------------------------------------------------------------------------

sub Fine()
%>
<!--#include virtual = "/include/closeconn.asp"-->
<!--#include virtual = "strutt_coda2.asp"-->
<%
end sub

'----------------------------------------- MAIN ------------------------------------------------------------------------------------------------------------------
dim swPrimaVolta
dim dDtRic

Inizio()

swPrimaVolta = Request.Form("txtPrimaVolta")

if swPrimaVolta = "" then
	ImpDtRicerca()
else
	dDtRic = Request.Form("txtDtRic")
	ImpostaPag()
end if

Fine()

%>