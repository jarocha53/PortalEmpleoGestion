<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->

<!-- #include virtual="/strutt_testa2.asp"-->
<!--#include Virtual = "/Include/OpenConn.asp"-->
<!--#include virtual = "/include/ControlDateVB.asp"-->
<!--#include virtual = "/util/dbutil.asp"-->
<%
if ValidateService(session("idutente"),"AZI_SELFILTRI", CC) <> "true" then 
	response.redirect "/util/error_login.asp"
end if
%>

<script language="javascript" src="ControlliImp.js"></script>
<script language="javascript">
<!--#include virtual = "/Include/help.inc"-->

//funzione di selezione dei comuni
<!--#include virtual = "/include/SelComune.js"-->

//funzione di selezione dei settori
<!--#include virtual = "/include/SelSettore.js"-->

//funzione di selezione dei CCNL
<!--#include virtual = "/include/SelCcnl.js"-->

//include del file per fare i controlli sulla validit� delle date
<!--#include virtual="/Include/ControlDate.inc"-->

//include del file per fare i controlli sulla numericit� dei campi
<!--#include virtual="/Include/ControlNum.inc"-->
<!--#include virtual="/Include/ControlString.inc"-->


function ControllaDati(frmSelFiltri){

	// ragione sociale corretta
	frmSelFiltri.txtRagSoc.value = TRIM(frmSelFiltri.txtRagSoc.value)
	if 	(!ValidateInputStringWithNumber(frmSelFiltri.txtRagSoc.value)) {
		alert("Il campo  Ragione sociale  � formalmente errato")
		frmSelFiltri.txtRagSoc.focus();
		return false;
	}

	// partita iva corretta	
	//controlli asteriscati perch� 
	//sul db � alfanumerico, per ricerca posso inserire anche solo un carattere
//	if (frmSelFiltri.txtPartIva.value.length > 0 ){ 
//		if (!IsNum(frmSelFiltri.txtPartIva.value)){
//			alert("Il campo Partita Iva deve essere numerico")
//			frmSelFiltri.txtPartIva.focus() 
//			return false
//		}
//		if (frmSelFiltri.txtPartIva.value.length != 11){
//			alert("Il campo Partita Iva deve essere di 11 caratteri")
//			frmSelFiltri.txtPartIva.focus() 
//			return false
//		}
//	}

	// codice fiscale corretto
	frmSelFiltri.txtCodFisc.value=TRIM(frmSelFiltri.txtCodFisc.value)
	if 	(frmSelFiltri.txtCodFisc.value.length > 0 ){
		if 	(!ValidateInputStringWithNumber(frmSelFiltri.txtCodFisc.value)){
			alert("Il campo Codice Fiscale � formalmente errato")
			frmSelFiltri.txtCodFisc.focus();
			return false;
		}
		blank = " ";
		if (!ChechSingolChar(frmSelFiltri.txtCodFisc.value, blank)){
			alert("Il campo Codice Fiscale non pu� contenere spazi")
			frmSelFiltri.txtCodFisc.focus();
			return false;
		}
		//if ((frmSelFiltri.txtCodFisc.value.length != 11) && (frmSelFiltri.txtCodFisc.value.length != 16)){
		//	alert("Il codice Fiscale  deve essere di 11  o 16 caratteri")
		//	frmSelFiltri.txtCodFisc.focus(); 
		//	return false;
		//}
	}

	//Controllo che se inserita la data validit� sia inserito il CCNL 
	frmSelFiltri.txtDtValid.value = TRIM(frmSelFiltri.txtDtValid.value)
	if (frmSelFiltri.txtDtValid.value != "" && frmSelFiltri.txtCcnlHid.value == ""){
		alert("Selezionare il Contratto");
		frmSelFiltri.txtCcnlHid.focus();
		return false
	}

	// Controllo Date

	// data costituzione corretta
	frmSelFiltri.txtDtCost.value = TRIM(frmSelFiltri.txtDtCost.value)

	if (frmSelFiltri.txtDtCost.value != ""){
		if (!ValidateInputDate(frmSelFiltri.txtDtCost.value)){
			frmSelFiltri.txtDtCost.focus() 
			return false
		}
		if (ValidateRangeDate(frmSelFiltri.txtDtCost.value, frmSelFiltri.txtoggi.value) == false){
			alert("Il campo Data di costituzione deve essere minore della data odierna")
			frmSelFiltri.txtDtCost.focus()
			return false
		}				
	}

	// data validit� del CCNL corretta

	if (frmSelFiltri.txtDtValid.value != ""){
		if (!ValidateInputDate(frmSelFiltri.txtDtValid.value)){
			frmSelFiltri.txtDtValid.focus() 
			return false
		}
		if (ValidateRangeDate(frmSelFiltri.txtDtValid.value, frmSelFiltri.txtoggi.value) == false){
			alert("Il campo Data di validit� del CCNL non pu� essere superiore della data odierna")
			frmSelFiltri.txtDtValid.focus()
			return false
		}				
	}

//	frmSelFiltri.txtDtIniVal.value = TRIM(frmSelFiltri.txtDtIniVal.value)
//	frmSelFiltri.txtDtFinVal.value = TRIM(frmSelFiltri.txtDtFinVal.value)
//	DataInizio = frmSelFiltri.txtDtIniVal.value;
//	DataFine = frmSelFiltri.txtDtFinVal.value;
//	DataOggi = frmSelFiltri.txtoggi.value;

	// se valorizzata data fine non pu� essere vuota data inizio
//	if (DataInizio = "" && DataFine != ""){
//		alert("Valorizzare la data inizio");
//		frmSelFiltri.txtDtIniVal.focus() 
//		return false
//	}

	// data Inizio
//	if (DataInizio != ""){
//		if (!ValidateInputDate(DataInizio)){
//			frmSelFiltri.txtDtIniVal.focus() 
//			return false
//		}
//		if (DataInizio != DataOggi){
//			if (ValidateRangeDate(DataOggi,DataInizio)){
//				alert("La data di Inizio validit� non pu� essere superiore della data odierna")
//				frmSelFiltri.txtDtIniVal.focus()
//				return false
//			}			
//		}
//	}	

	//  data fine 
//	if (DataFine != ""){
//		if (!ValidateInputDate(DataFine)){
//			frmSelFiltri.txtDtFinVal.focus() 
//			return false
//		}
//		if (ValidateRangeDate(DataFine, DataInizio)){
//			alert("La data di Fine validit� deve essere superiore alla data di Inzio validit�")
//			frmSelFiltri.txtDtFinVal.focus()
//			return false
//		}				
//	}	
				
//controlli relazione tra operatori logici e campi corrispondenti
	frmSelFiltri.txtRagSoc.value = TRIM(frmSelFiltri.txtRagSoc.value);
	
	if(frmSelFiltri.txtRagSoc.value != "" && frmSelFiltri.cmbOpeRagSoc.value == "") {
		alert("Selezionare l'operatore logico");
		frmSelFiltri.cmbOpeRagSoc.focus();
		return false;
	}
	
	if(frmSelFiltri.txtPartIva.value != "" && frmSelFiltri.cmbOpePartIva.value == "") {
		alert("Selezionare l'operatore logico");
		frmSelFiltri.cmbOpePartIva.focus();
		return false;
	}

	frmSelFiltri.txtCodFisc.value = TRIM(frmSelFiltri.txtCodFisc.value)
	if(frmSelFiltri.txtCodFisc.value != "" && frmSelFiltri.cmbOpeCodFisc.value == "") {
		alert("Selezionare l'operatore logico");
		frmSelFiltri.cmbOpeCodFisc.focus();
		return false;
	}

	frmSelFiltri.txtDtCost.value = TRIM(frmSelFiltri.txtDtCost.value)
	if(frmSelFiltri.txtDtCost.value != "" && frmSelFiltri.cmbOpeDtCost.value == "") {
		alert("Selezionare l'operatore logico");
		frmSelFiltri.cmbOpeDtCost.focus();
		return false;
	}

	frmSelFiltri.cmbTipImpr.value = TRIM(frmSelFiltri.cmbTipImpr.value)
	if(frmSelFiltri.cmbTipImpr.value != "" && frmSelFiltri.cmbOpeTipImpr.value == "") {
		alert("Selezionare l'operatore logico");
		frmSelFiltri.cmbOpeTipImpr.focus();
		return false;
	}

	frmSelFiltri.cmbFGiur.value = TRIM(frmSelFiltri.cmbFGiur.value)
	if(frmSelFiltri.cmbFGiur.value != "" && frmSelFiltri.cmbOpeFGiur.value == "") {
		alert("Selezionare l'operatore logico");
		frmSelFiltri.cmbOpeFGiur.focus();
		return false;
	}

	frmSelFiltri.txtSettoreHid.value = TRIM(frmSelFiltri.txtSettoreHid.value)
	if(frmSelFiltri.txtSettoreHid.value != "" && frmSelFiltri.cmbOpeSettore.value == "") {
		alert("Selezionare l'operatore logico");
		frmSelFiltri.cmbOpeSettore.focus();
		return false;
	}
	
	frmSelFiltri.txtCcnlHid.value = TRIM(frmSelFiltri.txtCcnlHid.value)
	if(frmSelFiltri.txtCcnlHid.value != "" && frmSelFiltri.cmbOpeCcnl.value == "") {
		alert("Selezionare l'operatore logico");
		frmSelFiltri.cmbOpeCcnl.focus();
		return false;
	}

	frmSelFiltri.cmbProv.value = TRIM(frmSelFiltri.cmbProv.value)
	if(frmSelFiltri.cmbProv.value != "" && frmSelFiltri.cmbOpeProv.value == "") {
		alert("Selezionare l'operatore logico");
		frmSelFiltri.cmbOpeProv.focus();
		return false;
	}

	frmSelFiltri.txtComune.value = TRIM(frmSelFiltri.txtComune.value)
	if(frmSelFiltri.txtComune.value != "" && frmSelFiltri.cmbOpeComune.value == "") {
		alert("Selezionare l'operatore logico");
		frmSelFiltri.cmbOpeComune.focus();
		return false;
	}

	frmSelFiltri.txtCentroImpiego.value = TRIM(frmSelFiltri.txtCentroImpiego.value)
	if(frmSelFiltri.txtCentroImpiego.value != "" && frmSelFiltri.cmbOpeCImp.value == "") {
		alert("Selezionare l'operatore logico");
		frmSelFiltri.cmbOpeCImp.focus();
		return false;
	}

	frmSelFiltri.cmbTipSede.value = TRIM(frmSelFiltri.cmbTipSede.value)
	if(frmSelFiltri.cmbTipSede.value != "" && frmSelFiltri.cmbOpeTipSede.value == "") {
		alert("Selezionare l'operatore logico");
		frmSelFiltri.cmbOpeTipSede.focus();
		return false;
	}
		
	return true;
}

</script>

<%
'----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

sub Inizio()
%>
<table border="0" width="520" cellspacing="0" cellpadding="0" height="70" align="center">
		<tr>
			<td width="500" background="<%=session("progetto")%>/images/titoli/servizi2g.gif" height="70" valign="bottom" align="right">
				<table border="0" background width="500" height="23" cellspacing="0" cellpadding="0">
					<tr>
						<td width="100%" valign="top" align="right"><b CLASS="tbltext1a">Iscrizione Batch Aziende
						  </b></td>
					</tr>
				</table>
			</td>
		</tr>
	</table> 
	<br>
	<!-- Lingetta superiore -->
	<table border="0" CELLPADDING="0" CELLSPACING="0" width="500" align="center">
		<tr height="18">
			<td class="sfondomenu" width="67%" height="18"><span class="tbltext0"><b>&nbsp;GESTIONE ISCRIZIONE BATCH AZIENDE</b></span></td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
			<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
		</tr>
	</table>
	<!-- Commento -->
	<table border="0" CELLPADDING="0" CELLSPACING="0" width="500" align="center">
		<tr>
			<td align="left" class="sfondocomm">
				Scegliere i criteri di ricerca per effettuare la selezione.
				<a href="Javascript:Show_Help('/Pgm/help/GestProgetti/Azienda/AziendaIscrBatch/AZI_SelFiltri')"><img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
			</td>
		</tr>
		<tr height="2">
			<td class="sfondocommaz" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>
	<br>
<%
end sub

'----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

function decodOplog(sNomeCampo)
dim swTrovato
dim aAppo
DIM aRisult

	swTrovato = 0
	for i = 0 to UBound(aDizDati, 2)
		if aDizDati(0, i) = sNomeCampo then
			aAppo = Split(aDizDati(1, i), "|")
			swTrovato = 1
			exit for
		end if
	next
	
	if swTrovato = 1 then
		for y = 0 to UBound(aAppo)
			if aAppo(y) <> "08" and aAppo(y) <> "09" and aAppo(y) <> "10" then
				if trim(aRisult) = "" then
					aRisult = aAppo(y)
				else
					aRisult = aRisult & "|" & aAppo(y)
				end if
			end if				
		next
	else
		aRisult = "*|*"
	end if
	
	decodOplog = split(aRisult, "|")
	
end function

'----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------		

function decodTades(sCodice)

	for i = 0 to UBound(aTades)
		if Mid(aTades(i), 1,instr(1, aTades(i), "|") - 1) = sCodice then
			decodTades = Split(aTades(i), "|")
			exit for
		end if
	next

end function

'----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------		

sub ImpostaPag()

dim rsOplog

redim aDizDati(1, 0)

	sAppo = decodTadesToArrayString("OPLOG" ,Date, "", 0, "0")
	aTades = Split(sAppo,"#")

	sSQL = "SELECT ID_CAMPO, TIPO_OPLOG FROM DIZ_DATI WHERE ID_TAB = 'IMPRESA'"
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	set rsOplog = CC.Execute(sSQL)
	
	if not rsOplog.EOF then
		rsOplog.movefirst
	end if

	Z = 0
	do while not rsOplog.EOF 
		aDizDati(0, Z) = rsOplog("ID_CAMPO")
		aDizDati(1, Z) = rsOplog("TIPO_OPLOG")
		rsOplog.movenext
		if not rsOplog.EOF then
			redim preserve aDizDati(1, Z + 1)
		end if
'		Response.Write "nome campo: " & aDizDati(0, Z) & "<BR>"
'		Response.Write "tipo oplog: " & aDizDati(1, Z) & "<BR>"
		Z = Z + 1
	loop
	%>
	<br>    	
	<form name="frmSelFiltri" onsubmit="return ControllaDati(this)" action="AZI_RicIscrBatch.asp" method="post">   
		<input type="hidden" name="txtoggi" value="<%=ConvDateToString(Date())%>">

		<table border="0" cellpadding="1" cellspacing="2" width="500">
			
			<tr>
				<td align="left" colspan="3" class="tbltext3">Dati dell'Impresa</td>
			</tr>
			<tr>
				<td align="left" colspan="2" class="tbltext3">&nbsp;</td>
			</tr>

			<tr>
				<td align="left" class="tbltext1" title="Ragione sociale">
					<b>Ragione sociale</b>
				</td>
				<td>
					<select class="textblacka" name="cmbOpeRagSoc">
					<option value></option>
					<%
					aTipoOplg = decodOplog("RAG_SOC")
					if aTipoOplg(0) <> "*" then
						for i = 0 to UBound(aTipoOplg)
							aAppo = decodTades(aTipoOplg(i))
							%>
							<option value="<%=aAppo(2)%>"><%=aAppo(1)%></option>
							<%					
							Erase aAppo
						next
						Erase aTipoOplg
					end if
					%>
 				</select>			
				</td>
				<td align="left">
					<input style="TEXT-TRANSFORM: uppercase;" size="50" maxlength="50" class="textblacka" name="txtRagSoc" value>
				</td>
			</tr>
			<tr>
				<td align="left" class="tbltext1" title="Partita iva">
					<b>Partita iva</b>
				</td>
				<td>
					<select class="textblacka" name="cmbOpePartIva">
					<option value></option>
					<%
					aTipoOplg = decodOplog("PART_IVA")
					
					if aTipoOplg(0) <> "*" then
						for i = 0 to UBound(aTipoOplg)
							aAppo = decodTades(aTipoOplg(i))
							%>
							<option value="<%=aAppo(2)%>"><%=aAppo(1)%></option>
							<%					
							Erase aAppo
						next
						Erase aTipoOplg
					end if
					%>
 				</select>			
				</td>				
				<td align="left">
					<input style="TEXT-TRANSFORM: uppercase;" size="11" maxlength="11" class="textblacka" name="txtPartIva" value>
				</td>
			</tr>
			<tr>
				<td align="left" class="tbltext1" title="Codice fiscale">
					<b>Codice fiscale</b>
				</td>
				<td>
					<select class="textblacka" name="cmbOpeCodFisc">
					<option value></option>
					<%
					aTipoOplg = decodOplog("COD_FISC")
					
					if aTipoOplg(0) <> "*" then
						for i = 0 to UBound(aTipoOplg)
							aAppo = decodTades(aTipoOplg(i))
							%>
							<option value="<%=aAppo(2)%>"><%=aAppo(1)%></option>
							<%					
							Erase aAppo
						next
						Erase aTipoOplg
					end if
					%>
 				</select>			
				</td>
				<td align="left">
					<input style="TEXT-TRANSFORM: uppercase;" size="16" maxlength="16" class="textblacka" name="txtCodFisc" value>
				</td>
			</tr>
				
			<tr>
				<td align="left" class="tbltext1" title="Data di costituzione">
					<b>Data costituzione</b><font size="1"> (gg/mm/aaaa)</font>
				</td>	
				<td>
					<select class="textblacka" name="cmbOpeDtCost">
					<option value></option>
					<%
					aTipoOplg = decodOplog("DT_COST")
					
					if aTipoOplg(0) <> "*" then
						for i = 0 to UBound(aTipoOplg)
							aAppo = decodTades(aTipoOplg(i))
							%>
							<option value="<%=aAppo(2)%>"><%=aAppo(1)%></option>
							<%					
							Erase aAppo
						next
						Erase aTipoOplg
					end if
					%>
 				</select>			
				</td>
				<td align="left">
					<input type="text" style="TEXT-TRANSFORM: uppercase" size="10" maxlength="10" class="textblacka" name="txtDtCost" value>
				</td>
			</tr>
			
			<tr>
				<td align="left" class="tbltext1" title="Tipologia">
					<b>Tipologia impresa</b>
				</td>
				<td>
					<select class="textblacka" name="cmbOpeTipImpr">
					<option value></option>
					<%
					aTipoOplg = decodOplog("COD_TIMPR")
					
					if aTipoOplg(0) <> "*" then
						for i = 0 to UBound(aTipoOplg)
							aAppo = decodTades(aTipoOplg(i))
							%>
							<option value="<%=aAppo(2)%>"><%=aAppo(1)%></option>
							<%					
							Erase aAppo
						next
						Erase aTipoOplg
					end if
					%>
 				</select>			
				</td>
				<td align="left">
					<%
					sAppo = "TIMPR|0|" & date & "||cmbTipImpr|ORDER BY DESCRIZIONE"			
					CreateCombo(sAppo)
					%>	
				</td>
			</tr>
			<tr>
				<td align="left" class="tbltext1" title="Forma giuridica">
					<b>Forma giuridica</b>
				</td>
				<td>
					<select class="textblacka" name="cmbOpeFGiur">
					<option value></option>
					<%
					aTipoOplg = decodOplog("COD_FORMA")
					
					if aTipoOplg(0) <> "*" then
						for i = 0 to UBound(aTipoOplg)
							aAppo = decodTades(aTipoOplg(i))
							%>
							<option value="<%=aAppo(2)%>"><%=aAppo(1)%></option>
							<%					
							Erase aAppo
						next
						Erase aTipoOplg
					end if
					%>
 				</select>			
				</td>
				<td align="left">
					<%
					sAppo = "FGIUR|0|" & date & "||cmbFGiur|ORDER BY DESCRIZIONE"			
					CreateCombo(sAppo)
					%>	
				</td>
			</tr>

			<tr>
				<td align="left" class="tbltext1" title="Settore merceologico">
					<b>Settore</b>
				</td>
				<td>
					<select class="textblacka" name="cmbOpeSettore">
					<option value></option>
					<%
					aTipoOplg = decodOplog("ID_SETTORE")
					
					if aTipoOplg(0) <> "*" then
						for i = 0 to UBound(aTipoOplg)
							aAppo = decodTades(aTipoOplg(i))
							%>
							<option value="<%=aAppo(2)%>"><%=aAppo(1)%></option>
							<%					
							Erase aAppo
						next
						Erase aTipoOplg
					end if
					%>
 				</select>			
				</td>

				<td align="left">
					<span class="tbltext">
					<input type="hidden" id="txtSettoreHid" name="txtSettoreHid" value>
					<input type="text" name="txtSettore" style="TEXT-TRANSFORM: uppercase;" class="textblack" size="62" readonly value>
						<%
						NomeForm="frmSelFiltri"
						NomeTxtCodSett="txtSettoreHid"
						NomeTxtDescSett="txtSettore"
						%>
					<a href="Javascript:SelSettore('<%=NomeForm%>','<%=NomeTxtDescSett%>','<%=NomeTxtCodSett%>')" ID="imgPunto1" name="imgPunto1" onmouseover="javascript:window.status='';return true">
					<img border="0" src="<%=Session("Progetto")%>/images/bullet1.gif"></a>
					</span>					
				</td>

			</tr>
<!-------------------------- CCNL ---------------------------------------------------------->
			<tr>
				<td colspan="2">&nbsp;
				</td>
			</tr>
			<tr height="2">
				<td class="sfondocommaz" colspan="4" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
			</tr>
			<tr>
				<td align="left" colspan="3" class="tbltext3">Dati del Contratto Nazionale</td>
			</tr>
			<tr>
				<td align="left" colspan="2" class="tbltext3">&nbsp;</td>
			</tr>

	<%
		Erase aDizDati
		Redim aDizDati(1, 0)
		sSQL = "SELECT ID_CAMPO, TIPO_OPLOG FROM DIZ_DATI WHERE ID_TAB = 'IMPRESA_CCNL'"
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
		set rsOplog = CC.Execute(sSQL)
		
		if not rsOplog.EOF then
			rsOplog.movefirst
		end if

		Z = 0
		do while not rsOplog.EOF 
			aDizDati(0, Z) = rsOplog("ID_CAMPO")
			aDizDati(1, Z) = rsOplog("TIPO_OPLOG")
			rsOplog.movenext
			if not rsOplog.EOF then
				redim preserve aDizDati(1, Z + 1)
			end if
	'		Response.Write "nome campo: " & aDizDati(0, Z) & "<BR>"
	'		Response.Write "tipo oplog: " & aDizDati(1, Z) & "<BR>"
			Z = Z + 1
		loop

		%>			
			<tr>
				<td align="left" class="tbltext1" title="Contratto nazionale applicato dall'azienda">
					<b>Ccnl</b>
				</td>
				<td>
					<select class="textblacka" name="cmbOpeCcnl">
					<option value></option>
					<%
					aTipoOplg = decodOplog("COD_CCNL")
					
					if aTipoOplg(0) <> "*" then
						for i = 0 to UBound(aTipoOplg)
							aAppo = decodTades(aTipoOplg(i))
							%>
							<option value="<%=aAppo(2)%>"><%=aAppo(1)%></option>
							<%					
							Erase aAppo
						next
						Erase aTipoOplg
					end if
					%>
 				</select>			
				</td>
				<td align="left">
					<span class="tbltext">
					<input type="hidden" id="txtCcnlHid" name="txtCcnlHid" value>
					<input type="text" name="txtCcnl" style="TEXT-TRANSFORM: uppercase;" class="textblack" size="62" readonly value>
						<%
						NomeForm="frmSelFiltri"
						NomeTxtCodCcnl="txtCcnlHid"
						NomeTxtDescCcnl="txtCcnl"
						%>
					<a href="Javascript:SelCcnl('<%=NomeForm%>','<%=NomeTxtDescCcnl%>','<%=NomeTxtCodCcnl%>')" ID="imgPunto1" name="imgPunto1" onmouseover="javascript:window.status='';return true">
					<img border="0" src="<%=Session("Progetto")%>/images/bullet1.gif"></a>
					</span>					
				</td>
			</tr>
			<!--tr>				<td align="left" class="tbltext1">					<b>Data inizio validit�</b>(gg/mm/aaaa)				</td>					<td align="left">					<input type="text" style="TEXT-TRANSFORM: uppercase" maxlength="10" size="10" name="txtDtIniVal" class="textblacka" value>				</td>			</tr>			<tr>				<td align="left" class="tbltext1">					<b>Data fine validit�</b>(gg/mm/aaaa)				</td>					<td align="left">					<input type="text" style="TEXT-TRANSFORM: uppercase" maxlength="10" size="10" name="txtDtFinVal" class="textblacka" value>				</td>			</tr-->
			
			<tr>
				<td align="left" class="tbltext1" title="Data alla quale le informazioni relative al CCNL devono essere valide">
					<b>Validi alla data</b><font size="1"> (gg/mm/aaaa)</font>
				</td>	
				
				<td align="left"></td>
				
				<td align="left">
					<input type="text" style="TEXT-TRANSFORM: uppercase" size="10" maxlength="10" class="textblacka" name="txtDtValid" value>
				</td>
			</tr>			

<!------------------------------ SEDE IMPRESA ------------------------------------------------------>
			<tr>
				<td colspan="2">&nbsp;
				</td>
			</tr>
			<tr height="2">
				<td class="sfondocommaz" colspan="4" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
			</tr>
			<tr>
				<td align="left" colspan="3" class="tbltext3">Dati della Sede</td>
			</tr>
			<tr>
				<td align="left" colspan="2" class="tbltext3">&nbsp;</td>
			</tr>
	<%
		Erase aDizDati
		redim aDizDati(1, 0)
		sSQL = "SELECT ID_CAMPO, TIPO_OPLOG FROM DIZ_DATI WHERE ID_TAB = 'SEDE_IMPRESA'"
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
		set rsOplog = CC.Execute(sSQL)
		
		if not rsOplog.EOF then
			rsOplog.movefirst
		end if

		Z = 0
		do while not rsOplog.EOF 
			aDizDati(0, Z) = rsOplog("ID_CAMPO")
			aDizDati(1, Z) = rsOplog("TIPO_OPLOG")
			rsOplog.movenext
			if not rsOplog.EOF then
				redim preserve aDizDati(1, Z + 1)
			end if
	'		Response.Write "nome campo: " & aDizDati(0, Z) & "<BR>"
	'		Response.Write "tipo oplog: " & aDizDati(1, Z) & "<BR>"
			Z = Z + 1
		loop
	%>							

			<tr>
				<td align="left" class="tbltext1" title="Tipologia della sede">
					<b>Tipologia sede</b>
				</td>
				<td>
					<select class="textblacka" name="cmbOpeTipSede">
					<option value></option>
					<%
					aTipoOplg = decodOplog("COD_SEDE")
					
					if aTipoOplg(0) <> "*" then
						for i = 0 to UBound(aTipoOplg)
							aAppo = decodTades(aTipoOplg(i))
							%>
							<option value="<%=aAppo(2)%>"><%=aAppo(1)%></option>
							<%					
							Erase aAppo
						next
						Erase aTipoOplg
					end if
					%>
 				</select>			
				</td>

				<td align="left">
					<%
					sAppo = "TSEDE|0|" & date & "||cmbTipSede|ORDER BY DESCRIZIONE"			
					CreateCombo(sAppo)
					%>	
				</td>
			</tr>
			<tr>
				<td align="left" class="tbltext1" title="Descrizione della sede">
					<b>Descrizione</b>
				</td>
				<td>
					<select class="textblacka" name="cmbOpeDescSede">
					<option value></option>
					<%
					aTipoOplg = decodOplog("DESCRIZIONE")
					if aTipoOplg(0) <> "*" then
						for i = 0 to UBound(aTipoOplg)
							aAppo = decodTades(aTipoOplg(i))
							%>
							<option value="<%=aAppo(2)%>"><%=aAppo(1)%></option>
							<%					
							Erase aAppo
						next
						Erase aTipoOplg
					end if
					%>
 				</select>			
				</td>
				<td align="left">
					<input style="TEXT-TRANSFORM: uppercase;" size="50" maxlength="50" class="textblacka" name="txtDescSede" value>
				</td>
			</tr>			

			<tr>
				<td align="left" class="tbltext1" title="Provincia">
					<b>Provincia</b>
	            </td>
				<td>
					<select class="textblacka" name="cmbOpeProv">
					<option value></option>
					<%
					aTipoOplg = decodOplog("PRV")
					
					if aTipoOplg(0) <> "*" then
						for i = 0 to UBound(aTipoOplg)
							aAppo = decodTades(aTipoOplg(i))
							%>
							<option value="<%=aAppo(2)%>"><%=aAppo(1)%></option>
							<%					
							Erase aAppo
						next
						Erase aTipoOplg
					end if
					%>
 				</select>			
				</td>
	            
				<td align="left">
					<%				
					'If session("progetto") <> "/PLAVORO" then
						sMask = Session("mask") 
						'Mi definisco le variabili Diritti
						Session("Diritti")= mid(sMask,1,6)
						'Mi definisco le variabili Filtro
						Session("Filtro")= mid(sMask,7)	
					'End if 
					nProvSedi = len(Session("Filtro")) ' le province che posso gestire.
					
					if nProvSedi <> 0 then
						nProvSedi = nProvSedi/2 ' quante province posso visualizzare.
						' Ciclo per creare la combo con le sole province di interesse.
						sCondizione = "AND Codice in ("						
						
						for i = 1 to nProvSedi
							if i <> 1 then
								sCondizione = sCondizione & ","
							end if
							pos_ini = 2 * (i - 1)
							sCondizione = sCondizione & "'" & mid(Session("Filtro"), 1 + pos_ini, 2) & "'"
						next											
						sCondizione = sCondizione & ")"
					else 	
						sCondizione = ""
					end if

					sAppo =	"PROV|"& sIsa & "|" & date & "||cmbProv" &_
							"|" & sCondizione & " ORDER BY DESCRIZIONE" 

					CreateCombo(sAppo)
					%>
				</td>
			</tr>
			
			<tr>
				<td align="left" class="tbltext1" title="Comune">
					<b>Comune</b>
				</td>
				<td>
					<select class="textblacka" name="cmbOpeComune">
					<option value></option>
					<%
					aTipoOplg = decodOplog("COMUNE")
					
					if aTipoOplg(0) <> "*" then
						for i = 0 to UBound(aTipoOplg)
							aAppo = decodTades(aTipoOplg(i))
							%>
							<option value="<%=aAppo(2)%>"><%=aAppo(1)%></option>
							<%					
							Erase aAppo
						next
						Erase aTipoOplg
					end if
					%>
 				</select>			
				</td>
				
				<td align="left">
					<span class="tbltext">
					<input type="hidden" id="txtComuneHid" name="txtComuneHid" value>
					<input type="text" name="txtComune" style="TEXT-TRANSFORM: uppercase; WIDTH: 220px" class="textblack" size="10" readonly value>
						<%
						NomeForm="frmSelFiltri"
						CodiceProvincia="cmbProv"
						NomeComune="txtComune"
						CodiceComune="txtComuneHid"
						Cap="txtCap"
						%>
					<a href="Javascript:SelComune('<%=NomeForm%>','<%=CodiceProvincia%>','<%=NomeComune%>','<%=CodiceComune%>','<%=Cap%>')" ID="imgPunto1" name="imgPunto1" onmouseover="javascript:window.status='';return true">
					<img border="0" src="<%=Session("Progetto")%>/images/bullet1.gif"></a>
					</span>					
				</td>
				<input type="hidden" name="txtCap" value>
			</tr>
			
			<tr>
				<td align="left" class="tbltext1" title="Centro per l'impiego competente territorialmente">
					<b>Centro Impiego</b>
	            </td>

				<td>
					<select class="textblacka" name="cmbOpeCImp">
					<option value></option>
					<%
					aTipoOplg = decodOplog("ID_CIMPIEGO")
					
					if aTipoOplg(0) <> "*" then
						for i = 0 to UBound(aTipoOplg)
							aAppo = decodTades(aTipoOplg(i))
							%>
							<option value="<%=aAppo(2)%>"><%=aAppo(1)%></option>
							<%					
							Erase aAppo
						next
						Erase aTipoOplg
					end if
					%>
 				</select>			
				</td>
	            
				<input type="hidden" id="txtCentroImpiegoHid" name="txtCentroImpiegoHid">
				<td align="left">
					<input type="text" name="txtCentroImpiego" style="TEXT-TRANSFORM: uppercase; WIDTH: 220px" class="textblack" size="10" readonly>
					<a href="Javascript:SelCentroImpiego('CENTRO_IMPIEGO','ID_CIMPIEGO','txtCentroImpiego')" onmouseover="javascript:window.status='';return true" name="imgPunto1">
					<img border="0" src="<%=Session("Progetto")%>/images/bullet1.gif" id="ImgCPI"></a>
				</td>
			</tr>

		</table>
		
		<br><br>
		
		<table width="500" cellspacing="2" cellpadding="1" border="0" align="center">
			<tr align="center">
				<td>
				<input type="image" src="<%=Session("progetto")%>/images/lente.gif" title="Avvia la ricerca" border="0" align="absBottom" id="image1" name="image1">
				</td>
			</tr>		
		</table>
	</form>
<%
end sub

'--------------------------------- MAIN -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------	
dim sSQL
dim aDizDati
dim aTades


Inizio()
ImpostaPag()


%>	
<!--#include virtual = "/include/closeconn.asp"-->
<!--#include virtual = "strutt_coda2.asp"-->
