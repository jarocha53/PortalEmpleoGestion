<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->

<!-- #include virtual="/strutt_testa2.asp"-->
<!--#include Virtual = "/Include/OpenConn.asp"-->
<!--#include virtual = "/include/ControlDateVB.asp"-->
<!--#include virtual = "/util/dbutil.asp"-->
<%
if ValidateService(session("idutente"),"AZI_SELFILTRI", CC) <> "true" then 
	response.redirect "/util/error_login.asp"
end if
%>

<script language="javascript">
<!--#include virtual = "/Include/help.inc"-->

function ControllaDati(frmElenco){
var i;
var numAziende = 0;
var nSel = 0;
var apChk;

	numAziende = parseInt(frmElenco.Contatore.value);
	
	for (i = 0; i < numAziende; i++){
		apChk = eval("frmElenco.chkSel" + i);
		
		if (eval("frmElenco.chkSel" + i + ".checked") == true){
			nSel++;
			apChk.value = "1"
		}else{
			apChk.value = "0"
		}
	}
	
	if (nSel == "0"){
		alert("Selezionare almeno un'azienda");
		return false;
	}
	return true;
}

</script>

<%
'----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Sub Msgetto(sTesto)
%>		
	<br><br>
	<table width="500" align="center" border="0" cellspacing="2" cellpadding="1">
		<tr height="20"> 
		  	<td class="tbltext3" align="middle" width="500">
				<b><%=sTesto%></b>
			</td>
		</tr>
	</table>
	<br><br>

	<a href="javascript:history.back()">
		<img src="<%=Session("progetto")%>/images/Indietro.gif" border="0" onmouseover="javascript:window.status='' ; return true">
	</a>
<%
End Sub

'----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

sub Inizio()
%>
<table border="0" width="520" cellspacing="0" cellpadding="0" height="70" align="center">
		<tr>
			<td width="500" background="<%=session("progetto")%>/images/titoli/servizi2g.gif" height="70" valign="bottom" align="right">
				<table border="0" background width="500" height="23" cellspacing="0" cellpadding="0">
					<tr>
						<td width="100%" valign="top" align="right"><b CLASS="tbltext1a">Iscrizione Batch Aziende
						  </b></td>
					</tr>
				</table>
			</td>
		</tr>
	</table> 
	<br>
	<!-- Lingetta superiore -->
	<table border="0" CELLPADDING="0" CELLSPACING="0" width="500" align="center">
		<tr height="18">
			<td class="sfondomenu" width="67%" height="18"><span class="tbltext0"><b>&nbsp;GESTIONE ISCRIZIONE BATCH AZIENDE</b></span></td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
			<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
		</tr>
	</table>
	<!-- Commento -->
	<table border="0" CELLPADDING="0" CELLSPACING="0" width="500" align="center">
		<tr>
			<td align="left" class="sfondocomm">
				Selezionare le aziende che si vogliono associare al progetto.
				<a href="Javascript:Show_Help('/Pgm/help/GestProgetti/Azienda/AziendaIscrBatch/AZI_SelFiltri')"><img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
			</td>
		</tr>
		<tr height="2">
			<td class="sfondocommaz" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>
	<br>
<%
end sub

'----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------	

function CreaCondizione(sOperatore, sValore)

	if UCase(sOperatore) = "LIKE" then
		CreaCondizione = sOperatore & " '%" & sValore & "%' "
	else
		CreaCondizione = sOperatore & " '" & sValore & "' "
	end if

end function

'----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------	

sub ImpSql()

'valori --------------------------------------------------
	sRagSoc		= UCase(Trim(Request.Form("txtRagSoc")))
	nPartIva	= UCase(Trim(Request.Form("txtPartIva")))
	sCodFisc	= UCase(Trim(Request.Form("txtCodFisc")))
	dDtCost		= Trim(Request.Form("txtDtCost"))
	sTipImpr	= Trim(Request.Form("cmbTipImpr"))
	sFGiur		= Trim(Request.Form("cmbFGiur"))
	nSettore	= Trim(Request.Form("txtSettoreHid"))
	
	sCcnl		= Trim(Request.Form("txtCcnlHid"))
	dDtValid	= Trim(Request.Form("txtDtValid"))
'	dDtIniVal	= Trim(Request.Form("txtDtIniVal"))
'	dDtFinVal	= Trim(Request.Form("txtDtFinVal"))

	sTipSede	= Trim(Request.Form("cmbTipSede"))
	sDescSede	= UCase(Trim(Request.Form("txtDescSede")))
	sProv		= Trim(Request.Form("cmbProv"))
	sComune		= UCase(Trim(Request.Form("txtComuneHid")))
	nIdCImpiego	= UCase(Trim(Request.Form("txtCentroImpiegoHid")))

'operatori logici -----------------------------------------
	sOpeRagSoc		= Trim(Request.Form("cmbOpeRagSoc"))
	sOpePartIva		= Trim(Request.Form("cmbOpePartIva"))
	sOpeCodFisc		= Trim(Request.Form("cmbOpeCodFisc"))
	sOpeDtCost		= Trim(Request.Form("cmbOpeDtCost"))
	sOpeTipImpr		= Trim(Request.Form("cmbOpeTipImpr"))
	sOpeFGiur		= Trim(Request.Form("cmbOpeFGiur"))
	sOpeSettore		= Trim(Request.Form("cmbOpeSettore"))
	sOpeCcnl		= Trim(Request.Form("cmbOpeCcnl"))
'	sOpeDtIniVal	= Trim(Request.Form("cmbOpeDtIniVal"))
'	sOpeDtFinVal	= Trim(Request.Form("cmbOpeDtFinVal"))

	sOpeTipSede		= Trim(Request.Form("cmbOpeTipSede"))
	sOpeDescSede	= Trim(Request.Form("cmbOpeDescSede"))
	sOpeProv		= Trim(Request.Form("cmbOpeProv"))
	sOpeComune		= Trim(Request.Form("cmbOpeComune"))
	sOpeCImp		= Trim(Request.Form("cmbOpeCImp"))


'	Response.Write "sRagSoc: " & sRagSoc & "<br>"
'	Response.Write "nPartIva: " & nPartIva & "<br>"
'	Response.Write "sCodFisc: " & sCodFisc & "<br>"
'	Response.Write "dDtCost: " & dDtCost & "<br>"
'	Response.Write "sTipImpr: " & sTipImpr & "<br>"
'	Response.Write "sFGiur: " & sFGiur & "<br>"
'	Response.Write "nSettore: " & nSettore & "<br>"
'	Response.Write "sCcnl: " & sCcnl & "<br>"
'	Response.Write "dDtValid: " & dDtValid & "<br>"
'	Response.Write "dDtIniVal: " & dDtIniVal & "<br>"
'	Response.Write "dDtFinVal: " & dDtFinVal & "<br>"
'	Response.Write "sProv: " & sProv & "<br>"
'	Response.Write "sComune: " & sComune & "<br>"
'	Response.Write "sTipSede: " & sTipSede & "<br>"
'	Response.Write "sDescSede: " & sDescSede & "<br>"
'	Response.Write "nIdCImpiego: " & nIdCImpiego & "<br>"
	
	sSQL = "SELECT I.ID_IMPRESA, I.RAG_SOC, I.PART_IVA, I.COD_FISC, " & _
			"SI.ID_SEDE, SI.REF_SEDE, SI.INDIRIZZO, " & _
			"SI.DESCRIZIONE, SI.COMUNE, SI.E_MAIL, SI.NUM_TEL " & _
			"FROM IMPRESA I, SEDE_IMPRESA SI WHERE " & _
			"SI.ID_IMPRESA = I.ID_IMPRESA "

'----- dati dell'impresa -----			
	if sRagSoc <> "" then
		sSQL = sSQL & " AND I.RAG_SOC " & CreaCondizione(sOpeRagSoc, sRagSoc)
	end if
	if nPartIva <> "" then
		sSQL = sSQL & " AND I.PART_IVA " & CreaCondizione(sOpePartIva, nPartIva)
	end if
	if sCodFisc <> "" then
		sSQL = sSQL & " AND I.COD_FISC " & CreaCondizione(sOpeCodFisc, sCodFisc)
	end if
	
	if dDtCost <> "" then
		sSQL = sSQL & " AND I.DT_COST " & sOpeDtCost & " TO_DATE('" & dDtCost & "', 'DD/MM/YYYY') "
	end if
	
	if sTipImpr <> "" then
		sSQL = sSQL & " AND I.COD_TIMPR " & CreaCondizione(sOpeTipImpr, sTipImpr)
	end if
	if sFGiur<> "" then
		sSQL = sSQL & " AND I.COD_FORMA " & CreaCondizione(sOpeFGiur, sFGiur)
	end if
	if nSettore <> "" then
		sSQL = sSQL & " AND I.ID_SETTORE " & sOpeSettore & " " & nSettore & " "
	end if

'----- dati della sede -----
	if sTipSede <> "" then
		sSQL = sSQL & " AND SI.COD_SEDE " & CreaCondizione(sOpeTipSede, sTipSede)
	end if	
	if sDescSede <> "" then
		sSQL = sSQL & " AND SI.DESCRIZIONE " & CreaCondizione(sOpeDescSede, sDescSede)
	end if	
	if sProv <> "" then
		sSQL = sSQL & " AND SI.PRV " & CreaCondizione(sOpeProv, sProv)
	end if
	if sComune <> "" then
		sSQL = sSQL & " AND SI.COMUNE " & CreaCondizione(sOpeComune, sComune)
	end if
	if nIdCImpiego <> "" then
		sSQL = sSQL & " AND SI.ID_CIMPIEGO " & sOpeCImp & " " & nIdCImpiego & " "
	end if

'----- dati del CCNL -----
	sSQL = sSQL & " AND I.ID_IMPRESA IN(SELECT DISTINCT(IC.ID_IMPRESA) FROM IMPRESA_CCNL IC WHERE 1 = 1 "
	if sCcnl <> "" then
		sSQL = sSQL & " AND IC.COD_CCNL " & CreaCondizione(sOpeCcnl, sCcnl)
	end if
	if dDtValid <> "" then
		sSQL = sSQL & " AND (TO_DATE('" & dDtValid & "', 'DD/MM/YYYY') BETWEEN IC.DT_INIVAL AND IC.DT_FINVAL " & _
				" OR (TO_DATE('" & dDtValid & "','DD/MM/YYYY') >= DT_INIVAL AND DT_FINVAL IS NULL))"
	end if
	
'	if dDtIniVal <> "" then
'		sSQL = sSQL & " AND IC.DT_INIVAL " & sOpeRagSoc & " TO_DATE('" & dDtIniVal & "', 'DD/MM/YYYY') "
'	end if
'	if dDtFinVal <> "" then
'		sSQL = sSQL & " AND IC.DT_FINVAL " & sOpeRagSoc & " TO_DATE('" & dDtFinVal & "', 'DD/MM/YYYY') "
'	end if
	
	sSQL = sSQL & ") AND SI.ID_SEDE NOT IN (SELECT DISTINCT (ID_SEDE) FROM SEDE_PROJ " & _
									"WHERE COD_CPROJ = '" & UCase(Mid(session("progetto"), 2)) & "' )"
	
'Response.Write sSQL
'Response.End

end sub

'----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------	

sub ImpostaPag()

	nTamPage = 10
	if Trim(Request.Form("page")) = "" then
		nActPage = 1
	else
		nActPage = CLng(Request("page"))
	end if
	
	set rsAziende = Server.CreateObject("ADODB.Recordset")
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	rsAziende.Open sSQL, CC, 3
	
	if rsAziende.EOF then
		Msgetto("Non ci sono aziende che soddisfano i criteri di ricerca.")
		exit sub
	end if
	
	rsAziende.PageSize = nTamPage
	rsAziende.CacheSize = nTamPage
	
	nTotPage = rsAziende.PageCount
	if nActPage < 1 then
		nActPage = 1
	end if
	if nActPage > nTotPage then
		nActPage = nTotPage
	end if
	rsAziende.AbsolutePage = CLng(nActPage)
	nTotRecord = 0
	
	%>
	<table border="0" cellpadding="1" cellspacing="2" width="500">
			
		<tr>
			<td align="left" colspan="2" class="tbltext3">
				Le aziende che soddisfano i criteri di selezione sono: <%=rsAziende.RecordCount%>
			</td>
		</tr>
	
	</table>			

	<form name="frmElenco" method="post" action="AZI_InsIscrBatch.asp" onsubmit="return ControllaDati(this)">
		
		<table width="500" cellpadding="1" cellspacing="2" border="0">
			<tr height="20" class="sfondocomm"> 
				<td width="10%"><b>Selezione</b></td>
				<td width="45%" align="left"><b>Ragione sociale</b></td>
				<td width="45%" align="left"><b>Indirizzo della sede</b></td>
			</tr>
			<%
			numRighe = 0
			do while not rsAziende.EOF and nTotRecord < nTamPage
			
				if IsNull(rsAziende("REF_SEDE")) then
					sRefSede = ""
				else
					sRefSede = rsAziende("REF_SEDE")
				end if
			%>
				<input type="hidden" name="IdImpresa<%=numRighe%>" value="<%=rsAziende("ID_IMPRESA")%>">
				<input type="hidden" name="RagSoc<%=numRighe%>" value="<%=rsAziende("RAG_SOC")%>">
				<input type="hidden" name="PartIva<%=numRighe%>" value="<%=rsAziende("PART_IVA")%>">
				<input type="hidden" name="CodFisc<%=numRighe%>" value="<%=rsAziende("COD_FISC")%>">
				
				<input type="hidden" name="IdSede<%=numRighe%>" value="<%=rsAziende("ID_SEDE")%>">
				<input type="hidden" name="RefSede<%=numRighe%>" value="<%=sRefSede%>">
				<input type="hidden" name="DescSede<%=numRighe%>" value="<%=rsAziende("DESCRIZIONE")%>">
				<input type="hidden" name="CodComune<%=numRighe%>" value="<%=rsAziende("COMUNE")%>">
				<input type="hidden" name="EMail<%=numRighe%>" value="<%=rsAziende("E_MAIL")%>">
				<input type="hidden" name="NumTel<%=numRighe%>" value="<%=rsAziende("NUM_TEL")%>">												
				
				
				<tr class="tblsfondo">
					<td class="tblAgg" width="10%">
						<input type="checkbox" name="chkSel<%=numRighe%>">
					</td>
					<td class="tblAgg" width="45%">
						<%=rsAziende("RAG_SOC")%>
					</td>
					<td class="tblAgg" width="45%">
						<%=rsAziende("INDIRIZZO")%>
					</td>
				</tr>
				<%	
				numRighe = numRighe + 1
				
				nTotRecord = nTotRecord + 1
				rsAziende.MoveNext
			loop
			rsAziende.Close	
			set rsAziende = nothing
			%>
			<input type="hidden" name="Contatore" value="<%=numRighe%>">
		</table>
		
		<br><br>
		
		<table border="0" cellpadding="1" cellspacing="1" width="200">
			<tr>
			    <td align="center" width="50%">
				    <input type="image" src="<%=Session("Progetto")%>/images/conferma.gif" border="0" value="Inserisci" id="image1" name="image1">
			    </td>
			<%
			If nActPage = nTotPage and nTotPage = 1 Then
				%>
			    <td align="center" width="50%">
					<a href="javascript:history.back()">
						<img src="<%=Session("progetto")%>/images/Indietro.gif" border="0" onmouseover="javascript:window.status='' ; return true">
					</a>
				</td>
				<%
			End If
			%>
			</tr>
		</table>

	</form>
	
	<table border="0" width="500">
		<tr>
			<%		
			If nActPage > 1 Then
				%>
				<form method="POST" action="AZI_RicIscrBatch.asp" name="Down">
					<input type="hidden" name="page" value="<%=nActPage - 1%>">
					
					<input type="hidden" name="txtRagSoc" value="<%=sRagSoc%>">
					<input type="hidden" name="txtPartIva" value="<%=nPartIva%>">
					<input type="hidden" name="txtCodFisc" value="<%=sCodFisc%>">
					<input type="hidden" name="txtDtCost" value="<%=dDtCost%>">
					<input type="hidden" name="cmbTipImpr" value="<%=sTipImpr%>">
					<input type="hidden" name="cmbFGiur" value="<%=sFGiur%>">
					<input type="hidden" name="txtSettoreHid" value="<%=nSettore%>">
					<input type="hidden" name="txtCcnlHid" value="<%=sCcnl%>">
					<input type="hidden" name="txtDtValid" value="<%=dDtValid%>">
					<!--input type='hidden' name='txtDtIniVal' value='<%=dDtIniVal%>'>					<input type='hidden' name='txtDtFinVal' value='<%=dDtFinVal%>'-->
					
					<input type="hidden" name="cmbTipSede" value="<%=sTipSede%>">
					<input type="hidden" name="txtDescSede" value="<%=sDescSede%>">
					<input type="hidden" name="cmbProv" value="<%=sProv%>">
					<input type="hidden" name="txtComuneHid" value="<%=sComune%>">
					<input type="hidden" name="txtCentroImpiegoHid" value="<%=nIdCImpiego%>">
					
					<!---------------------------------------------------------->
					
					<input type="hidden" name="cmbOpeRagSoc" value="<%=sOpeRagSoc%>">
					<input type="hidden" name="cmbOpePartIva" value="<%=sOpePartIva%>">
					<input type="hidden" name="cmbOpeCodFisc" value="<%=sOpeCodFisc%>">
					<input type="hidden" name="cmbOpeDtCost" value="<%=sOpeDtCost%>">
					<input type="hidden" name="cmbOpeTipImpr" value="<%=sOpeTipImpr%>">
					<input type="hidden" name="cmbOpeFGiur" value="<%=sOpeFGiur%>">
					<input type="hidden" name="cmbOpeSettore" value="<%=sOpeSettore%>">
					<input type="hidden" name="cmbOpeCcnl" value="<%=sOpeCcnl%>">
					<!--input type='hidden' name='cmbOpeDtIniVal' value='<%=sOpeDtIniVal%>'>					<input type='hidden' name='cmbOpeDtFinVal' value='<%=sOpeDtFinVal%>'-->
					
					<input type="hidden" name="cmbOpeTipSede" value="<%=sOpeTipSede%>">
					<input type="hidden" name="cmbOpeDescSede" value="<%=sOpeDescSede%>">
					<input type="hidden" name="cmbOpeProv" value="<%=sOpeProv%>">
					<input type="hidden" name="cmbOpeComune" value="<%=sOpeComune%>">
					<input type="hidden" name="cmbOpeCImp" value="<%=sOpeIdCImpiego%>">

				</form>
				<td align="right" width="480"><a href onclick="Javascript:Down.submit(); return false">
				<img border="0" alt="Pagina precedente" src="/Images/precedente.gif"></a></td>
				<%
			End If
	
			If nActPage < nTotPage Then
			%>
				<form method="POST" action="AZI_RicIscrBatch.asp" name="Up">
					<input type="hidden" name="page" value="<%=nActPage + 1%>">

					<input type="hidden" name="txtRagSoc" value="<%=sRagSoc%>">
					<input type="hidden" name="txtPartIva" value="<%=nPartIva%>">
					<input type="hidden" name="txtCodFisc" value="<%=sCodFisc%>">
					<input type="hidden" name="txtDtCost" value="<%=dDtCost%>">
					<input type="hidden" name="cmbTipImpr" value="<%=sTipImpr%>">
					<input type="hidden" name="cmbFGiur" value="<%=sFGiur%>">
					<input type="hidden" name="txtSettoreHid" value="<%=nSettore%>">
					<input type="hidden" name="txtCcnlHid" value="<%=sCcnl%>">
					<input type="hidden" name="txtDtValid" value="<%=dDtValid%>">
					<!--input type='hidden' name='txtDtIniVal' value='<%=dDtIniVal%>'>					<input type='hidden' name='txtDtFinVal' value='<%=dDtFinVal%>'-->
					
					<input type="hidden" name="cmbTipSede" value="<%=sTipSede%>">
					<input type="hidden" name="txtDescSede" value="<%=sDescSede%>">
					<input type="hidden" name="cmbProv" value="<%=sProv%>">
					<input type="hidden" name="txtComuneHid" value="<%=sComune%>">
					<input type="hidden" name="txtCentroImpiegoHid" value="<%=nIdCImpiego%>">
					
					<!---------------------------------------------------------->
					
					<input type="hidden" name="cmbOpeRagSoc" value="<%=sOpeRagSoc%>">
					<input type="hidden" name="cmbOpePartIva" value="<%=sOpePartIva%>">
					<input type="hidden" name="cmbOpeCodFisc" value="<%=sOpeCodFisc%>">
					<input type="hidden" name="cmbOpeDtCost" value="<%=sOpeDtCost%>">
					<input type="hidden" name="cmbOpeTipImpr" value="<%=sOpeTipImpr%>">
					<input type="hidden" name="cmbOpeFGiur" value="<%=sOpeFGiur%>">
					<input type="hidden" name="cmbOpeSettore" value="<%=sOpeSettore%>">
					<input type="hidden" name="cmbOpeCcnl" value="<%=sOpeCcnl%>">
					<!--input type='hidden' name='cmbOpeDtIniVal' value='<%=sOpeDtIniVal%>'>					<input type='hidden' name='cmbOpeDtFinVal' value='<%=sOpeDtFinVal%>'-->
					
					<input type="hidden" name="cmbOpeTipSede" value="<%=sOpeTipSede%>">
					<input type="hidden" name="cmbOpeDescSede" value="<%=sOpeDescSede%>">					
					<input type="hidden" name="cmbOpeProv" value="<%=sOpeProv%>">
					<input type="hidden" name="cmbOpeComune" value="<%=sOpeComune%>">
					<input type="hidden" name="cmbOpeCImp" value="<%=sOpeIdCImpiego%>">
					
				</form>
				<td align="right"><a href onclick="Javascript:Up.submit(); return false">
				<img border="0" alt="Pagina successiva" src="/Images/successivo.gif"></a></td>
				<%
			End If
			%>
		</tr>
	</table>
<%

end sub


'--------------------------------------- MAIN -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------	

dim sSQL
dim rsAziende

dim sRagSoc, nPartIva, sCodFisc, dDtCost, sTipImpr, sFGiur
dim nSettore, sCcnl, dDtValid
', dDtIniVal, dDtFinVal
dim sTipSede, sDescSede, sProv, sComune, nIdCImpiego

dim sOpeRagSoc, sOpePartIva, sOpeCodFisc, sOpeDtCost, sOpeTipImpr, sOpeFGiur
dim sOpeSettore, sOpeCcnl, sOpeDtIniVal, sOpeDtFinVal
dim sOpeTipSede, sOpeDescSede, sOpeProv, sOpeComune, sOpeCImp

Inizio()

ImpSql()

ImpostaPag()

%>	
<!--#include virtual = "/include/closeconn.asp"-->
<!--#include virtual = "strutt_coda2.asp"-->
