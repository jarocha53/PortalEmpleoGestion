<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include virtual="/strutt_testa2.asp"-->
<!--#include virtual="/util/dbutil.asp"-->
<!--#include virtual="/include/DecComun.asp"-->
<!--#include virtual="/include/ControlDateVB.asp"-->
<!--#include virtual="/include/SysFunction.asp"-->
<!--#include virtual = "/include/OpenConn.asp"-->
<!--#include Virtual = "/util/portallib.asp"-->

<%

' Paramentri passati dalla pagina precedente
dim TmstIMP
dim TmstSEDE

dim RagSoc
dim FormGiurVal
dim CodSettoreVal
dim Attivita
dim PIva
dim CodFis
dim SitoWeb

dim sDescSede
dim sTipoSedeVal
dim sIndirizzo
dim sCap
dim sComune
dim sProvinciaVal
dim sTel
dim sFax
dim sEmail

dim sRef
dim sRefCognome
dim sRefNome
dim CodRuoVal
dim sRefEmail
dim sRefEmailOri

dim idImp
dim idSed

TmstIMP = Request.Form("txtTmstIMP")
TmstSEDE = Request.Form("txtTmstSEDE")

idImp = Request.QueryString("idImpresa")

idSed = Request.QueryString("idSede")


RagSoc = UCase(Request.Form ("txtRagSoc"))

FormGiurVal = Request.Form ("cmbCodForm")

CodSettoreVal = Request.Form ("cmbCodSet")

Attivita = UCase(Request.Form ("txtDescAtt"))
Attivita = Replace(Server.HTMLEncode(Attivita), "'", "''")

PIva = UCase(Request.Form ("txtPartIva"))

CodFis = UCase(Request.Form ("txtCodFis"))

SitoWeb = Request.Form("txtSitoWeb")
SitoWeb = Replace(Server.HTMLEncode(SitoWeb), "'", "''")

sSedeTel = Request.Form ("txtSedeTel")

sSedeFax = Request.Form ("txtSedeFax")

sSedeEmail = Request.Form ("txtSedeEmail")
sSedeEmail = Replace(Server.HTMLEncode(sSedeEmail), "'", "''")

sDescrizione = UCase(Request.Form ("txtDescrizione"))

sCodSede	= UCase(Request.Form ("cmbCodSede"))

sIndirizzo = UCase(Request.Form ("txtIndirizzo"))
sIndirizzo = Replace(Server.HTMLEncode(sIndirizzo), "'", "''")

sCap = UCase(Request.Form ("txtCap"))

sComune = UCase(Request.Form ("txtComune"))

sProv	= UCase(mid(Request.Form ("cmbProv"),1,2))

sRefCognome = UCase(Request.Form ("txtCognome"))

sRefNome = UCase(Request.Form ("txtNome"))

sRefRuolo = UCase(Request.Form ("cmbCodRuo"))

sRefEmail = Request.Form("txtRefEmail")
sRefEmailOri = Request.Form("txtRefEmailOri")


'%%%%%%%%%%%%%%%%%%%%%%%%%
'% AGGIORNAMENTO TABELLE %
'%%%%%%%%%%%%%%%%%%%%%%%%%
dim sqlUpIMP
dim rsUpIMP

dim sqlUpSEDE
dim rsUpSEDE
	
Errore = "0"

'*******************************************
'Aggiornamento tabella IMPRESA
'*******************************************	

sqlUpIMP = "UPDATE IMPRESA SET DT_TMST = " & convdatetodb(Now()) & ", "

	'Controlli sull'inserimento di campi NULL
	'----------------------------------------
	if Attivita <> "" then
		sqlUpIMP = sqlUpIMP & "DESC_ATT = '" & Attivita & "', " 
	else
		sqlUpIMP = sqlUpIMP & "DESC_ATT = '', "
	end if

	if SitoWeb <> "" then
		sqlUpIMP = sqlUpIMP & "SITOWEB = '" & SitoWeb & "' " 
	else
		sqlUpIMP = sqlUpIMP & "SITOWEB = '' "
	end if
	'----------------------------------------
	
sqlUpIMP = sqlUpIMP & " WHERE ID_IMPRESA = " & IdImp

CC.BeginTrans

Errore = EseguiNoCTrace(UCASE(mid(session("progetto"),2)),IdImp,"IMPRESA",session("idutente"),IdImp,"S", "MOD", sqlUpIMP, 1, TmstIMP,cc)

if Errore = "0" then


'*******************************************
'Fine Aggiornamento tabella IMPRESA
'*******************************************

	'+++++++++++++++++++++++++++++++++++++++++++
	'Aggiornamento tabella SEDE_IMPRESA
	'+++++++++++++++++++++++++++++++++++++++++++
	sqlUpSEDE = "UPDATE SEDE_IMPRESA SET E_MAIL = '" & sRefEmail & "', DT_TMST = " & convdatetodb(Now()) & "," 

		'Controlli sull'inserimento di campi NULL
		'----------------------------------------
		if sIndirizzo <> "" then
			sqlUpSEDE = sqlUpSEDE & "INDIRIZZO = '" & sIndirizzo & "', " 
		else
			sqlUpSEDE = sqlUpSEDE & "INDIRIZZO = '', "
		end if

		if sCap <> "" then
			sqlUpSEDE = sqlUpSEDE & "CAP = '" & sCap & "', " 
		else
			sqlUpSEDE = sqlUpSEDE & "CAP = '', "
		end if

		if sSedeTel <> "" then
			sqlUpSEDE = sqlUpSEDE & "NUM_TEL_SEDE = '" & sSedeTel & "', " 
		else
			sqlUpSEDE = sqlUpSEDE & "NUM_TEL_SEDE = '', "
		end if

		if sSedeFax <> "" then
			sqlUpSEDE = sqlUpSEDE & "FAX_SEDE = '" & sSedeFax & "', " 
		else
			sqlUpSEDE = sqlUpSEDE & "FAX_SEDE = '', "
		end if

		if sSedeEmail <> "" then
			sqlUpSEDE = sqlUpSEDE & "E_MAIL_SEDE = '" & sSedeEmail & "', " 
		else
			sqlUpSEDE = sqlUpSEDE & "E_MAIL_SEDE = '', "
		end if

		if sRefRuolo <> "" then
			sqlUpSEDE = sqlUpSEDE & "COD_RUO = '" & sRefRuolo & "' " 
		else
			sqlUpSEDE = sqlUpSEDE & "COD_RUO = '' "
		end if
		'----------------------------------------
		
	sqlUpSEDE = sqlUpSEDE & " WHERE ID_SEDE = " & IdSed

	Errore = EseguiNoCTrace(UCase(mid(session("progetto"),2)),IdSed,"SEDE_IMPRESA",session("idutente"),IdSed,"S", "MOD", sqlUpSEDE, 1, TmstSEDE,cc)
	
	if Errore = "0" then
	'+++++++++++++++++++++++++++++++++++++++++++
	'Fine Aggiornamento tabella SEDE_IMPRESA
	'+++++++++++++++++++++++++++++++++++++++++++


		if (sRefEmail <> sRefEmailOri) then
		  
			dim sSqlUpUte
		  
			sSqlUpUte = "UPDATE UTENTE " &_
							"SET EMAIL='" & sRefEmail & "', " & _
		  					"DT_TMST=" & convdatetodb(Now()) & _
		  					"WHERE CREATOR = " & IdSed & _
		  					"AND TIPO_PERS='S'" &_
		  					"AND IND_ABIL = 'S'"
			Errore = EseguiNoC(sSqlUpUte,cc)
								
		end if

		if Errore = "0" then

  			CC.CommitTrans
			%>
	
			<table border="0" width="525" cellspacing="0" cellpadding="0" height="81">
			   <tr>
			     <td width="525" background="<%=Session("Progetto")%>/images/titoli/strumenti2g.gif" height="81" valign="bottom" align="right">
			       <table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
			         <tr>
			           <td width="100%" valign="top" align="right"><span class="tbltext1a"><b>Dati Aziendali &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></span></td>
			         </tr>
			       </table>
			     </td>
			   </tr>
			</table>
			<br>
			<table border="0" cellspacing="1" cellpadding="1" width="500">
				<tr align="middle">
					<td class="tbltext3">
						Modifica dei dati correttamente effettuata.
					</td>
				</tr>
			</table>
			<%
			
		else
			
			CC.RollbackTrans
			%>			
			<table border="0" width="525" cellspacing="0" cellpadding="0" height="81">
			   <tr>
			     <td width="525" background="<%=Session("Progetto")%>/images/titoli/strumenti2g.gif" height="81" valign="bottom" align="right">
			       <table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
			         <tr>
			           <td width="100%" valign="top" align="right"><span class="tbltext1a"><b>Dati Aziendali &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></span></td>
			         </tr>
			       </table>
			     </td>
			   </tr>
			</table>	
			<br>
			<table border="0" cellspacing="1" cellpadding="1" width="500">
				<tr align="middle">
					<td class="tbltext3">
						Modifica dei dati non effettuabile.
					</td>
				</tr>
				<tr align="middle">
					<td class="tbltext3">
						<%Response.Write (Errore)%> 
					</td>
				</tr>
			</table>
			<br>				
			<table border="0" cellpadding="0" cellspacing="1" width="500">
				<tr>
					<td align="middle" colspan="2" width="60%"><b>
					 <!--<A  class="textred" href="javascript:history.back()">Pagina Precedente</a></b>-->
					 <a HREF="javascript:history.back()"><img SRC="<%=Session("Progetto")%>/images/indietro.gif" border="0">
					</td>
				</tr>
			</table>	
			<% 
		end if

	else
			
		CC.RollbackTrans
		%>			
		<table border="0" width="525" cellspacing="0" cellpadding="0" height="81">
		   <tr>
		     <td width="525" background="<%=Session("Progetto")%>/images/titoli/strumenti2g.gif" height="81" valign="bottom" align="right">
		       <table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
		         <tr>
		           <td width="100%" valign="top" align="right"><span class="tbltext1a"><b>Dati Aziendali &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></span></td>
		         </tr>
		       </table>
		     </td>
		   </tr>
		</table>	
		<br>
		<table border="0" cellspacing="1" cellpadding="1" width="500">
			<tr align="middle">
				<td class="tbltext3">
					Modifica dei dati non effettuabile.
				</td>
			</tr>
			<tr align="middle">
				<td class="tbltext3">
					<%Response.Write (Errore)%> 
				</td>
			</tr>
		</table>
		<br>				
		<table border="0" cellpadding="0" cellspacing="1" width="500">
			<tr>
				<td align="middle" colspan="2" width="60%"><b>
				 <!--<A  class="textred" href="javascript:history.back()">Pagina Precedente</a></b>-->
				 <a HREF="javascript:history.back()"><img SRC="<%=Session("Progetto")%>/images/indietro.gif" border="0">
				</td>
			</tr>
		</table>	
		<% 
		
	end if
	
else
			
	CC.RollbackTrans
	%>			
	<table border="0" width="525" cellspacing="0" cellpadding="0" height="81">
	   <tr>
	     <td width="525" background="<%=Session("Progetto")%>/images/titoli/strumenti2g.gif" height="81" valign="bottom" align="right">
	       <table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
	         <tr>
	           <td width="100%" valign="top" align="right"><span class="tbltext1a"><b>Dati Aziendali &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></span></td>
	         </tr>
	       </table>
	     </td>
	   </tr>
	</table>	
	<br>
	<table border="0" cellspacing="1" cellpadding="1" width="500">
		<tr align="middle">
			<td class="tbltext3">
				Modifica dei dati non effettuabile.
			</td>
		</tr>
		<tr align="middle">
			<td class="tbltext3">
				<%Response.Write (Errore)%> 
			</td>
		</tr>
	</table>
	<br>				
	<table border="0" cellpadding="0" cellspacing="1" width="500">
		<tr>
			<td align="middle" colspan="2" width="60%"><b>
			 <!--<A  class="textred" href="javascript:history.back()">Pagina Precedente</a></b>-->
			 <a HREF="javascript:history.back()"><img SRC="<%=Session("Progetto")%>/images/indietro.gif" border="0">
			</td>
		</tr>
	</table>	
	<% 
end if	


'rsUpSede.Close
'rsUpIMP.Close

Set rsUpSEDE = Nothing
Set rsUpIMP = Nothing
%>
<!--#include virtual="/include/CloseConn.asp"-->
<!--#include virtual="/strutt_coda2.asp"-->

