<!--#include Virtual = "/strutt_testa2.asp"-->
<script>
var windowArea

function Avvio_Click()

{
	if (windowArea != null ) 
	{
		windowArea.close(); 
	}	
	if (FrmConsenso.radConsenso[1].checked) 
	{	
		f = 'AZI_Leg_Privacy.asp';
		windowArea=window.open(f, '','width=520,height=300,left=250,top=250,Resize=no,scrolling=no');
		window.event.returnValue = false;
		return false
	}
}	
</script>
<html>
<head>
<meta NAME="GENERATOR" Content="Microsoft Visual Studio 6.0">
</head>
<body>
<br><br>
<form name="FrmConsenso" method="post" onsubmit="return Avvio_Click()" action="AZI_Iscrizione.asp">
<table width="500">
	<tr>
		<td colspan="2" align="center" class="tbltext1" width="500">
			<!--b>Consenso ai sensi dell'art.11 della legge 31 dicembre 1996<br> n. 675/96</b><br><br-->
			<b>Consenso ai sensi dell'art.13 del D. Lgs. <br> 196/2003 </b><br><br>
			Consenso relativo alla legge sulla privacy: vi chiediamo gentilmente di leggere l'informativa relativa 
			alla privacy riportata di seguito e di esprimere<br>il 
			vostro consenso o meno al trattamento dei dati.
		</td>
	</tr>
	<tr>
		<td align="middle" colspan="2">&nbsp;
		</td>
	</tr>
	
</table>
<table width="520">   
	<tr>
		<td colspan="2" class="tbltext"><p align="justify">
		
<%
			on error resume next
			PathFileEdit = "/Testi/Autorizzazioni/info_privacy.htm"
			Server.Execute(PathFileEdit)
			If err.number <> 0 Then
				Response.Write "Errore nella visualizzazione. "
				Response.Write "Contattare il Gruppo Assistenza Portale Italia Lavoro "
				Response.Write "all'indirizzo po-assistenza@italialavoro.it "
			End If				
%>
			</p>
		</td>
	</tr>
</table>
<br>
<table>	
	<tr>
		<td colSpan="2"><br>
			<center>
			<input type="radio" value="0" name="radConsenso"><span class="tbltext">ACCETTO</span>
			<input type="radio" value="1" CHECKED name="radConsenso"><span class="tbltext">NON ACCETTO</span>
			<input type="hidden" name="sControllo" value="2">
			</center>
		</td>
	</tr>
</table>
<br><br>
<table border="0" cellpadding="1" cellspacing="1" width="360" align="center">
	<tr>
		<td align="center">
			<img src="<%=Session("Progetto")%>/images/Indietro.gif" border="0" onclick="javascript:history.back();">
		</td>
		<td align="center">
			<input type="image" name="Avanti" src="<%=Session("Progetto")%>/images/avanti.gif" border="0">
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
</table>
</form>
</body>
</html>
<!--#include Virtual = "/strutt_coda2.asp"-->