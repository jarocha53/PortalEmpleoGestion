<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%IF session("progetto") <> "" then%>

<!-- #include virtual="/include/OpenConn.asp" -->
<!-- #include virtual="/include/DecCod.asp" -->
<!-- #include virtual="/include/ElabRegole.asp" -->
<!-- #include virtual="/include/SelezioneProvince.asp" -->
<!--#include virtual="/include/DecComun.asp"-->

<script language="javascript" src="Util.js">
</script>

<title>Contratos en plazo</title>

<body topmargin="0" onafterprint="javascript:buttom.style.visibility='visible';" onbeforeprint="javascript:buttom.style.visibility='hidden'">
<%

    dataFine = dateadd ("m",6,date)
	
	Ambito = FiltroCPI(true)
	sProv = Request("sProv")
	
		
	ValProv = DecodTadesToArray("PROV",date,"CODICE='" & sProv & "'",0,0)
	sDescProv = ValProv(0,1,0)
	sDesc=ValProv(0,1,1)
	DescRegione = DecCodVal("REGIO",0,date,sDesc,0)
	
	
	Select case(ambito)
		case("x")
			titolo = "Enumeracion nominativa en plazo de " & _ 
					 "contrato inscripto en " & _
					 Request.QueryString("CPI")
		case(0)
			titolo = "Enumeracion nominativa en plazo de " & _ 
					 "contrato inscripto en " & _
					 Request.QueryString("CPI") 
		case(1)
			titolo = "Enumeracion nominativa en plazo de " & _ 
					 "contrato inscripto en el Territorio Nazional"
		case(2)
			titolo = "Enumeracion nominativa en plazo de " & _ 
					 "contrato inscripto en la provincia de " & sDescProv 
		case(3)
			titolo = "Enumeracion nominativa en plazo de " & _ 
					 "contrato inscripto en la region de " & DescRegione 				 					 
	End select
	
%>	
<link REL="STYLESHEET" TYPE="text/css" HREF="<%=session("progetto")%>/fogliostile.css">
<center>
<table border="0" width="488" cellspacing="0" cellpadding="0" height="81">
   <tr>
     <td width="520" background="<%=Session("Progetto")%>/images/titoli/strumenti2b.gif" height="81" valign="bottom" align="right">
       <table border="0" background width="260" height="30" cellspacing="0" cellpadding="0">
         <tr>
           <td width="100%" valign="top" align="right"><b class="tbltext1a">ESTADISTICAS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></td>
         </tr>
          <tr>
            <td width="100%" valign="top" align="right"><BR><BR></td>
          </tr>
       </table>
     </td>
   </tr>
</table>
<table cellpadding="0" cellspacing="0" width="488" border="0">
   <tr height="18">
		<td class="sfondomenu" height="18" width="45%">
		<span class="tbltext0"><b>&nbsp;CONTRATOS FINALIZADOS</b></span></td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
		<td valign="middle" align="right" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1"></td>
   </tr>
   <tr height="2">
		<td width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif"></td>     
	</tr>
   <tr width="371" class="SFONDOCOMM">
		<td colspan="3"><%=titolo%></b>
		</td>
	</tr>
	<tr height="2">
		<td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif">
		</td>
	</tr>
</table>
<br>
<br>
<br>
<table>
	<tr width="488">
		<td width="488" class="text" align="center"><b>&nbsp;El reporte se ha creado correctamente.</b></td> 
	</tr>
</table>	

	

<% 
sCpi=Request.QueryString("CPI")


sFilePath = Server.MapPath("/") & session("progetto") & "/DocPers/Statistiche/" &_
	        Session("IdUtente") & "_F.xls"

sFileNameJS = session("progetto") & "/DocPers/Statistiche/" &_
	Session("IdUtente") & "_F.xls"
	
set oFileSystemObject = Server.CreateObject("Scripting.FileSystemObject")

set oFileObj = oFileSystemObject.CreateTextFile(sFilePath,true)

' ********************************
' Scrittura intestazione del file.
' ********************************
oFileObj.WriteLine ("Trabajadores con contrato en plazo dentro del : " & dataFine)
oFileObj.WriteLine ("")
oFileObj.WriteLine ("Situacion al: " & date)
oFileObj.WriteLine ("")
oFileObj.WriteLine (titolo)
oFileObj.WriteLine ("")
oFileObj.WriteLine ("")




set rsCarico = Server.CreateObject("ADODB.recordset")
sReport = "NO"
 

 sSQL = " SELECT P.NOME,P.COGNOME,P.COM_NASC,P.DT_NASC,P.COD_FISC" &_	
		   " FROM PERSONA p , STATO_OCCUPAZIONALE d " &_
		   " WHERE p.id_persona = d.id_persona AND " &_
		   " d.dt_fin_tempodet between to_date('" & date & "', 'dd/mm/yyyy') and to_date('" & dataFine & "', 'dd/mm/yyyy') AND " &_
	       " EXISTS (SELECT id_persona FROM STATO_OCCUPAZIONALE " &_
	       " WHERE p.id_persona = id_persona AND ind_status ='0'"
 
Select case(ambito)
	case(0)
	sSQL = sSQL & " AND id_cimpiego=" & session("creator") & ")" 
	       
	case(1)	       
	sSQL = sSQL & " AND id_cimpiego is not null)"
	
	case(2)
	sSQL = " SELECT COUNT(P.ID_PERSONA) AS NTOT" &_	
		   " FROM PERSONA p , DICHIARAZ d " &_
		   " WHERE p.id_persona = d.id_persona AND " &_
		   " EXISTS (" &_
			 " SELECT id_persona FROM STATO_OCCUPAZIONALE SO, SEDE_IMPRESA SI" &_
			 " WHERE p.id_persona = SO.id_persona AND ind_status ='0' AND SO.id_CIMPIEGO in (" &_
				" SELECT A.ID_SEDE FROM sede_impresa a, impresa b " &_
				" where a.id_impresa = b.id_impresa" &_
				" AND a.prv ='" & sProv  & "' AND B.cod_timpr ='03'))"
				
	 case(3)
 	 valoreReg= SetProvince(ambito)
	 sSQL = " SELECT COUNT(P.ID_PERSONA) AS NTOT" &_	
		    " FROM PERSONA p , DICHIARAZ d " &_
		    " WHERE p.id_persona = d.id_persona AND " &_
		    " EXISTS (" &_
			  " SELECT id_persona FROM STATO_OCCUPAZIONALE SO" &_
			  " WHERE p.id_persona = SO.id_persona AND ind_status ='0' AND SO.id_CIMPIEGO in (" &_
				" SELECT A.ID_SEDE FROM sede_impresa A, impresa B " &_
				" WHERE A.id_impresa = B.id_impresa  AND B.cod_timpr ='03'" &_
				" AND A.prv  in (" & valoreReg  & ")))"
			
	End select
		
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	rsCarico.Open sSql,CC,3
	
	if not rsCarico.EOF then
	   		
		sReport = "OK"
		oFileObj.WriteLine ("NOME" & chr(9) & "COGNOME" & chr(9) & "COMUNE DI NASCITA" & chr(9) & "DATA DI NASCITA"& chr(9) & "CODICE FISCALE")
        oFileObj.WriteLine ()
        
	end if
	
	do while not rsCarico.EOF
	
      sDescrComune = DescrComune (rsCarico("COM_NASC")) 	
      oFileObj.WriteLine (rsCarico("NOME") & chr(9) & rsCarico("COGNOME") & chr(9) & sDescrComune & chr(9) & rsCarico("DT_NASC")& chr(9) & rsCarico("COD_FISC"))
	
	  rsCarico.MoveNext 
	loop
		
	
	rsCarico.Close
	set rsCarico = nothing 

	oFileObj.CLose
	set oFileObj = nothing
	set oFileSystemObject = nothing

%>  
</table>	
<br>
</center>
<%
	if sReport = "OK" then
		Fine()
	else
		Fine1()
	end if 
%>
</body>


<%sub Fine%> 
<label id="buttom">
  <table width="460" cellspacing="2" cellpadding="1" border="0" align="center">
	<tr align="center">
		<td>
			<a class="textred" href="Javascript:Scarica('<%=sFileNameJS%>')"><b>Abrir reporte</b></a>
		</td>
	</tr>
</table>
<br>
<br>
<br>
<table width="488" cellspacing="2" cellpadding="1" border="0" align="center">
	<tr align="center">
		<td>
			<a href="javascript:self.close()"><img src="<%=Session("progetto")%>/images/chiudi.gif" title="Chiudi la pagina" border="0" align="absBottom"></a>
		</td>
	</tr>		
</table> 
</label>  
<%end sub 

sub Fine1()%>

	<br><br>
	<table width="488" cellspacing="2" cellpadding="1" border="0" align="center">
		<tr align="center">
			<td class="tbltext3">
				Ningun elemento encontrado
			</td>
		</tr>		
		<tr align="center">
			<td>&nbsp;</td>
		</tr>		
		<tr align="center">
			<td>
				<a href="javascript:self.close()"><img src="<%=Session("progetto")%>/images/chiudi.gif" title="Chiudi la pagina" border="0" align="absBottom"></a>
			</td>
		</tr>		
	</table>

<%end sub%>
<!-- #include virtual="/include/closeConn.asp" -->
<%ELSE%>
	<script>
		alert("La sesion ha caducado")
		self.close()
	</script>
<%END IF%>
