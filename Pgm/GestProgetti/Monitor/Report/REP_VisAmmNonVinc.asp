<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<% if session("progetto") <> "" then%>
<!--#include virtual ="/util/portallib.asp"-->
<html>
<!--	BLOCCO HEAD			-->
<head>
<title>Report Ammissibili Non Vincitori</title>
<link REL="STYLESHEET" TYPE="text/css" HREF="..\..\fogliostile.css">
<!--	BLOCCO SCRIPT		-->
<script language="Javascript">
<!--#include Virtual = "/Include/help.inc"-->
function Scarica(report)
{
	f=report;
	fin=window.open(f,"","toolbar=0;location=0,directories=0,status=0,menubar=1,scrollbars=1,resizable=1,copyhistory=0,width=450,height=340,screenX=200,screenY200");
}
	
</script>
<!--	FINE BLOCCO SCRIPT	-->
</head>
<!--	FINE BLOCCO HEAD	-->
<!--	BLOCCO ASP			-->
<%
Sub Inizio()
%>	
<table border="0" width="500" cellspacing="0" cellpadding="0" height="81">
<tr>
  <td width="500" background="<%=Session("Progetto")%>/images/titoli/strumenti2b.gif" height="81" valign="bottom" align="right">
    <table border="0" width="500" height="30" cellspacing="0" cellpadding="0">
      <tr>
        <td width="100%" valign="top" align="right"><b class="tbltext1a">Gestione Progetti </span></b>
        </td>
      </tr>
    </table>
  </td>
</tr>
</table>
<br>
<table cellpadding="0" cellspacing="0" width="500" border="0">
<tr height="18">
	<td class="sfondomenu" height="18" width="67%">
	<span class="tbltext0"><b>&nbsp;ELENCO RISERVE</b></span></td>
	<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
	<td valign="middle" align="right" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1"></td>
</tr>
<tr width="371" class="SFONDOCOMM">
	<td colspan="3">
		Report degli utenti ammessi ma non vincitori del Bando.
		<a href="Javascript:Show_Help('/Pgm/help/Gestprogetti/monitor/Report/REP_VisAmmNonVinc')">
		<img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0">
		</a>
	</td>
</tr>
<tr height="2">
	<td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif">
	</td>
</tr>
</table>
<br><br>
<%End Sub
	
Sub Fine()
%>
	<!--#include virtual ="/include/closeconn.asp"-->
	<!-- #include virtual="/strutt_coda2.asp"-->
<%
End Sub
%>

<!--------------------------------------------------------------------------------------------------------------------------------------------------------->
<%
'**** Impostazione della tabella dei dati relativi alle persone facenti parte della classe selezionata ****
	Sub ImpostaPag()
%>
<!--#include virtual ="/include/openconn.asp"-->
<%
if ValidateService(session("idutente"),"REP_RICAMMNONVINC", CC) <> "true" then 
	response.redirect "/util/error_login.asp"
end if

	sSelezionato= Request.QueryString ("Sel")

'**** Mi prendo il CENTRO DI IMPIEGO selezionato dalla pagina precedente ****
	sCentroImp = Request.Form("cmbSede")
	aCentroImp = split(sCentroImp, "|")
	sBando = Request.Form("hdnCmbBando")
	sProvincia = Request.Form("cmbProv")
 	sDescPrv	   = DecCodVal("PROV", 0, "", sProvincia,"")
	sDescBando= Request.Form("hDescBnd")
	lPath=Server.MapPath ("/") & session("progetto") & "\DocPers\Temp\"
	lPath= replace(lPath, "/", "\")

%>
<table width="500" border="0" cellspacing="2" cellpadding="1">
<tr> 
	<td align="left" width="150" nowrap class="tbltext1">
		<b>Bando selezionato</b>
	</td>
	<td align="left" class="tbltext">
		<b><%=sDescBando%></b>
	</td>
</tr>		
</tr>
	<td align="left" width="150" nowrap class="tbltext1">
		<b>Provincia selezionata</b>
	</td>
	<td align="left" class="tbltext">
		<b><%=sDescPrv%></b>
	</td>	
</tr>
</table>
<%


select Case sSelezionato
	case 1	
		Dim SQL
				
		SQL = ""
		SQL="SELECT A.COD_FISC,A.COGNOME,A.NOME,TO_char(A.DT_NASC,'DD/MM/YYYY'),A.IND_RES,R.DESCOM," & _
			"A.PRV_RES,A.CAP_RES,A.NUM_TEL " & _
			"FROM PERSONA A, DOMANDA_ISCR B, COMUNE R, BANDO_POSTI BP " & _ 
			"WHERE B.ID_BANDO = " & sBando  & _
			" AND A.ID_PERSONA = B.ID_PERSONA " & _
			"AND A.COM_RES=R.CODCOM " & _
			"AND B.COD_ESITO IS NULL " & _
			"AND B.ESITO_GRAD ='N' " &_
			"AND BP.ID_SEDE = '" & aCentroImp(0) & "'" &_
			" AND B.ID_PERSONA NOT IN (SELECT ID_PERSONA FROM COMPONENTI_CLASSE Z WHERE Z.ID_PERSONA=B.ID_PERSONA) " & _
            "ORDER BY PUNTEGGIO DESC, DT_DOM"            
         
		set RR=server.CreateObject("ADODB.Recordset")			
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
		RR.Open SQL, CC, 1

		nomeFile = Session("IdUtente") & "_" & sBando & "_" & aCentroImp(0)  & "_AmmNonVincRep.xls"
		If RR.eof Then%>
			<br><br>
			<table width="500" cellspacing="0" cellpadding="0" border="0">
			<tr>
				<td align="center">
					<b class="tbltext3"><span class="size">
					Non ci sono riserve da utilizzare
					</b></span>
				</td>
			</tr>
			</table>
			<br>
			<br>
			<br>
			<table cellpadding="0" cellspacing="0" width="300" border="0">	
				<tr align="center">
				  <td nowrap><a href="javascript:history.go(-1)"><img src="<%=session("Progetto")%>/images/indietro.gif" border="0" name="imgPunto2" onmouseover="javascript:window.status=' '; return true"></a></td>
				</tr>
			</table>
		<%else
				 
			Set fso = CreateObject("Scripting.FileSystemObject")
			Set a = fso.CreateTextFile(lPath & nomeFile, True)
			uniti= session("progetto") & "\DocPers\Temp\" & nomeFile
			uniti = replace(uniti,"\","/")
	

			SQL1="SELECT DESCRIZIONE " & _
				"FROM SEDE_IMPRESA " & _ 
				"WHERE ID_SEDE = " & aCentroImp(0)
				
			set rsImpiego=server.CreateObject("ADODB.Recordset")			
'PL-SQL * T-SQL  
SQL1 = TransformPLSQLToTSQL (SQL1) 
			rsImpiego.Open SQL1, CC, 1

			sDescImpiego = rsImpiego("DESCRIZIONE")
			minuto= right("0" & minute(now),2)
			
			a.WriteLine "Elenco Ammissibili al progetto ma non vincitori per il bando " & sDescBando & "/" & sDescImpiego &" - Situazione al " & day(Now) & "/" & month(now) & "/" & year(now) & " Ore " & hour(now) & ":" & minuto
			a.WriteLine ""
			a.WriteLine "Codice Fiscale" & chr(9) & _
						"Cognome" & chr(9) & _
						"Nome" &  chr(9) & _
						"Data di nascita" & chr(9) & _ 
						"Indirizzo residenza" & chr(9) & _  
						"Comune" & chr(9) & _
						"Prv." & chr(9) & _ 
						"CAP" & chr(9) & _ 
						"Telefono" & chr(9) 

	        rptIscritti=RR.GetString (2,,chr(9) ,chr(13) ," ")
	        
	        a.WriteLine rptIscritti
			a.Close
			
		RR.Close
		set RR=nothing		
	%>	
	<table width="500" border="0">
	<tr>
		<td align="middle" colSpan="3">
		<br>
		<b class="tbltext3">
			<span class="size">Il report � stato creato correttamente.
		</b></span>
		<br><br><br>
		</td>
	</tr>
	</table>
	<table width="500" border="0">
	<tr>
		<td align="middle" colSpan="3">
	      <a href="javascript:Scarica('<%=uniti%>')" class="textred"><b>Apri il Report</b></a>&nbsp; 
		</td>
		<td align="middle" colSpan="3">
	      <a href="REP_RicAmmNonVinc.asp" class="textred"><b>Torna all'elenco</b></a> 
		</td>
	</tr>
	</table>
	<br><br><br><br><br>
	<%
	end if
'prendo il valore della provincia di Residenza
	case 0
	SQL="SELECT A.COD_FISC,A.COGNOME,A.NOME,TO_char(A.DT_NASC,'DD/MM/YYYY'),A.IND_RES,R.DESCOM," & _
		"A.PRV_RES,A.CAP_RES,A.NUM_TEL " & _
		"FROM PERSONA A, DOMANDA_ISCR B, COMUNE R " & _ 
		"WHERE B.ID_BANDO = " & sBando & _
		" AND A.ID_PERSONA = B.ID_PERSONA " & _
		"AND A.COM_RES=R.CODCOM " & _
		"AND B.COD_ESITO IS NULL " & _
		"AND B.ESITO_GRAD ='N' " &_
		"AND A.PRV_RES = '" & sProvincia & "'" &_
		" AND B.ID_PERSONA NOT IN (SELECT ID_PERSONA FROM COMPONENTI_CLASSE Z WHERE Z.ID_PERSONA=B.ID_PERSONA) " & _
        "ORDER BY PUNTEGGIO DESC, DT_DOM"
            
     set RR1=server.CreateObject("ADODB.Recordset")			
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
		 RR1.Open SQL, CC, 1
	nomeFile = Session("IdUtente") & "_" & sBando & "_" & sProvincia  & "_AmmNonVincRep.xls"

		If RR1.eof Then%>
			<br><br>
			<table width="500" cellspacing="0" cellpadding="0" border="0">
			<tr>
				<td align="center">
					<b class="tbltext3"><span class="size">
					Non ci sono riserve da utilizzare
					</b></span>
				</td>
			</tr>
			</table>
			<br>
			<br>
			<br>
			<table cellpadding="0" cellspacing="0" width="300" border="0">	
				<tr align="center">
				  <td nowrap><a href="javascript:history.go(-1)"><img src="<%=session("Progetto")%>/images/indietro.gif" border="0" name="imgPunto2" onmouseover="javascript:window.status=' '; return true"></a></td>
				</tr>
			</table>
	  <%else
				 
			Set fso = CreateObject("Scripting.FileSystemObject")
			Set a = fso.CreateTextFile(lPath & nomeFile, True)
			uniti= session("progetto") & "\DocPers\Temp\" & nomeFile
			uniti = replace(uniti,"\","/")
			
			sDescImpiego= DecCodVal("PROV", 0, "", sProvincia,"")	

			minuto= right("0" & minute(now),2)
			
			a.WriteLine "Elenco Ammissibili al progetto ma non vincitori per il bando " & sDescBando & " / Residenti nella Provincia di " & sDescPrv &" - Situazione al " & day(Now) & "/" & month(now) & "/" & year(now) & " Ore " & hour(now) & ":" & minuto
			a.WriteLine ""
			a.WriteLine "Codice Fiscale" & chr(9) & _
						"Cognome" & chr(9) & _
						"Nome" &  chr(9) & _
						"Data di nascita" & chr(9) & _ 
						"Indirizzo residenza" & chr(9) & _  
						"Comune" & chr(9) & _
						"Prv." & chr(9) & _ 
						"CAP" & chr(9) & _ 
						"Telefono" & chr(9) 

	        rptIscritti=RR1.GetString (2,,chr(9) ,chr(13) ," ")

	        a.WriteLine rptIscritti
			a.Close
	RR1.Close
	set RR1=nothing	       
	%>
	<table width="500" border="0">
	<tr>
		<td align="middle" colSpan="3">
		<br>
		<b class="tbltext3">
			<span class="size">Il report � stato creato correttamente.
		</b></span>
		<br><br><br>
		</td>
	</tr>
	</table>
	<table width="500" border="0">
	<tr>
		<td align="middle" colSpan="3">
	      <a href="javascript:Scarica('<%=uniti%>')" class="textred"><b>Apri il Report</b></a>&nbsp; 
		</td>
		<td align="middle" colSpan="3">
	      <a href="REP_RicAmmNonVinc.asp" class="textred"><b>Torna all'elenco</b></a> 
		</td>
	</tr>
	</table>
	<br><br><br><br><br>
<%
	end if
end select
End Sub
%>
<!--------------------------------------------------------------------------------------------------------------------------------------------------------->
<!--	FINE BLOCCO ASP		-->

<!--		MAIN			-->
	<!--#include virtual ="/include/openconn.asp"-->
	<!--include virtual ="/include/ckProfile.asp"-->
	<!--#include virtual ="/Util/DBUtil.asp"-->
	<!--#include virtual ="/include/ControlDateVB.asp"-->			
	<!--#include virtual ="/include/DecCod.asp"-->
	<!-- #include virtual="/strutt_testa2.asp"-->
<%	
	'if ValidateService(session("idpersona"),"rep_ricammnonvinc") <> "true" then 
	'	response.redirect "../../util/error_login.asp"
	'end if

	Dim strConn, nData, sDiritti, sDescPrv
	Dim sCform, sModo, nData1, RifClassi, icf, sCodSess
	Dim nClasse, sTpClasse, sTipo	
	const lenColOre = 45
	const lenColNomi = 200
		
	sBando=Request("cmbBando")
		
	Inizio()
	ImpostaPag()
%>
<!--	<br>	<table  width="500"  cellspacing=0 cellpadding=0 border=0>	<tr>		<td align="center">			<b class="tbltext3">	          <A href="REP_RicAmmNonVinc.asp">torna all'elenco</A> 			</b>		</td>-->
<!--			<td align="center">				<b class="tbltext">				<a href="javascript:history.back()">Pagina Precedente</a>				</b>			</td>	</tr>	</table>	<BR>-->
<%
	Fine()
%>
<!--	FINE BLOCCO MAIN	-->
<%
else%>
	<script>
	alert("Sessione scaduta")
	self.close()
	</script>
<%end if%>
