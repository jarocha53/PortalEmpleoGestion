<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include virtual = "/strutt_testa2.asp"-->
<!--#include virtual ="/include/openconn.asp"-->
<!--#include virtual = "util/portallib.asp"-->
<!--#include virtual = "util/dbutil.asp"-->
<!--#include virtual = "include/DecCod.asp"-->
<!--#include virtual = "include/SelAreaTerrBandi.asp"-->
<!--#include virtual = "include/ControlDateVB.asp"-->

<script language="Javascript">
<!--#include virtual="/Include/help.inc"-->

var fin
	
function Scarica(report) {
	fin=window.open(report,'wind',"toolbar=0;location=0,directories=0,status=0,menubar=1,scrollbars=1,resizable=1,copyhistory=0,width=450,height=340,screenX=200,screenY200");
}
</script>

<%  	
dim sBando,aBando,nIdBando,sDescBando
dim sSessForm,aSessForm,sCodSessForm,sDescSessForm
dim sDataPeriodo

sBando = Request.Form("CmbBando")
aBando = split(sBando,"|")
nIdBando = aBando(0)
sDescBando = aBando(1) 

sSessForm = Request.Form("CmbSessForm")
aSessForm = split(sSessForm,"|")
sCodSessForm = aSessForm(0)
sDescSessForm = aSessForm(1)

sDataPeriodo = Request.Form("Dt_Periodo")
%>

<table border="0" width="520" cellspacing="0" cellpadding="0" height="81">
	<tr>
		<td width="500" background="<%=Session("Progetto")%>/images/titoli/strumenti2b.gif" height="81" valign="bottom" align="right">
			<table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
				<tr>
					<td width="100%" valign="top" align="right"><b class="tbltext1a">Gestione Progetti</span></b></td>
				</tr>
			</table>
	   </td>
	</tr>
</table>
<br>
<table cellpadding="0" cellspacing="0" width="500" border="0">
	<tr height="18">
		<td class="sfondomenu" height="18" width="67%">
			<span class="tbltext0"><b>&nbsp;REPORT COMPONENTI CLASSE</b></span></td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
		<td valign="middle" align="right" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1"></td>
	</tr>
	<tr width="371" class="SFONDOCOMM">
		<td colspan="3">Da questa pagina e' possibile scaricare su file le informazioni sui discenti relativamente ai criteri di ricerca espressi.
			<a href="Javascript:Show_Help('/Pgm/help/Gestprogetti/Monitor/Report/REP_VisDiscenti')">
			<img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
		</td>
	</tr>
	<tr height="2">
		<td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif">
		</td>
	</tr>
</table>

<!------------------------------------------------------------------------------------>
<%

SERVER.ScriptTimeout = SERVER.ScriptTimeout * 3

sql =   " SELECT DESC_PROJ AS PROGETTO,P.ID_PROJ,AREA_WEB" &_
		" FROM BANDO B, PROGETTO P " &_
		" WHERE B.ID_PROJ = P.ID_PROJ " &_
		" AND B.ID_BANDO = " & nIdBando

'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
Set RsProj=CC.execute(sql)

'-----------------------------------------------------------------------------

sSql =	"SELECT DISTINCT P.COD_FISC,P.COGNOME, P.NOME,P.DT_NASC," &_
		" P.IND_RES,CO.DESCOM,P.PRV_RES,P.CAP_RES,a.desc_aula," &_
		" P.NUM_TEL,CP.ID_AULA,cf.indirizzo,cf.prv,cf.comune,CF.CAP,CF.DESC_CFORM,CP.DT_INISESS" &_
		" FROM PERSONA P,centro_formazione cf,aula a ,DOMANDA_ISCR DI, COMUNE CO," &_
		" CLASSE C, CLASSE_PERCORSO CP, COMPONENTI_CLASSE CC" &_
		" WHERE a.id_cform = cf.id_cform(+)" &_
		" and P.ID_PERSONA = DI.ID_PERSONA" &_
		" and CP.ID_AULA = A.ID_AULA(+)" &_
		" AND P.COM_RES = CO.CODCOM" &_
		" AND DI.ID_BANDO = " & nIdBando  &_
		" AND C.ID_BANDO = " & nIdBando  &_
		" AND C.ID_UORG = " & session("iduorg") &_
		" AND CP.ID_CLASSE = C.ID_CLASSE" &_
		" AND CP.COD_SESSIONE = '" & sCodSessForm & "'" &_
		" AND CP.DT_INISESS <= " & ConvDateToDb(ConvStringToDate(sDataPeriodo)) &_
		" AND CP.DT_FINSESS >= " & ConvDateToDb(ConvStringToDate(sDataPeriodo)) &_
		" AND CC.ID_CLASSE = C.ID_CLASSE" &_
		" AND CC.DT_RUOLODAL <= " & ConvDateToDb(ConvStringToDate(sDataPeriodo)) &_
		" AND (CC.DT_RUOLOAL >= " & ConvDateToDb(ConvStringToDate(sDataPeriodo)) &_
				" OR CC.DT_RUOLOAL IS NULL)" &_	
		" AND P.ID_PERSONA = CC.ID_PERSONA" &_
		" ORDER BY P.COGNOME,P.NOME"

'Response.Write sSql	  

set RsPersona=server.CreateObject("ADODB.Recordset")	
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
RsPersona.Open sSql,CC, 1

'---------------------------------------------------------------------------

lPath= Server.MapPath("/") &  session("progetto") & "\docpers\temp\"

sDtOggi = ConvDatetostring(now())
'Faccio la replace in modo che la data mi venga senza la sbarra
sDtOggi = replace(sDtOggi,"/","")
nomeFile = Session("IDUTENTE") & "_" & nIdBando & "_" & sDtOggi & ".xls"

sFileName= session("progetto") & "\\docpers\\temp\\" & nomeFile

nFileExt=lPath & nomeFile
%>
<form name="frmVisDiscenti" action method="post" action="REP_VisDiscenti.asp">
     <br>	
	<table width="500" cellspacing="2" cellpadding="1" border="0">		
		<tr align="left" height="23">
			<td class="tbltext1" width="145"><b> Bando </b></td>
			<td class="TBLTEXT"><b><%=sDescBando%></b></td>
		</tr>
		<tr align="left" height="23">
			<td class="tbltext1" width="145"><b>Sessione Formativa</b></td>
			<td class="TBLTEXT"><b><%=sDescSessForm%></b></td>				
		</tr>
		<tr align="left" height="23">
			<td class="tbltext1" width="145"><b>Data Riferimento</b></td>
			<td class="TBLTEXT"><b><%=sDataPeriodo%></b></td>				
		</tr>
	</table>
	<br>
<%
IF NOT RsPersona.EOF THEN%> 
	
	<br>
	<table cellpadding="0" cellspacing="0" width="300" border="0">	
		<tr>
			<td align="center" class="tbltext3" colspan="2">
				<b>Il report � stato creato correttamente</b>
			</td>
		</tr>	
	</table>	
	<br>
	<br>
	<table cellpadding="0" cellspacing="0" width="300" border="0">	
		<tr align="center">
			<td nowrap><a href="javascript:Scarica('<%=sFileName%>')" onmouseover="javascript:window.status=' '; return true" class="textred"><b>Visualizza Report </b> </a> </td>	
		</tr>
		<tr><td>&nbsp;</td></tr>
		<tr align="center">
			<td nowrap><a href="javascript:history.back()" onmouseover="javascript:window.status=' '; return true"><img src="<%=session("Progetto")%>/images/indietro.gif" border="0" name="imgPunto2"></a></td>
	    </tr>
	</table>	
<%
else
%>	
	<br>
	<table width="500" cellspacing="2" cellpadding="1" border="0">
		<tr>
			<td align="center" class="tbltext3" colspan="2">
				<b>Non ci sono Discenti in aula nel periodo selezionato</b>
			</td>
		</tr>	
	</table>	
	<br>
	<table cellpadding="0" cellspacing="0" width="300" border="0">	
		<tr align="center">
			<td nowrap><a href="javascript:javascript:history.back()" onmouseover="javascript:window.status=' '; return true"><img src="<%=session("Progetto")%>/images/indietro.gif" border="0" name="imgPunto2"></a></td>
	    </tr>
	</table>	

<%
End If
		
If Not RsPersona.eof Then
		 
	Set fso = CreateObject("Scripting.FileSystemObject")
	Set a = fso.CreateTextFile(nFileExt, True)
			
	a.WriteLine "Discenti in aula il " & sDataPeriodo & " per il Bando " & sDescBando & " e la Sessione Formativa " & sDescSessForm & " - Situazione al " & day(Now) & "/" & month(now) & "/" & year(now) & " Ore " & hour(now) & ":" & minute(now)
	a.WriteLine ""
	a.WriteLine chr(9) &	"Codice Fiscale"				& chr(9) & _  
							"Cognome"						& chr(9) & _
							"Nome"							& chr(9) & _
							"Data di nascita"				& chr(9) & _ 
							"Indirizzo Residenza"			& chr(9) & _ 						 
							"Comune"						& chr(9) & _
							"Prv."							& chr(9) & _ 						 
							"CAP"							& chr(9) & _ 						 
							"Telefono"						& chr(9) & _						 
 							"Centro di Formazione"			& chr(9) & _ 
							"Aula"							& chr(9) &_
							"Indirizzo"						& chr(9) &_
							"Comune"						& chr(9) &_
							"Prv."							& chr(9) & _ 
							"CAP"							& chr(9) & _ 						 
							"Data di presentazione in aula"	& chr(9) 

	RsPersona.MoveFirst
		
	Do while not RsPersona.eof

		'if RsPersona("ID_AULA") <> "" then
		'	sSql=   "SELECT ID_CFORM,DESC_AULA FROM AULA" &_
		'		" WHERE  ID_AULA =" & RsPersona("ID_AULA")
			'Response.Write sSql
		'	set RsAula = server.CreateObject("ADODB.Recordset")	
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
		'	RsAula.Open sSql,CC, 1
			'if not RsAula.EOF then 
				'sSql=   "SELECT DESC_CFORM FROM CENTRO_FORMAZIONE" &_
				'		" WHERE  ID_CFORM =" & RsAula("ID_CFORM")
				'Response.Write sSql
				'set RsCForm = server.CreateObject("ADODB.Recordset")	
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
				'RsCForm.Open sSql,CC, 1
			'end if
		'end if
		if RsPersona("comune") <> "" then
			sSql =	"SELECT DESCOM" &_
					" FROM COMUNE" &_
					" WHERE CODCOM = '" & RsPersona("comune") & "'"
			'Response.Write sSql
			set RsComune = server.CreateObject("ADODB.Recordset")	
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
			RsComune.Open sSql,CC, 1
		end if

				RptDiscenti = chr(9)
				RptDiscenti =  RptDiscenti &  RsPersona("COD_FISC")     & chr(9)
				RptDiscenti =  RptDiscenti &  RsPersona("COGNOME")      & chr(9)
				RptDiscenti =  RptDiscenti &  RsPersona("NOME")         & chr(9)
				RptDiscenti =  RptDiscenti &  ConvDateToString(RsPersona("DT_NASC"))    & chr(9)
				RptDiscenti =  RptDiscenti &  RsPersona("IND_RES")      & chr(9)
				RptDiscenti =  RptDiscenti &  RsPersona("DESCOM")       & chr(9)
				RptDiscenti =  RptDiscenti &  RsPersona("PRV_RES")      & chr(9)
				RptDiscenti =  RptDiscenti &  RsPersona("CAP_RES")      & chr(9)
				RptDiscenti =  RptDiscenti &  RsPersona("NUM_TEL")      & chr(9)

	if RsPersona("ID_AULA") <> "" then
			'if not RsCForm.EOF then
			RptDiscenti =  RptDiscenti &  RsPersona("DESC_CFORM")	& chr(9)
			RptDiscenti =  RptDiscenti &  RsPersona("DESC_AULA")	& chr(9)
			RptDiscenti = RptDiscenti &  RsPersona("INDIRIZZO")      & chr(9)
				if RsPersona("COMUNE") <> "" then
					if not RsComune.EOF then
						RptDiscenti =  RptDiscenti &  RsComune("DESCOM")    & chr(9)
						RsComune.Close: set RsComune = nothing
					else
						RptDiscenti =  RptDiscenti &  chr(9)			
					end if
			
			else
				RptDiscenti =  RptDiscenti &  chr(9)			
			end if

			RptDiscenti =  RptDiscenti &  RsPersona("PRV")      & chr(9)
			RptDiscenti =  RptDiscenti &  RsPersona("CAP")      & chr(9)
	else
			RptDiscenti =  RptDiscenti &  chr(9)			
			RptDiscenti =  RptDiscenti &  chr(9)
			RptDiscenti =  RptDiscenti &  chr(9)			
			RptDiscenti =  RptDiscenti &  chr(9)
			RptDiscenti =  RptDiscenti &  chr(9)			
			RptDiscenti =  RptDiscenti &  chr(9)					
	end if			

			'	RsCForm.Close: set RsCForm = nothing
			'else
			'	RptDiscenti =  RptDiscenti &  chr(9)			
			'end if
			'if not RsAula.EOF then
			
			'	RsAula.Close: set RsAula = nothing
			'else
				'RptDiscenti =  RptDiscenti &  chr(9)
		'end if		
		
		RptDiscenti =  RptDiscenti &  ConvDateToString(RsPersona("DT_INISESS"))     & chr(9)
		    			    
		a.WriteLine RptDiscenti

		RsPersona.MoveNext 
	Loop
	
	a.Close

	Set a   = nothing
	Set fSO = nothing
	RsPersona.Close: set RsPersona = nothing
			
    SERVER.ScriptTimeout = SERVER.ScriptTimeout / 3 		

End If
%>	

</form>

<!------------------------------------------------------------------------------->
<!--#include virtual ="/include/closeconn.asp"-->
<!--#include virtual = "strutt_coda2.asp"-->
<!------------------------------------------------------------------------------->
