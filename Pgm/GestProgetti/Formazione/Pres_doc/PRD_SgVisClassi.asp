<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include virtual ="/util/portallib.asp"-->
<html>
<!--	BLOCCO HEAD			-->
<head>
	<meta NAME="GENERATOR" Content="Microsoft Visual Studio 6.0">
	<title>Riepilogo stato registri</title>
	<link REL="STYLESHEET" TYPE="text/css" HREF="..\..\fogliostile.css">
	<!-- JavaScript immediate script -->
		<script LANGUAGE="JavaScript">
		<!--
		function Show_Help(W2Show)
		{
			f=W2Show;
			w=(screen.width-(screen.width/2))/2;	
			h=(screen.height-(screen.height/2))/2;
			fin=window.open(f,"pippo","toolbar=0, location=0,directories=0,status=0,menubar=0,scrollbars=0,resizable=0,copyhistory=0,width=600,height=480,screenX=w,screenY=h");	
		}
		
	// -->
		</script>
</head>
<!--	FINE BLOCCO HEAD	-->

<!--	BLOCCO ASP			-->
<%
	Sub Inizio()
%>	
	<body background="../../images/sfondo1.gif">
	<basefont class="tbltext">
	<br> 
	<table width="630" border="0" cellspacing="2" cellpadding="1">
		<tr> 
			<td colspan="2" align="middle" background="../../images/sfmnsmall.jpg">
				<b class="titolo">Riepilogo stato registri &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b>
				<a href="javascript:Show_Help('../../help/registro di classe/Prd_SgVisClassi.htm')">
				<img src="../../images/imghelp.gif" border="0">
				</a>
			</td>
		</tr>
	</table>
	<br>
<%
	End Sub
%>
<!--------------------------------------------------------------------------------------------------------------------------------------------------------->
<%
	Sub Fine()
%>
		<!--#include file ="../../include/fine.asp"-->
<%
	End Sub
%>	
<!--------------------------------------------------------------------------------------------------------------------------------------------------------->
<%
	Sub ImpostaPag()
		'Impostazione della tabella dei dati relativi alle classi
		dim  sql, RR, sPrv, sCodSess, Rif, sSess, sProvincia, RifSegn
		Dim   sCf, Cf, nCf
	
		sPrv = Request.Form("cboProv")
		if sPrv = "" then
			sPrv = Request.Form("prv")
			if sPrv = "" then
				sPrv = Request.QueryString("Prv")
			end if
		end if
	
		sCodSess = Request.Form("cboSess")
		Cf = split(Request.Form("cboCentri"), "$",-1,1)
		nCf = Cf(0)
		sCf = cf(1)
		
		sSess = DecCodVal("SESFO", "0", "", sCodSess, 1)
		sProvincia = DecCodVal("PROV", "0", "", sPrv, 1)
		
		'si estrapolano i centri di formazione e le rispettive classi relativi alla provincia selezionata
		'aventi una sessione formativa con formazione in aula.
		sql = " SELECT DISTINCT CENTRO_FORMAZIONE.ID_CFORM, CENTRO_FORMAZIONE.DESC_CFORM, SESS_FORM.ID_EDIZIONE, "
		sql = sql & " CLASSE.TIPOCLASSE, CLASSE.ID_CLASSE, AULA.DESC_AULA, SESS_FORM.DT_INISESS, SESS_FORM.DT_FINSESS"
		sql = sql & " FROM CENTRO_FORMAZIONE, CLASSE, SESS_FORM, AULA"
		sql = sql & " WHERE CENTRO_FORMAZIONE.PRV='" & sPrv & "'"
		sql = sql & " AND CENTRO_FORMAZIONE.ID_CFORM = AULA.ID_CFORM "
		sql = sql & " AND CLASSE.ID_AULA = AULA.ID_AULA "
		sql = sql & " AND NOT AULA.TIPO_AULA LIKE 'C%' OR AULA.TIPO_AULA IS NULL "
		sql = sql & " AND CENTRO_FORMAZIONE.ID_CFORM =" & nCf
		sql = sql & " AND SESS_FORM.COD_SESSIONE ='" & sCodSess & "'"
		sql = sql & " AND SESS_FORM.ID_EDIZIONE = CLASSE.ID_EDIZIONE"
		
		'Response.Write sql
		
		set RR=server.CreateObject("ADODB.Recordset")
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
		RR.open sql, CC, 3

		sql=""
%>
		<br>
		<table border="0" cellspacing="2" cellpadding="1" width="630">
			<tr align="center">
				<td class="tbltext" align="left" width="150">
					<b>Provincia &nbsp;</b>
				</td>	
				<td align="left" class="tbltext3" colspan="2">
					<strong><%=sProvincia%></strong>
				</td>
			</tr>
			<tr align="center">
				<td class="tbltext" align="left" width="150">
					<b>Sessione Formativa &nbsp;</b>
				</td>	
				<td align="left" class="tbltext3" colspan="2">
					<strong><%=sSess%></strong>
				</td>
			</tr>
			<tr align="center">
				<td class="tbltext" align="left" width="150">
					<b>Centro di Formazione &nbsp;</b>
				</td>	
				<td align="left" class="tbltext3" colspan="2">
					<strong><%=sCf%></strong>
				</td>
			</tr>
		</table>
<%	
		If not RR.EOF  then 
%>			
			<br>
			<table width="630" cellspacing="0" cellpadding="1" border="0">
				<tr>
					<td align="left" class="tbllabel1" height="23" width="190">
						&nbsp;Centro di Formazione
						</b></font>
					</td>
					<td align="left" class="tbllabel1">
						&nbsp;&nbsp;Classe
						</b></font>
					</td>
				</tr>
			</table>
			<table width="630px" cellspacing="2" cellpadding="1" border="0">
	<%		
				Do while not RR.eof
					
					'gi = day(RR.Fields("DT_INISESS"))
					'mi = month(RR.Fields("DT_INISESS"))
					'ai = year(RR.Fields("DT_INISESS"))
					dti = ConvDateToString(RR.Fields("DT_INISESS"))
				
				IF RR.Fields("DT_FINSESS") <> "" then
					'gf = day(RR.Fields("DT_FINSESS"))
					'mf = month(RR.Fields("DT_FINSESS"))
					'af = year(RR.Fields("DT_FINSESS"))
					dtf = ConvDateToString(RR.Fields("DT_FINSESS"))
				else
					dtf = "Data da definire"
				end if
				
					sDescAula = RR.Fields("DESC_AULA") & " (" & dti & "-" & dtf & ")"
					sCform =RR.fields("DESC_CFORM")
					Rif = "PRD_SgVisPresenze.asp?modo=0&prv=" & sPrv & "&CodSess=" & sCodSess & "&IdEdiz=" & RR.Fields("ID_EDIZIONE")
					Rif = Rif & "&Aula=" & Server.URLEncode(sDescAula) & "&classe=" & RR.Fields("ID_CLASSE") & "&cf=" & Server.URLEncode(sCform) & "&Icf=" & RR.fields("ID_CFORM")
	%>			<tr class="tblsfondo">
					<td class="tbltext" width="190">
							<%=sCform%>
					</td>
					<td class="tbltext">
						<a href="<%=Rif%>" onmouseover="window.status =' '; return true"><%=sDescAula%> </a>
					</td>
				</tr>
				<%RR.MoveNext
				  Loop%>
				<%RR.CLOSE%>
				
			</table>
<%		else%>
			<br><br>
			<table width="630px" cellspacing="0" cellpadding="0" border="0">
				<tr>
					<td align="center">
						<b class="tbltext3"><span class="size">
						Sessione formativa non attiva per la provincia - data selezionata
						</b></span>
					</td>
				</tr>
			</table>
<%		End if
%>		
		<br>
		<table width="630px" cellspacing="0" cellpadding="0" border="0">
			<tr>
				<td align="center">
					<b class="tbltext">
					<a href="PRD_SgRicPresenze.asp">Pagina Precedente</a>
					</b>
				</td>
			</tr>
		</table>
<%	End Sub%>
<!--------------------------------------------------------------------------------------------------------------------------------------------------------->
<!--	FINE BLOCCO ASP		-->
<!--		MAIN			-->

<%
	Dim strConn
%>		
	<!--#include virtual = "include/openconn.asp"-->
	<!--#include virtual = "include/ckProfile.asp"-->
	<!--#include virtual = "include/DecCod.asp"-->
	<!--#include virtual = "include/SysFunction.asp"-->
	<!--#include virtual = "include/ControlDateVB.asp"-->		
	<!--#include virtual = "Util/DBUtil.asp"-->
<%
	if ValidateService(session("idpersona"),"Sit_Presenze_doc") <> "true" then 
		response.redirect "../../util/error_login.asp"
	end if
	
	Inizio()
	ImpostaPag()
%>	
	<!--#include file ="../../include/closeconn.asp"-->
	
<%		
	Fine()
%>
<!--	FINE BLOCCO MAIN	-->
