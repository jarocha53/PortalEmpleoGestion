<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include virtual = "/include/openconn.asp"-->
<!--#include virtual = "/util/Portallib.asp"-->
<!--#include virtual = "/include/ControlDateVB.asp" -->
<!--#include virtual = "/util/dbUtil.asp" -->
<!--#include virtual = "/include/RuoloFunzionale.asp" -->
<html>
<head>
<title>Inserimento ruolo</title>
<link REL="STYLESHEET" TYPE="text/css" HREF="<%=session("progetto")%>/fogliostile.css">
</head>
<script language="Javascript">
<!--#include virtual = "/include/help.inc" -->

	function ControllaDatiGruppo(){
		if ( frmInsRuolo.cmbGruppo.value == "" ) {
			alert("Indicare il gruppo");
			frmInsRuolo.cmbGruppo.focus();
			return false;
		}
		return true;
		
	}

	function ControllaDati(){
		if ( frmInsRuolo.cmbRuolo.value == "" ) {
			alert("Indicare il ruolo organizzativo");
			frmInsRuolo.cmbRuolo.focus();
			return false;
		}
		return true;
		
	}
	function ControllaDatiCNF(){
		if ( frmRuolo.cmbUtente.value == "" ) {
			alert("Selezionare un utente");
			frmRuolo.cmbUtente.focus();
			return false;
		}
		return true;
	}
</script>
<%
if ValidateService(session("idutente"),"CLA_VisClassi",CC) <> "true" then 
	response.redirect "/util/error_login.asp"
end if
dim sValoriArray,nIdClasse,sSQL,nIdGruppo

nIdClasse = CLng(Request("IdClasse"))
sCodRuolo = Request.Form("cmbRuolo") 
sCodSessione = Request("sessione") 

dDtSessDal = Request("DtDal") 
dDtSessAl  = Request("DtAl")
nIdGruppo = clng(Request.Form("cmbGruppo"))
 
InfoProc()
sValoriArray= "cmbRuolo|1|" & sCodRuolo & "| onchange='Javascript:frmInsRuolo.submit()'"
%>
<form name="frmInsRuolo" method="Post" onsubmit="return ControllaDati()" action="CLA_InsRuolo.asp">
	<input type="hidden" name="IdClasse" value="<%=nIdClasse%>">
	<input type="hidden" name="sessione" value="<%=sCodSessione%>">
	<input type="hidden" name="DtDal" value="<%=dDtSessDal%>">
	<input type="hidden" name="DtAl" value="<%=dDtSessAl%>">
	
<table cellpadding="2" cellspacing="2" width="500" border="0">
	<tr>
		<td class="tbltext1" width="20%">
		<b>Ruolo :</b>
		</td>
		<td width="80%">
			<%CreateRuolo(sValoriArray)%>
		</td>
	</tr>
</form>
</table>

<%if sCodRuolo <> "" then%>

	
<table cellpadding="2" cellspacing="2" width="500" border="0">
<form name="frmGruppo" method="Post" onsubmit="return ControllaDatiGruppo()" action="CLA_InsRuolo.asp">
	<input type="hidden" name="IdClasse" value="<%=nIdClasse%>">
	<input type="hidden" name="sessione" value="<%=sCodSessione%>">
	<input type="hidden" name="DtDal" value="<%=dDtSessDal%>">
	<input type="hidden" name="DtAl" value="<%=dDtSessAl%>">
	<input type="hidden" name="cmbRuolo" value="<%=sCodRuolo%>">

	<tr>
		<td class="tbltext1" width="20%">
		<b>Gruppo :</b>
		</td>
		<td width="80%" class="textBlack">
			<%dim aGruppo(3)
			aGruppo(0) = "cmbGruppo"
			aGruppo(1) = nIdGruppo
			aGruppo(2) = "onchange=""Javascript:frmGruppo.submit();"""
			aGruppo(3) = sCodRuolo
			CreateGruppo(aGruppo)%>
		</td>
	</tr>
</form>
</table>


<%end if %>


<%if nIdGruppo <> 0 then%>
	<table cellpadding="2" cellspacing="2" width="500" border="0">
	<form name="frmRuolo" method="Post" onsubmit="return ControllaDatiCNF()" action="CLA_CnfRuolo.asp">
	<input type="hidden" name="sessione" value="<%=sCodSessione%>">
	<input type="hidden" name="cmbRuolo" value="<%=sCodRuolo%>">
	<input type="hidden" name="IdClasse" value="<%=nIdClasse%>">
	<input type="hidden" name="DtDal" value="<%=dDtSessDal%>">
	<input type="hidden" name="DtAl" value="<%=dDtSessAl%>">
		<tr>
			<td class="tbltext1" width="20%">
			<b>Utente :</b>
			</td>
			<td width="80%" class="textBlack"><b>
<%	
	' Faccio vedere tutti tranne i ruoli gi� assegnati.
'Mario 09/03/2004
	' Faccio vedere i soli utenti abilitati ad accedere (ind_abil=s)
	' Inserito anche l'ordinamento per cognome e nome 
'Mario 09/03/2004
	
	sSQL = "SELECT A.IDUTENTE,COGNOME,NOME FROM UTENTE A,GRUPPO B WHERE " &_
		" A.IDGRUPPO = B.IDGRUPPO " &_
		" AND ID_UORG =" & Session("iduorg") &_
		" AND B.COD_RUOFU='" & sCodRuolo & "'" &_
		" AND B.IDGRUPPO = " & nIdGruppo &_
		" AND IND_ABIL = 'S'" &_
		" AND NOT EXISTS (SELECT IDUTENTE " &_
						" FROM CLASSE_RUOLO " &_
						" WHERE IDUTENTE = A.IDUTENTE" &_
						" AND DT_RUOLOAL = " & ConvDateToDbs(ConvStringToDate(dDtSessAl)) &_
						" AND ID_CLASSE = " & nIdClasse &_
						" AND COD_SESSIONE ='" & sCodSessione & "')" &_
		" ORDER BY COGNOME, NOME"	
	
	'Response.Write sSQL
		
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	set rsUtente = CC.Execute(sSQL)
	
	nFind = 0
	if not rsUtente.eof then
		nFind = 1
	%>
		<select name="cmbUtente" class="textBlack">
			<option></option>
		<%do while not rsUtente.eof %>
			<option value="<%=rsUtente("IDUTENTE")%>"><%=rsUtente("COGNOME")%>&nbsp;<%=rsUtente("NOME")%></option>
			<%rsUtente.MoveNext()
		loop%>
		</select>
	<%else
		Response.Write "Non esistono utenti da poter selezionare"
	end if
	rsUtente.close
	set rsUtente = nothing%>
				</b>
			</td>
		</tr>
	</table>
	<%if nFind = 1 then%>
		<table cellpadding="2" cellspacing="2" width="500" border="0">
		<form name="frmInsRuolo" method="Post" action="CLA_CnfRuolo.asp">
			<tr>
				<td align="right" width="50%">
				<input title="Conferma" type="image" src="<%=Session("progetto")%>/images/conferma.gif">
				</td>
				<td align="left" width="50%">
				<a onmouseover="window.status =' '; return true" title="Chiudi" href="javascript:self.close()"><img border="0" src="<%=Session("progetto")%>/images/chiudi.gif"></a>
				</td>
			</tr>
		</table>
		</form>
	<%
	else
		chiudi()
	end if%>
<%
else
	chiudi()
end if%>
</html>
<!--#include virtual ="/include/closeconn.asp"-->
<%sub chiudi()%>
	<br>
	<table cellpadding="2" cellspacing="2" width="500" border="0">
		<tr>
			<td align="center" width="100%">
			<a onmouseover="window.status =' '; return true" title="Chiudi" href="javascript:self.close()"><img border="0" src="<%=Session("progetto")%>/images/chiudi.gif"></a>
			</td>
		</tr>
	</table>
<%end sub%>

<%sub InfoProc()%>
	<table cellpadding="0" cellspacing="0" width="500" border="0">
		<tr height="18">
			<td class="sfondomenu" height="18" width="50%">
				<span class="tbltext0"><b>&nbsp;GESTIONE CLASSI</b>
				</span>
			</td>
			<td width="5%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
			<td valign="middle" align="right" width="40%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1"></td>
		</tr>
		<tr width="371" class="SFONDOCOMM">
			<td colspan="3">
			Selezione prima la categoria del ruolo, il gruppo e di seguito 
			l'eventuale nominativo ritrovato.
			<a onmouseover="window.status =' '; return true" href="Javascript:Show_Help('/Pgm/Help/gestprogetti/Formazione/Classi/CLA_InsRuolo')">
			<img src="<%=Session("Progetto")%>/images/Help.gif" border="0" align="right" alt="Help">

			</td>
		</tr>
		<tr height="2">
			<td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>
<%end sub%>


