<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include virtual="/strutt_testa2.asp"-->
<!--#include virtual="/include/openconn.asp"-->
<!--#include virtual="/Util/DBUtil.asp"-->
<!--#include virtual="/include/ControlDateVB.asp"-->
<!--#include virtual="/Include/SysFunction.asp"-->
<!--#include virtual="/include/SetPulsanti.asp"-->
<!--#include file	="CLA_InizioClassi.asp"-->

<%sub InfoProc()%>
	<table cellpadding="0" cellspacing="0" width="500" border="0">
		<tr height="18">
			<td class="sfondomenu" height="18" width="50%">
				<span class="tbltext0"><b>&nbsp;GESTIONE CLASSI</b>
				</span>
			</td>
			<td width="5%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
			<td valign="middle" align="right" width="40%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1"></td>
		</tr>
		<tr width="371" class="SFONDOCOMM">
			<td colspan="3">
			Annullamento della programmazione per la classe selezionata.
			<a onmouseover="window.status =' '; return true" href="Javascript:Show_Help('/Pgm/Help/gestprogetti/Formazione/Classi/CLA_ModClassi')">
			<img src="<%=Session("Progetto")%>/images/Help.gif" border="0" align="right" alt="Help">
			</td>
		</tr>
		<tr height="2">
			<td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>
<%end sub

'Eseguo l'aggiornamento della tabella classe_percorso impostando la 
'data a null e cancello l'eventuale presenza su classe_ruolo
	
	sub Aggiorna
		CC.BeginTrans
		sSQL = "update classe_percorso set dt_inisess = NULL" &_
			", dt_finsess = NULL "  &_
			", dt_tmst = " & ConvDateToDb(Now())  &_
			" where id_classe = " & nIdClasse &_
			" and cod_sessione = '" & sCodSessione & "'" &_
			" and dt_inisess = " & ConvDateToDbs(dDTDal) &_
			" and dt_finsess = " & ConvDateToDbs(dDTAl)
		
		sErrore=EseguiNoC(sSQL,CC)
		
		if sErrore <> "0" then
			CC.RollBackTrans
%>
			<br><br>
			<table border="0" cellspacing="2" cellpadding="1" width="500">
				<tr align="middle">
					<td class="tbltext3">
						Impossibile rimuovere la Sessione Formativa.
						<br><br>
						Errore: <%=sErrore%>
					</td>
				</tr>
			</table>		
			<br><br>
<% 
		else
			sSQL = "delete from classe_ruolo where id_classe = " & nIdClasse &_
				" and cod_sessione = '" & sCodSessione & "'" &_
				" and dt_ruolodal = " & ConvDateToDbs(dDTDal) &_
				" and dt_ruoloal = " & ConvDateToDbs(dDTAl)
				
			sErrore=EseguiNoC(sSQL,CC)
		
			IF sErrore <> "0" then
				CC.RollBackTrans
			ELSE
				CC.CommitTrans
%>
					<script language="Javascript">
						indietro.Sessione.value = '<%=sSessNav%>'
						alert("Annullamento della Sessione Formativa\ncorrettamente effettuato")
						indietro.submit() 
					</script>
				<br><br>
<%
			END IF
		end if	
	end sub
%>
<script language="Javascript">
	<!--#include virtual = "/Include/help.inc"-->
</script>
<%
'---------------------------------------------------------------------
'	MAIN
'
		
	dim nIdClasse,sCodSessione
	dim dDTDal,dDTAl,dOggi,dDtTmst
	dim sSQL, sSQL2 

	Inizio()
	
	nBando			= Request.Form ("cmbBando")
	sDescAula		= Request.Form ("descAula")
	
	nIdClasse		= CLng(Request.Form("txtClasse"))	
	sCodSessione	= Request.Form("cmbSessioni")
	dDTDal			= Trim(Request.Form("txtDtDal"))
	dDTAl			= Trim(Request.Form("txtDtAl"))
	dOggi			= Date
	dDtTmst			= Date


%>
<form name="indietro" method="post" action="CLA_ModClassi.asp">
	<input type="hidden" name="Bando" value="<%=nBando%>">
	<input type="hidden" name="Sessione" value="<%=sSessione%>">
	<input type="hidden" name="ID" value="<%=nIdClasse%>">
	<input type="hidden" name="descAula" value="<%=sDescAula%>">
</form>
<%
	InfoProc()
'Controllo se per la classe in oggetto esistono altre Sessioni, che 
'hanno la stessa data d'inizio della sessione che voglio annullare: 
'in tal caso la posso annullare.
'Se invece la sessione � l'unica con la sua data inizio, devo controllare 
'sulla tabella componenti_corso se ci sono discenti associati: in tal caso
'non posso cancellare, altrimenti s�
	
	sSQL =  " select count(*) as CSess" &_
			" from classe_percorso" &_
			" where id_classe = " & nIdClasse &_
			" and cod_sessione <> '" & sCodSessione & "'" &_
			" and dt_inisess = " & ConvDateToDbs(dDTDal)
	
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	set rsCSess = cc.execute(sSQL)
		nContaSess = CLng(rsCSess("CSess"))
	rsCSess.close
	set rsCSess = nothing
	
	if nContaSess > 0  then
		Aggiorna()
	else
	
		sSQL =  " select count(*) as CComp" &_
				" from componenti_classe" &_
				" where id_classe = " & nIdClasse &_
				" and dt_ruolodal = " & ConvDateToDbs(dDTDal)
				
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
		set rsCComp = cc.execute(sSQL)
			nContaComp = CLng(rsCComp("CComp"))
			rsCComp.close
		set rsCComp = nothing
					
		IF nContaComp > 0  then	
%>
			<br><br>
			<table border="0" cellspacing="2" cellpadding="1" width="500">
				<tr align="middle">
					<td class="tbltext3">
						Non � possibile annullare la Sessione Formativa:<br>
						sono presenti discenti associati ad essa.
					</td>
				</tr>
			</table>		
			<br>
<%
		ELSE
			Aggiorna()
		END IF
	end if
	
%>
<br>
<table width="500" border="0" cellspacing="2" cellpadding="1">
	<tr> 
		<td align="center">
			<a onmouseover="window.status =' '; return true" title="Indietro" href="Javascript:indietro.submit()"><img border="0" src="<%=Session("Progetto")%>/images/indietro.gif"></a>
		</td>
	</tr>
</table>

<!--#include virtual="/include/closeconn.asp"-->
<!--#include virtual="/strutt_coda2.asp"-->
