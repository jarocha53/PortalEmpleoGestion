<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include virtual ="/Strutt_testa2.asp"-->
<!--#include virtual ="/include/openconn.asp"-->

<!--#include file ="CLA_InizioClassi.asp"-->
<!--#include virtual ="/util/portallib.asp"-->
<!--#include virtual ="/include/SelAreaTerrBandi.asp"-->

<!--#include virtual ="/include/ControlDateVB.asp"-->
<!--#include virtual ="/Util/DBUtil.asp"-->

<%
if ValidateService(session("idutente"),"CLA_VisClassi",CC) <> "true" then 
	response.redirect "/util/error_login.asp"
end if
%>

<script language="Javascript" src="/include/help.inc"></script>
<script LANGUAGE="Javascript">

//include del file per fare i controlli sulla numericit� dei campi
<!--#include virtual = "/Include/ControlNum.inc"-->
<!--#include virtual = "/Include/ControlString.inc"-->

//include del file per fare i controlli sulla validit� delle date
<!--#include virtual = "/Include/ControlDate.inc"-->

	function InviaCFP() {

		if (frmInviaCentro.cmbCForm.value=="") {
			alert("Indicare un centro di formazione")
			frmInviaCentro.cmbCForm.focus();
			return false
		}
		
		return true
		
	}

	function Control() {
	
		if (frmInvia.cmbProvincia.value=="") {
			alert("Indicare una provincia")
			frmInvia.cmbProvincia.focus();
			return false
		}
		return true
	}

	function ControllaDati() {

		if (eval("frmInsAula.cmbAula")) {
			if (frmInsAula.cmbAula.value == ""){
				alert("Dato obbligatorio")
				frmInsAula.cmbAula.focus()
				return false
			}	
		}

		// Controllo obbligatoriet� della data.
		if (frmInsAula.txtDtDal.value == "" ) {
			alert("Indicare la data inizio")
			frmInsAula.txtDtDal.focus();
			return false;
		}			
		// Controllo la validit� della data.
		if (!ValidateInputDate(frmInsAula.txtDtDal.value)) {
			frmInsAula.txtDtDal.focus();
			return false;
		}	
		// Controllo obbligatoriet� della data.
		if (frmInsAula.txtDtAl.value == "") {
			alert("Indicare la data fine")
			frmInsAula.txtDtAl.focus();
			return false;
		}
		
		// Controllo la validit� della data.
		if (!ValidateInputDate(frmInsAula.txtDtAl.value)) {
			frmInsAula.txtDtAl.focus();
			return false;
		}

		if (!ValidateRangeDate(frmInsAula.txtOggi.value,frmInsAula.txtDtDal.value)) {
			alert("La data deve essere superiore alla data odierna")
			frmInsAula.txtDtDal.focus();
			return false;
		
		}
		// Controllo validit� dell'intervallo.
		if (!ValidateRangeDate(frmInsAula.txtDtDal.value,frmInsAula.txtDtAl.value)) {
			alert("L'intervallo della sessione non � valido");
			frmInsAula.txtDtDal.focus();
			return false;
		}

		return true
	}
</script>

<%

dim sIdCForm
dim sSQL
dim sBando
dim sEdizione
dim sTipoFase
dim nBando
dim sSessione
dim sProvincia

nBando		= CLng(Request.Form("Bando"))
sSessione	= Request.Form("Sessione") 
sTipoFase	= DecTipoFase(nBando,sSessione)
sProvincia	= Request.Form ("cmbProvincia")
sCForm		= CLng(Request.Form ("cmbCForm"))
%>
<form name="Indietro" method="Post" action="CLA_VisClassi.asp">
<input type="hidden" name="cmbBando" value="<%=nBando%>">
<input type="hidden" name="cmbSessioni" value="<%=sSessione%>">
</form>

<%
Inizio()
InfoProc()
if sTipoFase = "S" then
	if sProvincia = "" then
		sProv = SetProvBando(nBando,Session("iduorg"))
		SelProvinvie(sProv)
	else
		if sCForm = "0" then
			sProv = SetProvBando(nBando,Session("iduorg"))
			SelProvinvie (sProv)
			SelCentroFormazione (sProvincia)
		else
			InfoArea()
			ImpostaPag()
		end if
	end if
else
	InfoArea()
	ImpostaPag()
end if

sub SelCentroFormazione(sProv)

%>

	<table border="0" cellspacing="2" cellpadding="1" width="500">
	<form name="frmInviaCentro" method="post" action="CLA_InsClassi.asp" onsubmit="return InviaCFP()">
		<input type="hidden" name="Bando" value="<%=nBando%>">
		<input type="hidden" name="Sessione" value="<%=sSessione%>">
		<input type="hidden" name="cmbProvincia" value="<%=sProv%>">
		<tr>
			<td width="150" class="tblText1">	
				<b>Centro formazione : </b>
			</td>
			<td width="350" class="textblack">
<%
			sSQL = "SELECT ID_CFORM , DESC_CFORM FROM CENTRO_FORMAZIONE WHERE PRV='" & sProv & "'"
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
			set rsCform = cc.execute(sSQL)
			if not rsCform.eof then%>
				<select name="cmbCForm" onchange="Javascript:frmInviaCentro.submit()" class="textblack">
					<option></option>
				<%do while not rsCform.eof%>
					<option value="<%=clng(rsCform("ID_CFORM"))%>"><%=rsCform("DESC_CFORM")%></option>
							
					<%rsCform.movenext
				loop
				%>
				</select>
				<%
			else%>
				Non sono presenti centri formazione	per la provincia selezionata
			<%end if
			rsCform.close
			set rsCform = nothing 
			
%>
			</td>
		</tr>
	</form>
	</table>
<%end sub


sub SelProvinvie(sProv)

	dim i
	for i = 1 to Len(sProv) step 2
		sValue = sValue & "'" & Mid(sProv,i,2) & "',"
	next
	sValue = left(sValue,len(sValue)-1) 
%>
	<br>
	<table border="0" cellspacing="2" cellpadding="1" width="500">
	<form name="frmInvia" method="post" action="CLA_InsClassi.asp" onsubmit="return Control()">
		<input type="hidden" name="Bando" value="<%=nBando%>">
		<input type="hidden" name="Sessione" value="<%=sSessione%>">
		<tr>
			<td width="150" class="tblText1">	
				<b>Provincia : </b>
			</td>
			<td width="350">
<%  sInt = "PROV|0|" & date & "|" & sProvincia & "|cmbProvincia' onchange='Javascript:frmInvia.submit()| AND CODICE IN (" & sValue & ") ORDER BY DESCRIZIONE"			
	CreateCombo(sInt)
%>
			</td>
		</tr>
	</form>
	</table>
<%end sub

function DecPrgFase(nBando,sSessione) 

	sSQL = "SELECT PRG_FASE FROM FASE_BANDO WHERE ID_BANDO = " &_
	  nBando & " AND COD_SESSIONE = '" & sSessione & "'"

'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	set rsTFase = cc.execute(sSQL)
	if not rsTFase.eof then
		DecPrgFase = rsTFase("PRG_FASE")
	else ' Questo caso non dovrebbe mai capitare ma lo gestisco ugualmente
		DecPrgFase = "1" 
	end if
	rsTFase.close
	set rsTFase = nothing 

end function


function DecTipoFase(nBando,sSessione) 

	sSQL = "SELECT COD_TFASE FROM FASE_BANDO WHERE ID_BANDO = " &_
	  nBando & " AND COD_SESSIONE = '" & sSessione & "'"

'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	set rsTFase = cc.execute(sSQL)
	if not rsTFase.eof then
		DecTipoFase = rsTFase("COD_TFASE")
	else ' Questo caso non dovrebbe mai capitare ma lo gestisco ugualmente
		DecTipoFase = "N" 
	end if
	rsTFase.close
	set rsTFase = nothing 

end function

sub ImpostaPag%>

	<form name="frmInsAula" onsubmit="return ControllaDati()" method="post" action="CLA_CnfClassi.asp">
	<input type="hidden" name="modo" value="INS">
	<input type="hidden" name="cmbBando" value="<%=nBando%>">
	<input type="hidden" name="cmbSessioni" value="<%=sSessione%>">
	<input type="hidden" name="txtOggi" value="<%=ConvDateToString(date)%>">
	<input type="hidden" name="prgFase" value="<%=DecPrgFase(nBando,sSessione)%>">

	
	<%if sTipoFase = "S" then%>
		<table border="0" cellpadding="1" cellspacing="2" width="500">
			<tr>
  				<td width="125" align="left" class="tblText1">
  					<b>Aule disponibili *</b>
				</td>
			  <%
				' **************************************************************************************
				' In base alla provincia del centro di formazione dell'aula a cui � assegnata la classe,
				' mi ricerco le aule disponibili (che non hanno una classe assegnata). 
				' Nella ricerca include anche l'aula a cui appartiene la classe in oggetto.
				' **************************************************************************************
				sSQL = " SELECT DESC_AULA,A.ID_AULA "
				sSQL = sSQL &  "    FROM AULA A,CENTRO_FORMAZIONE CF "
				sSQL = sSQL &  "    WHERE ID_UORG = " & Session("iduorg") 
				sSQL = sSQL &  " 	  AND A.ID_CFORM = CF.ID_CFORM "
				sSQL = sSQL &  " 	  AND A.ID_CFORM = " & sCForm
				sSQL = sSQL &  "      AND ID_BANDO = " & nBando 
				
'				sSQL = sSQL &  " 	  AND NOT EXISTS ("
'				sSQL = sSQL &  " 	  	  SELECT CP.ID_AULA FROM CLASSE_PERCORSO CP"
'				sSQL = sSQL &  " 	  	  WHERE SYSDATE-1 BETWEEN DT_INISESS AND DT_FINSESS "
'				sSQL = sSQL &  " 	  	  AND A.ID_AULA = CP.ID_AULA)"
				' **************************************************************************************

'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
				Set rsAula = CC.Execute(sSQL)
				if not rsAula.eof then%>
					<td align="left" width="375">
						<select name="cmbAula" class="textBlack">
						<%Do While Not rsAula.EOF%>
							<option value="<%=CLng(rsAula("ID_AULA"))%>" selected><%=rsAula("DESC_AULA")%>
							</option>
							<%rsAula.MoveNext()
						Loop%>
						</select>
					</td>
				<%else%>
					<td align="left" width="375" class="textBlack">
						<b>Non sono presenti aule per il centro d formazione scelto.</b>
					</td>
					</tr>
				</table>
				<br>
				<table border="0" cellpadding="1" cellspacing="2" width="500">
					<tr>
						<td align="center">
							<a onmouseover="window.status =' '; return true" title="Indietro" href="Javascript:Indietro.submit()"><img border="0" src="<%=Session("Progetto")%>/images/indietro.gif"></a>
						</td>
					</tr>
				</table>
					<%
					
					rsAula.Close
					Set rsAula = Nothing				
					exit sub
				end if
				rsAula.Close
				Set rsAula = Nothing%>
			</tr>
		</table>
	<%	end if %>
	<br>
	<table border="0" cellspacing="2" cellpadding="1" width="500">
		<tr class="tblsfondo">
			<td class="tbltext1" align="left" width="50">
				<b>Dal </b>
			</td>	
			<td class="textBlack" align="left" width="200"><b>
				<input class="textBlack" maxlength="10" size="12" type="text" name="txtDtDal">
				</b>
			</td>	
			<td class="tbltext1" align="left" width="50">
				<b>Al</b>
			</td>	
			<td class="textBlack" align="left" width="200"><b>
				<input class="textBlack" maxlength="10" size="12" type="text" name="txtDtAl">
				</b>
			</td>	
		</tr>
		<tr class="tblsfondo">
			<td class="tbltext1" align="center" colspan="4">
				Utilizzabili 
				<label name="NumCaratteri" id="NumCaratteri">100</label>
				caratteri
			</td>
		</tr>	
		<tr class="tblsfondo">
			<td class="tbltext1" align="center"><b>Note</b></td>				
			<td align="left" colspan="3">
			<textarea onKeyup="JavaScript:CheckLenTextArea(Testo,NumCaratteri,100)" cols="40" rows="4" name="Testo"></textarea>
			</td>
		</tr>
	</table>
	
	<% Indietro()%>
	</form>
<%end sub%>



<%sub Indietro()%>
	<br>
	<table border="0" cellpadding="1" cellspacing="2" width="500">
		<tr>
			<td align="right" width="40%">
				<input type="image" src="<%=Session("Progetto")%>/images/conferma.gif" name="Invia" value="Conferma">
			</td>
			<td align="left">
			<a onmouseover="window.status =' '; return true" title="Indietro" href="Javascript:Indietro.submit()"><img border="0" src="<%=Session("Progetto")%>/images/indietro.gif"></a>
			</td>

		</tr>
	</table>

<%end sub%>

<%sub InfoProc()%>
	<table cellpadding="0" cellspacing="0" width="500" border="0">
		<tr height="18">
			<td class="sfondomenu" height="18" width="50%">
				<span class="tbltext0"><b>&nbsp;GESTIONE CLASSI</b>
				</span>
			</td>
			<td width="5%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
			<td valign="middle" align="right" width="40%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1"></td>
		</tr>
		<tr width="371" class="SFONDOCOMM">
			<td colspan="3">
			Qui � possibile inserire il periodo della nuova sessione formativa e, nel caso
			sia una sessione in presenza, inserire anche l'aula che ospite la classe con una
			disponibile.

			<a onmouseover="window.status =' '; return true" href="Javascript:Show_Help('/Pgm/Help/gestprogetti/Formazione/Classi/CLA_InsClassi')">
			<img src="<%=Session("Progetto")%>/images/Help.gif" border="0" align="right" alt="Help">

			</td>
		</tr>
		<tr height="2">
			<td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>
<%end sub%>
<%sub InfoArea()%>
	<br>
	<table border="0" cellspacing="2" cellpadding="1" width="500">
		<tr>
			<td class="tbltext1" align="left" width="125">
				<b>Bando :</b>
			</td>	
			<td class="textBlack" align="left">
				<b><%=DecBando(nBando)%></b>
			</td>
		</tr>
		<tr>
			<td class="tbltext1" align="left" width="125">
				<b>Sessione Formativa :</b>
			</td>	
			<td class="textBlack" align="left" class="textBlack">
				<b><%=DecFaseBando(nBando,sSessione)%></b>
			</td>
		</tr>
	</table>
<%end sub%>

<!---------------------------------------------------------->
<!---------------------------------------------------------->
