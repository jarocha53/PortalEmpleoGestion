<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<script LANGUAGE="JavaScript" src="/Include/help.inc"></script>
<script LANGUAGE="JavaScript">
function Carica_Pagina(npagina) {
	document.frmPagina.pagina.value = npagina;
	document.frmPagina.submit();
}


// Questa funzione fa la replece per ogni carattere contenuto 
// nella stringa e non nel primo che trova.
function replace(argvalue, x, y) { 

	while (argvalue.indexOf(x) != -1) { 
		var leading = argvalue.substring(0, argvalue.indexOf(x)); 
		var trailing = argvalue.substring(argvalue.indexOf(x) + x.length, 
		argvalue.length); 
		argvalue = leading + y + trailing; 
	} 
	return argvalue; 
} 

function ComponentiClasse(nIdBando,sCodSessione,nIdClasse,sDescAula) {
	
	sDescAula = replace(sDescAula, "$", "'")
	window.open("CLA_VisComClas.asp?idClass=" + nIdClasse + "&DescAula=" + 
		 sDescAula + "&IdBando=" + nIdBando + "&CodSess=" + sCodSessione ,"InfoPersona","width=550, height=420; center: Yes; help: No; resizable: No; status: No");
	
}


</script>

<!--	BLOCCO ASP			-->
<%sub InfoProc()%>

	<table cellpadding="0" cellspacing="0" width="500" border="0">
		<tr height="18">
			<td class="sfondomenu" height="18" width="50%">
				<span class="tbltext0"><b>&nbsp;GESTIONE COMPONENTI CLASSI</b>
				</span>
			</td>
			<td width="5%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
			<td valign="middle" align="right" width="40%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1"></td>
		</tr>
		<tr width="371" class="SFONDOCOMM">
			<td colspan="3">
			Selezionare la classe per la quale effettuare la pianificazione dei candidati.
			Selezionare il numero degli allievi per visualizzare il dettaglio delle persone
			programmate nella classe nel periodo indicato.
				<a onmouseover="window.status =' '; return true" href="Javascript:Show_Help('/Pgm/Help/gestprogetti/Formazione/Classi/CLA_SelAule')">
				<img src="<%=Session("Progetto")%>/images/Help.gif" border="0" align="right" alt="Help">

			</td>
		</tr>
		<tr height="2">
			<td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>
<%end sub%>
<!--------------------------------------------------------------------------------------------------------------------------------------------------------->
<%Sub ImpostaPag()
	dim rsCount,nIDClasse,rsClassiDisp,dFInSess,sSQLConta
	dim nCount,sqlClassi,nInd,dIniSess,sDescClasse,nPrgFase
	dim i,Record,nTamPagina,nActPagina
	
	Record=0
	nTamPagina=15

	If Request.Form ("pagina")= "" Then
		nActPagina=1
	Else
		nActPagina=Clng(Request.Form("pagina"))
	End If
	if Err.number <> "0" then
		nActPagina = 1
		Err.Clear
	end if

	' Ritrovo le classi disponibili.
	sqlClassi = " SELECT "
	sqlClassi = sqlClassi &  "   CP.PRG_FASE,"
	sqlClassi = sqlClassi &  "   CP.ID_AULA,"
	sqlClassi = sqlClassi &  "   CP.ID_CLASSE,"
	sqlClassi = sqlClassi &  "   CP.DT_INISESS,"
	sqlClassi = sqlClassi &  "   CP.DT_FINSESS,"
	sqlClassi = sqlClassi &  "   NVL(A.DESC_AULA,'AULA A DISTANZA') AS DESC_AULA"
	sqlClassi = sqlClassi &  "   FROM "
'	sqlClassi = sqlClassi &  "   FASE_BANDO FB,"
	sqlClassi = sqlClassi &  "   CLASSE_PERCORSO CP,"
	sqlClassi = sqlClassi &  "   AULA A,CLASSE C"
	sqlClassi = sqlClassi &  " WHERE C.ID_CLASSE = CP.ID_CLASSE AND "
'	sqlClassi = sqlClassi &  "   CP.COD_SESSIONE = FB.COD_SESSIONE AND"
	sqlClassi = sqlClassi &  "   CP.ID_AULA = A.ID_AULA(+) AND"
	sqlClassi = sqlClassi &  "   C.ID_BANDO = " & nBando
	sqlClassi = sqlClassi &  "   AND CP.COD_SESSIONE = '" & sCodSessione & "'" 
	sqlClassi = sqlClassi &  "   AND DT_INISESS IS NOT NULL AND DT_FINSESS IS NOT NULL "
	sqlClassi = sqlClassi &  "   AND (" & ConvDateToDbs(date) & " BETWEEN DT_INISESS AND DT_FINSESS " 
	sqlClassi = sqlClassi &  "   OR  " & ConvDateToDbs(date) & " < DT_INISESS )"
	sqlClassi = sqlClassi &  " ORDER BY CP.DT_INISESS,CP.ID_CLASSE"
	
	set rsClassiDisp = Server.CreateObject("ADODB.Recordset")
'PL-SQL * T-SQL  
SQLCLASSI = TransformPLSQLToTSQL (SQLCLASSI) 
	rsClassiDisp.Open sqlClassi, CC, 3%>
	<br>
	<table border="0" cellspacing="2" cellpadding="1" width="500">
		<tr>
			<td class="tbltext1" align="left" width="125">
				<b>Bando :</b>
			</td>	
			<td class="textBlack" align="left">
				<b><%=DecBando(nBando)%></b>
			</td>
		</tr>
		<tr>
			<td class="tbltext1" align="left" width="125">
				<b>Sessione Formativa :</b>
			</td>	
			<td class="textBlack" align="left" class="textBlack">
				<b><%=DecFaseBando(nBando,sCodSessione)%></b>
			</td>
		</tr>
	</table>
	<br>
	<%nInd = 0
	If not rsClassiDisp.EOF then%>			
		<table width="500" cellspacing="2" cellpadding="1" border="0">
			<form name="frmPagina" method="post" action="CLA_SelAule.asp">
			    <input type="hidden" name="pagina" value>
			    <input type="hidden" name="cmbBando" value="<%=nBando%>">
			    <input type="hidden" name="cmbSessioni" value="<%=sCodSessione%>">
			</form>		
			<tr class="sfondoComm"> 
				<td width="450" align="left"><b>Classe</b></td>
<!--			<td width="100" align="left"><b>Progressivo fase</b></td>-->
				<td width="50" align="left"><b>Allievi</b></td>
			</tr>
	<%	
		rsClassiDisp.PageSize  = nTamPagina
		rsClassiDisp.CacheSize = nTamPagina	

		nTotPagina = rsClassiDisp.PageCount		
		If nActPagina < 1 Then
			nActPagina = 1
		End If
		If nActPagina > nTotPagina Then
			nActPagina = nTotPagina
		End If

		rsClassiDisp.AbsolutePage = nActPagina
		nTotRecord=0
	
		Do while not rsClassiDisp.EOF And nTotRecord < nTamPagina	
			dIniSess = ConvDateToString(rsClassiDisp("DT_INISESS"))
			dFInSess = ConvDateToString(rsClassiDisp("DT_FINSESS"))
			nIDClasse	= CLng(rsClassiDisp("ID_CLASSE"))
			sDescClasse = rsClassiDisp("DESC_AULA")
			nPrgFase	= CInt(rsClassiDisp("PRG_FASE"))
			sSQLConta = "SELECT COUNT(ID_PERSONA) AS ContaPers " &_
				"FROM COMPONENTI_CLASSE " &_
				"WHERE ID_CLASSE=" & nIDClasse &_
				"AND DT_RUOLODAL <= "& ConvDateToDbs(dFInSess) &_
				"AND (DT_RUOLOAL IS NULL OR SYSDATE <= DT_RUOLOAL)"  &_
				"AND COD_RUOLO = 'DI'"
				
'				"AND (DT_RUOLOAL IS NULL OR DT_RUOLOAL >= " & ConvDateToDbs(dIniSess)& ") "  &_

			'response.write sSQLConta
'PL-SQL * T-SQL  
SSQLCONTA = TransformPLSQLToTSQL (SSQLCONTA) 
			set rsCount = CC.Execute(sSQLConta)
			nCount = CInt(rsCount("ContaPers"))
			rsCount.close
			set rsCount = nothing%>
			<form name="frmAula<%=nInd%>" method="Post" action="CLA_VisAule.asp">
				<input type="hidden" name="PrgFase" value="<%=nPrgFase%>">
				<input type="hidden" name="Aula" value="<%=sDescClasse%>">
				<input type="hidden" name="Classe" value="<%=nIDClasse%>">
				<input type="hidden" name="DTI" value="<%=dIniSess%>">
				<input type="hidden" name="DTF" value="<%=dFInSess%>">
				<input type="hidden" name="sessione" value="<%=sCodSessione%>">
				<input type="hidden" name="Bando" value="<%=nBando%>">
			</form>
			<tr class="tblsfondo">
				<td width="450" align="left">
					<a title="Seleziona classe <%=sDescClasse%> ( <%=dIniSess%> - <%=dFInSess%> )" class="tblAgg" href="Javascript:frmAula<%=nInd%>.submit()" onmouseover="window.status =' '; return true"><%=sDescClasse%> ( <%=dIniSess%> - <%=dFInSess%> )</a>
				</td>
				<!--td width="100" class="tblDett"><%'=nPrgFase%></td-->
			<%if nCount > 0 then%>	
				<td width="50" class="tblDett"><a title="Elenco dei componenti della classe" class="tblAgg" onmouseover="window.status =' '; return true" href="Javascript:ComponentiClasse('<%=nBando%>','<%=sCodSessione%>','<%=nIDClasse%>','<%=Replace(sDescClasse & " (" & dIniSess & " - " & dFInSess & ")","'","$")%>')"><%=nCount%></a>
				</td>
			<%else%>
				<td width="50" class="tblDett">0</td>
			<%end if%>
			</tr>
	<%		rsClassiDisp.MoveNext()
			nInd = nInd + 1 
			nTotRecord=nTotRecord + 1 
		Loop
		rsClassiDisp.CLOSE
		set rsClassiDisp = nothing%>
		</table><%
		Response.Write "<TABLE border=0 width=470 align=center>"
		Response.Write "<tr>"
		if nActPagina > 1 then
			Response.Write "<td align=right width=480>"
			Response.Write "<A title=""Precedente"" HREF=""javascript:Carica_Pagina('" & nActPagina-1 & "')"">"
			Response.Write "<img border=0 src=" & Session("Progetto") & "/images/precedente.gif></A></td>"
		end if
		if nActPagina < nTotPagina then
			Response.Write "<td align=right>"
			Response.Write "<A title=""Successivo"" HREF=""javascript:Carica_Pagina('" & nActPagina+1 & "')"">"		
			Response.Write "<img border=0 src=" & Session("Progetto") & "/images/successivo.gif></A></td>"
		end if
		Response.Write "</tr></TABLE>"%>
		<br>
	<%else%>
		<table border="0" cellpadding="0" cellspacing="0" width="500">
			<tr>
				<td align="center" class="tbltext3">
					Nessuna classe attiva per il bando<br>e la sessione formativa indicata
				</td>
			</tr>
		</table>
		<br>
	<%End if%>		
	<table width="500" cellspacing="0" cellpadding="0" border="0">
		<tr>
			<td align="center"><a onmouseover="window.status =' '; return true" href="CLA_SelClassi.asp" title="Indietro"><img border="0" src="<%=Session("Progetto")%>/images/indietro.gif"></a>
			</td>
		</tr>
	</table>
<%End Sub%>
<!--------------------------------------------------------------------------------------------------------------------------------------------------------->
<!--	FINE BLOCCO ASP		-->
<!--		MAIN			-->
<!--#include virtual = "/Strutt_Testa2.asp"-->
<!--#include virtual = "/include/openconn.asp"-->
<!--#include virtual = "/include/SelAreaTerrBandi.asp"-->	
<!--#include virtual = "/include/ControlDateVB.asp"-->
<!--#include file = "CLA_Inizio.asp"-->
<%
dim nBando
dim sCodSessione

if ValidateService(Session("IdUtente"),"CLA_SelClassi",CC) <> "true" then 
	response.redirect "/util/error_login.asp"
end if

nBando			= CLng(Request.Form("cmbBando"))
sCodSessione	= Request.Form("cmbSessioni") 
Inizio()
InfoProc()
ImpostaPag()%>	
<!--#include virtual = "/include/closeconn.asp"-->
<!--#include virtual = "/Strutt_Coda2.asp"-->
<!--	FINE BLOCCO MAIN	-->
