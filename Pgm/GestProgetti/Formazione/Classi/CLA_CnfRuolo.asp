<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--include virtual = "/Strutt_Testa2.asp"-->
<!--#include virtual = "/include/openconn.asp"-->
<!--#include virtual = "/include/SysFunction.asp"-->
<!--#include virtual = "/include/ControlDateVB.asp"-->
<!--#include virtual = "/util/Portallib.asp"-->
<!--#include virtual = "/util/dbUtil.asp"-->
<!--#include virtual = "/include/RuoloFunzionale.asp" -->
<html>
<head>
<title>Inserimento ruolo</title>
<link REL="STYLESHEET" TYPE="text/css" HREF="<%=session("progetto")%>/fogliostile.css">
</Head>

<%
if ValidateService(session("idutente"),"CLA_VisClassi",CC) <> "true" then 
	response.redirect "/util/error_login.asp"
end if

dim nIdUtente, nIdClasse, sCodSessione, dOggi

dOggi = Date()

nIdUtente	= CLng(Request.Form("cmbUtente"))
nIdClasse	= CLng(Request.Form("idClasse"))
sCodSessione = Request.Form("sessione")
sCodRuolo	= Request.Form("cmbRuolo")
dDTSessDal	= ConvStringToDate(Request.Form("DtDal"))
dDTSessAl	= ConvStringToDate(Request.Form("DtAl"))

CC.BeginTrans

sSQL = "SELECT IDUTENTE,DT_RUOLODAL,DT_RUOLOAL FROM CLASSE_RUOLO " &_
	" WHERE ID_CLASSE = " & nIdClasse &_
	" AND COD_SESSIONE = '" & sCodSessione & "' AND COD_RUOFU ='" &_
	 sCodRuolo & "' ORDER BY DT_RUOLOAL DESC"
'	" AND " & ConvDateToDbs(dOggi) & " BETWEEN CR.DT_RUOLODAL AND CR.DT_RUOLOAL " &_
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
Set rsCRuolo = CC.Execute(sSQL)
if Not rsCRuolo.eof Then ' Esite gi� una persona.
	nIdDocente	= clng(rsCRuolo("IDUTENTE"))
	dDTRuoloDal = rsCRuolo("DT_RUOLODAL")
	dDTRuoloAl	= rsCRuolo("DT_RUOLOAL")
	if nIdUtente <> nIdDocente then ' Se si ha selezionato lo stesso non modifico nulla
		if CDate(dOggi) > CDate(dDTSessDal) then
			' Chiudo il ruolo alla data di 
			if CDate(dOggi) <> CDate(dDTRuoloDal) then
			
				if CDate(dOggi) < CDate(dDTRuoloDal) then
					sSQL = "DELETE CLASSE_RUOLO " &_
						" WHERE ID_CLASSE = " & nIdClasse &_
						" AND COD_SESSIONE = '" & sCodSessione & "'" &_
						" AND IDUTENTE = " & CLng(nIdDocente) &_
						" AND DT_RUOLODAL = " & ConvDateToDbs(dDTRuoloDal) &_
						" AND COD_RUOFU = '" & sCodRuolo & "'"
				else
					sSQL = "UPDATE CLASSE_RUOLO SET DT_RUOLOAL = " & ConvDateToDbs(dOggi-1) &_
						" WHERE ID_CLASSE = " & nIdClasse &_
						" AND COD_SESSIONE = '" & sCodSessione & "'" &_
						" AND IDUTENTE = " & CLng(nIdDocente) &_
						" AND DT_RUOLODAL = " & ConvDateToDbs(dDTRuoloDal) &_
						" AND COD_RUOFU = '" & sCodRuolo & "'"
				
				end if
				sErrore = EseguiNoC(sSQL,CC)

				if sErrore = "0" then
					sSQL = "INSERT INTO CLASSE_RUOLO " &_
						" (ID_CLASSE,COD_SESSIONE,IDUTENTE,COD_RUOFU,DT_RUOLODAL,DT_RUOLOAL,DT_TMST) " &_
						" VALUES (" &_
						nIdClasse & ",'" & sCodSessione & "'," &_
						nIdUtente & ",'" &_
						sCodRuolo & "'," &_
						ConvDateToDbs(dOggi) & "," &_
						ConvDateToDbs(dDTSessAl) & ",SYSDATE)"
					sErrore = EseguiNoC(sSQL,CC)
				end if

			else
				sSQL = "UPDATE CLASSE_RUOLO SET IDUTENTE = " & CLng(nIdUtente) &_
					" WHERE ID_CLASSE = " & nIdClasse &_
					" AND COD_SESSIONE = '" & sCodSessione & "'" &_
					" AND IDUTENTE = " & CLng(nIdDocente) &_
					" AND DT_RUOLODAL = " & ConvDateToDbs(dDTRuoloDal) &_
					" AND COD_RUOFU = '" & sCodRuolo & "'"				
				sErrore = EseguiNoC(sSQL,CC)
			
			end if
		end if
		if CDate(dOggi) <= CDate(dDTSessDal) then
			' Modifico solo idutente del vecchio docente
			' con quello scelto.
			sSQL = "UPDATE CLASSE_RUOLO SET IDUTENTE = " & CLng(nIdUtente) &_
				" WHERE ID_CLASSE = " & nIdClasse &_
				" AND COD_SESSIONE = '" & sCodSessione & "'" &_
				" AND IDUTENTE = " & CLng(nIdDocente) &_
				" AND DT_RUOLODAL = " & ConvDateToDbs(dDTRuoloDal) &_
				" AND COD_RUOFU = '" & sCodRuolo & "'"
			sErrore = EseguiNoC(sSQL,CC)
		end if
	else
		sErrore = "0"
	end if
else
	if CDate(dOggi) > CDate(dDTSessDal) then
		dDTSessDal = dOggi
	end if 
	sSQL = "INSERT INTO CLASSE_RUOLO " &_
		" (ID_CLASSE,COD_SESSIONE,IDUTENTE,COD_RUOFU,DT_RUOLODAL,DT_RUOLOAL,DT_TMST) " &_
		" VALUES (" &_
		nIdClasse & ",'" & sCodSessione & "'," &_
		nIdUtente & ",'" &_
		sCodRuolo & "'," &_
		ConvDateToDbs(dDTSessDal) & "," &_
		ConvDateToDbs(dDTSessAl) & ",SYSDATE)"
	sErrore = EseguiNoC(sSQL,CC)
end if
rsCRuolo.close
set rsCRuolo = nothing 
%>
<table width="500" border="0" cellspacing="2" cellpadding="1">
<tr> 
	<td colspan="2" align="middle" class="tbltext3">
  		<%if sErrore = "0" then
  			CC.CommitTrans
'   			CC.RollBackTrans%>
			<script language="Javascript">
				alert("Operazione correttamente effettuata")
				opener.document.frmReload.submit();
				self.close()
			</script>
		<%else
  			CC.RollBackTrans
			Response.Write "<BR>sErrore = " & sErrore
'			Indietro()
		end if %>
	</td>
</tr>
</table>
</html>
<!--#include virtual ="/include/closeconn.asp"-->
<!--include virtual ="/Strutt_coda2.asp"-->
