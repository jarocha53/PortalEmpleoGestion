<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%'Option Explicit 
   'Response.ExpiresAbsolute = Now() - 1 
   'Response.AddHeader "pragma","no-cache"
   'Response.AddHeader "cache-control","private"
   'Response.CacheControl = "no-cache"
%>
<!-- #include virtual="/strutt_testa2.asp"-->
<!--#include virtual ="/include/openconn.asp"-->
<!--#include virtual="/util/portallib.asp"-->
<!--#include virtual = "/util/dbutil.asp"-->
<!--#include virtual = "/include/SelAreaTerrBandi.asp"-->
<%
if ValidateService(session("idutente"),"CLA_VISEDIZIONE", CC) <> "true" then 
	response.redirect "/util/error_login.asp"
end if
%>
<!-- ************** Javascript Inizio ************ -->
<script LANGUAGE="Javascript">
<!--#include Virtual = "/Include/help.inc"-->
function Show_Help(W2Show)
{
	f=W2Show;
	w=(screen.width-(screen.width/2))/2;	
	h=(screen.height-(screen.height/2))/2;
	fin=window.open(f,"pippo","toolbar=0, location=0,directories=0,status=0,menubar=0,scrollbars=0,resizable=0,copyhistory=0,width=600,height=480,screenX=w,screenY=h");	
}

function ControllaDati(myForm){


	var nPrimoEle 
	var ok

	ok = 0 
	nPrimoEle = 0
 
if (myForm.Modo.value == 1){
	while (document.frmVisEdizione.elements[nPrimoEle].name != "txtTOTALE") {
		if (document.frmVisEdizione.elements[nPrimoEle].checked){
		ok = 1
		}

		nPrimoEle = nPrimoEle + 1
	}


	if (ok == 0) {
		alert ("Devi selezionare almeno un centro di formazione per creare le edizioni")
		return false
	}
	else {
		return true
			
	}
}
else{
    if (document.frmVisEdizione.txtposti.value=="") {
    alert("Il campo numero posti � obbligatorio");
    document.frmVisEdizione.txtposti.focus();
    return false;
    }
    if (isNaN (document.frmVisEdizione.txtposti.value)) {
    alert("Il campo numero posti deve essere numerico");
    document.frmVisEdizione.txtposti.focus();
    return false;
    }
	if (parseInt(document.frmVisEdizione.txtposti.value) > parseInt(document.frmVisEdizione.txtTotGroup.value)){
	alert("Il numero delle persone inserito deve essere minore o uguale a " + document.frmVisEdizione.txtTotGroup.value);
	document.frmVisEdizione.txtposti.focus();
	return false;
	} 
	else if  (parseInt(document.frmVisEdizione.txtposti.value) < 1){
	alert("Il numero delle persone inserito deve essere maggiore di zero");
	document.frmVisEdizione.txtposti.focus();
	return false;
	
	} 
	return true;
	}
	
}   
function Reload()
{
	var nSel

	nSel = frmVisEdizione.elements[0].selectedIndex
	if (frmVisEdizione.elements[0].options[nSel].value != "") 
		location.href = "CLA_VisEdizione.asp?Sel=" + frmVisEdizione.elements[0].options[nSel].value
	else
		alert ("Occorre effettuare una selezione")
}

function Calcola(ele) {
	var i = 0

	while (ele.id != frmVisEdizione.elements[i].id){
		i++
	}
	nTotale = parseInt(frmVisEdizione.txtTOTALE.value)
	if (ele.checked) {
		nValue = parseInt (frmVisEdizione.elements[i+3].value)
		nTotale = nTotale + nValue
	}else {
		nValue = parseInt (frmVisEdizione.elements[i+3].value)
		nTotale = nTotale - nValue
	}
	frmVisEdizione.txtTOTALE.value = nTotale
	return false
}

function GestioneArea()
{
		location.href = "CLA_VisEdizione.asp?Sel=" +
			frmVisEdizione.elements[0].value
}
</script>

<% sub inizio()%>
<br>
	<table cellpadding="0" cellspacing="0" width="500" border="0">
		<tr height="18">
			<td class="sfondomenu" height="18" width="67%">
			<span class="tbltext0"><b>&nbsp;Definizione Classi</b></span></td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
			<td valign="middle" align="right" class="tbltext1" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">(*) campi obbligatori</td>
		</tr>
		<tr>
			<td class="sfondocomm" width="57%" colspan="3">
			Per effettuare la programmazione dei soggetti selezionare il centro di formazione per i percorsi misti oppure per i percorsi a distanza indicare il
			numero complessivo dei soggetti per la singola attivit�. 
			<a href="Javascript:Show_Help('/Pgm/help/GestProgetti/Formazione/Classi/CLA_InsEdizione')" onmouseover="javascript:window.status=' '; return true"><img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
			</td>
		</tr>
		<tr height="2">
			<td colspan="3" class="sfondocomm" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
		</tr>
	</table>
	<br><br>
<%end sub%>	

<% sub RegolaTerr()%>
<table width="500" border="0">
	<tr> 
		<td> 
			<span class="tbltext" align="left">
			<b>
			Bando:&nbsp;			
			<%=sDescBando%>
			</b></span>
		</td> 
	</tr>
	<tr>
		<td> 
			<span class="tbltext" align="left">
			<b>
			Area Geografica:&nbsp;			
			<%=sGruppo%>
		 </b></span></td> 
	</tr>
</table>
<br>
<%
set objTer=Server.CreateObject("FuncRegola_Ter.clsRegola_Ter")
regola = objTer.Regola_Ter(cc,sBando,sGruppo)
set objTer= nothing
if regola = "" then
	sMsg =  "Errore durante la estrazione dei centri interessati"
	sNumPers = 0
else
	Session("regola") = regola
	
	sSQL = " SELECT count(b.id_persona) as Count " & _
	   " FROM PERSONA A, DOMANDA_ISCR B "  & _
	   " WHERE B.ID_PERSONA=A.ID_PERSONA " & _
	   " AND B.ID_BANDO =" & sBando & _
	   " AND ESITO_GRAD IN( 'S', 'X')" & _
	   " AND " & regola &_
	   " AND NOT EXISTS (SELECT Z.ID_PERSONA FROM COMPONENTI_CLASSE Z WHERE Z.ID_PERSONA=B.ID_PERSONA)"
	 'Response.Write ssql  
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	set rsNumEl = CC.Execute(sSQL)
	sNumPers = clng(rsNumEl("Count"))

	if sNumPers = 0 then
		sMess="<br> Non vi sono ulteriori persone da programmare"
		Messaggio(sMess)	
	end if 
	
	rsNumEl.Close
end if
if sNumPers > 0 then%>
	<table width="500" border="0">
	<tr> 
		<td> 
			<span class="tbltext" align="left">
			<b>
			Numero persone da programmare :&nbsp;		 		
				<%=sNumPers%>
			</b></span>
		</td> 
	</tr>
</table><br><br>	
	<input type="hidden" name="txtTotGroup" value="<%=sNumPers%>">
	<input type="hidden" name="cmbBando" value="<%=sBando%>">
	<input type="hidden" name="txtAreaGeo" value="<%=sGruppo%>">
	
<% end if
	end sub%>
	
<% sub CentriFormazione()%>	
	<br>
	<table border="0" cellspacing="2" cellpadding="1" width="500">
		<tr class="sfondocomm">	
			<td align="center" width="23">        
			<td align="center" width="220"><b>C.Formazione</b></td>
			<td align="center" width="100"><b>Comune</b></td>
			<td align="center" width="70"><b>N.Aule</b></td>
			<td align="center" width="70"><b>Posti Totali</b></td>
		</tr>
		
	<%
	'regola = replace(regola,"com_res","COMUNE")
	'regola = replace(regola,"cap_res","CAP")

	regola = replace(regola,"COM_RES","COMUNE")
	regola = replace(regola,"CAP_RES","CAP")
		
	'sSQL = "SELECT COMUNE,NUM_AULE,ID_CFORM,DESC_CFORM FROM CENTRO_FORMAZIONE A" &_
	'	" WHERE PRV='" & mid(sBando,1,2) & "' and " & regola &_
	'	" AND ((SELECT SUM(NUM_POSTI) AS MAX_POSTI " &_
	'	" FROM AULA B WHERE A.ID_CFORM = B.ID_CFORM) > 0 )"

    sSQL = "SELECT CF.ID_CFORM,CF.DESC_CFORM,CF.COMUNE,count(*) as NUM_AULE " &_
			" FROM CENTRO_FORMAZIONE CF, AULA AU" &_
			" WHERE CF.ID_CFORM =AU.ID_CFORM " &_
			" AND AU.ID_BANDO= " & sBando &_
			" AND AU.ID_UORG=" & Session("iduorg") &_
			" AND " & regola &_
			" AND ((SELECT SUM(NUM_POSTI) AS MAX_POSTI " &_
			" FROM AULA B WHERE B.ID_BANDO= " & sBando & " AND CF.ID_CFORM = B.ID_CFORM) > 0 )" &_
			" group by CF.ID_CFORM,CF.DESC_CFORM,CF.COMUNE"
 
  
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
  	set rsCForm = CC.Execute(sSQL)
	n=0
	do while not rsCForm.EOF
		n=n+1
	%>
		<tr class="tblsfondo"> 
	        <td align="center">
	        <input type="checkbox" id="chkSel<%=n%>" name="chkSel<%=n%>" onclick="Calcola(this)">
	        </td>
			<td align="center"><span class="tblDett">
			<%=rsCForm("DESC_CFORM")%>
	        <input type="hidden" name="txtCFORM<%=n%>" value="<%=rsCForm("ID_CFORM")%>">	        
			<span></td>
			<td align="center"><span class="tblDett">
<%
			sSQL = "Select DESCOM from comune where CODCOM = '" & rsCForm("COMUNE") & "'"
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
			set rsComune = CC.Execute(sSQL)
			Response.Write rsComune("DESCOM")
			rsComune.close
%>
			</span></td>
			<td align="center"><span class="tblDett">
			<%=rsCForm("NUM_AULE")%>
	        <input type="hidden" name="txtNUMAULA<%=n%>" value="<%=rsCForm("NUM_AULE")%>">
			</span></td>
			<td align="center"><span class="tblDett">
<%
			sSQL = "SELECT SUM(NUM_POSTI) AS MAX_POSTI" &_
				" FROM AULA WHERE ID_CFORM =" & rsCForm("ID_CFORM") &_
				" AND ID_BANDO=" & sBando   	
			 'Response.Write sSQL
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
			set rsMaxPosti = CC.Execute(sSQL)
			Response.Write rsMaxPosti("MAX_POSTI")
%>
		        <input type="hidden" name="txtMAX_POSTI<%=n%>" value="<%=rsMaxPosti("MAX_POSTI")%>">
				</span></td>
			</tr>
				
		<% 
		rsCForm.MoveNext
	loop%>
		<input type="hidden" name="txtElementi" value="<%=n%>">
		<input type="hidden" name="Modo" value="<%=1%>">
	</table>
	<br>
	<%if n > 0 then%>

		<table width="500" border="0" cellspacing="2" cellpadding="1">
			<tr class="tblsfondo"> 
				<td align="center"><span class="tblDett">
						Numero complessivo soggetti per singola attivit�&nbsp;
				</span></td>
				<td>
					<input type="text" name="txtTOTALE" style="WIDTH: 63px; HEIGHT: 22px" value="0" readonly>
				</td>
			</tr>
		</table>		
		
		<br>
		<table width="500" border="0" cellspacing="2" cellpadding="1">
			<tr>			
				<td align="center">	
					<a href="javascript:history.go(-1)" onmouseover="javascript:window.status=' '; return true">
					<img src="<%=Session("progetto")%>/images/indietro.gif" border="0" name="imgPunto2"></a>
				</td>
				<td align="center">
					<!--input type="submit" value="Preview" name="submit1"-->				
					<input type="image" value="pippo" name="pippo" src="<%=Session("progetto")%>/images/conferma.gif">
				</td> 	
			</tr>
		</table>
		<br>
	<%  else
		sMess=" Nessun Centro di Formazione disponibile per il gruppo selezionato"
		Messaggio(sMess)	
	%>
		<br>
		<table width="500" border="0" cellspacing="2" cellpadding="1">
			<tr>
				<td nowrap align="center"><a href="javascript:history.go(-1)" onmouseover="javascript:window.status=' '; return true">
					<img src="<% =Session("progetto")%>/images/indietro.gif" border="0" name="imgPunto2"></a><td>
				</td>
			</tr>			
		</table>
	<%end if
if sMsg <>"" then%>
	<br>
	<%Messaggio(sMsg)%>
	<br>
	<table width="500" border="0" cellspacing="2" cellpadding="1">
		<tr>
			<td nowrap align="center"><a href="javascript:history.go(-1)" onmouseover="javascript:window.status=' '; return true">
				<img src="<%=Session("progetto")%>/images/indietro.gif" border="0" name="imgPunto2"></a><td>
			</td>
		</tr>	
	</table>
	<br>	
<%end if
end sub%>

<% sub Messaggio(sMess)%>
	<table width="500" border="0" cellspacing="2" cellpadding="1">
		<tr> 
			<td colspan="2" align="middle">
				<span class="tbltext3">					   
				<% =sMess%>				
				</span>
			</td>
		</tr>
	</table><br>
		
<%end sub%>

<%sub BandoSessFad()%>

<table width="500" border="0" cellspacing="2" cellpadding="1">
	<tr class="tblsfondo"> 
		<td align="center"><span class="tblDett">
			Numero posti per ogni classe virtuale
			</span></td>
		<td>
			<input type="text" name="txtposti" style="WIDTH: 63px; HEIGHT: 22px" value="0">			
					<input type="hidden" name="Modo" value="<%=2%>">

		</td>
	</tr>
</table><br>
<table width="500" border="0" cellspacing="2" cellpadding="1">
			<tr>			
				<td align="center">	
					<a href="CLA_VisEdizione.asp" onmouseover="javascript:window.status=' '; return true">
					<img src="<%=Session("progetto")%>/images/indietro.gif" border="0" name="imgPunto2"></a>
				</td>
				<td align="center">
					<!--input type="submit" value="Preview" name="submit1"-->				
					<input type="image" value="pippo" name="pippo" src="<%=Session("progetto")%>/images/conferma.gif">
				</td> 	
			</tr>
</table>
<br>		
<% END SUB%>
<!-----------------MAIN---------------------------------------->

<%dim sSQL
dim sMask
dim rsCenForm
dim sBando
dim sGruppo
dim sMsg
dim objTer
dim regola
dim rsCodFase
dim sNumPers

sBando = Request.Form ("txtBando")
sGruppo = Request.Form ("txtAreaGeo")
sDescBando=DecBando(sBando)%>

<form name="frmVisEdizione" method="post" onsubmit="return ControllaDati(this)" action="CLA_CfnEdizione.asp">
<%
	inizio()
	RegolaTerr()
		sSQL=" SELECT 'F' as IND_FAD FROM DUAL WHERE NOT EXISTS (SELECT COD_TFASE FROM FASE_BANDO WHERE ID_BANDO = " & sBando &_
			" AND COD_TFASE='S')"  
		'Response.Write ssQl
		
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
		set rsCodFase = CC.Execute(sSQL)
		if rsCodFase.eof then
			pippo="P"
		else
			pippo="F"
		end if
		rsCodFase.close
	if sNumPers > 0 then
		if pippo <> "F" then	
			CentriFormazione()		
		else 
			BandoSessFad()
		end if
	end if			
%>
</form>
<!--#include virtual = "/include/closeconn.asp"-->
<!--#include virtual = "strutt_coda2.asp"-->
