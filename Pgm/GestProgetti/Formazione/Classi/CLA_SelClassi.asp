<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include virtual ="/strutt_testa2.asp"-->
<!--	BLOCCO SCRIPT		-->
<!-- JavaScript immediate script -->
<script LANGUAGE="JavaScript">
<!--#include virtual ="/Include/help.inc"-->
<!--
function VisSessione() {
	if (frmBandi.cmbBando.value == "")
		alert("Selezionare un bando per vedere le sessioni")
	else {
		frmSessioni.cmbBando.value = frmBandi.cmbBando.value;
		frmSessioni.submit(); 
	}
}
function Validator() {
	if (document.frmBandi.cmbBando.value == "")	{
		document.frmBandi.cmbBando.focus();
		alert("Il campo Bando � obbligatorio.");
		return false;
	}
	if (document.frmBandi.cmbSessioni.value == "") {
		document.frmBandi.cmbSessioni.focus();
		alert("Il campo Sessione Formativa � obbligatorio.");
		return false;
	}
	return true;	
}
// -->
</script>
<!--	FINE BLOCCO SCRIPT	-->
<!--	BLOCCO ASP			-->
<%function SelSessione(nBando)
	dim sSQLSessioni,rsSessioni

	sSQLSessioni = "SELECT COD_SESSIONE,DESC_FASE FROM FASE_BANDO " &_
		" WHERE ID_BANDO = " & nBando 
	set rsSessioni = Server.CreateObject("ADODB.Recordset")
'PL-SQL * T-SQL  
SSQLSESSIONI = TransformPLSQLToTSQL (SSQLSESSIONI) 
	rsSessioni.Open sSQLSessioni,CC,3
	if not rsSessioni.EOF then
		SelSessione = true%>
		<select name="cmbSessioni" class="textBlack">
			<option value></option>
			<%do while not rsSessioni.EOF 
			%><option value="<%=rsSessioni("COD_SESSIONE")%>"><%=rsSessioni("DESC_FASE")%></option><%
				rsSessioni.MoveNext() 
			loop%>
		</select>
	<%else
		SelSessione = false
	end if
	rsSessioni.Close()
	set rsSessioni = nothing
end function

sub InfoProc()%>
	<table cellpadding="0" cellspacing="0" width="500" border="0">
		<tr height="18">
			<td class="sfondomenu" height="18" width="50%">
				<span class="tbltext0"><b>&nbsp;GESTIONE COMPONENTI CLASSI</b>
				</span>
			</td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
			<td valign="middle" align="right" width="40%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1"></td>
		</tr>
		<tr width="371" class="SFONDOCOMM">
			<td colspan="3">
			Selezionare un bando e di seguito scegliere una sessione formativa
			per ritrovare le classi verso cui spostare i candidati.
				<a onmouseover="window.status =' '; return true" href="Javascript:Show_Help('/Pgm/Help/gestprogetti/Formazione/Classi/CLA_SelClassi')">
				<img src="<%=Session("Progetto")%>/images/Help.gif" border="0" align="right" alt="Help">

			</td>
		</tr>
		<tr height="2">
			<td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>
<%end sub%>

<!--------------------------------------------------------------------------------------------------------------------------------------------------------->
<%  
sub Bandi ()
	dim sComboBandi%>		
	<table border="0" cellspacing="2" cellpadding="2" width="500">
		<tr>
			<td class="tbltext1" align="left" width="150">
				<b>Bandi disponibili :</b>
			</td>
			<td align="left" class="tbltext">
			<%sComboBandi = Session("iduorg") & "|" & nBando & "|cmbBando|onchange=Javascript:VisSessione()"
			CreateBandi(sComboBandi)%>		
			</td>
		</tr>
	</table>
<%End sub%>	
<!--------------------------------------------------------------------------------------------------------------------------------------------------------->
<%
Sub Sessioni()
	Dim sCombo,sSQLSessioni,rsSessioni,bSessione
	bSessione = false%>
	<table border="0" cellspacing="2" cellpadding="2" width="500"> 
		<tr>
			<td class="tbltext1" align="left" width="150">
				<b>Sessione Formativa :</b>
			</td>
			<td align="left" class="tbltext3">
			<%bSessione = SelSessione(nBando)
			if not bSessione then
				Response.Write "Nessun bando associato"
			end if%>
			</td>
		</tr>
		<tr align="center">
			<td colspan="2">&nbsp;</td>
		</tr>
		<%if bSessione then%>
			<tr align="center">
				<td colspan="2">
					<input title="Conferma" type="image" src="<%=Session("Progetto")%>/images/Conferma.gif" value="Conferma">
				</td>
			</tr>
		<%end if%>
	</table>
<%End Sub%>	
<!--------------------------------------------------------------------------------------------------------------------------------------------------------->
<!--	FINE BLOCCO ASP		-->
<!--		MAIN			-->
<!--#include virtual = "/include/SelAreaTerrBandi.asp"-->	
<!--#include virtual = "/include/openconn.asp"-->
<!--#include file	 = "CLA_Inizio.asp"-->
<%
dim nBando

if ValidateService(session("idutente"),"CLA_SelClassi",CC) <> "true" then 
	response.redirect "/util/error_login.asp"
end if
	
Inizio()
InfoProc()
if  Request.Form("cmbBando") <> "" then
	nBando = CLng(Request.Form("cmbBando"))
end if
	
%>
<form method="post" action="CLA_SelAule.asp" onsubmit="return Validator()" name="frmBandi">
<%

Bandi()
if nBando <> "" then
	Sessioni()
end if
%>
</form>
<form name="frmSessioni" method="Post" action="CLA_SelClassi.asp">
<input type="hidden" name="cmbBando">
</form>
<!--#include virtual = "/include/closeconn.asp"-->
<!--#include virtual = "/strutt_coda2.asp"-->
<!--	FINE BLOCCO MAIN	-->
