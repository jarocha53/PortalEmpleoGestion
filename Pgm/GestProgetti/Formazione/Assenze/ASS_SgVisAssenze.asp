<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include virtual = "/strutt_testa2.asp"-->
<!--#include virtual = "/include/openconn.asp"-->
<!--#include virtual = "/util/dbutil.asp"-->
<!--#include virtual = "/include/DecCod.asp"-->
<!--#include virtual = "/include/ControlDateVB.asp"-->
<!--#include virtual = "/include/SelAreaTerrBandi.asp"-->
<!--#include virtual = "/util/portallib.asp"-->
<!--#include virtual = "/include/Report_Assenze.asp"-->

<script LANGUAGE="JavaScript">
<!--#include virtual = "/Include/Help.inc"-->

	function Show_Help(W2Show)
{
		f=W2Show;
		w=(screen.width-(screen.width/2))/2;	
		h=(screen.height-(screen.height/2))/2;
		fin=window.open(f,"pippo","toolbar=0, location=0,directories=0,status=0,menubar=0,scrollbars=0,resizable=0,copyhistory=0,width=600,height=480,screenX=w,screenY=h");	
}


	function Ritorno(){
		history.go(-1);
	}

	function Scarica(report){
		//f="temp\\"+ report;
		fin=window.open(report,"","toolbar=0;location=0,directories=0,status=0,menubar=1,scrollbars=1,resizable=1,copyhistory=0,width=450,height=340,screenX=200,screenY200");
	}

</script>	
	
<%
sub inizio()%>
<br>
	<table cellpadding="0" cellspacing="0" width="500" border="0">
		<tr height="18">
			<td class="sfondomenu" height="18" width="67%">
			<span class="tbltext0"><b>&nbsp;Riepilogo Assenze</b></span></td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
			<td valign="middle" align="right" class="tbltext1" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">(*) campi obbligatori</td>
		</tr>
		<tr>
			<td class="sfondocomm" width="57%" colspan="3">
			Per poter avere il resoconto delle assenze relative alla classe selezionata,cliccare sul link 
			<b>Apri il report</b>
			<a href="Javascript:Show_Help('/Pgm/help/GestProgetti/Formazione/Classi/CLA_InsEdizione')" onmouseover="javascript:window.status=' '; return true"><img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
			</td>
		</tr>
		<tr height="2">
			<td colspan="3" class="sfondocomm" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
		</tr>
	</table>
	<br>
	
<%end sub

'<!--------------------------------------------------------------------------------------------------------------------------------------------------------->
 Sub Fine()%>
		<!----------------Torna alla Pagina Iniziale ------------->
		 <table cellpadding="0" cellspacing="0" width="500" border="0">	
		    	<tr>
					<td nowrap align="center"><a href="javascript:Ritorno()" onmouseover="javascript:window.status=' '; return true"><img src="/ISI/images/indietro.gif" border="0" name="imgPunto2"></a><td>
			    </tr>
		 </table>
	   	 <br>
		<!--#include virtual ="/include/closeconn.asp"-->
		<!--#include virtual = "/strutt_coda2.asp"-->
<%End Sub 	
'-----------------------------------------------------------------------------------------------------------------
sub testa()

	'sSQL="SELECT DESC_CFORM FROM CENTRO_FORMAZIONE WHERE ID_CFORM=" & sCeForm 
	'Response.Write sSQL
	'	set rsDesCen = server.CreateObject("ADODB.Recordset")
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	'	rsDesCen.Open sSql, CC, 3
	sDescbando = DecBando(sBando)
	sDescSessione = DecFaseBando(sBando,sSess)
%>
	<br>
		<table border="0" cellspacing="2" cellpadding="1" width="500">
			<tr align="center">
				<td class="tbltext" align="left" width="150">
					<b>Bando</b>
				</td>	
				<td align="left" class="tbltext" colspan="2">
					<strong><%=sDescBando%></strong>
				</td>
			</tr>
			<tr align="center">
				<td class="tbltext" align="left" width="150">
					<b>Sessione Formativa</b>
				</td>	
				<td align="left" class="tbltext" colspan="2">
					<strong><%=sDescSessione%></strong>
				</td>
			</tr>
			<tr align="center">
				<td class="tbltext" align="left" width="150">
					<b>Provincia</b>
				</td>	
				<td align="left" class="tbltext" colspan="2">
					<strong><%=sProv%></strong>
				</td>
			</tr>
			<tr align="center">
				<td class="tbltext" align="left" width="150">
					<b>Centro di Formazione</b>
				</td>	
				<td align="left" class="tbltext" colspan="2">
					<strong><%=sCeForm %></strong>
				</td>
			</tr>
		</table>
		<br><br>		
<%
'	rsDesCen.close
end sub
'---------------------------------------------------------------------------------------------------------------------
sub ImpostaPag()

	If esito_Report = "KO" Then
		Response.Write "<table width=500 cellpadding=1 celspacing=2>"
		Response.Write "<tr><td align=center><span class=tbltext3>"
		Response.Write "Non ci sono discenti presenti in classe<span>"
		Response.Write "</span></td></tr></table><br><br>"
	else
	%>
	    <table width="500" border="0">
			<tr>
				<td align="center">
					<span class="tbltext3">
					<b>Il report � stato creato correttamente</b>
					</span>
				<td>
			</tr>	
			<tr>
				<td align="center">
					<a href="javascript:Scarica('<%=esito_Report%>')"><span class="textred"><b>Apri il report</b></span></a>&nbsp; 
				</td>
			</tr>
		</table>
	<%
	end if
end sub
'-------------------MAIN----------------------------------------------------------------------------------------------
dim sBando, sSess, sProv, sCeForm, esito_Report, sDescbando, sDescSessione
	
if ValidateService(session("idutente"),"ASS_SGRICASSENZE",CC) <> "true" then 
	response.redirect "/util/error_login.asp"
end if
	
sSess = Request.Form ("Sessione")
	
sProv = Request.Form ("Prov")		
sCeForm =Request.Form ("CentForm")	
sBando = clng(Request.Form ("Bando"))
sAula = Request.Form ("aula")

nIdClasse = clng(Request.Form ("IdClasse"))		
sDtinizio = Request.Form ("DTI")
sDtFine = Request.Form ("DTF")

inizio()
testa()

esito_Report = creaRepAssenze(sSess, sAula, nIdClasse, sDtinizio, sDtfine, sDescbando, sDescSessione)

ImpostaPag()
fine()
%>
