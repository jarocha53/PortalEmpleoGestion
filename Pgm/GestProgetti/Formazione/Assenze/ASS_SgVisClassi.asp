<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include virtual = "/strutt_testa2.asp"-->
<!--#include virtual ="/util/portallib.asp"-->
<!--#include virtual = "include/openconn.asp"-->
<!--#include virtual = "include/ckProfile.asp"-->
<!--#include virtual = "include/DecCod.asp"-->
<!--#include virtual = "include/SysFunction.asp"-->
<!--#include virtual = "include/ControlDateVB.asp"-->		
<!--#include virtual = "Util/DBUtil.asp"-->
<!--#include virtual = "/include/SelAreaTerrBandi.asp"-->

<script LANGUAGE="JavaScript">
<!--#include Virtual = "/Include/help.inc"-->

function Conferma(idAula){

	document.frmPers.aula.value = idAula
	document.frmPers.action='ASS_SgVisAssenze.asp';
	document.frmPers.submit();	

}

function VaiInizio(){
    history.go(-1)
}	
</script>

<% sub inizio()%>
<br>
	<table cellpadding="0" cellspacing="0" width="500" border="0">
		<tr height="18">
			<td class="sfondomenu" height="18" width="67%">
			<span class="tbltext0"><b>&nbsp;Riepilogo Assenze</b></span></td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
			<td valign="middle" align="right" class="tbltext1" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">(*) campi obbligatori</td>
		</tr>
		<tr>
			<td class="sfondocomm" width="57%" colspan="3">
			La pagina visualizza l'elenco delle classi corrispondenti ai criteri selezionati.
			Per visualizzare il link del report cliccare sulla classe.			
			<a href="Javascript:Show_Help('/Pgm/help/GestProgetti/Formazione/assenze/ASS_SgVisClassi')" onmouseover="javascript:window.status=' '; return true"><img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
			</td>
		</tr>
		<tr height="2">
			<td colspan="3" class="sfondocomm" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
		</tr>
	</table>
	<br>	
<%end sub%>
<!--------------------------------------------------------------------------------------------------------------------------------------------------------->
<% sub Fine()%>

  	<!--#include virtual = "/include/closeconn.asp"-->
	<!--#include virtual = "strutt_coda2.asp"-->
<%end sub
'<!--------------------------------------------------------------------------------------------------------------------------------------------------------->
sub testa()

	sSQL="SELECT DESC_CFORM,ID_CFORM FROM CENTRO_FORMAZIONE WHERE ID_CFORM=" & sCeForm
	'Response.Write sSQL
		set rsDesCen = server.CreateObject("ADODB.Recordset")
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
		rsDesCen.Open sSql, CC, 3
%>
	<br>
		<table border="0" cellspacing="2" cellpadding="1" width="500">
			<tr align="center">
				<td class="tbltext" align="left" width="150">
					<b>Bando</b>
				</td>	
				<td align="left" class="tbltext" colspan="2">
					<strong><%=DecBando(sBando)%></strong>
					
				</td>
			</tr>
			<tr align="center">
				<td class="tbltext" align="left" width="150">
					<b>Sessione Formativa</b>
				</td>	
				<td align="left" class="tbltext" colspan="2">
					<strong><%=DecFaseBando(sBando,sSess)%></strong>
				</td>
			</tr>
			<tr align="center">
				<td class="tbltext" align="left" width="150">
					<b>Provincia</b>
				</td>	
				<td align="left" class="tbltext" colspan="2">
					<strong><%=sProv%></strong>					
				</td>
			</tr>
			<tr align="center">
				<td class="tbltext" align="left" width="150">
					<b>Centro di Formazione</b>
				</td>	
				<td align="left" class="tbltext" colspan="2">
					<strong><%=rsDesCen("DESC_CFORM")%></strong>
				</td>
			</tr>
		</table>	
<%	descCentro =rsDesCen("DESC_CFORM")
    IdCform= rsDesCen("ID_CFORM")
	rsDesCen.close
end sub
'-----------------------------------------------------------------------------------------------------------------------
	Sub ImpostaPag()
		
		
		'si estrapolano i centri di formazione e le rispettive classi relativi alla provincia selezionata
		'aventi una sessione formativa con formazione in aula.
		sSQL= "SELECT A.DESC_AULA,A.ID_AULA,CP.ID_CLASSE,CP.DT_INISESS,CP.DT_FINSESS" & _
				" FROM AULA A,CLASSE_PERCORSO CP" & _
				" WHERE CP.ID_AULA=A.ID_AULA" &_
				" AND A.ID_CFORM=" & sCeForm & _
				" AND A.ID_BANDO=" & sBando &_
				" AND CP.COD_SESSIONE='" & sSess & "'" &_
				" AND (SYSDATE >= CP.DT_FINSESS or SYSDATE >= CP.DT_INISESS)" &_
				" ORDER BY CP.DT_INISESS,CP.DT_FINSESS"
'Response.Write ssql
		set RR=server.CreateObject("ADODB.Recordset")
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
		RR.open ssql, CC, 3		
	
	if not RR.EOF  then %>				
			<br>
			<table width="500" cellspacing="2" cellpadding="1" border="0">
				<tr class="sfondocomm"> 
					<td align="left" height="23" width="200">
						<b>Classi</b>
					</td>
					<td align="center">
						<b>Dal</b>
					</td>
					<td align="center">
						<b>Al</b>
					</td>
				</tr>
<%
				dim nConta
				nConta = 0
				Do while not RR.eof	
					
					dti = ConvDateToString(RR.Fields("DT_INISESS"))
				
				IF RR.Fields("DT_FINSESS") <> "" then
					
					dtf = ConvDateToString(RR.Fields("DT_FINSESS"))
				else
					dtf = "Data da definire"
					
				end if
				
%>			<tr>
			<form name="frmPers<%=nConta%>" method="post" action="ASS_SgVisAssenze.asp">
					<td class="tblDett" width="200" align="center">
						<input type="hidden" name="idCLasse" value="<%=RR.Fields("ID_CLASSE")%>">
						<input type="hidden" name="DTI" value="<%=dti%>">
						<input type="hidden" name="DTF" value="<%=dtf%>">
						<input type="hidden" name="aula" value="<%=RR.Fields("ID_AULA")%>">
						<input type="hidden" value="<%=sBando%>" name="Bando">
						<input type="hidden" value="<%=sProv%>" name="Prov">
						<input type="hidden" value="<%=sSess%>" name="Sessione">
						<input type="hidden" value="<%=descCentro%>" name="CentForm">					
						<a href="Javascript:frmPers<%=nConta%>.submit()" class="tblAgg" onmouseover="window.status =' '; return true"><%=RR.Fields("DESC_AULA")%></a>
					</td>
				</form>	
					<td align="center">
						<span class="tblDett">
						<%=dti%>						
						</span>	
					</td>
					<td align="center"><span class="tblDett">
						<%=dtf%>						
						</span>	
					</td>
				</tr>
				<%RR.MoveNext
					nConta= nConta +1
				  Loop%>
				<%RR.CLOSE%>				
			</table>
			<br><br>
<%		else%>
			<br><br>
			<table width="500" cellspacing="0" cellpadding="0" border="0">
				<tr>
					<td>
						<b class="tbltext3"><span class="size">
						Sessione formativa non attiva per la provincia - data selezionata
						</b></span>
					</td>
				</tr>
			</table>
			<br>
<%		End if
%>	<table width="500">
		<tr align="center">
				<td nowrap colspan="2">
					<a href="javascript:VaiInizio()" onmouseover="javascript:window.status=' '; return true"><img src="<%=session("Progetto")%>/images/indietro.gif" border="0" name="imgPunto2"></a>
				</td>
			</tr>
		</table>	
<%	End Sub%>
<!----------------MAIN----------------------------------------------->
<%
	if ValidateService(session("idutente"),"ASS_SGRICASSENZE",CC) <> "true" then 
		response.redirect "/util/error_login.asp"
	end if
	
	Dim strConn
	dim sBando,sSess,sProv,sCeForm,descCentro
	DIM IdCform
	
	sSess= Request.Form ("txtSessForm")		
	sProv= Request.Form ("cmbProv")		
	sCeForm= clng(Request.Form ("cmbCForm"))	
	sBando= clng(Request.Form ("cmbBando"))
	
	Inizio()	
	testa()
	ImpostaPag()
	Fine()	
	
%>
