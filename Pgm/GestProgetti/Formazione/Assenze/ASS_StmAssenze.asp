<!--#include virtual ="/util/portallib.asp"-->

<html>
<!--	BLOCCO HEAD			-->
<head>
	<META NAME="GENERATOR" Content="Microsoft Visual Studio 6.0">
	<title>Registro firme presenza</title>
	<LINK REL=STYLESHEET TYPE="text/css" HREF="../../fogliostile.css">
	<!--	BLOCCO SCRIPT		-->
	<script LANGUAGE="JavaScript">
		<!--
			function Show_Help(W2Show)
			{
			f=W2Show;
			w=(screen.width-(screen.width/2))/2;	
			h=(screen.height-(screen.height/2))/2;
			fin=window.open(f,"pippo","toolbar=0, location=0,directories=0,status=0,menubar=0,scrollbars=0,resizable=0,copyhistory=0,width=600,height=480,screenX=w,screenY=h");	
			}
			function fStampa()
				{
					if (document.form1.IOre.value == "" )
						{
							alert(" Indicare l'orario di inizio della lezione ")
							document.form1.IOre.focus();
							return(false)
						}
					
					if (document.form1.IMinuti.value >= 60 )
						{
							alert(" Controllare i minuti immessi ")
							document.form1.IOre.focus();
							return(false)
						}
					else
						{ 
							window.print();
							history.back();
							return(true)
						}
				}
			function ImpostaOra(k)
				{	//alert(k)
					// NUMERICITA' 
					var dim=document.form1.IOre.size;
					var anyString = document.form1.IOre.value;
					var v
					for ( var i=0; i<=anyString.length-1; i++ )
						{
							if ( (anyString.charAt(i) >= "0") && (anyString.charAt(i) <= "9") )
							{
								v = true
							}
							else
							{
								alert("Attenzione: valore immesso non numerico!");
								document.form1.IOre.focus();
								v = false
							}
						}
					if (v)
						{	
							for (i=0; i< k; i++)
								{
									document.form1.txtOre[i].value = document.form1.IOre.value
								}
						}
				}
				
				function ImpostaM(k)
				{
					// NUMERICITA' 
					var dim=document.form1.IMinuti.size;
					var anyString = document.form1.IMinuti.value;
					var v
					for ( var i=0; i<=anyString.length-1; i++ )
						{
							if ( (anyString.charAt(i) >= "0") && (anyString.charAt(i) <= "9") )
							{
								v = true
							}
							else
							{
								alert("Attenzione: valore immesso non numerico!");
								document.form1.IMinuti.focus();
								v = false	
							}
						}
					if (v)
						{
							for (i=0; i< k; i++)
							{
								document.form1.txtMinuti[i].value = document.form1.IMinuti.value
							}
						}
				}
		//-->
	</script>
	
	
	<!--	FINE BLOCCO SCRIPT	-->
</HEAD>
<!--	FINE BLOCCO HEAD	-->

<!--	BLOCCO ASP			-->
<!--------------------------------------------------------------------------------------------------------------------------------------------------------->
<%
	Sub InizioS()
%>	
	<body background="../../images/sfondo1.gif"  onafterprint="javascript:document.forms[0].ok.style.visibility='visible';
	document.forms[0].indietro.style.visibility='visible'"
		onbeforeprint="javascript:document.forms[0].ok.style.visibility='hidden';
		document.forms[0].indietro.style.visibility='hidden'">
	<basefont class="tbltext"> 
	<table border="0" cellpadding="0" cellspacing="0" width="570">
		<tr>
		    <!--td width="33%"><IMG height=70 src="../../images/ministero.gif" border=0></td-->
		    <td width="33%" class=size10><b> Amministrazione <br>provinciale di <br><%=DecCodVal("PROV", "0", "", sPrv, 1)%></b></td>
		    <td align="middle" width="33%"><IMG src="../../images/inn.gif" border=0></td>
		    <td align="right" width="34%"><IMG src="../../images/italia.gif" border=0></td>
		</tr>
	</table>
	<table width=630 border=0 cellspacing=2 cellpadding=1>
		<tr> 
			<td colspan=2 align=middle background=../../images/sfmnsmall.jpg>
			<b class="titolo">Registro firme presenza&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b>
				<a href="javascript:Show_Help('../../help/registro di classe/Ass_StmAssenze.htm')">
				<IMG src="../../images/imghelp.gif" border=0>
				</a>
			</td>
		</tr>
	</table>
	<br>
<%	End Sub
%>
<!--------------------------------------------------------------------------------------------------------------------------------------------------------->
<%
	Sub FineS()
%>		</basefont>
		</BODY>
		</html>
<%
	End Sub
%>	
<!--------------------------------------------------------------------------------------------------------------------------------------------------------->
<%
	Sub Imposta_Stampa()
		Dim sql, RR, aCognome, aNome, aMinuti
		Dim n, i, j, k
		
		'Response.Write Request.Form("Nome")
		'Response.Write Request.Form("Cognome")
		
		aCognome = Split(Request.Form("Cognome"), ",", -1, 1)
		aNome = Split(Request.Form("Nome"), ",", -1, 1)
		aOre = Split(Request.Form("Ore"), ",", -1, 1)
		aMinuti = Split(Request.Form("Minuti"), ",", -1, 1)
		n = Request.QueryString("n") + 0
		
%>		<table border=0 cellspacing=2 cellpadding=1 width="630">
			<tr align=center>
				<td class="tbltext3">
					<b>
					<%=sDescAula%> - <%=DecCodVal("SESFO", "0", "", sCodSess, 1)%>
					</b>
				<td>
			</tr>
		</table>
<%		
		i = 1
		k = 0		
		Do while i <= n
			if cstr(aOre(i-1)) = "0" or cstr(aOre(i-1)) = " 0" then
'				Response.Write "aOre" & i-1 & "=" & aOre(i-1)
				k=k+1
			end if
			i = i + 1
		loop
		
%>		<form method=post name=form1>
		<table border=0 cellspacing=2 cellpadding=1 width="630">
			<tr>
				<td class="tbltext" align=left>
					<b>
					Ora di inizio lezione
					</b>
				<td>
				<td class="tbltext" align=left>
					<input type=text name="IOre" border=0 maxlength=2 size=2 value="" onchange="javascript:ImpostaOra(<%=k%>)">
					: <INPUT type=text name="IMinuti" border=0 maxlength=2 size=2 value="" onchange="javascript:ImpostaM(<%=k%>)">
				</td>
				<td class="tbltext" align=right>
					<b>
					Presenze del giorno &nbsp; <%=nData%>
					</b>
				<td>
			</tr>
		</table>

		<table border=1 bordercolor="#f1f3f3" cellspacing=2 cellpadding=1 width=630>
			<tr align=center>
				<td class="tbltext" align=center width=192px>
					<b>Cognome e Nome</b>
				</td>
				<td class="tbltext" align=center width=153px>
					<b>Firma entrata</b>
				</td>
				<td class="tbltext" align=center width=83px>
					<b>ore</b>
				</td>
				<td class="tbltext" align=center width=153px>
					<b>Firma uscita</b>
				</td>
				<td class="tbltext" align=center width=73px>
					<b>ore</b>
				</td>
			</tr>
<%	 	
		j = 0
		i = 1

		Do while i <= n 
%>				<tr align=center height=25px>
					<!--td class="tbltext" align=center>
						<b><%=i%></b>
					</td-->
					<td class="tbltext" align=left>
						<%=aCognome(i-1)%> &nbsp; <%=aNome(i-1)%>
					</td>
					<td class="tbltext" align=center>
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					</td>
					<td class="tbltext" align=center nowrap>
<%						if (cstr(aOre(i-1)) = "0" or cstr(aOre(i-1)) = " 0") and (cstr(aMinuti(i-1)) = "0" or cstr(aMinuti(i-1)) = " 0") then
							j = j + 1
%>							<INPUT type="text" name=txtOre maxlength=2 size=2 disabled>:<INPUT type="text" name=txtMinuti maxlength=2 size=2 disabled>
<%						else
%>							&nbsp;
<%						end if
%>					</td>
					<td class="tbltext" align=center>
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					</td>
					<td class="tbltext" align=center>
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					</td>
				</tr>
<%			
			i=i+1
		LOOP
%>			</table>
			
			<table border=1 bordercolor="#f1f3f3" cellspacing=2 cellpadding=1 width=630>
			<tr height=120px> 
				<td colspan = 6 align=center>
					&nbsp;
				</td>
			</tr>
			</table>
			<br>
			<table border=0 bordercolor="#f1f3f3" cellspacing=2 cellpadding=1 width=630>
				<tr> 
					<td colspan = 6 align=center>
						<input type=button name="ok" value="Conferma" onclick="javascipt:return fStampa()">
					</td>
				</tr>
				<tr>
					<td colspan = 6 align=center>
						<input type=button value="Indietro" name="indietro" onclick="javascript:history.back()">
					</td>
					</form>
				</tr>
			</table>
		
<%		
	End Sub		
%>	
<!--------------------------------------------------------------------------------------------------------------------------------------------------------->
<!--	FINE BLOCCO ASP		-->
<!--		MAIN			-->

<!--#include file ="../../include/sysfunction.asp"-->
<%
	Dim strConn, sErrore, sPrv, Rif, sCForm, icf, sCodSess, bSt
	
	if ValidateService(session("idpersona"),"Registro Classe") <> "true" then 
		response.redirect "../../util/error_login.asp"
	end if
	
%>
	<!--#include file ="../../include/OpenConn.asp"-->
	<!--#include file ="../../include/DecCod.asp"-->
<%
	
	nClasse = Request.QueryString("classe")
	

	sPrv = Request.QueryString("prv")
	sCForm = Request.QueryString("cf")
	sDescAula = Request.QueryString("Aula")
	icf = Request.QueryString("icf")
	sCodSess = Request.QueryString("CodSess")
	nData = Request.QueryString("Data")
	
	Rif = "ASS_VisAssenze.asp?Data=" & nData & "&classe=" & nClasse & "&Aula=" & server.URLEncode(sDescAula)
	Rif = Rif & "&prv=" & sPrv & "&icf=" & icf & "&CodSess=" & sCodSess	& "&cf=" & server.URLEncode(sCForm)
	bSt = Request.Form("St")
	
	InizioS()
	Imposta_Stampa()

%><!--#include file ="../../include/CloseConn.asp"--><%
	FineS()
%>
<!--	FINE BLOCCO MAIN	-->