<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%'Option Explicit 
   'Response.ExpiresAbsolute = Now() - 1 
   'Response.AddHeader "pragma","no-cache"
   'Response.AddHeader "cache-control","private"
   'Response.CacheControl = "no-cache"
%>
<!-- #include virtual="/strutt_testa2.asp"-->
<!--#include virtual = "/include/openconn.asp"-->
<!--#include virtual="/util/portallib.asp"-->
<%
if ValidateService(session("idutente"),"CRR_VISCRITERIO", CC) <> "true" then 
	response.redirect "/util/error_login.asp"
end if
%>
<!-- ************** Javascript Inizio ************ -->

<script LANGUAGE="Javascript">

<!--#include Virtual = "/Include/help.inc"-->

//include del file per fare i controlli sulla numericit� dei campi
<!--#include virtual = "/Include/ControlNum.inc"-->
<!--#include virtual = "/Include/ControlString.inc"-->

function VaiInizio(sBando, sTipo){
	location.href = "CRR_VisCriterio.asp?Bando=" + sBando + "&Tipologia=" + sTipo;
}	

//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

function insTabelle(){
	newWindow = window.open('CRR_CreaRegola.asp','newWind','width=340,height=400')
	// Apro una finestra che mi d� la possibilit� di 
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

function ControllaDati(frmModCriterio,Tipologia) {
//		frmModCriterio.txtTipRegola		- Alfa di 1 -
//		frmModCriterio.txtIdBando		- Alfa di 3 -
//		frmModCriterio.txtPrgRegola		- Numerico - 
//		frmModCriterio.txtTabRif		- Alfa di 250 -
//		frmModCriterio.txtRegola		- Alfa di 250 -
//		frmModCriterio.txtValore		- Numerico -
//		frmModCriterio.txtDescRegola	- Alfa di 50 -
//		frmModCriterio.txtCodRegola		- Alfa di 2 -

	frmModCriterio.txtPrgRegola.value = TRIM(frmModCriterio.txtPrgRegola.value)
	if (frmModCriterio.txtPrgRegola.value == ""){
		alert("Campo 'Prg. regola' obbligatorio")
		frmModCriterio.txtPrgRegola.focus() 
		return false
	}
	if (!IsNum(frmModCriterio.txtPrgRegola.value)){
		alert("Il valore deve essere numerico")
		frmModCriterio.txtPrgRegola.focus() 
		return false
	}
	
	frmModCriterio.txtTabRif.value = TRIM(frmModCriterio.txtTabRif.value)
	if (frmModCriterio.txtTabRif.value == ""){
		alert("Campo 'Tabelle di riferimento' obbligatorio")
		frmModCriterio.txtTabRif.focus() 
		return false	
	}
	
	frmModCriterio.txtRegola.value = TRIM(frmModCriterio.txtRegola.value)
	if (frmModCriterio.txtRegola.value == ""){
		alert("Campo 'Regola' obbligatorio")
		frmModCriterio.txtRegola.focus() 
		return false
	}
	
	if ((Tipologia == "V") || (Tipologia == "A")){
	
		frmModCriterio.txtValore.value = TRIM(frmModCriterio.txtValore.value)
		if (frmModCriterio.txtValore.value == ""){
			alert("Campo 'Valore' obbligatorio")
			frmModCriterio.txtValore.focus() 
			return false
		}		
	}
		
	//controllo del campo valore		
	if  (frmModCriterio.txtValore.value != ""){	
	
		var pos = frmModCriterio.txtValore.value.lastIndexOf(".");
		sInteri = frmModCriterio.txtValore.value.substring(0,pos);
		sDecimali = frmModCriterio.txtValore.value.substring(pos+1);
		
		if (!IsNum(sInteri)){
			alert("Il valore deve essere numerico")
			frmModCriterio.txtValore.focus() 
			return false
		}
		
		if (!IsNum(sDecimali)){
			alert("Il valore deve essere numerico")
			frmModCriterio.txtValore.focus() 
			return false
		}
		
		//controllo inutile. VERIFICARE
		/*
		if (sValue.length != frmModCriterio.txtValore.value.length) {
			sValue = frmModCriterio.txtValore.value.substring(pos+1)
			if (!IsNum(sValue)){
				alert("Il valore deve essere numerico e pu� avere il formato (nn.nn)")
				frmModCriterio.txtValore.focus() 
				return false
			}
		}	
		*/
		
		if (frmModCriterio.txtTipRegola.value == "A"){
			if (frmModCriterio.txtValore.value > 100){
				alert("Il valore non pu� superare il 100%")
				frmModCriterio.txtValore.focus() 
				return false			
			}
		}
		
	}	
	
	frmModCriterio.txtDescRegola.value = TRIM(frmModCriterio.txtDescRegola.value)
	if (frmModCriterio.txtDescRegola.value == ""){
		alert("Campo 'Descrizione regola' obbligatorio")
		frmModCriterio.txtDescRegola.focus() 
		return false
	}

	if (Tipologia == "S"){
		frmModCriterio.txtCodRegola.value = TRIM(frmModCriterio.txtCodRegola.value)
		if (frmModCriterio.txtCodRegola.value == ""){
			alert("Campo 'Cod Regola' obbligatorio")
			frmModCriterio.txtCodRegola.focus() 
			return false
		}
	}
	return true
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

</script>
<!-- ************** Javascript Fine ************ -->

<!-- ************** ASP Inizio *************** -->

<%
sub Inizio()
%>
	<table border="0" width="520" cellspacing="0" cellpadding="0" height="81">
	   <tr>
	     <td width="500" background="<%=Session("Progetto")%>/images/titoli/strumenti2b.gif" height="81" valign="bottom" align="right">
	       <table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
	         <tr>
	           <td width="100%" valign="top" align="right"><b class="tbltext1a">Gestione Progetti</span></b></td>
	         </tr>
	       </table>
	     </td>
	   </tr>
	</table>
	<br>
	<table cellpadding="0" cellspacing="0" width="500" border="0">
		<tr height="18">
			<td class="sfondomenu" height="18" width="67%">
			<span class="tbltext0"><b>&nbsp;GESTIONE REGOLE</b></span></td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
			<td valign="middle" align="right" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1">(*) campi obbligatori</td>
		</tr>
		<tr width="371" class="SFONDOCOMM">
			<td colspan="3">
			Modifica dei dati gi� inseriti. <br>
			Premere <b>Invia</b> per salvare le modifiche. 
			<a href="Javascript:Show_Help('/Pgm/help/GestProgetti/Alfabeta/Regole/CRR_ModCriterio')" onmouseover="javascript:window.status=' '; return true"><img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a></td>
		</tr>
		<tr height="2">
			<td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>
	<br>			
	<br>
<%
end sub

'----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

sub ImpostaPag()

	sSQL = "SELECT ID_REGOLE, TIPO_REGOLA, ID_BANDO, PRG_REGOLA," &_
			"TAB_RIF, REGOLA, VALORE,DESC_REGOLA,COD_REGOLA," &_
			"DT_TMST from REGOLE where ID_REGOLE=" & nIdRegola
        
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	set  rsRegole = CC.Execute(sSQL)
	 if rsRegole.eof then
	 Response.Write "NON" 
	 else
	 
	sTipRegola = rsRegole("TIPO_REGOLA")
	
%>
<%

'sqlbando ="SELECT COD_BANDO FROM BANDO where ID_BANDO=" & sidbando
if rsRegole("ID_BANDO") <> "" then
	sIdBando = rsRegole("ID_BANDO")

	sqlbando ="SELECT COD_BANDO FROM BANDO where ID_BANDO=" & rsRegole("ID_BANDO") 
'PL-SQL * T-SQL  
SQLBANDO = TransformPLSQLToTSQL (SQLBANDO) 
    set rsbando = CC.Execute(sqlbando)
    
     cbando = rsbando("COD_BANDO")
     
    set rsbando = nothing 
else
	cbando = ""
	sIdBando = 0
end if


%>
<form name="frmModCriterio" method="POST" onsubmit="return ControllaDati(this,'<%=sTipRegola%>')" action="CRR_CnfCriterio.asp?">

	<input type="hidden" value="Mod" name="Action">
	<input type="hidden" value="<%=nIdRegola%>" name="IdReg">
	<input type="hidden" name="Bando" value="<%=sIdBando%>">
	<input type="hidden" value="<%=rsRegole("DT_TMST")%>" name="dtTmst">
	<input type="hidden" name="txtTipRegola" value="<%=sTipRegola%>">
	<br>
	<table border="0" cellpadding="1" cellspacing="2" width="500">
	    <tr class="tbltext1">
  			<td align="left">
				<b>Cod. Bando</b>
            </td>
  			<td class="tbltext" align="left">
				 <b><%=cbando%></b>
            </td>
        </tr>
		<tr class="tbltext1">
  			<td align="left">
				<b>Tipo Regola</b>
            </td>
  			<td class="tbltext" align="left">
				<b>
				<%
				select case sTipRegola
					case "S"
						Response.Write ("Selezione")
					case "V"
						Response.Write ("Valorizzazione")
					case "A"
						Response.Write ("Aggregazione")
				end select 					
				%>
				</b>
			</td>
        </tr>
		<tr class="tbltext1">
  			<td align="left">
				<b>Prg. regola*</b>
	        </td>
  			<td align="left">
				<input style="TEXT-TRANSFORM: uppercase;" class="textblack" size="15" maxlength="50" name="txtPrgRegola" value="<%=rsRegole("PRG_REGOLA")%>">
	        </td>
	    </tr>
		<tr class="tbltext1">
  			<td align="left">
				<b>Tabelle di riferimento*</b>
            </td>
  			<td align="left">
  				<textarea style="TEXT-TRANSFORM: uppercase; HEIGHT: 80px " cols="70" maxlength="50" name="txtTabRif" class="textblack" OnKeyup="JavaScript:CheckLenTxArea(txtTabRif,txtNumCarRif,250); return true"><%=rsRegole("TAB_RIF")%></textarea>
                <input type="hidden" name="txtNumCarRif" size="2">
            </td>
        </tr>
		<tr class="tbltext1">
  			<td align="left">
				<b>Regola*</b>
            </td>
  			<td align="left">
				<textarea style="TEXT-TRANSFORM: uppercase; HEIGHT: 80px" cols="70" maxlength="50" name="txtRegola" class="textblack" OnKeyup="JavaScript:CheckLenTxArea(txtRegola,txtNumCarRegola,250); return true"><%=rsRegole("REGOLA")%></textarea>
                <input type="hidden" name="txtNumCarRegola" size="2">
            </td>
        </tr>	    
        <%if sTipRegola <> "S" then%>
			<tr class="tbltext1">
  				<td align="left">
					<b>Valore* </b>
				</td>
  				<td align="left">
					<input style="TEXT-TRANSFORM: uppercase;" class="textblack" size="6" maxlength="6" name="txtValore" value="<%=rsRegole("VALORE")%>">
					<%
					if sTipRegola = "A" then
						%><span class="tbltext">%</span><%
					end if
					%>
				</td>
			</tr>
		<%end if%>

		<tr class="tbltext1">
  			<td align="left">
				<b>Descrizione regola*</b>
	        </td>
  			<td align="left">
				<input style="TEXT-TRANSFORM: uppercase;" class="textblack" size="71" maxlength="50" name="txtDescRegola" value="<%=rsRegole("DESC_REGOLA")%>">
	        </td>
	    </tr>
	       
	   <%if sTipRegola = "S" then%>
	       
		<tr class="tbltext1">
  			<td align="left">
					<strong>Cod Regola*</strong>
	        </td>
  			<td align="left">
				<input style="TEXT-TRANSFORM: uppercase;" class="textblack" size="15" maxlength="1" name="txtCodRegola" value="<%=rsRegole("COD_REGOLA")%>">
	        </td>
	    </tr>

	   <%end if%>
	   
	</table>	   
	
	<br>
	
	<!--impostazione dei comandi-->
	<table cellpadding="0" cellspacing="0" width="300" border="0">	
		<tr align="center">
			<td nowrap><input type="image" name="Conferma" src="<%=Session("progetto")%>/images/conferma.gif"></td value="Registra">
			<td nowrap><a href="javascript:document.frmModCriterio.reset()"><img border="0" src="<%=Session("progetto")%>/images/annulla.gif"></td>
			<td nowrap><a href="javascript:history.go(-1)"><img src="<%=session("Progetto")%>/images/indietro.gif" border="0" name="imgPunto2" onmouseover="javascript:window.status=' '; return true"></a></td>
		</tr>
	</table>

	<br>

	<table border="0" cellpadding="0" cellspacing="1" width="500">
		<tr> 
			<td align="center" colspan="2"> 
				<a href="Javascript:insTabelle()" class="textred" onmouseover="javascript:window.status=' '; return true">
				     <b>Crea Regola</b>
				</a>
			</td>
		</tr>
	</table>

</form>

<%
     end if
	rsRegole.Close
	set rsRegole = nothing

end sub

'----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

sub Fine()
%>
<!--#include virtual = "/include/closeconn.asp"-->
<!--#include virtual = "strutt_coda2.asp"-->
<%
end sub%>

<!-- ************** ASP Fine *************** -->

<!-- ************** MAIN Inizio ************ -->
<%


dim nIdRegola
dim sSQL
dim reRegole
dim sIdBando
dim sTipRegola

	nIdRegola = Request("idRegola")
	'sIdBando = Request("Bando")
		
	Inizio()
	ImpostaPag()
	Fine()
%>
<!-- ************** MAIN Fine ************ -->
