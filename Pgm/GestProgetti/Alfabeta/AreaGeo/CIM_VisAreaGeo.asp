<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include virtual = "/strutt_testa2.asp"-->
<!--#include virtual ="/include/openconn.asp"-->
<!--#include virtual = "util/portallib.asp"-->
<!--#include virtual = "util/dbutil.asp"-->
<!--#include virtual = "include/DecCod.asp"-->

<%
if ValidateService(session("idutente"),"CIM_VISAREAGEO", CC) <> "true" then 
	response.redirect "/util/error_login.asp"
end if
%>

<script LANGUAGE="Javascript">
<!--#include Virtual = "/Include/help.inc"-->

function Selezionato(){
	var ValoreRegione
	
	ValoreRegione=document.frmVisAreaGeo.cmbProv.value
	document.frmVisAreaGeo.ProvinciaSel.value=ValoreRegione
	
	if (ValoreRegione=="")
	{
		alert('Devi selezionare almeno una Regione.')
	}else{
		document.frmVisAreaGeo.submit();
	}
}

function Invia()
{
	if (document.frmVisAreaGeoCombo.CmbDescProv.value=="")
	{
		alert('Devi selezionare un Bando.')
	}else
	{
		document.frmVisAreaGeoCombo.submit();
	}
}

</script>
</head>

<body>
<table border="0" width="520" cellspacing="0" cellpadding="0" height="81">
   <tr>
     <td width="500" background="<%=Session("Progetto")%>/images/titoli/strumenti2b.gif" height="81" valign="bottom" align="right">
       <table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
         <tr>
           <td width="100%" valign="top" align="right"><b class="tbltext1a">Gestione Progetti</b></td>
         </tr>
       </table>
     </td>
   </tr>
</table>
<br>
<table cellpadding="0" cellspacing="0" width="500" border="0">
	<tr height="18">
		<td class="sfondomenu" height="18" width="50%">
			<span class="tbltext0"><b>&nbsp;Gestione&nbsp;Area&nbsp;Geografica&nbsp;dei&nbsp;Centri&nbsp;di&nbsp;Formazione</b>
			</span>
		</td>
		<td width="5%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
		<td valign="middle" align="right" width="40%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1"></td>
	</tr>
	<tr width="371" class="SFONDOCOMM">
		<td colspan="3">
			Selezionare un area territoriale per visualizzare i relativi Bandi.
			<a href="Javascript:Show_Help('/Pgm/help/GestProgetti/Alfabeta/AreaGeo/CIM_VisAreaGeo/Default.htm')" name="prov3" onmouseover="javascript:window.status='' ; return true">
				<img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0">
			</a>
		</td>
	</tr>
	<tr height="2">
		<td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif">
		</td>
	</tr>
</table>
<br><br>

<table width="500" border="0" cellspacing="2" cellpadding="1">
	<tr> 
		<td align="middle" class="tbltext3">
		<%
		dim sMask
		dim nProvSedi
		dim i
		dim sCondizione
		dim sFil
		dim sIsa
		dim rsRec

		sql="SELECT distinct(AB.ID_AREATERR),DESC_AREATERR FROM AREA_BANDO AB," &_
		    "AREA_TERR AT WHERE AB.ID_AREATERR=AT.ID_AREATERR " &_
		    "AND ID_UORG = " & session("iduorg")
		
        IF session("iduorg") <> "" then
		  	
		    sProv = Request.form("ProvinciaSel")
			if sProv = "" then	
%>
				
<%
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
					set rsUorg = cc.execute(sql)
                    	if not rsUorg.eof  then%>
					<font class="tbltext3">Selezionare un'Area Territoriale</font>
					<br><br>
				
					
					<form name="frmVisAreaGeo" action="CIM_VisAreaGeo.asp" method="post">
					<input type="hidden" id="ProvinciaSel" name="ProvinciaSel">
					
					<select ID="cmbProv" class="textblack" name="CmbDescProv" onchange="Selezionato()">
						<option></option>
<%
					Do until rsUorg.eof 
					    Response.write "<OPTION  value ='" & rsUorg("ID_AREATERR") & "'> " &_
						rsUorg("DESC_AREATERR") & "</OPTION>"
					rsUorg.movenext
				    Loop		
%>
				    </select>
					<input type="hidden" id="CmbRegSelez" name="CmbRegSelez">
					</form>	
<%					set rsUorg = nothing
            else%>
            <table align="center" width="500" border="0">
			<tr>
			<td>&nbsp;&nbsp;&nbsp;</td>
				<td class="tbltext3"><b>Non risulti essere associato a nessuna area territoriale.</b>
				</td>
			<tr><td>&nbsp;</td></tr>	
			</tr>
			
		</table>
    <%        end if
            
            
			else
					sSql=	"SELECT ID_BANDO FROM AREA_BANDO WHERE" &_
							" ID_AREATERR =" & sProv & " AND ID_UORG=" & session("iduorg") 
					
					dim RecBando
					dim nNumeroBandi
					SET RecBando=Server.CreateObject("ADODB.Recordset")
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
					RecBando.Open sSql,CC,3
					 
					dim nTrovati
					nTrovati=RecBando.RecordCount 
                    if nTrovati= 1 then
                       sIdBando= RecBando("ID_BANDO")
                    end if
                    RecBando.Close 
                    set RecBando = nothing   			
					If nTrovati = "0" then
%>
					<table WIDTH="500" ALIGN="center" BORDER="0" CELLSPACING="1" CELLPADDING="1">
						<tr>
							<td class="tbltext3" align="middle">
								Non ci sono Bandi per l'area selezionata.
								<br><br><br>
								<a href="javascript:history.back()">
									<img src="<%session("Progetto")%>/images/indietro.gif" border="0">
								</a>	
							</td>
						</tr>
					</table>
<%
					Elseif nTrovati = "1" then
%>
						<form name="frmVisAreaGeoSingola" action="CIM_ModAreaGeo.asp" method="post">
							<input type="hidden" class="textblack" name="txtAreaTerr" value="<%=sProv%>">
							<input type="text" class="textblack" id="CmbDescProv" name="CmbDescProv" value="<%=sIdBando%>">
							<script language="javascript">
								document.frmVisAreaGeoSingola.submit();
							</script>						
						</form>	
<%
					Else
%>
						<form name="frmVisAreaGeoCombo" action="CIM_ModAreaGeo.asp" method="post">
							<input type="hidden" class="textblack" name="txtAreaTerr" value="<%=sProv%>">
							<font class="tbltext3">Seleziona un Bando</font>
							<br><br>
<%
							sSql="SELECT ID_BANDO,DESC_BANDO FROM BANDO " & _
							     "WHERE ID_BANDO IN (SELECT ID_BANDO FROM " &_
							     " AREA_BANDO WHERE ID_AREATERR =" & sProv &_
							     " AND ID_UORG=" & session("iduorg") & ")" 
						
							dim RecDescBando
							set RecDescBando=Server.CreateObject("ADODB.recordset")
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
							RecDescBando.Open sSql,CC,3
							RecDescBando.MoveFirst
%>
							<select ID="CmbDescProv" class="textblack" name="CmbDescProv" onchange="Invia()">
							<option></option>
<%
							do until RecDescBando.EOF
								Response.write "<OPTION  value =" & RecDescBando("ID_BANDO") & "> " &_
								mid(RecDescBando("DESC_BANDO"),1,70)  & "</OPTION>"
								
							RecDescBando.MoveNext 
							loop
							Response.Write "</SELECT>"
							RecDescBando.Close	
%>
						</form>	
				
					<%
					End if	
			end if
		ELSE		
%>          
            <table WIDTH="500" ALIGN="center" BORDER="0" CELLSPACING="1" CELLPADDING="1">
				<tr>
					<td class="tbltext3" align="middle">
						Non ci sono Aree Territoriali Associate.<br><br><br>
					</td>
				</tr>
			</table>
<%		END IF%>		
		</td>
	</tr>	
</table>
</body>	

<!--#include virtual ="/include/CloseConn.asp"-->
<!--#include virtual = "/strutt_coda2.asp"-->
