<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include virtual = "/strutt_testa2.asp"-->
<!--#include virtual ="/include/openconn.asp"-->
<!--#include virtual = "util/portallib.asp"-->
<!--#include virtual = "util/dbutil.asp"-->
<!--#include virtual = "include/DecComun.asp"-->
<!--#include virtual = "include/SelAreaTerrBandi.asp"-->
<%
if ValidateService(session("idutente"),"DTC_VISDISTTERR", CC) <> "true" then 
	response.redirect "/util/error_login.asp"
end if
%>

<script LANGUAGE="Javascript">
<!--#include Virtual = "/Include/help.inc"-->

	function Scarica(report){
		fin=window.open(report,"","toolbar=0;location=0,directories=0,status=0,menubar=1,scrollbars=1,resizable=1,copyhistory=0,width=450,height=340,screenX=200,screenY200");
	}

</script>

<%

'-------------------------------------------------------------------------------------------------------------------------------

Sub Msgetto(Msg)
%>
	<br>
	<table align="center">
		<tr>
			<td class="tbltext3"><b><%=Msg%></b></td>
		</tr>	
	</table>
<%
End Sub

'-------------------------------------------------------------------------------------------------------------------------------

function CreaFile()

sSql="select a.esito_grad, B.COGNOME, B.NOME, B.COD_FISC, " & _
			"B.IND_RES, B.CAP_RES, B.E_MAIL, " & _
			"B.PRV_RES, B.COM_RES " & _
			"from domanda_iscr a,persona b" &_
			" where a.id_bando = " & sIdBando & "" & _ 
			" and esito_grad in( 'S','X')" & _
			" and a.id_persona = b.id_persona" &_
			" and prv_res in " & sTarga &_
			sCond &_
			" order by a.esito_grad,B.PRV_RES, B.COM_RES, B.COGNOME, B.NOME " 

	'Response.Write SSQL
	
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	set rsAppo1 = CC.Execute(sSQL)

	CreaFile = ""
	sDtOggi = day(Now) & month(Now) & year(Now)
	
	sPath = session("progetto") & "/DocPers/Temp/" & _
				"AmmessiDaPianificare_" & sIdBando & "_" & sDtOggi & ".xls"  
				
	sNomeFile = Server.MapPath("/") & sPath
	
	'Response.Write sNomeFile
	'Response.End
	
	If rsAppo1.eof Then
		CreaFile = "Nessun record trovato"
	end if
		 
	Set fso = CreateObject("Scripting.FileSystemObject")
	Set a = fso.CreateTextFile(sNomeFile, True)
			
	a.WriteLine "Candidati ammessi da pianificare per il Bando " & DecBando(sIdBando) & _
		" - Situazione al " & day(Now) & "/" & month(now) & "/" & year(now) & " Ore " & hour(now) & ":" & minute(now)
	a.WriteLine ""
	a.WriteLine chr(9) & _
		"Codice Fiscale"				& chr(9) & _  
		"Cognome"						& chr(9) & _
		"Nome"							& chr(9) & _
		"Indirizzo Residenza"			& chr(9) & _ 						 
		"Comune"						& chr(9) & _
		"Prv."							& chr(9) & _ 						 
		"CAP"							& chr(9) & _ 						 
		"E_Mail"						& chr(9)
	a.WriteLine ""

	rsAppo1.MoveFirst
	'Response.Write rsAppo1("COD_FISC")

	do while not rsAppo1.EOF
		sApp = chr(9) & _
			rsAppo1("COD_FISC")				& chr(9) & _  
			rsAppo1("COGNOME")				& chr(9) & _
			rsAppo1("NOME")					& chr(9) & _
			rsAppo1("IND_RES")				& chr(9) & _ 						 
			DescrComune(rsAppo1("COM_RES"))	& chr(9) & _
			rsAppo1("PRV_RES")				& chr(9) & _ 						 
			rsAppo1("CAP_RES")				& chr(9) & _ 						 
			rsAppo1("E_MAIL")				& chr(9)
			
		a.WriteLine sApp
	
		rsAppo1.MoveNext
	loop
	
	rsAppo1.Close
	set rsAppo1 = nothing
	
end function

'----------------------------------------------------------------------------------------------------------------------------------------------------------------------------

sub Inizio()
%>
<form name="frmInsDistTerr">
<table border="0" width="520" cellspacing="0" cellpadding="0" height="81">
	<tr>
		<td width="500" background="<%=Session("Progetto")%>/images/titoli/strumenti2b.gif" height="81" valign="bottom" align="right">
			<table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
				<tr>
					<td width="100%" valign="top" align="right"><b class="tbltext1a">Gestione Progetti</span></b></td>
				</tr>
			</table>
	   </td>
	</tr>
</table>
<br>
<table cellpadding="0" cellspacing="0" width="500" border="0">
	<tr height="18">
		<td class="sfondomenu" height="18" width="67%">
			<span class="tbltext0"><b>&nbsp;DISTRIBUZIONE TERRITORIALE AMMESSI</b></span></td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
		<td valign="middle" align="right" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1"></td>
	</tr>
	<tr width="371" class="SFONDOCOMM">
		<td colspan="3">
			Distribuzione sul territorio dei candidati ammessi a partecipare.
			<a href="Javascript:Show_Help('/Pgm/help/Gestprogetti/Alfabeta/Areageo/DTC_InsDistTerr')">
			<img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
		</td>
	</tr>
	<tr height="2">
		<td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif">
		</td>
	</tr>
</table>

<%	
end sub

'----------------------------------------------------------------------------------------------------------------------------------------------------------------------------

sub ImpostaPag()
	
	controllo = 1
	sIdBando = Request.Form("CmbDescBando")
	sAmmessi = Request.Form ("cmbSituazione")
	if sAmmessi = "AMM" then
	    controllo = 1
	    descContr = "CANDIDATI AMMESSI"
	    sCond = ""
	else
	    controllo = 0
	    descContr = "CANDIDATI AMMESSI DA PIANIFICARE"
	    sCond =" AND NOT EXISTS (SELECT ID_PERSONA FROM COMPONENTI_CLASSE Z " &_
			   "WHERE Z.ID_PERSONA=A.ID_PERSONA) "
	end if
	
	sProv = SetProvBando(sIdBando,session("idUorg"))
	
	'Response.Write sProv
	
	'controllo che ci sia almeno una provincia associata
	if trim (sProv) = "" then
		Msgetto("Non sono state associate Province all'area<br>territoriale relativa al bando " & DecBando(sIdBando))
		%>
		<br>
		  <table width="500" border="0">
					<tr align="center">
						<td nowrap><a href="javascript:history.go(-1)" onmouseover="javascript:window.status=' '; return true"><img src="<%=session("Progetto")%>/images/indietro.gif" border="0" name="imgPunto2"></a><td>
			    </tr>
		  </table>  
		<%
		exit sub
	end if
	
	nProvSedi = len(sProv)
	
    if nProvSedi <> 0 then
       pos_ini = 1
       sTarga="(" 	                
       for i=1 to nProvSedi/2
			if i>1 then
				sTarga=sTarga & ","
			end if
       		sTarga = starga& "'" & mid(sProv,pos_ini,2) & "'"										' DecCodVal(nome_tabella, isa, dt_decor, codice, flag_conn)
            pos_ini = pos_ini + 2
       next
       sTarga=sTarga & ")" 	                
    end if

	dim totali
	dim comune
	dim provincia
	
	sSql="select a.esito_grad, B.PRV_RES, B.COM_RES, count(*)as conta " & _
			"from domanda_iscr a,persona b" &_
			" where a.id_bando = " & sIdBando & "" & _ 
			" and esito_grad in( 'S','X')" & _
			" and a.id_persona = b.id_persona" &_
			" and prv_res in " & sTarga &_
			sCond &_
			" group by a.esito_grad, B.PRV_RES, B.COM_RES" 
	
	'Response.Write SSQL
	
	'sSql="select a.esito_grad, b.prv_res, b.com_res from domanda_iscr a,persona b" &_
	 '    " where a.id_bando = " & sIdBando & "" & _
	  '   " and esito_grad in( 'S')"
	 '    Response.write sSql
			 		 
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	set rsAppo = CC.Execute(sSQL)
%>
	<br><br>
	<table border="0" cellpadding="0" cellspacing="0" width="500" height="40">
		<tr>
			<td class="tbltext1" width="150" height="30">
				<b>Bando:</b>
			</td>
			
			<td class="tbltext">
				<b><%=DecBando(sIdBando)%></b>
				
			</td>
		</tr>
		
		<tr class="tbltext1"><td><b>Situazione relativa a:</b></td>
			<td class="tbltext">
				<b><%=descContr%></b>
			</td>
		</tr>
				
	</table>
<%								
	if rsAppo.EOF then	
%>			
		<br>
		<table align="center">
			<tr>
				<td class="tbltext3">
					<b>Non ci sono candidati ammessi sul territorio gestito.</b>
				</td>
			</tr>	
		</table>
<%	else%>
	    <br>	
	    <table border="0" cellpadding="1" cellspacing="2" width="500">
			<tr class="sfondocomm">
				<td align="center" width="30%">
					<b>Provincia di Residenza</b>
				</td>
				<td align="center" width="45%">
					<b>Comune di Residenza</b>
				</td>
				<td align="center" width="25%">
					<b>Totale Ammessi</b>
				</td>
				
			</tr>
				
			<% totGen = 0 
			
	'		num = 0
			do while not rsAppo.EOF
	'		num = num + 1
				sgrad = rsAppo("conta")
				if rsAppo("conta") <> "" then
						totali = rsAppo("conta")
				else
						totali = ""
				end if			
				if rsAppo("prv_res") <> "" then
						provincia = rsAppo("prv_res")
				else
						provincia = ""
				end if
				if rsAppo("com_res") <> "" then
						comune = DescrComune(rsAppo("COM_RES"))
				else
						comune = ""
				end if		
			    totGen = totGen + CLng(totali)
			%>	
		    <tr class="tblsfondo">
				
				<td class="tblDett" align="center">
					<%=provincia%>
				</td>
				<td class="tblDett" align="center">
					<%=comune%>
				</td>
				<td class="tblDett" align="center">
					<%=totali%>
				</td>												
				<%				
			rsAppo.MoveNext
			loop
	'		Response.Write "numero: " & num
			%>
			
				
			</tr>
			</table>
			
			<br>
		   <table border="0" cellpadding="1" cellspacing="2" width="500">
	
	    		<tr class="tblsfondo">
		     	   <td class="sfondocomm" width="75%">
			          <b> TOTALE CANDIDATI </b>
			       </td>
			       
			       <td class="tblDett">
			       <%
			       if UCase(descContr) = "CANDIDATI AMMESSI DA PIANIFICARE" then
						
						CreaFile()
						%>
						<a href="javascript:Scarica('<%=sPath%>')" class="tblDett">
							<b><%=totGen%>
							&nbsp;&nbsp;&nbsp;&nbsp;dettagli&nbsp;&nbsp;</b>
							<img src="<%session("Progetto")%>/images/formazione/txt.gif" border="0" height="20" width="21">
						</a>
						
						<%
			       else
			       %>
			           <b><%=totGen%></b>
			       <%
			       end if
			       %>
			       </td> 
			    </tr>
		    </table>
	<%end if
		
	rsAppo.Close
	set rsAppo = nothing
	%>
						
	        
<br>
  <table width="500" border="0">
			<tr align="center">
				<td nowrap><a href="javascript:history.go(-1)" onmouseover="javascript:window.status=' '; return true"><img src="<%=session("Progetto")%>/images/indietro.gif" border="0" name="imgPunto2"></a><td>
	    </tr>
  </table>  
</form>

<%

end sub

'--------------------------- MAIN -------------------------------------------------------------------------------------------------------------------------------------------------
	dim sTarga
	dim rsAppo1
	dim sIdBando
	
	dim sNomeFile
	dim sPath
	dim rsAppo
	
	dim sProv
	
	dim sCond
	dim totGen
	dim controllo
	
	Inizio()
	
	ImpostaPag()
%>

<!--#include virtual ="/include/closeconn.asp"-->
<!--#include virtual = "/strutt_coda2.asp"-->
