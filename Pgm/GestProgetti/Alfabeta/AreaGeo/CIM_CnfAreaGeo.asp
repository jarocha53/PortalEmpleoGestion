<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include Virtual = "/strutt_testa2.asp"-->
<!--#include virtual ="/include/openconn.asp"-->
<!--#include virtual = "/include/ControlDateVB.asp"-->
<!--#include Virtual = "/include/SysFunction.asp"-->

<%
if ValidateService(session("idutente"),"CIM_VISAREAGEO", CC) <> "true" then 
	response.redirect "/util/error_login.asp"
end if
%>

<script LANGUAGE="Javascript">
<!--#include virtual = "/Include/help.inc"-->
</script>

<%
dim RecConta, rsVerifica
dim nVerifica 
dim sBando, sDescBando
dim sComune
dim sCAP
dim sGruppo
dim sSQL
dim sProv
dim sProv2

sCAP=Request.Form ("txtCAP")
'Leuna, commentata riga => sBando=Request.Form ("txtBando")
sBando=clng(Request.Form ("txtBando"))
sDescBando=Request.Form ("txtDesBando")
sComune=Request.Form ("cmbComune")
sIdArea=Request.Form ("txtTerr")

sGruppo=Request.Form ("cmbGruppo")
sGruppo=UCASE(replace(sGruppo,"'","''"))

sSQL=	" SELECT count(*) Verifica FROM CI_AREAGEO" &_
		" WHERE ID_AREAGEO ='" & sGruppo & "'" &_
		" AND (ID_UORG <>" & session("iduorg") & " OR ID_UORG is null)"
		
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
set rsVerifica = CC.Execute(sSQL)
nVerifica = clng(rsVerifica("Verifica"))
rsVerifica.Close
set rsVerifica = nothing

IF nVerifica = 0 THEN 

	sSQL =	"select Count(*) as Contatore FROM CI_AREAGEO" &_
			" WHERE ID_BANDO=" & sBando &_
			" AND COMUNE='" & sComune & "'" &_
			" AND ID_AREAGEO='" & sGruppo & "'"

	set RecConta =Server.CreateObject("ADODB.Recordset")
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	RecConta.Open sSQL,CC,3

	If clng(RecConta("Contatore")) = 0 then

		if sCAP <> "" then
			'sSQL = "INSERT INTO CI_AREAGEO (ID_BANDO,COMUNE,CAP,ID_AREAGEO) " &_
			'	"VALUES (" & sBando & ",'" & sComune & "','" & sCAP & "','" & sGruppo & "')"
			sSQL =	"INSERT INTO CI_AREAGEO" &_
					" (ID_BANDO,COMUNE,CAP,ID_AREAGEO,ID_UORG)" &_
					" VALUES" &_
					" (" & sBando & ",'" & sComune & "','" & sCAP & "','" & sGruppo & "'," & session("iduorg") & ")"
		else					
			'sSQL = "INSERT INTO CI_AREAGEO (ID_BANDO,COMUNE,CAP,ID_AREAGEO) " &_
			'	"VALUES (" & sBando & ",'" & sComune & "',Null ,'" & sGruppo & "')"
			sSQL =	"INSERT INTO CI_AREAGEO" &_
					" (ID_BANDO,COMUNE,CAP,ID_AREAGEO,ID_UORG)" &_
					" VALUES" &_
					" (" & sBando & ",'" & sComune & "',Null,'" & sGruppo & "'," & session("iduorg") & ")"
		end if

		CC.BeginTrans
		Errore = EseguiNoC(sSQL, cc)

       ' Inizio()
%>
        
		<table width="500" border="0" cellspacing="2" cellpadding="1">
			<tr>  
				<td align="middle" class="tbltext3">
					<%if Errore = "0" then
						CC.CommitTrans
					%>
						<form name="frmTorna" action="CIM_ModAreaGeo.asp" method="post">
							<input type="hidden" value="<%=sBando%>" name="CmbDescProv">
							<input type="hidden" name="txtAreaTerr" value="<%=sIdArea%>"> 
							<script>
								alert("Inserimento correttamente effettuato.");
								frmTorna.submit()
							</script>	
						</form>								
					  	
					<%else
						CC.RollbackTrans
						Inizio()	
					%>  	
						Modifica dei dati non effettuabile. 
					    <br><br>
						<%Response.Write (Errore)%> 
						<br><br>
						<a href="javascript:history.back()">
							<img src="<%session("Progetto")%>/images/indietro.gif" border="0">
						</a>						
					<%end if%>				
				</td>
			</tr>
		</table>
<%
	Else
		Inizio()
%>	
		<table width="500" border="0" cellspacing="2" cellpadding="1">
			<tr> 
				<td align="middle" class="tbltext3">
					Esiste un'Area Geografica con le caratteristiche indicate
					<br><br>
					<a href="javascript:history.back()">
						<img src="<%session("Progetto")%>/images/indietro.gif" border="0">
					</a>
				</td>
			</tr>
			<tr> 
				<td align="middle">
				</td>
			</tr>
		</table>
	<%
	End if
	RecConta.Close 
	Set RecConta = nothing

ELSE
	Inizio()
%>
	<table width="500" border="0" cellspacing="2" cellpadding="1">
		<tr> 
			<td align="middle" class="tbltext3">
				Definizione Gruppo Area Geografica gi� utilizzata da altra Unit� Organizzativa.
				<br><br>
				<a href="javascript:history.back()">
					<img src="<%session("Progetto")%>/images/indietro.gif" border="0">
				</a>
			</td>
		</tr>
		<tr> 
			<td align="middle">
			</td>
		</tr>
	</table>	
<%
END IF

'------------------
Sub Inizio()%>
	<table border="0" width="520" cellspacing="0" cellpadding="0" height="81">
	   <tr>
	     <td width="500" background="<%=Session("Progetto")%>/images/titoli/strumenti2b.gif" height="81" valign="bottom" align="right">
	       <table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
	         <tr>
	           <td width="100%" valign="top" align="right"><b class="tbltext1a">Gestione Progetti</b></td>
	         </tr>
	       </table>
	     </td>
	   </tr>
	</table>
	<br>
	<table cellpadding="0" cellspacing="0" width="500" border="0">
		<tr height="18">
			<td class="sfondomenu" height="18" width="50%">
				<span class="tbltext0"><b>&nbsp;Gestione&nbsp;Area&nbsp;Geografica&nbsp;dei&nbsp;Centri&nbsp;di&nbsp;Formazione</b>
				</span>
			</td>
			<td width="5%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
			<td valign="middle" align="right" width="40%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1"></td>
		</tr>
		<tr width="371" class="SFONDOCOMM">
			<td colspan="3">
				Conferma Definizione/Modifica dell'Area Geografica.
				<a href="Javascript:Show_Help('/Pgm/help/GestProgetti/Alfabeta/AreaGeo/CIM_CnfAreaGeo/Default.htm')" name="prov3" onmouseover="javascript:window.status='' ; return true">
					<img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0">
				</a>
			</td>
		</tr>
		<tr height="2">
			<td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>
	<br><br>
	<table width="500" border="0" cellspacing="2" cellpadding="1">
		       <tr> 
			      <td>
			         <center>
		                <b class="tbltext1a">
				           Bando : <%=sDescBando%>
		                </b>
			         </center>
			      </td>
		      </tr>
	</table>
   <br><br>			
<%
End Sub
'------------------
%>	

<!--#include Virtual ="/Include/closeconn.asp"-->
<!--#include Virtual="/strutt_coda2.asp"-->


