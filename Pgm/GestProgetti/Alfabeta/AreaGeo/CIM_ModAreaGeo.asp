<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include virtual = "/strutt_testa2.asp"-->
<!--#include virtual ="/include/openconn.asp"-->
<!--#include virtual="/util/portallib.asp"-->
<!--#include virtual = "include/SelAreaTerrBandi.asp"-->

<%
if ValidateService(session("idutente"),"CIM_VISAREAGEO", CC) <> "true" then 
	response.redirect "/util/error_login.asp"
end if
%>


<script LANGUAGE="Javascript">
//include del file per fare i controlli sulla numericit� dei campi
	<!--#include virtual = "Include/ControlNum.inc"-->
	<!--#include virtual = "Include/ControlString.inc"-->
	<!--#include virtual = "Include/help.inc"-->

function Elimina()
{
	frmAreaGeoElimina.submit();
}
	
function ControllaDati2(myForm)
{
    var ok
	var nrec
	var chkbox
				
	ok = 0 
	nrec= document.frmAreaGeoElimina.txtNumElementi.value
	for (i=1; i<=nrec; i++)
	{
		chkbox = eval("document.frmAreaGeoElimina.ckFlag" + i)
		if (chkbox.checked)
		{
			ok = 1
			break
		}
	}
	if (ok == 0)
	{
		alert ("Devi selezionare almeno un'area Geografica")
		return false
	}
}
 
function CaricaCom() 
{
    gruppo=document.frmModAreaGeo.cmbGruppo.value;
    CAP=document.frmModAreaGeo.txtCAP.value; 
    sProv=document.frmModAreaGeo.cmbPrv.value;
    codice=document.frmModAreaGeo.txtCodBando.value; 
    AreaTerr=document.frmModAreaGeo.txtTerr.value; 
    location.href = "CIM_ModAreaGeo.asp?sel=" + sProv + "&Prv=" + sProv + "&Bando=" + codice + "&ATerr=" + AreaTerr + "&Gruppo=" + gruppo + "&CAP=" + CAP
}

function ControllaDati(myFrm)
{ 
	frmModAreaGeo.cmbGruppo.value = TRIM(frmModAreaGeo.cmbGruppo.value)
    
	if (frmModAreaGeo.cmbGruppo.value == "")
	{
		alert("Area Geografica Obbligatoria")
		frmModAreaGeo.cmbGruppo.focus()
		return false
	}  
	
	if (frmModAreaGeo.cmbComune.value == "")
	{
		alert("Comune Obbligatorio")
		frmModAreaGeo.cmbComune.focus()
		return false
	}  
		
	if (frmModAreaGeo.cmbGruppo.value != "")
	{
		var Trovo
		var Trovo2
		var Trovo3
		var Contenuto
		
		Contenuto=frmModAreaGeo.cmbGruppo.value
		
		Trovo=Contenuto.indexOf(",")
		Trovo2=Contenuto.indexOf('"')
		Trovo3=Contenuto.indexOf('�')
		if ((Trovo != -1)||
    	    (Trovo2 != -1)||
			(Trovo3 != -1))
		{
			alert('Alcuni caratteri relativi al Gruppo non sono ammessi')
		    frmModAreaGeo.cmbGruppo.focus();
			return false
		}		
	}  	
		 
	if (frmModAreaGeo.txtCAP.value !="")
	{
		if (frmModAreaGeo.txtCAP.value.length != 5) 
		{
		alert("Il cap deve essere di 5 caratteri")
		frmModAreaGeo.txtCAP.focus()
		return false
		}
	
	    if (!IsNum(frmModAreaGeo.txtCAP.value)) 
		{
		alert("Il valore deve essere numerico")
		frmModAreaGeo.txtCAP.focus()
		return false
		}
	}
	
	return true
}
</script>
<table border="0" width="520" cellspacing="0" cellpadding="0" height="81">
   <tr>
     <td width="500" background="<%=Session("Progetto")%>/images/titoli/strumenti2b.gif" height="81" valign="bottom" align="right">
       <table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
         <tr>
           <td width="100%" valign="top" align="right"><b class="tbltext1a">Gestione Progetti</b></td>
         </tr>
       </table>
     </td>
   </tr>
</table>
<br>
<table cellpadding="0" cellspacing="0" width="500" border="0">
	<tr height="18">
		<td class="sfondomenu" height="18" width="50%">
			<span class="tbltext0"><b>&nbsp;Gestione&nbsp;Area&nbsp;Geografica&nbsp;dei&nbsp;Centri&nbsp;di&nbsp;Formazione</b>
			</span>
		</td>
		<td width="5%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
		<td valign="middle" align="right" width="40%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1"></td>
	</tr>
	<tr width="371" class="SFONDOCOMM">
		<td colspan="3">
			Elenco/Struttura delle Aree Geografiche definite.
			<a href="Javascript:Show_Help('/Pgm/help/GestProgetti/Alfabeta/AreaGeo/CIM_ModAreaGeo/Default.htm')" name="prov3" onmouseover="javascript:window.status='' ; return true">
				<img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0">
			</a>
		</td>
	</tr>
	<tr height="2">
		<td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif">
		</td>
	</tr>
</table>
<br><br>
<%
dim sElencoProv
dim sProv
dim sMask
dim sBando 
dim sGruppo
dim sCAP

sCAP = Request.QueryString ("CAP")
sGruppo = Request.QueryString ("Gruppo")

sBando = Request.Form ("CmbDescProv")
if sBando = "" then
	sBando = Request.QueryString ("Bando")
end if

idArea = Request.Form ("txtAreaTerr")
if idArea = "" then
   idArea = Request.QueryString("ATerr")  
end if

'sMask=str delle prov. associate all'area terr(ad es.= FRLTRIRMVT)
sMask = SetProvAreaTerr(idArea)

sProv2 = Request.QueryString("Prv")
if sProv2 = "" then
   sProv2= mid(sMask,1,2)
end if

nProvSedi = len(sMask) 
i = 0	
if nProvSedi <> 0 then
	pos_ini=1
	sElencoprov=""  
	for i=1 to nProvSedi/2
		if i = nProvSedi/2 then
			sElencoProv =sElencoProv & "'" & mid(sMask,pos_ini,2) & "'"
		else
			sElencoProv =sElencoProv & "'" & mid(sMask,pos_ini,2) & "',"
		end if
		pos_ini = pos_ini + 2
	next
end if
%>
	<form name="frmModAreaGeo" method="post" onsubmit="return ControllaDati(this)" action="CIM_CnfAreaGeo.asp">
		<input type="hidden" name="txtTerr" value="<%=idArea%>"> 
		<input type="hidden" name="txtCodBando" value="<%=sBando%>">
<%																							
		sSQL = "SELECT ID_BANDO,DESC_BANDO " &_
		       "from BANDO WHERE ID_BANDO =" & sBando 
		
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
		set rsRec = CC.Execute(sSQL)
		IF NOT rsRec.EOF THEN
			Response.write "<INPUT type=hidden name=txtBando VALUE=" &_
			rsRec.Fields("ID_BANDO") & ">"
			sDescBando = rsRec.Fields("DESC_BANDO")
%>
            <input type="hidden" name="txtDesBando" value="<%=sDescBando%>">
		    <table width="500" border="0" cellspacing="2" cellpadding="1">
		       <tr> 
			      <td class="tbltext1a" align="right" width="200">
		                <b>Bando:&nbsp;</b>
			      </td>
			      <td class="tbltext1a" align="left" width="300">
		                <b><%=sDescBando%></b>
			      </td>
		      </tr>
			</table>
			<p>
			<input type="hidden" value="<%=mid(sProv,1,2)%>" name="txtPRV">
			<table width="500" border="0" cellspacing="2" cellpadding="1">
				<tr class="tblsfondo"> 
					<td colspan="2" align="middle" width="114">
						<b class="tbltext1a">
						    Gruppi&nbsp;Area&nbsp;Geografica
		        		</b>
					</td>
					<td align="middle" width="20">
			 		    <b class="tbltext1a">
					       Prv
					    </b>
					</td>
					<td align="middle" width="114">
						<b class="tbltext1a">
							Comune
						</b>
					</td>
					<td align="middle" width="50">
					    <b class="tbltext1a">
					       CAP
					    </b>
					</td>    
				</tr>
				<tr class="tblsfondo"> 
					<td colspan="2" align="middle" width="114" class="tblText3"><b>
            			<input type="text" class="textblack" maxlength="30" style="TEXT-TRANSFORM: uppercase;WIDTH: 153px" name="cmbGruppo" value="<%=sGruppo%>">
					</td>
					<td align="middle" width="20">
						<select NAME="cmbPrv" class="textblack" onchange="CaricaCom()">
							<option>&nbsp;</option>
<%
							if nProvSedi <> 0 then
							   pos_ini=1
							   for i=1 to nProvSedi/2
							      if sProv2 = mid(sMask,pos_ini,2) then
							          Response.Write "<option value='" & mid(sMask,pos_ini,2) & "' selected>" & mid(sMask,pos_ini,2) & "</option>"
							      else
							          Response.Write "<option value='" & mid(sMask,pos_ini,2) & "'>" & mid(sMask,pos_ini,2) & "</option>"
							      end if
							      pos_ini = pos_ini + 2
							   next
							end if
%>
						</select>
					</td>
					<td align="middle" width="114">
						<select name="cmbComune" class="textblack">
							<option>&nbsp;</option>
<%
							sSQL = "Select CODCOM, DESCOM " &_
							       "from COMUNE WHERE PRV ='" & sProv2 &_
							       "' ORDER BY DESCOM"
				
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
							set rsRec = CC.Execute(sSQL)
							if rsRec.EOF = false then
								 do while not rsRec.EOF
									 Response.Write "<OPTION "
									 Response.write "value ='" & rsRec.Fields("CODCOM") & _
										"'> " & rsRec.Fields("DESCOM")  & "</OPTION>"
									rsRec.MoveNext 
								 loop
							else
								  sAggiungi = false
								  Response.write "<OPTION value ='0'></OPTION>"
							end if 

						Response.Write "</SELECT>"
						rsRec.Close
%>
					</td>
					<td align="middle" width="50">
					    <input name="txtCAP" class="textblack" style="WIDTH: 41px; HEIGHT: 22px" size="5" maxlength="5" value="<%=sCAP%>">
					</td>
				</tr>
			</table>
			<input type="hidden" name="Prov2" value="<%=sProv2%>">
			<br>
			<table width="100" border="0">
				<tr> 
					<td align="center">
						<a HREF="CIM_VisAreaGeo.asp"><img SRC="<%=Session("Progetto")%>/images/indietro.gif" border="0">
					</td>
					<td>
						&nbsp;&nbsp;&nbsp;&nbsp;
					</td>
					<td align="center">
						<input type="image" src="<%=session("Progetto")%>/images/aggiungi.gif" border="0" value="Aggiungi" align="top" id="Aggiungi" name="Aggiungi">
					</td>
				</tr>
			</table>
	</form>
	<br>
<%
			sSQL =	"SELECT ID_BANDO, ID_AREAGEO, CAP, COMUNE" &_
					" FROM CI_AREAGEO" &_
					" WHERE ID_BANDO =" & sBando &_
					" AND ID_UORG=" & session("iduorg")
				
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
			set rsAreaGeo = CC.Execute(sSQL)
		
			If rsAreaGeo.Eof = false then
'###############################____INIZIO COSTRUZIONE____#########################################		
%>
				<form name="frmAreaGeoElimina" method="post" onsubmit="return ControllaDati2(this)" action="CIM_ElimAreaGeo.asp">
					<input type="hidden" name="txtTerr" value="<%=idArea%>"> 
					<table cellspacing="2" cellpadding="2" align="center" width="500" border="0">
						<tr class="sfondocomm" align="middle" width="500"> 
							<td width="20"></td>
							<td align="middle" valign="center" width="220"><b>Gruppi Area Geografica</b></td>
							<td valign="center" width="220"><b>Comune&nbsp;<b></td>
							<td valign="center" width="40"><b>CAP</b></td>
						</tr>
					</table>
					<table width="500" bordercolor="#000099" cellpadding="2" cellspacing="2" border="0">
						<input type="hidden" value="<%=sProv%>" name="txtPRV">
						<input type="hidden" name="Prov2" value="<%=sProv2%>">
						<input type="hidden" name="txtBando" value="<%=sBando%>">
<%
					n=0
					do while not rsAreaGeo.EOF
						n=n+1
%>
						<tr class="tblsfondo" width="500"> 
							<td align="middle" width="20">
								<span class="tbltext">
<%
								Dim Mix
								DIM GruppoAG
								dim ComuneAG
								dim CapAR
				
								Mix=""
								GruppoAG=rsAreaGeo("ID_AREAGEO")
								ComuneAG=rsAreaGeo("COMUNE")
								CapAR=rsAreaGeo("CAP")
								'chr(165): rappresenta il carattere di separazione �
								Mix=GruppoAG & chr(165) & ComuneAG & chr(165) & CapAR
%>
								<input type="checkbox" class="textblack" id="ckFlag" name="ckFlag<%=n%>" value="<%=mix%>">
								</span>
							</td>
							<td width="220" align="middle">
<%
								dim sDesc
								sDesc=Server.HTMLEncode(rsAreaGeo("ID_AREAGEO"))
%>
								<input type="hidden" class="textblack" value="<%=rsAreaGeo("ID_AREAGEO")%>" name="txtGruppo<%=n%>">
								<span class="tbltext">
								<%=sDesc%>
								</span>
							</td>
							<td width="220" align="middle">
								<input type="hidden" class="textblack" value="<%=rsAreaGeo("COMUNE")%>" name="txtComune<%=n%>">
								<span class="tbltext">
<%
								sSQL = " SELECT DESCOM FROM COMUNE WHERE CODCOM ='" &_
								       rsAreaGeo("COMUNE") & "'"
								      
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
								set rsComune = CC.Execute(sSQL)
								Response.Write rsComune("DESCOM")
								rsComune.Close
								
%>
								</span>
							</td>
							<td width="40" align="middle">
							  <input type="hidden" value="<%=rsAreaGeo("CAP")%>" name="txtCap<%=n%>">
             				  <span class="tbltext">
							      <%Response.Write rsAreaGeo("CAP")%>
							  </span>
							</td>
						</tr>
<%
			     rsAreaGeo.MoveNext
		         loop
		         rsAreaGeo.Close
%>
						<input type="hidden" value="<%=n%>" name="txtNumElementi">
					</table>
<%				if n > 0 then%>
					<br>
				    <table border="0" cellspacing="0" cellpadding="0" width="500">
						<tr>
							<td align="middle">
								<input type="image" src="<%=session("Progetto")%>/images/elimina.gif" border="0" value="Elimina" align="top" id="image1" name="image1">
						    </td>
					    </tr>
				    </table>
<%				else%>
			        <table border="0" cellspacing="1" cellpadding="1" width="500">
						<tr align="middle">
							<td class="tbltext3">
								Non � stata definita nessuna Area Geografica
						    </td>
					    </tr>
				    </table>
<%				end if %>
	          </form>	
<%          Else%>
	           <table border="0" cellspacing="1" cellpadding="1" width="500">
				  <tr align="middle">
					<td class="tbltext3">
						Non ci sono gruppi associati all'area geografica.
					</td>
				 </tr>
			   </table>
<%
	       End if
	  ELSE%>
			<table border="0" cellspacing="1" cellpadding="1" width="500">
				<tr align="middle">
					<td class="tbltext3">
						Non Esiste Nessun Bando.
					</td>
				</tr>
			</table>	
<%	
			sAggiungi = false
			rsRec.Close
	  END IF
%>
<!--#include virtual ="/include/CloseConn.asp"-->
<!--#include virtual = "/strutt_coda2.asp"-->
