<%'Option Explicit 
  ' Response.ExpiresAbsolute = Now() - 1 
  ' Response.AddHeader "pragma","no-cache"
  ' Response.AddHeader "cache-control","private"
  ' Response.CacheControl = "no-cache"
%>
<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #include virtual="/strutt_testa2.asp"-->
<!--#include virtual ="/include/openconn.asp"-->
<!--#include virtual ="/util/portallib.asp"-->
<%
'if ValidateService(session("idutente"),"SEL_INSDOMISCR", CC) <> "true" then 
'	response.redirect "/util/error_login.asp"
'end if
Session("menu")= ""
%>
<!-- ************** Javascript Inizio ************ -->
<!--include del file per fare i controlli sulle stringhe-->
<script LANGUAGE="Javascript" src="/Include/ControlString.inc"></script>
<!--include del file per fare i controlli sulla validit� delle date-->
<script LANGUAGE="Javascript" src="/Include/ControlDate.inc"></script>

<!--include del file per fare i controlli sulla numericit� dei campi-->
<script LANGUAGE="Javascript" src="/Include/ControlNum.inc"></script>

<!--include del file per fare i controlli sulla validit� del codice fiscale-->
<script LANGUAGE="Javascript" src="/Include/ControlCodFisc.inc"></script>

<script LANGUAGE="Javascript" src="/Include/help.inc"></script>
<script LANGUAGE="Javascript" src="/include/SelComune.js"></script>
<script LANGUAGE="Javascript" src="/ARTIGIANI/include/SelComuneRes.js"></script>
<script LANGUAGE="Javascript" src="Controlli.js"></script>
<script LANGUAGE="Javascript" src="ControlliIscr.js"></script>

<!-- ************** ASP Inizio *************** -->

<!--#include virtual = "/include/DecCod.asp"-->
<!--#include virtual = "/include/ControlDateVB.asp"-->
<!--#include virtual = "/util/dbutil.asp"-->


<%	
'-------------------------------------------------------------------------------------------------------------------------------
Sub Titolo(sTitolo,sDescTitolo) %>
    <table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
		<tr height="18">
			<td class="sfondomenu" width="67%" height="18"><span class="tbltext0"><b>&nbsp;<%=sTitolo%></b></span></td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
			<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
		</tr>
	</table>
	<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
		<tr>
			<td align="left" class="sfondocomm"><%=sDescTitolo%></td>
		</tr>
		<tr height="2">
			<td class="sfondocomm" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>
	<br>
<%
end sub

Sub Msgetto(Msg)

%>
	<br>
	<table align="center">
		<tr>
			<td class="tbltext3"><b><%=Msg%></b></td>
		</tr>	
		<tr>
			<td>&nbsp;</td>
		</tr>	
		<tr>
			<td align="center"><a HREF="javascript:history.back()"><img SRC="/ARTIGIANI/images/indietro.gif" border="0" WIDTH="55" HEIGHT="40"></a></td>
		</tr>
	</table>
<%
End Sub

'-------------------------------------------------------------------------------------------------------------------------------

sub Inizio()
%>
<script LANGUAGE="javascript" FOR="window" EVENT="onload">
	if (window.frmDomIscri.cbbAppartenenza.value=="03") {
		window.frmDomIscri.cbbDisocc.disabled=false;
		}		
//	window.frmDomIscri.cbbAppartenenza.value="";
//	window.frmDomIscri.cbbAppartenenza.text="";
//	window.frmDomIscri.cbbDisocc.value="";
//	window.frmDomIscri.cbbDisocc.text="";
</script>
	
	<br>
	<table cellpadding="0" cellspacing="0" width="500" border="0">
		<tr height="18">
			<td class="sfondomenu" height="18" width="67%">
			<span class="tbltext0"><b>&nbsp;DOMANDA DI ISCRIZIONE</b></span></td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
			<td valign="middle" align="right" class="tbltext1" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">(*) campi obbligatori</td>
		</tr>
		<tr>
			<td class="sfondocomm" width="57%" colspan="3">
				Modulo di iscrizione 
			<%if  sEsitoBando = "OK" then %>
			      per il bando&nbsp;<b><%=sDescBando%></b><br>  
			      Premere <b>Invia</b> per salvare. 
			<%end if%>
			<a href="Javascript:Show_Help('/Pgm/help/GestProgetti/Alfabeta/Selezione/SEL_InsDomIscr')" onmouseover="javascript:window.status=' '; return true"><img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
			</td>
		</tr>
		<tr height="2">
			<td colspan="3" class="sfondocomm" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>
	
	<form method="post" name="frmDomIscri" onsubmit="return ControllaDati(this)" action="SEL_CnfDomIscr.asp">
	<input type="hidden" name="COD_STDIS" value="<%=CodiceStato%>">
	<input type="hidden" name="txtScadenzaOld" value="<%=sSCadenza%>">
	<input type="hidden" name="txtoggi" value="<%=ConvDateToString(Date())%>">
<%
end sub

'------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

sub ImpPagina()
	
    dim sInt 
    
	       sCommento =  "Presta <b>molta attenzione</b> a quanto inserisci:<br>" &_ 
				        "Indica di seguito i tuoi dati anagrafici."  
				        
	       call Titolo("Informazioni Personali",sCommento)
	    %>
	   
		<table width="500" border="0" cellspacing="2" cellpadding="1">
		<tr>
			<td align="left" class="tbltext1" width="30%">
				<b>Cognome*</b>
			</td>
			<td align="left">
				<input style="TEXT-TRANSFORM: uppercase;" size="30" maxlength="30" class="textblack" name="txtCognome">
			</td>
	    </tr>
	    <tr>
			<td align="left" class="tbltext1" width="30%">
				<b>Nome*</b>
			</td>
			<td align="left">
				<input style="TEXT-TRANSFORM: uppercase;" size="30" maxlength="30" class="textblack" name="txtNome">
			</td>
	    </tr>

	    <tr>
			<td align="left" class="tbltext1" width="30%">
				<b>Data di Nascita*</b> (gg/mm/aaaa)
			</td>	
			<td align="left">
				<input style="TEXT-TRANSFORM: uppercase;" size="11" maxlength="10" class="textblack" name="txtDataNascita">
			</td>
	    </tr>
	    <tr><td>&nbsp;</td></tr>
	    <tr>
			<td align="left" class="textblack" colspan="2">
				Inserisci qui di seguito il comune e la provincia di nascita, se sei nato in Italia.
			</td>
	    </tr>
	    
		<tr>		
			<td align="left" class="tbltext1" width="30%">
				<b>Provincia di Nascita*</b>
			</td>			
			<td align="left">				
<%
			sInt = "PROV|0|" & date & "| |cmbProvinciaNascita' onchange='PulisciCom()|ORDER BY DESCRIZIONE"			
			CreateCombo(sInt)
						
%>
			</td>
		</tr>		
		<tr>		
			<td align="left" class="tbltext1" width="30%">
				<b>Comune di Nascita*</b>
			</td>
			<td nowrap>
				<span class="tbltext">
				<input type="text" name="txtComuneNascita" style="TEXT-TRANSFORM: uppercase;" class="textblack" size="35" readonly value="<%=DescrComuneNascita%>">
				<input type="hidden" id="txtComune" name="txtComune" value="<%=COM_NASC%>">
<%
				NomeForm="frmDomIscri"
				CodiceProvincia="cmbProvinciaNascita"
				NomeComune="txtComuneNascita"
				CodiceComune="txtComune"
				Cap="NO"
%>
				<a href="Javascript:SelComune('<%=NomeForm%>','<%=CodiceProvincia%>','<%=NomeComune%>','<%=CodiceComune%>','<%=Cap%>')" ID="imgPunto1" name="imgPunto1" onmouseover="javascript:window.status='';return true"><img border="0" src="<%=Session("Progetto")%>/images/bullet1.gif"></a>
				</span>				
			</td>
		</tr>		
		<tr>
			<td class="textblack" colspan="2">
				Per <b>i nati all'estero</b> invece compilare la nazione di nascita. 
			</td>
		</tr>
		
	    <tr>
			<td align="left" class="tbltext1" width="30%">
				<b>Nazione di Nascita</b>
	        </td>
			<td align="left">
				<%
				set rsComune = Server.CreateObject("ADODB.Recordset")
				sSQL = "SELECT CODCOM,DESCOM from COMUNE WHERE CODCOM like 'Z%' " &_
						"ORDER BY DESCOM"

				rsComune.Open sSQL, CC
				
				%>
				<select class="textblack" ID="cmbNazioneNascita" name="cmbNazioneNascita">
				<option></option>
				<%
				do while not rsComune.EOF 
					Response.Write "<OPTION "

					Response.write "value ='" & rsComune("CODCOM") & _
						"'> " & rsComune("DESCOM")  & "</OPTION>"
					
					rsComune.MoveNext 
				loop

				rsComune.Close	
				%>
				</select>
	        </td>
		</tr>
		<tr><td>&nbsp;</td></tr>
		<tr>
			<td align="left" class="tbltext1" width="30%">
				<b>Sesso*</b>
	        </td>
			<td align="left">
				<select class="textblack" id="cmbSesso" name="cmbSesso">
					<option selected>
					<option value="F">FEMMINILE
					<option value="M">MASCHILE
					</option>
				</select>
	        </td>
	  	</tr>
	  	<tr>
			<td align="left" class="tbltext1" width="30%">
				<b>Codice Fiscale*</b>
	        </td>
			<td align="left">
				<input style="TEXT-TRANSFORM: uppercase;" size="22" maxlength="16" class="textblack" name="txtCodFisc">
	        </td>
	    </tr>
		<tr>
			<td align="left" class="tbltext1" width="30%">
				<b>Cittadinanza*</b>
	        </td>
			<td align="left">
				<%	sInt = "STATO|0|" & date & "| |cmbCittadinanza|ORDER BY DESCRIZIONE"
					CreateCombo(sInt)	%>
	        </td>
	    </tr>		
		<tr>
			<td align="left" class="tbltext1" width="30%">
				<b>Posizione di Leva</b>
	        </td>
			<td align="left">
				<%
				sInt = "POMIL|0|" & date & "| |cmbLeva|ORDER BY DESCRIZIONE"			
				CreateCombo(sInt)
				%>
	        </td>
	    </tr>
		<tr>
			<td align="left" class="tbltext1" width="30%">
				<b>Stato Civile</b>
	        </td>
			<td align="left">
				<%
				sInt = "STCIV|0|" & date & "| |cmbStatoCiv|ORDER BY DESCRIZIONE"			
				CreateCombo(sInt)
				%>
	        </td>
	    </tr>
				
	    <tr>
			<td align="left" class="textblack" colspan="2">
			Indica un recapito telefonico presso cui gli operatori possono contattarti direttamente per comunicazioni urgenti.
			E' necessario scrivere di seguito tutti i numeri che lo compongono, senza aggiungere spazi o altri separatori.<br>
			Segnalaci il tuo indirizzo di posta elettronica personale, se lo hai, in modo da agevolare i contatti.
			</td>
	    </tr>
	    <tr>
			<td align="left" class="tbltext1" width="30%">
				<b>Recapito Telefonico*</b>
	        </td>
	        <td align="left">
				<input style="TEXT-TRANSFORM: uppercase;" size="25" maxlength="20" class="textblack" name="txtNumTel">
	        </td>
	    </tr>

	    <!--tr>			<td align="left" class="textblack" colspan="2">				Segnalaci il tuo indirizzo di posta elettronica personale, se lo hai, in modo da agevolare i contatti.			</td>	    </tr-->
	    <tr>
			<td align="left" class="tbltext1" width="30%">
	   			<b>E-Mail</b>
	        </td>
	        <td align="left">
		   		<input size="25" maxlength="30" class="textblack" name="txtEMail">
	        </td>
	    </tr>

	
	    <!--tr>			<td height="2" align="left" colspan="2" background="<%=Session("Progetto")%>/images/separazione.gif"></td>	    </tr-->

		<tr><td>&nbsp;</td></tr>
	    </table>
 <%
'	if trim(sCodTimpr) = "" then
' dovrebbe essere testato il solo codice 00

'17/05/2006 inizio
'	if trim(sCodTimpr) = "" or isnull(sCodTimpr) or sCodTimpr="00" or sCodTimpr="0" then
			%>
			  <!--input type="hidden" name="cmbEnteOcc" value="0">			  <input type="hidden" name="txtCodTimpr" value-->
			<%
'	else		
'			sSQL = "SELECT SI.ID_SEDE, SI.DESCRIZIONE, SI.PRV " & _
'					"FROM SEDE_IMPRESA SI, IMPRESA I " & _
'					"WHERE I.COD_TIMPR = '" & sCodTimpr & "' " & _
'					"AND I.ID_IMPRESA = SI.ID_IMPRESA " & _
'					"ORDER BY SI.PRV, SI.DESCRIZIONE"
'					
'					Response.Write sSQL
'				'	Response.END
'			set rsImpresa = Server.CreateObject("ADODB.Recordset")
'			rsImpresa.Open sSQL, CC, 3
'
'			if not rsImpresa.EOF then
'				
'				   call Titolo(sDescTimpr,"Seleziona la regione di residenza solo se vi risiedi da almeno sei mesi.")
				%>	    
    
		<!--table width="500" border="0" cellspacing="2" cellpadding="1">			<tr>				<td align="left" colspan="2" width="60%" nowrap>					<span class="tbltext">					<input type="hidden" name="cmbEnteOcc" value>					<input type="hidden" name="txtCodTimpr" value="<%'=sCodTimpr%>">						<input type="hidden" name="txtDescTimpr" value="<%'=sDescTimpr%>">																<textarea rows="4" cols="40" name="txtDescArea" readonly class="textblacka"></textarea>					<a href="Javascript:SelImpresa('<%'=sCodTimpr%>','<%'=sDescTimpr%>','SEDE_IMPRESA','ID_SEDE','txtImpresa','txtRagione','<%=nBando%>')" ID="imgPunto2" name="imgPunto2"><img border="0" src="<%=Session("Progetto")%>/images/bullet1.gif" onmouseover="javascript:window.status='';return true"></a>					</span>				</td>						</tr>		 </table>    <br-->
  <%	'end if
	'end if  
 
	       sCommento = "Specifica i tuoi dati di residenza soltanto se, al momento della domanda, risiedi in uno dei comuni indicati"
	       call Titolo("Residenza",sCommento)%>
	    
	    <table width="500" border="0" cellspacing="2" cellpadding="1">
	    <tr>
			<td align="left" class="tbltext1" width="30%">
				<b>Indirizzo*</b>
	        </td>
	        <td align="left">
				<input size="30" maxlength="50" name="txtIndirizzo" class="textblack" style="TEXT-TRANSFORM: uppercase;">
	        </td>
	    </tr>
			
	    <tr>
			<td align="left" class="tbltext1" width="30%">
				<b>Frazione</b>
	        </td>
	        <td align="left">
				<input size="30" maxlength="50" name="txtFrazione" class="textblack" style="TEXT-TRANSFORM: uppercase;">
	        </td>
	    </tr>
	    <tr>		
			<td align="left" class="tbltext1" width="30%">
				<b>Provincia di Residenza*</b>
			</td>			
			<td align="left">				
<%
	
		set rsProvRes = Server.CreateObject("ADODB.Recordset")

		sSQLProvRes = " SELECT distinct (prv) " &_
					" from COMUNE_PARCHI " 
				
			rsProvRes.Open sSQLProvRes, CC, 3

			if not rsProvRes.EOF then
				
			Response.Write "<Select class='textblack' name='cmbProvRes' onchange='PulisciRes()'"
					Response.Write ">" 
					Response.Write "<Option></Option>" 
				do while not rsProvRes.EOF
				sProv = DecCodVal("PROV", "0", "", rsProvRes("prv"), 1)
				
					Response.Write "<OPTION "
					Response.write "value ='" & rsProvRes("prv") & _
					"'> " & sProv  & "</OPTION>"
			
				 rsProvRes.movenext
				loop 
				Response.Write "</Select>" 
			end if
%>
			</td>
		</tr>		
		<!--tr>					<td align="left" class="tbltext1" width="30%">				<b>Provincia di Residenza*</b>			</td>						<td align="left"-->				
<%



	'		sInt = "PROV|0|" & date & "||cmbProvRes' onchange='PulisciRes()| ORDER BY DESCRIZIONE"			
	'		CreateCombo(sInt)	
%>
			<!--/td>		</tr-->		
		<tr>		
			<td align="left" class="tbltext1" width="30%">
				<b>Comune di Residenza*</b>
			</td>		
			
		
			<td nowrap>
				<span class="tbltext">
				<input type="text" name="txtComuneRes" style="TEXT-TRANSFORM: uppercase;" class="textblack" size="35" readonly value="<%=DescrComuneResid%>">
				<input type="hidden" id="txtComRes" name="txtComRes" value="<%=COM_RES%>">
<%
				NomeForm="frmDomIscri"
				CodiceProvincia="cmbProvRes"
				NomeComune="txtComuneRes"
				CodiceComune="txtComRes"
				Cap="txtCapRes"	

				
%>
				<a href="Javascript:SelComuneRes('<%=NomeForm%>','<%=CodiceProvincia%>','<%=NomeComune%>','<%=CodiceComune%>','<%=Cap%>')" ID="imgPunto1" name="imgPunto1" onmouseover="javascript:window.status='';return true"><img border="0" src="<%=Session("Progetto")%>/images/bullet1.gif"></a>
				</span>				
			</td>
		</tr>		
		<tr>
			<td align="left" nowrap class="tbltext1" width="30%">
				<b>C.A.P.*</b>
			</td>
			<td align="left">
				<input type="text" name="txtCapRes" class="textblack" value="<%=CAP_RES%>" size="5" maxlength="5">
			</td>
		</tr>	    
	    
	    <tr><td>&nbsp;</td></tr>
	</table>  
	<!-- ----------------------------------------------------------- -->
	   <%
	      sCommento = "Indica di seguito le informazioni relative all'ultimo titolo di studio conseguito." 
			           
	      call Titolo("Titolo di Studio Conseguito",sCommento)
	   %> 
	 <table width="500" border="0" cellspacing="2" cellpadding="1">
		<tr>
	        <td align="left" class="tbltext1" width="30%">
				<b>Livello di Studio*</b>
	        </td>
			<td align="left">
				<%
				'sInt = "LSTUD|0|" & date & "| |cmbLivStud' onchange='PulisciLivStud()|AND codice in ('LSP','DLA') ORDER BY DESCRIZIONE"			
				sInt = "LSTUD|0|" & date & "| |cmbLivStud' onchange='PulisciLivStud()|AND codice not in ('ELE','NES','SPC') ORDER BY DESCRIZIONE"			
				CreateCombo(sInt)
				%>
	        </td>
	    </tr>
	    <tr>
	        <td align="left" class="tbltext1" width="30%">
				<b>Titolo di Studio*</b>
	        </td>
			<td class="tbltext1">
				<input class="textblacka" readonly type="text" name="txtSpecifico" style="width=250px">
				<input type="hidden" name="txtTipoSpecifico">
				<a href="Javascript:SelSpecifico()" ID="imgPunto2" name="imgPunto2"><img border="0" src="<%=Session("Progetto")%>/images/bullet1.gif"></a>
			</td>
	    </tr>
		<tr>
	        <td align="left" class="tbltext1" width="30%">
				<b>Anno Rilascio Titolo* </b> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(aaaa)
			</td>
			<td align="left">
				<input style="TEXT-TRANSFORM: uppercase;" class="textblack" size="11" maxlength="4" name="txtAnnoRilascio">
			</td>
	    </tr>
	    <!--tr>			<td align="left" colspan="1" class="tbltext1">				<b>Voto*</b>			</td>			<td> <input class="MyTextBox" size="3" maxlength="3" name="txtVoto"> 			<b> / </b>			 <input class="MyTextBox" size="3" maxlength="3" name="txtBase">			</td>	    </tr-->
	    <tr><td>&nbsp;</td></tr>
		<!--tr>	        <td align="left" class="tbltext1" width="30%">				<b>Attualmente frequenta per conseguire</b>	        </td>	        <td align="left">				<%				'sInt = "LSTUD|0|" & date & "| |cmbAttIscri|AND codice<>'NES' ORDER BY VALORE"							'CreateCombo(sInt)				%>	        </td>	    </tr-->
	</table>
    <%	  
		'  sCommento = " Nella categoria 'Disoccupato', solo nel caso in cui nella 'Condizione di Appartenenza Dlgs. 181/2000' venga selezionato " 
		'  sCommento = sCommento & " 'Disoccupato di lunga durata', sara' possibile <br>indicare anche la 'Particolare categoria di appartenenza'" 	 
         
         sCommento = " Nella categoria <b>'Disoccupato'</b> specificare se si appartiene a una delle condizioni previste "
         sCommento = sCommento & " dal <b>Dlgs. 181/2000</b>. Nel caso si selezioni la voce 'Disoccupato di lunga durata', � possibile selezionare"
         sCommento = sCommento & " la particolare categoria di appartenenza (per es. <b>LSU, LPU, iscritto alle liste di mobilit�</b> ecc.)."	
		          
	      call Titolo("Stato Occupazionale",sCommento)
	%> 
    <table width="500" border="0" cellspacing="2" cellpadding="1">
    
     
    
    <%'------------DISOCCUPATO--------------------------------------------%>	    
		<!--tr>		<td colspan="2" align="left" class="tbltext1" width="500">			Nella categoria  <b>'Disoccupato'</b> specificare se si appartiene a una delle condizioni previste 			dal <b>Dlgs. 181/2000</b>. Nel caso si selezioni la voce <b>&quot;Disoccupato di lunga durata&quot;</b>, � possibile specificare			la particolare categoria di appartenenza (per es. <b>LSU, LPU, iscritto alle liste di mobilit�</b> ecc.)					</td>	</tr>		<tr>			<td>&nbsp;</td>		</tr-->
		<tr>
			<td class="tbltext0" valign="middle" align="left">
				<!--input type="radio" title="Selezionare una voce" name="chkStatOcc" value="Disoccupato" onClick="Javascript:ControllaRadio()"-->
				
				
				<span class="textblack">
				&nbsp;
				<b>Disoccupato:</b>
				<br><br>
				</span>
				
				
			</td>
		
		</tr>    
		<tr> <input type="hidden" name="txtStatOcc" value>
					</tr>
		
    
		<tr>
	        <td align="left" class="tbltext1" width="30%">
				&nbsp;
				<b>Condizione di <br> &nbsp; Appartenenza <br> &nbsp; Dlgs. 181/2000</b>
	        </td>
	       <td align="left">
			
<%			    sDisabilita =""
        
              '  sCondizione=" AND CODICE IN ('03','04') ORDER BY DESCRIZIONE"
              '	sInt = "ST181|0|" & date & "|" & sCategoria & "|cbbAppartenenza' " & sDisabilita & " onchange='ControllaDisocc()|" & sCondizione	
			
				sInt = "ST181|0|" & date & "|" & sCategoria & "|cbbAppartenenza' " & sDisabilita & " onchange='ControllaDisocc()|" 		
				CreateCombo(sInt)
	
%>																								
			</td>
		</tr>
		<tr>
			<td align="left" class="tbltext1">
				&nbsp;
					<strong>Dal</strong>&nbsp;(gg/mm/aaaa)
			</td>
			<td>
				<input type="text" id="txtDecDis" name="txtDecDis" value="<%=sDatDis%>" maxlength="10" class="textblacka" size="10" <%if s="2" and Stato<>"N" then Response.Write("disabled") end if%>>
			</td>
		</tr>
		
		<tr><td>&nbsp;</td></tr>
		<tr>
			 <td align="left" class="tbltext1" width="30%">
				&nbsp;
				<b>Particolare categoria<br> &nbsp; di appartenenza</b>
				
			</td>
			<td>
 <%			
                sDisabilita ="disabled"
                'sCondizione=" AND CODICE ='N05' ORDER BY DESCRIZIONE"
			    sCondizione=" AND CODICE LIKE 'N%' AND CODICE <>'N02' ORDER BY DESCRIZIONE"
               
				sInt = "STDIS|0|" & date & "|" & CodiceStato & "|cbbDisocc' " & sDisabilita & " width='10|" & sCondizione		
				CreateCombo(sInt)			

%>																
			</td>
		</tr>  
	<tr>
		<td>
		&nbsp;
		</td>
	</tr>		        
         
    </table>
    
    <%
    sCommento = " Nella categoria <b>'Lavoratore svantaggiato'</b> segnalare se si appartiene a una o pi� voci previste. "
      	          
	      call Titolo("Lavoratore svantaggiato",sCommento)
		
%> 
    <table width="500" border="1" cellspacing="2" cellpadding="1">
    
     
    
    <%'------------LAVORATORE SVANTAGGIATO-----------------------------%>	    
		<!--tr>		<td colspan="2" align="left" class="tbltext1" width="500">			Nella categoria  <b>'Disoccupato'</b> specificare se si appartiene a una delle condizioni previste 			dal <b>Dlgs. 181/2000</b>. Nel caso si selezioni la voce <b>&quot;Disoccupato di lunga durata&quot;</b>, � possibile specificare			la particolare categoria di appartenenza (per es. <b>LSU, LPU, iscritto alle liste di mobilit�</b> ecc.)					</td>	</tr>		<tr>			<td>&nbsp;</td>		</tr-->
		<tr>
			<td class="tbltext1" valign="middle" align="left" colspan="2">
				<!--input type="radio" title="Selezionare una voce" name="chkStatOcc" value="Disoccupato" onClick="Javascript:ControllaRadio()"-->
				
				
				<span class="textblack">
				&nbsp;
				<b>Descrizione</b>
				<br><br>
				</span>
				
				
			</td>
		
		</tr>  
		<% 
		
		set rsDecSvantaggio = Server.CreateObject("ADODB.Recordset")

		sSQLDecSvantaggio = " SELECT codice,decorrenza,scadenza,descrizione " &_
					" from DESC_LAV_SVANTAGGIO " &_
					" WHERE  " & nTime & " between decorrenza and scadenza "
			'	Response.Write sSQLDecSvantaggio
			rsDecSvantaggio.Open sSQLDecSvantaggio, CC, 3

		if not rsDecSvantaggio.EOF then
		%>
		
		<%	nCont = 0
			do while not rsDecSvantaggio.EOF
				sCodice  = rsDecSvantaggio("codice")
				sDescSvantaggio= rsDecSvantaggio("descrizione")
		'		Response.Write sDescSvantaggio & "Descrizione svantaggio"
				nCont = nCont + 1
		%>
		<tr>
			<td class="tbltext1" valign="middle" align="left"><br>
			<%=sDescSvantaggio%>	<br>
			</td>
			<td class="tbltext1" valign="middle" align="left"><br>
				<input type="hidden" name="txtCodiceSvant<%=nCont%>" value="<%=sCodice%>">
				<input type="checkbox" id="chkSvantaggio<%=nCont%>" name="chkSvantaggio<%=nCont%>" value="S">
				
			<br>
			</td>
		</tr>
		
		 <%	 
			
			rsDecSvantaggio.movenext
			loop
		 end if
		 %>
		 <input type="hidden" name="Contatore" value="<%=nCont%>"> 
</table>
<!-- ----------------------------------------------------------- -->	    

	<!--table width="500" border="0" cellspacing="2" cellpadding="1"-->
		<!--tr>			<td height="2" align="left" colspan="6" background="<%=Session("Progetto")%>/images/separazione.gif"></td>	    </tr-->
	    
	    <!--tr><td>&nbsp;</td></tr>	</table-->    
	    <%
'	        sCommento = "Indica di seguito il tuo livello di conoscenza nei seguenti argomenti." 
			            
 '           call Titolo("Conoscenze",sCommento)
	    %>
	    <!--tr>	        <td align="left" class="tbltext3" colspan="6">	        	<b>Conoscenze</b>			</td>				    </tr>	    <tr>			<td align="left" class="textblack" colspan="6">			Indica di seguito il tuo livello di conoscenza nei seguenti argomenti.			Questa informazione ci permettera' di comporre una classe omogenea per			la fase di formazione in aula.			</td>	    </tr-->
	    
	<!--table width="500" border="0" cellspacing="2" cellpadding="1">		<tr class="sfondocomm">		    <td align="center" nowrap>				<b>Argomento</b>			</td>			<td align="center">				<b>Nulla</b>			</td>			<td align="center">				<b>Suff.</b>			</td>			<td align="center">				<b>Discreta</b>			</td>			<td align="center">				<b>Buona</b>			</td>			<td align="center">				<b>Ottima</b>			</td>	    </tr>		    	    <tr class="tblsfondo">			<td align="left">				<p align="left">					<span class="tbltext">&nbsp;&nbsp;&nbsp;INGLESE</span>				</p>	        </td>	        <td align="left">				<p align="center">					<input checked type="radio" id="optInglese" name="optInglese" value="N">				</p>	        </td>			<td align="left">				<p align="center">					<input type="radio" id="optInglese" name="optInglese" value="S">				</p>	        </td>	        <td align="left">				<p align="center">					<input type="radio" id="optInglese" name="optInglese" value="D">				</p>	        </td>	        <td align="left">				<p align="center">					<input type="radio" id="optInglese" name="optInglese" value="B">				</p>	        </td>	        <td align="left">				<p align="center">					<input type="radio" id="optInglese" name="optInglese" value="O">				</p>	        </td>	    </tr-->
			
		<!--tr class="tblsfondo">	    	<td align="middle">				<p align="left">					<span class="tbltext">&nbsp;&nbsp;&nbsp;OFFICE AUTOMATION</span>				</p>	        </td>	        <td align="left">				<p align="center">					<input CHECKED type="radio" id="optOffice" name="optOffice" value="N">				</p>	        </td>	        <td align="left">				<p align="center">					<input type="radio" id="optOffice" name="optOffice" value="S">				</p>	        </td>	        <td align="left">				<p align="center">					<input type="radio" id="optOffice" name="optOffice" value="D">				</p>	        </td>	        <td align="left">				<p align="center">					<input type="radio" id="optOffice" name="optOffice" value="B">				</p>	        </td>	        <td align="left">				<p align="center">					<input type="radio" id="optOffice" name="optOffice" value="O">				</p>	        </td>	    </tr-->
		    
	    <!--tr class="tblsfondo">			<td align="middle">				<p align="left">					<span class="tbltext">&nbsp;&nbsp;&nbsp;SISTEMI OPERATIVI</span>				</p>	        </td>	        <td align="left">				<p align="center">					<input CHECKED type="radio" id="optSistemi" name="optSistemi" value="N">				</p>	        </td>	        <td align="left">				<p align="center">					<input type="radio" id="optSistemi" name="optSistemi" value="S">				</p>	        </td>	        <td align="left">				<p align="center">					<input type="radio" id="optSistemi" name="optSistemi" value="D">				</p>	        </td>	        <td align="left">				<p align="center">					<input type="radio" id="optSistemi" name="optSistemi" value="B">				</p>	        </td>	        <td align="left">				<p align="center">					<input type="radio" id="optSistemi" name="optSistemi" value="O">				</p>	        </td>	    </tr>		    	    <tr class="tblsfondo">			<td align="middle">				<p align="left">					<span class="tbltext">&nbsp;&nbsp;&nbsp;LINGUAGGI PROGRAMMAZIONE</span>				</p>	        </td>	        <td align="left">				<p align="center">					<input CHECKED type="radio" id="optProg" name="optProg" value="N">				</p>	        </td>	        <td align="left">				<p align="center">					<input type="radio" id="optProg" name="optProg" value="S">				</p>	        </td>	        <td align="left">				<p align="center">					<input type="radio" id="optProg" name="optProg" value="D">				</p>	        </td>	        <td align="left">				<p align="center">					<input type="radio" id="optProg" name="optProg" value="B">				</p>	        </td>	        <td align="left">				<p align="center">					<input type="radio" id="optProg" name="optProg" value="O">				</p>	        </td>	    </tr>		    	    <tr class="tblsfondo">			<td align="middle">				<p align="left">					<span class="tbltext">&nbsp;&nbsp;&nbsp;RETI</span>				</p>	        </td>	        <td align="left">				<p align="center">					<input CHECKED type="radio" id="optReti" name="optReti" value="N">				</p>	        </td>	        <td align="left">				<p align="center">					<input type="radio" id="optReti" name="optReti" value="S">				</p>	        </td>	        <td align="left">				<p align="center">					<input type="radio" id="optReti" name="optReti" value="D">				</p>	        </td>	        <td align="left">				<p align="center">					<input type="radio" id="optReti" name="optReti" value="B">				</p>	        </td>	        <td align="left">				<p align="center">					<input type="radio" id="optReti" name="optReti" value="O">				</p>	        </td>	    </tr>		    	    <tr class="tblsfondo">			<td align="middle">				<p align="left">					<span class="tbltext">&nbsp;&nbsp;&nbsp;INTERNET</span>				</p>	        </td>	        <td align="left">				<p align="center">					<input CHECKED type="radio" id="optInternet" name="optInternet" value="N">				</p>	        </td>	        <td align="left">				<p align="center">					<input type="radio" id="optInternet" name="optInternet" value="S">				</p>	        </td>	        <td align="left">				<p align="center">					<input type="radio" id="optInternet" name="optInternet" value="D">				</p>	        </td>	        <td align="left">				<p align="center">					<input type="radio" id="optInternet" name="optInternet" value="B">				</p>	        </td>	        <td align="left">				<p align="center">					<input type="radio" id="optInternet" name="optInternet" value="O">				</p>	        </td>	    </tr>	    <tr></tr-->
        <!--tr>			<td height="2" align="left" colspan="6" background="<%=Session("Progetto")%>/images/separazione.gif"></td>	    </tr-->
	<!--/table-->
	<br> 

<!-- ----------------------------------------------------------- -->	    	    
	<% 
	    call Titolo("Login E Password","Inserisci di seguito una Login ed una password.")
	%>
	
	<table width="500" border="0" cellspacing="2" cellpadding="1">     
        <tr>
	        <td align="left" class="tbltext1" width="30%">
					<b>Login*</b>
	        </td>
	        <td align="left">
	             <input size="34" class="textblack" maxlength="15" name="txtLogin" style="TEXT-TRANSFORM: uppercase;">
	        </td>
	    </tr>
	    <tr>
	        <td align="left" class="tbltext1" width="30%">
					<b>Password*</b>
	        </td>
	        <td align="left">
	             <input type="password" size="34" class="textblack" maxlength="15" name="txtPassword" style="TEXT-TRANSFORM: uppercase;">
	        </td>
	    </tr>
	    <tr>
	        <td align="left" class="tbltext1" width="30%">
					<b>Conferma Password*</b>
	        </td>
	        <td align="left">
	             <input type="password" size="34" class="textblack" maxlength="15" name="txtConfPassword" style="TEXT-TRANSFORM: uppercase;">
	        </td>
	    </tr>  
	</table>
	<br>
	<table width="520">		
	<tr>
		<td colspan="2" align="center" class="tbltext1" width="500">
			<!--b>Consenso ai sensi dell'art.11 della legge 31 dicembre 1996<br> n. 675/96</b><br><br-->
			<b>Consenso ai sensi dell'art.13 del D. Lgs. <br> 196/2003 </b><br><br>
			Consenso relativo alla legge sulla privacy: vi chiediamo gentilmente di leggere l'informativa relativa 
			alla privacy riportata di seguito e di esprimere<br>il 
			vostro consenso o meno al trattamento dei dati.
		</td>
	</tr>
	<tr>
		<td align="middle" colspan="2">&nbsp;
		</td>
   </tr>
   </table>
<table width="520">   
	<tr>
		<td colspan="2" class="tbltext"><p align="justify">
		
<%
			on error resume next 
			PathFileEdit = "/Testi/Autorizzazioni/info_privacy.htm"
			Server.Execute(PathFileEdit)
			If err.number <> 0 Then
				Response.Write "Errore nella visualizzazione. "
				Response.Write "Contattare il Gruppo Assistenza Portale Italia Lavoro "
				Response.Write "all'indirizzo po-assistenza@italialavoro.it "
			End If				
%>
			</p>
		</td>
	</tr>
</table>

<table>	
	<tr>
		<td colSpan="2"><br>
			<center>
			<input type="radio" value="0" id="radConsenso" name="radConsenso"><span class="tbltext">ACCETTO</span>
			<input type="radio" value="1" CHECKED id="radConsenso" name="radConsenso"><span class="tbltext">NON ACCETTO</span>
			<input type="hidden" name="sControllo" value="2">
			</center>
		</td>
	</tr>
</table>
	<br><br>

	<table cellpadding="0" cellspacing="0" width="500" border="0">	
		<tr align="center">
			<td nowrap align="right"><input type="image" name="Conferma" src="<%=Session("progetto")%>/images/conferma.gif"></td value="Conferma">
			<td nowrap align="center"><a href="javascript:document.frmDomIscri.reset()"><img border="0" src="<%=Session("progetto")%>/images/annulla.gif"></td>
			<td nowrap align="left"><a HREF="javascript:history.back()"><img SRC="<%=Session("Progetto")%>/images/indietro.gif" border="0"></td>
		    <td> 
				<input type="hidden" name="graduatoria" value="<%=sGraduatoria%>">
				<input type="hidden" name="gruppo" value="<%=sGruppo%>">
	           	               
	            <input type="hidden" name="IniIsc" value="<%=sDataIsc%>">
	            <input type="hidden" name="datasis" value="<%=sDataSis%>">
	            <input type="hidden" name="bando" value="<%=nBando%>">
	            <input type="hidden" name="descbando" value="<%=sDescBando%>">
	        </td>
		</tr>
	</table>
</form>

<%
end sub

'------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

function ctrlProj()
	ctrlProj = ""
	sSQL = "SELECT COD_TIMPR FROM PROGETTO WHERE ID_PROJ = " & CLng(nIdProgetto)
	set rsProj = CC.Execute(sSQL)
	
	if rsProj.EOF then
		Msgetto("Progetto inesistente")	
		ctrlProj = "KO"
	else
		sCodTimpr = rsProj("COD_TIMPR")
		ctrlProj = "OK"
	end if
	
end function

'------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

sub ctrlBando()

	'	Se la data di sistema non � compresa nel periodo di 
	'   acquisizione domande non devo caricare la domanda.
	set rsAbil = Server.CreateObject("ADODB.Recordset")

	sSQL = " SELECT B.ID_PROJ, B.DESC_BANDO, B.IDGRUPPO, P.IND_GRAD  FROM BANDO B, PROGETTO P " & _
		   "WHERE B.ID_PROJ = P.ID_PROJ "	 &_
			"AND B.ID_BANDO = "& nBando & " AND " & nTime & _
			" BETWEEN B.DT_INI_ACQ_DOM AND B.DT_FIN_ACQ_DOM"
	
	'-----------------------------------------------------------------------------
	' Questo controllo � stato inserito per permettere l'iscrizione di nominativi
	' anche a bando(sport2job) chiuso tale operazione puo essere eseguita solo dagli
	' utenti appartenenti al gruppo ISCR tramite la funzione ISCRIZIONE
	if ucase(nBypassdata) = "SI" then
		sSQL = " SELECT B.ID_PROJ, B.DESC_BANDO, B.IDGRUPPO, P.IND_GRAD  FROM BANDO B, PROGETTO P " & _
			   " WHERE B.ID_PROJ = P.ID_PROJ "	 &_
			   " AND B.ID_BANDO = "& nBando
				
	end if
	'Response.Write "sSQL " & sSQL
    '----------------------------------------------------------------------------
   	rsAbil.Open sSQL, CC, 3

	if  rsAbil.EOF  then
		Msgetto("Periodo di Acquisizione domande non valido")
		sEsitoBando = "KO"
	else
		sDescBando = rsAbil("DESC_BANDO")
	    nIdProgetto = rsAbil("ID_PROJ")
	    sEsitoBando = "OK"
	    sGruppo =rsAbil("IDGRUPPO")
	    sGraduatoria =rsAbil("IND_GRAD")
	end if  
	rsAbil.close
	set rsAbil = nothing	 

end sub

'------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

sub Fine()
%>
<!--#include virtual = "/include/closeconn.asp"-->
<%if session("login") = "" then%>
	<!--#include virtual = "/ARTIGIANI/struttura_codaART1.asp"-->
<%else%>
	<!--#include virtual = "/strutt_coda1.asp"-->
<%end if%>		
<%
end sub
%>
<!-- ************** ASP Fine *************** -->

<!-- ************** MAIN Inizio ************ -->
<%

dim sDataIsc
dim sSQL
dim CnConn
dim rsAbil

dim nBando
dim nIdProgetto
dim sDescBando
dim sEsitoBando

dim sCodTimpr
dim sDescTimpr

dim sGraduatoria
dim sGruppo, sCommento
dim sCondizione, sCategoria, sDisabilita

dim sProv, nCont , sCodice, sDescSvantaggio

sDataIsc = Now()

nBando=clng(Request("txtIdBando"))
'nTime = convdatetodb(Now())
nTime = convdatetodb(Date)
nBypassdata = Request("Bypassdata")

ctrlBando()
if  sEsitoBando = "OK" then
    Inizio()

	if Trim(ctrlProj()) = "OK" then
		sDescTimpr = DecCodVal("TIMPR", "0", "", sCodTimpr, 1)  
		sDataSis = ConvDateToString(Now())
		ImpPagina()
	end if
end if

Fine()
%>
<!-- ************** MAIN Fine ************ -->