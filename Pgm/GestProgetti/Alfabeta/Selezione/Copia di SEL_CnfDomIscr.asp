<%'Option Explicit
  Response.ExpiresAbsolute = Now() - 1 
  Response.AddHeader "pragma","no-cache"
  Response.AddHeader "cache-control","private"
  Response.CacheControl = "no-cache"
%> 
<!--#include virtual="/progettomda/strutt_testaMDA2.asp"-->
<!--#include virtual ="/include/openconn.asp"-->
<!--#include virtual="/util/portallib.asp"-->
<!--#include virtual ="/progettomda/include/ContrRegioneProvincia.asp"-->
<%
'if ValidateService(session("idutente"),"SEL_CNFDOMISCR", CC) <> "true" then 
'	response.redirect "/util/error_login.asp"
'end if
%>
<!-- ************** Javascript Inizio ************ -->
<script LANGUAGE="javascript">

function VaiInizio(strBando){
	location.href = "SEL_InsDomIscr.asp?txtIdBando=" + strBando;
}	
</script>
<!-- ************** Javascript Fine ************ -->
<!-- ************** ASP inizio *************** -->
<!--#include virtual = "/include/SysFunction.asp"-->
<!--#include virtual = "/include/ControlDateVB.asp"-->
<!--#include virtual = "/util/dbutil.asp"-->
<%
'*************************************************************
sub MessaggioErrori(DescrizioneErrore) 
%>

<br>
<table cellpadding="0" cellspacing="0" width="500" border="0">
		<tr height="18">
			<td class="sfondomenu" height="18" width="67%">
			<span class="tbltext0"><b>&nbsp;DOMANDA DI ISCRIZIONE</b></span></td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
			<td valign="middle" align="right" class="tbltext1" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
		</tr>
		<tr>
			<td class="sfondocomm" width="57%" colspan="3">
				Modulo di iscrizione 
			    per il bando&nbsp;<b><%=sDescBando%></b><br>  
			</td>
		</tr>
		<tr height="2">
			<td colspan="3" class="sfondocomm" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>
<br><br>

   	 <table border="0" cellspacing="1" cellpadding="1" width="370">
   	   <tr>
   		  <td class="tbltext3" align="center">Inserimento della Domanda d'Iscrizione non effettuabile.</td>
   	  </tr>
   	<tr>
   		<td class="tbltext3" align="center"><%Response.Write (DescrizioneErrore)%></td>
   	</tr>
   	</table>
   	<br><br>
				
   	<table border="0" cellpadding="0" cellspacing="1" width="370">
   		<tr>
   		<td align="center" colspan="2" width="60%"><b>
   		
   			<a HREF="javascript:history.go(-1)"><img SRC="<%=Session("Progetto")%>/images/indietro.gif" border="0" onmouseover="javascript:window.status=' '; return true"></a>
   			   			
   			</b>
   		</td>
   		</tr>
   	</table> 
 


<!--<br><br>		<table width="500" border="0" cellspacing="2" cellpadding="1">			<tr align="middle">				<td class="tbltext">					<b>Inserimento della Domanda d'Iscrizione non effettuabile.</b>				</td>			</tr>			<tr align="middle">				<td class="tbltext"><b><%=DescrizioneErrore%></b></td>			</tr>		</table><br><br><table width="500" border="0" cellspacing="2" cellpadding="1">	<tr align="center">		<td nowrap><a href="javascript:VaiInizio()"><img src="<%=session("Progetto")%>/images/indietro.gif" border="0" name="imgPunto2" onmouseover="javascript:window.status=' '; return true"></a></td>	</tr></table>   -->
<%
end sub

 	
   

'*************************************************************
sub AcquisizioneParametri()
'----------------------Cognome*
sCognome= Trim(UCase(Replace(Request("txtCognome"),"'","''")))
'----------------------Nome*
sNome= Trim(UCase(Replace(Request("txtNome"),"'","''")))
'----------------------Data di Nascita*
sDataNascita = ConvDateToDb(ConvStringToDate(Request("txtDataNascita")))
'----------------------Provincia di Nascita
sProvNascita = UCase(Request("cmbProvinciaNascita"))
'---------------------Comune di Nascita*
sComuneNascita = Trim(UCase(Request("txtComune")))
'---------------------Nazione di Nascita
sNazNascita = UCase(Request("cmbNazioneNascita"))
'---------------------Sesso*
sSesso = UCase(Request("cmbSesso"))
'---------------------Codice Fiscale*
sCodFisc = Trim(UCase(Request("txtCodFisc")))
'---------------------Cittadinanza*
sCittad = UCase(trim(Request("cmbCittadinanza")))
'---------------------Posizione di leva	
sPosLeva = UCase(Request("cmbLeva"))
'---------------------Stato Civile
sStCiv = UCase(Request("cmbStatoCiv"))
'---------------------Categoria Protetta
'sCategProt = trim(UCase(Request("cmbCategProt")))
'sDataRicCatProt = trim(Request("txtDtIniCatProt"))
'---------------------Recapito Telefonico*
sTelef = UCase(Request("txtNumTel"))
'---------------------E-MAIL
sEMail = Request("txtEMail")
sEMail = Trim(sEMail)
'---------------------INDIRIZZO*
sIndir = Replace(Trim(UCase(Request("txtIndirizzo"))),"'","''")
'---------------------FRAZIONE
sFraz = UCase(Request("txtFrazione"))
sFraz = Replace(sFraz,"'","''")
sFraz = Trim(sFraz)
'---------------------Provincia di Residenza*
sProv = UCase(Request("cmbProvRes"))
'---------------------Comune di Residenza*
sComune = Trim(UCase(Request("txtComRes")))
'---------------------C.A.P.*
sCap = UCase(Request("txtCAP"))
'----------------------Iscritto c/o il Centro Impiego di*
'sNumCentroImp = UCase(Request("cmbNumCentroImp"))
'----------------------Iscrizione(mm/aaaa)
'if Request ("txtDataIscri") <> "" then
'	sStatoDis = "01"                                     'Stato Disoccupazione
'	sDataIscri = "01/" & UCase(Request ("txtDataIscri")) 'Iscrizione(mm/aaaa)
'else
'	sStatoDis = UCase(Request ("cmbStatoDis"))           'Stato Disoccupazione
'	sDataIscri = "01/" & UCase(Request("txtDataDis"))   'Decorrenza (mm/aaaa)
'end if
'----------------------Titolo di Studio*
 sTitStud = UCase(trim(Request("txtTipoSpecifico")))
 sLivStud = UCase(trim(Request("cmbLivStud")))
 '---------------------Anno Rilascio Titolo* (di Studio)
 sAnnoTit = Request("txtAnnoRilascio")
 '--------------------- Voto Titolo* (di Studio)
 sVoto = Trim(Request("txtVoto"))
 'sVoto = trim(Replace(server.HTMLEncode(sVoto), "'", "''"))
 sBase = Trim(Request("txtBase"))
 '---------------------Attualmente iscritto a (corso di studo)
 ' sAttIscr = UCase(trim(Request ("cmbAttIscri")))

 '----------------------Occupazione
 sTempoIndet = Request("cbbTempIndet")
 sScadenza = trim(Request("txtScadenza"))
  '----------------------Disoccupazione
sAppartenenza = Request("cbbAppartenenza")
sDisocc = trim(Request("cbbDisocc"))
sDecorrenza = trim(Request("txtDecDis"))
nTotChkLavSvant = Request("Contatore")


if sDisocc = "" then
	sDisocc="N02"
end if
'------------------- Attualmente Iscritto a (Ente Occupazionale)
'nNumEnteOcc = trim(Request("cmbEnteOcc"))	
'Response.Write "ente occ " & nNumEnteOcc

'--------------------Login e Password*
sLogin = UCase(Request.Form ("txtLogin"))
sLogin = Replace(server.HTMLEncode(sLogin), "'", "''")

sPassword = UCase(Request.Form ("txtPassword"))
sPassword = Replace(server.HTMLEncode(sPassword), "'", "''")
'-------------------------- Hidden
sGraduatoria=Ucase(Request("graduatoria"))
sIniIsc = Request("IniIsc")
nBando = clng(Request("bando"))
sDescBando = Request("descbando")
sGruppo=Request("gruppo")
if sGruppo="" or sGruppo="0" then
	sGruppo="1"
end if
sDataSis = Request("datasis")
'------------------------------------
'------------------------Conoscenze
						
'select case UCase(Request ("optOffice"))
'	case "S"
'		sGrado(1) = 1
'	case "D"
'		sGrado(1) = 2
'	case "B"
'		sGrado(1) = 3
'	case "O"
'		sGrado(1) = 4
'	case else
'		sGrado(1) = 0
'end select	

select case UCase(Request ("optSistemi"))
	case "S"
		sGrado(2) = 1
	case "D"
		sGrado(2) = 2
	case "B"
		sGrado(2) = 3
	case "O"
		sGrado(2) = 4
	case else
		sGrado(2) = 0
end select	
							
select case UCase(Request ("optProg"))
	case "S"
		sGrado(3) = 1
	case "D"
		sGrado(3) = 2
	case "B"
		sGrado(3) = 3
	case "O"
		sGrado(3) = 4
	case else
		sGrado(3) = 0
end select	
							
select case UCase(Request ("optReti"))
	case "S"
		sGrado(4) = 1
	case "D"
		sGrado(4) = 2
	case "B"
		sGrado(4) = 3
	case "O"
		sGrado(4) = 4
	case else
		sGrado(4) = 0
end select	
							
select case UCase(Request ("optInternet"))
	case "S"
		sGrado(5) = 1
	case "D"
		sGrado(5) = 2
	case "B"
		sGrado(5) = 3
	case "O"
		sGrado(5) = 4
	case else
		sGrado(5) = 0
end select	

select case UCase(Request ("optInglese"))
	case "S"
		sGrado(6) = 1
	case "D"
		sGrado(6) = 2
	case "B"
		sGrado(6) = 3
	case "O"
		sGrado(6) = 4
	case else
		sGrado(6) = 0									
end select
'------------------------------------

set rsProvSede = Server.CreateObject("ADODB.Recordset")

sSQLProvSede = " SELECT distinct (id_sede) " &_
			" from COMUNE_PARCHI " &_
			" where CODCOM = '" & sComune & "'"
				
rsProvSede.Open sSQLProvSede, CC, 3

if not rsProvSede.EOF then
	sSede = rsProvSede("id_sede")		
end if
'Response.Write "sSede" &  sSede
'Response.End

end sub

'*************************************************************
sub ctrlCodiceFiscaleDomandaIscrDuplicato()
prgEsitoPersona = "OK"
set rsCodFisc = Server.CreateObject("ADODB.Recordset")

													
sSQL = "SELECT ID_PERSONA FROM PERSONA WHERE COD_FISC ='" & sCodFisc & "'"
	
rsCodFisc.open sSQL, CC, 3
	
if  rsCodFisc.EOF then 
    rsCodFisc.Close 
	set rsCodFisc = nothing
    exit sub
end if

sIDPers = rsCodFisc("ID_PERSONA")
'Response.Write "IDPERSONA=" & sIDPers & "<BR>"
sPersona = sIDPers

rsCodFisc.Close 
set rsCodFisc = nothing

set rsDomanda = Server.CreateObject("ADODB.Recordset")
											
'sSQL = "SELECT DT_DOM FROM DOMANDA_ISCR " & _
'       "WHERE ID_PERSONA = " & sIDPers & " AND ID_BANDO =" & nBando

 sSQL = "SELECT DT_DOM,DESC_BANDO" &_
		" FROM DOMANDA_ISCR A, BANDO B, PROGETTO C" &_
		" WHERE ID_PERSONA = " & sIDPers &_
		" AND A.ID_BANDO=B.ID_BANDO" &_
		" AND B.ID_PROJ=C.ID_PROJ" &_
		" AND AREA_WEB='" & mid(Session("Progetto"),2) & "'"

'Response.Write sSQL & "<BR>"	

rsDomanda.open sSQL, CC, 3

if rsDomanda.EOF  then 
   prgEsitoPersona = "NODOMI"
   rsDomanda.Close
   set rsDomanda = nothing
   exit sub   
end if
prgEsitoPersona = "KO"

sDtDom = ConvDateToString(rsDomanda("DT_DOM"))
sDecod=rsDomanda("DESC_BANDO")

'Response.Write "DTDOM=" & sDtDom & "<BR>"

rsDomanda.Close
set rsDomanda = nothing
'MessaggioErrori("Risulti essere gi� iscritto al Bando " & sDescBando & " in data " & sDtDom )
MessaggioErrori("Risulti essere gia' iscritto al Bando " & sDecodBando & " in data " & sDtDom )

end sub

sub InserimentoPersona()

sSQL = "INSERT INTO PERSONA " & _
				  "(COGNOME," &_
					"NOME," &_
					"DT_NASC," &_
					"COM_NASC," &_
					"PRV_NASC," &_
					"STAT_NASC," &_
					"SESSO," &_
					"POS_MIL," &_
					"COD_FISC," &_
					"STAT_CIV," &_
					"STAT_CIT," &_
					"IND_RES," &_
					"FRAZIONE_RES," &_
					"COM_RES," &_
					"PRV_RES," &_
					"CAP_RES," &_
					"NUM_TEL," &_
					"E_MAIL," &_
					"DT_TMST) VALUES " &_	
					"('" & sCognome  & _
					"','" & sNome & _
					"'," & sDataNascita & _
					",'" & sComuneNascita & _
					"','" & sProvNascita & _
					"','" & sNazNascita & _
					"','" & sSesso & _
					"','" & sPosLeva & _
					"','" & sCodFisc & _
					"','" & sStCiv & _
					"','" & sCittad & _
					"','" & sIndir & _
					"','" & sFraz & _
					"','" & sComune & _
					"','" & sProv & _
					"','" & sCap & _
					"','" & sTelef & _
					"','" & sEMail & _
					"'," & ConvDateToDb(Now()) & ")"

  Errore=EseguiNoC(sSQL,CC)	  
  if Errore <> "0" then
     MessaggioErrori("(Persona) " & Errore)
  end if
end sub

sub AcquisizioneIdPersona()
    'Accedo alla tabella PERSONA per prelevare Id_Persona e poter poi
    ' fare la INSERT nella tabella successiva

	set rsPers = Server.CreateObject("ADODB.Recordset")

	sSQL = "SELECT ID_PERSONA FROM PERSONA WHERE COD_FISC='" & sCodFisc & "'"
			
	rsPers.Open sSQL,CC
'	Response.Write "query " & sSQL
	IF not rsPers.EOF then
         sPersona = clng(rsPers("ID_PERSONA"))
	else
	     sPersona = 0
	     MessaggioErrori("Identificativo della Persona non trovato")
	end if	
				
    rsPers.Close
    set rsPers = nothing
end sub

sub InserimentoUtente()
  dim sCreator
  sEsitoUtente="OK"
  set rsLogin = Server.CreateObject("ADODB.Recordset")
     sSQL = "SELECT LOGIN FROM UTENTE WHERE LOGIN ='" & sLogin & "'"
    
     rsLogin.open sSQL,CC,3
    
     if not rsLogin.EOF then 
         MessaggioErrori("Login gia' presente.")
         sEsitoUtente="KO"				
     else
         sCreator = sPersona
         sUser=AddUserGroup(sNome,sCognome,sTipo,sLogin,sPassword,sEMail,sCreator,sGruppo,mid(session("progetto"),2),"", cc)
	     if  sUser= false then
	         sEsitoUtente="KO"
	         visErrore("Inserimento Utente fallito")
	     end if
	 end if
	 rsLogin.Close 
	 set  rsLogin = nothing  
	 if sEsitoUtente <> "OK" then
	     CC.RollbackTrans 
	 end if
end sub  



sub InserimentoDichiaraz()    
    sEsitoDichiaraz ="OK"
   
 
    sSQL = "INSERT INTO DICHIARAZ "   &_
           "(ID_PERSONA,"             &_ 
            "AUT_TRAT_DATI,"          &_             
            "DT_TMST) VALUES ("       &_
	        sPersona & ","            &_ 
	        "'S',"                    &_	      
	        Convdatetodb(Now()) & ")"
    'Response.Write sSQL
    
    Errore=EseguiNoC(sSQL,CC)
    
    if Errore <> "0" then
       CC.RollbackTrans
	   MessaggioErrori("(Dichiaraz) " & Errore)
	   sEsitoDichiaraz ="KO"
       exit sub
    end if 
end sub





sub InserimentoDomandaIscrizione()
    sEsitoDomandaIscrizione ="OK"
' Response.Write " dd " 
'Response.End 

    sSQL = "INSERT INTO DOMANDA_ISCR " &_
	       "(ID_PERSONA,"              &_
	        "ID_BANDO,"                &_
	        "DT_DOM,"                  &_ 
	        "DT_TMST,"                 &_
	        "ESITO_GRAD,"              &_
	        "ID_SEDE) VALUES ("        &_
		     sPersona & ","            &_ 
		     nBando & ","              &_ 
		     ConvDateToDb(Now()) & "," &_ 
		     ConvDateToDb(Now())
	
	if sGraduatoria = "S" then
	    sSQL =sSQL &  ", NULL" 
	else
 		sSQL =sSQL &  ",'S'"
	end if
'	IF clng(nNumEnteOcc) <> 0 THEN
'		sSQL =sSQL &  "," & nNumEnteOcc 
'	ELSE
'	    'sSQL =sSQL &  ", NULL" 
'	     sSQL =sSQL &  "," & 0  ' nuova direttiva riunione del 6/02/03 
'	END IF
	IF clng(sSede) <> 0 THEN
		sSQL =sSQL &  "," & sSede 
	ELSE	   
	     sSQL =sSQL &  "," & 0  ' nuova direttiva riunione del 6/02/03 
	END IF




	sSQL =sSQL &  ")"

	Errore=EseguiNoC(sSQL,CC)
'    Response.Write "query 2 " & sSQL
    if Errore <> "0" then
        CC.RollbackTrans
	    MessaggioErrori("(Domanda_Iscr) " & Errore)
	    sEsitoDomandaIscrizione ="KO"
    end if

end sub


sub InserimentoStatoOccupazionale()
    EsitoSO ="OK"

	
	IndStatus="0"	
	
		
	if sTempoIndet <> "" then
		Codice=sTempoIndet
	elseif sDisocc  <> "" then
		Codice=sDisocc		
	end if				
  '	Response.Write "codice " & codice
  '	Response.End
	
	sSQL=""
	sSQL = "INSERT INTO STATO_OCCUPAZIONALE " & _
			"(ID_PERSONA," & _
			"IND_STATUS," & _
			"COD_STDIS," & _
			"DT_DICH_STATUS," & _
			"DT_FIN_TEMPODET," & _
			"DT_DEC_DIS," & _
			"COD_ST181," & _			
			"DT_TMST) VALUES " & _	
			"(" & sPersona & _
			",'" & IndStatus & _
			"','" & Codice & _
			"'," & convdatetodbs(Now())

			if sScadenza <> "" then
			   sSQL = sSQL &  "," & convdatetodbs(ConvStringToDate(sScadenza))
			else
			   sSQL = sSQL &  ",null"

			end if

			if sDecorrenza <> "" then
			   sSQL = sSQL &  "," & convdatetodbs(ConvStringToDate(sDecorrenza))
			else
			   sSQL = sSQL &  ",null"

			end if
			
			if sAppartenenza <> "" then 
				sSQL = sSQL & ",'" & sAppartenenza & "'"
			else
			   sSQL = sSQL &  ",null"
			end if			  
		
			
			sSQL = sSQL & "," & convdatetodb(Now()) & ")"
		
	'Response.Write sSQL
	Errore=EseguiNoC(sSQL,CC)
	
	if Errore <> "0" then
				CC.RollbackTrans
				MessaggioErrori("(STATO_OCCUPAZIONALE) " & Errore)
			    EsitoSO ="KO"
			    exit sub
	end if	
	
end sub

sub InserimentoTitoloStudio()
    EsitoTitoloStudio ="OK"
    
    'maggio 2006
'    if sBase = "60" then
'    	nConvVoto = (cint(sVoto) * 100) / 60
'    else
'		nConvVoto = cint(sVoto)
'    end if 
'	sVotoInt = trim(sVoto) & "/" & trim(sBase)
'    nConvVoto= replace(nConvVoto,",",".")


   '  Response.Write sVotoInt
    ' Response.End
    sStato ="0" ' Titolo di studio Conseguito
 
 'maggio 2006   
'	sSQL = "INSERT INTO TISTUD " &_
'	       "(ID_PERSONA," &_
'			"COD_TIT_STUD," &_
'			"COD_LIV_STUD," &_
'			"COD_STAT_STUD," &_
'			"AA_STUD," &_
'			"VOTO_STUD,"  &_
'			"DT_TMST, " &_
'			" CONV_VOTO, " &_
'			" COD_STAT_RIC, " &_
'			" FL_LAUDE)  VALUES " &_
'			"(" & sPersona & _
'			",'" & sTitStud & _
'		   "','" & sLivStud & _
'		   "','" & sStato & _
'		   "'," & cint(sAnnoTit) & _
'		    ",'" & sVotoInt & _
'		  	"'," & ConvDateToDb(Now()) &_
'		  	", TRUNC(" & nConvVoto & ",2)" &_
'		  	",'IT' " &_
'		  	", 'N' )"
		  	
		  	
	sSQL = "INSERT INTO TISTUD " &_
	       "(ID_PERSONA," &_
			"COD_TIT_STUD," &_
			"COD_LIV_STUD," &_			
			"AA_STUD," &_
			"DT_TMST, " &_			
			" COD_STAT_RIC, " &_
			" FL_LAUDE)  VALUES " &_
			"(" & sPersona & _
			",'" & sTitStud & _
		    "','" & sLivStud & _		  
		    "'," & cint(sAnnoTit) & _
		  	"," & ConvDateToDb(Now()) &_		  	
		  	",'IT' " &_
		  	", 'N' )"
'Response.Write sSql
	Errore=EseguiNoC(sSQL,CC)
	
	if Errore = "0" then 
		InsProj sPersona,mid(session("progetto"),2),session("idutente"),CC
	else
				CC.RollbackTrans
				MessaggioErrori("(TISTUD) " & Errore)
			    EsitoTitoloStudio ="KO"
			    exit sub
	end if				
		
	'if Request ("cmbAttIscri") = "" then exit sub
    '	
	'sStato = "2" ' Titolo di Studio in frequenza
    '
	'sSQL = "INSERT INTO TISTUD (" &_
	'	   "ID_PERSONA,"          &_
	'	   "COD_TIT_STUD,"        &_
	'	   "COD_LIV_STUD,"        &_
	'	   "COD_STAT_STUD,"       &_
	'	   "DT_TMST)  VALUES ("   &_
	'	    sPersona              &_
	'		",'" & sAttIscr       &_
	'		"','" & sAttIscr      &_
	'		"','" & sStato        &_
	'		"'," & ConvDateToDb(Now()) & ")"
    '
  	'Errore=EseguiNoC(sSQL,CC)
  	'				
	'if Errore <> "0" then
	'	CC.RollbackTrans
	'	MessaggioErrori("(TISTUD2) " & Errore)
	'	EsitoTitoloStudio ="KO"
	'	'exit sub
	'end if				
						
end sub

sub InserimentoLavoratoreSvantaggiato()
 EsitoLavSvantaggio ="OK"
 sSQLLavSvant = "INSERT INTO LAV_SVANTAGGIO " &_
	       "(ID_PERSONA," &_
			"COD_SVANTAGGIO," &_			
			"DT_TMST) VALUES " &_
			"(" & sPersona & _
			",'" & sCodSvantaggio & _		   		  
		    "'," & ConvDateToDb(Now()) & ")"
		    
'Response.Write sSQLLavSvant
'Response.End
	Errore=EseguiNoC(sSQLLavSvant,CC)
	
	if Errore <> "0" then
				CC.RollbackTrans
				MessaggioErrori("(LAV_SVANTAGGIO) " & Errore)
			    EsitoLavSvantaggio ="KO"
			    exit sub
	end if	
						    		
 
end sub






sub VisualizzazioneMessaggioOperazioneEseguita()
%>
<br>
<table cellpadding="0" cellspacing="0" width="500" border="0">
		<tr height="18">
			<td class="sfondomenu" height="18" width="67%">
			<span class="tbltext0"><b>&nbsp;DOMANDA DI ISCRIZIONE</b></span></td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
			<td valign="middle" align="right" class="tbltext1" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
		</tr>
		<tr>
			<td class="sfondocomm" width="57%" colspan="3">
				Modulo di iscrizione 
			    per il bando&nbsp;<b><%=sDescBando%></b><br>  
			</td>
		</tr>
		<tr height="2">
			<td colspan="3" class="sfondocomm" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>
<br><br>
				<table width="500" border="0" cellspacing="2" cellpadding="1">
					<tr align="middle">
						<td class="tbltext">
								USER = <b><%=sLogin%></b><br>
								PASSWORD = <b><%=sPassword%></b><br><br>
								La domanda di iscrizione � stata inserita correttamente.<br>
								Prendi nota della tua login e password e accedi all'area a te riservata.<br><br>
								<%If session("progetto")="/SPORT2JOB" Then %>
								<a href="javascript:windowLoginOpenPRGGif('/SPORT2JOB')">
								<img src="/images/arearis.gif" border="0" WIDTH="70" HEIGHT="7"></a> 
							    <%else
							       'da definire l'utilizzo dell'area riservata per i vari progetti%>
							<!--	<a href="javascript:windowLoginOpenPRG('<%'=session("progetto")%>')">								<img src="/images/arearis.gif" border="0" WIDTH="70" HEIGHT="7"></a>  							-->
								<%End if%>
						</td>
					</tr>
				</table>
				<br>
				

<%
end sub

%>
<form method="post" name="frmDomIscri" onsubmit="return ControllaDati(this)" action="SEL_CnfDomIscr.asp">

<%





sub CorpoDelProgramma()
   
    ctrlCodiceFiscaleDomandaIscrDuplicato()
  
    if prgEsitoPersona = "KO" then exit sub 'Persona presente su tabelle Persona e Domanda Iscrizione  
    
    CC.beginTrans
    
    
    if prgEsitoPersona = "OK" then  'Persona assente su tabelle Persona e Domanda Iscrizione             
       InserimentoPersona() 
       if Errore <> "0" then exit sub 'Inserimento nella tabella Persona Fallito
   
       AcquisizioneIdPersona()
       if sPersona = 0 then exit sub 'Identificativo della Persona non trovato
       
       InserimentoDichiaraz()
	   if EsitoDichiaraz ="KO" then exit sub
    	       
       InserimentoTitoloStudio()
	   if EsitoTitoloStudio ="KO" then exit sub
    	
		InserimentoStatoOccupazionale()
		if EsitoSO ="KO" then exit sub
		
	'	Response.Write "totale check" & nTotChkLavSvant
	'	Response.End
	
		if nTotChkLavSvant <> "0" and nTotChkLavSvant <> "" then

			for b = 1 to nTotChkLavSvant
		
				valore=Request.Form("chkSvantaggio" & b) 
				'
				if UCase(valore) = "S" then
					sCodSvantaggio = Request.Form("txtCodiceSvant" & b)
					InserimentoLavoratoreSvantaggiato()
				
					if EsitoLavSvantaggio ="KO" then exit sub
				end if
				
			next
		end if  
    end if
         
  
    InserimentoDomandaIscrizione() 
    IF sEsitoDomandaIscrizione ="KO" then exit sub  
    
    InserimentoUtente()  
    if sEsitoUtente = "KO"  then exit sub 'Login presente o inserimento su tabella utente fallito
        
  '    Response.End    
    CC.CommitTrans
    
    VisualizzazioneMessaggioOperazioneEseguita()
    
end sub
'*************************************************************

sub Fine()
<!--#include virtual = "/include/closeconn.asp"-->
<!--#include virtual = "/progettomda/strutt_codaMDA2.asp"-->
end sub

'*************************************************************
%>
<!-- ************** ASP Fine *************** -->
<!-- ************** MAIN Inizio ************ -->
<%
dim sUser
dim CnConn
dim sCognome, sNome
dim sDataNascita
dim sComuneNascita
dim sProvNascita
dim sNazNascita
dim sSesso
dim sPosLeva
dim sCodFisc
dim sStCiv
dim sCittad
dim sIndir
dim sFraz
dim sComune
dim sProv
dim sCap
dim sTelef
dim sEMail
dim sNumCentroImp
dim sDataIscri
dim sCategProt
dim sTitStud
dim sLivStud
dim sStato
dim sAnnoTit
dim sVoto, sBase, nConvVoto, sVotoInt
'dim sAttIscr
dim sGrado(6)
dim sProvincia
dim nBando
dim sAutorizz
dim sStatoDis
dim sLogin
dim sPassword
dim sSQL
dim sPersona
dim sGruppo
dim sIniIsc
dim sDataSis, sDataRicCatProt
dim sIDPers , sGraduatoria
dim rsCodFisc, rsDomanda, rsPers, rsLogin
dim Errore
dim prgEsitoPersona, sDescBando, nNumEnteOcc
dim sEsitoUtente, sDichiaraz, sEsitoDomandaIscrizione
dim sEsitoStatoOccupazionale,EsitoSO, EsitoTitoloStudio
dim EsitoLavSvantaggio, nTotChkLavSvant, sCodSvantaggio, valore, b,  sSede
dim sTipo
dim sAppartenenza, sDisocc, sDecorrenza
dim sTempoIndet, sScadenza


sTipo ="P" 'Vengono trattate solo le persone fisiche (informazione per tabella UTENTE)

AcquisizioneParametri()
'sContrRes = controlloProvinciaRegione(nNumEnteOcc,sProv)

'if sContrRes = "OK" then
	CorpoDelProgramma()
'else
'    MessaggioErrori("Controllare la provincia - regione di residenza")
'end if
Fine()
%>
<!-- ************** MAIN Fine ************ -->