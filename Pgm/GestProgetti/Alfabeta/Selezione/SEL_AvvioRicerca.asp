<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include virtual="/util/portallib.asp"-->
<!--#include Virtual = "/strutt_testa2.asp"-->
<!--#include Virtual ="/Include/openconn.asp"-->
<!--#include File ="SEL_GestioneFile.asp"-->
<%
if ValidateService(session("idutente"),"SEL_CREAESTRATTORE", CC) <> "true" then 
	response.redirect "/util/error_login.asp"
end if
%>

<script LANGUAGE="Javascript">
<!--#include virtual = "/Include/help.inc"-->

var windowQuadro

function Controlla(){
    var Conf
    Conf=confirm("sei sicuro di voler iscrivere le persone selezionate?")
    if (Conf==true){
       return true
    }
    else{
       return false
    }
}

function Chiudi(){
	if (windowQuadro != null ){
		windowQuadro.close(); 
	}
	return true
}	 

</script>
<a name="inizio"></a>
<%	
'********************************
'*********** MAIN ***************

	Dim sModo, nIdRic, sIdSede
	Dim nTipoRicerca, sTipoArea, sTipoProf, nPosti, sDtDal, sDtAl, sDtAvv, sDt_Tmst
	dim nIdProfilo,sPRVCPI
	
	sModo = Request("Modo")
	nIdRic= Request("IdRic")	
	sIdsede=Request("Idsede")
			
	Err.Clear

	Inizio()
	Imposta_Pag()
		
'**********************************************************************************************************************************************************************************	
	Sub Imposta_Pag()
		Dim sql, RR		'recordset delle persone che soddisfano i parametri
		Dim sql1, RR1	'recordset dei parametri di ricerca

'Response.Write session("idbando")		
		Err.number = 0
				
		sql = "select a.id_persona from PERSONA a" &_
			  " where a.id_persona is not null " &_
			  " and not exists (select id_persona from domanda_iscr b, bando c, progetto d" &_
			  " where b.id_persona=a.id_persona " &_
			  " and b.id_bando=c.id_bando" &_
			  " and c.id_proj=d.id_proj" &_
			  " and area_web ='" & mid(Session("Progetto"),2) & "')"
		
		'Funzione che costruisce la regola.
		sql = sql & CreaCriteriDaFile(CC)		
		
		Testata()
		StampaVideo sql,bCImpiego
	    sGraduatoria=Request.Form ("txtGrado")
	    
	    if err.number <> 33 then
	        invia sql,sGraduatoria 
	    end if
end sub
  
sub invia(sql,sGraduatoria)		 %>
		<br>		
		<table width="500" border="0" cellpadding="0" cellspacing="0" align="center">
			<tr ALIGN="CENTER"> 
			  <td align="right">	
				<form method="post" name="FrmAR" action="SEL_SelFiltri.asp" onsubmit="javascript:Chiudi()">
				<input type="hidden" name="dtTmst" value="<%=sDt_Tmst%>">
				<input type="hidden" name="Idsede" value="<%=sIdsede%>">						
				
					<input type="image" src="<%=Session("progetto")%>/images/Indietro.gif" title="Pagina precedente" border="0" align="absBottom" id="image1" name="image1">
				
				</form>
			  </td>	
			
			   <td align="left"> 
				<form method="post" name="FrmConferma" action="SEL_CnfSelFiltri.asp" onsubmit="javascript:return Controlla()">
				<input type="hidden" name="txtSelect" value="<%=sql%>">
				<input type="hidden" name="txtIndGrad" value="<%=sGraduatoria%>">
					<input type="image" src="<%=Session("progetto")%>/images/conferma.gif" title="Pagina precedente" border="0" align="absBottom" id="image1" name="image1">
				
				</form>
			 </td>
			</tr>
		</table>
		
	<%	
		End Sub 
	'**********************************************************************************************************************************************************************************	
		Sub Indietro()
	%>
		<form method="post" name="FrmAR" action="SEL_SelFiltri.asp" onsubmit="javascript:Chiudi()">
			<input type="hidden" name="IdRic" value="<%=nIDRic%>">
			<input type="hidden" name="Idsede" value="<%=sIdsede%>">				
			<table WIDTH="500" BORDER="0" CELLPADDING="0" CELLSPACING="0" align="center">
				<tr align="center">
					<td>
						<input type="image" src="<%=Session("progetto")%>/images/Indietro.gif" title="Pagina precedente" border="0" align="absBottom" id="image1" name="image1">
					</td>
				</tr>
			</table>
		</form>
<%	End Sub 
'**********************************************************************************************************************************************************************************	
	Sub Inizio()
%>
	<table border="0" width="500" cellspacing="0" cellpadding="0" height="70" align="center">
		<tr>
			<td width="720" background="<%=session("progetto")%>/images/titoli/servizi1g.gif" height="70" valign="bottom" align="right">
				<table border="0" background width="500" height="23" cellspacing="0" cellpadding="0">
					<tr>
						<td width="100%" valign="top" align="right"><b CLASS="tbltext1a">Iscrizione Batch
						  </b></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<br>
<%
	End Sub
'**********************************************************************************************************************************************************************************	
	Sub Testata()
%>	
	<table border="0" CELLPADDING="0" CELLSPACING="0" width="500" align="center">
		<tr height="18">
			<td class="sfondomenu" width="67%" height="18"><span class="tbltext0"><b>&nbsp;RISULTATO SELEZIONE </b></span></td>
			<td width="2%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
			<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
		</tr>
	</table>
	<table border="0" CELLPADDING="0" CELLSPACING="0" width="500" align="center">
		<tr>
			<td align="left" class="sfondocomm">
			Visualizzazione del numero di persone che soddisfano i criteri di ricerca scelti.<br>
			premere sul tasto <b>Invia</b> per iscrivere i candidati.<br>
			premere sul tasto <b>Indietro</b> per definire/applicare nuovi criteri.   
			<a href="Javascript:Show_Help('/Pgm/help/GestProgetti/Alfabeta/Selezione/SEL_AvvioRicerca')"><img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
			</td>
		</tr>
		<tr height="2">
			<td class="sfondocommaz" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>
	<br>
<% 	
	End Sub
'**********************************************************************************************************************************************************************************
	Sub Messaggio(Testo)
%>		
		<br><br>
		<table width="500" align="center" border="0" cellspacing="2" cellpadding="1">
			<tr height="20"> 
			  	<td class="tbltext3" align="middle" width="743">
					<b><%=Testo%></b>
				</td>
			</tr>
		</table>
		<br><br>
<%				
	End Sub

' ************************************************************************************************
	sub StampaVideo(sql,bCImpiego)
		
		set RR = server.CreateObject("ADODB.recordset")

'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
		RR.Open sql,CC,3
		sErrore = 0
		if sErrore <> "0" then
			Messaggio(" Si � verificato un errore nell'elaborazione." & sErrore)
			Indietro()
			Err.number = 33
			exit sub
		end if
				
		if RR.EOF then
			Messaggio(" Nessuna persona presenta le caratteristiche richieste.")
			Indietro()
			Err.number = 33
			exit sub
		end if 
        
        numRec = rr.RecordCount 
 %>  
        <br><br>
		<table width="500" align="center" border="0" cellspacing="2" cellpadding="1">
			<tr height="20"> 
			  	<td class="tbltext3" align="middle" width="743">
					Le persone che rispondono ai criteri di ricerca
					   selezionati sono:&nbsp;<b><%=numRec %></b>
				</td>
			</tr>
		</table>
		<br><br>
 <%
        rr.Close : set rr = nothing 
        
	end sub
'**********************************************************************
%>
<!--#include Virtual ="/Include/closeconn.asp"-->
<!--#include Virtual="/strutt_coda2.asp"-->	
