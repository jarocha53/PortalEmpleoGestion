<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%
if ValidateService(session("idutente"),"SEL_CREAESTRATTORE", CC) <> "true" then 
	response.redirect "/util/error_login.asp"
end if

sFilePath = SERVER.MapPath("\") & session("Progetto") &_
	 "\DocPers\Selezione\" & session("IdBando") & "_SelBatchIscr.txt"

' ***********************************************
' Elimino una riga del file
' ***********************************************

function CancRiga(aFile,sNomeTab,sPrgReg,sCampo)
	
	if not IsArray (aFile) then
		aFile = ReadFile()
	end if

	set oFileSystemObject = Server.CreateObject("Scripting.FileSystemObject")
	set oFileCurriculum = oFileSystemObject.OpenTextFile(sFilePath,2)

	nRowDel = 0 

	for t = 0 to UBound(aFile)

		aRiga = Split(aFile(t),"#")
		
		if ( aRiga(0) = sNomeTab ) and ( aRiga(4) = sPrgReg or aRiga(5) = sPrgReg ) and ( sCampo = "0" or aRiga(1) = sCampo ) then 
			
			nRowDel = nRowDel + 1

		else
			oFileCurriculum.WriteLine( aRiga(0) &_
				"#" & aRiga(1) & "#" & aRiga(2) & "#" &_
				 aRiga(3) & "#" & aRiga(4) & "#" & aRiga(5))
		
		end if

	next

	oFileCurriculum.close
	set oFileSystemObject = nothing
	set oFileCurriculum = nothing

	CancRiga = nRowDel

end function

' ***********************************************
' Mi deve ritornare il progressivo regola da  
' indicare come riferimento.
' ***********************************************

function ProgRegRif(aFile,sNomeTab,sNomeCampo,sValore)

	dim aRiga

	if not IsArray (aFile) then
		aFile = ReadFile()
	end if

	for t = 0 to UBound(aFile)
		aRiga = Split(aFile(t),"#")
	
		if ( aRiga(0) = sNomeTab ) and ( aRiga(1) = sNomeCampo ) and ( clng(aRiga(5)) = 0 ) and ( sValore = aRiga(3) )then 
			ProgRegRif = aRiga(4)
		end if
	next

end function

' ***********************************************
' Mi deve ritornare il progressivo regola pi� 
' grande per un nuovo record.
' ***********************************************

function MaxProgReg()
	dim aRiga,nMax

	aFile = ReadFile()

	nMax = 0
	if IsArray(aFile) then
		for t = 0 to UBound(aFile)
			aRiga = Split(aFile(t),"#")
			if nMax <= aRiga(4) then
				nMax = aRiga(4)
			end if
		next
	end if
	MaxProgReg = nMax

end function

' ***********************************************
' La funzione controlla se il filtro che si 
' vuole aggiungere non sia gi� stato inserito
' Ritorna True o False.
' ***********************************************

function ValidFilter(aFile,sNomeTab,sNomeCampo,sValore,sOpLog)
	dim aRiga, t
	
	ValidFilter = 0
	nContaRighe = 0
	for t = 0 to UBound(aFile)
		aRiga = Split(aFile(t),"#")

		if aRiga(0) = sNomeTab then 

			if aRiga(1) = sNomeCampo then

				if (aRiga(2) = sOpLog) or IsNull(sOpLog) then 
					if aRiga(3) = sValore then
						nContaRighe = nContaRighe + 1
					end if

				end if

			end if	

		end if

	next
	ValidFilter = nContaRighe 

end function

' ***********************************************
' Legge il contenuto del file e ritorna un array.
' Ogni elemento dell'Array rappresenta una riga 
' di un file.
' *******************************************************************************
' Ogni elemento dell'Array aFile � cos� composto
' *******************************************************************************
'    0     #   1   #   2   #   3    #        4          #         5
' [Tabella]#[Campo]#[OpLog]#[Valore]#[Progessivo Regola]#[Progessivo Regola Rif.]
' *******************************************************************************

function ReadFile()
    dim cont 
	dim aFile 
	dim sFile	
	
	ReadFile = aFile
    cont = 0
	
	set oFileSystemObject = Server.CreateObject("Scripting.FileSystemObject")
	set oFileCurriculum = oFileSystemObject.OpenTextFile(sFilePath,1)
	
	   Do  While not oFileCurriculum.AtEndOfStream 
  
		  sFile = sFile & oFileCurriculum.ReadLine 

		  if not oFileCurriculum.AtEndOfStream then
		    	sFile = sFile & "##"
		  end if
          cont = 1
    	Loop
    
	aFile = split(sFile,"##")
	
	oFileCurriculum.close
	
	set oFileSystemObject = nothing
	set oFileCurriculum = nothing
	if UBound(aFile)  <> -1 then

		ReadFile = aFile

	end if

end function

function GestFile()
sFilePath = SERVER.MapPath("\") & session("Progetto") &_
	 "\DocPers\Selezione\" & session("IdBando") & "_SelBatchIscr.txt"

	' Questa funzione deve controllare se gi� esiste un file 
	' con l'ID bando oppure crearlo.
		
	set oFileSystemObject = Server.CreateObject("Scripting.FileSystemObject")
    
	if not oFileSystemObject.FileExists(sFilePath) then
		set oFileCurriculum = oFileSystemObject.CreateTextFile(sFilePath,true)
'			Se il file non esiste lo creo vuoto
        oFileCurriculum.close    
        set oFileCurriculum = nothing
	end if				

	set oFileSystemObject = nothing
	
End function

' ***********************************************
' Legge il contenuto del file e ritorna un array.
' Ogni elemento dell'Array rappresenta una riga 
' di un file.
' ***********************************************

function ReadFileWithInputTab(sTab)

	dim aFile 
	dim sFile	
	
	ReadFileWithInputTab = false
	
	set oFileSystemObject = Server.CreateObject("Scripting.FileSystemObject")
	set oFileCurriculum = oFileSystemObject.OpenTextFile(sFilePath,1)
	
	Do While not oFileCurriculum.AtEndOfStream 

		sFileTemp = oFileCurriculum.ReadLine
		aFileTemp = Split(sFileTemp,"#")

		if aFileTemp(0) = sTab then

			sFile = sFile & sFileTemp 

			if not oFileCurriculum.AtEndOfStream then
				sFile = sFile & "##"
			end if

		end if

	Loop
	
	if Right (sFile,2) = "##" then
		sFile = left(sFile,Len(sFile)-2)
	end if

	aFile = split(sFile,"##")
	oFileCurriculum.close
	
	set oFileSystemObject = nothing
	set oFileCurriculum = nothing

	if UBound(aFile)  <> -1 then

		ReadFileWithInputTab = aFile

	end if
	
end function

function OrderFile(sTab)

	dim aFile
	dim aFileOrder()
	
	if sTab <> "" then
		aFile = ReadFileWithInputTab(sTab)
	else
		aFile = ReadFile()
	end if	
	
	if 	not IsArray(aFile) then

		aFile = ""

		exit function

	end if

	redim aFileOrder(UBound(aFile))
	
	sTempTab  = ""
	nCountIns = 0

	for yy = 0 to UBound(aFile)

		aRiga = split(aFile(yy),"#")

		if InStr(sTempTab,aRiga(0)) = 0  then

			sTempTab	= sTempTab & aRiga(0)
			aFileTemp	= ReadFileWithInputTab(aRiga(0))
			
			do while 0 = 0 
				n = -1 
				for ee = 0 to UBound(aFileTemp) 
					if aFileTemp(ee) = "" then
						n = n + 1
					end if
					if n = UBound(aFileTemp) then
						exit do
					end if
				next

				for ee = 0 to UBound(aFileTemp) 
					
					aTempRiga = split(aFileTemp(ee),"#")
				
					if ( UBound (aTempRiga) <> -1 ) then
						if ( aTempRiga(5) = "0" ) then
							
							aFileOrder(nCountIns) = aFileTemp(ee)

							nCountIns = nCountIns + 1 

							for zz = 0 to UBound(aFileTemp) 
									
								aTempRiga2 = split(aFileTemp(zz),"#")
								if ( UBound(aTempRiga2) <> -1 ) then 	

									if ( aTempRiga2(5) = aTempRiga(4) ) then
										
										aFileOrder(nCountIns) = aFileTemp(zz)
										
										aFileTemp(zz) = ""
										
										nCountIns = nCountIns + 1 
								
									end if 

								end if
								
							next

							aFileTemp(ee) = ""

						end if 
					end if
				next

			loop

		end if	

	next 
	
	OrderFile = aFileOrder
	
end function

function DecodIDTab(sIdTab)

	sSQL = "SELECT DESC_TAB FROM DIZ_TAB WHERE ID_TAB = '" & sIdTab & "'" 
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	set rsDescTab = CC.Execute(sSQL)
	if not rsDescTab.EOF then
		DecodIDTab = rsDescTab("DESC_TAB")
	else
		DecodIDTab = ""
	end if
	rsDescTab.Close

end function

' Funzione che mi costruisce la stringa SQL con le regole da applicare

function CreaCriteriDaFile(CC)
	Dim aOpLog	
	Dim memProgr

	aFile = OrderFile("")

	if IsArray(aFile) then

		aOpLog = decodTadesToArray("OPLOG",Date(),"",1,0)		
		memProgr = 0
		
		for zz = 0 to ubound(aFile)
			aRiga = Split(aFile(zz),"#")

			sSQL = "SELECT Tipo_campo FROM DIZ_DATI " &_
				" WHERE ID_CAMPO = '" & aRiga(1) & "' AND" &_
				" ID_TAB = '" & aRiga(0) & "'"

'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
			set RR1 = CC.Execute (sSQL)
			if clng(aRiga(5)) <> 0 and clng(aRiga(5)) = memProgr then
				sql = sql & " and " & aRiga(1) &_
					 " " & searchIntoArray(aRiga(2),aOpLog,1) & Trasforma(aRiga(3), RR1.Fields("Tipo_campo"), searchIntoArray(aRiga(2),aOpLog,1)) &_
					 " And id_persona = a.id_persona " 
			else
				sql = sql & " and exists(select id_persona from " & aRiga(0) &_
					" Where " & aRiga(1) &_
					" " & searchIntoArray(aRiga(2),aOpLog,1) &_
					Trasforma(aRiga(3), RR1.Fields("Tipo_campo"), searchIntoArray(aRiga(2),aOpLog,1)) &_
					" And id_persona = a.id_persona " 
				memProgr = clng(aRiga(4))
			end if
			
			if (zz) < ubound(aFile) then
				aRiga = Split(aFile(zz + 1 ),"#")
				if clng(aRiga(5)) = 0 then 	sql = sql & ")"

			end if
			RR1.Close
			set rr1 = nothing 
		next

		sql = sql & ")"

		Erase aOpLog
	end if

	CreaCriteriDaFile = sql

end function
'**********************************************************************************************************************************************************************************	
Function Trasforma(valore,tipo,operatore)
	if operatore = "IN" or operatore = "NOT IN" then
	    if tipo = "A" then
		     Trasforma =  "('" & Replace(Valore,",","','") & "')"
	    else 
	         Trasforma =  "(" & Replace(Valore,"$","'") & ")"
	    end if
	else
	    
		select case tipo
			case "D"
				if operatore = "BETWEEN" then
					aVal = Split(valore,",",-1,1)
					    Trasforma = " " & ConvDateToDbS(aVal(0)) & " and " & ConvDateToDbS(aVal(1)) & " "
				else
					Trasforma = ConvDateToDbS(valore)
				end if
			case "A"
			    if operatore = "BETWEEN" then
					aVal = Split(valore,",",-1,1)
					    Trasforma = " '" & aVal(0) & "' and '" & aVal(1) & "' "
				else
				    Trasforma = "'" & valore & "'"
			    end if
			case else
			    if operatore = "BETWEEN" then
					aVal = Split(valore,",",-1,1)
					    Trasforma = " " & aVal(0) & " and " & aVal(1) & " "
				else 
				    Trasforma = valore
		        end if
		end select
	end if
			
End Function 
'**********************************************************************************************************************************************************************************	
%>
