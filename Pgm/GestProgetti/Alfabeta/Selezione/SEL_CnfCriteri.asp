<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include Virtual = "/strutt_testa2.asp"-->
<!--#include Virtual = "/util/dbutil.asp"-->
<!--#include Virtual = "/include/Openconn.asp"-->
<!--#include Virtual = "/include/SysFunction.asp"-->
<!--#include Virtual = "/Include/decCod.asp"-->
<!--#include virtual = "/util/portallib.asp"-->

<!--#include File = "SEL_GestioneFile.asp"-->

<%
dim sCondizione 
dim sTab		
dim sOpLog		
dim sVal1 
dim sVal2 
dim sCodArea
dim sCodSpec
dim nIdRic
dim sCampoArea 
dim sCampoSpec 
dim sAction
dim aOpLog
dim sIdSede

sIdSede = Request("IdSede")

sArea		= Request.form ("AREA")
sSpecifico	= Request.form ("SPECIFICO")
sCond		= Request.form ("CONDIZIONE")
sAction = Request.QueryString("Action")

sCondizione = Request.Form("txtCampoTab")
sTab		= Request.Form("IDTAB")
sOpLog		= Request.Form("cmbLogica")

if  trim(replace(Request.Form("txtCodReg"),",","")) = "" then
	sNumValori	= Request.Form("cmbValori").Count
	sAppoggio	= Request.Form("cmbValori")
else
	sNumValori	= Request.Form("txtCodReg").Count
	sAppoggio	= Request.Form("txtCodReg")
end if

if sNumValori > 1 then
	sValue		= split(sAppoggio,",")
else
	sValue		= sAppoggio
end if
sTipoCampo = Request.Form("txtTipo")

if IsArray (sValue) then	
	sAppoggio = ""
	if sTipoCampo <> "D" then
		for i = 0 to ubound(sValue)
			sValue(i) = replace(sValue(i),"'","''")
			if i < ubound(sValue) then
				sAppoggio = sAppoggio & trasforma(trim(sValue(i)),sTipoCampo) & ","
			else
				sAppoggio = sAppoggio & trasforma(trim(sValue(i)),sTipoCampo) 
			end if
		next
	else
		for i = 0 to ubound(sValue)
			if i < ubound(sValue) then
				sAppoggio = sAppoggio & sValue(i) & ","
			else
				sAppoggio = sAppoggio & sValue(i) 
			end if
		next
	end if
	sVal1 = sAppoggio
	sVal1 = replace(sAppoggio,"'","$")
else
	sVal1 = replace(sAppoggio,"'","$")
end if

sCodArea	= Request.Form("txtTipoArea") 
sCodSpec	= Request.Form("txtTipoSpecifico") 
' *************************************************
' Per Vecchia struttura della tabella PERS_CONOSC
 sCampoArea	= Request.Form("txtCampoArea")
' *************************************************
' Per nuova struttura della tabella PERS_CONOSC
' *************************************************
' sCampoArea	= Request.Form("txtCampoSpecifico")
' *************************************************

sCampoSpec	= Request.Form("txtCampoSpecifico")
nIdRic		= Request.Form("txtIDRic")

aOpLog = decodTadesToArray("OPLOG",date,"VALORE = '='",1,0)

if 	sValCod <> "" then
	sValore = sValCod
end if 
'*********************************************************
if sArea = "AREE_PROFESSIONALI" and sCodSpec <> "" then
    sTab = "PERS_FIGPROF"
    sCampoSpec = "ID_FIGPROF"
end if
'*********************************************************

aFile = ReadFile() 

if IsArray (aFile) then ' Se � un array ok

	select case sAction 
		case "INS" 
			if sTab = "VARIE" then
				sTab1 = Request.Form("IDTAB1")
				nCheck = ValidFilter(aFile,sTab,sCodArea,sVal1,sOplog)
				nCheck2 = 1
			elseif sCodSpec <> "" then
				nCheck = ValidFilter(aFile,sTab,sCampoSpec,Trasforma(sCodSpec,sTipoCampo),aOpLog(0,0,0))
			else
				nCheck = ValidFilter(aFile,sTab,sCampoArea,Trasforma(sCodArea,sTipoCampo),aOpLog(0,0,0))
			end if
					
			if sCondizione <> "" then
				nCheck2 = ValidFilter(aFile,sTab,sCondizione,Trasforma(sVal1,sTipoCampo),null)
			end if  

	    case "CAN"
			nCheck = 0
	end select

	nForAppending = 8
else ' Altimenti possono inserire senza problemi 
	nForAppending = 2
	nCheck = 0
end if

Errore = "0"
'V  trasferita la creazione dell'array all'inizio di pagina
dim n

if (nCheck = 0) or (nCheck2 = 0 and sOpLog <> "") then
	select case sAction 
		case "INS" 
			select case sTab
				case "VARIE"
					sTab1 = Request.Form("IDTAB1") 
					if nCheck = "0" then
						nMax = ( MaxProgReg() + 1 )

						set oFileSystemObject = Server.CreateObject("Scripting.FileSystemObject")
						set oFileCurriculum = oFileSystemObject.OpenTextFile(sFilePath,nForAppending)

						oFileCurriculum.WriteLine(sTab1 &_
							"#" & sCodArea & "#" & sOpLog & "#" &_
							UCASE(sVal1) & "#" & nMax & "#0")

						oFileCurriculum.Close
						set oFileCurriculum = nothing	
						set oFileSystemObject = nothing	
					end if 
					
				case "TISTUD","PERS_VINCOLI","PERS_DISPON"
					' Sono nella costruzione della regola pe Titolo di Studio.
					' Per cui devo prima di tutto controllare se � stato indicato lo specifico
					if sCodSpec <> "" then
						sValoreRif = Trasforma(sCodSpec,sTipoCampo)
						sIdCampoRif = sCampoSpec	
					else
						sValoreRif = Trasforma(sCodArea,sTipoCampo)
						sIdCampoRif = sCampoArea
					end if
					
					if nCheck = "0" then
						nMax = ( MaxProgReg() + 1 )
						
						set oFileSystemObject = Server.CreateObject("Scripting.FileSystemObject")
						set oFileCurriculum = oFileSystemObject.OpenTextFile(sFilePath,nForAppending)						

						if sCodSpec <> "" then
							oFileCurriculum.WriteLine(sTab &_
								"#" & sCampoSpec & "#" & aOpLog(0,0,0) & "#" &_
								Trasforma(sCodSpec,sTipoCampo) & "#" & nMax & "#0")
						else
							oFileCurriculum.WriteLine(sTab &_
								"#" & sCampoArea & "#" & aOpLog(0,0,0) & "#" &_
								Trasforma(sCodArea,sTipoCampo) & "#" & nMax & "#0")
						end if	

						oFileCurriculum.Close
						set oFileCurriculum = nothing	
						set oFileSystemObject = nothing	
									
					end if

					if sOpLog <> "" then	
						' Vuol dire che sono state assegnate dei valori di condizione.
						nMax = ( MaxProgReg() + 1 )
						nRegRif =  ProgRegRif(aFile,sTab,sIdCampoRif,sValoreRif)

						set oFileSystemObject = Server.CreateObject("Scripting.FileSystemObject")
						set oFileCurriculum = oFileSystemObject.OpenTextFile(sFilePath,nForAppending)						
						if sVal2 <> "" then 
						' Sicuramente e' una between
						' La between la utilizziamo solo per le date 

						'		oFileCurriculum.WriteLine(sTab &_
						'		"#" & sCondizione & "#" & sOpLog & "#" &_
						'		Trasforma(sVal1,sTipoCampo) & "#" & nMax & "#0")		
						else
							oFileCurriculum.WriteLine(sTab &_
							"#" & sCondizione & "#" & sOpLog & "#" &_
							Trasforma(sVal1,sTipoCampo) & "#" & nMax & "#" & nRegRif)		
						end if

						oFileCurriculum.Close
						set oFileCurriculum = nothing	
						set oFileSystemObject = nothing	
					end if			
					sMessaggio = "OK"
				case "ESPRO"
					' Per nuova struttura della tabella...
					if sCodSpec <> "" then
						sValoreRif = Trasforma(sCodSpec,sTipoCampo)
					else
						sValoreRif = Trasforma(sCodArea,sTipoCampo)
					end if
				
					if nCheck = "0" then
						nMax = ( MaxProgReg() + 1 )	

						set oFileSystemObject = Server.CreateObject("Scripting.FileSystemObject")
						set oFileCurriculum = oFileSystemObject.OpenTextFile(sFilePath,nForAppending)						

						if sCodSpec <> "" then
							oFileCurriculum.WriteLine(sTab &_
							"#" & sCampoSpec & "#" & aOpLog(0,0,0) & "#" &_
							Trasforma(sCodSpec,sTipoCampo) & "#" & nMax & "#0")		
						else
							oFileCurriculum.WriteLine(sTab &_
							"#" & sCampoArea & "#" & aOpLog(0,0,0) & "#" &_
							Trasforma(sCodArea,sTipoCampo) & "#" & nMax & "#0")		
						end if			

						oFileCurriculum.Close
						set oFileCurriculum = nothing	
						set oFileSystemObject = nothing	
					end if	

					if sOpLog <> "" then	
						nMax = ( MaxProgReg() + 1 )						
						if sCodSpec <> "" then
							nRegRif = ProgRegRif(aFile,sTab,sCampoSpec,sValoreRif)	
						else
							nRegRif = ProgRegRif(aFile,sTab,sCampoSpec,sValoreRif)	
						end if
						
						set oFileSystemObject = Server.CreateObject("Scripting.FileSystemObject")
						set oFileCurriculum = oFileSystemObject.OpenTextFile(sFilePath,8)						
							
						if sVal2 <> "" then ' Sicuramente e' una between											 
							oFileCurriculum.WriteLine(sTab &_
							"#" & sCondizione & "#" & sOpLog & "#" &_
							Trasforma(sVal1,sTipoCampo) & " AND " & sVal2 & "#" & nMax & "#" & nRegRif)		
						else
							oFileCurriculum.WriteLine(sTab &_
							"#" & sCondizione & "#" & sOpLog & "#" &_
							Trasforma(sVal1,sTipoCampo) & "#" & nMax & "#" & nRegRif)		
						end if

						oFileCurriculum.Close
						set oFileCurriculum = nothing	
						set oFileSystemObject = nothing	
					end if 
				sMessaggio = "OK"
		'************************
				case else
					' Per nuova struttura della tabella...

					if sCodSpec <> "" then
						sValoreRif = Trasforma(sCodSpec,sTipoCampo)
					else
						sValoreRif = Trasforma(sCodArea,sTipoCampo)
					end if
					
					if nCheck = "0" then
						nMax = ( MaxProgReg() + 1 )

						set oFileSystemObject = Server.CreateObject("Scripting.FileSystemObject")
						set oFileCurriculum = oFileSystemObject.OpenTextFile(sFilePath,nForAppending)

						if sCodSpec <> "" then
							oFileCurriculum.WriteLine(sTab &_
								"#" & sCampoSpec & "#" & aOpLog(0,0,0) & "#" &_
								Trasforma(sCodSpec,sTipoCampo) & "#" & nMax & "#0")
						else
							oFileCurriculum.WriteLine(sTab &_
								"#" & sCampoArea & "#" & aOpLog(0,0,0) & "#" &_
								Trasforma(sCodArea,sTipoCampo) & "#" & nMax & "#0")
						end if			

						oFileCurriculum.Close
						set oFileCurriculum = nothing	
						set oFileSystemObject = nothing	
					end if	
									
					if sOpLog <> "" then	
						nMax = ( MaxProgReg() + 1 )

						if sCodSpec <> "" then
							nRegRif = ProgRegRif(aFile,sTab,sCampoSpec,sValoreRif)	
						else
							nRegRif = ProgRegRif(aFile,sTab,sCampoArea,sValoreRif)	
						end if
					
						set oFileSystemObject = Server.CreateObject("Scripting.FileSystemObject")
						set oFileCurriculum = oFileSystemObject.OpenTextFile(sFilePath,8)

						if sVal2 <> "" then ' Sicuramente e' una between											 
							oFileCurriculum.WriteLine(sTab &_
								"#" & sCondizione & "#" & sOpLog & "#" &_
								Trasforma(sVal1,sTipoCampo) & " AND " & sVal2 & "#" & nMax & "#" & nRegRif)
						else
							oFileCurriculum.WriteLine(sTab &_
								"#" & sCondizione & "#" & sOpLog & "#" &_
								Trasforma(sVal1,sTipoCampo) & "#" & nMax & "#" & nRegRif)
						end if

						oFileCurriculum.Close
						set oFileCurriculum = nothing	
						set oFileSystemObject = nothing	
					end if 
					sMessaggio = "OK"
		'************************
			end select
		case "CAN" 
			' Mi prendo il numero delle righe che sono state generate.
			' Ciclo per ognuna di esse per ritrovare quella ceccata.
			Errore = "0"
			nRighe = request.FORM("txtRighe")
			n=0
			for i = 0 to nRighe
				sChecked = Request.form("chkCan" & i)
				if sChecked = "on" then
					sTab		= Request.form("txtIdTab" & i)	' Nome Tabella
					sCampo		= Request.form("txtIdCam" & i)	' Nome Campo
					nIdRic		= Request.form("txtIdRich")		' Id Richiesta
					sPrgReg		= Request.form("txtPrgReg" & i)  ' Progressivo regola
					sPrgRegRif	= Request.form("txtPrgRegRif" & i)		' Progressivo regola Riferimento
					
					select case sTab
						case "ESPRO"
								aFile = ReadFile()
								nRec = CancRiga(aFile,sTab,sPrgReg,"0")
								n = n + nRec

						case "TISTUD","PERS_VINCOLI","PERS_DISPON"
								aFile = ReadFile()
								nRec = CancRiga(aFile,sTab,sPrgReg,"0")
								n = n + nRec
							
						case else
							if Request.Form("IDTAB") <> "VARIE" then
								aFile = ReadFile()
								nRec = CancRiga(aFile,sTab,sPrgReg,"0")
								n = n + nRec
							else
								aFile = ReadFile()
								nRec = CancRiga(aFile,sTab,sPrgReg,sCampo)
								n = n + nRec
							end if
						end select
					end if 
				next
		end select
else
'*****************************************************    
    if sArea = "AREE_PROFESSIONALI" and sCodSpec <> "" then
        sTab = "AREE_PROFESSIONALI"
        sCampoSpec = "ID_AREAPROF"
    end if
'******************************************************    
	Errore="Esiste gi� un inserimento con le stesse caratteristiche"
end if		
%>
<br>
<table border="0" CELLPADDING="0" CELLSPACING="0" width="500" align="center">
<tr>
	<td class="tbltext3" align="center" valign="middle">
<%
			if Errore = "0" then
%>				
				<form name="frmIndietro" method="post" action="SEL_SelFiltri.asp">
				  <table border="0" CELLPADDING="0" CELLSPACING="0" width="500" align="center">
					 <tr>
						<td align="center"> 
						<input type="hidden" name="cmbTipoArea" value="<%=clng(Request("cmbTipoArea"))%>">
						<input type="hidden" name="cmbTipoProf" value="<%=clng(Request("cmbTipoProf"))%>">
						<input type="hidden" name="IdRic" value="<%=nIdRic%>">
						<input type="hidden" name="IdSede" value="<%=sIdSede%>">
						</td>
					 </tr>
				  </table>
				</form>
				
				<%
				select case sAction
					case "CAN"%>
					<script>
						alert("Cancellazione correttamente effettuata")
					</script>
					<%case "INS"%>
					<script>
						alert("Inserimento correttamente effettuato")
					</script>
				<%end select%>
				<script>
					frmIndietro.submit();
				</script>
<%
				Response.End 
			else
				Inizio()
				Response.Write Errore
			end if 
			%>
	</td>
</tr>
<tr>
	<td align="center" valign="middle" align="center">
		<br><br><br><br>
		<form name="frmIndietro1" method="POST" action="SEL_VisCriteri.asp">
			<input type="hidden" name="cmbTipoArea" value="<%=clng(Request("cmbTipoArea"))%>">
			<input type="hidden" name="cmbTipoProf" value="<%=clng(Request("cmbTipoProf"))%>">

			<%if (Request.Form("IDTAB") = "VARIE") and sAction ="CAN" then%>
				<input type="hidden" value="VARIE" name="IDTAB">
			<%	sTab1 = sTab
			else
			%>
				<input type="hidden" value="<%=sTab%>" name="IDTAB">
			<%End if%>
			<input type="hidden" value="<%=sTab1%>" name="IDTAB1">
			<input type="hidden" name="IdRic" value="<%=nIdRic%>">
			<input type="hidden" name="AREA" value="<%=sArea%>">
			<input type="hidden" value="<%=sIdSede%>" name="IdSede">
			<input type="hidden" name="SPECIFICO" value="<%=sSpecifico%>">
			<%if sCond <> "" then%>
				<input type="hidden" name="CONDIZIONE" value="<%=sCond%>"> 
			<%else%>
				<input type="hidden" name="CONDIZIONE" value="<%=sCondizione%>"> 
			<%end if%>
			<a href="Javascript:frmIndietro1.submit();" onmouseover="window.status=' '; return true"><img src="<%=session("progetto")%>/images/indietro.gif" border="0"></a>
		</form>
	</td>
</tr>
</table>
<%
Erase aOpLog

Sub Inizio()%>
	<table width="530px" border="0" cellspacing="0" cellpadding="0" height="70" align="center">
		<tr>
			<td width="500" background="<%=session("progetto")%>/images/titoli/servizi2g.gif" valign="bottom" align="right">
				<table border="0" background width="500" height="23" cellspacing="0" cellpadding="0" align="center">
					<tr>
						<td width="100%" valign="top" align="right"><b CLASS="tbltext1a">Interrogazioni
						  </b></td>
					</tr>
				</table>
			</td>
		</tr>
	</table> 
	<br>
<%End Sub%>
<%
Function Trasforma(valore,tipo)
	select case tipo
'		case "D"
'			Trasforma = ConvDateToDbs(valore)
'		case "A"
'			Trasforma = "'" & Valore & "'"
		case else
			Trasforma = valore
	end select
	
End Function 
%>
<!--#include Virtual = "/include/CloseConn.asp"-->
<!--#include Virtual="/strutt_coda2.asp"-->		
