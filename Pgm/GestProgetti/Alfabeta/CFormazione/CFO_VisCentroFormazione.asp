<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #include virtual="/strutt_testa2.asp"-->
<!--#include virtual ="/include/openconn.asp"-->
<!--#include virtual="/util/portallib.asp"-->
<!--#include virtual="/include/SelAreaTerrBandi.asp"-->
<%
if ValidateService(session("idutente"),"CFO_VISCENTROFORMAZIONE", CC) <> "true" then 
	response.redirect "/util/error_login.asp"
end if
%>
<!-- ************** Javascript inizio ************ -->
<script LANGUAGE="Javascript">
<!--#include virtual = "/Include/help.inc"-->

function Invia(){
	if (document.frmElencoCF.cmbProv.value==""){
		alert('Selezionare una Provincia.')
	}else{		
		document.frmElencoCF.action = "CFO_VisCentroFormazione.asp";
		document.frmElencoCF.submit();				
	}
}
function EseguiC(){
 
		frmElencoCF.action ="CFO_InsCentroFormazione.asp"
		frmElencoCF.submit() 	
	
}
function EseguiM(nfrmCF){
		frmCF.action  = "CFO_ModCentroFormazione.asp" 
		eval("frmCF"+nfrmCF+".submit()"); 
}
</script>
<!-- ******************  Javascript Fine *********** -->
<!-- ************** ASP inizio *************** -->
<!--#include virtual ="/include/ckProfile.asp"-->
<!--#include virtual ="/util/dbutil.asp"-->
<!--#include virtual ="/include/DecComun.asp"-->


<% sub Intestazione() %>
	<table border="0" width="520" cellspacing="0" cellpadding="0" height="81">
	   <tr>
	     <td width="500" background="<%=Session("Progetto")%>/images/titoli/strumenti2b.gif" height="81" valign="bottom" align="right">
	       <table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
	         <tr>
	           <td width="100%" valign="top" align="right"><b class="tbltext1a">Gestione Progetti</span></b></td>
	         </tr>
	       </table>
	     </td>
	   </tr>
	</table>
	<br>
	<table cellpadding="0" cellspacing="0" width="500" border="0">
		<tr height="18">
			<td class="sfondomenu" height="18" width="67%">
			<span class="tbltext0"><b>&nbsp;GESTIONE CENTRI DI FORMAZIONE</b></span></td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
			<td valign="middle" align="right" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1"></td>
		</tr>
		<tr width="371" class="SFONDOCOMM">
			<td colspan="3">
			Selezionare una provincia per ottenere la visualizzazione in tabella dei CF relativi.
			<br>Per visualizzare e/o modificare i dati premere sull'<b>Id Formazione</b>. 
			<br>Per inserire un centro di formazione non ancora registrato premere su<b> 
			Inserisci un nuovo centro di formazione
			<a title="Help" href="Javascript:Show_Help('/Pgm/help/GestProgetti/Alfabeta/CFormazione/CFO_VisCentroFormazione')" onmouseover="javascript:window.status=' '; return true"><img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a></td>
		</tr>
		<tr height="2">
			<td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>
	<br>
<%end sub%>
<!---------------------------------------------------------------->

<%'SetProvAreaTerr(idArea)%>
<%sub CaricaCombo()'INIZIO CaricaCombo della Provincie da selezionare %>
<br>

<form method="POST" action name="frmElencoCF">	
<table cellpadding="1" cellspacing="0" width="400" border="0">	
	<!--input type="hidden" name="cmbBando" value="<%=sBando%>"-->
	<input type="hidden" name="cmbProv" value="<%=sProv%>">
	<input type="hidden" name="MOD" value="1">
<%
	if sProv <> "" then
%>
	<tr align="left">	
		<td class="tbltext1" align="left" width="100">
			<b>Provincia:</b>
		</td>
		<td class="tbltext" align="left" width="300">
				
			<b><!--option value="<%=sProv%>"><%=DecCodVal("PROV", 0, "", sProv,"")%></option--></b>
			<b><%= DecCodVal("PROV", 0, "", nProv,"")%></b>
		</td>
	</tr>
<%
	else
	'Response.Write sProv
	sMask = SetProvUorg(Session("idUorg"))
	'Response.Write  sMask 
%>	
	<tr>	
		
		
<%		
	nProvSedi = len(sMask)
	
	    if sMask <> ""  then %>
	    
	    <td align="left" class="tbltext1" width="100">
			<b>Provincia:</b>
		</td>
	    
	    <td align="left" width="300">			 	 
			<select NAME="cmbProv" class="textblack" onchange="Invia()">			 
			<option>&nbsp;</option>
	    
	  <%  	pos_ini = 1 	                
			 for i=1 to nProvSedi/2
				sTarga = mid(sMask,pos_ini,2)															
%>
				<option value="<%=sTarga%>"><%=DecCodVal("PROV", 0, "", sTarga,"")%></option>
                    
	 <% 			pos_ini = pos_ini + 2
			next%>
	     </select>
	     </td>
	   <%  else%>
	     
				<td class="tbltext3"><b>Non risulti essere associato ad alcuna provincia.</b>
				</td>
			
	   <%     
	
		
        end if
%>
			
			<input type="hidden" name="cmbProv" value="<%=sProv%>">
		
	</tr>
			
<%
		end if
%>	
</table>
</form>
<br>
<%
end sub 
'----------------------------------------------------------------
sub ImpostaTab()

%>

	<table border="0" cellpadding="0" cellspacing="1" width="500">
		<tr> 
			<td align="center" colspan="2"> 
				<a href="Javascript:EseguiC()" class="textred" title="Inserisci Centro di Formazione" onmouseover="javascript:window.status=' '; return true">
				     <b>Inserisci un Centro di Formazione</b>
				</a>
			</td>
		</tr>
	</table>
	<br>
<%
   'Lettura tabella CENTRO_FORMAZIONE 
   sSQL = "SELECT ID_CFORM,DESC_CFORM,COMUNE " &_
          "FROM CENTRO_FORMAZIONE WHERE PRV = '"  &_
   	      nProv & "' ORDER BY ID_CFORM"
  'Response.Write sSQL
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	set rsCenForm = CC.Execute(sSQL)

	if rsCenForm.EOF then
%>		
		<table align="center" width="500" border="0">
			<tr>
				<td class="tbltext3"><b>Non ci sono Centri di Formazione per la Provincia selezionata.</b>
				</td>
			<tr><td>&nbsp;</td></tr>	
			</tr>
			<tr>
				<td align="center"><a title="Torna Indietro" href="CFO_VisCentroFormazione.asp"><img src="<%=session("Progetto")%>/images/indietro.gif" border="0" name="imgPunto2" onmouseover="javascript:window.status=' '; return true"></a>
				</td>
			</tr>		
		</table>
	<%else%>
        <table border="0" width="500">
		    <tr class="sfondocomm">
		   		<td align="center" valign="center" width="100">
		   			<b>ID Formazione</b>
		   		</td>
				<td align="center" valign="Center" width="300">
					<b>Descrizione</b>
				</td>
				<td align="center" valign="Center" width="100">
					<b>Comune</b>
				</td>
		    </tr> 
		       
<%			 i=0
			do while not rsCenForm.EOF
			i=i+1 %>
			<form method="POST" action="CFO_ModCentroFormazione.asp" name="frmCF<%=i%>">				
				<tr class="tblsfondo"> 
					<td class="tblDett">
						<input type="hidden" name="hIdCForm" value="<%=i%>">
						<input type="hidden" name="CForm" value="<%=rsCenForm("ID_CFORM")%>">
						<input type="hidden" name="DescCForm" value="<%=rsCenForm("DESC_CFORM")%>">
						<a class="tblAgg" title="Seleziona Centro Formazione" HREF="Javascript:document.frmCF<%=i%>.submit()" onmouseover="javascript:window.status=' '; return true">
							<%=rsCenForm("ID_CFORM")%>
						</a>
					</td>
				    <td class="tblDett">
						<%=rsCenForm("DESC_CFORM")%>
		            </td>
			        <td class="tblDett">
						<% sDescrComune =  DescrComune(rsCenForm("COMUNE"))
			        	if sDescrComune <> "0" then
			        	   Response.Write sDescrComune
			        	end if   
			      	    %>
				    </td>
				</tr>
			</form>
	<%          
				rsCenForm.MoveNext
			loop
	%>	
	</table><br>
<table border="0" width="500">
	<tr>
		<td align="center"><a title="Torna Indietro" href="CFO_VisCentroFormazione.asp">
			<img src="<%=session("Progetto")%>/images/indietro.gif" border="0" name="imgPunto2" onmouseover="javascript:window.status=' '; return true"></a>
		</td>
	</tr>
</table>	
<%  end if
  rsCenForm.Close
  set rsCenForm = nothing
%>


<%end sub%>

<!----------------------------------------------------------------->
<%sub Fine()%>
	
	<!--#include virtual ="/include/closeconn.asp"-->
	<!--#include virtual = "strutt_coda2.asp"-->
<%end sub %>
<!-- ************** ASP Fine *************** -->

<%	
'*************** INIZIO MAIN ***********
dim sProv, sModo, sMask, sSqlProv, nProv, i, sDescProv, rsCenForm, sDescrComune,nIdCenForm,DescCenForm
     'la variabile sMODO puo' assumere i valori:
     ' <>1: se e' la prima volta che si entra
     '   1: se si e' gia' selezionata la provincia
sModo = Request ("MOD")

Intestazione()
'Response.Write sProv & "Max"

if sModo = 1 then
		sProv =	Request("cmbProv")
		if len(sProv)>2 then
			nProv= Mid(sProv,3,2)
		else
			nProv = sProv
		end if	
        CaricaCombo() ' caricamento del COMBO delle PROVINCIE
        ImpostaTab()  ' caricamento della TABELLA con i Centri Formazione della provincia selezionata
else        
        sProv = "" 
        CaricaCombo() ' caricamento del COMBO delle PROVINCIE
end if

Fine()
'*************** FINE   MAIN ***********	
%>		
