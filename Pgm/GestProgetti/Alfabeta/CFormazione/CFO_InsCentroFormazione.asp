<!-- #include virtual="/strutt_testa2.asp"-->
<!--#include virtual = "/include/openconn.asp"-->
<!--#include virtual="/util/portallib.asp"-->
<%
if ValidateService(session("idutente"),"CFO_VISCENTROFORMAZIONE", CC) <> "true" then 
	response.redirect "/util/error_login.asp"
end if
%>
<!-- ************** Javascript inizio ************ -->

<script LANGUAGE="Javascript">
//esplosione dei comuni relativi alla provincia selezionata
<!--#include virtual = "/include/SelComune.js"-->

//include del file per fare i controlli sulla numericit� dei campi
<!--#include virtual = "/Include/ControlNum.inc"-->
<!--#include virtual = "/Include/ControlString.inc"-->
<!--#include virtual = "/Include/help.inc"-->

function VaiInizio(sPrv)
{
	location.href = "CFO_VisCentroFormazione.asp?MOD=1&cmbProv=" + sPrv;
}	

//--------------------------------------------------------------------------------------------------------------------------------------

//function insRegole(){
//	newWindow = window.open('CFO_CreaRegola.asp','newWind','width=290,height=380,Resize=No')
//	return false
//}

//--------------------------------------------------------------------------------------------------------------------------------------

function ControllaDati(myForm){

	myForm.txtDescCForm.value = TRIM(myForm.txtDescCForm.value)
	if (myForm.txtDescCForm.value == "") {
		alert("Campo obbligatorio")
		myForm.txtDescCForm.focus() 
		return false
	}
	
	myForm.txtComune.value=TRIM(myForm.txtComune.value)
	if (myForm.txtComune.value == "") {
		alert("Comune obbligatorio")
		myForm.txtComune.focus() 
		return false
	}
	
	myForm.txtCAP.value=TRIM(myForm.txtCAP.value)
	if (IsNum(myForm.txtCAP.value) != true){
		alert("Inserire un valore numerico!")
		myForm.txtCAP.focus() 
		return false
	}
	//CAP deve essere diverso da zero
	if (eval(myForm.txtCAP.value) == 0){
		alert("Il CAP non pu� essere zero!")
		myForm.txtCAP.focus()
		return false
	}

	if (myForm.txtCAP.value.length != 5){
		alert("Formato del CAP errato.")
		myForm.txtCAP.focus() 
		return false
	}
	
	myForm.txtIndirizzo.value=TRIM(myForm.txtIndirizzo.value)

	myForm.txtNumTelefono.value = TRIM(myForm.txtNumTelefono.value)
	if (!IsNum(myForm.txtNumTelefono.value)){
		alert("Il telefono deve essere numerico!");
		myForm.txtNumTelefono.focus(); 
		return false;
	}

	myForm.txtFax.value=TRIM(myForm.txtFax.value)
	if (!IsNum(myForm.txtFax.value)){
		alert("Il FAX deve essere numerico!");
		myForm.txtFax.focus(); 
		return false;
	}
		
	myForm.txtEmail.value=TRIM(myForm.txtEmail.value)
	if (myForm.txtEmail.value != ""){
		if (ValidateEmail(myForm.txtEmail.value) != true){
			alert("Inserire un indirizzo E-Mail corretto!")
			myForm.txtEmail.focus() 
			return false
		}
	}
	return true
}

</script>
<!-- ******************  Javascript Fine *********** -->

<!-- ************** ASP inizio *************** -->

<%	
sub Inizio()
%>
	<table border="0" width="520" cellspacing="0" cellpadding="0" height="81">
	   <tr>
	     <td width="500" background="<%=Session("Progetto")%>/images/titoli/strumenti2b.gif" height="81" valign="bottom" align="right">
	       <table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
	         <tr>
	           <td width="100%" valign="top" align="right">
	           <b class="tbltext1a">Gestione Centri Di Formazione</b></td>	         
			</tr>
	       </table>
	     </td>
	   </tr>
	</table>

	<br>
	<table cellpadding="0" cellspacing="0" width="500" border="0">
		<tr height="18">
			<td class="sfondomenu" height="18" width="67%">
			<span class="tbltext0"><b>&nbsp;GESTIONE CENTRI DI FORMAZIONE</b></span></td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
			<td valign="middle" align="right" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1">(*) campi obbligatori</td>
		</tr>
		<tr width="371" class="SFONDOCOMM">
			<td colspan="3">
			Inserire i dati relativi alla struttura da definire.<br>
			Premere <b>Invia</b> per salvare. 
			<a title="Help" href="Javascript:Show_Help('/Pgm/help/GestProgetti/Alfabeta/CFormazione/CFO_InsCentroFormazione')" onmouseover="javascript:window.status=' '; return true"><img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
			</td>
		</tr>
		<tr height="2">
			<td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>
	<br>
<%
end sub

'------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

sub ImpostaPag()

%>
<form name="frmInsCentroForm" method="post" onsubmit="return ControllaDati(this)" action="CFO_CfnCentroFormazione.asp">
<input type="hidden" name="txtAction" value="INS">

	<table border="0" cellpadding="1" cellspacing="2" width="500">
		<tr class="tbltext1">
  			<td align="left">
  				<b>Descrizione*</b>
			</td>
			<td align="left">
				<input style="TEXT-TRANSFORM: uppercase;" size="50" class="textblack" maxlength="50" name="txtDescCForm">
			</td>
		</tr>
		<tr class="tbltext1">
			<td align="left">
  				<b>Indirizzo</b>
			</td>
			<td align="left">
				<input style="TEXT-TRANSFORM: uppercase;" size="50" class="textblack" maxlength="50" name="txtIndirizzo">
			</td>
		</tr>
		<tr class="tbltext1">
  			<td align="left">
  				<b>Provincia</b>
			</td>
  			<td align="left" class="tbltext">
				<b><%=nProv%></b>
			</td>
		</tr>
		
		<input type="hidden" name="txtProvincia" value="<%=nProv%>">					
		
		<tr class="tbltext1">
  			<td align="left">
  				<b>Comune*</b>
			</td>			
			<td nowrap>
				<span class="tbltext">
				<input name="txtComune" style="TEXT-TRANSFORM: uppercase;" class="textblack" size="35" readonly value="<%=DescrComuneResid%>">
				<input type="hidden" id="txtComuneHid" name="txtComuneHid" value>
<%
				NomeForm="frmInsCentroForm"
				CodiceProvincia="txtProvincia"
				NomeComune="txtComune"
				CodiceComune="txtComuneHid"
				Cap="txtCAP"
%>
				<a title="Seleziona Comune" href="Javascript:SelComune('<%=NomeForm%>','<%=CodiceProvincia%>','<%=NomeComune%>','<%=CodiceComune%>','<%=Cap%>')" ID="imgPunto1" name="imgPunto1" onmouseover="javascript:window.status='';return true">
				<img border="0" src="<%=Session("Progetto")%>/images/bullet1.gif"></a>
				</span>				
			</td>
			
		</tr>
		<tr class="tbltext1">
  			<td align="left">
  				<b>CAP</b>
			</td>
			<td align="left">
  				<input style="TEXT-TRANSFORM: uppercase;" class="textblack" size="6" maxlength="5" name="txtCAP">
			</td>
		</tr>
		<tr class="tbltext1">
  			<td align="left">
  				<b>Numero Telefonico</b>
			</td>
			<td align="left">
  				<input style="TEXT-TRANSFORM: uppercase;" class="textblack" size="20" maxlength="20" name="txtNumTelefono">
			</td>
		</tr>
  		<tr class="tbltext1">
  			<td align="left">
  				<b>FAX</b>
			</td>
			<td align="left">
  				<input style="TEXT-TRANSFORM: uppercase;" class="textblack" size="20" maxlength="20" name="txtFax">
			</td>
		</tr>
		<tr class="tbltext1">
  			<td align="left">
  				<b>E_MAIL</b>
			</td>
			<td align="left">
  				<input style="TEXT-TRANSFORM: uppercase;" class="textblack" size="30" maxlength="30" name="txtEmail">
			</td>
		</tr>
	</table>
	<br>
	<!----impostazione dei comandi-------------------------------->
	<table cellpadding="0" cellspacing="0" width="300" border="0">	
		<tr align="center">
			<td nowrap><input title="Conferma" type="image" name="Conferma" src="<%=Session("progetto")%>/images/conferma.gif"></td value="Registra">			
			<td nowrap><a title="Annulla" href="javascript:document.frmInsCentroForm.reset()"><img border="0" src="<%=Session("progetto")%>/images/annulla.gif"></td>
			<td nowrap><a title="Torna Indietro" href="javascript:history.go(-1)"><img src="<%=session("Progetto")%>/images/indietro.gif" border="0" name="imgPunto2" onmouseover="javascript:window.status=' '; return true"></a></td>
		</tr>
	</table>
</form>
<%
end sub

'------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

sub Fine()
%>
<!--#include virtual = "/include/closeconn.asp"-->
<!--#include virtual = "strutt_coda2.asp"-->
<%
end sub

%>
<!-- ************** ASP Fine *************** -->

<!-- ************** MAIN Inizio ************ -->
<%
	dim sMask
	dim sProv 

	sProv = Request("cmbProv")
		if len(sProv)>2 then
			nProv= Mid(sProv,3,2)
		else
			nProv = sProv
		end if	
	'nProv= Mid(sProv,3,2)
	'Response.Write nProv & "=nProv"
	
	Inizio()
	ImpostaPag()
	Fine()
%>
<!-- ************** MAIN Fine ************ -->
