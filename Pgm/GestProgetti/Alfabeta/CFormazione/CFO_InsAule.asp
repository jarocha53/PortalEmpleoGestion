<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #include virtual="/strutt_testa2.asp"-->
<!--#include virtual ="/include/openconn.asp"-->
<!--#include virtual="/util/portallib.asp"-->
<!-- ************** Javascript inizio ************ -->
<script LANGUAGE="Javascript">
<!--#include virtual = "/Include/help.inc"-->

//include del file per fare i controlli sulla numericitÓ dei campi
<!--#include virtual = "/Include/ControlNum.inc"-->
<!--#include virtual = "/Include/ControlString.inc"-->

//-------------------------------------------------------------------

function SelCentroForm(sProv,sComune,NomeForm,IdUorg,idBando,NomeCampo,txtIDForm,txtDescBando)
{
	windowArea = window.open ('/include/SelCentroForm.asp?comune='+ sComune + '&Prov=' + sProv + '&NomeForm=' + NomeForm + '&idBando=' +  idBando + '&IdUorg=' +  IdUorg + '&NomeCampo=' + NomeCampo + '&NomeCampoCF=' + txtIDForm + '&DescrBando=' + txtDescBando,'Info','width=422,height=450,Resize=No,Scrollbars=yes')	
}	

//-------------------------------------------------------------------

function VaiInizio(sPrv){	
	location.href = "CFO_VisAule.asp?cmbProv=" + sPrv;
}	

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

function ControllaDati(myForm){
	if (myForm.txtDenominazione.value == "") {
		alert("Denominazione obbligatoria")
		myForm.txtDenominazione.focus() 
		return false
	}
	
	myForm.txtDenominazione.value=TRIM(myForm.txtDenominazione.value)

	if (myForm.txtNumPosti.value == "" || myForm.txtNumPosti.value == "0"){
		alert("Numero Posti obbligatorio")
		myForm.txtNumPosti.focus() 
		return false
	}
	
	if (!IsNum(myForm.txtNumPosti.value)){
		alert("Il valore deve essere numerico")
		myForm.txtNumPosti.focus() 
		return false
	}
	if (myForm.cmbTipoClasse.value == "") {
		alert("Indicare il tipo della classe")
		myForm.cmbTipoClasse.focus() 
		return false
	}
	if (myForm.txtcf.value == "") {
		alert("Indicare il centro di Formazione")
		myForm.txtcf.focus() 
		return false
	}
	return true
}

</script>
<!-- ******************  Javascript Fine *********** -->

<!-- ************** ASP inizio *************** -->

<!--#include virtual = "/include/ckProfile.asp"-->
<!--#include virtual = "/util/dbutil.asp"-->

<%sub Inizio()%>
	<table border="0" width="520" cellspacing="0" cellpadding="0" height="81">
	   <tr>
	     <td width="500" background="<%=Session("Progetto")%>/images/titoli/strumenti2b.gif" height="81" valign="bottom" align="right">
	       <table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
	         <tr>
	           <td width="100%" valign="top" align="right"><b class="tbltext1a">Gestione Progetti</b></td>
	         </tr>
	       </table>
	     </td>
	   </tr>
	</table>
	<br>
	<table cellpadding="0" cellspacing="0" width="500" border="0">
		<tr height="18">
			<td class="sfondomenu" height="18" width="67%">
			<span class="tbltext0"><b>&nbsp;DISPONIBILITA' AULE</b></span></td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
			<td valign="middle" align="right" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1"></td>
		</tr>
		<tr width="371" class="SFONDOCOMM">
			<td colspan="3">
			Inserimento dati. <br>
			Premere <b>Invia</b> per salvare. 
			<a title="Help" href="Javascript:Show_Help('/Pgm/help/GestProgetti/Alfabeta/CFormazione/CFO_InsAule')" onmouseover="javascript:window.status=' '; return true"><img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
			</td>
		</tr>
		<tr height="2">
			<td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>
	<br>
<%end sub
'-----------------------------------------------------------------------------------------------------------------------------------------------------------
sub ImpostaPag()
	NomeForm="frmInsAula"
	IdUorg=Session("iduorg")
%>
	<form name="frmInsAula" onsubmit="return ControllaDati(this)" method="post" action="CFO_CfnAule.asp">
	<input type="hidden" name="txtAction" value="INS">
	<input type="hidden" name="txtBando" value="<%=nIdBando%>">
	<input type="hidden" name="txtDescBando" value="<%=sBandoDesc%>">

	<table border="0" cellpadding="1" cellspacing="2" width="500">		
		<tr class="tbltext1" width="150">
  			<td align="left">
  				<b>Denominazione*</b>
			</td>
  			<td align="left">
				<input style="TEXT-TRANSFORM: uppercase;" size="30" maxlength="30" class="textblack" name="txtDenominazione">
			</td>
		</tr>
		<tr class="tbltext1">
  			<td align="left" width="150">
  				<b>Numero posti*</b>
			</td>
  			<td align="left">
				<input style="TEXT-TRANSFORM: uppercase;" size="3" maxlength="2" class="textblack" name="txtNumPosti">
			</td>
		</tr>
		<tr class="tbltext1">
  			<td align="left" width="150">
  				<b>DisponibilitÓ classe*</b>
			</td>
  			<td align="left">
				<select name="cmbTipoClasse" class="textblack">
					<option></option>
					<option Value="M">Mattino</option>
					<option Value="P">Pomeriggio</option>
				</select>
			</td>
		</tr>
		<tr class="tbltext1">
  			<td align="left">
	  			<b>Situata presso il Centro <br>di formazione*</b>
			</td>
  			<td valign="top">
  			<textarea readonly rows="4" cols="35" class="textblack" id="txtcf" style="TEXT-TRANSFORM: uppercase;" name="txtcf"></textarea>			
				<a title="Seleziona Centro di Formazione" href="Javascript:SelCentroForm('','','<%=NomeForm%>','<%=IdUorg%>','<%=nIdBando%>','txtcf','txtIdCForm','<%=sbandodesc%>')" ID="imgPunto1" name="imgPunto1" onmouseover="javascript:window.status='';return true">				
				<img border="0" src="<%=Session("Progetto")%>/images/bullet1.gif"></a>
				<input type="hidden" name="txtIdCForm">
			</td>
		</tr>
	</table>
	<br>
	<!--impostazione dei comandi-->
	<table cellpadding="0" cellspacing="0" width="300" border="0">	
		<tr align="center">
			<td nowrap><input title="Conferma" type="image" name="Conferma" src="<%=Session("progetto")%>/images/conferma.gif"></td value="Registra">
			<td nowrap><a title="Annulla" href="javascript:document.frmInsAula.reset()"><img border="0" src="<%=Session("progetto")%>/images/annulla.gif"></td>
			<td nowrap><a title="Indietro" href="Javascript:Indietro.submit()"><img src="<%=session("Progetto")%>/images/indietro.gif" border="0" name="imgPunto2" onmouseover="javascript:window.status=' '; return true"></a></td>
		</tr>
	</table>

</form>
<form name="Indietro" action="CFO_VisAule.asp" method="Post">
	<input type="hidden" name="CmbDescBando" value="<%=sBandoDesc%>|<%=nIdBando%>">
</form>

<%
end sub

'-----------------------------------------------------------------------------------------------------------------------------------------------------------

sub Fine()
%>
<!--#include virtual = "/include/closeconn.asp"-->
<!--#include virtual = "/strutt_coda2.asp"-->
<%
end sub%>

<!-- ************** ASP Fine *************** -->

<!-- ************** MAIN Inizio ************ -->

<%

if ValidateService(session("idutente"),"CFO_VisAule",CC) <> "true" then 
	response.redirect "/util/error_login.asp"
end if

'dim sIdCenForm

dim sTitolo
dim sProv
dim sBandoDesc

'sIdCenForm = Request ("iDCForm")
nIdBando=clng(Request.Form ("idBan"))		
sBandoDesc = Request.Form ("DescBando")

Inizio()
ImpostaPag()
Fine()
%>

<!-- ************** MAIN Fine ************ -->






