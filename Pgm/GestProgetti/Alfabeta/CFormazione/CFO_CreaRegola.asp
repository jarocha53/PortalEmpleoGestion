<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="GENERATOR" content="Microsoft FrontPage Express 2.0">
<LINK REL=STYLESHEET TYPE="text/css" HREF="../../fogliostile.css">
<title>Costruzione regole</title>
	<!--#include file = "../../include/ValDomIsc.asp"-->
	<!--#include file = "../../include/SysFunction.asp"-->
<SCRIPT LANGUAGE="Javascript">
//include del file per fare i controlli sulla validit� delle date
<!--#include file = "../../Include/ControlDate.inc"-->

//include del file per fare i controlli sulla numericit� dei campi
<!--#include file = "../../Include/ControlNum.inc"-->


function ConvJavaStringToDate(sData) {
		
	var sGiorno
	var sMese
	var sAnno

	if (sData.charAt(0) == "0")
		sGiorno = sData.charAt(1)
	else
		sGiorno = sData.substr(0,2)
			
	if (sData.charAt(3) == "0")
		sMese = sData.charAt(4)
	else
		sMese = sData.substr(3,2)

	if (sData.charAt(6) == "0")
		sAnno = sData.charAt(7)
	else
		sAnno = sData.substr(6,4)

	sDataConv = sMese + "/" + sGiorno + "/" + sAnno 
	return sDataConv
}

function Seleziona(nElem){

	nSelRic	= document.pluto.elements[nElem].selectedIndex
	sSelRic = document.pluto.elements[nElem].options[nSelRic].value

	window.resizeTo(300,450)

	document.pluto.txtSel.value = sSelRic
	sSelRic = "&Valore=" + sSelRic
	Campi(sSelRic)

}


function Ric(){
//	alert (pluto.txtDecod.value)
	Campi("&Risultato=" + pluto.txtDecod.value + "&Decod=YES")
	window.resizeTo(300,550)

}


function RicCod(){
	Campi("&Decod=YES")
	window.resizeTo(300,500)

}

function ControllaDati(sValore,sTipo){
	
	if (sValore == "") {
		return false
	}
	
	if (sTipo == "N") {
		if (!IsNum(sValore)) {
			alert("Il campo deve essere numerico")
			return false
		}		 
	}

	if (sTipo == "D") {
		if (!ValidateInputDate(sValore)) {
			return false
		}
	}


	return true
}	


function Canc() {
	opener.document.forms[0].txtRegTerritoriale.value = ""
}
function Uscita() {
		this.close()
}


function Campi(sCompresa) {
	var nSelTab
	var nSelCampo
	var sDecod
	
	nSelCampo	= document.pluto.elements[0].selectedIndex
	nOper  = document.pluto.elements[1].selectedIndex


	if (sCompresa == "0") {
		window.resizeTo(300,500)
		document.location.href ="CFO_CreaRegola.asp?prVolta=No&Campo=" + 
			document.pluto.elements[0].options[nSelCampo].value +
			"&OpeLog=" + document.pluto.elements[1].options[nOper].value

	}else {
		document.location.href ="CFO_CreaRegola.asp?prVolta=No&Campo=" + 
			document.pluto.elements[0].options[nSelCampo].value + sCompresa +
			"&OpeLog=" +	document.pluto.elements[1].options[nOper].value

	}
}

function Condizione(sSeparatore) {

	var nSelCampo
	var nOperLoc
	var sNomCampo
	var sOperLoc
	var sValore
	var nSelTab	
	var sSelTab 

	var sTabRif
	var arNomTab = new Array()

	
	if (document.pluto.txtSel.value == ""){
		alert("Occorre indidare un valore di condizione")
		document.pluto.txtSel.focus()
		return false
	}
	
	// Mi prendo il campo della tabella
	nSelCampo = document.pluto.elements[0].selectedIndex
	sNomCampo = document.pluto.elements[0].options[nSelCampo].value
	
	// Mi prendo l'operatore logico [Maggiore,Minore,Uguale,Diverso]
	nOperLoc = document.pluto.elements[1].selectedIndex
	sOperLoc = document.pluto.elements[1].options[nOperLoc].value

	if (sOperLoc == "") {
		alert ("Occorre indicare una condizione")
		document.pluto.elements[1].focus()
		return false
	}
	
	// Mi prendo l'operatore logico [AND,OR]
	nOperAndOr = document.pluto.elements[2].selectedIndex
	sOperAndOr = " " + document.pluto.elements[2].options[nOperAndOr].value


	//Mi prendo il valore digitato
	sValore = document.pluto.txtSel.value
	
	// Devo controllare che i dati indicati siano corretti.
	// Se il campo � una data occorre controllare che sia stata indicata una data
	// Se il campo � un numero occorre controllare la validit�.
	// Se non si indica nulla occorre avvertire l'utente che deve indicare un valore
	if (ControllaDati(sValore,sSeparatore)){
	
		// Controllo il tipo del dato ed aggancio al valore 
		// la codifica che SQL indica.
		if (sSeparatore == "N") {
			sSeparatore = ""
		}
		if (sSeparatore == "D") {
		// Trasformo la data in una data valida
			sValore = ConvJavaStringToDate(sValore)
			sSeparatore = "#"
		}
		if (sSeparatore == "A") {
			sSeparatore = "'"
		}

		sValore = sSeparatore + sValore + sSeparatore

		if (opener.document.forms[0].txtRegTerritoriale.value != "") {
			sCondizione	= sOperAndOr + " " + 
							sNomCampo + " " +  
							sOperLoc + " " + sValore
		}
		else {
			sCondizione	= sNomCampo + " " +  
						sOperLoc + " " + sValore
		}

		opener.document.forms[0].txtRegTerritoriale.value = opener.document.forms[0].txtRegTerritoriale.value +  
				sCondizione


	}
}
</Script>
</head>
<Body>
<!--
<body onblur="self.focus()">
-->
<FORM name=pluto>

<table width="260" border=0 cellspacing=2 cellpadding=1>
	<tr> 
		<td colspan=2 align=middle  background=../../images/sfmnsmall.jpg><b><span class="titolo">
			Costruzione regole
		</span></b>
		</td>
	</tr>
</table>
<BR>
<!--#include file ="../../include/openconn.asp"-->
<%

dim sOpeLog

dim sConf
dim sPrVolta
dim sTabella
dim sSQL
dim rsSel
dim CnConn
dim sDecod
dim sRisultato

dim sTipoSorg 
dim	sDesCampo 
dim	sLenCampo 
dim	sTipCampo 
dim sIDCampoRif 
dim sDesCampoRif
dim sValore

sPrVolta	= Request("prVolta")
sConf		= Request("Between")
sDecod		= Request("Decod")
sRisultato	= Request("Risultato")
sValore		= Request("Valore")
sOpeLog		= Request("OpeLog")

sTabella = "PERSONA"
sCampo = Request("Campo")

Response.Write ("<span class='tbltext1'><b><Center>Tabella Persona</span></Center></b>")


%>
<BR>
	<span class="tbltext1"><b><Center>Campi della tabella Persona</span></Center></b>
<%
'Mi prendo le tabelle su cui posso applicare le regole
sSQL = "Select ID_TAB,ID_CAMPO,DESC_CAMPO,LEN_CAMPO," &_
		"ID_CAMPO,TIPO_CAMPO,ID_TAB_RIF,ID_CAMPO_DESC,ID_CAMPO_RIF FROM DIZ_DATI WHERE ID_TAB='" & sTabella & "'"
	
'Response.Write sSQL
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
set rsSel = CC.Execute(sSQL)

Response.Write ("<Select name='cmbCampi' onchange='JAVASCRIPT:Campi(0)'>") 

sDesCampo = rsSel("DESC_CAMPO")
sLenCampo = rsSel("LEN_CAMPO")
sTipCampo = rsSel("TIPO_CAMPO")
sIdCampo =	rsSel("ID_CAMPO")
sTipoSorg = rsSel("ID_TAB_RIF")


sIDCampoRif = rsSel("ID_CAMPO_RIF")
sDesCampoRif= rsSel("ID_CAMPO_DESC")

do while not rsSel.EOF

	if sCampo = rsSel("ID_CAMPO") then
		sIdCampo =	rsSel("ID_CAMPO")
		sDesCampo = rsSel("DESC_CAMPO")
		sLenCampo = rsSel("LEN_CAMPO")
		sTipCampo = rsSel("TIPO_CAMPO")
		sTipoSorg = rsSel("ID_TAB_RIF")

		sIDCampoRif = rsSel("ID_CAMPO_RIF")
		sDesCampoRif= rsSel("ID_CAMPO_DESC")

	end if 

	Response.Write "<OPTION "

	if sCampo = rsSel("ID_CAMPO") then
		Response.Write " selected "
	end if

	Response.write "value ='" & rsSel("ID_CAMPO") &_
			"'> " & rsSel("DESC_CAMPO") & "</OPTION>"
	rsSel.MoveNext 
loop
Response.Write ("</Select>")

rsSel.Close 
'CnConn.Close 



%>
<HR>

	<span class="tbltext1"><b><Center>Condizioni logiche</span></Center></b>
	<Center>
	<SELECT name=cmbCondizione > <!--onchange='JAVASCRIPT:Diverso()'>-->
	<OPTION value=">" 	<%
		if sOpeLog = ">" then
			Response.Write ("selected ")
		end if
	%>
		>Maggiore</OPTION>
	<OPTION value="<"	<%
		if sOpeLog = "<" then
			Response.Write ("selected ")
		end if
	%>
		>Minore</OPTION> 
	<OPTION value="=" 	<%
		if sOpeLog = "=" then
			Response.Write ("selected ")
		end if
	%>
		>Uguale</OPTION>
	<OPTION value="<>"	<%
		if sOpeLog = "<>" then
			Response.Write ("selected ")
		end if
	%>
		>Diverso</OPTION></Center>
<%
'	if sTipCampo = "D" then
%><!--			<OPTION value="BETWEEN">Compresa</OPTION>  -->
<%
'	end if 
%>


	</SELECT>

	<SELECT name=cmbCon >
	<OPTION value='AND' selected>AND</OPTION>
	<OPTION value='OR'>OR</OPTION>
	</SELECT>

<BR>
<HR>
<!--
	<table border="0" cellspacing="0" cellpadding="0" width="300">
		<tr> 
            <td valign="top" align=left width="23"><IMG src="../../images/angolosin.jpg" border=0></td>
			<td class="tbllabel" align=left valign="middle" width="78"><span class="table" ><b><span class="tbltext1" >_</span></Span></b></td>
			<td class="tbllabel" align=center valign="middle" width="469"><span class="table" ><b></b></span></td>
		</tr>
	</table>
-->	<table width="273" cellpadding=2 cellspacing=2 border=0>
		<tr class="tblsfondo">
			<td width="30%" align=left><span class="tbltext">
				Nome campo :
				</span>
			</td>
			<td width="252" align=left>
				<span class="tbltext">
				<%=sIdCampo%>
				</span>
			</td>
		</tr>
		<tr class="tblsfondo">
			<td width="30" align=left><span class="tbltext">
				Lun.&nbsp;del&nbsp;campo
				</span>
			</td>
			<td width="252" align=left>
				<span class="tbltext">
				<%	select case  sTipCampo 
						case "D"					
							Response.Write 10
						case "N"
							Response.Write "11"
						case else
							Response.Write sLenCampo
					end select
				%>
				</span>
			</td>
		</tr>
		<tr class="tblsfondo">
			<td width="30" align=left><span class="tbltext">
				Tipo&nbsp;campo:
				</span>
			</td>
			<td width="252" align=left>
				<span class="tbltext">
				<%select case sTipCampo
					case "D"
						Response.Write "Data (gg/mm/aaaa)"
					case "A"
						Response.Write "Alfanumerico"
					case "N"
						Response.Write "Numerico"
				end select
				%>
				</span>
			</td>
		</tr>

		<%if sTipoSorg <> "" then%>
			
			<tr class="tblsfondo">
				<td width="30" align=left><span class="tbltext">
					Tab.&nbsp;riferimento:
					</span>
				</td>
				<td width="252" align=left >
					<span class="tbltext">
					<A onclick="RicCod()">
					<FONT color=red > 
					<%
					
					if mid(sTipoSorg,1,3) = "TAB" then
						Response.Write ("<U>" & mid(sTipoSorg,5) & "</U>")
					else
						Response.Write ("<U>" & sTipoSorg & "</U>")
						
					end if
					%>
					</Font>
					</A>
					</span>
				</td>
			</TR>
			<TR>
				<td width="252" align=Center>
					&nbsp;
					<span class="tbltext">
					</span>
				</td>
				
				<td width="252" align=Left>
					<span class="tbltext">
					<input type="button" name="Ricerca" 
						value="Decod" onclick="RicCod()">
					</span>
				</td>

				
			</tr>
		<%
		else 
			if sDesCampoRif <> "" then 
			sTipoSorg = "Combo"
		%>
				<td width="252" align=Center>
					&nbsp;
					<span class="tbltext">
					</span>
				</td>
			<td width="252" align=Left>
				<span class="tbltext">
				<input type="button" name="Ricerca" 
					value="Decod" onclick="RicCod()">
				</span>
			</td>
<%
			end if 
		end if 
%>
</TABLE>


<%if sConf = "Yes" then%>
	fra 
	<input type="text" name="txtData1" size=8 maxlength=10>
	 a 
	<input type="text" name="txtData2" size=8 maxlength=10>

<%else%>
	Valore <input type="text" size=15 name="txtSel" value="<%=sValore%>">
<%end if%>

<input type="button" name="Ok" 
			value="Ok" onclick="JAVASCRIPT:Condizione('<%=sTipCampo%>')">


<BR>
<BR>
<input type="button" 
		name="Esci" 
			value="Esci"
			 onclick='JAVASCRIPT:Uscita()'>

<input type="button" 
		name="Cancella" 
			value="Azzera regola"
			 onclick='JAVASCRIPT:Canc()'>

</CENTER>

<%if sDecod = "YES" and sTipoSorg <> "Combo" then%>
		<HR>

		<span class="tbltext1"><b><Center>Indicare una breve descrizione </span></b>
		<BR>

		<BR>
		<input type="text" name="txtDecod" size=15 maxlength=15 value="<%=sRisultato%>">

		<input type="button" 
				name="ritrova" 
					value="Ricerca"
					 onclick='JAVASCRIPT:Ric()'>
		</Center>

<%
	end if 	
	if  sTipoSorg = "Combo" and sDecod ="YES" then ' Costruisco la combo con i soli valori
%>
		<HR>
		<span class="tbltext1"><b><Center>Selezionare il codice desiderato</span></Center></b>
<%	
		dim sValoriArray
		sValoriArray = Split(sDesCampoRif, "|", -1, 1)
		
		Response.Write ("<Center><Select name='cmbRis' onchange='JAVASCRIPT:Seleziona(8)'>") 
		Response.Write ("<OPTION selected></Option>")

		for z = 0 to (UBound(sValoriArray)-1)

		Response.Write ("<Option value='" & sValoriArray(z) & "'>" & sValoriArray(z+1) & "</Option>")
		z=z+1
		next

		Response.Write ("</Select></Center>") 
	
	end if
%>
	 
<%if sRisultato <> "" then%>
		<span class="INPUT">
		<HR>
		<%
		' Craere la select con i dati che l'utente ha inserito
		if mid(sTipoSorg,1,3) = "TAB" then
			
			sIDCampoRif  = "CODICE"
			sDesCampoRif = "DESCRIZIONE"
			
			sSQL = "Select " & sIDCampoRif & "," & sDesCampoRif &_
				 " FROM TADES WHERE Nome_Tabella='" & mid(sTipoSorg,5) & "' AND " &_
				 " DESCRIZIONE Like '" & sRisultato & "%' ORDER BY DESCRIZIONE"
		else

		sSQL = "Select " & sIDCampoRif & "," & sDesCampoRif &_
			 " FROM " & sTipoSorg & " WHERE " & sDesCampoRif &_
				 " Like '" & sRisultato & "%'"
		end if

		'Response.Write sSQL

'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
		set rsSel = CC.Execute(sSQL)
'		rsSel.Open sSQL,CnConn

		if rsSel.EOF = true then
		%>
		<span class="tbltext1"><b><Center>Non esistono codici per la regola indicato</span></Center></b>
		<%else%>
		<span class="tbltext1"><b><Center>Selezionare il codice desiderato</span></Center></b>
		<%
			Response.Write ("<CENTER>")

			Response.Write ("<Select name='cmbRis' onchange='JAVASCRIPT:Seleziona(10)'>") 
			Response.Write "<OPTION selected></Option>"

			do while not rsSel.EOF

				Response.Write "<OPTION "

				if sCampo = rsSel(sIDCampoRif) then
					Response.Write " selected "
				end if

				Response.write "value ='" & rsSel(sIDCampoRif) &_
						"'> " & rsSel(sIDCampoRif) & " - " & rsSel(sDesCampoRif) & "</OPTION>"
				rsSel.MoveNext 
			loop
			Response.Write ("</Select></CENTER>")

		end if 

		rsSel.Close
		'CnConn.Close
	end if 	
%>
		</span>
<!--#include file ="../../include/closeconn.asp"-->
		
</FORM>
</body>
</Html>
