<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include virtual="/strutt_testa2.asp"-->
<!--#include virtual="/include/openconn.asp"-->
<!--#include virtual="/util/portallib.asp"-->

<!-- ************** Javascript inizio ************ -->

<script>
	function VaiInizio(){
	   document.frmConf.submit();		
	}
</script>
<!-- ************** Javascript Fine ************ -->

<!-- ************** ASP inizio *************** -->


<!--#include virtual = "/util/dbutil.asp"-->
<!--#include virtual = "/include/SysFunction.asp"-->
<!--#include virtual = "/include/DecComun.asp"-->

<%
Sub Msgetto(Msg)
%>
	<br>
	<table align="center">
		<tr>
			<td class="tbltext3"><b><%=Msg%></b></td>
		</tr>	
	</table>
<%
End Sub

'-------------------------------------------------------------------------------------------------------------------------------	

sub Inizio()
%>
	<table border="0" width="520" cellspacing="0" cellpadding="0" height="81">
	   <tr>
	     <td width="500" background="<%=Session("Progetto")%>/images/titoli/strumenti2b.gif" height="81" valign="bottom" align="right">
	       <table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
	         <tr>
	           <td width="100%" valign="top" align="right">
	           <b class="tbltext1a">Gestione Centri Di Formazione</b></td>	         
			</tr>
	       </table>
	     </td>
	   </tr>
	</table>
	<br>
	<table cellpadding="0" cellspacing="0" width="500" border="0">
		<tr height="18">
			<td class="sfondomenu" height="18" width="67%">
			<span class="tbltext0"><b>&nbsp;GESTIONE CENTRI DI FORMAZIONE</b></span></td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
			<td valign="middle" align="right" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1"></td>
		</tr>
		<tr width="371" class="SFONDOCOMM">
			<td colspan="3"> &nbsp;
			Modifica/Inserimento Centro di formazione
			</td>
		</tr>
		<tr class="SFONDOCOMM"><td colspan="3">&nbsp;</td></tr>
		<tr height="2">
			<td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>
	<br>
<%

end sub

'------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

sub GetParam()
	sModo = Request("MODO")
	
	
	sAction		= Request.Form ("txtAction")

	sDescCForm	= Ucase (Request.Form("txtDescCForm"))
	sIndirizzo	= Ucase (Request.Form("txtIndirizzo"))
	
	sComune		= Ucase (Request.Form("txtComuneHid"))
	
	sProv	= Ucase (Request("txtProvincia"))
	sCap		= Ucase (Request.Form("txtCap"))
	sNumTel		= Ucase (Request.Form("txtNumTelefono"))
	sFax		= Ucase (Request.Form("txtFax"))
	sEmail		= Ucase (Request.Form("txtEmail"))
	sRegolaTerr	= Ucase (Request.Form("txtRegTerritoriale"))
	sNumAule	= Ucase (Request.Form("txtNumAule"))
	sIdImpiego	= Ucase (Request.Form("txtIDImpiego"))

	sDescCForm	= Replace (sDescCForm,Chr(34),"'")
	sDescCForm	= Replace (sDescCForm,"'","''")

	sIndirizzo	= Replace (sIndirizzo,"'","''")
	sComune		= Replace (sComune,"'","''")
	sProv	= Replace (sProv,"'","''")
	'sProv = Request ("cmbProv")
	sCap		= Replace (sCap,"'","''")
	sNumTel		= Replace (sNumTel,"'","''")
	sFax		= Replace (sFax,"'","''")
	sEmail		= Replace (sEmail,"'","''")
	sNumAule	= Replace (sNumAule,"'","''")
	sIdImpiego	= Replace (sIdImpiego,"'","''")

	if sNumAule = "" then
		sNumAule = 0
	end if 

end sub

'------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

sub Inserisci()

	sSQL = "SELECT COUNT(*) AS CONTA FROM CENTRO_FORMAZIONE WHERE " &_
			"DESC_CFORM = '" & sDescCForm &_
			"' AND COMUNE ='" & sComune & "'"

'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	set rsConta = CC.Execute(sSQL)
	nConta = clng(rsConta("CONTA"))
	rsConta.Close			

	if nConta = 0 then 			
		sSQL = "INSERT INTO CENTRO_FORMAZIONE " &_
			"(DESC_CFORM, INDIRIZZO, COMUNE, PRV, CAP, " &_
			"NUM_TEL, FAX, E_MAIL, NUM_AULE, DT_TMST)  VALUES " &_
			"('" & sDescCForm  & _
			"','" & sIndirizzo & _
			"','" & sComune & _
			"','" & sProv & _
			"','" & sCap & _
			"','" & sNumTel & _
			"','" & sFax & _
			"','" & sEmail & _
			"'," & clng(sNumAule) &_
			"," & ConvDateToDb(Now()) & ")"

		Errore=Esegui("Nuovo Record","CENTRO_FORMAZIONE",Session("idutente"),"INS", sSQL, 1,Now())
	else
		Errore="Esiste un centro di formazione con la stessa descrizione"
	end if 	

	if Errore = "0" then
		'Msgetto("Acquisizione nuovo Centro di Formazione<br><div align=center width=500>correttamente effettuata</div>")
%>
		<script>
		    alert("Acquisizione nuovo Centro di Formazione correttamente effettuata");	
		
		    VaiInizio()
		</script>		
<%
		sSQL = "Select max(ID_CFORM) FROM CENTRO_FORMAZIONE WHERE PRV ='" &_
			sProv & "' AND DESC_CFORM = '" & sDescCForm & "'" 
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
		set rsCFORM = CC.Execute(sSQL)
%>
		<br>		
<%
		rsCFORM.Close
		set rsCFORM = nothing
	else
		Msgetto(Errore)
	end if

end sub
%>

<%
sub Modifica()

	sDtTmst		= Request.Form("DT_TMST")	
	
	sIdCenForm	= Request.Form("txtIdCenForm")

	sSQL = "SELECT COUNT(*) AS CONTA FROM CENTRO_FORMAZIONE WHERE " &_
			"DESC_CFORM = '" & sDescCForm & _
			"' AND COMUNE ='" & sComune  & _
			"' AND ID_CFORM <> " & sIdCenForm
	'Response.Write sSQL
			
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	set rsConta = CC.Execute(sSQL)
	nConta = clng(rsConta("CONTA"))
	rsConta.Close			
	
	if nConta = 0 then 			
		sSQL = "UPDATE CENTRO_FORMAZIONE " &_
				"SET " &_
				"DESC_CFORM='"	& sDescCForm &_
				"', INDIRIZZO='"& sIndirizzo &_
				"', COMUNE='"	& sComune &_
				"', PRV='"		& sProv &_
				"', CAP='"		& sCap &_
				"', NUM_TEL='"	& sNumTel &_
				"', FAX='"		& sFax &_
				"', E_MAIL='"	& sEmail &_
				"', DT_TMST ="	& ConvDateToDb(Now()) & _
				" WHERE ID_CFORM=" & clng(sIdCenForm)
	'Response.Write "sSQL: " & sSQL & "<br>"

		Errore=Esegui("Prova","CENTRO_FORMAZIONE",Session("idutente"),"MOD", sSQL, 1, sDtTmst)
	else
		Errore="Esiste un centro di formazione con la stessa descrizione"
	end if
	'----------------------------------------------------------------
	
	if Errore = "0" then
		
		%>
		<script>
		    alert("Operazione correttamente effettuata");	
		
		    VaiInizio()
		</script>
		
		<%
	else
		if Errore="<BR>Record aggiornato da altro utente." then
			CC.RollbackTrans
		end if
	'----------------------------------------------------------------
	''if Errore = "0" then
	''	Response.Write sProv
		'VaiInizio(sProv)
	''else
	'	if Errore="<BR>Record aggiornato da altro utente." then
	''		CC.RollbackTrans
	''	end if
		Msgetto(Errore)
	end if

end sub

'------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

sub Elimina()

	sIdCenForm = Request.QueryString("IdCenForm")
	
	sSQL = "select COUNT(*) AS CONTA " & _
		   "from classe a, aula b " & _
		   "where a.id_aula = b.id_aula and " & _
		   "b.id_cform =" & clng(sIdCenForm)
'	Response.Write sSQL

'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	set rsConta = CC.Execute(sSQL)
	nConta = clng(rsConta("CONTA"))
	rsConta.Close

	CC.BeginTrans
	if nConta = 0 then
		sSQL = "DELETE FROM CENTRO_FORMAZIONE WHERE ID_CFORM=" & CLng(sIdCForm) 
		Errore=EseguiNoC(sSQL,CC)
		if Errore = "0" then
			sSQL = "DELETE FROM AULA WHERE ID_CFORM=" & CLng(sIdCForm) 
			Errore=EseguiNoC(sSQL,CC)
'				Errore = "Cancellazione correttamente effettuata"
		end if
	else
		Errore = "Sono state gi� formate delle classi per il centro di formazione selezionato"
	end if			

	if Errore = "0" then
		CC.CommitTrans
		Msgetto("Cancellazione correttamente effettuata")
	else
		CC.RollbackTrans
		Msgetto(Errore)
	end if
	
end sub

'------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

sub Fine()
%>
	<br>
	<br>
	<table border="0" cellpadding="0" cellspacing="1" width="500">
		<tr>
			<td align="center">
				<!--a href="javascript:history.back()">	<img src="<%=session("Progetto")%>/images/indietro.gif" border="0" name="imgPunto2" onmouseover="javascript:window.status=' '; return true">				</a-->
				<a href="javascript:VaiInizio()"><img src="<%=session("Progetto")%>/images/indietro.gif" border="0" name="imgPunto2" onmouseover="javascript:window.status=' '; return true">
				<%'Response.Write sProv & "=sProv"%>

				</a>
			</td>
		</tr>
	</table>
	<br>
<!--#include virtual = "/include/closeconn.asp"-->
<!--#include virtual = "strutt_coda2.asp"-->
<%

end sub
%>
<!-- ************** ASP Fine *************** -->

<!-- ************** MAIN Inizio ************ -->


<%
dim sAction

dim sDescCForm
dim sIndirizzo
dim sComune
dim sProv
dim sCap
dim sNumTel
dim sFax
dim sEmail
dim sRegolaTerr
dim sIdCenForm
dim Errore

dim sNumAule
dim sIdImpiego
dim sDtTmst

dim sSQL
dim sModo

	Inizio()
	GetParam()
	%>
	<form name="frmConf" method="post" action="CFO_VisCentroFormazione.asp">
       <input type="hidden" name="cmbProv" value="<%=sProv%>">
       <input type="hidden" name="MOD" value="1">
    </form>
	<%
	if sModo = "CANC" then
		sAction = "CANC"
		sComDecod = "H501"
		sComune = sComDecod
	end if
	
	select case sAction 

		case "INS" 
			Inserisci()
				
		case "MOD" 
			Modifica()
			
		case "CANC" 
			Elimina()
			
	end select
	
	Fine()
	
%>

<!-- ************** MAIN Fine ************ -->
