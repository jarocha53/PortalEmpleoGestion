<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #include virtual="/strutt_testa2.asp"-->
<!--#include virtual = "/include/openconn.asp"-->
<!--#include virtual="/util/portallib.asp"-->
<%
if ValidateService(session("idutente"),"CFO_VISCENTROFORMAZIONE", CC) <> "true" then 
	response.redirect "/util/error_login.asp"
end if
%>

<!-- ************** Javascript Inizio ************ -->

<script LANGUAGE="Javascript">

<!--#include virtual = "/include/Help.inc"-->
<!--#include virtual = "/include/SelComune.js"-->

//include del file per fare i controlli sulla numericit� dei campi
<!--#include virtual ="/Include/ControlNum.inc"-->

//include del file per fare i controlli sulle stringhe
<!--#include virtual ="/Include/ControlString.inc"-->		

function VaiInizio(sPrv){
	location.href = "CFO_VisCentroFormazione.asp?MOD=1&cmbProv=" + sPrv;
}	

//--------------------------------------------------------------------------------------------------------------------------------------

function Elimina(nIdCenForm){
	if (confirm("Sei sicuro di voler eliminare questo centro di formazione?"))
	{
		location.href = "CFO_CfnCentroFormazione.asp?MODO=CANC&IdCenForm=" + nIdCenForm;
		//alert("CFO_CfnAule.asp?idCForm=" + nIdCenForm)
	}
}

//--------------------------------------------------------------------------------------------------------------------------------------

function InsAule(nIdCenForm){
	location.href = "CFO_VisAuleold.asp?idCForm=" + nIdCenForm + "&Rientro=null";
}

//--------------------------------------------------------------------------------------------------------------------------------------

function InsAuleRI(nIdCenForm){
	location.href = "CFO_VisAuleold.asp?idCForm=" + nIdCenForm + "&Rientro=RI";
}

//--------------------------------------------------------------------------------------------------------------------------------------

//function insRegole(){
//	newWindow = window.open('CFO_CreaRegola.asp','newWind','width=290,height=380,Resize=No')
//	return false
//}

//--------------------------------------------------------------------------------------------------------------------------------------

function ControllaDati(myForm){

	myForm.txtDescCForm.value = TRIM(myForm.txtDescCForm.value)
	if (myForm.txtDescCForm.value == "") {
		alert("Campo obbligatorio")
		myForm.txtDescCForm.focus() 
		return false
	}
	
	myForm.txtComune.value=TRIM(myForm.txtComune.value)
	if (myForm.txtComune.value == "") {
		alert("Campo obbligatorio")
		myForm.txtComune.focus() 
		return false
	}
	
	myForm.txtCAP.value=TRIM(myForm.txtCAP.value)
	if (IsNum(myForm.txtCAP.value) != true){
		alert("Inserire un valore numerico!")
		myForm.txtCAP.focus() 
		return false
	}
	//CAP deve essere diverso da zero
	if (eval(myForm.txtCAP.value) == 0){
		alert("Il CAP non pu� essere zero!")
		myForm.txtCAP.focus()
		return false
	}

	if (myForm.txtCAP.value.length != 5){
		alert("Formato del CAP errato.")
		myForm.txtCAP.focus() 
		return false
	}
	
	myForm.txtIndirizzo.value=TRIM(myForm.txtIndirizzo.value)

	myForm.txtNumTelefono.value = TRIM(myForm.txtNumTelefono.value)
	if (!IsNum(myForm.txtNumTelefono.value)){
		alert("Il telefono deve essere numerico!");
		myForm.txtNumTelefono.focus(); 
		return false;
	}

	myForm.txtFax.value=TRIM(myForm.txtFax.value)
	if (!IsNum(myForm.txtFax.value)){
		alert("Il FAX deve essere numerico!");
		myForm.txtFax.focus(); 
		return false;
	}
		
	myForm.txtEmail.value=TRIM(myForm.txtEmail.value)
	if (myForm.txtEmail.value != ""){
		if (ValidateEmail(myForm.txtEmail.value) != true){
			alert("Inserire un indirizzo E-Mail corretto!")
			myForm.txtEmail.focus() 
			return false
		}
	}
	return true
}
</script>

<!-- ************** Javascript Fine ************ -->

<!-- ************** ASP Inizio *************** -->

<!--#include virtual = "/include/DecComun.asp"-->

<%
sub Inizio()
%>
	<table border="0" width="520" cellspacing="0" cellpadding="0" height="81">
	   <tr>
	     <td width="500" background="<%=Session("Progetto")%>/images/titoli/strumenti2b.gif" height="81" valign="bottom" align="right">
	       <table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
	         <tr>
	           <td width="100%" valign="top" align="right"><b class="tbltext1a">Gestione Progetti</span></b></td>
	         </tr>
	       </table>
	     </td>
	   </tr>
	</table>
	<br>
	<table cellpadding="0" cellspacing="0" width="500" border="0">
		<tr height="18">
			<td class="sfondomenu" height="18" width="67%">
			<span class="tbltext0"><b>&nbsp;GESTIONE CENTRI DI FORMAZIONE</b></span></td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
			<td valign="middle" align="right" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1">(*) campi obbligatori</td>
		</tr>
		<tr width="371" class="SFONDOCOMM">
			<td colspan="3">
			Modifica dei dati relativi al Centro di Formazione. <br>
			Premere <b>Invia</b> per salvare le modifiche. 
			<a title="Help" href="Javascript:Show_Help('/Pgm/help/GestProgetti/Alfabeta/CFormazione/CFO_ModCentroFormazione')" onmouseover="javascript:window.status=' '; return true"><img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a></td>
		</tr>
		<tr height="2">
			<td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>
	<br>			
	<br>
<%
end sub

'----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

sub ImpostaPag()

	nIdCenForm = Request("CForm")
	DescCenForm = Request ("DescCForm")
	
	sSQL = "SELECT ID_CFORM, DESC_CFORM, INDIRIZZO," &_
		   "COMUNE, PRV, CAP, NUM_TEL, FAX, E_MAIL, NUM_AULE, DT_TMST " & _
		   "FROM CENTRO_FORMAZIONE WHERE ID_CFORM=" & clng(nIdCenForm)
	'Response.Write sSQL
	
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	set rsCenForm = CC.Execute(sSQL)
	sProv = rsCenForm("PRV")	
	
%>
<form name="frmModCForm" method="POST" onsubmit="return ControllaDati(this)" action="CFO_CfnCentroFormazione.asp">
	<input type="hidden" value="<%=rsCenForm("DT_TMST")%>" name="DT_TMST">
  	<input type="hidden" name="txtIdCenForm" value="<%=rsCenForm("ID_CFORM")%>">
  	<input type="hidden" name="txtAction" value="MOD">

	<br>
	<table border="0" cellpadding="1" cellspacing="2" width="500">
		<tr class="tbltext1">
  			<td align="left">
  				<b>Descrizione*</b>
			</td>
      		<td align="left">
				<input style="TEXT-TRANSFORM: uppercase;" class="textblack" size="50" maxlength="50" name="txtDescCForm" value="<%=rsCenForm("DESC_CFORM")%>">
			</td>
		</tr>
		<tr class="tbltext1">
  			<td align="left">
  				<b>Indirizzo</b>
			</td>
  			<td align="left">
				<input style="TEXT-TRANSFORM: uppercase;" class="textblack" size="50" maxlength="50" name="txtIndirizzo" value="<%=rsCenForm("INDIRIZZO")%>">
			</td>
		</tr>
		<tr class="tbltext1">
  			<td align="left">
  				<b>Provincia</b>
			</td>
  			<td align="left" class="tbltext">
				<b><%=sProv%></b>
			</td>
		</tr>
		
		<input type="hidden" name="txtProvincia" value="<%=rsCenForm("PRV")%>">					
		
		<tr class="tbltext1">
  			<td align="left">
  				<b>Comune*</b>
			</td>
			<td nowrap>
				<span class="tbltext">
				<input type="text" name="txtComune" style="TEXT-TRANSFORM: uppercase;" class="textblack" size="35" readonly value="<%=DescrComune(rsCenForm("COMUNE"))%>">
				<input type="hidden" id="txtComuneHid" name="txtComuneHid" value="<%=rsCenForm("COMUNE")%>">
<%
				NomeForm="frmModCForm"
				CodiceProvincia="txtProvincia"
				NomeComune="txtComune"
				CodiceComune="txtComuneHid"
				Cap="txtCAP"
%>
				<a title="Seleziona Comune" href="Javascript:SelComune('<%=NomeForm%>','<%=CodiceProvincia%>','<%=NomeComune%>','<%=CodiceComune%>','<%=Cap%>')" ID="imgPunto1" name="imgPunto1" onmouseover="javascript:window.status='';return true"><img border="0" src="<%=Session("Progetto")%>/images/bullet1.gif"></a>
				</span>				
			</td>
		</tr>
		<tr class="tbltext1">
  			<td align="left">
  				<b>CAP</b>
			</td>
  			<td align="left">
  				<input style="TEXT-TRANSFORM: uppercase;" size="6" maxlength="5" class="textblack" name="txtCAP" value="<%=rsCenForm("CAP")%>">
			</td>
		</tr>
		<tr class="tbltext1">
  			<td align="left">
  				<b>Numero Telefonico</b>
			</td>
  			<td align="left">
  				<input style="TEXT-TRANSFORM: uppercase;" size="20" maxlength="20" class="textblack" name="txtNumTelefono" value="<%=rsCenForm("NUM_TEL")%>">
			</td>
		</tr>
		<tr class="tbltext1">
  			<td align="left">
  				<b>FAX</b>
			</td>
  			<td align="left">
  				<input style="TEXT-TRANSFORM: uppercase;" size="20" maxlength="20" class="textblack" name="txtFax" value="<%=rsCenForm("FAX")%>">
			</td>
		</tr>
		<tr class="tbltext1">
  			<td align="left">
  				<b>E_MAIL</b>
			</td>
  			<td align="left">
  				<input style="TEXT-TRANSFORM: uppercase;" size="30" maxlength="30" class="textblack" name="txtEmail" value="<%=rsCenForm("E_MAIL")%>">
			</td>
		</tr>
    </table>
    <br>
	<!--impostazione dei comandi-->
	<table cellpadding="0" cellspacing="0" width="300" border="0">	
		<tr align="center">
			<td nowrap><input title="Conferma" type="image" name="Conferma" src="<%=Session("progetto")%>/images/conferma.gif"></td value="Registra">
			<td nowrap><a title="Annulla" href="javascript:document.frmModCForm.reset()"><img border="0" src="<%=Session("progetto")%>/images/annulla.gif"></td>
			<td nowrap><a title="Torna Indietro" href="javascript:history.go(-1)"><img src="<%=session("Progetto")%>/images/indietro.gif" border="0" name="imgPunto2" onmouseover="javascript:window.status=' '; return true"></a></td>
		</tr>
	</table>
  	
  	<br>

</form>

<%rsCenForm.Close

end sub

'----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

sub Fine()
%>
<!--#include virtual = "/include/closeconn.asp"-->
<!--#include virtual = "strutt_coda2.asp"-->
<%
end sub%>

<!-- ************** ASP Fine *************** -->

<!-- ************** MAIN Inizio ************ -->
<%
dim nIdCenForm
dim sSQL
dim rsCenForm
dim sProv
dim DescCenForm

	Inizio()
	ImpostaPag()
	Fine()
	
%>
<!-- ************** MAIN Fine ************ -->
