<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->

<!-- #include virtual="/strutt_testa2.asp"-->
<!--#include virtual ="/include/openconn.asp"-->
<!--#include virtual="/util/portallib.asp"-->
<%
if ValidateService(session("idutente"),"REP_RICCANDGRAD", CC) <> "true" then 
	response.redirect "/util/error_login.asp"
end if
%>
<!--	BLOCCO SCRIPT		-->
<script language="Javascript">

	<!--#include virtual = "/Include/Help.inc"-->

	function Ritorno(){
		location.href="REP_RicCandGrad.asp";
	}

	function Scarica(report){
		//f="temp\\"+ report;
		fin=window.open(report,"","toolbar=0;location=0,directories=0,status=0,menubar=1,scrollbars=1,resizable=1,copyhistory=0,width=450,height=340,screenX=200,screenY200");
	}
</script>
<!-----	FINE BLOCCO SCRIPT	-->

<!------BLOCCO ASP-------->

	<!--#include virtual = "/include/ckProfile.asp"-->
	<!--#include virtual = "/include/DecCod.asp"-->
    <!--#include virtual = "/Util/DBUtil.asp"-->
	<!--#include virtual = "/include/ControlDateVB.asp"-->	
   	<!--#include virtual = "/include/SelAreaTerrBandi.asp"-->	
<%SUB OpzioniDaSelezionare()%>
		 <br>
	     <table width="500" border="0">
			<tr>
			<%IF flagEsitoReport="OKAmmOKNonAmm" OR flagEsitoReport="OKAmmKONonAmm" THEN%>
				<td align="middle">
					  <a href="javascript:Scarica('<%=sPathFile & FileAmmessi%>')"><span class="textred"><b>Candidati Ammessi</b></span></a>&nbsp; 
				</td>
			<%END IF
			  IF flagEsitoReport="OKAmmOKNonAmm" OR flagEsitoReport="KOAmmOKNonAmm" THEN
			%>
				<td align="middle">
					  <a href="javascript:Scarica('<%=sPathFile & FileNonAmmessi%>')"><span class="textred"><b>Candidati Non Ammessi</b></span></a>&nbsp;
				</td>
			<%END IF%>
			</tr>
		 </table>
		 <br>
<%END SUB %>	 
<%
sub AcquisizioneDati()
    sBando= Request("cmbBando")
    nIdBando=sBando
    DescBando=DecBando(sBando) 
   
	sSede = trim(cstr(Request("cmbSede")))
	
	if sSede = "" then
		nSede = "0" 
		DescSede= ""
	else		
		nSede = Mid(sSede,1,InStr(1,sSede,"|") -1)	
		DescSede= Mid(sSede,InStr(1,sSede,"|") +1)	 
   end if
'---------- Fine Acquisizione Descrizione della  Sede -------
end sub %>
<%
  sub Messaggio(sMessaggio)%>
	<br>
	<table width="500">
		<tr><td align="center" class="tbltext3"><b><%=sMessaggio%></b></td></tr>	
	</table>
	<br>
<%end sub%>

<%Sub Inizio()%>	
	<table border="0" width="520" cellspacing="0" cellpadding="0" height="81">
	   <tr>
	     <td width="500" background="<%=Session("Progetto")%>/images/titoli/strumenti2b.gif" height="81" valign="bottom" align="right">
	       <table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
	         <tr>
	           <td width="100%" valign="top" align="right"><b class="tbltext1a">Gestione Progetti</span></b></td>
	         </tr>
	       </table>
	     </td>
	   </tr>
	</table>
	<br>
	<table cellpadding="0" cellspacing="0" width="500" border="0">
		<tr height="18">
			<td class="sfondomenu" height="18" width="67%">
			<span class="tbltext0"><b>&nbsp;REPORT GRADUATORIA</b></span></td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
			<td valign="middle" align="right" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1"></td>
		</tr>
		<tr width="371" class="SFONDOCOMM">
			<td colspan="3">Per visualizzare i report dei candidati cliccare sui link sottostanti<a href="Javascript:Show_Help('/Pgm/help/GestProgetti/Alfabeta/Graduatoria/REP_VisCandGrad')" onmouseover="javascript:window.status=' '; return true"><img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
			</td>
		</tr>
		<tr height="2">
			<td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
		</tr>
	</table>
	<br>
<% End Sub %>
<!--------------------------------------------------------------------------------------------------------------------------------------------------------->
<%Sub Fine()%>
		<!----------------Torna alla Pagina Iniziale ------------->
		 <table cellpadding="0" cellspacing="0" width="500" border="0">	
		    	<tr>
					<td nowrap align="center"><a href="javascript:Ritorno()" onmouseover="javascript:window.status=' '; return true"><img src="/PLAVORO/images/indietro.gif" border="0" name="imgPunto2"></a><td>
			    </tr>
		 </table>
	   	 <br>
		<!---------------------------------------------> 
	    <!--#include virtual ="/include/closeconn.asp"-->
		<!--#include virtual = "/strutt_coda2.asp"-->
<%End Sub %>	
<!--------------------------------------------------------------------------------------------------------------------------------------------------------->
<%
Sub ImpostaPag()
	FileAmmessi =   Session("idutente") & "_" & nIdBando & "_" & nSede & "_Ammessi.xls"
	FileNonAmmessi = Session("idutente") & "_" & nIdBando & "_" & nSede & "_Non_Ammessi.xls"
	lPath= Server.MapPath("/") & session("progetto") & "/DocPers/Temp/" 

	sPathFile = session("progetto") & "/DocPers/Temp/"	
	
	'*************************************************
	' Report dei candidati ammessi 
	'*************************************************
	 'sDescTit =""
		SQL="SELECT  P.COD_FISC,P.COGNOME,P.NOME, " & _
				"TO_CHAR(P.DT_NASC,'DD/MM/YYYY') AS DT_NASC, " & _
				"TO_CHAR(D.PUNTEGGIO,'990.99') AS PUNTEGGIO " & _
			"FROM PERSONA P, DOMANDA_ISCR D " & _ 
			"WHERE D.ID_BANDO = " & nIdBando & " AND D.ID_SEDE =" & nSede & _
			"AND P.ID_PERSONA = D.ID_PERSONA " & _			
			"AND D.COD_ESITO IS NULL " & _
			"AND D.ESITO_GRAD IN('S','X') " & _
		    " ORDER BY D.PUNTEGGIO DESC, D.DT_DOM"  
            
		'Response.Write sql        
					
		set RRAmm=server.CreateObject("ADODB.Recordset")			

'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
		RRAmm.Open SQL, CC, 1

		If RRAmm.eof Then
			flagEsitoReport ="KOAmm"
		else
		    'sDescTit =DecCodVal("LSTUD",0,DATE(),RRAmm("COD_LIV_STUD"),1) 
			Set fso = CreateObject("Scripting.FileSystemObject")
			Set a = fso.CreateTextFile(lPath & FileAmmessi, True)
									
			a.WriteLine "Elenco Candidati Ammessi per il bando " & DescBando & "-" & DescSede &" - Situazione al " & day(Now) & "/" & month(now) & "/" & year(now) & " Ore " & hour(now) & ":" & minute(now)
			a.WriteLine ""
			a.WriteLine "Progressivo " & chr(9) & _
			            "Codice Fiscale" & chr(9) & _
						"Cognome" & chr(9) & _
						"Nome" &  chr(9) & _
						"Data di nascita" & chr(9) & _ 						
                        "Punteggio" 
            a.WriteLine ""
            ID=1
            
            do until RRAmm.EOF
	              a.WriteLine ID & chr(9) & RRAmm("COD_FISC") & chr(9)& RRAmm("COGNOME") & chr(9)& RRAmm("NOME") & chr(9)& RRAmm("DT_NASC") & chr(9) & RRAmm("PUNTEGGIO")
                  id = id + 1             
			      rramm.MoveNext
	        loop
	           
	        RRAmm.Close
	        set RRAmm=nothing
	        
	       ' a.WriteLine rptIscritti
			a.Close
		   	flagEsitoReport ="OKAmm"
	    end if	
	'***************************************************	
	'	report dei candidati non ammessi 	
   	'***************************************************
	  ' sDescTit =""		
	   SQL1="SELECT P.COD_FISC,P.COGNOME,P.NOME," & _
				   "TO_CHAR(P.DT_NASC,'DD/MM/YYYY')AS DT_NASC, " & _
				   "TO_CHAR(D.PUNTEGGIO,'990.99')AS PUNTEGGIO " & _			
			 "FROM PERSONA P, DOMANDA_ISCR D " & _ 
			 "WHERE D.ID_BANDO = " & nIdBando & " AND D.ID_SEDE =" & nSede & _
			 "AND P.ID_PERSONA = D.ID_PERSONA " & _				   				 
			 "AND (D.ESITO_GRAD ='N' or D.COD_ESITO IS NOT NULL) " & _
				  		    
		    "ORDER BY D.PUNTEGGIO DESC, D.DT_DOM"            
            
	'Response.Write sql1
	'Response.End 
					
		set RRNonAmm=server.CreateObject("ADODB.Recordset")			

'PL-SQL * T-SQL  
SQL1 = TransformPLSQLToTSQL (SQL1) 
		RRNonAmm.Open SQL1, CC, 1

		If RRNonAmm.eof Then
		   flagEsitoReport = flagEsitoReport  & "KONonAmm"
		else
			'sDescTit =DecCodVal("LSTUD",0,DATE(),RRNonAmm("COD_LIV_STUD"),1) 
			Set fso1 = CreateObject("Scripting.FileSystemObject")
			Set b = fso1.CreateTextFile(lPath & FileNonAmmessi, True)
			
		
			b.WriteLine "Elenco Candidati Non Ammessi per il bando " & DescBando & "-" & DescSede &" - Situazione al " & day(Now) & "/" & month(now) & "/" & year(now) & " Ore " & hour(now) & ":" & minute(now)
			b.WriteLine ""
			b.WriteLine "Progressivo" & chr(9) & _
			            "Codice Fiscale" & chr(9) & _
						"Cognome" & chr(9) & _
						"Nome" &  chr(9) & _
						"Data di nascita" & chr(9) & _ 
						"Punteggio" 
            b.WriteLine ""            
	      '  rptIscritti=RRNonAmm.GetString (2,,chr(9) ,chr(13) ," ")
	        ID = 1
	        do until rrNonamm.EOF
	        
	            b.WriteLine ID & chr(9) & RRNonAmm("COD_FISC") & chr(9)& RRNonAmm("COGNOME") & chr(9)& RRNonAmm("NOME") & chr(9)& RRNonAmm("DT_NASC") & chr(9) & RRNonAmm("PUNTEGGIO")
             
                id = id + 1  
                RRNonAmm.MoveNext 
             loop    
	           
	        RRNonAmm.Close
	        set RRNonAmm=nothing
	        
	      '  b.WriteLine rptIscritti
			b.Close 
		    flagEsitoReport = flagEsitoReport  & "OKNonAmm"
	  end if 
	
	  select case flagEsitoReport
		case "OKAmmOKNonAmm"
		     Messaggio("I report per gli Ammessi e Non Ammessi sono stati creati correttamente")  
	         OpzioniDaSelezionare() 
	    case "OKAmmKONonAmm"
	         Messaggio("Il report per gli Ammessi � stato creato correttamente")
	         Messaggio("Tutti i candidati sono stati Ammessi, nessuno escluso")
	         OpzioniDaSelezionare() 
	    case "KOAmmOKNonAmm" 
	         Messaggio("Nessun Candidato e' stato ammesso")  
	         Messaggio("Il report per i Non Ammessi � stato creato correttamente")
	         OpzioniDaSelezionare()   
	    case "KOAmmKONonAmm" 
	         Messaggio("Non risultano candidati per i criteri inseriti")
	  end select
End Sub
%>
<!--	FINE BLOCCO ASP		-->
<!--		MAIN			-->
<%	Dim nIdBando, nSede, flagEsitoReport, sql, sql1, FileAmmessi, FileNonAmmessi,DescBando,sSede,DescSede
	dim sPathFile
	Inizio()
	AcquisizioneDati()
	ImpostaPag()
	Fine()
%>
<!--	FINE BLOCCO MAIN	-->
