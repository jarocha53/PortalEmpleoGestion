<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%'Option Explicit 
   Response.ExpiresAbsolute = Now() - 1 
   Response.AddHeader "pragma","no-cache"
   Response.AddHeader "cache-control","private"
   Response.CacheControl = "no-cache"
%>

<!-- #include virtual="/strutt_testa2.asp"-->
<!--#include virtual = "/include/openconn.asp"-->
<!--#include virtual="/util/portallib.asp"-->
<%
'if ValidateService(session("idutente"),"GRD_SELVALIDGRAD", CC) <> "true" then 
'	response.redirect "/util/error_login.asp"
'end if
%>
<!-- ************** Javascript inizio ************ -->

<script LANGUAGE="Javascript">
<!--#include virtual = "/Include/ControlString.inc"-->

function VaiInizio(){
	location.href = "GRD_SelValidGrad.asp";
}	

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

function InviaProv(){
	appoProv=frmProvincia.cmbProv.value
	if (appoProv == ""){
		alert("Selezionare la Provincia")
	}else
		frmProvincia.submit();
	}

function InviaBando(){
	appoBando=frmBando.cmbBando.value
	if (appoBando == ""){
		alert("Selezionare il Bando")
	}else
		frmBando.submit();
	}
	
function InviaCImp(){
	appoCImp=frmCImp.cmbCImp.value
	if (appoCImp == ""){
		alert("Selezionare il Centro dell'Impiego")
	}else
		frmCImp.submit();
	}

//Funzione per il controllo dei campi COGNOME e NOME
function ControllaCampi(frmCognomeNome){
	//Cognome
	if (((frmCognomeNome.txtCognome.value == "")||
		(frmCognomeNome.txtCognome.value == " "))&
		(frmCognomeNome.txtNome.value != "")){
			alert("Digitare anche il cognome")
			frmCognomeNome.txtCognome.focus() 
		return false
	}
	
//	frmCognomeNome.txtCognome.value = TRIM(frmCognomeNome.txtCognome.value)

//	if (TRIM(frmCognomeNome.txtNome.value) != "0")
//		frmCognomeNome.txtNome.value=TRIM(frmCognomeNome.txtNome.value)

	return true
}
	
</script>

<!-- ******************  Javascript Fine *********** -->
<!-- ************** ASP inizio *************** -->

<!-- #include virtual="/util/dbutil.asp" -->

<!--#include virtual = "/include/DecCod.asp"-->

<%
'----------------------------------------------------------------------------------------------------------------------------------------------------  

sub Inizio()
%>
	<table border="0" width="520" cellspacing="0" cellpadding="0" height="81">
	   <tr>
	     <td width="500" background="<%=Session("Progetto")%>/images/titoli/strumenti2b.gif" height="81" valign="bottom" align="right">
	
	       <table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
	         <tr>
	             <td width="100%" valign="top" align="right">
	                 <b class="tbltext1a">Gestione Progetti</span></b>
	             </td>
	         </tr>
	       </table>
	     </td>
	   </tr>
	</table>
	<br>
	<table cellpadding="0" cellspacing="0" width="500" border="0">
		<tr height="18">
			<td class="sfondomenu" height="18" width="67%">
			<span class="tbltext0"><b>Validazione ammissibili</b></span></td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
			<td valign="middle" align="right" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1"></td>
		</tr>
		<tr width="371" class="SFONDOCOMM">
			<td colspan="3">Testo di prova<a href="Javascript:Show_Help('/Pgm/help/GestProgetti/Alfabeta/Graduatoria/GRD_SelValidGrad')" onmouseover="javascript:window.status=' '; return true"><img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
			</td>
		</tr>
		<tr height="2">
			<td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>
	<br>
<%
end sub

'-----------------------------------------------------------------------------------------------------------------------------------------------------------

Sub ImpostazioneMASK()

dim sMask
		
	sMask = GetSectionVar("GRD_SELVALIDGRAD","MASK") 

'!!!!!!!!!!!!da togliere poi!!!!!!!!!!!!!!!!!!!!!!!!!
	sMask = "04RMNA"
'!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

	Session("Diritti")= mid(sMask,1,2)
	Session("Filtro")= mid(sMask,3)	

	'calcolo il numero delle provincie
	nProv = len(mid(sMask,3))/2

	i = 0	
	sSqlProv = ""
	if nProv <> 0 then
		for i = 1 to nProv
			pos_ini = 2 * (i - 1)
			'ricavo le provincie e ci costruisco una stringa
			sSqlProv = sSqlProv & "'" & mid(Session("Filtro"), 1 + pos_ini, 2) & "'," 
		next
		sSqlProv = mid(sSqlProv, 1, (len(sSqlProv) - 1))
	end if

end sub

'-----------------------------------------------------------------------------------------------------------------------------------------------------------

sub ImpProvince()
%>
<form method="POST" action="GRD_SelValidGrad.asp?" name="frmProvincia">
    <input type="hidden" name="MOD" value="1">
	    
	<table width="500" border="0" cellspacing="1" cellpadding="0">
		<tr>
			<td align="left" width="30%">
				<span class="tbltext1"><b>Provincia</b></span>
			</td>
			<td align="left" colspan="2">
	<% 				
	if trim(cstr(sProv)) <> "" then
		sCmbProv = "PROV|0|" & date & "|" & sProv & "|cmbProv' onchange='InviaProv()|AND CODICE IN(" & sSqlProv & ") ORDER BY DESCRIZIONE"
		'Response.Write "sCmbProv 1 : " & sCmbProv & "<br>"
		CreateCombo(sCmbProv)
	else
		sCmbProv = "PROV|0|" & date & "||cmbProv' onchange='InviaProv()|AND CODICE IN(" & sSqlProv & ") ORDER BY DESCRIZIONE"
		'Response.Write "sCmbProv 2 : " & sCmbProv & "<br>"
		CreateCombo(sCmbProv)
	end if
	%>
           </td>
		</tr>
   </table>	   
   <br>
</form>
<%		

end sub

'-----------------------------------------------------------------------------------------------------------------------------------------------------------

sub ImpBandi()
%>
<form method="POST" action="GRD_SelValidGrad.asp?" name="frmBando">
	<input type="hidden" name="MOD" value="2">
	<input type="hidden" name="cmbProv" value="<%=sProv%>">
	<br>
	<table width="500" border="0" cellspacing="1" cellpadding="0">
		<tr>
			<td align="left" width="30%" class="tbltext1">
				<b>Provincia </b>
			</td>
			<td align="left" class="tbltext1">
				<font color="red"><b><%=sProv%></b></font>
			</td>
		</tr>
	</table>
	<%
	sSQL = "SELECT ID_BANDO from BANDO " & _
			"WHERE ID_BANDO like '" & sProv & "%' " & _
			"AND " & convdatetodbs(now()) & " " & _
			"BETWEEN DT_FIN_ACQ_DOM AND DT_PUB_RIS_SEL " & _
			"ORDER BY ID_BANDO"

	set rsBando = Server.CreateObject("ADODB.Recordset")
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	rsBando.Open sSQL, CC, 3

	nCount = CInt(rsBando.RecordCount)
						
	select case nCount
		case 0
			'nel caso in cui non ci siano BANDI per la provincia
		%>
		<table width="500" border="0" cellspacing="1" cellpadding="0">
			<tr>
				<td class="tbltext3" colspan="2">
					<b>Non ci sono Bandi Attivi per la Provincia richiesta</b>
				</td>
			</tr>	
		</table>
		<%			
		case 1
			%>	
				<input type="hidden" name="cmbBando" value="<%=rsBando("ID_BANDO")%>">
				<script Language="JAVASCRIPT">
					InviaBando()
				</script>
			<%		
			rsBando.Close
			set rsBando = nothing
		case else
			'Ho pi� bandi quindi creo la combo
		%>
		<table width="500" border="0" cellspacing="1" cellpadding="0">
			<tr>						
				<td align="left" width="30%" class="tbltext1">
					<b>Bando</b>
				</td>
				<td align="left">
					<select ID="cmbBando" name="cmbBando" class="textblacka" onchange="InviaBando()">
					<option value></option>
					<%  
					do while not rsBando.EOF
					%>
						<option value="<%=rsBando("ID_BANDO")%>"><%=rsBando("ID_BANDO")%></option>
					<%
						rsBando.MoveNext
					loop
					%>
					</select>
				</td>
			</tr>
		</table>
	<%					
	end select
	
	rsBando.Close
	set rsBando = nothing
	%>
	
	<br><br>
	<table cellpadding="0" cellspacing="0" width="300" border="0">	
		<tr align="center">
			<td nowrap><a href="javascript:VaiInizio()" onmouseover="javascript:window.status=' '; return true"><img src="<%=session("Progetto")%>/images/indietro.gif" border="0" name="imgPunto2"></a><td>
	    </tr>
	</table>

</form>
<%

end sub

'-----------------------------------------------------------------------------------------------------------------------------------------------------------

sub ImpCImpiego()

%>
<form method="POST" action="GRD_SelValidGrad.asp?" name="frmCImp">
	<input type="hidden" name="MOD" value="3">
	<input type="hidden" name="cmbProv" value="<%=sProv%>">
	<input type="hidden" NAME="cmbBando" ID="cmbBando" VALUE="<%=sBando%>">
	
	<table width="500" border="0" cellspacing="1" cellpadding="0">
		<tr class="tbltext1">
			<td align="left" width="30%">
				<b>Provincia </b>
			</td>
			<td align="left">
				<font color="red"><b><%=sProv%></b></font>
			</td>
		</tr>
		<tr class="tbltext1">
			<td align="left" width="30%">
				<b>Bando </b>
			</td>
			<td>
				<font color="red"><b><%=sBando%></b></font>
			</td>
		</tr>
	</table>
		<%
	sSQL = "SELECT ID_CIMPIEGO,DESC_CIMPIEGO,PRV " & _
			"FROM CENTRO_IMPIEGO WHERE PRV ='" & sProv & "'" 
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	set rsCImpiego = CC.Execute(sSQL)						
		
	if rsCImpiego.eof then	%>
		<br>
		<table width="500" border="0" cellspacing="1" cellpadding="0">
			<tr>
				<td class="tbltext3" align="center" colspan="2">
					<b>Non ci sono Centri dell'Impiego per il Bando richiesto</b>
				</td>
			</tr>	
		</table>		
	<%else%>		
		<table width="500" border="0" cellspacing="1" cellpadding="0">
			<tr>
				<td align="left" width="30%" class="tbltext1">
					<b>Centro per l'Impiego</b>
				</td>
				
				<td align="left">
					<select ID="cmbCImp" name="cmbCImp" class="textblacka" onchange="InviaCImp()">
					<option value></option>
						<%
					do while not rsCImpiego.EOF
						%>
						<option value="<%=rsCImpiego("ID_CIMPIEGO") & "|" & rsCImpiego("DESC_CIMPIEGO")%>"><%=rsCImpiego("DESC_CIMPIEGO")%></option>
						<%
						rsCImpiego.MoveNext
					loop
						%>
					</select>
				</td>
			</tr>
		</table>
	<%
		rsCImpiego.Close
		set rsCImpiego = nothing
	end if %>
	
	<br><br>
	<table cellpadding="0" cellspacing="0" width="300" border="0">	
		<tr align="center">
			<td nowrap><a href="javascript:VaiInizio()" onmouseover="javascript:window.status=' '; return true"><img src="<%=session("Progetto")%>/images/indietro.gif" border="0" name="imgPunto2"></a><td>
	    </tr>
	</table>
	
</form>

<%
end sub

'-----------------------------------------------------------------------------------------------------------------------------------------------------------

sub ImpNome()

%>

<form method="POST" onsubmit="return ControllaCampi(this)" action="GRD_VisValidGrad.asp" name="frmCognomeNome">
	<input type="hidden" name="cmbProv" value="<%=sProv%>">
	<input type="hidden" NAME="cmbBando" ID="cmbBando" VALUE="<%=sBando%>">
	<input type="hidden" NAME="cmbCImp" ID="cmbCImp" VALUE="<%=sApCImp%>">

	<br>
	<table width="500" border="0" cellspacing="1" cellpadding="0">
		<tr class="tbltext1">
			<td align="left" width="30%">
				<b>Provincia </b>
			</td>
			<td align="left">
				<font color="red"><b><%=sProv%></b></font>
			</td>
		</tr>
		<tr class="tbltext1">
			<td align="left" width="30%">
				<b>Bando </b>
			</td>
			<td>
				<font color="red"><b><%=sBando%></b></font>
			</td>
		</tr>
		<tr class="tbltext1">
			<td align="left" width="30%">
				<b>Centro dell'Impiego </b>
			</td>
			<td>
				<font color="red"><b><%=sDescCImpiego%></b></font>
			</td>
		</tr>
		<tr>
			<td align="left" width="30%" class="tbltext1">
				<b>Cognome da ricercare</b>
			</td>
			<td>
				<input style="TEXT-TRANSFORM: uppercase;" size="30" maxlength="30" name="txtCognome" class="textblacka">
			</td>
		</tr>
		<tr>
			<td align="left" width="30%" class="tbltext1">
				<b>Nome da ricercare</b>
			</td>
			<td>
				<input style="TEXT-TRANSFORM: uppercase;" size="30" maxlength="30" name="txtNome" class="textblacka">
			</td>
		</tr>
	</table>
	<br><br>
	<table cellpadding="0" cellspacing="0" width="300" border="0">	
		<tr align="center">
			<td nowrap><input type="image" name="Conferma" src="<%=Session("progetto")%>/images/conferma.gif"></td value="Registra">
			<td nowrap><a href="javascript:VaiInizio()" onmouseover="javascript:window.status=' '; return true"><img src="<%=session("Progetto")%>/images/indietro.gif" border="0" name="imgPunto2"></a><td>
	    </tr>
	</table>

</form><%

end sub

'-----------------------------------------------------------------------------------------------------------------------------------------------------------

sub Fine()
%>
<!--#include virtual = "/include/closeconn.asp"-->
<!--#include virtual = "strutt_coda2.asp"-->
<%
end sub

%>
<!-- ************** ASP Fine *************** -->

<!-- ************** MAIN Inizio ************ -->
<%

dim sMask
dim sSqlProv
dim sSQL

dim sModo
dim sProv
dim sBando
dim sApCImp
dim sCImpiego
dim sDescCImpiego


Inizio()
ImpostazioneMASK()

sModo = Request("MOD")

sProv = Request("cmbProv")
sBando = Request("cmbBando")

sApCImp = trim(cstr(Request("cmbCImp")))
if sApCImp <> "" then
	sCImpiego = Mid(sApCImp, 1, instr(1, sApCImp, "|"))
	sDescCImpiego = Mid(sApCImp, instr(1, sApCImp, "|") + 1)
end if

select case sModo
	case 1
		'E' la seconda volta che entro, combo BANDI
		ImpBandi()
	case 2
		'E' la terza volta che entro, combo CENTRI DELL'IMPIEGO
		ImpCImpiego()
	case 3
		'E' la quarta volta che entro, campi txt COGNOME e NOME
		ImpNome()
	case else
		'E' la prima volta che entro, combo PROVINCIE
		ImpProvince()		
	end select
Fine()
	
%>
<!-- ************** MAIN Fine ************* -->



