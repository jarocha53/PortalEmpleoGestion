<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include virtual = "/strutt_testa2.asp"-->
<!--#include virtual ="/include/openconn.asp"-->
<!--#include virtual = "util/portallib.asp"-->
<!--#include virtual = "util/dbutil.asp"-->
<!--#include virtual = "include/DecCod.asp"-->
<!--#include virtual = "include/SelAreaTerrBandi.asp"-->
<!--#include virtual = "/include/ControlDateVB.asp"-->
<%
if ValidateService(session("idutente"),"REP_RICCANDGRAD", CC) <> "true" then 
	response.redirect "/util/error_login.asp"
end if
%>
<script LANGUAGE="Javascript">
<!--#include Virtual = "/Include/help.inc"-->

function VaiInizio(){
	location.href = "REP_RicCandGrad.asp";
}	

function validator(theForm) {
	appoCImp=frmVisAreaGeoCombo.cmbSede.value
	if (appoCImp == ""){
		alert("Selezionare la sede")
		return false
	}
	return true
}

function PagSucc(){
	var Valore
	
	Valore=document.frmVisAreaGeoCombo.cmbSede.value
	
	if (Valore==""){
		alert('Devi selezionare almeno una sede.')
	}else{
		document.frmVisAreaGeoCombo.action="REP_VisCandGrad.asp";
		document.frmVisAreaGeoCombo.submit();		
	}
}

function Invia(){
	if (document.frmVisAreaGeoCombo.cmbBando.value==""){
		alert('Devi selezionare un Bando.')
	}else{		
		document.frmVisAreaGeoCombo.action = "REP_RicCandGrad.asp";
		document.frmVisAreaGeoCombo.submit();				
	}
}
</script>
<%sub inizio()%>
<table border="0" width="520" cellspacing="0" cellpadding="0" height="81">
	   <tr>
	     <td width="500" background="<%=Session("Progetto")%>/images/titoli/strumenti2b.gif" height="81" valign="bottom" align="right">
	       <table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
	         <tr>
	           <td width="100%" valign="top" align="right"><b class="tbltext1a">Gestione Progetti</span></b></td>
	         </tr>
	       </table>
	     </td>
	   </tr>
	</table>

	<br>
	<table cellpadding="0" cellspacing="0" width="500" border="0">
		<tr height="18">
			<td class="sfondomenu" height="18" width="67%">
			<span class="tbltext0"><b>&nbsp;REPORT GRADUATORIA</b></span></td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
			<td valign="middle" align="right" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1"></td>
		</tr>
		<tr width="371" class="SFONDOCOMM">
			<td colspan="3">
				<br>Per visualizzare i report della graduatoria inserire i campi richiesti.				
				<a href="Javascript:Show_Help('/Pgm/help/Gestprogetti/alfabeta/graduatoria/GRD_StmGradDef')">
				<img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
			</td>
		</tr>
		<tr height="2">
			<td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>
	<br><br>
<%end sub%>


<%sub ImpBandi()%>
<form name="frmVisAreaGeoCombo" action method="post">
<table cellpadding="1" cellspacing="0" width="400" border="0">	
	<input type="hidden" name="Valore" value="1">	
<%	
			
	if nIdBando <> "" then
%>
	<tr>
		<td class="tbltext1" align="left" width="100">
			<b>Bando</b>
		</td>
		<td class="tbltext" align="left" width="300">
			<b><%=sDescBando%></b>
		</td>
	</tr>
<%
	else 
%>
	<tr>
		<td class="tbltext1" align="left" width="100">
			<b>Bando</b>
		</td>
		<td align="left" width="300">
		<span class="tbltext3">
<% strcombo= session("iduorg") & "||cmbBando|onchange='Javascript:Invia()'" 
   CreateBandi strcombo	

%>      </span> 
		</td>
    </tr> 
<%	end if%>	
     </table>
<%end sub

sub CodTimpr()
%>
	<input type="hidden" NAME="cmbBando" VALUE="<%=sBando%>">	

	 <script>
		frmVisAreaGeoCombo.action ='REP_VisCandGrad.asp';
		frmVisAreaGeoCombo.submit() 	
	</script>	
<%end sub		
	
sub ImpProvincia()
%>	
	
<table cellpadding="1" cellspacing="0" width="400" border="0">	
	<input type="hidden" name="cmbBando" value="<%=sBando%>">
<%
	if sProv <> "" then
%>
	<tr align="left">	
		<td class="tbltext1" align="left" width="100">
			<b>Provincia </b>
		</td>
		<td class="tbltext" align="left" width="300">
				
				<b><option value="<%=sProv%>"><%=DecCodVal("PROV", 0, "", sProv,"")%></option></b>
		</td>
	</tr>
		
<%
	else
	sMask = SetProvBando(nIdBando,session("idUorg"))
%>	
		<tr>	
			<td align="left" class="tbltext1" width="100">
			<b>Provincia</b>
			</td>
			<td align="left" width="300">			 	 
				<select NAME="cmbProvBan" class="textblack" onchange="Invia()">			 
				<option>&nbsp;</option>
<%				nProvSedi = len(sMask)
			      if nProvSedi <> 0 then
	                pos_ini = 1 	                
	                    for i=1 to nProvSedi/2
	                   		sTarga = mid(sMask,pos_ini,2)										' DecCodVal(nome_tabella, isa, dt_decor, codice, flag_conn)
%>
		                   <option value="<%=sTarga%>"><%=DecCodVal("PROV", 0, "", sTarga,"")%></option>
<%	                    
	                    pos_ini = pos_ini + 2
	                    next
                    end if
%>
			         </select>
				</td>
		</tr>
		
	<%
		end if	%>	
		</table>
	<%end sub
	
'------------------------------------------------------------------------------------------------------------------------------
sub ImpSede()
		'sStatus = 1
		'sIntestazione = ""	
%>		
	<table cellpadding="0" cellspacing="0" width="400" border="0">
	<input type="hidden" name="MOD" value="3">
	<input type="hidden" name="cmbProvBan" value="<%=sProv%>">
	
	<input type="hidden" name="Status" value="<%=sStatus%>">
	<input type="hidden" name="Intestazione" value="<%=sIntestazione%>">	
		<%
		sSQL =  "select si.id_sede,si.descrizione " & _ 
				" from sede_impresa si,impresa i, bando_posti p " & _
                " where si.id_impresa = i.id_impresa " & _ 
                " and si.id_sede = p.id_sede " & _ 
                " and p.id_bando =" & nIdBando & " and prv= '" & sProv & "'"
		'Response.Write "<BR> ass = " & ssql
		'Response.End 
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
			set rsSede = CC.Execute(sSQL)
								
		if rsSede.eof then	%>
			<br>
			<tr>
				<td class="tbltext3" align="center" colspan="2">
				<b>Non ci sono sedi per la provincia selezionata</b>
				</td>
			</tr>	
				<br>
			<tr align="center">
				<td nowrap colspan="2"><a href="javascript:VaiInizio()" onmouseover="javascript:window.status=' '; return true"><img src="<%=session("Progetto")%>/images/indietro.gif" border="0" name="imgPunto2"></a><td>
			 </tr>
<%			
		
		end if%>		
			<tr>
				<td align="left" width="100" class="tbltext1">
					<b>Sede</b>
				</td>
				<td align="left" width="300">
				 <select ID="cmbSede" name="cmbSede" class="textblacka">
					<option value></option>
<%
					do while not rsSede.EOF
%>
						<option value="<%=rsSede("ID_SEDE") & "|" & rsSede("DESCRIZIONE")%>"><%=rsSede("DESCRIZIONE")%></option>
<%						
						rsSede.MoveNext
					loop
%>
					</select>
				</td>				
			</tr>
		</table>
		<br><br>
		<table cellpadding="0" cellspacing="0" width="300" border="0">	
			<tr align="left">	
				<td nowrap width="150"><a href="javascript:PagSucc()"><img border="0" src="<%=Session("progetto")%>/images/conferma.gif"></td>	
				<td nowrap><a href="javascript:VaiInizio()" onmouseover="javascript:window.status=' '; return true"><img src="<%=session("Progetto")%>/images/indietro.gif" border="0" name="imgPunto2"></a><td>
		    </tr>
		</table>		
<%end sub%>	

<% sub Fine()%>
<!--#include virtual = "/strutt_coda2.asp"-->
<!--#include virtual ="/include/closeconn.asp"-->
<% end sub%>
				<!---------------------MAIN------------------------------>
	
<%	dim nIdBando
	dim sDescBando
	dim sBando
	dim sProv
	dim sIdSede
	
	
	sBando = Request("cmbBando")
	sProv    = Request("cmbProvBan")
	sStatus  = Request("sStatus")
	sIntestazione = Request("Intestazione")
	sIdSede  = Request("cmbSede")
				
	if sBando <> "" then		
		nIdBando=sBando
		sDescBando=DecBando(sBando)
	end if	
	inizio()
	ImpBandi()
	'Response.Write "nIdBando" & nIdBando
	
if nIdBando <> "" then	
	'sSql="SELECT COD_TIMPR FROM PROGETTO WHERE AREA_WEB='" & mid(session("progetto"),2) & "'"
	sSql="SELECT COUNT(*) AS AGGREGAZIONE FROM BANDO_POSTI WHERE ID_BANDO = " & nIdBando & " AND ID_SEDE <> 0"
	'Response.Write ssql
		set rsCodTimpr=Server.CreateObject("ADODB.recordset")
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
			rsCodTimpr.Open sSql,CC,3
			'if 	rsCodTimpr.eof then
			'	scod = ""
			'	else	
			sCod = rsCodTimpr("AGGREGAZIONE")
			'end if
			rsCodTimpr.close
	 
		if sCod <> "0" then		
			ImpProvincia()
				if sProv<>"" then
					ImpSede()					
				end if
		else
			CodTimpr()
		end if			
end if
	Fine()
%>			

	
