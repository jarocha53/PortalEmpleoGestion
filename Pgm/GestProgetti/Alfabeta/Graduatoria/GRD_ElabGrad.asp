<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%'Option Explicit 
   'Response.ExpiresAbsolute = Now() - 1 
   'Response.AddHeader "pragma","no-cache"
   'Response.AddHeader "cache-control","private"
   'Response.CacheControl = "no-cache"
%>

<!-- #include virtual="/strutt_testa2.asp"-->
<!--#include virtual = "/include/openconn.asp"-->
<!--#include virtual="/util/portallib.asp"-->
<%
if ValidateService(session("idutente"),"GRD_ELABGRAD", CC) <> "true" then 
	response.redirect "/util/error_login.asp"
end if
%>
<!-- ************** Javascript inizio ************ -->

<script language="JAVASCRIPT">

<!--#include virtual = "/Include/Help.inc"-->

/*function ControllaDati(myForm) {
	if (frmElabGrad.cmbTGrad.value == "") {
		alert ("Valore obbligatorio")
		frmElabGrad.cmbTGrad.focus()
		return false
	}
	return false
}*/

//-----------------------------------------------------------------------------------------------------------------------------------------------------------------

function Reload()
{
	var nSel

	nSel = frmElabGrad.elements[0].selectedIndex
	if (frmElabGrad.elements[0].options[nSel].value != "") 
		location.href = "GRD_ElabGrad.asp?Sel=" + frmElabGrad.elements[0].options[nSel].value
	else
		alert ("Occorre selezionare un bando")
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------------------

function GestioneArea()
{
		location.href = "GRD_ElabGrad.asp?Sel=" + frmElabGrad.elements[0].value;
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------------------

function Elabora(){
	frmElabGrad.submit();
}
	
</script>

<!-- ******************  Javascript Fine *********** -->
<!-- ************** ASP inizio *************** -->

<!--#include virtual="/util/dbutil.asp"-->
<%
'----------------------------------------------------------------------------------------------------------------------------------------------------  

sub Inizio()
%>
	<table border="0" width="520" cellspacing="0" cellpadding="0" height="81">
	   <tr>
	     <td width="500" background="<%=Session("Progetto")%>/images/titoli/strumenti2b.gif" height="81" valign="bottom" align="right">
	
	       <table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
	         <tr>
	             <td width="100%" valign="top" align="right">
	                 <b class="tbltext1a">Gestione Progetti</span></b>
	             </td>
	         </tr>
	       </table>
	     </td>
	   </tr>
	</table>
	<br>
	<table cellpadding="0" cellspacing="0" width="500" border="0">
		<tr height="18">
			<td class="sfondomenu" height="18" width="67%">
			<span class="tbltext0"><b>Elaborazione Graduatoria</b></span></td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
			<td valign="middle" align="right" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1"></td>
		</tr>
		<tr width="371" class="SFONDOCOMM">
			<td colspan="3">Indicare le informazioni utili per l'elaborazione della graduaoria. Selezionato il Bando sar� possibile indicare la struttura per la quale si intende procedere nell'elaborazione (se richiesta nel progetto) ed il tipo di elaborazione da effettuare.<a href="Javascript:Show_Help('/Pgm/help/GestProgetti/Alfabeta/Graduatoria/GRD_ElabGrad')" onmouseover="javascript:window.status=' '; return true"><img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
			</td>
		</tr>
		<tr height="2">
			<td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>
	<br>

<form name="frmElabGrad" action="GRD_CfnGrad.asp" method="post">
	
<%
end sub

'-----------------------------------------------------------------------------------------------------------------------------------------------------------

sub ImpComboBandi()

	set rsValBando = Server.CreateObject("ADODB.RecordSet")

	sSQL = "SELECT ID_BANDO, DESC_BANDO FROM BANDO a, PROGETTO b " & _
			"WHERE " & convdatetodb(Now()) & " " & _
			"BETWEEN DT_FIN_ACQ_DOM AND DT_PUB_RIS_SEL" &_
			" and a.id_proj=b.id_proj" &_
			" AND AREA_WEB='" & mid(Session("Progetto"),2) & "'"
	
	'Response.Write "sSQL: " & sSQL & "<br>"
	
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	set rsBando = CC.Execute(sSQL)
	
	if rsBando.EOF then
		%>
		<table width="500" align="center" border="0" cellspacing="1" cellpadding="0">
			<tr>
				<td align="center" class="tbltext3">
					<b>Impossibile effettuare la graduatoria:</b>
				</td>
			</tr>
			<tr>
				<td align="center" class="tbltext3">
					<b>non ci sono bandi validi</b>
				</td>
			</tr>
		</table>
		<%		
		exit sub
	end if
	%>
	<table width="500" border="0" cellspacing="1" cellpadding="0">
		<tr>						
			<td align="left" class="tbltext1">
				<b>Bando</b>
			</td>
			<td align="left">
				<select name="cmbBando" class="textblack" onchange="Reload()">
				<option></option>
				<%  
				do while not rsBando.EOF
				%>
					<option value="<%=rsBando("ID_BANDO") & "|" & rsBando("DESC_BANDO")%>"><%=rsBando("DESC_BANDO")%></option>
				<%
					rsBando.MoveNext
				loop
				%>
				</select>
			</td>
		</tr>
	</table>
	<%

end sub

'-----------------------------------------------------------------------------------------------------------------------------------------------------------

sub ImpComboBandoPosti()

	%>
	<table width="500" border="0" cellspacing="1" cellpadding="0">
		<tr>
			<td align="left" width="30%" class="tbltext1">
				<b>Bando </b>
			</td>
			<td align="left" class="tbltext">
				<b><%=sDescBando%></b>
			</td>
		</tr>
	</table>	

	<input type="hidden" name="txtBando" Value="<%=sBando%>">
    <%
    
    sSQL = "SELECT BP.ID_SEDE FROM BANDO_POSTI BP" & _
			" WHERE ID_BANDO = " & nIdBando &_
			" AND STATUS_GRAD=0"

'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	set rsApp = CC.Execute(sSQL)
	
	if not rsApp.eof then
		'Response.Write "ID_SEDE: " & CLng(rsApp("ID_SEDE"))
		if CLng(rsApp("ID_SEDE")) <> 0 then
				
			sSQL = "SELECT BP.ID_SEDE, BP.NUM_POSTI, SI.DESCRIZIONE, SI.PRV " & _
					"FROM BANDO_POSTI BP, SEDE_IMPRESA SI " & _
					"WHERE ID_BANDO = " & nIdBando  & _
					"AND BP.ID_SEDE = SI.ID_SEDE " & _
					"AND BP.STATUS_GRAD = 0" 

			'Response.Write sSQL & "<br>"

			set rsGrad = Server.CreateObject("ADODB.RecordSet")
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
			rsGrad.Open sSQL, CC

			if not rsGrad.EOF then
				'graduatoria da elaborare
				ElabStatusZero()
			else
				ElabStatusDue()
			end if
		else
			%>
			<table width="500" border="0" cellspacing="1" cellpadding="0">
				<tr>
					<td align="left" width="30%" class="tbltext1">
						<b>Tipo di graduatoria</b>
					</td>
					<td align="left">
						<select name="cmbTGrad" class="textblack">
							<option value="0" selected>Graduatoria Provvisoria
							<option value="1">Graduatoria Definitiva
						</select> 
					</td>
				</tr>		
				<tr><td colspan="2">&nbsp;</td>
				<tr>
					<td align="center" colspan="2">
						<a href="javascript:Elabora()" class="textred">
							<b>Elabora la graduatoria</b>
						</a>
					</td>
				</tr>
			</table>
			<input type="hidden" value="0" name="txtTipoElab">
			<br>
				<table border="0" cellpadding="0" cellspacing="1" width="500">
			<tr>
				<td align="center">
					<a href="javascript:history.go(-1)">
						<img src="<%=session("Progetto")%>/images/indietro.gif" border="0" name="imgPunto2" onmouseover="javascript:window.status=' '; return true">
					</a>
				</td>
			</tr>
		</table>
			<%	
		end if
	else
	%>
		<br>
		<br>
		<table width="500" border="0" cellspacing="1" cellpadding="0">
			<tr>
				<td align="center" class="tbltext3">L'iter di elaborazione della graduatoria � completo</td>
			</tr>
			<tr>
				<td>
					<br>&nbsp;
				</td>
			</tr>
			<tr>
				<td align="center">
					<a href="javascript:history.go(-1)">
						<img src="<%=session("Progetto")%>/images/indietro.gif" border="0" name="imgPunto2" onmouseover="javascript:window.status=' '; return true">
					</a>
				</td>
			</tr>
		</table>			
	<%
	end if

end sub

'-----------------------------------------------------------------------------------------------------------------------------------------------------------

sub ElabStatusZero()

    %>
	<table width="500" border="0" cellspacing="1" cellpadding="0">
		<tr>
			<td align="left" width="30%" class="tbltext1">
				<b>Relativo a: </b>
			</td>
			<td align="left">
				<select name="cmbSede" class="textblack">
				<option value></option>
					<%
				do while not rsGrad.EOF
					%>
					<option value="<%=rsGrad("ID_SEDE")%>"><%=rsGrad("DESCRIZIONE") & "(" & rsGrad("PRV") & ")"%></option>
					<%
					rsGrad.MoveNext
				loop
				
				rsGrad.Close
				set rsGrad = nothing
				%>
				</select>
			</td>
		</tr>              
		<tr>
			<td align="left" width="30%" class="tbltext1">
				<b>Tipo di graduatoria</b>
			</td>
			<td align="left">
				<select name="cmbTGrad" class="textblack">
					<option value="0" selected>Graduatoria Provvisoria
					<option value="1">Graduatoria Definitiva
				</select> 
			</td>
		</tr>
		
		<tr><td colspan="2">&nbsp;</td>
		<tr>
			<td align="center" colspan="2">
				<a href="javascript:Elabora()" class="textred">
					<b>Elabora la graduatoria</b>
				</a>
			</td>
		</tr>
		<tr>
			<td align="center" colspan="2">
				<br>
			</td>
		</tr>
		<tr>
			<td align="center" colspan="2">
				<a href="javascript:history.go(-1)">
					<img src="<%=session("Progetto")%>/images/indietro.gif" border="0" name="imgPunto2" onmouseover="javascript:window.status=' '; return true">
				</a>
			</td>
		</tr>
	</table>

	<input type="hidden" value="0" name="txtTipoElab">
	
	<br>
	<%

end sub
'-----------------------------------------------------------------------------------------------------------------------------------------------------------

sub ElabStatusDue()

	sSQL = "SELECT BP.ID_SEDE, BP.NUM_POSTI, SI.DESCRIZIONE, SI.PRV " & _
			"FROM BANDO_POSTI BP, SEDE_IMPRESA SI " & _
			"WHERE ID_BANDO = " & nIdBando & _
			"AND BP.ID_SEDE = SI.ID_SEDE " & _
			"AND STATUS_GRAD = 2" 

	set rsGrad = Server.CreateObject("ADODB.RecordSet")
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	rsGrad.Open sSQL, CC
			
	if rsGrad.eof then
	%>
		<br>
		<table width="500" border="0" cellspacing="1" cellpadding="0">
			<tr>
				<td align="center" class="tbltext3">L'iter di elaborazione della graduatoria � completo</td>
			</tr>
		</table>			
	<%
	else
	%>
	<!--input type="hidden" value="1" name="txtTipoElab">	<table width="500" align="center" border="0" cellspacing="1" cellpadding="0">		<tr>			<td align="center" class="tbltext3">				<b>E' possibile effettuare la quadratura</b>			</td>		</tr>	</table>				<br>	<table width="500" align="center" border="0" cellspacing="1" cellpadding="0">	<tr>		<td align="center" colspan="2">			<a href="javascript:Elabora()" class="textred">				<b>Elabora la graduatoria</b>			</a>		</td>	</tr>	</table-->
	<%             
	end if 
	
	rsGrad.Close
	set rsGrad = nothing
	
end sub

'-----------------------------------------------------------------------------------------------------------------------------------------------------------

sub Fine()
%>
</form>

<!--#include virtual = "/include/closeconn.asp"-->
<!--#include virtual = "strutt_coda2.asp"-->
<%
end sub
'------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

%>
<!-- ************** ASP Fine *************** -->

<!-- ************** MAIN Inizio ************ -->
<%

	dim sSQL
	dim rsGrad
	
'	dim CnConn
    dim sMask
    
	dim sBando
	dim nIdBando
	dim sDescBando

	Inizio()

	sBando	= Request("Sel")

	if sBando <> "" then
		nIdBando = Mid(sBando, 1, instr(1, sBando, "|") - 1)
		sDescBando = Mid(sBando, instr(1, sBando, "|") + 1)
	end if
	
	if sBando = "" then 
		ImpComboBandi()
	else
		ImpComboBandoPosti()
	end if

	Fine()	
%>
<!-- ************** MAIN Fine ************* -->
