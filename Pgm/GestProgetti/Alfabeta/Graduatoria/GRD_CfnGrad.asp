<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%'Option Explicit 
   'Response.ExpiresAbsolute = Now() - 1 
   'Response.AddHeader "pragma","no-cache"
   'Response.AddHeader "cache-control","private"
   'Response.CacheControl = "no-cache"
%>

<!-- #include virtual="/strutt_testa2.asp"-->
<!--#include virtual = "/include/openconn.asp"-->
<!--#include virtual="/util/portallib.asp"-->
<%
'if ValidateService(session("idutente"),"GRD_ElabGrad", CC) <> "true" then 
'	response.redirect "/util/error_login.asp"
'end if
%>
<!-- ************** Javascript inizio ************ -->

<script language="JAVASCRIPT">

<!--#include virtual = "/Include/Help.inc"-->

function VaiInizio(){
	location.href = "GRD_ElabGrad.asp";
}	

</script>

<!-- ******************  Javascript Fine *********** -->

<!-- ************** ASP inizio *************** -->
	<!--#include virtual = "/include/SysFunction.asp"-->
	<!--#include virtual = "/include/ControlDateVB.asp"-->

<%
'----------------------------------------------------------------------------------------------------------------------------------------------------  

sub Inizio()
%>
	<table border="0" width="520" cellspacing="0" cellpadding="0" height="81">
	   <tr>
	     <td width="500" background="<%=Session("Progetto")%>/images/titoli/strumenti2b.gif" height="81" valign="bottom" align="right">
	
	       <table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
	         <tr>
	             <td width="100%" valign="top" align="right">
	                 <b class="tbltext1a">Gestione Progetti</span></b>
	             </td>
	         </tr>
	       </table>
	     </td>
	   </tr>
	</table>
	<br>
	<table cellpadding="0" cellspacing="0" width="500" border="0">
		<tr height="18">
			<td class="sfondomenu" height="18" width="67%">
			<span class="tbltext0"><b>Elaborazione Graduatoria</b></span></td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
			<td valign="middle" align="right" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1"></td>
		</tr>
		<tr width="371" class="SFONDOCOMM">
			<td colspan="3">Si riporta l'esito della elaborazione della graduatoria. Si consiglia di visionare l'output prodotto dall'elaborazione stessa.<a href="Javascript:Show_Help('/Pgm/help/GestProgetti/Alfabeta/Graduatoria/GRD_ElabGrad')" onmouseover="javascript:window.status=' '; return true"><img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>		
			</td>
		</tr>
		<tr height="2">
			<td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>
	<br>

<form name="frmElabGrad" action="GRD_CfnGrad.asp" method="post">
	
<%
end sub

'-------------------------------------------------------------------------------------------------------------------------------

sub TornaIndietro()

	%>
	<form name="frmIndietro" method="post" action="BAN_VisBandi.asp">
		<input type="hidden" name="MOD" value="1">
	</form>
    <!--#include virtual = "/include/closeconn.asp"-->

	<script>
		alert("Operazione correttamente effettuata");
		frmIndietro.submit()
	</script>
	<%

end sub

'-----------------------------------------------------------------------------------------------------------------------------------------------------------

sub Fine()
%>
<br>	
<br>
	<table border="0" cellpadding="0" cellspacing="1" width="500">
		<tr>
			<td align="center">
				<a href="javascript:history.go(-1)">
					<img src="<%=session("Progetto")%>/images/indietro.gif" border="0" name="imgPunto2" onmouseover="javascript:window.status=' '; return true">
				</a>
			</td>
		</tr>
	</table>

<!--#include virtual = "/include/closeconn.asp"-->
<!--#include virtual = "strutt_coda2.asp"-->
<%
end sub

%>
<!-- ************** ASP Fine *************** -->

<!-- ************** MAIN Inizio ************ -->

<%
dim nIdSede
dim sRisposta
dim objGrad, Risposta
dim CnConn
dim sTgrad
Dim objMail 
dim rsEmail
dim sSQL
dim rsStatusGrad
dim sBando
dim nIdBando
dim sDescBando

Server.ScriptTimeout = Server.ScriptTimeout * 5

sBando = Request.Form("txtBando")
if sBando <> "" then
	nIdBando = Mid(sBando, 1, instr(1, sBando, "|") - 1)
	sDescBando = Mid(sBando, instr(1, sBando, "|") + 1)
end if

Inizio()

set objGrad = Server.CreateObject("FUNCGRADUATORIA.clsGraduatoria")

sTipoElab = Trim(Request("txtTipoElab"))
if sTipoElab = "0" then
	'elaborazione graduatoria
	nIdSede = Request("cmbSede")
	if trim(nIdSede) = "" then
		nIdSede = "0"
	end if 
	sTgrad = Request("cmbTgrad")
	sMessaggio="Elaborazione della Graduatoria correttamente effettuata"

'Response.Write "sBando:" & sBando & ":<br>"
'Response.Write "nIdSede:" & nIdSede & ":<br>"
'Response.Write "sTgrad:" & sTgrad & ":<br>"
'Response.Write "Progetto:" & Session("Progetto") & ":<br>"
'Response.write Server.MapPath("\")
	
	sRisposta = objGrad.Genera_Graduatoria(Clng(nIdBando), Clng(nIdSede), Clng(sTgrad), Session("Progetto"), Server.MapPath ("\")& "\",CC)
	
'Response.Write "sRisposta: " & sRisposta & "<br>"
	
'else
'	'elaborazione quadratura
'	sMessaggio="Quadratura della Graduatoria correttamente effettuata"
'	
'	sRisposta=objGrad.Quadratura_Grad(nIdBando,CC)
'	set objgrad = nothing
end if

if sRisposta = "0" then

'rivedere
'	if sTipoElab="0" then 
'		' Mando l'email con l'avvenuta elaborazione della gruaduatoria.
'		sSQL = "SELECT EMAIL FROM BANDO A, UTENTE B " & _
'				"WHERE A.ID_RTP = B.IDPERSONA " &_
'				"AND ID_BANDO = " & nIdBando 
'		set rsEmail = CC.Execute (sSQL)	
'		
'		Set objMail = Server.CreateObject("CDONTS.NewMail") 
'		objMail.From = "in-gestione@italialavoro.it"
'		objMail.To = rsEmail("EMAIL") 
'		objMail.Subject = "Elaborazione graduatoria" 
'		objMail.cc	= "in-webmaster@italialavoro.it"
'		objMail.bcc = "in-gestione@sdigroup.it"
'		objMail.Body = sMessaggio & " per il bando : " & nIdBando & " in data : " & ConvDateTimeToString(Now)
'		objMail.Send
'		rsEmail.Close 
'	end if	

%>
	<br>
	<br>
	<table width="500" border="0" cellpadding="0" cellspacing="0">
		<tr> 
			<td colspan="2" align="middle" class="tbltext3">
				<%=sMessaggio%>
			</td>
		</tr>	
	<%
	if sTgrad = 1 then%>	
		<tr> 
			<td colspan="2" align="middle" class="tbltext3">			
				<%
				Response.Write "Report creato correttamente"
				%>
			</td>	
		</tr>
	<%
	end if
	%>
	</table>
<%
else
%>
	<br>
	<br>
	<table width="500" border="0" cellpadding="0" cellspacing="0">
		<tr> 
			<td colspan="2" align="middle" class="tbltext3">
				Errore durante l'elaborazione della graduatoria 
			</td>
		</tr>
		<tr>
			<td colspan="2" align="middle" class="tbltext3">
				(<%Response.Write sRisposta%>)
			</td>
		</tr>		
	</table>
	<br>
<%
end if

set objgrad = nothing

Fine()

Server.ScriptTimeout = Server.ScriptTimeout / 5

%>
<!-- ************** MAIN Fine ************* -->
