<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<div align="center">
<!--#include virtual = "/include/openconn.asp"-->
<!--#include virtual ="/include/controldatevb.asp"-->
<!--#include virtual = "include/SelAreaTerrBandi.asp"-->

<link REL="STYLESHEET" TYPE="text/css" HREF="<%=session("progetto")%>/fogliostile.css">

<%
sub Inizio()
%>

<form name="frmstampa" method="post" action="GRD_RepGrad.asp">
       <input type="hidden" name="cmbStatus" value="<%=sStatus%>">
       <input type="hidden" name="cmbProvBan" value="<%=sProv%>">
       <input type="hidden" name="cmbBando" value="<%=sBando%>">
       <input type="hidden" name="cmbSede" value="<%=sSede%>">
       <input type="hidden" name="Intestazione" value="<%=sIntestazione%>">
       <input type="hidden" name="cmbOrder" value="<%=sOrdinamento%>">
</form>
<body topmargin="0" onafterprint="javascript:buttom.style.visibility='visible';" onbeforeprint="javascript:buttom.style.visibility='hidden'">

	<br>
<%
end sub

sub ImpostaPag()

dim sCodFisc	
dim sNominativo
dim sSesso
dim sDtNasc
'Response.Write ("Pippo " & "<br>")
	if sStatus="0" then
		sTitolo1 = "Graduatoria al " & ConvDateToString(now())
		if ssede <> "" then
		sTitolo = "Lista non Ammessi al progetto " & DescProj & " - " & sDescBando & " per la sede di " & sDescSede
		else
		sTitolo = "Lista non Ammessi al progetto " & DescProj & " - " & sDescBando & " per il territorio nazionale"
		 end if

		sWhere = " AND (ESITO_GRAD = 'N' OR COD_ESITO IS NOT NULL) "

		sApNomeFile ="_NonAmmessi_"
	else
		sTitolo1 = "Graduatoria al " & ConvDateToString(now())
		if ssede <> "" then
		sTitolo = sTitolo & " Lista Ammessi al progetto " & DescProj & " - " & sDescBando & " per la sede di " & sDescSede
		else
		sTitolo = "Lista  Ammessi al progetto " & DescProj & " - " & sDescBando & " per il territorio nazionale"
		end if

		sWhere = " AND (COD_ESITO IS NULL AND ESITO_GRAD IN ('S','X')) "

		sApNomeFile ="_Ammessi_"
	end if
     
	if sOrdinamento="0" then
		sOrder= "ORDER BY COGNOME, NOME, DT_NASC"
		if sStatus="0" then
			sInfo= "La graduatoria, disposta in ordine alfabetico, riporta i nominativi dei non ammessi e degli esclusi. L�esclusione dalle liste di partecipazione � avvenuta in seguito al riscontro dei requisiti disposti dal bando e attinenti ai criteri di selezione."
		else
			sInfo= "La graduatoria, disposta in ordine alfabetico, riporta i nominativi degli ammessi. L�inclusione nelle liste di partecipazione � avvenuta in seguito al riscontro positivo dei requisiti disposti dal bando e attinenti ai criteri di selezione."
		end if
	else 
		sOrder="ORDER BY PUNTEGGIO DESC, DT_DOM "
		if sStatus="0" then
			sInfo= "La graduatoria, disposta in ordine di punteggio, riporta i nominativi dei non ammessi e degli esclusi. L�esclusione dalle liste di partecipazione � avvenuta in seguito al riscontro dei requisiti disposti dal bando e attinenti ai criteri di selezione."
		else
			sInfo= "La graduatoria, disposta in ordine di punteggio, riporta i nominativi degli ammessi. L�inclusione nelle liste di partecipazione � avvenuta in seguito al riscontro positivo dei requisiti disposti dal bando e attinenti ai criteri di selezione."
		end if
	end if  

	'sempre vuota!!!!
	if sIntestazione <> "" then
	%>
		<!--table border="0" cellpadding="0" cellspacing="0" width="630">			<tr>		        <td width="80%" align="middle">					<b><font face="Verdana" size="2" Color="#008caa"><%=sIntestazione%></font></b>				</td>		    </tr>		</table-->
	<%
	end if

	set rsRR = server.CreateObject("ADODB.Recordset")
	
	sSQL="SELECT P.COD_FISC, P.COGNOME||' '||P.NOME AS Nominativo, " & _
	     " P.SESSO, TO_CHAR(P.DT_NASC,'DD/MM/YYYY') DT_NASC" & _
	     " FROM PERSONA P, DOMANDA_ISCR D WHERE " & _
		 " D.ID_BANDO=" & nIdBando & " AND D.ID_PERSONA = P.ID_PERSONA " & _
	       sqlsede & sWhere & sOrder

'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	rsRR.open sSQL,CC, 3
	if rsRR.EOF then
		%>
		<table width="500" border="0" cellspacing="1" cellpadding="0">
			<tr>
				<td class="tbltext3" align="center">
					<b>Non ci sono Candidati per i criteri selezionati</b>
				</td>
			</tr>	
		</table>
		<br>
		<table cellpadding="0" cellspacing="0" width="300" border="0">	
			<tr align="center">
				<td nowrap><a href="javascript:history.go(-1)" onmouseover="javascript:window.status=' '; return true"><img src="<%=session("Progetto")%>/images/indietro.gif" border="0" name="imgPunto2"></a><td>
			</tr>
		</table>
		<%	
		exit sub
	end if
	n = 1
	%>
	<br>
	<table width="500" border="0" cellspacing="1" cellpadding="0">
	    <tr>
	        <td width="80%" class="tbltext3">
				<%=sTitolo%>
			</td>
	    </tr>
	</table>
	<br>
	<br>
	<table width="500" border="0" cellspacing="1" cellpadding="0">
		<tr> 
			<td>
			 <span class="tbltext1">
				<p align="justify">
				La presente graduatoria si aggiorna costantemente anche in base
				alla modifica dei dati sensibili  causati dalla variazione dei dati individuali.
				Pertanto potrebbe differire dalla graduatoria ufficiale pubblicata per il bando.
			  </p>
			 </span>
			</td>
		</tr>
		<tr><td>&nbsp;</td></tr>
		<tr>
			<td class="tbltext1"><p align="justify"><%=sInfo%></p>
			</td>
		</tr>
	</table>
	<br>
	<table width="500" border="0" cellspacing="1" cellpadding="0">
		<tr class="sfondocomm">
			<td align="center"><b>N.</b></td>
			<td align="center"><b>Codice fiscale</b></td>
			<td align="center"><b>Nominativo</b></td>
			<td align="center"><b>Sesso</b></td>
			<td align="center"><b>Data di nascita</b></td>
		</tr>
<%		lPath = Server.MapPath("/") & session("progetto") & "/DocPers/Temp/" &_
	    Session("IdUtente") & sApNomeFile & nIdBando & "_" & sIdSede & ".xls"
		
		sNomeFile = session("progetto") & "/DocPers/Temp/" &_
	    Session("IdUtente") & sApNomeFile & nIdBando & "_" & sIdSede & ".xls"

	Set fso = CreateObject("Scripting.FileSystemObject")
	Set a = fso.CreateTextFile(lPath, True)
	a.writeline sTitolo1
	a.WriteLine ""
	a.writeline sTitolo 
	a.WriteLine ""
	a.WriteLine "Codice Fiscale" & chr(9) & "Nominativo" &  chr(9)& "Sesso" & chr(9) & "Data Nascita"
    rptIscritti=rsRR.GetString (2,, chr(9),chr(13), " ")
	a.WriteLine rptIscritti
	a.Close
	
	rsRR.MoveFirst

	Do while  not rsRR.EOF 
		
		sCodFisc = rsRR("COD_FISC")
		sNominativo = rsRR("NOMINATIVO")	
		sSesso = rsRR("SESSO")
		sDtNasc = rsRR("DT_NASC")
		%>
		<tr class="tblsfondo">
			<td align="center" class="tbltext1"><%=n%></td>
			<td align="center" class="tbltext1"><%=sCodFisc%></td>
			<td align="center" class="tbltext1"><%=sNominativo%></td>
			<td align="center" class="tbltext1"><%=sSesso%></td>
			<td align="center" class="tbltext1"><%=ConvDateToString(sDtNasc)%></td>
		</tr>
		<%
		rsRR.MoveNext
		n = n + 1	
	loop
	rsRR.Close
	set rsRR = nothing
	%>
	</table>
	<br>
	<label id="buttom">	
	<table cellpadding="0" cellspacing="0" width="500" border="0">	
		<tr align="center">
			<td nowrap><a href="javascript:self.print()" onmouseover="javascript:window.status=' '; return true"><img src="<%=session("Progetto")%>/images/stampa.gif" border="0" name="imgPunto1"></a><td>
	    
			<td nowrap><a href="javascript:frmstampa.submit()" onmouseover="javascript:window.status=' '; return true"><img src="<%=session("Progetto")%>/images/indietro.gif" border="0" name="imgPunto2"></a><td>
	    </tr>
	</table>
	<br>
	</label>
<%
end sub
'-----------------------------------------------------------------------------------------------------------------------------------------------------------

sub Fine()
%>
<!--#include virtual = "/include/closeconn.asp"-->
<%
end sub

%>
<!-- ************** ASP Fine *************** -->

<!-- ************** MAIN Inizio ************ -->
<%

dim sStatus
dim sIntestazione
dim sTipoGrad
dim sOrdinamento

dim sSede
dim sIdSede
dim sDescSede

dim sBando
dim nIdBando
dim sDescBando
dim DescProj


dim sNomeFile
dim sApNomeFile
'--------------------------------Mi prendo la descrizione del progetto ----------------
Ssql="select desc_proj from progetto where area_web= '" & Mid(session("progetto"),2) & "'"
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	set rsProg = cc.execute(Ssql)
	DescProj=rsProg("desc_proj")

'------------------------------------------------------------------------------------------
sStatus	= Request("cmbStatus")
sProv = Request("cmbProvBan")
sBando= Request("cmbBando")

sSede = trim(cstr(Request("cmbSede")))
if sSede <> "" then
	sIdSede = Clng(Mid(sSede, 1, InStr(1, sSede, "|") - 1))
	
	sDescSede = Trim(Mid(sSede, InStr(1, sSede, "|") + 1))
	sqlsede = "AND D.ID_sede=" & sIdSede
	else
	sqlsede = ""
end if

if sBando <> "" then		
		'nIdBando=Mid(sBando,1 ,InStr(1, sBando,"|") - 1)
		nIdBando = sBando
		sDescBando = DecBando(nIdBando)
		'sDescBando=Mid(sBando,InStr(1, sBando,"|") + 1)
end if	

sIntestazione = Request("Intestazione")
sOrdinamento = Request("cmbOrder")

Inizio()
ImpostaPag()
Fine()

%>
</div>
