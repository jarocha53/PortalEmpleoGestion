<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%'Option Explicit 
   Response.ExpiresAbsolute = Now() - 1 
   Response.AddHeader "pragma","no-cache"
   Response.AddHeader "cache-control","private"
   Response.CacheControl = "no-cache"
%>

<!-- #include virtual="/strutt_testa2.asp"-->
<!--#include virtual = "/include/openconn.asp"-->
<!--#include virtual="/util/portallib.asp"-->
<%
'if ValidateService(session("idutente"),"GRD_STMGRADPROV", CC) <> "true" then 
'	response.redirect "/util/error_login.asp"
'end if
%>
<!-- ************** Javascript inizio ************ -->

<script LANGUAGE="Javascript">

function VaiInizio(){
	location.href = "GRD_StmGradProv.asp";
}	

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

function InviaProv(){
	appoProv=frmProvincia.cmbProv.value
	if (appoProv == ""){
		alert("Selezionare la Provincia")
	}else
		frmProvincia.submit();
	}

//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

function InviaBando(){
	appoBando=frmBando.cmbBando.value
	if (appoBando == ""){
		alert("Selezionare il Bando")
	}else
		frmBando.submit();
	}
	
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

function InviaCImp(){
	appoCImp = frmCImp.cmbCImp.value
	if (appoCImp == ""){
		alert("Selezionare il Centro dell'Impiego")
	}else
		frmCImp.submit();
	}
	
</script>

<!-- ******************  Javascript Fine *********** -->
<!-- ************** ASP inizio *************** -->

<!--#include virtual = "/include/ckProfile.asp"-->
<%
'-------------------------------------------------------------------------------------------------------------------------------

Sub Msgetto(Msg)
%>
	<br><br>
	<table align="center">
		<tr>
			<td class="tbltext3"><b><%=Msg%></b></td>
		</tr>	
	</table>
<%
End Sub

'----------------------------------------------------------------------------------------------------------------------------------------------------  

sub Inizio()
%>
	<table border="0" width="520" cellspacing="0" cellpadding="0" height="81">
	   <tr>
	     <td width="500" background="<%=Session("Progetto")%>/images/titoli/strumenti2b.gif" height="81" valign="bottom" align="right">
	
	       <table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
	         <tr>
	             <td width="100%" valign="top" align="right">
	                 <b class="tbltext1a">Gestione Progetti</span></b>
	             </td>
	         </tr>
	       </table>
	     </td>
	   </tr>
	</table>
	<br>
	<table cellpadding="0" cellspacing="0" width="500" border="0">
		<tr height="18">
			<td class="sfondomenu" height="18" width="67%">
			<span class="tbltext0"><b>Lista ammissibili al progetto Alfabetizzazione</b></span></td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
			<td valign="middle" align="right" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1"></td>
		</tr>
		<tr width="371" class="SFONDOCOMM">
			<td colspan="3">Testo di prova<a href="Javascript:Show_Help('/Pgm/help/GestProgetti/Alfabeta/Graduatoria/GRD_StmGradProv')" onmouseover="javascript:window.status=' '; return true"><img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
			</td>
		</tr>
		<tr height="2">
			<td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>
	<br>
<%
end sub

'-----------------------------------------------------------------------------------------------------------------------------------------------------------

Sub ImpostazioneMASK()

	sMask = GetSectionVar("GRD_STMGRADPROV","MASK") 
'!!!!!!!!!!!!da togliere in seguito!!!!!!!!!!!!!!!!!!!!!!!!!
	sMask = "MTRMNA"
'!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	
	'sProv = Mid(sMask,1,2)
	nProv = len(sMask)/2
	i = 0	
	sSqlProv = ""

	if nProv <> 0 then
		for i=1 to nProv
			pos_ini = 2 * (i-1)
			'Mi ricavo la provincia e ci costruisco una srtinga
			sSqlProv = sSqlProv & "'" & mid(sMask,1+pos_ini,2) & "'," 
		next
		sSqlProv = mid(sSqlProv,1,(len(sSqlProv)-1))
	end if

end sub

'------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

sub ImpProvincie()

%>
<form method="POST" action="Grd_StmGradProv.asp?" name="frmProvincia">
	    <input type="hidden" name="MOD" value="1">
	    
		<table width="500" border="0" cellspacing="1" cellpadding="0">
			<tr>
				<td align="left" width="30%">
					<span class="tbltext1"><b>Provincia</b></span>
				</td>
				<td align="left" colspan="2">
	<% 				
	if trim(cstr(sProv)) <> "" then
		sCmbProv = "PROV|0|" & date & "|" & sProv & "|cmbProv' onchange='InviaProv()|AND CODICE IN(" & sSqlProv & ") ORDER BY DESCRIZIONE"
		'Response.Write "sCmbProv: " & sCmbProv & "<br>"
		CreateCombo(sCmbProv)
	else
		sCmbProv = "PROV|0|" & date & "||cmbProv' onchange='InviaProv()|AND CODICE IN(" & sSqlProv & ") ORDER BY DESCRIZIONE"
		'Response.Write "sCmbProv: " & sCmbProv & "<br>"
		CreateCombo(sCmbProv)
	end if
	%>
	           </td>
			</tr>
			<tr><td colspan="3">&nbsp;</td>
			</tr>
			<!--tr height="1"><td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif"></td></tr-->

	   </table>	   
	</form>
<%		

end sub

'------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

sub ImpBandi()

%>

<form method="POST" action="Grd_StmGradProv.asp?" name="frmBando">
	<input type="hidden" name="MOD" value="2">
	<input type="hidden" name="cmbProv" value="<%=sProv%>">
	<br>
	<table width="500" border="0" cellspacing="1" cellpadding="0">
		<tr>
			<td align="left" width="30%" class="tbltext1">
				<b>Provincia di </b>
			</td>
			<td align="left" class="tbltext1">
				<font color="red"><b><%=sProv%></b></font>
			</td>
		</tr>
		
	<%dim rsCount

	sSQL = "SELECT COUNT(ID_BANDO) as ContaBandi from BANDO " & _
			"WHERE ID_BANDO like '" & sProv & "%' " & _
			"AND " & ConvDateToDBs(now()) & " " & _
			"BETWEEN DT_FIN_ACQ_DOM AND DT_PUB_RIS_SEL " & _
			"ORDER BY ID_BANDO"
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	set rsCount = CC.Execute(sSQL)
						
	nCount = cint(rsCount("ContaBandi"))

	rsCount.close
	set rsCount = nothing
						
	select case nCount
		case 0
			'nel caso in cui non ci siano BANDI per la provincia
			'Msgetto ("Non ci sono Bandi Attivi per la Provincia richiesta")
		%>
		<tr><td class="tbltext3" colspan="2">&nbsp;</tr>
		<tr>
			<td class="tbltext3" colspan="2">
				<b>Non ci sono Bandi Attivi per la Provincia richiesta</b>
			</td>
		</tr>	
		<%			
		case 1
	'		sSQL = "SELECT ID_BANDO from BANDO WHERE ID_BANDO like '" & sProv & "%' ORDER BY ID_BANDO"
			sSQL = "SELECT ID_BANDO from BANDO " & _
					"WHERE ID_BANDO like '" & sProv & "%' " & _
					"AND " & convdatetodbs(now()) & " " & _
					"BETWEEN DT_FIN_ACQ_DOM AND DT_PUB_RIS_SEL " & _
					"ORDER BY ID_BANDO"

'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
			set rsBando = CC.Execute(sSQL)
			%>	
				<input type="hidden" name="cmbBando" value="<%=rsBando("ID_BANDO")%>">
				<script Language="JAVASCRIPT">
					InviaBando()
				</script>
			<%		
			rsBando.Close
			set rsBando = nothing
		case else
			'Ho pi� bandi quindi creo la combo
	'		sSQL = "SELECT ID_BANDO from BANDO WHERE ID_BANDO like '" & sProv & "%' ORDER BY ID_BANDO"
			sSQL = "SELECT ID_BANDO from BANDO " & _
					"WHERE ID_BANDO like '" & sProv & "%' " & _
					"AND " & convdatetodbs(now()) & " " & _
					"BETWEEN DT_FIN_ACQ_DOM AND DT_PUB_RIS_SEL " & _
					"ORDER BY ID_BANDO"

'PL-SQL * T-SQL  
SSQL%> = TransformPLSQLToTSQL (SSQL%>) 
			set rsBando = CC.Execute(sSQL)%>					
		<tr>						
			<td align="left" width="30%" class="tbltext1">
				<b>Bando</b>
			</td>
			<td align="left">
				<select ID="cmbBando" name="cmbBando" class="textblacka" onchange="InviaBando()">
				<option value></option>
				<%  
				do while not rsBando.EOF
				%>
					<option value="<%=rsBando("ID_BANDO")%>"><%=rsBando("ID_BANDO")%></option>
				<%
					rsBando.MoveNext
				loop
				%>
				</select>
			</td>
		</tr>
	<%					
			rsBando.Close
			set rsBando = nothing
	end select
	%>
</table>
<br><br>
<table cellpadding="0" cellspacing="0" width="300" border="0">	
	<tr align="center">
		<td nowrap><a href="javascript:VaiInizio()" onmouseover="javascript:window.status=' '; return true"><img src="<%=session("Progetto")%>/images/indietro.gif" border="0" name="imgPunto2"></a><td>
    </tr>
</table>

</form>
<%

end sub

'------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

sub ImpCImpiego()

		sStatus = 0
		sIntestazione = ""

		%>
<br>
<form method="POST" action="GRD_StmGrad.asp" name="frmCImp">
	<input type="hidden" NAME="Bando" ID="Bando" VALUE="<%=sBando%>">
	<input type="hidden" name="Status" value="<%=sStatus%>">
	<input type="hidden" name="Intestazione" value="<%=sIntestazione%>">

	<table width="500" border="0" cellspacing="1" cellpadding="0">
		<tr class="tbltext1">
			<td align="left" width="30%">
				<b>Provincia di </b>
			</td>
			<td align="left">
				<font color="red"><b><%=sProv%></b></font>
			</td>
		</tr>
		<tr class="tbltext1">
			<td align="left" width="30%">
				<b>Bando </b>
			</td>
			<td>
				<font color="red"><b><%=sBando%></b></font>
			</td>
		</tr>
		<%
'		sSQL = "SELECT ID_CIMPIEGO,DESC_CIMPIEGO,PRV FROM CENTRO_IMPIEGO WHERE PRV ='" & sProv & "' AND STATUS_GRAD =" & sStatus & " AND DT_ELAB_GRAD IS NOT NULL"
		sSQL = "SELECT ID_CIMPIEGO,DESC_CIMPIEGO,PRV " & _
				"FROM CENTRO_IMPIEGO WHERE PRV ='" & sProv & "' " & _
				"AND STATUS_GRAD =" & sStatus
		'Response.Write sSQL
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
		set rsCImpiego = CC.Execute(sSQL)						
		
		if rsCImpiego.eof then
			'Msgetto ("La Graduatoria � gi� stata elaborata")
		%>
		<tr><td class="tbltext3" colspan="2">&nbsp;</tr>
		<tr>
			<td class="tbltext3" align="center" colspan="2">
				<b>La Graduatoria � gi� stata elaborata</b>
			</td>
		</tr>	
		<%			
		else
		%>		
		<br>
		<tr>
			<td align="left" width="30%" class="tbltext1">
				<b>Centro per l'Impiego</b>
			</td>
			
			<td align="left">
				<select ID="cmbCImp" name="cmbCImp" class="textblacka" onchange="InviaCImp()">
				<option value></option>
					<%
				do while not rsCImpiego.EOF
					%>
					<option value="<%=rsCImpiego("ID_CIMPIEGO") & "|" & rsCImpiego("DESC_CIMPIEGO")%>"><%=rsCImpiego("DESC_CIMPIEGO")%></option>
					<%
					rsCImpiego.MoveNext
				loop
					%>
				</select>
			</td>
				<%
			rsCImpiego.Close
			set rsCImpiego = nothing
		end if %>
		</tr>
	</table>
	<br><br>
	<table cellpadding="0" cellspacing="0" width="300" border="0">	
		<tr align="center">
			<td nowrap><a href="javascript:VaiInizio()" onmouseover="javascript:window.status=' '; return true"><img src="<%=session("Progetto")%>/images/indietro.gif" border="0" name="imgPunto2"></a><td>
	    </tr>
	</table>
	
</form>
<%

end sub

'------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

sub Fine()
%>
<!--#include virtual = "/include/closeconn.asp"-->
<!--#include virtual = "strutt_coda2.asp"-->
<%
end sub

%>
<!-- ************** ASP Fine *************** -->

<!-- ************** MAIN Inizio ************ -->
<%

dim sMask
dim sSqlProv
dim sSQL

dim sModo
dim sProv
dim sBando

sModo = Request("MOD")
sProv = Request("cmbProv")
sBando = Request("cmbBando")

Inizio()
ImpostazioneMASK()

select case sModo
	case 1
		'E' la seconda volta che entro, combo BANDI
		ImpBandi()
	case 2
		'E' la terza volta che entro, combo CENTRI DELL'IMPIEGO
		ImpCImpiego()
	case else
		'E' la prima volta che entro, combo PROVINCIE
		ImpProvincie()
	end select

Fine() 
	
%>
<!-- ************** MAIN Fine ************* -->


