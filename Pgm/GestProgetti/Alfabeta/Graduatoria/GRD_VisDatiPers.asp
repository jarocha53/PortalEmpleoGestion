<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #include virtual="/util/portallib.asp"-->
<%
if ValidateService(session("idpersona"),"grd_selsituaz") <> "true" then 
	response.redirect "/util/error_login.asp"
end if
%>

<%
	Function Month2(D2E)
		if isdate(D2E) then
			Mese = month(D2E)
			Select Case Mese
				Case 1
					Month2 = "Gennaio"
				Case 2
					Month2 = "Febbraio"
				Case 3
					Month2 = "Marzo"
				Case 4
					Month2 = "Aprile"
				Case 5
					Month2 = "Maggio"
				Case 6
					Month2 = "Giugno"
				Case 7
					Month2 = "Luglio"
				Case 8
					Month2 = "Agosto"
				Case 9
					Month2 = "Settembre"
				Case 10
					Month2 = "Ottobre"
				Case 11
					Month2 = "Novembre"
				Case 12
					Month2 = "Dicembre"
				Case Else
					Month2 = "XXX"
			End Select
		else
			Month2 = "XXX2"
		end if
	End Function

sub FormPresOFad(flPres)
	sSQL="select  distinct v.desc_cform, t.descrizione, s.dt_inisess,s.dt_finsess, s.id_edizione " &_
		" from tades t,centro_formazione v, aula po, componenti_classe cc, classe c, sess_form s" &_
		" where T.NOME_TABELLA = 'SESFO' AND T.CODICE = s.cod_sessione and cc.id_persona=" & sIdPers &_
		" AND (NOT(PO.TIPO_AULA LIKE 'C%') OR (PO.TIPO_AULA IS NULL))" &_
		" and cc.cod_ruolo='DI'" &_
		" and cc.id_classe=c.id_classe " &_
		" and c.id_edizione=s.id_edizione " &_
		" and PO.ID_AULA=C.ID_AULA " &_
		" and PO.ID_CFORM= V.ID_CFORM " &_
		" and s.fl_presenza='" & flPres & "' " &_
		" and cc.dt_ruolodal <= s.dt_inisess and (cc.dt_ruoloal >= s.dt_finsess or cc.dt_ruoloal is null) " &_
		" order by s.dt_inisess " 
		'Response.Write SsqL
end sub
sub RicTutor()
       SQL=" SELECT cb.id_persona FROM componenti_classe cb" &_ 
                " WHERE cb.cod_ruolo = 'TU' AND cb.id_classe IN" &_
                "(SELECT distinct ca.id_classe FROM COMPONENTI_CLASSE ca" &_
                " WHERE ca.id_persona =" & sIdPers   & ")" &_
                " AND " & ConvDateToDb(Now()) & " BETWEEN cb.dt_ruolodal AND cb.dt_ruoloal"
                'Response.Write SQL
'PL-SQL * T-SQL  
OPTIMIZESQLSQL = TransformPLSQLToTSQL (OPTIMIZESQLSQL) 
                set sTut = CC.Execute(OptimizeSQL(SQL))
                
                                              
                if sTut.eof = false then 
                    nIdTutor= clng(sTut.Fields("id_persona"))
                else
                    nIdTutor = 0
                    Err.Clear 
                end if   
                sTut.close
                set sTut=nothing         
end sub 

sub RicNomeTut()
       SQL=" SELECT cognome,nome from persona where id_persona= " & nIdTutor
'PL-SQL * T-SQL  
OPTIMIZESQLSQL = TransformPLSQLToTSQL (OPTIMIZESQLSQL) 
                set sTutDen = CC.Execute(OptimizeSQL(SQL))
                                              
                if sTutDen.eof = false then 
                    sCognome= sTutDen.Fields("cognome")
                    sNome   = sTutDen.Fields("nome")
                else
                    sCognome = ""
                    sNome    = ""
                    Err.Clear 
                end if   
                sTutDen.close
                set sTutDen=nothing         
end sub 
%>
<html>
<head>
<!--#include virtual = "include/ckProfile.asp"-->
<!--#include virtual = "include/ControlDateVB.asp"-->
<!--#include virtual = "util/dbutil.asp"-->
<!--#include virtual = "include/Soglia.asp"-->
<!--#include virtual ="include/openconn.asp"-->
<LINK REL=STYLESHEET TYPE="text/css" HREF="../../fogliostile.css">
<title>Visualizzazione Dati Persona</title>

</head>

<body background="../../images/sfondo1.gif" >
<%
dim rsRes,nIdTutor,sCognome,sNome,sSQL
sIdPers = Request.QueryString("idpersona")
'Response.Write "sIdPers = " & sIdPers & "<br>"
'sCognome = request("Cognome")
'sNome = request("Nome")
'+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-
'La riga qui sotto va cancellata
'sIdPers = 13520
'+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-

'Estrazione dei dati personali
sSQL = "Select Cognome, Nome, dt_nasc, com_nasc, prv_nasc,cap_res, cod_fisc, stat_cit, com_res,ind_res, prv_res,num_tel,e_mail from persona where id_persona = " & sIDPers
'Response.Write ssql & "<br>"
'PL-SQL * T-SQL  
OPTIMIZESQLSSQL = TransformPLSQLToTSQL (OPTIMIZESQLSSQL) 
set rsRes = CC.Execute(OptimizeSQL(sSQL))
sCognome = rsRes("cognome")
sNome = rsRes("nome")
dDtNasc = ConvDateToString(rsRes("dt_nasc"))
sComNasc = rsRes("com_nasc")
sPrvNasc = rsRes("prv_nasc")
sCodFis = rsRes("cod_fisc")
sStatCit = rsRes("stat_cit")
sComRes = rsRes("com_res")
sPrvRes = rsRes("prv_res")
sNumTel = rsRes ("num_tel")
sEMail = rsRes ("e_mail")
sIndRes = rsRes ("ind_res")
sCap = rsRes ("cap_res")
rsRes.close
set rsRes = nothing
'Decodifica della cittadinanza
sSQL = "select descrizione from tades where nome_tabella = 'STATO' and codice = '" & sStatCit & "'"
'PL-SQL * T-SQL  
OPTIMIZESQLSSQL = TransformPLSQLToTSQL (OPTIMIZESQLSSQL) 
set rsSt = CC.execute(OptimizeSQL(sSQL))
sDStatCit=""
IF NOT rsST.eof then sDStatCit = rsSt("descrizione")
rsSt.close
set rsSt = nothing
'Decodifica dei comuni di nascita e residenza
sSQL = "SELECT descom from comune WHERE codcom ='" & sComNasc & "'"
'Response.Write ssql & "<br>"
'PL-SQL * T-SQL  
OPTIMIZESQLSSQL = TransformPLSQLToTSQL (OPTIMIZESQLSSQL) 
set rsCom = CC.Execute(OptimizeSQL(sSQL))
if not rsCom.eof then
	sDComNasc = rsCom("descom")
	rsCom.close
else
	sDComNasc = "Comune non presente"
end if		
sSQL = "SELECT descom from comune WHERE codcom ='" & sComRes & "'"
'Response.Write ssql & "<br>"
'PL-SQL * T-SQL  
OPTIMIZESQLSSQL = TransformPLSQLToTSQL (OPTIMIZESQLSSQL) 
set rsCom = CC.Execute(OptimizeSQL(sSQL))
sDComRes = rsCom("descom")
rsCom.close
set rsCom = nothing

'Estrazione del Centro per l'impiego e data decorrenza disoccupazione
sSQL =	"Select c.desc_cimpiego, s.dt_dec_dis from stato_occupazionale s, centro_impiego c " &_
		"where s.id_cimpiego = c.id_cimpiego and s.id_persona = " & sIDPers
		'Response.Write sSQL
'PL-SQL * T-SQL  
OPTIMIZESQLSSQL = TransformPLSQLToTSQL (OPTIMIZESQLSSQL) 
set rsCPI = cc.execute(OptimizeSQL(sSQL))
if not rsCPI.eof then
	sCPI = rsCPI("desc_cimpiego")
	sAD = Month2(rsCPI("dt_dec_dis")) & " " & year(rsCPI("dt_dec_dis"))
else
	sCPI = "Non trovato"
	sAD = "Non trovata"
end if
rsCPI.close
set rsCPI = nothing

'Estrazione dello 'Status Occupazionale'
sSQL =	"Select t.descrizione from tades t, stato_occupazionale s " &_
		"where s.cod_stdis = t.codice and t.nome_tabella = 'STDIS' and " &_
		"s.id_persona = " & sIDPers
'PL-SQL * T-SQL  
OPTIMIZESQLSSQL = TransformPLSQLToTSQL (OPTIMIZESQLSSQL) 
set rsSO = cc.execute(OptimizeSQL(sSQL))
if not rsSO.eof then
	sSO = rsSO("descrizione")
else
	sSO = "Non Trovato"
end if
rsSO.close
set rsSO = nothing

'Estrazione dell'esito della domanda

%>
<table width=630 border=0 cellspacing=2 cellpadding=1>
	<tr> 
		<td colspan=2 align=middle background=../../images/sfmnsmall.jpg>
		<b>
		<span class="titolo">
			Fascicolo Personale di "<I><%=UCASE(sCognome)%>&nbsp;<%=UCASE(sNome)%></I>"
		</span>
		</b>
		</td>
	</tr>
</table>
<br>
<table border=0 cellspacing=2 cellpadding=1 width=630>
	<tr height=23>
		<td class=tbllabel1 align=right width=200>
			Data di Nascita&nbsp;
		</td>
		<td class=tblsfondo width=430>
			<span class=tbltext>
				&nbsp;<%=dDtNasc%>
			</span>
		</td>
	</tr>
	<tr height=23>
		<td class=tbllabel1 align=right>
			Comune di Nascita&nbsp;
		</td>
		<td class=tblsfondo width=430>
			<span class=tbltext>
				&nbsp;<%=sDComNasc%> (<%=UCASE(sPrvNasc)%>)
			</span>
		</td>
		</tr>
		<tr height=23>
		<td class=tbllabel1 align=right>
			Cittadinanza&nbsp;
		</td>
		<td class=tblsfondo>
			<span class=tbltext>
				&nbsp;<%=UCASE(sDStatCit)%>
			</span>
		</td>
	</tr>
	<tr height=23>
		<td class=tbllabel1 align=right>
			Codice Fiscale&nbsp;
		</td>
		<td class=tblsfondo>
			<span class=tbltext>
				&nbsp;<%=UCASE(sCodFis)%>
			</span>
		</td>
	</tr>
	<tr height=23>
		<td class=tbllabel1 align=right>
			Indirizzo &nbsp;
		</td>
		<td class=tblsfondo>
			<span class=tbltext>
				&nbsp;<%=UCASE(sIndRes)%> 
			</span>
		</td>
	</tr>
	<tr height=23>
		<td class=tbllabel1 align=right>
			Cap &nbsp;
		</td>
		<td class=tblsfondo>
			<span class=tbltext>
				&nbsp;<%=sCap%> 
			</span>
		</td>
	</tr>
	
	<tr height=23>
		<td class=tbllabel1 align=right>
			Comune di Residenza&nbsp;
		</td>
		<td class=tblsfondo>
			<span class=tbltext>
				&nbsp;<%=UCASE(sDComRes)%> (<%=UCASE(sPrvRes)%>)
			</span>
		</td>
	</tr>
	<tr height=23>
		<td class=tbllabel1 align=right>
			Telefono &nbsp;
		</td>
		<td class=tblsfondo>
			<span class=tbltext>
				&nbsp;<%=UCASE(sNumTel)%> 
			</span>
		</td>
	</tr>
	<tr height=23>
		<td class=tbllabel1 align=right>
			E-Mail &nbsp;
		</td>
		<td class=tblsfondo>
			<span class=tbltext>
				&nbsp;<%=sEMail%> 
			</span>
		</td>
	</tr>
	<tr height=23>
		<td class=tbllabel1 align=right>
			Centro per l'Impiego&nbsp;
		</td>
		<td class=tblsfondo>
			<span class=tbltext>
				&nbsp;<%=UCASE(sCPI)%>
			</span>
		</td>
	</tr>
	<tr height=23>
		<td class=tbllabel1 align=right>
			Status Occupazionale&nbsp;
		</td>
		<td class=tblsfondo>
			<span class=tbltext>
				&nbsp;<%=UCASE(sSO)%>
			</span>
		</td>
	</tr>
	<tr height=23>
		<td class=tbllabel1 align=right>
			Anzianit� di disoccupazione&nbsp;
		</td>
		<td class=tblsfondo>
			<span class=tbltext>
				&nbsp;<%=UCASE(sAd)%>
			</span>
		</td>
	</tr>
  </table>
<br>

<table width=630 border=0 cellspacing=2 cellpadding=1>
<%
sSQL =	"select ta.descrizione, ti.cod_stat_stud, ti.aa_stud " &_
		"from tistud ti, tades ta " &_
		"where " &_
		"ti.cod_liv_stud = ta.codice and " &_
		"ta.nome_tabella = 'TSTUD' and " &_
		"ti.id_persona = " & sIDPers & " " &_
		"order by ti.COD_STAT_STUD, ti.aa_stud"
		'Response.Write sSQL
oldCSSt = -1
'PL-SQL * T-SQL  
OPTIMIZESQLSSQL = TransformPLSQLToTSQL (OPTIMIZESQLSSQL) 
set rsTS = cc.execute(OptimizeSQL(sSQL))
do while not rsTS.eof
	if rsTS("cod_stat_stud") <> oldCSSt then
		oldCSSt = rsTS("cod_stat_stud")
		select case rsTS("cod_stat_stud")
			Case 0		'Conseguito
				response.write ("<tr height=23><td class=tbllabel1>&nbsp;Titolo di Studio</td></tr>")
				sFrase = rsTS("descrizione") & " CONSEGUITO NEL " & rsTS("aa_stud")
			Case 2		'Frequenta
				response.write ("<tr height=23><td class=tbllabel1>&nbsp;Attualmente Frequenta</td></tr>")
				sFrase = rsTS("descrizione") & " DAL " & rsTS("aa_stud")
		end select
		response.write ("<tr height=23><td class=tblsfondo><span class=tbltext>&nbsp;" & sFrase & "</span</td></tr>")
	else
		select case rsTS("cod_stat_stud")
			Case 0		'Conseguito
				sFrase = rsTS("descrizione") & " conseguito nel " & rsTS("aa_stud")
			Case 2		'Frequenta
				sFrase = rsTS("descrizione") & " dal " & rsTS("aa_stud")
		end select
		response.write ("<tr height=23><td class=tblsfondo><span class=tbltext>&nbsp;" & sFrase & "</span</td></tr>")
	end if
	rsTS.movenext
loop
rsTS.close
set rsTS = nothing
%>
</table>

<table width=630 border=0 cellspacing=2 cellpadding=1>
 <tr height=23><td class=tbllabel1>&nbsp;Presentazione Domanda di Iscrizione</td></tr>
 <% dim rsDom
 
 sSQL = "SELECT DT_DOM,ID_BANDO from DOMANDA_ISCR WHERE ID_PERSONA =" & sIDPers  
          
'PL-SQL * T-SQL  
OPTIMIZESQLSSQL = TransformPLSQLToTSQL (OPTIMIZESQLSSQL) 
set rsDom= CC.Execute(OptimizeSQL(sSQL))
sBando = rsDom("ID_BANDO") 

sDom = ConvDateToString(rsDom("DT_DOM")) & " PER IL BANDO DI " & "<b>" & sBando & "</b>"

 
 %>
 <tr>
  <td class=tblsfondo><span class=tbltext>
 <%=sDom%>
  </span>
 </td>
</tr>
</TABLE>

<table width=630 border=0 cellspacing=2 cellpadding=1>
	<tr height=23><td class=tbllabel1>&nbsp;Esito della Domanda</td></tr>
<%
dim rsGrad
sSQL = "SELECT A.Status_Grad from CENTRO_IMPIEGO A,STATO_OCCUPAZIONALE B" &_
		" WHERE A.ID_CIMPIEGO = B.ID_CIMPIEGO AND B.ID_PERSONA = " & sIDPers
'PL-SQL * T-SQL  
OPTIMIZESQLSSQL = TransformPLSQLToTSQL (OPTIMIZESQLSSQL) 
set rsStatus = CC.Execute(OptimizeSQL(sSQL))
sStatus = cint(rsStatus("Status_Grad"))

	select case sStatus
		case 0 
			sDescEsito = "Graduatoria in corso di elaborazione"
		case 1
			sSQL = "SELECT Cod_Esito,Esito_Grad, punteggio from DOMANDA_ISCR" &_
					" WHERE ID_PERSONA =" & sIDPers
			'Response.Write sSQL

'PL-SQL * T-SQL  
OPTIMIZESQLSSQL = TransformPLSQLToTSQL (OPTIMIZESQLSSQL) 
			set rsGrad = CC.Execute(OptimizeSQL(sSQL))
			sCodEsito = rsGrad("Cod_Esito")
			sEsitoGrad = rsGrad("Esito_Grad")
			if sCodEsito <> "" then
				lCodEsito = len(sCodEsito)
				for i = 1 to lCodEsito
					Codice = mid(sCodEsito,i,1)
					sSQL = "SELECT Desc_Regola from REGOLE WHERE Cod_Regola ='" & Codice & "'"
					'Response.Write sSQL
'PL-SQL * T-SQL  
OPTIMIZESQLSSQL = TransformPLSQLToTSQL (OPTIMIZESQLSSQL) 
					set rsDesc = CC.Execute(OptimizeSQL(sSQL))
					sDescEsito = "ESCLUSO:" & rsDesc("Desc_Regola")
					
				next	
			ELSE			    
				if sEsitoGrad="" and punteggio>0 then
					sDescEsito = "AMMISSIBILE NON VINCITORE"
				else
					sSQL = "SELECT DESC_REGOLA FROM REGOLE" &_
							" WHERE TIPO_REGOLA ='W' AND Cod_Regola ='" & sEsitoGrad & "'"
					'Response.Write sSQL
'PL-SQL * T-SQL  
OPTIMIZESQLSSQL = TransformPLSQLToTSQL (OPTIMIZESQLSSQL) 
					set rsDesc = CC.Execute(OptimizeSQL(sSQL))
					if not rsDesc.eof then 
						sDescEsito = rsDesc("Desc_Regola")	
					end if	
				end if		
			end if
			
		end select
				
	response.write "<tr height=23><td class=tblsfondo><span class=tbltext>&nbsp;" & sDescEsito & "</span></td></tr>"
%>
</table>
<!-----------------------Inserimento Sospensioni/Rientri---------------------------------------------------------->
	
	
<%      sSQL=" SELECT T.DESCRIZIONE, US.COD_SOSP, us.dt_dal, us.dt_al FROM UTE_SOSPESI US, TADES T " &_
	         " WHERE T.NOME_TABELLA = 'MSOSP' AND US.ID_PERSONA = " & sIDPers & " AND us.cod_sosp = t.codice" 
	         
	         'Response.Write sSQL
'PL-SQL * T-SQL  
OPTIMIZESQLSSQL = TransformPLSQLToTSQL (OPTIMIZESQLSSQL) 
			set sRin = CC.Execute(OptimizeSQL(sSQL))
			
		if not sRin.eof then   
		%>	<table width=630 border=0 cellspacing=2 cellpadding=1>
				<tr height=23>
					<td class=tbllabel1 width=55%>&nbsp;Sospensioni/Rinunce</td>
					<td class=tbllabel1 width=30%>&nbsp;Dal</td>
					<td class=tbllabel1 width=30%>&nbsp;Al</td>
				</tr>     
				<tr height=23>
					<td class=tblsfondo ><span class=tbltext>&nbsp;<%=ucase(sRin("descrizione"))%></span></td>
					<td class=tblsfondo ><span class=tbltext>&nbsp;<%=ConvDateToString(sRin("dt_dal"))%></span></td>
					<td class=tblsfondo ><span class=tbltext>&nbsp;<%=ConvDateToString(sRin("dt_al"))%></span></td>
				</tr>
			</table>     
<%      end if %>

<!---------------------------------------------------------------------------------------------------------->



<table width=630 border=0 cellspacing=2 cellpadding=1>
<tr height=23>
	<td class=tbllabel1 width=44%>&nbsp;Formazione</td>
	<td class=tbllabel1 width=4%>&nbsp;Ediz.</td>
	<td class=tbllabel1 width=10%>&nbsp;Dal</td>
	<td class=tbllabel1 width=10%>&nbsp;Al</td>
	<td class=tbllabel1 width=20%>&nbsp;Centro Formazione </td>
	
</tr>
<% dim rsSess

FormPresOFad("S")
 
'PL-SQL * T-SQL  
OPTIMIZESQLSSQL = TransformPLSQLToTSQL (OPTIMIZESQLSSQL) 
set rsSess= CC.Execute(OptimizeSQL(sSQL))
    
if not rsSess.eof then
  
do while not rsSess.eof 
      %>
	<tr height=23>
		
		<td class=tblsfondo ><span class=tbltext><%=ucase(rsSess("descrizione"))%> </span></td>
		<td class=tblsfondo  align=center><span class=tbltext><%=(rsSess("id_edizione"))%> </span></td>
		<td class=tblsfondo ><span class=tbltext>&nbsp;<%=ConvDateToString(rsSess("dt_inisess"))%></span></td>
	    <td class=tblsfondo ><span class=tbltext>&nbsp;<%=ConvDateToString(rsSess("dt_finsess"))%></span></td> 
	    <td class=tblsfondo ><span class=tbltext>&nbsp;<%=rsSess("desc_cform")%></span></td>
  </tr>

<% rsSess.movenext
  
loop 
rsSess.close
set rsSess=nothing		 
    
   else    
%>  
    <td class=tblsfondo width=20% colspan=4><span class=tbltext>&nbsp;NESSUNA</span></td>
 </tr>
</table>     
<% 
   end if%>
   
   
   
   <table width=630 border=0 cellspacing=2 cellpadding=1>
	<tr height=23>
		<td class=tbllabel1 colspan=4>&nbsp;Tutor Attivo</td>
</tr>  
<%  RicTutor
    if  nIdTutor > 0 then  
        RicNomeTut
        if sCognome <> "" then
%>
	<tr height=23>
		<td class=tblsfondo colspan=4><span class=tbltext>&nbsp;<%=sCognome%>&nbsp;<%=sNome%></span></td>
 
   <%   else %>   
        <td class=tblsfondo colspan=4 ><span class=tbltext>&nbsp;NESSUNO</span></td>
        
<%      end if 
   else    %>
        <td class=tblsfondo colspan=4 ><span class=tbltext>&nbsp;NESSUNO</span></td>
<% end if  %>
	</tr>
   </table>  
 
<!--table width=630 border=0 cellspacing=2 cellpadding=1>
<tr height=23>
	<td class=tbllabel1 width=60%>&nbsp;Questionari formazione</td>
	<td class=tbllabel1 width=20%>&nbsp;Dal</td>
	<td class=tbllabel1 width=20%>&nbsp;Al</td>
	<td class=tbllabel1 width=20%>&nbsp;Centro Impiego </td>
</tr>
<% dim rsDist
 
sSQL ="SELECT  t.descrizione, cc.id_persona, cc.dt_ruolodal, cc.dt_ruoloal, " &_
      "sf.dt_inisess, sf.dt_finsess, ci.desc_cimpiego, ci.prv " &_
"FROM TADES T, componenti_classe cc, classe cl, sess_form sf, " &_
      "edizione ed, stato_occupazionale so, " &_
      "centro_impiego ci " &_
"WHERE T.NOME_TABELLA = 'SESFO' AND " &_
    "T.CODICE = sf.cod_sessione and  cc.id_persona = " & sIDPers & " AND " &_
    "CC.DT_RUOLOAL IS NULL AND " &_
    "cc.id_persona = so.id_persona AND " &_
    "so.id_cimpiego = ci.id_cimpiego AND " &_
    "cc.id_classe = cl.id_classe AND " &_
    "cl.id_edizione = ed.id_edizione AND " &_
    "ed.id_edizione = sf.id_edizione AND " &_
    "sf.fl_presenza NOT LIKE 'S%' and sf.fl_presenza NOT LIKE 'N%' " &_
"ORDER BY sf.dt_inisess" 
 
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
set rsDist= CC.Execute(sSQL)
    
if not rsDist.eof then
	while not rsDist.eof%> 
	<tr height=23>
		<td class=tblsfondo ><span class=tbltext>&nbsp;<%=rsDist("descrizione")%> </span></td>
		<td class=tblsfondo ><span class=tbltext>&nbsp;<%=ConvDateToString(rsDist("dt_inisess"))%></span></td>
	    <td class=tblsfondo ><span class=tbltext>&nbsp;<%=ConvDateToString(rsDist("dt_finsess"))%></span></td> 
	    <td class=tblsfondo ><span class=tbltext>&nbsp;<%=rsDist("desc_cimpiego")%></span></td></tr>
	    
	<% rsDist.movenext
   wend 
		rsDist.close		
else%>  
  <td class=tblsfondo width=20% colspan=4><span class=tbltext>&nbsp;Nessuno</span></td>
  </tr>
</table>     
<% 
end if
set rsDist=nothing
%> 
</table-->    






  <table width=630 border=0 cellspacing=2 cellpadding=1>
<tr height=23>
	<td class=tbllabel1 width=51%>&nbsp;Questionari eseguiti</td>
	<td class=tbllabel1 width=60% COLSPAN = 3>&nbsp;Data</td>
	<!--td class=tbllabel1 width=20% >&nbsp;Esito</td-->
</tr>  
 
<% dim rsTestGrd  
   dim swGTrovato
   swGTrovato=false
   
   sSQL = "select a.id_infoquest,a.desc_quest,b.dt_esec,b.eseguito  from info_quest a,iq_risultato b where a.id_infoquest=b.id_infoquest and id_persona =" & sIDPers 
'PL-SQL * T-SQL  
OPTIMIZESQLSSQL = TransformPLSQLToTSQL (OPTIMIZESQLSSQL) 
   set rsTestGrd = CC.Execute(OptimizeSQL(sSQL))
   do while not rsTestGrd.eof  
   swGTrovato=true
	'select case rsTestGrd("id_infoquest")
	'case "2"
	
		'sQuestG = "Effettuato il "
%>		
	<tr height=23><td class=tblsfondo width=51%><span class=tbltext>&nbsp;<%=ucase(rsTestGrd("desc_quest"))%></span></td>
	<td class=tblsfondo width=60%><span class=tbltext>&nbsp;<%=ConvDateToString(rsTestGrd("dt_esec"))%></span>
	<!--td class=tblsfondo width=20%>
<%	
		'if rsTestGrd("eseguito")= "N" THEN 
	 
%>   <span class=tbltext> &nbsp;esito negativo</span></td></tr> 
	 
<%		'else

%>	 
    <!--span class=tbltext3>&nbsp; esito positivo</span></td--></tr> 
    
<%
		'end if 
					     
		rsTestGrd.movenext
		loop
		if not swGTrovato then 
%>		
		<td class=tblsfondo width=60% colspan=4><span class=tbltext>&nbsp;NESSUNO</span></td>
		<%
		end if
		
		rsTestGrd.close
   set rsTestGrd = nothing
%>	
<!--table width=630 border=0 cellspacing=2 cellpadding=1>
 <tr height=23><td class=tbllabel1><span class=tblwhite>Bonus</span></td>
  <td class=tbllabel1><span class=tblwhite>Esito</span></td></t-->
<%  sSQl = "SELECT ID_QUESTIONARIO,fl_eseguito,NUM_PUNTI,DT_ESEC FROM RISULT_QUEST WHERE ID_PERSONA = " & sIDPers  & " AND ID_QUESTIONARIO=100" %>

'PL-SQL * T-SQL  
OPTIMIZESQLSSQL = TransformPLSQLToTSQL (OPTIMIZESQLSSQL) 
<%    set  rs= CC.Execute(OptimizeSQL(sSQL))
      'if rs.bof <>FALSE and rs.eof <>FALSE then
      'Response.Write sSQl
      flag=false
      while not rs.eof
	    flag=true
 %>
		<br>
     
		<table width=630 border=0 cellspacing=2 cellpadding=1>
		<tr height=23>
		<td class=tbllabel1 width=51%><span class=tblwhite>&nbsp;Test Bonus</span></td>
		<td class=tbllabel1 width=70%><span class=tblwhite>&nbsp;Punteggio Bonus</span></td>
		<td class=tbllabel1 width=70%><span class=tblwhite>&nbsp;Esito</span></td>
		</tr>
		<tr height=23>
		<td class=tblsfondo ><span class=tbltext>&nbsp;<%=ConvDateToString(rs("DT_ESEC"))%></span></td>
		<td class=tblsfondo><span class=tbltext>&nbsp;<%=rs("NUM_PUNTI")%></span></td>
<%		on error resume next
		NP= 0 + cint(rs("NUM_PUNTI"))
		ON ERROR GOTO 0
	   if rs("fl_eseguito")="S" then
			IF Soglia(100,NP,0) THEN%>
			<td class=tblsfondo><span class=tbltext3>&nbsp;POSITIVO</span></td>
			</tr>
	<%        ELSE%>
	        <td class=tblsfondo><span class=tbltext>&nbsp;NEGATIVO</span></td>
			</tr>
	<%		END IF
	   else%>
	        <td class=tblsfondo><span class=tbltext>&nbsp;VISUALIZZATO E NON CONFERMATO</span></td>
			</tr>
	<% end if 
	
		rs.movenext
      Wend
      rs.close
      set rs=nothing
      
 if flag<>true then%> 
      
    <!--'if rs.bof = true and rs.eof=true then -->
	<table width=630 border=0 cellspacing=2 cellpadding=1>
	<tr height=23>
	<td class=tbllabel1><span class=tblwhite>&nbsp;Bonus </span></td></tr>
	<td class=tblsfondo><span class=tbltext>&nbsp;NESSUNO </span></td></tr></table>
   
      
 <%
 end if%>
 <table width=630 border=0 cellspacing=2 cellpadding=1>
  <tr height=23>
	<td class=tbllabel1 width=51%><span class=tblwhite>&nbsp;Bonus prenotato presso </span></td>
     <td class=tbllabel1 width=60%><span class=tblwhite>&nbsp;Data di prevista consegna </span></td></tr>
  <% dim rsBonus
  
  sSQL="SELECT A.DT_TMST, RAG_SOC FROM BONUS A, FORNITORE B WHERE A.ID_FORNITORE = B.ID_FORNITORE AND ID_PERSONA = " & sIDPers
  'Response.Write sSQL 
'PL-SQL * T-SQL  
OPTIMIZESQLSSQL = TransformPLSQLToTSQL (OPTIMIZESQLSSQL) 
   set  rsBonus= CC.Execute(OptimizeSQL(sSQL))
   if not rsBonus.eof then  %> 
	 <td class=tblsfondo width=51%><span class=tbltext>&nbsp;<%=ucase(rsBonus("RAG_SOC"))%></span></td>
     <td class=tblsfondo width=60%><span class=tbltext>&nbsp;<%=ConvDateToString(rsBonus("DT_TMST"))%></span></td></tr>
<%	 rsBonus.close%>
  
<%	else%>
		<td class=tblsfondo><span class=tbltext>&nbsp;NESSUNO </span></td>
		<td class=tblsfondo>&nbsp;</td></tr></table>
<%	end if%>
<%  set rsBonus=nothing%>  


<table width=630 border=0 cellspacing=2 cellpadding=1>
  <tr height=23>
	<td class=tbllabel1 width=51%><span class=tblwhite>&nbsp;Bonus ritirato presso </span></td>
     <td class=tbllabel1 width=60%><span class=tblwhite>&nbsp;In data </span></td></tr>
  <% dim rsBRit
  
  sSQL="SELECT DT_CONSEGNA , RAG_SOC  FROM BONUS A, FORNITORE B WHERE A.ID_FORNITORE = B.ID_FORNITORE AND ID_PERSONA = " & sIDPers
  'Response.Write sSQL 
'PL-SQL * T-SQL  
OPTIMIZESQLSSQL = TransformPLSQLToTSQL (OPTIMIZESQLSSQL) 
   set  rsBRit= CC.Execute(OptimizeSQL(sSQL))
   if not rsBRit.eof then  %> 
	 <td class=tblsfondo><span class=tbltext>&nbsp;<%=ucase(rsBRit("RAG_SOC"))%></span></td>
     <td class=tblsfondo><span class=tbltext>&nbsp;<%=ConvDateToString(rsBRit("DT_CONSEGNA"))%></span></td></tr>
<%	 rsBRit.close%>
  
<%	else%>
		<td class=tblsfondo><span class=tbltext>&nbsp;NESSUNO</span></td>
		<td class=tblsfondo>&nbsp;</td></tr></table>
<%	end if%>
<%  set rsBRit=nothing%> 



<table width=630 border=0 cellspacing=2 cellpadding=1>
<tr height=23>
	<td class=tbllabel1 width=51%>&nbsp;Formazione a distanza</td>
	<td class=tbllabel1 width=30%>&nbsp;Dal</td>
	<td class=tbllabel1 width=30%>&nbsp;Al</td>
	
</tr>
<% dim rsFDist

FormPresOFad("N")

'Response.Write sSQL
'PL-SQL * T-SQL  
OPTIMIZESQLSSQL = TransformPLSQLToTSQL (OPTIMIZESQLSSQL) 
set rsFDist= CC.Execute(OptimizeSQL(sSQL))
    
if not rsFDist.eof then
  
	while not rsFDist.eof%> 
	<tr height=23>
		<td class=tblsfondo ><span class=tbltext>&nbsp;<%=ucase(rsFDist("descrizione"))%> </span></td>
		<td class=tblsfondo ><span class=tbltext>&nbsp;<%=ConvDateToString(rsFDist("dt_inisess"))%></span></td>
	    <td class=tblsfondo ><span class=tbltext>&nbsp;<%=ConvDateToString(rsFDist("dt_finsess"))%></span></td> 
	    
	    
	<% rsFDist.movenext
    wend 
	rsFDist.close
else    
%><td class=tblsfondo width=20% colspan=4><span class=tbltext>&nbsp;NESSUNA</span></td>
<%
end if
set rsFDist=nothing%>      
 </tr>
</table>



<table width=630 border=0 cellspacing=2 cellpadding=1>
<tr height=23>
	<td class=tbllabel1 width=51%>&nbsp;Test eseguiti</td>
	<td class=tbllabel1 width=30%>&nbsp;Data</td>
	<td class=tbllabel1 width=30% colspan=2>&nbsp;Esito</td>
</tr>
<% dim rsTest 
   dim swTrovato
   swTrovato=false	
 
sSQL = 	" SELECT Q.TIT_QUESTIONARIO, Q.ID_QUESTIONARIO," &_
		" DECODE(RQ.FL_ESEGUITO, 'S', 'POSITIVO', 'N', 'NEGATIVO') AS ESITO," &_
		" RQ.NUM_TENTATIVI, RQ.DT_ESEC, RQ.NUM_PUNTI" &_
		" FROM QUESTIONARIO Q, RISULT_QUEST RQ" &_
		" WHERE Q.ID_QUESTIONARIO<>100 AND Q.ID_QUESTIONARIO = RQ.ID_QUESTIONARIO AND" &_
		" RQ.ID_PERSONA = " & sIDPers &_
		" AND NUM_TENTATIVI>0"
		
'PL-SQL * T-SQL  
OPTIMIZESQLSSQL = TransformPLSQLToTSQL (OPTIMIZESQLSSQL) 
set rsTest= CC.Execute(OptimizeSQL(sSQL))
do while not rsTest.eof  
	swtrovato=true%>
	<tr height=23>
		<td class=tblsfondo width=51%><span class=tbltext>&nbsp;<%=ucase(rsTest("TIT_QUESTIONARIO"))%></span></td>
		<td class=tblsfondo width=30%><span class=tbltext>&nbsp;<%=ConvDateToString(rsTest("DT_ESEC"))%></span>
		<td class=tblsfondo width=30% colspan=2>
			<span class=tbltext3>&nbsp;<%=rsTest("ESITO")%></span>
		</td>
	</tr><%
    rsTest.movenext
loop
if not swTrovato then%>    
	<td class=tblsfondo width=20% colspan=4><span class=tbltext>&nbsp;NESSUNO</span></td>
<%end if
rsTest.close
set rsTest = nothing%>     
<SCRIPT Language="Javascript">
	function printit(){  
	if (NS) {
    	window.print() ;  
	} else {
    	var WebBrowser = '<OBJECT ID="WebBrowser1" WIDTH=0 HEIGHT=0 CLASSID="CLSID:8856F961-340A-11D0-A96B-00C04FD705A2"></OBJECT>';
		document.body.insertAdjacentHTML('beforeEnd', WebBrowser);
    	WebBrowser1.ExecWB(6, 2);
  		WebBrowser1.outerHTML = "";  
			}
	}
</script>

<SCRIPT Language="Javascript">  
var NS = (navigator.appName == "Netscape");
var VERSION = parseInt(navigator.appVersion);
//if (VERSION > 3) {
   //document.write('');        
//}
</script>

<table width=630 border=0 cellspacing=2 cellpadding=1>
 <tr>
  <td align=center>
	<input type="submit" name="Submit2" value="stampa" class="testi10" onclick="printit()">
	</input>
   </td>
  </tr>
 </table>  	  
<br>
<table width=630 border=0 cellspacing=2 cellpadding=1>
	<tr><td align=right class=tbltext><a href="javascript:history.back()">Torna Indietro</a></td><tr>
</table>
<br>
<!--#include file ="../../include/closeconn.asp"-->
<!--#include file = "../../include/Fine.asp"-->
</body>
</html>

