<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<html>
<head>
<title>Gestione Progetti</title>
<link REL="STYLESHEET" TYPE="text/css" HREF="../../fogliostile.css">

<script language="Javascript">
<!--#include Virtual = "/Include/help.inc"-->
<!--#include Virtual = "/Include/ControlString.inc"-->

function ControllaDati(myForm) 
{
	myForm.txtDescr.value=TRIM(myForm.txtDescr.value)
	if (myForm.txtDescr.value == "") 
	{
		alert ("Il campo Descrizione � obbligatorio")
		myForm.txtDescr.focus()
		return false
	}
	
	myForm.txtProgr.value=TRIM(myForm.txtProgr.value)
	if (myForm.txtProgr.value == "") 
	{
		alert ("Il campo Progressivo Sessione � obbligatorio")
		myForm.txtProgr.focus()
		return false
	}
	
	if (isNaN(myForm.txtProgr.value))
	{
		alert ("Il campo Progressivo Sessione deve essere numerico")
		myForm.txtProgr.focus()
		return false
	}
	
	if ((myForm.txtControllo.value == "SI") && (myForm.txtProgr.value == "0") ){
	    alert ("la Sessione zero � gia' stata definita")
		myForm.txtProgr.focus()
		return false
	}
		
	myForm.txtOre.value=TRIM(myForm.txtOre.value)
	    if (myForm.txtProgr.value == "0"){
	       if (myForm.txtOre.value != "") 
	           {
		        alert ("Il campo Ore Sessione deve essere vuoto in caso di progressivo uguale a zero")
		        myForm.txtOre.focus()
		        return false
	       }
         }
         else {
	
	       if (myForm.txtOre.value == "") 
	           {
		        alert ("Il campo Ore Sessione � obbligatorio")
		        myForm.txtOre.focus()
		        return false
	       }
	
	       if (isNaN(myForm.txtOre.value))
	           {
		        alert ("Il campo Ore Sessione deve essere numerico")
		        myForm.txtOre.focus()
		        return false
	       }
	        //inizio 20/10/03
	        if (myForm.txtOre.value == 0) 
	           {
		        alert ("Il campo Ore Sessione deve essere maggiore di zero")
		        myForm.txtOre.focus()
		        return false
	       }
	       //fine 20/10/2003
	    }
	return true
}

</script>
</head>

<%
'-------------------'
Sub Inizio()%>
<body>
	<table border="0" width="520" cellspacing="0" cellpadding="0" height="81">
	   <tr>
	     <td width="500" background="<%=Session("Progetto")%>/images/titoli/strumenti2b.gif" height="81" valign="bottom" align="right">
	       <table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
	         <tr>
	           <td width="100%" valign="top" align="right"><b class="tbltext1a">Gestione Progetti</b></td>
	         </tr>
	       </table>
	     </td>
	   </tr>
	</table>

	<br>
	<table cellpadding="0" cellspacing="0" width="500" border="0">
		<tr height="18">
			<td class="sfondomenu" height="18" width="67%">
			<span class="tbltext0"><b>&nbsp;MODIFICA PERCORSO PROGETTUALE</b></span></td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
			<td valign="middle" align="right" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1">(*) campi obbligatori</td>
		</tr>
		<tr width="371" class="SFONDOCOMM">
			<td colspan="3">
				Modifica delle Fasi attuative del Bando.
				<br>Per modificare il dettaglio della Fase attuativa 
				selezionata rettificare i dati della scheda sottostante 
				e premere <b>Invia</b>.
				<br>Se esistono domande di iscrizione al Bando i campi saranno 
				in sola lettura e il tasto <b>Invia</b> non sar� visibile.
				<a href="Javascript:Show_Help('/Pgm/help/Gestprogetti/alfabeta/PercProj/PPR_ModFaseBando')">
				<img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
			</td>
		</tr>
		<tr height="2">
			<td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>
	<br><br>
<%
End Sub

'-------------------'
Sub RicDettaglio()
	sSQL =	"SELECT DESC_FASE, PRG_FASE, COD_TFASE, DT_TMST, NUM_ORESESS FROM FASE_BANDO" &_
			" WHERE ID_BANDO=" & nBando &_
			" AND COD_SESSIONE='" & sCodSess & "'"
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	set rsSess = CC.Execute(sSQL)

	If rsSess.EOF then
%>
		<table cellpadding="0" cellspacing="0" width="500" border="0">
			<tr align="center">
				<td class="tbltext3">
					<b>Non sono state rilevate Sessioni corrispondenti al codice selezionato.</b>
				</td>
			</tr>	
		</table>
		<br>
		<table cellpadding="0" cellspacing="0" width="500" border="0">
			<tr align="center">
				<td><a HREF="Javascript:FrmBack.submit();" onmouseover="javascript:window.status='' ; return true"><img src="<%=session("Progetto")%>/images/indietro.gif" border="0" name="imgPunto2"></a><td>
			</tr>
		</table>
<%
	Else
%>
		<form name="FrmStrumenti" action="PPR_VisStrumenti.asp" method="post">
				<input type="hidden" name="hIdBando" value="<%=nBando%>">
				<input type="hidden" name="hCodSess" value="<%=sCodSess%>">
		</form>
		<table border="0" cellpadding="0" cellspacing="1" width="500">
			<tr>
				<td align="center">
					<a class="textred" HREF="Javascript:FrmStrumenti.submit();" onmouseover="javascript:window.status='' ; return true">
						<b>Associa Strumenti</b>
					</a>
				</td>																		
			</tr>
		</table>
<%		Modify()
	End if
End Sub

'-------------------'
Sub Modify()
	If bDomIscr = false then
		NoModif()
	Else
		SiModif()
	End if
End Sub

'-------------------'
Sub NoModif()
%>
		<br>
		<table border="0" cellpadding="0" cellspacing="1" width="500">
			<tr height="20">
				<td align="left" class="tbltext1" width="35%"><b>Bando</b></td>		
				<td align="left" class="tbltext"><b><%=sDescBando%></b></td>
			</tr>
			<tr height="20">
				<td align="left" class="tbltext1"><b>Codice Sessione</b></td>
				<td align="left" class="tbltext"><b><%=sCodSess%></b></td>
			</tr>
	    	<tr height="20">
				<td align="left" class="tbltext1"><b>Descrizione</b></td>
				<td align="left" class="tbltext"><b><%=rsSess("DESC_FASE")%></b></td>
			</tr>
			<tr height="20">
				<td align="left" class="tbltext1"><b>Progressivo Sessione</b></td>
				<td align="left" class="tbltext"><b><%=rsSess("PRG_FASE")%></b></td>
			</tr>
			<tr height="20">
				<td align="left" class="tbltext1"><b>Ore Sessione</b></td>
				<td align="left" class="tbltext"><b><%=rsSess("NUM_ORESESS")%></b></td>
			</tr>
			<tr height="20">
				<td align="left" class="tbltext1"><b>Tipologia Sessione</b></td>
				<td align="left" class="tbltext">			
<%				Select case rsSess("COD_TFASE")
					case "S" %>
						<b>PRESENZA</b>
<%					case "N" %>
						<b>DISTANZA</b>
<%				End Select%>
				</td>
			</tr>
		</table>
		<br>
		
		<table cellpadding="0" cellspacing="0" width="500" border="0">
			<tr align="center">
				<td><a HREF="Javascript:FrmBack.submit();" onmouseover="javascript:window.status='' ; return true"><img src="<%=session("Progetto")%>/images/indietro.gif" border="0" name="imgPunto2"></a><td>
			</tr>
		</table>
<%
End Sub

'-------------------'
Sub SiModif()
    dim sControllo
    
    sSQL = "SELECT COD_SESSIONE FROM FASE_BANDO " &_
       "WHERE ID_BANDO =" & nBando &_
       " AND PRG_FASE = 0"
  
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
    set rsFase = cc.execute(sSQL)
    if not rsFase.eof then
       sControllo = "SI"
    else
       sControllo = "NO"       
    end if
    set rsFase = nothing  
%>	
	<form name="frmModFaseB" method="post" onsubmit="return ControllaDati(this)" action="PPR_CnfFaseBando.asp">
		<input type="hidden" name="txtAction" value="MOD">
		<input type="hidden" name="txtControllo" value="<%=sControllo%>">
		<input type="hidden" name="txtIdBando" value="<%=nBando%>">					
		<input type="hidden" name="hdnCodSess" value="<%=sCodSess%>">
		<input type="hidden" name="txtTMST" value="<%=rsSess("DT_TMST")%>">
	
		<br>
		<table border="0" cellpadding="0" cellspacing="1" width="500">
			<tr height="20">
				<td align="left" class="tbltext1" width="35%">
					<b>Bando</b>
				</td>		
				<td align="left" class="tbltext"> 
					<b><%=sDescBando%></b>
				</td>
			</tr>
			<tr height="20">
				<td align="left" class="tbltext1">
					<b>Codice Sessione*</b>
				</td>
				<td align="left" class="tbltext"> 
					<b><%=sCodSess%></b>
				</td>
			</tr>
    		<tr class="tbltext1">
				<td align="left">
					<b>Descrizione*</b>
				</td>
				<td>			
					<input type="text" name="txtDescr" maxlength="50" size="50" align="left" class="textblack" style="TEXT-TRANSFORM:uppercase;" value="<%=rsSess("DESC_FASE")%>">			
				</td>
			</tr>
			<tr class="tbltext1">
				<td align="left">
					<b>Progressivo Sessione*</b>
				</td>
				<td>			
					<input type="text" name="txtProgr" maxlength="3" size="12" align="left" class="textblack" style="TEXT-TRANSFORM:uppercase;" value="<%=rsSess("PRG_FASE")%>">			
				</td>
			</tr>
						<tr class="tbltext1">
				<td align="left">
					<b>Ore Sessione*</b>
				</td>
				<td>			
					<input type="text" name="txtOre" maxlength="4" size="12" align="left" class="textblack" style="TEXT-TRANSFORM:uppercase;" value="<%=rsSess("NUM_ORESESS")%>">			
				</td>
			</tr>
			<tr class="tbltext1">
				<td align="left">
					<b>Tipologia Sessione*</b>
				</td>
				<td>			
					<select name="cmbTipoS" class="textblack" ID="cmbTipoS">
	<%				Select case rsSess("COD_TFASE")
						case "S" %>
							<option value="S" selected>Presenza</option>
							<option value="N">Distanza</option>
	<%					case "N" %>
							<option value="S">Presenza</option>
							<option value="N" selected>Distanza</option>
	<%				End Select%>
					</select>			
				</td>
			</tr>
		</table>
		<br><br>
    
		<table cellpadding="0" cellspacing="0" width="500" border="0">
			<tr align="center">
				<td nowrap><input type="image" name="Conferma" value="Registra" src="<%=Session("progetto")%>/images/conferma.gif"></td>
				<td><a HREF="Javascript:FrmBack.submit();" onmouseover="javascript:window.status='' ; return true"><img src="<%=session("Progetto")%>/images/indietro.gif" border="0" name="imgPunto2"></a><td>
		    </tr>
		</table>    
	</form>
<%
End Sub	

'-------------------'
Sub Fine()
	rsSess.Close
	set rsSess=nothing
%>
	<!--#include virtual ="/include/closeconn.asp"-->
	<!--#include virtual = "strutt_coda2.asp"-->
	</body>
	</html>
<%
End Sub

'-------------------------------------------------'
'-------------------' M A I N '-------------------'
%>
	<!-- #include virtual="/strutt_testa2.asp"-->
	<!--#include virtual ="/include/openconn.asp"-->
	<!--#include virtual="/util/portallib.asp"-->
	<!--#include virtual="/include/SelAreaTerrBandi.asp"-->
<%
	if ValidateService(session("idutente"),"PPR_VISFASEBANDO", CC) <> "true" then 
		response.redirect "/util/error_login.asp"
	end if

	dim nBando, sCodSess, bDomIscr
	dim sSQL, rsSess

	nBando= Request.Form("hdnID")
	sDescBando=DecBando(nBando)
	sCodSess= Request.Form("hdnFaseB")
	bDomIscr= cbool(Request.Form ("hDomIscr"))
%>
	<form name="FrmBack" action="PPR_VisFaseBando.asp" method="post">
		<input type="hidden" name="cmbBando" value="<%=nBando%>">
	</form>
<%
	Inizio()
	RicDettaglio()
	Fine()
%>



