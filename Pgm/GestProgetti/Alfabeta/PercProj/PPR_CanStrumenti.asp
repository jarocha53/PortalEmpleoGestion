<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include Virtual="/strutt_testa2.asp"-->
<!--#include virtual = "/include/SysFunction.asp"-->
<!--#include virtual = "/include/OpenConn.asp"-->
<!--#include virtual="/util/dbUtil.asp"-->
<!--#include virtual = "/include/ControlDateVB.asp"-->
<%
'--------------------'
Sub Inizio()
%>
<table border="0" width="500" height="81" cellspacing="0" cellpadding="0">
	<tr>
		<td width="500" height="81" background="<%=Session("Progetto")%>/images/titoli/strumenti2b.gif" valign="bottom" align="right">
			<table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
				<tr>
					<td width="100%" valign="top" align="right">
						<b class="tbltext1a">Gestione Progetti</b>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
	<br>
<table cellpadding="0" cellspacing="0" width="500" border="0">
	<tr height="18">
		<td class="sfondomenu" height="18" width="60%">
			<span class="tbltext0">
				<b>PERCORSO PROGETTUALE</b>
			</span>
		</td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">
		</td>
		<td valign="middle" align="right" width="35%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1">
		</td>
	</tr>
	<tr width="371" class="SFONDOCOMM">
		<td colspan="3">
			Cancellazione dell'Associazione strumenti.
		</td>
	</tr>
	<tr height="2">
		<td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif">
		</td>
	</tr>
</table>
<%
End Sub
'--------------------'

dim sParam, nBando, sFase, sRuolo, sCmbFun
dim sSQL, rs

	nBando=  Request.Form("txtBando")
	sFase=   Request.Form("txtFase")
	sRuolo=  Request.Form("txtRuolo")
	sCmbFun= Request.Form("CmbFun")
	dTimeStamp=	Request.Form("txtTMST")

 	Errore="0"
 	
 	sSql =	"DELETE FROM FASE_STRUMENTO"		&_
			" WHERE ID_BANDO=" & nBando			&_
			" AND COD_SESSIONE='" & sFase & "'" &_
			" AND COD_RUOFU='" & sRuolo & "'"	&_
			" AND IDFUNZIONE=" & sCmbFun    

	chiave=""
	Errore= Esegui(chiave,"FASE_STRUMENTO",session("idutente"),"CAN", sSql, 0, dTimeStamp)

	if Errore <> "0" then
		Inizio()
%>
		<br><br>
		<table border="0" cellspacing="2" cellpadding="1" width="500">
			<tr>
				<td class="tbltext3" align="middle">
					Eliminazione dei dati non effettuabile.
				</td>
			</tr>
			<tr>
				<td class="tbltext3" align="middle">
					<%=Errore%> 
				</td>
			</tr>
			<tr>
				<td align="middle">
					<br><br>
					<a href="JavaScript:history.back()">
						<img src="<%=Session("Progetto")%>/images/Indietro.gif" border="0">
					</a>
				</td>
			</tr>
		</table>
<%
	else
%>
		<form name="frmIndietro" method="post" action="PPR_VisStrumenti.asp">	
			<input type="hidden" name="hIDBando" value="<%=nBando%>">
			<input type="hidden" name="hCodSess" value="<%=sFase%>">
			<input type="hidden" name="cmbRuoFun" value="<%=sRuolo%>">	
		</form>					
		<script>				
			alert("Cancellazione correttamente effettuata");				
			frmIndietro.submit()				
		</script>		
<%
	end if
%>		
<!--#include Virtual="/strutt_Coda2.asp"-->
<!--#include Virtual = "/include/CloseConn.asp"-->	
