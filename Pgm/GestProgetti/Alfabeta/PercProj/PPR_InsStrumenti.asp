<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<script LANGUAGE="Javascript">
	<!--#include Virtual = "/Include/help.inc"-->
	<!--#include Virtual = "/include/ControlString.inc"-->
	
function ControllaDati(val)	
{
	var nSel
	nSel	= FrmIns.CmbFun.selectedIndex
	nFunz	= FrmIns.CmbFun.options[nSel].value

	if (nFunz == "")
	{
		alert("Selezionare una Funzione")
		FrmIns.CmbFun.focus();
		return false;
	}
		if (val==0)
		{
			w=(screen.width-600)/2;	
			h=(screen.height-400)/2;
			fin=window.open("/Include/InfoFun.asp?P=..............................................................................................................................................&idFun=" + nFunz,"","toolbar=0,location=0,directories=0,status=0,menubar=0,scrollbars=yes,resizable=0,copyhistory=0,width=600,height=400,screenX=100,screenY100");	
		}
		else
		{
			FrmIns.txtParam.value=TRIM(FrmIns.txtParam.value)
			FrmIns.txtValida.value=TRIM(FrmIns.txtValida.value)
			return true;
		}
}

function ApriDocum(pagina)
{
	w=(screen.width-600)/2;	
	h=(screen.height-400)/2;
	fin=window.open(pagina,"","toolbar=0,location=0,directories=0,status=0,menubar=0,scrollbars=yes,resizable=0,copyhistory=0,width=600,height=400,screenX=100,screenY100");	
}

</script>

<%
Sub Inizio()%>

<table border="0" width="520" cellspacing="0" cellpadding="0" height="81">
   <tr>
     <td width="500" background="<%=Session("Progetto")%>/images/titoli/strumenti2b.gif" height="81" valign="bottom" align="right">
       <table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
         <tr>
           <td width="100%" valign="top" align="right"><b class="tbltext1a">Gestione Progetti</b></td>
         </tr>
       </table>
     </td>
   </tr>
</table>
<br>
<table cellpadding="0" cellspacing="0" width="500" border="0">
	<tr height="18">
		<td class="sfondomenu" height="18" width="67%">
		<span class="tbltext0"><b>&nbsp;PERCORSO PROGETTUALE</b></span></td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
		<td valign="middle" align="right" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1">(*)Campi obbligatori</td>
	</tr>
	<tr width="371" class="SFONDOCOMM">
		<td colspan="3">
			Inserire le informazioni necessarie per aggiungere il dettaglio di uno Strumento/Servizio.<br>
			Per maggiori informazioni sui valori da inserire cliccare sulle icone di documentazione poste accanto
			ai campi Parametri e Validazione.
			<a href="Javascript:Show_Help('/Pgm/help/Gestprogetti/alfabeta/PercProj/PPR_InsStrumenti')">
			<img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0">
			</a>
		</td>
	</tr>
	<tr height="2">
		<td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif">
		</td>
	</tr>
</table>
<%
End Sub

'-------------------'
Sub Centrale()
	
	sLink= "/Pgm/Doc/Util/MenuFun.htm"
%>
<form name="FrmIns" method="post" action="PPR_CnfStrumenti.asp" onsubmit="javascript: return ControllaDati(1);">
	<input type="hidden" name="txtAction" value="INS">
	<input type="hidden" name="txtBando" value="<%=nBando%>">
	<input type="hidden" name="txtFase" value="<%=sFase%>">
	<input type="hidden" name="txtRuolo" value="<%=sRuolo%>">

	<table width="500" border="0" cellspacing="2" cellpadding="1">
		<tr height="20">
			<td class="tbltext1" width="20%">
				<b>Bando</b>
			</td>
			<td class="tbltext" colspan="2">
				<b><%=DecBando(nBando)%></b>
			</td>
		</tr>
		<tr height="20">
			<td class="tbltext1">
				<b>Sessione</b>
			</td>
			<td class="tbltext" colspan="2">
				<b><%=sFase%> - <%=DecFasebando(nBando,sFase)%></b>
			</td>
		</tr>
		<tr height="20">
			<td class="tbltext1">
				<b>Ruolo</b>
			</td>
			<td class="tbltext" colspan="2">
				<b><%=DecRuolo(sRuolo)%></b>
			</td>
		</tr>
		<tr height="20">
			<td class="tbltext1">
				<b>Funzioni*</b>
			</td>
			<td class="tbltext1" colspan="2">
		<%
				sSql =	"Select idfunzione, desfunzione from funzione" &_
						" where idfunzione Not in (" &_
						" Select idfunzione from Fase_strumento" &_
						" where id_bando=" & nBando &_
						" and cod_sessione='" & sFase &_
						"' and cod_ruofu='" & sRuolo & "')" &_
						" order by desfunzione "
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
				set rs= cc.Execute (sSql)

				Response.Write "<SELECT class='textblack' name='CmbFun'>"
				Response.Write "<option value=''></option>"
				If Not Rs.eof then
					DO
						Response.Write "<option value=" & Rs("IDFUNZIONE") & ">" & Rs("DESFUNZIONE") & "</option>"
						Rs.MOVENEXT
					LOOP UNTIL Rs.EOF
				End If
			Rs.close
			Set Rs = Nothing
			Response.Write "</SELECT>"
		%>
			</td>
		</tr>
		<tr height="20">
			<td class="tbltext1">
				<b>Parametri</b>
			</td>
			<td class="tbltext">
				<input type="text" style="TEXT-TRANSFORM:uppercase;" class="textblack" name="txtParam" maxlength="50" size="50">
			</td>
			<td align="left" width="25%"><a onmouseover="javascript:window.status=' '; return true" onclick="javascript:ControllaDati(0);"><img src="<%=Session("Progetto")%>/images/scheda.gif" border="0"></a></td>
		</tr>
		<tr height="20">
			<td class="tbltext1">
				<b>Validazione</b>
			</td>
			<td class="tbltext">
				<input type="text" style="TEXT-TRANSFORM:uppercase;" class="textblack" name="txtValida" maxlength="50" size="50">
			</td>
			<td align="left" width="25%"><a onmouseover="javascript:window.status=' '; return true" onclick="javascript:ApriDocum('<%=sLink%>');"><img src="<%=Session("Progetto")%>/images/scheda.gif" border="0"></a></td>
		</tr>
	</table>
	<br>
	
	<table width="500" border="0">
		<tr align="center">
			<td nowrap><a href="javascript:history.go(-1)" onmouseover="javascript:window.status=' '; return true"><img src="<%=session("Progetto")%>/images/indietro.gif" border="0" name="imgPunto2"></a><td>
			<td nowrap><input type="image" name="Conferma" src="<%=Session("progetto")%>/images/conferma.gif"></td value="Registra">
		</tr>
	</table>  
</form>	
<%
End sub

'-------------------'
Sub Fine()
%>
	<!--#include virtual ="/include/closeconn.asp"-->
	<!--#include virtual = "/strutt_coda2.asp"-->
<%
End sub 


'-------------------------------------------------' 
'-------------------' M A I N '-------------------' 
%>
	<!--#include virtual="/strutt_testa2.asp"-->
	<!--#include virtual ="/include/openconn.asp"-->
	<!--#include virtual="/util/portallib.asp"-->
	<!--#include virtual ="/include/SelAreaTerrBandi.asp"-->
	<!--#include virtual ="/include/RuoloFunzionale.asp"-->
<%
	if ValidateService(session("idutente"),"PPR_VISFASEBANDO", CC) <> "true" then 
		response.redirect "/util/error_login.asp"
	end if

	dim nBando, sFase, sRuolo

	nBando=		Request.Form("hBando")
	sFase=		Request.Form("hSessione")
	sRuolo=		Request.Form("hRuolo")
	
	Inizio()
	Centrale()
	Fine()
%>
