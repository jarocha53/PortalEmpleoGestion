<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #include virtual="/strutt_testa2.asp"-->
<!--#include virtual = "/util/dbutil.asp"-->
<!--#include virtual = "/include/ControlDateVB.asp"-->
<!--#include virtual = "/include/SysFunction.asp"-->
<!--#include virtual = "/include/openconn.asp"-->
<%
	dim nBando, nSede, sProv, bNoModify

	nBando=		Request.Form("hdnIdBando")
	sProv=		Request.Form("hdnProv")
	nSede=		Request.Form("hdnIdSede")
	bNoModify=	NoModifica(nBando,nSede)
%>
	<form name="FrmBack" action="BAN_ModPostiSede.asp" method="post">
		<input type="hidden" name="hIdBando" value="<%=nBando%>">
		<input type="hidden" name="cmbProv" value="<%=sProv%>">
	</form>
<% 
	if bNoModify = true then
		Inizio()
%>		
		<table border="0" cellspacing="1" cellpadding="1" width="500">
			<tr align="middle">
				<td class="tbltext3">
					Modifica non effettuabile.<br>
					Risulta stilata una graduatoria definitiva.
				</td>
			</tr>
		</table>
		<br><br>
		
		<table border="0" cellpadding="0" cellspacing="1" width="500">
			<tr>
				<td align="middle" colspan="2"><b>
					<a HREF="javascript:FrmBack.submit()"><img SRC="<%=Session("Progetto")%>/images/indietro.gif" border="0">
				</td>
			</tr>
		</table>
<%
	else
		Modifica()
	end if	

'----------------F U N C T I O N -----------------'
'TRUE => non si puo' modificare sede/posti (esiste graduatoria definitiva).
'FALSE=> si puo' modificare sede/posti (non esiste graduatoria definitiva).
Function NoModifica(nIdB, nIdS)
	
	dim rsVer, sSQL
	
	sSQL=	"SELECT NVL(status_grad,0) StatusGrad"	&_
			" FROM BANDO_POSTI"			&_
			" WHERE id_bando= " & nIdB	&_
			" AND id_sede= " & nIdS			
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	set rsVer = CC.Execute(sSQL)
	
	IF NOT rsVer.eof THEN
		If cint(rsVer("StatusGrad")) = 1 then
			NoModifica = true
		Else
			NoModifica = false
		End if
	END IF
	rsVer.close
	set rsVer = nothing
		
End Function

'-------------------'
Sub Inizio() %>
	<table border="0" width="520" cellspacing="0" cellpadding="0" height="81">
		<tr>
		  <td width="500" background="<%=Session("Progetto")%>/images/titoli/strumenti2b.gif" height="81" valign="bottom" align="right">
		    <table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
		      <tr>
		        <td width="100%" valign="top" align="right"><b class="tbltext1a">Gestione Progetti</b></td>
		      </tr>
		    </table>
		  </td>
		</tr>
	</table>
	<br>
	<table cellpadding="0" cellspacing="0" width="500" border="0">
		<tr height="18">
			<td class="sfondomenu" height="18" width="67%">
			<span class="tbltext0"><b>&nbsp;CONFERMA MODIFICHE DISPONIBILITA' POSTI / SEDE</b></span></td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
			<td valign="middle" align="right" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1"></td>
		</tr>
		<tr width="371" class="SFONDOCOMM">
			<td colspan="3">
				Conferma della modifica al numero dei posti per la sede selezionata.
			</td>
		</tr>
		<tr height="2">
			<td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>
	<br><br>
<%	
End Sub

'-------------------'
Sub Modifica()
	dim sSQL, rs
	dim nPosti, dTmst
	
	nPosti		 = Request.Form("txtPosti")
	dTmst		 = Request.Form("hdnTMST")

	Errore = "0"
	sSQL =	"UPDATE BANDO_POSTI SET"				&_
			" NUM_POSTI=" & nPosti & ","			&_
			" DT_TMST="	& ConvDateToDB(now())		&_
			" WHERE ID_BANDO=" & nBando				&_
			" AND ID_SEDE=" & nSede

	chiave=""
	Errore= Esegui(chiave,"BANDO_POSTI",session("idutente"),"MOD", sSQL, 0, dTmst)
		
	if Errore = "0" then
%>
		<script>
			alert("Modifica correttamente effettuata.");
			FrmBack.submit()
		</script>
<%		
	else
		Inizio()
	%>
		<table border="0" cellspacing="1" cellpadding="1" width="500">
			<tr align="middle">
				<td class="tbltext3">
					Modifica non effettuabile.
				</td>
			</tr>
			<tr align="middle">
				<td class="tbltext3">
					<%Response.Write(Errore)%> 
				</td>
			</tr>
		</table>
		<br>				
		<table border="0" cellpadding="0" cellspacing="1" width="500">
			<tr>
				<td align="middle" colspan="2"><b>
				 <a HREF="javascript:history.back()"><img SRC="<%=Session("Progetto")%>/images/indietro.gif" border="0">
				</td>
			</tr>
		</table>
<%
	End if
End Sub	

'-------------------'
%>
<!--#include virtual ="/include/CloseConn.asp"-->
<!--#include virtual = "/strutt_coda2.asp"-->
