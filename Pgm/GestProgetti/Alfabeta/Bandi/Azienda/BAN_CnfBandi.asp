<%'Option Explicit
   Response.ExpiresAbsolute = Now() - 1 
   Response.AddHeader "pragma","no-cache"
   Response.AddHeader "cache-control","private"
   Response.CacheControl = "no-cache"
%>
<!-- #include virtual="/strutt_testa2.asp"-->
<!--#include virtual ="/include/openconn.asp"-->
<!--#include virtual="/util/portallib.asp"-->
<!--#include virtual = "/include/SysFunction.asp"-->
<!--#include virtual = "/include/ControlDateVB.asp"-->
<%
if ValidateService(session("idutente"),"BAN_VISBANDI", CC) <> "true" then 
	response.redirect "/util/error_login.asp"
end if
%>
<!-- ************** Javascript Inizio ************ -->

<script LANGUAGE="javascript">
<!--#include Virtual = "/Include/help.inc"-->

</script>

<!-- ************** Javascript Fine ************ -->

<%
dim sModo, Errore

sModo = Request("MOD")

dim sIDBando
dim sDataPub
dim sDataInizio
dim sDataFine
dim sDataRis
dim sDescBando
dim sCodBando
dim sAreaTerr
dim sSQL

sCodBando = Replace(Ucase(trim(Request.Form ("txtCodBando"))), "'", "''")
sDescBando = Replace(Ucase(trim(Request.Form ("txtDescBando"))), "'", "''")
sDataPub = Request.Form ("txtDataPub")
sDataInizio = Request.Form ("txtDataIniAcq")
sDataFine = Request.Form ("txtDataFineAcq")
sDataRis = Request.Form ("txtDataPubRis")
sIdBando = Request.Form ("txtIdBando")
sTipo_Bando="A"

' Trasformo le Strighe contenenti le date in formato Data Valido
sDataPub	= ConvStringToDate(sDataPub)
if sDataInizio <> "" then
   sDataInizio = ConvStringToDate(sDataInizio)
else
   sDataInizio=""
end if
if sDataFine <> "" then
   sDataFine	= ConvStringToDate(sDataFine)
else
   sDataFine=""
end if
sDataRis	= ConvStringToDate(sDataRis)

Inizio()		
select case sModo
	case 1 
	Inserimento()

	case 2
	sTimeTemp = Request.Form ("txtTimeTemp")
	
	Modifica()
end select

Fine()%>

'-------------------------------------------------------------------------------------------------------------------------------
<% sub Inizio()
%>
	<table border="0" width="520" cellspacing="0" cellpadding="0" height="81">
	   <tr>
	     <td width="500" background="<%=Session("Progetto")%>/images/titoli/strumenti2b.gif" height="81" valign="bottom" align="right">
	       <table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
	         <tr>
	           <td width="100%" valign="top" align="right"><b class="tbltext1a">Gestione Progetti</span></b></td>
	         </tr>
	       </table>
	     </td>
	   </tr>
	</table>
	<br>
	<br>
<%
end sub
'-------------------------------------------------------------------------------------------------------------------------------

Sub Msgetto(Msg)
%>
	<br>
	<table align="center">
		<tr>
			<td class="tbltext3"><b><%=Msg%></b></td>
		</tr>	
	</table>
<%
End Sub

'-------------------------------------------------------------------------------------------------------------------------------

sub TornaIndietro()

	%>
	<form name="frmIndietro" method="post" action="BAN_VisBandi.asp">
		<input type="hidden" name="MOD" value="1">
	</form>  

	<script>
		alert("Operazione correttamente effettuata");
		frmIndietro.submit()
	</script>
	<%

end sub

'-------------------------------------------------------------------------------------------------------------------------------

sub Inserimento()

	'Controllo se esiste gi� in tabella un Bando con lo stesso codice
	dim sContatore
    
    sSQL ="SELECT ID_PROJ FROM PROGETTO WHERE AREA_WEB='" & MID(SESSION("PROGETTO"),2) & "'"
    SET rsProg = CC.execute(sSQL)
    if rsProg.eof then
        set rsProg = nothing
        Msgetto("Errore:Nessun Progetto associato")
        sIdProgetto = ""
    else
        sIdProgetto = rsProg("ID_PROJ")
        set rsProg = nothing
	    sContatore = 0

	    sSQL = "SELECT COUNT(*) AS Count FROM BANDO WHERE COD_BANDO = '" & sCodBando & "'" &_
	           "AND ID_PROJ =" & sIdProgetto
				
	    set rsBando = CC.Execute(sSQL)
		
	    sContatore = cint(rsBando("Count"))
		set rsBando = nothing
		
	    if sContatore = 0 then
     	   sSQL = "INSERT INTO BANDO " & _
		          "(COD_BANDO," &_
	    		  "ID_PROJ," &_
	    		  "DT_PUB,"
	       if sDataInizio <> "" then
		       sSQL = sSQL & "DT_INI_ACQ_DOM,"
		   end if		  
	       if sDataFine <> "" then		  
			   sSQL= sSQL &  "DT_FIN_ACQ_DOM," 
		   end if
		   
	  sSQL=sSQL & "DT_PUB_RIS_SEL," &_
				  "DT_TMST," &_	
				  "DESC_BANDO," &_
				  "TIPO_BANDO" &_
				  ")" & " VALUES " &_
				  "('" & sCodBando  & _
				  "'," & sIdProgetto & _
		   		  "," & ConvDateToDb(sDataPub) 
		   		  if sDataInizio <>"" then
				      sSQL = sSQL & "," & ConvDateToDb(sDataInizio) 
				  end if
				  if sDataFine <> "" then
				      sSQL = sSQL & "," & ConvDateToDb(sDataFine)
				  end if
	  sSQL=sSQL & "," & ConvDateToDb(sDataRis) & _
				  "," & ConvDateToDb(Now()) & _
				  ",'" & sDescBando & "'" & _ 
				  ",'" & sTipo_Bando & "')"
           CC.BeginTrans
           Errore = EseguiNoC(sSQL,CC)  
           
		   if Errore = "0" then
		      sSQL ="INSERT INTO GRUPPO(DESGRUPPO,WEBPATH,COD_RORGA," &_
		             "DT_TMST) VALUES ('ISCRITTI BANDO " & sCodBando &_
		             "','/AZIENDA','AZ'," & ConvDateToDb(Now()) & ")"  
		      
		      Errore = EseguiNoC(sSQL,CC)
		      sIdGrupppo = ""
		      if Errore = "0" then
		      
				sSQL = "SELECT IDGRUPPO FROM GRUPPO WHERE " &_
		                 "DESGRUPPO ='ISCRITTI BANDO " & sCodBando &_
		                 "' AND WEBPATH ='/AZIENDA' AND COD_RORGA= 'AZ'" 
		          
		          set rsGruppo = cc.execute(sSQL)
		          
		          if  not rsGruppo.eof then
		              sIdGruppo = rsGruppo("IDGRUPPO")
		          end if
		          
		          set rsGruppo = nothing
		          
		          sSQL ="UPDATE BANDO SET IDGRUPPO =" & CLNG(sIdGruppo) &_
		                " WHERE COD_BANDO ='" & sCodBando & "'" 
		          
		          Errore = EseguiNoC(sSQL,CC)               
		      end if
'*******************************
					set rsGruppoFun = Server.CreateObject("ADODB.Recordset")
	      
					sSqlFun = " SELECT idgruppo" &_
						      " FROM GRUPPO" &_
						      " WHERE webpath='/AZIENDA'" &_
						      " order by idgruppo"
							           
					rsGruppoFun.Open sSqlFun, CC, 3
				
					nNewGruppo = rsGruppoFun("idgruppo")
					
					rsGruppoFun.Close 
									
					set rsGruppoFun = nothing
					 					
					if CLNG(nNewGruppo) <> CLNG(sIdGruppo) Then
						
						sSql="insert into gruppofun (IDGRUPPO,IDFUNZIONE,NAMEVAR,VALORE) " &_
								"select " &  CLNG(sIdGruppo) & " ,IDFUNZIONE,NAMEVAR,VALORE " &_
									"from gruppofun " &_
									"where idgruppo=" & CLNG(nNewGruppo) 	
						 
						' Response.Write sSql
						Errore = EseguiNoC(sSql,CC)   			
					end if
					
'*************************		      
		      if Errore = "0" then  
		         CC.CommitTrans
		         TornaIndietro()
		      else
					Response.Write Errore
		         CC.RollbackTrans
		         Msgetto("Errore nell'inserimento del Gruppo")
		      end if   
		   else
		      CC.RollbackTrans
			  Msgetto("Errore: inserimento del Bando non effettuabile")
		   end if
	    else
		   Msgetto("Codice bando gia presente")
	    end if
    end if
'    set rsProg = nothing
End Sub
'-------------------------------------------------------------------------------------------------------------------------------

sub Modifica()
     
	sSQL = "UPDATE BANDO SET DT_PUB=" & ConvDateToDb(sDataPub) 
	if sDataInizio <>"" then
	   sSQL=sSQL & ", DT_INI_ACQ_DOM=" & ConvDateToDb(sDataInizio) 
	end if
	if sDataFine <> "" then       
	   sSQL=sSQL &", DT_FIN_ACQ_DOM=" & ConvDateToDb(sDataFine) 
	end if       
	sSQL=sSQL & ", DESC_BANDO='" & sDescBando & _
	            "', DT_PUB_RIS_SEL=" & ConvDateToDb(sDataRis) & _
	            ", DT_TMST=" & ConvDateToDb(Now()) & _
	            " WHERE ID_BANDO = '" & sIDBando & "'"
	
	Errore = Esegui("ID_BANDO", "BANDO", Session("idutente"), "MOD", sSQL, 1, sTimeTemp)
	   
	if Errore = "0" then
	    TornaIndietro()
	else
	   if Errore="<BR>Record aggiornato da altro utente." then
	       cc.RollbackTrans
	   end if
	   Msgetto("Errore in fase di aggionamento dati")
	end if
    
End Sub
'-------------------------------------------------------------------------------------------------------------------------------

sub Fine()
%>
	<br>
	<br>
	<table border="0" cellpadding="0" cellspacing="1" width="500">
		<tr>
			<td align="center">
				<a href="javascript:history.go(-1)">
					<img src="<%=session("Progetto")%>/images/indietro.gif" border="0" name="imgPunto2" onmouseover="javascript:window.status=' '; return true">
				</a>
			</td>
		</tr>
	</table>
	<br>
<!--#include virtual = "/include/closeconn.asp"-->
<!--#include virtual = "strutt_coda2.asp"-->
<%end sub%>
