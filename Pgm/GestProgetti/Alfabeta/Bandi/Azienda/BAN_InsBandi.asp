
<!-- #include virtual="/strutt_testa2.asp"-->
<!--#include virtual ="/include/openconn.asp"-->
<!--#include virtual="/util/portallib.asp"-->
<!--#include virtual = "include/ControlDateVB.asp"-->

<%
if ValidateService(session("idutente"),"BAN_VISBANDI", CC) <> "true" then 
	response.redirect "/util/error_login.asp"
end if
%>
<!-- ************** Javascript Inizio ************ -->
<script LANGUAGE="Javascript">
<!--#include virtual="/Include/help.inc"-->
<!--#include virtual = "/Include/ControlDate.inc"-->
<!--#include virtual = "/Include/ControlNum.inc"-->
<!--#include virtual = "/Include/ControlString.inc"-->  
	
//Funzione per i controlli dei campi da inserire 
	function ControllaDati(frmInsBando){
		
		frmInsBando.txtCodBando.value=TRIM(frmInsBando.txtCodBando.value)
		if (frmInsBando.txtCodBando.value == ""){
			alert("Codice del Bando obbligatorio")
			frmInsBando.txtCodBando.focus() 
			return false
		} 
		//Descrizione del Bando obbligatoria
		frmInsBando.txtDescBando.value = TRIM(frmInsBando.txtDescBando.value);
		if ((frmInsBando.txtDescBando.value == "")){
			alert("Descrizione del Bando obbligatoria")
			frmInsBando.txtDescBando.focus() 
			return false
		}
	
		//Data di Pubblicazione del Bando obbligatoria
		if (frmInsBando.txtDataPub.value == ""){
			alert("Data Pubblicazione del Bando obbligatoria")
			frmInsBando.txtDataPub.focus() 
			return false
		}
		//Data di Pubblicazione del Bando corretta
		sDataPub = frmInsBando.txtDataPub.value
		if (!ValidateInputDate(sDataPub)){
			frmInsBando.txtDataPub.focus() 
			return false
		}
		if (ValidateRangeDate(frmInsBando.txtInizioP.value,frmInsBando.txtDataPub.value)!=true){
			alert("La data di pubblicazione del bando " + frmInsBando.txtDataPub.value + " e' minore della data di inizio validita' del progetto " + frmInsBando.txtInizioP.value)
			frmInsBando.txtDataPub.focus()
			return false
		}	
		
		if (ValidateRangeDate(frmInsBando.txtDataPub.value,frmInsBando.txtFineP.value)==false){
			alert("La data di pubblicazione del bando" + frmInsBando.txtDataPub.value + " e' maggiore della data di fine validita' del progetto " + frmInsBando.txtFineP.value)
			frmInsBando.txtDataPub.focus()
			return false
		}	
		
		frmInsBando.txtIscr.value = TRIM(frmInsBando.txtIscr.value);
		//Data di Inizio Acquisizione della Domanda obbligatoria
		if(frmInsBando.txtIscr.value !="I" ){
		    if (frmInsBando.txtDataIniAcq.value == ""){
			   alert("Data di Inizio Acquisizione della Domanda obbligatoria")
			   frmInsBando.txtDataIniAcq.focus() 
			   return false
		    }
		}
		sDataIni = frmInsBando.txtDataIniAcq.value
		if (frmInsBando.txtDataIniAcq.value != ""){    
		    //Data di Inizio Acquisizione della Domanda corretta
		    if (!ValidateInputDate(sDataIni)){
			   frmInsBando.txtDataIniAcq.focus() 
			   return false
		    }
		    //Data di Inizio Acquisizione della Domanda deve essere superiore
		    // o uguale alla Data di Pubblicazione
		    if (!ValidateRangeDate(sDataPub,sDataIni)){
			   alert("La Data di Inizio Acquisizione della Domanda non puo' essere inferiore alla Data di Pubblicazione del Bando")
			   frmInsBando.txtDataIniAcq.focus()
			   return false
		    }
		}
		sDataFine = frmInsBando.txtDataFineAcq.value
		if(frmInsBando.txtIscr.value !="I" ){
		    //Data di Fine Acquisizione della Domanda obbligatoria
		    if (frmInsBando.txtDataFineAcq.value == ""){
			   alert("Data di Fine Acquisizione della Domanda obbligatoria")
			   frmInsBando.txtDataFineAcq.focus() 
			   return false
		    }
		}  
		
		if (frmInsBando.txtDataFineAcq.value != ""){        
		    //Data di Fine Acquisizione della Domanda corretta
		    if (!ValidateInputDate(sDataFine)){
			   frmInsBando.txtDataFineAcq.focus() 
			   return false
		    }
		}
		
		if ((frmInsBando.txtDataIniAcq.value != "")&& (frmInsBando.txtDataFineAcq.value != "")){  
		    //Data di Fine Acquisizione della Domanda deve essere superiore
		    // o uguale alla Data di Inizio Acquisizione della Domanda
		    if (!ValidateRangeDate(sDataIni,sDataFine)){
			    alert("La Data di Fine Acquisizione della Domanda non puo' essere inferiore alla Data di Inizio Acquisizione della Domanda")
			    frmInsBando.txtDataFineAcq.focus()
			    return false
		    }
		}    
		//Data di Pubblicazione dei Risultati della Selezione obbligatoria
		if (frmInsBando.txtDataPubRis.value == ""){
			alert("Data di Pubblicazione dei Risultati della Selezione obbligatoria")
			frmInsBando.txtDataPubRis.focus() 
			return false
		}
		//Data di Pubblicazione dei Risultati della Selezione obbligatoria
		sDataSel = frmInsBando.txtDataPubRis.value
		if (!ValidateInputDate(sDataSel)){
			frmInsBando.txtDataPubRis.focus() 
			return false
		}
		
		//Data di Pubblicazione dei Risultati della Selezione deve essere superiore
		// o uguale alla Data di Fine Acquisizione della Domanda
		if((frmInsBando.txtIscr.value !="I" )||(frmInsBando.txtDataFineAcq.value != "")){
		   if (ValidateRangeDate(sDataSel,sDataFine)==true){
			   alert("La Data di Pubblicazione dei Risultati della Selezione deve essere superiore alla Data di Fine Acquisizione della Domanda")
			   frmInsBando.txtDataPubRis.focus()
			   return false
		   }
		}   
	return true
	}
</script>
<!-- ************** Javascript Fine   ************ -->

<%

Intestazione()
PreparaPagina()
fine()

sub Intestazione() %>
<table border="0" width="520" cellspacing="0" cellpadding="0" height="81">
   <tr>
     <td width="500" background="<%=Session("Progetto")%>/images/titoli/strumenti2b.gif" height="81" valign="bottom" align="right">
       <table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
         <tr>
           <td width="100%" valign="top" align="right"><b class="tbltext1a">Gestione Progetti</span></b></td>
         </tr>
       </table>
     </td>
   </tr>
</table>
<br>
<table cellpadding="0" cellspacing="0" width="500" border="0">
		<tr height="18">
			<td class="sfondomenu" height="18" width="67%">
			<span class="tbltext0"><b>&nbsp;GESTIONE BANDI</b></span></td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
			<td valign="middle" align="right" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1">(*) campi obbligatori</td>
		</tr>
		<tr width="371" class="SFONDOCOMM">
			<td colspan="3">
			Inserimento dati. <br>
			Premere <b>Invia</b> per salvare. 
			<a href="Javascript:Show_Help('/Pgm/help/GestProgetti/Alfabeta/Bandi/BAN_InsBandi')" onmouseover="javascript:window.status=' '; return true"><img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a></td>
		</tr>
		<tr height="2">
			<td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>
	<br>		
<%end sub%>

<%sub PreparaPagina()

sSQL ="SELECT IND_ISCR,DT_INI_PROJ,DT_FIN_PROJ FROM PROGETTO WHERE AREA_WEB='" & MID(SESSION("PROGETTO"),2) & "'"
SET rsIscr = CC.execute(sSQL)
if not rsIscr.eof then
    sIscr = rsIscr("IND_ISCR") 
    dtInizio = ConvDateToString(rsIscr("DT_INI_PROJ"))
    dtFine = ConvDateToString(rsIscr("DT_FIN_PROJ"))  
end if

 %>
<form method="post" name="frmInsBando" onsubmit="return ControllaDati(this)" action="BAN_CnfBandi.asp">
	<input type="hidden" name="txtIscr" value="<%=sIscr%>">
	<input type="hidden" name="txtInizioP" value="<%=dtInizio%>">
	<input type="hidden" name="txtFineP" value="<%=dtFine%>">
	<input type="hidden" name="MOD" value="1"> 
	<table border="0" cellpadding="2" cellspacing="0" width="500"> 
		<tr>
		   <td align="left" width="250" class="tbltext1">
				<b>Codice Bando*&nbsp;</b>
		   </td>
		   <td align="left" width="250">
				<input type="text" style="TEXT-TRANSFORM:uppercase;" class="textblack" size="13" maxlength="3" name="txtCodBando">
		   </td>
		</tr>
		<tr>
		   <td align="left" class="tbltext1">
		        <b>Descrizione del Bando*</b>
		   </td>
		   <td>     
		        <input type="text" style="TEXT-TRANSFORM:uppercase;" class="textblack" size="45" maxlength="50" name="txtDescBando">
		   </td>
		</tr>
		<tr>
		   <td align="left" nowrap class="tbltext1">
		         <b>Data della Pubblicazione<br> del Bando*</b> (gg/mm/aaaa) 
		   </td>
		   <td align="left">
		        <input type="text" style="TEXT-TRANSFORM:uppercase;" class="textblack" size="13" maxlength="10" name="txtDataPub"> 
		   </td> 
		</tr> 
		<tr> 
		   <td align="left" nowrap class="tbltext1"> 
		       <b>Data di Inizio Acquisizione<br> della Domanda*</b> (gg/mm/aaaa)</b> 
		   </td> 
		  <td align="left"> 
		      <input type="text" style="TEXT-TRANSFORM:uppercase;" class="textblack" size="13" maxlength="10" name="txtDataIniAcq"> 
		  </td> 
		</tr> 
		<tr> 
		   <td align="left" nowrap class="tbltext1"> 
		          <b>Data di Fine Acquisizione<br> della Domanda*</b> (gg/mm/aaaa) 
		   </td> 
		   <td align="left"> 
		          <input type="text" style="TEXT-TRANSFORM:uppercase;" class="textblack" size="13" maxlength="10" name="txtDataFineAcq"> 
		   </td> 
		</tr> 
		<tr> 
		   <td align="left" nowrap class="tbltext1"> 
		       <b>Data della Pubblicazione dei<br> Risultati della Selezione*</b> (gg/mm/aaaa)
		   </td> 
		   <td align="left"> 
		       <input type="text" style="TEXT-TRANSFORM:uppercase;" class="textblack" size="13" maxlength="10" name="txtDataPubRis"> 
		   </td> 
		</tr> 
	</table> 
	<br>
	<br>
	<%'impostazione dei comandi%>
	<table cellpadding="0" cellspacing="0" width="300" border="0">	
	  <tr align="center">
		<td nowrap><input type="image" name="Conferma" src="<%=Session("progetto")%>/images/conferma.gif"></td value="Registra">
		<td nowrap><a href="javascript:history.go(-1)"><img src="<%=session("Progetto")%>/images/indietro.gif" border="0" name="imgPunto2" onmouseover="javascript:window.status=' '; return true"></a></td>
	  </tr>
	</table>
</form> 
<%end sub %>

<%sub fine()%>
<!--#include virtual ="/include/closeconn.asp"-->
<!--#include virtual = "strutt_coda2.asp"-->
<%end sub %>