<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #include virtual="/strutt_testa2.asp"-->
<!--#include virtual ="/include/openconn.asp"-->
<!--#include virtual="/util/portallib.asp"-->
<!--#include virtual = "/include/ControlDateVB.asp"-->
<!--#include virtual = "include/SelAreaTerrBandi.asp"-->

<%
if ValidateService(session("idutente"),"BAN_VISPOSTI", CC) <> "true" then 
	response.redirect "/util/error_login.asp"
end if
%>

<!-- ************** Javascript inizio ************ -->
<script LANGUAGE="Javascript">

<!--#include Virtual = "/Include/help.inc"-->
<!--#include Virtual = "/Include/ControlDate.inc"-->
	
function ChiamaBandi(sBando,sCodTimpr,nNumPosti)
{

var	NomePag
	if (sCodTimpr == 00 )
	{
		document.frmDettBandi.hIdBando.value = sBando;
		document.frmDettBandi.hNumPosti.value = nNumPosti;
		document.frmDettBandi.action = "BAN_InsPosti.asp"; 
		document.frmDettBandi.submit();    
	}
	else{
	    document.frmDettBandi.hIdBando.value = sBando; 
	    document.frmDettBandi.action = "BAN_ModPostiSede.asp";   
		document.frmDettBandi.submit(); 
		}
}	
</script>
<!-- ******************  Javascript Fine *********** -->
<%
	Inizio()
	ImpostaTab()    
	Fine()
'-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
sub Inizio()
%>
	<table border="0" width="520" cellspacing="0" cellpadding="0" height="81">
	<tr>
	  <td width="500" background="<%=Session("Progetto")%>/images/titoli/strumenti2b.gif" height="81" valign="bottom" align="right">
	    <table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
	      <tr>
	        <td width="100%" valign="top" align="right"><b class="tbltext1a">Gestione Progetti</span></b></td>
	      </tr>
	    </table>
	  </td>
	</tr>
	</table>
	<br>
	<table cellpadding="0" cellspacing="0" width="500" border="0">
	<tr height="18">
		<td class="sfondomenu" height="18" width="67%">
		<span class="tbltext0"><b>&nbsp;DISPONIBILITA' POSTI</b></span></td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
		<td valign="middle" align="right" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1"></td>
	</tr>
	<tr width="371" class="SFONDOCOMM">
		<td colspan="3">
			In questa maschera verr� indicata la disponibilita' dei posti ai bandi.
			<a href="Javascript:Show_Help('/Pgm/help/GestProgetti/Alfabeta/Bandi/BAN_VisPosti')" onmouseover="javascript:window.status=' '; return true">
			<img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
		</td>
	</tr>
	<tr height="2">
		<td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif">
		</td>
	</tr>
	</table>
	<br>
<%
end sub
'-------------------------------------------------------------------
 sub ImpostaTab()
     dim sBando, sDescBando, nNumPosti
%>  
   	<br>
<%
	'Accesso alla tabella BANDO per il prelevamento dei dati relativi alla descrizione bando

	sSQL ="SELECT B.ID_BANDO, B.DESC_BANDO,P.COD_TIMPR, sum(BP.NUM_POSTI) TOTPOSTI" & _
	      " FROM BANDO B, BANDO_POSTI BP, PROGETTO P " &_
	      "WHERE B.ID_PROJ= P.ID_PROJ AND B.ID_BANDO = BP.ID_BANDO(+) " &_
	      "AND AREA_WEB='" & MID(Session("Progetto"),2) & "'" &_
	      " group by B.ID_BANDO, B.DESC_BANDO,P.COD_TIMPR "
		
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	set rsBando = CC.Execute(sSQL)
if rsBando.EOF then
%>		
	   <table align="center">
			<tr><td class="tbltext3"><b>Non ci sono bandi selezionabili.</b></td></tr>	
	   </table>
<%	
else
	sCodTimpr = rsBando("COD_TIMPR")
%>      <form method="POST" name="frmDettBandi">
		<input type="hidden" name="hIdBando" value>
		<input type="hidden" name="hNumPosti" value>
       	<table border="0" cellpadding="1" cellspacing="2" width="500">
		<tr class="sfondocomm">
			<td align="center" width="80%">
				<b>Descrizione Bando</b>
			</td>
			<td align="center" width="20%">
				<b>Numero Posti</b>
			</td>
		</tr>
<%
		do while not rsBando.EOF
			sBando = rsBando("ID_BANDO")

			if rsBando("DESC_BANDO") <> "" then
				sDescBando = trim(rsBando("DESC_BANDO"))
			else
				sDescBando = ""
			end if
			
			if rsBando("TOTPOSTI") <> "" then
				nNumPosti = rsBando("TOTPOSTI")
			else
				nNumPosti = 0
			end if
			%>
			<tr class="tblsfondo">
			<%
			if sCodTimpr = 00 then	
				bNoModify = NoModifica(sBando)		
		
				if bNoModify = false then
				%>
					<td class="tbldett">
						<a class="tblAgg" HREF="Javascript:ChiamaBandi(<%=sBando%>,'<%=sCodTimpr%>',<%=nNumPosti%>)" onmouseover="javascript:window.status=' '; return true">
							<%=sDescBando%>
						</a>
					</td>
				<%else
			    %>	<td class="tbldett">
						<%=sDescBando%>			
					</td>
				<%end if
			else
			%>
				<td class="tbldett">
					<a class="tblAgg" HREF="Javascript:ChiamaBandi(<%=sBando%>,'<%=sCodTimpr%>',<%=nNumPosti%>)" onmouseover="javascript:window.status=' '; return true">
						<%=sDescBando%>
					</a>
				</td>
		<%				
			end if
		%>
				<td class="tblDett">
					<%=nNumPosti%>
				</td>							
			</tr>
<%		rsBando.MoveNext
		loop
%>		
        </table>
        </form>	
		<br>
<%
end if
	rsBando.Close
	set rsBando = nothing	
end sub

'----------------F U N C T I O N -----------------'
'TRUE => non si puo' modificare posti (esiste graduatoria definitiva).
'FALSE=> si puo' modificare posti (non esiste graduatoria definitiva).
Function NoModifica(sBando)
	
	dim rsVer, sSQL
	
	sSQL=	"SELECT NVL(status_grad,0) StatusGrad"	&_
			" FROM BANDO_POSTI"			&_
			" WHERE id_bando= " & sBando &_
			" AND id_sede = 0" 		
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	set rsVer = CC.Execute(sSQL)
	
	IF NOT rsVer.eof THEN
		If cint(rsVer("StatusGrad")) = 1 then
			NoModifica = true
		Else
			NoModifica = false
		End if
	END IF
	rsVer.close
	set rsVer = nothing
		
End Function

'-------------------------------------------------------------------
 sub Fine()%>
	<!--#include virtual ="/include/closeconn.asp"-->
	<!--#include virtual = "strutt_coda2.asp"--> 
<%end sub %>



