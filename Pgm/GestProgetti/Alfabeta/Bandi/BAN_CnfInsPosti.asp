<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%'Option Explicit
   Response.ExpiresAbsolute = Now() - 1 
   Response.AddHeader "pragma","no-cache"
   Response.AddHeader "cache-control","private"
   Response.CacheControl = "no-cache"
%>
<!--#include virtual="/strutt_testa2.asp"-->
<!--#include virtual ="/include/openconn.asp"-->
<!--#include virtual="/util/portallib.asp"-->
<!--#include virtual = "/include/SysFunction.asp"-->
<!--#include virtual = "/include/ControlDateVB.asp"-->

<!-- ************** Javascript Inizio ************ -->

<script LANGUAGE="javascript">
<!--#include Virtual = "/Include/help.inc"-->

</script>
<!-- ************** Javascript Fine ************ -->
<%
dim sModo, Errore
dim nPosti, sIdBando, sStatGrad, sSede
dim sDescBando
dim sCodBando
dim sAreaTerr
dim sSQL

sModo = Request("Action")
sIdBando = Request.Form("hIdBando")
nPosti = Request.Form ("txtNumPosti")
dDataTmst= Request.form("hDataTmst")

Inizio()		

select case sModo
case "INS" 
	Inserimento()
case "MOD"
	'dDataTmst= Request.form("hDataTmst")
	Modifica()
end select

Fine()

'-------------------------------------------------------------------------------------------------------------------------------
sub Inizio()
%>
	<table border="0" width="520" cellspacing="0" cellpadding="0" height="81">
	<tr>
	  <td width="500" background="<%=Session("Progetto")%>/images/titoli/strumenti2b.gif" height="81" valign="bottom" align="right">
	    <table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
	      <tr>
	        <td width="100%" valign="top" align="right"><b class="tbltext1a">Gestione Progetti</span></b></td>
	      </tr>
	    </table>
	  </td>
	</tr>
	</table>
	<br>
	<br>
<%
end sub
'-------------------------------------------------------------------------------------------------------------------------------

Sub Msgetto(Msg)
%>
	<br>
	<table align="center">
	<tr>
		<td class="tbltext3"><b><%=Msg%></b></td>
	</tr>	
	</table>
<%
End Sub
'-------------------------------------------------------------------------------------------------------------------------------
sub TornaIndietro()
%>
	<form name="frmIndietro" method="post" action="BAN_VisPosti.asp">
	</form>
    <!--#include virtual = "/include/closeconn.asp"-->
	<script>
		alert("Operazione correttamente effettuata");
		frmIndietro.submit()
	</script>
<%
end sub
'-------------------------------------------------------------------------------------------------------------------------------
sub Inserimento()
	'Controllo se esiste gi� in tabella un Bando con lo stesso numero posti
	dim sContatore
        sContatore = 0
		sSede= 0
		sStatGrad = 0
		
	    sSQL = "SELECT COUNT(*) AS Count FROM BANDO_POSTI WHERE ID_BANDO = " & sIdBando &_
	           "AND ID_SEDE =" & sSede 
				
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	    set rsBando = CC.Execute(sSQL)
	    sContatore = cint(rsBando("Count"))
	    rsBando.close
		set rsBando = nothing
		
		CC.BeginTrans
		Errore = "0"
		If sContatore > 0 then
			Errore = "<BR>Numero posti gia' presente per questo Bando."
		Else
     	   sSQL = "INSERT INTO BANDO_POSTI" &_
		          "(ID_BANDO," &_
	    		  "ID_SEDE," &_
	    		  "NUM_POSTI,"
		   
		sSQL=sSQL & "STATUS_GRAD," &_	
				    "DT_TMST) " &_	
				    "VALUES " &_
				    "(" & sIdBando &_
					"," & sSede &_
					"," & nPosti &_
		   			"," & sStatGrad &_
		   			"," & ConvDateToDb(Now()) & ")" 
	             
           Errore = EseguiNoC(sSQL,CC)
		END IF           
		  	      
		      if Errore = "0" then
		         CC.CommitTrans
		         TornaIndietro()
		      else
		         CC.RollbackTrans
		         Msgetto("Errore nell'inserimento")
		      end if   
		  %>
		<table border="0" cellspacing="1" cellpadding="1" width="500">
		<tr align="middle">
			<td class="tbltext3">
				Inserimento non effettuabile.
			</td>
		</tr>
		<tr align="middle">
			<td class="tbltext3">
				<%Response.Write(Errore)%> 
			</td>
		</tr>
		</table>
		<br>
	   <table border="0" cellpadding="0" cellspacing="1" width="500">
		<tr>
			<td align="middle" colspan="2"><b>
			 <a HREF="javascript:history.back()"><img SRC="<%=Session("Progetto")%>/images/indietro.gif" border="0">
			</td>
		</tr>
		</table>
	<%			
    set rsProg = nothing
End Sub
'-------------------------------------------------------------------------------------------------------------------------------

sub Modifica()

	sSQL = "SELECT COUNT(*) AS Count FROM BANDO_POSTI WHERE ID_BANDO = " & sIdBando &_
	       " AND ID_SEDE = 0" 
	       '& sSede 

'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	set rsBando = CC.Execute(sSQL)
	sContatore = cint(rsBando("Count"))
	rsBando.close
	set rsBando = nothing
		
	Errore = "0"
	
	If sContatore = 0 then
		Errore = "<BR>Il record che si vuole modificare non � stato trovato."
	Else		
		sSQL = "UPDATE BANDO_POSTI SET NUM_POSTI=" & nPosti & _
			   ", DT_TMST=" & ConvDateToDb(Now()) & _
	           " WHERE ID_BANDO = " & sIDBando &_
	           " AND ID_SEDE=0"

		chiave=""
		Errore = Esegui("ID_BANDO", "BANDO_POSTI", Session("idutente"), "MOD", sSQL, 0, dDataTmst)
	End If  
 
 	if Errore = "0" then
	    TornaIndietro()
	else
	%>
	  <table border="0" cellspacing="1" cellpadding="1" width="500">
	  <tr align="middle">
		  <td class="tbltext3">
		    	Modifica non effettuabile.
		  </td>
	  </tr>
	  <tr align="middle">
		<td class="tbltext3">
			<%Response.Write(Errore)%> 
		</td>
	  </tr>
	  </table>
		<br>				
<%
	End if
End Sub	
'-------------------'
sub Fine()
%>
	<br>
	<br>
	<table border="0" cellpadding="0" cellspacing="1" width="500">
	<tr>
		<td align="center">
			<a href="javascript:history.go(-1)">
				<img src="<%=session("Progetto")%>/images/indietro.gif" border="0" name="imgPunto2" onmouseover="javascript:window.status=' '; return true">
			</a>
		</td>
	</tr>
	</table>
	<br>
<!--#include virtual = "/include/closeconn.asp"-->
<!--#include virtual = "strutt_coda2.asp"-->
<%end sub%>

