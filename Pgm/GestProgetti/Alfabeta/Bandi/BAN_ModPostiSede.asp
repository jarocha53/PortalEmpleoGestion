<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<script LANGUAGE="Javascript">
<!--#include Virtual = "/Include/help.inc"-->

//PAGINAZIONE//
function AssegnaVal(val){
	document.FrmPagina.pagina.value = val;
	document.FrmPagina.submit()
}

//CARICAMENTO PAGINA//
function Reload(n)
{
	var nSel
	nSel = frmSelProv.cmbProv.selectedIndex
	nProvincia = frmSelProv.cmbProv.options[nSel].value

	if (nProvincia == "")
	{
		alert("Selezionare una Provincia")
		frmSelProv.cmbProv.focus();
		return false
	}
	
	if (n==0)
	{
		document.frmSelProv.action='BAN_ModPostiSede.asp';
		document.frmSelProv.submit();
	}
	else
	{
		document.frmSelProv.action='BAN_InsPostiSede.asp';
		document.frmSelProv.submit();
	}
}
</script>

<%
'-------------------- S U B ----------------------'
Sub Inizio()%>
	<table border="0" width="520" cellspacing="0" cellpadding="0" height="81">
	   <tr>
	     <td width="500" background="<%=Session("Progetto")%>/images/titoli/strumenti2b.gif" height="81" valign="bottom" align="right">
	       <table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
	         <tr>
	           <td width="100%" valign="top" align="right"><b class="tbltext1a">Gestione Progetti</b></td>
	         </tr>
	       </table>
	     </td>
	   </tr>
	</table>
	<br>
	<table cellpadding="0" cellspacing="0" width="500" border="0">
		<tr height="18">
			<td class="sfondomenu" height="18" width="67%">
			<span class="tbltext0"><b>&nbsp;GESTIONE DISPONIBILITA' POSTI / SEDE</b></span></td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
			<td valign="middle" align="right" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1"></td>
		</tr>
		<tr width="371" class="SFONDOCOMM">
			<td colspan="3">
				Selezionare una Provincia per visualizzare i Posti disponibili nelle varie Sedi.
				<br>Se risultano definiti Centri di aggregazione per il Bando, verra' visualizzato 
				l'elenco delle <b>Sedi</b> della provincia selezionata e i relativi <b>Posti</b>. 
				<br>Sara' quindi possibile effettuare <b>l'inserimento</b> di nuove Sedi/Posti fino all'apertura 
				delle iscrizioni al Bando, e la <b>modifica</b> di quelle esistenti finch� non viene realizzata la 
				graduatoria definitiva.
				<a href="Javascript:Show_Help('/Pgm/help/Gestprogetti/alfabeta/Bandi/BAN_ModPostiSede')">
				<img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0">
				</a>
			</td>
		</tr>
		<tr height="2">
			<td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>
<%
End Sub

'-------------------- S U B ----------------------'
Sub SelProv()
%>
	<form name="frmSelProv" method="post" action="BAN_ModPostiSede.asp">
	<input type="hidden" name="hIdBando" value="<%=nBando%>">
	<br>
	
	<table width="500" border="0" cellspacing="2" cellpadding="1">
		<tr>
			<td class="tbltext1" width="20%">
				<b>Bando</b>
			</td>
			<td class="tbltext">
				<b><%=sDescBando%></b>
			</td>
		</tr>
<%		sMask= SetProvBando(nBando,session("idUorg"))
		If sMask = "" then
%>		<tr>
			<td COLSPAN="2">
				&nbsp;
			</td>
		</tr>
		<tr>
			<td class="tbltext3" align="center" COLSPAN="2">
				<b>Non risultano province associate al Bando.</b>
			</td>
		</tr>
<%		Else
%>		<tr>
			<td class="tbltext1" width="20%">
				<b>Provincia</b>
			</td>
			<td align="left">
				<select NAME="cmbProv" class="textblack" onchange="Reload(0)">			 
					<option>&nbsp;</option>
<%					nProvSedi = len(sMask)
					if nProvSedi <> 0 then
						pos_ini = 1 	                
						for i=1 to nProvSedi/2
							sTarga = mid(sMask,pos_ini,2)
							
							Response.Write	"<OPTION "
							Response.write	"value ='" & sTarga & "'"
							if sProv = sTarga then
								Response.Write " selected"
							end if
							Response.write  "> " & DecCodVal("PROV", 0, "", sTarga,"") & "</OPTION>"
							
							pos_ini = pos_ini + 2
						next
					end if
%>
				</select>
			</td>
		</tr>
<%		End If
%>	</table>
	</form>
<%
End Sub

'-------------------- S U B ----------------------'
Sub VisSedi()

	dim nBando, nIdSede, nPosti
	dim sDescSede, sDescImpr, rsSedi, sSQL
	dim bNoAdd, bNoModify
	
	nBando= Request.Form("hIdBando")
	sProv = Request.Form("cmbProv")
	bNoAdd=	NoAggiungi(nBando)
	
	set rsSedi = server.CreateObject("ADODB.Recordset")	
	sSQL=	"SELECT si.descrizione, si.id_sede, i.rag_soc, bp.num_posti" &_
			" FROM SEDE_IMPRESA si, BANDO_POSTI bp, IMPRESA i" &_
			" WHERE bp.id_sede= si.id_sede" &_
			" AND si.id_impresa= i.id_impresa" &_
			" AND bp.id_bando= " & nBando &_
			" AND si.prv= '" & sProv & "'" &_
			" ORDER BY i.rag_soc, si.descrizione"
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	rsSedi.Open sSQL,CC,3	

	If rsSedi.EOF then
%>
		<table align="center">
			<tr>
				<td class="tbltext3" align="center">
					<b>Non risultano associazioni Sedi/Posti per la provincia selezionata.</b>
				</td>
			</tr>	
		</table>
<%		if bNoAdd = false then
			TastoBackAdd()
		else
			TastoBack()
		end if
	Else
		if bNoAdd = false then
			TastoAdd()
		end if
		
		rsSedi.PageSize = nTamPagina
		rsSedi.CacheSize = nTamPagina
		nTotPagina = rsSedi.PageCount 
		If nActPagina < 1 Then
			nActPagina = 1
		End If
		If nActPagina > nTotPagina Then
			nActPagina = nTotPagina
		End If
		rsSedi.AbsolutePage= nActPagina
%>
		<br>
		<table border="0" cellpadding="1" cellspacing="2" width="500">
			<tr class="sfondocomm">
				<td align="center" width="85%">
					<b>Rag. Soc. / Sede</b>
				</td>
				<td align="center">
					<b>N. Posti</b>
				</td>			
			</tr>
<%		i=0
		nTotRecord=0
		do while not rsSedi.EOF And nTotRecord < nTamPagina
			nIdSede =	rsSedi("id_sede")
			nPosti  =	rsSedi("num_posti")
			sDescSede = rsSedi("descrizione") 
			sDescImpr = rsSedi("rag_soc")
			bNoModify = NoModifica(nBando,nIdSede)
			i = i + 1
%>
			<form name="FrmVisSedi<%=i%>" action="BAN_DettPostiSede.asp" method="post">
				<input type="hidden" name="hdnIdBando" value="<%=nBando%>">
				<input type="hidden" name="hdnIdSede" value="<%=nIdSede%>">
				<input type="hidden" name="hdnProv" value="<%=sProv%>">
			</form>
			<tr class="tblsfondo">
<%			if bNoModify = false then
%>				<td class="tbldett">
					<a class="tblAgg" HREF="Javascript:FrmVisSedi<%=i%>.submit();" onmouseover="javascript:window.status='' ; return true">
						<b><%=sDescImpr%>&nbsp;/&nbsp;<%=sDescSede%></b>
					</a>
				</td>
<%			else
%>				<td class="tbldett">
					<%=sDescImpr%>&nbsp;/&nbsp;<%=sDescSede%>				
				</td>
<%			end if
%>				<td class="tbldett"><%=nPosti%></td>					
			</tr>				
<%			nTotRecord=nTotRecord + 1
		rsSedi.MoveNext
		loop
%>
		</table>
		
<!--PAGINAZIONE-->
		<form name="FrmPagina" method="post" action="BAN_ModPostiSede.asp">
		<input type="hidden" id="pagina" name="pagina" value="<%=nActPagina%>">
		<input type="hidden" name="hIdBando" value="<%=nBando%>">
		<input type="hidden" name="cmbProv" value="<%=sProv%>">
		<table border="0" width="500">
			<tr>
			<%
			if nActPagina > 1 then%>		
				<td align="right" width="480">
					<a href="Javascript:AssegnaVal('<%=(nActPagina - 1)%>')">
						<img src="<%=Session("Progetto")%>/images/precedente.gif" border="0" id="image1" name="image1">
					</a>
				</td>
			<%
			end if
			if nActPagina < nTotPagina then%>
				<td align="right">
					<a href="Javascript:AssegnaVal('<%=(nActPagina + 1)%>')">
						<img src="<%=Session("Progetto")%>/images/successivo.gif" border="0" id="image1" name="image1">
					</a>
				</td>
			<%
			end if%>		
			</tr>
		</table>
		</form>		
<%		TastoBack()
	End if
	rsSedi.Close
	set rsSedi=nothing
	
End Sub

'----------------F U N C T I O N -----------------'
'TRUE => non si puo' modificare sede/posti (esiste graduatoria definitiva).
'FALSE=> si puo' modificare sede/posti (non esiste graduatoria definitiva).
Function NoModifica(nIdB, nIdS)
	
	dim rsVer, sSQL
	
	sSQL=	"SELECT NVL(status_grad,0) StatusGrad"	&_
			" FROM BANDO_POSTI"			&_
			" WHERE id_bando= " & nIdB	&_
			" AND id_sede= " & nIdS			
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	set rsVer = CC.Execute(sSQL)
	
	IF NOT rsVer.eof THEN
		If cint(rsVer("StatusGrad")) = 1 then
			NoModifica = true
		Else
			NoModifica = false
		End if
	END IF
	rsVer.close
	set rsVer = nothing
		
End Function

'----------------F U N C T I O N -----------------'
'TRUE=> non si puo' aggiungere sede (sono aperte le iscrizioni al bando).
'FALSE=> si puo' aggiungere sede (non sono ancora aperte le iscrizioni).
Function NoAggiungi(nIdB)
	
	dim rsVer, sSQL, dDate, dNow
	
	sSQL=	"SELECT dt_ini_acq_dom FROM BANDO " &_
			"WHERE id_bando= " & nIdB	
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	set rsVer = CC.Execute(sSQL)
	
	IF NOT rsVer.eof THEN
		dDate = cDate(rsVer("dt_ini_acq_dom"))
		dNow = cDate(Date())
		If dDate > dNow then
			NoAggiungi=false
		else
			NoAggiungi=true
		end if 
	END IF
	rsVer.close
	set rsVer = nothing
		
End Function

'-------------------- S U B ----------------------'
Sub TastoBack()
%>
	<table width="500" border="0">
		<tr align="center">
			<td><a href="BAN_VisPosti.asp" onmouseover="javascript:window.status=' '; return true"><img src="<%=session("Progetto")%>/images/indietro.gif" border="0" name="imgPunto2"></a></td>
	    </tr>
	</table> 
<%
End Sub

'-------------------- S U B ----------------------'
Sub TastoAdd()
%>
	<table width="500" border="0">
		<tr align="center">
   			<td><a href="Javascript:Reload(1);" onmouseover="javascript:window.status=' '; return true"><img src="<%=session("Progetto")%>/images/aggiungi.gif" border="0" name="imgPunto2"></a></td>
	    </tr>
	</table>
<%	
End Sub

'-------------------- S U B ----------------------'
Sub TastoBackAdd()
%>
	<br>
	<table width="500" border="0">
		<tr align="center">
   			<td><a href="Javascript:Reload(1);" onmouseover="javascript:window.status=' '; return true"><img src="<%=session("Progetto")%>/images/aggiungi.gif" border="0" name="imgPunto2"></a></td>
			<td><a href="BAN_VisPosti.asp" onmouseover="javascript:window.status=' '; return true"><img src="<%=session("Progetto")%>/images/indietro.gif" border="0" name="imgPunto2"></a></td>
	    </tr>
	</table> 
<%
End Sub

'-------------------- S U B ----------------------'
Sub Fine()
%>
	<!--#include virtual ="/include/closeconn.asp"-->
	<!--#include virtual = "/strutt_coda2.asp"-->
<%
End sub 

'-------------------------------------------------' 
'-------------------' M A I N '-------------------' 
%>
	<!-- #include virtual="/strutt_testa2.asp"-->
	<!--#include virtual ="/include/openconn.asp"-->
	<!--#include virtual ="/include/SelAreaTerrBandi.asp"-->
	<!--#include virtual="/util/portallib.asp"-->
<%
	if ValidateService(session("idutente"),"BAN_VISPOSTI", CC) <> "true" then 
		response.redirect "/util/error_login.asp"
	end if

	dim nBando
	dim Record, nIndElem
	dim nActPagina, nTamPagina
	dim nTotPagina, nTotRecord
	
	nTamPagina=15
	If Request("pagina")= "" Then
		nActPagina=1
	Else
		nActPagina=Clng(Request("pagina"))
	End If
	
	nBando=		Request.Form("hIdBando")
	sProv=		Request.Form("cmbProv")
	sDescBando= DecBando(nBando)
	
	Inizio()
	SelProv()
		if Request.Form("cmbProv") <> "" then
			VisSedi()
		else
			TastoBack()
		end if
	Fine()
%>


