<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #include virtual="/strutt_testa2.asp"-->
<!--#include virtual ="/include/openconn.asp"-->
<!--#include virtual="/util/portallib.asp"-->
<!--#include virtual = "include/ControlDateVB.asp"-->
<!--#include virtual = "include/SelAreaTerrBandi.asp"-->
<%
if ValidateService(session("idutente"),"BAN_VISPOSTI", CC) <> "true" then 
	response.redirect "/util/error_login.asp"
end if
%>
<!-- ************** Javascript Inizio ************ -->
<script LANGUAGE="Javascript">
<!--#include virtual="/Include/help.inc"-->
<!--#include virtual = "/Include/ControlDate.inc"-->
<!--#include virtual = "/Include/ControlNum.inc"-->
<!--#include virtual = "/Include/ControlString.inc"-->  
	
//Funzione per i controlli dei campi da inserire 
	function ControllaDati(frmInsPosti){		
		
		if (frmInsPosti.cmbSedi.value == ""){
			alert("Sede Obbligatoria")
			frmInsPosti.cmbSedi.focus() 
			return false
		}
		 		
		//Numero Posti obbligatorio
				
		if ((frmInsPosti.txtNumPosti.value == "")){
			alert("Numero Posti obbligatorio")
			frmInsPosti.txtNumPosti.focus() 
			return false
		}
		
		//Numero Posti > 0
		
		if ((frmInsPosti.txtNumPosti.value == "0")){
			alert("Numero Posti deve essere maggiore di 0")
			frmInsPosti.txtNumPosti.focus() 
			return false
		}
		
		//Il valore deve essere numerico
		
		if (!IsNum(frmInsPosti.txtNumPosti.value)){
		alert("Il valore deve essere numerico")
		frmInsPosti.txtNumPosti.focus() 
		return false
	}		  
	return true
	}
</script>
<!-- ************** Javascript Fine   ************ -->
<%
Intestazione()
PreparaPagina()
fine()

sub Intestazione()
%>
<table border="0" width="520" cellspacing="0" cellpadding="0" height="81">
   <tr>
     <td width="500" background="<%=Session("Progetto")%>/images/titoli/strumenti2b.gif" height="81" valign="bottom" align="right">
       <table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
         <tr>
           <td width="100%" valign="top" align="right"><b class="tbltext1a">Gestione Progetti</span></b></td>
         </tr>
       </table>
     </td>
   </tr>
</table>
<br>
<table cellpadding="0" cellspacing="0" width="500" border="0">
		<tr height="18">
			<td class="sfondomenu" height="18" width="67%">
			<span class="tbltext0"><b>&nbsp;GESTIONE DISPONIBILITA' POSTI / SEDE</b></span></td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
			<td valign="middle" align="right" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1">(*) campi obbligatori</td>
		</tr>
		<tr width="371" class="SFONDOCOMM">
			<td colspan="3">
			Inserimento disponibilita' posti per la sede.<br>
			Premere <b>Invia</b> per salvare. 
			<a href="Javascript:Show_Help('/Pgm/help/GestProgetti/Alfabeta/Bandi/BAN_InsBandi')" onmouseover="javascript:window.status=' '; return true"><img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a></td>
		</tr>
		<tr height="2">
			<td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>
	<br>		
<%end sub%>

<%sub PreparaPagina()

IdBando=Request.Form ("hIdBando")
'Response.Write IdBando  & "<BR>"
Provincia=Request.Form ("CmbProv")
'Response.Write Provincia 
sDescBando= DecBando(IdBando)
'Response.Write sDescBando & "sDescBando"
%>
<form method="post" name="frmInsPosti" onsubmit="return ControllaDati(this)" action="BAN_CnfInsPostiSede.asp">	
	<table border="0" cellpadding="2" cellspacing="0" width="500"> 
		<tr>
		   <td class="tbltext1" align="left" width="100">
				<b>&nbsp;&nbsp;Bando</b>
			</td>
			<td class="tbltext" align="left" width="400">
				<b><%=sDescBando%></b>
			</td>		   
		</tr>
		<tr>
		   <td align="left" width="100" class="tbltext1">
				<b>&nbsp;&nbsp;Provincia</b>
		   </td>
		   <td class="tbltext" align="left" width="400">
				<b><%=DecCodVal("PROV", 0, "", Provincia,"")%></b>
			</td>
		</tr>
		<tr>
			<td width="100">&nbsp;</td>
			<td width="400">&nbsp;</td>
		</tr>
<%
		sSQL=	" SELECT i.rag_soc||' '||si.descrizione as descrizione,SI.ID_SEDE " &_
				" FROM sede_impresa si,impresa i,progetto p" &_
				" WHERE SI.PRV='" & Provincia & "'" &_
				" AND si.id_impresa=i.id_impresa " &_
				" AND p.cod_timpr=i.cod_timpr AND P.AREA_WEB ='" & MID(SESSION("PROGETTO"),2) & "'"  &_
				" AND  NOT EXISTS (SELECT ID_SEDE FROM BANDO_POSTI BP WHERE ID_BANDO = " & IdBando &_
				" AND BP.ID_SEDE=SI.ID_SEDE)" &_
				" ORDER BY i.rag_soc, SI.DESCRIZIONE"
	
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
		SET rsSedi = CC.execute(sSQL)
		If rsSedi.EOF then
%>							
		</table>
		<table align="center" border="0" width="500">
			<tr>
				<td class="tbltext3" align="center">
					<b>Non risultano sedi per la provincia di <%=DecCodVal("PROV", 0, "", Provincia,"")%>.</b>
				</td>
			</tr>	
		</table>
		<br>
		<table cellpadding="0" cellspacing="0" width="400" border="0">	
			<tr align="center">		
				<td nowrap><a href="javascript:history.back();"><img src="<%=session("Progetto")%>/images/indietro.gif" border="0" name="imgPunto2" onmouseover="javascript:window.status=' '; return true"></a></td>
				</tr>
		</table>
<%																																
		ELSE					
%>		
		<tr>
			<td align="left" width="100" class="tbltext1">
				<b>&nbsp;&nbsp;Sede*</b>
		    </td>		
			<td align="left" width="400" class="tbltext1">   					   		   		   	
				<select onchange class="textblacka" name="cmbSedi">
					<option selected value></option>
<%				
			do while not rsSedi.eof 
				sIdsede = rsSedi("ID_SEDE")
				Response.Write "<Option "			
				Response.Write "value=" & rsSedi ("ID_SEDE") &_
				"> " & rsSedi ("DESCRIZIONE")&_
				"</OPTION>"
				rsSedi.MoveNext
			loop
			Response.Write ("</select></center>")			
%>			
			</td>	
		</tr>
		<tr>
		   <td align="left" width="100" class="tbltext1">
				<b>&nbsp;&nbsp;N.Posti*</b>
		   </td>
		   <td align="left">
				<input style="TEXT-TRANSFORM: uppercase;" size="5" maxlength="4" class="textblack" name="txtNumPosti">
			</td>		   
		</tr>			 
	</table> 
	<br><br><br><br>
	<input type="hidden" name="hIdBando" value="<%=IdBando%>">
	<input type="hidden" name="Provincia" value="<%=Provincia%>">	
	
	<%'impostazione dei comandi%>
	<table cellpadding="0" cellspacing="0" width="400" border="0">	
	  <tr align="center">
		<td nowrap><input type="image" name="Conferma" src="<%=Session("progetto")%>/images/conferma.gif"></td value="Registra">
		<td nowrap><a href="javascript:history.back();"><img src="<%=session("Progetto")%>/images/indietro.gif" border="0" name="imgPunto2" onmouseover="javascript:window.status=' '; return true"></a></td>
	  </tr>
	</table>
</form>
<%
		END IF			
rsSedi.Close
set rsSedi=nothing	
end sub
%>

<%sub fine()%>
<!--#include virtual ="/include/closeconn.asp"-->
<!--#include virtual = "strutt_coda2.asp"-->
<% end sub %>
