<%'Option Explicit 
  ' Response.ExpiresAbsolute = Now() - 1 
  ' Response.AddHeader "pragma","no-cache"
  ' Response.AddHeader "cache-control","private"
  ' Response.CacheControl = "no-cache"
%>
<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #include virtual="/strutt_testa2.asp"-->
<!--#include virtual ="/include/openconn.asp"-->
<!--#include virtual ="/util/portallib.asp"-->

<%Session("menu")= ""%>
<!-- ************** Javascript Inizio ************ -->
<!--include del file per fare i controlli sulle stringhe-->
<script LANGUAGE="Javascript" src="/Include/ControlString.inc"></script>
<script LANGUAGE="Javascript" src="/Include/ControlloCI.inc"></script>
<!--include del file per fare i controlli sulla validit� delle date-->
<script LANGUAGE="Javascript" src="/Include/ControlDate.inc"></script>
<!--include del file per fare i controlli sulla numericit� dei campi-->
<script LANGUAGE="Javascript" src="/Include/ControlNum.inc"></script>

<script LANGUAGE="Javascript" src="/Include/help.inc"></script>
<script LANGUAGE="Javascript" src="/include/SelComune.js"></script>
<script LANGUAGE="Javascript" src="Controlli.js"></script>
<script LANGUAGE="Javascript" src="ControlliBac.js"></script>
<script LANGUAGE="Javascript" src="ControlliBac2.js"></script>
<script LANGUAGE="Javascript" src="ControlliBac3.js"></script>
<!-- ************** ASP Inizio *************** -->

<!--#include virtual = "/include/DecCod.asp"-->
<!--#include virtual = "/include/ControlDateVB.asp"-->
<!--#include virtual = "/util/dbutil.asp"-->

<script LANGUAGE="Javascript">

//Funzione per i controlli dei campi da inserire 
	function ControllaDati()
	{		
		// SM inizio- controllo C.I.
		
		document.frmDomIscri.txtCodFisc.value=TRIM(document.frmDomIscri.txtCodFisc.value)
		if (document.frmDomIscri.txtCodFisc.value != "")
		{
				blank = " ";
				if (!ChechSingolChar(document.frmDomIscri.txtCodFisc.value,blank))
				{
			
					alert("El campo C.I. no puede contener espacios en blanco")
					document.frmDomIscri.txtCodFisc.focus()
					return false;
				}
				
				if ((document.frmDomIscri.txtCodFisc.value.length != 8))
				{
					alert("El campo C.I. debe ser de 8 caracteres")
					document.frmDomIscri.txtCodFisc.focus() 
					return false
				}			
				if (!ValidateInputStringWithNumber(document.frmDomIscri.txtCodFisc.value))
				{
					alert("El campo C.I. es err�neo")
					document.frmDomIscri.txtCodFisc.focus() 
					return false
				}
				
				if (!IsNum(document.frmDomIscri.txtCodFisc.value))
				{
					alert("El campo C.I. debe ser num�rico")
					document.frmDomIscri.txtCodFisc.focus() 
					return false
				}		
		
				if (!ControlloCI(eval(document.frmDomIscri.txtCodFisc.value)))
				{
				     document.frmDomIscri.txtCodFisc.focus()
				     return false
				}
				
		} else {
			alert("Debe ingresar una C.I.")
			return false
		}		
		// SM fine
		
	alert("C.I. v�lida!")				
	return true
}

</script>
<br>
<p>
	<table cellpadding="0" cellspacing="0" width="500" border="0">
			<tr height="18">
				<td class="sfondomenu" height="18" width="67%">
				<span class="tbltext0"><b>&nbsp;CONTROL DE C.I.</b></span></td>
				<td width="3%" background="/Ba/images/tondo_linguetta.gif">&nbsp;</td>
				<td valign="middle" align="right" class="tbltext1" width="50%" background="/Ba/images/sfondo_linguetta.gif">(*) campos obligatorios</td>
			</tr>
			<tr>
				<td class="sfondocomm" width="57%" colspan="3">
				    Completar el numero de cedula sin puntos ni guion y presionar <b>Envia</b> para verificar el numero de CI.<br>
				<!--a href="Javascript:Show_Help('/Ba/Pgm/Help/GestProgetti/Alfabeta/Selezione/Utente/UTE_IscrUtente')" onmouseover="javascript:window.status=' '; return true"><img align="right" src="/Ba/images/help.gif" border="0"></a-->
				</td>
			</tr>
			<tr height="2">
				<td colspan="3" class="sfondocomm" background="/BA/images/separazione.gif">
				</td>
			</tr>
	</table>
	<form action="" method="post" name="frmDomIscri" id="frmDomIscri"  onsubmit="return ControllaDati(this)">
	<input type="hidden" name="progetto" value="<%=Session("Progetto")%>">
	<input type="hidden" name="txtcodproj" value="<%=sProgetto%>">
	<input type="hidden" name="txtIdBando" value="<%=nBando%>">
	<input type="hidden" name="txtDescTimpr" value="<%=sDescTimprI%>">
<br>
<table border="0" cellpadding="0" cellspacing="0" width="500">
    <tr height="18">
		<td class="sfondomenu" width="67%" height="18">
			<span class="tbltext0"><b>&nbsp;DATOS DEL CANDIDATO</b></span></td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
		<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
    </tr>
    <tr height="2">
			<!--td width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif">	</td-->     
	</tr>
 </table>
 <br>
<table border="0" cellpadding="2" cellspacing="2" width="500">
  	<tr>
        <td align="left" colspan="2" class="tbltext1">
				<span class="tbltext1">
				<strong>C.I.* </strong>(solo numeros, sin puntos ni guion) &nbsp; por ej. CI. 2.877.654-3, ingresarla 28776543
				</span>
        </td>
        <td align="left" colspan="2" width="60%">
				<input class="textblacka" style="TEXT-TRANSFORM: uppercase; WIDTH:   167px; HEIGHT: 22px" size="11" maxlength="8" name="txtCodFisc">
        </td>
	</tr>
</table>
        <input type="image" name="Invia" src="<%=Session("Progetto")%>/images/conferma.gif" border="0" value="Conferma"> 
</form>


