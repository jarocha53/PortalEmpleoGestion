<html>
<head>
<link href="estilos.css" rel="stylesheet" type="text/css">
<link href="template.css" rel="stylesheet" type="text/css">
<link href="StyleSECOL.css" rel="stylesheet" type="text/css">
<%'Option Explicit 
  ' Response.ExpiresAbsolute = Now() - 1 
  ' Response.AddHeader "pragma","no-cache"
  ' Response.AddHeader "cache-control","private"
  ' Response.CacheControl = "no-cache"
%>


<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #include virtual="/strutt_testa2.asp"-->
<!--#include virtual ="/include/openconn.asp"-->
<!--#include virtual ="/util/portallib.asp"-->

<%Session("menu")= ""%>
<!-- ************** Javascript Inizio ************ -->
<!--include del file per fare i controlli sulle stringhe-->
<script LANGUAGE="Javascript" src="/Include/ControlString.inc"></script>
<script LANGUAGE="Javascript" src="/Include/ControlloCI.inc"></script>
<!--include del file per fare i controlli sulla validit� delle date-->
<script LANGUAGE="Javascript" src="/Include/ControlDate.inc"></script>
<!--include del file per fare i controlli sulla numericit� dei campi-->
<script LANGUAGE="Javascript" src="/Include/ControlNum.inc"></script>

<script LANGUAGE="Javascript" src="/Include/help.inc"></script>
<script LANGUAGE="Javascript" src="/include/SelComune.js"></script>
<script LANGUAGE="Javascript" src="Controlli.js"></script>
<script LANGUAGE="Javascript" src="ControlliBac.js"></script>
<script LANGUAGE="Javascript" src="ControlliBac2.js"></script>
<script LANGUAGE="Javascript" src="ControlliBac3.js"></script>
<!-- ************** ASP Inizio *************** -->

<!--#include virtual = "/include/DecCod.asp"-->
<!--#include virtual = "/include/ControlDateVB.asp"-->
<!--#include virtual = "/util/dbutil.asp"-->

<script LANGUAGE="Javascript">
//include del file per fare i controlli sulla validit delle date
<!--#include virtual = "/include/SelComune.js"-->
<!--#include virtual = "/include/SelDepto.js"-->
<!--#include virtual = "/include/SelLoc.js"-->

//include del file per fare i controlli sulla validit delle date
<!--#include Virtual = "/Include/ControlDate.inc"-->

//include del file per fare i controlli sulla numericit dei campi
<!--#include Virtual = "/Include/ControlNum.inc"-->

//include del file per fare i controlli sulla validit del CF
<!--#include Virtual = "/Include/ControlCodFisc.inc"-->

//include del file per fare i controlli sulle stringhe
<!--#include Virtual = "/Include/ControlString.inc"-->
<!--#include Virtual = "/Include/help.inc"-->


/********************************************************
*   @autor: IPTECHONOLOGIES                             +
********************************************************/
$(function(){
   cmbPaisResidencia_onchange()
   cmbNazione_onchange() 

});

/**
* load options to cmbProvRes by cmbPaisResidencia select value in onchange event
*/
cmbPaisResidencia_onchange = function(){
    var cod_country_selected = $("#cmbPaisResidencia").val()

    var cmbProvRes = $("#cmbProvRes") 
    var imgPunto1  = $("#imgPunto1")
    var txtComRes = $("#txtComRes")



    cmbProvRes.html("");
    if(cod_country_selected != "CO"){
    	 cmbProvRes.attr("disabled","disabled")
    	 imgPunto1.hide()
    	 txtComRes.parent().children().filter("input").val("")
    }else{
    	cmbProvRes.removeAttr("disabled")
    	imgPunto1.show()
    }

    $.get("AJAX_load_states_by_country_code.asp?countrycode="+cod_country_selected,function(data){
         cmbProvRes.html(data);
    })

    
}

cmbNazione_onchange = function(){
	var cod_country_selected = $("#cmbNazione").val()

    var cmbProvNasc = $("#cmbProvNasc") 
    var imgPunto  = $("#A1")


    cmbProvNasc.html("");
    if(cod_country_selected != "CO"){
    	 cmbProvNasc.attr("disabled","disabled")
    	 imgPunto.hide()
    	 imgPunto.parent().children().filter("input").val("")
    }else{
    	cmbProvNasc.removeAttr("disabled")
    	imgPunto.show()
    }

    $.get("AJAX_load_states_by_country_code.asp?countrycode="+cod_country_selected,function(data){
         cmbProvNasc.html(data);
    })
}
/**++++++++++++++++++++++++++++++++++++++++++++++++++++*/
function PulisciCom()
{
	//document.txtComune.value = ""
	document.frmDomIscri.txtComNasc.value = ""
	// SM inizio
	//document.frmModDatiIscri.txtDeptoNacimiento.value = ""
	//document.frmModDatiIscri.txtDepto.value = ""
	//document.frmModDatiIscri.txtLocNacimiento.value = ""
	//document.frmModDatiIscri.txtLoc.value = ""
	// SM Fine
}



	function ApriSettori()
	{
	//document.AddEspro.DescrMansione.value = "";
	document.frmDomIscri.CriterioRicercaSettori.value=TRIM(document.frmDomIscri.CriterioRicercaSettori.value)
 	//document.AddEspro.RicCodIstat.value=TRIM(document.AddEspro.RicCodIstat.value)
   //document.AddEspro.Mansione.value = "";
			   if (!ValidateInputStringWithNumber(document.frmDomIscri.CriterioRicercaSettori.value))
				{
				alert("El criterio de b�squeda en el campo Actividad es err�neo")
				document.frmDomIscri.CriterioRicercaSettori.focus() 
				return;
				}

		CriterioRicerca = frmDomIscri.CriterioRicercaSettori.value
				
		//RicCodIstat = document.AddEspro.RicCodIstat.value
		RicCodIstat = ""
		URL = "/Pgm/BilancioCompetenze/Competenze/Settori.asp?VISMENU=NO" + "&RICERCA="+ CriterioRicerca + "&RicCodIstat=" + RicCodIstat + "&CodPagAnt=SI"
		opt = "address=no,status=no,width=950,height=300,top=150,left=30"
		//opt = "address=no,status=YES,width=1000,height=700,top=150,left=15"

		window.open (URL,"",opt)
		document.frmDomIscri.DescSettori.value =""
	}


	function PulisciLivStud()
	{
		document.frmDomIscri.txtSpecifico.value = ""
		document.frmDomIscri.txtTipoSpecifico.value = ""
	}

	function PulisciRes()
	{
		document.frmDomIscri.txtComuneRes.value = ""
		document.frmDomIscri.txtComRes.value = ""
				
	}
	
	function ActivInputFields()
    {
	    if (document.frmDomIscri.COD_STAT_STUD.value == "0")
	    {
		    document.frmDomIscri.AA_STUD_UltAnno.value ="";
    		document.frmDomIscri.AA_STUD_UltAnno.disabled = true;
	    	document.frmDomIscri.txtAnnoStud.disabled = false;
    	}	
	
	    if ((document.frmDomIscri.COD_STAT_STUD.value == "2")
		   || (document.frmDomIscri.COD_STAT_STUD.value == "1"))
			 {
				document.frmDomIscri.AA_STUD_UltAnno.disabled = false;
				document.frmDomIscri.txtAnnoStud.value= "";
				document.frmDomIscri.txtAnnoStud.disabled = true;
				
			}	
			
			
			if (document.frmDomIscri.COD_STAT_STUD.value == "4")
			{
				document.frmDomIscri.AA_STUD_UltAnno.value ="";
				document.frmDomIscri.AA_STUD_UltAnno.disabled = true;
				document.frmDomIscri.txtAnnoStud.value ="";
				document.frmDomIscri.txtAnnoStud.disabled = true;
				
			}			
	}	
	
	function esmenor (dateofborn)
	{
		//Set the two dates
		today=new Date()
		var borndate=new Date(dateofborn)
		//Set 1 day in milliseconds
		var one_day=1000*60*60*24

		//Calculate difference btw the two dates, and convert to days
		var days = Math.ceil((today.getTime()-borndate.getTime())/(one_day))
		return days		
	}
				
//Funzione per i controlli dei campi da inserire 
	function ControllaDati()
	{		

		// SM inizio- controllo N�mero de Identificaci�n		
		document.frmDomIscri.txtCodFisc.value=TRIM(document.frmDomIscri.txtCodFisc.value)
		if (document.frmDomIscri.txtCodFisc.value != "")
		{
				blank = " ";
				if (!ChechSingolChar(document.frmDomIscri.txtCodFisc.value,blank))
				{
			
					alert("El campo Numero no puede contener espacios en blanco")
					document.frmDomIscri.txtCodFisc.focus()
					return false;
				}
				
				/*if ((document.frmDomIscri.txtCodFisc.value.length > 10))
				{
					alert("El campo Numero debe ser de hasta 10 caracteres")
					document.frmDomIscri.txtCodFisc.focus() 
					return false
				}*/			
				if (!ValidateInputStringWithNumber(document.frmDomIscri.txtCodFisc.value))
				{
					alert("El campo Numero es err�neo")
					document.frmDomIscri.txtCodFisc.focus() 
					return false
				}
				
				if (!IsNum(document.frmDomIscri.txtCodFisc.value))
				{
					alert("El campo Numero debe ser num�rico")
					document.frmDomIscri.txtCodFisc.focus() 
					return false
				}		
		
				// if (!ControlloCI(eval(document.frmDomIscri.txtCodFisc.value)))
				// {
				//      document.frmDomIscri.txtCodFisc.focus()
				//      return false
				// }
				
		}		
		// SM fine
		

		//Nome
		document.frmDomIscri.txtNome.value=TRIM(document.frmDomIscri.txtNome.value)
		if (document.frmDomIscri.txtNome.value == "")
		{	
			alert("El campo Primer Nombre es obligatorio!")
			document.frmDomIscri.txtNome.focus() 
			return false
		}
		sNome=ValidateInputStringWithOutNumber(document.frmDomIscri.txtNome.value)	
		if  (sNome==false){
			alert("El campo Primer Nombre no es correcto.")
			document.frmDomIscri.txtNome.focus() 
			return false
		}

		// Secondo Nome
		sSecondoNome=ValidateInputStringWithOutNumber(document.frmDomIscri.txt_secondo_nome.value)	
		if  (sSecondoNome==false){
			alert("El campo Segundo Nombre no es correcto.")
			document.frmDomIscri.txt_secondo_nome.focus() 
			return false
		}
		
		//Cognome
		document.frmDomIscri.txtCognome.value=TRIM(document.frmDomIscri.txtCognome.value)
		if (document.frmDomIscri.txtCognome.value == "")
		{
			alert("El campo Primer Apellido es obligatorio!")
			document.frmDomIscri.txtCognome.focus() 
			return false
		}


		sCognome=ValidateInputStringWithOutNumber(document.frmDomIscri.txtCognome.value)
		if  (sCognome==false){
			alert("El campo Primer Apellido no es correcto.")
			document.frmDomIscri.txtCognome.focus() 
			return false
		}

		// Secondo Cognome
		sSecondoCognome=ValidateInputStringWithOutNumber(document.frmDomIscri.txt_secondo_cognome.value)	
		if  (sSecondoCognome==false){
			alert("El campo Segundo Apellido no es correcto.")
			document.frmDomIscri.txt_secondo_cognome.focus() 
			return false
		}

	   //Sesso
	   Sesso=document.frmDomIscri.cmbSesso.value
		if (Sesso == "")
		{
			alert("El campo Sexo es obligatorio!")
			document.frmDomIscri.cmbSesso.focus() 
			return false
		}	
  
          tipodoc = document.frmDomIscri.cmbTipoDoc.value
        if(tipodoc == "")
        {
            alert("Debe ingresar el Tipo de Documento")
			document.frmDomIscri.cmbTipoDoc.focus() 
			return false
        }
  
  
		
        if (document.frmDomIscri.txtCodFisc.value == "")
		         {
			      alert("Debe ingresar un N�mero de Documento")
			      document.frmDomIscri.txtCodFisc.focus() 
			      return false
		      }
		     
	 	//Data di Nascita
		if (document.frmDomIscri.txtDataNascita.value == "")
		{
			alert("El campo fecha de nacimiento es obligatorio!")
			document.frmDomIscri.txtDataNascita.focus() 
			return false
		}
		//Controla el registro de los menores de 15 a�os
		if (esmenor(document.frmDomIscri.txtDataNascita.value) < 5479)
		{
			alert("No se acepta el registro de menores de 15 a�os!")
			document.frmDomIscri.txtDataNascita.focus() 
			return false
		}
		//Controllo della validit� della Data di Nascita
		DataNascita = document.frmDomIscri.txtDataNascita.value
		DataOdierna = frmDomIscri.txtOggi.value
		if (!ValidateInputDate(DataNascita))
		{
			document.frmDomIscri.txtDataNascita.focus() 
			return false
		}
		if (ValidateRangeDate(DataNascita,DataOdierna)==false){
			//alert("La data di nascita deve essere precedente alla data odierna!")
			alert("El campo fecha de nacimiento debe ser anterior a la fecha actual!")
			frmDomIscri.txtDataNascita.focus()
			return false
		}
		
		document.frmDomIscri.txtLogin.value=TRIM(document.frmDomIscri.txtLogin.value)
		if (document.frmDomIscri.txtLogin.value == "")
		{
			alert("El campo usuario es obligatorio!")
			document.frmDomIscri.txtLogin.focus() 
			return false
		}     

		/********************************************************
		*   @autor: IPTECHONOLOGIES                             +
		********************************************************/  
		// Password Validation 
		var password = $("#txtPassword").val()
		var password_confirmation = $("#txtPasswordConfirmation").val()

		if (password == ""){
			alert("El campo Contrase�a es obligatorio!")
			$("#txtPassword").focus()
			return false
		}     

		if(password != password_confirmation){
			alert("Los campos de Contrase�a y Confirmaci�n de Contrase�a no coinciden!")
			$("#txtPassword").val("")
			$("#txtPasswordConfirmation").val("")
			$("#txtPassword").focus() 
			return false
		}
 
		     
		//El pais de residencia 
		paisresidencia = document.frmDomIscri.cmbPaisResidencia.value;
		if (( paisresidencia == "")||(paisresidencia == " ")) 
		{
				alert("El Pa�s de Residencia es obligatorio!")
				document.frmDomIscri.cmbPaisResidencia.focus() 
				return false
		}
				
		if (paisresidencia == "CO")
		{
    						
		    //Se la Provincia di Residenza � vuota, il Comune � obbligatorio
		    if ((document.frmDomIscri.cmbProvRes.value == "")||
			    (document.frmDomIscri.cmbProvRes.value == " ")) {
				    alert("El Departamento de Residencia es obligatorio!")
				    document.frmDomIscri.cmbProvRes.focus() 
				    return false
		    }		
    		
		    //Se Comune di Residenza � vuoto, il Comune � obbligatorio
		    if ((document.frmDomIscri.txtComuneRes.value == "")||
			    (document.frmDomIscri.txtComuneRes.value == " ")) {
				    alert("La Ciudad de Residencia es obligatorio!")
				    document.frmDomIscri.txtComuneRes.focus() 
				    return false
    			
		    }	
		    //Se Comune di Residenza � digitato, la Provincia � obbligatoria
		    if (document.frmDomIscri.txtComuneRes.value != ""){
			    if (document.frmDomIscri.cmbProvRes.value == ""){
				    //alert("Se viene selezionato il Comune di Residenza, anche il campo Provincia di Residenza � obbligatorio!")
				    alert("Si ha seleccionado el campo Ciudad, tambi�n el campo Departamento de Residencia es obligatorio!")
				    document.frmDomIscri.cmbProvRes.focus() 
				    return false
			    }
		    }
	        //Se la Provincia di Residenza � digitata, il Comune � obbligatorio
		    if ((document.frmDomIscri.txtComuneRes.value == "")||
			    (document.frmDomIscri.txtComuneRes.value == " ")) {
			    if (document.frmDomIscri.cmbProvRes.value != ""){
				    //alert("Se viene selezionata la Provincia di Residenza, il campo Comune di Residenza � obbligatorio!")
				    alert("Si ha seleccionado el Departamento de Residencia, el campo Ciudad de Residencia es obligatorio!")
				    document.frmDomIscri.txtComuneRes.focus() 
				    return false
			    }
		    }
				
		     Indirizzo=document.frmDomIscri.txtIndirizzo.value
		    if ((Indirizzo == "")||
			    (Indirizzo == " "))
		    {
			    alert("La direcci�n de la Residencia es obligatoria!")
			    document.frmDomIscri.txtIndirizzo.focus() 
			    return false
		    }	
				
				
    		    
    		/********************************************************
			*   @autor: IPTECHONOLOGIES                             +
			********************************************************/  
	      	var phone = $("#txtTelefono").val().trim();
	      	var mobile = $("#txtCellulare").val().trim();

	      	if((phone == "") && (mobile == "")){
	      		alert("Se debe diligenciar al menos uno de los dos tel�fonos!")
	      		$("#txtTelefono").focus() 			    
			    return false
	      	}

		    if (!IsNum(document.frmDomIscri.txtTelefono.value)) {
			    alert("El campo num�ro de Tel�fono debe ser num�rico")
			    document.frmDomIscri.txtTelefono.focus()
			    return false
		    }
		    if (eval(document.frmDomIscri.txtTelefono.value) == 0){
			    alert("El num�ro de tel�fono no puede ser cero")
			    document.frmDomIscri.txtTelefono.focus()
			    return false
		    }
    			
		    if (!IsNum(document.frmDomIscri.txtCellulare.value)) {
			    alert("El campo num�ro de Tel�fono celular debe ser num�rico")
			    document.frmDomIscri.txtCellulare.focus()
			    return false
		    }
		    if (eval(document.frmDomIscri.txtCellulare.value) == 0){
			    alert("El num�ro de tel�fono celular no puede ser cero")
			    document.frmDomIscri.txtCellulare.focus()
			    return false
		    }
    					
		}
		

        
	   //Email
	   if (document.frmDomIscri.txtEmail.value == "")
		      {
			      alert("El Correo electr�nico es  obligatorio!")
			      document.frmDomIscri.txtEmail.focus() 
			      return false
		      }  
	   
	   
	   if (document.frmDomIscri.txtEmail.value != ""){
		   pippo=ValidateEmail(document.frmDomIscri.txtEmail.value)
		
		   if  (pippo==false)
		   {
			   alert("El Correo electr�nico no es correcto!")
			   document.frmDomIscri.txtEmail.focus() 
			   return false
		   }
		}
		

		// Captcha sin datos
		/*document.frmDomIscri.strCAPTCHA.value=TRIM(document.frmDomIscri.strCAPTCHA.value)
		if (document.frmDomIscri.strCAPTCHA.value == "")
		{	
			alert("Por favor escriba el c�digo de seguridad")
			document.frmDomIscri.strCAPTCHA.focus() 
			return false
		}*/
				
		
		//Controlli Residenza
		


		// Terminos de uso
		if($('input[name="checkterminos"]:checked').val() != "on"){
			alert("Si est� de acuerdo con los t�rminos y condiciones, selecccione la casilla correspondiente.")
			$("#checkterminos").focus()
			return false	
		}		
	return true
}

</script>

</head>
<body>
<%

	sub CreaComboCOD_STAT_STUD(codice)
	dim stringa_frequenza,aFrequenza

		sSql="SELECT ID_CAMPO_DESC " &_
			"FROM DIZ_DATI " &_
			"WHERE " &_
			"ID_TAB='TISTUD' AND ID_CAMPO='COD_STAT_STUD'"

SSQL = TransformPLSQLToTSQL (SSQL) 
		set Rst=CC.Execute(sSql)
		stringa_frequenza=Rst("ID_CAMPO_DESC")
		aFrequenza=split(stringa_frequenza,"|")
	%>
		<select name="COD_STAT_STUD" class="textblack" id="COD_STAT_STUD" onChange="return ActivInputFields(this.value);">

	<%	for i=0 to Ubound(aFrequenza) step 2
			if aFrequenza(i) <> "3" then
				Response.Write "<option value='" & aFrequenza(i) & "'"
				if aFrequenza(i)=Codice then Response.Write " selected"
				Response.Write ">" & Ucase((aFrequenza(i+1))) & "</option>"
			end if
		next
	%>	
	</select>
	<%	Rst.Close
		set Rst=nothing
	end sub
%>

<%sub Inizio()%>
<table cellpadding="0" cellspacing="0" border="0">
	<tr>
		<td colspan = 4 class = "celdalogoministerio">
		    <img border = 0 src="/ba/images/logo_mintrabajo_253x45.png" border="0" alt="home page" height="45" width="253" valign= "top">
		</td>
	</tr>
    <tr>

    <td valign="top" align="left" >
        <a href="http://www.redempleo.gov.co/"  target=new> <img brder = 0 src="/ba/images/LogoSE.png" border="0" alt="home page" height="125" width="299" valign= "top"></a>
        <div align = "right">
		    <img border = 0 src="/ba/images/mensajecompatibilidad.png" alt ="Vista Compativilidad" border="0" height="35" width="257" valign= "top"/>
		</div>
    </td>
    <tr/>
</table>
<table cellpadding="0" cellspacing="0" width="500" border="0">
		<tr height="18">
				<td class="sfondomenu" height="18" width="67%">
				<span class="tbltext0"><b>&nbsp;SOLICITUD DE INSCRIPCI&Oacute;N</b></span></td>
				<td width="3%" background="/Ba/images/tondo_linguetta.gif">&nbsp;</td>
				<td valign="middle" align="right" class="tbltext1" width="50%" background="/Ba/images/sfondo_linguetta.gif">(*) campos obligatorios</td>
			</tr>
			<tr>
				<td class="sfondocomm" width="57%" colspan="3">
				    Diligencie la ficha con los datos y presione en el bot&oacute;n <b>enviar</b> para confirmar el registro.<br>
				    En caso de dificultad en el diligenciamiento de la informaci&oacute;n por favor <a href="http://pqrd.mintrabajo.gov.co/">cont&aacute;ctenos</a>
	</td>
			</tr>
			<tr height="2">
				<td colspan="3" class="sfondocomm" background="/BA/images/separazione.gif">
				</td>
			</tr>
	</table>
<%end sub%>	



<%Sub Msgetto(Msg)%>
	<br>
	<table align="center">
		<tr>
			<td class="tbltext3"><b><%=Msg%></b></td>
		</tr>	
		<tr>
			<td>&nbsp;</td>
		</tr>	
		<tr>
			<td align="center"><!--a HREF="javascript:history.back()"-->
			<a HREF="javascript:history.back()">
			<img SRC="/Ba/images/indietro.gif" border="0" WIDTH="55" HEIGHT="40"></a></td>
		</tr>
	</table>
<%End Sub%>


<%sub ImpPagina()%>
<form action="UTE_CnfIscrNoBando.asp" method="post" name="frmDomIscri" id="frmDomIscri"  onsubmit="return ControllaDati(this)">
	<input type="hidden" name="progetto" value="<%=Session("Progetto")%>">
	<input type="hidden" name="txtcodproj" value="<%=sProgetto%>">
	<input type="hidden" name="txtIdBando" value="<%=nBando%>">
	<input type="hidden" name="txtDescTimpr" value="<%=sDescTimprI%>">
<br>
<table border="0" cellpadding="0" cellspacing="0" width="500">
    <tr height="18">
		<td class="sfondomenu" width="67%" height="18">
			<span class="tbltext0"><b>&nbsp;DATOS PERSONALES</b></span></td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
		<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
    </tr>
 </table>
 <br>
<table border="0" cellpadding="2" cellspacing="2" width="500">

    <tr>
		<td align="left" colspan="2" nowrap class="tbltext1">
			<span class="tbltext1">
			<strong>Primer Nombre*</strong></span>
		</td>
		<td align="left" colspan="2" width="30%">
			<input class="textblacka" style="TEXT-TRANSFORM: uppercase; HEIGHT: 22px" size="35" maxlength="50" name="txtNome">
		</td>
    </tr>
   <tr>
      <td align="left" colspan="2" nowrap class="tbltext1"> <span class="tbltext1"> 
        <strong>Segundo Nombre</strong></span> 
      </td>
		<td align="left" colspan="2" width="60%">
			<input class="textblacka" style="TEXT-TRANSFORM: uppercase; WIDTH: 220px;" size="35" maxlength="50" name="txt_secondo_nome">		
		</td>
    </tr>
   <tr>
		<td align="left" colspan="2" nowrap class="tbltext1" width="100%">
			<span class="tbltext1">
				<strong>Primer Apellido*</strong></span>
		</td>
		<td align="left" colspan="2">
				<input class="textblacka" style="TEXT-TRANSFORM: uppercase; WIDTH: 220px; HEIGHT: 22px" size="35" maxlength="50" name="txtCognome">
		</td>
    </tr>
    <tr>
      	<td align="left" colspan="2" nowrap class="tbltext1"  width="100%"> 
      		<span class="tbltext1"><strong>Segundo Apellido</strong></span> 
        </td>
		<td align="left" colspan="2">
				<input class="textblacka" style="TEXT-TRANSFORM: uppercase; WIDTH: 220px; HEIGHT: 22px" size="35" maxlength="50" name="txt_secondo_cognome">		
		</td>
    </tr>
	<tr>
		<td align="middle" colspan="2" nowrap class="tbltext1">
				<p align="left">
					<strong>Sexo*</strong>
					<strong>&nbsp;</strong>
				</p>
			</td>
			<td align="left" colspan="2" width="60%">
				<select class="textblack" id="cmbSesso" name="cmbSesso">
					<option selected>
					<option value="F">FEMENINO
					<option value="M">MASCULINO
					</option>
				</select>
	        </td>
	</tr>	
	<tr>
          <td align="left" colspan="2" class="tbltext1"><strong></strong> <span class="tbltext1"><strong>Tipo Documento*</strong> </span></td>
	      <td align="left" colspan="2"><%
			sInt = "DOCST|0|" & date & "||cmbTipoDoc| and valore in('01','03') ORDER BY DESCRIZIONE"
			CreateCombo(sInt)
				%>          
				</td>
      </tr>   
  	<tr>
        <td align="left" colspan="2" class="tbltext1">
				<span class="tbltext1">
				<strong>N&uacute;mero* </strong>
				</span>
        </td>
        <td align="left" colspan="2" width="60%">
				<input class="textblacka" style="TEXT-TRANSFORM: uppercase; WIDTH:   167px; HEIGHT: 22px" size="14" maxlength="14" name="txtCodFisc">
        </td>
    </tr>
    <tr>
		<td align="left" colspan="2" nowrap class="tbltext1">	
				<span class="tbltext1">					
				<strong>Fecha de Nacimiento*</strong>(dd/mm/aaaa)								
		</td>	
		<td align="left" colspan="2" width="60%">
				<input class="textblacka fecha" style="TEXT-TRANSFORM: uppercase; WIDTH: 82px; HEIGHT: 22px" size="50" maxlength="10" name="txtDataNascita">			
		         <input type="hidden" name="txtOggi" value="<%=ConvDateToString(Date())%>">
		</td>
   </tr>
    <Table border="0" cellpadding="0" cellspacing="0" width="500">
    <tr height="18">
		<td class="sfondomenu" width="67%" height="18">
			<span class="tbltext0"><b>&nbsp;DATOS DE REGISTRO</b></span></td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
		<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
    </tr>
     <tr height="2">
	</tr>
  	<br>
 </table>
  <br>
	<table border="0" cellpadding="2" cellspacing="2" width="500">
    <tr >
		<td align="left" colspan="2" nowrap class="tbltext1" width="100%">
			<span class="tbltext1">
				<strong>Usuario*</strong></span>
		</td>
		<td align="left" colspan="2">
				<input class="textblacka" style="WIDTH: 220px;
					 HEIGHT: 22px" size="35" maxlength="15" name="txtLogin">
		</td>
    </tr>
    <tr>
		<td align="left" colspan="2" nowrap class="tbltext1" width="100%">
			<span class="tbltext1">
				<strong>Contrase&ntilde;a*</strong></span>
		</td>
		<td align="left" colspan="2">
				<input class="textblacka" style="WIDTH: 220px;
					 HEIGHT: 22px" size="35" maxlength="50" name="txtPassword" id="txtPassword" type="password">
		</td>
    </tr>
        <tr>
		<td align="left" colspan="2" nowrap class="tbltext1" width="100%">
			<span class="tbltext1">
				<strong>Confirmar Contrase&ntilde;a*</strong></span>
		</td>
		<td align="left" colspan="2">
				<input class="textblacka" style="TEXT-TRANSFORM: uppercase; WIDTH: 220px;
					 HEIGHT: 22px" size="35" maxlength="50" name="txtPasswordConfirmation" id="txtPasswordConfirmation" type="password">
		</td>
    </tr>
    </table>
         
    <!--inizio blocco RESIDENZA-->
    <Table border="0" cellpadding="0" cellspacing="0" width="500">
    <tr height="18">
		<td class="sfondomenu" width="67%" height="18">
			<span class="tbltext0"><b>&nbsp;DATOS DE CONTACTO</b></span></td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
		<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
    </tr>
     <tr height="2">
	</tr>
  	<br>
 </table>



<table border="0" cellpadding="2" cellspacing="2" width="500">
     <tr>
          <td align="left" colspan="2" class="tbltext1" width="100%">
          	<span class="tbltext1">
          		<strong>Pa&iacute;s de Residencia*</strong>
          	</span>
          </td>
	      <td align="left" colspan="2">
	      	<span class="tbltext">
            <%
				' @todo: Add call function load departments onchange Country ComboBox 
				sInt = "STATO|0|" & date & "||cmbPaisResidencia' onchange='cmbPaisResidencia_onchange()|ORDER BY DESCRIZIONE"
				
				
				CreateCombo(sInt)

				%>
            </span>
              <input type="hidden" name="txtProvRes" value="" />
              <% 'rsDocumento.Close
        set rsDocumento= nothing %>          </td>
      </tr>
      <tr>    
        <td align="left" colspan="2" class="tbltext1">
			<span class="tbltext1">
				<strong>Departamento*</strong>
			 </span>
		</td>
        <td align="left" colspan="2" width="60%">
		<%
			'''27/08/2003
			'''sInt = "PROV|0|" & date & "| |cmbProvRes' onchange='PulisciRes()|ORDER BY DESCRIZIONE"			
			   sInt = "PROV|0|" & date & "| |cmbProvRes' onchange='PulisciRes()|AND VALORE = 'CO' ORDER BY DESCRIZIONE"			
			'''FINE 27/08/2003
			CreateCombo(sInt)
		%>
        </td>
       </tr>
    <tr>		
    		<td align="left" colspan="2" nowrap class="tbltext1">
			<b>Ciudad*</b>
		</td>
		<td nowrap>
			<span class="tbltext">
			<input type="text" name="txtComuneRes" style="TEXT-TRANSFORM: uppercase;" class="textblacka" size="35" readonly value="<%=DescrComuneResid%>">
			<input type="hidden" id="txtComRes" name="txtComRes" value="<%=COM_RES%>">
<%
			NomeForm="frmDomIscri"
			CodiceProvincia="cmbProvRes"
			NomeComune="txtComuneRes"
			CodiceComune="txtComRes"
			Cap="NO"
			'Cap="txtCap"
%>
			<a href="Javascript:SelComune('<%=NomeForm%>','<%=CodiceProvincia%>','<%=NomeComune%>','<%=CodiceComune%>','<%=Cap%>')" ID="imgPunto1" name="imgPunto1" onmouseover="javascript:window.status='';return true"><img border="0" src="<%=Session("Progetto")%>/images/bullet1.gif"></a>
			</span>				
		</td>
	</tr>
   <tr>
        <td align="left" colspan="2" class="tbltext1">
				<span class="tbltext1">
				<strong>Direcci&oacute;n*</strong>
				</span>
        </td>
        <td align="left" colspan="2" width="60%">
				<input class="textblacka" style="TEXT-TRANSFORM: uppercase; HEIGHT: 22px" size="35" maxlength="50" name="txtIndirizzo">
        </td>
    </tr>
   
    <tr>
        <td align="left" colspan="2" class="tbltext1">
				<span class="tbltext1">
				<strong>N&uacute;mero de Tel&eacute;fono*</strong>
				</span>
        </td>
        <td align="left" colspan="2" width="60%">
				<input class="textblacka" style="WIDTH:167px; HEIGHT: 22px" size="50" maxlength="15" name="txtTelefono" id="txtTelefono">
        </td>
    </tr>
    <tr>
        <td align="left" colspan="2" class="tbltext1">
				<span class="tbltext1">
				<strong>N&uacute;mero de Tel&eacute;fono celular*</strong>
				</span>
        </td>
        <td align="left" colspan="2" width="60%">
				<input class="textblacka" style="WIDTH:167px; HEIGHT: 22px" size="50" maxlength="15" name="txtCellulare" id="txtCellulare">
        </td>
        <br>
    </tr>
    <tr>
        <td align="left" colspan="2" class="tbltext1">
				<span class="tbltext1">
				<strong>Correo Electr&oacute;nico*</strong>
				</span>
        </td>
        <td align="left" colspan="2" width="60%">
				<input class="textblacka" style="WIDTH:167px; HEIGHT: 22px" size="50" maxlength="100" name="txtEmail">
        </td>
    </tr>
    <tr>
        <td align="left" colspan="2" class="tbltext1">&nbsp;
	    </td>
    </tr>
    <!-- tr>
        <td align="left" colspan="2" class="tbltext1">
             <span class="tbltext1">
		     <strong>C&oacute;digo de seguridad*</strong>
		     </span>
        </td>
        <td width="182"><img src="../../../../../Util/aspcaptcha.asp" width="86" height="21" /></td>
    </tr-->
  	    <tr>
          <td height="2" align="left" colspan="4" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
      </tr>
</Table>
<table border="0" cellpadding="2" cellspacing="2" width="500">
    <tr> 
        <td align="left" colspan="2" class="tbltext1">
      		<span class="tbltext1">
			<strong>Ingrese las dos palabras en la casilla (incluya el espacio)*</strong>
			</span>
        </td>
        
		<td align="left" colspan="2">			
			 <script type="text/javascript" src="http://api.recaptcha.net/challenge?k=6LdHG-ISAAAAABd7utNm0Q262OmKhkKEns3Y8VYY"></script>

		    <noscript>
		    	<iframe src="http://api.recaptcha.net/noscript?k=6LdHG-ISAAAAABd7utNm0Q262OmKhkKEns3Y8VYY" height="300" width="500" frameborder="0"></iframe><br/>
		    	<textarea name="recaptcha_challenge_field" rows="3" cols="40"></textarea>
		    	<input type="hidden" name="recaptcha_response_field" value="manual_challenge"/>
		    </noscript>
		</td>
    </tr>
    <tr>
        <td colspan = "3">
            <span class="tbltext1">
            <input type="checkbox" id="checkterminos" name="checkterminos" onClick="if(this.checked==true){terminos.value='1'}else{terminos.value='0'}">
            <input name="terminos" type="text" value="0" 
                style="visibility: hidden; width: 0px; height: 0px; top: 0px; right: 0px; bottom: 0px; left: 0px;">
            <a href ="../../../../../BA/Testi/SistDoc/Documentale/DocLinkHP/Terminos de Uso.htm" target = "_blank">Acepto los t&eacute;rminos y las condiciones de uso</a></td>
            </span>
     </tr>
	</Table>
	<%end sub%>
	<%Sub Curriculum ()%>
	<!--inizio am blocco CARATTERISTICHE CURRICULARI -->
    <Table border="0" cellpadding="0" cellspacing="0" width="500">
    
</table>

	<input type="hidden" id="radConsenso" name="radConsenso" value="0">
<p>
<table border="0" cellpadding="1" cellspacing="1" width="370" align="center">
	 <tr>
        <td align="middle" colspan="2">
        <!--input type="image" name="Invia" src="<%=Session("Progetto")%>/images/conferma.gif" border="0" value="Conferma" onclick="return ControllaDati(this)"--> 
        <input type="image" name="Invia" src="<%=Session("Progetto")%>/images/conferma.gif" border="0" value="Conferma"> 
        <input type="hidden" name="graduatoria" value="<%=sGraduatoria%>">
                 <input type="hidden" name="bando" value="<%=nBando%>">
                 <input type="hidden" name="gruppo" value="36">
	            <input type="hidden" name="descbando" value="<%=sDescBando%>">
	            <!--input type="text" name="txtDescTimpr" value="<%=sDescTimprI%>"-->

        </td>
        <td nowrap align="left">
        <%'16/10/2007 GESTIONE TASTO INDIETRO IN AREA PUBBLICA
        
        IF session("login") = "" then %>
			<!--jhamon<a HREF="javascript:history.back()">-->
			<a HREF="http://www.redempleo.gov.co">
        <%ELSE %>
						<!---<a HREF="javascript:history.back()">-->
						<a HREF="http://www.redempleo.gov.co">

		<%END IF %>
			<img SRC="<%=Session("Progetto")%>/images/indietro.gif" border="0">
		</td>
     </tr>
     <tr>
  
     </tr>
</table>
</p>
</form>

<%end sub

%>
<%sub Fine()%>
<!--#include virtual="/strutt_coda2.asp"-->
<!--#include Virtual = "/include/CloseConn.asp"-->	
<%end sub%>
<!-- ************** ASP Fine *************** -->

<!-- ************** MAIN Inizio ************ -->
<%
dim sDataIsc, sEsitoBando, nCont
dim nIdProgetto,sGruppo,sDescBando, sProgetto
dim nBando, nTime, nBypassdata, lGrado, sGraduatoria, DataCateg, DataOdierna, sDescTimprI

sDataIsc = Now()

nBando=clng(Request("txtIdBando"))
nTime = convdatetodb(Date)
nBypassdata = Request("Bypassdata")
lGrado = Request("lGrado")

		Dim aMenu(0)
		aMenu(0) = Session("Progetto") & "/home.asp"
		Session("Indice") = aMenu
		Session("Menu") = aMenu(0)
		nPos = 0

    Inizio()

		sDescTimpr = DecCodVal("TIMPR", "0", "", sCodTimpr, 1)  
		sDataSis = ConvDateToString(Now())
		ImpPagina()		
		Curriculum()

Fine()
%>
<!-- ************** MAIN Fine ************ -->

</body>
</html>