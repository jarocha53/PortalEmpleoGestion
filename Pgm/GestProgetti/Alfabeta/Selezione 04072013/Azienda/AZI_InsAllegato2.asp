<%'Option Explicit 
  ' Response.ExpiresAbsolute = Now() - 1 
  ' Response.AddHeader "pragma","no-cache"
  ' Response.AddHeader "cache-control","private"
  ' Response.CacheControl = "no-cache"
%>
<%if session("login") = "" then%>
	<!-- #include virtual="/ARTIGIANI/strutt_testaART1.asp"-->
<%else%>
	<!-- #include virtual="/strutt_testa1.asp"-->
<%end if%>
<!--#include virtual ="/include/openconn.asp"-->
<!--#include virtual ="/util/portallib.asp"-->

<%

Session("menu")= ""
%>
<!-- ************** Javascript Inizio ************ -->
<!--include del file per fare i controlli sulle stringhe-->
<script LANGUAGE="Javascript" src="/Include/ControlString.inc"></script>
<!--include del file per fare i controlli sulla validit� delle date-->
<script LANGUAGE="Javascript" src="/Include/ControlDate.inc"></script>

<!--include del file per fare i controlli sulla numericit� dei campi-->
<script LANGUAGE="Javascript" src="/Include/ControlNum.inc"></script>

<!--include del file per fare i controlli sulla validit� del codice fiscale-->
<script LANGUAGE="Javascript" src="/Include/ControlCodFisc.inc"></script>

<script LANGUAGE="Javascript" src="/Include/help.inc"></script>
<script LANGUAGE="Javascript" src="/include/SelComune.js"></script>
<script LANGUAGE="Javascript" src="/ARTIGIANI/include/SelComuneRes.js"></script>
<script LANGUAGE="Javascript" src="/ARTIGIANI/include/SelSettore.js"></script>
<script LANGUAGE="Javascript" src="/ARTIGIANI/include/SelTipoConsulenza.js"></script>
<script LANGUAGE="Javascript" src="Controlli.js"></script>

<script LANGUAGE="Javascript">
<!-- ************** ASP Inizio *************** -->

<!--#include virtual = "/include/DecCod.asp"-->
<!--#include virtual = "/include/ControlDateVB.asp"-->
<!--#include virtual = "/util/dbutil.asp"-->

function SelCameraCom()
{ 
 windowArea = window.open ('SelCameraCom.asp' ,'Info','width=422,height=450,Resize=No,Scrollbars=yes')	
 }

function PulisciRes()
{
	document.frmDomIscri.txtComuneRes.value = ""
	document.frmDomIscri.txtSettore.value = ""
	document.frmDomIscri.txtTipoCons.value = ""
}

function IsNumIscReg(sNumIscReg) {
	var t
	
	if (sNumIscReg.length != 11) {
		alert("Il Numero del registro deve essere di 11 caratteri")
		return false
	}
	for (t=0; t<11;t++) {

		if (t==6){
			if (sNumIscReg.charAt(t)!= "/"){
				alert("Il formato corretto del Numero del registro � : 999999/9999")
				return false
			}
			t++ 
		}
		if (!IsNum(sNumIscReg.charAt(t))) {
				alert("Il Numero del registro deve essere numerico")
				return false
		}
	}
	return true
}

function ControllaDati(){

			if(document.frmDomIscri.cmbCodTimpr.value == ""){
				alert("Il campo Tipologia Impresa � obbligatorio")
				document.frmDomIscri.cmbCodTimpr.focus()
				return false
			}
			if (TRIM(document.frmDomIscri.txtRagSoc.value) == ""){
				alert("Il campo Ragione Sociale � obbligatorio")
				document.frmDomIscri.txtRagSoc.focus()
				return false
			}
			if (document.frmDomIscri.cmbCodSet.value == ""){
				alert("Il campo Codice Settore � obbligatorio")
				document.frmDomIscri.cmbCodSet.focus()
				return false
			}
			if (TRIM(document.frmDomIscri.txtPartIva.value) == ""){
				alert("E' obbligatorio compilare la Partita Iva")
				document.frmDomIscri.txtPartIva.focus()
				return false
			}
			if (document.frmDomIscri.txtPartIva.value.length != 11){
				alert("Il campo Partita Iva � formalmente errato")
				document.frmDomIscri.txtPartIva.focus()
				return false
			} 
			if (!ValidateInputNumber(document.frmDomIscri.txtPartIva.value) == true){
				alert("Il campo Partita Iva � formalmente errato")
				document.frmDomIscri.txtPartIva.focus()
				return false
			} 
			if (document.frmDomIscri.txtCodFis.value.length != 0 && document.frmDomIscri.txtCodFis.value.length != 16){
				alert("Il campo Codice Fiscale � formalmente errato")
				document.frmDomIscri.txtCodFis.focus()
				return false
			} 
			/*if (document.frmDomIscri.txtCodFis.value.length == 16 && ControllChkCodFisc(TRIM(document.frmDomIscri.txtCodFis.value)) == false){
				alert("Il Codice Fiscale non � valido")
				document.frmDomIscri.txtCodFis.focus()
				return false
			}*/
			blank = " ";
			if (!ChechSingolChar(document.frmDomIscri.txtCodFis.value,blank))
			{
				alert("Il campo Codice Fiscale non pu� contenere spazi bianchi")
				document.frmDomIscri.txtCodFis.focus();
				return false;
			}
			
			if (!ValidateInputStringWithNumber(document.frmDomIscri.txtCodFis.value))
			{
				alert("Il campo Codice fiscale � formalmente errato")
				document.frmDomIscri.txtCodFis.focus()
				return false;
			}
			
			
			
			if(document.frmDomIscri.cmbCodSede.value == ""){
				alert("Il campo Tipo Sede � obbligatorio")
				document.frmDomIscri.cmbCodSede.focus()
				return false
			}
			if(TRIM(document.frmDomIscri.txtIndirizzo.value) == ""){
				alert("Il campo Indirizzo � obbligatorio")
				document.frmDomIscri.txtIndirizzo.focus()
				return false
			}
			if(document.frmDomIscri.cmbProvRes.value == ""){
				alert("Il campo Provincia � obbligatorio")
				document.frmDomIscri.cmbProvRes.focus()
				return false
			}
			if(TRIM(document.frmDomIscri.txtComuneRes.value) == ""){
				alert("Il campo Comune � obbligatorio")
				document.frmDomIscri.txtComuneRes.focus()
				return false
			}
			 
			//telefono	
		
		if (TRIM(document.frmDomIscri.txtNumTel.value) == "") {
			alert("Il campo Telefono � obbligatorio")
			document.frmDomIscri.txtNumTel.focus() 
			return false
		}
		if (TRIM(document.frmDomIscri.txtNumTel.value) != "") {
			if (!ValidateInputNumber(document.frmDomIscri.txtNumTel.value)) {
				alert("Il campo Telefono deve essere numerico")
				document.frmDomIscri.txtNumTel.focus() 
				return false
			}
			if (eval(document.frmDomIscri.txtNumTel.value) == 0){
				alert("Il numero di telefono non pu� essere zero")
				document.frmDomIscri.txtNumTel.focus()
				return false
			}
		}
		
		// email
		if(TRIM(document.frmDomIscri.txtEMail.value) == ""){
			alert("Il campo E-mail � obbligatorio")
			document.frmDomIscri.txtEMail.focus()
			return false
		}
		document.frmDomIscri.txtEMail.value = TRIM(document.frmDomIscri.txtEMail.value)
		if (document.frmDomIscri.txtEMail.value != "") {
			if (!ValidateEmail(document.frmDomIscri.txtEMail.value)) {
				alert("Il campo E-mail � formalmente errato")
				document.frmDomIscri.txtEMail.focus() 
				return false
			}
		}
		
		// Numero dipendenti
				
		if (TRIM(document.frmDomIscri.txtNumDip.value) == "") {
			alert("Il campo Numero Dipendenti a Tempo Indeterminato � obbligatorio")
			document.frmDomIscri.txtNumDip.focus() 
			return false
		}
		if (TRIM(document.frmDomIscri.txtNumDip.value) != "") {
			if (!ValidateInputNumber(document.frmDomIscri.txtNumDip.value)) {
				alert("Il campo Numero Dipendenti a Tempo Indeterminato deve essere numerico")
				document.frmDomIscri.txtNumDip.focus() 
				return false
			}
			//dopo riunione con biasion deve accettare il valore zero
			/*if (eval(document.frmDomIscri.txtNumDip.value) == 0){
				alert("Il numero di Numero Dipendenti a Tempo Indeterminato non pu� essere zero")
				document.frmDomIscri.txtNumDip.focus()
				return false
			}*/
		}
		
		// richiedente
			if(TRIM(document.frmDomIscri.txtRichied.value) == ""){
				alert("Il campo Descrizione del Richiedente � obbligatorio")
				document.frmDomIscri.txtRichied.focus()
				return false
			}
		// Settore di appartenenza
				
		if(TRIM(document.frmDomIscri.txtSettore.value) == ""){
			alert("Il campo Settore di Appartenenza � obbligatorio")
			document.frmDomIscri.txtSettore.focus()
			return false
		}
			
		

	//''''''''''''''''''''''''''''''''''''
			
		// obiettivi
		
			if(TRIM(document.frmDomIscri.txtObiettivo.value) == ""){
				alert("Il campo Descrizione dell'Obiettivo � obbligatorio")
				document.frmDomIscri.txtObiettivo.focus()
				return false
			}
			
		// tipologia di assistenza
		if(TRIM(document.frmDomIscri.txtTipoCons.value) == ""){
			alert("Il campo Tipologia di Assistenza Tecnica � obbligatorio")
			document.frmDomIscri.txtTipoCons.focus()
			return false
		}
		
		if(TRIM(document.frmDomIscri.txtTipoCons.value) == "ALTRO"){
			if(TRIM(document.frmDomIscri.txtTipoConsAltro.value) == ""){
			alert("Se si seleziona la Tipologia di Assistenza Tecnica 'ALTRO' bisogna specificare nel campo 'Altra Tipologia' la tipologia di assistenza tecnica")
			document.frmDomIscri.txtTipoConsAltro.focus()
			return false
			}
		}
		
		if(TRIM(document.frmDomIscri.txtTipoCons.value) != "ALTRO"){
			if(TRIM(document.frmDomIscri.txtTipoConsAltro.value) != ""){
			alert("Se si seleziona una Tipologia di Assistenza Tecnica diversa da 'ALTRO' non bisogna specificare nel campo 'Altra Tipologia' la tipologia di assistenza tecnica")
			document.frmDomIscri.txtTipoConsAltro.focus()
			return false
			}
		}
		


		//Obbligatoriet� del numero di Iscrizione Albo delle Imprese
			if (TRIM(document.frmDomIscri.txtNumAlbo.value) == ""){
				alert("E' obbligatorio specificare il Numero di Iscrizione all'Albo delle Imprese Artigiane")
				document.frmDomIscri.txtNumAlbo.focus()
				return false
			} 
			
			if (TRIM(document.frmDomIscri.txtNumAlbo.value) != "") {
				if (!ValidateInputNumber(document.frmDomIscri.txtNumAlbo.value)) {
					alert("Il campo Numero di Iscrizione all'Albo delle Imprese Artigiane deve essere numerico")
					document.frmDomIscri.txtNumAlbo.focus() 
					return false
				}
				if (eval(document.frmDomIscri.txtNumAlbo.value) == 0){
					alert("Il Numero di Iscrizione all'Albo delle Imprese Artigiane non pu� essere zero")
					document.frmDomIscri.txtNumAlbo.focus()
					return false
				}
			}
	
			//Obbligatoriet� del numero di Iscrizione Camera di Commercio e Comune
			if (TRIM(document.frmDomIscri.txtPrvCameraCom.value) == ""){
				alert("E' obbligatorio specificare la Camera di Commercio di appartenenza")
				document.frmDomIscri.txtPrvCameraCom.focus()	
				return false
			} 
			if (TRIM(document.frmDomIscri.txtNumReg.value) == ""){
				alert("E' obbligatorio specificare il Numero di Iscrizione alla Camera di Commercio")
				document.frmDomIscri.txtNumReg.focus()
				return false
			} 
			
			
			// controllo correttezza formale del numero registro
			document.frmDomIscri.txtNumReg.value = TRIM(document.frmDomIscri.txtNumReg.value)		
			if (!document.frmDomIscri.txtNumReg.value == "")	
			{
				if (document.frmDomIscri.txtNumReg.value != "")
				{
					if (!IsNumIscReg(document.frmDomIscri.txtNumReg.value))
					{
						document.frmDomIscri.txtNumReg.focus()
						return false				
					}
				}
			}
			
			//ccnl
			if(document.frmDomIscri.cmbCodCcnl.value == ""){
				alert("Il campo Codice CCNL � obbligatorio")
				document.frmDomIscri.cmbCodCcnl.focus()
				return false
			}
			
		// obbligatoriet� Ref Sede Nome e Cognome
		
			if(document.frmDomIscri.txtRefSedeNome.value == ""){
				alert("Il campo Nome per il Referente Sede � obbligatorio")
				document.frmDomIscri.txtRefSedeNome.focus()
				return false
			}
			
			if(document.frmDomIscri.txtRefSedeCognome.value == ""){
				alert("Il campo Cognome per il Referente Sede � obbligatorio")
				document.frmDomIscri.txtRefSedeCognome.focus()
				return false
			}
			
			
		//Controlli su LOGIN: posso inserire solo numeri e / o lettere
			
		 frmDomIscri.txtLogin.value=TRIM(frmDomIscri.txtLogin.value)
		var anyString = frmDomIscri.txtLogin.value;
		 
		if (frmDomIscri.txtLogin.value == ""){
			alert("Login obbligatoria")
			frmDomIscri.txtLogin.focus() 
			return false
		}
					
		for (var i=0; i<=anyString.length-1; i++){
			if(((anyString.charAt(i) >= "A") && (anyString.charAt(i) <= "Z")) || 
				((anyString.charAt(i) >= "a") && (anyString.charAt(i) <= "z")) || 
				((anyString.charAt(i) >= "0") && (anyString.charAt(i) <= "9")))
			{
			}
			else
			{		
				frmDomIscri.txtLogin.focus();
			 	alert("Inserire nel campo Login un parametro valido!");
				return false
			}		
		}
		
		if (frmDomIscri.txtPassword.value == "")
		{
			alert("Il campo Password � obbligatorio!")
			frmDomIscri.txtPassword.focus() 
			return false
		}
		
		if (frmDomIscri.txtPassword.value.length < 8)
		{
			alert("Il campo Password deve essere lungo almeno 8 caratteri!")
			frmDomIscri.txtPassword.focus() 
			return false
		}
		
		if (frmDomIscri.txtConfPassword.value == "")
		{
			alert("Il campo Conferma Password � obbligatorio!")
			frmDomIscri.txtConfPassword.focus() 
			return false
		}
		
		if (frmDomIscri.txtPassword.value != frmDomIscri.txtConfPassword.value)
		{
			alert("Il campo Conferma Password deve essere uguale al campo Password")
			frmDomIscri.txtPassword.focus() 
			return false
		}
		
		
		
			if (document.frmDomIscri.radConsenso[0].checked == false) {
				alert("Per poter concludere la transazione � necessario Accettare il `Consenso al trattamento dei dati personali`")
				document.frmDomIscri.radConsenso[0].focus()
				return false
			}
		//	alert ("SCRIVE!")
}

</script>
<%	
'-------------------------------------------------------------------------------------------------------------------------------


Sub Titolo(sTitolo,sDescTitolo) %>
    <table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
		<tr height="18">
			<td class="sfondomenu" width="67%" height="18"><span class="tbltext0"><b>&nbsp;<%=sTitolo%></b></span></td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
			<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
		</tr>
	</table>
	<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
		<tr>
			<td align="left" class="sfondocomm"><%=sDescTitolo%></td>
		</tr>
		<tr height="2">
			<td class="sfondocomm" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>
	<br>
<%
end sub

Sub Msgetto(Msg)

%>
	<br>
	<table align="center">
		<tr>
			<td class="tbltext3"><b><%=Msg%></b></td>
		</tr>	
		<tr>
			<td>&nbsp;</td>
		</tr>	
		<tr>
			<td align="center"><a HREF="javascript:history.back()"><img SRC="/ARTIGIANI/images/indietro.gif" border="0" WIDTH="55" HEIGHT="40"></a></td>
		</tr>
	</table>
<%
End Sub

'-------------------------------------------------------------------------------------------------------------------------------

sub Inizio()
%>
	
	<table cellpadding="0" cellspacing="0" width="500" border="0">
		<tr height="18">
			<td class="sfondomenu" height="18" width="67%">
			<span class="tbltext0"><b>&nbsp;DOMANDA DI ISCRIZIONE</b></span></td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
			<td valign="middle" align="right" class="tbltext1" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">(*) campi obbligatori</td>
		</tr>
		<tr>
			<td class="sfondocomm" width="57%" colspan="3">
				Modulo di iscrizione 
			<%if  sEsitoBando = "OK" then %>
			      per il bando&nbsp;<b><%=sDescBando%></b><br>  
			      Premere <b>Invia</b> per salvare. 
			<%end if%>
			<a href="Javascript:Show_Help('/Pgm/help/GestProgetti/Alfabeta/Selezione/SEL_InsDomIscr')" onmouseover="javascript:window.status=' '; return true"><img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
			</td>
		</tr>
		<tr height="2">
			<td colspan="3" class="sfondocomm" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>
	
	<form method="post" name="frmDomIscri" onsubmit="return ControllaDati(this)" action="AZI_CnfAllegato2.asp">
	<input type="hidden" name="COD_STDIS" value="<%=CodiceStato%>">
	<input type="hidden" name="txtScadenzaOld" value="<%=sSCadenza%>">
	<input type="hidden" name="txtoggi" value="<%=ConvDateToString(Date())%>">
<%
end sub

'------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

sub ImpPagina()
'	Response.Write "AMBRA " & Session("Progetto")
    dim sInt 
    
	       sCommento =  "Presta <b>molta attenzione</b> a quanto inserisci:<br>" &_ 
				        "Indicare di seguito i dati dell'azienda."  
				        
	       call Titolo("Dati Azienda",sCommento)
	    %>
	   
		<table width="500" border="0" cellspacing="2" cellpadding="1">
		<!-------------dati simo--->
		
		<tr>
			<td align="middle" colspan="2" nowrap>
				<p align="left">
					<span class="tbltext1" Style="CURSOR:HELP" title="Scegliere la tipologia d'impresa che corrisponde all'azienda"><b>Tipologia Impresa*<b></span>
				</p>
			</td>
			<td align="left">
<%
					sTimpr = "TIMPR|0|" & date & "|" & sTipoImp & "|cmbCodTimpr|AND CODICE <> '03' ORDER BY DESCRIZIONE"
		'		else
		'			sTimpr = "TIMPR|0|" & date & "|" & sTipoImp & "|cmbCodTimpr' onchange='InviaProv(this.value)' '|ORDER BY DESCRIZIONE"
		'		end if									
				CreateCombo(sTimpr)				
%>	
			</td>
		</tr>    
        <tr>
			<td align="middle" colspan="2" nowrap>
				<p align="left">
				<span class="tbltext1" Style="CURSOR:HELP" title="Inserire Denominazione Azienda"><b>Ragione sociale*<b></span>
				</p>
			</td>
            <td align="left" colspan="2" width="57%"> 
				<p align="left">
					<input style="TEXT-TRANSFORM:uppercase" WIDTH="220px" class="textblack" size="40" maxlength="50" name="txtRagSoc">					
				</p>
            </td>
		</tr>
        <tr>
			<td align="middle" colspan="2" nowrap>
				<p align="left">
				<span class="tbltext1" Style="CURSOR:HELP" title="Inserire forma giuridica che si adatta alla tipologia dell'attivit� esercitata."><b>Forma Giuridica*</b></span>&nbsp;
				</p>
            </td>
            
			<td align="left" colspan="2" width="57%"> 
				<p align="left">
				<span class="table">
<%
				sInt = "FGIUR|" & sIsa & "|" & date() & "| |cmbCodForm' | ORDER BY DESCRIZIONE"						
				CreateCombo(sInt)
%>
				</span>
				</p>
            </td>
		</tr>
        <tr>
			<td align="left" colspan="2">
				<p align="left">
					<span class="tbltext1" Style="CURSOR:HELP" title="Indicare classificazione delle attivit� economiche secondo estratto Istat."><b>Settore*</b></span>					
				</p>
			</td>

			<td align="left" colspan="2" width="57%"> 
				<p align="left">
					<span class="table">				
<%
				sSQL = "SELECT ID_SETTORE,subStr(DENOMINAZIONE,1,62) As DENOMINAZIONE from Settori ORDER BY DENOMINAZIONE"

				set rsSett = CC.Execute(sSQL)
				Response.Write "<SELECT ID='cmbCodSet' name='cmbCodSet' class='textblack'><OPTION value=></OPTION>"

				do while not rsSett.EOF 

					Response.Write "<OPTION "

					Response.write "value ='" & clng(rsSett("ID_SETTORE")) & _
						"'> " & rsSett("DENOMINAZIONE")  & "</OPTION>"
					rsSett.MoveNext 
				loop
	
				Response.Write "</SELECT>"

				rsSett.Close	
				set rsSett = nothing
%>					
					</span>
				</p>
			</td>
		</tr>
		
		<tr height="2">
		
			<td class="sfondocomm" colspan="4" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
		</tr>
		<tr>
			<td colspan="4" class="textblack">
				 <br>
			</td>
		</tr>
        <tr>
			<td align="left" colspan="2">
				<p align="left">
					<span class="tbltext1"><b>Partita IVA*</b></span>
				</p>
            </td>            
			<td align="left" colspan="2" width="57%"> 
				<p align="left">					
					<input class="textblack" size="16" maxlength="11" name="txtPartIva">					
				</p>
            </td>
        </tr>
        <tr>
			<td align="left" colspan="2">
				<p align="left">
					<span class="tbltext1"><b>Codice fiscale</b></span>					 
				</p>
            </td>            
			<td align="left" colspan="2" width="57%"> 
				<p align="left">
					<input class="textblack" size="22" maxlength="16" name="txtCodFis" onkeyup="this.value=this.value.toUpperCase()">					
				</p>
            </td>
        </tr>
		</table>
		<table width="500" border="0" cellspacing="2" cellpadding="1">	    
		<tr>
			<td align="left" class="textblack" colspan="1">
				Indicare i dati delle Unit� Produttive con cui l'azienda partecipa all'avviso pubblico
			</td>
	    </tr>
	    </table>
	    <table width="500" border="0" cellspacing="2" cellpadding="1">	    
        <tr>
			<td align="middle" colspan="2" nowrap>
				<p align="left">
					<span class="tbltext1" Style="CURSOR:HELP" title="Indicare che funzione si svolge nella sede indicata."><b>Tipo sede*</b></span>
				</p>		
            </td>            
			<td align="left" colspan="2"> 
				<p align="left">
					<span class="table">
<%
					sInt = "TSEDE|" & sisa & "|" & date & "|" & sCodSede & "|cmbCodSede| ORDER BY DESCRIZIONE"			
					CreateCombo(sInt)
%>
				</p>
            </td>
        </tr>
        <tr>
			<td align="left" colspan="2">
				<p align="left">
					<span class="tbltext1"><b>Indirizzo*</b></span>
				</p>
			</td>			
			<td align="left" colspan="2"> 
				<p align="left">				
					<input style="TEXT-TRANSFORM:uppercase" WIDTH="220px" class="textblack" size="40" maxlength="50" name="txtIndirizzo">
				</p>
			</td>
        </tr>               
        </table>

	    
	    <table width="500" border="0" cellspacing="2" cellpadding="1">
	    
	    <tr>		
			<td align="left" class="tbltext1" colspan="2">
				<b>Provincia*&nbsp;</b>
			</td>			
			<td align="left">
							
<%
	
		set rsProvRes = Server.CreateObject("ADODB.Recordset")

		sSQLProvRes = " SELECT distinct (prv) " &_
					" from COMUNE_PARCHI " 
				
			rsProvRes.Open sSQLProvRes, CC, 3

			if not rsProvRes.EOF then
				
			Response.Write "<Select class='textblack' name='cmbProvRes' onchange='PulisciRes()'"
					Response.Write ">" 
					Response.Write "<Option></Option>" 
				do while not rsProvRes.EOF
				sProv = DecCodVal("PROV", "0", "", rsProvRes("prv"), 1)
				
				
					
					Response.Write "<OPTION "
					Response.write "value ='" & rsProvRes("prv") & _
					"'> " & sProv  & "</OPTION>"
					
				
				
				
				
				 rsProvRes.movenext
				loop 
				Response.Write "</Select>" 
			end if
%>
			</td>
		</tr>		
		<!--tr>					<td align="left" class="tbltext1" width="30%">				<b>Provincia di Residenza*</b>			</td>						<td align="left"-->				
<%



	'		sInt = "PROV|0|" & date & "||cmbProvRes' onchange='PulisciRes()| ORDER BY DESCRIZIONE"			
	'		CreateCombo(sInt)	
%>
			<!--/td>		</tr-->		
		<tr>		
			<td align="left" class="tbltext1" colspan="2">
				<b>Comune*&nbsp;</b>
			</td>		
			
		
			<td nowrap>
				<span class="tbltext">
				<input type="text" name="txtComuneRes" style="TEXT-TRANSFORM: uppercase;" class="textblack" size="35" readonly value="<%=DescrComuneResid%>">
				<input type="hidden" id="txtComRes" name="txtComRes" value="<%=COM_RES%>">
<%
				NomeForm="frmDomIscri"
				CodiceProvincia="cmbProvRes"
				NomeComune="txtComuneRes"
				CodiceComune="txtComRes"
				Cap="txtCapRes"	

				
%>
				<input type="HIDDEN" name="txtCapRes" class="textblack" value="<%=CAP_RES%>" size="5" maxlength="5">
				<a href="Javascript:SelComuneRes('<%=NomeForm%>','<%=CodiceProvincia%>','<%=NomeComune%>','<%=CodiceComune%>','<%=Cap%>')" ID="imgPunto1" name="imgPunto1" onmouseover="javascript:window.status='';return true"><img border="0" src="<%=Session("Progetto")%>/images/bullet1.gif"></a>
				</span>				
			</td>
		</tr>		
	    
	</table>
	
	   <table width="500" border="0" cellspacing="2" cellpadding="1">
	    <tr>
			<td align="left" class="textblack" colspan="2">
			Indicare un recapito telefonico presso cui gli operatori possono contattare direttamente per comunicazioni urgenti.
			E' necessario scrivere di seguito tutti i numeri che lo compongono, senza aggiungere spazi o altri separatori.<br>
			Segnalaci il tuo indirizzo di posta elettronica personale, se lo hai, in modo da agevolare i contatti.
			</td>
	    </tr>
	    <tr>
			<td align="left" class="tbltext1" width="23%">
				<b>N� Telefono*</b>
	        </td>
	        <td align="left">
				<input style="TEXT-TRANSFORM: uppercase;" size="25" maxlength="20" class="textblack" name="txtNumTel">
	        </td>
	    </tr>
	    <tr>
			<td align="left" class="tbltext1" width="23%">
	   			<b>E-Mail*</b>
	        </td>
	        <td align="left">
		   		<input size="25" maxlength="100" class="textblack" name="txtEMail">
	        </td>
	    </tr>
	<!-- Numero dipendenti a tempo indeterminato--->
	 <tr>
			<td align="left" class="tbltext1" width="50%">
	   			<b>NUMERO DI DIPENDENTI A TEMPO INDETERMINATO:</b>
	        </td>	
			<td align="left" colspan="2"> 
				<p align="left">				
					<input style="TEXT-TRANSFORM:uppercase" WIDTH="220px" class="textblack" size="25" maxlength="5" name="txtNumDip">
				</p>
			</td>
        </tr>      
	</table>	
	
<!-- ----------------------------------------------------------- -->	   
	<table width="500" border="0" cellspacing="2" cellpadding="1">   
	<hr>
	<tr>
		<td colspan="4" class="tbltext1">
			<b> RICHIEDENTE* </b>
		</td>
	</tr>
		<tr>
			<td colspan="4" class="textblack">
				

				<br>DESCRIZIONE (MAX 10 RIGHE) DELLA STORIA DEL SOGGETTO, IL SETTORE IN CUI OPERA, IL TIPO DI ATTIVITA'
				SVOLTA, I PRODOTTI, ETC.
			</td>
		</tr>
		
	</table>
	
	<!-- ----------------------------------------------------------- -->	   
	<br>
	<table width="500" border="0" cellspacing="2" cellpadding="1" align="center">   
			<tr> 
					<span class="tbltext1">- Utilizzabili 
					<b><label name="NumCaratteri" id="NumCaratteri1">600</label></b>
					 caratteri  -
					</span>
				</td>
			</tr>
			<tr> 
				<td align="left" valign="middle" colspan="4">
					<textarea name="txtRichied" onKeyup="JavaScript:CheckLenTextArea(document.frmDomIscri.txtRichied,NumCaratteri1,600)" CLASS="boxcommunity" cols="60" rows="10"></textarea>
				</td>
			</tr>
			
	</table>
	<br>	
	<!--SM    INIZIO  Settore di Appartenenza-->		
	<table width="500" border="0" cellspacing="2" cellpadding="1">
	<tr height="2">	
		<td class="sfondocomm" colspan="4" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
	</tr>
	<tr>
		<td align="left" class="textblack" colspan="4">
		Indicare il Settore di Appartenenza dell'azienda con cui l'azienda 
		partecipa all'avviso pubblico.
		</td>
	</tr>   
	<br>
	<tr>
		<td class="tbltext1">

			<b> SETTORE DI APPARTENENZA* </b>
		</td>
	
			<td nowrap>
				<span class="tbltext">
				<input type="text" name="txtSettore" style="TEXT-TRANSFORM: uppercase;" class="textblack" size="35" readonly>
				<input type="hidden" id="txtSettoreHid" name="txtSettoreHid">
<%
				NomeForm="frmDomIscri"
				CodiceProvincia="cmbProvRes"
				NomeSettore="txtSettore"
				CodiceSettore="txtSettoreHid"
				
%>
				<a href="Javascript:SelSettore('<%=NomeForm%>','<%=CodiceProvincia%>','<%=NomeSettore%>','<%=CodiceSettore%>')" ID="imgPunto1" name="imgPunto1" onmouseover="javascript:window.status='';return true"><img border="0" src="<%=Session("Progetto")%>/images/bullet1.gif"></a>
				</span>				
			</td>
		</tr>	
	</table>  		
<!--SM    INIZIO  Disponiblit� Tirocinanti-->		
	<table width="500" border="0" cellspacing="2" cellpadding="1">	
	<tr height="2">	
		<td class="sfondocomm" colspan="4" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
	</tr>
	<tr>
		<td align="left" class="textblack" colspan="4">
		La presente dichiarazione non rileva ai fini dell'assegnazione del contributo e non �
		in alcun modo vincolante per l'azienda.
		</td>
	</tr>   
	<br> 
	<tr>
			<td class="tbltext1" valign="left" align="left">
				<b>DISPONIBILITA' AD ACCOGLIERE TIROCINANTI:<b>
			</td>
			<td colSpan="2">
				<center>
				<input type="radio" value="S" id="radTirocinanti" name="radTirocinanti"><span class="tbltext">&nbsp;SI&nbsp;&nbsp;&nbsp;&nbsp;</span>
				<input type="radio" value="N" CHECKED id="radTirocinanti" name="radTirocinanti"><span class="tbltext">&nbsp;NO</span>
				<input type="hidden" name="sControllo" value="2">
				</center>
			</td>
	</tr>
	<br>
	<tr height="2">	
		<td class="sfondocomm" colspan="4" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
	</tr>
	<br> 
	<tr>
		<td class="tbltext1" valign="left" align="left">
			<b>Disponibilit� a partecipare ad attivit� future del presente Progetto che coinvolgeranno le Filiere<b>
		</td>
		<td colSpan="2">
			<center>
			<input type="radio" value="S" id="radAttivita" name="radAttivita"><span class="tbltext">&nbsp;SI&nbsp;&nbsp;&nbsp;&nbsp;</span>
			<input type="radio" value="N" CHECKED id="radAttivita" name="radAttivita"><span class="tbltext">&nbsp;NO</span>
			<input type="hidden" name="sControllo" value="2">
			</center>
		</td>
	</tr>
	</table>
	
<!-- ----------------------------------------------------------- -->	   
	<table width="500" border="0" cellspacing="2" cellpadding="1">   
	<hr>
	<tr>
		<td colspan="4" class="tbltext1">
			<b> OBIETTIVI* </b>
		</td>
	</tr>
		<tr>
			<td colspan="4" class="textblack">
				

				<br>ESPORRE CON CHIAREZZA L'OBIETTIVO / GLI OBIETTIVI (MAX 10 RIGHE) CHE IL SOGGETTO INTENDE PERSEGUIRE 
				ATTRAVERSO IL SERVIZIO PRESCELTO.
			</td>
		</tr>
		
	</table>
	<br>
	<table width="500" border="0" cellspacing="2" cellpadding="1" align="center">   
			<tr> 
					<span class="tbltext1">- Utilizzabili 
					<b><label name="NumCaratteri" id="NumCaratteri">600</label></b>
					 caratteri  -
					</span>
				</td>
			</tr>
			<tr> 
				<td align="left" valign="middle" colspan="4">
					<textarea name="txtObiettivo" onKeyup="JavaScript:CheckLenTextArea(document.frmDomIscri.txtObiettivo,NumCaratteri,600)" CLASS="boxcommunity" cols="60" rows="10"></textarea>
				</td>
			</tr>
			
	</table>

<!--SM    INIZIO  TIPOLOGIA ASSISTENZA TECNICA-->		
	<table width="500" border="0" cellspacing="2" cellpadding="1">
	<tr height="2">	
		<td class="sfondocomm" colspan="4" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
	</tr>
	<tr>
		<td align="left" class="textblack" colspan="4">
		Indicare la consulenza specialistica e/o assistenza tecnica di cui si necessita.
		</td>
	</tr>   
	<br>
	<tr>
		<td class="tbltext1">

			<b> TIPOLOGIA DI ASSISTENZA TECNICA* </b>
		</td>
	</tr>
	<tr>
			<td nowrap>
				<span class="tbltext">
				<input type="text" name="txtTipoCons" style="TEXT-TRANSFORM: uppercase;" class="textblack" size="80" readonly>
				<input type="hidden" id="txtTipoConsHid" name="txtTipoConsHid">
				
<%
				NomeForm="frmDomIscri"
				CodiceProvincia="cmbProvRes"
				NomeTipoCons="txtTipoCons"
				CodiceTipoCons="txtTipoConsHid"
				NomeTipoConsAltro= "txtTipoConsAltro"
				
%>
				<a href="Javascript:SelTipoConsulenza('<%=NomeForm%>','<%=CodiceProvincia%>','<%=NomeTipoCons%>','<%=CodiceTipoCons%>','<%=NomeTipoConsAltro%>')" ID="imgPunto1" name="imgPunto1" onmouseover="javascript:window.status='';return true"><img border="0" src="<%=Session("Progetto")%>/images/bullet1.gif"></a>
				</span>	
						
			</td>
		</tr>
		<tr>
		<td align="left" class="textblack" colspan="4">
			In caso di selezione della tipologia 'Altro' specificare la consulenza specialistica e/o assistenza tecnica:
		</td>
		</tr> 
		<tr>
		<td class="tbltext1">
			<b> Altra tipologia </b>
		</td>
		</tr>  
		<tr>
			<td>
			<input type="text" style="TEXT-TRANSFORM:uppercase" id="txtTipoConsAltro" name="txtTipoConsAltro" size="65" maxlength="50">
			</td>
		</tr>	
	</table> 	   


<!-- SM fine Assistenza Tecnica  -->

<!-- ----------------------------------------------------------- -->	   
<table width="500" border="0" cellspacing="2" cellpadding="1">   
		<tr>
			<td colspan="4" class="tbltext1">
			<hr>
				<br>AI FINI DELLA RICHIESTA DEI CONTRIBUTI PREVISTI DAL PROGETTO &quot;ARTIGIANI&quot;<br>
			</td>
		</tr>
		<br>
		<tr>
			<td colspan="1" align="center" class="tbltext1">
				<b>DICHIARA</b><br><br>
			</td>
		</tr>
		<tr>
			<td colspan="1" align="left" class="tbltext1">
				1) Di essere iscritto all'Albo delle Imprese Artigiane al numero <input title="Indicare il numero di iscrizione all'Albo delle Imprese Artigiane" style="TEXT-TRANSFORM:uppercase" class="textblack" size="40" maxlength="7" name="txtNumAlbo">;
			</td>
		</tr>
		<tr>
			<td colspan="1" align="left" class="tbltext1" style="cursor:help" title="Se il Numero Camera di Commercio � inferiore alle 6 cifre, farle precedere da zero es: '001234/0000'; per le ultime 4 cifre inserire l'anno di iscrizione oppure tutti zeri">
				2) di essere iscritto alla Camera di Commercio di  <input type="text" name="txtPrvCameraCom" style="TEXT-TRANSFORM: uppercase;" class="textblack" size="30" readonly>
				<a title="Seleziona Sede" href="Javascript:SelCameraCom()" ID="imgPunto1" name="imgPunto1" onmouseover="javascript:window.status='';return true"><img border="0" src="<%=Session("Progetto")%>/images/bullet1.gif"></a>
				<input type="hidden" name="txtComCameraCom" style="TEXT-TRANSFORM: uppercase;" class="textblack" size="4">
				<!--input type="hidden" id="txtCodComCameraCom" name="txtCodComCameraCom"-->
				<input type="hidden" id="txtCodProvReg" name="txtCodProvReg">
				<br>&nbsp;&nbsp;&nbsp;al numero <input style="TEXT-TRANSFORM:uppercase" class="textblack" size="16" maxlength="11" name="txtNumReg" id="txtNumReg">
				(in caso di erogazione dell'incentivo si impegna a fornire &nbsp;&nbsp; originale del certificato di iscrizione alla 
				CCIIAA non anteriore a due mesi dalla data &nbsp;&nbsp; &nbsp;&nbsp; di pubblicazione dell'avviso, dichiarando che nelle more 
				non sono intervenute &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;modificazioni)
			</td>
		</tr>
		<tr>
			<td colspan="1" align="left" class="tbltext1">
				3) di essere in regola con gli adempimenti previsti dal D. Lgs. 626/94 e successive
				&nbsp;&nbsp;&nbsp;&nbsp;modificazioni in merito al piano di sicurezza e di coordinamento;
			</td>
		</tr>
		<tr>
			<td colspan="1" align="left" class="tbltext1">
			 4) di essere in regola con l'applicazione del Contratto Collettivo Nazionale di Lavoro (C.C.N.L.) applicato che risulta essere il seguente:<br><br>
			&nbsp;&nbsp;&nbsp;&nbsp;
	<%
					EleCcnl = "TCCNL|0|" & date & "||cmbCodCcnl| ORDER BY DESCRIZIONE"
					CreateCombo(EleCcnl)
	%>
			<br>	
			</td>
		</tr>
		<tr>
			<td colspan="1" align="left" class="tbltext1">
				5) di essere in regola con il versamento degli obblighi contributivi ed assicurativi;
			</td>
		</tr>
		<tr>
			<td colspan="1" align="left" class="tbltext1">
				6) di essere in regola con le norme che disciplinano il diritto al lavoro dei disabili;
			</td>
		</tr>
		<tr>
			<td colspan="1" align="left" class="tbltext1">
				7) di aver preso visione dell'avviso e accettarne il contenuto;
			</td>
		</tr>
		<tr>
			<td colspan="1" align="left" class="tbltext1">
				8) di essere disponibile a fornire tutte le altre informazioni, documenti e notizie utili,
				&nbsp;&nbsp;&nbsp;&nbsp;designando a tal fine (Nome) <input title="Nome" style="TEXT-TRANSFORM:uppercase" class="textblack" size="40" maxlength="50" name="txtRefSedeNome"> &nbsp;
				(Cognome) <input title="Cognome" style="TEXT-TRANSFORM:uppercase" class="textblack" size="40" maxlength="50" name="txtRefSedeCognome">, come persona da contattare presso la sede dell'impresa;
			</td>
		</tr>
		<tr>
			<td colspan="1" align="left" class="tbltext1">
				9) di non aver beneficiato (compreso il presente contributo), nell'esercizio finanziario 
				in corso e nei due esercizi immediatamente precedenti, di contributi pubblici percepiti 
				a qualunque titolo un importo eccedente quello indicato nel Reg. CE n. 1998/2006 del 15 
				dicembre 2006 - pubblicato nella GUCE L379 del 28 dicembre 2006.
			</td>
		</tr>
	</table>
	<table width="500" border="0" cellspacing="2" cellpadding="1">   
	<tr>
		<td colspan="4" class="tbltext1">
			<br>Fa formale richiesta per accedere ai CONTRIBUTI FINALIZZATI ALLA PRESTAZIONE 
			DI SERVIZI DI CONSULENZA SPECIALISTICA E/O ASSISTENZA TECNICA.
			
		</td>
	</tr>
	</table>
	<!-- SM login e password -->
	<br>
	<% 
	    call Titolo("Login E Password","Inserisci di seguito una Login ed una password.")
	%>
	
	<table width="500" border="0" cellspacing="2" cellpadding="1">     
        <tr>
	        <td align="left" class="tbltext1" width="30%">
					<b>Login*</b>
	        </td>
	        <td align="left">
	             <input size="34" class="textblack" maxlength="15" name="txtLogin" style="TEXT-TRANSFORM: uppercase;">
	        </td>
	    </tr>
	    <tr>
	        <td align="left" class="tbltext1" width="30%">
					<b>Password*</b>
	        </td>
	        <td align="left">
	             <input type="password" size="34" class="textblack" maxlength="15" name="txtPassword" style="TEXT-TRANSFORM: uppercase;">
	        </td>
	    </tr>
	    <tr>
	        <td align="left" class="tbltext1" width="30%">
					<b>Conferma Password*</b>
	        </td>
	        <td align="left">
	             <input type="password" size="34" class="textblack" maxlength="15" name="txtConfPassword" style="TEXT-TRANSFORM: uppercase;">
	        </td>
	    </tr>  
	</table>
	
	<!--SM fine -->

<!------------------------------------------------------------------------------>
<!------------------------------------------------------------------------------>
<!-- ----------------------------------------------------------- -->	   

	
	<table width="520">		
	<tr>
		<td colspan="2" align="center" class="tbltext1" width="500">
			<!--b>Consenso ai sensi dell'art.11 della legge 31 dicembre 1996<br> n. 675/96</b><br><br-->
			<b>Consenso ai sensi dell'art.13 del D. Lgs. <br> 196/2003 </b><br><br>
			Consenso relativo alla legge sulla privacy: vi chiediamo gentilmente di leggere l'informativa relativa 
			alla privacy riportata di seguito e di esprimere<br>il 
			vostro consenso o meno al trattamento dei dati.
		</td>
	</tr>
	<tr>
		<td align="middle" colspan="2">&nbsp;
		</td>
   </tr>
   </table>
<table width="520">   
	<tr>
		<td colspan="2" class="tbltext"><p align="justify">
		
<%
			on error resume next 
			PathFileEdit = "/ARTIGIANI/Testi/Autorizzazioni/info_privacyaz.htm"
			Server.Execute(PathFileEdit)
			If err.number <> 0 Then
				Response.Write "Errore nella visualizzazione. "
				Response.Write "Contattare il Gruppo Assistenza Portale Italia Lavoro "
				Response.Write "all'indirizzo po-assistenza@italialavoro.it "
			End If				
%>
			</p>
		</td>
	</tr>
</table>

<table>	
	<tr>
		<td colSpan="2"><br>
			<center>
			<input type="radio" value="0" id="radConsenso" name="radConsenso"><span class="tbltext">&nbsp;ACCETTO&nbsp;&nbsp;&nbsp;&nbsp;</span>
			<input type="radio" value="1" CHECKED id="radConsenso" name="radConsenso"><span class="tbltext">&nbsp;NON ACCETTO</span>
			<input type="hidden" name="sControllo" value="2">
			</center>
		</td>
	</tr>
</table>
	<br><br>

	<table cellpadding="0" cellspacing="0" width="500" border="0">	
		<tr align="center">
			<td nowrap align="right"><input type="image" name="Conferma" src="<%=Session("progetto")%>/images/conferma.gif"></td value="Conferma">
			<td nowrap align="center"><a href="javascript:document.frmDomIscri.reset()"><img border="0" src="<%=Session("progetto")%>/images/annulla.gif"></td>
			<td nowrap align="left"><a HREF="javascript:history.back()"><img SRC="<%=Session("Progetto")%>/images/indietro.gif" border="0"></td>
		    <td> 
				<input type="hidden" name="graduatoria" value="<%=sGraduatoria%>">
				<input type="hidden" name="gruppo" value="<%=sGruppo%>">
	           	               
	            <input type="hidden" name="IniIsc" value="<%=sDataIsc%>">
	            <input type="hidden" name="datasis" value="<%=sDataSis%>">
	            <input type="hidden" name="bando" value="<%=nBando%>">
	            <input type="hidden" name="descbando" value="<%=sDescBando%>">
	        </td>
		</tr>
	</table>
</form>

<%
end sub

'------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

function ctrlProj()
	ctrlProj = ""
	sSQL = "SELECT COD_TIMPR FROM PROGETTO WHERE ID_PROJ = " & CLng(nIdProgetto)
	set rsProj = CC.Execute(sSQL)
	
	if rsProj.EOF then
		Msgetto("Progetto inesistente")	
		ctrlProj = "KO"
	else
		sCodTimpr = rsProj("COD_TIMPR")
		ctrlProj = "OK"
	end if
	
end function

'------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

sub ctrlBando()

	'	Se la data di sistema non � compresa nel periodo di 
	'   acquisizione domande non devo caricare la domanda.
	set rsAbil = Server.CreateObject("ADODB.Recordset")

	sSQL = " SELECT B.ID_PROJ, B.DESC_BANDO, B.IDGRUPPO, P.IND_GRAD  FROM BANDO B, PROGETTO P " & _
		   "WHERE B.ID_PROJ = P.ID_PROJ "	 &_
			"AND B.ID_BANDO = "& nBando & " AND " & nTime & _
			" BETWEEN B.DT_INI_ACQ_DOM AND B.DT_FIN_ACQ_DOM"
	
	'-----------------------------------------------------------------------------
	' Questo controllo � stato inserito per permettere l'iscrizione di nominativi
	' anche a bando(sport2job) chiuso tale operazione puo essere eseguita solo dagli
	' utenti appartenenti al gruppo ISCR tramite la funzione ISCRIZIONE
	if ucase(nBypassdata) = "SI" then
		sSQL = " SELECT B.ID_PROJ, B.DESC_BANDO, B.IDGRUPPO, P.IND_GRAD  FROM BANDO B, PROGETTO P " & _
			   " WHERE B.ID_PROJ = P.ID_PROJ "	 &_
			   " AND B.ID_BANDO = "& nBando
				
	end if
	' Response.Write "sSQL " & sSQL
    '----------------------------------------------------------------------------
   	rsAbil.Open sSQL, CC, 3

	if  rsAbil.EOF  then
		Msgetto("Periodo di Acquisizione domande non valido")
		sEsitoBando = "KO"
	else
		sDescBando = rsAbil("DESC_BANDO")
	    nIdProgetto = rsAbil("ID_PROJ")
	    sEsitoBando = "OK"
	    sGruppo =rsAbil("IDGRUPPO")
	    sGraduatoria =rsAbil("IND_GRAD")
	end if  
	rsAbil.close
	set rsAbil = nothing	 

end sub

'------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

sub Fine()
%>
<!--#include virtual = "/include/closeconn.asp"-->
<%if session("login") = "" then%>
	<!--#include virtual = "/ARTIGIANI/struttura_codaART1.asp"-->
<%else%>
	<!--#include virtual = "/strutt_coda1.asp"-->
<%end if%>		
<%
end sub
%>
<!-- ************** ASP Fine *************** -->

<!-- ************** MAIN Inizio ************ -->
<%

dim sDataIsc
dim sSQL
dim CnConn
dim rsAbil

dim nBando
dim nIdProgetto
dim sDescBando
dim sEsitoBando

dim sCodTimpr
dim sDescTimpr

dim sGraduatoria
dim sGruppo, sCommento
dim sCondizione, sCategoria, sDisabilita

dim sProv, nCont ,sDescSvantaggio

sDataIsc = Now()

nBando=clng(Request("txtIdBando"))
'nTime = convdatetodb(Now())
nTime = convdatetodb(Date)
nBypassdata = Request("Bypassdata")

'inizio  20/04/07
		Dim aMenu(0)
		aMenu(0) = Session("Progetto") & "/homeART.asp"
		Session("Indice") = aMenu
		Session("Menu") = aMenu(0)
		nPos = 0
'fine  20/04/07	

ctrlBando()
if  sEsitoBando = "OK" then
    Inizio()

	if Trim(ctrlProj()) = "OK" then
		sDescTimpr = DecCodVal("TIMPR", "0", "", sCodTimpr, 1)  
		sDataSis = ConvDateToString(Now())
		ImpPagina()
	end if
end if

Fine()
%>
<!-- ************** MAIN Fine ************ -->