<%'Option Explicit 
  ' Response.ExpiresAbsolute = Now() - 1 
  ' Response.AddHeader "pragma","no-cache"
  ' Response.AddHeader "cache-control","private"
  ' Response.CacheControl = "no-cache"
%>
<%if session("login") = "" then%>
	<!-- #include virtual="/progettomda/strutt_testaMDA1.asp"-->
<%else%>
	<!-- #include virtual="/strutt_testa1.asp"-->
<%end if%>
<!--#include virtual ="/include/openconn.asp"-->
<!--#include virtual ="/util/portallib.asp"-->
<%

Session("menu")= ""
%>
<!-- ************** Javascript Inizio ************ -->
<!--include del file per fare i controlli sulle stringhe-->
<script LANGUAGE="Javascript" src="/Include/ControlString.inc"></script>
<!--include del file per fare i controlli sulla validit� delle date-->
<script LANGUAGE="Javascript" src="/Include/ControlDate.inc"></script>

<!--include del file per fare i controlli sulla numericit� dei campi-->
<script LANGUAGE="Javascript" src="/Include/ControlNum.inc"></script>

<!--include del file per fare i controlli sulla validit� del codice fiscale-->
<script LANGUAGE="Javascript" src="/Include/ControlCodFisc.inc"></script>

<script LANGUAGE="Javascript" src="/Include/help.inc"></script>
<script LANGUAGE="Javascript" src="/include/SelComune.js"></script>
<script LANGUAGE="Javascript" src="/ProgettoMdA/include/SelComuneRes.js"></script>
<!--<script LANGUAGE="Javascript" src="/ControlliAllegato1.js"></script>--> 

<!-- ************** ASP Inizio *************** -->

<!--#include virtual = "/include/DecCod.asp"-->
<!--#include virtual = "/include/ControlDateVB.asp"-->
<!--#include virtual = "/util/dbutil.asp"-->

<script LANGUAGE="Javascript">
function SelCameraCom()
{ 
 windowArea = window.open ('SelCameraCom.asp' ,'Info','width=422,height=450,Resize=No,Scrollbars=yes')	
 }

function PulisciRes()
{
	document.frmDomIscri.txtComuneRes.value = ""
}
//'''''''''''''''''''''''''''''''''''''''''''''''''''
function IsNumIscReg(sNumIscReg) {
	var t
	
	if (sNumIscReg.length != 11) {
		alert("Il Numero del registro deve essere di 11 caratteri")
		return false
	}
	for (t=0; t<11;t++) {

		if (t==6){
			if (sNumIscReg.charAt(t)!= "/"){
				alert("Il formato corretto del Numero del registro � : 999999/9999")
				return false
			}
			t++ 
		}
		if (!IsNum(sNumIscReg.charAt(t))) {
				alert("Il Numero del registro deve essere numerico")
				return false
		}
	}
	return true
}

//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
function ControllaDati(){


			if(document.frmDomIscri.cmbCodTimpr.value == ""){
				alert("Il campo Tipologia Impresa � obbligatorio")
				document.frmDomIscri.cmbCodTimpr.focus()
				return false
			}
			if (TRIM(document.frmDomIscri.txtRagSoc.value) == ""){
				alert("Il campo Ragione Sociale � obbligatorio")
				document.frmDomIscri.txtRagSoc.focus()
				return false
			}
			if (document.frmDomIscri.cmbCodSet.value == ""){
				alert("Il campo Codice Settore � obbligatorio")
				document.frmDomIscri.cmbCodSet.focus()
				return false
			}
			if (TRIM(document.frmDomIscri.txtPartIva.value) == ""){
				alert("Il campo Partita Iva � obbligatorio")
				document.frmDomIscri.txtPartIva.focus()
				return false
			}
			if (document.frmDomIscri.txtPartIva.value.length != 11){
				alert("Il campo Partita Iva � formalmente errato")
				document.frmDomIscri.txtPartIva.focus()
				return false
			} 
			if (!ValidateInputNumber(document.frmDomIscri.txtPartIva.value) == true){
				alert("Il campo Partita Iva � formalmente errato")
				document.frmDomIscri.txtPartIva.focus()
				return false
			} 
			if (document.frmDomIscri.txtCodFis.value.length != 0 && document.frmDomIscri.txtCodFis.value.length != 16){
				alert("Il campo Codice Fiscale � formalmente errato")
				document.frmDomIscri.txtCodFis.focus()
				return false
			} 
			/*if (document.frmDomIscri.txtCodFis.value.length == 16 && ControllChkCodFisc(TRIM(document.frmDomIscri.txtCodFis.value)) == false){
				alert("Il Codice Fiscale non � valido")
				document.frmDomIscri.txtCodFis.focus()
				return false
			}*/
			blank = " ";
			if (!ChechSingolChar(document.frmDomIscri.txtCodFis.value,blank))
			{
				alert("Il campo Codice Fiscale non pu� contenere spazi bianchi")
				document.frmDomIscri.txtCodFis.focus();
				return false;
			}
			
			if (!ValidateInputStringWithNumber(document.frmDomIscri.txtCodFis.value))
			{
				alert("Il campo Codice fiscale � formalmente errato")
				document.frmDomIscri.txtCodFis.focus()
				return false;
			}
			
			
			if(document.frmDomIscri.cmbCodSede.value == ""){
				alert("Il campo Tipo Sede � obbligatorio")
				document.frmDomIscri.cmbCodSede.focus()
				return false
			}
			if(TRIM(document.frmDomIscri.txtIndirizzo.value) == ""){
				alert("Il campo Indirizzo � obbligatorio")
				document.frmDomIscri.txtIndirizzo.focus()
				return false
			}
			if(document.frmDomIscri.cmbProvRes.value == ""){
				alert("Il campo Provincia � obbligatorio")
				document.frmDomIscri.cmbProvRes.focus()
				return false
			}
			if(TRIM(document.frmDomIscri.txtComuneRes.value) == ""){
				alert("Il campo Comune � obbligatorio")
				document.frmDomIscri.txtComuneRes.focus()
				return false
			}
			
			//telefono	
		
		if (TRIM(document.frmDomIscri.txtNumTel.value) == "") {
			alert("Il campo Telefono � obbligatorio")
			document.frmDomIscri.txtNumTel.focus() 
			return false
		}
		if (TRIM(document.frmDomIscri.txtNumTel.value) != "") {
			if (!ValidateInputNumber(document.frmDomIscri.txtNumTel.value)) {
				alert("Il campo Telefono deve essere numerico")
				document.frmDomIscri.txtNumTel.focus() 
				return false
			}
			if (eval(document.frmDomIscri.txtNumTel.value) == 0){
				alert("Il numero di telefono non pu� essere zero")
				document.frmDomIscri.txtNumTel.focus()
				return false
			}
		}
		
		// email
		
		document.frmDomIscri.txtEMail.value = TRIM(document.frmDomIscri.txtEMail.value)
		if (document.frmDomIscri.txtEMail.value != "") {
			if (!ValidateEmail(document.frmDomIscri.txtEMail.value)) {
				alert("Il campo E-mail � formalmente errato")
				document.frmDomIscri.txtEMail.focus() 
				return false
			}
		}
		// servizio
		
		var y = 0;
			for (i = 0; i < document.frmDomIscri.chkServizio.length; i++) {
				if (document.frmDomIscri.chkServizio[i].checked) {
					y++
				}
			}
			if (y==0){
				alert("E' obbligatorio selezionare un Settore di Appartenenza")
				return false
			}


	//'''''''''''''''''''''''''''''''''''''
	//		if (TRIM(document.frmDomIscri.txtComCameraCom.value) == ""){
			if (TRIM(document.frmDomIscri.txtPrvCameraCom.value) == ""){
				alert("E' obbligatorio specificare la Camera di Commercio di appartenenza")
	//			document.frmDomIscri.txtComCameraCom.focus()
				document.frmDomIscri.txtPrvCameraCom.focus()	
				return false
			} 
			if (TRIM(document.frmDomIscri.txtNumReg.value) == ""){
				alert("E' obbligatorio specificare il Numero di Iscrizione alla Camera di Commercio")
				document.frmDomIscri.txtNumReg.focus()
				return false
			} 
			
			
			// controllo correttezza formale del numero registro
			document.frmDomIscri.txtNumReg.value = TRIM(document.frmDomIscri.txtNumReg.value)		
			if (!document.frmDomIscri.txtNumReg.value == "")	
			{
				if (document.frmDomIscri.txtNumReg.value != "")
				{
					if (!IsNumIscReg(document.frmDomIscri.txtNumReg.value))
					{
						document.frmDomIscri.txtNumReg.focus()
						return false				
					}
				}
			}
			
			
	/*		if(document.frmDomIscri.cmbCodForm.value !="16"){
				if(document.frmDomIscri.cmbCodCcnl.value == ""){
					alert("Il campo Codice CCNL � obbligatorio")
					document.frmDomIscri.cmbCodCcnl.focus()
					return false
				}
			}
		*/	
			if(document.frmDomIscri.txtRefSede.value == ""){
				alert("Il campo Referente Sede � obbligatorio")
				document.frmDomIscri.txtRefSede.focus()
				return false
			}
		
			
			
			// controllo obbligatoriet� chk Modalit� Lavoro
			
			var y = 0;
			for (i = 1; i <= document.frmDomIscri.Contatore.value; i++) {
				if (eval("document.frmDomIscri.chkModLav" + i + ".checked")) {
					y++;
					if (eval("document.frmDomIscri.txtNumPers" + i + ".value")== "") {
						alert("Indicare il numero di persone per la Modalit� di Lavoro indicata")
						eval("document.frmDomIscri.txtNumPers" + i + ".focus()")
						return false
					}
					
					
					
				}
				if (!ValidateInputNumber(eval("document.frmDomIscri.txtNumPers" + i + ".value")) == true){
						alert("Nel campo nr. persone devono essere immessi valori numerici")
						eval("document.frmDomIscri.txtNumPers" + i + ".focus()")
						return false
				} 
				
				if (eval("document.frmDomIscri.txtNumPers" + i + ".value")!= "") {
						if (eval("document.frmDomIscri.chkModLav" + i + ".checked")==false) {
							alert("Se si indica il numero di persone selezionare la Modalit� di Lavoro corrispondente")
							eval("document.frmDomIscri.txtNumPers" + i + ".focus()")
							return false
						}
				}
				
				
			}
			
			// controllo 
			
			if (y==0){
				alert("E' obbligatorio indicare almeno una Modalit� di Lavoro")
				document.frmDomIscri.chkModLav1.focus()
				return false
			}
			
			
				if (document.frmDomIscri.radConsenso[0].checked == false) {
				alert("Per poter concludere la transazione � necessario Accettare il `Consenso al trattamento dei dati personali`")
				document.frmDomIscri.radConsenso[0].focus()
				return false
			}
			
}

</script>
<%	
'-------------------------------------------------------------------------------------------------------------------------------
Sub Titolo(sTitolo,sDescTitolo) %>
    <table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
		<tr height="18">
			<td class="sfondomenu" width="67%" height="18"><span class="tbltext0"><b>&nbsp;<%=sTitolo%></b></span></td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
			<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
		</tr>
	</table>
	<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
		<tr>
			<td align="left" class="sfondocomm"><%=sDescTitolo%></td>
		</tr>
		<tr height="2">
			<td class="sfondocomm" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>
	<br>
<%
end sub

Sub Msgetto(Msg)

%>
	<br>
	<table align="center">
		<tr>
			<td class="tbltext3"><b><%=Msg%></b></td>
		</tr>	
		<tr>
			<td>&nbsp;</td>
		</tr>	
		<tr>
			<td align="center"><a HREF="javascript:history.back()"><img SRC="/progettomda/images/indietro.gif" border="0" WIDTH="55" HEIGHT="40"></a></td>
		</tr>
	</table>
<%
End Sub

'-------------------------------------------------------------------------------------------------------------------------------

sub Inizio()
%>

	
	<br>
	<table cellpadding="0" cellspacing="0" width="500" border="0">
		<tr height="18">
			<td class="sfondomenu" height="18" width="67%">
			<span class="tbltext0"><b>&nbsp;DOMANDA DI ISCRIZIONE</b></span></td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
			<td valign="middle" align="right" class="tbltext1" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">(*) campi obbligatori</td>
		</tr>
		<tr>
			<td class="sfondocomm" width="57%" colspan="3">
				Modulo di iscrizione 
			<%if  sEsitoBando = "OK" then %>
			      per il bando&nbsp;<b><%=sDescBando%></b><br>  
			      Premere <b>Invia</b> per salvare. 
			<%end if%>
			<a href="Javascript:Show_Help('/Pgm/help/GestProgetti/Alfabeta/Selezione/SEL_InsDomIscr')" onmouseover="javascript:window.status=' '; return true"><img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
			</td>
		</tr>
		<tr height="2">
			<td colspan="3" class="sfondocomm" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>
	
	<form method="post" name="frmDomIscri" onsubmit="return ControllaDati(this)" action="AZI_CnfAllegato1.asp">
	
	<input type="hidden" name="txtScadenzaOld" value="<%=sSCadenza%>">
	<input type="hidden" name="txtoggi" value="<%=ConvDateToString(Date())%>">
<%
end sub

'------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

sub ImpPagina()
'	Response.Write "AMBRA " & Session("Progetto")
    dim sInt 
    
	       sCommento =  "Presta <b>molta attenzione</b> a quanto inserisci:<br>" &_ 
				        "Indicare di seguito i dati dell'azienda."  
				        
	       call Titolo("Dati Azienda",sCommento)
	    %>
	   	   
		<table width="500" border="0" cellspacing="2" cellpadding="1">
		<!-------------dati simo--->
		
		<tr>
			<td align="middle" colspan="2" nowrap>
				<p align="left">
					<span class="tbltext1" Style="CURSOR:HELP" title="Scegliere la tipologia d'impresa che corrisponde all'azienda"><b>Tipologia Impresa*<b></span>
				</p>
			</td>
			<td align="left">
<%
					sTimpr = "TIMPR|0|" & date & "|" & sTipoImp & "|cmbCodTimpr|AND CODICE <> '03' ORDER BY DESCRIZIONE"
		'		else
		'			sTimpr = "TIMPR|0|" & date & "|" & sTipoImp & "|cmbCodTimpr' onchange='InviaProv(this.value)' '|ORDER BY DESCRIZIONE"
		'		end if									
				CreateCombo(sTimpr)				
%>	
			</td>
		</tr>    
        <tr>
			<td align="middle" colspan="2" nowrap>
				<p align="left">
				<span class="tbltext1" Style="CURSOR:HELP" title="Inserire Denominazione Azienda"><b>Ragione sociale*<b></span>
				</p>
			</td>
            <td align="left" colspan="2" width="57%"> 
				<p align="left">
					<input style="TEXT-TRANSFORM:uppercase" WIDTH="220px" class="textblack" size="40" maxlength="50" name="txtRagSoc">					
				</p>
            </td>
		</tr>
        <tr>
			<td align="middle" colspan="2" nowrap>
				<p align="left">
				<span class="tbltext1" Style="CURSOR:HELP" title="Inserire forma giuridica che si adatta alla tipologia dell'attivit� esercitata."><b>Forma Giuridica*</b></span>&nbsp;
				</p>
            </td>
            
			<td align="left" colspan="2" width="57%"> 
				<p align="left">
				<span class="table">
<%
				sInt = "FGIUR|" & sIsa & "|" & date() & "| |cmbCodForm | ORDER BY DESCRIZIONE"						
				CreateCombo(sInt)
%>
				</span>
				</p>
            </td>
		</tr>
        <tr>
			<td align="left" colspan="2">
				<p align="left">
					<span class="tbltext1" Style="CURSOR:HELP" title="Indicare classificazione delle attivit� economiche secondo estratto Istat."><b>Settore*</b></span>					
				</p>
			</td>
			
			<td align="left" colspan="2" width="57%"> 
				<p align="left">
					<span class="table">				
<%
				sSQL = "SELECT ID_SETTORE,subStr(DENOMINAZIONE,1,62) As DENOMINAZIONE from Settori ORDER BY DENOMINAZIONE"

				set rsSett = CC.Execute(sSQL)
				Response.Write "<SELECT ID='cmbCodSet' name='cmbCodSet' class='textblack'><OPTION value=></OPTION>"

				do while not rsSett.EOF 

					Response.Write "<OPTION "

					Response.write "value ='" & clng(rsSett("ID_SETTORE")) & _
						"'> " & rsSett("DENOMINAZIONE")  & "</OPTION>"
					rsSett.MoveNext 
				loop
	
				Response.Write "</SELECT>"

				rsSett.Close	
				set rsSett = nothing
%>					
					</span>
				</p>
			</td>
		</tr>
		
		<tr height="2">
		
			<td class="sfondocomm" colspan="4" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
		</tr>
		<tr>
			<td colspan="4" class="textblack">
				 <br>
			</td>
		</tr>
        <tr>
			<td align="left" colspan="2">
				<p align="left">
					<span class="tbltext1"><b>Partita IVA*</b></span>
				</p>
            </td>            
			<td align="left" colspan="2" width="57%"> 
				<p align="left">					
					<input style="TEXT-TRANSFORM:uppercase" class="textblack" size="16" maxlength="11" name="txtPartIva">					
				</p>
            </td>
        </tr>
        <tr>
			<td align="left" colspan="2">
				<p align="left">
					<span class="tbltext1"><b>Codice fiscale</b></span>					 
				</p>
            </td>            
			<td align="left" colspan="2" width="57%"> 
				<p align="left">
					<input class="textblack" size="22" maxlength="16" name="txtCodFis" onkeyup="this.value=this.value.toUpperCase()">					
				</p>
            </td>
        </tr>
        </table>
<!---------------------FINE DATI SIMO------------>
		<table width="500" border="0" cellspacing="2" cellpadding="1">	    
		<tr>
			<td align="left" class="textblack" colspan="1">
				Indicare i dati delle Unit� Produttive con cui l'azienda partecipa all'avviso pubblico (Rif. dell' art.4 dell'avviso pubblico)
			</td>
	    </tr>
	    </table>
	    <table width="500" border="0" cellspacing="2" cellpadding="1">
        <tr>
			<td align="middle" colspan="2" nowrap>
				<p align="left">
					<span class="tbltext1" Style="CURSOR:HELP" title="Indicare che funzione si svolge nella sede indicata."><b>Tipo sede*</b></span>
				</p>		
            </td>            
			<td align="left" colspan="2"> 
				<p align="left">
					<span class="table">
<%
					sInt = "TSEDE|" & sisa & "|" & date & "|" & sCodSede & "|cmbCodSede| ORDER BY DESCRIZIONE"			
					CreateCombo(sInt)
%>
				</p>
            </td>
        </tr>
        <tr>
			<td align="left" colspan="2">
				<p align="left">
					<span class="tbltext1"><b>Indirizzo*</b></span>
				</p>
			</td>			
			<td align="left" colspan="2"> 
				<p align="left">				
					<input style="TEXT-TRANSFORM:uppercase" WIDTH="220px" class="textblack" size="40" maxlength="50" name="txtIndirizzo">
				</p>
			</td>
        </tr>               
        
        </table>
 <%
'	if trim(sCodTimpr) = "" then
' dovrebbe essere testato il solo codice 00

'17/05/2006 inizio
'	if trim(sCodTimpr) = "" or isnull(sCodTimpr) or sCodTimpr="00" or sCodTimpr="0" then
			%>
			  <!--input type="hidden" name="cmbEnteOcc" value="0">			  <input type="hidden" name="txtCodTimpr" value-->
			<%
'	else		
'			sSQL = "SELECT SI.ID_SEDE, SI.DESCRIZIONE, SI.PRV " & _
'					"FROM SEDE_IMPRESA SI, IMPRESA I " & _
'					"WHERE I.COD_TIMPR = '" & sCodTimpr & "' " & _
'					"AND I.ID_IMPRESA = SI.ID_IMPRESA " & _
'					"ORDER BY SI.PRV, SI.DESCRIZIONE"
'					
'					Response.Write sSQL
'				'	Response.END
'			set rsImpresa = Server.CreateObject("ADODB.Recordset")
'			rsImpresa.Open sSQL, CC, 3
'
'			if not rsImpresa.EOF then
'				
'				   call Titolo(sDescTimpr,"Seleziona la regione di residenza solo se vi risiedi da almeno sei mesi.")
				%>	    
    
		<!--table width="500" border="0" cellspacing="2" cellpadding="1">			<tr>				<td align="left" colspan="2" width="60%" nowrap>					<span class="tbltext">					<input type="hidden" name="cmbEnteOcc" value>					<input type="hidden" name="txtCodTimpr" value="<%'=sCodTimpr%>">						<input type="hidden" name="txtDescTimpr" value="<%'=sDescTimpr%>">																<textarea rows="4" cols="40" name="txtDescArea" readonly class="textblacka"></textarea>					<a href="Javascript:SelImpresa('<%'=sCodTimpr%>','<%'=sDescTimpr%>','SEDE_IMPRESA','ID_SEDE','txtImpresa','txtRagione','<%=nBando%>')" ID="imgPunto2" name="imgPunto2"><img border="0" src="<%=Session("Progetto")%>/images/bullet1.gif" onmouseover="javascript:window.status='';return true"></a>					</span>				</td>						</tr>		 </table>    <br-->
  <%	'end if
	'end if  
 
	      %>
	    
	    <table width="500" border="0" cellspacing="2" cellpadding="1">
	    
	    <tr>		
			<td align="left" class="tbltext1">
				<b>Provincia*&nbsp;</b>
			</td>			
			<td align="left">				
<%
	
		set rsProvRes = Server.CreateObject("ADODB.Recordset")

		sSQLProvRes = " SELECT distinct (prv) " &_
					" from COMUNE_PARCHI " 
				
			rsProvRes.Open sSQLProvRes, CC, 3

			if not rsProvRes.EOF then
				
			Response.Write "<Select class='textblack' name='cmbProvRes' onchange='PulisciRes()'"
					Response.Write ">" 
					Response.Write "<Option></Option>" 
				do while not rsProvRes.EOF
				sProv = DecCodVal("PROV", "0", "", rsProvRes("prv"), 1)
				
				
					
					Response.Write "<OPTION "
					Response.write "value ='" & rsProvRes("prv") & _
					"'> " & sProv  & "</OPTION>"
					
				
				
				
				
				 rsProvRes.movenext
				loop 
				Response.Write "</Select>" 
			end if
%>
			</td>
		</tr>		
		<!--tr>					<td align="left" class="tbltext1" width="30%">				<b>Provincia di Residenza*</b>			</td>						<td align="left"-->				
<%



	'		sInt = "PROV|0|" & date & "||cmbProvRes' onchange='PulisciRes()| ORDER BY DESCRIZIONE"			
	'		CreateCombo(sInt)	
%>
			<!--/td>		</tr-->		
		<tr>		
			<td align="left" class="tbltext1">
				<b>Comune*&nbsp;</b>
			</td>		
			
		
			<td nowrap>
				<span class="tbltext">
				<input type="text" name="txtComuneRes" style="TEXT-TRANSFORM: uppercase;" class="textblack" size="35" readonly value="<%=DescrComuneResid%>">
				<input type="hidden" id="txtComRes" name="txtComRes" value="<%=COM_RES%>">
<%
				NomeForm="frmDomIscri"
				CodiceProvincia="cmbProvRes"
				NomeComune="txtComuneRes"
				CodiceComune="txtComRes"
				Cap="txtCapRes"	

				
%>
				<input type="HIDDEN" name="txtCapRes" class="textblack" value="<%=CAP_RES%>" size="5" maxlength="5">
				<a href="Javascript:SelComuneRes('<%=NomeForm%>','<%=CodiceProvincia%>','<%=NomeComune%>','<%=CodiceComune%>','<%=Cap%>')" ID="imgPunto1" name="imgPunto1" onmouseover="javascript:window.status='';return true"><img border="0" src="<%=Session("Progetto")%>/images/bullet1.gif"></a>
				</span>				
			</td>
		</tr>			
	   </table>  
	   <table width="500" border="0" cellspacing="2" cellpadding="1">
	    <tr>
			<td align="left" class="textblack" colspan="2">
			Indicare un recapito telefonico presso cui gli operatori possono contattare direttamente per comunicazioni urgenti.
			E' necessario scrivere di seguito tutti i numeri che lo compongono, senza aggiungere spazi o altri separatori.<br>
			Segnalare il tuo indirizzo di posta elettronica personale, se lo hai, in modo da agevolare i contatti.
			</td>
	    </tr>
	    <tr>
			<td align="left" class="tbltext1" width="23%">
				<b>N� Telefono*</b>
	        </td>
	        <td align="left">
				<input style="TEXT-TRANSFORM: uppercase;" size="25" maxlength="20" class="textblack" name="txtNumTel">
	        </td>
	    </tr>
	    <tr>
			<td align="left" class="tbltext1" width="23%">
	   			<b>E-Mail</b>
	        </td>
	        <td align="left">
		   		<input size="25" maxlength="100" class="textblack" name="txtEMail">
	        </td>
	    </tr>
	</table>

	<br>	
	<table width="500" border="0" cellspacing="2" cellpadding="1" align="center">   
	<tr>
		<td colspan="4" class="textblack">

			<b> SETTORE DI APPARTENENZA </b>
		</td>
	</tr>
	   
	</table>  
	<!--inizio simona gab-->
	<table width="500" border="0" cellspacing="2" cellpadding="1" align="center">   
	<tr>
		<td colspan="4" class="textblack">
	<%	
   		set rsDecServizio = Server.CreateObject("ADODB.Recordset")

		sSQLDecServizio = " SELECT CODICE, DESCRIZIONE" &_
					" from DESC_PROD_SERV " &_
					" WHERE  " & nTime & " between decorrenza and scadenza "
			
			rsDecServizio.Open sSQLDecServizio, CC, 3

		if not rsDecServizio.EOF then
			nContServ = 0
			do while not rsDecServizio.EOF
				sCodiceServizio  = rsDecServizio("codice")
				sDescServizio= rsDecServizio("descrizione")
		
				nContServ = nContServ + 1
		%>
		<tr>
				<td class="tbltext1" valign="middle" align="left">
				<input type="radio" id="chkServizio" name="chkServizio" value="<%=sCodiceServizio%>">								
			</td>
			
			<td class="tbltext1" valign="middle" align="left">
				<%=sDescServizio%>	<br>			
				<input type="hidden" name="txtServizio<%=nContServ%>" value="<%=sCodiceServizio%>">				
			</td>
			
		 <%	 
			
			rsDecServizio.movenext
			loop
		 end if
		 %>
		 
			<input type="hidden" name="ContServ" value="<%=nContServ%>"> 

		</table>
		<!--fine simona gab--> 
		
		
		

	<table width="500" border="0" cellspacing="2" cellpadding="1">   
		<tr>
			<td colspan="4" class="tbltext1">
			<hr>
				
				<br>AI FINI DELLA RICHIESTA DEGLI INCENTIVI PER L'ASSUNZIONE PREVISTI DAL <br>
				PROGRAMMA &quot;MARCHI D'AREA - STRUMENTI PER LO SVILUPPO DELL'OCCUPAZIONE <br>
				NEL SETTORE &quot;AGRO-ALIMENTARE&quot; E NELLA PIENA CONSAPEVOLEZZA DELLA RESPONSABILITA'<br>
				PENALE CUI PUO' ANDARE INCONTRO IN CASO DI AFFERMAZIONI MENDACI AI SENSI <br>
				DELL'ART. '4 DELLA LEGGE 04.01.1968 N.15 E SUCCESSIVE MODIFICHE (L. 191/98 E D.P.R.<br>
				403/98)
				
			</td>
		</tr>
		
		<tr>
			<td colspan="1" align="center" class="tbltext1">
				<b>DICHIARA</b><br><br>
			</td>
		</tr>
		
		<tr>
			<td colspan="1" align="left" class="tbltext1" style="cursor:help" title="Se il Numero Camera di Commercio � inferiore alle 6 cifre, farle precedere da zero es: '001234/0000'; per le ultime 4 cifre inserire l'anno di iscrizione oppure tutti zeri">
				1) di essere iscritto alla Camera di Commercio di  <input type="text" name="txtPrvCameraCom" style="TEXT-TRANSFORM: uppercase;" class="textblack" size="30" readonly>
				<a title="Seleziona Sede" href="Javascript:SelCameraCom()" ID="imgPunto1" name="imgPunto1" onmouseover="javascript:window.status='';return true"><img border="0" src="<%=Session("Progetto")%>/images/bullet1.gif"></a>
				<input type="hidden" name="txtComCameraCom" style="TEXT-TRANSFORM: uppercase;" class="textblack" size="4">
				<!--input type="hidden" id="txtCodComCameraCom" name="txtCodComCameraCom"-->
				<input type="hidden" id="txtCodProvReg" name="txtCodProvReg">
				<br>&nbsp;&nbsp;&nbsp;al numero <input style="TEXT-TRANSFORM:uppercase" class="textblack" size="16" maxlength="11" name="txtNumReg" id="txtNumReg">
				(in caso di erogazione dell'incentivo si impegna a fornire &nbsp;&nbsp; originale del certificato di iscrizione alla 
				CCIIAA non anteriore a due mesi dalla data &nbsp;&nbsp; &nbsp;&nbsp; di pubblicazione dell'avviso, dichiarando che nelle more 
				non sono intervenute &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;modificazioni)
			</td>
		</tr>
		<tr>
			<td colspan="1" align="left" class="tbltext1">
				2) di essere in regola con gli adempimenti previsti dal D. Lgs. 626/94 e successive
				&nbsp;&nbsp;&nbsp;&nbsp;modificazioni in merito al piano di sicurezza e di coordinamento;
			</td>
		</tr>
		<tr>
			<td colspan="1" align="left" class="tbltext1">
			 3) di essere in regola con l'applicazione del CCNL applicato che � il seguente:<br><br>
			&nbsp;&nbsp;&nbsp;&nbsp;
	<%
					EleCcnl = "TCCNL|0|" & date & "||cmbCodCcnl| ORDER BY DESCRIZIONE"
					CreateCombo(EleCcnl)
	%>
			<br>	
			</td>
		</tr>
		<tr>
			<td colspan="1" align="left" class="tbltext1">
				4) di essere in regola con il versamento degli obblighi contributivi ed assicurativi;
			</td>
		</tr>
		<tr>
			<td colspan="1" align="left" class="tbltext1">
				5) di essere in regola con le norme che disciplinano il diritto al lavoro dei disabili;
			</td>
		</tr>
		<tr>
			<td colspan="1" align="left" class="tbltext1">
				6) di aver preso visione dell'avviso e accettarne il contenuto;
			</td>
		</tr>
		<tr>
			<td colspan="1" align="left" class="tbltext1">
				7) di essere disponibile a fornire tutte le altre informazioni, documenti e notizie utili,
				&nbsp;&nbsp;&nbsp;&nbsp;designando a tal fine <input style="TEXT-TRANSFORM:uppercase" class="textblack" size="40" maxlength="50" name="txtRefSede">,
				la persona da &nbsp;&nbsp;&nbsp;&nbsp;contattare presso la sede dell'impresa.
			</td>
		</tr>
		<tr>
			<td colspan="1" align="left" class="tbltext1">
				8) di non essere stato interessato, negli ultimi 12 mesi, da procedure di cassa &nbsp;&nbsp;&nbsp;&nbsp;integrazione
				 straordinaria o di riduzione del personale;
			</td>
		</tr>
		<tr>
			<td colspan="1" align="left" class="tbltext1">
				9) di non aver licenziato negli ultimi 6 mesi, senza giustificato motivo, personale con le &nbsp;&nbsp;&nbsp;&nbsp;stesse
				qualifiche per le quali si richiede l'incentivo;
			</td>
		</tr>
	</table>
 	    
 	 <table width="500" border="0" cellspacing="2" cellpadding="1">   
	<tr>
		<td colspan="4" class="tbltext1">
			<br>Fa formale richiesta per accedere agli incentivi all'assunzione, impegnandosi ad assumere<b> 'A
			TEMPO INDETERMINATO'</b> secondo le seguenti modalit�:
		</td>
	</tr>
   

   
		<%'''''''''''''''''''''''''''''''NEW CODMODLAV''''''''''
		set rsDecModalitaLav = Server.CreateObject("ADODB.Recordset")

		sSQLDecModalitaLav = " SELECT CODICE, DESCRIZIONE" &_
					" from DESC_MODALITALAV " &_
					" WHERE  " & nTime & " between decorrenza and scadenza "
			
			rsDecModalitaLav.Open sSQLDecModalitaLav, CC, 3

		if not rsDecModalitaLav.EOF then
			nCont = 0
			do while not rsDecModalitaLav.EOF
				sCodiceModLav  = rsDecModalitaLav("codice")
				sDescModLav= rsDecModalitaLav("descrizione")
		'
				nCont = nCont + 1
		%>
		<tr>
		
		
			<td class="tbltext1" valign="middle" align="left">
				<input type="checkbox" id="chkModLav<%=nCont%>" name="chkModLav<%=nCont%>" value="S">								
			</td>
			
			<td class="tbltext1" valign="middle" align="left">
				<%=sDescModLav%>	<br>			
				<input type="hidden" name="txtModLav<%=nCont%>" value="<%=sCodiceModLav%>">				
			</td>
			
			<td width="20%" class="tbltext1" valign="middle" align="left">nr. persone
					<input class="textblack" size="20" maxlength="22" name="txtNumPers<%=nCont%>">					
			</td>


<!--					<td class="tbltext1" valign="middle" align="left"><br>						<%=sDescModLav%>	<br>					</td>						<td class="tbltext1" valign="middle" align="left"><br>								<input type="hidden" name="txtModLav<%=nCont%>" value="<%=sCodiceModLav%>">								<input type="checkbox" id="chkModLav<%=nCont%>" name="chkModLav<%=nCont%>" value="S">											<br>						</td>		-->		</tr>
	
		 <%	 
			
			rsDecModalitaLav.movenext
			loop
		 end if
		 %>
		 
			<input type="hidden" name="Contatore" value="<%=nCont%>"> 

		</table>

	
	<br>
	<table width="520">
	<tr><td>&nbsp;</td></tr>		
	<tr>
		<td colspan="2" align="center" class="tbltext1" width="500">
			<!--b>Consenso ai sensi dell'art.11 della legge 31 dicembre 1996<br> n. 675/96</b><br><br-->
			<b>Consenso ai sensi dell'art.13 del D. Lgs. <br> 196/2003 </b><br><br>
			Consenso relativo alla legge sulla privacy: vi chiediamo gentilmente di leggere l'informativa relativa 
			alla privacy riportata di seguito e di esprimere<br>il 
			vostro consenso o meno al trattamento dei dati.
		</td>
	</tr>
	<tr>
		<td align="middle" colspan="2">&nbsp;
		</td>
   </tr>
   </table>
<table width="520">   
	<tr>
		<td colspan="2" class="tbltext"><p align="justify">
		
<%
			on error resume next 
			PathFileEdit = "/ProgettoMDA/Testi/Autorizzazioni/info_privacyaz.htm"
			Server.Execute(PathFileEdit)
			If err.number <> 0 Then
				Response.Write "Errore nella visualizzazione. "
				Response.Write "Contattare il Gruppo Assistenza Portale Italia Lavoro "
				Response.Write "all'indirizzo po-assistenza@italialavoro.it "
			End If				
%>
			</p>
		</td>
	</tr>
</table>

<table>	
	<tr>
		<td colSpan="2"><br>
			<center>
			<input type="radio" value="0" id="radConsenso" name="radConsenso"><span class="tbltext">&nbsp;ACCETTO&nbsp;&nbsp;&nbsp;&nbsp;</span>
			<input type="radio" value="1" CHECKED id="radConsenso" name="radConsenso"><span class="tbltext">&nbsp;NON ACCETTO</span>
			<input type="hidden" name="sControllo" value="2">
			</center>
		</td>
	</tr>
</table>
	<br><br>

	<table cellpadding="0" cellspacing="0" width="500" border="0">	
		<tr align="center">
			<td nowrap align="right"><input type="image" name="Conferma" src="<%=Session("progetto")%>/images/conferma.gif"></td value="Conferma">
			<td nowrap align="center"><a href="javascript:document.frmDomIscri.reset()"><img border="0" src="<%=Session("progetto")%>/images/annulla.gif"></td>
			<td nowrap align="left"><a HREF="javascript:history.back()"><img SRC="<%=Session("Progetto")%>/images/indietro.gif" border="0"></td>
		    <td> 
				<input type="hidden" name="graduatoria" value="<%=sGraduatoria%>">
				<input type="hidden" name="gruppo" value="<%=sGruppo%>">
	           	               
	            <input type="hidden" name="IniIsc" value="<%=sDataIsc%>">
	            <input type="hidden" name="datasis" value="<%=sDataSis%>">
	            <input type="hidden" name="bando" value="<%=nBando%>">
	            <input type="hidden" name="descbando" value="<%=sDescBando%>">
	        </td>
		</tr>
	</table>
</form>

<%
end sub

'------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

function ctrlProj()
	ctrlProj = ""
	sSQL = "SELECT COD_TIMPR FROM PROGETTO WHERE ID_PROJ = " & CLng(nIdProgetto)
	set rsProj = CC.Execute(sSQL)
	
	if rsProj.EOF then
		Msgetto("Progetto inesistente")	
		ctrlProj = "KO"
	else
		sCodTimpr = rsProj("COD_TIMPR")
		ctrlProj = "OK"
	end if
	
end function

'------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

sub ctrlBando()

	'	Se la data di sistema non � compresa nel periodo di 
	'   acquisizione domande non devo caricare la domanda.
	set rsAbil = Server.CreateObject("ADODB.Recordset")

	sSQL = " SELECT B.ID_PROJ, B.DESC_BANDO, B.IDGRUPPO, P.IND_GRAD  FROM BANDO B, PROGETTO P " & _
		   "WHERE B.ID_PROJ = P.ID_PROJ "	 &_
			"AND B.ID_BANDO = "& nBando & " AND " & nTime & _
			" BETWEEN B.DT_INI_ACQ_DOM AND B.DT_FIN_ACQ_DOM"
	
	'-----------------------------------------------------------------------------
	' Questo controllo � stato inserito per permettere l'iscrizione di nominativi
	' anche a bando(sport2job) chiuso tale operazione puo essere eseguita solo dagli
	' utenti appartenenti al gruppo ISCR tramite la funzione ISCRIZIONE
	if ucase(nBypassdata) = "SI" then
		sSQL = " SELECT B.ID_PROJ, B.DESC_BANDO, B.IDGRUPPO, P.IND_GRAD  FROM BANDO B, PROGETTO P " & _
			   " WHERE B.ID_PROJ = P.ID_PROJ "	 &_
			   " AND B.ID_BANDO = "& nBando
				
	end if
	'Response.Write "sSQL " & sSQL
    '----------------------------------------------------------------------------
   	rsAbil.Open sSQL, CC, 3

	if  rsAbil.EOF  then
		Msgetto("Periodo di Acquisizione domande non valido")
		sEsitoBando = "KO"
	else
		sDescBando = rsAbil("DESC_BANDO")
	    nIdProgetto = rsAbil("ID_PROJ")
	    sEsitoBando = "OK"
	    sGruppo =rsAbil("IDGRUPPO")
	    sGraduatoria =rsAbil("IND_GRAD")
	end if  
	rsAbil.close
	set rsAbil = nothing	 

end sub

'------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

sub Fine()
%>
<!--#include virtual = "/include/closeconn.asp"-->
<%if session("login") = "" then%>
	<!--#include virtual = "/progettomda/struttura_codaMDA1.asp"-->
<%else%>
	<!--#include virtual = "/strutt_coda1.asp"-->
<%end if%>		
<%
end sub
%>
<!-- ************** ASP Fine *************** -->

<!-- ************** MAIN Inizio ************ -->
<%

		
dim sDataIsc
dim sSQL
dim CnConn
dim rsAbil

dim nBando
dim nIdProgetto
dim sDescBando
dim sEsitoBando

dim sCodTimpr
dim sDescTimpr

dim sGraduatoria
dim sGruppo, sCommento
dim sCondizione, sCategoria, sDisabilita
dim sScadenza

dim sProv, nCont,sDescModlav,nContServ,sDescServizio,sCodiceServizio

sDataIsc = Now()

nBando=clng(Request("txtIdBando"))
'nTime = convdatetodb(Now())
nTime = convdatetodb(Date)
nBypassdata = Request("Bypassdata")

ctrlBando()
if  sEsitoBando = "OK" then
    Inizio()

	if Trim(ctrlProj()) = "OK" then
		sDescTimpr = DecCodVal("TIMPR", "0", "", sCodTimpr, 1)  
		sDataSis = ConvDateToString(Now())
		ImpPagina()
	end if
end if

Fine()
%>
<!-- ************** MAIN Fine ************ -->