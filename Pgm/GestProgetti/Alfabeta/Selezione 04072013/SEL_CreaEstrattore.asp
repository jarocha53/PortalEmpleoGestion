<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #include virtual="/strutt_testa2.asp"-->
<!--#include Virtual = "/Include/OpenConn.asp"-->
<!--#include File = "SEL_GestioneFile.asp"-->
<!--#include virtual = "/include/ControlDateVB.asp"-->
<!--#include virtual = "/util/dbutil.asp"-->
<%
if ValidateService(session("idutente"),"SEL_CREAESTRATTORE", CC) <> "true" then 
	response.redirect "/util/error_login.asp"
end if
%>

<script language="javascript">
<!--#include virtual = "/Include/help.inc"-->

function Selezionato(){

    if(document.frmVisFiltri.cmbBando.value == ""){
       alert("Selezionare un bando");
       document.frmVisFiltri.cmbBando.focus();
        
    }
    else
    {
       document.frmVisFiltri.submit();
    }
      
}

</script>

<table border="0" width="520" cellspacing="0" cellpadding="0" height="70" align="center">
		<tr>
			<td width="500" background="<%=session("progetto")%>/images/titoli/servizi2g.gif" height="70" valign="bottom" align="right">
				<table border="0" background width="500" height="23" cellspacing="0" cellpadding="0">
					<tr>
						<td width="100%" valign="top" align="right"><b CLASS="tbltext1a">Iscrizione Batch
						  </b></td>
					</tr>
				</table>
			</td>
		</tr>
	</table> 
	<br>
	<!-- Lingetta superiore -->
	<table border="0" CELLPADDING="0" CELLSPACING="0" width="500" align="center">
		<tr height="18">
			<td class="sfondomenu" width="67%" height="18"><span class="tbltext0"><b>&nbsp;GESTIONE ISCRIZIONE BATCH </b></span></td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
			<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
		</tr>
	</table>
	<!-- Commento -->
	<table border="0" CELLPADDING="0" CELLSPACING="0" width="500" align="center">
		<tr>
			<td align="left" class="sfondocomm">
				Selezionare il bando su cui si vuole effettuare la selezione 
				<a href="Javascript:Show_Help('/Pgm/help/GestProgetti/alfabeta/Selezione/SEL_CreaEstrattore')"><img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
			</td>
		</tr>
		<tr height="2">
			<td class="sfondocommaz" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>
	<br>
	
<%	sql="SELECT B.ID_BANDO,B.DESC_BANDO FROM " &_
		    "BANDO B, PROGETTO P WHERE  " &_
		    " NOT EXISTS(SELECT DISTINCT DI.ID_BANDO FROM " &_
		    "DOMANDA_ISCR DI WHERE DI.ID_BANDO=B.ID_BANDO) " &_
		    " AND B.ID_PROJ=P.ID_PROJ" &_
		    " AND P.AREA_WEB='" & mid(Session("Progetto"),2) & "'" 

'		    "  DT_FIN_ACQ_DOM IS NULL OR DT_FIN_ACQ_DOM >= " &_
'		     ConvDateToDB(date()) & " " &_
	   
'Response.Write sql

'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
	set rsUorg = cc.execute(sql)
    if not rsUorg.eof then
		  	
		    	%>
		<br>    	
		<form name="frmVisFiltri" action="SEL_CreaEstrattore.asp" method="post">    	
	   	<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
	       <tr>
			<td align="left" class="tbltext1">
				<b>Selezionare un bando</b>&nbsp;
	        </td>
	       	<td align="left">
					<select class="textblack" name="cmbBando" onchange="Selezionato()">
						<option></option>
						
					    <%
					do until rsUorg.eof 
					    Response.write "<OPTION  value ='" & rsUorg("ID_BANDO") & "'> " &_
						rsUorg("DESC_BANDO") & "</OPTION>"
					rsUorg.movenext
				    loop		
					    
				%>
				    </select>
				    </td>
			</tr>
        </table> 
		</form>	
					
		<%		    
	else %>
		<br><br>
		<table width="500" align="center" border="0" cellspacing="2" cellpadding="1">
			<tr height="20"> 
			  	<td class="tbltext3" align="middle" width="500">
					<b>Non ci sono bandi selezionabili</b>
				</td>
			</tr>
		</table>
		<br><br>
		    
<%	end if
    set rsUorg = nothing

sBando = Request.Form ("cmbBando")

session("IdBando") =""

if sBando <> "" then
	    sql = "SELECT COUNT(*) AS TOTALE FROM DOMANDA_ISCR WHERE ID_BANDO = " & sBando

'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
        set rsConta = cc.execute(sql)
	   
	    if clng(rsConta("TOTALE")) > 0 then %>
             <br><br>
		      <table width="500" align="center" border="0" cellspacing="2" cellpadding="1">
			     <tr height="20"> 
			  	    <td class="tbltext3" align="middle">
					   <b>Bando gia' elaborato</b>
				    </td>
			     </tr>
		      </table>
		     <br><br>
<%		
        else

	         session("IdBando") = sBando	
	          
	         GestFile()
		     ReadFile()
%>
             <form name="frmBandi" action="SEL_SelFiltri.asp" method="post">    	
	   		   <input type="hidden" name="txtIdBando" value="<%=sBando%>"> 
	   		 </form>
	   		 <script>
	   		      frmBandi.submit() 
	   		 </script>
	   		     
<%		     
	    end if
	    set rsConta = nothing	
end if
%>	
<!--#include virtual = "/include/closeconn.asp"-->
<!--#include virtual = "strutt_coda2.asp"-->
