<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include Virtual = "/strutt_testa2.asp"-->
<!--#include Virtual ="/Include/openconn.asp"-->

<!-- #include virtual="/include/DecCod.asp" -->

<!-- #include virtual="/include/DecComun.asp" -->


<script language="javascript" src="/include/help.inc"></script>
<script language="Javascript" src="/include/ControlDate.inc"></script>
<script language="Javascript" src="/include/ControlNum.inc"></script>
<script language="Javascript" src="/include/ControlString.inc"></script>
<script language="Javascript" src="Controlli.js"></script>
 
<script language="Javascript">

function Controlla(){

	if(TRIM(forma.nConv.value) == ""){
		alert("Inserire in numero della convenzione.");
		forma.nConv.focus();
		return false;
	}
	
	if(TRIM(forma.dataConv.value) == ""){
		alert("Inserire la data di stipula della convenzione.");
		forma.dataConv.focus();
		return false;
	}

	//Data di stipula della convenzione corretta
	if (!ValidateInputDate(forma.dataConv.value)){
		forma.dataConv.focus();
		return false;
	}

	if (TRIM(forma.Posto.value) == ""){
		alert("Inserire il luogo di compilazione del modello.");
		forma.Posto.focus();
		return false;
	}
		
	if (TRIM(forma.DATAODIERNA.value) == ""){
		alert("Inserire la data di compilazione del modello.");
		forma.DATAODIERNA.focus();
		return false;
	}
	
	if (!ValidateInputDate(forma.DATAODIERNA.value)){
		forma.DATAODIERNA.focus();
		return false;
	}

	return true
}

/*function Invia(){
	var appo;
	appo = Controlla();
	
	if (appo == true){
		forma.action = "window.open (GES_Risultati.asp ,Rubrica,Status=no,scrollbars=yes,toolbar=no,width=550,height=500,top=20,left=100)";
		forma.submit();
	}else{
	
	}
}
*/


</script>

<%
'--------------------------------------------------------------------------------------------------------------------------------------------------

sub ImpVariabili()
	idPersona=Request.Form("txtIdPers")
	sSede=Request.Form("txtSede")
	nIdRic = Request.Form("txtIdRichiesta")

'	Response.Write "idPersona: " & idPersona & "<br>"
'	Response.Write "sSede: " & sSede & "<br>"
'	Response.Write "nIdRic: " & nIdRic & "<br>"

	SP = Session("Progetto")
	SP = replace(SP,"/","\")
	Session("PosDoc") = Server.MapPath("\") & SP & "\DOCPRG\"


	'CONSUELO 07/04/2004 aggiunte request per dati utili
	sNome	  = Request.Form("txtNome")
	sCognome  = Request.Form("txtCognome")
	dDtIni	  = Request.Form("txtDtIniStage")
	dDtFin	  = Request.Form("txtDtFinStage")
	sDescSede = Request.Form("txtDescSede")
	sRagSoc	  = Request.Form("txtRagSoc")

'	Response.Write "sNome: " & sNome & "<br>"
'	Response.Write "sCognome: " & sCognome & "<br>"
'	Response.Write "dDtIni: " & dDtIni & "<br>"
'	Response.Write "dDtFin: " & dDtFin & "<br>"
'	Response.Write "sDescSede: " & sDescSede & "<br>"
'	Response.Write "sRagSoc: " & sRagSoc & "<br>"

	nIdTutor   = Request.Form("txtIdTut")
	nIdTutorAz = Request.Form("txtIdTutAz")

	sPrvNasc  = Request.Form("txtPrvNasc")
	sComNasc  = Request.Form("txtComNasc")
	dDtNasc   = Request.Form("txtDtNasc")
	sPrvRes   = Request.Form("txtPrvRes")
	sComRes   = Request.Form("txtComRes")
	sIndRes   = Request.Form("txtIndRes")
	sCodFisc  = Request.Form("txtCodFisc")
	
	sApDescCom = DescrComune(sComNasc)
	sLuogoNascita = sApDescCom & " (" & sPrvNasc & ")"
	
	sApDescCom = DescrComune(sComRes)
	sLuogoResidenza  = sIndRes & ", " & sApDescCom & " (" & sPrvRes & ")"

	'TUTOR
	sSQL = "SELECT NOME, COGNOME FROM UTENTE WHERE IDUTENTE = " & nIdTutor
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	set rsRisultato = CC.Execute(sSQL)

	if not rsRisultato.EOF then
		sNomeTut = rsRisultato("NOME")
		sCognomeTut = rsRisultato("COGNOME")
	end if

	'TUTOR AZIENDALE
	sSQL = "SELECT NOME, COGNOME FROM UTENTE WHERE IDUTENTE = " & nIdTutorAz
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	set rsRisultato = CC.Execute(sSQL)

	if not rsRisultato.EOF then
		sNomeTutAz = rsRisultato("NOME")
		sCognomeTutAz = rsRisultato("COGNOME")
	end if

'	Response.Write "sNomeTut: " & sNomeTut & "<br>"
'	Response.Write "sCognomeTut: " & sCognomeTut & "<br>"
'	Response.Write "sNomeTutAz: " & sNomeTutAz & "<br>"
'	Response.Write "sCognomeTutAz: " & sCognomeTutAz & "<br>"
'Response.End

end sub

'--------------------------------------------------------------------------------------------------------------------------------------------------

sub ImpPagina()
%>

<br>

<!--- TITOLO PAGINA ---->
<center>

<form method="post" name="forma" onsubmit="return Controlla()" action="GES_Risultati.asp">
<!--form method="post" name="forma" onsubmit="" action="GES_Risultati.asp"-->
	<input type="hidden" value="<%=nIdRic%>" name="txtIdRichiesta">
	<input type="hidden" value="<%=sSede%>" name="txtSede">
	<input type="hidden" value="<%=idPersona%>" name="txtIdPers">
	<input type="hidden" name="txtDescSede" value="<%=Request.Form("txtDescSede")%>">
	<input type="hidden" name="txtRagSoc" value="<%=Request.Form("txtRagSoc")%>">
	<input type="hidden" name="txtIdTut" value="<%=Request.Form("txtIdTut")%>">
	<input type="hidden" name="txtIdTutAz" value="<%=Request.Form("txtIdTutAz")%>">			
	

	<input type="hidden" name="Cognome" value="<%=sCognome%>">
	<input type="hidden" name="Nome" value="<%=sNome%>">
	<input type="hidden" name="LUOGO" value="<%=sLuogoNascita%>">
	<input type="hidden" name="dataTir" value="<%=dDtNasc%>">
	<input type="hidden" name="RESTIR" value="<%=sLuogoResidenza%>">
	<input type="hidden" name="CODFISCTIR" value="<%=sCodFisc%>">
	<input type="hidden" name="NAZOSP" value="<%=sRagSoc%>">
	<input type="hidden" name="SEDETIR" value="<%=sDescSede%>">
	<input type="hidden" name="INTERDA" value="<%=dDtIni%>">
	<input type="hidden" name="INTERAL" value="<%=dDtFin%>">
	
	<input type="hidden" name="NTUTORFOR" value="<%=sNomeTut%>">
	<input type="hidden" name="CTUTORFOR" value="<%=sCognomeTut%>">
	
	<input type="hidden" name="NTUTORAZ" value="<%=sNomeTutAz%>">
	<input type="hidden" name="CTUTORAZ" value="<%=sCognomeTutAz%>">

	<table cellSpacing="4" align="center" cellPadding="4" width="500" border="1"> 
	<tr>
	<td class="tbltext">
	  <p align="center" class="tbltext1"><b>
		MODELLO DI PROGETTO FORMATIVO</b><br>
		(su carta intestata del soggetto promotore)<br>
		<br>
		<b>PROGETTO FORMATIVO E DI ORIENTAMENTO</b><br>
		(rif. Convenzione n.<input class="textred" id="nConv" size="15" name="nConv" maxlength="15"> stipulata in data <input class="textred" id="dataConv" name="dataConv" size="11" maxlength="10">) 
	  </p>
	  
	  <p align="justify">Tirocinante <%=sCognome & " " & sNome%><br>
		nato a <%=sLuogoNascita%> il <%=dDtNasc%><br>
		residente in <%=sLuogoResidenza%><br>
		codice fiscale <%=sCodFisc%>.<br>
		<br>
		<b>Attuale condizione (barrare la casella):</b><br>
		
		<ul>
		<div class="tbltext">
	 	<table class="tbltext">
		 <tr><td>studente scuola secondaria superiore</td><td><input type="radio" class="textred" name="Condiz" value="1"></td></tr>
		 <tr><td>universitario</td><td><input type="radio" class="textred" name="Condiz" value="2"></td></tr>
		 <tr><td>frequentante corso post-diploma</td><td><input type="radio" class="textred" name="Condiz" value="3"></td></tr>
		 <tr><td>frequentante corso post-laurea</td><td><input type="radio" class="textred" name="Condiz" value="4"></td></tr>
		 <tr><td>allievo della formazione professionale</td><td><input type="radio" class="textred" name="Condiz" value="5"></td></tr>
		 <tr><td>disoccupato/in mobilitÓ</td><td><input type="radio" class="textred" name="Condiz" value="6"></td></tr>
		 <tr><td>inoccupato</td><td><input type="radio" class="textred" name="Condiz" value="7"></td></tr></table>
		</div>
		(barrare se trattasi di soggetto portatore di handicap) si<input type="radio" class="textred" name="CondizH" value="1">no<input type="radio" class="textred" name="CondizH" value="2"> <br>

		</ul>
		Azienda ospitante <br>
		<%=sRagSoc%><br>
		<br>
		Sede del tirocinio (stabilimento/reparto/ufficio) <br>
		<%=sDescSede%><br>
		<br>
		Tempi di accesso ai locali aziendali <input class="textred" id="TEMPI" size="15" name="TEMPI" maxlength="15"><br>
		<br>
		Periodo di tirocinio n. mesi <input class="textred" id="nMESI" size="4" name="nMESI" maxlength="3">
		dal <%=dDtIni%> al <%=dDtFin%><br>
		<br>
		Tutor formativo (indicato dal soggetto promotore) <%=sCognomeTut & " " & sNomeTut%><br>
		<br>
		Tutor aziendale <%=sCognomeTutAz & " " & sNomeTutAz%><br>
		<br>
		Polizze assicurative:<br> 
		- Infortuni sul lavoro INAIL posizione n. <input class="textred" size="25" name="INAIL" maxlength="25"><br>
		- ResponsabilitÓ civile posizione n. <input class="textred" size="25" name="RESCIV" maxlength="25"> compagnia <input class="textred" id="nComp" size="25" name="nComp" maxlength="25"><br><br>

		Obiettivi e modalitÓ del tirocinio:<br>
		<textarea rows="6" cols="50" id="textarea1" name="textarea1"></textarea><br><br>

		Facilitazioni previste:<br>
		<textarea rows="4" cols="50" id="textarea2" name="textarea2"></textarea><br><br>
		Obblighi del tirocinante:
		<li>seguire le indicazioni dei tutori e fare riferimento ad essi per qualsiasi esigenza di tipo organizzativo od altre evenienze;</li> 
		<li> rispettare gli obblighi di riservatezza circa processi produttivi, prodotti od altre notizie relative all'azienda di cui venga a conoscenza, sia durante che dopo lo svolgimento del tirocinio; </li>
		<li>rispettare i regolamenti aziendali e le norme in materia di igiene e sicurezza.</li>
		<br><br><br>

		(luogo)<input class="textred" size="35" name="Posto" maxlength="30"> (data) <input class="textred" id="DATAODIERNA" size="15" name="DATAODIERNA" maxlength="15"><br><br>

		Firma per presa visione ed accettazione del tirocinante ....................................... <br><br>

		Firma per il soggetto promotore ........................................................ <br><br>

		Firma per l'azienda .....................................................................<br><br>
	</p>
	</td>
	</tr>  
	</table>

	<!--- PULSANTE INVIA ---->
	<br><br>
	<table cellSpacing="4" cellPadding="4" width="500" align="center" border="0">
		<tr>
			<td align="right">
				<a href="javascript:history.back()">
					<img src="<%=session("Progetto")%>/images/indietro.gif" border="0" name="imgPunto2" onmouseover="javascript:window.status=' '; return true">
				</a>
			</td>
			<td align="left">
				<input type="image" id="IMG1" alt src="<%=session("Progetto")%>/images/conferma.gif" border="0" WIDTH="55" HEIGHT="40">
			</td>
		</tr>  
	</table>
	<br>	
</form>

<%
end sub

'------------------------------------- MAIN -----------------------------------------------------------------

dim nIdRic, sSede, idPersona

dim sNome, sCognome, dDtIni, dDtFin, sDescSede, sRagSoc
dim nIdTutor, nIdTutorAz
dim sPrvNasc, sComNasc, dDtNasc, sPrvRes, sComRes, sIndRes, sCodFisc

dim sLuogoNascita, sLuogoResidenza
dim sNomeTut, sCognomeTut
dim sNomeTutAz, sCognomeTutAz

ImpVariabili()
ImpPagina()            


%> 

<!--#include Virtual ="/Include/closeconn.asp"-->
<!--#include Virtual="/strutt_coda2.asp"-->
