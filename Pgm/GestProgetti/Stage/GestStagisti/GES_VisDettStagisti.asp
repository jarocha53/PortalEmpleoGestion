<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include virtual="/util/portallib.asp"-->
<!--#include Virtual = "/strutt_testa2.asp"-->
<!--#include Virtual ="/Include/openconn.asp"-->
<!--#include virtual="/include/SelAreaTerrBandi.asp"-->

<script language="javascript">

//include del file per fare i controlli sulla validitÓ delle date
<!--#include Virtual = "/Include/ControlDate.inc"-->

//include del file per fare i controlli sulla numericitÓ dei campi
<!--#include Virtual = "/Include/ControlString.inc"-->
<!--#include virtual = "/Include/ControlNum.inc"-->
<!--#include virtual = "/Include/help.inc"-->

///LC**:1/10/03 - modificata per malfunzionamento selezione tutor; creata VediProroghe
/*function SelTutorAz(ID,IDF){
    var NewWin
	NewWin = window.open('GES_AssTutorAz.asp?idPers=' + ID + '&idTutor=' + IDF,'info' ,'width=520,height=280,Resize=No')
}*/
function SelTutorAz(ID){	  
    var NewWin
    valTut = document.frmStage.txtTutorAzHid.value;
    NewWin = window.open('GES_AssTutorAz.asp?idPers=' + ID + '&idTutor=' + valTut,'info' ,'width=520,height=280,Resize=No')
}

function VediProroghe(IDP,IDR){	  
    var NewWin
    var DATA 
    DATA = frmStage.txtAl.value
	NewWin = window.open('GES_ModProroga.asp?idPers=' + IDP + '&idRick=' + IDR + '&DTMAX=' + DATA,'info' ,'width=422,height=280,Resize=No,Scrollbars=yes')
}
///FINE LC**.

function ApriDett(ID,IDRIC){
	var cod
	cod= document.frmStage.cmbEsito.value; 
	if (cod=="04"){
		windowArea = window.open ('GES_DettEspro.asp?ID=' + ID + '&Rich=' + IDRIC,'Info','width=600,height=450,Resize=No,Scrollbars=yes')	
    }
    if (cod=="05"){
        var esito
		var DATA 
		esito =controlliDate()
		if (esito==false){
			return 
		}else
        {
        DATA = frmStage.txtAl.value
    	windowArea = window.open ('GES_VisProroga.asp?ID=' + ID + '&Rich=' + IDRIC + '&DT=' + DATA,'Info','width=422,height=300,Resize=No,Scrollbars=yes')	
		}
    }
}

function SelImpresa(sArea,sCampo,sNomeCampo,sNomeCampo2){

var sArea;
var sAreaOld;

controllaFiniestra()

	switch (sArea){
	
		default:
			windowArea = window.open ('GES_SelImpresa.asp?Area=' + sArea + '&sTxtArea=' + sCampo +'&NomeCampo=' + sNomeCampo +'&NomeCampo2=' + sNomeCampo2,'Info','width=425,height=450,Resize=No,Scrollbars=yes')	
	}
}

function controllaFiniestra()
{
	var windowArea
	var windowSpecifico
	if (windowSpecifico != null )
		{
		nomefinestra.close(); 
		}
	if (windowArea != null )
	 
		{
		nomefinestra.close(); 
		}
}
	
function controlliDate(){
	frmStage.txtDal.value = TRIM(frmStage.txtDal.value)
    if(frmStage.txtDal.value==""){
        alert("Indicare la data di inizio stage")
        return false
    }
    
	if(document.frmStage.txtDal.value != ""){
		if (!ValidateInputDate(document.frmStage.txtDal.value)){
			document.frmStage.txtDal.focus(); 
			return false
		}
	}
	
	DataInizio = frmStage.txtDal.value
	DataOdierna=document.frmStage.txtIniDom.value
	if (ValidateRangeDate(DataOdierna,DataInizio)==false){
		alert("La data di inizio stage non deve essere minore della data di inizio stage prevista dall'azienda!")
		frmStage.txtDal.focus()
	    return false
	}
	
	frmStage.txtAl.value = TRIM(frmStage.txtAl.value)
    if(frmStage.txtAl.value==""){
        alert("Indicare la data di fine stage")
        document.frmStage.txtAl.focus();
        return false
    }
    
    if(document.frmStage.txtAl.value != ""){
		if (!ValidateInputDate(document.frmStage.txtAl.value)){
			document.frmStage.txtAl.focus(); 
			return false
		}
	}
	
	DataFine = frmStage.txtAl.value
	DataOdierna=document.frmStage.txtDal.value
	if (ValidateRangeDate(DataOdierna,DataFine)==false){
		alert("La data di fine stage non deve essere minore della data di inizio stage")
		frmStage.txtDal.focus()
	    return false
	}
	
	DataFine = frmStage.txtAl.value
	DataOdierna=document.frmStage.txtFineDom.value
	if (ValidateRangeDate(DataFine,DataOdierna)==false){
		alert("La data di fine stage non deve essere maggiore della data di fine stage prevista dall'azienda!")
		frmStage.txtAl.focus()
	    return false
	}
}

function controlli(){
    
    if(document.frmStage.txtTutorAzHid.value == ""){
		alert("Tutor aziendale obbligatorio")
		document.frmStage.txtTutorAz.focus(); 
		return false   
    }
    if(document.frmStage.txtDiretta.value == "N"){
		if(frmStage.txtRagione.value=="" && frmStage.txtImpresa.value==""){
			alert("Sede stage obbligatoria")
			return false
		}   
    }
    
    var esito
    esito =controlliDate()
	
	if (esito==false){
		return false
	}
	
	if (document.frmStage.cmbEsito.value=="05" && document.frmStage.txtContrProroga.value!=1){
		alert("Se si seleziona Proroga stage,devi compilare i dati relativi alla proroga \n utilizzando il pulsantino blu del campo esito stage.")
        return false
	}
	
	if (document.frmStage.cmbEsito.value=="04" && document.frmStage.txtContrEsperienze.value!=1){
		alert("Se si seleziona Stage Terminato Positivamente,devi compilare i dati relativi all'esperienza professionale")
        return false
	}
	
}

</script>

<%	
'********************************
'*********** MAIN ***************

'if ValidateService(session("idutente"),"GES_VISSTAGE",cc) <> "true" then 
'	response.redirect "/util/error_login.asp"
'end if
	
Inizio()

Imposta_Pag()

'**********************************************************************************************************************************************************************************	
	
Sub Imposta_Pag() 

		sSetProv = SetProvUorg(Session("idUorg"))
		sNome = Request.Form ("txtNome")
		sCognome = Request.Form ("txtCognome")
		IdSede = Request.Form ("txtSede")
		sIdPers = Request.Form ("txtIdPers")
		sIdrich= Request.Form ("txtIdRichiesta")
	
		sSQL="SELECT A.DESCRIZIONE,B.RAG_SOC,B.COD_TIMPR " &_
			 "FROM SEDE_IMPRESA A,IMPRESA B,PERS_STAGE PS " &_
			 "WHERE A.ID_IMPRESA=B.ID_IMPRESA " &_
			 " AND A.ID_SEDE= PS.ID_SEDEDEST" &_
			 " AND PS.ID_PERSONA = " & sIdPers &_
		     " AND PS.ID_RICHSTAGE = " & sIdrich
		
		     
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
		Set rstSede = CC.execute(sSQL)
		if not rstSede.eof then
			DescSede=rstSede("DESCRIZIONE")
			DescRagione=rstSede("RAG_SOC")
		else
		    DescSede=""
		    DescRagione=""
		end if
		rstSede.Close()
		set rstsede = nothing
		
		sSQL="SELECT COD_ESPLA ,DT_INIZIO,DT_FINE " &_
			 "FROM PERS_STAGE " &_
			 "WHERE ID_PERSONA = " & sIdPers &_
			 " AND ID_RICHSTAGE=" & sIdrich
		
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
		Set rstDate = CC.execute(sSQL)
		if not rstDate.eof then
			sCodice= rstDate("COD_ESPLA") & "*"
			if sCodice ="*" then
				sCodice = ""
			else
				sCodice = rstDate("COD_ESPLA")
			end if
			sInizio=rstDate("DT_INIZIO")
			sFine=rstDate("DT_FINE")
		else
		    sCodice = ""
		    sInizio = ""
		    sFine = ""
		end if
		rstDate.Close()
		set rstDate = nothing
		
		sSQL ="SELECT ID_TUTORAZ,NOME,COGNOME FROM PERS_STAGE PS,UTENTE U" &_
		      " WHERE ID_TUTORAZ = IDUTENTE AND ID_PERSONA=" & sIdPers
		
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
		Set rstTutor = CC.execute(sSQL)
		if not rstTutor.eof then
		      sIdTutorAz=rstTutor("ID_TUTORAZ")
		      sNomeTAz=rstTutor("NOME")
		      sCognomeTAz=rstTutor("COGNOME")
		else  
		      sIdTutorAz=""
		      sNomeTAz=""
		      sCognomeTAz=""
		end if
		rstTutor.Close()
		set rstTutor = nothing   
		
		sSQL="SELECT DT_INISTAGE,DT_FINSTAGE,FL_DOMDIRETTA " &_
			 "FROM DOMANDA_STAGE " &_
			 "WHERE ID_RICHSTAGE=" & sIdrich
		
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
		Set rstDom = CC.execute(sSQL)
		    sDiretta =rstDom("FL_DOMDIRETTA")
			sInizioDom=rstDom("DT_INISTAGE")
			sFineDom=rstDom("DT_FINSTAGE")
		
		rstDom.Close()
		set rstDom = nothing   
%>
	<form name="frmStage" action="GES_CnfDettStagisti.asp" method="post" onsubmit="return controlli()">
		<input type="hidden" name="txtContrProroga">
        <input type="hidden" name="txtContrEsperienze">
        <input type="hidden" name="txtIdSede" value="<%=IdSede%>">
        <input type="hidden" name="txtRichiesta" value="<%=sIdrich%>">
        <input type="hidden" name="txtPersona" value="<%=sIdPers%>">
        <input type="hidden" name="txtIniDom" value="<%=sInizioDom%>">
        <input type="hidden" name="txtFineDom" value="<%=sFineDom%>">
        <input type="hidden" name="txtDiretta" value="<%=sDiretta%>">

		<input type="hidden" name="txtDescSede" value="<%=Request.Form("txtDescSede")%>">
		<input type="hidden" name="txtRagSoc" value="<%=Request.Form("txtRagSoc")%>">
		<input type="hidden" name="txtIdTut" value="<%=Request.Form("txtIdTut")%>">
		<input type="hidden" name="txtIdTutAz" value="<%=Request.Form("txtIdTutAz")%>">


		<table border="0" cellpadding="2" cellspacing="2" width="515" align="center">
			<tr>
			    <td align="left" class="tbltext1" width="100">
					<b>Stagista </b>
				</td>
				<td nowrap>
          			<span class="textblack">
			           <%=sNome%>&nbsp;<%=sCognome%>
			        </span>
			    </td>
			</tr>
			<tr>
			    <td align="left" class="tbltext1" width="100">
					<b>ValiditÓ Stage</b>
				</td>
				<td nowrap>
          			<span class="textblack">
          			   <%=sInizioDom%> - <%=sFineDom%>
			       </span>
			    </td>
			</tr>
			<tr class="tbltext1">
  				<td align="left">
  					<b>Tutor Aziendale*</b>
				</td>
				<td nowrap>
					<span class="tbltext">
						<input type="text" name="txtTutorAz" style="TEXT-TRANSFORM: uppercase;" class="textblack" size="50" value="<%=sNomeTAz%>&nbsp;<%=sCognomeTAz%>" readonly>
						<input type="hidden" id="txtTutorAzHid" name="txtTutorAzHid" value="<%=sIdTutorAz%>">
					<%
					'''LC**:1/10/03 - aggiunto controllo "COD_ESPLA" e modificata "SelTutorAz".
					if sCodice <> "05" and sCodice <> "" then  %>
						&nbsp;
					<%
					else %>
						<!--a title="Seleziona Tutor aziendale" href="Javascript:SelTutorAz('<%'''=sIdPers%>','<%'''=sIdTutorAz%>')" ID="imgPunto1" name="imgPunto1" onmouseover="javascript:window.status='';return true"><img border="0" src="<%=Session("Progetto")%>/images/bullet1.gif"></a-->
						<a title="Seleziona Tutor aziendale" href="Javascript:SelTutorAz('<%=sIdPers%>')" ID="imgPunto1" name="imgPunto1" onmouseover="javascript:window.status='';return true"><img border="0" src="<%=Session("Progetto")%>/images/bullet1.gif"></a>
					<%
					end if %>
					</span>				
				</td>
			</tr>
<%'  if sDiretta="N" then %>   		
	<!--		<tr class="tbltext1">				<td align="left">					<strong>					   Sede Stage					</strong>				</td>					<td nowrap>					<span class="tbltext">					<%'txtImpresa%>					<input type="hidden" name="txtRagione" value="<%'=DescRagione%>">					<input type="hidden" name="txtImpresa" value="<%'=DescSede%>"> 					<input type="hidden" name="txtImpresaHid">					<input type="hidden" name="txtRagioneHid">					<textarea rows="4" cols="40" id="textarea1" name="textarea1" readonly class="textblacka"><%'=DescRagione%>&nbsp;<%'=DescSede%></textarea>					<%					'''LC**:01/10/2003 - aggiunto controllo COD_ESPLA					'if sCodice <> "05" and sCodice <> "" then  %>						&nbsp;					<%					'else %>					<a href="Javascript:SelImpresa('SEDE_IMPRESA','ID_SEDE','txtImpresa','txtRagione')" ID="imgPunto2" name="imgPunto2"><img border="0" src="<%'=Session("Progetto")%>/images/bullet1.gif" onmouseover="javascript:window.status='';return true"></a>					<%					'end if %>					</span>				</td>			</tr>	-->
<% '  end if %>		
			<tr class="tbltext1">
  				<td align="left">
  					<b>Periodo Stage (dal/al)</b> (gg/mm/aaaa)
				</td>
				<td nowrap>
					<span class="tbltext">
					<%
					'''LC**:01/10/2003 - aggiunto controllo COD_ESPLA
					if sCodice <> "05" and sCodice <> "" then  %>
   						<input type="text" name="txtDal" style="TEXT-TRANSFORM: uppercase;" class="textblack" size="12" maxlength="10" value="<%=sInizio%>" readonly>	/     <input type="text" name="txtAl" style="TEXT-TRANSFORM: uppercase;" class="textblack" size="12" maxlength="10" value="<%=sFine%>" readonly>
					<%
					else %>
					    <input type="text" name="txtDal" style="TEXT-TRANSFORM: uppercase;" class="textblack" size="12" maxlength="10" value="<%=sInizio%>">	/     <input type="text" name="txtAl" style="TEXT-TRANSFORM: uppercase;" class="textblack" size="12" maxlength="10" value="<%=sFine%>">
					<%
					end if %>
					</span>				
				</td>
			</tr>
			<tr>
				<td align="left" class="tbltext1" width="100">
					<b>Esito Stage</b>
				</td>
			    <td align="left" colspan="2" width="60%">
					<%
					if isnull(sFine) then
						sInt = "ESPLA|0|" & date & "|" & sCodice & "|cmbEsito'| AND CODICE NOT IN ('04','05') ORDER BY DESCRIZIONE"			
						CreateCombo(sInt)
					else
						if sCodice <> "05" and sCodice <> "" then
							sInt = "ESPLA|0|" & date & "|" & sCodice & "|cmbEsito' disabled color='|ORDER BY DESCRIZIONE"			
							CreateCombo(sInt)
						else 
							sInt = "ESPLA|0|" & date & "|" & sCodice & "|cmbEsito'|ORDER BY DESCRIZIONE"			
							CreateCombo(sInt)
						end if
					end if
					%>
			    </td>
			    <td>
						<a href="Javascript:ApriDett('<%=sIdPers%>','<%=sIdrich%>')" ID="imgPunto1" name="imgPunto1" onmouseover="javascript:window.status='';return true"><img border="0" src="<%=Session("Progetto")%>/images/bullet1.gif"></a>
			    </td>
			</tr>
			<%
			sSqlPror =	"select count(id_proroga) as numpror from proroga_stage" &_
						" where id_richstage=" & sIdrich &_
						" and id_persona=" & sIdPers
					
'PL-SQL * T-SQL  
SSQLPROR = TransformPLSQLToTSQL (SSQLPROR) 
			set rsPror = CC.Execute(sSqlPror)
			If cint(rsPror("numpror")) <> 0 then			
			%>
			<tr>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td align="right" colspan="3" class="textred">
					<b><a class="textred" href="Javascript:VediProroghe(<%=sIdPers%>,<%=sIdrich%>);" name="nPror" title="Visualizza proroghe">
					Visualizza Proroghe
					</a></b>
			    </td>
			</tr>
			<%
			End if
			rsPror.close
			set rsPror=nothing%>
		</table>
		<br><br>
		
		<table border="0" cellpadding="0" cellspacing="1" width="300">
			<tr>
		<%	if sCodice = "05" or sCodice = "" then %>
				<td nowrap>
			       <input type="image" name="Conferma" src="<%=Session("progetto")%>/images/conferma.gif">
			    </td value="Registra">
		<%  end if %>
				<td align="center">
					<a href="javascript:history.go(-1)">
						<img src="<%=session("Progetto")%>/images/indietro.gif" border="0" name="imgPunto2" onmouseover="javascript:window.status=' '; return true">
					</a>
				</td>
			</tr>
		</table>
    </form>
    
<%  End Sub 

	Sub Inizio()
%>
<!-- Lingetta superiore 	Scrittura dell'intestazione della pagina-->

	<table border="0" width="520" cellspacing="0" cellpadding="0" height="70" align="center">
		<tr>
			<td width="500" background="<%=session("progetto")%>/images/titoli/strumenti2b.gif" height="70" valign="bottom" align="right">
				<table border="0" background width="500" height="23" cellspacing="0" cellpadding="0">
					<tr>
						<td width="100%" valign="top" align="right"><b CLASS="tbltext1a">Gestione stagisti
						  </b></td>
					</tr>
				</table>
			</td>
		</tr>
	</table> 
	<br>
	<table border="0" CELLPADDING="0" CELLSPACING="0" width="500" align="center">
		<tr height="18">
			<td class="sfondomenu" width="67%" height="18"><span class="tbltext0"><b>&nbsp;VISUALIZZAZIONE DETTAGLIO STAGISTI </b></span></td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
		  	<td valign="middle" align="right" class="tbltext1" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">(*)campo obbligatorio</td>
		</tr>
	</table>
	<!-- Commento -->
	<table border="0" CELLPADDING="0" CELLSPACING="0" width="500" align="center">
		<tr>
			<td align="left" class="sfondocomm">

                       Premere su <b>invia</b> per validare i dati selezionati. <br>
                       Premere su <b>indietro</b> per tornare alla lista degli stagisti.    
				<a href="Javascript:Show_Help('/pgm/help/gestprogetti/stage/GestStagisti/GES_VisDettStagisti')"><img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
			</td>
		</tr>
		<tr height="2">
			<td class="sfondocommaz" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>

<%	
	End Sub

%>	
<!--#include Virtual ="/Include/closeconn.asp"-->
<!--#include Virtual="/strutt_coda2.asp"-->		

