<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->

<!--#include Virtual = '/strutt_testa2.asp'-->
<% 
'-------------------------------------------------------------------------------------------------------------

sub CreaDoc()
	
	' Controllo che non esista una file con lo stesso nome		
	If (fso.FileExists(sFileEdit)) then
		Response.Write "<span class='tbltext3'>" & "Esiste giÓ un documento con lo stesso titolo." & "<br>" & "Impossibile sovrascrivere il documento" & "</SPAN>"
		Response.Write "<br><br><br><br><a href='' onclick='Javascript:history.back(); return false'><IMG SRC=" & Session("Progetto") & "/images/indietro.gif border=0></a>"
		Response.End
	End if

	sNomeFile = split(sFileEdit,Server.MapPath("\"))

	SP = Session("Progetto")
	Sp = replace(SP,"/","\")

	'COSTRUISCO IL DOCUMENTO
	sHead = "<html><head><link REL='STYLESHEET' TYPE='text/css' HREF='/Pgm/BancaDati/fogliostile.css'></head>"
'	sTestata = "<table widht='100%' border='0'><tr><td align=left><IMG SRC='/images/Italialog.gif' border=0></td></tr></table><HR>" 
	
	sTesto = "<table widht='100%' border='0'><tr><td class='tbltext' align='left' colspan='2'>" & _
			"<p align='center' class='tbltext1'>" & "MODELLO DI PROGETTO FORMATIVO" & "<br>" & _ 
			"(su carta intestata del soggetto promotore)" & "<br><br>"& _ 
			"<b>" & "PROGETTO FORMATIVO E DI ORIENTAMENTO" & "</B><br>"  & _ 
			"(rif. Convenzione n." & request.form("nConv") & "stipulata in data" & request.form("dataConv")& ")</p>"
			
	sTesto = sTesto & "<br><br><p align='justify'>Nominativo del tirocinante " & request.form("Cognome")& " " & request.form("Nome")& "<br>" & _ 
			"nato a " & request.form("LUOGO")& "<br>" & _
			" il " & request.form("dataTir") & "<br>" & _
			" residente in " & request.form("RESTIR") & "<br>" &_
			"codice fiscale " & request.form("CODFISCTIR") & " <br><br> " &_
			"<b>Attuale condizione (barrare la casella):</b><br><ul><div class='tbltext'> </p>"
			
	sTesto = sTesto & "<table class='tbltext' border='0' align='center'>"

		if Request.Form("condiz")= 1 then
		ncheck="checked"
		Else
		ncheck="disabled"
		end if

		sTesto = sTesto & "<tr><td>studente scuola secondaria superiore</td><td>" & "<input type='radio' name='prova' "  & ncheck & ">" 

		if Request.Form("condiz")= 2 then
		ncheck1="checked"
		Else
		ncheck1="disabled"
		end if

		sTesto = sTesto & "<tr><td>universitario</td><td>"& "<input type='radio' name='prova' "  & ncheck1 & ">"

		if Request.Form("condiz")=3 then
		ncheck2="checked"
		Else
		ncheck2="disabled"
		end if

		sTesto = sTesto & "<tr><td>frequentante corso post-diploma</td><td>" & "<input type='radio' name='prova' "  & ncheck2 & ">"  


		if Request.Form("condiz")=4 then
		ncheck3="checked"
		Else
		ncheck3="disabled"
		end if

		sTesto = sTesto & "<tr><td>frequentante corso post-laurea</td><td>" & "<input type='radio' name='prova' "  & ncheck3 & ">" 

		if Request.Form("condiz")=5 then
		ncheck4="checked"
		Else
		ncheck4="disabled"
		end if

		sTesto = sTesto & " <tr><td>allievo della formazione professionale</td><td>" & "<input type='radio' name='prova' "  & ncheck4 & ">" 

		if Request.Form("condiz")=6 then
		ncheck5="checked"
		Else
		ncheck5="disabled"
		end if

		sTesto = sTesto & "<tr><td>disoccupato/in mobilitÓ</td><td>" & "<input type='radio' name='prova' "  & ncheck5 & ">"

		if Request.Form("condiz")=7 then
		ncheck6="checked"
		Else
		ncheck6="disabled"
		end if

		sTesto = sTesto & "<tr><td>inoccupato</td><td>" & "<input type='radio' name='prova' "  & ncheck6 & ">" & _
				"</div></table>" 

		if Request.Form("condizH")=1 then
		ncheck7="checked"
		Else
		ncheck7="disabled"
		end if

		sTesto = sTesto & "(barrare se trattasi di soggetto portatore di handicap) si" & "<input type='radio' name='HANDICAP' "  & ncheck7 & ">"  

		if Request.Form("condizH")=2 then
		ncheck8="checked"
		Else
		ncheck8="disabled"
		end if

	sTesto = sTesto & "no"  & "<input type='radio' name='HANDICAP' "  & ncheck8 & ">" & "</ul>" &_
		"<span class='tbltext'>Azienda ospitante &nbsp;" & request.form("NAZOSP")& "<BR>" &_
		"Sede/i del tirocinio (stabilimento/reparto/ufficio): &nbsp;" & request.form("SEDETIR")& "<br>" &_ 
		"Tempi di accesso ai locali aziendali:  &nbsp;" & request.form("TEMPI")& "<br>"&_
		"Periodo di tirocinio n. mesi: &nbsp;"& request.form("nMESI")& "&nbsp;" & "dal &nbsp;" & request.form("INTERDA")& "&nbsp;" & "al &nbsp;" & request.form("INTERAL") & "<br>"&_
		"Tutor formativo (indicato dal soggetto promotore) " & request.form("CTUTORFOR") & " " & request.form("NTUTORFOR")& "<br>" &_ 
		"Tutor aziendale  &nbsp;" & request.form("CTUTORAZ") & " " & request.form("NTUTORAZ") & "<br>" &_ 
		"Polizze assicurative:<br>"&_ 
		"- Infortuni sul lavoro INAIL posizione n. &nbsp;" & request.form("INAIL")& "<br>" &_
		"- ResponsabilitÓ civile posizione n.  &nbsp;" & request.form("RESCIV")& " " & "Compagnia: " & request.form("NCOMP")& ";<br>" & _
		"Obiettivi e modalitÓ del tirocinio:<br>"&_
		Request.form("textarea1")& "<br>"&_
		"Facilitazioni previste: <br>" &_
		Request.form("textarea2")& "<br>" &_
		"Obblighi del tirocinante:"&_
		"<li>seguire le indicazioni dei tutori e fare riferimento ad essi per qualsiasi esigenza di tipo organizzativo od altre evenienze;</li>"&_ 
		"<li> rispettare gli obblighi di riservatezza circa processi produttivi, prodotti od altre notizie relative all'azienda di cui venga a conoscenza, sia durante che dopo lo svolgimento del tirocinio; </li>"&_
		"<li>rispettare i regolamenti aziendali e le norme in materia di igiene e sicurezza.</li>" &_
		"<br><br>" & _
		Request.Form("Posto") & " " & Request.Form("DATAODIERNA")& "<br><br>"&_
		"Firma per presa visione ed accettazione del tirocinante ................................................. <br><br>"&_
		"Firma per il soggetto promotore ................................................................................. <br><br>"&_
		"Firma per l'azienda ...................................................................................................<br><br>"&_
		"</td></tr></span></table>"

	sChiudi = "<table widht='100%' border=0 align=center><tr><td><a href='javascript:window.print()'><img src='/images/stampa.gif' title='Stampa la pagina' border='0'></a></td>" & _
				"<td><a href='javascript:self.close()'><img src='/images/chiudi.gif' title='Chiudi la pagina' border='0'></a></td></tr></table>"
	%>

	<br><center>
	<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
		<tr height="18">
			<td class="sfondomenu" width="77%" height="18"><span class="tbltext0"><b>&nbsp;DOCUMENTA - INSERIMENTO NUOVO DOCUMENTO </b></span></td>
			<td width="3%" background="<%=session("progetto")%>/images/tondo_linguetta.gif"></td>
			<td valign="middle" align="right" class="tbltext1" width="20%" background="<%=session("progetto")%>/images/sfondo_linguetta.gif">&nbsp;</td>
		</tr>
	</table>
	<!-- Commento -->
	<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
		<tr>
		<td align="left" class="sfondocomm">Inserimento del documento.</td>
		</tr>
		<tr height="2">
			<td class="sfondocomm" width="100%" colspan="3" background="<%=session("progetto")%>/images/separazione.gif"></td>
		</tr>
	</table>

	<%

	if  sTitoloFile <> "" then

		NomFile = replace(sNomeFile(1),"\","/")
		NomFile = "/Pgm/gestProgetti/Stage/GestStagisti/GES_VisDocumento.asp?pag=" & NomFile

		' SCRITTURA DOCUMENTO
'		Dim fso, f
'		Set fso = CreateObject("Scripting.FileSystemObject")
		Set f = fso.CreateTextFile(sFileEdit)
		f.writeline  sHead
'		f.writeline  sTestata
		f.writeline  sTesto
		f.writeline	 sChiudi
		f.close 
		Set f = Nothing
'		Set fso = Nothing

'		ok = "<center><br><br><br><span class=tbltext3>Operazione correttamente eseguita</span><br><br>"
'		indietro="<table width='100%' border='0'><tr align='center'><td align='middle'><input type='image' src='" & session("progetto") & "/images/indietro.gif' title border='0' id='image'1 name='image'1></a></td></tr></table>"

'		Response.Write ok
'		Response.Write indietro
		
		%>
		<script language="javascript" action="">
			alert("Operazione correttamente effettuata");
			frmIndietro_VisStagisti.action="GES_VisStagisti.asp";
			frmIndietro_VisStagisti.submit();
		</script>
		<% 


	else
		Response.Write "<br><br><br><br><span class=tbltext3>Torna alla pagina di inserimento</span>"
		Response.Write "<br><br><br><br><a href=/pgm/GestProgetti/stage/geststagisti/GES_insdoc1.asp><IMG SRC=" & Session("Progetto") & "/images/indietro.gif border=0></a>"
	end if

end sub

'--------------------------- MAIN ----------------------------------------------------------------------------------

dim sNomeFile, sTitoloFile, idRichiesta, idSede, sFileEdit, fso

sNomeFile = Replace(Request.Form("txtIdPers"), " ", "")

sTitoloFile = Request.Form("txtIdPers")

idRichiesta= Request.Form("txtIdRichiesta")
idSede= Request.Form("txtSede")

sFileEdit = Session("PosDoc") & sNomeFile & ".htm"
'Response.Write "sFileEdit: " & sFileEdit & "<BR>"

'action="/Pgm/GestProgetti/stage/GestStagisti/GES_VisStagisti.asp"
%>
<form method="post" name="frmIndietro_VisStagisti" onsubmit="" action="GES_VisStagisti.asp">
	<input type="hidden" name="txtIdRichiesta" value="<%=idRichiesta%>">
	<input type="hidden" name="txtSede" value="<%=idSede%>">
	<input type="hidden" name="txtDescSede" value="<%=Request.Form("txtDescSede")%>">
	<input type="hidden" name="txtRagSoc" value="<%=Request.Form("txtRagSoc")%>">
	<input type="hidden" name="txtIdTut" value="<%=Request.Form("txtIdTut")%>">
	<input type="hidden" name="txtIdTutAz" value="<%=Request.Form("txtIdTutAz")%>">			
</form>
<%
Set fso = CreateObject("Scripting.FileSystemObject")

'cancellazione del documento
if Request.Form("MOD") = "D" then
	apNomeFile = Request.Form("txtNomeFile")
	set OggFile = fso.GetFile(Server.MapPath(apNomeFile))
	OggFile.Delete
	Set fso = Nothing

	%>
	<script language="javascript" action="">
		alert("Cancellazione correttamente effettuata");
		frmIndietro_VisStagisti.action="GES_VisStagisti.asp";
		frmIndietro_VisStagisti.submit();
	</script>
	<% 
	'Response.End
else
	CreaDoc()	
end if

Set fso = Nothing

%>
<br>
<!--#include Virtual = '/strutt_coda2.asp'-->
