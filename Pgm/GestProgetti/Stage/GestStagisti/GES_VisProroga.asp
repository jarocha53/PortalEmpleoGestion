<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include Virtual = "/include/OpenConn.asp"-->
<!--#include Virtual = "/include/ControlDateVB.asp"-->
<title>Proroga Stage</title>
<link REL="STYLESHEET" TYPE="text/css" HREF="<%=session("progetto")%>/fogliostile.css">

<script LANGUAGE="Javascript">
//include del file per fare i controlli sulla validit� delle date
<!--#include Virtual = "/Include/ControlDate.inc"-->

//include del file per fare i controlli sulla numericit� dei campi 
<!--#include Virtual = "/Include/ControlNum.inc"-->

//include del file per fare i controlli sulle stringhe
<!--#include Virtual = "/Include/ControlString.inc"-->
<!--#include Virtual = "/Include/help.inc"-->


function ControllaDati(){

		frmProroga.txtInizio.value = TRIM(frmProroga.txtInizio.value)
		
		if (frmProroga.txtInizio.value == ""){
			alert("la Data di inizio proroga � obbligatoria!")
			frmProroga.txtInizio.focus() 
			return false
		}
		
		DataInizio = frmProroga.txtInizio.value
		if (!ValidateInputDate(DataInizio)){
			frmProroga.txtInizio.focus() 
			return false
		}
		
		DataInizio = frmProroga.txtInizio.value
		DataOdierna=frmProroga.txtoggi.value
		SACTION = frmProroga.txtAction.value
		FUTURA = frmProroga.txtFutura.value
		
		if(SACTION=="INSERIMENTO" || FUTURA == "SI"){
			if (ValidateRangeDate(DataOdierna,DataInizio)==false){
				alert("La data di inizio proroga non deve essere minore della data odierna!")
				frmProroga.txtInizio.focus()
				return false
			}	
		}
				
		frmProroga.txtFine.value = TRIM(frmProroga.txtFine.value)
		
        if (frmProroga.txtFine.value == ""){
			alert("la Data di fine proroga � obbligatoria!")
			frmProroga.txtFine.focus() 
			return false
		}
		
		DataInizio = frmProroga.txtFine.value
		if (!ValidateInputDate(DataInizio)){
			frmProroga.txtFine.focus() 
			return false
		}
		
		DataInizio = frmProroga.txtFine.value
		DataOdierna=frmProroga.txtoggi.value
		if (ValidateRangeDate(DataInizio,DataOdierna)==true){
			alert("La data di fine proroga non deve essere minore della data odierna!")
			frmProroga.txtFine.focus()
		    return false
		}
		DataInizio = frmProroga.txtInizio.value
		DataFine=frmProroga.txtFine.value
		if (ValidateRangeDate(DataInizio,DataFine)==false){
			alert("La data di fine proroga non deve essere minore della data di inizio proroga!")
			frmProroga.txtFine.focus()
		    return false
		}
		
		DataFine =frmProroga.txtMassimo.value
     	DataInizio = frmProroga.txtInizio.value
		
		
		if (ValidateRangeDate(DataFine,DataInizio)==false){
			alert("La data di inizio proroga non deve essere minore della data di fine stage/ultima proroga!")
			frmProroga.txtInizio.focus()
			return false
		}
		
		frmProroga.txtGiorni.value = TRIM(frmProroga.txtGiorni.value)
		
		if (frmProroga.txtGiorni.value == ""){
			alert("Il numero di giorni � obbligatorio!")
			frmProroga.txtGiorni.focus() 
			return false
		}
		if (isNaN(frmProroga.txtGiorni.value)){
			alert("Il numero di giorni deve essere numerico")
			frmProroga.txtGiorni.focus() 
			return false
		}
	}
</script>

<!--#include Virtual = "/include/DecCod.asp"-->
<!--#include Virtual="/util/dbUtil.asp"-->

<%
dim sRic,IDP,Data,sProroga,sDataInizio,sDataFine,sGiorni,sAction,sContrData,sFutura

sRic =Request.QueryString("Rich") 
IDP= Request.QueryString("ID") 
Data= Request.QueryString("DT") 

sSQL="SELECT MAX(DT_FINEPRO) AS MASSIMO FROM PROROGA_STAGE WHERE" &_
     " ID_RICHSTAGE=" & sRic & " AND ID_PERSONA=" & IDP 

     
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
Set rstMax = CC.execute(sSQL)
	if not rstMax.eof then
	   sMassimo=rstMax("MASSIMO") & "*"
	   if sMassimo = "*" then
		  sAction = "INSERIMENTO"	
	      sProroga = "SI"
	      sMassimo = Data 
	      Descrizione = "Data di fine stage"
	   else
	      sMassimo=rstMax("MASSIMO")
	      Descrizione="Data di fine proroga precedente"
		  
		  sql="SELECT DT_INIPRO,DT_FINEPRO,NUM_GIORNI" &_
		      " FROM PROROGA_STAGE WHERE DT_FINEPRO=" & CONVDATETODBS(sMassimo)	&_
			  " AND ID_RICHSTAGE=" & sRic & " AND ID_PERSONA=" & IDP 
		  
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
		  Set rsPro = CC.execute(sql)
		  sDataInizio = ""
		  sDataFine = ""
		  sGiorni =	""		  
		  
		  if cdate(date()) > cdate(rsPro("DT_FINEPRO")) then
				sProroga = "SI"
				sAction = "INSERIMENTO"
		  end if   
		  sContrData=""
		  if cdate(rsPro("DT_INIPRO"))< cdate(date()) then
		 		sContrData ="readonly"	
		  else		
		 		sFutura = "SI"
		  end if  
		  
		  if (cdate(rsPro("DT_INIPRO")) > cdate(date())) or (cdate(date()) >= cdate(rsPro("DT_INIPRO")) and  cdate(date()) <= cdate(rsPro("DT_FINEPRO"))) then
				sProroga = "SI"
				sAction = "MODIFICA"
				sDataInizio = rsPro("DT_INIPRO")
				sDataFine = rsPro("DT_FINEPRO")
				sGiorni =	rsPro("NUM_GIORNI")
				
				sSQL="SELECT MAX(DT_FINEPRO) AS MASSIMO FROM PROROGA_STAGE WHERE" &_
					 " ID_RICHSTAGE=" & sRic & " AND ID_PERSONA=" & IDP &_
					 " AND DT_FINEPRO <=" & convdatetodbs(date())
				
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
				Set rsMax = CC.execute(sSQL)
				if not rsMax.eof then
					sMassimo=rsMax("MASSIMO") & "*"
					if sMassimo = "*" then
						sMassimo = Data 
					 else
						sMassimo=rsMax("MASSIMO")
				    end if
				else
					sMassimo = Data 
				end if 
				Set rsMax = nothing   
		  end if
		 
		  Set rsPro = nothing			 

	   end if
	else 
	   sAction = "INSERIMENTO"	
	   sProroga = "SI"
	   sMassimo = Data 
	   Descrizione ="Data di fine stage" 
	end if   
	rstMax.Close()
set rstMax = nothing     
%>

<table border="0" CELLPADDING="0" CELLSPACING="0" width="350" align="center">
	<tr height="18">
		<td class="sfondomenu" width="67%" height="18"><span class="tbltext0"><b>&nbsp;PROROGA STAGE</b></span></td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
	  	<td valign="middle" align="right" class="tbltext1" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
	</tr>
</table>
<!-- Commento -->
<table border="0" CELLPADDING="0" CELLSPACING="0" width="350" align="center">
	<tr>
		<td align="left" class="SFONDOCOMM">
                 Premere <b>Invia</b> per salvare i dati relativi alla proroga.<br>
                 Premere <b>Chiudi</b> per non selezionare nulla.    
			<a href="Javascript:Show_Help('/pgm/help/gestprogetti/stage/GestStagisti/GES_VisProroga')"><img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
		</td>
	</tr>
	<tr height="2">
		<td class="sfondocommaz" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif">
		</td>
	</tr>
</table>
<br>

<% if sProroga = "SI" then%>
	
<table border="0" cellpadding="0" cellspacing="1" width="350">
	   
	     <tr>
			<td align="middle" colspan="2" nowrap class="tbltext1">
				<p align="left">
					<strong>&nbsp;&nbsp;&nbsp;&nbsp;</strong>
					<strong><%=Descrizione%></strong>
					<strong>&nbsp;</strong>
				</p>
			</td>	
			<td align="center" colspan="2" width="60%">
				<span class="tbltext">
				     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				     &nbsp;&nbsp;&nbsp;<%=sMassimo%>
				</span>
			</td>
	    </tr>
</table>	    
<br>	   	
<form method="post" name="frmProroga" onsubmit="return ControllaDati()" action="GES_CnfProroga.asp">	
	<input type="hidden" name="txtIdPers" value="<%=IDP%>">
	<input type="hidden" name="txtIdRic" value="<%=sRic%>">
	<input type="hidden" name="txtAction" value="<%=sAction%>">
	<input type="hidden" name="txtFutura" value="<%=sFutura%>">
	<input type="hidden" name="txtMassimo" value="<%=sMassimo%>">
    <input type="hidden" name="txtoggi" value="<%=ConvDateToString(Date())%>">

	<table border="0" cellpadding="0" cellspacing="1" width="350">

	    <tr>
			<td align="middle" colspan="2" nowrap class="tbltext1">
				<p align="left">
					<strong>&nbsp;&nbsp;&nbsp;&nbsp;</strong>
					<strong>Data Inizio Proroga*</strong><font size="1"> (gg/mm/aaaa)</font>
					<strong>&nbsp;</strong>
				</p>
			</td>	
			<td align="left" colspan="2" width="60%">
				<input type="text" name="txtInizio" maxlength="10" class="textblacka" size="10" value="<%=sDataInizio%>" <%=sContrData%>>
			</td>
	    </tr>
	    <tr>
			<td align="middle" colspan="2" nowrap class="tbltext1">
				<p align="left">
					<strong>&nbsp;&nbsp;&nbsp;&nbsp;</strong>
					<strong>Data fine Proroga*</strong><font size="1"> (gg/mm/aaaa)</font>
					<strong>&nbsp;</strong>
				</p>
			</td>	
			<td align="left" colspan="2" width="60%">
				<input type="text" name="txtFine" style="TEXT-TRANSFORM: uppercase" maxlength="10" class="textblacka" size="10" value="<%=sDataFine%>">
			</td>
	    </tr>
	    <tr>
			<td align="middle" colspan="2" nowrap class="tbltext1">
				<p align="left">
					<strong>&nbsp;&nbsp;&nbsp;&nbsp;</strong>
					<strong>Numero giorni*</strong>
					<strong>&nbsp;</strong>
				</p>
			</td>
			<td align="left" colspan="2" width="60%">
				<span class="tbltext">
					<input type="text" name="txtGiorni" style="TEXT-TRANSFORM: uppercase" maxlength="4" class="textblacka" size="5" value="<%=sGiorni%>">					
				</span>
			</td>
	    </tr>
	</table>

	<br>
	
	<table border="0" cellpadding="1" cellspacing="1" width="100%">
		<tr>
			<td align="center">
				<a HREF="javascript:self.close()"><img SRC="<%=Session("Progetto")%>/images/chiudi.gif" border="0"></a>
				<input type="image" src="<%=Session("Progetto")%>/images/conferma.gif" border="0" value="Inserisci" onclick="return ControllaDati(this)" id="image1" name="image1">
		    </td>
		</tr>
	</table>
</form>
<%else%>
		<table border="0" cellpadding="1" cellspacing="1" width="100%">
		<tr>
			<td align="center" class="tbltext3">
				Non Puoi inserire proroghe in questo momento
			</td>
		</tr>
		</table>
		<br>
		<table border="0" cellpadding="1" cellspacing="1" width="100%">
		<tr>
			<td align="center">
				<input type="image" src="<%=Session("Progetto")%>/images/conferma.gif" border="0" value="Inserisci" onclick="return ControllaDati(this)" id="image1" name="image1">
		    </td>
		</tr>
	</table>
<%end if %>
<!--#include virtual = "/include/CloseConn.asp"-->

