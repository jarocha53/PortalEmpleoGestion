<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include Virtual = "/include/OpenConn.asp"-->
<!--#include Virtual = "/include/ControlDateVB.asp"-->
<title>Modifica proroghe stage</title>
<link REL="STYLESHEET" TYPE="text/css" HREF="<%=session("progetto")%>/fogliostile.css">
<script LANGUAGE="Javascript">
//include del file per fare i controlli sulla validit� delle date
<!--#include Virtual = "/Include/ControlDate.inc"-->

//include del file per fare i controlli sulla numericit� dei campi 
<!--#include Virtual = "/Include/ControlNum.inc"-->

//include del file per fare i controlli sulle stringhe
<!--#include Virtual = "/Include/ControlString.inc"-->
<!--#include Virtual = "/Include/help.inc"-->

function ControllaDati(){
	frmModProroga.txtInizio.value = TRIM(frmModProroga.txtInizio.value)
	if (frmModProroga.txtInizio.value == ""){
		alert("La Data Inizio Proroga � obbligatoria!")
		frmModProroga.txtInizio.focus() 
		return false
	}
	DataInizio = frmModProroga.txtInizio.value
	if (!ValidateInputDate(DataInizio)){
		frmModProroga.txtInizio.focus() 
		return false
	}
	//DataInizio = frmModProroga.txtInizio.value
	/*DataOdierna=frmModProroga.txtoggi.value
	if (ValidateRangeDate(DataOdierna,DataInizio)==false){
		alert("La data di inizio proroga non deve essere minore della data odierna!")
		frmModProroga.txtInizio.focus()
	    return false
	}*/

	frmModProroga.txtFine.value = TRIM(frmModProroga.txtFine.value)
    if (frmModProroga.txtFine.value == ""){
		alert("La Data Fine Proroga � obbligatoria!")
		frmModProroga.txtFine.focus() 
		return false
	}
	DataFine = frmModProroga.txtFine.value
	if (!ValidateInputDate(DataFine)){
		frmModProroga.txtFine.focus() 
		return false
	}
		
	DataFine = frmModProroga.txtFine.value
	DataOdierna=frmModProroga.txtoggi.value
	if (ValidateRangeDate(DataOdierna,DataFine)==false){
		alert("La Data Fine Proroga non deve essere minore della data odierna!")
		frmModProroga.txtFine.focus()
	    return false
	}

	DataInizio = frmModProroga.txtInizio.value
	DataFine=frmModProroga.txtFine.value
	if (ValidateRangeDate(DataInizio,DataFine)==false){
		alert("La Data Fine Proroga non deve essere minore della data di inizio proroga!")
		frmModProroga.txtFine.focus()
	    return false
	}
		
	DataMax =frmModProroga.txtMassimo.value
    DataInizio = frmModProroga.txtInizio.value
	if (ValidateRangeDate(DataMax,DataInizio)==false){
		alert("La Data Inizio Proroga non deve essere minore della data di fine stage/ultima proroga!")
		frmModProroga.txtInizio.focus()
	    return false
	}
		
	frmModProroga.txtGiorni.value = TRIM(frmModProroga.txtGiorni.value)
	if (frmModProroga.txtGiorni.value == ""){
		alert("Il numero di giorni � obbligatorio!")
		frmModProroga.txtGiorni.focus() 
		return false
	}
	if (isNaN(frmModProroga.txtGiorni.value)){
		alert("Il numero di giorni deve essere numerico")
		frmModProroga.txtGiorni.focus() 
		return false
	}
}
</script>

<!--#include Virtual = "/include/DecCod.asp"-->
<!--#include Virtual="/util/dbUtil.asp"-->

<%
dim nRic,IDP,dFineMax

nRic =Request.QueryString("idRick") 
IDP= Request.QueryString("idPers") 
dFineMAx=Request.QueryString("DTMAX")
%>

<table border="0" CELLPADDING="0" CELLSPACING="0" width="100%" align="center">
	<tr height="18">
		<td class="sfondomenu" width="67%" height="18"><span class="tbltext0"><b>&nbsp;MODIFICA PROROGHE STAGE</b></span></td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
	  	<td valign="middle" align="right" class="tbltext1" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
	</tr>
</table>
<!-- Commento -->
<table border="0" CELLPADDING="0" CELLSPACING="0" width="100%" align="center">
	<tr>
		<td align="left" class="SFONDOCOMM">
                   Elenco delle proroghe.
                   Premere <b>Chiudi</b> per uscire.    
			<a href="Javascript:Show_Help('/pgm/help/gestprogetti/stage/GestStagisti/GES_ModProroga')"><img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
		</td>
	</tr>
	<tr height="2">
		<td class="sfondocommaz" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif">
		</td>
	</tr>
</table>
<br><br>
<%
sSQLPror =	"select id_proroga, dt_finepro, dt_inipro, num_giorni, dt_tmst" &_
			" from proroga_stage where id_richstage=" & nRic &_
			" and id_persona=" & IDP & " order by id_proroga desc"
'PL-SQL * T-SQL  
SSQLPROR = TransformPLSQLToTSQL (SSQLPROR) 
set rsPror = CC.Execute(sSQLPror)

if not rsPror.eof then
%>	   	
	<form method="post" name="frmModProroga" onsubmit="return ControllaDati()" action="GES_CnfModProroga.asp">	
		<input type="hidden" name="txtIdPers" value="<%=IDP%>">
		<input type="hidden" name="txtIdRic" value="<%=nRic%>">
	    <input type="hidden" name="txtoggi" value="<%=ConvDateToString(Date())%>">

		<table border="0" cellpadding="0" cellspacing="1" width="100%">
		    <tr class="sfondocomm">
				<td align="left" class="tbltext1">
					<strong>Data Inizio Proroga</strong><br><font size="1">(gg/mm/aaaa)</font>
				</td>	
				<td align="left" class="tbltext1">
					<strong>Data Fine Proroga</strong><br><font size="1">(gg/mm/aaaa)</font>
				</td>	
				<td align="left" class="tbltext1">
					<strong>Numero giorni</strong>
				</td>
			</tr>
			<%
			interruttore=1
			do while not rsPror.eof
				if interruttore = 1 then 
					dDate = rsPror("dt_finepro") %>
					<input type="hidden" name="txtIdPror" value="<%=rsPror("id_proroga")%>">
					<input type="hidden" name="txtTmst" value="<%=rsPror("dt_tmst")%>">
					<%
					if ConvStringToDate(dDate) < Cdate(date()) then %>
					<tr>	
						<td align="center" class="textblack">
							<%=rsPror("dt_inipro")%>
						</td>
						<td align="center" class="textblack">
							<%=rsPror("dt_finepro")%>
						</td>
						<td align="center" class="textblack">
							<%=rsPror("num_giorni")%>					
						</td>
					</tr>
					<%
					else %>
					<tr>	
						<td align="center" class="textblack">
							<%=rsPror("dt_inipro")%>
						</td>
						<td align="center" class="textblack">
							<%=rsPror("dt_finepro")%>
						</td>
						<td align="center" class="textblack">
							<%=rsPror("num_giorni")%>					
						</td>
					</tr>
			<%		end if
			
					interruttore=2
				else 
					if interruttore=2 then 
						dFineMax = rsPror("dt_finepro")
						interruttore=0
					end if %>
					<tr>	
						<td align="center" class="textblack">
							<%=rsPror("dt_inipro")%>
						</td>
						<td align="center" class="textblack">
							<%=rsPror("dt_finepro")%>
						</td>
						<td align="center" class="textblack">
							<%=rsPror("num_giorni")%>
						</td>
					</tr>
				<%	
				end if
			rsPror.movenext
			loop 
			
			rsPror.close
			set rsPror=nothing %>
			<input type="hidden" name="txtMassimo" value="<%=dFineMax%>">
		</table>
		<br>
		<table border="0" cellpadding="1" cellspacing="1" width="100%">
			<tr>
				<td align="center">
					<a HREF="javascript:self.close()"><img SRC="<%=Session("Progetto")%>/images/chiudi.gif" border="0"></a>
				</td>
			</tr>
		</table>
	</form>
<%
end if %>
<!--#include virtual = "/include/CloseConn.asp"-->
