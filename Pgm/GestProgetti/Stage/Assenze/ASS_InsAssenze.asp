<html>
<!--	BLOCCO HEAD			-->
<head>
	<meta NAME="GENERATOR" Content="Microsoft Visual Studio 6.0">
	<title>Inserimento Assenza</title>
	<link REL="STYLESHEET" TYPE="text/css" HREF="<%=session("progetto")%>/fogliostile.css">

	<!--#include virtual ="/include/openconn.asp"-->
	<!--#include virtual = "/util/portallib.asp"-->
	<!--#include virtual = "/util/dbutil.asp"-->
	<!--#include virtual = "/include/DecCod.asp"-->
	<!--#include virtual = "/include/SelAreaTerrBandi.asp"-->
	<!--#include virtual = "/include/ControlDateVB.asp"-->
<script LANGUAGE="Javascript">
	<!--#include virtual = "/Include/help.inc"-->
			
	function FormRegistra_Validator(TheForm)
	{
		// MOTIVO ASSENZA
		 if ( TheForm.cmbMotivoAss.value <= "" ) 
			{
				TheForm.cmbMotivoAss.focus();
				alert("Indicare il motivo di assenza.");
				return (false);
		  	}
		  // ORE ASSENZA
		 if ( (TheForm.ore.value == "" || TheForm.ore.value == "0" || TheForm.ore.value == "00") && 
				(TheForm.minuti.value == "" || TheForm.minuti.value == "0" || TheForm.minuti.value == "00")) 
			{
				TheForm.ore.focus();
				alert("Indicare il numero di ore di assenza.");
				return (false);
		  	}
		  if ( TheForm.ore.value >= 9 ) 
			{
				TheForm.ore.focus();
				alert("Il numero massimo di ore di assenza � 8 .");
				return (false);
		  	}
		  if ( TheForm.minuti.value >= 60 ) 
			{
				TheForm.minuti.focus();
				alert("Controllare i minuti immessi .");
				return (false);
		  	}
		  if ( TheForm.ore.value == 8 && TheForm.minuti.value != "" && TheForm.minuti.value != "0" ) 
			{
				TheForm.ore.focus();
				alert("Il numero massimo di ore di assenza � 8 .");
				return (false);
		  	}
		 // NUMERICITA' CAMPI NUMERICI
		
		var dim=TheForm.ore.size;
		var anyString = TheForm.ore.value;

		for ( var i=0; i<=anyString.length-1; i++ )
			{
				if ( (anyString.charAt(i) >= "0") && (anyString.charAt(i) <= "9") )
				{
				}
				else
				{
					alert("Attenzione: valore immesso non numerico!");
					TheForm.ore.focus();
					return(false);
				}
			}
					
		var dim=TheForm.minuti.size;
		var anyString = TheForm.minuti.value;

		for ( var i=0; i<=anyString.length-1; i++ )
			{
				if ( (anyString.charAt(i) >= "0") && (anyString.charAt(i) <= "9") )
				{
				}
				else
				{
					alert("Attenzione: valore immesso non numerico!");
					TheForm.minuti.focus();
					return(false);
				}
			}
		if (confirm("Conferma l' assenza?"))
			{return true}
		else
			{return false}
				 
	}
	function Conferma()
	{
		if (confirm("Conferma l' eliminazione dell' assenza?"))
			{return true}
		else
			{return false}	
	}
			
	function Invia()
	{
		document.IForm.submit();								
	}
	
	function Destro(e) 
	{
	if (navigator.appName == 'Netscape' && 
		(e.which == 3 || e.which == 2))
		return false;
	else if (navigator.appName == 'Microsoft Internet Explorer' && 
			(event.button == 2 || event.button == 3))
		 {
			alert("Spiacenti, il tasto destro del mouse e' disabilitato");
			return false;
		  }
	return true;
	}

	document.onmousedown=Destro;
	if (document.layers) window.captureEvents(Event.MOUSEDOWN);
	window.onmousedown=Destro;

// -->
</script>
	
	
	<!--	FINE BLOCCO SCRIPT	-->
</head>
<!--	FINE BLOCCO HEAD	-->

<!--	BLOCCO ASP			-->
<%

	Sub Inizio()
%>	
	<body class="sfondocentro" onload="javascript: document.forms[0].ore.focus()">
	<basefont class="tbltext"> 
<%

	End Sub
%>
<!--------------------------------------------------------------------------------------------------------------------------------------------------------->
<%

	Sub ImpostaPag()
		dim sCombo, nPersona, nData1, Rif, sPrv, sCform, nCodAss, sCodSess
		dim nOre, dtTMST, Rif1, Rif2, icf, nMinuti
		
		nOre = Request.Querystring("ore")
		nMinuti = Request.Querystring("minuti")
		nClasse = Request.Querystring("classe")
		sDescAula = Request.Querystring ("Aula")
		icf = Request.Querystring("icf")
		nCodAss = Request.Querystring("codass") 
		if nCodAss = "0" then
			nCodAss = "002"
		end if
		
		nPersona= Request.Querystring("persona")
		sPrv = Request.Querystring("prv")
		sCform = Request.Querystring("cf")
		sCodSess = Request.Querystring("CodSess")
		dtTMST = Request.Querystring("dtTMST")%>					
	
<form name="IForm" id="IForm" onsubmit="return FormRegistra_Validator(this);" action="ASS_CnfAssenze.asp" method="POST">		
			<input type="hidden" name="aula" value="<%=sDescAula%>">
			<input type="hidden" name="data" value="<%=nData%>">
			<input type="hidden" name="classe" value="<%=nClasse%>">
			<input type="hidden" name="CodSess" value="<%=sCodSess%>">
			<input type="hidden" name="persona" value="<%=nPersona%>">
			<input type="hidden" name="icf" value="<%=icf%>">
			<input type="hidden" name="prv" value="<%=sPrv%>">
			<input type="hidden" name="cform" value="<%=sCForm%>">
			<input type="hidden" name="dttmst" value="<%=dtTMST%>">
			<input type="hidden" name="modo" value="<%=2%>">
			<input type="hidden" name="dtin" value="<%=dtin%>">
			<input type="hidden" name="dtfin" value="<%=dtfin%>">			
	<br>
	<%'------------------------------------------------------------%>
	<table border="0" CELLPADDING="0" CELLSPACING="0" width="380">

	<tr height="17">
		<td class="sfondomenu" width="67%" height="18"><span class="tbltext0">
			<b>&nbsp;Assenze</b>
		</td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">
		</td>
		<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
	</tr>
	<tr>
		<td class="SFONDOCOMM" width="57%" colspan="3">
			Premere <b>Invia</b> per inserire l'assenza.<br>
			Premere <b>Elimina</b> per eliminare l'assenza,se l'assenza e' gia' esistente.<br>
			Premere <b>Chiudi</b> per chiudere la pagina.
			<a title="Help" href="Javascript:Show_Help('/Pgm/help/Gestprogetti/Stage/Assenze/ASS_InsAssenze')">
			<img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0" onmouseover="javascript:window.status='' ; return true"></a> 
		</td>
	</tr>
	<tr height="2">
		<td class="sfondocommaz" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif">
		</td>
	</tr>
	</table>
	<%'------------------------------------------------------------%>
	<br>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr align="center">
					<td class="tbltext" colspan="2">
						<b><%=Ucase(replace(sCognome,"$", "'"))%> &nbsp;&nbsp;<%=ucase(replace(sNome,"$","'"))%> &nbsp;</b>
						
					</td>
				</tr>
				<tr>
					<td colspan="2">&nbsp;&nbsp;</td>
				</tr>
				<tr>
					<td align="center"><span class="tbltext1">
						<b>Risulta assente &nbsp;il giorno <%=nData%>
						&nbsp;</b></span>
					</td>
				</tr>
				
				<tr>
					<td colspan="2">&nbsp;&nbsp;</td>
				</tr>
			</table>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="20%" align="right"><span class="tbltext1">
						 <b>Motivo &nbsp; </b> 
						</span>
					</td>
					<td class="textblack">						
						<% 'Richiamo la funzione CreateCombo della include DecCod
							'passando come parametri: il nome della tabella di riferimento,
							'0, "", "", nome del combo
							sCombo = "ASSEN|0|" & "" & "|" & nCodAss & "|cmbMotivoAss|" & ""
							CreateCombo sCombo
						%>
					</td>
				</tr>
				<tr>
					<td colspan="2">&nbsp;&nbsp;</td>
				</tr>
				<tr>
					<td width="20%" align="right">
						<span class="tbltext1">
						 <b>Ore &nbsp; </b>
						</span>
					</td>
					<td class="tbltext">
					<% if nOre = 0 then%>
						<input type="text" name="ore" id="ore" border="0" maxlength="1" size="1">
					<%	else%>
						<input type="text" name="ore" id="ore" border="0" maxlength="1" size="1" value="<%=nOre%>">
					<%	end if%>
					</td>
				</tr>
				<tr>
					<td width="20%" align="right">
						<span class="tbltext1">
						 <b>Minuti &nbsp; </b>
						</span>
					</td>
					<td class="tbltext">
					<% if nMinuti = 0 then%>
						<input type="text" name="minuti" border="0" maxlength="2" size="2">
					<%	else%>
						<input type="text" name="minuti" border="0" maxlength="2" size="2" value="<%=nMinuti%>">
					<%	end if%>
					</td>
				</tr>
			</table>	
			<br>
			<% if (nOre <> 0 or nMinuti <> 0) then %>
			<table align="center" width="300" border="0" cellspacing="0" cellpadding="0">				
				<tr align="center">					
					<td class="tbltext">
						<!--a href="javascript:Invia()" onmouseover="window.status =' '; return true">										<img src="<%'=Session("progetto")%>/images/conferma.gif" title="Conferma" border="0">												</a-->
						<input title="Conferma" type="image" name="Conferma" src="<%=Session("progetto")%>/images/conferma.gif">						
					</td>
					</form>
					<td class="tbltext">
					<form name="can" method="post" action="javascript:window.close()">
						<a href="javascript:self.close()">
						<img src="<%=Session("progetto")%>/images/chiudi.gif" title="Chiudi la pagina" border="0" align="absBottom"></a>	
					</td>
					</form>										
					<td align="center">											
						<form name="can" method="post" action="ASS_CnfAssenze.asp" onsubmit="return Conferma()">							
							<td nowrap><input title="Elimina" type="image" name="Elimina" src="<%=Session("progetto")%>/images/elimina.gif"></td value="Elimina">
							<input type="hidden" name="modo" value="<%=3%>">
							<input type="hidden" name="aula" value="<%=sDescAula%>">
							<input type="hidden" name="data" value="<%=nData%>">
							<input type="hidden" name="classe" value="<%=nClasse%>">
							<input type="hidden" name="CodSess" value="<%=sCodSess%>">
							<input type="hidden" name="persona" value="<%=nPersona%>">
							<input type="hidden" name="icf" value="<%=icf%>">
							<input type="hidden" name="prv" value="<%=sPrv%>">
							<input type="hidden" name="cform" value="<%=sCForm%>">
							<input type="hidden" name="dttmst" value="<%=dtTMST%>">
							<input type="hidden" name="dtin" value="<%=dtin%>">
							<input type="hidden" name="dtfin" value="<%=dtfin%>">
					</td>
					</form>											
				</tr>
			</table>
			<%else%>
			<table align="center" width="200" border="0" cellspacing="0" cellpadding="0">
				<tr align="center">					
					<td class="tbltext">
						<!--a href="javascript:Invia()" onmouseover="window.status =' '; return true">										<img src="<%'=Session("progetto")%>/images/conferma.gif" title="Conferma" border="0">												</a-->						
						<input title="Conferma" type="image" name="Conferma" src="<%=Session("progetto")%>/images/conferma.gif">
					</td>
				</form>
					<td>
						<form name="can" method="post" action="javascript:window.close()">
						<a href="javascript:self.close()"><br>
						<img src="<%=Session("progetto")%>/images/chiudi.gif" title="Chiudi la pagina" border="0" align="absBottom"></a>						
						</form>
					</td>											
				</tr>
			</table>
			<% end if%>		
<%	 End Sub
%>
<!--------------------------------------------------------------------------------------------------------------------------------------------------------->
<!--	FINE BLOCCO ASP		-->
<!--		MAIN			-->

<%
	Dim strConn, sNome, sCognome, nData, nDay, nMonth, nYear, sDiritti	
%>

	
	<!--#include virtual = "/util/portallib.asp"-->
	<!--#include virtual = "/util/dbutil.asp"-->
	<!--#include virtual = "/include/DecCod.asp"-->
	
<%
   if ValidateService(session("idutente"),"ASS_CALENDARIOSTAGE",CC) <> "true" then 
	    response.redirect "/util/error_login.asp"
	end if
	
	sDiritti = Session("Diritti")
	sNome=Request.Querystring("Nome")
	sCognome=Request.Querystring("Cognome")
	dtin=Request.Querystring("dtin")
	dtfin=Request.Querystring("dtfin")
	nData = Request.Querystring("data")
	
	Inizio()
	ImpostaPag()
%>
	<!--#include virtual ="/include/closeconn.asp"-->
	<!--include virtual ="/include/fine.asp"-->
<!--	FINE BLOCCO MAIN	-->
