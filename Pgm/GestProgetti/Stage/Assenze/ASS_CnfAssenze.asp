<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<html>
<!--	BLOCCO HEAD			-->
<head>
	
	<meta NAME="GENERATOR" Content="Microsoft Visual Studio 6.0">
	<title>Gestione Registro di Classe</title>
	<link REL="STYLESHEET" TYPE="text/css" HREF="<%=session("progetto")%>/fogliostile.css">
	<!--	BLOCCO SCRIPT	-->
	<!-- JavaScript immediate script -->
	<!--#include virtual ="/include/openconn.asp"-->
	<!--#include virtual = "/util/portallib.asp"-->
	<!--#include virtual = "/util/dbutil.asp"-->
	<!--#include virtual = "/include/DecCod.asp"-->
	<!--#include virtual = "/include/SelAreaTerrBandi.asp"-->
	<!--#include virtual = "/include/ControlDateVB.asp"-->
	<!--#include virtual = "/include/SysFunction.asp"-->
		<script LANGUAGE="JavaScript">
		<!--
		var newWindow
		
		function Segnalazione(Aula,Classe,Sess)
		
		{	
			if (newWindow != null ) 
			{
				newWindow.close()  
			}
			location.href  ='ASS_InsSegnalazioni.asp?Classe=' + Classe + '&CodSess=' + Sess + '&Aula=' + Aula 			
		}
		
		function invia()
		{
		var appo= document.forms[0].classe.value
		 if ( appo == "vuoto" ) 
			{
				alert("Selezionare la Classe")
			}
		else
			document.forms[0].submit();
		}
		
		function Assente(Aula,Cognome,Nome,Classe,Persona,Data,Prv,Sess,Cform,CodAss,Ore, dtTmst, Minuti, Ediz, Comune, dtin, dtfin,Icf)
		{	
			
			if (newWindow != null ) 
			{
				newWindow.close()  
			}
			f  = 'ASS_InsAssenze.asp?cognome=' + Cognome + '&nome=' + Nome +  '&data=' + Data + '&classe=' + Classe + '&persona=' + Persona + '&Aula=' + Aula + '&IdEdiz=' + Ediz
			f += '&prv=' + Prv + '&CodSess=' + Sess  + '&cf=' + Cform  + '&ore=' + Ore + '&minuti=' + Minuti + '&codass=' + CodAss + '&dtTmst=' + dtTmst + '&Comune=' + Comune
			f += '&dtin=' + dtin + '&dtfin=' + dtfin 
			
			newWindow = window.open(f, 'Assen','width=420,height=400,Resize=no,scrollbars=yes')
			document.forms[0].reload();  
		}
		
		function Destro(e){
			if (navigator.appName == 'Netscape' && 
				(e.which == 3 || e.which == 2))
				return false;
			else if (navigator.appName == 'Microsoft Internet Explorer' && 
				(event.button == 2 || event.button == 3)) {
					//alert("Spiacenti, il tasto destro del mouse e' disabilitato");
					//return false;
				}
			return true;
		}

		document.onmousedown=Destro;
		if (document.layers) window.captureEvents(Event.MOUSEDOWN);
		window.onmousedown=Destro;
		// -->
		</script>
	
	
	<!--	FINE BLOCCO SCRIPT	-->
</head>
<!--	FINE BLOCCO HEAD	-->

<!--	BLOCCO ASP			-->
<%
	
%>
<!--------------------------------------------------------------------------------------------------------------------------------------------------------->
<%
	Sub Fine()
%>
		<!--#include Virtual ="/include/closeconn.asp"-->
		
<%
	End Sub


	Function Ins_Mod()
		'Inserimento nella tabella REG_CLASSE di una occorrenza relativa alla persona mancante
	
		dim nOre, nCodAssen, sCodErr, sql1, sql, RR, nIdPersona, dtTMST
		dim nMinuti,ang,asg,TOT,sCodSess
				
		dtTmst = request.form("dtTmst")
		sCodSess  = request.form("CodSess")
		nOre = request.form("ore")
		if nOre = "" then
			nOre = "0"
		end if
		nMinuti = request.form("minuti")
		if nMinuti = "" then
			nMinuti = "0"
		end if
		
		nCodAssen=request.form("cmbMotivoAss")
		nIdPersona=request.form("persona")

		sql1= "Select NUM_ORE From REG_CLASSE Where ID_PERSONA =" & nIdPersona
		sql1= sql1 & " And DT_ASSEN=" & ConvDateToDB(ConvStringToDate(nData))
		sql1= sql1 & " And cod_sessione = '0'" 

'PL-SQL * T-SQL  
SQL1 = TransformPLSQLToTSQL (SQL1) 
		set RR=CC.Execute(sql1)
		
		If not RR.Eof then
			
			RR.close
			sql1=""
			sql1= "Update REG_CLASSE "
			sql1= sql1 & " Set NUM_ORE =" & nOre & ", COD_ASSEN='" & nCodAssen & "', "
			sql1= sql1 & " MINUTI =" & nMinuti & ",  DT_TMST =" & ConvDateToDB(Now()) 
			sql1= sql1 & " Where COD_SESSIONE='0' AND ID_PERSONA =" & nIdPersona & " And DT_ASSEN=" & ConvDateToDB(ConvStringToDate(nData))
			
			sErrore = EseguiNoC(sql1,CC)
	'==============================================================================	
		else
			RR.close
			sql = "INSERT INTO REG_CLASSE (ID_PERSONA, DT_ASSEN, NUM_ORE, MINUTI, COD_ASSEN, COD_SESSIONE, DT_TMST) "
			sql=sql & " VALUES (" & nIdPersona & "," & ConvDateToDB(ConvStringToDate(nData)) & ", " & nOre & ", " & nMinuti & ", '" & nCodAssen & "','0'," & ConvDateToDB(Now()) & ")"

			sErrore = EseguiNoC(sql,CC)
		end if
		
		
	End Function
%>
<!--------------------------------------------------------------------------------------------------------------------------------------------------------->
<%	Function Cancella()
		'Inserimento nella tabella REG_CLASSE di una occorrenza relativa alla persona mancante
	
		dim nOre, nCodAssen, sSql, sql, RR, nIdPersona
		dim dtTMST
				
		nOre = request.form("ore")
		nCodAssen=request.form("cod")
		nIdPersona=request.form("persona")
		dtTmst = request.form("dtTmst")
		
		sql="DELETE FROM REG_CLASSE WHERE ID_PERSONA = " & nIdPersona 
		sql=sql & " AND COD_SESSIONE='0' AND  DT_ASSEN =" & ConvDateToDB(ConvStringToDate(nData))

		sErrore = EseguiNoC(sql,CC)
		
	End Function
%>
<!--------------------------------------------------------------------------------------------------------------------------------------------------------->
<!--	FINE BLOCCO ASP		-->
<!--		MAIN			-->

<%
	Dim strConn, nData, sDiritti
	Dim sPrv, sCform, sModo, nData1, RifClassi, icf, sCodSess, sComune
	Dim nClasse, sRuolo,sErrore
	Dim flDocente, flSessAttiva
	
if ValidateService(session("idutente"),"ASS_CALENDARIOSTAGE", CC) <> "true" then 
	response.redirect "/util/error_login.asp"
end if
	
	
	CC.BeginTrans

	nData = Request.Form("data")
		
	nClasse	  = request.form("classe")
	sDiritti  = Session("Diritti")
	sRuolo	  = Session("Ruolo")
	sPrv	  = request.form("prv")
	sCForm	  = request.form("cf")
	icf		  = request.form("icf")
	sCodSess  = request.form("CodSess")
	sDescAula = request.form("Aula")
	sComune   = request.form("Comune")
	sCodBando = request.form("CodBando")

	dtin  = request.form("dtin")
	dtfin = request.form("dtfin")

	nData1 = "&day=" & mid(nData,1,2) & "&month=" & mid(nData,4,2) & "&year=" & mid(nData,7,4)
	sModo=request.form("modo")
		
	if sModo= "2" then Ins_Mod()
	if sModo= "3" then Cancella()	
%>
	
<table width="500" border="0" cellspacing="2" cellpadding="1">
<tr> 
	<td colspan="2" align="middle" class="tbltext3">
  		<%if sErrore = "0" then
  			CC.CommitTrans%>
			<script language="Javascript">				
				alert("Operazione correttamente effettuata")				
				opener.document.frmReload.submit();
				self.close()				
			</script>
		<%else
		Response.Write sERRore & "err"
  			CC.RollBackTrans
			Response.Write "<BR>sErrore = " & sErrore

		end if %>
	</td>
</tr>
</table>
<%		
	Fine()
%>

<!--	FINE BLOCCO MAIN	-->
