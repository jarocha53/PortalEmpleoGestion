<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%if session("progetto")<>"" then%>
	<!--#include virtual = "/include/openconn.asp" -->
	<!--#include Virtual = "/include/DecCod.asp"-->
	<!--#include Virtual = "/include/ControlDateVB.asp"-->

	<html>
	<head>
	<title>Sede</title>
	<link REL="STYLESHEET" TYPE="text/css" HREF="<%=session("progetto")%>/fogliostile.css">

	<script language="javascript">
	<!-- #include virtual="/include/help.inc" -->

	function InviaSelezione(ValCodSede,ValDescSede){
		replace = ValDescSede.replace("$","'");
		opener.document.frmStage.txtSede.value = replace
		opener.document.frmStage.txtSedeHid.value = ValCodSede
		self.close()
	}

	function Destro(e) 
	{
		if (navigator.appName == 'Netscape' && 
			(e.which == 3 || e.which == 2))
			return false;
		else if (navigator.appName == 'Microsoft Internet Explorer' && 
				(event.button == 2 || event.button == 3))
			 {
				alert("Spiacenti, il tasto destro del mouse e' disabilitato");
				return false;
			  }
		return true;
	}

	document.onmousedown=Destro;
	if (document.layers) window.captureEvents(Event.MOUSEDOWN);
	window.onmousedown=Destro;

	</script>
	</head>

	<body class="sfondocentro">
	<form name="frmSelSede">

	<table border="0" CELLPADDING="0" CELLSPACING="0" width="380">

	<tr height="17">
		<td class="sfondomenu" width="67%" height="18"><span class="tbltext0">
			<b>&nbsp;SELEZIONE SEDE</b>
		</td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">
		</td>
		<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
	</tr>
	<tr>
		<td class="sfondocommaz" width="57%" colspan="3">
			Selezionare la Sede dall'elenco visualizzato. <br>
			Premere <b>Chiudi</b> per non selezionare nulla.
			<a title="Help" href="Javascript:Show_Help('/Pgm/help/gestprogetti/Stage/Stage_Candidature/SelSede')">
			<img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0" onmouseover="javascript:window.status='' ; return true"></a> 
		</td>
	</tr>
	<tr height="2">
		<td class="sfondocommaz" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif">
		</td>
	</tr>
	</table>

	<br>

	<table border="0" CELLPADDING="2" CELLSPACING="2" width="380">
		<tr>
			<td colspan="2" align="center">
				<a class="textred" title="Rimuovi la sede selezionata" href="Javascript:InviaSelezione('','')"><b>Rimuovi Selezione</b></a>
			</td>
		</tr>

	<%
	CodiceProvincia= Request.QueryString("CodProv")
	if CodiceProvincia <> "" then
		'sSQL = "SELECT ID_SEDE,DESCRIZIONE FROM SEDE_IMPRESA WHERE PRV='" & CodiceProvincia & "' ORDER BY DESCRIZIONE"
		sSQL = " SELECT distinct (ds.ID_SEDE),DESCRIZIONE " &_
		       " FROM SEDE_IMPRESA SI, DOMANDA_STAGE DS, sede_proj sp " &_
		       " WHERE PRV='" & CodiceProvincia &_
		       "' AND DS.ID_SEDE = SI.ID_SEDE" &_ 
			   "  AND DS.ID_SEDE = SP.ID_SEDE" &_
			   " ORDER BY DESCRIZIONE"
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
		Set rsSede = CC.Execute(sSQL)		
		if not rsSede.EOF then
			Do While Not rsSede.EOF
	%>
		<tr>
			<td nowrap width="190" class="tblsfondo">
				<a class="textblack" title="Seleziona Sede" href="Javascript:InviaSelezione('<%=rsSede("ID_SEDE")%>','<%=replace(rsSede("DESCRIZIONE"),"'","$")%>')"><b><%=ucase(rsSede("DESCRIZIONE"))%></b></a>
			</td>
			<%rsSede.MoveNext%>
			<td nowrap width="190" class="tblsfondo">
	<%		if not rsSede.EOF then
	%>				<a class="textblack" title="Seleziona Sede" href="Javascript:InviaSelezione('<%=rsSede("ID_SEDE")%>','<%=replace(rsSede("DESCRIZIONE"),"'","$")%>')"><b><%=ucase(rsSede("DESCRIZIONE"))%></b></a>
	<%			rsSede.MoveNext
			else
	%>
				&nbsp;
	<%		end if
	%>
			</td>
		</tr>
	<%
			loop
			rsSede.Close
	%>
		<tr>
			<td colspan="2" align="center">
				<a class="textred" title="Rimuovi la sede selezionata" href="Javascript:InviaSelezione('','')"><b>Rimuovi Selezione</b></a>
			</td>
		</tr>
		<%else %>
		    
			<table width="380" cellspacing="2" cellpadding="1" border="0">
			<tr align="center">
				<td class="tbltext3">
					 &nbsp;
				</td>
			</tr>
			<tr align="center">
				<td class="tbltext3">
					 <b>Nessuna sede associata alla provincia</b>
				</td>
			</tr>		
		</table>	
	<%	end if
	end if
	
	%>	
	</table>
	</form>

	<table width="380" cellspacing="2" cellpadding="1" border="0">
		<tr align="center">
			<td>
				<a href="javascript:self.close()"><img src="<%=Session("progetto")%>/images/chiudi.gif" title="Chiudi la pagina" border="0" align="absBottom"></a>
			</td>
		</tr>		
	</table>
	</body>
	</html>
	<!-- #include virtual="/include/closeconn.asp" -->
<%else%>
	<script>
		alert("Sessione scaduta")
		self.close()
	</script>
<%end if%>
