<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include virtual="/util/portallib.asp"-->
<!--#include Virtual = "/strutt_testa2.asp"-->
<!--#include Virtual ="/Include/openconn.asp"-->
<!--#include virtual="/include/SelAreaTerrBandi.asp"-->

<script language="javascript">

<!--#include virtual = "/Include/ControlDate.inc"-->

//include del file per fare i controlli sulla numericit� dei campi
<!--#include virtual = "/Include/ControlNum.inc"-->
<!--#include virtual = "/Include/help.inc"-->

function SelSede()
{
	
	if (document.frmStage.cmbProv.value =="") {
	    alert("Selezionare la provincia")
	}
	else {  
	    codice = document.frmStage.cmbProv.value;   
	    windowArea = window.open ('SelSede.asp?codProv=' + codice ,'Info','width=422,height=450,Resize=No,Scrollbars=yes')	
    }
}

function controlli(){
    if (document.frmStage.cmbProv.value =="") {
	    alert("Selezionare la provincia")
	    document.frmStage.cmbProv.focus();
	    return false;   
	}
	if(document.frmStage.txtDal.value != ""){
		if (!ValidateInputDate(document.frmStage.txtDal.value)){
			document.frmStage.txtDal.focus(); 
			return false
		}
	}
	if(document.frmStage.txtAl.value != ""){
		if (!ValidateInputDate(document.frmStage.txtAl.value)){
			document.frmStage.txtAl.focus(); 
			return false
		}
	}
	if((document.frmStage.txtDal.value == "")&&(document.frmStage.txtAl.value != "")){
	    alert("Se si indica la data Al, deve essere indicata anche la data Dal")
	    document.frmStage.txtDal.focus();
	    return false; 
	}
	if((document.frmStage.txtDal.value != "")&&(document.frmStage.txtAl.value != "")){
		if (ValidateRangeDate(document.frmStage.txtDal.value,document.frmStage.txtAl.value)==false){
				alert("La data di fine validita' non deve essere minore della data di inizio validita'")
				document.frmStage.txtDal.focus()
				return false
		}	
	}
	return true	
}

function Dettagli(ind){
    document.frmDett.txtIdRichiesta.value = ind;
    document.frmDett.submit()  
}
</script>

<%	
'********************************
'*********** MAIN ***************

if ValidateService(session("idutente"),"STA_VISSTAGE",cc) <> "true" then 
	response.redirect "/util/error_login.asp"
end if
	
dim sIdProv,sSede,sAreaProf,sdtDal,sdtAl

sIdProv=Request.Form ("cmbProv")
sSede = Request.Form ("txtSedeHid")
sDescSede = Request.Form ("txtSede")
sAreaProf =Request.Form ("cmbAreaProf")
sdtDal = Request.Form ("txtDal")
sdtAl = Request.Form ("txtAl")

'''LC**:25/09/2003
if sdtDal= "" then
	sdtDal=date()
else
   sdtDal=ConvStringToDate(sdtDal)	
end if

dData= "31/12/9999"
if sDtAl <> "" then
	sDtAl= ConvStringToDate(request("txtAl"))
else
    sDtAl=convStringtoDate(dData)	
end if
'''FINE LC**

'sIdProv="AG"

Inizio()

if sIdProv <> "" then
    visRisult()
else			
	Imposta_Pag()
end if
'**********************************************************************************************************************************************************************************	
	
	Sub Imposta_Pag() 
		sSetProv = SetProvUorg(Session("idUorg"))
%>
	<form name="frmStage" action="STA_VisStage.asp" method="post" onsubmit="return controlli()">
		<table border="0" cellpadding="2" cellspacing="2" width="515" align="center">
			<tr>
<%		
	        nProvSedi = len(sSetProv)
%>		    
				<td align="left" class="tbltext1" width="100">
					<b>Provincia*</b>
				</td>
	    
				<td align="left" width="300">			 	 
					<select NAME="cmbProv" class="textblack">			 
					<option></option>
	    
<%  	            pos_ini = 1 	                
			        for i=1 to nProvSedi/2
				        sTarga = mid(sSetProv,pos_ini,2)															
%>
				        <option value="<%=sTarga%>"><%=DecCodVal("PROV", 0, "", sTarga,"")%></option>
                    
<% 			            pos_ini = pos_ini + 2
			        next%>
	                </select>
	            </td>
			
		<tr class="tbltext1">
  			<td align="left">
  				<b>Denominazione azienda</b>
			</td>
			<td nowrap>
				<span class="tbltext">
				<input type="text" name="txtSede" style="TEXT-TRANSFORM: uppercase;" class="textblack" size="50" readonly>
				<input type="hidden" id="txtSedeHid" name="txtSedeHid">

				<a title="Seleziona Sede" href="Javascript:SelSede()" ID="imgPunto1" name="imgPunto1" onmouseover="javascript:window.status='';return true"><img border="0" src="<%=Session("Progetto")%>/images/bullet1.gif"></a>
				</span>				
			</td>
		</tr>
		<tr class="tbltext1">
  			<td align="left">
  				<b>Area Professionale</b>
			</td>
			<td nowrap>
				<span class="tbltext">
				<select NAME="cmbAreaProf" class="textblack">			 
					    <option></option>
						<%sql="SELECT ID_AREAPROF,DENOMINAZIONE FROM AREE_PROFESSIONALI ORDER BY DENOMINAZIONE"
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
						  set rsProf = cc.execute(sql)
						  if not rsProf.eof then
						       do while not rsProf.eof %>
						            <option value="<%=rsProf("ID_AREAPROF")%>"><%=rsProf("DENOMINAZIONE")%></option>
						<%           rsProf.movenext
						       loop
						  end if
						  set rsProf = nothing 
						%>				
			    </select>
				</span>				
			</td>
		</tr>
		<tr class="tbltext1">
  			<td align="left">
  				<b>Richieste valide<br>dal/al</b>&nbsp;(gg/mm/aaaa)
			</td>
			<td nowrap>
				<span class="tbltext">
				     <input type="text" name="txtDal" style="TEXT-TRANSFORM: uppercase;" class="textblack" size="12" maxlength="10">	/     <input type="text" name="txtAl" style="TEXT-TRANSFORM: uppercase;" class="textblack" size="12" maxlength="10">
			
				</span>				
			</td>
			
		</tr>
		</table>
		<br>		
       <table width="500" align="center" border="0" cellspacing="2" cellpadding="1">
			<tr height="2">
				<td align="center"><input type="image" src="<%=Session("progetto")%>/images/lente.gif" title="Conferma parametri di ricerca" name="submit" border="0" align="absBottom"></td>
			</tr>
		</table>
    </form>
    
<%  End Sub 

	Sub Inizio()
%>
<!-- Lingetta superiore 	Scrittura dell'intestazione della pagina-->
	
	<table border="0" width="520" cellspacing="0" cellpadding="0" height="70" align="center">
		<tr>
			<td width="500" background="<%=session("progetto")%>/images/titoli/strumenti2b.gif" height="70" valign="bottom" align="right">
				<table border="0" background width="500" height="23" cellspacing="0" cellpadding="0">
					<tr>
						<td width="100%" valign="top" align="right"><b CLASS="tbltext1a">Gestione Candidature
						  </b></td>
					</tr>
				</table>
			</td>
		</tr>
	</table> 
	<br>
	<table border="0" CELLPADDING="0" CELLSPACING="0" width="500" align="center">
		<tr height="18">
		<%if sIdProv <> "" then%>
			<td class="sfondomenu" width="67%" height="18"><span class="tbltext0"><b>&nbsp;VISUALIZZAZIONE RICHIESTE STAGE </b></span></td>
		<%else%>
		   	<td class="sfondomenu" width="67%" height="18"><span class="tbltext0"><b>&nbsp;RICERCA RICHIESTE STAGE </b></span></td>
		<%end if%>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
		  	<td valign="middle" align="right" class="tbltext1" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
		</tr>
	</table>
	<!-- Commento -->
	<table border="0" CELLPADDING="0" CELLSPACING="0" width="500" align="center">
		<tr>
			<td align="left" class="sfondocomm">
<%			    if sIdProv <> "" then %>
                      Selezionare <b> Rif. Rich. </b> per visualizzare il dettaglio dello stage.<br>
                      Selezionare <b> Indietro </b> per effettuare un'altra ricerca. 
<%              else  %>
                       Selezionare uno o piu' parametri per effettuare la ricerca.<br>
                       La ricerca, se non vengono impostati i campi data, verr� effettuata a partire 
                       dalla data odierna.  
<%              end if  %>
				<a href="Javascript:Show_Help('/pgm/help/gestprogetti/stage/Stage_Candidature/STA_VisStage')"><img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
			</td>
		</tr>
		<tr height="2">
			<td class="sfondocommaz" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>

<%	
	End Sub

	Sub visRisult()
	
	'''LC**:25/09/2003 - modificata sql per errata lettura date in ricerca e per controllo su sede_proj.
	
		'''sSQL ="SELECT ID_RICHSTAGE,ID_SEDE,ID_AREAPROF,NUM_POSTI_DISP,DT_INIVAL,DT_FIN_VAL " &_
		'''      "FROM DOMANDA_STAGE WHERE ID_SEDE IN " &_
		'''      " (SELECT ID_SEDE FROM SEDE_IMPRESA WHERE PRV ='" & sIdProv & "')" &_
		'''      " AND DT_INIVAL <=" & convDateToDbs(now) &_
		'''      " AND (DT_FIN_VAL IS NULL OR DT_FIN_VAL >= " & convDateToDbs(now) & ")"
		'''if sSede <> "" then
		'''     sSQL = sSQL & " AND ID_SEDE=" & sSede
		'''end if	      
		'''if sAreaProf <> "" then
		'''     sSQL = sSQL & " AND ID_AREAPROF=" & sAreaProf
		'''end if
		'''if sdtDal <> "" then
		'''    sSQL = sSQL & " AND DT_INIVAL >=" & convDatetodbs(sdtDal)
		'''end if
		'''if sdtAl <> "" then
		'''    sSQL = sSQL & " AND DT_FIN_VAL <=" & convDatetodbs(sdtAl)
		'''end if
		'''sSQL = sSQL & " ORDER BY ID_RICHSTAGE"
    
		sSQL =	"SELECT ID_RICHSTAGE,DS.ID_SEDE,ID_AREAPROF,NUM_POSTI_DISP,DT_INIVAL,DT_FIN_VAL " &_
				"FROM DOMANDA_STAGE DS, SEDE_PROJ SP " &_
				"WHERE DS.ID_SEDE=SP.ID_SEDE " &_
				"AND DS.ID_SEDE IN (SELECT ID_SEDE FROM SEDE_IMPRESA WHERE PRV ='" & sIdProv & "') "			
		if sSede <> "" then
		     sSQL = sSQL & " AND DS.ID_SEDE=" & sSede
		end if	      
		if sAreaProf <> "" then
		     sSQL = sSQL & " AND ID_AREAPROF=" & sAreaProf
		end if
		sSQL =	sSQL  + " AND (" &_
				convDatetodbs(sdtDal) & " BETWEEN DT_INIVAL AND NVL(DT_FIN_VAL,TO_DATE('31/12/9999','dd/mm/yyyy')) " &_
				"OR " &_
				convDatetodbs(sdtAl) & " BETWEEN DT_INIVAL AND NVL(DT_FIN_VAL,TO_DATE('31/12/9999','dd/mm/yyyy')) " &_
				"OR " &_
				"DT_INIVAL BETWEEN " & convDatetodbs(sdtDal) & " AND " & convDatetodbs(sdtAl) &_
				") " &_
				"AND (DS.PROGETTO ='" & mid(session("progetto"),2) & "' OR DS.PROGETTO IS NULL) " &_
				"AND SP.COD_CPROJ='" & mid(session("progetto"),2) & "' " &_ 
				"ORDER BY ID_RICHSTAGE"
				
	'''FINE LC**

'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
    SET rsRisultato = cc.execute(sSQL)
    
    if not rsRisultato.eof then %>
       <form name="frmDett" method="post" action="STA_VisDettStage.asp">
           <input type="hidden" name="txtIdRichiesta">
           <input type="hidden" name="txtProvincia" value="<%=sIdProv%>">
           <input type="hidden" name="txtSede" value="<%=sSede%>">
           <input type="hidden" name="txtDescSede" value="<%=sDescSede%>">
           <input type="hidden" name="txtAreaP" value="<%=sAreaProf%>">
           <input type="hidden" name="txtDataDal" value="<%=sdtDal%>">
           <input type="hidden" name="txtDataAl" value="<%=sdtAl%>">
       </form>
      
		<table width="500" align="center" border="0" cellspacing="2" cellpadding="1" class="sfondocommaz">
            <tr height="20"> 
			  	<td class="sfondocomm" align="middle" width="10">
					<b class="tbltext1">Rif. Rich.</b>
				</td>
				<td class="sfondocomm" align="middle" width="120">
					<b class="tbltext1">Denominazione Azienda</b>
				</td>
				<td class="sfondocomm" align="middle" width="220">
					<b class="tbltext1">Area Professionale</b>
				</td>
				<td class="sfondocomm" align="middle" width="40">
					<b class="tbltext1">Numero Candidati</b>
				</td>
				<td class="sfondocomm" align="middle" width="100">
					<b class="tbltext1">Validita' Richiesta<br>Dal - Al</b>
				</td>
			</tr>
<%      do while not rsRisultato.eof 
%>			<tr height="20" class="tblsfondo"> 
			  	<td class="tbldett" align="middle">
			  	    <a class="tblAgg" HREF="Javascript:Dettagli('<%=rsRisultato("ID_RICHSTAGE")%>')" onmouseover="javascript:window.status=' '; return true">
					<b><%=rsRisultato("ID_RICHSTAGE")%></b>
					</a>
				</td>
				<td class="tbldett" align="middle">
					<% sSQl="SELECT DESCRIZIONE,RAG_SOC FROM IMPRESA I,SEDE_IMPRESA SI" &_
                            " WHERE I.ID_IMPRESA = SI.ID_IMPRESA AND ID_SEDE =" & rsRisultato("ID_SEDE")
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
                         Set rsSede = CC.Execute(sSQL)		
		                 if not rsSede.EOF then
							appoSede=rsSede("RAG_SOC")& " -<br>" & rsSede("DESCRIZIONE")
		                 else
		                    apposede="-"   
		                 end if
		                 Set rsSede =nothing
		                 Response.Write apposede 
		            %>
				</td>
				<td class="tbldett" align="middle">
					<%=DenominazioneArea(rsRisultato("ID_AREAPROF"))%>
				</td>
				<td align="middle">
					<span class="tbldett"><%=rsRisultato("NUM_POSTI_DISP")%></span>
				</td>
				<td align="middle">
					<span class="tbldett">&nbsp;&nbsp;<%=rsRisultato("DT_INIVAL")%> - <%=rsRisultato("DT_FIN_VAL")%></span>
				</td>
			</tr>
<%      rsRisultato.movenext
        loop
%>			
		</table>
<%  else
       Testo = "Non ci sono richieste di stage per i parametri selezionati"
    
%>		
		<br><br>
		<table width="500" align="center" border="0" cellspacing="2" cellpadding="1" class="sfondocommaz">
			<tr height="20"> 
			  	<td class="tbltext3" align="middle" width="500">
					<b><%=Testo%></b>
				</td>
			</tr>
		</table>
<%end if %>		
		<br>
		<br>
		<table border="0" cellpadding="0" cellspacing="1" width="500">
			<tr>
				<td align="center">
					<a href="STA_VisStage.asp">
						<img src="<%=session("Progetto")%>/images/indietro.gif" border="0" name="imgPunto2" onmouseover="javascript:window.status=' '; return true">
					</a>
				</td>
			</tr>
		</table>
		<br>
<%				
	End Sub
	
	function DenominazioneArea(ID)
	    sql1="SELECT DENOMINAZIONE FROM AREE_PROFESSIONALI WHERE ID_AREAPROF=" & ID
'PL-SQL * T-SQL  
SQL1 = TransformPLSQLToTSQL (SQL1) 
	    set rs = cc.execute(sql1)
        appo = ""
	    if not rs.eof then
	        appo = rs("DENOMINAZIONE")
	    end if
	    DenominazioneArea= appo
	end function
%>	
<!--#include Virtual ="/Include/closeconn.asp"-->
<!--#include Virtual="/strutt_coda2.asp"-->		
