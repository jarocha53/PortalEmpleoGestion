<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include Virtual = '/strutt_testa2.asp'-->
<!-- #include virtual="/include/OpenConn.asp" -->
<!-- #include virtual="/include/DecCod.asp" -->
<!-- #include virtual="/include/ControlDateVB.asp" -->
<% 

'If Not ValidateService(Session("IdUtente"),"BDD_InsDoc",cc) Then 
'	response.redirect "/util/error_login.asp"
'End If

'----------------------------------------------------------------------------------------------------------------------------------------------------------

sub ImpVariabili()

	idRichiesta= Request.Form("txtIdRichiesta")
	sSede= Request.Form("txtSedeHid")
	'Response.Write "sSede: " & sSede & "<br>"
	
	sNomeFile = Replace(Request.Form("nidentifsedeaz"), " ", "")
	sTitoloFile = Request.Form("nidentifsedeaz")

	sProvincia=Request.Form ("cmbProv")
	if Trim(sProvincia) = "" then
		sProvincia=Request.Form ("txtProvincia")
	end if

	'Response.Write "sProvincia: " & sProvincia & "<br>"
	sDescSede=Request.Form ("txtSede")
	sAreaP=Request.Form ("cmbAreaProf")
	sDataDal=Request.Form ("txtDal")
	sDataAl=Request.Form ("txtAl")

end sub

'----------------------------------------------------------------------------------------------------------------------------------------------------------

sub CreaDoc()


	sFileEdit = Session("PosDoc") & sNomeFile & ".htm"
		
	Set fso = CreateObject("Scripting.FileSystemObject")
	' Controllo che non esista una file con lo stesso nome		

	If (fso.FileExists(sFileEdit)) then
		Response.Write "<span class='tbltext3'>" & "Esiste gi� un documento con lo stesso titolo." & "<br>" & "Impossibile sovrascrivere il documento" & "</SPAN>"
		Response.Write "<br><br><br><br><a href='/pgm/bancadati/bdd_insdoc.asp' onclick='Javascript:history.back(); return false'><IMG SRC=" & Session("Progetto") & "/images/indietro.gif border=0></a>"
		Response.End
	End if


	sNomeFile = split(sFileEdit,Server.MapPath("\"))

		SP = Session("Progetto")
		Sp = replace(SP,"/","\")
	'COSTRUISCO IL DOCUMENTO
	sHead = "<html><head><link REL='STYLESHEET' TYPE='text/css' HREF='/Pgm/BancaDati/fogliostile.css'></head>"
'	sTestata = "<table widht='100%' border=0><tr><td align=left><IMG SRC='/images/Italialog.gif' border=0></td></tr></table><HR>" 
	
	sTitolo = "<table widht='100%' border=0 class='tbltext'><tr><td align=left>" &_
		"<p align='center' class='tbltext1'>" & "MODELLO DI CONVENZIONE" & "<br>" &_ 
		"<b>" & "CONVENZIONE DI TIROCINIO DI FORMAZIONE ED ORIENTAMENTO " & "<br>"  &_ 
		"TRA" & "</B>" & "</p>" &_ 
		"<p align='justify'>" & "Il/la " & request.form("RAGSOC") & " " & _ 
		"con sede in " & request.form("sede") & ", " &_
		"codice fiscale  (o partita iva) " & request.form("CODFISC") & " " & _
		"d�ora in poi denominato 'soggetto promotore', rappresentato/a dal sig. " & _ 
		request.form("RAPCOGNOME") & " " & request.form("RAPNOME") & " " & _
		"nato a " & request.form("Born")& " il " & request.form("dataimm") & ";<br>" & _
		"</p><p align='center'>" & _
		"<b>" & "E" & "</b></p>" & _
		"<p align='justify'>" & request.form("NOMEAZOSP") & " con sede legale in " & request.form("SEDEAZOSP") & "," & _
		"codice fiscale (o partita iva) " & request.form("CODFISCaz") & " d�ora in poi denominato 'soggetto ospitante', " & _
		"<br>rappresentato/a dal sig. " & request.form("COGN") & " " & request.form("NOME") & ", " & _
		"nato a " & request.form("NATO")& " il " & request.form("datanasc") & ";<br>" & _
		"</p><p align='center'><b>" & "Premesso" & "</b></p><br>" & _
		"che al fine di agevolare le scelte professionali mediante la conoscenza diretta " &_ 
		"del mondo del lavoro e realizzare momenti di alternanza tra studio e lavoro " &_
		"nell'ambito dei processi formativi i soggetti richiamati all�art. 18, comma 1, "&_ 
		"lettera a), della legge 24 giugno 1997, n. 196, possono promuovere tirocini di "&_
		"formazione ed orientamento in impresa a beneficio di coloro che abbiano gi� "&_
		"assolto l�obbligo scolastico ai sensi della legge 31 dicembre 1962, n. 1859. <br><br>" & _
		"Si conviene quanto segue:<br>" & _
		"<b>" & "Art. 1.</b><br>" & _
		"Ai sensi dell�art. 18 della legge 24 giugno 1997, n. 196 " & _ 
		"la " & request.form("ragione") & " " & _
		"si impegna ad accogliere presso le sue strutture soggetti in " &_
		"tirocinio di formazione ed orientamento su proposta di " & Request.Form("nrag") & ", " & _
		"ai sensi dell�art. 5 del decreto attuativo dell�art. 18 della legge n. 196 del 1997.<br>" & _
		"<b>Art. 2.</b><br>" & _
		"Il tirocinio formativo e di orientamento, ai sensi " & _
		"all�art. 18, comma 1, lettera d), della legge n. 196 "&_
		"del 1997 non costituisce rapporto di lavoro." &_
		"Durante lo svolgimento del tirocinio l�attivit� di" &_
		"formazione ed orientamento � seguita e verificata da un " & _
		"tutore designato dal soggetto promotore in veste di responsabile " & _
		"didattico-organizzativo, e da un responsabile aziendale, indicato " & _
		" dal soggetto ospitante. " & _
		"Per ciascun tirocinante inserito nell�impresa ospitante in base "&_ 
		"alla presente Convenzione viene predisposto un progetto formativo.<br>" & _
		"<b>Art. 3.</b><br>" & _
		"Durante lo svolgimento del tirocinio formativo e" & _
		" di orientamento il tirocinante � tenuto a:<br>" & _ 
		" svolgere le attivit� previste dal progetto formativo <br>" & _
		" e di orientamento; " & _
		" rispettare le norme in materia di igiene e sicurezza e salute<br>" & _
		" sui luoghi di lavoro; " &_
		" mantenere la necessaria riservatezza per quanto attiene ai dati,<br>" & _
		" informazioni o conoscenze in merito a processi produttivi e prodotti, " & _ 
		" acquisiti durante lo svolgimento del tirocinio.<br>" & _
		"<b>Art. 4.</b><br>" & _
		"Il soggetto promotore assicura il/i tirocinante/i contro gli infortuni sul " & _
		"lavoro presso l�Inail, nonch� per la responsabilit� civile presso compagnie " & _
		"assicurative operanti nel settore. In caso di incidente durante lo svolgimento " & _
		"del tirocinio, il soggetto ospitante si impegna a segnalare l�evento, entro " & _
		"i tempi previsti dalla normativa vigente, agli istituti assicurativi" & _
		"(facendo riferimento al numero della polizza sottoscritta dal soggetto" & _
		"promotore) ed al soggetto promotore." & _
		"Il soggetto promotore si impegna a far pervenire alla regione o " & _
		"alla provincia delegata, alle strutture provinciali del Ministero " & _ 
		"del lavoro e della previdenza sociale competenti per territorio in " & _
		"materia di ispezione, nonch� alle rappresentanze sindacali copia della " & _
		"Convenzione di ciascun progetto formativo e di orientamento." &"<br>" & _
		"<b>Art. 5.</b><br>" & _
		"Il datore di lavoro che ospita lo stagista si impegna a:<br>" & _
		" garantire allo stagista l�assistenza e la formazione necessarie al buon esito dello stage;<br>" & _
		" rispettare le norme antinfortunistiche e di igiene sul lavoro; <br>" & _
		" consentire al tutor dell�ente promotore di contattare lo stagista e il tutor aziendale per verificare l�andamento dello stage e per la stesura della relazione finale;<br>" & _ 
		" informare l�ente promotore di qualsiasi incidente possa accadere al tirocinante.<br>" & _
		"<b>Art. 6.</b><br>" & _
		"La presente convenzione decorre dalla data sottoindicata e ha durata di 6 mesi .<br><br>" & _
		Request.Form("Posto") & " " & Request.Form("DATAODIERNA")& "<br><br>" & _
		"Firma per il soggetto promotore) ............................................................<br><br>" & _
		"Firma per il soggetto ospitante) ..............................................................<br>" & _
		"</p>"

	'sAbst =		 "<label id='abst'><br><br><span class=textblacka><b>" & Request.Form("abst") & "</b></span></label><br>"
	'sContenuto = "<label id='contenuto'><br><span class=textblacka>" & Request.Form("contenuto") & "</label>"
	'sAutore =	 "<label id='autore'><br><br><span class=tbltext><em>" & Request.Form("autore") & " - " & fonteins & " - " & Request.Form("dataimm") & "</em></label>"
	'sLink =	     "<label id='link'><br><br><span class=tbltext>" & link & "</label><br><br></p>"
	
	sChiudi = "<table widht='100%' border=0 align=center><tr><td><a href='javascript:window.print()'><img src='/images/stampa.gif' title='Stampa la pagina' border='0'></a></td>" & _
				"<td><a href='javascript:self.close()'><img src='/images/chiudi.gif' title='Chiudi la pagina' border='0'></a></td></tr></table>"
					
	%>
	<!-- #include virtual="/include/CloseConn.asp" -->

	<br><center>
	<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
		<tr height="18">
			<td class="sfondomenu" width="77%" height="18"><span class="tbltext0"><b>&nbsp;DOCUMENTA - INSERIMENTO NUOVO DOCUMENTO </b></span></td>
			<td width="3%" background="<%=session("progetto")%>/images/tondo_linguetta.gif"></td>
			<td valign="middle" align="right" class="tbltext1" width="20%" background="<%=session("progetto")%>/images/sfondo_linguetta.gif">&nbsp;</td>
		</tr>
	</table>
	<!-- Commento -->
	<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
		<tr>
		<td align="left" class="sfondocomm">Inserimento in lista del documento.</td>
		</tr>
		<tr height="2">
			<td class="sfondocomm" width="100%" colspan="3" background="<%=session("progetto")%>/images/separazione.gif"></td>
		</tr>
	</table>

	<form name=findietro method="post" action="/pgm/gestprogetti/stage/Stage_Candidature/STA_VisDettStage.asp">
	
	<input type="hidden" value="<%=idRichiesta%>" name="txtIdRichiesta">
	
	<input type="hidden" name="txtSede" value="<%=sDescSede%>">
	<input type="hidden" name="txtProvincia" value="<%=sProvincia%>">
	<input type="hidden" name="txtSedeHid" value="<%=sSede%>">
	<input type="hidden" name="cmbAreaProf" value="<%=sAreaP%>">
	<input type="hidden" name="txtDal" value="<%=sDataDal%>">
	<input type="hidden" name="txtAl" value="<%=sDataAl%>">
	<%

	'Response.Write "sTitolo =  " & sTitoloFile & "<br>"
	'Response.Write "sFile ="  & sFileEdit

	if  sTitoloFile > "" then
		NomFile = replace(sNomeFile(1),"\","/")
		NomFile = "/Pgm/gestProgetti/Stage/GestStagisti/GES_VisDocumento.asp?pag=" & NomFile

		' SCRITTURA DOCUMENTO

		'Response.Write sFileEdit
		
		Dim fso, f
		Set fso = CreateObject("Scripting.FileSystemObject")
		Set f = fso.CreateTextFile(sFileEdit)

		f.writeline  sHead
'		f.writeline  sTestata
		'f.writeline  metaKeytitle
		'f.writeline  metaKeywords
		'f.writeline  metaContenuto
		'f.writeline  metaAutore
		'f.writeline  metaDataImm
		'f.writeline  metaAbstract
		'f.writeline  metaFonte
		'f.writeline  metaDataFonte
		'f.writeline  metalinktitolo
		'f.writeline  metaExtra	
		f.writeline  sTitolo
		'f.writeline  sAbst
		'f.writeline  sContenuto
		'f.writeline  sAutore
		'f.writeline	 sLink
		f.writeline	 sChiudi

		f.close 
		Set f = Nothing
		Set fso = Nothing

'		ok = "<center><br><br><br><span class=tbltext3>Operazione correttamente eseguita</span>"
'		att="<table width='100%' border='0'><tr align='center'><td align='middle'><input type='image' src='/images/indietro.gif' title border='0' id='image'1 name='image'1></a></td></tr></table>"

'		Response.Write ok
'		Response.Write att

		%>
		<script language="javascript" action="">
			alert("Operazione correttamente effettuata");
			findietro.submit();
		</script>
		<% 		

	else
		Response.Write "<br><br><br><br><span class=tbltext3>Torna alla pagina di inserimento</span>"
		Response.Write "<br><br><br><br><a href=/pgm/GestProgetti/stage/geststagisti/GES_insdoc1.asp><IMG SRC=" & Session("Progetto") & "/images/indietro.gif border=0></a>"
	end if
	%>
	<br>
	</form>

<%
end sub

'--------------------------- MAIN ----------------------------------------------------------------------------------

dim idRichiesta, sNomeFile, sTitoloFile
dim sProvincia, sSede, sDescSede, sAreaP, sDataDal, sDataAl

ImpVariabili()

%>
<form method="post" name="frmIndietro_VisDettStage" onsubmit="" action="STA_VisDettStage.asp">
	<input type="hidden" name="txtIdRichiesta" value="<%=idRichiesta%>">
	<input type="hidden" name="txtSedeHid" value="<%=sSede%>">
	<input type="hidden" name="txtSede" value="<%=sDescSede%>">

	<input type="hidden" name="txtProvincia" value="<%=sProvincia%>">
	<input type="hidden" name="cmbProv" value="<%=sProvincia%>">
	
	<input type="hidden" name="txtAreaP" value="<%=sAreaP%>">
	<input type="hidden" name="txtDataDal" value="<%=sDataDal%>">
	<input type="hidden" name="txtDataAl" value="<%=sDataAl%>">
	<input type="hidden" name="cmbBando" value="<%=Request.Form("cmbBando")%>">
	
</form>
<%
Set fso = CreateObject("Scripting.FileSystemObject")

'cancellazione del documento
if Request.Form("MOD") = "D" then
	apNomeFile = Request.Form("txtNomeFile")
	set OggFile = fso.GetFile(Server.MapPath(apNomeFile))
	OggFile.Delete
	Set fso = Nothing

	%>
	<script language="javascript" action="">
		alert("Cancellazione correttamente effettuata");
		frmIndietro_VisDettStage.action="STA_VisDettStage.asp";
		frmIndietro_VisDettStage.submit();
	</script>
	<% 
else
	CreaDoc()	
end if

Set fso = Nothing


%>

<!--#include Virtual = '/strutt_coda2.asp'-->
