<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include Virtual = "/strutt_testa2.asp"-->
<!-- #include virtual="/include/OpenConn.asp" -->
<!-- #include virtual="/include/DecCod.asp" -->
<!-- #include virtual="/include/DecComun.asp" -->
<%
'If Not ValidateService(Session("IdUtente"),"BDD_InsDoc",cc) Then 
	'response.redirect "/util/error_login.asp"
'End If

%> 
<script language="javascript" src="/include/help.inc"></script>
<script language="Javascript" src="/include/ControlDate.inc"></script>
<script language="Javascript" src="/include/ControlNum.inc"></script>
<script language="Javascript" src="/include/ControlString.inc"></script>
<script language="Javascript" src="Controlli.js"></script> 
<script language="Javascript">

function riempi(){
	document.forma.nrag.value = document.forma.RAGSOC.value;
}

function Controlla(){

	document.forma.nrag.value = document.forma.RAGSOC.value;

	if (TRIM(forma.Posto.value) == ""){
		alert("Inserire il luogo di compilazione del modello.");
		forma.Posto.focus();
		return false;
	}
		
	if (TRIM(forma.DATAODIERNA.value) == ""){
		alert("Inserire la data di compilazione del modello.");
		forma.DATAODIERNA.focus();
		return false;
	}
	
	if (!ValidateInputDate(forma.DATAODIERNA.value)){
		forma.DATAODIERNA.focus();
		return false;
	}

	return true
		
}

</script>

<%
sub ImpVariabili()

	hIdRich = Request.Form("txtIdRichiesta")
	sSede=Request.Form("txtSedeHid")
	'Response.Write "sSede: " & sSede & "<br>"

	idstagista=Request.Form("txtIdPers")
	sProvincia=Request.Form ("cmbProv")
	sDescSede=Request.Form ("txtSede")
	sAreaP=Request.Form ("cmbAreaProf")
	sDataDal=Request.Form ("txtDal")
	sDataAl=Request.Form ("txtAl")

	SP = Session("Progetto")
	SP = replace(SP,"/","\")
	Session("PosDoc") = Server.MapPath("\") & SP & "\DOCPRG\"
'	Response.Write Session("PosDoc")

end sub

'-----------------------------------------------------------------------------------------------------------------------------------------------

sub ImpPagina()

	sSQL = "SELECT I.RAG_SOC, I.COD_FISC, I.PART_IVA, SI.REF_SEDE, " & _
			"SI.PRV, SI.COMUNE, SI.INDIRIZZO " & _
			"FROM IMPRESA I, SEDE_IMPRESA SI " & _
			"WHERE I.ID_IMPRESA = SI.ID_IMPRESA AND " & _
			"SI.ID_SEDE = " & sSede
	'Response.Write SSQL
	
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	set rsImpresa = CC.Execute(sSQL)
	if rsImpresa.EOF then
		Response.Write "Impossibile reperire i dati realivi all'azienda"
		Response.End	
	end if

	sApDescCom = DescrComune(rsImpresa("COMUNE"))
	sApIndSede = rsImpresa("INDIRIZZO") & ", " & sApDescCom & " (" & rsImpresa("PRV") & ")"

	if Trim(rsImpresa("COD_FISC")) <> "" then
		apCodFisc = rsImpresa("COD_FISC")
	else
		apCodFisc = rsImpresa("PART_IVA")
	end if

%>
	<br>
	<center>
	<form method="post" name="forma" onsubmit="return Controlla()" action="STA_Risultati.asp">
	
		<input type="hidden" value="<%=sSede%>" name="nidentifsedeaz">
		<input type="hidden" value="<%=hIdRich%>" name="txtIdRichiesta">
		<input type="hidden" name="cmbProv" value="<%=sProvincia%>">
		<input type="hidden" name="txtSedeHid" value="<%=sSede%>">
		<input type="hidden" name="txtSede" value="<%=sDescSede%>">
		<input type="hidden" name="cmbAreaProf" value="<%=sAreaP%>">
		<input type="hidden" name="txtDal" value="<%=sDataDal%>">
		<input type="hidden" name="txtAl" value="<%=sDataAl%>">

		<!--input class="textred" id="NOMEAZOSP" size="15" name="NOMEAZOSP" maxlength="15" onchange="javascript:riempi()">		<input class="textred" id="SEDEAZOSP" size="15" name="SEDEAZOSP" maxlength="15">		<input class="textred" id="CODFISCaz" size="16" name="CODFISCaz" maxlength="16">		<input class="textred" id="COGN" size="15" name="COGN" maxlength="15">		<input class="textred" id="NOME" size="15" name="NOME" maxlength="15"-->
		<input type="hidden" name="NOMEAZOSP" value="<%=rsImpresa("RAG_SOC")%>">
		<input type="hidden" name="SEDEAZOSP" value="<%=sApIndSede%>">
		<input type="hidden" name="CODFISCaz" value="<%=apCodFisc%>">
		<input type="hidden" name="COGN" value="<%=rsImpresa("REF_SEDE")%>">
		<input type="hidden" name="NOME" value>		
		
		<!--input class="textred" id="ragione" size="15" name="ragione" maxlength="15"-->			
		<input type="hidden" name="ragione" value="<%=rsImpresa("RAG_SOC")%>">
		
		<table cellSpacing="4" align="center" cellPadding="4" width="500" border="1"> 
		<tr>
		<td class="tbltext">
		<p align="center">
		MODELLO DI CONVENZIONE <br>
		<b>CONVENZIONE DI TIROCINIO DI FORMAZIONE ED ORIENTAMENTO <br>
		TRA</b>
		</p>
		<p align="justify">
		Il/la&nbsp;<input class="textred" id="RAGSOC" size="25" name="RAGSOC" maxlength="20" onchange="javascript:riempi()">&nbsp;(soggetto promotore)<br>
		con sede in <input class="textred" id="SEDE" size="35" name="SEDE" maxlength="35">,<br>
		codice fiscale (o partita iva) <input class="textred" id="CODFISC" size="16" name="CODFISC" maxlength="16"><br>
		d&#146;ora in poi denominato &quot;soggetto promotore&quot;, rappresentato/a dal sig. <br>
		(Cognome)<input class="textred" id="RAPCOGNOME" size="15" name="RAPCOGNOME" maxlength="50"><br>
		(Nome)<input class="textred" id="RAPNOME" size="15" name="RAPNOME" maxlength="50"><br>

		nato a <input class="textred" id="Born" size="25" name="Born" maxlength="35"> il <input class="textred" id="dataimm" name="dataimm" size="11" maxlength="10">;<br>
		</p>
		<p align="center">
		<b>E</b>
		</p>
		<p align="justify">
		(denominazione dell&#146;azienda ospitante) <%=rsImpresa("RAG_SOC")%><br> 
		con sede legale in <%=sApIndSede%>,<br> 
		codice fiscale (o partita iva) <%=apCodFisc%><br> 
		d&#146;ora in poi denominato &quot;soggetto ospitante&quot;,<br>
		rappresentato/a dal sig. &nbsp;<%=rsImpresa("REF_SEDE")%>,<br>
		nato a &nbsp;<input class="textred" id="NATO" size="25" name="NATO" maxlength="35"> il <input class="textred" id="datanasc" name="datanasc" size="11" maxlength="10">;<br>
		</p>
		<p align="center">
		<b>Premesso</b>
		</p>
		<br>

		che al fine di agevolare le scelte professionali mediante la conoscenza diretta 
		del mondo del lavoro e realizzare momenti di alternanza tra studio e lavoro 
		nell&#146;ambito dei processi formativi i soggetti richiamati all&#146;art. 18, comma 1, 
		lettera a), della legge 24 giugno 1997, n. 196, possono promuovere tirocini di 
		formazione ed orientamento in impresa a beneficio di coloro che abbiano gi� 
		assolto l&#146;obbligo scolastico ai sensi della legge 31 dicembre 1962, n. 1859.<br><br>

		Si conviene quanto segue:<br>
		<b>Art. 1.</b><br>
		Ai sensi dell&#146;art. 18 della legge 24 giugno 1997, n. 196 
		la <%=rsImpresa("RAG_SOC")%> (azienda ospitante)
		si impegna ad accogliere presso le sue strutture soggetti in
		tirocinio di formazione ed orientamento su proposta di <input disabled class="textred" id="nrag" size="25" name="nrag" maxlength="20"> (soggetto promotore),
		 ai sensi dell&#146;art. 5 del decreto attuativo dell&#146;art. 18 della legge n. 196 del 1997.<br>

		<b>Art. 2.</b><br>
		Il tirocinio formativo e di orientamento, ai sensi 
		all&#146;art. 18, comma 1, lettera d), della legge n. 196
		 del 1997 non costituisce rapporto di lavoro. 
		Durante lo svolgimento del tirocinio l&#146;attivit� di 
		formazione ed orientamento � seguita e verificata da un 
		tutore designato dal soggetto promotore in veste di responsabile 
		didattico-organizzativo, e da un responsabile aziendale, indicato
		 dal soggetto ospitante. 
		Per ciascun tirocinante inserito nell&#146;impresa ospitante in base 
		alla presente Convenzione viene predisposto un progetto formativo. <br>

		<b>Art. 3.</b><br>
		Durante lo svolgimento del tirocinio formativo e
		 di orientamento il tirocinante � tenuto a: 
		q svolgere le attivit� previste dal progetto formativo
		 e di orientamento; 
		q rispettare le norme in materia di igiene e sicurezza e salute
		 sui luoghi di lavoro; 
		q mantenere la necessaria riservatezza per quanto attiene ai dati,
		 informazioni o conoscenze in merito a processi produttivi e prodotti, 
		 acquisiti durante lo svolgimento del tirocinio.<br>

		<b>Art. 4.</b><br>
		Il soggetto promotore assicura il/i tirocinante/i contro gli infortuni sul
		 lavoro presso l&#146;Inail, nonch� per la responsabilit� civile presso compagnie
		  assicurative operanti nel settore. In caso di incidente durante lo svolgimento
		   del tirocinio, il soggetto ospitante si impegna a segnalare l&#146;evento, entro 
		   i tempi previsti dalla normativa vigente, agli istituti assicurativi 
		   (facendo riferimento al numero della polizza sottoscritta dal soggetto
		    promotore) ed al soggetto promotore. 
		Il soggetto promotore si impegna a far pervenire alla regione o 
		alla provincia delegata, alle strutture provinciali del Ministero 
		del lavoro e della previdenza sociale competenti per territorio in 
		materia di ispezione, nonch� alle rappresentanze sindacali copia della
		 Convenzione di ciascun progetto formativo e di orientamento. <br>

		<b>Art. 5.</b><br>
		Il datore di lavoro che ospita lo stagista si impegna a:
		q garantire allo stagista l&#146;assistenza e la formazione necessarie al buon esito dello stage; 
		q rispettare le norme antinfortunistiche e di igiene sul lavoro; 
		q consentire al tutor dell&#146;ente promotore di contattare lo stagista e il tutor aziendale per verificare l&#146;andamento dello stage e per la stesura della relazione finale; 
		q informare l&#146;ente promotore di qualsiasi incidente possa accadere al tirocinante. <br>

		<b>Art. 6</b><br>
		La presente convenzione decorre dalla data sottoindicata e ha durata di 6 mesi .<br><br>


		(luogo)<input class="textred" size="35" name="Posto" maxlength="30"> (data) <input class="textred" id="DATAODIERNA" size="15" name="DATAODIERNA" maxlength="15"><br><br>


		(Firma per il soggetto promotore) ............................................................<br>
		<br>
		(Firma per il soggetto ospitante) ..............................................................<br>

		</p>
		</td>
		</tr>  
		</table>

		<!--- PULSANTE INVIA ---->
		<br><br>
		<table cellSpacing="4" cellPadding="4" width="500" align="center" border="0">
			<tr>
				<td align="right">
					<a href="javascript:history.back()">
						<img src="<%=session("Progetto")%>/images/indietro.gif" border="0" name="imgPunto2" onmouseover="javascript:window.status=' '; return true">
					</a>
				</td>
				<td align="left" colspan="3">
					<input type="image" id="IMG1" alt src="<%=session("Progetto")%>/images/conferma.gif" border="0" WIDTH="55" HEIGHT="40">
				</td>
			</tr>  
		</table>
		<br>

	</form>

	<%
end sub

'--------------------------------------- MAIN --------------------------------------------------------------------------------------------------------------------

dim hIdRich, sSede, idstagista
dim sProvincia, sDescSede, sAreaP, sDataDal, sDataAl

ImpVariabili()

ImpPagina()            

%>

<!-- #include virtual="/include/closeconn.asp" -->
<!--#include Virtual = "/strutt_coda2.asp"-->
