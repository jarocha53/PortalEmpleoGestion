<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include Virtual = "/strutt_testa2.asp"-->
<!--#include Virtual = "/include/SysFunction.asp"-->
<!--#include Virtual = "/include/openconn.asp"-->
<!--#include virtual = "/include/DecCod.asp"-->
<!--#include virtual = "/util/portallib.asp"-->
<%
if ValidateService(session("idutente"),"STA_VISIMPRESE",cc) <> "true" then 
	response.redirect "/util/error_login.asp"
end if
%>

<script language="javascript">
	//include del file per fare i controlli sulla validit� delle date
	<!--#include virtual = "/Include/ControlDate.inc"-->
	//include del file per fare i controlli sulla numericit� dei campi
	<!--#include virtual = "/Include/ControlNum.inc"-->
	<!--#include virtual = "/Include/help.inc"-->

	function decodifica(nVal,sTipoSorg) {
		if (windowArea != null ) {
			windowArea.close(); 
		}
		windowArea = window.open ('STA_VisDecod.asp?Dec=' + sTipoSorg + '&Valore=' + nVal ,'Info','width=400,height=280,Resize=No')
	}
</script>
<script language="javascript" src="ControlliFiltri.js"></script>

<%
dim sIdSede, nIdRic, nValori, nMax
dim bVarie, bCheck, bCheck1, bOpLog, bFilter, bIdemOpeLog
dim sSQL, rsTabella, rsDizDati, rsSel, rsDecod
DIM rsNomeTabella, rsCampoArea, rsTabRif, rsCriteri 

dim aOperLog
dim sIDtab, sDescTab, sArea, sSpecifico, sCondizione
dim sNomeHelp, sCampo, sCampo1, sCampo2, sIdCampo
dim sTipoDato, sTipoOplg, sTipoSorg, sValori, sValore
dim sPrVolta, sConf, sDecod, sRisultato, sOpeLog, sTabella
dim sDesCampo, sLenCampo, sTipCampo, sDesCampoRif, sIDCampoRif


if Request("NumValori") = "" then
	nValori = 0
else
	nValori = Request("NumValori")-1
end if

Inizio()

sIDtab			= Request("IDTAB")
sArea			= Request("AREA")
sSpecifico		= Request("SPECIFICO")
sCondizione		= Request("CONDIZIONE")
nIdRic			= Request("IdRic")
sIdSede			= Request("IdSede")

sIDtab			= Replace(sIDtab,"'","''")
sArea			= Replace(sArea,"'","''")
sSpecifico		= Replace(sSpecifico,"'","''")

if sIDtab = "SEL_TAB" then
	sDescTab = "Selezione Libera"
else
	if sIDtab = "VARIE" then
		sSQL = "SELECT DESC_TAB FROM DIZ_TAB WHERE ID_TAB='" & Request("IDTAB1") & "'"
	else
		sSQL = "SELECT DESC_TAB FROM DIZ_TAB WHERE ID_TAB='" & sIDtab & "'"
	end if
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	set rsTabella = CC.Execute (sSQL)
		sDescTab = rsTabella("DESC_TAB")
	rsTabella.Close
end if
%>
<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
	<tr height="18">
		<td class="sfondomenu" width="67%" height="18"><span class="tbltext0"><b>&nbsp;INSERIMENTO CARATTERISTICHE CANDIDATI</b></span></td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
		<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">(*) campi obbligatori</td>
	</tr>
	<tr>
		<td class="sfondocomm" colspan="3">
<%		if sDescTab = "Selezione Libera" then %>
			<br>Selezionare l'<b>ambito di informazione</b> per cui si vogliono impostare dei filtri.
<%		else %>
			<br>Impostare le caratteristiche, relative a &quot;<b><%=sDescTab%></b>&quot;, che i candidati devono possedere per accedere alla selezione.
<%		end if

		select case sIDtab
			case "PERS_VINCOLI","PERS_DISPON"
				sNomeHelp = "_2"
			case "ESPRO"
				sNomeHelp = "_3"
			case "SEL_TAB"
				sNomeHelp = "_4"
			case "VARIE" 
				sNomeHelp = "_4"
			case "TISTUD"
				sNomeHelp = "_1"
			case else			'(PERS_CONOSC, PERS_CAPAC, PERC_COMPOR)
				sNomeHelp = "_1"
		end select
		sNomeHelp = "STA_InsCriteriSel" & sNomeHelp %>
			<a href="Javascript:Show_Help('/Pgm/help/GestProgetti/Stage/Stage_Definizione/<%=sNomeHelp%>')"><img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
		</td>
	</tr>
	<tr height="2">
		<td class="sfondocomm" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
	</tr>
</table>
<br>

<%
bVarie = false
SELECT CASE sIDtab 
	CASE "ESPRO"
		sCondizione = "FLOOR(MONTHS_BETWEEN(NVL(DT_FIN_ESP_PRO,SYSDATE),DT_INI_ESP_PRO)/12)"
		sArea		= "ESPRO"
		sCampo1		= "SUBSTR(COD_MANSIONE,1,4)"
		sCampo2		= "SUBSTR(COD_MANSIONE,1,8)"
		sSpecifico	= "COD_MANSIONE" %>
		<form name="frmInsCriteri" method="POST" action="STA_CnfCriteriSel.asp?Action=INS" onsubmit="return ControlloDatiEspro()">
			<input type="hidden" value="<%=sIDtab%>" name="IDTAB">
			<input type="hidden" value="<%=sCampo1%>" name="txtCampoArea">
			<input type="hidden" value="<%=sCondizione%>" name="txtCampoTab">
			<input type="hidden" value="<%=nIdRic%>" name="txtIDRic">
			<input type="hidden" value="<%=sIdSede%>" name="IdSede">
			<input type="hidden" value="COD_MANSIONE" name="txtCampoSpecifico">
			<input type="hidden" name="txtTipo" value="A">
			<input type="hidden" name="AREA" value="<%=sArea%>">
			<input type="hidden" name="SPECIFICO" value="<%=sSpecifico%>">
			<input type="hidden" name="CONDIZIONE" value="<%=sCondizione%>">
	
			<table border="0" CELLPADDING="1" CELLSPACING="2" width="500" align="center">
				<tr height="17">
					<td width="200" class="tbltext1"><b>Categoria professionale*</b></td>
					<td width="270" class="tbltext1">
						<b><input class="textblacka" readonly type="text" name="txtArea" style="width=270px">
						<input class="textblacka" type="hidden" name="txtTipoArea" value></b>
					</td>
					<td align="center" width="30" class="tbltext1">
						<a href="Javascript:SelCodIstat('<%=sArea%>','1')" ID="imgPunto1" name="imgPunto1"><img border="0" src="<%=Session("Progetto")%>/images/bullet1.gif"></a>
					</td>
				</tr>
				<tr height="17">
					<td width="200" class="tbltext1"><b>Qualifica*</b></td>
					<td width="270" class="tbltext1">
						<b><input class="textblacka" readonly type="text" name="txtProf" style="width=270px">
						<input type="hidden" name="txtTipoProf" value></b>
					</td>
					<td align="center" width="30" class="tbltext1"><b>
						<a href="Javascript:SelCodIstat('<%=sArea%>','2')" ID="imgPunto2" name="imgPunto2"><img border="0" src="<%=Session("Progetto")%>/images/bullet1.gif"></a>
					</td>
				</tr>
				<tr height="17">
					<td width="200" class="tbltext1"><b>Mansione*</b></td>
					<td width="270" class="tbltext1">
						<b><input class="textblacka" readonly type="text" name="txtSpecifico" style="width=270px">
						<input type="hidden" name="txtTipoSpecifico" value></b>
					</td>
					<td align="center" width="30" class="tbltext1"><b>
						<a href="Javascript:SelCodIstat('<%=sArea%>','3')" ID="imgPunto3" name="imgPunto3"><img border="0" src="<%=Session("Progetto")%>/images/bullet1.gif"></a>
					</td>
				</tr>
				<tr height="17">
					<td width="200" class="tbltext1"><b>Condizione Logica*</b></td>
<%				
				sSQL =	"SELECT ID_CAMPO_DESC,TIPO_OPLOG,TIPO_CAMPO,LEN_CAMPO, DESC_CAMPO FROM DIZ_DATI" &_
						" WHERE ID_TAB='" & sIDtab & "'" &_
						" AND ID_CAMPO='" & sCondizione & "'" &_
						" ORDER BY DESC_CAMPO" 
				nMax=100
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
				set rsDizDati = cc.Execute(sSQL)
					bCheck	= false
					bCheck1 = false
					sTipoDato = rsDizDati("TIPO_CAMPO")
					if not isnull(rsDizDati("LEN_CAMPO"))  then
						nMax=clng(rsDizDati("LEN_CAMPO"))
					end if 
					if rsDizDati("TIPO_OPLOG") <> "" then
						bCheck = true
						sTipoOplg = split(rsDizDati("TIPO_OPLOG"),"|")
					end if 
					if  rsDizDati("ID_CAMPO_DESC") <> "" then
						bCheck1 = true
						sValori	= split(rsDizDati("ID_CAMPO_DESC"),"|")
					end if 
				rsDizDati.Close
%>
					<td width="270" colspan="2" class="tbltext1"><b>
<%
					sSQL = ""
					if bCheck then
						sSQL = sSQL & " CODICE IN ("
						for i = 0 to Ubound(sTipoOplg)
							sSQL = sSQL & "'" & sTipoOplg(i) & "'"
							if i <> (Ubound(sTipoOplg)) then
								sSQL = sSQL & ","
							end if 
						next
						sSQL = sSQL & ")"
					end if	
					aOperLog = decodTadesToArray("OPLOG",Date,sSQL,0,"0")
%>
						<select class="textblacka" name="cmbLogica" style="width=130">
							<option value></option>
<%
					if not IsNull(aOperLog(0,0,0)) then
						for yy = 0 to ubound(aOperLog)-1 %>
							<option value="<%=aOperLog(yy,yy,yy)%>"><%=aOperLog(yy,yy+1,yy)%></option>
<%						next
					end if
					erase aOperLog
%>
				 		</select></b>
						<input type="hidden" value="<%=sTipoDato%>" name="txtTipoDato">
					</td>
				</tr>
				<tr height="17">
					<td width="200" class="tbltext1"><b>Anni di anzianit�*</b></td>
					<td width="270" colspan="2" class="tbltext1">
<%					if bCheck1 then		'==>Se i valori sono definiti in DIZ_DATI li decodifico...%>
						<select class="textblacka" name="cmbValori" style="width=130">
<%						for i = 0 to Ubound(sValori) step 2 %>
							<option value="<%=sValori(i)%>"><%=ucase(sValori(i+1))%></option>
<%						next %>
						</select>
<%					else	'==>...altrimenti creo i campi liberi.%>
						<input class="textblacka" type="text" name="cmbValori" style="TEXT-TRANSFORM: uppercase; width=50" maxlength="<%=nMax%>">
<%					end if %>
					</td>
				</tr>
			</table>
			<table border="0" CELLPADDING="1" CELLSPACING="2" width="500" align="center">
				<tr>			
					<td align="center" class="tbltext1"><br>
						<input type="hidden" value="<%=sIdSede%>" name="IdSede">
						<input type="image" title="Invia Richiesta" src="<%=Session("Progetto")%>/images/conferma.gif" id="image1" name="image1">
					</td>
		</form>
		<form name="frmIndietro" method="post" onsubmit="return CheckWindow()" action="STA_ModRichStage.asp">
			<input type="hidden" name="nRichSta" value="<%=nIdRic%>">
					<td colspan="2" class="tbltext1" align="center">
						<input type="image" title="Indietro" src="<%=Session("Progetto")%>/images/indietro.gif" id="image1" name="image1">
					</td>
				</tr>
			</table>
		</form>
<%
	CASE "SEL_TAB"
		sSQL =	"SELECT ID_TAB,DESC_TAB FROM DIZ_TAB" &_
				" WHERE IND_INCROCIO='S'" &_
				" AND ID_TAB NOT IN('ESPRO','PERS_CONOSC','TISTUD','PERS_DISPON','PERS_CAPAC','PERS_VINCOLI','PERS_COMPOR')" &_
				" ORDER BY DESC_TAB"
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
		set rsNomeTabella = CC.Execute(sSQL)
%>
		<form name="frmTabelle" method="POST" action="STA_InsCriteriSel.asp">
			<input type="hidden" value="<%=nIdRic%>" name="IdRic">
			<input type="hidden" value="<%=sIdSede%>" name="IdSede">
			<input type="hidden" value="VARIE" name="IDTAB">
			<table border="0" CELLPADDING="0" CELLSPACING="0" width="500" align="center">
				<tr>
					<td align="center" class="tbltext1" width="250"><b>Selezionare ambito informazioni</b></td>
					<td width="250">
						<select class="textblacka" style="WIDTH: 300px" name="IDTAB1" onchange="javascript:frmTabelle.submit()">
							<option></option>
<%
						do while not rsNomeTabella.EOF %>
							<option value="<%=rsNomeTabella("ID_TAB")%>">
								<%=rsNomeTabella("DESC_TAB")%>
							</option>
<%						rsNomeTabella.MoveNext %>
<%						loop %>
						</select>
					</td>
				</tr>
			</table>
		</form>
		
		<form name="frmIndietro" method="post" onsubmit="return CheckWindow()" action="STA_ModRichStage.asp">
			<input type="hidden" name="nRichSta" value="<%=nIdRic%>">
			<table border="0" CELLPADDING="0" CELLSPACING="0" width="500" align="center">
				<tr>
					<td align="center"> 
						<input type="image" title="Indietro" src="<%=Session("Progetto")%>/images/indietro.gif" id="image1" name="image1">
					</td>
				</tr>
			</table>
		</form>
<%
	CASE "VARIE"
		bVarie = true
		'Ricordarsi di prendere in esame tutte le tabelle e non solo quella persona
		sIDtab = Request("IDTAB1")
		select case sIDtab
			case else
				sPrVolta	= Request("prVolta")
				sConf		= Request("Between")
				sDecod		= Request("Decod")
				sRisultato	= Request("Risultato")
				sValore		= Request("Valore")
				sOpeLog		= Request("OpeLog")

				if sPrVolta <> "No" then
					sTabella = "PERSONA"
				else
					sTabella= Request("Tabella")
					sCampo = Request("Campo")
				end if
	
		'************************************************************************************************ %>
		<form name="frmInsCriteri" method="POST" action="STA_CnfCriteriSel.asp?Action=INS" onsubmit="return Condizione()">
			<table width="500" border="0" cellspacing="2" cellpadding="2" align="center">
				<tr align="middle">
<%
				'******************************************************
				'Mi prendo le tabelle su cui posso applicare le regole
				sSQL = "Select ID_TAB,ID_CAMPO,DESC_CAMPO,LEN_CAMPO," &_
						"TIPO_OPLOG,TIPO_CAMPO,ID_TAB_RIF,ID_CAMPO_DESC,ID_CAMPO_RIF FROM DIZ_DATI" &_
						" WHERE ID_TAB='" & sIDTab & "' AND IND_INCROCIO='S' order by DESC_CAMPO"
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
				set rsSel = CC.Execute(sSQL)
					bFilter = false %>
					<td align="left" width="150"><b class="tbltext1">Nome Campo*</b></td>
					<td align="left">
						<select class="textblacka" name="txtTipoArea" onchange="JAVASCRIPT:Campi(0)" style="width=350">
<%
						sDesCampo = rsSel("DESC_CAMPO")
						sLenCampo = rsSel("LEN_CAMPO")
						sTipCampo = rsSel("TIPO_CAMPO")
						sIdCampo =	rsSel("ID_CAMPO")
						sTipoSorg = rsSel("ID_TAB_RIF")
						sIDCampoRif = rsSel("ID_CAMPO_RIF")
						if isnull(rsSel("ID_CAMPO_DESC")) then
							sDesCampoRif = ""
						else
							sDesCampoRif= rsSel("ID_CAMPO_DESC")
						end if
			
						do while not rsSel.EOF
							if sCampo = rsSel("ID_CAMPO") then
								sIdCampo =	rsSel("ID_CAMPO")
								sDesCampo = rsSel("DESC_CAMPO")
								sLenCampo = rsSel("LEN_CAMPO")
								sTipCampo = rsSel("TIPO_CAMPO")
								sTipoSorg = rsSel("ID_TAB_RIF")
								sIDCampoRif = rsSel("ID_CAMPO_RIF")
								'sDesCampoRif= rsSel("ID_CAMPO_DESC")
								if isnull(rsSel("ID_CAMPO_DESC")) then
									sDesCampoRif = ""
								else
									sDesCampoRif= rsSel("ID_CAMPO_DESC")
								end if
							end if 
							Response.Write "<OPTION "
							if sCampo = rsSel("ID_CAMPO") then
								Response.Write " selected "
								if not IsNull(rsSel("TIPO_OPLOG")) then
									sTipoOplg = split(rsSel("TIPO_OPLOG"),"|")
									bFilter = true
								else
									bFilter = false
									sTipoOplg = ""
								end if 
								bOpLog = true
							end if
							Response.write	"value ='" & rsSel("ID_CAMPO") &_
											"'> " & UCASE(rsSel("DESC_CAMPO")) & "</OPTION>"
						rsSel.MoveNext 
						loop %>
						</select>
					</td>
				</tr>
				<tr align="middle">
					<td align="left" width="150"><b class="tbltext1">Condizione*</b></td>
					<td align="left">
<%
					if not bOpLog then
						rsSel.MoveFirst
						if not IsNull(rsSel("TIPO_OPLOG")) then
							sTipoOplg = split(rsSel("TIPO_OPLOG"),"|")
							bFilter = true
						else
							sTipoOplg = ""
							bFilter = false
						end if 
					end if 
					rsSel.Close 
					sSQL = ""
					if bFilter  then
						sSQL = sSQL & " CODICE IN ("
						for i = 0 to Ubound(sTipoOplg)
							sSQL = sSQL & "'" & sTipoOplg(i) & "'"
							if i <> (Ubound(sTipoOplg)) then
								sSQL = sSQL & ","
							end if 
						next
						sSQL = sSQL & ")"
					end if	
					aOperLog = decodTadesToArray("OPLOG",Date,sSQL,0,"0") %>
						<select class="textblacka" name="cmbLogica" style="width=130" onchange="Javascript:GestOpLog()">
							<option value></option>
<%
					if not IsNull(aOperLog(0,0,0)) then
						for yy = 0 to ubound(aOperLog)-1 %>
							<option <%IF sOpeLog = aOperLog(yy,yy,yy) THEN Response.Write " selected "%> value="<%=aOperLog(yy,yy,yy)%>"><%=aOperLog(yy,yy+1,yy)%></option>
<%						'''LC. 23/07/2003
							IF sOpeLog = aOperLog(yy,yy,yy) THEN
								bIdemOpeLog = true
							END IF
						'''FINE LC
						next
					end if
					erase aOperLog %>
						</select>
<%
					'''LC 23/07/2003: COMMENTATA IF E MODIFICATA.
					'''if (sOpeLog="08" or sOpeLog="10") then 
					if (sOpeLog="08" or sOpeLog="10") and (bIdemOpeLog = true) then%>
						<a href="Javascript:SiaIn()" ID="imgPunto1" name="imgPunto1"><img border="0" src="<%=Session("Progetto")%>/images/bullet1.gif"></a>
<%					end if %>
					</td>
				</tr>
				<tr>
					<td alIGN="LEFT" width="150"><b class="tbltext1">Valore*</b></td>
					<td>
<%
				if sDesCampoRif = "" then
					for i=0 to nValori %> 
						<input type="hidden" name="txtCodReg" value>
						<input class="textblacka" type="text" style="TEXT-TRANSFORM: uppercase" size="50" name="cmbValori" value="<%=sValore%>" <%
						if sTipoSorg <> "" then
							Response.Write " readonly "
						end if %>>
<%
						if sTipoSorg <> "" then%>
							<a href="Javascript:decodifica('<%=i%>','<%=sTipoSorg%>')" ID="imgPunto1" name="imgPunto1">
								<img border="0" src="<%=Session("Progetto")%>/images/bullet1.gif">
							</a>
<%						
						end if 
					next 
				else %>
						<select class="textblacka" name="cmbValori">
<%
						sValue = Split(sDesCampoRif,"|")
						for i=0 to ubound(sValue) step 2 %>
							<option value="<%=sValue(i)%>"><%=sValue(i+1)%></option>			
<%
						next %>
						</select>
						<input type="hidden" name="txtCodReg" value>
<%
				end if %>
					</td>
				</tr>
			</table>
			
			<br>
			<input type="hidden" name="txtTipo" value="<%=sTipCampo%>">
			<input type="hidden" name="DESCTAB" value="<%=sDescTab%>">
			<input type="hidden" name="IDRIC" value="<%=nIdRic%>">
			<input type="hidden" name="txtIDRic" value="<%=nIdRic%>">
			<input type="hidden" value="<%=sIDtab%>" name="IDTAB1">
			<input type="hidden" value="VARIE" name="IDTAB">
			<input type="hidden" value="<%=sIdSede%>" name="IdSede">
			<input type="hidden" name="AREA" value="<%=sArea%>">
			<input type="hidden" name="SPECIFICO" value="<%=sSpecifico%>">
			<input type="hidden" name="CONDIZIONE" value="<%=sCondizione%>">
			
			<table border="0" CELLPADDING="1" CELLSPACING="2" width="500" align="center">
				<tr>
					<td class="tbltext1" align="center"><br>
						<input type="image" title="Invia Richiesta" src="<%=Session("Progetto")%>/images/conferma.gif" id="image1" name="image1">
					</td>
		</form>
		<form name="frmIndietro" method="post" onsubmit="return CheckWindow()" action="STA_ModRichStage.asp">
			<input type="hidden" name="nRichSta" value="<%=nIdRic%>">
					<td colspan="2" class="tbltext1" align="center"><br>
						<input type="image" title="Indietro" src="<%=Session("Progetto")%>/images/indietro.gif" id="image1" name="image1">
					</td>
		</form>
				</tr>
			</table>
<%
		end select

	CASE "TISTUD","PERS_VINCOLI","PERS_DISPON" %>
		<form name="frmInsCriteri" method="POST" action="STA_CnfCriteriSel.asp?Action=INS" onsubmit="return ControlloDati()">
			<input type="hidden" value="<%=sIDtab%>" name="IDTAB">
			<input type="hidden" value="<%=sCondizione%>" name="txtCampoTab">
			<input type="hidden" value="<%=nIdRic%>" name="txtIDRic">
<%
		sSQL =	"SELECT ID_CAMPO FROM DIZ_DATI WHERE ID_TAB ='"& sIDtab &"'" &_
				" AND ID_TAB_RIF = 'TAB_" & sArea & "'"
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
		set rsCampoArea = CC.Execute(sSQL)
		if not rsCampoArea.EOF then %>	
			<input type="hidden" value="<%=rsCampoArea(0)%>" name="txtCampoArea">
<%	
		else %>	
			<input type="hidden" value name="txtCampoArea">
<%	
		end if
		rsCampoArea.Close
		set rsCampoArea = nothing		
		
		sSQL =	"SELECT ID_CAMPO FROM DIZ_DATI WHERE ID_TAB ='"& sIDtab &"'" &_
				" AND ID_TAB_RIF = 'TAB_" & sSpecifico & "'"
'PL-SQL * T-SQL  
SSQL %> = TransformPLSQLToTSQL (SSQL %>) 
		set rsCampoArea = CC.Execute(sSQL) %>
			<input type="hidden" value="<%=rsCampoArea(0)%>" name="txtCampoSpecifico">
<%
		rsCampoArea.Close
		set rsCampoArea = nothing %>
			
			<input type="hidden" name="txtTipo" value="A">
			<input type="hidden" name="AREA" value="<%=sArea%>">
			<input type="hidden" name="SPECIFICO" value="<%=sSpecifico%>">
			<input type="hidden" name="CONDIZIONE" value="<%=sCondizione%>">
		
			<table border="0" CELLPADDING="1" CELLSPACING="2" width="500" align="center">
				<tr height="17">
					<td width="220" class="tbltext1"><b>Selezionare l'area di interesse*</b></td>
					<td width="250" class="tbltext1"><b>
						<input class="textblacka" readonly type="text" name="txtArea" style="width=250px">
						<input class="textblacka" type="hidden" name="txtTipoArea" value></b>
					</td>
					<td align="center" width="30" class="tbltext1">
						<a href="Javascript:SelArea('<%=sArea%>')" ID="imgPunto1" name="imgPunto1"><img border="0" src="<%=Session("Progetto")%>/images/bullet1.gif"></a>
					</td>
				</tr>
				<tr height="17">
					<td width="220" class="tbltext1"><b>
						Selezionare lo specifico dell'area<%if sIDtab = "PERS_DISPON" OR sIDtab = "PERS_VINCOLI" then Response.Write "*"%></b>
					</td>
					<td width="250" class="tbltext1"><b>
						<input class="textblacka" readonly type="text" name="txtSpecifico" style="width=250px">
						<input type="hidden" name="txtTipoSpecifico" value></b>
					</td>
					<td align="center" width="30" class="tbltext1"><b>
						<a href="Javascript:SelSpecifico('<%=sSpecifico%>')" ID="imgPunto2" name="imgPunto2"><img border="0" src="<%=Session("Progetto")%>/images/bullet1.gif"></a>
					</td>
				</tr>
				<tr height="17">
					<td colspan="3">&nbsp;</td>
				</tr>
<%
			if sCondizione <> "" then%>
				<tr height="2">
					<td width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
				</tr>
				<tr class="textBlack">
					<td colspan="3" align="left" valign="top">
						<b>Spuntare se si vuole indicare un valore :</b>
						<input type="checkbox" id="ckbValore" name="ckbValore" onclick="ChkAbil()">
					</td>
				</tr>
				<tr height="2">
					<td width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
				</tr>
				<tr height="17">
					<td colspan="3">&nbsp;</td>
				</tr>
				<tr height="17">
					<td width="200" class="tbltext1"><b>Condizione Logica</b></td>
<%
				sSQL =	"SELECT ID_CAMPO_DESC,TIPO_OPLOG,TIPO_CAMPO,LEN_CAMPO FROM DIZ_DATI" &_
						" WHERE ID_TAB='"& sIDtab &"'" &_
						" AND ID_CAMPO='" & sCondizione & "' ORDER BY DESC_CAMPO" 
				nMax=100
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
				set rsDizDati = cc.Execute(sSQL)
					bCheck	= false
					bCheck1 = false
					sTipoDato = rsDizDati("TIPO_CAMPO")
					if not isnull(rsDizDati("LEN_CAMPO"))  then
						nMax=clng(rsDizDati("LEN_CAMPO"))
					end if
					if rsDizDati("TIPO_OPLOG") <> "" then
						bCheck = true
						sTipoOplg = split(rsDizDati("TIPO_OPLOG"),"|")
					end if 
					if rsDizDati("ID_CAMPO_DESC") <> "" then
						bCheck1 = true
						sValori	= split(rsDizDati("ID_CAMPO_DESC"),"|")
					end if 
				rsDizDati.Close	%>
					<td width="270" colspan="2" class="tbltext1"><b>
<% 	
					sSQL = ""
					if bCheck then
						sSQL = sSQL & " CODICE IN ("
						for i = 0 to Ubound(sTipoOplg)
							sSQL = sSQL & "'" & sTipoOplg(i) & "'"
							if i <> (Ubound(sTipoOplg)) then
								sSQL = sSQL & ","
							end if 
						next
						sSQL = sSQL & ")"
					end if	
					aOperLog = decodTadesToArray("OPLOG",Date,sSQL,0,"0") %>
						<select disabled class="textblacka" name="cmbLogica" style="width=130">
							<option value></option>
<%
					if not IsNull(aOperLog(0,0,0))then
						for yy = 0 to ubound(aOperLog)-1 %>
							<option value="<%=aOperLog(yy,yy,yy)%>"><%=aOperLog(yy,yy+1,yy)%></option>
<%
						next
					end if
					erase aOperLog %>
 						</select>
						<input type="hidden" value="<%=sTipoDato%>" name="txtTipoDato"></b>
					</td>
				</tr>
				<tr height="17">
					<td width="200" class="tbltext1"><b>Valore<label id="Compreso"></label></b></td>
					<td width="270" colspan="2" class="tbltext1">
<%					
					if bCheck1 then		'==>Se i valori sono definiti in DIZ_DATI li decodifico...%>
						<select class="textblacka" disabled name="cmbValori" style="width=130">
<%	
						for i = 0 to Ubound(sValori) step 2 %>
							<option value="<%=sValori(i)%>"><%=ucase(sValori(i+1))%></option>
<%
						next %>
						</select>
<%
					else	'... altrimenti creo i campi liberi.%>
						<input class="textblacka" type="text" name="cmbValori" style="TEXT-TRANSFORM: uppercase; width=50" maxlength="<%=nMax%>" disabled>
<%
					end if %>
					</td>
				</tr>
<%
			end if %>
			</table>
			<input type="hidden" value="<%=sIdSede%>" name="IdSede">
			<table border="0" CELLPADDING="1" CELLSPACING="2" width="500" align="center">
				<tr>
					<td align="center" class="tbltext1"><br>
						<input type="image" title="Invia Richiesta" src="<%=Session("Progetto")%>/images/conferma.gif" id="image1" name="image1">
					</td>
		</form>
		<form name="frmIndietro" method="post" onsubmit="return CheckWindow()" action="STA_ModRichStage.asp">
			<input type="hidden" name="nRichSta" value="<%=nIdRic%>">
					<td align="center" colspan="2" class="tbltext1"><br>
						<input type="image" title="Indietro" src="<%=Session("Progetto")%>/images/indietro.gif" id="image1" name="image1">
					</td>
		</form>
				</tr>
			</table>
<%
	CASE ELSE %>
		<form name="frmInsCriteri" method="POST" action="STA_CnfCriteriSel.asp?Action=INS" onsubmit="return ControlloDati()">
<%
		if mid(sArea,1,4) = "TAB_" then
			sSQL =	"SELECT ID_CAMPO FROM DIZ_DATI" &_
					" WHERE ID_TAB ='"& sArea & "' AND ID_CAMPO LIKE 'ID%' AND ID_TAB_RIF IS NULL"
		else
			sSQL = "SELECT ID_CAMPO FROM DIZ_DATI" &_
					" WHERE ID_TAB ='"& sArea & "' AND ID_CAMPO LIKE 'ID%' AND ID_TAB_RIF IS NULL"
		end if
'PL-SQL * T-SQL  
SSQL %> = TransformPLSQLToTSQL (SSQL %>) 
		set rsTabRif = CC.Execute(sSQL) %>
			<input type="hidden" value="<%=rsTabRif(0)%>" name="txtCampoArea">
<%
		rsTabRif.Close
		set rsTabRif = nothing

		sSQL =	"SELECT ID_CAMPO FROM DIZ_DATI" &_ 
				" WHERE ID_TAB ='"& sSpecifico & "' AND ID_CAMPO LIKE 'ID%' AND ID_TAB_RIF IS NULL"
'PL-SQL * T-SQL  
SSQL	%> = TransformPLSQLToTSQL (SSQL	%>) 
		set rsTabRif = CC.Execute(sSQL)	%>
			<input type="hidden" value="<%=rsTabRif(0)%>" name="txtCampoSpecifico">
<%
		rsTabRif.Close
		set rsTabRif = nothing %>
			<input type="hidden" name="AREA" value="<%=sArea%>">
			<input type="hidden" name="SPECIFICO" value="<%=sSpecifico%>">
			<input type="hidden" name="IDTAB" value="<%=sIDtab%>">
			<input type="hidden" name="txtCampoTab" value="<%=sCondizione%>">
			<input type="hidden" name="txtIDRic" value="<%=nIdRic%>">
			<input type="hidden" name="txtTipo" value="A">
			<table border="0" CELLPADDING="1" CELLSPACING="2" width="500" align="center">
				<tr height="17">
					<td width="200" class="tbltext1"><b>Selezionare l'area di interesse*</b></td>
					<td width="270" class="tbltext1"><b>
						<input class="textblacka" readonly type="text" name="txtArea" style="width=270px">
						<input class="textblacka" type="hidden" name="txtTipoArea" value></b>
					</td>
					<td align="center" width="30" class="tbltext1">
						<a href="Javascript:SelArea('<%=sArea%>')" ID="imgPunto1" name="imgPunto1"><img border="0" src="<%=Session("Progetto")%>/images/bullet1.gif"></a>
					</td>
				</tr>
				<tr height="17">
					<td width="200" class="tbltext1"><b>Selezionare lo specifico dell'area*</b></td>
					<td width="270" class="tbltext1"><b>
						<input class="textblacka" readonly type="text" name="txtSpecifico" style="width=270px">
						<input type="hidden" name="txtTipoSpecifico" value></b>
					</td>
					<td align="center" width="30" class="tbltext1"><b>
						<a href="Javascript:SelSpecifico('<%=sSpecifico%>')" ID="imgPunto2" name="imgPunto2"><img border="0" src="<%=Session("Progetto")%>/images/bullet1.gif"></a>
					</td>
				</tr>
				<tr height="17">
					<td colspan="3">&nbsp;</td>
				</tr>
<%		
			if sCondizione <> "" then %>		
				<tr height="2">
					<td width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
				</tr>
				<tr class="textBlack">
					<td colspan="3" align="left" valign="top"><b>Spuntare se si vuole indicare un valore : </b>
						<input type="checkbox" id="ckbValore" name="ckbValore" onclick="ChkAbil()">
					</td>
				</tr>
				<tr height="2">
					<td width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
				</tr>
				<tr height="17">
					<td colspan="3">&nbsp;</td>
				</tr>
				<tr height="17">
					<td width="200" class="tbltext1"><b>Condizione Logica</b></td>
<%
				sSQL =	"SELECT ID_CAMPO_DESC,TIPO_OPLOG,TIPO_CAMPO,LEN_CAMPO FROM DIZ_DATI" &_
						" WHERE ID_TAB='" & sIDtab & "' AND ID_CAMPO='" & sCondizione & "'" 
				nMax=100
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
				set rsDizDati = cc.Execute(sSQL)
					bCheck	= false
					bCheck1 = false
					sTipoDato = rsDizDati("TIPO_CAMPO")
					if not isnull(rsDizDati("LEN_CAMPO"))  then
						nMax=clng(rsDizDati("LEN_CAMPO"))
					end if 
					if  rsDizDati("TIPO_OPLOG") <> "" then
						bCheck = true
						sTipoOplg	= split(rsDizDati("TIPO_OPLOG"),"|")
					end if 
					if  rsDizDati("ID_CAMPO_DESC") <> "" then
						bCheck1 = true
						sValori	= split(rsDizDati("ID_CAMPO_DESC"),"|")
					end if 
				rsDizDati.Close %>
					<td width="270" colspan="2" class="tbltext1"><b>
<%	
					sSQL = ""
					if bCheck  then
						sSQL = sSQL & " CODICE IN ("
						for i = 0 to Ubound(sTipoOplg)
							sSQL = sSQL & "'" & sTipoOplg(i) & "'"
							if i <> (Ubound(sTipoOplg)) then
								sSQL = sSQL & ","
							end if 
						next
						sSQL = sSQL & ")"
					end if	
					aOperLog = decodTadesToArray("OPLOG",Date,sSQL,0,"0") %>
						<select disabled class="textblacka" name="cmbLogica" style="width=130">
							<option value></option>
<%
					if not IsNull(aOperLog(0,0,0)) then
						for yy = 0 to ubound(aOperLog)-1 %>
							<option value="<%=aOperLog(yy,yy,yy)%>"><%=aOperLog(yy,yy+1,yy)%></option>
<%
						next
					end if
					erase aOperLog %>
						</select>
						<input type="hidden" value="<%=sTipoDato%>" name="txtTipoDato"></b>
					</td>
				</tr>
				<tr height="17">
					<td width="200" class="tbltext1"><b>Valore<label id="Compreso"></label></b></td>
					<td width="270" colspan="2" class="tbltext1">
<%
					if bCheck1 then		'==>Se i valori sono definiti in DIZ_DATI li decodifico ...%>
						<select class="textblacka" disabled name="cmbValori" style="width=130">
<%
						for i = 0 to Ubound(sValori) step 2 %>
							<option value="<%=sValori(i)%>"><%=ucase(sValori(i+1))%></option>
<%
						next %>
						</select>
<%
					else	'... altrimenti creo i campi liberi.%>
						<input class="textblacka" type="text" name="cmbValori" style="TEXT-TRANSFORM: uppercase; width=50" maxlength="<%=nMax%>" disabled>
<%
					end if %>
					</td>
				</tr>
<%
			end if %>
			</table>
			<input type="hidden" value="<%=sIdSede%>" name="IdSede">
			<table border="0" CELLPADDING="1" CELLSPACING="2" width="500" align="center">
				<tr>
					<td class="tbltext1" align="center"><br>
						<input type="image" title="Invia Richiesta" src="<%=Session("Progetto")%>/images/conferma.gif" id="image1" name="image1">
					</td>
		</form>
		<form name="frmIndietro" method="post" onsubmit="return CheckWindow()" action="STA_ModRichStage.asp">
			<input type="hidden" name="nRichSta" value="<%=nIdRic%>">
					<td colspan="2" class="tbltext1" align="center"><br>
						<input type="image" title="Indietro" src="<%=Session("Progetto")%>/images/indietro.gif" id="image1" name="image1">
					</td>
		</form>
				</tr>
			</table>
<%
END SELECT
	 
if sIDtab <> "SEL_TAB" then %>
	<br>
	<table border="0" CELLPADDING="0" CELLSPACING="0" width="500" align="center">
		<tr height="2">
			<td class="sfondocomm" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
		</tr>
		<tr height="2" class="textblack" align="center">
			<td>
				<br>Scegliere le caratteristiche che si vogliono eliminare e premere il tasto <b>Elimina</b>.
			</td>
		</tr>
		<tr height="2" class="textblack" align="center">
			<td><br></td>
		</tr>
		<tr height="2">
			<td class="sfondocomm" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
		</tr>
	</table>
	<br>
	
	<form name="frmCanCriteri" method="POST" action="STA_CnfCriteriSel.asp?Action=CAN" onsubmit="return ControlloCheck()">
<%
	if bVarie then %>
		<input type="hidden" name="IDTAB" value="VARIE">
<%
	end if %>
	<table border="0" cellspacing="2" cellpadding="2" width="500" align="center">
		<tr class="sfondocomm" height="20"> 
			<td colspan="4" align="middle" valign="center">
				<b class="tbltext1">Caratteristiche per la preselezione ambito <%=sDescTab%></b>
			</td>
		</tr>
<%
	sSQL =	"SELECT ID_TAB,ID_CAMPO,COD_OPLOG,VALORE,PRG_REGOLA,PRG_REGOLA_RIF FROM REGOLE_STAGE" &_
			" WHERE ID_RICHSTAGE=" & nIdRic & " AND ID_TAB = '"& sIDtab & "'" &_
			" ORDER BY PRG_REGOLA,PRG_REGOLA_RIF"
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	set rsCriteri = CC.Execute(sSQL)
	if not rsCriteri.EOF then
		z = 0
		aOpLog = decodTadesToArray("OPLOG",date,"",1,0)
		do while not rsCriteri.EOF %>			
		<tr class="tblsfondo"> 
			<td class="tbltext0" align="middle" valign="center" width="10">
				<input type="checkbox" name="chkCan<%=z%>">
				<input type="hidden" name="txtIdTab<%=z%>" value="<%=rsCriteri("ID_TAB")%>">
				<input type="hidden" name="txtIdCam<%=z%>" value="<%=rsCriteri("ID_CAMPO")%>">
				<input type="hidden" name="txtPrgReg<%=z%>" value="<%=rsCriteri("PRG_REGOLA")%>">
				<input type="hidden" name="txtPrgRegRif<%=z%>" value="<%=rsCriteri("PRG_REGOLA_RIF")%>">
			</td>
			<td class="textblack" align="left" valign="center" width="140">
<%
			sSQL =	"SELECT ID_DESC,ID_CAMPO,DESC_CAMPO,ID_CAMPO_RIF,ID_TAB_RIF,TIPO_CAMPO,ID_CAMPO_DESC FROM DIZ_DATI" &_
					" WHERE ID_TAB='" &	rsCriteri("ID_TAB")	& "' AND ID_CAMPO='" & rsCriteri("ID_CAMPO") & "'"
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
			set rsDecod = CC.Execute(sSQL)
			
				Response.Write ucase(rsDecod("DESC_CAMPO"))
				sDecNomTab		= rsDecod("ID_TAB_RIF")
				sTipoCampo		= rsDecod("Tipo_campo")
				sDescNomeCampo  = rsDecod("ID_CAMPO_DESC")
				sDenominazione	= rsDecod("ID_DESC")
				if isnull(rsDecod("ID_CAMPO_rif")) then
					sDecNomCampo	= rsDecod("ID_CAMPO")
				else
					sDecNomCampo	= rsDecod("ID_CAMPO_RIF")
				end if
			rsDecod.Close
			set rsDecod = Nothing %>
			</td>
			<td class="textblack" align="left" valign="center" width="80">
				<%=(UCASE(searchIntoArray(rsCriteri("COD_OPLOG"),aOpLog,0)))%>
			</td>
			<td class="textblack" align="left" valign="center" width="220">
<%
			if sDecNomTab <> "" then
				select case mid(sDecNomTab,1,4)
					case "TAB_"
						on error resume next
						aValore = Split(rsCriteri("Valore"),",",-1,1)
						if err.number <> 0 then
							Response.Write ucase(DecCodVal(mid(sDecNomTab,5), 0, date, rsCriteri("VALORE"),0))
						else
							lung = Ubound(aValore)
							Ciccio =  ""
							for i = 0 to lung 
								Ciccio = Ciccio & ucase(DecCodVal(mid(sDecNomTab,5), 0, date, replace(aValore(i),"$",""),0)) & ","
							next
							Response.Write Server.HTMLEncode(left(Ciccio,len(Ciccio) - 1))
						end if
						on error goto 0
					case "COMU"
						on error resume next
						aValore = Split(rsCriteri("Valore"),",",-1,1)
						if err.number <> 0 then
							sSQL =	"SELECT DESCOM FROM " & sDecNomTab &_
									" WHERE " &	sDecNomCampo & " = " & Trasforma(rsCriteri("VALORE"),sTipoCampo)
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
							set rsDecod = CC.Execute(sSQL)
								Response.Write ucase(rsDecod("DESCOM"))
							rsDecod.Close
							set rsDecod = Nothing
						else
							lung = Ubound(aValore)
							Comune= ""
							if lung > 0 then
								for i = 0 to lung 
									sSQL =	"SELECT DESCOM FROM " & sDecNomTab &_
											" WHERE " &	sDecNomCampo & " = " & Replace(aValore(i),"$","'")
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
									set rsDecod = CC.Execute(sSQL)
										Comune = Comune  &  ucase(rsDecod("DESCOM")) & ","
									rsDecod.Close
									set rsDecod = Nothing
								next
								Response.Write left(Comune,len(Comune) - 1)
							else
								sSQL =	"SELECT DESCOM FROM " & sDecNomTab &_
										" WHERE " &	sDecNomCampo & " ='" & aValore(0) & "'"
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
								set rsDecod = CC.Execute(sSQL)
									Response.write ucase(rsDecod("DESCOM")) 
								rsDecod.Close
								set rsDecod = Nothing
							end if
						end if
						on error goto 0
					case else
						on error resume next
						aValore = Split(rsCriteri("Valore"),",",-1,1)
						if err.number <> 0 then
							sSQL =	"SELECT DENOMINAZIONE FROM " & sDecNomTab &_
									" WHERE " &	sDecNomCampo & " = " & REPLACE(rsCriteri("VALORE"),"'","")
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
							set rsDecod = CC.Execute(sSQL)
								Response.Write ucase(rsDecod("DENOMINAZIONE"))
							rsDecod.Close
							set rsDecod = Nothing
						else
							lung = Ubound(aValore)
							Denom = ""
							for i = 0 to lung 
								sSQL =	"SELECT " & sDenominazione & " FROM " & sDecNomTab &_
										" WHERE " &	sDecNomCampo & " = " & Trasforma(replace(aValore(i),"$",""),sTipoCampo)
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
								set rsDecod = CC.Execute(sSQL)
									Denom = Denom & ucase(rsDecod(0)) & ","
								rsDecod.Close
								set rsDecod = Nothing
							next
							Response.Write left(Denom,len(Denom) - 1)
						end if
						on error goto 0
					end select
				else
					if sDescNomeCampo <> "" then
						aVal = split(sDescNomeCampo,"|")
						for i=0 to ubound(aVal) step 2
							if aVal(i) = REPLACE(rsCriteri("VALORE"),"$","") then
								Response.Write Server.HTMLEncode(UCASE(aVal(i+1)))
							end if
						next
					else
						Response.Write Server.HTMLEncode(replace(rsCriteri("VALORE"),"$",""))
					end if
				end if%>
				</td>
			</tr>
<%
		rsCriteri.MoveNext
		z=z+1 
		loop %>
		<input type="hidden" name="txtIdRich" value="<%=CLng(nIdRic)%>">
		<input type="hidden" name="txtRighe" value="<%=(z-1)%>">
		<input type="hidden" name="AREA" value="<%=sArea%>">
		<input type="hidden" name="SPECIFICO" value="<%=sSpecifico%>">
		<input type="hidden" name="CONDIZIONE" value="<%=sCondizione%>">
		<input type="hidden" name="IdSede" value="<%=sIdSede%>">
			<tr>
				<td align="middle" colspan="4">
					<br><input type="Image" value="Elimina" src="<%=Session("Progetto")%>/images/Elimina.gif" title="Elimina" id="Image2" name="Image2">
				</td>
			</tr>
<%		Erase aOpLog 
	Else %>
			<tr height="17"> 
				<td colspan="4" class="tbltext1" align="middle" valign="center"><br><b>NESSUNO</b></td>
			</tr>
<%
	End if %>
		</table>
<%
	rsCriteri.close
	set rsCriteri = nothing %>
	</form>
<%
end if

'**********************************************************************************************************************************************************************************	
Sub Inizio()%>
	<table width="520" border="0" cellspacing="0" cellpadding="0" height="81">
		<tr>
			<td width="500" height="81" background="<%=session("progetto")%>/images/titoli/strumenti2b.gif" valign="bottom" align="right">
				<table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
					<tr>
						<td width="100%" valign="top" align="right">
							<b CLASS="tbltext1a">Definizione Stage</b>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table> 
	<br>
<%End Sub

'**********************************************************************************************************************************************************************************	
Function Trasforma(valore,tipo)
	select case tipo
		case "D"
			Trasforma = ConvDateToDb(valore)
		case "A"
			Trasforma = "'" & Valore & "'"
		case else
			Trasforma = valore
	end select
	
End Function 
%>
	
<!--#include Virtual ="/Include/closeconn.asp"-->
<!--#include Virtual="/strutt_coda2.asp"-->		
