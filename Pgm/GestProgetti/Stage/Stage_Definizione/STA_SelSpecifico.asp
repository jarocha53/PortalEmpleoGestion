<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #include virtual="/util/dbutil.asp" -->
<!-- #include virtual="/include/openconn.asp" -->
<!-- #include virtual="/include/DecCod.asp" -->

<%
dim sIDArea, sSpecifico, sCampoArea, sCampo
dim sSQL
dim rsSpecifico, rsTitolo, rsDenom


sSpecifico = Request.QueryString ("Specifico")
sIDArea = Request.QueryString ("Area")
sCampoArea = Request.QueryString ("sCampoArea")
sCampo = Request.QueryString ("CampoSpec")

sSQL = "SELECT DESC_TAB FROM DIZ_TAB WHERE ID_TAB='" & Request.QueryString("sTab") & "'"
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
set rsTitolo = CC.Execute(sSQL)
	sTitolo = rsTitolo("DESC_TAB")
rsTitolo.Close
set rsTitolo = Nothing
%>

<html>
<head>
<title><%=sTitolo%></title>
<link REL="STYLESHEET" TYPE="text/css" HREF="<%=session("progetto")%>/fogliostile.css">
	<script language="javascript">
	<!-- #include virtual="/include/help.inc" -->
	
		function InviaSelezione(idArea,Denominazione) {
			opener.document.frmInsCriteri.txtTipoSpecifico.value = idArea
			var ciccio = Denominazione.replace("$", "'")
			opener.document.frmInsCriteri.txtSpecifico.value = ciccio.toUpperCase();
			self.close()
		}
		
		function Chiudi() {
			self.close()
		}
		
		function Annulla()	{
			opener.document.frmInsCriteri.txtSpecifico.value = ""
			opener.document.frmInsCriteri.txtTipoSpecifico.value = ""
			Chiudi()
		}

		function Destro(e) {
			if (navigator.appName == 'Netscape' && 
				(e.which == 3 || e.which == 2))
				return false;
			else if (navigator.appName == 'Microsoft Internet Explorer' && 
					(event.button == 2 || event.button == 3)) {
				alert("Spiacenti, il tasto destro del mouse e' disabilitato");
				return false;
			}
			return true;
		}

		document.onmousedown=Destro;
		if (document.layers) window.captureEvents(Event.MOUSEDOWN);
		window.onmousedown=Destro;
	</script>
</head>
<body>

<form name="frmSelArea">
<table border="0" CELLPADDING="0" CELLSPACING="0" width="460" align="center">
	<tr height="17">
		<td class="sfondomenu" width="67%" height="18">
			<span class="tbltext0"><b>&nbsp;SELEZIONE DEL DETTAGLIO DELL'AREA</b></span>
		</td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
		<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
	</tr>
	<tr>
		<td class="sfondocomm" width="57%" colspan="3">
			<br>Selezionare il dettaglio dell'area che si vuole registrare; premere <b>chiudi</b> per non selezionare nulla
				o scegliere <b>Rimuovi Selezione</b> se si vuole deselezionare una eventuale scelta fatta in precedenza.
		</td>
	</tr>
	<tr height="2">
		<td class="sfondocomm" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
	</tr>
</table>
<br>
<p class="tbltext1" align="center"><b>Selezionare lo specifico per <%=sTitolo%></b></p>

<%
Sub Rimuovi_selezione() %>
	<table width="460" cellspacing="2" cellpadding="1" border="0" align="center">
		<tr align="center">
			<td>
				<b><a class="textred" href="javascript: Annulla()">
				Rimuovi selezione</a></b>
			</td>
		</tr>		
	</table>
<%
End sub

if sSpecifico <> "TSTUD" and sSpecifico <> "TVINC" and sSpecifico <> "TDISP" then

	sSQL = "SELECT ID_DESC FROM DIZ_DATI WHERE ID_TAB = '" &_
		 Request.QueryString("sTab") & "' AND ID_CAMPO ='" & sCampo & "'"
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	set rsDenom = CC.Execute(sSQL)
		sDenominazione = rsDenom("ID_DESC")
	rsDenom.Close
	set rsSpec = nothing

	sSQL =	"SELECT " & sCampo & ", " & sDenominazione & " FROM "& sSpecifico &" A, VALIDAZIONE B" &_
			" WHERE A.ID_VALID = B.ID_VALID AND FL_VALID = 1 AND " & sCampoArea & " = " & clng(sIDArea) & " ORDER BY " & sDenominazione
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	set rsSpecifico = CC.Execute(sSQL)
	
	if not rsSpecifico.EOF then
		Rimuovi_selezione() %>
		<table border="0" CELLPADDING="2" CELLSPACING="2" width="460" align="center">
			<tr>
				<td colspan="2" class="tbltext1">&nbsp;</td>
			</tr>
<%	 		
		do while not rsSpecifico.EOF %>
			<tr>
				<td width="230" class="tblsfondo">
					<a class="textblack" href="Javascript:InviaSelezione('<%=rsSpecifico(0)%>','<%=server.HTMLEncode (Replace(rsSpecifico(1),"'","$"))%>')"><b><%=server.HTMLEncode (ucase(rsSpecifico(1)))%></b></a></td>
<%
			rsSpecifico.MoveNext %>
				<td width="230" class="tblsfondo">
<%
			if not rsSpecifico.EOF then %>
				<a class="textblack" href="Javascript:InviaSelezione('<%=rsSpecifico(0)%>','<%=server.HTMLEncode (Replace(rsSpecifico(1),"'","$"))%>')"><b><%=server.HTMLEncode (ucase(rsSpecifico(1)))%></b></a>
<%
				rsSpecifico.MoveNext
			else %>
				&nbsp;
<%				
			end if %>
				</td>
			</tr>
<%			
		loop
		rsSpecifico.Close
		set rsSpecifico = nothing %>		
		</table>
<%	end if
else
	sCondizione = "VALORE = '" & sIDArea & "'"
	sTabella = sSpecifico
	dData	 = Date()
	nOrder   = 0
	Isa		 = 0
	aSpecifico = decodTadesToArray(sTabella,dData,sCondizione,nOrder,Isa)
	if isArray(aSpecifico) then
		Rimuovi_selezione() %>
		<table border="0" CELLPADDING="2" CELLSPACING="2" width="460" align="center">
			<tr>
				<td colspan="2" class="tbltext1">&nbsp;</td>
			</tr>
<%
		for i = 0 to ubound(aSpecifico) - 1 step 2
			j = i + 1 %>			
			<tr>
				<td width="230" class="tblsfondo">
					<a class="textblack" href="Javascript:InviaSelezione('<%=aSpecifico(i,i,i)%>','<%=server.HTMLEncode (Replace(aSpecifico(i,i+1,i),"'","$"))%>')"><b><%=server.HTMLEncode (ucase(aSpecifico(i,i+1,i)))%></b></a>
				</td>
				<td width="230" class="tblsfondo">
<%				
				if j <= (ubound(aSpecifico) - 1) then %>					
					<a class="textblack" href="Javascript:InviaSelezione('<%=aSpecifico(j,j,j)%>','<%=server.HTMLEncode (Replace(aSpecifico(j,j+1,j),"'","$"))%>')"><b><%=server.HTMLEncode (ucase(aSpecifico(j,j+1,j)))%></b></a>
<%				
				else %>					
					&nbsp;
<%				
				end if %>
				</td>
			</tr>
<%		
		next
		Erase aSpecifico
	end if
end if %>
	
<br>
<table width="460" cellspacing="2" cellpadding="1" border="0" align="center">
	<tr align="center">
		<td>
			<a href="javascript: Chiudi()"><img src="<%=Session("progetto")%>/images/chiudi.gif" title="Chiudi la pagina" border="0" align="absBottom"></a>
		</td>
	</tr>		
</table>

</form>
</body>
</html>
<!-- #include virtual="/include/closeconn.asp" -->
