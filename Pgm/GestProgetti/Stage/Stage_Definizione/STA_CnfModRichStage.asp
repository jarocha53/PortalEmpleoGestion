<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include virtual="/util/portallib.asp"-->
<!--#include Virtual = "/strutt_testa2.asp"-->
<!--#include Virtual = "/include/SysFunction.asp"-->
<!--#include Virtual ="/Include/openconn.asp"-->
<!--#include Virtual ="/Include/ControlDateVB.asp"-->
<!--#include Virtual = "/include/DecCod.asp"-->
<!--#include Virtual = "/include/HTMLEncode.asp"-->
<!--#include virtual= "/util/dbutil.asp"-->

<%
'*********** MAIN ***************
Dim nIdSede, nIdAreaP, nPostiDisp, nIdRichSt
Dim sDescStage, sDDiretta, sChiudi
Dim dDt_IniVal, dDt_IniStage, dDt_FinStage, dDt_Now, dDt_Tmst
dim sSql

nIdRichSt = clng(Request.Form("txtIdRich"))
nIdSede = clng(Request.Form("nSede"))
sDescStage	= strHTMLEncode(Trim(Request.Form("txtDescStage")))
sDescStage	= replace(sDescStage,"'","''")
nPostiDisp	= clng(Request.Form("txtPostiDisp"))

dDt_IniVal	= ConvDateToDB(ConvStringToDate(Request.Form("txtIniVal")))
dDt_IniStage= ConvDateToDB(ConvStringToDate(Request.Form("txtIniStage")))
dDt_FinStage= ConvDateToDB(ConvStringToDate(Request.Form("txtFinStage")))
dDt_Tmst = Request.Form("txtTmst")
dDt_Now	= ConvDateToDB(Now())

sDDiretta = Request.Form("rDomDiretta")
if sDDiretta <> "S" then
	sDDiretta = "N"
end if

sChiudi = Request.Form("txtChiudi")
%>

<form name="frmIndietro" method="post" action="STA_RicStage.asp">
	<input type="hidden" name="txtSede" value="<%=nIdsede%>">
</form>

<%	
if sChiudi = "SI" then
	'''LC_77: 18/0/2003 - Aggiunto DT_INIVAL all'update di Chiusura Validit�.
	sSql =	"Update Domanda_Stage" &_
			" set DT_FIN_VAL=" & ConvDateToDB(date()) &"," &_
			" DT_INIVAL=" & dDt_IniVal &"," &_
			" DT_TMST=" & dDt_Now &_
			" where ID_RICHSTAGE=" & nIdRichSt
else
	sSql =	"Update Domanda_Stage" &_
			" set NUM_POSTI_DISP=" & nPostiDisp &"," &_
			" DT_FINSTAGE=" & dDt_FinStage &"," &_
			" FL_DOMDIRETTA='" & sDDiretta & "'," &_
			" DESC_STAGE='" & sDescStage & "'," &_
			" DT_INISTAGE=" & dDt_IniStage & "," &_
			" DT_INIVAL=" & dDt_IniVal & "," &_
			" DT_TMST=" & dDt_Now &_
			" where ID_RICHSTAGE=" & nIdRichSt
end if

sErrore=Esegui("","Domanda_Stage",session("idutente"),"MOD",sSql,0,dDt_Tmst)
		
IF sErrore="0" then
	if sChiudi = "SI" then %>
		<script>
			alert("La Validit� della proposta di stage � stata chiusa ad oggi");
			frmIndietro.submit()
		</script>
<%
	else %>
		<script>
			alert("Modifica correttamente effettuata");
			frmIndietro.submit()
		</script>
<%
	end if		
ELSE
	Inizio()
%>
	<br><br>
	<table width="500" align="center" border="0" cellspacing="2" cellpadding="1">
		<tr height="20"> 
		  	<td class="tbltext3" align="middle" width="500">
				<b>Errore in fase di modifica.</b>
			</td>
		</tr>
		<tr height="20"> 
		  	<td class="tbltext3" align="middle" width="500">
				<b><%=sErrore%></b>
			</td>
		</tr>
	</table>
	<br><br>
	<table width="500" cellspacing="2" cellpadding="1" border="0">
		<tr align="center">
			<td>
				<a href="javascript:history.back()"><img src="<%=Session("progetto")%>/images/Indietro.gif" title="Pagina precedente" border="0" align="absBottom"></a>
			</td>
		</tr>
	</table>
<%
END IF

'**********************************************************************************************************************************************************************************	
Sub Inizio()
%>
	<table border="0" width="520" cellspacing="0" cellpadding="0" height="70">
		<tr>
			<td width="500" background="<%=session("progetto")%>/images/titoli/strumenti2b.gif" height="70" valign="bottom" align="right">
				<table border="0" background width="500" height="23" cellspacing="0" cellpadding="0">
					<tr>
						<td width="100%" valign="top" align="right"><b CLASS="tbltext1a">Definizione Stage
						  </b></td>
					</tr>
				</table>
			</td>
		</tr>
	</table> 
	<br>
	<!-- Linguetta superiore -->
		<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
			<tr height="18">
				<td class="sfondomenu" width="67%" height="18"><span class="tbltext0"><b>&nbsp;CONFERMA MODIFICA DOMANDA STAGE </b></span></td>
				<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
				<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
			</tr>
		</table>
		<!-- Commento -->
		<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
			<tr>
				<td align="left" class="sfondocomm">
					Pagina di conferma della modifica della proposta di stage.
				</td>
			</tr>
			<tr height="2">
				<td class="sfondocomm" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif">
				</td>
			</tr>
		</table>
	<br>
<%
End Sub %>

<!--#include Virtual ="/Include/closeconn.asp"-->
<!--#include Virtual="/strutt_coda2.asp"-->	
