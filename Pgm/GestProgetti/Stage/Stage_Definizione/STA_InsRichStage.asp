<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include virtual="/util/portallib.asp"-->
<!--#include virtual="/strutt_testa2.asp"-->
<!--#include virtual="/include/openconn.asp"-->
<!--#include virtual="/include/DecCod.asp"-->
<%
	if ValidateService(session("idutente"),"STA_RICSTAGE",cc) <> "true" then 
		response.redirect "/util/error_login.asp"
	end if
%>

<script LANGUAGE="Javascript">
//include del file per fare i controlli sulla validit� delle date
<!--#include virtual="/Include/ControlDate.inc"-->
//include del file per fare i controlli sulla numericit� dei campi
<!--#include virtual="/Include/ControlNum.inc"-->
<!--#include virtual="/Include/ControlString.inc"-->
<!--#include virtual="/Include/help.inc"-->


var windowSpiega

function Ricarica()
{			
	//ChiudiFigli();
	
	if (windowSpiega != null)
	{
		windowSpiega.close();
	}
	if (document.frmDomandaStage.cmbTipoArea.value=="")
		{
			alert("Selezionare un'area professionale.");
			return false;
		}
	else
		{
			document.FrmRicarica.Area.value = document.frmDomandaStage.cmbTipoArea.value;
			document.FrmRicarica.txtDescrizione.value = document.frmDomandaStage.txtDescStage.value;
			document.FrmRicarica.txtTipoRicerca.value = document.frmDomandaStage.cmbTipoRicerca.value;
			document.FrmRicarica.txtTipoStage.value = document.frmDomandaStage.cmbTipoStage.value;
			document.FrmRicarica.txtPosti.value = document.frmDomandaStage.txtPostiDisp.value;
			document.FrmRicarica.txtValidita.value = document.frmDomandaStage.txtIniVal.value;
			document.FrmRicarica.txtInizio.value = document.frmDomandaStage.txtIniStage.value;
			document.FrmRicarica.txtFine.value = document.frmDomandaStage.txtFinStage.value;
			document.FrmRicarica.submit();
			return true;
		}
}

function Spiega()
{	
	var nId
	var aValore
	
	if (document.frmDomandaStage.cmbTipoArea.value=="")
	{
			alert("Selezionare un profilo professionale.");
			return 
	}
	else
	{
	
	aValore = document.frmDomandaStage.cmbTipoProf.value.split("$")
	nId = aValore[0]
	
	document.FrmSpiega.id_figprof.value = nId;
	document.FrmSpiega.submit();
	}
}	 	
//Funzione per i controlli dei campi 
function ControllaDati(frmDomandaStage){
	
	if (frmDomandaStage.txtIdSede.value == 0) {
		alert ("Attenzione: Impossibile effettuare l'inserimento, manca la definizione della sede!")
		return false
	}
	//Descrizione
	frmDomandaStage.txtDescStage.value = TRIM(frmDomandaStage.txtDescStage.value)
	if (frmDomandaStage.txtDescStage.value == ""){
		alert("Il campo Descrizione Stage � obbligatorio")
		frmDomandaStage.txtDescStage.focus() 
		return false
	}
	if(!ValidateInputStringWithNumbereChar(frmDomandaStage.txtDescStage.value)) {
		alert("Il campo Descrizione � formalmente errato")
		frmDomandaStage.txtDescStage.focus() 
		return false
	}
		
	//Area Professionale
	if (frmDomandaStage.cmbTipoArea.value == "") {
		alert ("Il campo Area Professionale � obbligatorio")
		frmDomandaStage.cmbTipoArea.focus()
		return false
	}
	
	//Profilo Professionale
	if (frmDomandaStage.cmbTipoProf.value == "") {
		alert ("Il campo Profilo Professionale � obbligatorio")
		frmDomandaStage.cmbTipoProf.focus()
		return false
	}
	
	//Tipo ricerca
	if (frmDomandaStage.cmbTipoRicerca.value == "") {
		alert ("Il campo Tipo di Ricerca � obbligatorio")
		frmDomandaStage.cmbTipoRicerca.focus()
		return false
	}
	
	//Tipologia di Stage
	if (frmDomandaStage.cmbTipoStage.value == "") {
		alert ("Specificare la Richiesta di Stage")
		frmDomandaStage.cmbTipoStage.focus()
		return false
	}
		
	//Posti disponibili
	frmDomandaStage.txtPostiDisp.value = TRIM(frmDomandaStage.txtPostiDisp.value)
	if (frmDomandaStage.txtPostiDisp.value == "") {
		alert("Il Numero Candidati � obbligatorio")
		frmDomandaStage.txtPostiDisp.focus() 
		return false
	}
	if (frmDomandaStage.txtPostiDisp.value != ""){
		if (!IsNum(frmDomandaStage.txtPostiDisp.value)){
			alert("Il dato deve essere numerico")
			frmDomandaStage.txtPostiDisp.focus() 
			return false
		}
		if (eval(frmDomandaStage.txtPostiDisp.value) == 0){
			alert("Il Numero Candidati non pu� essere zero")
			frmDomandaStage.txtPostiDisp.focus()
			return false
		}
	}
	
	//Date
	//===>Validit� dal
	frmDomandaStage.txtIniVal.value = TRIM(frmDomandaStage.txtIniVal.value)
	if (frmDomandaStage.txtIniVal.value == "") {
		alert('La data di inizio della Validit� � obbligatoria')
		frmDomandaStage.txtIniVal.focus()
		return false
	}
	if (!ValidateInputDate(frmDomandaStage.txtIniVal.value)) {
		frmDomandaStage.txtIniVal.focus();
		return false;
	}
	if (!ValidateRangeDate(frmDomandaStage.txtOggi.value,frmDomandaStage.txtIniVal.value)) {
		alert('La data di inizio della Validit� non pu� essere inferiore alla data odierna')
		frmDomandaStage.txtIniVal.focus()
		return false
	}
	//===>Data Inizio Stage
	frmDomandaStage.txtIniStage.value = TRIM(frmDomandaStage.txtIniStage.value)
	if (frmDomandaStage.txtIniStage.value == "") {
		alert('Indicare la data di inizio dello Stage')
		frmDomandaStage.txtIniStage.focus()
		return false
	}
	if (!ValidateInputDate(frmDomandaStage.txtIniStage.value)) {
		frmDomandaStage.txtIniStage.focus();
		return false;
	}
	if (!ValidateRangeDate(frmDomandaStage.txtOggi.value,frmDomandaStage.txtIniStage.value)) {
		alert('La data di inizio dello Stage non pu� essere inferiore alla data odierna')
		frmDomandaStage.txtIniStage.focus()
		return false
	}
	if (!ValidateRangeDate(frmDomandaStage.txtIniVal.value,frmDomandaStage.txtIniStage.value)) {
		alert('Errata indicazione delle date: la Validit� non pu� essere successiva alla data di inizio Stage')
		frmDomandaStage.txtIniStage.focus()
		return false
	}
	//===>Data Fine Stage
	frmDomandaStage.txtFinStage.value = TRIM(frmDomandaStage.txtFinStage.value)
	if (frmDomandaStage.txtFinStage.value == "") {
		alert('Indicare la data fine dello Stage')
		frmDomandaStage.txtFinStage.focus()
		return false
	}
	if (!ValidateInputDate(frmDomandaStage.txtFinStage.value)) {
		frmDomandaStage.txtFinStage.focus();
		return false;
	}
	if (!ValidateRangeDate(frmDomandaStage.txtOggi.value,frmDomandaStage.txtFinStage.value)) {
		alert('La data di fine Stage non pu� essere inferiore alla data odierna')
		frmDomandaStage.txtFinStage.focus()
		return false
	}
	if (!ValidateRangeDate(frmDomandaStage.txtIniStage.value,frmDomandaStage.txtFinStage.value)) {
		alert('La data di fine Stage non pu� essere precedente alla data di inizio dello Stage')
		frmDomandaStage.txtFinStage.focus()
		return false
	}
	//===>Sede di svolgimento dello stage
	/*
	if ((frmDomandaStage.rDomDiretta[0].checked == false)&&(frmDomandaStage.rDomDiretta[1].checked == false)) {
		alert("Indicare il luogo di svolgimento dello stage")
		frmDomandaStage.rDomDiretta[0].focus()
		return false
	}
	*/
	return true
}	
</script>

<%
dim nIdSede, sDescSede, sDescImpr
dim rsSede, sSQL, RsA, sqlA,sArea
dim cMsgDomDir, cMsgDomIndir,sValidita,sInizio,sFine
dim sDescStage,sTipoRicerca,sTipoStage,sPosti

sArea = Request.Form("Area")
sDescStage = Request.Form("txtDescrizione")
sTipoRicerca = Request.Form("txtTipoRicerca")
sTipoStage = Request.Form("txtTipoStage")
sPosti = Request.Form("txtPosti")
sValidita = Request.Form("txtValidita") 
sInizio = Request.Form("txtInizio")
sFine = Request.Form("txtFine")

cMsgDomDir = "Lo Stage si svolger� presso una delle sedi operative dell'azienda"
cMsgDomIndir = "Lo Stage si svolger� presso una azienda esterna"
nIdSede = clng(Request.Form("hdnIdSede"))

set rsSede = Server.CreateObject("ADODB.Recordset")
	sSQL =	"SELECT DESCRIZIONE, RAG_SOC FROM SEDE_IMPRESA SI, IMPRESA I" &_
			" WHERE SI.ID_SEDE=" & nIdSede &_
			" AND SI.ID_IMPRESA = I.ID_IMPRESA"
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
rsSede.open sSQL,CC,3
	if not rsSede.EOF then
		sDescImpr = rsSede("RAG_SOC")
		sDescSede = rsSede("DESCRIZIONE")
	end if
rsSede.Close()
set rsSede = nothing

set RsA=Server.CreateObject("ADODB.Recordset")
sqlA =	"Select id_areaprof, denominazione from aree_professionali a," &_
		" validazione b where a.id_valid=b.id_valid and fl_valid=1 order by denominazione"
'''sqlA =	"Select id_areaprof, denominazione from aree_professionali" &_
'''		" order by denominazione"
'PL-SQL * T-SQL  
SQLA = TransformPLSQLToTSQL (SQLA) 
RsA.open sqlA,CC,3
%>
<form method="post" name="FrmRicarica" action="STA_InsRichStage.asp">
		<input type="hidden" name="Area" value>
		<input type="hidden" name="txtDescrizione" value>
		<input type="hidden" name="txtTipoRicerca" value>
		<input type="hidden" name="txtTipoStage" value>
		<input type="hidden" name="txtPosti" value>
		<input type="hidden" name="txtValidita" value>
		<input type="hidden" name="txtInizio" value>
		<input type="hidden" name="txtFine" value>
		<input type="hidden" name="hdnIdSede" value="<%=nIdSede%>">
</form>
<form method="post" name="FrmSpiega" target="_New" action="/pgm/bilanciocompetenze/DettagliArea/Focusfigprof.asp">
		<input type="hidden" name="VisMenu" value="NO">
		<input type="hidden" name="id_figprof" value>
</form>		
		
<form name="Indietro" action="STA_RicStage.asp" method="post">
	<input type="hidden" name="txtSede" value="<%=nIdSede%>">
</form>	

<table border="0" width="520" height="81" cellspacing="0" cellpadding="0">
	<tr>
		<td width="500" height="81" background="<%=Session("Progetto")%>/images/titoli/strumenti2b.gif" valign="bottom" align="right">
			<table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
				<tr>
					<td width="100%" valign="top" align="right">
						<b class="tbltext1a">Definizione Stage</b>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<br>
<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
	<tr height="18">
		<td class="sfondomenu" width="67%" height="18">
			<span class="tbltext0">
				<b>&nbsp;INSERIMENTO RICHIESTA DI STAGE</b>
			</span>
		</td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
		<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">(*) campi obbligatori</td>
	</tr>
</table>
<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
	<tr>
		<td align="left" class="sfondocomm">
			In questa schermata e' possibile effettuare l'inserimento di una nuova richiesta di stage,
			compilando i campi presenti con i dati necessari alla registrazione.
			Oltre alla compilazione dei campi obbligatori, per un corretto inserimento si dovra' indicare 
			anche se la sede di svolgimento dello stage sara' interna o esterna all'azienda proponente.  
			<a href="Javascript:Show_Help('/Pgm/help/GestProgetti/Stage/Stage_Definizione/STA_InsRichStage')">
			<img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
		</td>
	</tr>
	<tr height="2">
		<td class="sfondocomm" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif">
		</td>
	</tr>
</table>
<br>

<form method="post" name="frmDomandaStage" onsubmit="return ControllaDati(this)" action="STA_CnfInsRichStage.asp">
	<input type="hidden" name="txtIdSede" value="<%=nIdSede%>">
	<input type="hidden" name="txtOggi" value="<%=ConvDateToString(date())%>">
		
	<table border="0" cellpadding="0" cellspacing="1" width="500">
		<tr height="20">
			<td align="left" colspan="2" nowrap class="tbltext1">
				<b>Denominazione Azienda:</b>
			</td>
			<td align="left" colspan="2" width="65%">
				<p class="textblack">
					<b><%=sDescImpr%></b>&nbsp;-&nbsp;<%=sDescSede%>
				</p>
			</td>
		</tr>
		<tr>       
			<td align="left" colspan="2" nowrap class="tbltext1">
				<b>Descrizione Stage*</b><br>
				Disponibili <b><label name="NumCaratteri" id="NumCaratteri">150</label></b>
					caratteri 
			</td>
			<td align="left" colspan="2">
				<textarea name="txtDescStage" onKeyup="JavaScript:CheckLenTextArea(document.frmDomandaStage.txtDescStage,NumCaratteri,150)" class="textblack" cols="58" rows="2"><%=sDescStage%></textarea>
          
            </td>
        </tr>
		<tr>
		       <td align="left" colspan="2" nowrap class="tbltext1">
					<b>Area Professionale*</b>
				</td>
		        <td class="textblack" ALIGN="LEFT" colspan="2"> 
					<select style="size:30" name="cmbTipoArea" class="textblacka" onchange="javascript:return Ricarica()">						
						<option value></option>
<%						
						do until RsA.EOF
							if sArea <> ""  then	
								if clng(sArea) = clng(RsA.Fields("ID_AREAPROF")) then		
									Response.Write "<option value=" & clng(RsA.Fields("ID_AREAPROF"))& " selected>" & ucase(RsA.Fields("Denominazione")) & "</option>"
								else
									Response.Write "<option value=" & clng(RsA.Fields("ID_AREAPROF"))& ">" & ucase(RsA.Fields("Denominazione")) & "</option>"				
								end if
							else
								Response.Write "<option value=" & clng(RsA.Fields("ID_AREAPROF")) & ">" & ucase(RsA.Fields("Denominazione")) & "</option>"	
							end if
							RsA.MoveNext
						loop
						RsA.Close
						set RsA =nothing
						sqlA =""
%>					</select>
				</td>
			</tr>
		
		
<%		
	
		sqlP = "Select id_figprof, denominazione" &_
				" from figureprofessionali a,validazione b " &_
				" where id_areaprof=" & clng(sArea)&_
				" and a.id_valid=b.id_valid" &_
				" and fl_valid = 1 order by denominazione"
		
		set RRP=Server.CreateObject("ADODB.Recordset")
		
'PL-SQL * T-SQL  
SQLP = TransformPLSQLToTSQL (SQLP) 
		RRP.Open sqlP, CC, 1, 2
		
%>	
		
			<tr>
		       <td align="left" colspan="2" nowrap class="tbltext1">
					<b>Profilo professionale*</b>
				</td>
		        <td class="textblacka" align="left"> 
					<select name="cmbTipoProf" class="textblacka">
						
<%		 				Do Until RRP.EOF
							If clng(sFigura)=clng(RRP.Fields("ID_FIGPROF")) Then
%>
								<option value="<%=clng(RRP.Fields("ID_FIGPROF"))%>" selected><%=ucase(RRP.Fields("Denominazione"))%></option>
<%	
							Else
%>
								<option value="<%=clng(RRP.Fields("ID_FIGPROF"))%>"><%=ucase(RRP.Fields("Denominazione"))%></option>
<%	
							End if
							RRP.MoveNext
						Loop
						RRP.Close
						set RRP =nothing
						sqlP =""						
%>					</select>
				</td>
				<td width="10">	
					<a href="Javascript:Spiega()"><img border="0" src="<%=Session("Progetto")%>/images/bullet1.gif" alt="Descrizione del profilo professionale"></a>
				</td>
			</tr>
			<tr>
		        <td align="left" colspan="2" nowrap class="tbltext1">
					<b>Tipo di ricerca*</b>
				</td>
		        <td class="textblacka" colspan="2">
					<select name="cmbTipoRicerca" class="textblack">
						<option value></option>
						<option value="0" <%if sTipoRicerca="0" then Response.Write "selected" end if%>> PER AREA PROFESSIONALE</option>
						<option value="1" <%if sTipoRicerca="1" then Response.Write "selected" end if%>> PER PROFILO PROFESSIONALE</option>
					</select>
				</td>
			</tr>
		<tr> 
			<td align="left" colspan="2" nowrap class="tbltext1">
				<b>Richiesta Stage*</b>
			</td>
			<td colspan="2" align="left">
				<select name="cmbTipoStage" class="textblacka">						
					<option value></option>
					<option value="<%=mid(session("progetto"),2)%>" <%if sTipoStage=mid(session("progetto"),2) then Response.Write "selected" end if%>>PRIVATA - DI PROGETTO</option>	
					<option value="PUB" <%if sTipoStage="PUB" then Response.Write "selected" end if%>>PUBBLICA</option>	
				</select>
			</td>			
		</tr>
        <tr>
			<td align="left" colspan="2" class="tbltext1">
				<b>Numero Candidati*</b>
			</td>
			<td align="left" colspan="2">
				<input name="txtPostiDisp" class="textblack" type="text" style="TEXT-TRANSFORM:uppercase" size="10" maxlength="50" value="<%=sPosti%>">
			</td>
        </tr>
        <tr>		
			<td align="left" colspan="2" nowrap class="tbltext1">
				<b>Richiesta Valida dal*</b><br>&nbsp;(gg/mm/aaaa)
			</td>			
			<td align="left" colspan="2">
				<input name="txtIniVal" class="textblack" type="text" size="15" maxlength="10" value="<%=sValidita%>">
			</td>
		</tr>
        <tr>		
			<td align="left" colspan="2" nowrap class="tbltext1">
				<b>Periodo stage dal/al*</b><br>&nbsp;(gg/mm/aaaa)
			</td>			
			<td align="left" colspan="2">
				<input name="txtIniStage" class="textblack" type="text" size="15" maxlength="10" value="<%=sInizio%>">&nbsp;/
			    <input name="txtFinStage" class="textblack" type="text" size="15" maxlength="10" value="<%=sFine%>">
			</td>
		</tr>	
		
		<tr>
            <td align="left" colspan="4">&nbsp;</td>
		</tr>
		<input name="rDomDiretta" class="textblack" type="hidden" value="S">
		<!--		<tr>            <td align="left" colspan="4" nowrap class="tbltext1">				<input name="rDomDiretta" class="textblack" type="radio" value="S">				&nbsp;<b><%'=cMsgDomDir%>*</b>			</td>		</tr>		<tr>			<td align="left" colspan="4" class="tbltext1">				<input name="rDomDiretta" class="textblack" type="radio" value="N">				&nbsp;<b><%'=cMsgDomIndir%>*</b>			</td>		</tr>		-->
		<tr>
            <td align="left" colspan="4">&nbsp;</td>
		</tr>
	</table>
	<table cellpadding="0" cellspacing="0" width="500" border="0">
		<tr>
			<td align="right">
			<a href="javascript:Indietro.submit()">
			<img src="<%=Session("progetto")%>/images/indietro.gif" border="0" alt="Torna alla pagina precedente">
			</a>
			</td>
			<td width="5%"></td>
			<td align="left">
			<input type="image" name="Invia" src="<%=Session("progetto")%>/images/conferma.gif" value="Registra" alt="Conferma la registrazione dei dati">
			</td>
		</tr>
	</table>
</form>

<!--#include virtual="/strutt_coda2.asp"-->
<!--#include virtual="/include/closeconn.asp"-->
