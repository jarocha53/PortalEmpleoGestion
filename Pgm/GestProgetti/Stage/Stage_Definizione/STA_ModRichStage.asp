<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include virtual="/util/portallib.asp"-->
<!--#include virtual="/strutt_testa2.asp"-->
<!--#include virtual="/include/openconn.asp"-->
<!--#include virtual="/include/DecCod.asp"-->
<%
	'if ValidateService(session("idutente"),"STA_VISIMPRESE",cc) <> "true" then 
	'	response.redirect "/util/error_login.asp"
	'end if
%>

<script LANGUAGE="Javascript">
//include del file per fare i controlli sulla validit� delle date
<!--#include virtual="/Include/ControlDate.inc"-->
//include del file per fare i controlli sulla numericit� dei campi
<!--#include virtual="/Include/ControlNum.inc"-->
<!--#include virtual="/Include/ControlString.inc"-->
<!--#include virtual="/Include/help.inc"-->

//Funzione per la chiusura della Validit� 
function ChiudiVal(){
	if (ControllaDati(frmDomandaStage)== true) {
		//inizio Am
		if (!ValidateRangeDate(document.frmDomandaStage.txtIniVal.value,document.frmDomandaStage.txtOggi.value)) {
			alert("Per poter chiudere la validit�, la data di inizio della Validit� deve essere uguale o inferiore alla data odierna");
			frmDomandaStage.txtIniVal.focus();
		}
		else{
		//fine Am
			if (confirm("Si � sicuri di voler chiudere la Validit� della richiesta di stage alla data odierna?")) {
				condizione = "SI";
				document.frmDomandaStage.txtChiudi.value = condizione;
				document.frmDomandaStage.submit()
			}
		}		
	}
}	

//Funzione per i controlli dei campi 
function ControllaDati(frmDomandaStage){
	//Descrizione
	frmDomandaStage.txtDescStage.value = TRIM(frmDomandaStage.txtDescStage.value)
	if (frmDomandaStage.txtDescStage.value == ""){
		alert("Il campo Descrizione Stage � obbligatorio")
		frmDomandaStage.txtDescStage.focus() 
		return false
	}
	//if(!ValidateInputStringWithNumber(frmDomandaStage.txtDescStage.value)) {
		if(!ValidateInputStringWithNumbereChar(frmDomandaStage.txtDescStage.value)) {
	
		alert("Il campo Descrizione � formalmente errato")
		frmDomandaStage.txtDescStage.focus() 
		return false
	}
			
	//Posti disponibili
	frmDomandaStage.txtPostiDisp.value = TRIM(frmDomandaStage.txtPostiDisp.value)
	if (frmDomandaStage.txtPostiDisp.value == "") {
		alert("Il Numero Candidati � obbligatorio")
		frmDomandaStage.txtPostiDisp.focus() 
		return false
	}
	if (frmDomandaStage.txtPostiDisp.value != ""){
		if (!IsNum(frmDomandaStage.txtPostiDisp.value)){
			alert("Il dato deve essere numerico")
			frmDomandaStage.txtPostiDisp.focus() 
			return false
		}
		if (eval(frmDomandaStage.txtPostiDisp.value) == 0){
			alert("Il Numero Candidati non pu� essere zero")
			frmDomandaStage.txtPostiDisp.focus()
			return false
		}
		//LC**
		/*if ((eval(frmDomandaStage.hdnPostiAss.value) > 0) && (eval(frmDomandaStage.txtPostiDisp.value) < eval(frmDomandaStage.hdnPostiAss.value))) {
			alert("Il Numero Candidati non pu� essere inferiore a quello dei Posti Assegnati")
			frmDomandaStage.txtPostiDisp.focus()
			return false
		}*/
		//FINE LC**
		//inizio Am
		if (eval(frmDomandaStage.txtPostiDisp.value) < eval(frmDomandaStage.hdnNumCand.value)) {
			alert("Il Numero Candidati non puo' essere inferiore rispetto a quello attuale")
			frmDomandaStage.txtPostiDisp.focus()
			return false
		}
		//Fine Am
	}
	
	//Date
	//===>Validit� dal
	frmDomandaStage.txtIniVal.value = TRIM(frmDomandaStage.txtIniVal.value)
	if (frmDomandaStage.txtIniVal.value == "") {
		alert('La data di inizio della Validit� � obbligatoria')
		frmDomandaStage.txtIniVal.focus()
		return false
	}
	if (!ValidateInputDate(frmDomandaStage.txtIniVal.value)) {
		frmDomandaStage.txtIniVal.focus();
		return false;
	}
	/*if (!ValidateRangeDate(frmDomandaStage.txtOggi.value,frmDomandaStage.txtIniVal.value)) {
		alert('La data di inizio della Validit� non pu� essere inferiore alla data odierna')
		frmDomandaStage.txtIniVal.focus()
		return false
	}*/
	//===>Data Inizio Stage
	frmDomandaStage.txtIniStage.value = TRIM(frmDomandaStage.txtIniStage.value)
	if (frmDomandaStage.txtIniStage.value == "") {
		alert('Indicare la data di inizio dello stage')
		frmDomandaStage.txtIniStage.focus()
		return false
	}
	if (!ValidateInputDate(frmDomandaStage.txtIniStage.value)) {
		frmDomandaStage.txtIniStage.focus();
		return false;
	}
	/*if (!ValidateRangeDate(frmDomandaStage.txtOggi.value,frmDomandaStage.txtIniStage.value)) {
		alert('La data di inizio dello Stage non pu� essere inferiore alla data odierna')
		frmDomandaStage.txtIniStage.focus()
		return false
	}*/
	if (!ValidateRangeDate(frmDomandaStage.txtIniVal.value,frmDomandaStage.txtIniStage.value)) {
		alert('Errata indicazione delle date: la Validit� non pu� essere successiva alla data di inizio Stage')
		frmDomandaStage.txtIniStage.focus()
		return false
	}
	//===>Data Fine Stage
	frmDomandaStage.txtFinStage.value = TRIM(frmDomandaStage.txtFinStage.value)
	if (frmDomandaStage.txtFinStage.value == "") {
		alert('Indicare la data fine Stage')
		frmDomandaStage.txtFinStage.focus()
		return false
	}
	if (!ValidateInputDate(frmDomandaStage.txtFinStage.value)) {
		frmDomandaStage.txtFinStage.focus();
		return false;
	}
	/*if (!ValidateRangeDate(frmDomandaStage.txtOggi.value,frmDomandaStage.txtFinStage.value)) {
		alert('La data di fine Stage non pu� essere inferiore alla data odierna')
		frmDomandaStage.txtFinStage.focus()
		return false
	}*/
	if (!ValidateRangeDate(frmDomandaStage.txtIniStage.value,frmDomandaStage.txtFinStage.value)) {
		alert('La data fine Stage non pu� essere precedente alla data di inizio dello Stage')
		frmDomandaStage.txtFinStage.focus()
		return false
	}
	return true
}	
</script>

<%
'**********M A I N **********'
dim nIdRichStage
dim nIdSede, sDescSede, sDenomAP, sDescImpr, sProgetto, cMsgDomDir, cMsgDomIndir
dim nPostiAss, nPostiDisp, sDescStage, nIdAreaP, sDomDir
dim dDt_IStage, dDt_FStage, dDt_IVal, dDt_FVal, dDt_Tmst
dim rsSta, sSqlSta,nTipo,nIdFigura,sDenominazione

cMsgDomDir = "Lo Stage si svolger� presso una delle sedi operative dell'azienda"
cMsgDomIndir = "Lo Stage si svolger� presso una azienda esterna"
nIdRichStage = clng(Request.Form("nRichSta"))

set rsSta = server.CreateObject("ADODB.Recordset")
	sSqlSta =	"Select nvl(NUM_POSTI_ASS,0) as num_posti_ass, NUM_POSTI_DISP, DESC_STAGE," &_
				" ds.ID_SEDE,ds.ID_FIGPROF,ds.COD_TIPO_RICERCA, ds.ID_AREAPROF, FL_DOMDIRETTA, DT_INISTAGE, DT_FINSTAGE," &_
				" DT_INIVAL, DT_FIN_VAL, PROGETTO, ds.DT_TMST, si.DESCRIZIONE, ap.DENOMINAZIONE, i.RAG_SOC" &_
				" from Domanda_stage ds, Sede_Impresa si, Aree_professionali ap, Impresa i" &_
				" where ID_RICHSTAGE=" & nIdRichStage &_
				" and si.id_sede = ds.id_sede" &_
				" and si.id_impresa = i.id_impresa" &_
				" and ap.id_areaprof = ds.id_areaprof"
'PL-SQL * T-SQL  
SSQLSTA = TransformPLSQLToTSQL (SSQLSTA) 
rsSta.open sSqlSta,CC,3
	if not rsSta.EOF then
		nIdSede = clng(rsSta.Fields("ID_SEDE"))
		nPostiAss = clng(rsSta("NUM_POSTI_ASS"))
		nPostiDisp = clng(rsSta.Fields("NUM_POSTI_DISP"))
		nIdAreaP = clng(rsSta.Fields("ID_AREAPROF"))
		nIdFigura = clng(rsSta.Fields("ID_FIGPROF"))
		nTipo = clng(rsSta.Fields("COD_TIPO_RICERCA"))

		sDescStage = rsSta.Fields("DESC_STAGE")
		sDescImpr = rsSta.Fields("RAG_SOC")
		sDescSede = rsSta.Fields("DESCRIZIONE")
		sDenomAP = rsSta.Fields("DENOMINAZIONE")
		sDomDir = rsSta.Fields("FL_DOMDIRETTA")
		sProgetto = rsSta.Fields("PROGETTO")

		dDt_IStage = rsSta.Fields("DT_INISTAGE")
		dDt_FStage = rsSta.Fields("DT_FINSTAGE")
		'''dDt_IVal = rsSta.Fields("DT_INIVAL")
		dDt_IVal = ConvStringToDate(rsSta.Fields("DT_INIVAL"))
		dDt_FVal = rsSta.Fields("DT_FIN_VAL")
		dDt_Tmst = rsSta.Fields("DT_TMST")
	end if
rsSta.Close()
set rsSta=nothing

sql ="SELECT DENOMINAZIONE FROM FIGUREPROFESSIONALI WHERE ID_FIGPROF=" & nIdFigura

'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
SET rs = cc.execute(sql)

sDenominazione = rs("DENOMINAZIONE")

set rs = nothing

Inizio()
%>

<form name="Indietro" action="STA_RicStage.asp" method="post">
	<input type="hidden" name="txtSede" value="<%=nIdSede%>">
</form>	

<form method="post" name="FormFiltri" action="STA_VisCriteriSel.asp">
	<input type="hidden" name="dtTmst" value="<%=dDt_Tmst%>">
	<input type="hidden" name="idRic" value="<%=nIdRichStage%>">
	
	<table border="0" cellpadding="0" cellspacing="1" width="500">
		<tr>
			<td align="center">
				<a class="textred" href="javascript:FormFiltri.submit()" onmouseover="javascript: window.status= ' '; return true"><b>Caratteristiche Candidati</b></a>
			</td>
		</tr>
	</table>
</form>
<%
if isnull(dDt_FVal) then 
	LinkFiltri()
end if
%>
		
<form method="post" name="frmDomandaStage" onsubmit="return ControllaDati(this)" action="STA_CnfModRichStage.asp">
	<input type="hidden" name="txtIdRich" value="<%=nIdRichStage%>">
	<!--input type="hidden" name="txtOggi" value="<%'''=ConvDateToString(date())%>"-->
	<input type="hidden" name="txtOggi" value="<%=cdate(date())%>">
	<input type="hidden" name="txtTmst" value="<%=dDt_Tmst%>">
	<input type="hidden" name="nSede" value="<%=nIdSede%>">
	<input type="hidden" name="txtChiudi" value="NO">
	
	<table border="0" cellpadding="0" cellspacing="1" width="500">
	<tr height="20">
		<td align="left" colspan="2" nowrap class="tbltext1"><b>Busqueda Requerida</b> 
		</td>
		<td align="left" colspan="2" width="65%">
			<p class="textblack">
			<b><%=nIdRichStage%></b>
			</p>
		</td>
	</tr>
<%
	IF isnull(dDt_FVal) THEN %>	
		<tr height="20">
			<td align="left" colspan="2" nowrap class="tbltext1">
				<b>Denominazione Azienda</b>
			</td>
			<td align="left" colspan="2" width="65%">
				<p class="textblack">
					<b><%=sDescImpr%></b>&nbsp;-&nbsp;<%=sDescSede%>
				</p>
			</td>
		</tr>
		<tr>       
			<td align="left" colspan="2" nowrap class="tbltext1">
				<b>Descrizione Stage*</b><br>
				Disponibili <b><label name="NumCaratteri" id="NumCaratteri">150</label></b>
				caratteri 
			</td>
			<td align="left" colspan="2">
				<!--textarea name="txtDescStage" class="textblack" cols="58" rows="2"><%'=sDescStage%></textarea-->
				<textarea name="txtDescStage" onKeyup="JavaScript:CheckLenTextArea(document.frmDomandaStage.txtDescStage,NumCaratteri,150)" class="textblack" cols="58" rows="2"><%=sDescStage%></textarea>
            </td>
        </tr>
        <tr> 
			<td align="left" colspan="2" nowrap class="tbltext1" height="20">
				<b>Area Professionale</b>
			</td>
			<td colspan="2" align="left" height="20">
				<p class="textblack">
					<%=sDenomAP%>				
				</p>
			</td>						
		</tr>
		 <tr> 
			<td align="left" colspan="2" nowrap class="tbltext1" height="20">
				<b>Figura Professionale</b>
			</td>
			<td colspan="2" align="left" height="20">
				<p class="textblack">
					<%=sDenominazione%>				
				</p>
			</td>						
		</tr>
		 <tr> 
			<td align="left" colspan="2" nowrap class="tbltext1" height="20">
				<b>Tipo di Ricerca</b>
			</td>
			<td colspan="2" align="left" height="20">
				<p class="textblack"> 
					<%if nTipo = 0 then 
							Response.Write "PER AREA PROFESSIONALE"
					  else
							Response.Write "PER PROFILO PROFESSIONALE"
					  end if		%>				
				</p>
			</td>						
		</tr>
		<tr height="20"> 
			<td align="left" colspan="2" nowrap class="tbltext1">
				<b>Richiesta Stage</b>
			</td>
			<td colspan="2" align="left">
				<p class="textblack">
					<%
					If not isnull(sProgetto) then
						Response.Write "PRIVATA - DI PROGETTO"
					else
						Response.Write "PUBBLICA"
					end if
					%>				
				</p>
			</td>			
		</tr>	
        <tr>
			<td align="left" colspan="2" class="tbltext1">
				<b>Numero Candidati*</b>
			</td>
			<td align="left" colspan="2">
				<input type="hidden" name="hdnNumCand" value="<%=nPostiDisp%>">
				<input name="txtPostiDisp" class="textblack" type="text" size="10" maxlength="50" value="<%=nPostiDisp%>">
			</td>
        </tr>
        <tr height="20">
			<td align="left" colspan="2" class="tbltext1">
				<b>Posti Assegnati</b>
			</td>
			<td align="left" colspan="2">
				<input type="hidden" name="hdnPostiAss" value="<%=nPostiAss%>">
				<p class="textblack">
					<%=nPostiAss%>
				</p>
			</td>
        </tr>
        <tr>		
			<td align="left" colspan="2" nowrap class="tbltext1">
				<b>Richiesta valida dal*</b><br>&nbsp;(gg/mm/aaaa)
			</td>			
			<td align="left" colspan="2">
				<input name="txtIniVal" class="textblack" type="text" size="15" maxlength="10" value="<%=dDt_IVal%>">
			</td>
		</tr>
        <tr>		
			<td align="left" colspan="2" nowrap class="tbltext1">
				<b>Periodo del/al*</b><br>&nbsp;(dd/mm/aaaa)
			</td>			
			<td align="left" colspan="2">
				<input name="txtIniStage" class="textblack" type="text" size="15" maxlength="10" value="<%=dDt_IStage%>">&nbsp;/
				<input name="txtFinStage" class="textblack" type="text" size="15" maxlength="10" value="<%=dDt_FStage%>">
			</td>
		</tr>	
		<tr>
            <td align="left" colspan="4">&nbsp;</td>
		</tr>
		<input name="rDomDiretta" class="textblack" type="hidden" value="S">
		<!--		<tr>            <td align="left" colspan="4" nowrap class="tbltext1">				<input name="rDomDiretta" class="textblack" type="radio" value="S" <%'if sDomDir = "S" then Response.Write " checked"%>>				&nbsp;<b><%'=cMsgDomDir%>*</b>			</td>		</tr>		<tr>			<td align="left" colspan="4" class="tbltext1">				<input name="rDomDiretta" class="textblack" type="radio" value="N" <%'if sDomDir = "N" then Response.Write " checked"%>>				&nbsp;<b><%'=cMsgDomIndir%>*</b>			</td>		</tr> -->
		<tr>
            <td align="left" colspan="4">&nbsp;</td>
		</tr>
<%
	ELSE %>
		<tr height="20">
			<td align="left" colspan="2" nowrap class="tbltext1">
				<b>Nombre de la Empresa</b>
			</td>
			<td align="left" colspan="2" width="65%">
				<p class="textblack">
					<b><%=sDescImpr%></b>&nbsp;-&nbsp;<%=sDescSede%>
				</p>
			</td>
		</tr>
		<tr height="20">       
			<td align="left" colspan="2" nowrap class="tbltext1">
				<b>Descripci�n Fase</b>
			</td>
			<td align="left" colspan="2">
				<p class="textblack">
					<%=sDescStage%>
				</p>
            </td>
        </tr>
        <tr height="20"> 
			<td align="left" colspan="2" nowrap class="tbltext1">
				<b>Area Profesional</b>
			</td>
			<td colspan="2" align="left">
				<p class="textblack">
					<%=sDenomAP%>				
				</p>
			</td>			
		</tr>
		 <tr height="20"> 
			<td align="left" colspan="2" nowrap class="tbltext1">
				<b>Figura Profesional</b>
			</td>
			<td colspan="2" align="left">
				<p class="textblack">
					<%=sDenominazione%>				
				</p>
			</td>						
		</tr>
		 <tr height="20"> 
			<td align="left" colspan="2" nowrap class="tbltext1">
				<b>Tipo de Busqueda</b>
			</td>
			<td colspan="2" align="left">
				<p class="textblack">
					<%if nTipo = 0 then 
							Response.Write "PER AREA PROFESSIONALE"
					  else
							Response.Write "PER PROFILO PROFESSIONALE"
					  end if		%>				
				</p>
			</td>						
		</tr>
		<tr height="20"> 
			<td align="left" colspan="2" nowrap class="tbltext1">
				<b>Tipologia de Etapa</b>
			</td>
			<td colspan="2" align="left">
				<p class="textblack">
					<%
					If not isnull(sProgetto) then
						Response.Write "PRIVATO - DI PROGETTO"
					else
						Response.Write "PUBBLICO"
					end if
					%>				
				</p>
			</td>			
		</tr>	
        <tr height="20">
			<td align="left" colspan="2" class="tbltext1">
				<b>Lugar Disponible</b>
			</td>
			<td align="left" colspan="2">
				<p class="textblack">
					<%=nPostiDisp%>
				</p>
			</td>
        </tr>
        <tr height="20">
			<td align="left" colspan="2" class="tbltext1">
				<b>Lugar Asignado</b>
			</td>
			<td align="left" colspan="2">
				<p class="textblack">
					<%=nPostiAss%>
				</p>			
			</td>
        </tr>
        <tr height="20">
            <td align="center" class="tbltext1" colspan="4">&nbsp;</td>
		</tr>
        <tr height="20">		
			<td align="left" colspan="2" nowrap class="tbltext1">
				<b>Busqueda Valida del/al</b>
			</td>			
			<td align="left" colspan="2">
				<p class="textblack">
					<%=dDt_IVal%>
					<b> - </b>
					<%=dDt_FVal%>
				</p>
			</td>
		</tr>
		<!--tr height="20">					<td align="left" colspan="2" nowrap class="tbltext1">				<b> Al </b>			</td>						<td align="left" colspan="2">				<p class="textblack">					<%=dDt_FVal%>				</p>			</td>		</tr-->
        <tr height="20">		
			<td align="left" colspan="2" nowrap class="tbltext1">
				<b>Periodo Etapa del/al</b>
			</td>			
			<td align="left" colspan="2">
				<p class="textblack">
					<%=dDt_IStage%><b> - </b>
					<%=dDt_FStage%>
				</p>
			</td>
		</tr>	
		<!--tr height="20">					<td align="left" colspan="2" nowrap class="tbltext1">				<b>Data Fine Stage</b>			</td>						<td align="left" colspan="2">				<p class="textblack">					<%=dDt_FStage%>				</p>			</td>		</tr-->
		<tr height="20">
            <td align="left" colspan="4">&nbsp;</td>
		</tr>
		<tr height="20">
            <td align="left" colspan="4">
				<p class="tbltext1">
<%
					if sDomDir = "S" then 
						Response.Write "<b>"& cMsgDomDir &"</b>"
					else
						Response.Write "<b>"& cMsgDomIndir &"</b>"
					end if %>
				</p>
			</td>
		</tr>
		<tr height="20">
            <td align="left" colspan="4">&nbsp;</td>
		</tr>
<%
	END IF%>
	</table>
	<!--Inizio -->
	<table width="371" cellpadding="1" cellspacing="2" border="0">
		<tr> 
			<td align="center"> 
				<a class="textRed" href="javascript: FrmVisualizza.submit();">
					<b>Visualiza postulantes</b>
				</a>
			</td>
		</tr>
		<tr>
			<td>&nbsp;
			</td>
		</tr>
	</table>
	<!--Fine -->
	<table cellpadding="0" cellspacing="0" width="500" border="0">
		<tr>
<%
		IF isnull(dDt_FVal) THEN %>	
			<td align="right" width="33%">
				<a href="javascript:Indietro.submit()">
					<img src="<%=Session("progetto")%>/images/indietro.gif" border="0" alt="Torna alla pagina precedente">
				</a>
			</td>
			<td align="center" width="33%">
				<input type="image" name="Invia" src="<%=Session("progetto")%>/images/conferma.gif" value="Registra" alt="Conferma la registrazione dei dati">
			</td>
			<td align="left" width="33%">
		        <a href="javascript:ChiudiVal()" id="FineVal"><img src="<%=session("Progetto")%>/images/chiudivalidita.gif" value="chiudi" border="0" alt="Chiude la validit� della proposta di stage"></a>
			</td>
<%
		ELSE%>
			<td align="center">
				<a href="javascript:Indietro.submit()">
					<img src="<%=Session("progetto")%>/images/indietro.gif" border="0" alt="Torna alla pagina precedente">
				</a>
			</td>
<%
		END IF%>
		</tr>
	</table>
</form>

<!--Inizio '04-->
<form method="post" name="FrmVisualizza" action="STA_VisCandidati.asp">
	<input type="hidden" name="hdnIdSede" id="hdnIdSede" value="<%=nIdSede%>">
	<input type="hidden" name="IdRic" id="IdRic" value="<%=nIdRichStage%>">
	<input type="hidden" name="hdnDescStage" id="hdnDescStage" value="<%=sDescStage%>">
	<input type="hidden" name="hdnAreaProf" id="hdnAreaProf" value="<%=nIdAreaP%>">
</form>
<!--Fine '04-->

<%'****************************'
Sub LinkFiltri()%>
	<table width="500" align="center" cellspacing="1" cellpadding="1" border="0">
		<tr height="20">
			<form method="post" name="FrmC1" action="STA_InsCriteriSel.asp">
				<input type="hidden" name="idTab" value="PERS_CONOSC">
				<input type="hidden" name="Area" value="AREA_CONOSCENZA">
				<input type="hidden" name="Specifico" value="CONOSCENZE">
				<input type="hidden" name="Condizione" value="COD_GRADO_CONOSC">
				<input type="hidden" name="IdRic" value="<%=nIdRichStage%>">
				<input type="hidden" name="dtTmst" value="<%=dDt_Tmst%>">
				<input type="hidden" name="Idsede" value="<%=nIdSede%>">
			</form>				
			<td valign="middle">
				<a href="javascript:document.FrmC1.submit();" ONMOUSEOVER="window.status = ' '; return true"><u class="tbltext1"><b>&nbsp;Conocimiento&nbsp;</b></u></a>
			</td>
			
			<form method="post" name="FrmC2" action="STA_InsCriteriSel.asp">
				<input type="hidden" name="idTab" value="TISTUD">
				<input type="hidden" name="Area" value="LSTUD">
				<input type="hidden" name="Specifico" value="TSTUD">
				<input type="hidden" name="Condizione" value="VOTO_STUD">
				<input type="hidden" name="IdRic" value="<%=nIdRichStage%>">
				<input type="hidden" name="dtTmst" value="<%=dDt_Tmst%>">
				<input type="hidden" name="Idsede" value="<%=nIdSede%>">
			</form>									
			<td width="180" valign="middle">
				<a href="javascript:document.FrmC2.submit();" ONMOUSEOVER="window.status = ' '; return true"><u class="tbltext1"><b>&nbsp;Nivel Educativo&nbsp;</b></u></a>
			</td>
			
			<form method="post" name="FrmC3" action="STA_InsCriteriSel.asp">
				<input type="hidden" name="idTab" value="PERS_DISPON">
				<input type="hidden" name="Area" value="CL_VD">
				<input type="hidden" name="Specifico" value="TDISP">
				<input type="hidden" name="Condizione" value>
				<input type="hidden" name="IdRic" value="<%=nIdRichStage%>">
				<input type="hidden" name="dtTmst" value="<%=dDt_Tmst%>">
				<input type="hidden" name="Idsede" value="<%=nIdSede%>">
			</form>								
			<td valign="middle">
				<a href="javascript:document.FrmC3.submit();" ONMOUSEOVER="window.status = ' '; return true"><u class="tbltext1"><b>&nbsp;Disponibilidad&nbsp;</b></u></a>
			</td>
			
			<form method="post" name="FrmC4" action="STA_InsCriteriSel.asp">
				<input type="hidden" name="idTab" value="SEL_TAB">
				<input type="hidden" name="Area" value="DIZ_TAB">
				<input type="hidden" name="Specifico" value="DIZ_DATI">
				<input type="hidden" name="Condizione" value>
				<input type="hidden" name="IdRic" value="<%=nIdRichStage%>">
				<input type="hidden" name="dtTmst" value="<%=dDt_Tmst%>">
				<input type="hidden" name="Idsede" value="<%=nIdSede%>">
			</form>
			<td valign="middle">
				<a href="javascript:document.FrmC4.submit();" ONMOUSEOVER="window.status = ' '; return true"><u class="tbltext1"><b>&nbsp;Varios&nbsp;</b></u></a>
			</td>
		</tr>
		<tr height="20">
			<form method="post" name="FrmC5" action="STA_InsCriteriSel.asp">
				<input type="hidden" name="idTab" value="PERS_CAPAC">
				<input type="hidden" name="Area" value="AREA_CAPACITA">
				<input type="hidden" name="Specifico" value="CAPACITA">
				<input type="hidden" name="Condizione" value="GRADO_CAPACITA">
				<input type="hidden" name="IdRic" value="<%=nIdRichStage%>">
				<input type="hidden" name="dtTmst" value="<%=dDt_Tmst%>">
				<input type="hidden" name="Idsede" value="<%=nIdSede%>">
			</form>
			<td valign="middle">
				<a href="javascript:document.FrmC5.submit();" ONMOUSEOVER="window.status = ' '; return true"><u class="tbltext1"><b>&nbsp;Capacidad&nbsp;</b></u></a>
			</td>
			
			<form method="post" name="FrmC6" action="STA_InsCriteriSel.asp">
				<input type="hidden" name="idTab" value="ESPRO">
				<input type="hidden" name="Area" value="DIZ_TAB">
				<input type="hidden" name="Specifico" value="DIZ_DATI">
				<input type="hidden" name="Condizione" value>
				<input type="hidden" name="IdRic" value="<%=nIdRichStage%>">
				<input type="hidden" name="dtTmst" value="<%=dDt_Tmst%>">
				<input type="hidden" name="Idsede" value="<%=nIdSede%>">
			</form>	
			<td valign="middle">
				<a href="javascript:document.FrmC6.submit();" ONMOUSEOVER="window.status = ' '; return true"><u class="tbltext1"><b>&nbsp;Experiencia Profesional&nbsp;</b></u></a>
			</td>
			
			<form method="post" name="FrmC7" action="STA_InsCriteriSel.asp">
				<input type="hidden" name="idTab" value="PERS_VINCOLI">
				<input type="hidden" name="Area" value="CL_VD">
				<input type="hidden" name="Specifico" value="TVINC">
				<input type="hidden" name="Condizione" value>
				<input type="hidden" name="IdRic" value="<%=nIdRichStage%>">
				<input type="hidden" name="dtTmst" value="<%=dDt_Tmst%>">
				<input type="hidden" name="Idsede" value="<%=nIdSede%>">
			</form>
			<td valign="middle">
				<a href="javascript:document.FrmC7.submit();" ONMOUSEOVER="window.status = ' '; return true"><u class="tbltext1"><b>&nbsp;Vinculo&nbsp;</b></u></a>
			</td>
		</tr>
		<tr height="20">
			<form method="post" name="FrmC8" action="STA_InsCriteriSel.asp">
				<input type="hidden" name="idTab" value="PERS_COMPOR">
				<input type="hidden" name="Area" value="AREA_COMPORTAMENTI">
				<input type="hidden" name="Specifico" value="COMPORTAMENTI">
				<input type="hidden" name="Condizione" value="GRADO_PERS_COMPOR">
				<input type="hidden" name="IdRic" value="<%=nIdRichStage%>">
				<input type="hidden" name="dtTmst" value="<%=dDt_Tmst%>">
				<input type="hidden" name="Idsede" value="<%=nIdSede%>">
			</form>
			<td valign="middle" colspan="4">
				<a href="javascript:document.FrmC8.submit();" ONMOUSEOVER="window.status = ' '; return true"><u class="tbltext1"><b>&nbsp;Comportamiento&nbsp;</b></u></a>
			</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
		</tr>
		<tr height="2">
			<td class="sfondomenu" colspan="4" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
		</tr>
	</table>
<%
End Sub
'****************************'

Sub Inizio()%>
<table border="0" width="520" height="81" cellspacing="0" cellpadding="0"> 
	<tr>
		<td width="500" height="81" background="<%=Session("Progetto")%>/images/titoli/strumenti2b.gif" valign="bottom" align="right">
			<table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
				<tr>
					<td width="100%" valign="top" align="right">
						<b class="tbltext1a">Definizione Stage</b>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<br>
<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
	<tr height="18">
		<td class="sfondomenu" width="67%" height="18">
			<span class="tbltext0">
				<b>&nbsp;MODIFICA BUSQUEDA DE ETAPAS</b>
			</span>
		</td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
		<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"><%if isnull(dDt_FVal) then Response.Write "(*) campi obbligatori" %></td>
	</tr>
</table>
<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
	<tr>
		<td align="left" class="sfondocomm">
			Es posible modificar el dato de la busqueda por etapa, confirmando la modificaci�n y presionando <b>Enviar</b>.<br>
			El campo Area Profesional, Busqueda por Etapas y Lugar Asignado no pueden ser modificados.<br>
			Si presiona <b>Cancelar Validaci�n</b> cancela la validez de la busqueda al d�a de la fecha.<br>
			En el caso que la busqueda ya no sea valida, los datos presentes seran de solo lectura.<br>
			El vinculo <b>Postulantes caracteristicosi</b> visualiza las caracteristicas de la busqueda de los postulantes, 
			mientras que los vinvulos inferiores permiten ingresar estas caracteristicas.
			<a href="Javascript:Show_Help('/Pgm/help/GestProgetti/Stage/Stage_Definizione/STA_ModRichStage')">
			<img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
		</td>
	</tr>
	<tr height="2">
		<td class="sfondocomm" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif">
		</td>
	</tr>
</table>
<br>
<%
End sub
'****************************'%>


<!--#include virtual="/strutt_coda2.asp"-->
<!--#include virtual="/include/closeconn.asp"-->
