<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include Virtual = "/strutt_testa2.asp"-->
<!--#include Virtual = "/include/SysFunction.asp"-->
<!--#include Virtual = "/include/openconn.asp"-->
<!--#include virtual = "/include/DecCod.asp"-->
<!--#include virtual = "/util/portallib.asp"-->

<%
if ValidateService(session("idutente"),"STA_VISIMPRESE",cc) <> "true" then 
	response.redirect "/util/error_login.asp"
end if
%>

<script LANGUAGE="Javascript">
	<!--#include virtual = "/Include/help.inc"-->
</script>

<%
'*********** MAIN ***************
Dim nIdRic, nSede
dim Rs, sSql, rsCriteri, rsDecod
dim sRagSoc, sDenomSede, sDenomArea, sStage
DIM aOpLog, sDescrTab, memId,nTipo
DIM sDt_Tmst, dChiusaVal,nIdFigura,sDenominazione


sDt_tmst	= Request("dtTmst")
nIdRic		= Request("IdRic")

sSql =	"Select DESC_STAGE, ds.DT_FIN_VAL, ds.ID_SEDE, ds.ID_AREAPROF, ds.DT_TMST," &_
        "si.DESCRIZIONE, ap.DENOMINAZIONE, i.RAG_SOC,DS.ID_FIGPROF,DS.COD_TIPO_RICERCA" &_
		" from Domanda_stage ds, Sede_Impresa si, Aree_professionali ap, Impresa i" &_
		" where ID_RICHSTAGE=" & nIdRic &_
		" and si.id_sede = ds.id_sede" &_
		" and si.id_impresa = i.id_impresa" &_
		" and ap.id_areaprof = ds.id_areaprof"
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
set Rs = CC.Execute(sSql)
	if not Rs.eof then
		sRagSoc = Rs("rag_soc")
		sDenomSede = Rs("descrizione")
		sDenomArea = Rs("denominazione")
		sStage = Rs("DESC_STAGE")
		nSede = Rs("ID_SEDE")
		dChiusaVal = Rs("DT_FIN_VAL")
		nIdFigura = clng(rs("ID_FIGPROF"))
		nTipo = clng(rs("COD_TIPO_RICERCA"))            		           
	end if
	
rs.close()
set rs=nothing

sql ="SELECT DENOMINAZIONE FROM FIGUREPROFESSIONALI WHERE ID_FIGPROF=" & nIdFigura

'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
SET rs1 = cc.execute(sql)
sDenominazione = rs1("DENOMINAZIONE")
set rs1 = nothing

Inizio()
Imposta_pag()

'**********************************************************************************************************************************************************************************
Sub Imposta_Pag()
%>
	<table border="0" cellpadding="0" cellspacing="1" width="500">
		<tr height="20">
			<td align="left" colspan="2" nowrap class="tbltext1">
				<b>Riferimento Richiesta</b>
			</td>
			<td align="left" colspan="2" width="65%">
				<p class="textblack">
					<b><%=nIdRic%></b>
				</p>
			</td>
		</tr>
		<tr height="20">
			<td align="left" colspan="2" nowrap class="tbltext1">
				<b>Denominazione Azienda</b>
			</td>
			<td align="left" colspan="2" width="65%">
				<p class="textblack">
					<b><%=sRagSoc%></b>&nbsp;-&nbsp;<%=sDenomSede%>
				</p>
			</td>
		</tr>
		<tr height="20"> 
			<td align="left" colspan="2" nowrap class="tbltext1">
				<b>Descrizione Stage</b>
			</td>
			<td colspan="2" align="left">
				<p class="textblack">
					<%=sStage%>				
				</p>
			</td>			
		</tr>
		<tr height="20"> 
			<td align="left" colspan="2" nowrap class="tbltext1">
				<b>Area Professionale</b>
			</td>
			<td colspan="2" align="left">
				<p class="textblack">
					<%=sDenomArea%>				
				</p>
			</td>			
		</tr>
		<tr> 
			<td align="left" colspan="2" nowrap class="tbltext1" height="20">
				<b>Figura Professionale</b>
			</td>
			<td colspan="2" align="left" height="20">
				<p class="textblack">
					<%=sDenominazione%>				
				</p>
			</td>						
		</tr>
		 <tr> 
			<td align="left" colspan="2" nowrap class="tbltext1" height="20">
				<b>Tipo di Ricerca</b>
			</td>
			<td colspan="2" align="left" height="20">
				<p class="textblack"> 
					<%if nTipo = 0 then 
							Response.Write "PER AREA PROFESSIONALE"
					  else
							Response.Write "PER PROFILO PROFESSIONALE"
					  end if		%>				
				</p>
			</td>						
		</tr>
	</table>
	<br>
	
	<table border="0" cellspacing="1" cellpadding="1" width="500" align="center">
<%
	sSQL = "SELECT a.ID_TAB,a.ID_CAMPO,a.COD_OPLOG,a.VALORE,a.PRG_REGOLA,a.PRG_REGOLA_RIF, b.DESC_TAB" &_
			" FROM REGOLE_STAGE a, DIZ_TAB b" &_ 
			" WHERE ID_RICHSTAGE=" & nIdRic &_
			" AND a.ID_TAB = b.ID_TAB" &_
			" ORDER BY ID_TAB,PRG_REGOLA,PRG_REGOLA_RIF"
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	set rsCriteri = CC.Execute(sSQL)
	
	if not rsCriteri.EOF then
		z = 0
		aOpLog = decodTadesToArray("OPLOG",date,"",1,0)
		sDescrTab= rsCriteri("DESC_TAB")
		memId =  rsCriteri.Fields("ID_TAB")
%>		<tr>
			<td colspan="3">
				<table border="0" CELLPADDING="0" CELLSPACING="0" width="500" align="center">
					<tr height="18">
						<td class="sfondomenu" width="350" height="18"><span class="tbltext0"><b>&nbsp;<%=sDescrTab%></b></span></td>
						<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
						<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
					</tr>
				</table>
			</td>
		</tr>
<%		
		DO while not rsCriteri.EOF
			if 	rsCriteri.Fields("ID_TAB") <> memId then
				sDescrTab= rsCriteri("DESC_TAB")
				memId =  rsCriteri.Fields("ID_TAB")
%>		<tr>
			<td colspan="3">
				<table border="0" CELLPADDING="0" CELLSPACING="0" width="500" align="center">
					<tr height="18">
						<td class="sfondomenu" width="350" height="18"><span class="tbltext0"><b>&nbsp;<%=sDescrTab%></b></span></td>
						<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
						<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
					</tr>
				</table>
			</td>
		</tr>
<%
			end if %>
		<tr class="tblsfondo"> 
			<td class="textblack" align="left" valign="center" width="140">
<%
			sSQL =	"SELECT ID_DESC,ID_CAMPO,DESC_CAMPO,ID_CAMPO_RIF,ID_TAB_RIF,TIPO_CAMPO,ID_CAMPO_DESC" &_ 
					" FROM DIZ_DATI" &_ 
					" WHERE ID_TAB='" & rsCriteri("ID_TAB")	& "'" &_ 
					" AND ID_CAMPO='" & rsCriteri("ID_CAMPO") & "'" 
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
			set rsDecod = CC.Execute(sSQL)

				Response.Write ucase(rsDecod("DESC_CAMPO"))
				sDecNomTab		= rsDecod("ID_TAB_RIF")
				sTipoCampo		= rsDecod("Tipo_campo")
				sDescNomeCampo  = rsDecod("ID_CAMPO_DESC")
				sDenominazione  = rsDecod("ID_DESC")
				if isnull(rsDecod("ID_CAMPO_rif")) then
					sDecNomCampo = rsDecod("ID_CAMPO")
				else
					sDecNomCampo = rsDecod("ID_CAMPO_RIF")
				end if
			rsDecod.Close
			set rsDecod = Nothing %>
			</td>
			<td class="textblack" align="left" valign="center" width="80">
				<%=(UCASE(searchIntoArray(rsCriteri("COD_OPLOG"),aOpLog,0)))%>
			</td>
			<td class="textblack" align="left" valign="center" width="220">
<% 
			if sDecNomTab <> "" then
				select case mid(sDecNomTab,1,4)
					case "TAB_"
						aValore = Split(rsCriteri("Valore"),",",-1,1)
						lung = Ubound(aValore)
						sTab = ""
						for i = 0 to lung 
							sTab = sTab & ucase(DecCodVal(mid(sDecNomTab,5), 0, date, replace(aValore(i),"$",""),0)) & ","
						next
						Response.Write left(sTab,len(sTab) - 1)
					case "COMU"
						aValore = Split(rsCriteri("Valore"),",",-1,1)
						lung = Ubound(aValore)
						sCom=""
						if lung > 0 then 
							for i = 0 to lung 
								sSQL =	"SELECT DESCOM FROM " & sDecNomTab & " WHERE " &_
										sDecNomCampo & " = " & Replace(aValore(i),"$","'")
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
								set rsDecod = CC.Execute(sSQL)
									sCom = sCom  &  ucase(rsDecod("DESCOM")) & ","
								rsDecod.Close
								set rsDecod = Nothing
							next
							Response.Write left(sCom,len(sCom) - 1)
						else
							sSQL =	"SELECT DESCOM FROM " & sDecNomTab & " WHERE " &_
									sDecNomCampo & " ='" & aValore(0) & "'"
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
							set rsDecod = CC.Execute(sSQL)
								Response.write ucase(rsDecod("DESCOM")) 
							rsDecod.Close
							set rsDecod = Nothing
						end if
					case else
						aValore = Split(rsCriteri("Valore"),",",-1,1)
						lung = Ubound(aValore)
						Denom = ""
						for i = 0 to lung 
							sSQL =	"SELECT " & sDenominazione & " FROM " & sDecNomTab & " WHERE " &_
									sDecNomCampo & " = " & Trasforma(replace(aValore(i),"$",""),sTipoCampo)
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
							set rsDecod = CC.Execute(sSQL)
								Denom = Denom & ucase(rsDecod(0)) & ","
							rsDecod.Close
							set rsDecod = Nothing
						next
						Response.Write left(Denom,len(Denom) - 1)
				end select
				
			else
				if sDescNomeCampo <> "" then
					aVal = split(sDescNomeCampo,"|")
					for i=0 to ubound(aVal) step 2
						if aVal(i) = replace(rsCriteri("VALORE"),"$","") then
							Response.Write server.HTMLEncode(UCASE(aVal(i+1)))
						end if
					next
				else
					Response.Write server.HTMLEncode(replace(rsCriteri("VALORE"),"$",""))
				end if
			end if %>
			</td>
		</tr>
<%
		rsCriteri.MoveNext
		z=z+1
		LOOP
		
		Erase aOpLog
		Response.Write "</table>"
		'''Indietro1()
		
		if dChiusaVal <> "" or dChiusaVal <> " " then
			Indietro()
		else
			Indietro1()	
		end if

	Else %>
		<tr height="17"> 
			<td colspan="4" class="tbltext3" align="middle" valign="center">
				<br><b>Nessuna caratteristica impostata.</b>
			</td>
		</tr>
	</table>
<%		Indietro()
	End if
	
rsCriteri.close
set rsCriteri = nothing		
			
End Sub 
'**********************************************************************************************************************************************************************************
Sub Inizio()%>
	<table width="520px" border="0" cellspacing="0" cellpadding="0" height="70" align="center">
		<tr>
			<td width="500" background="<%=session("progetto")%>/images/titoli/strumenti2b.gif" valign="bottom" align="right">
				<table border="0" background width="500" height="23" cellspacing="0" cellpadding="0">
					<tr>
						<td width="100%" valign="top" align="right"><b CLASS="tbltext1a">Definizione Stage</b></td>
					</tr>
				</table>
			</td>
		</tr>
	</table> 
	<br>
	<table border="0" CELLPADDING="0" CELLSPACING="0" width="500" align="center">
		<tr height="18">
			<td class="sfondomenu" width="67%" height="18"><span class="tbltext0"><b>&nbsp;ELENCO CARATTERISTICHE CANDIDATO</b></span></td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
			<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
		</tr>
	</table>
	<!-- Commento -->
	<table border="0" CELLPADDING="0" CELLSPACING="0" width="500" align="center">
		<tr>
			<td align="left" class="sfondocomm">
				La pagina visualizza l'elenco delle caratteristiche impostate per la selezione dei candidati idonei allo stage.  
				Premere il tasto <b>Elimina</b> per cancellare tutte le caratteristiche inserite per questo stage.<br>
				Premere sul tasto <b>Indietro</b> per tornare alla pagina precedente.
				<a href="Javascript:Show_Help('/Pgm/help/GestProgetti/Stage/Stage_Definizione/STA_VisCriteriSel')"><img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
			</td>
		</tr>
		<tr height="2">
			<td class="sfondocomm" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
		</tr>
	</table>
	<br>
<%
End Sub
'**********************************************************************************************************************************************************************************
Sub Indietro1()%>
	<br>
	<table border="0" CELLPADDING="0" CELLSPACING="0" width="500" align="center">
		<tr>
			<td align="center"> 
				<form name="frmElimina" method="post" action="STA_DelCriteriSel.asp" onsubmit="return confirm('Sei sicuro di voler eliminare tutti \ni filtri inseriti per questo stage?')">
					<input type="hidden" name="IdRic" value="<%=nIdRic%>">
					<input type="hidden" name="IdSede" value="<%=nSede%>">
					<input type="image" title="Elimina tutti i filtri inseriti" src="<%=Session("Progetto")%>/images/Elimina.gif" id="image2" name="image2">
				</form>
			</td>
			<td align="center"> 
				<form name="frmIndietro1" method="post" action="STA_ModRichStage.asp">
					<input type="hidden" name="nRichSta" value="<%=nIdRic%>">
					<input type="image" title="Indietro" src="<%=Session("Progetto")%>/images/indietro.gif" id="image1" name="image1">
				</form>
			</td>
		</tr>
	</table>	
<%End Sub
'**********************************************************************************************************************************************************************************
Sub Indietro()%>
	<br>
	<form name="frmIndietro" method="post" action="STA_ModRichStage.asp">
	<table border="0" CELLPADDING="0" CELLSPACING="0" width="500" align="center">
		<tr>
			<td align="center"> 
				<input type="hidden" name="nRichSta" value="<%=nIdRic%>">
				<input type="image" title="Indietro" src="<%=Session("Progetto")%>/images/indietro.gif" id="image1" name="image1">
			</td>
		</tr>
	</table>
	</form>
<%End Sub
'**********************************************************************************************************************************************************************************	
Function Trasforma(valore,tipo)
	select case tipo
		case "D"
			Trasforma = ConvDateToDb(valore)
		case "A"
			Trasforma = "'" & Valore & "'"
		case else
			Trasforma = valore
	end select
End Function 
%>
<!--#include Virtual ="/Include/closeconn.asp"-->
<!--#include Virtual="/strutt_coda2.asp"-->		
