<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #INCLUDE VIRTUAL="strutt_testa2.asp" -->
<!--#include virtual = "/include/OpenConn.asp"-->
<!-- #INCLUDE Virtual="/pgm/Bilanciocompetenze/Utils.asp" -->

<%
if ValidateService(session("idutente"),"STA_BILCOMP",cc) <> "true" then 
	response.redirect "/util/error_login.asp"
end if
%>

<script language="Javascript">
	function addElement() {
		Salva.action = "/Pgm/BilancioCompetenze/Competenze/Elementi_USR/AddElemento.asp"
		Salva.VISMENU.value = ""
		Salva.Ini.value = "<%=Request("Ini")%>"
		Salva.Limit.value = "<%=Request("Limit")%>"
		Salva.submit()
		return true;
	}
	
	function PopolaPulsante(stringa) {
		document.Salva.Pulsante.value = stringa
		document.Salva.submit();
	}
</script>

<%
Dim Tipo, Tipo2
Dim sCritSel, CriRic
Dim nAree_Professionali, nArea_Conoscenze, nArea_Capacita, nArea_Comportamenti, nIdRich
Dim sql, Rs
Dim MM, VV


'Preleva l'Id Stage:
nIdRich = Request.Form("IdRich")

Tipo= request("Tipo")
Tipo2= request("Radio")
nAree_Professionali = Request("CmbAreaProf")

sCritSel = Request.Form("rCriterio")
If sCritSel = "" and Tipo <> "" then
	sCritSel = Tipo
End if

Select case sCritSel
	case "Conoscenze"
		nArea_Conoscenze = Request.Form("CmbConosco")
	case "Capacita"
		nArea_Capacita = Request.Form("CmbCapacita")
	case "Comportamenti"
		nArea_Comportamenti = Request.Form("CmbComportamenti")
End Select

'CONOSCENZE
IF sCritSel = "Conoscenze" THEN
	Sql = ""
	Sql =	"SELECT ID_CONOSCENZA, COD_GRADO_CONOSC" &_
			" FROM PERS_CONOSC" &_
			" WHERE ID_PERSONA = " & IDP
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
	Set Rs = CC.Execute (Sql)
		If not rs.eof then 
			MM = Rs.getrows()
		End if
	Set Rs = Nothing
	Tipo = sCritSel
	
	If nAree_Professionali <> "" Then  
		Sql = ""
		Sql =	"SELECT DISTINCT" &_
				" CON.ID_CONOSCENZA, CON.DENOMINAZIONE, ARE.DENOMINAZIONE" &_
				" FROM" &_
				" AREE_PROFESSIONALI ARE," &_
				" FIGUREPROFESSIONALI FIG," &_
				" CONOSCENZE CON, VALIDAZIONE, AREA_CONOSCENZA," &_
				" COMPET_CONOSC COM_CON," &_
				" COMPET_FP COM_FP" &_
				" WHERE" &_
				" ((CON.ID_AREACONOSCENZA = AREA_CONOSCENZA.ID_AREACONOSCENZA) AND" &_
				" (CON.ID_VALID = VALIDAZIONE.ID_VALID)) AND" &_
				" ((VALIDAZIONE.FL_VALID = 1) OR (VALIDAZIONE.ID_PERS_LAV = " & IDP & "))" &_
				" AND (ARE.ID_AREAPROF=FIG.ID_AREAPROF) AND" &_
				" (FIG.ID_FIGPROF=COM_FP.ID_FIGPROF) AND" &_
				" (COM_CON.ID_CONOSCENZA=CON.ID_CONOSCENZA) AND" &_
				" (COM_FP.ID_COMPETENZA=COM_CON.ID_COMPETENZA) AND" &_
				"  FIG.ID_AREAPROF = " & nAree_Professionali
		if nArea_Conoscenze <> "" then
			Sql = Sql & " AND (CON.ID_AREACONOSCENZA = " & nArea_Conoscenze & ")"
		end if
		Sql = Sql &  " ORDER BY UPPER(CON.DENOMINAZIONE)"

	Else
		Sql = ""
		Sql =	"SELECT CONOSCENZE.ID_CONOSCENZA, CONOSCENZE.DENOMINAZIONE, AREA_CONOSCENZA.DENOMINAZIONE" &_
				" FROM" &_
				" CONOSCENZE, AREA_CONOSCENZA, VALIDAZIONE" &_
				" WHERE" &_
				" ((CONOSCENZE.ID_AREACONOSCENZA = AREA_CONOSCENZA.ID_AREACONOSCENZA) AND" &_
				" (CONOSCENZE.ID_VALID = VALIDAZIONE.ID_VALID)) AND" &_
				" ((VALIDAZIONE.FL_VALID = 1) OR (VALIDAZIONE.ID_PERS_LAV = " & IDP & "))"
		if nArea_Conoscenze <> "" then
			Sql = Sql & " AND (CONOSCENZE.ID_AREACONOSCENZA = " & nArea_Conoscenze & ")"
		end if
		Sql = Sql & " ORDER BY CONOSCENZE.DENOMINAZIONE"
		
	End if
END IF

'CAPACITA
IF sCritSel = "Capacita" THEN
	Sql = ""
	Sql =	"SELECT ID_CAPACITA, GRADO_CAPACITA" &_
			" FROM PERS_CAPAC" &_
			" WHERE ID_PERSONA = " & IDP
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
	Set Rs = CC.Execute (Sql)
		if not rs.eof then 
			MM = Rs.getrows()
		end if
	Set Rs = Nothing
	Tipo = sCritSel
	
	If nAree_Professionali <> "" Then
		Sql = ""
		Sql =	"SELECT DISTINCT" &_
				" CAPACITA.ID_CAPACITA,CAPACITA.DENOMINAZIONE, ARE.DENOMINAZIONE" &_
				" FROM" &_
				" AREE_PROFESSIONALI ARE," &_
				" CAPACITA CAPACITA," &_
				" COMPET_FP COMP," &_
				" COMPET_CAPAC COM," &_
				" FIGUREPROFESSIONALI FIG, AREA_CAPACITA, VALIDAZIONE" &_
				" WHERE" &_
				" ((CAPACITA.ID_AREACAPACITA = AREA_CAPACITA.ID_AREACAPACITA) AND" &_
				" (CAPACITA.ID_VALID = VALIDAZIONE.ID_VALID)) AND" &_
				" ((VALIDAZIONE.FL_VALID = 1) OR (VALIDAZIONE.ID_PERS_LAV = " & IDP & ")) AND" &_
				" ( ARE.ID_AREAPROF=FIG.ID_AREAPROF ) AND" &_
				" ( COM.ID_COMPETENZA=COMP.ID_COMPETENZA ) AND" &_
				" ( COM.ID_CAPACITA=CAPACITA.ID_CAPACITA ) AND" &_
				" ( FIG.ID_FIGPROF=COMP.ID_FIGPROF ) AND" &_
				" FIG.ID_AREAPROF = " & nAree_Professionali
		if nArea_Capacita <> "" then
			Sql = Sql & " AND (CAPACITA.ID_AREACAPACITA = " & nArea_Capacita & ")"
		end if
		Sql = Sql &  " ORDER BY UPPER(CAPACITA.DENOMINAZIONE)"
	
	Else	
		Sql = ""
		Sql =	"SELECT CAPACITA.ID_CAPACITA, CAPACITA.DENOMINAZIONE, AREA_CAPACITA.DENOMINAZIONE" &_
				" FROM" &_
				" CAPACITA, AREA_CAPACITA, VALIDAZIONE" &_
				" WHERE" &_
				" ((CAPACITA.ID_AREACAPACITA = AREA_CAPACITA.ID_AREACAPACITA) AND" &_
				" (CAPACITA.ID_VALID = VALIDAZIONE.ID_VALID)) AND" &_
				" ((VALIDAZIONE.FL_VALID = 1) OR (VALIDAZIONE.ID_PERS_LAV = " & IDP & "))"
		if nArea_Capacita <> "" then
			Sql = Sql & " AND (CAPACITA.ID_AREACAPACITA = " & nArea_Capacita & ")"
		end if
		Sql = Sql & " ORDER BY CAPACITA.DENOMINAZIONE "
	
	End if
END IF

'COMPORTAMENTI
IF sCritSel = "Comportamenti" THEN
	Sql = ""
	Sql =	"SELECT ID_COMPORTAMENTO, GRADO_PERS_COMPOR" &_
			" FROM PERS_COMPOR" &_
			" WHERE ID_PERSONA = " & IDP
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
	Set Rs = CC.Execute (Sql)
		if not rs.eof then 
			MM = Rs.getrows()
		end if
	Set Rs = Nothing
	Tipo = sCritSel

	If nAree_Professionali <> "" Then
		Sql = ""
		Sql =	"SELECT DISTINCT" &_
				" COM.ID_COMPORTAMENTO, COM.DENOMINAZIONE, ARE.DENOMINAZIONE" &_
				" FROM" &_
				" AREE_PROFESSIONALI ARE," &_
				" COMPORTAMENTI COM," &_
				" COMPOR_FP COM_FP," &_
				" FIGUREPROFESSIONALI FIG, VALIDAZIONE, AREA_COMPORTAMENTI" &_
				" WHERE" &_
				" ((COM.ID_AREACOMPORTAMENTO = AREA_COMPORTAMENTI.ID_AREACOMPORTAMENTO) AND" &_
				" (COM.ID_VALID = VALIDAZIONE.ID_VALID)) AND" &_
				" ((VALIDAZIONE.FL_VALID = 1) OR (VALIDAZIONE.ID_PERS_LAV = " & IDP & ")) AND" &_
				" ( ARE.ID_AREAPROF=FIG.ID_AREAPROF ) AND" &_
				" ( COM_FP.ID_COMPORTAMENTO=COM.ID_COMPORTAMENTO ) AND" &_
				" ( FIG.ID_FIGPROF=COM_FP.ID_FIGPROF ) AND" &_
				" FIG.ID_AREAPROF=" & nAree_Professionali
		if nArea_Comportamenti <> "" then
			Sql = Sql & " and (COM.ID_AREACOMPORTAMENTO = " & nArea_Comportamenti & ")"
		end if
		Sql = Sql &  " ORDER BY UPPER(COM.DENOMINAZIONE)"

	Else
		Sql = ""
		Sql =	"SELECT COMPORTAMENTI.ID_COMPORTAMENTO, COMPORTAMENTI.DENOMINAZIONE, AREA_COMPORTAMENTI.DENOMINAZIONE" &_
				" FROM" &_
				" COMPORTAMENTI, AREA_COMPORTAMENTI, VALIDAZIONE" &_
				" WHERE" &_
				" ((COMPORTAMENTI.ID_AREACOMPORTAMENTO = AREA_COMPORTAMENTI.ID_AREACOMPORTAMENTO) AND" &_
				" (COMPORTAMENTI.ID_VALID = VALIDAZIONE.ID_VALID)) AND" &_
				" ((VALIDAZIONE.FL_VALID = 1) OR (VALIDAZIONE.ID_PERS_LAV = " & IDP & "))"
		if nArea_Comportamenti <> "" then
			Sql = Sql & " and (COMPORTAMENTI.ID_AREACOMPORTAMENTO = " & nArea_Comportamenti & ")"
		end if
		Sql = Sql & " ORDER BY COMPORTAMENTI.DENOMINAZIONE"
	
	End if
END IF
%>
	<form name="TornaIndietro" action="Bil_Ricerca2.asp" method="post">
		<input type="hidden" name="richstage" value="<%=nIdRich%>">	
		<input type="hidden" name="VISMENU" value="NO">
	</form>
<%
IF Sql <> "" THEN
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
	Set Rs = CC.Execute (Sql)
		If not rs.eof Then
			CriRic = rs(2)
			VV = Rs.getrows()
		Else
%>
			<script language="Javascript">
				alert("ATENCIÓN:\nNo hay datos para los parámetros ingresados.");
				document.TornaIndietro.submit();
			</script>
<%
			Response.Flush
			Response.End
		End if
	Set Rs = Nothing
	
	'PER CREARE HELP
	select case UCASE(Tipo)
		case "CONOSCENZE"
			HELP = "Javascript:Show_Help('/Pgm/help/GestProgetti/Stage/BilancioCompetenze/Dettagli1')"
		case "CAPACITA"
			HELP = "Javascript:Show_Help('/Pgm/help/GestProgetti/Stage/BilancioCompetenze/Dettagli2')"
		case "COMPORTAMENTI"
			HELP = "Javascript:Show_Help('/Pgm/help/GestProgetti/Stage/BilancioCompetenze/Dettagli3')"
	end select
%>

	<table border="0" width="520" height="81" cellspacing="0" cellpadding="0">
		<tr>
			<td width="500" height="81" background="<%=Session("Progetto")%>/images/titoli/strumenti2b.gif" valign="bottom" align="right">
				<table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
					<tr>
						<td width="100%" valign="top" align="right">
							<b class="tbltext1a">Bilancio Competenze</b>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<br>
	<table cellpadding="0" cellspacing="0" width="500" border="0">
		<tr height="18">
			<td class="sfondomenu" height="18" width="60%">
				<span class="tbltext0">
					<b>STAGE, TIROCINI, APPRENDISTATO</b>
				</span>
			</td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
			<td valign="middle" align="right" width="35%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1"></td>
		</tr>
		<tr width="371" class="SFONDOCOMM">
			<td colspan="3">
				Analisi:&nbsp;<b><%=Tipo%></b>
				<a href="<%=HELP%>" name="prov3" onmouseover="javascript:window.status='' ; return true">
					<img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0">
				</a>
			</td>
		</tr>
		<tr height="2">
			<td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>

	<form name="Salva" action="SalvaDettagli.asp" method="post">
		<input type="hidden" name="AREE_PROFESSIONALI" value="<%=nAree_Professionali%>">
		<input type="hidden" name="IdPers" value="<%=Request("IdPers")%>">
		<input type="hidden" name="back" value="<%=request("back")%>">
		<input type="hidden" name="hdnTipo" value="<%=Tipo%>">
		<input type="hidden" name="Pulsante" value>
		<input type="hidden" name="IdRichStage" value="<%=nIdRich%>">
		<input type="hidden" name="VISMENU" value="NO">

<%
	If IsArray(VV) then
		Response.Write "<input type='hidden' value='" & ubound(VV,2) & "' name='Limite'>"
		Response.Write "<table cellspacing=1 cellpadding=0 width='500' border=0>"
			Response.Write "<tr class='sfondocomm'>"
			Response.Write "<td colspan='2'>"
			Response.Write "<b>Criterio di Ricerca:</b>&nbsp;" & CriRic 
			Response.Write "</td>"
			Response.Write "</tr>"
			Response.Write "<TR><TD></TD></TR>"
			Response.Write "<TR class='sfondocomm'>"
			Response.Write "<TD><b>Denominazione</b></TD>"
			Response.Write "<td align=center ><b>Possesso<br> 0 - 1 - 2 - 3 - 4</b></font></td>"
			Response.Write "</tr>"

			if request("Limit") <> "" then
				Ini = request("Limit")
				Fin = Ini + 10
				if Ini<0 then Ini=0
			else
				Ini = 0
				Fin = 9
			end if

			if Fin > ubound(VV,2) then Fin = ubound(VV,2)
			if Ini < lbound(VV,2) then Ini = lbound(VV,2)

			for x = Ini to Fin
				Response.Write chr(13) & chr(10)

				if (x mod 2)= 0 then 
					sfondo="class='tblsfondo'" 
				else
					sfondo="class='tbltext1'"
				end if
				Response.Write "<tr " & sfondo & ">"
				Response.Write "<td class='tbltext1'>&nbsp;" & Ucase(VV(1,x)) & "</td>"
				Response.Write "<td class='tbltext1' align='center'>"
				Response.write "<input type=hidden name='Id" & x & "' value='" & VV(0,x) & "'>"
				Response.write "<input type=hidden name='exist" & x & "' value='" & (Mettival(MM,VV(0,x),0,0) <> "") & "'>"
				
				Response.Write "<input type='radio' value='' name='Grado" & x & "'"
					if Int("0" & Mettival(MM,VV(0,x),0,1)) = 0 then Response.Write " CHECKED "
				Response.Write " >"
				Response.Write "<input type='radio' value=1 name='Grado" & x & "'"
					if Int("0" & Mettival(MM,VV(0,x),0,1)) = 1 then Response.Write " CHECKED "
				Response.Write " >"
				Response.Write "<input type='radio' value=2 name='Grado" & x & "'"
					if Int("0" & Mettival(MM,VV(0,x),0,1)) = 2 then Response.Write " CHECKED "
				Response.Write " >"
				Response.Write "<input type='radio' value=3 name='Grado" & x & "'"
					if Int("0" & Mettival(MM,VV(0,x),0,1)) = 3 then Response.Write " CHECKED "
				Response.Write " >"
				Response.Write "<input type='radio' value=4 name='Grado" & x & "'"
					if Int("0" & Mettival(MM,VV(0,x),0,1)) = 4 then Response.Write " CHECKED "
				Response.Write " >"
				Response.Write "</td>"
				Response.Write "</tr>"
			next
			Response.write "<TR>"
			Response.write "<TD colspan=2>&nbsp"
			Response.write "</TD>"
			Response.Write "</TR>"
		Response.write "</table>"
	End if
END IF
%>
		<table border="0" width="500" CELLPADDING="0" cellspacing="0">
			<tr>
				<td class="tbltext1" colspan="2" align="center">
					<input type="hidden" name="Ini" Value="<%=Ini%>">
					<input type="hidden" name="Limit" Value="<%=Fin%>">
					<a href="javascript:document.TornaIndietro.submit();">
						<img src="<%=Session("progetto")%>/images/indietro.gif" border="0" alt="Torna alla pagina precedente">
					</a>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<%		
					If isArray(VV) then 
						NrPag=int(ubound(VV,2)/9)
					End if
		 
					if NrPag=0 then NrPag=1
		
					PagCor=int((ini+9)/9)

					If PagCor <> 1 then %>
						<a href="javascript:PopolaPulsante('Indietro');">
							<img src="<%=Session("progetto")%>/images/precedente.gif" border="0" alt="Salva i dati e scorre le pagine a ritroso">
						</a>&nbsp;&nbsp;
<%
					End if %>
		
					Pag. <b><%=PagCor%></b> di <b><%=NrPag%></b>

<%		
					if PagCor <> NrPag then %>
						&nbsp;&nbsp;<a href="javascript:PopolaPulsante('Avanti');">
							<img src="<%=Session("progetto")%>/images/successivo.gif" border="0" alt="Salva i dati e scorre le pagine in avanti">
						</a>
<%		
					end if%>
							
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="image" Name="ImgSalva" onclick="javascript:PopolaPulsante('Salva');" src="<%=Session("Progetto")%>/images/conferma.gif" border="0" alt="Registra i dati">

					<input type="hidden" name="DETTAGLI" Value="<%=Request.ServerVariables("URL")%>">
					<input type="hidden" name="AREA_CONOSCENZA" Value="<%=nArea_Conoscenze%>">
					<input type="hidden" name="AREA_CAPACITA" Value="<%=nArea_Capacita%>">
					<input type="hidden" name="AREA_COMPORTAMENTI" Value="<%=nArea_Comportamenti%>">
				</td>
			</tr>
			<tr>
				<td colspan="3" bgcolor="#3399CC"></td>
			</tr>
		</table>
	</form>
<!-- #INCLUDE VIRTUAL="strutt_coda2.asp" -->
<!--#include virtual = "/include/CloseConn.asp"-->



