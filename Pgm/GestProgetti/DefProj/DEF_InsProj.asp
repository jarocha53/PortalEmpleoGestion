<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include virtual="/strutt_testa2.asp"-->
<!--#include virtual="/Include/ControlDateVB.asp" -->
<!--#include virtual="/Include/DecCod.asp" -->
<!--#include virtual="/Include/OpenConn.asp"-->
<%
If Not ValidateService(Session("IdUtente"),"DEF_DefProj",cc) Then 
	Response.Redirect "/util/error_login.asp"
End If
%>
<html>
<head>
<meta NAME="GENERATOR" Content="Microsoft Visual Studio 6.0">
<title>Definizione Progetto</title>
<link REL="STYLESHEET" TYPE="text/css" HREF="<%=session("progetto")%>/fogliostile.css">
<script LANGUAGE="JavaScript">
<!--#include Virtual = "/Include/help.inc"-->
</script>
</head>
<script language="JavaScript">
<!--#include virtual="/include/ControlString.inc"-->
<!--#include virtual="/include/ControlDate.inc"-->

//------------------------------------------------------------------
function Vuoto(st,nomeCampo)
{
	if (st=='')
	{
		alert('Il campo ' + nomeCampo + ' � obbligatorio.')
		return true
	}
return false
}
//------------------------------------------------------------------
function AvvisoCarNonAmmessi(nCampo)
{
	alert('Il campo "' + nCampo + '" presenta caratteri non ammessi.')
}
//-------------------------------------------------------------------
function ControllaDati()
{
	var s,sDataOdierna,sDtInizio,sDtFine,sDtRil
	
	document.frmInsProj.txtDescrizione.value=TRIM(document.frmInsProj.txtDescrizione.value)
	s=document.frmInsProj.txtDescrizione.value
	if (Vuoto(s,'Descrizione')) 
	{
		document.frmInsProj.txtDescrizione.focus()
		return false;
	}
/*	if (!ValidateInputStringWithNumbereChar(s))
	{
		AvvisoCarNonAmmessi('Descrizione')
		document.frmInsProj.txtDescrizione.focus()
		return false;
	}
*/
	document.frmInsProj.txtTesto.value=TRIM(document.frmInsProj.txtTesto.value)
	
	document.frmInsProj.txtObiettivi.value=TRIM(document.frmInsProj.txtObiettivi.value)
	s=document.frmInsProj.txtObiettivi.value
	if (Vuoto(s,'Obiettivi')) 
	{
		document.frmInsProj.txtObiettivi.focus()
		return false;
	}
	
	document.frmInsProj.txtDestinatari.value=TRIM(document.frmInsProj.txtDestinatari.value)
	s=document.frmInsProj.txtDestinatari.value
	if (Vuoto(s,'Destinatari')) 
	{
		document.frmInsProj.txtDestinatari.focus()
		return false;
	}
	document.frmInsProj.txtrespons.value=TRIM(document.frmInsProj.txtrespons.value)
	s=document.frmInsProj.txtrespons.value
	if (Vuoto(s,'Responsabile'))
	{
		document.frmInsProj.txtrespons.focus()
		return false;
	}
	
	sDataOdierna=document.frmInsProj.txtDataOdierna.value
	sDtInizio = document.frmInsProj.txtDataInizio.value
	if (Vuoto(sDtInizio,'Data di Inizio')) 
	{
		document.frmInsProj.txtDataInizio.focus()
		return false;
	}
	
	if (!ValidateInputDate(sDtInizio)) 
	{	
		document.frmInsProj.txtDataInizio.focus()
		return false;
	}
	if (!ValidateRangeDate(sDataOdierna,sDtInizio))
	{
		//alert('sDataOdierna>sDtInizio')
		alert('La data di inizio non pu� essere inferiore alla data odierna.')
		document.frmInsProj.txtDataInizio.focus()
		return false;
	}
	
	sDtFine=document.frmInsProj.txtDataFine.value
	if (Vuoto(sDtFine,'Data di Fine')) 
	{
		document.frmInsProj.txtDataFine.focus()
		return false;
	}
	if (!ValidateInputDate(sDtFine)) 
	{	
		document.frmInsProj.txtDataFine.focus()
		return false;
	}
	
	if (ValidateRangeDate(sDtFine,sDtInizio))
	//if (!ValidateRangeDate(sDtInizio,sDtFine))
	{
		//alert('sDtFine<=sDtInizio')
		alert ('La data di fine deve essere maggiore della data di inizio.')
		document.frmInsProj.txtDataFine.focus()
		return false;
	}

	/*document.frmInsProj.txtPathLogo.value=TRIM(document.frmInsProj.txtPathLogo.value)
	s = document.frmInsProj.txtPathLogo.value
	if (!ValidatePath(s))
	{
		AvvisoCarNonAmmessi('Path del logo')
		document.frmInsProj.txtPathLogo.focus()
		return false;
	}*/

	document.frmInsProj.txtFinanziatore.value=TRIM(document.frmInsProj.txtFinanziatore.value)
	s=document.frmInsProj.txtFinanziatore.value
	if (Vuoto(s,'Finanziato da')) 
	{
		document.frmInsProj.txtFinanziatore.focus()
		return false;
	}
	
	document.frmInsProj.txtLegge.value=TRIM(document.frmInsProj.txtLegge.value)
	s = document.frmInsProj.txtLegge.value
	if (Vuoto(s,'Legge')) 
	{
		document.frmInsProj.txtLegge.focus()
		return false;
	}
	
	document.frmInsProj.txtIndIscr.value=TRIM(document.frmInsProj.txtIndIscr.value)
	s=document.frmInsProj.txtIndIscr.value
	if (Vuoto(s,'Modalit� di iscrizione'))
	{
		document.frmInsProj.txtIndIscr.focus()
		return false;
	}
	
/*	
	s = document.frmInsProj.cmbCodProg.value
	if (Vuoto(s,'Appartenente al progetto')) 
	{
		document.frmInsProj.cmbCodProg.focus()
		return false;
	}
*/

/*	if (document.frmInsProj.rdoIndGrad[0].checked && document.frmInsProj.cmbCodTimpr.value=='')
	{
		alert('Se il progetto prevede l`elaborazione di una graduatoria \nbisogna scegliere un elemento di aggregazione.')
		document.frmInsProj.cmbCodTimpr.focus()
		return false
	}*/

return true	
}
//--------------------------------------------------------------------
function Abilita_combo()
{
	document.frmInsProj.cmbCodTimpr.disabled=false
}

function Disabilita_combo()
{

	document.frmInsProj.cmbCodTimpr.selectedIndex=0
	document.frmInsProj.cmbCodTimpr.disabled=true
}
var windowarea
function openwin(f)
{		

	if (windowarea != null ) 
		{
		windowarea.close(); 
		}
	windowarea = window.open(f, '','width=520,height=530,left=10,top=10,Scrollbars=no');		
	
}	
</script>

<body>
<%
'Response.Write "session('progetto')=" & session("progetto") & "<br>"
'Response.Write "session('idutente')=" & session("idutente") 
'Response.Write "session('creator')=" & session("creator") & "<br>"






%>

	<br>
	<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
		<tr height="18">
			<td class="sfondomenu" width="67%" height="18"><span class="tbltext0"><b>&nbsp;RICHIESTA ATTIVAZIONE</b></span></td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
			<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">(*) campi obbligatori</td>
		</tr>		
	</table>
	<!-- Commento -->
	<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
		<tr>
			<td align="left" class="sfondocomm">
				Richiesta di attivazione  di un nuovo progetto. <br>
				Premere <b>Invia</b> per attivare la richiesta.
				<a href="Javascript:Show_Help('/Pgm/help/GestProgetti/DefProj/DEF_InsProj')" name onmouseover="javascript:status='' ; return true">
				<img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
			</td>
		</tr>
	  
		<tr height="2">
			<td class="sfondocomm" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>
	<br>
	<table width="500" border="0">
		<tr>
			<td width="60%" class="textred" align="right" valign="middle">Per ulteriori informazioni
			</td>
			<td width="40%" align="left">
				<a href="javascript: openwin('/pgm/gestprogetti/defproj/def_leggi.asp')">
				<img border="0" src="/images/Icons/documento.jpg"></a>
			</td>
		</tr>	
	</table>
<%
	Dim str
%>
	<form method="post" name="frmInsProj" onsubmit="return ControllaDati()" action="DEF_CnfInsProj.asp">
		<input type="hidden" name="hCodProg" value="<%=mid(Session("Progetto"),2)%>">
		<table border="0" cellpadding="0" cellspacing="1" width="500">
			<tr>
				<td align="left" class="tbltext1">
					&nbsp;&nbsp;&nbsp;&nbsp;
					<strong>Descrizione*</strong>
					&nbsp;
				</td>
				<td align="left">
					<input type="text" name="txtDescrizione" class="textblacka" style="TEXT-TRANSFORM: uppercase;" size="35" maxlength="50">
				</td>
		    </tr>
		    <tr>
				<td align="left" nowrap class="tbltext1">
					&nbsp;&nbsp;&nbsp;&nbsp;
					<strong>Testo di presentazione</strong>
					&nbsp;
				</td>
				<td align="left">
					<span class="tbltext1">&nbsp;- Utilizzabili <label name="lblNumCar1" id="lblNumCar1">250</label> caratteri -</span>
					<textarea name="txtTesto" id="txtTesto" class="textblack" style="TEXT-TRANSFORM: uppercase" cols="41" OnKeyUp="JavaScript:CheckLenTextArea(txtTesto,lblNumCar1,250)"></textarea>
				</td>
		    </tr>
		    <tr>
				<td align="left" nowrap class="tbltext1">
					&nbsp;&nbsp;&nbsp;&nbsp;
					<strong>Obiettivi*</strong>
					&nbsp;
				</td>
				<td align="left">
					<span class="tbltext1">&nbsp;- Utilizzabili <label name="lblNumCar2" id="lblNumCar2">250</label> caratteri -</span>
					<textarea name="txtObiettivi" id="txtObiettivi" class="textblack" style="TEXT-TRANSFORM: uppercase" cols="41" OnKeyUp="JavaScript:CheckLenTextArea(txtObiettivi,lblNumCar2,250)"></textarea>
				</td>
		    </tr>
		    <tr>
				<td align="left" nowrap class="tbltext1">
					&nbsp;&nbsp;&nbsp;&nbsp;
					<strong>Destinatari*</strong>
					&nbsp;
				</td>
				<td align="left">
					<span class="tbltext1">&nbsp;- Utilizzabili <label name="lblNumCar3" id="lblNumCar3">250</label> caratteri -</span>
					<textarea name="txtDestinatari" id="txtDestinatari" class="textblack" style="TEXT-TRANSFORM: uppercase" cols="41" OnKeyUp="JavaScript:CheckLenTextArea(txtDestinatari,lblNumCar3,250)"></textarea>
				</td>
		    </tr>
		    <tr>
				<td align="left" nowrap class="tbltext1">
					&nbsp;&nbsp;&nbsp;&nbsp;
					<strong>Responsabile*</strong>	
				</td>
				<td align="left">
<%					Set Rs = Server.CreateObject("ADODB.RECORDSET")

					SqlRespo = "select nome,cognome,email from utente order by cognome"				   			  
				   			   					
'PL-SQL * T-SQL  
SQLRESPO = TransformPLSQLToTSQL (SQLRESPO) 
					Rs.Open SqlRespo,cc,3 
					
					If  Not Rs.EOF Then%>
						<select name="txtrespons" class="textblack">
							<option value></option>	
<%							Do while Not rs.EOF  									
%>							
							<option value="<%=Rs.Fields("email") & "|"%> <%=Rs.Fields("nome") & " "%> <%=Rs.Fields("cognome")%>"><%=Rs.Fields("cognome")%>&nbsp;<%=Rs.Fields("nome")%></option>
				
<%							rs.MoveNext 
							Loop%>
						</select>					
				</td>		
			</tr>
		    <tr>
				<input type="hidden" name="txtDataOdierna" value="<%=ConvDateToString(Date())%>">
				<td align="left" nowrap class="tbltext1">
					&nbsp;&nbsp;&nbsp;&nbsp;
					<strong>Data di inizio*</strong> (gg/mm/aaaa)
					&nbsp;
				</td>
				<td align="left">
					<input type="text" name="txtDataInizio" class="textblacka" maxlength="10" size="10">					
				</td>
		    </tr>
		    <tr>
				<td align="left" nowrap class="tbltext1">
					&nbsp;&nbsp;&nbsp;&nbsp;
					<strong>Data di fine*</strong> (gg/mm/aaaa)
					&nbsp;
				</td>
				<td align="left">
					<input type="text" name="txtDataFine" class="textblacka" maxlength="10" size="10">					
				</td>
		    </tr>
		    
		    <!--<tr>				<td align="left" nowrap class="tbltext1">					&nbsp;&nbsp;&nbsp;&nbsp;					<strong>Path del logo</strong>					&nbsp;				</td>				<td align="left">					<input type="text" name="txtPathLogo" class="textblacka" style="TEXT-TRANSFORM: uppercase" maxlength="250" size="35">									</td>			</tr>-->
			
		    <tr>
				<td align="left" nowrap class="tbltext1">
					&nbsp;&nbsp;&nbsp;&nbsp;
					<strong>Finanziato da*</strong>
					&nbsp;
				</td>
				<td align="left">
					<input type="text" name="txtFinanziatore" class="textblacka" style="TEXT-TRANSFORM: uppercase" maxlength="50" size="35">					
				</td>
			</tr>
			<tr>
				<td align="left" nowrap class="tbltext1">
					&nbsp;&nbsp;&nbsp;&nbsp;
					<strong>Legge*</strong>
					&nbsp;
				</td>
				<td align="left">
					<input type="text" name="txtLegge" class="textblacka" style="TEXT-TRANSFORM: uppercase" maxlength="50" size="35">					
				</td>
			</tr>
			
			<!--<tr>				<td align="left" nowrap class="tbltext1">					&nbsp;&nbsp;&nbsp;&nbsp;					<strong>Attuato da*</strong>					&nbsp;				</td>				<td align="left">					<span class="textblacka">ITALIA LAVORO</span>					<input type="hidden" name="txtAttuatore" maxlength="50" value="ITALIA LAVORO">									</td>			</tr>-->
			
			<!--tr>				<td align="left" nowrap class="tbltext1">					&nbsp;&nbsp;&nbsp;&nbsp;					<strong>Appartenente al progetto*</strong>					&nbsp;				</td>				<td align="left">					str="CPROJ|0|" & Date() & "|" & mid(Session("Progetto"),2) & "|cmbCodProg|ORDER BY DESCRIZIONE"					CreateCombo(str)				</td>			</tr-->
			<tr>
				<td align="left" nowrap class="tbltext1">
					&nbsp;&nbsp;&nbsp;&nbsp;
					<strong>Modalit� di iscrizione*</strong>
					&nbsp;
				</td>
				<td align="left">
					<select name="txtIndIscr" id="txtIndIscr" class="textblack">
						<option></option>
						<option value="D">DIRETTA (tramite form)</option>
						<option value="I">INDIRETTA (tramite estrazione)</option>
					</select>				
				</td>
			</tr>
			<tr height="2">
				<td colspan="2">&nbsp;
				</td>
			<tr>	
			<tr>
				<td align="left" nowrap class="tbltext1">
					&nbsp;&nbsp;&nbsp;&nbsp;
					<strong>Il progetto individua dei criteri<br>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;restrittivi di selezione?&nbsp;</strong>				
				</td>				
				<td align="left">
					<table border="0">
						<tr>
							<td><input type="radio" id="rdoIndSel" name="rdoIndSel" Value="S" checked></td>
							<td class="tbltext"><b>SI </b></td>
							<td><input type="radio" id="rdoIndSel" name="rdoIndSel" Value="N"></td>
							<td class="tbltext"><b>NO </b></td>
						</tr>
					</table>
				</td>
			</tr>
			<tr height="2">
				<td colspan="2">&nbsp;
				</td>
			<tr>
			<tr>
				<td align="left" nowrap class="tbltext1">
					&nbsp;&nbsp;&nbsp;&nbsp;
					<strong>Il progetto prevede l'elaborazione<br>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;di una graduatoria?&nbsp;</strong>					
				</td>
				<td align="left">
					<table border="0">
						<tr>
							<td><input type="radio" id="rdoIndGrad" name="rdoIndGrad" Value="S" checked onClick="JavaScript:Abilita_combo();"></td>
							<td class="tbltext"><b>SI </b></td>
							<td><input type="radio" id="rdoIndGrad" name="rdoIndGrad" Value="N" onClick="JavaScript:Disabilita_combo();"></td>
							<td class="tbltext"><b>NO </b></td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td align="left" nowrap class="tbltext1">
					&nbsp;&nbsp;&nbsp;&nbsp;
					<strong>Il progetto prevede la partecipazione<br>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ad uno stage?&nbsp;</strong>					
				</td>
				<td align="left">
					<table border="0">
						<tr>
							<td><input type="radio" id="rdoStage" name="rdoStage" Value="S" checked></td>
							<td class="tbltext"><b>SI </b></td>
							<td><input type="radio" id="rdoStage" name="rdoStage" Value="N"></td>
							<td class="tbltext"><b>NO </b></td>
						</tr>
					</table>
				</td>
			</tr>
			<tr height="2">
				<td colspan="2">&nbsp;
				</td>
			<tr>
			<tr>
				<td align="left" nowrap class="tbltext1">
					&nbsp;&nbsp;&nbsp;&nbsp;
					<strong>Su quali elementi di aggregazione?</strong>					
				</td>
				<td align="left">
<%					str="TIMPR|0|" & Date() & "||cmbCodTimpr|ORDER BY DESCRIZIONE"
					CreateCombo(str)
%>
				</td>
			</tr>
			
			
			
<%					Else
					
					End If		%>									
		</table>
		<br>
<%		rs.close
		set rs = nothing %>
		<table border="0" cellpadding="0" cellspacing="1" width="500">
			<tr>
				<td align="center">
					<!--<a HREF="javascript:history.back()"><img SRC="<%=Session("Progetto")%>/images/indietro.gif" border="0"></a>-->
					<input type="image" src="<%=Session("Progetto")%>/images/conferma.gif" border="0" value="Inserisci" id="image1" name="image1">
			    </td>
			</tr>
		</table>
	</form>

</body>
</html>
<!--#include virtual="/Include/CloseConn.asp"-->
<!--#include virtual="/strutt_coda2.asp"-->
