<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #include virtual="/util/dbutil.asp" -->
<!-- #include virtual="/include/openconn.asp" -->
<!--#include Virtual = "/include/DecCod.asp"-->
<!-- #include virtual="/include/SelAreaTerrBandi.asp" -->
<html>
<head>
<title>Oficina de Empleo</title>
<link REL="STYLESHEET" TYPE="text/css" HREF="<%=session("progetto")%>/fogliostile.css">

<script language="javascript">
<!-- #include virtual="/include/help.inc" -->

function controlloVal(valoreDesc,text){

	var valoreRegione;
	var valoreArea;
	
	valoreArea = opener.document.frmModOperatore.txtCentroImpiego.value;
	
	valoreDesc = valoreDesc.toUpperCase();

	switch (text){
			
		case "txtCentroImpiego":
			if (valoreDesc != valoreArea){
				opener.document.frmModOperatore.txtCentroImpiego.value = ""
				opener.document.frmModOperatore.txtCentroImpiegoHid.value = ""
			}
		break;	
		
	}
}

function InviaSelezione(idArea,Denominazione,sNomeCampo){
		
	controlloVal(Denominazione,sNomeCampo)
		
	var i = 0
		
	while (opener.document.frmModOperatore.elements[i].name != sNomeCampo){
		i++;
	}
		
	opener.document.frmModOperatore.elements[i+1].value = idArea
	var ciccio = Denominazione.replace("$", "'")
	opener.document.frmModOperatore.elements[i].value = ciccio.toUpperCase()
	self.close()
}


function InviaProv(){
	frmSelArea.submit();
}

function Chiudi()
{
	self.close()
}

</script>
</head>
<%
 
sArea= Request.QueryString("Area")
sCampo= Request.QueryString("sTxtArea")
sNomeCampo= Request.QueryString("NomeCampo")
sProv=Request.QueryString("cmbProv")

aProv=SetProvUorg(Session("idUorg"))

nProvSedi = len(aProv)

if nProvSedi <> 0 then

	''''''SM 28082007 inizio
	sCondizione = "AND Codice in ('"

	for i=1 to nProvSedi
		if mid(aProv,i,1)="," then
		
			sCondizione = sCondizione & "'" & mid(aProv,i,1) & "'"
		else
			sCondizione = sCondizione  & mid(aProv,i,1)	
		end if
	''''''''SM 28082007 fine 
	next				
	sCondizione = sCondizione & "')"
else 	
	sCondizione = ""
end if


%>

<body>
<form name="frmSelArea" method="get" action"OPE_SelArea.asp">

<input type="hidden" name="Area" value="<%=sArea%>">
<input type="hidden" name="sTxtArea" value="<%=sCampo%>">
<input type="hidden" name="NomeCampo" value="<%=sNomeCampo%>">
<input type="hidden" id="txtProv" name="txtProv" value="<%=sProv%>">

<table border="0" CELLPADDING="0" CELLSPACING="0" width="380">

<tr height="17">
	  <td class="sfondomenu" width="67%" height="18"><span class="tbltext0"> <b>&nbsp;SELECCION 
        DE LA OFICINA DE EMPLEO</b> </td>
	<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">
	</td>
	<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
</tr>
<tr>
	  <td class="sfondocommaz" width="57%" colspan="3"> Seleccionar el departamento
        y la oficina de empleo de la lista que se desplegará.<br>
        Presionar Cancelar para no seleccionar nada<a href="Javascript:Show_Help('/Pgm/help/AbilOpe/OPE_SelArea')"> 
        <img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0" onmouseover="javascript:window.status='' ; return true"></a> 
      </td>
</tr>
<tr height="2">
	<td class="sfondocommaz" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif">
	</td>
</tr>
</table>

<br>
  <p class="tbltext1" align="center"><b>Seleccionar el departamento de la Ofina de Empleo</b></p>

<table border="0" CELLPADDING="2" CELLSPACING="2" width="380">
	<tr>
		<td align="center" colspan="3">
<%	    
		sInt = "PRVEX|0|" & date & "|" & sProv & "|cmbProv' onchange='InviaProv()|" & sCondizione & " ORDER BY NOME_TABELLA, DESCRIZIONE"			
				CreateCombo(sInt)
%>	
		</td>
	</tr>
<%
if sProv <> "" then
	if session("Progetto")="/LAVSICILIA" then
		sTimpr="in('03','20')"
	else
		sTimpr="='03'"
	end if
'	sSQL = "SELECT B.ID_SEDE, B.DESCRIZIONE,A.RAG_SOC FROM IMPRESA A, sede_impresa b WHERE a.COD_TIMPR='03' and a.id_impresa=b.id_impresa and prv='" & sProv & "' ORDER BY DESCRIZIONE"
	sSQL = "SELECT B.ID_SEDE, B.DESCRIZIONE,A.RAG_SOC FROM IMPRESA A, sede_impresa b WHERE a.COD_TIMPR " & sTimpr & " and a.id_impresa=b.id_impresa and prv='" & sProv & "' ORDER BY DESCRIZIONE"
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	Set rsArea = CC.Execute(sSQL)		

	if not rsArea.EOF then
%>	
	<tr>
		<td colspan="2" align="center">&nbsp;</td>
	</tr>
	<tr>
		
      <td colspan="2" align="center"> <a class="textred" href="Javascript:InviaSelezione('','','<%=sNomeCampo%>')"><b>Remover 
        Selecci&oacute;n </b></a> </td>
	</tr>
	<tr>
		<td colspan="2" align="center">&nbsp;</td>
	</tr>
<%
		Do While Not rsArea.EOF
		dim sRagDesc
		sRagDesc = rsArea("RAG_SOC") & " - " & rsArea("DESCRIZIONE")
%>
	<tr>
		<td nowrap width="190" class="tblsfondo">
			<a class="textblack" href="Javascript:InviaSelezione('<%=rsArea(0)%>','<%=replace(sRagDesc,"'","$")%>','<%=sNomeCampo%>')"><b><%=ucase(rsArea(1))%></b></a>
		</td>
		<%rsArea.MoveNext%>
		<td nowrap width="190" class="tblsfondo">
		<%if not rsArea.EOF then
		     sRagDesc = rsArea("RAG_SOC") & " - " & rsArea("DESCRIZIONE")%>
			<a class="textblack" href="Javascript:InviaSelezione('<%=rsArea(0)%>','<%=replace(sRagDesc,"'","$")%>','<%=sNomeCampo%>')"><b><%=ucase(rsArea(1))%></b></a>
				<%rsArea.MoveNext
		 else%>
			&nbsp;
		<%end if%>
		</td>
	</tr>
<%
		loop
		rsArea.Close
%>
	<tr>
		<td colspan="2" align="center">&nbsp;</td>
	</tr>
	<tr>
		
      <td colspan="2" align="center"> <a class="textred" href="Javascript:InviaSelezione('','','<%=sNomeCampo%>')"><b>Remover 
        Selecci&oacute;n </b></a> </td>
	</tr>
	<tr>
		<td colspan="2" align="center">&nbsp;</td>
	</tr>	
<%
	else
%>
	<tr>
		<td>&nbsp;</td>
	</tr>

	<tr>
		
      <td class="tbltext3" colspan="2" align="center"> <b>No se encontraron Oficinas 
        para el departamento seleccionado</b></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>

<%
		
	end if
end if
%>	
</table>
</form>

<table width="380" cellspacing="2" cellpadding="1" border="0">
	<tr align="center">
		<td>
			<a href="javascript: Chiudi()"><img src="<%=Session("progetto")%>/images/chiudi.gif" title="Chiudi la pagina" border="0" align="absBottom"></a>
		</td>
	</tr>		
</table>
</body>
</html>
<!-- #include virtual="/include/closeconn.asp" -->
