<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #include Virtual="/strutt_testa2.asp" -->
<!-- #include virtual="/include/OpenConn.asp" -->
<!-- #include virtual="/include/RuoloFunzionale.asp" -->
<%
If Not ValidateService(Session("IdUtente"),"Gestione Wizard",cc)  Then 
	Response.Redirect "/util/error_login.asp"
End If

	Session("ck_idgruppo")=""
	Session("ck_idfunzione")=""
	Session("ck_desgruppo")=""
	Session("ck_webpath")=""
	Session("Msg_Error")=""
%>

<script Language="javascript">
<!--#include Virtual = "/Include/help.inc"-->
<!--#include Virtual = "/Include/ControlString.inc"-->

function Controlla(frm) {

	if  (FormGruppo.desgruppo.value != "")  {
		alert("E' stato riempito il campo Descrizione: non e' possibile selezionare il pulsante avanti ");
		FormGruppo.desgruppo.focus();
		return false;
	}
	if  (FormGruppo.webpath.value != "")  {
		alert("E' stato riempito il campo Nome cartella: non e' possibile selezionare il pulsante avanti ");
		FormGruppo.webpath.focus();
		return false;
	}
	if (FormGruppo.cod_rorga.selectedIndex != 0 ){
	
		alert("E' stato riempito il campo Ruolo Organizzativo: non e' possibile selezionare il pulsante avanti");
		FormGruppo.cod_rorga.focus();
		return false;
		}
	
}

function checkField(frm){
// 18/04/2005 controllo apostrofo nei campi Descrizione e Nome cartella (funzione ChechSingolChar) 

	if (frm.desgruppo.value=="") {
		alert("Il campo Descrizione � obbligatorio");
		frm.desgruppo.focus();
		return false;
	}else{
		frm.desgruppo.value=frm.desgruppo.value.toUpperCase();
		
		apostrofo = "'";
			if (!ChechSingolChar(frm.desgruppo.value,apostrofo))
			{
				alert("L'apostrofo nel campo Descrizione non e' consentito");
				frm.desgruppo.focus();
				return false;
			}
						
	}
	
	if (frm.webpath.value=="") {
		alert("Il campo Nome cartella � obbligatorio");
		frm.webpath.focus();
		return false;
	}else{
		if (notValidChar(frm.webpath.value)) return false;
		
		apostrofo = "'";
			if (!ChechSingolChar(frm.webpath.value,apostrofo))
			{
				alert("L'apostrofo nel campo Nome cartella non e' consentito");
				frm.webpath.focus();
				return false;
			}
			
	}
	
	if (frm.cod_rorga.value==0){
		alert("Il campo Ruolo Organizzativo � obbligatorio");
		frm.cod_rorga.focus();
		return false;
	}
	
	
	return confirm("Confermi la creazione di : \nDescrizione - "+frm.desgruppo.value+"\nNome cartella - "+frm.webpath.value+"\nCodice Rorga - "+frm.cod_rorga.value+ "?");
}

function notValidChar(cadena){
	var cars=" \\/\"?<>|:*";
	var carsnotok="\\/\"?<>|:*";
	var i, longitud;
	longitud = cadena.length;
	if (longitud==0) return true;
	for (i=0;i<longitud;i++){
		if (cars.indexOf(cadena.charAt(i)) > -1){
			alert ("Il campo Acronimo non pu� contenere lo spazio ne i seguenti caratteri : "+carsnotok);
			return true;
		}
	}
	return false;
}


<%
	if Session("Msg_Error") <> "" then
		response.write "alert('" & Session("Msg_Error") & "');"
	end if
%>
	
//-->e
</script>


<body onload="FormGruppo.desgruppo.focus()">
<br>
<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
	<tr height="18">
		<td class="sfondomenu" width="67%" height="18"><span class="tbltext0"><b>&nbsp;ADMINISTRACION PORTAL - Crear Grupo</b></span></td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
		<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">(*) campo obligatorio</td>
	</tr>
	<tr>
		<td class="sfondocomm" width="57%" colspan="3"><br>
			<p>En esta p�gina podr� definir un grupo, dando una descripci�n del mismo, una identificaci�n (que se usara como login) que lo identificar� dentro del sistema.</p> 
            Este identificador no podr� contener caracteres distintos a n�meros o letras.
			<p> Si se desea utilizar un grupo ya existente presionar el boton &quot;Avanzar&quot;.</p>
		</td>
	</tr>
	<tr height="17">
		<td class="sfondocomm" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
	</tr>
</table>
<br>  
<form method="GET" action="pag1fun.asp" name="FormGruppo" onsubmit="return checkField(this);">
<table border="0" cellspacing="0" cellpadding="0" width="500">
	<tr height="30" class="sfondocomm"> 
		<td width="480" align="left" valign="middle"><b>Gesti�n de Grupos</b></td>
		<td align="right" valign="middle" colspan="2"> 
		<!--img src="<%=Session("Progetto")%>/images/help.gif" border="0"-->
		<a href="Javascript:Show_Help('/Pgm/help/Wizard/creagruppo/pag1/')" name onmouseover="javascript:status='' ; return true">
     	     <img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
		</td>  
	</tr>
</table>
<table border="0" cellpadding="2" cellspacing="2" width="500">
	<tr>
		<td height="25" class="tbltext1" width="200"><b>Descripci�n*</b></td>
		<td width="400"><input class="textblack" type="text" name="desgruppo" size="50" Value></td>
	</tr>
	<tr>
		<td height="25" class="tbltext1" width="200"><b>Identificador/login*</b></td>
		<td width="400"><input class="textblack" type="text" name="webpath" size="40" Value></td>
	</tr>
	
	<tr>
		<td height="25" class="tbltext1" width="200"><b>Rol Organizativo*</b></td>
		<td>

					<select name="cod_rorga" class="textblack">
					<option value="0"></option>
					
		<%    


					sCondizione = ""		
					sTabella = "RORGA"
					dData	 = Date()
					nOrder   = 0
					Isa	 = 0    

				

					aRuoloOrganizzativo = decodTadesToArray(sTabella,dData,sCondizione,nOrder,Isa)
					
					if IsArray(aRuoloOrganizzativo) then
						for iRorga = 0 to ubound(aRuoloOrganizzativo) - 1
							response.write "<option value='" & aRuoloOrganizzativo(iRorga,iRorga,iRorga) & "'>" & aRuoloOrganizzativo(iRorga,iRorga+1,iRorga) & "</option>"
						next
						Erase aRuoloOrganizzativo    
					end if
		%>
					</select>
				</td>
			</tr>
		<tr> 
		<td align="left" nowrap class="tbltext1"><b>Funci�n de Monitoreo</b></td>
		<td align="left"><input type="checkbox" name="aut_log" style="WIDTH: 14px; HEIGHT: 20px"></td>
		</tr>
		<tr>
			
			<td align="left" nowrap class="tbltext1">
				<b>Rol Funcional</b>
			</td>			
	
	        <td>			
<%
				sInt = "cmbruolo|0||"			
				CreateRuolo (sInt)
%>
			</td>
	
	</tr>
	
	<tr>
	<td><br>
		</td>
		</tr>
	<tr>
		<td colspan="2" align="center">
			<input type="image" alt="Invia" src="<%=Session("Progetto")%>/images/conferma.gif">
			<a href="pag2.asp"><img border="0" alt="Avanti" src="<%=Session("Progetto")%>/images/avanti.gif" onclick="return Controlla(this)"></a>
		</td>
	</tr>
</table>
<br>
<table border="0" cellpadding="2" cellspacing="1" width="500">
	<tr>
	   <td width="420"></td>	
	   <td height="25" class="sfondocomm" align="center"><b>Paso 1/4</b></td>
	</tr>
</table>
</form>

<!-- #include Virtual="/strutt_coda2.asp" -->
<!-- #include virtual="/include/closeconn.asp" -->
