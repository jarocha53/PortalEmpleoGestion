<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--include virtual=include/DecCod.asp-->
<%
	dim aLingue
	if inStr(1,aCV(8),"|") = 0 and len(aCV(8)) > 0 then
		aLingue = Array(aCV(8))
	elseif len(aCV(8)) < 1 then
		aLingue = Array()
	else
		aLingue = Split(aCV(8),"|")
	end if
%>
<TR valign='top'>
	<TD>
		<table border='0' cellspacing='3' cellpadding='4'>
			<TR>
				<TD class='textdesciz' align='left' colspan=2>
					Idiomas Conocidos
				</TD>
				<TD class='textdesciz' align='left'>
					&nbsp;
				</TD>
				<TD class='textdesciz' align='left'>
					Nivel
				</TD>
			</TR>
			<TR>
				<TD class='textdesciz' align='center' colspan='4'>
					&nbsp;
				</TD>
			</TR>
<%
	sSQL = "SELECT PERS_CONOSC.ID_CONOSCENZA, CONOSCENZE.DENOMINAZIONE, PERS_CONOSC.COD_GRADO_CONOSC " &_ 
			" FROM CONOSCENZE, PERS_CONOSC " &_ 
			" WHERE CONOSCENZE.ID_CONOSCENZA = PERS_CONOSC.ID_CONOSCENZA " &_ 
			" AND PERS_CONOSC.ID_PERSONA = " & id_utente &_
			" AND PERS_CONOSC.IND_STATUS = 0 " &_ 
			" AND CONOSCENZE.ID_AREACONOSCENZA = 23 " &_
			" ORDER BY CONOSCENZE.DENOMINAZIONE"
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	set rsLingue = cc.execute(sSQL)
	if not rsLingue.eof then
		iNLingue = 0
		do while not rsLingue.eof
		iNLingue = iNLingue + 1
%>
			<TR>
				<TD class='textdesciz' valign='middle'>
					<input type='checkbox' name='lingua<%=iNLingue%>' value='<%=rsLingue.Fields("ID_CONOSCENZA").value%>'<%
						if UBound(aLingue) > -1 then
							for cont = 0 to UBound(aLingue)
								if Clng(aLingue(cont)) = Clng(rsLingue.Fields("ID_CONOSCENZA").value) then
									Response.Write " checked"
								end if
							next
						elseif len(aCV(8)) < 1 then
									Response.Write " checked"
						end if %>
					>
				</TD>
				<TD class='textblubold' valign='middle'>
					<%=rsLingue.Fields("DENOMINAZIONE").value%>
				</TD>
				<TD class='textdesciz' align='center'>
					&nbsp;&nbsp;&nbsp;&nbsp;
				</TD>
				<TD class='textblubold' valign='middle'>
					<%= DecCodValDizDati("PERS_CONOSC","COD_GRADO_CONOSC",Cstr(rsLingue.Fields("COD_GRADO_CONOSC").value))%>
					<%'decGradoConosc(CInt(rsLingue.Fields("COD_GRADO_CONOSC").value))%>
				</TD>
			</TR>
			
<%
			rsLingue.MoveNext
		loop
%>
			<TR>
				<TD colspan=4>
					<input type='hidden' name='iNLingue' value='<%=iNLingue%>'>
				</TD>
			</TR>
<%
	end if
%>
		</table>
	</td>
</tr>
