
3717838

<% 

Dim sqlEuropeo
Dim RREuropeo
set RREuropeo = Server.CreateObject("ADODB.Recordset")
aDati = Split(aCV(2),"|")
aSezioni = Split(aCV(3),"|")
nSezioni = ubound(aSezioni)
call getDatiDB(id_utente)

oFileCurr.writeline ("<tr>")
oFileCurr.writeline ("	<td>&nbsp;</td>")
oFileCurr.writeline ("</tr>")
oFileCurr.writeline ("<tr>")
oFileCurr.writeline ("	<td>")

if STAMPA = "SI" then 
	oFileCurr.writeline ("<table border='0' cellspacing='0' cellpadding='3' align='center' width='600'>")
else 
	oFileCurr.writeline ("<table border='0' cellspacing='0' cellpadding='3' width='100%'>")
end if 

oFileCurr.writeline ("<tr>")
oFileCurr.writeline ("	<td background='/images/Curriculum/sfondomenudx1.gif' class='titoloEuropeo' align='LEFT' valign='top' width='150'><b>FORMATO EUROPEO PARA EL CURRICULUM VITAE</b>")
'oFileCurr.writeline ("		<br><img src='/images/Curriculum/europa.gif'>")
oFileCurr.writeline ("	</td>")
oFileCurr.writeline ("	<td width='60%'>&nbsp;</td>")
oFileCurr.writeline ("</tr>")
oFileCurr.writeline ("<tr>")
oFileCurr.writeline ("	<td background='/images/Curriculum/sfondomenudx1.gif'> &nbsp;</td>")
oFileCurr.writeline ("	<td> &nbsp;</td>")
oFileCurr.writeline ("</tr>")
oFileCurr.writeline ("<tr>")
oFileCurr.writeline ("	<td background='/images/Curriculum/sfondomenudx1.gif' class='textblubold' align='LEFT' valign='top' width='150'><b>INFORMACION PERSONAL</b></td>")
oFileCurr.writeline ("	<td width='60%'>&nbsp;</td>")
oFileCurr.writeline ("</tr>")
oFileCurr.writeline ("<tr>")
oFileCurr.writeline ("	<td background='/images/Curriculum/sfondomenudx1.gif' class='textblu' valign='top' align='LEFT' width='150'>Nombre</td>")
oFileCurr.writeline ("	<td class='textblu'>")
oFileCurr.writeline ("		<table border=0>")
oFileCurr.writeline ("			<tr>")
oFileCurr.writeline ("				<td class=textblu width=5>&nbsp;</td>")
oFileCurr.writeline ("				<td class='textblu'><b>" & sCognome & " " & sNome & "</b>")
oFileCurr.writeline ("				</td>")
oFileCurr.writeline ("			</tr>")
oFileCurr.writeline ("		</table>")
oFileCurr.writeline ("	</td>")
oFileCurr.writeline ("</tr>")

' RESIDENZA 
if aDati(3) = "1" then
	oFileCurr.writeline ("<tr>")
	oFileCurr.writeline ("	<td background='/images/Curriculum/sfondomenudx1.gif' class='textblu' valign='top' align='LEFT' width='150'>Direcci�n</td>")
	oFileCurr.writeline ("	<td class='textblu'>")
	oFileCurr.writeline ("			<table border=0>")
	oFileCurr.writeline ("			<tr>")
	oFileCurr.writeline ("				<td class=textblu width=5>&nbsp;</td>")
	oFileCurr.writeline ("				<td class='textblu'>")
	if len(sInd_Res) > 0 then 
		oFileCurr.writeline ( sInd_Res & " - ")
	end if 
	if len(sCap_Res) > 0 then
		oFileCurr.writeline ( sCap_Res & " ")
	end if 
	if len(DescrComune(sCom_Res)) > 1 then 
		oFileCurr.writeline ( DescrComune(sCom_Res) )
	end if
	if len(sPrv_Res) > 0 then 
		oFileCurr.writeline (" ("  & sPrv_Res & ")")
	end if
	oFileCurr.writeline ("				</td>")
	oFileCurr.writeline ("			</tr>")
	oFileCurr.writeline ("		</table>")
	oFileCurr.writeline ("	</td>")
	oFileCurr.writeline ("</tr>")

end if
	
' RECAPITO TELEFONICO 
if aDati(5) = "1" and len(sNum_Tel) > 0 then
	oFileCurr.writeline ("<tr>")
	oFileCurr.writeline ("	<td background='/images/Curriculum/sfondomenudx1.gif' class='textblu' valign='top' align='LEFT' width='150'>Tel�fono para mensajes</td>")
	oFileCurr.writeline ("	<td class='textblu'>")
	oFileCurr.writeline ("		<table border=0>")
	oFileCurr.writeline ("			<tr>")
	oFileCurr.writeline ("				<td class=textblu width=5>&nbsp;</td>")
	oFileCurr.writeline ("				<td class='textblu'>")
	oFileCurr.writeline (Server.HTMLEncode(sNum_Tel))
	oFileCurr.writeline ("				</td>")
	oFileCurr.writeline ("			</tr>")
	oFileCurr.writeline ("		</table>")
	oFileCurr.writeline ("	</td>")
	oFileCurr.writeline ("</tr>")
end if

' RECAPITO TELEFONICO ALTERNATIVO
'if aDati(6) = "1" and len(aCV(4)) > 0 then
if aDati(6) = "1" and len(sNum_Tel2) > 0 then
	oFileCurr.writeline ("<tr>")
	oFileCurr.writeline ("	<td background='/images/Curriculum/sfondomenudx1.gif' class='textblu' valign='top' align='LEFT' width='150'>Otro tel�fono para mensajes</td>")
	oFileCurr.writeline ("	<td class='textblu'>")
	oFileCurr.writeline ("		<table border=0>")
	oFileCurr.writeline ("			<tr>")
	oFileCurr.writeline ("				<td class=textblu width=5>&nbsp;</td>")
	oFileCurr.writeline ("				<td class='textblu'>")
	'oFileCurr.writeline ( Server.HTMLEncode(aCV(4)) )
	oFileCurr.writeline ( Server.HTMLEncode(sNum_Tel2) )
	oFileCurr.writeline ("				</td>")
	oFileCurr.writeline ("			</tr>")
	oFileCurr.writeline ("		</table>")
	oFileCurr.writeline ("	</td>")
	oFileCurr.writeline ("</tr>")
end if

' FAX 
if aDati(7) = "1" and len(aCV(5)) > 0 then
	oFileCurr.writeline ("<tr>")
	oFileCurr.writeline ("	<td background='/images/Curriculum/sfondomenudx1.gif' class='textblu' valign='top' align='LEFT' width='150'>Fax</td>")
	oFileCurr.writeline ("	<td class='textblu'>")
	oFileCurr.writeline ("		<table border=0>")
	oFileCurr.writeline ("			<tr>")
	oFileCurr.writeline ("				<td class=textblu width=5>&nbsp;</td>")
	oFileCurr.writeline ("				<td class='textblu'>")
	oFileCurr.writeline ( Server.HTMLEncode(aCV(5)) )
	oFileCurr.writeline ("				</td>")
	oFileCurr.writeline ("			</tr>")
	oFileCurr.writeline ("		</table>")
	oFileCurr.writeline ("	</td>")
	oFileCurr.writeline ("</tr>")
end if

' EMAIL 
if len(sE_Mail) > 0 and aDati(4) = "1" then
	oFileCurr.writeline ("<tr>")
	oFileCurr.writeline ("	<td background='/images/Curriculum/sfondomenudx1.gif' class='textblu' valign='top' align='LEFT' width='150'>E-mail</td>")
	oFileCurr.writeline ("	<td class='textblu'>")
	oFileCurr.writeline ("		<table border=0>")
	oFileCurr.writeline ("			<tr>")
	oFileCurr.writeline ("				<td class=textblu width=5>&nbsp;</td>")
	oFileCurr.writeline ("				<td class='textblu'>")
	oFileCurr.writeline ( Server.HTMLEncode(sE_Mail) )
	oFileCurr.writeline ("				</td>")
	oFileCurr.writeline ("			</tr>")
	oFileCurr.writeline ("		</table>")
	oFileCurr.writeline ("	</td>")
	oFileCurr.writeline ("</tr>")
end if


' NAZIONALITA' 
sSCit = decCodVal("STATO","0","",sStat_Cit,"")
if Len(sStat_Cit) > 0 and aDati(1) = "1" and len(sSCit) > 0 then
	oFileCurr.writeline ("<tr>")
	oFileCurr.writeline ("	<td background='/images/Curriculum/sfondomenudx1.gif' class='textblu' valign='top' align='LEFT' width='150'>Ciudadania</td>")
	oFileCurr.writeline ("	<td class='textblu'>")
	oFileCurr.writeline ("		<table border=0>")
	oFileCurr.writeline ("			<tr>")
	oFileCurr.writeline ("				<td class=textblu width=5>&nbsp;</td>")
	oFileCurr.writeline ("				<td class='textblu'>")
	oFileCurr.writeline ( Trasforma(sSCit) )
	oFileCurr.writeline ("				</td>")
	oFileCurr.writeline ("			</tr>")
	oFileCurr.writeline ("		</table>")
	oFileCurr.writeline ("	</td>")
	oFileCurr.writeline ("</tr>")
end if

' DATA DI NASCITA
if aDati(2) = "1" and len(sDt_Nasc) > 0 then
	oFileCurr.writeline ("<tr>")
	oFileCurr.writeline ("	<td background='/images/Curriculum/sfondomenudx1.gif' class='textblu' valign='top' align='LEFT' width='150'>Fecha de Nacimiento</td>")
	oFileCurr.writeline ("	<td class='textblu'>")
	oFileCurr.writeline ("		<table border=0>")
	oFileCurr.writeline ("			<tr>")
	oFileCurr.writeline ("				<td class=textblu width=5>&nbsp;</td>")
	oFileCurr.writeline ("				<td class='textblu'>")
	oFileCurr.writeline ( sDt_Nasc )
	oFileCurr.writeline ("				</td>")
	oFileCurr.writeline ("			</tr>")
	oFileCurr.writeline ("		</table>")
	oFileCurr.writeline ("	</td> ")
	oFileCurr.writeline ("</tr>")
end if

' LUOGO DI NASCITA
' si indica il comune di nascita o lo stato di nascita.
sSNasc = DescrComune(sStat_Nasc)
sComNasc = DescrComune(sCom_Nasc)
if aDati(2) = "1" and (len(sComNasc) > 1 or len(sSNasc) > 1) then
	oFileCurr.writeline ("<tr>")
	oFileCurr.writeline ("	<td background='/images/Curriculum/sfondomenudx1.gif' class='textblu' valign='top' align='LEFT' width='150'>Lugar de Nacimiento</td>")
	oFileCurr.writeline ("	<td class='textblu'>")
	oFileCurr.writeline ("		<table border=0>")
	oFileCurr.writeline ("			<tr>")
	oFileCurr.writeline ("				<td class=textblu width=5>&nbsp;</td>")
	oFileCurr.writeline ("				<td class='textblu'>")
	if len(sComNasc) > 1 then
		oFileCurr.writeline ( Trasforma(sComNasc))
	else
		oFileCurr.writeline ( Trasforma(sSNasc) )
	end if 
	if len(sPrv_Nasc) > 0 then
		oFileCurr.writeline ( " (" & sPrv_Nasc & ")" )
	end if
	oFileCurr.writeline ("				</td>")
	oFileCurr.writeline ("			</tr>")
	oFileCurr.writeline ("		</table>")
	oFileCurr.writeline ("	</td>")
	oFileCurr.writeline ("</tr>")
end if

' STATO CIVILE 
sSCiv = decCodVal("STCIV","0","",sStat_Civ,"")
if len(sStat_Civ) > 0 and aDati(0) = "1" and len(sSCiv) > 0 then
	oFileCurr.writeline ("<tr>")
	oFileCurr.writeline ("	<td background='/images/Curriculum/sfondomenudx1.gif' class='textblu' valign='top' align='LEFT' width='150'>Estado Civil</td>")
	oFileCurr.writeline ("	<td class='textblu'>")
	oFileCurr.writeline ("		<table border=0>")
	oFileCurr.writeline ("			<tr>")
	oFileCurr.writeline ("				<td class=textblu width=5>&nbsp;</td>")
	oFileCurr.writeline ("				<td class='textblu'>")
	oFileCurr.writeline ( Trasforma(sSCiv) )
	oFileCurr.writeline ("				</td>")
	oFileCurr.writeline ("			</tr>")
	oFileCurr.writeline ("		</table>")
	oFileCurr.writeline ("	</td>")
	oFileCurr.writeline ("</tr>")
end if
	
' (RKL)05/07 Sacar del CV la opci�n Servicio Militar 
	
' OBBLIGO DI LEVA
'if len(sPos_Mil) > 0 and aDati(8) = "1" then
'	oFileCurr.writeline ("<tr>")
'	oFileCurr.writeline ("	<td background='/images/Curriculum/sfondomenudx1.gif' class='textblu' valign='top' align='LEFT' width='150'>Servicio Militar</td>")
'	oFileCurr.writeline ("	<td class='textblu'>")
'	oFileCurr.writeline ("		<table border=0>")
'	oFileCurr.writeline ("			<tr>")
'	oFileCurr.writeline ("				<td class=textblu width=5>&nbsp;</td>")
'	oFileCurr.writeline ("				<td class='textblu'>")
'	oFileCurr.writeline ( Trasforma(decCodVal("POMIL","0","",sPos_Mil,"")) )
'	oFileCurr.writeline ("				</td>")
'	oFileCurr.writeline ("			</tr>")
'	oFileCurr.writeline ("		</table>")
'	oFileCurr.writeline ("	</td>")
'	oFileCurr.writeline ("</tr>")
'end if

' ESPERIENZE PROFESSIONALI
if nSezioni > 2 then
	if aSezioni(3) = "1" and len(aCV(10)) > 0 then
		if IsArray(Split(aCV(10),"")) and aCV(10) <> "0" then
			aPro = Split(aCV(10),"")
			sIn = "("
			for cont = 0 to ubound(aPro)
				sMansione = Split(aPro(cont),"$",-1,1)(0)
				sIn = sIn & "'" &  sMansione & "',"
			next
			sIn = mid(sIn,1,len(sIn)-1)
			sIn = sIn & ")"
			
			sqlEuropeo = "SELECT ESPRO.RAG_SOC AS AZIENDA, ESPRO.DT_INI_ESP_PRO AS DI, nvl(ESPRO.DT_FIN_ESP_PRO,to_Date('31/12/9999','dd/mm/yyyy')) AS DF, ESPRO.COD_MANSIONE AS MAN " &_ 
						" FROM ESPRO " &_ 
						" WHERE ID_PERSONA = " & id_utente &_ 
						" AND ESPRO.COD_MANSIONE IN " & sIn  &_
						" ORDER BY DT_FIN_ESP_PRO desc, ESPRO.DT_INI_ESP_PRO desc "
'PL-SQL * T-SQL  
SQLEUROPEO = TransformPLSQLToTSQL (SQLEUROPEO) 
			RREuropeo.Open sqlEuropeo, CC
			if  not RREuropeo.eof then
				oFileCurr.writeline ("<tr>")
				oFileCurr.writeline ("	<td background='/images/Curriculum/sfondomenudx1.gif' class='textblubold' align='LEFT' valign='top' width='150'><b><br>ANTECEDENTES LABORALES</b></td>")
				oFileCurr.writeline ("	<td>&nbsp;</td>")
				oFileCurr.writeline ("</tr>")
				do while not RREuropeo.eof
					oFileCurr.writeline ("<tr>")
					oFileCurr.writeline ("	<td background='/images/Curriculum/sfondomenudx1.gif' class='textblu' valign='top' align='LEFT' width='150'>")
					oFileCurr.writeline ("		<li>dal " & mid(ConvDateToString(RREuropeo.Fields("DI").value),4) )
					if RREuropeo.Fields("DF") <> ConvStringToDate("31/12/9999") then
						oFileCurr.writeline (" al "  & mid(ConvDateToString(RREuropeo.Fields("DF").value),4) )
					else
						oFileCurr.writeline (" a OGGI ")
					end if
					oFileCurr.writeline ("	</td>")
					oFileCurr.writeline ("	<td class='textblu'>")
					oFileCurr.writeline ("		<table border=0><tr><td valign=top width=5><li class='tratto'>&nbsp;</li><TD valign=top  class=textblu>")
					if RREuropeo.Fields("MAN") <> "" then
						sMansi = "SELECT MANSIONE FROM CODICI_ISTAT WHERE CODICE='" & RREuropeo.Fields("MAN") & "'"
'PL-SQL * T-SQL  
SMANSI = TransformPLSQLToTSQL (SMANSI) 
						set rsMansi = cc.execute(sMansi)
						if not rsMansi.eof then		
							oFileCurr.writeline ("in qualit� di " & lcase(TRIM(rsMansi("MANSIONE"))) )
							rsMansi.close
						end if
						set rsMansi=nothing
					end if
					oFileCurr.writeline ("		</TD></TR></TABLE>")
					oFileCurr.writeline ("	</td>")
					oFileCurr.writeline ("</tr>	")		
					if RREuropeo.Fields("AZIENDA") <> ""  and ucase(RREuropeo.Fields("AZIENDA")) <> "NON DICHIARATA" then
						oFileCurr.writeline ("<tr>")
						oFileCurr.writeline ("	<td background='/images/Curriculum/sfondomenudx1.gif' class='textblu' valign='top' align='LEFT' width='150'>")
						oFileCurr.writeline ("		<table width='120' align='center' border='0'><tr><td class='textblu'>Empresa</td></tr></table>")
						oFileCurr.writeline ("	</td>")
						oFileCurr.writeline ("	<td class='textblu'>")
						oFileCurr.writeline ("		<table border=0>")
						oFileCurr.writeline ("			<tr>")
						oFileCurr.writeline ("				<td width=18 class=textblu>&nbsp;</td>")
						oFileCurr.writeline ("				<td class=textblu>" & Trasforma(RREuropeo.Fields("AZIENDA")) & " </td>")
						oFileCurr.writeline ("			</tr>")
						oFileCurr.writeline ("		</table>")
						oFileCurr.writeline ("	</td>")
						oFileCurr.writeline ("</tr>")
					end if
					RREuropeo.MoveNext
				loop
			end if	
			RREuropeo.Close
			sqlEuropeo = ""
		end if
	end if
end if

' ISTRUZIONE E FORMAZIONE'
sCondizione = false
if nSezioni > 8 then
	if (aSezioni(9) = "1" and len(aCV(15)) > 0) OR (aSezioni(2) = "1" and len(aCV(9)) > 0) then
		sCondizione = true
	end if

elseif nSezioni > 1 then
	if (aSezioni(2) = "1" and len(aCV(9)) > 0) then
		sConsizione = true
	end if
end if

if sCondizione then
	oFileCurr.writeline ("	<tr>")
	oFileCurr.writeline ("		<td background='/images/Curriculum/sfondomenudx1.gif' class='textblubold' align='LEFT' valign='top' width='150'><b><br>Educaci�n y Formaci�n</b></td>")
	oFileCurr.writeline ("		<td>&nbsp;</td>")
	oFileCurr.writeline ("	</tr>")

	' TITOLI DI STUDIO
	if aSezioni(2) = "1" and len(aCV(9)) > 0 then
		if IsArray(Split(aCV(9),"")) and aCV(9) <> "0" then
			aFor = Split(aCV(9),"|")
			sDatiTitoli = "'" & Replace(aCV(9),"|","','") & "'"
			oFileCurr.writeline ("<tr>")
			oFileCurr.writeline ("	<td background='/images/Curriculum/sfondomenudx1.gif' class='textblu' valign='top' align='LEFT' width='150'>NIVEL EDUCATIVO</td>")
			oFileCurr.writeline ("	<td class='textblu'>&nbsp;</td>")
			oFileCurr.writeline ("</tr>")

			sqlEuropeo = "SELECT AA_STUD, COD_STAT_STUD, COD_TIT_STUD, DT_TMST" &_ 
						" FROM TISTUD" &_ 
						" WHERE ID_PERSONA = " & id_utente &_
						" AND COD_TIT_STUD IN (" & sDatiTitoli & ")" &_
						" ORDER BY AA_STUD DESC ,COD_STAT_STUD"
'PL-SQL * T-SQL  
SQLEUROPEO = TransformPLSQLToTSQL (SQLEUROPEO) 
			RREuropeo.Open sqlEuropeo, CC				
			nAnno = 0000
			if  not RREuropeo.eof then
				do while not RREuropeo.eof
					Anio = RREuropeo.Fields("AA_STUD")
					if (not isnull(Anio)) and (Anio <> "") then
						Anio = clng(Anio)
					end if
					
					if Anio <> clng(nAnno) then
						if clng(nAnno) <> 0000 then
							oFileCurr.writeline ("</td></tr>")
						end if
						oFileCurr.writeline ("<tr>" &_ 
										" <td background='/images/Curriculum/sfondomenudx1.gif' class='textblu' valign='top' align='LEFT' width='150'>" &_
										"	<li>" & RREuropeo.Fields("AA_STUD") & "</li>" &_
										" </td>" &_
										" <td class='textblu'>")
						nAnno = RREuropeo.Fields("AA_STUD")
					end if
					oFileCurr.writeline ("<TABLE border=0>" &_ 
									" <TR>" &_ 
									"  <TD valign=top width=5><li class=tratto>&nbsp;</LI></TD>" &_ 
									"  <TD valign=top  class=textblu>" &  Trasforma(DecCodVal("TSTUD","0",RREuropeo.Fields("DT_TMST"),RREuropeo.Fields("COD_TIT_STUD"),0))  &_ 
										"(" & decDiz_Dati(RREuropeo.Fields("COD_STAT_STUD"), "COD_STAT_STUD") & ")" &_
									"  </TD>" &_
									" </TR>" &_
									"</TABLE>")
					RREuropeo.MoveNext
				loop
			end if
			RREuropeo.Close
			sqlEuropeo = ""
		end if
		oFileCurr.writeline ("</td>")
		oFileCurr.writeline ("</tr>")
	end if
end if

	' ISCRIZIONE ALBO 
if nSezioni > 9 then
	if aSezioni(10) = "1" and len(aCV(16)) > 0 then
		if IsArray(Split(aCV(16),"|")) and aCV(16) <> "0" then
			aFor = Split(aCV(16),"|")
			sDatiTitoli = "'" & Replace(aCV(16),"|","','") & "'"
			oFileCurr.writeline ("	<tr>")
			oFileCurr.writeline ("		<td background='/images/Curriculum/sfondomenudx1.gif' class='textblubold' align='LEFT' valign='top' width='150'><b><br>MATRICULA PROFESIONAL</b></td>")
			oFileCurr.writeline ("		<td>&nbsp;</td>")
			oFileCurr.writeline ("	</tr>")

			sqlEuropeo = "SELECT COD_ALBO,DT_DEC_ISCR,PRV_ISCR,NUM_ISCR, DT_TMST " &_
				" FROM PERS_ALBO WHERE ID_PERSONA = " & id_utente &_ 
				" AND COD_ALBO IN (" & sDatiTitoli & ") " &_
				" ORDER BY DT_DEC_ISCR DESC "
'PL-SQL * T-SQL  
SQLEUROPEO = TransformPLSQLToTSQL (SQLEUROPEO) 
			RREuropeo.Open sqlEuropeo, CC								
					
			if  not RREuropeo.eof then
				do while not RREuropeo.eof	
					oFileCurr.writeline ("</td></tr>")
					oFileCurr.writeline ("<tr>" &_ 
						" <td background='/images/Curriculum/sfondomenudx1.gif' class='textblu' valign='top' align='LEFT' width='150'>" &_
						"	<li>" & Trasforma(DecCodVal("TALBO","0",RREuropeo.Fields("DT_TMST"),RREuropeo.Fields("COD_ALBO"),0)) & "</li>" &_
						" </td>" &_
						" <td class='textblu'>")

						oFileCurr.writeline ("<TABLE border=0>" &_ 
							" <TR>" &_ 
							"  <TD valign=top width=5><li class=tratto>&nbsp;</LI></TD>" &_ 
							"  <TD valign=top  class=textblu>Inscripci�n n� " & RREuropeo.Fields("NUM_ISCR") & " el " & ConvDateToString(RREuropeo.Fields("DT_DEC_ISCR"))  &_ 
								"(" & RREuropeo.Fields("PRV_ISCR") & ")" &_
							"  </TD>" &_
							" </TR>" &_
							"</TABLE>")
				

					RREuropeo.MoveNext
				loop
										
				end if
				RREuropeo.close
				sqlEuropeo = ""
			end if
		oFileCurr.writeline ("</td></tr>")
	end if
end if	

'FORMAZIONE
if nSezioni > 8 then
	if aSezioni(9) = "1" and len(aCV(15)) > 0 then
		if IsArray(Split(aCV(15),"|")) and aCV(15) <> "0" then
			aFormaz = Split(aCV(15),"|")
			sFormazione = "'" & Replace(aCV(15),"|","','") & "'"
			oFileCurr.writeline ("<tr>")
			oFileCurr.writeline ("	<td background='/images/Curriculum/sfondomenudx1.gif' class='textblu' valign='top' align='LEFT' width='150'>CAPACITACI�N Y FORMACI�N PROFESIONAL</td>")
			oFileCurr.writeline ("	<td class='textblu'>&nbsp;</td>")
			oFileCurr.writeline ("</tr>")
	
			sqlEuropeo = "SELECT F.ID_FORMAZIONE, F.COD_TCORSO, F.AA_CORSO, F.COD_STATUS_CORSO, A.DENOMINAZIONE " & _
						" FROM FORMAZIONE F, AREA_CORSO A " & _
						" WHERE A.ID_AREA = F.ID_AREA " & _
						" AND ID_PERSONA = " & id_utente &_
						" AND ID_FORMAZIONE IN (" & sFormazione & ")" &_
						" ORDER BY F.AA_CORSO DESC"
'PL-SQL * T-SQL  
SQLEUROPEO = TransformPLSQLToTSQL (SQLEUROPEO) 
			RREuropeo.Open sqlEuropeo, CC
		
			if  not RREuropeo.eof then
				nAnno = "0000"
				do while not RREuropeo.eof							
					'if clng(RREuropeo.Fields("AA_CORSO")) <> clng(nAnno) then
						if nAnno <> "0000" then
							oFileCurr.writeline ("</td></tr>")
						end if
						oFileCurr.writeline ("<tr>" &_ 
											" <td background='/images/Curriculum/sfondomenudx1.gif' class='textblu' valign='top' align='LEFT' width='150'>" &_
											"	<li>" & RREuropeo.Fields("AA_CORSO") & "</li>" &_
											" </td>" &_
											" <td class='textblu'>")
						nAnno = RREuropeo.Fields("AA_CORSO")
					'end if
					oFileCurr.writeline ("<TABLE border=0>" &_ 
										" <TR>" &_ 
										"  <TD valign=top width=5><li class=tratto>&nbsp;</LI></TD>" &_ 
										"  <TD valign=top  class=textblu>" &  Trasforma(RREuropeo.Fields("DENOMINAZIONE")) &_ 
										" - " & LCASE(RREuropeo.Fields("COD_TCORSO")) &_
										" (")
					select case RREuropeo.Fields("COD_STATUS_CORSO")
						case "0"
							oFileCurr.writeline ("Complet�")
						case "1"
							oFileCurr.writeline ("Abandon�")
						case "2"
							oFileCurr.writeline ("En curso")
					end select
					oFileCurr.writeline (")" &_
										"  </TD>" &_
										" </TR>" &_
										"</TABLE>")
					RREuropeo.MoveNext
				loop
				oFileCurr.writeline ("</td></tr>")
			end if
			RREuropeo.Close
			sqlEuropeo = ""
			oFileCurr.writeline ("	</td>")
			oFileCurr.writeline ("</tr>	")

		end if
	end if
End if
	
'CAPACITA' E COMPETENZE PERSONALI	
sCondizione = false
if nSezioni > 7 then
	if (aSezioni(1) = "1" and len(aCV(8)) > 0) or (aSezioni(6) = "1" and len(aCV(12)) > 0) or (aSezioni(7) = "1" and len(aCV()) > 0) or (aSezioni(8) = "1" and len(aCV(14)) > 0) then
		sCondizione = true
	end if

elseif nSezioni > 6 then
		if (aSezioni(1) = "1" and len(aCV(8)) > 0) or (aSezioni(6) = "1" and len(aCV(12)) > 0) or (aSezioni(7) = "1" and len(aCV(13)) > 0) then
			sConsizione = true
		end	if
	elseif nSezioni > 5 then
			if (aSezioni(1) = "1" and len(aCV(8)) > 0) or (aSezioni(6) = "1" and len(aCV(12)) > 0) then
				sConsizione = true
			end if
		elseif nSezioni > 0 then
				if (aSezioni(1) = "1" and len(aCV(8)) > 0)then
					sConsizione = true
				end if
	
end if

if sCondizione then
	oFileCurr.writeline ("<tr>")
	oFileCurr.writeline ("	<td background='/images/Curriculum/sfondomenudx1.gif' class='textblubold' align='LEFT' valign='top' width='150'><b><br>DESEMPE�O Y COMPETENCIAS</b></td>")
	oFileCurr.writeline ("	<td>&nbsp;</td>")
	oFileCurr.writeline ("</tr>	")

	' LINGUE 
		if aSezioni(1) = "1" and len(aCV(8)) > 0 then
			if IsArray(Split(aCV(8),"|")) and aCV(8) <> "0" then
				aDatiLingue = Split(aCV(8),"|")
				oFileCurr.writeline ("<tr>")
				oFileCurr.writeline ("	<td background='/images/Curriculum/sfondomenudx1.gif' class='textblu' valign='top' align='LEFT' width='150'>IDIOMAS</td>")
				oFileCurr.writeline ("	<td class='textblu'>")
 					sDatiLingue = Replace(aCV(8),"|",",")
					sqlEuropeo = "SELECT CONOSCENZE.DENOMINAZIONE, PERS_CONOSC.COD_GRADO_CONOSC " &_ 
								" FROM CONOSCENZE, PERS_CONOSC " &_ 
								" WHERE CONOSCENZE.ID_CONOSCENZA = PERS_CONOSC.ID_CONOSCENZA " &_ 
								" AND PERS_CONOSC.ID_PERSONA = " & id_utente &_ 
								" AND PERS_CONOSC.IND_STATUS = 0 " &_ 
								" AND PERS_CONOSC.ID_CONOSCENZA IN (" & sDatiLingue & ")" &_
								" ORDER BY CONOSCENZE.DENOMINAZIONE"
								
'PL-SQL * T-SQL  
SQLEUROPEO = TransformPLSQLToTSQL (SQLEUROPEO) 
					RREuropeo.Open sqlEuropeo, CC
					if not RREuropeo.eof then
						do while not RREuropeo.eof
							oFileCurr.writeline ("<TABLE border=0><TR><TD valign=top width=5><li class=tratto>&nbsp;</LI></TD><TD valign=top  class=textblu>" &_
													Trasforma(RREuropeo.Fields("DENOMINAZIONE")) & " (" & decGradoConosc(CInt(RREuropeo.Fields("COD_GRADO_CONOSC"))) & ")" &_
													"</TD></TR></TABLE>")
							RREuropeo.MoveNext
						loop
					end if
					RREuropeo.Close
					sqlEuropeo = ""
				oFileCurr.writeline ("	</td>")
				oFileCurr.writeline ("</tr>")
			end if
		end if
END IF
	
' CONOSCENZE INFORMATICHE
if nSezioni > 5 then
	if aSezioni(6) = "1" and len(aCV(12)) > 0 then
		if IsArray(Split(aCV(12),"|")) and aCV(12) <> "0" then
			aDatiLingue = Split(aCV(12),"|")
			oFileCurr.writeline ("<tr>")
			oFileCurr.writeline ("	<td background='/images/Curriculum/sfondomenudx1.gif' class='textblu' valign='top' align='LEFT' width='150'><br>CONOCIMIENTOS INFORM�TICOS</td>")
			oFileCurr.writeline ("	<td class='textblu'><br>")

			sDatiLingue = Replace(aCV(12),"|",",")
			sqlEuropeo = "SELECT CONOSCENZE.DENOMINAZIONE, PERS_CONOSC.COD_GRADO_CONOSC " &_ 
						" FROM CONOSCENZE, PERS_CONOSC " &_ 
						" WHERE CONOSCENZE.ID_CONOSCENZA = PERS_CONOSC.ID_CONOSCENZA " &_ 
						" AND PERS_CONOSC.ID_PERSONA = " & id_utente &_ 
						" AND PERS_CONOSC.IND_STATUS = 0 " &_ 
						" AND PERS_CONOSC.ID_CONOSCENZA IN (" & sDatiLingue & ")" &_
						" ORDER BY CONOSCENZE.DENOMINAZIONE"
'PL-SQL * T-SQL  
SQLEUROPEO = TransformPLSQLToTSQL (SQLEUROPEO) 
			RREuropeo.Open sqlEuropeo, CC
			if not RREuropeo.eof then
				do while not RREuropeo.eof
					oFileCurr.writeline ("<TABLE border=0><TR><TD valign=top align=left width=5><li class=tratto>&nbsp;</LI></TD><TD valign=top  class=textblu>" &_
											 Trasforma(RREuropeo.Fields("DENOMINAZIONE")) & " (" & decGradoConosc(CInt(RREuropeo.Fields("COD_GRADO_CONOSC"))) & ")" &_
											 "</TD></TR></TABLE>")
					RREuropeo.MoveNext
				loop
			end if
			RREuropeo.Close
			sqlEuropeo = ""
			oFileCurr.writeline ("</td>")
			oFileCurr.writeline ("</tr>")
		end if
	end if
end if

'PATENTI E CERTIFICAZIONI'
if nSezioni > 7 then
	if aSezioni(8) = "1" and len(aCV(14)) > 0 then
		if IsArray(Split(aCV(14),"|")) and aCV(14) <> "0" then
			aPat = Split(aCV(14),"|")
			sPatenti = "'" & Replace(aCV(14),"|","','") & "'"
			oFileCurr.writeline ("<tr>")
			oFileCurr.writeline ("	<td background='/images/Curriculum/sfondomenudx1.gif' class='textblu' valign='top' align='LEFT' width='150'><br>PERMISOS/LICENCIAS</td>")
			oFileCurr.writeline ("	<td class='textblu'><br>")
			sqlEuropeo = "SELECT COD_PERM, DT_TMST" & _
						" FROM AUTCERT " & _
						" WHERE ID_PERSONA = " & id_utente & _
						" AND COD_PERM IN (" & sPatenti & ")" &_
						" AND nvl(DT_SCADENZA, to_date('31/12/9999', 'dd/mm/yyyy')) >= " & convDateToDB(date)
				
'PL-SQL * T-SQL  
SQLEUROPEO = TransformPLSQLToTSQL (SQLEUROPEO) 
			RREuropeo.Open sqlEuropeo, CC		

			if  not RREuropeo.eof then
				do while not RREuropeo.eof
					oFileCurr.writeline ("<TABLE border=0><TR><TD valign=top width=5><li class=tratto>&nbsp;</LI></TD><TD valign=top  class=textblu>" &_
											Trasforma(DecCodVal("TPERM","0",RREuropeo.Fields("DT_TMST"),RREuropeo.Fields("COD_PERM"),0)) &_
											"</TD></TR></TABLE>")
					RREuropeo.MoveNext
				loop
			end if
			RREuropeo.Close
			sqlEuropeo = ""
			oFileCurr.writeline ("	</td>")
			oFileCurr.writeline ("</tr>")
		end if
	end if
end if			
			
	' DISPONIBILITA' 
	
if nSezioni > 6 then	
	if aSezioni(7) = "1" and len(aCV(13)) > 0 then
		if IsArray(Split(aCV(13),"|")) and aCV(13) <> "0" then
			sDisponibilita = "'" & Replace(aCV(13),"|","','") & "'"
			sCondizione = "codice IN (" & sDisponibilita & ")"
			sTabella = "TDISP"
			dData	 = Date()
			nOrder   = 2
			Isa		 = 0
			aDisponibilita = decodTadesToArray(sTabella,dData,sCondizione,nOrder,Isa)
			if isArray(aDisponibilita) then
				oFileCurr.writeline ("<tr>")
				oFileCurr.writeline ("	<td background='/images/Curriculum/sfondomenudx1.gif' class='textblubold' align='LEFT' valign='top' width='150'><b><br>DISPONIBILIDADES/LIMITACIONES</b></td>")
				oFileCurr.writeline ("	<td>&nbsp;</td>")
				oFileCurr.writeline ("</tr>	")
		
				dim nCodArea, sbr
				nCodArea = 00
				
				for i = 0 to ubound(aDisponibilita) -1						
					if clng(aDisponibilita(i,i+1,i+1)) <> clng(nCodArea) then
							if clng(nCodArea) <> 00 then
								oFileCurr.writeline ("</td></tr>")
							end if
							oFileCurr.writeline ("<tr>" &_ 
													" <td background='/images/Curriculum/sfondomenudx1.gif' class='textblu' valign='top' align='LEFT' width='150'>" &_
													"	<br>" & Trasforma(DecCodVal("CL_VD", 0, Date(),aDisponibilita(i,i+1,i+1),0))  &_
													" </td>" &_
													" <td class='textblu'><br>")
							nCodArea = aDisponibilita(i,i+1,i+1)
						end if
						oFileCurr.writeline ("<TABLE border=0>" &_ 
												"<TR>" &_ 
												"  <TD valign=top width=5><li class=tratto>&nbsp;</LI></TD>" &_ 
												"  <TD valign=top  class=textblu>" &  Trasforma(aDisponibilita(i,i+1,i)) & "</TD>" &_
												"</TR>" &_
											 "</TABLE>")
				next
				oFileCurr.writeline ("</td></tr>")
				Erase aDisponibilita
			end if
		end if
	end if
end if

			
' PRESENTAZIONE 
if nSezioni <> -1 then
	if aSezioni(0) = "1" and len(aCV(6)) > 0 then
		oFileCurr.writeline ("<tr>")
		oFileCurr.writeline ("	<td background='/images/Curriculum/sfondomenudx1.gif' class='textblubold' align='LEFT' valign='top' width='150'><b><br>PRESENTACION</b></td>")
		oFileCurr.writeline ("	<td class='textblu'>")
		oFileCurr.writeline ("		<table border=0>")
		oFileCurr.writeline ("			<td width=5 class=textblu>&nbsp;</td>")
		oFileCurr.writeline ("			<td class='textblu'>")
		oFileCurr.writeline ("				<br>" & Server.HTMLEncode(aCV(6)) )
		oFileCurr.writeline ("			</td>")
		oFileCurr.writeline ("		</table>")
		oFileCurr.writeline ("	</td>")
		oFileCurr.writeline ("</tr>")

	end if
end if

' OBIETTIVI 
if nSezioni > 3 then
	if aSezioni(4) = "1" and len(aCV(7)) > 0 then
		oFileCurr.writeline ("<tr>")
		oFileCurr.writeline ("	<td background='/images/Curriculum/sfondomenudx1.gif' class='textblubold' align='LEFT' valign='top' width='150'><b><br>OBJETIVOS</b></td>")
		oFileCurr.writeline ("	<td class='textblu'>")
		oFileCurr.writeline ("		<table border=0>")
		oFileCurr.writeline ("			<td width=5 class=textblu>&nbsp;</td>")
		oFileCurr.writeline ("			<td class='textblu'>")
		oFileCurr.writeline ("				<br>" & Server.HTMLEncode(aCV(7)) )
		oFileCurr.writeline ("			</td>")
		oFileCurr.writeline ("		</table>")
		oFileCurr.writeline ("	</td>")
		oFileCurr.writeline ("</tr>")
	end if
end if

' ALTRE INFO '
if nSezioni > 4 then
	if aSezioni(5) = "1" then
		if IsArray(Split(aCV(11),"|")) and len(aCV(11)) > 0 then
			oFileCurr.writeline ("<tr>")
			oFileCurr.writeline ("	<td background='/images/Curriculum/sfondomenudx1.gif' class='textblubold' align='LEFT' valign='top' width='150'><b><br>INFORMACION ADICIONAL</b></td>")
			oFileCurr.writeline ("	<td class='textblu'>&nbsp;</td>")
			oFileCurr.writeline ("</tr>")
			
			oFileCurr.writeline ("<tr>")
			oFileCurr.writeline ("	<td background='/images/Curriculum/sfondomenudx1.gif' class='textblu' valign='top' align='LEFT' width='150'>&nbsp;</td>")
			oFileCurr.writeline ("	<td class='textblu'>")
			aInfo = Split(aCV(11),"|")
			for cont = 0 to UBound(aInfo)
				oFileCurr.writeline ("		<TABLE border=0><TR><TD valign=top width=5><li class=tratto>&nbsp;</LI></TD><TD valign=top  class=textblu>" &_
											Server.HTMLEncode(aInfo(cont)) &_
											"</TD></TR></TABLE>")
			next
			oFileCurr.writeline ("	</td>")
			oFileCurr.writeline ("</tr>")
		end if
	end if
end if

' PRIVACY 
oFileCurr.writeline ("		<tr>")
oFileCurr.writeline ("			<td class='textblu' colspan='2'>&nbsp;</td>")
oFileCurr.writeline ("		</tr>")
oFileCurr.writeline ("		<tr>")
oFileCurr.writeline ("			<td class='textblu' colspan='2'>")
'oFileCurr.writeline ("				<b>Dichiaro di essere stato informato, ai sensi e per gli effetti di cui all'art'10 della legge 675/96 che i dati personali raccolti saranno trattati, anche con strumenti informatici, esclusivamente nell'ambito del procedimento per il quale la presente dichiarazione viene resa.<b>")
'oFileCurr.writeline ("				<b>Declaro estar informado, de acuerdo a la Res. Nro. que determina que los datos registrados sera utilizados exclusivamente en el ambito para el cual la presente declaraci�n establece.<b>")
oFileCurr.writeline ("			</td>")
oFileCurr.writeline ("		</tr>")

' FINE CAMPI 
oFileCurr.writeline ("</table>")
oFileCurr.writeline ("	</td>")
oFileCurr.writeline ("</tr>")
 
'Rilascio il recordset
set RREuropeo = nothing
%>
