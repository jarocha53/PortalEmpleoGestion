

<%
Dim sqlInformale	'stringa sql di esexcuzione
Dim RRInformale	'recordset utilizzato per tutte le query
set RRInformale = Server.CreateObject("ADODB.Recordset")
aDati = Split(aCV(2),"|")
aSezioni = Split(aCV(3),"|")
nSezioni = ubound(aSezioni)
call getDatiDB(id_utente)


	oFileCurr.writeline ("<tr>")
	oFileCurr.writeline ("	<td align='center'>")
	'oFileCurr.writeline ("		<SPAN class='textblutitolo'><br>Hoja de Vida<br></SPAN>")
	'oFileCurr.writeline ("		<SPAN class='textblubold'>de</SPAN>")
	oFileCurr.writeline ("		<SPAN class='textblunome'><br>" & sNome & " " & sSNome & " " & sCognome & " " & sSCognome & "</SPAN>")
	oFileCurr.writeline ("	</td>")
	oFileCurr.writeline ("</tr>")
	oFileCurr.writeline ("<tr><td>&nbsp;</td></tr>")

	oFileCurr.writeline ("<tr>")
	oFileCurr.writeline ("	<td>")
	if STAMPA = "SI" then 
		oFileCurr.writeline ("<table border='0' cellspacing='0' cellpadding='5' align=center width=600>")
	else 
		oFileCurr.writeline ("<table border='0' cellspacing='2' cellpadding='3' width='100%'>")
	end if 
	
if aDati(0) = "1" or aDati(1) = "1" or aDati(2) = "1" or _
	aDati(3) = "1" or aDati(4) = "1" or aDati(5) = "1" or _
	aDati(6) = "1" or aDati(7) = "1" or aDati(8) = "1" then
				
	oFileCurr.writeline ("<tr>")
	oFileCurr.writeline ("	<td class='textblubold' colspan=2><B>INFORMACI&Oacute;N PERSONAL</B><br><HR></td>")
	oFileCurr.writeline ("</tr>")
	oFileCurr.writeline ("<tr>")
	oFileCurr.writeline ("	<td class='textblu' width=20>&nbsp</td>")
	oFileCurr.writeline ("	<td class='textblu'><ul>")

	' DOCUMENTO DE IDENTIDAD
		sCod_Docst = left(sCod_Fisc,2)
		if sCod_Docst="CC" then
			sCod_Docst= "C�DULA DE CIUDADAN�A"
		elseif sCod_Docst="PA" then
			sCod_Docst= "PASAPORTE"
		elseif sCod_Docst="TI" then
			sCod_Docst= "TARJETA DE IDENTIDAD"
		else
			sCod_Docst= "C�DULA DE EXTRANJERIA"
		end if
			
		oFileCurr.writeline ("<LI> " & sCod_Docst & ": " & mid(sCod_Fisc,3,len(sCod_Fisc)-2) & "</LI>")
	
	' ESTADO CIVIL 
		sSCiv = decCodVal("STCIV","0","",sStat_Civ,"")
		if len(sStat_Civ) > 0 and aDati(0) = "1" and len(sSCiv) > 0 then
			oFileCurr.writeline ("<LI>Estado Civil: " & sSCiv & "</LI>")
		end if
	
	' NACIONALIDAD
		sSCit = decCodVal("STATO","0","",sStat_Cit,"") 
		if Len(sStat_Cit) > 0 and aDati(1) = "1" and len(sSCit) > 0 then
			oFileCurr.writeline ("<LI>Nacionalidad: " & sSCit & "</LI>")
		end if

	' FECHA DE NACIMIENTO 
		if aDati(2) = "1" and len(sDt_Nasc) > 0 then
			oFileCurr.writeline ("<LI>Fecha de Nacimiento: " & sDt_Nasc & "</LI>")
		end if
	
	' LUGAR DE NACIMIENTO 
		sSNasc = DescrComune(sStat_Nasc)
		sComNasc = DescrComune(sCom_Nasc)
		sPrvNasc = DescrProvincia2(sPrv_Nasc)
		if aDati(2) = "1" and (len(sComNasc) > 1 or len(sSNasc) > 1) then
			oFileCurr.writeline ("<LI>Lugar de Nacimiento: ")
			if len(sComNasc) > 1 then
				oFileCurr.writeline ( Trasforma(sComNasc))
			else
				oFileCurr.writeline ( Trasforma(sSNasc))
			end if
			if len(sPrv_Nasc) > 0 then
				oFileCurr.writeline (" (" & sPrvNasc & ")")
			end if
			oFileCurr.writeline ("</LI>")
		end if

	' RESIDENCIA 
		if aDati(3) = "1" then
			oFileCurr.writeline ("<LI>Domicilio:")
			if len(sInd_Res) > 0 then 
				oFileCurr.writeline (trasforma(sInd_Res) & " - ")
			end if
			if len(sCap_Res) > 0 then
				oFileCurr.writeline (sCap_Res & " ")
			end if 
			if len(DescrComune(sCom_Res)) > 1 then 
				oFileCurr.writeline (Trasforma(DescrComune(sCom_Res)))
				if len(sPrv_Res) > 0 then
					oFileCurr.writeline (" (" & Trasforma(DescrProvincia2(sPrv_Res)) & ")")
				end if 
			end if
			oFileCurr.writeline ("</LI>")
		end if

	' TEL�FONO FIJO 
		if aDati(5) = "1" and len(sNum_Tel) > 0 then
			oFileCurr.writeline ("<LI>Nro. Tel�fono : " & Server.HTMLEncode(sNum_Tel) & "</LI>")
		end if

	' TEL�FONO CELULAR 
		'if aDati(6) = "1" and len(aCV(4)) > 0 then
		if aDati(6) = "1" and len(sNum_Tel2) > 0 then
			'oFileCurr.writeline ("<LI>Nro. Tel�fono alternativo: " & Server.HTMLEncode(aCV(4)) & "</LI>")
			oFileCurr.writeline ("<LI>Nro. Tel�fono Celular: " & Server.HTMLEncode(sNum_Tel2) & "</LI>")
		end if
	
	' FAX 
		if aDati(7) = "1" and len(aCV(5)) > 0 then
			oFileCurr.writeline ("<LI>Fax: " & Server.HTMLEncode(aCV(5)) & "</LI>")
		end if

	' EMAIL 
		if len(sE_Mail) > 0 and aDati(4) = "1" then
			oFileCurr.writeline ("<LI>Correo Electronico: " & Server.HTMLEncode(sE_Mail) & "</LI>")
 		end if

	' LIBRETA MILITAR 
		if len(sPos_Mil) > 0 and aDati(8) = "1" and sPos_Mil  <> 3 then
			if sPos_Mil="1" then
				sPos_Mil= "<LI>Libreta Militar: Primera Clase"
			else
				sPos_Mil= "<LI>Libreta Militar: Segunda Clase"
			end if
			oFileCurr.writeline (sPos_Mil & "</LI>")			
 		end if

		oFileCurr.writeline ("</ul></td></tr>")
else
	oFileCurr.writeline ("<tr><td class='textblu' colspan=2>&nbsp;</td></tr>")
end if

	' PRESENTAZIONE 
		if nSezioni <> -1 then
			if aSezioni(0) = "1" and len(aCV(6)) > 0 then
				oFileCurr.writeline ("<tr>")
				oFileCurr.writeline ("	<td class='textblubold' colspan=2><B>PERFIL LABORAL</B><br><HR></td>")
				oFileCurr.writeline ("</tr>")
				oFileCurr.writeline ("<tr>")
				oFileCurr.writeline ("	<td class='textblu' width=20>&nbsp</td>")
				oFileCurr.writeline ("	<td class='textblu'><ul>" & Server.HTMLEncode(aCV(6)) & "</ul>")
				oFileCurr.writeline ("	</td>")
				oFileCurr.writeline ("</tr>")

			end if
		end if


	' ESPERIENZE PROFESSIONALI 
		if nSezioni > 2 then
			if aSezioni(3) = "1" and len(aCV(10)) > 0 then
				if IsArray(Split(aCV(10),"|")) and aCV(10) <> "0" then
					aPro = Split(aCV(10),"|")
					sIn = "("
					for cont = 0 to ubound(aPro)
						sMansione = Split(aPro(cont),"$",-1,1)(0)
						sIn = sIn & "'" &  sMansione & "',"
					next
					sIn = mid(sIn,1,len(sIn)-1)
					sIn = sIn & ")"
			
						sqlInformale = "SELECT ESPRO.CARGO_ASOC AS CARGO_ASOC, ESPRO.RAG_SOC AS AZIENDA, ESPRO.DT_INI_ESP_PRO AS DI, nvl(ESPRO.DT_FIN_ESP_PRO,Convert(DateTime,'31/12/9999',103)) AS DF, ESPRO.COD_MANSIONE AS MAN, ESPRO.TAREASREALIZADAS AS TASKS " &_ 
									" FROM ESPRO " &_ 
									" WHERE ID_PERSONA = " & id_utente &_ 
									" AND ESPRO.COD_MANSIONE IN " & sIn  &_
									" ORDER BY ESPRO.DT_INI_ESP_PRO desc, DT_FIN_ESP_PRO asc"
'PL-SQL * T-SQL  
SQLINFORMALE = TransformPLSQLToTSQL (SQLINFORMALE) 
					RRInformale.Open sqlInformale, CC
					if  not RRInformale.eof then
						oFileCurr.writeline ("<tr>")
						oFileCurr.writeline ("	<td class='textblubold' colspan=2><B>EXPERIENCIA LABORAL</B><br><HR></td>")
						oFileCurr.writeline ("</tr>")
						oFileCurr.writeline ("<tr>")
						oFileCurr.writeline ("	<td class='textblu' width=20>&nbsp</td>")
						oFileCurr.writeline ("	<td class='textblu'><ul>")
					
						Do while not RRInformale.EOF
								if RRInformale.Fields("AZIENDA") <> "" and ucase(RRInformale.Fields("AZIENDA")) <> "NO DECLARADA" then
								  oFileCurr.writeline ("<b>" & RRInformale.Fields("AZIENDA") & "</b><br>") 
								end if

							'oFileCurr.writeline ("<li>Del "  & ConvDateToString(RRInformale.Fields("DI")))
							'if RRInformale.Fields("DF") <> ConvStringToDate("31/12/9999") then
							''	oFileCurr.writeline (" al " & RRInformale.Fields("DF"))
							'else
							''	oFileCurr.writeline (" a la fecha ")
							'end if
							'oFileCurr.writeline (" - ")


							if RRInformale.Fields("MAN").value <> "" then
								'sMansi = "SELECT MANSIONE FROM CODICI_ISTAT WHERE CODICE='" & RRInformale.Fields("MAN").value & "'"	
								sMansi = "SELECT DENOMINAZIONE as MANSIONE FROM CODICI_ISTAT c, figureprofessionali f WHERE f.cod_ej=c.CODICE and f.id_figprof='" & RRInformale.Fields("MAN").value & "'"
		
								'PL-SQL * T-SQL  
								SMANSI = TransformPLSQLToTSQL (SMANSI) 
								set rsMansi = cc.execute(sMansi)
								if not rsMansi.eof then		
									'oFileCurr.writeline ("como " & lcase(TRIM(rsMansi("MANSIONE"))))
									oFileCurr.writeline (trasforma(TRIM(RRInformale.Fields("CARGO_ASOC"))) & "<br>")
									rsMansi.close
								end if
								set rsMansi=nothing								
							end if
							oFileCurr.writeline (ConvDateToString(RRInformale.Fields("DI")))
							'if RRInformale.Fields("AZIENDA") <> "" and ucase(RRInformale.Fields("AZIENDA")) <> "NON DICHIARATA" then
							''  oFileCurr.writeline (" desarrollado en " & RRInformale.Fields("AZIENDA"))
							'end if
							'oFileCurr.writeline ("</li>")
							if RRInformale.Fields("DF") <> ConvStringToDate("31/12/9999") then
								oFileCurr.writeline (" - " & RRInformale.Fields("DF").value & "<br>")
							else
								oFileCurr.writeline (" - Actualmente <br>")
							end if
							if RRInformale.Fields("TASKS").value <> "" then
								oFileCurr.writeline ("Funciones: <br>")
								oFileCurr.writeline (RRInformale.Fields("TASKS").value & "<br><br>")
							else
								oFileCurr.writeline ("<br><br>")
							end if
							RRInformale.MoveNext
						loop
					end if
					RRInformale.close
					sqlInformale = ""
					oFileCurr.writeline ("</ul>	</td></tr>")

				end if
			end if
		end if


			
	' ESPERIENZE FORMATIVE 
		if nSezioni > 1 then
			if aSezioni(2) = "1" and len(aCV(9)) > 0 then
				if IsArray(Split(aCV(9),"|")) and aCV(9) <> "0" then
					sDatiTitoli = "'" & Replace(aCV(9),"|","','") & "'"
					oFileCurr.writeline ("<tr>")
					oFileCurr.writeline ("	<td class='textblubold' colspan=2><B>FORMACI&Oacute;N ACAD&Eacute;MICA</B><br><HR></td>")
					oFileCurr.writeline ("</tr>")
					oFileCurr.writeline ("<tr>")
					oFileCurr.writeline ("	<td class='textblu' width=20>&nbsp</td>")
					oFileCurr.writeline ("	<td class='textblu'><ul>")
					
					'sqlInformale = "SELECT AA_STUD, COD_STAT_STUD, COD_TIT_STUD, DT_TMST " &_
					'			" FROM TISTUD WHERE ID_PERSONA = " & id_utente &_
					'			" AND COD_TIT_STUD IN (" & sDatiTitoli & ") " &_
					'			" ORDER BY AA_STUD DESC ,COD_STAT_STUD"
					sqlInformale = "SELECT AA_STUD, COD_STAT_STUD, COD_TIT_STUD, COD_LIV_STUD, DT_TMST, TIT_EST_TXT, NOME_ISTITUTO " &_
								" FROM TISTUD WHERE ID_PERSONA = " & id_utente &_
								" AND COD_TIT_STUD IN (" & sDatiTitoli & ") " &_
								" ORDER BY AA_STUD DESC ,COD_STAT_STUD"
					
					'PL-SQL * T-SQL  
					SQLINFORMALE = TransformPLSQLToTSQL (SQLINFORMALE) 
					RRInformale.Open sqlInformale, CC
							
					if  not RRInformale.eof then
						do while not RRInformale.eof							
							'oFileCurr.writeline ("<li>" & RRInformale.Fields("AA_STUD") & " - "  & Trasforma(DecCodVal("TSTUD","0",RRInformale.Fields("DT_TMST"),RRInformale.Fields("COD_TIT_STUD"),0)) & " (" & decDiz_Dati(RRInformale.Fields("COD_STAT_STUD"), "COD_STAT_STUD") & ")</li>")
							oFileCurr.writeline ("<b>" & ucase(RRInformale.Fields("NOME_ISTITUTO")) & "</b><br>")
							if (RRInformale.Fields("TIT_EST_TXT") <> "") then
								oFileCurr.writeline (trasforma(RRInformale.Fields("TIT_EST_TXT")) & "<br>")
							end if
							if(ucase(DecCodVal("LSTUD","0",RRInformale.Fields("DT_TMST"),RRInformale.Fields("COD_LIV_STUD"),0)) <> "NINGUNO" and ucase(DecCodVal("LSTUD","0",RRInformale.Fields("DT_TMST"),RRInformale.Fields("COD_LIV_STUD"),0)) <> "PREESCOLAR") then
										oFileCurr.writeline (trasforma(DecCodVal("LSTUD","0",RRInformale.Fields("DT_TMST"),RRInformale.Fields("COD_LIV_STUD"),0)) & "<br>") 
							end if 
							if (RRInformale.Fields("AA_STUD") <> "") then
								oFileCurr.writeline (RRInformale.Fields("AA_STUD")) 
							end if 
							if (decDiz_Dati(RRInformale.Fields("COD_STAT_STUD"), "COD_STAT_STUD")="Si") then 
								oFileCurr.writeline (" (Culminado)<br><br>")
							elseif (decDiz_Dati(RRInformale.Fields("COD_STAT_STUD"), "COD_STAT_STUD")="No") then
								oFileCurr.writeline (" (No Culminado)<br><br>")							
							end if
							RRInformale.MoveNext
						loop
					end if
					RRInformale.close
					sqlInformale = ""
				end if
				oFileCurr.writeline ("</ul>	</td></tr>")
			end if
		end if

	'FORMAZIONE'
		if nSezioni > 8 then	
			if aSezioni(9) = "1" and len(aCV(15)) > 0 then
				if IsArray(Split(aCV(15),"|")) and aCV(15) <> "0" then
					sFormazione = "'" & Replace(aCV(15),"|","','") & "'"
					oFileCurr.writeline ("<tr>")
					oFileCurr.writeline ("	<td class='textblu' colspan=2>&nbsp</td>")
					oFileCurr.writeline ("</tr>")
					oFileCurr.writeline ("<tr>")
					oFileCurr.writeline ("	<td class='textblubold' colspan=2><B>CURSOS DE CAPACITACI&Oacute;N</B><br><HR></td>")
					oFileCurr.writeline ("</tr>")
					oFileCurr.writeline ("<tr>")
					oFileCurr.writeline ("	<td class='textblu' width=20>&nbsp</td>")
					oFileCurr.writeline ("	<td class='textblu'><ul>")
	
					'sqlInformale = "SELECT F.ID_FORMAZIONE, F.COD_TCORSO, F.AA_CORSO, F.COD_STATUS_CORSO, A.DENOMINAZIONE " & _
					''				" FROM FORMAZIONE F, AREA_CORSO A " & _
					''				" WHERE A.ID_AREA = F.ID_AREA " & _
					''				" AND ID_PERSONA = " & id_utente &_
					''				" AND ID_FORMAZIONE IN (" & sFormazione & ")" &_
					''				" ORDER BY F.AA_CORSO DESC"
					sqlInformale = "SELECT F.ID_FORMAZIONE, F.COD_TCORSO, F.AA_CORSO, F.COD_STATUS_CORSO, F.DESC_ENTE, F.DTINICIO, F.DTFIN, A.DENOMINAZIONE " & _
										" FROM FORMAZIONE F, AREA_CORSO A " & _
										" WHERE A.ID_AREA = F.ID_AREA " & _
										" AND ID_PERSONA = " & id_utente &_
										" AND ID_FORMAZIONE IN (" & sFormazione & ")" &_
										" ORDER BY F.AA_CORSO DESC"
					'PL-SQL * T-SQL  
					SQLINFORMALE = TransformPLSQLToTSQL (SQLINFORMALE) 
					RRInformale.Open sqlInformale, CC

					if  not RRInformale.eof then
						do while not RRInformale.eof							
							'oFileCurr.writeline ("<LI>" &  RRInformale.Fields("AA_CORSO") & " - " & RRInformale.Fields("DENOMINAZIONE") & " - " & LCASE(RRInformale.Fields("COD_TCORSO")) & " (")
							oFileCurr.writeline ("<b>" & RRInformale.Fields("DESC_ENTE") & "</b><br>") 
							oFileCurr.writeline (LCASE(RRInformale.Fields("COD_TCORSO")) & " (")
							select case RRInformale.Fields("COD_STATUS_CORSO")
								case "0"
									oFileCurr.writeline ("Terminado")
								case "1"
									oFileCurr.writeline ("No Terminado")
								case "2"
									oFileCurr.writeline ("En Curso")
								case "3"
									oFileCurr.writeline ("Incompleto")
								case "4"
									oFileCurr.writeline ("No corresponde")						
								case else
							end select
							'oFileCurr.writeline (")</LI>")
							oFileCurr.writeline (")<br>")
							oFileCurr.writeline (RRInformale.Fields("DTINICIO") & " - ")
							if (RRInformale.Fields("DTFIN") <> "") then
								oFileCurr.writeline (RRInformale.Fields("DTFIN") & "<br><br>")
							else
								oFileCurr.writeline ("Actualmente<br><br>")
							end if
							RRInformale.MoveNext
						loop
					end if
					RRInformale.close
					sqlInformale = ""
					oFileCurr.writeline ("</ul></td></tr>")
				end if
			end if
		end if
	
	' LINGUE 
		if nSezioni > 0 then
			if aSezioni(1) = "1" and len(aCV(8)) > 0 then
				if IsArray(Split(aCV(8),"|")) and aCV(8) <> "0" then
					oFileCurr.writeline ("<tr>")
					oFileCurr.writeline ("	<td class='textblubold' colspan=2><B>IDIOMAS</B><br><HR></td>")
					oFileCurr.writeline ("</tr>")
					oFileCurr.writeline ("<tr>")
					oFileCurr.writeline ("	<td class='textblu' width=20>&nbsp</td>")
					oFileCurr.writeline ("	<td class='textblu'><ul>")
					
					sDatiLingue = Replace(aCV(8),"|",",")
					sqlInformale = "SELECT CONOSCENZE.DENOMINAZIONE, PERS_CONOSC.COD_GRADO_CONOSC " &_ 
								" FROM CONOSCENZE, PERS_CONOSC " &_ 
								" WHERE CONOSCENZE.ID_CONOSCENZA = PERS_CONOSC.ID_CONOSCENZA " &_ 
								" AND PERS_CONOSC.ID_PERSONA = " & id_utente &_ 
								" AND PERS_CONOSC.IND_STATUS = 0 " &_ 
								" AND PERS_CONOSC.ID_CONOSCENZA IN (" & sDatiLingue & ")" &_
								" ORDER BY CONOSCENZE.DENOMINAZIONE"
						
'PL-SQL * T-SQL  
SQLINFORMALE = TransformPLSQLToTSQL (SQLINFORMALE) 
					RRInformale.Open sqlInformale, CC
						
					if not RRInformale.eof then
						do while not RRInformale.eof
							oFileCurr.writeline ("<li>" & Trasforma(RRInformale.Fields("DENOMINAZIONE")) & " ("  & decGradoConosc(CInt(RRInformale.Fields("COD_GRADO_CONOSC"))) & ")</li>")
							RRInformale.MoveNext
						loop
					end if
					RRInformale.close
					sqlInformale = ""
				end if
				oFileCurr.writeline ("</ul></td></tr>")
			end if
		end if
				
	' CONOSCENZE INFORMATICHE 
		if nSezioni > 5 then
			if aSezioni(6) = "1" and len(aCV(12)) > 0 then
				if IsArray(Split(aCV(12),"|")) and aCV(12) <> "0" then
					oFileCurr.writeline ("<tr>")
					oFileCurr.writeline ("	<td class='textblubold' colspan=2><B>CONOCIMIENTOS INFORM&Aacute;TICOS</B><br><HR></td>")
					oFileCurr.writeline ("</tr>")
					oFileCurr.writeline ("<tr>")
					oFileCurr.writeline ("	<td class='textblu' width=20>&nbsp</td>")
					oFileCurr.writeline ("	<td class='textblu'><ul>")

					sDatiInformatica = Replace(aCV(12),"|",",")
					sqlInformale = "SELECT CONOSCENZE.DENOMINAZIONE, PERS_CONOSC.COD_GRADO_CONOSC " &_ 
								" FROM CONOSCENZE, PERS_CONOSC " &_ 
								" WHERE CONOSCENZE.ID_CONOSCENZA = PERS_CONOSC.ID_CONOSCENZA " &_ 
								" AND PERS_CONOSC.ID_PERSONA = " & id_utente &_ 
								" AND PERS_CONOSC.IND_STATUS = 0 " &_ 
								" AND PERS_CONOSC.ID_CONOSCENZA IN (" & sDatiInformatica & ")" &_
								" ORDER BY CONOSCENZE.DENOMINAZIONE"
					'PL-SQL * T-SQL  
					SQLINFORMALE = TransformPLSQLToTSQL (SQLINFORMALE) 
					RRInformale.Open sqlInformale, CC
						
					if not RRInformale.eof then
						do while not RRInformale.eof
							oFileCurr.writeline ("<li>" & Trasforma(RRInformale.Fields("DENOMINAZIONE")) & " (" & decGradoConosc(CInt(RRInformale.Fields("COD_GRADO_CONOSC"))) & ")</li>")
							RRInformale.MoveNext
						loop
					end if
					RRInformale.close
					sqlInformale = ""
				end if
				oFileCurr.writeline ("</ul></td></tr>")
			end if
		end if

		oFileCurr.writeline ("<tr><td class='textblubold' colspan=2><B>INFORMACI&Oacute;N COMPLEMENTARIA</B><br><HR></td></tr>")

	' ALTRE INFO '
		if nSezioni > 4 then	
			if aSezioni(5) = "1" then
				if IsArray(Split(aCV(11),"|")) and len(aCV(11)) > 0 then
					'oFileCurr.writeline ("<tr>")
					'oFileCurr.writeline ("	<td class='textblubold' colspan=2><B>INFORMACION ADICIONAL</B><br><HR></td>")
					'oFileCurr.writeline ("</tr>")
					oFileCurr.writeline ("<tr>")
					oFileCurr.writeline ("	<td class='textblu' width=20>&nbsp</td>")
					oFileCurr.writeline ("	<td class='textblu'><ul>")
					aInfo = Split(aCV(11),"|")
					for cont = 0 to UBound(aInfo)
						oFileCurr.writeline ("<LI>" & Server.HTMLEncode(aInfo(cont)) & "</LI>")
					next
					oFileCurr.writeline ("</ul></td></tr>")
				end if
			end if
		end if
			
	
	' ISCRIZIONE ALBO 
		if nSezioni > 9 then
			if aSezioni(10) = "1" and len(aCV(16)) > 0 then
				if IsArray(Split(aCV(16),"|")) and aCV(16) <> "0" then
					aFor = Split(aCV(16),"|")
					sDatiTitoli = "'" & Replace(aCV(16),"|","','") & "'"

					'oFileCurr.writeline ("<tr>")
					'oFileCurr.writeline ("	<td class='textblubold' colspan=2><B>TARJETA PROFESIONAL</B><br><HR></td>")
					'oFileCurr.writeline ("</tr>")
					oFileCurr.writeline ("<tr>")
					oFileCurr.writeline ("	<td class='textblu' width=20>&nbsp</td>")
					oFileCurr.writeline ("	<td class='textblu'><ul>")

					sqlInformale = "SELECT COD_ALBO,DT_DEC_ISCR,PRV_ISCR,NUM_ISCR, DT_TMST " &_
						" FROM PERS_ALBO WHERE ID_PERSONA = " & id_utente &_ 
						" AND COD_ALBO IN (" & sDatiTitoli & ") " &_
						" ORDER BY DT_DEC_ISCR DESC "

					'PL-SQL * T-SQL  
					SQLINFORMALE = TransformPLSQLToTSQL (SQLINFORMALE) 
					RRInformale.Open sqlInformale, CC								
					
					if  not RRInformale.eof then
						do while not RRInformale.eof	
							'oFileCurr.writeline ("<li>" & Trasforma(DecCodVal("TALBO","0",RRInformale.Fields("DT_TMST"),RRInformale.Fields("COD_ALBO"),0)) &_
							''	" - Inscripci�n n� " & RRInformale.Fields("NUM_ISCR") & " el "  &_ 
							''	ConvDateToString(RRInformale.Fields("DT_DEC_ISCR")) &_ 
							''	" (" & RRInformale.Fields("PRV_ISCR") & ")</li>")
							oFileCurr.writeline ("<li>" & "Tarjeta Profesional N� " & RRInformale.Fields("NUM_ISCR") & " de " & Trasforma(RRInformale.Fields("COD_ALBO")) &_
									", expedida el "  & ConvDateToString(RRInformale.Fields("DT_DEC_ISCR")) &_ 
									"</li>")

							RRInformale.MoveNext
						loop
										
						end if
						RRInformale.close
						sqlInformale = ""
					end if
				oFileCurr.writeline ("</ul></td></tr>")
			end if
		end if				
			
	'PATENTI'
		if nSezioni > 7 then	
			if aSezioni(8) = "1" and len(aCV(14)) > 0 then
				if IsArray(Split(aCV(14),"|")) and aCV(14) <> "0" then
					sPatenti = "'" & Replace(aCV(14),"|","','") & "'"
					oFileCurr.writeline ("<tr>")
					oFileCurr.writeline ("	<td class='textblu' colspan=2>&nbsp</td>")
					oFileCurr.writeline ("</tr>")
					'oFileCurr.writeline ("<tr>")
					'oFileCurr.writeline ("	<td class='textblubold' colspan=2><B>LICENCIAS DE CONDUCCI�N</B><br><HR></td>")
					'oFileCurr.writeline ("</tr>")
					oFileCurr.writeline ("<tr>")
					oFileCurr.writeline ("	<td class='textblu' width=20>&nbsp</td>")
					oFileCurr.writeline ("	<td class='textblu'><ul>")
					'sqlInformale = "SELECT COD_PERM, DT_TMST " & _
					''				" FROM AUTCERT " & _
					''				" WHERE ID_PERSONA = " & id_utente & _
					''				" AND COD_PERM IN (" & sPatenti & ")" &_
					''				" AND nvl(DT_SCADENZA, to_date('31/12/9999', 'dd/mm/yyyy')) >= " &  convDateToDB(date)
					sqlInformale = "SELECT COD_PERM, DT_TMST " & _
									" FROM AUTCERT " & _
									" WHERE ID_PERSONA = " & id_utente & _
									" AND COD_PERM IN (" & sPatenti & ")" &_
									" AND (DT_SCADENZA >= " &  convDateToDB(date) & "or dt_scadenza is null)"
							
					'PL-SQL * T-SQL  
					SQLINFORMALE = TransformPLSQLToTSQL (SQLINFORMALE) 
					RRInformale.Open sqlInformale, CC

					if  not RRInformale.eof then
						do while not RRInformale.eof							
							'oFileCurr.writeline ("<LI>" & trasforma(DecCodVal("TPERM","0",RRInformale.Fields("DT_TMST"),RRInformale.Fields("COD_PERM"),0)) & "</LI>")
							oFileCurr.writeline ("<LI>Licencia de Conducci&oacute;n " & trasforma(DecCodVal("TPERM","0",RRInformale.Fields("DT_TMST"),RRInformale.Fields("COD_PERM"),0)) & "</LI>")
							RRInformale.MoveNext
						loop
					end if
					RRInformale.close
					sqlInformale = ""
					oFileCurr.writeline ("</ul></td></tr>")
				end if
			end if
		end if

	' DISPONIBILITA'
		if nSezioni > 6 then	
			if aSezioni(7) = "1" and len(aCV(13)) > 0 then
				if IsArray(Split(aCV(13),"|")) and aCV(13) <> "0" then
					sDisponibilita = "'" & Replace(aCV(13),"|","','") & "'"
					sCondizione = "codice IN (" & sDisponibilita & ")"
					sTabella = "TDISP"
					dData	 = Date()
					nOrder   = 2
					Isa		 = 0
					
					'Response.Write sTabella & "<br>"
					'Response.Write dData & "<br>"
					'Response.Write sCondizione & "<br>"
					'Response.Write nOrder & "<br>"
					'Response.Write Isa & "<br>"
					'Response.End 
					
					aDisponibilita = decodTadesToArray(sTabella,dData,sCondizione,nOrder,Isa)

					if isArray(aDisponibilita) then
						'oFileCurr.writeline ("<tr>")
						'oFileCurr.writeline ("	<td class='textblubold' colspan=2><B>DISPONIBILIDADES/LIMITACIONES</B><br><HR></td>")
						'oFileCurr.writeline ("</tr>")
						
						nCodArea = 00
						for i = 0 to ubound(aDisponibilita) -1
							if clng(aDisponibilita(i,i+1,i+1)) <> clng(nCodArea) then
								if i <> 0 then oFileCurr.writeline ("</ul></td></tr>") end if 
								oFileCurr.writeline ("<TR><td class='textblubold' colspan=2><b>" & Trasforma(DecCodVal("CL_VD", 0, Date(),aDisponibilita(i,i+1,i+1),0)) & "</td></TR>")
								nCodArea = aDisponibilita(i,i+1,i+1)
							end if						
							oFileCurr.writeline ("<tr>")
							oFileCurr.writeline ("	<td class='textblu' width=20>&nbsp</td>")
							oFileCurr.writeline ("	<td class='textblu'><ul>")
							oFileCurr.writeline ("<LI>" & Trasforma(aDisponibilita(i,i+1,i)) & "</LI>")
						next
						Erase aDisponibilita
						oFileCurr.writeline ("</ul></td></tr>")
					end if
				end if
			end if
		end if

			
	' PRIVACY 
	oFileCurr.writeline ("<tr><td class='textblu' colspan=2>&nbsp;</td></tr>")
	'oFileCurr.writeline ("<tr><td class='textblu' colspan=2><B>Dichiaro di essere stato informato, ai sensi e per gli effetti di cui all'art'10 della legge 675/96 che i dati personali raccolti saranno trattati, anche con strumenti informatici, esclusivamente nell'ambito del procedimento per il quale la presente dichiarazione viene resa.<B></td></tr>")
	
	oFileCurr.writeline ("</table>")
	oFileCurr.writeline ("</td>")
	oFileCurr.writeline ("</tr>")
	
	set RRInformale = nothing
%>
