<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%
	'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	' name:		textToHtml                                     
	' purpose:	sostituisce in una stringa i caratteri di      
	'			ritorno carrello con la stringa "<br>"         
	'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	function textToHtml(text)
		dim accato
		accapo = Chr(13) & Chr(10)
		do while inStr(1,text,accapo) <> 0
			text = Replace(text,accapo,"<br>")
		loop
		textToHtml = text
	end function
	'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	' name:		htmlToText                                     
	' purpose:	sostituisce in una stringa la stringa "<br>"   
	'			con i caratteri di ritorno carrello            
	'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	function htmlToText(html)
		dim accapo
		accapo = Chr(13) & Chr(10)
		do while inStr(1,html,"<br>") <> 0
			html = Replace(html,"<br>",accapo)
		loop
	end function
	'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	' name:		getDatiDB                                      
	' purpose:	data una ID_PERSONA, preleva dal DB i dati     
	'			rilevanti per la composizione del Curriculum   
	'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	function getDatiDB(id_persona)
		dim sSQL
		dim rsDati
		sSQL = "SELECT DISTINCT "
			sSQL = sSQL & "PERSONA.NOME, "
			sSQL = sSQL & "PERSONA.COGNOME, "
			sSQL = sSQL & "PERSONA.SESSO, "
			sSQL = sSQL & "PERSONA.DT_NASC, "
			sSQL = sSQL & "PERSONA.COM_NASC, "
			sSQL = sSQL & "PERSONA.PRV_NASC, "
			sSQL = sSQL & "PERSONA.PRV_RES, "
			sSQL = sSQL & "PERSONA.IND_RES, "
			sSQL = sSQL & "PERSONA.CAP_RES, "
			sSQL = sSQL & "PERSONA.NUM_TEL, "
			sSQL = sSQL & "PERSONA.NUM_TEL_DOM, "
			sSQL = sSQL & "PERSONA.E_MAIL, "
			sSQL = sSQL & "PERSONA.STAT_CIV, "
			sSQL = sSQL & "PERSONA.COM_RES "
		sSQL = sSQL & "FROM "
			sSQL = sSQL & "PERSONA "
		sSQL = sSQL & "WHERE "
			sSQL = sSQL & "ID_PERSONA = " & id_persona
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
		set rsDati = cc.execute(sSQL)
		if not rsDati.eof then
			sNome = rsDati.Fields("NOME").value
			sCognome = rsDati.Fields("COGNOME").value
			sSesso = rsDati.Fields("SESSO").value
			sDt_Nasc = ConvDateToString(rsDati.Fields("DT_NASC").value)
			sCom_Nasc = rsDati.Fields("COM_NASC").value
			sPrv_Nasc = rsDati.Fields("PRV_NASC").value
			sCom_Res = rsDati.Fields("COM_RES").value
			sPrv_Res = rsDati.Fields("PRV_RES").value
			sInd_Res = rsDati.Fields("IND_RES").value
			sCap_Res = rsDati.Fields("CAP_RES").value
			sNum_Tel = rsDati.Fields("NUM_TEL").value
			sNum_Tel2 = rsDati.Fields("NUM_TEL_DOM").value
			sE_Mail = rsDati.Fields("E_MAIL").value
			sStat_Civ = rsDati.Fields("STAT_CIV").value
		else
			getDatiDB = "0"
		end if
	end function
	'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	' name:		initFile                                       
	' purpose:	data una ID_PERSONA, inzializza un oggetto     
	'			oFileCurriculum aprendo un file di testo dal   
	'			server o, se qesto non esiste, lo crea         
	'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	function initFile(id_persona)
		 
		sFilePath = left(Request.ServerVariables("PATH_TRANSLATED"),InStrRev(Request.ServerVariables("PATH_TRANSLATED"),"\")) & "curricula\" & id_persona & ".cv"
		set oFileSystemObject = Server.CreateObject("Scripting.FileSystemObject")
		if oFileSystemObject.FileExists(sFilePath) then
			set oFileCurriculum = oFileSystemObject.OpenTextFile(sFilePath,1)
			bNewCV = false
			initFile = "true"
		else
			set oFileCurriculum = oFileSystemObject.CreateTextFile(sFilePath,true)
			oFileCurriculum.WriteLine("#1#")
			oFileCurriculum.WriteLine("1")
			oFileCurriculum.WriteLine("#2#")
			oFileCurriculum.WriteLine("1|1|0|0")
			oFileCurriculum.WriteLine(sBoundary)
			oFileCurriculum.WriteLine("# presentazione")
			oFileCurriculum.WriteLine("")
			oFileCurriculum.WriteLine(sBoundary)
			oFileCurriculum.WriteLine("# esperienze formative")
			oFileCurriculum.WriteLine("")
			oFileCurriculum.WriteLine(sBoundary)
			oFileCurriculum.WriteLine("# esperienze professionali")
			oFileCurriculum.WriteLine("")
			oFileCurriculum.WriteLine(sBoundary)
			oFileCurriculum.WriteLine("# altro")
			oFileCurriculum.WriteLine("")
			oFileCurriculum.WriteLine(sBoundary)
			bNewCV = true
			set oFileCurriculum = oFileSystemObject.OpenTextFile(sFilePath,1)
			initFile = "false"
		end if
	end function
	'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	' name:		getNatale                                      
	' purpose:	preleva da DB i dati relativi alla nascita     
	'			dell'utente (data, comune, eccetera)           
	'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	function getNatale(sesso,codcom)
		dim sSQL
		dim rsNatale
		sSQL = "SELECT DISTINCT DESCOM FROM COMUNE WHERE CODCOM = '" + codcom + "'"
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
		set rsNatale = cc.execute(sSQL)
		if not rsNatale.eof then
			if sesso = "M" then
				getNatale = "<tr><td class='tbltext'>NATO&nbsp;A " & rsNatale.Fields("DESCOM") & " (" & sPrv_Nasc & ") IL " & sDt_Nasc & "</td></tr>"
			else
				getNatale = "<tr><td class='tbltext'>NATA&nbsp;A " & rsNatale.Fields("DESCOM") & " (" & sPrv_Nasc & ") IL " & sDt_Nasc & "</td></tr>"
			end if
		else
			getNatale = ""
		end if
	end function
	'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	' name:		getResidenza                                   
	' purpose:	preleva da DB i dati relativi alla residenza   
	'			dell'utente (data, comune, eccetera)           
	'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	function getResidenza(codcom)
		dim sSQL
		dim rsResidenza
		sSQL = "SELECT DISTINCT DESCOM FROM COMUNE WHERE CODCOM = '" + codcom + "'"
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
		set rsResidenza = cc.execute(sSQL)
		if not rsResidenza.eof then
			getResidenza = "<tr><td class='tbltext'>RESIDENTE IN " & sInd_Res & " - " & sCap_Res & " - " & rsResidenza.Fields("DESCOM").value & " (" & sPrv_Res & ")</td></tr>"
		else
			getResidenza = ""
		end if
	end function
	'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	' name:		getFileDati                                    
	' purpose:	elabora i dati archiviati sul file di testo    
	'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	function getFileDati()
		dim inString
		dim aDatiAnagrafici
		inString = ""
		do while oFileCurriculum.AtEndOfStream <> true
			inString = oFileCurriculum.ReadLine
			if left(inString, 1) <> "#" then
				aDatiAnagrafici = Split(inString,"|")
				bStat_Civ = aDatiAnagrafici(0)
				bE_Mail = aDatiAnagrafici(1)
				bPos_Mil = aDatiAnagrafici(2)
				if aDatiAnagrafici(3) = "0" then
					bNum_Tel_Alt = ""
				else
					'bNum_Tel_Alt = aDatiAnagrafici(3)
					bNum_Tel_Alt = sNum_Tel2
				end if
			else
				if inString = sBoundary then
					exit do
				end if
			end if
		loop
		sPresentazione = ""
		do while oFileCurriculum.AtEndOfStream <> true
			inString = oFileCurriculum.ReadLine
			if left(inString, 1) <> "#" then
				sPresentazione = sPresentazione & inString & Chr(13) & Chr(10)
			else
				if inString = sBoundary then
					exit do
				end if
			end if
		loop
		do while oFileCurriculum.AtEndOfStream <> true
			inString = oFileCurriculum.ReadLine
			if left(inString, 1) <> "#" then
				if InStr(1,inString,"|") <> 0 then
					aEsp_For = Split(inString,"|")
				else
					dim aTmp_For(1)
					aTmp_For(0) = inString
					aEsp_For = aTmp_For
				end if
			else
				if inString = sBoundary then
					exit do
				end if
			end if
		loop
		do while oFileCurriculum.AtEndOfStream <> true
			inString = oFileCurriculum.ReadLine
			if left(inString, 1) <> "#" then
				if InStr(1,inString,"|") <> 0 then
					aEsp_Pro = Split(inString,"|")
				else
					dim aTmp_Pro(1)
					aTmp_Pro(0) = inString
					aEsp_Pro = aTmp_Pro
				end if
			else
				if inString = sBoundary then
					exit do
				end if
			end if
		loop
		getFileDati = "1"
	end function
	'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	' name:		getDatiAnagrafici                              
	' purpose:	                                               
	'			                                               
	'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	function getDatiAnagrafici()
		dim sResponse, sPoMil
		sResponse = ""
		if not task = "mod" then
			if bNum_Tel_Alt <> "0" then
				sResponse = sResponse & "<tr><td class='tbltext'>ALTRO RECAPITO TELEFONICO " & bNum_Tel_Alt & "</td></tr>"
			end if
			if bStat_Civ <> "0" and len(sStat_Civ) > 0 and len(sSesso) > 0 then
				sResponse = sResponse & "<tr><td class='tbltext'>STATO CIVILE " & decStatoCivile(sStat_Civ, sSesso) & "</td></tr>"
			end if
			if bE_Mail <> "0" and len(sE_Mail) > 0 then
				sResponse = sResponse & "<tr><td class='tbltext'>E-MAIL " & sE_Mail & "</td></tr>"
			end if
			if bPos_Mil <> "0" and len(sPos_Mil) > 0 then
				sPoMil = ""
				select case sPos_Mil
					Case "1"
						sPoMil = "ASSOLTO"
					Case "2"
						sPoMil = "RIVEDIBILE"
					Case "3"
						sPoMil = "ESONERATO"
					Case "4"
						sPoMil = "RIFORMATO"
					Case "5"
						sPoMil = "DA ASSOLVERE"
				end select
				sResponse = sResponse & "<tr><td class='tbltext'>POSIZIONE MILITARE " & sPoMil & "</td></tr>"
			end if
		else
			'ALTRO RECAPITO TELEFONICO
			sResponse = sResponse & "<tr><td class='tbltext'>"
			sResponse = sResponse & "<input onClick='bNum_Tel_Alt_Click(this)' type='checkbox' value='1' name='bNum_Tel_Alt'"
			if bNum_Tel_Alt <> "" then
				sResponse = sResponse & " checked"
			end if
			sResponse = sResponse & ">&nbsp;ALTRO RECAPITO TELEFONICO "
			'sResponse = sResponse & "<input type='text' name='bNum_Tel_Alt_Value' value='" & bNum_Tel_Alt & "' onChange=""bNum_Tel_Alt_Change(this.value);"">"
			sResponse = sResponse & "<input type='text' name='bNum_Tel_Alt_Value' value='" & sNum_Tel2 & "' onChange=""bNum_Tel_Alt_Change(this.value);"">"			
			sResponse = sResponse & "</td></tr>"
			'STATO CIVILE
			if len(sStat_Civ) > 0 and len(sSesso) > 0 then
				sResponse = sResponse & "<tr><td class='tbltext'>"
				sResponse = sResponse & "<input type='checkbox' value='1' name='bStat_civ'"
				if bStat_Civ <> "0" then
					sResponse = sResponse & " checked"
				end if
				sResponse = sResponse & ">&nbsp;STATO CIVILE " & decStatoCivile(sStat_Civ, sSesso) & "</td></tr>"
			end if
			'EMAIL
			if len(sE_Mail) > 0 then
				sResponse = sResponse & "<tr><td class='tbltext'>"
				sResponse = sResponse & "<input type='checkbox' value='1' name='bE_Mail'"
				if bE_mail <> "0" then
					sResponse = sResponse & " checked"
				end if
				sResponse = sResponse & ">&nbsp;E-MAIL " & sE_Mail & "</td></tr>"
			end if
			if len(sPos_Mil) > 0 then
				sPoMil = ""
				select case sPos_Mil
					Case "1"
						sPoMil = "ASSOLTO"
					Case "2"
						sPoMil = "RIVEDIBILE"
					Case "3"
						sPoMil = "ESONERATO"
					Case "4"
						sPoMil = "RIFORMATO"
					Case "5"
						sPoMil = "DA ASSOLVERE"
				end select
				sResponse = sResponse & "<tr><td class='tbltext'>"
				sResponse = sResponse & "<input type='checkbox' value='1' name='bPos_Mil'"
				if bPos_Mil <> "0" then
					sResponse = sResponse & " checked"
				end if
				sResponse = sResponse & ">&nbsp;POSIZIONE MILITARE " & sPoMil & "</td></tr>"
			end if
		end if
		getDatiAnagrafici = sResponse
	end function
	'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	' name:		decStatoCivile                                 
	' purpose:	                                               
	'                                                          
	'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	function decStatoCivile(code,sex)
		select case code
			Case "01"
				if sex = "M" then
					decStatoCivile = "CELIBE"
				else
					decStatoCivile = "NUBILE"
				end if
			Case "02"
				if sex = "M" then
					decStatoCivile = "CONIUGATO"
				else
					decStatoCivile = "CONIUGATA"
				end if
			Case "03"
				if sex = "M" then
					decStatoCivile = "VEDOVO"
				else
					decStatoCivile = "VEDOVA"
				end if
			Case "04"
				if sex = "M" then
					decStatoCivile = "SEPARATO LEGALMENTE"
				else
					decStatoCivile = "SEPARATA LEGALMENTE"
				end if
			Case "05"
				if sex = "M" then
					decStatoCivile = "DIVORZIATO"
				else
					decStatoCivile = "DIVORZIATA"
				end if
		end select
	end function
%>
