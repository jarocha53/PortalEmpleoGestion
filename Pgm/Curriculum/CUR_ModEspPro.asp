<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%
dim aEsp_Pro


if inStr(1,aCV(10),"|") = 0 and len(aCV(10)) > 0 then
	aEsp_Pro = Array(aCV(10))
elseif len(aCV(10)) < 1 then
	aEsp_Pro = Array()
else
	aEsp_Pro = Split(aCV(10),"|")
end if
dim rsEsp_Pro
sSQL = "SELECT ESPRO.CARGO_ASOC AS CARGO_ASOC, ESPRO.RAG_SOC AS AZIENDA, ESPRO.DT_INI_ESP_PRO AS DI, nvl(ESPRO.DT_FIN_ESP_PRO,to_Date('31/12/9999','dd/mm/yyyy')) AS DF, ESPRO.COD_MANSIONE AS MAN " &_ 
		" FROM ESPRO " &_ 
		" WHERE ID_PERSONA = " & id_utente &_
		" ORDER BY ESPRO.DT_FIN_ESP_PRO desc, DT_INI_ESP_PRO DESC"
'Response.Write ssql
'Response.End 
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
set rsEsp_Pro = cc.execute(sSQL)
nPro = 0
%>
<tr>
	<td class='textblu'>
		<table border='0' cellspacing='3' cellpadding='4'>
<%
if not rsEsp_Pro.eof then
	do while rsEsp_Pro.eof <> true
	
%>
<tr valign='top'>
	<td class='textblu'>
		<input type='checkbox' name='Esp_Pro<%=nPro%>' value='<%=rsEsp_Pro.Fields("MAN") & "$" & ConvDateToString(rsEsp_Pro.Fields("DI"))%>'
<%
	if isArray(aEsp_Pro) then
		
		for cont = 0 to UBound(aEsp_Pro)
		' Nei vecchi curricula le esperienze formative venivano memorizzare sul file solo con la data di inizio 
		' dell'esperienza, attualmente, invece si memorizza anche il codice dell'esperienza
			aMansione =	Split(aEsp_Pro(cont),"$",-1,1)
			
			if ubound(aMansione) = 1 then
				sMansione	= aMansione(0)
				sDt_Ini		= aMansione(1)
			else
				sMansione	= ""
				sDt_Ini		= aMansione(0)
			end if
			
			if sMansione <> "" then
				if sDt_Ini = ConvDateToString(rsEsp_Pro.Fields("DI")) and sMansione = rsEsp_Pro.Fields("MAN") then
					Response.Write " checked"
				end if
			end if
		next
	elseif len(aCV(10)) < 1 then
				Response.Write " checked"
	end if
%>
		>
	</td>
	<td class='textblubold'>
		del <%=ConvDateToString(rsEsp_Pro.Fields("DI").value)%>
<%
		if rsEsp_Pro.Fields("DF") = convStringtoDate("31/12/9999") then
			Response.Write " a la fecha "			
		else
			Response.Write " al " & rsEsp_Pro.Fields("DF")
		end if
%>
		<% if rsEsp_Pro.Fields("MAN").value <> "" then 
		''''' SM inizio modificata select per prendere codice mansione
				'sMansi = "SELECT MANSIONE FROM CODICI_ISTAT WHERE CODICE='" & rsEsp_Pro.Fields("MAN").value & "'"			
				
				'02/01/2009 DAMIAN CAMBIA EL NOMBRE DEL CAMPO PARA QUE MUESTRE COHERENTEMENTE EL PUESTO				
				'sMansi = "SELECT MANSIONE FROM CODICI_ISTAT c, figureprofessionali f WHERE f.cod_ej=c.CODICE and f.id_figprof='" & rsEsp_Pro.Fields("MAN").value & "'"
				sMansi = "SELECT DENOMINAZIONE as MANSIONE FROM CODICI_ISTAT c, figureprofessionali f WHERE f.cod_ej=c.CODICE and f.id_figprof='" & rsEsp_Pro.Fields("MAN").value & "'"
		'''''' SM fine
'PL-SQL * T-SQL  
SMANSI = TransformPLSQLToTSQL (SMANSI) 
'Response.Write SMANSI
				set rsMansi = cc.execute(sMansi)
				if not rsMansi.eof then		
		%>
					<br>como <%=TRIM(rsEsp_Pro("CARGO_ASOC"))%> (<%=TRIM(rsMansi("MANSIONE"))%>)		
		<% 
					rsMansi.close
				end if
				set rsMansi=nothing
			end if
		   if rsEsp_Pro.Fields("AZIENDA").value <> "" then %>
		<br>en la empresa <%=rsEsp_Pro.Fields("AZIENDA")%>
		<% end if %>
<%
		rsEsp_Pro.movenext
		nPro = nPro + 1
%>
	</td>
</tr>

<%
	loop
end if
%>
		<TR>
			<TD colspan=2>
				<input type='hidden' name='nPro' value='<%=nPro%>'>
			</TD>
		</TR>
	</table>
</td>
</tr>
