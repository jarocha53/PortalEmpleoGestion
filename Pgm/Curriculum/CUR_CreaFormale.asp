
<%

Dim sqlFormale	'stringa sql di esexcuzione
Dim RRFormale	'recordset utilizzato per tutte le query
set RRFormale = Server.CreateObject("ADODB.Recordset")
aDati = Split(aCV(2),"|")
aSezioni = Split(aCV(3),"|")
nSezioni = ubound(aSezioni)
call getDatiDB(id_utente)

oFileCurr.writeline ("<tr>")
oFileCurr.writeline ("	<td align='center'>")
oFileCurr.writeline ("		<SPAN class='textblutitolo'><br>Curriculum Vitae<br></SPAN>")
oFileCurr.writeline ("		<SPAN class='textblubold'>de</SPAN>")
oFileCurr.writeline ("		<SPAN class='textblunome'><br>" & sNome & " " & sCognome & "</P>")
oFileCurr.writeline ("	</td>")
oFileCurr.writeline ("</tr>")
oFileCurr.writeline ("<tr>")
oFileCurr.writeline ("	<td>&nbsp;</td>")
oFileCurr.writeline ("</tr>")
oFileCurr.writeline ("<tr>")
oFileCurr.writeline ("	<td>")

if STAMPA = "SI" then 
	oFileCurr.writeline ("<br><br><table border='0' cellspacing='2' cellpadding='3' align=center width=600>")
else 
	oFileCurr.writeline ("<table border='0' cellspacing='2' cellpadding='3' width='100%'>")
end if 
	
if aDati(0) = "1" or aDati(1) = "1" or aDati(2) = "1" or _
	aDati(3) = "1" or aDati(4) = "1" or aDati(5) = "1" or _
	aDati(6) = "1" or aDati(7) = "1" or aDati(8) = "1" then
	
	oFileCurr.writeline ("<tr>")
	if STAMPA = "SI" then 
		oFileCurr.writeline ("<td class='textblu' width=170 valign=top >INFORMACION PERSONAL</td>")
	else 
		oFileCurr.writeline ("<td class='textblu' width=90 valign=top >INFORMACION PERSONAL</td>")
	end if 
	oFileCurr.writeline ("<td class='textblu'>")
		
	' STATO CIVILE 
	sSCiv = decCodVal("STCIV","0","",sStat_Civ,"")
	if len(sStat_Civ) > 0 and aDati(0) = "1" and sSCiv <> "" then
		oFileCurr.writeline ("<table>")
		oFileCurr.writeline ("			<tr>")
		oFileCurr.writeline ("				<td  width=5><li>&nbsp;</li></td>")
		oFileCurr.writeline ("				<td class=textblu>Estado Civil : " & lcase(sSCiv) & "</td>")
		oFileCurr.writeline ("			</tr>")
		oFileCurr.writeline ("</table>")
	end if
			 
	' NAZIONALITA' 
	sSCit = decCodVal("STATO","0","",sStat_Cit,"")
	if Len(sStat_Cit) > 0 and aDati(1) = "1" and sSCit <> "" then
		oFileCurr.writeline ("<table>")
		oFileCurr.writeline ("			<tr>")
		oFileCurr.writeline ("				<td  width=5><li>&nbsp;</li></td>")
		oFileCurr.writeline ("				<td class=textblu>Nacionalidad : " & decCodVal("STATO","0","",sStat_Cit,"") & "</td>")
		oFileCurr.writeline ("			</tr>")
		oFileCurr.writeline ("</table>")
	end if

	' DATA DI NASCITA 
	if aDati(2) = "1" and len(sDt_Nasc) > 0 then
		oFileCurr.writeline ("<table>")
		oFileCurr.writeline ("			<tr>")
		oFileCurr.writeline ("				<td  width=5><li>&nbsp;</li></td>")
		oFileCurr.writeline ("				<td class=textblu>Fecha de Nacimiento : " & sDt_Nasc & "</td>")
		oFileCurr.writeline ("			</tr>")
		oFileCurr.writeline ("</table>")
	end if

	' LUOGO DI NASCITA 
	sSNasc = DescrComune(sStat_Nasc)
	sComNasc = DescrComune(sCom_Nasc)
	if aDati(2) = "1" and (len(sComNasc) > 1 or len(sSNasc) > 1) then
		oFileCurr.writeline ("<table>")
		oFileCurr.writeline ("			<tr>")
		oFileCurr.writeline ("				<td  width=5><li>&nbsp;</li></td>")
		oFileCurr.writeline ("				<td class=textblu>Lugar de Nacimiento : ")
		if len(sComNasc) > 1 then
			oFileCurr.writeline ( Trasforma(sComNasc))
		else
			oFileCurr.writeline ( Trasforma(sSNasc))
		end if
											 
		if len(sPrv_Nasc) > 0 then 
			oFileCurr.writeline (" (" & sPrv_Nasc & ")")
		end if 
		oFileCurr.writeline ("				</td>")
		oFileCurr.writeline ("			</tr>")
		oFileCurr.writeline ("</table>")	
	end if

	' RESIDENZA 
	if aDati(3) = "1" then
		oFileCurr.writeline ("<table>")
		oFileCurr.writeline ("			<tr>")
		oFileCurr.writeline ("				<td  width=5><li>&nbsp;</li></td>")
		oFileCurr.writeline ("				<td class=textblu>Residencia : ")
 		if len(sInd_Res) > 0 then
			oFileCurr.writeline ( Trasforma(sInd_Res) & " - ")
		end if 
		if len(sCap_Res) > 0 then 
			oFileCurr.writeline (sCap_Res & " ")
		end if 
		if len(DescrComune(sCom_Res)) > 1 then 
			oFileCurr.writeline (Trasforma(DescrComune(sCom_Res)))
			if len(sPrv_Res) then 
				oFileCurr.writeline (" ("  & sPrv_Res & ")")
			end if
		end if
		oFileCurr.writeline ("				</td>")
		oFileCurr.writeline ("			</tr>")
		oFileCurr.writeline ("</table>")						
	end if
	' RECAPITO TELEFONICO 
	if aDati(5) = "1" and len(sNum_Tel) > 0 then
		oFileCurr.writeline ("<table>")
		oFileCurr.writeline ("			<tr>")
		oFileCurr.writeline ("				<td  width=5><li>&nbsp;</li></td>")
		oFileCurr.writeline ("				<td class=textblu>Nro. Tel�fono : " & Server.HTMLEncode(sNum_Tel) & "</td>")
		oFileCurr.writeline ("			</tr>")
		oFileCurr.writeline ("</table>")
	end if
			
	' RECAPITO TELEFONICO ALTERNATIVO 
	'if aDati(6) = "1" and len(aCV(4)) > 0 then
	if aDati(6) = "1" and len(sNum_Tel2) > 0 then
		oFileCurr.writeline ("<table>")
		oFileCurr.writeline ("			<tr>")
		oFileCurr.writeline ("				<td  width=5><li>&nbsp;</li></td>")
		'oFileCurr.writeline ("				<td class=textblu>Nro. Tel�fono alternativo : " & Server.HTMLEncode(aCV(4)) & "</td>")
		oFileCurr.writeline ("				<td class=textblu>Nro. Tel�fono celular : " & Server.HTMLEncode(sNum_Tel2) & "</td>")
		oFileCurr.writeline ("			</tr>")
		oFileCurr.writeline ("</table>")
	end if

	' FAX 
	if aDati(7) = "1" and len(aCV(5)) > 0 then
		oFileCurr.writeline ("<table>")
		oFileCurr.writeline ("			<tr>")
		oFileCurr.writeline ("				<td  width=5><li>&nbsp;</li></td>")
		oFileCurr.writeline ("				<td class=textblu>Fax : " & Server.HTMLEncode(aCV(5)) &  "</td>")
		oFileCurr.writeline ("			</tr>")
		oFileCurr.writeline ("</table>")
	end if
	 
	' EMAIL 
	if len(sE_Mail) > 0 and aDati(4) = "1" then
		oFileCurr.writeline ("<table>")
		oFileCurr.writeline ("			<tr>")
		oFileCurr.writeline ("				<td  width=5><li>&nbsp;</li></td>")
		oFileCurr.writeline ("				<td class=textblu>E-Mail : " & Server.HTMLEncode(sE_Mail) &  "</td>")
		oFileCurr.writeline ("			</tr>")
		oFileCurr.writeline ("</table>")
	end if
	
	' OBBLIGO DI LEVA 
	if len(sPos_Mil) > 0 and aDati(8) = "1" then
		oFileCurr.writeline ("<table>")
		oFileCurr.writeline ("			<tr>")
		oFileCurr.writeline ("				<td  width=5><li>&nbsp;</li></td>")
		oFileCurr.writeline ("				<td class=textblu>Servicio Militar : " & Trasforma(decCodVal("POMIL","0","",sPos_Mil,"")) &  "</td>")
		oFileCurr.writeline ("			</tr>")
		oFileCurr.writeline ("</table>")
	end if
	oFileCurr.writeline ("</td></tr>")

else
	oFileCurr.writeline ("<tr><td class='textblu' colspan=2>&nbsp;</td></tr>")
end if

	' PRESENTAZIONE 
if nSezioni <> -1 then
	if aSezioni(0) = "1" and len(aCV(6)) > 0 then
		oFileCurr.writeline ("<tr>")
		oFileCurr.writeline ("	<td class='textblu' valign=top >PRESENTACION</td>")
		oFileCurr.writeline ("	<td class='textblu'>")
		oFileCurr.writeline ("		<table>")
		oFileCurr.writeline ("			<tr>")
		oFileCurr.writeline ("				<td  width=5>&nbsp;</td>")
		oFileCurr.writeline ("				<td class=textblu>" & Server.HTMLEncode(aCV(6)) &  "</td>")
		oFileCurr.writeline ("			</tr>")
		oFileCurr.writeline ("		</table>") 
		oFileCurr.writeline ("	</td>")
		oFileCurr.writeline ("</tr>")
	end if
end if
				
' ESPERIENZE FORMATIVE 
if nSezioni > 1 then
	if aSezioni(2) = "1" and len(aCV(9)) > 0 then
		if IsArray(Split(aCV(9),"|")) and aCV(9) <> "0" then
			aFor = Split(aCV(9),"|")
			sDatiTitoli = "'" & Replace(aCV(9),"|","','") & "'"
			oFileCurr.writeline ("<tr>")
			oFileCurr.writeline ("	<td class='textblu' valign=top >NIVEL EDUCATIVO</td>")
			oFileCurr.writeline ("	<td class='textblu'>")
			sqlFormale = "SELECT AA_STUD, COD_STAT_STUD, COD_TIT_STUD, DT_TMST " &_
				" FROM TISTUD WHERE ID_PERSONA = " & id_utente &_ 
				" AND COD_TIT_STUD IN (" & sDatiTitoli & ") " &_
				" ORDER BY AA_STUD DESC ,COD_STAT_STUD"
'PL-SQL * T-SQL  
SQLFORMALE = TransformPLSQLToTSQL (SQLFORMALE) 
			RRFormale.Open sqlFormale, CC								
					
			if  not RRFormale.eof then
				do while not RRFormale.eof	
					oFileCurr.writeline ("<table>")
					oFileCurr.writeline ("	<tr>")
					oFileCurr.writeline ("		<td  width=5><li>&nbsp;</li></td>")
					oFileCurr.writeline ("		<td class=textblu>" & RRFormale.Fields("AA_STUD") & " - " & Trasforma(DecCodVal("TSTUD","0",RRFormale.Fields("DT_TMST"),RRFormale.Fields("COD_TIT_STUD"),0)) & 	" (" & decDiz_Dati(RRFormale.Fields("COD_STAT_STUD"), "COD_STAT_STUD") & ")</td>")
					oFileCurr.writeline ("	</tr>")
					oFileCurr.writeline ("</table>") 
					RRFormale.MoveNext
				loop
			end if
			RRFormale.close
			sqlFormale = ""
		end if
		oFileCurr.writeline ("</td></tr>")
	end if
end if



' ISCRIZIONE ALBO 
if nSezioni > 9 then
	if aSezioni(10) = "1" and len(aCV(16)) > 0 then
		if IsArray(Split(aCV(16),"|")) and aCV(16) <> "0" then
			aFor = Split(aCV(16),"|")
			sDatiTitoli = "'" & Replace(aCV(16),"|","','") & "'"

			oFileCurr.writeline ("<tr>")
			oFileCurr.writeline ("	<td class='textblu' valign=top >MATRICULA PROFESIONAL</td>")
			oFileCurr.writeline ("	<td class='textblu'>")
			sqlFormale = "SELECT COD_ALBO,DT_DEC_ISCR,PRV_ISCR,NUM_ISCR, DT_TMST " &_
				" FROM PERS_ALBO WHERE ID_PERSONA = " & id_utente &_ 
				" AND COD_ALBO IN (" & sDatiTitoli & ") " &_
				" ORDER BY DT_DEC_ISCR DESC "
'PL-SQL * T-SQL  
SQLFORMALE = TransformPLSQLToTSQL (SQLFORMALE) 
			RRFormale.Open sqlFormale, CC								
					
			if  not RRFormale.eof then
				do while not RRFormale.eof	
					oFileCurr.writeline ("<table>")
					oFileCurr.writeline ("	<tr>")
					oFileCurr.writeline ("		<td  width=5><li>&nbsp;</li></td>")
					oFileCurr.writeline ("		<td class=textblu>Inscripci�n n� " & RRFormale.Fields("NUM_ISCR") & " de " & Trasforma(DecCodVal("TALBO","0",RRFormale.Fields("DT_TMST"),RRFormale.Fields("COD_ALBO"),0)) & " el " & ConvDateToString(RRFormale.Fields("DT_DEC_ISCR")) & " (" & RRFormale.Fields("PRV_ISCR") & ") </td>")
					oFileCurr.writeline ("	</tr>")
					oFileCurr.writeline ("</table>") 

					RRFormale.MoveNext
				loop
										
			end if
			RRFormale.close
			sqlFormale = ""
		end if
		oFileCurr.writeline ("</td></tr>")
	end if
end if


				
	' LINGUE 
if nSezioni > 0 then
	if aSezioni(1) = "1" and len(aCV(8)) > 0 then
		if IsArray(Split(aCV(8),"|")) and aCV(8) <> "0" then
			'	aDatiLingue = Split(aCV(8),"|")
			oFileCurr.writeline ("<tr>")
			oFileCurr.writeline ("	<td class='textblu' valign=top >IDIOMAS</td>")
			oFileCurr.writeline ("	<td class='textblu'>")
			sDatiLingue = Replace(aCV(8),"|",",")
			sqlFormale = "SELECT CONOSCENZE.DENOMINAZIONE, PERS_CONOSC.COD_GRADO_CONOSC " &_ 
				" FROM CONOSCENZE, PERS_CONOSC " &_ 
				" WHERE CONOSCENZE.ID_CONOSCENZA = PERS_CONOSC.ID_CONOSCENZA " &_ 
				" AND PERS_CONOSC.ID_PERSONA = " & id_utente &_ 
				" AND PERS_CONOSC.IND_STATUS = 0 " &_ 
				" AND PERS_CONOSC.ID_CONOSCENZA IN (" & sDatiLingue & ") " &_
				" ORDER BY CONOSCENZE.DENOMINAZIONE" 

'PL-SQL * T-SQL  
SQLFORMALE = TransformPLSQLToTSQL (SQLFORMALE) 
			RRFormale.Open sqlFormale, CC
			if not RRFormale.eof then
				do while not RRFormale.eof
					oFileCurr.writeline ("<table>")
					oFileCurr.writeline ("	<tr>")
					oFileCurr.writeline ("		<td width=5><li>&nbsp;</li></td>")
					oFileCurr.writeline ("		<td class=textblu>" & Trasforma(RRFormale.Fields("DENOMINAZIONE")) & " (" & decGradoConosc(CInt(RRFormale.Fields("COD_GRADO_CONOSC"))) & ")</td>")
					oFileCurr.writeline ("	</tr>")
					oFileCurr.writeline ("</table>") 
					RRFormale.MoveNext
				loop
			end if
			RRFormale.Close
			sqlFormale = ""
		end if
		oFileCurr.writeline ("</td></tr>")
	end if
end if

	' CONOSCENZE INFORMATICHE 
if nSezioni > 5 then
	if aSezioni(6) = "1" and len(aCV(12)) > 0 then
		if IsArray(Split(aCV(12),"|")) and aCV(12) <> "0" then
			oFileCurr.writeline ("<tr>")
			oFileCurr.writeline ("	<td class='textblu' valign=top >CONOCIMIENTOS INFORM�TICOS</td>")
			oFileCurr.writeline ("	<td class='textblu'>")
			sConoscenze = Replace(aCV(12),"|",",")
			sqlFormale = "SELECT CONOSCENZE.DENOMINAZIONE, PERS_CONOSC.COD_GRADO_CONOSC " &_ 
				" FROM CONOSCENZE, PERS_CONOSC " &_ 
				" WHERE CONOSCENZE.ID_CONOSCENZA = PERS_CONOSC.ID_CONOSCENZA " &_ 
				" AND PERS_CONOSC.ID_PERSONA = " & id_utente &_
				" AND PERS_CONOSC.IND_STATUS = 0 " &_
				" AND PERS_CONOSC.ID_CONOSCENZA IN (" & sConoscenze & ")" &_
				" ORDER BY CONOSCENZE.DENOMINAZIONE"
						
'PL-SQL * T-SQL  
SQLFORMALE = TransformPLSQLToTSQL (SQLFORMALE) 
			RRFormale.Open sqlFormale, CC
						
			if not RRFormale.eof then
				do while not RRFormale.eof
					oFileCurr.writeline ("<table>")
					oFileCurr.writeline ("	<tr>")
					oFileCurr.writeline ("		<td  width=5><li>&nbsp;</li></td>")
					oFileCurr.writeline ("		<td class=textblu>" & Trasforma(RRFormale.Fields("DENOMINAZIONE")) & " (" & decGradoConosc(CInt(RRFormale.Fields("COD_GRADO_CONOSC"))) & ")</td>")
					oFileCurr.writeline ("	</tr>")
					oFileCurr.writeline ("</table>") 

					RRFormale.MoveNext
				loop
			end if
			RRFormale.Close
			sqlFormale = ""
		end if

		oFileCurr.writeline ("</td></tr>")
	end if
end if

' ESPERIENZE PROFESSIONALI 
if nSezioni > 2 then
	if aSezioni(3) = "1" and len(aCV(10)) > 0 then
		if IsArray(Split(aCV(10),"|")) and aCV(10) <> "0" then
			aPro = Split(aCV(10),"|")
			sIn = "("
			for cont = 0 to ubound(aPro)
				sMansione = Split(aPro(cont),"$",-1,1)(0)
				sIn = sIn & "'" &  sMansione & "',"
			next
			sIn = mid(sIn,1,len(sIn)-1)
			sIn = sIn & ")"
			
			sqlFormale = "SELECT ESPRO.RAG_SOC AS AZIENDA, ESPRO.DT_INI_ESP_PRO AS DI, nvl(ESPRO.DT_FIN_ESP_PRO,to_Date('31/12/9999','dd/mm/yyyy')) AS DF, ESPRO.COD_MANSIONE AS MAN " &_ 
				" FROM ESPRO " &_ 
				" WHERE ID_PERSONA = " & id_utente &_ 
				" AND ESPRO.COD_MANSIONE IN " & sIn  &_
				" ORDER BY DT_FIN_ESP_PRO desc, ESPRO.DT_INI_ESP_PRO desc "
'PL-SQL * T-SQL  
SQLFORMALE = TransformPLSQLToTSQL (SQLFORMALE) 
			RRFormale.Open sqlFormale, CC
			if  not RRFormale.eof then
				oFileCurr.writeline ("<tr>")
				oFileCurr.writeline ("	<td class='textblu' valign=top >ANTECEDENTES LABORALES</p></td>")
				oFileCurr.writeline ("	<td class='textblu'>")
						
				do while not RRFormale.eof
					oFileCurr.writeline ("<table>")
					oFileCurr.writeline ("	<tr>")
					oFileCurr.writeline ("		<td  width=5><li>&nbsp;</li></td>")
					oFileCurr.writeline ("		<td class=textblu>Del "  & ConvDateToString(RRFormale.Fields("DI")))
					if RRFormale.Fields("DF") <> ConvStringToDate("31/12/9999") then
						oFileCurr.writeline (" al " & RRFormale.Fields("DF"))
					else
						oFileCurr.writeline (" a la fecha ")
					end if
					oFileCurr.writeline (" - ")
					if RRFormale.Fields("MAN") <> "" then
						sMansi = "SELECT MANSIONE FROM CODICI_ISTAT WHERE CODICE='" & RRFormale.Fields("MAN") & "'"			
'PL-SQL * T-SQL  
SMANSI = TransformPLSQLToTSQL (SMANSI) 
						set rsMansi = cc.execute(sMansi)
						if not rsMansi.eof then		
							oFileCurr.writeline ("como " & lcase(TRIM(rsMansi("MANSIONE"))))
						end if
						rsMansi.close
						set rsMansi=nothing								
					end if
					if RRFormale.Fields("AZIENDA") <> "" and ucase(RRFormale.Fields("AZIENDA")) <> "NON DICHIARATA" then
					  oFileCurr.writeline (" en " & RRFormale.Fields("AZIENDA"))
					end if
					oFileCurr.writeline ("		</td>")
					oFileCurr.writeline ("	</tr>")
					oFileCurr.writeline ("</table>")
					RRFormale.MoveNext
				loop
			end if
			RRFormale.Close
			sqlFormale = ""
			oFileCurr.writeline ("</td></tr>")
		end if
	end if
end if
					
	' DISPONIBILITA' 
if nSezioni > 6 then
	if aSezioni(7) = "1" and len(aCV(13)) > 0 then
		if IsArray(Split(aCV(13),"|")) and aCV(13) <> "0" then
			sDisponibilita = "'" & Replace(aCV(13),"|","','") & "'"
			sCondizione = "codice IN (" & sDisponibilita & ")"
			sTabella = "TDISP"
			dData	 = Date()
			nOrder   = 2
			Isa		 = 0
			aDisponibilita = decodTadesToArray(sTabella,dData,sCondizione,nOrder,Isa)
		
			nCodArea = 00
			if isArray(aDisponibilita) then
				oFileCurr.writeline ("<tr>")
				oFileCurr.writeline ("	<td class='textblu' valign=top >DISPONIBILIDADES/LIMITACIONES</p></td>")
				oFileCurr.writeline ("	<td class='textblu'>&nbsp;</td>")
				oFileCurr.writeline ("</tr>")
				for i = 0 to ubound(aDisponibilita) -1	
					if clng(aDisponibilita(i,i+1,i+1)) <> clng(nCodArea) then		
						if i <> 0 then oFileCurr.writeline ("</td></tr>") end if
							oFileCurr.writeline ("<tr><td class='textblu' valign=top >" & Trasforma(DecCodVal("CL_VD", 0, Date(),aDisponibilita(i,i+1,i+1),0)) &_
								 "</td><td class='textblu'>")
							nCodArea = aDisponibilita(i,i+1,i+1)
					end if	
					oFileCurr.writeline ("<table>")
					oFileCurr.writeline ("	<tr>")
					oFileCurr.writeline ("		<td  width=5><li>&nbsp;</li></td>")
					oFileCurr.writeline ("		<td class=textblu>" & Trasforma(aDisponibilita(i,i+1,i)) & "</td>")
					oFileCurr.writeline ("	</tr>")
					oFileCurr.writeline ("</table>") 
				next
				Erase aDisponibilita
				oFileCurr.writeline ("<br></td></tr>")
			end if
		end if
	end if
end if

'PATENTI'		
if nSezioni > 7 then	
	if aSezioni(8) = "1" and len(aCV(14)) > 0 then
		if IsArray(Split(aCV(14),"|")) and aCV(14) <> "0" then
			aPat = Split(aCV(14),"|")
			sPatenti = "'" & Replace(aCV(14),"|","','") & "'"
			oFileCurr.writeline ("<tr>")
			oFileCurr.writeline ("	<td class='textblu' valign=top >LICENCIAS DE CONDUCCI�N</p></td>")
			oFileCurr.writeline ("	<td class='textblu'>")
			sqlFormale = "SELECT COD_PERM, DT_TMST " & _
				" FROM AUTCERT " & _
				" WHERE ID_PERSONA = " & id_utente & _
				" AND COD_PERM IN (" & sPatenti & ")" &_
				" AND nvl(DT_SCADENZA, to_date('31/12/9999', 'dd/mm/yyyy')) >= " &  convDateToDB(date)
						
'PL-SQL * T-SQL  
SQLFORMALE = TransformPLSQLToTSQL (SQLFORMALE) 
			RRFormale.Open sqlFormale, CC

			if  not RRFormale.eof then
				do while not RRFormale.eof	
					oFileCurr.writeline ("<table>")
					oFileCurr.writeline ("	<tr>")
					oFileCurr.writeline ("		<td  width=5><li>&nbsp;</li></td>")
					oFileCurr.writeline ("		<td class=textblu>" & Trasforma(DecCodVal("TPERM","0",RRFormale.Fields("DT_TMST"),RRFormale.Fields("COD_PERM"),0)) &  "</td>")
					oFileCurr.writeline ("	</tr>")
					oFileCurr.writeline ("</table>") 						
					RRFormale.MoveNext
				loop
			end if
			RRFormale.Close
			sqlFormale = ""
			oFileCurr.writeline ("</td></tr>")
		end if
	end if
end if	
		
'FORMAZIONE'
if nSezioni > 8 then	
	if aSezioni(9) = "1" and len(aCV(15)) > 0 then
		if IsArray(Split(aCV(15),"|")) and aCV(15) <> "0" then
			aFormaz = Split(aCV(15),"|")
			sFormazione = "'" & Replace(aCV(15),"|","','") & "'"
			oFileCurr.writeline ("<tr>")
			oFileCurr.writeline ("	<td class='textblu' valign=top >CAPACITACI�N Y FORMACI�N PROFESIONAL</p></td>")
			oFileCurr.writeline ("	<td class='textblu'>")
			sqlFormale = "SELECT F.ID_FORMAZIONE, F.COD_TCORSO, F.AA_CORSO, F.COD_STATUS_CORSO, A.DENOMINAZIONE " & _
				" FROM FORMAZIONE F, AREA_CORSO A " & _
				" WHERE A.ID_AREA = F.ID_AREA " & _
				" AND ID_PERSONA = " & id_utente &_
				" AND ID_FORMAZIONE IN (" & sFormazione & ")" &_
				" ORDER BY F.AA_CORSO DESC"
			
'PL-SQL * T-SQL  
SQLFORMALE = TransformPLSQLToTSQL (SQLFORMALE) 
			RRFormale.Open sqlFormale, CC

			if  not RRFormale.eof then
				do while not RRFormale.eof	
					oFileCurr.writeline ("<table>")
					oFileCurr.writeline ("	<tr>")
					oFileCurr.writeline ("		<td  width=5><li>&nbsp;</li></td>")
					oFileCurr.writeline ("		<td class=textblu>" &  RRFormale.Fields("AA_CORSO") & " - "  & RRFormale.Fields("DENOMINAZIONE") & " - " & LCASE(RRFormale.Fields("COD_TCORSO")) & " (")
					select case RRFormale.Fields("COD_STATUS_CORSO")
						case "0"
							oFileCurr.writeline ("complet�")
						case "1"
							oFileCurr.writeline ("abandon�")
						case "2"
							oFileCurr.writeline ("en curso")
					end select
					oFileCurr.writeline (")")
					oFileCurr.writeline ("		</td>")
					oFileCurr.writeline ("	</tr>")
					oFileCurr.writeline ("</table>")
					RRFormale.MoveNext
				loop
			end if
			RRFormale.Close
			sqlFormale = ""
			oFileCurr.writeline ("</td></tr>")
		end if
	end if
end if
			
' ALTRE INFO '
if nSezioni > 4 then
	if aSezioni(5) = "1" then
		if IsArray(Split(aCV(11),"|")) and len(aCV(11)) > 0 then
			oFileCurr.writeline ("<tr>")
			oFileCurr.writeline ("	<td class='textblu' valign=top >INFORMACION ADICIONAL</td>")
			oFileCurr.writeline ("	<td class='textblu'>")

			aInfo = Split(aCV(11),"|")
			for cont = 0 to UBound(aInfo)
				oFileCurr.writeline ("<table>")
				oFileCurr.writeline ("	<tr>")
				oFileCurr.writeline ("		<td  width=5><li>&nbsp;</li></td>")
				oFileCurr.writeline ("		<td class=textblu>" & Server.HTMLEncode(aInfo(cont)) &  "</td>")
				oFileCurr.writeline ("	</tr>")
				oFileCurr.writeline ("</table>") 
			next
			oFileCurr.writeline ("</td></tr>")
		end if
	end if
end if
			
' OBIETTIVI
if nSezioni > 3 then
	if aSezioni(4) = "1" and len(aCV(7)) > 0 then
		oFileCurr.writeline ("<tr>")
		oFileCurr.writeline ("	<td class='textblu' valign=top >OBJETIVOS</td>")
		oFileCurr.writeline ("	<td class='textblu'>")
		oFileCurr.writeline ("		<table>")
		oFileCurr.writeline ("			<tr>")
		oFileCurr.writeline ("				<td  width=5>&nbsp;</td>")
		oFileCurr.writeline ("				<td class=textblu>" & Server.HTMLEncode(aCV(7)) &  "</td>")
		oFileCurr.writeline ("			</tr>")
		oFileCurr.writeline ("		</table>")
		oFileCurr.writeline ("	</td>")
		oFileCurr.writeline ("</tr>")
	end if
end if
			
' PRIVACY 
oFileCurr.writeline ("<tr>")
oFileCurr.writeline ("	<td class='textblu' colspan=2>&nbsp;</td>")
oFileCurr.writeline ("</tr>")
oFileCurr.writeline ("<tr>")
oFileCurr.writeline ("	<td class='textblu' colspan=2><B>")
'oFileCurr.writeline ("Dichiaro di essere stato informato, ai sensi e per gli effetti di cui all'art'10 della legge 675/96 che i dati personali raccolti saranno trattati, anche con strumenti informatici, esclusivamente nell'ambito del procedimento per il quale la presente dichiarazione viene resa.")		
'oFileCurr.writeline ("Dichiaro di essere stato informato, ai sensi e per gli effetti di cui all'art. 13 del D. Lgs. 196/2003 che i dati personali raccolti saranno trattati, anche con strumenti informatici, esclusivamente nell'ambito del procedimento per il quale la presente dichiarazione viene resa.")		
oFileCurr.writeline ("	</B></td>")
oFileCurr.writeline ("</tr>")
oFileCurr.writeline ("	</table>")
oFileCurr.writeline ("</td>")
oFileCurr.writeline ("</tr>")

'Rilascio il recordset
set RRFormale = nothing
%>
