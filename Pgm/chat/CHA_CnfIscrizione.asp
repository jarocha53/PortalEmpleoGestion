<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%
Sub Cancella_Pubblico()
	'Inserimento nella tabella FRM_ISCRIZIONE
	dim nIdArea, bCanale, nRec
	dim sErrore, sql, RR, i, j, sqlFrmIscri,sqlDELETE

	CC.BeginTrans
	' Eliminazione dell'occorrenza relativa al canale di forum "Pubblico"
	sqlDELETE = "DELETE FROM frm_iscrizione WHERE cod_ruolo IS NULL AND id_frm_area=" & nID
	sErrore = EseguiNoC(sqlDELETE,CC) 	

	if sErrore = "0" then
		for i = 0 to  n-1
			' So quanti sottogruppi esistono per quel rorga.
			' Response.Write "<BR>" & Request.form("chkidgru" & i ).Count 
			' Se non esistono non ci sono sotto gruppi, altrimenti
			' devo fare tante insert quanti sono i sottogruppi.
			if Trim(Request.form("txtDtDal" & i)) <> "" or Request.form("chkidgru" & i ).Count > 0 then
				if Request.form("chkRuo" & i) <> "" then
					sRuolo = Request.form("chkRuo" & i)
				else
					sRuolo = Request.form("HchkRuo" & i)
				end if
	
				if Request.form("chkidgru" & i ).Count = 0 then
					sql = "INSERT INTO FRM_ISCRIZIONE (ID_FRM_AREA, COD_RUOLO, DT_DAL, TIPO, DT_TMST" 
					if isDate(Request.form("txtDtAl" & i)) then
						sql = sql & ",DT_AL"
					end if
					sql = sql & ") VALUES (" & nId
					sql = sql & ", '" & sRuolo & "'," & ConvDateToDbS(ConvStringToDate(Request.form("txtDtDal" & i))) & "," 
					sql = sql & "'C', " & ConvDateToDb(Now()) 
					if isDate(Request.form("txtDtAl" & i)) then
						sql = sql & "," & ConvDateToDbS(ConvStringToDate(Request.form("txtDtAl" & i)))
					end if
					sql = sql & ")"

					sErrore = EseguiNoC(sql,CC)				
				else
					
					if sErrore <> "0" then
						exit for
					end if	 

					sIDGrup = 0 
					sIDGrup = Trim(Request.form("chkidgru" & i ))
					if sIDGrup <> "" then
						sRuolo = Request.Form("RorgaGru" & i)
						aIDGrup = Split(sIDGrup,",")
						sIDIniVal = Trim(Request.form("txtDtDalGru" & i ))
						sIDFinVal = Trim(Request.form("txtDtAlGru" & i ))

						aIDIniVal = Split(sIDIniVal,",")
						aIDFinVal = Split(sIDFinVal,",")

						if ubound(aIDFinVal) = -1 then
							redim aIDFinVal(0)
							aIDFinVal(0) = ""
						end if

						sAppo = ""
						sAppo2 = ""
						for zz = 0 to UBound(aIDIniVal)
							if Trim(aIDIniVal(zz)) <> "" then
								sAppo = sAppo & Trim(aIDIniVal(zz)) & ","
								sAppo2 = sAppo2 & Trim(aIDFinVal(zz)) & ","
							end if 
						next 
						sAppo	= Left(sAppo,Len(sAppo)-1)
						sAppo2	= Left(sAppo2,Len(sAppo2)-1)
						aIDIniVal = Split(sAppo,",")
						aIDFinVal = Split(sAppo2,",")

						if ubound(aIDFinVal) = -1 then
							redim aIDFinVal(0)
							aIDFinVal(0) = ""
						end if


						' Mi formatto le date per inserirle nel DB.
						for nGrup = 0 to UBound(aIDGrup) 
							aIDIniVal(nGrup) = ConvDateToDBs(ConvStringToDate(aIDIniVal(nGrup)))
							if aIDFinVal(nGrup) <> "" then
								aIDFinVal(nGrup) = ConvDateToDBs(ConvStringToDate(aIDFinVal(nGrup)))
							else
								aIDFinVal(nGrup) = "null"
							end if
							sqlFrmIscri = "INSERT INTO FRM_ISCRIZIONE (ID_FRM_AREA, COD_RUOLO, DT_DAL,DT_AL, TIPO,IDGRUPPO, DT_TMST) " &_
								  "VALUES (" &  nId & ",'" & sRuolo & "'," &_
								  aIDIniVal(nGrup) & "," & aIDFinVal(nGrup) & ",'C'," & aIDGrup(nGrup) & ",SYSDATE)"
							sErrore = EseguiNoC(sqlFrmIscri,CC)
							if sErrore <> "0" then
								exit for
							end if	
						next
					end if 
				end if
			end if
			if sErrore <> "0" then
				exit for
			end if	
			sql = ""				
		next
	end if

	if sErrore <> "0" then
		CC.RollbackTrans
		strErrore=sErrore
	else
		CC.CommitTrans
	end if%><br><br>
<%	 if sErrore <> "0" then%>			
		<table border="0" cellspacing="0" cellpadding="0" width="570">
			<tr align="center"> 
				<td class="tbltext3">
					<b>Errore. <%=strErrore%></b>
			    </td>
			</tr>
		</table>		
		<br><br>
		<table border="0" cellspacing="0" cellpadding="0" width="570">
			<tr> 
				<td align="center"> 
					<a class="textred" href="javascript:history.back()" onmouseover="window.status =' '; return true">
					<b>Pagina precedente </b></a>
				</td>
				<td align="center"> 
					<a class="textred" href="/Pgm/Chat/CHA_VisCanali.asp" onmouseover="window.status =' '; return true">
					<b>Torna all'elenco delle stanze</b></a>
				</td>
			</tr>
		</table>
<%	else %>			
		<table border="0" cellspacing="0" cellpadding="0" width="570">
			<tr align="center"> 
				<td class="tbltext3">
					<b>
					<%=sOperazione%> correttamente effettuata.
		            </b>
		        </td>
			</tr>
		</table>		
		<br><br>
		<meta HTTP-EQUIV="Refresh" CONTENT="2; URL=/Pgm/Chat/CHA_VisCanali.asp">
<%	end if
End Sub

Sub Inserisci_Pubblico()

	dim nIdArea, sDT_Dal, sDt_Al
	dim sErrore, sql, RR, i, j
		
	sDt_Dal = Request.form("txtDtDalPN")
	sDt_Al =  Request.form("txtDtAlPN")
	CC.BeginTrans
	sqlD = "DELETE FROM FRM_ISCRIZIONE where  ID_FRM_AREA=" & nID 
	sErrore = EseguiNoC(sqlD,CC)	
	if sErrore = "0" then		
		sql = "INSERT INTO FRM_ISCRIZIONE (ID_FRM_AREA, DT_DAL, TIPO, DT_TMST"
		if isdate(sDt_Al) then
			sql = sql & ",DT_AL"
		end if
		sql = sql & ") Values(" & nId
		sql = sql & "," & ConvDateToDbS(ConvStringToDate(sDt_Dal)) & "," 
		sql = sql & "'C', " & ConvDateToDb(Now()) 
		if isdate(sDt_Al) then
			sql = sql & ","  & ConvDateToDbS(ConvStringToDate(sDt_Al)) 
		end if
		sql = sql & ")"
							
		sErrore = EseguiNoC(sql,CC)
	end if
	if sErrore <> "0" then
		CC.RollbackTrans
		strErrore=sErrore
	else
		CC.CommitTrans
	end if%>	
	<br><br>
<%	if sErrore <> "0" then %>
		<table border="0" cellspacing="0" cellpadding="0" width="570">
			<tr align="center"> 
				<td class="tbltext3">
					<b>Errore. <%=strErrore%></b>
		        </td>
			</tr>
		</table>		
		<br><br>
		<table border="0" cellspacing="0" cellpadding="0" width="570">
			<tr> 
				<td align="center"> 
					<a class="textred" href="javascript:history.back()" onmouseover="window.status =' '; return true">
					<b>Pagina precedente </b></a>
				</td>
				<td align="center"> 
					<a class="textred" href="/Pgm/Chat/CHA_VisCanali.asp" onmouseover="window.status =' '; return true">
					<b>Torna alle Categorie </b></a>
				</td>
			</tr>
		</table>
<%	else %>			
		<table border="0" cellspacing="0" cellpadding="0" width="570">
			<tr align="center"> 
				<td class="tbltext3">
					<b><%=sOperazione%> correttamente effettuata.</b>
		        </td>
			</tr>
		</table>		
		<br><br>
		<meta HTTP-EQUIV="Refresh" CONTENT="2; URL=/Pgm/Chat/CHA_VisCanali.asp">
<%	end if
			
End Sub 

Sub Modifica()
		'Inserimento nella tabella FRM_ISCRIZIONE
	dim nIdArea, bCanale, sPDt_Dal, sPDt_Al
	dim sErrore, sql, RR, i, j
	sPN = request.form("chkPN")
	sPS = request.form("chkPS")
	sErrore = "0" 
	R = 0
	for i = 0 to n-1
		If Trim(Request.form("txtDtDal" & i)) <> "" then
			R = R + 1
		end if
		If Trim(Request.form("chkIdGru" & i)) <> "" then ' Se esistono sottogruppi.
			R = R + 1
		end if

	next

	if sPN <> "" then
		'Si vuol modificare un canale di forum trasformandolo in pubblico
		Inserisci_Pubblico()
		exit sub	
	end if
	if sPS <> "Pubblico" and R <> 0 then 
		'Si vuol modificare un canale di forum trasformandolo da pubblico a privato
		Cancella_Pubblico()
		exit sub
	end if		
	if sPS = "XX" then	
		'Si vuol modificare un canale pubblico
		sPDt_Dal = Request.form("txtDtDalPS")
		sPDt_Al =  Request.form("txtDtAlPS")
		sql = "UPDATE frm_iscrizione SET DT_DAL =" & ConvDateToDbS(convStringToDate(sPDt_Dal)) & "," 
		if isDate(sPDt_Al) then
			sql = sql & " DT_AL  =" & ConvDateToDbS(convStringToDate(sPDt_Al))  & "," 
		end if
		sql = sql & " TIPO = 'C',"
		sql = sql & " DT_TMST=" & ConvDateToDb(Now())
		sql = sql & " WHERE ID_FRM_AREA =" & nID & " and COD_RUOLO is null"
		sErrore = EseguiNoC(sql,CC)
		if sErrore <> "0" then
			sErrore=decod_errore
		end if
	else
		CC.BeginTrans
		
		for i = 0 to n-1
			if Request.form("chkRuo" & i) <> "" then
				sRuolo = Request.form("chkRuo" & i)
			else
				sRuolo = Request.form("HchkRuo" & i)
			end if	
			if sRuolo = "" then
				sRuolo = Request.Form("RorgaGru" & i)
			end if
			if Trim(Request.Form("txtDtDal" & i)) <> "" and Trim(Request.form("chkIdGru" & i )) = "" then

				sqlI = "SELECT id_frm_area FROM frm_iscrizione WHERE id_frm_area =" & nID
				sqlI = sqlI & " AND cod_ruolo ='" & sRuolo & "' AND idgruppo IS NULL "
'PL-SQL * T-SQL  
SQLI = TransformPLSQLToTSQL (SQLI) 
				set RRI = CC.Execute(sqlI)
				if not RRI.Eof then
					sql = "UPDATE frm_iscrizione SET "
					sql = sql & " DT_DAL =" & ConvDateToDbS(ConvStringToDate(Request.form("txtDtDal" & i))) & "," 
					if isDate(Request.form("txtDtAl" & i)) then
						sql = sql & " DT_AL  =" & ConvDateToDbS(ConvStringToDate(Request.form("txtDtAl" & i)))  & "," 
					end if
					sql = sql & " TIPO = 'C',"
					sql = sql & " DT_TMST=" & ConvDateToDb(Now())
					sql = sql & " WHERE ID_FRM_AREA =" & nID & " and COD_RUOLO ='" & sRuolo & "'"
				else
					
					sqlI = "DELETE FRM_ISCRIZIONE WHERE ID_FRM_AREA =" & nID
					sqlI = sqlI & " AND COD_RUOLO ='" & sRuolo & "'"
					
					sErrore = EseguiNoC(sqlI,CC)
					if sErrore <> "0" then
						strErrore=sErrore
						exit for
					end if

					sql = "INSERT INTO FRM_ISCRIZIONE (ID_FRM_AREA, COD_RUOLO, DT_DAL, TIPO, DT_TMST"
					if isdate(Request.form("txtDtAl" & i)) then
						sql = sql & ",DT_AL"
					end if
					sql = sql & ") VALUES(" & nId
					sql = sql & ", '" & sRuolo & "'," & ConvDateToDbS(ConvStringToDate(Request.form("txtDtDal" & i))) 
					sql = sql & ", 'C', " & ConvDateToDb(Now())
					if isdate(Request.form("txtDtAl" & i)) then
						sql = sql & "," & ConvDateToDbS(ConvStringToDate(Request.form("txtDtAl" & i)))
					end if
					sql = sql & ")"
				end if
				sErrore = EseguiNoC(sql,CC)
				if sErrore <> "0" then
					strErrore=sErrore
				end if
			else
				if Trim(Request.form("chkIdGru" & i )) <> "" then 
					' Se � valido st� facendo un inserimento
					sSQL = "DELETE FROM FRM_ISCRIZIONE WHERE COD_RUOLO = '" &_
						   sRuolo & "' AND IDGRUPPO IS NULL AND ID_FRM_AREA = " & nId
					sErrore = EseguiNoC(sSQL,CC)
					sIDGrup = Trim(Request.form("chkIdGru" & i ))

					sPosGruppi	= Trim(Request.form("Gruppo" & i ))
					aPos		= Split(sPosGruppi,",")
					aIDGrup		= Split(sIDGrup,",")
					sIDIniVal	= Trim(Request.form("txtDtDalGru" & i ))
					sIDFinVal	= Trim(Request.form("txtDtAlGru" & i ))
					aIDIniVal	= Split(sIDIniVal,",")
					aIDFinVal	= Split(sIDFinVal,",")

					if ubound(aIDFinVal) = -1 then
						redim aIDFinVal(0)
						aIDFinVal(0) = ""
					end if					

					' Nel caso in cui faccio un inserimento  
					' prendo le date inizio e date fine corrette.
					' Infatti nel caso in cui ci siano tre elementi 
					' di cui due gi� inseriti devo prendere i dati 
					' dell'ultimo (DATA INIZIO e DATA FINE),  
					' nella split  ** aIDIniVal = Split(sIDIniVal,",") ** e nella
					' split ** aIDFinVal = Split(sIDFinVal,",") ** ho tutte le date.

					redim aSort(UBound(aIDGrup))
					redim aSortDateDal(UBound(aIDGrup))
					redim aSortDateAl(UBound(aIDGrup))
					nPos = 0 
					do while not ( nPos > UBound(aIDGrup) or nPos >  UBound(aPos) )
						for nData = 0 to UBound(aPos)
							if clng(aPos(nData)) = clng(aIDGrup(nPos)) then
								aSort(nPos)			= CLNG(aIDGrup(nPos))
								aSortDateDal(nPos)	= Trim(aIDIniVal(nData))
								aSortDateAl(nPos)	= Trim(aIDFinVal(nData))
								nPos = nPos + 1
							end if
							if nPos > UBound(aIDGrup) then
								exit for
							end if							
						next			

					loop	
	
					' Mi formatto le date per inserirle nel DB.
					for nGrup = 0 to UBound(aSort) 
						aSortDateDal(nGrup) = ConvDateToDBs(ConvStringToDate(aSortDateDal(nGrup)))
						if aSortDateAl(nGrup) <> "" then
							aSortDateAl(nGrup) = ConvDateToDBs(ConvStringToDate(aSortDateAl(nGrup)))
						else				
							aSortDateAl(nGrup) = "null"
						end if
						sqlFrmIscri = "INSERT INTO FRM_ISCRIZIONE (ID_FRM_AREA, COD_RUOLO, DT_DAL,DT_AL, TIPO, IDGRUPPO, DT_TMST) " &_
							          " VALUES (" &  nId & ",'" & sRuolo & "'," &_
							  aSortDateDal(nGrup) & "," & aSortDateAl(nGrup) & ",'C'," & CLng(aIDGrup(nGrup)) & ",SYSDATE)"
						sErrore = EseguiNoC(sqlFrmIscri,CC)
						if sErrore <> "0" then
							exit for
						end if	
					next
				else ' Aggiorno i dati che ci sono 
					sqlI = ""
					sqlFrmIscri = ""
					sIDGrup = Trim(Request.form("CheckGru" & i ))

					if Trim(Replace(sIDGrup,",","")) <> "" then
						sqlI = "SELECT COUNT(*) as Conta from FRM_ISCRIZIONE WHERE ID_FRM_AREA = " & nID
						sqlI = sqlI & " AND COD_RUOLO ='" & sRuolo & "' AND IDGRUPPO in (" & sIDGrup & ")" 

'PL-SQL * T-SQL  
SQLI = TransformPLSQLToTSQL (SQLI) 
						set rsCheck = cc.Execute(sqlI) 
						if cint(rsCheck("Conta")) > 0 then
							aIDGrup = Split(Trim(sIDGrup),",")
							sIDIniVal = Trim(Request.form("txtDtDalGru" & i ))
							sIDFinVal = Trim(Request.form("txtDtAlGru" & i ))
							aIDIniVal = Split(sIDIniVal,",")
							aIDFinVal = Split(sIDFinVal,",")
							if ubound(aIDFinVal) = -1 then
								redim aIDFinVal(0)
								aIDFinVal(0) = ""
							end if
							sAppo = ""
							sAppo2 = ""
							for zz = 0 to UBound(aIDIniVal)
								if Trim(aIDIniVal(zz)) <> "" then
									sAppo = sAppo & Trim(aIDIniVal(zz)) & ","
									sAppo2 = sAppo2 & Trim(aIDFinVal(zz)) & ","
								end if 
							next 
							sAppo = left(sAppo,len(sAppo)-1)
							sAppo2 = left(sAppo2,len(sAppo2)-1)
							aIDIniVal = Split(sAppo,",")
							aIDFinVal = Split(sAppo2,",")
							if ubound(aIDFinVal) = -1 then
								redim aIDFinVal(0)
								aIDFinVal(0) = ""
							end if
							' Mi formatto le date per inserirle nel DB.
							for nGrup = 0 to UBound(aIDGrup) 
								aIDIniVal(nGrup) = ConvDateToDBs(ConvStringToDate(aIDIniVal(nGrup)))
								if aIDFinVal(nGrup) <> "" then
									aIDFinVal(nGrup) = ConvDateToDBs(ConvStringToDate(aIDFinVal(nGrup)))
								else				
									aIDFinVal(nGrup) = "null"
								end if
								sqlFrmIscri = "UPDATE FRM_ISCRIZIONE SET DT_DAL=" & aIDIniVal(nGrup) &_
									          ",DT_AL = " & aIDFinVal(nGrup) & ",DT_TMST = SYSDATE " &_
									          " WHERE ID_FRM_AREA = " & nId  & " AND IDGRUPPO in " & CLng(aIDGrup(nGrup))
								sErrore = EseguiNoC(sqlFrmIscri,CC)
								'Response.Write "<BR> Errore = " & sErrore
								if sErrore <> "0" then
									exit for
								end if	
							next
						end if
						rsCheck.close
						set rsCheck=nothing			
					end if
				end if 	
			end if
		next

		if sErrore <> "0" then
			CC.RollbackTrans
			strErrore=sErrore
		else
			CC.CommitTrans
		end if
	end if%>		
	<br>
	<br>
<%	if sErrore <> "0" then%>
		<table border="0" cellspacing="0" cellpadding="0" width="570">
			<tr align="center"> 
				<td class="tbltext3">
					<b>
					Errore. <%=strErrore%>
	                </b>
	            </td>
			</tr>
		</table>		
		<br><br>
		<table border="0" cellspacing="0" cellpadding="0" width="570">
			<tr> 
				<td align="center"> 
					<a class="textred" href="javascript:history.back()" onmouseover="window.status =' '; return true">
					<b>Pagina precedente </b></a>
				</td>
				<td align="center"> 
					<a class="textred" href="/Pgm/Chat/CHA_VisCanali.asp" onmouseover="window.status =' '; return true">
					<b>Torna alle Categorie </b></a>
				</td>
			</tr>
		</table>
<%	else %>			
		<table border="0" cellspacing="0" cellpadding="0" width="570">
			<tr align="center"> 
				<td class="tbltext3">
					<b>
					<%=sOperazione%> correttamente effettuata.
	                </b>
	            </td>
			</tr>
		</table>		
		<br><br>
		<meta HTTP-EQUIV="Refresh" CONTENT="2; URL=/Pgm/Chat/CHA_VisCanali.asp">
<%	end if
			
End Sub
%>

<!--#include virtual ="/util/portallib.asp"-->
<!--#include virtual ="/strutt_testa1.asp"-->
<!--#include Virtual="/include/openconn.asp"-->
<!--#include Virtual="/include/DecCod.asp"-->
<!--#include Virtual="/include/ControlDateVb.asp"-->
<!--#include Virtual="/include/SysFunction.asp"-->
<%
Dim strConn, Rs, sql
Dim sDescr, sOggetto, n, nId
dim sOperazione
nID = clng(request("cat")) ' ID della categoria.
n	= clng(Request("n"))   ' Numero dei codici rorga.
sAction = Request.Form ("Action")

if sAction = "INS" then
	sOperazione = "Inserimento"
	nID = InserisciCanale()
else
	sOperazione = "Modifica"
end if
if isNumeric(nID) then
	Modifica()
else
Response.Write "Errore"
end if

function InserisciCanale()
		'Inserimento nella tabella FRM_AREA
	
		dim nIdArea, sDescr, bCanale, sOggetto
		dim sErrore, sql, RR, n, i, j


		sDescr  = Replace(Request.Form("txtDescr"), "'", "''")
		sOggetto= Replace(Request.Form("txtCanale"), "'", "''")
		n = Request("n")
		
		CC.BeginTrans
			
		dt_tmst = ConvDateToDb(Now())
		sql = "INSERT INTO FRM_AREA (TITOLO_AREA, DESC_AREA, IMG_AREA, DT_TMST, TIPO) VALUES " &_
			  "('" & sOggetto & "','" & sDescr & "',' '," & dt_Tmst & ", 'C')"
		
		sErrore=EseguiNoC(Sql,CC)		

		if sErrore <> "0" then
			InserisciCanale = sErrore
			CC.RollbackTrans
		else
			sql = "SELECT id_frm_area FROM frm_area WHERE dt_tmst=" & dt_Tmst
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
			set RR = CC.Execute (sql)		
			nIdAreaFrm = clng(RR("ID_FRM_AREA"))
			RR.Close
			set RR = nothing
			InserisciCanale = nIdAreaFrm
			CC.CommitTrans 
		end if

end function 
%>
<!--#include Virtual="/include/closeconn.asp"-->
<!--#include Virtual="/strutt_coda1.asp"-->
