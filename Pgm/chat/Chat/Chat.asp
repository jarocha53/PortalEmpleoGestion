<%
Option Explicit 
%>
<!-- #include file="constants.inc" -->
<%


Dim ChatID, ChatRoom, Nuova, IP, ChatName, x, Room, imag, moder
ChatID = Request("ChatID")
ChatRoom = Request("ChatRoom")
Room = Request("Room")

moder = Request("moder")

If Len(ChatRoom) = 1 Then
	ChatRoom = "0" & ChatRoom
End If
 
Nuova = Request("Nuova")
ChatName = Request("ChatName")
imag = Request("imag")

If Len(ChatName) < 2 Then
	ChatName = Request("requestedUsername")
End If
	
If (ChatID = "") Then
	Response.Redirect "Expired.asp"
	Response.End
End If

If Nuova = "SI" Then 

	Application.Lock

'************* Notifica l'uscita del vecchio utente
	
	Room = mid(ChatName,1,2)

	For x = 1 To MESSAGES Step + 1
		Application("chatline_" & x) = Application("chatline_" & x+1)
	Next
		Application("chatline_" & MESSAGES) = "<tr><td colspan='2' class='Name' align='left' ><span class=textred >  <b>" & ChatName & "</b> esce alle : " & FormatDateTime(Now, 4) & "</span></td></tr>"

	Application.StaticObjects.Item("ASPChat").Remove(CStr(ChatID))
	Application.StaticObjects.Item("ASPChatTime").Remove(CStr(ChatID))
	Application.StaticObjects.Item("ASPChatIP").Remove(CStr(ChatID))
	Application.StaticObjects.Item("ASPChatRoom").Remove(CStr(ChatID))
	Application.StaticObjects.Item("ASPChatIMG").Remove(CStr(ChatID))
	Application.StaticObjects.Item("ASPChatORA").Remove(CStr(ChatID))
	Application.StaticObjects.Item("ASPChatMIN").Remove(CStr(ChatID))
	
	Response.Cookies("CHAT") = ""


	IP = request.servervariables("REMOTE_ADDR")
	ChatID = CStr(Timer) 
	ChatName = ChatRoom & mid(ChatName,3)
			
	Application.StaticObjects.Item("ASPChat").Add ChatID, ChatName
	Application.StaticObjects.Item("ASPChatTime").Add ChatID, CStr(Now())
	Application.StaticObjects.Item("ASPChatIP").Add ChatID, IP
	Application.StaticObjects.Item("ASPChatRoom").Add ChatID, ChatRoom
	Application.StaticObjects.Item("ASPChatIMG").Add ChatID, imag
	Application.StaticObjects.Item("ASPChatORA").Add ChatID, hour(Now)
	Application.StaticObjects.Item("ASPChatMIN").Add ChatID, minute(Now)
	
	Response.Cookies ("CHAT") = ChatID
	Response.Cookies ("CHAT").expires = DateAdd ("d" , 1, date) 	
						
'***************** Notifica l'entrata del nuovo utente
	For x = 1 To MESSAGES Step + 1
		Application("chatline_" & x) = Application("chatline_" & x+1)
	Next
	Application("chatline_" & MESSAGES) = "<tr><td colspan=2  align='left'><span class='textgreen'><b>" & ChatName & " </b> entra alle : " & FormatDateTime(Now, 4) & "</span></td></tr>"
			
	Application.Unlock
	
	Response.Redirect "Chat.asp?ChatID=" & ChatID & "&ChatRoom=" & ChatRoom & "&ChatName=" & ChatName& "&moder=" & moder& "&Room=" & ChatID & "&!" & Room
	Response.End	
End If
%>	
<html>
<head>
	<title>La Chat di <%=Session("PrgDesc")%></title>
	<link rel="stylesheet" type="text/css" href="<%=Session("Progetto")%>/fogliostile.css">
</head>

<script LANGUAGE="JavaScript" type="text/javascript">
	function aclose() {
		alert("Grazie per aver partecipato alla Chat di <%=Session("PrgDesc")%> !!!")		
	}
</script>

<frameset rows="120,*,65" frameborder="0">
	<frame name="Banner"  src="top.asp?ChatID=<%=ChatID%>&ChatRoom=<%=ChatRoom%>&ChatName=<%=ChatName%>" scrolling="no" target="_self">
	<frameset cols="*,125" frameborder="1">
		<frame name="History" src="History.asp?ChatID=<%=ChatID%>&ChatRoom=<%=ChatRoom%>">
		<frame name="Chatters"   src="Chatters.asp?ChatRoom=<%=ChatRoom%>&ChatID=<%=ChatID%>" noresize >
	</frameset>
	<frameset cols="*,125" frameborder="0">
		<frame name="Message" src="Message.asp?ChatID=<%=ChatID%>&ChatRoom=<%=ChatRoom%>&moder=<%=moder%>" scrolling="no" target="_self">
		<frame name="ChatterSt"  src="ChatterST.asp?ChatRoom=<%=ChatRoom%>&ChatID=<%=ChatID%>&moder=<%=moder%>&Room=<%=Room%>" scrolling="no" >
</frameset>
	
	<noframes>
	<body OnUnload='aclose()'>
		Your browser does NOT support FRAMES
	</body>
	</noframes>
</frameset>

</html>