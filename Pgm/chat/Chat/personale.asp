<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include virtual ="/include/controlDateVB.asp"-->
<%
Dim Persn
Dim textMessage
Dim nick
Dim ChatID
Dim chatname
Dim kickID
Dim ChatRoom
%>
<!-- #include file="constants.inc" -->
<html>
<head>
	<title>Personale</title>
	<link rel="stylesheet" type="text/css" href="<%=Session("Progetto")%>/fogliostile.css">
</head>

<%
Function CheckProfanity( Text )
	Dim arrSwear, x, n
	'Get the Arrays
	arrSwear = Application("SwearWords")
	Text = Split(Text, " ")
	For x  = LBound(Text) To UBound(Text)
		'Check if it is profane
		For n = LBound(arrSwear) To UBound(arrSwear)
			If InStr(UCase(Text(x)), arrSwear(n)) > 0 Then
				Text(x) = "[Censurata]"
				Exit For
			End If
		Next
	Next
	CheckProfanity = Join(Text, " ")
End Function

nick = Request.QueryString("nick")
ChatID = Request.QueryString("ChatID")
persn = Request.QueryString("persn")
textMessage = server.HTMLEncode(Request.Form("textm"))
kickID = Request.QueryString("kickID")
Elim = Request.QueryString("elim")
ChatRoom = Request.QueryString("ChatRoom")

if persn = "SI" then
	on error resume next
	If textMessage > ""  Then
		If (Len(textMessage) > 0) Then
			textMessage = CheckProfanity(textMessage)
   		Dim i, text, r, chn, nk
   		Dim PVar
   			Application.Lock
	    	'Response.Write "MESS=" & MESSAGES 

			For i = 1 To MESSAGES Step + 1
				Application("chatline_" & i) = Application("chatline_" & i+1)
			Next
			If (Application.StaticObjects.Item("ASPChat").Exists(CStr(ChatID))) Then
				chatname = Application.StaticObjects.Item("ASPChat").Item(CStr(ChatID))
			Else
				chatname = "Guest"
			End If
			
			'Response.Write "chatName = " & chatname
			' build new message
			nk = mid(nick,3)
			if len(nick) < 10 then
				For i = len(nick) To 10 
					nk = nk & " "
				Next
			end if	
			chn = mid(chatname,3)
			if len(chatname) < 10 then
				For r = len(chatname) To 10
					chn = chn & " "
				Next
			end if	

			IF len(textMessage) > 30 then
				TXT1 = mid(textMessage,1,30)
				TXT2 = mid(textMessage,31)
				IF len(TXT2) > 50 then
					TXT3 = mid(TXT2,1,50)
					TXT4 = mid(TXT2,51)
					TXT5 = TXT3 & "<br>" & TXT4
				Else
					TXT5 = TXT2
				End if					
			textMessage = TXT1 & "<br>" & TXT5
			End if

			text = "<tr><td align='left' colspan='2' valign='top'                  class='textred'>Messaggio personale da " & chn & " per " & nk & " alle : " & ConvDateTimeToString(Now) & " : <font color='000000'>" & textMessage & "</font></td></tr>" & vbCrLf
			' add message as first message in queue
			Application("chatLine_" & MESSAGES) = text
			' update users timestamp
			Application.StaticObjects.Item("ASPChatTime").Item(ChatID) = CStr(Now())
    		PVar = Application.StaticObjects.Item("ASPChatTime").Item(ChatID)
			Application.UnLock

		End If
	End If
end if

if elim = "SI" then
		Dim w
		Application.Lock
		' add a leaving message to ChatRoom
		For x = 1 To MESSAGES Step + 1
			Application("chatline_" & x) = Application("chatline_" & x+1)
		Next
		if cstr(mid(nick,1,2)) = cstr(ChatRoom) then 
			Application("chatline_" & MESSAGES) = "<tr><td colspan='2' class='Name' align='left' ><span class='textred'>  <b>" & nick & "</b> � stato eliminato per comportamento non corretto alle : " & ConvDateTimeToString(Now) & "</span></td></tr>"
		end if
		' remove user
		Application.StaticObjects.Item("ASPChat").Remove(CStr(kickID))
		Application.StaticObjects.Item("ASPChatTime").Remove(CStr(kickID))
		Application.StaticObjects.Item("ASPChatIP").Remove(CStr(kickID))
		Application.StaticObjects.Item("ASPChatIMG").Remove(CStr(kickID))
		Application.StaticObjects.Item("ASPChatRoom").Remove(CStr(kickID))
		Application.StaticObjects.Item("ASPChatORA").Remove(CStr(kickID))
		Application.StaticObjects.Item("ASPChatMIN").Remove(CStr(kickID))
		Application.UnLock

end if
%>


<body bgcolor="#005d9b">
<table border="0" width="100%">
	<tr> 
		<td width="100%" class="tbltext0a"><b>Messaggio personale per <%=mid(nick,3)%> </b></font></td>
		<td nowrap valign="top">&nbsp;</td>
	</tr>
</table>
<form name="frmPers" id="frmPers" method="post" action="Personale.asp?persn=SI&amp;nick=<%=nick%>&amp;ChatID=<%=ChatID%>&amp;kickID=<%=kickID%>&amp;ChatRoom=<%=ChatRoom%>">
<table cellSpacing="1" cellPadding="1" width="75%" border="0" align="center">
	<tr>
		<td>
			<input class="editField" name="textm" id="textm" style="WIDTH: 370" border="1" maxLength="130">
		</td>
	</tr>
	<tr>
		<td align="middle"></td>
	</tr>
	<tr>	
		<td align="middle">
			<input type="image" src="images\invia.gif" value="submit" border="0" name="Invio" onclick="javascript:window.close();" WIDTH="63" HEIGHT="34">
			<a href="javascript:window.close();"><img SRC="images/esci.gif" border="0" WIDTH="61" HEIGHT="34"></a>
		</td>
	</tr>
</table>
</form>
<% if session("moder") = "15" then%>
<form name="frmElim" id="frmElim" method="post" action="Personale.asp?elim=SI&amp;nick=<%=nick%>&amp;ChatID=<%=ChatID%>&amp;kickID=<%=kickID%>&amp;ChatRoom=<%=ChatRoom%>">
<table cellSpacing="1" cellPadding="1" width="100%" border="0" align="center">
	<tr>
		<td align="right">
			<input type="image" src="images\elimina.gif" value="submit" border="0" name="Invio" onclick="javascript:window.close();" WIDTH="137" HEIGHT="17">
		</td>
	</tr>
</table>
</form>
<% end if%>
<script language="JavaScript">
	frmPers.textm.focus();
</script>

</body>
</html>
