<%
Const MESSAGES = 100	  	    ' (no limits) a maximum of 50 messages are shown on screen
Const USERS    = 50			    ' (max 999)   a maximum of 999 users may join
CONST TIMEOUT  = 1150 			' (no limits) in seconds - a session times out after 10 minutes (10*60=600)
Const CLEAR_ON_EMPTY = True  	'             specify is all messages should be wiped, when last user
								'             leaves the chatroom. The 'whiteboard cleaner'.
Const STANZE = 15				' (max 99)     number of Room'.

Function FindUser(userName)
	userName = Trim(userName)
	Dim UserWasFound
	UserWasFound = False

	Dim arUserNames, i
	arUserNames = Application.StaticObjects.Item("ASPChat").Items
	For i = 0 To Application.StaticObjects.Item("ASPChat").Count-1
		If (StrComp(Mid(userName,3), mid(arUserNames(i),3), 1) = 0) Then
			UserWasFound = True
			Exit For
		End If
	Next
	FindUser = UserWasFound
End Function


'Function FindID(CHD)
'	CHD = Trim(CHD)
'
'	Dim IPWasFound
'	IPWasFound = False
'
'	Dim arUserID, i
'	arUserID = Application.StaticObjects.Item("ASPChat").Keys
'	For i = 0 To Application.StaticObjects.Item("ASPChat").Count-1
'		If (StrComp(CHD, arUserID(i), 1) = 0) Then
'			IPWasFound = True
'			Exit For
'		End If
'	Next
'	FindIP = IPWasFound
'End Function



%>