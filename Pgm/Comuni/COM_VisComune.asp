<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include Virtual="/strutt_testa3.asp"-->
<!--#include virtual ="/include/openconn.asp"-->
<!--#include VIRTUAL = "/util/dbutil.asp"-->
<!-- #include virtual="/util/portallib.asp"-->
<!--#include virtual="/include/DecCod.asp"-->
<!--#include virtual="/include/ckProfile.asp"-->
<%
if ValidateService(session("idutente"),"com_selprov",cc) <> "true" then 
	response.redirect "/util/error_login.asp"
end if
%>
<script LANGUAGE="Javascript">
//include del file per fare i controlli sulla numericit� dei campi
<!--#include virtual="/Include/ControlNum.inc"-->
//include funzioni stringhe
<!--#include virtual="/Include/ControlString.inc"-->
<!--#include virtual = "/Include/help.inc"-->


//Funzione per i controlli dei campi da modificare 
	function ControllaDati(frmModComune){
	//-------- Controlli di OBBLIGATORIETA' --------
	//-------- Controlli FORMALI            --------
	//-------- Controlli di RELAZIONE       --------
		//Descrizione obbligatoria
		frmModComune.txtDesc.value = TRIM(frmModComune.txtDesc.value)
		if ((frmModComune.txtDesc.value == "")||
			(frmModComune.txtDesc.value == " ")){
			alert("Descrizione del Comune obbligatoria")
			frmModComune.txtDesc.focus() 
			return false
		}
		if (frmModComune.Estero.value == "False") {

			//Provincia obbligatoria
			if (frmModComune.cmbProv.value == ""){
				alert("Departamento obbligatoria")
				frmModComune.cmbProv.focus() 
				return false
			}	

			//CAP obbligatorio
			if ((frmModComune.txtCap.value == "")||
				(frmModComune.txtCap.value == " ")){
				alert("CAP obbligatorio")
				frmModComune.txtCap.focus() 
				return false
			}
			//CAP deve essere numerico
			if (frmModComune.txtCap.value != ""){
				if (!IsNum(frmModComune.txtCap.value)){
					alert("Il CAP deve essere Numerico")
					frmModComune.txtCap.focus() 
					return false
				}
			}
			//CAP deve essere diverso da zero
			if (frmModComune.txtCap.value != ""){
				if (eval(frmModComune.txtCap.value) == 0){
					alert("Il CAP non pu� essere zero")
					frmModComune.txtCap.focus()
					return false
				}
			}
			//CAP deve essere di 5 caratteri
			if (frmModComune.txtCap.value != ""){
				if (frmModComune.txtCap.value.length != 5){
					alert("Il CAP deve essere composto da 5 caratteri")
					frmModComune.txtCap.focus() 
					return false
				}
			}
			//Prefisso obbligatorio
			if ((frmModComune.txtPref.value == "")||
				(frmModComune.txtPref.value == " ")){
				alert("Prefisso obbligatorio")
				frmModComune.txtPref.focus() 
				return false
			}
			//Prefisso deve essere numerico
			if (frmModComune.txtPref.value != ""){
				if (!IsNum(frmModComune.txtPref.value)){
					alert("Il Prefisso deve essere Numerico")
					frmModComune.txtPref.focus() 
					return false
				}
			}
			//Prefisso deve essere diverso da zero
			if (frmModComune.txtPref.value != ""){
				if (eval(frmModComune.txtPref.value) == 0){
					alert("Il Prefisso non pu� essere zero")
					frmModComune.txtPref.focus()
					return false
				}
			}	
		}
	return true
	}
</script>

<body>

<table border="0" width="520" cellspacing="0" cellpadding="0" height="81">
   <tr>
     <td width="500" background="<%=Session("Progetto")%>/images/titoli/strumenti2b.gif" height="81" valign="bottom" align="right">
       <table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
         <tr>
           <td width="100%" valign="top" align="right"><b class="tbltext1a">Gestione Comuni</span></b></td>
         </tr>
       </table>
     </td>
   </tr>
</table><br>
<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
<tr height="17">

	<td class="sfondomenu" width="67%" height="18"><span class="tbltext0">
		<b>&nbsp;Gestione Tabella Comune </b>
	</td>
	<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">
	</td>
	<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">(*) campi obbligatori</td>
</tr>
<tr>
	<td class="sfondocomm" width="57%" colspan="3">
		<br>Inidicare i valori che si vogliono modificare e premere <b>Invia</b>
		<a href="Javascript:Show_Help('/pgm/help/Comuni/COM_VisComune')"><img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
	</td>
</tr>
<tr height="2">
	<td class="sfondocomm" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif">
	</td>
</tr>
</table>
<br>
<%
sCodComune = request ("cmbComune")
nNumVal = clng(Request("NumVal"))
'Response.write nNumVal
'dim sMask
sMask = GetSectionVar("COM_SELPROV","MASK") 
'sMask = 15
'Response.Write "MASK=" & sMask
sEstero = false
if mid(sCodComune,1,1) = "Z" then sEstero = true 

Response.Write "<form method='post' name='frmModComune' onsubmit='return ControllaDati(this)' action='COM_CnfComune.asp?CodComune=" & sCodComune & "&ACTION=MOD'>"

dim rsComune

sSQL = "SELECT CodCom, DesCom, Prv, Cap, Prefisso from COMUNE WHERE CODCOM='" & sCodComune & "'"

'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
set rsComune = CC.Execute(sSQL)

sCodCom = rsComune("CodCom")
sDesc = rsComune("Descom")
sProv = rsComune("Prv")
sCap  = rsComune("Cap")
sPref = rsComune("Prefisso")

rsComune.close

'Titolo della Pagina:
' se Mask 01 - Visualizzazione
' se Mask 15 - Modifica/Inserimento
'select case sMask 
	'case 01
		'Response.Write "<table width=500 border=0 cellspacing=2 cellpadding=1>"
		'Response.Write "<tr>"
		'Response.Write "<td colspan=2 align=middle background=../../images/sfmnsmall.jpg>"
		'Response.Write "<b>"
		'Response.Write "<span class='titolo'>"
		'Response.Write "Gestione Tabelle Comune"
		'Response.Write "</span>"
		'Response.Write "</b>"
		'Response.Write "</td>"
		'Response.Write "</tr>"
		'Response.Write "</table>"
	'case 15
		'Response.Write "<table width=500 border=0 cellspacing=2 cellpadding=1>"
		'Response.Write "<tr>"
		'Response.Write "<td colspan=2 align=middle background=../../images/sfmnsmall.jpg>"
		'Response.Write "<b>"
		'Response.Write "<span class='titolo'>"
		'Response.Write "Gestione Tabella Comune"
		'Response.Write "</span>"
		'Response.Write "</b>"
		'Response.Write "</td>"
		'Response.Write "</tr>"
		'Response.Write "</table>"
'end select

%>
<input type="Hidden" name="Estero" value="<%=sEstero%>">

   <table border="0" cellpadding="0" cellspacing="1" width="500">
    <tr>
      <td align="center" colspan="2" align="left" width="60%">
       <b>
        <a class="textred" href="COM_SelProv.asp">
         Torna all'inizio
         </a>
        </b>
       </td>
      </tr>
     </table>
     <br>
     <br>

     
      <table width="500" border="0" cellspacing="2" cellpadding="2" align="center">
       <tr>
        <td nowrap class="tbltext1" align="left">
         <b>
            Codice*
         </b>
        </span>
        <span class="tbltext3">
        </span>
       </td>
        <td nowrap width="30" align="middle" class="textblack">
		<b>
<%
'select case sMask
'	case 01
		Response.Write sCodCom
'	case 15
'		Response.Write "<input style=TEXT-TRANSFORM:uppercase name='txtCodice' size='22' maxlength='4' value='" & sCodCom & "' readonly>"
'end select
%>
		</b>
       </td>
      </tr>
      <tr>
       <td nowrap align="left">
        <span class="tbltext1">
         <b>
          <span class="tbltext1"></span>
      Descrizione*
          </b></span>
         </td>
         <td nowrap align="left" class="tbltext">
         
<%         
select case sMask
	case 01
		Response.Write sDesc
	case 15
		Response.Write "<input class=textblacka style=TEXT-TRANSFORM:uppercase name='txtDesc' size='40' maxlength='35' value=""" & sDesc & """>"
end select
%>
        </td>
       </tr>
       <tr>
        <td nowrap width="110" class="tbltext1" align="left">
         <span class="table">
          <b>
           <span class="tbltext1"></span>
       Departamento <%if not sEstero then Response.Write "*"%>
          </b></span>
         <span class="tbltext3"></span>
        </td>
        <td nowrap width="30" align="middle" class="tbltext">
        
<%        
select case sMask 
	case 01	
		dim rsProv
		sSQL = "SELECT Descrizione from TADES WHERE Nome_Tabella='PROV' and Codice='" & sProv & "'"
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
		set rsProv = CC.Execute(sSQL)
		sDescProv = rsProv("Descrizione")
		rsProv.close
		Response.Write sDescProv
	case 15
		sSQL = " Select distinct PRV  from COMUNE where PRV is not null order by PRV "
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
		set rsProv = CC.Execute(sSQL)
		
		Response.Write "<select name='cmbProv' class='textblacka'>" &_
							"<option value=''></option>"
		do until rsProv.eof
				if rsProv.Fields("PRV") = sProv then
					Response.Write  "<option selected value='" & rsProv.Fields("PRV") & "'>" &_
										rsProv.Fields("PRV")&_
									"</option>"
				else
					Response.Write  "<option value='" & rsProv.Fields("PRV") & "'>" &_
										rsProv.Fields("PRV")&_
									"</option>"
				end if
				rsProv.Movenext
		loop
		Response.Write "</select>"
		rsProv.close
		set rsProv = nothing
'		sCondizione = " And exists(Select PRV from COMUNE where PRV = TADES.CODICE)"
'		sInt = "PROV|0|" & date & "|" & sProv & "|cmbProv|" & sCondizione & " ORDER BY DESCRIZIONE"			
'		CreateCombo(sInt)
end select

%>
    </td>
   </tr>
   <tr>
    <td nowrap width="110" align="left">
     <span class="tbltext1">
      <b>
       <span class="tbltext1"></span>
     Cap<%if not sEstero then Response.Write "*"%>
       </b></span>
       <span class="tbltext1"></span>
       </td>
        <td nowrap width="30" align="middle" class="tbltext">
        
<%        
select case sMask
	case 01
		Response.Write sCap
	case 15
		Response.Write "<input class=textblacka style=TEXT-TRANSFORM:uppercase name='txtCap' size='22' maxlength='5' value='" & sCap & "'>"
end select
%>
       </td>	
      </tr>
      <tr>
       <td nowrap width="110" align="left">
        <span class="tbltext1">
         <b> 
          <span class="tbltext1"></span>
      Prefisso<%if not sEstero then Response.Write "*"%>
          </b></span>
          <span class="tbltext1"></span>
         </td>
          <td nowrap width="30" align="middle" class="tbltext">
          
<%          
select case sMask
	case 01
		Response.Write sPref
	case 15
		Response.Write "<input class=textblacka style=TEXT-TRANSFORM:uppercase name='txtPref' size='22' maxlength='4' value='" & sPref & "'>"
end select
%>

        </td>
       </tr>
       
      
<%       
'Link per fare un'altra ricerca (mask=01) o pulsanto (mask=15)
select case sMask
	case 01

	
		Response.Write "<table width='500'>"
		Response.Write "<tr>"
		Response.Write "<td align=left>"
		Response.Write "<A href='COM_SelProv.asp'>"
		Response.Write "<b>"
		Response.Write "<span class='tbltext'>"
		Response.Write "<CENTER>"
		Response.Write "Ricerca un'altro Comune"
		Response.Write "</CENTER>"
		Response.Write "</font>"
		Response.Write "</b>"
		Response.Write "</A>"
		Response.Write "</td>"
		Response.Write "</tr>"
		Response.Write "</table>"
	case 15
		
		Response.Write "<tr>"
		Response.Write "<td colspan='2' >"
		Response.Write "<p align='center'>"
		
%>

<br>
<table border="0" cellpadding="1" cellspacing="0" width="500">
	<tr height="23">
		<td width="100">&nbsp;</td>
		<td align="rigth" width="150">
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="image" src="<%=Session("progetto")%>/images/conferma.gif" name="Invia" border="0" align="absBottom">
		</td>
<!--		<td align="left" width=80>			<a id=Annulla name='Annulla' href="javascript:frmModComune.reset();" >			 <IMG src="<%=Session("progetto")%>/images/annulla.gif" border=0>			</a>		</TD>-->
		<td align="rigth" width="170">
			<%if nNumVal = 1 then%>
				<a id="Annulla" name="Annulla" href="COM_SelProv.asp">
			<%else%>
				<a id="Annulla" name="Annulla" href="javascript:history.back();">
			<%end if%>
			 <img src="<%=Session("progetto")%>/images/indietro.gif" border="0">
			</a>
		</td>
	</tr>
</table>


<%
'		Response.Write "<input type='reset' name='Annulla' value='Annulla'>&nbsp;"
'		Response.Write "<input type='submit' name='Invia' value='Conferma'>"
		Response.Write "</p>"
		Response.Write "</td>"
		Response.Write "</tr>"
		Response.Write "</table>"
end select
%>

<!--#include file ="../../include/closeconn.asp"-->
<!--#include Virtual="/strutt_coda2.asp"-->
