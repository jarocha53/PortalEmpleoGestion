<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%
function ComNewProfile(ID_FIGPROF,ID_AREAPROF,FL_STATOLAV)
	'fUNZIONE PER COMUNICAZIONE AL CENTRO DI REGIA DI UN NUOVO PROFILO DA VALIDARE

	OraDate=ConvDateToDB(Now())

	SqlDenFp = "" 
	SqlDenFp = SqlDenFp & " SELECT "
	SqlDenFp = SqlDenFp & "		FIGUREPROFESSIONALI.DENOMINAZIONE, CODICI_ISTAT.MANSIONE, CODICI_ISTAT.CODICE"
	SqlDenFp = SqlDenFp & " FROM "
	SqlDenFp = SqlDenFp & "		CODICI_ISTAT, FIGUREPROFESSIONALI "
	SqlDenFp = SqlDenFp & " WHERE "
	SqlDenFp = SqlDenFp & "		ID_FIGPROF  =" & ID_FIGPROF & " AND " 
	SqlDenFp = SqlDenFp & "		CODICI_ISTAT.CODICE = FIGUREPROFESSIONALI.COD_EJ "

'PL-SQL * T-SQL  
SQLDENFP = TransformPLSQLToTSQL (SQLDENFP) 
	Rs = Conn.Execute(SqlDenFp)
	DenFp = CleanXsql(Rs(0))
	MansioneFP= CleanXsql(Rs(1))
	CodIstatFP= CleanXsql(Rs(2))

	set Rs = Nothing

	' Estrazioni dati relativi alla Sede dell'Impresa

	SqlImp = SqlImp &  " SELECT "
	SqlImp = SqlImp &  "   IMPRESA.RAG_SOC, SED.DESCRIZIONE, SED.PRV, "
	SqlImp = SqlImp &  "   SETTORI.DENOMINAZIONE"
	SqlImp = SqlImp &  " FROM "
	SqlImp = SqlImp &  "   IMPRESA IMPRESA, "
	SqlImp = SqlImp &  "   SEDE_IMPRESA SED, "
	SqlImp = SqlImp &  "   SETTORI SETTORI"
	SqlImp = SqlImp &  " WHERE "
	SqlImp = SqlImp &  "	IMPRESA.ID_IMPRESA=SED.ID_IMPRESA AND "
	SqlImp = SqlImp &  "  IMPRESA.ID_SETTORE=SETTORI.ID_SETTORE AND"
	SqlImp = SqlImp &  "  SED.ID_SEDE=" & IDSedeAz

'PL-SQL * T-SQL  
SQLIMP = TransformPLSQLToTSQL (SQLIMP) 
	Set Rs = Conn.Execute (SqlImp)
		if not rs.eof then 
			RAG_SOC = rs(0)
			DESCRIZIONE = rs(1)
			SETTORE = rs(3)
			PROVINCIA = rs(2)
		end if 
	Set Rs = Nothing

	'Caso inserimento nuovo profilo e attesa validaizone (! non si comunica niente agli utenti del portale)
	if FL_STATOLAV = 1 then
		OggCom = ""
		OggCom = "Ingreso de una nueva Figura Profesional "
	else 
		OggCom = ""
		OggCom = "Anular el ingreso de una Figura Profesional (en estado de elaboraci�n) " 
	end if
	

	TestoCom = ""
	TestoCom = TestoCom & " Nuevo Perfil Profesional : " & DenFP 
	TestoCom = TestoCom & " Asociado a la Funci�n Istat : " & MansioneFP
	TestoCom = TestoCom & " Relacionada con el Codigo ISTAT : " &  CodIstatFP
	TestoCom = TestoCom & " Ingresado por la Empresa: " & RAG_SOC
	TestoCom = TestoCom & " Sector: " & SETTORE
	TestoCom = TestoCom & " Sede: " & SEDE_IMPRESA
	TestoCom = TestoCom & " Provincia: " & PROVINCIA
	
	OggCom = cleanXsql (OggCom)
	TestoCom = cleanXsql (TestoCom)
'Ricerca del'ID del mittente, ovvero il corrispettivo IDUTENTE del CREATOR

	SqlIdMit = ""
	SqlIdMit = SqlIdMit & " SELECT IDUTENTE "
	SqlIdMit = SqlIdMit & " FROM UTENTE "
	SqlIdMit = SqlIdMit & " WHERE "
	SqlIdMit = SqlIdMit & "CREATOR = " & IdSedeAz

'PL-SQL * T-SQL  
SQLIDMIT = TransformPLSQLToTSQL (SQLIDMIT) 
	Set Rs = Conn.Execute (SqlIdMit)
	IdMittente = Rs(0)

'Response.Write idmittente
'Response.End 

	Set Rs = Nothing			
	
	
	'Inserimento del messaggio in comunicazione
	SqlCom = ""
	SqlCom = SqlCom & " INSERT INTO COMUNICAZIONE " 
	SqlCom = SqlCom & " (IDUTENTE,OGGETTO,TESTO_COM,LINK_COM,TESTO_LINK_COM, "
	SqlCom = SqlCom & " DT_INS_COM, DT_INVIO_COM,DT_TMST) "
	SqlCom = SqlCom & " VALUES "
	SqlCom = SqlCom & "(" & IdMittente & ",'" & OggCom & "','" & TestoCom & "',"
	SqlCom = SqlCom & " '',''," & convdatetodb(Now()) & "," & convdatetodb(Now())  
	SqlCom = SqlCom & "," & convdatetodb(Now()) & ")"


	
'PL-SQL * T-SQL  
SQLCOM = TransformPLSQLToTSQL (SQLCOM) 
	conn.execute(SqlCom)

	'Cerco il max ID
	sql = "SELECT Max(ID_COMUNICAZIONE) FROM COMUNICAZIONE WHERE IDUTENTE=" & IdMittente

'PL-SQL * T-SQL  
	Set Rs = Conn.Execute ("SELECT Max(ID_COMUNICAZIONE) FROM COMUNICAZIONE WHERE IDUTENTE=" & IdMittente)

	ID_COMUNICAZIONE = Rs(0)
	Set Rs = Nothing			

		'Elenco destinatari gruppo di regia 
		'### per prova utilizzo il GRUPPO REDAZIONALE: IDGRUPPO = 3
		'Da modificare  IdCentroRegia = 3 in FunctionBP.ASP se il gruppo Cabina di Regia cambia
		
		Sql = "  SELECT UTENTE.IDUTENTE, UTENTE.EMAIL"
		Sql = Sql &  "  FROM UTENTE, GRUPPO  "
		Sql = Sql &  "  WHERE  "
		Sql = Sql &  "  UTENTE.IDGRUPPO = GRUPPO.IDGRUPPO"
		Sql = Sql &  "  AND UTENTE.IDGRUPPO = "	& IdCentroRegia

'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
		Set RsUsr = Conn.Execute (Sql)
			
		if not RsUsr.eof then
			Utenti = RsUsr.getrows
		end if
			
			for I = lbound(Utenti,2) to Ubound(Utenti, 2)
				Sql = ""
				Sql = Sql & "INSERT INTO COM_PERSONA "
				Sql = Sql & "(ID_COMUNICAZIONE, IDUTENTE, DT_TMST) "
				Sql = Sql & "VALUES "
				Sql = Sql & "(" & ID_COMUNICAZIONE & ", " & Utenti(0,i) & ", sysdate)"
			
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
				conn.execute(Sql)
			next


	' Ricerca caselle testo e-mail Cabina di Regia
	if IsArray(Utenti) then
		Destinatari=""
		for I = Lbound(Utenti,2) to Ubound(Utenti,2)
			Destinatari= Destinatari & Utenti(1,I) & ";"
		next
	end if

	if Destinatari<>"NOT AVIABLE" AND Destinatari<>""  then
		Set objSession = CreateObject ("CDONTS.Session")
		objSession.LogonSMTP "Profili Professionali Aziendali", "ejobplace@consiel.it"
			objSession.MessageFormat = 0
						
			Set objOutbox = objSession.GetDefaultFolder(2)
			Set objMessage =  objOutbox.Messages.Add
			TestoCom = " In Data " & Day(date()) & " / " & MonthName(Month(Date)) & " / " & Year(Date()) &  Chr(10) & Chr(13)
			TestoCom = TestoCom  & " Nuevo Perfil Profesional : " & DenFP & Chr(10) & Chr(13)
			TestoCom = TestoCom  & " Asociado a la Funci�n Istat : " & MansioneFP & Chr(10) & Chr(13)
			TestoCom = TestoCom  & " Relacionada con el c�digo ISTAT : " &  CodIstatFP	 & Chr(10) & Chr(13)
			objMessage.Subject = OggCom
			objMessage.Text = TestoCom 
					
			objMessage.Importance = 1
			
			objMessage.Recipients.Add Destinatari,1
					
			objMessage.Send
		objSession.Logoff()
	end if 
END FUNCTION

Function ComCandidatiProfili(ID_AREAPROF,ID_FIGPROF)
	
	'##### SELEZIONE DEI CANDIDATI CON PROSSIMITA' RISPETTO ALL'AREA DEL NUOVO PROFILO
	'##### CHE SI SONO COLLEGATI ALMENO UNA VOLTA NEGLI ULTIMI 3 MESI
	Sql = ""
	Sql = Sql & "SELECT DISTINCT "
	Sql = Sql & "	PERS_AREAPROF.ID_PERSONA, AREE_PROFESSIONALI.DENOMINAZIONE "
	Sql = Sql & "FROM "
	Sql = Sql & "	PERS_AREAPROF, AREE_PROFESSIONALI, UTENTE "
	Sql = Sql & "WHERE "
	Sql = Sql & "	UTENTE.CREATOR=PERS_AREAPROF.ID_PERSONA AND "
	Sql = Sql & "	PERS_AREAPROF.ID_AREAPROF =  AREE_PROFESSIONALI.ID_AREAPROF AND "
	Sql = Sql & "	PERS_AREAPROF.ID_AREAPROF = " & trim(ID_AREAPROF) & "  AND "
	Sql = Sql & "	PERS_AREAPROF.ID_FASE IN (3,5) AND "
	Sql = Sql & "	PERS_AREAPROF.IND_STATUS = 0  AND"
	Sql = Sql & "	UTENTE.TIPO_PERS='P' AND "
	Sql = Sql & "	MONTHS_BETWEEN(TO_DATE(SYSDATE,'DD/MM/YYYY'),TO_DATE(UTENTE.DT_ACCESSO, 'DD/MM/YYYY'))<3 "

		
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
	Set RsCandidabili = Conn.Execute (Sql)

	if not RsCandidabili.eof then
		Candidabili = RsCandidabili.getrows
		NomeArea = cleanXsql(Candidabili (1,0))

			' ###### MESSAGGIO DI NOTIFICA: AZIENDA RICERCA PROFILO PER L'AREA.... 

			SqlCom = ""
			SqlCom = SqlCom & " INSERT INTO COMUNICAZIONE " 
			SqlCom = SqlCom & " (IDUTENTE,OGGETTO,TESTO_COM,LINK_COM,TESTO_LINK_COM, "
			SqlCom = SqlCom & " DT_INS_COM, DT_INVIO_COM,DT_TMST) "
			SqlCom = SqlCom & " VALUES "
			SqlCom = SqlCom & "(" & IDP & ", 'OPPORTUNITA LAVORO, AREA PROFESSIONALE: " & NomeArea  & "',"
			SqlCom = SqlCom & " 'La empresa esta buscando el perfil para el cual esta postulado,  Funcion: " & DenFp & "',"
			SqlCom = SqlCom & " '',''," & convdatetodb(Now()) & "," & convdatetodb(Now())  
			SqlCom = SqlCom & "," & convdatetodb(Now()) & ")"

'PL-SQL * T-SQL  
SQLCOM = TransformPLSQLToTSQL (SQLCOM) 
			conn.execute(SqlCom)

'PL-SQL * T-SQL  
			Set Rs = Conn.Execute ("SELECT Max(ID_COMUNICAZIONE) FROM COMUNICAZIONE WHERE IDUTENTE=" & IDP)
			ID_COMUNICAZIONE =	Null
			ID_COMUNICAZIONE = Rs(0)
			Set Rs = Nothing			

		for I = lbound(Candidabili,2) to Ubound(Candidabili,2)
					
			LstCampi = "ID_COMUNICAZIONE,IDUTENTE,DT_TMST"

			LstVal = ""
			LstVal = LstVal & ID_COMUNICAZIONE & "|"
			LstVal = LstVal & Candidabili(0,i) & "|"
			LstVal = LstVal & OraDate

			Sql = ""
			Sql = QryIns("COM_PERSONA",LstCampi,LstVal)	
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
			Conn.Execute Sql
					
		next 

	end if
	set RsCandidabili = Nothing



	'#######################################################################################
	'###### SELEZIONE CANDIDATI CON PROSSIMITA' RISPETTO ALLA MANSIONE ISTAT RICERCATA #####


		Sql = ""
		Sql = " SELECT DISTINCT  PERS_FIGPROF.ID_PERSONA"
		Sql = Sql &  " FROM"
		Sql = Sql &  " FIGUREPROFESSIONALI, PERS_FIGPROF"
		Sql = Sql &  " WHERE  FIGUREPROFESSIONALI.ID_FIGPROF = PERS_FIGPROF.ID_FIGPROF"
		Sql = Sql &  " AND PERS_FIGPROF.ID_FASE IN (3,5) "
		Sql = Sql &  " AND FIGUREPROFESSIONALI.COD_EJ = '" & CodIstatFP &  "'"
			

'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
		Set RsCandidFP = Conn.Execute (Sql)
			
		if not RsCandidFP.eof then
			CandidFP = RsCandidFP.getrows


		' ###### MESSAGGIO DI NOTIFICA: AZIENDA RICERCA PROFILO PER LA MANSIONE...  
					OggCom = " Nueva oferta de empleo"
					TestoCom= "Se ha ingresado un nuevo perfil relacionado con la funcion a la cual esta postulado :" 
					TestoCom=TestoCom & DenFp & ". Le solicitamos que reelabore su Balance de Proximidad "
					TestoCom=TestoCom & " para lograr una mayor oportunidad de empleo. "
					
					OggCom = cleanXsql (OggCom)
					TestoCom = cleanXsql (TestoCom)
					
					SqlCom = ""
					SqlCom = SqlCom & " INSERT INTO COMUNICAZIONE " 
					SqlCom = SqlCom & " (IDUTENTE,OGGETTO,TESTO_COM,LINK_COM,TESTO_LINK_COM, "
					SqlCom = SqlCom & " DT_INS_COM, DT_INVIO_COM,DT_TMST) "
					SqlCom = SqlCom & " VALUES "
					SqlCom = SqlCom & "(" & IDP & ", '" & OggCom & "','" & TestoCom & "',"
					SqlCom = SqlCom & " '',''," & convdatetodb(Now()) & "," & convdatetodb(Now())  
					SqlCom = SqlCom & "," & convdatetodb(Now()) & ")"

'PL-SQL * T-SQL  
SQLCOM = TransformPLSQLToTSQL (SQLCOM) 
					conn.execute(SqlCom)


'PL-SQL * T-SQL  
					Set Rs = Conn.Execute ("SELECT Max(ID_COMUNICAZIONE) FROM COMUNICAZIONE WHERE IDUTENTE=" & IDP)
					ID_COMUNICAZIONE =	Null
					ID_COMUNICAZIONE = Rs(0)
					Set Rs = Nothing							

					for I = lbound(CandidFP,2) to Ubound(CandidFP,2)
						LstCampi = "ID_COMUNICAZIONE,IDUTENTE,DT_TMST"

						LstVal = ""
						LstVal = LstVal & ID_COMUNICAZIONE & "|"
						LstVal = LstVal & Candidabili(0,i) & "|"
						LstVal = LstVal & OraDate

						Sql = ""
						Sql = QryIns("COM_PERSONA",LstCampi,LstVal)	
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
						Conn.Execute Sql
					next 
		end if
	set CandidFP = Nothing
End function
%>
