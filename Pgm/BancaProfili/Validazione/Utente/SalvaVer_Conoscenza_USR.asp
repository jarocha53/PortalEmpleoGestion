<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #INCLUDE FILE="Utils.asp" -->
<%
Set Conn = server.CreateObject ("ADODB.Connection")
Conn.open strConn
OraDate = "TO_DATE('" & day(date) & "/" & month(Date) & "/" & year(Date) & "','DD/MM/YYYY')"
Conn.BeginTrans

'#### Inserimento In VALIDAZIONE
LstCampi = "ID_PERS_INS,ID_PERS_LAV,ID_PERS_CERT,FL_VALID,DT_TMST"
LstVal = ""
LstVal = LstVal & request("ID_PERSONA") & "|"
LstVal = LstVal & IDP & "|"
LstVal = LstVal & IDP & "|"
LstVal = LstVal & "1" & "|"
LstVal = LstVal & OraDate

Sql = ""
Sql = QryIns("VALIDAZIONE",LstCampi,LstVal)
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
Conn.Execute Sql

'#### Cerco l'ID_VALID appena inserito
'PL-SQL * T-SQL  
Set Rs = Conn.Execute ("SELECT max(ID_VALID) FROM VALIDAZIONE WHERE ID_PERS_LAV = " & IDP)
ID_VALID = Rs(0)
set Rs = nothing

'#### Inserisco in CONOSCENZE
LstCampi = "ID_AREACONOSCENZA,DENOMINAZIONE,ID_VALID"
LstVal = ""
LstVal = LstVal & request("ID_AREACONOSCENZA") & "|"
LstVal = LstVal & "'" & replace(request("DENOMINAZIONE"),"'","`") & "'" & "|"
LstVal = LstVal & ID_VALID

Sql = ""
Sql = QryIns("CONOSCENZE",LstCampi,LstVal)
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
Conn.Execute Sql

'#### Cerco l'ID_CONOSCENZA appena inserito
'PL-SQL * T-SQL  
Set Rs = Conn.Execute ("SELECT max(ID_CONOSCENZA) FROM CONOSCENZE")
ID_CONOSCENZA = Rs(0)
set Rs = nothing

'#### Inserisco l'associazione PERSONA CONOSCENZA
LstCampi = "ID_PERSONA,ID_CONOSCENZA,COD_GRADO_CONOSC,DT_TMST"
LstVal = ""
LstVal = LstVal & request("ID_PERSONA") & "|"
LstVal = LstVal & ID_CONOSCENZA & "|"
LstVal = LstVal & request("POSSESSO") & "|"
LstVal = LstVal & OraDate

Sql = ""
Sql = QryIns("PERS_CONOSC",LstCampi,LstVal)

'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
Conn.Execute Sql

'#### Cancello l'elemento inserito dall'utente
SqlCanc = "DELETE FROM ELEMENTI_USR WHERE ID_ELEMENTO = " & request("ID_ELEMENTO")
'PL-SQL * T-SQL  
SQLCANC = TransformPLSQLToTSQL (SQLCANC) 
Conn.Execute SqlCanc

Conn.CommitTrans
Conn.Close
Set conn = Nothing

%>

<CENTER>
<table border=0 cellspacing=1 cellpadding=0 width='500' style="font-family: Verdana; font-size: 8pt; color:#ffffff">
	<tr>
	<td align=center>
		<table border=0 width='500' CELLPADDING=0 cellspacing=0>
		    <tr>
				<td colspan=3 bgcolor='#3399CC'></td>
			</tr>
		    <tr>
				<td align="CENTER" bgcolor='#C6DFFF' >
				<font face=Verdana color=#000000 size=1>
				 <B>Validazione Conoscenza USR </B>
				</font></td>
		    </tr>
		    <tr>
				<td colspan=3 bgcolor='#3399CC'></td>
	    </tr>
</table>

<br>
<table border=0 width='500' cellspacing=1 cellpadding=0 style='font-family: Verdana; font-size: 10pt; color:#000000'>
<tr><td>
	<table border=0 cellspacing=1 width='500' style='font-size: 8pt; font-family: verdana'>
		<tr><td bgcolor='#FFFFFF'><small><b>Operazione Completata Con successo</b></small></td></tr>
		<tr><td bgcolor='#FFFFFF' align='right'>
			<input type='button' value='<< indietro' OnClick='document.location="Conoscenze_USR.asp"' CLASS='My'>
		</td></tr>
	</table>
</td></tr>
</table>

