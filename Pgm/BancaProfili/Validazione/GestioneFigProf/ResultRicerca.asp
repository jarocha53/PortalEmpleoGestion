<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #include Virtual="/strutt_testa2.asp" -->
<!-- #INCLUDE FILE="../Utils.asp" -->
<%

'<!-- #INCLUDE FILE="FunctionJs.asp" -->

if request("lDwn") = "" Or Int(request("lDwn")) < 0 then
	lDwn = 0
else
	lDwn = request("lDwn")
end if

Set Conn = cc
'Conn.open strConn

if request("Figura Profesional") <> "" OR request("Competencia") <> ""  then

	if request("Figura Profesional") <> "" then
		Tipologia="Figura Profesional"

		Sql = ""
		Sql = Sql & "SELECT "
		Sql = Sql & "FIGUREPROFESSIONALI.DENOMINAZIONE, "
		Sql = Sql & "FIGUREPROFESSIONALI.ID_FIGPROF, VALIDAZIONE.FL_VALID, "
		Sql = Sql & "AREE_PROFESSIONALI.DENOMINAZIONE Area, VALIDAZIONE.FL_STATOLAV, "
		Sql = Sql & " FIGUREPROFESSIONALI.BREVE_DESC "
		Sql = Sql & " FROM "
		Sql = Sql & "FIGUREPROFESSIONALI, VALIDAZIONE, AREE_PROFESSIONALI "
		Sql = Sql & "WHERE "

		W= ""

		W = W & " AREE_PROFESSIONALI.ID_AREAPROF = FIGUREPROFESSIONALI.ID_AREAPROF "
		W = W & " AND FIGUREPROFESSIONALI.ID_VALID = VALIDAZIONE.ID_VALID "
		W = W & " AND VALIDAZIONE.FL_VALID =1"

		if request("ID_AREA") <>"" then
			W = W &  " AND FIGUREPROFESSIONALI.ID_AREAPROF = " & request("ID_AREA")
		end if

		W = W & " AND UPPER(FIGUREPROFESSIONALI.DENOMINAZIONE) LIKE '%" & UCASE(REQUEST("testo")) & "%'"

		Sql = Sql & W & " ORDER BY FIGUREPROFESSIONALI.DENOMINAZIONE "

		SqlCount = ""
		SqlCount = SqlCount & "SELECT count(FIGUREPROFESSIONALI.ID_FIGPROF) maxresult "
		SqlCount = SqlCount & "FROM FIGUREPROFESSIONALI, VALIDAZIONE, AREE_PROFESSIONALI "
		SqlCount = SqlCount & "WHERE "
		SqlCount = SqlCount & W

		TestoMsg= ""
		TestoMsg=TestoMsg & "Lista de <b>Figuras Profesionales</b> que satisfacen el criterio de busqueda ingresado."
	
		Cappello = ""
		Cappello = Cappello & "<a href=""Javascript:Show_Help('" & Session("Progetto") & "/HelpBp/Validazione/Ricerche/Resultricerca2.htm')"">"
		Cappello = Cappello & "<img src='" & Session("Progetto")& "/images/Help.gif' border='0'  align=right alt='Help'></a>"

		Puls="Figura Profesional"
	end if

	if request("Competencia") <> "" then
		Tipologia="Competencia"

		Sql = ""
		Sql = Sql & "SELECT "
		Sql = Sql & " DISTINCT COMPETENZE.DENOMINAZIONE, COMPETENZE.ID_COMPETENZA, "
		Sql = Sql & " VALIDAZIONE.FL_VALID "
		Sql = Sql & " FROM COMPETENZE, VALIDAZIONE, COMPET_FP, FIGUREPROFESSIONALI "
		Sql = Sql & " WHERE "
		W = W & " COMPETENZE.ID_VALID = VALIDAZIONE.ID_VALID AND "
		W = W & "  VALIDAZIONE.FL_VALID =1 AND "
		W = W & "  COMPET_FP.ID_COMPETENZA = COMPETENZE.ID_COMPETENZA AND "
		W = W & "  COMPET_FP.ID_FIGPROF= FIGUREPROFESSIONALI.ID_FIGPROF"

		if request("ID_AREA") <>"" then
			W = W &  " AND FIGUREPROFESSIONALI.ID_AREAPROF = " & request("ID_AREA")
		end if
			W = W &  " AND UPPER(COMPETENZE.DENOMINAZIONE) LIKE '%" & UCASE(REQUEST("testo")) & "%'"

		Sql = Sql & W & "   ORDER BY COMPETENZE.DENOMINAZIONE"

		SqlCount = ""
		SqlCount = SqlCount & "SELECT count(COMPETENZE.ID_COMPETENZA) maxresult "
		SqlCount = SqlCount & "FROM COMPETENZE, VALIDAZIONE, COMPET_FP, FIGUREPROFESSIONALI "
		SqlCount = SqlCount & "WHERE "
		SqlCount = SqlCount & W

		Puls="Competencia"
		
		TestoMsg= ""
		TestoMsg=TestoMsg & "Lista de las <b>Competencias</b> que satisfacen el criterio de busqueda ingresado."
	
		Cappello = ""
		Cappello = Cappello & "<a href=""Javascript:Show_Help('" & Session("Progetto") & "/HelpBp/Validazione/Ricerche/Resultricerca4.htm')"">"
		Cappello = Cappello & "<img src='" & Session("Progetto")& "/images/Help.gif' border='0'  align=right alt='Help'></a>"

	end if

	Set Rs = server.CreateObject ("ADODB.Recordset")

		Rs.Open Sql,Conn,3,3,1
		if not rs.eof then
			Rs.Move (lDwn)
			RESULT = Rs.getrows(20)
'PL-SQL * T-SQL  
SQLCOUNT = TransformPLSQLToTSQL (SQLCOUNT) 
			RsCount = Conn.Execute(SqlCount)
			RC = RsCount(0)
			Set RsCount = Nothing
		end if
	Conn.Close
	Set conn = Nothing

end if
%>
<script language='Javascript'>
	<!--
		function mandaAvanti(lDwn)
		{
		A = lDwn + 20

		URL =  "ResultRicerca.asp?"
		URL += "lDwn=" + A + "&"
		URL += 'testo=<%=CleanXsql(request("testo"))%>&'
		URL += 'ID_AREA=<%=request("ID_AREA")%>&'
		URL += 'ID_FIGPROF=<%=request("ID_FIGPROF")%>&'
		URL += 'TitoloArea=<%=request("TitoloArea")%>&'
		URL += '<%=Puls%>=<%=Puls%>'
		goToPage(URL)
		}

		function mandaIndietro(lDwn)
		{
		A = lDwn - 20

		URL =  "ResultRicerca.asp?"
		URL += "lDwn=" + A + "&"
		URL += 'testo=<%=CleanXsql(request("testo"))%>&'
		URL += 'ID_AREA=<%=request("ID_AREA")%>&'
		URL += 'ID_FIGPROF=<%=request("ID_FIGPROF")%>&'
		URL += 'TitoloArea=<%=request("TitoloArea")%>&'
		URL += '<%=Puls%>=<%=Puls%>'
		goToPage(URL)
		}

	function FocusFigProf(ID_FIGPROF)
	{
		URL = "../Ricerche/FocusFigProf.asp?"
		URL = URL + "ID_FIGPROF=" + ID_FIGPROF + "&VISMENU=NO"

		opt = "address=no,status=no,width=610,height=550,top=150,left=35,scrollbars=yes"

		window.open (URL,"",opt)
	}

	function FocusCompet(ID_COMPETENZA)
	{
		URL = "../Ricerche/FocusCompet.asp?"
		URL = URL + "ID_COMPETENZA=" + ID_COMPETENZA + "&VISMENU=NO" 

		opt = "address=no,status=no,width=660,height=550,top=50,left=35,scrollbars=yes"

		window.open (URL,"",opt)
	}
	
	function AssociaFP(ID,DEN)
	{
		URL="DettFigProf.asp?ID_FIGPROF=" + ID + "&DENOMINAZIONE=" + DEN 
		goToPage(URL)
	}

	function AssociaComp(ID,DEN)
	{
		URL="DettEle_Compet.asp?ID_COMPETENZA=" + ID + "&DENOMINAZIONE=" + DEN 
		goToPage(URL)
	}
	
	function AbilitaValidazione(A,B,C)
	{
		URL = "AbilitaValidazione.asp?ID_FIGPROF=" + A + "&FL_STATOLAV=" + B + "&ID_VALID=" + C 
		goToPage(URL)
	}

	function RiepilogoFigProf(C,D)
	{
		URL = "riepilogoFigProf.asp?"
		URL = URL + "ID_FIGPROF=" + C + "&DENOMINAZIONE=" + D 
		URL =  URL + "&Figure Professionali=<%=Request("Figura Profesional")%>"
		URL =  URL + "&Competenze=<%=Request("Competencia")%>" 
		goToPage(URL)
	}
//-->
</script>

<CENTER>
<table border=0 width='500' CELLPADDING=0 cellspacing=0>
	<tr>
		<td class=tbltext0  bgcolor='#3399CC' align=left bordercolor="#C2E0FF" width='350'>
			<b>ELEMENTOS VALIDADOS </b>
		</td>
	<td width="25" valign='bottom' background="<%=session("progetto")%>/images/sfondo_linguetta.gif" >
		<img border="0" src="<%=session("progetto")%>/images/tondo_linguetta.gif"></td>
	<td width="278" valign='bottom' background="<%=session("progetto")%>/images/sfondo_linguetta.gif" ></td>
	</tr>
</table>
<table border=0 width='500' CELLPADDING=0 cellspacing=0>
   <tr class=sfondocomm>
		<td align="left" colspan="3" >
		<%=TestoMsg%>
		</td>
		<td>
		<%=Cappello%>
		</td>
    </tr>
    <tr>
		<td colspan=4 bgcolor='#3399cc'></td>
    </tr>
</table>
<br>

<form name='ListaItem' Method=POST Action='SalvaAssociaEle_Compet.asp'>
<input type="hidden" name="IdPers" value="<%=sIdPers%>">
<input type="hidden" name="IND_FASE" value="<%=IND_FASE%>">

<%

	if request("lDwn") = "" Or clng(request("lDwn")) < 0 then
		lDwn = 0
	else
		lDwn = request("lDwn")
	end if

	if IsArray(RESULT) then
		Response.Write "<input type='hidden' name='limite' value='" & Ubound(RESULT,2) & "'>"
		Pag= clng(RC) mod 20
		
		if Pag> 0 then
			Pagine= 0 + int(clng(RC)/20)+1
		else
			Pagine = 0 + int(clng(RC)/20)
		end if

		Response.Write "<table width='500' border=0 cellspacing=1 cellpadding=1>"
			Response.Write "<tr class='sfondocomm'>"
				Response.Write "<td valign=top width='50%' align='right' colspan='4'>"
				Response.write "<b>Página:</b> " & Pagine & "&nbsp;&nbsp;&nbsp;<b>Nro. de Elementos :</b> " & RC  & "</b>"
				Response.Write "</td>"
				Response.Write "</tr>"
				Response.Write "<tr class=sfondocomm>"
					Response.write "<td align='left'><small>"
					Response.Write "<small>Criterio de Busqueda <br>"
					Response.Write "<b>Texto: " & request("testo") & "<br>AREA : " & request("TitoloArea") & "</small>"
					Response.Write "</td>"
				Response.Write "</tr>"
		Response.Write "</table>"

		Response.Write "<table border=0 cellspacing=1 width='500'>"
			Response.Write "<tr class='sfondocomm'>"
				Response.Write "<td>&nbsp;</td>"
				Response.Write "<td class='tbltext1'><b>Denominación</b></td>"
				Response.Write "<td class='tbltext1' WIDTH='15%' align=center><b>Validado</b></td>"
				Response.write "<td class='tbltext1'align='Center'><b>Opciones</b></td>"
			Response.Write "</tr>"

			for I = lbound(RESULT,2) to Ubound(RESULT,2)
			
				if (i mod 2)= 0 then
					sfondo="class='tblsfondo'"
				else
					sfondo="class='tbltext1'"
				end if
			
				Response.Write "<tr " & sfondo & ">"
					Response.Write "<td align='center' class='tbltext1'><b>"

					if Tipologia="Competencia" then
						Response.Write "&nbsp;<a href='javascript: FocusCompet(" & RESULT(1,I) & ")' title=' Detalle de la Competencia'>"

					end if
					if Tipologia="Figura Profesional" then
						Response.Write "&nbsp;<a href='javascript: FocusFigProf(" & RESULT(1,I) & ")' title=' Detalle de la Figura Profesional'>"
					end if

					Response.write "<SMALL>" & I + 1 + lDwn & "</SMALL></b><a>&nbsp;&nbsp;</td>"
					Response.Write "<td class='tbltext1' ><SMALL>" & CleanXsql(RESULT(0,I)) & "</SMALL></td>"

					Response.Write "<td class='tbltext1' align='center'><b>"

					if RESULT(2,I)="1" then
						Response.Write "<font color=green>SI"
					else
						Response.Write "<font color=red>NO"
					end if

					Response.Write "</font></b></td>"

					Response.Write "<td class='tbltext1' align='center'>"

					if Tipologia="Figura Profesional" then
						Response.Write "<input type='button' value='Asociar' CLASS='My' OnClick=""AssociaFP(" & RESULT(1,I)& ",'" & Server.HTMLEncode(RESULT(0,I)) & "')"">"
					end if

					if Tipologia="Competencia" then
						Response.Write "<input type='button' value='Asociar' CLASS='My' OnClick=""AssociaComp(" & RESULT(1,I) & ",'" & CleanXsql(RESULT(0,I)) & "')"">"
					end if

					if Tipologia="Figura Profesional" then
						'Response.Write "<input type='button' value='Valida' OnClick='AbilitaValidazione(" & RESULT(1,I) & "," & RESULT(4,I) & "," & RESULT(2,I) &  ")' CLASS='My'>&nbsp;"
						Response.Write "<input type='button' value='Ver Detalle' OnClick=""RiepilogoFigProf(" & RESULT(1,I) & ",'" & Server.HTMLEncode(RESULT(0,I)) & "')"" CLASS='My'>&nbsp;"
					end if
					if Tipologia="Competencia" then
						Response.Write "<input type='button' value='Ver Detalle' OnClick='goToPage(""RiepilogoEle_Compet.asp?ID_COMPETENZA=" & RESULT(1,I) & "&DENOMINAZIONE=" & cleanXsql(RESULT(0,I)) & """)' CLASS='My'>&nbsp;"
					end if
					Response.Write "</td>"
				Response.Write "</tr>"
			next

			Response.Write "<tr>"
			Response.Write "<td colspan='4'>&nbsp;"
			Response.Write "</td>"
			Response.Write "</tr>"
							
			Response.Write "<tr>"
			Response.Write "<td align='left'>"
			Response.Write "		<input type='button' value='Cancelar' onClick=""goToPage('TipoRicerca.asp')"" CLASS='My' >"
			Response.Write "</td>"
			
			Response.Write "<td colspan=3 class='tbltext1' align=center>"
				if lDwn > 0 then
					Response.Write "<input type='button' value='Volver' OnClick='mandaIndietro(" & lDwn & ")' Class='My'>"
				else
					Response.Write "<input type='button' value='Volver' OnClick='' Class='My' disabled>"
				end if

			Response.Write "<b>&nbsp;Pag: " & clng((lDwn + 20)/20) & "&nbsp;&nbsp;de " & Pagine & "&nbsp;</b>"
				if I + 1 +lDwn < clng(RC) then
					Response.Write "<input type='button' value='Avanzar' OnClick='mandaAvanti(" & lDwn & ")' Class='My'>"
				else
					Response.Write "<input type='button' value='Avanzar' OnClick='' Class='My' disabled>"
				end if

			Response.Write "</td>"
			Response.Write "</tr>"
			Response.Write "</table>"
	
	Else
	
		Response.Write "<table border=0 cellspacing=1 width='500'>"
			Response.Write "<tr>"
				Response.Write "<td class='tbltext3'><b>No se encontraron elementos que satisfagan los criterios de busqueda ingresados</b>"
				Response.Write "</td>"
			Response.Write "</tr>"	
			Response.Write "<tr>"
				Response.Write "<td>&nbsp;</td>"
			Response.Write "</tr>"	
			Response.Write "<tr>"
				Response.Write "<td align='left'>"
				Response.Write "<input type='button' value='Cancelar' onClick=""goToPage('TipoRicerca.asp')"" CLASS='My'>"
				Response.Write "</td>"
			Response.Write "</tr>"	
			Response.Write "<tr>"
				Response.Write "<td  bgcolor='#3399CC'></td>"
			Response.Write "</tr>"
		Response.Write "</table>"
	end if
Response.Write "</form>"
Response.Write "</CENTER>"
%>
<!-- #include Virtual="/strutt_coda2.asp" -->
