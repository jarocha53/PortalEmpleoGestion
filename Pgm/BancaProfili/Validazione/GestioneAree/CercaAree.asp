<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #include Virtual="/strutt_testa2.asp" -->
<!-- #INCLUDE FILE="../Utils.asp" -->
<!-- #INCLUDE FILE="MenuAree.asp" -->
	<script language='Javascript'>
	<!--
		function AreaPart()
			{
				obj=document.CercaFP;
				testo = obj.ID_AREAPART.options[obj.ID_AREAPART.selectedIndex];
				obj.AREA_PART.value = testo.title
			}

		function AreaDest()
			{
				obj=document.CercaFP;
				testo = obj.ID_AREADEST.options[obj.ID_AREADEST.selectedIndex];
				obj.AREA_DEST.value = testo.title
			}

		function chkfrm(frm)
		{
				if ( (frm.ID_AREADEST.options[frm.ID_AREADEST.selectedIndex].value=="") || (frm.ID_AREAPART.options[frm.ID_AREAPART.selectedIndex].value==""))
				{
					alert("Especificar el �rea de Pertenencia y de Destino");
					return false;
				}
				if ((frm.ID_AREADEST.options[frm.ID_AREADEST.selectedIndex].value) == (frm.ID_AREAPART.options[frm.ID_AREAPART.selectedIndex].value))
				{
					alert("Las Areas de origen y destino no deben ser las mismas");
					return false;
				}
		}

		function ValidaForm2(frm)
		{
				if ( (frm.Denominazione.value=="") || (frm.Tipologia.options[frm.Tipologia.selectedIndex].value==""))
				{
					alert("Los campos DENOMINACION y TIPOLOGIA son obligatorios");
					return false;
				}
		}
	//-->
	</script>

<CENTER>
<%
	if request("selettore") <> "" then
%>
<table border=1 width='500' cellspacing=1 cellpadding=0>
	<%
	select case request("selettore")
		case "Comportamiento"
			Sql = "SELECT ID_AREACOMPORTAMENTO, DENOMINAZIONE FROM AREA_COMPORTAMENTI ORDER BY UPPER(DENOMINAZIONE)"
			Area = "Comportamiento"
			Cappello = ""
			Cappello = Cappello & "<a href=""Javascript:Show_Help('" & Session("Progetto") & "/HelpBp/Validazione/GestioneAree/CercaAree.htm')"">"
			Cappello = Cappello & "<img src='" & Session("Progetto")& "/images/Help.gif' border='0'  align=right alt='Help'></a>"
		case "Capacidad"
			Sql = "SELECT ID_AREACAPACITA, DENOMINAZIONE FROM AREA_CAPACITA ORDER BY UPPER(DENOMINAZIONE)"
			Area = "Capacidad"
			Cappello = ""
			Cappello = Cappello & "<a href=""Javascript:Show_Help('" & Session("Progetto") & "/HelpBp/Validazione/GestioneAree/CercaAree1.htm')"">"
			Cappello = Cappello & "<img src='" & Session("Progetto")& "/images/Help.gif' border='0'  align=right alt='Help'></a>"
		case "Conocimiento"
			Sql = "SELECT ID_AREACONOSCENZA, DENOMINAZIONE FROM AREA_CONOSCENZA ORDER BY UPPER(DENOMINAZIONE)"
			Area = "Conocimiento"
			Cappello = ""
			Cappello = Cappello & "<a href=""Javascript:Show_Help('" & Session("Progetto") & "/HelpBp/Validazione/GestioneAree/CercaAree2.htm')"">"
			Cappello = Cappello & "<img src='" & Session("Progetto")& "/images/Help.gif' border='0'  align=right alt='Help'></a>"
		case "Area Profesional"
			Sql = "SELECT ID_AREAPROF, DENOMINAZIONE FROM AREE_PROFESSIONALI ORDER BY UPPER(DENOMINAZIONE)"
			Area = "Figura Profesional"
			Cappello = ""
			Cappello = Cappello & "<a href=""Javascript:Show_Help('" & Session("Progetto") & "/HelpBp/Validazione/GestioneAree/CercaAree3.htm')"">"
			Cappello = Cappello & "<img src='" & Session("Progetto")& "/images/Help.gif' border='0'  align=right alt='Help'></a>"
		end select

	if Sql<>"" then
	Response.Write "<form name='CercaFP' Action='ResultAree.asp?selettore=" & request("selettore") & "' Method='POST'  OnSubmit='return chkfrm(this)'>"
		set Conn = cc
		'Conn.Open strConn
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
		Set Rs = Conn.Execute (Sql)
			if not Rs.eof then

				AREE = Rs.getrows()

				Set Rs = nothing
				Set Conn = Nothing
	%>

  <table border=0 cellspacing=0 cellpadding=0 width='500'>
			      <tr>
				<td bgcolor='#3399CC' align=left  width='199' class='tbltext0'>
					<%
					Response.Write "<b>Gesti�n del Area " & (AREA) & "</b>"
				    %>
				</td>
			<td width="25" valign='bottom' background="<%=session("progetto")%>/images/sfondo_linguetta.gif" >
				<img border="0" src="<%=session("progetto")%>/images/tondo_linguetta.gif"></td>
			<td width="278"  class='tbltext1' align='right' valign='middle'  background="<%=session("progetto")%>/images/sfondo_linguetta.gif" >(*) campo obligatorio</td>
			</tr>
  </table>
  <table border=0 width='500' CELLPADDING=0 cellspacing=0>
		<tr class="sfondocomm">
		      <td>Este procedimiento le permite importar elementos de otra area de agrupamiento.</td>

		    <td><%=cappello%></td>
		  </tr>
		  <tr>
		  	    <td colspan=2 bgcolor='#3399CC'></td>
          </tr>
  </table>

  <table border=0 cellspacing=1 cellpadding=1 width='500'>
	<tr>
	<td align=center>

<%

					Response.Write "<tr>"
					Response.Write "<tr>"
					Response.Write "<td class='tbltext1' align=left><b>Area de Pertenencia*&nbsp;</b></td>"

					Response.Write "<td class='tbltext1'>&nbsp;"
					if isArray(AREE) then

						Response.Write "<select  class='MY' name='ID_AREAPART' OnChange='AreaPart()'>"
						Response.Write "<option value=''></option>"

							for I = lbound(AREE,2) to Ubound(AREE,2)
								Response.Write "<option value='" & AREE(0,I) & "' title='" & replace(AREE(1,I),"'","`") & "'>" & replace(AREE(1,I),"'","`") & "</option>"
							next
						Response.Write "</select>"
					end if

					Response.Write "</td>"
					Response.Write "</tr>"

					Response.Write "<tr>"
					Response.Write "<td align=left class='tbltext1'><b>Area de Destino*&nbsp;</b></td>"
					Response.Write "<td>&nbsp;"
					if isArray(AREE) then
						Response.Write "<select  class='MY' name='ID_AREADEST' OnChange='AreaDest()'>"
						Response.Write "<option value=''></option>"
							for I = lbound(AREE,2) to Ubound(AREE,2)
								Response.Write "<option value='" & AREE(0,I) & "' title='" & replace(AREE(1,I),"'","`") & "'>" & replace(AREE(1,I),"'","`")  & "</option>"
							next
						Response.Write "</select>"
					end if
					Response.Write "</td>"
					Response.Write "</tr>"

					Response.Write "<tr>"
						Response.Write "<td colspan=2 align=right>"
						Response.Write "<input type='hidden' name='AREA_PART' value=''>"
						Response.Write "<input type='hidden' name='AREA_DEST' value=''>"
						Response.Write "<input type='hidden' name='selettore' value='" & request("selettore") & "'>"
						Response.Write "<input type='Submit' name='" & request("selettore") & "' value='Buscar' CLASS='My'>"
						Response.Write "</td>"
						Response.Write "</tr>"
						Response.Write "<tr>"
				        Response.Write "<td colspan=3 bgcolor='#3399CC'></td>"
	                    Response.Write "</tr>"

			end if
			Response.Write "</form>"
	else
		Response.Write "<form name='SalvaNew' Action='SalvaNewArea.asp' Method='POST'  OnSubmit='return ValidaForm2(this)'>"
		%>

</td>
</tr>
</table>
<input type='hidden' name='VISMENU' value='NO'>
<table border=0 cellspacing=0 cellpadding=0 width='500'>
		    <tr>
				<td bgcolor='#3399CC' align=left  width='199' class='tbltext0'>
					<b>Ingresar un nuevo Agrupamiento</b>
				</td>
			<td width="25" valign='bottom' background="<%=session("progetto")%>/images/sfondo_linguetta.gif" >
				<img border="0" src="<%=session("progetto")%>/images/tondo_linguetta.gif"></td>
			<td width="278"  class='tbltext1' align='right' valign='middle'  background="<%=session("progetto")%>/images/sfondo_linguetta.gif" >(*) campo obligatorio</td>
			</tr>
</table>
<table border=0 width='500' CELLPADDING=0 cellspacing=0>
	<tr class="sfondocomm">
		<td>
		Este procedimiento le permite ingresar un nuevo agrupamiento.
		</td>
	   	<td class=sfondocomm>
	   	<a href="Javascript:Show_Help('<%=Session("Progetto")%>/HelpBP/Validazione/GestioneAree/CercaAree8.htm')">
		<img src="<%=Session("Progetto")%>/images/Help.gif" border='0'  align=right alt='Help'></a>
		</td>
    </tr>
    <tr>
		<td colspan=2 bgcolor='#3399CC'></td>
    </tr>
</table>
<table border=0 cellspacing=1 cellpadding=1 width='500'>
<tr>
<td align=center>

	<%
		Response.Write "<tr>"
			Response.Write "<td align=LEFT class='tbltext1'><b>Denominaci�n del Area*&nbsp;</b></td>"
			Response.Write "<td><input type='text' class='MyTextBox' size='50' maxlength='60' name='Denominazione' value=''></td>"
		Response.Write "</tr>"

		Response.Write "<tr>"
			Response.Write "<td class='tbltext1' align=LEFT><b>Tipo de Agrupamiento*&nbsp;</b></td>"
			Response.Write "<td>"
			Response.Write "<select  class='MY' name='Tipologia'>"
				Response.Write "<option value='' selected></option>"
				Response.Write "<option value='AREA_CONOSCENZA'>CONOCIMIENTO</option>"
				Response.Write "<option value='AREA_CAPACITA'>DESEMPE�O</option>"
				Response.Write "<option value='AREA_COMPORTAMENTI'>COMPORTAMIENTO</option>"
				Response.Write "<option value='AREE_PROFESSIONALI'>AREA PROFESIONAL</option>"
			Response.Write "</select>"
		Response.Write "</td>"
		Response.Write "</tr>"

		Response.Write "<tr>"
			Response.Write "<td class='tbltext1' align=LEFT><b>NOTA&nbsp;</b></td>"
			Response.Write "<td><textarea name='Nota' class='MyTextBox' cols=50 rows=3></textarea></td>"
		Response.Write "</tr>"

		Response.Write "<tr>"
			Response.Write "<td  align=right colspan=2>"
			Response.Write "<input type='submit' name='Salva' value='Guardar' CLASS='My'>"
			Response.Write "</td>"
		Response.Write "</tr>"
		Response.Write "<tr>"
		Response.Write "<td colspan=3 bgcolor='#3399CC'></td>"
	    Response.Write "</tr>"
		Response.write "</form>"
	end if
%>
</table>

</td>
</tr>
</table>

<%
	end if
%>
<!-- #include Virtual="/strutt_coda2.asp" -->
