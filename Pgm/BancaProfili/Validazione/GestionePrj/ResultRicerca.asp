<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #include Virtual="/strutt_testa2.asp" -->
<!-- #INCLUDE FILE="../Utils.asp" -->
<%

if request("lDwn") = "" Or Int(request("lDwn")) < 0 then
	lDwn = 0
else
	lDwn = request("lDwn")
end if

if request("Impresa") <> "" OR request("Processi") <> "" OR request("Figure Professionali") <> "" OR request("Capacit�") <> "" OR request("Comportamenti") <> "" OR request("Competenze") <> "" OR request("Conoscenze") <> ""   then

Set Conn = server.CreateObject ("ADODB.Connection")
Conn.open strConn

if request("Conoscenze") <> "" then
	Tipologia="Conoscenze"

	Sql = ""
	Sql = Sql & "SELECT "
	Sql = Sql & "CONOSCENZE.DENOMINAZIONE, CONOSCENZE.ID_CONOSCENZA, VALIDAZIONE.FL_VALID "
	Sql = Sql & "FROM "
	Sql = Sql & "CONOSCENZE, VALIDAZIONE, AREA_CONOSCENZA "
	Sql = Sql & "WHERE "

	W= ""
	W = W & "AREA_CONOSCENZA.ID_AREACONOSCENZA = CONOSCENZE.ID_AREACONOSCENZA "
	W = W & "AND "
	W = W & "CONOSCENZE.ID_VALID = VALIDAZIONE.ID_VALID "
	W = W & "AND "
	W = W & " UPPER(CONOSCENZE.DENOMINAZIONE) LIKE '%" & ucase(CleanXsql(request("testo"))) & "%' "
	if request("ID_AREA") <> "" then
		W = W & "AND "
		W = W & "CONOSCENZE.ID_AREACONOSCENZA = " & request("ID_AREA")
	end if

	Sql = Sql & W & " ORDER BY CONOSCENZE.DENOMINAZIONE "

	SqlCount= ""
	SqlCount= SqlCount & " SELECT COUNT(CONOSCENZE.ID_CONOSCENZA) maxresult "
	SqlCount= SqlCount & "FROM CONOSCENZE, VALIDAZIONE, AREA_CONOSCENZA "
	SqlCount= SqlCount & "WHERE "
	SqlCount= SqlCount & W

	Puls= "Conoscenze"
	TestoMsg= ""
	TestoMsg=TestoMsg & "E� uno degli elementi della competenza. Denota prevalentemente l�avvenuta "
	TestoMsg=TestoMsg & " acquisizione e memorizzazione di un contenuto conoscitivo (fatti, regole, norme, "
	TestoMsg=TestoMsg & " concetti, teorie, ecc.); � una padronanza mentale, formale, di per s� astratta "
	TestoMsg=TestoMsg & " dall�operativit�. Attiene al sapere � e quindi anche al sapere come fare (know "
	TestoMsg=TestoMsg & "how) � non al fare, al saper fare, n� all�essere."
end if

if request("Capacit�") <> "" then
	Tipologia="Capacit�"

	Sql = ""
	Sql = Sql & "SELECT "
	Sql = Sql & "CAPACITA.DENOMINAZIONE, CAPACITA.ID_CAPACITA, VALIDAZIONE.FL_VALID "
	Sql = Sql & "FROM "
	Sql = Sql & "CAPACITA, VALIDAZIONE, AREA_CAPACITA "
	Sql = Sql & "WHERE "

	W= ""
	W = W & "AREA_CAPACITA.ID_AREACAPACITA = CAPACITA.ID_AREACAPACITA "
	W = W & "AND "
	W = W & "CAPACITA.ID_VALID = VALIDAZIONE.ID_VALID "
	W = W & "AND "
	W = W & " UPPER(CAPACITA.DENOMINAZIONE) LIKE '%" & ucase(CleanXsql(request("testo"))) & "%' "
	if request("ID_AREA") <> "" then
		W = W & "AND "
		W = W & "CAPACITA.ID_AREACAPACITA = " & request("ID_AREA")
	end if

	Sql = Sql & W & " ORDER BY CAPACITA.DENOMINAZIONE"

	SqlCount= ""
	SqlCount= SqlCount & " SELECT COUNT(CAPACITA.ID_CAPACITA) maxresult "
	SqlCount= SqlCount & "FROM CAPACITA, VALIDAZIONE , AREA_CAPACITA "
	SqlCount= SqlCount & "WHERE "
	SqlCount= SqlCount & W

	Puls="Capacit�"
	TestoMsg= ""
	TestoMsg=TestoMsg & " E� uno degli elementi della competenza (v.). Denota l'essere in grado di "
	TestoMsg=TestoMsg & "utilizzare specifici strumenti operativi (tecniche, metodi, tecnologie ecc.) per" 
	TestoMsg=TestoMsg & "la realizzazione di un compito. Attiene al (saper) fare." 


end if

if request("Comportamenti") <> "" then
	Tipologia="Comportamenti"

	Sql = ""
	Sql = Sql & "SELECT "
	Sql = Sql & "COMPORTAMENTI.DENOMINAZIONE, COMPORTAMENTI.ID_COMPORTAMENTO, VALIDAZIONE.FL_VALID "
	Sql = Sql & "FROM "
	Sql = Sql & "COMPORTAMENTI, VALIDAZIONE, AREA_COMPORTAMENTI "
	Sql = Sql & "WHERE "

	W= ""
	W = W & "AREA_COMPORTAMENTI.ID_AREACOMPORTAMENTO = COMPORTAMENTI.ID_AREACOMPORTAMENTO "
	W = W & "AND "
	W = W & "COMPORTAMENTI.ID_VALID = VALIDAZIONE.ID_VALID "
	W = W & "AND "
	W = W & " UPPER(COMPORTAMENTI.DENOMINAZIONE) LIKE '%" & ucase(CleanXsql(request("testo"))) & "%' "
	if request("ID_AREA") <> "" then
		W = W & "AND "
		W = W & "COMPORTAMENTI.ID_AREACOMPORTAMENTO = " & request("ID_AREA")
	end if

	Sql = Sql & W & " ORDER BY COMPORTAMENTI.DENOMINAZIONE"

	SqlCount= ""
	SqlCount= SqlCount & " SELECT COUNT(COMPORTAMENTI.ID_COMPORTAMENTO) maxresult "
	SqlCount= SqlCount & "FROM COMPORTAMENTI, VALIDAZIONE, AREA_COMPORTAMENTI "
	SqlCount= SqlCount & "WHERE "
	SqlCount= SqlCount & W

	Puls = "Comportamenti"
	TestoMsg= ""
	TestoMsg=TestoMsg & "E� uno degli elementi della competenza (unitamente a conoscenze e capacit�)." 
	TestoMsg=TestoMsg & "Coniuga doti e attitudini personali con necessit� espresse dall�organizzazione e "
	TestoMsg=TestoMsg & "dalle persone clienti/utenti. Denota l�essere in grado di comunicare, operare, "
	TestoMsg=TestoMsg & "interagire, ecc. in coerenza con uno specifico contesto ambientale e "
	TestoMsg=TestoMsg & "organizzativo e con i suoi valori di riferimento. Attiene al (saper) essere.  "
								

end if

if request("Competenze") <> "" then
	Tipologia="Competenze"

	Sql = ""
	Sql = Sql & "SELECT "
	Sql = Sql & " COMPETENZE.DENOMINAZIONE, COMPETENZE.ID_COMPETENZA, COMPETENZE.ID_VALID "
	Sql = Sql & " FROM COMPETENZE, VALIDAZIONE, COMPET_FP, FIGUREPROFESSIONALI "
	Sql = Sql & " WHERE "

	W = W & " COMPETENZE.ID_VALID = VALIDAZIONE.ID_VALID "
	W = W & " AND COMPET_FP.ID_COMPETENZA = COMPETENZE.ID_COMPETENZA "
	W = W & " AND COMPET_FP.ID_FIGPROF= FIGUREPROFESSIONALI.ID_FIGPROF"

	if request("ID_AREA") <>"" then
		W = W &  " AND FIGUREPROFESSIONALI.ID_AREAPROF = " & request("ID_AREA")
	end if
		W = W &  " AND UPPER(COMPETENZE.DENOMINAZIONE) LIKE '%" & UCASE(CleanXsql(REQUEST("testo"))) & "%'"

	Sql = Sql & W & " ORDER BY COMPETENZE.DENOMINAZIONE"

	SqlCount = ""
	SqlCount = SqlCount & "SELECT count(COMPETENZE.ID_COMPETENZA) maxresult "
	SqlCount = SqlCount & "FROM COMPETENZE, VALIDAZIONE, COMPET_FP, FIGUREPROFESSIONALI "
	SqlCount = SqlCount & "WHERE "
	SqlCount = SqlCount & W

	Puls="Competenze"
	TestoMsg= ""
	TestoMsg=TestoMsg & "Si definisce competenza �l'essere in grado di integrare le conoscenze, le" 
    TestoMsg=TestoMsg & "capacit�, i valori/comportamenti che consentono di realizzare l'output di "
	TestoMsg=TestoMsg & "un'attivit� richiesta in una specifica situazione�. In sede di analisi di un" 
	TestoMsg=TestoMsg & "processo produttivo, la competenza qualifica l�operatore che � in grado di "
	TestoMsg=TestoMsg & "realizzare l'output di un'attivit� principale/intermedia (un output, quindi, che" 
	TestoMsg=TestoMsg & "pur essendo intermedio per il processo, ha una propria completezza, una propria "
	TestoMsg=TestoMsg & "autonomia, un proprio valore quotabile anche in termini economici. "
	

end if

if request("Processi") <> "" then
	Tipologia="Processi"
	Sql = ""
	Sql = Sql & "SELECT "
	Sql = Sql & "PROCESSI.DESCRIZIONE, PROCESSI.ID_PROCESSO, VALIDAZIONE.FL_VALID "
	Sql = Sql & "FROM "
	Sql = Sql & "PROCESSI, VALIDAZIONE "
	Sql = Sql & "WHERE "

	W= ""
	W = W & "PROCESSI.ID_VALID = VALIDAZIONE.ID_VALID "
	W = W & "AND "
	W = W & " UPPER(PROCESSI.DESCRIZIONE) LIKE '%" & ucase(CleanXsql(request("testo"))) & "%' "

	Sql = Sql & W & " ORDER BY PROCESSI.DESCRIZIONE"

	SqlCount = ""
	SqlCount = SqlCount & "SELECT count(PROCESSI.ID_PROCESSO) maxresult "
	SqlCount = SqlCount & "FROM PROCESSI, VALIDAZIONE "
	SqlCount = SqlCount & "WHERE "
	SqlCount = SqlCount & W

	Puls="Processi"
    
    TestoMsg= ""
	TestoMsg=TestoMsg & "Flujo de actividades empresariales en que son llevadas a la pr�ctica las competencias " 
    TestoMsg=TestoMsg & "solicitadas a las figuras profesionales.  "

end if

if request("Figure Professionali") <> "" then
	Tipologia="Figure Professionali"

	Sql = ""
	Sql = Sql & "SELECT "
	Sql = Sql & "FIGUREPROFESSIONALI.DENOMINAZIONE, FIGUREPROFESSIONALI.ID_FIGPROF, VALIDAZIONE.FL_VALID, "
	Sql = Sql & "FIGUREPROFESSIONALI.COD_EJ, CODICI_ISTAT.MANSIONE "
	Sql = Sql & "FROM "
	Sql = Sql & "FIGUREPROFESSIONALI, VALIDAZIONE, AREE_PROFESSIONALI, CODICI_ISTAT "
	
	if request("ID_SETT") <> "" then
		Sql = Sql & " ,SETTORI_FIGPROF "
	END IF
	
	if request("ID_PROJ") <> "" then
		Sql = Sql & " ,FIGPROF_PRJ "
	END IF

	Sql = Sql & "WHERE "

	W= ""
	W = W & " CODICI_ISTAT.CODICE = FIGUREPROFESSIONALI.COD_EJ AND "
	W = W & " AREE_PROFESSIONALI.ID_AREAPROF = FIGUREPROFESSIONALI.ID_AREAPROF "
	W = W & " AND "
	W = W & "FIGUREPROFESSIONALI.ID_VALID = VALIDAZIONE.ID_VALID "
	W = W & " AND "
	W = W & " UPPER(FIGUREPROFESSIONALI.DENOMINAZIONE) LIKE '%" & ucase(CleanXsql(request("testo"))) & "%' "
	
	if request("ID_AREA") <> "" then
		W = W & "AND "
		W = W & " FIGUREPROFESSIONALI.ID_AREAPROF = " & request("ID_AREA")
	end if

	if request("ID_SETT") <> "" then
		W = W & "AND "
		W = W & " SETTORI_FIGPROF.ID_SETTORE = " & request("ID_SETT")
		W = W & "AND "
		W = W & " SETTORI_FIGPROF.ID_FIGPROF = FIGUREPROFESSIONALI.ID_FIGPROF"
	end if

	if request("ID_PROJ") <> "" then
		W = W & " AND "
		W = W & " UPPER(FIGPROF_PRJ.COD_PRJ) = '" & UCASE(request("ID_PROJ")) & "'"
		W = W & " AND "
		W = W & " FIGUREPROFESSIONALI.ID_FIGPROF = FIGPROF_PRJ.ID_FIGPROF "
	end if
	
	Sql = Sql & W & " ORDER BY FIGUREPROFESSIONALI.DENOMINAZIONE "

	SqlCount = ""
	SqlCount = SqlCount & "SELECT count(FIGUREPROFESSIONALI.ID_FIGPROF) maxresult "
	SqlCount = SqlCount & "FROM FIGUREPROFESSIONALI, VALIDAZIONE, AREE_PROFESSIONALI, CODICI_ISTAT "
	
	if request("ID_SETT") <> "" then
		SqlCount = SqlCount & " ,SETTORI_FIGPROF "
	END IF
	
	if request("ID_PROJ") <> "" then
		SqlCount = SqlCount & " ,FIGPROF_PRJ "
	END IF
	SqlCount = SqlCount & "WHERE "
	SqlCount = SqlCount & W

	Puls="Figure Professionali"
	TestoMsg= ""
	TestoMsg=TestoMsg & "Define una profesi�n en t�rminos de tareas, salida y procesos (o fases de "
	TestoMsg=TestoMsg & "proceso) asignados a su �rea de responsabilidad profesional. La profesi�n "
	TestoMsg=TestoMsg & "vuelve a llamar, por cada figura profesional, competencias definidas por cuyas salidas "
	TestoMsg=TestoMsg & "una figura es dedicada. "
			
end if

if request("Impresa") <> "" then
	Tipologia="Impresa"
	Sql = ""
	Sql = Sql & "SELECT "
	Sql = Sql & "IMPRESA.RAG_SOC, IMPRESA.ID_IMPRESA, SEDE_IMPRESA.PRV, SEDE_IMPRESA.ID_SEDE "
	Sql = Sql & "FROM "
	Sql = Sql & "IMPRESA, SEDE_IMPRESA "
	Sql = Sql & "WHERE "

	W= ""
	W = W & "IMPRESA.ID_IMPRESA = SEDE_IMPRESA.ID_IMPRESA "
	W = W & "AND "
	W = W & " UPPER(IMPRESA.RAG_SOC) LIKE '%" & ucase(CleanXsql(request("testo"))) & "%' "
	if request("IVA") <> "" then
		W = W & " AND UPPER(IMPRESA.PART_IVA) LIKE '" & ucase(CleanXsql(request("IVA"))) & "%' "
	end if

	Sql = Sql & W & " ORDER BY IMPRESA.RAG_SOC"

	SqlCount = ""
	SqlCount = SqlCount & "SELECT count(IMPRESA.ID_IMPRESA) maxresult "
	SqlCount = SqlCount & "FROM IMPRESA, SEDE_IMPRESA "
	SqlCount = SqlCount & "WHERE "
	SqlCount = SqlCount & W

	Puls="Impresa"

end if

	'Response.Write sql
	'Response.End 
	Set Rs = server.CreateObject ("ADODB.Recordset")
		Rs.Open Sql,Conn,3,3,1
		if not rs.eof then
			Rs.Move (lDwn)
			RESULT = Rs.getrows(20)
'PL-SQL * T-SQL  
SQLCOUNT = TransformPLSQLToTSQL (SQLCOUNT) 
			RsCount = Conn.Execute(SqlCount)
			RC = RsCount(0)
			Set RsCount = Nothing
		end if
	Conn.Close
	Set conn = Nothing

end if
%>
<script language='Javascript'>
<!--
	function ApriConosc()
	{
		URL = 'Conoscenza.asp?'
		URL = URL + 'ID_COMPETENZA=<%=request("ID_COMPETENZA")%>'

		opt = "address=no,status=no,width=500,height=480,top=150,left=35"

		window.open (URL,"",opt)
	}

	function ApriCapac()
	{
		URL = 'Capacita.asp?'
		URL = URL + 'ID_COMPETENZA=<%=request("ID_COMPETENZA")%>'

		opt = "address=no,status=no,width=500,height=480,top=150,left=35"

		window.open (URL,"",opt)
	}

	function mandaAvanti(lDwn)
	{
	A = lDwn + 20

	URL  =  "ResultRicerca.asp?"
	URL += "lDwn=" + A + "&"
	URL += 'testo=<%=CleanXsql(request("testo"))%>&'
	URL += 'ID_AREA=<%=request("ID_AREA")%>&'
	URL += 'ID_FIGPROF=<%=request("ID_FIGPROF")%>&'
	URL += 'TitoloArea=<%=request("TitoloArea")%>&'
	URL += '<%=Puls%>=<%=Puls%>'

	goToPage(URL)
	}

	function mandaIndietro(lDwn)
	{
	A = lDwn - 20

	URL =  "ResultRicerca.asp?"
	URL += "lDwn=" + A + "&"
	URL += 'testo=<%=CleanXsql(request("testo"))%>&'
	URL += 'ID_AREA=<%=request("ID_AREA")%>&'
	URL += 'ID_FIGPROF=<%=request("ID_FIGPROF")%>&'
	URL += 'TitoloArea=<%=request("TitoloArea")%>&'
	URL += '<%=Puls%>=<%=Puls%>'

	goToPage(URL)
	}

	function FocusCompet(ID_COMPETENZA)
	{
		URL = "FocusCompet.asp?"
		URL = URL + "ID_COMPETENZA=" + ID_COMPETENZA  + "&VISMENU=NO"

		opt = "address=no,status=no,width=600,height=480,top=150,left=35,scrollbars=yes"

		window.open (URL,"",opt)
	}

	function FocusImpresa(ID_IMPRESA,ID_SEDE)
	{
		URL = "FocusImpresa.asp?"
		URL = URL + "ID_IMPRESA=" + ID_IMPRESA
		URL = URL + "&ID_SEDE=" + ID_SEDE  + "&VISMENU=NO"

		opt = "address=no,status=no,width=600,height=400,top=150,left=35,scrollbars=yes"

		window.open (URL,"",opt)
	}

	function FocusFigProf(ID_FIGPROF)
	{
		URL = "FocusFigProf.asp?"
		URL = URL + "ID_FIGPROF=" + ID_FIGPROF + "&VISMENU=NO"

		opt = "address=no,status=no,width=660,height=570,top=150,left=35,scrollbars=yes"

		window.open (URL,"",opt)
	}
	function FocusProcessi(ID_PROCESSO)
	{
		URL = "FocusProcessi.asp?"
		URL = URL + "ID_PROCESSO=" + ID_PROCESSO + "&VISMENU=NO"

		opt = "address=no,status=no,width=600,height=480,top=50,left=35,scrollbars=yes"

		window.open (URL,"",opt)
	}

	function FocusConoscenza(ID_CONOSCENZA)
	{
		URL = "FocusConoscenza.asp?"
		URL = URL + "ID_CONOSCENZA=" + ID_CONOSCENZA  + "&VISMENU=NO"

		opt = "address=no,status=no,width=680,height=480,top=150,left=35,scrollbars=yes"

		window.open (URL,"",opt)
	}

	function FocusCapacita(ID_CAPACITA)
	{
		URL = "FocusCapacita.asp?"
		URL = URL + "ID_CAPACITA=" + ID_CAPACITA  + "&VISMENU=NO"

		opt = "address=no,status=no,width=600,height=480,top=150,left=35,scrollbars=yes"

		window.open (URL,"",opt)
	}

	function FocusComportamenti(ID_COMPORTAMENTO)
	{
		URL = "FocusComportamenti.asp?"
		URL = URL + "ID_COMPORTAMENTO=" + ID_COMPORTAMENTO  + "&VISMENU=NO"

		opt = "address=no,status=no,width=600,height=480,top=150,left=35,scrollbars=yes"

		window.open (URL,"",opt)
	}
//-->
</script>


<center>

<table border=0 width='500' CELLPADDING=0 cellspacing=0>
	<tr>
		<td class=tbltext0  bgcolor='#3399CC' align=left bordercolor="#C2E0FF" width='350'>
			<b>RESULTADO BUSQUEDA: <%=ucase(Tipologia)%></b>
		</td>
	<td width="25" valign='bottom' background="<%=session("progetto")%>/images/sfondo_linguetta.gif" >
		<img border="0" src="<%=session("progetto")%>/images/tondo_linguetta.gif"></td>
	<td width="278" valign='bottom' background="<%=session("progetto")%>/images/sfondo_linguetta.gif" ></td>
	</tr>
    <tr class=sfondocomm>
		<td class=tbltext1 align="left" colspan="3" >
		<%=TestoMsg%>
		</td>
    </tr>
    <tr>
		<td colspan=3 bgcolor='#3399cc'></td>
    </tr>
</table>


<form name='ListaItem' Method=POST Action='SalvaAssociaEle_Compet.asp'>
<input type="hidden" name="IdPers" value="<%=sIdPers%>">
<input type="hidden" name="IND_FASE" value="<%=IND_FASE%>">

<%
if request("lDwn") = "" Or Clng(request("lDwn")) < 0 then
	lDwn = 0
else
	lDwn = request("lDwn")
end if

	if IsArray(RESULT) then
		Response.Write "<input type='hidden' name='limite' value='" & Ubound(RESULT,2) & "'>"


		Pag= Clng(RC) mod 20
		if Pag> 0 then
			Pagine= 0 + int(Clng(RC)/20)+1
		else
			Pagine = 0 + int(Clng(RC)/20)
		end if

Response.Write "<table width='500' border=0 cellspacing=1 cellpadding=1>"
	Response.Write "<tr class='sfondocomm'>"
		Response.Write "<td valign=top width='50%' align='right' colspan='4'>"
		Response.write "<b>Pagina:</b> " & Pagine & "&nbsp;&nbsp;&nbsp;<b>Nro. Elementos :</b> " & RC  & "</b>"
		Response.Write "</td>"
		Response.Write "</tr>"
		Response.Write "<tr class=sfondocomm>"
			Response.write "<td align='left'><small>"
			Response.Write "<small>Criterio de B�squeda <br>"
			Response.Write "<b>Texto: " & request("testo") & "<br>AREA : " & request("TitoloArea") & "</small>"
			Response.Write "</td>"
		Response.Write "</tr>"
	Response.Write "<tr class='sfondocomm'>"
		Response.write "<td colspan=4 align='RIGHT'><b>Opciones&nbsp;&nbsp;</b></td>"
	Response.Write "</tr>"
Response.Write "</table>"
Response.Write "<table border=0 cellspacing=1 width='500'>"
	Response.Write "<tr>"
		Response.Write "<td>&nbsp;</td>"
		Response.Write "<td class='tbltext1'><b>Denominaci�n</b></td>"
			if Tipologia <> "Impresa" then
				Response.Write "<td WIDTH='15%' class='tbltext1' align=center><b>Validado</b></td>"
			else
				Response.Write "<td WIDTH='15%' class='tbltext1'><b>Provincia</b></td>"
			end if
	Response.Write "</tr>"
			for I = lbound(RESULT,2) to Ubound(RESULT,2)
			Response.Write "<input type='hidden' name='ID_" & SaveAs & I & "' value='" & RESULT(1,I) & "'>"
				
			if (i mod 2)= 0 then 
				sfondo="class='tblsfondo'" 
			else
				sfondo="class='tbltext1'"
			end if
				
				Response.Write "<tr " & sfondo & ">"
					Response.Write "<td align='center' class='tbltext1'><b>"

					if Tipologia="Competenze" then
						Response.Write "&nbsp;<a href='javascript: FocusCompet(" & RESULT(1,I) & ")' title='Visualizar Detalle Competencia'>"
					end if
					if Tipologia="Figure Professionali" then
						Response.Write "&nbsp;&nbsp;&nbsp;<a href='javascript: FocusFigProf(" & RESULT(1,I) & ")' title='Visualizar Detalle Figura Profesional'>"
					end if
					if Tipologia="Processi" then
						Response.Write "&nbsp;&nbsp;&nbsp;<a href='javascript: FocusProcessi(" & RESULT(1,I) & ")' title='Visualizar Proceso'>"
					end if
					if Tipologia="Conoscenze" then
						Response.Write "&nbsp;&nbsp;&nbsp;<a href='javascript: FocusConoscenza(" & RESULT(1,I) & ")' title='Visualizar Conocimiento'>"
					end if
					if Tipologia="Capacit�" then
						Response.Write "&nbsp;&nbsp;&nbsp;<a href='javascript: FocusCapacita(" & RESULT(1,I) & ")' title='Visualizar Detalle Desempe�o'>"
					end if
					if Tipologia="Comportamenti" then
						Response.Write "&nbsp;&nbsp;&nbsp;<a href='javascript: FocusComportamenti(" & RESULT(1,I) & ")' title='Visualizar Detalle Comportamiento'>"
					end if
					if Tipologia="Impresa" then
						Response.Write "&nbsp;&nbsp;&nbsp;<a href='javascript: FocusImpresa(" & RESULT(1,I) & "," & RESULT(3,I) & ")' title='Visualizar Perfiles Asociados'>"
					end if
					Response.write "" & I + 1 + lDwn & "</b><a>&nbsp;&nbsp;</td>"
					Response.Write "<td class='tbltext1'>" & RESULT(0,I) & "</td>"
					Response.Write "<td align='center' class='tbltext1'><b>"
					if Tipologia <> "Impresa" then
						if RESULT(2,I)="1" then
							Response.Write "<font color=green>SI"
						else
							Response.Write "<font color=red>NO"
						end if
					else
						Response.Write RESULT(2,I)
					end if
					Response.Write "</b></td>"
				Response.Write "</tr>"
			next

			Response.Write "<tr>"
		
		%>

		<td	valign='bottom' class='tbltext1'><br>
			<input type='button' value='Esci' onClick="goToPage('CercaElementi.asp')" CLASS='My'>
		</td>
		
		<%
				
			Response.Write "<td colspan=2 align=center  class='tbltext1'><br>"
				if lDwn > 1 then
					Response.Write "<input type='button' value='Atr�s' OnClick='mandaIndietro(" & lDwn & ")' Class='My'>"
				else
					Response.Write "<input type='button' value='Atr�s' OnClick='' Class='My' disabled>"
				end if

			Response.Write "<b>&nbsp;Pag: " & Clng((lDwn + 20)/20) & "&nbsp;&nbsp;di " & Pagine & "&nbsp;</b>"
			if I + 1 +lDwn <= Clng(RC) then
					Response.Write "<input type='button' value='Adelante' OnClick='mandaAvanti(" & lDwn & ")' Class='My'>"
				else
					Response.Write "<input type='button' value='Adelante' OnClick='' Class='My' disabled>"
				end if

			Response.Write "</td>"
			Response.Write "</tr>"
			Response.Write "</table>"
	else
		Response.Write "<table border=0 cellspacing=1 width=500>"
		Response.Write "<tr><td class=tbltext3 align=center><b>"
		Response.Write "No han sido encontrados elementos que satisfagan los criterios especificados"
		Response.Write "</b></td>"
		Response.Write "</tr>"
		Response.Write "<tr>"
		Response.Write "<td>"
		Response.Write "<input type='button' value='Atr�s' onClick='goToPage(""CercaElementi.asp"")' CLASS='My'>"
		Response.Write "</td>"
		Response.Write "</tr>"
		Response.Write "<tr>"
		Response.Write "<td colspan=3 bgcolor='#3399cc'></td>"
        Response.Write "</tr>"
		Response.Write "</table>"
	end if

%>
</form>
</center>
<!-- #include Virtual="/strutt_coda2.asp" -->
