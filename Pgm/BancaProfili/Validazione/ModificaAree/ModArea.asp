<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #include Virtual="/strutt_testa2.asp" -->
<!-- #INCLUDE FILE="../Utils.asp" -->
	<script language='Javascript'>
	<!--
		function chkfrm()
			{
				if (Ver_Capacita.Denominazione.value=="")
					{
						alert("El campo Nueva Denominación es obligatorio")
						return false;
					}
			}
			
	//-->
	</script>
<%
	Set Conn = cc
	'Conn.open strConn
	select case request("selettore")
		case "Comportamiento"
			Sql = "SELECT ID_AREACOMPORTAMENTO, DENOMINAZIONE, NOTA FROM AREA_COMPORTAMENTI WHERE ID_AREACOMPORTAMENTO=" & request("ID_AREA")
		case "Capacidad"
			Sql = "SELECT ID_AREACAPACITA, DENOMINAZIONE, NOTA FROM AREA_CAPACITA WHERE ID_AREACAPACITA=" & request("ID_AREA")
		case "Conocimiento"
			Sql = "SELECT ID_AREACONOSCENZA, DENOMINAZIONE, NOTA FROM AREA_CONOSCENZA WHERE ID_AREACONOSCENZA=" & request("ID_AREA")
		case "Area Profesional"
			Sql = "SELECT ID_AREAPROF, DENOMINAZIONE, NOTA FROM AREE_PROFESSIONALI WHERE ID_AREAPROF=" & request("ID_AREA")
	end select
	IF SQL<>"" THEN
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
		Set Rs = Conn.Execute (Sql)
		if not rs.eof then AREA = Rs.getrows()
		Set Rs = Nothing
	end if
	Conn.Close
	Set conn = Nothing

%>

<center>
<form name='Ver_Capacita' Method=POST Action='SalvaModArea.asp' OnSubmit="return chkfrm(this)">
<input type="hidden" name="IdPers" value="<%=sIdPers%>">
<input type="hidden" name="IND_FASE" value="<%=IND_FASE%>">
<input type='hidden' name='VISMENU' value='NO'>

<table border=0 width='500' CELLPADDING=0 cellspacing=0>
	<tr>
		<td bgcolor='#3399CC' align=left width='300' class="tbltext0">
		<B>MODIFICAR AREA: <%=UCASE(request("selettore"))%></B>
		</td>
		<td width="25" valign='bottom' background="<%=session("progetto")%>/images/sfondo_linguetta.gif" >
		<img border="0" src="<%=session("progetto")%>/images/tondo_linguetta.gif"></td>
		<td width="278" valign='MIDDLE' align="right" class=tbltext1 background="<%=session("progetto")%>/images/sfondo_linguetta.gif" >(*) campo obligatorio</td>
	</tr>
	<tr class="sfondocomm">
		<td align="left" colspan="3">
		Este procedimiento permite modificar la Denominación del área de referencia.
		</td>
	</tr>
	<tr>
		<td colspan=3 bgcolor='#3399CC'></td>
	</tr>
</table>

	<%
		if IsArray(AREA) then
			Response.Write "<input type='hidden' name='selettore' value='" & request("selettore") & "'>"
			Response.Write "<input type='hidden' name='ID_AREA' value='" & request("ID_AREA") & "'>"

			Response.Write "<table border='0' cellspacing=1 cellpadding=1 width=500>"
				Response.Write "<tr>"
					Response.Write "<td align=left class=tbltext1><b>Denominación</b></td>"
					Response.Write "<td>"
					Response.Write "<input type='text' class=MyTextBox name='VecchiaDenominazione' value='" & trim(cleanXsql(AREA(1,0))) & "' maxlength='60' size='63' readonly>"
					Response.Write "</td>"
					Response.Write "</tr>"
					Response.Write "<tr>"
					Response.Write "<td align=left class=tbltext1><b>Nueva Denominación*</b></td>"
					Response.Write "<td>"
					Response.Write "<input type='text' class=MyTextBox name='Denominazione' value='' maxlength='60' size='63' >"
					Response.Write "</td>"
				Response.Write "</tr>"
				Response.Write "<tr>"
					Response.Write "<td align=left class=tbltext1><b>Notas&nbsp;&nbsp;</b></td>"
					Response.Write "<td>"
						Response.Write "<textarea class=MyTextBox name='Nota' cols='60' rows='4'>" & trim(AREA(2,0)) & "</textarea>"
					Response.Write "</td>"
				Response.Write "</tr>"
				Response.Write "<tr>"
					Response.Write "<td align='left'>"
						Response.Write "<input type='button' value='Cerrar' OnClick='goToPage(""DettAree.asp"")' CLASS='My'>&nbsp;&nbsp;&nbsp;</td>"
						Response.Write "<td colspan='2' align='right'>"
						Response.Write "<input type='submit' name='Salva' value='Guardar' CLASS='My'>&nbsp;&nbsp;&nbsp;</td>"
			Response.Write "</td>"
			Response.Write "</tr>"
			Response.Write "<tr>"
				Response.Write "<td colspan=3 bgcolor='#3399CC'></td>"
			Response.Write "</tr>"
			Response.Write "</table>"
		end if
	%>
</form>
<!-- #include Virtual="/strutt_coda2.asp" -->
