<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #include Virtual="/strutt_testa2.asp" -->
<!-- #INCLUDE FILE="../Utils.asp" -->

<%
Set Conn = cc
'Conn.open strConn

	Sql = ""
	Sql = Sql & "SELECT "
	Sql = Sql & "CONOSCENZE.DENOMINAZIONE, VALIDAZIONE.ID_PERS_INS, VALIDAZIONE.DT_TMST, "
	Sql = Sql & "PERSONA.COGNOME, PERSONA.NOME, VALIDAZIONE.ID_VALID, VALIDAZIONE.FL_VALID, "
	Sql = Sql & "CONOSCENZE.ID_AREACONOSCENZA "
	Sql = Sql & "FROM "
	Sql = Sql & "CONOSCENZE, VALIDAZIONE, PERSONA "
	Sql = Sql & "WHERE "
	Sql = Sql & "VALIDAZIONE.ID_VALID = CONOSCENZE.ID_VALID "
	Sql = Sql & "AND "
	Sql = Sql & "PERSONA.ID_PERSONA (+) = VALIDAZIONE.ID_PERS_INS "
	Sql = Sql & "AND "
	Sql = Sql & "CONOSCENZE.ID_CONOSCENZA = " & request("ID_CONOSCENZA")
	Sql = Sql & "ORDER BY UPPER(CONOSCENZE.DENOMINAZIONE)"

	
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
	Set Rs = Conn.Execute (Sql)
	if not rs.eof then CONOSC = Rs.getrows()

	Sql = ""
	Sql = Sql & "SELECT ID_AREACONOSCENZA, DENOMINAZIONE "
	Sql = Sql & "FROM AREA_CONOSCENZA ORDER BY UPPER(DENOMINAZIONE)"
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
	Set Rs = Conn.Execute (Sql)
	if not rs.eof then AREE = Rs.getrows()

	SqlAss = ""
	SqlAss = SqlAss & " SELECT COMPETENZE.ID_COMPETENZA, COMPETENZE.DENOMINAZIONE "
	SqlAss = SqlAss & " FROM COMPETENZE, COMPET_CONOSC "
	SqlAss = SqlAss & " WHERE "
	SqlAss = SqlAss & " COMPETENZE.ID_COMPETENZA = COMPET_CONOSC.ID_COMPETENZA "
	SqlAss = SqlAss & " AND COMPET_CONOSC.ID_CONOSCENZA=" &  request("ID_CONOSCENZA")
	SqlAss = SqlAss & " ORDER BY UPPER(COMPETENZE.DENOMINAZIONE)"
	
'PL-SQL * T-SQL  
SQLASS = TransformPLSQLToTSQL (SQLASS) 
	Set Rs = Conn.Execute (SqlAss)
	if not rs.eof then ASSOCIAZIONE = Rs.getrows()

	SqlFP = ""
	SqlFP = SqlFP & " SELECT FIGUREPROFESSIONALI.ID_FIGPROF, FIGUREPROFESSIONALI.DENOMINAZIONE "
	SqlFP = SqlFP & " FROM CONOSCENZE, FIGUREPROFESSIONALI, COMPET_FP, COMPETENZE, COMPET_CONOSC "
	SqlFP = SqlFP & " WHERE ((COMPET_FP.ID_FIGPROF=FIGUREPROFESSIONALI.ID_FIGPROF) "
	SqlFP = SqlFP & " AND (COMPET_FP.ID_COMPETENZA=COMPETENZE.ID_COMPETENZA) "
	SqlFP = SqlFP & " AND (COMPET_CONOSC.ID_COMPETENZA=COMPETENZE.ID_COMPETENZA) "
	SqlFP = SqlFP & " AND (COMPET_CONOSC.ID_CONOSCENZA=CONOSCENZE.ID_CONOSCENZA) "
	SqlFP = SqlFP & " AND (CONOSCENZE.ID_CONOSCENZA= " & request("ID_CONOSCENZA") & " ))"
	SqlFP = SqlFP & " GROUP BY FIGUREPROFESSIONALI.ID_FIGPROF, FIGUREPROFESSIONALI.DENOMINAZIONE "
	SqlFP = SqlFP & " ORDER BY UPPER(FIGUREPROFESSIONALI.DENOMINAZIONE)"
	
'PL-SQL * T-SQL  
SQLFP = TransformPLSQLToTSQL (SQLFP) 
	Set Rs = Conn.Execute (SqlFP)
	if not rs.eof then FP_ASSOCIATE = Rs.getrows()

	SqlPers= ""
	SqlPers= SqlPers & "SELECT count(PERS_CONOSC.ID_PERSONA) MaxPersone FROM PERS_CONOSC WHERE ID_CONOSCENZA=" & request("ID_CONOSCENZA")
'PL-SQL * T-SQL  
SQLPERS = TransformPLSQLToTSQL (SQLPERS) 
	Set Rs = Conn.Execute (SqlPers)

if not rs.eof then PERSONE = Rs(0)
Set Rs = Nothing
Conn.Close
Set conn = Nothing

%>

	<script language="Javascript">
<!--
function del(ID_CONOSCENZA)
{
	a = confirm("Esta seguro de querer eliminar este conocimiento?")
	if (a)
	{
		goToPage("DelConoscenza.asp?ID_CONOSCENZA=" + ID_CONOSCENZA)
	}

}
//-->
</script>

<form name='Ver_Comportamento' Method=POST Action='SalvaVer_Conoscenza.asp'>
<input type="hidden" name="IdPers" value="<%=sIdPers%>">
<input type="hidden" name="IND_FASE" value="<%=IND_FASE%>">
<input type='hidden' name='VISMENU' value='NO'>


<CENTER>
<table border=0 cellspacing=0 cellpadding=0 width='500'>
	<tr>
		<td bgcolor='#3399CC' class='tbltext0' align=left bordercolor="#C2E0FF" width='199'>
			<b>DETALLE DEL CONOCIMIENTO</b></td>
		<td width="25" valign='bottom' background="<%=session("progetto")%>/images/sfondo_linguetta.gif" >
			<img border="0" src="<%=session("progetto")%>/images/tondo_linguetta.gif"></td>
		<td width="278" valign='bottom' background="<%=session("progetto")%>/images/sfondo_linguetta.gif"></td>
	</tr>
</table>
<table border=0 cellspacing=0 cellpadding=0 width='500'>
	<tr class='sfondocomm'>
		<td align="left" colspan="3" bgcolor='#C6DFFF'>
			<b>Conocimiento validado: </b> <%=request("DENOMINAZIONE")%>
		</td>
		<td class=sfondocomm>
		
			<%
		if int(0 & CONOSC(6,0)) = 0 then
			Response.Write "<a href=""Javascript:Show_Help('" & Session("Progetto") & "/HelpBP/validazione/Conoscenze/Ver_Conoscenza.htm')"">"
			Response.Write "<img src='" & Session("Progetto")& "/images/Help.gif' border='0'  align=right alt='Help'></a>"
		else
			Response.Write "<a href=""Javascript:Show_Help('" & Session("Progetto") & "/HelpBP/validazione/Conoscenze/Ver_Conoscenza1.htm')"">"
			Response.Write "<img src='" & Session("Progetto")& "/images/Help.gif' border='0'  align=right alt='Help'></a>"

		end if		
			%>
		
		</td>
	</tr>
	<tr>
		<td colspan=4 bgcolor='#3399CC'></td>
	</tr>
</table>

<%
	if IsArray(CONOSC) then
		Response.Write "<input type='hidden' name='ID_CONOSCENZA' value='" & request("ID_CONOSCENZA") & "'>"
		Response.Write "<input type='hidden' name='ID_VALID' value='" & CONOSC(5,0) & "'>"

		Response.Write "<table border=0 width='500' cellspacing=1 cellpadding=1>"
			Response.Write "<tr>"
				Response.Write "<td class='tbltext1' align=LEFT><b>Denominación&nbsp;&nbsp;</B></td>"
				Response.Write "<td>"
					Response.Write "<input type='text'  class='MyTextBox' name='Denominazione' value='" & trim(request("DENOMINAZIONE")) & "' size=60 >"
				Response.Write "</td>"
			Response.Write "</tr>"
			Response.Write "<tr>"
				Response.Write "<td class='tbltext1' align=LEFT><b>Area de Conocimiento&nbsp;&nbsp;</B></td>"
				Response.Write "<td>"

					if IsArray(AREE) then
						Response.Write "<select class='MY' name='ID_AREACONOSCENZA'>"
						for J = lbound(AREE,2) to Ubound(AREE,2)
							Response.Write "<option value='" & AREE(0,J) & "' "
							if int(0 & AREE(0,J)) = Int(0 & CONOSC(7,0)) then Response.Write "selected"
							Response.Write " >" & AREE(1,J) & "</option>"
						next
						Response.Write "</select>"
					end if

				Response.Write "</td>"
			Response.Write "</tr>"
			Response.Write "<tr>"
				Response.Write "<td  class='tbltext1' align=LEFT><b>Asociación efectuada por&nbsp;&nbsp;</B></td>"
				Response.Write "<td class='tbltext1'>" & CONOSC(3,0) & " " & CONOSC(4,0) & "</td>"
			Response.Write "</tr>"
			Response.Write "<tr>"
				Response.Write "<td class='tbltext1' align=LEFT><b>Ultima operación efectuada el&nbsp;&nbsp;</B></td>"
				Response.Write "<td class='tbltext1'>" & CONOSC(2,0) & "</td>"
			Response.Write "</tr>"
			Response.Write "<tr>"
				Response.Write "<td  align=LEFT class='tbltext1'><b>Estado de Elaboración&nbsp;&nbsp;</B></td>"
				Response.Write "<td class='tbltext1'><small>"
				if int(0 & CONOSC(6,0)) = 0 then
					Response.Write "<font color=red>No Validado</font>"
				else
					Response.Write "Validado"
				end if
				Response.Write "</small></td>"
			Response.Write "</tr>"
			Response.Write "<tr>"
				Response.Write "<td align='left'>"
					Response.Write "<input type='button' value='Cancelar' OnClick='goToPage(""RicercaConoscenze.asp"")' CLASS='My'>&nbsp;&nbsp;&nbsp;</td>"
					if int(0 & CONOSC(6,0)) = 0 then
						Response.Write "<td align='RIGHT'><input type='submit' name='SalvaValida' value='Guardar y Validar' CLASS='My'>&nbsp;&nbsp;&nbsp;"
					else
						Response.Write "<td align='RIGHT'><input type='submit' name='Salva' value='Guardar' CLASS='My'>&nbsp;&nbsp;&nbsp;"
					end if
					if clng(PERSONE) = 0 THEN Response.Write "<input type='button' name='Elimina' value='Eliminar' CLASS='My' OnClick='del(" & request("ID_CONOSCENZA") & ")'>&nbsp;&nbsp;&nbsp;"
				Response.Write "</td>"
		      Response.Write "</tr>"
		        Response.Write "<tr>"
				 Response.Write "<td colspan=3 bgcolor='#3399CC'></td>"
	        Response.Write "</tr>"
		    Response.Write "</table>"
	end if

	Response.Write "<br>"
	Response.Write "<table border=0 width='500' CELLPADDING=1 cellspacing=1>"
	Response.Write "<tr class=sfondocomm><td align=left colspan=2>"
	Response.Write "<b>Postulantes que poseen este conocimiento: " & PERSONE & "</b>"
    Response.Write "</td></tr>"
	Response.Write "</table>"
	Response.Write "<br>"

	if IsArray(ASSOCIAZIONE) then


	Response.Write "<br>"
		Response.Write " <table border=0 width='500' CELLPADDING=1 cellspacing=1>"
		Response.Write " <tr class='sfondocomm'>"
		Response.Write " <td align='left'><b>"
		Response.Write " Listado de Compentencias a las que esta asociado"
		Response.Write " </b></td>"
		Response.Write " </tr>"


		for J = lbound(ASSOCIAZIONE,2) to Ubound(ASSOCIAZIONE,2)
			Response.Write "<tr>"
				Response.Write "<td class='tbltext1'>" & ASSOCIAZIONE(1,J) & "</td>"
			Response.Write "</tr>"
		next

		Response.Write "</table>"
	end if

	if IsArray(FP_ASSOCIATE) then

	Response.Write "<br>"

		Response.Write " <table border=0 width='500' CELLPADDING=1 cellspacing=1>"
		Response.Write " <tr class='sfondocomm'>"
		Response.Write " <td align='left'><b>"
		Response.Write " Listado de Figuras Profesionales a las que esta asociado"
		Response.Write " </b></td>"
		Response.Write " </tr>"


		for J = lbound(FP_ASSOCIATE,2) to Ubound(FP_ASSOCIATE,2)
			Response.Write "<tr>"
				Response.Write "<td class='tbltext1'>" & FP_ASSOCIATE(1,J) & "</td>"
			Response.Write "</tr>"
		next

		Response.Write "</table>"
	end if
%>
</form>
<!-- #include Virtual="/strutt_coda2.asp" -->
