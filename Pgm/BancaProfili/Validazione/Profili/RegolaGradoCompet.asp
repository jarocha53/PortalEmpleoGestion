<!-- #INCLUDE FILE="../Utils.asp" -->
<TITLE>GRADO DI POSSESSO DELLA COMPETENZA</TITLE>
<LINK REL=STYLESHEET TYPE="text/css" HREF='<%=session("progetto")%>/fogliostile.css'>
<body class=sfondocentro topmargin="10" leftmargin="0">

<form name='GradoCompet' Action='SalvaRegolaGradoCompet.asp' Method='POST'>
<input type='hidden' name='ID_FIGPROF' value='<%= request("ID_FIGPROF")%>'>
<input type='hidden' name='DENOMINAZIONE' value='<%= request("DENOMINAZIONE")%>'>
<input type='hidden' name='ID_VALID' value='<%= request("ID_VALID")%>'>
<input type='hidden' name='ID_COMPETENZA' value='<%= request("ID_COMPETENZA")%>'>
<input type="hidden" name="IdPers" value="<%=sIdPers%>">
<input type="hidden" name="IND_FASE" value="<%=IND_FASE%>">


	<CENTER>
	<table border=0 width='500' CELLPADDING=0 cellspacing=0>
	<tr>
		<td bgcolor='#3399CC' class="tbltext0" align=left width='300'>
		<B>GRADO DI POSSESSO DELLA COMPETENZA</B>
		</td>
		<td width="25" valign='bottom' background="<%=session("progetto")%>/images/sfondo_linguetta.gif" >
		<img border="0" src="<%=session("progetto")%>/images/tondo_linguetta.gif"></td>
		<td width="278" valign='bottom' background="<%=session("progetto")%>/images/sfondo_linguetta.gif"></td>
	</tr>
	<tr class='sfondocomm'>
		<td align="left" colspan="3" bgcolor='#C6DFFF'>
		Scegliere in che misura bisogna possedere la <b>Competenza</b> in relazione alla Figura Professionale in esame.
		</td>
	</tr>
	<tr>
		<td colspan=3 bgcolor='#3399CC'></td>
	</tr>
</table>
<BR>


	<table border=0 width='500' cellspacing=1 cellpadding=1>
		<tr>
			<td class=tbltext1 align=LEFT WIDTH=110><b>Grado di possesso&nbsp;</b></td>
			<td class=tbltext1>&nbsp;&nbsp;1&nbsp;&nbsp;-&nbsp;&nbsp;2&nbsp;&nbsp;-&nbsp;&nbsp;3&nbsp;-&nbsp;4&nbsp;-&nbsp;5<br>
				<input type='radio' name='Grado' value='1'>
				<input type='radio' name='Grado' value='2' checked>
				<input type='radio' name='Grado' value='3'>
				<input type='radio' name='Grado' value='4'>
				<input type='radio' name='Grado' value='5'>
			</td>
		</tr>
		<tr>
			<td colspan=2 align=right>
				<input type='Submit' value='Salva' CLASS='My'>
			</td>
		</tr>
		<tr>
		<td colspan=3 bgcolor='#3399CC'></td>
	</tr>
	</table>
</center>
</form>
