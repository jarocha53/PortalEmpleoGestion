<!-- #INCLUDE FILE="../Utils.asp" -->
<%
Set Conn = server.CreateObject ("ADODB.Connection")
Conn.open strConn
OraDate = ConvDateToDB(Now())

Conn.BeginTrans

if request("Valida") <> "" then
	LstCampi = "ID_PERS_LAV,ID_PERS_CERT,FL_VALID,DT_TMST"

	LstVal = ""
	LstVal = LstVal & IDP & "|"
	LstVal = LstVal & IDP & "|"
	LstVal = LstVal & "1" & "|"
	LstVal = LstVal & OraDate

	Sql = ""
	Sql = QryUpd("VALIDAZIONE",LstCampi,LstVal)
	Sql = Sql & "WHERE "
	Sql = Sql & "VALIDAZIONE.ID_VALID = " & request("ID_VALID")

	Conn.Execute Sql

	LstCampi = "DENOMINAZIONE, DESCRIZIONE, COD_EJ, ID_AREAPROF"

	LstVal = ""
	LstVal = LstVal & "'" & CleanXsql(request("NewDenominazione")) & "'" & "|"
	LstVal = LstVal & "'" & CleanXsql(request("DESCRIZIONE")) & "'" & "|"
	LstVal = LstVal & "'" & CleanXsql(request("COD_ISTAT")) & "'" & "|"
	LstVal = LstVal & request("ID_AREAPROF")

	Sql = ""
	Sql = QryUpd("FIGUREPROFESSIONALI",LstCampi,LstVal)
	Sql = Sql & "WHERE "
	Sql = Sql & "FIGUREPROFESSIONALI.ID_FIGPROF = " & request("ID_FIGPROF")

	Conn.Execute Sql
	'INVIO COMUNICAZIONE CANDIDATI , PASSO I PARAMETRI DI AREA E PROFILO
	ComCandidatiProfili request("ID_AREAPROF"),request("ID_FIGPROF")
	
	LogTrace ucase(mid(session("progetto"),2)),IDP, "P",IDP,"FIGUREPROFESSIONALI", "" &IDP &  "", "MOD"
end if

Conn.CommitTrans
Conn.Close
Set conn = Nothing

QS = ""
QS = QS & "ID_FIGPROF=" & request("ID_FIGPROF") & "&"
QS = QS & "DENOMINAZIONE=" & request("DENOMINAZIONE")
%>

<script>
<!--
  goToPage("RiepilogoProfilo.asp?<%=QS%>")
//-->

</script>

