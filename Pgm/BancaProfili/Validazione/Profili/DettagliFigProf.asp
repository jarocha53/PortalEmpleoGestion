<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #include Virtual="/strutt_testa2.asp" -->
<!-- #INCLUDE FILE="../Utils.asp" -->
<%
Set Conn = server.CreateObject ("ADODB.Connection")
Conn.open strConn

	Sql = ""
	Sql = Sql & "SELECT CODICI_ISTAT.MANSIONE "
	Sql = Sql & "FROM "
	Sql = Sql & "CODICI_ISTAT, FIGUREPROFESSIONALI "
	Sql = Sql & "WHERE "
	Sql = Sql & "CODICI_ISTAT.CODICE = FIGUREPROFESSIONALI.COD_EJ "
	Sql = Sql & "AND "
	Sql = Sql & "FIGUREPROFESSIONALI.ID_FIGPROF = " & request("ID_FIGPROF")
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
	Set Rs = Conn.Execute (Sql)
	if not rs.eof then DESC_ISTAT = Rs(0)

	Sql = ""
	Sql = Sql & "SELECT "
	Sql = Sql & "FIGUREPROFESSIONALI.DESCRIZIONE, VALIDAZIONE.ID_PERS_INS, VALIDAZIONE.DT_TMST, "
	Sql = Sql & "PERSONA.COGNOME, PERSONA.NOME, VALIDAZIONE.ID_VALID, AREE_PROFESSIONALI.DENOMINAZIONE areaProf, "
	Sql = Sql & "AREE_PROFESSIONALI.ID_AREAPROF, VALIDAZIONE.FL_VALID, FIGUREPROFESSIONALI.COD_EJ COD_ISTAT, FIGUREPROFESSIONALI.BREVE_DESC "
	Sql = Sql & "FROM "
	Sql = Sql & "FIGUREPROFESSIONALI, VALIDAZIONE, PERSONA, AREE_PROFESSIONALI "
	Sql = Sql & "WHERE "
	Sql = Sql & "FIGUREPROFESSIONALI.ID_AREAPROF = AREE_PROFESSIONALI.ID_AREAPROF "
	Sql = Sql & "AND "
	Sql = Sql & "VALIDAZIONE.ID_VALID = FIGUREPROFESSIONALI.ID_VALID "
	Sql = Sql & "AND "
	Sql = Sql & "PERSONA.ID_PERSONA (+) = VALIDAZIONE.ID_PERS_INS "
	Sql = Sql & "AND "
	Sql = Sql & "FIGUREPROFESSIONALI.ID_FIGPROF = " & request("ID_FIGPROF")

'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
Set Rs = Conn.Execute (Sql)
if not rs.eof then FIGPROF = Rs.getrows()

Sql = ""
Sql = Sql & "SELECT ID_AREAPROF, DENOMINAZIONE "
Sql = Sql & "FROM AREE_PROFESSIONALI ORDER BY UPPER(DENOMINAZIONE)"

'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
Set Rs = Conn.Execute (Sql)
if not rs.eof then AREE = Rs.getrows()

Set Rs = Nothing
Conn.Close
Set conn = Nothing

%>

	<script language='Javascript'>
	<!--

function ApriCodIstat(Obj)
{
	CriterioRicerca= Obj.CriterioRicerca.value
	//alert (CriterioRicerca)
	URL = "CodiceIstat.asp?VISMENU=NO" + "&RICERCA="+ CriterioRicerca

	opt = "address=no,status=no,width=650,height=240,top=150,left=15"

	window.open (URL,"",opt)
}
	//-->
	</script>


<form name='DettagliFigProf' Method=POST Action='SalvaDettagliFigProf.asp'>
	<input type="hidden" name="IdPers" value="<%=sIdPers%>">
	<input type="hidden" name="IND_FASE" value="<%=IND_FASE%>">
	<input type="hidden" name="VISMENU" value="NO">

<CENTER>

<table border=0 cellspacing=0 cellpadding=0 width='500'>

	<tr>
		<td bgcolor='#3399CC' align=left class='tbltext0' WIDTH=300>
			<b>DETALLE DE LA FIGURA PROFESIONAL</b>
		</td>
		<td width="25" valign='bottom' background="<%=session("progetto")%>/images/sfondo_linguetta.gif" >
			<img border="0" src="<%=session("progetto")%>/images/tondo_linguetta.gif"></td>
		<td width="278" valign='bottom' background="<%=session("progetto")%>/images/sfondo_linguetta.gif" >&nbsp;</td>
	</tr>
</table>

<table border=0 cellspacing=0 cellpadding=0 width='500'>
	<tr class='sfondocomm'>
		<td>
			En esta página podrá visualizar el detalle de la <b>Figura Profesional</b> seleccionada.
		</td>
		<td>
		<%
		if clng(0 & FIGPROF(8,0)) = 1 then
			Response.Write "<a href=""Javascript:Show_Help('" & Session("Progetto") & "/HelpBP/validazione/Profili/DettagliFigProf1.htm')"">"
			Response.Write "<img src='" & Session("Progetto")& "/images/Help.gif' border='0'  align=right alt='Help'></a>"

		else
			Response.Write "<a href=""Javascript:Show_Help('" & Session("Progetto") & "/HelpBP/validazione/Profili/DettagliFigProf.htm')"">"
			Response.Write "<img src='" & Session("Progetto")& "/images/Help.gif' border='0'  align=right alt='Help'></a>"
		end if
		%>

		</td>
	</tr>

	<tr>
		<td colspan=3 bgcolor='#3399CC'></td>
	</tr>
<%
	if IsArray(FIGPROF) then
		Response.Write "<input type='hidden' name='ID_FIGPROF' value='" & request("ID_FIGPROF") & "'>"
		Response.Write "<input type='hidden' name='DENOMINAZIONE' value='" & request("DENOMINAZIONE") & "'>"
		Response.Write "<input type='hidden' name='ID_VALID' value='" & FIGPROF(5,0) & "'>"
		Response.Write "	</td>"
		Response.Write "</tr>"
		Response.write "<tr>"
		Response.Write "<td colspan=3>"
			Response.Write "<table border=0 cellspacing=1 cellpadding=1 width=500>"
			Response.Write "<tr>"
				Response.Write "<td class='tbltext1' align=LEFT><b>Figura Profesional&nbsp;&nbsp;</B></td>"
				Response.Write "<td>"
					Response.write "<input type='text'class='MyTextBox' Name='NewDenominazione' value='" & trim(request("DENOMINAZIONE")) & "' size='60'>"
				Response.Write "</td>"
			Response.Write "</tr>"

			Response.Write "<tr>"
				Response.Write "<td class='tbltext1' align=LEFT><b>Descripción&nbsp;&nbsp;</B></td>"
				Response.Write "<td>"
				Response.write "<TEXTAREA rows=2  class='MyTextBox' cols=55 name='DESCRIZIONE'>" & FIGPROF(0,0) & "</TEXTAREA>"
				Response.Write "</td>"
			Response.Write "</tr>"
			Response.Write "<tr>"
				Response.Write "<td class='tbltext1' align=LEFT><b>Breve Descripción&nbsp;&nbsp;</B></td>"
				Response.Write "<td>"
				Response.write "<TEXTAREA rows=2  class='MyTextBox' cols=55 name='BREVEDESCRIZIONE'>" & FIGPROF(10,0) & "</TEXTAREA>"
				Response.Write "</td>"
			Response.Write "</tr>"
			Response.Write "<tr>"
				Response.Write "<td class='tbltext1' align=LEFT><b>Area Profesional&nbsp;&nbsp;</B></td>"
				Response.Write "<td>"
				if isArray(AREE) then
				Response.Write "<select name='ID_AREAPROF' class='MY'>"
				for J = lbound(AREE,2) to Ubound(AREE,2)
					Response.Write "<option value='" & AREE(0,J) & "' "
					if int(0 & AREE(0,J)) = int(0 & FIGPROF(7,0)) then Response.Write "selected"
					Response.Write " >" & AREE(1,J) & "</option>"
				next
				Response.Write "</select>"
				end if
				Response.Write "</td>"
			Response.Write "</tr>"
			Response.Write "<td colspan='2' bgcolor='#3399CC'></td>"
				Response.Write "</tr>"
				Response.Write "<tr class='textblack'>"
				Response.Write "<td colspan=2 >"
						Response.Write "<small>Ingresando a continuación parte de la denominación de la <b>Figura Profesional</b>"
						Response.Write " podrá efectuar una busqueda dentro de la clasificación <b>ISTAT</b>.<br>"
						Response.Write "Para acceder al diccionario de las profesiones es suficiente"
						Response.Write "con presionar BUSCAR sin ingresar nada en la casilla de texto."
						Response.Write "</small></td>"
				Response.Write "</tr>"
				Response.Write "<tr>"
				Response.Write "<td class=tbltext1  align=LEFT><B>Texto de Busqueda</B></td>"
				Response.Write "<td><input type='text' class='MyTextBox' size=50  maxlength=50 name='CriterioRicerca'></td>"
				Response.Write "</tr>"

			Response.Write "<tr>"
				Response.Write "<td class='tbltext1' align=LEFT><b>CODIGO ISTAT&nbsp;&nbsp;</B></td>"
				Response.Write "<td>"
				Response.write "<input type='text' class='MyTextBox' Name='COD_ISTAT' readonly OnClick='ApriCodIstat(this.form)' value='" & FIGPROF(9,0) & "' size='20'>"
					Response.write "<input type='button' value='Buscar' CLASS='My' OnClick='ApriCodIstat(this.form)'>"
				Response.write "</td>"
			Response.Write "</tr>"

			Response.Write "<tr>"
				Response.Write "<td class='tbltext1' align=LEFT><b>Calificación ISTAT&nbsp;&nbsp;</B></td>"
				Response.Write "<td class=tbltext1><input type='text' Name='DescrMansione' size=60 class='MyTextBox' Value='" & left(trim(DESC_ISTAT),50) & "'></td>"
			Response.Write "</tr>"

			Response.Write "<tr>"
				Response.Write "<td class='tbltext1' align=LEFT><b>Ingresado por&nbsp;&nbsp;</B></td>"
				Response.Write "<td class='tbltext1'><small>" & FIGPROF(3,0) & " " & FIGPROF(4,0) & "</small></td>"
			Response.Write "</tr>"
			Response.Write "<tr>"
				Response.Write "<td class='tbltext1' align=LEFT><b>Ultima Modificación: &nbsp;&nbsp;</B></td>"
				Response.Write "<td valign='top' class='tbltext1'>" & FIGPROF(2,0) & "</td>"
			Response.Write "</tr>"
			Response.Write "<tr>"
				Response.Write "<td class='tbltext1' align='left'>"
					Response.Write "<input type='button' value='Cancelar' OnClick='goToPage(""RiepilogoProfilo.asp?ID_FIGPROF=" & request("ID_FIGPROF") & "&DENOMINAZIONE=" & request("DENOMINAZIONE") & """)' CLASS='My'>&nbsp;&nbsp;&nbsp;</td>"
					if clng(0 & FIGPROF(8,0)) = 1 then
						Response.Write "<td class='tbltext1' align='right'><input type='submit' name='Valida' value='Guardar' CLASS='My'>&nbsp;&nbsp;&nbsp;"
					else
						Response.Write "<td class='tbltext1' align='right'><input type='submit' name='Valida' value='Guardar y Validar' CLASS='My'>&nbsp;&nbsp;&nbsp;"
					end if
				Response.Write "</td>"
			Response.Write "</tr>"
			Response.Write "</table>"
		Response.Write "</td>"
		Response.Write "</tr>"
		%>
			<tr>
				<td colspan=3 bgcolor='#3399CC'></td>
			</tr>
		<%
		Response.Write "</table>"
	end if
%>
</form>
</CENTER>
<!-- #include Virtual="/strutt_coda2.asp" -->

