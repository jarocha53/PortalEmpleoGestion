<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #include Virtual="/strutt_testa2.asp" -->
<!-- #INCLUDE FILE="../Utils.asp" -->

<center>
<form name='ListaItem' Method=POST Action='ReportFigProf.asp'>
<input type="hidden" name="IdPers" value="<%=sIdPers%>">
<input type="hidden" name="IND_FASE" value="<%=IND_FASE%>">


<table border=0 width='500' CELLPADDING=0 cellspacing=0>
	<tr>
		<td class=sfondomenu  class='tbltext1' align=left >
		<span class="tbltext0"><B>&nbsp;FIGURA PROFESIONAL</SPAN></td>
		<td width="25" valign='bottom' background="<%=session("progetto")%>/Images/sfondo_linguetta.gif" >
		<img border="0" src="<%=session("progetto")%>/Images/tondo_linguetta.gif"></td>
		<td width="180" valign='middle' align="right" class=tbltext1 background="<%=session("progetto")%>/Images/sfondo_linguetta.gif" >
		</td>
	</tr>
</table>

<table border=0 width='500' CELLPADDING=0 cellspacing=0>
	<tr>
		<td align="left"  class="sfondocomm">
			Lista de <b>Figuras Profesionales</b> que verifican los criterios de b&uacutesqueda.
		</td>
		<td class=sfondocomm><a href="Javascript:Show_Help('<%=Session("Progetto")%>/HelpBP/Validazione/Profili/ResultProfili.htm')">
		 <img src="<%=Session("Progetto")%>/images/Help.gif" border='0'  align=right alt='Help'>
		</a>
		</td>
	</tr>
	<tr>
		<td bgcolor='#3399CC' colspan='2'></td>
	</tr>
</table>
<br>

<%
if request("ID_AREA")<>"" or Request("testo")<>"" or Request("ID_PROJ") <> "" or Request("StatoLav") <> "" or Request("Aziendali") <> "" then

	if request("lDwn") = "" Or Int(request("lDwn")) < 0 then
		lDwn = 0
	else
		lDwn = request("lDwn")
	end if

	W=""

	Sql = ""
	Sql = Sql & "SELECT DISTINCT"
	Sql = Sql & "	FIGUREPROFESSIONALI.ID_FIGPROF,"
	Sql = Sql & "	UPPER(FIGUREPROFESSIONALI.DENOMINAZIONE),  "
	Sql = Sql & "	FIGUREPROFESSIONALI.ID_VALID, "
	Sql = Sql & "	VALIDAZIONE.FL_STATOLAV, "
	Sql = Sql & "	VALIDAZIONE.FL_VALID "
	Sql = Sql & "FROM "
	Sql = Sql & "	FIGUREPROFESSIONALI, VALIDAZIONE "

	if request("ID_PROJ") <> "" then
		Sql = Sql & " ,FIGPROF_PRJ "
	end if

	if  Request("Aziendali") ="SI" then
		Sql = Sql & " ,IMPRESA_FP "
	end if

	Sql = Sql & "WHERE "
	W = W & " FIGUREPROFESSIONALI.IND_MAPPA='S'  AND "
	W = W & " FIGUREPROFESSIONALI.ID_VALID = VALIDAZIONE.ID_VALID "

	if request("ID_AREA") <> "" then
		W = W & " AND "
		W = W & " FIGUREPROFESSIONALI.ID_AREAPROF = " & request("ID_AREA")
	end if

	W = W & " AND UPPER(FIGUREPROFESSIONALI.DENOMINAZIONE) LIKE '%" & trim(UCASE(CleanXsql(REQUEST("testo")))) & "%'"


	if request("ID_PROJ") <> "" then
		W = W & " AND "
		W = W & " trim(UPPER(FIGPROF_PRJ.COD_PRJ)) = '" & trim(UCASE(request("ID_PROJ"))) & "'"
		W = W & " AND "
		W = W & " FIGUREPROFESSIONALI.ID_FIGPROF = FIGPROF_PRJ.ID_FIGPROF "
	end if


	Select case request("StatoLav")
		Case "InLavorazione"
			W = W & " AND "
			W = W & " VALIDAZIONE.FL_STATOLAV = 0"
		Case "DaValidare"
			W = W & " AND "
			W = W & "	VALIDAZIONE.FL_STATOLAV = 1"
			W = W & " AND "
			W = W & "	VALIDAZIONE.FL_VALID = 0"
		Case "Validati"
			W = W & " AND "
			W = W & "	VALIDAZIONE.FL_VALID = 1"
	End Select

	If request("Aziendali") ="SI" then
		W = W & " AND "
		W = W & " FIGUREPROFESSIONALI.ID_FIGPROF = IMPRESA_FP.ID_FIGPROF"
	end if


	Sql = Sql & W & " ORDER BY VALIDAZIONE.FL_STATOLAV desc, VALIDAZIONE.FL_VALID,"
	Sql = Sql & " UPPER(FIGUREPROFESSIONALI.DENOMINAZIONE) "

	SqlCount = ""
	SqlCount = SqlCount & "SELECT count(FIGUREPROFESSIONALI.ID_FIGPROF) maxresult "
	SqlCount = SqlCount & "FROM FIGUREPROFESSIONALI, VALIDAZIONE "

	if Request("Aziendali") ="SI" then
		SqlCount = SqlCount & " ,IMPRESA_FP "
	end if

	if request("ID_PROJ") <> "" then
		SqlCount = SqlCount & " ,FIGPROF_PRJ "
	end if

	SqlCount = SqlCount & "WHERE "
	SqlCount = SqlCount & W

    Set Conn = CC
'	Set Conn = server.CreateObject ("ADODB.Connection")
	Set Rs = server.CreateObject ("ADODB.Recordset")
	'Conn.open strConn

'Response.Write sql
'Response.End
        'PL-SQL * T-SQL
        Sql = TransformPLSQLToTSQL (Sql)
		Rs.Open Sql,Conn,3,3,1

		if not rs.eof then
			Rs.Move (lDwn)
			RESULT = Rs.getrows(20)
'PL-SQL * T-SQL
SQLCOUNT = TransformPLSQLToTSQL (SQLCOUNT)
			RsCount = Conn.Execute(SqlCount)
			RC = RsCount(0)
			Set RsCount = Nothing
			Set Rs=Nothing
		else

			Response.Write "<table border=0 cellspacing=1 width=500>"
			Response.Write "<tr><td align=center class='tbltext3'>"
			Response.Write "No se encontraron elementos que satisfagan los criterios especificados"
			Response.Write "</td></tr>"
			Response.Write "<tr>"
			Response.Write "<td>"
			Response.Write "<input type='button' value='Volver' OnClick='goToPage(""RicercaProfili.asp"")' Class='My'>"
			Response.Write "</td></tr>"
			Response.Write " <tr>"
				 Response.Write " <td colspan=3 bgcolor='#3399CC'></td>"
			Response.Write "</tr>"
			Response.Write "</table>"
		end if
	Conn.Close
	Set conn = Nothing
end if

%>

<script language='Javascript'>
	<!--
		function mandaAvanti(lDwn)
		{
		A = lDwn + 20

		URL =  "ResultProfili.asp?"
		URL += "lDwn=" + A + "&"
		URL += 'testo=<%=CleanXsql(request("testo"))%>&'
		URL += 'ID_AREA=<%=request("ID_AREA")%>&'
		URL += 'ID_FIGPROF=<%=request("ID_FIGPROF")%>&'
		URL += 'TitoloArea=<%=request("TitoloArea")%>&'
		URL += 'TitoloProgetto=<%=request("TitoloProgetto")%>&'
		URL += 'ID_PROJ=<%=request("ID_PROJ")%>&'
		URL += 'Aziendali=<%=request("Aziendali")%>&'
		URL += 'StatoLav=<%=request("StatoLav")%>&'
		URL += '<%=Puls%>=<%=Puls%>'

		goToPage(URL)

		}

		function mandaIndietro(lDwn)
		{
		A = lDwn - 20

		URL =  "ResultProfili.asp?"
		URL += "lDwn=" + A + "&"
		URL += 'testo=<%=CleanXsql(request("testo"))%>&'
		URL += 'ID_AREA=<%=request("ID_AREA")%>&'
		URL += 'ID_FIGPROF=<%=request("ID_FIGPROF")%>&'
		URL += 'TitoloArea=<%=request("TitoloArea")%>&'
		URL += 'TitoloProgetto=<%=request("TitoloProgetto")%>&'
		URL += 'ID_PROJ=<%=request("ID_PROJ")%>&'
		URL += 'Aziendali=<%=request("Aziendali")%>&'
		URL += 'StatoLav=<%=request("StatoLav")%>&'
		URL += '<%=Puls%>=<%=Puls%>'

		goToPage(URL)
	}

	function FocusFigProf(ID_FIGPROF)
	{
		URL = "FocusFigProf.asp?"
		URL = URL + "ID_FIGPROF=" + ID_FIGPROF + "&VISMENU=NO"

		opt = "address=no,status=no,width=610,height=550,top=150,left=35,scrollbars=yes"

		window.open (URL,"",opt)
	}
//-->
</script>

<%

	if request("lDwn") = "" Or Int(request("lDwn")) < 0 then
		lDwn = 0
	else
		lDwn = request("lDwn")
	end if

	if IsArray(RESULT) then

		Pag= cint(RC) mod 20
		if Pag> 0 then
			Pagine= 0 + int(cLng(RC)/20)+1
		else
			Pagine = 0 + int(cLng(RC)/20)
		end if

		Response.Write "<input type='hidden' name='limite' value='" & Ubound(RESULT,2) & "'>"
		Response.Write "<table width='500' border=0 cellspacing=1 cellpadding=1>"
		Response.Write "<tr class=sfondocomm>"
			    Response.Write "<td colspan='4' align='right'>"
			    Response.write "<b>P&aacute;gina: </b>" & Pagine & "&nbsp;<b>Nro. de Elementos:</b> " & RC & ""
			    Response.Write "</td>"
		  Response.Write "</tr>"
		  Response.Write "<tr class='sfondocomm'>"
				Response.write "<td ' align='left' colspan=4>"
				Response.Write "Criterios Especificados:&nbsp;&nbsp;&nbsp;"

		        if request("Testo") <> "" then 	Response.write "<br>Testo: <b>" & request("testo") & "</b>"
				if request("ID_AREA") <> "" then Response.write "<br>Area: <b>" & request("TitoloArea") & "</b>"
				If request("ID_PROJ") <> "" then Response.write "<br>Progetto: <b>" & request("TitoloProgetto") & "</b>"
				If request("StatoLav") <> "" then Response.write "<br>Stato Lavorazione: <b>" & request("StatoLav") & "</b>"
				If request("Aziendali") = "SI" then Response.write "<br>Aziendali: <b>" & request("Aziendali") & "</b>"
			       Response.Write "</td>"
			Response.Write "</tr>"




		      Response.Write "<tr class='sfondocomm'>"
   			    Response.Write "<td align='center'>&nbsp;</td>"
			    Response.Write "<td align='left' width=425><b>Denominación<b></td>"
			    Response.Write "<td align='center' width=105><b>Estado<b></td>"
			    Response.Write "<td width=100 align=center><b>Opciones</b></td>"
			    Response.Write "</tr>"

			for I = lbound(RESULT,2) to Ubound(RESULT,2)
				Response.Write "<input type='hidden' name='ID_FIGPROF' value='" & RESULT(0,I) & "'>"

				if (i mod 2)= 0 then
					sfondo="class='tblsfondo'"
				else
					sfondo="class='tbltext1'"
				end if

				Response.Write "<tr " & sfondo & ">"
				Response.Write "	<td class='tbltext1' align='center'><b><a href='javascript: FocusFigProf(" & RESULT(0,I) & ")' title='Visualiza el detalle de la Figura Profesional'>" & I+ lDwn + 1 & "&nbsp;</b></td>"
				Response.Write "<td ALIGN='LEFT' class='tbltext1'><SMALL>" & RESULT(1,I) & "</SMALL></a></td>"

				validato = "no"


				if clng(RESULT(4,I)) = 1 then
						Response.Write "<td ALIGN='center'><font face=verdana color='#008000' size=1><B>&nbsp;Validado</b></font></td>"
				end if


				If clng(RESULT(4,I)) <> 1 then
					if clng(RESULT(3,I)) = 1 then
							Response.Write "<td ALIGN='center' ><font face=verdana color='#FF0000' size=1><small><B>A validad</b></small></font></td>"
					else
					Response.Write "<td ALIGN='center'><font face=verdana color='#008000' size='1'><small><B>En elaboraci&oacute;n</b></small></font></td>"
					end if
				end if
					Response.Write "<td align='right'>"
				Response.Write "<input type='button' value='Detalle' OnClick='goToPage(""RiepilogoProfilo.asp?ID_FIGPROF=" & RESULT(0,I) & "&DENOMINAZIONE=" & replace(RESULT(1,I),"'","`") & """)'  CLASS='My'>"
				Response.Write "</td>"
				Response.Write "</tr>"
		next
		Response.Write "</table>"

	Response.Write "<table border=0 cellspacing=1  cellpadding=1 width=500 align='center'>"
		Response.Write "<tr>"
			Response.Write "<td colspan='3'>&nbsp;</td>"
		Response.Write "</tr>"
		Response.Write "<tr>"
			Response.Write "<td align=LEFT  class='tbltext1'><input type='button' value='Cancelar' OnClick='goToPage(""RicercaProfili.asp"")' Class='My'></td>"
			Response.Write "<td align=center class='tbltext1'>"
				if lDwn > 1 then
					Response.Write "<input type='button' value='Volver' OnClick='mandaIndietro(" & lDwn & ")' Class='My' >"
				else
					Response.Write "<input type='button' value='Volver' OnClick='' Class='My' disabled>"
				end if
			Response.Write "<b>&nbsp;Pag: " & cLng((lDwn + 20)/20) & "&nbsp;&nbsp;de " & Pagine & "&nbsp;</b>"
				if I + 1 +lDwn < cint(RC) then
					Response.Write "<input type='button' value='Avanzar' OnClick='mandaAvanti(" & lDwn & ")' Class='My' >"
				else
					Response.Write "<input type='button' value='Avanzar' OnClick='' Class='My' disabled >"
				end if
			 Response.Write "</td>"
		 Response.Write "</tr>"
	Response.Write "</table>"
	end if
%>
</form>
</center>
<!-- #include Virtual="/strutt_coda2.asp" -->
