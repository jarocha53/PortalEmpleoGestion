<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->

<!-- #INCLUDE FILE="../Utils.asp" -->
<%
function ID_VALID

Dim LstVal, LstCampi, Sql

	'Creazione di un nuovo record di validazione
	LstCampi = "ID_PERS_INS,ID_PERS_LAV,FL_VALID,DT_TMST"

	LstVal = ""
	LstVal = LstVal & IDP & "|"
	LstVal = LstVal & IDP & "|"
	LstVal = LstVal & "0" & "|"
	LstVal = LstVal & OraDate

	Sql = ""
	Sql = QryIns("VALIDAZIONE",LstCampi,LstVal)
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
	Conn.Execute Sql

	'Recupero del massimo ID di validaizone inserito
	Sql = ""
	Sql = "SELECT Max(ID_VALID) FROM VALIDAZIONE WHERE ID_PERS_INS = " & IDP

'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
	Set Rs = Conn.Execute (Sql)
	If not Rs.EOF then
		ID_VALID = Rs(0)
	else
		ID_VALID = null
	end if
	set Rs = Nothing
end function


Set Conn = CC
'Set Conn = server.CreateObject ("ADODB.Connection")
'Conn.open strConn
OraDate=ConvDateToDB(Now())
'OraDate = "TO_DATE('" & day(date) & "/" & month(Date) & "/" & year(Date) & " " & left(time,len(time & "")-2) & "','DD/MM/YYYY HH:MI:SS')"

Conn.BeginTrans

	Compet_ID_VALID = ID_VALID()

	'Inserimento informazioni sulla nuova Competenza
	LstCampi = "ATTIVITA,OUTPUT,NOTA,ID_VERBO,DENOMINAZIONE,ID_VALID"

	LstVal = ""
	LstVal = LstVal & "'" & CleanXsql(request("ATTIVITA")) & "'" & "|"
	LstVal = LstVal & "'" & CleanXsql(request("OUTPUT")) & "'" & "|"
	LstVal = LstVal & "'" & CleanXsql(request("NOTE")) & "'" & "|"
	LstVal = LstVal & request("ID_VERBO") & "|"
	LstVal = LstVal & "'" & CleanXsql(request("DENOMINAZIONE")) & "'" & "|"
	LstVal = LstVal & Compet_ID_VALID

	Sql = ""
	Sql = QryIns("COMPETENZE",LstCampi,LstVal)
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
	Conn.Execute Sql


	' Recupero l'ID della COMPETENZA appena inserita
	set rs=nothing
	Sql=""
	Sql= "SELECT ID_COMPETENZA FROM COMPETENZE WHERE ID_VALID=" & Compet_ID_VALID
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
	Rs = Conn.Execute(Sql)
	ID_COMPETENZA = Rs(0)
	set Rs = Nothing

'#### Inizio salvataggio Dati da associare


lst_ID_CONOSCENZA = split(request("lst_ID_CONOSCENZA"),"~")
lst_ID_CAPACITA   = split(request("lst_ID_CAPACITA"),"~")

if Ubound(lst_ID_CONOSCENZA) > 0 then
	for x = lbound(lst_ID_CONOSCENZA) to Ubound(lst_ID_CONOSCENZA)-1

		Valori = split(lst_ID_CONOSCENZA(x),"#")

		LstCampi = "ID_COMPETENZA,ID_CONOSCENZA,GRADO_COMP_CON,ID_VALID"

		LstVal = ""
		LstVal = LstVal & ID_COMPETENZA  & "|"
		LstVal = LstVal & Valori(0) & "|"
		LstVal = LstVal & Valori(1) & "|"
		LstVal = LstVal & ID_VALID()

		Sql = ""
		Sql = QryIns("COMPET_CONOSC",LstCampi,LstVal)

'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
		Conn.Execute Sql
	next
end if

if Ubound(lst_ID_CAPACITA) > 0 then
	for x = lbound(lst_ID_CAPACITA) to Ubound(lst_ID_CAPACITA)-1

		Valori = split(lst_ID_CAPACITA(x),"#")

		LstCampi = "ID_COMPETENZA,ID_CAPACITA,GRADO_COMP_CAP,ID_VALID"

		LstVal = ""
		LstVal = LstVal & ID_COMPETENZA  & "|"
		LstVal = LstVal & Valori(0) & "|"
		LstVal = LstVal & Valori(1) & "|"
		LstVal = LstVal & ID_VALID()

		Sql = ""
		Sql = QryIns("COMPET_CAPAC",LstCampi,LstVal)

'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
		Conn.Execute Sql
	next
end if

LogTrace ucase(mid(session("progetto"),2)),IDP, "P",IDP,"COMPETENZE", "" &IDP &"", "INS"

Conn.CommitTrans
Conn.Close
Set conn = Nothing
%>
<script>
<!--
	 top.close()
//-->
</script>
