<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #INCLUDE FILE="../Utils.asp" -->
<title>Detalle de la Competencia</title>
<LINK REL=STYLESHEET TYPE="text/css" HREF='<%=session("progetto")%>/fogliostile.css'>
<body class=sfondocentro topmargin="10" leftmargin="0">

<%


Set Conn = server.CreateObject ("ADODB.Connection")
Conn.open strConn

	Sql = ""
	Sql = Sql & "SELECT "
	Sql = Sql & "COMPETENZE.OUTPUT, COMPETENZE.ATTIVITA, COMPETENZE.DENOMINAZIONE, "
	Sql = Sql & "COMPETENZE.NOTA, VERBI.DENOMINAZIONE Verbo "
	Sql = Sql & "FROM "
	Sql = Sql & "COMPETENZE, VERBI "
	Sql = Sql & "WHERE "
	Sql = Sql & "COMPETENZE.ID_VERBO = VERBI.ID_VERBO "
	Sql = Sql & "AND "
	Sql = Sql & "COMPETENZE.ID_COMPETENZA = " & request("ID_COMPETENZA")

'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
	Set Rs = Conn.Execute (Sql)
	if not rs.eof then COMPET = Rs.getrows()

Sql = ""
	Sql = Sql & "SELECT "
	Sql = Sql & "CONOSCENZE.DENOMINAZIONE, CONOSCENZE.ID_CONOSCENZA, "
	Sql = Sql & "VALIDAZIONE.ID_VALID, VALIDAZIONE.FL_VALID, VALIDAZIONE.ID_PERS_INS, COMPET_CONOSC.GRADO_COMP_CON "
	Sql = Sql & "FROM "
	Sql = Sql & "CONOSCENZE, COMPET_CONOSC, VALIDAZIONE "
	Sql = Sql & "WHERE "
	Sql = Sql & "COMPET_CONOSC.ID_VALID = VALIDAZIONE.ID_VALID "
	Sql = Sql & "AND "
	Sql = Sql & "COMPET_CONOSC.ID_CONOSCENZA = CONOSCENZE.ID_CONOSCENZA "
	Sql = Sql & "AND "
	Sql = Sql & "COMPET_CONOSC.ID_COMPETENZA = " & request("ID_COMPETENZA")
	Sql = Sql & "ORDER BY UPPER(CONOSCENZE.DENOMINAZIONE)"

'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
	Set Rs = Conn.Execute (Sql)
	if not rs.eof then CONOSC = Rs.getrows()

	Sql = ""
	Sql = Sql & "SELECT "
	Sql = Sql & "CAPACITA.DENOMINAZIONE, CAPACITA.ID_CAPACITA, "
	Sql = Sql & "VALIDAZIONE.ID_VALID, VALIDAZIONE.FL_VALID, VALIDAZIONE.ID_PERS_INS, COMPET_CAPAC.GRADO_COMP_CAP "
	Sql = Sql & "FROM "
	Sql = Sql & "CAPACITA, COMPET_CAPAC, VALIDAZIONE "
	Sql = Sql & "WHERE "
	Sql = Sql & "COMPET_CAPAC.ID_VALID = VALIDAZIONE.ID_VALID "
	Sql = Sql & "AND "
	Sql = Sql & "COMPET_CAPAC.ID_CAPACITA = CAPACITA.ID_CAPACITA "
	Sql = Sql & "AND "
	Sql = Sql & "COMPET_CAPAC.ID_COMPETENZA = " & request("ID_COMPETENZA")
	Sql = Sql & "ORDER BY UPPER(CAPACITA.DENOMINAZIONE)"

'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
	Set Rs = Conn.Execute (Sql)
	if not rs.eof then CAPAC = Rs.getrows()

	Sql =""
	Sql = Sql &  "SELECT "
	Sql = Sql &  "   FIG.DENOMINAZIONE PROFILO, FIG.IND_MAPPA, COM.DENOMINAZIONE COMPETENZA,  "
	Sql = Sql &  "   COM_FP.GRADO_FP_COMP, VAL.FL_VALID"
	Sql = Sql &  " FROM "
	Sql = Sql &  "   COMPET_FP COM_FP, "
	Sql = Sql &  "   COMPETENZE COM, "
	Sql = Sql &  "   FIGUREPROFESSIONALI FIG, "
	Sql = Sql &  "   VALIDAZIONE VAL"
	Sql = Sql &  " WHERE "
	Sql = Sql &  " ( COM_FP.ID_VALID=VAL.ID_VALID ) AND "
	Sql = Sql &  "   ( COM.ID_COMPETENZA=COM_FP.ID_COMPETENZA ) AND "
	Sql = Sql &  "   ( FIG.ID_FIGPROF=COM_FP.ID_FIGPROF ) and"
	Sql = Sql &  "   COM_FP.ID_COMPETENZA =" & request("ID_COMPETENZA") & " and "
	Sql = Sql &  "   VAL.FL_VALID=1"
	Sql = Sql &  "ORDER BY UPPER(FIG.DENOMINAZIONE)"

'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
	Set Rs = Conn.Execute (Sql)
	if not rs.eof then FIGPROF = Rs.getrows()

set Rs = Nothing
Conn.Close
Set conn = Nothing

%>

<SCRIPT LANGUAGE="JavaScript">
function stp(){
bb=(document.layers)?document.layers['aa']:(document.getElementById)?document.getElementById('aa').style:document.all['aa'].style;
bb.visibility="hidden"
self.print();
bb.visibility="visible"
}
</SCRIPT>


<center>
<table border=0 width='500' CELLPADDING=0 cellspacing=0>
	<tr>
		<td class='tbltext0' colspan='2' bgcolor='#3399CC' align=left bordercolor="#C2E0FF" width='300'>
			<b>DETALLE DE LA COMPETENCIA</b>
		</td>
	<td width="25" valign='bottom' background="<%=session("progetto")%>/images/sfondo_linguetta.gif" >
		<img border="0" src="<%=session("progetto")%>/images/tondo_linguetta.gif"></td>
	<td width="278" valign='bottom' background="<%=session("progetto")%>/images/sfondo_linguetta.gif" ></td>
	</tr>
    <tr class="Sfondocomm">
		<td align="left" colspan="4">
				Se define competencia como el grado de integración entre los <b> conocimientos </b>, los
				<b> Desempeños </b>, y los <b> valores/Comportamientos </b> que conforman los requisitos de
				una actividad requerida en una situación especifica”. Si nos referimos al análisis de un
				procedimiento productivo, la <b> competencia </b> califica el operador para estar en condiciones de
				realizar una actividad principal/intermedia.
	    </td>
    </tr>
    <tr>
		<td colspan=4 bgcolor='#3399CC'></td>
    </tr>
	  <tr><td>&nbsp;</td></tr>
	<tr>
		<td class="tbltext1" valign=top align='left' width='20%'><b>Actividad&nbsp;</b></td>
		<td class="tbltext1"><%=COMPET(1,0)%></td>
	</tr>
	<tr>
		<td class="tbltext1" valign=top align='left' width='20%'><b>Salida&nbsp;</b></td>
		<td class="tbltext1"><%=COMPET(0,0)%></td>
	</tr>
	<tr>
		<td class="tbltext1" valign=top align='left' width='20%'><b>Verbo&nbsp;</b></td>
		<td class="tbltext1"><%=COMPET(4,0)%></td>
	</tr>
	<tr>
		<td class="tbltext1" valign=top align='left' width='20%'><b>Denominación&nbsp;</b></td>
		<td class="tbltext1"><%=COMPET(2,0)%></td>
	</tr>
	<tr>
		<td class="tbltext1" valign=top align='left' width='20%'><b>Descripción&nbsp;</b></td>
		<td class="tbltext1"><%= COMPET(3,0)%></td>
	</tr>
	</table>
	<BR>

<%
if IsArray(CONOSC) then
	Response.Write "<table border=0 width='500' cellspacing=1 cellpadding=1>"
	Response.Write "<tr class='sfondocomm'>"
		Response.Write "<td ><b>CONOCIMIENTOS ASOCIADOS</b></td>"
		Response.Write "<td align='center'><b>Nivel Requerido</b></td>"
	Response.Write "</tr>"
		for I = lbound(CONOSC,2) to Ubound(CONOSC,2)
			Response.Write "<tr>"
				Response.Write "<td class='tbltext1'><SMALL>" & CONOSC(0,I) & "</SMALL></td>"
				Response.Write "<td align='center' class='tbltext1'><b><SMALL>" & CONOSC(5,I) & "</SMALL></b></td>"
			Response.Write "</tr>"
		next
		Response.Write "</table>"
	end if
%>
<br>
<%
	if IsArray(CAPAC) then
	Response.Write "<table border=0 width='500' cellspacing=1 cellpadding=1>"
	Response.Write "<tr class='sfondocomm'>"
		Response.Write "<td><b>DESEMPEÑOS ASOCIADOS</b></td>"
		Response.Write "<td align='center'><b>Nivel Requerido</b></td>"
	Response.Write "</tr>"
		for I = lbound(CAPAC,2) to Ubound(CAPAC,2)
			Response.Write "<tr>"
				Response.Write "<td class='tbltext1'><SMALL>" & CAPAC(0,I) & "</SMALL></td>"
				Response.Write "<td class='tbltext1' align='center' widht='15%'><SMALL><b>" & CAPAC(5,I) & "</SMALL></b></td>"
			Response.Write "</tr>"
		next
		Response.Write "</table>"
	end if
%>
<br>
<%
	if IsArray(FIGPROF) then
	Response.Write "<table border=0 width='500' cellspacing=1 CELLPADDING=1>"
	Response.Write "<tr class=sfondocomm>"
	Response.Write "<td ><b>FIGURA PROFESIONAL </b></td>"
	Response.Write "<td align='center' widht='20%'><b>Nivel Requerido</b></td>"
	Response.Write "</tr>"

		for I = lbound(FIGPROF,2) to Ubound(FIGPROF,2)
			Response.Write "<tr>"
				Response.Write "<td class=tbltext1>" & FIGPROF(0,I) & "</td>"
				Response.Write "<td class=tbltext1 align='center' widht='20%'><b>" & FIGPROF(3,I) & "</b></td>"
			Response.Write "</tr>"
		next
	Response.Write "</table>"
	end if
%>

<form name='indietro'>
<div id="aa">
<input type='button' value='Cancelar' OnClick='top.close()' CLASS='My'>
<input type='button' value='Imprimir' OnClick='stp()' CLASS='My'>
</form>
</div>
<TABLE width=500>
<tr>
<td align="center"></td>
</tr>
<tr>
	<td bgcolor='#3399CC'></td>
</tr>
</table>
</center>
