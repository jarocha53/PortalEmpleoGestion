<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #include Virtual="/strutt_testa2.asp" -->
<!-- #INCLUDE FILE="../Utils.asp" -->
<%

Set Conn = CC
'Set Conn = server.CreateObject ("ADODB.Connection")
'Conn.open strConn

Sql = ""
Sql = Sql & "SELECT ID_AREAPROF, DENOMINAZIONE "
Sql = Sql & "FROM AREE_PROFESSIONALI "
Sql = Sql & "WHERE ID_AREAPROF <>0 ORDER BY UPPER(DENOMINAZIONE) "
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
Set Rs = Conn.Execute (Sql)
if not rs.eof then AREEPROF = Rs.getrows()

Set Rs = Nothing
Conn.Close
Set conn = Nothing
%>
<script language='Javascript'>
<!--
function valida(Obj)
{
	if ((Obj.Denominazione.value == "") || (Obj.ID_AREAPROF.value == "") || (Obj.BreveDescr.value == "") || (Obj.COD_ISTAT.value == ""))
	{
		alert("Los campos :\n'Denominacion' , 'Area Profesional', 'Breve Descripcion' y 'Codigo Istat'\n son OBLIGATORIOS")
		return false
	}
}

function elimina(A,B)
{
	if (confirm("Seguro desea ELIMINAR definitivamente esta figura profesional?"))
	{
		goToPage("CancFigProf.asp?ID_FIGPROF=" + A + "&ID_VALID=" + B)
	}
}

function AbilitaValidazione(A,B,C)
{
		goToPage("AbilitaValidazione.asp?ID_FIGPROF=" + A + "&FL_STATOLAV=" + B + "&ID_VALID=" + C)
}

function ApriCodIstat(Obj)
{
	CriterioRicerca= Obj.CriterioRicerca.value
	//alert (CriterioRicerca)
	URL = "CodiceIstat.asp?VISMENU=NO" + "&RICERCA="+ CriterioRicerca

	opt = "address=no,status=no,width=650,height=240,top=150,left=15"

	window.open (URL,"",opt)
}

//-->
</script>

<CENTER>

<table border=0 width='500' CELLPADDING=0 cellspacing=0>
	<tr>
		<td  class=tbltext0 bgcolor='#3399CC' align=left bordercolor="#C2E0FF" width='199'>
			<b>FIGURA PROFESIONAL</b>
		</td>
	<td width="25" valign='bottom' background="<%=session("progetto")%>/images/sfondo_linguetta.gif" >
		<img border="0" src="<%=session("progetto")%>/images/tondo_linguetta.gif"></td>
	<td width="278" valign='MIDDLE' align=right class=tbltext1 background="<%=session("progetto")%>/images/sfondo_linguetta.gif" >(*) campo obligatorio</td>
	</tr>

</table>
<table border=0 width='500' CELLPADDING=0 cellspacing=0>
		<tr class="sfondocomm">
			<td>Define un oficio en t�rminos de tarea, salida y procesamiento ( o fases de procedimiento )
				asignados a su �rea de responsabilidad profesional.
				El oficio se define para cada <b> Figura Profesional </b>, <b> Competencias </b> definidas como salida
				salida de una figura dada.</td>
		   	<td class=sfondocomm>
				<a href="Javascript:Show_Help('<%=Session("Progetto")%>/HelpBP/Placement/Fig_Prof/Add_FigProf.htm')">
				<img src="<%=Session("Progetto")%>/images/Help.gif" border='0'  align=right alt='Help'></a>
			</td>
	    </tr>
	    <tr>
			<td colspan=2 bgcolor='#3399CC'></td>
	    </tr>
</table>


<table border=0 cellspacing=1 cellpadding=1 width='500'>
<tr>
<td align=center>
<form name='FigureProfessionali' Action='SalvaFigProf.asp' Method='POST' OnSubmit='return valida(this)'>
<input type="hidden" name="IdPers" value="<%=sIdPers%>">
<input type="hidden" name="IND_FASE" value="<%=IND_FASE%>">
<input type='hidden' name='VISMENU' value='NO'>

	<table border=0 width='500' cellspacing=1 cellpadding=1 >

		<tr>
			<td class=tbltext1  align=LEFT><B>Denominaci�n*&nbsp;</B></td>
			<td><input type='text' class='MyTextBox' maxlength='60' size='60'name='Denominazione' value=''></td>
		</tr>
		<tr>
			<td class=tbltext1  align=LEFT><B>Descripci�n*&nbsp;</B></td>
			<td><textarea rows=3 class='MyTextBox' cols=55 name='Descrizione'></textarea></td>
		</tr>
		<tr>
			<td class=tbltext1  align=LEFT><B>Breve Descripci�n&nbsp;*</B></td>
			<td><textarea rows=3 class='MyTextBox' cols=55 name='BreveDescr'></textarea></td>
		</tr>
		<tr>
			<td class=tbltext1  align=LEFT><B>Area Profesional*&nbsp;</B></td>
			<td>
			<%
			Response.Write "<select class=MY name='ID_AREAPROF' style='width:400'>"
			Response.Write "<option value=''></option>"
			for K = lbound(AREEPROF,2) to Ubound(AREEPROF,2)
				Response.Write "<option value='" & AREEPROF(0,K) & "'>" & AREEPROF(1,K) & "</option>"
			next
			Response.Write "</select>"
			%>
			</td>
		</tr>
		<tr>
			<td colspan=2 bgcolor='#3399CC'></td>
		</tr>
		<tr>
			<td colspan=2 class='textblack'>
			<small>
			Si ingresa parte de la denominaci�n de la <b> Tarea de ISTAT </B> que quiere asociar a la
			Figura Profesional que est� queriendo ingresar.<br>
			Para consultar el diccionario de las clasificaciones de las profesiones de ISTAT es suficiente
			presionar el bot�n BUSCAR sin ingresar nada en la caja de texto.
			</small></td>
		</tr>
		<tr>
			<td class=tbltext1  align=LEFT><B>Buscar el Perf�l</B></td>
			<td><input type='text' class='MyTextBox' size=50  maxlength=50 name='CriterioRicerca'></td>
		</tr>
		<tr>
			<td  class=tbltext1  align=LEFT><B>Codigo Istat*&nbsp;</B></td>
			<td bgcolor='#ffffff'><input type='text' class='MyTextBox' maxlength='15' size='15' name='COD_ISTAT' readonly value=''>&nbsp;&nbsp;&nbsp;
			<input type='button' value='Buscar' CLASS='My' OnClick='ApriCodIstat(this.form)'><br>
			<input type='text' class='MyTextBox' size='60' name='DescrMansione' readonly value=''>
			</td>
		</tr>
		<tr>
			<td bgcolor='#FFFFFF' align=Left>
				<input type='Button' value='Volver' CLASS='My' OnClick='goToPage("FigProf.asp")'>
			</td>
			<td bgcolor='#FFFFFF' align=right>
				<input type='Submit' value='Guardar' CLASS='My'>
			</td>
		<tr>
			<td colspan=2 bgcolor='#3399CC'></td>
		</tr>

		</tr>
	</table>
</form>
</table>
</center>

<!-- #include Virtual="/strutt_coda2.asp" -->
