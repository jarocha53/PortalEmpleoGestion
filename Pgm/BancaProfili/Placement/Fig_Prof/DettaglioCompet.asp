<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #include Virtual="/strutt_testa2.asp" -->
<!-- #INCLUDE FILE="../Utils.asp" -->
<%


Set Conn = server.CreateObject ("ADODB.Connection")
Conn.open strConn

	Sql = ""
	Sql = Sql & "SELECT "
	Sql = Sql & " CONOSCENZE.DENOMINAZIONE, CONOSCENZE.ID_CONOSCENZA, "
	Sql = Sql & " VALIDAZIONE.ID_VALID, VALIDAZIONE.FL_VALID, VALIDAZIONE.ID_PERS_INS,"
	Sql = Sql & " COMPET_CONOSC.GRADO_COMP_CON "
	Sql = Sql & "FROM "
	Sql = Sql & " CONOSCENZE, COMPET_CONOSC, VALIDAZIONE "
	Sql = Sql & "WHERE "
	Sql = Sql & " COMPET_CONOSC.ID_VALID = VALIDAZIONE.ID_VALID "
	Sql = Sql & "AND "
	Sql = Sql & " COMPET_CONOSC.ID_CONOSCENZA = CONOSCENZE.ID_CONOSCENZA "
	Sql = Sql & "AND "
	Sql = Sql & " COMPET_CONOSC.ID_COMPETENZA = " & request("ID_COMPETENZA")
	Sql = Sql & " ORDER BY UPPER(CONOSCENZE.DENOMINAZIONE)"

'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
Set Rs = Conn.Execute (Sql)
if not rs.eof then CONOSC = Rs.getrows()

	Sql = ""
	Sql = Sql & "SELECT "
	Sql = Sql & " CAPACITA.DENOMINAZIONE, CAPACITA.ID_CAPACITA, "
	Sql = Sql & " VALIDAZIONE.ID_VALID, VALIDAZIONE.FL_VALID, VALIDAZIONE.ID_PERS_INS,"
	Sql = Sql & " COMPET_CAPAC.GRADO_COMP_CAP "
	Sql = Sql & "FROM "
	Sql = Sql & " CAPACITA, COMPET_CAPAC, VALIDAZIONE "
	Sql = Sql & "WHERE "
	Sql = Sql & " COMPET_CAPAC.ID_VALID = VALIDAZIONE.ID_VALID "
	Sql = Sql & "AND "
	Sql = Sql & " COMPET_CAPAC.ID_CAPACITA = CAPACITA.ID_CAPACITA "
	Sql = Sql & "AND "
	Sql = Sql & " COMPET_CAPAC.ID_COMPETENZA = " & request("ID_COMPETENZA")
	Sql = Sql & " ORDER BY UPPER (CAPACITA.DENOMINAZIONE)"
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
Set Rs = Conn.Execute (Sql)
if not rs.eof then CAPAC = Rs.getrows()

	Sql = ""
	Sql = Sql & "SELECT "
	Sql = Sql & "COMPETENZE.DENOMINAZIONE, COMPETENZE.ID_COMPETENZA, "
	Sql = Sql & "COMPET_FP.GRADO_FP_COMP "
	Sql = Sql & "FROM "
	Sql = Sql & "COMPETENZE, COMPET_FP, VALIDAZIONE "
	Sql = Sql & "WHERE "
	Sql = Sql & "COMPET_FP.ID_VALID = VALIDAZIONE.ID_VALID "
	Sql = Sql & "AND "
	Sql = Sql & "COMPET_FP.ID_COMPETENZA = COMPETENZE.ID_COMPETENZA "
	Sql = Sql & "AND "
	Sql = Sql & "COMPET_FP.ID_FIGPROF = " & request("ID_FIGPROF")
	Sql = Sql & " AND "
	Sql = Sql & " COMPETENZE.ID_COMPETENZA = " & request("ID_COMPETENZA")
	Sql = Sql & " ORDER BY UPPER (COMPETENZE.DENOMINAZIONE)"
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
Set Rs = Conn.Execute (Sql)
if not rs.eof then
	Des_Compet = Rs(0)
	Liv_Atteso= Rs(2)
end if

set Rs = Nothing
Conn.Close
Set conn = Nothing

%>
<center>
<table border=0 width='500' CELLPADDING=0 cellspacing=0>
	<tr>
		<td  class=tbltext0 bgcolor='#3399CC' align=left bordercolor="#C2E0FF" width='350'>
			<b>DETALLE DE LA FIGURA PROFESIONAL</b>
		</td>
	<td width="25" valign='bottom' background="<%=session("progetto")%>/images/sfondo_linguetta.gif" >
		<img border="0" src="<%=session("progetto")%>/images/tondo_linguetta.gif"></td>
	<td width="278" valign='bottom' background="<%=session("progetto")%>/images/sfondo_linguetta.gif" ></td>
	</tr>

    <tr class="sfondocomm">
		<td align="left" colspan=2>
		A continuación se listan los elementos de competencia. <br>
		<b>Denominación:</B> <%=request("DENOMINAZIONE")%>
		</td>
		<td><a href="Javascript:Show_Help('<%=Session("Progetto")%>/HelpBP/Placement/Fig_Prof/DettaglioCompet.htm')">
	     <img src="<%=Session("Progetto")%>/images/Help.gif" border='0'  align=right alt='Help'></a>
	</td>
		
	</tr>
	<tr>
		<td colspan=3 bgcolor='#3399CC'></td>
    </tr>
</table>
</center>
<br>

<%
	Response.Write "<table border=0 cellspacing=1 cellpadding=1 width='500'>"
	Response.Write "<tr class=sfondocomm>"
	Response.Write "	<td valign=top colspan=2>"
	Response.Write "		<B>Detalle de Competencias</B>"
	Response.Write "	</td>"
	Response.Write "</tr>"
	Response.Write "	<tr class=sfondocomm>"
	Response.Write "		<td><b>Denominación</b></td>"
	Response.Write "		<td width='20%'><b>Nivel Requerido</b></td>"
	Response.Write "	</tr>"
	Response.Write "	<tr>"
	Response.Write "		<td class=tbltext1>" & Des_Compet & "</td>"
	Response.Write "		<td class=tbltext1 align='center'>" & Liv_Atteso & "</td>"
	Response.Write "	</tr>"
	Response.Write "</table>"
	Response.Write "<br>"

	if IsArray(CONOSC) then
	Response.Write "<table border=0 width='500' cellspacing=1 cellpadding=1>"
	Response.write "<tr class=sfondocomm>"

		Response.Write "<td><b>Conocimientos</b></td>"
		Response.Write "<td width='20%'><b>Nivel Requerido</b></td>"
	Response.Write "</tr>"
		for I = lbound(CONOSC,2) to Ubound(CONOSC,2)
		if (i mod 2)= 0 then
		  sfondo="class='tblsfondo'"
		else
		  sfondo="class='tbltext1'"
        end if
			Response.Write "<tr>"
				Response.Write "<td class=tbltext1>" & CONOSC(0,I) & "</td>"
				Response.Write "<td class=tbltext1 align='center'>" & CONOSC(5,I) & "</td>"
			Response.Write "</tr>"
		next
		Response.Write "</table>"

	end if
%>
<br>
<%
	if IsArray(CAPAC) then
	Response.Write "<table border=0 width='500' cellspacing=1 cellpadding=1>"

	Response.Write "<tr class=sfondocomm>"
		Response.Write "<td><b>Desempeños</b></td>"
		Response.Write "<td width='20%'><b>Nivel Requerido</b></td>"
	Response.Write "</tr>"
		for I = lbound(CAPAC,2) to Ubound(CAPAC,2)
		if (i mod 2)= 0 then
		  sfondo="class='tblsfondo'"
		else
		  sfondo="class='tbltext1'"
        end if
			Response.Write "<tr>"

				Response.Write "<td class=tbltext1>" & CAPAC(0,I) & "</td>"
				Response.Write "<td class=tbltext1 align='center'>" & CAPAC(5,I) & "</td>"
			Response.Write "</tr>"
		next
		Response.Write "</table>"

	end if

	if not IsArray(CONOSC) AND not IsArray(CAPAC) then
		Response.Write "<p align=center class='tbltext3'>No se han ingresado desempeños ni conocimientos ...</p>"
	end if
	DEN = request("DENOMINAZIONE")
	FIG = request("ID_FIGPROF")
	BREVE_DESC= request("BREVE_DESC")
%>
<form name='indietro'>
<p align="left">&nbsp;
<input type='button' value='Volver' OnClick='goToPage("RiepilogoFigProf.asp?DENOMINAZIONE=<%=DEN%>&ID_FIGPROF=<%=FIG%>&BREVE_DESC=<%=BREVE_DESC%>")' CLASS='My'>
</p>
</form>
<!-- #include Virtual="/strutt_coda2.asp" -->

