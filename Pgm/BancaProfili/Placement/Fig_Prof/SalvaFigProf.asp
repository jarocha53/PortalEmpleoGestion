<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #INCLUDE FILE="../Utils.asp" -->
<%
Set Conn = CC
'Set Conn = server.CreateObject ("ADODB.Connection")
'Conn.open strConn
OraDate=ConvDateToDB(Now())

Conn.BeginTrans

	'Creazione di un nuovo record di validazione
	LstCampi = "ID_PERS_INS,ID_PERS_LAV,FL_VALID,DT_TMST"

	LstVal = ""
	LstVal = LstVal & IDP & "|"
	LstVal = LstVal & IDP & "|"
	LstVal = LstVal & "0" & "|"
	LstVal = LstVal & OraDate

	Sql = ""
	Sql = QryIns("VALIDAZIONE",LstCampi,LstVal)
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
	Conn.Execute Sql
	'Recupero del massimo ID di validaizone inserito
	Sql = ""
	Sql = "SELECT Max(ID_VALID) FROM VALIDAZIONE WHERE ID_PERS_INS = " & IDP
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
	Rs = Conn.Execute(Sql)
	ID_VALID = Rs(0)
	set Rs = Nothing

	'Inserimento informazioni sulla nuova Fig Prof
	LstCampi = "DENOMINAZIONE,ID_AREAPROF,DESCRIZIONE,BREVE_DESC,ID_VALID,COD_EJ"

	LstVal = ""
	LstVal = LstVal & "'" & ucase(trim(CleanXsql(request("DENOMINAZIONE")))) & "'" & "|"
	LstVal = LstVal & request("ID_AREAPROF") & "|"
	LstVal = LstVal & "'" & ucase(TRIM(CleanXsql(request("DESCRIZIONE")))) & "'" & "|"
	LstVal = LstVal & "'" & ucase(TRIM(CleanXsql(request("BREVEDESCR")))) & "'" & "|"
	LstVal = LstVal & ID_VALID & "|"
	LstVal = LstVal & "'" & TRIM(CleanXsql(request("COD_ISTAT"))) & "'"

	Sql = ""
	Sql = QryIns("FIGUREPROFESSIONALI",LstCampi,LstVal)

'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
	Conn.Execute Sql

	' Recupero l'ID della fig. Profess. appena inserita
	set rs=nothing
	Sql=""
	Sql= "SELECT ID_FIGPROF FROM FIGUREPROFESSIONALI WHERE ID_VALID=" & ID_VALID
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
	Rs = Conn.Execute(Sql)
	ID_FIGPROF = Rs(0)
	set Rs = Nothing

	'ASSOCIAZIONE AUTOMATICA PROFILO - PROGETTO
	PRJ = ucase(mid(session("progetto"),2))
	Sql=""
	Sql= "INSERT INTO FIGPROF_PRJ (ID_FIGPROF,COD_PRJ) VALUES(" & ID_FIGPROF & ",'" & PRJ & "')"
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
	Rs = Conn.Execute(Sql)

	LogTrace ucase(mid(session("progetto"),2)),IDP, "P",IDP,"FIGUREPROFESSIONALI", "" & IDP &"", "INS"
Conn.CommitTrans
Conn.Close
Set conn = Nothing
%>
<script>
<!--
	goToPage("FigProf.asp")
//-->
</script>
