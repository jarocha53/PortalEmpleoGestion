<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #include Virtual="/strutt_testa2.asp" -->
<!-- #INCLUDE FILE="../Utils.asp" -->
<%
if request("lDwn") = "" Or Int(request("lDwn")) < 0 then
	lDwn = 0
else
	lDwn = request("lDwn")
end if

dim V2

if (request("competencias") <> "" OR request("comportamientos") <> "" )  then
	Set Conn = server.CreateObject ("ADODB.Connection")
	Conn.open strConn

	if request("competencias") <> "" then
			Sql = ""
			Sql = Sql & "SELECT DISTINCT "
			Sql = Sql & " COMPETENZE.DENOMINAZIONE, COMPETENZE.ID_COMPETENZA "
			Sql = Sql & " FROM COMPETENZE, VALIDAZIONE, FIGUREPROFESSIONALI, COMPET_FP "
			Sql = Sql & " WHERE "
			
			W = W & " COMPETENZE.ID_VALID = VALIDAZIONE.ID_VALID "
			W = W & " AND COMPET_FP.ID_COMPETENZA = COMPETENZE.ID_COMPETENZA "
			W = W & " AND COMPET_FP.ID_FIGPROF= FIGUREPROFESSIONALI.ID_FIGPROF "

			if request("ID_AREA") <>"" then
				W = W &  " AND FIGUREPROFESSIONALI.ID_AREAPROF = " & request("ID_AREA")
			end if
			
			W = W & " AND COMPETENZE.ID_COMPETENZA NOT IN "
			W = W & "	(SELECT ID_COMPETENZA "
			W = W & "	FROM COMPET_FP "
			W = W & "	WHERE ID_FIGPROF = " & request("ID_FIGPROF") & ") "

			W = W & " AND (VALIDAZIONE.ID_PERS_INS = " & IdSedeAz & " OR VALIDAZIONE.FL_VALID = 1) "
			W = W &  " AND UPPER(COMPETENZE.DENOMINAZIONE) LIKE '%" & UCASE(CleanXsql(REQUEST("testo"))) & "%'"
			
			Sql = Sql & W & " ORDER BY upper(COMPETENZE.DENOMINAZIONE)"

			SqlCount = ""
			SqlCount = SqlCount & " SELECT count(COMPETENZE.ID_COMPETENZA) maxresult "
			SqlCount = SqlCount & " FROM COMPETENZE, VALIDAZIONE, FIGUREPROFESSIONALI, COMPET_FP "
			SqlCount = SqlCount & " WHERE "
			SqlCount = SqlCount & W

		SaveAs = "competenze"
		Puls= "competencias"
		V2 = "competencias"

	end if

	if request("comportamientos") <> "" then
		Sql = ""
		Sql = Sql & "SELECT "
		Sql = Sql & "COMPORTAMENTI.DENOMINAZIONE,  COMPORTAMENTI.ID_COMPORTAMENTO "
		Sql = Sql & "FROM "
		Sql = Sql & "COMPORTAMENTI, VALIDAZIONE "
		Sql = Sql & "WHERE "

		W=""
		W = W & "COMPORTAMENTI.ID_VALID = VALIDAZIONE.ID_VALID "
		W = W & "AND "
		W = W & "(VALIDAZIONE.ID_PERS_INS = " & IdSedeAz
		W = W & " OR "
		W = W & "VALIDAZIONE.FL_VALID = 1) "
		W = W & "AND "
		W = W & "COMPORTAMENTI.ID_COMPORTAMENTO NOT IN "
		W = W & "	(SELECT ID_COMPORTAMENTO "
		W = W & "	FROM COMPOR_FP "
		W = W & "	WHERE ID_FIGPROF = " & request("ID_FIGPROF") & ") "
		W = W & "AND "
		W = W & " UPPER(COMPORTAMENTI.DENOMINAZIONE) LIKE '%" & UCASE(CleanXsql(request("testo"))) & "%' "
		if request("ID_AREACOMPORTAMENTO") <> "" then
			W = W & "AND "
			W = W & "COMPORTAMENTI.ID_AREACOMPORTAMENTO = " & request("ID_AREACOMPORTAMENTO") & " "
		end if
		Sql = Sql & W & " ORDER BY COMPORTAMENTI.DENOMINAZIONE"

		SqlCount= ""
		SqlCount= SqlCount & " SELECT COUNT(COMPORTAMENTI.ID_COMPORTAMENTO) maxresult "
		SqlCount= SqlCount & "FROM COMPORTAMENTI, VALIDAZIONE "
		SqlCount= SqlCount & "WHERE "
		SqlCount= SqlCount & W

		SaveAs = "comportamenti"
		Puls = "comportamientos"
		V2 = "comportamientos"

	end if

	Response.Write " <table border=0 width='500' CELLPADDING=0 cellspacing=0>"
		Response.Write " <tr>"
			 Response.Write " <td  class=tbltext0 bgcolor='#3399CC' align=left bordercolor='#C2E0FF' width='350'>"
			 Response.Write " <b>ASOCIACION FIG. PROF. / " & UCASE(V2) & "</b></td>" 
			 Response.Write " <td width='25' valign='bottom' background='" & session("progetto") & "/images/sfondo_linguetta.gif' >"
			 Response.Write " <img border='0' src='" & session("progetto") & "/images/tondo_linguetta.gif'></td>"
		Response.Write "<td width='278' valign='bottom' background='" & session("progetto") & "/images/sfondo_linguetta.gif' ></td>"
		Response.Write "</tr>"
	Response.Write "</table>"

	Response.Write "<table border=0 width='500' CELLPADDING=0 cellspacing=0>"
		Response.Write "<tr class=sfondocomm>"
		Response.Write "<td   align='left' colspan='2' >"
		Msg=""
		Msg=Msg & "Esta pagina le permite construir el mapa de la <b>Figura Professionale</b> seleccionada, asociandole "
		Msg=Msg & "<B>" & ucase (V2) & "</B> que estan presentes en el diccionario."
		Response.Write Msg
		Response.Write "<td><a href=""Javascript:Show_Help('" & Session("Progetto") & "/HelpBP/MyProfile/NuovoProfilo/ResultFigProf.htm')"">"
		Response.Write " <img src='" & Session("Progetto")& "/images/Help.gif' border='0'  align=right alt='Help'>"
		Response.Write "</a>"
		Response.Write "</td>"
		Response.Write " </tr>"
		Response.Write "<tr>"
		Response.Write "<td colspan=3 bgcolor='#3399CC'></td>"
		Response.Write "</tr>"
	Response.Write "</table>"


	QS = ""
	QS = QS & "BREVE_DESC="   & request("BREVE_DESC")
	QS = QS & "&DENOMINAZIONE="   & request("DENOMINAZIONE")
	QS = QS & "&AREAPROF="  & request("AREAPROF")
	QS = QS & "&ID_VALID="  & request("ID_VALID")
	QS = QS & "&ID_FIGPROF="  & request("ID_FIGPROF")
	
	Set Rs = server.CreateObject ("ADODB.Recordset")

		Rs.Open Sql,Conn,3,3,1

		if not rs.eof then
			Rs.Move (lDwn)
			RESULT = Rs.getrows(20)
'PL-SQL * T-SQL  
SQLCOUNT = TransformPLSQLToTSQL (SQLCOUNT) 
			RsCount = Conn.Execute(SqlCount)
			RC = RsCount(0)
			Set RsCount = Nothing
		else
			Response.Write "<CENTER>"
		Response.Write "<table border=0 cellspacing=1 cellpadding=1 width=500>"
				Response.Write "<tr>"
					Response.Write "<td class=tbltext3 align=center valign=top>"
					Response.Write "<b>No se han encontrado elementos que satisfagan los criterios de b�squeda</b>"
					Response.Write "</td>"
				Response.Write "</tr>"
				Response.Write "<tr>"
					Response.Write "<td>"
					Response.Write "<input type='button' value='Atr�s' OnClick='goToPage(""DettFigProf.asp?" & QS & """)' Class='My'>"
					Response.Write "</td>"
				Response.Write "</tr>"
				Response.Write " <tr>"
					Response.Write " <td colspan=3 bgcolor='#3399CC'></td>"
				Response.Write "</tr>"
		Response.Write "</table>"
		end if
	Conn.Close
	Set conn = Nothing
end if
%>
<script language='Javascript'>
	<!--
	    function Salva(lDwn)
		{
			A = lDwn

			URL =  "ResultFigProf.asp?"
			URL += "lDwn=" + A + "&"
			URL += 'testo=<%=CleanXsql(request("testo"))%>&'
			URL += 'ID_AREA=<%=request("ID_AREA")%>&'
			URL += 'DENOMINAZIONE=<%=request("DENOMINAZIONE")%>&'
			URL += 'AREAPROF=<%=request("AREAPROF")%>&'
			URL += 'ID_FIGPROF=<%=request("ID_FIGPROF")%>&'
			URL += 'IdSedeAz=<%=request("IdSedeAz")%>&'
			URL += 'BREVE_DESC=<%=request("BREVE_DESC")%>&'
			URL += 'TitoloArea=<%=request("TitoloArea")%>&'          
			URL += 'ID_VALID=<%=Request("ID_VALID")%>&'
			URL += '<%=Puls%>=<%=Puls%>&'
			
			document.ListaItem.URL.value = URL
		}

		function mandaAvanti(lDwn)
		{
			A = lDwn + 20

			URL =  "ResultFigProf.asp?"
			URL += "lDwn=" + A + "&"
			URL += 'testo=<%=CleanXsql(request("testo"))%>&'
			URL += 'ID_AREA=<%=request("ID_AREA")%>&'
			URL += 'ID_FIGPROF=<%=request("ID_FIGPROF")%>&'
			URL += 'TitoloArea=<%=request("TitoloArea")%>&'
			URL += 'BREVE_DESC=<%=request("BREVE_DESC")%>&'    
			URL += 'AREAPROF=<%=request("AREAPROF")%>&'
			URL += 'DENOMINAZIONE=<%=request("DENOMINAZIONE")%>&'
			URL += '=<%=request("ID_FIGPROF")%>&'
			URL += 'ID_VALID=<%=Request("ID_VALID")%>&'
			URL += '<%=Puls%>=<%=Puls%>'

			goToPage(URL)
		}

		function mandaIndietro(lDwn)
		{
			A = lDwn - 20
			URL =  "ResultFigProf.asp?"
			URL += "lDwn=" + A + "&"
			URL += 'testo=<%=CleanXsql(request("testo"))%>&'
			URL += 'ID_AREA=<%=request("ID_AREA")%>&'
			URL += 'ID_FIGPROF=<%=request("ID_FIGPROF")%>&'
			URL += 'BREVE_DESC=<%=request("BREVE_DESC")%>&'
			URL += 'AREAPROF=<%=request("AREAPROF")%>&'
			URL += 'DENOMINAZIONE=<%=request("DENOMINAZIONE")%>&'
			URL += 'TitoloArea=<%=request("TitoloArea")%>&'    
			URL += 'ID_VALID=<%=Request("ID_VALID")%>&'
			URL += '<%=Puls%>=<%=Puls%>'

			goToPage(URL)
		}

		function FocusCompet(ID_COMPETENZA)
		{
			URL = "FocusCompet.asp?"
			URL = URL + "IdSedeAz=" + <%=Request("IdSedeAz")%> + "&ID_COMPETENZA=" + ID_COMPETENZA + "&VISMENU=NO" 

			opt = "address=no,status=no,width=600,height=550,top=150,left=35,scrollbars=yes"

			window.open (URL,"",opt)
		}
		
		function FocusComportamenti(ID_COMPORTAMENTO)
		{
			URL = "FocusComportamenti.asp?"
			URL = URL + "IdSedeAz=" + <%=Request("IdSedeAz")%> +  "&ID_COMPORTAMENTO=" + ID_COMPORTAMENTO  + "&VISMENU=NO"

			opt = "address=no,status=no,width=600,height=480,top=150,left=35,scrollbars=yes"

			window.open (URL,"",opt)
		}
		
//-->
</script>


<form name='ListaItem' Method=POST Action='SalvaAssociaFigProf.asp' OnSubmit="Salva(<%=lDwn%>)">
	<input type="hidden" name="IdPers" value="<%=sIdPers%>">
	<input type="hidden" name="IdSedeAz" value="<%=IdSedeAz%>">
	<input type="hidden" name="IND_FASE" value="<%=IND_FASE%>">
	<input type="hidden" name="VISMENU" value="NO">
	<input type='hidden' name='ID_FIGPROF' value='<%=request("ID_FIGPROF")%>'>
	<input type='hidden' name='ID_VALID' value='<%=request("ID_VALID")%>'>
	<input type='hidden' name='DENOMINAZIONE' value='<%=request("DENOMINAZIONE")%>'>
	<input type='hidden' name='AREAPROF' value='<%=request("AREAPROF")%>'>
	<input type='hidden' name='BREVE_DESC' value='<%=request("BREVE_DESC")%>'>
	<input type='hidden' name='URL' value=''>
	<center>
<%

	if IsArray(RESULT) then
		Response.Write "<input type='hidden' name='limite' value='" & Ubound(RESULT,2) & "'>"
		Pag= cint(RC) mod 20
		if Pag > 0 then
			Pagine= 0 + int(cint(RC)/20)+1
		else
			Pagine = 0 + int(cint(RC)/20)
		end if
		
		Response.Write "<CENTER>"

		Response.Write "<table width='500' border=0 cellspacing=1 CELLPADDING=1>"
			Response.Write "<tr class=sfondocomm> "
				Response.Write "<td valign=top  align='left' colspan='2'>"
				Response.write "Lista  " & ucase (V2) & " no asociadas a la FIGURA PROFESIONAL:	<br><b> " & ucase(request("DENOMINAZIONE")) & "</b>"
				Response.Write "</td>"
			Response.Write "</tr>"
			Response.Write "<tr class=sfondocomm> "
				Response.Write "<td valign=top colspan='2' align='right'>"
				Response.write "<b>Paginas: " & Pagine & "</b>&nbsp;&nbsp;&nbsp;Nro. Elementos : " & RC  & "<small></b>"
				Response.Write "</td>"
			Response.Write "</tr>"
			Response.Write "<tr class=sfondocomm>"
				Response.write "<td align='left' colspan='2'><small>"
				Response.Write "<small>Criterio de b�squeda <br>"
				
				if trim(request("testo"))<>"" then 
					Response.Write "Texto: <b>" & request("testo") & "</b><br>"
				end if 
				
				If request("TitoloArea") <> "" then
					Response.Write "AREA:<b> " & request("TitoloArea") & "</small>"
				end if
				
				Response.Write "</td>"
			Response.Write "</tr>"
			Response.Write "<tr class=sfondocomm>"
			
				Response.write "<td align='right' colspan='2'><b>Opciones&nbsp;</b></td>"
			Response.Write "</tr>"
		Response.Write "</table>"
	      
		Response.Write "<table width='500' border=0 cellspacing=1 CELLPADDING=1>"
			Response.Write "<tr>"
			Response.Write "<td>&nbsp;</td>"
			Response.Write "<td class=tbltext1><b>Denominaci�n</b></td>"
				if request("settori") <> "" then
					Response.Write "<td class=tbltext1><b>Si - No</b></td>"
				else
					Response.Write "<td  align='right' class=tbltext1><b>&nbsp;Nivel requerido<br>0 &nbsp;&nbsp;1 &nbsp;&nbsp;&nbsp;2 &nbsp;&nbsp;&nbsp;3 &nbsp;&nbsp;&nbsp;4&nbsp;&nbsp;&nbsp;&nbsp;</b></td>"
				end if
			Response.Write "</tr>"
		
			for I = lbound(RESULT,2) to Ubound(RESULT,2)
				Response.Write "<input  type='hidden' name='ID_" & SaveAs & I & "' value='" & RESULT(1,I) & "'>"
				if (i mod 2)= 0 then 
				  sfondo="class='tblsfondo'" 
				else
				  sfondo="class='tbltext1'"
				end if
			
				Response.Write "<tr " & sfondo & ">"
				Response.Write "<td " & bg & " valign=middle align=center class=tbltext1><b>"
				    if Puls = "comportamientos" then
				       Response.Write "&nbsp;&nbsp;&nbsp;<a href='javascript: FocusComportamenti(" & RESULT(1,I) & ")' title='Visualizar detalle'>"
					end if
				    if Puls="competencias" then
					   Response.Write "<a href='javascript: FocusCompet(" & RESULT(1,I) & ")' title='Visualizar detalle'>"
				    end if
		
					Response.Write "<small>" & I + 1 + lDwn & "</a>&nbsp;</b></small></td>"
					Response.Write "<td class=tbltext1> <small>" & RESULT(0,I) & "</small></td>"
					if request("settori") <> "" then
						Response.Write "<td class=tbltext1 align='right'>"
							Response.Write "<input type='radio' name='radio" & I & "' value='1'>"
							Response.Write "<input type='radio' name='radio" & I & "' value='0' checked>"
						Response.Write "</td>"
					else
						Response.Write "<td class=tbltext1 align='right'>"
							Response.Write "<input type='radio' name='radio" & I & "' value='0' checked>"
							Response.Write "<input type='radio' name='radio" & I & "' value='1'>"
							Response.Write "<input type='radio' name='radio" & I & "' value='2'>"
							Response.Write "<input type='radio' name='radio" & I & "' value='3'>"
							Response.Write "<input type='radio' name='radio" & I & "' value='4'>"
						Response.Write "</td>"
					end if
				Response.Write "</tr>"
			next
		Response.Write "</table>"
	
		Response.Write "<table width='500' border=0 cellspacing=1 CELLPADDING=1>"
		Response.Write "<tr><br>"
			Response.Write "<td>"
			Response.Write "<input type='button' value='Cerrar' onClick='goToPage(""DettFigProf.asp?" & QS & """)' CLASS='My'>"
			Response.Write "</td>"
			Response.Write "<td colspan='1' align=center class=tbltext1>"
				if lDwn >= 1 then
					Response.Write "<input type='button' value='Atr�s' OnClick='mandaIndietro(" & lDwn & ")' Class='My'>"
				else
					Response.Write "<input type='button' value='Atr�s' OnClick='' Class='My' disabled>"
				end if

				Response.Write "<b>&nbsp;Pag: " & cint((lDwn + 20)/20) & "&nbsp;&nbsp;de " & Pagine & "&nbsp;</b>"
				if I + 1 +lDwn <= cint(RC) then
					Response.Write "<input type='button' value='Adelante' OnClick='mandaAvanti(" & lDwn & ")' Class='My'>"
				else
					Response.Write "<input type='button' value='Adelante' OnClick='' Class='My' disabled>"
				end if
			Response.Write "</td>"
			Response.Write "<td colspan='1' align='right'>"
				Response.Write "<input type='Submit' name='" & SaveAs & "' value='Guardar' CLASS='My'>"
			Response.Write "</td>"
		Response.Write "</tr>"
			Response.write "<tr>"
			Response.write "<td colspan=3 bgcolor='#3399CC'></td>"
		Response.write "</tr>"
		Response.Write "</table>"
	end if
%>
</form>
<!-- #include Virtual="/strutt_coda2.asp" -->
