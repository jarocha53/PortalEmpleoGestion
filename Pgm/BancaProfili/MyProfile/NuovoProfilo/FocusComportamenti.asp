<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #INCLUDE FILE="../Utils.asp" -->
<html>
<head>
<LINK REL=STYLESHEET TYPE="text/css" HREF='<%=session("progetto")%>/fogliostile.css'>
</head>
<body class=sfondocentro topmargin="10" leftmargin="0">
<title>Detalle del Comportamiento</title>

<%
	set Conn = server.CreateObject ("ADODB.Connection")
	Conn.Open strConn
	
	Sql = ""
    Sql = Sql &  " SELECT "
    Sql = Sql &  "    FIG.DENOMINAZIONE, COM.GRADO_FP_COMPOR, VAL.FL_VALID"
    Sql = Sql &  " FROM "
    Sql = Sql &  "   COMPOR_FP COM, "
	Sql = Sql &  "   FIGUREPROFESSIONALI FIG, "
	Sql = Sql &  "   VALIDAZIONE VAL"
	Sql = Sql &  " WHERE "
	Sql = Sql &  " 	   COM.ID_FIGPROF = FIG.ID_FIGPROF AND "
	Sql = Sql &  " 	   FIG.ID_VALID = VAL.ID_VALID  AND"
	Sql = Sql &  " 	   VAL.FL_VALID = 1 AND"
	Sql = Sql &  " 	   COM.ID_COMPORTAMENTO=" & REQUEST("id_comportamento") 

'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
	Set Rs = Conn.Execute (Sql)
	if not rs.eof then ASSOCIAZIONE= Rs.getrows()
	
	
	Sql = ""
	Sql = Sql & " SELECT "
	Sql = Sql & "	COMPORTAMENTI.DENOMINAZIONE, COMPORTAMENTI.NOTA, "
	Sql = Sql & "	AREA_COMPORTAMENTI.DENOMINAZIONE  "
	Sql = Sql & " FROM COMPORTAMENTI, AREA_COMPORTAMENTI "
	Sql = Sql & " WHERE "
	Sql = Sql & " COMPORTAMENTI.ID_AREACOMPORTAMENTO = AREA_COMPORTAMENTI.ID_AREACOMPORTAMENTO "
	Sql = Sql & " AND COMPORTAMENTI.ID_COMPORTAMENTO = " & request("ID_COMPORTAMENTO")

	
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
	Set Rs = Conn.Execute (Sql)
if not Rs.eof then
%>
<SCRIPT LANGUAGE="JavaScript">
function stp()
{
	bb=(document.layers)?document.layers['aa']:(document.getElementById)?document.getElementById('aa').style:document.all['aa'].style;
	bb.visibility="hidden"
	self.print();
	bb.visibility="visible"
}
</SCRIPT>
	
<center>
<table border=0 width='500' CELLPADDING=0 cellspacing=0>
	<tr>
		<td class="tbltext0" align=left bgcolor='#3399CC' bordercolor="#C2E0FF" width='199'>
			<b>DETALLE DEL COMPORTAMIENTO</b>
		</td>
		<td width="25" valign='bottom' background="<%=session("progetto")%>/images/sfondo_linguetta.gif" >
			<img border="0" src="<%=session("progetto")%>/images/tondo_linguetta.gif"></td>
		<td width="278" valign='bottom' background="<%=session("progetto")%>/images/sfondo_linguetta.gif" >&nbsp;</td>
	</tr>
</table>
	
	
<table border=0 width='500' CELLPADDING=0 cellspacing=0>
	<tr>
		<td align="left"  class="sfondocomm">	Esta visualizando el detalle del comportamiento:<b><br> <%=Rs(0)%></br>	</td>
		<td class=sfondocomm><a href="Javascript:Show_Help('<%=Session("Progetto")%>/HelpBP/MyProfile/NuovoProfilo/FocusComportamenti.htm')">
		 <img src="<%=Session("Progetto")%>/images/Help.gif" border='0'  align=right alt='Help'>
		</a>
		</td>							
	</tr>
	<tr>
		<td bgcolor='#3399CC' colspan='2'></td>
	</tr>
</table>
		
<form name='Conoscenze'>
<input type="hidden" name="IdPers" value="<%=sIdPers%>">
<input type="hidden" name="IdSedeAz" value="<%=IdSedeAz%>">
<input type="hidden" name="IND_FASE" value="<%=IND_FASE%>">
<table border=0 width='500' cellspacing=1 cellpadding=1>
	 <tr>
	 	<td class=tbltext1 align=LEFT><b>Denominación&nbsp;</B></td>
	 	<td class=tbltext1><%=Rs(0)%></td>
	 </tr>
	 <tr>
	 	<td class=tbltext1 align=LEFT><b>Area de Referencia&nbsp;</B></td>
	 	<td class=tbltext1><%=Rs(2)%></td>
	 </tr>
	 <tr>
	 	<td  class=tbltext1  align=LEFT><b>Nota&nbsp;</B></td>
	 	<td  class=tbltext1 ><%=Rs(1)%></td>
	 </tr>
</table>
</form>

<%
	Set RS=Nothing
	set CONN=Nothing
end if
	
	if IsArray(ASSOCIAZIONE) then
		Response.Write "<br>"
		Response.Write " <table border=0 width='500' CELLPADDING=1 cellspacing=1>"
		Response.Write " <tr class='sfondocomm'>"
		Response.Write " <td align='left'><b> Lista de figuras profesionales asociadas</b></td>"
		Response.Write " <td align='left'><b> Niv. Requerido</b></td>"
		Response.Write " </tr>"
	

		for J = lbound(ASSOCIAZIONE,2) to Ubound(ASSOCIAZIONE,2)
			Response.Write "<tr>"
				Response.Write "<td class='tbltext1'>" & ASSOCIAZIONE(0,J) & "</td>"
				Response.Write "<td align=center class='tbltext1'>" & ASSOCIAZIONE(1,J) & "</td>"
			Response.Write "</tr>"
		next

		Response.Write "</table>"
	end if
%>
<br>
<table border=0 width='500' CELLPADDING=0 cellspacing=0>
	 <tr>
	 		<td align="center"><div id="aa">
	 		<input type='button' value='Cerrar' OnClick='top.close()' CLASS='My' >
	 		<input type='button' value='Imprimir' OnClick='stp()' CLASS='My' >
	 		</div></td>
    </tr>
	<tr>
	 <td colspan=3 bgcolor='#3399CC'></td>
	</tr>
</table>
</body>
</html>
