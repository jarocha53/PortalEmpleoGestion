<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #INCLUDE FILE="../Utils.asp" -->
<%

Set Conn = server.CreateObject ("ADODB.Connection")
Conn.open strConn
OraDate=ConvDateToDB(Now())

Conn.BeginTrans
for x = 0 to request("limite")
	if request("radio" & x) <> 0 then
		'generazione identificativo di validazione da utilizzare per l'associazione 
		LstCampi = "ID_PERS_INS,ID_PERS_LAV,FL_VALID,DT_TMST"

		LstVal = ""
		LstVal = LstVal & IDSedeAz & "|"
		LstVal = LstVal & IDSedeAz & "|"
		LstVal = LstVal & "0" & "|"
		LstVal = LstVal & OraDate
		Sql = ""
		Sql = QryIns("VALIDAZIONE",LstCampi,LstVal)
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
		Conn.Execute Sql

		Sql = ""
		Sql = "SELECT Max(ID_VALID) FROM VALIDAZIONE WHERE ID_PERS_INS = " & IdSedeAz
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
		Rs = Conn.Execute(Sql)
		ID_VALID = Rs(0)

	set Rs = Nothing
		
		
		if request("competenze") <> "" then

			LstCampi = "ID_FIGPROF,ID_COMPETENZA,GRADO_FP_COMP,ID_VALID"
			LstVal = ""
			LstVal = LstVal  & request("ID_FIGPROF")  & "|"
			LstVal = LstVal  & request("ID_competenze" & x) & "|"
			LstVal = LstVal  & request("radio" & x) & "|"
			LstVal = LstVal  & ID_VALID

			Sql = ""
			Sql = QryIns("COMPET_FP",LstCampi,LstVal)
			
			Tab_COMPET_FP = true
		end if

		if request("comportamenti") <> "" then

			LstCampi = "ID_FIGPROF,ID_COMPORTAMENTO,GRADO_FP_COMPOR,ID_VALID"

			LstVal = ""
			LstVal = LstVal  & request("ID_FIGPROF")  & "|"
			LstVal = LstVal  & request("ID_comportamenti" & x) & "|"
			LstVal = LstVal  & request("radio" & x) & "|"
			LstVal = LstVal  & ID_VALID

			Sql = ""
			Sql = QryIns("COMPOR_FP",LstCampi,LstVal)
			Tab_COMPOR_FP = true

		end if

'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
		Conn.Execute Sql
	end if
next

if 	Tab_COMPET_FP then
	LogTrace ucase(mid(session("progetto"),2)),IdSedeAz, "S",IdSedeAz,"COMPET_FP", "" & IDSedeAz &"", "INS"
end if 

if 	Tab_COMPOR_FP then
	LogTrace ucase(mid(session("progetto"),2)),IdSedeAz, "S",IdSedeAz,"COMPOR_FP", "" & IDSedeAz &"", "INS"
end if 

Conn.CommitTrans
Conn.Close
Set conn = Nothing

%>
<script language='Javascript'>
<!--
	goToPage("<%=trim(request("URL"))%>")
//-->
</script>
