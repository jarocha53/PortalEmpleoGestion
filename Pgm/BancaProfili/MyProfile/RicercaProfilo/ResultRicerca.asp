<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #include Virtual="/strutt_testa2.asp" -->
<!-- #INCLUDE FILE="../Utils.asp" -->
<%

if request("lDwn") = "" Or Int(request("lDwn")) < 0 then
	lDwn = 0
else
	lDwn = request("lDwn")
end if

if request("Figure Professionali") <> "" OR request("Comportamenti") <> "" OR request("Competenze") <> ""   then

Set Conn = server.CreateObject ("ADODB.Connection")
Conn.open strConn



if request("Comportamenti") <> "" then
	Tipologia="Comportamenti"
	
	Sql = ""
	
	if request("ID_AREA") <>"" then
		Sql = Sql & "SELECT distinct "
	else
		Sql = Sql & "SELECT "
	end if 
	
	Sql = Sql & "COMPORTAMENTI.DENOMINAZIONE, COMPORTAMENTI.ID_COMPORTAMENTO, VALIDAZIONE.FL_VALID "
	Sql = Sql & "FROM "
	Sql = Sql & "COMPORTAMENTI, VALIDAZIONE, AREA_COMPORTAMENTI "
	Sql = Sql & "WHERE "

	W= ""
	W = W & "AREA_COMPORTAMENTI.ID_AREACOMPORTAMENTO = COMPORTAMENTI.ID_AREACOMPORTAMENTO "
	W = W & "AND "
	W = W & "COMPORTAMENTI.ID_VALID = VALIDAZIONE.ID_VALID "
	W = W & "AND "
	W = W & " UPPER(COMPORTAMENTI.DENOMINAZIONE) LIKE '%" & trim(ucase(CleanXsql(request("testo")))) & "%' "
	if request("ID_AREA") <> "" then
		W = W & "AND "
		W = W & "COMPORTAMENTI.ID_AREACOMPORTAMENTO = " & request("ID_AREA")
	end if

	Sql = Sql & W & " ORDER BY COMPORTAMENTI.DENOMINAZIONE"

	SqlCount= ""
	SqlCount= SqlCount & " SELECT COUNT(COMPORTAMENTI.ID_COMPORTAMENTO) maxresult "
	SqlCount= SqlCount & "FROM COMPORTAMENTI, VALIDAZIONE, AREA_COMPORTAMENTI "
	SqlCount= SqlCount & "WHERE "
	SqlCount= SqlCount & W

	Puls = "Comportamenti"
	TestoMsg= ""
	TestoMsg=TestoMsg & "E� uno degli elementi della competenza (unitamente a conoscenze e capacit�)." 
	TestoMsg=TestoMsg & "Coniuga doti e attitudini personali con necessit� espresse dall�organizzazione e "
	TestoMsg=TestoMsg & "dalle persone clienti/utenti. Denota l�essere in grado di comunicare, operare, "
	TestoMsg=TestoMsg & "interagire, ecc. in coerenza con uno specifico contesto ambientale e "
	TestoMsg=TestoMsg & "organizzativo e con i suoi valori di riferimento. Attiene al (saper) essere.  "
	HelpLink = "ResultRicerca_2.htm"								

end if

if request("Competenze") <> "" then
	Tipologia="Competenze"

	Sql = ""
	
	if request("ID_AREA") <>"" then
		Sql = Sql & "SELECT distinct "
	else
		Sql = Sql & "SELECT "
	end if  
	Sql = Sql & " COMPETENZE.DENOMINAZIONE, COMPETENZE.ID_COMPETENZA, COMPETENZE.ID_VALID "
	Sql = Sql & " FROM COMPETENZE, VALIDAZIONE, COMPET_FP, FIGUREPROFESSIONALI "
	Sql = Sql & " WHERE "

	W = W & " COMPETENZE.ID_VALID = VALIDAZIONE.ID_VALID "
	W = W & " AND COMPET_FP.ID_COMPETENZA = COMPETENZE.ID_COMPETENZA "
	W = W & " AND COMPET_FP.ID_FIGPROF= FIGUREPROFESSIONALI.ID_FIGPROF"

	if request("ID_AREA") <>"" then
		W = W &  " AND FIGUREPROFESSIONALI.ID_AREAPROF = " & request("ID_AREA")
	end if
		W = W &  " AND UPPER(COMPETENZE.DENOMINAZIONE) LIKE '%" & trim(UCASE(CleanXsql(REQUEST("testo")))) & "%'"

	Sql = Sql & W & " ORDER BY COMPETENZE.DENOMINAZIONE"

	SqlCount = ""
	SqlCount = SqlCount & "SELECT count(COMPETENZE.ID_COMPETENZA) maxresult "
	SqlCount = SqlCount & "FROM COMPETENZE, VALIDAZIONE, COMPET_FP, FIGUREPROFESSIONALI "
	SqlCount = SqlCount & "WHERE "
	SqlCount = SqlCount & W

	Puls="Competenze"
	TestoMsg= ""
	TestoMsg=TestoMsg & "Si definisce competenza �l'essere in grado di integrare le conoscenze, le" 
    TestoMsg=TestoMsg & "capacit�, i valori/comportamenti che consentono di realizzare l'output di "
	TestoMsg=TestoMsg & "un'attivit� richiesta in una specifica situazione�. In sede di analisi di un" 
	TestoMsg=TestoMsg & "processo produttivo, la competenza qualifica l�operatore che � in grado di "
	TestoMsg=TestoMsg & "realizzare l'output di un'attivit� principale/intermedia (un output, quindi, che" 
	TestoMsg=TestoMsg & "pur essendo intermedio per il processo, ha una propria completezza, una propria "
	TestoMsg=TestoMsg & "autonomia, un proprio valore quotabile anche in termini economici. "
	HelpLink = "ResultRicerca_1.htm"
end if



if request("Figure Professionali") <> "" then
	Tipologia="Figure Professionali"

	Sql = ""
	Sql = Sql & "SELECT "
	Sql = Sql & " FIGUREPROFESSIONALI.DENOMINAZIONE, FIGUREPROFESSIONALI.ID_FIGPROF, VALIDAZIONE.FL_VALID "
	Sql = Sql & " FROM "
	Sql = Sql & " FIGUREPROFESSIONALI, VALIDAZIONE, AREE_PROFESSIONALI "

	if request("ID_SETT") <> "" then
		Sql = Sql & " ,SETTORI_FIGPROF "
	END IF

	Sql = Sql & " WHERE "

	W= ""
	W = W & " AREE_PROFESSIONALI.ID_AREAPROF = FIGUREPROFESSIONALI.ID_AREAPROF "
	W = W & " AND "
	W = W & " FIGUREPROFESSIONALI.ID_VALID = VALIDAZIONE.ID_VALID "
	W = W & " AND "
	W = W & " UPPER(FIGUREPROFESSIONALI.DENOMINAZIONE) LIKE '%" & trim(ucase(CleanXsql(request("testo")))) & "%' "
	W = W & " AND FIGUREPROFESSIONALI.IND_MAPPA='S' "	'Solo Figure Professionali Mappate
	
	if request("ID_AREA") <> "" then
		W = W & " AND "
		W = W & " FIGUREPROFESSIONALI.ID_AREAPROF = " & request("ID_AREA")
	end if

	if request("ID_SETT") <> "" then
		W = W & " AND "
		W = W & " SETTORI_FIGPROF.ID_SETTORE = " & request("ID_SETT")
		W = W & " AND "
		W = W & " SETTORI_FIGPROF.ID_FIGPROF = FIGUREPROFESSIONALI.ID_FIGPROF"
	end if
	
	
	
	Sql = Sql & W & " ORDER BY FIGUREPROFESSIONALI.DENOMINAZIONE "

	SqlCount = ""
	SqlCount = SqlCount & "SELECT count(FIGUREPROFESSIONALI.ID_FIGPROF) maxresult "
	SqlCount = SqlCount & "FROM FIGUREPROFESSIONALI, VALIDAZIONE, AREE_PROFESSIONALI "
	if request("ID_SETT") <> "" then
		SqlCount = SqlCount & " ,SETTORI_FIGPROF "
	END IF
	SqlCount = SqlCount & "WHERE "
	SqlCount = SqlCount & W

	Puls="Figure Professionali"
	TestoMsg= ""
	TestoMsg=TestoMsg & "In questa pagina � possibile visualizzare il risultato della ricerca Impostata."
	TestoMsg=TestoMsg & "Il sistema restituir� se presenti solo le Figure Professionali alle quali risultano associati elementi di competenza."
	HelpLink = "ResultRicerca.htm"

end if

	Set Rs = server.CreateObject ("ADODB.Recordset")
		Rs.Open Sql,Conn,3,3,1
		if not rs.eof then
			Rs.Move (lDwn)
			RESULT = Rs.getrows(20)
'PL-SQL * T-SQL  
SQLCOUNT = TransformPLSQLToTSQL (SQLCOUNT) 
			RsCount = Conn.Execute(SqlCount)
			RC = RsCount(0)
			Set RsCount = Nothing
		end if
	Conn.Close
	Set conn = Nothing

end if
%>
<script language='Javascript'>
<!--

	function mandaAvanti(lDwn)
	{
	A = lDwn + 20

	URL  =  "ResultRicerca.asp?"
	URL += "lDwn=" + A + "&"
	URL += 'testo=<%=CleanXsql(request("testo"))%>&'
	URL += 'ID_AREA=<%=request("ID_AREA")%>&'
	URL += 'ID_FIGPROF=<%=request("ID_FIGPROF")%>&'
	URL += 'TitoloArea=<%=request("TitoloArea")%>&'
	URL += '<%=Puls%>=<%=Puls%>'

	goToPage(URL)
	}

	function mandaIndietro(lDwn)
	{
	A = lDwn - 20

	URL =  "ResultRicerca.asp?"
	URL += "lDwn=" + A + "&"
	URL += 'testo=<%=CleanXsql(request("testo"))%>&'
	URL += 'ID_AREA=<%=request("ID_AREA")%>&'
	URL += 'ID_FIGPROF=<%=request("ID_FIGPROF")%>&'
	URL += 'TitoloArea=<%=request("TitoloArea")%>&'
	URL += '<%=Puls%>=<%=Puls%>'

	goToPage(URL)
	}

	function FocusCompet(ID_COMPETENZA)
	{
		URL = "FocusCompet.asp?"
		URL = URL + "IdSedeAz=" + <%=request("IdSedeAz")%> + "&ID_COMPETENZA=" + ID_COMPETENZA  + "&VISMENU=NO"
		opt = "address=no,status=no,width=600,height=480,top=150,left=35,scrollbars=yes"

		window.open (URL,"",opt)
	}


	function FocusFigProf(ID_FIGPROF)
	{
		URL = "FocusFigProf.asp?"
		URL = URL + "IdSedeAz=" + <%=request("IdSedeAz")%> + "&ID_FIGPROF=" + ID_FIGPROF + "&VISMENU=NO"

		opt = "address=no,status=no,width=660,height=570,top=150,left=35,scrollbars=yes"

		window.open (URL,"",opt)
	}


	function FocusComportamenti(ID_COMPORTAMENTO)
	{
		URL = "FocusComportamenti.asp?"
		URL = URL +  "IdSedeAz=" + <%=request("IdSedeAz")%> + "&ID_COMPORTAMENTO=" + ID_COMPORTAMENTO  + "&VISMENU=NO"

		opt = "address=no,status=no,width=600,height=480,top=150,left=35,scrollbars=yes"

		window.open (URL,"",opt)
	}
//-->
</script>


<center>

<table border=0 width='500' CELLPADDING=0 cellspacing=0>
	<tr>
		<td class=tbltext0  bgcolor='#3399CC' align=left bordercolor="#C2E0FF" width='350'>
			<b>RESULTADO DE LA BUSQUEDA: <%=ucase(Tipologia)%></b>
		</td>
	<td width="25" valign='bottom' background="<%=session("progetto")%>/images/sfondo_linguetta.gif" >
		<img border="0" src="<%=session("progetto")%>/images/tondo_linguetta.gif"></td>
	<td width="278" valign='bottom' background="<%=session("progetto")%>/images/sfondo_linguetta.gif" ></td>
	</tr>
	</table>

	<table border=0 width='500' CELLPADDING=0 cellspacing=0>
    <tr>
		<td align="left" class=sfondocomm >
En esta p�gina podra buscar en el diccionario de <B>Perfiles</B>. 	
		</td>
		<td class=sfondocomm>
		<a href="Javascript:Show_Help('<%=Session("Progetto")%>/HelpBP/MyProfile/RicercaProfilo/<%=HelpLink%>')">
		 <img src="<%=Session("Progetto")%>/images/Help.gif" border='0'  align=right alt='Help'>
		</a>
		</td>	
    </tr>
    <tr>
		<td colspan=2 bgcolor='#3399CC'></td>
    </tr>
</table>	
    

<form name='ListaItem' Method=POST Action='SalvaAssociaEle_Compet.asp'>
<input type="hidden" name="IdPers" value="<%=sIdPers%>">
<input type="hidden" name="IdSedeAz" value="<%=Request("IdSedeAz")%>">
<input type="hidden" name="COD_EJ" value="<%=COD_EJ%>">
<input type="hidden" name="BREVE_DESC" value="<%=BREVE_DESC%>">

<%
if request("lDwn") = "" Or Int(request("lDwn")) < 0 then
	lDwn = 0
else
	lDwn = request("lDwn")
end if

	if IsArray(RESULT) then
		Response.Write "<input type='hidden' name='limite' value='" & Ubound(RESULT,2) & "'>"


		Pag= cint(RC) mod 20
		if Pag> 0 then
			Pagine= 0 + int(cint(RC)/20)+1
		else
			Pagine = 0 + int(cint(RC)/20)
		end if

Response.Write "<table width='500' border=0 cellspacing=1 cellpadding=1>"
	Response.Write "<tr class='sfondocomm'>"
		Response.Write "<td valign=top width='50%' align='right' colspan='4'>"
		Response.write "<b>Pagine:</b> " & Pagine & "&nbsp;&nbsp;&nbsp;<b>Nr. Elementi: </b> " & RC  & "</b>"
		Response.Write "</td>"
		Response.Write "</tr>"
		Response.Write "<tr class=sfondocomm>"
			Response.write "<td align='left'><small>"
			Response.Write "<small>Criteri di Ricerca <br>"
			Response.Write "<b>" & Puls & "</b>"
			Response.Write "<br>"
			Response.Write "<b>Testo: " & request("testo") & "<br>AREA: " & request("TitoloArea") & "</small>"
			Response.Write "</td>"
		Response.Write "</tr>"	
Response.Write "</table>"
Response.Write "<table border=0 cellspacing=1 width='500'>"
	Response.Write "<tr>"
		Response.Write "<td>&nbsp;</td>"
		Response.Write "<td class='tbltext1'><b>Denominazione</b></td>"
				Response.Write "</tr>"
			for I = lbound(RESULT,2) to Ubound(RESULT,2)
			Response.Write "<input type='hidden' name='ID_" & SaveAs & I & "' value='" & RESULT(1,I) & "'>"
				
			if (i mod 2)= 0 then 
				sfondo="class='tblsfondo'" 
			else
				sfondo="class='tbltext1'"
			end if
				
				Response.Write "<tr " & sfondo & ">"
					Response.Write "<td align='center' class='tbltext1'><b>"

					if Tipologia="Competenze" then
						Response.Write "&nbsp;<a href='javascript: FocusCompet(" & RESULT(1,I) & ")' title='Visualizza Dettaglio Competenza'>"
					end if
					if Tipologia="Figure Professionali" then
						Response.Write "&nbsp;&nbsp;&nbsp;<a href='javascript: FocusFigProf(" & RESULT(1,I) & ")' title='Visualizza Dettaglio Figura Professionale'>"
					end if
					if Tipologia="Comportamenti" then
						Response.Write "&nbsp;&nbsp;&nbsp;<a href='javascript: FocusComportamenti(" & RESULT(1,I) & ")' title='Visualizza Dettaglio Comportamento'>"
					end if
					Response.write "" & I + 1 + lDwn & "</b><a>&nbsp;&nbsp;</td>"
					Response.Write "<td class='tbltext1'>" & RESULT(0,I) & "</td>"
					Response.Write "</tr>"
				next

			Response.Write "<tr>"
		
		%>

		<td	valign='bottom' class='tbltext1'><br>
			<input type='button' value='Cancelar' onClick="goToPage('CercaElementi.asp')" CLASS='My'>
		</td>
		
		<%
				
			Response.Write "<td colspan=2 align=center  class='tbltext1'><br>"
				if lDwn >= 0 then
					Response.Write "<input type='button' value='Volver' OnClick='mandaIndietro(" & lDwn & ")' Class='My'>"
				else
					Response.Write "<input type='button' value='Volver' OnClick='' Class='My' disabled>"
				end if

			Response.Write "<b>&nbsp;Pag: " & cint((lDwn + 20)/20) & "&nbsp;&nbsp;di " & Pagine & "&nbsp;</b>"
			if I + 1 +lDwn <= cint(RC) then
					Response.Write "<input type='button' value='Avanti' OnClick='mandaAvanti(" & lDwn & ")' Class='My'>"
				else
					Response.Write "<input type='button' value='Avanti' OnClick='' Class='My' disabled>"
				end if

			Response.Write "</td>"
			Response.Write "</tr>"
			Response.Write "</table>"
	else
		Response.Write "<table border=0 cellspacing=1 width=500>"
		Response.Write "<tr><td class=tbltext3 align=center><b>"
		Response.Write "No se encotraron elementos que satisfagan el criterio de la busqueda"
		Response.Write "</b></td>"
		Response.Write "</tr>"
		Response.Write "<tr>"
		Response.Write "<td>"
		Response.Write "<input type='button' value='Volver' onClick='goToPage(""CercaElementi.asp"")' CLASS='My'>"
		Response.Write "</td>"
		Response.Write "</tr>"
		Response.Write "<tr>"
		Response.Write "<td colspan=3 bgcolor='#3399cc'></td>"
        Response.Write "</tr>"
		Response.Write "</table>"
	end if

%>
</form>
</center>
<!-- #include Virtual="/strutt_coda2.asp" -->
