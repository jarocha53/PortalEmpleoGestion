<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #INCLUDE FILE="../Utils.asp" -->
<%
function ID_VALID

Dim LstVal, LstCampi, Sql

	'Creazione di un nuovo record di validazione
	LstCampi = "ID_PERS_INS,ID_PERS_LAV,FL_VALID,DT_TMST"

	LstVal = ""
	LstVal = LstVal & IDSedeAz & "|"
	LstVal = LstVal & IDSedeAz & "|"
	LstVal = LstVal & "0" & "|"
	LstVal = LstVal & OraDate

	Sql = ""
	Sql = QryIns("VALIDAZIONE",LstCampi,LstVal)
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
	Conn.Execute Sql
	
	'Recupero del massimo ID di validaizone inserito
	Sql = ""
	Sql = "SELECT Max(ID_VALID) FROM VALIDAZIONE WHERE ID_PERS_INS = " & IDSedeAz
	
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
	Set Rs = Conn.Execute (Sql)
	If not Rs.EOF then
		ID_VALID = Rs(0)
	else
		ID_VALID = null
	end if
	set Rs = Nothing
end function


Set Conn = server.CreateObject ("ADODB.Connection")
Conn.open strConn
OraDate=ConvDateToDB(Now())

Conn.BeginTrans

	FigProf_ID_VALID = ID_VALID()

	'Inserimento informazioni sulla nuova Fig Prof
	LstCampi = "DENOMINAZIONE,ID_AREAPROF,DESCRIZIONE,BREVE_DESC,IND_MAPPA,ID_VALID,COD_EJ"

	LstVal = ""
	LstVal = LstVal & "'" & trim(CleanXsql(request("DENOMINAZIONE"))) & "'" & "|"
	LstVal = LstVal & request("ID_AREAPROF") & "|"
	LstVal = LstVal & "'" & TRIM(CleanXsql(request("DESCRIZIONE"))) & "'" & "|"
	LstVal = LstVal & "'" & TRIM(CleanXsql(request("BREVE_DESC"))) & "'" & "|"
	LstVal = LstVal & "'" & request("IND_MAPPA") & "'" & "|"
	LstVal = LstVal & FigProf_ID_VALID & "|"
	LstVal = LstVal & "'" & request("COD_EJ") & "'"

	Sql = ""
	Sql = QryIns("FIGUREPROFESSIONALI",LstCampi,LstVal)

'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
	Conn.Execute Sql

	' Recupero l'ID della fig. Profess. appena inserita
	set rs=nothing
	Sql=""
	Sql= "SELECT ID_FIGPROF FROM FIGUREPROFESSIONALI WHERE ID_VALID=" & FigProf_ID_VALID
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
	Rs = Conn.Execute(Sql)
	ID_FIGPROF = Rs(0)
	set Rs = Nothing
	
'#### Inizio salvataggio Dati da associare


'### Associazione all'Azienda che lo sta inserendo 

		LstCampi = "ID_FIGPROF,ID_SEDE, DT_TMST"

		LstVal = ""
		LstVal = LstVal & ID_FIGPROF  & "|"
		LstVal = LstVal & IDSedeAz & "|"
		LstVal = LstVal & OraDate 

		Sql = ""
		Sql = QryIns("IMPRESA_FP",LstCampi,LstVal)
		
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
		Conn.Execute Sql
'#### Fine associazione azienda 

'Associazione automatica PROFILO - PROGETTO

	Sql=""
	Sql= "INSERT INTO FIGPROF_PRJ (ID_FIGPROF,COD_PRJ) VALUES(" & ID_FIGPROF & ",'" & Session("Progetto") & "')"

'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
	Rs = Conn.Execute(Sql)
'### Fine associazione

lst_ID_COMPETENZA       = split(request("lst_ID_COMPETENZA"),"~")
lst_ID_COMPORTAMENTO    = split(request("lst_ID_COMPORTAMENTO"),"~")
lst_ID_SETTORE          = split(request("lst_ID_SETTORE"),"#")

if Ubound(lst_ID_COMPETENZA) > 0 then
	for x = lbound(lst_ID_COMPETENZA) to Ubound(lst_ID_COMPETENZA)-1

		Valori = split(lst_ID_COMPETENZA(x),"#")

		LstCampi = "ID_FIGPROF,ID_COMPETENZA,GRADO_FP_COMP,ID_VALID"
		
		LstVal = ""
		LstVal = LstVal & ID_FIGPROF  & "|"
		LstVal = LstVal & Valori(0) & "|"
		LstVal = LstVal & Valori(1) & "|"
		LstVal = LstVal & ID_VALID()

		Sql = ""
		Sql = QryIns("COMPET_FP",LstCampi,LstVal)
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
		Conn.Execute Sql
		
next
   LogTrace ucase(mid(session("progetto"),2)),IDSedeAz, "S",IDSedeAz,"COMPET_FP", "" & IDSedeAz &"", "INS"
end if

if Ubound(lst_ID_SETTORE) > 0 then
	for x = lbound(lst_ID_SETTORE) to Ubound(lst_ID_SETTORE)-1
		LstCampi = "ID_FIGPROF,ID_SETTORE,ID_VALID"

		LstVal = ""
		LstVal = LstVal & ID_FIGPROF  & "|"
		LstVal = LstVal & lst_ID_SETTORE(x) & "|"
		LstVal = LstVal & ID_VALID()

		Sql = ""
		Sql = QryIns("SETTORI_FIGPROF",LstCampi,LstVal)
		
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
		Conn.Execute Sql
next
	LogTrace ucase(mid(session("progetto"),2)),IDSedeAz, "S",IDSedeAz,"SETTORI_FIGPROF", "" & IDSedeAz &"", "INS"
end if

if Ubound(lst_ID_COMPORTAMENTO) > 0 then
for x = lbound(lst_ID_COMPORTAMENTO) to Ubound(lst_ID_COMPORTAMENTO)-1

		Valori = split(lst_ID_COMPORTAMENTO(x),"#")

		LstCampi = "ID_FIGPROF,ID_COMPORTAMENTO,GRADO_FP_COMPOR,ID_VALID"

		LstVal = ""
		LstVal = LstVal  & ID_FIGPROF & "|"
		LstVal = LstVal  & Valori(0) & "|"
		LstVal = LstVal  & Valori(1) & "|"
		LstVal = LstVal & ID_VALID()

		Sql = ""
		Sql = QryIns("COMPOR_FP",LstCampi,LstVal)
	
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
		Conn.Execute Sql
next
     LogTrace ucase(mid(session("progetto"),2)),IDSedeAz, "S",IDSedeAz,"COMPOR_FP", "" & IDSedeAz &"", "INS"
end if

LogTrace ucase(mid(session("progetto"),2)),IDSedeAz, "S",IDSedeAz,"FIGUREPROFESSIONALI", "" & IDSedeAz &"", "INS"

Conn.CommitTrans
Conn.Close
Set conn = Nothing
%>
<script>
<!--
	top.close()
//-->
</script>
