<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #INCLUDE FILE="../Utils.asp" -->
<title>Detalle Figura Profesional</title>
<script language='Javascript'>
<!--
	self.resizeTo (600,480)
//-->
</script>

<LINK REL=STYLESHEET TYPE="text/css" HREF='<%=session("progetto")%>/fogliostile.css'>
<body class=sfondocentro topmargin="10" leftmargin="0">

<%


Set Conn = server.CreateObject ("ADODB.Connection")
Conn.open strConn

	Sql = ""
	Sql = Sql & "SELECT "
	Sql = Sql & "COMPETENZE.DENOMINAZIONE, COMPETENZE.ID_COMPETENZA, COMPET_FP.GRADO_FP_COMP "
	Sql = Sql & "FROM "
	Sql = Sql & "COMPETENZE, COMPET_FP "
	Sql = Sql & "WHERE "
	Sql = Sql & "COMPETENZE.ID_COMPETENZA = COMPET_FP.ID_COMPETENZA "
	Sql = Sql & "AND "
	Sql = Sql & "COMPET_FP.ID_FIGPROF = " & request("ID_FIGPROF")

'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
	Set Rs = Conn.Execute (Sql)
	if not rs.eof then COMPETENZE = Rs.getrows()

	Sql = ""
	Sql = Sql & "SELECT "
	Sql = Sql & "CONOSCENZE.DENOMINAZIONE, COMPET_CONOSC.ID_COMPETENZA, COMPET_CONOSC.GRADO_COMP_CON "
	Sql = Sql & "FROM "
	Sql = Sql & "CONOSCENZE, COMPET_CONOSC, COMPET_FP "
	Sql = Sql & "WHERE "
	Sql = Sql & "CONOSCENZE.ID_CONOSCENZA = COMPET_CONOSC.ID_CONOSCENZA "
	Sql = Sql & "AND "
	Sql = Sql & "COMPET_CONOSC.ID_COMPETENZA = COMPET_FP.ID_COMPETENZA "
	Sql = Sql & "AND "
	Sql = Sql & "COMPET_FP.ID_FIGPROF = " & request("ID_FIGPROF")

'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
	Set Rs = Conn.Execute (Sql)
	if not rs.eof then CONOSCENZE = Rs.getrows()

	Sql = ""
	Sql = Sql & "SELECT "
	Sql = Sql & "CAPACITA.DENOMINAZIONE, COMPET_CAPAC.ID_COMPETENZA, COMPET_CAPAC.GRADO_COMP_CAP "
	Sql = Sql & "FROM "
	Sql = Sql & "CAPACITA, COMPET_CAPAC, COMPET_FP "
	Sql = Sql & "WHERE "
	Sql = Sql & "CAPACITA.ID_CAPACITA = COMPET_CAPAC.ID_CAPACITA "
	Sql = Sql & "AND "
	Sql = Sql & "COMPET_CAPAC.ID_COMPETENZA = COMPET_FP.ID_COMPETENZA "
	Sql = Sql & "AND "
	Sql = Sql & "COMPET_FP.ID_FIGPROF = " & request("ID_FIGPROF")

'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
	Set Rs = Conn.Execute (Sql)
	if not rs.eof then CAPACITA = Rs.getrows()

	Sql = ""
	Sql = Sql & "SELECT "
	Sql = Sql & "SETTORI.DENOMINAZIONE, SETTORI.ID_SETTORE, "
	Sql = Sql & "VALIDAZIONE.ID_VALID, VALIDAZIONE.FL_VALID, VALIDAZIONE.ID_PERS_INS "
	Sql = Sql & "FROM "
	Sql = Sql & "SETTORI, SETTORI_FIGPROF, VALIDAZIONE "
	Sql = Sql & "WHERE "
	Sql = Sql & "SETTORI_FIGPROF.ID_VALID = VALIDAZIONE.ID_VALID "
	Sql = Sql & "AND "
	Sql = Sql & "SETTORI.ID_SETTORE = SETTORI_FIGPROF.ID_SETTORE "
	Sql = Sql & "AND "
	Sql = Sql & "SETTORI_FIGPROF.ID_FIGPROF = " & request("ID_FIGPROF")

'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
	Set Rs = Conn.Execute (Sql)
	if not rs.eof then SETTORI = Rs.getrows()


	Sql = ""
	Sql = Sql & "SELECT "
	Sql = Sql & "COMPORTAMENTI.DENOMINAZIONE, COMPORTAMENTI.ID_COMPORTAMENTO, "
	Sql = Sql & "VALIDAZIONE.ID_VALID, VALIDAZIONE.FL_VALID, VALIDAZIONE.ID_PERS_INS, COMPOR_FP.GRADO_FP_COMPOR "
	Sql = Sql & "FROM "
	Sql = Sql & "COMPORTAMENTI, COMPOR_FP, VALIDAZIONE "
	Sql = Sql & "WHERE "
	Sql = Sql & "COMPOR_FP.ID_VALID = VALIDAZIONE.ID_VALID "
	Sql = Sql & "AND "
	Sql = Sql & "COMPORTAMENTI.ID_COMPORTAMENTO = COMPOR_FP.ID_COMPORTAMENTO "
	Sql = Sql & "AND "
	Sql = Sql & "COMPOR_FP.ID_FIGPROF = " & request("ID_FIGPROF")

'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
	Set Rs = Conn.Execute (Sql)
	if not rs.eof then COMPORT = Rs.getrows()

	Sql = ""
	Sql = Sql & "SELECT "
	Sql = Sql & "FIGUREPROFESSIONALI.DENOMINAZIONE, AREE_PROFESSIONALI.DENOMINAZIONE, "
	Sql = Sql & "AREE_PROFESSIONALI.ID_AREAPROF, FIGUREPROFESSIONALI.COD_EJ, FIGUREPROFESSIONALI.IND_MAPPA, "
	Sql = Sql & "FIGUREPROFESSIONALI.BREVE_DESC, CODICI_ISTAT.MANSIONE "
	Sql = Sql & "FROM "
	Sql = Sql & "FIGUREPROFESSIONALI, AREE_PROFESSIONALI, CODICI_ISTAT "
	Sql = Sql & "WHERE "
	Sql = Sql & "CODICI_ISTAT.CODICE (+) = FIGUREPROFESSIONALI.COD_EJ AND "
	Sql = Sql & "FIGUREPROFESSIONALI.ID_AREAPROF = AREE_PROFESSIONALI.ID_AREAPROF "
	Sql = Sql & "AND "
	Sql = Sql & "FIGUREPROFESSIONALI.ID_FIGPROF = " & request("ID_FIGPROF")
	
		
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
	Set Rs = Conn.Execute (Sql)

	if not rs.eof then
		Des_FigProf  = Rs(0)
		Des_AreaProf = Rs(1)
		Id_AreaProf  = Rs(2)
		COD_EJ       = Rs(3)
		IND_MAPPA    = Rs(4)
		BREVE_DESC   = Rs(5)
		MANSIONE     = Rs(6)
	END IF

set Rs = Nothing
Conn.Close
Set conn = Nothing

%>
<SCRIPT LANGUAGE="JavaScript">
function stp(){
	bb=(document.layers)?document.layers['aa']:(document.getElementById)?document.getElementById('aa').style:document.all['aa'].style;
	bb.visibility="hidden"
	cc=(document.layers)?document.layers['su']:(document.getElementById)?document.getElementById('su').style:document.all['su'].style;
	cc.visibility="hidden"

	self.print();
	bb.visibility="visible"
	cc.visibility="visible"
}
</SCRIPT>
<CENTER>

<table border=0 width='500' CELLPADDING=0 cellspacing=0>
	<tr>
		<td class=tbltext0  bgcolor='#3399CC' align=left bordercolor="#C2E0FF" width='300'>
			<b>DETALLE FIGURA PROFESIONAL</b>
		</td>
	<td width="25" valign='bottom' background="<%=session("progetto")%>/images/sfondo_linguetta.gif" >
		<img border="0" src="<%=session("progetto")%>/images/tondo_linguetta.gif"></td>
	<td width="278" valign='bottom' background="<%=session("progetto")%>/images/sfondo_linguetta.gif" ></td>
	</tr>
	
			</table>
	<table border=0 width='500' CELLPADDING=0 cellspacing=0>
    <tr class=sfondocomm>
		<td align="left" >
		Esta visualizando el detalle de la Figura Profesional:<b><br><%=Des_FigProf%></b>
		</td>
		<td class=sfondocomm><a href="Javascript:Show_Help('<%=Session("Progetto")%>/HelpBP/MyProfile/RicercaProfilo/FocusFigProf.htm')">
		 <img src="<%=Session("Progetto")%>/images/Help.gif" border='0'  align=right alt='Help'>
		</a>
		</td>	
	</tr>
    <tr>
		<td colspan=2 bgcolor='#3399CC'></td>
    </tr>
    <tr><td>&nbsp;</td></tr>
    <tr>
		<td colspan=3 bgcolor='#3399CC'></td>
    </tr>
</table>
<br>

<form name='indietro' Action='Duplica_figProf.asp?VISMENU=NO' method="POST">
	<div id="su">
		<input type="hidden" name="IdSedeAz" value="<%=IdSedeAz%>">
		<input type='button' value='Cancelar' OnClick='top.close()' CLASS='My'>
		<input type='button' value='Imprimir' OnClick='stp()' CLASS='My' >
		<input type='submit' value='Duplicar Perf�l' CLASS='My'>
	</div>

<%
	if IsArray(SETTORI) then
		Response.Write "<table border=0 width='500' cellspacing=1 cellpadding=1>"
		Response.Write "<tr class='sfondocomm'>"
			Response.Write "<td><b>Settori Merceologici</b></td>"
		Response.Write "</tr>"
		for I = lbound(SETTORI,2) to Ubound(SETTORI,2)
			Response.Write "<tr>"
				Response.Write "<td class='tbltext1'>" & SETTORI(0,I) & "</td>"
			Response.Write "</tr>"
		next
		Response.Write "</table>"
	end if

	Response.write "<br>"

	Response.Write "<table border=0 width='500' cellspacing=1 cellpadding=1>"
	Response.Write "<tr class='sfondocomm'>"
	Response.Write "<td><b>Area Professionale</b></td>"
	Response.Write "</tr>"
	Response.Write "<tr>"
	Response.Write "<td class='tbltext1'>" & Des_AreaProf & "</td>"
	Response.Write "</tr>"
	Response.Write "</table>"
%>
<br>
<%
	if IsArray(COMPETENZE) then
	Response.Write "<table border=0 cellspacing=1 width=500>"
	Response.Write "<tr class='sfondocomm'>"
	Response.Write "<td><b>Competenze</b></td>"
	Response.Write "<td><b>&nbsp;</b></td>"
	Response.Write "<td NOWRAP width='73'><b>Liv. Atteso</b></td>"
	Response.Write "</tr>"

	for i = lbound(COMPETENZE,2) to Ubound(COMPETENZE,2)
		Response.Write "<tr><td>&nbsp;</td></tr>"
		Response.Write "<tr>"
		Response.Write "<td  colspan='2' class='tbltext1'><b>&nbsp;&nbsp;" & COMPETENZE(0,I) & "</b></td>"
		Response.Write "<td align='center' class='tbltext1'><b>&nbsp;&nbsp;" & COMPETENZE(2,I) & "</b></td>"
		Response.Write "</tr>"

		Response.Write "<tr>"
		Response.Write "<td WIDTH='20%'>&nbsp;</td>"
		Response.Write "<td class='tbltext1'><b>Conoscenze</b></td>"
		Response.Write "</tr>"
		if isarray(CONOSCENZE) THEN
			for jj = lbound(CONOSCENZE,2) to Ubound(CONOSCENZE,2)
			if int(0 & COMPETENZE(1,I)) = int(0 & CONOSCENZE(1,jj)) then
				Response.Write "<tr>"
					Response.Write "<td>&nbsp;</td>"
					Response.Write "<td class='tbltext1'>" & CONOSCENZE(0,jj) & "</td>"
					Response.Write "<td align='center' class='tbltext1'><b>&nbsp;&nbsp;" & CONOSCENZE(2,jj) & "</b></td>"
				Response.Write "</tr>"
			end if
			next
		END IF

		Response.Write "<tr>"
		Response.Write "<td WIDTH='20%'>&nbsp;</td>"
		Response.Write "<td class='tbltext1'><b>Capacit�</b></td>"
		Response.Write "</tr>"
		IF ISARRAY(CAPACITA) THEN
			for jj = lbound(CAPACITA,2) to Ubound(CAPACITA,2)
			if int(0 & COMPETENZE(1,I)) = int(0 & CAPACITA(1,jj)) then
				Response.Write "<tr>"
					Response.Write "<td>&nbsp;</td>"
					Response.Write "<td class='tbltext1'>" & CAPACITA(0,jj) & "</td>"
					Response.Write "<td align='center' class='tbltext1'><b>&nbsp;&nbsp;" & CAPACITA(2,jj) & "</b></td>"
				Response.Write "</tr>"
			end if
			next
		END IF
	next
	Response.Write "</table>"
	end if
%>
<br>
<%
	if IsArray(COMPORT) then
	Response.Write "<table border=0 width='500' cellspacing=1 cellpadding=1>"
	Response.Write "<tr class='sfondocomm'>"
	Response.Write "<td><b>Comportamenti</b></td>"
	Response.Write "<td NOWRAP width='73'><b>Liv. Atteso</b></td>"
	Response.Write "</tr>"

		for I = lbound(COMPORT,2) to Ubound(COMPORT,2)
			Response.Write "<tr>"
				Response.Write "<td class='tbltext1'>&nbsp;&nbsp;" & COMPORT(0,I) & "</td>"
				Response.Write "<td align='center' class='tbltext1'><b>&nbsp;&nbsp;" & COMPORT(5,I) & "</b></td>"
			Response.Write "</tr>"
		next
		Response.Write "</table>"
	
	end if

if IsArray(COMPORT) then	
	for I = lbound(COMPORT,2) to Ubound(COMPORT,2)
		lst_ID_COMPORTAMENTO = lst_ID_COMPORTAMENTO & COMPORT(1,I) & "#" & COMPORT(5,I) & "~"
	next
end if
if IsArray(COMPETENZE) then
	for I = lbound(COMPETENZE,2) to Ubound(COMPETENZE,2)
		lst_ID_COMPETENZA = lst_ID_COMPETENZA & COMPETENZE(1,I) & "#" & COMPETENZE(2,I) & "~"
	next
end if
if IsArray(SETTORI) then
	for I = lbound(SETTORI,2) to Ubound(SETTORI,2)
		lst_ID_SETTORE = lst_ID_SETTORE & SETTORI(1,I) & "#"
	next
end if

QS = ""
QS = QS & "BREVE_DESC="   & request("BREVE_DESC")
%>
<div id="aa">
	<input type='button' value='Chiudi' OnClick='top.close()' CLASS='My'>
	<input type='button' value='Stampa' OnClick='stp()' CLASS='My'>
	<input type='submit' value='Duplica Profilo' CLASS='My'>
	<input type="hidden" value="<%=lst_ID_COMPORTAMENTO%>" name="lst_ID_COMPORTAMENTO">
	<input type="hidden" value="<%=lst_ID_COMPETENZA%>" name="lst_ID_COMPETENZA">
	<input type="hidden" value="<%=lst_ID_SETTORE%>" name="lst_ID_SETTORE">
	<input type="hidden" value="<%=Id_AreaProf%>" name="Id_AreaProf">
	<input type="hidden" value="<%=Des_AreaProf%>" name="Des_AreaProf">
	<input type="hidden" value="<%=COD_EJ%>" name="COD_EJ">
	<input type="hidden" value="<%=IND_MAPPA%>" name="IND_MAPPA">
	<input type="hidden" value="<%=BREVE_DESC%>" name="BREVE_DESC">
	<input type="hidden" value="<%=MANSIONE%>" name="MANSIONE">
</form>
</div>
<table width=500>
	<tr>
	<td colspan=3 bgcolor='#3399CC'></td>
	</tr>
</table>
