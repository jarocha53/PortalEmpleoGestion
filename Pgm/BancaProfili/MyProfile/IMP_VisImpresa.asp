<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include virtual="/strutt_testa2.asp"-->
<!--#include virtual ="/include/OpenConn.asp"-->
<!--#include virtual="/util/portallib.asp"-->
<!--#include virtual="/pgm/bancaprofili/StringConn.asp"-->
<!--#include virtual ="/include/ckProfile.asp"-->
<!--#include virtual ="/include/SelAreaTerrBandi.asp"-->
<%

	if ValidateService(session("idutente"),"IMP_VISIMPRESA",cc) <> "true" then 
		response.redirect "/util/error_login.asp"
	end if
	
%>
<script LANGUAGE="Javascript">
//include del file per fare i controlli sulla numericit� dei campi
<!--#include virtual = "/Include/ControlNum.inc"-->
<!--#include virtual="/Include/help.inc"-->
<!-- #Include Virtual="/Include/ControlString.inc" -->

function ControllaDati(frmVisImpresa){
	if (frmVisImpresa.txtPartIva.value.length > 0 ){ 
		if (!IsNum(frmVisImpresa.txtPartIva.value)){
			alert("El valor debe ser num�rico")
			frmVisImpresa.txtPartIva.focus() 
			return false
		}

		if (frmVisImpresa.txtPartIva.value.length != 11) {
			alert("La partida Iva debe ser de 11 caracteres")
			frmVisImpresa.txtPartIva.focus() 
			return false
		}
	}
	
	frmVisImpresa.txtCodFiscale.value=TRIM(frmVisImpresa.txtCodFiscale.value)
	if 	(frmVisImpresa.txtCodFiscale.value.length > 0 )	{
		if 	(!ValidateInputStringWithNumber(frmVisImpresa.txtCodFiscale.value))	{
			alert("Campo CUIT err�neo")
			frmVisImpresa.txtCodFiscale.focus();
			return false;
		}
		blank = " ";
		if (!ChechSingolChar(frmVisImpresa.txtCodFiscale.value,blank))	{
			alert("El campo CUIT no puede contener espacios en blanco")
			frmVisImpresa.txtCodFiscale.focus();
			return false;
		}
		if ((frmVisImpresa.txtCodFiscale.value.length != 11) && (frmVisImpresa.txtCodFiscale.value.length != 16)) {
			alert("El CUIT debe ser de 11 a 16 caracteres")
			frmVisImpresa.txtCodFiscale.focus(); 
			return false;
		}
	}	
	return true;
}

function userfocus()
{
document.frmVisImpresa.txtDescrizione.focus()
//document.forms(0).item("txtDescrizione").focus();
}
	
</script>

</head>



<body background="../../images/sfondo1.gif" onload="return userfocus()">
<%
	'on error resume next 
	dim CnConn
	dim rsAziende
	dim sSQL
	dim nCont
	dim sIsa
	dim sPrVolta
	dim sDescrizione
	dim sPartIva
	dim i
	dim nMaxElem
	dim sMask
	
	' Controllo i diritti dell'utente connesso.
	' La variabile di sessione mask � cos� formatta:
	' 1 Byte abilitazione funzione Impresa
	' 2 Byte abilitazione funzione Sede_impresa
	' 3 Byte abilitazione funzione Val_impresa
	' dal 4� byte in poi si visualizzano le provincie.
		
	sMask = Session("mask") 
	' Mi definisco le variabili Diritti
	'Session("Diritti")= mid(sMask,1,6)
	' Mi definisco le variabili Filtro
	'Session("Filtro")= mid(sMask,7)	

	'sMask = Session("Diritti")

	nMaxElem = 30 ' Numero massimo di imprese da visualizzare.
	
	sDescrizione= UCase(Request.Form ("txtDescrizione"))
	sDescrizione= Replace(sDescrizione,"'","''")

	sPartIva = Request.Form ("txtPartIva")
	sCodFiscale = ucase(Request("txtCodFiscale"))
	'sPrVolta = Request.QueryString("prVolta")
	sPrVolta = Request.Form("prVolta")
	'Response.Write sPrVolta & "     SIAMO QUI..........."
	
	if mid(Session("UserProfiles"),1,1) = "0" then
		sIsa = "0"
	else
		sIsa = mid(Session("UserProfiles"),1,2)
	end if

	sSetProv = SetProvUorg(Session("idUorg"))
	
	IF sSetProv="" THEN
%>
		<script>
			alert("No es posible acceder a la informaci�n territorial !")
<%	
			response.write "top.location.href='" & Session("Progetto") & "/default.asp';"		
%>			
		</script>
<%
		Response.End 
	end if
	
	nProvSedi = len(sSetProv)
	eleProv=""
	pos_ini = 1 	                
	for i=1 to nProvSedi/2
		sTarga = mid(sSetProv,pos_ini,2)
		eleProv= eleProv & "'" & sTarga & "',"															
		pos_ini = pos_ini + 2
	next
	lung =len(eleProv)-1
	sProv= mid(eleProv,1,lung)
	sCondizione = " AND SED.PRV in (" & sProv & ")"

%>

<br>
<table cellpadding="0" cellspacing="0" width="500" border="0">
   <tr height="18">
		<td class="sfondomenu" height="18" width="35%">
		<span class="tbltext0"><b>&nbsp;BUSQUEDA DE EMPRESAS</b></span></td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
		<td valign="middle" align="right" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1"></td>
   </tr>
   <tr width="371" class="SFONDOCOMMAZ">
   <td colspan="3">Indicar los datos para la busqueda.<a href="Javascript:Show_Help('../help/IMP_VisImpresa.htm')"><img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a></td>
	</tr>
	<tr height="2">
		<td colspan="3" class="SFONDOCOMMAZ" background="<%=Session("Progetto")%>/images/separazione.gif">
		</td>
	</tr>
</table><br>
<!--form method="post" name="frmVisImpresa" onsubmit="return ControllaDati(this)" action="IMP_VisImpresa.asp?prVolta=No&amp;VISMENU=NO"-->
<form method="post" name="frmVisImpresa" onsubmit="return ControllaDati(this)" action="IMP_VisImpresa.asp">


<table width="500" border="0" cellspacing="1" cellpadding="0">
		<tr align="left"> 
		<td nowrap width="75" class="tbltext1">
			<b>Raz�n Social</b>
		</td>
		
		
		<td nowrap width="200" align="middle" class="tbltext">
			<span class="size">
			<input style="TEXT-TRANSFORM: uppercase;WIDTH:  254px; HEIGHT: 22px" name="txtDescrizione" size="32" maxlength="470">
			</span>
		</td>
		
	<tr align="left">
		<td nowrap width="75" class="tbltext1"><b>Nro.Iva</b>
		</td>
		<td nowrap width="200" align="middle" class="tbltext">
			<span class="size">
			<input style="TEXT-TRANSFORM: uppercase; WIDTH: 254px; HEIGHT: 22px" name="txtPartIva" size="32" maxlength="11">
		</span>
		</td>
	</tr>
		<tr align="left">
			<td nowrap width="75" class="tbltext1">
				<b>CUIT</b>
			</td>
			<td nowrap width="200" align="middle" class="tbltext">
				<span class="size">
				<input style="TEXT-TRANSFORM: uppercase; WIDTH: 254px; HEIGHT: 22px" name="txtCodFiscale" size="32" maxlength="16">
				</span>
			</td>
		</tr>
</table>
<br>

<table cellpadding="0" cellspacing="0" width="300" border="0">	 
	<tr align="center">
	<td nowrap><input type="image" name="Conferma" src="<%=Session("progetto")%>/images/lente.gif"></td value="Registra">
	<td nowrap><a href="javascript:document.frmVisImpresa.reset()"><img border="0" src="<%=Session("progetto")%>/images/annulla.gif"></td>
	</tr>
</table>
<input type="hidden" name="prVolta" value="No">
</form>
	

<%
if sPrVolta = "No" then
	' Controlla la abilitazione all'inserimento
	%>
	
	<br>
	<table width="500" cellpadding="1" cellspacing="1" border="0">
			<tr height="20" class="SFONDOCOMMAZ"> 
				<td align="middle" valign="center" width="20" class="tbltext1"><b>&nbsp;Id Oficina</b></td>
				<td align="middle" valign="center" width="100" class="tbltext1"><b>Empresa</b></td>
				<td align="middle" valign="center" width="110" class="tbltext1"><b>Nro.Iva</b></td>
				<td align="middle" valign="center" width="110" class="tbltext1"><b>Cod.Fiscal</b></td>
				<td align="middle" valign="center" width="80" class="tbltext1"><b>Descripci�n</b></td>
				<td align="middle" valign="center" width="80" class="tbltext1"><b>Municipio</b></td>
			</tr>
	<%
		nCont = 0 
		
		'sSQL = "SELECT ID_IMPRESA,RAG_SOC,PART_IVA FROM IMPRESA WHERE ID_IMPRESA = ID_IMPRESA "	
		Sql = " SELECT IMPRESA.RAG_SOC, IMPRESA.PART_IVA, IMPRESA.COD_FISC, SED.ID_SEDE, SED.DESCRIZIONE, COMUNE.DESCOM"
		Sql = Sql & " FROM IMPRESA, SEDE_IMPRESA SED, COMUNE"
		Sql = Sql & " WHERE IMPRESA.ID_IMPRESA=SED.ID_IMPRESA" 
		Sql = Sql & " AND COMUNE.CODCOM=SED.COMUNE " & sCondizione
		
		if sDescrizione <> "" then
			Sql = Sql + " AND RAG_SOC Like '%" & sDescrizione & "%'"
		end if	
		
		if sPartIva <> "" then
			Sql = Sql + " AND PART_IVA='" & sPartIva & "'"
		end if	
		
		if sCodFiscale <> "" then
			Sql = Sql + " AND COD_FISC ='" & sCodFiscale & "'"
		end if
		
		'Response.Write Sql
		'Response.End 
		
		Set Conn = server.CreateObject ("ADODB.Connection")
		Conn.open strConn
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
		set rsAziende = Conn.Execute(SQL)
		'cicla fino alla fine del recor [do while not]  
		i = 0
		do while not rsAziende.EOF
			i = i +1				
			nCont = 1
	%> 
			<tr class="tblsfondo"> 
			  <td align="middle" width="100">
					<span class="tbltext1">
				<%
				' Controlla la abilitazione all'inserimento
					Response.write "<A class=""tbltext1"" HREF='Default.asp?IDSedeAz=" & _
						rsAziende("ID_SEDE") & "' ><b>" & _
						rsAziende("ID_SEDE") & "</b></A>"		
				%>
					</span>
				</td>
				<td align="middle" width="270">
					<span class="tbltext">
					<%Response.Write (rsAziende("RAG_SOC")) 'Ragione Sociale %>
					</span>
				</td>	
							
				<td width="130" align="middle">
					<span class="tbltext">
					<%Response.Write (rsAziende("PART_IVA")) 'Partita IVA %>
					</span>
				</td>
				
				<td width="130" align="middle">
					<span class="tbltext">
					<%Response.Write (rsAziende("COD_FISC")) 'Codice Fiscale %>
					</span>
				</td>
				<td width="130" align="middle">
					<span class="tbltext">
					<%Response.Write (rsAziende("DESCRIZIONE")) 'Descrizione %>
					</span>
				</td>				

				<td align="middle" width="270">
					<span class="tbltext">
					<%Response.Write (rsAziende("DESCOM")) 'Comune %>
					</span>
				</td>
				
			</tr>
	<% 

			if i => nMaxElem then
	%>
				</table>
				<table width="371" cellpadding="0" cellspacing="0" border="0">
					<tr align="middle">
						<td>
							<span class="tbltext1">
							<% 
								nCont = 1
								response.write ("<BR>Se encontraron m�s de " & nMaxElem & " recuerde modificar los par�metros de b�squeda")
							%>
							</span><br><br><br>
						</td>
					</tr>
				</table> 
	<%
				exit do
			end if
			rsAziende.MoveNext 
		Loop
		
		rsAziende.Close 
		set rsAziende = nothing 
	%> 
	</table>
	<br>

	<% 
		if nCont = 0 then
	%>
			<table width="371" cellpadding="0" cellspacing="0" border="0">
				<tr align="middle">
					<td>
						<span class="tbltext3">
						<% response.write ("<STRONG>No se especificaron empresas</STRONG>")%>
						</span>

					</td>
				</tr>
			</table> 

	<%
		end if
	end if
	set conn=nothing
%> 
<!--#include virtual ="/strutt_coda2.asp"-->

