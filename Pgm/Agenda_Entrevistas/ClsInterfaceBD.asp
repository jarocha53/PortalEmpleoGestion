<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#INCLUDE VIRTUAL= "/IncludeMtss/adovbs.asp"-->
<!--#INCLUDE VIRTUAL= "/Util/dbutil.asp"-->
<!--#INCLUDE VIRTUAL= "/Include/OpenConn.asp"-->

<%
class InterfaceBD
%>
<!--#INCLUDE VIRTUAL= "/IncludeMTSS/funcionesGenerales.asp"-->
<%
'dim conn
Public Function GenerarComboTipoMovimientosCuil(Nombre, Clase, Validacion, Seleccion, CUIL)
'=========================================================================================
' GENERA UN COMBO CON LOS TIPOS DE MOVIMIENTOS PERMITIDOS PARA ESE CUIL
'=========================================================================================
	Dim Rs
	Dim Sp
	Dim Retorna
	Dim Consulta

	'InicioBD()

	Set Rs = Server.CreateObject("adodb.recordset")
	Set Sp = Server.CreateObject("adodb.command")

	Sp.ActiveConnection = connLavoro 'InicioBD()

	Sp.CommandText = "AgCitaDefineTiposMovimientos('" & Cuil & "',,,'')"

'response.Write cuil

	Sp.CommandType = 4
'PL-SQL * T-SQL

	Set Rs = Sp.execute()

	Retorna = ""

'response.write "jhamon_Aqui_"

	If not Rs.eof then
	'response.write "jhamon_funciono_" 
		Retorna = Retorna & "<Select Name='" & Nombre & "' Class='" & Clase & "' OnChange=" & Validacion & ">"
		Retorna = Retorna & "<option></option>"
		
		Do while not Rs.eof
			If cstr(Rs("TipoMovimiento")) = cstr(Seleccion) then
				Seleccionado = "Selected"
			else
				Seleccionado = ""
			end if

			Retorna = Retorna & "<option value='" & Rs("TipoMovimiento") & "' " & Seleccionado & ">" & Rs("Descripcion") & "</option>"
			Rs.movenext
		Loop
		Retorna = Retorna & "</Select>"
	end if
	'response.write "jhamon_funciono pero no entro en el if_" 
	GenerarComboTipoMovimientosCuil = Retorna

	'FinBD()
	Set Rs = Nothing
End Function


Public Function GenerarComboTipoMovimientos(Nombre,Clase,Validacion,Seleccion,Agrupacion)
'=========================================================================================
' GENERA UN COMBO CON TODOS LOS TIPOS DE MOVIMIENTOS
'=========================================================================================
	Dim Rs
	Dim Consulta
	Dim Retorna

	'InicioBD()

	Set Rs = Server.CreateObject("adodb.recordset")

	Consulta = "Select TipoMovimiento, Descripcion from AgCitaMovimientosTipos where TIPOAGRUPADOR =" & cint(Agrupacion)

'PL-SQL * T-SQL
'CONSULTA = TransformPLSQLToTSQL (CONSULTA)
	Set Rs = connLavoro.execute(Consulta)

	Retorna = ""

	If not Rs.eof then
		Retorna = Retorna & "<Select Name='" & Nombre & "' Class='" & Clase & "' OnChange=" & Validacion & ">"
		Retorna = Retorna & "<option></option>"
		Do while not Rs.eof
			If cstr(Rs("TipoMovimiento")) = cstr(Seleccion) then
				Seleccionado = "Selected"
			else
				Seleccionado = ""
			end if

			Retorna = Retorna & "<option value='" & Rs("TipoMovimiento") & "' " & Seleccionado & ">" & Rs("Descripcion") & "</option>"
			Rs.movenext
		Loop
		Retorna = Retorna & "</Select>"
	end if
	GenerarComboTipoMovimientos = Retorna

'	FinBD()

	Set Rs = Nothing
End Function

Public Function GenerarComboTipoCitas(Nombre, Clase, Validacion, Seleccion)
'=========================================================================================
' GENERA UN COMBO CON LOS TIPOS DE CITAS
'=========================================================================================
	Dim Rs
	Dim Consulta
	Dim Retorna
'response.write "jhamon_Aqui_"
'	InicioBD()

	Set Rs = Server.CreateObject("adodb.recordset")

	Consulta = "Select CitaTipo, Descripcion from AgCitaTipos "

'PL-SQL * T-SQL
'CONSULTA = TransformPLSQLToTSQL (CONSULTA)
	Set Rs = connLavoro.execute(Consulta)

	Retorna = ""

	If not Rs.eof then
		Rs.movefirst
		Retorna = Retorna & "<Select Name='" & Nombre & "' Class='" & Clase & "' OnChange=" & Validacion & ">"
		Retorna = Retorna & "<option></option>"
		Do while not Rs.eof
			If cstr(Rs("CitaTipo")) = cstr(Seleccion) then
				Seleccionado = "Selected"
			else
				Seleccionado = ""
			end if
			Retorna = Retorna & "<option value='" & Rs("CitaTipo") & "' " & Seleccionado & ">" & Rs("Descripcion") & "</option>"
			Rs.movenext
		Loop
		Retorna = Retorna & "</Select>"
	end if

	GenerarComboTipoCitas = Retorna

'	FinBD()

	Set Rs = Nothing
End Function


Public Function GenerarComboEstados(Nombre,Clase,Validacion,Seleccion)
'=========================================================================================
' GENERA UN COMBO CON LOS TIPOS DE ESTADOS
'=========================================================================================
	Dim Rs
	Dim Consulta
	Dim Retorna

	'InicioBD()

	Set Rs = Server.CreateObject("adodb.recordset")

	Consulta = "Select Estado, Descripcion from AgCitaEstados where Estado not in (5,6,10)"

'PL-SQL * T-SQL
'CONSULTA = TransformPLSQLToTSQL (CONSULTA)
	Set Rs = connLavoro.execute(Consulta)

	Retorna = ""

	If not Rs.eof then
		Rs.movefirst
		Retorna = Retorna & "<Select Name='" & Nombre & "' Class='" & Clase & "' OnChange=" & Validacion & ">"
		Retorna = Retorna & "<option></option>"
		Do while not Rs.eof
			If cstr(Rs("Estado")) = cstr(Seleccion) then
				Seleccionado = "Selected"
			else
				Seleccionado = ""
			end if
			Retorna = Retorna & "<option value='" & Rs("Estado") & "' " & Seleccionado & ">" & Rs("Descripcion") & "</option>"
			Rs.movenext
		Loop
		Retorna = Retorna & "</Select>"
	end if

	GenerarComboEstados = Retorna

'	FinBD()

	Set Rs = Nothing
End Function

Public Function GenerarComboDuracionCita(Nombre,Clase,TipoCita,Validacion,Salto,MaxDur)
'=========================================================================================
' GENERA UN COMBO CON LAS DURACIONES PARA LA CITA, POR DEFAULT VIENE SELECCIONADO EL VALOR
' DE LA BASE
'=========================================================================================
	Dim Rs
	Dim Consulta
	Dim Retorna
	'MaxLong,Tamanio,
'	InicioBD()

	Set Rs = Server.CreateObject("adodb.recordset")

	Consulta = "Select Descripcion, Duracion from AgCitaTipos where CitaTipo =" & cint(TipoCita)

'PL-SQL * T-SQL
'CONSULTA = TransformPLSQLToTSQL (CONSULTA)
	Set Rs = connLavoro.execute(Consulta)

	Retorna = ""

	If not Rs.eof then
		Rs.movefirst
		Valor = Rs("Duracion")
		'Retorna = Retorna & "<input name='" & Nombre & "' type='text' Class='" & Clase & "' maxlength='" & MaxLong & "' size='" & Tamanio & "' value='" & Rs("Duracion") & "'>"
	end if

	Retorna = Retorna & "<select name='" & Nombre & "' onchange='" & Validacion & "' class='" & Clase & "'>"

	Seleccionado = ""

	for i = Salto to MaxDur step Salto
		if cint(i) = cint(Valor) then
			Seleccionado = "Selected"
		else
			Seleccionado = ""
		end if
		Retorna = Retorna & "<option value ='" & i & "' " & Seleccionado & ">" & i & " Min</option>"
	next

	Retorna = Retorna & "</select>"

	GenerarComboDuracionCita = Retorna

'	FinBD()

	Set Rs = Nothing
End Function

Public Function ObtenerCitaParaMovimiento(TipoMov)
'=========================================================================================
' OBTIENE EL TIPO DE CITA QUE ACEPTA ESE MOVIMIENTO
'=========================================================================================
	Dim Rs
	Dim Consulta
	Dim Retorna

'	InicioBD()

	Set Rs = Server.CreateObject("adodb.recordset")

	Consulta = "Select CitaTipo from AgCitaMovimientosTipos where TipoMovimiento =" & cint(TipoMov)


'PL-SQL * T-SQL
'CONSULTA = TransformPLSQLToTSQL (CONSULTA)
	Set Rs = connLavoro.execute(Consulta)

	Retorna = ""

	If not Rs.eof then
		Retorna = Rs("CitaTipo")
	end if

	ObtenerCitaParaMovimiento = Retorna

'	FinBD()

	Set Rs = Nothing
End Function

Public Function EmiteReporteEnPaginaASP(ASPActual, ComandoStore, AspVarQueryString)
'=========================================================================================
' Emite Un reporte en una pagina ASP con paginado
'
'Necesitan:
'	ASPactual		ASP que llamo a la funcion
'	ComandoStore	String con la lina de comando SQL armada,
'					puede ser un Store con todos sus parametros armados
'	AspVarQueryString	Varialbes para el query String que necesita mantener el ASPactual
'						Armadas de la forma "&Var1=Valor1&Var2=Valor2"
'Devuelve:
'	Armar la Tabla con datos en la pagina ASPactual
'	Maneja la paginacion de los datos
'=========================================================================================
	Dim SP, Rs, sDato
	Dim Retorna

	Retorna = ""

	'----------------------------------------
	' Si se llama a la pagina por el paginado de registros
	' recupero el comando SQL del Query string
	'----------------------------------------
	'..Response.Write "x" & ComandoStore & "x<br>"

	sDato = ""
	sDato = Request.QueryString("xCmdSql")
	if len(sDato)> 0 then
		ComandoStore = sDato
		'..Response.Write "*" & sDato & "*<br>"
		'..Response.end
	end if



	'----------------------------------------
	' Muestra la consulta
	'----------------------------------------
	Set Rs = Server.CreateObject("adodb.recordset")
	Set Sp = Server.CreateObject("adodb.command")

	Sp.ActiveConnection = connLavoro 'InicioBD()
	Sp.CommandText = ComandoStore
	Sp.CommandType = 4
'PL-SQL * T-SQL
	Set Rs = Sp.Execute()

	Retorna = ""

'''paginacion
	if rs.state = 1 then
		if not rs.eof then
			nActPagina	=Request("txtPagNum")
			Record=0
			nTamPagina=7	' Cantidad de registos a mostrar

			If nActPagina = "" Then
				nActPagina=1
			Else
				nActPagina=Clng(Request("txtPagNum"))
			End If

			rs.PageSize = nTamPagina
			rs.CacheSize = nTamPagina
			nTotPagina = rs.PageCount
			If nActPagina < 1 Then
				nActPagina = 1
			End If
			If nActPagina > nTotPagina Then
				nActPagina = nTotPagina
			End If

			rs.AbsolutePage=nActPagina
			nTotRecord=0

		'''paginacion
		'Retorna = Retorna & "<table bordercolor='MidnightBlue'	border='2' width='350'>"
			While rs.EOF <> True And nTotRecord < nTamPagina
				' Titulos
		  		if Record = 0 then
					Retorna = Retorna & "<tr>"
					for each x in Rs.fields
						Retorna = Retorna & "<td nowrap width = '25%' align='center' class='tbltext1' bgcolor='LightYellow'><b>" & x.name & "</b></td>"
					next
					cantcampos = Rs.fields.count
					Retorna = Retorna & "</tr>"
					record = 1
				end if

				' Datos
				Retorna = Retorna & "<tr>"
				for each x in Rs.fields
					ValorX = x.value
					Retorna = Retorna & "<td align='center' class='tbltext1' bgcolor='LightYellow'>" & ValorX & "</td>"
				next
				Retorna = Retorna & "</tr>"
			'Rs.movenext
		'loop
	'end if
				nTotRecord=nTotRecord + 1
				rs.MoveNext
			wend

		'Retorna = Retorna & "</table>"
		Retorna = Retorna & "<tr>"
		Retorna = Retorna & "<td colspan='" & cantcampos & "'>"

			Retorna = Retorna & "<table border=0 width=480>"
			Retorna = Retorna & "<tr>"

			' paginacion
			' paginacion
			if nActPagina > 1 then
				Retorna = Retorna & "<td align='right' width='480'>"
				Retorna = Retorna & "<a href=" & ASPActual & "?txtPagNum=" & (nActPagina - 1) & AspVarQueryString & "&xCmdSql=" & ComandoStore & "><img src='" & Session("Progetto") & "/images/precedente.gif' border='0' id='image1' name='image1'>"
				Retorna = Retorna & "</td>"
			end if
			if nActPagina < nTotPagina then
				Retorna = Retorna & "<td align='right'>"
				Retorna = Retorna & "<a href=" & ASPActual & "?txtPagNum=" & (nActPagina + 1) & AspVarQueryString & "&xCmdSql=" & ComandoStore & "><img src='" & Session("Progetto") & "/images/successivo.gif' border='0' id='image1' name='image1'>"
				Retorna = Retorna & "</td>"
			end if
			Retorna = Retorna & "</tr>"
			Retorna = Retorna & "</table>"

		Retorna = Retorna & "</td>"
		Retorna = Retorna & "</tr>"
		end if
	end if

	' cierre todo
	'Rs.close
	set Rs = nothing
	set Sp = nothing

	'---------------------------
	' salida
	'---------------------------
	EmiteReporteEnPaginaASP = Retorna

end function


Public Function MostrarDisponibilidad(ASPActual, Oficina, Fecha)
'=========================================================================================
' Reporte de Disponibilidad de diaria de puestos de trabajo de una Oficina
'
'Necesitan:
'	ASPactual		ASP que llamo a la funcion
'	Oficina			Oficina de trabajo del usuario
'	Fecha			Fecha de analisis de la disponibilidad
'Devuelve:
'	Armar la Tabla con datos en la pagina ASPactual
'	Maneja la paginacion de los datos
'
'=========================================================================================
	Dim Rs, Sp
	Dim Retorna
	Dim Opciones, Puesto, DuracionCita

	Retorna = ""

'	InicioBD()
	'----------------------------------------
	' Parametros de ejecucion del Store
	'----------------------------------------
	Oficina = Oficina
	Fecha = Fecha
	Opciones = 2	' 2: disponibilidad por puesto
	Puesto	= 0
	DuracionCita = 0

	ComandoStore = "AgAgendaEstadoConsultaDia(" & Oficina & ",'" & Fecha & "'," & Opciones & "," & Puesto & "," & DuracionCita & ",0,'')"

	'----------------------------------------
	' Varialbes que debe mantener (x QueryString)
	'----------------------------------------
	AspVarQueryString = "&Oficina=" & Oficina & "&TxtFecha=" & Fecha

	'----------------------------------------
	' Ejecuta la consulta
	'----------------------------------------
	MostrarDisponibilidad = EmiteReporteEnPaginaASP (ASPActual, ComandoStore, AspVarQueryString)

End Function



Public Function GenerarFilasPorEstado(ASPActual,Oficina,Fecha,Estado)
'=========================================================================================
' GENERA UNA TABLA CON LAS CITAS SEGUN EL ESTADO INDICADO
'=========================================================================================
	Dim Rs
	Dim Retorna
	Dim RsNom

	Retorna = ""

'	InicioBD()


	'Consulta = "Select A.NROCITA, A.OFICINA, A.FECHA, A.HORADESDE, A.HORAHASTA, B.DESCRIPCION as ESTADO, C.DESCRIPCION as [TIPO DE CITA], A.CUIL from AgCitas A, AgCitaEstados B, AgCitaTipos C where A.Estado = B.Estado and A.CitaTipo = C.CitaTipo and A.Fecha =" & ConvDateToDBsSQL(Fecha) & " and A.Estado = " & Estado & " and A.oficina  = " & Oficina & " ORDER BY HORADESDE ASC"
'<P class="tbltext1"><B>Estado: </B></P>

	Consulta = "Select A.NROCITA, A.HORADESDE, A.HORAHASTA, B.DESCRIPCION as ESTADO, C.DESCRIPCION as [TIPO DE CITA], A.CUIL from AgCitas A, AgCitaEstados B, AgCitaTipos C where A.Estado = B.Estado and A.CitaTipo = C.CitaTipo and A.Fecha =" & ConvDateToDBsSQL(Fecha) & " and A.Estado = " & Estado & " and A.oficina  = " & Oficina & " ORDER BY HORADESDE ASC"

	Set Rs = Server.CreateObject("adodb.recordset")
	Set RsNom = Server.CreateObject("adodb.recordset")

'PL-SQL * T-SQL
'CONSULTA = TransformPLSQLToTSQL (CONSULTA)
	Set Rs = connLavoro.execute(Consulta)

'''paginacion
	if rs.state = 1 then
		if not rs.eof then

			nActPagina	=Request("txtPagNum")

			Record=0
			nTamPagina=7

			If nActPagina = "" Then
				nActPagina=1
			Else
				nActPagina=Clng(Request("txtPagNum"))
			End If

			rs.PageSize = nTamPagina

			rs.CacheSize = nTamPagina

			nTotPagina = rs.PageCount

			If nActPagina < 1 Then
				nActPagina = 1
			End If

			If nActPagina > nTotPagina Then
				nActPagina = nTotPagina
			End If

			rs.AbsolutePage=nActPagina
			nTotRecord=0

		'''paginacion
			While rs.EOF <> True And nTotRecord < nTamPagina
		  		if Record = 0 then
					Retorna = Retorna & "<tr bgcolor='LightYellow'>"
					Retorna = Retorna & "<td align='center' class='tbltext1' colspan='6'><b><FONT SIZE='1'>ESTADO: " & UCASE(RS("ESTADO"))
					Retorna = Retorna & "</FONT></b></td>"
					Retorna = Retorna & "</tr>"

					Retorna = Retorna & "<tr>"
					for each x in Rs.fields
						if ucase(x.name) <> "ESTADO" Then
							if ucase(x.name) <> "HORADESDE" and ucase(x.name) <> "HORAHASTA" then
								Retorna = Retorna & "<td width = '25%' align='center' NOWRAP class='tbltext1' bgcolor='LightYellow'><b>" & x.name & "</b></td>"
							else
								if ucase(x.name) = "HORADESDE" then
									nom = "HORA DESDE"
								else
									nom = "HORA HASTA"
								end if

								Retorna = Retorna & "<td align='center' NOWRAP class='tbltext1' bgcolor='LightYellow'><b>" & nom & "</b></td>"
							end if
						end if
					next

					Retorna = Retorna & "<td align='center' NOWRAP class='tbltext1' bgcolor='LightYellow'><b>APELLIDO Y NOMBRE</b></td>"

					Retorna = Retorna & "</tr>"
					record = 1
				end if

		'do while not Rs.eof

				ConsNom = "Select cognome, nome from PERSONA where cod_fisc = '" & trim(Rs("CUIL")) & "'"

'PL-SQL * T-SQL
'CONSNOM = TransformPLSQLToTSQL (CONSNOM)
				Set RsNom = CC.execute(ConsNom)

				Retorna = Retorna & "<tr>"
				for each x in Rs.fields
					if ucase(x.name) <> "ESTADO" then
						if ucase(x.name) <> "HORADESDE" and ucase(x.name) <> "HORAHASTA" then
							Retorna = Retorna & "<td align='center' class='tbltext1' bgcolor='LightYellow'>" & x.value & "</td>"
						else
							Retorna = Retorna & "<td align='center' class='tbltext1' bgcolor='LightYellow'>" & ConvertirMinutosAHorario(x.value) & "</td>"
						end if
					end if
				next

				if not RsNom.EOF then
					NomPers = RsNom("Cognome") & " " & RsNom("Nome")
				end if

				Retorna = Retorna & "<td align='center' class='tbltext1' bgcolor='LightYellow'>" & NomPers & "</td>"

				Retorna = Retorna & "</tr>"
			'Rs.movenext
		'loop
	'end if

				nTotRecord=nTotRecord + 1
				rs.MoveNext
			wend

			Retorna = Retorna & "<table border=0 width=480>"
			Retorna = Retorna & "<tr>"
			if nActPagina > 1 then
				Retorna = Retorna & "<td align='right' width='480'>"
				Retorna = Retorna & "<a href=" & ASPActual & "?txtPagNum=" & (nActPagina - 1) & "&Oficina=" & Oficina & "&TxtFecha=" & Fecha & "&CmbEstadoCita=" & Estado & "><img src='" & Session("Progetto") & "/images/precedente.gif' border='0' id='image1' name='image1'>"
				Retorna = Retorna & "</td>"
			end if
			if nActPagina < nTotPagina then
				Retorna = Retorna & "<td align='right'>"
				Retorna = Retorna & "<a href=" & ASPActual & "?txtPagNum=" & (nActPagina + 1) & "&Oficina=" & Oficina & "&TxtFecha=" & Fecha & "&CmbEstadoCita=" & Estado & "><img src='" & Session("Progetto") & "/images/successivo.gif' border='0' id='image1' name='image1'>"
				Retorna = Retorna & "</td>"
			end if
			Retorna = Retorna & "</tr>"
			Retorna = Retorna & "</table>"

		end if
	end if
'	FinBD()

	set Rs = nothing

	GenerarFilasPorEstado = Retorna
End Function

Public Function GenerarFilasRangosPosibles(ASPActual,Oficina,Fecha,Opciones,Puesto,Duracion)
'=========================================================================================
' GENERA UNA TABLA CON LOS RANGOS HORARIOS POSIBLES PARA INGRESAR UNA CITA
'=========================================================================================
	Dim Rs
	Dim Sp
	Dim Retorna

'	InicioBD()

	Set Rs = Server.CreateObject("adodb.recordset")
	Set Sp = Server.CreateObject("adodb.command")

	Sp.ActiveConnection = connLavoro 'InicioBD()

	Sp.CommandText = "AgAgendaEstadoConsultaDia(" & Oficina & ",'" & Fecha & "'," & Opciones & "," & Puesto & "," & Duracion & ",0,'')"

	Sp.CommandType = 4

'PL-SQL * T-SQL
	Set Rs = Sp.Execute()

	Retorna = ""

'''paginacion
	if rs.state = 1 then
		if not rs.eof then
			nActPagina	=Request("txtPagNum")

			Record=0
			nTamPagina=7

			If nActPagina = "" Then
				nActPagina=1
			Else
				nActPagina=Clng(Request("txtPagNum"))
			End If

			rs.PageSize = nTamPagina

			rs.CacheSize = nTamPagina

			nTotPagina = rs.PageCount

			If nActPagina < 1 Then
				nActPagina = 1
			End If

			If nActPagina > nTotPagina Then
				nActPagina = nTotPagina
			End If

			rs.AbsolutePage=nActPagina
			nTotRecord=0

		'''paginacion
			While rs.EOF <> True And nTotRecord < nTamPagina
		  		if Record = 0 then
					Retorna = Retorna & "<tr>"
					for each x in Rs.fields
						if x.name <> "HoraDesde" and x.name <> "HoraHasta" and x.name <> "x" then
							Retorna = Retorna & "<td width = '25%' align='center' class='tbltext1' bgcolor='LightYellow'><b>" & x.name & "</b></td>"
						end if
					next
					Retorna = Retorna & "</tr>"
					cantcampos = Rs.fields.count
					record = 1
				end if

				Retorna = Retorna & "<tr>"
				for each x in Rs.fields
					if x.name <> "HoraDesde" and x.name <> "HoraHasta" and x.name <> "x" then
						Retorna = Retorna & "<td align='center' class='tbltext1' bgcolor='LightYellow'>" & x.value & "</td>"
					end if
				next
				Retorna = Retorna & "</tr>"

				nTotRecord=nTotRecord + 1
				rs.MoveNext
			wend

		Retorna = Retorna & "<tr>"
		Retorna = Retorna & "<td colspan='" & cantcampos & "'>"
			Retorna = Retorna & "<table border=0 width=360>"
			Retorna = Retorna & "<tr>"
			if nActPagina > 1 then
				Retorna = Retorna & "<td align='right' width='480'>"
				if instr(1,ASPActual,"?") <> 0 then
					Retorna = Retorna & "<a href=" & ASPActual & "&txtPagNum=" & (nActPagina - 1) & "&Oficina=" & Oficina & "&TxtFecha=" & Fecha & "&Opciones=" & Opciones & "&Puesto=" & Puesto & "&CmbDuracion=" & Duracion & "><img src='" & Session("Progetto") & "/images/precedente.gif' border='0' id='image1' name='image1'>"
				else
					Retorna = Retorna & "<a href=" & ASPActual & "?txtPagNum=" & (nActPagina - 1) & "&Oficina=" & Oficina & "&TxtFecha=" & Fecha & "&Opciones=" & Opciones & "&Puesto=" & Puesto & "&CmbDuracion=" & Duracion & "><img src='" & Session("Progetto") & "/images/precedente.gif' border='0' id='image1' name='image1'>"
				end if

				Retorna = Retorna & "</td>"
			end if
			if nActPagina < nTotPagina then
				Retorna = Retorna & "<td align='right'>"

				if instr(1,ASPActual,"?") <> 0 then
					Retorna = Retorna & "<a href=" & ASPActual & "&txtPagNum=" & (nActPagina + 1) & "&Oficina=" & Oficina & "&TxtFecha=" & Fecha & "&Opciones=" & Opciones & "&Puesto=" & Puesto & "&CmbDuracion=" & Duracion & "><img src='" & Session("Progetto") & "/images/successivo.gif' border='0' id='image1' name='image1'>"
				else
					Retorna = Retorna & "<a href=" & ASPActual & "?txtPagNum=" & (nActPagina + 1) & "&Oficina=" & Oficina & "&TxtFecha=" & Fecha & "&Opciones=" & Opciones & "&Puesto=" & Puesto & "&CmbDuracion=" & Duracion & "><img src='" & Session("Progetto") & "/images/successivo.gif' border='0' id='image1' name='image1'>"
				end if
				Retorna = Retorna & "</td>"
			end if
			Retorna = Retorna & "</tr>"
			Retorna = Retorna & "</table>"
		Retorna = Retorna & "</td>"
		Retorna = Retorna & "</tr>"

		end if
	end if

	GenerarFilasRangosPosibles = Retorna

'	FinBD()

	Set Rs = Nothing
	Set Sp = Nothing
End Function

Public Function GenerarStringFranjasDisponibles(Oficina,Fecha,Opciones,Puesto,Duracion)
'=========================================================================================
' GENERA UN STRING CON LOS HORARIOS DISPONIBLES
'=========================================================================================
	Dim Rs
	Dim Sp
	Dim Retorna

'	InicioBD()

	Set Rs = Server.CreateObject("adodb.recordset")
	Set Sp = Server.CreateObject("adodb.command")

	Sp.ActiveConnection = connLavoro 'InicioBD()
	Sp.CommandText = "AgAgendaEstadoConsultaDia(" & Oficina & ",'" & Fecha & "'," & Opciones & "," & Puesto & "," & Duracion & ",0,'')"
	Sp.CommandType = 4
'PL-SQL * T-SQL
	Set Rs = Sp.Execute()

	Retorna = ""
	If not Rs.eof then
		do while not Rs.eof
			for each x in Rs.fields
				if x.name = "HoraDesde" or x.name = "HoraHasta" then
					Retorna = Retorna & x.value & ","
				end if
			next
			if right(Retorna,1) = "," then
				Retorna = left(Retorna,len(Retorna)-1)
			end if
			Retorna = Retorna & "|"
			Rs.movenext
		loop
	end if

	if right(Retorna,1) = "|" then
		Retorna = left(Retorna,len(Retorna)-1)
	end if

	GenerarStringFranjasDisponibles = Retorna

'	FinBD()

	Set Rs = Nothing
	Set Sp = Nothing
End Function


Public Function GenerarArrayTipoDia(Oficina,Anio,Mes,Duracion,Cuil,TipoMov,TipoCita)
'=========================================================================================
' GENERA UN ARRAY CON LOS TIPOS DE DIAS
'=========================================================================================

	Dim Retorna

	Stat = 1
	Ms = "OK"

	Retorna = ""

			Set Sp 				= Server.CreateObject("ADODB.Command")
			Set Rec 			= Server.CreateObject("ADODB.Recordset")

			Sp.ActiveConnection = connLavoro 'InicioBD()

		if Cuil = "" and TipoMov = "" and TipoCita = "" then
			Sp.CommandText = "AgAgendaEstadoConsultaMes(" & clng(Oficina) & ", "  & cint(Anio) & ", " & cint(Mes) & ", " & cint(Duracion) & ", " & clng(Stat) & ", '" & cstr(Ms) & "')"
		else
			Sp.CommandText = "AgAgendaEstadoConsultaMesCuil(" & clng(Oficina) & ", "  & cint(Anio) & ", " & cint(Mes) & ", " & cint(Duracion) & ",'" & Cuil & "'," & cint(TipoMov) & "," & cint(TipoCita) & ", " & cint(Stat) & ", '" & cstr(Ms) & "')"
		end if


			'Response.Write Sp.CommandText
			'Response.end

			Sp.CommandType 		= 4 'adCmdStoredProc

'PL-SQL * T-SQL
			Set Rec = Sp.Execute()
			Set Sp = Nothing
'
'			'ACA LO PUEDO PASAR A UN ARRAY O USAR DIRECTAMENTE EL RECORDSET QUE CONTIENE LOS DATOS..

			If Not Rec.EOF then
				Do While not Rec.EOF
					DiaTipo = Rec("DiaTipo")
					DispMasLargo = Rec("DisponibleMasLargo")
					TiempoAsignado = Rec("TotTiempoAsignado")
					TiempoAtencion = Rec("TotTiempoDeAtencion")
					'DiaPresente    = Rec("DiaPresente")

					HayDisponible = Rec("HayDisponible")

					select case HayDisponible

						case 1 ' Dia no laborable
							TipoDia = "F"
						case 2 ' Dia anterior a la fecha de hoy
							TipoDia = "P"
						case 3 ' No hay Disponible para la duracion
							if DispMasLargo = 0 then
								TipoDia = "C"
							else
								TipoDia = "C" 'era P
							end if
						case 4 ' Hay uno solo disponible (aproximadamente 1 por puesto habilitado)
							TipoDia = "M"
						case 5 ' Hay bastante  (aproximadamente mas de 1 por puesto habilitado)
							if DispMasLargo > 0 and TiempoAsignado = 0 then
								TipoDia = "L"
							else
								TipoDia = "A"
							end if
						case else
							TipoDia = "F"
					end select

					Select case cstr(Rec("Dia"))
					case 0,1,2,3,4,5,6,7,8,9
						Dia = "0" & cstr(Rec("Dia"))
					case else
						Dia = cstr(Rec("Dia"))
					end select

					Retorna = Retorna & "|" & Dia & "|" & TipoDia

					Rec.movenext
				Loop
			End If

			If right(Retorna,1) = "|" then
				Retorna = left(Retorna,len(Retorna)-1)
			End if
			if left(Retorna,1) = "|" then
				Retorna = right(Retorna,len(Retorna)-1)
			end if

			GenerarArrayTipoDia = Retorna

'		FinBD()
End Function


Public Function GenerarArraySaltos(Oficina,Anio,Mes,Duracion,SoloMuestra,Cuil,TipoMov,TipoCita)
'=========================================================================================
' GENERA UN ARRAY CON EL ASP AL CUAL DEBE IR PARA UN DIA DETERMINADO
'=========================================================================================

	Dim Retorna

	if SoloMuestra = false then
		Salto1 = "MostrarCalendario.asp"
		Salto2 = "IngresarDatosCita.asp"
	else
		Salto1 = "DispCitasMes.asp"
		Salto2 = "MostrarDisponibilidadMes.asp"
	end if

	Stat = 1
	Ms = "OK"

	Retorna = ""

			Set Sp 				= Server.CreateObject("ADODB.Command")
			Set Rec 			= Server.CreateObject("ADODB.Recordset")

			Sp.ActiveConnection = connLavoro

		if Cuil = "" and TipoMov = "" and TipoCita = "" then
			Sp.CommandText = "AgAgendaEstadoConsultaMes(" & Oficina & "," & Anio & "," & Mes & "," & Duracion & "," & Stat & ",'" & Ms & "')"
		else
			Sp.CommandText = "AgAgendaEstadoConsultaMesCuil(" & Oficina & "," & Anio & "," & Mes & "," & Duracion & ",'" & Cuil & "'," & cint(TipoMov) & "," & cint(TipoCita) & ", " & Stat & ",'" & Ms & "')"
		end if


			Sp.CommandType = 4 'adCmdStoredProc


'PL-SQL * T-SQL
			Set Rec = Sp.Execute()
			Set Sp = Nothing
'
'			'ACA LO PUEDO PASAR A UN ARRAY O USAR DIRECTAMENTE EL RECORDSET QUE CONTIENE LOS DATOS..

			If Not Rec.EOF then
				Do While not Rec.EOF
					DiaTipo = Rec("DiaTipo")
					DispMasLargo = Rec("DisponibleMasLargo")
					TiempoAsignado = Rec("TotTiempoAsignado")
					TiempoAtencion = Rec("TotTiempoDeAtencion")
					'DiaPresente = Rec("DiaPresente")
					HayDisponible = Rec("HayDisponible")

					select case HayDisponible

						case 1 ' Dia no laborable
							TipoDia = Salto1
						case 2 ' Dia anterior a la fecha de hoy
							TipoDia = Salto1
						case 3 ' No hay Disponible para la duracion
							if DispMasLargo = 0 then
								TipoDia = Salto1
							else
								TipoDia = Salto1
							end if
						case 4 ' Hay uno solo disponible (aproximadamente 1 por puesto habilitado)
							TipoDia = Salto2
						case 5 ' Hay bastante  (aproximadamente mas de 1 por puesto habilitado)
							if DispMasLargo > 0 and TiempoAsignado = 0 then
								TipoDia = Salto2
							else
								TipoDia = Salto2
							end if
						case else
							TipoDia = Salto1
					end select

					Select case cstr(Rec("Dia"))
					case 0,1,2,3,4,5,6,7,8,9
						Dia = "0" & cstr(Rec("Dia"))
					case else
						Dia = cstr(Rec("Dia"))
					end select

					Retorna = Retorna & "|" & Dia & "|" & TipoDia

					Rec.movenext
				Loop
			End If

			If right(Retorna,1) = "|" then
				Retorna = left(Retorna,len(Retorna)-1)
			End if
			if left(Retorna,1) = "|" then
				Retorna = right(Retorna,len(Retorna)-1)
			end if

			GenerarArraySaltos = Retorna

'		FinBD()
End Function

Public Function GenerarComboMotivos(Nombre,Clase,Validacion,Seleccion,TipoMovimiento)
'=========================================================================================
' GENERA UN COMBO CON LOS POSIBLES MOTIVOS
'=========================================================================================
	Dim Rs
	Dim Consulta
	Dim Contador
	Dim Retorna

'	InicioBD()

	Set Rs = Server.CreateObject("adodb.recordset")

	Consulta = "Select Motivo, Descripcion from AgCitaMovimientosTiposMotivos where TipoMovimiento = " & cint(TipoMovimiento) & " order by Motivo, Descripcion"

'PL-SQL * T-SQL
'CONSULTA = TransformPLSQLToTSQL (CONSULTA)
	Set Rs = connLavoro.execute(Consulta)

	Retorna = ""

	Contador = 0

	If not Rs.eof then
		Rs.movefirst
		Retorna = Retorna & "<Select Name='" & Nombre & "' Class='" & Clase & "' OnChange=" & Validacion & ">"
		Retorna = Retorna & "<option></option>"
		Do while not Rs.eof
			If cstr(Rs("Motivo")) = cstr(Seleccion) then
				Seleccionado = "Selected"
			else
				Seleccionado = ""
			end if
			IdMotivo = Rs("Motivo")
			Retorna = Retorna & "<option value='" & Rs("Motivo") & "' " & Seleccionado & ">" & Rs("Descripcion") & "</option>"
			Rs.movenext
			Contador = Contador + 1
		Loop
		Retorna = Retorna & "</Select>"

		If Contador = 1 and cint(IdMotivo) = 0 then
			Retorna = ""
		end if
	else
		Retorna = ""
	end if

	GenerarComboMotivos = Retorna

'	FinBD()

	Set Rs = Nothing
End Function

Public Function ObtenerDuracionCita(NroCita)
'=========================================================================================
' OBTIENE LA DURACION DE UNA CITA DETERMINADA
'=========================================================================================
	Dim Rs
	Dim Consulta
	Dim Retorna

'	InicioBD()

	Set Rs = Server.CreateObject("adodb.recordset")

	Consulta = "Select HoraDesde, HoraHasta from AgCitas where NroCita = " & clng(NroCita)

'PL-SQL * T-SQL
'CONSULTA = TransformPLSQLToTSQL (CONSULTA)
	Set Rs = connLavoro.execute(Consulta)

	Retorna = ""

	If not Rs.eof then
		' Eltiempo de la cita incluye los minutos inicial(HoraDesde) y final(HoraHasta)
		Retorna = cint(rs("HoraHasta")) - cint(rs("HoraDesde")) + 1
	end if

	ObtenerDuracionCita = Retorna

'	FinBD()

	Set Rs = Nothing
End Function

Public Function GrabarMovimiento(Oficina, TipoMovimiento, CitaAnteriorNro, CitaNro, TipoCita, FechaCita, HoraDesde, Duracion, Cuil, NroEntrevista, Motivo, MotivoDesc, NroMovimientoAsociado, RefExternaTipo, RefExternaNro, Usuario)
'=========================================================================================
' GRABA UN MOVIMIENTO DE AGENDA
'=========================================================================================
' que parametros son de entrada
'NECESITA:

' "OFICINA"
' "TIPOMOVIMIENTO"
' "CITAANTERIOR"
' "CITANRO"
' "TIPOCITA"
' "FECHACITA"
' "HORADESDE"
' "HORAHASTA"
' "CUIL"
' "NROENTREVISTA"
' "MOTIVO"
' "MOTIVODESC"
' "NROMOVIMIENTOSASOCIADO"
' "REFEXTERNATIPO"
' "NREFEXTERNANRO"
' "USUARIO"
'
' que parametros son de salida
'DEVUELVE:

' "NROMOVIMIENTO"
' "PUESTO"
' "STATUS"
' "MENSAJE"
'---------------------------------------------------------------------------------------------------

	Dim Spx
	Dim Retorna
	Dim HoraHasta
	Dim Horas
	Dim Minutos
	dim xDuracion

	' duracion, debe restar un minuto porque incluye al minuto del desde
	' si no se hace se pasa en 1 minuto
	xDuracion = cint(Duracion) - 1
	if cint(xDuracion) < cint(1) then
		xDuracion = cint(1)
	end if


	if len(HoraDesde) > 0 then
		Horas = cint(left(HoraDesde,2)) * 60
		Minutos = cint(right(HoraDesde,2))

		HoraDesde = Horas + Minutos
		HoraHasta = Horas + Minutos + cint(xDuracion)
	end if

	'''
	NroEntrevista = 0
	'''

	Set Spx = Server.CreateObject("adodb.command")
	with Spx
		.activeconnection = connLavoro
		.Commandtext = "AgAgendaMovimientoGraba"
		.commandtype = adCmdStoredProc
		'--------------------------------------------
		' CArga parametros
		'--------------------------------------------
		'CitaAnteriorNro=0
		'Motivo=0
		'NroMovimientoAsociado=0
		'RefExternaNro=0
		'NroMovimiento=0
		
		For Each Parameter In .Parameters
		'response.Write Parameter.Name & "=" 
			If Parameter.Direction And adParamInput Then
			    'response.Write Parameter.Name & "<br>" 
				Select case ucase(Mid(Parameter.Name, 2))
					case "OFICINA"
						Parameter.value = Oficina
					CASE "TIPOMOVIMIENTO"
						Parameter.value = TipoMovimiento
					CASE "CITAANTERIORNRO"
						if len(CitaAnteriorNro) = 0 then
							CitaAnteriorNro = null
						end if
						Parameter.value = CitaAnteriorNro
					CASE "CITANRO"
						if len(CitaNro) = 0 then
							CitaNro = 0
						end if
						Parameter.value = CitaNro
					CASE "TIPOCITA"
						Parameter.value = TipoCita
					CASE "FECHACITA"
						Parameter.value = FechaCita
					CASE "HORADESDE"
						if len(HoraDesde) = 0 then
							HoraDesde = null
						end if
						Parameter.value = HoraDesde
					CASE "HORAHASTA"
						if len(HoraHasta) = 0 then
							HoraHasta = null
						end if
						Parameter.value = HoraHasta
					CASE "CUIL"
						Parameter.value = Cuil
					CASE "NROENTREVISTA"
						Parameter.value = NroEntrevista
					CASE "MOTIVO"
						if len(Motivo) = 0 then
							Motivo = null
						end if
						Parameter.value = Motivo
					CASE "MOTIVODESC"
						Parameter.value = MotivoDesc
					CASE "NROMOVIMIENTOASOCIADO"
					    if len(NroMovimientoAsociado) = 0 then
							NroMovimientoAsociado = null
						end if
						Parameter.value = NroMovimientoAsociado
					CASE "REFEXTERNATIPO"
						if len(RefExternaTipo) = 0 then
							RefExternaTipo = 0
						end if
						Parameter.value = RefExternaTipo
					CASE "REFEXTERNANRO"
						if len(RefExternaNro) = 0 then
							RefExternaNro = 0
						end if
						Parameter.value = RefExternaNro
					CASE "USUARIO"
						Parameter.value = Usuario
					'CASE "NROMOVIMIENTO"
					'	Parameter.value = Oficina
					'CASE "PUESTO"
					'	Parameter.value = Oficina
					CASE "STATUS"
						if len(Status) = 0 then
							Status = 0
						end if
						Parameter.value = Status
					CASE "MENSAJE"
						Parameter.value = "" & MENSAJE
					CASE ELSE
						' ????
				END SELECT
			End If
	    'response.Write Parameter.Name & "=" & Parameter.value  & "<br>" 

		Next
		'--------------------------------------------
		' Ejecuta
		'--------------------------------------------
		
'response.Write "Oficina=" & Oficina & "--TipoMovimiento=" & TipoMovimiento & "--TipoCita=" & TipoCita & "--HoraDesde=" & HoraDesde &_
'                 "--HoraHasta=" & HoraHasta & "--NroEntrevista=" & NroEntrevista & "--Motivo=" & Motivo & "--NroMovimientoAsociado=" & NroMovimientoAsociado &_
'                 "--RefExternaTipo=" & RefExternaTipo & "--RefExternaNro=" & RefExternaNro & "--Usuario=" & Usuario
'PL-SQL * T-SQL
'XXX = TransformPLSQLToTSQL (XXX)
		.Execute XXX, , adexecutenorecords

		'--------------------------------------------
		' Devuelve parametros
		'--------------------------------------------
		For Each Parameter In .Parameters
			If Parameter.Direction And (adParamOutput Or adParamReturnValue) Then
				Select case ucase(Mid(Parameter.Name, 2))
					case "STATUS"
						STATUS = trim(Parameter.value)
					case "MENSAJE"
						MENSAJE = trim(Parameter.value)
					case "CITANRO"
						CITANRO = trim(Parameter.value)
					case else
						' ???
				end select
			End If
		Next
	end with

	'--------------------------------------------
	' Prepara salida
	'--------------------------------------------
	if STATUS <> 1 then
		Retorna = "Error: " & STATUS & "<br> Mensaje: " & MENSAJE
	else
		Retorna = CITANRO
	end if

	'retorna el nro de cita que genera..
	GrabarMovimiento = Retorna

'	FinBD()

	Set Spx = Nothing
End Function


Public function ListarReservas(Oficina,Usuario,Lote,CantEntrev,Observaciones,DirSede)
'=========================================================================================
' GENERA EL LISTADO DE RESERVAS PARA SER NOMINADAS
'=========================================================================================
	Oficina = Oficina
	RefExternaTipo = 1
	RefExternaNro = Lote
	CantEntrevistadores = CantEntrev
	CantFormHojas = 3
	Observaciones = Observaciones
	Usuario = Usuario

	Secuencia = 1
	NroCopia = 1
	Status = 1
	Mensaje = "OK"

	DirSede = DirSede


	'InicioBD()

	Dim Sp
	Dim Retorna

	Retorna = ""

	Set Sp = Server.CreateObject("adodb.command")

	Set Rec = Server.CreateObject("ADODB.Recordset")

	Sp.ActiveConnection = connLavoro 'InicioBD()

	Sp.CommandText = "AgReservasListado(" & Oficina & "," & RefExternaTipo & "," & RefExternaNro & "," & CantEntrevistadores & "," & CantFormHojas & ",'" & Observaciones & "'," & Usuario & "," & Secuencia & "," & NroCopia & "," & Status & ",'" & Mensaje & "')"

	Sp.CommandType = 4 'adCmdStoredProc


'PL-SQL * T-SQL
	Set Rec = Sp.Execute()
	Set Sp = Nothing


if Rec.state = 1 then
	if not Rec.eof then
		Contador = 0

		Rec.movefirst

		Retorna = Retorna & "<br>"

		Retorna = Retorna & "<div align='center'>"
		Retorna = Retorna & "	<table border='0' cellspacing='0' cellpadding='0'>"

		do while not Rec.eof

			Contador = Contador + 1

			if (Rec.fields(10) = 1) or (Contador = 1) then  'PaginaSalto
			'	'Retorna = Retorna & "<P align='right'>P&aacute;gina " & Rec.fields(8) & " de " & Rec.fields(9) & "</P>" 'PaginaNro y PaginaTotal

				'Retorna = Retorna & "<tr><td colspan=2>SALTO</td></tr>"
				if Contador <> 1 then
					Retorna = Retorna & "<tr style='PAGE-BREAK-AFTER: always'><td colspan=2>&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>"
				end if

				Retorna = Retorna & " <tr><td colspan=4>"
				Retorna = Retorna & " <table cellpadding=0 cellspacing=0 width = '100%' border='0'>"
				Retorna = Retorna & "    <tr>"
				Retorna = Retorna & "	<td align='left'>"
				'Retorna = Retorna & "		"
				Retorna = Retorna & "		<b class='tbltext1'>Lote Nro: " & Lote & "</b><br>"
				Retorna = Retorna & "		<b class='tbltext1'>Observaciones: " & ucase(Observaciones) & "</b><br>"
				Retorna = Retorna & "		<b class='tbltext1'>Entrevistador Nro.: " & Rec.fields(6) & "</b><br>" 'Entrevistador
				Retorna = Retorna & "	</td>"
				Retorna = Retorna & "	</tr>"
				Retorna = Retorna & "    <tr>"
				Retorna = Retorna & "    <td class='tbltext1' align='left'><b>Hoja " & Rec.Fields(8) & " de " & Rec.fields(9) & "</b>" 'PaginaNro y PaginaTotal
				Retorna = Retorna & "    </td>"
				Retorna = Retorna & "    </tr>"

				Retorna = Retorna & "	<tr height='2'>"
				Retorna = Retorna & "		<td colspan='3'><br></td>"
				Retorna = Retorna & "	</tr>"

				'Retorna = Retorna & "	<tr height='2'>"
				'Retorna = Retorna & "		<td colspan='3'>"
				'Retorna = Retorna & "		<td colspan='3' class='SFONDOCOMM' background='" & Session("Progetto") & "/images/separazione.gif'>"
				'Retorna = Retorna & "		</td>"
				'Retorna = Retorna & "	</tr>"
				Retorna = Retorna & "</table>"
				Retorna = Retorna & "</td></tr>"
			end if


			if (Rec.fields(10) = 0) and (Contador <> 1) then
				Retorna = Retorna & "<tr><td valign='top' colspan=4>..............................................................................................................................................................</td></tr>"
				Retorna = Retorna & "<tr><td colspan=4><br></td></tr>"
			end if

			Retorna = Retorna & "<tr>"

			for i = 1 to 2
				Retorna =   Retorna & "<td width=220>"
				Retorna	=	Retorna	&	"<table	border=1 cellpadding=1 cellspacing=1 bordercolor=black class='textblack'>"
				Retorna	=	Retorna	&	"	<tr	bordercolor=white>"
				''''giffffffffffffffffff
				Retorna	=	Retorna	&	"		<td	width='10%'><img src=" & Session("Progetto") & "/Images/logonotif.gif></img></td>"
				Retorna	=	Retorna	&	"		<td	colspan=3><b>Seguro	de Capacitaci&oacute;n	y	Empleo <br>Citaci&oacute;n	para Entrevista	Inicial	<br><br></b></td>"
				Retorna	=	Retorna	&	"	</tr>"

				Retorna	=	Retorna	&	"	<tr	bordercolor=white>"
				Retorna	=	Retorna	&	"		<td	colspan=4>Me comprometo	a	concurrir	a	la entrevista	inicial	el d&iacute;a <b>" & Rec.fields(1) 'CitaFecha
				Retorna	=	Retorna	&	"</b> a	la hora	<b>" & Rec.fields(2) & "</b> en la	Oficina	de Empleo	local	situada	en <b>" & DirSede & "</b>" 'CitaHora
				'Retorna	=	Retorna	&	"		xxxxxxx	xxxx xxxx	xxxxx	xxxx xxx xxx xx	xxx	xxxxx	xxxx xxxx"
				Retorna	=	Retorna	&	"		</td>"
				Retorna	=	Retorna	&	"	</tr>"

				Retorna	=	Retorna	&	"	<tr	bordercolor=white>"
				Retorna	=	Retorna	&	"		<td	height=20 width='25%'><b>Nombre:</b></td>"
				Retorna	=	Retorna	&	"		<td	height=20	colspan=3>...........................................................</td>"
				Retorna	=	Retorna	&	"	</tr>"

				Retorna	=	Retorna	&	"	<tr	bordercolor=white>"
				Retorna	=	Retorna	&	"		<td	height=15	width='25%'><b>C.I.:</b></td>"
				Retorna	=	Retorna	&	"		<td	height=15	colspan=3>...........................................................</td>"
				Retorna	=	Retorna	&	"	</tr>"

				Retorna	=	Retorna	&	"	<tr	bordercolor=white>"
				Retorna =   Retorna &   "	<td height=15 colspan=4>"
				Retorna =   Retorna &   "	<table border=0 cellpadding=0 cellspacing=0 width='100%' class='textblack'>"
				Retorna =   Retorna &   "	<tr>"
				Retorna	=	Retorna	&	"		<td	width='20%' nowrap height=15><b>Tipo Doc.:</b></td>"
				Retorna	=	Retorna	&	"		<td	width='5%'  height=15>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;...............</td>"
				Retorna	=	Retorna	&	"		<td	width='20%' height=15><b>N&uacute;mero:</b></td>"
				Retorna	=	Retorna	&	"		<td	width='45%' height=15>................................</td>"
				Retorna =   Retorna &   "	</tr>"
				Retorna =   Retorna &   "	</table>"
				Retorna	=	Retorna	&	"	</td>"
				Retorna =   Retorna &   "	</tr>"

				Retorna	=	Retorna	&	"	<tr	height=10	bordercolor=white>"
				Retorna	=	Retorna	&	"		<td	colspan=4>Si por alguna	raz&oacute;n	no pudiera concurrir en	la fecha indicada,"
				Retorna	=	Retorna	&	"		me remitir&eacute;	a	la Oficina de	Empleo para	definir	una	nueva	cita."
				Retorna	=	Retorna	&	"		</td>"
				Retorna	=	Retorna	&	"	</tr>"

				Retorna	=	Retorna	&	"	<tr	bordercolor=white>"
				Retorna	=	Retorna	&	"		<td	colspan=4>"
				Retorna	=	Retorna	&	"			<table width='100%'	cellpadding=0 cellspacing=0 class='textblack'>"
				Retorna	=	Retorna	&	"				<tr	height=15>"
				Retorna	=	Retorna	&	"					<td	width='50%'	align='center'>........................................</td>"
				Retorna	=	Retorna	&	"					<td	width='50%'	align='center'>........................................</td>"
				Retorna	=	Retorna	&	"				</tr>"
				Retorna	=	Retorna	&	"				<tr	height=4>"
				Retorna	=	Retorna	&	"					<td	align='center'><b>Firma	Entrevistador</b></td>"
				Retorna	=	Retorna	&	"					<td	align='center'><b>Firma	Beneficiario</b></td>"
				Retorna	=	Retorna	&	"				</tr>"
				Retorna	=	Retorna	&	"			</table>"
				Retorna	=	Retorna	&	"		</td>"
				Retorna	=	Retorna	&	"	</tr>"

				Retorna	=	Retorna	&	"	<tr	bordercolor=white>"
				Retorna	=	Retorna	&	"		<td	colspan=4>"
				Retorna	=	Retorna	&	"			<table cellpadding=1 cellspacing=1 width=100%	class='textblack'>"
				Retorna	=	Retorna	&	"				<tr height=3>"
				Retorna	=	Retorna	&	"					<td	width=35%   align='left'>" & Rec.fields(11) & "</td>" 'FechaHoraEmision
				Retorna	=	Retorna	&	"					<td	width=20%	align='right'><b>N�	Lote:</b></td>"
				Retorna	=	Retorna	&	"					<td	width=10%	align='left'>" & Rec.fields(4) & "</td>" 'Nrolote
				Retorna	=	Retorna	&	"					<td	width=20%	align='right'><b>N�	Cita:</b></td>"
				Retorna	=	Retorna	&	"					<td	width=15%	align='left'>" & Rec.fields(0) & "</td>" 'CitaNro
				Retorna	=	Retorna	&	"				</tr>"
				Retorna	=	Retorna	&	"			</table>"
				Retorna	=	Retorna	&	"		</td>"
				Retorna	=	Retorna	&	"	</tr>"

				Retorna	=	Retorna	&	"	<tr	bordercolor=white height=3>"
				Retorna	=	Retorna	&	"		<td	colspan=4>"
				Retorna	=	Retorna	&	"			<table width=100% cellpadding=1 cellspacing=1 class='textblack'>"
				Retorna	=	Retorna	&	"				<tr>"
				Retorna	=	Retorna	&	"					<td>"


				if Rec.fields(5) > 1 then
					IMPR = "Reimpresi&oacute;n"
				else
					IMPR = "Impresi&oacute;n"
				end if

				Retorna	=	Retorna	&	IMPR  & " N&uacute;mero: " & Rec.fields(5) 'CopiaNro
				Retorna	=	Retorna	&	"					</td>"
				Retorna	=	Retorna	&	"					<td>"

				if i = 1 then
					V1 = "Original "
					V2 = "Oficina de Empleo"
				else
					V1 = "Duplicado "
					V2 = "Beneficiario"
				end if

				Retorna	=	Retorna	& V1 & V2
				Retorna	=	Retorna	&	"					</td>"
				Retorna	=	Retorna	&	"				</tr>"
				Retorna	=	Retorna	&	"	</table>"
				Retorna	=	Retorna	&	"	</td>"
				Retorna	=	Retorna	&	"	</tr>"
				Retorna	=	Retorna	&	"</table>"

				Retorna = Retorna & "</td>"
				Retorna = Retorna & "<td colspan=2>&nbsp;&nbsp;&nbsp;&nbsp;</td>"

			next
			Retorna = Retorna & "</tr>"

			Rec.movenext
		loop

		Retorna = Retorna & "		</table>"
		Retorna = Retorna & "</div>"
	end if
end if

FinBD()

	ListarReservas = Retorna

	Set Rec = nothing

end function


Public Function GrabarReservas(Oficina, TipoMovimiento, FechaBase, CantReservas, Motivo, MotivoDesc, Usuario)
'=========================================================================================
' GRABA LAS RESERVAS
'=========================================================================================
' que parametros son de entrada
'NECESITA:

' Oficina
' TipoMovimiento
' FechaBase
' CantReservas
' Motivo
' MotivoDesc
' Usuario
'
' que parametros son de salida
'DEVUELVE:

' "Nrolotereservas"
' "Status"
' "Mensaje"
'---------------------------------------------------------------------------------------------------

	Dim Spx
	Dim Retorna

	Set Spx = Server.CreateObject("adodb.command")

	with Spx
		.activeconnection = connLavoro 'InicioBD()
		.Commandtext = "AgReservasGenera"
		.commandtype = adCmdStoredProc
		.CommandTimeout = 300
		'--------------------------------------------
		' CArga parametros
		'--------------------------------------------
		For Each Parameter In .Parameters
			If Parameter.Direction And adParamInput Then
				Select case ucase(Mid(Parameter.Name, 2))
					case "OFICINA"
						Parameter.value = Oficina
					CASE "TIPOMOVIMIENTO"
						Parameter.value = TipoMovimiento
					CASE "FECHABASE"
						Parameter.value = FechaBase
					CASE "CANTRESERVAS"
						if len(CantReservas) = 0 then
							CantReservas = 0
						end if
						Parameter.value = "" & CantReservas
					CASE "MOTIVO"
						if len(Motivo) = 0 then
							Motivo = 0
						end if
						Parameter.value = Motivo
					CASE "MOTIVODESC"
						Parameter.value = MotivoDesc
					CASE "USUARIO"
						Parameter.value = Usuario
					'CASE "NROLOTERESERVAS"
					'	Parameter.value = Oficina
					CASE "STATUS"
						if len(Status) = 0 then
							Status = 0
						end if
						Parameter.value = Status
					CASE "MENSAJE"
						Parameter.value = "" & Mensaje
					CASE ELSE
						' ????
				END SELECT
			End If
		Next
		
'		For Each Parameter In .Parameters
'			response.write Parameter.Name & "=" & Parameter.value & "<BR>"
'		Next
'		Response.End

		'--------------------------------------------
		' Ejecuta
		'--------------------------------------------
'PL-SQL * T-SQL
'XXX = TransformPLSQLToTSQL (XXX)
		.Execute XXX, , adexecutenorecords

		'--------------------------------------------
		' Devuelve parametros
		'--------------------------------------------
		For Each Parameter In .Parameters
			If Parameter.Direction And (adParamOutput Or adParamReturnValue) Then
				Select case ucase(Mid(Parameter.Name, 2))
					case "STATUS"
						STATUS = trim(Parameter.value)
					case "MENSAJE"
						MENSAJE = trim(Parameter.value)
					case "NROLOTERESERVAS"
						NROLOTERESERVAS = trim(Parameter.value)
					case else
						' ???
				end select
			End If
		Next
	end with

	'--------------------------------------------
	' Prepara salida
	'--------------------------------------------
	if STATUS <> 1 then
		Retorna = "Error: " & STATUS & "<br> Mensaje: " & MENSAJE
	else
		Retorna = NROLOTERESERVAS
	end if

	'retorna el nro de cita que genera..
	GrabarReservas = Retorna

'	FinBD()

	Set Spx = Nothing
End Function

Public Function AnularReservas(Oficina, TipoMovimiento, ReservaTipo, NroLoteReservas, Motivo, MotivoDesc, Usuario)
'=========================================================================================
' ANULA LAS RESERVAS
'=========================================================================================
' que parametros son de entrada
'NECESITA:
' Oficina
' TipoMovimiento
' ReservaTipo
' NroLoteReservas
' Motivo
' MotivoDesc
' Usuario
'
' que parametros son de salida
'DEVUELVE:

' "CantReservasAnuladas"
' "Status"
' "Mensaje"
'---------------------------------------------------------------------------------------------------

	Dim Spx
	Dim Retorna

	Set Spx = Server.CreateObject("adodb.command")
	with Spx
		.activeconnection = connLavoro 'InicioBD()
		.Commandtext = "AgReservasAnula"
		.commandtype = adCmdStoredProc
		'--------------------------------------------
		' CArga parametros
		'--------------------------------------------
		For Each Parameter In .Parameters
			If Parameter.Direction And adParamInput Then
				Select case ucase(Mid(Parameter.Name, 2))
					case "OFICINA"
						Parameter.value = Oficina
					CASE "TIPOMOVIMIENTO"
						Parameter.value = TipoMovimiento
					CASE "RESERVATIPO"
						Parameter.value = ReservaTipo
					CASE "NROLOTERESERVAS"
						if len(NroLoteReservas) = 0 then
							NroLoteReservas = 0
						end if
						Parameter.value = "" & NroLoteReservas
					CASE "MOTIVO"
						if len(Motivo) = 0 then
							Motivo = 0
						end if
						Parameter.value = Motivo
					CASE "MOTIVODESC"
						Parameter.value = MotivoDesc
					CASE "USUARIO"
						Parameter.value = Usuario
					CASE "CANTRESERVASANULADAS"
						if len(CantReservasAnuladas) = 0 then
							CantReservasAnuladas = 0
						end if
						Parameter.value = CantReservasAnuladas
					CASE "STATUS"
						if len(Status) = 0 then
							Status = 0
						end if
						Parameter.value = Status
					CASE "MENSAJE"
						Parameter.value = "" & Mensaje
					CASE ELSE
						' ????
				END SELECT
			End If
		Next

		'For Each Parameter In .Parameters
	'		response.write Parameter.Name & "=" & Parameter.value & "<BR>"
		'Next
		'Response.End
		'--------------------------------------------
		' Ejecuta
		'--------------------------------------------
'PL-SQL * T-SQL
'XXX = TransformPLSQLToTSQL (XXX)
		.Execute XXX, , adexecutenorecords

		'--------------------------------------------
		' Devuelve parametros
		'--------------------------------------------
		For Each Parameter In .Parameters
			If Parameter.Direction And (adParamOutput Or adParamReturnValue) Then
				Select case ucase(Mid(Parameter.Name, 2))
					case "STATUS"
						STATUS = trim(Parameter.value)
					case "MENSAJE"
						MENSAJE = trim(Parameter.value)
					case "CANTRESERVASANULADAS"
						CANTRESERVASANULADAS = trim(Parameter.value)
					case else
						' ???
				end select
			End If
		Next
	end with

	'--------------------------------------------
	' Prepara salida
	'--------------------------------------------
	if STATUS <> 1 then
		Retorna = "Error: " & STATUS & "<br> Mensaje: " & MENSAJE
	else
		Retorna = CANTRESERVASANULADAS
	end if

	'retorna el nro de cita que genera..
	AnularReservas = Retorna

'	FinBD()

	Set Spx = Nothing
End Function


Public Function GenerarComboHorario(Oficina,Fecha,Opciones,Puesto,Duracion,Nombre,Clase,Validacion)
'=========================================================================================
' GENERA UN COMBO CON LOS HORARIOS POSIBLES PARA INGRESAR UNA CITA
'=========================================================================================
	Dim Rs
	Dim Sp
	Dim Retorna

'	InicioBD()

	Set Rs = Server.CreateObject("adodb.recordset")
	Set Sp = Server.CreateObject("adodb.command")

	Sp.ActiveConnection = connLavoro 'InicioBD()

	stat=0
	msgexec=""
	Sp.CommandText = "AgAgendaEstadoConsultaDia(" & Oficina & ",'" & Fecha & "'," & Opciones & "," & Puesto & "," & Duracion & "," & stat & ",'" & msgexec & "')"

	Sp.CommandType = 4

'Response.Write  "AgAgendaEstadoConsultaDia(" & Oficina & ",'" & Fecha & "'," & Opciones & "," & Puesto & "," & Duracion & "," & stat & ",'" & msgexec & "')"

'PL-SQL * T-SQL
	Set Rs = Sp.Execute()

'Response.Write  "AgAgendaEstadoConsultaDia(" & Oficina & ",'" & Fecha & "'," & Opciones & "," & Puesto & "," & Duracion & "," & stat & ",'" & msgexec & "')"

	Retorna = ""

	If not Rs.eof then
		Retorna = Retorna & "<select name='" & Nombre & "' Onchange='" & Validacion & "' Class='" & Clase & "'>"
		do while not Rs.eof
			If rs.fields(6) <> 0 then
				Retorna = Retorna & "<option value ='" & Rs.fields(3) & "'>" & Rs.fields(3) & "</option>"
			end if
			Rs.movenext
		loop
	end if

	Retorna = Retorna & "</select>"


	GenerarComboHorario = Retorna

'	FinBD()

	Set Rs = Nothing
	Set Sp = Nothing
End Function


Public Function ObtenerValor(Tabla,Identificador)
'=========================================================================================
' OBTIENE UN VALOR ESPECIFICO A TRAVES DE LA TABLA Y EL CAMPO IDENTIFICADOR
'=========================================================================================
	Dim Rs
	Dim Consulta
	Dim Retorna
	Dim campoid

'	InicioBD()

	Set Rs = Server.CreateObject("adodb.recordset")

	select case ucase(Tabla)
		case "AGCITATIPOS"
			campoid = "CitaTipo"
		case "AGCITAMOVIMIENTOSTIPOS"
			campoid = "TipoMovimiento"
		case else

	end select

	Consulta = "Select Descripcion from " & Tabla & " where " & campoid & "=" & Cint(Identificador)

'PL-SQL * T-SQL
'CONSULTA = TransformPLSQLToTSQL (CONSULTA)
	Set Rs = connLavoro.execute(Consulta)

	Retorna = ""

	If not Rs.eof then
		Rs.movefirst
		Retorna = Rs("Descripcion")
	end if

	ObtenerValor = Retorna

'	FinBD()

	Set Rs = Nothing
End function

Public Function MostrarDatosCita(Oficina, TipoMovimiento, NroLoteReservas, NroCita)
'=========================================================================================
' MUESTRA LOS DATOS DE UNA CITA EN PARTICULAR
'=========================================================================================
'
' que parametros son de entrada
'NECESITA:
' @Oficina		 	int,
' @TipoMovimiento	int = 0,
' @NroLoteReservas	int = 0,
' @NroCita	 		int = 0,
'
' que parametros son de salida
'DEVUELVE:

'	@CitaFecha			varchar(10) output,
'	@HoraDesdeMin		int			output,
'	@HoraHastaMin		int			output,
'	@HoraDesdeHs		varchar(5)	output,
'	@HoraHastaHs		varchar(5)	output,
'	@CUIL				varchar(16) output,
'	@CitaEstado			int			output,
'	@citaEstadoDesc		varchar(50) output,
'	@Puesto				int			output,
'	@NroEntrevista		int			output,
'	@UltNroMovimento	int			output,
'
'	@Status				int			OUTPUT,	-- Status = 1 --> OK
'	@Mensaje			varchar(255)OUTPUT	-- Mensaje de Error

'---------------------------------------------------------------------------------------------------

	Dim Spx
	Dim Retorna

	Retorna = ""

	Set Rs = server.CreateObject("adodb.recordset")
	Set Spx = Server.CreateObject("adodb.command")
	with Spx
		.activeconnection = connLavoro 'InicioBD()
		.Commandtext = "AgCitaDatos"
		.commandtype = adCmdStoredProc
		'--------------------------------------------
		' CArga parametros
		'--------------------------------------------
		For Each Parameter In .Parameters
			If Parameter.Direction And adParamInput Then
				Select case ucase(Mid(Parameter.Name, 2))
					case "OFICINA"
						Parameter.value = Oficina
						
					CASE "TIPOMOVIMIENTO"
						Parameter.value = TipoMovimiento
						
					CASE "NROLOTERESERVAS"
						Parameter.value = NroLoteReservas
						
					CASE "NROCITA"
						if len(NroCita) = 0 then
							NroCita = 0
						end if
						Parameter.value = "" & NroCita
						
					CASE "STATUS"
						if len(Status) = 0 then
							Status = 0
						end if
						Parameter.value = Status
					CASE "MENSAJE"
						Parameter.value = "" & Mensaje
					CASE ELSE
						' ????
				END SELECT
			End If
		Next
		'--------------------------------------------
		' Ejecuta
		'--------------------------------------------
'PL-SQL * T-SQL
'XXX = TransformPLSQLToTSQL (XXX)
		.Execute XXX, , adexecutenorecords

		'--------------------------------------------
		' Devuelve parametros
		'--------------------------------------------
		For Each Parameter In .Parameters
			If Parameter.Direction And (adParamOutput Or adParamReturnValue) Then
				Select case ucase(Mid(Parameter.Name, 2))
					case "STATUS"
						STATUS = trim(Parameter.value)
					case "MENSAJE"
						MENSAJE = trim(Parameter.value)
					case "CITAFECHA"
						CITAFECHA = trim(Parameter.value)
					case "HORADESDEMIN"
						HORADESDEMIN = trim(Parameter.value)
					case "HORAHASTAMIN"
						HORAHASTAMIN = trim(Parameter.value)
					case "HORADESDEHS"
						HORADESDEHS = trim(Parameter.value)
					case "HORAHASTAHS"
						HORAHASTAHS = trim(Parameter.value)
					case "CUIL"
						CUIL = trim(Parameter.value)
					case "CITAESTADO"
						CITAESTADO = trim(Parameter.value)
					case "CITAESTADODESC"
						CITAESTADODESC = trim(Parameter.value)
					case "PUESTO"
						PUESTO = trim(Parameter.value)
					case "NROENTREVISTA"
						NROENTREVISTA = trim(Parameter.value)
					case "ULTNROMOVIMIENTO"
						ULTNROMOVIMIENTO = trim(Parameter.value)
					case else
						' ???
				end select
			End If
		Next
	end with

	'--------------------------------------------
	' Prepara salida
	'--------------------------------------------
	
	
	if STATUS <> 1 then
		Retorna = "Error: " & STATUS & "<br> Mensaje: " & MENSAJE
	else
        Retorna = Retorna & "<input type='hidden' name='TxtCitaFecha' value='" & CITAFECHA & "'>"
        Retorna = Retorna & "<input type='hidden' name='TxtHoraDesdeMin' value='" & HORADESDEMIN & "'>"
        Retorna = Retorna & "<input type='hidden' name='TxtHoraHastaMin' value='" & HORAHASTAMIN & "'>"
        Retorna = Retorna & "<input type='hidden' name='TxtHoraDesdeHs' value='" & HORADESDEHS & "'>"
        Retorna = Retorna & "<input type='hidden' name='TxtHoraHastaHs' value='" & HORAHASTAHS & "'>"
       ' Retorna = Retorna & "<input type='hidden' name='TxtCUIL' value='" & CUIL & "'>"
        Retorna = Retorna & "<input type='hidden' name='TxtCitaEstado' value='" & CITAESTADO & "'>"
        Retorna = Retorna & "<input type='hidden' name='TxtcitaEstadoDesc' value='" & CITAESTADODESC & "'>"
        Retorna = Retorna & "<input type='hidden' name='TxtPuesto' value='" & PUESTO & "'>"
        Retorna = Retorna & "<input type='hidden' name='TxtNroEntrevista' value='" & NROENTREVISTA & "'>"
        Retorna = Retorna & "<input type='hidden' name='TxtUltNroMovimento' value='" & ULTNROMOVIMIENTO & "'>"

		Retorna = Retorna & "	<table width='100%' border=2 bordercolor='MidnightBlue'>"
		Retorna = Retorna & "			<tr>"
		Retorna = Retorna & "				<th align='center' width=10% class='tbltext1' bgcolor='#D9D9AE'><b>Fecha</b></td>"
		Retorna = Retorna & "				<th align='center' width=10% class='tbltext1' bgcolor='#D9D9AE'><b>Hora Inicio</b></td>"
		Retorna = Retorna & "				<th align='center' width=10% class='tbltext1' bgcolor='#D9D9AE'><b>Hora Fin</b></td>"
		Retorna = Retorna & "				<th align='center' width=30% class='tbltext1' bgcolor='#D9D9AE'><b>C.I.</b></td>"
		Retorna = Retorna & "				<th align='center' width=40% class='tbltext1' bgcolor='#D9D9AE'><b>Apellido y Nombre</b></td>"
		Retorna = Retorna & "			</tr>"
		Retorna = Retorna & "			<tr>"
		Retorna = Retorna & "				<td align='center' class='tbltext1' bgcolor='LightYellow'><b>" & CITAFECHA & "</b></td>"
		Retorna = Retorna & "				<td align='center' class='tbltext1' bgcolor='LightYellow'><b>" & HORADESDEHS & "</b></td>"
		Retorna = Retorna & "				<td align='center' class='tbltext1' bgcolor='LightYellow'><b>" & HORAHASTAHS & "</b></td>"
		Retorna = Retorna & "				<td align='center' class='tbltext1' bgcolor='LightYellow'>"
		Retorna = Retorna & "					<input name='TxtCuilNH' type='text' class='textblack' style='TEXT-TRANSFORM:uppercase' maxlength='8' size='20'>"
		Retorna = Retorna & "					<input name='TxtCuil' type='hidden'>"
		Retorna = Retorna & "					<a href=Javascript:IrA('../BuscarPostulante.asp') ID='imgPunto1' name='imgPunto1' onmouseover=javascript:window.status='';return true><img border='0' src='" & Session("Progetto") & "/images/bullet1.gif'></a>"
		Retorna = Retorna & "				<td align='center' class='tbltext1' bgcolor='LightYellow'>"
		Retorna = Retorna & "				<input name='TxtNombre' type='text' size='40' disabled class='textblack' style='TEXT-TRANSFORM:uppercase' disabled>"
		Retorna = Retorna & "					<input name='TxtNom' type='hidden'>"
		Retorna = Retorna & "				</td>"
		Retorna = Retorna & "				</td>"
		Retorna = Retorna & "			</tr>"
		Retorna = Retorna & "		</table>"
		Retorna = Retorna & "		<table width='90%'>"
		Retorna = Retorna & "			<tr>"
		Retorna = Retorna & "				<td align='right'>"
		Retorna = Retorna & "					<br><input type='submit' class='my' value='Nominar' id='submit'1 name='submit'1>"
		Retorna = Retorna & "				</td>"
		Retorna = Retorna & "			</tr>"
		Retorna = Retorna & "		</table>"
	end if


	'retorna el nro de cita que genera..
	MostrarDatosCita = Retorna



'	FinBD()

	Set Spx = Nothing
End Function

Public Function InicioBD()

	'================================================================================================
	'EN DESUSO
	' Esto estaba hasta el 27/03/2006 cambios realizados por Hern�n
	'================================================================================================
	'Hosting = "PORTALDESARROLLO"
    'Application(Hosting & "_AgendaConnectionString") = "Provider=MSDASQL.1;Password=1234*;Persist Security Info=True;User ID=EmpleoLavoro;Data Source=EmpleoLavoro;Extended Properties=""DSN=EmpleoLavoro;Description=EmpleoLavoro;UID=EmpleoLavoro;APP=????4?????4?????T0???????;WSID=PC0297;DATABASE=EmpleoLavoro"";Initial Catalog=EmpleoLavoro;User Id=EmpleoLavoro;PASSWORD=1234*;"
	'Application(Hosting & "_AgendaConnectionTimeout") = 60
	'Application(Hosting & "_AgendaCommandTimeout") = 180
	'Application(Hosting & "_AgendaCursorLocation") = 3
	'Application(Hosting & "_AgendaRuntimeUserName") = "EmpleoLavoro"
	'Application(Hosting & "_AgendaRuntimePassword") = "1234*"

	'Hosting = "PORTALPRUEBA"
    'Application(Hosting & "_AgendaConnectionString") = "Provider=MSDASQL.1;Password=1234*;Persist Security Info=True;User ID=EmpleoLavoro;Data Source=EmpleoLavoro;Extended Properties=""DSN=EmpleoLavoro;Description=EmpleoLavoro;UID=EmpleoLavoro;APP=????4?????4?????T0???????;WSID=PC0297;DATABASE=EmpleoLavoro"";Initial Catalog=EmpleoLavoro;User Id=EmpleoLavoro;PASSWORD=1234*;"
	'Application(Hosting & "_AgendaConnectionTimeout") = 60
	'Application(Hosting & "_AgendaCommandTimeout") = 180
	'Application(Hosting & "_AgendaCursorLocation") = 3
	'Application(Hosting & "_AgendaRuntimeUserName") = "EmpleoLavoro"
	'Application(Hosting & "_AgendaRuntimePassword") = "1234*"

	'Hosting = "PORTALEMPLEO"
    'Application(Hosting & "_AgendaConnectionString") = "Provider=MSDASQL.1;Password=1234*;Persist Security Info=True;User ID=EmpleoLavoro;Data Source=EmpleoLavoro;Extended Properties=""DSN=EmpleoLavoro;Description=EmpleoLavoro;UID=EmpleoLavoro;APP=????4?????4?????T0???????;WSID=PC0297;DATABASE=EmpleoLavoro"";Initial Catalog=EmpleoLavoro;User Id=EmpleoLavoro;PASSWORD=1234*;"
	'Application(Hosting & "_AgendaConnectionTimeout") = 60
	'Application(Hosting & "_AgendaCommandTimeout") = 180
	'Application(Hosting & "_AgendaCursorLocation") = 3
	'Application(Hosting & "_AgendaRuntimeUserName") = "EmpleoLavoro"
	'Application(Hosting & "_AgendaRuntimePassword") = "1234*"

	' Determina ServerName
	'If Len(ServerName) = 0 Then
	'	ServerName = Request.ServerVariables("SERVER_NAME")
	'	Application("SERVER_NAME") = ServerName
	'End If

	'Set conn = Server.CreateObject("adodb.connection")

	'with conn
	 '   .ConnectionString = Application(ServerName & "_AgendaConnectionString")
	 '   .ConnectionTimeout = Application(ServerName & "_AgendaConnectionTimeout")
	 '   .CommandTimeout = Application(ServerName & "_AgendaCommandTimeout")
	 '   .CursorLocation = Application(ServerName & "_AgendaCursorLocation")
'PL-SQL * T-SQL
'OPEN = TransformPLSQLToTSQL (OPEN)
	 '   .Open ,Application(ServerName & "_AgendaRuntimeUserName"), Application(ServerName & "_AgendaRuntimePassword")
	'end with

	'set InicioBD = conn

	'===============================================================================================
	' Este es el cambio que se introdujo el 27/03/2006
	'===============================================================================================
	set InicioBD = connLavoro
End Function


Public Function GenerarTablaMostrarCita(ASPActual,Oficina,TipoMov,TipoCita,Cuil,Nombre,Estado,Clase,FuncionJS,EsHistorial)
'=========================================================================================
' GENERA UNA TABLA CON LA INFORMACION PARA UN CUIL DETERMINADO (SOLO LAS ACTIVAS
' O HISTORIAL)
'=========================================================================================
	Dim Rs
	Dim Consulta
	Dim Retorna
'response.write "<br>ASPActual= " & ASPActual
'response.write "<br>Oficina= " & Oficina
'response.write "<br>TipoMov= " & TipoMov
'response.write "<br>TipoCita= " & TipoCita
'response.write "<br>Cuil= " & Cuil
'response.write "<br>Nombre= " & Nombre
'response.write "<br>Estado= " & Estado
'response.write "<br>Clase= " & Clase
'response.write "<br>FuncionJS= " & FuncionJS
'response.write "<br>EsHistorial= " & EsHistorial

'	InicioBD()




    Set Rs = Server.CreateObject("adodb.recordset")

	if TipoMov = "" and TipoCita = "" then
		if EsHistorial = 0 then
			Consulta = "Select NroCita as [Nro. de Cita],Fecha,HoraDesde as [Hora Inicial],HoraHasta as [Hora Final],Cuil as [C.I.], " ',Puesto,EntrevistaNro as [Nro. de Entrevista],UltNroMovimiento as [Nro. Ult. Mov.], "
			Consulta = Consulta & " CT.Descripcion as [Tipo de Cita]" ', CE.Descripcion as [Estado Cita] "
			Consulta = Consulta & " from AgCitas C, "
			Consulta = Consulta & " AgCitaEstados CE , AgCitaTipos CT where "
			Consulta = Consulta & " C.Cuil = '" & Cuil & "' and C.Estado = " & Estado
			Consulta = Consulta & " and C.Estado = CE.Estado and C.CitaTipo = CT.CitaTipo "
		elseif EsHistorial = 1 then
			Consulta = "Select NroCita as [Nro. de Cita],c.Fecha,HoraDesde as [Hora Inicial],HoraHasta as [Hora Final],c.Cuil as [C.I.]," ',Puesto,EntrevistaNro as [Nro. de Entrevista],UltNroMovimiento as [Nro. Ult. Mov.], "
			Consulta = Consulta & " CT.Descripcion as [Tipo de Cita], CE.Descripcion as [Estado Cita], "
			Consulta = Consulta & " case C.Estado "
			Consulta = Consulta & " when  3 then mtm.descripcion "
			Consulta = Consulta & " else '' "
			Consulta = Consulta & " end "
			Consulta = Consulta & " as [Motivo Reprogramaci�n] "
			Consulta = Consulta & " from AgCitas C "
			Consulta = Consulta & "inner join AgCitaEstados CE "
			Consulta = Consulta & "	on C.Estado = CE.Estado "
			Consulta = Consulta & "inner join AgCitaTipos CT "
			Consulta = Consulta & "	on C.CitaTipo = CT.CitaTipo "
			Consulta = Consulta & "inner join AgCitaMovimientos M "
			Consulta = Consulta & "	on C.ultnromovimiento = m.nromovimiento "
			Consulta = Consulta & "inner join AGCITAMOVIMIENTOSTIPOS tm "
			Consulta = Consulta & "on m.tipomovimiento = tm.tipomovimiento "
			Consulta = Consulta & "left join agcitamovimientostiposmotivos mtm "
			Consulta = Consulta & "	on m.motivo = mtm.motivo "
			Consulta = Consulta & "and  m.tipomovimiento = mtm.tipomovimiento "
			Consulta = Consulta & "where c.cuil  = '" & Cuil & "' "
				
			'Consulta = Consulta & " from AgCitas C, "
			'Consulta = Consulta & " AgCitaEstados CE, AgCitaTipos CT, AgCitaMovimientos M, "
			'Consulta = Consulta & " agcitamovimientostiposmotivos tm where "
			'Arreglada la consulta para que muestre todo el historial de citas
			'Consulta = Consulta & "AGCITAMOVIMIENTOSTIPOS tm where"
			'Consulta = Consulta & " C.Cuil = '" & Cuil & "'"
			'Consulta = Consulta & " and C.Estado = CE.Estado and C.CitaTipo = CT.CitaTipo and C.ultnromovimiento = m.nromovimiento and m.tipomovimiento = tm.tipomovimiento"
		    'and m.motivo = tm.motivo" Comentado para que muestre todo la historia de las citas de una persona (RKL)
		end if
	else
		if EsHistorial = 0 then
			Consulta = "Select NroCita as [Nro. de Cita],Fecha,HoraDesde as [Hora Inicial],HoraHasta as [Hora Final],Cuil  as [C.I.] " ',Puesto,EntrevistaNro as [Nro. de Entrevista],UltNroMovimiento as [Nro. Ult. Mov.], "
			'Consulta = Consulta & " CT.Descripcion as [Tipo de Cita], CE.Descripcion as [Estado Cita] "
			Consulta = Consulta & " from AgCitas C, "
			Consulta = Consulta & " AgCitaEstados CE, AgCitaTipos CT where "
			Consulta = Consulta & " C.CitaTipo = " & TipoCita & " and C.Cuil = '" & Cuil & "' and C.Estado = " & Estado
			Consulta = Consulta & " and C.Estado = CE.Estado and C.CitaTipo = CT.CitaTipo"
		elseif EsHistorial = 1 then
			Consulta = "Select NroCita as [Nro. de Cita],Fecha,HoraDesde as [Hora Inicial],HoraHasta as [Hora Final],Cuil  as [C.I.], " ',Puesto,EntrevistaNro as [Nro. de Entrevista],UltNroMovimiento as [Nro. Ult. Mov.], "
			Consulta = Consulta & " CT.Descripcion as [Tipo de Cita], CE.Descripcion as [Estado Cita] "
			Consulta = Consulta & " from AgCitas C, "
			Consulta = Consulta & " AgCitaEstados CE, AgCitaTipos CT where "
			Consulta = Consulta & " C.CitaTipo = " & TipoCita & " and C.Cuil = '" & Cuil & "'"
			Consulta = Consulta & " and C.Estado = CE.Estado and C.CitaTipo = CT.CitaTipo"
		end if
	end if

'PL-SQL * T-SQL
'response.write "<br>CONSULTA= " & CONSULTA

'response.write CONSULTA    

	Set Rs = connLavoro.execute(Consulta)

	Retorna = ""

'''paginacion
	if rs.state = 1 then
		if not rs.eof then
		nActPagina	=Request("txtPagNum")

		Record=0
		nTamPagina=7

		If nActPagina = "" Then
			nActPagina=1
		Else
			nActPagina=Clng(Request("txtPagNum"))
		End If

		rs.PageSize = nTamPagina

		rs.CacheSize = nTamPagina

		nTotPagina = rs.PageCount

		If nActPagina < 1 Then
			nActPagina = 1
		End If

		If nActPagina > nTotPagina Then
			nActPagina = nTotPagina
		End If

		rs.AbsolutePage=nActPagina
		nTotRecord=0


		'''paginacion
			While rs.EOF <> True And nTotRecord < nTamPagina
		  		if Record = 0 then
		  			Retorna = Retorna & "<Table width = 80% class='" & Clase & "' border=2 bordercolor='MidnightBlue'>"
					'Rs.movefirst
					Retorna = Retorna & "<tr>"
					for each x in Rs.fields
						Retorna = Retorna & "<th align='center' bgcolor='#D9D9AE' width='20%'><b>" & x.name & "</b></th>"
					next
					Retorna = Retorna & "</tr>"
					record = 1
				end if

			HoraInicial = ConvertirMinutosAHorario(Rs.fields(2))
			HoraFinal = ConvertirMinutosAHorario(Rs.fields(3))



			Retorna = Retorna & "<tr>"
			for each x in Rs.fields

				'if x.name = "Nro. de Cita" then
                if UCase(x.name) = UCase("Nro. de Cita") then

					If FuncionJS <> "" then

						if instr(1,FuncionJS,"(") <> 0 then
							if Nombre <> "" then
								Nombre = replace(Nombre," ","_")
							end if

							if Nombre <> "" then
								Retorna = Retorna & "<td align='center' bgcolor='LightYellow'><b><a href=" & FuncionJS & "," & TipoMov & "," & TipoCita & "," & x.value & "," & Cuil & ",'" & Nombre & "')>" & x.value & "</a></b></td>"
							else
								Retorna = Retorna & "<td align='center' bgcolor='LightYellow'><b><a href=" & FuncionJS & "," & TipoMov & "," & TipoCita & "," & x.value & "," & Cuil & ")>" & x.value & "</a></b></td>"
							end if
						else
							if Nombre <> "" then
								Retorna = Retorna & "<td align='center' bgcolor='LightYellow'><b><a href=" & FuncionJS & "(" & TipoMov & "," & TipoCita & "," & x.value & "," & Cuil & ",'" & Nombre & "')>" & x.value & "</a></b></td>"
							else
								Retorna = Retorna & "<td align='center' bgcolor='LightYellow'><b><a href='" & FuncionJS & "(" & TipoMov & "," & TipoCita & "," & x.value & "," & Cuil & ")'>" & x.value & "</a></b></td>"
							end if
						end if
					else

						Retorna = Retorna & "<td align='center' bgcolor='LightYellow'><b>" & x.value & "</b></td>"
					end if
				else
					if UCase(x.name) = UCase("Hora Inicial") then
						Retorna = Retorna & "<td align='center' bgcolor='LightYellow'>" & HoraInicial & "</td>"
					elseif UCase(x.name) = UCase("Hora Final") then
						Retorna = Retorna & "<td align='center' bgcolor='LightYellow'>" & HoraFinal & "</td>"
					else
						Retorna = Retorna & "<td align='center' bgcolor='LightYellow'>" & x.value & "</td>"
					end if
				end if
			next
			Retorna = Retorna & "</tr>"
			nTotRecord=nTotRecord + 1
			rs.MoveNext

		wend
		Retorna = Retorna & "</Table>"

			Retorna = Retorna & "<table border=0 width=480>"
			Retorna = Retorna & "<tr>"
			if nActPagina > 1 then
				Retorna = Retorna & "<td align='right' width='480'>"
				Retorna = Retorna & "<a href=" & ASPActual & "?txtPagNum=" & (nActPagina - 1) & "&TxtOficina=" & Oficina & "&TipoMov=" & TipoMov & "&TipoCita=" & TipoCita & "&TxtCuil=" & Cuil & "&Nombre=" & Nombre & "&Estado=" & Estado & "><img src='" & Session("Progetto") & "/images/precedente.gif' border='0' id='image1' name='image1'>"
				Retorna = Retorna & "</td>"
			end if
			if nActPagina < nTotPagina then
				Retorna = Retorna & "<td align='right'>"
				Retorna = Retorna & "<a href=" & ASPActual & "?txtPagNum=" & (nActPagina + 1) & "&TxtOficina=" & Oficina & "&TipoMov=" & TipoMov & "&TipoCita=" & TipoCita & "&TxtCuil=" & Cuil & "&Nombre=" & Nombre & "&Estado=" & Estado & "><img src='" & Session("Progetto") & "/images/successivo.gif' border='0' id='image1' name='image1'>"
				Retorna = Retorna & "</td>"
			end if
			Retorna = Retorna & "</tr>"
			Retorna = Retorna & "</table>"

			'Retorna = Retorna & "</Table>"
		end if
	end if


	GenerarTablaMostrarCita = Retorna

'	FinBD()

	Set Rs = Nothing
End Function


Public Function MostrarDatosCitaEliminarNominacion(Oficina, TipoMovimiento, NroLoteReservas, NroCita)
'=========================================================================================
' MUESTRA LOS DATOS DE UNA CITA A LA CUAL SE QUIERE QUITAR LA NOMINACION
'=========================================================================================
'
' que parametros son de entrada
'NECESITA:
' @Oficina		 	int,
' @TipoMovimiento	int = 0,
' @NroLoteReservas	int = 0,
' @NroCita	 		int = 0,
'
' que parametros son de salida
'DEVUELVE:

'	@CitaFecha			varchar(10) output,
'	@HoraDesdeMin		int			output,
'	@HoraHastaMin		int			output,
'	@HoraDesdeHs		varchar(5)	output,
'	@HoraHastaHs		varchar(5)	output,
'	@CUIL				varchar(16) output,
'	@CitaEstado			int			output,
'	@citaEstadoDesc		varchar(50) output,
'	@Puesto				int			output,
'	@NroEntrevista		int			output,
'	@UltNroMovimento	int			output,
'
'	@Status				int			OUTPUT,	-- Status = 1 --> OK
'	@Mensaje			varchar(255)OUTPUT	-- Mensaje de Error

'---------------------------------------------------------------------------------------------------

	Dim Spx
	Dim Retorna

	Retorna = ""

	Set Rs = server.CreateObject("adodb.recordset")
	Set Spx = Server.CreateObject("adodb.command")
	with Spx
		.activeconnection = connLavoro 'InicioBD()
		.Commandtext = "AgCitaDatos"
		.commandtype = adCmdStoredProc
		'--------------------------------------------
		' CArga parametros
		'--------------------------------------------
		For Each Parameter In .Parameters
			If Parameter.Direction And adParamInput Then
				Select case ucase(Mid(Parameter.Name, 2))
					case "OFICINA"
						Parameter.value = Oficina
					CASE "TIPOMOVIMIENTO"
						Parameter.value = TipoMovimiento
					CASE "NROLOTERESERVAS"
						Parameter.value = NroLoteReservas
					CASE "NROCITA"
						if len(NroCita) = 0 then
							NroCita = 0
						end if
						Parameter.value = "" & NroCita
					CASE "STATUS"
						if len(Status) = 0 then
							Status = 0
						end if
						Parameter.value = Status
					CASE "MENSAJE"
						Parameter.value = "" & Mensaje
					CASE ELSE
						' ????
				END SELECT
			End If
		Next
		'--------------------------------------------
		' Ejecuta
		'--------------------------------------------
'PL-SQL * T-SQL
'XXX = TransformPLSQLToTSQL (XXX)
		.Execute XXX, , adexecutenorecords

		'--------------------------------------------
		' Devuelve parametros
		'--------------------------------------------
		For Each Parameter In .Parameters
			If Parameter.Direction And (adParamOutput Or adParamReturnValue) Then
				Select case ucase(Mid(Parameter.Name, 2))
					case "STATUS"
						STATUS = trim(Parameter.value)
					case "MENSAJE"
						MENSAJE = trim(Parameter.value)
					case "CITAFECHA"
						CITAFECHA = trim(Parameter.value)
					case "HORADESDEMIN"
						HORADESDEMIN = trim(Parameter.value)
					case "HORAHASTAMIN"
						HORAHASTAMIN = trim(Parameter.value)
					case "HORADESDEHS"
						HORADESDEHS = trim(Parameter.value)
					case "HORAHASTAHS"
						HORAHASTAHS = trim(Parameter.value)
					case "CUIL"
						CUIL = trim(Parameter.value)
					case "CITAESTADO"
						CITAESTADO = trim(Parameter.value)
					case "CITAESTADODESC"
						CITAESTADODESC = trim(Parameter.value)
					case "PUESTO"
						PUESTO = trim(Parameter.value)
					case "NROENTREVISTA"
						NROENTREVISTA = trim(Parameter.value)
					case "ULTNROMOVIMIENTO"
						ULTNROMOVIMIENTO = trim(Parameter.value)
					case else
						' ???
				end select
			End If
		Next
	end with

	'--------------------------------------------
	' Prepara salida
	'--------------------------------------------
	if STATUS <> 1 then
		Retorna = "Error: " & STATUS & "<br> Mensaje: " & MENSAJE
	else
        Retorna = Retorna & "<input type='hidden' name='TxtCitaFecha' value='" & CITAFECHA & "'>"
        Retorna = Retorna & "<input type='hidden' name='TxtHoraDesdeMin' value='" & HORADESDEMIN & "'>"
        Retorna = Retorna & "<input type='hidden' name='TxtHoraHastaMin' value='" & HORAHASTAMIN & "'>"
        Retorna = Retorna & "<input type='hidden' name='TxtHoraDesdeHs' value='" & HORADESDEHS & "'>"
        Retorna = Retorna & "<input type='hidden' name='TxtHoraHastaHs' value='" & HORAHASTAHS & "'>"
        Retorna = Retorna & "<input type='hidden' name='TxtCUIL' value='" & CUIL & "'>"
        Retorna = Retorna & "<input type='hidden' name='TxtCitaEstado' value='" & CITAESTADO & "'>"
        Retorna = Retorna & "<input type='hidden' name='TxtcitaEstadoDesc' value='" & CITAESTADODESC & "'>"
        Retorna = Retorna & "<input type='hidden' name='TxtPuesto' value='" & PUESTO & "'>"
        Retorna = Retorna & "<input type='hidden' name='TxtNroEntrevista' value='" & NROENTREVISTA & "'>"
        Retorna = Retorna & "<input type='hidden' name='TxtUltNroMovimento' value='" & ULTNROMOVIMIENTO & "'>"

		NOMBREYAPELLIDO = ExistePostulante(CUIL)

		Retorna = Retorna & "	<table width='100%' border=2 bordercolor='MidnightBlue'>"
		Retorna = Retorna & "			<tr>"
		Retorna = Retorna & "				<th align='center' width=10% class='tbltext1' bgcolor='#D9D9AE'><b>Fecha</b></td>"
		Retorna = Retorna & "				<th align='center' width=10% class='tbltext1' bgcolor='#D9D9AE'><b>Hora Inicio</b></td>"
		Retorna = Retorna & "				<th align='center' width=10% class='tbltext1' bgcolor='#D9D9AE'><b>Hora Fin</b></td>"
		Retorna = Retorna & "				<th align='center' width=30% class='tbltext1' bgcolor='#D9D9AE'><b>C.I.</b></td>"
		Retorna = Retorna & "				<th align='center' width=40% class='tbltext1' bgcolor='#D9D9AE'><b>Apellido y Nombre</b></td>"
		Retorna = Retorna & "			</tr>"
		Retorna = Retorna & "			<tr>"
		Retorna = Retorna & "				<td align='center' class='tbltext1' bgcolor='LightYellow'><b>" & CITAFECHA & "</b></td>"
		Retorna = Retorna & "				<td align='center' class='tbltext1' bgcolor='LightYellow'><b>" & HORADESDEHS & "</b></td>"
		Retorna = Retorna & "				<td align='center' class='tbltext1' bgcolor='LightYellow'><b>" & HORAHASTAHS & "</b></td>"
		Retorna = Retorna & "				<td align='center' class='tbltext1' bgcolor='LightYellow'><b>" & CUIL & "</b></td>"
		Retorna = Retorna & "				<td align='center' class='tbltext1' bgcolor='LightYellow'><b>" & NOMBREYAPELLIDO & "</b></td>"
		Retorna = Retorna & "			</tr>"
		Retorna = Retorna & "		</table>"
		Retorna = Retorna & "		<table width='90%'>"
		Retorna = Retorna & "			<tr>"
		Retorna = Retorna & "				<td align='right'>"
		Retorna = Retorna & "					<br><input type='submit' class='my' value='Quitar Nominaci&oacute;n'>"
		Retorna = Retorna & "				</td>"
		Retorna = Retorna & "			</tr>"
		Retorna = Retorna & "		</table>"
	end if


	'retorna el nro de cita que genera..
	MostrarDatosCitaEliminarNominacion = Retorna



'	FinBD()

	Set Spx = Nothing
End Function


''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

Public function ReporteReservas(OpcionListado,Oficina,FechaDesde,FechaHasta,TituloListado)
'=========================================================================================
' GENERA UN REPORTE DE LAS RESERVAS
'=========================================================================================
	OpcionListado = 1
	Oficina = Oficina
	FechaDesde = FechaDesde
	FechaHasta = FechaHasta
	TituloListado = TituloListado

	if OpcionListado = "" then
		OpcionListado = request("txtFechaIni")
	end if

	if Oficina = "" then
		Oficina = request("txtOficina")
	end if

	if FechaDesde = "" then
		FechaDesde = request("txtFechaIni")
	end if

	if FechaHasta = "" then
		FechaHasta = request("txtFechaFin")
	end if

	if OpcionListado = "" then
		OpcionListado = request("txtOpcion")
	end if

	if TituloListado = "" then
		TituloListado = request("txtTitulo")
	end if

		TituloListado = replace(TituloListado,"_"," ")

	Status = 1
	Mensaje = "OK"

'InicioBD()

	Dim Sp
	Dim Retorna

	Retorna = ""

	Set Sp = Server.CreateObject("adodb.command")

	Set Rec = Server.CreateObject("ADODB.Recordset")

	Sp.ActiveConnection = connLavoro 'InicioBD()

	'AgReservasReportes 'OpcionListado', 'Oficina', 'Fecha desde', 'Fecha hasta', 'Titulo listado'

	Sp.CommandText = "AgReservasReportes(" & OpcionListado & "," & Oficina & ",'" & FechaDesde & "','" & FechaHasta & "','" & TituloListado & "')"

	Sp.CommandType = 4 'adCmdStoredProc


'PL-SQL * T-SQL
	Set Rec = Sp.Execute()
	Set Sp = Nothing

'''paginacion
	if Rec.state = 1 then
		if not Rec.eof then
		nActPagina	=Request("txtPagNum")

		Record=0
		nTamPagina=15

		If nActPagina = "" Then
			nActPagina=1
		Else
			nActPagina=Clng(Request("txtPagNum"))
		End If

		rec.PageSize = nTamPagina

		rec.CacheSize = nTamPagina

		nTotPagina = rec.PageCount

		If nActPagina < 1 Then
			nActPagina = 1
		End If

		If nActPagina > nTotPagina Then
			nActPagina = nTotPagina
		End If

		rec.AbsolutePage=nActPagina
		nTotRecord=0


		'''paginacion
			While rec.EOF <> True And nTotRecord < nTamPagina
		  		if Record = 0 then
					Retorna = Retorna & "	<tr bgcolor='#D9D9AE'>"

					for each x in rec.Fields
						Retorna = Retorna & "	<td	align='center' class='tbltext1'><b>" & x.name & "</b></td>"
					next

					Retorna = Retorna & "	</tr>"
					record = 1
				end if



			'do while not Rec.eof
				Retorna = Retorna & "	<tr	bordercolor='LightYellow' bgcolor='#EAEAD0'>"

				for each x in rec.Fields
					Retorna = Retorna & "	<td	bordercolor='LightYellow' align='center' class='tbltext1'><font size='1'>" & x.value & "</font></td>"
				next

				Retorna = Retorna & "	</tr>"
				'Rec.movenext
			'loop
				nTotRecord=nTotRecord + 1
				rec.MoveNext

			wend

	'''paginacion
		Retorna = Retorna & "<tr><td colspan='13' align='right'>"
				Retorna = Retorna & "<table border='0' width='470'>"
				Retorna = Retorna & "<tr>"

		Retorna = Retorna & "<form name='frm2' action='MostrarReporteReservas.asp' method='post'>"


		Retorna = Retorna & "	<input type='hidden' id='txtPagNum' name='txtPagNum' value>"
		Retorna = Retorna & "	<input type='hidden' id='txtFechaIni' name='txtFechaIni' value>"
		Retorna = Retorna & "	<input type='hidden' id='txtFechaFin' name='txtFechaFin' value>"
		Retorna = Retorna & "	<input type='hidden' id='txtOficina' name='txtOficina' value>"
		Retorna = Retorna & "	<input type='hidden' id='txtTitulo' name='txtTitulo' value>"
		Retorna = Retorna & "	<input type='hidden' id='txtOpcion' name='txtOpcion' value>"


			TituloListado = replace(TituloListado," ","_")

				if nActPagina > 1 then

					Retorna = Retorna & "<td align='right' width='480'>"
						Retorna = Retorna & "<a href=Javascript:AssegnaVal('" & (nActPagina - 1) & "','" & FechaDesde & "','" & FechaHasta & "','" & Oficina & "','" & TituloListado & "','" & OpcionListado & "')><img src='" & Session("Progetto") & "/images/precedente.gif' border='0' id='image1' name='image1'>"
					Retorna = Retorna & "</td>"

				end if
				if nActPagina < nTotPagina then

					Retorna = Retorna & "<td align='right'>"
						Retorna = Retorna & "<a href=Javascript:AssegnaVal('" & (nActPagina + 1) & "','" & FechaDesde & "','" & FechaHasta & "','" & Oficina & "','" & TituloListado & "','" & OpcionListado & "')><img src='" & Session("Progetto") & "/images/successivo.gif' border='0' id='image1' name='image1'>"
					Retorna = Retorna & "</td>"

				end if
		Retorna = Retorna & "</form>"
		Retorna = Retorna & "		</tr>"
		Retorna = Retorna & "	</table>"
		Retorna = Retorna & "</td></tr>"
		end if
	end if



'''paginacion

	FinBD()

	ReporteReservas = Retorna

	Set Rec = nothing

end function




''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''


Public Function ConvertirMinutosAHorario(Minutos)
'=========================================================================================
' CONVIERTE LOS MINUTOS A HORARIO EN FORMATO HH:MM
'=========================================================================================
	Dim retorna

	retorna = ""

	Hs=ObtenerHoras(Minutos)
	Mins=ObtenerMinutos(Minutos)

	retorna =Hs & ":" & Mins

	ConvertirMinutosAHorario = retorna
End Function

Public Function ObtenerHoras(Valor)
'=========================================================================================
' OBTIENE LAS HORAS
'=========================================================================================
	Dim retorna
	'Response.Write Valor & "<br>"

	Valor = clng(Valor)/60

	retorna = ""

	pos=instr(1,cstr(Valor),",")

	if pos = 0 then
		retorna = Valor
	else
		retorna = cstr(left(Valor,pos-1))
	end if

	ObtenerHoras = retorna
End Function

Public Function ObtenerMinutos(Valor)
'=========================================================================================
' OBTIENE LOS MINUTOS
'=========================================================================================

	Dim retorna
	'Valor = clng(Valor)/60

	pos =instr(1,cstr(Valor),",")
	retorna = ""


	If pos <> 0 Then
		inter = round(mid(Valor,pos)  * 60 )
		retorna = inter
	Else
		retorna = 0
	End If

	select case retorna

	case 0,1,2,3,4,5,6,7,8,9
		retorna = "0" & retorna
	case else

	end select

	ObtenerMinutos = retorna
End Function


Public function ExistePostulante(Cuil)
'=========================================================================================
' VERIFICA LA EXISTENCIA DEL POSTULANTE A TRAVES DEL CUIL Y DEVUELVE EL NOMBRE Y APELLIDO
'=========================================================================================
	Dim RsBusca
	Dim SqlBusca

	ExistePostulante = ""

	set RsBusca = server.CreateObject("Adodb.recordset")

	SqlBusca = "Select cognome,nome from Persona where cod_fisc='" & cstr(Cuil) & "'"

'PL-SQL * T-SQL
'SQLBUSCA = TransformPLSQLToTSQL (SQLBUSCA)
	set RsBusca = CC.execute(SqlBusca)

	if not RsBusca.EOF then
		RsBusca.MoveFirst
		ExistePostulante = RsBusca.Fields(0) & " " & RsBusca.fields(1)
	else
		ExistePostulante = ""
	end if

	set RsBusca = nothing
End function


Public Function ModeloGenerarTabla(Stored,Parametros,Valores,ASPLink,ASPActual,TamanioPagina,Clase)
'=========================================================================================
' MODELO
'=========================================================================================
	Dim Rs
	Dim cmdConsulta
	Dim Retorna

	Set cmdConsulta = Server.CreateObject("adodb.command")
	Set Rs = Server.CreateObject("adodb.recordset")

	sParametros = split(Parametros,",")
	sValores = split(Valores,",")

	With cmdConsulta
		.ActiveConnection = ConnLavoro
		.CommandTimeout = 1200
		.CommandText = Stored & "(" & Valores & ")"
		.CommandType = 4
		for i = lbound(sParametros) to ubound(sParametros)
			'Nombre = "@" & sParametros(i)
			'Response.Write Nombre & "<br>"
			'.Parameters(" & cstr(Nombre) & ").value = sValores(i)
			CoupleParameters = CoupleParameters & "&" & sParametros(i) & "=" & sValores(i)
		next
		'Response.End
		'.Parameters("@Oficina").Value = Oficina
'PL-SQL * T-SQL
		Set Rs = .Execute()
	End With

	if right(CoupleParameters,1) = "&" then
		CoupleParameters = left(CoupleParameters,len(CoupleParameters)-1)
	end if


	Retorna = ""
	ExisteColCuil = 0

'''paginacion
	if rs.state = 1 then
		if not rs.eof then
			Retorna = Retorna & "<Table width = 100% class='" & Clase & "' border=2 bordercolor='MidnightBlue'>"

			nActPagina	=Request("txtPagNum")

			Record=0
			nTamPagina=TamanioPagina

			If nActPagina = "" Then
				nActPagina=1
			Else
				nActPagina=Clng(Request("txtPagNum"))
			End If

			rs.PageSize = nTamPagina

			rs.CacheSize = nTamPagina

			nTotPagina = rs.PageCount

			If nActPagina < 1 Then
				nActPagina = 1
			End If

			If nActPagina > nTotPagina Then
				nActPagina = nTotPagina
			End If

			rs.AbsolutePage=nActPagina
			nTotRecord=0

		'''paginacion
			While rs.EOF <> True And nTotRecord < nTamPagina
		  		if Record = 0 then
					Retorna = Retorna & "<tr>"
					for i = 1 to rs.fields.count - 1
						if trim(ucase(rs.fields(i).name)) = "CUIL" then
							ExisteColCuil = 1
						end if
						Retorna = Retorna & "<th align='center' bgcolor='#D9D9AE' width='15%'><b>" & rs.fields(i).name & "</b></th>"
					next

					if ExisteColCuil = 1 then
						Retorna = Retorna & "<th align='center' bgcolor='#D9D9AE' width='100'><b>Apellido y Nombre</b></th>"
					end if
					Retorna = Retorna & "</tr>"
					record = 1
				end if

				Cuil = ""
				NroEntrevista = 0
				Cuil = Rs.fields(1).value
				NroEntrevista = Rs.fields(2).value

				Retorna = Retorna & "<tr>"
				for i = 1 to rs.fields.count - 1
					Retorna = Retorna & "<td align='center' bgcolor='LightYellow'><b><a class='tbltext1' href='" & ASPLink & "?" & RS.FIELDS(0).VALUE & "'>" & rs.fields(i) & "</a></b></td>"
				next

				if ExisteColCuil = 1 then
					Retorna = Retorna & "<td align='center' bgcolor='LightYellow'><b><a class='tbltext1' href='" & ASPLink & "?" & RS.FIELDS(0).VALUE & "'>" & ExistePostulante(rs.fields("CUIL")) & "</a></b></td>"
				end if

				nTotRecord=nTotRecord + 1
				rs.MoveNext
			wend

			Retorna = Retorna & "<table border=0 width=480>"
			Retorna = Retorna & "<tr>"
			if nActPagina > 1 then
				Retorna = Retorna & "<td align='right' width='480'>"
				Retorna = Retorna & "<a href=" & ASPActual & "?txtPagNum=" & (nActPagina - 1) & CoupleParameters & "><img src='" & Session("Progetto") & "/images/precedente.gif' border='0' id='image1' name='image1'>"
				Retorna = Retorna & "</td>"
			end if
			if nActPagina < nTotPagina then
				Retorna = Retorna & "<td align='right'>"
				Retorna = Retorna & "<a href=" & ASPActual & "?txtPagNum=" & (nActPagina + 1) & CoupleParameters & "><img src='" & Session("Progetto") & "/images/successivo.gif' border='0' id='image1' name='image1'>"
				Retorna = Retorna & "</td>"
			end if
			Retorna = Retorna & "</tr>"
			Retorna = Retorna & "</table>"

			Retorna = Retorna & "</Table>"
		end if
	end if

	''''FALTARIA EL REQUEST DE LOS PARAMETROS EN LA PAG QUE INVOCA LA FUNCION''''

	ModeloGenerarTabla = Retorna

	Set Rs = Nothing
End Function


Public Function FinBD()
'=========================================================================================
' EN DESUSO
'=========================================================================================
	Set connLavoro = nothing
End Function

End Class
%>
