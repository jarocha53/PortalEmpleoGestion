<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #include file="Soporte/ProcCalendario.asp" -->
<!-- #include Virtual="/strutt_testa2.asp" -->
<!-- #include Virtual="/include/openconn.asp" -->
<!-- #include Virtual="/Pgm/Agenda_Entrevistas/ClsInterfaceBD.asp"-->

<style>
.EstiloComboFecha {
	background-color:LightYellow;
	font-family: Verdana; 
	font-size: 7pt; 
	color: navy;
	font-weight: bold; 
	border-style:ridge;
	text-transform:uppercase;
}
.EstiloPalabra {
	font-family: Verdana; 
	font-size: 7pt; 
	color: navy;
	font-weight: bold; 
	text-transform:uppercase;
}
.EstiloLink:hover{
	color: Black;
	font-weight: bolder;
}
.EstiloLink:visited
{
	color: Black;
	font-weight: bolder;
}
.EstiloLink:link
{
	color: Black;
	font-weight: bolder;
}
.EstiloLink:active
{
	color: Black;
	font-weight: bolder;
}
</style>

<table border="0" width="530" cellspacing="0" cellpadding="0" height="81">
   <tr>
     <td width="530" background="<%=Session("Progetto")%>/images/titoli/strumenti2g.gif" height="81" valign="bottom" align="right">
       <table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
         <tr>
          <td width="100%" valign="top" align="right">
          		<b class="tbltext1a">Calendario</b>
          </td>
         </tr>
       </table>
     </td>
   </tr>
</table>
<br>
<table cellpadding="0" cellspacing="0" width="500" border="0">
	<tr height="18">
		<td class="sfondomenu" height="18" width="67%"> <span class="tbltext0"> 
		<b>Seleccionar Mes y A�o</b>
		</td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
		<td valign="middle" align="right" class="tbltext1" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">
		
		</td>
    </tr>
    <tr>
	<td colspan=3>
		<br><p class="tbltext1">Seleccionar un d�a para ingresar la cita deseada.</p><br>
	</td>
	<td valign="middle" align="right" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1"></td>
	</tr>
	 
	<tr height="2">
		<td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif">
		</td>
	</tr>
</table>




<!--Inicio del Formulario-->
<%
	dim Cl
	dim Inter
			
	set Cl = new Calendario
	set Inter = new InterfaceBD
%>

<%


sOficina = Request("TxtOficina")

if sOficina = "" then 
	sOficina = request("Ofi")
end if 

sTipoMov = Request("CmbTipoMov")
sTipoCita = Request("CmbTipoCita")
sDuracion = Request("CmbDuracion")

if sDuracion = "" then 
	sDuracion = request("Dur")
end if
if sTipoMov = "" then
	sTipoMov = request("Mov")
end if
if sTipoCita = "" then 
	sTipoCita = request("Cita")
end if 

if sDuracion = "" then 
	sDuracion = request("Dur")
end if


sVieneDeDatos = request("Viene")

if sVieneDeDatos = "" then 
	sVieneDeDatos = request("TxtViene")
end if 

sNroEntrevista = (request("sNroEntrevista"))
sTarea = (request("sTarea"))

'Response.Write sVieneDeDatos
''''estos son unicamente si es reprogramacion

sNroCita  = Request("NroCita")
sIdMotivo  = Request("IdMotivo")
sMotivoDesc  = Request("MotivoDesc")
sNombre = request("Nombre")

if sNombre = "" then 
	sNombre = request("TxtNom")
end if 
sNombre = replace(sNombre," ","_")

if sIdMotivo = "" then 
	sIdMotivo = request("CmbMotivo")
end if 

if sMotivoDesc = "" then 
	sMotivoDesc = request("TxtMotivoDesc")
end if

sCuil       = Request("Cuil")
if sCuil = "" then 
	sCuil = request("TxtCuil")
end if

''esto es si es una reprogramacion pasa mas valores..
'if sTipoMov = 6 then 
%>

<%IDP = ObtenerIdPersona(sCuil)%>

<%if sVieneDeDatos = "1" then %>
	<p CLASS='tbltext1a' align='right'><b><a href='/pgm/Iscr_Utente/UTE_ModDati.asp?Idpers=<%=IDP%>'>Reanudar Entrevista</a></b></p>
<%end if%>

<%if sVieneDeDatos = "2" then %>
	<p CLASS='tbltext1a' align='right'><b><a href='/pgm/Entrevistas/EntrevistaAccion.asp?Idpers=<%=IDP%>&sCuil=<%=sCuil%>&sNroEntrevista=<%=Session("NroEntrevista")%>&sTarea=<%=Session("TareaEntrevista")%>'>Reanudar Entrevista</a></b></p>
<%end if%>

<div align="center">
	<table width="500" border="0">
			<% if sNombre <> "" then %>
			<tr>
				<td align="right" class="tbltext1"><b>C.I.: </b></td>
				<td align="left" class="tbltext1"><%=sCuil%></td>
			</tr>
			<tr>
				<td align="right" class="tbltext1"><b>Nombre: </b></td>
				<td align="left" class="tbltext1"><%=ucase(replace(sNombre,"_"," "))%></td>
			</tr>
			<%end if %>
		<tr>
			<td class="tbltext1" align="right"><b>Tipo de Movimiento:</b></td>
			<td class="tbltext1" align="left"><%=Inter.ObtenerValor("AgCitaMovimientosTipos",sTipoMov)%></td>
		</tr>
		<tr>
			<td class="tbltext1" align="right"><b>Tipo de Cita:</b></td>
			<td class="tbltext1" align="left"><%=Inter.ObtenerValor("AgCitaTipos",sTipoCita)%></td>
		</tr>
		<tr>
			<td class="tbltext1" align="right"><b>Duraci�n:</b></td>
			<td class="tbltext1" align="left"><%=sDuracion%> Minutos</td>
		</tr>
	</table>
</div>
<br>
	<div align="center">
		<table width="50%" border="0">
			<tr>
				<form name="FrmCalendario" Action="MostrarCalendario.asp" method="POST">
					<input type="hidden" name="TxtOficina" value="<%=sOficina%>">
					<input type="hidden" name="CmbTipoMov" value="<%=sTipoMov%>">
					<input type="hidden" name="CmbTipoCita" value="<%=sTipoCita%>">
					<input type="hidden" name="CmbDuracion" value="<%=sDuracion%>">
					<input type="hidden" name="TxtViene" value="<%=sVieneDeDatos%>">
					<input type="hidden" name="TxtCuil" value="<%=sCuil%>">
					<input type="hidden" name="TxtNom" value="<%=sNombre%>">
					<input type="hidden" name="NroCita" value="<%=sNroCita%>">
					<input type="hidden" name="IdMotivo" value="<%=sIdMotivo%>">
					<input type="hidden" name="MotivoDesc" value="<%=sMotivoDesc%>">
																				
					<td colspan="3" align="center">
						<%
							Mes = Request("Mes")

							If Mes = "" then Mes = Month(date)

							Response.write Cl.CrearComboAgenda("Mes","EstiloComboFecha","OnChange='this.form.submit()'","Meses","",Mes,"")
						%>
						&nbsp;&nbsp;
						<%
							Anio = Request("Anio")
						
							If Anio = "" then Anio = Year(date)
		
							Response.write Cl.CrearComboAgenda("Anio","EstiloComboFecha","OnChange='this.form.submit()'","Anios","",Anio,"")
						%>
					</td>
				</form>
			</tr>
			<tr>
				<td colspan="3" align="center">
					<br>
					<% 
						
						ssAnio = Anio
						ssMes = Cl.ObtenerMes(Mes,"Act")
											
						String1 = Inter.GenerarArrayTipoDia(sOficina,ssAnio,ssMes,sDuracion,sCuil,sTipoMov,sTipoCita)
						'response.write sOficina & " " &  ssAnio & " " & ssMes & " " & sDuracion & " " & sCuil & " " & sTipoMov & " " & sTipoCita 
						String2 = Inter.GenerarArraySaltos(sOficina,ssAnio,ssMes,sDuracion,false,sCuil,sTipoMov,sTipoCita)
						
						
						'******************************************************************'
						'AgAgendaEstadoConsultaMesCuil SP MODIFICADO VANI******************'
						'******************************************************************'
						
						'Response.Write "ssmes: " & ssmes & "<br>"
						'Response.Write "ssAnio: " & ssAnio & "<br>"
						'Response.Write "String1: " & String1 & "<br>"
						'Response.Write "String2: " & String2 & "<br>"
						'Response.Write "sOficina: " & sOficina & "<br>"
						'Response.Write "sTipoMov: " & sTipoMov & "<br>"
						'Response.Write "sTipoCita: " & sTipoCita & "<br>"
						'Response.Write "sDuracion: " & sDuracion & "<br>"
						'Response.Write "sNroCita: " & sNroCita & "<br>"
						'Response.Write "sIdMotivo: " & sIdMotivo & "<br>"
						'Response.Write "sMotivoDesc: " & sMotivoDesc & "<br>"
						'Response.Write "sCuil: " & sCuil & "<br>"
						'Response.Write "sNombre: " & sNombre & "<br>"
						'Response.Write "sVieneDeDatos: " & sVieneDeDatos & "<br>"
						
						'x tipo mov 6, reprogramacion paso mas parametros
						Response.write Cl.ProcesarCalendario (ssMes,ssAnio,String1,String2,sOficina,sTipoMov,sTipoCita,sDuracion,sNroCita,sIdMotivo,sMotivoDesc,sCuil,sNombre,sVieneDeDatos) 


						'Response.write Cl.ProcesarCalendario (Cl.ObtenerMes(Mes,"Act"),Anio,"01|F|05|L|06|A|16|L|21|A|25|C|27|C","01|MostrarCalendario.asp|05|MostrarCalendario.asp|06|MostrarCalendario.asp|16|MostrarCalendario.asp|21|MostrarCalendario.asp|25|MostrarCalendario.asp|27|MostrarCalendario.asp") 
					%>
				</td>
			</tr>
<!---->
					<% 
						MesPre = Cl.ObtenerMes(ssMes,"Pos")
						AnioPre = Cl.ObtenerAnio(ssMes,ssAnio,"Pos")
							
						
						MesPos = Cl.ObtenerMes(MesPre,"Pos")
						AnioPos =Cl.ObtenerAnio(MesPre,AnioPre,"Pos")
						
						
					%>
					
			<tr Class="EstiloPalabra">
				<td align="left">
					<b><%= mesi(Int(Cl.ObtenerMes(ssMes,"Pos"))-1) %></b>
				</td>
				<td width="40" bgcolor="#FFFFFF">
					&nbsp;
				</td>
				<td align="right">	
					<b><%= mesi(Int(Cl.ObtenerMes(MesPre,"Pos")) -1) %></b>
				</td>
			</tr>
			
			<tr>
				<td align="left">

					<%
						StringMA1 = Inter.GenerarArrayTipoDia(sOficina,AnioPre,MesPre,sDuracion,sCuil,sTipoMov,sTipoCita)
						StringMA2 = Inter.GenerarArraySaltos(sOficina,AnioPre,MesPre,sDuracion,false,sCuil,sTipoMov,sTipoCita)
					
						'x tipo mov 6, reprogramacion paso mas parametros
						Response.write Cl.ProcesarCalendario (MesPre,AnioPre,StringMA1,StringMA2,sOficina,sTipoMov,sTipoCita,sDuracion,sNroCita,sIdMotivo,sMotivoDesc,sCuil,sNombre,sVieneDeDatos) 
						
											
					%>
				</td>
				<td width="40">
					&nbsp;
				</td>
				<td align="right">

						<%
						StringMP1 = Inter.GenerarArrayTipoDia(sOficina,AnioPos,MesPos,sDuracion,sCuil,sTipoMov,sTipoCita)
						StringMP2 = Inter.GenerarArraySaltos(sOficina,AnioPos,MesPos,sDuracion,false,sCuil,sTipoMov,sTipoCita)
				
						'x tipo mov 6, reprogramacion paso mas parametros
						Response.write Cl.ProcesarCalendario (MesPos,AnioPos,StringMP1,StringMP2,sOficina,sTipoMov,sTipoCita,sDuracion,sNroCita,sIdMotivo,sMotivoDesc,sCuil,sNombre,sVieneDeDatos) 

					%>				
				</td>
			</tr>
		</table>
	</div>
<br>
<div align="right">
	<table class="tbltext1" border=1 bordercolor="white">
		<tr>
		<td colspan=4 align="right"><b>Referencias</b></td>
		</tr>
		<tr>
		<td colspan=4 align="right"><br></td>
		</tr>
		<tr>
			<td width=10 bgcolor="#CCCCCC"></td>		
			<td colspan=4>No Disponible</td>
		</tr>
		<tr>
			<td width=10 bgcolor="#FF0000"></td>
			<td>Completo</td>
			<td width=10 bgcolor="#FF9900"></td>
			<td>Grado de Ocupaci�n Alto</td>
			<td width=10 bgcolor="#FFFF00"></td>
			<td>Grado de Ocupaci�n Bajo</td>
			<td width=10 bgcolor="#33FF99"></td>
			<td>Libre</td>
		</tr>
		<tr>
			<td width=10 bgcolor="#00CCFF"></td>
			<td colspan=4>Feriado</td>
		</tr>
		
	</table>
</div>
		<%
set Cl = nothing
Set Inter = nothing
		
function ObtenerIdPersona(Cuil)
	dim rs
	set rs = server.CreateObject("adodb.recordset")
	
    VariableSQL = "select id_persona from PERSONA where cod_fisc = '" & Cuil & "'"
    'PL-SQL * T-SQL
    VariableSQL = TransformPLSQLToTSQL(VariableSQL)

	set rs = cc.execute(VariableSQL)
	
	if not rs.eof then 
		rs.movefirst
		ObtenerIdPersona = Rs.fields("id_persona")
	end if
	
	set rs =nothing
end function
%>
<!-- #include Virtual="include/closeconn.asp" -->
<!-- #include Virtual="/strutt_coda2.asp" -->
