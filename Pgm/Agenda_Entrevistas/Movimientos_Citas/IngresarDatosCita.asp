<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #include file="Soporte/ProcCalendario.asp" -->
<!-- #include Virtual="/strutt_testa2.asp" -->
<!-- #include Virtual="include/openconn.asp" -->
<!-- #include Virtual="/Pgm/Agenda_Entrevistas/ClsInterfaceBD.asp"-->

<table cellpadding="0" cellspacing="0" width="500" border="0">
	<tr>
		<td width="500">
<table border="0" width="530" cellspacing="0" cellpadding="0" height="81">
   <tr>
     <td width="530" background="<%=Session("Progetto")%>/images/titoli/strumenti2g.gif" height="81" valign="bottom" align="right">
       <table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
         <tr>
          <td width="100%" valign="top" align="right">
          		<b class="tbltext1a">Ingreso de datos de la Cita</b>
          </td>
         </tr>
       </table>
     </td>
   </tr>
</table>
<br>
<table cellpadding="0" cellspacing="0" width="500" border="0">
	<tr height="18">
		<td class="sfondomenu" height="18" width="67%"> <span class="tbltext0"> 
		<b>Ingresar datos de la Cita</b>
		</td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
		<td valign="middle" align="right" class="tbltext1" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">
		
		</td>
    </tr>
    <tr>
	<td colspan=3>
		<br><p class="tbltext1">Ingresar la información correspondiente a la cita.</p><br>
	</td>
	<td valign="middle" align="right" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1"></td>
	</tr>
	 
	<tr height="2">
		<td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif">
		</td>
	</tr>
</table>

<!--<table cellpadding="0" cellspacing="0" width="500" border="1">
	<tr>
		<td width="500">-->
<!--Inicio del Formulario-->
<!--La primera tabla se llenaria con las franjas horarias disponibles q devuelve el sp
para ese dia en particular-->



<script language="javascript">
function ControlarDatos()
{
	if (document.FrmDatos.TxtCuil.value == "")
	{
		alert("Debe ingresar una C.I.");
		return false;
	}
//	if (document.FrmDatos.CmbMotivo.value == "")
//	{
//		alert("Debe ingresar el motivo");
//		return false;
//	}
	if (document.FrmDatos.HDesde.value == "")
	{
		alert("Debe ingresar un horario");
		return false;
	}
}
</script>
<script src="../FuncionesAgenda.js">
</script>
<% 

SFecha    = Request("data")                       
STipoCita = Request("Cita")
STipoMov = Request("Mov")
SDuracion = Request("Dur")
SOficina  = Request("Ofi")
if sOficina = "" then 
	sOficina = request("Oficina")
end if 
sCuil = request("Cuil")
sNombre = request("Nombre")
sVieneDeDatos = request("Viene")

'if STipoMov = "6" then 
	sNroCita = request("NroCita")
	sIdMotivoReprog = request("IdMotivo")
	sMotivoDescReprog = request("MotivoDesc")
	'sCuil = Cuil
'end if


Dim Cl
Dim Inter

Set Inter = New InterfaceBD
Set Cl = New Calendario

NomDia = Cl.ObtenerNombreDia(cint(weekday(SFecha,2)))
NomMes = Cl.ObtenerNombreMes(cint(month(SFecha)))

FranjasDisponibles = Inter.GenerarStringFranjasDisponibles(SOficina,cstr(SFecha),1,0,SDuracion)
ValorElegido = ""


CadenaPagina = "IngresarDatosCita.asp?data=" & SFecha & "&Ofi=" & sOficina & "&Mov=" & STipoMov & "&Cita=" & STipoCita & "&Dur=" & sDuracion & "&NroCita=" & sNroCita & "&IdMotivo=" & sIdMotivoReprog & "&MotivoDesc=" & sMotivoDescReprog & "&Cuil=" & sCuil & "&Nombre=" & sNombre & "&Viene=" & sVieneDeDatos

'Response.Write CadenaPagina
%>

<%IDP = ObtenerIdPersona(sCuil)%>

	<table width="500" border="0">
	<tr><td>
<%if sVieneDeDatos = "1" then %>
	<p CLASS='tbltext1a' align='right'><b><a href='/pgm/Iscr_Utente/UTE_ModDati.asp?Idpers=<%=IDP%>'>Reanudar Entrevista</a></b></p>
<%end if%>

<%if sVieneDeDatos = "2" then %>
	<p CLASS='tbltext1a' align='right'><b><a href='/pgm/Entrevistas/EntrevistaAccion.asp?Idpers=<%=IDP%>&sCuil=<%=sCuil%>&sNroEntrevista=<%=Session("NroEntrevista")%>&sTarea=<%=Session("TareaEntrevista")%>'>Reanudar Entrevista</a></b></p>
<%end if%>
	</td></tr>
	</table>
<div align="center">
	<table width="500" border="0">
			<% if sNombre <> "" then %>
			<tr>
				<td align="right" class="tbltext1"><b>C.I.: </b></td>
				<td align="left" class="tbltext1"><%=sCuil%></td>
			</tr>
			<tr>
				<td align="right" class="tbltext1"><b>Nombre: </b></td>
				<td align="left" class="tbltext1"><%=ucase(replace(sNombre,"_"," "))%></td>
			</tr>
			<%end if %>
		<tr>
			<td class="tbltext1" align="right"><b>Tipo de Movimiento:</b></td>
			<td class="tbltext1" align="left"><%=Inter.ObtenerValor("AgCitaMovimientosTipos",sTipoMov)%></td>
		</tr>
		<tr>
			<td class="tbltext1" align="right"><b>Tipo de Cita:</b></td>
			<td class="tbltext1" align="left"><%=Inter.ObtenerValor("AgCitaTipos",sTipoCita)%></td>
		</tr>
		<tr>
			<td class="tbltext1" align="right"><b>Duración:</b></td>
			<td class="tbltext1" align="left"><%=sDuracion%> Minutos</td>
		</tr>
	</table>
</div>

<br>
<div align="center">
<table border=0><tr><td>
	<table border="0" width="70%">
		<tr>
			<td>
				<div align="center">
					<table 	bordercolor="MidnightBlue"	border="2" width="350">
						<tr>
							<th align="center" colspan=4 class="tbltext1" bgcolor="#D9D9AE"><b><%=NomDia%>&nbsp;&nbsp;<%=day(SFecha)%>&nbsp;&nbsp;de&nbsp;&nbsp;<%=NomMes%>&nbsp;&nbsp;de&nbsp;&nbsp;<%=year(SFecha)%></b></th>
						</tr>
						<%=Inter.GenerarFilasRangosPosibles(CadenaPagina,SOficina,cstr(SFecha),1,0,SDuracion)%>
					</table>
				</div>
			</td>	
		</tr>
	</table>
		<!--<tr><td><br></td></tr>
		<tr>
			<td>
				<div align="center">-->
					<table  border="0" width="350">
						<form name="FrmDatos" action="MostrarCitaProgramada.asp" method="post" onsubmit="return ControlarDatos();">
							<input type = "hidden" name="Fecha" value="<%=SFecha%>">
							<input type = "hidden" name="TipoCita" value="<%=STipoCita%>">
							<input type = "hidden" name="TipoMov" value="<%=STipoMov%>">
							<input type = "hidden" name="Duracion" value="<%=SDuracion%>">
							<input type = "hidden" name="Oficina" value="<%=SOficina%>">
							<input type = "hidden" name="NroCita" value="<%=sNroCita%>">
							<input type = "hidden" name="IdMotivoReprog" value="<%=sIdMotivoReprog%>">
							<input type = "hidden" name="MotivoDescReprog" value="<%=sMotivoDescReprog%>">
							<input type="hidden" name="TxtNombre" value="<%=sNombre%>">
							<input name="TxtCuil" type="hidden" value="<%=sCuil%>">
							<input name="TxtViene" type="hidden" value="<%=sVieneDeDatos%>">
							<tr>
								<td class="tbltext1"><b>Hora Inicio</b></td>
								<td class="tbltext1"><%=Inter.GenerarComboHorario(SOficina,cstr(SFecha),1,0,SDuracion,"HDesde","textblack","")%></td>
								<!--<td class="tbltext1"><b>Duración (min)</b></td>
								<td>
									<select class="textblack">
										<option>30</option>
										<option>60</option>
									</select>
								</td>-->
							</tr>
							<!--<tr>
								<td class="tbltext1"><b>CUIL</b></td>
								<td><input name="TxtCuilNH" type="text" disabled class="textblack" style="TEXT-TRANSFORM:uppercase" maxlength="11" size="20">
								<input name="TxtCuil" type="hidden">
								<a href="Javascript:IrA('../BuscarPostulante.asp')" ID="imgPunto1" name="imgPunto1" onmouseover="javascript:window.status='';return true"><img border="0" src="<%=Session("Progetto")%>/images/bullet1.gif"></a></td>
							</tr>
							<tr>
								<td class="tbltext1"><b>Apellido y Nombre</b></td>
								<td class="tbltext1">
								<input name="TxtNombre" type="text" size="40" disabled class="textblack" style="TEXT-TRANSFORM:uppercase" disabled>
								</td>
							</tr>
							<%MotivoResultado = Inter.GenerarComboMotivos("CmbMotivo","textblack","","",STipoMov)
						
							if STipoMov <> "6" and  MotivoResultado <> "" then 
							%>
							<tr>
								<td class="tbltext1"><b>Motivo</b></td>
								<td><%=MotivoResultado%>
								</td>			
							</tr>
							<% end if 
							if STipoMov <> "6" then %>
							<tr>
								<td class="tbltext1"><b>Observaciones</b></td>
								<td><input name="TxtObservaciones" type="text" class="textblack" style="TEXT-TRANSFORM:uppercase" WIDTH="220px"></td>
							</tr>
							<%end if%>-->
							<tr>
								<td colspan=4 align = right><br>
									<input type = "submit" class="my" value="Confirmar Cita">
								</td>
							</tr>
						</form>
					</table>
					<!--</td>
					</tr>
					</table>-->
			<!--	</div>
			</td>
		</tr>
	</table>-->
</div>
<br><br>

</td>
</tr>
</table>
<%

Set Inter = nothing
Set Cl = nothing
		
function ObtenerIdPersona(Cuil)
	dim rs
	set rs = server.CreateObject("adodb.recordset")
	
    VariableSQL = "select id_persona from PERSONA where cod_fisc = '" & Cuil & "'"
    'PL-SQL * T-SQL
    VariableSQL = TransformPLSQLToTSQL(VariableSQL)

	set rs = cc.execute(VariableSQL)
	
	if not rs.eof then 
		rs.movefirst
		ObtenerIdPersona = Rs.fields("id_persona")
	end if
	
	set rs =nothing
end function
%>

</td>
</tr>
</table>

<!-- #include Virtual="/strutt_coda2.asp" -->

