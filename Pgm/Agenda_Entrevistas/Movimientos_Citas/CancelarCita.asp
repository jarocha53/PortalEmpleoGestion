<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #include file="Soporte/ProcCalendario.asp" -->
<!-- #include Virtual="/strutt_testa2.asp" -->
<!-- #include Virtual="/Pgm/Agenda_Entrevistas/ClsInterfaceBD.asp"-->

<script language="javascript">
function RecargarCombos()
{
	var ValorMov
	var ValorCita
	
	ValorMov = document.FrmDatos.CmbTipoMov.options[document.FrmDatos.CmbTipoMov.selectedIndex].value
	ValorCita = document.FrmDatos.CmbTipoCita.options[document.FrmDatos.CmbTipoCita.selectedIndex].value
	Agrupacion = document.FrmDatos.TxtAgrupacion.value;
	
	window.navigate ("CancelarCita.asp?TipoMov="+ValorMov+"&TipoCita="+ValorCita+"&Agrupacion="+Agrupacion);

}
function ControlarDatos()
{
	if (document.FrmDatos.CmbTipoMov.value == "")
	{
		alert("Debe elegir un tipo de movimiento");
		return false;
	}
	if (document.FrmDatos.CmbTipoCita.value == "")
	{
		alert("Debe elegir un tipo de cita");
		return false;
	}
	if (document.FrmDatos.TxtCuil.value == "")
	{
		alert("Debe elegir una C.I.");
		return false;
	}
	if (document.FrmDatos.TxtNombre.value == "")
	{
		alert("Debe elegir un postulante");
		return false;
	}
}
function CancelarCita(TipoMov,TipoCita,NroCita,Cuil)
{
	if (confirm("Seguro desea Cancelar la cita seleccionada?"))
	{
		document.location.href = "CnfCancelarCita.asp?TipoMov="+TipoMov+"&TipoCita="+TipoCita+"&NroCita="+NroCita+"&Cuil="+Cuil
	}
}
</script>

<table border="0" width="530" cellspacing="0" cellpadding="0" height="81">
   <tr>
     <td width="530" background="<%=Session("Progetto")%>/images/titoli/strumenti2g.gif" height="81" valign="bottom" align="right">
       <table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
         <tr>
          <td width="100%" valign="top" align="right">
          		<b class="tbltext1a">Cancelación de una Cita</b>
          </td>
         </tr>
       </table>
     </td>
   </tr>
</table>
<br>
<table cellpadding="0" cellspacing="0" width="500" border="0">
	<tr height="18">
		<td class="sfondomenu" height="18" width="67%"> <span class="tbltext0"> 
		<b>Cancelar Cita</b>
		</td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
		<td valign="middle" align="right" class="tbltext1" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">
		
		</td>
    </tr>
    <tr>
	<td colspan=3>
		<br><p class="tbltext1">Ingresar la información correspondiente a la cita a cancelar.</p><br>
	</td>
	<td valign="middle" align="right" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1"></td>
	</tr>
	 
	<tr height="2">
		<td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif">
		</td>
	</tr>
</table>

<!--Inicio del Formulario-->

<br>
<br>
<%
Dim Inter

Set Inter = New InterfaceBD

if request("TipoMov") = "" then 
	sTipoMov = request("CmbTipoMov")
else
	sTipoMov = request("TipoMov")
end if

sAgrupacion = request("Agrupacion")

if sTipoMov <> "" then 
	sTipoCita = Inter.ObtenerCitaParaMovimiento(sTipoMov)
end if 

sOficina = request("TxtOficina")
sCuil = request("TxtCuil")

'if sTipoCita = 0 then
'	sTipoCita = 1
'end if 
%>


<script src="../FuncionesAgenda.js">
</script>

<br>
<div align="center">

<%if sTipoMov = "" or sTipoCita = "" or sOficina = "" or sCuil = "" then %>
	<table width="70%">
		<form name="FrmDatos" action="CancelarCita.asp" method="post" OnSubmit= 'return ControlarDatos();'>
			<input type="hidden" name="TxtOficina" value="<%=Session("creator")%>">
			<input type="hidden" name="TxtAgrupacion" value="<%=sAgrupacion%>">
			<tr>
				<td class="tbltext1"><b>Tipo de Movimiento</b></td>
				<td><%=Inter.GenerarComboTipoMovimientos("CmbTipoMov","tbltext1","onchange=RecargarCombos();",sTipoMov,sAgrupacion)%></td>
			</tr>
			<tr>
				<td class="tbltext1"><b>Tipo de Cita</b></td>
				<td><%=Inter.GenerarComboTipoCitas("CmbTipoCita","tbltext1","onchange=RecargarCombos();",sTipoCita)%></td>
			</tr>
			
			<!--<tr>
				<td class="tbltext1"><b>Tipo de Cita</b></td>
				<td><%=Inter.GenerarComboTipoCitas("CmbTipoCita","tbltext1","","")%>
				</td>
			</tr>-->
			<tr>
				<td class="tbltext1"><b>C.I.</b></td>
				<td><input name="TxtCuilNH" type="text" disabled class="textblack" style="TEXT-TRANSFORM:uppercase" maxlength="8" size="20">
				<input name="TxtCuil" type="hidden">
				<a href="Javascript:IrA('../BuscarPostulante.asp')" ID="imgPunto1" name="imgPunto1" onmouseover="javascript:window.status='';return true"><img border="0" src="<%=Session("Progetto")%>/images/bullet1.gif"></a></td>
			</tr>
			<tr>
				<td class="tbltext1"><b>Apellido y Nombre</b></td>
				<td class="tbltext1">
					<input name="TxtNombre" type="text" size="40" disabled class="textblack" style="TEXT-TRANSFORM:uppercase" disabled>
				</td>
			</tr>
			<tr>
				<td colspan=2 align = right><br>
					<input type = "submit" class="my" value="Buscar Cita a Cancelar">
				</td>
			</tr>
		</form>
	</table>
<%else
	Resultado = Inter.GenerarTablaMostrarCita(sTipoMov,sTipoCita,sCuil,"",1,"tbltext1","Javascript:CancelarCita",0)

	if Resultado <> "" then 
		%>
		<p class="tbltext1"><b>Seleccione a continuación el número de cita que desea cancelar.</b></p>
		<br>
		<%
		Response.Write Resultado 
	else%>
		<p class="tbltext1"><b>No se encontraron citas que coincidan con los datos ingresados.</b></p>
		<br>
	<%	
	end if 

end if%>
</div>
<%Set Inter = nothing%>
<!-- #include Virtual="/strutt_coda2.asp" -->
