<!-- #include Virtual="/Pgm/Agenda_Entrevistas/Movimientos_Citas/Soporte/ProcCalendario.asp" -->
<!-- #include Virtual="/strutt_testa2.asp" -->
<!-- #include Virtual="include/openconn.asp" -->
<!-- #include Virtual="/Pgm/Agenda_Entrevistas/ClsInterfaceBD.asp"-->

<%
Dim Inter
Dim Cl

Set Inter = New InterfaceBD
Set Cl = New Calendario

sFecha = request("TxtFecha")

NomDia = Cl.ObtenerNombreDia(cint(weekday(sFecha,2)))
NomMes = Cl.ObtenerNombreMes(cint(month(sFecha)))

sAgrupacion = request("TxtAgrupacion")

sOficina = Session("creator")

if sOficina = "" then
	sOficina = request("Oficina")
end if 

%>
<br>
<br>
<%

dim Rdo

Rdo = Inter.MostrarDisponibilidad("MostrarDisponibilidadDiaria.asp",SOficina,cstr(SFecha))



%>

<div align="center">
<table border="0" width="70%"><tr><td>
	<table border="0" width="70%">
<%   if Rdo <> "" then%>
		<tr>
			<td>
				<p align="center" class="tbltext1"><b>A continuación se muestra la disponibilidad para la fecha solicitada.</b></p>
			</td>
		</tr>
		<tr>
			<td>
				<br><br>
			</td>
		</tr>
		<tr>
			<td>
				<div align="center">
					<table 	bordercolor="MidnightBlue"	border="2" width="350">
						<tr>
							<th align="center" colspan="14" class="tbltext1" bgcolor="#D9D9AE"><b><%=NomDia%>&nbsp;&nbsp;<%=day(SFecha)%>&nbsp;&nbsp;de&nbsp;&nbsp;<%=NomMes%>&nbsp;&nbsp;de&nbsp;&nbsp;<%=year(SFecha)%></b></th>
							<%=Rdo%>
						</tr>
					</table>
				</div>
			</td>	
		</tr>
<%
else
%>
		<tr>
			<td>
				<p align="center" class="tbltext1"><b>No hay información disponible para la fecha solicitada.</b></p>
			</td>
		</tr>
<%
end if 
%>
	<tr>	
		<td align=right><br><br></td>
	</tr>
	<tr>	
		<td align=right><input type="button" value="Volver" onclick="javascript:document.location.href='DispCitasDia.asp?Agrupacion=2'" class="my"></td>
	</tr>
	</table>
</td></tr></table>
</div>		


<%
Set Inter = nothing
Set Cl = nothing
%>

<!-- #include Virtual="/strutt_coda2.asp" -->
