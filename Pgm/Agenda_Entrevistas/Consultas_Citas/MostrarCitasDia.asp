<!-- #include Virtual="/Pgm/Agenda_Entrevistas/Movimientos_Citas/Soporte/ProcCalendario.asp" -->
<!-- #include Virtual="/strutt_testa2.asp" -->
<!-- #include Virtual="include/openconn.asp" -->
<!-- #include Virtual="/Pgm/Agenda_Entrevistas/ClsInterfaceBD.asp"-->

<%
Dim Inter
Dim Cl

Set Inter = New InterfaceBD
Set Cl = New Calendario

sFecha = request("TxtFecha")

NomDia = Cl.ObtenerNombreDia(cint(weekday(sFecha,2)))
NomMes = Cl.ObtenerNombreMes(cint(month(sFecha)))

sAgrupacion = request("TxtAgrupacion")
sTipoCita = request("CmbTipoCita")
sDuracion = request("CmbDuracion")

sOficina = Session("creator")
if sOficina = "" then
	sOficina = request("Oficina")
end if
%>
<br>
<br>
<%

dim Rdo

Opcion = 1

if Opcion = "" then 
	Opcion = request("Opciones")
end if 

Rdo = Inter.GenerarFilasRangosPosibles("MostrarCitasDia.asp",SOficina,cstr(SFecha),Opcion,0,SDuracion)
if Opcion = 1 then 
	ColumSpan = 4
elseif Opcion = 0 then
	ColumSpan = 10
end if
%>

<div align="center">
<table  border="0" width="70%"><tr><td>
	<table border="0" width="70%">
<%   if Rdo <> "" then%>
		<tr>
			<td>
				<p align="center" class="tbltext1"><b>A continuaci�n se muestran las franjas horarias y los puestos para la fecha y duraci�n de cita especificada.</b></p>
			</td>
		</tr>
		<tr>
			<td>
				<br><br>
			</td>
		</tr>
		<tr>
			<td>
				<div align="center">
					<table 	bordercolor="MidnightBlue"	border="2" width="350">
						<tr>
							<th align="center" colspan="<%=ColumSpan%>" class="tbltext1" bgcolor="#D9D9AE"><b><%=NomDia%>&nbsp;&nbsp;<%=day(SFecha)%>&nbsp;&nbsp;de&nbsp;&nbsp;<%=NomMes%>&nbsp;&nbsp;de&nbsp;&nbsp;<%=year(SFecha)%></b></th>
							<%=Rdo%>
						</tr>
					</table>
				</div>
			</td>	
		</tr>
<%
else
%>
		<tr>
			<td>
				<p align="center" class="tbltext1"><b>No hay citas agendadas para esa fecha y esa duraci�n.</b></p>
			</td>
		</tr>
<%
end if 
%>
	<tr>	
		<td align=right><br><br></td>
	</tr>
	<tr>	
		<td align=right><input type="button" value="Volver" onclick="javascript:document.location.href='AgendaCitasDia.asp?Agrupacion=2'" class="my"></td>
	</tr>
	</table>
</td></tr></table>
</div>		


<%
Set Inter = nothing
Set Cl = nothing
%>

<!-- #include Virtual="/strutt_coda2.asp" -->
<!-- #include Virtual="include/closeconn.asp" -->