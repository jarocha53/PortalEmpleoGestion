<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->


<%
'=================================================================
'	Definiciones de los ABMS
'=================================================================
	redim DbCampos(20,9)

	' ..(n,0) = Nombre del Parametro o fields
	' ..(n,1) = Descripcion del campo en pantalla (va a la izquierda del campo)
	' ..(n,2) = Tipo de Dato 
	'			T :Texto F :Fecha
	'			N		numerico real
	'			NE		numerico entero
	'			NEP		numerico entero positivo
	'			NEP0	numerico entero positivo sin cero
	'			NRP		numerico real positivo
	'			NRP0	numerico real positivo sin cero
	' ..(n,3) = Tipo de Control (1: Text 2:Combo)
	' ..(n,4) = Longitud para textos (>1) 
	' ..(n,5) = Refresh para combos (0:No 1: con refresco)
	' ..(n,6) = Obligatorio (R)
	' ..(n,7) = Base a la que se conecta 0:Oracle 1:SQL EmpleoLaboro
	' ..(n,8) = ComboSql StoreProcedure para la lista del combo o por parametro busca la descripcion de un dato(codigo

'-------------------------------
'Definiciones Particulares
'-------------------------------


Select Case Fields("TipoListado")
	' Alta (informe temporario)
		Case "ConsultaCita"
		'Fields("Titulo") = "Consulta de Cita"
		'Fields("Subtitulo") = "Resultado de citas de la Persona Consultada" 
		'Fields("Detalle") = "Ingresar la informaci�n correspondiente."
		'Fields("Nota") = "A continuaci�n se listan las citas que corresponden con los datos ingresados."
		'Fields("ErrorMsg") = "No se encontraron citas que coincidan con los datos ingresados."
		'Modificadas para hacer m�s interpretable la consulta 05.07 (RKL)
		 Fields("Titulo") = "Consulta de Citas Programadas"
		 Fields("Subtitulo") = "Resultado de la Consulta de Citas Programadas" 
		 Fields("Detalle") = "Esta consulta muestra si una persona tiene actualmente una cita programada. Ingresar los datos de la misma."
		 Fields("Nota") = "A continuaci�n se muestra la cita Programada que corresponde con los datos de la persona ingresada:"
		 Fields("ErrorMsg") = "La persona no tiene actualmente citas Programadas"
		 Fields("EsHistorial") = "0"
		 Fields("Estado") = "1"
		'************************************************************************************
		'Nombre del SP o String SQL
		'************************************************************************************
		SPname = "AgCitaListaCitas"
		'************************************************************************************
		'Fields para amar los saltos simples y multiples
		'************************************************************************************
		saltohref = ""

		'-----------------------------------------------
		' Definicion de parametros a pedir en las consultas
		'-----------------------------------------------
		DbCampos(0,0)="2"				' Cantidad de parametros de datos a pedir 
		'Nombre del Parametro			'Descripcion del campo				'Tipo de Dato			' Tipo de Control	'Longitud del campo	'Refresco			'Obligatorio		'Coneccion a Usar	'SQL para el combo					
		DbCampos(1,0)="Cuil"			:DbCampos(1,1)="C.I."				:DbCampos(1,2)="T":		:DbCampos(1,3)="2"	:DbCampos(1,4)="11"	:DbCampos(1,5)="0"	:DbCampos(1,6)="R"	:DbCampos(1,7)="1"	:DbCampos(1,8)=""					
		DbCampos(2,0)="TxtNombre"		:DbCampos(2,1)="Apellido y Nombre"	:DbCampos(2,2)="T":		:DbCampos(2,3)="1"	:DbCampos(2,4)="50"	:DbCampos(2,5)="0"	:DbCampos(2,6)=""	:DbCampos(2,7)="1"	:DbCampos(2,8)=""					
		
		
	Case "ConsultaCitaEstado"
		'************************************************************************************
		'Fields para amar los datos de cabecera
		'************************************************************************************
		Fields("Titulo") = "Consulta de Totales de Citas por Estado"
		Fields("Subtitulo") = "Totales de Citas por Estado"
	    Fields("Detalle") = "Nos posibilita consultar para un determinado d�a o para un rango de fechas la cantidad de"_
		&" citas  que  ten�amos            en un principio Programadas y  el estado   de  las mismas. "_
		&" Al transcurrir el d�a y a medida que se van realizando las entrevistas, las citas se convierten en concretadas, estas pueden ser "_
		&"(Concretadas en Fecha), como tambi�n pueden venir personas que tenian cita otro d�a, en este caso se reflejara en"_ 
		&"(Concretadas en Fecha Distinta),al final del d�a  las  citas que no tuvieron su entrevista, quedaran registradas como"_
		&" Programadas. Si se quiere visualizar la cantidad de citas que hubo en un d�a, se deben sumar las citas programadas "_ 
		&" mas las citas concretadas en Fecha."
		Fields("Nota") = "A continuaci�n se listan los Totales de citas que corresponden con los datos ingresados."
		Fields("ErrorMsg") = "No se encontraron registros que coincidan con los datos ingresados."
		'************************************************************************************
		'Nombre del SP o String SQL
		'************************************************************************************
		SPname = "AgCitaTotalesPorEstado_Fields"
		'************************************************************************************
		'Define el comportamiento del formulario
		'************************************************************************************
		'Fields("frmestado") = "nuevo"
		
		'************************************************************************************
		'Fields para amar los saltos simples y multiples
		'************************************************************************************
		saltohref = ""
		SaltosDefinidos = ucase("*x1*x2*x3*x4*")
		Fields("SaltoHrefx1") = "ConsultasResultado.asp?TipoListado=ConsultaPorCitaEstado&"
		Fields("SaltoHrefx2") = "ConsultasResultado.asp?TipoListado=ConsultaPorCitaEstado&"
		Fields("SaltoHrefx3") = "ConsultasResultado.asp?TipoListado=ConsultaPorCitaEstado&"
		Fields("SaltoHrefx4") = "ConsultasResultado.asp?TipoListado=ConsultaPorCitaEstado&"
		Fields("Ayudax1") = "Ver Detalle de Reservas"
		Fields("Ayudax2") = "Ver Detalle de Citas Programadas"
		Fields("Ayudax3") = "Ver Detalle de Citas Concretadas"
		Fields("Ayudax4") = "Ver Detalle de Citas Concretadas"
		
		
		'--------------------------------------------------
		' Definicion de parametros a pedir en las consultas
		'--------------------------------------------------
		DbCampos(0,0)="2"				' Cantidad de parametros de datos a pedir 
		'Nombre del Parametro			'Descripcion del campo				'Tipo de Dato			' Tipo de Control	'Longitud del campo	'Refresco			'Obligatorio		'Coneccion a Usar	'SQL para el combo					
		DbCampos(1,0)="FechaDesde"		:DbCampos(1,1)="Fecha Desde"		:DbCampos(1,2)="F":		:DbCampos(1,3)="1"	:DbCampos(1,4)="10"	:DbCampos(1,5)="0"	:DbCampos(1,6)="R"	:DbCampos(1,7)="1"	:DbCampos(1,8)=""					
		DbCampos(2,0)="FechaHasta"		:DbCampos(2,1)="Fecha Hasta"		:DbCampos(2,2)="F":		:DbCampos(2,3)="1"	:DbCampos(2,4)="10"	:DbCampos(2,5)="0"	:DbCampos(2,6)="R"	:DbCampos(2,7)="1"	:DbCampos(2,8)=""					
		
		
		
	Case "ConsultaPorCitaEstado"
	
		Fields("Titulo") = "Consulta de Cita por Estado"
		Fields("Subtitulo") = "Resultado de citas por Estados"
		Fields("Detalle") = "Ingresar la informaci�n correspondiente." 
		Fields("Nota") = "A continuaci�n se listan las citas que corresponden con los datos ingresados."
		Fields("ErrorMsg") = "No se encontraron citas que coincidan con los datos ingresados."
		SPname = "AgAgendaPorEstadoConsultaDia"
		saltohref = ""
		
		'-----------------------------------------------
		' Defiincion de parametros a pedir en las consultas
		'-----------------------------------------------
		DbCampos(0,0)="1"				' Cantidad de parametros de datos a pedir 
		'Nombre del Parametro			'Descripcion del campo				'Tipo de Dato			' Tipo de Control	'Longitud del campo	'Refresco			'Obligatorio		'Coneccion a Usar	'SQL para el combo					
		DbCampos(1,0)="FechaDia"		:DbCampos(1,1)="Fecha"				:DbCampos(1,2)="F":		:DbCampos(1,3)="1"	:DbCampos(1,4)="10"	:DbCampos(1,5)="0"	:DbCampos(1,6)="R"	:DbCampos(1,7)="1"	:DbCampos(1,8)=""					
		'DbCampos(2,0)="FechaHasta"		:DbCampos(2,1)="Fecha Hasta"		:DbCampos(2,2)="F":		:DbCampos(2,3)="1"	:DbCampos(2,4)="10"	:DbCampos(2,5)="0"	:DbCampos(2,6)="R"	:DbCampos(2,7)="1"	:DbCampos(2,8)=""					
		
	Case "ConsultaReservasTodas"
		'************************************************************************************
		'Fields para amar los datos de cabezera
		'************************************************************************************
		Fields("Titulo") = "Consulta de Reservas"
		Fields("Subtitulo") = "Resultado de Reservas" 
		Fields("Detalle") = "Ingresar la informaci�n correspondiente."
		Fields("Nota") = "A continuaci�n se listan las Reservas de citas que corresponden con los datos ingresados."
		Fields("ErrorMsg") = "No se encontraron registros que coincidan con los datos ingresados."
		Fields("OpcionListado") = 1
		'************************************************************************************
		'Nombre del SP o String SQL
		'************************************************************************************
		SPname = "AgReservasReportes"
		'************************************************************************************
		'Define el comportamiento del formulario
		'************************************************************************************
		'Fields("frmestado") = "nuevo"
		
		'************************************************************************************
		'Fields para amar los saltos simples y multiples
		'************************************************************************************
		saltohref = ""
				
		'--------------------------------------------------
		' Defiincion de parametros a pedir en las consultas
		'--------------------------------------------------
		DbCampos(0,0)="2"				' Cantidad de parametros de datos a pedir 
		'Nombre del Parametro			'Descripcion del campo				'Tipo de Dato			' Tipo de Control	'Longitud del campo	'Refresco			'Obligatorio		'Coneccion a Usar	'SQL para el combo					
		DbCampos(1,0)="FechaDesde"		:DbCampos(1,1)="Fecha Desde"		:DbCampos(1,2)="F":		:DbCampos(1,3)="1"	:DbCampos(1,4)="10"	:DbCampos(1,5)="0"	:DbCampos(1,6)="R"	:DbCampos(1,7)="1"	:DbCampos(1,8)=""					
		DbCampos(2,0)="FechaHasta"		:DbCampos(2,1)="Fecha Hasta"		:DbCampos(2,2)="F":		:DbCampos(2,3)="1"	:DbCampos(2,4)="10"	:DbCampos(2,5)="0"	:DbCampos(2,6)="R"	:DbCampos(2,7)="1"	:DbCampos(2,8)=""					
	
	Case "ConsultaConveniosTotalesEstado"
		'************************************************************************************
		'Fields para amar los datos de cabezera
		'************************************************************************************
		Fields("Titulo") = "Convenios"
		Fields("Subtitulo") = "Visualizar Totales de Convenios por Estado y Fecha" 
		Fields("Detalle") = "Ingresar la informaci�n solicitada.<BR> Se debe completar Fecha Desde y Fecha Hasta.La consulta" _ 
							& "mostrar� los totales por Estado de Convenios para el rango de fechas solicitadas."
		
		Fields("Nota") = ""
		Fields("ErrorMsg") = "No se encontraron registros que coincidan con los datos ingresados."
		Fields("OpcionListado") = 1
		'************************************************************************************
		'Nombre del SP o String SQL
		'************************************************************************************
		SPname = "SeguroConveniosTotalesPorEstado"
		'************************************************************************************
		'Define el comportamiento del formulario
		'************************************************************************************
		'Fields("frmestado") = "nuevo"
		
		'************************************************************************************
		'Fields para amar los saltos simples y multiples
		'************************************************************************************
		saltohref = ""
		SaltosDefinidos = ucase("*x0*x1*x2*x3*x4*x5*x6*x7*x8*")
		Fields("SaltoHrefx0") = "ConsultasResultado.asp?TipoListado=ConsultaConveniosPorDiaEstado&"
		Fields("SaltoHrefx1") = "ConsultasResultado.asp?TipoListado=ConsultaConveniosPorDiaEstado&"
		Fields("SaltoHrefx2") = "ConsultasResultado.asp?TipoListado=ConsultaConveniosPorDiaEstado&"
		Fields("SaltoHrefx3") = "ConsultasResultado.asp?TipoListado=ConsultaConveniosPorDiaEstado&"
		Fields("SaltoHrefx4") = "ConsultasResultado.asp?TipoListado=ConsultaConveniosPorDiaEstado&"
		Fields("SaltoHrefx5") = "ConsultasResultado.asp?TipoListado=ConsultaConveniosPorDiaEstado&"
		Fields("SaltoHrefx6") = "ConsultasResultado.asp?TipoListado=ConsultaConveniosPorDiaEstado&"
		Fields("SaltoHrefx7") = "ConsultasResultado.asp?TipoListado=ConsultaConveniosPorDiaEstado&"
		Fields("SaltoHrefx8") = "ConsultasResultado.asp?TipoListado=ConsultaConveniosPorDiaEstado&"
	
				
		'--------------------------------------------------
		' Defiincion de parametros a pedir en las consultas
		'--------------------------------------------------
		DbCampos(0,0)="2"				' Cantidad de parametros de datos a pedir 
		'Nombre del Parametro			'Descripcion del campo				'Tipo de Dato			' Tipo de Control	'Longitud del campo	'Refresco			'Obligatorio		'Coneccion a Usar	'SQL para el combo					
		DbCampos(1,0)="FechaDesde"		:DbCampos(1,1)="Fecha Desde"		:DbCampos(1,2)="F":		:DbCampos(1,3)="1"	:DbCampos(1,4)="10"	:DbCampos(1,5)="0"	:DbCampos(1,6)="R"	:DbCampos(1,7)="1"	:DbCampos(1,8)=""					
		DbCampos(2,0)="FechaHasta"		:DbCampos(2,1)="Fecha Hasta"		:DbCampos(2,2)="F":		:DbCampos(2,3)="1"	:DbCampos(2,4)="10"	:DbCampos(2,5)="0"	:DbCampos(2,6)="R"	:DbCampos(2,7)="1"	:DbCampos(2,8)=""					
		
	Case "ConsultaConveniosPorDiaEstado"
		'************************************************************************************
		'Fields para amar los datos de cabezera
		'************************************************************************************
		Fields("Titulo") = "Convenios"
		Fields("Subtitulo") = "Visualizar Totales de Convenios por Estado y Fecha" 
		Fields("Detalle") =  "Ingresar la informaci�n solicitada.Se debe completar Fecha Desde y Fecha Hasta.La consulta" _ 
							& "mostrar� los totales por Estado de Convenios para el rango de fechas solicitadas."
		
		Fields("Nota") = ""
		Fields("ErrorMsg") = "No se encontraron registros que coincidan con los datos ingresados."
		Fields("OpcionListado") = 1
		'************************************************************************************
		'Nombre del SP o String SQL
		'************************************************************************************
		SPname = "SeguroConveniosReportes"
		'************************************************************************************
		'Define el comportamiento del formulario
		'************************************************************************************
		'Fields("frmestado") = "nuevo"
		
		'************************************************************************************
		'Fields para amar los saltos simples y multiples
		'************************************************************************************
		saltohref = ""
						
		'--------------------------------------------------
		' Defiincion de parametros a pedir en las consultas
		'--------------------------------------------------
		DbCampos(0,0)="2"				' Cantidad de parametros de datos a pedir 
		'Nombre del Parametro			'Descripcion del campo				'Tipo de Dato			' Tipo de Control	'Longitud del campo	'Refresco			'Obligatorio		'Coneccion a Usar	'SQL para el combo					
		DbCampos(1,0)="FechaDesde"		:DbCampos(1,1)="Fecha Desde"		:DbCampos(1,2)="F":		:DbCampos(1,3)="1"	:DbCampos(1,4)="10"	:DbCampos(1,5)="0"	:DbCampos(1,6)="R"	:DbCampos(1,7)="1"	:DbCampos(1,8)=""					
		DbCampos(2,0)="FechaHasta"		:DbCampos(2,1)="Fecha Hasta"		:DbCampos(2,2)="F":		:DbCampos(2,3)="1"	:DbCampos(2,4)="10"	:DbCampos(2,5)="0"	:DbCampos(2,6)="R"	:DbCampos(2,7)="1"	:DbCampos(2,8)=""					
		
	case else
end select

'Tiempo= Server.ScriptTimeOut 
'Server.ScriptTimeOut = 300	' 5 minutos

'-----------------------------------------------
' Definicion de parametros a pedir en las consultas
'-----------------------------------------------
	'DbCampos(0,0)="4"				' Cantidad de parametros de datos

	'Nombre del Parametro			'Descripcion del campo				'Tipo de Dato			' Tipo de Control	'Longitud del campo	'Refresco			'Obligatorio		'SQL para combos
	'DbCampos(1,0)="FechaVisita"		:DbCampos(1,1)="Fecha de Visita"	:DbCampos(1,2)="F":		:DbCampos(1,3)="1"	:DbCampos(1,4)="10"	:DbCampos(1,5)="0"	:DbCampos(1,6)="R"	:DbCampos(1,7)="SELECT descripcion, pcia from provincias"
	'DbCampos(2,0)="Supervisor"		:DbCampos(2,1)="Supervisor"			:DbCampos(2,2)="NEP":	:DbCampos(2,3)="1"	:DbCampos(2,4)="10"	:DbCampos(2,5)="0"	:DbCampos(2,6)="R"	:DbCampos(2,7)="SELECT descripcion, pcia from provincias"
	'DbCampos(3,0)="Entrevistado"	:DbCampos(3,1)="Entrevistado"		:DbCampos(3,2)="T":		:DbCampos(3,3)="1"	:DbCampos(3,4)="10"	:DbCampos(3,5)="0"	:DbCampos(3,6)="R"	:DbCampos(3,7)="SELECT descripcion, pcia from provincias"
	'DbCampos(4,0)="Resultado"		:DbCampos(4,1)="Resultado Visita"	:DbCampos(4,2)="NEP":	:DbCampos(4,3)="1"	:DbCampos(4,4)="10"	:DbCampos(4,5)="0"	:DbCampos(4,6)="R"	:DbCampos(4,7)="SELECT descripcion, pcia from provincias"

	
%>
