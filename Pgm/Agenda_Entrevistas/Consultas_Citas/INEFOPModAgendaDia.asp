<!-- #include Virtual="/strutt_testa2.asp" -->
<!-- #include Virtual="include/openconn.asp" -->
<!-- #include Virtual="/Pgm/Agenda_Entrevistas/ClsInterfaceBD.asp"-->

<%
sFecha = Request.form("Fecha")
sCI =  Request.form("CI")
sPuesto = Request.form("Puesto")
sOrden = Request.form("Orden")
'Response.Write Request.Form("TxtFecha")
%>

<table border="0" width="530" cellspacing="0" cellpadding="0" height="81">
   <tr>
     <td width="530" background="<%=Session("Progetto")%>/images/titoli/strumenti2g.gif" height="81" valign="bottom" align="right">
       <table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
         <tr>
          <td width="100%" valign="top" align="right">
          		<b class="tbltext1a">AGENDA INEFOP <br>Modificación/Confirmación Agenda</b>
          </td>
         </tr>
       </table>
     </td>
   </tr>
</table>
<br>
<table class="SFONDOCOMM" cellpadding="0" cellspacing="0" width="500" border="0">
	<tr height="18">
		<td class="sfondomenu" height="18" width="67%"> <span class="tbltext0"> 
		<b>Modificar/Confirmar Agenda Diaria</b>
		</td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
		<td valign="middle" align="right" class="tbltext1" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">
		
		</td>
    </tr>
    <tr>
	<td colspan=3>
		<p class="tbltext1">Ingresar la información solicitada.</p>
	</td>
	<td valign="middle" align="right" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1"></td>
	</tr>
	 
	<tr height="2">
		<td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif">
		</td>
	</tr>
</table>

<!--Inicio del Formulario-->
<script language="javascript">
<!--#include virtual = "/Include/ControlDate.inc"-->

function ControlarDatos()
{


<% 'Si ya paso la fecha, muestro estado     de lo contrario, permito eliminar cita
sSQL = "Select '1' as Primero,  convert(datetime, '" & sFecha & "',103) as Fecha union all select '2', getdate() order by Fecha"
set rs = cc.execute(sSQL)
dim bPrimero 
bPrimero = False
if rs("Primero") = "1" then 
	bPrimero = True	
else
%>	

	if(confirm('Está seguro que desea eliminar esta cita?\nUna vez procesada no podrá realizar cambios')){
	/* Envia los datos*/
	}else{
	return false;
	}	
	
<% end if %>

	if (document.FrmDatos.Estado.value == "")
	{
		alert("Debe ingresar un Estado");
		return false;
	}	
	
}

</script>

<br>
<br>

<%
Dim Inter

Set Inter = New InterfaceBD

sAgrupacion = request("Agrupacion")

%>
	<table width="70%">
		<form name="FrmDatos" action="INEFOPcnfEstadoCita.asp" method="post" OnSubmit= 'return ControlarDatos();'>
			<input type="hidden" name="TxtOficina" value="<%=Session("creator")%>">
			<input type="hidden" name="TxtAgrupacion" value="<%=sAgrupacion%>">
			<input type="hidden" name="MuestraCalendario" value="1">
			<input type="hidden" name="sFecha" value="<%=sFecha%>">
			<input type="hidden" name="sCI" value="<%=sCI%>">
			<input type="hidden" name="sPuesto" value="<%=sPuesto%>">
			<input type="hidden" name="sOrden" value="<%=sOrden%>">	
			
			<% 'Muestro los datos de la cita y doy la posibilidad de 
				'	CONFIRMAR ASISTENCIA
				'	DECIR QUE NO VINO
				
				'	ELIMINAR LA CITA (si la fecha no ha pasado)
			
			%>
			<tr>
				<td class="tbltext1"><b>Fecha:</b></td>
				<td class="tbltext1"><input type="text" class="tbltext1" align=center value="<%=sFecha%>" maxlength=10 size=14 name="TxtFecha" id="TxTFecha" disabled>  </td>
			</tr>
			<tr>
				<td class="tbltext1"><b>CI:</b></td>
				<td class="tbltext1"><input type="text" class="tbltext1" align=center value="<%=sCI%>" maxlength=10 size=14 name="CI" id="CI" disabled>  </td>
			</tr>

			<tr>
				<td class="tbltext1"><b>Puesto:</b></td>
				<td class="tbltext1"><input type="text" class="tbltext1" align=center value="<%=sPuesto%>" maxlength=10 size=14 name="Puesto" id="Puesto" disabled>  </td>
			</tr>
			<tr>
				<td class="tbltext1"><b>Orden:</b></td>
				<td class="tbltext1"><input type="text" class="tbltext1" align=center value="<%=sOrden%>" maxlength=10 size=14 name="Orden" id="Orden" disabled>  </td>
			</tr>
			<% 'Si ya paso la fecha, muestro estado     de lo contrario, permito eliminar cita
			'sSQL = "Select '1' as Primero,  convert(datetime, '" & sFecha & "',103) as Fecha union all select '2', getdate() order by Fecha"
			'set rs = cc.execute(sSQL)
			'if rs("Primero") = "1" then
			if bPrimero then
			    sSQL = "Select Estado FROM Inefop_agenda where oficina = '" & session("creator") & "' and puesto = '" & sPuesto & "' and orden = '" & sOrden & "' and fecha = convert(datetime,'" & sFecha & "',103) and uteconf is not null"
			    set rs = cc.execute(sSQL)
			    if not rs.eof then sEstado = cstr(rs("Estado"))
			%>
			<tr>
				<td class="tbltext1"><b>Estado:</b></td>
				<td class="tbltext1"><select name=Estado class="tbltext1"><option value=""></option><option value="1" <%if sEstado = "1" then response.write "selected" %>>Concretada</option><option value="0" <%if sEstado = "0" then response.write "selected" %>>No Asistió</option></select></td>
			</tr>			
			<tr>
				<td colspan=2 align = right><br>
					<input type="hidden" name="Accion" value="Modificar">	
					<input type = "submit" class="tbltext1" value="Guardar Cambios" id=submit1 name=submit1>  
				</td>
			</tr>
			<% else %>			
			<tr>
				<td colspan=2 align = right><br>
					<input type="hidden" name="Accion" value="Eliminar">	
					<input type = "submit" class="tbltext1" value="Eliminar cita" id=submit1 name=submit1>  					
				</td>
			</tr>
			<% end if %>				
		</form>
	</table>

<%
Set Inter = nothing

%>

<!-- #include Virtual="/strutt_coda2.asp" -->