<!-- #include Virtual="/strutt_testa2.asp" -->
<!-- #include Virtual="/Pgm/Agenda_Entrevistas/ClsInterfaceBD.asp"-->

<table border="0" width="530" cellspacing="0" cellpadding="0" height="81">
   <tr>
     <td width="530" background="<%=Session("Progetto")%>/images/titoli/strumenti2g.gif" height="81" valign="bottom" align="right">
       <table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
         <tr>
          <td width="100%" valign="top" align="right">
          		<b class="tbltext1a">Listado de Reserva</b>
          </td>
         </tr>
       </table>
     </td>
   </tr>
</table>

<br>

<table class="SFONDOCOMM" cellpadding="0" cellspacing="0" width="500" border="0">
	<tr height="18">
		<td class="sfondomenu" height="18" width="67%"> <span class="tbltext0"> 
		<b>Listar Reserva</b>
		</td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
		<td valign="middle" align="right" class="tbltext1" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">
		
		</td>
    </tr>
    <tr>
	<td colspan=3>
		<br><p class="tbltext1">Ingresar información sobre la reserva.</p><br>
	</td>
	<td valign="middle" align="right" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1"></td>
	</tr>
	 
	<tr height="2">
		<td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif">
		</td>
	</tr>
</table>

<!--Inicio del Formulario-->

<script language="javascript">
<!--#include virtual = "/Include/ControlDate.inc"-->
<!--#include virtual = "/Include/ControlString.inc"-->
<!--#include virtual = "/Include/ControlNum.inc"-->

function ControlarDatos()
{
	if ((document.FrmListarReserva.TxtNroLote.value == "")||
		(document.FrmListarReserva.TxtCantEntrev.value == ""))
	{
		alert("Los campos Nro de Lote y Cantidad de Personas que pactan citas son obligatorios");
		return false;
	}
	
	
	if (!IsNum(document.FrmListarReserva.TxtNroLote.value))
	{
		alert("El campo Nro de Lote debe ser numérico");
		return false;
	}

	if (!IsNum(document.FrmListarReserva.TxtCantEntrev.value))
	{
		alert("El campo Cantidad de Personas que pactan citas debe ser numérico");
		return false;
	}

	if (document.FrmListarReserva.TxtObservaciones.value != "")
	{
		if (IsNum(document.FrmListarReserva.TxtObservaciones.value))
		{
			alert("El campo Observaciones no debe ser numérico");
			return false;
		}
	}
	
	
	Ventana = window.open("CnfListarReserva.asp?NroLote="+document.FrmListarReserva.TxtNroLote.value+"&CantEntrev="+document.FrmListarReserva.TxtCantEntrev.value+"&Observaciones="+document.FrmListarReserva.TxtObservaciones.value,'Ventana','toolbar=no,menubar=no,scrollbars=yes,status=yes,height=800,width=900');
	Ventana.moveTo((self.screen.width /2)-(900/2),(self.screen.height /2)-(800/2))
}
</script>

<script src="../FuncionesAgenda.js">
</script>

<%
Dim Inter

Set Inter = New InterfaceBD
%>

<br>
<br>
<div align="center">
	<table width="70%">
		<form name="FrmListarReserva" action="CnfListarReserva.asp" method="post">
			<input type="hidden" name="TxtOficina" value=<%=Session("creator")%>>
			<!--<input type="hidden" name="TxtTipoMov" value=2>-->
			<tr>
				<td class="tbltext1"><b>Nro. de Lote</b></td>
				<td>
					<%

'------------------Pino agosto 2007 per gestire il numero lotto come combo e non doverlo ricordare a memoria (era un campo testo)					
Set RsLote = server.CreateObject("adodb.recordset")					

	SSQL = "select distinct (RefExternaNro ) FrOM AgCitaMovimientos with (nolock) where Oficina = " & session("creator") & " and RefExternaTipo = 1"
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
		Set rsLote = ConnLavoro.Execute(sSQL)		

	Retorna = ""

	If not RsLote.eof then
		Retorna = Retorna & "<select name='TxtNroLote'>"
		do while not RsLote.eof
				Retorna = Retorna & "<option value ='" & RsLote.fields(0) & "'>" & RsLote.fields(0) & "</option>"
			RsLote.movenext
		loop
	end if

	Retorna = Retorna & "</select>"
	Response.Write Retorna
	
	'------------------Pino agosto 2007 fino alla prossima istruzione html remmata	
%>
				
				<!--input type="text" name="TxtNroLote" class="textblack" style="TEXT-TRANSFORM:uppercase" maxlength=5 size=10-->
				</td>
			</tr>
			<tr>
				<td class="tbltext1"><b>Cantidad de Personas que pactan citas</b></td>
				<td><input type="text" name="TxtCantEntrev" class="textblack" style="TEXT-TRANSFORM:uppercase" maxlength=3 size=10></td>
			</tr>
			<tr>
				<td class="tbltext1"><b>Observaciones</b></td>
				<td><input type="text" name="TxtObservaciones" class="textblack" style="TEXT-TRANSFORM:uppercase" maxlength=250 size=40></td>
			</tr>
			<tr>
				<td colspan=2 align = right><br>
					<input type = "button" class="my" value="Listar Reserva" onclick='ControlarDatos()'>
				</td>
			</tr>
		</form>
	</table>
</div>

<%
set Inter = nothing
%>

<!-- #include Virtual="/strutt_coda2.asp" -->