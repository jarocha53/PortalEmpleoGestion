<!-- #include Virtual="/strutt_testa2.asp" -->
<!-- #include Virtual="/Pgm/Agenda_Entrevistas/ClsInterfaceBD.asp"-->

<%

Dim Inter

Set Inter = New InterfaceBD

Oficina			= Request("TxtOficina")
TipoMovimiento  = Request("TxtTipoMov")
NroLoteReservas = Request("TxtNroLote")
Motivo          = Request("CmbMotivo")
MotivoDesc      = Request("TxtMotivoDesc")
ReservaTipo     = 1 'indica el tipo de referencia externa, que es el lote de reservas..

CantAnul = Inter.AnularReservas(Oficina, TipoMovimiento, ReservaTipo, NroLoteReservas, Motivo, MotivoDesc, Session("IdUtente"))

If Isnumeric(CantAnul) then 
	%>
	
	<br>
	
	<p><B class=tbltext1>Las reservas se han anulado exitosamente</B></p>

	<br>

	<div align="center">
		<table  border="0" width="300">
			<form name="" action="" method="">
				<tr>
					<td class="tbltext1"><b>Tipo de Movimiento:</b></td>
					<td class="tbltext1"><%=Inter.ObtenerValor("AgCitaMovimientosTipos",TipoMovimiento)%></td>
				</tr>			
				<tr>	
					<td class="tbltext1"><b>Nro. de Lote:</b></td>
					<td class="tbltext1"><%=NroLoteReservas%></td>
				</tr>				
				<tr>	
					<td class="tbltext1"><b>Cantidad de Reservas Anuladas:</b></td>
					<td class="tbltext1"><%=CantAnul%></td>
				</tr>
				<tr>	
					<td colspan=2><br></td>
				</tr>
				<tr>	
					<td align=right colspan=2><input type="button" value="Volver" onclick="javascript:document.location.href='AnularReservaNoNominal.asp'" class="my" id=button1 name=button1></td>
				</tr>
			</form>
		</table>
	</div>
<%
else
%>
	<p><B class=tbltext1><%=CantAnul%></B></p>
<%
end if

set Inter = nothing

%>

<!-- #include Virtual="/strutt_coda2.asp" -->
