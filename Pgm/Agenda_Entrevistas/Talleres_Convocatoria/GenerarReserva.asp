<!-- #include Virtual="/strutt_testa2.asp" -->
<!-- #include Virtual="/Pgm/Agenda_Entrevistas/ClsInterfaceBD.asp"-->

<%
	if ValidateService(session("idutente"),"AG_GENERA_RESERVAS",cc) <> "true" then 
		response.redirect "/util/error_login.asp"
	end if
	
%>

<table border="0" width="530" cellspacing="0" cellpadding="0" height="81">
   <tr>
     <td width="530" background="<%=Session("Progetto")%>/images/titoli/strumenti2g.gif" height="81" valign="bottom" align="right">
       <table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
         <tr>
          <td width="100%" valign="top" align="right">
          		<b class="tbltext1a">Generación de Reserva</b>
          </td>
         </tr>
       </table>
     </td>
   </tr>
</table>
<br>
<table cellpadding="0" class="SFONDOCOMM" cellspacing="0" width="500" border="0">
	<tr height="18">
		<td class="sfondomenu" height="18" width="67%"> <span class="tbltext0"> 
		<b>Generar Reserva</b>
		</td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
		<td valign="middle" align="right" class="tbltext1" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">
		
		</td>
    </tr>
    <tr>
	<td colspan=3>
		<br><p class="tbltext1">Ingresar información sobre la reserva.</p><br>
	</td>
	<td valign="middle" align="right" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1"></td>
	</tr>
	 
	<tr height="2">
		<td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif">
		</td>
	</tr>
</table>

<!--Inicio del Formulario-->
<script language="javascript">
<!--#include virtual = "/Include/ControlDate.inc"-->
<!--#include virtual = "/Include/ControlString.inc"-->
<!--#include virtual = "/Include/ControlNum.inc"-->

function ControlarDatos()
{
	if ((document.FrmGenerarReserva.TxtCantPers.value == "")||
		(document.FrmGenerarReserva.TxtFechaInicio.value == ""))
	{
		alert("Los campos Cantidad de Personas y Fecha de Inicio de Entrevistas son obligatorios");
		return false;
	}
	
	
	if (!IsNum(document.FrmGenerarReserva.TxtCantPers.value))
	{
		alert("El campo Cantidad de Personas debe ser numérico");
		return false;
	}
	if (!ValidateInputDate(document.FrmGenerarReserva.TxtFechaInicio.value))
	{
		//alert("El campo Cantidad de Personas debe ser numérico");
		return false;
	}
	if (document.FrmGenerarReserva.TxtMotivoDesc.value != ""){
		if (IsNum(document.FrmGenerarReserva.TxtMotivoDesc.value))
		{
			alert("El campo Motivo no puede ser numérico");
			return false;
		}
	}
}
</script>
<%
Dim Inter

Set Inter = New InterfaceBD
%>

<br>
<br>
<div align="center">
	<table width="90%">
		<form name="FrmGenerarReserva" action="CnfGenerarReserva.asp" method="post" OnSubmit='return ControlarDatos();'>
			<input type="hidden" name="TxtOficina" value=<%=Session("creator")%>>
			<input type="hidden" name="TxtTipoMov" value=2>
			<tr>
				<td class="tbltext1"><b>Cantidad de Personas</b></td>
				<td><input type="text" name="TxtCantPers" class="textblack" style="TEXT-TRANSFORM:uppercase" maxlength=3 size=4></td>
			</tr>
			<tr>
				<td class="tbltext1"><b>Fecha Inicio Entrevistas</b></td>
				<td><input type="text" name="TxtFechaInicio" class="textblack" style="TEXT-TRANSFORM:uppercase" maxlength=10 size=12></td>
			</tr>
			<%
			ComboMotivo = Inter.GenerarComboMotivos("CmbMotivo","textblack","","",2)
			
			If ComboMotivo <> "" then 
			%>
			<tr>
				<td class="tbltext1"><b>Motivo</b></td>
				<td><%=ComboMotivo%></td>
			</tr>
			<%
			end if 
			%>
			<tr>
				<td class="tbltext1"><b>Motivo (Observaciones)</b></td>
				<td><input type="text" name="TxtMotivoDesc" class="textblack" style="TEXT-TRANSFORM:uppercase" size=60 maxlength=60></td>
			</tr>
			<tr>
				<td colspan=2 align = right><br>
					<input type = "Submit" class="my" value="Generar Reserva">
				</td>
			</tr>
		</form>
	</table>
</div>
<%
set Inter = nothing
%>

<!-- #include Virtual="/strutt_coda2.asp" -->