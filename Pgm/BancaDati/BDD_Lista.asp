<!--#include Virtual = "/strutt_testa2.asp"-->
<!--#include Virtual="/include/DecCod.asp"-->

<br>
<table border="0" width="510" cellspacing="0" cellpadding="0" height="30">
	<tr>
		<td width="530" background="<%=Session("Progetto")%>/images/titoli/barracerca.jpg" valign="bottom" align="right">
			<table border="0" width="460" height="30" cellspacing="0" cellpadding="0">
				<tr>
					<td width="100%" valign="middle" align="right"><b class="tbltext1a">Listado de documentos ingresados &nbsp;&nbsp;</span></b></td>
				</tr>
			</table>
		</td>
	</tr>
</table><br>
<center>
<br>
<%
Dim nActPagina
Dim nTotPagina
Dim nTamPagina		
Dim nTotRecord

nTamPagina = 5

If Request.Querystring("Page") = "" Then
	nActPagina = 1
	nRecVis = 0 
Else
	nActPagina = Clng(Request.Querystring("Page"))
	nRecVis = (nActPagina-1)*5
End If

	CompSearch = "a"
	W = ""
	W = " @size > 1 "

Set Q = Server.CreateObject("ixsso.Query")
Set ObjetUtil=Server.CreateObject("Ixsso.Util")
'si fissa come criterio di ordinamento la raggiungibilità, discendente

'si fdefiniscono alcune colonne ulteriori
Q.DefineColumn "description (DBTYPE_WSTR) = d1b5d3f0-c0b3-11cf-9a92-00a0c908dbf1 description"
Q.DefineColumn "comments (DBTYPE_WSTR) = d1b5d3f0-c0b3-11cf-9a92-00a0c908dbf1 comments"
Q.DefineColumn "abstract (DBTYPE_WSTR) = d1b5d3f0-c0b3-11cf-9a92-00a0c908dbf1 abstract"
Q.DefineColumn "progid (DBTYPE_WSTR) = d1b5d3f0-c0b3-11cf-9a92-00a0c908dbf1 progid"
Q.DefineColumn "originator (DBTYPE_WSTR) = d1b5d3f0-c0b3-11cf-9a92-00a0c908dbf1 originator"
' si specificano le proprietà dei documenti che vogliamo estrarre:
' il nome del file, il suo virtual path, etc.
' si definisce il catalogo su cui ricercare
' si definisce il catalogo dove fare la ricerca
Q.Columns = "DocTitle, DocAuthor, FileName, Path, vpath, Size, description, comments, abstract, progid, originator"

SortBy = "FileName[d]"
Q.SortBy = SortBy

if mid(Session("mask"),1,1) = "0" then
	Q.Catalog = "BancaDati"
end if

if mid(Session("mask"),1,1) = "1" then
	Q.Catalog = "BancaDatiPub"
end if
 
' si definisce la query

Q.Query = W

'Q.CiFlags = SHALLOW

' si definisce il numero massimo di documenti che vogliamo siano estratti
'Q.MaxRecords = 100
On error resume next
' viene effettuata la ricerca e viene creato il recordset dei risultati

if mid(Session("mask"),1,1) = "1" then
	ObjetUtil.AddScopeToQuery Q, Server.MapPath("\") & "\Pgm\BancaDati\Doc\Pub", "shallow"
end if

Set rstIndex = Q.CreateRecordSet("nonsequential")

If err <> 0 then
Response.Write err.description
	Response.Write "<TABLE width='370' border='0'>"
	Response.Write "<tr><td width='20' rowspan='2'></td>"
	Response.Write "<td><span class='tbltext1'>La ricerca non puo' essere eseguita</span><br></td></tr>"
	Response.Write "<tr><td><span class='tbltext1'>Chiave di ricerca non significativa.<br> Eseguirla nuovamente in una differente modalita'</span><br></td></tr>"
	Response.Write "</TABLE>"
Else
	If rstIndex.EOF Then
		Response.Write "<TABLE border=0><tr><td class=tbltext3><br><br>Non sono presenti documenti<br><br><br></td></tr>"
		Response.Write "<tr><td class=tbltext3 align=center ><a href=Javascript:history.back()><IMG SRC='" & Session("Progetto") & "/images/indietro.gif' border=0></a></td></tr></TABLE>"
	Else


	rstIndex.PageSize = nTamPagina
	rstIndex.CacheSize = nTamPagina
	

	nTotPagina = rstIndex.PageCount
	 
	If nActPagina < 1 Then
		nActPagina = 1
	End If
	If nActPagina > nTotPagina Then
		nActPagina = nTotPagina
	End If
	
	rstIndex.AbsolutePage = nActPagina
	nTotDocFind = rstIndex.Recordcount
	nTotRecord = 0 
	nInitDocVis = nRecVis + 1
	nEndDocVis = nRecVis + 5
	If nEndDocVis > nTotDocFind Then
		nEndDocVis = nTotDocFind
	End If
			
	Response.Write "<table width='95%' background='/Plavoro/images/sfondogrigio.gif'>"
	Response.Write "<tr height='20'><td align=right class=tbltext1><b>Visualizzati risultati da " & nInitDocVis & " a " & nEndDocVis & " su un totale di " & nTotDocFind & "</b><br></span></td></tr></table><br>"

' -------
	Response.Write "<br><TABLE border=0 width=250 cellspacing=2 cellpadding=2>"
	Response.Write "<td align=right class=textred nowrap >pagine di risultato :&nbsp;&nbsp;&nbsp;</td>"
	If nActPagina > 1 Then
		Response.Write "<td class=tbltext>precedente</td><td class=textreda nowrap align=right valign=middle width='25%'><A class=textreda HREF='BDD_Lista.asp?Page=" & nActPagina-1 & "&search=" & Replace(W,"'","$") & "'><img border=0 alt='Pagina precedente' src=" & Session("Progetto") & "/images/precedente.gif></A></td>"
	End If
'	Response.Write rstIndex.Recordcount
	
	npagin = rstIndex.Recordcount/nTamPagina
	
	if rstIndex.Recordcount mod nTamPagina <> 0 then
		npagin = npagin + 1
	end if	
	
	Response.Write "<td align=center nowrap >"
	
	for i = 1 to npagin
		puls = puls & "<a class=textreda href='BDD_Lista.asp?Page=" & i & "&search=" & Replace(W,"'","$") & "'><u>" 
		if i = 1 and Request.Querystring("page")  & "*" = "*" then
				puls = puls & "<b>"
		else
		 	if i = Clng(Request.Querystring("page")) then 
				puls = puls & "<b>"
		    end if
		end if

		puls = puls & i
		
		if i = 1 and Request.Querystring("page")  & "*" = "*" then
				puls = puls & "</b>"
		else
		 	if i = Clng(Request.Querystring("page")) then 
				puls = puls & "</b>"
		    end if
		end if
		
		puls = puls & "</u></a> - "
	next

	Response.Write left(puls,len(puls)-2)
	Response.Write "</td>"
	If nActPagina < nTotPagina Then
		Response.Write "<td class=textreda nowrap align=left><A class=textreda HREF='BDD_Lista.asp?Page=" & nActPagina+1 & "&search=" & Replace(W,"'","$") & "'><img border=0 alt='Pagina successiva' src=" & Session("Progetto") & "/images/successivo.gif></A></td><td class=tbltext>successivo</td>"
	End If
	Response.Write "</tr></TABLE><br>"

' -------

	'mostra i risultati
		Do While Not rstIndex.EOF And nTotRecord < nTamPagina

		path = rstIndex("path")
		strdoc = instr(1,path,"doc")
		strhref = mid(path,strdoc)
		p = mid(path,strdoc+4,2)
		
		if ucase(p) = "IL" then
			privat = "<br>documento privato riservato agli utenti di ItaliaLavoro "
			varil = "IL"			
		else
			privat = ""	
			varil = "PB"			
		end if		
		
		p1 = mid(path,strdoc+7,4)
		p2 = mid(path,strdoc+8,4)
		
		Response.Write "<br><br><br><br><br><br>" & p1 & "<br>"
		Response.Write "" & p2 & "<br><br><br><br><br><br><br>"
		
		
		if ucase(p1) = "PROV" or ucase(p2) = "PROV" then
			privat = privat & " <br>documento provvisorio"
			varpr = "PR"			
		else
			varpr = ""			
		end if

		
		filnam = rstIndex("Filename") & "&varil=" & varil & "&varpr=" & varpr
		
	
		cdataagg = mid(rstIndex("comments"),1,10)

		cnote = ""
		if mid(Session("mask"),1,1) = "0" and mid(rstIndex("comments"),11) > "" and mid(rstIndex("comments"),12) > "" then
			cnote = "<span class='tbltext' align='left'>note : " & mid(rstIndex("comments"),12) & "<br>"
		end if

		pubbl = ""	
		if mid(Session("mask"),1,1) = "0" then
			modif = "</td><td align=center width=100 class=sfondocomm><a class=textred href='BDD_modifica.asp?nf=" & filnam & "'><b>Modifica</b></a><br><br>" 
			cance = "<a class=textred href='BDD_cancella.asp?nf=" & filnam & "'><b>Cancella</b></a><br><br>"
			if varpr > "" then
				pubbl = "<a class=textred href='BDD_pubblica.asp?nf=" & filnam & "'><b>Pubblica</b></a><br><br>"
			else
				pubbl = ""
			end if
		end if
		
		Response.Write "<TABLE border=0 width=500>" & _
							"<tr>" & _
							"<td align=left nowrap width=450>" & _
							"<a class=tbltext href=""" & strhref & """ target=blank><u><b>" & rstIndex("DocTitle") & "</a></b></u><span class=textred>" & privat & "<br>" & _
							"<span class='tbltext'>" & rstIndex("abstract") & "<br>" & _
							"<span class='tbltext'>" & mid(rstIndex("description"),4) & " - " & rstIndex("progid") & "<br>" & _
							"<span class='tbltext'>Autore : " & rstIndex("DocAuthor") & "&nbsp;&nbsp; - &nbsp;&nbsp;" & _
							"<span class='tbltext'>Data Aggiornamento : " & cdataagg & "<br>" & _
							cnote & _
							modif & _
							cance & _
							pubbl & _
							"</tr><tr height='1'><td colspan=2 background='/Plavoro/images/separagrigio.gif'></td></tr><tr><td>" & _
							"</td></tr>"
		Response.Write "</TABLE>"
		nRecVis = nRecVis + 1
		nTotRecord = nTotRecord + 1	
		rstIndex.movenext
		Loop

	'Response.Write nActPagina	
	Response.Write "<br><TABLE border=0 width=250 cellspacing=2 cellpadding=2>"
	Response.Write "<td align=right class=textred nowrap >pagine di risultato :&nbsp;&nbsp;&nbsp;</td>"
	If nActPagina > 1 Then
		Response.Write "<td class=tbltext>precedente</td><td class=textreda nowrap align=right valign=middle width='25%'><A class=textreda HREF='BDD_Lista.asp?Page=" & nActPagina-1 & "&search=" & Replace(W,"'","$") & "'><img border=0 alt='Pagina precedente' src=" & Session("Progetto") & "/images/precedente.gif></A></td>"
	End If
'	Response.Write rstIndex.Recordcount

	npagin = rstIndex.Recordcount/nTamPagina
	
	if rstIndex.Recordcount mod nTamPagina <> 0 then
		npagin = npagin + 1
	end if	
	
	Response.Write "<td align=center nowrap >"
	
	for i = 1 to npagin
		pulscoda = pulscoda & "<a class=textreda href='BDD_Lista.asp?Page=" & i & "&search=" & Replace(W,"'","$") & "'><u>" 
		if i = 1 and Request.Querystring("page")  & "*" = "*" then
				pulscoda = pulscoda & "<b>"
		else
		 	if i = Clng(Request.Querystring("page")) then 
				pulscoda = pulscoda & "<b>"
		    end if
		end if

		pulscoda = pulscoda & i
		
		if i = 1 and Request.Querystring("page")  & "*" = "*" then
				pulscoda = pulscoda & "</b>"
		else
		 	if i = Clng(Request.Querystring("page")) then 
				pulscoda = pulscoda & "</b>"
		    end if
		end if
		
		pulscoda = pulscoda & "</u></a> - "
	next

	Response.Write left(pulscoda,len(puls)-2)
	Response.Write "</td>"
	If nActPagina < nTotPagina Then
		Response.Write "<td class=textreda nowrap align=left><A class=textreda HREF='BDD_Lista.asp?Page=" & nActPagina+1 & "&search=" & Replace(W,"'","$") & "'><img border=0 alt='Pagina successiva' src=" & Session("Progetto") & "/images/successivo.gif></A></td><td class=tbltext>successivo</td>"
	End If
	Response.Write "</tr></TABLE>"

	rstIndex.Close
	Set rstIndex = nothing
	
	End If
End If



%>
<br><table cellSpacing="4" cellPadding="4" width="500" align="center" border="0">
	<tr>
		<td align="middle"><a href="BDD_Ricerca.asp" class=textred><b>Buscar documento</b></a></td>
		<% if mid(Session("mask"),1,1) = "0" then%>
		<td align="middle"><a href="BDD_InsDoc.asp" class=textred><b>Ingresar nuevo documento</b></a></td>
		<%end if %>	
	</tr>  
</table>
<!--#include Virtual = "/strutt_coda2.asp"-->
