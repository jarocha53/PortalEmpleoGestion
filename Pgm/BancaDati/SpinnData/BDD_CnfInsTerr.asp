<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include virtual = "/include/SysFunction.asp"-->
<!--#include virtual = "/include/OpenConn.asp"-->
<!--#include Virtual="/include/DecCod.asp"-->
<!--#include virtual="/util/dbUtil.asp"-->
<!--#include virtual = "/include/ControlDateVB.asp"-->

<%
Sub Inizio()
%>
<head>
	<link REL="STYLESHEET" TYPE="text/css" HREF="<%=session("progetto")%>/fogliostile.css">
	<title>DOCUMENTA</title>
</head>
<body>
<center>
<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
	<tr height="18">
		<td class="sfondomenu" width="67%" height="18"><span class="tbltext0"><b>&nbsp;CONFERMA INSERIMENTO DELLE SOTTOCATEGORIE </b></span></td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
		<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
	</tr>
</table>
<!-- Commento -->
<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
	<tr>
		<td align="left" class="sfondocomm">Finestra di conferma inserimento delle sottocategorie.  
		</td>
	</tr>
	<tr height="2">
		<td class="sfondocomm" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif">
		</td>
	</tr>
</table>

<%
End Sub
'--------------------'

dim sDescEle
dim vData
dim sSQL
dim rs
dim chiave
dim Errore
dim Max
dim s, tbl, val

	s  = UCase(Request.form("id")) 
	tbl = UCase(Request.form("tab"))
	val = UCase(Request.form("val"))

sDescEle = server.HTMLEncode(UCASE(TRIM(Request.Form("txtElemento"))))
sDescEle = Replace(sDescEle, "'", "''")
vData = ""


'Controllo che non abbia gia inserito quella descrizione
sSQL=""
sSQL= "SELECT DESCRIZIONE FROM TADES WHERE NOME_TABELLA='" & tbl & "'" & _
	  " AND  DESCRIZIONE = '" & sDescEle & "'"
	
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
Set rs = CC.Execute(sSQL)

IF NOT rs.EOF THEN
	Inizio()
%>
	<br><br>
	<table border="0" cellspacing="1" cellpadding="1" width="500">
		<tr align="middle">
			<td class="tbltext3">
				Inserimento non effettuabile.<br><br>
				Descrizione elemento categoria gi� presente.
			</td>
		</tr>
	</table>
	<br>				
	<table border="0" cellpadding="0" cellspacing="1" width="500">
		<tr>
			<td align="middle" colspan="2" width="60%"><b>
				<a HREF="javascript:history.back()"><img SRC="<%=Session("Progetto")%>/images/indietro.gif" border="0">
			</td>
		</tr>
	</table>
<%
ELSE
	'Inserimento CATEGORIA DALLA TABELLA DI TADES 'BDARE'
	
	
	sSqlMax= "Select max(codice) as Massi from tades where nome_tabella='" & tbl & "'"
	
'PL-SQL * T-SQL  
SSQLMAX = TransformPLSQLToTSQL (SSQLMAX) 
	Set rsMax = CC.Execute(sSqlMax)

		Max= rsMax("Massi") + 1
		
	rsMax.Close
	Set rsMax = nothing
	
	DIM dOggi
	dOggi = now()
	
	Errore="0" 
	sSQL=""
	sSQL = "INSERT INTO TADES " &_
		   "(ISA, CODICE,NOME_TABELLA, DESCRIZIONE, VALORE, DECORRENZA, SCADENZA) " &_
		   "VALUES " &_
		   "('0','" & max  & "','" & tbl & "','" & sDescEle & "','1'," &   ConvDateToDbs(dOggi) & ", TO_DATE('31/12/9999', 'DD/MM/YYYY'))"
		
	chiave=sDescEle
	Errore=Esegui(chiave,"TADES",session("idutente"),"INS", sSQL, 0, vData)

	If Errore="0" then
%>
		<form name="frmIndietro" method="post" action="BDD_Territori.asp">
			<input type="hidden" name="sDescEle" value="<%=sDescEle%>">
			<input type="hidden" name="id" value="<%=s%>">
			<input type="hidden" name="tab" value="<%=tbl%>">
			<input type="hidden" name="val" value="<%=val%>">
		</form>
		
			<script>
				alert("Inserimento correttamente effettuato");
				self.close()
			</script>
<%
	Else
		Inizio()
%>
		<br><br>
		<table border="0" cellspacing="1" cellpadding="1" width="500">
			<tr align="middle">
				<td class="tbltext3">
					Inserimento non effettuabile.
				</td>
			</tr>
			<tr align="middle">
				<td class="tbltext3">
					<%Response.Write(Errore)%> 
				</td>
			</tr>
		</table>
		<br>				
		<table border="0" cellpadding="0" cellspacing="1" width="500">
			<tr>
				<td align="middle" colspan="2" width="60%"><b>
				 <a HREF="javascript:history.back()"><img SRC="<%=Session("Progetto")%>/images/indietro.gif" border="0">
				</td>
			</tr>
		</table>
<%
	End if
END IF
rs.Close
Set rs = nothing
%>
<!--#include Virtual = "/include/CloseConn.asp"-->	

