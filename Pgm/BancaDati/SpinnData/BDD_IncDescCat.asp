<!-- TABELLA CON LA DESCRIZIONE DI TUTTE GLI ARGOMENTI  -->

<table border="0" CELLPADDING="0" CELLSPACING="1" width="500">
	<tr>
     <td height="2" align="left" background="/images/separazione.gif"></td>
	</tr>
	<tr>
     <td height="2" align="left">&nbsp;</td>
	</tr>
	<tr>
		<td class="tbltexta"><b>Informazioni relative alle aree di ricerca</b><br><br></td>
	</tr>
	<tr>
		<td class="tbltext1"><b>Normativa</b></td>
	</tr>
	<tr>
		<td class="tbltext"><i>Sono proposte le leggi, i decreti legislativi, i decreti legge, i decreti ministeriali e direttoriali, le circolari, ecc., che riguardano in primo luogo le politiche occupazionali e del lavoro europee e nazionali, ma anche la legislazione di quei settori, ad  esempio Terzo settore, ambiente, immigrazione, ecc. dei quali sono palesi le ricadute occupazionali.</i><br><br></td>
	</tr>
	<tr>
		<td class="tbltext1"><b>Documentazione</b></td>
	</tr>
	<tr>
		<td class="tbltext"><i>sono proposti i principali documenti di programmazione nazionale, regionale ed europea che definiscono il quadro complessivo delle politiche per l&#146;occupazione e l&#146;inclusione sociale.</i><br><br></td>
	</tr>
	<tr>
		<td class="tbltext1"><b>Bandi e Avvisi</b></td>
	</tr>
	<tr>
		<td class="tbltext"><i>fornisce un quadro delle opportunit� europee, nazionali e regionali per la presentazione e il finanziamento di azioni in favore dell&#146;occupazione</i><br><br></td>
	</tr>
	<tr>
		<td class="tbltext1"><b>Rapporti di monitoraggio e statistiche</b></td>
	</tr>
	<tr>
		<td class="tbltext"><i>i Rapporti, di diverse Autorit�, tra le quali: MLPS; ISFOL; Banca d&#146;Italia; ecc. forniscono una rendicontazione dello stato dell&#146;arte delle politiche e delle realt� monitorate. Il quadro informativo � spesso corredato da approfondimenti tematici e da allegati statistici. I dati statistici, in primo luogo dell&#146;ISTAT, costituiscono inoltre oggetto di informazioni a se stante.</i><br><br></td>
	</tr>
	<tr>
		<td class="tbltext1"><b>Best practices</b></td>
	</tr>
	<tr>
		<td class="tbltext"><i>viene dato conto delle buone pratiche come esempi di eccellenza da condividere. L&#146;utilit� di riferimenti alle buone pratiche sono presenti sia in numerosi documenti europei, sia in  documenti italiani che, come ad esempio nel Piano Nazionale per l&#146;Occupazione 2002, ne danno conto.</i><br><br></td>
	</tr>
	<tr>
		<td class="tbltext1"><b>Benchmark</b></td>
	</tr>
	<tr>
		<td class="tbltext"><i>consiste nella raccolta e comparazione di indicatori e delle migliori pratiche relative alle politiche del lavoro in Europa.</i><br><br></td>
	</tr>
	<tr>
		<td class="tbltext1"><b>Progetti di ItaliaLavoro S.p.A.</b></td>
	</tr>
	<tr>
		<td class="tbltext"><i>vengono presentati i pi� significativi progetti promossi e gestiti da Italia Lavoro S.p.A.</i><br><br></td>
	</tr>
	<tr>
     <td height="2" align="left" background="/images/separazione.gif"></td>
	</tr>
</table>
