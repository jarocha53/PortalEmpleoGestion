<script language="javascript" src="/include/help.inc"></script>
<script language="Javascript">
<!--
	function addItem()
	{
		sel = ""
		nsel = 0
		numcheck = parseInt(formcheck.CampiCheck.value,10)
	
		for (i=0; i<numcheck; i++) {
				
			if (document.formcheck.elements[i].checked) 	
			{
				sel = sel + document.formcheck.elements[i].value + ", "
				nsel++;
			}
					
		}
			
		if (nsel == numcheck)
		{
			sel = "tutte le documentazioni"
			window.opener.document.forma.chkdocum.checked = true
			window.opener.document.forma.docum1.value = "documentazione"
		}
		else
		{
			sel = sel.substring (0,(sel.length-2))
			window.opener.document.forma.chkdocum.checked = false	
			window.opener.document.forma.docum1.value = "documentazione, " + sel
		}
		
		window.opener.docum.innerHTML = sel
		window.opener.document.forma.chkcateg.checked = false
		window.close();
	}		
//-->
</script>

<html>
<head>
	<link REL="STYLESHEET" TYPE="text/css" HREF="<%=session("progetto")%>/fogliostile.css">
	<title>DATA SPINN - Documentazione</title>
</head>
<body>
<center>
<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
	<tr height="18">
		<td class="sfondomenu" width="67%" height="18"><span class="tbltext0"><b>&nbsp;SELEZIONE DELLE SOTTOCATEGORIE </b></span></td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
		<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
	</tr>
</table>
<!-- Commento -->
<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
	<tr>
		<td align="left" class="sfondocomm">Utilizza questa form per selezionare delle sottocategorie da inserire nei parametri di ricerca del documento.  
			<a href="Javascript:Show_Help('/Pgm/Help/BancaDati/BDD_Documentazione')"><img align="right" src="/images/help.gif" border="0"></a>
		</td>
	</tr>
	<tr height="2">
		<td class="sfondocomm" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif">
		</td>
	</tr>
</table>

<form method="post" name="formcheck">
<table WIDTH="400" ALIGN="center" BORDER="0" CELLSPACING="0" CELLPADDING="0">
	<tr>
		<td colspan="2"><p align="center" class="textreda">Documentazione</p></td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
<!--#include Virtual="/include/OpenConn.asp"-->
<!--#include Virtual="/include/DecCod.asp"-->
<%	Dim aDescrDocumentazione
	sDocumentazione = UCase(Request.QueryString("id"))

	If sDocumentazione = "TUTTE LE DOCUMENTAZIONI" then
		bAllDocum = "checked"
	Else
		bAllDocum = ""
	End If
	aDescrDocumentazione = decodTadesToArray("BDDOC",DATE,"",0,"0")
	nInd = 0
	If Not IsNull(aDescrDocumentazione(0,0,0)) Then 
		For yy=0 To Ubound(aDescrDocumentazione)-1
%> 
			<tr>
				<td>
					<input type="checkbox" id="checkbox<%=nInd%>" <%=bAllDocum%> name="checkbox<%=nInd%>" <% If instr(sDocumentazione,aDescrDocumentazione(yy,yy+1,yy)) Then Response.Write "checked" %> value="<%=LCase(aDescrDocumentazione(yy,yy+1,yy))%>">
				</td>
				<td class="tbltext"><b><%=aDescrDocumentazione(yy,yy+1,yy)%></b></td>
			</tr>
<%		
		nInd=nInd+1
		Next
	End if
	Erase aDescrDocumentazione
%>
<!--#include Virtual="/include/CloseConn.asp"-->
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td width="400" colspan="2">
			<table width="400" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td align="right" valign="top" width="200"><a href="javascript:addItem()"><img src="/images/aggiungi.gif" border="0"></a></td>
					<td width="200" valign="top"><a href="javascript:window.close()"><img src="/images/chiudi.gif" border="0" WIDTH="55" HEIGHT="40"></a></td>
				</tr>
			</table>
		</td>
	</tr>
	<input type="hidden" name="CampiCheck" value="<%=nInd%>"> 
</table>
</form>

</body>
</html>
