<script language="javascript" src="/include/help.inc"></script>
<script language="Javascript">
<!--
	function addItem(txt)
	{
		window.opener.note.innerHTML = "note: " + txt;
		window.opener.document.forma.note1.value = txt;
		window.close()
	}
//-->
</script>

<html>
	<link REL="STYLESHEET" TYPE="text/css" HREF="<%=session("progetto")%>/fogliostile.css">
	<title>DATA SPINN - Note</title>
<body bgcolor="#ffffff" onload="javascript:document.formlink.txt.focus()">
<center>
<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
	<tr height="18">
		<td class="sfondomenu" width="77%" height="18"><span class="tbltext0"><b>&nbsp;INSERIMENTO DI NOTE RELATIVE AL DOCUMENTO</b></span></td>
		<td width="3%" background="/images/tondo_linguetta.gif"></td>
		<td valign="middle" align="right" class="tbltext1" width="20%" background="/images/sfondo_linguetta.gif"></td>
	</tr>
</table>
<!-- Commento -->
<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
	<tr>
		<td align="left" class="sfondocomm">Utilizza questa form per inserire delle note che saranno visibili solo agli utenti di ItaliaLavoro.  
			<a href="Javascript:Show_Help('/Pgm/Help/BancaDati/BDD_Note')"><img align="right" src="/images/help.gif" border="0"></a>
		</td>
	</tr>
	<tr height="2">
		<td class="sfondocomm" width="100%" colspan="3" background="/images/separazione.gif"></td>
	</tr>
</table>

<form method="post" name="formlink">
<table WIDTH="500" ALIGN="center" BORDER="0" CELLSPACING="3" CELLPADDING="3">
	<tr>
		<td colspan="2" align="middle" class="tbltext1a"><b>Inserimento delle note riguardanti il documento</b></td>
	</tr>
	<tr>
		<td><textarea rows="3" cols="60" id="txt" name="txt"></textarea></td>
	</tr>
	<tr>
		<td width="500">
			<table width="500" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td align="right" valign="top" width="250"><input type="image" src="/images/conferma.gif" OnClick="addItem(document.formlink.txt.value)" name="image1"></td>
					<td width="250" valign="top"><a href="javascript:window.close()"><img src="/images/chiudi.gif" border="0"></a></td>
				</tr>
			</table>
		</td>
	</tr>  
</table>
</form>

</body>
</html>
