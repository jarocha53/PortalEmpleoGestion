<!--#include Virtual = "/strutt_testa2.asp"-->
<!--#include Virtual="/include/DecCod.asp"-->
<script LANGUAGE="JavaScript">
function LinkNews(ID)
{
	f= ID ;
	fin=window.open(f,"","minimize=0,maximize=0,toolbar=0,location=0,directories=0,status=0,menubar=0,scrollbars=yes,resizable=0,copyhistory=0,width=550,height=470,screenX=100,screenY100");	
}

function Canc(fn,vil,vpr)
{
	if (confirm("Sei sicuro di voler eliminare il documento?"))
		{
			document.location.href="BDD_cancella.asp?nf=" + fn + "&varil=" + vil + "&varpr=" + vpr ;
		}
}

</script>
<center>
<br>
<TABLE border="0" width="510" cellspacing="0" cellpadding="0" height="30">
	<tr>
		<td width="530"  background="<%=Session("Progetto")%>/images/titoli/barracerca.jpg" valign="bottom" align="right">
			<table border="0" width="90%" height="30" cellspacing="0" cellpadding="0">
				<tr>
					<td width="100%" valign="middle" align="right"><b class="tbltext1a">DATA SPINN - I tuoi documenti da validare&nbsp;&nbsp;</span></b></td>
				</tr>
			</table>
		</td>
	</tr>
</TABLE><br>
 
<br>
<%
Dim nActPagina
Dim nTotPagina
Dim nTamPagina		
Dim nTotRecord

nTamPagina = 10

If Request.Querystring("Page") = "" Then
	nActPagina = 1
	nRecVis = 0 
Else
	nActPagina = Clng(Request.Querystring("Page"))
	nRecVis = (nActPagina-1)*10
End If

	CompSearch = "a"
	W = ""
	W = " @size > 1 "

Set Q = Server.CreateObject("ixsso.Query")
'si fissa come criterio di ordinamento la raggiungibilità, discendente
Set ObjetUtil=Server.CreateObject("Ixsso.Util")

'si fdefiniscono alcune colonne ulteriori
Q.DefineColumn "description (DBTYPE_WSTR) = d1b5d3f0-c0b3-11cf-9a92-00a0c908dbf1 description"
Q.DefineColumn "comments (DBTYPE_WSTR) = d1b5d3f0-c0b3-11cf-9a92-00a0c908dbf1 comments"
Q.DefineColumn "abstract (DBTYPE_WSTR) = d1b5d3f0-c0b3-11cf-9a92-00a0c908dbf1 abstract"
Q.DefineColumn "progid (DBTYPE_WSTR) = d1b5d3f0-c0b3-11cf-9a92-00a0c908dbf1 progid"
Q.DefineColumn "originator (DBTYPE_WSTR) = d1b5d3f0-c0b3-11cf-9a92-00a0c908dbf1 originator"
' si specificano le proprietà dei documenti che vogliamo estrarre:
' il nome del file, il suo virtual path, etc.
' si definisce il catalogo su cui ricercare
' si definisce il catalogo dove fare la ricerca
Q.Columns = "DocTitle, DocAuthor, FileName, Write, Path, vpath, Size, description, comments, abstract, progid, originator"

'si fissa come criterio di ordinamento la raggiungibilità, discendente
SortBy = "Write[d]" 
Q.SortBy = SortBy 

if mid(Session("mask"),1,1) = "0" then
	Q.Catalog = "BancaDati"
end if


if mid(Session("mask"),1,1) = "1" then
	Q.Catalog = "BancaDatiPub" 
end if

' si definisce la query

Q.Query = W

'Q.CiFlags = SHALLOW

' si definisce il numero massimo di documenti che vogliamo siano estratti
'Q.MaxRecords = 100
On error resume next
' viene effettuata la ricerca e viene creato il recordset dei risultati


Set rstIndex = Q.CreateRecordSet("nonsequential")
If err <> 0 then
	Response.Write "<TABLE width='370' border='0'>"
	Response.Write "<tr><td width='20' rowspan='2'></td>"
	Response.Write "<td><span class='tbltext1'>La ricerca non puo' essere eseguita</span><br></td></tr>"
	Response.Write "<tr><td><span class='tbltext1'>Chiave di ricerca non significativa.<br> Eseguirla nuovamente in una differente modalita'</span><br></td></tr>"
	Response.Write "</TABLE>"
Else
	If rstIndex.EOF Then
		Response.Write "<TABLE border=0><tr><td class=tbltext3><br><br>Non sono presenti documenti non ancora validati<br><br><br></td></tr>"
		Response.Write "<tr><td class=tbltext3 align=center ><a href=Javascript:history.back()><IMG SRC='" & Session("Progetto") & "/images/indietro.gif' border=0></a></td></tr></TABLE>"
	Else


	rstIndex.PageSize = nTamPagina
	rstIndex.CacheSize = nTamPagina
	

	nTotPagina = rstIndex.PageCount
	 
	If nActPagina < 1 Then
		nActPagina = 1
	End If
	If nActPagina > nTotPagina Then
		nActPagina = nTotPagina
	End If
	
	rstIndex.AbsolutePage = nActPagina
	nTotDocFind = rstIndex.Recordcount
	nTotRecord = 0 
	nInitDocVis = nRecVis + 1
	nEndDocVis = nRecVis + 10
	If nEndDocVis > nTotDocFind Then
		nEndDocVis = nTotDocFind
	End If
	
	n = 0 		
	'mostra i risultati

	'Response.Write Session("mask")
	Do While Not rstIndex.EOF
	
	stitle = 0
	title  = rstIndex("DocTitle")	
	stitle = InStr(1, lcase(title), "bozza")

	sauthor = 0
	author  = rstIndex("DocAuthor")
	nom =  Session("Nome") & " " & Session("Cognome")

	if trim(lcase(author)) = trim(lcase(nom)) then
		sauthor = 1
	end if	

	if stitle = 0 and sauthor > 0 then
	
		path = rstIndex("path")
 		strhref = split(lcase(path),lcase(Server.MapPath("\")))(1)
'        strhref = replace(strhref,"\","\\")
		strhref = "/Pgm/BancaDati/BDD_VisDocumento.asp?pag=" & strhref 
        st1 = split(lcase(path),lcase(Server.MapPath("\")))(1)
        st2 = split(lcase(st1),"\")(1)
        strdoc = split(lcase(st1),lcase(st2))(1)

		p = mid(strdoc,13,2)

		if ucase(p) = "IL" then
			privat = "<br>documento privato riservato agli utenti di ItaliaLavoro "
			varil = "IL"			
		else
			privat = ""	
			varil = "PB"			
		end if		
		
		p1 = mid(strdoc,16,4)
		p2 = mid(strdoc,17,4)
		
		if ucase(p1) = "PROV" or ucase(p2) = "PROV" then
			privat = privat & "<br>documento provvisorio"
			varpr = "PR"			
		else
			varpr = "DF"			
		end if
 
		if 	varpr = "PR" then

		
			' documento visibile a tutti
				filnam = ""		
                nomef = ""

			filnam = rstIndex("Filename") & "&varil=" & varil & "&varpr=" & varpr
			nomef = rstIndex("Filename")		
	
			cnote = ""
			modif = ""
			cance = ""
			pubbl = ""

		UB =  Ubound(split(rstIndex("comments"),","))
		comments = split(rstIndex("comments"),",")

		cdataagg = comments(0)
		
		for i = 1 to UB
			cnote = cnote & comments(i) & ","
		next		
		
		lencnote = len(cnote)
		cnote= mid(cnote,1,lencnote-1)
		
			if cnote > "" then
				cnote = "<span class='tbltext' align='left'>note : " & cnote & "<br>"
			end if

			if mid(Session("mask"),1,1) = "0" then
				modif = "</td><td align=center width=100 class=sfondocomm><a class=textred href='BDD_modifica.asp?nf=" & filnam & "'><b>Modifica</b></a><br><br>" 
				cance = "<a class=textred href=Javascript:Canc('" & nomef & "','" & varil & "','" & varpr & "');><b>Cancella</b></a><br><br>"
				if varpr = "PR" and  mid(Session("mask"),2,1) = "0" then
					pubbl = "<a class=textred href='BDD_pubblica.asp?nf=" & filnam & "'><b>Pubblica</b></a><br><br>"
				else
					pubbl = ""
				end if	
			end if

			Response.Write "<TABLE border=0 width=450>" & _
								"<tr>" & _
								"<td align=left nowrap width=450>" & _
								"<a class=tbltext href='" & Strhref & "'><u><b>" & rstIndex("DocTitle") & "</a></b></u><span class=textred>" & privat & "<br>" & _
								"<span class='tbltext'>" & rstIndex("abstract") & "<br>" & _
								"<span class='tbltext'>" & mid(rstIndex("description"),4) & " - " & rstIndex("progid") & "<br>" 
								if mid(Session("mask"),1,1) = "0" then	%>		
									<span class='tbltext'>Redattore : <%=rstIndex("DocAuthor") %>
									 <br> 
								<%end if 								
								Response.Write "<span class='tbltext'>Ultimo Aggiornamento : " & cdataagg & "<br>" & _
								cnote  & _
								modif & _
								cance & _
								pubbl & _
								"</tr><tr height='1'><td colspan=2 background='/images/separagrigio.gif'></td></tr><tr><td>" & _
								"</td></tr>"
			Response.Write "</TABLE>"
			nRecVis = nRecVis + 1
			nTotRecord = nTotRecord + 1	

		n = n + 1
		end if
	end if

			title = ""
			stitle = ""
			publ = ""
			path = ""
			strhref = ""
			st1 = ""
			st2 = ""
			strdoc = ""
			privat = ""
			varil = ""		
			p1 = ""
			p2 = ""
			varpr = ""			
			filnam = ""
			nomef = ""
			cnote = ""
			modif = ""
			cance = ""
			pubbl = ""
			comments = ""
			cdataagg = ""
			cnote = ""
			
	rstIndex.movenext
	Loop
	
	
	rstIndex.Close
	Set rstIndex = nothing
	
	End If
End If

	If not rstIndex.EOF Then
		if n = 0 then
			Response.Write "<TABLE border=0><tr><td class=tbltext3><br><br>Non sono presenti documenti non ancora validati<br><br><br></td></tr>"
			Response.Write "<tr><td class=tbltext3 align=center ><a href=Javascript:history.back()><IMG SRC='" & Session("Progetto") & "/images/indietro.gif' border=0></a></td></tr></TABLE>"
		end if
	end if	
%>
<br><table cellSpacing="4" cellPadding="4" width="500" align="center" border="0">
	<tr>
		<td align="middle"><a href="BDD_Ricerca.asp" class="textred"><b>Ricerca documento</b></a></td>
		<% if mid(Session("mask"),3,1) = "0" then%>
		<td align="middle"><a href="BDD_InsDoc.asp" class="textred"><b>Inserimento nuovo documento</b></a></td>
		<%end if %>	
	</tr>  
</table>
<!--#include Virtual = "/strutt_coda2.asp"-->
