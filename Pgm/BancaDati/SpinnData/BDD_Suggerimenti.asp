<%@ Language=VBScript %>
<html>
<head>
<meta NAME="GENERATOR" Content="Microsoft Visual Studio 6.0">
<link REL="STYLESHEET" TYPE="text/css" HREF="<%=session("progetto")%>/fogliostile.css">

</head>
<body>
<table border="0" width="510" cellspacing="0" cellpadding="0" height="30">
	<tr>
		<td width="530" background="<%=Session("Progetto")%>/images/titoli/barracerca.jpg" valign="bottom" align="right">
			<table border="0" width="360" height="30" cellspacing="0" cellpadding="0">
				<tr>
					<td width="100%" valign="middle" align="right"><b class="tbltext1a">Suggerimenti per la ricerca semplice&nbsp;&nbsp;</span></b></td>
				</tr>
			</table>
		</td>
	</tr>
</table><br>

</table><br><table border="0" CELLPADDING="0" CELLSPACING="1" width="500">
	<tr>
		<td class="tbltexta"><a name="help"><b>Suggerimenti per la ricerca per parola</b><br><br></td>
	</tr>
<tr><td class="tbltext">

Lo scopo della Ricerca per parola � delimitare il campo di ricerca ai documenti in cui compaiono entrambi i termini uniti attraverso l'operatore 'AND'.<br>
Procedura: separate i termini con uno spazio.<br>
Esempio: se digitate &quot;asilo nido&quot; otterrete la lista di tutti i documenti che contengono <b>sia</b> la parola asilo <b>sia</b> la parola nido.

<br><br>
</td>
	</tr>

	<tr>
		<td class="tbltexta"><a name="help"><b>Selezione delle categorie</b><br><br></td>
	</tr>
	<tr>
		<td class="tbltext">La Ricerca per categorie � uno strumento molto utile per effettuare ricerche limitate su un argomento specifico. 
		Ad esempio, per visualizzare solo risultati relativi alla Categoria Normativa, � sufficiente inserire il termine da ricercare 
		e selezionare solo &quot;Normativa&quot; nella lista . In questo modo si evita che vengano visualizzati risultati relativi alle
		 altre categorie, al gioco omonimo o altri risultati che possano essere correlati al termine scelto. <br>Le ricerche all'interno
		  di categorie specifiche permettono di restringere il campo di ricerca e di visualizzare rapidamente solo le pagine desiderate. 
<br>NB: E' abbinabile alla ricerca per parola<br></td>
	</tr>
</table>
</table><br><table border="0" CELLPADDING="0" CELLSPACING="1" width="500">
<tr><td align="center"><a HREF="javascript:window.close()"><img SRC="<%=Session("Progetto")%>/Images/chiudi.gif" border="0"></a></td></tr>
</table>
</body>
</html>
