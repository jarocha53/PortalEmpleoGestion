<!--#include Virtual = "/strutt_testa2.asp"-->
<!-- #include virtual="/include/OpenConn.asp" -->
<!-- #include virtual="/include/DecCod.asp" -->
<script language="javascript" src="/include/help.inc"></script>

<%
If Not ValidateService(Session("IdUtente"),"BDD_InsDoc",cc) Then 
	response.redirect "/util/error_login.asp"
End If

nf = Request.QueryString("nf")

varil = Request.QueryString("varil")

varpr = Request.QueryString("varpr")

nomefile = nf

SP = Session("Progetto")

if varil = "PB" then
	Session("PosDoc") = Server.MapPath("\") & SP & "\DocPrg\Doc\Pub\"
else
	Session("PosDoc") = Server.MapPath("\") & SP & "\DocPrg\Doc\IL\"
end if

if varpr = "PR" then
	Session("PosDoc") = Session("PosDoc") & "Prov\"
end if

'Response.Write Session("PosDoc")

%> 
<script language="Javascript" src="/include/ControlDate.inc"></script>
<script language="Javascript" src="/include/ControlNum.inc"></script>
<script language="Javascript" src="/include/ControlString.inc"></script>
<script language="Javascript" src="Controlli.js"></script>

<script language="Javascript">

// apre i popup

	function viewlink(URL,CAMPO,DESCR,TAB,VAL)
	{
		var sValue
		sValue = document.all.item(CAMPO).innerText
		URL = URL + "?descr=" + DESCR + "&tab=" + TAB + "&val=" + VAL + "&campo=" + CAMPO + "&id=" + sValue  
		window.open ( URL,"Rubrica","Status=no,scrollbars=yes,toolbar=no,width=550,height=500,top=20,left=100")
	}

// inizializzazione della pagina, vengono valorizzati
// i campi con i valori letti all'interno del documento


	function inizio()
	{
	
		window.document.forma.linktit1.style.display = "none";
		window.document.forma.alletit1.style.display = "none";
		window.document.forma.note1.style.display = "none";
		window.document.forma.imgprov.style.display = "none";
		window.document.forma.imgreg.style.display = "none";
		window.document.forma.titolo.value = wtitolo.value;
		window.document.forma.autore.value = wautore.value;
		window.document.forma.dataimm.value = wdataimm.value;
		window.document.forma.note1.value = wnote.value;
		window.note.innerHTML = "note: " + wnote.value;
		window.document.forma.abst.value = wabstract.value;
		window.document.forma.dataimm.value = wdataimm.value;
		window.document.forma.contenuto.value = wcontenuto.value;
		window.linktit.innerHTML = wlinktit.value;
		window.document.forma.linktit1.value = wlinktit.value;
		window.alletit.innerHTML = walletit.value;
		window.document.forma.linkh1.value = wlink1h.value;
		window.document.forma.linkt1.value = wlink1t.value;
		window.document.forma.linkh2.value = wlink2h.value;
		window.document.forma.linkt2.value = wlink2t.value;
		window.document.forma.linkh3.value = wlink3h.value;
		window.document.forma.linkt3.value = wlink3t.value;
		window.document.forma.linkh4.value = wlink4h.value;
		window.document.forma.linkt4.value = wlink4t.value;
		window.document.forma.linkh5.value = wlink5h.value;
		window.document.forma.linkt5.value = wlink5t.value;
		window.document.forma.linkh6.value = wlink6h.value;
		window.document.forma.linkt6.value = wlink6t.value;
		window.document.forma.linkh7.value = wlink7h.value;
		window.document.forma.linkt7.value = wlink7t.value;
		window.document.forma.linkh8.value = wlink8h.value;
		window.document.forma.linkt8.value = wlink8t.value;
		window.document.forma.linkh9.value = wlink9h.value;
		window.document.forma.linkt9.value = wlink9t.value;
		window.document.forma.linkh10.value = wlink10h.value;
		window.document.forma.linkt10.value = wlink10t.value;
		window.link1(1).innerHTML = wlink.value;

/////////////////////////////////////////////////////////////////////////
///////////
// determino i checkbox che devono essere ceccati, il valore da attribuire ai campi nascosti
// e alle label delle categorie dopo aver letto i dati direttamente dal documento

		if ((ch1.value > " ") && (wcat1.value == ""))
		{
			window.document.forma.chknorma1.checked = true
			norma1.innerHTML = "TUTTE"
			window.document.forma.norma1a.value = "normativa nazionale"			
		}	
		else
		{
			window.document.forma.chknorma1.checked = false
			norma1.innerHTML = wcat1.value
			window.document.forma.norma1a.value = ch1.value + ',' + wcat1.value
		}
/////////////////////////////////////////////////////////////////////////
///////////
		if ((ch2.value > " ") && (wcat2.value == ""))
		{
			window.document.forma.chknorma2.checked = true
			norma2.innerHTML = "TUTTE"
			window.document.forma.norma2a.value = "normativa regionale"
		}
		else
		{
			window.document.forma.chknorma2.checked = false
			norma2.innerHTML = wcat2.value
			window.document.forma.norma2a.value = ch2.value + ',' + wcat2.value
		}
/////////////////////////////////////////////////////////////////////////
///////////
		if ((ch3.value > " ") && (wcat3.value == ""))
		{
			window.document.forma.chknorma3.checked = true
			norma3.innerHTML = "TUTTE"
			window.document.forma.norma3a.value = "normativa europea"
		}
		else
		{
			window.document.forma.chknorma3.checked = false
			norma3.innerHTML = wcat3.value
			window.document.forma.norma3a.value = ch3.value + ',' + wcat3.value
		}
/////////////////////////////////////////////////////////////////////////
///////////
		if ((window.document.forma.chknorma1.checked) && 
(window.document.forma.chknorma2.checked) && 
(window.document.forma.chknorma3.checked))
		{
			window.document.forma.chknorma.checked = true
		}		
		else
		{
			window.document.forma.chknorma.checked = false
		}
		
/////////////////////////////////////////////////////////////////////////
///////////
		if ((ch4.value > " ") && (wcat4.value == ""))
		{
			window.document.forma.chkbandi1.checked = true
			bandi1.innerHTML = "TUTTE"
			window.document.forma.bandi1a.value = "bandi e avvisi"
		}
		else
		{
			window.document.forma.chkbandi1.checked = false
			bandi1.innerHTML = wcat4.value
			window.document.forma.bandi1a.value = ch4.value + ',' + wcat4.value
		}
/////////////////////////////////////////////////////////////////////////
///////////
		if ((ch5.value > " ") && (wcat5.value == ""))
		{
			window.document.forma.chkstrum1.checked = true
			strum1.innerHTML = "TUTTE"
			window.document.forma.strum1a.value = "strumenti"
		}
		else
		{
			window.document.forma.chkstrum1.checked = false
			strum1.innerHTML = wcat5.value
			window.document.forma.strum1a.value = ch5.value + ',' + wcat5.value
		}
/////////////////////////////////////////////////////////////////////////
///////////
		if ((ch6.value > " ") && (wcat6.value == ""))
		{
			window.document.forma.chkbench1.checked = true
			bench1.innerHTML = "TUTTE"
			window.document.forma.bench1a.value = "benchmark"
		}
		else
		{
			window.document.forma.chkbench1.checked = false
			bench1.innerHTML = wcat6.value
			window.document.forma.bench1a.value = ch6.value + ',' + wcat6.value
		}
/////////////////////////////////////////////////////////////////////////
///////////
		if ((ch7.value > " ") && (wcat7.value == ""))
		{
			window.document.forma.chkmater1.checked = true
			mater1.innerHTML = "TUTTE"
			window.document.forma.mater1a.value = "materiali italia lavoro"
		}
		else
		{
			window.document.forma.chkmater1.checked = false
			mater1.innerHTML = wcat7.value
			window.document.forma.mater1a.value = ch7.value + ',' + wcat7.value
		}
/////////////////////////////////////////////////////////////////////////
///////////
		if ((ch8.value > " ") && (wcat8.value == ""))
		{
			window.document.forma.chkdocum1.checked = true
			docum1.innerHTML = "TUTTE"
			window.document.forma.docum1a.value = "documentazione nazionale"
		}
		else
		{
			window.document.forma.chkdocum1.checked = false
			docum1.innerHTML = wcat8.value
			window.document.forma.docum1a.value = ch8.value + ',' + wcat8.value
		}
/////////////////////////////////////////////////////////////////////////
///////////
		if ((ch9.value > " ") && (wcat9.value == ""))
		{
			window.document.forma.chkdocum2.checked = true
			docum2.innerHTML = "TUTTE"
			window.document.forma.docum2a.value = "documentazione regionale"
		}
		else
		{
			window.document.forma.chkdocum2.checked = false
			docum2.innerHTML = wcat9.value
			window.document.forma.docum2a.value = ch9.value + ',' + wcat9.value
		}
/////////////////////////////////////////////////////////////////////////
///////////
		if ((ch10.value > " ") && (wcat10.value == ""))
		{
			window.document.forma.chkdocum3.checked = true
			docum3.innerHTML = "TUTTE"
			window.document.forma.docum3a.value = "documentazione europea"
		}
		else
		{
			window.document.forma.chkdocum3.checked = false
			docum3.innerHTML = wcat10.value
			window.document.forma.docum3a.value = ch10.value + ',' + wcat10.value
		}
/////////////////////////////////////////////////////////////////////////
///////////
		if ((window.document.forma.chkdocum1.checked) && 
(window.document.forma.chkdocum2.checked) && 
(window.document.forma.chkdocum3.checked))
		{
			window.document.forma.chkdocum.checked = true
		}
		else
		{
			window.document.forma.chkdocum.checked = false
		}
/////////////////////////////////////////////////////////////////////////
///////////
		if ((ch11.value > " ") && (wcat11.value == ""))
		{
			window.document.forma.chkschede1.checked = true
			settore1.innerHTML = "TUTTE"
			window.document.forma.schede1a.value = "schede e recensioni"
		}
		else
		{
			window.document.forma.chkschede1.checked = false
			schede1.innerHTML = wcat11.value
			window.document.forma.schede1a.value = ch11.value + ',' + wcat11.value
		}
/////////////////////////////////////////////////////////////////////////
///////////
		if ((ch12.value > " ") && (wcat12.value == ""))
		{
			window.document.forma.chksettore1.checked = true
			settore1.innerHTML = "TUTTE"
			window.document.forma.settore1a.value = "settore"
		}
		else
		{
			window.document.forma.chksettore1.checked = false
			settore1.innerHTML = wcat12.value
			window.document.forma.settore1a.value = ch12.value + ',' + wcat12.value
		}
/////////////////////////////////////////////////////////////////////////
///////////
		if ((ch13.value > " ") && (wcat13.value == ""))
		{
			window.document.forma.chkazioni1.checked = true
			azioni1.innerHTML = "TUTTE"
			window.document.forma.azioni1a.value = "azioni"
		}
		else
		{
			window.document.forma.chkazioni1.checked = false
			azioni1.innerHTML = wcat13.value
			window.document.forma.azioni1a.value = ch13.value + ',' + wcat13.value
		}
/////////////////////////////////////////////////////////////////////////
///////////
		if ((ch14.value > " ") && (wcat14.value == ""))
		{
			window.document.forma.chkarea1.checked = true
			area1.innerHTML = "TUTTE"
			window.document.forma.area1a.value = "territorio"
		}
		else
		{
			window.document.forma.chkarea1.checked = false
			area1.innerHTML = wcat14.value
			window.document.forma.area1a.value = ch14.value + ',' + wcat14.value
		}
/////////////////////////////////////////////////////////////////////////
///////////
		if ((ch15.value > " ") && (wcat15.value == ""))
		{
			window.document.forma.chkeuro1.checked = true
			euro1.innerHTML = "TUTTE"
			window.document.forma.euro1a.value = "unione europea"
		}
		else
		{
			window.document.forma.chkeuro1.checked = false
			euro1.innerHTML = wcat15.value
			window.document.forma.euro1a.value = ch15.value + ',' + wcat15.value
		}
/////////////////////////////////////////////////////////////////////////
///////////
		if ((ch16.value > " ") && (wcat16.value == ""))
		{
			window.document.forma.chkregio1.checked = true
			regio1.innerHTML = "TUTTE"
			window.document.forma.regio1a.value = "regione"
		}
		else
		{
			window.document.forma.chkregio1.checked = false
			regio1.innerHTML = wcat16.value
			window.document.forma.regio1a.value = ch16.value + ',' + wcat16.value
		}
/////////////////////////////////////////////////////////////////////////
///////////
		if ((ch17.value > " ") && (wcat17.value == ""))
		{
			window.document.forma.chkprov1.checked = true
			prov1.innerHTML = "TUTTE"
			window.document.forma.prov1a.value = "provincia"
		}
		else
		{
			window.document.forma.chkprov1.checked = false
			prov1.innerHTML = wcat17.value
			window.document.forma.prov1a.value = ch17.value + ',' + wcat17.value
		}
/////////////////////////////////////////////////////////////////////////
///////////
		window.document.forma.titolo.focus() 
	}
/////////////////////////////////////////////////////////////////////////
///////////
/////////////////////////////////////////////////////////////////////////
///////////


	function invia()
	{
		if ((window.document.forma.titolo.value == "")||
			(window.document.forma.titolo.value == " ")){
			alert("Il campo Titolo � obbligatorio!")
			window.document.forma.titolo.focus() 
			return false
		}
		
		//inizio 02/04
		stitolo=ValidateInputStringIsValid(document.forma.titolo.value)
		 
		if  (stitolo==false){
			document.forma.titolo.focus() 
			return false
		}
		//fine 02/04
		
		if ((window.document.forma.abst.value == "")||
			(forma.abst.value == " ")){
			alert("Il campo Abstract � obbligatorio!")
			window.document.forma.abst.focus() 
			return false
		}		

		if ((window.document.forma.contenuto.value == "")||
			(forma.contenuto.value == " ")){
			alert("Il campo Contenuto � obbligatorio!")
			window.document.forma.contenuto.focus() 
			return false
		}

		if ((window.document.forma.fonte.value == "")||
			(forma.fonte.value == " ")){
			alert("Il campo Fonte � obbligatorio!")
			window.document.forma.fonte.focus() 
			return false
		}
	
		if ((window.document.forma.autore.value == "")||
			(forma.autore.value == " ")){
			alert("Il campo Redattore � obbligatorio!")
			window.document.forma.autore.focus() 
			return false
		}		

		if ((window.document.forma.dataimm.value == "")||
			(forma.dataimm.value == " ")){
			alert("Il campo Data � obbligatorio!")
			window.document.forma.dataimm.focus() 
			return false
		}

		if (!ValidateInputDate(window.document.forma.dataimm.value))
		{
			window.document.forma.dataimm.focus() 
			return false
		}		
			
		if ((!document.forma.chknorma1.checked) &&
			(!document.forma.chknorma2.checked) &&
			(!document.forma.chknorma3.checked) &&
			(!document.forma.chkbandi1.checked) &&
			(!document.forma.chkstrum1.checked) &&
			(!document.forma.chkbench1.checked) &&
			(!document.forma.chkmater1.checked) &&
			(!document.forma.chkdocum1.checked) &&
			(!document.forma.chkdocum2.checked) &&
			(!document.forma.chkdocum3.checked) &&
			(!document.forma.chkschede1.checked) &&
			(!document.forma.chksettore1.checked) &&
			(!document.forma.chkazioni1.checked) &&
			(document.forma.norma1a.value == "") &&
			(document.forma.norma2a.value == "") &&
			(document.forma.norma3a.value == "") &&
			(document.forma.bandi1a.value == "") &&
			(document.forma.strum1a.value == "") &&
			(document.forma.bench1a.value == "") &&
			(document.forma.mater1a.value == "") &&
			(document.forma.docum1a.value == "") &&
			(document.forma.docum2a.value == "") &&
			(document.forma.docum3a.value == "") &&
			(document.forma.schede1a.value == "") &&
			(document.forma.settore1a.value == "") &&
			(document.forma.azioni1a.value == ""))
		{
			alert("Devi selezionare almeno un argomento!")
			return false
		}	
	      return true
		
	}
	
	function Salva()
{
	var appo;
	appo= invia();
	
	if (appo==true) 
	{
		forma.target="_self";
		forma.action="BDD_CnfModifica.asp";
		forma.submit();
	}	
}

	
	function anteprima()
{
	forma.target="_new";
	forma.action="BDD_PreAnteprima.asp";
	forma.submit();
}

</script>


<%

' inizio elaborazione file



W = " @FileName " & nomefile
'Response.Write nomefile
Set Q = Server.CreateObject("ixsso.Query")
'si fissa come criterio di ordinamento la raggiungibilit�, discendente
SortBy = "filename, rank[d]"
Q.SortBy = SortBy
'si definiscono alcune colonne ulteriori
Q.DefineColumn "description (DBTYPE_WSTR) = d1b5d3f0-c0b3-11cf-9a92-00a0c908dbf1 description"
Q.DefineColumn "comments (DBTYPE_WSTR) = d1b5d3f0-c0b3-11cf-9a92-00a0c908dbf1 comments"
Q.DefineColumn "abstract (DBTYPE_WSTR) = d1b5d3f0-c0b3-11cf-9a92-00a0c908dbf1 abstract"
Q.DefineColumn "progid (DBTYPE_WSTR) = d1b5d3f0-c0b3-11cf-9a92-00a0c908dbf1 progid"
Q.DefineColumn "originator (DBTYPE_WSTR) = d1b5d3f0-c0b3-11cf-9a92-00a0c908dbf1 originator"
Q.DefineColumn "generator (DBTYPE_WSTR) = d1b5d3f0-c0b3-11cf-9a92-00a0c908dbf1 generator"
Q.DefineColumn "content-type (DBTYPE_WSTR) = d1b5d3f0-c0b3-11cf-9a92-00a0c908dbf1 content-type"

' si specificano le propriet� dei documenti che vogliamo estrarre:
' il nome del file, il suo virtual path, etc.
Q.Columns = "DocTitle, DocAuthor, FileName, Path, vpath, Size, description, comments, abstract, progid, originator, DocKeywords, generator, content-type "

' si definisce il catalogo su cui ricercare
if mid(Session("mask"),1,1) = "1" then
	Q.Catalog = "BancaDatiPub" 
else
	Q.Catalog = "Bancadati" 
end if
' si definisce la query
Q.Query = W
'Response.Write W
' si definisce il numero massimo di documenti che vogliamo siano estratti
On error resume next
' viene effettuata la ricerca e viene creato il recordset dei risultati
Set rstIndex = Q.CreateRecordSet("nonsequential")

'Response.Write "<TABLE border=0 width=500 class=tbltext>" & _
'					"<tr><td align=left>" & _
'					"DocTitle (titolo file)       : "   & rstIndex("DocTitle")     & 
'"<br><br>" & _
'					"linktitolo (href titolo)     : "   & rstIndex("content-type")    
'& "<br><br>" & _
'					"Abstract (abstract)           : "   & rstIndex("abstract")     & 
'"<br><br>" & _
'					"DocAuthor (Autore)           : "   & rstIndex("DocAuthor")    & 
'"<br><br>" & _
'					"ProgID (data fonte)          : "   & rstIndex("progid")       & 
'"<br><br>" & _
'					"Description1 (IDFonte)       : "   & 
'mid(rstIndex("description"),1,2) & "<br><br>" & _
'					"Description2 (Fonte)         : "   & 
'mid(rstIndex("description"),4) & "<br><br>" & _
'					"comments1 (dataimm)          : "   & 
'mid(rstIndex("comments"),1,10) & "<br><br>" & _
'					"comments2 (note)             : "   & mid(rstIndex("comments"),12) 
'& "<br><br>" & _
'					"generator (contenuto)        : "   & rstIndex("generator")    & 
'"<br><br>" & _
'					"originator (links)           : "   & rstIndex("originator")   & 
'"<br><br>" & _
'					"DocKeywords (chiavi)         : "   & rstIndex("DocKeywords")  & 
'"<br><br>" & _
'					"</td></tr></TABLE>"
'Response.Write Q.Catalog 
'Response.Write rstIndex("Filename")

wFilename = rstIndex("Filename")
wDocTitle = rstIndex("DocTitle")
wprogid = rstIndex("progid")
wabstract = rstIndex("abstract")
wDocAuthor = rstIndex("DocAuthor")	

wDocKeywords = rstIndex("DocKeywords")
'Response.Write "wDocKeywords: " & wDocKeywords & "<BR>"	
woriginator = rstIndex("originator") 
wgenerator = rstIndex("generator")
wcontenttype = rstIndex("content-type")


UB =  Ubound(split(rstIndex("comments"),","))
comments = split(rstIndex("comments"),",")

wdataimm = comments(0)
		
for i = 1 to UB
	cnote = cnote & comments(i) & ","
next		
		
lencnote = len(cnote)
wnote= mid(cnote,1,lencnote-1)


widfonte = mid(rstIndex("description"),1,2)
wfonte = mid(rstIndex("description"),4)

	menu=split(rstIndex("originator"),"$")

	wlinkh1 = menu(0) 
	wlinkt1 = menu(1)
	
	wlinkh2 = menu(2)
	wlinkt2 = menu(3)
	
	wlinkh3 = menu(4)
	wlinkt3 = menu(5)
	
	wlinkh4 = menu(6)
	wlinkt4 = menu(7)
	
	wlinkh5 = menu(8)
	wlinkt5 = menu(9)
	
	wlinkh6 = menu(10)
	wlinkt6 = menu(11)
	
	wlinkh7 = menu(12)
	wlinkt7 = menu(13)
	
	wlinkh8 = menu(14) 
	wlinkt8 = menu(15)
	
	wlinkh9 = menu(16)
	wlinkt9 = menu(17)		

	wlinkh10 = menu(18)
	wlinkt10 = menu(19)			


	menucateg=split(rstIndex("DocKeywords"),",$,")
	
	'Response.Write " menucateg " & menucateg

	wcat1=menucateg(1) ' normativa nazionale
	wcat1=Replace(wcat1, "##", chr(34) )
	wcat1=Replace(wcat1, "#", "'" )				

	wcat2=menucateg(2) ' normativa regionale
	wcat2=Replace(wcat2, "##", chr(34) )
	wcat2=Replace(wcat2, "#", "'" )				
	
	wcat3=menucateg(3) ' normativa europea
	wcat3=Replace(wcat3, "##", chr(34) )
	wcat3=Replace(wcat3, "#", "'" )				
	
	wcat4=menucateg(4) ' bandi e avvisi
	wcat4=Replace(wcat4, "##", chr(34) )
	wcat4=Replace(wcat4, "#", "'" )				
	
	wcat5=menucateg(5) ' strumenti
	wcat5=Replace(wcat5, "##", chr(34) )
	wcat5=Replace(wcat5, "#", "'" )				
	
	wcat6=menucateg(6) ' Benchmarking
	wcat6=Replace(wcat6, "##", chr(34) )
	wcat6=Replace(wcat6, "#", "'" )				
	
	wcat7=menucateg(7) ' Materiali ItaliaLavoro
	wcat7=Replace(wcat7, "##", chr(34) )
	wcat7=Replace(wcat7, "#", "'" )				
	
	wcat8=menucateg(8) ' documentazione nazionale
	wcat8=Replace(wcat8, "##", chr(34) )
	wcat8=Replace(wcat8, "#", "'" )				
	
	wcat9=menucateg(9) ' documentazione regionale
	wcat9=Replace(wcat9, "##", chr(34) )
	wcat9=Replace(wcat9, "#", "'" )				
	
	wcat10=menucateg(10) ' documentazione europea
	wcat10=Replace(wcat10, "##", chr(34) )
	wcat10=Replace(wcat10, "#", "'" )				
	
	wcat11=menucateg(11) ' settore
	wcat11=Replace(wcat11, "##", chr(34) )
	wcat11=Replace(wcat11, "#", "'" )				
	
	wcat12=menucateg(12) ' azione	 
	wcat12=Replace(wcat12, "##", chr(34) )
	wcat12=Replace(wcat12, "#", "'" )				

	wcat13=menucateg(13) ' territorio	 
	wcat13=Replace(wcat13, "##", chr(34) )
	wcat13=Replace(wcat13, "#", "'" )				

	wcat14=menucateg(14) ' unione europea	 
	wcat14=Replace(wcat14, "##", chr(34) )
	wcat14=Replace(wcat14, "#", "'" )				

	wcat15=menucateg(15) ' regioni italiane 	 
	wcat15=Replace(wcat15, "##", chr(34) )
	wcat15=Replace(wcat15, "#", "'" )				

	wcat16=menucateg(16) ' province italiane	 
	wcat16=Replace(wcat16, "##", chr(34) )
	wcat16=Replace(wcat16, "#", "'" )				
	
	
	if wlinkh1 > "" then
		wlink = "<b>" & wlinkt1 & "</b> (http://" & wlinkh1 & ") <br><br>" 
	end if
	if wlinkh2 > "" then
		wlink = wlink & "<b>" & wlinkt2 & "</b> (http://" & wlinkh2 & ")<br><br>" 
	end if
	if wlinkh3 > "" then
		wlink = wlink & "<b>" & wlinkt3 & "</b> (http://" & wlinkh3 & ")<br><br>" 
	end if
	if wlinkh4 > "" then
		wlink = wlink & "<b>" & wlinkt4 & "</b> (http://" & wlinkh4 & ")<br><br>"  
	end if
	if wlinkh5 > "" then
		wlink = wlink & "<b>" & wlinkt5 & "</b> (http://" & wlinkh5 & ")<br><br>" 
	end if
	if wlinkh6 > "" then
		wlink = wlink & "<b>" & wlinkt6 & "</b> (http://" & wlinkh6 & ")<br><br>"  
	end if
	if wlinkh7 > "" then
		wlink = wlink & "<b>" & wlinkt7 & "</b> (http://" & wlinkh7 & ")<br><br>" 
	end if
	if wlinkh8 > "" then
		wlink = wlink & "<b>" & wlinkt8 & "</b> (http://" & wlinkh8 & ")<br><br>"  
	end if
	if wlinkh9 > "" then
		wlink = wlink & "<b>" & wlinkt9 & "</b> (http://" & wlinkh9 & ")<br><br>"  
	end if
	if wlinkh10 > "" then
		wlink = wlink & "<b>" & wlinkt10 & "</b> (http://" & wlinkh10 & ")<br><br>"  
	end if

if mid(wcontenttype,1,4) = "http" then
	hreftitolo = wcontenttype
else
	if mid(wcontenttype,1,4) > "" then
		alletitolo = wcontenttype				
	else
		hreftitolo = ""
		alletitolo = ""
	end if
end if

if tipofile = "PB" then
	perc = "Doc\Pub\" 
else
	perc = "Doc\IL\" 
end if

nfile = Server.MapPath("\") & "\" & perc & wFilename

Dim fso, f
Set fso = CreateObject("Scripting.FileSystemObject")
Set fileop = fso.OpenTextFile(nfile)

%>
	<input type="hidden" id="wtitolo" name="wtitolo" value="<%=wDocTitle%>">
	<input type="hidden" id="wlinktit" name="wlinktit" value="<%=hreftitolo%>">
	<input type="hidden" id="walletit" name="walletit" value="<%=alletitolo%>">
	<input type="hidden" id="wautore" name="wautore" value="<%=wDocAuthor%>">
	<input type="hidden" id="wdataimm" name="wdataimm" value="<%=wprogid%>">
	<input type="hidden" id="wnote" name="wnote" value="<%=wnote%>">
	<input type="hidden" id="wfonte" name="wfonte" value="<%=wfonte%>">
	<input type="hidden" id="widfonte" name="widfonte" value="<%=widfonte%>">
	<input type="hidden" id="wabstract" name="wabstract" value="<%=wabstract%>">
	<input type="hidden" id="wcontenuto" name="wcontenuto" value="<%=wgenerator%>">
	<input type="hidden" id="wlink" name="wlink" value="<%=wlink%>">
	<input type="hidden" id="wlink1h" name="wlink1h" value="<%=wlinkh1%>">
	<input type="hidden" id="wlink1t" name="wlink1t" value="<%=wlinkt1%>">
	<input type="hidden" id="wlink2h" name="wlink2h" value="<%=wlinkh2%>">
	<input type="hidden" id="wlink2t" name="wlink2t" value="<%=wlinkt2%>">
	<input type="hidden" id="wlink3h" name="wlink3h" value="<%=wlinkh3%>">
	<input type="hidden" id="wlink3t" name="wlink3t" value="<%=wlinkt3%>">
	<input type="hidden" id="wlink4h" name="wlink4h" value="<%=wlinkh4%>">
	<input type="hidden" id="wlink4t" name="wlink4t" value="<%=wlinkt4%>">
	<input type="hidden" id="wlink5h" name="wlink5h" value="<%=wlinkh5%>">
	<input type="hidden" id="wlink5t" name="wlink5t" value="<%=wlinkt5%>">
	<input type="hidden" id="wlink6h" name="wlink6h" value="<%=wlinkh6%>">
	<input type="hidden" id="wlink6t" name="wlink6t" value="<%=wlinkt6%>">
	<input type="hidden" id="wlink7h" name="wlink7h" value="<%=wlinkh7%>">
	<input type="hidden" id="wlink7t" name="wlink7t" value="<%=wlinkt7%>">
	<input type="hidden" id="wlink8h" name="wlink8h" value="<%=wlinkh8%>">
	<input type="hidden" id="wlink8t" name="wlink8t" value="<%=wlinkt8%>">
	<input type="hidden" id="wlink9h" name="wlink9h" value="<%=wlinkh9%>">
	<input type="hidden" id="wlink9t" name="wlink9t" value="<%=wlinkt9%>">
	<input type="hidden" id="wlink10h" name="wlink10h" value="<%=wlinkh10%>">
	<input type="hidden" id="wlink10t" name="wlink10t" value="<%=wlinkt10%>">


	<%
	pos = instr(1,wcat1,",")
	ch1 = mid(wcat1,1,pos-1)
	if len(ch1) < 1 then
		ch1 = mid(wcat1,pos+1)
		wcat1 = ""
	else
		wcat1 = mid(wcat1,pos+1)
	end if	
	%>
	<input type="hidden" id="ch1" name="ch1" value="<%=ch1%>">
	<input type="hidden" id="wcat1" name="wcat1" value="<%=wcat1%>">
		
	<%
	pos = instr(1,wcat2,",")
	ch2 = mid(wcat2,1,pos-1)
	if len(ch2) < 1 then
		ch2 = mid(wcat2,pos+1)
		wcat2 = ""
	else
		wcat2 = mid(wcat2,pos+1)
	end if
	%>
	<input type="hidden" id="ch2" name="ch2" value="<%=ch2%>">
	<input type="hidden" id="wcat2" name="wcat2" value="<%=wcat2%>">		
	<%
	pos = instr(1,wcat3,",")
	ch3 = mid(wcat3,1,pos-1)
	if len(ch3) < 1 then
		ch3 = mid(wcat3,pos+1)
		wcat3 = ""
	else
		wcat3 = mid(wcat3,pos+1)
	end if
	%>
	<input type="hidden" id="ch3" name="ch3" value="<%=ch3%>">
	<input type="hidden" id="wcat3" name="wcat3" value="<%=wcat3%>">		
	<%
	pos = instr(1,wcat4,",")
	ch4 = mid(wcat4,1,pos-1)
	if len(ch4) < 1 then
		ch4 = mid(wcat4,pos+1)
		wcat4 = ""
	else
		wcat4 = mid(wcat4,pos+1)
	end if	
	%>
	<input type="hidden" id="ch4" name="ch4" value="<%=ch4%>">
	<input type="hidden" id="wcat4" name="wcat4" value="<%=wcat4%>">		
	<%
	pos = instr(1,wcat5,",")
	ch5 = mid(wcat5,1,pos-1)
	if len(ch5) < 1 then
		ch5 = mid(wcat5,pos+1)
		wcat5 = ""
	else
		wcat5 = mid(wcat5,pos+1)
	end if	
	%>
	<input type="hidden" id="ch5" name="ch5" value="<%=ch5%>">
	<input type="hidden" id="wcat5" name="wcat5" value="<%=wcat5%>">		
	<%
	pos = instr(1,wcat6,",")
	ch6 = mid(wcat6,1,pos-1)
	if len(ch6) < 1 then
		ch6 = mid(wcat6,pos+1)
		wcat6 = ""
	else
		wcat6 = mid(wcat6,pos+1)
	end if	
	%>
	<input type="hidden" id="ch6" name="ch6" value="<%=ch6%>">
	<input type="hidden" id="wcat6" name="wcat6" value="<%=wcat6%>">		
	<%
	pos = instr(1,wcat7,",")
	ch7 = mid(wcat7,1,pos-1)
	if len(ch7) < 1 then
		ch7 = mid(wcat7,pos+1)
		wcat7 = ""
	else
		wcat7 = mid(wcat7,pos+1)
	end if	
	%>
	<input type="hidden" id="ch7" name="ch7" value="<%=ch7%>">
	<input type="hidden" id="wcat7" name="wcat7" value="<%=wcat7%>">		
	<%
	pos = instr(1,wcat8,",")
	ch8 = mid(wcat8,1,pos-1)
	if len(ch8) < 1 then
		ch8 = mid(wcat8,pos+1)
		wcat8 = ""
	else
		wcat8 = mid(wcat8,pos+1)
	end if	
	%>
	<input type="hidden" id="ch8" name="ch8" value="<%=ch8%>">
	<input type="hidden" id="wcat8" name="wcat8" value="<%=wcat8%>">		
	<%
	pos = instr(1,wcat9,",")
	ch9 = mid(wcat9,1,pos-1)
	if len(ch9) < 1 then
		ch9 = mid(wcat9,pos+1)
		wcat9 = ""
	else
		wcat9 = mid(wcat9,pos+1)
	end if	
	%>
	<input type="hidden" id="ch9" name="ch9" value="<%=ch9%>">
	<input type="hidden" id="wcat9" name="wcat9" value="<%=wcat9%>">		
	<%
	pos = instr(1,wcat10,",")
	ch10 = mid(wcat10,1,pos-1)
	if len(ch10) < 1 then
		ch10 = mid(wcat10,pos+1)
		wcat10 = ""
	else
		wcat10 = mid(wcat10,pos+1)
	end if	
	%>
	<input type="hidden" id="ch10" name="ch10" value="<%=ch10%>">
	<input type="hidden" id="wcat10" name="wcat10" value="<%=wcat10%>">		
	<%
	pos = instr(1,wcat11,",")
	ch11 = mid(wcat11,1,pos-1)
	if len(ch11) < 1 then
		ch11 = mid(wcat11,pos+1)
		wcat11 = ""
	else
		wcat11 = mid(wcat11,pos+1)
	end if	
	%>
	<input type="hidden" id="ch11" name="ch11" value="<%=ch11%>">
	<input type="hidden" id="wcat11" name="wcat11" value="<%=wcat11%>">		
	<%
	pos = instr(1,wcat12,",")
	ch12 = mid(wcat12,1,pos-1)
	if len(ch12) < 1 then
		ch12 = mid(wcat12,pos+1)
		wcat12 = ""
	else
		wcat12 = mid(wcat12,pos+1)
	end if	
	%>
	<input type="hidden" id="ch12" name="ch12" value="<%=ch12%>">
	<input type="hidden" id="wcat12" name="wcat12" value="<%=wcat12%>">		
	<%
	pos = instr(1,wcat13,",")
	ch13 = mid(wcat13,1,pos-1)
	if len(ch13) < 1 then
		ch13 = mid(wcat13,pos+1)
		wcat13 = ""
	else
		wcat13 = mid(wcat13,pos+1)
	end if	
	%>
	<input type="hidden" id="ch13" name="ch13" value="<%=ch13%>">
	<input type="hidden" id="wcat13" name="wcat13" value="<%=wcat13%>">		
	<%
	pos = instr(1,wcat14,",")
	ch14 = mid(wcat14,1,pos-1)
	if len(ch14) < 1 then
		ch14 = mid(wcat14,pos+1)
		wcat14 = ""
	else
		wcat14 = mid(wcat14,pos+1)
	end if	
	%>
	<input type="hidden" id="ch14" name="ch14" value="<%=ch14%>">
	<input type="hidden" id="wcat14" name="wcat14" value="<%=wcat14%>">		
	<%
	pos = instr(1,wcat15,",")
	ch15 = mid(wcat15,1,pos-1)
	if len(ch15) < 1 then
		ch15 = mid(wcat15,pos+1)
		wcat15 = ""
	else
		wcat15 = mid(wcat15,pos+1)
	end if	
	%>
	<input type="hidden" id="ch15" name="ch15" value="<%=ch15%>">
	<input type="hidden" id="wcat15" name="wcat15" value="<%=wcat15%>">		
	<%
	pos = instr(1,wcat16,",")
	ch16 = mid(wcat16,1,pos-1)
	if len(ch16) < 1 then
		ch16 = mid(wcat16,pos+1)
		wcat16 = ""
	else
		wcat16 = mid(wcat16,pos+1)
	end if	
	%>
	<input type="hidden" id="ch16" name="ch16" value="<%=ch16%>">
	<input type="hidden" id="wcat16" name="wcat16" value="<%=wcat16%>">	
	<%
	pos = instr(1,wcat17,",")
	ch16 = mid(wcat17,1,pos-1)
	if len(ch17) < 1 then
		ch17 = mid(wcat17,pos+1)
		wcat17 = ""
	else
		wcat17 = mid(wcat17,pos+1)
	end if	
	%>
	<input type="hidden" id="ch17" name="ch17" value="<%=ch17%>">
	<input type="hidden" id="wcat17" name="wcat17" value="<%=wcat17%>">		

<%
' inizio costruzione pagina
%>

<br>
<a name="su">
<!--- TITOLO PAGINA ---->

<center>
<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
	<tr height="18">
		<td class="sfondomenu" width="67%" height="18"><span class="tbltext0"><b>&nbsp;MODIFICA DOCUMENTO</b></span></td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
		<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>%20/images/sfondo_linguetta.gif">(*) campi obbligatori</td>
	</tr>
</table>
<!-- Commento -->
<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
	<tr>
		<td align="left" class="sfondocomm">Utilizza questa form per 
modificare il documento <b>'<%=rstIndex("DocTitle")%>'</b>.<br> Riempi 
i campi obbligatori, e utilizza le opzioni per personalizzarlo e 
catalogarlo.  
			<a href="Javascript:Show_Help('/pgm/help/BancaDati/BDD_InsDoc')">
			<img align="right" src="/images/help.gif" border="0"></a>
		</td>
	</tr>
	<tr height="2">
		<td class="sfondocomm" width="100%" colspan="3" background="/images/separazione.gif"></td>
	</tr>
</table>


<form method="post" name="forma" onsubmit action>
<!--form method="post" name="forma" onsubmit="return invia()" action="BDD_Cnfmodifica.asp"-->

<!--- SEPARAZIONE ---->

<!--- MENU ORIZZONTALE ---->
<a name="titolo">
<table cellSpacing="0" cellPadding="0" width="500" align="center" border="0">
	<tr>
		<td height="2" align="left" colspan="2" background="/images/separazione.gif"></td>
	</tr>
	<tr>
		<td class="tblsfondo" valign="top" height="2" align="center">
			<a class="tbltext" href="#titolo"><b>Titolo</b></a> | <a class="tbltext" href="#contenuto">Contenuto</a> | <a class="tbltext" href="#categorie">Argomenti di ricerca</a>&nbsp;&nbsp;&nbsp;&nbsp;
		</td>
		<td class="tblsfondo" width="5%">&nbsp;</a></td>
	</tr>  
</table>

<!--- TITOLO ---->

<table cellSpacing="1" cellPadding="1" width="500" align="center" border="0">
	<tr>
	    <td colspan="3" class="tbltext"><strong>Titolo (*) </strong></span><span class="textblack"> - massimo 70 caratteri</span></td>
	</tr>
	<tr>
	    <td colspan="3" class="tbltext"><input class="textred" id="titolo" size="70" name="titolo" maxlength="70"></td>
	</tr>
	<tr>
	    <td width="75%" class="tbltext">Per associare al titolo un link esterno indicarne l'URL : &nbsp;</td>
	    <td width="25%"><a class="tbltext" href="javascript:viewlink('BDD_linktit.asp','linktit')">
	    <img alt src="/images/bulletagg.gif" border="0" WIDTH="70" HEIGHT="14"></a><span class="tbltext">&nbsp;</span></td>
	</tr>
	<tr height="2">
	    <td height="2" colspan="3" valign="top">
	    	<label class="textred" id="linktit" name="linktit"></label>
	    	<input type="hidden" id="linktit1" name="linktit1">
		</td>
	</tr>
	<tr>
		<td width="75%" class="tbltext">Altrimenti � possibile scaricare un file da associare : &nbsp;</td>
	    <td width="25%"><a class="tbltext" href="javascript:viewlink('BDD_alletit.asp','alletit')">
		<img alt src="/images/bulletagg.gif" border="0" WIDTH="70" HEIGHT="14"></a>
		<span class="tbltext">&nbsp;</span>
		</td>
	</tr>
	<tr>
	    <td colspan="3" valign="top">
	    	<label class="textred" id="alletit" name="alletit"></label>
	    	<input type="text" id="alletit1" name="alletit1">
		</td>
	</tr>
</table>

<!--- FONTE ---->
<table cellSpacing="1" align="center" cellPadding="1" width="500" border="0">
	<tr>
		<td class="tbltext1"><strong> Fonte (*)</strong></td>
		<td class="tbltext">
<%
		sStringa = "BDFNT|0|" & Date & "|" & widfonte & "|fonte| ORDER BY DESCRIZIONE"  
		CreateCombo(sStringa)
%>
		</td>
	</tr>
	<tr>
  <!--- REDATTORE ---->
	<tr>
		<td class="tbltext1"><strong> Redattore (*)</strong></td>
		<td><input type="text" class="textred" id="autore" name="autore" size="63"></label></td>
	</tr>
	<tr>
		<td colSpan="2"></td>
	</tr>
	<!--- DATA IMMISSIONE ---->
	<tr>
		<td class="tbltext1"><strong>Data (*)</strong></td>
		<td><input class="textred" id="dataimm" name="dataimm" size="11" maxlength="10"><span class="tbltext"> (gg/mm/aaaa)</td>
	</tr>
	<tr>
		<td colSpan="2"></td>
	</tr> 
</table>

<table cellSpacing="4" align="center" cellPadding="4" width="500" border="0">    
<!--- NOTE ---->
	<tr>
		<td width="60%" class="tbltext">E' possibile inserire delle note riguardanti il documento </td>
		<td width="30%"><a class="tbltext" href="javascript:viewlink('BDD_note.asp','note')">
		<img alt src="/images/bulletagg.gif" border="0" WIDTH="70" HEIGHT="14"></a><span class="tbltext">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="4">
			<label class="textred" id="note" name="note"></label>
			<textarea class="textred" id="note1" name="note1" rows="3" cols="95"></textarea>
		</td>
	</tr>
	<tr>	
		<td colSpan="2"><strong></strong></td>
	</tr>
</table>

<a name="contenuto">
<table cellSpacing="0" cellPadding="0" width="500" align="center" border="0">
	<tr>
		<td height="2" align="left" colspan="2" background="/images/separazione.gif"></td>
	</tr>
	<tr>
		<td class="tblsfondo" valign="top" height="2" align="center">
			<a class="tbltext" href="#titolo">Titolo</a> | <a class="tbltext" href="#contenuto"><b>Contenuto</b></a> | <a class="tbltext" href="#categorie">Argomenti di ricerca</a>&nbsp;&nbsp;&nbsp;&nbsp;
		</td>
		<td class="tblsfondo" width="5%">
			<a class="tbltext" href="#su">
			<img SRC="/images/sopra.gif" border="0" WIDTH="20" HEIGHT="20">
			</a>
		</td>
	</tr>  
</table>
<!--- CONTENUTO ---->
<table cellSpacing="4" cellPadding="4" width="500" align="center" border="0">
	<tr>
		<td class="tbltext"><strong>Abstract (*)</strong><br>
		<textarea class="textred" id="abst" name="abst" rows="3" cols="95"></textarea></td>
	</tr>
</table>

<table cellSpacing="4" cellPadding="4" width="500" align="center" border="0">
	<tr>
		<td class="tbltext"><strong>Contenuto (*)</strong><br>
		<textarea class="textred" id="contenuto" name="contenuto" rows="10" cols="95"></textarea></td>
	</tr>
</table>


<!--- APPROFONDIMENTI ---->
<table cellSpacing="4" cellPadding="4" align="center" width="500" border="0">
	<tr>
		<td class="tbltext"><strong>Approfondimenti</strong></td>
	</tr>
	<tr>
		<td class="tbltext">Indicare di seguito le URL dei siti web per approfondimenti &nbsp;
			<a class="tbltext" href="javascript:viewlink('BDD_link.asp','link1')">
			<img alt src="/images/bulletagg.gif" border="0" WIDTH="70" HEIGHT="14"></a>&nbsp;&nbsp;
		</td>
	</tr>
	<tr>
		<td valign="center">
			<label class="textred" id="link1" name="link1"></label>
			<input type="hidden" size="70" id="linkh1" name="linkh1">
			<input type="hidden" size="70" id="linkt1" name="linkt1">
			<label class="textred" id="link1" name="link1"></label>
			<input type="hidden" size="70" id="linkh2" name="linkh2">
			<input type="hidden" size="70" id="linkt2" name="linkt2">
			<label class="textred" id="link1" name="link1"></label>
			<input type="hidden" size="70" id="linkh3" name="linkh3">
			<input type="hidden" size="70" id="linkt3" name="linkt3">
			<label class="textred" id="link1" name="link1"></label>
			<input type="hidden" size="70" id="linkh4" name="linkh4">
			<input type="hidden" size="70" id="linkt4" name="linkt4">
			<label class="textred" id="link1" name="link1"></label>
			<input type="hidden" size="70" id="linkh5" name="linkh5">
			<input type="hidden" size="70" id="linkt5" name="linkt5">
			<label class="textred" id="link1" name="link1"></label>
			<input type="hidden" size="70" id="linkh6" name="linkh6">
			<input type="hidden" size="70" id="linkt6" name="linkt6">
			<label class="textred" id="link1" name="link1"></label>
			<input type="hidden" size="70" id="linkh7" name="linkh7">
			<input type="hidden" size="70" id="linkt7" name="linkt7">
			<label class="textred" id="link1" name="link1"></label>
			<input type="hidden" size="70" id="linkh8" name="linkh8">
			<input type="hidden" size="70" id="linkt8" name="linkt8">
			<label class="textred" id="link1" name="link1"></label>
			<input type="hidden" size="70" id="linkh9" name="linkh9">
			<input type="hidden" size="70" id="linkt9" name="linkt9">
			<label class="textred" id="link1" name="link1"></label>
			<input type="hidden" size="70" id="linkh10" name="linkh10">
			<input type="hidden" size="70" id="linkt10" name="linkt10">
		</td>
	</tr>
</table>

<a name="categorie">
<table cellSpacing="0" cellPadding="0" width="500" align="center" border="0">
	<tr>
		<td height="2" align="left" colspan="2" background="/images/separazione.gif"></td>
	</tr>
	<tr>
		<td class="tblsfondo" height="2" align="center">
			<a class="tbltext" href="#titolo">Titolo</a> | <a class="tbltext" href="#contenuto">Contenuto</a> | <a class="tbltext" href="#categorie"><b>Argomenti di ricerca</b></a>&nbsp;&nbsp;&nbsp;&nbsp;
		</td>
		<td class="tblsfondo" width="5%">
			<a class="tbltext" href="#su"><img SRC="/images/sopra.gif" border="0" WIDTH="20" HEIGHT="20"></a>
		</td>
	</tr>
</table>
<br>


<!-- TABELLA CON TUTTI GLI ARGOMENTI NEI QUALI E' POSSIBILE EFFETTUARE LA RICERCA -->


<!--- TUTTE LE NORMATIVE ---->
<table cellSpacing="1" cellPadding="1" width="500" align="center" border="0">
	<tr>
		<td colspan="2" class="tbltext"><strong>Argomenti di ricerca(*)</strong></td>
	</tr>
	<tr>
		<td colspan="4" class="tbltext">Di seguito selezionare i parametri 
			da associare al documento, che verranno poi utilizzati come argomenti di 
			ricerca. E' possibile indicare tutti gli argomenti, selezionando il 
			relativo check 
			<input type="checkbox" checked readonly disabled id="checkbox1" name="checkbox1"> che troverai accanto ad ogni voce, 
			oppure � possibile scegliere nel dettaglio premendo il pulsante <img alt src="/images/bulletagg.gif" border="0" WIDTH="70" HEIGHT="14">.<br>
			Gli argomenti individuati compariranno 
			automaticamente sotto la voce selezionata.
			<br><br>
		</td>
	</tr>  
	<tr>
		<td width="2%">
			<input class="tbltext" type="checkbox" id="chknorma" value="chknorma" name="chknorma" onclick="chknormatutte()"></td>
		<td width="98%" colspan="3" class="tbltext"><b>TUTTE LE NORMATIVE</b> (e relative sottocategorie)</td>
	</tr>
</table>	
<!--- FINE TUTTE LE NORMATIVE ---->



<!--- NORMATIVE ---->
<table cellSpacing="1" cellPadding="1" width="500" align="center" border="0">
<%	Dim aDescrNorm

	aDescrNorm = decodTadesToArray("BDNOR",DATE,"valore='1'",1,"0")
	nInd = 1
	If Not IsNull(aDescrNorm(0,0,0)) Then 
		For yy=0 To Ubound(aDescrNorm)-1
%> 
			<tr>
				<td width="20">&nbsp;</td>
				<td width="20">
				<input type="checkbox" id="chknorma<%=nInd%>" name="chknorma<%=nInd%>" onclick="chknorma<%=nInd%>tutte()" value="<%=Lcase(aDescrNorm(yy,yy+1,yy))%>"></td>
				<td width="360" class="tbltext"><b><%=aDescrNorm(yy,yy+1,yy)%></b>
				<td width="100" class="tbltext">
				<a class="tbltext" href="javascript:viewlink('BDD_Sottocategorie.asp','norma<%=nInd%>','<%=aDescrNorm(yy,yy+1,yy)%>','BDNRM','<%=nInd%>')">
					<img alt src="/images/bulletagg.gif" border="0" WIDTH="70" HEIGHT="14"></a></td>

			</tr>
			<tr class="textred">
				<td colspan="2"></td>
				<td colspan="4" valign="top">
			  		<label class="textred" id="norma<%=nInd%>" name="norma<%=nInd%>"></label>
			  		<input type="hidden" id="norma<%=nInd%>a" name="norma<%=nInd%>a">
			  	</td>
			</tr>
<%		
		nInd=nInd+1
		Next
	End if
	Erase aDescrNorm
%>
</table>
<!--- FINE NORMATIVE ---->

<!--- BANDI E AVVISI ---->
<table cellSpacing="1" cellPadding="1" width="500" align="center" border="0">
	<tr>
		<td width="20"><input type="checkbox" id="chkbandi1" name="chkbandi1" onclick="chkbanditutte()" value="BANDI E AVVISI"></td>
		<td width="380" class="tbltext"><b>BANDI E AVVISI</b>
		<td width="100" class="tbltext"><a class="tbltext" href="javascript:viewlink('BDD_Sottocategorie.asp','bandi1','bandi e avvisi','BDBDA','1')">
			<img alt src="/images/bulletagg.gif" border="0" WIDTH="70" HEIGHT="14"></a></td>
	</tr>
	<tr class="textred">
		<td></td>
		<td colspan="2" valign="top">
	  		<label class="textred" id="bandi1" name="bandi1&gt;"></label>
	  		<input type="hidden" id="bandi1a" name="bandi1a">
	  	</td>
	</tr>
</table>
<!--- FINE BANDI E AVVISI ---->


<!--- STRUMENTI ---->
<table cellSpacing="1" cellPadding="1" width="500" align="center" border="0">
	<tr>
		<td width="20"><input type="checkbox" id="chkstrum1" name="chkstrum1" onclick="chkstrumtutte()" value="STRUMENTI"></td>
		<td width="380" class="tbltext"><b>STRUMENTI</b>
		<td width="100" class="tbltext">
		<a class="tbltext" href="javascript:viewlink('BDD_Sottocategorie.asp','strum1','strumenti','BDSTM','1')">
		<img alt src="/images/bulletagg.gif" border="0" WIDTH="70" HEIGHT="14"></a></td>
	</tr>
	<tr class="textred">
		<td></td>
		<td colspan="2" valign="top">
	  		<label class="textred" id="strum1" name="strum1"></label>
	  		<input type="hidden" id="strum1a" name="strum1a">
	  	</td>
	</tr>
</table>
<!--- FINE STRUMENTI ---->


<!--- BENCHMARKING ---->
<table cellSpacing="1" cellPadding="1" width="500" align="center" border="0">
	<tr>
		<td width="20">
		<input type="checkbox" id="chkbench1" name="chkbench1" onclick="chkbenchtutte()" value="BENCHMARKING"></td>
		<td width="380" class="tbltext"><b>BENCHMARKING</b>
		<td width="100" class="tbltext">
		<a class="tbltext" href="javascript:viewlink('BDD_Sottocategorie.asp','bench1','benchmarking','BDBCH','1')">
		<img alt src="/images/bulletagg.gif" border="0" WIDTH="70" HEIGHT="14"></a></td>
	</tr>
	<tr class="textred">
		<td></td>
		<td colspan="2" valign="top">
	  		<label class="textred" id="bench1" name="bench1"></label>
	  		<input type="hidden" id="bench1a" name="bench1a">
	  	</td>
	</tr>
</table>
<!--- FINE BENCHMARKING ---->


<!--- MATERIALI ITALIA LAVORO ---->
<table cellSpacing="1" cellPadding="1" width="500" align="center" border="0">
	<tr>
		<td width="20"><input type="checkbox" id="chkmater1" name="chkmater1" onclick="chkmatertutte()" value="MATERIALI ITALIA LAVORO"></td>
		<td width="380" class="tbltext"><b>MATERIALI ITALIA LAVORO</b>
		<td width="100" class="tbltext"><a class="tbltext" href="javascript:viewlink('BDD_Sottocategorie.asp','mater1','materiali italia lavoro','BDMTR','1')">
		<img alt src="/images/bulletagg.gif" border="0" WIDTH="70" HEIGHT="14"></a></td>

	</tr>
	<tr class="textred">
		<td></td>
		<td colspan="2" valign="top">
	  		<label class="textred" id="mater1" name="mater1"></label>
	  		<input type="hidden" id="mater1a" name="mater1a">
	  	</td>
	</tr>
</table>
<!--- FINE MATERIALI ITALIA LAVORO ---->


<!--- TUTTE LE DOCUMENTAZIONI ---->
<table cellSpacing="1" cellPadding="1" width="500" align="center" border="0">
	<tr>
		<td width="2%"><input class="tbltext" type="checkbox" id="chkdocum" value="chkdocum" name="chkdocum" onclick="chkdocumtutte()"></td>
		<td width="98%" colspan="3" class="tbltext"><b>TUTTE LE DOCUMENTAZIONI</b> (e relative sottocategorie)</td>
	</tr>
</table>	
<!--- FINE TUTTE LE DOCUMENTAZIONI ---->



<!--- DOCUMENTAZIONE ---->
<table cellSpacing="1" cellPadding="1" width="500" align="center" border="0">
<%	Dim aDescrDocum

	aDescrDocum = decodTadesToArray("BDDOC",DATE,"valore='1'",1,"0")
	nInd = 1
	If Not IsNull(aDescrDocum(0,0,0)) Then 
		For yy=0 To Ubound(aDescrDocum)-1
%> 
			<tr>
				<td width="20">&nbsp;</td>
				<td width="20">
				<input type="checkbox" id="chkdocum<%=nInd%>" name="chkdocum<%=nInd%>" onclick="chkdocum<%=nInd%>tutte()" value="<%=Lcase(aDescrDocum(yy,yy+1,yy))%>"></td>
				<td width="360" class="tbltext"><b><%=aDescrDocum(yy,yy+1,yy)%></b>
				<td width="100" class="tbltext"><a class="tbltext" href="javascript:viewlink('BDD_Sottocategorie.asp','docum<%=nInd%>','<%=Lcase(aDescrDocum(yy,yy+1,yy))%>','BDDCM','<%=nInd%>')">
				<img alt src="/images/bulletagg.gif" border="0" WIDTH="70" HEIGHT="14"></a></td>
			</tr>
			<tr class="textred">
				<td colspan="2"></td>
				<td colspan="2" valign="top">
			  		<label class="textred" id="docum<%=nInd%>" name="docum<%=nInd%>"></label>
			  		<input type="hidden" id="docum<%=nInd%>a" name="docum<%=nInd%>a">
			  	</td>
			</tr>
<%		
		nInd=nInd+1
		Next
	End if
	Erase aDescrDocum
%>
</table>
<!--- FINE DOCUMENTAZIONE ---->

<!--- SCHEDE E RECENSIONI ---->
<table cellSpacing="1" cellPadding="1" width="500" align="center" border="0">
	<tr>
		<td width="2%"><input class="tbltext" type="checkbox" id="chkschede1" value="chkschede1" name="chkschede1" onclick="chkschedetutte()"></td>
		<td width="98%" colspan="3" class="tbltext"><b>SCHEDE E 
RECENSIONI</b></td>
	</tr>
	<tr class="textred">
		<td></td>
		<td colspan="2" valign="top">
	  		<label class="textred" id="schede1" name="schede1"></label>
	  		<input type="hidden" id="schede1a" name="schede1a">
	  	</td>
	</tr>	
</table>	
<!--- FINE SCHEDE E RECENSIONI ---->


<!--- SETTORE ---->
<table cellSpacing="1" cellPadding="1" width="500" align="center" border="0">
	<tr>
		<td width="20"><input type="checkbox" id="chksettore1" name="chksettore1" onclick="chksetttutte()" value="SETTORE"></td>
		<td width="380" class="tbltext"><b>SETTORE</b>
		<td width="100" class="tbltext">
		<a class="tbltext" href="javascript:viewlink('BDD_Sottocategorie.asp','settore1','settore','BDSTT','1')">
		<img alt src="/images/bulletagg.gif" border="0" WIDTH="70" HEIGHT="14"></a></td>
	</tr>
	<tr class="textred">
		<td></td>
		<td colspan="2" valign="top">
	  		<label class="textred" id="settore1" name="settore1"></label>
	  		<input type="hidden" id="settore1a" name="settore1a">
	  	</td>
	</tr>
</table>
<!--- FINE SETTORE ---->

<!--- AZIONI ---->
<table cellSpacing="1" cellPadding="1" width="500" align="center" border="0">
	<tr>
		<td width="20"><input type="checkbox" id="chkazioni1" name="chkazioni1" onclick="chkazionitutte()" value="AZIONI"></td>
		<td width="380" class="tbltext"><b>AZIONI</b>
		<td width="100" class="tbltext">
		<a class="tbltext" href="javascript:viewlink('BDD_Sottocategorie.asp','azioni1','azioni','BDAZN','1')">
		<img alt src="/images/bulletagg.gif" border="0" WIDTH="70" HEIGHT="14"></a></td>
	</tr>
	<tr class="textred">
		<td></td>
		<td colspan="2" valign="top">
	  		<label class="textred" id="azioni1" name="azioni1"></label>
	  		<input type="hidden" id="azioni1a" name="azioni1a">
	  	</td>
	</tr>
</table>
<!--- FINE AZIONI ---->

<!--- TERRITORIO ---->

<table cellSpacing="1" cellPadding="1" width="500" align="center" border="0">
	<tr>
		<td width="20"><input type="checkbox" id="chkarea1" name="chkarea1" onclick="chkareatutte()" value="AREA"></td>
		<td width="380" class="tbltext"><b>TERRITORIO</b>
		<td width="100" class="tbltext"><a class="tbltext" href="javascript:viewlink('BDD_Territori.asp','area1','territorio','BDARE','1')"><img alt src="/images/bulletagg.gif" border="0" WIDTH="70" HEIGHT="14"></a></td>
	</tr>
	<tr class="textred">
		<td></td>
		<td colspan="2" valign="top">
	  		<label class="textred" id="area1" name="area1"></label>
	  		<input type="hidden" id="area1a" name="area1a">
	  	</td>
	</tr>
</table>
	
<!--- UNIONE EUROPEA ---->
<table cellSpacing="1" cellPadding="1" width="500" align="center" border="0">
	<tr>
		<td width="20">&nbsp;</td>
		<td width="20"><input type="checkbox" id="chkeuro1" name="chkeuro1" disabled onclick="chkeurotutte()" value="UNIONE EUROPEA"></td>
		<td width="360" class="tbltext"><b>UNIONE EUROPEA</b>
		<td width="100" class="tbltext">
		<a class="tbltext" href="javascript:viewlink('BDD_Sottocategorie.asp','euro1','unione europea','BDEUR','1')"><img alt src="/images/bulletagg.gif" border="0" id="imgeur" WIDTH="70" HEIGHT="14"></a></td>
	</tr>
	<tr class="textred">
		<td colspan="2"></td>
		<td colspan="2" valign="top">
	  		<label class="textred" id="euro1" name="euro1"></label>
	  		<input type="hidden" id="euro1a" name="euro1a">
	  	</td>
	</tr>
<!--- REGIONE ---->
	<tr>
		<td width="20">&nbsp;</td>
		<td width="20">
		<input type="checkbox" id="chkregio1" name="chkregio1" disabled onclick="chkregiotutte()" value="REGIONE"></td>
		<td width="360" class="tbltext"><b>REGIONI ITALIANE</b>
		<td width="100" class="tbltext"><a class="tbltext" href="javascript:viewlink('BDD_Regione.asp','regio1','regione','REGIO','')">
<img alt src="/images/bulletagg.gif" border="0" id="imgreg" WIDTH="70" HEIGHT="14"></a></td>
	</tr>
	<tr class="textred">
		<td colspan="2"></td>
		<td colspan="2" valign="top">
	  		<label class="textred" id="regio1" name="regio1"></label>
	  		<input type="hidden" id="regio1a" name="regio1a">
	  	</td>
	</tr>
<!--- PROVINCIA ---->
	<tr>
		<td width="20">&nbsp;</td>
		<td width="20">
		<input type="checkbox" id="chkprov1" name="chkprov1" disabled onclick="chkprovtutte()" value="PROVINCIA"></td>
		<td width="360" class="tbltext"><b>PROVINCE ITALIANE</b>
		<td width="100" class="tbltext"><a class="tbltext" href="javascript:viewlink('BDD_Provincia.asp','regio1','regione','PROV','')">
		<img alt src="/images/bulletagg.gif" border="0" id="imgprov" WIDTH="70" HEIGHT="14"></a></td>
	</tr>
	<tr class="textred">
		<td colspan="2"></td>
		<td colspan="2" valign="top">
	  		<label class="textred" id="prov1" name="prov1"></label>
	  		<input type="hidden" id="prov1a" name="prov1a">
	  	</td>
	</tr>
</table> 


<!--- SEPARAZIONE ---->
<table cellSpacing="4" cellPadding="4" width="500" align="center" border="0">
	<tr>
		<td colspan="4">&nbsp;</td>
	</tr> 
	<tr>
		<td height="2" align="left" colspan="4" background="/images/separazione.gif"></td>
	</tr>
  </tbody>
</table>
<!--- PULSANTE INVIA ---->
<br>
<input type="hidden" name="titfile" value="<%=nomefile%>"> 
<table cellSpacing="4" cellPadding="4" width="500" align="center" border="0">
	<tr>
	<!--Inizio am-->
		<td align="middle" colspan="3">
			<a href="javascript:Salva()"> 
			<img src="/images/conferma.gif" border="0" WIDTH="55" HEIGHT="40">
		</a>	
			<!--input type="image" id="IMG1" src="/images/conferma.gif" border="0" WIDTH="55" HEIGHT="40"-->
		</td>
		
		<td align="middle" colspan="3">
			<a href="javascript:anteprima()">
				<img src="/images/visualizza.gif" border="0">
			</a>
		</td>
	<!--Fine Am-->
	</tr>  
</table>
<br>
<table cellSpacing="4" cellPadding="4" width="500" align="center" border="0">
	<tr>
		<td align="middle"><a href="javascript:history.back()" class="textred"><b>Torna indietro</b></a></td>
		<td align="middle">
		<a href="BDD_Ricerca.asp" class="textred"><b>Ricerca documento</b></a></td>
		<% if mid(Session("mask"),3,1) = "0"  then%>
		<td align="middle"><a href="BDD_InsDoc.asp" class="textred"><b>Inserimento nuovo documento</b></a></td>
		<%end if %>	

	</tr>  
</table>
<br>
</form>

<script language="javascript">
	inizio()
</script>

<!-- #include virtual="/include/closeconn.asp" -->
<!--#include Virtual = "/strutt_coda2.asp"-->
