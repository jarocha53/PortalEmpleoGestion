<%@ Language=VBScript %>
<html>
<head>
<meta NAME="GENERATOR" Content="Microsoft Visual Studio 6.0">
<link REL="STYLESHEET" TYPE="text/css" HREF="<%=session("progetto")%>/fogliostile.css">

</head>
<body>
<table border="0" width="510" cellspacing="0" cellpadding="0" height="30">
	<tr>
		<td width="530" background="<%=Session("Progetto")%>/images/titoli/barracerca.jpg" valign="bottom" align="right">
			<table border="0" width="360" height="30" cellspacing="0" cellpadding="0">
				<tr>
					<td width="100%" valign="middle" align="right"><b class="tbltext1a">Sugerencias para la b�squeda simple&nbsp;&nbsp;</span></b></td>
				</tr>
			</table>
		</td>
	</tr>
</table><br>

</table><br><table border="0" CELLPADDING="0" CELLSPACING="1" width="500">
	<tr>
		<td class="tbltexta"><a name="help"><b>Sugerencias para la b�squeda por palabra</b><br><br></td>
	</tr>
<tr><td class="tbltext">

El intento de B�squeda por palabra delimita el campo de b�squeda de los documentos en los cuales aparecen ambos t�rminos unidos a trav�s del operador 'AND'.<br>
Procedimiento: separe los t�rminos con un espacio.<br>
Ejemplo: si escribe &quot;asilo de nido&quot; obtendr� la lista de todos los documentos que contengan <b>sia</b> la palabra asilo <b>sia</b> la palabra nido.

<br><br>
</td>
	</tr>

	<tr>
		<td class="tbltexta"><a name="help"><b>Selecci�n de las categorias</b><br><br></td>
	</tr>
	<tr>
		<td class="tbltext">La B�squeda por categoria es un instrumento muy �til para efectuar b�squedas limitadas sobre un argumento espec�fico. 
		Por ejemplo, para visualizar solo los resultados relativos a la Categoria Normativa, es suficiente ingresar el t�rmino a buscar 
		y seleccionar solo &quot;Normativa&quot; en la lista . En este modo si evita que se visualicen resultados relativos a las
		 otras categorias, al juego hom�nimo u otros resultados que puedan ser correlativos al t�rmino elegido. <br>La b�squeda al interior
		  de categorias especificas permiten restringir el campo de b�squeda y visualizar rapidamente solo las p�ginas deseadas. 
<br>NB: Esta acoplado a la b�squeda por palabra<br></td>
	</tr>
</table>
</table><br><table border="0" CELLPADDING="0" CELLSPACING="1" width="500">
<tr><td align="center"><a HREF="javascript:window.close()"><img SRC="<%=Session("Progetto")%>/Images/chiudi.gif" border="0"></a></td></tr>
</table>
</body>
</html>
