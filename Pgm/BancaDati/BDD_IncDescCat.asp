<!-- TABELLA CON LA DESCRIZIONE DI TUTTE GLI ARGOMENTI  -->

<table border="0" CELLPADDING="0" CELLSPACING="1" width="500">
	<tr>
     <td height="2" align="left" background="/images/separazione.gif"></td>
	</tr>
	<tr>
     <td height="2" align="left">&nbsp;</td>
	</tr>
	<tr>
		<td class="tbltexta"><b> Informaci�n relativa al area de b�squeda</b><br><br></td>
	</tr>
	<tr>
		<td class="tbltext1"><b>Normativa</b></td>
	</tr>
	<tr>
		<td class="tbltext"><i>Son propuestas las leyes, los decretos legislativos, los decretos de leyes, los decretos ministeriales y directoriales, los circulares, etc., que resguardan en primer lugar las pol�ticas ocupacionales y del trabajo exterior y nacional, pero tambi�n la legislaci�n de cada sector, por ejemplo Tercer sector, ambiente, inmigraci�n, etc. de los cuales son claras las recaidas ocupacionales.</i><br><br></td>
	</tr>
	<tr>
		<td class="tbltext1"><b>Documentaci�n</b></td>
	</tr>
	<tr>
		<td class="tbltext"><i>Son propuestos los principales documentos de programaci�n nacional, regional y exterior que definen el cuadro conjuntivo de las pol�ticas para la &#146;ocupaci�n y la &#146;inclusi�n social.</i><br><br></td>
	</tr>
	<tr>
		<td class="tbltext1"><b>Bandi y Avisos</b></td>
	</tr>
	<tr>
		<td class="tbltext"><i>Abastece un cuadro de las oportunidades en el exterior, nacionales y regionales para la presentaci�n y el financiamiento de acciones en favor de las&#146;ocupaciones</i><br><br></td>
	</tr>
	<tr>
		<td class="tbltext1"><b>Reportes de monitoreo y estad�sticas</b></td>
	</tr>
	<tr>
		<td class="tbltext"><i>Reportes, de diversa autoridad, tras los cuales: MLPS; ISFOL; Banco de&#146;Italia; etc. abastecen una rendici�n del estado del&#146;arte de las pol�ticas y de la realidad monitoreada. El cuadro informativo y espeso proveido de investigaciones tem�ticas y de alegatos est�ticos. Los datos estadisticos, en primer lugar del&#146;ISTAT, constituyen en otro objeto de informaciones a causa de la misma.</i><br><br></td>
	</tr>
	<tr>
		<td class="tbltext1"><b>Mejores pr�cticas</b></td>
	</tr>
	<tr>
		<td class="tbltext"><i>Viene adjunto dato de las buenas pr�cticas como ejemplo de la excelencia de compartir. La&#146;utilidad referida a las buenas pr�cticas son presentes en numerosos documentos del exterior, en documentos argentinos que, como por ejemplo en el Plano Nacional para la&#146;Ocupaci�n 2002, dan cuenta.</i><br><br></td>
	</tr>
	<tr>
		<td class="tbltext1"><b>Benchmark</b></td>
	</tr>
	<tr>
		<td class="tbltext"><i>Consiste en la recolecci�n y comparaci�n de indicadores y de las mejores pr�cticas relativas a las pol�ticas de trabajo en sudam�rica.</i><br><br></td>
	</tr>
	<tr>
		<td class="tbltext1"><b>Proyectos de ItaliaLavoro S.p.A.</b></td>
	</tr>
	<tr>
		<td class="tbltext"><i>Vienen presentados los m�s significativos proyectos pr�ximos y administrados por Italia Lavoro S.p.A.</i><br><br></td>
	</tr>
	<tr>
     <td height="2" align="left" background="/images/separazione.gif"></td>
	</tr>
</table>
