<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include virtual ="/strutt_testa2.asp"-->
<!--#include virtual = "/Include/ControlDateVB.asp"-->
<!--#include virtual = "/Include/OpenConn.asp"-->
<!--#include virtual = "/Util/DBUtil.asp"-->
<!--#include virtual ="/include/ckProfile.asp"-->
<!--#include virtual = "/util/portallib.asp"-->
<%
if ValidateService(session("idutente"),"TAB_ELENCOTABELLE",cc) <> "true" then 
	response.redirect "/util/error_login.asp"
end if
%>


<script LANGUAGE="javascript">

// Include del file per i controlli sulla 
// validit� delle datedi tipo JavaScript
<!--#include virtual = "/Include/ControlDate.inc"-->	
<!--#include virtual = "/Include/ControlNum.inc"-->
<!--#include Virtual = "/Include/help.inc"-->	

/*
	La funzione RICERCA permette di ricaricare i codici con una nuova 
	data di validit� in forma tabellare contenuti nel frame ElencoCodici.Tab.
*/
	function ricerca(){	
		if (ControllaData()){
			return true
		}
		else {
			return false
		}
	}

function Invia(sIsa,sTabella,sProfilo)
	{
		ciccio = "TAB_VisCodici.asp?Profilo=" + 
				sProfilo + "&isa=" + 
				sIsa + "&NomeTab=" + 
				sTabella + "&Data=" + 
				ElencoCodici.txtData.value
			document.location.href = ciccio
	}	
/*
	La funzione ControllaData esegue dei controlli sulle esatezza 
	della data indicata dall'utente nel campo txtData per un nuovo
	filtro.
*/

	function ControllaData(){	
		DataRicerca = ElencoCodici.txtData.value 

		// Verifica che sia stata indicata la DATA DI RICERCA
		// valore obbligatorio.
		if (DataRicerca == ""){
			alert("Data di Ricerca obbligatoria")
			ElencoCodici.txtData.focus() 
			return false
		}

		// Richiamo la funzione ValidateInputDate per 
		// verificare la validit� della data indicata
		if (!ValidateInputDate(DataRicerca)) {
			ElencoCodici.txtData.focus() 
			return false
		}

		return true
	}
	
	// Posizione il focus sul Bottone 
	function posiziona()	{
		document.forms[0].button1.focus();
	}
</script>
<table border="0" width="520" cellspacing="0" cellpadding="0" height="81">
   <tr>
     <td width="500" background="<%=Session("Progetto")%>/images/titoli/strumenti2b.gif" height="81" valign="bottom" align="right">
       <table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
         <tr>
           <td width="100%" valign="top" align="right"><b class="tbltext1a">Gestione Codici Tabelle</span></b></td>
         </tr>
       </table>
     </td>
   </tr>
</table><br>

<p align="center">
<strong>
<%	
on error resume next 

dim rsTabelle	
dim sSQL		
dim sParamRic

dim sTabella
dim sIsa
dim sData	
dim sProfilo

'if mid(Session("UserProfiles"),1,1) = "0" then
'	sIsa = "0"
'else
'	sIsa = mid(Session("UserProfiles"),1,2)
'end if

'sProfilo = mid(Session("UserProfiles"),3,1)
	
if mid(Session("mask"),1,1) = "0" then
	sIsa = "0"
else
	sIsa = mid(Session("mask"),1,2)
end if
sProfilo = mid(Session("mask"),3,2)
					
sTabella = Request ("NomeTab")
sData = Request("Data")

if sData = "" then
	sData = ConvDateToString(Date())
end if
			
sSQL = "SELECT Nome_Tabella, Descrizione, Decorrenza, Scadenza, Lunghezza_Codice, Caratteristiche, Lunghezza_desc, Aut_Supporto, Aut_funzionale, Aut_Decisionale, Attributi FROM IDTAB WHERE Nome_Tabella = '" & sTabella & "'"
			
' Apro il RecordSet
set rsTabelle = Server.CreateObject("ADODB.Recordset")
			
' Eseguo la istruzione SQL
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
rsTabelle.Open sSQL, CC, 3
				
' Stampo la Descrizione della tabella in testa dopo il titolo
value = rsTabelle("Descrizione")
			
' Converto valori data in formato stringa
DecorrenzaT = ConvDateToString(rsTabelle("Decorrenza"))
ScadenzaT = ConvDateToString(rsTabelle("Scadenza"))
							
' Chiudo la connessione al DB ed il RecordSet, 
' rilascio l'oggetto CnConn e rsTabella
rsTabelle.Close
set rsTabelle = nothing

' Indico nella variabile valori i parametri da passare
' per la funzione ricerca
sParamRic = "'" & sIsa & "','" & sTabella & "','" & sProfilo & "'"
'Response.Write sParamRic

%>
<form name="ElencoCodici" action="javascript:Invia(<%=sParamRic%>)" onsubmit="Javascript:return ricerca()">
<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
<tr height="17">
<!--	<td class="sfondomenu" width="67%" height="18"><span class="tbltext0">		&nbsp;<B>TABELLA <%=sTabella%> ( <%=value%> )</b>	</td>-->	
	<td class="sfondomenu" width="67%" height="18"><span class="tbltext0">
		&nbsp;<b>VISUALIZZAZIONE ELENCO CODICI</b>
	</td>

	<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">
	</td>
	<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">(*) campi obbligatori</td>
</tr>
<tr>
	<td class="sfondocomm" width="57%" colspan="3">
		<br>
		Selezionare il codice da modificare o scegliere <b>Inserisci Nuovo Codice</b> per crearne uno nuovo. 
		<!--<IMG align=right src="<%=Session("Progetto")%>/images/help.gif" border=0>-->
		<a href="Javascript:Show_Help('../Help/Tab_VisCodici.htm')">
	<img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
	</td>
</tr>
<tr height="2">
	<td class="sfondocomm" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif">
	</td>
</tr>
</table>
<br>

<table cellspacing="0" cellpadding="0" align="center" width="500" border="0">
	<tr>
	<td height="15" align="center" valign="middle" class="tbltext3">
	<b>
<%	
'	Response.Write ("Tabella valida dal ")
'	Response.Write DecorrenzaT
'	if ScadenzaT = "31/12/9999" then
'		Response.Write "."
'	else
'		response.write " al " & ScadenzaT & "."
'	end if
%>
<table border="0" cellpadding="2" cellspacing="2" width="500">
   <tr>
		<td align="left" colspan="2" nowrap class="tbltext1" width="20">
				<strong>Tabella:</strong></span>
		</td>
		<td align="left" colspan="2" width="500" class="textblack">
<%
			Response.Write sTabella
%>		
		</b></td>
    </tr>
   <tr>
		<td align="left" colspan="2" nowrap class="tbltext1" width="20">
				<strong>Descrizione:</strong></span>
		</td>
		<td align="left" colspan="2" width="500" class="textblack">
<%
			Response.Write value
%>		
		<b></td>
    </tr>
   <tr>
		<td align="left" colspan="2" nowrap class="tbltext1">
				<strong>Valida dal</strong></span>
		</td>
		<td align="left" colspan="2" width="480" class="textblack">
<%
			Response.Write DecorrenzaT
%>		
		</b></td>
    
    <% if ScadenzaT <> "31/12/9999" then
		%>
			<td></td>
			<td align="left" colspan="2" nowrap class="tbltext1" width="20">
				<strong>al</strong></span>
			</td>
			<td align="left" colspan="2" width="500" class="textblack">
		<%
			Response.Write ScadenzaT
		%>		
			</b></td></tr>
		
	<% END IF %>

</table><br>
<table border="0" cellpadding="2" cellspacing="2" width="500">
    <tr>
		<td height="2" align="left" colspan="4" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
    </tr>    
</table>

</b>
</td>
</tr>
</table>
<table border="0">
<tr>
	<td>
		<p align="center" class="tbltext1">
		<b>Elenco dei codici validi alla data del :&nbsp;&nbsp;&nbsp;&nbsp;
		<input id="txtData" name="txtData" style="HEIGHT: 22px; WIDTH: 71px" maxlength="10" value="<%=sData%>">
		&nbsp;

	</p>
	</td>
	<td>
		<br>
		<input type="image" src="<%=session("progetto")%>/images/lente.gif" border="0">
	</td>
</tr>
</table>
<table cellspacing="1" cellpadding="1" align="center" width="500" border="0">
<%
'	Se il profilo � 0 l'utente pu� solo visualizzare 
'	 e non inserire nuovi codici.
'	if (sProfilo <> "0") then
	if (ConvStringToDate(ScadenzaT) > date()) and (ckProfile(sProfilo,2)=true) then
%>
		<tr>
			<td width="250" align="center"> <a class="textRed" HREF="TAB_Scheda.asp?NomeTab=<%=sTabella%>&amp;Action=INS">
				<b>Inserisci Nuovo Codice</b></a>
			</td>
			<td width="250" align="center">
				<a class="textRed" HREF="TAB_ElencoTabelle.asp"><b>Torna all'Elenco delle Tabelle</b></a>
			</td>
		</tr>
<%
	else
%>
		<tr>
			<td width="500" align="center">
				<a class="textRed" HREF="TAB_ElencoTabelle.asp"><b>Torna all'Elenco delle Tabelle</b></a>
			</td>
		</tr>		
<%
	end if
%>
</table>
</form>

<%' ****************** fine supersiore *****************%>
<!--FORM name=ElencoCodiciTab action='TAB_VisCodici.asp' method='post'-->
<%

dim cont
dim pagina
dim numero

'	if mid(Session("UserProfiles"),1,1) = "0" then
'		sIsa = "0"
'	else
'		sIsa = mid(Session("UserProfiles"),1,2)
'	end if

'	sProfilo = mid(Session("UserProfiles"),3,1)
	'sIsa = "0"

	' Se ISA = 0 non occorre filtrare la query SQL con la condizione ISA.
'	if sIsa = "0" then
'		sSQL = "SELECT ISA, Nome_tabella, Codice, Decorrenza, Scadenza, Descrizione  FROM TADES WHERE Nome_Tabella = '" & _
'				sTabella & "' AND " & ConvDateToDBs(ConvStringToDate(sData)) & _
'				" BETWEEN Decorrenza AND Scadenza ORDER BY Codice"
'	else
		sSQL = "SELECT ISA, Nome_tabella, Codice, Decorrenza, Scadenza, Descrizione  FROM TADES WHERE Nome_Tabella = '" & _
				sTabella & "' AND ISA = '" & sIsa & _
				"' AND " & ConvDateToDBs(ConvStringToDate(sData)) & _
				" BETWEEN Decorrenza AND Scadenza ORDER BY Codice"
'	end if

	' Creo l'oggetto RecordSet
	set rsCodici = Server.CreateObject("ADODB.Recordset")
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	rsCodici.Open sSQL, cc, 3

	Record = 0
 
Dim nActPagina
Dim nTotPagina
Dim nTamPagina		
Dim nTotRecord

nTamPagina=10

If Request("pagina")= "" Then
	nActPagina=1
Else
	nActPagina=Clng(Request("pagina"))
End If

 rsCodici.PageSize = nTamPagina
 rsCodici.CacheSize = nTamPagina

 nTotPagina = rsCodici.PageCount

 If nActPagina < 1 Then
	nActPagina = 1
 End If
 If nActPagina > nTotPagina Then
	nActPagina = nTotPagina
 End If

 rsCodici.AbsolutePage=nActPagina
 nTotRecord=0
 
  While rsCodici.EOF <> True And nTotRecord < nTamPagina
  	
		if 	Record = 0 then%>
			<table width="500" cellspacing="0" cellpadding="0" border="0" align="center">
				<tr class="sfondocomm" align="middle" width="500">
					<td width="80" align="center"><b>Codice</b></td>
					<td width="220" align="center"><b>Descrizione</b></td>
					<td width="100" align="center"><b>Decorrenza</b></td>
					<td width="100" align="center"><b>Scadenza</b></td>
				</tr>
			</table>
			<%			
			' Se esite almeno un record valorizzo con 1 record.
			record = 1  
		end if
	
			%>
			<table cellspacing="1" cellpadding="1" align="center" BORDER="0" WIDTH="500">

				<tr class="tblsfondo">
					<td class="tbltext1" align="left" width="80">
					
					<%
					'Stampo il codice
					value = rsCodici("Codice")
					sIsaCodice = rsCodici("ISA")

					' Se ISA = 0 non occorre passarla alla pagina succesiva.
					if sIsa = "0" then
						Response.write "<A HREF='TAB_Scheda.asp?IsaCodice=" & sIsaCodice & "&NomeTab=" & sTabella & _
							"&Codice=" & value & "&Action=MOD&Data=" & _
							sData & "' class='tbltext1'>" & _
							value & "</A>"
					else 
						if rsCodici("ISA") = "0" then
							Response.write value
						else				
							Response.write "<A HREF='TAB_Scheda.asp?IsaCodice=" & sIsaCodice & _
								"&NomeTab=" & sTabella & _
								"&Codice=" & value & "&Action=MOD&Data=" & _
								sData & "' class='tbltext1' >" & _
								value & "</A>"
						end if
					end if
				%>
				
			</td>
			<td class="tbltext1" align="left" width="220">
				<%
				    'Stampo la descrizione
					value = rsCodici("Descrizione")
					Response.write value
				%>
			</td>
			<td class="tbltext1" align="left" width="100">
				<%
					
   					' Converto valori data in formato stringa
					value = ConvDateToString(rsCodici("Decorrenza"))

					if value = "01/01/1900" then
						value= "<FONT face='Verdana, Arial, Helvetica, sans-serif' size=2 color='#f1f3f3'>&nbsp;</FONT>"
					end if

				    'Stampo la descrizione
					Response.write value
				%>
			</td>
			<td class="tbltext1" align="left" width="100">
				<%
   					' Converto valori data in formato stringa
					value = ConvDateToString(rsCodici("Scadenza"))

					if value = "31/12/9999" then
						value= "<FONT face='Verdana, Arial, Helvetica, sans-serif' size=2 color='#f1f3f3'>&nbsp;</FONT>"
					end if

				    'Stampo la scadenza
					Response.write value
				%>
        
			</td>
			
		</tr>
	 <%

			nTotRecord=nTotRecord + 1	
			rsCodici.MoveNext
  Wend
	rsCodici.Close
	Set rsCodici = Nothing		

	Response.Write "<TABLE border=0 width=470>"
	Response.Write "<tr>"
	
	if nActPagina > 1 then
		Response.Write "<td align=right width=480>"
		Response.Write "<A HREF='TAB_VisCodici.asp?pagina="& nActPagina-1 & "&Data="& sData & "&NomeTab=" & sTabella & "'>"
		Response.Write "<img border=0 src=" & Session("Progetto") & "/images/precedente.gif></A></td>"
	end if
	
	if nActPagina < nTotPagina then
		Response.Write "<td align=right>"
		Response.Write "<A HREF='TAB_VisCodici.asp?pagina="& nActPagina+1 & "&Data="& sData & "&NomeTab=" & sTabella & "'>"
		Response.Write "<img border=0 src=" & Session("Progetto") & "/images/successivo.gif></A></td>"
	end if

	Response.Write "</tr></TABLE>"
%>
<br>
<!--<A class=textred HREF='javascript:onclick=history.back()'><img src="<% session("Progetto") %>/Plavoro/images/indietro.gif" border=0></A>-->
<!--/FORM-->	
</body>
</html> 
<!--#include virtual = "/Include/CloseConn.asp"-->
<!--#include virtual = "/strutt_coda2.asp"-->


