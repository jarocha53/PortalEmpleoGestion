<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include virtual ="/strutt_testa2.asp"-->
<!--#include virtual = "/Include/ControlDateVB.asp"-->
<!--#include virtual = "/Include/OpenConn.asp"-->
<!--#include virtual = "/util/DbUtil.asp"-->
<!--#include Virtual = "/include/SysFunction.asp"-->


<script LANGUAGE="javascript">
//include del file per i controlli sulla validit� delle date
<!--#include virtual = "/Include/ControlDate.inc"-->
	
	//Questa funzione serve per tornare alla pagina precedente
	//con i dati che erano stati digitati dall'utente
	function Indietro()
	{
		history.back();
	}
</script>
<%
'*************  MAIN ********************************
	
	dim sTabella
	dim sIsa
	dim sAction
	dim sCodice
	dim sScadenza
	dim sDecorrenza
	dim sDescrizione
	dim sAttributi
	dim sProfilo
	dim CnConn
	dim rsTabelle
	dim sSQL
	dim Contatore
	dim sUrl
	dim sData
	dim sIsaCodice
	dim sScadenzaCodice
	dim sDecorrenzaCodice
	
	'sIsa = request ("IsaCodice")

	'if mid(Session("UserProfiles"),1,1) = "0" then
	'	sIsa = "0"
	'else
	'	sIsa = mid(Session("UserProfiles"),1,2)
	'end if
	'sProfilo = mid(Session("UserProfiles"),3,1)
	
	if mid(Session("mask"),1,1) = "0" then
		sIsa = "0"
	else
		sIsa = mid(Session("mask"),1,2)
	end if
	'sProfilo = mid(Session("mask"),3,2)
	
	'Response.Write ("IsaC " & sIsaCodice & "<BR>")
	'Response.Write ("Isa  " & sIsa & "<BR>")

	' ********* Per debug **********
	'sIsa = 0
	' ******************************
	
	sData = request ("Data")
	sTabella = request ("Tabella")
	sAction = request ("Action")
	sCodice = request ("Codice")
	sScadenza = ConvStringToDate(request ("Scadenza"))
	sDecorrenza = ConvStringToDate(request ("Decorrenza"))
	sDecorrenzaOld=ConvStringToDate(request ("DecorrenzaOld"))
	sDescrizione = Replace(request ("Descrizione"),"'","''")
	sAttributi = request ("Attributi")
	sScadenzaCodice = ConvStringToDate(request ("ScadenzaCodice"))
	sDecorrenzaCodice = ConvStringToDate(request ("DecorrenzaCodice"))
	
	sCodice=UCase(sCodice)
	sDescrizione = UCase(sDescrizione)
	sAttributi=Trim(UCase(sAttributi))
	
	Inizio()
	ImpostaPag()
	
'****************************************************
'***************************************************************************************************************************************************
	Sub Inizio()
%>
		<table border="0" width="520" cellspacing="0" cellpadding="0" height="81">
		   <tr>
		     <td width="500" background="<%=Session("Progetto")%>/images/titoli/strumenti2b.gif" height="81" valign="bottom" align="right">
		       <table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
		         <tr>
		           <td width="100%" valign="top" align="right"><b class="tbltext1a">Gestione Codici Tabelle</span></b></td>
		         </tr>
		       </table>
		     </td>
		   </tr>
		</table>
		<br><br>
		
<%	End Sub	
'***************************************************************************************************************************************************
	Sub Indietro()
%>
		<br><br>
		<table cellspacing="2" cellpadding="1" border="0" align="center" width="500">
			<tr ALIGN="CENTER">
				<td class="tbltext3">
					<a class="textred" HREF="javascript:Indietro()" onmouseover="javascript: window.status = ' '; return true;"><img src="<%=Session("progetto")%>/images/Indietro.gif" title="Pagina precedente" border="0" align="absBottom"></a>
				</td>
			<tr>
		</table>
		<br><br>
		
<%	End Sub	
'***************************************************************************************************************************************************
	Sub ImpostaPag()	

'	on error resume next 
	Contatore = 0
	
	if (sAction <> "Can") then	
		'Controllo se la Validit� del CODICE che sto Inserendo/Modificando
		'� compatibile con la Validit� della TABELLA
		sSQL = "SELECT COUNT(*) AS Contatore FROM IDTAB WHERE Nome_Tabella = '" & _
			sTabella & "' AND " & ConvDateToDbS(sDecorrenza) & " >= Decorrenza AND " & _
			ConvDateToDbS(sDecorrenza) & " <= Scadenza AND " & ConvDateToDbS(sScadenza) & _
			" >= Decorrenza AND " & ConvDateToDbS(sScadenza) & " <= Scadenza"

		set rsTabelle = Server.CreateObject("ADODB.Recordset")
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
		rsTabelle.Open sSQL, CC,3
	
		if rsTabelle.Eof then
			Messaggio("Pagina momentaneamente no disponible")
			Indietro()
			exit sub
		end if
	
		sContatore = clng(rsTabelle("Contatore"))
		rsTabelle.Close
		set rsTabelle = nothing
	end if

'Controllo il valore della variabile sACTION
	if (sAction = "Can") then
	'Se sono nella CHIUSURA DELLA VALIDITA' metto la data odierna nella SCADENZA

'		if sIsa = "0" then
			sSQL = "UPDATE TADES SET Scadenza=" & ConvDateToDbS(date()) &_
					" WHERE ISA='" & sIsa &_
					"' and Nome_Tabella = '" & sTabella &_
					"' and  Codice = '" & sCodice &_
					"' and " & ConvDateToDbS(sData) & " BETWEEN Decorrenza AND Scadenza"
'		else
'			sSQL = "UPDATE TADES SET Scadenza=" & ConvDateToDbS(date()) &_
'					" WHERE ISA='" & sIsa &_
'					"' and Nome_Tabella = '" & sTabella &_
'					"' and  Codice = '" & sCodice &_
'					"' and " & ConvDateToDbS(sData) & " BETWEEN Decorrenza AND Scadenza"
'		end if
		
		set CnConn=Server.CreateObject("ADODB.Connection")
'		CC.Execute sSQL	

		CC.BeginTrans		

		sErrore=EseguiNoC(sSQL,CC)
	
		IF sErrore="0" then
			CC.CommitTrans
			Messaggio("Chiusura delle validit� del Codice '" & sCodice & " ' correttamente effettuata.")
%>
			<meta http-equiv="Refresh" content="1; URL=TAB_VisCodici.asp?NomeTab=<%=sTabella%>">	
			<!--TABLE cellspacing=2 cellpadding=1 border=0 align=center width=500>			<TR ALIGN=CENTER>				<td class=tbltext3>					Chiusura delle validit� del Codice ' <%=sCodice %> ' correttamente effettuata.				</td>			<Tr>		</table>		<br><br>		<TABLE cellspacing=2 cellpadding=1 border=0 align=center width=500>			<TR ALIGN=CENTER>				<td class=tbltext3>					<A class=textred HREF='TAB_ElencoTabelle.asp' onmouseover="javascript: window.status = ' '; return true;"><img src="<%=Session("progetto")%>/images/Indietro.gif" title="Torna all'elenco delle tabelle"  border="0" align ="absBottom"></A>				</td>			<Tr>		</table-->
<%
		else
			CC.RollbackTrans		
			Messaggio("Se ha encontrado un error. <br>" & sErrore)
			Indietro()
		end if
	else 
		Select case sContatore
			case 0 
				'La Validit� del CODICE non rispetta la Validit� della TABELLA
				Messaggio("Periodo di validit� del Codice non congruente con il periodo di validit� della Tabella.")
				Indietro()
%>				
				<!--TABLE cellspacing=2 cellpadding=1 border=0 align=center width=500>					<TR ALIGN=CENTER>						<td class=tbltext3>							Periodo di validit� del Codice non congruente con il periodo di validit� della Tabella.						</td>					<Tr>				</table>				<br><br>				<TABLE cellspacing=2 cellpadding=1 border=0 align=center width=500>					<TR ALIGN=CENTER>						<td class=tbltext3>							<A class=textred HREF="javascript:Indietro()" 								onmouseover="javascript: window.status = ' '; return true;"><img src="<%=Session("progetto")%>/images/Indietro.gif" title="Pagina precedente"  border="0" align ="absBottom"></A>						</td>					<Tr>				</table>-->				
<%			case 1	
				Count = 0

				sCond=" ((DECORRENZA BETWEEN " & ConvDateToDbS(sDecorrenza) &_
					  " AND " & ConvDateToDbS(sScadenza) & ") OR " &_
				      " (SCADENZA BETWEEN " & ConvDateToDbS(sDecorrenza) &_
					  " AND " & ConvDateToDbS(sScadenza) & ") OR " &_
					  " (" & ConvDateToDbS(sDecorrenza) & " BETWEEN DECORRENZA AND SCADENZA) OR " &_
					  " (" & ConvDateToDbS(sScadenza) & " BETWEEN DECORRENZA AND SCADENZA)" &_
					  ")" 

				if sAction ="Ins" then
					sSQL = "SELECT COUNT(*) AS Count FROM TADES " &_
							"WHERE Codice = '" & sCodice & "'" & _
							" AND ISA='" & sIsa & "'" & _
							" AND Nome_Tabella = '" & sTabella & "'" & _
							" AND " & sCond				
					'Response.Write ("Inserimento " & sSQL & "<BR>")
				else
					sSQL = "SELECT COUNT(*) AS Count FROM TADES " &_
							" WHERE Codice = '" & sCodice & "'" & _
							" AND ISA='" & sIsa & "'" &_
							" AND Nome_Tabella = '" & sTabella & "'" & _
							" AND " & sCond	& " AND DECORRENZA !=" & ConvDateToDbS(sDecorrenzaOld)						
'							" AND ((" & ConvDateToDbS(sDecorrenza) & " >= Decorrenza AND " & _
''						ConvDateToDbS(sDecorrenza) & " <= Scadenza) OR (" & _
''						ConvDateToDbS(sScadenza) & _
''						" >= Decorrenza AND " & ConvDateToDbS(sScadenza) & " <= Scadenza))"
'					Response.Write ("Altro " & sSQL & "<BR>")						
				end if
				
'				Response.Write sSQL
'				Response.End 
				set rsTabelle = Server.CreateObject("ADODB.Recordset")
				rsTabelle.Open sSQL, CC,3
	
				sCount = clng(rsTabelle ("Count"))
'
' utile per la modifica
'				
'				if sIsa = "0" and _
'				if	sCount = 0 and _ 
'					sAction = "Mod" then
'					Count = 1
'				end if
				
				rsTabelle.Close
				set rsTabelle = nothing

				Select case sCount
					case 0 
						if sAction = "Ins" then
							'Se NON ci sono ATTRIBUTI devo mettervi uno spazio altrimenti
							' l'inserimento in tabella non funziona
							if sAttributi = "" then
								sAttributi = " "
							end if
							'I controlli di SOVRAPPOSIZIONE tra CODICI sono andati bene, 
							'quindi si pu� effettuare l'inserimento.
							sSQL = "INSERT INTO TADES (ISA,Nome_Tabella,Codice,Decorrenza,Scadenza,Descrizione,Valore) VALUES " & _
								"('" & sIsa & "','" & _
								sTabella & "','" & _
								sCodice & "'," & _
								ConvDateToDbS(sDecorrenza) & _
								"," & ConvDateToDbS(sScadenza) & _
								",'" & sDescrizione _
								& "','" & sAttributi & "')"
							
							CC.BeginTrans													

							sErrore = EseguiNoC(sSQL, CC)

							if sErrore <> "0" then
								CC.RollBackTrans													
								%>
								<table cellspacing="2" cellpadding="1" border="0" align="center" width="500">
									<tr ALIGN="CENTER">
										<td class="tbltext3">
											Si � verificato un errore nell'inserimento.<br>
											<%=sErrore%>
										</td>
									<tr>
								</table>
								<br><br>
								<table cellspacing="2" cellpadding="1" border="0" align="center" width="500">
									<tr ALIGN="CENTER">
										<td class="tbltext3">
											<a class="textred" HREF="javascript:history.back()" onmouseover="javascript: window.status = ' '; return true;"><img src="<%=Session("progetto")%>/images/Indietro.gif" title="Torna all'elenco delle tabelle" border="0" align="absBottom"></a>
										</td>
									<tr>
								</table>	
	<%
						else
							CC.CommitTrans
		%>
							<table cellspacing="2" cellpadding="1" border="0" align="center" width="500">
								<tr ALIGN="CENTER">
									<td class="tbltext3">
										Ingreso correctamente efectuado.
									</td>
								<tr>
							</table>
							<br><br>
							<meta http-equiv="Refresh" content="1; URL=TAB_VisCodici.asp?NomeTab=<%=sTabella%>">	

							<!--TABLE cellspacing=2 cellpadding=1 border=0 align=center width=500>								<TR ALIGN=CENTER>									<td class=tbltext3>										<A class=textred HREF="TAB_VisCodici.asp?NomeTab=<%=sTabella%>" 											onmouseover="javascript: window.status = ' '; return true;"><img src="<%=Session("progetto")%>/images/Indietro.gif" title="Torna all'elenco delle tabelle"  border="0" align ="absBottom"></A>									</td>								<Tr>							</table-->	
	<%					end if
					elseif sAction="Mod" then
'						if sAction = "Mod" then
							'Se NON ci sono ATTRIBUTI devo mettervi uno spazio altrimenti
							' l'inserimento in tabella non funziona
							if sAttributi = "" then
								sAttributi = " "
							end if

							' Si pu� effettuare la modifica
'							if sIsa = "0" then
								sSQL = "UPDATE TADES SET Decorrenza=" & ConvDateToDbS(sDecorrenza) & _
									" , Scadenza=" & ConvDateToDbS(sScadenza) & _
									" , descrizione = '" & sDescrizione & _
									"', Valore = '" & sAttributi & _
								"' WHERE Isa='" & sIsa & _
									"' AND Nome_tabella='" & sTabella & _
									"' AND Codice='" & sCodice & _
									"' AND Decorrenza=" & ConvDateToDbS(sDecorrenzaCodice) & _
									" AND Scadenza=" & ConvDateToDbS(sScadenzaCodice) & ""
'							else
'								sSQL = "UPDATE TADES SET Decorrenza=" & ConvDateToDbS(sDecorrenza) & _
'									" , Scadenza=" & ConvDateToDbS(sScadenza) & _
'									" , descrizione = '" & sDescrizione & _
'									"', Valore = '" & sAttributi & _
'								"' WHERE Isa='" & sIsa & _
'									"' AND Nome_tabella='" & sTabella & _
'									"' AND Codice='" & sCodice & "'"
'							end if 
'							CC.Execute sSQL

							CC.BeginTrans
							
							sErrore = EseguiNoC(sSQL,CC)
													
							if sErrore = "0" then
								CC.CommitTrans
%>
								<table cellspacing="2" cellpadding="1" border="0" align="center" width="500">
									<tr ALIGN="CENTER">
										<td class="tbltext3">
											Modifica correttamente effettuata.
										</td>
									<tr>
								</table>
								<br><br>
								<meta http-equiv="Refresh" content="1; URL=TAB_VisCodici.asp?NomeTab=<%=sTabella%>">	
								<!--TABLE cellspacing=2 cellpadding=1 border=0 align=center width=500>									<TR ALIGN=CENTER>										<td class=tbltext3>											<A class=textred HREF="TAB_VisCodici.asp?NomeTab=<%=sTabella%>" 												onmouseover="javascript: window.status = ' '; return true;"><img src="<%=Session("progetto")%>/images/Indietro.gif" title="Torna all'elenco delle tabelle"  border="0" align ="absBottom"></A>										</td>									<Tr>								</table-->
								
<%							else
								CC.RollbackTrans
								Messaggio("Si � verificato un errore.<br>" & sErrore)
								Indietro()
							end if						
'						else	
%>
<!--							<TABLE cellspacing=2 cellpadding=1 border=0 align=center width=500>								<TR ALIGN=CENTER>									<td class=tbltext3>										Sovrapposizione dei periodi di validit� del Codice.									</td>								<Tr>							</table>							<br><br>							<TABLE cellspacing=2 cellpadding=1 border=0 align=center width=500>								<TR ALIGN=CENTER>									<td class=tbltext3>										<A class=textred HREF="javascript:Indietro()" 											onmouseover="javascript: window.status = ' '; return true;"><img src="<%=Session("progetto")%>/images/Indietro.gif" title="Pagina precedente"  border="0" align ="absBottom"></A>									</td>								<Tr>							</table>							-->
<%						end if					
					case else
%>						<table cellspacing="2" cellpadding="1" border="0" align="center" width="500">
							<tr ALIGN="CENTER">
								<td class="tbltext3">
									Sovrapposizione dei periodi di validit� del Codice.
								</td>
							<tr>
						</table>
						<br><br>
						<table cellspacing="2" cellpadding="1" border="0" align="center" width="500">
							<tr ALIGN="CENTER">
								<td class="tbltext3">
									<a class="textred" HREF="javascript:Indietro()" onmouseover="javascript: window.status = ' '; return true;"><img src="<%=Session("progetto")%>/images/Indietro.gif" title="Pagina precedente" border="0" align="absBottom"></a>
								</td>
							<tr>
						</table>	
<%				end select			
		end select
	end if	
	End Sub
'**********************************************************************************************************************************************************************************
	Sub Messaggio(Testo)
%>		
		<br>
		<table width="500" align="center" border="0" cellspacing="2" cellpadding="1">
			<tr height="20"> 
			  	<td class="tbltext3" align="middle" width="500">
					<b><%=Testo%></b>
				</td>
			</tr>
		</table>
		<br>
<%				
	End Sub
%>
<!--#include virtual ="/include/closeconn.asp"-->
<!--#include virtual = "/strutt_coda2.asp"-->
