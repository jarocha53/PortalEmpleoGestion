<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%@ Language=VBScript %>
<html>
<head>
<title> Aggiungi Corsi Facoltativi </title>
<link REL="STYLESHEET" TYPE="text/css" HREF="<%=session("progetto")%>/fogliostile.css">
<meta NAME="GENERATOR" Content="Microsoft Visual Studio 6.0">
</head>
<body class="sfondocentro">
<%
'-------------------------------------------------------------------------------
Sub Inizio
	dim sFunzione
	dim sTitolo
	dim sCommento
	dim bCampiObbl

	sFunzione = ""
	sTitolo = "Aggiungi CORSI FACOLTATIVI"
	sCommento = "Scegliere un' Area Tematica, quindi selezionare uno o pi� corsi e premere<br> il tasto Invia."
	bCampiObbl = false
%>	<center>
	<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
	<tr height="18">
		<td class="sfondomenu" width="67%" height="18"><span class="tbltext0"><b>&nbsp;<%=sTitolo%></b></span></td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
		<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">
<%			
			if bCampiObbl then
				Response.Write "(*) campi obbligatori"
			end if
%>
		</td>
	</tr>
	</table>
	<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
		<tr>
			<td align="left" class="sfondocomm">
				<%=sCommento%>
			<!--a href="Javascript:Show_Help('/Pgm/help/Conoscenze/CON_VisConoscenze')"-->
			<img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0" onmouseover="javascript:window.status='' ; return true"><!--/a-->
			</td>
		</tr>
		<tr height="2">
			<td class="sfondocomm" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>
	</center>
	<br>
<%
End sub
'-------------------------------------------------------------------------------
Sub Inserimento
	dim sSql
	dim sIdCorFac'� una stringa contenente gli id dei corsi scelti
	dim aIdCorFac' � il vettore
	dim iIdGruppo
	dim dtOra
	
	sIdCorFac=Request("chkCorFac")
	iIdGruppo=Request("hIdGruppo")
	'Response.Write "sIdCorFac=" & sIdCorFac & "<br>"
	'Response.Write "iIdGruppo=" & iIdGruppo & "<br>"
	aIdCorFac=split(sIdCorFac,", ")
	'Response.Write "Ubound(aIdCorFac)=" & Ubound(aIdCorFac) & "<br>"
	dtOra=Now()
	for i=0 to Ubound(aIdCorFac)
	'Response.Write "aIdCorFac=*" & aIdCorFac(i) & "*<br>"
		sSql=	"INSERT INTO CATCORSIFACOLTATIVI " &_
				"(IDGRUPPO,ID_CORSO,DT_TMST) " &_
				"VALUES " &_
				"(" & iIdGruppo & "," & aIdCorFac(i) & "," & ConvDateToDB (dtOra) &  ")"
	'Response.Write sSql & "<br>"
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
		CC.execute(sSql)
	next
	
%>	<script>
		alert('Inserimento correttamente effettuato.')
		self.close();
		window.opener.frmGruppo.submit();
	</script>
	
<%	
End sub
'-------------------------------------------------------------------------------

'MAIN
%>
<!--#include virtual="/include/OpenConn.asp"-->
<!--#include virtual="/util/dbutil.asp"-->
<center>
<%
	call Inizio
	call Inserimento
%>
</center>
<!--#include virtual="/include/CloseConn.asp"-->
</body>
</html>
