<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%@ Language=VBScript %>
<html>
<head>
<title> Elimina Corsi Facoltativi </title>
<link REL="STYLESHEET" TYPE="text/css" HREF="<%=session("progetto")%>/fogliostile.css">
<meta NAME="GENERATOR" Content="Microsoft Visual Studio 6.0">
</head>
<body class="sfondocentro">
<%
'-------------------------------------------------------------------------------
Sub Inizio
	dim sFunzione
	dim sTitolo
	dim sCommento
	dim bCampiObbl

	sFunzione = ""
	sTitolo = "Elimina CORSI FACOLTATIVI"
	sCommento = "Selezionare uno o pi� corsi da eliminare e premere il tasto Elimina."
	bCampiObbl = false
%>	<center>
	<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
	<tr height="18">
		<td class="sfondomenu" width="67%" height="18"><span class="tbltext0"><b>&nbsp;<%=sTitolo%></b></span></td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
		<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">
<%			
			if bCampiObbl then
				Response.Write "(*) campi obbligatori"
			end if
%>
		</td>
	</tr>
	</table>
	<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
		<tr>
			<td align="left" class="sfondocomm">
				<%=sCommento%>
			<!--a href="Javascript:Show_Help('/Pgm/help/Conoscenze/CON_VisConoscenze')"-->
			<img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0" onmouseover="javascript:window.status='' ; return true"><!--/a-->
			</td>
		</tr>
		<tr height="2">
			<td class="sfondocomm" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>
	</center>
	<br>
<%
End sub
'-------------------------------------------------------------------------------
Sub Cancellazione
	dim sSql,nRec
	dim sIdCorFac'� una stringa contenente gli id dei corsi scelti separati da virgola
	dim aIdCorFac,iElementiSelezionati
	dim iIdGruppo
	dim dtOra
	
	iIdGruppo=Request("hIdGruppo")
	sIdCorFac=Request("chkCorFac")
	aIdCorFac=split(sIdCorFac,", ")
	iElementiSelezionati=Ubound(aIdCorFac)+1
	 
	'Response.Write "iIdGruppo=" & iIdGruppo & "<br>"
	'Response.Write "sIdCorFac=" & sIdCorFac & "<br>"
	'Response.Write "iElementiSelezionati=" & iElementiSelezionati & "<br>"
	CC.BeginTrans
	
	sSql=	"DELETE FROM CATCORSIFACOLTATIVI " &_
			"WHERE IDGRUPPO=" & iIdGruppo &_
			" AND ID_CORSO IN (" & sIdCorFac & ")"
			
'Response.Write sSql & "<br>"
'Response.End	
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	CC.execute sSql,nRec
'Response.Write "nRec=" & nRec & "<br>"
	
	if (nRec=iElementiSelezionati) then
		CC.CommitTrans
%>		<script>
			alert('Eliminazione effettuata.')
			self.close()
			window.opener.frmGruppo.submit();
		</script>
<%	else
		CC.RollbackTrans
%>		<br><br><br><br>
		<span class="tbltext3">
			Dati modificati da altro utente.
		</span>
		<br><br><br><br><br><br><br><br><br><br>
		<a href="JavaScript:self.close();">
			<img src="<%=session("progetto")%>/images/chiudi.gif" border="0">
		</a>
<%	end if
	
End sub
'-------------------------------------------------------------------------------

'MAIN
%>
<!--#include virtual="/include/OpenConn.asp"-->
<!--#include virtual="/util/dbutil.asp"-->
<center>
<%
	call Inizio
	call Cancellazione
%>
<!--#include virtual="/include/CloseConn.asp"-->
</center>
</body>
</html>
