<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%@ Language=VBScript %>
<script LANGUAGE="JavaScript" src="/Include/Help.inc">
</script>

<html>
<head>
<title> Elimina Corsi Facoltativi </title>
<link REL="STYLESHEET" TYPE="text/css" HREF="<%=session("progetto")%>/fogliostile.css">
<meta NAME="GENERATOR" Content="Microsoft Visual Studio 6.0">

<script LANGUAGE="JavaScript">
function ControllaCestino()
{
	var n
	var cancellato
		
	n=document.frmModCorFac.chkCorFac.length

	if (n==null)
	{
		if (document.frmModCorFac.chkCorFac.checked)
		{
			//document.frmModCorFac.imgCestino.src=imgPathCestino
			ldbTitCor.innerHTML='<s>' + ldbTitCor.innerHTML + '</s>'
			ldbDescCor.innerHTML='<s>' + ldbDescCor.innerHTML + '</s>'
		}
		else
		{
			//document.frmModCorFac.imgCestino.src=imgPathVuoto
			ldbTitCor.innerHTML=(ldbTitCor.innerHTML.substring(3,ldbTitCor.innerHTML.length-4))
			ldbDescCor.innerHTML=(ldbDescCor.innerHTML.substring(3,ldbDescCor.innerHTML.length-4))
		}
	}
	else
	{
	var c
		for (i=0;i<n;i++)
		{
			cancellato=( ((ldbTitCor[i].innerHTML.substring(0,3)=='<S>') || (ldbTitCor[i].innerHTML.substring(0,3)=='<s>'))&& ( ldbTitCor[i].innerHTML.substr(ldbTitCor[i].innerHTML.length-4)=='</S>') || (ldbTitCor[i].innerHTML.substr(ldbTitCor[i].innerHTML.length-4)=='</s>') )
			if ((document.frmModCorFac.chkCorFac[i].checked) && (!cancellato))
			{
				//document.frmModCorFac.imgCestino[i].src=imgPathCestino
				ldbTitCor[i].innerHTML='<s>' + ldbTitCor[i].innerHTML + '</s>'
				ldbDescCor[i].innerHTML='<s>' + ldbDescCor[i].innerHTML + '</s>'
			}
			if ((!document.frmModCorFac.chkCorFac[i].checked) && cancellato)
			{
				//document.frmModCorFac.imgCestino[i].src=imgPathVuoto
				ldbTitCor[i].innerHTML=(ldbTitCor[i].innerHTML.substring(3,ldbTitCor[i].innerHTML.length-4))
				ldbDescCor[i].innerHTML=(ldbDescCor[i].innerHTML.substring(3,ldbDescCor[i].innerHTML.length-4))					
			}
		}
	}
}
	
function ControllaDati()
{
	var n
	n=document.frmModCorFac.chkCorFac.length

	if (n==null)
	{
		if (!document.frmModCorFac.chkCorFac.checked)
		{
			alert('Selezionare il corso per eliminarlo.') 
			return false
		}
	}
	else
	{
		for (i=0;i<n;i++)
		{
			if (document.frmModCorFac.chkCorFac[i].checked)
			{
				return true
			}
		}
		alert('Non � stato selezionato nessun corso da eliminare.')
		return false
	}
return true	
}
</script>

</head>
<body class="sfondocentro">
<%
'-------------------------------------------------------------------------------
Sub Inizio
	dim sFunzione
	dim sTitolo
	dim sCommento
	dim bCampiObbl

	sFunzione = ""
	sTitolo = "Elimina CORSI FACOLTATIVI"
	sCommento = "Selezionare uno o pi� corsi da eliminare e premere il tasto Elimina."
	bCampiObbl = false
%>	
	<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
	<tr height="18">
		<td class="sfondomenu" width="67%" height="18"><span class="tbltext0"><b>&nbsp;<%=sTitolo%></b></span></td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
		<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">
<%			
			if bCampiObbl then
				Response.Write "(*) campi obbligatori"
			end if
%>
		</td>
	</tr>
	</table>
	<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
		<tr>
			<td align="left" class="sfondocomm">
				<%=sCommento%>
			<a href="Javascript:Show_Help('/Pgm/help/Formazione/CorsiFac/CFA_ModCorFac/')">
			<img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0" onmouseover="javascript:window.status='' ; return true"></a>
			</td>
		</tr>
		<tr height="2">
			<td class="sfondocomm" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>
	<br>
	
<%
End sub
'-------------------------------------------------------------------------------
Sub ImpostaPag
	dim sSql
	dim sIdDescGruppo,aIdDescGruppo,iIdGruppo
	dim sDescGruppo'non viene utilizzata
	dim sTitCor,sDescCor

	sIdDescGruppo=Request("UIdDescGruppo")
	aIdDescGruppo=split(sIdDescGruppo,"|")
	iIdGruppo=clng(aIdDescGruppo(0))
	sDescGruppo=aIdDescGruppo(1)
	
	'Response.Write "sIdDescGruppo=" & sIdDescGruppo & "<br>"
	'Response.Write "iIdGruppo=" & iIdGruppo & "<br>"
	'Response.Write "sDescGruppo=" & sDescGruppo & "<br>"
	
	sSql="SELECT C.ID_CORSO,C.TIT_CORSO,C.DESC_CORSO " &_
		"FROM CORSO C,CATCORSIFACOLTATIVI CF " &_
		"WHERE C.ID_CORSO=CF.ID_CORSO AND " &_
		"CF.IDGRUPPO=" & iIdGruppo &_
		" ORDER BY C.TIT_CORSO"
	
	'Response.Write sSql & "<br>"	
	'Response.End

'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	set RstCorFac=CC.execute(sSql)
	
	if not RstCorFac.eof then
	
%>		<br>
		<form name="frmModCorFac" action="CFA_CnfModCorFac.asp" onsubmit="return ControllaDati()" method="post" id="frmModCorFac">
		<table border="0" cellspacing="1" width="98%">
			<tr class="sfondoCommFad">
				<td nowrap width="4">&nbsp;
				</td>
				<td>&nbsp;<b>Titolo</b>
				</td>
				<td>&nbsp;<b>Descrizione</b>
				</td>
			</tr>
			
<%			do while not RstCorFac.eof
				sTitCor=server.HTMLEncode(RstCorFac("TIT_CORSO"))
				sDescCor=server.HTMLEncode(RstCorFac("DESC_CORSO"))
%>				<tr class="tblSfondoFad">
					<td width="4">
						<input type="checkbox" onClick="JavaScript:ControllaCestino();" id="chkCorFac" name="chkCorFac" value="<%=RstCorFac("ID_CORSO")%>">
					</td>
					<td class="tblDettFad">&nbsp;<label id="ldbTitCor" name="ldbTitCor"><%=sTitCor%></label>
					</td>
					<td class="tblDettFad">&nbsp;<label id="ldbDescCor" name="ldbDescCor"><%=sDescCor%></label>
					</td>
				</tr>
<%				RstCorFac.MoveNext
			loop
%>		</table>
		<br><br>
		<table border="0">
			<tr>
				<td>
					<a href="JavaScript:self.close();">
					<img src="<%=session("progetto")%>/images/annulla.gif" border="0">
					</a>
				</td>
				<td width="10">&nbsp;
				</td>
				<td>
					<input type="image" src="<%=session("progetto")%>/images/elimina.gif" title="Elimina i corsi selezionati" id="image1" name="image1">
				</td>
			</tr>
		</table>
		<input type="hidden" name="hIdGruppo" value="<%=iIdGruppo%>">
		</form>
<%	else
%>		<br><br><br><br>
		<span class="tbltext3">
			Non vi sono corsi per le scelte effettuate.
		</span>
		<br><br><br><br><br><br><br><br><br><br>
		<a href="JavaScript:self.close();">
			<img src="<%=session("progetto")%>/images/annulla.gif" border="0">
		</a>		
<%	end if
	
	RstCorFac.close
	set RstCorFac=nothing
End sub
'-------------------------------------------------------------------------------

'MAIN
%>
<!--#include virtual="/include/OpenConn.asp"-->
	<center>
<%
	call Inizio
	call ImpostaPag
%>
	</center>
<!--#include virtual="/include/CloseConn.asp"-->
</body>
</html>
