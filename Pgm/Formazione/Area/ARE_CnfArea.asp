<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%
Sub Inizio()
	dim sFunzione
	dim sTitolo
	dim sCommento
	dim bCampiObbl
	
	sFunzione = "GESTIONE CORSI"
'	Select case sOper 
'	case "INS"
'		sTitolo = "INSERIMENTO"
'		sCommento = "Commento all'inserimento della<br>mediateca"
'	case "MOD"	
'		sTitolo = "MODIFICA"
'		sCommento = "Commento alla modifica della<br>mediateca"
'	case else
'		sTitolo = "CANCELLAZIONE"
'		sCommento = "Commento alla cancellazione della<br>mediateca"
'	end select

'	sTitolo = sTitolo & " MEDIATECA"
	sTitolo = "AREA"
	bCampiObbl = false
	sHelp = "/Pgm/help/Formazione/Area/ARE_CnfArea/"	

%>
	<!--#include virtual="include/SetTestata.asp"-->
<%
End Sub
'--------------------------------------------------------------
Function Inserisci()

	dim sSQL
	dim sCodice, sTitolo, sDescr, dDtTmst
	
	sCodice = Replace(Request.Form("txtCodArea"), "'", "''")
	sTitolo = Replace(Request.Form("txtTitArea"), "'", "''")
	sDescr = Replace(Request.Form("txtDescArea"), "'", "''")
	dDtTmst = ConvDateToDB(Now())
	
	sSQL = "INSERT INTO AREA (" &_
			"CODICEAREA, TITOLOAREA, DESCRIZIONEAREA, DT_TMST " &_
			") VALUES (" &_
			"'" & sCodice & "', '" & sTitolo & "', '" & sDescr & "', " & dDtTmst & ")"
	'Response.Write sSQL

	sErrore = EseguiNoC(sSQL ,CC)

	if sErrore = "0" then
		sMess = "Inserimento correttamente eseguito"
	else
		sMess = "Errore durante l'inserimento:<br>" & sErrore
	end if

	call VisRisultato(sMess)
End Function
'---------------------------------------------------------------------------------------------
Function Modifica()

	dim sSQL
	dim sCodice, sTitolo, sDescr, dDtTmst
	dim nIdArea
	
	nIdArea = Request.Form("hIdArea")
	sCodice = Replace(Request.Form("txtCodArea"), "'", "''")
	sTitolo = Replace(Request.Form("txtTitArea"), "'", "''")
	sDescr = Replace(Request.Form("txtDescArea"), "'", "''")
	dDtTmst = Request.Form("hDtTmst")
	
	sSQL = "UPDATE AREA SET " &_
			"CODICEAREA = '" & sCodice & "', " &_
			"TITOLOAREA = '" & sTitolo & "', " &_
			"DESCRIZIONEAREA = '" & sDescr & "', " &_
			"DT_TMST = " & ConvDateToDB(Now()) & " " &_
			"WHERE ID_AREA = " & nIdArea
				
'	Response.Write sSQL 
	sErrore = Esegui(nIdArea, "AREA", Session("idutente"), "MOD", sSQL, 1, dDtTmst)
	if sErrore = "0" then
		sMess = "Modifica correttamente eseguita"
	else
		sMess = "Errore durante la modifica:<br>" & sErrore
	end if

	call VisRisultato(sMess)
	
End Function
'--------------------------------------------------------------------------
Sub VisRisultato(sMessaggio)
%>		
<table border=0 cellspacing=2 cellpadding=1 width="500">
	<tr align=center>
		<td class="tbltext3">
			<%=sMessaggio%>
		</td>
	<tr>
	<tr><td>&nbsp;</td></tr>
	<tr align=center>
		<td class="tbltext3">
		<%
			PlsLinkRosso "ARE_VisArea.asp", "Elenco delle Aree"
		%>
		</td>
	</tr>
</table>
<%	
End Sub
'--------------------------------------------------------------------------
'M A I N
%>
<!--#include virtual="/strutt_testa2.asp"-->
<!--#include virtual="/include/openconn.asp"-->
<!--#include virtual="/Util/DBUtil.asp"-->
<!--#include virtual="/Include/SysFunction.asp"-->
<!--#include virtual="/Include/SetPulsanti.asp"-->
<%
	Dim sOper 
	
	sOper = Request("hMod")	
	Inizio()
	Select case sOper
		case "INS"
			call Inserisci()
		case "MOD"
			call Modifica()
		case else
%>		
		<table border=0 cellspacing=2 cellpadding=1 width="500">
			<tr align=center>
				<td class="tbltext3">
					Errore nel Passaggio dei parametri. (<%=sOper%>)
				</td>
			<tr>
			<tr align=center>
				<td class="tbltext3">
				<%
					PlsIndietro()
				%>
				</td>
			</tr>
		</table>
<%	
	End select
%>
<!--#include virtual="/include/closeconn.asp"-->
<!--#include virtual="/strutt_coda2.asp"-->
