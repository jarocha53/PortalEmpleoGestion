<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%
	Sub Inizio()
	dim sFunzione
	dim sTitolo
	dim sCommento
	dim bCampiObbl
	
	sFunzione = "GESTIONE QUESTIONARIO"
	sTitolo = "QUESTIONARIO"
	sCommento = "Inserimento del<br>Questionario"
	bCampiObbl = false
%>
	<!--#include virtual="/include/SetTestata.asp"-->
<%
	End Sub
'--------------------------------------------------------------------------
	Sub Inserisci()
		Dim sTitolo, nNumDomande, nTempo, sTipo, nId_Area
		Dim sFlMemoRisp, sIdProjOwn, sFlPubPri, sFlRep
		Dim sErrore, nMax
		
		sTitolo = Replace(Request.Form("QTitolo"), "'", "''")
		nId_Area = Request.Form("QArea")
		sTipo=Request.Form("QTipo")
		nNumdomande = Request.Form("QNumDomande")
		nTempo = Request.Form("QTempo")
		nSoglia = Request.Form("QSoglia")
		
'		sFlMemoRisp = Request.Form("chkMemorisp")
		sIdProjOwn = Request.Form("cboProjOwner")
		sFlPubPri = Request.Form("cboFlPubblico")
		sFlRep = Request.Form("chkFlRepeat")
		
		if nTempo <= "0" then
			nTempo = 0
		end if
		
'		if sFlMemoRisp = "on" then
'			sFlMemoRisp = "T"
'		else
'			sFlMemoRisp = "T"
'		end if
		
		if sIdProjOwn = "" then
			sIdProjOwn = mid(session("progetto"),2)
		end if
		
		if sFlRep = "on" then
			sFlRep = "1"
		else
			sFlRep = "0"
		end if

		DTTMST = ConvDateToDB(now())

		sql = "INSERT INTO QUESTIONARIO (NUM_DOM, TIP_QUESTIONARIO, ID_AREA, " &_
				"TEM_QUESTIONARIO, TIT_QUESTIONARIO, SGL_QUESTIONARIO, FL_COMPLETO, " &_
				"DT_TMST, ID_PROJOWNER, FL_PUBBLICO, FL_REPEAT) " &_
				" VALUES (" & nNumDomande & " " &_
				",'" & sTipo & "' " &_
				"," & nId_Area & " " &_
				"," & nTempo & " " &_
				",'" & sTitolo & "' " &_
				"," & nSoglia & " " &_
				",'N' " &_
				"," & DTTMST & " " &_
				",'" & sIdProjOwn & "' " &_
				"," & sFlPubPri & " " &_
				"," & sFlRep & ")"
			
		'Response.Write sql
		sErrore=Esegui("pgm/formazione/questionari/que","pgm/formazione/questionari/queSTIONARIO",IDP,"INS",SQL,1,"")

'		sErrore="0"

		IF sErrore <> "0" then
		%>
			<br><br>
			<table border=0 cellspacing=2 cellpadding=1 width="500">
				<tr align=middle>
					<td class="tbltext3">
						Errore nell'inserimento: <%=sErrore%>
					</td>
				</tr>
			</table>		
			<br><br>
		<% else %>
			<br><br><table border=0 cellspacing=2 cellpadding=1 width="500">
			<tr align=middle>
				<td class="tbltext3">
					Inserimento Questionario correttamente effettuato
				</td>
			<tr></tr>
			</table>
			<br><br>
			<%
			
		End If %>
		 	<table border=0 cellspacing=2 cellpadding=1 width="500">
				<tr align=center>
					<td align=center>
					<%
						PlsLinkRosso "QUE_VisQuestionari.asp", "Elenco Questionari"
					%>
					</td>
				</tr>
			</table>
		<br><br>
		<%
	End Sub
'-------------------------------------------------------------------------
'M A I N
%>
<!--#include virtual="/strutt_testa2.asp"-->
<!--#include virtual="/include/openconn.asp"-->
<!--#include virtual="/Util/DBUtil.asp"-->
<!--#include virtual="/Include/SysFunction.asp"-->
<!--#include virtual="/include/SetPulsanti.asp"-->
<%	
'	Inizio()
	Inserisci()
%>			
<!--#include virtual="/include/closeconn.asp"-->
<!--#include virtual="/strutt_coda2.asp"-->
