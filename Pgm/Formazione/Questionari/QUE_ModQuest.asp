<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%
Sub ControlliJavaScript()
%>
<script language="Javascript">

	function validateIt()
	{
	if(document.form1.QArea.value == "" || document.form1.QArea.value == " ")
	  {
	  document.form1.QArea.focus();
	  alert("Inserire l'area di appartenenza. ");
	  return false;
	  }
	 if(document.form1.QTitolo.value == "" || document.form1.QTitolo.value == " ")
	  {
	  document.form1.QTitolo.focus();
	  alert("Inserire il Titolo. ");
	  return false;
	  } 
	 if(document.form1.QTipo.value == "" || document.form1.QTipo.value == " ")
	  {
	  document.form1.QTipo.focus();
	  alert("Inserire il Tipo. ");
	  return false;
	  } 
	 if(document.form1.QNumDomande.value > 100 || document.form1.QNumDomande.value < "1" || document.form1.QNumDomande.value > "9")
	  {
	  document.form1.QNumDomande.focus();
	  alert("Inserire nel campo Numero Aree un numero da 1 a 100. ");
	  return false;
	  } 
	 if(document.form1.QTempo.value > 100 || document.form1.QTempo.value < "1" || document.form1.QTempo.value > "9")
	  {
	  document.form1.QTempo.focus();
	  alert("Inserire nel campo Tempo Previsto un numero di minuti da 1 a 100. ");
	  return false;
	  } 
	 if(document.form1.QSoglia.value > 100 || document.form1.QSoglia.value < "1" || document.form1.QSoglia.value > "9")
	  {
	  document.form1.QSoglia.focus();
	  alert("Inserire nel campo Punteggio minimo un numero percentuale da 10 a 100.");
	  return false;
	  } 
	return true;
	}
	
	function ConfElimina(id, tmst)
	{
		if (confirm("Confermi la cancellazione del Questionario?") == true)
		{
			window.location = "QUE_DelQuest.asp?id="+id+"&tmst="+tmst
		}
	}
	
</script>
<%
End Sub
'---------------------------------------------------------------------------
Sub Inizio()
	dim sFunzione
	dim sTitolo
	dim sCommento
	dim bCampiObbl
	
	sFunzione = "GESTIONE QUESTIONARIO"
	sTitolo = "QUESTIONARIO"
	'sCommento = "Modifica del<br>Questionario"
	sCommento = "Modifica Questionario<br>Modificare le informazioni e premere Invia per memorizzare. Premere Elimina per cancellare il questionario." 
	bCampiObbl = true
	sHelp = "/Pgm/help/Formazione/Questionari/QUE_ModQuest/"	
%>
	<!--#include virtual="include/SetTestata.asp"-->
<%
End Sub
'------------------------------------------------------------------------------------------------
Sub PaginaVuota()
%>
<table width="500" border="0">
	<tr>
		<td class="tbltext3" align=center>
			Errore durante la ricerca del Questionario.
		</td>
	</tr>
	<tr><td>&nbsp;</td></tr>
	<tr>
		<td align=center>
			<%
				PlsIndietro()
			%>
		</td>
	</tr>
</table>
<%
End Sub
'--------------------------------------------------------------------------
Sub ImpostaPag()

Dim RS, idquest 
idquest = Request.querystring("id")
	
	set Rs=server.CreateObject("ADODB.Recordset")
	sql1 = "SELECT ID_QUESTIONARIO, NUM_DOM, TIP_QUESTIONARIO, ID_AREA, " &_
			"TEM_QUESTIONARIO, TIT_QUESTIONARIO, FL_COMPLETO, INT_QUESTIONARIO, " &_
			"SGL_QUESTIONARIO, DT_TMST, ID_PROJOWNER, FL_PUBBLICO, FL_REPEAT from Questionario where ID_Questionario = " & idquest
	'Response.Write sql1
'PL-SQL * T-SQL  
SQL1 = TransformPLSQLToTSQL (SQL1) 
	Rs.open sql1, CC, 3
	if rs.EOF then
		PaginaVuota()
		exit sub
	end if

	fArea = Rs.Fields("ID_QUESTIONARIO")
	xnumdom = Rs.Fields("NUM_DOM") 
	xtipque = Rs.Fields("TIP_QUESTIONARIO") 
	xidarea = Rs.Fields("ID_AREA") 
	xtempoq = Rs.Fields("TEM_QUESTIONARIO") 
	xtitque = Rs.Fields("TIT_QUESTIONARIO")
	xcomple = Rs.Fields("FL_COMPLETO") 
	xintque = Rs.Fields("INT_QUESTIONARIO") 
	xsoglia = Rs.Fields("SGL_QUESTIONARIO")
	xIdPrOw = Rs.Fields("ID_PROJOWNER")
	xFlPubb = Rs.Fields("FL_PUBBLICO")
	xFlRepe = Rs.Fields("FL_REPEAT")
	dTmSt = Rs.fields("DT_TMST")
	Rs.close
	set Rs = nothing 
		
	set Rs=server.CreateObject("ADODB.Recordset")
	sql1="SELECT TITOLOAREA from AREA where ID_AREA = " & xidarea
'	Response.Write sql1
'PL-SQL * T-SQL  
SQL1 = TransformPLSQLToTSQL (SQL1) 
	Rs.open sql1, CC, 3
	fArea = Rs.Fields("TITOLOAREA")
	c = 1

	Rs.close
	set Rs = nothing %>
		
		
	<form method="POST" action="QUE_CnfModQuest.asp?Oper=Ins&IdQuest=<%=IdQuest%>" id=form1 name=form1 onsubmit="return validateIt();">
				
			<table border=0 cellspacing=2 cellpadding=1 width="500px" >
			<tr>
				<td class="tbltext1"> 
					<b>Area*</b>
				</td>
				<td>
					<select name="QArea" id="QArea" class="textblacka">			
					<%	set RR=server.CreateObject("ADODB.Recordset")
					sql="SELECT distinct ID_AREA, CodiceArea, TitoloArea from Area ORDER BY CodiceArea"
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
					RR.open sql, CC,3
					%>
					<option selected value="<%=xidarea%>"><%=fArea%></option>						
					<%
					Do While not RR.Eof					
					if cint(RR.FIELDS("ID_AREA")) <> cint(xidarea) then
					%>
					<option value="<%=RR.FIELDS("ID_AREA")%>"><%=RR.FIELDS("TITOLOAREA")%></option>
					<%
					end if
					RR.MoveNext 
					Loop
					RR.CLOSE
					set rr = nothing
				%>	</select>
				</td>
			</tr>
				<tr>
				<td class="tbltext1"> 
					<b>Titolo*</b>
				</td>
				<td>
					<input size="30" maxlength ="250" name="QTitolo" id="QTitolo" value="<%=xtitque%>" class="textblacka">
				</td>
			</tr>
			<tr>
				<td class="tbltext1">
					<b>Tipo*</b>
				</td>
				<td>
					<select name="QTipo" id="QTipo" class="textblacka">
					<% if xtipque = "S" then%>									
						<option selected value="S">Statico</option>						
						<option value="D">Dinamico</option>
					<% else %>
						<option selected value="D">Dinamico</option>						
						<option value="S">Statico</option>
					<% end if %>
					</select>
				</td>
			</tr>
				<td class="tbltext1">
					<b>Numero di Aree*</b>
				</td>
				<td>
					<input name="QNumDomande" id="QNumDomande" size="3" cols=3 maxlength="3" value="<%=xnumdom%>" name="QNumDomande" id="QNumDomande" class="textblacka">
				</td>
			</tr>
			<tr>
				<td class="tbltext1">
					<b>Tempo previsto* </b>(in minuti)
				</td>
				<td>
					<input size="3" maxlength="3" cols=3 value="<%=xtempoq%>"  name="QTempo" id="QTempo" class="textblacka">
				</td>
			</tr>
			<tr>
				<td class="tbltext1">
					<b>Punteggio minimo*</b>
				</td>
				<td>
					<input size="3" maxlength="3" cols=3 value="<%=xsoglia%>" name="QSoglia" id="QSoglia" class="textblacka">
					<span class="tbltext1">% del punteggio totale</span>
				</td>
			</tr>
				<tr height=20> 
					<td align="left" nowrap class="tbltext1">
						<b>Progetto proprietario</b>
					</td>
					<td class=textblack>
						<b>
						<%=DecCodVal("CPROJ", "0", date, xIdPrOw, 1)%>
						</b>
					</td>
						<div id=dHideCombo1 style="visibility:hidden">
						<%
							CreateCombo("CPROJ|||" & xIdPrOw & "|cboProjOwner||")
						%>
						</div>
				</tr>
				<tr height=20> 
					<td align="left" nowrap class="tbltext1">
						<b>Pubblico/Privato</b>
					</td>
					<td class=textblack> 
						<b>
						<%
						if cint(xFlPubb) = 0 then
							Response.Write "PUBBLICO"
						else
							Response.Write "PRIVATO"
						end if
						%>
						</b>
					</td>
					<div id=dHideCombo2 style="visibility:hidden">
						<select name="cboFlPubblico" id="cboFlPubblico" class=textblack>
							<option value=""></option>
							<%
							if cint(xFlPubb) = 0 then
								Response.Write "<option value='0' selected>Pubblico</option>"
							else
								Response.Write "<option value='0'>Pubblico</option>"
							end if
							if cint(xFlPubb) = 1 then
								Response.Write "<option value='1' selected>Privato</option>"
							else
								Response.Write "<option value='1'>Privato</option>"
							end if
							%>
						</select>
					</div>
				</tr>
				<tr> 
					<td align="left" nowrap class="tbltext1">
						<b>Ripetibile</b>
					</td>
					<td>
						<input type="checkbox" name="chkFlRepeat" id="chkFlRepeat"
						<%
							if xFlRepe = "1" then 
								Response.Write "checked"
							end if
						%>
						>
					</td>
				</tr>
			
			<tr><td>&nbsp;</td></tr>
		</table>
	<table width=500>
		<tr>
			<td align=center>
				<%
					PlsIndietro()
				%>
				<input type=hidden name=hDTmSt id=hDTmSt value="<%=dTmSt%>">
				<%
					PlsInvia("conferma")
					PlsElimina("javascript:ConfElimina(" & idquest & ",'" & dTmSt & "')")
				%>
			</td>
		</tr>
	</table>
</form>
<br>
<%
End Sub
'--------------------------------------------------------------------------------
'M A I N
%>
<!--#include virtual="/strutt_testa2.asp"-->
<!--#include virtual="/include/openconn.asp"-->
<!--#include virtual="/include/SetPulsanti.asp"-->
<%	
	ControlliJavaScript()	
	Inizio()
	ImpostaPag()
%>			
<!--#include virtual="/include/closeconn.asp"-->
<!--#include virtual="/strutt_coda2.asp"-->
