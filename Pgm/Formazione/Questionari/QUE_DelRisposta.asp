<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%
	Sub Inizio()
	dim sFunzione
	dim sTitolo
	dim sCommento
	dim bCampiObbl
	
	sFunzione = "GESTIONE QUESTIONARIO"
	sTitolo = "QUESTIONARIO"
	sCommento = "Cancellazione della risposta"
	bCampiObbl = false
%>	
	<!--#include virtual="include/setTestata.asp"-->
<%
	End Sub

'--------------------------------------------------------------------------------------
	Sub Cancella()
		dim sErrore, sSQL
		dim nIdRisp, nIdDom, nIdArea, nIdQuest, dDtTmst
		dim rsCRisp
		
		nIdRisp = Request.QueryString("idr")
		nIdDom = Request.QueryString("idd")
		nIdArea = Request.QueryString("ida")
		nIdQuest = Request.QueryString("idq")
		dDtTmst = Request.QueryString("tmst")
		
		sSQL = "delete from risposta where id_risposta = " & nIdRisp
		sErrore=Esegui(nIdRisp ,"risposta",Session("persona"),"DEL",sSQL,1,dDtTmst)
		
		IF sErrore <> "0" then
		%>
			<br><br>
			<table border=0 cellspacing=2 cellpadding=1 width="500">
				<tr align=middle>
					<td class="tbltext3">
						Errore nella cancellazione. <%=sErrore%>
					</td>
				</tr>
			</table>		
			<br><br>
		<% else %>
			<br><br>
			<table border=0 cellspacing=2 cellpadding=1 width="500">
			<tr align=middle>
					<td class=tbltext3>
						Cancellazione correttamente effettuata
					</td>
				</tr>
			</table>
			<br><br>
			<%
		End If
		%>
		<table border=0 cellspacing=2 cellpadding=1 width="500">
			<tr align=center>
				<td>
					<%
					PlsLinkRosso "QUE_VisRisposta.asp?idd=" & nIdDom & "&idq=" & nIdQuest & "&ida=" & nIdArea,"Elenco delle Risposte"
					%>
				</td>
			</tr>
		</table>
		<br><br>
		<%
	End Sub
'---------------------------------------------------------------------------
'M A I N
%>
<!--#include virtual="/strutt_testa2.asp"-->
<!--#include virtual="/include/openconn.asp"-->
<!--#include virtual="/Util/DBUtil.asp"-->
<!--#include virtual="/Include/SysFunction.asp"-->
<!--#include virtual="/Include/SetPulsanti.asp"-->
<%
	Inizio()
	Cancella()
%>			
<!--#include virtual="/include/closeconn.asp"-->
<!--#include virtual="/strutt_coda2.asp"-->
