<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%
Sub Inizio()
%>
<html>
<head>
	<link REL="STYLESHEET" TYPE="text/css" HREF="<%=session("progetto")%>/fogliostile.css">
	<meta NAME="GENERATOR" Content="Microsoft Visual Studio 6.0">
	<title>Pubblicazione Questionario</title>	
</head>
<script LANGUAGE="JavaScript">
function InviaSelezione(Immagine)
{
	opener.document.form1.txtMedia.value = Immagine
	self.close()
}
</script>
<body>
<p align="center">
<%
	dim sFunzione
	dim sTitolo
	dim sCommento
	dim bCampiObbl
	
	sFunzione = "GESTIONE QUESTIONARIO"
	sTitolo = "QUESTIONARIO"
	sCommento = "Pubblicazione del Questionario"
	bCampiObbl = false
	sHelp = "/Pgm/help/Formazione/Questionari/QUE_ModPubQuest/"
%>
	<!--#include virtual="/include/SetTestata.asp"-->
<%
End Sub
'------------------------------------------------------------------------
Sub ImpostaPag()
nIdQuest = Request.QueryString("id")

dim bCompleto
dim sSQL0, sSQL1, sSQL2, sSQL3, sSQL4
dim rsAppo0, rsAppo1, rsAppo2, rsAppo3, rsAppo4
dim nArea, nDomanda
dim sCompl

sCompl = ""
sSQL0  = "SELECT NUM_DOM, " &_
		"(SELECT COUNT(*) FROM AREA_DOM WHERE id_questionario = " & nIdQuest & ") AS Conta " &_
		"FROM QUESTIONARIO " &_
		"WHERE id_questionario = " & nIdQuest
'PL-SQL * T-SQL  
SSQL0 = TransformPLSQLToTSQL (SSQL0) 
set rsAppo0 = cc.execute(sSQL0)
if CInt(rsAppo0("NUM_DOM")) = CInt(rsAppo0("Conta")) then
	sCompl = sCompl & "S"
	sSQL1 = "SELECT ID_AREA_DOM FROM AREA_DOM WHERE ID_QUESTIONARIO = " & nIdQuest
'PL-SQL * T-SQL  
SSQL1 = TransformPLSQLToTSQL (SSQL1) 
	set rsAppo1 = cc.execute(sSQL1)
	do while not rsAppo1.eof
		nArea = rsAppo1("ID_AREA_DOM")
		sSQL2  = "SELECT NUM_DOM_AREA, " &_
				"(SELECT COUNT(*) FROM DOMANDA WHERE id_area_dom = " & nArea & ") AS Conta " &_
				"FROM AREA_DOM " &_
				"WHERE id_area_dom = " & nArea
'PL-SQL * T-SQL  
SSQL2 = TransformPLSQLToTSQL (SSQL2) 
		set rsAppo2 = cc.execute(sSQL2)
		if not rsAppo2.eof then
			if CInt(rsAppo2("NUM_DOM_AREA")) = CInt(rsAppo2("Conta")) then
				sCompl = sCompl & "S"
				sSQL3 = "SELECT ID_DOMANDA FROM DOMANDA WHERE ID_AREA_DOM = " & nArea
'PL-SQL * T-SQL  
SSQL3 = TransformPLSQLToTSQL (SSQL3) 
				set rsAppo3 = cc.execute(sSQL3)
				do while not rsAppo3.eof
					nDomanda = rsAppo3("ID_DOMANDA")
					sSQL4 = "SELECT NUM_RISPOSTE, " &_
							"(SELECT COUNT(*) FROM RISPOSTA WHERE id_domanda = " & nDomanda & ") AS Conta " &_
							"FROM DOMANDA " &_
							"WHERE id_domanda = " & nDomanda
'PL-SQL * T-SQL  
SSQL4 = TransformPLSQLToTSQL (SSQL4) 
					set rsAppo4 = cc.execute(sSQL4)
					if CInt(rsAppo4("NUM_RISPOSTE")) = CInt(rsAppo4("Conta")) then
						sCompl = sCompl & "S"
					else
						sCompl = sCompl & "N"
					end if
					rsAppo4.close
					rsAppo3.movenext
				loop
				rsAppo3.close
			else
				sCompl = sCompl & "N"
			end if
		else
			sCompl = sCompl & "N"
		end if
		rsAppo2.close
		rsAppo1.movenext
	loop
	rsAppo1.close
else
	sCompl = sCompl & "N"
end if
rsAppo0.close

if InStr(sCompl, "N") > 0 then
	bCompleto = false
else
	bCompleto = true
end if 

if bCompleto = false then
	Call ImpostaFlag(nIdQuest, "N")
else
	Call ImpostaFlag(nIdQuest, "S")
end if

set rsAppo0 = nothing
set rsAppo1 = nothing
set rsAppo2 = nothing
set rsAppo3 = nothing
set rsAppo4 = nothing

End Sub
'------------------------------------------------------------------------
Sub ImpostaFlag(nIdQ, sFlag)
dim sMess, sErrore
dim sSQL, rsQuest
dim sSQLUpd
select case sFlag
	case "S"
		sSQL = "Select FL_COMPLETO from QUESTIONARIO where id_questionario = " & nIdQ
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
		set rsQuest = cc.execute(sSQL)
		if rsQuest("FL_COMPLETO") = "S" then
			sMess = "Questionario gi� pubblicato"
		else
			sSQLUpd =	"update QUESTIONARIO set FL_COMPLETO = 'S', " &_ 
						"DT_TMST = " & ConvDateToDB(now()) & " " &_
						"where ID_QUESTIONARIO = " & nIdQ
			sErrore=EseguiNoC(sSQLUpd, CC)
			if sErrore = "0" then
				sMess = "Questionario pubblicato"
			else
				sMess = "Impossibile pubblicare il questionario:" & sErrore
			end if
		end if
	case "N"
		sSQL = "Select FL_COMPLETO from QUESTIONARIO where id_questionario = " & nIdQ
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
		set rsQuest = cc.execute(sSQL)
		if rsQuest("FL_COMPLETO") = "S" then
			sSQLUpd =	"update QUESTIONARIO set FL_COMPLETO = 'N', " &_ 
						"DT_TMST = " & ConvDateToDB(now()) & " " &_
						"where ID_QUESTIONARIO = " & nIdQ
			sErrore=EseguiNoC(sSQLUpd, CC)
			if sErrore = "0" then
				sMess = "Questionario non pubblicabile (precedentemente pubblicato)"
			else
				sMess = sErrore
			end if			
		else
			sMess = "Questionario non pubblicabile"
		end if
end select

call VisMessaggio(sMess)

End Sub
'------------------------------------------------------------------------
Sub VisMessaggio(sMessaggio)
%>
	<br>
		<table border="0" cellspacing="2" cellpadding="1" width="500">
			<tr align="middle">
				<td class="tbltext3">
					<%=sMessaggio%>
				</td>
			</tr>
		</table>
	<br>
<%
End Sub
'------------------------------------------------------------------------
Sub Fine()
%>
<br>
<table width="500">
	<tr>
		<td align="center">
			<a href="javascript:window.close()">
				<img src="<%=Session("progetto")%>/images/chiudi.gif" border="0" onmouseover="javascript:window.status='' ; return true">
			</a>
		</td>
	</tr>
</table>
</p>		
</body>
</html>
<%
End Sub
'------------------------------------------------------------------------
'M A I N
Response.Buffer = true
%>	
<!--#include Virtual ="/include/openconn.asp"-->
<!--#include virtual="/Util/DBUtil.asp"-->
<!--#include virtual="/Include/SysFunction.asp"-->
<%
Inizio()
Response.Flush()
ImpostaPag()
%>	
<!--#include Virtual ="/include/closeconn.asp"-->
<%
Fine()
%>
