<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include Virtual="/strutt_testa2.asp"-->
<!--#include virtual ="/util/portallib.asp"-->
<!--#include virtual="include/SetTestataFAD.asp"-->
<!--#include virtual="include/VisQuestionari.asp"-->

<script language="javascript">
	function ArgRif(nIdQuest) {
		win = window.open ('/Pgm/formazione/questionari/riferimenti/QRI_argrif.asp?inW=Yes&ResQ=' + nIdQuest,'info','width=550,height=350,Resize=No,Scrollbars=yes,screenX=w,screenY=h')
	}

</script>
<% 
'	Response.Buffer = true
'	Response.ExpiresAbsolute = Now() - 1 
'	Response.AddHeader "pragma","no-cache"
'	Response.AddHeader "cache-control","private"
'	Response.CacheControl = "no-cache"

	Sub Inizio()
		dim sTitolo
		dim sCommento
	
		sTitolo = "Questionario"
		sCommento = "In questa pagina puoi visualizzare i risultati del questionario"
		sHelp = ""
		call TestataFAD(sTitolo, sCommento, false, sHelp)

	End Sub

	Sub Aggiorna_Percorso()
		Dim sqlP
		
		sqlP = "Update PERCORSO  set dt_fine =" & ConvDateToDB(now()) & ", dt_tmst=" & ConvDateToDB(now())   
		sqlP = sqlP & " Where ID_PERSONA =" & IDP & " And ID_ELEMENTO =" & nElem & " And ID_CORSO=" & nIDCorso 	
		sOpe = "MOD"
		
'		sErrore=Esegui(nElem,"PERCORSO", Session("IdPersona"),sOpe,sqlP,1,dt_Tmst)
		sErrore = EseguiNoC(sqlP,CC)
	End Sub

'----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------'----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

sub ToppaCore(valore)
'++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
' aggiunta per controllare QModo=0 +
	
	if nElem = ""  and nIDCorso ="" then
		exit sub
	end if
'++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
'sub aggiunta in data 04/02/2004 per il tracciamento dei questionari di valutazione +
'ASSOLUTAMENTE DA RIVEDERE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!                        +
'++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++					
	sSQL = "SELECT ELE_PREC FROM REGOLA_DIPENDENZA WHERE ID_CORSO = " & nIDCorso & _
			" AND ID_ELEMENTO = " & nElem
	set rsRegDipen = Server.CreateObject("ADODB.Recordset")
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	rsRegDipen.Open sSQL, CC, 3
		
	apStatoLezione = valore				
	
	'if not rsRegDipen.eof then
	'	if rsRegDipen("ELE_PREC") <> "" then
	'		sModuloPrecedente = clng(rsRegDipen("ELE_PREC"))
	'						
	'		rsRegDipen.Close
	'									
	'		sSQL = "SELECT STATO_LEZIONE " & _
	'				" FROM REGOLA_DIPENDENZA WHERE ID_CORSO = " & nIDCorso & _
	'				" AND ID_ELEMENTO=" & sModuloPrecedente
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	'		rsRegDipen.Open sSQL, CC, 3
	'		if not rsRegDipen.eof then
	'			apStatoLezione = rsRegDipen("STATO_LEZIONE")
	'		else
	'			apStatoLezione = "NULL"
	'		end if
	'	end if
	'end if

	sSSQL = "SELECT NOME || ' ' || COGNOME as CN FROM UTENTE WHERE IDUTENTE = " & Session("idUtente")
	set rs= Server.CreateObject("ADODB.Recordset")
'PL-SQL * T-SQL  
SSSQL = TransformPLSQLToTSQL (SSSQL) 
	rs.Open sSSQL, CC, 3
	sCognNome = rs("CN")
	sCognNome = Replace(sCognNome,"'", "''")
	rs.CLOSE 

	
	sSSQL = "SELECT ID_CORSOAICC FROM CORSO WHERE ID_CORSO = " & nIDCorso
	set rs= Server.CreateObject("ADODB.Recordset")
'PL-SQL * T-SQL  
SSSQL = TransformPLSQLToTSQL (SSSQL) 
	rs.Open sSSQL, CC, 3
	sIdCorsoAicc = rs("ID_CORSOAICC")
	rs.CLOSE 
	
	sSSQL = "SELECT ID_ELEMENTOAICC FROM ELEMENTO WHERE ID_ELEMENTO = " & nElem
'PL-SQL * T-SQL  
SSSQL = TransformPLSQLToTSQL (SSSQL) 
	rs.Open sSSQL, CC, 3
	sIdElementoAicc = rs("ID_ELEMENTOAICC")
	rs.CLOSE 
	
	sSQLDel = "DELETE FROM CORE " &_
				"WHERE STUDENT_ID ='" &	Session("Idutente") & "'" & _
				"AND LESSON_ID ='" & sIdElementoAicc & "'" & _
				"AND COURSE_ID ='" & sIdCorsoAicc & "'"
	sErrore = EseguiNoC(sSQLDel, CC)

	sSQL = "SELECT ATTEMPT_NUMBER FROM STUDENT_DATA " & _
			" WHERE STUDENT_ID = '" & Session("idUtente") & "'" & _
			" and COURSE_ID = '" & sIdCorsoAicc & "'" & _
			" and LESSON_ID = '" & sIdElementoAicc & "'"
		
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	rs.Open sSQL, CC, 3
	if rs.eof then
		sAttempt = 1
	else	
		on error resume next
		sAttempt = rs("ATTEMPT_NUMBER")
		if isnull(sAttempt) then
			sAttempt = 1
		else 
			sAttempt = 1 + CLng(sAttempt)	
		end if
		on error goto 0
	end if	
	rs.CLOSE 
	set rs = nothing

	sSQLDel = "DELETE FROM STUDENT_DATA " &_
				"WHERE STUDENT_ID ='" &	Session("Idutente") & "'" & _
				"AND LESSON_ID ='" & sIdElementoAicc & "'" & _
				"AND COURSE_ID ='" & sIdCorsoAicc & "'"
	sErrore = EseguiNoC(sSQLDel, CC)

	sSQLData = "INSERT INTO STUDENT_DATA " & _
				"(LESSON_ID,STUDENT_ID,COURSE_ID," & _
				"LESSON_STATUS,TRY_TIME, ATTEMPT_NUMBER) VALUES (" & _
				"'" & sIdElementoAicc & "'," & _
				"'" & Session("idUtente") & "'," & _
				"'" & sIdCorsoAicc & "'," & _
				"'" & apStatoLezione & _
				"','00:00:00', " & sAttempt & ")"

									
	sErrore = EseguiNoC(sSQLData,CC)
	
	
	
	if sErrore = "0" then
		sSQLCore = "INSERT INTO CORE " & _
					"(LESSON_ID,STUDENT_ID,COURSE_ID," & _
					"LESSON_STATUS,CORE_TIME, STUDENT_NAME) VALUES (" & _
					"'" & sIdElementoAicc & "'," & _
					"'" & Session("idUtente") & "'," & _
					"'" & sIdCorsoAicc & "'," & _
					"'" & apStatoLezione & _
					"','00:00:00', '" & sCognNome & "')"

		sErrore  = EseguiNoC(sSQLCore,CC)
		
	end if


end sub

'----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Sub ImpostaPag()
	dim iTotPeso
	dim sErrore
		
	CC.BeginTrans
	iTotPeso = 0
	sSQL = "DELETE FROM RISULT_RISP WHERE ID_RISULT_QUEST = " & nIdQuest
	sErrore=EseguiNoC(sSQL,CC)
	if sErrore = "0" then
		for i = 1 to NDOM 
			'	D = Domanda
			'	R = Risposta
			'	E = Esatta
			'	P = Peso	
			ND = "DOM" & i
			ND = Request.Form(ND)
			D = cint(InStr(1,ND,"D")) + 1
			R = cint(InStr(1,ND,"R")) + 1
			E = cint(InStr(1,ND,"E")) + 1
			P = cint(InStr(1,ND,"P")) + 1
				
			LunD = cint((R - D)) - 1
			Dom = Mid(ND,D,LunD)
			LunR = cint((E - R)) - 1 
			Ris = Mid(ND,R,LunR)
			Esa = Mid(ND,E,1)
			Pes = clng(Mid(ND,P))
			If Esa = "S" then
				punt = punt + Pes
				ES = cint(ES) + 1
			end if 	
			'	Calcolo peso totale delle domande	
			iTotPeso = iTotPeso + Pes
			sSQLInsRisposte = "INSERT INTO RISULT_RISP (ID_RISULT_QUEST,ID_DOMANDA,ID_RISPOSTA) " &_				
				" VALUES(" & nIdQuest & "," & Dom & "," & Ris & ") "
			'Response.Write sSQLInsRisposte
			sErrore = EseguiNoc(sSQLInsRisposte,CC)
		next

		set RR=Server.CreateObject("ADODB.Recordset")
	   	sql="SELECT distinct TEM_QUESTIONARIO, SGL_QUESTIONARIO , FL_REPEAT FROM QUESTIONARIO where ID_QUESTIONARIO = " & IDQue
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
		RR.open sql, CC, 3
		If not RR.EOF then
			Durata = (RR.Fields("TEM_QUESTIONARIO")) 
			Soglia = (RR.Fields("SGL_QUESTIONARIO"))
			bRipetibile =  CINT(RR.Fields("FL_REPEAT"))
		end if
		RR.CLOSE
		set RR=Server.CreateObject("ADODB.Recordset")
	   	sql="SELECT NUM_TENTATIVI, FL_ESEGUITO, DT_ESEC, DT_TMST FROM RISULT_QUEST where ID_QUESTIONARIO = " & IDQue & " and ID_PERSONA = " & IDP
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
		RR.open sql, CC, 3
		If not RR.EOF then
			eseguito = (RR.Fields("FL_ESEGUITO"))
			tentativi = (RR.Fields("NUM_TENTATIVI")) 
			dataesec = (RR.Fields("DT_ESEC"))  
			DtTmst = (RR.Fields("DT_TMST"))  
		end if
		RR.CLOSE
		TIMEDurata = int((datediff("s",TimeInizio,TimeFine)) / 60 )
		if eseguito = "S" and bRipetibile = 0 then
			CC.CommitTrans
			DataEsec = ConvDateToString(DataEsec)%>
			<br><br><table border="0" cellspacing="2" cellpadding="1" width="500" align="center">
				<tr align="center">
					<td class="tbltext3"><b>Questionario gi� eseguito il giorno <%=dataesec%></b></span></td>
				<tr>
			</table><br><br>
			<table border="0" cellspacing="2" cellpadding="1" width="500" align="center">
				<tr align="center">
					<td><b><a class="textred" href="Javascript:history.go(-2)">Torna all'inizio</a></b></td>					
				</tr>
			</table><br><br>
		<%else
			if TIMEDURATA > 0 and TIMEDURATA < 1 then
				TIMEDURATA = 1
			end if 	
			if cint(TIMEDurata) < cint(durata) then
				Response.Write "<table border=0 cellspacing=2 cellpadding=1 width=500 align=center><tr><td align=center>"
'	Calcolo peso % delle risposte esatte rispetto al peso totale e lo confronto con la soglia
				
				if cint(((cdbl(Punt) / cdbl (iTotPeso)))*100) < cInt (Soglia) then%>
					<table border="0" cellspacing="2" cellpadding="1" width="500" align="center">
						<tr align="center">
							<td class="tbltext3"><b>Non hai superato il questionario</b></span></td>
						</tr>
						<tr align="center">
							<td class="tbltext3"><b>Punteggio realizzato <%=cint(((cdbl(Punt) / cdbl (iTotPeso)))*100)%>%</b></span></td>
						</tr>
					</table>
					<br>
					<%	
					tentativi = cint(tentativi) + 1	
					sql="UPDATE RISULT_QUEST SET DT_ESEC = " & ora & " , TEMPO_ESEC = " & TIMEDurata & ", NUM_PUNTI = " & PUNT & ", NUM_RISPESA = " & ES & ", FL_ESEGUITO = 'N', NUM_TENTATIVI= " & tentativi & " where ID_Questionario=" & IDQue & " and ID_PERSONA = " & IDP
			'		sErrore=Esegui(IDQUE ,"RISULT_QUEST",IDP,"MOD",SQL,1,dtTmst)
					sErrore=EseguiNoc(sql,CC)

					ToppaCore("F")

					%>			
					<table border="0" cellspacing="2" cellpadding="1" width="500" align="center">
						<tr align="center">
							<td class="tbltext3"><b>Devi riprovare</b></span></td>
						<tr>
					</table>
					<br>
						<table border="0" cellspacing="2" cellpadding="1" width="500" align="center">
							<tr align="center">
								<td>
									<a class="textred" href="Javascript:history.go(-2)" onmouseover="window.status =' '; return true"><b>Torna indietro</b></span></a>
								</td>
							</tr>
							</table>
						</td></tr></table>					
					<%		
					VisArgRis()	
					if sErrore="0" then
						CC.CommitTrans
					else 
						CC.RollBackTrans%>
						<table border="0" cellspacing="2" cellpadding="1" width="500" align="center">
						<tr align="center">
							<td span class="tblText1Fad"><b>Errore nell'inserimento. <%=sErrore%></b></span></td>
						<tr>
						</table>		
						<br><br>
						<!--table border="0" cellspacing="2" cellpadding="1" width="500" align="center">						<tr align="center">							<td><a class="tblText1Fad" href="javascript:history.back()" onmouseover="window.status =' '; return true"><b>Torna indietro</b></span></a></td>						</tr>						</table-->
						<br><br>
						<%
					end if 
				else
					tentativi = cint(tentativi) + 1		
					sql="UPDATE RISULT_QUEST SET DT_ESEC = " & ora & " , TEMPO_ESEC = " & TIMEDurata & ", NUM_PUNTI = " & PUNT & ", NUM_RISPESA = " & ES & ", FL_ESEGUITO = 'S', NUM_TENTATIVI= " & TENTATIVI & " where ID_QUESTIONARIO = " & IDQue & " and ID_PERSONA = " & IDP
'						sErrore=Esegui(IDQUE,"RISULT_QUEST",IDP,"MOD",SQL,1,dtTmst)
					sErrore=EseguiNoC(SQL,CC)
					IF sErrore = "0" then
					%>
						<table border="0" cellspacing="2" cellpadding="1" width="500" align="center">
							<tr align="center">
								<td class="tbltext3"><span class="size"><b>Questionario eseguito con successo</b></span></td>
							<tr>
						<!--																							<tr align=center>																<td class="tbltext"><span class="size"><b>Punteggio realizzato <%=cint(((cdbl(Punt) / cdbl (iTotPeso)))*100)%>%</b>						</span></td>									</tr>-->									
						</table>
						<%
						VisArgRis()
						
'----------------- TOPPA CONSUELO 04/02/2004 -------------------------------------------
						
						if QModo = 1 then
						
						'se il Qmodo � 1 il questionario � parte di un corso.
							ToppaCore("P")
						end if
						
'----------------- FINE TOPPA CONSUELO 04/02/2004 --------------------------------------						
						
						%>
						<br><br>
					<%else
						CC.RollBackTrans %>
						<table border="0" cellspacing="2" cellpadding="1" width="500" align="center">
						<tr align="center">
							<td span class="tbltext3"><span class="size"><b>Errore nell'aggiornamento. <%=sErrore%></b></span>
							</td>
						<tr>
						</table>		
						<br><br>
						<table border="0" cellspacing="2" cellpadding="1" width="500" align="center">
						<tr align="center">
							<td>
								<a class="bltext1" href="Javascript:history.go(-2)" onmouseover="window.status =' '; return true"><b>Torna indietro</b></span></a>
							</td>
						</tr>
						</table>
						<br><br>
					<%end if 					
							
'					if QModo = 1 then
'						Aggiorna_Percorso()
'					end if
					if sErrore="0" then
						CC.CommitTrans%>
							<table border="0" cellspacing="2" cellpadding="1" width="500" align="center">
								<tr align="center">
									<td>
										<a class="textred" href="Javascript:history.go(-2)" onmouseover="window.status =' '; return true"><b>Torna indietro</b></span></a>
									</td>
								</tr>
								</table>
							</td></tr></table>
					<%else 
						CC.RollBackTrans%>
						<table border="0" cellspacing="2" cellpadding="1" width="500" align="center">
						<tr align="center">
							<td span class="tbltext3"><b>Errore nell'inserimento. <%=sErrore%></b></span>
							</td>
						<tr>
						</table>		
						<br><br>
						<table border="0" cellspacing="2" cellpadding="1" width="500" align="center">
						<tr align="center">
							<td>
								<a class="textred" href="Javascript:history.go(-2)" onmouseover="window.status =' '; return true"><b>Torna indietro</b></span></a>
							</td>
						</tr>
						</table>
						<br><br>
					   <%
				   End If	
				End if
			else
				if QModo = 1 then
					'se il Qmodo � 1 il questionario � parte di un corso.
					ToppaCore("F")
				end if

				CC.CommitTrans
		%>
				<br><br><br>
				<table border="0" cellspacing="2" cellpadding="1" width="500" align="center">
				<tr align="center">
					<td span class="tblText1Fad"><span class="tblSfondoFad"><b>Il tempo a tua disposizione � scaduto!.</b></span>
					</td>
				<tr>
				</table>		
				<br><br>
				<table border="0" cellspacing="2" cellpadding="1" width="500" align="center">
				<tr align="center">
					<td>
				<%'IF IDQUE <> 100 THEN %>
					<!--a class="tblText1Fad" 						href="/pgm/Formazione/catalogo/ERO_PrimaPAgina.asp"><b>ripeti 						il questionario</b></span></a-->
				<%'ELSE%>
					<!--a class="tblText1Fad" 						href="/pgm/formazione/questionari/QUE_INTROBONUS.asp"><b>ripeti 						il questionario</b></span></a-->				
				<%'END IF%>
					</td>
				</tr>
				</table>
				<br><br>
<%			End if	
		End if
	else
		CC.RollBackTrans	
	end if
End Sub
%>

<!--------------------------------------------------------------------------------------------------------------------------------------------------------->

<!--	FINE BLOCCO ASP		-->

<!--		MAIN			-->


<!--#include Virtual ="/include/sysfunction.asp"-->
<!--#include Virtual ="/include/ControlDateVB.asp"-->
<!--#include Virtual ="/include/openconn.asp"-->
<!--#include Virtual ="/Util/DBUtil.asp"-->

<%

Dim sqlQ, NDom, codArea, Titolo, IDQue, nd, nr
Dim sSQL, nResQ, rsApp,IDP
dim nIdQuest
Dim TipoPagina, nApp
Const ConRisposte = 0
Const SenzaRisposte = 1
Const ConRisposte1 = 2
	
dim QModo

' IDP = Session("Creator")
IDP = Session("IdUtente")
	
MODO	= Request.form("modo")	
QModo   = Request.form("Qmodo")	
IDQue   = clng(Request.form("IDQue"))
nIdQuest = RisultQuest(IDQue)
NDom	= Request.form("NDom")
CodArea = Request.form("CodArea")	
TimeFine = now()

Titolo  = Request.Form("Titolo")
AREA    = Request.Form("Area")	
TimeInizio = Request.Form("TimeIni")	

ora = ConvDateToDB(now())

if IDP="" Then %>
	<br><br><br>
	<table border="0" cellspacing="2" cellpadding="1" width="500" align="center">
	<tr align="center">
		<td span class="tbltext3"><span class="tblSfondoFad"><b>Il tempo a tua disposizione � scaduto!.</b></span>
		</td>
	<tr>
	</table>		
	<br><br>
	<table border="0" cellspacing="2" cellpadding="1" width="500" align="center">
	<tr align="center">
		<td>
			<span class="bltext1"><a href="/copertina.htm" target="_top"><b>ripetere il login</b></span></a>
		</td>
	</tr>
	</table>
	<br><br>
	<%Response.End 
end if		
		
ES = 0
punt = 0
if NDOM = "" then NDOM = 10 end if
'if Titolo = "" then Titolo = "pgm/formazione/questionari/questionario Dinamico" end if
	
Inizio()

if QModo = 1 then	
	nElem = Request.Form("Elem")
	nIdCorso = Request.Form("IdCorso")
	dt_tmst = Request.Form("dt_tmst")
	nIdArgom = Request.Form("IdArgom")
	sCodCorso = Request.Form("CodCorso")
end if
	
ImpostaPag()

'rimposto il timeout
Session.Timeout = 20


'function RisultQuest(IDQue) 

'	sSQL = "SELECT ID_RISULT_QUEST FROM RISULT_QUEST WHERE id_questionario = " & IDQue & " AND ID_PERSONA = " & IDP

'	set rsIdRQuest = CC.Execute(sSQL)
'	if not rsIdRQuest.eof then
'		RisultQuest = rsIdRQuest("ID_RISULT_QUEST") 
'	end if
'	rsIdRQuest.close
'	set rsIdRQuest = nothing 	

'end function %>

<!--#include Virtual ="/include/closeconn.asp"-->
<!--#include Virtual="/strutt_coda2.asp"-->
