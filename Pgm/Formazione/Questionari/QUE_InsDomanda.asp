<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%
Sub ControlliJavaScript()
%>
	<script LANGUAGE="JavaScript">
		function ValidateIt(frmMe)
		{
		if(frmMe.Areadom.value == "" || frmMe.Areadom.value == " ")
		  {
		  frmMe.Areadom.focus();
		  alert("Inserire il testo della Domanda. ");
		  return false;
		  }
		 if(frmMe.NRisposte.value < 2 || frmMe.NRisposte.value > 100)
		  {
		  frmMe.NRisposte.focus();
		  alert("Inserire nel campo Numero Risposte un numero da 2 a 100. ");
		  return false;
		  } 
		 if(frmMe.NRisposte.value == "" || frmMe.NRisposte.value == " ")
		  {
		  frmMe.NRisposte.focus();
		  alert("Inserire il numero di Risposte.");
		  return false;
		  } 
		 if(frmMe.Valore.value == "" || frmMe.Valore.value == " ")
		  {
		  frmMe.Valore.focus();
		  alert("Inserire il Peso della domanda. ");
		  return false;
		  } 
  		if (isNaN(frmMe.Valore.value))
		  {
		  frmMe.Valore.focus();
		  alert("'Peso' deve essere un valore numerico.");
		  return false;
		  }		  
		if (isNaN(frmMe.NRisposte.value))
		  {
		  frmMe.NRisposte.focus();
		  alert("'Numero Risposte' deve essere un valore numerico.");
		  return false;
		  }		  

		return true;
		}

		var finestra
		function ListaImmagini(sImage)
		{
			if (finestra != null ) 
			{
				finestra.close(); 
			}
			f='QUE_VisImmagini.asp?img=' + sImage
			finestra=window.open(f,"","width=550,height=450,location=no,menubar=no,scrollbars=yes,resizable=no")
		}

		var finestraA
		function ListaAudio(sImage)
		{
			if (finestraA != null ) 
			{
				finestraA.close(); 
			}
			f='QUE_VisAudio.asp?img=' + sImage
			finestraA=window.open(f,"","width=550,height=450,location=no,menubar=no,scrollbars=yes,resizable=no")
		}		
	</script>
<%
End Sub
'--------------------------------------------------------------------------
Sub Inizio()
	dim sFunzione
	dim sTitolo
	dim sCommento
	dim bCampiObbl
	
	sFunzione = "GESTIONE QUESTIONARIO"
	sTitolo = "QUESTIONARIO"
	'sCommento = "Inserimento della<br>Domanda" 
	sCommento = "Inserimento domanda<br>Compilare i campi sottostanti e premere Invia per memorizzare le informazioni."
	bCampiObbl = true
	sHelp = "/Pgm/help/Formazione/Questionari/QUE_InsDomanda/"	
%>
	<!--#include virtual="include/setTestata.asp"-->
<%
End Sub
'--------------------------------------------------------------------------
Sub ImpostaPag()

set RR=server.CreateObject("ADODB.Recordset")
sql="SELECT NUM_DOM, TIT_QUESTIONARIO from QUESTIONARIO where ID_Questionario = " & IDQUE
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
RR.open sql, CC, 3
TIT = RR.FIELDS("TIT_QUESTIONARIO")
RR.CLOSE
%>
<table width="500" border="0">
	<tr>
		<td class="tbltext3">
			Test: &quot;<%=TIT%>&quot; 
		</td>
	</tr>
</table>
<%

   	set RR=server.CreateObject("ADODB.Recordset")
   	sql="SELECT Desc_Area_Dom, IMG_AREA_DOM from Area_Dom where ID_Area_Dom = " & IdArea
'		Response.Write sql
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
   	RR.open sql, CC, 3
   	TITarea = RR.FIELDS("Desc_Area_Dom")
   	IMGarea = RR.FIELDS("IMG_AREA_DOM")
   	RR.CLOSE

%>		
<table width="500" border="0">
	<% if TITarea > " " then %>
	<tr><td class="tbltext3">Testo Area: &quot;<%=Titarea%>&quot; </td></tr>
	<% end if
	   if IMGArea > " " then %>
	<tr><td class="tbltext3" valign="middle">Immagine Area: <br>
		<img SRC="<%=IMGArea%>">
	</td></tr>
	<% end if%>
</table>
			
<br>		
<form name="form1" id="form1" action="QUE_CnfInsDomanda.asp?modo=ins&amp;IDQUE=<%=IDQUE%>&amp;IDAREA=<%=IDAREA%>&amp;IDDOM=<%=IDDOM%>" method="POST" onsubmit="return ValidateIt(this);">
<table border="0" width="500" cellspacing="2" cellpadding="1"> 
	<tr> 
		<td class="tbltext1">
			<b>Domanda*</b>
		</td>
		<td>
			<input type="text" id="Areadom" name="Areadom" size="40" maxlength="250" value="<%=xtextdo%>" class="textblacka">
		</td>
	</tr>		
	<tr>
		<td class="tbltext1">
			<b>Immagine</b>
		</td>
		<td>
			<input name="txtMedia" id="txtMedia" class="textblacka">
			<a href="javascript:ListaImmagini('')">
				<img src="<%=Session("progetto")%>/images/bullet1.gif" border="0"><!-- onmouseover="javascript:window.status='' ; return true"-->
			</a>
		</td>
	</tr>
	<tr>
		<td class="tbltext1">
			<b>Audio</b>
		</td>
		<td>
			<input name="txtAudio" id="txtAudio" class="textblacka">
			<a href="javascript:ListaAudio('')">
				<img src="<%=Session("progetto")%>/images/bullet1.gif" border="0"><!-- onmouseover="javascript:window.status='' ; return true"-->
			</a>
		</td>
	</tr>
	<tr>
		<td class="tbltext1">
			<b>Num Risposte*</b>
		</td>
		<td>
			<input type="text" size="2" cols="2" name="NRisposte" id="NRisposte" maxlength="2" class="textblacka">
		</td>
	</tr>
	<tr>
		<td class="tbltext1">
			<b>Peso*</b>
		</td>
		<td>
			<input type="text" size="2" cols="2" name="Valore" id="Valore" maxlength="2" class="textblacka">
		</td>
	</tr>
	<tr> 
		<td colspan="3" align="center">&nbsp;</td>
	</tr>
</table>
<table width="500">
    <tr> 
      <td align="right">
		<%
			PlsIndietro()
		%>
      </td>
      <td align="left">
		<%
			PlsInvia("Conferma")
		%>
      </td>
    </tr>
</table>
</form>
<br>
<%
End Sub
'--------------------------------------------------------------------------------
'M a i n
%>
<!--#include virtual="/strutt_testa2.asp"-->
<!--#include virtual="/include/openconn.asp"-->
<!--#include virtual="/include/SetPulsanti.asp"-->
<%

	IdQue=Request.QueryString("IDQ")
	IdArea=Request.QueryString("IDA")
	Iddom=Request.QueryString("IDD")

	ControlliJavaScript()
	Inizio()
 	ImpostaPag()

%>
<!--#include virtual="/include/closeconn.asp"-->
<!--#include virtual="/strutt_coda2.asp"-->
