<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%
Sub ControlliJavaScript()
%>
	<script LANGUAGE="JavaScript">
		function validateIt()
		{
		if(document.form1.Areadom.value == "" || document.form1.Areadom.value == " ")
		  {
		  document.form1.Areadom.focus();
		  alert("Inserire il testo dell'Area.");
		  return false;
		  }
		if(document.form1.NDomande.value == "")
		  {
		  document.form1.NDomande.focus();
		  alert("Inserire il numero di domande per area.");
		  return false;
		  }
		if (isNaN(document.form1.NDomande.value))
		  {
		  document.form1.NDomande.focus();
		  alert("'Numero Domande' deve essere un valore numerico.");
		  return false;
		  }		  
		return true;
		}

		function validRisp(num)
		{
		var k;
		for (k=1; k<=num; ++k)
			{
				if(document.forms[0].Risposta+k.value == "" && document.form[0].Immagine+k.value == "")
				{
					alert("Inserire tutte le risposte richieste");
					return 1;
				}
			}	 
		document.form2.submit();
		return 1;
		}  
		var finestra
		function ListaImmagini(sImage)
		{
			if (finestra != null ) 
			{
				finestra.close(); 
			}
			f='QUE_VisImmagini.asp?img=' + sImage
			finestra=window.open(f,"","width=550,height=450,location=no,menubar=no,scrollbars=yes,resizable=no")
		}

		function ListaAudio()
		{
			f='QUE_BEListaAudio.asp' 
			finestra=window.open(f,"","width=650,height=450,location=no,menubar=no,scrollbars=yes,resizable=no")
		}
		//-->
	</script>
<%
End sub
'------------------------------------------------------------------
Sub Inizio()
	dim sFunzione
	dim sTitolo
	dim sCommento
	dim bCampiObbl
	
	sFunzione = "GESTIONE QUESTIONARIO"
	sTitolo = "QUESTIONARIO"
	sCommento = "Inserimento dell'area"
	bCampiObbl = true
	sHelp = "/Pgm/help/Formazione/Questionari/QUE_InsArea/"	
%>
	<!--#include virtual="include/SetTestata.asp"-->
<%
End Sub
'---------------------------------------------------------------------
Sub ImpostaPag()
	
		set RR=server.CreateObject("ADODB.Recordset")
		sql="SELECT TIT_QUESTIONARIO from QUESTIONARIO where ID_Questionario = " & IDQUE
 '		Response.Write sql
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
		RR.open sql, CC, 3
		TIT = RR.FIELDS("TIT_QUESTIONARIO")
'		Response.Write nQST
		RR.CLOSE

%>
	<table width="500" border="0">
		<tr><td class="tbltext3">Test: '<%=TIT%>'</td></tr>
	</table>		
	<br>		
	<form action="QUE_CnfInsArea.asp" method="POST" id="form1" name="form1" onsubmit="return validateIt(this);">
	<input type="hidden" name="IDQUE" id="IDQUE" value="<%=IDQUE%>">
	<input type="hidden" name="IDAREA" id="IDAREA" value="<%=IDAREA%>">
	<input type="hidden" name="DTTMST" id="DTTMST" value="<%=DTTMST%>">
		<table border="0" width="500" cellspacing="2" cellpadding="1"> 
			<tr>
				<td class="tbltext1">
					<b>Area Domande*</b>
				</td>
				<td>
					<input type="text" id="Areadom" name="Areadom" size="40" maxlength="250" value="<%=xdescar%>" class="textblacka">
				</td>
			</tr>
		    <tr>
				<td class="tbltext1">
					<b>Immagine</b>
				</td>
				<td>
					<input name="txtMedia" id="txtMedia" value="<%=ximagea%>" class="textblacka" readonly>
					<span class="tbltext">
						<a href="javascript:ListaImmagini('<%=ximagea%>')">
							<img src="<%=Session("progetto")%>/images/bullet1.gif" border="0"><!-- onmouseover="javascript:window.status='' ; return true"-->
						</a>
					</span>
				</td>
		    </tr>
		    <tr>
				<td class="tbltext1">
					<b>Numero Domande*</b>
				</td>
				<td>
					<input type="text" size="2" name="NDomande" id="NDomande" class="textblacka" maxlength="2" value="<%=xnumdom%>">
				</td>
		    </tr>
			<tr> 
				<td align="center">&nbsp;</td>
			</tr>
		</table>
		<table width="500">
		    <tr> 
				<td align="right">
				<%
					PlsIndietro()
				%>
				</td>
				<td align="left">
				<%
					PlsInvia("Conferma")
				%>
				</td>
		    </tr>		
		</table>
	</form>
<%
End Sub
'------------------------------------------------------------------
'M A I N 
%>
<!--#include virtual="/strutt_testa2.asp"-->
<!--#include virtual="/include/openconn.asp"-->
<!--#include virtual="/include/SetPulsanti.asp"-->
<%

	IdQue=Request.QueryString("IDQ")
	IdArea=Request.QueryString("IDA")
	
	ControlliJavaScript()
	Inizio()
 	ImpostaPag()

%>
<!--#include virtual="/include/closeconn.asp"-->
<!--#include virtual="/strutt_coda2.asp"-->
