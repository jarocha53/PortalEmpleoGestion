<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%
Sub Inizio()

	dim sFunzione
	dim sTitolo
	dim sCommento
	dim bCampiObbl
	
	sFunzione = "GESTIONE QUESTIONARIO"
	sTitolo = "QUESTIONARIO" 
	'sCommento = "Elenco delle aree del<br>Questionario"
	sCommento = "Elenco aree<br>Cliccando sul titolo � possibile modificare le caratteristiche dell'area. L'icona della colonna Immagine permette di visualizzare un'anteprima dell'immagine. L'icona della colonna Domanda visualizza l'elenco delle domande dell'area. Il link Inserisci Area mancante permette di inserire le aree mancanti." 
	bCampiObbl = false
	sHelp = "/Pgm/help/Formazione/Questionari/QUE_VisArea/"
%>
	<!--#include virtual="include/setTestata.asp"-->
<%
End Sub
'---------------------------------------------------------------------------
Sub ImpostaPag()

dim sSQL, nIdQuest
dim rsArea, rsQuest

nIdQuest = request("id")

sSQL = "Select TIT_QUESTIONARIO, NUM_DOM from QUESTIONARIO where id_QUESTIONARIO = " & nIdQuest
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
set rsQuest = cc.execute(sSQL)
if not rsQuest.eof then
%>
	<table width="500" border="0">
		<tr>
			<td class="tbltext3">Test: '<%=rsQuest("TIT_QUESTIONARIO")%>'</td></tr>
	</table>
	<br>
<%	
naree = rsQuest("NUM_DOM")
end if
rsQuest.close


sSQL = "select ID_AREA_DOM, DESC_AREA_DOM, IMG_AREA_DOM from AREA_DOM where ID_QUESTIONARIO = " & nIdQuest & " order by ID_AREA_DOM"
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
set rsArea = CC.execute(sSQL)
%>
<table border="0" cellspacing="2" cellpadding="1" width="500">
	<tr class="sfondocomm">
		<td width="20" align="center"><b>#</b></td>
		<td><b>Descrizione</b></td>
		<td width="50"><b>Immagine</b></td>
		<td width="50"><b>Domande</b></td>
	</tr>

<%
c= 1
do while not rsArea.eof
%>
	<tr class="tblsfondo">
		<td class="tbldett"><%=c%></td>
		<!--td class=tbllabel1 align=center width="60">Elimina</a></td-->
		<td class="tbldett">
			<a href="QUE_ModArea.asp?ida=<%=rsArea("ID_AREA_DOM")%>&amp;idq=<%=nIdQuest%>" class="tblAgg"><%=rsArea("DESC_AREA_DOM")%></a>
		</td>		
		<td align="center">
		<% if rsArea("IMG_AREA_DOM") > " " then %>
			<a href="<%=rsArea("IMG_AREA_DOM")%>" target="_blank">
				<img src="<%=Session("progetto")%>/images/icons2/gif.gif" border="0" alt="Anteprima Immagine">
			</a>
		<% end if %> 
		</td>
		<td align="center">
			<a href="QUE_VisDomanda.asp?ida=<%=rsArea("ID_AREA_DOM")%>&amp;idq=<%=nIdQuest%>" class="tblAgg">
				<img src="<%=Session("progetto")%>/images/re.gif" border="0" alt="Elenco delle Domande">
			</a>
		</td>
	</tr>	
<%
	rsArea.movenext
	c = c + 1 
loop
'+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-
'Riga di chiusura
'+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-
%>
<tr><td>&nbsp;</td></tr>
</table>

<table width="500" border="0" cellpadding="2" cellspacing="3">
	<tr>
<% if cint(naree) > cint(c-1) then %> 

		<td align="middle">
			<%
			PlsLinkRosso "QUE_InsArea.asp?idq=" &nIdQuest, "Inserisci Area mancante"
			%>
		</td>
<% end if
rsArea.close
%>
		<td align="center">
			<%
			PlsLinkRosso "QUE_VisQuestionari.asp", "Elenco Questionari"
			%>
		</td>
	</tr>
</table>

<%
End Sub
'--------------------------------------------------------------------------------
'M A I N
%>
<!--#include virtual="/strutt_testa2.asp"-->
<!--#include virtual="/include/openconn.asp"-->
<!--#include virtual="/include/SetPulsanti.asp"-->
<%	
	Inizio()
	ImpostaPag()
%>			
<!--#include virtual="/include/closeconn.asp"-->
<!--#include virtual="/strutt_coda2.asp"-->
