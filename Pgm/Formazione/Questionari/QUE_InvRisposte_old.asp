<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include Virtual="/strutt_testa2.asp"-->
<!--#include virtual ="/util/portallib.asp"-->
<!--#include virtual="include/SetTestataFAD.asp"-->

<script language="javascript">
	function ArgRif(nIdQuest) {
		win = window.open ('/Pgm/formazione/questionari/riferimenti/QRI_argrif.asp?inW=Yes&ResQ=' + nIdQuest,'info','width=550,height=350,Resize=No,Scrollbars=yes,screenX=w,screenY=h')
	}

</script>
<% 
'	Response.Buffer = true
'	Response.ExpiresAbsolute = Now() - 1 
'	Response.AddHeader "pragma","no-cache"
'	Response.AddHeader "cache-control","private"
'	Response.CacheControl = "no-cache"

	Sub Inizio()
		dim sTitolo
		dim sCommento
	
		sTitolo = "Questionario"
		sCommento = "In questa pagina puoi visualizzare i risultati del questionario"
		sHelp = ""
		call TestataFAD(sTitolo, sCommento, false, sHelp)

	End Sub

	Sub Aggiorna_Percorso()
		Dim sqlP
		
		sqlP = "Update PERCORSO  set dt_fine =" & ConvDateToDB(now()) & ", dt_tmst=" & ConvDateToDB(now())   
		sqlP = sqlP & " Where ID_PERSONA =" & IDP & " And ID_ELEMENTO =" & nElem & " And ID_CORSO=" & nIDCorso 	
		sOpe = "MOD"
		
'		sErrore=Esegui(nElem,"PERCORSO", Session("IdPersona"),sOpe,sqlP,1,dt_Tmst)
		sErrore = EseguiNoC(sqlP,CC)
	End Sub

'----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------'----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

sub ToppaCore(valore)

'++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
'sub aggiunta in data 04/02/2004 per il tracciamento dei questionari di valutazione +
'ASSOLUTAMENTE DA RIVEDERE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!                        +
'++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	 						
	sSQL = "SELECT ELE_PREC FROM REGOLA_DIPENDENZA WHERE ID_CORSO = " & nIDCorso & _
			" AND ID_ELEMENTO = " & nElem
	set rsRegDipen = Server.CreateObject("ADODB.Recordset")
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	rsRegDipen.Open sSQL, CC, 3
		
	apStatoLezione = valore				
	
	'if not rsRegDipen.eof then
	'	if rsRegDipen("ELE_PREC") <> "" then
	'		sModuloPrecedente = clng(rsRegDipen("ELE_PREC"))
	'						
	'		rsRegDipen.Close
	'									
	'		sSQL = "SELECT STATO_LEZIONE " & _
	'				" FROM REGOLA_DIPENDENZA WHERE ID_CORSO = " & nIDCorso & _
	'				" AND ID_ELEMENTO=" & sModuloPrecedente
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	'		rsRegDipen.Open sSQL, CC, 3
	'		if not rsRegDipen.eof then
	'			apStatoLezione = rsRegDipen("STATO_LEZIONE")
	'		else
	'			apStatoLezione = "NULL"
	'		end if
	'	end if
	'end if

	sSSQL = "SELECT NOME || ' ' || COGNOME as CN FROM UTENTE WHERE IDUTENTE = " & Session("idUtente")
	set rs= Server.CreateObject("ADODB.Recordset")
'PL-SQL * T-SQL  
SSSQL = TransformPLSQLToTSQL (SSSQL) 
	rs.Open sSSQL, CC, 3
	sCognNome = rs("CN")
	sCognNome = Replace(sCognNome,"'", "''")
	rs.CLOSE 

	
	sSSQL = "SELECT ID_CORSOAICC FROM CORSO WHERE ID_CORSO = " & nIDCorso
	set rs= Server.CreateObject("ADODB.Recordset")
'PL-SQL * T-SQL  
SSSQL = TransformPLSQLToTSQL (SSSQL) 
	rs.Open sSSQL, CC, 3
	sIdCorsoAicc = rs("ID_CORSOAICC")
	rs.CLOSE 
	
	sSSQL = "SELECT ID_ELEMENTOAICC FROM ELEMENTO WHERE ID_ELEMENTO = " & nElem
'PL-SQL * T-SQL  
SSSQL = TransformPLSQLToTSQL (SSSQL) 
	rs.Open sSSQL, CC, 3
	sIdElementoAicc = rs("ID_ELEMENTOAICC")
	rs.CLOSE 
	
	sSQLDel = "DELETE FROM CORE " &_
				"WHERE STUDENT_ID ='" &	Session("Idutente") & "'" & _
				"AND LESSON_ID ='" & sIdElementoAicc & "'" & _
				"AND COURSE_ID ='" & sIdCorsoAicc & "'"
	sErrore = EseguiNoC(sSQLDel, CC)

	sSQL = "SELECT ATTEMPT_NUMBER FROM STUDENT_DATA " & _
			" WHERE STUDENT_ID = '" & Session("idUtente") & "'" & _
			" and COURSE_ID = '" & sIdCorsoAicc & "'" & _
			" and LESSON_ID = '" & sIdElementoAicc & "'"
		
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	rs.Open sSQL, CC, 3
	if rs.eof then
		sAttempt = 1
	else	
		on error resume next
		sAttempt = rs("ATTEMPT_NUMBER")
		if isnull(sAttempt) then
			sAttempt = 1
		else 
			sAttempt = 1 + CLng(sAttempt)	
		end if
		on error goto 0
	end if	
	rs.CLOSE 
	set rs = nothing

	sSQLDel = "DELETE FROM STUDENT_DATA " &_
				"WHERE STUDENT_ID ='" &	Session("Idutente") & "'" & _
				"AND LESSON_ID ='" & sIdElementoAicc & "'" & _
				"AND COURSE_ID ='" & sIdCorsoAicc & "'"
	sErrore = EseguiNoC(sSQLDel, CC)

	sSQLData = "INSERT INTO STUDENT_DATA " & _
				"(LESSON_ID,STUDENT_ID,COURSE_ID," & _
				"LESSON_STATUS,TRY_TIME, ATTEMPT_NUMBER) VALUES (" & _
				"'" & sIdElementoAicc & "'," & _
				"'" & Session("idUtente") & "'," & _
				"'" & sIdCorsoAicc & "'," & _
				"'" & apStatoLezione & _
				"','00:00:00', " & sAttempt & ")"

									
	sErrore = EseguiNoC(sSQLData,CC)
	
	
	
	if sErrore = "0" then
		sSQLCore = "INSERT INTO CORE " & _
					"(LESSON_ID,STUDENT_ID,COURSE_ID," & _
					"LESSON_STATUS,CORE_TIME, STUDENT_NAME) VALUES (" & _
					"'" & sIdElementoAicc & "'," & _
					"'" & Session("idUtente") & "'," & _
					"'" & sIdCorsoAicc & "'," & _
					"'" & apStatoLezione & _
					"','00:00:00', '" & sCognNome & "')"

		sErrore  = EseguiNoC(sSQLCore,CC)
		
	end if


end sub

'----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Sub ImpostaPag()
	dim iTotPeso
	dim sErrore
		
	CC.BeginTrans
	iTotPeso = 0
	sSQL = "DELETE FROM RISULT_RISP WHERE ID_RISULT_QUEST = " & nIdQuest
	sErrore=EseguiNoC(sSQL,CC)
	if sErrore = "0" then
		for i = 1 to NDOM 
			'	D = Domanda
			'	R = Risposta
			'	E = Esatta
			'	P = Peso	
			ND = "DOM" & i
			ND = Request.Form(ND)
			D = cint(InStr(1,ND,"D")) + 1
			R = cint(InStr(1,ND,"R")) + 1
			E = cint(InStr(1,ND,"E")) + 1
			P = cint(InStr(1,ND,"P")) + 1
				
			LunD = cint((R - D)) - 1
			Dom = Mid(ND,D,LunD)
			LunR = cint((E - R)) - 1 
			Ris = Mid(ND,R,LunR)
			Esa = Mid(ND,E,1)
			Pes = clng(Mid(ND,P))
			If Esa = "S" then
				punt = punt + Pes
				ES = cint(ES) + 1
			end if 	
			'	Calcolo peso totale delle domande	
			iTotPeso = iTotPeso + Pes
			sSQLInsRisposte = "INSERT INTO RISULT_RISP (ID_RISULT_QUEST,ID_DOMANDA,ID_RISPOSTA) " &_				
				" VALUES(" & nIdQuest & "," & Dom & "," & Ris & ") "
			'Response.Write sSQLInsRisposte
			sErrore = EseguiNoc(sSQLInsRisposte,CC)
		next

		set RR=Server.CreateObject("ADODB.Recordset")
	   	sql="SELECT distinct TEM_QUESTIONARIO, SGL_QUESTIONARIO , FL_REPEAT FROM QUESTIONARIO where ID_QUESTIONARIO = " & IDQue
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
		RR.open sql, CC, 3
		If not RR.EOF then
			Durata = (RR.Fields("TEM_QUESTIONARIO")) 
			Soglia = (RR.Fields("SGL_QUESTIONARIO"))
			bRipetibile =  CINT(RR.Fields("FL_REPEAT"))
		end if
		RR.CLOSE
		set RR=Server.CreateObject("ADODB.Recordset")
	   	sql="SELECT NUM_TENTATIVI, FL_ESEGUITO, DT_ESEC, DT_TMST FROM RISULT_QUEST where ID_QUESTIONARIO = " & IDQue & " and ID_PERSONA = " & IDP
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
		RR.open sql, CC, 3
		If not RR.EOF then
			eseguito = (RR.Fields("FL_ESEGUITO"))
			tentativi = (RR.Fields("NUM_TENTATIVI")) 
			dataesec = (RR.Fields("DT_ESEC"))  
			DtTmst = (RR.Fields("DT_TMST"))  
		end if
		RR.CLOSE
		TIMEDurata = int((datediff("s",TimeInizio,TimeFine)) / 60 )
		if eseguito = "S" and bRipetibile = 0 then
			CC.CommitTrans
			DataEsec = ConvDateToString(DataEsec)%>
			<br><br><table border="0" cellspacing="2" cellpadding="1" width="500" align="center">
				<tr align="center">
					<td class="tbltext3"><b>Questionario gi� eseguito il giorno <%=dataesec%></b></span></td>
				<tr>
			</table><br><br>
			<table border="0" cellspacing="2" cellpadding="1" width="500" align="center">
				<tr align="center">
					<td><b><a class="textred" href="Javascript:history.go(-2)">Torna all'inizio</a></b></td>					
				</tr>
			</table><br><br>
		<%else
			if TIMEDURATA > 0 and TIMEDURATA < 1 then
				TIMEDURATA = 1
			end if 	
			if cint(TIMEDurata) < cint(durata) then
				Response.Write "<table border=0 cellspacing=2 cellpadding=1 width=500 align=center><tr><td align=center>"
'	Calcolo peso % delle risposte esatte rispetto al peso totale e lo confronto con la soglia
				
				if cint(((cdbl(Punt) / cdbl (iTotPeso)))*100) < cInt (Soglia) then%>
					<table border="0" cellspacing="2" cellpadding="1" width="500" align="center">
						<tr align="center">
							<td class="tbltext3"><b>Non hai superato il questionario</b></span></td>
						</tr>
						<tr align="center">
							<td class="tbltext3"><b>Punteggio realizzato <%=cint(((cdbl(Punt) / cdbl (iTotPeso)))*100)%>%</b></span></td>
						</tr>
					</table>
					<br>
					<%	
					tentativi = cint(tentativi) + 1	
					sql="UPDATE RISULT_QUEST SET DT_ESEC = " & ora & " , TEMPO_ESEC = " & TIMEDurata & ", NUM_PUNTI = " & PUNT & ", NUM_RISPESA = " & ES & ", FL_ESEGUITO = 'N', NUM_TENTATIVI= " & tentativi & " where ID_Questionario=" & IDQue & " and ID_PERSONA = " & IDP
			'		sErrore=Esegui(IDQUE ,"RISULT_QUEST",IDP,"MOD",SQL,1,dtTmst)
					sErrore=EseguiNoc(sql,CC)

					ToppaCore("F")

					%>			
					<table border="0" cellspacing="2" cellpadding="1" width="500" align="center">
						<tr align="center">
							<td class="tbltext3"><b>Devi riprovare</b></span></td>
						<tr>
					</table>
					<br>
						<table border="0" cellspacing="2" cellpadding="1" width="500" align="center">
							<tr align="center">
								<td>
									<a class="textred" href="Javascript:history.go(-2)" onmouseover="window.status =' '; return true"><b>Torna indietro</b></span></a>
								</td>
							</tr>
							</table>
						</td></tr></table>					
					<%		
					VisArgRis()	
					if sErrore="0" then
						CC.CommitTrans
					else 
						CC.RollBackTrans%>
						<table border="0" cellspacing="2" cellpadding="1" width="500" align="center">
						<tr align="center">
							<td span class="tblText1Fad"><b>Errore nell'inserimento. <%=sErrore%></b></span></td>
						<tr>
						</table>		
						<br><br>
						<!--table border="0" cellspacing="2" cellpadding="1" width="500" align="center">						<tr align="center">							<td><a class="tblText1Fad" href="javascript:history.back()" onmouseover="window.status =' '; return true"><b>Torna indietro</b></span></a></td>						</tr>						</table-->
						<br><br>
						<%
					end if 
				else
					tentativi = cint(tentativi) + 1		
					sql="UPDATE RISULT_QUEST SET DT_ESEC = " & ora & " , TEMPO_ESEC = " & TIMEDurata & ", NUM_PUNTI = " & PUNT & ", NUM_RISPESA = " & ES & ", FL_ESEGUITO = 'S', NUM_TENTATIVI= " & TENTATIVI & " where ID_QUESTIONARIO = " & IDQue & " and ID_PERSONA = " & IDP
'						sErrore=Esegui(IDQUE,"RISULT_QUEST",IDP,"MOD",SQL,1,dtTmst)
					sErrore=EseguiNoC(SQL,CC)
					IF sErrore = "0" then
					%>
						<table border="0" cellspacing="2" cellpadding="1" width="500" align="center">
							<tr align="center">
								<td class="tbltext3"><span class="size"><b>Questionario eseguito con successo</b></span></td>
							<tr>
						<!--																							<tr align=center>																<td class="tbltext"><span class="size"><b>Punteggio realizzato <%=cint(((cdbl(Punt) / cdbl (iTotPeso)))*100)%>%</b>						</span></td>									</tr>-->									
						</table>
						<%
						VisArgRis()
						
'----------------- TOPPA CONSUELO 04/02/2004 -------------------------------------------
						
						if QModo = 1 then
						
						'se il Qmodo � 1 il questionario � parte di un corso.
							ToppaCore("P")
						end if
						
'----------------- FINE TOPPA CONSUELO 04/02/2004 --------------------------------------						
						
						%>
						<br><br>
					<%else
						CC.RollBackTrans %>
						<table border="0" cellspacing="2" cellpadding="1" width="500" align="center">
						<tr align="center">
							<td span class="tbltext3"><span class="size"><b>Errore nell'aggiornamento. <%=sErrore%></b></span>
							</td>
						<tr>
						</table>		
						<br><br>
						<table border="0" cellspacing="2" cellpadding="1" width="500" align="center">
						<tr align="center">
							<td>
								<a class="bltext1" href="Javascript:history.go(-2)" onmouseover="window.status =' '; return true"><b>Torna indietro</b></span></a>
							</td>
						</tr>
						</table>
						<br><br>
					<%end if 					
							
'					if QModo = 1 then
'						Aggiorna_Percorso()
'					end if
					if sErrore="0" then
						CC.CommitTrans%>
							<table border="0" cellspacing="2" cellpadding="1" width="500" align="center">
								<tr align="center">
									<td>
										<a class="textred" href="Javascript:history.go(-2)" onmouseover="window.status =' '; return true"><b>Torna indietro</b></span></a>
									</td>
								</tr>
								</table>
							</td></tr></table>
					<%else 
						CC.RollBackTrans%>
						<table border="0" cellspacing="2" cellpadding="1" width="500" align="center">
						<tr align="center">
							<td span class="tbltext3"><b>Errore nell'inserimento. <%=sErrore%></b></span>
							</td>
						<tr>
						</table>		
						<br><br>
						<table border="0" cellspacing="2" cellpadding="1" width="500" align="center">
						<tr align="center">
							<td>
								<a class="textred" href="Javascript:history.go(-2)" onmouseover="window.status =' '; return true"><b>Torna indietro</b></span></a>
							</td>
						</tr>
						</table>
						<br><br>
					   <%
				   End If	
				End if
			else
				ToppaCore("F")

				CC.CommitTrans
		%>
				<br><br><br>
				<table border="0" cellspacing="2" cellpadding="1" width="500" align="center">
				<tr align="center">
					<td span class="tblText1Fad"><span class="tblSfondoFad"><b>Il tempo a tua disposizione � scaduto!.</b></span>
					</td>
				<tr>
				</table>		
				<br><br>
				<table border="0" cellspacing="2" cellpadding="1" width="500" align="center">
				<tr align="center">
					<td>
				<%'IF IDQUE <> 100 THEN %>
					<!--a class="tblText1Fad" 						href="/pgm/Formazione/catalogo/ERO_PrimaPAgina.asp"><b>ripeti 						il questionario</b></span></a-->
				<%'ELSE%>
					<!--a class="tblText1Fad" 						href="/pgm/formazione/questionari/QUE_INTROBONUS.asp"><b>ripeti 						il questionario</b></span></a-->				
				<%'END IF%>
					</td>
				</tr>
				</table>
				<br><br>
<%			End if	
		End if
	else
		CC.RollBackTrans	
	end if
End Sub
%>

<!--------------------------------------------------------------------------------------------------------------------------------------------------------->

<!--	FINE BLOCCO ASP		-->

<!--		MAIN			-->


<!--#include Virtual ="/include/sysfunction.asp"-->
<!--#include Virtual ="/include/ControlDateVB.asp"-->
<!--#include Virtual ="/include/openconn.asp"-->
<!--#include Virtual ="/Util/DBUtil.asp"-->

<%

Dim sqlQ, NDom, codArea, Titolo, IDQue, nd, nr
Dim sSQL, nResQ, rsApp,IDP
dim nIdQuest
Dim TipoPagina, nApp
Const ConRisposte = 0
Const SenzaRisposte = 1
Const ConRisposte1 = 2
	
dim QModo

' IDP = Session("Creator")
IDP = Session("IdUtente")
	
MODO	= Request.form("modo")	
QModo   = Request.form("Qmodo")	
IDQue   = clng(Request.form("IDQue"))
nIdQuest = RisultQuest(IDQue)
NDom	= Request.form("NDom")
CodArea = Request.form("CodArea")	
TimeFine = now()

Titolo  = Request.Form("Titolo")
AREA    = Request.Form("Area")	
TimeInizio = Request.Form("TimeIni")	

ora = ConvDateToDB(now())

if IDP="" Then %>
	<br><br><br>
	<table border="0" cellspacing="2" cellpadding="1" width="500" align="center">
	<tr align="center">
		<td span class="tbltext3"><span class="tblSfondoFad"><b>Il tempo a tua disposizione � scaduto!.</b></span>
		</td>
	<tr>
	</table>		
	<br><br>
	<table border="0" cellspacing="2" cellpadding="1" width="500" align="center">
	<tr align="center">
		<td>
			<span class="bltext1"><a href="/copertina.htm" target="_top"><b>ripetere il login</b></span></a>
		</td>
	</tr>
	</table>
	<br><br>
	<%Response.End 
end if		
		
ES = 0
punt = 0
if NDOM = "" then NDOM = 10 end if
'if Titolo = "" then Titolo = "pgm/formazione/questionari/questionario Dinamico" end if
	
Inizio()

if Request.Form("Qmodo") = 1 then	
	nElem = Request.Form("Elem")
	nIdCorso = Request.Form("IdCorso")
	dt_tmst = Request.Form("dt_tmst")
	nIdArgom = Request.Form("IdArgom")
	sCodCorso = Request.Form("CodCorso")
end if
	
ImpostaPag()

'rimposto il timeout
Session.Timeout = 20

sub ConfDomande()

	set RRW=server.CreateObject("ADODB.Recordset")
'	sqlw="SELECT distinct ID_AREA_DOM, DESC_AREA_DOM, IMG_AREA_DOM, ID_QUESTIONARIO, AUD_AREA_DOM from AREA_DOM where ID_QUESTIONARIO = " & IDQue & " order by ID_AREA_DOM "
	sqlw="SELECT distinct ID_AREA_DOM, DESC_AREA_DOM, IMG_AREA_DOM, ID_QUESTIONARIO from AREA_DOM where ID_QUESTIONARIO = " & IDQue & " order by ID_AREA_DOM "

'PL-SQL * T-SQL  
SQLW = TransformPLSQLToTSQL (SQLW) 
	RRW.open sqlw, CC, 3
	If RRW.EOF then 
		%>
		<table border="0" cellspacing="2" cellpadding="1" width="500px">
			<tr>
				<td align="center">
					<span class="tbltext3"><span class="size">
					<b>Non sono presenti Aree</b>
					</span></span>
				</td>
			</tr>
		</table>
		<%	
   else
		idn = 1
		Do While not RRW.Eof
		
			IDAREADOM  = RRW.Fields("ID_AREA_DOM")
			DESCAREADOM  = RRW.Fields("DESC_AREA_DOM")
			IMGAREADOM  = RRW.Fields("IMG_AREA_DOM")
			'AUDAREADOM  = RRW.Fields(4)					
			if cint(IDAREADOM) <> cint(MEMOAREADOM) then

%>				<br>
				<br>
				<table border="0" cellspacing="0" cellpadding="0" width="500px">
					<tr>
				        <!--td align=left width="23"><IMG src="../../../images/angolosin.jpg" border=0></td-->
						<td align="left" class="sfondocomm" width="100%"><b><%=DescAreaDom%></td>
					</tr>
					<%if IMGAREADOM > " " then
					%>		
						<tr>
						    <!--td align=left class="tblSfondoFad" width="23">&nbsp</td-->
							<td align="left" width="100%"><img src="/pgm/formazione/questionari/<%=IMGAreaDom%>" border="0"></td>
						</tr>
					<%end if
					if AUDAREADOM > " " then
					%>
					<tr>
						<td align="left" class="tblSfondoFad" colspan="2"><a href="/pgm/formazione/questionari/<%=AUDAREADOM%>">
							<span class="tblText1Fad"><span class="size10">
							<img SRC="../audio.gif" border="0">ascolta l'audio</span></a>
						</td>
					</tr>
					<%end if%>
					
				</table>
<%			end if
			set RR=server.CreateObject("ADODB.Recordset")
			sql="SELECT distinct TXT_DOMANDA, NUM_RISPOSTE, ID_DOMANDA, ID_QUESTIONARIO, PESO_DOMANDA, IMG_DOMANDA, AUD_DOMANDA, ID_AREA_DOM from DOMANDA where ID_QUESTIONARIO = " & IDQue & " and ID_AREA_DOM = " & IDAREADOM
			sql=sql & " ORDER BY ID_DOMANDA"
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
			RR.open sql, CC, 3
			If RR.EOF then %>
				<table border="0" cellspacing="2" cellpadding="1" width="500px">
					<tr>
						<td align="center">
							<span class="tbltext3"><span class="size">
							<b>Non sono presenti domande</b>
							</span></span>
						</td>
					</tr>
				</table>
			<% else
				RR.MoveFirst
				nd = 1
				NDOM = NDOM * 1
				Do While not RR.Eof
					IDDM  = RR.Fields(2)
					TITL  = RR.Fields(0)
					NRip  = RR.Fields(1) 
					PESO  = RR.Fields(4)
					IMG   = RR.Fields(5)
					AUDIO = RR.Fields(6)%>
					<table border="0" cellspacing="2" cellpadding="1" width="500px">
						<!--tr height="23" class="sfondocommFad"-->
						<tr height="23">
							<td align="right" width="22" class="tblText1Fad">
								<b><%=idn%></b>
							</td>
							<td class="tblText1Fad">
								 <b><%=TITL%></b>
							</td>
						<%if TipoPagina = SenzaRisposte then

							sSQL =	"SELECT RISPOSTA.FL_ESATTO FROM RISPOSTA, RISULT_RISP " &_
									"WHERE RISPOSTA.ID_DOMANDA = RISULT_RISP.ID_DOMANDA AND " &_
									"RISPOSTA.ID_RISPOSTA = RISULT_RISP.ID_RISPOSTA AND " &_
									"RISULT_RISP.ID_RISULT_QUEST = " & nResQ & " AND " &_
									"RISPOSTA.ID_DOMANDA = " & IDDM
							Response.Write sSQL		
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
							set rsRisp = cc.execute(sSQL)
							if not rsRisp.eof then
								if rsRisp("FL_ESATTO") = "S" then%>
									<td class="tblText1Fad" width="25" align="center">
										<img src="<%=session("progetto")%>/images/okv.gif" border="0" WIDTH="17" HEIGHT="15">
									</td>						
								<%else%>
										<td class="tblText1Fad" width="25" align="center">
											<img src="<%=session("progetto")%>/images/ko.gif" border="0" WIDTH="17" HEIGHT="15">
										</td>						
								<%end if
							end if
							rsRisp.close
						end if
						%>
						</tr>
					</table>
					<%if IMG > " " then%>
						<table border="0" cellspacing="2" cellpadding="1" width="500px">
							<tr>
								<td align="left" class="sfondocommFad">
									<img SRC="/Pgm/Formazione/Questionari/<%=IMG%>">
								</td>
							</tr>
						</table>
					<%end if
					if Audio > " " then	%>
						<table border="0" cellspacing="2" cellpadding="1" width="500px">
							<tr>
								<td align="left" class="tblSfondoFad">
									<a href="../<%=Audio%>">
										<span class="tblText1Fad"><span class="size10">
											<img SRC="../audio.gif" border="0">
											ascolta l'audio
										</span>
									</a>
								</td>
							</tr>
						</table>
					<% end if

					if TipoPagina = ConRisposte then
						set RRR=server.CreateObject("ADODB.Recordset")
						sql2="SELECT distinct TXT_RISPOSTA, ID_RISPOSTA, ID_DOMANDA, FL_ESATTO, IMG_RISPOSTA from RISPOSTA where ID_DOMANDA = '" & IDDM & "' order by ID_DOMANDA, ID_RISPOSTA "
'PL-SQL * T-SQL  
SQL2 = TransformPLSQLToTSQL (SQL2) 
						RRR.open sql2, CC, 3
						If RRR.EOF then 
							Response.Write "Non sono presenti risposte" 
						else
							RRR.MoveFirst%>
							<table border="0" cellspacing="2" cellpadding="1" width="500px" bordercolor="#dddddd">
							<%nrr = 0
							Do While not RRR.Eof
								TST  = RRR.Fields(0)
								IDRS = RRR.Fields(1)
								ESAT = RRR.Fields(3)
								IMAG = RRR.Fields(4)
								IDDO = RRR("ID_DOMANDA")%>	
								<tr>
									<td class="tblSfondoFad" width="22" align="center" height="23">
										<span class="tbltext">
										<%
										sSQL =	"SELECT id_risposta FROM risult_risp where " &_
												"risult_risp.ID_RISULT_QUEST = " & nResQ & " and " &_
												"risult_risp.ID_DOMANDA = " & IDDO & " and " &_
												"risult_risp.ID_RISPOSTA = " & IDRS 
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
										set rsApp = cc.execute(sSQL)
										if not rsApp.eof then
											if ESAT = "S" then
												Response.Write "<img src='" & session("progetto") & "/images/okv.gif' border=0>"
											else
												sRispOK = "N"
												sSQL = "SELECT TITOLO_ELEMENTO,A.ID_ELEMENTO FROM DOMANDA A,ELEMENTO B "&_
												" WHERE A.ID_DOMANDA = " & IDDO & " AND A.ID_ELEMENTO = B.ID_ELEMENTO "
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
												set rsElemento = CC.Execute(sSQL)
												if not rsElemento.Eof then
													sTitoloLezione	= rsElemento("TITOLO_ELEMENTO")
													nIdElemento		= clng(rsElemento("ID_ELEMENTO"))
												else
													sTitoloLezione	= ""
													nIdElemento		= 0
												end if
												rsElemento.close
												set rsElemento = nothing
												Response.Write "<img src='" & session("progetto") & "/images/ko.gif' border=0>"
											
											end if
										else
											if ESAT = "S" then
											%>	<img SRC="<%=session("progetto")%>/Images/Formazione/freccia.gif" WIDTH="14" HEIGHT="14"><%
											end if										
										end if
										rsApp.close%>
									</td>
									<td class="tblSfondoFad" width="548">
										<span class="tbltext">
										<%if IMAG > " " then%>
											<img SRC="/Pgm/Formazione/Questionari/<%=IMAG%>"><br>
										<%end if%>
										    
									    <%if esat = "S" then %>
											<b><font color="#000080">
											<%=TST%></b></font></span>
										<%else%>
											<%=TST%> </span>
										<%end if%>
											
									</td>
								</tr>
								<%RRR.MoveNext 
								nrr = nrr + 1
							Loop
							RRR.CLOSE%>
							<%if sRispOK = "N" and sTitoloLezione <> "" then%>
							<tr>
								<td class="tblSfondoFad" colspan="2"><span class="tbltext"><b>Modulo di riferimento : </b><%=sTitoloLezione%></span>
								</td>
							</tr>
							<%end if
							sRispOK = ""%>
							
						</table>
						<%End if
					else
						if TipoPagina = ConRisposte1 then
							set RRR=server.CreateObject("ADODB.Recordset")
							sql2="SELECT distinct TXT_RISPOSTA, ID_RISPOSTA, ID_DOMANDA, FL_ESATTO, IMG_RISPOSTA from RISPOSTA where ID_DOMANDA = '" & IDDM & "' order by ID_DOMANDA, ID_RISPOSTA "
'PL-SQL * T-SQL  
SQL2 = TransformPLSQLToTSQL (SQL2) 
							RRR.open sql2, CC, 3
							If RRR.EOF then 
								Response.Write "Non sono presenti risposte" 
							else
								RRR.MoveFirst%>
								<table border="0" cellspacing="2" cellpadding="1" width="500px" bordercolor="#dddddd">
								<%nrr = 0
								Do While not RRR.Eof
									TST  = RRR.Fields("TXT_RISPOSTA")
									IDRS = RRR.Fields("ID_RISPOSTA")
									ESAT = RRR.Fields("FL_ESATTO")
									IMAG = RRR.Fields("IMG_RISPOSTA")
									IDDO = RRR("ID_DOMANDA")%>	
									<tr>
										<td class="tblSfondoFad" width="22" align="center" height="23">
											<span class="tbltext">
											<%
											sSQL =	"SELECT id_risposta FROM risult_risp where " &_
													"risult_risp.ID_RISULT_QUEST = " & nResQ & " and " &_
													"risult_risp.ID_DOMANDA = " & IDDO & " and " &_
													"risult_risp.ID_RISPOSTA = " & IDRS 
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
											set rsApp = cc.execute(sSQL)
											if not rsApp.eof then
												if ESAT = "S" then
													Response.Write "<img src='" & session("progetto") & "/images/okv.gif' border=0>"
												else
													sRispOK = "N"
													sSQL = "SELECT TITOLO_ELEMENTO,A.ID_ELEMENTO FROM DOMANDA A,ELEMENTO B "&_
													" WHERE A.ID_DOMANDA = " & IDDO & " AND A.ID_ELEMENTO = B.ID_ELEMENTO "
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
													set rsElemento = CC.Execute(sSQL)
													if not rsElemento.Eof then
														sTitoloLezione	= rsElemento("TITOLO_ELEMENTO")
														nIdElemento		= clng(rsElemento("ID_ELEMENTO"))
													else
														sTitoloLezione	= ""
														nIdElemento		= 0
													end if
													rsElemento.close
													set rsElemento = nothing
													Response.Write "<img src='" & session("progetto") & "/images/ko.gif' border=0>"
												end if
											end if
											rsApp.close%>
										</td>
										<td class="tblSfondoFad" width="548">
											<span class="tbltext">
											<%if IMAG > " " then%>
												<img SRC="../<%=IMAG%>"><br>
											<%end if%>
											<%=TST%></span>
										</td>
									</tr>
									<%RRR.MoveNext 
									nrr = nrr + 1
								Loop
								RRR.CLOSE%>
								<%if sRispOK = "N" and sTitoloLezione <> ""  then%>
								<tr>
									<td class="tblSfondoFad" colspan="2"><span class="tbltext"><b>Guarda il corso : </b><%=sTitoloLezione%></span>
									</td>
								</tr>
								<%end if%>
								</table>
								<%sRispOK = ""
							End if
						End if
					End if
					nd = nd + 1
					idn = idn + 1
					nr = nr + nrr 
					RR.MoveNext 
				Loop
				RR.CLOSE
			end if
			MEMOAREADOM = IDAREADOM
			RRW.MoveNext 
		Loop
		RRW.CLOSE
	end if

end sub


sub  VisArgRis()%>
		
	<!--table border="0" cellspacing="2" cellpadding="1" width="500" align="center">		<tr align="center">			<td class=TextRed>				<a class=textRed 					href="Javascript:ArgRif('<%'=idQue%>')"><B>Argomenti di riferimento</B></A>			</td>			<td class=TextRed>				<a class=textRed 					href="/Pgm/formazione/questionari/riferimenti/QRI_RisTest.asp?inW=No&ResQ=<%=idQue%>&RisTest=0"><B>Risultato del questionario</B></A>			</td>		<tr>	</table-->		

	<table border="0" cellspacing="2" cellpadding="1" width="500" align="center">
		<tr align="center">
			<td class="TextRed">
			<%
				nResQ = nIdQuest
				nApp = 0

				'-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
				'Modifica del 04/04/2002
				'Marco Attenni
				sSQL = "Select q.id_questionario from questionario q, risult_quest r where q.id_questionario = r.id_questionario and r.id_risult_quest = " & nResQ
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
				set rsApp = cc.execute(sSQL)
				if not rsApp.eof then
					IDQue = rsApp("id_questionario")
				end if
				rsApp.close
				set rsApp = nothing
				'-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+	
'				sqlQ = "Select * From QUESTIONARIO where ID_QUESTIONARIO =" & IDque
'				set RRQ = server.CreateObject("ADODB.Recordset")
'				RRQ.open sqlQ, CC, 3		 
'				If not RRQ.EOF then		
'					NDom = cint(RRQ.Fields("Num_Dom"))
'					CodArea = RRQ.Fields("ID_AREA")
'					Titolo = RRQ.Fields("Tit_Questionario")
'				end if	

'				RRQ.Close
'				set RRQ = nothing
				ConfDomande()				
'				Server.execute("/Pgm/formazione/questionari/riferimenti/QRI_RisTest.asp")
'				Response.Write nResQ
			%>
			</td>
		<tr>
	</table>		


<%end sub%>

<% function RisultQuest(IDQue) 

	sSQL = "SELECT ID_RISULT_QUEST FROM RISULT_QUEST WHERE id_questionario = " & IDQue & " AND ID_PERSONA = " & IDP

'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	set rsIdRQuest = CC.Execute(sSQL)
	if not rsIdRQuest.eof then
		RisultQuest = rsIdRQuest("ID_RISULT_QUEST") 
	end if
	rsIdRQuest.close
	set rsIdRQuest = nothing 	

end function %>

<!--#include Virtual ="/include/closeconn.asp"-->
<!--#include Virtual="/strutt_coda2.asp"-->
