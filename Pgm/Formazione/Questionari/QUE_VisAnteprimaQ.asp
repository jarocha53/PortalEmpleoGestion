<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%
Sub Inizio()
	dim sFunzione
	dim sTitolo
	dim sCommento
	dim bCampiObbl
	
	sFunzione = "GESTIONE QUESTIONARIO"
	sTitolo = "QUESTIONARIO"
	sCommento = "Anteprima questionario"
	bCampiObbl = false
	sHelp = "/Pgm/help/Formazione/Questionari/QUE_VisAnteprimaQ/"
%>
	<!--#include virtual="include/SetTestata.asp"-->
<%
End Sub
'-------------------------------------------------------------------------
	Sub Fine()
%>
	<br>
	<table border="0" cellspacing="2" cellpadding="1" width="500px">
		<tr align="center">
			<td align="center">
			<%
				PlsLinkRosso "QUE_VisQuestionari.asp", "Elenco Questionari"
			%>
			</td>
		</tr>
	</table>
	<br>
<%
	End Sub
'-------------------------------------------------------------------------
	Sub ImpostaPag()

	set RRW=server.CreateObject("ADODB.Recordset")
	sqlw="SELECT distinct ID_AREA_DOM, DESC_AREA_DOM, IMG_AREA_DOM, ID_QUESTIONARIO from AREA_DOM where ID_QUESTIONARIO = " & IDQue & " order by ID_AREA_DOM "
	
'Response.Write "impostapag= " & sqlw & "<br>"

'PL-SQL * T-SQL  
SQLW = TransformPLSQLToTSQL (SQLW) 
	RRW.open sqlw, CC, 3
	If RRW.EOF then 
		%>
		<table border="0" cellspacing="2" cellpadding="1" width="500">
		<tr>
			<td align="center">
				<span class="tbltext3">
					<b>Non sono presenti Aree</b>
				</span>
			</td>
		</tr>

		<%	
   else
		idn = 1
%>
		<table border="0" cellspacing="2" cellpadding="1" width="500">
		<tr>
			<td colspan="2">
				<span class="tbltext3"><b>Titolo: &quot;<%=Titolo%>&quot;</b></span>
			</td>
		</tr>
		<%	

		Do While not RRW.Eof
		
			IDAREADOM  = RRW.Fields("ID_AREA_DOM")
			DESCAREADOM  = RRW.Fields("DESC_AREA_DOM")
			IMGAREADOM  = RRW.Fields("IMG_AREA_DOM")
					
			if cint(IDAREADOM) <> cint(MEMOAREADOM) then

%>	
					<tr>
						<td align="left" class="sfondocomm" colspan="2">
							<%=DescAreaDom%>
					<%
						if IMGAREADOM > " " then
					%>		
							<br><br><img src="<%=IMGAreaDom%>" border="0">
					<%
						end if
					%>		
						</td>
					</tr>
<%			end if
			set RR=server.CreateObject("ADODB.Recordset")
			sql = "SELECT distinct TXT_DOMANDA, NUM_RISPOSTE, ID_DOMANDA, ID_QUESTIONARIO, PESO_DOMANDA, IMG_DOMANDA, AUD_DOMANDA, ID_AREA_DOM from DOMANDA where ID_QUESTIONARIO = " & IDQue & " and ID_AREA_DOM = " & IDAREADOM &_
					" ORDER BY ID_DOMANDA"
			
'Response.Write sql
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
			RR.open sql, CC, 3
			If RR.EOF then 
				%>
				<tr>
					<td align="center" class="tbltext3">
						<b>Non sono presenti domande</b>
					</td>
				</tr>
				<%	
			else
				RR.MoveFirst
				nd = 1
				NDOM = NDOM * 1

				Do While not RR.Eof
					IDDM  = RR.Fields("ID_DOMANDA")
					TITL  = RR.Fields("TXT_DOMANDA")
					NRip  = RR.Fields("NUM_RISPOSTE") 
					PESO  = RR.Fields("PESO_DOMANDA")
					IMG   = RR.Fields("IMG_DOMANDA")
					AUDIO = RR.Fields("AUD_DOMANDA")
							
					%>
					<tr>
						<td align="left" class="tblsfondo" colspan="2">
							<span class="tbltext1"><b><% =idn%>) <%=TITL%></b> (Peso: <%=peso%>)</span>
					<%
					if IMG > " " then
					%>
							<br><img SRC="<%=IMG%>">
					<%
					end if

					if Audio > " " then
					%>
							<br><a href="<%=Audio%>"><img SRC="/Images/Formazione/audio.gif" border="0">Ascolta l'audio</a>
					<%
					end if
					%>
						</td>
					</tr>
					<%
						set RRR=server.CreateObject("ADODB.Recordset")
						sql2="SELECT distinct TXT_RISPOSTA, ID_RISPOSTA, ID_DOMANDA, FL_ESATTO, IMG_RISPOSTA from RISPOSTA where ID_DOMANDA = '" & IDDM & "' order by ID_DOMANDA, ID_RISPOSTA "
				        'Response.Write sql2
'PL-SQL * T-SQL  
SQL2 = TransformPLSQLToTSQL (SQL2) 
						RRR.open sql2, CC, 3
						If RRR.EOF then 
							%>
							<tr>
								<td align="center" class="tbltext3">
									<b>Non sono presenti risposte</b>
								</td>
							</tr>
							<%	
						else
							RRR.MoveFirst
							nrr = 0
							Do While not RRR.Eof
								TST  = RRR.Fields("TXT_RISPOSTA")
								IDRS = RRR.Fields("ID_RISPOSTA")
								ESAT = RRR.Fields("FL_ESATTO")
								IMAG = RRR.Fields("IMG_RISPOSTA")
								%>	
								<tr>
									<td class="tblsfondo" width="22" align="center">
										<span class="tbltext">
										<input type="radio" id="DOM<%=idn%>" name="DOM<%=idn%>" value="D<%=IDDM%>R<%=IDRS%>E<%=ESAT%>P<%=PESO%>">
										</td>
										<td class="tblsfondo" width="548">
											<span class="tbltext">
										<%
											if IMAG > " " then
										%>
											<img SRC="<%=IMAG%>"><br>
										<%
											end if
										    if esat = "S" then %>
												<b>
													<%=TST%>
												</b>
											<% 
											else
											%>
													<%=TST%>
											<%end if%>
											</span>
										</td>
								</tr>
								<%
								RRR.MoveNext 
								nrr = nrr + 1
							Loop
							RRR.CLOSE
					End if
					%>
					</tr>
					<%
					nd = nd + 1
					idn = idn + 1
					nr = nr + nrr 
					RR.MoveNext 
				Loop
				RR.CLOSE
			end if
			MEMOAREADOM = IDAREADOM
			RRW.MoveNext 
	    Loop
		RRW.CLOSE
	end if
%>
</table>
<%
  End Sub
'-------------------------------------------------------------------------
'M A I N
%>
<!--#include virtual="/strutt_testa2.asp"-->
<!--#include virtual="/include/openconn.asp"-->
<!--#include virtual="/include/SetPulsanti.asp"-->
<%	
 	Dim sqlQ, NDom, codArea, Titolo, IDQue, nd, nr
	
	on error goto 0

	IDQue = Request.QueryString("id")

	nd = 0
	nr = 0

	Inizio()

	sqlQ = "Select * From QUESTIONARIO where ID_QUESTIONARIO =" & IDque
	set RRQ = server.CreateObject("ADODB.Recordset")

'PL-SQL * T-SQL  
SQLQ = TransformPLSQLToTSQL (SQLQ) 
	RRQ.open sqlQ, CC, 3		 
	
	If not RRQ.EOF then		
		NDom = cint(RRQ.Fields("Num_Dom"))
		CodArea = RRQ.Fields("ID_AREA")
		Titolo = RRQ.Fields("Tit_Questionario")
	end if	

	RRQ.Close
	set RRQ = nothing

	ImpostaPag()
		
	Fine()
%>
<!--#include virtual="/include/closeconn.asp"-->
<!--#include virtual="/strutt_coda2.asp"-->
