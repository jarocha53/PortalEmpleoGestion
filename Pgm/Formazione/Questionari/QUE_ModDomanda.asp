<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%
Sub ControlliJavascript()
%>
	<script LANGUAGE="JavaScript">
		function validateIt(frmMe)
		{
		if(document.form1.Areadom.value == "" || document.form1.Areadom.value == " ")
		  {
		  document.form1.Areadom.focus();
		  alert("Inserire l' Area Domanda. ");
		  return false;
		  }
		 if(document.form1.NRisposte.value < 2 || document.form1.NRisposte.value > 100)
		  {
		  document.form1.NRisposte.focus();
		  alert("Inserire nel campo Numero Risposte un numero da 2 a 100. ");
		  alert(document.form1.NRisposte.value);
		  return false;
		  } 
		 if(document.form1.NRisposte.value == "" || document.form1.NRisposte.value == " ")
		  {
		  document.form1.NRisposte.focus();
		  alert("Inserire il numero di Risposte previste per quest'area. ");
		  return false;
		  } 
		 if(document.form1.Valore.value == "" || document.form1.Valore.value == " ")
		  {
		  document.form1.Valore.focus();
		  alert("Inserire il valore della risposta. ");
		  return false;
		  }
		return true;
		}
	
		var finestra
		function ListaImmagini(sImage)
		{
			if (finestra != null ) 
			{
				finestra.close(); 
			}
			f='QUE_VisImmagini.asp?img=' + sImage
			finestra=window.open(f,"","width=550,height=450,location=no,menubar=no,scrollbars=yes,resizable=no")
		}
		var finestraA
		function ListaAudio(sImage)
		{
			if (finestraA != null ) 
			{
				finestraA.close(); 
			}
			f='QUE_VisAudio.asp?img=' + sImage
			finestraA=window.open(f,"","width=550,height=450,location=no,menubar=no,scrollbars=yes,resizable=no")
		}		
		function ConfElimina(idd, ida, idq, tmst)
		{
			if (confirm("Confermi la cancellazione della Domanda?") == true)
			{
				window.location = "QUE_DelDomanda.asp?idd="+idd+"&ida="+ida+"&idq="+idq+"&tmst="+tmst
			}
		}

		//-->
	</script>
<%
End Sub
'------------------------------------------------------------------------------------------------
Sub Inizio()

	dim sFunzione
	dim sTitolo
	dim sCommento
	dim bCampiObbl
	
	sFunzione = "GESTIONE QUESTIONARIO"
	sTitolo = "QUESTIONARIO"
	sCommento = "Modifica della domanda"
	bCampiObbl = true
	sHelp = "/Pgm/help/Formazione/Questionari/QUE_ModDomanda/"		
%>
	<!--#include virtual="include/SetTestata.asp"-->
<%

End Sub
'------------------------------------------------------------------------------------------------
Sub PaginaVuota()
%>
<table width="500" border="0">
	<tr>
		<td class="tbltext3" align="center">
			Errore durante la ricerca della Domanda.
		</td>
	</tr>
	<tr><td>&nbsp;</td></tr>
	<tr>
		<td align="center">
			<%
				PlsIndietro()
			%>
		</td>
	</tr>
</table>
<%
End Sub
'------------------------------------------------------------------------------------------------
Sub ImpostaPag()
	
   	set Rs=server.CreateObject("ADODB.Recordset")
   	sql1="SELECT TXT_DOMANDA, AUD_DOMANDA, IMG_DOMANDA, PESO_DOMANDA, NUM_RISPOSTE, DT_TMST from DOMANDA where ID_DOMANDA = " & iddom
   '	Response.Write sql1
'PL-SQL * T-SQL  
SQL1 = TransformPLSQLToTSQL (SQL1) 
   	Rs.open sql1, CC, 3
   	if rs.EOF then
   		PaginaVuota()
   		exit sub
   	end if
   	xtextdo = Rs.Fields("TXT_DOMANDA")
   	xaudiod = Rs.Fields("AUD_DOMANDA")
   	ximaged = Rs.Fields("IMG_DOMANDA")
   	xpesdom = Rs.Fields("PESO_DOMANDA")
   	xnumris = Rs.Fields("NUM_RISPOSTE")
   	dtTmst  = Rs.Fields("DT_TMST")
		
   	Rs.close
   	set Rs = nothing 

   	set RR=server.CreateObject("ADODB.Recordset")
   	sqlq="SELECT TIT_QUESTIONARIO from QUESTIONARIO where ID_Questionario = " & IDQUE
'		Response.Write sql
'PL-SQL * T-SQL  
SQLQ = TransformPLSQLToTSQL (SQLQ) 
   	RR.open sqlq, CC, 3
   	TIT = RR.FIELDS("TIT_QUESTIONARIO")
'		Response.Write nQST
   	RR.CLOSE
		
   	set RR=server.CreateObject("ADODB.Recordset")
   	sql="SELECT Desc_Area_Dom, IMG_AREA_DOM from Area_Dom where ID_Area_Dom = " & IdArea
'		Response.Write sql
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
   	RR.open sql, CC, 3
   	TITarea = RR.FIELDS("Desc_Area_Dom")
   	IMGar = RR.FIELDS("IMG_AREA_DOM")
'		Response.Write IMGAR
   	RR.CLOSE
	set RR = nothing
%>
	<table width="500" border="0">
		<tr>
			<td class="tbltext3">
				Questionario: &quot;<%=TIT%>&quot;
			</td>
		</tr>
	</table>
	<table width="500" border="0">
	<%
	if TITAREA > " " then
	%>
   		<tr>
   			<td class="tbltext3">
   				Testo Area: &quot;<%=TITAREA%>&quot;
   			</td>
   		</tr>
   	<%
   	end if
   	if IMGAR > " " then
   	%>
   		<tr>
   			<td class="tbltext3">
   				Immagine Area:<br>
   				<img SRC="<%=IMGAR%>">
   			</td>
   		</tr>
   	<%
   	end if
   	%>
	</table>

   	<br>		
   	<form action="QUE_CnfModDomanda.asp?modo=upd&amp;IDQUE=<%=IDQUE%>&amp;IDAREA=<%=IDAREA%>&amp;IDDOM=<%=IDDOM%>" method="POST" id="form1" name="form1" onsubmit="return validateIt(this)">
		
   	<table border="0" width="500" cellspacing="2" cellpadding="1"> 
   		<tr> 
			<td class="tbltext1">
				<b>Domanda</b>
			</td>
			<td>
				<input type="text" id="Areadom" name="Areadom" size="40" maxlength="250" value="<%=xtextdo%>" class="textblacka">
			</td>
   		</tr>
   		<tr>
   			<td class="tbltext1">
   				<b>Immagine</b>
   			</td>
   			<td>
   				<input name="txtMedia" id="txtMedia" value="<%=ximaged%>" class="textblacka">
   				<a href="javascript:ListaImmagini('<%=ximaged%>')">
   					<img src="<%=Session("progetto")%>/images/bullet1.gif" border="0" alt="Lista delle immagini"><!-- onmouseover="javascript:window.status='' ; return true"-->
   				</a>
   			</td>
   	    </tr>
   	    <tr>
   			<td class="tbltext1">
   				<b>Audio</b>
   			</td>
   			<td>
   				<input name="txtAudio" id="txtAudio" value="<%=xaudiod%>" class="textblacka">
   				<a href="javascript:ListaAudio('<%=xaudiod%>')">
   					<img src="<%=Session("progetto")%>/images/bullet1.gif" border="0" alt="Lista dei file audio"><!-- onmouseover="javascript:window.status='' ; return true"-->
   				</a>
   			</td>
   	    </tr>
   	    <tr>
   			<td class="tbltext1">
   				<b>Num Risposte</b>
   			</td>
   			<td>
   				<input type="text" size="2" cols="2" name="NRisposte" id="NRisposte" maxlength="100" value="<%=xnumris%>" class="textblacka">
   			</td>
   	    </tr>
   	    <tr>
   			<td class="tbltext1">
   				<b>Peso</b>
   			</td>
   			<td>
   				<input type="text" size="2" cols="2" name="Valore" id="Valore" maxlength="2" value="<%=xpesdom%>" class="textblacka">
   			</td>
   	    </tr>
	</table>
	<table width="500">
   	    <tr> 
   			<td align="center">
   				<%
   					PlsIndietro()
   					PlsInvia("Conferma")
   					PlsElimina("javascript:ConfElimina("& iddom &"," & idarea & ", " & idque & ",'" & dtTmSt & "')")
   				%>
   			</td>
   	    </tr>
    </table>
   	</form>
<%
End Sub
'------------------------------------------------------------------------------------------------
'M A I N
%>
<!--#include virtual="/strutt_testa2.asp"-->
<!--#include virtual="/include/openconn.asp"-->
<!--#include virtual="/include/SetPulsanti.asp"-->
<%	
	IdQue=Request.QueryString("IDQ")
	IdArea=Request.QueryString("IDA")
	Iddom=Request.QueryString("IDD")
	
	ControlliJavaScript()	
	Inizio()
	ImpostaPag()
%>			
<!--#include virtual="/include/closeconn.asp"-->
<!--#include virtual="/strutt_coda2.asp"-->
