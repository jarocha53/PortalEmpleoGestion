<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%
Sub Inizio()

	dim sFunzione
	dim sTitolo
	dim sCommento
	dim bCampiObbl
	
	sFunzione = "GESTIONE QUESTIONARIO"
	sTitolo = "QUESTIONARIO"
	sCommento = "Modifica della Domanda"
	bCampiObbl = false
%>
	<!--#include virtual="include/SetTestata.asp"-->
<%
End Sub
'------------------------------------------------------------------------------
Sub Modifica()

	set RR=server.CreateObject("ADODB.Recordset")
	sql="SELECT DT_TMST from DOMANDA where ID_DOMANDA =" & iddom
'		response.write sql
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
	RR.open sql, CC,3
	dtTmst=RR.FIELDS(0)
	RR.CLOSE

	set RR=server.CreateObject("ADODB.Recordset")
		
	' SQL statement
	sql = "UPDATE DOMANDA SET TXT_DOMANDA = '" & AREADOM 
	sql = sql & "' , IMG_DOMANDA = '" & IMMAGINE 
	sql = sql & "' , AUD_DOMANDA = '" & AUDIO
	sql = sql & "' , NUM_RISPOSTE = " & NRISPOSTE
	sql = sql & " , PESO_DOMANDA = " & VALORE
	sql = sql & " WHERE ID_DOMANDA = " & IdDom
			
			
'	Response.Write sql
	sErrore=Esegui(iddom ,"Domanda",Session("persona"),"MOD",sql,1,dtTmst)

	IF sErrore="0" then
		
	%>
	<br>
		<table border=0 cellspacing=2 cellpadding=1 width="500">
			<tr align=middle>
				<td class="tbltext3">
					Modificación correctamente efectuada.
				</td>
			</tr>
		</table>
	<br><br>
	<%
		else
	%>
		<br>
		<table border=0 cellspacing=2 cellpadding=1 width="500">
			<tr align=middle>
				<td span class="tbltext3">
					Error en la modificación. <%=sErrore%>				
				</td>
			</tr>
		</table>		
		<br><br>
	<%
	End If	

End sub
'---------------------------------------------------------------------
Sub Inserisci()

		DTTMST = now() 
		
		sql = "INSERT INTO DOMANDA (TXT_DOMANDA, AUD_DOMANDA, IMG_DOMANDA, PESO_DOMANDA, NUM_RISPOSTE, ID_QUESTIONARIO, ID_AREA_DOM)"
		sql = sql & " VALUES ('" & AREADOM & "'"
		sql = sql & ",'" & AUDIO &  "'"
		sql = sql & ",'" & IMMAGINE &  "'"
		sql = sql & "," & VALORE
		sql = sql & "," & NRISPOSTE
		sql = sql & "," & IDQUE 
		sql = sql & "," & IDAREA & ")"
	'	Response.Write sql
		sErrore=Esegui("pgm/formazione/questionari/que","DOMANDA",Session("persona"),"INS",SQL,1,"")

	IF sErrore="0" then
		
	%>
		<br>
		<table border=0 cellspacing=2 cellpadding=1 width="500">
			<tr align=middle>
				<td class="tbltext3">
					Ingreso correctamente efectuado.
				</td>
			</tr>
		</table>
		<br><br>
	<%
	else
	%>
		<br>
		<table border=0 cellspacing=2 cellpadding=1 width="500">
			<tr align=middle>
				<td span class="tbltext3">Error en el ingreso. <%=sErrore%>
				</td>
			<tr></tr>
		</table>		
		<br><br>
<%
	End If	

End Sub
'-------------------------------------------------------------------------------
Sub TornaIndietro()
%>
	<br>
	<table border=0 cellspacing=2 cellpadding=1 width="500">
		<tr>
			<td align=center>
			<%
				PlsLinkRosso "QUE_VisDomanda.asp?ida=" & IDAREA & "&idq=" & IDQUE, "Elenco delle Domande"
			%>
			</td>
		</tr>
	</table>
<%
End Sub
'-------------------------------------------------------------------------------
'M A I N
	
	IDQUE=Request.QueryString("IDQUE")
	IDAREA=Request.QueryString("IDAREA")
	IDDOM=Request.QueryString("IDDOM")
	AREADOM=Replace(Request.Form("AREADOM"), "'", "''")
	IMMAGINE=Request.Form("txtMedia")
	AUDIO=Request.Form("txtAUDIO")
	NRISPOSTE=Request.Form("NRISPOSTE")
	VALORE =Request.Form("VALORE")
	DTTMST = now()
	IDP = Session("creator")
	modo = Request.QueryString("modo")

%>
<!--#include virtual="/strutt_testa2.asp"-->
<!--#include virtual="/include/openconn.asp"-->
<!--#include virtual="/Util/DBUtil.asp"-->
<!--#include virtual="/Include/SysFunction.asp"-->
<!--#include virtual="/Include/SetPulsanti.asp"-->
<%
	Inizio()
	if modo = "ins" then
		Inserisci()
	else
		Modifica()
	end if
	TornaIndietro()
%>	
<!--#include virtual="/include/closeconn.asp"-->
<!--#include virtual="/strutt_coda2.asp"-->
