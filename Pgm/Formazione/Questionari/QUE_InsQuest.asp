<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%
Sub ControlliJavaScript()
%>
	<script language="Javascript">

		function validateIt(frmQ)
		{
		if(frmQ.QArea.value == "" || frmQ.QArea.value == " ")
		  {
		  frmQ.QArea.focus();
		  alert("Inserire l'area di appartenenza. ");
		  return false;
		  }
		 if(frmQ.QTitolo.value == "" || frmQ.QTitolo.value == " ")
		  {
		  frmQ.QTitolo.focus();
		  alert("Inserire il Titolo. ");
		  return false;
		  } 
		 if(frmQ.QTipo.value == "" || frmQ.QTipo.value == " ")
		  {
		  frmQ.QTipo.focus();
		  alert("Inserire il Tipo. ");
		  return false;
		  } 
		 if(frmQ.cboFlPubblico.value == "")
		  {
		  frmQ.cboFlPubblico.focus();
		  alert("Inserire il 'Pubblico/Privato'. ");
		  return false;
		  } 
		 
		 if(frmQ.QNumDomande.value > 100 || frmQ.QNumDomande.value < "1" || frmQ.QNumDomande.value > "9")
		  {
		  frmQ.QNumDomande.focus();
		  alert("Inserire nel campo Numero Aree un numero da 1 a 100. ");
		  return false;
		  } 
		 if(frmQ.QTempo.value > 100 || frmQ.QTempo.value < "1" || frmQ.QTempo.value > "9")
		  {
		  frmQ.QTempo.focus();
		  alert("Inserire nel campo Tempo Previsto un numero di minuti da 1 a 100. ");
		  return false;
		  } 
		 if(frmQ.QSoglia.value > 100 || frmQ.QSoglia.value < "1" || frmQ.QSoglia.value > "9")
		  {
		  frmQ.QSoglia.focus();
		  alert("Inserire nel campo Punteggio minimo un numero percentuale da 10 a 100. ");
		  return false;
		  } 
		return true;
		}
	</script>
<%	
End Sub
'------------------------------------------------------------------------
Sub Inizio()
	dim sFunzione
	dim sTitolo
	dim sCommento
	dim bCampiObbl
	
	sFunzione = "GESTIONE QUESTIONARIO"
	sTitolo = "QUESTIONARIO"
	'sCommento = "Inserimento del<br>Questionario"
	sCommento = "Inserimento Questionario.<br>Compilare i campi sottostanti e premere Invia per memorizzare le informazioni."
	bCampiObbl = true
	sHelp = "/Pgm/help/Formazione/Questionari/QUE_InsQuest/"	
	
%>
	<!--#include virtual="/include/SetTestata.asp"-->
<%
End Sub
'------------------------------------------------------------------------
	Sub ImpostaPag()
		Dim sql, RR
%>		
		
		<form method="POST" action="QUE_CnfInsQuest.asp" onsubmit="return validateIt(this);">
				<table border=0 cellspacing=2 cellpadding=1 width="500px">
				<tr>
					<td class="tbltext1"> 
						<b>Area*</b>
					</td>
					<td>
						<select name="QArea" id="QArea" size="1" class="textblacka">
						<option selected></option>
					<%	set RR=server.CreateObject("ADODB.Recordset")
						sql="SELECT distinct ID_AREA, CodiceArea, TitoloArea from Area ORDER BY CodiceArea"
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
						RR.open sql, CC,3
						Do While not RR.Eof					
					%><option value="<%=RR.FIELDS("ID_AREA")%>"> <%=RR.FIELDS("CODICEAREA")%> - <%=RR.FIELDS("TITOLOAREA")%></option>
					<%		RR.MoveNext 
						Loop
						RR.CLOSE
					%>	</select>
					</td>
				</tr>				
				<tr>
					<td class="tbltext1"> 
						<b>Titolo*</b>
					</td>
					<td>
						<input size="30" maxlength ="250" name="QTitolo" id="QTitolo" class="textblacka">
					</td>
				</tr>
				<tr>
					<td class="tbltext1">
						<b>Tipo*</b>
					</td>
					<td>
						<select name="QTipo" id="QTipo" size="1" class="textblacka">
						<option selected></option>
						<option value="D">Dinamico</option>
						<option value="S">Statico</option>
						</select>
					</td>
				</tr>
					<td class="tbltext1">
						<b>Numero di Aree*</b>
					</td>
					<td>
						<input size="3" cols=3 maxlength="3" name="QNumDomande" id="QNumDomande" class="textblacka">
					</td>
				</tr>
				<tr>
					<td class="tbltext1">
						<b>Tempo previsto* </b>(in minuti)
					</td>
					<td >
						<input size="3" cols=3 maxlength="3" name="QTempo" id="QTempo" class="textblacka">
					</td>
				</tr>
				<tr>
					<td class="tbltext1">
						<b>Punteggio minimo*</b>
					</td>
					<td>
						<input size="3" cols=3  maxlength=3 name="QSoglia" id="QSoglia" class="textblacka">
						<span class="tbltext1">% del punteggio totale</span>
					</td>
				</tr>
				<!--tr> 
					<td align="left" nowrap class="tbltext1">
						<b>Memorizzare le risposte</b>
					</td>
					<td> 
						<input type="checkbox" name="chkMemoRisp" id="chkMemoRisp">
					</td>
				</tr-->
				<tr height=20> 
					<td align="left" nowrap class="tbltext1">
						<b>Progetto proprietario</b>
					</td>
					<td class=textblack>
						<b>
						<%=DecCodVal("CPROJ", "0", date, mid(session("progetto"),2), 1)%>
						</b>
					</td>
					<div id=dHideCombo1 style="visibility:hidden">
						<%
						CreateCombo("CPROJ||||cboProjOwn||")
						%>
					</div>
				</tr>
				<tr> 
					<td align="left" nowrap class="tbltext1">
						<b>Pubblico/Privato</b>
					</td>
					<td class=textblack>
						<b>
						PRIVATO
						</b>
					</td>
					<div id=dHideCombo2 style="visibility:hidden">
					<select name="cboFlPubblico" id="cboFlPubblico" class=textblack>
						<option value="0">Pubblico</option>
						<option value="1" selected>Privato</option>
					</select>
					</div>
				</tr>
				<tr> 
					<td align="left" nowrap class="tbltext1">
						<b>Ripetibile</b>
					</td>
					<td> 
						<input type="checkbox" name="chkFlRepeat" id="chkFlRepeat">
					</td>
				</tr>
				<tr>
					<td colspan=2>
						&nbsp;
					</td>
				</tr>

			</table>
		<table border=0 cellspacing=2 cellpadding=1 width="500px">
			<tr>
				<td align=right>
					<%
					PlsIndietro()
					%>
				</td>
				<td align=left>
					<%
					PlsInvia ("Conferma")
					%>
				</td>
			</tr>
		</table>
		</form>
		<br>
<%
	End Sub
'------------------------------------------------------------------------
'M A I N

%>
<!--#include virtual="/strutt_testa2.asp"-->
<!--#include virtual="/include/openconn.asp"-->
<!--#include virtual="/include/SetPulsanti.asp"-->
<%
	ControlliJavaScript()
	Inizio()
	ImpostaPag()
%>			
<!--#include virtual="/include/closeconn.asp"-->
<!--#include virtual="/strutt_coda2.asp"-->
