<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%
Sub Inizio()
%>
<html>
<head>
	<link REL="STYLESHEET" TYPE="text/css" HREF="<%=session("progetto")%>/fogliostile.css">
	<meta NAME="GENERATOR" Content="Microsoft Visual Studio 6.0">
	<title>Richiesta di assistenza</title>	
</head>
<script LANGUAGE="JavaScript">
<!--#include virtual="/include/ControlString.inc"-->
function ValidaRisposta(frmRic)
{
	if (frmRic.txtTestoRisposta.value=="")
	{
		alert("Inserire il Testo della Risposta")
		frmRic.txtTestoRisposta.focus()
		return (false)
	}
	return (true)
}
function ChiudiERicarica()
{
	opener.location.reload()
	self.close()
}
</script>
<body>
<%
dim sFunzione
dim sTitolo
dim sCommento
dim bCampiObbl
	
sFunzione = "RICHIESTA DI ASSISTENZA"
sTitolo = "RISPOSTA RICHIESTE DI ASSISTENZA"
sCommento = "Visualizza la risposta della richiesta d'assistenza inviata"
bCampiObbl = false
sHelp="/Pgm/help/formazione/RicAss/RIC_VisRispRicAss"
%>
<!--#include virtual="/include/SetTestata.asp"-->
<%
End Sub
';););););););););););););););););););););););););););););););););););););););)
Function DecodArea(nArea)
Dim sSQL, rsArea
if isnull(nArea) then exit function

sSQL = " select DESCRIZIONEAREA from area where ID_AREA = " & nArea
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
set rsArea = CC.execute(sSQL)
if rsArea.eof then
	DecodArea = "Area non disponibile"
else
	DecodArea = rsArea("DESCRIZIONEAREA")
end if
rsArea.close
set rsArea = nothing

End Function
';););););););););););););););););););););););););););););););););););););););)
Sub ImpostaPag()
dim nIdRich
dim sSQL, rsRicAss
nIdRich = Request.QueryString("idR")
sSQL = "Select Cod_Area, Oggetto, Testo_Dom, Dt_Invio, dt_tmst, dt_risposta, testo_risp from RIC_ASS where id_ric_ass = " & nIdRich
Set rsRicAss = Server.CreateObject("ADODB.Recordset")
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
rsRicAss.Open sSQL, CC, 3
if rsRicAss.EOF then
%>
	<br>
	<table border="0" width="500" cellpadding="0" cellspacing="0">
	   <tr align="center">
	   	<td class="tbltext3"> 
	   		Errore Brutto!!! 
	   	</td>
	   </tr>
	</table>
	<br>
<%
else
%>
<table border="0" cellspacing="2" cellpadding="1" width="500">
    <%
    if rsRicAss("COD_AREA") > "" then
    %>
    <tr> 
		<td align="left" nowrap class="tbltext1">
			<b>Area</b>
		</td>
		<td class="textblack">
			<% 'DecCodVal("AREAT", 0, date, rsRicAss("cod_area"),1)
				Response.Write(DecodArea(rsRicAss("cod_area")))
			%>
		</td>
    </tr>
    <tr><td>&nbsp;</td></tr>    
    <%
    end if
    %>
	<tr> 
		<td align="left" nowrap class="tbltext1" width="150">
			<b>Oggetto</b>
		</td>
		<td class="textblack">
			<%=rsRicAss("OGGETTO")%>
		</td>
	</tr>
	<tr><td>&nbsp;</td></tr>
	<tr>
		<td align="left" nowrap class="tbltext1">
			<b>Testo Domanda</b>
		</td>
		<td class="textblack">
			<%=rsRicAss("TESTO_DOM")%>
		</td>
	</tr>
	<tr><td>&nbsp;</td></tr>
	<tr>
		<td align="left" nowrap class="tbltext1">
			<b>Data Invio</b>
		</td>
		<td class="textblack">
			<%=ConvDateToString(rsRicAss("DT_INVIO"))%>
		</td>
	</tr>
    <tr><td>&nbsp;</td></tr>
    <tr> 
		<td align="left" nowrap class="tbltext1">
			<b>Testo Risposta</b>
		</td>
		<td class="textblack">
			<%=rsRicAss("TESTO_RISP")%>
		</td>
    </tr>
	<tr><td>&nbsp;</td></tr>
	<tr>
		<td align="left" nowrap class="tbltext1">
			<b>Data Risposta</b>
		</td>
		<td class="textblack">
			<%=ConvDateToString(rsRicAss("DT_RISPOSTA"))%>
		</td>
	</tr>
</table>
<br>
<table width="500" border="0" cellspacing="2" cellpadding="1">
	<tr align="center">
		<td align="center">			
			<%
				PlsChiudi()
			%>
		</td>
	</tr>
</table> 
<br>
<%
end if
End Sub
';););););););););););););););););););););););););););););););););););););););)
Sub VisMessaggio(sMess)
%>
<br>
<table border="0" width="500" cellpadding="0" cellspacing="0">
   <tr align="center">
   	<td class="tbltext3"> 
   		<%=sMess%>
   	</td>
   </tr>
</table>
<br>
<%
End Sub
';););););););););););););););););););););););););););););););););););););););)
'M A I N
%>
<!--#include virtual="/include/openconn.asp"-->
<!--#include virtual="/include/DecCod.asp"-->
<!--#include virtual="/include/SetPulsanti.asp"-->
<!--#include virtual="/include/ControlDateVB.asp"-->
<!--#include virtual="/Include/SysFunction.asp"-->
<%
Inizio()
ImpostaPag()
%>
<!--#include virtual="/include/closeconn.asp"-->
