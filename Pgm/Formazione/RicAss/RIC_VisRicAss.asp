<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%
Sub Inizio()
	dim sFunzione
	dim sTitolo
	dim sCommento
	dim bCampiObbl
	
	sFunzione = "RICHIESTA DI ASSISTENZA"
	sTitolo = "RICHIESTA DI ASSISTENZA"
	sCommento = "Visualizza l'elenco delle Richieste di Assistenza presenti"
	bCampiObbl = false
	sHelp="../../help/formazione/RicAss/RIC_VisRicAss"
%>
	<!--#include virtual="/include/SetTestata.asp"-->
<%
End Sub
';););););););););););););););););););););););););););););););););););););););)
Function DecodArea(nArea)
Dim sSQL, rsArea
if isnull(nArea) then exit function

sSQL = " select DESCRIZIONEAREA from area where ID_AREA = " & nArea
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
set rsArea = CC.execute(sSQL)
if rsArea.eof then
	DecodArea = "Area non disponibile"
else
	DecodArea = rsArea("DESCRIZIONEAREA")
end if
rsArea.close
set rsArea = nothing

End Function

';););););););););););););););););););););););););););););););););););););););)
Sub ImpostaPag()
dim sSQL, rsRicAss
dim nProg, sOldCodRuo

sSQL = "SELECT cod_ruolo, cod_area, destinatario, oggetto, testo_dom, dt_invio, " &_
		"dt_risposta, testo_risp, dt_tmst FROM RIC_ASS " &_
		"WHERE idutente = " & session("idutente") & " " &_
		"ORDER BY cod_ruolo, dt_invio"
'Response.Write sSQL		
Set rsRicAss = Server.CreateObject("ADODB.Recordset")
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
rsRicAss.Open sSQL, CC, 3
if rsRicAss.EOF then
	%>
	<br>
	<table border="0" width="500" cellpadding="0" cellspacing="0">
	   <tr align="center">
	   	<td class="tbltext3"> 
	   		Non sono presenti Richieste di Assistenza
	   	</td>
	   </tr>
	</table>
	<br>
	<%
else
	%>
	<br>
	<table border="0" width="500" cellpadding="1" cellspacing="2">
		<tr class="sfondocomm" height=23>
			<td width="10"><b>#</b></td>
			<td><b>Oggetto</b></td>
			<td><b>Area</b></td>
			<td width="80"><b>Data Invio</b></td>
			<td width="80"><b>Risposta</b></td>
		</tr>
	<%
	sOldCodRuo = ""
	nProg = 1
	do while not rsRicAss.EOF
		if rsRicAss("cod_ruolo") <> sOldCodRuo then
			if sOldCodRuo > "" then
				'Inserisco una riga vuota
				Response.Write "<tr><td>&nbsp;</td></tr>"
			end if
			%>
			<tr class="tblsfondo">
				<td colspan=5 class="sfondocomm">
					<%=DecCodVal("RORGA", 0, date, rsRicAss("cod_ruolo"),1)%>
				</td>
			</tr>
			<%
			sOldCodRuo = rsRicAss("cod_ruolo")
			nProg = 1
		end if
	%>
		<tr class="tblsfondo">
			<td class="tblDett">
				<%=nProg%>
			</td>
			<td class="tblDett">
				<%=strHTMLEncode(rsRicAss("Oggetto"))%>
			</td>
			<td class="tblDett">
				<%
				'DecCodVal("AREAT", 0, date, rsRicAss("cod_area"),1)
				DecodArea(rsRicAss("cod_area"))
				%>
			</td>
			<td class="tblDett">
				<%=ConvDateToString(rsRicAss("dt_invio"))%>
			</td>
			<td class="tblDett">
				<%
				if rsRicAss("dt_risposta") > "" then
					Response.Write ConvDateToString(rsRicAss("dt_invio"))
				else
					Response.Write "In attesa..."
				end if
				%>
			</td>
		</tr>
	<%
		nProg = nProg + 1
		rsRicAss.MoveNext
	loop
	%>
	</table>
	<br>
	<%
	
end if
rsRicAss.Close
set rsRicAss = nothing
%>
<table border="0" width="500" cellpadding="0" cellspacing="0">
   <tr align="center">
   	<td>
   		<%
   		PlsLinkRosso "RIC_InsRicAss.asp", "Nuova Richiesta"
   		%>
   	</td>
   </tr>
</table>
<%  
End Sub
';););););););););););););););););););););););););););););););););););););););)
'M A I N
%>
<!--#include virtual="/strutt_testa2.asp"-->
<!--#include virtual="/include/openconn.asp"-->
<!--#include virtual="/include/ControlDateVB.asp"-->
<!--#include virtual="/include/DecCod.asp"-->
<!--#include virtual="/include/SetPulsanti.asp"-->
<!--#include virtual="/include/HTMLEncode.asp"-->
<%	
if ValidateService(session("idutente"),"RIC_VISRICHIESTE",CC) <> "true" then 
	response.redirect "/util/error_login.asp"
end if

Inizio()
ImpostaPag()
%>
<!--#include virtual ="/include/closeconn.asp"-->
<!--#include virtual="/strutt_coda2.asp"-->
