<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%
Sub Inizio()
%>
<html>
<head>
	<link REL="STYLESHEET" TYPE="text/css" HREF="<%=session("progetto")%>/fogliostile.css">
	<meta NAME="GENERATOR" Content="Microsoft Visual Studio 6.0">
	<title>Richiesta di assistenza</title>	
</head>
<script LANGUAGE="JavaScript">
<!--#include virtual="/include/ControlString.inc"-->
function ValidaRisposta(frmRic)
{
	if (frmRic.txtTestoRisposta.value=="")
	{
		alert("Inserire il Testo della Risposta")
		frmRic.txtTestoRisposta.focus()
		return (false)
	}
	return (true)
}
function ChiudiERicarica()
{
	opener.location.reload()
	self.close()
}
</script>
<body>
<%
dim sFunzione
dim sTitolo
dim sCommento
dim bCampiObbl
	
sFunzione = "RICHIESTA DI ASSISTENZA"
sTitolo = "EVASIONE RICHIESTE DI ASSISTENZA"
sCommento = "Visualizza il dettaglio della richiesta d'assistenza da evadere"
bCampiObbl = true
sHelp="/Pgm/help/formazione/RicAss/RIC_InsRispRicAss"
%>
<!--#include virtual="/include/SetTestata.asp"-->
<%
End Sub
';););););););););););););););););););););););););););););););););););););););)
Function DecodArea(nArea)
Dim sSQL, rsArea
if isnull(nArea) then exit function

sSQL = " select DESCRIZIONEAREA from area where ID_AREA = " & nArea
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
set rsArea = CC.execute(sSQL)
if rsArea.eof then
	DecodArea = "Area non disponibile"
else
	DecodArea = rsArea("DESCRIZIONEAREA")
end if
rsArea.close
set rsArea = nothing

End Function
';););););););););););););););););););););););););););););););););););););););)
Sub ImpostaPag()
dim nIdRich
dim sSQL, rsRicAss
nIdRich = Request.QueryString("idR")
sSQL = "Select (select Cognome || ' ' || Nome from persona " & _
		" where id_persona in (select creator from utente where idutente = ra.idutente)) as Utente " & _
		" , ra.Cod_Area, ra.Oggetto, " & _
		" ra.Testo_Dom, ra.Dt_Invio, ra.dt_tmst from RIC_ASS ra where ra.id_ric_ass = " & nIdRich
Set rsRicAss = Server.CreateObject("ADODB.Recordset")

'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
rsRicAss.Open sSQL, CC, 3
if rsRicAss.EOF then
%>
	<br>
	<table border="0" width="500" cellpadding="0" cellspacing="0">
	   <tr align="center">
	   	<td class="tbltext3"> 
	   		Errore Brutto!!! 
	   	</td>
	   </tr>
	</table>
	<br>
<%
else
%>
<form METHOD="POST" onsubmit="return ValidaRisposta(this)" name="frmInsRisp" action="RIC_InsRispRicAss.asp">
<input type="hidden" name="hIdRic" id="hIdRic" value="<%=nIdRich%>">
<input type="hidden" name="hModo" id="hModo" value="Ins">
<input type="hidden" name="hDtTmst" id="hDtTmst" value="<%=rsRicAss("DT_TMST")%>">
<table border="0" cellspacing="2" cellpadding="1" width="500">
    <%
    if rsRicAss("COD_AREA") > "" then
    %>
    <tr> 
		<td align="left" nowrap class="tbltext1">
			<b>Area</b>
		</td>
		<td class="textblack">
			<%
			'DecCodVal("AREAT", 0, date, rsRicAss("cod_area"),1)
			Response.Write DecodArea(rsRicAss("cod_area"))
			%>
		</td>
    </tr>    
    <%
    end if
    %>
    <tr> 
		<td align="left" nowrap class="tbltext1" width="150">
			<b>Mittente</b>
		</td>
		<td class="textblack">
			<%=rsRicAss("utente")%>
		</td>
	</tr>
	<tr> 
		<td align="left" nowrap class="tbltext1" width="150">
			<b>Oggetto</b>
		</td>
		<td class="textblack">
			<%=rsRicAss("OGGETTO")%>
		</td>
	</tr>
	<tr>
		<td align="left" nowrap class="tbltext1">
			<b>Testo Domanda</b>
		</td>
		<td class="textblack">
			<%=rsRicAss("TESTO_DOM")%>
		</td>
	</tr>
	<tr>
		<td align="left" nowrap class="tbltext1">
			<b>Data Ricezione</b>
		</td>
		<td class="textblack">
			<%=ConvDateToString(rsRicAss("DT_INVIO"))%>
		</td>
	</tr>
    <tr> 
		<td align="left" nowrap class="tbltext1">
			<b>Testo Risposta</b>*
		</td>
		<td>
			<span class="textblack">&nbsp;- Utilizzabili <label name="lblNumCar1" id="lblNumCar1">2000</label> caratteri -</span>
			<textarea name="txtTestoRisposta" class="textblack" cols="50" rows="5" OnKeyUp="JavaScript:CheckLenTextArea(txtTestoRisposta,lblNumCar1,2000)"></textarea>
		</td>
    </tr>
</table>
<br>
<table width="500" border="0" cellspacing="2" cellpadding="1">
	<tr align="center">
		<td align="right">			
			<%
				PlsChiudi()
			%>
		</td>
		<td align="left">
			<%
				PlsInvia("InserisciR")
			%>
		 </td>
	</tr>
</table> 
</form> 
<br>
<%
end if
End Sub
';););););););););););););););););););););););););););););););););););););););)
Sub VisMessaggio(sMess)
%>
<br>
<table border="0" width="500" cellpadding="0" cellspacing="0">
   <tr align="center">
   	<td class="tbltext3"> 
   		<%=sMess%>
   	</td>
   </tr>
</table>
<br>
<%
End Sub
';););););););););););););););););););););););););););););););););););););););)
Sub SalvaRich()
dim sSQL
dim sTestoRisp, nIdRic, dDtTmst
dim sErrore

nIdRic = Request.Form("hIdRic")
sTestoRisp = replace(Request.Form("txtTestoRisposta"),"'","''")
dDtTmst = Request.Form("hDtTmst")

sSQL = "Update RIC_ASS Set " &_
		"TESTO_RISP = '" & sTestoRisp & "', " &_
		"DT_RISPOSTA = " & convdatetodb(Date()) & ", " &_
		"DT_TMST = " & convdatetodb(Now()) & " " &_
		"Where ID_RIC_ASS = " & nIdRic
		
sErrore=Esegui(nIdRic, "RIC_ASS", Session("idPersona"), "MOD", sSQL, 1, dDtTmst)
if sErrore = "0" then
	VisMessaggio("Richiesta di Assistenza correttamente evasa")
else
	VisMessaggio("Errore durante l'evasione della Richiesta di Assistenza:<br>" & sErrore)
end if
%>
<table border="0" width="500" cellpadding="0" cellspacing="0">
	<tr align="center">
		<td> 
			<a href="javascript:ChiudiERicarica()">
				<img src="<%=Session("progetto")%>/images/chiudi.gif" border="0" onmouseover="javascript:window.status='' ; return true">
			</a>
		</td>
	</tr>
</table>
<br>
<%
End Sub
';););););););););););););););););););););););););););););););););););););););)
'M A I N
%>
<!--#include virtual="/include/openconn.asp"-->
<!--#include virtual="/include/DecCod.asp"-->
<!--#include virtual="/include/SetPulsanti.asp"-->
<!--#include virtual="/include/ControlDateVB.asp"-->
<!--#include virtual="/Include/SysFunction.asp"-->
<%
Inizio()
if Request.Form("hModo") = "Ins" then
	SalvaRich()
else
	ImpostaPag()
end if
%>
<!--#include virtual="/include/closeconn.asp"-->
