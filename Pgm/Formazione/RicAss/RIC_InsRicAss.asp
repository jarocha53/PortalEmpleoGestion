<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%
Sub ControlliJavaScript()
%>
<script language="Javascript">
<!--#include virtual="/include/ControlDate.inc"-->
<!--#include virtual="/include/ControlNum.inc"-->
<!--#include virtual="/include/ControlString.inc"-->
function ValidaRichieste(frmRic)
{
/*
	var nConta=0
	var nInd=0
    for (var i=0;i<frmRic.radDest.length;i++) 
    {
        if (frmRic.radDest[i].checked)
        {
			nConta++
			if ((frmRic.radDest[i].value == "ES") && (frmRic.cmbArea.value == ""))
			{
				frmRic.cmbArea.focus();
				alert("Il campo 'Area' � obbligatorio.");
				return (false);				
			}
        }
    }
    if (nConta==0)
    {
		alert("Selezionare un 'Destinatario'");
		return false;    
    }
*/
	if ((frmRic.radDest.value == "ES") && (frmRic.cmbArea.value == ""))
	{
		frmRic.cmbArea.focus();
		alert("Il campo 'Area' � obbligatorio.");
		return (false);				
	}

	if (frmRic.txtOggetto.value == "")
	{
		frmRic.txtOggetto.focus();
		alert("Il campo 'Oggetto' � obbligatorio.");
		return (false);
  	}
	if (frmRic.txtTestoDomanda.value == "")
	{
		frmRic.txtTestoDomanda.focus();
		alert("Il campo 'Testo Domanda' � obbligatorio.");
		return (false);
  	}
	return (true);
}
</script>
<%
End Sub
'-----------------------------------------------------------------------
Sub Inizio()
	dim sFunzione
	dim sTitolo
	dim sCommento
	dim bCampiObbl
	
	sFunzione = "RICHIESTA DI ASSISTENZA"
	sTitolo = "RICHIESTA DI ASSISTENZA"
	sCommento = "Da questa pagina puoi inviare la tua richiesta di " &_
				"supporto all'assistente da te selezionato. Digita la tua " &_
				"domanda nella casella sottostante e fai click su <b>Invia</b>"
	bCampiObbl = True
	sHelp="../../help/formazione/RicAss/RIC_InsRicAss"
%>
	<!--#include virtual="/include/SetTestata.asp"-->
<%
End Sub
'-----------------------------------------------------------------------
Function ImpostaDestinatario(sCodRuo)

select case sCodRuo
	case "TU"
		ImpostaDestinatario = "TUTOR"
	case "TS"
		ImpostaDestinatario = "TUTOR STAGE"	
	case "ME"
		ImpostaDestinatario = "MENTOR"
	case "ES"
		ImpostaDestinatario = "ESPERTO"
	case else
		ImpostaDestinatario = "***"
end select

End Function
'-----------------------------------------------------------------------
Sub CaricaCombo()
dim sSQL, rsArea

sSQL = " SELECT DISTINCT E.ID_AREATEMATICA, A.DESCRIZIONEAREA" &_
		" FROM ESPERTOAREA E, AREA A" &_
		" WHERE A.ID_AREA = E.id_areatematica AND" &_
		" SYSDATE BETWEEN E.DAL AND E.AL"
'Response.Write sSQL
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
set rsArea = CC.Execute(sSQL)
%>
	<select name="cmbArea" id="cmbarea" class="textblack">
		<option value="" selected></option>
<%
	do while not rsArea.eof
		Response.Write "<option value=" & chr(34) & rsArea("ID_AREATEMATICA") & chr(34) & ">" & rsArea("DESCRIZIONEAREA") & "</option>"
		rsArea.movenext
	loop
%>
	</select>
<%
rsArea.close
set rsArea = nothing
End Sub
'-----------------------------------------------------------------------
Sub ImpostaPag()
dim sCodRuo
sCodRuo = Request.Form("hDest")
%>
<br>
<form METHOD="POST" onsubmit="return ValidaRichieste(this)" name="frmInsRich" action="RIC_CnfInsRicAss.asp">
<table border="0" cellspacing="2" cellpadding="1" width="500">
	<tr> 
		<td align="left" nowrap class="tbltext1">
			<b>Destinatario</b>*
		</td>
		<td class="textBlack">
			<b><%=ImpostaDestinatario(sCodRuo)%></b>
			<input type="hidden" name="radDest" id="radDest" value="<%=sCodRuo%>">
		</td>
	</tr>
	<tr>
		<td align="left" nowrap class="tbltext1">
			<b>Area</b>
		</td>
		<td class="tbltext1">
			<%
			'CreateCombo("AREAT||||cmbArea||")
			CaricaCombo()
			if sCodRuo <> "ES" then
			%>
			<script language=javascript>
				frmInsRich.cmbArea.disabled=true;
			</script>
			<%
			end if
			%>
		</td>
	</tr>
	<tr>
		<td align="left" nowrap class="tbltext1">
			<b>Oggetto</b>*
		</td>
		<td class="tbltext1">
			<input type="text" name="txtOggetto" id="txtOggetto" maxlength="50" size="50" class="textblacka">
		</td>
	</tr>
    <tr> 
		<td align="left" nowrap class="tbltext1">
			<b>Testo Domanda</b>*
		</td>
		<td>
			<span class="tbltext1">&nbsp;- Utilizzabili <label name="lblNumCar1" id="lblNumCar1">2000</label> caratteri -</span>
			<textarea name="txtTestoDomanda" class="textblack" cols="50" rows="5" OnKeyUp="JavaScript:CheckLenTextArea(txtTestoDomanda,lblNumCar1,2000)"></textarea>
		</td>
    </tr>
</table>
<br>
<table width="500" border="0" cellspacing="2" cellpadding="1">
	<tr align="center">
		<td align="right">			
			<%
				PlsIndietro()
			%>
		</td>
		<td align="left">
			<%
				PlsInvia("InserisciR")
			%>
		 </td>
	</tr>
</table> 
</form> 
<br>
<%
End Sub
'------------------------------------------------------------------------
%>
<!--#include virtual="/strutt_testa2.asp"-->
<!--#include virtual="/include/openconn.asp"-->
<!--#include virtual="/include/DecCod.asp"-->
<!--#include virtual="/include/SetPulsanti.asp"-->
<%	
if ValidateService(session("idutente"),"RIC_VISRICHIESTE",CC) <> "true" then 
	response.redirect "/util/error_login.asp"
end if

	ControlliJavaScript()
	Inizio()
	ImpostaPag()
%>
<!--#include virtual="/include/closeconn.asp"-->
<!--#include virtual="/strutt_coda2.asp"-->
