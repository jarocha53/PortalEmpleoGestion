<!--#include Virtual="/strutt_testa2.asp"-->
<!--#include virtual="/Include/OpenConn.asp"-->
<!--#include virtual="/Include/ControlDateVB.asp"-->
<!--#include file="GRA_Inizio.asp"-->
<script language="Javascript" src="Controlli.js"></script>
<script language="Javascript">
<!--#include virtual = "/Include/ControlDate.inc"-->
<!--#include virtual = "/Include/help.inc"-->

function VediRegola() {

	var i;
	i = 0;
	while (document.frmInsGruAss.cmbRegole.length != 1) 
		document.frmInsGruAss.cmbRegole[i+1].removeNode();
	
	if (document.frmInsGruAss.rdTRegE.checked) {
		// Carico la combo con i dati relativi 
		// al Mentor Ruolo.
		for(i=0;i<aRegola.length;i++) {
			aAppo = aRegola[i].split("|||");
			if ( aAppo[1] == "MENTOR" ) {
				sDesc = aAppo[0].split("||");
				z = document.frmInsGruAss.cmbRegole.length + 1
				document.frmInsGruAss.cmbRegole.length = z
				document.frmInsGruAss.cmbRegole[parseInt(z,10)-1].value = aAppo[0];
				document.frmInsGruAss.cmbRegole[parseInt(z,10)-1].text = sDesc[1] + ' ( ' + aAppo[1] + ' )';
			}
		}
	} else {
		// Carico la combo con i dati relativi 
		// al Tutor Ruolo.
		for(i=0;i<aRegola.length;i++) {
			aAppo = aRegola[i].split("|||");
			if ( aAppo[1] == "TUTOR" ) {
				sDesc = aAppo[0].split("||");
				z = document.frmInsGruAss.cmbRegole.length + 1
				document.frmInsGruAss.cmbRegole.length = z
				document.frmInsGruAss.cmbRegole[parseInt(z,10)-1].value = aAppo[0];
				document.frmInsGruAss.cmbRegole[parseInt(z,10)-1].text = sDesc[1] + ' ( ' + aAppo[1] + ' )';
			}
		}
	}
}

</script>
<%

dim fModo

Inizio()


' Identifico se devo segliere la regola oppure il tutore
fModo = Request.Form("Modo")
select case fModo
	case ""		' Devo selezionare la regola da applicare.
		InfoProcedura("La funzione permette l'inserimento di nuovi gruppi di assistenza.")
%>
	<form name="frmInsGruAss" method="post" onsubmit="return CheckRegola()" action="GRA_InsGruAss.asp">
	<input type="hidden" name="Modo" value="1">
	<br>
	<table border="0" CELLPADDING="0" CELLSPACING="0" width="500" align="center">
		<tr>
			<td width="200" class="tbltext1BFad">
				Selezionare il tipo di assistente*
			</td>
			<td width="300" class="TextBlack">
				Mentor <input onclick="Javascript:VediRegola()" checked class="TextBlack" type="radio" id="rdTRegE" name="rdTReg" value="E">
				Tutor <input onclick="Javascript:VediRegola()" class="TextBlack" type="radio" name="rdTReg" id="rdTRegM" value="M">
			</td>
		</tr>
		<tr>
			<td width="200" class="tbltext1BFad">
				Regole assistenza*
			</td>
			<td width="300">
				<%ComboRegole()%>
			</td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2" align="center">
				<a href="GRA_VisGruAss.asp" onmouseover="javascript: window.status=' '; 
				return true"><img src="<%=Session("Progetto")%>/images/indietro.gif" border="0"></a>
				<input type="image" src="<%=Session("Progetto")%>/images/Avanti.gif">
			</td>
		</tr>
	</table>

	</form>
<%	case "1"	' Devo scegliere gli eventuali tutori. 
		InfoProcedura("La funzione permette l'inserimento di nuovi gruppi di assistenza.")
		dim nIdRegola,sDescRegola
		
		nIdRegola	= clng(split(Request.Form("cmbRegole"),"||")(0))
		sDescRegola = split(Request.Form("cmbRegole"),"||")(1)
	
		sSQL = "SELECT REGOLA,DECODE(TIPO_REGOLA,'E','ME','U','TU') AS TIPO " &_
			" FROM REGOLE WHERE ID_REGOLE = " & nIdRegola
		set rsRegola = CC.Execute(sSQL)
		if not rsRegola.eof then
			sFiltro		= rsRegola("REGOLA")
			sCodRorga	= rsRegola("TIPO")
		else
			sFiltro = "" 
		end if
		rsRegola.Close
		set rsRegola = nothing 

		sSQL = "SELECT B.IDUTENTE FROM PERSONA A, UTENTE B, GRUPPO C " &_
			" WHERE A.ID_PERSONA = B.CREATOR AND C.IDGRUPPO = B.IDGRUPPO AND C.COD_RORGA = 'DI' AND " & sFiltro 
		sSQL = sSQL	& " AND NOT EXISTS (  SELECT AU.IDUTENTE FROM ASSISTENZAUTENTE AU, ASSISTENZAGRUPPO AG " &_
			" WHERE AU.ID_GRUTASS = AG.ID_GRUTASS AND COD_RUOLO='" & sCodRorga & "' AND AU.IDUTENTE = B.IDUTENTE ) ORDER BY ID_PERSONA"
		set rsConta = Server.CreateObject("ADODB.Recordset")

		rsConta.Open sSQL, CC, 3
		if not rsConta.EOF then
			%><form name="frmInsGruAss" method="post" action="GRA_CnfGruAss.asp" onsubmit="return Controlli()"><%
			aUtenti		= rsConta.GetRows
			nIdConto	= rsConta.RecordCount
			i=0 
			for i=0 to nIdConto-1
				Response.Write "<input type=""hidden"" name=""aUtenti"" value=""" &_
				 aUtenti(0,i) & """>"
			next%>			
			<br>
			<input type="hidden" name="txtNumUte" value="<%=nIdConto%>">			
			<input type="hidden" name="txtAction" value="INS">			
			<input type="hidden" name="txtOggi" value="<%=ConvDateToString(Date)%>">			
			<table border="0" CELLPADDING="2" CELLSPACING="2" width="500">
				<tr>
					<td align="left" width="200" class="tbltext1BFad">
						Regola selezionata  :
					</td>
					<td align="left" width="300" class="tbltextBFad">
						<%=sDescRegola%>
					</td>
				</tr>
				<tr>
					<td align="left" width="200" class="tbltext1BFad">
						Numero discenti identificati :
					</td>
					<td align="left" width="300" class="tbltextBFad">
						<%=nIdConto%>
					</td>
				</tr>
				<tr>
					<td align="left" width="200" class="tbltext1BFad">
					<%if sCodRorga = "ME" then%>
						Selezionare un Mentor* :
					<%else%>
						Selezionare un Tutor* :
					<%end if%>
					
					</td>
					<td align="left" width="300">
						<%ComboMenTut(sCodRorga)%>
					</td>
				</tr>
				<tr>
					<td align="left" width="200" class="tbltext1BFad">
						Data inizio assistenza* :
					</td>
					<td align="left" width="300">
						<input maxlength="10" value="<%=ConvDateToString(Date)%>" type="text" size="12" class="textBlackFad" name="txtDtIniAss">			
					</td>
				</tr>
				<tr>
					<td align="left" width="200" class="tbltext1BFad">
						Data fine assistenza :
					</td>
					<td align="left" width="300">
						<input size="12" maxlength="10" type="text" class="textBlackFad" name="txtDtFinAss">			
					</td>
				</tr>
				<tr>
					<td colspan="2">&nbsp;</td>
				</tr>
			</table>
			<table border="0" CELLPADDING="2" CELLSPACING="2" width="500">
				<tr>
					<td width="50%" align="right">
						<input type="image" src="<%=Session("Progetto")%>/images/Conferma.gif" name="image1">
					</td>
					<td width="50%" align="left">
					<a href="GRA_InsGruAss.asp" onmouseover="javascript: window.status=' '; 
						return true"><img src="<%=Session("Progetto")%>/images/indietro.gif" border="0"></a>
					</td>
				</tr>
			</table>
			</form>				

<%		else%>
			<br><br>
			<table border="0" CELLPADDING="2" CELLSPACING="2" width="500">
				<tr>
					<td class="tbltext3" align="right">
						Nessun discente ritrovato con il raggruppamento selezionato.
					</td>
				</tr>
			</table>
			<br>
			<table border="0" CELLPADDING="2" CELLSPACING="2" width="500">
				<tr>
					<td width="50%" align="center">
						<a href="GRA_InsGruAss.asp" onmouseover="javascript: window.status=' '; 
						return true"><img src="<%=Session("Progetto")%>/images/indietro.gif" border="0"></a>
					</td>
				</tr>
			</table>
			
		<%end if
		rsConta.Close
		set rsConta  = nothing%> 
	<%case "2"	
		
		sTipo = Request("Assistente")
		
		InfoProcedura("La funzione permette di associare le persone rimaste ad un gruppo precedentemente creato.")
		' Devo prendere il numero delle persone e gli id che rimando
		%>
		<form name="frmInsGruAss" method="post" onsubmit="return Controlla()" action="GRA_CnfGruAss.asp">
		<input type="hidden" name="txtAction" value="ASS">			
	
		<%	
		if sTipo = "ME" then
			sSQL = "SELECT B.IDUTENTE FROM PERSONA A, UTENTE B, GRUPPO C " &_
				" WHERE A.ID_PERSONA = B.CREATOR AND C.COD_RORGA = 'DI' AND  C.IDGRUPPO = B.IDGRUPPO "
			sSQL = sSQL	& " AND NOT EXISTS ( SELECT AU.IDUTENTE FROM ASSISTENZAUTENTE AU, ASSISTENZAGRUPPO AG " &_
				" WHERE AU.ID_GRUTASS = AG.ID_GRUTASS AND COD_RUOLO='ME' AND AU.IDUTENTE = B.IDUTENTE ) ORDER BY ID_PERSONA"
			set rsConta = Server.CreateObject("ADODB.Recordset")
			rsConta.Open sSQL, CC, 3
			if not rsConta.EOF then
				aUtenti		= rsConta.GetRows
				nIdConto	= rsConta.RecordCount
				i=0 
				for i=0 to nIdConto-1
					Response.Write "<input type=""hidden"" name=""aMEUtenti"" value=""" &_
					 aUtenti(0,i) & """>"
				next
			end if
		
			rsConta.close
			set rsConta = nothing
		else
			sSQL = "SELECT B.IDUTENTE FROM PERSONA A, UTENTE B, GRUPPO C " &_
				" WHERE A.ID_PERSONA = B.CREATOR AND C.COD_RORGA = 'DI'  AND  C.IDGRUPPO = B.IDGRUPPO  "
			sSQL = sSQL	& " AND NOT EXISTS ( SELECT AU.IDUTENTE FROM ASSISTENZAUTENTE AU, ASSISTENZAGRUPPO AG " &_
				" WHERE AU.ID_GRUTASS = AG.ID_GRUTASS AND COD_RUOLO='TU' AND AU.IDUTENTE = B.IDUTENTE ) ORDER BY ID_PERSONA"
			set rsConta = Server.CreateObject("ADODB.Recordset")
			rsConta.Open sSQL, CC, 3
			if not rsConta.EOF then
				aUtenti		= rsConta.GetRows
				nIdConto	= rsConta.RecordCount
				i=0 
				for i=0 to nIdConto-1
					Response.Write "<input type=""hidden"" name=""aTUUtenti"" value=""" &_
					 aUtenti(0,i) & """>"
				next
			end if
		
			rsConta.close 
			set rsConta = nothing
		end if
		%>	
		<br>		
		<table border="0" CELLPADDING="2" CELLSPACING="2" width="500">
			<tr>
				<td width="170" class="tblText1BFad">
				Elenco dei gruppi disponibili*
				</td>
				<td width="330" class="tblTextBFad"> 
				<%ComboAssGruppi(sTipo)%>
				</td>
			</tr>
			<tr>
				<td colspan="2">&nbsp;</td>
			</tr>
			<tr>
				<td colspan="2" align="center">
					<a href="GRA_VisGruAss.asp" onmouseover="javascript: window.status=' '; 
					return true"><img src="<%=Session("Progetto")%>/images/indietro.gif" border="0"></a>
					<input type="image" src="<%=Session("Progetto")%>/images/Avanti.gif" name="Avanti">
				</td>
			</tr>
		</table>
		</form>
			
<%end select%> 

<!--#include virtual="/Include/CloseConn.asp"-->
<!--#include Virtual="/strutt_coda2.asp"-->

<%

sub ComboMenTut(sRorga)
	dim sSQL, rsTutMen
	
	sSQL = "SELECT IDUTENTE,NOME,COGNOME,COD_RORGA FROM UTENTE U, GRUPPO GR " &_
		" WHERE U.IDGRUPPO = GR.IDGRUPPO AND COD_RORGA = '" & sRorga & "'" 

	set rsTutMen = CC.Execute(sSQL)
	if not rsTutMen.eof then%>
		<input type="hidden" value="<%=rsTutMen("COD_RORGA")%>" name="Ruolo">
		<select name="cmbMenTut" class="textBlackFad">
			<option></option>
	<%	do while not rsTutMen.eof%>
			<option value="<%=CLng(rsTutMen("IDUTENTE"))%>"><%=rsTutMen("NOME")%> - <%=rsTutMen("COGNOME")%></option>
			<%rsTutMen.MoveNext
		loop
	else

	end if
	rsTutMen.close
	set rsTutMen = nothing
end sub



Sub  ComboRegole
	dim sSQL,rsRegole
	
	sSQL = "SELECT ID_REGOLE,DESC_REGOLA," &_
		" DECODE(TIPO_REGOLA,'E','MENTOR','U','TUTOR') AS TIPO " &_
		" FROM REGOLE WHERE TIPO_REGOLA IN ('E','U') ORDER BY TIPO_REGOLA"
	set rsRegole = CC.Execute(sSQL)
	
	if not rsRegole.EOF then	
		set rsARegole = Server.CreateObject("ADODB.Recordset")
		rsARegole.Open sSQL, CC, 3
		aPippo = rsARegole.GetRows()
		rsARegole.Close
		set rsARegole = nothing%>
		<script language="JavaScript">
			var aRegola;
			aRegola = new Array('<%=ubound(aPippo,2)%>')
			<%for i = 0 to ubound(aPippo,2)	%>
				z = '<%=i%>';
				aRegola[parseInt(z,10)] = '<%=aPippo(0,i)%>';
				aRegola[parseInt(z,10)] = aRegola[parseInt(z,10)] + '||';
				aRegola[parseInt(z,10)] = aRegola[parseInt(z,10)] + '<%=aPippo(1,i)%>';
				aRegola[parseInt(z,10)] = aRegola[parseInt(z,10)] + '|||';
				aRegola[parseInt(z,10)] = aRegola[parseInt(z,10)] + '<%=aPippo(2,i)%>';
			<%next%>
		</script>
		<select class="textBlackFad" name="cmbRegole">
			<option></option>	
		<%do while not rsRegole.EOF%>
			<option value="<%=rsRegole("ID_REGOLE")%>||<%=rsRegole("DESC_REGOLA")%>"><%=rsRegole("DESC_REGOLA")%> ( <%=rsRegole("TIPO")%> )</option>	
			<%rsRegole.MoveNext()
		loop%>
	</select>

	<script language="JavaScript">
		VediRegola()
	</script>	

	<%else
	
	end if
	rsRegole.Close
	set rsRegole = nothing 

end sub
%>
<%'Sub InfoProcedura(sCommento)%>
<%Sub InfoProcedura(sCommento)%>
<!-- Lingetta superiore 	Scrittura dell'intestazione della pagina-->
	<br>
	<table border="0" CELLPADDING="0" CELLSPACING="0" width="500" align="center">
		<tr height="18">
			<td class="sfondomenu" width="67%" height="18"><span class="tbltext0"><b>&nbsp;ASSISTENZA UTENTI</b></span></td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
			<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
		</tr>
	</table>
	<!-- Commento -->
	<table border="0" CELLPADDING="0" CELLSPACING="0" width="500" align="center">
		<tr>
			<td align="left" class="sfondocommaz">
				<%=sCommento%>
				<a onmouseover="javascript: window.status=' '; 
					return true" href="Javascript:Show_Help('/pgm/help/Formazione/GruppiAssistenza/GRA_InsGruAss')"><img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
			</td>
		</tr>
		<tr height="2">
			<td class="sfondocommaz" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>
<%End Sub%>



<%
sub ComboAssGruppi(sTipo)
	dim dNow

	dNow = ConvDateToDBs(Date)


'	sSQL = "SELECT ID_ASSGR FROM ASSISTENZAGRUPPO WHERE  " &_
'		" (SYSDATE BETWEEN DT_INIASS AND DT_FINEASS " & _
'		" OR (SYSDATE > DT_INIASS AND DT_FINEASS IS NULL)) "
'	sSQL = "SELECT ID_GRUTASS,ID_ASSGR, NOME,COGNOME " &_
'		"FROM ASSISTENZAGRUPPO AG, UTENTE U WHERE AG.IDUTENTE = U.IDUTENTE " &_
'		"AND (SYSDATE BETWEEN DT_INIASS AND DT_FINEASS " & _
'		" OR (SYSDATE > DT_INIASS AND DT_FINEASS IS NULL)) "	

	sSQL = "SELECT ID_GRUTASS,ID_ASSGR, NOME,COGNOME " &_
		"FROM ASSISTENZAGRUPPO AG, UTENTE U WHERE AG.IDUTENTE = U.IDUTENTE " &_
		" AND STATO_ASS='A' AND COD_RUOLO ='" & sTipo & "'" 

'		"AND (" & dNow & " BETWEEN DT_INIASS AND DT_FINEASS " & _
'		" OR (" & dNow & " >= DT_INIASS AND DT_FINEASS IS NULL)) AND STATO_ASS='A'"

	set rsGruppi = Server.CreateObject("ADODB.Recordset")
	rsGruppi.Open sSQL, CC, 3

	if not rsGruppi.EOF then
		if rsGruppi.RecordCount > 1 then%>
			<select name="cmbGruppiAss" class="textBlackFad">	
				<option value></option>
			<%do while not rsGruppi.EOF%>
				<option value="<%=clng(rsGruppi("ID_GRUTASS"))%>"><%=clng(rsGruppi("ID_ASSGR"))%> - <%=rsGruppi("NOME")%>&nbsp;<%=rsGruppi("COGNOME")%></option>
			<%rsGruppi.MoveNext() 
			loop%>
			</select>
		<%else%>	
			<%=clng(rsGruppi("ID_ASSGR"))%> - <%=rsGruppi("NOME")%>&nbsp;<%=rsGruppi("COGNOME")%>			
			<input type="hidden" name="cmbGruppiAss" value="<%=clng(rsGruppi("ID_GRUTASS"))%>">
		<%end if
	else
		Response.Write "Errore nella ricerca del gruppo"
	end if
	rsGruppi.Close
	set rsGruppi = nothing  


end sub%>