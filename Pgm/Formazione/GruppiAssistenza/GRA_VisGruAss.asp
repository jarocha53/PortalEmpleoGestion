<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include Virtual="/strutt_testa2.asp"-->
<!--#include virtual="/Include/OpenConn.asp"-->
<!--#include virtual="/Include/ControlDateVB.asp"-->
<!--#include virtual="/Include/DecCod.asp"-->
<!--#include virtual="/Include/SysFunction.asp"-->
<!--#include virtual="/Include/RuoloFunzionale.asp"-->

<!--#include virtual="/Util/dbUtil.asp"-->
<!--#include file="GRA_Inizio.asp"-->

<script language="javascript">
	function AssGrup(sTipo,nIdUorg){

		frmRaggruppa.Assistente.value = sTipo;
		frmRaggruppa.UnitaOrga.value = nIdUorg;
		frmRaggruppa.submit();
	}


	function DettPers(id)
	{
		
		var newWin;
		
		newWin = window.open("/pgm/formazione/GruppiAssistenza/GRA_DettAss.asp?idGruppo=" + id,"Info","width=520,height=400,Resize=No,Scrollbars=yes,screenX=w,screenY=h"); 
			
	}
 

	<!--#include virtual = "/Include/help.inc"-->

	function Carica_Pagina(npagina)
	{
		document.frmPagina.pagina.value = npagina;
		document.frmPagina.submit();
	}
	
</script>

<%
dim nIdGrAss,dDtIniAss,dtFinAss,sCodRuolo,sTutore
dim dNow

dNow = ConvDateToDBs(Date)
' *************************************************
' Questa funzione scioglie i gruppi che al momento 
' non sono pi� validi.
SciogliGruppiScaduti()
' *************************************************

Inizio()
InfoProcedura()

' *************************************************
' Ritrovo le persone che non hanno un MENTOR assegnato.
' OLD 11/04/2003
'sSQL = "SELECT COUNT(ID_PERSONA) as NUM_PER FROM PERSONA A, UTENTE B, GRUPPO C " &_
'	" WHERE A.ID_PERSONA = B.CREATOR AND C.IDGRUPPO = B.IDGRUPPO AND C.COD_RUOFU = 'DI' "  
'sSQL = sSQL	& " AND NOT EXISTS ( SELECT AU.IDUTENTE FROM ASSISTENZAUTENTE AU, ASSISTENZAGRUPPO AG " &_
'	" WHERE AU.ID_GRUTASS = AG.ID_GRUTASS AND COD_RUOLO='ME' AND AU.IDUTENTE = B.IDUTENTE) ORDER BY ID_PERSONA"

' ********************************
' EPILI 14/04/2003
sSQL = "  SELECT COUNT(ID_PERSONA) AS NUM_PERS, NVL(C.ID_UORG,0) AS ID_UORG  FROM COMPONENTI_CLASSE CC, CLASSE C ,UTENTE U"
sSQL = sSQL &  "   WHERE CC.ID_PERSONA = U.CREATOR "
sSQL = sSQL &  "   AND C.ID_CLASSE = CC.ID_CLASSE  "
sSQL = sSQL &  "   AND DT_RUOLOAL IS NULL "
sSQL = sSQL &  "    AND NOT EXISTS ( SELECT AU.IDUTENTE FROM ASSISTENZAUTENTE AU, ASSISTENZAGRUPPO AG "
sSQL = sSQL &  "  WHERE AU.ID_GRUTASS = AG.ID_GRUTASS AND COD_RUOLO='ME' AND AU.IDUTENTE = U.IDUTENTE)"
sSQL = sSQL &  "  GROUP BY C.ID_UORG ORDER BY  NUM_PERS DESC"
'Response.write 	sSQL
set rsNumPer = Server.CreateObject("ADODB.Recordset") 
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
rsNumPer.Open sSQL,cc,3
if not rsNumPer.eof then
	aDatiMentor = rsNumPer.GetRows()
else
	dim aDatiMentor(1,0)
	aDatiMentor(0,0) = 0 
	aDatiMentor(1,0) = 0 
end if
rsNumPer.Close
set rsNumPer = nothing


' *************************************************

' *************************************************
' Ritrovo le persone che non hanno un TUTOR assegnato.
' OLD EPILI -> 11/04/2003 <-
'sSQL = "SELECT COUNT(ID_PERSONA) as NUM_PER FROM PERSONA A, UTENTE B, GRUPPO C " &_
'	" WHERE A.ID_PERSONA = B.CREATOR AND C.IDGRUPPO = B.IDGRUPPO AND C.COD_RUOFU = 'DI' "  
'sSQL = sSQL	& " AND NOT EXISTS ( SELECT AU.IDUTENTE FROM ASSISTENZAUTENTE AU, ASSISTENZAGRUPPO AG " &_
'	" WHERE AU.ID_GRUTASS = AG.ID_GRUTASS AND COD_RUOLO='TU' AND AU.IDUTENTE = B.IDUTENTE) ORDER BY ID_PERSONA"

' ********************************
' EPILI 14/04/2003
sSQL = "  SELECT COUNT(ID_PERSONA) AS NUM_PERS, NVL(C.ID_UORG,0) AS ID_UORG FROM COMPONENTI_CLASSE CC, CLASSE C ,UTENTE U"
sSQL = sSQL &  "   WHERE CC.ID_PERSONA = U.CREATOR "
sSQL = sSQL &  "   AND C.ID_CLASSE = CC.ID_CLASSE  "
sSQL = sSQL &  "   AND DT_RUOLOAL IS NULL "
sSQL = sSQL &  "    AND NOT EXISTS ( SELECT AU.IDUTENTE FROM ASSISTENZAUTENTE AU, ASSISTENZAGRUPPO AG "
sSQL = sSQL &  "  WHERE AU.ID_GRUTASS = AG.ID_GRUTASS AND COD_RUOLO='TU' AND AU.IDUTENTE = U.IDUTENTE)"
sSQL = sSQL &  "  GROUP BY C.ID_UORG ORDER BY  NUM_PERS DESC"
'Response.write 	sSQL
set rsNumPer = Server.CreateObject("ADODB.Recordset") 
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
rsNumPer.Open sSQL,cc,3
if not rsNumPer.eof then
	aDatiTutor = rsNumPer.GetRows()
else
	dim aDatiTutor(1,0)
	aDatiTutor(0,0) = 0 
	aDatiTutor(1,0) = 0 
end if
rsNumPer.Close
set rsNumPer = nothing
' *************************************************


' *************************************************
' Mi prendo i gruppi creati !!!!! - APERTI - !!!!!  .
sSQL = "SELECT U.ID_UORG, NUM_ASSISTITI, ID_GRUTASS, ID_ASSGR, NOME,COGNOME ,DT_INIASS," &_
	"DECODE(STATO_ASS,'A','APERTA','C','CHIUSO','S','SOSTITUITA','R','CAMBIO TUTOR/MENTOR') AS STATO,COD_RUOLO " &_
	" FROM ASSISTENZAGRUPPO AG, UTENTE U WHERE AG.IDUTENTE = U.IDUTENTE " &_
	"AND STATO_ASS in ('A') " 
' Response.Write sSQL

'	"AND (" & dNow & " BETWEEN DT_INIASS AND DT_FINEASS " & _
'	" OR (" & dNow & " >= DT_INIASS AND DT_FINEASS IS NULL)) AND STATO_ASS in ('A')  "

set rsAssGruppo = Server.CreateObject("ADODB.Recordset")
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
rsAssGruppo.Open sSQL, CC, 3

if not rsAssGruppo.EOF then
	sNumGruppo = clng(rsAssGruppo.RecordCount)
	do while not rsAssGruppo.EOF
		if rsAssGruppo("COD_RUOLO") = "ME" then
			nMen = 1
		end if		
		rsAssGruppo.Movenext
	loop
	rsAssGruppo.MoveFirst

	do while not rsAssGruppo.EOF
		if rsAssGruppo("COD_RUOLO") = "TU" then
			nTut = 1
		end if		
		rsAssGruppo.MoveNext
	loop
	rsAssGruppo.MoveFirst
	
end if
%>
<form name="frmPagina" method="post" action="GRA_VisGruAss.asp">
	<input type="hidden" name="pagina" value>
</form>
<%if (CInt(aDatiMentor(0,0)) > 0) or (CInt(aDatiTutor(0,0)) > 0) then%>
	<%if (CInt(aDatiMentor(0,0)) > 0) then %>

		<table width="500px" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td align="left" class="sfondoCommFad">
					<b>Numero di discenti a cui assegnare un <u>Mentor</u> divisi 
					per unit� orgnanizzativa</b>
				</td>
			</tr>
		</table>

	<%	for i = 0 to ubound(aDatiMentor,2)%>

			<table width="500px" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="5%" align="left" class="tblText1bFad">
						&nbsp;			 
					</td>
					<td width="60%" align="left" class="tblText1bFad">
						<%=DecUniOrga(CLng(aDatiMentor(1,i)))%> : 
					</td>
					<td align="left" class="tblTextFad">
						<%=CInt(aDatiMentor(0,i))%>
					</td>
			<%if nMen > 0 and CInt(aDatiMentor(0,i)) > 0 then%>
					<td align="left"><a title="Associa a gruppo esistente" onmouseover="javascript: window.status=' '; 
						return true" href="Javascript:AssGrup('ME','<%=CLng(aDatiMentor(1,i))%>')" class="textRed"><img border="0" src="<%=Session("Progetto")%>/images/Formazione/Raggruppa.gif" WIDTH="36" HEIGHT="18"></a>
					</td>
			<%end if%>		
				</tr>
			</table> 
	<%	next 
	end if
	if (CInt(aDatiTutor(0,0)) > 0) then %>

		<br>
		<table width="500px" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td align="left" class="sfondoCommFad">
					<b>Numero di discenti  a cui assegnare un <u>Tutor</u> divisi 
					per unit� orgnanizzativa</b>
				</td>
			</tr>
		</table>

	<%	for i = 0 to ubound(aDatiTutor,2)%>
			<table width="500px" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="5%" align="left" class="tblText1bFad">
						&nbsp;			 
					</td>
					<td width="60%" align="left" class="tblText1bFad">
						 <%=DecUniOrga(CLng(aDatiTutor(1,i)))%>: </td>
					<td align="left" class="tblTextFad">
						<%=CInt(aDatiTutor(0,i))%>
					</td>
				<%if nTut > 0 and CInt(aDatiTutor(0,i)) > 0 then%>
					<td align="left"><a title="Associa a gruppo esistente" onmouseover="javascript: window.status=' '; 
						return true" href="Javascript:AssGrup('TU','<%=CLng(aDatiTutor(1,i))%>')" class="textRed"><img border="0" src="<%=Session("Progetto")%>/images/Formazione/Raggruppa.gif" WIDTH="36" HEIGHT="18"></a>
					</td>
				<%end if%>		
				</tr>
			</table> 
		<%next
	end if%>
	<form name="frmRaggruppa" method="post" action="GRA_InsGruAss.asp">
		<input type="hidden" name="Modo" value="2">
		<input type="hidden" name="Assistente">
		<input type="hidden" name="UnitaOrga">
	</form>

	<table width="500px" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td align="center"><a title="Crea nuovo gruppo" onmouseover="javascript: window.status=' '; 
				return true" href="GRA_InsGruAss.asp" class="textRed"><b>Crea nuovo gruppo</b></a>
			</td>
		</tr>
	</table> 
<%else%>
	<table width="500px" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td width="45%" align="center" class="tblText3">
			Non sono presenti discenti da raggruppare.
		</tr>
	</table> 

<%end if


Record=0
nTamPagina=15
If Request.Form("pagina")= "" Then
	nActPagina=1
Else
	nActPagina=Clng(Request.Form("pagina"))
End If

if not rsAssGruppo.EOF then

	rsAssGruppo.PageSize = nTamPagina
	rsAssGruppo.CacheSize = nTamPagina	

	nTotPagina = rsAssGruppo.PageCount		
	If nActPagina < 1 Then
		nActPagina = 1
	End If
	If nActPagina > nTotPagina Then
		nActPagina = nTotPagina
	End If

	rsAssGruppo.AbsolutePage=nActPagina
	nTotRecord=0

%>
	<br>
	<table width="500px" border="0" cellspacing="1" cellpadding="1">
		<tr class="sfondoCommFad">
			<td width="50" align="left"><b>Gruppo</b></td>
			<td width="120" align="left"><b>Assistito da </b></td>
			<td width="150" align="left"><b>Unit� Organizzativa</b></td>
			<td width="50" align="left"><b>Ruolo</b></td>
			<td width="75" align="left"><b>Dal</b></td>
<!--		<td width="130" align="left"><b>Fine assegnazione</b></td>-->
			<td width="50" align="left"><b>Assistiti</b></td>
			<td width="5" align="left"><b>Dettaglio</b></td>
		</tr>
	<%
	While rsAssGruppo.EOF <> True And nTotRecord < nTamPagina
		nIdGrAss	= CLng(rsAssGruppo("ID_ASSGR"))
		sTutore		= rsAssGruppo("NOME") & " " & rsAssGruppo("COGNOME")
		dDtIniAss	= rsAssGruppo("DT_IniAss")
'		dtFinAss	= rsAssGruppo("DT_FINEASS")
		fStatus		= rsAssGruppo("STATO")
		sCodRuolo	= rsAssGruppo("COD_RUOLO")
		nAssistiti  = rsAssGruppo("NUM_ASSISTITI")
		nGrutAss	= clng(rsAssGruppo("ID_GRUTASS"))
		sUnOrga		= DecUniOrga(clng(rsAssGruppo("ID_UORG")))
		%>
		<tr class="tblSfondoFad">
			<td align="left" class="tblAggFad"><a title="Modifica gruppo assistenza" onmouseover="javascript: window.status=' '; 
				return true" class="tblAggFad" href="Javascript:frmModifica<%=nTotRecord%>.submit()"><%=nIdGrAss%></a></td>
			<td align="left" class="tblDettFad"><%=sTutore%></td>
			<td align="left" class="tblDettFad"><%=sUnOrga%></td>
			<td align="left" class="tblDettFad"><%=DecRuolo(sCodRuolo)%></td>
			<td align="left" class="tblDettFad"><%=ConvDateToString(dDtIniAss)%></td>
			<!--td align="left" class="tblDettFad"><%'=ConvDateToString(dtFinAss)%></td-->
			<td align="left" class="tblDettFad"><%=nAssistiti%></td>
			<td align="right" class="tblDettFad"><a title="Informazioni persone del gruppo" onmouseover="javascript: window.status=' '; return true" href="Javascript:DettPers(<%=nGrutAss%>)"><img border="0" src="<%=Session("Progetto")%>/images/Formazione/GRUPPO.gif" WIDTH="23" HEIGHT="23"></a></td>

			<form name="frmModifica<%=nTotRecord%>" method="post" action="GRA_ModGruAss.asp">
				<input type="hidden" name="IDASSGR" value="<%=nIdGrAss%>">
			</form>
		</tr>
	
	<%
		nTotRecord=nTotRecord + 1	
		rsAssGruppo.MoveNext()
	wend%>
	</table>

<%else%>
	<br>
	<table width="500px" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td align="center" class="tblText3">
				Nessun raggruppamento formato
			</td>
		</tr>
	</table> 
<%end if
rsAssGruppo.close
set rsAssGruppo = nothing  

Response.Write "<TABLE border=0 width=470 align=center>"
Response.Write "<tr>"
if nActPagina > 1 then
	Response.Write "<td align=right width=480>"
	Response.Write "<A HREF=""javascript:Carica_Pagina('" & nActPagina-1 & "')"">"
	Response.Write "<img border=0 src=" & Session("Progetto") & "/images/precedente.gif></A></td>"
end if
if nActPagina < nTotPagina then
	Response.Write "<td align=right>"
	Response.Write "<A HREF=""javascript:Carica_Pagina('" & nActPagina+1 & "')"">"		
	Response.Write "<img border=0 src=" & Session("Progetto") & "/images/successivo.gif></A></td>"
end if
Response.Write "</tr></TABLE>"
%>

<!--#include virtual="/Include/CloseConn.asp"-->
<!--#include Virtual="/strutt_coda2.asp"-->
<%
Sub InfoProcedura()
%>
<!-- Lingetta superiore 	Scrittura dell'intestazione della pagina-->
	<br>
	<table border="0" CELLPADDING="0" CELLSPACING="0" width="500" align="center">
		<tr height="18">
			<td class="sfondomenu" width="67%" height="18"><span class="tbltext0"><b>&nbsp;ASSISTENZA UTENTI</b></span></td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
			<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
		</tr>
	</table>
	<!-- Commento -->
	<table border="0" CELLPADDING="0" CELLSPACING="0" width="500" align="center">
		<tr>
			<td align="left" class="sfondocommaz">
				Elenco dei gruppi di assistenza disponibili.
				<a title="Help" onmouseover="javascript: window.status=' '; 
					return true" href="Javascript:Show_Help('/pgm/help/Formazione/GruppiAssistenza/GRA_VisGruAss')"><img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
			</td>
		</tr>
		<tr height="2">
			<td class="sfondocommaz" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>
<%End Sub

sub SciogliGruppiScaduti()
	dim sSQL,rsSciogli,sIdGrutAss

	dNow = ConvDateToDBs(Date)
	
	CC.BeginTrans	

	sSQL = "SELECT AG.ID_ASSGR,AG.ID_GRUTASS FROM ASSISTENZAGRUPPO AG, ASSISTENZAUTENTE AU " &_
		" WHERE " & dNow &_
		" > DT_FINEASS AND AG.ID_GRUTASS = AU.ID_GRUTASS AND STATO_ASS ='A'" 

'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	set rsSciogli = CC.Execute(sSQL)
	
	if not rsSciogli.eof then
		sIdAssGr	= clng(rsSciogli("ID_ASSGR")) & ","
		sIdGrutAss	= clng(rsSciogli("ID_GRUTASS")) & ","
		rsSciogli.MoveNext
		do while not rsSciogli.eof
			sIdAssGr = sIdAssGr & clng(rsSciogli("ID_ASSGR")) & ","
			sIdGrutAss	= clng(rsSciogli("ID_GRUTASS")) & ","
			rsSciogli.MoveNext
		loop
		sIdAssGr = LEFT(sIdAssGr,len(sIdAssGr)-1)
		sIdGrutAss = LEFT(sIdGrutAss,len(sIdGrutAss)-1)
		sSQL = "UPDATE ASSISTENZAGRUPPO SET STATO_ASS = 'C' WHERE ID_ASSGR IN (" & sIdAssGr & ")"
		sErrore = EseguiNoC(sSQL,CC)
		if sErrore ="0" then 
			sSQL = "DELETE FROM ASSISTENZAUTENTE WHERE ID_GRUTASS IN (" & sIdGrutAss & ")"		
			sErrore = EseguiNoC(sSQL,CC)
		end if
	end if
	rsSciogli.Close
	set rsSciogli = nothing 
	if sErrore = "0" then
		CC.CommitTrans
	else
		CC.RollBackTrans
	end if
end sub

function DecUniOrga(nIdUorga)

	dim sSQL
	sSQL = "SELECT DESC_UORG FROM UNITA_ORGANIZZATIVA WHERE ID_UORG = " & nIdUorga
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	set rsDecUorg = CC.Execute(sSQL) 
	if not rsDecUorg.eof then
		DecUniOrga = rsDecUorg("DESC_UORG") 
	else
		DecUniOrga = ""
	end if
	rsDecUorg.close
	set rsDecUorg = nothing

end function%>
