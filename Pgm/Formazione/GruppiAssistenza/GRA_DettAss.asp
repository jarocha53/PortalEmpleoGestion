<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include virtual="/Include/CreaAlbero.asp"-->
<!--#include virtual="/Include/HTMLEncode.asp"-->
<!--#include virtual="/Include/OpenConn.asp"-->
<script language="Javascript">
	function Carica_Pagina(npagina)	{
		document.frmPagina.pagina.value = npagina;
		document.frmPagina.submit();
	}
</script>

<html>
<head>
<meta NAME="GENERATOR" Content="Microsoft Visual Studio 6.0">
<title>Informazioni discenti</title>
<link REL="STYLESHEET" TYPE="text/css" HREF="<%=session("progetto")%>/fogliostile.css">
</head>
<table border="0" CELLPADDING="0" CELLSPACING="0" width="100%">
	<tr height="18">
		<td class="sfondomenu" width="37%" height="18"><span class="tbltext0"><b>Informazioni discenti</b></span></td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
		<td valign="middle" align="right" class="tbltext1" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
	</tr>		
</table>
<table border="0" CELLPADDING="0" CELLSPACING="0" width="100%">
	<tr>
		<td align="left" class="sfondocommaz">
			Di seguito viene riportato l'elenco dei discenti del gruppo di assistenza 
			scelto. I dati visibili sono : nome, cognome, indirizzo email e 
			l'indirizzo della MailBox del portale.<br><br>		
		</td>
	</tr>	  
	<tr height="2">
		<td class="sfondocommFad" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif">
		</td>
	</tr>
</table>
<br>
<!-- Corpo della pagina-->


<table border="0" CELLPADDING="1" CELLSPACING="1" width="100%">
	<tr class="sfondocomm">
		<td><b>Cognome</b></td>
		<td><b>Nome</b></td>
		<td><b>Email</b></td>
		<td><b>MailBox</b></td>
	</tr>

<%
dim nIdGrutAss,nTotRecord
dim Record,Sql,nActPagina
dim nTamPagina

nIdGrutAss = clng(Request("idGruppo"))

Record=0
nTamPagina=15

If Request("pagina")= "" Then
	nActPagina=1
Else
	nActPagina=Clng(Request("pagina"))
End If

Sql = "SELECT "
Sql = Sql &  " 	   COGNOME,"
Sql = Sql &  " 	   NOME,"
Sql = Sql &  " 	   EMAIL,"
Sql = Sql &  " 	   LOGIN "
Sql = Sql &  " FROM "
Sql = Sql &  " 	 UTENTE A,"
Sql = Sql &  " 	 ASSISTENZAUTENTE B"
Sql = Sql &  " WHERE"
Sql = Sql &  " 	 A.IDUTENTE = B.IDUTENTE"
Sql = Sql &  " 	 AND ID_GRUTASS = " & nIdGrutAss
Sql = Sql &  " ORDER BY COGNOME,NOME" 	 
set RR = Server.CreateObject("ADODB.Recordset")

'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
RR.Open sql,CC,3

if not RR.EOF then	
	RR.PageSize = nTamPagina
	RR.CacheSize = nTamPagina	

	nTotPagina = RR.PageCount		
	If nActPagina < 1 Then
		nActPagina = 1
	End If
	If nActPagina > nTotPagina Then
		nActPagina = nTotPagina
	End If

	RR.AbsolutePage=nActPagina
	nTotRecord=0
	While RR.EOF <> True And nTotRecord < nTamPagina%>
		<tr class="tblsfondo">
			<td class="tblDett"><%=RR("COGNOME")%></td>
			<td class="tblDett"><%=RR("NOME")%></td>
			<td class="tblDett"><%=RR("EMAIL")%></td>
			<td class="tblDett"><%=RR("LOGIN")%></td>
		</tr>
<%		nTotRecord=nTotRecord + 1	
		RR.MoveNext
	Wend
	RR.Close
	set RR = nothing
	sql = ""
end if
%>	
<form method="post" action="GRA_DettAss.asp" name="frmPagina"> 
<input type="hidden" name="pagina">
<input type="hidden" name="idGruppo" value="<%=nIdGrutAss%>">
</form>
</table>


<table border="0" width="470" align="center">
	<tr>
	<%if nActPagina > 1 then%>
		<td align="right" width="480">
		<a HREF="javascript:Carica_Pagina('<%=nActPagina-1%>')">
		<img border="0" src="<%=Session("Progetto")%>/images/precedente.gif"></a></td>
	<%end if%>

	
	<%if nActPagina < nTotPagina then%>
		<td align="right">
		<a HREF="javascript:Carica_Pagina('<%=nActPagina+1%>')">		
		<img border="0" src="<%=Session("Progetto")%>/images/successivo.gif"></a></td>
	<%end if%>
	</tr>
</table>


<!-- Fine corpo della pagina-->
<br>
<table width="100%">
<tr>
<td align="center">
	<a href="Javascript:self.close()">
		<img border="0" src="<%=Session("Progetto")%>/images/chiudi.gif">
	</a>
</td>
</tr>
</table>
</body>
</html>
<!--#include virtual="/Include/CloseConn.asp"-->
