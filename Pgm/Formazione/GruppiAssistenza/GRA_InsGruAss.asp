<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include Virtual="/strutt_testa2.asp"-->
<!--#include virtual="/Include/OpenConn.asp"-->
<!--#include virtual="/Include/ControlDateVB.asp"-->
<!--#include file="GRA_Inizio.asp"-->
<script language="Javascript" src="Controlli.js"></script>
<script language="Javascript">
<!--#include virtual = "/Include/ControlDate.inc"-->
<!--#include virtual = "/Include/help.inc"-->

//include del file per fare i controlli sulla numericit� dei campi
<!--#include virtual = "/Include/ControlString.inc"-->
<!--#include virtual = "/Include/ControlNum.inc"-->
<!--#include Virtual = "/Include/ControlCodFisc.inc"-->

function VediRegola(aRegola,sNomeCampo,sPrimoValore) {
	var aRegola;
	var i;
	i = 0;
	
	// Svuoto la combo lasciando il valore nullo;
	while (document.all.item(sNomeCampo).length != 1 ) 
		document.all.item(sNomeCampo)[i+1].removeNode();
	
	if (document.frmInsGruAss.rdTRegE.checked) {
	
		if (sPrimoValore == "1") {
			z = document.all.item(sNomeCampo).length + 1
			document.all.item(sNomeCampo).length = z
			document.all.item(sNomeCampo)[0].value = "0||Manuale||ME";
			document.all.item(sNomeCampo)[0].text = "Manuale";
			document.all.item(sNomeCampo)[1].value = "0||Appartenenti all'unita organizzativa del mentor||ME";
			document.all.item(sNomeCampo)[1].text = "Appartenenti all'unita organizzativa del mentor";
		}
		// Carico la combo con i dati relativi 
		// al Mentor Ruolo.
		for(i=0;i<aRegola.length;i++) {
			aAppo = aRegola[i].split("|||");
			if ( aAppo[1] == "MENTOR" || aAppo[1] == "ME" ) {
				sDesc = aAppo[0].split("||");
				z = document.all.item(sNomeCampo).length + 1
				document.all.item(sNomeCampo).length = z
				document.all.item(sNomeCampo)[parseInt(z,10)-1].value = aAppo[0];
				document.all.item(sNomeCampo)[parseInt(z,10)-1].text = sDesc[1] + ' ( MENTOR )';
			}
		}
		
		if ( document.all.item(sNomeCampo).length == 1 ) {
			z = document.all.item(sNomeCampo).length + 1
			document.all.item(sNomeCampo).length = z
		//	document.all.item(sNomeCampo)[0].value = "0||Manuale||ME";
		//	document.all.item(sNomeCampo)[0].text = "Manuale";
		//	document.all.item(sNomeCampo)[1].value = "0||Appartenenti all'unita organizzativa del mentor||ME";
		//	document.all.item(sNomeCampo)[1].text = "Appartenenti all'unita organizzativa del mentor";
		}
				
	} else {
	
		if (sPrimoValore == "1") {
			z = document.all.item(sNomeCampo).length + 1
			document.all.item(sNomeCampo).length = z
			document.all.item(sNomeCampo)[0].value = "0||Manuale||TU";
			document.all.item(sNomeCampo)[0].text = "Manuale";
			document.all.item(sNomeCampo)[1].value = "0||Appartenenti all'unita organizzativa del tutor||TU";
			document.all.item(sNomeCampo)[1].text = "Appartenenti all'unita organizzativa del Tutor";
		}
		// Carico la combo con i dati relativi 
		// al Tutor Ruolo.
		for(i=0;i<aRegola.length;i++) {
			aAppo = aRegola[i].split("|||");
			if ( aAppo[1] == "TUTOR" || aAppo[1] == "TU" ) {
				sDesc = aAppo[0].split("||");
				z = document.all.item(sNomeCampo).length + 1
				document.all.item(sNomeCampo).length = z
				document.all.item(sNomeCampo)[parseInt(z,10)-1].value = aAppo[0];
				document.all.item(sNomeCampo)[parseInt(z,10)-1].text = sDesc[1] + ' ( TUTOR )';
			}
		}
		
		if ( document.all.item(sNomeCampo).length == 1 ) {
			z = document.all.item(sNomeCampo).length + 1
			document.all.item(sNomeCampo).length = z
		//	document.all.item(sNomeCampo)[0].value = "0||Manuale||TU";
		//	document.all.item(sNomeCampo)[0].text = "Manuale";
		//	document.all.item(sNomeCampo)[1].value = "0||Appartenenti all'unita organizzativa del tutor||TU";
		//	document.all.item(sNomeCampo)[1].text = "Appartenenti all'unita organizzativa del Tutor";
		}
		
	}
}

</script>
<%

dim fModo

Inizio()


' Identifico se devo segliere la regola oppure il tutore
fModo = Request.Form("Modo")
if clng(fModo) = 1 then
	nIdRegola	= clng(split(Request.Form("cmbRegole"),"||")(0))
	if nIdRegola = 0 then
		sDescRegola = split(Request.Form("cmbRegole"),"||")(1)
	end if
	if 	sDescRegola ="Manuale" then	
		fModo=3
	end if
end if
select case fModo
	case ""		' Devo selezionare la regola da applicare.
		InfoProcedura("La funzione permette l'inserimento di nuovi gruppi di assistenza.")
%>
	<form name="frmInsGruAss" method="post" onsubmit="return CheckRegola()" action="GRA_InsGruAss.asp">
	<input type="hidden" name="Modo" value="1">
	<br>
	<table border="0" CELLPADDING="2" CELLSPACING="1" width="500" align="center">
		<tr>
			<td width="200" class="tbltext1BFad">
				Selezionare il tipo di assistente*
			</td>
			<td width="300" class="TextBlack">
				Mentor <input onclick="Javascript:VediRegola(aRegola,'cmbRegole',1);VediRegola(aTeacher,'cmbMenTut',0);" checked class="TextBlack" type="radio" id="rdTRegE" name="rdTReg" value="E">
				Tutor <input onclick="Javascript:VediRegola(aRegola,'cmbRegole',1);VediRegola(aTeacher,'cmbMenTut',0);" class="TextBlack" type="radio" name="rdTReg" id="rdTRegM" value="M">
			</td>
		</tr>
		<tr>
			<td width="200" class="tbltext1BFad">
				Tutor / Mentor* :
			</td>
			<td width="300">
				<%pippo=ComboMenTut()%>
			</td>
		</tr>
			<%if pippo ="OK" then%>
		<tr>
			<td width="200" class="tbltext1BFad">
				Regole assistenza* :
			</td>
			<td width="300">
				<%ComboRegole()%>
			</td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2" align="center">
				<a title="Indietro" href="GRA_VisGruAss.asp" onmouseover="javascript: window.status=' '; 
				return true"><img src="<%=Session("Progetto")%>/images/indietro.gif" border="0"></a>
				<input title="Avanti" type="image" src="<%=Session("Progetto")%>/images/Avanti.gif">
			</td>
		</tr>
		<%else%>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>	
		<tr>	
			<td colspan="2" align="center">
				<a title="Indietro" href="GRA_VisGruAss.asp" onmouseover="javascript: window.status=' '; 
				return true"><img src="<%=Session("Progetto")%>/images/indietro.gif" border="0"></a>
			</td>
		</tr>
	<%end if%>
	</table>

	</form>
<%	case "1"	' Devo scegliere gli eventuali tutori. 
		InfoProcedura("La funzione permette l'inserimento di nuovi gruppi di assistenza.")
		dim nIdRegola,sDescRegola
		
		sTutore = Request.Form("cmbMenTut")
		'Response.Write sTutore
		sDatiTutMet			= Split(sTutore,"||")(0)
		
		sNomeCognomeTutMet	= Split(sTutore,"||")(1)
		nIdUtenteTutMet		= clng(Split(sDatiTutMet,"!!!!")(0))
		nIdUorg			= clng(Split(sDatiTutMet,"!!!!")(1))
		'response.write nIdUorg
		nIdRegola	= clng(split(Request.Form("cmbRegole"),"||")(0))
		'response.write nIdRegola
		if nIdRegola <> 0 then
			sDescRegola = split(Request.Form("cmbRegole"),"||")(1)
			sSQL = "SELECT REGOLA,DECODE(TIPO_REGOLA,'E','ME','U','TU') AS TIPO " &_
				",TAB_RIF FROM REGOLE WHERE ID_REGOLE = " & nIdRegola
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
			set rsRegola = CC.Execute(sSQL)
		'Response.Write sSQL
			if not rsRegola.eof then
				sTabRif		= "" & rsRegola("TAB_RIF")
				if trim(sTabRif)="" then
					sSelect		= "AND " & rsRegola("REGOLA")
				else
					sSelect		= "AND EXISTS (SELECT ID_PERSONA FROM " &_
						 sTabRif & " WHERE " & rsRegola("REGOLA") & " AND CC.ID_PERSONA = ID_PERSONA) "
				end if		 
				sFiltro		= " AND " & rsRegola("REGOLA")
				sCodRorga	= rsRegola("TIPO")
			else
				sFiltro = "" 
			end if
			rsRegola.Close
			set rsRegola = nothing 
		else ' Se non esistono regole creo un filtro senza di esso
			sDescRegola = "Nessuna regola selezionata"
			sFiltro		= "" 
			sCodRorga	= Split(Request.Form("cmbRegole"),"||")(2)
			sSelect		= ""
		end if
'		Response.write "<BR> sSelect = " & sSelect
		' *****************************************************
		' La query deve essere cos� costituita :
		' Tutte le persone che sono in componenti classe la cui  
		' classe appatiene all ID_UORG della Mentor / Tutor
		' *****************************************************
		sSQL = " SELECT U.IDUTENTE FROM COMPONENTI_CLASSE CC, CLASSE C ,UTENTE U"
		sSQL = sSQL &  "  WHERE CC.ID_PERSONA = U.CREATOR "
		sSQL = sSQL &  "  AND C.ID_CLASSE = CC.ID_CLASSE  "
		sSQL = sSQL &  "  AND C.ID_UORG = " & nIdUorg
		sSQL = sSQL &  "  AND DT_RUOLOAL IS NULL " 
		sSQL = sSQL &  "  AND NOT EXISTS ( SELECT AU.IDUTENTE FROM ASSISTENZAUTENTE AU, ASSISTENZAGRUPPO AG "
		sSQL = sSQL &  "  	 WHERE AU.ID_GRUTASS = AG.ID_GRUTASS AND COD_RUOLO='" & sCodRorga & "' AND AU.IDUTENTE = U.IDUTENTE ) "
		sSQL = sSQL &  sSelect
		sSQL = sSQL &  "  ORDER BY ID_PERSONA"
		' *****************************************************
        'Response.Write sSQL
		set rsConta = Server.CreateObject("ADODB.Recordset")

'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
		rsConta.Open sSQL, CC, 3
		if not rsConta.EOF then
			%><form name="frmInsGruAss" method="post" action="GRA_CnfGruAss.asp" onsubmit="return Controlli()"><%
			aUtenti		= rsConta.GetRows
			nIdConto	= rsConta.RecordCount
			i=0 
			for i=0 to nIdConto-1
				Response.Write "<input type=""hidden"" name=""aUtenti"" value=""" &_
				 aUtenti(0,i) & """>"
			next%>			
			<br>
			<input type="hidden" name="txtNumUte" value="<%=nIdConto%>">			
			<input type="hidden" name="txtAction" value="INS">			
			<input type="hidden" name="txtOggi" value="<%=ConvDateToString(Date)%>">			
			<table border="0" CELLPADDING="2" CELLSPACING="2" width="500">
				<tr>
					<td align="left" width="200" class="tbltext1BFad">
						Regola selezionata  :
					</td>
					<td align="left" width="300" class="tbltextBFad">
						<%=sDescRegola%>
					</td>
				</tr>
				<tr>
					<td align="left" width="200" class="tbltext1BFad">
						Numero discenti identificati :
					</td>
					<td align="left" width="300" class="tbltextBFad">
						<%=nIdConto%>
					</td>
				</tr>
				<tr>
					<td align="left" width="200" class="tbltext1BFad">
					<%select case sCodRorga%>
						<%case "ME"%>
							Mentor selezionato* :
						<%case "TU"%>
							Tutor selezionato* :
						<%case else%>
							Mentor selezionato* :
					<%end select%>
					
					</td>
					<td align="left" width="300" class="tbltextBFad">
						<%=sNomeCognomeTutMet%>
						<input type="hidden" name="cmbMenTut" value="<%=nIdUtenteTutMet%>">
						<input type="hidden" value="<%=sCodRorga%>" name="Ruolo">
					</td>
				</tr>
				<tr>
					<td align="left" width="200" class="tbltext1BFad">
						Data inizio assistenza* :
					</td>
					<td align="left" width="300">
						<input maxlength="10" value="<%=ConvDateToString(Date)%>" type="text" size="12" class="textBlackFad" name="txtDtIniAss">			
					</td>
				</tr>
				<tr>
					<td align="left" width="200" class="tbltext1BFad">
						Data fine assistenza :
					</td>
					<td align="left" width="300">
						<input size="12" maxlength="10" type="text" class="textBlackFad" name="txtDtFinAss">			
					</td>
				</tr>
				<tr>
					<td colspan="2">&nbsp;</td>
				</tr>
			</table>
			<table border="0" CELLPADDING="2" CELLSPACING="2" width="500">
				<tr>
					<td width="50%" align="right">
						<input title="Conferma" type="image" src="<%=Session("Progetto")%>/images/Conferma.gif" name="image1">
					</td>
					<td width="50%" align="left">
					<a title="Indietro" href="GRA_InsGruAss.asp" onmouseover="javascript: window.status=' '; 
						return true"><img src="<%=Session("Progetto")%>/images/indietro.gif" border="0"></a>
					</td>
				</tr>
			</table>
			</form>				

<%		else%>
			<br><br>
			<table border="0" CELLPADDING="2" CELLSPACING="2" width="500">
				<tr>
					<td class="tbltext3" align="right">
						Nessun discente ritrovato con il raggruppamento selezionato.
					</td>
				</tr>
			</table>
			<br>
			<table border="0" CELLPADDING="2" CELLSPACING="2" width="500">
				<tr>
					<td width="50%" align="center">
						<a title="Title" href="GRA_InsGruAss.asp" onmouseover="javascript: window.status=' '; 
						return true"><img src="<%=Session("Progetto")%>/images/indietro.gif" border="0"></a>
					</td>
				</tr>
			</table>
			
		<%end if
		rsConta.Close
		set rsConta  = nothing%> 
	<%case "2"	
		
		sTipo = Request("Assistente")
		nIdUorg = clng(Request("UnitaOrga"))
		
		InfoProcedura("La funzione permette di associare le persone rimaste ad un gruppo precedentemente creato.")
		' Devo prendere il numero delle persone e gli id che rimando
		%>
		<form name="frmInsGruAss" method="post" onsubmit="return Controlla()" action="GRA_CnfGruAss.asp">
		<input type="hidden" name="txtAction" value="ASS">			
	
		<%	
		if sTipo = "ME" then

			sSQL = " SELECT U.IDUTENTE FROM COMPONENTI_CLASSE CC, CLASSE C ,UTENTE U"
			sSQL = sSQL &  "  WHERE CC.ID_PERSONA = U.CREATOR "
			sSQL = sSQL &  "  AND C.ID_CLASSE = CC.ID_CLASSE  "
			sSQL = sSQL &  "  AND C.ID_UORG = " & nIdUorg
			sSQL = sSQL &  "  AND DT_RUOLOAL IS NULL " 
			sSQL = sSQL &  "  AND NOT EXISTS ( SELECT AU.IDUTENTE FROM ASSISTENZAUTENTE AU, ASSISTENZAGRUPPO AG "
			sSQL = sSQL &  "  	 WHERE AU.ID_GRUTASS = AG.ID_GRUTASS AND COD_RUOLO='ME' AND AU.IDUTENTE = U.IDUTENTE ) "
			sSQL = sSQL &  "  ORDER BY ID_PERSONA"
		
		
'			sSQL = "SELECT B.IDUTENTE FROM PERSONA A, UTENTE B, GRUPPO C " &_
'				" WHERE A.ID_PERSONA = B.CREATOR AND C.COD_RUOFU = 'DI' AND  C.IDGRUPPO = B.IDGRUPPO "
'			sSQL = sSQL	& " AND NOT EXISTS ( SELECT AU.IDUTENTE FROM ASSISTENZAUTENTE AU, ASSISTENZAGRUPPO AG " &_
'				" WHERE AU.ID_GRUTASS = AG.ID_GRUTASS AND COD_RUOLO='ME' AND AU.IDUTENTE = B.IDUTENTE ) ORDER BY ID_PERSONA"

			set rsConta = Server.CreateObject("ADODB.Recordset")
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
			rsConta.Open sSQL, CC, 3
			'Response.Write sSQL
			if not rsConta.EOF then
				aUtenti		= rsConta.GetRows
				nIdConto	= rsConta.RecordCount
				i=0 
				for i=0 to nIdConto-1
					Response.Write "<input type=""hidden"" name=""aMEUtenti"" value=""" &_
					 aUtenti(0,i) & """>"
				next
			end if
		
			rsConta.close
			set rsConta = nothing
		else
		
			sSQL = " SELECT U.IDUTENTE FROM COMPONENTI_CLASSE CC, CLASSE C ,UTENTE U"
			sSQL = sSQL &  "  WHERE CC.ID_PERSONA = U.CREATOR "
			sSQL = sSQL &  "  AND C.ID_CLASSE = CC.ID_CLASSE  "
			sSQL = sSQL &  "  AND C.ID_UORG = " & nIdUorg
			sSQL = sSQL &  "  AND DT_RUOLOAL IS NULL " 
			sSQL = sSQL &  "  AND NOT EXISTS ( SELECT AU.IDUTENTE FROM ASSISTENZAUTENTE AU, ASSISTENZAGRUPPO AG "
			sSQL = sSQL &  "  	 WHERE AU.ID_GRUTASS = AG.ID_GRUTASS AND COD_RUOLO='TU' AND AU.IDUTENTE = U.IDUTENTE ) "
			sSQL = sSQL &  "  ORDER BY ID_PERSONA"
		
'			sSQL = "SELECT B.IDUTENTE FROM PERSONA A, UTENTE B, GRUPPO C " &_
'				" WHERE A.ID_PERSONA = B.CREATOR AND C.COD_RUOFU = 'DI'  AND  C.IDGRUPPO = B.IDGRUPPO  "
'			sSQL = sSQL	& " AND NOT EXISTS ( SELECT AU.IDUTENTE FROM ASSISTENZAUTENTE AU, ASSISTENZAGRUPPO AG " &_
'				" WHERE AU.ID_GRUTASS = AG.ID_GRUTASS AND COD_RUOLO='TU' AND AU.IDUTENTE = B.IDUTENTE ) ORDER BY ID_PERSONA"
			
			
			set rsConta = Server.CreateObject("ADODB.Recordset")
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
			rsConta.Open sSQL, CC, 3
			if not rsConta.EOF then
				aUtenti		= rsConta.GetRows
				nIdConto	= rsConta.RecordCount
				i=0 
				for i=0 to nIdConto-1
					Response.Write "<input type=""hidden"" name=""aTUUtenti"" value=""" &_
					 aUtenti(0,i) & """>"
				next
			end if
		
			rsConta.close 
			set rsConta = nothing
		end if
		%>	
		<br>		
		<table border="0" CELLPADDING="2" CELLSPACING="2" width="500">
			<tr>
				<td width="170" class="tblText1BFad">
				Elenco dei gruppi disponibili*
				</td>
				<td width="330" class="tblTextBFad"> 
				<%ComboAssGruppi sTipo,nIdUorg%>
				</td>
			</tr>
			<tr>
				<td colspan="2">&nbsp;</td>
			</tr>
<%'------------------------------------------------------%>			
			
		</table>
		</form>
<%   case "3"
'--------------------------------------------------------------
	InfoProcedura("La funzione permette l'inserimento di nuovi gruppi di assistenza.")
		
		sTutore = Request.Form("cmbMenTut")
		'Response.Write sTutore
		sDatiTutMet			= Split(sTutore,"||")(0)
		
		sNomeCognomeTutMet	= Split(sTutore,"||")(1)
		nIdUtenteTutMet		= clng(Split(sDatiTutMet,"!!!!")(0))
		nIdUorg			= clng(Split(sDatiTutMet,"!!!!")(1))
		'response.write nIdUorg
		nIdRegola	= clng(split(Request.Form("cmbRegole"),"||")(0))
		'response.write nIdRegola
		
			sDescRegola = "Manuale"
			sFiltro		= "" 
			sCodRorga	= Split(Request.Form("cmbRegole"),"||")(2)
			sSelect		= ""
%>		
			<form name="frmInsGruAss1" method="post" action="GRA_VisDiscenti.asp" onsubmit="return Controlli1()">
			<br>
			<input type="hidden" name="txtUorga" value="<%=nIdUorg%>">
			<input type="hidden" name="txtRorga" value="<%=sCodRorga%>">			
			<input type="hidden" name="txtOggi" value="<%=ConvDateToString(Date)%>">			
			<table border="0" CELLPADDING="2" CELLSPACING="2" width="500">
				<tr>
					<td align="left" width="200" class="tbltext1BFad">
						Regola selezionata  :
					</td>
					<td align="left" width="300" class="tbltextBFad">
						<%=sDescRegola%>
					</td>
				</tr>
				
				<tr>
					<td align="left" width="200" class="tbltext1BFad">
					<%select case sCodRorga%>
						<%case "ME"%>
							Mentor selezionato* :
						<%case "TU"%>
							Tutor selezionato* :
						<%case else%>
							Mentor selezionato* :
					<%end select%>
					
					</td>
					<td align="left" width="300" class="tbltextBFad">
						<%=sNomeCognomeTutMet%>
						<input type="hidden" name="cmbMenTut" value="<%=nIdUtenteTutMet%>">
						<input type="hidden" value="<%=sCodRorga%>" name="Ruolo">
					</td>
				</tr>
				<tr>
					<td align="left" width="200" class="tbltext1BFad">
						Data inizio assistenza* :
					</td>
					<td align="left" width="300">
						<input maxlength="10" value="<%=ConvDateToString(Date)%>" type="text" size="12" class="textBlackFad" name="txtDtIniAss">			
					</td>
				</tr>
				<tr>
					<td align="left" width="200" class="tbltext1BFad">
						Data fine assistenza :
					</td>
					<td align="left" width="300">
						<input size="12" maxlength="10" type="text" class="textBlackFad" name="txtDtFinAss">			
					</td>
				</tr>
				
			</table>
			
	<table border="0" CELLPADDING="1" CELLSPACING="2" width="500">
		<tr height="17">
			<td colspan="3">
				&nbsp;
			</td>
		</tr>
		<tr height="2">
			<td width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
		</tr>
		<tr height="2">
			<td width="100%" colspan="3" background></td>
		</tr>
		<tr height="17">
			<td colspan="3">
				&nbsp;
			</td>
		</tr>
		<tr>
			<td width="200" class="tbltext1BFad">
				Unita Organizzativa
			</td>
			<td width="300" class="TextBlack">
				PROPRIA <input checked class="tbltext1BFad" type="radio" id="radUO" name="radUO" value="P">
				TUTTE <input class="tbltext1BFad" type="radio" name="radUO" id="radUO" value="T">
			</td>
		</tr>
		<tr height="17">
			<td colspan="3">
				&nbsp;
			</td>
		</tr>
		<tr height="2">
			<td width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
		</tr>
		<tr height="2">
			<td width="100%" colspan="3" background></td>
		</tr>
		<tr height="17">
			<td colspan="3">
				&nbsp;
			</td>
		</tr>
		<tr height="17">
			<td width="200" class="tbltext1BFad">
				<b>Cognome</b>
			</td>
			<td width="400" class="tbltext1BFad">
				<b><input class="textBlackFad" type="text" name="txtCognome" style="TEXT-TRANSFORM: uppercase" size="35"></b>
			</td>
		</tr>
		<tr height="17">
			<td width="200" class="tbltext1BFad">
				<b>Nome</b>
			</td>
			<td width="200" class="tbltext1">
				<b><input class="textBlackFad" type="text" name="txtNome" style="TEXT-TRANSFORM: uppercase" size="35"></b>
			</td>
		</tr> 
		<tr height="17">
			<td width="200" class="tbltext1BFad">
				<b>Data di Nascita</b> 
				<br>(gg/mm/aaaa)
			</td>
			<td width="400" class="tbltext1">
				<b><input class="textBlackFad" type="text" name="txtDataN" maxlength="10" size="10">
			</td>
		</tr>
		<tr height="17">
			<td colspan="3">
				&nbsp;
			</td>
		</tr>
		<tr height="2">
			<td width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
		</tr>
		<tr height="2">
			<td width="100%" colspan="3" background></td>
		</tr>
		<tr height="17">
			<td colspan="3">
				&nbsp;
			</td>
		</tr>
		<tr height="17">
			<td width="100" class="tbltext1BFad">
				<b>Codice Fiscale</b>
			</td>
			<td width="400" class="tbltext1BFad">
				<b><input class="textBlackFad" type="text" name="txtCodFisc" style="TEXT-TRANSFORM: uppercase" maxlength="16" size="20">
			</td>
		</tr>		
</table>
<br>
			<table border="0" CELLPADDING="2" CELLSPACING="2" width="500">
				<tr>
					<td width="50%" align="right">
						<input title="Conferma" type="image" src="<%=Session("Progetto")%>/images/Conferma.gif" name="image1">
					</td>
					<td width="50%" align="left">
					<a title="Indietro" href="GRA_InsGruAss.asp" onmouseover="javascript: window.status=' '; 
						return true"><img src="<%=Session("Progetto")%>/images/indietro.gif" border="0"></a>
					</td>
				</tr>
			</table>
			</form>				
<%			
'--------------------------------------------------------------
%>			
<%end select%> 

<!--#include virtual="/Include/CloseConn.asp"-->
<!--#include Virtual="/strutt_coda2.asp"-->

<%

function ComboMenTut()
	dim sSQL, rsTutMen
	
	sSQL = "SELECT IDUTENTE,NOME,COGNOME,COD_RUOFU, NVL(U.ID_UORG,0) AS ID_UORG FROM UTENTE U, GRUPPO GR " &_
		" WHERE U.IDGRUPPO = GR.IDGRUPPO AND COD_RUOFU IN ('TU','ME') ORDER BY COD_RUOFU " 
		

'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	set rsTutMen = CC.Execute(sSQL)  
	'Response.Write sSQL
	if not rsTutMen.eof then
	
		set rsATutMen = Server.CreateObject("ADODB.Recordset")
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
		rsATutMen.Open sSQL, CC, 3
		aPippo = rsATutMen.GetRows()
		rsATutMen.Close
		set rsATutMen = nothing%>
		<script language="JavaScript">
			aTeacher = new Array('<%=ubound(aPippo,2)%>')
			<%for i = 0 to ubound(aPippo,2)	%>
				z = '<%=i%>';
				aTeacher[parseInt(z,10)] = '<%=CLng(aPippo(0,i))%>' + '!!!!' + '<%=CLng(aPippo(4,i))%>';
				aTeacher[parseInt(z,10)] = aTeacher[parseInt(z,10)] + '||';
				aTeacher[parseInt(z,10)] = aTeacher[parseInt(z,10)] + '<%=aPippo(1,i)%>' + ' ' + '<%=aPippo(2,i)%>';
				aTeacher[parseInt(z,10)] = aTeacher[parseInt(z,10)] + '|||';
				aTeacher[parseInt(z,10)] = aTeacher[parseInt(z,10)] + '<%=aPippo(3,i)%>';
			<%next%>
		</script>
		<select name="cmbMenTut" class="textBlackFad">
			<option></option>
	<%	do while not rsTutMen.eof%>
			<option value="<%=CLng(rsTutMen("IDUTENTE"))%>!!!!<%=CLng(rsTutMen("ID_UORG"))%>"><%=rsTutMen("NOME")%> - <%=rsTutMen("COGNOME")%></option>
			<%rsTutMen.MoveNext
		loop%>
		</select>
		<script language="JavaScript">
			VediRegola(aTeacher,'cmbMenTut',0)
		</script>	
	
		<%
	ComboMenTut="OK"
	else
		ComboMenTut="KO"
		Response.Write "<span class='TextBlack'>Tipologie di assistenti non presenti</span>"
	end if
	rsTutMen.close
	set rsTutMen = nothing
end function


sub ComboMenTutOLD(sRuofu)
	dim sSQL, rsTutMen
	
	sSQL = "SELECT IDUTENTE,NOME,COGNOME,COD_RUOFU FROM UTENTE U, GRUPPO GR " &_
		" WHERE U.IDGRUPPO = GR.IDGRUPPO AND COD_RUOFU = '" & sRuofu & "'" 

'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	set rsTutMen = CC.Execute(sSQL)
	'Response.Write sSQL
	if not rsTutMen.eof then%>
	

		<input type="hidden" value="<%=rsTutMen("COD_RUOFU")%>" name="Ruolo">
		<select name="cmbMenTut" class="textBlackFad">
			<option></option>
	<%	do while not rsTutMen.eof%>
			<option value="<%=CLng(rsTutMen("IDUTENTE"))%>"><%=rsTutMen("NOME")%> - <%=rsTutMen("COGNOME")%></option>
			<%rsTutMen.MoveNext
		loop
	else

	end if
	rsTutMen.close
	set rsTutMen = nothing
end sub


Sub  ComboRegole
	dim sSQL,rsRegole
	
	sSQL = "SELECT ID_REGOLE,DESC_REGOLA," &_
		" DECODE(TIPO_REGOLA,'E','MENTOR','U','TUTOR') AS TIPO " &_
		" FROM REGOLE WHERE TIPO_REGOLA IN ('E','U') ORDER BY TIPO_REGOLA"
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	set rsRegole = CC.Execute(sSQL)
	'Response.Write sSQL
	if not rsRegole.EOF then	
		set rsARegole = Server.CreateObject("ADODB.Recordset")
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
		rsARegole.Open sSQL, CC, 3
		aPippo = rsARegole.GetRows()
		rsARegole.Close
		set rsARegole = nothing%>
		<script language="JavaScript">
			aRegola = new Array('<%=ubound(aPippo,2)%>')
/*
			aRegola[0] = '0';
			aRegola[0] = aRegola[0] + '||';
			aRegola[0] = aRegola[0] + 'Nessun filtro';
			aRegola[0] = aRegola[0] + '|||';
			aRegola[0] = aRegola[0] + 'MENTOR';

			aRegola[1] = '0';
			aRegola[1] = aRegola[1] + '||';
			aRegola[1] = aRegola[1] + 'Nessun filtro';
			aRegola[1] = aRegola[1] + '|||';
			aRegola[1] = aRegola[1] + 'TUTOR';
*/
			<%for i = 0 to ubound(aPippo,2)	%>
				z = '<%=i%>';
				aRegola[parseInt(z,10)] = '<%=aPippo(0,i)%>';
				aRegola[parseInt(z,10)] = aRegola[parseInt(z,10)] + '||';
				aRegola[parseInt(z,10)] = aRegola[parseInt(z,10)] + '<%=aPippo(1,i)%>';
				aRegola[parseInt(z,10)] = aRegola[parseInt(z,10)] + '|||';
				aRegola[parseInt(z,10)] = aRegola[parseInt(z,10)] + '<%=aPippo(2,i)%>';
			<%next%>
		</script>
		<select class="textBlackFad" name="cmbRegole">
			<option></option>
			<option value="0">Nessun filtro</option>
		<%do while not rsRegole.EOF%>
			<option value="<%=rsRegole("ID_REGOLE")%>||<%=rsRegole("DESC_REGOLA")%>"><%=rsRegole("DESC_REGOLA")%> ( <%=rsRegole("TIPO")%> )</option>	
			<%rsRegole.MoveNext()
		loop%>
	</select>

	<script language="JavaScript">
		VediRegola(aRegola,'cmbRegole',1)
	</script>	

	<%else
	
	end if
	rsRegole.Close
	set rsRegole = nothing 

end sub
%>
<%'Sub InfoProcedura(sCommento)%>
<%Sub InfoProcedura(sCommento)%>
<!-- Lingetta superiore 	Scrittura dell'intestazione della pagina-->
	<br>
	<table border="0" CELLPADDING="0" CELLSPACING="0" width="500" align="center">
		<tr height="18">
			<td class="sfondomenu" width="67%" height="18"><span class="tbltext0"><b>&nbsp;ASSISTENZA UTENTI</b></span></td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
			<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
		</tr>
	</table>
	<!-- Commento -->
	<table border="0" CELLPADDING="0" CELLSPACING="0" width="500" align="center">
		<tr>
			<td align="left" class="sfondocommaz">
				<%=sCommento%>
				<a title="Help" onmouseover="javascript: window.status=' '; 
					return true" href="Javascript:Show_Help('/pgm/help/Formazione/GruppiAssistenza/GRA_InsGruAss')"><img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
			</td>
		</tr>
		<tr height="2">
			<td class="sfondocommaz" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>
	
<%End Sub
'---------------
%>
<%
sub ComboAssGruppi(sTipo,nIdUorg)
	dim dNow

'	dNow = ConvDateToDBs(Date)


'	sSQL = "SELECT ID_ASSGR FROM ASSISTENZAGRUPPO WHERE  " &_
'		" (SYSDATE BETWEEN DT_INIASS AND DT_FINEASS " & _
'		" OR (SYSDATE > DT_INIASS AND DT_FINEASS IS NULL)) "
'	sSQL = "SELECT ID_GRUTASS,ID_ASSGR, NOME,COGNOME " &_
'		"FROM ASSISTENZAGRUPPO AG, UTENTE U WHERE AG.IDUTENTE = U.IDUTENTE " &_
'		"AND (SYSDATE BETWEEN DT_INIASS AND DT_FINEASS " & _
'		" OR (SYSDATE > DT_INIASS AND DT_FINEASS IS NULL)) "	

	sSQL = "SELECT ID_UORG,ID_GRUTASS,ID_ASSGR, NOME,COGNOME " &_
		"FROM ASSISTENZAGRUPPO AG, UTENTE U WHERE AG.IDUTENTE = U.IDUTENTE " &_
		" AND STATO_ASS='A' AND COD_RUOLO ='" & sTipo & "' AND U.ID_UORG=" & nIdUorg 
' Response.Write sSQL
'		"AND (" & dNow & " BETWEEN DT_INIASS AND DT_FINEASS " & _
'		" OR (" & dNow & " >= DT_INIASS AND DT_FINEASS IS NULL)) AND STATO_ASS='A'"
	
	set rsGruppi = Server.CreateObject("ADODB.Recordset")
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	rsGruppi.Open sSQL, CC, 3

	if not rsGruppi.EOF then
		if rsGruppi.RecordCount > 1 then%>
			<select name="cmbGruppiAss" class="textBlackFad">	
				<option value></option>
			<%do while not rsGruppi.EOF%>
				<option value="<%=clng(rsGruppi("ID_GRUTASS"))%>"><%=clng(rsGruppi("ID_ASSGR"))%> - <%=rsGruppi("NOME")%>&nbsp;<%=rsGruppi("COGNOME")%></option>
			<%rsGruppi.MoveNext() 
			loop%>
			</select>
		<!--tr>				<td colspan="2" align="center">			<table width="400" border="0">			<tr>				<td>&nbsp;</td>			</tr>				<tr>				<td colspan="2" align="center">					<a title="Indietro" href="GRA_VisGruAss.asp" onmouseover="javascript: window.status=' '; 					return true"><img src="<%=Session("Progetto")%>/images/indietro.gif" border="0"></a>					<input title="Avanti" type="image" src="<%=Session("Progetto")%>/images/Avanti.gif" name="Avanti">				</td>			</tr>			</table>			</td>			</tr-->
			
		<%else%>	
			<%=clng(rsGruppi("ID_ASSGR"))%> - <%=rsGruppi("NOME")%>&nbsp;<%=rsGruppi("COGNOME")%>			
			<input type="hidden" name="cmbGruppiAss" value="<%=clng(rsGruppi("ID_GRUTASS"))%>">
		<%end if%>
		<tr>
				<td colspan="2" align="center">
			<table width="400" border="0">
			<tr>
				<td>&nbsp;</td>
			</tr>
				<tr>
				<td colspan="2" align="center">
					<a title="Indietro" href="GRA_VisGruAss.asp" onmouseover="javascript: window.status=' '; 
					return true"><img src="<%=Session("Progetto")%>/images/indietro.gif" border="0"></a>
					<input title="Avanti" type="image" src="<%=Session("Progetto")%>/images/Avanti.gif" name="Avanti">
				</td>
			</tr>
			</table>
			</td>
			</tr>
<%	else
	
		Response.Write "<b>Non ci sono gruppi esistenti<b>"
		
%>		<tr>
				<td colspan="2" align="center">
		<table border="0" CELLPADDING="2" CELLSPACING="2" width="400">				
				<tr><td>&nbsp;</td></tr>
				<tr>					
					<td width="50%" align="center">
					<a title="Indietro" href="GRA_VisGruAss.asp" onmouseover="javascript: window.status=' '; 
						return true"><img src="<%=Session("Progetto")%>/images/indietro.gif" border="0"></a>
					</td>
				</tr>
			</table>
			</td>
		</tr>	
<%			
	end if
	rsGruppi.Close
	set rsGruppi = nothing  


end sub%>
