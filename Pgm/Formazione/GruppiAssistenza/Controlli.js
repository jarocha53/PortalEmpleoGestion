

function Controlla() {
	if (frmInsGruAss.cmbGruppiAss.value == ""){
		alert("Indicare un gruppo di assistenza.");
		frmInsGruAss.cmbGruppiAss.focus();
		return false;
	
	}


}

function ControlliMod() 
{
	if (frmInsGruAss.cmbMenTut.value == "") 
	{
		alert("Indicare il Mentor o il Tutor.");
		frmInsGruAss.cmbMenTut.focus();	
		return false;
	}else {
		if ((frmInsGruAss.cmbMenTut.value == frmInsGruAss.txtOldAss.value) && (frmInsGruAss.txtOldDTfine.value == frmInsGruAss.txtDtFinAss.value))
		{	
			if (frmInsGruAss.txtDtIniAss.type != "hidden") {
				if (frmInsGruAss.txtPrec.value == frmInsGruAss.txtDtIniAss.value ) {
					alert("Nessun dato � stato modificato.");
					return false;
				}
				
			}else {		
				alert("Nessun dato � stato modificato.");
				return false;
			}
		}
	}

	if (frmInsGruAss.txtDtFinAss.value != "")
	{
		if (!ValidateInputDate(frmInsGruAss.txtDtFinAss.value)) {
			frmInsGruAss.txtDtFinAss.focus();	
			return false;
		}else{
			if (!ValidateRangeDate(frmInsGruAss.txtDtIniAss.value,frmInsGruAss.txtDtFinAss.value))
			{
				frmInsGruAss.txtDtFinAss.focus();
				alert("La data fine assistenza deve essere superiore\nalla data inizio assistenza.");
				return false;
			}
		}
	}

	
	return true; 
}


function Controlli() 
{
	if (frmInsGruAss.cmbMenTut.value == "") 
	{
		alert("Indicare il Mentor o il Tutor.");
		frmInsGruAss.cmbMenTut.focus();	
		return false;
	}
	if (frmInsGruAss.txtDtIniAss.value == "") 
	{
		alert("Indicare la data di inizio Gruppo Assistenza.");
		frmInsGruAss.txtDtIniAss.focus();	
		return false;
	}else
	{
		if (!ValidateInputDate(frmInsGruAss.txtDtIniAss.value)) {
			frmInsGruAss.txtDtIniAss.focus();	
			return false;
		}
		dOggi = frmInsGruAss.txtOggi.value
		if (!ValidateRangeDate(frmInsGruAss.txtOggi.value,frmInsGruAss.txtDtIniAss.value))
		{
			alert("La data inizio gruppo assistenza deve\nessere superiore/uguale alla data odierna.")
			return false;
		}
		
	}

	
	if (frmInsGruAss.txtDtFinAss.value != "")
	{
		if (!ValidateInputDate(frmInsGruAss.txtDtFinAss.value)) {
			frmInsGruAss.txtDtFinAss.focus();	
			return false;
		}else{
			if (!ValidateRangeDate(frmInsGruAss.txtDtIniAss.value,frmInsGruAss.txtDtFinAss.value))
			{
				frmInsGruAss.txtDtFinAss.focus();
				alert("La data fine assistenza deve essere superiore\nalla data inizio assistenza.");
				return false;
			}
		}
	}
	
	return true; 
}

function CheckRegola()
{
	if (frmInsGruAss.cmbMenTut.value == "") 
	{
		alert("Selezionare un Mentor / Tutor");
		frmInsGruAss.cmbMenTut.focus();	
		return false;
	}

	if (frmInsGruAss.cmbRegole.value == "") 
	{
		alert("Selezionare una regola per la generazione\ndi un gruppo di assistenza.");
		frmInsGruAss.cmbRegole.focus();	
		return false;
	}

}

function Controlli1() 
{ 
	if (frmInsGruAss1.cmbMenTut.value == "") 
	{
		alert("Indicare il Mentor o il Tutor.");
		frmInsGruAss1.cmbMenTut.focus();	
		return false;
	}
	if (frmInsGruAss1.txtDtIniAss.value == "") 
	{
		alert("Indicare la data di inizio Gruppo Assistenza.");
		frmInsGruAss1.txtDtIniAss.focus();	
		return false;
	}else
	{
		if (!ValidateInputDate(frmInsGruAss1.txtDtIniAss.value)) {
			frmInsGruAss1.txtDtIniAss.focus();	
			return false;
		}
		dOggi = frmInsGruAss1.txtOggi.value
		if (!ValidateRangeDate(frmInsGruAss1.txtOggi.value,frmInsGruAss1.txtDtIniAss.value))
		{
			alert("La data inizio gruppo assistenza deve\nessere superiore/uguale alla data odierna.")
			return false;
		}
		
	}
	
	
	if (frmInsGruAss1.txtDtFinAss.value != "")
	{
		if (!ValidateInputDate(frmInsGruAss1.txtDtFinAss.value)) {
			frmInsGruAss1.txtDtFinAss.focus();	
			return false;
		}else{
			if (!ValidateRangeDate(frmInsGruAss1.txtDtIniAss.value,frmInsGruAss1.txtDtFinAss.value))
			{
				frmInsGruAss1.txtDtFinAss.focus();
				alert("La data fine assistenza deve essere superiore\nalla data inizio assistenza.");
				return false;
			}
		}
	}


document.frmInsGruAss1.txtNome.value=TRIM(document.frmInsGruAss1.txtNome.value)
document.frmInsGruAss1.txtCognome.value=TRIM(document.frmInsGruAss1.txtCognome.value)
document.frmInsGruAss1.txtDataN.value=TRIM(document.frmInsGruAss1.txtDataN.value)
document.frmInsGruAss1.txtCodFisc.value=TRIM(document.frmInsGruAss1.txtCodFisc.value)


if ((document.frmInsGruAss1.txtCognome.value!="")||
	(document.frmInsGruAss1.txtNome.value!="")||
	(document.frmInsGruAss1.txtDataN.value!="")||
	(document.frmInsGruAss1.txtCodFisc.value!="")){
	
	
	if (document.frmInsGruAss1.txtCognome.value!=""){
		var testoC
		testoC=isNaN(document.frmInsGruAss1.txtCognome.value)
		if (testoC!=true){
		alert('Il campo Cognome non pu� contenere numeri!')
		return false
		}
	}
	
	
	if (document.frmInsGruAss1.txtNome.value!=""){
		var testoN
		testoN=isNaN(document.frmInsGruAss1.txtNome.value)
		if (testoN!=true){
		alert('Il campo Nome non pu� contenere numeri!')
		return false
		}
	}
	
		var sDataNascita1
		sDataNascita1=document.frmInsGruAss1.txtDataN.value
		if (sDataNascita1 !="") 
		{ 
		if (!ValidateInputDate(sDataNascita1))
			{
				document.frmInsGruAss1.txtDataN.focus() 
				return false
			}
		}
	
if ((document.frmInsGruAss1.txtCognome.value!="")||
	(document.frmInsGruAss1.txtNome.value!="")||
	(document.frmInsGruAss1.txtDataN.value!="")){
	
		if (document.frmInsGruAss1.txtCodFisc.value!=""){
		var apice
		apice="'"
			alert('La ricerca non pu� essere effettuata inserendo contemporaneamente Dati Anagrafici e Codice Fiscale. ')
			document.frmInsGruAss1.txtCognome.focus()
			return false	
			}


		if ((document.frmInsGruAss1.txtCognome.value!="")&&
			(document.frmInsGruAss1.txtNome.value=="")&&
			(document.frmInsGruAss1.txtDataN.value=="")){
			alert('Devi inserire anche Nome e Data.')
			document.frmInsGruAss1.txtNome.focus()
			return false
			}


		if ((document.frmInsGruAss1.txtNome.value!="")&&
			(document.frmInsGruAss1.txtCognome.value=="")&&
			(document.frmInsGruAss1.txtDataN.value=="")){
			alert('Devi inserire anche Cognome e Data.')
			document.frmInsGruAss1.txtCognome.focus()
			return false
			}

		if ((document.frmInsGruAss1.txtDataN.value!="")&&
			(document.frmInsGruAss1.txtNome.value=="")&&
			(document.frmInsGruAss1.txtCognome.value=="")){
			alert('Devi inserire anche Cognome e Nome.')
			document.frmInsGruAss1.txtCognome.focus()
			return false
			}


		if ((document.frmInsGruAss1.txtCognome.value!="")&&
			(document.frmInsGruAss1.txtNome.value!="")&&
			(document.frmInsGruAss1.txtDataN.value=="")){
			alert('Devi inserire anche la Data.')
			document.frmInsGruAss1.txtDataN.focus()
			return false
			}

		
		
		if ((document.frmInsGruAss1.txtCognome.value!="")&&
			(document.frmInsGruAss1.txtDataN.value!="")&&
			(document.frmInsGruAss1.txtNome.value=="")){
			alert('Devi inserire anche il Nome.')
			document.frmInsGruAss1.txtNome.focus()
			return false
			}
		
		
		if ((document.frmInsGruAss1.txtNome.value!="")&&
			(document.frmInsGruAss1.txtDataN.value!="")&&
			(document.frmInsGruAss1.txtCognome.value=="")){
			alert('Devi inserire anche il Cognome.')
			document.frmInsGruAss1.txtCognome.focus()
			return false
			}



	if ((document.frmInsGruAss1.txtNome.value=="")||
		(document.frmInsGruAss1.txtCognome.value=="")||
		(document.frmInsGruAss1.txtDataN.value=="")){

		alert('Per avviare la ricerca devi inserire i dati anagrafici.')
		return false
		}
		
		//*******ff
		sCognome=ValidateInputStringWithNumber(document.frmInsGruAss1.txtCognome.value)
    if  (sCognome==false)
		{
			alert("Cognome formalmente errato.")
			document.frmInsGruAss1.txtCognome.focus() 
			return false
		}
		
		
	 var anyStringC = document.frmInsGruAss1.txtCognome.value;
	    for (var i=0; i<=anyStringC.length-1; i++)
		{
			if(((anyStringC.charAt(i) >= "A") && (anyStringC.charAt(i) <= "Z")) || 
				((anyStringC.charAt(i) >= "a") && (anyStringC.charAt(i) <= "z"))||
				(anyStringC.charAt(i) == " ")||(anyStringC.charAt(i) == "'"))
			{
			}
			else
			{		
				document.frmInsGruAss1.txtCognome.focus();
			 	alert("Cognome formalmente errato.");
				return false
			}		
		}
		
		
		//NOME
	
		var anyStringN = document.frmInsGruAss1.txtNome.value;
	    for (var i=0; i<=anyStringN.length-1; i++)
		{
			if(((anyStringN.charAt(i) >= "A") && (anyStringN.charAt(i) <= "Z")) || 
				((anyStringN.charAt(i) >= "a") && (anyStringN.charAt(i) <= "z"))||
				(anyStringN.charAt(i) == " ")||(anyStringN.charAt(i) == "'"))
			{
			}
			else
			{		
				alert("Nome formalmente errato.")
				document.frmInsGruAss1.txtNome.focus()
				return false
			}		
		}
		
		
		var sDataNascita
		sDataNascita=document.frmInsGruAss1.txtDataN.value
		if (sDataNascita !="") 
		{ 
		if (!ValidateInputDate(sDataNascita))
			{
				document.frmInsGruAss1.txtDataN.focus() 
				return false
			}
		}
		
	
	
	//controllo che l'anno non sia superiore a quello odierno

	if(document.frmInsGruAss1.txtDataN.value !=""){
		var intAnnoOdierno
		var sDataOdierna
		var sDataCompleta
		var intAnno

		sDataCompleta=document.frmInsGruAss1.txtDataN.value

		intAnno=sDataCompleta.substr(6,4)


		sDataOdierna = new Date();
		intAnnoOdierno= sDataOdierna.getFullYear();

			if (intAnno >=intAnnoOdierno){
				alert ("La Data di Nascita deve essere precedente alla data odierna!")
			
			return false
			}
	}
	
}	
				//Contollo la validit� del Codice Fiscale
	//DataNascita = document.frmInsGruAss1.txtDataN.value
	//CodFisc = document.frmInsGruAss1.txtCodFisc.value.toUpperCase()
	//Codice Fiscale deve essere di 16 caratteri
	//if ((document.frmInsGruAss1.txtCodFisc.value.length != 16) &&
	//	(document.frmInsGruAss1.txtCodFisc.value != ""))
	//	{
	//		alert("Formato del Codice Fiscale errato.")
	//		document.frmInsGruAss1.txtCodFisc.focus() 
	//	return false
	//	}
		
	//if ((!ControllChkCodFisc(CodFisc)) &&
	//	(document.frmInsGruAss1.txtCodFisc.value != ""))
	//	{
	//		alert("Codice Fiscale Errato.")
	//		document.frmInsGruAss1.txtCodFisc.focus()
	//		return false
	//	}
	}	
return true
}
