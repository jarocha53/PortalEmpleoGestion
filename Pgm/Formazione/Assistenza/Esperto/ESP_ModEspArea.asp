<%'Option Explicit 
   Response.ExpiresAbsolute = Now() - 1 
   Response.AddHeader "pragma","no-cache"
   Response.AddHeader "cache-control","private"
   Response.CacheControl = "no-cache"
%>

<!-- #include virtual="/strutt_testa2.asp"-->
<!--#include virtual = "/include/openconn.asp"-->
<!--#include virtual="/util/portallib.asp"-->
<!--#include virtual="/util/DBUtil.asp"-->
<!--#include virtual = "/include/ControlDateVB.asp"-->


<%
'if ValidateService(session("idutente"),"ESP_VisEspArea", CC) <> "true" then 
'	response.redirect "/util/error_login.asp"
'end if
%>
<!-- ************** Javascript Inizio ************ -->

<script LANGUAGE="Javascript">
<!--#include virtual = "/Include/ControlDate.inc"-->
<!--#include virtual = "/Include/ControlNum.inc"-->
<!--#include Virtual = "/Include/ControlString.inc"-->
<!--#include virtual="/include/help.inc"-->



function Reload(){

	frmModEspArea.action = "ESP_ModEspArea.asp"
	frmModEspArea.submit()
}
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function VaiInizio(sDatiEsperto){
	location.href="ESP_VisEspArea.asp?cmbEsperto=" + sDatiEsperto + "&Modo=1";
}	

//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function ControllaDati(frmModEspArea,oldDataDal,oldDataAl,DataOdierna) {
//		frmModEspArea.txtDataDal			- Data - 
//		frmModEspArea.txtDataAl				- Data -
	
	var appoDataDal;
	var appoDataAl;

	appoDataDal = TRIM(frmModEspArea.txtDataDal.value)
	if (appoDataDal == ""){
		alert("Campo 'Validit� Dal' obbligatorio")
		frmModEspArea.txtDataDal.focus() 
		return false	
	}

	if (!ValidateInputDate(appoDataDal)){
		frmModEspArea.txtDataDal.focus()  
		return false
	}

	var data1;
	var data2;
		
	data1 = appoDataDal.substr(6,4) + appoDataDal.substr(3,2) + appoDataDal.substr(0,2);
	data2 = DataOdierna.substr(6,4) + DataOdierna.substr(3,2) + DataOdierna.substr(0,2);
		
	if (oldDataDal != appoDataDal) {
		if (data1 < data2){
			alert("La Data di Inizio validit� non pu� essere inferiore alla Data Odierna")
			frmModEspArea.txtDataDal.focus()  
			return false
		}
	}	
	
	if (TRIM(frmModEspArea.txtDataAl.value) == ""){
//		alert("Campo 'Validit� Al' obbligatorio")
//		frmModEspArea.txtDataAl.focus() 
//		return false	
		frmModEspArea.txtDataAl.value = "31/12/9999"	
	}

	appoDataAl = TRIM(frmModEspArea.txtDataAl.value)
	if (!ValidateInputDate(appoDataAl)){
		frmModEspArea.txtDataAl.focus()  
		return false
	}
	
	data1 = appoDataAl.substr(6,4) + appoDataAl.substr(3,2) + appoDataAl.substr(0,2);

	if (oldDataAl != appoDataAl) {
		if (data1 < data2){
			alert("La Data di Fine validit� non pu� essere inferiore alla Data Odierna")
			frmModEspArea.txtDataAl.focus()  
			return false
		}	
	}
	
	data1 = appoDataDal.substr(6,4) + appoDataDal.substr(3,2) + appoDataDal.substr(0,2);
	data2 = appoDataAl.substr(6,4) + appoDataAl.substr(3,2) + appoDataAl.substr(0,2);

	if (data2 < data1){
		alert("La Data di Fine Validit� non pu� essere inferiore alla Inizio Validit�")
		frmModEspArea.txtDataAl.focus()  
		return false
	}	
	
	return true	
}

</script>
<!-- ************** Javascript Fine ************ -->

<!-- ************** ASP Inizio *************** -->


<!-- ************** ASP Fine *************** -->
<!-- ************** ASP inizio *************** -->
<!--#include virtual = "/include/DecCod.asp"-->
<%sub Inizio()%>
<!--	<table border="0" width="520" cellspacing="0" cellpadding="0" height="81">	   <tr>	     <td width="500" background="<%'=Session("Progetto")%>/images/titoli/strumenti2b.gif" height="81" valign="bottom" align="right">		       <table border="0" width="260" height="30" cellspacing="0" cellpadding="0">	         <tr>	             <td width="100%" valign="top" align="right">	                 <b class="TBLTEXT1FADa">Esperto-Area Tematica</span></b>	             </td>	         </tr>	       </table>	     </td>	   </tr>	</table>-->
	<br>
	<table cellpadding="0" cellspacing="0" width="500" border="0">
		<tr height="18">
			<td class="sfondomenu" height="18" width="67%">
			<span class="tbltext0"><b>Associazione Esperto-Area Tematica</b></span></td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
			<td valign="middle" align="right" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="TBLTEXT1FAD">(*) campi obbligatori</td>
		</tr>
		<tr width="371" class="SFONDOCOMMFAD">
			<td colspan="3">Testo di prova<a href="Javascript:Show_Help('/Pgm/help/Formazione/Assistenza/Esperto/ESP_ModEspArea')" onmouseover="javascript:window.status=' '; return true"><img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
			</td>
		</tr>
		<tr height="2">
			<td colspan="3" class="SFONDOCOMMFAD" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>
	<br>

	<form name="frmModEspArea" method="post" onsubmit="return ControllaDati(this,'<%=sDataDal%>','<%=sDataAl%>','<%=dDataOdierna%>')" action="ESP_CnfEspArea.asp">

<%
end sub

'------------------------------------------------------------------------------------------------------------------------------------------------------------------------

sub Msgetto(sMessaggio)
%>
<br>
    <table width="500">
		<tr><td class="tbltext3" align="center"><b><%=sMessaggio%></b></td></tr>	
	</table>
<br>		
<%			
end sub

'------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	
sub Fine()
%>
</form>
<!--#include virtual = "/include/closeconn.asp"-->
<!--#include virtual = "strutt_coda2.asp"-->
<%
end sub



%>
<!-- ************** ASP Fine *************** -->
<!-- ************** MAIN Inizio ************ -->
<%

dim sSQL

dim sIdArea
dim sIdUtente
dim sCognome
dim sNome
dim sDescArea
dim sDataDal
dim sDataAl
dim sDataTmst
dim sAppo
dim sDatiEsperto
dim dDataOdierna
		
	dDataOdierna=Date


	sDatiEsperto = Request("cmbEsperto")

	if trim(sDatiEsperto) <> "" then
		aAppo = Split(sDatiEsperto,"|")
		sIdUtente	= aAppo(0)
		sCognome	= aAppo(1)
		sNome		= aAppo(2)
	end if

	sIdArea = Request("txtIdArea")
	sDescArea = Request("txtDescArea")
	sDataDal = Request("txtDataDal")
	sDataAl = Request("txtDataAl")
	sDtTmst = Request("txtDataTmst")	

	if sDataAl = " - " then
		sDataAl = ""
	end if

	inizio()
%>
	
	<input type="hidden" value="Mod" name="Action">
	<input type="hidden" value="<%=sDatiEsperto%>" name="DatiEsperto">	
	<input type="hidden" value="<%=sIdArea%>|<%=sDescArea%>" name="cmbAreaTematica">
	<input type="hidden" value="<%=sDataTmst%>" name="DataTmst">	

	<table border="0" cellpadding="1" cellspacing="1" width="500">
		<tr>
			<td align="left" width="35%">
				<span class="TBLTEXT1FAD"><b>Esperto </b></span>
			</td>	
			<td align="left">
				<input class="TEXTblackFAD" type="text" disabled value="<%=sCognome &  " " & sNome%>" name="txtUtente" size="60">
			</td>
		</tr>
		<tr>
			<td align="left" width="35%">
				<span class="TBLTEXT1FAD"><b>Area Tematica </b></span>
			</td>	
			<td align="left">
				<input class="TEXTblackFAD" type="text" disabled value="<%=sDescArea%>" name="txtAreaTematica" size="60">
			</td>
		</tr>
		<tr>
			<td align="left" width="35%">
				<span class="TBLTEXT1FAD"><b>Validit� Dal *</b></span>
			</td>	
			<td align="left">
				<input class="TEXTblackFAD" type="text" value="<%=sDataDal%>" name="txtDataDal" size="12">
			</td>
		</tr>
		<tr>
			<td align="left" width="35%">
				<span class="TBLTEXT1FAD"><b>Validit� Al *</b></span>
			</td>	
			<td align="left">
				<input class="TEXTblackFAD" type="text" value="<%=sDataAl%>" name="txtDataAl" size="12">
			</td>
		</tr>
	</table>
	
	<br>
	
	<!--impostazione dei comandi-->
	<table align="center" cellpadding="0" cellspacing="0" width="200" border="0">	
		<tr align="center">
			<td nowrap><input type="image" name="Conferma" src="<%=Session("progetto")%>/images/conferma.gif"></td value="Registra">
			<td nowrap><a href="javascript:VaiInizio('<%=sDatiEsperto%>')"><img src="<%=session("Progetto")%>/images/indietro.gif" border="0" name="imgPunto2" onmouseover="javascript:window.status=' '; return true"></a></td>
		</tr>
	</table>
	<br>
<%
	Fine()
%>
<!-- ************** MAIN Fine ************ -->
