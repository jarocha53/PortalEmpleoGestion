<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%'Option Explicit 
   Response.ExpiresAbsolute = Now() - 1 
   Response.AddHeader "pragma","no-cache"
   Response.AddHeader "cache-control","private"
   Response.CacheControl = "no-cache"
%>

<!--#include virtual="/strutt_testa2.asp"-->
<!--#include virtual="/include/openconn.asp"-->
<!--#include virtual="/util/portallib.asp"-->
<%
'if ValidateService(session("idutente"),"ESP_VisEspArea", CC) <> "true" then 
'	response.redirect "/util/error_login.asp"
'end if
%>

<!-- ************** Javascript inizio ************ -->

<script language="JAVAScript">

function VaiInizio(sDatiEsperto){
	location.href="ESP_VisEspArea.asp?cmbEsperto=" + sDatiEsperto + "&Modo=1";
}
</script>

<!-- ************** Javascript Fine ************ -->

<!-- ************** ASP inizio *************** -->

	<!--#include virtual = "/util/dbutil.asp"-->

	<!--#include virtual = "/include/ControlDateVB.asp"-->
	<!--#include virtual = "/include/SysFunction.asp"-->
<%
Sub Msgetto(Msg)
%>
	<br>
	<table align="center">
		<tr>
			<td class="tbltext3"><b><%=Msg%></b></td>
		</tr>	
	</table>
<%
End Sub

'-------------------------------------------------------------------------------------------------------------------------------	

sub Inizio()
%><!--	<table border="0" width="520" cellspacing="0" cellpadding="0" height="81">	   <tr>	     <td width="500" background="<%'=Session("Progetto")%>/images/titoli/strumenti2b.gif" height="81" valign="bottom" align="right">	       <table border="0" width="260" height="30" cellspacing="0" cellpadding="0">	         <tr>	           <td width="100%" valign="top" align="right">	           <b class="tbltext1a">Esperto-Area Tematica</b></td>	         			</tr>	       </table>	     </td>	   </tr>	</table>	<br>	<table cellpadding="0" cellspacing="0" width="500" border="0">		<tr height="18">			<td class="sfondomenu" height="18" width="67%">			<span class="tbltext0"><b>&nbsp;Gestione Esperto per Area Tematica</b></span></td>			<td width="3%" background="<%'=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>			<td valign="middle" align="right" width="50%" 				background="<%'=Session("Progetto")%>/images/sfondo_linguetta.gif" 				class="tbltext1"></td>		</tr>		<tr height="2">			<td colspan="3" class="SFONDOCOMM" 			background="<%'=Session("Progetto")%>/images/separazione.gif">			</td>		</tr>	</table>	<br>-->
<%

end sub

'------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

sub GetParam()

	sAction				= Request("Action")
	sDatiAreaTematica	= Request("cmbAreaTematica")
	sDatiEsperto		= Request("DatiEsperto")
	sDataDal			= Request("txtDataDal")
	sDataAl				= Request("txtDataAl")
	sDtTmst				= Request("DataTmst")

end sub

'------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

sub Inserisci()

	sSQL =	"INSERT INTO ESPERTOAREA " &_
				"(IDUTENTE," &_
				"ID_AREATEMATICA," & _
				"DAL," &_
				"AL," &_
				"DT_TMST) " &_
			"VALUES (" &_
				     sIdUtente & "," &_
				     sIdAreaTematica & "," & _
					 ConvDateToDbs(sDataDal) & "," & _
					 ConvDateToDbs(sDataAl) & "," & _
					 ConvDateToDb(Now()) & ")"
	
	'Response.Write sSQL
			
	
	Errore=Esegui("IDUTENTE = & sIdUtente","ESPERTOAREA",Session("idutente"),"INS", sSQL,1,Now())

	if Errore <>  "0" then
		Msgetto("Errore durante l'inserimento dell'associazione." & Errore)
	else
		Msgetto("Associazione correttamente inserita.") 
	end if 

end sub

'------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

sub Modifica()

'	sSQL = "UPDATE ESPERTOAREA SET " &_
'			"AL = '" & sDataAl & "'" &_
'			" WHERE IDUTENTE = " & sIdUtente  & " AND" &_
'			" ID_AREATEMATICA = " & sIdAreaTematica & " AND" &_
'			" DAL = " & ConvDateToDbs(sDataDal)   
	
	cc.begintrans
	
	sSQL = "DELETE FROM ESPERTOAREA" &_
			" WHERE IDUTENTE = " & sIdUtente  & " AND" &_
			" ID_AREATEMATICA = " & sIdAreaTematica & " AND" &_
			" DAL = " & ConvDateToDbs(sDataDal)   

	
	'Response.Write sSQL

'	Errore=Esegui("IDUTENTE=" & sIdUtente ,"ESPEWRTOAREA",Session("idutente"),"MOD", sSQL, 1,sDtTmst)

'	if Errore <>  "0" then
'		Msgetto("Errore durante la modifica dell'associazione Esperto/Area Tematica" & Errore)
'	else
'		Msgetto("Modifica associazione Esperto/Area Tematica correttamente effettuata.") 
'	end if 

	Errore=EseguiNoCTrace(Session("progetto"),"IDUTENTE=" & sIdUtente ,"ESPERTOAREA",Session("idutente"),Session("Id_Persona"),"P","DEL", sSQL,1,sDtTmst,CC)

	if Errore <>  "0" then
		cc.rollbacktrans
		response.write errore
		Msgetto("Errore durante la modifica dell'associazione " & Errore)
		exit sub
	end if
		
	sSQL =	"INSERT INTO ESPERTOAREA " &_
				"(IDUTENTE," &_
				"ID_AREATEMATICA," & _
				"DAL," &_
				"AL," &_
				"DT_TMST) " &_
			"VALUES (" &_
				     sIdUtente & "," &_
				     sIdAreaTematica & "," & _
					 ConvDateToDbs(sDataDal) & "," & _
					 ConvDateToDbs(sDataAl) & "," & _
					 ConvDateToDb(Now()) & ")"

	'Response.Write sSQL

	Errore=EseguiNoCTrace(Session("progetto"),"IDUTENTE=" & sIdUtente ,"ESPERTOAREA",Session("idutente"),Session("Id_Persona"),"P","DEL", sSQL, 1,sDtTmst,CC)

	if Errore <>  "0" then
		cc.rollbacktrans
		Msgetto("Errore durante la modifica dell'associazione " & Errore)
	else
		cc.committrans
		Msgetto("Modifica associazione correttamente effettuata.") 
	end if 

end sub

'------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

sub Fine()
%>
	<br>
	<br>
	<table border="0" cellpadding="0" cellspacing="1" width="500">
		<tr>
			<td align="center">
				<!--a href="javascript:history.back()">					<img src="<%=session("Progetto")%>/images/indietro.gif" border="0" name="imgPunto2" onmouseover="javascript:window.status=' '; return true">				</a-->
				<a href="javascript:VaiInizio('<%=sDatiEsperto%>')">
					<img src="<%=session("Progetto")%>/images/indietro.gif" border="0" name="imgPunto2" onmouseover="javascript:window.status=' '; return true">
				</a>
			</td>
		</tr>
	</table>
	<br>
<!--#include virtual = "/include/closeconn.asp"-->
<!--#include virtual = "strutt_coda2.asp"-->
<%
end sub

%>
<!-- ************** ASP Fine *************** -->

<!-- ************** MAIN Inizio ************ -->
<%

dim sSQL

dim	sAction
dim	sDatiAreaTematica
dim	sDatiEsperto
dim sIdUtente
dim sIdAreaTematica
dim	sDataDal
dim	sDataAl
dim sDtTmst

dim aAppo

	Inizio()

	GetParam()

	aAppo = Split(sDatiEsperto,"|")
	sIdUtente	= aAppo(0)

	aAppo = Split(sDatiAreaTematica,"|")
	sIdAreaTematica	= aAppo(0)

	select	case sAction

			case "Ins"
				Inserisci()

			case "Mod" 
				Modifica()
	end select
	
	Fine()
%>
<!-- ************** MAIN Fine ************ -->
