<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%'Option Explicit 
   Response.ExpiresAbsolute = Now() - 1 
   Response.AddHeader "pragma","no-cache"
   Response.AddHeader "cache-control","private"
   Response.CacheControl = "no-cache"
%>

<!-- #include virtual="/strutt_testa2.asp"-->
<!--#include virtual = "/include/openconn.asp"-->
<!--#include virtual="/util/portallib.asp"-->
<!--#include virtual="/util/DBUtil.asp"-->
<!--#include virtual = "/include/ControlDateVB.asp"-->


<%
'if ValidateService(session("idutente"),"ESP_VisEspArea", CC) <> "true" then 
'	response.redirect "/util/error_login.asp"
'end if
%>
<!-- ************** Javascript inizio ************ -->

<script LANGUAGE="Javascript">
<!--#include virtual="/include/ControlString.inc"-->
<!--#include virtual="/include/help.inc"-->

function Reload(){
	
	if (frmVisEspArea.cmbEsperto.value != "") {
		frmVisEspArea.action = "ESP_VisEspArea.asp"
		frmVisEspArea.submit()
	} else {
		alert("Indicare un esperto");
		frmVisEspArea.cmbEsperto.focus();
	}

}

//--------------------------------------------------------------------------------------------------------------------------------------------------------	
  function VaiInserimento(){

	if (frmVisEspArea.cmbEsperto.value != "") {
		frmVisEspArea.action = "ESP_InsEspArea.asp";
		frmVisEspArea.submit();
	} else {
		alert("Indicare un esperto");
		frmVisEspArea.cmbEsperto.focus();
	}

//	newWindow = window.open("ESP_InsEspArea.asp?IdUtente=" + sIdUtente + "&Cognome=" + sCognome + "&Nome=" + sNome,'newWind','width=540,height=350,Resize=No')
}
//--------------------------------------------------------------------------------------------------------------------------------------------------------

function VaiModifica(sArea,sDescArea,sDataDal,sDataAl,sDtTmst){

	frmVisEspArea.txtIdArea.value = sArea;
	frmVisEspArea.txtDescArea.value = sDescArea;	
	frmVisEspArea.txtDataDal.value = sDataDal;
	frmVisEspArea.txtDataAl.value = sDataAl;		
	frmVisEspArea.txtDataTmst.value = sDtTmst;		
	
	frmVisEspArea.action = "ESP_ModEspArea.asp";
	frmVisEspArea.submit();

//	newWindow = window.open("ESP_ModEspArea.asp?IdUtente=" + sIdUtente + "&Cognome=" + sCognome + "&Nome=" + sNome + "&IdArea=" + sArea + "&DescArea=" + sDescArea + "&DataDal=" + sDataDal + "&DataAl=" + sDataAl,'newWind','width=540,height=350,Resize=No')
}

</script>

<!-- ******************  Javascript Fine *********** -->
<!-- ************** ASP inizio *************** -->
<!--#include virtual = "/include/DecCod.asp"-->
<%sub Inizio()%>
<!--	<table border="0" width="520" cellspacing="0" cellpadding="0" height="81">	   <tr>	     <td width="500" background="<%'=Session("Progetto")%>/images/titoli/strumenti2b.gif" 	     height="81" valign="bottom" align="right">		       <table border="0" width="260" height="30" cellspacing="0" cellpadding="0">	         <tr>	             <td width="100%" valign="top" align="right">	                 <b class="TBLTEXT1FADa">Esperto-Area Tematica</span></b>	             </td>	         </tr>	       </table>	     </td>	   </tr>	</table>-->
	<br>
	<table cellpadding="0" cellspacing="0" width="500" border="0">
		<tr height="18">
			<td class="sfondomenu" height="18" width="67%">
			<span class="tbltext0"><b>Associazione Esperto-Area Tematica</b></span></td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
			<td valign="middle" align="right" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="TBLTEXT1FAD"></td>
		</tr>
		<tr width="371" class="SFONDOCOMMFAD">
			<td colspan="3"></b>Selezionare l'esperto per visualizzare le associazioni 
			esistenti.Per associare l'esperto ad n area, selezionarlo e cliccare sul link 
			crea associazione.<a href="Javascript:Show_Help('/Pgm/help/Formazione/Assistenza/Esperto/ESP_VisEspArea')" onmouseover="javascript:window.status=' '; return true"><img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
			</td>
		</tr>
		<tr height="2">
			<td colspan="3" class="SFONDOCOMMFAD" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>
	<br>

	<form name="frmVisEspArea" method="post" action="ESP_VisEspArea.asp">

<%
end sub

'------------------------------------------------------------------------------------------------------------------------------------------------------------------------

sub Msgetto(sMessaggio)
%>
<br>
    <table width="500">
		<tr><td class="tbltext3" align="center"><b><%=sMessaggio%></b></td></tr>	
	</table>
<br>		
<%			
end sub

'------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	
sub Fine()
%>
</form>
<!--#include virtual = "/include/closeconn.asp"-->
<!--#include virtual = "strutt_coda2.asp"-->
<%
end sub

'-----------------------------------------------------------------------------------------------------------------------------------------------------------

function CaricaEsperti()
dim sSQL
dim i
dim rsEsperti
dim aOutArray
dim nEle

	sSQL =	"SELECT B.IDUTENTE, B.COGNOME, B.NOME " & _
			"FROM GRUPPO A, UTENTE B " &_
			"WHERE A.COD_RUOFU = 'ES' " &_
			"AND A.IDGRUPPO = B.IDGRUPPO " &_
			"ORDER BY B.COGNOME, B.NOME" 

	set rsEsperti = Server.CreateObject("ADODB.Recordset")
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	rsEsperti.open sSQL, CC, 3

	if rsEsperti.EOF then
	   rsEsperti.Close
	   set rsEsperti = nothing
	   redim aOutArray (0,0)
	   aOutArray(0, 0) = "null"
	   CaricaEsperti = aOutArray
	   exit function
	end if

	nEle = rsEsperti.RecordCount

	i = 0
	redim aOutArray(2, nEle - 1)
	do while not rsEsperti.EOF
		aOutArray(0, i) = rsEsperti("IDUTENTE")
		aOutArray(1, i) = rsEsperti("COGNOME")
		aOutArray(2, i) = rsEsperti("NOME")		
	
		i = i + 1
		rsEsperti.MoveNext
	loop
	
	CaricaEsperti = aOutArray

end function

'-----------------------------------------------------------------------------------------------------------------------------------------------------------

function CaricaEspertoArea(sIdUtente)
dim sSQL
dim i
dim rsEspertoArea
dim aOutArray
dim nEle
dim dDataOdierna

	dDataOdierna=ConvDateToDB(Date)
	
	sSQL =	"SELECT A.ID_AREATEMATICA, B.TITOLOAREA, " &_
			"A.DAL, A.AL, A.DT_TMST " & _
			"FROM ESPERTOAREA A, AREA B " &_ 
			"WHERE A.IDUTENTE = " & sIdUtente &_
			" AND B.ID_AREA = A.ID_AREATEMATICA" &_
			" AND A.AL >= " & dDataOdierna
				
	'Response.Write sSql
				
	set rsEspertoArea = Server.CreateObject("ADODB.Recordset")
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	rsEspertoArea.open sSQL, CC, 3

	if rsEspertoArea.EOF then
	   rsEspertoArea.Close
	   set rsEspertoArea = nothing
	   redim aOutArray (0,0)
	   aOutArray(0, 0) = "null"
	   CaricaEspertoArea = aOutArray
	   exit function
	end if

	nEle = rsEspertoArea.RecordCount	
	
	i = 0
	redim aOutArray(4, nEle - 1)
	do while not rsEspertoArea.EOF
		aOutArray(0, i) = rsEspertoArea("ID_AREATEMATICA")
		aOutArray(1, i) = rsEspertoArea("TITOLOAREA")
		aOutArray(2, i) = rsEspertoArea("DAL")
		aOutArray(3, i) = rsEspertoArea("AL")
		aOutArray(4, i) = rsEspertoArea("DT_TMST")
		
		i = i + 1
		rsEspertoArea.MoveNext
	loop
	
	CaricaEspertoArea = aOutArray

end function

'-----------------------------------------------------------------------------------------------------------------------------------------------------------

sub ImpEsperti()

dim aEsperti
dim i

	aEsperti = CaricaEsperti()
	if aEsperti(0, 0) = "null" then
		Msgetto("Non ci sono Utenti")
		exit sub
	end if

'	if trim(sIdUtente) <> "" then%>
	<%'else%>
		<input type="hidden" name="Modo" value="1">
		<br>
		<table width="500" border="0" cellspacing="1" cellpadding="0">
			<tr>
				<td align="left">
					<span class="TBLTEXT1FAD"><b>Esperto</b></span>
				</td>	
				<td align="left">
		    		<select class="TEXTBLACKFAD" name="cmbEsperto" onchange="JAVASCRIPT:Reload()">
					<%
					if sIdUtente = "" then
						%>
						<option selected></option>
						<%
					else	
						%>
						<option></option>
						<%
					end if		
					for i = 0 to ubound(aEsperti, 2)
						Response.Write "<OPTION "
						if trim(sIdUtente) = trim(aEsperti(0, i)) then 
							Response.Write ("Selected ")
						end if 
						Response.write "value ='" & aEsperti(0, i) & "|" & aEsperti(1, i) & "|" & _ 
						aEsperti(2, i) & "'> " & aEsperti(1, i) & " " & aEsperti(2, i) & "</OPTION>"
					next
					%>
					</select>
				</td>
			</tr>
		</table>
	<%'end if%>
	<br>
	<table width="500">
		<tr> 
			<td align="center"> 
				 <a href="Javascript:VaiInserimento()" class="textred" onmouseover="javascript:window.status=' '; return true">
			     <b>Crea associazione</b></a>
			</td>
	   </tr>		
	</table>		
	<%
end sub

'------------------------------------------------------------------------------------------------------------------------------------------------------------------------
sub ImpEspertoArea()

dim aEspertoArea
dim sArea
dim sDescArea
dim sDataDal
dim sDataAl
dim sDtTmst
dim i

	%>
	<!--input type="hidden" name="cmbEsperto" value="<%=sIdUtente%>"-->
	<%
	aEspertoArea = CaricaEspertoArea(sIdUtente)
	if aEspertoArea(0, 0) = "null" then
		Msgetto("Non ci sono associazioni Esperto/Area Tematica")
	else
		%>
		<br>
		<table border="0" width="500">
			<tr class="SFONDOCOMMFAD"> 
				<td align="left" valign="center" width="70%">
					<b>Descrizione</b>
				</td>
				<td align="left" valign="center" width="15%">
					<b>Valida dal</b>
				</td>
				<td align="left" valign="center" width="15%">
					<b>Al</b>
				</td>
			</tr>
			<%
			for i = 0 to ubound(aEspertoArea, 2)
				sArea = trim(aEspertoArea(0,i))
				sDescArea = trim(aEspertoArea(1,i))
				sDataDal = ConvDateToString(CSTR(aEspertoArea(2,i)))
				sDataAl = ConvDateToString(CSTR(aEspertoArea(3,i)))
				sDtTmst = ConvDateToString(CSTR(aEspertoArea(4,i)))
				if sDataAl = "31/12/9999" then
					sDataAl = " - "
				end if
			%>			
				<tr class="TBLSFONDOFAD">
					<td width="70%">
						<a class="TBLAGGFAD" href="Javascript:VaiModifica('<%=sArea%>','<%=sDescArea%>','<%=sDataDal%>','<%=sDataAL%>','<%=sDtTmst%>')" onmouseover="javascript:window.status=' '; return true">
						<%=sDescArea%></a>
					</td>
					<td width="15%" align="center" class="TBLDETTFAD">
						<span class="TBLDETTFAD"><%=sDataDal%></span>
					</td>
					<td width="15%" align="center" class="TBLDETTFAD">
						<span class="TBLDETTFAD"><%=sDataAl%></span>
					</td>
				</tr>
			<%
			next
			%>	
		</table>
		<br>
		<%
 	end if
end sub

%>
<!-- ************** ASP Fine *************** -->
<!-- ************** MAIN Inizio ************ -->
<%
	dim sSQL
	dim sIdUtente
	dim sCognome
	dim sNome
	dim sModo
	
	dim sAppo
	dim aAppo	
	
	sModo	= Request("Modo")
	
	sAppo = Request("cmbEsperto")

	if trim(sAppo) <> "" then
		aAppo = Split(sAppo,"|")
		sIdUtente	= aAppo(0)
		sCognome	= aAppo(1)
		sNome		= aAppo(2)
	end if
	
	Inizio()
	ImpEsperti()

	if trim(sModo) = "1" then
		ImpEspertoArea()
	end if
%>
	<input type="hidden" name="txtIdArea" value>
	<input type="hidden" name="txtDescArea" value>
	<input type="hidden" name="txtDataDal" value>
	<input type="hidden" name="txtDataAl" value>
	<input type="hidden" name="txtDataTmst" value>			
<%
	Fine()
%>
<!-- ************** MAIN Fine ************ -->



