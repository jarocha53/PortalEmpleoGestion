<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%
Sub ControlliJavaScript()
%>
<script language=javascript>
var finestra
function VisRis(nId, sTipo)
{
	if (finestra != null )
	{
		finestra.close(); 
	}
	f='PRO_VisCoOQu.asp?id='+nId+'&tipo='+sTipo
	finestra=window.open(f,"","width=560,height=450,location=no,menubar=no,scrollbars=yes,resizable=no")
}
</script>
<%
End Sub
';););););););););););););););););););););););););););););););););););););););)
Sub Inizio()
	dim sTitolo
	dim sCommento
	dim sHelp
	sTitolo = ""
	sCommento = "La scheda dei progressi ti permette di visualizzare il " &_
				"riepilogo delle attivit� da te svolte nel tuo percorso di " &_
				"formazione. In questo modo puoi controllare costantemente " &_
				"il livello di completamento delle attivit� ed i risultati " &_
				"da te ottenuti nelle prove di valutazione. "
	sHelp = ""
	
	call TestataFAD(sTitolo, sCommento, false, sHelp)
End Sub
';););););););););););););););););););););););););););););););););););););););)
Function Decodifica(sYoN)
dim sMess
select case sYoN
case "S"
	sMess = "Superato"
case "N"
	sMess = "Non Superato"
end select 
Decodifica = sMess

End Function
';););););););););););););););););););););););););););););););););););););););)
Sub VisualizzaCorsi()
dim sSQL, rsCorsi
dim nProg, aCorsi()

'sSQL = "SELECT CORSO.ID_CORSO, CORSO.TIT_CORSO, CORSO.ID_AREATEMATICA " &_
'		"FROM CATCORSI, CORSO " &_
'		"WHERE " &_
'		"CATCORSI.ID_CORSO = CORSO.ID_CORSO AND " &_
'		"CATCORSI.FL_OBBL = 1 AND " &_
'		"CATCORSI.IDGRUPPO = " & session("idgruppo") &_  
'		" and sysdate >= dt_disponibile"

sSQL = "SELECT CORSO.TIT_CORSO, CORSO.ID_CORSO " &_
		"FROM CATCORSI, CORSO " &_
		"WHERE " &_
		"CATCORSI.ID_CORSO = CORSO.ID_CORSO AND " &_
		"CATCORSI.FL_OBBL = 1 AND " &_
		"CATCORSI.IDGRUPPO = " & session("idgruppo") &_  
		" and sysdate >= dt_disponibile"

'Response.Write sSQL
'set rsCorsi = CC.execute(sSQL)
set rsCorsi = Server.CreateObject("ADODB.Recordset")

'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
rsCorsi.Open sSQL,CC,3
if not rsCorsi.EOF then
   aCodice = rsCorsi.GetRows
	redim preserve aCorsi(2,0)
	aCorsi(0,0) = "Massimo"
	aCorsi(1,0) = 100
	aCorsi(2,0) = 0
	redim preserve aCorsi(2,1)

	for i = 0 to UBound(aCodice,2)
		sSQL = "SELECT LESSON_STATUS FROM STUDENT_DATA WHERE " &_
			" COURSE_ID = '" & aCodice(1,i) & "'" &_
			" AND STUDENT_ID = '" & Session("idutente")& "'"

'		sSQL = "SELECT LESSON_STATUS FROM CORE WHERE " &_
'			" COURSE_ID = '" & aCodice(1,i) & "'" &_
'			" AND STUDENT_ID = '" & Session("idutente")& "'"
			
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
		set rsRisultCorso = CC.Execute(sSQL)

		nSTato = 0 
		nElem = 0
		if not rsRisultCorso.eof then
			do while not rsRisultCorso.eof
				nElem = nElem + 1
				sLStatus = rsRisultCorso("LESSON_STATUS")

				select case UCase(sLStatus)
					case "P"
						nSTato = nSTato + 6
					case "C"
						nSTato = nSTato + 5
					case "F"
						nSTato = nSTato + 4
					case "I"
						nSTato = nSTato + 3
					case "B"
						nSTato = nSTato + 2
					case "A"
						nSTato = nSTato + 1
				end select 
				rsRisultCorso.Movenext
			loop 
			sPunteggio = nSTato * 100 / (6*nElem)

		else
			sPunteggio = 0
		end if
		rsRisultCorso.close
		set rsRisultCorso = nothing 
		redim preserve aCorsi(2,i+1)

		aCorsi(0,i+1) = aCodice(0,i)
		aCorsi(1,i+1) = sPunteggio
		aCorsi(2,i+1) = aCodice(1,i)

	next 

	MakeGraph aCorsi, "CORSI" 

else 
	sMessaggio = "Nessun corso "

end if
rsCorsi.Close
set rsCorsi = nothing


End Sub
';););););););););););););););););););););););););););););););););););););););)
Sub VisualizzaQuestionari()
dim sSQL, rsQuest
dim nProg 

'sSQL =  "SELECT RISULT_QUEST.ID_RISULT_QUEST, QUESTIONARIO.ID_QUESTIONARIO, QUESTIONARIO.TIT_QUESTIONARIO " &_
'		"FROM RISULT_QUEST, QUESTIONARIO " &_
'		"WHERE " &_
'		"RISULT_QUEST.ID_QUESTIONARIO = QUESTIONARIO.ID_QUESTIONARIO AND " &_		
'		"RISULT_QUEST.ID_PERSONA = " & session("idutente")

		 
sSQL =  "SELECT RISULT_QUEST.ID_RISULT_QUEST, QUESTIONARIO.ID_QUESTIONARIO, QUESTIONARIO.TIT_QUESTIONARIO, RISULT_QUEST.FL_ESEGUITO, " &_
		"RISULT_QUEST.NUM_PUNTI, RISULT_QUEST.NUM_TENTATIVI " &_
		"FROM RISULT_QUEST, QUESTIONARIO " &_
		"WHERE " &_
		"RISULT_QUEST.ID_QUESTIONARIO = QUESTIONARIO.ID_QUESTIONARIO AND " &_
		"RISULT_QUEST.ID_PERSONA = " & session("idutente")
		
		'esponse.Write sSQL

'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
set rsQuest = CC.execute(sSQL)
if rsQuest.eof then
	if sMessaggio > "" then
		sMessaggio = sMessaggio & " e nessun questionario "
	else
		sMessaggio = "Nessun questionario "
	end if
else
%>
<table border="0" width="500" cellpadding="1" cellspacing="2">
	<tr class="sfondocomm" height="23">
		<td><b>QUESTIONARI</b></td>
	</tr>
</table>
<table border="0" width="500" cellpadding="1" cellspacing="2">
	<tr class="sfondocomm" height="23">
		<!--<td width="10"><b>#</b></td>-->
		<td width="260"><b>Titolo Questionario</b></td>
		<td width="80"><b>Stato</b></td>
		<td width="60"><b>Punti</b></td>
		<td width="100"><b>Tentativi</b></td>
	</tr>
	<%
	nProg = 1 
	do while not rsQuest.eof
	%>
		<tr class=tblsfondo>
			<!--<td class=tbldett>
				<%=nProg%>
			</td>-->
			<td class=tbldett>
				<!--<a href="javascript:VisRis(<%=rsQuest("ID_QUESTIONARIO")%>,'T')" class="tblagg">-->
					<%=strHTMLEncode(rsQuest("TIT_QUESTIONARIO"))%>
				<!--</a>-->
			</td>
		    <td class=tbldett>
				<%=Decodifica(rsQuest("FL_ESEGUITO"))%>
		    </td>
		    <td class=tbldett>
				<%=rsQuest("NUM_PUNTI")%>
			</td>	
		    <td class=tbldett>
				<%=rsQuest("NUM_TENTATIVI")%>
			</td>	
		</tr>		
	<%
		nProg = nProg + 1
		rsQuest.movenext
	loop
	%>
	</table>
	</br>
<% 
end if
End Sub
';););););););););););););););););););););););););););););););););););););););)
'M A I N
%>
<!--#include virtual="/strutt_testa2.asp"-->
<!--#include virtual='/include/SetTestataFAD.asp'-->
<!--#include virtual="/include/openconn.asp"-->
<!--#include virtual="/include/DecCod.asp"-->
<!--#include virtual="/include/HTMLEncode.asp"-->
<!--#include virtual="/include/SetPulsanti.asp"-->
<!--#include virtual="/include/MakeGraph.asp"-->
<%
dim sMessaggio
ControlliJavaScript()
Inizio()
VisualizzaCorsi()
Response.Write "<br>"
VisualizzaQuestionari()
if sMessaggio <> "" then%>
	<BR>
	<table border="0" width="500" cellpadding="0" cellspacing="0">
	   <tr align="center">
	   	<td class="tbltext3"> 
	   		<%=sMessaggio%> disponibile
	   	</td>
	   </tr>
	</table>
	<br>	
<%end if%>
<!--#include virtual ="/include/closeconn.asp"-->
<!--#include virtual="/strutt_coda2.asp"-->
