
function ControllaDateGruppo(ind,Oggi) {
	var nGrupElem;
	var i;
	var nElemChecked;

	nGrupElem = 1;
	if ( eval("document.form1.chkIdGru" + ind )) {
		if ( eval("document.form1.chkIdGru" + ind + ".length")) 
			nGrupElem = eval("document.form1.chkIdGru" + ind + ".length")
		// Quali sono ceccati.
		nElemChecked = 0;
		if (nGrupElem > 1) {
			for ( i=0; i<nGrupElem; i++ ) {
				if (eval("document.form1.chkIdGru" + ind + "[" + i + "].checked")) {
					nElemChecked = nElemChecked + 1;
					
					// Prima controllo se data valida.
					if (!ValidateInputDate(eval("document.form1.txtDtDalGru" + ind + "[" + i + "].value ") ) ) {
						eval("document.form1.txtDtDalGru" + ind + "[" + i + "].focus()") ;
						return false;
					}
					// Controllo se maggiore della data odierna.
					if (ValidateRangeDate(eval("document.form1.txtDtDalGru" + ind + "[" + i + "].value "),Oggi)) {
						eval("document.form1.txtDtDalGru" + ind + "[" + i + "].focus()") ;
						alert("La data deve essere superiore alla data odierna."); 
						return false;
					}
					
					// Controllo se esiste la data fine.
					if (eval("document.form1.txtDtAlGru" + ind + "[" + i + "].value ") != "" ) {

						// Controllo che la data fine sia una data valida.
						if (!ValidateInputDate(eval("document.form1.txtDtAlGru" + ind + "[" + i + "].value ") ) ) {
							eval("document.form1.txtDtAlGru" + ind + "[" + i + "].focus()") ;
							return false;
						}
						// Controllo l'integrit� con la data inizio.
						if (!ValidateRangeDate(eval("document.form1.txtDtDalGru" + ind + "[" + i + "].value "), eval("document.form1.txtDtAlGru" + ind + "[" + i + "].value "))) {
							eval("document.form1.txtDtDalGru" + ind + "[" + i + "].focus()") ;
							alert("Controllare l'intervallo di date indicato."); 
							return false;
						}
					}
				} else {
					if (eval("document.form1.txtDtDalGru" + ind + "[" + i + "].value ") != "") {
						alert("Se si ha indicato la data occorre selezionare anche il gruppo."); 
						eval("document.form1.txtDtDalGru" + ind + "[" + i + "].focus()");
						return false;
					}
					if (eval("document.form1.txtDtDalGru" + ind + "[" + i + "].value ") != "") {
						alert("Se si ha indicato la data di fine occorre selezionare anche il gruppo."); 
						eval("document.form1.txtDtDalGru" + ind + "[" + i + "].focus()");
						return false;
					}
				}
			}
		} else {
			if (eval("document.form1.chkIdGru" + ind + ".checked")) {
				nElemChecked = 1;

				// Prima controllo se data valida.
				if (!ValidateInputDate(eval("document.form1.txtDtDalGru" + ind + ".value ") ) ) {
					eval("document.form1.txtDtDalGru" + ind + ".focus()") ;
					return false;
				}
				// Controllo se maggiore della data odierna.
				if (ValidateRangeDate(eval("document.form1.txtDtDalGru" + ind + ".value "),Oggi)) {
					eval("document.form1.txtDtDalGru" + ind + ".focus()") ;
					alert("La data deve essere superiore alla data odierna."); 
					return false;
				}
					
				
				if (eval("document.form1.txtDtAlGru" + ind + ".value ") != "" ) {
					// Prima controllo se data valida.
					if (!ValidateInputDate(eval("document.form1.txtDtAlGru" + ind + ".value ") ) ) {
						eval("document.form1.txtDtAlGru" + ind + ".focus()") ;
						return false;
					}
					// Controllo validit� l'omogenit� tra data fine e data inizio
					if (!ValidateRangeDate(eval("document.form1.txtDtDalGru" + ind + ".value "), eval("document.form1.txtDtAlGru" + ind + ".value "))) {
						eval("document.form1.txtDtDalGru" + ind + ".focus()") ;
						alert("Controllare l'intervallo di date indicato."); 
						return false;
					}
				}
			} else {
				if (eval("document.form1.txtDtDalGru" + ind + ".value") != "") {
					alert("Se � stata indicata la data di inizio occorre selezionare anche il gruppo."); 
					eval("document.form1.txtDtDalGru" + ind + ".focus()");
					return false;
				}
				if (eval("document.form1.txtDtAlGru" + ind + ".value") != "") {
					alert("Se � stata indicata la data di fine occorre selezionare anche il gruppo."); 
					eval("document.form1.txtDtAlGru" + ind + ".focus()");
					return false;
				}
							
			}
		
		}

	}
	return true
}
		
function ControllaDateRorga(ind,Oggi) {

	if (eval("document.form1.txtDtDal" + ind + ".value") == "")	{
		alert ("Indicare la data di inizio periodo di fruizione del gruppo");
		eval("document.form1.txtDtDal" + ind + ".focus()");
		return false;
	} else {
		if (ValidateRangeDate(eval("document.form1.txtDtDal" + ind + ".value"), Oggi)) {
			alert ("Data di inizio periodo di fruizione del gruppo inferiore alla data odierna");
			eval("document.form1.txtDtDal" + ind + ".focus()");
			return false;
		}
		if (!ValidateInputDate(eval("document.form1.txtDtDal" + ind + ".value"))) {
			eval("document.form1.txtDtDal" + ind + ".focus()");
			return false;
		}
	}
							
	if (eval("document.form1.txtDtAl" + ind + ".value") == "") {
	/*	alert ("Indicare la data di fine periodo di fruizione del gruppo");
		eval("document.form1.txtDtAl" + i + ".focus()");
		return false;
	*/	
	} else {
		if (!ValidateInputDate(eval("document.form1.txtDtAl" + ind + ".value "))) {
			eval("document.form1.txtDtAl" + ind + ".focus()") ;
			return false;
		}
	}

	if (eval("document.form1.txtDtDal" + ind + ".value ") != "" && eval("document.form1.txtDtAl" + ind + ".value ") != "")	{
		if (!ValidateRangeDate(eval("document.form1.txtDtDal" + ind + ".value "), eval("document.form1.txtDtAl" + ind + ".value "))) {
			eval("document.form1.txtDtAl" + ind + ".focus()") ;
			alert("Controllare l'intervallo di date indicato."); 
			return false;
		}
	}
	return true

}
			
	function controllo_date(n , Oggi) {		
		var ind;

		for ( ind = 0; ind < n; ind++ ) {		
			if(eval("document.form1.chkRuo" + ind + ".checked")) {
				if (!eval("document.form1.chkIdGru" + ind )) { // Controlla esistenza dei sottogruppi.
					if (!ControllaDateRorga(ind,Oggi)) 
						return false
				} else {
					// Controllo se � stato selezionato un sottogruppo,
					// se � selezionato verifico la esistenza della data inizio.
					nGrupElem = 1;
					if ( eval("document.form1.chkIdGru" + ind + ".length")) 
						nGrupElem = eval("document.form1.chkIdGru" + ind + ".length")
					if (nGrupElem == 1 ) {
					
						if (eval("document.form1.chkIdGru" + ind + ".checked") ) {
							if ((!eval("document.form1.chkRuo" + ind + ".disabled")) && (eval("document.form1.txtDtDalGru" + ind + ".value") != "")) {
								if (!ControllaDateGruppo(ind,Oggi)) 
									return false
							} else {
								if (eval("document.form1.txtDtDalGru" + ind + ".value ") == "") { 
									alert("Spuntare il ruolo organizzativo oppure un gruppo.")
									eval("document.form1.txtDtDalGru" + ind + ".focus()")
									return false
								} else {
									if (!ControllaDateGruppo(ind,Oggi)) 
										return false
								}
								
								
							}
						} else {
							if 	((eval("document.form1.txtDtDalGru" + ind + ".value") != "") && ( !eval("document.form1.chkIdGru" + ind + ".disabled")) ){
								alert("Spuntare il ruolo organizzativo oppure un gruppo.")
								eval("document.form1.txtDtDalGru" + ind + ".focus()")
								return false
							} else {
								if (!ControllaDateRorga(ind,Oggi))
									return false
							}
						}
					} else {
						for (nEle=0 ; nEle<nGrupElem; nEle++) {
							if (eval("document.form1.chkIdGru" + ind + "[" + nEle + "].checked") ) {
								if ((eval("document.form1.chkRuo" + ind + ".disabled")) && (eval("document.form1.txtDtDalGru" + ind + "[" + nEle + "].value") != "")) {
									if (!ControllaDateGruppo(ind,Oggi)) 
										return false
								} else {
									if (eval("document.form1.txtDtDalGru" + ind + "[" + nEle + "].value ") == "") { 
										alert("Spuntare il ruolo organizzativo oppure un gruppo.")
										eval("document.form1.txtDtDalGru" + ind + "[" + nEle + "].focus()")
										return false
									} else {
										if (!ControllaDateGruppo(ind,Oggi)) 
											return false
									}
								}
							} else {
	
								if 	((eval("document.form1.txtDtDalGru" + ind + "[" + nEle + "].value") != "") && (!eval("document.form1.chkRuo" + ind + ".disabled"))) {
									alert("Spuntare il ruolo organizzativo oppure un gruppo.")
									eval("document.form1.txtDtDalGru" + ind + "[" + nEle + "].focus()")
								} else {
									if (!ControllaDateRorga(ind,Oggi))
										return false
								}
							
							}
						}
					}
				}	
				
			} else {
				if (!ControllaDateGruppo(ind,Oggi))
					return false
			}
		}
		return true
		//return false;
	}
				
			
	function validateIt(n, Oggi) { 
	// si controlla il numero dei ruoli selezionati per la fruizione del forum
		var R = 0	//indicatore del numero dei ruoli
		var nGrupElem = 0;
		// Non gestiamo pi� l'inferiorit� della data
		// odierna perch� nella modifica/inserimento 
		// canale il controllo non permetteva di insierire 
		// un nuovo canale.
		Oggi = "01/01/1901"
		for (var ind=0 ; ind<n;ind++) {	
			if (eval("document.form1.chkRuo" + ind + ".checked")) {
				R = R + 1;
			} 

			if (eval("document.form1.chkIdGru" + ind ))  {
				if ( eval("document.form1.chkIdGru" + ind + ".length")) 
					nGrupElem = eval("document.form1.chkIdGru" + ind + ".length")
				if (nGrupElem == 1 ) {
					if (eval("document.form1.chkIdGru" + ind + ".checked") ) {
						R = R + 1;
					}
				} else {
					for (nEle=0 ; nEle<nGrupElem; nEle++) {
						if (eval("document.form1.chkIdGru" + ind + "[" + nEle + "].checked") )
							R = R + 1;
					}				
				}
			}			

			nGrupElem = 0; 
		}
		// Si controlla se l'utente vuole passare da un tipo di fruizione pubblica a una particolare
				
		if (document.form1.chkPS.value!="Pubblico")	//chkPs � != Pubblico se il forum  � pubblico
		{	
			if (R != 0 ) {
		//se l'utente seleziona uno dei canali disponibili essendo il forum di tipo pubblico
		//la sua scelta comporter� l'eliminazione del canale pubblico dal forum
				if (confirm("Selezionando i seguenti canali verr� eliminato il canale Pubblico dai fruitori del forum.Si vuole procedere?")) {
					if (controllo_date(n-1, Oggi))	{
						 // document.form1.submit();
						return true;
					}else
						return false;
				
				} else
					return false;	
			} else {
				if (document.form1.txtDtDalPS.value == "")	{ 
					alert ("Indicare la data di inizio periodo di fruizione del gruppo");
					document.form1.txtDtDalPS.focus();
					return false;
				}else	{ 
					if (ValidateRangeDate(document.form1.txtDtDalPS.value, Oggi)) {
						alert ("Data di inizio periodo di fruizione del gruppo inferiore alla data odierna");
						document.form1.txtDtDalPS.focus() ;
						return false;
					}
					if (!ValidateInputDate(document.form1.txtDtDalPS.value)) {
						document.form1.txtDtDalPS.focus() ;
						return false;
					}
				}
				if (document.form1.txtDtAlPS.value == "") {	
			/*		alert ("Indicare la data di fine periodo di fruizione del gruppo");
					document.form1.txtDtAlPS.focus();
					return false;
			*/	} else {	
					if (!ValidateInputDate(document.form1.txtDtAlPS.value)) {
		 				document.form1.txtDtAlPS.focus();
						return false;
					}
				}
				if (document.form1.txtDtDalPS.value != "" && document.form1.txtDtAlPS.value != "") {	
					if (!ValidateRangeDate(document.form1.txtDtDalPS.value, document.form1.txtDtAlPS.value)) {
						document.form1.txtDtAlPS.focus();
						alert("Controllare l'intervallo di date indicato."); 
						return false;
					}
				}	
			}
		}
				
		if (document.form1.chkPS.value =="Pubblico") //chkPs � = Pubblico se il forum  non � pubblico
		{
		//se l'utente seleziona il canale pubblico essendo il forum di tipo particolare
		//la sua scelta comporter� l'eliminazione dei canali dal forum
			if (document.form1.chkPN.checked) {
				if (confirm("Se si seleziona il canale 'Pubblico' verranno eliminati tutti gli altri canali di fruizione.Si vuole procedere?")) {	
						
					if (document.form1.txtDtDalPN.value == "")	{
						alert ("Indicare la data di inizio periodo di fruizione del gruppo");
						document.form1.txtDtDalPN.focus();
						return false;
					}
					else {
						if (ValidateRangeDate(document.form1.txtDtDalPN.value, Oggi))
						{
							alert ("Data di inizio periodo di fruizione del gruppo inferiore alla data odierna");
							document.form1.txtDtDalPN.focus() ;
							return false;
						}
							
						if (!ValidateInputDate(document.form1.txtDtDalPN.value)) {
							document.form1.txtDtDalPN.focus() ;
							return false;
							}
					}
							
					if (document.form1.txtDtAlPN.value == "") {
					/*		alert ("Indicare la data di fine periodo di fruizione del gruppo");
							document.form1.txtDtAlPN.focus();
							return false;
					*/	
					} else {
						if (!ValidateInputDate(document.form1.txtDtAlPN.value)) {
							document.form1.txtDtAlPN.focus();
							return false;
						}
					}
					return true;	
				}
				else
				{
					return false;
				}
			// se entrambi i campi data sono riempiti faccio un controllo per verificare il range della data.
				if (document.form1.txtDtDalPN.value != "" && document.form1.txtDtAlPN.value != "") {
					if (!ValidateRangeDate(document.form1.txtDtDalPN.value, document.form1.txtDtAlPN.value)) {
						document.form1.txtDtAlPN.focus();
						alert("Controllare l'intervallo di date indicato."); 
						return false;
					}
					else {
						//document.form1.submit();
						return true;
					}
				} else
					return false;	
			}
			
		}
				
		for (var ind=0 ; ind<n;ind++) {	
			if (!eval("document.form1.chkRuo" + ind +".checked")) { 
				if ((eval("document.form1.txtDtDal" + ind + ".value") != "" || eval("document.form1.txtDtAl" + ind + ".value") != "")) { 
						alert("Selezionare il canale di fruizione relativo alla data inserita")
						eval("document.form1.txtDtDal" + ind +".focus()");
						return false;
				}			
			}
		}

		if(!document.form1.chkPN.checked && (document.form1.txtDtDalPN.value != "" || document.form1.txtDtAlPN.value != ""))
			{ 
				alert("Selezionare il canale di fruizione relativo alla data inserita")
				document.form1.chkPN.focus();
				return false;
			}	
		if (controllo_date(n, Oggi)) {	
			//document.form1.submit();
			//return true;
		} else {
			return false;
		}
		
	return true;
}	
//--------------------------------------------------------------------------	
	function chktutte(ind)	{
	
		// Controllo se ci sono sotto elementi.
		if (eval("document.form1.chkIdGru" + ind )) { 

			if (eval("document.form1.chkRuo" + ind + ".checked"))
				chk = true
			else
				chk = false
				 
			// Controllo se i sotto elementi sono pi� di uno.
			if (eval("document.form1.chkIdGru" + ind + ".length")) {
				nNumEle = eval("document.form1.chkIdGru" + ind + ".length")
				for(i=0;i<nNumEle;i++){
					pippo = eval("document.form1.chkIdGru" + ind + "[" + i + "]")
					if ((!pippo.disabled) && (chk))
					pippo.checked = false
					//pippo.disabled = chk
				}		
			}/* else {
				pippo = eval("document.form1.chkIdGru" + ind )
//				if (!pippo.disabled)
//					pippo.checked = chk
				//pippo.disabled = chk
			}*/
		}			

	}
	

