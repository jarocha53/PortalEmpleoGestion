<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include virtual = "util/portallib.asp"-->
<!--#include virtual = "include/ckProfile.asp"-->
<html>
<!--	BLOCCO HEAD			-->
<head>
<title>Dettaglio Risposte</title>
<link REL="STYLESHEET" TYPE="text/css" HREF="<%=Session("Progetto")%>/fogliostile.css">
<script LANGUAGE="JavaScript">
<!--
<!--#include Virtual = "/Include/help.inc"-->	
	function Elimina(categoria, idRisp, idDom){
		
		if (confirm("Conferma l'eliminazione della risposta?"))	{
			opener.frmAlbero.id.value= idRisp;
			opener.frmAlbero.s.value= "0";
			opener.frmAlbero.cat.value= categoria;
			opener.frmAlbero.qt.value= idDom;
			opener.frmAlbero.modo.value="del";
			opener.frmAlbero.submit();
		//	opener.location.replace("FOR_Albero.asp?modo=del&cat=" + categoria + "&id=" + idRisp + "&qt=" + idDom);
			window.close();
			}
	}

	function Rispondi(categoria, idRisp, idDom){
		opener.frmRisp.risp.value= idRisp;
		opener.frmRisp.cat.value= categoria;
		opener.frmRisp.qt.value= idDom;
		opener.frmRisp.submit();
		//opener.location.replace("FOR_InsRisposte.asp?qt=" + idDom + "&cat=" + categoria + "&risp=" + idRisp);
		window.close();
	}	
//-->	
</script>
</head>
<!--	FINE BLOCCO HEAD	-->

<!--	BLOCCO ASP			-->
<%
	Sub Inizio()

	If not ValidateService(Session("IdUtente"),"GESTIONE FORUM",cc) Then 
		response.redirect "/util/error_login.asp"
	End If
	
	
%>
<body>
<table border="0" width="100%" cellspacing="0" cellpadding="0" height="80">
   <tr>
      <td width="380" background="<%=Session("Progetto")%>/images/titoli/Community3b.gif" height="80" valign="bottom" align="left">
        <table border="0" background align="left" width="368" height="25" cellspacing="0" cellpadding="0">
          <tr>
            <td width="100%" valign="top" align="right"><b><font face="Verdana" size="2" color="#006699">Dettaglio Forum <i><%=Session("Category")%>&nbsp;&nbsp;</i></font></b>
            <a href="Javascript:Show_Help('/Pgm/help/Formazione/Forum/FOR_VisRisposte')" name onmouseover="javascript:status='' ; return true">
							<img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a></span>
            </td>
          </tr>
        </table>
      </td>
	</tr>
 </table>
 <br>
<%
	End Sub
%>	
<!--------------------------------------------------------------------------------------------------------------------------------------------------------->
<%
	Sub ImpostaPag()
		
	Dim sqlRisp, RRRisp, strText
	Dim SQLs, RRs
		
	sqlRisp = "SELECT ID_FRM_RISPOSTA, FRM_RISPOSTA.OGGETTO, FRM_RISPOSTA.MITTENTE, FRM_RISPOSTA.DT_TMST AS RespDate, FRM_RISPOSTA.ID_FRM_RISPOSTA" &_
			", FRM_RISPOSTA.IP_ADDRESS, FRM_RISPOSTA.RISPOSTA" & _
			" FROM FRM_RISPOSTA " & _
			" WHERE ID_FRM_DOMANDA=" & idDom & _
			" ORDER BY FRM_RISPOSTA.DT_TMST DESC"		
		
'	Response.Write sqlRisp
	set rsRisp  = Server.createObject("adodb.recordset")
'PL-SQL * T-SQL  
SQLRISP = TransformPLSQLToTSQL (SQLRISP) 
	rsRisp.open  sqlRisp, CC, 2, 2
	
	Do While not rsRisp.eof 
		idRisp = rsRisp("ID_FRM_RISPOSTA")
		
		SQLs = " Select ID_FRM_RISPOSTA From FRM_RISPOSTA where ID_RISPOSTA ="  & idRisp
'PL-SQL * T-SQL  
SQLS = TransformPLSQLToTSQL (SQLS) 
		set RRs = CC.Execute(SQLs)
%>	
		<table border="0" cellspacing="0" cellpadding="0" width="100%" align="center">	
 		<tr>
 			<td align="center" width="15" valign="bottom" height="20"> 
 				&nbsp;
<%				if ckProfile(Session("mask"),8) and RRs.Eof then
%>					<a href="javascript:Elimina(<%=cat%>,<%=idRisp%>,<%=idDom%>)" onmouseover="window.status =' '; return true">
					<img src="<%=Session("Progetto")%>/images/cestino.gif" border="0" alt="Elimina la risposta"></td>
<%				end if
				RRs.Close
				set RRs = nothing
%>		
			</td>
	  		<td align="right" colspan="2" valign="bottom"><img src="<%=Session("Progetto")%>/images/msg.gif" border="0"><a class="textblack" name="<%=idRisp%>"><span class="size9"><%=idRisp%></span></a></td>
	  	</tr>		  	
	  	<tr class="tblsfondo2">
	  		<tr height="20">
			<td height="15" width="15" valign="middle"><img SRC="<%=Session("Progetto")%>/images/righinaPSX.jpg"></td> 
			<td width="400" class="tblsfondo3"><b class="tbltext0"><%=Server.HTMLEncode(rsRisp("OGGETTO"))%></b></td>
			<td height="15" width="16" valign="middle"><img SRC="<%=Session("Progetto")%>/images/righinaPDX.jpg"></td>
	  	</tr>
	  	</table>
		<table border="0" cellspacing="1" cellpadding="1" width="90%%" align="center">	
	  	<tr>
	  		<td COLSPAN="1" class="tblsfondo">	
	  			<span class="tbltext">data:&nbsp;<%= ConvDateTimeToString(rsRisp("RespDate"))%>
	  		</span>
	  		</td>	  		
	  	</tr>
	  	<tr class="tblsfondo" height="80">
	  		<td COLSPAN="3">
 			  <span class="tbltext"><b><%=Server.HTMLEncode(rsRisp("risposta"))%></b></span>
	  		</td>
	  	</tr>
	  	</table>
		<table border="0" cellspacing="1" cellpadding="1" width="90%" align="center">	
		<tr class="tblsfondo2">
	  		<td valign="bottom" align="center" width="50%">
	  		<% if ckProfile(Session("mask"),2) then %>
	  		<a class="textblacka" href="javascript:Rispondi('<%=cat%>','<%=idRisp%>','<%=idDom%>')"><span class="size9"><b>Rispondi al messaggio</b></span></a>
	  		<% end if %>
	  		</td>
	  		<td valign="bottom" align="center"><a class="textblacka" href="javascript:window.close()"><span class="size9"><b>Chiudi</b></span></a></td>
	  	</tr>
		</table>
		<br><br><br><br>
<%	
		rsRisp.movenext
	loop 
	rsRisp.Close
	Set rsRisp=Nothing 
%> 
<br>
<br>
<%
	End Sub
%>
<!--------------------------------------------------------------------------------------------------------------------------------------------------------->
<!--	FINE BLOCCO ASP		-->

<!--		MAIN			-->
<%
	Dim strConn, Rs, sql
%>	
<!--#include virtual = "include/openconn.asp"-->
<!--#include virtual = "include/controlDateVB.asp"-->
<%

	idDom = clng(Request("qt"))
	cat	  = clng(Request("cat"))
		
	Inizio()
	ImpostaPag()
		
%>	<!--#include virtual ="include/closeconn.asp"-->

<!--	FINE BLOCCO MAIN	-->
