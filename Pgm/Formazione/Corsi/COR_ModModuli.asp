<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%

 Response.ExpiresAbsolute = Now() - 1 
 Response.AddHeader "pragma","no-cache"
 Response.AddHeader "cache-control","private"
 Response.CacheControl = "no-cache"

%>
<script language="javascript">

<!--#include virtual = "/include/SelQuestionario.js"-->	
<!--#include virtual="/include/Help.inc"-->

	function Invia(frmQuest){
	
		/*if (document.frmQuest.NomeTxtDesc.value == ""){
			alert('Al Modulo devi associare un tipo di Questionario.')
			return false;
		}else{
			document.frmQuest.submit();
			return true;
		}*/
		
	}
</script>


<!--#include virtual="/strutt_testa2.asp"-->
<!--#include virtual="/include/HTMLEncode.asp"-->

<%
dim DescrQuestio
dim sql
dim recSel
dim IdCorso
dim IdAreaTem
dim rec
dim NessunErrore
dim TrovatoErrore
dim DescCorso,sTitCorso
dim CodQuest
dim IdElemento

Sub Inizio()
	dim sFunzione
	dim sTitolo
	dim sCommento
	dim bCampiObbl
	
	sFunzione = "ASSOCIAZIONE TEST-CORSO"
	sTitolo = "ASSOCIAZIONE TEST-CORSO"
	sCommento = "E' possibile associare un test al corso o ai moduli del corso, " &_
				"cliccando sull'icona posta a destra della casella di testo."
	bCampiObbl = false
	sHelp="/Pgm/help/formazione/corsi/Default.htm"
%>
<!--#include virtual="/include/SetTestata.asp"-->
<!--#include virtual="/include/openconn.asp"-->

<%
End Sub

call Inizio()

call Struttura()

SUB Struttura()

IdCorso=Request.Form("IdCorso")
IdAreaTem=Request.Form("CodArea")


set recSel=Server.CreateObject("ADODB.Recordset")

sSqlDesc="select TIT_CORSO from Corso where" &_
		 " ID_CORSO=" & IdCorso & ""


'PL-SQL * T-SQL  
SSQLDESC = TransformPLSQLToTSQL (SSQLDESC) 
recSel.Open sSqlDesc,CC,3
	
if recSel.EOF then%>
	
	<script language="javascript">
		alert('Errore non previsto')
		history.back()
	</script>
	
	<%exit sub
else
	
	set recQ=Server.CreateObject("ADODB.Recordset")
	'seleziono il test DEL CORSO!
	sqlQ="select q.tit_questionario,q.id_questionario from " &_
		"questionario q, provevalutazione p where " &_
		"p.id_corso=" & IdCorso & " and  p.id_questionario=q.id_questionario"
		

'PL-SQL * T-SQL  
SQLQ = TransformPLSQLToTSQL (SQLQ) 
	recQ.Open sqlQ,CC,3
	
	if recQ.eof then
		DescrQuestio=""
		CodQuest=""
	else
		DescrQuestio=recQ("TIT_QUESTIONARIO")
		CodQuest=recQ("ID_QUESTIONARIO")
		
	end if
	
	sTitCorso=recSel("TIT_CORSO")
end if

	
			
END SUB


%>

<form name="frmQuest" action="COR_CnfSelModuli.asp" method="post">

<input type="hidden" id="IdCorso" name="IdCorso" value="<%=IdCorso%>"><%'IdCorso%>
<input type="hidden" id="NomeTxtIdArea" name="NomeTxtIdArea"><%'NomeTxtIdArea%>


<table WIDTH="500" BORDER="0">
	<tr class="sfondocommFad">
		<td>
			Corso
		</td>
		<td colspan="2">
			Test di valutazione
		</td>
	</tr>	
	<tr class="tblsfondoFad">
		<td class="tblDettFad">
			<%=strHTMLEncode(sTitCorso)%>
		</td>
		<td>
			<input type="text" id="NomeTxtDesc" name="NomeTxtDesc" size="35" value="<%=DescrQuestio%>" readonly style="TEXT-TRANSFORM: uppercase" class="textBlackFad">
			<input type="hidden" id="NomeTxtIdQuest" name="NomeTxtIdQuest"><%'NomeTxtIdQuest%>
			<input type="hidden" id="VecchioIdQuest" name="VecchioIdQuest" value="<%=CodQuest%>"><%'vecchio Id Qu %>
		
			<input type="hidden" id="txtIdElemento" name="txtIdElemento" value="<%=IdElemento%>"><%'txtIdElemento (NON VISUALIZZA NULLA)%>
			<%'*******************************************************************************	
				'conterr� il valore "RIMUOVI" se si intende rimuove la selezione del test precedentemente associata al CORSO%>
				<input type="hidden" id="TxtRimozioneTestCorso" name="TxtRimozioneTestCorso">
			<%'*******************************************************************************%>	

		</td>
		<td align="left">
			<a href="Javascript:SelQuestionario('frmQuest','NomeTxtDesc',<%=IdAreaTem%>,'NomeTxtIdArea','<%=IdCorso%>','NomeTxtIdQuest','TxtRimozioneTestCorso','<%=CodQuest%>')" ID="imgPunto1" name="imgPunto1" onmouseover="javascript:window.status='';return true"><img border="0" src="<%=Session("Progetto")%>/images/bullet1.gif"></a>

		</td>
	</tr>
	
</table>

<br>



<%

'************* Inizio la select per i moduli associati al Corso**************
dim recModu
dim SqlModuli

	set recModu=Server.CreateObject("ADODB.Recordset")
'seleziono i Moduli del corso !
	SqlModuli = "SELECT e.id_elemento, e.titolo_elemento, e.tipo_elemento " &_
		"FROM elemento e, regola_dipendenza r " &_
		"WHERE e.id_elemento = r.id_elemento and " &_
		"r.id_corso = " & IdCorso & " and e.tipo_elemento='L'"
	
'PL-SQL * T-SQL  
SQLMODULI = TransformPLSQLToTSQL (SQLMODULI) 
		recModu.Open SqlModuli,CC,3
	
		cont=0	
		if not recModu.EOF then
			dim IdElem
%>			<table WIDTH="500" BORDER="0">
				<tr>
					<td width="23">&nbsp;</td>
					<td class="sfondocommFad">Modulo</td>
					<td colspan="2" class="sfondocommFad">
						Test di valutazione
					</td>
				</tr>
					
	<%			do until recModu.EOF	
					IdElem=recModu("id_elemento")
					VecchiaDescrQuest=""
	%>				<tr>
						<td>&nbsp;</td>
						<td class="tblSfondoFad">
							<font class="tblDettFad">
							<%=strHTMLEncode(recModu("TITOLO_ELEMENTO"))%>
							</font>
						</td>
						<td class="tblsfondoFad">
		<%					dim sqlE
							dim recE
							dim VecchioID_ELE
							dim VecchiaDescrEle
							set recE=Server.CreateObject("ADODB.Recordset")
							sqlE="select q.id_questionario,q.tit_questionario,p.id_elemento from questionario q, provevalutazione p where p.id_elemento=" & IdElem & " and p.id_questionario=q.id_questionario"
								
'PL-SQL * T-SQL  
SQLE = TransformPLSQLToTSQL (SQLE) 
							recE.Open sqlE,CC,3
									
							if recE.eof then
								VecchioID_ELE=recModu("id_elemento")
								VecchiaDescrEle=""
							else
									
								VecchioID_ELE=recE("ID_ELEMENTO")
								VecchiaDescrEle=recE("ID_QUESTIONARIO")
								VecchiaDescrQuest=strHTMLEncode(recE("TIT_QUESTIONARIO"))
							end if
									
							'contengono i nuovi dati da inserire o modificare%>
							<input type="hidden" id="txtHidModQuesti<%=cont%>" name="txtHidModQuesti<%=cont%>">
							<input type="hidden" id="NomeTxtIdQuesti<%=cont%>" name="NomeTxtIdQuesti<%=cont%>"><%'nuovo ID_Q%>
									
							<input type="hidden" id="NomeTxtIdEle<%=cont%>" name="NomeTxtIdEle<%=cont%>" value="<%=VecchioID_ELE%>"><%'idelemento%>
								
						<%'*******************************************************************************%>	
						<%'contiene i vecchi dati presi dal DB%>
							<input type="text" id="VecchiaDescModQuesti<%=cont%>" name="VecchiaDescModQuesti<%=cont%>" value="<%=VecchiaDescrQuest%>" SIZE="35" readonly style="TEXT-TRANSFORM: uppercase" class="textBlackFad"><%'descrizione del QUESTIONARIO%>
							<input type="hidden" id="VecchiaHidModQuesti<%=cont%>" name="VecchiaHidModQuesti<%=cont%>" value>
							<input type="hidden" id="VecchiaTxtIdQuesti<%=cont%>" name="VecchiaTxtIdQuesti<%=cont%>" value="<%=VecchiaDescrEle%>"><%'vecchio id quest%>
								
						<%'*******************************************************************************	
							'conterr� il valore "RIMUOVI" se si intende rimuove la selezione del test precedentemente associata al MODULO%>
							<input type="hidden" id="TxtRimozioneTestModulo<%=cont%>" name="TxtRimozioneTestModulo<%=cont%>">
						<%'*******************************************************************************%>	
						</td>
						<td class="tblsfondo" ALIGN="LEFT">                                                                                                                                                                      <%'contiene il vecchio ID Questionario %>
							<a href="Javascript:SelQuestionario('frmQuest','VecchiaDescModQuesti<%=cont%>',<%=IdAreaTem%>,'txtHidModQuesti<%=cont%>','<%=IdCorso%>','NomeTxtIdQuesti<%=cont%>','TxtRimozioneTestModulo<%=cont%>','<%=VecchiaDescrEle%>')" ID="imgPunto" name="imgPunto" onmouseover="javascript:window.status='';return true"><img border="0" src="<%=Session("Progetto")%>/images/bullet1.gif"></a>
						</td>
					</tr>
	<%				recE.close
					set recE=nothing
					cont=cont+1
					recModu.MoveNext
			    loop
%>
	</table>
	<input type="hidden" id="txtContatore" name="txtContatore" value="<%=cont%>"><%'contatore%>

<%		

		end if
%>
	<br>
	<table WIDTH="500" BORDER="0" CELLSPACING="1" CELLPADDING="1" align="middle">
		<tr>
			<td colspan="5" align="middle">
				<a href="javascript:history.back()">
					<img src="<%=Session("progetto")%>/images/indietro.gif" border="0" onmouseover="javascript:window.status='' ; return true">
				</a>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<input type="image" name="Invia" src="<%=Session("Progetto")%>/images/conferma.gif" border="0" value="Conferma" onclick="return Invia(frmQuest)">
			</td>
		</tr>
	</table>

	</form>


<!--#include virtual="/include/closeconn.asp"-->
<!--#include virtual="/strutt_coda2.asp"-->







