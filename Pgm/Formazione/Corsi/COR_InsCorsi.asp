<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%
Sub NoCache()
	Response.ExpiresAbsolute = Now() -1
	Response.AddHeader "pragma", "no-cache"
	Response.AddHeader "cache-control", "private"
	Response.CacheControl = "no-cache"
End Sub
'-----------------------------------------------------------------------
Sub ControlliJavaScript()
%>
<script language="Javascript">
<!--#include virtual="/include/ControlDate.inc"-->
<!--#include virtual="/include/ControlNum.inc"-->
<!--#include virtual="/include/ControlString.inc"-->
function OreMinuti(valore)
{
	if (valore.substr(2,1) != ":")
	{
		return false;
	}
	if (isNaN(valore.substr(0,2)))
	{
		return false;
	}
	if (isNaN(valore.substr(3,2)))
	{
		return false;
	}
	if (valore.substr(3,2) > 60)
	{
		//alert ('ma de che')
		return false;
	}
	return true;	
}
function FormInserisciCorso_Validator(frmCorso)
	{
	if (frmCorso.cmbArea.value == "")
		{
		frmCorso.cmbArea.focus();
		alert("Il campo 'Area' � obbligatorio.");
		return (false);
	  	}
		
	if (frmCorso.txtTitoloCorso.value == "")
		{
		frmCorso.txtTitoloCorso.focus();
		alert("Il campo 'Titolo Corso' � obbligatorio.");
		return (false);
	  	}

	if (frmCorso.txtDescrizione.value == "")
		{
		frmCorso.txtDescrizione.focus();
		alert("Il campo 'Descrizione' � obbligatorio.");
		return (false);
	  	}

	if (frmCorso.cboFlErogazione.value == "")
		{
		frmCorso.cboFlErogazione.focus();
		alert("Il campo 'Modalit� di erogazione' � obbligatorio.");
		return (false);
	  	}

	if (frmCorso.txtDurata.value != "")
		{
		if (OreMinuti(frmCorso.txtDurata.value) == false)
			{
				frmCorso.txtDurata.focus();
				alert("Il campo 'Durata corso' deve essere in formato 'hh:mm'.");
				return (false);				
			}
		}

	if (frmCorso.chkDispo.checked == false)
	{
		if (!ValidateInputDate(frmCorso.txtDisponibilitaDal.value))
			{
				frmCorso.txtDisponibilitaDal.focus();
				return (false);
			}
	}	

	if (frmCorso.cboFlPubblico.value == "")
		{
		frmCorso.cboFlPubblico.focus();
		alert("Il campo 'Pubblico/Privato' � obbligatorio.");
		return (false);
	  	}			

	if (FormInserisciCorso.ChkCorsoAICC.checked==true)
	{
		if (FormInserisciCorso.txtVerCorsoAICC.value=="")
			{
			frmCorso.txtVerCorsoAICC.focus();
			alert("Il campo 'Versione' � obbligatorio.");
			return (false);
			}

		if (frmCorso.cboLivCorso.value == "")
			{
			frmCorso.cboLivCorso.focus();
			alert("Il campo 'Livello Corso' � obbligatorio.");
			return (false);
		  	}

		if (frmCorso.txtNumCredCorso.value == "")
			{
			frmCorso.txtNumCredCorso.focus();
			alert("Il campo 'Crediti corso' � obbligatorio.");
			return (false);				
			}
		else
			{
			if (isNaN(frmCorso.txtNumCredCorso.value))
				{
					frmCorso.txtNumCredCorso.focus();
					alert("Il campo 'Crediti corso' � numerico.");
					return (false);				
				}
			}
		//if (frmCorso.txtAuthoring.value == "")
		//	{
		//	frmCorso.txtAuthoring.focus();
		//	alert("Il campo 'Authoring' � obbligatorio.");
		//	return (false);
		//	}


		if (frmCorso.txtInfoProd.value == "")
			{
			frmCorso.txtInfoProd.focus();
			alert("Il campo 'Informazioni Produttore' � obbligatorio.");
			return (false);
			}
	}
	return (true);
}

function ImpFlag()
{
	if (FormInserisciCorso.chkDispo.checked == true)
		{
			FormInserisciCorso.txtDisponibilitaDal.disabled = true
		}
	else
		{
			FormInserisciCorso.txtDisponibilitaDal.disabled = false
		}
}
function ImpFlag2()
{
	if (FormInserisciCorso.ChkCorsoAICC.checked == true)
		{
			FormInserisciCorso.txtVerCorsoAICC.disabled = false
			FormInserisciCorso.cboLivCorso.disabled = false
			FormInserisciCorso.txtNumCredCorso.disabled = false
			FormInserisciCorso.txtAuthoring.disabled = false
			FormInserisciCorso.txtInfoProd.disabled = false
		}
	else
		{
			FormInserisciCorso.txtVerCorsoAICC.disabled = true
			FormInserisciCorso.cboLivCorso.disabled = true
			FormInserisciCorso.txtNumCredCorso.disabled = true
			//FormInserisciCorso.txtAuthoring.disabled = true
			//FormInserisciCorso.txtInfoProd.disabled = true
		}
}
</script>
<%
End Sub
'-----------------------------------------------------------------------
Sub Inizio()
	dim sFunzione
	dim sTitolo
	dim sCommento
	dim bCampiObbl
	
	sFunzione = "GESTIONE CORSI"
	sTitolo = "INSERIMENTO CORSO"
	sCommento = "Compilare i campi sottostanti e premere il pulsante <b>Invia</b> per memorizzare le informazioni"
	bCampiObbl = true
	sHelp = "/Pgm/help/Formazione/Corsi/COR_InsCorsi/"	
%>
	<!--#include virtual="/include/SetTestata.asp"-->
<%

End Sub
'-----------------------------------------------------------------------
	Sub ImpostaPag()
		Dim sql, RR, sql2, RR2
%>
		<form METHOD="POST" onsubmit="return FormInserisciCorso_Validator(this)" name="FormInserisciCorso" action="COR_CnfCorsi.asp">
		<input type=hidden name="Oper" id="Oper" value="Ins">
		<table border="0" cellspacing="2" cellpadding="1" width="500">
			<tr> 
				<td align="left" nowrap class="tbltext1">
					<b>Area</b>*
				</td>
				<td> 
<%					
					sql="SELECT DISTINCT TitoloArea, ID_AREA FROM AREA ORDER BY TitoloArea"
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
					SET RR=CC.EXECUTE (sql)
%>					
					<select name="cmbArea" id="cmbArea" class="textblack">
						<option VALUE></option>
						<% DO WHILE NOT RR.EOF %> 
							<option value="<%=RR.FIELDS("ID_AREA")%>"><%=RR.FIELDS("TitoloArea")%> 
							</option>
						<% RR.MOVENEXT
						   LOOP
%>						
					</select>
<%					RR.CLOSE
%>				</td>
			</tr>
		    <tr> 
				<td align="left" nowrap class="tbltext1">
					<b>Titolo Corso*</b>
				</td>
				<td>
					<input type="text" name="txtTitoloCorso" class="textblacka" size="50" id="txtTitoloCorso" maxlength="100">
				</td>
		    </tr>
		    <tr> 
				<td align="left" nowrap class="tbltext1">
					<b>Descrizione*</b>
				</td>
				<td>
					<span class="tbltext1">&nbsp;- Utilizzabili <label name="lblNumCar1" id="lblNumCar1">254</label> caratteri -</span>
					<textarea name="txtDescrizione" class="textblack" cols="50" rows="3" OnKeyUp="JavaScript:CheckLenTextArea(txtDescrizione,lblNumCar1,254)"></textarea>
					<!--input type="text" name="txtLenDescr" id="txtLenDescr" class="textblacka" size="3" value="254" disabled-->
				</td>
		    </tr>
		    <tr> 
				<td align="left" nowrap class="tbltext1">
					<b>Obiettivo</b>
				</td>
				<td> 
					<span class="tbltext1">&nbsp;- Utilizzabili <label name="lblNumCar2" id="lblNumCar2">254</label> caratteri -</span>
					<textarea name="txtObiettivo" class="textblack" cols="50" rows="3" OnKeyUp="JavaScript:CheckLenTextArea(txtObiettivo,lblNumCar2,254)"></textarea>
				</td>
		    </tr>
		    <tr> 
				<td align="left" nowrap class="tbltext1">
					<b>Modalit� di Erogazione*</b>
				</td>
				<td> 
					<select name="cboFlErogazione" id="cboFlErogazione" class=textblack>
						<option value="" selected></option>
						<option value="0">In presenza</option>
						<option value="1">A distanza</option>
					</select>
				</td>
		    </tr>
		    <tr> 
				<td align="left" nowrap class="tbltext1">
					<b>Durata corso</b>&nbsp;(hh:mm)
				</td>
				<td> 
					<input name="txtDurata" type="text" class="textblacka" size=5 maxlength=5>
				</td>
		    </tr>
		    <tr> 
				<td align="left" nowrap class="tbltext1">
					<b>Disponibilit� dal*</b>
				</td>
				<td class=tbltext1> 
					<input name="txtDisponibilitaDal" type="text" class="textblacka" size="10" maxlength=10>
					&nbsp;&nbsp;&nbsp;
					<input type="checkbox" name="chkDispo" id="chkDispo" onclick="javascript:ImpFlag()">
					(non disponibile)
				</td>
		    </tr>
			<tr height=20> 
				<td align="left" nowrap class="tbltext1">
					<b>Progetto proprietario</b>
				</td>
				<td class=textblack>
					<b>
					<%=DecCodVal("CPROJ", "0", date, mid(session("progetto"),2), 1)%>
					</b>
				</td>
				<div id=dHideCombo1 style="visibility:hidden">
					<%
					CreateCombo("CPROJ||||cboProjOwn||")
					%>
				</div>
			</tr>
			<tr> 
				<td align="left" nowrap class="tbltext1">
					<b>Pubblico/Privato</b>
				</td>
				<td class=textblack>
					<b>
					PRIVATO
					</b>
				</td>
				<div id=dHideCombo2 style="visibility:hidden">
				<select name="cboFlPubblico" id="cboFlPubblico" class=textblack>
					<option value="0">Pubblico</option>
					<option value="1" selected>Privato</option>
				</select>
				</div>
			</tr>
		    <tr> 
				<td align="left" nowrap class="tbltext1">
					<b>Prerequisiti necessari</b>
				</td>
				<td> 
					<span class="tbltext1">&nbsp;- Utilizzabili <label name="lblNumCar4" id="lblNumCar4">2000</label> caratteri -</span>
					<textarea name="txtPrereqCorso" class="textblack" cols="50" rows="3" OnKeyUp="JavaScript:CheckLenTextArea(txtPrereqCorso,lblNumCar4,2000)"></textarea>
				</td>
		    </tr>
		    <tr> 
				<td align="left" nowrap class="tbltext1">
					<b>Destinatari del Corso</b>
				</td>
				<td> 
					<span class="tbltext1">&nbsp;- Utilizzabili <label name="lblNumCar5" id="lblNumCar5">2000</label> caratteri -</span>
					<textarea name="txtDestinatari" class="textblack" cols="50" rows="3" OnKeyUp="JavaScript:CheckLenTextArea(txtDestinatari,lblNumCar5,2000)"></textarea>
				</td>
		    </tr>		    
		    <tr> 
				<td align="left" nowrap class="tbltext1">
					<b>Authoring</b>
				</td>
				<td> 
					<span class="tbltext1">&nbsp;- Utilizzabili <label name="lblNumCar7" id="lblNumCar7">254</label> caratteri -</span>
					<textarea name="txtAuthoring" class="textblack" cols="50" rows="3" OnKeyUp="JavaScript:CheckLenTextArea(txtAuthoring,lblNumCar7,254)"></textarea>
				</td>
		    </tr>
		    <tr> 
				<td align="left" nowrap class="tbltext1">
					<b>Informazioni<br>Produttore</b>
				</td>
				<td>
					<span class="tbltext1">&nbsp;- Utilizzabili <label name="lblNumCar3" id="lblNumCar3">2000</label> caratteri -</span>
					<textarea name="txtInfoProd" class="textblack" cols="50" rows="3" OnKeyUp="JavaScript:CheckLenTextArea(txtInfoProd,lblNumCar3,2000)"></textarea>
				</td>
		    </tr>
		    <tr><td colspan=2><hr></td></tr>
		    <tr> 
				<td align="left" nowrap class="tbltext1">
					<b>Corso AICC</b>
				</td>
				<td class="tbltext1"> 
					<input name="ChkCorsoAICC" type="checkbox" class="textblacka" onclick="javascript:ImpFlag2()">
				</td>
		    </tr>
		    <tr> 
				<td align="left" nowrap class="tbltext1">
					<b>Versione</b>
				</td>
				<td class="tbltext1">
					<input name="txtVerCorsoAICC" size="3" maxlength="3" type="text" class="textblacka" disabled>
				</td>
		    </tr>
		    <tr> 
				<td align="left" nowrap class="tbltext1">
					<b>Livello Corso</b>
				</td>
				<td> 
					<select name="cboLivCorso" id="cboLivCorso" class=textblack  disabled>
						<option value="" selected></option>
						<option value="1">1</option>
						<option value="2">2</option>
						<option value="3">3</option>
					</select>
				</td>
		    </tr>
		    <tr> 
				<td align="left" nowrap class="tbltext1">
					<b>Crediti corso</b>
				</td>
				<td> 
					<input name="txtNumCredCorso" type="text" class="textblacka" maxlength="3" size=3  disabled>
				</td>
		    </tr>
		</table>
		<br>
		<table width="500" border="0" cellspacing="2" cellpadding="1">
			<tr align="center">
				<td align="right">			
					<%
						PlsIndietro()
					%>
				</td>
				<td align="left">
					<%
						PlsInvia("InserisciC")
					%>
				 </td>
			</tr>
		</table> 
		</form> 
		<br>
<%
	End Sub
'------------------------------------------------------------------------
	NoCache()
%>
<!--#include virtual="/strutt_testa2.asp"-->
<!--#include virtual="/include/openconn.asp"-->
<!--#include virtual="/include/DecCod.asp"-->
<!--#include virtual="/include/SetPulsanti.asp"-->
<%	
	
	ControlliJavaScript()
	Inizio()
	ImpostaPag()
%>
<!--#include virtual="/include/closeconn.asp"-->
<!--#include virtual="/strutt_coda2.asp"-->
