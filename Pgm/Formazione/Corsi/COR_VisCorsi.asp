<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%
Sub Inizio()
	dim sFunzione
	dim sTitolo
	dim sCommento
	dim bCampiObbl
	
	sFunzione = "GESTIONE CORSI"
	sTitolo = "GESTIONE CORSO"
	sCommento = "<b>Elenco corsi</b>. Cliccando sul titolo � possibile modificare " &_
				"le caratteristiche del corso; cliccando sull'icona presente " &_
				"sulla colonna " & chr(34) & "<b>Moduli</b>" & chr(34) & " � possibile " &_
				"modificare i moduli del corso"
	bCampiObbl = false
	sHelp = "/Pgm/help/Formazione/Corsi/COR_VisCorsi/"	
	
%>
	<!--#include virtual="/include/SetTestata.asp"-->
<%
End Sub
';););););););););););););););););););););););););););););););););););););););)
Function VerificaData(dDate)
	if dDate = "31/12/9999" then
		VerificaData = "<center>-</center>"
	else
		VerificaData = dDate
	end if
End Function
';););););););););););););););););););););););););););););););););););););););)
Sub ImpostaPag()
	Dim aTipo(2)
	Dim sSQL, ssSQLCond
	Dim sCondTitCorso
	dim sCondTitArea
	dim sCondDestinatari
	dim sCondObiettivi
	dim sCondModalita
	dim sCondDurata
	dim sProgetto

	sProgetto = mid(Session("Progetto"),2)
	aTipo(0) = "In presenza"
	aTipo(1) = "A distanza"
		
	sSQL = "SELECT a.id_area,c.id_corso,c.tit_corso,c.desc_corso,c.dt_disponibile," &_
			"fl_erogazione FROM Corso C, Area A WHERE a.id_area = c.id_areatematica " &_
			" AND ( (id_projowner='" & sProgetto & "' AND  fl_pubblico=1) OR fl_pubblico=0 )"
	sSQLCond = ""

	sCondTitCorso	= Request.Form("txtTitCorso")
	sCondTitArea	= CLng(Request.Form("txtTitArea"))
	sCondDestinatari = Request.Form("txtDestinatari")
	sCondObiettivi	= Request.Form("txtObiettivi")
	sCondModalita	= Request.Form("txtModalita")
	sCondDurata		= Request.Form("txtDurata")

	If sCondTitCorso <> "" Then	
		sSQLCond = sSQLCond & " AND TIT_CORSO LIKE '%" & _
		Replace(sCondTitCorso, "'", "''") & "%'"
	End If

	If sCondTitArea <> 0 Then
		sSQLCond = sSQLCond & " AND A.ID_AREA = " & sCondTitArea
	End If

	If sCondObiettivi <> "" Then
		sSQLCond = sSQLCond & " AND OBIETTIVO_CORSO LIKE '%" & _
		Replace(sCondObiettivi, "'", "''") & "%'"
	End If

	If sCondModalita <> 0 Then
		sSQLCond = sSQLCond & " AND FL_EROGAZIONE ='" & sCondModalita & "'"
	End If

	If sCondDurata <> "" Then	
		sSQLCond = sSQLCond & " AND DURATA_CORSO LIKE '%" & _
		Replace(sCondDurata, "'", "''") & "%'"
	End If

	sSQL = sSQL & sSQLCond & " ORDER BY TIT_CORSO"

	Set rsCorsi = Server.CreateObject("ADODB.Recordset")

'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	rsCorsi.Open sSQL, CC, 3
		
	If rsCorsi.BOF and rsCorsi.EOF Then
	%>
		 <table border="0" width="500" cellpadding="0" cellspacing="0">
			<tr align="center">
				<td class="tbltext3"> 
					Non vi sono Corsi che soddisfano le condizioni di ricerca
				</td>
			</tr>
		 </table>
<%  
	Else	
        rsCorsi.MoveLast
        rsCorsi.MoveFirst
    End If
		
	Set RR = Server.CreateObject("ADODB.Recordset")
	sSQL = "SELECT count(id_corso) as ContaCorso FROM Corso C , Area A WHERE a.id_area= c.id_areatematica " & sSQLCond

'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	set RR = CC.Execute(sSQL)
	nTot = CINT(RR.Fields("ContaCorso"))
	RR.Close
	set RR = nothing
		
	If nTot > 25 Then
	%>
		<table border="0" width="500" cellpadding="0" cellspacing="0">
			<tr align="center">
				<td class="tbltext3"> 
					Troppi Corsi soddisfano le condizioni di ricerca
					<br>
					Raffinare la ricerca!
				</td>
			</tr>
		 </table>
		<br>
<% 
	Else
	    If Not rsCorsi.BOF Then
%>
			<table border="0" width="500">
				<tr class="sfondocomm" height=20>
					<td><b>Titolo</b></td>
					<td><b>Descrizione</b></td>
					<td><b>Moduli</b></td>
					<td><b>Disponibilit�</b></td>
					<td><b>Erogazione</b></td>
				</tr>
		 <%     
				nPrgCorso = 0
				Do While Not rsCorsi.EOF
				nPrgCorso = nPrgCorso + 1
				sNomeForm = "FrmCorso" & nPrgCorso
				if rsCorsi("DT_DISPONIBILE") > date() then
					bPubbl = false
				else
					bPubbl = true
				end if
		 %>		<tr class="tblsfondo"> 
					<form METHOD="POST" name="<%=sNomeForm%>" id="<%=sNomeForm%>" action="COR_ModCorsi.asp">
					<td>
							<input type="hidden" name="CodArea" id="CodArea" value="<%=rsCorsi("ID_AREA")%>">
							<input type="hidden" name="IdCorso" id="IdCorso" value="<%=rsCorsi("ID_CORSO")%>">
							<a HREF="javascript:<%=sNomeForm%>.submit()" class="tblAgg" onmouseover="javascript:window.status='' ; return true"> 
								<%=strHTMLEncode(rsCorsi("TIT_CORSO"))%>
							</a>
					</td>
					</form>
					<td class="tblDett"><%=strHTMLEncode(rsCorsi("Desc_CORSO"))%></td>
					<form METHOD="POST" name="<%=sNomeForm%>E" id="<%=sNomeForm%>E" action="COR_VisElementi.asp">
					<td align="center">
						<input type="hidden" name="IdCorso" id="IdCorso" value="<%=rsCorsi("ID_CORSO")%>">
						<input type="hidden" name="bPubblicato" id="bPubblicato" value="<%=bPubbl%>">
						<a href="javascript:<%=sNomeForm%>E.submit()" onmouseover="javascript:window.status='' ; return true"> 
							<img src="<%=Session("Progetto")%>/images/Formazione/creadoc.gif" width="20" height="20" border="0" alt="Visualizza i moduli"> 
						</a>
					</td>
					</form>
					
					<td class="tblDett">
						<%=VerificaData(ConvDateToString(rsCorsi("dt_disponibile")))%>
					</td>
					<td class="tblDett"><%=aTipo(rsCorsi("FL_EROGAZIONE"))%> 
					</td>
				</tr>
	    <%		rsCorsi.MoveNext
				Loop
		%> 
			</table>
	<%	
		End If
	End If
	rsCorsi.Close
	%> 
	<br>
	<table border="0" width="500" cellpadding="0" cellspacing="0">
		<tr align="center">
			<td> 
				<%
					PlsLinkRosso "COR_RicCorsi.asp", "Nuova ricerca"
				%>
			</td>
		</tr>
	</table>
	<br>
<%
End Sub
';););););););););););););););););););););););););););););););););););););););)
'M A I N
%>
<!--#include virtual="/strutt_testa2.asp"-->
<!--#include virtual="/include/openconn.asp"-->
<!--#include virtual="/include/HTMLEncode.asp"-->
<!--#include virtual="/include/SetPulsanti.asp"-->
<!--#include virtual="/include/ControlDateVB.asp"-->
<%	
Inizio()

ImpostaPag()
%>
<!--#include virtual ="/include/closeconn.asp"-->
<!--#include virtual="/strutt_coda2.asp"-->
