<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%
Sub Inizio()
	dim sFunzione
	dim sTitolo
	dim sCommento
	dim bCampiObbl
	
	sFunzione = "GESTIONE CORSI"
	Select case sOper 
	case "Ins"
		sCommento = "Commento all'inserimento del<br>elemento"
	case "Mod"	
		sCommento = "Commento alla modifica del<br>elemento"
	case else
		sCommento = "Commento alla cancellazione del<br>elemento"
	end select

	sTitolo = "GESTIONE CORSO"
	bCampiObbl = false
%>
	<!--#include virtual="/include/SetTestata.asp"-->
<%
End Sub
'--------------------------------------------------------------
Function Inserisci()
	dim sSQL, sSQL0, sSQL1, sSQL2, sSQL3, sErrore
	dim sTitEle, sDescEle, sTipoEle, sModErog, sObiettEle, sLinkEle, nDurata, nMasterSc
	dim nMinSc, nMaxSc, nMaxTime, nCredEle, sAuthoring, sInfoProd, dDtTmst
	dim nIdCorso
	dim rsLastElem
	
	nIdCorso = Request.Form("hIdCorso")
	sTitEle = Replace(Request.Form("txtTitoloElemento"), "'", "''")
	sDescEle = Replace(Request.Form("txtDescrElemento"), "'", "''")
	sTipoEle = Request.Form("cmbTipoElemento")
	sModErog = Request.Form("cmbFlErogazione")
	sObiettEle = Replace(Request.Form("txtObiettivoElemento"), "'", "''")
	sLinkEle = Replace(Request.Form("txtLinkElemento"), "'", "''")
	nDurata = Request.Form("txtDurataElemento")
	nMasterSc = Request.Form("txtMasterScore")
	nMinSc = Request.Form("txtMinScore")
	nMaxSc = Request.Form("txtMaxScore")
	nMaxTime = Request.Form("txtMaxTime")
	nCredEle = Request.Form("txtCreditElemento")
	sAuthoring = Replace(Request.Form("txtAuthoring"), "'", "''")
	sInfoProd = Replace(Request.Form("txtInfoProd"), "'", "''")
	sQuestionario = Request.Form("hIdQuest")
	dDtTmst = ConvDateToDB(Now())
	
	nDurata = cint(mid(nDurata,1,2) * 60) + cint(mid(nDurata,4,2))
	
	sSQL0 = "INSERT INTO ELEMENTO ("
	sSQL1 = "TITOLO_ELEMENTO, DESC_ELEMENTO, TIPO_ELEMENTO, FL_EROGAZIONE, LINK_ELEMENTO, DURATA_ELEMENTO, MASTER_SCORE, INFO_PRODUTTORE, DT_TMST, "
	sSQL2 = ") VALUES ("
	sSQL3 = "'" & sTitEle & "', '" & sDescEle & "', '" & sTipoEle & "', '" & sModErog & "', '" & sLinkEle & "', " & nDurata & ", " & nMasterSc & ", '" & sInfoProd & "', " & dDtTmst & ", "

	if sObiettEle > "" then
		sSQL1 = sSQL1 & "OBIETTIVO_ELEMENTO, "
		sSQL3 = sSQL3 & "'" & sObiettEle & "', "
	end if
	if nMinSc > "" then
		sSQL1 = sSQL1 & "MIN_SCORE, "
		sSQL3 = sSQL3 & nMinSc & ", "
	end if
	if nMaxSc > "" then
		sSQL1 = sSQL1 & "MAX_SCORE, "
		sSQL3 = sSQL3 & nMaxSc & ", "
	end if
	if nMaxTime > "" then
		nMaxTime = cint(mid(nMaxTime,1,2) * 60) + cint(mid(nMaxTime,4,2))
		sSQL1 = sSQL1 & "MAX_TIME, "
		sSQL3 = sSQL3 & nMaxTime & ", "
	end if
	if nCredEle > "" then
		sSQL1 = sSQL1 & "CREDIT_ELEMENTO, "
		sSQL3 = sSQL3 & nCredEle & ", "
	end if	
	if sAuthoring > "" then
		sSQL1 = sSQL1 & "AUTHOR_ELEMENTO, "
		sSQL3 = sSQL3 & "'" & sAuthoring & "', "
	end if	
	
	if sQuestionario > "" then
		sSQL1 = sSQL1 & "ID_QUESTIONARIO, "
		sSQL3 = sSQL3 & "'" & sQuestionario & "', "
	end if	
	
	sSQL1 = mid(sSQL1,1,len(sSQL1)-2)
	sSQL3 = mid(sSQL3,1,len(sSQL3)-2)
	sSQL = sSQL0 & sSQL1 & sSQL2 & sSQL3 & ")"

	'Response.Write sSQL
	sErrore = EseguiNoC(sSQL ,CC)
	if sErrore = "0" then
		sSQL = "SELECT ID_ELEMENTO FROM ELEMENTO WHERE DT_TMST = " & dDtTmst
		Set rsLastElem = Server.CreateObject("ADODB.Recordset")
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
		rsLastElem.Open sSQL, CC, 3
		if rsLastElem.EOF then
			sErrore = "Errore inserimento (anche se questo messaggio non lo leggeremo mai)"
		else
			sSQL = "INSERT INTO REGOLA_DIPENDENZA (ID_CORSO, ID_ELEMENTO, ID_OBIETTIVO) VALUES (" &_
			nIdCorso & ", " & rsLastElem("ID_ELEMENTO") & ",0)"
			sErrore = EseguiNoC(sSQL ,CC)
		end if
	end if
	Inserisci = sErrore
End Function
'---------------------------------------------------------------------------------------------
Function Modifica()

	dim sSQL, sSQL0, sSQL1, sSQL2, sSQL3, sErrore
	dim sTitEle, sDescEle, sTipoEle, sModErog, sObiettEle, sLinkEle, nDurata, nMasterSc, sQuestionario
	dim nMinSc, nMaxSc, nMaxTime, nCredEle, sAuthoring, sInfoProd, dDtTmst
	dim nIdCorso, nIdElemento
	dim rsLastElem
	
	nIdCorso = Request.Form("hIdCorso")
	nIdElemento = Request.Form("hIdElem")
	sTitEle = Replace(Request.Form("txtTitoloElemento"), "'", "''")
	sDescEle = Replace(Request.Form("txtDescrElemento"), "'", "''")
	sTipoEle = Request.Form("cmbTipoElemento")
	sModErog = Request.Form("cmbFlErogazione")
	sObiettEle = Replace(Request.Form("txtObiettivoElemento"), "'", "''")
	sLinkEle = Replace(Request.Form("txtLinkElemento"), "'", "''")
	nDurata = Request.Form("txtDurataElemento")
	nMasterSc = Request.Form("txtMasterScore")
	nMinSc = Request.Form("txtMinScore")
	nMaxSc = Request.Form("txtMaxScore")
	nMaxTime = Request.Form("txtMaxTime")
	nCredEle = Request.Form("radCredito")
	sAuthoring = Replace(Request.Form("txtAuthoring"), "'", "''")
	sInfoProd = Replace(Request.Form("txtInfoProd"), "'", "''")
	sQuestionario = Request.Form("hIdQuest")
	dDtTmst = Request.Form("hDtTmst")
	
	nDurata = cint(mid(nDurata,1,2) * 60) + cint(mid(nDurata,4,2))
	
	sSQL = "UPDATE ELEMENTO SET " &_
			"TITOLO_ELEMENTO = '" & sTitEle & "', " &_
			"DESC_ELEMENTO = '" & sDescEle & "', " &_
			"TIPO_ELEMENTO = '" & sTipoEle  & "', " &_
			"FL_EROGAZIONE = '" & sModErog & "', " &_
			"DURATA_ELEMENTO = " & nDurata & ", " &_
			"MASTER_SCORE = " & nMasterSc & ", "
			
	if sObiettEle > "" then
		sSQL = sSQL & "OBIETTIVO_ELEMENTO = '" & sObiettEle & "', "
	end if
	'if sLinkEle > "" then
		sSQL = sSQL & "LINK_ELEMENTO = '" & sLinkEle & "', "
	'end if
	if nMinSc > "" then
		sSQL = sSQL & "MIN_SCORE = " & nMinSc & ", "
	end if
	if nMaxSc > "" then
		sSQL = sSQL & "MAX_SCORE = " & nMaxSc  & ", "
	end if
	if nMaxTime > "" then
		nMaxTime = cint(mid(nMaxTime,1,2) * 60) + cint(mid(nMaxTime,4,2))
		sSQL = sSQL & "MAX_TIME = " & nMaxTime & ", "
	end if
	if nCredEle > "" then
		sSQL = sSQL & "CREDIT_ELEMENTO = " & nCredEle  & ", "
	end if
	if sAuthoring > "" then
		sSQL = sSQL & "AUTHOR_ELEMENTO = '" & sAuthoring & "', "
	end if
	if sInfoProd > "" then
		sSQL = sSQL & "INFO_PRODUTTORE = '" & sInfoProd & "', "
	end if
	if sQuestionario > "" then
		sSQL = sSQL & "ID_QUESTIONARIO = " & sQuestionario & ", "
	end if

	sSQL = sSQL & "DT_TMST = " & convdatetodb(Now) & " WHERE ID_ELEMENTO = " & nIdElemento
				
	'Response.Write sSQL 
	sErrore=Esegui(nIdElemento, "ELEMENTO", Session("idPersona"), "MOD", sSQL, 1, dDtTmst)
	Modifica = sErrore

End Function
'--------------------------------------------------------------------------
Function Cancella()
	Dim sErrore, dtTmst
	Dim sql
	dim nIE, nIC
	nIE = Request("IdEle")
	nIC = Request("IdCorso")
	dtTmst = Request("DtTmst")	
	dtTmst = mid(dtTmst,2)
	dtTmst = mid(dtTmst,1,(len(dtTmst)-1))
	sql="DELETE FROM ELEMENTO WHERE ID_ELEMENTO = " & nIE
	'Response.Write sql
	sErrore = Esegui(nIE,"ELEMENTO",Session("idPersona"),"CAN",sql,1,dtTmst)
	Cancella = sErrore 

End Function
'-----------------------------------------------------------------------------
'M A I N
%>
<!--#include virtual="/strutt_testa2.asp"-->
<!--#include virtual="/include/openconn.asp"-->
<!--#include virtual="/Util/DBUtil.asp"-->
<!--#include virtual="/Include/SysFunction.asp"-->
<!--#include virtual="/include/SetPulsanti.asp"-->
<%
	Dim strConn, sOper, nIDCorso1
	Dim sMessOk, sMessKo
	Dim sErr
	
	sOper = Request("Oper")
	nIDCorso1 = Request("hIDCorso")
	sErr = "mmm"
	Inizio()
	Select case sOper
		case "Ins"
			sMessOk = "Inserimento correttamente effettuato"
			sMessKo = "Errore nell'inserimento"
			sErr = Inserisci()
		case "Mod"
			sMessOk = "Modifica correttamente effettuata"
			sMessKo = "Errore nella modifica"
			sErr = Modifica()
		case "Can"
			sMessOk = "Cancellazione correttamente effettuata"
			sMessKo = "Errore nella cancellazione"
			sErr = Cancella()
		case else
%>		
		<table border=0 cellspacing=2 cellpadding=1 width="500">
			<tr align=center>
				<td class="tbltext3">
					Errore nel Passaggio dei parametri. (<%=sOper%>)
				</td>
			<tr>
			<tr align=center>
				<td class="tbltext3">
				<%
					PlsIndietro()
				%>
				</td>
			</tr>
		</table>
<%	
	End select
	if sErr <> "mmm" then
		if sErr = "0" then
%>				
	<br><br>
	<table border=0 cellspacing=2 cellpadding=1 width="500">
		<tr align=center>
			<td class="tbltext3">
				<%=sMessOk%>
			</td>
		<tr>
	</table>
	<br><br>
	<table border=0 cellspacing=2 cellpadding=1 width="500">
		<tr align=center>
				<td>
					<%
					PlsLinkRosso "COR_VisElementi.asp?IDCorso=" & nIDCorso1, "Elenco Moduli"
					%>
				</td>
		</tr>
	</table>
	<br><br>
<%		
		else
%>
	<br><br>
	<table border=0 cellspacing=2 cellpadding=1 width="500">
		<tr align=center>
			<td class="tbltext3">
				<%=sMessKo%><br><%=sErr%>
			</td>
		<tr>
	</table>		
	<br><br>
	<table border=0 cellspacing=2 cellpadding=1 width="500">
		<tr align=center>
			<td>
				<%
				PlsIndietro()
				%>
			</td>
		</tr>
		<tr align=center>
			<td>
				<%
				plsLinkRosso "COR_RicCorsi.asp", "Nuova Ricerca"
				%>
			</td>
		</tr>
	</table>
	<br><br>
<%
		end if
	end if
%>
<!--#include virtual="/include/closeconn.asp"-->
<!--#include virtual="/strutt_coda2.asp"-->
