<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%
Sub NoCache()
	Response.ExpiresAbsolute = Now() -1
	Response.AddHeader "pragma", "no-cache"
	Response.AddHeader "cache-control", "private"
	Response.CacheControl = "no-cache"
End Sub
'-----------------------------------------------------------------------
Sub ControlliJavaScript()
%>
<script language="Javascript">
<!--#include virtual="/include/ControlDate.inc"-->
<!--#include virtual="/include/ControlNum.inc"-->
<!--#include virtual="/include/ControlString.inc"-->
function OreMinuti(valore)
{
	if (valore.substr(2,1) != ":")
	{
		return false;
	}
	if (isNaN(valore.substr(0,2)))
	{
		return false;
	}
	if (isNaN(valore.substr(3,2)))
	{
		return false;
	}
	if (valore.substr(3,2) > 59)
	{
		//alert ('ma de che')
		return false;
	}
	return true;	
}

function FormInserisciElemento_Validator(frmElemento)
	{
	if (frmElemento.txtTitoloElemento.value == "")
		{
		frmElemento.txtTitoloElemento.focus();
		alert("Il campo 'Titolo Elemento' � obbligatorio.");
		return (false);
	  	}

	if (frmElemento.txtDescrElemento.value == "")
		{
		frmElemento.txtDescrElemento.focus();
		alert("Il campo 'Descrizione Elemento' � obbligatorio.");
		return (false);
	  	}
		
	if (frmElemento.cmbTipoElemento.value == "")
		{
		frmElemento.cmbTipoElemento.focus();
		alert("Il campo 'Tipo Elemento' � obbligatorio.");
		return (false);
	  	}

	if (frmElemento.cmbFlErogazione.value == "")
		{
		frmElemento.cmbFlErogazione.focus();
		alert("Il campo 'Modalit� di erogazione' � obbligatorio.");
		return (false);
	  	}
/*
	if (frmElemento.txtLinkElemento.value == "")
		{
		frmElemento.txtLinkElemento.focus();
		alert("Il campo 'Link Elemento' � obbligatorio.");
		return (false);
	  	}
*/		
	if (frmElemento.txtDurataElemento.value == "")
		{
			frmElemento.txtDurataElemento.focus();
			alert("Il campo 'Durata' � obbligatorio.");
			return (false);				
		}
	if (OreMinuti(frmElemento.txtDurataElemento.value) == false)
		{
			frmElemento.txtDurataElemento.focus();
			alert("Il campo 'Durata' deve essere in formato 'hh:mm'.");
			return (false);						
		}

	if (frmElemento.txtMasterScore.value == "")
		{
		frmElemento.txtMasterScore.focus();
		alert("Il campo 'Master Score' � obbligatorio.");
		return (false);
	  	}

	if (frmElemento.txtInfoProd.value == "")
		{
		frmElemento.txtInfoProd.focus();
		alert("Il campo 'Informazioni Produttore' � obbligatorio.");
		return (false);
	  	}
	  	
	if (isNaN(frmElemento.txtMasterScore.value))
		{
			frmElemento.txtMasterScore.focus();
			alert("Il campo 'Master Score' � numerico.");
			return (false);				
		}

	if (frmElemento.txtInfoProd.value == "")
		{
		frmElemento.txtInfoProd.focus();
		alert("Il campo 'Informazioni Produttore' � obbligatorio.");
		return (false);
	  	}
	
	if (frmElemento.txtMinScore.value != "")
		{
		if (isNaN(frmElemento.txtMinScore.value))
			{
				frmElemento.txtMinScore.focus();
				alert("Il campo 'Min Score' � numerico.");
				return (false);				
			}
		}

	if (frmElemento.txtMaxScore.value != "")
		{
		if (isNaN(frmElemento.txtMaxScore.value))
			{
				frmElemento.txtMaxScore.focus();
				alert("Il campo 'Max Score' � numerico.");
				return (false);				
			}
		}

	if (frmElemento.txtMaxTime.value != "")
		{
		if (OreMinuti(frmElemento.txtMaxTime.value) == false)
			{
				frmElemento.txtMaxTime.focus();
				alert("Il campo 'Max Time' deve essere in formato 'hh:mm'.");
				return (false);						
			}
		}

	if ((frmElemento.cmbTipoElemento.value == 'T') && (frmElemento.cmbQuestionario.value == ''))
		{
			frmElemento.cmbQuestionario.focus();
			alert("Selezionare un Questionario");
			return (false);				
		}	

	if ((frmElemento.cmbQuestionario.value != '') && (frmElemento.txtLinkElemento.value != ''))
		{
			frmElemento.cmbQuestionario.focus();
			alert("Selezionare un Questionario o il Link");
			return (false);				
		}	

	return (true);
	}

	function AttivaCombo()
	{
		if (FormInserisciElementi.cmbTipoElemento.value == 'T')
		{
			FormInserisciElementi.cmbQuestionario.disabled = false;
			document.all.VisQu.style.visibility="visible";
		}
		else
		{
			FormInserisciElementi.cmbQuestionario.disabled = true;
			document.all.VisQu.style.visibility="hidden";
			FormInserisciElementi.cmbQuestionario.value = "";
			FormInserisciElementi.hIdQuest.value = "";
		}		
	}

var fin
function VisQuest()
{
	if (fin != null ) 
	{
		fin.close(); 
	}
	f='COR_VisQuest.asp'
	finestra=window.open(f,"","width=550,height=450,location=no,menubar=no,scrollbars=yes,resizable=no")
}

var finestra
function SelLink(sImg)
{
	if (finestra != null ) 
	{
		finestra.close(); 
	}
	f='COR_SelLink.asp?img='+sImg
	finestra=window.open(f,"","width=550,height=450,location=no,menubar=no,scrollbars=yes,resizable=no")
}

</script>
<%
End Sub
'-----------------------------------------------------------------------
Sub Inizio()
	dim sFunzione
	dim sTitolo
	dim sCommento
	dim bCampiObbl
	
	sFunzione = "GESTIONE CORSI"
	sTitolo = "GESTIONE CORSO"
	sCommento = "Inserire le informazioni nei campi sottostanti e premere <B>Invia</B> per memorizzarle."
	bCampiObbl = true
	sHelp = "/Pgm/help/Formazione/Corsi/COR_InsElementi/"	
%>
	<!--#include virtual="/include/SetTestata.asp"-->
<%
End Sub
'-----------------------------------------------------------------------
Sub CreaTipoElemento()
	dim sItem
	sItem = "<option value='L'>Lezione</option>" &_
			"<option value='T'>Test</option>"
	Response.Write sItem 
End Sub
'-----------------------------------------------------------------------
Sub CreaFlErogazione()
	dim sItem
	sItem = "<option value=0>In presenza</option>" &_
			"<option value=1>A distanza</option>"
	Response.Write sItem 
End Sub
'-----------------------------------------------------------------------
Function MenoUno(nVal)
	if IsNull(nVal) then
		MenoUno=-1
	else
		MenoUno = nVal
	end if
End Function
'-----------------------------------------------------------------------
Sub CreaQuestionario()
dim rsQuest, sSQL
dim sMess
	
	Set rsQuest = Server.CreateObject("ADODB.Recordset")
	sSQL = "Select ID_QUESTIONARIO, TIT_QUESTIONARIO From Questionario order by ID_QUESTIONARIO"
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	rsQuest.Open sSQL, CC, adOpenForwardOnly
	do while not rsQuest.EOF
		sMess = "<option value=" & rsQuest("ID_QUESTIONARIO") & ">" & rsQuest("TIT_QUESTIONARIO") & "</option>"
		Response.Write sMess
		rsQuest.MoveNext
	loop
	rsQuest.Close
	set rsQuest = nothing
End Sub

'-----------------------------------------------------------------------
Sub ImpostaPag()
	dim nIdCorso
	nIdCorso = Request.QueryString("IdC")
%>
<br>
	<form METHOD="POST" onsubmit="return FormInserisciElemento_Validator(this)" name="FormInserisciElementi" action="COR_CnfElementi.asp">
	<input type="hidden" name="Oper" id="Oper" value="Ins">
	<input type="hidden" name="hIdCorso" id="hIdCorso" value="<%=nIdCorso%>">
		<table border="0" cellspacing="2" cellpadding="1" width="500">
		    <tr> 
				<td align="left" nowrap class="tbltext1">
					<b>Titolo*</b>
				</td>
				<td>
					<span class="tbltext1">&nbsp;- Utilizzabili <label name="lblNumCar10" id="lblNumCar10">100</label> caratteri -</span>
					<input type="text" name="txtTitoloElemento" class="textblacka" size="50" id="txtTitoloElemento" maxlength="100" OnKeyUp="JavaScript:CheckLenTextArea(txtTitoloElemento,lblNumCar10,100)">
				</td>
		    </tr>
		    <tr> 
				<td align="left" nowrap class="tbltext1">
					<b>Descrizione*</b>
				</td>
				<td>
					<span class="tbltext1">&nbsp;- Utilizzabili <label name="lblNumCar0" id="lblNumCar0">512</label> caratteri -</span>
					<textarea name="txtDescrElemento" id="txtDescrElemento" class="textblack" cols="50" rows="3" OnKeyUp="JavaScript:CheckLenTextArea(txtDescrElemento,lblNumCar0,512)"></textarea>
				</td>
		    </tr>
		    <tr> 
				<td align="left" nowrap class="tbltext1">
					<b>Tipo*</b>
				</td>
				<td>
					<select name="cmbTipoElemento" id="cmbTipoElemento" class="textblacka" onchange="javascript:AttivaCombo()">
						<option value selected></option>
						<%
							CreaTipoElemento()
						%>
					</select>
					&nbsp;
					<div id="VisQu" name="VisQu" style="position:absolute; visibility:hidden;">
					<input type="hidden" name="hIdQuest" id="hIdQuest" value>
					<input type="text" name="cmbQuestionario" id="cmbQuestionario" class="textblacka" readonly size="35" value>
  						<a href="javascript:VisQuest()">
   							<img src="<%=Session("progetto")%>/images/bullet1.gif" border="0" alt="Selezionare il Questionario" onmouseover="javascript:window.status='' ; return true">
   						</a>
   					</div>
					&nbsp;&nbsp;
					<!--					<select name="cmbQuestionario" id="cmbQuestionario" class="textblacka" disabled>						<option value=""></option>					<%						CreaQuestionario()					%>					</select>					-->
				</td>
		    </tr>
			<tr> 
				<td align="left" nowrap class="tbltext1">
					<b>Modalit� di<br>erogazione</b>*
				</td>
				<td>
					<select name="cmbFlErogazione" id="cmbFlErogazione" class="textblacka">
						<option value selected></option>
						<%
							CreaFlErogazione()
						%>
					</select>
				</td>
			</tr>
		    <tr> 
				<td align="left" nowrap class="tbltext1">
					<b>Obiettivo</b>
				</td>
				<td>
					<span class="tbltext1">&nbsp;- Utilizzabili <label name="lblNumCar1" id="lblNumCar1">250</label> caratteri -</span>
					<textarea name="txtObiettivoElemento" id="txtObiettivoElemento" class="textblack" cols="50" rows="3" OnKeyUp="JavaScript:CheckLenTextArea(txtObiettivoElemento,lblNumCar1,250)"></textarea>
				</td>
		    </tr>
		    <tr> 
				<td align="left" nowrap class="tbltext1">
					<b>Link*</b>
				</td>
				<td>
					<input type="text" name="txtLinkElemento" class="textblacka" size="50" id="txtLinkElemento" maxlength="100" readonly>
	   				<a href="javascript:SelLink('')">
	   					<img src="<%=Session("progetto")%>/images/bullet1.gif" border="0" alt="Selezionare il LINK" onmouseover="javascript:window.status='' ; return true">
   					</a>

				</td>
		    </tr>
		    <tr> 
				<td align="left" nowrap class="tbltext1">
					<b>Durata*</b> (hh:mm)
				</td>
				<td>
					<input type="text" name="txtDurataElemento" class="textblacka" size="5" id="txtDurataElemento" maxlength="5">
				</td>
		    </tr>
		    <tr> 
				<td align="left" nowrap class="tbltext1">
					<b>Master Score*</b>
				</td>
				<td>
					<input type="text" name="txtMasterScore" class="textblacka" size="3" id="txtMasterScore" maxlength="3">
				</td>
		    </tr>
		    <tr> 
				<td align="left" nowrap class="tbltext1">
					<b>Min Score</b>
				</td>
				<td>
					<input type="text" name="txtMinScore" class="textblacka" size="10" id="txtMinScore" maxlength="10">
				</td>
		    </tr>
		    <tr> 
				<td align="left" nowrap class="tbltext1">
					<b>Max Score</b>
				</td>
				<td>
					<input type="text" name="txtMaxScore" class="textblacka" size="10" id="txtMaxScore" maxlength="10">
				</td>
		    </tr>
		    <tr> 
				<td align="left" nowrap class="tbltext1">
					<b>Max Time</b> (hh:mm)
				</td>
				<td>
					<input type="text" name="txtMaxTime" class="textblacka" size="10" id="txtMaxTime" maxlength="10">
				</td>
		    </tr>
		    <tr> 
				<td align="left" nowrap class="tbltext1">
					<b>Credito</b>
				</td>
				<td class="tbltext1">
					Si <input type="radio" name="radCredito" id="radCredito" value="1">&nbsp;&nbsp;
					No <input type="radio" name="radCredito" id="radCredito" value="0" checked>
				</td>
		    </tr>
		    <tr> 
				<td align="left" nowrap class="tbltext1">
					<b>Authoring</b>
				</td>
				<td> 
					<span class="tbltext1">&nbsp;- Utilizzabili <label name="lblNumCar7" id="lblNumCar7">254</label> caratteri -</span>
					<textarea name="txtAuthoring" id="txtAuthoring" class="textblack" cols="50" rows="3" OnKeyUp="JavaScript:CheckLenTextArea(txtAuthoring,lblNumCar7,254)"></textarea>
				</td>
		    </tr>
		    <tr> 
				<td align="left" nowrap class="tbltext1">
					<b>Informazioni<br>Produttore*</b>
				</td>
				<td> 
					<span class="tbltext1">&nbsp;- Utilizzabili <label name="lblNumCar2" id="lblNumCar2">254</label> caratteri -</span>
					<textarea name="txtInfoProd" class="textblack" cols="50" rows="3" OnKeyUp="JavaScript:CheckLenTextArea(txtInfoProd,lblNumCar2,254)"></textarea>
				</td>
		    </tr>
		</table>
		<br>
		<table width="500" border="0" cellspacing="2" cellpadding="1">
			<tr align="center">
				<td align="right">			
					<%
						PlsIndietro()
					%>
				</td>
				<td align="left">
					<%
						PlsInvia("InserisciE")
					%>
				 </td>
			</tr>
		</table> 
	</form> 
	<br>
<%
End Sub
'------------------------------------------------------------------------
	NoCache()
%>
<!--#include virtual="/strutt_testa2.asp"-->
<!--#include virtual="/include/openconn.asp"-->
<!--#include virtual="/include/DecCod.asp"-->
<!--#include virtual="/include/SetPulsanti.asp"-->
<%	
	ControlliJavaScript()
	Inizio()
	ImpostaPag()
%>
<!--#include virtual="/include/closeconn.asp"-->
<!--#include virtual="/strutt_coda2.asp"-->
