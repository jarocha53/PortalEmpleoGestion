<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%
Sub Inizio()
	dim sFunzione
	dim sTitolo
	dim sCommento
	dim bCampiObbl
	dim sTitCorso
	
	sTitCorso = Request.QueryString("CodCorso")
	sFunzione = "GESTIONE CORSI"
	sTitolo = "GESTIONE MODULI"
	sCommento = "Elenco dei moduli del corso. Cliccando sul titolo del modulo � possibile modificarne le caratteristiche. " &_
				"E' inoltre possibile inserire un nuovo modulo usando il link <b>Nuovo Modulo</b> oppure tornare alla pagina di " &_
				"<b>Ricerca Corsi</b>"
	bCampiObbl = false
	sHelp = "/Pgm/help/Formazione/Corsi/COR_VisElementi/"		
%>
	<!--#include virtual="/include/SetTestata.asp"-->
<%
End Sub
';););););););););););););););););););););););););););););););););););););););)
Function DecTipoElemento(TipoE)
	select case TipoE
		case "L"
			DecTipoElemento = "Lezione"
		case "T"
			DecTipoElemento = "Test"
		case else
			DecTipoElemento = "***"
	end select
End Function
';););););););););););););););););););););););););););););););););););););););)
Function DecFlErogazione(FlEro)
	select case FlEro
		case 0
			DecFlErogazione = "In presenza"
		case 1
			DecFlErogazione = "A distanza"
		case else
			DecFlErogazione = "***"
	end select
End Function
';););););););););););););););););););););););););););););););););););););););)
Sub ImpostaPag()
	Dim aErog(2)
	Dim nIdCorso, sTitCorso
	Dim rsElemento, rsCorso, sSQL
	
	nIdCorso = Request("IDCorso")
	sSQL = "SELECT TIT_CORSO FROM CORSO WHERE ID_CORSO = " & nIdCorso
	
	Set rsCorso = Server.CreateObject("ADODB.Recordset")
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	rsCorso.Open sSQL, CC, 3
	if not rsCorso.EOF then
		sTitCorso = rsCorso("TIT_CORSO")
	end if
	rsCorso.Close
	%>
		<table border="0" width="500">
			<tr>
				<td class="tbltext3">
					Corso: <%=strHTMLEncode(sTitCorso)%>
				</td>
			</tr>
		</table>
		<br>
	<%
	
	aErog(0) = "In presenza"
	aErog(1) = "A distanza"
			
	sSQL = "SELECT e.id_elemento, e.titolo_elemento, e.tipo_elemento, e.fl_erogazione " &_
			"FROM elemento e, regola_dipendenza r " &_
			"WHERE e.id_elemento = r.id_elemento and " &_
			"r.id_corso = " & nIdCorso
	
	Set rsElemento = Server.CreateObject("ADODB.Recordset")
		
	'Response.Write sSQL
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	rsElemento.Open sSQL, CC, 3
		
	If rsElemento.EOF Then
	%>
		 <table border="0" width="500" cellpadding="0" cellspacing="0">
			<tr align="center">
				<td class="tbltext3"> 
					Non esistono Moduli per il corso
				</td>
			</tr>
		 </table>
	<%  
    Else
	%>
		<table border="0" width="500">
			<tr class="sfondocomm">
				<td><b>Titolo</b></td>
				<td width="80"><b>Tipo Modulo</b></td>
				<td width="80"><b>Modalit� di erogazione</b></td>
			</tr>
	 <%    
		nPrgEle = 0
		Do While Not rsElemento.EOF
			nPrgEle = nPrgEle + 1
			sNomeForm = "frmElem" & nPrgEle
	 %>		<tr class="tblsfondo">
				<form METHOD="POST" name="<%=sNomeForm%>" id="<%=sNomeForm%>" action="COR_ModElementi.asp">
				<input type=hidden name="IdElem" id="IdElem" value="<%=rsElemento("ID_ELEMENTO")%>">
				<input type=hidden name="IdCorso" id="IdCorso" value="<%=nIdCorso%>">
				<td>
					<a HREF="javascript:<%=sNomeForm%>.submit()" class="tblAgg" onmouseover="javascript:window.status='' ; return true"> 
						<%=strHTMLEncode(rsElemento("TITOLO_ELEMENTO"))%>
					</a> 
				</td>
				</form>
				<td class="tblDett">
					<%=DecTipoElemento(rsElemento("Tipo_Elemento"))%>
				</td>
				<td class="tblDett"> 
					<%=DecFlErogazione(rsElemento("Fl_Erogazione"))%>
				</td>
			</tr>
    <%		rsElemento.MoveNext
			Loop
	%> 
		</table>
	<%	
	End If
	rsElemento.Close
	set rsElemento = nothing
	%> 
	<br>
	<table border="0" width="500" cellpadding="0" cellspacing="0">
		<tr align="center">
			<td> 
				<%
					PlsLinkRosso "COR_RicCorsi.asp", "Ricerca corsi"
				%>
			</td>
<%
		if Request.Form("bPubblicato") = "False" then
%>
			<td> 
				<%
					PlsLinkRosso "COR_InsElementi.asp?IdC=" & nIdCorso, "Nuovo Modulo"
				%>
			</td>
<%
		end if
%>
		</tr>
	</table>
	<br>
<%
End Sub
';););););););););););););););););););););););););););););););););););););););)
'M A I N
%>
<!--#include virtual="/strutt_testa2.asp"-->
<!--#include virtual="/include/openconn.asp"-->
<!--#include virtual="/include/HTMLEncode.asp"-->
<!--#include virtual="/include/SetPulsanti.asp"-->
<%	
Inizio()
ImpostaPag()
%>
<!--#include virtual ="/include/closeconn.asp"-->
<!--#include virtual="/strutt_coda2.asp"-->
