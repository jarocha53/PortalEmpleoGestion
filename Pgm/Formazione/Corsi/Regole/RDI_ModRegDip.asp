<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%'Option Explicit 
   Response.ExpiresAbsolute = Now() - 1 
   Response.AddHeader "pragma","no-cache"
   Response.AddHeader "cache-control","private"
   Response.CacheControl = "no-cache"
%>
<!-- #include virtual="/strutt_testa2.asp"-->
<!--#include virtual = "/include/openconn.asp"-->
<!--#include virtual="/util/portallib.asp"-->
<%
'if ValidateService(session("idutente"),"RDI_VisRegDip", CC) <> "true" then 
'	response.redirect "/util/error_login.asp"
'end if
%>
<!-- ************** Javascript Inizio ************ -->

<script LANGUAGE="Javascript">
//include del file per fare i controlli sulla numericit� dei campi
<!--#include virtual = "/Include/ControlNum.inc"-->
<!--#include virtual = "/Include/ControlString.inc"-->
<!--#include virtual = "/Include/help.inc"-->

function Annulla() {
	frmRegDip.cmbObiettivo.value	= "0";
	frmRegDip.cmbElePrec.value		= "";
	frmRegDip.cmbEleSucc.value		= "";
	frmRegDip.txtDipendenza.value	= "";
	frmRegDip.txtPunteggio.value	= "";
	frmRegDip.txtStatoLezioni.value = "";
	frmRegDip.txtStatoObiettivi.value = "";
	
}

function replace(argvalue, x, y) { 

	while (argvalue.indexOf(x) != -1) { 
		var leading = argvalue.substring(0, argvalue.indexOf(x)); 
		var trailing = argvalue.substring(argvalue.indexOf(x) + x.length, 
		argvalue.length); 
		argvalue = leading + y + trailing; 
	} 
	return argvalue; 
} 
function VaiInizio(sDatiCorso, sDatiElemento){
	location.href = "RDI_VisRegDip.asp?cmbCorso=" + sDatiCorso + "&cmbElemento=" + sDatiElemento + "&Modo=2";
}	

//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

function InsDipendenze(apCorso,nCmbEle,fStatus){

		var pippo
		pippo =  "";
		
		sRegole = frmRegDip.txtDipendenza.value + "!" + frmRegDip.txtPunteggio.value + "!" + frmRegDip.txtStatoLezioni.value + "!" + frmRegDip.txtStatoObiettivi.value
		
		window.open('RDI_CreaRegDip.asp?Regola=' + replace(replace(sRegole,"#","-"),"&","$") + '&fStatus=' + fStatus + '&idCorso=' + apCorso + '&nEle=' + nCmbEle,'newWind','Scrollbars=yes,width=340,height=500')
	}

//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

function ControllaDati(frmRegDip, IdElemento, MinScore, MaxScore, PrimoElemento, UltimoElemento) {
//		frmRegDip.cmbElePrec			- Numerico - 
//		frmRegDip.cmbEleSucc			- Numerico - 
//		frmRegDip.cmbObiettivo			- Alfa di   -
//		frmRegDip.txtDipendenze			- Alfa di   -
//		frmRegDip.txtPunteggio			- Alfa di   -
//		frmRegDip.txtStatoLezioni		- Alfa di   -
//		frmRegDip.txtStatoObietttivi	- Alfa di   -
	
	frmRegDip.cmbObiettivo.value = TRIM(frmRegDip.cmbObiettivo.value)
	var sMess;
	if (frmRegDip.cmbEleSucc.value == ""){
		sMess=false
	}else {
		sMess=true
	}

	if ((frmRegDip.cmbEleSucc.value != "") && (frmRegDip.IdElemento.value != ""))
	{
		if (frmRegDip.cmbEleSucc.value == frmRegDip.IdElemento.value){
			alert("L'elemento successivo non pu� essere uguale all'elemento")
			frmRegDip.cmbEleSucc.focus() 
			return false
		}

		if (frmRegDip.cmbEleSucc.value == frmRegDip.cmbElePrec.value){
			alert("L'elemento successivo non pu� essere uguale all'elemento precedente")
			frmRegDip.cmbEleSucc.focus() 
			return false
		}

	}
	if (!sMess){
		if (!confirm("Non � stato indicato l'elemento \nsuccessivo, confermando quest� modulo rappresenter� l'ultimo del corso.\n Confermi?"))
			return false	
		else 
			return true	
	}else
		return true	
}

</script>
<!-- ************** Javascript Fine ************ -->

<!-- ************** ASP Inizio *************** -->

<%
'------------------------------------------------------------------------------------------------------------------------------------------------------------------------

sub Msgetto(sMessaggio)
%>
<br>
    <table width="500">
		<tr><td class="tbltext3" align="center"><b><%=sMessaggio%></b></td></tr>	
	</table>
<br>		
<%			
end sub

'------------------------------------------------------------------------------------------------------------------------------------------------------------------------

sub Inizio()
%>
<!--	<table border="0" width="520" cellspacing="0" cellpadding="0" height="81">	   <tr>	     <td width="500" background="<%'=Session("Progetto")%>/images/titoli/strumenti2b.gif" height="81" valign="bottom" align="right">	       <table border="0" width="260" height="30" cellspacing="0" cellpadding="0">	         <tr>	           <td width="100%" valign="top" align="right"><b class="tbltext1a">Struttura Corso</span></b></td>	         </tr>	       </table>	     </td>	   </tr>	</table>-->
	<br>
	<table cellpadding="0" cellspacing="0" width="500" border="0">
		<tr height="18">
			<td class="sfondomenu" height="18" width="67%">
			<span class="tbltext0"><b>STRUTTURA CORSO</b></span></td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
			<td valign="middle" align="right" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1fad"> (*) Campi Obbligatori</td>
		</tr>
		<tr width="371" class="SFONDOCOMM">
			<td colspan="3">Testo di prova.<br>Non indicando il <b>modulo successivo</b> il modulo 
			in esame rappresenter� l'ultimo del corso formativo.<a href="Javascript:Show_Help('/Pgm/help/Formazione/Corsi/Regole/RDI_ModRegDip')" onmouseover="javascript:window.status=' '; return true"><img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
			</td>
		</tr>
		<tr height="2">
			<td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>


	<br>
<%
end sub

'----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

sub ImpostaPag()

	dim i
	dim ind
	dim aElementi
	dim aObiettivi
	dim sApIdElemento
	dim sApMax
	dim sApMin
	dim sCarattere
	dim sLezionePrec
	dim sLezioneSucc
	dim sDipendenza

	' ********************************
	' Ritrovo ID l'elemento precedente.
	sSQL =	"SELECT COUNT(*) AS CONTA " & _
			"FROM REGOLA_DIPENDENZA " &_ 
			"WHERE ID_CORSO = " & sIdCorso &_
			" AND NOT ID_ELEMENTO = " & sIdElemento &_
			" AND ID_ELEMENTO = ELE_PREC" 

'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	set rsConta = CC.Execute(sSQL)
	FlagPrimoElemento = rsConta("CONTA")
	rsConta.Close
	' ********************************

	' ********************************
	' Ritrovo ID l'elemento successivo.
	sSQL =	"SELECT COUNT(*) AS CONTA " & _
			"FROM REGOLA_DIPENDENZA " &_ 
			"WHERE ID_CORSO = " & sIdCorso &_
			" AND NOT ID_ELEMENTO = " & sIdElemento &_
			" AND ID_ELEMENTO = ELE_SUCC" 

'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	set rsConta = CC.Execute(sSQL)
	FlagUltimoElemento = rsConta("CONTA")
	rsConta.Close
	' ********************************

	sSQL =	"SELECT ID_CORSO, ID_ELEMENTO, ID_OBIETTIVO, DIPENDENZE, " & _
			"PUNTEGGIO, STATO_LEZIONE, STATO_OBIETTIVO, " & _
			"ELE_PREC, ELE_SUCC " &_
			"FROM REGOLA_DIPENDENZA " &_ 
			"WHERE ID_CORSO = " & sIdCorso &_
			" AND ID_ELEMENTO = " & sIdElemento

'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	set rsDipendenza = CC.Execute(sSQL)

	aElementi = CaricaElementi(sIdCorso)
	for i = 0 to ubound(aElementi ,2)
		if I = 0 then
			sApIdElemento = aElementi(0, i)
			sApMin = aElementi(2, i)
			sApMax = aElementi(3, i)
		else 
			sApIdElemento = sApIdElemento & "|" & aElementi(0, i)
			sApMin = sApMin & "|" & aElementi(2, i)
			sApMax = sApMax & "|" & aElementi(3, i)
		end if		
	next
		
	aObiettivi = CaricaObiettivi(sIdCorso)
%>
<table width="500" border="0" cellspacing="1" cellpadding="1">
	<tr class="tbltext1fad">
		<td align="left" width="35%">
			<span class="tbltext1fad"><b>Corso</b></span>
		</td>	
		<td align="left" class="textBlackFad">
			<b><%=sDescCorso%></b>
		</td>
	</tr>
	<tr>
		<td align="left" width="35%">
			<span class="tbltext1fad"><b>Modulo </b></span>
		</td>	
		<td align="left" class="textBlackFad">
		<b><%=sDescElemento%></b>
	</tr>
</table>

<table border="0" cellpadding="1" cellspacing="1" width="500">
<form method="post" name="frmRegDip" onsubmit="return ControllaDati(this,'<%=sApIdElemento%>','<%=sApMin%>','<%=sApMax%>','<%=FlagPrimoElemento%>','<%=FlagUltimoElemento%>')" action="RDI_CnfRegDip.asp">
	<input type="hidden" value="Mod" name="Action">
	<input type="hidden" value="<%=clng(rsDipendenza("ID_ELEMENTO"))%>" name="IdElemento">
	<input type="hidden" value="<%=sDatiCorso%>" name="DatiCorso">
	<input type="hidden" value="<%=sDatiElemento%>" name="DatiElemento">		<input class="textBlackFad" type="hidden" disabled value="<%=clng(rsDipendenza("ID_ELEMENTO")) & " - " & sDescElemento%>" name="txtElemento" size="50">
		<%
		' ****************************************************
		' Asteriscato -- Da vedere se il messaggio � errato !!
		' ****************************************************
		if aObiettivi(0, 0) = "null" then
			nCheck = 0%>
<!--			</table>-->
			<%'Msgetto("Non ci sono Obiettivi")
			'exit sub
		end if
		if nCheck <> "0" then
			sSQL = "SELECT COUNT(ID_CORSO) as conta FROM OBIETTIVO WHERE ID_CORSO = " & sIdCorso		
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
			set rsConta = cc.Execute(sSQL)
			nCheck = clng(rsConta("conta"))
			if nCheck <> 0 then
			%>
			<tr>
				<td class="tbllabel" align="left" width="35%">
					<span class="tbltext1fad"><b>Obiettivo</b></span>
				</td>	
				<td align="left">
					<select style="width=265" class="textBlackFad" name="cmbObiettivo">
						<option value="0"></option>
					<%
					for i = 0 to ubound(aObiettivi, 2)
						Response.Write "<OPTION "
						if aObiettivi(0, i) = clng(rsDipendenza("ID_OBIETTIVO")) then
							Response.Write "selected "
						end if	
						Response.write "value ='" & aObiettivi(0, i) & _
						"'> " & aObiettivi(1, i) & "</OPTION>"
					next
					%>
					</select>
				</td>
			</tr>
			<%else%>
				<input type="hidden" value="0" name="cmbObiettivo">
			<%end if
		
			rsConta.close
			set rsConta = nothing
		else%>
			<input type="hidden" value="0" name="cmbObiettivo">
		<%end if
		if aElementi(0, 0) = "null" then
			Msgetto("Non esistono ulteriori elementi")
			exit sub
		end if
		%>
	</table>	
	<br>	
		
	<table cellpadding="0" cellspacing="0" width="500" border="0">
		<tr height="18">
			<td height="18" width="67%">
			<span class="tbltext1"><b>PROPEDEUTICITA'</b></span></td>
			<td width="3%">&nbsp;</td>
			<td valign="middle" align="right" width="50%" class="tbltext1fad"></td>
		</tr>
		<tr height="2">
			<td colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>		

	<table background="/images/formazione/separabordo.gif" border="0" cellpadding="1" cellspacing="1" width="500">
		<tr>
			<td width="5%">&nbsp;</td>
			<td align="LEFT" width="50%" class="tbltext1fad">
				<b>Modulo precedente</b>
			</td>
			<td align="LEFT" width="50%" class="tbltext1fad">
				<b>Modulo successivo</b>
			</td>
		</tr>
		<tr>
			<td width="5%">&nbsp;</td>
			<td align="LEFT" width="50%">
        		<select class="textBlackFad" style="width=150" name="cmbElePrec">
					<option selected></option>
					<%
					if rsDipendenza("ELE_PREC") <> "" then
						nElePrec = clng(rsDipendenza("ELE_PREC"))
					else
						nElePrec = 0
					end if
					for i = 0 to ubound(aElementi, 2)
						Response.Write "<OPTION "
						if aElementi(0, i) = nElePrec then
							Response.Write "selected "
						end if	
						Response.write "value ='" & aElementi(0, i) & _
						"'> " & aElementi(1, i) & "</OPTION>"
					next
					%>
				</select>
			</td>
			<td align="LEFT" width="50%">
        		<select class="textBlackFad" style="width=150" name="cmbEleSucc">
					<option selected></option>
					<%
				if rsDipendenza("ELE_SUCC") <> "" then
					nEleSuccc = clng(rsDipendenza("ELE_SUCC"))
				else
					nEleSuccc = 0
				end if
				for i = 0 to ubound(aElementi, 2)
					if aElementi(0, i) <> clng(sIdElemento) then
						Response.Write "<OPTION "
						if aElementi(0, i) = nEleSuccc then
							Response.Write "selected "
						end if	
						Response.write "value ='" & aElementi(0, i) & _
						"'> " & aElementi(1, i) & "</OPTION>"
					end if
				next
					%>
				</select>
			</td>				
		</tr>
	</table>
	<table background="/images/formazione/separabordo.gif" cellpadding="0" cellspacing="0" width="500" border="0">
		<tr height="2">
			<td colspan="4">&nbsp;
			</td>
		</tr>	
		<tr height="18">
			<td width="5%">&nbsp;</td>
			<td height="18" width="67%">
			<span class="tbltext1"><b>REGOLE</b></span></td>
			<td width="3%">&nbsp;</td>
			<td valign="middle" align="right" width="50%" class="tbltext1fad"></td>

		</tr>
		<tr height="2">
			<td colspan="4" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>	
	</table>	
	<table background="/images/formazione/separabordo.gif" border="0" cellpadding="1" cellspacing="1" width="500">

		<tr>
			<td width="5%">&nbsp;</td>
			<td align="left" width="35%">
				<span class="tbltext1fad"><b>Dipendenze del modulo</b></span>
			</td>	
			<td align="left">
			<input class="textBlackFad" readonly type="text" maxLength="250" name="txtDipendenza" value="<%=rsDipendenza("DIPENDENZE")%>" size="45"><a href="Javascript:InsDipendenze('<%=sIdCorso%>','<%=clng(sIdElemento)%>','<%=nCheck%>')" class="textred" onmouseover="javascript:window.status=' '; return true">&nbsp;&nbsp;<img border="0" src="<%=Session("Progetto")%>/images/bullet1.gif"></a></td>
		</tr>
		<tr>
			<td width="5%">&nbsp;</td>
			<td align="left" width="35%">
				<span class="tbltext1fad"><b>Punteggio moduli</b></span>
			</td>
			<td align="left">
			<input class="textBlackFad" readonly type="text" maxLength="250" name="txtPunteggio" value="<%=rsDipendenza("PUNTEGGIO")%>" size="50"></td>
		</tr>
		<tr>
			<td width="5%">&nbsp;</td>		
			<td align="left" width="35%">
				<span class="tbltext1fad"><b>Stato Moduli</b></span>
			</td>
			<td align="left">
			<input class="textBlackFad" readonly type="text" maxLength="250" name="txtStatoLezioni" value="<%=rsDipendenza("STATO_LEZIONE")%>" size="50"></td>
	    </tr>
		<%if nCheck > 0 then%>
			<tr>
				<td width="5%">&nbsp;</td>			
				<td align="left" width="35%">				
				<span class="tbltext1fad"><b>Stato Obiettivi</b></span>			
				</td>			
				<td align="left"> 				
				<input class="textBlackFad" readonly type="text" maxLength="250" name="txtStatoObiettivi" value="<%=rsDipendenza("STATO_OBIETTIVO")%>" size="50"></td>	    
			</tr>
		<%else%>
			<input class="textBlackFad" type="hidden" name="txtStatoObiettivi" value="#" size="50"></td>
		<%end if%>
		<tr height="2">
			<td colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>		
	</table>
	<br>
	<!--impostazione dei comandi-->
	<table cellpadding="0" cellspacing="0" width="300" border="0">	
		<tr align="center">
			<td nowrap><input type="image" name="Conferma" src="<%=Session("progetto")%>/images/conferma.gif"></td value="Registra">
			<td nowrap><a href="javascript:Annulla()"><img border="0" src="<%=Session("progetto")%>/images/annulla.gif"></td>
			<td nowrap><a href="javascript:frmIndietro.submit()"><img src="<%=session("Progetto")%>/images/indietro.gif" border="0" name="imgPunto2" onmouseover="javascript:window.status=' '; return true"></a></td>
		</tr>
	</table>
</form>
<form name="frmIndietro" method="Post" action="RDI_VisElementi.asp">
	<input type="Hidden" name="cmbCorso" value="<%=sDatiCorso%>">
	<input type="Hidden" name="cmbElemento" value="<%=sDatiElemento%>">
</form>
<%
	rsDipendenza.Close
	set rsDipendenza = nothing

end sub

'----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

sub Fine()
%>
<!--#include virtual = "/include/closeconn.asp"-->
<!--#include virtual = "strutt_coda2.asp"-->
<%
end sub

'-----------------------------------------------------------------------------------------------------------------------------------------------------------

function CaricaObiettivi(sIdCorso)
dim sSQL
dim i
dim rsObiettivo
dim aOutArray
dim nEle

	sSQL = "SELECT ID_OBIETTIVO, DESC_OBIETTIVO " & _
			"FROM OBIETTIVO WHERE ID_CORSO = " & clng(sIdCorso) 
	set rsObiettivo = Server.CreateObject("ADODB.Recordset")

'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	rsObiettivo.open sSQL, CC, 3

	if rsObiettivo.EOF then
	   rsObiettivo.Close
	   set rsObiettivo = nothing
	   redim aOutArray (0,0)
	   aOutArray(0, 0) = "null"
	   CaricaObiettivi = aOutArray
	   exit function
	end if

	nEle = rsObiettivo.RecordCount	
	i = 0
	redim aOutArray(1, nEle - 1)
	do while not rsObiettivo.EOF
		aOutArray(0, i) = clng(rsObiettivo("ID_OBIETTIVO"))
		aOutArray(1, i) = rsObiettivo("DESC_OBIETTIVO")
		
		i = i + 1
		rsObiettivo.MoveNext
	loop
	
	CaricaObiettivi = aOutArray

end function

'-----------------------------------------------------------------------------------------------------------------------------------------------------------

function CaricaElementi(sIdCorso)
dim sSQL
dim i
dim rsElemento
dim aOutArray
dim nEle

	sSQL =	"SELECT A.ID_ELEMENTO, B.TITOLO_ELEMENTO, " & _
			"B.MIN_SCORE, B.MAX_SCORE " &_
			"FROM REGOLA_DIPENDENZA A, ELEMENTO B" &_ 
			" WHERE A.ID_CORSO = " & sIdCorso &_
			" AND B.ID_ELEMENTO = A.ID_ELEMENTO"
	
	set rsElemento = Server.CreateObject("ADODB.Recordset")
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	rsElemento.open sSQL, CC, 3

	if rsElemento.EOF then
	   rsElemento.Close
	   set rsElemento = nothing
	   redim aOutArray (0,0)
	   aOutArray(0, 0) = "null"
	   CaricaElementi = aOutArray
	   exit function
	end if

	nEle = rsElemento.RecordCount	
	i = 0
	redim aOutArray(3, nEle - 1)
	do while not rsElemento.EOF
		aOutArray(0, i) = clng(rsElemento("ID_ELEMENTO"))
		aOutArray(1, i) = rsElemento("TITOLO_ELEMENTO")
		aOutArray(2, i) = rsElemento("MIN_SCORE")
		aOutArray(3, i) = rsElemento("MAX_SCORE")
		
		i = i + 1
		rsElemento.MoveNext
	loop
	
	CaricaElementi = aOutArray
end function
%>
<!-- ************** ASP Fine *************** -->

<!-- ************** MAIN Inizio ************ -->
<%
dim sSQL
dim reRegole
dim sIdCorso
dim sIdElemento
dim sDescElemento
dim sDatiCorso
dim sDatiElemento

'	sDatiCorso = Request("cmbCorso")
	sDatiCorso = Request("IdCorso")
	if TRIM(sDatiCorso) <> "" then
		sIdCorso = clng(Mid(sDatiCorso,1 ,InStr(1, sDatiCorso, "|") - 1))
		sDescCorso = Mid(sDatiCorso,InStr(1, sDatiCorso, "|") + 1)
	end if
	sDatiElemento = Request("cmbElemento")

	if TRIM(sDatiElemento) <> "" then
		sIdElemento		= clng(Mid(sDatiElemento,1 ,InStr(1, sDatiElemento, "|") - 1))
		sDescElemento	= Mid(sDatiElemento, InStr(1, sDatiElemento, "|") + 1)
	end if
			
	Inizio()
	ImpostaPag()
	Fine()
%>
<!-- ************** MAIN Fine ************ -->
