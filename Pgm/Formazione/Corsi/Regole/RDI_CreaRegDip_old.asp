<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<html>
<head>
<link REL="STYLESHEET" TYPE="text/css" HREF="<%=session("progetto")%>/fogliostile.css">
<title> Definizione regole di propedeuticitÓ
</title>
<%'Option Explicit 
Response.ExpiresAbsolute = Now() - 1 
Response.AddHeader "pragma","no-cache"
Response.AddHeader "cache-control","private"
Response.CacheControl = "no-cache"
%>
<!--#include virtual = "/include/openconn.asp"-->
<!--#include virtual="/util/portallib.asp"-->
<%
'if ValidateService(session("idutente"),"RDI_VisRegDip", CC) <> "true" then 
'	response.redirect "/util/error_login.asp"
'end if
%>

<!-- ************** Javascript inizio ************ -->

<script LANGUAGE="Javascript">
//include del file per fare i controlli sulla numericitÓ dei campi
<!--#include virtual = "/Include/ControlNum.inc"-->
<!--#include virtual = "/Include/ControlString.inc"-->

function Info_regola(sElem,sPunt,sSTMod,sSTObb,sMinScore,sMaxScore)
{

	var aMinScore;
	var aMaxScore; 
    var sRange;
    var z,p,i,n,p;
    var sPunteggi;
    var sAppo,aAppo;
	var aPunteggi;
	var text;
	var sModuli

	text = "";
    sPunteggi = "";
	aMinScore = sMinScore.split(",")
	aMaxScore = sMaxScore.split(",")
	
//	frmCreaRegole.txtOut.value = sElem 
    
	// Identificativo delle lezioni inserite nella regola.
	sModuli = sElem
	sModuli = replace(sModuli,"|","&")
	aModuli = sModuli.split("&");
	
	// Crea tre Array che conterranno le info sul corso :
	// ID, Minimo punteggio, massimo punteggio. 
	aIdCorso  = new Array(aModuli.length)

	aScore = sPunt.split(",")
	aSTMod = sSTMod.split(",")
	sSTObb = sSTObb.split(",")

	for(t=0;t<aModuli.length;t++) {
		z = z + 1;
		if (aScore[0]=="#") aScore[0] = 0;
		if (aScore[1]=="#") aScore[1] = 0;

		if ((aScore[0] == 0) || (aScore[1] == 0))
			text = text + "<Table width='300' border='0' cellspacing='2' cellpadding='2'><TR><TD align=center width='45%' class='tblText1Fad'><B>" + aModuli[t] + "</B> : (Nessuno) <Span class=textBlack></span><input value='#' type=hidden readonly style='TEXT-TRANSFORM: uppercase;' class='textBlackFad' name='txtPP' size='2'></TD>";
		else
			text = text + "<Table width='300' border='0' cellspacing='2' cellpadding='2'><TR><TD align=center width='45%' class='tblText1Fad'><B>" + aModuli[t] + "</B> : <input maxlength='" + aMinScore[t].length + "' style='TEXT-TRANSFORM: uppercase;' value='" + aScore[t] + "' class='textBlackFad' name='txtPP' size='2'>(" + aMinScore[t] + "/" + aMaxScore[t] + ")</TD>";
		if (frmCreaRegole.fStatus.value != "0"){
				
			text = text + "<TD align=center width='15%'><select class='textBlackFad' name='cmbLStatus'><Option value='#'>#</option><Option value='P'>P</option><Option value='C'>C</option><Option value='F'>F</option><Option value='I'>I</option><Option value='B'>B</option><Option value='N'>N</option></select></TD>";
			text = text + "<TD align=left width='40%'><select class='textBlackFad' name='cmbOStatus'><Option value='#'>#</option><Option value='P'>P</option><Option value='C'>C</option><Option value='F'>F</option><Option value='I'>I</option><Option value='B'>B</option><Option value='N'>N</option></select></TD>";
			text = text + "<input type=hidden name='txtMaxScore' value='" + aMaxScore[t] + "'><input type=hidden value='" + aMinScore[t] + "' name='txtMinScore'>";

		} else {

			text = text + "<TD align=center width='15%' ><select class='textBlackFad' name='cmbLStatus'><Option value='#'>#</option><Option value='P'>P</option><Option value='C'>C</option><Option value='F'>F</option><Option value='I'>I</option><Option value='B'>B</option><Option value='N'>N</option></select></TD>";
			text = text + "<TD align=center width='40%'><input type=hidden name='txtMaxScore' value='" + aMaxScore[t] + "'><input type=hidden value='" + aMinScore[t] + "' name='txtMinScore'></TD>";
		}
			
	}

	text = text + "</TR></TABLE>"
	lblCampi.innerHTML = text


	if ( parseInt(aModuli.length,10) == 1 ) {
		document.frmCreaRegole.cmbLStatus.value = aSTMod[0];
		if (frmCreaRegole.fStatus.value != "0")
			document.frmCreaRegole.cmbOStatus.value = sSTObb[0];
	}else {

		for (t=0;t<aModuli.length;t++) {
			document.frmCreaRegole.cmbLStatus[t].value = aSTMod[t];
			if (frmCreaRegole.fStatus.value != "0")
				document.frmCreaRegole.cmbOStatus[t].value = sSTObb[t];
			
		}
	
	} 
	
	 
	lblLegenda1.innerHTML = "<b>#</b> = <Span class=textBlack>nessun valore</Span><BR>"  
	lblLegenda1.innerHTML = lblLegenda1.innerHTML + "<b>P</b> = <Span class=textBlack>passed</Span><BR>"  
	lblLegenda2.innerHTML = "<b>C</b> = <Span class=textBlack>completed</Span><BR>"  
	lblLegenda1.innerHTML = lblLegenda1.innerHTML + "<b>F</b> = <Span class=textBlack>Failed</Span><BR>"  
	lblLegenda2.innerHTML = lblLegenda2.innerHTML + "<b>I</b> = <Span class=textBlack>Incompleted</Span><BR>"  
	lblLegenda1.innerHTML = lblLegenda1.innerHTML + "<b>B</b> = <Span class=textBlack>Browsed</Span><BR>"  
	lblLegenda2.innerHTML = lblLegenda2.innerHTML + "<b>N</b> = <Span class=textBlack>Not attempted</Span><BR>"  
	


}


//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function numCharacter(argvalue, x) { 
	var i;
	i = 0 ;
	while (argvalue.indexOf(x) != -1) { 
		var leading = argvalue.substring(0, argvalue.indexOf(x)); 
		var trailing = argvalue.substring(argvalue.indexOf(x) + x.length, 
		argvalue.length); 
		argvalue = leading + "" + trailing; 
		i = i + 1;
	} 
	return i; 
}
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Questa funzione fa la replece per ogni carattere contenuto 
// nella stringa e non nel primo che trova.
function replace(argvalue, x, y) { 

	while (argvalue.indexOf(x) != -1) { 
		var leading = argvalue.substring(0, argvalue.indexOf(x)); 
		var trailing = argvalue.substring(argvalue.indexOf(x) + x.length, 
		argvalue.length); 
		argvalue = leading + y + trailing; 
	} 
	return argvalue; 
} 
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

function Cerca(re, s)
{
	var s1,re;
		
	s1 = re;
	re = re.replace(s,"")
	if (re != s1)
		return true
	else
		return false
}

function Convalida_Ok()
{
	var sAppo;
	var nParAp,nParCh;
	var sPar;

	frmCreaRegole.cmbEle.value = ""; 

	lblCampi.innerHTML = ""; 

	nParAp = parseInt(numCharacter(frmCreaRegole.txtGruppo.value,"("),10);
	nParCh = parseInt(numCharacter(frmCreaRegole.txtGruppo.value,")"),10);
	// *********************************************
	// Controllo della formalitÓ utilizzo parentesi.
	if (nParAp != nParCh) {
		alert("Verificare la correttezza dell'utilizzo\n delle parentesi nella formula!")
			return false;
	}

	sAppo = frmCreaRegole.txtGruppo.value;
	sPar = false
	
	if ( Cerca(sAppo,"()") ) {
		alert("Verificare la correttezza dell'utilizzo\n delle parentesi nella formula!");
		return false ;
	}
		
	if ( Cerca(sAppo,")(") ) {
		alert("Verificare la correttezza dell'utilizzo\n delle parentesi nella formula!");
		return false ;
	}

	// *********************************************
	// Pulisco la stringa togliendo le parentesi per
	// il controllo sulla formalitÓ della regola. 
	sAppo = replace(sAppo, "(", "");
	sAppo = replace(sAppo, ")", "");
	sPar = false;

	if ((!IsNum(sAppo.charAt(0))) || (!IsNum(sAppo.charAt(sAppo.length-1)))) {
		alert("Verificare la formalitÓ nella formula!");
		return false;
	}
	for(i=0;i<sAppo.length;i++)	{
		if ((sAppo.charAt(i) == "&") || (sAppo.charAt(i) == "|"))
		{
			if (sPar == false)
				sPar=true;
			else {
				alert("Verificare la correttezza dell'utilizzo\n degli operatori nella formula!")
				return false
			}
						
		}else
			sPar=false;
	}
	Costruisci_regola();
	frmCreaRegole.txtOut.value = frmCreaRegole.txtGruppo.value;
	frmCreaRegole.txtGruppo.value = "";
	
	frmCreaRegole.Can.disabled		= true;
	frmCreaRegole.txtOut.disabled   = true;
	frmCreaRegole.txtGruppo.disabled = true;
	frmCreaRegole.cmbEle.disabled	= true; 
    frmCreaRegole.Apri.disabled		= true;
    frmCreaRegole.Chiudi.disabled	= true;
    frmCreaRegole.And.disabled		= true;
    frmCreaRegole.Or.disabled		= true;
    frmCreaRegole.Ok.disabled		= true;	
}
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function Aggiungi_Elemento()
{
	frmCreaRegole.txtGruppo.value = frmCreaRegole.txtGruppo.value + frmCreaRegole.cmbEle.value; 
	frmCreaRegole.cmbEle.value = ""; 
	frmCreaRegole.cmbEle.disabled = true; 
}
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

function ApriParentesi(){
	frmCreaRegole.txtGruppo.value = frmCreaRegole.txtGruppo.value + "(";
	frmCreaRegole.cmbEle.disabled = false; 

}
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

function Aggiungi_Or(){
	frmCreaRegole.txtGruppo.value = frmCreaRegole.txtGruppo.value + "|"
	frmCreaRegole.cmbEle.disabled = false; 
}
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

function Aggiungi_And(){
	frmCreaRegole.txtGruppo.value = frmCreaRegole.txtGruppo.value + "&"
	frmCreaRegole.cmbEle.disabled = false; 
}
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

function Togli_Car(){
	var sAppo;
	var sVal;
	
	sVal="";
	
	sAppo = frmCreaRegole.txtGruppo.value;
	for (i=0;i<sAppo.length-1;i++)
	{
		sVal = sVal + sAppo.charAt(i) ;
	}
	frmCreaRegole.txtGruppo.value = sVal;
	frmCreaRegole.cmbEle.disabled = false; 
}
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

function ChiudiParentesi(){
	frmCreaRegole.txtGruppo.value = frmCreaRegole.txtGruppo.value + ")"
	frmCreaRegole.cmbEle.disabled = false; 
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function Pulisci_finestra() {
	location.reload();
}
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function Uscita() {
	this.close();
}
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

function Costruisci_regola()
{

	var aMinScore;
	var aMaxScore; 
    var sRange;
    var z,p,i,n,p;
    var sPunteggi;
    var sAppo,aAppo;
	var aPunteggi;
	var text;
	text = "";
    sPunteggi = "";
    
	// Dal campo nascosto mi prendo gli ID dei corsi 
	// ed il range dei loro punteggi.
	sRange = frmCreaRegole.txtP.value; 
	aRange = sRange.split(",")

	// Numero delle lezioni inserite nella regola.
	n = frmCreaRegole.txtNCorsi.value 
	z = parseInt(n,10);

	// Identificativo delle lezioni inserite nella regola.
	sModuli = frmCreaRegole.txtGruppo.value
	sModuli = replace(sModuli, "|", "&") ;
	sModuli = replace(sModuli, "(", "") ;
	sModuli = replace(sModuli, ")", "") ;
	aModuli = sModuli.split("&");

	// Crea tre Array che conterranno le info sul corso :
	// ID, Minimo punteggio, massimo punteggio. 
	aIdCorso  = new Array(aModuli.length)
	aMinScore = new Array(aModuli.length)
	aMaxScore = new Array(aModuli.length)
	aPunteggi = new Array((aRange.length * 2))

	t = 0;
	for (i=0;i<aRange.length;i++) {
		aAppo = aRange[i].split("(");
		aAppo[1] = aAppo[1].replace(")","");
		aPunteggi[t] = aAppo[0];
		t = t + 1;
		aPunteggi[t] = aAppo[1];
		t = t + 1;
	}

	for(t=0;t<aModuli.length;t++) {
		for(i=0;i<aPunteggi.length;i++){
			if (aModuli[t] == aPunteggi[i] ) {
				z = z + 1;
				aScore = aPunteggi[i+1].split(":"); 	
				if (aScore[0]=="-1") aScore[0] = 0;
				if (aScore[1]=="-1") aScore[1] = 0;

				if ((aScore[0] == 0) || (aScore[1] == 0))
					text = text + "<Table width='300' border='0' cellspacing='2' cellpadding='2'><TR><TD align=center width='45%' class='tblText1Fad'><B>" + aModuli[t] + "</B> : (Nessuno) <Span class=textBlack></span><input value='#' type=hidden readonly style='TEXT-TRANSFORM: uppercase;' class='textBlackFad' name='txtPP' size='2'></TD>";
				else
					text = text + "<Table width='300' border='0' cellspacing='2' cellpadding='2'><TR><TD align=center width='45%' class='tblText1Fad'><B>" + aModuli[t] + "</B> : <input maxlength='" + aScore[1].length + "' style='TEXT-TRANSFORM: uppercase;' class='textBlackFad' name='txtPP' size='2'>(" + aScore[0] + "/" + aScore[1] + ")</TD>";

				if (frmCreaRegole.fStatus.value != "0"){
				
				text = text + "<TD align=center width='15%'><select class='textBlackFad' name='cmbLStatus'><Option value='#'>#</option><Option value='P'>P</option><Option value='C'>C</option><Option value='F'>F</option><Option value='I'>I</option><Option value='B'>B</option><Option value='N'>N</option></select></TD>";
				text = text + "<TD align=center width='40%'><select class='textBlackFad' name='cmbOStatus'><Option value='#'>#</option><Option value='P'>P</option><Option value='C'>C</option><Option value='F'>F</option><Option value='I'>I</option><Option value='B'>B</option><Option value='N'>N</option></select></TD>";
				text = text + "<input type=hidden name='txtMaxScore' value='" + aScore[1] + "'><input type=hidden value='" + aScore[0] + "' name='txtMinScore'>";

				} else {
					text = text + "<TD align=center width='15%' ><select class='textBlackFad' name='cmbLStatus'><Option value='#'>#</option><Option value='P'>P</option><Option value='C'>C</option><Option value='F'>F</option><Option value='I'>I</option><Option value='B'>B</option><Option value='N'>N</option></select></TD>";
					text = text + "<TD align=center width='40%'><input type=hidden name='txtMaxScore' value='" + aScore[1] + "'><input type=hidden value='" + aScore[0] + "' name='txtMinScore'></TD>";
				}
				
			}
		}
	}
	//lblCampi.innerHTML = lblCampi.innerHTML + "</TR></TABLE>"
	text = text + "</TR></TABLE>"
	lblCampi.innerHTML = text
	if (parseInt(z,10) != parseInt(aModuli.length,10))
	{
		lblCampi.innerHTML = "";
		alert("Regola non valida");
		location.reload();
		return false;
	}
	lblLegenda1.innerHTML = "<b>#</b> = <Span class=textBlack>nessun valore</Span><BR>"  
	lblLegenda1.innerHTML = lblLegenda1.innerHTML + "<b>P</b> = <Span class=textBlack>passed</Span><BR>"  
	lblLegenda2.innerHTML = "<b>C</b> = <Span class=textBlack>completed</Span><BR>"  
	lblLegenda1.innerHTML = lblLegenda1.innerHTML + "<b>F</b> = <Span class=textBlack>Failed</Span><BR>"  
	lblLegenda2.innerHTML = lblLegenda2.innerHTML + "<b>I</b> = <Span class=textBlack>Incompleted</Span><BR>"  
	lblLegenda1.innerHTML = lblLegenda1.innerHTML + "<b>B</b> = <Span class=textBlack>Browsed</Span><BR>"  
	lblLegenda2.innerHTML = lblLegenda2.innerHTML + "<b>N</b> = <Span class=textBlack>Not attempted</Span><BR>"  
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function cmbEle_cambia() {

	var aValori;
	var nConta;
	var i,n;
	var delimitatore;
    
    frmCreaRegole.Apri.disabled = false;
    frmCreaRegole.Chiudi.disabled = false;
 //   frmCreaRegole.Ragg.disabled = false;
    frmCreaRegole.And.disabled = false;
    frmCreaRegole.Or.disabled = false;
    frmCreaRegole.Ok.disabled = false;
    
    delimitatore = "&";
    if (frmCreaRegole.cmbEle.value != "") {
        if (TRIM(frmCreaRegole.txtGruppo.value) == "") { 
            frmCreaRegole.txtGruppo.value = frmCreaRegole.cmbEle.value;
       }else{
            aValori = frmCreaRegole.txtGruppo.value;
            aValori = aValori.split(delimitatore);            
            nConta = 0;
            for (i = 0; i <= aValori.length; i++) {
                if (aValori[i] == frmCreaRegole.cmbEle.value) { 
                    nConta = nConta + 1
                }
            }
            if (nConta > 0) {
                alert ("Modulo giÓ presente");
           }else{
                frmCreaRegole.txtGruppo.value = frmCreaRegole.txtGruppo.value + "&" + frmCreaRegole.cmbEle.value;
				// **********************
				n = frmCreaRegole.txtNCorsi.value
				i = parseInt(n,10) 
				i = i + 1
				frmCreaRegole.txtNCorsi.value = i 
            }
        }
    }
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function Bottone_Conferma () {

	// opener.document.frmRegDip.txtDipendenza.value = frmCreaRegole.txtOut.value;
	// opener.document.frmRegDip.txtPunteggio.value = frmCreaRegole.txtPP.value;

	var sPunteggio,sLStatus,sOStatus,sAppo,i;
	sAppo="";
	sLStatus = "";
	sOStatus = "";
	sPunteggio = "";

	if (  frmCreaRegole.txtGruppo.value == "" && frmCreaRegole.txtOut.value == "" ) {
		alert("Indicare almeno un modulo per la creazione della regola");
		return false;
	}
	
	if (!isNaN(frmCreaRegole.txtPP.length)) {
		for(i=0;i<frmCreaRegole.txtPP.length;i++) {
			if (frmCreaRegole.txtPP[i].value == "") {
				alert("Valore obbligatorio!");
				frmCreaRegole.txtPP[i].focus();
				return false
				
			}
			if ((!IsNum(frmCreaRegole.txtPP[i].value)) && (frmCreaRegole.txtPP[i].type != "hidden") ) {
				alert("Indicare un valore numerico!");
				frmCreaRegole.txtPP[i].focus();
				return false
			}
			sAppo = frmCreaRegole.txtPP[i].value;
			if ((parseInt(sAppo,10) < parseInt(frmCreaRegole.txtMinScore[i].value,10)) || (parseInt(sAppo,10) > parseInt(frmCreaRegole.txtMaxScore[i].value,10))) {
				alert("Il punteggio deve essere compreso fra " + frmCreaRegole.txtMinScore[i].value + " e " + frmCreaRegole.txtMaxScore[i].value);
				frmCreaRegole.txtPP[i].focus();
				return false;
			}			
			sPunteggio = sPunteggio + frmCreaRegole.txtPP[i].value + ",";
			sLStatus = sLStatus + frmCreaRegole.cmbLStatus[i].value + ",";
			
			if (frmCreaRegole.fStatus.value != "0") 
				sOStatus = sOStatus + frmCreaRegole.cmbOStatus[i].value + ",";
		}
		sPunteggio = sPunteggio.slice(0,sPunteggio.length-1);
		sLStatus = sLStatus.slice(0,sLStatus.length-1);
		if (frmCreaRegole.fStatus.value != "0") 
			sOStatus = sOStatus.slice(0,sOStatus.length-1);

	} else {
		if (frmCreaRegole.txtPP.value == "") {
			alert("Valore obbligatorio!");
			frmCreaRegole.txtPP.focus();
			return false
		}
		if ((!IsNum(frmCreaRegole.txtPP.value)) && (frmCreaRegole.txtPP.type != "hidden")) {
			alert("Indicare un valore numerico!");
			frmCreaRegole.txtPP.focus();
			return false
		}
	
		sPunteggio	= frmCreaRegole.txtPP.value;
		
		if ((parseInt(sPunteggio,10) < parseInt(frmCreaRegole.txtMinScore.value,10)) || (parseInt(sPunteggio,10) > parseInt(frmCreaRegole.txtMaxScore.value,10))) {
			alert("Il punteggio deve essere compreso fra " + frmCreaRegole.txtMinScore.value + " e " +frmCreaRegole.txtMaxScore.value);
			return false;
		}
		
		sLStatus	= frmCreaRegole.cmbLStatus.value;
		if (frmCreaRegole.fStatus.value != "0") 
			sOStatus	= frmCreaRegole.cmbOStatus.value;
	}
	opener.document.frmRegDip.txtDipendenza.value = frmCreaRegole.txtOut.value;
	opener.document.frmRegDip.txtPunteggio.value = sPunteggio;	
	opener.document.frmRegDip.txtStatoLezioni.value = sLStatus;	

	if (frmCreaRegole.fStatus.value != "0") 
		opener.document.frmRegDip.txtStatoObiettivi.value = sOStatus;	

	Uscita()

}

</script>

<!-- ******************  Javascript Fine *********** -->

<!-- ********************* ASP inizio *************** -->

<!--#include virtual = "/include/DecCod.asp"-->
<!--#include virtual = "/include/SysFunction.asp"-->
<%

'----------------------------------------------------------------------------------------------------------------------------------------------------  
sub Msgetto(sMessaggio)
%>
<br>
    <table width="500">
		<tr><td class="tbltext3" align="center"><b><%=sMessaggio%></b></td></tr>	
	</table>
<br>		
<%			
end sub

'------------------------------------------------------------------------------------------------------------------------------------------------------------------------

function CaricaElementi(sIdCorso)
	dim sSQL
	dim i
	dim rsElemento
	dim aOutArray
	dim nEle

	sSQL =	"SELECT A.ID_ELEMENTO, B.TITOLO_ELEMENTO, " & _
			"B.MIN_SCORE, B.MAX_SCORE " &_ 
			"FROM REGOLA_DIPENDENZA A, ELEMENTO B " &_ 
			"WHERE A.ID_CORSO = " & sIdCorso & _
			" AND B.ID_ELEMENTO = A.ID_ELEMENTO"

	set rsElemento = Server.CreateObject("ADODB.Recordset")
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	rsElemento.open sSQL, CC, 3

	if rsElemento.EOF then 
	   rsElemento.Close
	   set rsElemento = nothing
	   CaricaElementi(0, 0) = "null"
	   exit function
	end if
	
	nEle = rsElemento.RecordCount	
	
	i = 0
	redim aOutArray(3, nEle - 1)
	do while not rsElemento.EOF
		aOutArray(0, i) = clng(rsElemento("ID_ELEMENTO"))
		aOutArray(1, i) = rsElemento("TITOLO_ELEMENTO")
		aOutArray(2, i) = rsElemento("MIN_SCORE")
		aOutArray(3, i) = rsElemento("MAX_SCORE")
		
		i = i + 1
		rsElemento.MoveNext
	loop
	
	CaricaElementi = aOutArray
end function

'----------------------------------------------------------------------------------------------------------------------------------------------------  

%>    
<!-- ************** ASP fine   *************** -->

</head>
<body>

<form name="frmCreaRegole">
<input type="hidden" name="txtNCorsi" value="0">
<table width="300" border="0" cellspacing="2" cellpadding="1">
	<tr> 
		<td colspan="2" align="middle" class="tbltext3"><b>Definizione regole di propedeuticitÓ</b></td>
	</tr>
</table>
<br>
<input type="hidden" name="txtPrec">
<%

dim aElementi
dim i
dim sIdCorso
dim txtPrec

nIdElem		= clng(Request("nEle"))
sIdCorso	= clng(Request("idCorso"))


Response.Write "<BR>" & sDipReg
	
fStatus = Request("fStatus") ' Flag per l'obbiettivo.
	
aElementi = CaricaElementi(sIdCorso)
if aElementi(0, 0) = "null" then
	Msgetto("Non ci sono Elementi")
end if

for i = 0 to ubound(aElementi, 2)
	sPunteggi = sPunteggi & aElementi(0,i) & "(" & aElementi(2,i) & ":" & aElementi(3,i) & "),"
next
sPunteggi = left(sPunteggi,len(sPunteggi)-1)
%>

<input type="hidden" value="<%=sPunteggi%>" disabled maxLength="250" name="txtP" size="21">
<table width="300" border="0" cellspacing="2" cellpadding="1">
	<tr>
		<td align="left" width="150">
			<span class="tblText1Fad"><b>Modulo</b></span>
		</td>	
		<td align="left">
	   		<select class="textBlackFad" style="width=150" name="cmbEle" onchange="JAVASCRIPT:Aggiungi_Elemento()">
				<option selected></option>

				<%
				for i = 0 to ubound(aElementi, 2)
					Response.Write "<BR> " & nIdElem & " = " & aElementi(0, i)
					if clng(aElementi(0, i)) <> clng(nIdElem)  then
						Response.Write "<OPTION "
						Response.write "value ='" & aElementi(0, i) & _
						"'> " & aElementi(1, i) & "</OPTION>"
					end if
				next
				%>
			</select>
		</td>
	</tr>
	<tr>
		<td align="left" width="150">
			<span class="tblText1Fad"><b>Moduli Selezionati</b></span>
		</td>
		<td align="left">
			<input class="textBlackFad" type="text" disabled maxLength="250" name="txtGruppo" size="21">
		</td>
	</tr>

</table>
<br>
<div align="center">
	<input type="button" name="Apri" value="(" onclick="JAVASCRIPT:ApriParentesi()">
	<input type="button" name="Chiudi" value=")" onclick="JAVASCRIPT:ChiudiParentesi()">
	<input type="button" name="And" value="And" onclick="JAVASCRIPT:Aggiungi_And()">
	<input type="button" name="Or" value="Or" onclick="JAVASCRIPT:Aggiungi_Or()">
	<input type="button" name="Can" value="Can" onclick="JAVASCRIPT:Togli_Car()">
	<input type="button" name="Ok" value=" = " onclick="JAVASCRIPT:Convalida_Ok()">
</div>
<br>
<table width="300" border="0" cellspacing="2" cellpadding="1">
		<tr height="2">
			<td background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>
<br>
<table width="300" border="0" cellspacing="2" cellpadding="2">
	<tr>
		<td align="left" width="45%">
			<span class="tblText1Fad"><b>Regola Dipendenza</b></span>
		</td>
		<td align="left">
			<input class="textBlackFad" type="text" disabled maxLength="250" name="txtOut" size="28">
		</td>
	</tr>
</table>
<!--<b>Punteggio (Range)<br>Stato Modulo <br> <%'if fStatus > 0 then Response.Write "Stato Obbiettivi" %></b>-->
<table width="300" border="0" cellspacing="2" cellpadding="2">
	<input name="fStatus" value="<%=fStatus%>" type="hidden">
	<tr>
		<td class="tblText1Fad" align="left" width="45%">
			<b>Punteggio </b>
		</td>
		<td class="tblText1Fad" align="left" width="15%">
			<b>Stato Modulo <label id="lblPun" class="tblText1Fad"></label></b>
		</td>
		<td class="tblText1Fad" align="left" width="40%">
			<b><%if fStatus > 0 then Response.Write "Stato<BR>Obbiettivi" %></b>
		</td>
	</tr>
</table>	
<div id="lblCampi" class="tblText1Fad">

</div>			
	
<table width="300" border="0" cellspacing="2" cellpadding="2">
	<tr>
		<td class="tblText1Fad" align="left" width="45%">
		<label id="lblLegenda1"></label>
		</td>
		<td class="tblText1Fad" align="left">
		<label id="lblLegenda2"></label>
		</td>
	</tr>
	<tr>
		<td colspan="2">	
			&nbsp;
		</td>
	</tr>
	<tr> 
		<td colspan="2" align="center">	
			<input type="image" name="Conferma" src="<%=Session("Progetto")%>/images/conferma.gif" value="Conferma" onclick="return Bottone_Conferma()">
			<a href="javascript:Pulisci_finestra()"><img src="<%=Session("progetto")%>/images/Annulla.gif" border="0"></a>
			<a href="JAVASCRIPT:Uscita()" class="textred"><img border="0" src="<%=Session("progetto")%>/images/chiudi.gif"></a>
		</td>
	</tr>
</table>

</body>
</html>
<%if Request("Regola") <> "!!!" then

	sDipReg = Replace(Replace(Request("Regola"),"-","#"),"$","&")
	aAppo	= Split(sDipReg,"!")
	sCodRegola = aAppo(0)
	aAppo(0) = Replace (Replace(aAppo(0),"(",""),")","")
	aModuli	= Split(Replace(aAppo(0),"|","&"),"&")
	
	for i = 0 to UBound(aModuli)
		if InStr(sPunteggi,aModuli(i) & "(") then
			nPos = InStr(sPunteggi,aModuli(i) & "(")
			sAppo = mid(sPunteggi,nPos)
			sAppo = replace(sAppo,aModuli(i) & "(","(")
			aScore = split(sAppo,":")
			aScore(0) = replace(aScore(0),"(","")
			nPPos = InStr(aScore(1),")")
			aScore(1) = mid(aScore(1),1,nPPos-1)
			sRangeMin = sRangeMin & aScore(0) & ","
			sRangeMax = sRangeMax & aScore(1) & ","
		end if

	next 
	sRangeMin = Left(sRangeMin,len(sRangeMin)-1)
	sRangeMax = Left(sRangeMax,len(sRangeMax)-1)
%>
	<script language="Javascript">
		frmCreaRegole.txtOut.value = '<%=sCodRegola%>' 

		Info_regola('<%=aAppo(0)%>','<%=aAppo(1)%>','<%=aAppo(2)%>','<%=aAppo(3)%>','<%=sRangeMin%>','<%=sRangeMax%>')
	</script>
<%end if%>
</form>
