<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%'Option Explicit 
   Response.ExpiresAbsolute = Now() - 1 
   Response.AddHeader "pragma","no-cache"
   Response.AddHeader "cache-control","private"
   Response.CacheControl = "no-cache"
%>

<!--#include virtual="/strutt_testa2.asp"-->
<!--#include virtual="/include/openconn.asp"-->
<!--#include virtual="/util/portallib.asp"-->
<%
'if ValidateService(session("idutente"),"RDI_VisRegDip", CC) <> "true" then 
'	response.redirect "/util/error_login.asp"
'end if
%>

<!-- ************** Javascript inizio ************ -->

<script language="JAVAScript">

	function VaiInizio(sDatiCorso, sDatiElemento){
	location.href = "RDI_VisRegDip.asp?cmbCorso=" + sDatiCorso + "&cmbElemento=" + sDatiElemento + "&Modo=2";
}	

</script>

<!-- ************** Javascript Fine ************ -->

<!-- ************** ASP inizio *************** -->
<!--#include virtual = "/util/dbutil.asp"-->
<!--#include virtual = "/include/ControlDateVB.asp"-->
<!--#include virtual = "/include/SysFunction.asp"-->
<%Sub Msgetto(Msg)%>
	<br>
	<table align="center">
		<tr>
			<td class="tbltext3"><b><%=Msg%></b></td>
		</tr>	
	</table>
<%End Sub

'-------------------------------------------------------------------------------------------------------------------------------	

sub Inizio()
%>
	<br>
	<table cellpadding="0" cellspacing="0" width="500" border="0">
		<tr height="18">
			<td class="sfondomenu" height="18" width="67%">
			<span class="tbltext0"><b>&nbsp;Gestione Regole di Dipendenza</b></span></td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
			<td valign="middle" align="right" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1"></td>
		</tr>
		<tr width="371" class="SFONDOCOMM">
			<td colspan="3">Testo di prova<a href="Javascript:Show_Help('/Pgm/help/Formazione/FAD/Corsi/Regole/RDI_VisRegDip')" onmouseover="javascript:window.status=' '; return true"><img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
			</td>
		</tr>
		<tr height="2">
			<td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>
	<br>
<%end sub

'------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

sub GetParam()

	sAction			= Request("Action")
	sDatiCorso = Request("DatiCorso")
	if TRIM(sDatiCorso) <> "" then
		sIdCorso = Mid(sDatiCorso,1 ,InStr(1, sDatiCorso, "|") - 1)
	end if
	sDatiElemento = Request("DatiElemento")
	if TRIM(sDatiElemento) <> "" then
		sIdElemento		= Mid(sDatiElemento,1 ,InStr(1, sDatiElemento, "|") - 1)
	end if
	sIdObiettivo	= Request("cmbObiettivo")
	sidElePrec		= Request("cmbElePrec")
	sidEleSucc		= Request("cmbEleSucc")	
	sDipendenze		= Request("txtDipendenza")
	sPunteggio		= Request("txtPunteggio")
	sStatoLezione	= Request("txtStatoLezioni")
	sStatoObiettivo	= Request("txtStatoObiettivi")
	
	if sIdObiettivo = "" then
		sIdObiettivo = "0"
	end if
	if sidElePrec = "" then
		sidElePrec = "0"
	end if
	if sidEleSucc = "" then
		sidEleSucc = "99999"
	end if

end sub

'------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

sub Modifica()

	sSQL = "UPDATE REGOLA_DIPENDENZA SET " &_
			"ID_OBIETTIVO =	"	& sIdObiettivo    & "," &_
			"DIPENDENZE = '"		& sDipendenze     & "'," &_
			"PUNTEGGIO = '"		& sPunteggio      & "'," &_
			"STATO_LEZIONE = '"	& sStatoLezione   & "'," &_
			"STATO_OBIETTIVO = '"	& sStatoObiettivo & "'," &_
			"ELE_PREC = "	& sIdElePrec &_
			", ELE_SUCC = "	& sIdEleSucc &_
			" WHERE ID_ELEMENTO = " & sIdElemento  & " AND" &_
			" ID_CORSO = " & sIdCorso 
	Errore=Esegui("ID_REGOLA=" & sIdRegola ,"REGOLE",Session("idutente"),"MOD", sSQL, 1,sDtTmst)
	if Errore <>  "0" then
		Msgetto("Errore Durante la modifica della regola di dipendenza." & Errore)
	else
		MsgettoOk() 
	end if 

end sub

'------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

sub MsgettoOk()
%>
	<form name="frmIndietro" method="Post" action="RDI_VisElementi.asp">
		<input type="Hidden" name="cmbCorso" value="<%=sDatiCorso%>">
		<input type="Hidden" name="cmbElemento" value="<%=sDatiElemento%>">
	</form>
	<script language="JavaScript">
		alert('Modifica regola di dipendenza correttamente effettuata.')
		frmIndietro.submit()
	</script>
<%
end sub

'------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

sub Fine()
%>
	<br>
	<br>
	<table border="0" cellpadding="0" cellspacing="1" width="500">
		<form name="frmIndietro" method="Post" action="RDI_ModRegDip.asp">
			<input type="Hidden" name="IdCorso" value="<%=sDatiCorso%>">
			<input type="Hidden" name="cmbElemento" value="<%=sDatiElemento%>">
		</form>
		<tr>
			<td align="center">
				<a href="javascript:frmIndietro.submit()">
					<img src="<%=Session("Progetto")%>/images/indietro.gif" border="0" name="imgPunto2" onmouseover="javascript:window.status=' '; return true">
				</a>
			</td>
		</tr>
	</table>
	<br>
<!--#include virtual = "/include/closeconn.asp"-->
<!--#include virtual = "strutt_coda2.asp"-->
<%end sub%>
<!-- ************** ASP Fine *************** -->

<!-- ************** MAIN Inizio ************ -->
<%

dim sSQL

dim	sAction
dim sDatiCorso
dim sDatiElemento
dim	sIdCorso
dim	sIdElemento
dim	sIdObiettivo
dim	sDipendenze
dim	sPunteggio
dim	sStatoLezione
dim	sStatoObiettivo
dim sIdElePrec
dim sIdEleSucc

Inizio()
'reperisco i dati inseriti nella form precedente
GetParam()
Modifica()
Fine()
	
%>
<!-- ************** MAIN Fine ************ -->
