<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include virtual="/strutt_testa2.asp"-->
<!--#include virtual="/Include/settestatafad.asp"-->
<!--#include virtual="/Include/OpenConn.asp"-->
<!--#include virtual="/Include/Htmlencode.asp"-->
<!--#include virtual="/Include/FruizioneModuli.asp"-->

<script language="javascript">
<!--#include Virtual = "/Include/help.inc"-->
function manda(scorsi,stitcorso,sidele,PagIndietro)
{
   document.txtcorsi.titoli.value = scorsi;
   document.txtcorsi.hidcor.value = stitcorso;
   document.txtcorsi.element.value = sidele;
   document.txtcorsi.hpag.value = PagIndietro;
   document.txtcorsi.submit()
} 
function InviaIndietro()
{
		frmPaginaIndietro.submit();
} 

function funAssociazione(ID,Q){

	document.frmQuest.IDQue.value=ID;
	document.frmQuest.QModo.value=Q;
	document.frmQuest.submit();
}

function VisMateriali(IDCorso,Tipo){
	document.frmMatCorso.txtID_corso.value=IDCorso
	document.frmMatCorso.txtTipo.value=Tipo
	document.frmMatCorso.submit();

}
</script> 
<link REL="STYLESHEET" TYPE="text/css" HREF="<%=session("progetto")%>/fogliostile.css">
<%
'-----------------parametro per sapere la pagina per tornare indietro------------
PagIndietro = Request.Form ("hpag")
'--------------------------------------------------------------------------------
scorsi = Request.Form ("titoli")

sSqlcorso = "SELECT id_AreaTematica, Tit_Corso, desc_Corso, Obiettivo_Corso, NumCredit_Corso, Durata_Corso, Info_Produttore, PreReq_Corso, Destinatari_Corso, fl_erogazione, ver_corsoaicc " &_
			"FROM Corso WHERE id_Corso = " & scorsi 
			
'PL-SQL * T-SQL  
SSQLCORSO = TransformPLSQLToTSQL (SSQLCORSO) 
set rscorso = CC.execute(sSqlcorso)
	if not rscorso.eof then
			sareatematica =	rscorso.fields("id_AreaTematica")
			stitcorso = strhtmlencode(rscorso.fields("Tit_Corso"))
			sdesccorso = rscorso.fields("desc_Corso")
			sobiettivocorso = rscorso.fields("Obiettivo_Corso")
			snumcredit = rscorso.fields("NumCredit_Corso")
			sdurata = rscorso.fields("Durata_Corso")
			sproduttore = rscorso.fields("Info_Produttore")
			sprereq = rscorso.fields("PreReq_Corso") 
			sdestinatari = rscorso.fields("Destinatari_Corso")
			serogazione = rscorso.fields("fl_erogazione")
			sVerCorsoAICC = rscorso.fields("ver_corsoaicc")
sSqlDesc= "SELECT DescrizioneArea FROM Area WHERE id_Area =" & sareatematica 	
'PL-SQL * T-SQL  
SSQLDESC = TransformPLSQLToTSQL (SSQLDESC) 
set rsdesc= CC.execute(sSqlDesc)

sTitolo = "Scheda Corso"
sComm = "La Scheda Corso contiene tutte le informazioni relative al corso da te selezionato. " &_
		"Ciascun corso � suddiviso in moduli (lezioni) e, se previsti, Test di Valutazione  " &_
		"che vedrai elencati nella parte inferiore della pagina chiamata 'Struttura del Corso'. " &_ 
		"Puoi visualizzare la scheda descrittiva dei singoli moduli o test che compongono il " &_
		"corso facendo click sull'Icona corrispondente. Per iniziare il corso fai " &_
		"click su &quot;Inizia il corso&quot;."

sHlp = "/Pgm/help/formazione/Catalogo/corsi/COR_VisSchedacorso"
call TestataFAD(sTitolo, sComm, false, sHlp)
%>
<form name="txtcorsi" action="/pgm/formazione/catalogo/corsi/cor_visschedamoduli.asp" method="post">
	<input type="hidden" name="titoli">
	<input type="hidden" name="hidcor">
	<input type="hidden" name="element">
	<input type="hidden" name="hpag">
</form>
<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<tr>
		<td width="450" class="tbltext3" align="center">
			<%=stitcorso%>
		</td>
		<%
		dim sqlAsso
		dim recAsso
		dim IDQuest
		set recAsso=Server.CreateObject("ADODB.Recordset")
		
		'controllo che ci sia un'associazione tra il CORSO e il QUESTIONARIO
		sqlAsso="SELECT id_questionario FROM provevalutazione WHERE id_corso=" & scorsi
'PL-SQL * T-SQL  
SQLASSO = TransformPLSQLToTSQL (SQLASSO) 
		recAsso.Open sqlAsso,CC,3
		
		If not recAsso.eof then
			Associazione="si"
			IDQuest=recAsso("ID_QUESTIONARIO")
		else
			Associazione="no"
		end if
		
		if Associazione="si" then%>
		<td width="50" align="left">
			<a href="javascript:funAssociazione(<%=IDQuest%>,0)" class="textred" onmouseover="javascript:status='' ; return true">
			<img src="<%=session("progetto")%>/images/formazione/questr.gif" border="0" alt="Prove di Autovalutazione" WIDTH="27" HEIGHT="26">
			</a>
		</td>
		<%
		end if		
		recAsso.Close
		set recAsso = nothing
		%>
	</tr>
<br>		
</table>
<br>
<table border="0" width="100%" cellpadding="0" cellspacing="1">	
	<tr>
		<td width="40%"><b class="tbltext">DESCRIZIONE</b>
		</td>
			<td width="60%" class="tbltext"><%=sdesccorso%>
		</td>
	</tr>	
	<tr>
		<td width="40%"><b class="tbltext">AREA TEMATICA</b>
		</td>
		<td width="60%" class="tbltext"><%=rsdesc("DescrizioneArea")%>
		</td>
	</tr>
	<tr>
		<td width="40%"><b class="tbltext">OBIETTIVO CORSO</b>
		</td>
		<td width="60%" class="tbltext"><%=sobiettivocorso%>
		</td>
	</tr>
	<tr>
		<td width="40%"><b class="tbltext">DURATA CORSO</b>
		</td>
<%If sdurata <> "" Then%>
		<td width="60%" class="tbltext"><%=sdurata%>&nbsp;&nbsp;ore
		</td>
<%Else%>
		<td width="60%" class="tbltext">
		</td>
<%End If%>		
	</tr>
	<tr>
		<td width="40%"><b class="tbltext">PREREQUISITI</b>
		</td>
		<td width="60%" class="tbltext"><%=sprereq%>
		</td>
	</tr>
	<tr>
		<td width="40%"><b class="tbltext">DESTINATARI</b>
		</td>
		<td width="60%" class="tbltext"><%=sdestinatari%>
		</td>
	</tr>
	<tr>
		<td width="40%"><b class="tbltext">ORGANIZZAZIONE</b>
		</td>
		<% Select Case serogazione
		Case "0"
		serogazione= "In Presenza"
		Case "1"
		serogazione= "A Distanza"
		Case "2"
		serogazione= "Integrata"
		End Select
		%>
		<td width="60%" class="tbltext"><%=serogazione%>
		</td>
	</tr>
</table>
<br>
<table border="0" width="100%" cellpadding="0" cellspacing="1" width="100%">	
	<tr>
		<td width="25%" align="left">
			<a class="textred" href="<%=LinkIniziaCorso()%>" onmouseover="javascript:status='' ; return true">
				<% Response.Write "<b>INIZIA IL CORSO</b>" %>
			</a>
		</td>
		<td width="40%" align="left">
			&nbsp;&nbsp;
		</td>		
		<form name="frmQuest" action="/pgm/formazione/Questionari/QUE_Questionario.asp" method="post">
			<input type="hidden" id="IDQue" name="IDQue">
			<input type="hidden" id="QModo" name="QModo">
		</form>
			<td width="35%" align="middle">
				<%'controllo che questo corso contenga del MATERIALE!
				dim recMat
				dim sqlMat
				sqlMat="SELECT desc_materialedid FROM materialedidattico WHERE id_corso=" & scorsi & ""
		
				set recMat=Server.CreateObject("ADODB.Recordset")
'PL-SQL * T-SQL  
SQLMAT = TransformPLSQLToTSQL (SQLMAT) 
				recMat.Open sqlMat,CC,3
				if recMat.EOF then%>
					&nbsp;&nbsp;
				<%else%>
					
					<a class="textred" href="javascript:VisMateriali(<%=scorsi%>,'MAT_CORSO')" onmouseover="javascript:status='' ; return true">
						<b>MATERIALI DEL CORSO</b>
					</a>
					
				<%
				end if
				%>
		  </td>
			<form name="frmMatCorso" action="COR_VisMaterialeCorMod.asp" method="post">
				<input type="hidden" id="txtID_corso" name="txtID_corso">
				<input type="hidden" id="txtTipo" name="txtTipo">
			</form>
	</tr>	
	</table>	
<%	rscorso.close
Else%>
<table>
	<tr class="sfondocomm">
		<td>Non sono presenti corsi
		</td>
	</tr>
</table>
<% 
	set rscorso = nothing
End If

sSql = "SELECT E.id_Elemento,R.ele_prec, R.ele_succ,E.tipo_elemento,E.Titolo_Elemento  " &_
       "FROM Elemento E, Corso C, Regola_Dipendenza R " &_
       "WHERE C.id_corso = " & scorsi  & " AND C.id_Corso = R.id_Corso " &_
       "AND R.ID_Elemento = E.id_Elemento AND r.ele_prec IS NOT NULL ORDER BY r.ele_prec"
		 
set rs = Server.CreateObject("ADODB.Recordset")
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
rs.open sSQL, CC, 3

%>
<table border="0" width="100%" cellpadding="0" cellspacing="1">
		
	<tr>
		<td width="100%" align="center" class="sfondocomm"><b class="tbltext">STRUTTURA DEL CORSO</b>
		</td>
	</tr>
</table>
<table border="0" width="100%" cellpadding="0" cellspacing="1">	
<%if not rs.eof then		

	redim aAppo((rs.RecordCount)-1,3) 
	i = 0
	do while not rs.eof
		aAppo(i,0) = clng(rs("ELE_PREC"))
		aAppo(i,1) = clng(rs("ELE_SUCC"))
		aAppo(i,2) = clng(rs("id_Elemento"))
		rs.MoveNext
		i = i + 1
	loop
	sOrdinamento = ordinaModuli(aAppo,UBound(aAppo,1)+1)
	RS.MoveFirst 
	aOrdinato = rs.GetRows 
	if InStr(sOrdinamento,"#") = 0 then 
		aOrdinato = Sort(aOrdinato,sOrdinamento)
	end if
	bTrovato = false
	for i = 0 to UBound(aOrdinato,2)
'	do while not rs.eof 
		stitele= aOrdinato(4,i)
		sidele = aOrdinato(0,i)
		if bTrovato = false then
			' Se la funzione LanciaPrimo � false ...
			' .. il corso � terminato perch� non ha trovato 
			' nessun elemento da terminare.
			if LanciaPrimo(scorsi, sidele) then
				bTrovato = true
			end if
		end if
		
		select case aOrdinato(3,i)
	 		
			case "L"
%>
				<tr class="tblsfondo">
				<td width="60%" class="tbltext"><!--br--><b><%=stitele%></b>
				</td>
				<td width="40%" align="center" class="tblsfondo"><a onmouseover="javascript:window.status='' ; return true" href="javascript:manda('<%=scorsi%>','<%=stitcorso%>','<%=sidele%>','<%=PagIndietro%>')">
				<img src="<%=session("progetto")%>/images/formazione/modulo.gif" width="24" height="26" border="0" alt="Visualizza scheda modulo"></a>					
				</td>
<%			case "T" %>
				<tr class="tblsfondo">
				<td width="60%" class="tbltext"><!--br--><b><%=stitele%></b>
				</td>
				<td width="40%" align="center" class="tblsfondo"><a onmouseover="javascript:window.status='' ; return true" href="javascript:manda('<%=scorsi%>','<%=stitcorso%>','<%=sidele%>','<%=PagIndietro%>')">
				<img src="<%=session("progetto")%>/images/formazione/note05.gif" border="0" alt="Visualizza scheda Test" WIDTH="27" HEIGHT="26"></a>					
				</td>

				<form name="frmCorso" method="POST" action="<%=sPathQuest%>">
				<input type="hidden" name="idque" value="<%=nIdQuestionario%>">
				<!--input type="hidden" name="QModo" value="1"><input type="hidden" name="Elem" value="<%'=CodElem%>"><input type="hidden" name="IDCorso" value="<%'=nIdCorso%>"><input type="hidden" name="dtTmst" value="<%'=dtTmst%>"><input type="hidden" name="IdArgom" value="<%'=nIdArgom%>"><input type="hidden" name="CodCorso" value="<%'=sCodCorso%>"-->
				</form>
<%  
		end select			
	next
	if bTrovato = false then%>
		<script language="javascript">
		function IniziaCorso()
		{
			alert('Corso Completato');
		}
		</script>
		<%
	end if
	rs.close
%>
</table>	
<table>
<%	
	Else
		bTrovato = LanciaPrimo(-1,-1)	'Condizione impossibile: serve per visualizzare la dicitura "Impossibile avviare il corso"
%>
	<table>
		<tr>
			<td class="tbltextFad"><br>Non esistono moduli per questo corso</td>
		</tr>						
<%	End If
	Set rs = nothing
%>
 	
<br>
	<tr>
		<td align="center" width="100%">
<%
Select Case PagIndietro
Case "1"  
%>
	<form name="frmPaginaIndietro" method="post" action="/pgm/formazione/catalogo/CAT_VisCatalogo.asp">
		<a href="javascript:InviaIndietro()" onmouseover="javascript:status='' ; return true"><br>		
		<img border="0" src="<%=Session("Progetto")%>/images/indietro.gif"></a>
		<!--input type="hidden" name="titoli" value="<%'=scorsi%>"-->
	</form>
<%			
Case "2"%>
	<form name="frmPaginaIndietro" method="post" action="/pgm/formazione/catalogo/CAT_VisCatalogo.asp">
		<a href="javascript:InviaIndietro()" onmouseover="javascript:status='' ; return true"><br>	
		<img border="0" src="<%=Session("Progetto")%>/images/indietro.gif"></a>
		<!--input type="hidden" name="titoli" value="<%'=scorsi%>"-->
	</form>
<%	
Case "3"%>
	<form name="frmPaginaIndietro" method="post" action="/pgm/formazione/catalogo/CAT_VisFormativo.asp">
		<a href="javascript:InviaIndietro()" onmouseover="javascript:status='' ; return true"><br>	
		<img border="0" src="<%=Session("Progetto")%>/images/indietro.gif"></a>
		<!--input type="hidden" name="titoli" value="<%'=scorsi%>"-->
	</form>	
		</td>
	</tr>
<%
End Select
%>	
</table>	
<!--#include virtual="/Include/CloseConn.asp"-->
<!--#include virtual="/strutt_coda2.asp"-->
<%
Function LinkIniziaCorso()
	LinkIniziaCorso = "javascript:IniziaCorso()"
End Function
'***************************************************************************
Function LanciaPrimo(nIdCorso, nIdElem)
	dim sSQL, rsPrimo

	sSQL = " SELECT E.CREDIT_ELEMENTO, E.MASTER_SCORE, E.MAX_TIME, C.ID_CORSOAICC, C.ID_CORSO, "
	sSQL = sSQL &  " E.ID_ELEMENTOAICC, E.ID_ELEMENTO, C.INFO_PRODUTTORE, VER_CORSOAICC, "
	sSQL = sSQL &  " E.DESC_ELEMENTO, E.DURATA_ELEMENTO, E.TIPO_ELEMENTO, E.LINK_ELEMENTO, "
	sSQL = sSQL &  " E.ID_QUESTIONARIO, E.TITOLO_ELEMENTO, E.OBIETTIVO_ELEMENTO, E.FL_EROGAZIONE"
	sSQL = sSQL &  " FROM ELEMENTO E, CORSO C, REGOLA_DIPENDENZA R"
	sSQL = sSQL &  " WHERE  C.ID_CORSO = R.ID_CORSO AND R.ID_ELEMENTO = E.ID_ELEMENTO "
	sSQL = sSQL &  " AND E.ID_ELEMENTO=" & nIdElem & " AND C.ID_CORSO =" & nIdCorso 

'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	set rsPrimo = cc.execute(sSQL)
	if rsPrimo.eof then
		%>
		<script language="javascript">
		function IniziaCorso()
		{
			alert('Impossibile avviare il corso');
		}
		</script>
		<%
		LanciaPrimo = false
	else 
		sLinkEle = rsPrimo("LINK_ELEMENTO")
		' Controllo se il corso � AICC.
		if trim(IsNull(rsPrimo("VER_CORSOAICC"))) then ' Non � AICC ...
			nVerAICC = ""
			sPathQuest = slinkele 
			LanciaPrimo = True
		%>
			<script language="javascript">
			function IniziaCorso()
			{
				var corso = window.open('COR_VisCorso.asp?CredEle=<%=rsPrimo("CREDIT_ELEMENTO")%>&MasScore=<%=rsPrimo("MASTER_SCORE")%>&MaxTime=<%=rsPrimo("MAX_TIME")%>&InfoPro=<%=rsPrimo("INFO_PRODUTTORE")%>&IdCAicc=<%=rsPrimo("ID_CORSOAICC")%>&IdEAICC=<%=rsPrimo("ID_ELEMENTOAICC")%>&AICC=<%=nVerAICC%>&idque=<%=rsPrimo("ID_QUESTIONARIO")%>&idele=<%=nIdElem%>&idcorso=<%=nIdCorso%>&Per=<%=sPathQuest%>','Info','width=800,height=600,Resize=No,Scrollbars=yes,screenX=w,screenY=h')
			}
			</script>
		<%			
		else ' ..... � AICC.
			if ControllaSeFinito(rsPrimo("ID_CORSOAICC"), rsPrimo("ID_ELEMENTOAICC")) then
				LanciaPrimo = False
			else
				LanciaPrimo = True
				sPag = "http://" & Request.ServerVariables("HTTP_HOST") & "/pgm/formazione/AICC/girasole.asp"
				sPag = Server.URLEncode(sPag)
				nVerAICC = rsPrimo("VER_CORSOAICC")
				sParamElem = session("idutente") & "|" & rsPrimo("ID_CORSOAICC") & "|" & rsPrimo("ID_ELEMENTOAICC")
				sPathQuest = slinkele & "?aicc_sid=" & sParamElem & "&aicc_url=" & sPag
				sPathQuest = Replace(sPathQuest, "&", "�")
		%>
				<script language="javascript">
				function IniziaCorso()
				{
					var corso = window.open('COR_Aggiorna.asp?CredEle=<%=rsPrimo("CREDIT_ELEMENTO")%>&MasScore=<%=rsPrimo("MASTER_SCORE")%>&MaxTime=<%=rsPrimo("MAX_TIME")%>&InfoPro=<%=rsPrimo("INFO_PRODUTTORE")%>&IdCAicc=<%=rsPrimo("ID_CORSOAICC")%>&IdEAICC=<%=rsPrimo("ID_ELEMENTOAICC")%>&AICC=<%=nVerAICC%>&idque=<%=rsPrimo("ID_QUESTIONARIO")%>&idele=<%=nIdElem%>&idcorso=<%=nIdCorso%>&Per=<%=sPathQuest%>','Info','width=800,height=600,Resize=No,Scrollbars=yes,screenX=w,screenY=h')
				}
				</script>
		<%			
			end if
		end if
	end if
	rsPrimo.close
	set rsPrimo = nothing
End Function
'-------------------------------------------------------------------------
Function ControllaSeFinito(nIdCorso, nIdElem)
	dim rsCSF
	sSQL = "SELECT lesson_status FROM core WHERE lesson_id = '" &_
		   nIdElem & "' AND course_id = '" & nIdCorso & "' AND student_id='" & Session("IdUtente") & "'"
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	set rsCSF = cc.execute (sSQL)
	if rsCSF.eof then	'Non � possibile, ma...  have you ever seen?...e gli passo true
		'ControllaSeFinito = true
		'modifica del 17/09/2003 permette l'inizio corso da Inizia Corso
		ControllaSeFinito = false
	else
		if mid(ucase(rsCSF("LESSON_STATUS")),1,1) = "C" or mid(ucase(rsCSF("LESSON_STATUS")),1,1) = "P" then
			ControllaSeFinito = true
		else
			ControllaSeFinito = false
		end if
	end if
	rsCSF.close
	set rsCSF = nothing
End Function

%>
