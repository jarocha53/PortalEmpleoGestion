<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include virtual="/strutt_testa2.asp"-->
<!--#include virtual="/pgm/formazione/menu/menu.asp"-->
<!--#include virtual="/Include/OpenConn.asp"-->
<!--#include virtual="/Include/Htmlencode.asp"-->
<!--#include virtual="/Include/SetTestataFAD.asp"-->
<script language="javascript">
<!--#include virtual="include/Help.inc"-->

	function Consulta(Pagina){
		windowReport = window.open (Pagina,"","toolbar=0;location=0,directories=0,status=0,menubar=1,scrollbars=1,resizable=1,copyhistory=0,width=450,height=340,screenX=200,screenY200");
	}
	function Scarica(nfile) {   
		url = "/Pgm/Formazione/Mediateca/MED_Download.asp?nfile=" + nfile
		var DochSCR=window.open(url,"Doc1","toolbar=no,width=350,left=1,top=1,height=250,directories=no,location=no,status=no,statusbar=no,resizable=no,menubar=no,scrollbars=no");
	   if(!DochSCR.opener) DochSCR.opener=self;
	   if(DochSCR.focus!=null) DochSCR.focus();
	}

</script>
<%
dim Flag
dim Percorso
dim sTipo
dim sIDCorso
dim sIDElem

sIDElem=Request.Form("txtID_ele")
sIDCorso=Request.Form("txtID_corso")

sTipo=Request.Form("txtTipo")

if sTipo="MAT_CORSO" then
	sTitolo="Materiale del Corso"
	sCommento="Di seguito troverai l'elenco dei materiali disponibili per approfondire le tematiche " &_
			  "del corso scelto. Potrai consultare il documento proposto cliccando sull'icona a forma " &_
			  "di libro aperto oppure potrai scaricarlo direttamente sul tuo computer cliccando sull'icona " &_
			  "corrispondente nella casella 'Preleva'.<br> Troverai ulteriori approfondimenti visitando " &_
			  "l'indirizo web eventualmente presente."
	sHelp="/Pgm/help/formazione/Catalogo/corsi/COR_VisSchedacorso"
else
	sTitolo="Materiale del Modulo"
	sCommento="Questa pagina ti consente di visualizzare l'elenco di tutta la documentazione disponibile " &_
			  "relativa al modulo selezionato. Potrai consultarla direttamente (icona Consulta) o scaricarla " &_
			  "sul tuo computer (icona Preleva).<br> Troverai ulteriori approfondimenti visitando l'indirizzo " &_
			  "web eventualmente presente."
	sHelp="/Pgm/help/formazione/Catalogo/corsi/COR_VisSchedamoduli"
end if
call TestataFAD(sTitolo, sCommento, false, sHelp)
%>

<table Width="500" cellspacing="1" cellpadding="1">
	<tr class="sfondocommfad">
		<td Width="140" nowrap>
			<b>Materiale Didattico</b>
		</td>
		<td Width="20" nowrap>
			<b>Consulta</b>
		</td>
		<td Width="20" nowrap>
			<b>Preleva</b>
		</td>
		<td Width="20" nowrap>
			<b>Link</b>
		</td>
	</tr>
<%
if sTipo="MAT_CORSO" then
	'*****ININZIO strutturazione MATERIALE CORSO ***************
		dim sSQLC
		dim recMatC
		sSQLC="select DESC_MATERIALEDID,ID_MATERIALEDID,PATH_MATERIALEDID," &_
			  " LINK_MATERIALEDID,FL_ACTMATERIALEDID from" &_
			  " MATERIALEDIDATTICO where ID_CORSO=" & sIDCorso & ""
		
		set recMatC=Server.CreateObject("ADODB.Recordset")	
'PL-SQL * T-SQL  
SSQLC = TransformPLSQLToTSQL (SSQLC) 
		recMatC.Open sSQLC,CC,3
		Do until recMatC.EOF
%>			
		<tr align="center" height="8" class="tblsfondofad">
			<td class="tblAggfad" align="center" width="200">
				<b><%=recMatC("DESC_MATERIALEDID")%></b>
			</td>
		
			<td class="tblAggfad">
				<center>
				<%if recMatC("LINK_MATERIALEDID")<>"" then%>
					<td>&nbsp;</td>
					<td width="200" class="tblAggfad">
						<a href="http://<%=recMatC("LINK_MATERIALEDID")%>">
							<%=recMatC("LINK_MATERIALEDID")%>
						</a>	
					</td>
				<%else
					
					Flag=recMatC("FL_ACTMATERIALEDID")
					Percorso=recMatC("PATH_MATERIALEDID")
					select case cint(Flag)
						case 1 'Consulta%>
							<a href="javascript:Consulta('<%=Percorso%>')" onmouseover="javascript:window.status='' ; return true">
								<img src="<%=Session ("Progetto")%>/images/formazione/icons/consulta.gif" border="0" WIDTH="15" HEIGHT="13">
							</a>
							<td>&nbsp;</td>
							<td width="200">&nbsp;&nbsp;</td>

					  <%case 2 'Preleva%>
								&nbsp;
							<td>
								<a href="javascript:Scarica('<%=Percorso%>')">
								  <img src="<%=Session ("Progetto")%>/images/formazione/icons/download.gif" border="0">
							    </a>
							</td>
							<td width="200">&nbsp;&nbsp;</td>

					  <%case 4 'Consulta / Preleva%>
							
								<a href="javascript:Consulta('<%=Percorso%>')">
								  <img src="<%=Session ("Progetto")%>/images/formazione/icons/consulta.gif" border="0" WIDTH="15" HEIGHT="13">
							    </a>
							
							<td>
								<a href="javascript:Scarica('<%=Percorso%>')">
								  <img src="<%=Session ("Progetto")%>/images/formazione/icons/download.gif" border="0">
							    </a>
							</td>
							<td width="200">&nbsp;&nbsp;</td>
				  <%end select
					%>
					
				
				<%end if%>
			 	</center>
			</td>
		</tr>
		
		
	<%
		recMatC.MoveNext
		loop
	%>
	


<%
else
	'******ININZIO strutturazione MATERIALE MODULO ***************


		dim sSQLM
		dim recMatM
		sSQLM="select DESC_MATERIALEDID,ID_MATERIALEDID,PATH_MATERIALEDID," &_
			  " LINK_MATERIALEDID,FL_ACTMATERIALEDID from" &_
			  " MATERIALEDIDATTICO where ID_ELEMENTO=" & sIDElem & ""
		
		set recMatM=Server.CreateObject("ADODB.Recordset")	
'PL-SQL * T-SQL  
SSQLM = TransformPLSQLToTSQL (SSQLM) 
		recMatM.Open sSQLM,CC,3
		Do until recMatM.EOF
%>			
		<tr align="center" height="8" class="tblsfondofad">
			<td class="tblAggfad" align="center" width="200">
					<b><%=recMatM("DESC_MATERIALEDID")%></b>
			</td>
		
			<td class="tblAggfad">
				<center>
				<%if recMatM("LINK_MATERIALEDID")<>"" then%>
					<td>&nbsp;</td>
					<td width="200" class="tblAggfad">
						<a href="http://<%=recMatM("LINK_MATERIALEDID")%>" target="new">
							<b><%=recMatM("LINK_MATERIALEDID")%></b>
						</a>	
					</td>
				<%else
					
					Flag=recMatM("FL_ACTMATERIALEDID")
					Percorso=recMatM("PATH_MATERIALEDID")
					select case cint(Flag)
						case 1 'Consulta%>
							<a href="javascript:Consulta('<%=Percorso%>')" onmouseover="javascript:window.status='' ; return true">
								<img src="<%=Session ("Progetto")%>/images/formazione/icons/consulta.gif" border="0" WIDTH="15" HEIGHT="13">
							</a>
							<td>&nbsp;</td>
							<td width="200">&nbsp;&nbsp;</td>

					  <%case 2 'Preleva%>
								&nbsp;
							<td>
								<a href="javascript:Scarica('<%=Percorso%>')">
								  <img src="<%=Session ("Progetto")%>/images/formazione/icons/download.gif" border="0">
							    </a>
							</td>
							<td width="200">&nbsp;&nbsp;</td>

					  <%case 4 'Consulta / Preleva%>
							
								<a href="javascript:Consulta('<%=Percorso%>')">
								  <img src="<%=Session ("Progetto")%>/images/formazione/icons/consulta.gif" border="0" WIDTH="15" HEIGHT="13">
							    </a>
							
							<td>
								<a href="javascript:Scarica('<%=Percorso%>')">
								  <img src="<%=Session ("Progetto")%>/images/formazione/icons/download.gif" border="0">
							    </a>
							</td>
							<td width="200">&nbsp;&nbsp;</td>
				  <%end select
					%>
					
				
				<%end if%>
			 	</center>
			</td>
		</tr>
		
		
	<%
		recMatM.MoveNext
		loop
	%>
	





<%
end if
%>
</table>
<br>
<table width="500">
	<tr>
		<td align="center">
			<a href="javascript:history.back()" onmouseover="javascript:window.status=''; return true">
				<img src="<%=Session("Progetto")%>/images/indietro.gif" BORDER="0">
			</a>			
		</td>
	</tr>	
</table>
<!--#include virtual="/Include/CloseConn.asp"-->
<!--#include virtual="/strutt_coda2.asp"-->
