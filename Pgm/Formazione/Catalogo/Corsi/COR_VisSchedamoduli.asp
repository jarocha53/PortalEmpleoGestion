<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include virtual="/strutt_testa2.asp"-->
<!--#include virtual="/Include/settestatafad.asp"-->
<!--#include virtual="/Include/OpenConn.asp"-->
<!--#include virtual="/Include/Htmlencode.asp"-->
<!--#include virtual="/Include/FruizioneModuli.asp"-->

<script language="javascript">
<!--#include Virtual = "/Include/help.inc"-->
function InviaIndietro()
{
		frmPaginaIndietro.submit();
} 

function InviaAsso(ID,Q){

	document.frmQuestE.IDQue.value=ID;
	document.frmQuestE.QModo.value=Q;
	document.frmQuestE.submit();
}
function VisMateriali(ID_elem,Tipo){
	document.frmMatModulo.txtID_ele.value=ID_elem
	document.frmMatModulo.txtTipo.value=Tipo
	document.frmMatModulo.submit();
}
</script>
<%
	PagIndietro = Request.Form ("hpag")
	
	scorsi = Request.Form ("titoli")
	stitolocorso = Request.Form ("hidcor")
	sidele = Request.Form ("element")
	sSqlskmodulo = "SELECT e.credit_elemento, e.master_score, e.max_time, c.id_corsoaicc, e.id_elementoaicc, " &_
	               "c.info_produttore, ver_corsoaicc, e.desc_elemento, e.durata_elemento, e.tipo_elemento," &_
	               "E.link_Elemento,e.id_questionario,E.Titolo_Elemento, E.obiettivo_elemento, E.fl_erogazione  " &_
				   "FROM Elemento E, Corso C, Regola_Dipendenza R " &_
                   "WHERE C.id_corso =" & scorsi  & " AND C.id_Corso = R.id_Corso " &_
                   "AND R.ID_Elemento = E.id_Elemento AND E.id_Elemento=" & sidele
      
'PL-SQL * T-SQL  
SSQLSKMODULO = TransformPLSQLToTSQL (SSQLSKMODULO) 
	set rsmodulo = CC.execute(sSqlskmodulo)
		if not rsmodulo.eof then
			sCredEle = rsmodulo.fields("CREDIT_ELEMENTO")
			nVerAicc = rsmodulo.fields("VER_CORSOAICC")
			sInfoPro = rsmodulo.fields("INFO_PRODUTTORE")
			sIdCAICC = rsmodulo.fields("ID_CORSOAICC")
			sIdEAICC = rsmodulo.fields("ID_ELEMENTOAICC")
			sMasterScor = rsmodulo.fields("MASTER_SCORE")
			sMaxTime	= rsmodulo.fields("MAX_TIME")
			
			stitolo =	strhtmlencode(rsmodulo.fields("Titolo_Elemento"))
			sobiettivo = strhtmlencode(rsmodulo.fields("obiettivo_elemento"))
			serogazione = rsmodulo.fields("fl_erogazione")
			slinkele = rsmodulo.fields("link_Elemento")
			stipoelemento=rsmodulo.fields("tipo_elemento")
			sDescElemento = strhtmlencode(rsmodulo.fields("DESC_ELEMENTO"))
			sDurataElemento = rsmodulo.fields("DURATA_ELEMENTO")
			
				
			select case stipoelemento
			case "L" 
				sTitolo = "Scheda Modulo"
				sComm = "Questa pagina riassume tutte le informazioni relative al modulo che hai selezionato. " &_
						"Il sistema controller� se puoi seguire il modulo scelto secondo il percorso che hai " &_
						"gi� svolto. Se il modulo scelto segue il percorso congruente con il tuo profilo potrai " &_
						"cliccare su &quot;<b>SEGUI IL MODULO</b>&quot; e iniziare a studiare la lezione, altrimenti " &_
						"potrai soltanto consultare i materiali che lo compongono cliccando su &quot;<b>MATERIALE MODULO</b>&quot;. "
				sHlp = "/Pgm/help/formazione/Catalogo/corsi/COR_VisSchedamoduli/"
			case "T"
				sTitolo = "Scheda Test"
				sComm = "La Scheda Test riassume le caratteristiche principali del Test di Valutazione. Il Test di " &_
						"Valutazione � lo strumento che ti permette di verificare le conoscenze acquisite. Infatti, in " &_
						"base alle risposte fornite potrai, se necessario, rivedere gli argomenti gi� studiati per " &_ 
						"migliorare le tue conoscenze. Il sistema controller� se puoi eseguire il Test dettagliato nella " &_
						"scheda in base al percorso che hai gi� svolto. Se il Test scelto � congruente con il tuo percorso " &_
						"potrai cliccare su &quot;ESEGUI IL TEST&quot; e iniziare a svolgerlo (In bocca al lupo!), altrimenti " &_
						"troverai l'informazione &quot;TEST NON ESEGUIBILE&quot; e dovrai riprendere il percorso da dove lo " &_
						"hai lasciato. Tieni presente che se il sistema non ti permette di eseguire il Test non potrai " &_
						"neanche consultare i materiali relativi."
				sHlp = "/Pgm/help/formazione/Catalogo/corsi/COR_VisSchedacorso/"
			end select

call TestataFAD(sTitolo, sComm, false, sHlp)
%>
<table border="0" CELLPADDING="0" CELLSPACING="0" width="100%">	
	<tr>
		<td width="500" class="tbltext" align="left">
			<b>Corso:&nbsp;<%=stitolocorso%></b>
		</td>
	</tr>
	<tr><td>&nbsp;</td></tr>
	<tr>
		<td width="450" class="tbltext3" align="center">
			<%=stitolo%>
		</td>
<%

'*** parte riguardante il controllo di associazione ELEMENTO /QUESTIONARIO
		dim sqlAssoE
		dim recAssoE
		dim AssoE
		dim IdQuestio
		
		set recAssoE=Server.CreateObject("ADODB.Recordset")
		'controllo che ci sia un'associazione tra l'ELEMENTO e il QUESTIONARIO
		sqlAssoE="SELECT id_questionario FROM provevalutazione WHERE ID_ELEMENTO=" & sidele & ""
		 
'PL-SQL * T-SQL  
SQLASSOE = TransformPLSQLToTSQL (SQLASSOE) 
		recAssoE.Open sqlAssoE,CC,3
		
		if not recAssoE.EOF then
			AssoE="si"
			IdQuestio=recAssoE("ID_QUESTIONARIO")
		else
			AssoE="no"
		end if
'****   fine **********
		if AssoE="si" then%>
			<td width="50" align="left">
				<a href="javascript:InviaAsso(<%=IdQuestio%>,0)" class="textred">
					<img src="<%=Session("Progetto")%>/images/formazione/questr.gif" border="0" alt="Prove di Autovalutazione" WIDTH="27" HEIGHT="26">
				</a>
			</td>
		<%
		end if
%>	
	</tr>
</table>
<br>
<%		
		select case stipoelemento
			case "L"
				if nVerAicc > "" then				
							
					sPag = "http://" & Request.ServerVariables("HTTP_HOST") & "/pgm/formazione/AICC/girasole.asp"
					sParamElem = session("idutente") & "|" & sIdCAICC & "|" & sIdEAICC
					sPathQuest = slinkele & "?aicc_sid=" & sParamElem & "&aicc_url=" & sPag
				else
					sPathQuest = slinkele
				end if
				sPathQuest = Replace(sPathQuest, "&", "�")				
%>
				<script language="Javascript">
					function FaiCorso() {
						var ver = '<%=nVerAicc%>';
						//var corso = window.open('COR_VisCorso.asp?CredEle=<%=sCredEle%>&MasScore=<%=sMasterScor%>&MaxTime=<%=sMaxTime%>&InfoPro=<%=sInfoPro%>&IdCAicc=<%=sIdCAICC%>&IdEAICC=<%=sIdEAICC%>&AICC=<%=nVerAicc%>&idque=<%=nIdQuestionario%>&idele=<%=sidele%>&idcorso=<%=scorsi%>&Per=<%=sPathQuest%>','Info','width=800,height=400,resizable=yes,location=no,menubar=no,toolbar=no,status=no') 
						if (ver != "") {
							//var p = window.open('<%=sPathQuest%>','Info','width=800,height=400,resizable=yes,location=no,menubar=no,toolbar=no,status=no');
							var corso = window.open('COR_Aggiorna.asp?CredEle=<%=sCredEle%>&MasScore=<%=sMasterScor%>&MaxTime=<%=sMaxTime%>&InfoPro=<%=sInfoPro%>&IdCAicc=<%=sIdCAICC%>&IdEAICC=<%=sIdEAICC%>&AICC=<%=nVerAicc%>&idque=<%=nIdQuestionario%>&idele=<%=sidele%>&idcorso=<%=scorsi%>&Per=<%=sPathQuest%>','Info','width=800,height=600,Resize=No,Scrollbars=yes,screenX=w,screenY=h');
						} else {
							var corso = window.open('COR_VisCorso.asp?CredEle=<%=sCredEle%>&MasScore=<%=sMasterScor%>&MaxTime=<%=sMaxTime%>&InfoPro=<%=sInfoPro%>&IdCAicc=<%=sIdCAICC%>&IdEAICC=<%=sIdEAICC%>&AICC=<%=nVerAicc%>&idque=<%=nIdQuestionario%>&idele=<%=sidele%>&idcorso=<%=scorsi%>&Per=<%=sPathQuest%>','Info','width=800,height=600,Resize=No,Scrollbars=yes,screenX=w,screenY=h');
						}
					}
				</script>

<%			case "T"
			
				nIdQuestionario = clng(rsmodulo.fields("id_questionario"))
				sPathQuest = "/Pgm/Formazione/Questionari/QUE_Questionario.asp"
				
%>
				<form name="frmCorso" method="POST" action="<%=sPathQuest%>">
					<input type="hidden" name="idque" value="<%=nIdQuestionario%>">
					<input type="hidden" name="QModo" value="1">
					<input type="hidden" name="Elem" value="<%=sidele%>">
					<input type="hidden" name="IDCorso" value="<%=scorsi%>">
				</form>
<%  	end select


%>
<table border="0" width="100%" cellpadding="0" cellspacing="1">		 
	<tr>
		<td width="40%"><b class="tbltext">DESCRIZIONE</b>
		</td>
		<td width="60%" class="tbltext"><%=sDescElemento%>
		</td>
	</tr>
	<tr>
		<td width="40%"><b class="tbltext">DURATA</b>
		</td>
		<td width="60%" class="tbltext"><%=sDurataElemento%>
		</td>
	</tr>
		
	<tr>
		<td width="40%"><b class="tbltext">OBIETTIVI</b>
		</td>
		<td width="60%" class="tbltext"><%=sobiettivo%>
		</td>
	</tr>
		
	<tr>
		<td width="40%"><b class="tbltext">PREREQUISITI</b>
		</td>
		<td width="60%" class="tbltext">
		</td>
	</tr>
	<tr>
		<td width="40%"><b class="tbltext">MODALITA' EROGAZIONE</b>
		</td>
<%If serogazione=0 Then
	serogazione = "In Presenza"
%>
		<td width="60%" class="tbltext"><%=serogazione%>
		</td>
<%Else	
	serogazione = "A Distanza"
%>
		<td width="60%" class="tbltext"><%=serogazione%>
		</td>
<%End IF
%>	
	</tr>
</table>
<table border="0" width="100%" cellpadding="0" cellspacing="1" width="100%">	
	<br>
	<tr>	
<%		select case stipoelemento
			case "L" 
				if ControllaFruizioneModulo(scorsi,sidele) then	
				%>
					<td width="25%" align="left" valign="bottom">
						<!--a class="textred" href="javascript:LanciaCorso('<%=sLinkEle%>','<%=sParamElem%>')" onmouseover="window.status =' '; return true"-->
						<a class="textred" href="javascript:FaiCorso()" onmouseover="window.status =' '; return true">
							<b>SEGUI IL MODULO</b>
						</a>
					</td>
				<%else %>
    				<td width="40%" align="left" valign="bottom">
							<p class="textred"><b>NON PUOI ESEGUIRE IL MODULO</b></p>
	    			</td>		
				<%end if%>
						
		 			<td width="35%" align="left">
		 				&nbsp;&nbsp;
		 			</td>
				
				<td width="25%" align="left" valign="bottom" nowrap><b>
					<a class="textred" href="javascript:VisMateriali(<%=sidele%>,'MAT_ELE')" onmouseover="javascript:status='' ; return true">
						<b>MATERIALE MODULO</b>
					</a>
				</td>
				
					<form name="frmMatModulo" action="COR_VisMaterialeCorMod.asp" method="post">
						<input type="hidden" id="txtID_ele" name="txtID_ele">
						<input type="hidden" id="txtTipo" name="txtTipo">
					</form>
<%			case "T" 
				if ControllaFruizioneModulo(scorsi,sidele) then	%>
				<td width="40%" align="left" valign="top">
					<a class="textred" href="Javascript:frmCorso.submit();" onmouseover="window.status =' '; return true">
						<b>ESEGUI IL TEST</b>
					</a>
				<%else %>
					<p class="textred"><b>NON PUOI ESEGUIRE IL TEST</b></p>
				<%end if
				
				if AssoE="si" then%>
					<td width="35%" align="left" valign="bottom">
						<a href="javascript:InviaAsso(<%=IdQuestio%>,0)" class="textred">
							<b>PROVE DI AUTOVALUTAZIONE</b>	
						</a>
					</td>
				<%else%>
		
		 			<td width="35%" align="left">
		 				&nbsp;&nbsp;
		 			</td>
				<%end if%>
				
		<td width="50%" align="left" valign="bottom">
			<b><a class="textred" href>MATERIALE TEST</b>
		</td>
<%  	end select%>
		
	</tr>
	
	<!--tr> 		<td width="100%" align="right" COLSPAN="2">				< Inizio Modifica Marco Attenni 28/11/2002 >					<a class="textred" title="Fruisci modulo" href="Javascript:frmCorso.submit();" onmouseover="window.status =' '; 				return true"><b>FRUISCI MODULO</b></a>	</tr-->
</table>	
		
<form name="frmQuestE" action="../../Questionari/QUE_Questionario.asp" method="post">
			<input type="hidden" id="IDQue" name="IDQue">
			<input type="hidden" id="QModo" name="QModo">
</form>
<%	
Else%>
<table>
	<tr class="sfondocomm">
		<td>Non sono presenti corsi
		</td>
	</tr>
</table>
<% 
End If
rsmodulo.close
set rsmodulo = nothing
%>
<table>
<br>
	<tr>
		<td align="right" width="100%">
			<form name="frmPaginaIndietro" method="post" action="COR_VisSchedacorso.asp">
			<a href="javascript:InviaIndietro()" onmouseover="javascript:status='' ; return true">		
			<img border="0" src="<%=Session("Progetto")%>/images/indietro.gif"></a>
			<input type="hidden" name="titoli" value="<%=scorsi%>">
			<input type="hidden" name="hpag" value="<%=PagIndietro%>">
			</form>
		</td>
	</tr>
</table>	
<!--#include virtual="/Include/CloseConn.asp"-->
<!--#include virtual="/strutt_coda2.asp"-->
