<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%
Sub Cancella_Pubblico()
	'Inserimento nella tabella FRM_ISCRIZIONE
	Dim nIdArea, sErrore, i, j, zz 
	Dim sqlFrmIscri, sqlDelCanPub, sqlInsCanPub

	CC.BeginTrans
	' Eliminazione dell'occorrenza relativa al canale di faq "Pubblico"
	sqlDelCanPub = "DELETE FROM frm_iscrizione WHERE cod_ruolo IS NULL AND id_frm_area=" & nIdCanale 
	sErrore = EseguiNoC(sqlDelCanPub,CC) 	

	If sErrore = "0" Then
		For i = 0 To  n-1
			' So quanti sottogruppi esistono per quel rorga.
			' Response.Write "<BR>" & Request.form("chkidgru" & i ).Count 
			' Se non esistono non ci sono sotto gruppi, altrimenti
			' devo fare tante insert quanti sono i sottogruppi.
			If Trim(Request.form("txtDtDal" & i)) <> "" or Request.form("chkidgru" & i ).Count > 0 then
				If Request.form("chkRuo" & i) <> "" Then
					sRuolo = Request.Form("chkRuo" & i)
				Else
					sRuolo = Request.Form("HchkRuo" & i)
				End If
	
				If Request.form("chkidgru" & i ).Count = 0 then
					sqlInsCanPub = "INSERT INTO frm_iscrizione (ID_FRM_AREA, COD_RUOLO, DT_DAL, TIPO, DT_TMST" 
					If isDate(Request.form("txtDtAl" & i)) Then
						sqlInsCanPub = sqlInsCanPub & ",DT_AL"
					End If
					sqlInsCanPub = sqlInsCanPub & ") VALUES (" & nIdCanale 
					sqlInsCanPub = sqlInsCanPub & ", '" & sRuolo & "'," & ConvDateToDbS(ConvStringToDate(Request.form("txtDtDal" & i))) & "," 
					sqlInsCanPub = sqlInsCanPub & "'Q', " & ConvDateToDb(Now()) 
					If isDate(Request.form("txtDtAl" & i)) Then
						sqlInsCanPub = sqlInsCanPub & "," & ConvDateToDbS(ConvStringToDate(Request.form("txtDtAl" & i)))
					End If
					sqlInsCanPub = sqlInsCanPub & ")"

					sErrore = EseguiNoC(sqlInsCanPub,CC)				
				Else
					
					If sErrore <> "0" Then
						exit for
					End If	 

					sIDGrup = 0 
					sIDGrup = Trim(Request.form("chkidgru" & i ))
					if sIDGrup <> "" then
						sRuolo = Request.Form("RorgaGru" & i)
						aIDGrup = Split(sIDGrup,",")
						sIDIniVal = Trim(Request.form("txtDtDalGru" & i ))
						sIDFinVal = Trim(Request.form("txtDtAlGru" & i ))

						aIDIniVal = Split(sIDIniVal,",")
						aIDFinVal = Split(sIDFinVal,",")

						If ubound(aIDFinVal) = -1 Then
							redim aIDFinVal(0)
							aIDFinVal(0) = ""
						End If

						sAppo = ""
						sAppo2 = ""
						For zz = 0 to UBound(aIDIniVal)
							If Trim(aIDIniVal(zz)) <> "" Then
								sAppo = sAppo & Trim(aIDIniVal(zz)) & ","
								sAppo2 = sAppo2 & Trim(aIDFinVal(zz)) & ","
							End If 
						Next 
						sAppo	= Left(sAppo,Len(sAppo)-1)
						sAppo2	= Left(sAppo2,Len(sAppo2)-1)
						aIDIniVal = Split(sAppo,",")
						aIDFinVal = Split(sAppo2,",")

						If ubound(aIDFinVal) = -1 Then
							redim aIDFinVal(0)
							aIDFinVal(0) = ""
						End If

						' Mi formatto le date per inserirle nel DB.
						For nGrup = 0 to UBound(aIDGrup) 
							aIDIniVal(nGrup) = ConvDateToDBs(ConvStringToDate(aIDIniVal(nGrup)))
							If aIDFinVal(nGrup) <> "" Then
								aIDFinVal(nGrup) = ConvDateToDBs(ConvStringToDate(aIDFinVal(nGrup)))
							Else
								aIDFinVal(nGrup) = "null"
							End If
							sqlFrmIscri = "INSERT INTO frm_iscrizione (ID_FRM_AREA, COD_RUOLO, DT_DAL,DT_AL, TIPO,IDGRUPPO, DT_TMST) " &_
								          "VALUES (" &  nIdCanale & ",'" & sRuolo & "'," & aIDIniVal(nGrup) &_
								          "," & aIDFinVal(nGrup) & ",'Q'," & aIDGrup(nGrup) & ",SYSDATE)"
							sErrore = EseguiNoC(sqlFrmIscri,CC)
							If sErrore <> "0" Then
								exit for
							End If	
						Next
					End If 
				End If
			End If
			If sErrore <> "0" Then
				exit for
			End If				
		Next
	End If

	If sErrore <> "0" Then
		CC.RollbackTrans
		strErrore=sErrore
	Else
		CC.CommitTrans
	End If

	 If sErrore <> "0" Then
%>	
<br><br>		
		<TABLE border="0" cellspacing="0" cellpadding="0" width="100%">
			<tr align="center"> 
				<td class="tbltext3">
					<b>Errore. <%=strErrore%></b>
			    </td>
			</tr>
		</TABLE>		
		<br><br>
		<TABLE border="0" cellspacing="0" cellpadding="0" width="100%">
			<tr> 
				<td align="center"> 
					<a class="textred" href="javascript:history.back()" onmouseover="window.status =' '; return true">
					<b>Pagina precedente </b></a>
				</td>
				<td align="center"> 
					<a class="textred" href="/Pgm/formazione/faq/FAQ_VisCanali.asp" onmouseover="window.status =' '; return true">
					<b>Torna alle Aree </b></a>
				</td>
			</tr>
		</TABLE>
<%	Else %>	
<br><br>		
		<TABLE border="0" cellspacing="0" cellpadding="0" width="100%">
			<tr align="center"> 
				<td class="tbltext3">
					<b><%=sOperazione%> correttamente effettuata.</b>
		        </td>
			</tr>
		</TABLE>		
		<br><br>
		<meta HTTP-EQUIV="Refresh" CONTENT="2; URL=/Pgm/formazione/faq/FAQ_VisCanali.asp">
<%	End If
End Sub

Sub Inserisci_Pubblico()

	Dim nIdArea, sDT_Dal, sDt_Al
	Dim sErrore, sqlInsIscr,sqlDelInsPub, RR, i, j
		
	sDt_Dal = Request.form("txtDtDalPN")
	sDt_Al =  Request.form("txtDtAlPN")
	CC.BeginTrans
	
	sqlDelInsPub = "DELETE FROM frm_iscrizione where id_frm_area=" & nIdCanale 
	sErrore = EseguiNoC(sqlDelInsPub,CC)
		
	if sErrore = "0" then		
		sqlInsIscr = "INSERT INTO frm_iscrizione (ID_FRM_AREA, DT_DAL, TIPO, DT_TMST"
		if isdate(sDt_Al) then
			sqlInsIscr = sqlInsIscr & ",DT_AL"
		end if
		sqlInsIscr = sqlInsIscr & ") Values(" & nIdCanale
		sqlInsIscr = sqlInsIscr & "," & ConvDateToDbS(ConvStringToDate(sDt_Dal)) & "," 
		sqlInsIscr = sqlInsIscr & "'Q', " & ConvDateToDb(Now()) 
		if isdate(sDt_Al) then
			sqlInsIscr = sqlInsIscr & ","  & ConvDateToDbS(ConvStringToDate(sDt_Al)) 
		end if
		sqlInsIscr = sqlInsIscr & ")"
						
		sErrore = EseguiNoC(sqlInsIscr,CC)
	end if
	if sErrore <> "0" then
		CC.RollbackTrans
		strErrore=sErrore
	else
		CC.CommitTrans
	end if%>	
	<br><br>
<%	if sErrore <> "0" then %>
		<TABLE border="0" cellspacing="0" cellpadding="0" width="100%">
			<tr align="center"> 
				<td class="tbltext3">
					<b>Errore. <%=strErrore%></b>
		        </td>
			</tr>
		</TABLE>		
		<br><br>
		<TABLE border="0" cellspacing="0" cellpadding="0" width="100%">
			<tr> 
				<td align="center"> 
					<a class="textred" href="javascript:history.back()" onmouseover="window.status =' '; return true">
					<b>Pagina precedente </b></a>
				</td>
				<td align="center"> 
					<a class="textred" href="/Pgm/formazione/faq/FAQ_VisCanali.asp" onmouseover="window.status =' '; return true">
					<b>Torna alle Aree </b></a>
				</td>
			</tr>
		</TABLE>
<%	else %>			
		<TABLE border="0" cellspacing="0" cellpadding="0" width="100%">
			<tr align="center"> 
				<td class="tbltext3">
					<b><%=sOperazione%> correttamente effettuata.</b>
		        </td>
			</tr>
		</TABLE>		
		<br><br>
		<meta HTTP-EQUIV="Refresh" CONTENT="2; URL=/Pgm/formazione/faq/FAQ_VisCanali.asp">
<%	end if
			
End Sub 

Sub Modifica()
'Inserimento nella tabella FRM_ISCRIZIONE
	dim nIdArea, sPDt_Dal, sPDt_Al, sqlDelMod
	dim sErrore, sqlUpIscr, RR, i, j
	sPN = request.form("chkPN")
	sPS = request.form("chkPS")
	sErrore = "0" 
	R = 0
	For i = 0 to n-1
		If Trim(Request.form("txtDtDal" & i)) <> "" Then
			R = R + 1
		End If
		If Trim(Request.form("chkIdGru" & i)) <> "" Then ' Se esistono sottogruppi.
			R = R + 1
		End If
	Next
	
	If sPN <> "" Then
'Si vuol modificare un canale di faq trasformandolo in pubblico
		Inserisci_Pubblico()
		exit sub	
	End If
	
	If sPS <> "Pubblico" And R <> 0 Then 
'Si vuol modificare un canale di faq trasformandolo da pubblico a privato
		Cancella_Pubblico()
		exit sub
	End If
	
	If sPS = "XX" Then	
'Si vuol modificare un canale pubblico
		sPDt_Dal = Request.form("txtDtDalPS")
		sPDt_Al =  Request.form("txtDtAlPS")
		sqlUpIscr = "UPDATE frm_iscrizione set "
		sqlUpIscr = sqlUpIscr & " dt_dal =" & ConvDateToDbS(convStringToDate(sPDt_Dal)) & "," 
		if isDate(sPDt_Al) then
			sqlUpIscr = sqlUpIscr & " dt_al  =" & ConvDateToDbS(convStringToDate(sPDt_Al))  & "," 
		end if
		sqlUpIscr = sqlUpIscr & " tipo = 'Q',"
		sqlUpIscr = sqlUpIscr & " dt_tmst=" & ConvDateToDb(Now())
		sqlUpIscr = sqlUpIscr & " WHERE id_frm_area =" & nIdCanale & " and cod_ruolo is null"
		sErrore = EseguiNoC(sqlUpIscr,CC)
		If sErrore <> "0" Then
			sErrore=decod_errore
		End If
	Else
		CC.BeginTrans
		For i = 0 To n-1
			If Request.form("chkRuo" & i) <> "" Then
				sRuolo = Request.form("chkRuo" & i)
			Else
				sRuolo = Request.form("HchkRuo" & i)
			End If	
			If sRuolo = "" Then
				sRuolo = Request.Form("RorgaGru" & i)
			End If
			If Trim(Request.Form("txtDtDal" & i)) <> "" and Trim(Request.form("chkIdGru" & i )) = "" Then

				sqlI = "SELECT id_frm_area FROM frm_iscrizione WHERE id_frm_area =" & nIdCanale
				sqlI = sqlI & " and cod_ruolo ='" & sRuolo & "' AND idgruppo IS NULL "
'PL-SQL * T-SQL  
SQLI = TransformPLSQLToTSQL (SQLI) 
				set rstIscr = CC.Execute(sqlI)
				If not rstIscr.EOF Then
					sql = "UPDATE frm_iscrizione set "
					sql = sql & " dt_dal =" & ConvDateToDbS(ConvStringToDate(Request.form("txtDtDal" & i))) & "," 
					If isDate(Request.form("txtDtAl" & i)) Then
						sql = sql & " dt_al  =" & ConvDateToDbS(ConvStringToDate(Request.form("txtDtAl" & i)))  & "," 
					End If
					sql = sql & " tipo = 'Q',"
					sql = sql & " dt_tmst=" & ConvDateToDb(Now())
					sql = sql & " WHERE id_frm_area =" & nIdCanale & " and cod_ruolo ='" & sRuolo & "'"
				Else
						
					sqlDelArea = "DELETE frm_iscrizione WHERE id_frm_area =" & nIdCanale &_
					             " and cod_ruolo ='" & sRuolo & "'"
						
					sErrore = EseguiNoC(sqlDelArea,CC)
					If sErrore <> "0" then
						strErrore=sErrore
						exit for
					End If

					sql = "INSERT INTO frm_iscrizione (ID_FRM_AREA, COD_RUOLO, DT_DAL, TIPO, DT_TMST"
					If isdate(Request.form("txtDtAl" & i)) Then
						sql = sql & ",DT_AL"
					End If
					sql = sql & ") VALUES(" & nIdCanale
					sql = sql & ", '" & sRuolo & "'," & ConvDateToDbS(ConvStringToDate(Request.form("txtDtDal" & i))) 
					sql = sql & ", 'Q', " & ConvDateToDb(Now())
					If isdate(Request.form("txtDtAl" & i)) Then
						sql = sql & "," & ConvDateToDbS(ConvStringToDate(Request.form("txtDtAl" & i)))
					End If
					sql = sql & ")"
				End If
				sErrore = EseguiNoC(sql,CC)
				If sErrore <> "0" Then
					strErrore=sErrore
				End If
					
			Else
			
				If Trim(Request.form("chkIdGru" & i )) <> "" Then 
					' Se � valido st� facendo un inserimento
					sqlDelMod = "DELETE FROM frm_iscrizione WHERE cod_ruolo = '" & sRuolo &_
						   "' AND idgruppo IS NULL AND id_frm_area = " & nIdCanale
					sErrore = EseguiNoC(sqlDelMod,CC)
					sIDGrup = Trim(Request.form("chkIdGru" & i ))

					sPosGruppi	= Trim(Request.form("Gruppo" & i ))
					aPos		= Split(sPosGruppi,",")
					aIDGrup		= Split(sIDGrup,",")
					sIDIniVal	= Trim(Request.form("txtDtDalGru" & i ))
					sIDFinVal	= Trim(Request.form("txtDtAlGru" & i ))
					aIDIniVal	= Split(sIDIniVal,",")
					aIDFinVal	= Split(sIDFinVal,",")

					If ubound(aIDFinVal) = -1 Then
						redim aIDFinVal(0)
						aIDFinVal(0) = ""
					End If					

					' Nel caso in cui faccio un inserimento  
					' prendo le date inizio e date fine corrette.
					' Infatti nel caso in cui ci siano tre elementi 
					' di cui due gi� inseriti devo prendere i dati 
					' dell'ultimo (DATA INIZIO e DATA FINE),  
					' nella split  ** aIDIniVal = Split(sIDIniVal,",") ** e nella
					' split ** aIDFinVal = Split(sIDFinVal,",") ** ho tutte le date.

					redim aSort(UBound(aIDGrup))
					redim aSortDateDal(UBound(aIDGrup))
					redim aSortDateAl(UBound(aIDGrup))
					nPos = 0 
					Do While Not ( nPos > UBound(aIDGrup) or nPos >  UBound(aPos) )
						For nData = 0 to UBound(aPos)
							If clng(aPos(nData)) = clng(aIDGrup(nPos)) Then
								aSort(nPos)			= CLNG(aIDGrup(nPos))
								aSortDateDal(nPos)	= Trim(aIDIniVal(nData))
								aSortDateAl(nPos)	= Trim(aIDFinVal(nData))
								nPos = nPos + 1
							End If
							If nPos > UBound(aIDGrup) Then
								exit for
							End If						
						Next			

					Loop	
	
					' Mi formatto le date per inserirle nel DB.
					For nGrup = 0 to UBound(aSort) 
						aSortDateDal(nGrup) = ConvDateToDBs(ConvStringToDate(aSortDateDal(nGrup)))
						If aSortDateAl(nGrup) <> "" Then
							aSortDateAl(nGrup) = ConvDateToDBs(ConvStringToDate(aSortDateAl(nGrup)))
						Else				
							aSortDateAl(nGrup) = "null"
						End If
						sqlFrmIscri = "INSERT INTO frm_iscrizione (ID_FRM_AREA, COD_RUOLO, DT_DAL,DT_AL, TIPO,IDGRUPPO, DT_TMST) " &_
							  "VALUES (" &  nIdCanale & ",'" & sRuolo & "'," &_
							  aSortDateDal(nGrup) & "," & aSortDateAl(nGrup) & ",'Q'," & CLng(aIDGrup(nGrup)) & ",SYSDATE)"
						sErrore = EseguiNoC(sqlFrmIscri,CC)
						If sErrore <> "0" Then
							exit for
						End If	
					Next
				Else ' Aggiorno i dati che ci sono 
					sqlI = ""
					sqlFrmIscri = ""
					sIDGrup = Trim(Request.form("CheckGru" & i ))

					If Trim(Replace(sIDGrup,",","")) <> "" Then
						sqlI = "SELECT count(id_frm_area) as Conta FROM frm_iscrizione WHERE id_frm_area = " & nIdCanale
						sqlI = sqlI & " AND cod_ruolo ='" & sRuolo & "' AND idgruppo in (" & sIDGrup & ")" 

'PL-SQL * T-SQL  
SQLI = TransformPLSQLToTSQL (SQLI) 
						set rsCheck = cc.Execute(sqlI) 
						If cint(rsCheck("Conta")) > 0 Then
							aIDGrup = Split(Trim(sIDGrup),",")
							sIDIniVal = Trim(Request.form("txtDtDalGru" & i ))
							sIDFinVal = Trim(Request.form("txtDtAlGru" & i ))
							aIDIniVal = Split(sIDIniVal,",")
							aIDFinVal = Split(sIDFinVal,",")
							If ubound(aIDFinVal) = -1 Then
								redim aIDFinVal(0)
								aIDFinVal(0) = ""
							End If
							sAppo = ""
							sAppo2 = ""
							for zz = 0 to UBound(aIDIniVal)
								if Trim(aIDIniVal(zz)) <> "" Then
									sAppo = sAppo & Trim(aIDIniVal(zz)) & ","
									sAppo2 = sAppo2 & Trim(aIDFinVal(zz)) & ","
								End If 
							next 
							sAppo = left(sAppo,len(sAppo)-1)
							sAppo2 = left(sAppo2,len(sAppo2)-1)
							aIDIniVal = Split(sAppo,",")
							aIDFinVal = Split(sAppo2,",")
							If ubound(aIDFinVal) = -1 Then
								redim aIDFinVal(0)
								aIDFinVal(0) = ""
							End If
							' Mi formatto le date per inserirle nel DB.
							For nGrup = 0 to UBound(aIDGrup) 
								aIDIniVal(nGrup) = ConvDateToDBs(ConvStringToDate(aIDIniVal(nGrup)))
								If aIDFinVal(nGrup) <> "" Then
									aIDFinVal(nGrup) = ConvDateToDBs(ConvStringToDate(aIDFinVal(nGrup)))
								Else				
									aIDFinVal(nGrup) = "null"
								End If
								sqlFrmIscri = "UPDATE frm_iscrizione " &_
									"SET dt_dal=" & aIDIniVal(nGrup) &_
									",dt_al = " & aIDFinVal(nGrup) & ",dt_tmst = SYSDATE " &_
									" WHERE id_frm_area = " & nIdCanale  & " AND idgruppo in " & CLng(aIDGrup(nGrup))
								sErrore = EseguiNoC(sqlFrmIscri,CC)
								If sErrore <> "0" Then
									exit for
								End If
							next
						End If
						rsCheck.close
						set rsCheck=nothing			
					End If
				End If 	
			End If
		Next

		If sErrore <> "0" Then

			CC.RollbackTrans
			strErrore=sErrore
		Else
			CC.CommitTrans
		End If
	End If
		
	If sErrore <> "0" Then %>
	<br><br>
	<TABLE border="0" cellspacing="0" cellpadding="0" width="100%">
			<tr align="center"> 
				<td class="tbltext3">
					<b>
					Errore. <%=strErrore%>
	                </b>
	            </td>
			</tr>
		</TABLE>		
		<br><br>
		<TABLE border="0" cellspacing="0" cellpadding="0" width="100%">
			<tr> 
				<td align="center"> 
					<a class="textred" href="javascript:history.back()" onmouseover="window.status =' '; return true">
					<b>Pagina precedente </b></a>
				</td>
				<td align="center"> 
					<a class="textred" href="/Pgm/formazione/faq/FAQ_VisCanali.asp" onmouseover="window.status =' '; return true">
					<b>Torna alle Aree </b></a>
				</td>
			</tr>
		</TABLE>
<%	Else %>			
		<TABLE border="0" cellspacing="0" cellpadding="0" width="100%">
			<tr align="center"> 
				<td class="tbltext3">
					<b>
					<%=sOperazione%> correttamente effettuata.
	                </b>
	            </td>
			</tr>
		</TABLE>		
		<br><br>
		<meta HTTP-EQUIV="Refresh" CONTENT="2; URL=/Pgm/formazione/faq/FAQ_VisCanali.asp">
<%	End If
			
End Sub
'************************  MAIN	*******************+
%>
<!--#include virtual ="/util/portallib.asp"-->
<!--#include virtual ="/strutt_testa2.asp"-->
<!--#include Virtual="/include/openconn.asp"-->
<!--#include Virtual="/include/DecCod.asp"-->
<!--#include Virtual="/include/ControlDateVb.asp"-->
<!--#include Virtual="/include/SysFunction.asp"-->
<%
Dim strConn, sql
Dim sDescr, sOggetto, n, nIdCanale
Dim sOperazione
'nID = clng(Request.Form("cat")) ' ID della categoria.
nIdCanale = clng(Request.Form("IdCanale")) 'ID del canale
nIdAreaTem = clng(Request.Form("IdAreaTem")) ' ID area Tematica.
n	= clng(Request.Form("n"))   ' Numero dei codici rorga.
sAction = Request.Form ("Action")

If sAction = "INS" Then
	sOperazione = "Inserimento"
	nIdCanale = InserisciCanale()
Else
	sOperazione = "Modifica"
End If
If isNumeric(nIdCanale) Then
	Modifica()
Else
Response.Write "Errore"
End If

Function InserisciCanale()
		'Inserimento nella tabella FRM_AREA
	
		Dim nIdArea, sDescr, sOggetto
		Dim sErrore, sqlInsArea, rstCanale, n, i, j

		sDescr  = Replace(Request.Form("txtDescr"), "'", "''")
		sOggetto= Replace(Request.Form("txtCanale"), "'", "''")
		
		CC.BeginTrans
			
		dt_tmst = ConvDateToDb(Now())
		sqlInsArea = "INSERT INTO frm_area (TITOLO_AREA, DESC_AREA, IMG_AREA, DT_TMST, TIPO, ID_AREA) VALUES " &_
		             "('" & sOggetto & "','" & sDescr & "',' '," & dt_Tmst & ", 'Q', " & nIdAreaTem & ")"
		
		sErrore=EseguiNoC(sqlInsArea,CC)		

		If sErrore <> "0" then
			InserisciCanale = sErrore
			CC.RollbackTrans
		Else
			set rstCanale = Server.CreateObject("ADODB.Recordset")
			sqlCanale = "SELECT id_frm_area FROM frm_area WHERE dt_tmst=" & dt_Tmst
'PL-SQL * T-SQL  
SQLCANALE = TransformPLSQLToTSQL (SQLCANALE) 
			rstCanale.open sqlCanale, CC, 3	
			nIdCanaleFaq = clng(rstCanale("ID_FRM_AREA"))
			rstCanale.Close
			set rstCanale = nothing
			InserisciCanale = nIdCanaleFaq
			CC.CommitTrans 
		End if

End Function 
%>
<!--#include Virtual="/include/closeconn.asp"-->
<!--#include Virtual="/strutt_coda2.asp"-->
