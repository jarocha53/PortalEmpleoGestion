<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include Virtual="/strutt_testa2.asp"-->
<!--#include Virtual="/util/portallib.asp"-->
<!--#include Virtual="/include/ControlDateVB.asp"-->
<!--#include Virtual="/include/openconn.asp"-->
<!--#include Virtual="/pgm/formazione/FAQ/FAQ_Intestazione.asp"-->

<script LANGUAGE="JavaScript" src="/Include/help.inc"></script>
<script language="JavaScript">
	function Invia(Path,nIdCanale,Area,ID,R,pag,n)
	{
		document.FrmFAQ.action = Path;
		document.FrmFAQ.IdCanale.value = nIdCanale;
		document.FrmFAQ.sArea.value = Area;
		document.FrmFAQ.R.value = R;
		document.FrmFAQ.pag.value = pag;
		document.FrmFAQ.n.value = n;
		document.FrmFAQ.ID_FAQ.value = ID;
		if (Path == "FAQ_ElencoBackR.asp"){
			document.FrmFAQ.Page.value = "FAQ_ElencoBackR"}
		if (Path == "FAQ_RispostaBack.asp"){
		document.FrmFAQ.Page.value = "FAQ_RispostaBack"}
		if (Path == "FAQ_ElencoBack.asp"){
			document.FrmFAQ.Page.value = "FAQ_ElencoBack"}
		if (Path == "FAQ_CancellaBack.asp"){
			document.FrmFAQ.Page.value = "FAQ_CancellaBack"}
				
		document.FrmFAQ.submit();
	}	
</script>
<%
	If not ValidateService(Session("IdUtente"),"GESTIONE FAQ",cc) Then 
		response.redirect "/util/error_login.asp"
	End If

 	ImpostaPag()
%>	
<!--#include Virtual="/include/closeconn.asp"-->
<!--#include Virtual="/strutt_coda2.asp"-->

<%
Sub ImpostaPag()
	Dim rstFaq, rstNumFaq, Sql
	Dim nNum, sData, sArea

	sArea = Request.Form("sArea")	
	nIdCanale = Request.Form("IdCanale")

	Response.Write 	"<br><br><TABLE width='100%' border='0'><tr>"
	Response.Write 	"<td align='center' class='tbltext1a'>"
	
	Set rstNumFaq = server.CreateObject("ADODB.Recordset")
	If sArea = "P" Then		'private
		sqlNumFaq = "SELECT Count(id_faq) as conta FROM faq_ur WHERE risposta IS NULL " &_
					"AND id_frm_area = " & nIdCanale &_
					" AND profilo = '" & sArea & "' "
		Response.Write "<b>Elenco Faq Private senza risposta</b>"
	Else					'pubbliche
		sqlNumFaq = "SELECT Count(id_faq) as conta FROM faq_ur WHERE risposta IS NULL " &_
					"AND id_frm_area = " & nIdCanale &_
					" AND (Profilo is null or Profilo <= ' ') "
		Response.Write "<b>Elenco Faq Pubbliche senza risposta</b>" 
	End If
	Response.Write 	"</td></tr></TABLE>"
	
'PL-SQL * T-SQL  
SQLNUMFAQ = TransformPLSQLToTSQL (SQLNUMFAQ) 
	rstNumFaq.open sqlNumFaq, CC, 3
		
	If not rstNumFaq.EOF Then
		Tot= rstNumFaq("conta")
	End If

	rstNumFaq.Close
	Set rstNumFaq = Nothing
	Set rstFaq = server.CreateObject("ADODB.Recordset")
	If cint(Tot) > " " Then
		If sArea = "P" Then		'private
			sqlFaq = "SELECT f.id_faq, f.data, f.argomento, f.domanda, u.nome, u.cognome FROM faq_ur f, utente u WHERE f.idutente = u.idutente " &_
					 "AND risposta IS NULL AND id_frm_area = " & nIdCanale &_
				     " AND profilo = '" & sArea & "' ORDER BY DATA desc"
		Else					'pubbliche
			sqlFaq = "SELECT id_faq, data, argomento, domanda FROM faq_ur " &_
					 "WHERE risposta IS NULL AND id_frm_area = " & nIdCanale &_
					 " AND (profilo is null or Profilo <= ' ') ORDER BY DATA desc"
		End If
		
'PL-SQL * T-SQL  
SQLFAQ = TransformPLSQLToTSQL (SQLFAQ) 
		rstFaq.open sqlFaq, CC, 3
			
		pag = Request.Form("pag") 
			
		If pag <> "" Then
			n=Request.Form ("n")
		Else
			n = 1
			pag = 1
		End If
		
		If not rstFaq.EOF Then
			nNum = n                                    
%>			
			<table border="0" cellspacing="0" cellpadding="0" width="100%" align="center">
				<tr> 
					<td align="left"> 
						<a class="textred" href="Faq_VisCanali.asp" onmouseover="window.status =' '; return true">
						<b>Lista Canali</b></a>
					</td>
<%				If sArea = "P" Then		'private %>
					<td align="right"> 
						<a class="textred" href="javascript:Invia('FAQ_ElencoBack.asp','<%=nIdCanale%>','','','','','')" onmouseover="window.status =' '; return true">
						<b>Vai alle Faq Pubbliche </b></a>
					</td>			
<%				Else					 'pubbliche %>
					<td align="right"> 
						<a class="textred" href="javascript:Invia('FAQ_ElencoBack.asp','<%=nIdCanale%>','P','','','','')" onmouseover="window.status =' '; return true">
						<b>Vai alle Faq Private </b></a>
					</td>
<%				End If %>
				</tr>
			</table>
				
			<table width="100%" BORDER="0" CELLSPACING="2" CELLPADDING="1">
				<tr align="center">
					<td>
						<a class="textred" href="javascript:Invia('FAQ_DomandaBack.asp','<%=nIdCanale%>','<%=sArea%>','','NO','','')"><strong>Nuova Domanda</strong></a>
					</td>
					<td>
						<a class="textred" href="javascript:Invia('FAQ_ElencoBackR.asp','<%=nIdCanale%>','<%=sArea%>','','','','')"><strong>Domande con risposta</strong></a>
					</td>
				</tr>
			</table>
			<br>
			<table width="100%" cellspacing="0" cellpadding="0" border="0">
				<tr align="middle">
					<td>&nbsp;</td> 
				</tr>
			</table>
<%				
			rstFaq.MoveFirst
			i = 1 
				
			Do Until CINT(i) = CINT(n)
				rstFaq.MoveNext
				i = i + 1
			Loop
		
			i = 1
			Do Until rstFaq.EOF or i > 10 
				sData=ConvDateToString(rstFaq("Data"))   
				If sArea = "P" Then	                                 
					sNome=rstFaq("nome")
					sCognome=rstFaq("cognome")
					sMitt=sNome & "&nbsp;" & sCognome & "&nbsp;-&nbsp;"
				Else
					sMitt=""
				End If
%>
				<table width="100%" cellspacing="2" cellpadding="1" border="0">
					 <tr> 
						<td height="15" width="2%" valign="middle" class="sfondocomm"><span class="tbltext"><b>&nbsp;<% = nNum %></span></td> 
						<td width="100%" align="left" class="sfondocomm" valign="top"><span class="tbltext"><b><%=sMitt%><%=sData%></b></span></td>
						<td align="right" width="3%" height="18" class="sfondocomm" valign="top"> 
						    <a class="tbltext" href="javascript:Invia('FAQ_RispostaBack.asp','<%=nIdCanale%>','<%=sArea%>',<%=rstFaq("ID_FAQ")%>,'NO',<%=pag%>,<%=((pag-1) * 10 ) + 1%>)" onmouseover="window.status =' '; return true"><b>Rispondi/Modifica</b></a> </td>
						<td align="right" width="3%" height="18" class="sfondocomm" valign="top"> 
						    <a class="tbltext" href="javascript:Invia('FAQ_CancellaBack.asp','<%=nIdCanale%>','<%=sArea%>',<%=rstFaq("ID_FAQ")%>,'NO',<%=pag%>,<%=((pag-1) * 10 ) + 1%>)" onmouseover="window.status =' '; return true"><b>Cancella</b></a>
						</td>
					</tr>
					<tr>		
						<td height="15" width="2%" class="tblsfondo"><span class="tbltext"><b>&nbsp;Oggetto:</span></td> 
						<td colspan="3" height="18" class="tblsfondo" align="left"> 
						  <span class="tbltext"><%=Server.HTMLEncode(rstFaq("Argomento"))%></span>
						</td>
					</tr>	
					<tr>					
						<td height="15" width="2%" class="tblsfondo"><span class="tbltext"><b>&nbsp;Domanda:</span></td> 
						<td colspan="3" height="18" align="left" class="tblsfondo">
							<span class="tbltext"><%=Server.HTMLEncode( rstFaq("Domanda"))%></span>
						</td>
					</tr>
					<tr>
						<td height="15" width="2%" class="tblsfondo"><span class="tbltext"><b>&nbsp;Risposta:</span></td> 
						<td colspan="3" height="18" class="tblsfondo" align="left">
							<span class="tbltext"><%'=Server.HTMLEncode( rstFaq("Risposta"))%> </span></td>
						<td height="15" width="2%" valign="middle">&nbsp;</td> 
					</tr>
				</table><br>

<%					
				rstFaq.MoveNext
				nNum = nNum + 1 
				n = n + 1
				i = i + 1
			Loop

			If i > 10 And Not rstFaq.EOF Then
%>				
				<table width="100%" border="0" align="center">
					<tr align="center"> 
						<td colspan="2" width="35%" align="right"> 
<%
						If pag > 1 Then
							pagold = pag - 1
							nold = ((pagold - 1) * 10 ) + 1
%>							
							<a class="textred" href="javascript:Invia('FAQ_ElencoBack.asp','<%=nIdCanale%>','<%=sArea%>','NO','',<%=pagold%>,<%=nold%>)">
							<img src="<%=Session("Progetto")%>/images/precedente.gif" border="0"></a>
<%						
						End If
						n = (pag * 10 ) + 1
						pag = pag + 1
						If cint(n) <= cint(tot) Then	
%>							<a class="textred" href="javascript:Invia('FAQ_ElencoBack.asp','<%=nIdCanale%>','<%=sArea%>','NO','',<%=pag%>,<%=n%>)">
								<img src="<%=Session("Progetto")%>/images/successivo.gif" border="0"></a>
						</td>
<%						End If
%>					</tr>
				</table>
<%			Else %>
				<table width="100%" align="center" border="0">
					<tr align="center"> 
						<td colspan="2" width="35%" align="right"> 
<%						If pag > 1 Then
							pagold = pag - 1
							nold = ((pagold - 1) * 10 ) + 1
%>							<a href="javascript:Invia('FAQ_ElencoBack.asp','=<%=nIdCanale%>','<%=sArea%>','NO','',<%=pagold%>,<%=nold%>)">
								<img SRC="<%=Session("Progetto")%>/images/precedente.gif" border="0">
							</a>
<%						End If %>
						</td>
					</tr>
				</table>
<%			
			End If			
		Else
			Messaggio("Non ci sono nuove domande")
		End if
		rstFaq.Close 
  		set rstFaq = nothing
	End If	  
%> 
	<br> 
	<table width="100%" BORDER="0" CELLSPACING="2" CELLPADDING="1">
		<tr align="center">
			<td>
				<a class="textred" href="javascript:Invia('FAQ_DomandaBack.asp','<%=nIdCanale%>','<%=sArea%>','','NO','','')"><strong>Nuova Domanda</strong></a>
			</td>
			<td>
				<a class="textred" href="javascript:Invia('FAQ_ElencoBackR.asp','<%=nIdCanale%>','<%=sArea%>','','','','')"><strong>Domande con risposta</strong></a>
			</td>
		</tr>
	</table>
	<table border="0" cellspacing="0" cellpadding="0" width="100%" align="center">
		<tr> 
			<td align="left"> 
				<a class="textred" href="Faq_VisCanali.asp" onmouseover="window.status =' '; return true">
				<b>Lista Canali</b></a>
			</td>			
<%			If sArea = "P" Then		'private %>
				<td align="right"> 
					<a class="textred" href="javascript:Invia('FAQ_ElencoBack.asp','<%=nIdCanale%>','','','','','')" onmouseover="window.status =' '; return true">
					<b>Vai alle Faq Pubbliche </b></a>
				</td>			
<%			Else	'pubbliche %>

				<td align="right"> 
					<a class="textred" href="javascript:Invia('FAQ_ElencoBack.asp','<%=nIdCanale%>','P','','','','')" onmouseover="window.status =' '; return true">
					<b>Vai alle Faq Private </b></a>
				</td>
<%			End If %>
		</tr>
	</table>
	<form name="FrmFAQ" action method="post">
		<input type="hidden" name="IdCanale" value>
		<input type="hidden" name="sArea" value>
		<input type="hidden" name="R" value>
		<input type="hidden" name="pag" value>
		<input type="hidden" name="n" value>
		<input type="hidden" name="ID_FAQ" value>
		<input type="hidden" name="Page" value>
	</form>	
<%
End Sub

Sub Messaggio(Testo)
%>
<br>
<table WIDTH="100%" align="center" BORDER="0">
	<tr> 
		<td align="center" class="tbltext3"><%=Testo%></td>
	</tr>
	<tr> 
		<td align="center" class="tbltext3">&nbsp;</td>
	</tr>
	<tr>
		<td align="center">
			<a href="/pgm/formazione/faq/FAQ_VisCanali.asp"><img src="<%=Session("Progetto")%>/images/indietro.gif" border="0"></a>
		</td>
	</tr>
</table>	
<%
End Sub
%>




 
