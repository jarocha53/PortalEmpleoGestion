<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include Virtual="/strutt_testa2.asp"-->
<!--#include virtual="util/dbutil.asp"-->
<!--#include Virtual="/Util/DBUtil.asp"-->
<!--#include Virtual="/include/SysFunction.asp"-->
<!--#include Virtual="/include/openconn.asp"-->
<!--#include Virtual="/pgm/formazione/FAQ/FAQ_Intestazione.asp"-->
<%
Dim sModo
	
	sModo = Request.Form("Modo")
	'nIdCanale  = Request("IdCanale")
	
	If sModo = 1 Then
		InvioDomanda()
	Else
		ImpostaPag()
	End If
%>
<!--#include Virtual="/include/closeconn.asp"-->
<!--#include Virtual="/strutt_coda2.asp"-->

<script LANGUAGE="JavaScript" src="/Include/help.inc"></script>
<script LANGUAGE="JavaScript" src="/Include/ControlString.inc"></script>	
<script language="JavaScript">

function validateIt() {

	Valore = TRIM(document.FormDom.txtArgomento.value)
	if(Valore.length == 0)
	 {
	 document.FormDom.txtArgomento.value = "";
	 document.FormDom.txtArgomento.focus();
	 alert("Inserire l'Oggetto. Grazie");
	 return false;
	 } 

	if(document.FormDom.txtArgomento.value.length > 500)
	 {
	 document.FormDom.txtArgomento.focus();
	 alert("Attenzione!!!Hai superato la lunghezza massima di 500 caratteri nell'Oggetto.");
	 return false;
	 }
					
	 Valore = TRIM(document.FormDom.txtDomanda.value)
	 if(Valore.length == 0)
	 {
	 document.FormDom.txtDomanda.value = "";
	 document.FormDom.txtDomanda.focus();
	 alert("Inserire la Domanda. Grazie");
	 return false;
	 } 

	if(document.FormDom.txtDomanda.value.length > 2000)
	 {
	   document.FormDom.txtDomanda.focus();
	   alert("Attenzione!!!Hai superato la lunghezza massima di 2000 caratteri.");
	   return false;
	 }
	return true;			
 } 

</script>

<%
Sub ImpostaPag()
%>
<br><br>
<form action="FAQ_DomandaFront.asp" method="POST" name="FormDom" onsubmit="javascript:return validateIt()">
	<input type="hidden" name="modo" value="1">
	<input type="hidden" name="IdCanale" value="<%=nIdCanale%>">
    <table border="0" cellspacing="0" cellpadding="0" width="100%" align="center">
		<tr align="center"> 
			<td class="tbltext3">
				Usa il seguente form per inserire i tuoi messaggi 
                </b>
            </td>
		</tr>
	</table>
	<br>
			
	<table align="center" border="0" cellspacing="0" cellpadding="0" width="100%">
		<tr height="16"> 
			<td height="15" width="2%" valign="middle"><img src="<%=Session("Progetto")%>/images/righinaPSX.jpg"></td> 
			<td valign="middle" width="60%" align="left" class="tblsfondo3"><b class="tbltext0">&nbsp; Oggetto</b></td>
			<td width="36%" class="tblsfondo3">
				<span class="tbltext0">- Utilizzabile <b><label name="NumCaratteri0" id="NumCaratteri0">500</label></b> caratteri -</span></td>
			<td height="15" width="2%" valign="middle"><img src="<%=Session("Progetto")%>/images/righinaPDX.jpg"></td> 
		</tr>
		<tr> 
			<td width="2%" class="tbltext"> &nbsp; </td>
			<td colspan="2" align="left" width="96%" valign="middle"><textarea name="txtArgomento" onKeyup="JavaScript:CheckLenTextArea(document.FormDom.txtArgomento,NumCaratteri0,500)" CLASS="MyTextBox" cols="80" rows="15"></textarea><br></td>
			<td width="2%" class="tbltext"> &nbsp; </td>
		</tr>				
		<tr height="17"> 
			<td colspan="4">&nbsp;</td>
		</tr>
	</table>	
				
	<table align="center" border="0" cellspacing="0" cellpadding="0" width="100%">
		<tr height="17"> 
			<td height="2%" width="15" valign="middle"><img src="<%=Session("Progetto")%>/images/righinaPSX.jpg"></td> 
			<td valign="middle" width="60%" align="left" class="tblsfondo3"><b class="tbltext0">&nbsp; Testo del Messaggio</b></td>
			<td width="36%" class="tblsfondo3">
				<span class="tbltext0">- Utilizzabile <b><label name="NumCaratteri" id="NumCaratteri">2000</label></b> caratteri -</span></td>
			<td height="2%" width="15" valign="middle"><img src="<%=Session("Progetto")%>/images/righinaPDX.jpg"></td> 
		</tr>
		<tr> 
			<td width="2%" class="tbltext"> &nbsp; </td>
			<td colspan="2" align="left" width="96%" valign="middle"><textarea name="txtDomanda" onKeyup="JavaScript:CheckLenTextArea(document.FormDom.txtDomanda,NumCaratteri,2000)" CLASS="MyTextBox" cols="80" rows="15"></textarea><br></td>
			<td width="2%" class="tbltext"> &nbsp; </td>
		</tr>				
		<tr height="17"> 
			<td colspan="4">&nbsp;</td>
		</tr>
	</table>	
				
	<table border="0" align="center" cellspacing="0" cellpadding="0" width="100%">
		<tr> 
			<td colspan="2" align="center">
		        <input type="image" name="Invia" src="<%=Session("Progetto")%>/images/conferma.gif" border="0">
			</td>
		</tr>
	</table>

</form>
<form name="frmLista" method="post" action="/Pgm/formazione/Faq/FAQ_ListaFront.asp">
	<input type="hidden" name="IdCanale" value="<%=nIdCanale%>">
</form>
<table width="100%">
	<tr> 
		<td align="center"> 							
			<a class="textred" href="javascript:frmLista.submit()">
			<b>Torna alla lista delle domande</b></a>
		</td>
	</tr>
</table>
<br>
<%
End Sub

Sub InvioDomanda()
	Dim RR, sql
	Dim sArgomento, sDomanda, sProfilo

	sArgomento=Replace(Request.Form("txtArgomento"), "'", "''")
	sDomanda=Replace(Request.Form("txtDomanda"), "'", "''")
		

	If session("login") = "" Then
		sProfilo = ""	'FAQ PUBBLICA
	Else
		sProfilo = "P"	'FAQ PRIVATA
	End If
			
	sql = "INSERT INTO FAQ_UR (DATA,ARGOMENTO,DOMANDA,SITO,PROFILO, ID_FRM_AREA,IDUTENTE) VALUES(" & ConvDateToDB (date) & ",'" & sArgomento & "','" & sDomanda & "','Si','" & sProfilo & "','" & nIdCanale & "', '" & SESSION("idutente")&"')"
	sErrore=Esegui("FAQ","FAQ_UR","11","INS",SQL,1,"")
		
	If sErrore = "0" Then
		Messaggio("La tua domanda � stata inoltrata ")
	Else 
		Messaggio("Si � verificato un errore: " & sErrore)
		Indietro()
		exit sub
	End if 
%>		
<form name="frmLista" method="post" action="/Pgm/formazione/Faq/FAQ_ListaFront.asp">
	<input type="hidden" name="IdCanale" value="<%=nIdCanale%>">
</form>
<form name="frmDomanda" method="post" action="/Pgm/formazione/Faq/FAQ_DomandaFront.asp">
	<input type="hidden" name="IdCanale" value="<%=nIdCanale%>">
</form>
<table width="100%" cellpadding="0" cellspacing="0">
	<tr> 
		<td colspan="2" width="50%" align="center">
			<a class="textred" href="javascript:frmDomanda.submit()"><b>Invia un'altra domanda</b></a>
		</td>
		<td width="50%" height="17" align="center">
			<a class="textred" href="javascript:frmLista.submit()"><b>Torna alla lista delle domande</b></a>
		</td>
	</tr>
</table>
<br><br>
<%
End Sub

Sub Messaggio(Testo)
%>
<br><br>
<table WIDTH="100%" align="center" BORDER="0">
	<tr> 
		<td align="center" class="tbltext3"><%=Testo%></td>
	</tr>
</table>
<br><br>
<%
End Sub

Sub Indietro()
%>
<table WIDTH="100%" align="center" BORDER="0">
	<tr> 
		<td align="center">
			<a class="textred" href="javascript:history.back()"><b>Pagina precedente</b></a>
		</td>
	</tr>
</table>
<br><br>
<%
End Sub
%>
