<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include Virtual="/strutt_testa2.asp"-->
<!--#include Virtual="/include/openconn.asp"-->
<!--#include Virtual="/pgm/formazione/FAQ/FAQ_Intestazione.asp"-->
<script LANGUAGE="JavaScript" src="/Include/help.inc"></script>
<script LANGUAGE="JavaScript" src="/Include/ControlDateVB.asp"></script>
<script language="JavaScript">
function risp(idf,num,prg){

		f="/pgm/Formazione/Faq/FAQ_Risposta.asp?IDF="+idf+"&Num="+num+"&prg="+prg;
		window.open(f,'','toolbar=0,location=0,directories=0,status=0,menubar=no,scrollbars=yes,resizable=no,copyhistory=0,width=520,height=350,screenX=0,screenY=0');
}	
</script>
<%
	ImpostaPag()
%>	
<!--#include Virtual="/include/closeconn.asp"-->
<!--#include Virtual="/strutt_coda2.asp"-->
<%
Sub ImpostaPag()
	
	Dim sNew, nNum, nTot, nMemTot, nConta, sParola, sParolaU
	Dim sSitoA, sCodArea, sID, nTotale, nAppo
	Dim sData, sProfilo,sDataR
	Dim rstFaqNum, sqlFaqNum, sqlFaqUr, sWhere, area
		
	sNew = Request.Form("neww")
	nNum = Request.Form("numero")
	nMemTot = Request.Form("totaleg")
	sParola=Request.Form("termine")
	sParolaU=UCase(Replace(Request.Form("termine"), "'", "''"))
	sWhere = ""
		
	If session("login") = "" Then
		sWhere = " AND PROFILO is null"
	End If
		
	ntot = 0
	If sNew = "" Then
	    sqlFaqNum = "SELECT COUNT(id_faq) AS TOTALE " &_
					" FROM FAQ_UR " &_
					" WHERE (UPPER(DOMANDA) LIKE '%" & sParolaU & "%' OR UPPER(RISPOSTA) LIKE '%" & sParolaU & "%' OR UPPER(ARGOMENTO) LIKE '%" & sParolaU & "%' ) " &_ 
					" AND RISPOSTA IS NOT NULL " &_
					" " & sWhere '&_
					'" ORDER BY DATAR DESC"
		
		set rstFaqNum=Server.CreateObject("ADODB.Recordset")    
'PL-SQL * T-SQL  
SQLFAQNUM = TransformPLSQLToTSQL (SQLFAQNUM) 
	    Set rstFaqNum=CC.Execute(sqlFaqNum)
		nTot = CLng(rstFaqNum("totale"))
		nMemTot = nTot
		nNum = 1
		rstFaqNum.Close
		Set rstFaqNum=Nothing
	Else
	    nTot = Request.Form("totale")
	End If

%>		
	<br><br>
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td align="center">
				<b class="tbltext3">Risultati della Ricerca	(Domande trovate  <span class="tbltext1a"><%=nMemTot%></span>)</b>
			</td>
		</tr>
	</table>
	<br><br>
	<table cellspacing="0" width="100%">
		<tr> 
			<td colspan="2" align="center">
				<b class="tbltext1a">Parola cercata : </b><b class="tbltext1a">&quot;<% =server.HTMLEncode(sParola) %>&quot;</b>
			</td>
		</tr>
	</table>
	<br><br>

<%	If cint(nTot) = 0 Then %>
		<table cellspacing="0" width="100%">
			<tr> 
				<td colspan="2" align="center">
				<b class="tbltext1a">Nessuna domanda contiene la parola cercata</b></td>
			</tr>
		</table>
		<br><br>
<%	Else

		sqlFaqUr = "SELECT ID_FAQ, CODICEAREA, DATA, ARGOMENTO, DOMANDA, RISPOSTA, DATAR " &_ 
			       " FROM FAQ_UR " &_
		           " WHERE (UPPER(DOMANDA) LIKE '%" & sParolaU & "%' OR UPPER(RISPOSTA) LIKE '%" & sParolaU & "%' OR UPPER(ARGOMENTO) LIKE '%" & sParolaU & "%' ) " &_ 
		           " AND RISPOSTA IS NOT NULL " &_
		           " " & sWhere &_
		           " ORDER BY DATAR DESC"

		set rstFaqUr=Server.CreateObject("ADODB.Recordset")
'PL-SQL * T-SQL  
SQLFAQUR = TransformPLSQLToTSQL (SQLFAQUR) 
		rstFaqUr.open sqlFaqUr, CC, 1, 2
		If not rstFaqUr.EOF Then 
			nTotale = nTot
			rstFaqUr.MoveFirst 
			If nNum > 0 Then
				nAppo = 1
				Do While nAppo - nNum < 0
					rstFaqUr.MoveNext
					nAppo = nAppo + 1
				Loop
			End if
				
			Do Until rstFaqUr.EOF or nTot = nTotale - 10 or nTot = 0 
				'impostazione delle date nel formato gg/mm/aaaa       
				sData=ConvDateToString(rstFaqUr("Data"))                               
				sDataR=ConvDateToString(rstFaqUr("DataR"))                             
%>					
				<table width="100%" align="center" border="0" cellspacing="0" height="0" cellpadding="0">
					<tr>
						<td height="20" width="1%" valign="middle"><img SRC="<%=Session("Progetto")%>/images/righinaPSX.jpg"></td> 
						<td height="20" width="3%" align="center" valign="middle" class="tblsfondo3"><b class="tbltext0"><%=nNum%></td> 
						<td height="20" width="1%" valign="middle"><img SRC="<%=Session("Progetto")%>/images/righinaPDX.jpg"></td>
						<td height="20" width="95%" valign="middle" background="<%=Session("Progetto")%>/images/righina.jpg"></td>
					</tr>	  
					<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td width="95%">
							<b><a class="tbltext1" href="javascript:risp('<%=rstFaqUr("ID_FAQ")%>','<%=nNum%>','<%=session("Progetto")%>')"><%=server.HTMLEncode(rstFaqUr("argomento"))%></a></b>
						</td>
					</tr>	
					<tr valign="top">
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td class="tbltext" align="LEFT" width="95%" valign="top">
							<a class="tbltext" href="javascript:risp('<%=rstFaqUr("ID_FAQ")%>','<%=nNum%>','<%=session("Progetto")%>')"><% =server.HTMLEncode(rstFaqUr("domanda"))%>
						    <span class="tbltext1"> (<% =sData%>)</span>
							</a>
						</td>
					</tr>
				</table>
<%					
				sId = rstFaqUr("ID_FAQ")
				rstFaqUr.MoveNext 
				nTot = nTot - 1                        
				nNum = nNum + 1  
			Loop
						
			rstFaqUr.CLOSE
			Set rstFaqUr = nothing	
			If sNew > "" or nTot > 0 Then 
%>				<table width="100%" align="center">
					<tr> 
						<td class="tbltext3" colspan="2" width="100%" align="right"> 
<%						If sNew > "" Then %>
							<a href="javascript:history.back()"><b class="tbltext1">
							<img SRC="<%=Session("Progetto")%>/images/precedente.gif" border="0"></b></a>
<%						End If
						If nTot <> 0 Then
%>								 
							&nbsp; &nbsp; <a href="javascript:frmImpagina.submit()"><b class="tbltext1"><img SRC="<%=Session("Progetto")%>/images/successivo.gif" border="0"></b></a>
<%						End If %>
						</td>
					</tr>
				</table>
<%				
			End If
		Else
%>				
			<table width="100%">
				<tr>
					<td align="center" class="tbltext3">
						Non ci sono domande relative ai<br>
						parametri di ricerca inseriti
					</td>
				</tr>
			</table>
			<br><br>
<%					
		End If
	End If
	
%>		
	<br>
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<form name="frmDomanda" method="post" action="/Pgm/formazione/Faq/FAQ_DomandaFront.asp">
			<input type="hidden" name="IdCanale" value="<%=nIdCanale%>">
			<input type="hidden" name="Page" value="FAQ_DomandaFront">
		</form>
		<form name="frmRicerca" method="post" action="/Pgm/formazione/Faq/FAQ_RicercaFront.asp">
			<input type="hidden" name="IdCanale" value="<%=nIdCanale%>">
			<input type="hidden" name="Page" value="FAQ_RicercaFront">
		</form>
		<form name="frmLista" method="post" action="/Pgm/formazione/Faq/FAQ_ListaFront.asp">
			<input type="hidden" name="IdCanale" value="<%=nIdCanale%>">
			<input type="hidden" name="Page" value="FAQ_ListaFront">
		</form>
		<tr> 
			<td align="center"> 
				<a class="textred" href="javascript:frmDomanda.submit()">
				<b>Invia una nuova domanda</b></a>
			</td>
			<td align="center"> 
				<a class="textred" href="javascript:frmRicerca.submit()">
				<b>Nuova ricerca tra le domande</b></a>
			</td>
			<td align="center"> 							
				<a class="textred" href="javascript:frmLista.submit()">
				<b>Torna alla lista delle domande</b></a>
			</td>
		</tr>
	</table>
	<br><br>
		
	<form name="frmImpagina" method="post" action="/Pgm/Formazione/faq/FAQ_RisultatoFront.asp">
		<input type="hidden" name="neww" value="si">
		<input type="hidden" name="totale" value="<%=nTot%>">
		<input type="hidden" name="numero" value="<%=nNum%>">
		<input type="hidden" name="totaleg" value="<%=nMemTot%>">
		<input type="hidden" name="termine" value="<%=sParola%>">
	</form>
<%
End Sub
%>
