<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%	Response.ExpiresAbsolute=Now()-1
	Response.AddHeader "pragm","no-cache"
	Response.AddHeader "cache-control","private"
	Response.CacheControl = "no-cache"
%>
<script language="Javascript">
	
var windowArea
function ApriFinestra(NomePag,ElencoScelti,IdGruppo,CorDellaSez,IdSezPercForm,IdPercForm)
{	
	if (windowArea!=null)
	{
		windowArea.close();
	}
	windowArea=window.open(NomePag + '?UCorScel=' + ElencoScelti + '&UIdGruppo=' + IdGruppo + '&UCorDellaSez=' + CorDellaSez + '&UIdSezPercForm=' + IdSezPercForm + '&UIdPercForm=' + IdPercForm,'','toolbar=0,channelmode=0,location=0,menubar=0,status=0,width=550,height=450,top=0,left=0,Resize=No,Scrollbars=yes')
}
	
		
function ControllaSequenza(ObjSeqDaControllare)
{	var n,i
	var SequenzaCorretta//non serve � solo indicativa
	var indice
		
	//alert('Oggetto='+ObjSeqDaControllare)
	n=ObjSeqDaControllare.length
	if (n==null)
	{
		n=1
	}
	//alert('n=' + n)
		
	var aProgCor=new Array(n+1)
	//n+1 � il numero di elementi del vettore che quindi avr� idice da 0 a n
	//l'idea � inserire nel vettore in ogni csella indicata con il value del checkbox
	//il value stesso. se la sequenza � completa in ogni posizione a partire dalla 1
	//dovr� essserci il valore dell'indice stesso, se la sequenza non � completa rimmarr�
	//qualche casella vuota
		
	if (n==1)//un solo elemento in elenco
	{
		SequenzaCorretta=true
		indice=ObjSeqDaControllare.value
		if (indice!=1)
		{
			alert('Il progressivo deve essere 1.')
			ObjSeqDaControllare.value=1
			ObjSeqDaControllare.focus()
			SequenzaCorretta=false
			return false
		}
	}
		
		
	if (n>1)//+ di un elemnto in elenco
	{
		//alert(aProgCor.length)
		for (i=0;i<n;i++)
		{//ciclo sui txtProgr
			indice=ObjSeqDaControllare[i].value
			if (indice>n)
			{
				alert('Il progressivo non pu� essere maggiore di ' + n + '.')
				ObjSeqDaControllare[i].focus()
				return false
			}
			if (indice<1)
			{
				alert('Il progressivo deve essere compreso tra 1 e '+n+'.')
				ObjSeqDaControllare[i].focus()
				return false
			}
			aProgCor[indice]=indice
			//alert('aProgCor[' + indice +']=' + aProgCor[indice])
		}
		
		SequenzaCorretta=true
		for (i=1;i<aProgCor.length;i++)
		{	//alert(aProgCor[i])
			if (aProgCor[i]!=i)
			{
				SequenzaCorretta=false
				//alert('SequenzaCorretta=' + SequenzaCorretta)
				alert('Progressivo duplicato.')
				return false
			}
		}
	}
	//se n>1 quindi + di un elemnto in elenco
	//alert('SequenzaCorretta=' + SequenzaCorretta)
return true
}
//-----------------------------------------------------------------------------	
function ChiamaControllaSequenza()
{
	if (document.frmPercForma.txtProgr!=null)
	{
		if (!ControllaSequenza(document.frmPercForma.txtProgr)) return false
	}
//alert('seq corretta')
return true
}
//-----------------------------------------------------------------------------		
function Sottometti()
{
	if (ChiamaControllaSequenza())
	{
		document.frmPercForma.action="PFO_CnfModElePercForma.asp"
		document.frmPercForma.submit();
	}
}
</script>
<%
'-----------------------------------------------------------------------
Sub Inizio
	dim sFunzione
	dim sTitolo
	dim sCommento
	dim bCampiObbl
	
	sFunzione = "GESTIONE PERCORSI FORMATIVI"
	sTitolo = "PERCORSO FORMATIVO"
	sCommento = "Premere <b>Corsi</b> e <b>Questionari</b> per associare o eliminare i corsi e i questionari dal Percorso Formativo, " &_
				"quindi premere Invia per confermare la scelta."
	bCampiObbl = false
	sHelp = "/Pgm/help/Formazione/PercForma/PFO_ModElePercForma/"
%>
	<!--#include virtual="/include/SetTestata.asp"-->
<%

End Sub
'-----------------------------------------------------------------------
Sub ImpostaPag
	dim sSql,Sql
	dim sCorsiScelti,sQuestScelti
	dim sDescPercForm,sDescGruppo,sDescSezione
	dim iIdSezPercForm
	dim iIdPercForm
	dim sCorsiPresenti'viene utilizzato la 
	dim sQuestPresenti
	dim sPrimaVolta
	dim iIdGruppo
	dim iNumCor'conta i corsi scelti
	dim iNumQuest'conta i questionari scelti
	
	sDescPercForm=Request("hDescPercForm")
	sDescPercForm=Replace(sDescPercForm,"#","""")
	sDescGruppo=Request("hDescGruppo")
	iIdPercForm=Request.Form("hIdPercForm")
	iIdGruppo=Request.Form("hIdGruppo")
	
	sDescSezione=Request("hDescSezione")
	iIdSezPercForm=Request.Form("hIdSezPercForm")
	
	sPrimaVolta=Request.Form("hPrimaVolta")
	sCorsiPresenti=Request.Form("hCorsiPresenti")
	sQuestPresenti=Request.Form("hQuestPresenti")
	
	'Response.Write "sDescPercForm=" & sDescPercForm & "<br>"
	'Response.Write "sDescGruppo=" & sDescGruppo & "<br>"
	'Response.Write "iIdPercForm=" & iIdPercForm & "<br>"	
	'Response.Write "iIdGruppo=" & iIdGruppo & "<br>"	
	'Response.Write "iIdSezPercForm=" & iIdSezPercForm & "<br>"	
	'Response.Write "sPrimaVolta=" & sPrimaVolta & "<br>"	

	sCorsiScelti=Request("hCorsiScelti")
	'hCorsiScelti � un hidden di questo form che viene per�
	'inizializzato nella finestra window.open di Associa corsi

	sQuestScelti=Request("hQuestScelti")	
	'hQuestScelti � un hidden di questo form che viene per�
	'inizializzato nella finestra window.open di Associa Questionari
	
	'Response.Write "sCorsiPresenti=" & sCorsiPresenti & "<br>"	
	'Response.Write "sQuestPresenti=" & sQuestPresenti & "<br>"	

%>	<table border="0" width="500" cellspacing="6">
		<tr>
			<td class="tbltext1BFad" width="35%">Percorso Formativo
			</td>
			<td class="tbltextBFad" width="65%"><%=strHTMLEncode(sDescPercForm)%> 
			</td>
		</tr>
		<tr>
			<td class="tbltext1BFad">Gruppo
			</td>
			<td class="tbltextBFad"><%=strHTMLEncode(sDescGruppo)%> 
			</td>
		</tr>
	</table>
	<br>
<%	
	if (sPrimaVolta="true") then
		'seleziono i corsi gi� presenti e costruisco la stringa della sequenza di id
		
		Sql = " SELECT "
		Sql = Sql &  "   ID_ELEPERCFORM"
		Sql = Sql &  " FROM "
		Sql = Sql &  "  ELEMENTOPERCFORMATIVO"
		Sql = Sql &  " WHERE "
		Sql = Sql &  "   id_percformativo=" & iIdPercForm & " AND"
		Sql = Sql &  "   TIPO_ELEPERCFORM='C'"
		Sql = Sql &  " ORDER BY ID_ELEPERCFORM"
		'Response.Write "1- " & Sql & "<br><br>"
		
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
		Set Rst=CC.execute(Sql)
		do while not Rst.eof
			sCorsiPresenti=sCorsiPresenti & "," & Rst.fields("ID_ELEPERCFORM")
			Rst.MoveNext
		loop
		sCorsiPresenti=mid(sCorsiPresenti,2)
		'Response.Write "sCorsiPresenti prima volta=" & sCorsiPresenti & "<br>"
		sCorsiScelti=sCorsiPresenti
		Rst.close
		
		'seleziono i questionari gi� presenti  e costruisco la stringa della sequenza di id
		Sql = " SELECT "
		Sql = Sql &  "   ID_ELEPERCFORM"
		Sql = Sql &  " FROM "
		Sql = Sql &  "   ELEMENTOPERCFORMATIVO"
		Sql = Sql &  " WHERE "
		Sql = Sql &  "   id_percformativo=" & iIdPercForm & " AND"
		Sql = Sql &  "   TIPO_ELEPERCFORM='T'"
		Sql = Sql &  " ORDER BY ID_ELEPERCFORM"
		'Response.Write "2- " &Sql & "<br><br>"
		
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
		Set Rst=CC.execute(Sql)
		do while not Rst.eof
			sQuestPresenti=sQuestPresenti & "," & Rst.fields("ID_ELEPERCFORM")
			Rst.MoveNext
		loop
		sQuestPresenti=mid(sQuestPresenti,2)
		'Response.Write "sQuestPresenti prima volta=" & sQuestPresenti & "<br>"
		sQuestScelti=sQuestPresenti
		Rst.close
		set Rst=nothing
	end if
	
	'Response.Write "sCorsiScelti=" & sCorsiScelti & "<br>"
	'Response.Write "sQuestScelti=" & sQuestScelti & "<br>"	

%>	<table border="0" width="300">
	<tr>
		<td>
			<a href="JavaScript:ApriFinestra('PFO_ModCorPerForm.asp','<%=sCorsiScelti%>','<%=iIdGruppo%>','<%=sCorsiPresenti%>','<%=iIdSezPercForm%>','');" class="textred"><b>Corsi</b></a>
		</td>
		<td align="right">
			<a href="JavaScript:ApriFinestra('PFO_ModQuePerForm.asp','<%=sQuestScelti%>','<%=iIdGruppo%>','<%=sQuestPresenti%>','<%=iIdSezPercForm%>','<%=iIdPercForm%>')" class="textred"><b>Questionari</b></a>
		</td>
	</tr>
	</table>
	
	<br>
	<form name="frmPercForma" id="frmPercForma" method="post">
<%	'ATTENZIONE non inserire in questo form un action  perch� viene gestito 
	'dalle finestre window.open che in questa pagina vengono utilizzati
	
	iNumCor=0
	if sCorsiScelti<>"" then'se sono stati scelti dei corsi o ci sono gi� presenti

		'quelli presenti e scelti
		sSql="SELECT " &_
				"E.PROG_ELEPERCFORM,C.ID_CORSO,C.TIT_CORSO,C.DESC_CORSO," &_
				"E.FL_STATOELEPERCFORM" &_
			" FROM CORSO C,ELEMENTOPERCFORMATIVO E" &_
			" WHERE E.id_percformativo=" & iIdPercForm &_
			" AND E.TIPO_ELEPERCFORM='C'" &_
			" AND E.ID_ELEPERCFORM=C.ID_CORSO" &_
			" AND E.ID_ELEPERCFORM IN (" & sCorsiScelti & ")" &_
			" ORDER BY E.PROG_ELEPERCFORM,C.TIT_CORSO"
		'Response.Write "3- " & sSql & "<br><br>"

'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
		set RstPreSce=CC.execute(sSql)
		'con sql2 quelli scelti e non presenti
		'sql1 � quelli gi� presenti � la stessa di prima ma senza order
		sSql1="SELECT C.ID_CORSO" &_
			" FROM CORSO C,ELEMENTOPERCFORMATIVO E" &_
			" WHERE E.id_percformativo=" & iIdPercForm &_
			" AND E.TIPO_ELEPERCFORM='C'" &_
			" AND E.ID_ELEPERCFORM=C.ID_CORSO" &_
			" AND E.ID_ELEPERCFORM IN (" & sCorsiScelti & ")"
				
		sSql2="SELECT ID_CORSO,TIT_CORSO,DESC_CORSO " &_
			"FROM CORSO " &_
			"WHERE ID_CORSO IN (" & sCorsiScelti & ") " &_
			"AND ID_CORSO NOT IN (" & sSql1 & ") " &_ 
			"ORDER BY TIT_CORSO"
		'Response.Write "4- " & sSql2 & "<br>" 
'PL-SQL * T-SQL  
SSQL2 = TransformPLSQLToTSQL (SSQL2) 
		set RstSce=CC.execute(sSql2)
		
		if (not RstPreSce.eof) or (not RstSce.eof) then 'se ci sono corsi 
%>			<br>
			<table border="0" width="500">
				<tr class="sfondoCommBFad">
					<td colspan="4" align="center">
						<b>CORSI</b>
					</td>
				</tr>
				<tr class="sfondoCommBFad">
					<td width="1%">Progressivo</td>
					<td width="49%">Titolo</td>
					<td width="49%">Descrizione</td>
					<td width="1%">Stato elemento</td>
				</tr>
<%				Do while not RstPreSce.eof 
					iNumCor=iNumCor+1
%>					<tr class="tblSfondoFad">
						<td valign="middle" align="center">
							<input type="text" name="txtProgr" id="txtProgr" value="<%=RstPreSce.fields("PROG_ELEPERCFORM")%>" class="textblackFad" size="1" maxlength="2">
							<input type="hidden" name="hIdCorso" id="hIdCorso" value="<%=RstPreSce.fields("ID_CORSO")%>">
						</td>
						<td class="tblDettFad"><%=strHTMLEncode(RstPreSce("TIT_CORSO"))%>&nbsp;</td>
						<td class="tblDettFad"><%=strHTMLEncode(RstPreSce("DESC_CORSO"))%>&nbsp;</td>
						<td>
							<select name="cmbStatoCor" id="cmbStatoCor" class="textblackFad">
								<option value="N" <% if RstPreSce.fields("FL_STATOELEPERCFORM")="N" then Response.Write "selected"%>>Pu� non essere completato</option>
								<option value="C" <% if RstPreSce.fields("FL_STATOELEPERCFORM")="C" then Response.Write "selected"%>>Deve essere completato</option>
								<option value="P" <% if RstPreSce.fields("FL_STATOELEPERCFORM")="P" then Response.Write "selected"%>>Deve essere superato</option>
							</select>
						</td>
					</tr>
<%					RstPreSce.MoveNext
				loop
				Do while not RstSce.eof 
					iNumCor=iNumCor+1
%>					<tr class="tblSfondoFad">	
						<td valign="middle" align="center">
							<input type="text" name="txtProgr" id="txtProgr" value class="textblackFad" size="1" maxlength="2">
							<input type="hidden" name="hIdCorso" id="hIdCorso" value="<%=RstSce.fields("ID_CORSO")%>">
						</td>
						<td class="tblDettFad"><%=strHTMLEncode(RstSce("TIT_CORSO"))%>&nbsp;</td>
						<td class="tblDettFad"><%=strHTMLEncode(RstSce("DESC_CORSO"))%>&nbsp;</td>
						<td>
							<select name="cmbStatoCor" id="cmbStatoCor" class="textblackFad">
								<option value="N">Pu� non essere completato</option>
								<option value="C">Deve essere completato</option>
								<option value="P">Deve essere superato</option>
							</select>
						</td>
					</tr>
<%				RstSce.MoveNext	
				loop
%>			</table>
<%		end if'se ci sono corsi
	end if 'se sono stati scelti dei corsi o ci sono. sCorsiScelti<>""


	iNumQuest=0
	if sQuestScelti<>"" then 'se ci sono stati questionari selezionati o ci sono gi� presenti
		sSql="SELECT E.PROG_ELEPERCFORM,Q.ID_QUESTIONARIO,Q.TIT_QUESTIONARIO,E.FL_STATOELEPERCFORM" &_
			" FROM QUESTIONARIO Q,ELEMENTOPERCFORMATIVO E" &_
			" WHERE E.id_percformativo=" & iIdPercForm &_
			" AND E.TIPO_ELEPERCFORM='T'" &_
			" AND E.ID_ELEPERCFORM=Q.ID_QUESTIONARIO" &_
			" AND E.ID_ELEPERCFORM IN (" & sQuestScelti & ")" &_
			" ORDER BY E.PROG_ELEPERCFORM,Q.TIT_QUESTIONARIO"
		'Response.Write "5- " & sSql & "<br><br>"
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
		set RstPreSce=CC.execute(sSql)
			
		'con sql2 quelli scelti e non presenti
		'sql1 � quelli gi� presenti � la stessa di prima senza order
		sSql1="SELECT Q.ID_QUESTIONARIO" &_
			" FROM QUESTIONARIO Q,ELEMENTOPERCFORMATIVO E" &_
			" WHERE E.id_percformativo=" & iIdPercForm &_
			" AND E.TIPO_ELEPERCFORM='T'" &_
			" AND E.ID_ELEPERCFORM=Q.ID_QUESTIONARIO" &_
			" AND E.ID_ELEPERCFORM IN (" & sQuestScelti & ")"
			
		sSql2="SELECT ID_QUESTIONARIO,TIT_QUESTIONARIO " &_
			"FROM QUESTIONARIO " &_
			"WHERE ID_QUESTIONARIO IN (" & sQuestScelti & ") " &_
			"AND ID_QUESTIONARIO NOT IN (" & sSql1 & ") " &_ 
			"ORDER BY TIT_QUESTIONARIO"
		'Response.Write "6- " & sSql2 & "<br>"
'PL-SQL * T-SQL  
SSQL2 = TransformPLSQLToTSQL (SSQL2) 
		set RstSce=CC.execute(sSql2)

		if (not RstPreSce.eof) or (not RstSce.eof) then 'se ci sono questionari 
%>			<br>
			<table border="0" width="500">
				<tr class="sfondoCommBFad">
					<td colspan="3" align="center" class>
						<b>QUESTIONARI</b>
					</td>
				</tr>
				<tr class="sfondoCommBFad">
					<td width="1%">Progressivo</td>
					<td width="98%">Titolo</td>
					<td width="1%">Stato elemento</td>
				</tr>
<%				Do while not RstPreSce.eof
					iNumQuest=iNumQuest+1
%>					<tr class="tblSfondoFad">
						<td valign="middle" align="center">
							<input type="text" name="txtProgr" id="txtProgr" value="<%=RstPreSce.fields("PROG_ELEPERCFORM")%>" class="textblackFad" size="1" maxlength="2">
							<input type="hidden" name="hIdQuest" id="hIdQuest" value="<%=RstPreSce.fields("ID_QUESTIONARIO")%>">
						</td>
						<td class="tblDettFad">
							<%=strHTMLEncode(RstPreSce("TIT_QUESTIONARIO"))%>
						</td>
						<td>
							<select name="cmbStatoQuest" id="cmbStatoQuest" class="textblackFad">
								<option value="N" <% if RstPreSce.fields("FL_STATOELEPERCFORM")="N" then Response.Write "selected"%>>Pu� non essere completato</option>
								<option value="C" <% if RstPreSce.fields("FL_STATOELEPERCFORM")="C" then Response.Write "selected"%>>Deve essere completato</option>
								<option value="P" <% if RstPreSce.fields("FL_STATOELEPERCFORM")="P" then Response.Write "selected"%>>Deve essere superato</option>
							</select>
						</td>
					</tr>
<%					RstPreSce.MoveNext		
				loop
				Do while not RstSce.eof 
					iNumQuest=iNumQuest+1
%>					<tr class="tblSfondoFad">
						<td valign="middle" align="center">
							<input type="text" name="txtProgr" id="txtProgr" value class="textblackFad" size="1" maxlength="2">
							<input type="hidden" name="hIdQuest" id="hIdQuest" value="<%=RstSce.fields("ID_QUESTIONARIO")%>">
						</td>
						<td class="tblDettFad">
							<%=strHTMLEncode(RstSce("TIT_QUESTIONARIO"))%>
						</td>
						<td>
							<select name="cmbStatoQuest" id="cmbStatoQuest" class="textblackFad">
								<option value="N">Pu� non essere completato</option>
								<option value="C">Deve essere completato</option>
								<option value="P">Deve essere superato</option>
							</select>
						</td>
					</tr>
<%					RstSce.MoveNext		
				loop
%>			</table>
<%		end if' se la select per i questionari scelti dovesse dare erroneamente nessuna occorrenza
	end if'se ci sono stati questionari selezionati o ci sono. sQuestScelti<>""

	
	
%>	<input type="hidden" name="hCorsiScelti" id="hCorsiScelti" value="<%=sCorsiScelti%>">
	<input type="hidden" name="hQuestScelti" id="hQuestScelti" value="<%=sQuestScelti%>">

	<input type="hidden" name="hDescPercForm" id="hDescPercForm" value="<%=Replace(sDescPercForm,"""","#")%>">
	<input type="hidden" name="hDescGruppo" id="hDescGruppo" value="<%=sDescGruppo%>">
	<input type="hidden" name="hDescSezione" id="hDescSezione" value="<%=sDescSezione%>">
	<input type="hidden" name="hIdSezPercForm" id="hIdSezPercForm" value="<%=iIdSezPercForm%>">
	<input type="hidden" name="hIdPercForm" id="hIdPercForm" value="<%=iIdPercForm%>">
	<input type="hidden" name="hIdGruppo" id="hIdGruppo" value="<%=iIdGruppo%>">
	<input type="hidden" name="hCorsiPresenti" id="hCorsiPresenti" value="<%=sCorsiPresenti%>">
	<input type="hidden" name="hQuestPresenti" id="hQuestPresenti" value="<%=sQuestPresenti%>">
	<input type="hidden" name="hNumCor" id="hNumCor" value="<%=iNumCor%>">
	<input type="hidden" name="hNumQuest" id="hNumQuest" value="<%=iNumQuest%>">
	</form>
	
	<br><br>
	<table border="0" width="300">
		<tr>
			<form name="frmTornaIndietro" id="frmTornaIndietro" action="PFO_VisPercForma.asp" method="post">
			<td align="center">
				<a href="JavaScript:document.frmTornaIndietro.submit();">
					<img src="<%=session("progetto")%>/images/indietro.gif" border="0">
				</a>
			</td>
			</form>
			<td align="center">
				<a href="JavaScript:Sottometti();">
					<img src="<%=session("progetto")%>/images/conferma.gif" border="0" alt="Conferma">
				</a>
			</td>
		</tr>
	</table>	
<%	

End Sub
'------------------------------------------------------------------------
%>
<!--#include virtual="/strutt_testa2.asp"-->
<!--#include virtual="/include/openconn.asp"-->
<!--#include virtual="/include/HTMLEncode.asp"-->
<%	
	call Inizio
	call ImpostaPag
%>
<!--#include virtual="/include/closeconn.asp"-->
<!--#include virtual="/strutt_coda2.asp"-->
