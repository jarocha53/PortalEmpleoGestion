<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<script language="Javascript">
<!--#include virtual="/include/ControlString.inc"-->
<!--#include virtual="/include/ControlDate.inc"-->
<!--#include virtual="/include/ControlNum.inc"-->

function ControllaDati(dOdierna)
{	
	var dDisp
	document.frmPercForma.txtDescPercForm.value=TRIM(document.frmPercForma.txtDescPercForm.value)
	if (document.frmPercForma.txtDescPercForm.value=='')
	{
		alert('Il campo Descrizione � obbligatorio.')
		document.frmPercForma.txtDescPercForm.focus()
		return false
	}
	dDisp=document.frmPercForma.txtDataDisp.value
	if (!document.frmPercForma.chkDtDisp.checked)
	{	
		if (!ValidateInputDate(dDisp))
		{
			document.frmPercForma.txtDataDisp.focus()
			return false
		}
		if (!ValidateRangeDate(dOdierna,dDisp))
		{
			alert('La data di disponibilit� non pu� essere\nprecedente alla data odierna.')
			return false
		}
	}
	
return true
}
//---------------------------------------------------------------------------------------
function ControlloSuDataDisp(PercorsoHaElementi)
{

	if (!PercorsoHaElementi)
	{
		alert('Per rendere disponibile il Percorso � necessario\nassociargli almeno un elemento.')
		document.frmPercForma.chkDtDisp.checked=true
		/*if (confirm('Voui inserire ora gli elementi?'))
		{
			document.frmVaiAssociaElementi.submit()
		}*/
	}
	if (document.frmPercForma.chkDtDisp.checked)
	{
		document.frmPercForma.txtDataDisp.value=''
		document.frmPercForma.txtDataDisp.disabled=true
	}
	else
	{
		document.frmPercForma.txtDataDisp.disabled=false
		document.frmPercForma.txtDataDisp.focus()
	}
}
//-------------------------------------------------------------------------------------
function ControllaElimina(PresenzaElementi)
{
	//alert(PresenzaSezioni)
	if (PresenzaElementi)
	{	
		if (confirm('Sono presenti degli elementi.\nConfermi la cancellazione del Percorso Formativo?'))
		{
			return true
		}
		else
		{
			return false	
		}
	}
	else
	{
		if (confirm('Confermi la cancellazione del Percorso Formativo?'))
		{
			return true
		}
		else
		{
			return false	
		}
	}
return true
}
</script>
<%
'-----------------------------------------------------------------------
Sub Inizio
	dim sFunzione
	dim sTitolo
	dim sCommento
	dim bCampiObbl
	
	sFunzione = "GESTIONE PERCORSI FORMATIVI"
	sTitolo = "PERCORSO FORMATIVO"
	sCommento = "Inserire le informazioni e premere Invia per confermare le modifiche.<br>" &_
				"E` possibile rendere disponibile il Percorso Formativo solo dopo averlo completato."
	bCampiObbl = true
	sHelp = "/Pgm/help/Formazione/PercForma/PFO_ModDatPercForma/"
%>
	<!--#include virtual="/include/SetTestata.asp"-->
<%
End Sub
'-----------------------------------------------------------------------
function TrovaElementi(iIdPercForm2)
	'Verifica la presenza di eventuali ELEMENTI nel Percorso Formativo
	dim Sql
	Sql = " SELECT COUNT(*) AS NUM_ELE "
	Sql = Sql &  " FROM"
	Sql = Sql &  "   ELEMENTOPERCFORMATIVO E"
	Sql = Sql &  " WHERE "
	Sql = Sql &  "   E.id_percformativo=" & iIdPercForm2
	
 	'Response.Write Sql & "<br>"
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
 	Set RstTS=CC.execute(Sql)
 	if cstr(RstTS.fields("NUM_ELE"))<>cstr(0) then
 		TrovaElementi=true
 	else
 		TrovaElementi=false
 	end if
 	'Response.Write "NUM_ELE=" & RstTS.fields("NUM_ELE") & "<br>"
 	RstTS.Close
 	set RstTS=nothing
end function
'-----------------------------------------------------------------------
Sub ImpostaPag
	dim sCodRorg
	dim sSql
	dim iIdPercForm,iIdGruppo
	dim sDescPercForm,sDescGruppo
	dim dDtDisp,dDtDisp2
	dim bPercorsoHaElementi
	dim dOggi
	dim dtTmst
	dOggi=ConvDateToString(date())
	sCodRorg="DI"

	sDescPercForm=Request.Form("hDescPercForm")
	sDescPercForm=Replace(sDescPercForm,"#","""")
	iIdPercForm=Request.Form("hIdPercForm")
	sDescGruppo=Request.Form("hDescGruppo")
	iIdGruppo=Request.Form("hIdGruppo")
	dDtDisp=Request.Form("hDtDisp")
	dtTmst=Request.Form("hTmst")
	
	'Response.Write "sDescPercForm=" & sDescPercForm & "<br>"
	'Response.Write "iIdPercForm=" & iIdPercForm & "<br>"
	'Response.Write "sDescGruppo=" & sDescGruppo & "<br>"
	'Response.Write "iIdGruppo=" & iIdGruppo & "<br>"
	'Response.Write "dDtDisp=" & dDtDisp & "<br>"
	'Response.Write "dtTmst=" & dtTmst & "<br>"
	
	bPercorsoHaElementi=TrovaElementi(iIdPercForm)
	'Response.Write "bPercorsoHaElementi=" & bPercorsoHaElementi & "<br>"

	if (dDtDisp="non disponibile") then
		dDtDisp=""
	end if

%>	<form name="frmPercForma" id="frmPercForma" method="post" onsubmit="JavaScript:return ControllaDati('<%=dOggi%>');" action="PFO_CnfModDatPercForma.asp">
	<table border="0" width="500">
		<tr>
			<td class="tbltext1BFad" width="35%">Descrizione*
			</td>
			<td width="65%">
				<span class="tbltext1Fad">
					- Utilizzabili <label name="lblNumCar" id="lblNumCar"><%=1024-Len(sDescPercForm)%></label> caratteri -
				</span>
				<textarea name="txtDescPercForm" id="txtDescPercForm" class="textblackFad" rows="3" cols="40" style="text-transform:uppercase" OnKeyUp="JavaScript:CheckLenTextArea(document.frmPercForma.txtDescPercForm,lblNumCar,1024)"><%=strHTMLEncode(sDescPercForm)%></textarea>
			</td>
		</tr>
		<tr>
			<td class="tbltext1BFad">Gruppo*&nbsp;
			</td>
			<td class="tbltextBFad"><%=strHTMLEncode(sDescGruppo)%></td>
		</tr>
		<tr>
			<td class="tbltext1BFad">Disponibilit� dal&nbsp;
			</td>
			<td>
				<input type="text" name="txtDataDisp" id="txtDataDisp" value="<%=dDtDisp%>" <%if dDtDisp="" then Response.Write "disabled"%> class="textblackFad" size="12" maxlength="10">
				&nbsp;&nbsp;&nbsp;
				<input type="checkbox" name="chkDtDisp" id="chkDtDisp" <%if dDtDisp="" then Response.Write "checked"%> OnClick="JavaScript:ControlloSuDataDisp('<%=Lcase(bPercorsoHaElementi)%>');">
					<span class="tbltext1Fad">(non disponibile)</span>
			</td>
		</tr>
	</table>
	<br><br><br><br><br>
		
	<table border="0" width="250">
		<tr>
			<td align="center">
				<a href="JavaScript:history.back()"><img src="<%=session("progetto")%>/images/indietro.gif" border="0"></a>
			</td>
			<td align="center">
				<input type="image" src="<%=session("progetto")%>/images/conferma.gif">
			</td>
	<input type="hidden" name="hIdPercForm" id="hIdPercForm" value="<%=iIdPercForm%>">
	<input type="hidden" name="hIdGruppo" id="hIdGruppo" value="<%=iIdGruppo%>">
	<input type="hidden" name="hDescGruppo" id="hDescGruppo" value="<%=sDescGruppo%>">
	<input type="hidden" name="hTmst" id="hTmst" value="<%=dtTmst%>">
	</form>	
		
		<form name="frmElimPercForm" id="frmElimPercForm" onsubmit="JavaScript:return ControllaElimina('<%=Lcase(bPercorsoHaElementi)%>');" action="PFO_CnfElimPercForm.asp" method="post">
			<input type="hidden" name="hIdPercForm" id="hIdPercForm" value="<%=iIdPercForm%>">
			<input type="hidden" name="hIdGruppo" id="hIdGruppo" value="<%=iIdGruppo%>">
			<td align="center">
				<input type="image" src="<%=session("progetto")%>/images/elimina.gif" id="image1" name="image1">
			</td>
		</form>
		</tr>
	</table>
	
	<form name="frmVaiAssociaElementi" id="frmVaiAssociaElementi" action="PFO_ModElePercForma.asp" method="post">
		<input type="hidden" name="hIdPercForm" id="hIdPercForm" value="<%=iIdPercForm%>">
		<input type="hidden" name="hDescPercForm" id="hDescPercForm" value="<%=Replace(sDescPercForm,"""","#")%>">
		<input type="hidden" name="hDescGruppo" id="hDescGruppo" value="<%=sDescGruppo%>">
		<input type="hidden" name="hIdGruppo" id="hIdGruppo" value="<%=iIdGruppo%>">
		<input type="hidden" name="hPrimaVolta" id="hPrimaVolta" value="true">
	</form>
<%	
End Sub
'------------------------------------------------------------------------
%>
<!--#include virtual="/strutt_testa2.asp"-->
<!--#include virtual="/include/openconn.asp"-->
<!--#include virtual="/include/HTMLEncode.asp"-->
<!--#include virtual="/include/ControlDateVB.asp"-->
<center>
<%	
	call Inizio
	call ImpostaPag
%>
</center>
<!--#include virtual="/include/closeconn.asp"-->
<!--#include virtual="/strutt_coda2.asp"-->
