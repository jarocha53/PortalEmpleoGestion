<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%	Response.ExpiresAbsolute=Now()-1
	Response.AddHeader "pragm","no-cache"
	Response.AddHeader "cache-control","private"
	Response.CacheControl = "no-cache"
%>
<script language="Javascript">
</script>
<%

'-----------------------------------------------------------------------
Sub Inizio
	dim sFunzione
	dim sTitolo
	dim sCommento
	dim bCampiObbl
	
	sFunzione = "GESTIONE PERCORSI FORMATIVI"
	sTitolo = "PERCORSO FORMATIVO"
	sCommento = "Elenco delle sezioni nel percorso formativo.<br>" &_
				"E' inoltre possibile eliminare il Percorso Formativo."
	bCampiObbl = false

%>
	<!--#include virtual="/include/SetTestata.asp"-->
<%
End Sub
'-----------------------------------------------------------------------
sub MessaggioErrore(Errore)
%>	<br><br>
	<span class="tbltext3">
<%	if Errore<>"0" then
		Response.Write "Cancellazione non effettuata.<br>"
		Response.Write Errore
	end if
%>	</span><br>
<%
end sub
'-----------------------------------------------------------------------
Sub ImpostaPag
	dim sSql
	dim iIdPercForm
	dim iIdGruppo
	'CodErrore � globale
	iIdPercForm=Request.Form("hIdPercForm")
	iIdGruppo=Request.Form("hIdGruppo")
	
	'Response.Write "iIdPercForm=" & iIdPercForm & "<br>"
	'Response.Write "iIdGruppo=" & iIdGruppo & "<br>"
	'Response.End
	
	CC.BeginTrans
'-------------------------------------------------------------------------------------------
		'Cancella tutti i corsi e questionari eventualmente presenti su ELEMENTOPERCFORMATIVO 
		'relazionati al Percorso Formativo
		sSql="DELETE" &_
			" FROM ELEMENTOPERCFORMATIVO" &_
			" WHERE id_percformativo =" & iIdPercForm
		'Response.Write sSql & "<br><br>"
		CodErrore=EseguiNoC(sSql,CC)'Quando il DELETE pu� anche trovare nessun Record, non viene fatto controllo sul num di record cancellati
		if CodErrore<>"0" then
			call MessaggioErrore(CodErrore)
			exit sub
		end if
'-------------------------------------------------------------------------------------------
		'Cancella il Percorso Formativo da PERCORSOFORMATIVO
		sSql="DELETE" &_
			" FROM PERCORSOFORMATIVO" &_
			" WHERE ID_PERCFORMATIVO=" & iIdPercForm &_
			" AND IDGRUPPO=" & iIdGruppo
		'Response.Write sSql & "<br><br>"
		'CodErrore=EseguiNoC(sSql,CC)
		CodErrore=EseguiNoCTrace("","","","","","","DEL",sSQL,"0","",CC)'Quando il DELETE deve cancellare almeno un Record, viene fatto il controllo sul num di record cancellati se=0 significa che � stato cancellato da un altro utente
		if CodErrore<>"0" then
			call MessaggioErrore(CodErrore)
			exit sub
		end if
End Sub
'------------------------------------------------------------------------
sub ControlloErrore
	if CodErrore="0" then
		CC.CommitTrans
%>		<script>
			alert('Cancellazione correttamente effettuate.')
			location.href='PFO_VisPercForma.asp'
		</script>
<%	else
		CC.RollBackTrans
%>		<br><br><br><br>
		<a href='PFO_VisPercForma.asp'  class="textred">
			<b>Elenco Percorsi Formativi</b>
		</a>
<%	end if
end Sub
'--------------------------------------------------------------------------------
%>
<!--#include virtual="/strutt_testa2.asp"-->
<!--#include virtual="/include/SysFunction.asp"-->
<!--#include virtual="/include/OpenConn.asp"-->
<%	dim CodErrore
	call Inizio
	call ImpostaPag
	call ControlloErrore
%>
<!--#include virtual="/include/CloseConn.asp"-->
<!--#include virtual="/strutt_coda2.asp"-->
