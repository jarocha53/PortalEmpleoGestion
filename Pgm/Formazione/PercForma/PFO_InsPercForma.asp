<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<script language="Javascript">
<!--#include virtual="/include/ControlString.inc"-->
<!--#include virtual="/include/ControlDate.inc"-->
<!--#include virtual="/include/ControlNum.inc"-->

function ControllaDati()
{
	document.frmPercForma.txtDescPercForm.value=TRIM(document.frmPercForma.txtDescPercForm.value)
	if (document.frmPercForma.txtDescPercForm.value=='')
	{
		alert('Il campo Descrizione � obbligatorio.')
		document.frmPercForma.txtDescPercForm.focus()
		return false
	}
	if (!document.frmPercForma.chkDtDisp.checked)
	{	
		if (!ValidateInputDate(document.frmPercForma.txtDataDisp.value))
		{
			document.frmPercForma.txtDataDisp.focus()
			return false
		}
	}
	if (document.frmPercForma.cmbGruppo.selectedIndex==0)
	{
		alert('E` obbligatorio scegliere un gruppo.')
		document.frmPercForma.cmbGruppo.focus()
		return false
	}
return true
}
</script>
<%

'-----------------------------------------------------------------------
Sub Inizio
	dim sFunzione
	dim sTitolo
	dim sCommento
	dim bCampiObbl
	
	sFunzione = ""
	sTitolo = "PERCORSO FORMATIVO"
	sCommento = "Inserire le informazioni e premere Invia per confermare l'inserimeto.<br>" &_
				"E` possibile rendere disponibile il Percorso Formativo solo dopo averlo completato."
	bCampiObbl = true
	sHelp = "/Pgm/help/Formazione/PercForma/PFO_InsPercForma/"
%>
	<!--#include virtual="/include/SetTestata.asp"-->
<%

End Sub
'-----------------------------------------------------------------------
Sub ImpostaPag
	dim sCodRorg
	dim sSql
	sCodRorg="DI"
	
	sSql="SELECT IDGRUPPO,DESGRUPPO" &_
		" FROM GRUPPO" &_
		" WHERE COD_RUOFU='DI'" &_
		" AND IDGRUPPO NOT IN" &_
		" (SELECT IDGRUPPO FROM PERCORSOFORMATIVO)" &_
		" ORDER BY DESGRUPPO"
	'Response.Write sSql & "<br>"

'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	set RstG=CC.execute (sSql)
	if not RstG.eof then'se ci sono gruppi
%>		<br>
		<form name="frmPercForma" id="frmPercForma" method="post" onsubmit="JavaScript:return ControllaDati();" action="PFO_CnfInsPercForma.asp">
		<table border="0" width="500">
			<tr width="35%">
				<td class="tbltext1BFad">Descrizione*
				</td>
				<td width="65%">
					<span class="tbltext1Fad">
						- Utilizzabili <label name="lblNumCar" id="lblNumCar">1024</label> caratteri -
					</span>
					<textarea name="txtDescPercForm" id="txtDescPercForm" class="textblackFad" rows="3" cols="40" style="text-transform:uppercase" OnKeyUp="JavaScript:CheckLenTextArea(document.frmPercForma.txtDescPercForm,lblNumCar,1024)"></textarea>
				</td>
			</tr>
			<tr>
				<td class="tbltext1BFad">Gruppo*&nbsp;
				</td>
				<td>
					<select id="cmbGruppo" name="cmbGruppo" class="textblackFad">
						<option value></option>
<%						do while not RstG.eof
%>							<option value="<%=RstG("IDGRUPPO")%>"><%=strHTMLEncode(RstG("DESGRUPPO"))%></option>
<%						RstG.MoveNext
						loop
%>					</select>
				</td>
			</tr>
			<tr>
				<td class="tbltext1BFad">Disponibilit� dal&nbsp;
				</td>
				<td>
					<input type="text" name="txtDataDisp" id="txtDataDisp" class="textblackFad" size="12" maxlength="10" disabled>
					&nbsp;&nbsp;&nbsp;
					<input type="checkbox" name="chkDtDisp" id="chkDtDisp" checked OnClick="JavaScript:alert('E` possibile rendere disponibile il Percorso Formativo\nsolo dopo averlo completato.');document.frmPercForma.chkDtDisp.checked=true;">
					<span class="tbltext1Fad">(non disponibile)</span>
				</td>
			</tr>
		</table>
		<br><br><br><br><br><br>
		<table border="0" width="250">
		<tr>
			<td align="center">
				<a href="JavaScript:history.back()"><img src="<%=session("progetto")%>/images/indietro.gif" border="0"></a>
			</td>
			<td align="center">
				<input type="image" src="<%=session("progetto")%>/images/conferma.gif">
			</td>
		</tr>
		</table>
		</form>
<%	else%>
		<br><br><br>
		<span class="tbltext3">
			Non vi sono gruppi oppure a quelli esistenti<br>
			gi� � stato assegnato un percorso formativo.
		</span><br><br><br><br>
		<a href="JavaScript:history.back()"><img src="<%=session("progetto")%>/images/indietro.gif" border="0"></a>
<%	end if 'se ci sono gruppi
	
	RstG.close
	set RstG=nothing
End Sub
'------------------------------------------------------------------------
%>
<!--#include virtual="/strutt_testa2.asp"-->
<!--#include virtual="/include/openconn.asp"-->
<!--#include virtual="/include/HTMLEncode.asp"-->
<center>
<%	
	call Inizio
	call ImpostaPag
%>
</center>
<!--#include virtual="/include/closeconn.asp"-->
<!--#include virtual="/strutt_coda2.asp"-->
