<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<script LANGUAGE="JavaScript">
	function Dettagli(idPercForm)
	{
		windowArea = window.open ('PFO_VisDettagli.asp?id=' + idPercForm + '&link=N','info','width=400,height=500,scrollbars=1')
	}
</script>
<%

'-------------------------------------------------------------------------------
Sub Inizio
	dim sFunzione
	dim sTitolo
	dim sCommento
	dim bCampiObbl
	
	sFunzione = ""
	sTitolo = "PERCORSO FORMATIVO"
	sCommento = "Elenco dei percorsi formativi del progetto.<br>" &_
			"Clickando sul titolo del percorso si attiva la pagina di modifica dei dati del percorso; " &_
			"facendo click sull'icona posta a sinistra della descrizione viene visualizzata la composizione del percorso formativo; " &_
			"per modificare la struttura del percorso formativo clickare sull'icona di destra."
	bCampiObbl = false
	sHelp = "/Pgm/help/Formazione/PercForma/PFO_VisPercForma/"
%>
	<!--#include virtual="/include/SetTestata.asp"-->
<%
End sub
'-------------------------------------------------------------------------------
Sub ImpostaPag
	dim sSql,Sql
	dim dData,sData
	
	Sql = " SELECT P.ID_PERCFORMATIVO, P.DESC_PERCFORMATIVO, P.DT_TMST, G.DESGRUPPO, G.IDGRUPPO,"
	Sql = Sql &  " decode(P.DT_DISPONIBILE, to_date('31/12/9999','dd/mm/yyyy'),"
	Sql = Sql &  " 'non disponibile',P.DT_DISPONIBILE) AS DT_DISPONIBILE"
	Sql = Sql &  " FROM PERCORSOFORMATIVO P, GRUPPO G"
	Sql = Sql &  " WHERE P.IDGRUPPO=G.IDGRUPPO "
	Sql = Sql &  " ORDER BY P.DESC_PERCFORMATIVO"
	
	'Sql = Sql &  "WHERE P.IDGRUPPO=G.IDGRUPPO (+)
	'eliminato malfunzionamento visualizzazione percorsoformativo
	
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
	set Rst=CC.execute (Sql)
	if not Rst.eof then
%>	
		<table border="0" width="500">
			<tr class="SfondoCommBFad">
				<td width="1%">Struttura</td>
				<td>Descrizione
				</td>
				<td>Gruppo
				</td>
				<td width="1%">&nbsp;Disponibile<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;dal
				</td>
				<td width="1%">&nbsp;&nbsp;Associa<br>&nbsp;Elementi</td>
			</tr>
<%		i=0
		do while not Rst.eof
			i=i+1
%>			<tr class="tblSfondoFad">
				<td align="center">
					<a title="Visualizza dettagli percorso formativo" onmouseover="javascript:status='' ; return true" href="Javascript:Dettagli(<%=Rst.fields("ID_PERCFORMATIVO")%>)">
					<img border="0" src="<%=Session("progetto")%>/images/bullet.gif"></a>
				</td>
				<form name="frmPercForm<%=i%>" id="frmPercForm<%=i%>" action="PFO_ModElePercForma.asp" method="post">
					<td>
						<a class="tblAggFad" href="JavaScript:document.frmPercForm<%=i%>.action='PFO_ModDatPercForma.asp';document.frmPercForm<%=i%>.submit();">
							<%=strHTMLEncode(Rst.fields("DESC_PERCFORMATIVO"))%>
						</a>
					</td>
					<td class="tblDettFad"><%=strHTMLEncode(Rst.fields("DESGRUPPO"))%>
					</td>
					<input type="hidden" name="hIdPercForm" id="hIdPercForm" value="<%=Rst.fields("ID_PERCFORMATIVO")%>">
					<input type="hidden" name="hDescPercForm" id="hDescPercForm" value="<%=Replace(Rst.fields("DESC_PERCFORMATIVO"),"""","#")%>">
					<input type="hidden" name="hDescGruppo" id="hDescGruppo" value="<%=Rst.fields("DESGRUPPO")%>">
					<input type="hidden" name="hIdGruppo" id="hIdGruppo" value="<%=Rst.fields("IDGRUPPO")%>">
					<input type="hidden" name="hPrimaVolta" id="hPrimaVolta" value="true">
					<input type="hidden" name="hTmst" id="hTmst" value="<%=Rst.fields("DT_TMST")%>">
					<td class="tblDettFad">
<%						dData=Rst.fields("DT_DISPONIBILE")
						if isdate(dData) then
							sData=ConvDateToString(dData)
						else
							sData=dData'scriver� "non disponibile"
						end if
						if sData="non disponibile" then
							Response.Write "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-"
						else
							Response.Write sData
						end if
%>					<input type="hidden" name="hDtDisp" id="hDtDisp" value="<%=sData%>">
					</td>
					<td align="center">
						<input type="image" src="<%=session("progetto")%>/images/re.gif" title="Definizione percorso formativo">
					</td>
				</form>
			</tr>
<%			Rst.MoveNext
		loop
%>		</table>
<%	else
%>		<span class="tbltext3">
			Non ci sono percorsi formativi
		</span>
<%	end if
	Rst.Close
	set Rst=nothing	
	
%>	<br><br>
	<table border="0">
		<tr>
			<td>
				<a href="PFO_InsPercForma.asp" class="textred"><b>Crea percorso formativo</b></a>
			</td>
		</tr>
	</table>
<%
end sub

'-------------------------------------------------------------------------------------
'M A I N
%>
<!--#include virtual="/strutt_testa2.asp"-->
<!--#include virtual="/include/openconn.asp"-->
<!--#include virtual="/include/HTMLEncode.asp"-->
<!--#include virtual="/include/ControlDateVB.asp"-->
<center>
<%
	call Inizio
	call ImpostaPag
%>
</center>
<!--#include virtual="/include/closeconn.asp"-->
<!--#include virtual="/strutt_coda2.asp"-->
