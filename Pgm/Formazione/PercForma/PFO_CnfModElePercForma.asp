<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<script language="Javascript">
</script>
<%

'-----------------------------------------------------------------------
Sub Inizio
	dim sFunzione
	dim sTitolo
	dim sCommento
	dim bCampiObbl
	
	sFunzione = "GESTIONE PERCORSI FORMATIVI"
	sTitolo = "PERCORSO FORMATIVO"
	sCommento = "Premere <b>Corsi</b> e <b>Questionari</b> per associare o eliminare i corsi e i questionari al Percorso Formativo, " &_
				"quindi premere Invia per confermare la scelta."
	bCampiObbl = false
%>
	<!--#include virtual="/include/SetTestata.asp"-->
<%
End Sub
'-----------------------------------------------------------------------
sub MessaggioErrore(Errore)
%>	<br><br>
	<span class="tbltext3">
<%	if Errore<>"0" then
		Response.Write "Modifica non effettuata.<br>"
		Response.Write Errore
	end if
%>	</span><br>
<%
end sub
'-----------------------------------------------------------------------
Sub ImpostaPag
	dim aIdCorsi,sIdCorsi'Stringa contenente gli Id dei corsi "hIdCorso" � un vettore
	dim aProgr,sProgr'Stringa contenente il progresivo di ci� che � stato scelto
	dim aProgrCors,sProgrCor'Stringa contenente il progresivo per i corsi
	dim aStatoCor,sStatoCor'Stringa contenente lo stato del rispettivo corso. "cmbStatoCor" � un vettore
	dim aIdQuest,sIdQuest'Stringa contenente gli Id dei questionari "hIdQuest" � un vettore
	dim aProgrQuest,sProgrQuest'Stringa contenente il progresivo per i corsi
	dim aStatoQuest,sStatoQuest'Stringa contenente lo stato del rispettivo questionario. "cmbStatoQuest" � un vettore
	dim sSql,dtOra,sdtOra
	dim sIdSezPercForm'id della Sezione del Percorso Formativo
	dim iIdPercForm
	dim sDescPercForm,sDescGruppo
	dim iNumCor,iNumQuest
	
	sProgr=Request.Form("txtProgr")
	
	'sProgrCor=Request.Form("txtProgrCor")
	'sProgrQuest=Request.Form("txtProgrQuest")

	sIdCorsi=Request.Form("hIdCorso")
	sIdQuest=Request.Form("hIdQuest")
	sIdSezPercForm=Request.Form("hIdSezPercForm")
	iIdPercForm=Request.Form("hIdPercForm")
	sDescPercForm=Request.Form("hDescPercForm")
	sDescGruppo=Request.Form("hDescGruppo")
	sStatoCor=Request.Form("cmbStatoCor")
	sStatoQuest=Request.Form("cmbStatoQuest")
	sIdGruppo=Request.Form("hIdGruppo")
	iNumCor=Request.Form("hNumCor")
	iNumQuest=Request.Form("hNumQuest")
	

	'Response.Write "sProgr=" & sProgr & "<br>"
	'Response.Write "sIdCorsi=" & sIdCorsi & "<br>"
	'Response.Write "sIdQuest=" & sIdQuest & "<br>"
	'Response.Write "sIdSezPercForm=" & sIdSezPercForm & "<br>"
	'Response.Write "iIdPercForm=" & iIdPercForm & "<br>"
	'Response.Write "sDescPercForm=" & sDescPercForm & "<br>"
	'Response.Write "sDescGruppo=" & sDescGruppo & "<br>"
	'Response.Write "sStatoCor=" & sStatoCor & "<br>"
	'Response.Write "sStatoQuest=" & sStatoQuest & "<br>"
	'Response.Write "sIdGruppo=" & sIdGruppo & "<br>"
	'Response.Write "iNumCor=" & iNumCor & "<br>"
	'Response.Write "iNumQuest=" & iNumQuest & "<br>"
	
	aProgr=split(sProgr,", ")
	'for i=0 to ubound(aProgr)
		'Response.Write "*" & aProgr(i) & "*"
	'next
	'Response.Write "<br>"
	
	sProgrCor=""
	for i=0 to (iNumCor-1)
		sProgrCor=sProgrCor & ", " &  aProgr(i)
	next
	sProgrCor=mid(sProgrCor,3)
	
	sProgrQuest=""
	for i=iNumCor to ubound(aProgr)
		sProgrQuest=sProgrQuest & ", " &  aProgr(i)
	next
	sProgrQuest=mid(sProgrQuest,3)
	
	'Response.Write "sProgrCor=" & sProgrCor & "<br>"
	'Response.Write "sProgrQuest=" & sProgrQuest & "<br>"
	
	CC.BeginTrans
	
		dtOra=Now()
		sdtOra=ConvDateToDB(dtOra)
'-------------------------------------------------------------------------------------------
		sSql="DELETE" &_
			" FROM ELEMENTOPERCFORMATIVO" &_
			" WHERE id_percformativo=" & iIdPercForm
			'Response.Write "1- " & sSql & "<br><br>"
			CodErrore=EseguiNoC(sSql,CC)
			if CodErrore<>"0" then exit sub
'-------------------------------------------------------------------------------------------
		aIdCorsi=split(sIdCorsi,", ")
		aProgrCors=split(sProgrCor,", ")
		aStatoCor=split(sStatoCor,", ")
		for i=0 to ubound(aProgrCors)
			'Response.Write "aIdCorsi=" & aIdCorsi(i) & " aProgrCors=" & aProgrCors(i) &"<br>"
			sSql="INSERT INTO ELEMENTOPERCFORMATIVO" &_
				" (ID_ELEPERCFORM,id_percformativo,TIPO_ELEPERCFORM," &_
				"PROG_ELEPERCFORM, FL_STATOELEPERCFORM, DT_TMST)" &_
				" VALUES" &_
				" (" & aIdCorsi(i) & "," & iIdPercForm & ",'" & "C" & "'," &_
				aProgrCors(i) & ",'" & aStatoCor(i) & "'," & sdtOra &")"
			'Response.Write "2- " & sSql & "<br><br>"
			CodErrore=EseguiNoCTrace("","","","","","","INS",sSql,"0","",CC)
			if CodErrore<>"0" then exit sub
		next
'-------------------------------------------------------------------------------------------
		aIdQuest=split(sIdQuest,", ")
		aProgrQuest=split(sProgrQuest,", ")
		aStatoQuest=split(sStatoQuest,", ")
		for i=0 to ubound(aProgrQuest)
			'Response.Write "aIdQuest=" & aIdQuest(i) & " aProgrQuest=" & aProgrQuest(i) &"<br>"
			sSql="INSERT INTO ELEMENTOPERCFORMATIVO" &_
				" (ID_ELEPERCFORM,id_percformativo,TIPO_ELEPERCFORM," &_
				"PROG_ELEPERCFORM, FL_STATOELEPERCFORM, DT_TMST)" &_
				" VALUES" &_
				" (" & aIdQuest(i) & "," & iIdPercForm & ",'" & "T" & "'," &_
				aProgrQuest(i) & ",'" & aStatoQuest(i) & "'," & sdtOra &")"
			'Response.Write "3- " & sSql & "<br><br>"
			CodErrore=EseguiNoCTrace("","","","","","","INS",sSql,"0","",CC)
		next
End Sub
'------------------------------------------------------------------------
sub ControlloErrore
	if CodErrore="0" then
		CC.CommitTrans
%>		<br><br><br>
		<font class="tbltext3">
			Modifiche correttamente effettuate.
		</font>
<%	else
		CC.RollBackTrans
		call MessaggioErrore(CodErrore)
	end if
%>	<br><br><br><br>
	<a href='PFO_VisPercForma.asp'  class="textred">
		<b>Elenco Percorsi Formativi</b>
	</a>
<%	
end Sub
'------------------------------------------------------------------------
%>
<!--#include virtual="/strutt_testa2.asp"-->
<!--#include virtual="/include/OpenConn.asp"-->
<!--#include virtual="/util/dbutil.asp"-->
<!--#include virtual="/include/SysFunction.asp"-->
<%	dim CodErrore
	call Inizio
	call ImpostaPag
	call ControlloErrore
%>
<!--#include virtual="/include/CloseConn.asp"-->
<!--#include virtual="/strutt_coda2.asp"-->
