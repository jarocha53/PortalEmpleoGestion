<%@LANGUAGE = JScript %>

<!-- #include VIRTUAL="M3Migracion/migracion2.asp" -->
<!--#INCLUDE file = "include/OpenConnJSSqlServer.asp"-->
<%
// Imposta il numero di messaggi per pagina
var quanti_per_pagina = 5;

function messaggio(id,livello,data,autore,oggetto) {
  // Oggetto "messaggio"
  //Response.Write("oggetto=" + oggetto + "<br>");
  //Response.Write ("id=" + id + "<br>");
  //Response.Write ("liv=" + livello + "<br>");
  //Response.Write ("aut=" + autore + "<br>");
  //if (oggetto.length>60-(livello*2))
  //  oggetto = oggetto.substring(0,57-(livello*2)) + "...";
  this.id = id;
  this.livello = livello;
  this.data = new Date(data);
  this.autore = autore;
  this.oggetto = oggetto;
}

var lista = new Array();
var i_ris = new Array();
var il = 0;
var conta = 0;
var last = 0;
var appo = "NO";
var RespName = " ";

function contaRisp(idRisp)	{
		
		var sqlRisp
		var conta
		
		sqlRisp = "SELECT COUNT(ID_FRM_RISPOSTA)FROM FRM_RISPOSTA WHERE ID_RISPOSTA =" + idRisp;

//SM 24/08/2007 inizio		
%>
<script language="VBScript" runat="server"> 		
'PL-SQL 2 T-SQL  
sqlRisp = TransformPLSQLToTSQL (sqlRisp)
 </script>
 <%  
//SM  fine

		conta = CC.Execute(sqlRisp);
				
		if (parseInt(conta[0])!=0) return true;
		
		return false;				
}	

function leggiMessaggi(st,rt,liv) {
		
  // Funzione ricorsiva di lettura
  sql = "SELECT	ID_FRM_RISPOSTA, OGGETTO, RISPOSTA, DT_TMST, MITTENTE " 
  sql += " FROM FRM_RISPOSTA"
  sql += " WHERE ID_FRM_DOMANDA = " + qt + " AND ID_RISPOSTA=" + rt
   
  if (st>0){ 
	sql += " AND ID_FRM_RISPOSTA < " + st;
  }
  sql += " ORDER BY DT_TMST";
  if (liv==0){ 
	sql += " DESC";
  }
 
//SM 24/08/2007 inizio		
%>
<script language="VBScript" runat="server"> 		
'PL-SQL * T-SQL  
sql = TransformPLSQLToTSQL (sql)
 </script>
 <% 
 //SM fine
 
  i_ris[liv] = CC.Execute(sql);
  
  while (!i_ris[liv].EOF) 
	{
		lista[il++] = new messaggio(parseInt(i_ris[liv]("ID_FRM_RISPOSTA")),
                                liv,
                                String(i_ris[liv]("DT_TMST")),                          
                                String(i_ris[liv]("MITTENTE")),
                                String(i_ris[liv]("OGGETTO")));                                                  
		if (contaRisp(parseInt(i_ris[liv]("ID_FRM_RISPOSTA")))) leggiMessaggi(0,parseInt(i_ris[liv]("ID_FRM_RISPOSTA")),liv+1);
		if (liv==0)
		{
			conta++;
			if (conta==quanti_per_pagina) 
			{
			  last = parseInt(i_ris[liv]("ID_FRM_RISPOSTA"))
			  return;
			}
		}
		i_ris[liv].MoveNext();
	}
}
// Leggi i dati per la paginazione
var s = parseInt(Request("s"));

// Categoria del Forum
var cat = Request.Form("IdCanale");

// Id della Domanda
var qt = Request("qt");

if (isNaN(s) || s<0) s = 0;

if (Request("modo")=="del"){
		var sqlCanc
		sqlCanc = "DELETE FRM_RISPOSTA WHERE ID_FRM_RISPOSTA=" + Request("id") + " OR ID_RISPOSTA=" + Request("id")

//SM 24/08/2007 inizio
%>
<script language="VBScript" runat="server"> 		
'PL-SQL * T-SQL  
sqlCanc = TransformPLSQLToTSQL (sqlCanc)
 </script>
 <% 
 //SM fine 

		CC.Execute(sqlCanc);	
} 			

// Porta i messaggi interessati dal database all'Array lista
leggiMessaggi(s, 0, 0);

if ((conta==quanti_per_pagina) && !i_ris[0].EOF) {
		i_ris[0].MoveNext();
		if (!i_ris[0].EOF)	appo="SI"; 
	}
%>

<script language="JavaScript" src="/Include/help.inc"></script>
<script language="JavaScript">
function modifica(cat,can,ris) {	
	document.frmMod.mod.value= cat;
	document.frmMod.IdCanale.value= cat;
	document.frmMod.mod1.value= can;
	document.frmMod.mod2.value= ris;
	document.frmMod.submit();
}
	
function elimina(cat,can,id)
{	
	if (confirm("Conferma l'eliminazione della risposta e di tutte le altre risposte associate?")) 
		{
		document.frmCancella.cat.value = cat;
		document.frmCancella.IdCanale.value= cat;
		document.frmCancella.can.value = can;
		document.frmCancella.id.value = id;
		document.frmCancella.submit();
		}
}

function mostra(qt, id, cat) {
  	
  window.open("FOR_VisRisposte.asp?cat=" + cat + "&id=" + id + "&qt=" + qt + "#" + id,"visualizza","width=405,height=250,scrollbars=yes");
}

function sAlbero(num,qt)
{
	document.frmAlbero.id.value=""
	document.frmAlbero.modo.value=""
	document.frmAlbero.s.value=num
	document.frmAlbero.qt.value=qt
	document.frmAlbero.submit();
}
</script>

<%
//---------------------------------MESSAGGIO ORIGINALE------------------------
   
	sqlDom = "SELECT FRM_DOMANDA.DOMANDA, FRM_DOMANDA.OGGETTO, FRM_DOMANDA.MITTENTE,"
	
	//SM 24/08/2007 inizio
	
	// to_char (ORACLE) --> convert (SQLServer)
	//sqlDom += " TO_CHAR(FRM_DOMANDA.DT_TMST,'DD/MM/YYYY HH24:MI:SS') AS DATA"
	sqlDom += " CONVERT(datetime, FRM_DOMANDA.DT_TMST) AS DATA"
	
	//SM fine
	
	sqlDom += " FROM FRM_DOMANDA"
	sqlDom += " WHERE FRM_DOMANDA.ID_FRM_DOMANDA=" + qt 

	RRDom  = Server.createObject("adodb.recordset");
	
//SM 24/08/2007 inizio
%>
<script language="VBScript" runat="server"> 		
'PL-SQL * T-SQL  
sqlDom = TransformPLSQLToTSQL (sqlDom)
 </script>
 <% 
 //SM fine 
 
	RRDom.Open(sqlDom, CC);

	if (!RRDom.eof){ 		 
			RespName = RRDom.Fields("MITTENTE")
	}	
%> 
<!--Epili 13/01/02 Per poter utilizzare le strutture standard.-->
<script language="VBScript" runat="server"> 
'PL-SQL * T-SQL  
	Server.Execute ("/strutt_testa1.asp") 
</script>

<center>
 <br>
<table border="0" width="740" cellspacing="0" cellpadding="0" height="73">
  <tr>
    <td width="700" background="<%=Session("Progetto")%>/images/titoli/Community1b.gif" height="73" valign="bottom" align="right">
      <table border="0" background width="600" height="30" cellspacing="0" cellpadding="0">
        <tr>
          <td width="100%" valign="top" align="right"><b class="tbltext1">Forum di discussione <i><%= Session("Category") %></i></b>
           <a href="Javascript:Show_Help('/Pgm/help/Formazione/Forum/FOR_Albero')" onmouseover="javascript:status='' ; return true">
  			<img align="right" src="<%=Session("Progetto")%>/images/help.gif" alt="per maggiori informazioni" border="0"></a>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<br>
<table align="center" border="0" cellspacing="0" cellpadding="0" width="630"> 
	<tr height="20"> 
		<td height="15" width="15" valign="middle"><img SRC="<%=Session("Progetto")%>/images/righinaPSX.jpg" alt></td>
		<td width="200" class="tblsfondo3"><b class="tbltext0">&nbsp;&nbsp;Inviata da </b></td>
		<td height="15" width="16" valign="middle"><img src="<%=Session("Progetto")%>/images/righinaPDX.jpg" alt></td>
		<td width="385"> <span class="textblack"><b>&nbsp;<%=RespName%></b>&nbsp;&nbsp;&nbsp;(<%=RRDom("DATA")%>)</span> 
		<td width="15" valign="middle">&nbsp;</td>
    </td>
	</tr>
	<tr height="2"> 
		<td colspan="5" background="<%=Session("Progetto")%>/images/separarighe.gif"></td>
	</tr>
	<tr class="tblsfondo3" height="17"> 
		<td height="15" width="15" valign="middle"><img src="<%=Session("Progetto")%>/images/righinaPSX.jpg" alt></td> 
		<td valign="middle" width="600" align="left" class="tbltext0" colspan="3">
			<b>&nbsp; Oggetto</b>
		</td>
		<td height="15" width="15" valign="middle"><img src="<%=Session("Progetto")%>/images/righinaPDX.jpg" alt></td> 
	</tr>
	<tr>
		<td width="15">&nbsp;</td>
		<td class="tblsfondo" width="600" colspan="3">
			<span class="textblack">&nbsp;<%=Server.HTMLEncode(RRDom("OGGETTO"))%></span>
		</td>
		<td width="15">&nbsp;</td>
	</tr>
	<tr height="2"> 
		<td colspan="5" background="<%=Session("Progetto")%>/images/separarighe.gif"></td>
	</tr>
	<tr class="tblsfondo3" height="17"> 
		<td height="15" width="15" valign="middle"><img src="<%=Session("Progetto")%>/images/righinaPSX.jpg" alt></td> 
		<td valign="middle" width="600" align="left" class="tbltext0" colspan="3">
			<b>&nbsp; Domanda</b>
		</td>
		<td height="15" width="15" valign="middle"><img src="<%=Session("Progetto")%>/images/righinaPDX.jpg" alt></td> 
	</tr>
	<tr>
       <td width="15">&nbsp;</td>
       <td width="600" colspan="3" class="tblsfondo">
			<span class="textblack">&nbsp;<%=Server.HTMLEncode(RRDom("DOMANDA"))%></span>
		</td>
		<td width="15">&nbsp;</td>
	</tr>
</table>  
<br>
<table align="center" border="0" cellspacing="0" cellpadding="0" width="630">
	<tr class="tblsfondo3" height="17"> 
		<td height="15" width="15" valign="middle"><img src="<%=Session("Progetto")%>/images/righinaPSX.jpg" alt></td> 
		<td valign="middle" width="600" align="left" class="tbltext0" colspan="2">
			<b>&nbsp; Risposte</b>
		</td>
		<td height="15" width="15" valign="middle"><img src="<%=Session("Progetto")%>/images/righinaPDX.jpg" alt></td> 
	</tr>
</table>  	  	
<table border="0" cellspacing="1" cellpadding="1" width="550">
<%
var sm
sm = Session("mask")

  if (lista.length==0){	
       Response.Write("<tr class=tblsfondo><td nowrap align=center class=tbltext3><b>");
       Response.Write("-- Non ci sono risposte --");
       Response.Write("</td></tr>");
  }     
  else{	
  for (i=0;i<lista.length;i++) {
       d = "";
       d += (lista[i].data.getDate()<10) ? "0" + lista[i].data.getDate()
                                         : lista[i].data.getDate();
       d += "/";
       d += (lista[i].data.getMonth()+1<10) ? "0" + (lista[i].data.getMonth()+1)
                                            : (lista[i].data.getMonth()+1);
       d += "/";
       d += lista[i].data.getYear() + " ";
       d += (lista[i].data.getHours()<10) ? "0" + lista[i].data.getHours()
                                          : lista[i].data.getHours();
       d += ":";
       d += (lista[i].data.getMinutes()<10) ? "0" + lista[i].data.getMinutes() 
                                            : lista[i].data.getMinutes();
       Response.Write("<tr class=tblsfondo>");
       Response.Write("<td nowrap width=190 class=textblack>");
       for (j=0;j<lista[i].livello;j++)
			Response.Write("<img src='" + Session("Progetto") + "/images/blank_trasp.gif' width='12' height='16'  alt=''>");
       if (lista[i].livello==0)
			Response.Write("<img src='" + Session("Progetto") + "/images/msg.gif' width='12' height='16'  alt=''>");
       else
			Response.Write("<img src='" + Session("Progetto") + "/images/re.gif' width='12' height='16'  alt=''>");	
		%>	<a class="textblack" onmouseover="javascript:window.status=' '; return true;" href="javascript:mostra(<%=qt%>,<%=lista[i].id%>,<%=cat%>)">
			<%=Server.HTMLEncode(lista[i].oggetto)%>
			</a>
			</td>
			<td nowrap width="180" class="textblack"><b><span class="size9"><%=lista[i].autore%></span></b></td>
			<td width="180" nowrap class="textblack" width="100"><span class="size9"><%=d%></span></td>
			
			<%
				if (sm > "04" ) { %>					
					<td width="16" align="center" nowrap>
						<a href="javascript:modifica(<%=qt%>,<%=cat%>,<%=lista[i].id%>)" onmouseover="window.status =' '; return true"><img src="/images/SistDoc/ModDoc.gif" width="23" height="21" border="0" alt="Modifica la risposta"></a>
					</td>					
					<td width="16" align="center" nowrap> 
						<a href="javascript:elimina(<%=cat%>,<%=qt%>,<%=lista[i].id%>)" onmouseover="window.status =' '; return true"><img src="<%=Session("Progetto")%>/images/cestino.gif" border="0" alt="Elimina la risposta e tutti gli altri messaggi associati"></a>
					</td>
<%
				}
%>			
		</tr>
  <%   }
    } 
  %>
</table>

<form method="post" name="frmCancella" action="FOR_VisDomande.asp">
	<input type="hidden" name="modo" value="2">
	<input type="hidden" name="cat" value>
	<input type="hidden" name="can" value>
	<input type="hidden" name="id" value>
	<input type="hidden" name="IdCanale" value="<%=cat%>">
</form>

<form method="post" name="frmMod" action="FOR_Modifica.asp">
	<input type="hidden" name="oggetto" value="risp">
	<input type="hidden" name="mod" value>
	<input type="hidden" name="mod1" value>
	<input type="hidden" name="mod2" value>
	<input type="hidden" name="IdCanale" value="<%=cat%>">
</form>

<br>
<table border="0" cellspacing="1" cellpadding="1" width="630">		
   <tr> 
		<td align="right" class="textblack">
<%
		if (s>0) {%> 
			[<a class="textred" href="javascript:history.back()" onmouseover="javascript:window.status=' '; return true;">Prev</a>]
<%	
		}
		else 
			Response.Write("[<font size=1 face='Verdana' color='#AAAAAA'>Prev</font>]");
%>
		</td>
		<td>&nbsp;</td>
		<td align="left" class="textblack">
<%		if (last>0 && appo == "SI") { %>
			[<a class="textred" href="javascript:sAlbero(<%=last%>,<%=qt%>)" onmouseover="javascript:window.status=' '; return true;">Next</a>]
<%	
			}
		else 
			Response.Write("[<font size=1 face='Verdana' color='#AAAAAA'>Next</font>]");
%>
		</td>
   </tr>
</table>
<br>
<table border="0" cellspacing="1" cellpadding="1" width="630">	
<script language="VBScript" runat="server"> 
'PL-SQL * T-SQL  
	Server.Execute ("/Include/ckProfile.asp") 
</script>
<% if (sm != "01" )  { %>
   <tr>	
		<td align="center" colspan="2">
			<b><a class="textred" href="javascript:document.frmRisp.risp.value=''; document.frmRisp.submit();" onmouseover="window.status =' '; return true">Rispondi</a></b>
		</td>
   </tr>
<%
} 			
%>
   <tr>
		<td align="center"> 
			<a class="textred" href="FOR_VisCanali.asp" onmouseover="window.status =' '; return true">
			<b>Torna alle Aree</b></a>
		</td>
		<td align="center"> 
			<a class="textred" href="javascript:document.frmDom.submit();" onmouseover="window.status =' '; return true">
			<b>Torna ai Messaggi</b></a>
		</td>
   </tr>
</table>
<br>   
<form method="post" name="frmRisp" action="FOR_InsRisposte.asp">
	<input type="hidden" name="s" value="<%=s%>">
	<input type="hidden" name="risp" value>
	<input type="hidden" name="qt" value="<%=qt%>">
	<input type="hidden" name="cat" value="<%=cat%>">
	<input type="hidden" name="IdCanale" value="<%=cat%>">
	<input type="hidden" name="NomePage" value="FOR_InsRisposte">
</form>
<form method="post" name="frmAlbero" action="FOR_Albero.asp">
	<input type="hidden" name="id" value>
	<input type="hidden" name="modo" value>
	<input type="hidden" name="s" value>
	<input type="hidden" name="qt" value>
	<input type="hidden" name="cat" value="<%=cat%>">
	<input type="hidden" name="IdCanale" value="<%=cat%>">
</form>
<form method="post" name="frmDom" action="FOR_VisDomande.asp">
	<input type="hidden" name="cat" value="<%=cat%>">
	<input type="hidden" name="IdCanale" value="<%=cat%>">
</form>
<%    
// Chiude la connessione al database
CC.Close();
%>
<!--Epili 13/01/02 Per poter utilizzare le strutture standard.-->
