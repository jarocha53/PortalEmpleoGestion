<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include virtual ="/strutt_testa2.asp"-->
<!--#include virtual ="/util/portallib.asp"-->
<!--#include virtual ="/Include/ckProfile.asp"-->
<!--#include virtual ="/include/openconn.asp"-->

<script LANGUAGE="JavaScript" src="/Include/help.inc"></script>
<script LANGUAGE="JavaScript" src="/Include/ControlString.inc"></script>
<script LANGUAGE="JavaScript">
function validateIt() {
	if(document.form1.txtTitle.value == "" || document.form1.txtTitle.value == " ")
	  {
	  document.form1.txtTitle.focus();
	  alert("Inserire l'Oggetto. ");
	  return false;
	  }
	if(document.form1.txtQuestion.value == "" || document.form1.txtQuestion.value == " ")
	  {
	  document.form1.txtQuestion.focus();
	  alert("Inserire il testo della risposta. ");
	  return false;
	  }
} 	  
</script>	
<%
Sub Inizio()
%>
<br>
<table border="0" width="100%" height="70" cellspacing="0" cellpadding="0" background="<%=Session("Progetto")%>/images/titoli/Community2b.gif">
	<tr>
		<td valign="bottom" align="right"><b class="tbltext1">Forum di discussione <i> <%= Session("Category") %></i></b></td>
		<td valign="bottom" align="left" width="3%">
			<a href="Javascript:Show_Help('/Pgm/help/Formazione/Forum/FOR_InsRisposte')" name onmouseover="javascript:status='' ; return true">
			<img alt="per maggiori informazioni" align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
		</td>
	</tr>	
</table>
<br>
<%
End Sub

Sub ImpostaPag()

	Dim sql, RR, strText
%>		
<br><br>
	<table align="center" border="0" cellspacing="0" cellpadding="0" width="540">
		<tr> 
			<td height="15" width="15" valign="middle"><img src="<%=Session("Progetto")%>/images/righinaPSX.jpg" alt></td> 
			<td valign="middle" width="570" align="left" class="tblsfondo3">
				<b class="tbltext0">&nbsp; Testo del Messaggio</b>
			</td>
			<td height="15" width="15" valign="middle"><img src="<%=Session("Progetto")%>/images/righinaPDX.jpg" alt></td> 
		</tr>
		<tr height="25"> 
			<td width="15" class="tbltext"> &nbsp; </td>
			<td width="485" class="tblsfondo"><span class="tbltext"> &nbsp; <%= sDomanda %></td>
			<td width="15" class="tbltext"> &nbsp; </td>
		</tr>				
	</table>

	<form Name="form1" onSubmit="return validateIt();" ACTION="FOR_CnfRisposte.asp" METHOD="POST">
	    <br>
	    <input type="hidden" name="s" value="<%=Request("s")%>">
	    <input type="hidden" name="idRisp" value="<%=idRisp%>">
	    <input type="hidden" name="qt" value="<%=idDom%>">
	    <input type="hidden" name="cat" value="<%=clng(cat)%>">
		<input type="hidden" name="IdCanale" value="<%=cat%>">
	    <table border="0" cellspacing="0" cellpadding="0" width="540" align="center">
			<tr align="center"> 
				<td class="tbltext3">
					<b>Usa il seguente form per rispondere al messaggio 
	                </b>
	            </td>
			</tr>
		</table>
		<br>
		<table align="center" border="0" cellspacing="0" cellpadding="0" width="540">
			<tr> 
				<td height="15" width="15" valign="middle"><img src="<%=Session("Progetto")%>/images/righinaPSX.jpg" alt></td> 
				<td valign="middle" width="100" align="left" class="tblsfondo3">
					<b class="tbltext0">&nbsp; Oggetto </b>
				</td>
				<td height="15" width="15" valign="middle"><img src="<%=Session("Progetto")%>/images/righinaPDX.jpg" alt></td> 
				<td width="410" class="tbltext"> &nbsp; Re:<%=sOggetto%><input type="hidden" name="txtTitle" maxlength="100" size="30" class="tbltext" value="Re:<%=sOggetto%>"></td>
			</tr>
			<tr>
				<td colspan="4" height="2" width="15" valign="middle" background="<%=Session("Progetto")%>/images/separarighe.gif"></td> 
	        </tr>
		</table>
		<table align="center" border="0" cellspacing="0" cellpadding="0" width="540">
			<tr> 
				<td height="15" width="15" valign="middle"><img src="<%=Session("Progetto")%>/images/righinaPSX.jpg" alt></td>
				<td valign="middle" width="570" align="left" class="tblsfondo3"><b class="tbltext0">&nbsp; Testo della Risposta</b>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<span class="tbltext0">- Utilizzabile 
					<b><label name="NumCaratteri" id="NumCaratteri">485</label></b>
					caratteri -
					</span>
				</td>
				<td height="15" width="15" valign="middle"><img src="<%=Session("Progetto")%>/images/righinaPDX.jpg" alt></td> 
			</tr>
			<tr> 
				<td width="15" class="tbltext"> &nbsp; </td>
				<td align="left" width="500" valign="middle">
					<textarea name="txtQuestion" onKeyup="JavaScript:CheckLenTextArea(document.form1.txtQuestion,NumCaratteri,485)" CLASS="MyTextBox" cols="80" rows="8"></textarea>
				</td>
				<td width="15" class="tbltext"> &nbsp; </td>
			</tr>				
			<tr height="17"> 
				<td colspan="3">&nbsp;</td>
			</tr>
		</table>	

		<table align="center" border="0" cellspacing="0" cellpadding="0" width="540">
			<tr> 
				<td colspan="2" align="center">
					<input type="image" src="<%=Session("progetto")%>/images/conferma.gif" alt="Conferma la tua risposta" border="0" align="absBottom" name="image2">					
				</td>
			</tr>
	    </table>
	</form>
	<table border="0" cellspacing="0" cellpadding="0" align="center" width="540">
		<tr> 
			<td align="center"> 
				<a class="textred" href="/Pgm/formazione/forum/FOR_VisCanali.asp" onmouseover="window.status =' '; return true">
				<b>Torna alle Aree </b></a>
			</td>
			<td align="center"> 
				<a class="textred" href="javascript:frmAlbero.submit();" onmouseover="window.status =' '; return true">
				<b>Torna ai messaggi</b></a>
			</td>
		</tr>
	</table>
	<form method="post" name="frmAlbero" action="/Pgm/formazione/forum/FOR_Albero.asp">
		<input type="hidden" name="s" value="<%=s%>">
		<input type="hidden" name="qt" value="<%=idDom%>">
		<input type="hidden" name="cat" value="<%=clng(cat)%>">
		<input type="hidden" name="IdCanale" value="<%=cat%>">
	</form>
<%
End Sub

'*********************		MAIN			-->
	If not ValidateService(Session("IdUtente"),"GESTIONE FORUM",cc) Then 
		response.redirect "/util/error_login.asp"
	End If
		
	Dim strConn, Rs, sql
	Dim idRisp
	Dim idDom
	
	cat = clng(Request.form("IdCanale"))
	
	If Request("qt") <> "" Then
		idDom  = clng(Request("qt"))
	End If
	If Request("risp") <> "" Then
		idRisp = clng(Request("risp"))
	End If
	
	If idRisp <> ""  Then
		sql = "SELECT risposta, oggetto FROM frm_risposta WHERE id_frm_risposta=" & idRisp
	Else
		sql = "SELECT domanda, oggetto FROM frm_domanda WHERE id_frm_domanda=" & idDom
	End If
	
	set rsFrmDom = Server.CreateObject("ADODB.RECORDSET")

'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
	rsFrmDom.Open sql, CC, 2, 2
			
	If not rsFrmDom.eof Then  
		sDomanda = rsFrmDom(0)
		sOggetto = rsFrmDom(1) 
	End If
			
	rsFrmDom.close
	Set rsFrmDom = Nothing
		
	s = Request("s")

	If ckProfile(Session("mask"),2) Then 
		
		Inizio()
		ImpostaPag()
	
	End If
	
%>	
<!--#include virtual ="/include/closeconn.asp"-->
<!--#include virtual ="/strutt_coda2.asp"-->
