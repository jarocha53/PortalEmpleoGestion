<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include virtual = "util/portallib.asp"-->
<!--#include virtual = "include/ckProfile.asp"-->
<!--#include virtual = "include/openconn.asp"-->
<!--#include virtual = "include/controlDateVB.asp"-->
<html>
<!--	BLOCCO HEAD			-->
<head>
<title>Dettaglio Risposte</title>
<link REL="STYLESHEET" TYPE="text/css" HREF="<%=Session("Progetto")%>/fogliostile.css">
<script LANGUAGE="JavaScript" src="/Include/help.inc"></script>
<script LANGUAGE="JavaScript">
function Elimina(categoria, idRisp, idDom){
		
	if (confirm("Conferma l'eliminazione della risposta?"))	{
		opener.frmAlbero.id.value= idRisp;
		opener.frmAlbero.s.value= "0";
		opener.frmAlbero.cat.value= categoria;
		opener.frmAlbero.qt.value= idDom;
		opener.frmAlbero.modo.value="del";
		opener.frmAlbero.submit();
		window.close();
		}
}

function Rispondi(categoria, idRisp, idDom){
	opener.frmRisp.risp.value= idRisp;
	opener.frmRisp.cat.value= categoria;
	opener.frmRisp.IdCanale.value= categoria;
	opener.frmRisp.qt.value= idDom;
	opener.frmRisp.submit();
	window.close();
}		
</script>
</head>
<%
Sub Inizio()

	If not ValidateService(Session("IdUtente"),"GESTIONE FORUM",cc) Then 
		response.redirect "/util/error_login.asp"
	End If
%>
<body>
<table border="0" width="100%" cellspacing="0" cellpadding="0" height="80">
	<tr>
		<td width="380" background="<%=Session("Progetto")%>/images/titoli/Community3b.gif" height="80" valign="bottom" align="left">
			<table border="0" background align="left" width="368" height="25" cellspacing="0" cellpadding="0">
				<tr>
					<td width="100%" valign="top" align="right"><b><font face="Verdana" size="2" color="#006699">Dettaglio Forum <i><%=Session("Category")%>&nbsp;&nbsp;</i></font></b>
						<a href="Javascript:Show_Help('/Pgm/help/Formazione/Forum/FOR_VisRisposte')" name onmouseover="javascript:status='' ; return true">
						<img alt="per maggiori informazioni" align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a></span>
					</td>
				</tr>
			</table>
		</td>
	</tr>
 </table>
 <br>
<%
End Sub

Sub ImpostaPag()
		
	Dim sqlRisp, RRRisp, strText
	Dim SQLs, RRs
		
	sqlRisp = "SELECT id_frm_risposta, frm_risposta.oggetto, frm_risposta.mittente, frm_risposta.dt_tmst as respdate, " &_
			  "frm_risposta.id_frm_risposta, frm_risposta.ip_address, frm_risposta.risposta " & _
			  "FROM frm_risposta " & _
			  "WHERE id_frm_domanda = " & idDom & _
			  " ORDER BY frm_risposta.dt_tmst DESC"		
		
	set rsRisp  = Server.createObject("adodb.recordset")
'PL-SQL * T-SQL  
SQLRISP = TransformPLSQLToTSQL (SQLRISP) 
	rsRisp.open  sqlRisp, CC, 2, 2
	
	Do While not rsRisp.eof 
		idRisp = rsRisp("ID_FRM_RISPOSTA")
		
		SQLs = "SELECT id_frm_risposta FROM frm_risposta WHERE id_risposta = "  & idRisp
'PL-SQL * T-SQL  
SQLS = TransformPLSQLToTSQL (SQLS) 
		set RRs = CC.Execute(SQLs)
%>	
		<table border="0" cellspacing="0" cellpadding="0" width="100%" align="center">	
 		<tr>
 			<td align="center" width="15" valign="bottom" height="20"> 
 				&nbsp;
<%				
				If ckProfile(Session("mask"),8) And RRs.Eof Then %>
					<a href="javascript:Elimina(<%=cat%>,<%=idRisp%>,<%=idDom%>)" onmouseover="window.status =' '; return true">
					<img src="<%=Session("Progetto")%>/images/cestino.gif" border="0" alt="Elimina la risposta"></td>
<%				
				End If
				RRs.Close
				Set RRs = Nothing
%>		
			</td>
	  		<td align="right" colspan="2" valign="bottom"><img src="<%=Session("Progetto")%>/images/msg.gif" border="0"><a class="textblack" name="<%=idRisp%>"><span class="size9"><%=idRisp%></span></a></td>
	  	</tr>		  	
	  	<tr class="tblsfondo2">
	  		<tr height="20">
			<td height="15" width="15" valign="middle"><img src="<%=Session("Progetto")%>/images/righinaPSX.jpg" alt></td> 
			<td width="400" class="tblsfondo3"><b class="tbltext0"><%=Server.HTMLEncode(rsRisp("OGGETTO"))%></b></td>
			<td height="15" width="16" valign="middle"><img src="<%=Session("Progetto")%>/images/righinaPDX.jpg" alt></td>
	  	</tr>
	  	</table>
		<table border="0" cellspacing="1" cellpadding="1" width="90%%" align="center">	
	  	<tr>
	  		<td colspan="1" class="tblsfondo">	
	  			<span class="tbltext">data:&nbsp;<%= ConvDateTimeToString(rsRisp("RespDate"))%>
	  		</span>
	  		</td>	  		
	  	</tr>
	  	<tr class="tblsfondo" height="80">
	  		<td colspan="3">
 			  <span class="tbltext"><b><%=Server.HTMLEncode(rsRisp("risposta"))%></b></span>
	  		</td>
	  	</tr>
	  	</table>
		<table border="0" cellspacing="1" cellpadding="1" width="90%" align="center">	
		<tr class="tblsfondo2">
	  		<td valign="bottom" align="center" width="50%">
<%			If ckProfile(Session("mask"),2) Then %>
	  		<a class="textblacka" href="javascript:Rispondi('<%=cat%>','<%=idRisp%>','<%=idDom%>')"><span class="size9"><b>Rispondi al messaggio</b></span></a>
<%			End If %>
	  		</td>
	  		<td valign="bottom" align="center"><a class="textblacka" href="javascript:window.close()"><span class="size9"><b>Chiudi</b></span></a></td>
	  	</tr>
		</table>
		<br>
<%	
		rsRisp.movenext
	Loop 
	rsRisp.Close
	Set rsRisp=Nothing 

End Sub
'**************************  MAIN	**********************

	Dim strConn, Rs, sql

	idDom = clng(Request("qt"))
	'cat	  = clng(Request("cat"))
	cat = clng(Request.form("IdCanale"))
	Inizio()
	ImpostaPag()
		
%>	
<!--#include virtual ="include/closeconn.asp"-->
