<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%
Sub ControlliJavaScript()
%>
<script language="Javascript">
<!--#include virtual="/include/ControlDate.inc"-->
<!--#include virtual="/include/ControlNum.inc"-->
<!--#include virtual="/include/ControlString.inc"-->
function FormInserisciCorso_Validator(frmCorso)
	{
	if (frmCorso.cmbArea.value == "")
		{
		frmCorso.cmbArea.focus();
		alert("Il campo 'Area' � obbligatorio.");
		return (false);
	  	}
		
	if (frmCorso.txtTitoloCorso.value == "")
		{
		frmCorso.txtTitoloCorso.focus();
		alert("Il campo 'Titolo Corso' � obbligatorio.");
		return (false);
	  	}

	if (frmCorso.txtDescrizione.value == "")
		{
		frmCorso.txtDescrizione.focus();
		alert("Il campo 'Descrizione' � obbligatorio.");
		return (false);
	  	}

	if (frmCorso.cboLivCorso.value == "")
		{
		frmCorso.cboLivCorso.focus();
		alert("Il campo 'Livello Corso' � obbligatorio.");
		return (false);
	  	}
		
	if (frmCorso.cboFlErogazione.value == "")
		{
		frmCorso.cboFlErogazione.focus();
		alert("Il campo 'Modalit� di erogazione' � obbligatorio.");
		return (false);
	  	}

	
	if (frmCorso.chkDispo.checked == false)
	{
		if (!ValidateInputDate(frmCorso.txtDisponibilitaDal.value))
			{
				frmCorso.txtDisponibilitaDal.focus();
				return (false);
			}
	}	

	if (frmCorso.txtInfoProd.value == "")
		{
		frmCorso.txtInfoProd.focus();
		alert("Il campo 'Informazioni Produttore' � obbligatorio.");
		return (false);
	  	}	

	if (frmCorso.cboFlPubblico.value == "")
		{
		frmCorso.cboFlPubblico.focus();
		alert("Il campo 'Pubblico/Privato' � obbligatorio.");
		return (false);
	  	}			
	if (frmCorso.txtDurata.value != "")
		{
		if (isNaN(frmCorso.txtDurata.value))
			{
				frmCorso.txtDurata.focus();
				alert("Il campo 'Durata corso' � numerico.");
				return (false);				
			}
		}
	return (true);
	}
function ImpFlag()
{
	//alert(FormInserisciCorso.chkDispo.checked);
	if (FormInserisciCorso.chkDispo.checked == true)
		{
			FormInserisciCorso.txtDisponibilitaDal.disabled = true
		}
	else
		{
			FormInserisciCorso.txtDisponibilitaDal.disabled = false
		}
}
</script>
<%
End Sub
'-----------------------------------------------------------------------
Sub Inizio()
	dim sFunzione
	dim sTitolo
	dim sCommento
	dim bCampiObbl
	
	sFunzione = "GESTIONE CORSI"
	sTitolo = "INSERIMENTO CORSO"
	sCommento = "Commento all'inserimento del corso"
	bCampiObbl = true
%>
	<!--#include file="COR_Testata.asp"-->
<%

End Sub
'-----------------------------------------------------------------------
	Sub ImpostaPag()
		Dim sql, RR, sql2, RR2
%>
		<br>
		<form METHOD="POST" onsubmit="return FormInserisciCorso_Validator(this)" name="FormInserisciCorso" action="COR_CnfCorsi.asp">
		<input type=hidden name="Oper" id="Oper" value="Ins">
		<table border="0" cellspacing="2" cellpadding="1" width="500">
			<tr> 
				<td align="left" nowrap class="tbltext1">
					<b>Area</b>*
				</td>
				<td> 
<%					
					sql="SELECT DISTINCT TitoloArea, ID_AREA FROM AREA ORDER BY TitoloArea"
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
					SET RR=CC.EXECUTE (sql)
%>					
					<select name="cmbArea" id="cmbArea" class="textblack"> 
						
						<option VALUE></option>
						<% DO WHILE NOT RR.EOF %> 
							<option value="<%=RR.FIELDS("ID_AREA")%>"><%=RR.FIELDS("TitoloArea")%> 
							</option>
						<% RR.MOVENEXT
						   LOOP
%>						
					</select>
<%					RR.CLOSE
%>				</td>
			</tr>
		    <tr> 
				<td align="left" nowrap class="tbltext1">
					<b>Titolo Corso*</b>
				</td>
				<td>
					<input type="text" name="txtTitoloCorso" class="textblacka" size="50" id="txtTitoloCorso maxlength="50">
				</td>
		    </tr>
		    <tr> 
				<td align="left" nowrap class="tbltext1">
					<b>Descrizione*</b>
				</td>
				<td>
					<span class="tbltext1">&nbsp;- Utilizzabili <label name="lblNumCar1" id="lblNumCar1">254</label> caratteri -</span>
					<textarea name="txtDescrizione" class="textblack" cols="50" rows="3" OnKeyUp="JavaScript:CheckLenTextArea(txtDescrizione,lblNumCar1,254)"></textarea>
					<!--input type="text" name="txtLenDescr" id="txtLenDescr" class="textblacka" size="3" value="254" disabled-->
				</td>
		    </tr>
		    <tr> 
				<td align="left" nowrap class="tbltext1">
					<b>Obiettivo</b>
				</td>
				<td> 
					<span class="tbltext1">&nbsp;- Utilizzabili <label name="lblNumCar2" id="lblNumCar2">254</label> caratteri -</span>
					<textarea name="txtObiettivo" class="textblack" cols="50" rows="3" OnKeyUp="JavaScript:CheckLenTextArea(txtObiettivo,lblNumCar2,254)"></textarea>
				</td>
		    </tr>
		    <tr> 
				<td align="left" nowrap class="tbltext1">
					<b>Livello Corso*</b>
				</td>
				<td> 
					<select name="cboLivCorso" id="cboLivCorso" class=textblack>
						<option value="" selected></option>
						<option value="1">1</option>
						<option value="2">2</option>
						<option value="3">3</option>
					</select>
				</td>
		    </tr>
		    <tr> 
				<td align="left" nowrap class="tbltext1">
					<b>Versione Corso AICC</b>
				</td>
				<td> 
					<input name="txtVerCorsoAICC" size="3" maxlength="3" type="text" class="textblacka">
				</td>
		    </tr>
		    <tr> 
				<td align="left" nowrap class="tbltext1">
					<b>Crediti corso</b>
				</td>
				<td> 
					<input name="txtNumCredCorso" type="text" class="textblacka">
				</td>
		    </tr>
		    <tr> 
				<td align="left" nowrap class="tbltext1">
					<b>Modalit� di Erogazione*</b>
				</td>
				<td> 
					<select name="cboFlErogazione" id="cboFlErogazione" class=textblack>
						<option value="" selected></option>
						<option value="0">Off Line</option>
						<option value="1">On Line</option>
					</select>
				</td>
		    </tr>
		    <tr> 
				<td align="left" nowrap class="tbltext1">
					<b>Durata corso</b>&nbsp;(in ore)
				</td>
				<td> 
					<input name="txtDurata" type="text" class="textblacka" size=10>
				</td>
		    </tr>
		    <tr> 
				<td align="left" nowrap class="tbltext1">
					<b>Disponibilit� dal*</b>
				</td>
				<td class=tbltext1> 
					<input name="txtDisponibilitaDal" type="text" class="textblacka" size="10" maxlength=10>
					&nbsp;&nbsp;&nbsp;
					<input type="checkbox" name="chkDispo" id="chkDispo" onclick="javascript:ImpFlag()">
					(non disponibile)
				</td>
		    </tr>
		    <tr> 
				<td align="left" nowrap class="tbltext1">
					<b>Authoring</b>
				</td>
				<td> 
					<input name="txtAuthoring" type="text" size="50" class="textblacka" maxlength=254>
				</td>
		    </tr>
		    <tr> 
				<td align="left" nowrap class="tbltext1">
					<b>Informazioni<br>Produttore*</b>
				</td>
				<td>
					<span class="tbltext1">&nbsp;- Utilizzabili <label name="lblNumCar3" id="lblNumCar3">2000</label> caratteri -</span>
					<textarea name="txtInfoProd" class="textblack" cols="50" rows="3" OnKeyUp="JavaScript:CheckLenTextArea(txtInfoProd,lblNumCar3,2000)"></textarea>
				</td>
		    </tr>
		    <tr> 
				<td align="left" nowrap class="tbltext1">
					<b>Progetto proprietario</b>
				</td>
				<td> 
					<%
						CreateCombo("CPROJ||||cboProjOwn||")
					%>
				</td>
		    </tr>
		    <tr> 
				<td align="left" nowrap class="tbltext1">
					<b>Pubblico/Privato*</b>
				</td>
				<td> 
					<select name="cboFlPubblico" id="cboFlPubblico" class=textblack>
						<option value="" selected></option>
						<option value="0">Pubblico</option>
						<option value="1">Privato</option>
					</select>
				</td>
		    </tr>
		    <tr> 
				<td align="left" nowrap class="tbltext1">
					<b>Prerequisiti necessari</b>
				</td>
				<td> 
					<span class="tbltext1">&nbsp;- Utilizzabili <label name="lblNumCar4" id="lblNumCar4">2000</label> caratteri -</span>
					<textarea name="txtPrereqCorso" class="textblack" cols="50" rows="3" OnKeyUp="JavaScript:CheckLenTextArea(txtPrereqCorso,lblNumCar4,2000)"></textarea>
				</td>
		    </tr>
		    <tr> 
				<td align="left" nowrap class="tbltext1">
					<b>Destinatari del Corso</b>
				</td>
				<td> 
					<span class="tbltext1">&nbsp;- Utilizzabili <label name="lblNumCar5" id="lblNumCar5">2000</label> caratteri -</span>
					<textarea name="txtDestinatari" class="textblack" cols="50" rows="3" OnKeyUp="JavaScript:CheckLenTextArea(txtDestinatari,lblNumCar5,2000)"></textarea>
				</td>
		    </tr>
		</table>
		<br>
		<table width="500" border="0" cellspacing="2" cellpadding="1">
			<tr align="center">
				<td align="right">			
					<%
						PlsIndietro()
					%>
				</td>
				<td align="left">
					<%
						PlsInvia("InserisciC")
					%>
				 </td>
			</tr>
		</table> 
		</form> 
		<br>
<%
	End Sub
'------------------------------------------------------------------------
%>
<!--#include virtual="/strutt_testa2.asp"-->
<!--#include virtual="/include/openconn.asp"-->
<!--#include virtual="/include/DecCod.asp"-->
<!--#include file="COR_Pulsanti.asp"-->
<%	
	ControlliJavaScript()
	Inizio()
	ImpostaPag()
%>
<!--#include virtual="/include/closeconn.asp"-->
<!--#include virtual="/strutt_coda2.asp"-->
