<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%
Sub Inizio()
	dim sFunzione
	dim sTitolo
	dim sCommento
	dim bCampiObbl
	
	sFunzione = "GESTIONE CORSI"
	sTitolo = "GESTIONE CORSI"
	sCommento = "Elenco dei<br>Corsi ricercati"
	bCampiObbl = false
%>
	<!--#include file="COR_Testata.asp"-->
<%
End Sub
';););););););););););););););););););););););););););););););););););););););)
Sub ImpostaPag()
	Dim aTipo(2)
	Dim sSQL, ssSQLCond
	Dim sCondTitCorso
	dim sCondTitArea
	dim sCondDestinatari
	dim sCondObiettivi
	dim sCondModalita
	dim sCondDurata

	aTipo(0) = "Off Line"
	aTipo(1) = "On Line"
		
	sSQL = "SELECT * FROM Corso, Area WHERE Area.ID_AREA = Corso.ID_AREATEMATICA "
	sSQLCond = ""

	sCondTitCorso = Request.Form("txtTitCorso")
	sCondTitArea = Request.Form("txtTitArea")
	sCondDestinatari = Request.Form("txtDestinatari")
	sCondObiettivi = Request.Form("txtObiettivi")
	sCondModalita = Request.Form("txtModalita")
	sCondDurata = Request.Form("txtDurata")

	If sCondTitCorso <> "" Then	
		sSQLCond = sSQLCond & " AND TIT_CORSO LIKE '%" & _
		Replace(sCondTitCorso, "'", "''") & "%'"
	End If

	If sCondTitArea <> "" AND sCondTitArea <> "TUTTE" Then
		sSQLCond = sSQLCond & " AND Area.TitoloArea LIKE '%" & Replace(sCondTitArea, "'", "''") & "%'"
	End If

	If sCondObiettivi <> "" Then
		sSQLCond = sSQLCond & " AND OBIETTIVO_CORSO LIKE '%" & _
		Replace(sCondObiettivi, "'", "''") & "%'"
	End If

	If sCondModalita <> "" AND sCondModalita <> "TUTTE" Then
		sSQLCond = sSQLCond & " AND FL_EROGAZIONE ='" & sCondModalita & "'"
	End If

	If sCondDurata <> "" Then	
		sSQLCond = sSQLCond & " AND DURATA_CORSO LIKE '%" & _
		Replace(sCondDurata, "'", "''") & "%'"
	End If

	sSQL = sSQL & sSQLCond & " ORDER BY ID_CORSO, TIT_CORSO"

	Set rsCorsi = Server.CreateObject("ADODB.Recordset")
		
	'Response.Write sSQL
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	rsCorsi.Open sSQL, CC, 3
		
	If rsCorsi.BOF and rsCorsi.EOF Then
	%>
		 <table border="0" width="500" cellpadding="0" cellspacing="0">
			<tr align="center">
				<td class="tbltext3"> 
					Non vi sono Corsi che soddisfano le condizioni di ricerca
				</td>
			</tr>
		 </table>
<%  
	Else	
        rsCorsi.MoveLast
        rsCorsi.MoveFirst
    End If
		
	Set RR = Server.CreateObject("ADODB.Recordset")
	sSQL = "Select count(*) as ContaCorso FROM Corso, Area " &_
			"WHERE Area.ID_AREA= Corso.ID_AREATEMATICA " & sSQLCond
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	set RR = CC.Execute(sSQL)
	nTot = CINT(RR.Fields("ContaCorso"))
	RR.Close
	set RR = nothing
		
	If nTot > 25 Then
	%>
		<table border="0" width="500" cellpadding="0" cellspacing="0">
			<tr align="center">
				<td class="tbltext3"> 
					Troppi Corsi soddisfano le condizioni di ricerca
					<br>
					Raffinare la ricerca!
				</td>
			</tr>
		 </table>
		<br>
<% 
	Else
	    If Not rsCorsi.BOF Then
%>
			<table border="0" width="500">
				<tr class="sfondocomm">
					<td><b>Titolo</b></td>
					<td><b>Descrizione</b></td>
					<td width="80"><b>Elementi</b></td>
					<td width="80"><b>Modalitą di erogazione</b></td>
				</tr>
		 <%     Do While Not rsCorsi.EOF
		 %>		<tr class="tblsfondo"> 
					<td>
						<a HREF="COR_ModCorsi.asp?CodArea=<%=rsCorsi("ID_AREA")%>&amp;IDCorso=<%=rsCorsi("ID_CORSO")%>" class="tblAgg"> 
						 <%=strHTMLEncode(rsCorsi("TIT_CORSO"))%></a> 
					</td>
					<td class="tblDett"><%=strHTMLEncode(rsCorsi("Desc_CORSO"))%></td>
					<td align="center"> 
						
						<a href="ELE_VisElementi.asp?CodCorso=<%=server.URLEncode(rsCorsi.Fields("TIT_CORSO"))%>&amp;IDCorso=<%=rsCorsi("ID_CORSO")%>"> 
						
						<!--a href="javascript:alert('Funzionalitą non attiva')"-->
							<img src="/images/sistdoc/creadoc.gif" width="20" height="20" border="0" alt="Visualizza i moduli"><!-- onmouseover="javascript:window.status='' ; return true"--> 
						</a>
					</td>
					<td class="tblDett"><%=aTipo(rsCorsi("FL_EROGAZIONE"))%> 
					</td>
				</tr>
	    <%		rsCorsi.MoveNext
				Loop
		%> 
			</table>
	<%	
		End If
	End If
	rsCorsi.Close
	%> 
	<br>
	<table border="0" width="500" cellpadding="0" cellspacing="0">
		<tr align="center">
			<td> 
				<%
					PlsLinkRosso "COR_RicCorsi.asp", "Nuova ricerca"
				%>
			</td>
		</tr>
	</table>
	<br>
<%
End Sub
';););););););););););););););););););););););););););););););););););););););)
'M A I N
%>
<!--#include virtual="/strutt_testa2.asp"-->
<!--#include virtual="/include/openconn.asp"-->
<!--#include virtual="/include/HTMLEncode.asp"-->
<!--#include file="COR_Pulsanti.asp"-->
<%	
Inizio()
ImpostaPag()
%>
<!--#include virtual ="/include/closeconn.asp"-->
<!--#include virtual="/strutt_coda2.asp"-->
