<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%@ Language=VBScript %>
<html>
<head>
<title> Aggiungi Corsi Facoltativi </title>
<link REL="STYLESHEET" TYPE="text/css" HREF="<%=session("progetto")%>/fogliostile.css">
<meta NAME="GENERATOR" Content="Microsoft Visual Studio 6.0">
</head>
<body class="sfondocentro">
<%
'-------------------------------------------------------------------------------
Sub Inizio
	dim sFunzione
	dim sTitolo
	dim sCommento
	dim bCampiObbl

	sFunzione = ""
	sTitolo = "Aggiungi CORSI FACOLTATIVI"
	sCommento = "Testo del commento<br>per Aggiungi Corsi facoltativi"
	bCampiObbl = false
%>	
	<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
	<tr height="18">
		<td class="sfondomenu" width="67%" height="18"><span class="tbltext0"><b>&nbsp;<%=sTitolo%></b></span></td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
		<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">
<%			
			if bCampiObbl then
				Response.Write "(*) campi obbligatori"
			end if
%>
		</td>
	</tr>
	</table>
	<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
		<tr>
			<td align="left" class="sfondocomm">
				<%=sCommento%>
			<!--a href="Javascript:Show_Help('/Pgm/help/Conoscenze/CON_VisConoscenze')"-->
			<img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0" onmouseover="javascript:window.status='' ; return true"><!--/a-->
			</td>
		</tr>
		<tr height="2">
			<td class="sfondocomm" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>
	<br>
	
<%
End sub
'-------------------------------------------------------------------------------
Sub ImpostaPag
	dim sSql
	dim iIdGruppo,iIdArea
	dim sIdCorPresenti	'contiene gli id dei corsi gi� presenti nella tabella CATCORSIFACOLTATIVI
						'per quel gruppo e per l'area tematica scelta
	
	iIdGruppo=Request("UidGruppo")
	iIdArea=Request("UidArea")

'Response.Write "iIdGruppo=" & iIdGruppo & "<br>"
'Response.Write "iIdArea=" & iIdArea & "<br>"

	sSql="SELECT C.ID_CORSO,C.TIT_CORSO,C.DESC_CORSO " &_
		"FROM CORSO C " &_
		"WHERE C.ID_AREATEMATICA=" & iIdArea &_
		" AND C.DT_DISPONIBILE<=" & ConvDateToDBs(Date()) &_
		" AND (C.FL_PUBBLICO=0 OR C.ID_PROJOWNER='" & mid(session("progetto"),2,12) & "')" &_
		" ORDER BY C.TIT_CORSO"
	
'Response.Write sSql & "<br>"	
'Response.End

'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	set RstCorFac=CC.execute(sSql)
	
	if not RstCorFac.eof then
		'Ora cerco i corsi facoltativi gi� presenti con l'area tematica scelta
		sSql=	"SELECT ID_CORSO FROM " &_
				"CATCORSIFACOLTATIVI " &_
				"WHERE IDGRUPPO=" & iIdGruppo & " AND ID_CORSO IN (" &_
					"SELECT ID_CORSO FROM CORSO WHERE ID_AREATEMATICA=" & iIdArea & ")"

'Response.Write sSql & "<br>"

'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
		set RstCorPresenti=CC.execute(sSql)
		sIdCorPresenti=""
		do while not RstCorPresenti.eof
			sIdCorPresenti=sIdCorPresenti & "," & RstCorPresenti("ID_CORSO")
			RstCorPresenti.MoveNext
		loop
		RstCorPresenti.close
		set RstCorPresenti=nothing
		sIdCorPresenti=Mid(sIdCorPresenti,2)
		
'Response.Write "sIdCorPresenti=" & sIdCorPresenti & "<br>"
'RstCorFac.close
'set RstCorFac=nothing
'Response.End	
	
%>		<br>
		<form name="frmAggCorFac" action="CFA_CnfAggCorFac.asp" method="post" id="frmAggCorFac">
		<table border="0" cellspacing="1" width="98%">
			<tr class="sfondocomm">
				<td width="5">&nbsp;
				</td>
				<td>&nbsp;<b>Titolo</b>
				</td>
				<td>&nbsp;<b>Descrizione</b>
				</td>
			</tr>
			
<%			do while not RstCorFac.eof
%>				<tr class="tblsfondo">
					<td width="5">
<%						if ( InStr(sIdCorPresenti,RstCorFac("ID_CORSO"))<>0 ) then
							'Corso gi� presente
%>							<img src="<%=session("progetto")%>/images/visto.gif">							
<%						else
							'Corso non presente
%>							<input type="checkbox" id="chkCorFac" name="chkCorFac" value="<%=RstCorFac("ID_CORSO")%>">
<%						end if
%>					</td>
					<td class="tblDett">&nbsp;<%=server.HTMLEncode(RstCorFac("TIT_CORSO"))%>
					</td>
					<td class="tblDett">&nbsp;<%=server.HTMLEncode(RstCorFac("DESC_CORSO"))%>
					</td>
				</tr>
<%				RstCorFac.MoveNext
			loop
%>		</table>
		<br>
		<br>
		<table border="0">
			<tr>
				<td>
					<a href="JavaScript:self.close();">
					<img src="<%=session("progetto")%>/images/chiudi.gif" border="0"></a>
				</td>
				<td width="10">&nbsp;</td>
				<td>
					<input type="image" src="<%=session("progetto")%>/images/conferma.gif" title="Conferma l`inserimento" id="image1" name="image1">
				</td>
			</tr>
		</table>
		<input type="hidden" name="hIdGruppo" value="<%=iIdGruppo%>">
		</form>
<%	else
%>		<br><br><br><br>
		<span class="tbltext3">
			Non vi sono corsi disponibili per le scelte effettuate.
		</span>
		<br><br><br><br><br><br><br><br><br><br>
		<a href="JavaScript:self.close();">
			<img src="<%=session("progetto")%>/images/chiudi.gif" border="0">
		</a>
<%	end if
	
	RstCorFac.close
	set RstCorFac=nothing
End sub
'-------------------------------------------------------------------------------

'MAIN
%>
<!--#include virtual="/include/OpenConn.asp"-->
<!--#include virtual="/util/dbutil.asp"-->
<center>
<%
	call Inizio
	call ImpostaPag
%>
</center>
<!--#include virtual="/include/CloseConn.asp"-->
</body>
</html>
