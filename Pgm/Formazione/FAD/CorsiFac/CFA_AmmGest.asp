<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
	<script LANGUAGE="JavaScript">
	function ControllaDati()
	{
		if (document.frmGruppo.cmbGruppo.selectedIndex==0)
		{
			alert('Selezionare un gruppo')
			document.frmGruppo.cmbGruppo.focus()
			return false
		}
		
	return true	
	}
	
	function SubmPag(NomePag)
	{
		if (ControllaDati())
		{
			document.frmGruppo.action=NomePag;
			document.frmGruppo.submit();
		}
	}
	
	
	var windowArea
	function ApriFinestra(NomePag)
	{	
		var IdDescGruppo
	
		if (windowArea!=null)
		{
			windowArea.close();
		}
		if (ControllaDati())
		{
			IdDescGruppo=document.frmGruppo.cmbGruppo.value
			windowArea=window.open(NomePag + '?UIdDescGruppo=' + IdDescGruppo,'','toolbar=0,channelmode=0,location=0,menubar=0,status=0,width=550,height=450,Resize=No,Scrollbars=yes')
		}
	}
	</script>
<%

'-------------------------------------------------------------------------------
Sub Inizio
	dim sFunzione
	dim sTitolo
	dim sCommento
	dim bCampiObbl
	
	sFunzione = "GESTIONE CORSI FACOLTATIVI"
	sTitolo = "GESTIONE CORSI FACOLTATIVI"
	sCommento = "Testo del commento<br>per i Corsi facoltativi"
	bCampiObbl = false
%>
	<!--#include file="Testata.asp"-->
<%
End sub
'-------------------------------------------------------------------------------
Sub ImpostaPag
	dim sCodRorg
	dim sSql
	sCodRorg="UT"
	
	sSql="SELECT IDGRUPPO,DESGRUPPO " &_
		"FROM GRUPPO " &_
		"WHERE COD_RORGA='" & sCodRorg & "' " &_
		"ORDER BY DESGRUPPO"

'Response.Write sSql & "<br>"

'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	set Rst=CC.execute (sSql)
	if not Rst.eof then
		
%>	<br>
	<form name="frmGruppo" id="frmGruppo" method="post">
	<table border=0>
	<tr>
		<td class="tbltext1a"><b>Gruppo:</b>&nbsp;
		</td>
		<td>
			<SELECT id="cmbGruppo" name="cmbGruppo" class="textblacka">
			<OPTION></OPTION>
<%			do while not Rst.eof
%>				<OPTION value="<%=Rst("IDGRUPPO") & "|" & Rst("DESGRUPPO")%>"><%=Rst("DESGRUPPO")%></OPTION>
<%				Rst.MoveNext
			loop			
%>			</SELECT>
		</td>
	</tr>
	</table>
<%	
	end if
	
	Rst.close
	set Rst=nothing
	
%>	<br>
	<table border=0>
		<tr>
			<td width="30">&nbsp;</td>
			<td>
				<a href="JavaScript:SubmPag('CFA_ScelAreaTem.asp');" class="textred" OnMouseOver="JavaScript:window.status=''; return true;"><b>Aggiungi corso</b></a>
			</td>
			<td width="20">&nbsp;</td>
			<td>
				<a href="JavaScript:ApriFinestra('CFA_ModCorFac.asp')" class="textred" OnMouseOver="JavaScript:window.status=''; return true;"><b>Modifica catalogo</b></a>
			</td>
		</tr>
	</table>
	</form>
<%
end sub

'-------------------------------------------------------------------------------------
'M A I N
%>
<!--#include virtual="/strutt_testa2.asp"-->
<!--#include virtual="/include/openconn.asp"-->
<center>
<%
	call Inizio
	call ImpostaPag
%>
</center>
<!--#include virtual="/include/closeconn.asp"-->
<!--#include virtual="/strutt_coda2.asp"-->
