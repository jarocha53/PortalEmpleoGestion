<script language=javascript>
	function Carica_Pagina(npagina)
	{
		document.frmPagina.pagina.value = npagina;
		document.frmPagina.submit();
	}	
</script>
<%
'----------------------------------------------------------------------
Sub Inizio()
	dim sFunzione
	dim sTitolo
	dim sCommento
	dim bCampiObbl
	
	sFunzione = "CUESTIONARIO"
	sTitolo = "LISTA DE CUESTIONARIOS"
	sCommento = "Sus opiniones nos permiten mejorar constantemente nuestro servicio. " &_
				"En esta secci&oacute;n se pueden descargar las respuestas de los cuestionarios.<br>" &_
				"La descarga se realiza para cada secci&oacute;n que componga el cuestionario y es necesario " &_
				"hacer click en el icono <b>Secci&oacute;n cuestionarios</b>.<br>" 
				
	bCampiObbl = false
%>
	<!--#include virtual="/include/SetTestata.asp"-->
<%
End Sub
'----------------------------------------------------------------------
Sub ElencoQ()

	dim sSQL
	dim rsQuest
	dim nProg, nViewTit
	dim sCognome, sNome
	Set rsUtente = Server.CreateObject("ADODB.Recordset")
	
	sSQLUtente =	"SELECT cognome,nome from UTENTE " &_
					"where idutente = " & Session("idutente")
	'Response.Write 				
	rsUtente.Open sSQLUtente, CC, 3
	
	if not rsUtente.eof then
		sCognome= rsUtente("cognome")
		sNome= rsUtente("nome")
	end if
	
	nProg = 0
	nViewTit = 0
	
	Set rsQuest = Server.CreateObject("ADODB.Recordset")
	
	
	If Session("mask")= "0R" Then 'AMMINISTRATORE FORMAZIONE
			sSQL =	"SELECT id_infoquest, desc_quest, fl_repeat "
			sSQL = sSQL & " FROM info_quest ORDER BY id_infoquest"
		
	else
	'	sSQL =	"SELECT a.id_infoquest, i.desc_quest, i.fl_repeat"
		sSQL =	"SELECT a.id_infoquest, i.desc_quest, i.fl_repeat, cod_destinatario"
		
		IF SESSION("RUOFU") = "DI" THEN
			sSQL =	sSQL & ", a.fl_attivazione"
		END IF
		
		
			sSQL = sSQL & " FROM info_quest i, ass_quest a"
			 
			sSQL = sSQL &	" WHERE (a.cod_ruofu ='" & Session("Ruofu") & "' or a.cod_ruofu is null)" &_
							" AND (a.cod_rorga ='" & Session("rorga") & "' or a.cod_rorga is null)" &_
							" AND a.id_infoquest=i.id_infoquest"
		
		IF SESSION("RUOFU") = "DI" THEN
			sSQL =	sSQL &	" AND (a.cod_sessione ='" & aFase(1) & "' or a.cod_sessione is null)" &_
							" AND (a.id_bando in(select id_bando from domanda_iscr" &_
							" where id_persona = " & session("creator") & ") or a.id_bando is null)" 
		END IF
	
		sSQL =	sSQL & " ORDER BY a.id_infoquest"	
	end if

	'Response.Write  sSQL
	rsQuest.Open sSQL, CC, 3
	
	Record=0
	nTamPagina=5

	If Request("pagina")= "" Then
		nActPagina=1
	Else
		nActPagina=Clng(Request("pagina"))
	End If 
%>

	<table border="0" cellspacing="1" cellpadding="2" width="500">
		<tr class="sfondocommFad">
		    <td width="20" colspan="4" align="left">&nbsp;</td>
		</tr>
<%	
	IF NOT rsQuest.eof THEN
		rsQuest.PageSize	= nTamPagina
		rsQuest.CacheSize	= nTamPagina	
		nTotPagina			= rsQuest.PageCount		
		If nActPagina < 1 Then
			nActPagina = 1
		End If
	
		If nActPagina > nTotPagina Then
			nActPagina = nTotPagina
		End If
		rsQuest.AbsolutePage=nActPagina
		nTotRecord=0
		
		While rsQuest.EOF <> True And nTotRecord < nTamPagina
		
			nProg = nProg + 1
			
	
			If sQuestAttiv="no" then
				nProg = nProg - 1
			Else
				nViewTit = nViewTit + 1
				IF nViewTit = 1 THEN %>
					<tr class="sfondocommFad">
					    <td width="10" align="left"><b>#</b></td>
					    <td width="240"><b><center>T&iacute;tulo</center></b></td>
					    <td width="60" align="left"><B><center>Secci&oacute;n de Cuestionarios</center></B></td>
					    <td width="120" align="left"><B><center>Estado</center></B></td>
					</tr>
<%		END IF  %>
				<tr class="tblsfondoFad">
					<td align="left" class="tblDettFad">
						<%=nProg + (nActPagina * nTamPagina) - nTamPagina%>
					</td>
					<td>
						<%
						'if sEseguito="S" and cint(flRepeat)=0 then%> 
							<span class="tblAggFad"><%=rsQuest("desc_quest")%></span>
						<%
						'else%>
							<!--a onmouseover="window.status =' '; return true" 
								title="Seleziona questionario"
								href="Javascript:frmQuest<%=nProg%>.submit()" class="tblAggFad"-->
								<%'=rsQuest("desc_quest")%>
						<%
						'end if%>
					</td>
					<td align="center">
						<a href="javascript:frmAnteprima<%=nProg%>.submit();" class="tblAgg">
							<img src="/images/formazione/txt.gif" border="0" alt="Anteprima Questionario di Gradimento" WIDTH="20" HEIGHT="21">
						</a>
					</tr>
					<%
					Set rsInfo = Server.CreateObject("ADODB.Recordset")
					'sSQLInfo ="Select eseguito  from iq_risultato where id_infoquest=" & clng(rsQuest("id_infoquest"))
						'Response.Write "cc " & sSQLInfo
					sSQLInfo ="SELECT SUM(TOTALE) AS TOTALE FROM ( " &_
								"SELECT  DENOMINAZIONE_QUESTIONARIO AS DENOMINAZIONE_QUESTIONARIO,SUM(NUMERO_RISPOSTE) AS TOTALE  FROM (" &_
								"SELECT DESC_QUEST AS DENOMINAZIONE_QUESTIONARIO, COUNT(DISTINCT(B.IDUTENTE)) AS NUMERO_RISPOSTE " &_ 
								"FROM INFO_QUEST a, IQ_RISULTATO B" &_
								" WHERE A.ID_INFOQUEST=B.ID_INFOQUEST " &_
								" AND A.ID_INFOQUEST= " & clng(rsQuest("id_infoquest"))  &_
								" AND B.IDUTENTE IS NOT NULL " &_
								" AND ID_DESTINATARIO IS NULL " &_
								" GROUP BY DESC_QUEST,B.IDUTENTE" &_ 
								" UNION ALL" &_
								"(SELECT DESC_QUEST AS DENOMINAZIONE_QUESTIONARIO,COUNT(DISTINCT(B.ID_DESTINATARIO)) AS NUMERO_RISPOSTE " &_
								" FROM INFO_QUEST a, IQ_RISULTATO B" &_
								" WHERE A.ID_INFOQUEST=B.ID_INFOQUEST " &_
								" AND A.ID_INFOQUEST= " & clng(rsQuest("id_infoquest"))  &_
								" AND ID_DESTINATARIO IS NOT NULL" &_
								" GROUP BY DESC_QUEST, B.ID_DESTINATARIO)) A " &_
								" GROUP BY DENOMINAZIONE_QUESTIONARIO) AA "
									
						'Response.Write "SS " & sSQLInfo
						rsInfo.Open sSQLInfo, CC, 3
					if not rsInfo.eof then
						nEseg= rsInfo("TOTALE")
					end if
					%>
					<td align="center" class="tblAgg">
						<%If nEseg > "0" then
							Response.Write "Completado"
						else
							Response.Write "No completado"
						end if%>
					</td>	
				</tr>		
				<form method=post action="QGR_VisArea.asp" name="frmQuest<%=nProg%>">
					<input type=hidden name="id"	value="<%=clng(rsQuest("id_infoquest"))%>">
					<input type="hidden" name="mod" value="<%=3%>">
				</form>
				<form name="frmAnteprima<%=nProg%>" method="post" action="QGR_VisArea.asp" >
					<input type="hidden" name="id" value="<%=rsQuest("id_infoquest")%>">
					<input type="hidden" name="mod" value="<%=4%>">
					<input type="hidden" name="ero"  value="<%=sErogazione%>">
					<input type="hidden" name="frui" value="<%=sFruizione%>">
					<input type="hidden" name="commessa" value="<%=sCodCommessa%>">
					<input type="hidden" name="progetto" value="<%=sTestoProgetto%>">
					<input type="hidden" name="intervento" value="<%=intervento%>">
				</form>
<%			End if

		nTotRecord=nTotRecord + 1 
		rsQuest.movenext	
		
		Wend 
		
		If nViewTit = 0 then %>
			<tr>
				<TD align=center class="tblText3">
					No hay cuestionarios disponibles.
				</TD>
			</tr>
<%
		End if %>
	</table>
	
		<TABLE border=0 width=470 align=center>
			<form name="frmPagina" method="post" action="QGR_VisFrontGradimento.asp">
			    <input type="hidden" name="pagina" value>
			</form>	
			<tr>
				<%	
				if nActPagina > 1 then
					Response.Write "<td align=right width=480>"
					Response.Write "<A HREF=""javascript:Carica_Pagina('" & nActPagina-1 & "')"">"
					Response.Write "<img border=0 src=/images/precedente.gif></A></td>"
				end if
				
				if nActPagina < nTotPagina then			
						Response.Write "<td align=right>"
						Response.Write "<A HREF=""javascript:Carica_Pagina('" & nActPagina+1 & "')"">"		
						Response.Write "<img border=0 src=/images/successivo.gif></A></td>"
				end if
				%>
			</tr>
		</TABLE>	
<%	ELSE %>	
		<tr>
			<TD align=center class="tblText3">
				No hay cuestionarios disponibles.
			</TD>
		</tr>
	</table>
<%	END IF %>
	<br>
<%
	rsQuest.close
End Sub
'----------------------------------------------------------------------
'M A I N
%>
<!--#include virtual="/strutt_testa2.asp"-->
<!--#include virtual="/include/openconn.asp"-->
<!--#include virtual="/include/ControlDateVB.asp"-->
<!--#include virtual="include/SetPulsanti.asp"-->
<!--#include Virtual = "/include/ControlFase.asp"-->

<%
if ValidateService(session("idutente"),"QGR_VisFrontGradimento", CC) <> "true" then 
	response.redirect "/util/error_login.asp"
end if

dim sErogazione, sFruizione, sCodCommessa, sTestoProgetto, intervento
dim sCodDest, nTotQDir
dim sEseguo, nEseg

sErogazione = Request.Form ("ero")
sFruizione	= Request.Form ("frui")				 
sCodCommessa = Request.Form ("commessa")							 
sTestoProgetto = Request.Form ("progetto")					
intervento = Request.Form ("intervento")	
	
Inizio()
ElencoQ()
%>
<!--#include virtual="/include/closeconn.asp"-->
<!--#include virtual="/strutt_coda2.asp"-->