<!--#include virtual ="/Strutt_testa2.asp"-->
<!--#include virtual = "/Include/OpenConn.asp"-->
<!--#include virtual = "Include/HTMLEncode.asp"-->
<!--#include virtual="/util/portallib.asp"-->
<!--#include virtual="include/RuoloFunzionale.asp"-->
<!--#include virtual = "include/DecCod.asp"-->
<!--#include virtual = "/pgm/Comunicazioni/DecDizDati.asp"-->

<script language="javascript" src="Util.js"></script>
<center>
<body topmargin="0" onafterprint="javascript:buttom.style.visibility='visible';" onbeforeprint="javascript:buttom.style.visibility='hidden'">
<form name="frmIndietro" method="post" action="QGR_VisArea.asp">
<input type=hidden name="id"	value="<%=clng(idquest)%>">
<input type="hidden" name="mod" value="<%=3%>">
</form>  


<%sub Inizio()%>
<html>
<head>


</head>

<!-- *==============================******=============================* -->
	<body>
	<table border="0" width="520" height="81" cellspacing="0" cellpadding="0">
		<tr>
			<td width="500" height="81" background="/images/titoli/strumenti2b.gif" valign="bottom" align="right">
				<table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
					<tr>
						<td width="100%" valign="top" align="right">
							<b class="tbltext1a">Reportes Cuestionarios</b>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<br>
	<table cellpadding="0" cellspacing="0" width="500" border="0">
		<tr height="18">
			<td class="sfondomenu" height="18" width="60%">
				<span class="tbltext0">
					<b>&nbsp; REPORTES DE CUESTIONARIOS</b>
				</span>
			</td>
			<td width="3%" background="/images/tondo_linguetta.gif">
			</td>
			<td valign="middle" align="right" width="35%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1">(*) campos obligatorios
			</td>
		</tr>
		<tr width="371" class="SFONDOCOMM">
			<td colspan="3">
				Desde esta pagina se puede descargar en un archivo la informacion de los Cuestionarios.
				<a href="Javascript:Show_Help('/Pgm/help/Formazione/QGradimento/QGR_IndQuest')" name="prov3" onmouseover="javascript:window.status='' ; return true">
					<img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0">
				</a>
			</td>
		</tr>
		<tr height="2">
			<td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>
	


<%end sub
'''''''''''''''''''''''''''''''''''''
sub CaricamentoIntervistato(idArea,idQuest,idUtente)

dim sSQL,q, nRecord
	
sSQL = "SELECT distinct RIS.ID_INFOQUEST,RIS.ID_AREAIQ, RIS.IDUTENTE, RIS.COD_TIPO_DEST, RIS.ID_DESTINATARIO " 
sSQL = sSQL & "FROM IQ_RISULTATO RIS,IQ_RISPOSTE RES WHERE RIS.ID_RISULTATO = RES.ID_RISULTATO AND RIS.ID_AREAIQ = " & IdArea & " AND RIS.ID_INFOQUEST = " & idQuest 
	
set rsIntervistato = Server.CreateObject("ADODB.Recordset")

rsIntervistato.open sSQL,CC,3	
	
nRecord = (rsIntervistato.RecordCount -1)
redim mIntervistato(7, nRecord)
	q=0
	do until rsIntervistato.EOF
	
		if isnull(rsIntervistato("ID_DESTINATARIO")) then
		else
			''''compilazione indiretta
			if rsIntervistato("COD_TIPO_DEST")="P" then
				sql="SELECT COGNOME, NOME,COD_FISC FROM PERSONA WHERE ID_PERSONA=" & rsIntervistato("ID_DESTINATARIO")
				
				set rsPersona = Server.CreateObject("ADODB.Recordset")
				
				rsPersona.open sql,CC,3
				
				if not rsPersona.EOF then
					'mIntervistato(5,q)	= rsPersona("COGNOME") & " " &  rsPersona("NOME")
					mIntervistato(5,q)	= rsPersona("COGNOME") & " " &  rsPersona("NOME")
					mIntervistato(6,q)	= rsPersona("COD_FISC") 
				end if
				
				rsPersona.Close()
				set rsPersona = nothing
			end if
			
			if rsIntervistato("COD_TIPO_DEST")="S" then
				sql = "SELECT S.ID_IMPRESA,S.ID_SEDE,S.DESCRIZIONE,S.INDIRIZZO, S.COMUNE, C.DESCOM, I.ID_IMPRESA,I.RAG_SOC, I.PART_IVA FROM SEDE_IMPRESA S," &_
				" IMPRESA I, COMUNE C " &_
				" WHERE S.ID_SEDE =" & rsIntervistato("ID_DESTINATARIO") &_ 
				" AND S.COMUNE =C.CODCOM"  &_ 
				" AND S.ID_IMPRESA =I.ID_IMPRESA"
				
				set rsImpresa = Server.CreateObject("ADODB.Recordset")
		
				rsImpresa.open sql,CC,3
				
				if not rsImpresa.EOF then
					'mIntervistato(5,q)	= rsImpresa("RAG_SOC") & " " & rsImpresa("DESCRIZIONE") & " " & rsImpresa("INDIRIZZO")& " " & rsImpresa("COMUNE")
					mIntervistato(5,q)	=  rsImpresa("RAG_SOC") & " - " & rsImpresa("INDIRIZZO")& " " & rsImpresa("DESCOM")
					mIntervistato(6,q)	= rsImpresa("PART_IVA")
				end if
				
				rsImpresa.Close()
				set rsImpresa = nothing
			end if
		END IF	
			
			'''''compilazione diretta
			sql = "SELECT COGNOME, NOME, IDUTENTE FROM UTENTE WHERE " &_
				"IDUTENTE=" & rsIntervistato("IDUTENTE")
				
			set rsUtente = Server.CreateObject("ADODB.Recordset")
				
			rsUtente.open sql,CC,3
			
			if not rsUtente.EOF then	
				if isnull(rsIntervistato("ID_DESTINATARIO")) then
					mIntervistato(5,q)	= rsUtente("COGNOME") & " " &  rsUtente("NOME") 
					mIntervistato(6,q)	= rsUtente("IDUTENTE") 
					mIntervistato(7,q)	= ""
				else
					
					mIntervistato(7,q)	= rsUtente("COGNOME") & " " &  rsUtente("NOME") & " (" &  rsUtente("IDUTENTE") & ")"
				end if
			end if
				
			rsUtente.Close()
			set rsUtente = nothing
	
		
			mIntervistato(0,q)		= rsIntervistato("ID_INFOQUEST")
			mIntervistato(1,q)	= rsIntervistato("ID_AREAIQ")
			mIntervistato(2,q)	= rsIntervistato("IDUTENTE")
			mIntervistato(3,q)		= rsIntervistato("COD_TIPO_DEST")
			mIntervistato(4,q)	= rsIntervistato("ID_DESTINATARIO")
				
			q=q+1
			rsIntervistato.MoveNext()
	loop
	
	rsIntervistato.Close()
	set rsIntervistato = nothing


end sub

''''''''''''''''''''''''''''''''''''''''''''''
sub ScritturaRisposte()
dim q,x, RISPOSTA_UTENTE, FlagRisp, IdDom, IdDomPadre

q=0
x=0

'RISPOSTA_UTENTE =  chr(9) 
oFileObj.WriteLine (RISPOSTA_UTENTE)

For q=0 to Ubound(mIntervistato,2)

RISPOSTA_UTENTE=  mIntervistato(6,q) & chr(9) & mIntervistato(5,q) & chr(9) & mIntervistato(7,q)

	For x=0 To UBound(mStrutturaQuest, 2) 'scorrimento matrice con struttura quest
	
   		if mStrutturaQuest(1,x) ="D"  and   mStrutturaQuest(2,x) = 0 then  'tipo_elemento domanda
   				
   				For k=0 To UBound(mStrutturaQuest, 2) 'scorro la matrice per vedere la 
   													' tipologia di risposta associata
   				
			'verifica per id_elemento uguale all'id_domanda e livello diverso da zero
			
   					if cstr(mStrutturaQuest(6,k)) = cstr(mStrutturaQuest(0,x)) _
   					and cstr(mStrutturaQuest(2,k)) <> cstr(mStrutturaQuest(2,x)) _
   					and  mStrutturaQuest(1,k) ="R" then
   					
  
							if mIntervistato(4,q)<> ""  then
								sqlRisp1 = "SELECT RT.ID_RISULTATO, RES.ID_RISPOSTAIQ, RES.VALORE_RISPOSTA, RES.TESTO_RISPOSTA FROM IQ_RISULTATO RT , IQ_RISPOSTE RES " 
								sqlRisp1 = sqlRisp1 & " WHERE  RT.IDUTENTE = " & mIntervistato(2,q) & " AND RT.ID_RISULTATO = RES.ID_RISULTATO AND RES.ID_RISPOSTAIQ =  " & mStrutturaQuest(0,k) & " and cod_tipo_dest = '" & mIntervistato(3,q) & "' and id_destinatario = " & mIntervistato(4,q)   
							else
								sqlRisp1 = "SELECT RT.ID_RISULTATO, RES.ID_RISPOSTAIQ, RES.VALORE_RISPOSTA, RES.TESTO_RISPOSTA FROM IQ_RISULTATO RT , IQ_RISPOSTE RES " 
								sqlRisp1 = sqlRisp1 & " WHERE  RT.IDUTENTE = " & mIntervistato(2,q) & " AND RT.ID_RISULTATO = RES.ID_RISULTATO AND RES.ID_RISPOSTAIQ =  " & mStrutturaQuest(0,k) & " and id_destinatario is null "
							end if
	
				
							set rsRisp1 = Server.CreateObject("ADODB.Recordset")
 	
							rsRisp1.open sqlRisp1,CC,3
	
			
							if not rsRisp1.EOF then
									
								if rsRisp1("VALORE_RISPOSTA") > "" then
										RISPOSTA_UTENTE = RISPOSTA_UTENTE & chr(9) & rsRisp1("VALORE_RISPOSTA")
								else
									if rsRisp1("TESTO_RISPOSTA") > "" then
										  'textarea
										RISPOSTA_UTENTE = RISPOSTA_UTENTE & chr(9) & rsRisp1("TESTO_RISPOSTA")
									else
										if mStrutturaQuest(5,x)="M" then
											'checkbox				
											RISPOSTA_UTENTE = RISPOSTA_UTENTE & chr(9) & "Si"
										else
											if mStrutturaQuest(5,x)="E" then 'radio semplice
												'RISPOSTA_UTENTE = RISPOSTA_UTENTE & chr(9) & mStrutturaQuest(8, k)
												''05122007
												If IdDomPadre <> CStr(mStrutturaQuest(6, k)) Then
                                                
                                                    RISPOSTA_UTENTE = RISPOSTA_UTENTE & Chr(9) & mStrutturaQuest(8, k)
                                                    IdDomPadre = CStr(mStrutturaQuest(6, k))
                                                End If
                                                '''fine 05122007
											else
											'textarea senza risposta
												RISPOSTA_UTENTE = RISPOSTA_UTENTE & chr(9) & " "
											end if 
										end if
									end if
								end if	
						
								
								rsRisp1.Close()
								set rsRisp1 = nothing	
							else
								
								' Tipo Selezione = Multipla(M)	
   								if mStrutturaQuest(5,x)="M" then
			 						RISPOSTA_UTENTE = RISPOSTA_UTENTE & chr(9) & "No"
			 					else
			 						if mStrutturaQuest(5,x)="E" then
			 							 If IsNull(mStrutturaQuest(8, k)) Then 'radio con valore a cui non hanno
			 							 ' risposto          
                                            RISPOSTA_UTENTE = RISPOSTA_UTENTE & Chr(9)
                                            ''05122007
                                            
                                         else  
											If IdDomPadre <> CStr(mStrutturaQuest(6, k)) Then
                                                
                                                       If mIntervistato(4, q) <> "" Then
                                                            sqlVerifica = "SELECT RIS.TXT_RISPOSTA FROM IQ_RISULTATO RT , IQ_RISPOSTE RES, IQ_RISPOSTA RIS "
                                                             sqlVerifica = sqlVerifica & " WHERE  RT.IDUTENTE = " & mIntervistato(2, q) & " AND RT.ID_RISULTATO = RES.ID_RISULTATO AND RIS.ID_RISPOSTAIQ = RES.ID_RISPOSTAIQ AND RIS.ID_DOMANDAIQ =  " & mStrutturaQuest(6, k) & " and cod_tipo_dest = '" & mIntervistato(3, q) & "' and id_destinatario = " & mIntervistato(4, q)
                                
                                                        Else
                                                            sqlVerifica = "SELECT RIS.TXT_RISPOSTA FROM IQ_RISULTATO RT , IQ_RISPOSTE RES, IQ_RISPOSTA RIS "
                                                             sqlVerifica = sqlVerifica & " WHERE  RT.IDUTENTE = " & mIntervistato(2, q) & " AND RT.ID_RISULTATO = RES.ID_RISULTATO AND RIS.ID_RISPOSTAIQ = RES.ID_RISPOSTAIQ AND RIS.ID_DOMANDAIQ =  " & mStrutturaQuest(6, k) & " and id_destinatario is null "
                                                        End If
    
                                                        set rsVerifica = Server.CreateObject("ADODB.Recordset")
 	
														rsVerifica.open sqlVerifica,CC,3
                                                        
                                                        If Not rsVerifica.EOF Then
                                                         
                                                            RISPOSTA_UTENTE = RISPOSTA_UTENTE & Chr(9) & rsVerifica("TXT_RISPOSTA")

                                                            '''''
                                                            IdDomPadre = CStr(mStrutturaQuest(6, k))
                                                            
                                                            rsVerifica.Close()
															set rsVerifica = nothing	
                                                        Else
                                                            RISPOSTA_UTENTE = RISPOSTA_UTENTE & Chr(9)
                                                       
                                                        End If
											 end If
                                            
                                            ''' fine 05122007
                                         End If
			 						else
			 							RISPOSTA_UTENTE = RISPOSTA_UTENTE & chr(9)
			 						end if
   								end if	
   									
								
							end if
					
   						''''''''controllo se la risposta ha delle domande figlie
   							s=0
   							For s=0 To UBound(mStrutturaQuest, 2) 
   							
   						
   								   	if cstr(mStrutturaQuest(6,s)) = cstr(mStrutturaQuest(0,k)) _
   								   	and cstr(mStrutturaQuest(2,s)) <> cstr(mStrutturaQuest(2,k)) _ 
   								   	and mStrutturaQuest(1,s) ="D" then
   								   		
   								   		For w=0 To UBound(mStrutturaQuest, 2) 'scorro la matrice per vedere la 
   													' tipologia di risposta associata
   					
			'verifica per id_elemento uguale all'id_domanda e livello diverso da zero
			
   											if cstr(mStrutturaQuest(6,w)) = cstr(mStrutturaQuest(0,s)) _
   											and cstr(mStrutturaQuest(2,w)) <> cstr(mStrutturaQuest(2,s)) _
   											and  mStrutturaQuest(1,w) ="R" then
   											
   												''''***********************

   								   					if mIntervistato(4,q)<> "" then
														sqlRisp1 = "SELECT RT.ID_RISULTATO, RES.ID_RISPOSTAIQ, RES.VALORE_RISPOSTA, RES.TESTO_RISPOSTA FROM IQ_RISULTATO RT , IQ_RISPOSTE RES " 
														sqlRisp1 = sqlRisp1 & " WHERE  RT.IDUTENTE = " & mIntervistato(2,q) & " AND RT.ID_RISULTATO = RES.ID_RISULTATO AND RES.ID_RISPOSTAIQ =  " & mStrutturaQuest(0,w) & " and cod_tipo_dest = '" & mIntervistato(3,q) & "' and id_destinatario = " & mIntervistato(4,q)   
													else
														sqlRisp1 = "SELECT RT.ID_RISULTATO, RES.ID_RISPOSTAIQ, RES.VALORE_RISPOSTA, RES.TESTO_RISPOSTA FROM IQ_RISULTATO RT , IQ_RISPOSTE RES " 
														sqlRisp1 = sqlRisp1 & " WHERE  RT.IDUTENTE = " & mIntervistato(2,q) & " AND RT.ID_RISULTATO = RES.ID_RISULTATO AND RES.ID_RISPOSTAIQ =  " & mStrutturaQuest(0,w) & " and id_destinatario is null "
													end if
	
				
													set rsRisp1 = Server.CreateObject("ADODB.Recordset")
 	
													rsRisp1.open sqlRisp1,CC,3
	
													if not rsRisp1.EOF then
															
																
														if rsRisp1("VALORE_RISPOSTA") > "" then
															RISPOSTA_UTENTE = RISPOSTA_UTENTE & chr(9) & rsRisp1("VALORE_RISPOSTA")
														else
															if rsRisp1("TESTO_RISPOSTA") > "" then
																	'textarea
																	RISPOSTA_UTENTE = RISPOSTA_UTENTE & chr(9) & rsRisp1("TESTO_RISPOSTA")
															else	
																if mStrutturaQuest(5,s)="M" then
																	'checkbox
																	RISPOSTA_UTENTE = RISPOSTA_UTENTE & chr(9) & "Si"
																else
																	if mStrutturaQuest(5,s)="E" then 'radio semplice
																		'RISPOSTA_UTENTE = RISPOSTA_UTENTE & chr(9) & mStrutturaQuest(8, w)
																		 If FlagRisp = "OK" Then 'flag di avanzamento di colonna
                                                                            RISPOSTA_UTENTE = RISPOSTA_UTENTE & mStrutturaQuest(8, w)
                                                                        Else
                                                                            RISPOSTA_UTENTE = RISPOSTA_UTENTE & Chr(9) & mStrutturaQuest(8, w)
                                                                        End If
                                                                        '''''
                                                                        IdDom = CStr(mStrutturaQuest(6, w))'acquisizione dell'id_domanda
																	else
																	'textarea senza risposta
																		RISPOSTA_UTENTE = RISPOSTA_UTENTE & chr(9) & " "
																	end if 
																end if
															end if
														end if	
															
														rsRisp1.Close()
														set rsRisp1 = nothing
													else
														
														' Tipo Selezione = Multipla(M)	checkbox imputato "No"
   														if mStrutturaQuest(5,s)="M" then
			 												RISPOSTA_UTENTE = RISPOSTA_UTENTE & chr(9) & "No"
   														else
   															if mStrutturaQuest(5,s)="E" then 'radio figlia
   																If IsNull(mStrutturaQuest(8, w)) Then 'non risposto alla radio
   																' con valore
                                                                        
                                                                     RISPOSTA_UTENTE = RISPOSTA_UTENTE & Chr(9)
                                                                 else
                                                                    If IdDom <> CStr(mStrutturaQuest(6, w)) Then 'verifica se
                                                         'l'id_domanda � differente
                                                                        If FlagRisp = "" Then 'verifica di avanzamento
                                                                        'colonna se ho gi� avanzato per radio senza risposta
                                                                            RISPOSTA_UTENTE = RISPOSTA_UTENTE & Chr(9)
                                                                            FlagRisp = "OK"
                                                                        End If
                                                                    End If
                                                                 	
                                                                 end if
                                             
   															else
			 													RISPOSTA_UTENTE = RISPOSTA_UTENTE & chr(9)
			 												end if
   														end if		
   															
														
													end if
   										
											end if 'if per risposta figlia
   										next ' ciclo per risposta figlia		
   									
   									end if ' if per domanda figlia
   								FlagRisp = ""
 							next   ' ciclo per domanda figlia
 					  end if
				   next' ciclo per risposta padre
		   end if
		
	next' ciclo per domanda padre
	
RISPOSTA_UTENTE = Replace (RISPOSTA_UTENTE, "&#8217;", "'")
RISPOSTA_UTENTE = Replace (RISPOSTA_UTENTE, "&#224;", "�")
RISPOSTA_UTENTE = Replace (RISPOSTA_UTENTE, "&quot;", "''")
RISPOSTA_UTENTE = Replace (RISPOSTA_UTENTE, "&#8220;", "''")
RISPOSTA_UTENTE = Replace (RISPOSTA_UTENTE, "&#8221;", "''")
RISPOSTA_UTENTE = Replace (RISPOSTA_UTENTE, "&#225;", "�") 
	
	oFileObj.WriteLine (RISPOSTA_UTENTE)
	RISPOSTA_UTENTE = ""
	IdDom = ""
	IdDomPadre = ""
next ' ciclo intervistati
end sub
''''''''''''''''''''''''''''''''
sub ScritturaTitoli()
dim x,k,w,s, sTipoRisp, sElementoDom, sRiga, iLivello, IdPadre, IdPadreSecondo


oFileObj.WriteLine ( sDescrQuest & " / " & sDescrArea)
oFileObj.WriteLine ("")

''''''''''''''''''''''''''''''''''''''
sRiga =  "Identificativo Intevistato" & chr(9) & "Nome Intervistato" &  chr(9) & "Intervistatore"

x=0

For x=0 To UBound(mStrutturaQuest, 2) 'scorrimento matrice con struttura quest
	    
   		if mStrutturaQuest(1,x) ="D"  and   mStrutturaQuest(2,x) = 0 then  'tipo_elemento domanda
   				
   				For k=0 To UBound(mStrutturaQuest, 2) 'scorro la matrice per vedere la 
   													' tipologia di risposta associata
   					
			'verifica per id_elemento uguale all'id_domanda e livello diverso da zero
			
   					if cstr(mStrutturaQuest(6,k)) = cstr(mStrutturaQuest(0,x)) _
   					and cstr(mStrutturaQuest(2,k)) <> cstr(mStrutturaQuest(2,x)) _
   					and  mStrutturaQuest(1,k) ="R" then

						' Tipo Selezione = Multipla(M)	
   							if mStrutturaQuest(5,x)="M" then
			 					if mStrutturaQuest(4,x)= "S" then
			 							
			 						if mStrutturaQuest(13,k)= "A" then ' checkbox + textarea
			 							sRiga = sRiga & chr(9) & mStrutturaQuest(3,x) & " Se " &  mStrutturaQuest(8,k)
			 						else
			 							sRiga = sRiga & chr(9) & mStrutturaQuest(3,x) & " Se " &  mStrutturaQuest(8,k)
			 						end if
			 					end if	
			 											
			 					if mStrutturaQuest(4,x)="C" then
			 						sRiga = sRiga & chr(9) & mStrutturaQuest(3,x) & " Se " &  mStrutturaQuest(8,k)
			 					end if
   							end if	
   							
   						' Tipo Selezione = Escludente(E)
						if mStrutturaQuest(5,x)="E" then
						
							if IdPadre<>cstr(mStrutturaQuest(6,k)) then	
								if mStrutturaQuest(4,x)= "C" then' radio tipo "T"
									sRiga = sRiga & chr(9) & mStrutturaQuest(3,x)
								end if											
								if mStrutturaQuest(4,x)= "S" then ' radio + textarea
									sRiga = sRiga & chr(9) & mStrutturaQuest(3,x) & " Se " &  mStrutturaQuest(8,k)
								end if
								IdPadre = cstr(mStrutturaQuest(6,k))
							end if
						end if	
						'Tipo Selezione = Testo libero (A)	
							if mStrutturaQuest(4,x)= "A" then
								sRiga = sRiga & chr(9) & mStrutturaQuest(3,x)
								
			  				end if
   						
						'oFileObj.WriteLine (sRiga)  ''''INSERISCO SUL FILE il primo livello
   	
   						''''''''controllo se la risposta ha delle domande figlie
   							s=0
   							For s=0 To UBound(mStrutturaQuest, 2) 
   							
   								   	if cstr(mStrutturaQuest(6,s)) = cstr(mStrutturaQuest(0,k)) _
   								   	and cstr(mStrutturaQuest(2,s)) <> cstr(mStrutturaQuest(2,k)) _ 
   								   	and mStrutturaQuest(1,s) ="D" then
   								   		
   								   		For w=0 To UBound(mStrutturaQuest, 2) 'scorro la matrice per vedere la 
   													' tipologia di risposta associata
   					
			'verifica per id_elemento uguale all'id_domanda e livello diverso da zero
			
   											if cstr(mStrutturaQuest(6,w)) = cstr(mStrutturaQuest(0,s)) _
   											and cstr(mStrutturaQuest(2,w)) <> cstr(mStrutturaQuest(2,s)) _
   											and  mStrutturaQuest(1,w) ="R" then
   											
   						
   								   		
   									   			' Tipo Selezione = Multipla(M)	CHECKBOX
   												if mStrutturaQuest(5,x)="M" then ' checkbox padre
			 										IF mStrutturaQuest(5,s)="M" then ' checkbox figlia
			 											sRiga = sRiga & chr(9) & mStrutturaQuest(3,x) & " Se " &  mStrutturaQuest(8,k) & " - " & mStrutturaQuest(3,s) & ": " & mStrutturaQuest(8,w)
			 										else 
			 											if mStrutturaQuest(5,s)="E" then 'radio figlia
														   If IdPadreSecondo <> CStr(mStrutturaQuest(6, w)) Then
                                                                sRiga = sRiga & Chr(9) & mStrutturaQuest(3, x) & " Se " & mStrutturaQuest(8, k) & " - " & mStrutturaQuest(3, s) & ":"
                                                                IdPadreSecondo = CStr(mStrutturaQuest(6, w))
                                                           End If
                                                        else
			 												' tutti gli altri casi
			 												sRiga = sRiga & chr(9) & mStrutturaQuest(3,x) & " Se " &  mStrutturaQuest(8,k) & " - " & mStrutturaQuest(3,s) & ": "
			 											end if
			 										end if
   												end if	
   												
   												
   												'Tipo Selezione = Escludente(E)	
												if mStrutturaQuest(5,x)="E" then ' radio padre
														if mStrutturaQuest(5,s)="E" then 'radio figlia
														   If IdPadreSecondo <> CStr(mStrutturaQuest(6, w)) Then
                                                                sRiga = sRiga & Chr(9) & mStrutturaQuest(3, x) & " Se " & mStrutturaQuest(8, k) & " - " & mStrutturaQuest(3, s) & ":"
                                                                IdPadreSecondo = CStr(mStrutturaQuest(6, w))
                                                            End If
                                                        end if
														if mStrutturaQuest(4,s)= "A" then 'textarea figlia
															sRiga = sRiga & chr(9) & mStrutturaQuest(3,x) & " " &  mStrutturaQuest(8,k) & " " &  mStrutturaQuest(3,s) & ": "
														end if
														IF mStrutturaQuest(5,s)="M" then ' checkbox figlia
															sRiga = sRiga & chr(9) & mStrutturaQuest(3,x) & " Se " &  mStrutturaQuest(8,k) & " - " & mStrutturaQuest(3,s) & ": " & mStrutturaQuest(8,w)
														end if
												end if
											
												'Tipo Selezione = Testo libero (A)	
												if mStrutturaQuest(4,x)= "A" then 'textarea padre
													if mStrutturaQuest(5,s)="E" then 'radio figlia
														sRiga = sRiga & chr(9) & mStrutturaQuest(3,x) & " Se " &  mStrutturaQuest(8,k) & " - " & mStrutturaQuest(3,s) & ": "
													end if
													if mStrutturaQuest(4,s)= "A" then 'textarea figlia
														sRiga = sRiga & chr(9) & mStrutturaQuest(3,x) & " " &  mStrutturaQuest(8,k) & " " &  mStrutturaQuest(3,s) & ": "
													end if
													IF mStrutturaQuest(5,s)="M" then ' checkbox figlia
														sRiga = sRiga & chr(9) & mStrutturaQuest(3,x) & " Se " &  mStrutturaQuest(8,k) & " - " & mStrutturaQuest(3,s) & ": " & mStrutturaQuest(8,w)
													end if
														
			  									end if
   												
   												'oFileObj.WriteLine (sRiga)  ''''INSERISCO SUL FILE il secondo livello
   												   		
											end if 'if per risposta figlia
   										next ' ciclo per risposta figlia		
   									
   									end if ' if per domanda figlia
   			
 							next   ' ciclo per domanda figlia
 					end if
				next		
		end if
		
next
sRiga = Replace (sRiga, "&#8217;", "'")
sRiga = Replace (sRiga, "&#224;", "�")
sRiga = Replace (sRiga, "&quot;", "''")
sRiga = Replace (sRiga, "&#8220;", "''")
sRiga = Replace (sRiga, "&#8221;", "''")
sRiga = Replace (sRiga, "&#225;", "�") 

oFileObj.WriteLine (sRiga)
end sub
'''''''''''''''''''''''''''''''''''''''''''''
sub CaricamentoMQuestionario(rt,liv)

dim i,y, sql, nEle, Lunghezza


sql =  " SELECT ID_INFOQUEST, ID_AREAIQ, ID_BLOCCO, TIPO_ELEMENTO, ID_ELEMENTO, ID_ELEM_ORIGINE, DT_TMST " & _
	" FROM STRUTTURA_QUEST " & _
	" WHERE ID_AREAIQ = " & IdArea & " AND ID_ELEM_ORIGINE = " & rt & " AND ID_INFOQUEST = " & idquest & _
	" ORDER BY ID_BLOCCO, ID_ELEMENTO"
	
	set rsStruttura = Server.CreateObject("ADODB.Recordset")
 	
 	rsStruttura.open sql,CC,3
	
	If rsStruttura.RecordCount>0 then

			nEle = rsStruttura.RecordCount
			'''''''''''''''''''''''''''''
			for y =0 to Ubound(mIndici, 1)
				if liv = mIndici(y , 0) then

					i = mIndici(y , 3)
					Lunghezza = nEle + mIndici(y , 2)
					exit for
				end if
					
			next		
		
		if UBound(mQuestionario, 3)< Lunghezza then
			 
			redim preserve mQuestionario(6, 3, Lunghezza)
		end if
	'	
		do until rsStruttura.EOF
	
				mQuestionario(0,liv,i)		= trim(rsStruttura("ID_INFOQUEST"))
				mQuestionario(1,liv,i)	= trim(rsStruttura("ID_AREAIQ"))
				mQuestionario(2,liv,i)	= trim(rsStruttura("ID_BLOCCO"))
				mQuestionario(3,liv,i)		= trim(rsStruttura("TIPO_ELEMENTO"))
				mQuestionario(4,liv,i)	= trim(rsStruttura("ID_ELEMENTO"))
				mQuestionario(5,liv,i)	= trim(rsStruttura("ID_ELEM_ORIGINE"))
				mQuestionario(6,liv,i)		= trim(rsStruttura("DT_TMST"))
				
				i = i + 1
				rsStruttura.MoveNext()
				
		loop
		''''''''''''
			y = 0	
			for y =0 to Ubound(mIndici, 1)
				if liv = mIndici(y , 0) then
					mIndici(y , 2) = i -1
					exit for
				end if
					
			next
			
		'''''''''''''''
		Esito_ElemOrigine = "OK"
	else 
		liv = liv - 1
		Esito_ElemOrigine = "KO"
	end if
	
	rsStruttura.Close()
	set rsStruttura = nothing

end sub
''''''''''''''''''''''''''''''''''''''''''
sub CaricamentoMatriceStruttura(liv)

dim j, w, y, sqlRisp, sqlRisp1, sqlDom, sqlDom1, IdElem, somma


		y=0
		for y =0 to Ubound(mIndici, 1)
			if liv = mIndici(y , 0) then
					iIndiceMin = mIndici(y , 3)
					iIndiceMax = mIndici(y , 2)	
					exit for
			end if			
		next 

		For j=iIndiceMin To iIndiceMax
		
	
			''LETTURA DOMANDA
			if mQuestionario(3, liv, j) = "D" then
				
				sqlDom = "SELECT TESTO_DOMANDA, TIPO_DOMANDA, TIPO_SEL, ID_DOMANDAIQ "
				sqlDom = sqlDom & " FROM IQ_DOMANDA WHERE ID_DOMANDAIQ =" & mQuestionario(4, liv, j)
				
				set rsDom = Server.CreateObject("ADODB.Recordset")
 	
				rsDom.open sqlDom,CC,3
				
				if liv=0 then
					ID_DOMANDAIQ = rsDom("ID_DOMANDAIQ")
				else
					ID_DOMANDAIQ = mQuestionario(5, liv, j)
				end if
				TESTO_DOMANDA =	rsDom("TESTO_DOMANDA")

			
				TIPO_DOMANDA = rsDom("TIPO_DOMANDA")
				TIPO_SEL =	rsDom("TIPO_SEL")
				
				rsDom.Close()
				set rsDom = nothing
			
			end if
			
			''LETTURA RISPOSTA
				RISPOSTA_UTENTE = ""
			
			if mQuestionario(3, liv, j) = "R" then
				
				sqlRisp = "SELECT ID_DOMANDAIQ, TIPO, TXT_RISPOSTA, TESTO_MAX, TESTO_MIN, IQ_MIN, IQ_MAX, FL_TIPRISP "
				sqlRisp = sqlRisp & " From IQ_RISPOSTA where ID_RISPOSTAIQ = " & mQuestionario(4, liv, j)
			
				set rsRisp = Server.CreateObject("ADODB.Recordset")
 	
				rsRisp.open sqlRisp,CC,3
			
				ID_DOMANDAIQ = rsRisp("ID_DOMANDAIQ")
				TIPO_RISPOSTA = rsRisp("TIPO")
				TXT_RISPOSTA = rsRisp("TXT_RISPOSTA")
				TESTO_MAX = rsRisp("TESTO_MAX")
				TESTO_MIN = rsRisp("TESTO_MIN")
				IQ_MIN = rsRisp("IQ_MIN")
				IQ_MAX = rsRisp("IQ_MAX")
				FL_TIPRISP = rsRisp("FL_TIPRISP")
				
			
				rsRisp.Close()
				set rsRisp = nothing
				
				sqlDom1 = "SELECT TIPO_DOMANDA, TIPO_SEL "
				sqlDom1 = sqlDom1 & " FROM IQ_DOMANDA WHERE ID_DOMANDAIQ =" & ID_DOMANDAIQ
				
				set rsDom1 = Server.CreateObject("ADODB.Recordset")
 	
				rsDom1.open sqlDom1,CC,3
				
				TIPO_DOMANDA = rsDom1("TIPO_DOMANDA")
				TIPO_SEL =	rsDom1("TIPO_SEL")
	
				rsDom1.Close()
				set rsDom1 = nothing
				
				
			end if
			
			'testuale[il] = RISPOSTA_UTENTE
			
			w =(Ubound(mStrutturaQuest, 2))
			
			mStrutturaQuest(0,w) = mQuestionario(4, liv, j) 'id_elemento
			mStrutturaQuest(1,w) = mQuestionario(3,liv, j)	'tipo_elemento
			mStrutturaQuest(2,w) =	liv
			mStrutturaQuest(3,w) =	TESTO_DOMANDA
			mStrutturaQuest(4,w) =	TIPO_DOMANDA
			mStrutturaQuest(5,w) =	TIPO_SEL
			mStrutturaQuest(6,w) = ID_DOMANDAIQ
			mStrutturaQuest(7,w) = TIPO_RISPOSTA
			mStrutturaQuest(8,w) = TXT_RISPOSTA
			mStrutturaQuest(9,w) = TESTO_MAX
			mStrutturaQuest(10,w) = TESTO_MIN
			mStrutturaQuest(11,w) = IQ_MIN
			mStrutturaQuest(12,w) = IQ_MAX
			mStrutturaQuest(13,w) = FL_TIPRISP
			mStrutturaQuest(14,w) = ""  'RISPOSTA_UTENTE
			mStrutturaQuest(15,w) = mQuestionario(2, liv ,j) 'id_blocco
			
			redim preserve mStrutturaQuest(15, UBound(mStrutturaQuest, 2) + 1)
			
			IdElem = mQuestionario(4, liv, j)
			somma = liv + 1
	
			y = 0
			for y =0 to Ubound(mIndici, 1)
				if liv = mIndici(y , 0) then
					If mIndici(y, 3) <> mIndici(y, 2) Then
						mIndici(y , 3) = j + 1 ' minimo
						exit for
					end if
				end if
					
			next 
	
			 if liv<4 then
	
					call CaricamentoStrutturaQuest(IdElem,somma)
					y=0
					for y =0 to Ubound(mIndici, 1)
						if liv = mIndici(y , 0) then
							iIndiceMin = mIndici(y , 3)
							iIndiceMax = mIndici(y , 2)
							exit for
						end if
							
					next 
				''''''inserito mercoled�
			else
				liv=liv-1
				y=0
					for y =0 to Ubound(mIndici, 1)
						if liv = mIndici(y , 0) then
							iIndiceMin = mIndici(y , 3)
							iIndiceMax = mIndici(y , 2)
							exit for
						end if
							
					next 	
			end if		
				''''''''fine inserito mercoled�
		Next
		
end sub

''''''funzioni di caricamento delle matrici
sub CaricamentoStrutturaQuest(rt,liv) 

call CaricamentoMQuestionario(rt,liv)

if Esito_ElemOrigine = "OK" Then
	call CaricamentoMatriceStruttura(liv)
end if

if Esito_ElemOrigine = "KO" Then exit sub
   
end sub
'''''''''''''''''''''''''''''''''''''''''''''''''''
sub Fine()%>
	<table width="488" cellspacing="2" cellpadding="1" border="0" align="center">
	
		<tr>
			<td class="tbltext1" align="left">
				<br>
				<b>Cuestionario: <%=sDescrQuest%></b><br>
				<br>
				<b>Seccion: <%=sDescrArea%></b>
			</td>
		</tr>
		<tr>
			<td class="tbltext1" align="left">
				<br>
				<b>EL REPORTE HA SIDO CREADO CORRECTAMENTE</b><br>
				<br>
			</td>
		
		</tr>
		<tr align="center">
			<td>
				<!--<a href="javascript:self.close()"><img src="<%=Session("progetto")%>/images/chiudi.gif" title="Chiudi la pagina" border="0" align="absBottom"></a>
				<a href="javascript:print()"><input type="image" src="<%=Session("progetto")%>/images/stampa.gif" title="Stampa la pagina" border="0" align="center" onclick="self.print();" id="stampa" name="stampa"></a>-->
			</td>
		</tr>		
		<tr align="center">
			<td>&nbsp;</td>
		</tr>		
		<tr align="center">
			<td>
				<a class="textred" href="javascript:Scarica('<%=sFileNameJS%>')"><b>Abrir reporte</b></a>
			</td>
		</tr>		
	</table>

<%end sub


' *************************************************************************************
''''MAIN

dim mIntervistato()
dim mQuestionario()
dim mStrutturaQuest()
dim mIndici()

dim idquest,IdArea, IdUtente, sDescrQuest, sDescrArea

'''''lunghezze matrici
dim iLungPrima, iLungSeconda, iLungTerza, iLungQuarta
dim iIndiceMin, iIndiceMax
dim rsArea,rsQuest,sSQL

IdArea = Request("ida")
idquest = Request.Form("idq")
IdUtente = session("idutente")

ReDim mQuestionario(6,3,0)

ReDim mIntervistato(2, 0)

ReDim mStrutturaQuest(15, 0)
redim mIndici(3, 4)

'matrice indice 0 = liv
'matrice indice 1 = inizio
'matrice indice 2 = fine
'matrice indice 3 = inizio indice
'matrice indice 4 = fine indice

mIndici(0, 0)= 0
mIndici(1, 0)= 1
mIndici(2, 0)= 2
mIndici(3, 0)= 3

mIndici(0, 1)= 0
mIndici(1, 1)= 0
mIndici(2, 1)= 0
mIndici(3, 1)= 0

mIndici(0, 2)= 0
mIndici(1, 2)= 0
mIndici(2, 2)= 0
mIndici(3, 2)= 0

mIndici(0, 3)= 0
mIndici(1, 3)= 0
mIndici(2, 3)= 0
mIndici(3, 3)= 0

Esito_ElemOrigine = "OK"
''''''''''''''''''''''''''''''''''''
'descrizione dell'area
sSQL = "select id_areaiq, desc_areaiq from iq_area where id_areaiq = '" & IdArea  & "' "

set rsArea = server.CreateObject ("adodb.recordset")
rsArea.open sSQL,CC,3

sDescrArea = rsArea("desc_areaiq")

rsArea.Close()
set rsArea = nothing

'descrizione del questionario
sSQL = "Select desc_quest, intest_quest,fl_repeat from info_quest where id_infoquest = " & idquest

set rsQuest =server.CreateObject ("adodb.recordset")
rsQuest.open sSQL,CC,3

sDescrQuest = rsQuest("desc_quest")

rsQuest.Close()
set rsQuest = nothing
''''''''''''''''''''''''''''''''''''''''
inizio()

server.ScriptTimeout = server.ScriptTimeout * 10

call CaricamentoStrutturaQuest(0, 0)


sFileName = Server.MapPath("/") & session("Progetto") & "/Docpers/Rep_Questionari/" &_
	 idquest & "_" & IdArea & ".xls"

sFileNameJS = session("Progetto") & "/DocPers/Rep_Questionari/" &_
	idquest & "_" & IdArea & ".xls"

set oFileSystemObject	= Server.CreateObject("Scripting.FileSystemObject")
set oFileObj = oFileSystemObject.CreateTextFile(replace(sFileName, "/","\"))

ScritturaTitoli()

call CaricamentoIntervistato(IdArea,idquest,idUtente)

ScritturaRisposte()

Fine()
server.ScriptTimeout = server.ScriptTimeout / 10
' ************************************************************************************
%>