<%
Sub Inizio
	dim sFunzione
	dim sTitolo
	dim sCommento
	dim bCampiObbl
	
	sFunzione = "GESTION DE CUESTIONARI "
	sTitolo = "ELIMINACION DEL CUESTIONARIO"
	sCommento = "Confirmación de la eliminación del Cuestionario"
	bCampiObbl = false
%>
	<!--#include virtual="include/SetTestata.asp"-->
<%
End Sub
'--------------------------------------------------------------------------
Sub Cancella()

	dim nIdQuest, dDtTmst
	dim sSQL, sSQL2, rsRisp


'''9/7/2003: RICEVUTI VALORI DA FORM ANZICHE' QUERYSTRING 
	'''nIdQuest = Request("id")
	'''dDtTmst = Request("tmst")
	nIdQuest = Request("IdQuest")
	dDtTmst = Request("dtTmst")
'''FINE 9/7/2003

	'Controllo che non esistano Aree associate al questionario
	sSQL =  " select count(*) as CAre" &_
			" from iq_area" &_
			" where id_infoiq = " & nIdQuest
	set rsCAre = cc.execute(sSQL)
		nContaAree = cint(rsCAre("CAre"))
	rsCAre.close
	set rsCAre = nothing
	
	'Controllo che non esistano associazioni del questionario in ass_quest
	sSQL =  " select count(*) as CAsso" &_
			" from ass_quest" &_
			" where id_infoquest = " & nIdQuest
	set rsCAsso = cc.execute(sSQL)
		nContaAsso = cint(rsCAsso("CAsso"))
	rsCAsso.close
	set rsCAsso = nothing
	
		if nContaAree > 0 or nContaAsso > 0 then
%>
			<br><br>
			<table border=0 cellspacing=2 cellpadding=1 width="500">
				<tr align=middle>
					<td class="tbltext3">
						No es posible eliminar el Cuestionario.<br>
						Eliminar antes las Areas asociadas y/o eliminar 
						las asociaciones del Cuestionario al Rol y a la Sesión.
					</td>
				</tr>
				<tr align=middle>
					<td align="center">
						&nbsp;<br>
						<%PlsIndietro()%>
					</td>
				</tr>
			</table>		
			<br><br>
<%
		else
			sSQL = "delete from info_quest where id_infoquest = " & nIdQuest
			sErrore=Esegui(nIdQuest ,"info_quest",Session("persona"),"DEL",sSQL,0,dDtTmst)
		  	
			IF sErrore <> "0" then
%>
				<br><br>
				<table border=0 cellspacing=2 cellpadding=1 width="500">
					<tr align=middle>
						<td class="tbltext3">
							Imposible eliminar el Cuestionario.
							<BR>
							Errore: <%=sErrore%>
						</td>
					</tr>	
					<tr align=middle>	
						<td align="center">
							&nbsp;<br>
							<%PlsIndietro()%>
						</td>
					</tr>
				</table>		
				<br><br>
<% 
			ELSE %>
				<br><br>
				<table border=0 cellspacing=2 cellpadding=1 width="500">
				<tr align=middle>
						<td class=tbltext3>
							Eliminacion del Cuestionario correctamente efectuada
						</td>
					</tr>
				</table>
				<br><br>
<%
			END IF
		end if
	
%>
<br>
<table width=500>
	<tr>
		<td align=center>
			<%
			PlsLinkRosso "QGR_VisQuestionario.asp", "Lista de Cuestionarios"
			%>
		</td>
	</tr>
</table>
<%
End Sub
'------------------------------------------------------------------------
'M A I N
%>
<!--#include virtual="/strutt_testa2.asp"-->
<!--#include virtual="/include/openconn.asp"-->
<!--#include virtual="/Util/DBUtil.asp"-->
<!--#include virtual="/Include/SysFunction.asp"-->
<!--#include virtual="include/SetPulsanti.asp"-->
<%	
	Inizio()
	Cancella()
%>
<!--#include virtual="/include/closeconn.asp"-->
<!--#include virtual="/strutt_coda2.asp"-->
