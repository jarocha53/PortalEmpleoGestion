<script LANGUAGE="Javascript">

function ChiudiTutto()
{
	opener.document.frmReload.submit();
	self.close();

}
</script>
<%
Sub Inizio
	dim sFunzione
	dim sTitolo
	dim sCommento
	dim bCampiObbl
	
	sFunzione = "GESTION DE CUESTIONARIOS"
	sTitolo = "MODIFICA SECCION"
	sCommento = "Confirmación de la modificación de la Sección"
	bCampiObbl = false
%>
	<!--#include virtual="include/SetTestata.asp"-->
<%
End Sub
'--------------------------------------------------------------------------
Sub Conferma()
dim DescA, nIdArea, nIdQuest, dDtTmst
dim sSQL

DescA = strHTMLEncode(Trim(Request.Form("DescArea")))
DescA = replace(DescA,"'","''")

nIdArea = Request.Form("IdArea")
nIdQuest = Request.Form("IdQuest")
dDtTmst = Request.Form("dtTmst")

Adesso="TO_DATE('" & day(now) & "/" & _
		   month(now) & "/" & year(now) & _
		   " " & hour(now) & ":" & minute(now) & ":" & second(now) & _
		   "','DD/MM/YYYY HH24:MI:SS')"	

sSQL = "UPDATE iq_area SET desc_areaiq = '" & DescA & "', dt_tmst = " & Adesso & " where id_areaiq = " & nIdArea

sErrore = Esegui(nIdArea,"IQ_AREA",Session("persona"),"MOD",sSQL,1,dDtTmst)		

if sErrore="0" then
%>
<br><br>
<table border="0" cellspacing="2" cellpadding="1" width="500">
	<tr align="middle">
		<td class="tbltext3">
			Modificación de la sección correctamente efectuada
		</td>
	</tr>
</table>
<%
else
%>
<br><br>
<table border="0" cellspacing="2" cellpadding="1" width="500">
	<tr align="middle">
		<td class="tbltext3">
			Error en la modificación. <%=sErrore%>
		</td>
	</tr>
</table>
<%
end if
%>
<br><br>


<%'''8/7/2003: PASSATI VALORI TRAMITE FORM ANZICHE' QUERYSTRING %>	
	<form name="frmArea" id="frmArea" method="post" action="QGR_ModQuest.asp">
		<input type="hidden" name="id" value="<%=nIdQuest%>">

	<td colspan="2" align="center">
	<a href="javascript:ChiudiTutto()">
		<img src="<%=Session("progetto")%>/images/chiudi.gif" border="0" alt="Cerrar">
	</a>
	&nbsp;
		
	</form>
	
	
<%'''FINE 8/7/2003

End Sub
'------------------------------------------------------------------------
'M A I N
%>
<html>
<head>
<title><%=Session("TIT")%> [ <%=Session("login")%> ] </title>
<link REL="STYLESHEET" TYPE="text/css" HREF="<%=session("progetto")%>/fogliostile.css">
</head>
<body class="sfondocentro" topmargin="2" leftmargin="0">
<center>
<!--#include virtual="/include/openconn.asp"-->
<!--#include virtual="/Util/DBUtil.asp"-->
<!--#include virtual="/Include/HTMLEncode.asp"-->
<!--#include virtual="/Include/SysFunction.asp"-->
<!--#include virtual="include/SetPulsanti.asp"-->
<%	
	Inizio()
	Conferma()
%>
<!--#include virtual="/include/closeconn.asp"-->
</body></html>