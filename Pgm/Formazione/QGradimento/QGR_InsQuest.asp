<%
Sub ControlliJavaScript()
%>
<script LANGUAGE="Javascript">
<!--#include virtual="include/ControlString.inc"-->

function inviaAree(){
	frmInsQuest.txtAree.value = TRIM(frmInsQuest.txtAree.value)
   	if (frmInsQuest.txtAree.value == "") {
		alert("Ingresar la cantidad de secciones")
		frmInsQuest.txtAree.focus() 
		return
	}
	if (isNaN(frmInsQuest.txtAree.value)) {
		alert("La cantidad de secciones debe ser numerico")
		frmInsQuest.txtAree.focus() 
		return
	}

	else{
		frmInsAree.txtDQ.value = frmInsQuest.DescQuest.value
		frmInsAree.txtIQ.value = frmInsQuest.IntQuest.value 
		frmInsAree.txtNumero.value = frmInsQuest.txtAree.value  
		frmInsAree.txtRip.value = frmInsQuest.ckRipetibile.checked 
		frmInsAree.numaree.value = 1
		frmInsAree.submit() 
	}
}

function ControllaCampi(frmInsQuest) {
	frmInsQuest.DescQuest.value = TRIM(frmInsQuest.DescQuest.value)
	if (frmInsQuest.DescQuest.value == "") {
		alert("Ingresar el t�tulo del cuestionario")
		frmInsQuest.DescQuest.focus() 
		return false
	}
//alert ("frmInsAree.numaree.value "+ frmInsAree.numaree.value);
	if (frmInsAree.numaree.value == "" && frmInsQuest.txtAree.value != "" && frmInsQuest.txtAree.value != 0 ) {
		alert("Seleccionar 'Agregar' e ingresar las descripciones de las secciones")
		return false
	}	
	
	frmInsQuest.IntQuest.value = TRIM(frmInsQuest.IntQuest.value)
	if (frmInsQuest.IntQuest.value == "") {
		alert("Ingresar la 'Descripcion del Cuestionario'")
		frmInsQuest.IntQuest.focus() 
		return false
	}
	
	frmInsQuest.txtAree.value = TRIM(frmInsQuest.txtAree.value)
	//controllo che il numero sezioni sia almeno 1
	if (frmInsQuest.txtAree.value == "" || frmInsQuest.txtAree.value == 0) {
		alert("Ingresar la cantidad de secciones")
		frmInsQuest.txtAree.focus() 
		return false
	}
	//........fine...........
	if (frmInsQuest.txtAree.value != "" && frmInsQuest.txtAree.value != "0") {
	
		if (isNaN(frmInsQuest.txtAree.value)) {
			alert("La cantidad de secciones debe ser numerico")
			frmInsQuest.txtAree.focus() 
			return false
		}

		var Narea 	
		var appo
		var VALORE
		var ro = / /g
		
		Narea = frmInsQuest.txtAree.value
		for(i=0;i<Narea;i++){
		    appo = i + 1
		    VALORE =eval('frmInsQuest.DescArea' + appo + '.value')
		    VALORE = VALORE.replace( ro, "" )
		    
		    if(VALORE==""){
				alert("Ingresar la descripcion de la seccion numero " + appo)
				eval('frmInsQuest.DescArea' + appo + '.focus()')
			    return false
			}
		}   
	}
}

</script>
<%
End Sub
'-----------------------------------------------------------------------
Sub Inizio()
	dim sFunzione
	dim sTitolo
	dim sCommento
	dim bCampiObbl
	
	sFunzione = "GESTION DE CUESTIONARIOS"
	sTitolo = "INGRESO DE CUESTIONARIOS"
	sCommento = "En esta pagina puede ingresar los datos de un nuevo cuestionario. En el caso en que el mismo est� compuesto por secciones, indicar el numero total de las mismas y hacer clic en Agrega, aparecer�n unos campos donde ingresar los t�tulos de las secciones. De otra forma, completa los campos obligatorios y haga clic en <B>Enviar</B>."
	bCampiObbl = true
	sHelp = "/Pgm/help/Formazione/QGradimento/QGR_InsQuest/"		
%>
	<!--#include virtual="include/SetTestata.asp"-->
<%
End Sub
'-----------------------------------------------------------------------
Sub Inserimento()

dim sSQL, rsTades
%>
<form method="post" action="QGR_CnfInsQuest.asp" onsubmit="return ControllaCampi(this)" id="frmInsQuest" name="frmInsQuest">
<br>	
<table border="0" width="500">
	<tr>
		<td class="tbltext1">
			<b>T�tulo del Cuestionario*</b>
		</td>
		<td>
			<input id="DescQuest" maxLength="200" size="41" name="DescQuest" class="textblacka" value="<%=Descrizione%>">
			<script language="javascript">
				frmInsQuest.DescQuest.focus();
			</script>
		</td>
	</tr>
	
	<tr>
		<td vAlign="top" class="tbltext1">
			<b>Descripci�n del Cuestionario*</b><br>
			<span class="tbltext1">- Le quedan 
				<b><label name="NumCaratteri" id="NumCaratteri">2000</label></b>
				caracteres disponibles -
			</span>
		</td>
		<td>
			<textarea id="IntQuest" name="IntQuest" rows="5" cols="40" class="textblacka" maxlength="2000" onKeyup="JavaScript:CheckLenTextArea(document.frmInsQuest.IntQuest,NumCaratteri,2000)"><%=Intestazione%></textarea>
		</td>
	</tr>
	
	<tr>
		<td class="tbltext1">
			<b>Repetible</b>
		</td>
		<td align="left">
		<% if Ripetibile= "true" then%>
				<input type="checkbox" id="ckRipetibile" name="ckRipetibile" class="textblacka" value="1" checked>
		<% else %>
			    <input type="checkbox" id="ckRipetibile" name="ckRipetibile" class="textblacka" value="1">
		<% end if %>
		</td>
	</tr>

	<tr>
		<td COLSPAN="2" ALIGN="LEFT"><hr WIDTH="92%"></td>
	</tr>	

	<tr>
		<td class="tbltext1">
			<b>Indicar el numero de secciones<br>y seleccionar &quot;agrega&quot;</b>
		</td>
		<td>
		<table border="0" width="250">
			<td align="left" width="20">
				<input type="text" name="txtAree" class="textblacka" size="2" maxlength="2" value="<%=Numero%>">
			</td>
			<td align="left">&nbsp;<b><a class="textred" href="javascript:inviaAree()">Agrega</a></b>
			</td>
		</table>
		</td>
	</tr>
	
	
<%  for i = 1 to Numero%>
		<tr>
			<td class="tbltext1" align="right">
			<b>Secci�n&nbsp;<%=i%></b>
		</td>
			<td>
		<!--	    <TEXTAREA id=IntArea<%=i%> name=IntArea<%=i%> rows=2 cols=40 class=textblacka maxlength="200" size="200" ></TEXTAREA>		-->	
				<input type="text" maxLength="200" size="41" name="DescArea<%=i%>" class="textblacka">
		</td>
		</tr>
<%  next		%>	

    
	<tr>
		<td COLSPAN="2" ALIGN="LEFT"><hr WIDTH="92%"></td>
	</tr>	

	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2" align="center">
			<a onmouseover="javascript:window.status='' ; return true" href="QGR_VisQuestionario.asp">
		         <img src="<%=Session("progetto")%>/images/indietro.gif" border="0" onmouseover="javascript:window.status='' ; return true">
	        </a>
		<%
			
			PlsInvia("Conferma")
		%>
		</td>
	</tr>

</table>


</form>
<%
End Sub
'-----------------------------------------------------------------------
'M A I N
%>
<!--#include virtual="/strutt_testa2.asp"-->
<!--#include virtual="/include/openconn.asp"-->
<!--#include virtual="/include/DecCod.asp"-->
<!--#include virtual="include/SetPulsanti.asp"-->
<!--#include virtual="include/RuoloFunzionale.asp"-->
<%
if ValidateService(session("idutente"),"QGR_VisQuestionario", CC) <> "true" then 
	response.redirect "/util/error_login.asp"
end if

Session("ck_cod_ruofu")=""
%>

<%
dim Numero,Descrizione,Intestazione,Ripetibile

    Numero =Request.Form ("txtNumero")

    if Numero & "*" = "*" then
		Numero = 0 
    end if

    Descrizione = Request.Form ("txtDQ")
    Intestazione = Request.Form ("txtIQ")
    Ripetibile = Request.Form ("txtRip")
    Numaree = Request.Form ("numaree")
%>

<form method="post" action="QGR_InsQuest.asp" name="frmInsAree">
	<input type="hidden" name="txtNumero">
	<input type="hidden" name="txtDQ">
	<input type="hidden" name="txtIQ">
	<input type="hidden" name="txtRip">
	<input type="hidden" name="numaree" value="<%=Numaree%>">
</form>

<%
	ControlliJavaScript()
	Inizio()
	Inserimento()
%>
<!--#include virtual="/include/closeconn.asp"-->
<!--#include virtual="/strutt_coda2.asp"-->
