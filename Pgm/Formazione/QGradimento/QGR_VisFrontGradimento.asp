<script language=javascript>
	function Carica_Pagina(npagina)
	{
		document.frmPagina.pagina.value = npagina;
		document.frmPagina.submit();
	}	
</script>
<%
'----------------------------------------------------------------------
Sub Inizio()
	dim sFunzione
	dim sTitolo
	dim sCommento
	dim bCampiObbl
	
	sFunzione = "CUESTIONARIO"
	sTitolo = "LISTA CUESTIONARIOS"
	sCommento = "Tus opiniones nos permiten mejorar constantemente nuestro servicio. " &_
				"En esta pagina puedes encontrar algunos cuestionarios relativos a las secciones y funciones del sitio.<br>" &_
				"Haga click sobre el t�tulo del cuestionario y responda a las preguntas del cuestionario.<br>" &_
				"Pera ver la <b>vista previa</b> del cuestionario haga click sobre la imagen relativa.<br> " &_
				"Para <b>imprimir</b> haga click sobre el icono de la impresora."
	bCampiObbl = false
	'sHelp="/Pgm/help/formazione/QGradimento/QGR_VisFrontGradimento"	
%>
	<!--#include virtual="/include/SetTestata.asp"-->
<%
End Sub
'----------------------------------------------------------------------
Sub ElencoQ()

	dim sSQL
	dim rsQuest
	dim nProg, nViewTit
	'01/03/2007
	dim sCognome, sNome
	Set rsUtente = Server.CreateObject("ADODB.Recordset")
	
	sSQLUtente =	"SELECT cognome,nome from UTENTE " &_
					"where idutente = " & Session("idutente")
	'Response.Write 				
	rsUtente.Open sSQLUtente, CC, 3
	
	if not rsUtente.eof then
		sCognome= rsUtente("cognome")
		sNome= rsUtente("nome")
	end if
	'01/03/2007 fine
	
	nProg = 0
	nViewTit = 0

	IF SESSION("RUOFU") = "DI" THEN
		aFase = SelPrgFase(session("creator"))
	END IF
	
	Set rsQuest = Server.CreateObject("ADODB.Recordset")
	'	sSQL =	"SELECT a.id_infoquest, i.desc_quest, i.fl_repeat"
		sSQL =	"SELECT a.id_infoquest, i.desc_quest, i.fl_repeat, cod_destinatario"
			
		IF SESSION("RUOFU") = "DI" THEN
			sSQL =	sSQL & ", a.fl_attivazione"
		END IF
		
		sSQL = sSQL & " FROM info_quest i, ass_quest a"
		 
		sSQL = sSQL &	" WHERE (a.cod_ruofu ='" & Session("Ruofu") & "' or a.cod_ruofu is null)" &_
						" AND (a.cod_rorga ='" & Session("rorga") & "' or a.cod_rorga is null)" &_
						" AND a.id_infoquest=i.id_infoquest and cod_destinatario is null"
		
		IF SESSION("RUOFU") = "DI" THEN
			sSQL =	sSQL &	" AND (a.cod_sessione ='" & aFase(1) & "' or a.cod_sessione is null)" &_
							" AND (a.id_bando in(select id_bando from domanda_iscr" &_
							" where id_persona = " & session("creator") & ") or a.id_bando is null)" 
		END IF
	
		sSQL =	sSQL & " ORDER BY a.id_infoquest"	

'	Response.Write  sSQL
	rsQuest.Open sSQL, CC, 3
'''fine 25/6/2003.
	
	Record=0
	nTamPagina=5

	If Request("pagina")= "" Then
		nActPagina=1
	Else
		nActPagina=Clng(Request("pagina"))
	End If 
%>

	<table border="0" cellspacing="1" cellpadding="2" width="500">
		<tr class="sfondocommFad">
		    <td width="20" colspan="4" align="left">&nbsp;</td>
		</tr>
<%	
	IF NOT rsQuest.eof THEN
		rsQuest.PageSize	= nTamPagina
		rsQuest.CacheSize	= nTamPagina	
		nTotPagina			= rsQuest.PageCount		
		If nActPagina < 1 Then
			nActPagina = 1
		End If
	
		If nActPagina > nTotPagina Then
			nActPagina = nTotPagina
		End If
		rsQuest.AbsolutePage=nActPagina
		nTotRecord=0
		
		While rsQuest.EOF <> True And nTotRecord < nTamPagina
		'16/01/2007 inizio estraggo i questionari non intermediati
		sCodDest = rsQuest("cod_destinatario")
	'	Response.Write "cod dest" &   sCodDest
		if isnull(sCodDest) then
		nTotQDir = nTotQDir + 1
		'16/01/2007 fine
'''26/6/2003
           
			flRepeat = rsQuest("FL_REPEAT")
			sQuestAttiv = "no"
			
			IF Session("RUOFU") <> "DI" or isnull(Session("RUOFU"))then
				sQuestAttiv = "si"
			ELSE
				sFlAttiv= rsQuest("FL_ATTIVAZIONE")
				
				Select case sFlAttiv
					case "I"
						If cdate(date()) = cdate(aFase(2)) then
							sQuestAttiv="si"
						End if
					case "F"
						If cdate(date()) = cdate(aFase(3)) then
							sQuestAttiv="si"
						End if
					case "D"
						If cdate(date()) => cdate(aFase(2)) and cdate(date()) <= cdate(aFase(3)) then
							sQuestAttiv="si"
						End if
					case else
					    sQuestAttiv="si"	
				End select
			END IF
			
'''fine 26/6/2003.	
			sSQL = "select count(*) as totaree from iq_area where id_infoiq = " & rsQuest("id_infoquest") 
					set rsArea = CC.Execute(sSQL)
					if not rsArea.eof then
						appoArea = rsArea("totaree")
					end if	
	'		Response.Write sSQL & "<br>"	
	'08/03/2007 	aggiungo la condizione per estrarre solo i questionari diretti 	
	'		sSQL = "select count(*) as totInfo from IQ_RISULTATO WHERE ID_INFOQUEST = " & rsQuest("id_infoquest") 
			sSQL = "select count(*) as totInfo from IQ_RISULTATO WHERE ID_INFOQUEST = " & rsQuest("id_infoquest") & "and id_destinatario is null"
					set rsInfo = CC.Execute(sSQL)
					if not rsInfo.eof then
						appoInfo = rsInfo("totInfo")
					end if
		'08/03/2007 aggiungo la condizione per estrarre solo i questionari diretti 
	'	Response.Write sSQL & "<br>"
		
			if cint(appoArea) = cint(appoInfo) then		
					sContaEseguito = 0	
					sSQL =	"SELECT DT_ESEC, ESEGUITO FROM IQ_RISULTATO" &_
							" WHERE ID_INFOQUEST = " & rsQuest("id_infoquest") &_
							" and id_destinatario is null " &_ 							
							" AND IDUTENTE = " & SESSION("IDUTENTE")& " ORDER BY DT_ESEC "					
	'				Response.Write sSQL
					set rsEsec = CC.Execute(sSQL)
						if not rsEsec.eof then
							do while not rsEsec.eof
								if rsEsec("ESEGUITO") = "S" then
									sContaEseguito = sContaEseguito + 1								
									sEseguito = rsEsec("ESEGUITO")
									sDtEsec = ConvDateToString(rsEsec("DT_ESEC"))								
								end if
								rsEsec.movenext
							loop
							if cint(sContaEseguito) <> cint(appoArea) then
								sEseguito = ""
								sDtEsec = ""							
							end if
							rsEsec.Close
							set rsEsec = nothing
						end if							
			else
				sEseguito = ""
				sDtEsec = ""
			end if					
			nProg = nProg + 1
			
'''11/7/2003	
		
			If sQuestAttiv="no" then
				nProg = nProg - 1
			Else
				nViewTit = nViewTit + 1
				IF nViewTit = 1 THEN %>
					<tr class="sfondocommFad">
					    <td width="10" align="left"><b>#</b></td>
					    <td width="240"><b>Titulo</b></td>
					    <td width="60" align="left"><b>Fecha entrevista</b></td>
					    <td width="60" align="left"><B>Vista Previa</B></td>
					    <td width="10" align="left"><B>Imprime </B></td>
					    <td width="120" align="left"><B>Estado</B></td>
					</tr>
<%				END IF 
'''fine 11//2003. %>
				<tr class="tblsfondoFad">
					<td align="left" class="tblDettFad">
						<%=nProg + (nActPagina * nTamPagina) - nTamPagina%>
					</td>
					<td>
						<%
						if sEseguito="S" and cint(flRepeat)=0 then%> 
							<span class="tblAggFad"><%=rsQuest("desc_quest")%></span>
						<%
						else%>
							<a onmouseover="window.status =' '; return true" 
								title="Seleziona questionario"
								href="Javascript:frmQuest<%=nProg%>.submit()" class="tblAggFad">
								<%=rsQuest("desc_quest")%>
							</a>			
						<%
						end if%>
					</td>
					<td align="left" class=tblDettFad>
						<%=sDtEsec%>&nbsp;
					</td>
					<td align="center">
					<%' if sDtEsec <> "" then %>				
							<a href="javascript:frmAnteprima<%=nProg%>.submit();" class="tblAgg">
					<%	'end if%>
							<img src="<%=Session("progetto")%>/images/formazione/txt.gif" border="0" alt="Vista previa del Cuestionario" WIDTH="20" HEIGHT="21">
						</a>
						<!--anto 01/03/2007 inizio -->
					<td align="middle">
						<a href="javascript:frmStampa<%=nProg%>.submit();" >
						<img src="<%=Session("progetto")%>/images/stampa_img.gif" WIDTH="41" HEIGHT="22" border="0" alt="Imprime cuestionario">
						</a>
					</td>
					<!--anto 01/03/2007 fine -->
				<%
			'	Response.Write "eseguito " & sEseguito
				if sEseguito = "S" then %>
				   <%'Inizio ciclo per verifica ripetibilit� di un questionario%>
					<%if cint(flRepeat)=0 then %>
						<td align="left" class=tblDettFad>
							<img title="Questionario eseguito" src="<%=Session("progetto")%>/images/formazione/okv.gif" 
								border="0" WIDTH="17" HEIGHT="15">Completado
						</td>
					<%else%>	
						<td align="left" class=tblDettFad>
							<img title="Questionario eseguito" src="<%=Session("progetto")%>/images/formazione/okv.gif" 
								border="0" WIDTH="17" HEIGHT="15">Completado - repetible
						</td>
					<%end if %>	
				  <%'Fine ciclo per verifica ripetibilit� di un questionario%>	
				<%else%>
					<td align="left" class=tblDettFad>
						<img title="Questionario non eseguito" src="<%=Session("progetto")%>/images/formazione/ko.gif" border="0" 
							WIDTH="17" HEIGHT="15">Por Completar
					</td>
				<%end if %>
				</tr>	
				<form method=post action="QGR_VisArea.asp" name="frmQuest<%=nProg%>">
					<input type=hidden name="id"	value="<%=clng(rsQuest("id_infoquest"))%>">
					<input type="hidden" name="mod" value="<%=3%>">
				</form>
				<form name="frmAnteprima<%=nProg%>" method="post" action="QGR_VisArea.asp" >
					<input type="hidden" name="id" value="<%=rsQuest("id_infoquest")%>">
					<input type="hidden" name="mod" value="<%=4%>">
					<input type="hidden" name="ero"  value="<%=sErogazione%>">
					<input type="hidden" name="frui" value="<%=sFruizione%>">
					<input type="hidden" name="commessa" value="<%=sCodCommessa%>">
					<input type="hidden" name="progetto" value="<%=sTestoProgetto%>">
					<input type="hidden" name="intervento" value="<%=intervento%>">
				</form>
				<!--anto 01/03/2007 inizio -->
				<form name="frmStampa<%=nProg%>" method="post" action="QGR_Stampa.asp" target="_new">
					<input type="hidden" name="idq" value="<%=rsQuest("id_infoquest")%>">
					<input type="hidden" name="mod" value="<%=4%>">
					<input type="hidden" name="flgCompDir"  value="S">
					<input type="hidden" name="IdUtente" value="<%=Session("IdUtente")%>">
					<input type="hidden" name="CognomeUte" value="<%=sCognome%>">
					<input type="hidden" name="NomeUte" value="<%=sNome%>">
					<input type="hidden" name="dtEsec" value="<%=sDtEsec%>">
				</form>
				<!--anto 01/03/2007 fine -->
<%			End if

'16/01/2007 inizio		
		end if
'16/01/2007 fine
		nTotRecord=nTotRecord + 1 
		rsQuest.movenext	
		
		Wend 
		
		If nViewTit = 0 then %>
			<tr>
				<TD align=center class="tblText3">
					No hay cuestionarios disponibles.
				</TD>
			</tr>
<%
		End if %>
	</table>
	
		<TABLE border=0 width=470 align=center>
			<form name="frmPagina" method="post" action="QGR_VisFrontGradimento.asp">
			    <input type="hidden" name="pagina" value>
			</form>	
			<tr>
				<%	
				if nActPagina > 1 then
					Response.Write "<td align=right width=480>"
					Response.Write "<A HREF=""javascript:Carica_Pagina('" & nActPagina-1 & "')"">"
					Response.Write "<img border=0 src=" & Session("Progetto") & "/images/precedente.gif></A></td>"
				end if
				
				if nActPagina < nTotPagina then			
						Response.Write "<td align=right>"
						Response.Write "<A HREF=""javascript:Carica_Pagina('" & nActPagina+1 & "')"">"		
						Response.Write "<img border=0 src=" & Session("Progetto") & "/images/successivo.gif></A></td>"
				end if
				%>
			</tr>
		</TABLE>	
<%	ELSE %>	
		<tr>
			<TD align=center class="tblText3">
				No hay cuestionarios disponibles.
			</TD>
		</tr>
	</table>
<%	END IF %>
	<br>
<%
	rsQuest.close
End Sub
'----------------------------------------------------------------------
'M A I N
%>
<!--#include virtual="/strutt_testa2.asp"-->
<!--#include virtual="/include/openconn.asp"-->
<!--#include virtual="/include/ControlDateVB.asp"-->
<!--#include virtual="include/SetPulsanti.asp"-->
<!--#include Virtual = "/include/ControlFase.asp"-->

<%

'20/11/2007 DAMIAN QUITA VALIDACION
'if ValidateService(session("idutente"),"QGR_VisFrontGradimento", CC) <> "true" then 
'	response.redirect "/util/error_login.asp"
'end if

dim sErogazione, sFruizione, sCodCommessa, sTestoProgetto, intervento
dim sCodDest, nTotQDir
sErogazione = Request.Form ("ero")
sFruizione	= Request.Form ("frui")				 
sCodCommessa = Request.Form ("commessa")							 
sTestoProgetto = Request.Form ("progetto")					
intervento = Request.Form ("intervento")	
	
Inizio()
ElencoQ()
%>
<!--#include virtual="/include/closeconn.asp"-->
<!--#include virtual="/strutt_coda2.asp"-->