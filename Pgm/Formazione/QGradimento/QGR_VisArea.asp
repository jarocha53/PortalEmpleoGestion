
<%
Sub Inizio()
	dim sFunzione
	dim sTitolo
	dim sCommento
	dim bCampiObbl
	
	sFunzione = "GESTION DE CUESTIONARIOS"
	sTitolo = "Lista de Secciones"
	bCampiObbl = false
	'sHelp = "/Pgm/help/Formazione/QGradimento/QGR_VisArea/"	
	
	if nMod = 2 then
		sCommento = "En esta pagina se mustran las secciones que constityen el cuestionario seleccionado.<BR>Para responder a las preguntas, haga clic sobre <B>Completa</B>.  "
		%>
			<!--#include virtual="/include/SetTestata.asp"-->
		<%
	else 
		if nMod = 1 or nMod = 4 then
			sCommento = "En esta pagina puedes acceder a las secciones que constituyen el cuestionario seleccionado, has clic en <b>Responde</b>.<BR>Para visualizar la <b>Vista Previa</b>, has clic en el link correspondiente.  "
			
			%>
				<!--#include virtual="/include/SetTestata.asp"-->
			<%
		else
			if nMod = 3 then
				if sTipoDest <> "" then
					if sTipoDest = "P" then
						sCommento = "En questa pagina puedes acceder a las secciones que constituyen el cuestionario seleccionado, haciendo clic en <b>Responde</b>.<BR> Para visualizar la <b>Vista Previa</b>, has clic en el link correspondiente, y para imprimirlo utiliza el icono <b>Imprime</b>. "
			
						%>
							<!-- # i n c l u d e virtual="/include/SetTestata_Int.asp" -->
							<!--#include virtual="/include/SetTestata.asp"-->
						<%
					end if	
					if sTipoDest = "S" then
						sCommento = "En questa pagina puedes acceder a las secciones que constituyen el cuestionario seleccionado, haciendo clic en <b>Responde</b>.<BR> Para visualizar la <b>Vista Previa</b>, has clic en el link correspondiente, y para imprimirlo utiliza el icono <b>Imprime</b>. "						
						%>
							<!-- # i n c l u d e virtual="/include/SetTestata_IntAz.asp"-->
							<!--#include virtual="/include/SetTestata.asp"-->
						<%
					end if	
				else
					sCommento = "En questa pagina puedes acceder a las secciones que constituyen el cuestionario seleccionado, haciendo clic en <b>Responde</b>"										
					%>
						<!--#include virtual="/include/SetTestata.asp"-->
					<%
				end if
			end if
		end if
	end if
		

End Sub
'----------------------------------------------------------------------
Sub ElencoA()
dim sSQL, sSQL2, sSQL3, nIdQuest, appoarea, sFunzione

dim rsArea, rsQuest, rsQuest2, rsQuest3
dim nCRighe

nIdQuest = request("id")

'''''SM  definisco da quale funzione richiamo la pag
sFunzione= request("sFunz")
'''''''''''''''fine

'Response.Write "nMod = " & nMod & "<br>"
'+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-
'Ricerca dell'intestazione del questionario
'+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-



'sSQL = "Select desc_quest, intest_quest from info_quest where id_infoquest = " & nIdQuest
sSQL = "Select desc_quest, intest_quest,fl_repeat from info_quest where id_infoquest = " & nIdQuest

set rsQuest = myConn.execute(sSQL)

nCRighe = 0



sSQL = "select id_areaiq, desc_areaiq from iq_area where id_infoiq = " & nIdQuest & " order by id_areaiq"


set rsArea = server.CreateObject ("adodb.recordset")

rsArea.Open sSQL,myConn,3 

appoArea = rsArea.RecordCount 
flgRipetibile = rsQuest("fl_repeat")


if nMod = 2 then%>
	<form name="frmSessioni" method="post" action="QGR_Sessioni.asp">
<%else
	if nMod = 1 then%>
		
		

		<form name="frmSessioni" method="post" action="QGR_VisAnteprima.asp">
		<input type="hidden" name="myProg" value="<%=progetto%>">
		<!--SM valore per nMod=1 ------->
		<input type="hidden" id="mod" name="mod" value="1">
		<input type="hidden" id="sFunzione" name="sFunzione" value="<%=sFunzione%>">
		<!--SM fINE------->
	<%else if nMod = 3 then
			sSQL2 = "Select cod_commessa,TESTO_PROJ  from progetto where cod_cproj = " & "'" & mid(session("progetto"),2) & "'"
			sSQL3 = "Select erogazione, fruizione from ass_quest where id_infoquest = " & nIdQuest
						
			if session("ruofu") <> "" then
				' Mi prendo la sessione
				sSQL3 = sSQL3 & " AND COD_RUOFU ='" & session("ruofu") & "'" 
				if 	session("ruofu") = "DI" then
					aFase = SelPrgFase(session("creator")) 
					sSQL3 =	sSQL3 &	" AND (a.cod_sessione ='" & aFase(1) & "' or a.cod_sessione is null)" &_
						" AND (a.id_bando in(select id_bando from domanda_iscr" &_
						" where id_persona = " & session("creator") & ") or a.id_bando is null)" 
				end if
		
			else
				sSQL3 = sSQL3 & " AND COD_RUOFU  is null"
			end if	
			
			 if session("rorga") <> "" then
				sSQL3 = sSQL3 & " AND COD_RORGA ='" & session("rorga") & "'" 
			else
				sSQL3 = sSQL3 & " AND COD_RORGA  is null"
			end if	


			set rsQuest2 = myConn.execute(sSQL2)
			set rsQuest3 = myConn.execute(sSQL3)
			if not rsQuest.eof then %>
			
			<table width="500" border="0" >
				<tr>
					<td class="tbltext1" align="left"><b> Cuestionario</b> "<%=rsQuest("desc_quest")%>": <%=rsQuest("intest_quest")%><br><br></td>
				</tr>
								
				<%
				' 15/01/2007 inizio Questionari intermediati
				if sTipoDest <> "" then
					if sTipoDest = "S" then				
					%>
					<tr>
						<td class="tbltext1" align="left">
							<b>Entervistado:</b>
							&nbsp;&nbsp;&nbsp;&nbsp; <%=sRagSoc%>&nbsp;<br><br>
							<b>Entervistador:</b>
							&nbsp;&nbsp;&nbsp;&nbsp; <%=sCognomeUte%>&nbsp;<%=sNomeUte%><br>
							
							<!--<b>Questionario intermediato per la sede &nbsp;<%=sDescSede%><br>
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							dell'azienda :&nbsp;<%=sRagSoc%></b>-->
						</td>
					</tr>
					<%
					else
					%>
					<tr>
						<tr>
							<td class="tbltext1" align="center">&nbsp;</td>
						</tr>
						<td class="tbltext1" align="left">
							<b>Entervistado:</b>
							&nbsp;&nbsp;&nbsp;&nbsp; <%=sCognome%>&nbsp;<%=sNome%><br><br>
							<b>Entervistador:</b>
							&nbsp;&nbsp;&nbsp;&nbsp;<%=sCognomeUte%>&nbsp;<%=sNomeUte%><br>
						</td>
					</tr>
					<%
					end if					
				
				end if 
				'15/01/2007 fine
				sCodCommessa = rsQuest2("cod_commessa")
				sTestoProgetto = rsQuest2("TESTO_PROJ")
				if not rsQuest3.eof then %>
						  <% if rsQuest3("erogazione") <> "" then %>	
								<tr>
									<td class="tbltext1" align="left">
										<b>Suministro:&nbsp;&nbsp;<%

								sSQL = "SELECT ID_CAMPO_DESC FROM DIZ_DATI WHERE " &_
									"ID_TAB='ASS_QUEST' AND ID_CAMPO='EROGAZIONE'" 
								
								set rsDizDati = cc.Execute(sSQL)
								bTipo = split(rsDizDati("ID_CAMPO_DESC"),"|")
								for i = 0 to ubound (bTipo) step 2
									if ( bTipo(i) = rsQuest3("erogazione")) then
										sErogazione = bTipo(i + 1)
										Response.Write bTipo(i + 1)
										exit for
									end if	
								next
								rsDizDati.close()
								set rsDizDati = nothing
										
										
										%></b>
									</td>
								</tr>
						  <% end if %>
						  <% if rsQuest3("fruizione") <> "" then %>
								<tr>
									<td class="tbltext1" align="left">
										<b>Explotacion:&nbsp;&nbsp;<%
								sSQL = "SELECT ID_CAMPO_DESC FROM DIZ_DATI WHERE " &_
									"ID_TAB='ASS_QUEST' AND ID_CAMPO='FRUIZIONE'" 
								
								set rsDizDati = cc.Execute(sSQL)
								bTipo = split(rsDizDati("ID_CAMPO_DESC"),"|")
								
								for i = 0 to ubound (bTipo) step 2
									if ( bTipo(i) = rsQuest3("fruizione")) then
										sFruizione = bTipo(i + 1)
										Response.Write bTipo(i + 1)
										exit for
									end if	
								next
								rsDizDati.close()
								
								set rsDizDati = nothing										
										
										%></b>
									</td>
								</tr>
						  <% end if
				   end if				
		end if
		rsQuest2.close
		rsQuest3.close
		set rsQuest2 = nothing
		set rsQuest3 = nothing
		
		
		intervento = getInterventoFormativo (Session("creator"),mid( session("progetto"),2) )
		if intervento <> "" then
		%>
				<tr>
					<td class="tbltext1" align="left"><B>Titulo intervencion formativa : <%=intervento%></B></td>
				</tr>
		<%end if%>
			</table>
			
			<form name="frmSessioni" method="post" action="QGR_Questionario.asp">
			<input type="hidden" name="sezioni" value="<%=nTotSez%>">	
			<input type="hidden" name="txtTipoDest" value="<%=sTipoDest%>">
				
		<% else if nMod = 4 then

			sSQL2 = "Select cod_commessa,TESTO_PROJ  from progetto where cod_cproj = " & "'" & mid(session("progetto"),2) & "'"
			sSQL3 = "Select erogazione, fruizione from ass_quest where id_infoquest = " & nIdQuest

			if session("ruofu") <> "" then
				' Mi prendo la sessione
				sSQL3 = sSQL3 & " AND COD_RUOFU ='" & session("ruofu") & "'" 
				if 	session("ruofu") = "DI" then
					aFase = SelPrgFase(session("creator")) 
					sSQL3 =	sSQL3 &	" AND (a.cod_sessione ='" & aFase(1) & "' or a.cod_sessione is null)" &_
						" AND (a.id_bando in(select id_bando from domanda_iscr" &_
						" where id_persona = " & session("creator") & ") or a.id_bando is null)" 
				end if
		
			else
				sSQL3 = sSQL3 & " AND COD_RUOFU  is null"
			end if	
			
			 if session("rorga") <> "" then
				sSQL3 = sSQL3 & " AND COD_RORGA ='" & session("rorga") & "'" 
			else
				sSQL3 = sSQL3 & " AND COD_RORGA  is null"
			end if	


			set rsQuest2 = myConn.execute(sSQL2)
			set rsQuest3 = myConn.execute(sSQL3)
			if not rsQuest.eof then%>
			<table width="500" border="0" >
				
				<tr>
					<td class="tbltext3" align="center">&nbsp;</td>
				</tr>
				<tr>
					<td class="tbldett" align="center">
						<%=rsQuest("intest_quest")%>
					</td>
				</tr>
				<tr>
					<td class="tbltext1" align="center">&nbsp;</td>
				</tr>
				<!--<tr>
					<td class="tbltext1" align="left">
						<b>Programma : <%=rsQuest2("TESTO_PROJ")%></b>
					</td>
				</tr>
				<tr>
					<td class="tbltext1" align="left">
						<b>Codice commessa del progetto:&nbsp;&nbsp;<%=rsQuest2("cod_commessa")%></b>
					</td>
				</tr>-->
				<%
				' 15/01/2007 inizio Questionari intermediati
				if sTipoDest <> "" then
					if sTipoDest = "S" then				
					%>
					<!--<tr>
						<td class="tbltext1" align="left">
							<b>Questionario intermediato per la sede &nbsp;<%=sDescSede%>dell'azienda :&nbsp;<%=sRagSoc%></b>
						</td>
					</tr>-->
					<%
					else
					%>
					<!--<tr>
						<td class="tbltext1" align="left">
							<b>Questionario intermediato per : </b><%=sCognome%>&nbsp;<%=sNome%><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<b>codice fiscale : </b><%=sCodFisc%>  
						</td>
					</tr>-->
					<%
					end if	
				end if					
				'15/01/2007 fine
				sCodCommessa = rsQuest2("cod_commessa")
				sTestoProgetto = rsQuest2("TESTO_PROJ")
				if not rsQuest3.eof then %>
						  <% if rsQuest3("erogazione") <> "" then %>	
								<tr>
									<td class="tbltext1" align="left">
										<b>Suministro:&nbsp;&nbsp;<%

								sSQL = "SELECT ID_CAMPO_DESC FROM DIZ_DATI WHERE " &_
									"ID_TAB='ASS_QUEST' AND ID_CAMPO='EROGAZIONE'" 
								
								set rsDizDati = cc.Execute(sSQL)
								bTipo = split(rsDizDati("ID_CAMPO_DESC"),"|")
								for i = 0 to ubound (bTipo) step 2
									if ( bTipo(i) = rsQuest3("erogazione")) then
										sErogazione = bTipo(i + 1)
										Response.Write bTipo(i + 1)
										exit for
									end if	
								next
								rsDizDati.close()
								set rsDizDati = nothing
										
										
										%></b>
									</td>
								</tr>
						  <% end if %>		
										
							 <% if rsQuest3("fruizione") <> "" then %>
								<tr>
									<td class="tbltext1" align="left">
										<b>Explotaci�n:&nbsp;&nbsp;<%
								sSQL = "SELECT ID_CAMPO_DESC FROM DIZ_DATI WHERE " &_
									"ID_TAB='ASS_QUEST' AND ID_CAMPO='FRUIZIONE'" 
								
								set rsDizDati = cc.Execute(sSQL)
								bTipo = split(rsDizDati("ID_CAMPO_DESC"),"|")
								
								for i = 0 to ubound (bTipo) step 2
									if ( bTipo(i) = rsQuest3("fruizione")) then
										sFruizione = bTipo(i + 1)
										Response.Write bTipo(i + 1)
										exit for
									end if	
								next
								rsDizDati.close()
								
								set rsDizDati = nothing										
										
										%></b>
									</td>
								</tr>
							<% end if
				   end if				
		end if		
		rsQuest2.close
		rsQuest3.close
		set rsQuest2 = nothing
		set rsQuest3 = nothing
		
		
		intervento = getInterventoFormativo (Session("creator"),mid( session("progetto"),2) )
		
		%>
					<form name="frmSessioni" method="post" action="QGR_VisAnteprimaUte.asp">
					<!--SM valore per nMod=4 Anteprima Utente------->
					<input type="hidden" id="mod" name="mod" value="4">
					<input type="hidden" name="flgCompDir" value="<%=sFlgCompDir%>">
					
					<!--SM INE------->
				<%end if
		end if
	end if		
 end if 
rsQuest.close
set rsQuest = nothing %>
<input type="hidden" name="idq" value="<%=nIdQuest%>">

<%'if clng(appoarea) = 1  OR clng(appoarea) = 0 then %>
		<!--<input type="hidden" name="ida" value="0">-->
<%'end if

	if clng(appoarea) =  0 then %>
		<input type="hidden" name="ida" value="0">
	<%end if 

	if clng(appoarea) = 1 then %>
		<input type="hidden" name="ida" value="<%=rsArea("id_areaiq")%>">
	<%end if %>		
</form>


<%''SM inizio per nMod=1 visualizzo direttamente QGR_VisAnteprima
'if clng(appoarea) < 1  then 
if clng(appoarea) < 1 and (nMod=1 or nMod=4) then 
''''SM   fine%>
	<script>
		document.frmSessioni.submit()  
	</script>
<%else %>
	<table width="500" border="0">
		<tr class="sfondocomm">
	        <td width="20" align="center"><b>#</b></td>
	        <td><b>Secciones</b></td>
	       <% if nMod = 2 then%>
				<td width="60" align="center"><b>Completa</b></td>
	        <%else
				if nMod = 1 or nMod = 4 then %>
      			   <td width="60" align="center"><b>Vista Previa</b></td>
      	        <%else 
      			  if nMod = 3 then %>  			   
					    <td width="100" align="left"><b>Fecha entervista</b></td>					    
					    <td width="20" align="left"><B>Estado</B></td>
					<%    
						' 15/01/2007 inizio Questionari intermediati
					  if sTipoDest = "S" or sTipoDest = "P" then %>
						 <td width="20" align="left"><b>Vista Previa</b></td>
						 <!--anto 01/03/2007 inizio -->
						 <!--td width="10" align="left"><b>Stampa</b></td-->
						 <!--anto 01/03/2007 fine -->
					<%end if
					    ' 15/01/2007 fine
					    %>
					    <td width="60" align="center"><b>Responde</b></td>
				<%end if
				end if	
			  end if%>
		</tr>
	<%
	do while not rsArea.eof
		nCRighe = nCRighe + 1 
		
	'''8/7/2003: PASSATI VALORI TRAMITE FORM ANZICHE' QUERYSTRING 
	if nMod = 2 then%>
		<form name="frmDomanda<%=nCRighe%>" method="post" action="QGR_Sessioni.asp">
			<input type="hidden" name="idq" value="<%=nIdQuest%>">
			<input type="hidden" name="ida" value="<%=rsArea("id_areaiq")%>">
			<input type="hidden" name="mod" value="<%=nMod%>">
		</form>
	<%else
		if nMod = 1 then%>
			<form name="frmDomanda<%=nCRighe%>" method="post" action="QGR_VisAnteprima.asp">
				<input type="hidden" name="idq" value="<%=nIdQuest%>">
				<input type="hidden" name="ida" value="<%=rsArea("id_areaiq")%>">
				<input type="hidden" name="myProg" value="<%=progetto%>">
			</form>
		<%else
				if nMod = 3 then%>
					<form name="frmDomanda<%=nCRighe%>" method="post" action="QGR_Questionario.asp">
						<input type="hidden" name="idq" value="<%=nIdQuest%>">						
						<input type="hidden" name="ida" value="<%=rsArea("id_areaiq")%>">
						<input type="hidden" name="ero"  value="<%=sErogazione%>">
						<input type="hidden" name="frui" value="<%=sFruizione%>">
						<input type="hidden" name="commessa" value="<%=sCodCommessa%>">
						<input type="hidden" name="progetto" value="<%=sTestoProgetto%>">
						<input type="hidden" name="intervento" value="<%=intervento%>">
						<!--15/01/2007 inizio Questionari intermediati-->
						<input type="hidden" name="txtTipoDest" value="<%=sTipoDest%>">
						<input type="hidden" name="txtIdPers" value="<%=sIdPers%>">
						<input type="hidden" name="txtIdSede" value="<%=sIdSede%>">
						<input type="hidden" name="txtIdSedeLegale" value="<%=sIdSedeLegale%>">
						<input type="hidden" name="txtDescSede" value="<%=sDescSede%>">	
						<input type="hidden" name="txtRagSoc" value="<%=sRagSoc%>">	
						<input type="hidden" name="txtCognome" value="<%=sCognome%>">	
						<input type="hidden" name="txtNome" value="<%=sNome%>">	
						<input type="hidden" name="txtCodFisc" value="<%=sCodFisc%>">
						<input type="hidden" name="txtIdProj" value="<%=nIdProj%>">
						<input type="hidden" name="sezioni" value="<%=nTotSez%>">
						<!--fine-->
					</form>
					<!--  15/01/2007 form Questionari intermediati-->
					<form name="frmDomandaInt<%=nCRighe%>" method="post" action="QGR_VisAnteprimaUte.asp">
						<input type="hidden" name="idq" value="<%=nIdQuest%>">						
						<input type="hidden" name="ida" value="<%=rsArea("id_areaiq")%>">
						<input type="hidden" name="ero"  value="<%=sErogazione%>">
						<input type="hidden" name="frui" value="<%=sFruizione%>">
						<input type="hidden" name="commessa" value="<%=sCodCommessa%>">
						<input type="hidden" name="progetto" value="<%=sTestoProgetto%>">
						<input type="hidden" name="intervento" value="<%=intervento%>">
						<input type="hidden" name="mod" value="<%=nMod%>">
						<input type="hidden" name="txtTipoDest" value="<%=sTipoDest%>">
						<input type="hidden" name="txtIdPers" value="<%=sIdPers%>">
						<input type="hidden" name="txtIdSede" value="<%=sIdSede%>">
						<input type="hidden" name="txtIdSedeLegale" value="<%=sIdSedeLegale%>">
						<input type="hidden" name="flgCompDir" value="<%=sFlgCompDir%>">
					</form>
				
				<%		
				else
					if nMod = "4" then%>
						<form name="frmDomanda<%=nCRighe%>" method="post" action="QGR_VisAnteprimaUte.asp">
							<input type="hidden" name="idq" value="<%=nIdQuest%>">						
							<input type="hidden" name="ida" value="<%=rsArea("id_areaiq")%>">
							<input type="hidden" name="mod" value="<%=nMod%>">
							<input type="hidden" name="ero"  value="<%=sErogazione%>">
							<input type="hidden" name="frui" value="<%=sFruizione%>">
							<input type="hidden" name="commessa" value="<%=sCodCommessa%>">
							<input type="hidden" name="progetto" value="<%=sTestoProgetto%>">
							<input type="hidden" name="intervento" value="<%=intervento%>">
							<input type="hidden" name="txtTipoDest" value="<%=sTipoDest%>">
							<input type="hidden" name="txtIdPers" value="<%=sIdPers%>">
							<input type="hidden" name="txtIdSede" value="<%=sIdSede%>">
							<input type="hidden" name="txtIdSedeLegale" value="<%=sIdSedeLegale%>">
							<input type="hidden" name="flgCompDir" value="<%=sFlgCompDir%>">
						</form>
					<%
					end if
				end if					
		end if							
	end if %>
		
	<%'''FINE 8/7/2003%>
		<tr class="tblsfondo">
			<td class="tblDett">
				<%=nCRighe%>
			</td>
			<td class="tblDett">

					<%
					if rsArea("desc_areaiq") > " " then
						Response.Write rsArea("desc_areaiq")
					else
						Response.Write "--- Senza Titolo ---"
					end if
					%>
				</a>
			</td>
		<form name="frmRisposta<%=nCRighe%>" method="post" action="QGR_VisRisultati.asp">
			<input type="hidden" name="idq" value="<%=nIdQuest%>">
			<input type="hidden" name="ida" value="<%=rsArea("id_areaiq")%>">
			<input type="hidden" name="mod" value="<%=nMod%>">
		</form>
		
				
			<%	if nMod = 2 then%>
					<td align="center">
					<a href="javascript:frmDomanda<%=nCRighe%>.submit();" class="tblagg">
					<img src="<%=Session("progetto")%>/images/formazione/re.gif" border="0" alt="Completa Secci�n" WIDTH="12" HEIGHT="16">
			<%	else
					if nMod = 1 or nMod = 4 then%>
						<td align="center">
						<a href="javascript:frmDomanda<%=nCRighe%>.submit();" class="tblagg">
						<img src="<%=Session("progetto")%>/images/formazione/re.gif" border="0" alt="Visualiza" WIDTH="12" HEIGHT="16">
				<%	else
						if nMod = 3 then
						' 15/01/2007 inizio Questionari intermediati
							if sTipoDest <> "" then								
								sSQL =	"SELECT DT_ESEC, ESEGUITO FROM IQ_RISULTATO" &_
								" WHERE ID_INFOQUEST = " & nIdQuest &_
								" AND ID_AREAIQ = " & rsArea("id_areaiq") &_	
								" AND COD_TIPO_DEST = '" & sTipoDest &"'"
								if sTipoDest = "S" then
									sSQL = sSQL & " AND ID_DESTINATARIO = " & sIdSede
								else
									sSQL = sSQL & " AND ID_DESTINATARIO = " & sIdPers
								end if			
							else
							' 15/01/2007 fine
							'08/03/2007 per i questionari non intermediati aggiungo id_destinatario = null
								sSQL =	"SELECT DT_ESEC, ESEGUITO FROM IQ_RISULTATO" &_
									" WHERE ID_INFOQUEST = " & nIdQuest &_
									" AND ID_AREAIQ = " & rsArea("id_areaiq") &_	
									" AND IDUTENTE = " & SESSION("IDUTENTE") &_
									" AND ID_DESTINATARIO is null "		
							end if
						'	Response.Write sSQL
							set rsEsec = myConn.Execute(sSQL)
							if not rsEsec.eof then
								sEseguito = rsEsec("ESEGUITO")
								sDtEsec = ConvDateToString(rsEsec("DT_ESEC")) 
							else
								sEseguito = ""
								sDtEsec = ""
							end if%>
								
							<td align="left" class=tblDettFad>							
								<%=sDtEsec%>&nbsp;
							</td>															
							<td align="left" class=tblDettFad>
							<%if sEseguito = "S" then %>
								<img title="Questionario eseguito" src="<%=Session("progetto")%>/images/formazione/okv.gif" border="0" 
								WIDTH="17" HEIGHT="15">Completado								
								<%
								' 15/01/2007 inizio Questionari intermediati
								if sTipoDest = "S" or sTipoDest = "P" then %>
								<td align="center">
									<a href="javascript:frmDomandaInt<%=nCRighe%>.submit();" class="tblagg">
									<!--img src="<%=Session("progetto")%>/images/formazione/re.gif" border="0" alt="Visualizza" WIDTH="12" HEIGHT="16"-->
									<img src="<%=Session("progetto")%>/images/formazione/txt.gif" border="0" alt="Visualiza" WIDTH="20" HEIGHT="21">
								</td>
								<!--anto 01/03/2007 inizio -->
								<!--td align="left" class=tblDettFad>
									<a href="javascript:frmStampa<%=nProg%>.submit();">
									<img src="<%=Session("progetto")%>/images/stamp_piccolo.gif" WIDTH="41" HEIGHT="22" border="0" alt="Stampa questionario">
									</a>
								</td-->
								<!--anto 01/03/2007 fine -->
								<%end if
								if flgRipetibile > "0" then
								'15/01/2007 fine%>
								<td align="center">
								<a href="javascript:frmDomanda<%=nCRighe%>.submit();" class="tblagg">													
		     						<img src="<%=Session("progetto")%>/images/formazione/re.gif" border="0" alt="Visualizza" WIDTH="12" HEIGHT="16">
								</td>								
							<%	' 15/01/2007 inizio Questionari intermediati
								else%>
								<td align="center" class=tblDettFad>
								non ripetibile
								</td><%
								end if
								'15/01/2007 fine
							else %>	
								<img title="Cuestionario sin completar" src="<%=Session("progetto")%>/images/formazione/ko.gif" border="0" 
								WIDTH="17" HEIGHT="15">Para Completar
								
								<%
								' 15/01/2007 inizio Questionari intermediati
								if sTipoDest = "S" or sTipoDest = "P" then %> 
								<td align="center">
									<a href="javascript:frmDomandaInt<%=nCRighe%>.submit();" class="tblagg">
									<!--img src="<%=Session("progetto")%>/images/formazione/re.gif" border="0" alt="Visualizza" WIDTH="12" HEIGHT="16"-->
									<img src="<%=Session("progetto")%>/images/formazione/txt.gif" border="0" alt="Visualiza" WIDTH="20" HEIGHT="21">
								</td>
								<!--td align="center">
								-
								</td-->	
								<%end if
								'15/01/2007fine	%>
								<td align="center">
								<a href="javascript:frmDomanda<%=nCRighe%>.submit();" class="tblagg">													
		     					<img src="<%=Session("progetto")%>/images/formazione/re.gif" border="0" alt="Responde" WIDTH="12" HEIGHT="16">
								</td>
							<%end if%>	
					<%	end if
					end if	
				end if%>
				</a>
			</td>
		</tr>	
	<%
		rsArea.movenext
	loop
end if	
rsArea.close
set rsArea = nothing
%>
<!--anto 01/03/2007 inizio -->
<form name="frmStampa" method="post" action="QGR_Stampa.asp" target="_new">
	<input type="hidden" name="idq" value="<%=nIdQuest%>">
	<input type="hidden" name="mod" value="<%=4%>">
	<input type="hidden" name="flgCompDir"  value="N">
	<input type="hidden" name="IdUtente" value="<%=Session("IdUtente")%>">
	<input type="hidden" name="CognomeUte" value="<%=sCognomeUte%>">
	<input type="hidden" name="NomeUte" value="<%=sNomeUte%>">
	<input type="hidden" name="Cognome" value="<%=sCognome%>">
	<input type="hidden" name="Nome" value="<%=sNome%>">
	<input type="hidden" name="dtEsec" value="<%=sDtEsec%>">
	<input type="hidden" name="txtTipoDest" value="<%=sTipoDest%>">
	<input type="hidden" name="txtIdPers" value="<%=sIdPers%>">
	<input type="hidden" name="txtIdSede" value="<%=sIdSede%>">
	<input type="hidden" name="txtDescSede" value="<%=sDescSede%>">	
	<input type="hidden" name="txtRagSoc" value="<%=sRagSoc%>">	
</form>
<!--anto 01/03/2007 fine -->
</table>
<br>
<!--anto 08/05/2007 inizio -->
<form name="frmAnagPersona" method="post" action="/pgm/Formazione/QGradimento/Intermediati/INT_InsDatiUtente.asp" >
	<input type="hidden" name="txtIdQuest" value="<%=nIdQuest%>">	
	<input type="hidden" name="IdUtente" value="<%=Session("IdUtente")%>">	
	<input type="hidden" name="txtTipoDest" value="<%=sTipoDest%>">
	
</form>
<form name="frmAnagAzienda" method="post" action="/pgm/Formazione/QGradimento/Intermediati/INT_InsDatiAzienda.asp" >
	<input type="hidden" name="txtIdQuest" value="<%=nIdQuest%>">	
	<input type="hidden" name="IdUtente" value="<%=Session("IdUtente")%>">	
	<input type="hidden" name="txtTipoDest" value="<%=sTipoDest%>">	
</form>
<!--anto 08/05/2007 fine -->

<table width="500" border ="0">
	<tr>
		<td align="center" width="50%">
		<%	
		
		if nMod = 3  or nMod = 4 then
		'15/01/2007 inizio
		''	PlsLinkRosso "QGR_VisFrontGradimento.asp","Elenco dei Questionari"			
			'	if sTipoDest = "S" or sTipoDest = "P" then 
			'		PlsLinkRosso "/pgm/Formazione/QGradimento/Intermediati/INT_VisFrontGradimento.asp","Elenco dei Questionari"
			 '08/05/2007 inizio 
				if sTipoDest <> "" then
					if sTipoDest = "S" then 
						PlsLinkRosso "/pgm/Formazione/QGradimento/Intermediati/INT_VisFrontGradimento.asp","Lista de Cuestionarios"
						%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<a href="javascript:frmAnagAzienda.submit();" class="textred">
						<b>Vuelve a la secci�n de Datos Personales</b>
						</a><%
					end if
					if sTipoDest = "P" then 
						PlsLinkRosso "/pgm/Formazione/QGradimento/Intermediati/INT_VisFrontGradimento.asp","Lista de Cuestionarios"
						%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<a href="javascript:frmAnagPersona.submit();" class="textred">
						<b>Vuelve a los datos personales</b>
						</a><%
					end if
 				'08/05/2007 fine		
			    else				
					PlsLinkRosso "QGR_VisFrontGradimento.asp","Lista de Cuestionarios"
				end if
		'15/01/2007 fine
			else 
				PlsLinkRosso apLinkIndietro, descrizioneLinkIndietro
			end if%>
		</td>
	</tr>
	<!--01/03/2007 stampa inizio-->
<%	if sTipoDest <> "" and nMod <> 1 then %>
	<tr>
		<td>&nbsp;
		</td>
	</tr>	
	<TR>
		<td align="center" width="50%">
			<a href="javascript:frmStampa.submit();">
			<img src="<%=Session("progetto")%>/images/stampa.gif" border="0" alt="Imprime cuestionario" align="middle">
			</a>
		</td>								
	</TR>
	<%end if %>
	<!--01/03/2007 stampa fine-->
</table>
<%
End Sub
'----------------------------------------------------------------------
'M A I N
%>
<!--#include virtual="/strutt_testa2.asp"-->
<!--#include virtual="/include/openconn.asp"-->
<!--#include virtual="include/SetPulsanti.asp"-->
<!--#include virtual="/include/InterventoFormativo.asp"-->
<!--#include Virtual = "/include/ControlFase.asp"-->
<%
'inizio am 25/01/07
		Dim aMenu(0)
		'aMenu(0) = Session("Progetto") & "/homeITES.asp"
		aMenu(0) = Session("Progetto") & "/home.asp"
		Session("Indice") = aMenu
		Session("Menu") = aMenu(0)
		nPos = 0
	'fine am 25/01/07	
%>
<%
sOrigen = request("txtOrigen")
if sOrigen = "AreaScheda" then
		sIdpers=Request("txtIdPers")
		'Response.Write sIdpers
		'Response.End
	%>	 <br>
		<!--#include virtual="/ba/pgm/iscr_utente/menu.asp"-->
		<br><%
end if

''if ValidateService(session("idutente"),"QGR_VisQuestionario", CC) <> "true" then 
''	response.redirect "/util/error_login.asp"
''end if
dim nMod
	nMod = request("mod")
'Response.Write "Mod" & nMod
'Response.End
dim sErogazione, sFruizione, sCodCommessa, sTestoProgetto, intervento
dim sTipoDest, sIdPers, sIdSede, sCognome, sNome, sDataNasc, sSesso,sDescSede, sRagSoc,sCodFisc,flgRipetibile,nIdProj
dim sCognomeUte, sNomeUte, rsIntervistatore
dim nTotSez, nIdArea, nIdQ, sFlgCompDir

sErogazione = Request.Form ("ero")
sFruizione	= Request.Form ("frui")				 
sCodCommessa = Request.Form ("commessa")							 
sTestoProgetto = Request.Form ("progetto")					
intervento = Request.Form ("intervento")			
sTipoDest = Request.Form ("txtTipoDest")
sIdPers =	Request.Form ("txtIdPers")
sIdSede = Request.Form ("txtIdSede")
sIdSedeLegale = Request.Form ("txtIdSedeLegale")

if sIdSede = "" then
	sIdSede = Request.Form ("txtIdSedeLegale")	
end if
if sIdSede = "" then
	if sIdSedeLegale = "" then
		sIdSede = Request.Form ("txtIdDestinatario")
	end if	
end if

if sTipoDest = "" then
	sFlgCompDir = "S"
else
	sFlgCompDir = "N"	
end if

sDescSede = Request.Form ("txtDescSede")
sRagSoc = Request.Form ("txtRagSoc")
sCognome = Request.Form ("txtCognome")
sNome = Request.Form ("txtNome")
'sDataNasc =  Request.Form ("txtDataNascita")
'sSesso = Request.Form ("cmbSesso")
sCodFisc = Request.Form ("txtCodFisc")
nIdProj = Request("txtIdProj")
nTotSez = Request("sezioni")
nIdArea = Request("ida")
nIdQ = Request.Form("id")
'Response.Write nMod

'  inizio SM  ''''
'PER REPERIRE NOME E COGNOME DELL'INTERVISTATORE
if sTipoDest <> "" then
		sSQL = "SELECT COGNOME, NOME FROM UTENTE WHERE " &_
				"IDUTENTE=" & session("idutente")
				
						
		set rsIntervistatore = cc.Execute(sSQL)
		sCognomeUte= rsIntervistatore("COGNOME")	
		sNomeUte= rsIntervistatore("NOME")	
		
END IF

''' FINE SM

'INIZIO PILI

	apLinkIndietro = Request("pagIndietro")
	if apLinkIndietro = "" then
		apLinkIndietro = "QGR_VisQuestionario.asp"
		descrizioneLinkIndietro = "Lista de Cuestionarios"
		'15/01/2007 questionari intermediati inizio
		if sTipoDest <> "" then
			if sOrigen = "AreaScheda" then
				apLinkIndietro = "/pgm/Formazione/QGradimento/Intermediati/INT_VisFrontGradimento.asp?Idpers="& sIdPers
			else
				apLinkIndietro = "/pgm/Formazione/QGradimento/Intermediati/INT_VisFrontGradimento.asp"
			end if			
			descrizioneLinkIndietro = "Vuelve a la lista de los Cuestionarios"
		end if
		'15/01/2007 fine
	else
		descrizioneLinkIndietro = "Lista de Cuestionarios para importar"
	end if


'dichiaro una connessione di comodo
dim myConn
'progetto = "\" & LCase(Request("myProg"))
progetto = Request("myProg")

if progetto <> "" then
	progetto = "\" & LCase(Request("myProg"))
	
%>
	<!--#include virtual="/include/openpar.asp"-->
<%
	set myConn = CX
else
	set myConn = CC
end if	
	
	Inizio()
	ElencoA()

if progetto <> "" then
%>
	<!--#include virtual="/include/closepar.asp"-->
<%
end if	

'FINE PILI




'	Inizio()
'	ElencoA()
%>
<!--#include virtual="/include/closeconn.asp"-->
<!--#include virtual="/strutt_coda2.asp"-->
