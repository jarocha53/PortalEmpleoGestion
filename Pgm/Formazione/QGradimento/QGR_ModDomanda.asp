<%
Sub ControlliJavaScript()
%>
<SCRIPT LANGUAGE="Javascript">
<!--#include virtual="include/ControlString.inc"-->
<!--#include virtual="include/ControlNum.inc"-->

function ControllaCampi(frmInsDomanda) {
	frmInsDomanda.DescDomanda.value = TRIM(frmInsDomanda.DescDomanda.value)
	if (frmInsDomanda.DescDomanda.value == "") {
		alert("Ingresar la 'Descripcion de la Pregunta'")
		frmInsDomanda.DescDomanda.focus() 
		return false
	}	
	
	if (frmInsDomanda.txtTipoRisposta.value=="V"){
		if (frmInsDomanda.cmbTipoSel.value == "M"){
			alert('Una Pregunta con respuesta de Tipo <Valor> debe tener como Tipo de Seleccion <Excluyente>.!')
			return false;
		}
	}
	//___________Obbligatoriet� Tipo Selezione____________
	/*
	if (frmInsDomanda.cmbTipoSel.value == "") {
		alert("Specificare il Tipo di Selezione: Escludente o Multipla")
		frmInsDomanda.cmbTipoSel.focus() 
		return false
	}
	*/
}
	

/*==> 9/7/2003: PASSATI VALORI TRAMITE FORM ANZICHE' QUERYSTRING <==*/
//function ConfElimina(idd, ida, idq, tmst) {
function ConfElimina() {
	if (confirm("Confirma la eliminaci�n de la Pregunta?") == true) {
	//	window.location = "QGR_DelDomanda.asp?idd="+idd+"&ida="+ida+"&idq="+idq+"&tmst="+tmst
	document.frmInsDomanda.action = "QGR_DelDomanda.asp"
	document.frmInsDomanda.submit();
	}
}

</SCRIPT>
<%
End Sub
'--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++
Sub Inizio()
	dim sFunzione
	dim sTitolo
	dim sCommento
	dim bCampiObbl
	
	sFunzione = "GESTION DE CUESTIONARIOS"
	sTitolo = "MODIFICA PREGUNTA"
	sCommento = "Modificaci�n de la Pregunta<br>Cuestionarios"
	bCampiObbl = true
	sHelp = "/Pgm/help/Formazione/QGradimento/QGR_ModDomanda/"		
%>
	<!--#include virtual="include/SetTestata.asp"-->
<%
End Sub
'--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++
Sub Modifica()
	dim sSQL, rsDom, sTDom
	dim nIdQuest, nIdArea, nIdDom, dDtTmst, nNumRisp, sTipo, sTipoSel

	nIdDom = request("id")
	nIdQuest = request("idq")
	nIdArea = request("ida")

	sSQL = "Select testo_domanda, dt_tmst, tipo_sel from iq_domanda where id_domandaiq = " & nIdDom
	set rsDom = cc.execute(sSQL)
		if not rsDom.eof then
			sTDom = rsDom("testo_domanda")
			dDtTmst = rsDom("dt_tmst")
			sTipoSel = rsDom ("tipo_sel")
		end if
	rsDom.close
	
	sSQL = "Select tipo from iq_risposta where id_domandaiq = " & nIdDom
	set rsRisposta = cc.execute(sSQL)
		if not rsRisposta.eof then
			sTipoRisposta = rsRisposta ("tipo")
		end if
	rsRisposta.close
%>	
	<BR>
	<form method=post action="QGR_CnfModDomanda.asp" onsubmit="return ControllaCampi(this)" id=frmInsDomanda name=frmInsDomanda>
	<TABLE border=0 width=500> 
		<TR>
			<TD class=tbltext1>
				<b>Texto de la Pregunta*</b>
			</TD>
			<TD>
				<INPUT id=DescDomanda maxLength=255 name=DescDomanda value="<%=sTDom%>" size=50 class=textblacka>
				<INPUT type=hidden id=IdDom name=IdDom value=<%=nIdDom%>>
				<INPUT type=hidden id=IdQuest name=IdQuest value=<%=nIdQuest%>>
				<INPUT type=hidden id=IdArea name=IdArea value=<%=nIdArea%>>
				<INPUT type=hidden id=dtTmst name=dtTmst value="<%=dDtTmst%>">
				
				<INPUT type=hidden id=txtTipoRisposta name=txtTipoRisposta value="<%=sTipoRisposta%>">
			</TD>
		</TR>
<%
			set rsDdati = server.CreateObject("ADODB.Recordset")
			sSQL =	"SELECT id_campo_desc FROM diz_dati WHERE id_tab='IQ_DOMANDA'" &_
					" AND id_campo='TIPO_SEL'"
			rsDdati.Open sSQL, CC, 3
				sTSel = rsDdati("id_campo_desc")
				aTSel = split(sTSel,"|")
			rsDdati.close
%>	
		<TR>
			<TD class=tbltext1>
				<b>Tipo de Selecci�n</b>
			</TD>
			<TD>
				<select name="cmbTipoSel" id="cmbTipoSel" class="textblacka">
					<option value="">&nbsp;</option>
					<%
					for i=0 to Ubound(aTSel) step 2
						Response.Write "<option value='" & aTSel(i) & "'"
						IF aTSel(i) = sTipoSel THEN
							Response.Write " selected"
						END IF
						Response.Write ">" & aTSel(i+1) & "</option>"
					next%>
				</select> 
			</TD>
		</TR>
	</table>
	<br><br>

	<table width=500>
		<TR>
			<td align=center>
				<%
				plsInvia("Conferma")
'''9/7/2003: PASSATI VALORI TRAMITE FORM ANZICHE' QUERYSTRING
				'''PlsElimina("javascript:ConfElimina(" & nIdDom & ", " & nIdArea & ", " & nIdQuest & ", '" & dDtTmst & "')")
				PlsChiudi()
				%>
			</td>
		</TR>
	</table>
	</form>
	<br>
<%
End Sub
'--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++
'M A I N
%>
<HTML>
<HEAD>
<TITLE><%=Session("TIT")%> [ <%=Session("login")%> ] </TITLE>
<LINK REL=STYLESHEET TYPE="text/css" HREF='<%=session("progetto")%>/fogliostile.css'>
</head>
<body class=sfondocentro topmargin="2" leftmargin="0">
<center>
<!--#include virtual="/include/openconn.asp"-->
<!--#include virtual="include/SetPulsanti.asp"-->

<%
	
	ControlliJavaScript()
	Inizio()
	Modifica()
%>
<!--#include virtual="/include/closeconn.asp"-->
</body></html>