<%
'Questo include permette di visualizzare la testata delle pagine;
'Nel programma chiamante occorre definire e valorizzare le seguenti variabili:
' - "sFunzione"   --> Nome della funzione
' - "sTitolo"     --> Titolo della pagina
' - "sCommento"   --> Commento della pagina
' - "bCampiObbl"  --> Flag per visualizzare la dicitura "Campi obbligatori"
' - "sHelp"       --> Nome della pagina contenente l'help (non attivo)
%>
<!--<table width="520px" border="0" cellspacing="0" cellpadding="0" height="70" align="center">	<tr>		<td width="500" background="<%'=session("progetto")%>/images/titoli/fad2b.gif" valign="bottom" align="right">			<table border="0" background width="500" height="23" cellspacing="0" cellpadding="0">				<tr>					<td width="100%" valign="top" align="right"><b CLASS="tbltext1a">Questionario Gradimento					</b></td>				</tr>			</table>		</td>	</tr></table> -->
<br>
<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
	<tr height="18">
		<td class="sfondomenu" width="67%" height="18"><span class="tbltext0"><b>&nbsp;<%=sTitolo%></b></span></td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
		<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">
<%			
			if bCampiObbl then
				Response.Write "(*) campi obbligatori"
			end if
%>
		</td>
	</tr>
</table>
<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
	<tr>
		<td align="left" class="sfondocomm">
			<%=sCommento%>
		<!--a href="Javascript:Show_Help('/Pgm/help/Conoscenze/CON_VisConoscenze')"-->
		<img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0" onmouseover="javascript:window.status='' ; return true"><!--/a-->
		</td>
	</tr>
	<tr height="2">
		<td class="sfondocomm" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif">
		</td>
	</tr>
</table>
<br>
