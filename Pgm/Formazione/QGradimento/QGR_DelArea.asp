<%
Sub Inizio
	dim sFunzione
	dim sTitolo
	dim sCommento
	dim bCampiObbl
	
	sFunzione = "GESTION DE CUESTIONARIOS"
	sTitolo = "ELIMINAR SECCION"
	sCommento = "Confirma la eliminaci�n de la secci�n"
	bCampiObbl = false
%>
	<!--#include virtual="include/SetTestata.asp"-->
<%
End Sub
'--------------------------------------------------------------------------
Sub Cancella()
	dim idA, idQ,sDesc,sInt,sRipet,sNum
	dim sSQL, sSQL2, rsRisp

	idA = Request.Form ("txtArea")
	sDesc= Request.Form ("txtDQ")
	sInt = Request.Form ("txtIQ")
	sRipet = Request.Form ("txtRip")
	sNum = Request.Form ("txtNumero")
	idQ = Request.form("id")
	dDtTmst = Request("dtTtmst")
	
%>
<form method="post" action="QGR_ModQuest.asp" name="frmModAree">
		<input type="hidden" name="txtNumero" value="<%=sNum%>">
		<input type="hidden" name="txtDQ" value="<%=sDesc%>">
		<input type="hidden" name="txtIQ" value="<%=sInt%>">
		<input type="hidden" name="txtRip" value="<%=sRipet%>">
		<input type="hidden" name="id" value="<%=idQ%>">
</form>
<%	
		
	'Controllo che non esistano Domande associate all'area
	sSQL =  " select count(*) as CDom" &_
			" from iq_domanda" &_
			" where id_areaiq = " & idA
	set rsCDom = cc.execute(sSQL)
	    sNumDom =rsCDom("CDom")
	rsCDom.close
	set rsCDom = nothing    
	
		if cint(sNumDom) > 0 then
%>
			<br><br>
			<table border=0 cellspacing=2 cellpadding=1 width="500">
				<tr align=middle>
					<td class="tbltext3">
						No es posibile eliminar la secci�n.<br>
						Eliminar antes las Preguntas asociadas.
					</td>
				</tr>
				<tr align=middle>
					<td align="center">
						&nbsp;<br>
						<%PlsIndietro()%>
					</td>
				</tr>
			</table>		
			<br><br>
<%
		else
			sSQL = "delete from iq_area where id_areaiq =" & idA
			sErrore=Esegui(idA ,"iq_area",Session("persona"),"DEL",sSQL,1,dDtTmst)
			IF sErrore <> "0" then
%>
				<br><br>
				<table border=0 cellspacing=2 cellpadding=1 width="500">
					<tr align=middle>
						<td class="tbltext3">
							Imposible eliminar la secci�n.
							<BR>
							Error: <%=sErrore%>
						</td>
					</tr>
					<tr align=middle>
						<td align="center">
						    &nbsp;<br>
							<%PlsIndietro()%>
						</td>
					</tr>
				</table>		
				<br><br>
<% 
			ELSE %>
				<br><br>
				<script>
					alert("Eliminaci�n correctamente efectuada")
					frmModAree.submit() 
				</script>
<%
			END IF
		end if
		
%>
<br>

	
<%
End Sub


'------------------------------------------------------------------------
'M A I N
%>
<!--#include virtual="/strutt_testa2.asp"-->
<!--#include virtual="/include/openconn.asp"-->
<!--#include virtual="/Util/DBUtil.asp"-->
<!--#include virtual="/Include/SysFunction.asp"-->
<!--#include virtual="include/SetPulsanti.asp"-->
<%	
	Inizio()
	
	Cancella()
	
%>
<!--#include virtual="/include/closeconn.asp"-->
<!--#include virtual="/strutt_coda2.asp"-->
