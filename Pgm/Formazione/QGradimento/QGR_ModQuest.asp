<%
Sub ControlliJavaScript()
%>
<script LANGUAGE="Javascript">
<!--#include virtual="include/ControlString.inc"-->

function inviaAree(){
    frmInsQuest.txtAree.value = TRIM(frmInsQuest.txtAree.value)
   	if (frmInsQuest.txtAree.value == "") {
		alert("Inserire il numero delle sezioni")
		frmInsQuest.txtAree.focus() 
		return
	}
	if (isNaN(frmInsQuest.txtAree.value)) {
		alert("Il numero delle sezioni deve essere numerico")
		frmInsQuest.txtAree.focus() 
		return
	}

	if (parseInt(frmInsQuest.txtAree.value) <= parseInt(frmInsQuest.txtAreeHid.value)) {
			alert("Il numero delle sezioni non pu� essere minore o uguale del numero di sezioni esistenti.\nPer eliminare le sezione selezionare il cestino.")
			frmInsQuest.txtAree.focus() 
			return 
	}
	else{
		frmInsAree.txtDQ.value = frmInsQuest.DescQuest.value
		frmInsAree.txtIQ.value = frmInsQuest.IntQuest.value 
		frmInsAree.txtNumero.value = frmInsQuest.txtAree.value 
		frmInsAree.idq.value = frmInsQuest.IdQuest.value 
		frmInsAree.txtRip.value = frmInsQuest.ckRipetibile.checked 
		frmInsAree.numaree.value = 1		
		frmInsAree.submit() 
	}
}

function eliminaAree(id){
	frmElimina.txtArea.value = id;
	frmElimina.submit();  
}

function modificaAree(idq,ida)
{
	var URL
	URL = "QGR_ModArea.asp?idq=" + idq + "&ida=" + ida 
	window.open(URL,"Area","Status=no,scrollbars=yes,toolbar=no,width=550,height=500,top=20,left=100");
}
//function modificaAree(ida,idq){
//	frmModifica.idq.value = idq;
//	frmModifica.ida.value = ida;
//	frmModifica.submit();  
//}

function ControllaCampi(frmInsQuest) {

	frmInsQuest.DescQuest.value = TRIM(frmInsQuest.DescQuest.value)
	if (frmInsQuest.DescQuest.value == "") {
		alert("Inserire la 'Descrizione Questionario'")
		frmInsQuest.DescQuest.focus() 
		return false
	}
//alert("frmInsAree.numaree.value" + frmInsAree.numaree.value);
//alert("frmInsQuest.txtAree.value" + frmInsQuest.txtAree.value);

	//if (frmInsAree.numaree.value == "" && frmInsQuest.txtAree.value != "" && frmInsQuest.txtAree.value != 0 ) {
	//	alert("Selezionare 'aggiungi' e inserire le descrizioni delle sezioni")
	//	return false
	//}	
		
	frmInsQuest.IntQuest.value = TRIM(frmInsQuest.IntQuest.value)
	if (frmInsQuest.IntQuest.value == "") {
		alert("Ingresar el 'Encabezado del Cuestionario'")
		frmInsQuest.IntQuest.focus() 
		return false
	}
	
	//frmInsQuest.txtAree.value = TRIM(frmInsQuest.txtAree.value)
	//if (frmInsQuest.txtAree.value != "" && frmInsQuest.txtAree.value != "0") {
	   
	//	if (isNaN(frmInsQuest.txtAree.value)) {
	//		alert("Il numero delle sezioni deve essere numerico")
	//		frmInsQuest.txtAree.focus() 
	//		return false
	//	}

	//	if (frmInsQuest.txtAree.value < frmInsQuest.txtAreeHid.value) {
	//		alert("Il numero delle sezioni non puo' essere minore del numero di sezioni esistenti.\nPer eliminare le sezione selezionare il cestino")
	//		frmInsQuest.txtAree.focus() 
	//		return false
	//	}
	//	var Narea
	//	var appo
	//	var VALORE
	//	var ro = / /g
		
	//	Narea = frmInsQuest.txtAree.value
	//	for(i=0;i<Narea;i++){
	//	    appo = i + 1
	//	    VALORE =eval('frmInsQuest.DescArea' + appo + '.value')
	//	    VALORE = VALORE.replace( ro, "" )
		    
	//	    if(VALORE==""){
	//	     	alert("Inserire la descrizione della sezione " + appo)
	//			eval('frmInsQuest.DescArea' + appo + '.focus()')
	//		    return false
	//		}
	//	}   
	//}
}


function ConfElimina() {
	if (confirm("Confirma la eliminaci�n del Cuestionario?") == true) {
		document.frmInsQuest.action = "QGR_DelQuest.asp"
		document.frmInsQuest.submit();
	}
}

</script>
<%
End Sub

'------------------------------------------------------------------------
Sub Inizio
	dim sFunzione
	dim sTitolo
	dim sCommento
	dim bCampiObbl
	
	sFunzione = "GESTION DE CUESTIONARIOS"
	sTitolo = "MODIFICA CUESTIONARIOS"
	sCommento = "En esta pagina puede intervenir sobre elementos descriptivos del cuestionario, haciendo clic en  <B> Envia</B>, o puede eliminarlo haciendo clic en  <B>Elimina</B>. <BR>Para eliminar o modificar una o m�s secciones constituyentes, haga clic sobre el icono asociado a la secci�n que se quiere eliminar o modificar. "  
	bCampiObbl = true
	sHelp = "/Pgm/help/Formazione/QGradimento/QGR_ModQuest/"	
%>
	<!--#include virtual="include/SetTestata.asp"-->
<%
End Sub
'------------------------------------------------------------------------
Sub Modifica()
%>
<body>
<!--#include virtual ="/include/openconn.asp"-->
<%
dim sSQL, rsTades
dim sSEL, rsQuest
dim sRipetibile
dim sDescQuest, sTipoQuest, sIntestQuest, sModExec, sCodSess, dDtTmst

nIdQuest = request("id")

'sSEL = "Select desc_quest,tipo_quest,intest_quest," &_
'       "mod_exec,fl_repeat,dt_tmst from info_quest " &_
'       "where id_infoquest = " & nIdQuest

sSEL = "Select desc_quest,tipo_quest,intest_quest," &_
       "fl_repeat,dt_tmst from info_quest " &_
       "where id_infoquest = " & nIdQuest


set rsQuest = cc.execute(sSEL)

if not rsQuest.eof then
	sDescQuest = rsQuest("desc_quest")
	
'sTipoQuest vuoto perche non pi� richiesto	
'	sTipoQuest = rsQuest("tipo_quest")
	sTipoQuest = ""
	
	sIntestQuest = rsQuest("intest_quest")

'sModExec vuoto perche non pi� richiesto	
'	sModExec = rsQuest("mod_exec")
	sModExec = ""
	
	'sCodRuolo = rsQuest("cod_ruolo")
	sRuolo	= rs

    sRipetibile = rsQuest("fl_repeat")
	dDtTmst = rsQuest("dt_tmst")

end if
%>
<form name="frmElimina" method="post" action="QGR_DelArea.asp">
	<input type="hidden" name="txtArea">
	<input type="hidden" name="txtNumero" value="<%=Numero%>">
	<input type="hidden" name="txtDQ" value="<%=Descrizione%>">
	<input type="hidden" name="txtIQ" value="<%=Intestazione%>">
	<input type="hidden" name="txtRip" value="<%=Ripetibile%>">
	<input type="hidden" name="id" value="<%=nIdQuest%>">
</form>

<form name="frmModifica" method="post" action="QGR_ModArea.asp">
	<input type="hidden" name="idq" value="<%=nIdQuest%>">
	<input type="hidden" name="ida" value="<%=nIdArea%>">
</form>

<form method="post" action="QGR_CnfModQuest.asp" onsubmit="return ControllaCampi(this)" id="frmInsQuest" name="frmInsQuest">
<table border="0" width="500"> 
	<tr>
		<td class="tbltext1" >
			<b>T�tulo Cuestionario*</b>
		</td>
		<td>
			<input id="DescQuest" maxLength="200" size="41" name="DescQuest" value="<%=sDescQuest%>" class="textblacka"> 
			<input type="hidden" id="IdQuest" name="IdQuest" value="<%=nIdQuest%>">
			<input type="hidden" id="dtTmst" name="dtTmst" value="<%=dDtTmst%>">
			<input type="hidden" name="ida" value="<%=nIdArea%>">
		</td>
	</tr>
	<!--TR>		<TD class=tbltext1>			<b>Tipo Questionario*</b>		</TD>		<TD>			<INPUT id=TipoQuest maxLength=2 size="10" name=TipoQuest value="<%=sTipoQuest%>" class=textblacka>		</TD>	</TR-->
	<tr>
		<td vAlign="top" class="tbltext1">
			<b>Descripci�n del Cuestionario*</b><br>
			<span class="tbltext1">- Le quedan  
				<b><label name="NumCaratteri" id="NumCaratteri">2000</label></b>
				caracteres disponibles -
			</span>
		</td>
		<td>
			<textarea id="IntQuest" name="IntQuest" rows="5" cols="40" class="textblacka" maxlength="2000" onKeyup="JavaScript:CheckLenTextArea(document.frmInsQuest.IntQuest,NumCaratteri,2000)"><%=sIntestQuest%></textarea>
		</td>
	</tr>
	<!--TR>		<TD class=tbltext1>			<b>Modalit� di Esecuzione</b>		</TD>		<TD>			<INPUT id=ModExec maxLength=1 size=10" name=ModExec value="<%=sModExec%>" class=textblacka>		</TD>	</TR-->
	<tr>
		<td class="tbltext1">
			<b>Repetible</b>
		</td>
<%if cint(sRipetibile)= 1 then %>		
		<td align="left">
			<input type="checkbox" id="ckRipetibile" name="ckRipetibile" class="textblacka" value="1" checked>
		</td>
<%else%>
        <td align="left">
			<input type="checkbox" id="ckRipetibile" name="ckRipetibile" class="textblacka" value="1">
		</td>
<%end if%>		
	</tr>
	<tr>
		<td COLSPAN="2" ALIGN="LEFT"><hr WIDTH="92%"></td>
	</tr>


<%	'31/05/2007 tolta l'order by desc_areaiq
	sqlnum="SELECT ID_AREAIQ,DESC_AREAIQ FROM IQ_AREA WHERE ID_INFOIQ=" & nIdQuest '&_
	      ' " ORDER BY DESC_AREAIQ"

	SET rsNum=Server.CreateObject("ADODB.Recordset")
	rsNum.Open sqlnum,CC,3

	appoNum = rsNum.RecordCount 

	if Numero = "" then
	   Numero = appoNum
	else
	   Numero = Numero
	end if 
	if nMod = 0 then
		if not controllaStruttura( nIdQuest ) then
%>	

	<tr>
		<td class="tbltext1">
			<!--b>Indicare il numero di sezioni<br>e selezionare &quot;aggiungi&quot;</b-->
		</td>
		<td>
		<table border="0" width="250">
			<td align="left"><b><a class="textred" href="javascript:inviaAree()">Agregar una Secci�n</a></b>
			<td align="left" width="20">
				<input type="hidden" name="txtAree" class="textblacka" size="2" maxlength="2" value="<%=Numero+1%>">
			    <input type="hidden" name="txtAreeHid" class="textblacka" size="2" maxlength="2" value="<%=Numero%>">
			</td>

		</table>
		</td>
	</tr>
	
	
<%		else %>

	<tr>
		<td colspan="2" align="left" class="tbltext1">
				Existen unas preguntas por lo que no es posible agregar un secci�n. Eliminar antes las preguntas.
		</td>
	</tr>


<%		end if
	end if
	for i = 1 to clng(Numero)%>
		<tr>
			<td Align="right" class="tbltext1">
			<b>Secci�n&nbsp;<%=i%></b>
		</td>
		<td>
		<%if not rsNum.EOF then%>
			<input readonly type="text" maxLength="200" size="33" name="DescArea<%=i%>" class="textblacka" value="<%=rsNum("DESC_AREAIQ")%>">
		    <%if nMod = 0 then%>
		    <a onmouseover="javascript:window.status='' ; return true" href="javascript:eliminaAree('<%=rsNum("ID_AREAIQ")%>')">
			     <img src="<%=Session("Progetto")%>/images/cestino.gif" border="0" id="image1" name="image1" alt="elimina secci�n" title="elimina secci�n">
		    <a onmouseover="javascript:window.status='' ; return true" href="javascript:modificaAree('<%=nIdQuest%>','<%=rsNum("ID_AREAIQ")%>')">
			     <img src="<%=Session("Progetto")%>/images/moddoc.gif" border="0" id="image2" name="image2" alt="modifica secci�n" title="modifica secci�n" WIDTH="16" HEIGHT="16">
			</a>
		    <%end if%>
			
		<%  rsNum.MoveNext 
				else%>
			<input type="text" maxLength="200" size="41" name="DescArea<%=i%>" class="textblacka">
		<%end if%>
		</td>
		</tr>
<%  next
rsNum.Close 
set rsNum = nothing
%>	

	<tr>
		<td COLSPAN="2" ALIGN="LEFT"><hr WIDTH="92%"></td>
	</tr>
	
</table>
<br>
<table border="0" width="500">
	<tr>
		<td align="center">
			<a onmouseover="javascript:window.status='' ; return true" href="QGR_VisQuestionario.asp">
		         <img src="<%=Session("progetto")%>/images/indietro.gif" border="0" onmouseover="javascript:window.status='' ; return true">
	        </a>
			<%
			if nMod = 0 then
				PlsInvia ("Conferma")
				PlsElimina("javascript:ConfElimina();")
			end if
			%>
		</td>
	</tr>
</table>
</form>
<%
End Sub

function controllaStruttura(idQuest)

	dim sSQL
	controllaStruttura = false

	sSQL = "SELECT COUNT(*) as CONTA FROM STRUTTURA_QUEST WHERE ID_INFOQUEST = " & idQuest
	
	set rsConta = CC.Execute(sSQL)
	if cint(rsConta("CONTA")) > 0 then
		controllaStruttura = true
	end if

end function

'------------------------------------------------------------------------
'M A I N
%>
<!--#include virtual="/strutt_testa2.asp"-->
<!--#include virtual="/include/openconn.asp"-->
<!--#include virtual="include/SetPulsanti.asp"-->
<!--#include virtual="include/RuoloFunzionale.asp"-->
<%
dim Numaree, nIdQuest

if ValidateService(session("idutente"),"QGR_VisQuestionario", CC) <> "true" then 
	response.redirect "/util/error_login.asp"
end if

numaree = Request.Form("numaree")
nIdQuest = request("id")
nMod	= request("Mod")
'Response.Write "numaree = " & numaree & "<br>"
	
Session("ck_cod_ruofu")=""
%>	<form method="post" action="QGR_InsArea.asp" name="frmInsAree">
	<input type="hidden" name="txtNumero">
	<input type="hidden" name="txtDQ">
	<input type="hidden" name="txtIQ">
	<input type="hidden" name="txtRip">
	<input type="hidden" name="idq" value="<%=nIdQuest%>">
	<input type="hidden" name="numaree" value="<%=numaree%>">
</form>

<form id="frmReload" name="frmReload" action="QGR_ModQuest.asp" method="post">
	<input type="hidden" name="id" value="<%=nIdQuest%>">
</form>
<%
dim Numero,Descrizione,Intestazione,Ripetibile

    Numero =Request.Form ("txtNumero")
    Descrizione = Request.Form ("txtDQ")
    Intestazione = Request.Form ("txtIQ")
    Ripetibile = Request.Form ("txtRip")


	ControlliJavaScript()
	Inizio()
	Modifica()
%>
<!--#include virtual="/include/closeconn.asp"-->
<!--#include virtual="/strutt_coda2.asp"-->
