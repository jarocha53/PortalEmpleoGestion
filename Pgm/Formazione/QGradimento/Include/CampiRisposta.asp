					
<%

function CampiRisposta()

	SELECT CASE TIPOR
		CASE "T"
			sTestoRisp = RRR.Fields("TXT_RISPOSTA")
			if TIPODOMRIS = "A" then
			'Non serve controllo che Tipo Selezione sia Escludente, Multipla o null.
			%>
			<TR class=tblsfondo>
				<td align="left" class="tbltext">
					<TEXTAREA id=Altro name=Altro rows=2 cols=40 readonly class=textblacka value="<%=IDDOM%>-1"><%=lcase(sTestoRisp)%>
					</TEXTAREA>
				</td>
			</TR>	
			<%
			end if
										
			'Tipo Selezione = Escludente(E)	
			IF TIPOSEL = "E" THEN
				If TIPODOMRIS = "C" then
				%>
				<tr class=tblsfondo>
					<td align="left" class="tbltext">
						<input type="radio" name="DOM<%=ndom%>" value="<%=IDDOM%>-1"><%=lcase(sTestoRisp)%>
					</td>
				</tr>	
				<%
				End If
				If TIPODOMRIS = "S" then
				%>
					<tr class=tblsfondo>
						<td align="left" class="tbltext">
							<input type="radio" name="DOM<%=ndom%>" value="<%=IDDOM%>-1"><%=lcase(sTestoRisp)%>
							<%
							if FLTIPORIS = "A" then
							%>
									<TEXTAREA id=Altro name=Altro rows=1 cols=40 class=textblacka readonly value="<%=IDDOM%>-1"></TEXTAREA>
							   </td>	
							<%
							end if
							%>
						</td>
					</tr>
						<%
				End If
			END IF
			'Tipo Selezione = Multipla(M)	
			IF TIPOSEL = "M" THEN
				If TIPODOMRIS = "S" then	
			%>
					<tr class=tblsfondo>
						<td align="left" class="tbltext">
							<input type="checkbox" name="chkMulti<%=ndom%>" id="chkMulti" value="<%=IDDOM%>-1"><%=lcase(sTestoRisp)%>
					<%
					if FLTIPORIS = "A" then
					%>
								<TEXTAREA id=Altro name=Altro rows=1 cols=40 class=textblacka readonly value="<%=IDDOM%>-1"></TEXTAREA>
						   </td>	
					<%
					end if
					%>	
						</td>
					</tr>
					<%
				End If
				If TIPODOMRIS = "C" then
				%>	
					<tr class=tblsfondo>
						<td align="left" class="tbltext">
							<input type="checkbox" name="chkMulti<%=ndom%>" id="chkMulti" value="<%=IDDOM%>-1"><%=lcase(sTestoRisp)%>
						</td>
					</tr>	
				<%
				End If
			END IF
								
			'Controllo congruenza Tipo Selezione per risposta Testo e domanda Chiusa/Semiaperta.
			IF isnull(TIPOSEL) THEN
				If TIPODOMRIS = "C" or TIPODOMRIS = "S" then 
					if cVerify=1 then %>
						<tr class=tblsfondo>
							<td align="center">
								<span class="tbltext3"><span class="size">
								<b>Attenzione: Tipo Selezione non definito!!!</b>
								</span></span>
							</td>
						</tr>
			<%			cVerify=cVerify+1
					end if
				End if
			END IF
														
		CASE "V"
			if not isnull(RRR.Fields("IQ_MIN")) then
			    TestoMin = RRR.Fields("TESTO_MIN")
				RMIN  = CLNG(RRR.Fields("IQ_MIN"))	' PQ 18-06-2001
			end if
			if not isnull(RRR.Fields("IQ_MAX")) then
			    TestoMax = RRR.Fields("TESTO_MAX")
				RMAX  = CLNG(RRR.Fields("IQ_MAX")) ' PQ 18-06-2001 
			end if
									
			'Tipo Selezione = Escludente(E)	
			IF TIPOSEL = "E" THEN
			%>
				<tr class=tblsfondo>
					<td align="left" class="tbltext">
					<%
					'visualizzazione label testuale relativo al Min
					Response.Write TestoMin 
					for r=rmin to RMAX
					%>
					  &nbsp;<input type="radio" name="DOM<%=ndom%>"  value="<%=IDDOM%>-<%=r%>"><%=r%>&nbsp;
					<%			
					nr = nr + 1
					next
					'visualizzazione label testuale relativo al Max
					Response.Write TestoMAX
					%>
					</td>
				</tr>
			<%
			END IF
			'Tipo Selezione = Multipla(M)
			IF TIPOSEL = "M" THEN
			%>
				<tr class=tblsfondo>
					<td align="left" class="tbltext">
					<%
					'visualizzazione label testuale relativo al Min
					Response.Write TestoMin 
					for r=rmin to RMAX
					%>
						&nbsp;<input type="checkbox" name="chkMulti<%=ndom%>_<%=r%>" value="<%=IDDOM%>-1"><%=r%>&nbsp;
					<%			
					nr = nr + 1
					next
					'visualizzazione label testuale relativo al Max
					Response.Write TestoMAX
					%>
					</td>
				</tr>
			<%
			END IF
									
			'Controllo congruenza Tipo Selezione per risposta Valore.
			IF isnull(TIPOSEL) THEN
			%>
				<tr class=tblsfondo>
					<td align="center">
						<span class="tbltext3"><span class="size">
						<b>Attenzione: Tipo Selezione non definito!!!</b>
						</span></span>
					</td>
				</tr>
			<%
			END IF
								
		END SELECT
							
end function
	%>