<!--#include virtual ="/Strutt_testa2.asp"-->
<!--#include virtual = "/Include/OpenConn.asp"-->
<!--#include virtual = "Include/HTMLEncode.asp"-->
<!--#include virtual="/util/portallib.asp"-->
<!--#include virtual="include/RuoloFunzionale.asp"-->
<!--#include virtual = "include/DecCod.asp"-->
<!--#include virtual = "/pgm/Comunicazioni/DecDizDati.asp"-->

<%
	if ValidateService(session("idutente"),"QGR_INDQUEST", CC) <> "true" then 
		response.redirect "/util/error_login.asp"
	end if

Dim iValCmb,iValCmb2, iValCmbSess, sInd
Dim Rs
Dim Sql, sSQL
Dim CC
Dim sProgetto
Dim bPresenzaBando, hPresenzaBando
Dim nIndElem

	iValCmb=Request("CmbQuest")
	iValCmb2=Request("CmbQuest2")
	IF iValCmb = "" AND iValCmb2 <> "" THEN
		iValCmb = iValCmb2
	END IF
	
	sProgetto = mid(session("Progetto"), 2)
%>

<html>
<head>
<script LANGUAGE="javascript">
	<!--#include virtual = "/Include/ControlDate.inc"-->	
	<!--#include Virtual = "/Include/help.inc"-->
	<!--#include virtual = "/Include/ControlNum.inc"-->
	<!--#include Virtual = "/Include/ControlString.inc"-->
			
//================================******=============================			
	function Aggiungi()
	{
		if (document.frmInserimento.CmbQuest2.value == ""){
		alert('Selezionare un Questionario')
		document.frmInserimento.CmbQuest2.focus();
		return false;
		}
			
		
		if (document.frmInserimento.cmbInd.value == ""){
		alert('Selezionare un Indicatore di Indagine')
		document.frmInserimento.cmbInd.focus();
		return false;
		}	 						
	}					
//================================******=============================			
//per fare Refresh della pagina 

	function Controllo(nVal) {
		var ruofu
				
		if ((document.frmInserimento.cmbInd.value != "") && (document.frmInserimento.CmbQuest2.value == ""))  {
			alert('Selezionare prima un Questionario')
			document.frmInserimento.cmbInd.value = ""
			document.frmInserimento.CmbQuest2.focus()
			return false
		}
				
//		
		if (nVal == 1) {
			document.frmInserimento.action = "QGR_IndQuest.asp"
			document.frmInserimento.submit();
		}	
	}
//================================******=============================			
	function ControllaDati(){
		var ok
		var nrec
		var chkbox
				
		ok = 0 
		nrec= (document.frmElimina.txtNumElementi.value)
		for (i=1; i<=nrec; i++){
			chkbox = eval("document.frmElimina.ckFlag" + i)
			if (chkbox.checked){
				ok = 1
				break
			}
		}
		if (ok == 0){
			alert ('Selezionare almeno una voce dalla tabella!')
			return false
		}
		document.frmElimina.submit();
	}
</script>
</head>

<!-- *==============================******=============================* -->
	<body>
	<table border="0" width="520" height="81" cellspacing="0" cellpadding="0">
		<tr>
			<td width="500" height="81" background="<%=Session("Progetto")%>/images/titoli/strumenti2b.gif" valign="bottom" align="right">
				<table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
					<tr>
						<td width="100%" valign="top" align="right">
							<b class="tbltext1a">Asociacion Indicadores Encuestas</b>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<br>
	<table cellpadding="0" cellspacing="0" width="500" border="0">
		<tr height="18">
			<td class="sfondomenu" height="18" width="60%">
				<span class="tbltext0">
					<b>&nbsp; ASOCIACION INDICADORES ENCUESTAS</b>
				</span>
			</td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">
			</td>
			<td valign="middle" align="right" width="35%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1">(*) campos obligatorios
			</td>
		</tr>
		<tr width="371" class="SFONDOCOMM">
			<td colspan="3">
				Per visualizzare l'associazione del Questionario con l'Indicatore 
				di Indagine premere su <b>Ricerca</b>.<br>
				Lasciando vuoto il campo verr� effettuata una ricerca libera.<br>
				Per Associare un questionario a un Indicatore di Indagine cliccare su <b>Aggiungi</b>.<br>
				Per Eliminare una associazione selezionare il relativo record e cliccare su <b>Elimina</b>.
				<a href="Javascript:Show_Help('/Pgm/help/Formazione/QGradimento/QGR_IndQuest')" name="prov3" onmouseover="javascript:window.status='' ; return true">
					<img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0">
				</a>
			</td>
		</tr>
		<tr height="2">
			<td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>

<!--FORM DI RICERCA-->
	<form name="frmRicerca" action="QGR_IndQuest.asp" method="post">
	<table border="0" CELLPADDING="1" CELLSPACING="2" width="500">
		<tr>
			<td class="tbltext1" width="150" align="left">
				<b>Questionario*</b>
			</td>
			<td align="left">
				<%
				Sql= "SELECT ID_INFOQUEST, DESC_QUEST FROM INFO_QUEST " &_
					 "ORDER BY DESC_QUEST"
						
				Set Rs = CC.Execute (Sql)
					Response.Write "<SELECT class='textblack' name='CmbQuest'>"
					Response.Write "<option value=''></option>"
					If Not Rs.eof then
						DO
							Response.Write "<option value=" & Rs("ID_INFOQUEST") & ">" & Rs("DESC_QUEST") & "</option>"
							Rs.MOVENEXT
						LOOP UNTIL Rs.EOF
					End If
				Rs.close
				Set Rs = Nothing
				Response.Write "</SELECT>"
				%>
			</td>
			<td align="left">
				<input type="image" name="Cerca" value="Ricerca" src="<%=Session("Progetto")%>/images/lente.gif" border="0">		
			</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
		</tr>
		<tr>
		    <td height="2" align="left" colspan="4" background="<%=Session("Progetto")%>/images/separazione.gif">
		    </td>
	    </tr>
	</table>				
	</form>
	
<!--FORM DI INSERIMENTO-->
	<form name="frmInserimento" action="QGR_CnfIndQuest.asp" method="post" onsubmit="return Aggiungi(this);">
	<table width="500" border="0" cellspacing="1" cellpadding="1">
		<tr> 
			<td class="tbltext1" align="left" width="150">
				<b>Cuestionario*</b>
			</td>
			<td align="left" COLSPAN="2">
				<%
				Sql= "SELECT ID_INFOQUEST, DESC_QUEST FROM INFO_QUEST "
				IF iValCmb <> "" then
					Sql= Sql & " WHERE ID_INFOQUEST=" & iValCmb
				ELSE			
					Sql = Sql & " ORDER BY DESC_QUEST"
				END IF
	'Response.Write "sql: " & sql
				Set Rs = CC.Execute (Sql)
				Response.Write "<SELECT class='textblack' name='CmbQuest2'>"
					IF iValCmb <> "" then
						Response.Write "<option value=" & iValCmb & ">" & Rs("DESC_QUEST") & "</option>"				
					ELSE
						Response.Write "<option value=''></option>"
						
						If Not Rs.eof then
							DO
							'*inizio 10/09
							'if iValCmb2 <> "" and  nIndElem = 0  then
							'*	
								If clng(iValCmb2) = clng(Rs("ID_INFOQUEST")) then
									Response.Write "<option selected value=" & Rs("ID_INFOQUEST") & ">" & Rs("DESC_QUEST") & "</option>"
								else
									Response.Write "<option value=" & Rs("ID_INFOQUEST") & ">" & Rs("DESC_QUEST") & "</option>"
								End if
							'*	
							'	Response.Write"<strong>Non sono presenti occorrenze nella tabella.</strong>"
							'end if
						'fine 10/09	
								Rs.MOVENEXT
							LOOP UNTIL Rs.EOF
						End If
					END IF
				Response.Write "</SELECT>"
				Rs.close
				Set Rs = Nothing
				%>
			</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td class="tbltext1" align="left">
				<b>Indicadores de Encuesta*</b>
			</td>
			<td align="left">
				<%
					sInd = "TQIND|0|"& date() &"|0|cmbInd' onchange='javascript:Controllo(0)'|ORDER BY DESCRIZIONE"
						CreateCombo (sInd)
				%>	
			</td>
		</tr>	
		<tr>
			<td>&nbsp;</td>
		</tr>		
		<tr>
			<td align="center" colspan="2">
				<input type="image" src="<%'=impostaProgetto() %>/images/aggiungi.gif" border="0" value="Aggiungi" align="top" id="Aggiungi" name="Aggiungi">
			</td>
		</tr>
	</table>
	</form>
		
<!--FORM DI ELIMINAZIONE-->			
<form name="frmElimina" method="post" action="QGR_CanIndQuest.asp">
<% 	
on error resume next 
   	set rsQuest2= server.CreateObject("ADODB.Recordset")

   
	If iValCmb= "" Then
		sSQL =	"SELECT iq.DESC_QUEST, ii.ID_INFOQUEST, ii.COD_IND_INDAGINE FROM IQ_IND_INDAGINE ii,INFO_QUEST iq " &_
				" WHERE ii.id_infoquest = iq.id_infoquest" &_
				" ORDER BY IQ.DESC_QUEST "
	Else
		sSQL =	"SELECT iq.DESC_QUEST, ii.ID_INFOQUEST, ii.COD_IND_INDAGINE FROM IQ_IND_INDAGINE ii,INFO_QUEST iq " &_
				" WHERE ii.id_infoquest = iq.id_infoquest" & _
				" AND ii.id_infoquest =" & iValCmb &_
				" ORDER BY IQ.DESC_QUEST "
	End If			
	set rsQuest2= CC.execute (sSQL)
		
	If not rsQuest2.eof Then
		
	    if clng(nIndElem) = 0 then 
			titoliTabella()
	%>
			<!--table WIDTH="500" cellspacing="1" cellpadding="1" align="center" BORDER="0"-->
	<%	end if 
		While rsQuest2.EOF <> True 
			nIndElem = nIndElem + 1 
	%>
		<input type="hidden" value="<%= rsQuest2("ID_INFOQUEST")%>" name="txtidQuest<%=nIndElem%>">

		<tr class="tblsfondo">
			<td width="25"><input type="checkbox" name="ckFlag<%=nIndElem%>"></td>
			<%
			'If iValCmb= "" Then%>
			<td class="tblDett" width="145">
				<%'=strHTMLEncode (rsQuest2("DESC_QUEST"))%>
				<%=rsQuest2("DESC_QUEST")%>
				<input type="hidden" value="<%=rsQuest2("DESC_QUEST")%>" name="txtQuest<%=nIndElem%>">
			</td>
			
			
			<td class="tblDett" width="130">
			<%
				if rsQuest2("COD_IND_INDAGINE") <> "" then
					Response.Write DecCodVal("TQIND",0,date(),rsQuest2("COD_IND_INDAGINE"),1)
				else	
					Response.Write "---"
				end if
				Response.Write "&nbsp;&nbsp;"
				%>
				<input type="hidden" value="<%=rsQuest2("COD_IND_INDAGINE")%>" name="txtInd<%=nIndElem%>">
			</td>
		</tr>
		<%
		rsQuest2.MoveNext
		Wend
		rsQuest2.Close
		Set rsQuest2 = Nothing
	End if	
	%>
	
	<%if clng(nIndElem) = 0 then  
	    Response.Write "<table align=center>"
		Response.Write"<tr>"
		Response.Write"<td class=tbltext3><br>"
		Response.Write"<strong>Non sono presenti occorrenze nella tabella.</strong>"
		Response.Write"</td>"
		Response.Write"</tr>"
		Response.Write"</table>"
	  else
	    Response.Write "</table>" 	
	  end if	
	%>
	<input type="hidden" value="<%=nIndElem%>" name="txtNumElementi">
</form>

	<table border="0" cellspacing="0" cellpadding="0" width="500">
		<tr>
			<td align="middle">
				<input type="image" src="<%=session("Progetto")%>/images/elimina.gif" border="0" value="Elimina" align="top" id="image1" name="image1" onclick="Javascript:ControllaDati();">
			</td>
		</tr>
	</table>
	</body>
</html>
<% sub titoliTabella() %>
       <table width="500" cellspacing="1" cellpadding="1" align="center" border="0">
			<tr class="sfondocomm">
				<td width="25" align="center">
					<b> <img src="<%=session("Progetto")%>/images/cestino.gif"></b>
				</td>
				<td width="145">
					<b>Questionario</b>
				</td>
				<td width="100">
					<b>Indicatore di Indagine</b>
				</td>
			</tr>
	  
<% end sub %>
<!--#include virtual ="/include/closeconn.asp"-->
<!--#include virtual ="/Strutt_coda2.asp"-->