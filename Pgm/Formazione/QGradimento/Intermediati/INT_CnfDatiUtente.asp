<%'Option Explicit
  Response.ExpiresAbsolute = Now() - 1 
  Response.AddHeader "pragma","no-cache"
  Response.AddHeader "cache-control","private"
  Response.CacheControl = "no-cache"

%> 
<!--#include virtual="/strutt_testa2.asp"-->
<!--#include virtual ="/include/openconn.asp"-->
<!--#include virtual="/util/portallib.asp"-->
<%
'if ValidateService(session("idutente"),"SEL_CNFDOMISCR", CC) <> "true" then 
'	response.redirect "/util/error_login.asp"
'end if
%>
<!-- ************** Javascript Inizio ************ -->


<script LANGUAGE="javascript">


function VaiInizio(strBando){
	location.href = "SEL_InsDomIscr.asp?txtIdBando=" + strBando;
}	
</script>
<script language=javascript>	
	
	function CompilaQuestVisArea()
	{		
		document.FrmCompilaQuest.submit();
	}	
	
	
</script>
<!-- ************** Javascript Fine ************ -->
<!-- ************** ASP inizio *************** -->
<!--#include virtual = "/include/SysFunction.asp"-->
<!--#include virtual = "/include/ControlDateVB.asp"-->
<!--#include virtual = "/util/dbutil.asp"-->

<%
'*************************************************************
sub MessaggioErrori(DescrizioneErrore) 
%>

<br>
<table cellpadding="0" cellspacing="0" width="500" border="0">
		<tr height="18">
			<td class="sfondocommQuest" height="18" width="50%" align="center">
			<span class="tbltext0"><b>&nbsp;Anagrafica</b><br> </span></td>
			<TD height="18" width="50%"align=center valign=middle class=sfondomenu><b>Questionario</b></TD>
		</tr>		
		<!--tr height="2">
			<td colspan="3" class="sfondocomm" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr-->
	</table>
<br><br>

   	 <table border="0" cellspacing="1" cellpadding="1" width="370">
   	   <tr>
   		  <td class="tbltext3" align="center">Compilazione del questionario non effettuabile.</td>
   	  </tr>
   	<tr>
   		<td class="tbltext3" align="center"><%Response.Write (DescrizioneErrore)%></td>
   	</tr>
   	</table>
   	<br><br>
				
   	<table border="0" cellpadding="0" cellspacing="1" width="370">
   		<tr>
   		<td align="center" colspan="2" width="60%"><b>
   		
   			<a HREF="javascript:history.go(-1)"><img SRC="<%=Session("Progetto")%>/images/indietro.gif" border="0" onmouseover="javascript:window.status=' '; return true"></a>
   			   			
   			</b>
   		</td>
   		</tr>
   	</table> 
 


<!--<br><br>		<table width="500" border="0" cellspacing="2" cellpadding="1">			<tr align="middle">				<td class="tbltext">					<b>Inserimento della Domanda d'Iscrizione non effettuabile.</b>				</td>			</tr>			<tr align="middle">				<td class="tbltext"><b><%=DescrizioneErrore%></b></td>			</tr>		</table><br><br><table width="500" border="0" cellspacing="2" cellpadding="1">	<tr align="center">		<td nowrap><a href="javascript:VaiInizio()"><img src="<%=session("Progetto")%>/images/indietro.gif" border="0" name="imgPunto2" onmouseover="javascript:window.status=' '; return true"></a></td>	</tr></table>   -->
<%
end sub

 	
   

'*************************************************************
sub AcquisizioneParametri()
'----------------------Cognome*
sCognome= Trim(UCase(Replace(Request("txtCognome"),"'","''")))
'----------------------Nome*
sNome= Trim(UCase(Replace(Request("txtNome"),"'","''")))
'----------------------Data di Nascita*
sDataNascita = ConvDateToDb(ConvStringToDate(Request("txtDataNascita")))
'----------------------Provincia di Nascita
sProvNascita = UCase(Request("cmbProvinciaNascita"))
'---------------------Comune di Nascita*
sComuneNascita = Trim(UCase(Request("txtComune")))
'---------------------Nazione di Nascita
sNazNascita = UCase(Request("cmbNazioneNascita"))
'---------------------Sesso*
sSesso = UCase(Request("cmbSesso"))
'---------------------Codice Fiscale*
sCodFisc = Trim(UCase(Request("txtCodFisc")))


'sCodFisc = Replace(server.HTMLEncode(sCodFisc), "'", "''")
vuoto = 0
if trim(sCodFisc)="" then
     sCodFisc = Session("idutente")
     vuoto = 1
end if

sTipoDest = UCase(Request("txtTipoDest"))
sIdUtente = Request("txtIdUtente")
nIdProj = Request("txtIdProj")
nIdQuest = Request("id")


end sub

'*************************************************************

sub ControlloSezioni()
	set rsSezioni = Server.CreateObject("ADODB.Recordset")
	
	sSQL = "select count(id_areaiq) as TotSezioni from iq_area where id_infoiq = " & nIdQuest 
	rsSezioni.Open sSQL,CC
	Response.Write "query " & sSQL
	if not rsSezioni.EOF then
         nTotSez = clng(rsSezioni("TotSezioni"))  
         if  nTotSez = 1 then
			set rsAreaSez = Server.CreateObject("ADODB.Recordset")
	
			sSQLAreaSez = "select id_areaiq from iq_area where id_infoiq = " & nIdQuest 
			rsAreaSez.Open sSQLAreaSez,CC
			if not rsAreaSez.EOF then
				nIdArea = rsAreaSez("id_areaiq")
			end if	 
			rsAreaSez.Close
			set rsAreaSez = nothing 
         end if      
	else
	     nTotSez = 0
	end if	
				
    rsSezioni.Close
    set rsSezioni = nothing
end sub

'*************************************************************

sub ctrlCodiceFiscaleDuplicato()
prgEsitoPersona = "OK"
set rsCodFisc = Server.CreateObject("ADODB.Recordset")

													
sSQL = "SELECT ID_PERSONA FROM PERSONA WHERE COD_FISC ='" & sCodFisc & "'"
	
rsCodFisc.open sSQL, CC, 3
	
if  rsCodFisc.EOF then 
    rsCodFisc.Close 
	set rsCodFisc = nothing
    exit sub
else
	sIDPers = rsCodFisc("ID_PERSONA")
'	Response.Write "IDPERSONA=" & sIDPers & "<BR>"

	sPersona = sIDPers
	prgEsitoPersona = "KO"
	rsCodFisc.Close 
	set rsCodFisc = nothing    
end if

end sub


'******************************************************

sub InserimentoPersona()

sSQL = "INSERT INTO PERSONA " & _
				  "(COGNOME," &_
					"NOME," &_
					"DT_NASC," &_
					"COM_NASC," &_
					"PRV_NASC," &_
					"STAT_NASC," &_
					"SESSO," &_
					"POS_MIL," &_
					"COD_FISC," &_
					"STAT_CIV," &_
					"STAT_CIT," &_
					"IND_RES," &_
					"FRAZIONE_RES," &_
					"COM_RES," &_
					"PRV_RES," &_
					"CAP_RES," &_
					"NUM_TEL," &_
					"E_MAIL," &_
					"DT_TMST) VALUES " &_	
					"('" & sCognome  & _
					"','" & sNome & _
					"'," & sDataNascita & _
					",'" & sComuneNascita & _
					"','" & sProvNascita & _
					"','" & sNazNascita & _
					"','" & sSesso & _
					"','" & sPosLeva & _
					"','" & sCodFisc & _
					"','" & sStCiv & _
					"','" & sCittad & _
					"','" & sIndir & _
					"','" & sFraz & _
					"','" & sComune & _
					"','" & sProv & _
					"','" & sCap & _
					"','" & sTelef & _
					"','" & sEMail & _
					"'," & ConvDateToDb(Now()) & ")"

  Errore=EseguiNoC(sSQL,CC)	  
  if Errore <> "0" then
     MessaggioErrori("(Persona) " & Errore)
  end if
end sub

sub AcquisizioneIdPersona()
    'Accedo alla tabella PERSONA per prelevare Id_Persona e poter poi
    ' fare la INSERT nella tabella successiva

	set rsPers = Server.CreateObject("ADODB.Recordset")

	sSQL = "SELECT ID_PERSONA, cognome, nome FROM PERSONA WHERE COD_FISC='" & sCodFisc & "'"
			
	rsPers.Open sSQL,CC
'	Response.Write "query " & sSQL
	IF not rsPers.EOF then
         sPersona = clng(rsPers("ID_PERSONA"))
         sCognome = rsPers("cognome")
         sNome = rsPers("nome")
	else
	     sPersona = 0
	     MessaggioErrori("Identificativo della Persona non trovato")
	end if	
				
    rsPers.Close
    set rsPers = nothing
end sub

'''Inizio SM 29/03/2007'''''''''''

sub InserimentoDichiaraz()

sSQL = "INSERT INTO DICHIARAZ " &_
				"(ID_PERSONA," &_
				"AUT_TRAT_DATI," &_
				"DT_TMST) VALUES " &_
				"(" & sPersona & _
				",'S'" & _
				"," & ConvDateToDb(now) &")"
								
Errore=EseguiNoC(sSQL,CC)
if Errore <> "0" then
     MessaggioErrori("(Dicharaz Persona) " & Errore)
 end if
	
end sub

'''''Fine SM 29/03/2007''''''''''
sub CompilazioneQuestionario()

%>
<!--form method="post" name="FrmCompilaQuest" action='<%=Session("Progetto")%>/pgm/Formazione/QGradimento/QGR_VisArea.asp'-->
<form method="post" name="FrmCompilaQuest" action='/pgm/Formazione/QGradimento/QGR_VisArea.asp'>
	<input type="hidden" name="txtTipoDest" value="<%=sTipoDest%>">
	<input type="hidden" name="txtIdPers" value="<%=sPersona%>">
	<input type="hidden" name="txtIdUtente" value="<%=sIdUtente%>">
	<input type="hidden" name="txtCognome" value="<%=sCognome%>">
	<input type="hidden" name="txtNome" value="<%=sNome%>">
	<input type="hidden" name="txtCodFisc" value="<%=sCodFisc%>">
	<input type="hidden" name="id" value="<%=nIdQuest%>">
	<input type="hidden" name="mod" value="<%=3%>">
	<input type="hidden" name="txtIdProj" value="<%=nIdProj%>">
	<input type="hidden" name="txtDataNascita" value="<%=Request("txtDataNascita")%>">	
	<input type="hidden" name="cmbSesso" value="<%=sSesso%>">
	<input type="hidden" name="sezioni" value="<%=nTotSez%>">
	<input type="hidden" name="ida" value="<%=nIdArea%>">
</form>

<!-- 23/01/2007 se ho piu' sezioni passo per QGR_VisArea-->
<script language=javascript>
	CompilaQuestVisArea()	
</script>

<!--23/01/2007 fine-->
<table cellpadding="0" cellspacing="0" width="100%" border="0">
		<tr height="18">
			<!--td class="sfondocommQuest" height="18" width="45%" align="center">
			<span class="tbltext2"><b>&nbsp;Anagrafica</b> </span></td><td><img src="<%=Session("Progetto")%>/images/Frecciagrigia.gif" border ="0" height="18" width="50%"></td>
			<td class="sfondomenu" height="18" width="45%" align="center"><a class=tbltext0 href="javascript:document.FrmCompilaQuest.submit();" ONMOUSEOVER="window.status = ' '; return true">
			<b>COMPILAZIONE QUESTIONARIO</b></a></td><td><img src="<%=Session("Progetto")%>/images/Frecciablu.gif" border ="0" height="18"></TD-->
			<td nowrap height="18"><img src="<%=Session("Progetto")%>/images/tab_inactive_left.gif" border="0" height="18"></td>
			<td nowrap background="<%=Session("Progetto")%>/images/tab_inactive_center.gif" height="18" title="Anagrafica questionario"><span
			    class="home_testo_bianco_b">Anagrafica</span></td>
			<td nowrap width="5%" height="18"><img src="<%=Session("Progetto")%>/images/tab_inactive_right.gif" border="0" height="18"></td>
			<!--td nowrap height="18" width="1%"><img src="<%=Session("Progetto")%>/images/1x1.gif" border="0" height="18px" ></td-->
			<td nowrap height="18"><img src="<%=Session("Progetto")%>/images/tab_blu_left.gif" border="0" height="18" ></td>
			<td nowrap background="<%=Session("Progetto")%>/images/tab_blu_center.gif" height="18" title="Istruzioni per la compilazione"><a span class="home_testo_bianco_b" href="javascript:document.FrmCompilaQuest.submit();" ONMOUSEOVER="window.status = ' '; return true">Questionario</a></span></td>
			<td nowrap height="18" width="80%"><img src="<%=Session("Progetto")%>/images/tab_blu_right.gif" border="0" height="18"></td>
		</tr>
</table>
	<Table cellpadding="4" cellspacing="0" width="100%" border="0" style="border-collapse: collapse;">
		<Tr>
			<Td class="stroked">
			<!--td class="sfondocomm" width="57%" colspan="7">
				<a href="Javascript:Show_Help('/Pgm/help/GestProgetti/Alfabeta/Selezione/SEL_InsDomIscr')" onmouseover="javascript:window.status=' '; return true"><img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
			</td>
		</tr-->
				<table cellpadding="0" cellspacing="0" width="100%" border="0">
					<tr>
						<td> &nbsp;</td>
					</tr>		
				<% if flgIns = "S" then	%>
					<tr>
						<td class="tbltext" width="57%" colspan="7" size="15">
							&nbsp;&nbsp;&nbsp;I dati della persona sono stati inseriti.<br>
							&nbsp;&nbsp;&nbsp;Per accedere al questionario cliccare sulla scheda
							 <b><a href="javascript:document.FrmCompilaQuest.submit();" ONMOUSEOVER="window.status = ' '; return true">"Questionario"</a></b>			     
						</td>
					</tr>
				<% end if
				if flgIns = "N" then	%>
					<tr>
						<td class="tbltext" width="57%" colspan="7">
							&nbsp;&nbsp;&nbsp;I dati della persona sono gi� presenti.<br>
							&nbsp;&nbsp;&nbsp;Per accedere al questionario cliccare sulla scheda 
							<b><a href="javascript:document.FrmCompilaQuest.submit();" ONMOUSEOVER="window.status = ' '; return true">"Questionario"</a></b>			     
						</td>
					</tr>
				<% end if%>
					<tr>
						<td> &nbsp;</td>
					</tr>
				</table>
			 </Td>
		</Tr>
	</Table>
	<%
end sub 

sub VisualizzazioneMessaggioOperazioneEseguita()
%>
<br>
<table cellpadding="0" cellspacing="0" width="500" border="0">
		<tr height="18">
			<td class="sfondomenu" height="18" width="67%">
			<span class="tbltext0"><b>&nbsp;DOMANDA DI ISCRIZIONE</b></span></td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
			<td valign="middle" align="right" class="tbltext1" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
		</tr>
		<tr>
			<td class="sfondocomm" width="57%" colspan="3">
				Modulo di iscrizione 
			    per il bando&nbsp;<b><%=sDescBando%></b><br>  
			</td>
		</tr>
		<tr height="2">
			<td colspan="3" class="sfondocomm" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>
<br><br>

	<body bgcolor="#ffffff" onafterprint="javascript:buttom.style.visibility='visible';" onbeforeprint="javascript:buttom.style.visibility='hidden';" text="#000000">
		<form name="formstampa">
				<table width="500" border="0" cellspacing="2" cellpadding="1">
					<tr align="middle">
						<td class="tbltext">
						
								NOME = <b><%=sNome%></b><br>				
								COGNOME = <b><%=sCognome%></b><br>								
								CODICE FISCALE = <b><%=sCodFisc%></b><br>							
						
								USER = <b><%=sLogin%></b><br>
								PASSWORD = <b><%=sPassword%></b><br><br>
								La domanda di iscrizione � stata inserita correttamente<br>
								il <b><%= sDataIscri%></b><br><br>
								
								Prendi nota della tua login e password e accedi all'area a te riservata.<br><br>
				
								
								<%If session("progetto")="/SPORT2JOB" Then %>
								<a href="javascript:windowLoginOpenPRGGif('/SPORT2JOB')">
								<img src="/images/arearis.gif" border="0" WIDTH="70" HEIGHT="7"></a> 
							    <%else
							       'da definire l'utilizzo dell'area riservata per i vari progetti%>
							<!--	<a href="javascript:windowLoginOpenPRG('<%'=session("progetto")%>')">								<img src="/images/arearis.gif" border="0" WIDTH="70" HEIGHT="7"></a>  							-->
								<%End if%>
						</td>
					</tr>
				</table>
				<br>
			<label id="buttom">
				<a href="javascript:window.print()"><img name="stampa" id="stampa" src="<%=Session("Progetto")%>/images/stampadati.gif" border="0"></a>
				<br><br><br>
				<a href="javascript:document.location='<%=Session("Progetto")%>/home.asp'">
			<img src="<%=Session("Progetto")%>/images/indietro.gif" border="0">
		</a>
			</label>
		</form>
	</body>	
<br><br>	

<%
end sub


%>
<!--form method="post" name="frmDomIscri" onsubmit="return ControllaDati(this)" action="INT_CnfDatiUtente.asp"-->

<%

sub CorpoDelProgramma()

	ControlloSezioni()
	
	if sCodFisc <> "" then
		ctrlCodiceFiscaleDuplicato()
	end if
	
    if prgEsitoPersona = "KO" then  'Persona presente su tabella Persona 
		flgIns = "N"
		CompilazioneQuestionario()
		exit sub
    end if
    
    CC.beginTrans
    
    
    if prgEsitoPersona = "OK" then  'Persona assente su tabelle Persona           
       InserimentoPersona() 
		if Errore <> "0" then exit sub 'Inserimento nella tabella Persona Fallito
   
       AcquisizioneIdPersona()
       if sPersona = 0 then exit sub 'Identificativo della Persona non trovato      
    ''''Inizio SM 29/03/2007  
        InserimentoDichiaraz() 
		if Errore <> "0" then exit sub 'Inserimento nella tabella Dichiaraz Fallito
	''''Fine
		flgIns = "S"
        CompilazioneQuestionario()
    end if 
   
   CC.CommitTrans
  '  VisualizzazioneMessaggioOperazioneEseguita()
    
end sub
'*************************************************************

sub Fine()
<!--#include virtual = "/include/closeconn.asp"-->
<!--#include virtual = "/strutt_coda2.asp"-->
end sub

'*************************************************************
%>
<!-- ************** ASP Fine *************** -->
<!-- ************** MAIN Inizio ************ -->
<%
dim sUser
dim CnConn
dim sCognome, sNome
dim sDataNascita
dim sComuneNascita
dim sProvNascita
dim sNazNascita
dim sSesso
dim sPosLeva
dim sCodFisc
dim sStCiv
dim sCittad
dim sIndir
dim sFraz
dim sComune
dim sProv
dim sCap
dim sTelef
dim sEMail
dim sNumCentroImp
dim sDataIscri
dim sCategProt
dim sTitStud
dim sLivStud
dim sStato
dim sAnnoTit
dim sVoto, sBase, nConvVoto, sVotoInt
'dim sAttIscr
dim sGrado(6)
dim sProvincia
dim nBando
dim sAutorizz
dim sStatoDis
dim sLogin
dim sPassword
dim sSQL
dim sPersona
dim sGruppo
dim sIniIsc
dim sDataSis, sDataRicCatProt
dim sIDPers , sGraduatoria
dim rsCodFisc, rsDomanda, rsPers, rsLogin
dim Errore
dim prgEsitoPersona, sDescBando, nNumEnteOcc
dim sEsitoUtente, sDichiaraz, sEsitoDomandaIscrizione
dim sEsitoStatoOccupazionale,EsitoSO, EsitoTitoloStudio
dim EsitoLavSvantaggio, nTotChkLavSvant, sCodSvantaggio, valore, b,  sSede
dim sTipo
dim sAppartenenza, sDisocc, sDecorrenza
dim sTempoIndet, sScadenza
dim sTipoDest, sIdUtente, nIdProj, nIdQuest, flgIns , nTotSez, nIdArea

  Response.Buffer = True
  Response.ExpiresAbsolute = Now() - 1
  Response.Expires = 0
  Response.CacheControl = "no-cache"

 ' If Len(Session("FirstTimeToPage")) > 0 then
 '   'The user has come back to this page after having visited
 '   'it... wipe out the session variable and redirect them back
 '   'to the login page
 '   Session("FirstTimeToPage") = ""
 '   Response.Redirect "INT_VisFrontGradimento.asp"
 '   Response.End
 ' End If

'  'If we reach here, the user can view the page, create the form


sTipo ="P" 'Vengono trattate solo le persone fisiche (informazione per tabella UTENTE)

AcquisizioneParametri()
'sContrRes = controlloProvinciaRegione(nNumEnteOcc,sProv)

'if sContrRes = "OK" then
	CorpoDelProgramma()
'else
'    MessaggioErrori("Controllare la provincia - regione di residenza")
'end if
Fine()
%>
<!-- ************** MAIN Fine ************ -->