<%@LANGUAGE = JScript %>
<!--#INCLUDE file = "include/openConnJS.asp"-->
<%

//---------------------------------------------------------------------------------------------------------------------------------------------------------------

/*function stampa(idQuest,IdDest) {

var windowArea
if (windowArea != null ) 
	{
		windowArea.close(); 
	}

//	var IdQuestion;
//	var IdDestin;
	
//	IdQuestion = document.frmStampa.idq.value
//	IdDestin = document.frmStampa.IdDest.value
	document.frmStampa.submit
//	windowArea = window.open ('QGR_Stampa.asp?idQuestionario=' + idQuest + '&IdDestinatario=' + IdDest,'Info','width=700,height=550,Scrollbars=yes,status=no,Resize=no')
}
*/
//


// Imposta il numero di messaggi per pagina
//var quanti_per_pagina = 5;

function messaggio(id_elemento, tipo_elemento ,livello, testo_domanda, tipo_domanda, tipo_sel, id_domandaiq, tipo_risposta, testo_risposta, testo_max, testo_min, iq_min, iq_max, fl_tiprisp, risposta_utente, id_blocco)
{
	
//response.write ("<br>MESSAGGIO")
//	this.id = id;
	this.livello = livello;
	this.id_elemento = id_elemento;
	this.tipo_elemento = tipo_elemento;
	this.testo_domanda = testo_domanda;
	this.tipo_sel = tipo_sel;
	this.id_domandaiq = id_domandaiq;
	this.tipo_domanda = tipo_domanda;
	this.tipo_risposta = tipo_risposta;
	this.txt_risposta = testo_risposta;
	this.testo_max = testo_max;
	this.testo_min = testo_min;
	this.iq_min = iq_min;
	this.iq_max = iq_max;
	this.fl_tiprisp = fl_tiprisp;
	this.risposta_ute = risposta_utente;
	this.id_blocco = id_blocco;  

	//Response.Write ("<br>" + "il " + il + "<br> livello" + livello + "<br> id_elemento=" + id_elemento + "<br> tipo_elemento=" + tipo_elemento + "<br> testo_domanda=" + testo_domanda + "<br> tipo_sel=" + tipo_sel + "<br> id_domandaiq=" + id_domandaiq + "<br> tipo_domanda=" + tipo_domanda + "<br> tipo_risposta=" + tipo_risposta + "<br> testo_risposta=" + testo_risposta + "<br> risposta_uteNTE=" + risposta_utente + "<br> fl_tiprisp=" + fl_tiprisp + "<br>-------<br>");
	

}

//---------------------------------------------------------------------------------------------------------------------------------------------------------------

function leggiMessaggi(rt,liv) {
		
//response.write ("<br>LEGGIMESSAGGI")
	// Funzione ricorsiva di lettura
	sql =  " SELECT ID_INFOQUEST, ID_AREAIQ, ID_BLOCCO, TIPO_ELEMENTO, ID_ELEMENTO, ID_ELEM_ORIGINE, DT_TMST " 
	sql += " FROM STRUTTURA_QUEST "
	sql += " WHERE ID_AREAIQ = " + IdArea + " AND ID_ELEM_ORIGINE = " + rt + " AND ID_INFOQUEST = " + idquest 
	sql += " ORDER BY ID_BLOCCO, ID_ELEMENTO";
	
	//Response.Write(sql);
 
	i_ris[liv] = CC.Execute(sql);
  
 	var TESTO_DOMANDA;
	var TIPO_SEL;
 	var ID_DOMANDAIQ;
	var TIPO_RISPOSTA;
	var TIPO_DOMANDA;
	var TXT_RISPOSTA;
	var TESTO_MAX;
	var TESTO_MIN;
	var IQ_MIN;
	var IQ_MAX;
	var FL_TIPRISP;
	var ID_RISPOSTAIQ;
	var ID_BLOCCO;
 
  
	while (!i_ris[liv].EOF){

		//LETTURA DOMANDA
		if (String(i_ris[liv]("TIPO_ELEMENTO"))=="D"){
			
			sqlDom = "SELECT TESTO_DOMANDA, TIPO_DOMANDA, TIPO_SEL, ID_DOMANDAIQ "
			sqlDom += " FROM IQ_DOMANDA WHERE ID_DOMANDAIQ =" + parseInt(i_ris[liv]("ID_ELEMENTO"))
			//Response.Write(RRdom);
			RRdom = CC.Execute(sqlDom);
			
			ID_DOMANDAIQ = String(RRdom("ID_DOMANDAIQ"))
			TESTO_DOMANDA =	String(RRdom("TESTO_DOMANDA"))

		
			TIPO_DOMANDA = String(RRdom("TIPO_DOMANDA"))
			TIPO_SEL =	String(RRdom("TIPO_SEL"))
			
		}

		//LETTURA RISPOSTA
			RISPOSTA_UTENTE = ""
		
		if (String(i_ris[liv]("TIPO_ELEMENTO"))=="R"){
			
			sqlRisp = "SELECT ID_DOMANDAIQ, TIPO, TXT_RISPOSTA, TESTO_MAX, TESTO_MIN, IQ_MIN, IQ_MAX, FL_TIPRISP "
			sqlRisp += " From IQ_RISPOSTA where ID_RISPOSTAIQ = " + parseInt(i_ris[liv]("ID_ELEMENTO"))
		
			RRrisp = CC.Execute(sqlRisp);
		
			ID_DOMANDAIQ = String(RRrisp("ID_DOMANDAIQ"))
			TIPO_RISPOSTA = String(RRrisp("TIPO"))
			TXT_RISPOSTA = String(RRrisp("TXT_RISPOSTA"))
			TESTO_MAX = String(RRrisp("TESTO_MAX"))
			TESTO_MIN = String(RRrisp("TESTO_MIN"))
			IQ_MIN = String(RRrisp("IQ_MIN"))
			IQ_MAX = String(RRrisp("IQ_MAX"))
			FL_TIPRISP = String(RRrisp("FL_TIPRISP"))
			
		
			sqlDom1 = "SELECT TIPO_DOMANDA, TIPO_SEL "
			sqlDom1 += " FROM IQ_DOMANDA WHERE ID_DOMANDAIQ =" + ID_DOMANDAIQ
			RRdom1 = CC.Execute(sqlDom1);
			
			TIPO_DOMANDA = String(RRdom1("TIPO_DOMANDA"))
			TIPO_SEL =	String(RRdom1("TIPO_SEL"))
	
	//25/01/2007	inizio gestione questionari intermediati	
	//		sqlRisp1 = "SELECT RT.ID_RISULTATO, RES.ID_RISPOSTAIQ, RES.VALORE_RISPOSTA, RES.TESTO_RISPOSTA " 
	//		sqlRisp1 +=  " FROM IQ_RISULTATO RT , IQ_RISPOSTE RES WHERE  RT.IDUTENTE = " + Session("idutente") + " AND RT.ID_RISULTATO = RES.ID_RISULTATO AND RES.ID_RISPOSTAIQ =  " + parseInt(i_ris[liv]("ID_ELEMENTO"))
			
	//		response.write (" flgCompDiretta---- " + flgCompDiretta);
			
			if (flgCompDiretta == "N"){
			sqlRisp1 = "SELECT RT.ID_RISULTATO, RES.ID_RISPOSTAIQ, RES.VALORE_RISPOSTA, RES.TESTO_RISPOSTA FROM IQ_RISULTATO RT , IQ_RISPOSTE RES " 
			sqlRisp1 +=  " WHERE  RT.IDUTENTE = " + Session("idutente") + " AND RT.ID_RISULTATO = RES.ID_RISULTATO AND RES.ID_RISPOSTAIQ =  " + parseInt(i_ris[liv]("ID_ELEMENTO")) + " and cod_tipo_dest = '" + tipoDest + "' and id_destinatario = " + idDestinatario   
			}
			
			if (flgCompDiretta == "S"){
			sqlRisp1 = "SELECT RT.ID_RISULTATO, RES.ID_RISPOSTAIQ, RES.VALORE_RISPOSTA, RES.TESTO_RISPOSTA FROM IQ_RISULTATO RT , IQ_RISPOSTE RES " 
			sqlRisp1 +=  " WHERE  RT.IDUTENTE = " + Session("idutente") + " AND RT.ID_RISULTATO = RES.ID_RISULTATO AND RES.ID_RISPOSTAIQ =  " + parseInt(i_ris[liv]("ID_ELEMENTO")) + " and id_destinatario is null "
			}
	
			
	// 25/01/2007 fine		
			
			RRrisp1 = CC.Execute(sqlRisp1);
		
			//response.write (sqlRisp1);
		
			if (!RRrisp1.eof){ 		 
				
				if (RRrisp1("VALORE_RISPOSTA") > "")
				{
					RISPOSTA_UTENTE = parseInt(RRrisp1("VALORE_RISPOSTA"))
				}
				else
				{    
					if (RRrisp1("TESTO_RISPOSTA") > "")
					{
						RISPOSTA_UTENTE = RRrisp1("TESTO_RISPOSTA")
					}
					else
					{
						RISPOSTA_UTENTE = "1"
					}
				}	
			}
			//		response.write ("IND" +	il + " VALORE RISPOSTA-" + RRrisp1("VALORE_RISPOSTA") + "TESTO RISPOSTA " + RRrisp1("TESTO_RISPOSTA") + "risposta_ute " + RISPOSTA_UTENTE);	
		}		

		testuale[il] = RISPOSTA_UTENTE

		lista[il++] = new messaggio(parseInt(i_ris[liv]("ID_ELEMENTO")),
									String(i_ris[liv]("TIPO_ELEMENTO")),
									liv,
									TESTO_DOMANDA,
									TIPO_DOMANDA,
									TIPO_SEL,
									ID_DOMANDAIQ,
									TIPO_RISPOSTA,
									TXT_RISPOSTA,
									TESTO_MAX,
									TESTO_MIN,
									IQ_MIN,
									IQ_MAX,
									FL_TIPRISP,
									String(RISPOSTA_UTENTE),
									parseInt(i_ris[liv]("ID_BLOCCO"))
									);       
									
			                                           
		
		leggiMessaggi(parseInt(i_ris[liv]("ID_ELEMENTO")), liv + 1);
		
    	i_ris[liv].MoveNext();
    	
	}
	
}

//28/02/2007
//---------------------------------------- MAIN -----------------------------------------------------------------------------------------------------------------------

var il = 0;
var i = 0;
var conta = 0;
var last = 0;
var appo = "NO";
var RespName = " ";
var idquest
var IdArea
var ii

//25/01/2007 inizio 
var tipoDest 
var idPers 
var idSede 
var tipoDest = ""
var idPers = ""
var idSede = ""
var flgCompDiretta 
//var idSedeLegale
var idDestinatario

	var RISPOSTA_UTENTE


var tipoDest = request("txtTipoDest")
var idPers = request("txtIdPers")
var idSede = request("txtIdSede")
var flgCompDiretta = request("flgCompDir")
//var idSedeLegale = request("txtIdSedeLegale")

var lista = new Array();
var i_ris = new Array();
var testuale = new Array();

if (flgCompDiretta == "N"){
	if (idSede != ""){
		idDestinatario = idSede
	}
	if (idPers != ""){
		idDestinatario = idPers
	}
}
if (flgCompDiretta == "S"){
	idDestinatario = ""
}	
//25/01/2007 fine

// ID Questionario
var idquest = Request("idq");

// Id dell'Area
var IdArea = Request("ida");
// Porta i messaggi interessati dal database all'Array lista


leggiMessaggi(0, 0);

//  SM request per verifica se Mod=4 quindi in modalità Anteprima
var Mod = Request("mod");
//  SM   fine

if ( !i_ris[0].EOF) {
		i_ris[0].MoveNext();
		if (!i_ris[0].EOF)	appo="SI"; 
	}
%>

<script language="JavaScript">
<!--#include Virtual = "/Include/help.inc"-->


</script>


<%

	var descarea = 1
	var appoArea
	var numaree
	
	//sSQL = "select count(*) as totaree from iq_area where id_infoiq = " + idquest + " order by id_areaiq"
	sSQL = "select count(*) as totaree from iq_area where id_infoiq = " + idquest + " "
	var rsArea = server.CreateObject ("adodb.recordset")
	rsArea.Open(sSQL, CC); 
	appoArea = rsArea("totaree")
//response.write (appoArea)
	if(appoArea > 1 )
	{
		numaree = 2
	}
	
	sqlArea = "SELECT DESC_AREAIQ, ID_AREAIQ FROM IQ_AREA WHERE ID_AREAIQ =" + IdArea 
	RRArea  = Server.createObject("adodb.recordset");
	RRArea.Open(sqlArea, CC);
	
	if (!RRArea.eof){ 		 
			RespName = RRArea.Fields("DESC_AREAIQ")
	}		
//23/04/2007 aggiunta l'estrazione del campo intest_quest
	sqlQuest = "SELECT DESC_QUEST,intest_quest FROM INFO_QUEST WHERE ID_INFOQUEST =" + idquest 
	RRQuest  = Server.createObject("adodb.recordset");
	RRQuest.Open(sqlQuest, CC);
	
	if (!RRQuest.eof){ 		 
			RespQuest = RRQuest.Fields("DESC_QUEST")
			IntestQuest = RRQuest.Fields("intest_quest")
	}		
	%> 
	<!--Epili 13/01/02 Per poter utilizzare le strutture standard.-->


	<script language="VBScript" runat="server"> 
		Server.Execute ("/strutt_testa1.asp") 
	</script>
	<br>
<!--	<p align="left"><span class="tbltext"><b>&nbsp;<%=Request.Form("frui")%></b></span><br> -->

<!-- 08/05/2007 -- inizio -->
	<!--p align="left"><span class="tbltext"><b>&nbsp;Questionario</b> <%=Server.HTMLEncode(RespQuest)%> :  <%=IntestQuest%></span><br-->

<!--
<table width="100%" border="0">
	<tr><td width="30%" class="tbldett">&nbsp;
		<img src="<%=Session("progetto")%>/images/logo_ites.gif"></td>
		<td  width="50%" class="tbltext" align="center">&nbsp;
		<b>Occupazione e sviluppo della <br> Comunità degli Italiani all'estero</b></td>
		<td width="20%"  class="tbldett">&nbsp;
		<img src="<%=Session("progetto")%>/images/logo_ministero.gif"></td>
	</tr>
</table>-->
<table width="100%" >
	<tr>
		<td width="100%" align="center" class="tbltext3"><%=IntestQuest%></td>
	</tr>
</table>

<!-- 08/05/2007 -- fine -->
		
<!--<table width="100%" border="0">
	<tr>
		<td width="100%" align="left" class="tbldett"></td>
	</tr>
</table>-->	
	<table width="100%" border="0">
		<!--<tr>
			<td width="100%" align="left" class="tbltext1">
			<b>Programma : <%=Request.Form ("progetto")%></b>
			</td>
		</tr>

		<tr>
			<td width="100%" align="left" class="tbltext1">
			<b>Codice commessa del progetto : <%=Request.Form("commessa")%></b>
			</td>
		</tr>
		<tr>
			<td width="100%" align="left" class="tbltext1">
			<b>Erogazione : <%=Request.Form("ero")%></b>
			</td>
		</tr>
		<!--tr>			<td width="100%" align="left" class="tbltext1">			<b>Fruizione : <%=Request("frui")%></b>			</td>		</tr-->
	<%//	if ( Request("intervento")!="" ){ %>
		<!--<tr>
			<td width="100%" align="left" class="tbltext1">
			<b>Titolo intervento formativo : <%//=Request.Form("intervento")%></b>
			</td>
		</tr>-->
	<%//	}	%>	
	</table>

<%	if (numaree==2){ %>
	<span class="tbltext"><b>&nbsp;Sezione:</b> <%=Server.HTMLEncode(RespName)%></span>
<%
		}
%>	</p>	

	<%
	
	var sm
	var tpr
	//sm = Session("mask")
	sm = "05"
//01/03/2007
var NumDomVideo = 0

    if (lista.length==0){	
       Response.Write(" <br><br><span class=textred><b>NON SONO PRESENTI DOMANDE</b></span><br><br><br><br> ");
    }else{	
		for (i=0;i<lista.length;i++) {
			 tpr = lista[i].id_domandaiq 
			 Response.Write("<div align=left id='div" + tpr + "' name='div" + tpr + "' style='visibility:visible'><table border=0 width=100% ><tr class=tblsfondo>")
		     Response.Write("<td  width=5% height=1 class=textblack colspan=2>");
		    
			     for (j=0;j<lista[i].livello;j++)
		     
					Response.Write("<img src='" + Session("Progetto") + "/images/blank_trasp.gif' width=20 height=16>")
					//response.write("j " + j + " - lista[i].livello " + lista[i].livello)

					if (lista[i].tipo_elemento=="D"){
					//anto 01/03/2007
					 NumDomVideo = NumDomVideo + 1	 
					//		
						Response.Write("</TD><TD class=textblack>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src='" + Session("Progetto") + "/images/re.gif' >")						   %> 
						    <!--anto 01/03/2007 inizio-->
						    <!--b><%=lista[i].testo_domanda%></b-->
						     <b><%=NumDomVideo%>&nbsp;&nbsp;<%=lista[i].testo_domanda%></b>
						   <!-- 01/03/2007 fine-->
						   <!-- - <%=lista[i].tipo_elemento%> - <%=lista[i].id_elemento%> - <%=lista[i].tipo_sel%> - <%=lista[i].tipo_domanda%>-->
						   <%
   				
 					}else{

				 		Response.Write("</TD><TD class=textblack><img src='" + Session("Progetto") + "/images/blank_trasp.gif' width=30 height=16>")	

	// raoul Response.Write ("<br>" + i + "<br> id_elemento=" + lista[i].id_elemento + "<br> tipo_elemento=" + lista[i].tipo_elemento + "<br> testo_domanda=" + lista[i].testo_domanda + "<br> tipo_sel=" + lista[i].tipo_sel + "<br> id_domandaiq=" + lista[i].id_domandaiq + "<br> tipo_domanda=" + lista[i].tipo_domanda + "<br> tipo_risposta=" + lista[i].tipo_risposta + "<br> testo_risposta=" + lista[i].testo_risposta + "<br> risposta_ute=" + lista[i].risposta_ute + "<br> txt_risposta=" + lista[i].txt_risposta + "<br>-------<br>");



						%>
						<!--#INCLUDE file = "QGR_inc_antrisp.asp"-->
						<%						

						
					}
					Response.Write("</td></tr></table></div>")
					
		}
	} 
	

  %>

<script language="VBScript" runat="server"> 
	Server.Execute ("/Include/ckProfile.asp") 
</script>

<!------SM form per ritornare alla QGR_VisQuestionario-->

<form name="frmIndietro" method="post" action="QGR_VisFrontGradimento.asp">	
</form>	

<!-----fine--->
<% 

if (sm != "01" ){ 
	%>
<br><br>	
  <table width="730px" cellspacing="0" cellpadding="0" border="0">
	<tr>
		<td align="center">
			<b class="textblack">
			<%
			var nPag
			if (IdArea == 0) 
			{
				nPag = 2
			}
			else
			{
				nPag = 1
			}
			
			//------SM controllo che in modalità anteprima ritorni alla QGR_VisFrontGradimento
			if (Mod == 4) 
			// 08/03/2007 inizio - solo se la sezione non e' unica torno a QGR_VisFrontGradimento			
				{ 
				if (appoArea == 1)
			// 08/03/2007 fine		
				{%>
					<a HREF="javascript:frmIndietro.submit()">			
						<img src="<%=Session("progetto")%>/images/indietro.gif" border="0" alt="Torna alla pagina precedente">
					</a>
				<%}
				// 08/03/2007 inizio 
				if (appoArea > 1)
				{%>
				<a onmouseover="javascript:window.status='' ; return true" href="javascript:history.go(-1)">			
					<img src="<%=Session("progetto")%>/images/indietro.gif" border="0" alt="Torna alla pagina precedente">
				</a>
				<%}
				}
				// 08/03/2007 fine
			else
			{%>
				<a onmouseover="javascript:window.status='' ; return true" href="javascript:history.go(-<%=nPag%>)">			
					<img src="<%=Session("progetto")%>/images/indietro.gif" border="0" alt="Torna alla pagina precedente">
				</a>
			<%}
			//---------------fine%>
			<!--27/02/2007 aggiunta funzione stampa-->
			<!--28/2/2007-->
				<!--a HREF="javascript:window.print()">&nbsp;&nbsp;&nbsp;<img src="<%=Session("Progetto")%>/images/stampa.gif" border="0" alt="STAMPA"></a-->
			
<!--			<a HREF="javascript:stampa('<%=idquest%>','<%=idDestinatario%>')" onmouseover="javascript:window.status='';return true" target="new">&nbsp;&nbsp;&nbsp;<img src="<%=Session("Progetto")%>/images/stampa.gif" border="0" alt="STAMPA"></a>  -->
			<!--a class="textRed" href="javascript:history.back()" onmouseover="window.status =' '; return true">Pagina Precedente</a-->
			</b>
		</td>
	</tr>
</table>
	<%
} 			
%>	

<br><br>
<table border="0" width="750" height="8" cellpadding="0" cellspacing="0"> 
<tr height="1">
	<td width="100%" background="/DEMOGP/images/separazione.gif"></td>
</tr>
</table>

<br>


<form id="frmReload" name="frmReload" action="QGR_VisAnteprima.asp" method="post">
	<input type="hidden" name="txtTipoRisposta" id="txtTipoRisposta" value>
	<input type="hidden" name="ida" id="ida" value="<%=IdArea%>">
	<input type="hidden" name="idq" id="idq" value="<%=idquest%>">	
</form> 
   
<%    
// Chiude la connessione al database
CC.Close();
%>

<!--Epili 13/01/02 Per poter utilizzare le strutture standard.-->


