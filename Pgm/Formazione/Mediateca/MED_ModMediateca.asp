<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%
Sub ControlliJavaScript()
%>
<script language="Javascript">
<!--#include virtual="/include/ControlString.inc"-->
function ValidaMediateca(frmMedia)
	{
	//Controllo dei campi obbligatori
	if (frmMedia.txtDescrizione.value == "")
		{
		frmMedia.txtDescrizione.focus();
		alert("Il campo 'Descrizione' � obbligatorio.");
		return (false);
	  	}
		
	if (frmMedia.cboFlAct.value == "")
		{
		frmMedia.cboFlAct.focus();
		alert("Il campo 'Azione' � obbligatorio.");
		return (false);
	  	}
	//Controllo che il PATH o il LINK sia inserito
	if ((frmMedia.txtPathMatDid.value == "") && (frmMedia.txtLinkMatDid.value == ""))
		{
		frmMedia.txtPathMatDid.focus();
		alert("Inserire il PATH o il LINK.");
		return (false);
	  	}

	if ((frmMedia.txtPathMatDid.value != "") && (frmMedia.txtLinkMatDid.value != ""))
		{
		frmMedia.txtPathMatDid.focus();
		alert("Inserire il PATH o il LINK.");
		return (false);
	  	}
	// Controllo che se � stato valorizzato il LINK il Flag non sia impostato
	if ((frmMedia.txtLinkMatDid.value != "") && (frmMedia.cboFlAct.value != "0"))
		{
		frmMedia.cboFlAct.focus();
		alert("Non � possibile selezionare un'azione se � stato indicato il LINK");
		return (false);
		}
	if ((frmMedia.txtPathMatDid.value != "") && (frmMedia.cboFlAct.value == "0"))
		{
		frmMedia.cboFlAct.focus();
		alert("Selezionare un'azione");
		return (false);
	  	}
	return (true);
	}
	function LinkMatDid()
	{
		if (frmInsMediateca.txtLinkMatDid.value != "")
		{
		f = 'http://'+frmInsMediateca.txtLinkMatDid.value
		MyNewWindow=window.open(f, "Finestra"); 
		}
		else
		{
			frmInsMediateca.txtLinkMatDid.focus();
			alert("Inserire il LINK da testare.");
		}
	}
	
	var finestra
	function PathMatDid(sImg)
	{
		if (finestra != null ) 
		{
			finestra.close(); 
		}
		f='MED_InsPathMatDid.asp?img='+sImg
		finestra=window.open(f,"","width=550,height=450,location=no,menubar=no,scrollbars=yes,resizable=no")
	}

</script>
<%
End Sub
'-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
Sub Inizio()
	dim sFunzione
	dim sTitolo
	dim sCommento
	dim bCampiObbl
	
	sFunzione = "GESTIONE CORSI"
	sTitolo = "MEDIATECA"
	sCommento = "Digitare le informazioni nei campi sottostanti e premere <B>invia</B> " &_
		"per memorizzare le informazioni. Per eliminare il materiale premere <B>elimina</B>"
	bCampiObbl = true
	sHelp = "/Pgm/help/Formazione/Mediateca/MED_ModMediateca/"	
%>
	<!--#include virtual="/include/SetTestata.asp"-->
<%
End Sub
';););););););););););););););););););););););););););););););););););););););)
Sub ImpostaPag()
dim nIdCorso, nIdElem
dim nIdMedia
dim sSQL, rsMatDid
		
	nIdCorso = Request("idC")
	nIdElem = Request("idE")	
	nIdMedia = Request("idM")
	sTitolo = Request("Desc")
	
	sSQL =	"Select DESC_MATERIALEDID, PATH_MATERIALEDID, LINK_MATERIALEDID, FL_ACTMATERIALEDID, DT_TMST " &_
			"From MATERIALEDIDATTICO " &_
			"Where ID_MATERIALEDID = " & nIdMedia
	
	Set rsMatDid = Server.CreateObject("ADODB.Recordset")	
	'Response.Write sSQL
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	rsMatDid.Open sSQL, CC, 3
	if rsMatDid.EOF then
		PagErrore()
	else
%>
<form METHOD="POST" onsubmit="return ValidaMediateca(this)" name="frmInsMediateca" action="MED_CnfMediateca.asp">

	<input type="hidden" name="hIdCorso" id="hIdCorso" value="<%=nIdCorso%>">
	<input type="hidden" name="hIdElemento" id="hIdElemento" value="<%=nIdElem%>">
	<input type="hidden" name="hIdMedia" id="hIdMedia" value="<%=nIdMedia%>">
	<input type="hidden" name="dDtTmst" id="dDtTmst" value="<%=rsMatDid("DT_TMST")%>">
	<input type="hidden" name="hOper" id="hOper" value="MOD">
	<input type="hidden" name="Desc" value="<%=sTitolo%>">

	<table border="0" width="500" cellpadding="0" cellspacing="0">
		<tr>
			<td width="100" class="tbltext1Fad"><b>Titolo</b></td>
			<td class="textBlack"><b><%=sTitolo%></b></td>
		</tr>
	</table>
	<br>

	<table border="0" cellspacing="2" cellpadding="1" width="500">
		<tr> 
			<td align="left" nowrap class="tbltext1FAD">
				<b>Descrizione</b>*
			</td>
			<td>
				<span class="tbltext1FAD">&nbsp;- Utilizzabili <label name="lblNumCar1" id="lblNumCar1">512</label> caratteri -</span><br>
				<textarea name="txtDescrizione" class="textblackFAD" cols="50" rows="3" OnKeyUp="JavaScript:CheckLenTextArea(txtDescrizione,lblNumCar1,512)"><%=rsMatDid("Desc_MaterialeDid")%></textarea>
			</td>
		</tr>
		<tr> 
			<td align="left" nowrap class="tbltext1FAD">
				<b>Path</b>
			</td>
			<td>
				<input type="text" name="txtPathMatDid" id="txtPathMatDid" class="textblackaFAD" size="50" maxlength="512" value="<%=rsMatDid("Path_MaterialeDid")%>" readonly>
   				<a href="javascript:PathMatDid('<%=rsMatDid("Path_MaterialeDid")%>')">
   					<img src="<%=Session("progetto")%>/images/bullet1.gif" border="0" alt="Selezionare il PATH del materiale didattico" onmouseover="javascript:window.status='' ; return true">
   				</a>
			</td>
		</tr>
		<tr> 
			<td align="left" nowrap class="tbltext1FAD">
				<b>Link</b>
			</td>
			<td>
				<input type="text" name="txtLinkMatDid" id="txtLinkMatDid" class="textblackaFAD" size="50" maxlength="512" value="<%=rsMatDid("Link_MaterialeDid")%>">
   				<a href="javascript:LinkMatDid()">
   					<img src="<%=Session("progetto")%>/images/testlink.gif" border="0" alt="Selezionare il LINK del materiale didattico" onmouseover="javascript:window.status='' ; return true">
   				</a>
			</td>
		</tr>
		<tr> 
			<td align="left" nowrap class="tbltext1FAD">
				<b>Azione</b>
			</td>
			<td>
				<select name="cboFlAct" id="cboFlAct" class="textblackaFAD">
					<%
					select case rsMatDid("Fl_ActMaterialeDid")
						case "0"
					%>
						<option value="0" selected></option>
						<option value="1">Consulta</option>
						<option value="2">Preleva</option>
						<option value="4">Consulta e Preleva</option>
					<%						
						case "1"
					%>
						<option value="0"></option>
						<option value="1" selected>Consulta</option>
						<option value="2">Preleva</option>
						<option value="4">Consulta e Preleva</option>
					<%
						case "2"
					%>					
						<option value="0"></option>
						<option value="1">Consulta</option>
						<option value="2" selected>Preleva</option>
						<option value="4">Consulta e Preleva</option>
					<%
						case "4"
					%>					
						<option value="0"></option>
						<option value="1">Consulta</option>
						<option value="2">Preleva</option>
						<option value="4" selected>Consulta e Preleva</option>
					<%
					end select
					%>
				</select>
			</td>
		</tr>
	</table>
	<br>
	<table border="0" width="500" cellpadding="0" cellspacing="0">
		<tr>
			<td align="right" width="231"> 
				<%
					PlsIndietro()
				%>
			</td>
			<td align="left" width="30"> 
				<%
					PlsInvia("InviaM")
				%>
			</td>
			<td width="231">
				<% 
					PlsElimina("javascript:elimina.submit()")
				%>
			</td>
		</tr>
	</table>
	<br>
</form>
<form name="elimina" method="post" action="MED_CnfMediateca.asp">
	<input type="hidden" name="hIdCorso" id="hIdCorso" value="<%=nIdCorso%>">
	<input type="hidden" name="hIdElemento" id="hIdElemento" value="<%=nIdElem%>">
	<input type="hidden" name="hIdMedia" id="hIdMedia" value="<%=nIdMedia%>">
	<input type="hidden" name="dDtTmst" id="dDtTmst" value="<%=rsMatDid("DT_TMST")%>">
	<input type="hidden" name="hOper" id="hOper" value="Can">
	<input type="hidden" name="Desc" value="<%=sTitolo%>">

</form>
<%
	End if
	rsMatDid.Close
	set rsMatDid = nothing
End Sub
';););););););););););););););););););););););););););););););););););););););)
'M A I N
%>
<!--#include virtual="/strutt_testa2.asp"-->
<!--#include virtual="/include/openconn.asp"-->
<!--#include virtual="/include/SetPulsanti.asp"-->
<%	
ControlliJavaScript()
Inizio()
ImpostaPag()
%>
<!--#include virtual ="/include/closeconn.asp"-->
<!--#include virtual="/strutt_coda2.asp"-->
