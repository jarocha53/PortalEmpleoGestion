<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%
Sub Inizio()
	dim sFunzione
	dim sTitolo
	dim sCommento
	dim bCampiObbl
	
	sFunzione = "GESTIONE CORSI"
'	Select case sOper 
'	case "INS"
'		sTitolo = "INSERIMENTO"
'		sCommento = "Commento all'inserimento della<br>mediateca"
'	case "MOD"	
'		sTitolo = "MODIFICA"
'		sCommento = "Commento alla modifica della<br>mediateca"
'	case else
'		sTitolo = "CANCELLAZIONE"
'		sCommento = "Commento alla cancellazione della<br>mediateca"
'	end select

'	sTitolo = sTitolo & " MEDIATECA"
	sTitolo = "MEDIATECA"
	bCampiObbl = false
%>
	<!--#include virtual="include/SetTestata.asp"-->
<%
End Sub
'--------------------------------------------------------------
Function Inserisci()
	dim sSQL, sSQL0, sSQL1, sSQL2, sSQL3, sErrore
	dim sDescr, sPath, sLink, nAzione
	Dim nIdCorso, nIdElem
	
	nIdCorso = Request.Form("hIdCorso")
	nIdElem =  Request.Form("hIdElemento")
	if nIdElem > "" then
		nIdE = nIdElem 
		nIdC = ""
	else
		nIdE = ""
		nIdC = nIdCorso		
	end if
	sDescr = Replace(Request.Form("txtDescrizione"), "'", "''")
	sPath = Replace(Request.Form("txtPathMatDid"), "'", "''")
	sLink = Replace(Request.Form("txtLinkMatDid"), "'", "''")
	nAzione = Request.Form("cboFlAct")
	dDtTmst = ConvDateToDB(Now())
	
	sSQL0 = "INSERT INTO MATERIALEDIDATTICO ("
	sSQL1 = "DESC_MATERIALEDID, FL_ACTMATERIALEDID, DT_TMST, "
	sSQL2 = ") VALUES ("
	sSQL3 = "'" & sDescr & "', " & nAzione & ", " & dDtTmst & ", "
		
	if nIdC > "" then
		sSQL1 = sSQL1 & "ID_CORSO, "
		sSQL3 = sSQL3 & nIdCorso & ", "
	end if
	if nIdE > "" then
		sSQL1 = sSQL1 & "ID_ELEMENTO, "
		sSQL3 = sSQL3 & nIdElem & ", "
	end if
	if sPath > "" then
		sSQL1 = sSQL1 & "PATH_MATERIALEDID, "
		sSQL3 = sSQL3 & "'" & sPath & "', "
	end if
	if sLink > "" then
		sSQL1 = sSQL1 & "LINK_MATERIALEDID, "
		sSQL3 = sSQL3 & "'" & sLink & "', "
	end if
	
	sSQL1 = mid(sSQL1,1,len(sSQL1)-2)
	sSQL3 = mid(sSQL3,1,len(sSQL3)-2)
	sSQL = sSQL0 & sSQL1 & sSQL2 & sSQL3 & ")"

	'Response.Write sSQL

	sErrore = EseguiNoC(sSQL ,CC)

	Inserisci = sErrore
End Function
'---------------------------------------------------------------------------------------------
Function Modifica()

	dim sSQL, sErrore
	dim nIdCorso, nIdElem, nIdMedia, dDtTmst, dNTmst
	dim sDescr, sPath, sLink, nAzione
	
	nIdMedia =  Request.Form("hIdMedia")
	nIdCorso = Request.Form("hIdCorso")
	nIdElem =  Request.Form("hIdElemento")
	sDescr = Replace(Request.Form("txtDescrizione"), "'", "''")
	sPath = Replace(Request.Form("txtPathMatDid"), "'", "''")
	sLink = Replace(Request.Form("txtLinkMatDid"), "'", "''")
	nAzione = Request.Form("cboFlAct")
	dDtTmst = Request.Form("hDtTmst")
	
	sSQL = "UPDATE MATERIALEDIDATTICO SET " &_
			"DESC_MATERIALEDID = '" & sDescr  & "', " &_
			"FL_ACTMATERIALEDID = " & nAzione & ", "

	if nIdElem > "" then
		nIdE = nIdElem 
		nIdC = ""
	else
		nIdE = ""
		nIdC = nIdCorso		
	end if

	if nIdC > "" then
		sSQL = sSQL & "ID_CORSO = " & nIdCorso & ", "
	end if
	if nIdE > "" then
		sSQL = sSQL & "ID_ELEMENTO = " & nIdElem & ", "
	end if
	'if sPath > "" then
		sSQL = sSQL & "PATH_MATERIALEDID = '" & sPath & "', "
	'end if
	'if sLink > "" then
		sSQL = sSQL & "LINK_MATERIALEDID = '" & sLink & "', "
	'end if
			

	sSQL = sSQL & "DT_TMST = " & convdatetodb(Now) & " WHERE ID_MATERIALEDID = " & nIdMedia
				
		'Response.Write sSQL 
	sErrore=Esegui(nIdMedia, "MATERIALEDIDATTICO", Session("idutente"), "MOD", sSQL, 1, dDtTmst)
	Modifica = sErrore
	
End Function
'--------------------------------------------------------------------------
Function Cancella()
	Dim sErrore, dtTmst
	Dim sql
	dim nIE, nIC

	nIdMedia =  Request.Form("hIdMedia")
	nIdCorso = Request.Form("hIdCorso")
	nIdElem =  Request.Form("hIdElemento")
	dtTmst = Request("dDtTmst")	

	sql="DELETE FROM MATERIALEDIDATTICO WHERE ID_MATERIALEDID = " & nIdMedia 
	sErrore = Esegui(nIdElem,"MATERIALEDIDATTICO",Session("idutente"),"CAN",sql,1,dtTmst)
	Cancella = sErrore 

End Function
'-----------------------------------------------------------------------------
'M A I N
%>
<!--#include virtual="/strutt_testa2.asp"-->
<!--#include virtual="/include/openconn.asp"-->
<!--#include virtual="/Util/DBUtil.asp"-->
<!--#include virtual="/Include/SysFunction.asp"-->
<!--#include virtual="/Include/SetPulsanti.asp"-->
<%
	Dim strConn, sOper 
	Dim sMessOk, sMessKo
	Dim sErr
	
	sOper = Request("hOper")	
	sTitolo = Request("Desc")	
	sErr = "mmm"
	'Inizio()
	Select case sOper
		case "INS"
			sMessOk = "Inserimento correttamente effettuato"
			sMessKo = "Errore nell'inserimento"
			sErr = Inserisci()
		case "MOD"
			sMessOk = "Modifica correttamente effettuata"
			sMessKo = "Errore nella modifica"
			sErr = Modifica()
		case "Can"
			sMessOk = "Cancellazione correttamente effettuata"
			sMessKo = "Errore nella cancellazione"
			sErr = Cancella()
		case else
%>		
		<table border=0 cellspacing=2 cellpadding=1 width="500">
			<tr align=center>
				<td class="tbltext3">
					Errore nel Passaggio dei parametri. (<%=sOper%>)
				</td>
			<tr>
			<tr align=center>
				<td class="tbltext3">
				<%
					PlsIndietro()
				%>
				</td>
			</tr>
		</table>
<%	
	End select

	if sErr <> "mmm" then
		if sErr = "0" then
%>				
	<br><br>
	<table border=0 cellspacing=2 cellpadding=1 width="500">
		<tr align=center>
			<td class="tbltext3">
				<%=sMessOk%>
			</td>
		<tr>
	</table>
	<br><br>
	<table border=0 cellspacing=2 cellpadding=1 width="500">
		<tr align=center>
				<td>
					<%
					if Request("hIdCorso") <> "" then
						PlsLinkRosso "MED_VisMediateca.asp?idC=" & Request("hIdCorso") &_
							 "||" & sTitolo, "Elenco materiali"
					else
						PlsLinkRosso "MED_VisMediateca.asp?idE=" &_
							 Request("hIdElemento") & "||" & sTitolo, "Elenco materiali"
					end if
					%>
				</td>
		</tr>
	</table>
	<br><br>
<%		
		else
%>
	<br><br>
	<table border=0 cellspacing=2 cellpadding=1 width="500">
		<tr align=center>
			<td class="tbltext3">
				<%=sMessKo%><br><%=sErr%>
			</td>
		<tr>
	</table>		
	<br><br>
	<table border=0 cellspacing=2 cellpadding=1 width="500">
		<tr align=center>
			<td>
				<%
				PlsIndietro()
				%>
			</td>
		</tr>
	</table>
	<br><br>
<%
		end if
	end if
%>
<!--#include virtual="/include/closeconn.asp"-->
<!--#include virtual="/strutt_coda2.asp"-->
