<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include virtual="/strutt_testa2.asp"-->
<!--#include virtual="/include/openconn.asp"-->

<script language="Javascript">

function Carica_Pagina(npagina)
{
	document.frmPagina.pagina.value = npagina;
	document.frmPagina.submit();
}


	
function EsportaQ (num) {
	var sIdInfo = "";
	var i = 1;
	for (i = 1; i <= num; i++) {

		if (eval("chkEsporta" + i + ".checked") == true){
			if (sIdInfo == ""){
				sIdInfo = eval("chkEsporta" + i + ".value");
			}else{
				sIdInfo = sIdInfo + "$" + eval("chkEsporta" + i + ".value");
			}
		}
		
	}
	if (sIdInfo == ""){
		alert("Selezionare almeno un questionario da esportare.");
	}else{
//		alert(sIdInfo);	
		frmEsporta.idInfoQ.value = sIdInfo;
		frmEsporta.submit();
		}
	
}

</script>

<%

function controllaUtilizzo(nIdQuest)
	dim sSQL
	
	sSQL = "SELECT COUNT(*) as conta FROM IQ_RISULTATO WHERE ID_INFOQUEST = " & nIdQuest
	
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	set conta = CC.Execute(sSQL)
	
	if ( cint(conta("conta")) > 0 ) then
		controllaUtilizzo = true
	else
		controllaUtilizzo = false
	end if
	
end function

Sub Inizio()
	dim sFunzione
	dim sTitolo
	dim sCommento
	dim bCampiObbl
	
	sFunzione = "GESTIONE QUESTIONARI DI GRADIMENTO"
	sTitolo = "ELENCO QUESTIONARI DI GRADIMENTO"
	sCommento = "In questa pagina sono presentati tutti i questionari di gradimento presenti in archivio. Per visionarne la composizione, clicca sulla relativa icona di anteprima.<BR>Se ne vuoi modificare la composizione, clicca sulla immagine associata per verificare le sezioni costituenti. <BR>Per inserirne uno nuovo, clicca sul link <B>Inserisci nuovo questionario</B>. <BR>Per rendere pubblico un questionario, selezionalo e poi clicca sul link <B>Esporta i questionari selezionati </B>.   "
	bCampiObbl = false
	sHelp = "/Pgm/help/Formazione/QGradimento/QGR_VisQuestionario/"	
	
%>
	<!--#include virtual="include/SetTestata.asp"-->
<%
End Sub
'----------------------------------------------------------------------
Sub ElencoQ()

dim sSQL
dim rsQuest
dim nProg
dim verifica
dim enable

nProg = 0

sSql = "Select id_infoquest, Desc_quest,Intest_Quest " &_
	"from info_quest WHERE TIPO_QUEST <> 'T' OR TIPO_QUEST IS NULL order by id_infoquest"

'set rsQuest = CC.execute(sSQL)
' Paginazione 18/02/2004 EPILI
Record=0
nTamPagina=15
If Request.Form("pagina")= "" Then
	nActPagina=1
Else
	nActPagina=Clng(Request.Form("pagina"))
End If

Set rsQuest = Server.CreateObject("ADODB.Recordset")
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
rsQuest.Open sSQL, CC, 3


if not rsQuest.eof then

	rsQuest.PageSize = nTamPagina
	rsQuest.CacheSize = nTamPagina	

	nTotPagina = rsQuest.PageCount		
	If nActPagina < 1 Then
		nActPagina = 1
	End If
	If nActPagina > nTotPagina Then
		nActPagina = nTotPagina
	End If

	rsQuest.AbsolutePage=nActPagina
	nTotRecord=0		    


	%>
	<table border="0" cellspacing="1" cellpadding="2" width="500">
		<tr class="sfondocomm">
	        <td width="20"><b>Esporta</b></td>
	        <td width="20" align="center"><b>#</b></td>
	        <td width="360"><b>Titolo</b></td>
	        <!--td width="60" align="center"><b>Tipo</b></td-->
	        <td width="60" align="center"><b>Anteprima Questionario</b></td>
	        <td width="60" align="center"><b>Composizione Questionario</b></td>
		</tr>
		
		<form name="frmEsporta" method="post" action="QGR_EsportaQGrad.asp">
			<input type="hidden" name="idInfoQ" value>
		</form>

	<%
	while rsQuest.EOF <> True And nTotRecord < nTamPagina
	'do while not rsQuest.eof
		nProg = nProg + 1 
	'	Response.Write "nProg = " & nProg & "<BR>"

		' *********************************************************************
		' EPILI 17/02/2004
		' Per evitare che permetta di importare sempre lo stesso questionario e 
		' vada in errore la DLL per chiave duplicata.
		sSql = "SELECT COUNT(*) AS CONTA FROM INFO_QUEST WHERE " &_
			" DESC_QUEST = '" & Replace(rsQuest("DESC_QUEST"),"'","''") & "'" &_
			" AND INTEST_QUEST = '" & Replace(rsQuest("INTEST_QUEST"),"'","''") & "'"
	
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
		set rsConta = CX.execute(sSQL)
		verifica = cint(rsConta("CONTA"))
		rsConta.close
		set rsConta = nothing			
		if verifica > 0 then
			enable = "disabled title='Questionario gi� condiviso' Style='CURSOR:HELP'"	
		else
			enable = " title='Selezionare per condividere il questionario' Style='CURSOR:HELP' "	
		end if
		' *********************************************************************
		modificabile = true
		nMod = 1 
		if not controllaUtilizzo(clng(rsQuest("id_infoquest"))) then
			modificabile = false
			nMod = 0 
		end if
%>

<%'''8/7/2003: PASSATI VALORI TRAMITE FORM ANZICHE' QUERYSTRING %>
		<form name="frmModifica<%=nProg%>" method="post" action="QGR_ModQuest.asp">
			<input type="hidden" name="id" value="<%=rsQuest("id_infoquest")%>">
			<input type="hidden" name="mod" value="<%=nMod%>">
		</form>
		<form name="frmAnteprima<%=nProg%>" method="post" action="QGR_VisArea.asp">
			<input type="hidden" name="id" value="<%=rsQuest("id_infoquest")%>">
			<input type="hidden" name="mod" value="<%=1%>">
		</form>
		<form name="frmArea<%=nProg%>" method="post" action="QGR_VisArea.asp">
			<input type="hidden" name="id" value="<%=rsQuest("id_infoquest")%>">
			<input type="hidden" name="mod" value="<%=2%>">
		</form>
<%'''FINE 8/7/2003%>

		<tr class="tblsfondo">
			<td class="tblAgg">
			<%if verifica > 0 then%>
				<img SRC="/images/icons/Disabled.gif" title="Questionario gi� condiviso" Style="CURSOR:HELP" border="0" WIDTH="15" HEIGHT="15">	
				<input type="hidden" name="chkEsporta<%=nProg%>" value="false">
			<%else%>
				<input type="checkbox" <%=enable%> name="chkEsporta<%=nProg%>" value="<%=rsQuest("id_infoquest")%>">
			<%end if%>

			</td>

			<td class="tblDett">
				<%=nProg + (nActPagina * nTamPagina) - nTamPagina%>
			</td>
<%'''8/7/2003: PASSATI VALORI TRAMITE FORM ANZICHE' QUERYSTRING %>			
			<td>
				<!--a href="QGR_ModQuest.asp?id=<%'''=rsQuest("id_infoquest")%>" class="tblagg" onmouseover="javascript:window.status='' ; return true">					<%'''=rsQuest("desc_quest")%>				</a-->
				<a title="Modifica questionario" href="javascript:frmModifica<%=nProg%>.submit();" class="tblagg">
					<%=rsQuest("desc_quest")%>
				</a>			
			</td>
			<td align="center">
				<!--a href="QGR_VisAnteprima.asp?id=<%'''=rsQuest("id_infoquest")%>" class="tblAgg" onmouseover="javascript:window.status='' ; return true">					<img src="<%'''=Session("progetto")%>/images/formazione/txt.gif" border="0" alt="Anteprima Questionario di Gradimento" WIDTH="20" HEIGHT="21">				</a-->
				<a href="javascript:frmAnteprima<%=nProg%>.submit();" class="tblAgg">
					<img src="<%=Session("progetto")%>/images/formazione/txt.gif" border="0" alt="Anteprima Questionario di Gradimento" WIDTH="20" HEIGHT="21">
				</a>
			</td>
			<%
			if not modificabile then
			%>
				<td align="center">
					<!--a href="QGR_VisArea.asp?id=<%'''=rsQuest("id_infoquest")%>" class="tblAgg" onmouseover="javascript:window.status='' ; return true">						<img src="<%'''=Session("progetto")%>/images/formazione/re.gif" border="0" alt="Elenco delle Aree" WIDTH="12" HEIGHT="16">					</a-->
					<a href="javascript:frmArea<%=nProg%>.submit();" class="tblAgg">
						<img src="<%=Session("progetto")%>/images/formazione/re.gif" border="0" alt="Elenco delle Sezioni" WIDTH="12" HEIGHT="16">
					</a>
				</td>
			<%else%>
				<td align="center">
					-
				</td>
			<%end if%>
			
				
<%'''FINE 8/7/2003%>
		</tr>	
<%
		rsQuest.movenext
		nTotRecord=nTotRecord + 1
	wend 
	

	
	%>
	</table>
<%

	Response.Write "<TABLE border=0 width=470 align=center>"
	Response.Write "<tr>"
	if nActPagina > 1 then
		Response.Write "<td align=right width=480>"
		Response.Write "<A HREF=""javascript:Carica_Pagina('" & nActPagina-1 & "')"">"
		Response.Write "<img border=0 src=" & Session("Progetto") & "/images/precedente.gif></A></td>"
	end if
	if nActPagina < nTotPagina then
		Response.Write "<td align=right>"
		Response.Write "<A HREF=""javascript:Carica_Pagina('" & nActPagina+1 & "')"">"		
		Response.Write "<img border=0 src=" & Session("Progetto") & "/images/successivo.gif></A></td>"
	end if
	Response.Write "</tr></TABLE>"

%>
<br>
<table border="0" cellspacing="1" cellpadding="2" width="500">
	<tr>
		<td align="center">
			<%
				PlsLinkRosso "QGR_InsQuest.asp", "Inserisci nuovo questionario"
			%>
		</td>
		<td align="center">
			<%
				PlsLinkRosso "javascript:EsportaQ(" & nProg & ")", "Esporta i questionari selezionati"
			%>
		</td>
	</tr>
</table>
<%	
else%>
	
	<table border="0" cellspacing="1" cellpadding="2" width="500">
		<tr>
			<td align="center" class="tbltext3">
				Non ci sono questionari disponibili
			</td>
		</tr>
	</table>
	<br>
	<table border="0" cellspacing="1" cellpadding="2" width="500">
		<tr>
			<td align="center">
				<%
					PlsLinkRosso "QGR_InsQuest.asp", "Nuovo Questionario"
				%>
			</td>
		</tr>
	</table>
	
<%
end if%>
<form name="frmPagina" method="post" action="QGR_VisQuestionario.asp">
	<input type="hidden" name="pagina" value>
</form>

<%
rsQuest.close
End Sub
'----------------------------------------------------------------------
'M A I N
%>
<!--#include virtual="include/SetPulsanti.asp"-->
<!--#include virtual="/include/HTMLEncode.asp"-->

<%
'if ValidateService(session("idutente"),"QGR_VisQuestionario", CC) <> "true" then 
'	response.redirect "/util/error_login.asp"
'end if	


dim Progetto
Progetto = "/PLAVORO"
'Progetto = impostaprogetto()
'<!--#include virtual="/include/openpar.asp"-->
%>

<%
Inizio()
ElencoQ()
%>
<!--#include virtual="/include/closepar.asp"-->
<!--#include virtual="/include/closeconn.asp"-->
<!--#include virtual="/strutt_coda2.asp"-->
