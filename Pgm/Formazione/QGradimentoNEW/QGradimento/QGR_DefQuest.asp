<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include virtual ="/Strutt_testa2.asp"-->
<!--#include virtual = "/Include/OpenConn.asp"-->
<!--#include virtual = "Include/HTMLEncode.asp"-->
<!--#include virtual="/util/portallib.asp"-->
<!--#include virtual="include/RuoloFunzionale.asp"-->
<!--#include virtual = "include/DecCod.asp"-->
<!--#include virtual = "/pgm/Comunicazioni/DecDizDati.asp"-->

<%
	if ValidateService(session("idutente"),"QGR_DEFQUEST", CC) <> "true" then 
		response.redirect "/util/error_login.asp"
	end if

Dim iValCmb,iValCmb2, iValCmbSess, iRuolo, nRorga 
Dim Rs
Dim Sql, sSQL
Dim CC
Dim sProgetto
Dim bPresenzaBando, hPresenzaBando
Dim nIndElem

	iValCmb=Request("CmbQuest")
	iValCmb2=Request("CmbQuest2")
	IF iValCmb = "" AND iValCmb2 <> "" THEN
		iValCmb = iValCmb2
	END IF
	
	nRorga = request("cmbRorga")
	
	iRuolo = "" 
	iRuolo = Request("cmbruolo")
	
	iBando = Request("cmbBando")
	
	sCodiceSess = request("cmbCodSess")
	
	sProgetto = mid(session("Progetto"), 2)
%>

<html>
<head>
<script LANGUAGE="javascript">
	<!--#include virtual = "/Include/ControlDate.inc"-->	
	<!--#include Virtual = "/Include/help.inc"-->
	<!--#include virtual = "/Include/ControlNum.inc"-->
	<!--#include Virtual = "/Include/ControlString.inc"-->
			
//================================******=============================			
	function Aggiungi()
	{
		if (document.frmInserimento.CmbQuest2.value == ""){
		alert('Selezionare un Questionario di Gradimento')
		document.frmInserimento.CmbQuest2.focus();
		return false;
		}
				 
		if ((document.frmInserimento.cmbruolo.value == "")&&(document.frmInserimento.cmbRorga.value == "")){
		alert('� obbligatorio selezionare almeno uno dei due campi Ruolo Organizzativo/Ruolo Funzionale')
		document.frmInserimento.cmbRorga.focus()
		return false
		}
				
		if (document.frmInserimento.cmbruolo.value == 'DI')
		{
			if (document.frmInserimento.cmbBando.value != "") 
			{
				if (document.frmInserimento.cmbCodSess.value == "")
				{	
					alert('Selezionare una Sessione Formativa')
					document.frmInserimento.cmbCodSess.focus()
					return false;					
				}
				if (document.frmInserimento.cmbDurata.value == "")
				{
					alert('Selezionare il campo Durata ')
					document.frmInserimento.cmbDurata.focus()
					return false;					
				}
			}
					
			if (document.frmInserimento.cmbBando.value == ""){
				if ((document.frmInserimento.cmbCodSess.value != "") || (document.frmInserimento.cmbDurata.value != "")){
					alert('Selezionare un Bando')
					document.frmInserimento.cmbBando.focus()
					return false;					
				}
			} 
			if (document.frmInserimento.cmbBando.value == ""){
				alert('Selezionare un Bando')
				document.frmInserimento.cmbBando.focus()
				return false;					
			}
		}						
	}					
//================================******=============================			
//per fare Refresh della pagina 

	function Controllo(nVal) {
		var ruofu
				
		if (((document.frmInserimento.cmbRorga.value != "") && (document.frmInserimento.CmbQuest2.value == "")) ||
			((document.frmInserimento.cmbruolo.value != "") && (document.frmInserimento.CmbQuest2.value == ""))) {
			alert('Selezionare prima un Questionario')
			document.frmInserimento.cmbruolo.value = ""
			document.frmInserimento.cmbRorga.value = ""
			document.frmInserimento.CmbQuest2.focus()
			return false
		}
				
//		iRuolo = document.frmInserimento.cmbruolo.value;
//		if (document.frmInserimento.cmbruolo.value == "DI"){
		if (nVal == 1) {
			document.frmInserimento.action = "QGR_DefQuest.asp"
			document.frmInserimento.submit();
		}	
	}
//================================******=============================			
	function ControllaDati(){
		var ok
		var nrec
		var chkbox
				
		ok = 0 
		nrec= (document.frmElimina.txtNumElementi.value)
		for (i=1; i<=nrec; i++){
			chkbox = eval("document.frmElimina.ckFlag" + i)
			if (chkbox.checked){
				ok = 1
				break
			}
		}
		if (ok == 0){
			alert ('Selezionare almeno una voce dalla tabella!')
			return false
		}
		document.frmElimina.submit();
	}
</script>
</head>

<!-- *==============================******=============================* -->
	<body>
	<table border="0" width="520" height="81" cellspacing="0" cellpadding="0">
		<tr>
			<td width="500" height="81" background="<%=Session("Progetto")%>/images/titoli/strumenti2b.gif" valign="bottom" align="right">
				<table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
					<tr>
						<td width="100%" valign="top" align="right">
							<b class="tbltext1a">Associazione questionario</b>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<br>
	<table cellpadding="0" cellspacing="0" width="500" border="0">
		<tr height="18">
			<td class="sfondomenu" height="18" width="60%">
				<span class="tbltext0">
					<b>&nbsp; ASSOCIAZIONE QUESTIONARIO</b>
				</span>
			</td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">
			</td>
			<td valign="middle" align="right" width="35%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1">(*) campi obbligatori
			</td>
		</tr>
		<tr width="371" class="SFONDOCOMM">
			<td colspan="3">
				Per visualizzare l'associazione del Questionario di Gradimento con il Ruolo  
				Funzionale e/o Organizzativo premere su <b>Ricerca</b>.<br>
				Lasciando vuoto il campo verr� effettuata una ricerca libera.<br>
				Per Associare un questionario a un Ruolo Funzionale e/o Organizzativo cliccare su <b>Aggiungi</b>.<br>
				Per Eliminare una associazione selezionare il relativo record e cliccare su <b>Elimina</b>.
				<a href="Javascript:Show_Help('/Pgm/help/Formazione/QGradimento/QGR_DefQuest')" name="prov3" onmouseover="javascript:window.status='' ; return true">
					<img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0">
				</a>
			</td>
		</tr>
		<tr height="2">
			<td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>

<!--FORM DI RICERCA-->
	<form name="frmRicerca" action="QGR_DefQuest.asp" method="post">
	<table border="0" CELLPADDING="1" CELLSPACING="2" width="500">
		<tr>
			<td class="tbltext1" width="150" align="left">
				<b>Questionario</b>
			</td>
			<td align="left">
				<%
				Sql= "SELECT ID_INFOQUEST, DESC_QUEST FROM INFO_QUEST " &_
					 "ORDER BY DESC_QUEST"
						
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
				Set Rs = CC.Execute (Sql)
					Response.Write "<SELECT class='textblack' name='CmbQuest'>"
					Response.Write "<option value=''></option>"
					If Not Rs.eof then
						DO
							Response.Write "<option value=" & Rs("ID_INFOQUEST") & ">" & Rs("DESC_QUEST") & "</option>"
							Rs.MOVENEXT
						LOOP UNTIL Rs.EOF
					End If
				Rs.close
				Set Rs = Nothing
				Response.Write "</SELECT>"
				%>
			</td>
			<td align="left">
				<input type="image" name="Cerca" value="Ricerca" src="<%=Session("Progetto")%>/images/lente.gif" border="0">		
			</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
		</tr>
		<tr>
		    <td height="2" align="left" colspan="4" background="<%=Session("Progetto")%>/images/separazione.gif">
		    </td>
	    </tr>
	</table>				
	</form>
	
<!--FORM DI INSERIMENTO-->
	<form name="frmInserimento" action="QGR_CnfDefQuest.asp" method="post" onsubmit="return Aggiungi(this);">
	<table width="500" border="0" cellspacing="1" cellpadding="1">
		<tr> 
			<td class="tbltext1" align="left" width="150">
				<b>Questionario*</b>
			</td>
			<td align="left" COLSPAN="2">
				<%
				Sql= "SELECT ID_INFOQUEST, DESC_QUEST FROM INFO_QUEST "
				IF iValCmb <> "" then
					Sql= Sql & " WHERE ID_INFOQUEST=" & iValCmb
				ELSE			
					Sql = Sql & " ORDER BY DESC_QUEST"
				END IF
	'Response.Write "sql: " & sql
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
				Set Rs = CC.Execute (Sql)
				Response.Write "<SELECT class='textblack' name='CmbQuest2'>"
					IF iValCmb <> "" then
						Response.Write "<option value=" & iValCmb & ">" & Rs("DESC_QUEST") & "</option>"				
					ELSE
						Response.Write "<option value=''></option>"
						
						If Not Rs.eof then
							DO
							'*inizio 10/09
							'if iValCmb2 <> "" and  nIndElem = 0  then
							'*	
								If clng(iValCmb2) = clng(Rs("ID_INFOQUEST")) then
									Response.Write "<option selected value=" & Rs("ID_INFOQUEST") & ">" & Rs("DESC_QUEST") & "</option>"
								else
									Response.Write "<option value=" & Rs("ID_INFOQUEST") & ">" & Rs("DESC_QUEST") & "</option>"
								End if
							'*	
							'	Response.Write"<strong>Non sono presenti occorrenze nella tabella.</strong>"
							'end if
						'fine 10/09	
								Rs.MOVENEXT
							LOOP UNTIL Rs.EOF
						End If
					END IF
				Response.Write "</SELECT>"
				Rs.close
				Set Rs = Nothing
				%>
			</td>
		</tr>
		<tr>
			<td class="tbltext1" align="left">
				<b>Ruolo Organizzativo</b>
			</td>
			<td align="left">
				<%
					sRorga = "RORGA|0|"& date() &"|"& nRorga &"|cmbRorga' onchange='javascript:Controllo(0)'|ORDER BY DESCRIZIONE"
					CreateCombo (sRorga)
				%>	
			</td>
		</tr>			
		<tr>
			<td class="tbltext1" align="left">
				<b>Ruolo Funzionale</b>
			</td>
			<td align="left">
				<%
				sSQL = "Select idgruppo, cod_ruofu, desgruppo from gruppo ORDER BY cod_ruofu" 
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
					set rsRuoFunz = CC.execute(sSQL)
						sCodRuofu = rsRuoFunz("cod_ruofu")
					rsRuoFunz.MoveFirst
					Set rsRuoFunz = nothing 
				   
					sInt = "cmbruolo|0|" & iRuolo & "|onchange='javascript:Controllo(1)'"
					CreateRuolo (sInt)
					sRuofu =Request.Form("cmbruolo")
				%>	
			</td>
		</tr>
		<tr>
		<%
		IF iRuolo = "DI"  THEN
		%>
			<td class="tbltext1" align="left">
				<b>Bando*</b>
			</td>
			<td align="left">
				<%
				Sql =	"SELECT ID_BANDO, DESC_BANDO FROM BANDO B, PROGETTO P WHERE AREA_WEB = '" & sProgetto & "'" &_
						" AND B.ID_PROJ = P.ID_PROJ" &_
						" ORDER BY DESC_BANDO"
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
				Set Rs = CC.Execute (Sql)
				nIdBando = rs("ID_BANDO")
				
				If Not Rs.eof then
					bPresenzaBando = true
					Response.Write "<SELECT class='textblack' name='cmbBando' onchange='javascript:Controllo(1)'>"
					Response.Write "<option value=''></option>"
					Do
						Response.Write "<option value=" & Rs("ID_BANDO")
						If clng(Rs("ID_BANDO")) = clng(iBando) then
							Response.Write " selected"
						end if
						Response.Write  ">" & strHTMLEncode(Rs("DESC_BANDO")) & "</option>"
						'Response.Write "<option value=" & iBando & ">" & strHTMLEncode(Rs("DESC_BANDO")) & "</option>"
						Rs.MOVENEXT
					LOOP UNTIL Rs.EOF
					Response.Write "</SELECT>"
					Rs.close
					Set Rs = Nothing
				Else	
					bPresenzaBando = false
					Response.Write "<SELECT class='textblack' name='cmbBando' disabled>"
					Response.Write "<option value=''></option>"
					Response.Write "</SELECT>"	
				End if								
				%>
				<input type="hidden" name="hPresenzaBando" value="<%=bPresenzaBando%>">
			</td>
		</tr>
		<%
			if clng(iBando) = "" then
			   cmbCodSess = false
			end if	
		%>
		<input type="hidden" value="<%=iValCmb%>" name="hValCmb">
		<tr>
			<td class="tbltext1" align="left">
				<b>Sessione Formativa*</b>
			</td>
			<td align="left">
			<%
			Response.Write "<SELECT class='textblack' name='cmbCodSess'>"

				Sql =	"SELECT F.COD_SESSIONE, F.DESC_FASE,  B.ID_BANDO FROM BANDO B, PROGETTO P, FASE_BANDO F WHERE AREA_WEB = '" & sProgetto & "'" &_
						" AND P.ID_PROJ = B.ID_PROJ" &_
						" AND B.ID_BANDO = " & clng(iBando) &_
						" AND F.ID_BANDO = " & clng(iBando) &_
						" ORDER BY F.COD_SESSIONE"
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
				Set RsCodSess = CC.Execute (Sql)
				
			if clng(iBando) = "" then
			   cmbCodSess = false
			end if
			IF not RsCodSess.eof then	
				sValCodSess = RsCodSess("COD_SESSIONE")
				sValDescSess = RsCodSess("DESC_FASE")
			ELSE
			   	sValCodSess= ""
			   	sValDescSess= ""			
			END IF
				Response.Write "<option value=''></option>"
						If Not RsCodSess.eof then
							DO
								Response.Write "<option value=" & RsCodSess("COD_SESSIONE")
								if sCodiceSess = RsCodSess("COD_SESSIONE") then
									Response.Write " selected" 
								end if
''								Response.Write  ">" &  RsCodSess("COD_SESSIONE") & "</option>"
								Response.Write  ">" &  RsCodSess("DESC_FASE") & "</option>"
								''	Response.Write "<option value='" & sValCodSess & "'>" & RsCodSess("COD_SESSIONE") & "</option>"
								RsCodSess.MOVENEXT
							LOOP UNTIL RsCodSess.EOF
						End If
			Response.Write "</SELECT>"
				RsCodSess.close
				Set RsCodSess = Nothing
			%>

			</td>
		</tr>
		<tr>
			<td class="tbltext1" align="left">
				<b>Durata*</b>
			</td>
			<td align="left">
					<select class="textblack" name="cmbDurata" id="cmbDurata">
			<%
			' INIZIO modifica 08/07/2003 perch� inserita include DECDIZDATI (DecCampoDesc)
			aEsito=DecCampoDesc("ASS_QUEST","FL_ATTIVAZIONE")

			'Sql =" SELECT id_campo_desc FROM DIZ_DATI WHERE ID_TAB= 'ASS_QUEST' " &_
			'	 " AND id_campo = 'FL_ATTIVAZIONE'"
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
			'	Set RsAssQ = CC.Execute (Sql)

			'	sEsito = RsAssQ("id_campo_desc")
			'	aEsito = split(sEsito, "|")
				Response.Write "<OPTION selected></OPTION>" 
				for  i = 0 to Ubound(aEsito) step 2
					Response.Write "<option value='" & aEsito(i) & "'>" & Ucase((aEsito(i+1))) & "</option>"
				next
					Response.Write "</SELECT>"
			'	RsAssQ.Close
			'	set RsAssQ = nothing
			' FINE modifica 08/07/2003
			%>
			</td>
		</tr>
		<%
		END IF
		%>	
		<input TYPE="HIDDEN" NAME="TXTESISTE" VALUE="<%=esiste%>">
		
		<!--fine -->
			<tr>
				<td class="tbltext1"><b>Erogazione</td></b>
				<td>
				<select class="textblack" name="cmbErog">
				<%
				Response.Write "<option value=''></option>"
				sSQL = "SELECT ID_CAMPO_DESC FROM DIZ_DATI WHERE " &_
					"ID_TAB='ASS_QUEST' AND ID_CAMPO='EROGAZIONE'" 
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
				set rsDizDati = cc.Execute(sSQL)
				aTipo = split(rsDizDati("ID_CAMPO_DESC"),"|")
				
				for i = 0 to ubound (aTipo) step 2
				Response.Write "<option value='" & aTipo(i) & "'>" & (aTipo(i+1)) & "</option>"
				next
				%>
				</select>
			</td>
			</tr>
				<tr>
				<td class="tbltext1"><b>Fruizione</td></b>
				<td>
				<select class="textblack" name="cmbFruiz">
				<%
				Response.Write "<option value=''></option>"
				sSQL = "SELECT ID_CAMPO_DESC FROM DIZ_DATI WHERE " &_
					"ID_TAB='ASS_QUEST' AND ID_CAMPO='FRUIZIONE'" 
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
				set rsDizDati = cc.Execute(sSQL)
				bTipo = split(rsDizDati("ID_CAMPO_DESC"),"|")
				
				for i = 0 to ubound (bTipo) step 2
				Response.Write "<option value='" & bTipo(i) & "'>" & (bTipo(i+1)) & "</option>"
				next
				%>
				</select>
			</td>
			</tr>
			<td align="center" colspan="2">
				<input type="image" src="<%'=impostaProgetto() %>/images/aggiungi.gif" border="0" value="Aggiungi" align="top" id="Aggiungi" name="Aggiungi">
			</td>
		</tr>
	</table>
	</form>
		
<!--FORM DI ELIMINAZIONE-->			
<form name="frmElimina" method="post" action="QGR_CanDefQuest.asp">
<% 	
on error resume next 
   	set rsQuest= server.CreateObject("ADODB.Recordset")

   	If iValCmb= "" Then
		sSQL =	"SELECT IQ.DESC_QUEST, b.id_bando ,AQ.COD_RUOFU, B.DESC_BANDO, FB.COD_SESSIONE, AQ.FL_ATTIVAZIONE, AQ.ID_INFOQUEST, AQ.COD_RORGA, FB.DESC_FASE " &_
				" FROM info_quest iq, ass_quest aq, bando b, fase_bando FB, progetto P" &_
				" WHERE p.id_proj = b.id_proj" &_ 
				" AND fb.id_bando = b.id_bando" &_
				" AND AQ.ID_INFOQUEST (+) = IQ.ID_INFOQUEST" &_
				" AND AQ.COD_sessione = FB.cod_sessione  " &_
				" AND aq.id_bando = b.id_bando " &_
				" AND AREA_WEB = '" & sProgetto & "'" &_
				" ORDER BY IQ.DESC_QUEST, B.DESC_BANDO, FB.COD_SESSIONE, AQ.COD_RUOFU"
    Else
		sSQL =	"SELECT IQ.DESC_QUEST, b.id_bando ,AQ.COD_RUOFU, B.DESC_BANDO, FB.COD_SESSIONE, AQ.FL_ATTIVAZIONE, AQ.ID_INFOQUEST, AQ.COD_RORGA, FB.DESC_FASE " &_
				" FROM info_quest iq, ass_quest aq, bando b, fase_bando FB, progetto P" &_
				" WHERE p.id_proj = b.id_proj" &_ 
				" AND fb.id_bando = b.id_bando" &_
				" AND AQ.ID_INFOQUEST (+) = IQ.ID_INFOQUEST" &_
				" AND AQ.COD_sessione = FB.cod_sessione  " &_
				" AND aq.id_bando = b.id_bando " &_
				" AND IQ.ID_INFOQUEST =" & iValCmb &_
				" AND AREA_WEB = '" & sProgetto & "'" &_
				" ORDER BY IQ.DESC_QUEST, B.DESC_BANDO, FB.COD_SESSIONE, AQ.COD_RUOFU"
    End If
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	rsQuest.Open sSQL,CC,3
	nIndElem=0
	If not rsQuest.eof Then
%>
<input type="hidden" value="<%=iValCmb%>" name="hValCmb">
<%	titoliTabella()	%>

	<!--table WIDTH="500" cellspacing="1" cellpadding="1" align="center" BORDER="0"-->
	<%
	While rsQuest.EOF <> True 'And nTotRecord < nTamPagina
		nIndElem = nIndElem + 1 
	%>
	<input type="hidden" value="<%=rsQuest("ID_INFOQUEST")%>" name="txtidQuest<%=nIndElem%>">

		<tr class="tblsfondo">
			<td width="25" align="center">
				<input type="checkbox" name="ckFlag<%=nIndElem%>">
			</td>
			<%
			If iValCmb= "" Then%>
			<td class="tblDett" width="145">
				<%'=strHTMLEncode (rsQuest("DESC_QUEST"))%>
				<%=rsQuest("DESC_QUEST")%>
				<input type="hidden" value="<%=rsQuest("DESC_QUEST")%>" name="txtQuest<%=nIndElem%>">
			</td>
			
			<td class="tblDett" width="100">
				<%
				if rsQuest("COD_RORGA") <> "" then
					Response.Write DecCodVal("RORGA",0,date(),rsQuest("COD_RORGA"),1)
				else
					Response.Write "---"
				end if
				Response.Write "&nbsp;/&nbsp;"
				if rsQuest("COD_RUOFU") <> "" then
					Response.Write DecRuolo(rsQuest("COD_RUOFU"))
				else
					Response.Write "---"
				end if %>
				<input type="hidden" value="<%=rsQuest("COD_RORGA")%>" name="txtorga<%=nIndElem%>">
				<input type="hidden" value="<%=rsQuest("COD_RUOFU")%>" name="txtruolo<%=nIndElem%>">
			</td>
			<td class="tblDett" width="130">
				<%=rsQuest("DESC_BANDO")%>
				<input type="hidden" value="<%=rsQuest("DESC_BANDO")%>" name="txtBando<%=nIndElem%>">
			</td>
			<td class="tblDett" width="50">
				<%
				'modifica 09/09/03
				'=rsQuest("COD_SESSIONE")%>
				<input type="hidden" value="<%=rsQuest("COD_SESSIONE")%>" name="txtCodSess<%=nIndElem%>">
				<%=rsQuest("DESC_FASE")%>
				<input type="hidden" value="<%=rsQuest("DESC_FASE")%>" name="txtDescSess<%=nIndElem%>">
			</td>
			<td class="tblDett" width="50">
				<%
				'modifica 08/07/2003
				'=rsQuest("FL_ATTIVAZIONE")
				aEsito=DecCampoDesc("ASS_QUEST","FL_ATTIVAZIONE")
				for i = 0 to UBound(aEsito) step 2
					if aEsito(i) = rsQuest("FL_ATTIVAZIONE") then
						Response.write aEsito(i+1)
					end if
				next
				'fine modifica 
				%>
				<input type="hidden" value="<%=rsQuest("FL_ATTIVAZIONE")%>" name="txtDurata<%=nIndElem%>">
				<input type="hidden" value="<%=rsQuest("ID_INFOQUEST")%>" name="CmbQuestionario2<%=CmbQuest2%>">
			</td>
			
			<%Else%>
			
			<td class="tblDett" width="145">
				<%
				if rsQuest("COD_RORGA") <> "" then
					Response.Write DecCodVal("RORGA",0,date(),rsQuest("COD_RORGA"),1)
				else
					Response.Write "---"
				end if
				Response.Write "&nbsp;/&nbsp;"
				if rsQuest("COD_RUOFU") <> "" then
					Response.Write DecRuolo(rsQuest("COD_RUOFU"))
				else
					Response.Write "---"
				end if %>
				<input type="hidden" value="<%=rsQuest("COD_RORGA")%>" name="txtorga<%=nIndElem%>">
				<input type="hidden" value="<%=rsQuest("COD_RUOFU")%>" name="txtruolo<%=nIndElem%>">
			</td>
			<td class="tblDett" width="130">
				<%=rsQuest("DESC_BANDO")%>
				<input type="hidden" value="<%=rsQuest("DESC_BANDO")%>" name="txtBando<%=nIndElem%>">
			</td>
			<td class="tblDett" width="150">
				<%
				'modifica 09/09/03
				'=rsQuest("COD_SESSIONE")%>
				<input type="hidden" value="<%=rsQuest("COD_SESSIONE")%>" name="txtCodSess<%=nIndElem%>">
				<%=rsQuest("DESC_FASE")%>
				<input type="hidden" value="<%=rsQuest("DESC_FASE")%>" name="txtDescSess<%=nIndElem%>">
			</td>
			<td class="tblDett" width="50">
				<%
				'modifica 08/07/2003
				'=rsQuest("FL_ATTIVAZIONE")
				aEsito=DecCampoDesc("ASS_QUEST","FL_ATTIVAZIONE")
				for i = 0 to UBound(aEsito) step 2
					if aEsito(i) = rsQuest("FL_ATTIVAZIONE") then
						Response.write aEsito(i+1)
					end if
				next
				'fine modifica 
				%>
				<input type="hidden" value="<%=rsQuest("FL_ATTIVAZIONE")%>" name="txtDurata<%=nIndElem%>">
				<input type="hidden" value="<%=rsQuest("ID_INFOQUEST")%>" name="CmbQuestionario2<%=CmbQuest2%>">
			</td>
			<%End if%>
		</tr>
	<%
	rsQuest.MoveNext
	Wend%>
	<!--/table-->
	<%	rsQuest.Close
		Set rsQuest = Nothing					
	End If
	
	If iValCmb= "" Then
		sSQL =	"SELECT cod_ruofu, cod_rorga, desc_quest, AQ.ID_INFOQUEST FROM ASS_QUEST aq,INFO_QUEST iq " &_
				" WHERE cod_sessione IS NULL " &_
				" AND aq.id_infoquest = iq.id_infoquest" &_
				" ORDER BY IQ.DESC_QUEST "
	Else
		sSQL =	"SELECT cod_ruofu, cod_rorga, desc_quest, AQ.ID_INFOQUEST FROM ASS_QUEST aq,INFO_QUEST iq " &_
				" WHERE cod_sessione IS NULL " &_
				" AND aq.id_infoquest = iq.id_infoquest" &_
				" AND aq.id_infoquest =" & iValCmb &_
				" ORDER BY IQ.DESC_QUEST "
	End If			
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	set rsQuest2= CC.execute (sSQL)
	
	If not rsQuest2.eof Then
	    if clng(nIndElem) = 0 then 
			titoliTabella()
	%>
			<!--table WIDTH="500" cellspacing="1" cellpadding="1" align="center" BORDER="0"-->
	<%	end if 
		While rsQuest2.EOF <> True 
			nIndElem = nIndElem + 1 
	%>
		<input type="hidden" value="<%= rsQuest2("ID_INFOQUEST")%>" name="txtidQuest<%=nIndElem%>">

		<tr class="tblsfondo">
			<td width="25"><input type="checkbox" name="ckFlag<%=nIndElem%>"></td>
			<%
			If iValCmb= "" Then%>
			<td class="tblDett" width="145">
				<%'=strHTMLEncode (rsQuest2("DESC_QUEST"))%>
				<%=rsQuest2("DESC_QUEST")%>
				<input type="hidden" value="<%=rsQuest2("DESC_QUEST")%>" name="txtQuest<%=nIndElem%>">
			</td>
			
			<td class="tblDett" width="100">
				<%
				if rsQuest2("COD_RORGA") <> "" then
					Response.Write DecCodVal("RORGA",0,date(),rsQuest2("COD_RORGA"),1)
				else	
					Response.Write "---"
				end if
				Response.Write "&nbsp;/&nbsp;"
				if rsQuest2("COD_RUOFU") <> "" then
					Response.Write DecRuolo(rsQuest2("COD_RUOFU"))
				else
					Response.Write "---"
				end if
				%>
				<input type="hidden" value="<%=rsQuest2("COD_RORGA")%>" name="txtorga<%=nIndElem%>">
				<input type="hidden" value="<%=rsQuest2("COD_RUOFU")%>" name="txtruolo<%=nIndElem%>">
			</td>
			<td class="tblDett" width="130">
				&nbsp;
			</td>
			<td class="tblDett" width="50">
				&nbsp;
			</td>
			<td class="tblDett" width="50">
				&nbsp;
			</td>
			<%Else%>
			<td class="tblDett" width="145">
				<%
				if rsQuest2("COD_RORGA") <> "" then
					Response.Write DecCodVal("RORGA",0,date(),rsQuest2("COD_RORGA"),1)
				else	
					Response.Write "---"
				end if
				Response.Write "&nbsp;/&nbsp;"
				if rsQuest2("COD_RUOFU") <> "" then
					Response.Write DecRuolo(rsQuest2("COD_RUOFU"))
				else
					Response.Write "---"
				end if
				%>
				<input type="hidden" value="<%=rsQuest2("COD_RORGA")%>" name="txtorga<%=nIndElem%>">
				<input type="hidden" value="<%=rsQuest2("COD_RUOFU")%>" name="txtruolo<%=nIndElem%>">
			</td>
			<td class="tblDett" width="130">
				&nbsp;
			</td>
			<td class="tblDett" width="150">
				&nbsp;
			</td>
			<td class="tblDett" width="50">
				&nbsp;
			</td>
			<%End if%>
		</tr>
		<%
		rsQuest2.MoveNext
		Wend
		rsQuest2.Close
		Set rsQuest2 = Nothing
	End if	
	%>
	
	<%if clng(nIndElem) = 0 then  
	    Response.Write "<table align=center>"
		Response.Write"<tr>"
		Response.Write"<td class=tbltext3><br>"
		Response.Write"<strong>Non sono presenti occorrenze nella tabella.</strong>"
		Response.Write"</td>"
		Response.Write"</tr>"
		Response.Write"</table>"
	  else
	    Response.Write "</table>" 	
	  end if	
	%>
	<input type="hidden" value="<%=nIndElem%>" name="txtNumElementi">
</form>

	<table border="0" cellspacing="0" cellpadding="0" width="500">
		<tr>
			<td align="middle">
				<input type="image" src="<%=session("Progetto")%>/images/elimina.gif" border="0" value="Elimina" align="top" id="image1" name="image1" onclick="Javascript:ControllaDati();">
			</td>
		</tr>
	</table>
	</body>
</html>
<% sub titoliTabella() %>
       <table width="500" cellspacing="1" cellpadding="1" align="center" border="0">
			<tr class="sfondocomm">
				<td width="25" align="center">
					<b> <img src="<%=session("Progetto")%>/images/cestino.gif"></b>
				</td>
				<%
				If iValCmb= "" Then%>
				<td width="145">
					<b>Questionario</b>
				</td>
				<td width="100">
					<b>Ruolo Organizz.<br>Ruolo Funzion.</b>
				</td>
				<td width="130">
					<b>Bando</b>
				</td>
				<td width="50">
					<b>Sessione Formativa</b>
				</td>
				<td width="50">
					<b>Durata</b>
				</td>
				<%ELse%>
				<td width="150">
					<b>Ruolo Organizz.<br>Ruolo Funzion.</b>
				</td>
				<td width="130">
					<b>Bando</b>
				</td>
				<td width="150">
					<b>Sessione Formativa</b>
				</td>
				<td width="50">
					<b>Durata</b>
				</td>
				<%End if%>
			</tr>
	  
<% end sub %>
<!--#include virtual ="/include/closeconn.asp"-->
<!--#include virtual ="/Strutt_coda2.asp"-->
