<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%
Sub Inizio()
	dim sFunzione
	dim sTitolo
	dim sCommento
	dim bCampiObbl
	
	sFunzione = "GESTIONE QUESTIONARIO DI GRADIMENTO"
	sTitolo = "CANCELLAZIONE RISPOSTA"
	sCommento = "Cancellazione della risposta"
	bCampiObbl = false
%>	
	<!--#include virtual="include/setTestata.asp"-->
<%
End Sub

'--------------------------------------------------------------------------------------
Sub Cancella()
		
	dim sErrore, sSQL
	dim nIdRisp,dDtTmst
	dim rsCRisp
	
	'Prendo i valori del tipo_domanda
	sSQL =	"SELECT id_campo_desc FROM diz_dati WHERE id_tab='IQ_DOMANDA'" &_
			" AND id_campo='TIPO_DOMANDA'"
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	SET rsDdati = CC.Execute(sSQL)
		sTipDom = rsDdati("id_campo_desc")
		aTipDom = split(sTipDom,"|")
	rsDdati.close
	set rsDdati = nothing	
		
	CC.BeginTrans
	
	If sTipo = "T" then	
		nNumElem = clng(Request.Form("nElementi"))
		for i=1 to (nNumElem - 1)
			sCnfElim = Request.Form("chkElim" & i)
			nIdRisp = clng(Request.Form("nIdR" & i))
			dDtTmst = Request.Form("dTMST" & i)
			
			If sCnfElim = "on" then
				sSQL = "delete from iq_risposta where id_rispostaiq = " & nIdRisp
				sErrore= EseguiNoCTrace("",nIdRisp,"iq_risposta",Session("persona"),"","","DEL", sSQL,0,dDtTmst,CC)
				if sErrore <> "0" then
					exit for
				end if
			End if
	
			if sErrore = "0" then
				sql = "Delete from STRUTTURA_QUEST where ID_ELEMENTO = " & nIdRisp 
				sErrore = EseguiNoC(sql,CC)
			end if

		next
	Else
		nIdRisp = clng(Request.Form("nIdR"))
		dDtTmst = Request.Form("dTMST")
		
		sSQL = "delete from iq_risposta where id_rispostaiq = " & nIdRisp
		sErrore= EseguiNoCTrace("",nIdRisp,"iq_risposta",Session("persona"),"","","DEL", sSQL,0,dDtTmst,CC)

		if sErrore = "0" then
			sql = "Delete from STRUTTURA_QUEST where ID_ELEMENTO = " & nIdRisp 
			sErrore = EseguiNoC(sql,CC)
		end if

	End if 

%>

<form name="frmRitorna" id="frmRitorna" action="QGR_VisRisposta.asp" method="post">
	<input type="hidden" name="txtTipoRisposta" id="txtTipoRisposta" value="<%=sTipo%>">
	<input type=hidden name=Idr id=Idr value="<%=nIdDom%>">
	<input type=hidden name=Ida id=Ida value="<%=nIdArea%>">
	<input type=hidden name=Idq id=Idq value="<%=nIdQuest%>">
	<input type=hidden name=Idb id=Idb value="<%=nIdBlocco%>">
	<input type=hidden name=Ide id=Ide value="<%=nIdElem%>">
</form>		

<%	IF sErrore <> "0" then 
		cc.RollbackTrans
		Errore sErrore
	ELSE
		If sTipo="T" then 
			nRispCh = 0
			nRispAp = 0
			sql	= "SELECT fl_tiprisp FROM iq_risposta WHERE id_domandaiq=" & nIdDom
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
			set rs	= CC.execute(sql)
				Do While not rs.EOF
					if rs("fl_tiprisp") = "C" then
						nRispCh = nRispCh + 1
					else
						nRispAp = nRispAp +1
					end if
				rs.movenext
				Loop
			rs.close
			set rs=nothing
			
			if nRispCh = 0 and nRispAp > 0 then
				'sTipo_domanda = "A"
				sTipo_domanda = aTipDom(0)
			elseif nRispAp = 0 and nRispCh > 0 then
				'sTipo_domanda = "C"
				sTipo_domanda = aTipDom(2)
			elseif nRispCh > 0 and nRispAp > 0 then
				'sTipo_domanda = "S"
				sTipo_domanda = aTipDom(4)
			elseif nRispCh = 0 and nRispAp = 0 then
				sTipo_domanda = ""
			end if
			
			sql= "UPDATE iq_domanda SET tipo_domanda="
			if sTipo_domanda = "" then
				sql = sql & "null"
			else
				sql = sql & "'" & sTipo_domanda & "'"
			end if
			sql = sql &  " WHERE id_domandaiq =" & nIdDom
			
			sErrT = EseguiNoC(sql,CC)
			if sErrT <> "0" then
				cc.RollbackTrans
				Errore sErrT
			else
				CC.CommitTrans %>
				<script LANGUAGE="Javascript">
					frmRitorna.submit();
				</script>
<%			end if	
		Else 
			sql= "UPDATE iq_domanda SET tipo_domanda = null WHERE id_domandaiq =" & nIdDom
			sErrV = EseguiNoC(sql,CC)
			if sErrV <> "0" then
				CC.RollbackTrans
				Errore sErrV
			else	
				CC.CommitTrans%>
				<br><br>
				<table border=0 cellspacing=2 cellpadding=1 width="500">
				<tr align=middle>
						<td class=tbltext3>
							Cancellazione correttamente effettuata
						</td>
					</tr>
				</table>
				<br><br>
				<form name="frmReload" method="post" action="QGR_Sessioni.asp">
					<input type="hidden" name="idq" value="<%=nIdQuest%>">
					<input type="hidden" name="ida" value="<%=nIdArea%>">
				</form>
				<script language="javascript">
					opener.document.frmReload.submit();
					self.close();
				</script>

				<br><br>
<%			end if
		End if
	END IF 
	
End Sub
'---------------------------------------------------------------------------
Sub Errore(sErrore)%>
	<br><br>
	<table border=0 cellspacing=2 cellpadding=1 width="500">
		<tr align=middle>
			<td class="tbltext3">
				Errore nella cancellazione. <%=sErrore%>
			</td>
		</tr>
	</table>		
	<br><br>

	<br><br>
	<%plsChiudi()%>
	
<%
End sub
'---------------------------------------------------------------------------
'M A I N
%>
<html>
<head>
<title><%=Session("TIT")%> [ <%=Session("login")%> ] </title>
<link REL="STYLESHEET" TYPE="text/css" HREF="<%=session("progetto")%>/fogliostile.css">
</head>
<body class="sfondocentro" topmargin="2" leftmargin="0">
<center>
<!--#include virtual="/include/openconn.asp"-->
<!--#include virtual="/Util/DBUtil.asp"-->
<!--#include virtual="/Include/SysFunction.asp"-->
<!--#include virtual="/Include/SetPulsanti.asp"-->
<%
	dim nIdDom, nIdArea, nIdQuest, sTipo
	
	nIdQuest= clng(Request.Form("nIdQuest"))
	nIdArea = clng(Request.Form("nIdArea"))
	nIdDom	= clng(Request.Form("nIdDom"))
	nIdElem	= clng(Request.Form("nIdElem"))
	nIdBlocco = clng(Request.Form("nIdBlocco"))
	sTipo =	Request.Form("rTipo")
	
	
	'Response.Write "idd=" & nIdDom & "<br>"
	'Response.Write "ida=" & nIdArea & "<br>"
	'Response.Write "idq" & nIdQuest & "<br>"
	'Response.Write "ide=" & nIdElem & "<br>"
	'Response.Write "idb=" & nIdBlocco & "<br>"
	

	Inizio()
	Cancella()
%>			
<!--#include virtual="/include/closeconn.asp"-->
</body></html>
