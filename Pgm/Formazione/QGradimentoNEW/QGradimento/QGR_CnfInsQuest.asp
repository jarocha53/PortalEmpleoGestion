<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%
Sub Inizio
	dim sFunzione
	dim sTitolo
	dim sCommento
	dim bCampiObbl
	
	sFunzione = "GESTIONE QUESTIONARI DI GRADIMENTO"
	sTitolo = "INSERIMENTO QUESTIONARI DI GRADIMENTO"
	sCommento = "Conferma dell'inserimento del Questionario di Gradimento"
	bCampiObbl = false
%>
	<!--#include virtual="include/SetTestata.asp"-->
<%
End Sub
'------------------------------------------------------------------------
Sub Conferma()
dim DescQ, TipoQ, IntQ, MExec, CodS,sRipetibile
dim sSQL, sErrore

DescQ	= strHTMLEncode(Trim(Request.Form("DescQuest")))
DescQ	= replace(DescQ,"'","''")

TipoQ = ""

IntQ = strHTMLEncode(Trim(Request.Form("IntQuest")))
IntQ = replace(IntQ,"'","''")

MExec = ""

CodS = replace(Request.Form("CodSessione"),"'","''")

sRipetibile=Request.Form("ckRipetibile")

if sRipetibile <> 1 then
   sRipetibile = 0
end if


'INSERIMENTO"
'<br>DescQ: " & DescQ
'<br>TipoQ: " & TipoQ
'<br>IntQ: " & IntQ
'<br>MExec: " & MExec
'<br>CodR: " & CodR
'<br>CodS: " & CodS
'<br>"

Adesso="TO_DATE('" & day(now) & "/" & _
		   month(now) & "/" & year(now) & _
		   " " & hour(now) & ":" & minute(now) & ":" & second(now) & _
		   "','DD/MM/YYYY HH24:MI:SS')"	

' ****** SELECT asteriscata e ricreata il 02/07/2003 x modifica struttura Tab. INFO_QUEST relativa al campo COD_RUOLO *******
' ******* che � stato tolto **************
'sSQL = "INSERT INTO INFO_QUEST (DESC_QUEST, TIPO_QUEST, INTEST_QUEST, MOD_EXEC, COD_RUOLO,  DT_TMST) " &_
'       "VALUES ('" & DescQ & "','" & TipoQ & "', '" & IntQ & "', '" & MExec & "', '" & CodR & "', " & Adesso & ")"

'sSQL = "INSERT INTO INFO_QUEST (DESC_QUEST, TIPO_QUEST, INTEST_QUEST, " & _
'		"MOD_EXEC,FL_REPEAT, DT_TMST) " &_
'		"VALUES ('" & DescQ & "','" & TipoQ & "', '" & IntQ & _
'		"', '" & MExec & "', "  & sRipetibile & "," & Adesso & ")"

sql="SELECT DESC_QUEST FROM INFO_QUEST WHERE DESC_QUEST ='" & DescQ & "'"
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
Set rsDescQuest = cc.execute(sql)
if  not rsDescQuest.eof then
%>
<br><br>
<table border=0 cellspacing=2 cellpadding=1 width='500'>
	<tr align=middle>
		<td class='tbltext3'>
			Titolo del questionario gi� presente. 
		</td>
	</tr>
</table>
<%
else

sSQL = "INSERT INTO INFO_QUEST (DESC_QUEST, TIPO_QUEST, INTEST_QUEST, " & _
		"FL_REPEAT, DT_TMST) " &_
		"VALUES ('" & DescQ & "','" & TipoQ & "', '" & IntQ & _
		"',"  & sRipetibile & "," & Adesso & ")"


sErrore = Esegui(nIdQuest ,"Info_Quest",Session("persona"),"INS",sSQL,1,dDtTmst)

if sErrore="0" then
    
    sArea = Request.Form("txtAree")
    if sArea <> "" then
        sql="SELECT ID_INFOQUEST FROM INFO_QUEST WHERE DESC_QUEST ='" & DescQ & "'"
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
        SET rsQuest = cc.execute(sql)
        IdQuest = rsQuest("ID_INFOQUEST")
        set rsQuest = nothing
		for i = 1 to sArea
			DescA = strHTMLEncode(Trim(Request.Form("DescArea" & i)))
			DescA = replace(DescA,"'","''")

			Adesso="TO_DATE('" & day(now) & "/" & _
					   month(now) & "/" & year(now) & _
					   " " & hour(now) & ":" & minute(now) & ":" & second(now) & _
					   "','DD/MM/YYYY HH24:MI:SS')"	

			sSQL = "INSERT INTO IQ_AREA (DESC_AREAIQ, ID_INFOIQ, DT_TMST) " &_
			       "VALUES ('" & DescA & "', " & IdQuest& ", " & Adesso & ")"

			sErrore = Esegui(nIdQuest ,"IQ_AREA",Session("persona"),"INS",sSQL,1,dDtTmst)
		next
    end if
end if

if sErrore="0" then
%>
<br><br>
<table border=0 cellspacing=2 cellpadding=1 width='500'>
	<tr align=middle>
		<td class='tbltext3'>
			Inserimento del questionario correttamente effettuato
		</td>
	</tr>
</table>
<%
else
%>
<br><br>
<table border=0 cellspacing=2 cellpadding=1 width='500'>
	<tr align=middle>
		<td class='tbltext3'>
			Errore nell'inserimento. <%=sErrore%>
		</td>
	</tr>
</table>
<%
end if
end if
Set rsDescQuest = nothing
%>
<br>
<br>
<table width=500>
	<tr>
		<td align=center>
			<%
			PlsLinkRosso "QGR_VisQuestionario.asp", "Elenco dei Questionari di Gradimento"
			%>
		</td>
	</tr>
</table>
<%
End Sub
'------------------------------------------------------------------------
'M A I N
%>
<!--#include virtual="/strutt_testa2.asp"-->
<!--#include virtual="/include/openconn.asp"-->
<!--#include virtual="/Util/DBUtil.asp"-->
<!--#include virtual="/Include/HTMLEncode.asp"-->
<!--#include virtual="/Include/SysFunction.asp"-->
<!--#include virtual="include/SetPulsanti.asp"-->
<%	
	Inizio()
	Conferma()
%>
<!--#include virtual="/include/closeconn.asp"-->
<!--#include virtual="/strutt_coda2.asp"-->
