<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%
Sub Inizio
	dim sFunzione
	dim sTitolo
	dim sCommento
	dim bCampiObbl
	
	sFunzione = "GESTIONE QUESTIONARI DI GRADIMENTO"
	sTitolo = "INSERIMENTO SEZIONE"
	sCommento = "Conferma dell'inserimento Sezione<br>Questionari di Gradimento"
	bCampiObbl = false
%>
	<!--#include virtual="include/SetTestata.asp"-->
<%
End Sub
'------------------------------------------------------------------------
Sub Conferma()

dim DescA, IdQuest
dim sSQL

DescA = strHTMLEncode(Trim(Request.Form("DescArea")))
DescA = replace(DescA,"'","''")

IdQuest = replace(Request.Form("IdQuest"),"'","''")

Adesso="TO_DATE('" & day(now) & "/" & _
		   month(now) & "/" & year(now) & _
		   " " & hour(now) & ":" & minute(now) & ":" & second(now) & _
		   "','DD/MM/YYYY HH24:MI:SS')"	

sSQL = "INSERT INTO IQ_AREA (DESC_AREAIQ, ID_INFOIQ, DT_TMST) " &_
       "VALUES ('" & DescA & "', " & IdQuest& ", " & Adesso & ")"

sErrore = Esegui(nIdQuest ,"Info_Quest",Session("persona"),"INS",sSQL,1,dDtTmst)

if sErrore="0" then
%>
<br><br>
<table border=0 cellspacing=2 cellpadding=1 width='500'>
	<tr align=middle>
		<td class='tbltext3'>
			Inserimento della Sezione correttamente effettuato
		</td>
	</tr>
</table>
<%
else
%>
<br><br>
<table border=0 cellspacing=2 cellpadding=1 width='500'>
	<tr align=middle>
		<td class='tbltext3'>
			Errore nell'inserimento. <%=sErrore%>
		</td>
	</tr>
</table>
<%
end if
%>
<br>
<br>

	<form name="frmArea" method="post" action="QGR_ModQuest.asp">
		<input type="hidden" name="id" value="<%=IdQuest%>">
	</form>

<table width=500>
	<tr>
		<td align=center>
			<a onmouseover="javascript:window.status='' ; return true" href="javascript:frmArea.submit();" class="textred">
				<b>Torna al Questionario</b>
			</a>
		</td>
	</tr>
</table>
<%
End Sub
'------------------------------------------------------------------------
'M A I N
%>
<!--#include virtual="/strutt_testa2.asp"-->
<!--#include virtual="/include/openconn.asp"-->
<!--#include virtual="/Util/DBUtil.asp"-->
<!--#include virtual="/Include/HTMLEncode.asp"-->
<!--#include virtual="/Include/SysFunction.asp"-->
<!--#include virtual="include/SetPulsanti.asp"-->
<%	
	Inizio()
	Conferma()
%>
<!--#include virtual="/include/closeconn.asp"-->
<!--#include virtual="/strutt_coda2.asp"-->
