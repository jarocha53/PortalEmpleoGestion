<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%
Sub ControlliJavaScript()
%>
<SCRIPT LANGUAGE="Javascript">
<!--#include virtual="include/ControlString.inc"-->

//////////**********//////////
function ControllaCampi(frmModRisp) {
			
	if ((document.frmModRisp.rFlTipoRisp[0].checked == false) && (document.frmModRisp.rFlTipoRisp[1].checked == false)) {
		alert("Indicare se la risposta � Chiusa o Aperta");
		document.frmModRisp.rFlTipoRisp[0].focus();
		return false
	}
	document.frmModRisp.txtRisposta.value = TRIM(document.frmModRisp.txtRisposta.value)
	if ((document.frmModRisp.rFlTipoRisp[0].checked == true) && (document.frmModRisp.txtRisposta.value == "")) {
		alert('Inserire il Testo della Risposta')
		document.frmModRisp.txtRisposta.focus()
		return false	
	}
}

</SCRIPT>
<%
End Sub
'--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++
Sub Inizio()
	dim sFunzione
	dim sTitolo
	dim sCommento
	dim bCampiObbl
	
	sFunzione = "GESTIONE QUESTIONARI DI GRADIMENTO"
	sTitolo = "MODIFICA RISPOSTE"
	sCommento = "Modifica delle Risposte<br>Questionari di Gradimento"
	bCampiObbl = false
	sHelp = "/Pgm/help/Formazione/QGradimento/QGR_ModRisposta/"	
%>
	<!--#include virtual="include/SetTestata.asp"-->
<%
End Sub
'--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++
Sub ElencoR()
	
	dim sSQL
	dim nIdDom, nIdArea, nIdQuest, nIdRisp, sTipo
	dim rsDomanda, rsRisposta

	nIdRisp	= clng(request("Id"))
	sErrore = request("txtErrore")
		
	IF sErrore <> "" then
		Errore sErrore
	ELSE
		sSQL =	"SELECT r.id_domandaiq, d.testo_domanda, d.id_areaiq, d.id_infoquest" &_
				" FROM iq_risposta r, iq_domanda d" &_
				" WHERE r.id_rispostaiq = " & nIdRisp &_
				" AND r.id_domandaiq = d.id_domandaiq"
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
		set rsDomanda = cc.execute(sSQL)
		If not rsDomanda.eof then
			nIdDom = rsDomanda("id_domandaiq")
			nIdArea = rsDomanda("id_areaiq")
			nIdQuest = rsDomanda("id_infoquest")
			sTestoDom = rsDomanda("testo_domanda")
%>
			<table width=500 border=0>
				<tr>
					<td class="tbltext3">
						Domanda: "<%=sTestoDom%>"
					</td>
				</tr>
			</table>
			<BR>
<%			
			sSQL =	"Select tipo, txt_risposta, fl_tiprisp, dt_tmst from iq_risposta" &_
					" where id_rispostaiq = " & nIdRisp
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
			set rsRisposta = CC.execute(sSQL)
			If not rsRisposta.eof then
				dtTmSt  = rsRisposta("dt_tmst")
				sTipo = rsRisposta("tipo")
				
				set rsDdati = server.CreateObject("ADODB.Recordset")
				strSQL ="SELECT id_campo_desc FROM diz_dati WHERE id_tab='IQ_RISPOSTA'" &_
						" AND id_campo='FL_TIPRISP'"
'PL-SQL * T-SQL  
STRSQL = TransformPLSQLToTSQL (STRSQL) 
				rsDdati.Open strSQL, CC, 3
					sFlTRis = rsDdati("id_campo_desc")
					aFlTRis = split(sFlTRis,"|")
				rsDdati.close
				set rsDdati = nothing 
%>
				<form method=post action="QGR_CnfModRisp.asp" onsubmit="return ControllaCampi(this);" id=frmModRisp name=frmModRisp>
				<table width=500 border=0>
					<tr>
						<td class=tbltext1 colspan="3">
							<input type=hidden name=nIdR id=nIdR value="<%=nIdRisp%>">
							<input type=hidden name=IdDom id=IdDom value="<%=nIdDom%>">
							<input type=hidden name=IdArea id=IdArea value="<%=nIdArea%>">
							<input type=hidden name=IdQuest id=IdQuest value="<%=nIdQuest%>">
							<input type=hidden name=dTmstT id=dTmstT value="<%=dtTmSt%>">
							<input type=hidden name=rTipo id=rTipo value="<%=sTipo%>">
						</td>
					</tr>
					<tr>
						<%
						for i=0 to Ubound(aFlTRis) step 2%>
						<td width="15%" class="tbltext1" align="center">
							<b><%=aFlTRis(i+1)%></b>
						</td>
						<%
						next%>
						<td class="tbltext1" align="center">
							<b>Testo Risposta</b>
						</td>
					</tr>
					<tr class="sfondocomm">
	<%					for i=0 to Ubound(aFlTRis) step 2%>
							<td align="center">
								<input type="radio" name="rFlTipoRisp" id="rFlTipoRisp" value="<%=aFlTRis(i)%>" 
								<%If rsRisposta("fl_tiprisp")=aFlTRis(i) then Response.Write "checked"%>>
							</td>
	<%					next %>
						<td align="center">
							<input type="text" name="txtRisposta" id="txtRisposta" value="<%=rsRisposta("txt_risposta")%>" size="40" maxlength="250">
						</td>
					</tr>
					<tr align=center>
						<td colspan=3>&nbsp;</td>
					</tr>
					<tr align=center>
						<td colspan=3>&nbsp;</td>
					</tr>
					<tr class=tbltext>
						<td colspan=3 align=center>
							<%
							PlsChiudi()%>
							&nbsp;&nbsp;
							<%
							PlsInvia("Conferma")%>
						</td>
					</tr>	
				</table>
				</form>
<%			End if
			rsRisposta.close
		Else
			Errore "Non e' stata trovata l'occorrenza in tabella."
		End if
		rsDomanda.close				
	END IF

End Sub
'----------------------------------------------------------------------
Sub Errore(sErrore) %>
	<br><br>
	<table border=0 cellspacing=2 cellpadding=1 width='500'>
		<tr align=middle>
			<td class='tbltext3'>
				Errore nella modifica.
				<br>
				<%=sErrore%>
			</td>
		</tr>
	</table>
	<br><br>
	<table width=500>
		<tr>
			<td align=center>
<%				PlsChiudi()%>
			</td>
		</tr>
	</table>
	<br><br>
<%
End sub
'----------------------------------------------------------------------

'M A I N
%><!--#include virtual="/util/portallib.asp"-->
<!--#include virtual="/include/openconn.asp"-->
<!--#include virtual="/include/SetPulsanti.asp"-->


<html>
	<title>
		Questionari di Gradimento - Modifica Risposta
	</title>
	<link REL="STYLESHEET" TYPE="text/css" HREF="<%=session("progetto")%>/fogliostile.css">
<%	
	ControlliJavaScript()
	Inizio()
	ElencoR()
%>
</html>
<!--#include virtual="/include/closeconn.asp"-->
