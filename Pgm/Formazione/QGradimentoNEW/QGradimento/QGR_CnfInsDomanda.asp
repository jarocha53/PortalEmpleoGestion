<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%
'--------------------------------------------------------------------------
function bloccoSucc(nIdQuest,nIdArea)
	Dim iDblocco
		
	sSql = " SELECT nvl(MAX(ID_BLOCCO),0) as bloccoMax FROM STRUTTURA_QUEST WHERE ID_INFOQUEST=" & nIdQuest &_
		" AND ID_AREAIQ = " & nIdArea
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	set blocco = cc.Execute(sSql)
	if not blocco.eof then
		iDblocco = cint(blocco("bloccoMax"))
		iDblocco = iDblocco + 1
	else
		iDblocco = 1
	end if
	
	bloccoSucc = iDblocco
	
	blocco.close()
	set blocco = nothing

end function
'--------------------------------------------------------------------------
Sub Inizio
	dim sFunzione
	dim sTitolo
	dim sCommento
	dim bCampiObbl
	
	sFunzione = "GESTIONE QUESTIONARI DI GRADIMENTO"
	sTitolo = "INSERIMENTO DOMANDE"
	sCommento = "Conferma dell'inserimento del<br>Questionari di Gradimento"
	bCampiObbl = false
%>
	<!--#include virtual="include/SetTestata.asp"-->
<%
End Sub
'------------------------------------------------------------------------
Sub Conferma()

	dim sDescD, nIdQuest, nIdArea, sTipoRisp, nNumRisp, sTipoSel
	dim sSQL,nIdBlocco

	sDescD	= strHTMLEncode(Trim(Request.Form("DescDomanda")))
	sDescD	= replace(sDescD,"'","''")
	nIdQuest = Request.Form("IdQuest")
	nIdArea	 = Request.Form("IdArea")
	nIdBlocco = Request.Form("IdBlocco")
	nIdElem = Request.Form("IdElem")
	nIdDom = Request.Form("IdDom")
	'Response.Write "nidDom = " & nIdDom & "<br>"
	'Response.End
	
	if ( nIdElem = "" ) then
		nIdBlocco = bloccoSucc(nIdQuest,nIdArea)
	end if
	
	Adesso	 ="TO_DATE('" & day(now) & "/" & _
			   month(now) & "/" & year(now) & _
			   " " & hour(now) & ":" & minute(now) & ":" & second(now) & _
			   "','DD/MM/YYYY HH24:MI:SS')"	
	
	sTipoSel = Request.Form("cmbTipoSel")
	
	CC.BeginTrans
		
	sSQL =	"INSERT INTO IQ_DOMANDA (ID_INFOQUEST, ID_AREAIQ, TESTO_DOMANDA, DT_TMST"
			If sTipoSel = "" then
				sSQL = sSQL & ")"
			else
				sSQL = sSQL & ", TIPO_SEL)"
			end if
			
			sSQL = sSQL & " VALUES (" & nIdQuest & ", " & nIdArea & ", '" & sDescD & "', " & Adesso
			
			If sTipoSel = "" then
				sSQL = sSQL & ")"
			else
				sSQL = sSQL & ", '" & sTipoSel & "')"
			end if
	sErrore = EseguiNoC(sSQL,CC)

	if sErrore="0" then
	
		sSQL =	"Select ID_DOMANDAIQ from IQ_DOMANDA where DT_TMST =" &  Adesso
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
		set RRDom  = CC.Execute(sSQL)
		iddo = RRDom("ID_DOMANDAIQ")
		if nIdElem = "" then
			'nIdBlocco = 1
			nIdElem = 0
		end if
	end if
	
	if sErrore="0" then		
		sSQL =	"INSERT INTO STRUTTURA_QUEST (ID_INFOQUEST, ID_AREAIQ, ID_BLOCCO, TIPO_ELEMENTO, ID_ELEMENTO, ID_ELEM_ORIGINE, DT_TMST) values (" & nIdQuest & "," & nIdArea & "," & nIdBlocco & ",'D'," & iddo & "," & nIdElem & "," & Adesso & ")"
		sErrore = EseguiNoC(sSQL,CC)
	end if
	if sErrore="0" then
    	 CC.CommitTrans

	%>
		<br><br>
		<table border=0 cellspacing=2 cellpadding=1 width='500'>
			<tr align=middle>
				<td class='tbltext3'>
					Inserimento della Domanda correttamente effettuato
				</td>
			</tr>
		</table>
	<%		
	else
		CC.RollbackTrans
	%>
		<br><br>
		<table border=0 cellspacing=2 cellpadding=1 width='500'>
			<tr align=middle>
				<td class='tbltext3'>
					Errore nell'inserimento. <%=sErrore%>
				</td>
			</tr>
		</table>
	<%
	end if
	%>
	<br>
<%'''9/7/2003: PASSATI VALORI TRAMITE FORM ANZICHE' QUERYSTRING %>
	<form name="frmDomanda" method="post" action="QGR_VisRisposta.asp">
		<input type="hidden" name="idq" value="<%=nIdQuest%>">
		<input type="hidden" name="ida" value="<%=nIdArea%>">
		<input type="hidden" name="idr" value="<%=iddo%>">
		<input type="hidden" name="idb" value="<%=nIdBlocco%>">
		<input type="hidden" name="ide" value="<%=iddo%>">
	</form>
<%'''FINE 9/7/2003
'Response.Write "iddo = "  & iddo &"<br>"
'response.end
%>
	<table width=500>
		<tr>
			<td align=center>
<%'''9/7/2003: PASSATI VALORI TRAMITE FORM ANZICHE' QUERYSTRING 
				'''PlsLinkRosso "QGR_VisDomanda.asp?idq=" & nIdQuest & "&ida=" & nIdArea, "Elenco delle Domande" %>
				<a onmouseover="javascript:window.status='' ; return true" href="javascript:frmDomanda.submit();" class="textred">
					<b>Inserisci Risposte</b>
				</a>
			</td>
		</tr>
	</table>
<%
End Sub
'------------------------------------------------------------------------
'M A I N
%>
<HTML>
<HEAD>
<TITLE><%=Session("TIT")%> [ <%=Session("login")%> ] </TITLE>
<LINK REL=STYLESHEET TYPE="text/css" HREF='<%=session("progetto")%>/fogliostile.css'>
</head>
<body class=sfondocentro topmargin="2" leftmargin="0">
<center>

<!--#include virtual="/include/openconn.asp"-->
<!--#include virtual="/Util/DBUtil.asp"-->
<!--#include virtual="/Include/HTMLEncode.asp"-->
<!--#include virtual="/Include/SysFunction.asp"-->
<!--#include virtual="include/SetPulsanti.asp"-->
<%	
	dim iddo
	Inizio()
	Conferma()
%>
<!--#include virtual="/include/closeconn.asp"-->
</body></html>
