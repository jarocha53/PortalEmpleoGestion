<%
Sub ControlliJavaScript()
%>
<script LANGUAGE="Javascript">
<!--#include virtual="include/ControlString.inc"-->

function inviaAree(){
	frmInsQuest.txtAree.value = TRIM(frmInsQuest.txtAree.value)
   	if (frmInsQuest.txtAree.value == "") {
		alert("Inserire il numero delle sezioni")
		frmInsQuest.txtAree.focus() 
		return
	}
	if (isNaN(frmInsQuest.txtAree.value)) {
		alert("Il numero delle sezioni deve essere numerico")
		frmInsQuest.txtAree.focus() 
		return
	}

	else{
		frmInsAree.txtDQ.value = frmInsQuest.DescQuest.value
		frmInsAree.txtIQ.value = frmInsQuest.IntQuest.value 
		frmInsAree.txtNumero.value = frmInsQuest.txtAree.value  
		frmInsAree.txtRip.value = frmInsQuest.ckRipetibile.checked 
		frmInsAree.numaree.value = 1
		frmInsAree.submit() 
	}
}

function ControllaCampi(frmInsQuest) {
	frmInsQuest.DescQuest.value = TRIM(frmInsQuest.DescQuest.value)
	if (frmInsQuest.DescQuest.value == "") {
		alert("Inserire il titolo del questionario")
		frmInsQuest.DescQuest.focus() 
		return false
	}
//alert ("frmInsAree.numaree.value "+ frmInsAree.numaree.value);
	if (frmInsAree.numaree.value == "" && frmInsQuest.txtAree.value != "" && frmInsQuest.txtAree.value != 0 ) {
		alert("Selezionare 'Aggiungi' e inserire le descrizioni delle sezioni")
		return false
	}	
	
	frmInsQuest.IntQuest.value = TRIM(frmInsQuest.IntQuest.value)
	if (frmInsQuest.IntQuest.value == "") {
		alert("Inserire la 'Descrizione  Questionario'")
		frmInsQuest.IntQuest.focus() 
		return false
	}
	
	frmInsQuest.txtAree.value = TRIM(frmInsQuest.txtAree.value)
	if (frmInsQuest.txtAree.value != "" && frmInsQuest.txtAree.value != "0") {
	
		if (isNaN(frmInsQuest.txtAree.value)) {
			alert("Il numero delle sezioni deve essere numerico")
			frmInsQuest.txtAree.focus() 
			return false
		}

		var Narea 	
		var appo
		var VALORE
		var ro = / /g
		
		Narea = frmInsQuest.txtAree.value
		for(i=0;i<Narea;i++){
		    appo = i + 1
		    VALORE =eval('frmInsQuest.DescArea' + appo + '.value')
		    VALORE = VALORE.replace( ro, "" )
		    
		    if(VALORE==""){
				alert("Inserire la descrizione della sezione numero " + appo)
				eval('frmInsQuest.DescArea' + appo + '.focus()')
			    return false
			}
		}   
	}
}

</script>
<%
End Sub
'-----------------------------------------------------------------------
Sub Inizio()
	dim sFunzione
	dim sTitolo
	dim sCommento
	dim bCampiObbl
	
	sFunzione = "GESTIONE QUESTIONARI DI GRADIMENTO"
	sTitolo = "INSERIMENTO QUESTIONARI DI GRADIMENTO"
	sCommento = "In questa pagina puoi inserire gli estremi di un nuovo questionario di gradimento. Nel caso in cui questo sia composto da sezioni, indicane il numero complessivo e premi Aggiungi, ti appariranno dei campi in cui inserire i titoli delle sezioni. Viceversa completa i campi obbligatori e premi <B>Invia</B>."
	bCampiObbl = true
	sHelp = "/Pgm/help/Formazione/QGradimento/QGR_InsQuest/"		
%>
	<!--#include virtual="include/SetTestata.asp"-->
<%
End Sub
'-----------------------------------------------------------------------
Sub Inserimento()

dim sSQL, rsTades
%>
<form method="post" action="QGR_CnfInsQuest.asp" onsubmit="return ControllaCampi(this)" id="frmInsQuest" name="frmInsQuest">
<br>	
<table border="0" width="500">
	<tr>
		<td class="tbltext1">
			<b>Titolo Questionario*</b>
		</td>
		<td>
			<input id="DescQuest" maxLength="200" size="41" name="DescQuest" class="textblacka" value="<%=Descrizione%>">
			<script language="javascript">
				frmInsQuest.DescQuest.focus();
			</script>
		</td>
	</tr>
	
	<tr>
		<td vAlign="top" class="tbltext1">
			<b>Descrizione Questionario*</b><br>
			<span class="tbltext1">- Utilizzabili 
				<b><label name="NumCaratteri" id="NumCaratteri">2000</label></b>
				caratteri -
			</span>
		</td>
		<td>
			<textarea id="IntQuest" name="IntQuest" rows="5" cols="40" class="textblacka" maxlength="2000" onKeyup="JavaScript:CheckLenTextArea(document.frmInsQuest.IntQuest,NumCaratteri,2000)"><%=Intestazione%></textarea>
		</td>
	</tr>
	
	<tr>
		<td class="tbltext1">
			<b>Ripetibile</b>
		</td>
		<td align="left">
		<% if Ripetibile= "true" then%>
				<input type="checkbox" id="ckRipetibile" name="ckRipetibile" class="textblacka" value="1" checked>
		<% else %>
			    <input type="checkbox" id="ckRipetibile" name="ckRipetibile" class="textblacka" value="1">
		<% end if %>
		</td>
	</tr>

	<tr>
		<td COLSPAN="2" ALIGN="LEFT"><hr WIDTH="92%"></td>
	</tr>	

	<tr>
		<td class="tbltext1">
			<b>Indicare il numero di sezioni<br>e selezionare &quot;aggiungi&quot;</b>
		</td>
		<td>
		<table border="0" width="250">
			<td align="left" width="20">
				<input type="text" name="txtAree" class="textblacka" size="2" maxlength="2" value="<%=Numero%>">
			</td>
			<td align="left">&nbsp;<b><a class="textred" href="javascript:inviaAree()">Aggiungi</a></b>
			</td>
		</table>
		</td>
	</tr>
	
	
<%  for i = 1 to Numero%>
		<tr>
			<td class="tbltext1" align="right">
			<b>Sezione&nbsp;<%=i%></b>
		</td>
			<td>
		<!--	    <TEXTAREA id=IntArea<%=i%> name=IntArea<%=i%> rows=2 cols=40 class=textblacka maxlength="200" size="200" ></TEXTAREA>		-->	
				<input type="text" maxLength="200" size="41" name="DescArea<%=i%>" class="textblacka">
		</td>
		</tr>
<%  next		%>	

    
	<tr>
		<td COLSPAN="2" ALIGN="LEFT"><hr WIDTH="92%"></td>
	</tr>	

	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2" align="center">
			<a onmouseover="javascript:window.status='' ; return true" href="QGR_VisQuestionario.asp">
		         <img src="<%=Session("progetto")%>/images/indietro.gif" border="0" onmouseover="javascript:window.status='' ; return true">
	        </a>
		<%
			
			PlsInvia("Conferma")
		%>
		</td>
	</tr>

</table>


</form>
<%
End Sub
'-----------------------------------------------------------------------
'M A I N
%>
<!--#include virtual="/strutt_testa2.asp"-->
<!--#include virtual="/include/openconn.asp"-->
<!--#include virtual="/include/DecCod.asp"-->
<!--#include virtual="include/SetPulsanti.asp"-->
<!--#include virtual="include/RuoloFunzionale.asp"-->
<%
if ValidateService(session("idutente"),"QGR_VisQuestionario", CC) <> "true" then 
	response.redirect "/util/error_login.asp"
end if

Session("ck_cod_ruofu")=""
%>

<%
dim Numero,Descrizione,Intestazione,Ripetibile

    Numero =Request.Form ("txtNumero")

    if Numero & "*" = "*" then
		Numero = 0 
    end if

    Descrizione = Request.Form ("txtDQ")
    Intestazione = Request.Form ("txtIQ")
    Ripetibile = Request.Form ("txtRip")
    Numaree = Request.Form ("numaree")
%>

<form method="post" action="QGR_InsQuest.asp" name="frmInsAree">
	<input type="hidden" name="txtNumero">
	<input type="hidden" name="txtDQ">
	<input type="hidden" name="txtIQ">
	<input type="hidden" name="txtRip">
	<input type="hidden" name="numaree" value="<%=Numaree%>">
</form>

<%
	ControlliJavaScript()
	Inizio()
	Inserimento()
%>
<!--#include virtual="/include/closeconn.asp"-->
<!--#include virtual="/strutt_coda2.asp"-->
