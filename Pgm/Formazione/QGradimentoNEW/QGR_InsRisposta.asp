<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%
Sub ControlliJavaScript()
%>
<SCRIPT LANGUAGE="Javascript">
<!--#include virtual="include/ControlString.inc"-->

	//////////**********//////////
	function ControllaCampi(frmRisposta) {

		if ( (frmRisposta.rTipo[0].disabled == false) && (frmRisposta.rTipo[1].disabled == false) ) {
			//if ((frmRisposta.rTipo[0].checked == false) && (frmRisposta.rTipo[1].checked == false) && (frmRisposta.rTipo[2].checked == false)) {
			if ( (frmRisposta.rTipo[0].checked == false) && (frmRisposta.rTipo[1].checked == false) ) {
				alert("Indicare la Tipologia di Risposta")
				frmRisposta.rTipo[0].focus()
				return false
			}
		}
		
		var Tipo
		if (document.frmRisposta.rTipo[0].checked == true) {
			Tipo = document.frmRisposta.rTipo[0].value
		}
		if (document.frmRisposta.rTipo[1].checked == true) {
			Tipo = document.frmRisposta.rTipo[1].value
		}
		/*if (document.frmRisposta.rTipo[2].checked == true) {
			//Tipo = document.frmRisposta.rTipo[2].value
			Tipo = ""
		}*/
		
		if (Tipo == "T") {
			frmRisposta.txtRisp.value = TRIM(frmRisposta.txtRisp.value)
			if (frmRisposta.txtRisp.value == "") {
				alert("Compilare il campo Testo Risposta")
				frmRisposta.txtRisp.focus()
				return false
			}
		}
		else if (Tipo == "V") {
			frmRisposta.VMin.value = TRIM(frmRisposta.VMin.value)
			frmRisposta.VMax.value = TRIM(frmRisposta.VMax.value)
			if ((frmRisposta.VMin.value == "") && (frmRisposta.VMax.value == "")) {
				alert("Compilare i campi Valore Min e Valore Max")
				frmRisposta.VMin.focus()
				return false
			}
			if ( (frmRisposta.VMin.value == "") || (frmRisposta.VMax.value == "") ) {
				alert("Inserire tutti i Valori")
				frmRisposta.VMin.focus()
				return false
			}
			if ( (frmRisposta.VMin.value != "") && (frmRisposta.VMax.value != "") ) {
				if ( (!Numerico(frmRisposta.VMin)) || (!Numerico(frmRisposta.VMax)) ) {
					return false;
				}
				if (frmRisposta.VMin.value >= frmRisposta.VMax.value) {
					alert("Valore Min non pu� essere maggiore o uguale a Valore Max")
					frmRisposta.VMin.focus()
					return false
				}
			}
		}
	}		
	
	//////////**********//////////
	function Numerico(campo) {			
		var dim=campo.size;
		var anyString = campo.value;

		for ( var i=0; i<=anyString.length-1; i++ ) {
			if ( (anyString.charAt(i) >= "0") && (anyString.charAt(i) <= "9") ) 
			{
			}
			else {
				alert("Attenzione: valore immesso non numerico!");
				campo.focus();
				return(false);
			}
		}
		return true;
	}
	
	//////////**********//////////
	function Reload(sValue){
		document.frmReload.txtRadioValue.value = sValue
		document.frmReload.submit()
	}
	
</SCRIPT>
<%
End Sub
'--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++
Sub Inizio()
	dim sFunzione
	dim sTitolo
	dim sCommento
	dim bCampiObbl
	
	sFunzione = "GESTIONE QUESTIONARI DI GRADIMENTO"
	sTitolo = "INSERIMENTO RISPOSTE"
	sCommento = "Inserimento delle Risposte<br>Questionari di Gradimento"
	bCampiObbl = false
	sHelp = "/Pgm/help/Formazione/QGradimento/QGR_InsRisposta/"	
%>
	<!--#include virtual="include/SetTestata.asp"-->
<%
End Sub
'--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++
Sub InsR()

dim sSQL, rsDomanda, rsCont
dim nIdDom, nIdArea, nIdQuest 
dim sRadioV, sTipR_D, nNumRisp, nRispIns

nIdDom		= clng(request("idd"))
nIdArea		= clng(request("ida"))
nIdQuest	= clng(request("idq"))

'==>sRadioV la prima volta che entro � vuoto; 
'	i valori che pu� assumere sono T(testo), V(valore), TL(testo libero);
'	in tabella registro per� solo T, V oppure null.	
sRadioV		= Request.Form("txtRadioValue")		

sSQL = "Select Testo_Domanda, Tipo_Risposta, Num_Risposta from iq_domanda where id_domandaiq = " & nIdDom
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
set rsDomanda = cc.execute(sSQL)
	If not rsDomanda.eof then %>
		<table width=500 border=0>
			<tr>
				<td class="tbltext3">
					Domanda: "<%=rsDomanda("testo_domanda")%>"
				</td>
			</tr>
		</table>
		<BR>
<%		'==>sTipR_D pu� assumere valore A(aperta),C(chiusa),S(semi-aperta)
		sTipR_D		= rsDomanda("Tipo_Risposta")
		nNumRisp	= clng(rsDomanda("Num_Risposta"))
	End if
rsDomanda.close

If sTipR_D = "A" then		'==> con questa impostaz. posso vedere il testo informativo, 
	sRadioV = "TL"			'	 relativo alle risposte libere, anche se � la prima volta
End if						'	 che entro nella pagina.

If sTipR_D = "S" then
	sRadioV = "T"
	
	sSql = "Select count(ID_RISPOSTAIQ) as Somma from IQ_RISPOSTA where ID_DOMANDAIQ = " & nIdDom
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	set rsCont = cc.execute(sSql)
		nRispIns = clng(rsCont("Somma"))
	rsCont.close
	
	if nRispIns = (nNumRisp-1) then
		sRispAltern = "si"
	else
		sRispAltern = "no"
	end if
End if
%>

<form id="frmReload" name="frmReload" action="QGR_InsRisposta.asp" method="post">
	<input type="hidden" name="txtRadioValue" id="txtRadioValue" value>
	<input type=hidden name=Idd id=Idd value="<%=nIdDom%>">
	<input type=hidden name=Ida id=Ida value="<%=nIdArea%>">
	<input type=hidden name=Idq id=Idq value="<%=nIdQuest%>">	
</form>

<form method=post action="QGR_CnfInsRisp.asp" onsubmit="return ControllaCampi(this)" id=frmRisposta name=frmRisposta>
<table width=500 border=0>
	<tr>
		<td class="tbltext1" width="35%">
			<b>Tipologia Risposta</b>
			<input type=hidden name=nIdDom id=IdDom value="<%=nIdDom%>">
			<input type=hidden name=nIdArea id=IdArea value="<%=nIdArea%>">
			<input type=hidden name=nIdQuest id=IdQuest value="<%=nIdQuest%>">
		</td>
		<td class="textblacka">
			Testo
				<input type=radio id="rTipo" name="rTipo" value="T" onclick="JavaScript:Reload(this.value);" <%If sTipR_D = "A" then Response.Write "disabled"%> <%If sRadioV = "T" then Response.Write "checked"%>>&nbsp;&nbsp;&nbsp;
			Valore
				<input type=radio id="rTipo" name="rTipo" value="V" onclick="JavaScript:Reload(this.value);" <%If sTipR_D = "A" Or sTipR_D = "S" then Response.Write "disabled"%> <%If sRadioV = "V" then Response.Write "checked"%>>&nbsp;&nbsp;&nbsp;
			<!--Libera-->
				<input type=radio id="rTipo" name="rTipo" value="TL" style="visibility:hidden" <%If sTipR_D = "A" then Response.Write "checked"%>>
				<!--input type=radio id="rTipo" name="rTipo" value="L" onclick="JavaScript:Reload(this.value);" <%'If sTipR_D = "A" then Response.Write "checked" else Response.Write "disabled"%>-->
		</td>
	</tr>
<%
	Select case sRadioV
		case "T" %>
			<tr>
				<td class=tbltext1>
					<b>Testo Risposta</b>
				</td>
				<td>
<%					If sRispAltern = "si" then %>
						<input type=text maxlength=250 id=txtRisp name=txtRisp class="textblacka" value="altro:">
						&nbsp;
						<textarea id=textarea1 name=textarea1 rows="1" cols="23" class="textblacka" disabled>sezione riservata all'utente</textarea>
				</td>
			</tr>
			<tr>
				<td colspan="2" class=tbltext1 align="center">
					<br>
					L'ultima risposta a una domanda <b>'Semi-aperta'</b> rappresenta l'alternativa alle risposte gia' definite in precedenza per la stessa domanda.
				</td>
			</tr>
<%					Else%>
						<input type=text maxlength=250 id=txtRisp name=txtRisp class="textblacka">
				</td>
			</tr>
<%					End if
		case "V" %>
			<tr>
				<td class=tbltext1>
					<b>Valore Min</b>
				</td>
				<td>
					<input type=text maxlength=2 size="10" id=VMin name=VMin class="textblacka">
				</td>
			</tr>
			<tr>
				<td class=tbltext1>
					<b>Valore Max</b>
				</td>
				<td>
					<input type=text maxlength=2 size="10" id=VMax name=VMax class="textblacka">
				</td>
			</tr>
	<%
		case "TL" %>
			<tr>
				<td colspan="2">&nbsp;</td>
			</tr>
			<tr>
				<td colspan="2" class=tbltext1 align="center">
					Questa domanda prevede una risposta <b>'Libera'</b> che consiste nell'inserimento di testo libero da parte dell'utente e non richiede altre impostazioni in questa fase.
					<br><br>
					<b>Salvare l'inserimento</b>.
				</td>
			</tr>
	<%
	End select %>
	<tr colspan=2 align=center><td>&nbsp;</td></tr>
	<tr class=tbltext>
		<td colspan=2 align=center>
			<%
			PlsIndietro()
			PlsInvia("Conferma")
			%>
		</td>
	</tr>	
</table>
</form>
<%
End Sub
'----------------------------------------------------------------------
'M A I N
%>
<!--#include virtual="/strutt_testa2.asp"-->
<!--#include virtual="/include/openconn.asp"-->
<!--#include virtual="include/SetPulsanti.asp"-->
<%	
	ControlliJavaScript()
	Inizio()
	InsR()
%>
<!--#include virtual="/include/closeconn.asp"-->
<!--#include virtual="/strutt_coda2.asp"-->
