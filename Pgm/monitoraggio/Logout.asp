<%@ LANGUAGE=VBSCRIPT %>

<% 
   session.abandon()
   Application.lock()
   if(not isNull(session) and not isEmpty(session)) then
	   Application(Cstr(session("ente"))) = ""
   end if
   Application.unlock()
   Server.transfer("Login.html")
%>