<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%@ LANGUAGE=VBSCRIPT %>
<!-- #include file="db/connessione.asp" -->

<%
	codici = split(Request.Form("codice"), ",")
	query = "SELECT P.ID_PERSONA, P.NOME, P.COGNOME, P.COD_FISC " & _
	        "FROM PERSONA P " & _
			"WHERE P.ID_PERSONA NOT IN (SELECT ID_PERSONA FROM RICH_VAR_ANAG WHERE TIPO_OPERAZIONE = 'C') AND " & _
			"P.ID_PERSONA IN ("
	for i = lbound(codici) to ubound(codici) 
		query = query & "'"&codici(i)&"'"
		if (i < ubound(codici)) then 
			query = query & ", "
		else
			query = query & ")"
		end if
	next
	
	Set res = Server.createObject("ADODB.Recordset")
'PL-SQL * T-SQL  
QUERY = TransformPLSQLToTSQL (QUERY) 
	Set res = connection.execute(query)
	
	if res.EOF then
		' FIXME -> in questo caso non ci sono LSU da segnalare
		Server.transfer("MonitoraggioMain.asp") 
	end if
%>


<html>
	<head>
		<style type="text/css">
		<!--
			 td {
			 		font-family: tahoma;
					font-size: 12;
					font-weight: bold;
			 }
			 td.valore {
			 		font-family: tahoma;
					font-size: 14;
					font-weight: bolder
			 }
			 td.header {
			 		font-family: tahoma;
					font-size: 20;
					font-weight: bolder;
					text-align: center;
			 }
			 td.codfis {
			 		font-family: courier new;
					font-size: 20;
					font-weight: bolder;
					text-align: center;
			 }
		-->
		</style>
	</head>
	<body>
		<script  language="JavaScript" scr="utils/utility.js">
		
			function conferma() {
				window.location="MonitoraggioMain.asp"
			}
			
		</script>
		
		<table width="100%" height="100%">
			<tr>
		      <td class="header">
				Per segnalare i seguenti lsu premere conferma
			  </td>
			</tr>
			<tr><td valign="middle" align="center">
				<form>
				<table style="border: 1 solid black; border-right: 2 solid black" width="70%" border="0" align="center" valign="middle" cellpadding="0" cellspacing="0">
				
					<%			
						do while not res.EOF 
					%>
						<input type="hidden" name="codice" value="<%=res("id_persona")%>">
						<tr>
							<td colspan="2">
								<table style="border-right: 1 solid black" border="0" cellpadding="8" bgcolor="#dddddd" width="100%" height="100%">
									<tr>
										<td valign="middle" width="100">
											Nome
										</td>
										<td class="valore" valign="middle" width="150">
											<%=res("nome")%>
										</td>
										<td valign="middle" width="100">
											Cognome
										</td>
										<td class="valore" valign="middle" width="150">
											<%=res("cognome")%>
										</td>
									<td valign="middle" width="100">
										Codice Fiscale
									</td>
									<td class="valore" colspan="3" valign="middle">
										<%=res("cod_fisc")%>
									</td>
								  </tr>	
								  <tr>
									<td align="left" colspan="2">
										<input type="checkbox" name="escludi">Escludi dall'operazione</checkbox>
									</td>
									</tr>		
								</table>
							</td>
						</tr>
						<tr><td height="2" bgcolor="black" colspan="2"></td></tr>
					<%
						res.movenext
						loop
					%>
					<tr>
						<td align="center"> 
							<a  href="javascript:conferma()"><img src="images/Ok.gif" alt="Conferma" border="0"/></a>
						</td>
						<td align="center">
							<a  href="javascript:annulla()"><img src="images/NoWay.gif" alt="Annulla" border="0"/></a>
						</td>
					</tr>
				</table>
			</form>
		</td></tr></table>
	</body>
</html>

<%
	res.close
	connection.close
%>
