<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%@ LANGUAGE=VBSCRIPT %>
<!-- #include file="db/gestionelsu.asp" -->
<%
	'''
	'' --------------->   CONTROLLO AUTENTICAZIONE UTENTE
	'
	if (Session("ente") = null or Session("ente") = "") then 
		Response.redirect("Login.html")
		return
	end if
%>

<%
	'stampaStato()
	Set connection = Server.createObject("ADODB.Connection")
	connection.open(session("connectString"))

	tipoUtente = Session("tipoUtente")
	Dim filter
	action = Request.queryString("action")
	filter = Request.queryString("filter")
	if(filter = null or filter = "") then
	   filter = "cognome, nome"
	end if

	'####
	'###  Quando si clicca su una lettera avviene il post della form quindi i parametri
	'##   groupby
	'#    arrivano da li.
	'##################################################################################

	Dim mode, state
	mode = Request.Form("groupby") ' FORM quando si clicca sulle lettere
	if (mode = null or mode = "") then
		mode = Request.queryString("groupby") ' QUERY quando si clicca sullo stato
		if(mode = null or mode = "") then
			mode = "letter"
		end if
	end if

	'Response.write("MODALITA': " & mode & "<br>")

	if (mode = "letter") then
		'Response.write("begin letter<br>")
	   ' � necessario salvare in sessione gli LSU eventualmente selezionati dalla schermata
	   ' precendente in modo da poterli caricare tutti una volta selezionata l'operazione

		actualLetter = Request.Form("letteraCliccata")
		previousLetter = session("letter")
		if(isNull(previousLetter) or isEmpty(previousLetter)) then
			previousLetter = "AZ"
			rimuoviLettere()
			rimuoviAZ()
			'Response.write("prev<br>")
		end if
		if(isNull(actualLetter) or isEmpty(actualLetter)) then
			rimuoviLettere()
			rimuoviAZ()
			actualLetter = "AZ"
			'Response.write("act<br>")
		end if
		session("letter") = actualLetter

		if(Request.Form("dopoOperazione") <> "si") then
		    'Response.write("PRIMA " & previousLetter & " DOPO " & actualLetter & "<br>")
			' metto in sessione un array di codici di lsu selezionati per la lettera scelta
			listaco = Request.Form("codice")
			if(not isNull(listaco) and not isEmpty(listaco) and not trim(listaco) = "") then
				codiciscelti = split(listaco, ",")
				if (not isNull(previousLetter) and not isEmpty(previousLetter)) then
					' ATTENZIONE: si deve usare trim per prenderli dall'array !!!
					session(previousLetter) = codiciscelti
					'Response.write("inserito in " & previousLetter & ": " & Request.Form("codice") & "<br>")
				end if
			end if

			' SE E' AZ ALLORA CANCELLO TUTTO DALLA SESSIONE
			if(actualLetter = "AZ") then
				rimuoviLettere()
				'Response.write("act � AZ => rimosso lettere<br>")
			else
				rimuoviAZ()
				'Response.write("act � lettere => rimosso AZ<br>")
			end if
		end if

		session.contents.remove("state")

		' ################################################################
		' ATTENZIONE: se actualLetter era "AZ" lo rimetto a stringa vuota
		' in modo che la select con like vada a buon fine
		' #############################
		if(actualLetter = "AZ") then actualLetter = "" end if
		' #################################################################

		'Response.write("query letter<br>")
		query = "SELECT DISTINCT P.ID_PERSONA, P.NOME, P.COGNOME, P.COD_FISC, " & _		
				"PE.COD_MANSIONE_LSU, PE.COD_SETTORE_LSU, PE.FL_STATO_PERSONA STATUS, PE.CERTIFICATO " & _
				"FROM   PERSONA P, PERS_ENTE PE " & _
				"WHERE  P.COGNOME LIKE '" & actualLetter & "%' AND " & _
				"       PE.ID_IMPRESA = " & session("ente") & " AND " & _
				"       (PE.FL_STATO_PERSONA IN ('A', 'S', 'E') "
		if (tipoUtente = "operatore") then
			query = query & " OR (PE.FL_STATO_PERSONA = 'U' AND PE.CERTIFICATO = 'N')) AND "
		else
			query = query & " OR (PE.FL_STATO_PERSONA = 'U' AND PE.CERTIFICATO = 'S')) AND "
		end if
		query = query & "       PE.ID_PERSONA = P.ID_PERSONA "
		if (tipoUtente = "supervisore") then
			query = query & " AND PE.CERTIFICATO = 'S' "
		end if
		query = query &	"ORDER BY " & filter
	else
		'=================================================================
		'
		'   modalit�                                           STATO
		'
		'=================================================================
		
		state = Request.Form("letteraCliccata")
		actualLetter = session("letter")
	'	session("state") = state
		'Response.write("begin state<br>state: " & state & "<br>")
		if(actualLetter = "AZ") then
			actualLetter = ""
		end if
		query = "SELECT DISTINCT P.ID_PERSONA, P.NOME, P.COGNOME, P.COD_FISC, " & _		
				"PE.COD_MANSIONE_LSU, PE.COD_SETTORE_LSU, PE.FL_STATO_PERSONA STATUS, PE.CERTIFICATO " & _
				"FROM   PERSONA P, PERS_ENTE PE " & _
				"WHERE  PE.ID_IMPRESA = " & session("ente") & " AND PE.ID_PERSONA = P.ID_PERSONA "
		if (state <> "") then 
			query = query & " AND PE.FL_STATO_PERSONA = '" & state & "' "
		end if
		if (actualLetter <> "") then
		   query = query & "AND P.COGNOME LIKE '" & actualLetter & "%'"
		end if
		if (tipoUtente = "supervisore") then
			query = query & "AND PE.CERTIFICATO = 'S' "
		elseif (state = "U") then
			   query = query & "AND PE.CERTIFICATO = 'N' "
		end if
		query = query &	"ORDER BY " & filter
	end if

	'Response.write("QUERY: " & query & "<br>")

	'Set res = Server.createObject("ADODB.Recordset")
'PL-SQL * T-SQL  
QUERY = TransformPLSQLToTSQL (QUERY) 
	Set res = connection.execute(query)

	'stampaStato()	
%>

<html>
	<head>
		<style type="text/css">
			 td {
			 		font-family: tahoma;
					font-size: 12;
					font-weight: bold;
			 }
			 
			 td.cfis {
			 		font-family: arial;
					font-size: 12;
					font-weight: bold;
			 }
			 
			 a.miniBottone {
			 		font-family: tahoma;
					font-size: 8;
					font-weight: bold;
					text-decoration: none;
					vertical-align: middle;
					border: 1 solid black;
					color: black;
			 }
		</style>
	</head>
	<body>
		<script language="javascript">
		
		     function modificaSingolo(codice, stato){
			      if (stato == "U") {
				  	 checks = document.forms[0].codice
				  	 for (k = 1; k < checks.length; k++) {
					 	 if (checks[k].value == codice) {
						 	checks[k].checked = true
						 } else {
						   checks[k].checked = false
						 } 
					 }
					 parent.frameLettere.location = "Blank.asp"
					 parent.headerInterno.location = "HeaderTabella.asp?mode=uscitaSingolo"
					 parent.barraBottoni.location = "Blank.asp"
					 document.FormDati.target = "mainInterno"
					 document.location = "UscitaSingolo.asp?codice="+codice
				  } else if (stato == "S" || stato == "A"){
				  	checks = document.forms[0].codice
				  	 for (k = 1; k < checks.length; k++) {
					 	 if (checks[k].value == codice) {
						 	checks[k].checked = true
						 } else {
						   checks[k].checked = false
						 } 
					 }
					 parent.frameLettere.location = "Blank.asp"
					 parent.headerInterno.location = "HeaderTabella.asp?mode=sospensioneSingolo"
					 parent.barraBottoni.location = "Blank.asp"
					 document.FormDati.target = "mainInterno"
					 document.location = "SospensioneSingolo.asp?codice="+codice
				  } else if (stato == "E")  {
				  	checks = document.forms[0].codice
				  	for (k = 1; k < checks.length; k++) {
					 	 if (checks[k].value == codice) {
						 	checks[k].checked = true
						 } else {
						   checks[k].checked = false
						 } 
					 }
					 parent.frameLettere.location = "Blank.asp"
					 parent.headerInterno.location = "HeaderTabella.asp?mode=esclusioneSingolo"
					 parent.barraBottoni.location = "Blank.asp"
					 document.FormDati.target = "mainInterno"
					 document.location = "EsclusioneSingolo.asp?codice="+codice
				}
			 }
		</script>
		<form name="FormDati" method="post" action="">
		<%
		if(actualLetter = "") then
			actualLetter = "AZ"
		end if
		%>
		<input type="hidden" name="letteraCliccata" value="<%=actualLetter%>">
		<input type="hidden" name="groupby" value="">
		<!--
		
		################################################
		La letteraCliccata � SEMPRE:
		- la lettera che filtra la lista che sta qui sotto
		- lo stato da filtrare selezionato
		################################################
		
		-->
		<input type="hidden" name="count" value="<%=contaLSUInSessione()%>">
		<!-- perch� siano sempre array -->
		<input type="hidden" name="codice" value="-1" >
		<input type="hidden" name="nome" >
		<input type="hidden" name="cognome" >
		<input type="hidden" name="cdfiscale" >
		<input type="hidden" name="mansione" >
		<input type="hidden" name="settore" >
		<table border="0" cellpadding="1" style="border-left: 1 solid black; border-right: 1 solid black" width="100%" cellspacing="0">

		<%
			Do while NOT res.eof
				codice = res("id_persona")
				nome = res("nome")
				cognome = res("cognome")
				cdfiscale = res("cod_fisc")
				settore = res("cod_settore_lsu")
				mansione = res("cod_mansione_lsu")
				stato = res("status")
				certificato = res("certificato")
				if (stato = "E") then
				   escluso = 1
				else 
				   escluso = 0
				end if
				Dim colore
				if escluso = 0 then 
				   colore = "white"
				else 
					 colore = "#999999"
				end if
				' CONTROLLO SE SELEZIONATO ##############################
				Dim selezionato
				selezionato = ""
				Dim tmparray
				if(actualLetter <> "") then
					tmparray = session(actualLetter)
				else
					tmparray = session("AZ")
				end if
				if(not isNull(tmparray) and not isEmpty(tmparray) and isArray(tmparray)) then
					For Each ll in tmparray
						if(StrComp(trim(ll), trim(codice), 1) = 0) then
							selezionato = "checked"
						end if
					next
				end if
				' #######################################################
		%>

			<tr>
				<td width="20" height="20" align="center" bgcolor="<%=colore%>">
					<% select case stato
					          case "A"
							  	  if (certificato = "S") then %>
					   			   	  <img  src="images/Attivo.gif" alt"Attivo">
								   <% else %>
								   	  <img  src="images/AttivoAnim.gif" alt"Attivo">
								   <% end if %> 
					<%        case "S" 
							  	   if (certificato = "S") then %>
					   			   	  <img  src="images/Sospeso.gif" alt"Sospeso">
								   <% else %>
								   	  <img  src="images/SospesoAnim.gif" alt"Sospeso">
								   <% end if %>
					<%        case "U"
							  	   if (certificato = "S") then %>
					   			   	  <img  src="images/Uscito.gif" alt"In uscita">
								   <% else %>
								   	  <img  src="images/UscitoAnim.gif" alt"In uscita">
								   <% end if %>
					<%        case "E"
							  	   if (certificato = "N") then %>
					   			   	  <img  src="images/Escluso.gif" alt"Sconosciuto">
								   <% else %>
								   	  &nbsp;
								   <% end if %>
					<%  end select %>
				</td>
				<td width="10" bgcolor="<%=colore%>">
				   <% if certificato = "S" then %>
				   
				   <% else %>
				   		<a class="miniBottone" href="javascript:modificaSingolo('<%=codice%>', '<%=stato%>')">...</a>
				   <%end if%>
				</td>
				<%if (escluso = 0 and not (tipoUtente = "supervisore")) then %>
					<td width="20" align="left" bgcolor="<%=colore%>">
						<input type="checkbox" value="<%=codice%>" name="codice" <%=selezionato%>>
					</td>
				<% else %>
					<td width="20" align="left" bgcolor="<%=colore%>">&nbsp;</td>
					<input type="hidden" name="codice" value="-1">
				<% end if %>
				<td width="180" align="left" bgcolor="<%=colore%>"><%=cognome%></td>
				<td width="180" align="left" bgcolor="<%=colore%>"><%=nome%></td>
				<td width="150" class="cfis" bgcolor="<%=colore%>" align="left"><%=Ucase(cdfiscale)%></td>
				<td width="150" align="left" bgcolor="<%=colore%>"><%=mansione%></td>
				<td width="150" align="left" bgcolor="<%=colore%>"><%=settore%></td>
			</tr>
			<tr>
			   <td colspan="8" height="1" bgcolor="black"></td>
			</tr>

		<%
		    res.Movenext
			Loop
		res.Close() 
		Set res = nothing
		connection.close()
		Set connection = nothing
		%>

		</table>
		</form>
	</body>
</html>
