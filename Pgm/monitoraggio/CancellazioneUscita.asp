<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%@ LANGUAGE=VBSCRIPT %>

<!-- #include file="adovbs.inc" -->

<%
	'''
	'' --------------->   CONTROLLO AUTENTICAZIONE UTENTE
	'
	if (Session("ente") = null or Session("ente") = "") then 
		Response.redirect("Login.html")
		return
	end if
%>
<%
   
		Set connection = Server.createObject("ADODB.Connection")
			connection.open(session("connectString"))
			
		Set resStato = Server.createObject("ADODB.Recordset")
		Set resCancellazione = Server.createObject("ADODB.Recordset")
		Set resUpdate = Server.createObject("ADODB.Recordset")
		
	   codici = split(Request.Form("codice"), ",")
	   codice = codici(1)
	   
	   ' cancellazione dell'operazione da AREA_LAVORO
	   queryCancellazione = "SELECT ID_PERSONA, ID_IMPRESA, ID_SEDE, PROGRESSIVO, TIPO_OPERAZIONE FROM AREA_LAVORO WHERE ID_PERSONA = " & codice & _
	                        " AND PROGRESSIVO = (SELECT MAX(PROGRESSIVO) FROM AREA_LAVORO WHERE ID_PERSONA = " & codice &")"
		resCancellazione.open queryCancellazione, connection, adOpenDynamic, adLockOptimistic
		if (not resCancellazione.EOF) then
			resCancellazione.delete()
			resCancellazione.update()
		end if
		resCancellazione.close()
		
		' cancellazione del record relativo da TMP_USCITA
		queryCancellazione = "SELECT ID_PERSONA, ID_IMPRESA, ID_SEDE, PROGRESSIVO, DT_USCITA, CAUSALE_USCITA FROM TMP_USCITA WHERE ID_PERSONA = " & codice & _
	                        " AND PROGRESSIVO = (SELECT MAX(PROGRESSIVO) FROM TMP_USCITA WHERE ID_PERSONA = " & codice &")"
		resCancellazione.open queryCancellazione, connection, adOpenDynamic, adLockOptimistic
		if (not resCancellazione.EOF) then
			resCancellazione.delete adAffectCurrent
			resCancellazione.update()
		end if
		resCancellazione.close()
		
		
		Dim progressivo
			progressivo = 0
		Dim stato
			stato = "A"
		Dim certificato
			certificato = "N"
			
		'ripristino dello stato precedente all'operazione
		queryStato = "SELECT MAX(PROGRESSIVO) PROGRESSIVO FROM AREA_LAVORO WHERE ID_PERSONA = " & codice						
	   	resStato.open queryStato, connection
		if (not resStato.EOF) then
			progressivo = resStato("progressivo")
			if (not isNull(progressivo)) then
				' lo stato da ripristinare � relativo ad una operazione non certificata contenuta in AREA_LAVORO
				queryUpdate = "SELECT TIPO_OPERAZIONE FROM AREA_LAVORO WHERE ID_PERSONA = " & codice & " AND PROGRESSIVO = " & progressivo
				resUpdate.open queryUpdate, connection
				if (not resUpdate.EOF) then
					operazione = resUpdate("tipo_operazione")
					select case operazione:
						case "S":
							stato = "S"
						case "R":
							stato = "A"
						case "U":
							stato = "U"
						case "E":
							stato = "E"
					end select
				end if
				resUpdate.close()
				certificato = "N"
			else
				' lo stato da ripristinare � l'ultimo stato certificato per l'LSU
				queryUpdate = "SELECT FL_STATO_PERSONA FROM STATO_CERTIFICATO WHERE ID_PERSONA = " & codice & _
				              " AND DT_RIFERIMENTO = (SELECT MAX(DT_RIFERIMENTO) FROM STATO_CERTIFICATO WHERE ID_PERSONA = "& codice & ")"
				resUpdate.open queryUpdate, connection
				if (not resUpdate.EOF) then
					stato = resUpdate("fl_stato_persona")
				end if
				resUpdate.close()
				certificato = "S"
			end if
		end if
		resStato.close()
		
		queryStato = "SELECT FL_STATO_PERSONA, CERTIFICATO FROM PERS_ENTE WHERE ID_PERSONA = " & codice & " AND ID_IMPRESA = " & session("ente")
		resStato.open queryStato, connection, adOpenDynamic, adLockOptimistic
		if (not resStato.EOF) then
			resStato("fl_stato_persona") = stato
			resStato("certificato") = certificato
			resStato.update()
		end if
	    resStato.close()
	   
	   Set resStato = nothing
	   Set resUpdate = nothing
	   Set resCancellazione = nothing
	   
	   connection.close()
	   Set connection = nothing
	   
   Server.transfer("Visualizzazione.asp")
%>
