<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%@ LANGUAGE=VBSCRIPT %>

<% response.expires = -1500 %>
<!-- #include file="db/gestionelsu.asp" -->
<!-- #include file="adovbs.inc" -->
<%
	'''
	'' --------------->   CONTROLLO AUTENTICAZIONE UTENTE
	'
	if (Session("ente") = null or Session("ente") = "") then 
		Response.redirect("Login.html")
		return
	end if
%>
<%	
	Set connection = Server.createObject("ADODB.Connection")
		connection.open(session("connectString"))

	Dim codici
	actualLetter = Request.Form("letteraCliccata") ' LA LETTERA CHE FILTRAVA
	                                               ' QUANDO E' STATO PREMUTO IL
												   ' TASTO FUNZIONE
	
	codici = split(Request.Form("codice"), ",")
	if(actualLetter = "AZ") then
		' NON SAREBBE NECESSARIO perch� se actual � AZ
		' allora la schermata che si vede � quella AZ
		' quindi i dati li prendiamo dalla form direttamente
		tmparray = session("AZ")
		if(not isNull(tmparray) and not isEmpty(tmparray) and isArray(tmparray)) then
			indice = UBound(codici)
			ReDim Preserve codici(indice + UBound(tmparray))
			for k = indice to UBound(codici)
				codici(k) = tmparray(k - indice)
			next
		end if
	else
		for each le in LetterKeys
			tmparray = session(le)
			if(not isNull(tmparray) and not isEmpty(tmparray) and isArray(tmparray)) then
				indice = UBound(codici)
				ReDim Preserve codici(indice + UBound(tmparray))
				for k = indice + 1  to UBound(codici)
					codici(k) = tmparray(k - indice)
				next
			end if
		next
	end if

	'codici = split(Request.Form("codice"), ",")
	
	'IPOTESI: possono essere riattivati solo gli LSU sospesi
	query = "SELECT DISTINCT P.ID_PERSONA, P.NOME, P.COGNOME, P.COD_FISC " & _
	        "FROM PERSONA P, PERS_ENTE PE " & _
			"WHERE P.ID_PERSONA = PE.ID_PERSONA AND PE.FL_STATO_PERSONA = 'S' AND " & _
			"P.ID_PERSONA IN ("
			
	for i = lbound(codici) to ubound(codici) 
		query = query &codici(i)
		if (i < ubound(codici)) then 
			query = query & ", "
		else
			query = query & ")"
		end if
	next
	
	Set res = Server.createObject("ADODB.Recordset")
'PL-SQL * T-SQL  
QUERY = TransformPLSQLToTSQL (QUERY) 
	Set res = connection.execute(query)
	
	if res.EOF then
		' FIXME nessuno degli LSU selezionati pu� essere sospeso
		Session("messaggioErrore") = "Operazione non disponibile per nessuno degli LSU selezionati"
		Server.transfer("Errore.asp")
	end if
%>
<html>
<style type="text/css">
<!--
			 td {
			 		font-family: tahoma;
					font-size: 10;
					font-weight: bold;
			 }
			 
			 td.cfis {
			 		font-family: tahoma;
					font-size: 10;
			 }
			 
			 select {
			        border: 1 solid black;
					width: 120;
					font-family: tahoma;
					font-size: 10;
					font-weight: bold;
					padding: 2;
					background: white;
					margin: 0;
			 }
			 
			 input {
					width: 120;
					font-family: tahoma;
					font-size: 10;
					font-weight: bold;
					padding: 2;
					background: white;
					margin: 0;
			 }
-->
</style>
	<head>
	</head>
	<body bgcolor="white" leftmargin="0" rightmargin="0" topmargin="0">
	    <script src="utils/utility.js"></script>
		<form method="post" action="SalvataggioRiattivazione.asp">
		<table border="0" cellpadding="2" style="border-left: 1 solid black; border-right: 1 solid black" width="100%" cellspacing="0" cellpadding="0">
		
		<%
		'il campo dopoOperazione viene messo a 'no' solo dalle funzioni
		'javascript per la selezione dei filtri. Se vale 'si' la sessione
		'viene ripulita al caricamento di questa pagina. Questo evita il
		'problema della propagazione dei dati della form.
		%>
		<input type="hidden" name="dopoOperazione" value="si"/>
		
		
			<!-- per fare in modo che sia sempre un array -->
				<input type="hidden" name="codice" value="0">
				<input type="hidden" name="inizioSospensione" value="0">
				<input type="hidden" name="lista" value="0">
				<input type="hidden" name="escludi" value="">
		<%
			Do while NOT res.eof
			querySosp = "SELECT DT_INIZIO_SOSP, FL_MATERNITA FROM PERS_SOSP PS, PERS_ENTE PE " & _
			            "	WHERE PE.CERTIFICATO = 'S' AND PE.ID_PERSONA = PS.ID_PERSONA AND PS.ID_PERSONA = " & res("id_persona")  & " " & _
						"UNION " & _
						"SELECT DT_INIZIO_SOSP, FL_MATERNITA FROM PERS_ENTE PE, TMP_PERS_SOSP PS " & _
						"	WHERE PE.CERTIFICATO = 'N' AND PE.ID_PERSONA = PS.ID_PERSONA AND " & _
						"		  PS.PROGRESSIVO = (SELECT MAX(PROGRESSIVO) FROM TMP_PERS_SOSP WHERE ID_PERSONA = " & res("id_persona") & ") AND PE.ID_PERSONA = " & res("id_persona")
'PL-SQL * T-SQL  
'QUERYSOSP = TransformPLSQLToTSQL (QUERYSOSP) 
			resSosp = connection.execute(querySosp)
			
		%>
			<tr>
			    <input type="hidden" name="codice" value="<%=res("id_persona")%>">
				<input type="hidden" name="inizioSospensione" value="<%=resSosp("dt_inizio_sosp")%>">
				<input type="hidden" name="lista" value="1">
				<td width="10" align="left">
				   <input style="width: 14; height: 14; margin: 0" type="checkbox" name="escludi" checked>
				</td>
				<td style="min-width: 120" align="left"><%=res("cognome")%></td>
				<td style="min-width: 120" align="left"><%=res("nome")%></td>
				<td style="min-width: 120" align="left"><%=res("cod_fisc")%></td>
				<td style="min-width: 120" align="left"><%=resSosp("dt_inizio_sosp")%></td>
			</tr>
			<tr>
			   <td colspan="10" height="1" bgcolor="black"></td>
			</tr>
			
		<%
		    res.Movenext
			Loop
		%>
			<input type="hidden" name="fineSospensione">
		</table>
		</form>
	</body>
</html>
<%
	res.close
	connection.close
%>
