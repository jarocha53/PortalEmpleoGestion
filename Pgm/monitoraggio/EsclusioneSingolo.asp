<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%@ LANGUAGE=VBSCRIPT %>

<% response.expires = -1500 %>


<!-- #include file="adovbs.inc" -->

<%	
	Set connection = Server.createObject("ADODB.Connection")
		connection.open(session("connectString"))
		
%>
<%
	'''
	'' --------------->   CONTROLLO AUTENTICAZIONE UTENTE
	'
	if (Session("ente") = null or Session("ente") = "") then 
		Response.redirect("Login.html")
		return
	end if
%>
<%
	Dim codice
	codice = request.queryString("codice")
	
	query = "SELECT P.ID_PERSONA, P.NOME, P.COGNOME, P.COD_FISC " & _
	        "FROM PERSONA P " & _
			"WHERE P.ID_PERSONA  = " & codice
	
	Set res = Server.createObject("ADODB.Recordset")
'PL-SQL * T-SQL  
QUERY = TransformPLSQLToTSQL (QUERY) 
	Set res = connection.execute(query)
	
%>
<html>
<style type="text/css">
<!--
			 td {
			 		font-family: tahoma;
					font-size: 10;
					font-weight: bold;
			 }
			 
			 td.cfis {
			 		font-family: tahoma;
					font-size: 10;
			 }
			 
			 select {
			        border: 1 solid black;
					width: 120;
					font-family: tahoma;
					font-size: 10;
					font-weight: bold;
					padding: 2;
					background: white;
					margin: 0;
			 }
			 
			 input {
					width: 120;
					font-family: tahoma;
					font-size: 10;
					font-weight: bold;
					padding: 2;
					background: white;
					margin: 0;
			 }
			 
			 a.gen {
				    font-family: tahoma;
				    font-size: 12;
					font-weight: bold;
					color: white;
					text-align: center;
					border: 1 solid black;
					background-color: #006796;
					text-decoration: none;
					padding: 2;
					margin: 0;
					vertical-align: middle;
					width: 80;
			 }
			 
			 a.miniBottone {
			 			   font-family: tahoma;
						   font-size: 8;
						   font-weight: bold;
						   text-decoration: none;
						   vertical-align: middle;
						   border: 1 solid black;
						   color: black;
			 }
-->
</style>
	<head>
	</head>
	<script src="utils/utility.js"></script>
	<script>
		
		function annullaRegistrazione()	{
			parent.frameLettere.location = "Lettere.asp"
			parent.titoloInterno.location = "TitoloInterno.asp"
			parent.headerInterno.location = "HeaderTabella.asp"
	  		parent.barraBottoni.location = "Bottoni.asp"
			document.forms[0].action = "CancellazioneEsclusione.asp"
			document.forms[0].submit()
		}
		
	</script>
	<body bgcolor="white" leftmargin="0" rightmargin="0" topmargin="0">
	    <script src="utils/utility.js"></script>
		<form method="post" action="">
		<input type="hidden" name="codice" value="-1">
		<table border="0" cellpadding="2" style="border-left: 1 solid black; border-right: 1 solid black" width="100%" cellspacing="0" cellpadding="0">
		
		
		<%
		'il campo dopoOperazione viene messo a 'no' solo dalle funzioni
		'javascript per la selezione dei filtri. Se vale 'si' la sessione
		'viene ripulita al caricamento di questa pagina. Questo evita il
		'problema della propagazione dei dati della form.
		%>
		<input type="hidden" name="dopoOperazione" value="si"/>
		
		
		<%
			if (not res.EOF) then
				codice = res("id_persona")
				nome = res("nome")
				cognome = res("cognome")
				cdfiscale = res("cod_fisc")
		%>	
			<tr>
				<td width="200" align="left"><%=nome%></td>
				<td width="200" align="left"><%=cognome%></td>
				<td width="120" align="left"><%=Ucase(cdfiscale)%></td>
				<input type="hidden" name="codice" value="<%=codice%>">
				<input type="hidden" name="codfisc" value="<%=cdfiscale%>">
			</tr>
			<tr>
			   <td colspan="3" height="1" bgcolor="black"></td>
			</tr>
			
		<%
			end if
		%>
		</table>
		<br>
		<table align="right" cellspacing="20" cellpadding="0">
		  <tr>
			<td align="center">
				<a class="gen" href="javascript:annullaRegistrazione()">Cancella</a>
			</td>
			<td align="center"><a class="gen" href="javascript:annulla()">Indietro</a></td>
		  </tr>
		</table>
		</form>
	</body>
</html>
<%
	res.close
	connection.close
%>
