<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%@ LANGUAGE=VBSCRIPT %>

<!-- #include file="adovbs.inc" -->

<%
	'''
	'' --------------->   CONTROLLO AUTENTICAZIONE UTENTE
	'
	if (Session("ente") = null or Session("ente") = "") then 
		Response.redirect("Login.html")
		return
	end if
%>	
<%	
		Set connection = Server.createObject("ADODB.Connection")
		connection.open(session("connectString"))
		
		Set res = Server.createObject("ADODB.Recordset")
		Set resCertificazione = Server.createObject("ADODB.Recordset")
		Set resStato = Server.createObject("ADODB.Recordset")
		Set resDati = Server.createObject("ADODB.Recordset")
		Set resDati2 = Server.createObject("ADODB.Recordset")
		
		query = "SELECT ID_PERSONA, FL_STATO_PERSONA, CERTIFICATO FROM PERS_ENTE WHERE ID_IMPRESA = " & session("ente")
		
		insertCertificazione = "SELECT ID_IMPRESA, ID_SEDE, DT_RIFERIMENTO, DT_ELABORAZIONE FROM CERTIFICAZIONE"
		
		insertStato = "SELECT ID_PERSONA, FL_STATO_PERSONA, DT_RIFERIMENTO, VARIATO FROM STATO_CERTIFICATO"
		
		resStato.open insertStato, connection, adOptionDynamic, adLockOptimistic
		
		'response.write("-------------------------------------------------------"& "<br>")
		'response.write(query& "<br>")
		res.open query, connection, adOptionDynamic, adLockOptimistic
		do while (not res.EOF) 
			id_persona = res("id_persona")
			stato = res("fl_stato_persona")
			certificato = res("certificato")
			
			'response.write("id persona = " & id_persona & " stato = "& stato&" certificato = " & certificato & "<br>")
			
			if (certificato = "S") then
				'response.write("Dato certificato <br>")
				' non sono state fatte operazioni sull'LSU dall'ultima certificazione
				resStato.addNew()
					resStato("id_persona") = id_persona
					resStato("dt_riferimento") = Date()
					resStato("fl_stato_persona") = stato
					resStato("variato") = "N"
				resStato.update()
			else
				'response.write("Dato modificato <br>")
				select case stato:
					case "A":
						'response.write("LSU riattivato <br>")
						' aggiorniamo in PERS_SOSP le informazioni relative alla sospensione
						queryDati = "SELECT DT_FINE_SOSP, FL_MATERNITA FROM TMP_PERS_SOSP WHERE ID_PERSONA = " & id_persona
						resDati.open queryDati, connection
						if (not resDati.EOF) then
							fine = resDati("dt_fine_sosp")
							maternita = resDati("fl_maternita")						
						end if
						resDati.close()
						
						queryDati = "SELECT DT_FINE_SOSP, FL_MATERNITA FROM PERS_SOSP WHERE ID_PERSONA = " & id_persona & " AND DT_FINE_SOSP IS NULL"
						resDati.open queryDati, connection, adOptionDynamic, adLockOptimistic
						if (not resDati.EOF) then
							resDati("dt_fine_sosp") = fine
							resDati("fl_maternita") = maternita
							resDati.update()
						end if
						resDati.close() 
					case "S":
						'response.write("LSU sospeso <br>")
						' inseriamo in PERS_SOSP le informazioni relative alla sospensione
						queryDati = "SELECT DT_INIZIO_SOSP, FL_MATERNITA FROM TMP_PERS_SOSP WHERE ID_PERSONA = " & id_persona
						resDati.open queryDati, connection
						if (not resDati.EOF) then
							inizio = resDati("dt_inizio_sosp")
							maternita = resDati("fl_maternita")						
						end if
						resDati.close()
						
						queryDati = "SELECT * FROM PERS_SOSP"
						resDati.open queryDati, connection, adOptionDynamic, adLockOptimistic
						resDati.addNew()
							resDati("dt_inizio_sosp") = inizio
							resDati("fl_maternita") = maternita
							resDati("id_persona") = id_persona
						resDati.update()
						resDati.close() 
					case "U":
						'response.write("LSU uscito <br>")
						' inseriamo in USCITA le informazioni relative all'uscita
						queryDati = "SELECT DT_USCITA, CAUSALE_USCITA FROM TMP_USCITA WHERE ID_PERSONA = " & id_persona
						resDati.open queryDati, connection
						if (not resDati.EOF) then
							uscita = resDati("dt_uscita")
							causale = resDati("causale_uscita")						
						end if
						resDati.close()
						
						queryDati = "SELECT DT_USCITA, CAUSALE_USCITA, ID_PERSONA, ID_IMPRESA, ID_SEDE FROM USCITA"
						resDati.open queryDati, connection, adOptionDynamic, adLockOptimistic
						resDati.addNew()
							resDati("dt_uscita") = uscita
							resDati("causale_uscita") = causale
							resDati("id_persona") = id_persona
							resDati("id_impresa") = session("ente")
							resDati("id_sede") = session("ente")
						resDati.update()
						resDati.close() 
						
						'aggiustiamo eventuali sospensioni pendenti
						'verifichiamo se c'� una sospensione certificata aperta...
						queryDati = "SELECT DT_FINE_SOSP FROM PERS_SOSP WHERE ID_PERSONA = " & id_persona & " AND DT_FINE_SOSP IS NULL"
						resDati.open queryDati, connection, adOptionDynamic, adLockOptimistic
						if (not resDati.EOF) then
							resDati("dt_fine_sosp") = uscita
							resDati.update()
						else
							' verifichiamo se c'� una sospensione non certificata aperta...
							' FIXME da implementare
						end if
						resDati.close()
				end select
				
				resStato.addNew()
					resStato("id_persona") = id_persona
					resStato("dt_riferimento") = Date()
					resStato("fl_stato_persona") = stato
					resStato("variato") = "S"
				resStato.update()
			end if
		
			res.moveNext()
			'response.write("-------------------------------------------------------"& "<br>")
		loop
		res.close()
		
		resStato.close()
				
		resCertificazione.open insertCertificazione, connection, 3, 3
		resCertificazione.addNew()
			resCertificazione("id_impresa") = session("ente")
			resCertificazione("id_sede") = session("ente")
			resCertificazione("dt_riferimento") = Date()
			resCertificazione("dt_elaborazione") = Date()
		resCertificazione.update()
		resCertificazione.close()
		
		'response.write("Salvato record in certificazione <br>")
				
		'rimozione dei dati da AREA_LAVORO
		query = "DELETE FROM AREA_LAVORO WHERE ID_PERSONA IN (SELECT ID_PERSONA FROM PERS_ENTE WHERE ID_IMPRESA = " & session("ente") & ")"
'PL-SQL * T-SQL  
QUERY = TransformPLSQLToTSQL (QUERY) 
		connection.execute(query)
		
		'res.open query, connection, adOptionDynamic, adLockOptimistic
		'do while (not res.EOF) 
		'	res.delete()
		'	res.update()
		'	res.moveNext()
		'loop
		'res.close()
		
		query = "DELETE FROM TMP_PERS_SOSP WHERE ID_PERSONA IN (SELECT ID_PERSONA FROM PERS_ENTE WHERE ID_IMPRESA = " & session("ente") & ")"
'PL-SQL * T-SQL  
QUERY = TransformPLSQLToTSQL (QUERY) 
		connection.execute(query)

		'res.open query, connection, adOptionDynamic, adLockOptimistic
		'do while (not res.EOF)
		'	res.delete()
		'	res.update()
		'	res.moveNext()
		'loop
		'res.close()
		
		query = "DELETE FROM TMP_USCITA WHERE ID_PERSONA IN (SELECT ID_PERSONA FROM PERS_ENTE WHERE ID_IMPRESA = " & session("ente") & ")"
'PL-SQL * T-SQL  
QUERY = TransformPLSQLToTSQL (QUERY) 
		connection.execute(query)
		
		'res.open query, connection, adOptionDynamic, adLockOptimistic
		'do while (not res.EOF)
		'	res.delete()
		'	res.update()
		'	res.moveNext()
		'loop
		'res.close()
		
		query = "SELECT CERTIFICATO FROM PERS_ENTE WHERE CERTIFICATO = 'N' AND ID_IMPRESA = " & session("ente")
		res.open query, connection, adOptionDynamic, adLockOptimistic
		if (not res.EOF) then 
			'response.write("certificato = " & res("certificato"))
			res("certificato") = "S"
			res.update()
			'res.moveNext()
		end if
		res.close()
		
        ''response.write("Salvato record in certificazione <br>")
		Set res = nothing
		Set resCertificazione = nothing
		Set resStato = nothing
		connection.close()
		Set connection = nothing
		
	session("certificazione") = "disabled"
	Server.transfer("Visualizzazione.asp")
%>
