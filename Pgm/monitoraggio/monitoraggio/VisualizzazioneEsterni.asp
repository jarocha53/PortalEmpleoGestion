<%@ LANGUAGE=VBSCRIPT %>
<%
	'''
	'' --------------->   CONTROLLO AUTENTICAZIONE UTENTE
	'
	if (Session("ente") = null or Session("ente") = "") then 
		Response.redirect("Login.html")
		return
	end if
	
	if(session("livelloUtente") = 2) then
	// ENTE UTILIZZATORE
		Server.transfer("MascheraA.asp")
	elseif (session("livelloUtente") = 3) then
	// ENTE PROVINCIA
		Server.transfer("MascheraB.asp")
	elseif (session("livelloUtente") = 4) then
	// ENTE REGIONE
		Server.transfer("MascheraC.asp")
	elseif (session("livelloUtente") = 5) then
	// ENTE MINISTERO
		Server.transfer("MascheraD.asp")
	else
	
	end if
%>