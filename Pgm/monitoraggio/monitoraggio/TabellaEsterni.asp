<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%@ LANGUAGE=VBSCRIPT %>
<%
	'''
	'' ---------------> CONTROLLO VALIDITA' SESSIONE
	'
	if(isNull(Session)) then
		Server.transfer("Redirect.html")
	end if
	
	'''
	'' --------------->   CONTROLLO AUTENTICAZIONE UTENTE
	'
	if (Session("ente") = null or Session("ente") = "") then 
		Response.redirect("Login.html")
		return
	end if
%>

<html>
<head>
	<link rel ="stylesheet" type="text/css" href="stylesheet.css" title="Style">
</head>
<body bgcolor="white" rightmargin="0" leftmargin="10" topmargin="0" marginwidth="0" marginheight="0">
<%
		Set connection = Server.createObject("ADODB.Connection")
			connection.open(session("connectString"))
		Dim query
		Dim inner
		query = "SELECT I.ID_IMPRESA, I.RAG_SOC FROM IMPRESA I WHERE I.ID_IMPRESA = " & Request("ente")
'PL-SQL * T-SQL  
QUERY = TransformPLSQLToTSQL (QUERY) 
		Set outer = connection.execute(query)
		id_impresa = outer("id_impresa")
		ragione_sociale = outer("rag_soc")

		query = "SELECT MIN(DT_ELABORAZIONE) VALIDAZIONE, MAX(DT_ELABORAZIONE) CERTIFICAZIONE " & _
	            "FROM CERTIFICAZIONE " & _
		        "WHERE ID_IMPRESA = " & id_impresa
'PL-SQL * T-SQL  
QUERY = TransformPLSQLToTSQL (QUERY) 
		Set inner = connection.execute(query)
		data_validazione = inner("validazione")
		ultima_certificazione = inner("certificazione")
		inner.close

		query = "SELECT COUNT(ID_PERSONA) ATTIVI " & _
                "FROM PERS_ENTE " & _
				"WHERE FL_STATO_PERSONA = 'A' AND ID_IMPRESA = " & id_impresa & " AND CERTIFICATO = 'S'"
'PL-SQL * T-SQL  
QUERY = TransformPLSQLToTSQL (QUERY) 
		Set inner = connection.execute(query)
		lsu_attivi = inner("attivi")
		inner.close

		query = "SELECT COUNT(ID_PERSONA) SOSPESI " & _
                "FROM PERS_ENTE " & _
				"WHERE FL_STATO_PERSONA = 'S' AND ID_IMPRESA = " & id_impresa & " AND CERTIFICATO = 'S'"
'PL-SQL * T-SQL  
QUERY = TransformPLSQLToTSQL (QUERY) 
		Set inner = connection.execute(query)
		lsu_sospesi = inner("sospesi")
		inner.close

		query = "SELECT COUNT(ID_PERSONA) USCITI " & _
                "FROM PERS_ENTE " & _
				"WHERE FL_STATO_PERSONA = 'U' AND ID_IMPRESA = " & id_impresa & " AND CERTIFICATO = 'S'"
'PL-SQL * T-SQL  
QUERY = TransformPLSQLToTSQL (QUERY) 
		Set inner = connection.execute(query)
		lsu_usciti = inner("usciti")
		inner.close
%>
	<table align="center" border="0" width="100%" height="100%" cellspacing="0" cellpadding="0">
	<tr>
		<td height="40" class="entePiccolo" align="center">Dati riassuntivi per: <%=ragione_sociale%></td>
	</tr>
	<tr>
		<td height="30">
			<table width="100%" cellpadding="4"><tr>
				<td class="titololista" width="200">Data validazione</td>
				<td class="lista">
				<% if(data_validazione <> null and trim(data_validazione) <> "") then %>
					<blink>Nessuna</blink>
				<% else %>
					<%=data_validazione%>
				<% end if %>
				</td>
				<td class="titololista" width="200">Ultima certificazione</td>
				<td class="lista"><%=ultima_certificazione%></td>
			</tr></table>
		</td>
	</tr>
	<tr>
		<td height="10">
			<table width="100%" cellpadding="4"><tr>
				<td class="titololista" width="120">LSU attivi</td>
				<td class="lista" align="left"><%=lsu_attivi%></td>
				<td class="titololista" width="120">LSU sospesi</td>
				<td class="lista" align="left"><%=lsu_sospesi%></td>
				<td class="titololista" width="120">LSU usciti</td>
				<td class="lista" align="left"><%=lsu_usciti%></td>
			</tr></table>
		</td>
	</tr>
<%
		outer.close()
		connection.close()
%>
	<tr>
		<td height="20">
			<table border="0" width="100%" align="center">
				<tr>
					<td align="center"><a class="bottone" href="FramesetListaLSUEsterni.asp?vedi=<%=Request("ente")%>&filtro=attivi" target="listaLSUEsterni">Attivi</a></td>
					<td align="center"><a class="bottone" href="FramesetListaLSUEsterni.asp?vedi=<%=Request("ente")%>&filtro=sospesi" target="listaLSUEsterni">Sospesi</a></td>
				    <td align="center"><a class="bottone" href="FramesetListaLSUEsterni.asp?vedi=<%=Request("ente")%>&filtro=usciti" target="listaLSUEsterni">Usciti</a></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			
			<iframe name="listaLSUEsterni" src="FramesetListaLSUEsterni.asp?vedi=<%=Request("ente")%>" frameborder="0" border="0" width="100%" height="100%"></iframe>
		</td>
	</tr>
</table>
</body>
</html>
