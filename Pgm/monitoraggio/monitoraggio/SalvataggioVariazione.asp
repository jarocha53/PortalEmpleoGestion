<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%@ LANGUAGE=VBSCRIPT %>
<!-- #include file="db/salvataggio.asp" -->
<%
	'''
	'' ---------------> CONTROLLO VALIDITA' SESSIONE
	'
	if(isNull(Session)) then
		Server.transfer("Redirect.html")
	end if
	
	'''
	'' --------------->   CONTROLLO AUTENTICAZIONE UTENTE
	'
	if (Session("ente") = null or Session("ente") = "") then 
		Response.redirect("Login.html")
		return
	end if
%>
<%
	Set connection = Server.createObject("ADODB.Connection")
		connection.open(session("connectString"))
   
	codici    = split(request.Form("codice"), ",")
	lista     = split(request.Form("lista"), ",")
	
	mansioni  = split(request.Form("mansione"), ",")
	settori  = split(request.Form("settore"), ",")
	
	Set res = Server.createObject("ADODB.Recordset")
	
	 for k = Lbound(codici)+1 to Ubound(codici)
		if (lista(k) > 0) then
			query  = "SELECT ID_PERSONA, COD_SETTORE_LSU, COD_MANSIONE_LSU " & _
	                 "FROM PERS_ENTE " &_
			         "WHERE ID_PERSONA = " & codici(k) & " AND ID_IMPRESA = " & session("ente")
			res.open query, connection, 3, 3
			
			res("cod_mansione_lsu") = mansioni(k)
			res("cod_settore_lsu")    = settori(k)
			res.update()
			res.close()
		
		end if
	next
	Set res = nothing
	connection.close()
	Set connection = nothing

	Server.transfer("Visualizzazione.asp")
%>
