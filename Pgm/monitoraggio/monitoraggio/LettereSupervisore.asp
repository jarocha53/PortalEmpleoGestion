<%@ LANGUAGE=VBSCRIPT %>

<% response.expires = -1500 %>


<!-- #include file="adovbs.inc" -->
<%
	'''
	'' --------------->   CONTROLLO AUTENTICAZIONE UTENTE
	'
	if (Session("ente") = null or Session("ente") = "") then 
		Response.redirect("Login.html")
		return
	end if
	
	Dim guardato
		guardato = request.queryString("guarda")
%>
<html>
<style>
  a.lettere {
    font-family: tahoma;
    font-size: 10;
	font-weight: bold;
	color: white;
	text-decoration: none;
	text-align: center;
	background-color: #003162;
	width: 40;
	height: 14;
	border: 1 solid black;
	padding: 0;
	margin: 0;
	margin-left: 0;
  }
  a.lettera {
    font-family: tahoma;
    font-size: 10;
	font-weight: bold;
	color: white;
	text-decoration: none;
	text-align: center;
	background-color: #003162;
	width: 20;
	height: 14;
	border: 1 solid black;
	padding: 0;
	margin: 0;
	margin-left: 0;
  }
  a.stato {
    font-family: tahoma;
    font-size: 11;
	font-weight: bolder;
	color: white;
	text-decoration: none;
	background-color: #003162;
	text-align: center;
	width: 100;
	border: 1 solid black;
	padding: 0;
	margin: 0;
	margin-left: 4;
  }
</style>
<head>
</head>
<body  bgcolor="#006796" style="border: 1 solid black">
	<script language="javascript">
		function aggiornaPagina(lettera){
			parent.mainInterno.document.location = "MascheraA.asp?guarda=<%=guardato%>&groupby=letter&letter="+lettera
		}
		function aggiornaPaginaStato(stato){
			parent.mainInterno.document.location = "MascheraA.asp?guarda=<%=guardato%>&groupby=state&state="+stato
		}
	</script>

<table border="0" width="100%" align="center">
	<tr>
		<td align="center"><a class="lettere" href="javascript:aggiornaPagina('')">A - Z</a></td>
	    <td align="center"><a class="lettera" href="javascript:aggiornaPagina('A')">A</a></td>
		<td align="center"><a class="lettera" href="javascript:aggiornaPagina('B')">B</a></td>
		<td align="center"><a class="lettera" href="javascript:aggiornaPagina('C')">C</a></td>
		<td align="center"><a class="lettera" href="javascript:aggiornaPagina('D')">D</a></td>
		<td align="center"><a class="lettera" href="javascript:aggiornaPagina('E')">E</a></td>
		<td align="center"><a class="lettera" href="javascript:aggiornaPagina('F')">F</a></td>
		<td align="center"><a class="lettera" href="javascript:aggiornaPagina('G')">G</a></td>
		<td align="center"><a class="lettera" href="javascript:aggiornaPagina('H')">H</a></td>
		<td align="center"><a class="lettera" href="javascript:aggiornaPagina('I')">I</a></td>
		<td align="center"><a class="lettera" href="javascript:aggiornaPagina('J')">J</a></td>
		<td align="center"><a class="lettera" href="javascript:aggiornaPagina('K')">K</a></td>
		<td align="center"><a class="lettera" href="javascript:aggiornaPagina('L')">L</a></td>
		<td align="center"><a class="lettera" href="javascript:aggiornaPagina('M')">M</a></td>
		<td align="center"><a class="lettera" href="javascript:aggiornaPagina('N')">N</a></td>
		<td align="center"><a class="lettera" href="javascript:aggiornaPagina('O')">O</a></td>
		<td align="center"><a class="lettera" href="javascript:aggiornaPagina('P')">P</a></td>
		<td align="center"><a class="lettera" href="javascript:aggiornaPagina('Q')">Q</a></td>
		<td align="center"><a class="lettera" href="javascript:aggiornaPagina('R')">R</a></td>
		<td align="center"><a class="lettera" href="javascript:aggiornaPagina('S')">S</a></td>
		<td align="center"><a class="lettera" href="javascript:aggiornaPagina('T')">T</a></td>
		<td align="center"><a class="lettera" href="javascript:aggiornaPagina('U')">U</a></td>
		<td align="center"><a class="lettera" href="javascript:aggiornaPagina('V')">V</a></td>
		<td align="center"><a class="lettera" href="javascript:aggiornaPagina('W')">W</a></td>
		<td align="center"><a class="lettera" href="javascript:aggiornaPagina('X')">X</a></td>
		<td align="center"><a class="lettera" href="javascript:aggiornaPagina('Y')">Y</a></td>
		<td align="center"><a class="lettera" href="javascript:aggiornaPagina('Z')">Z</a></td>
	</tr>
</table>
<table border="0" width="100%" align="center">
	<tr>
		<td align="center"><a class="stato" href="javascript:aggiornaPaginaStato('A')">Attivi</a></td>
	    <td align="center"><a class="stato" href="javascript:aggiornaPaginaStato('S')">Sospesi</a></td>
		<td align="center"><a class="stato" href="javascript:aggiornaPaginaStato('U')">Usciti</a></td>
		<td align="center"><a class="stato" href="javascript:aggiornaPaginaStato('E')">Sconosciuti</a></td>
	</tr>
</table>
</body>
</html>
