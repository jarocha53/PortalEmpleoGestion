<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%@ LANGUAGE=VBSCRIPT %>
<!-- #include file="db/connessione.asp" -->

<% response.expires = - 1500 %>

<%
	codici = split(Request.Form("codice"), ",")
	query = "SELECT P.ID_PERSONA, P.NOME, P.COGNOME, P.COD_FISC " & _
	        "FROM  PERSONA P " & _
			"WHERE  P.ID_PERSONA NOT IN (SELECT ID_PERSONA FROM RICH_VAR_ANAG WHERE TIPO_RICHIESTA = 'V') AND " & _
			"P.ID_PERSONA IN ("
	for i = lbound(codici) to ubound(codici) 
		query = query & "'"&codici(i)&"'"
		if (i < ubound(codici)) then 
			query = query & ", "
		else
			query = query & ")"
		end if
	next
	
	Set res = Server.createObject("ADODB.Recordset")
'PL-SQL * T-SQL  
QUERY = TransformPLSQLToTSQL (QUERY) 
	Set res = connection.execute(query)
	
	if res.EOF then
		' FIXME -> in questo caso non ci sono LSU da mettere in uscita
		Server.transfer("MonitoraggioMain.asp") 
	end if
%>	


<html>
	<head>
<style type="text/css">
<!--
	 td {
	 		font-family: tahoma;
			font-size: 12;
			font-weight: bold;
	 }
	 td.valore {
	 		font-family: tahoma;
			font-size: 14;
			font-weight: bolder
	 }
	 td.header {
	 		font-family: tahoma;
			font-size: 20;
			font-weight: bolder;
			text-align: center;
	 }
	 td.codfis {
	 		font-family: courier new;
			font-size: 20;
			font-weight: bolder;
			text-align: center;
	 }
-->
</style>
	</head>
	<body>
		<script  language="JavaScript" src="utils/utility.js"></script>
		<script  language="JavaScript">
			function conferma() {
				checks = document.forms[0].escludi
				codici = document.forms[0].codice
				nomi   = document.forms[0].nome
				cognomi= document.forms[0].cognome
				lista  = document.forms[0].lista
				cfisc  = document.forms[0].codfiscale
				
				for (k = 1; k < codici.length; k++) {
					if (!checks[k].checked)  { // l'LSU non � stato escluso dall'operazione
						if (isEmpty(nomi[k].value)) {
							alert("Tutti i campi sono obbligatori")
							nomi[k].select()
							nomi[k].focus()
							return
						}
						if (isEmpty(cognomi[k].value)) {
							alert("Tutti i campi sono obbligatori")
							cognomi[k].select()
							cognomi[k].focus()
							return
						}
						//if (isEmpty(cfisc[k].value)) {
						//	alert("Tutti i campi sono obbligatori")
						//	cfisc[k].select()
						//	cfisc[k].focus()
						//	return
						//}
 					} else {
						// escludiamo l'LSU dal salvataggio
						lista[k].value="0";
					}
				}
				document.forms[0].submit()
			}
			
		</script>
		
		<table width="100%" height="100%">
		<tr>
      <td class="header">
			Inserire i nuovi dati e premere conferma 
			</td>
		</tr>
		<tr><td valign="middle" align="center">
		<form method="post" action="SegnalazioneVariazione.asp">
		<table style="border: 1 solid black; border-right: 2 solid black" width="70%" border="0" align="center" valign="middle" cellpadding="0" cellspacing="0">
			<input type="hidden" name="codice" value="0">
			<input type="hidden" name="escludi" value="0">
			<input type="hidden" name="nome" value="0">
			<input type="hidden" name="cognome" value="0>
			<input type="hidden" name="codfiscale" value="0">
			<input type="hidden" name="nomeOld" value="0">
			<input type="hidden" name="cognomeOld" value="0>
			<input type="hidden" name="codfiscaleOld" value="0">
			<input type="hidden" name="lista" value="0">
			
		<%	
			do while not res.EOF 

		%>
		<input type="hidden" name="codice" value="<%=res("id_persona")%>">
		<tr>
			<td colspan="2">
				<table style="border-right: 1 solid black" border="0" cellpadding="6" bgcolor="#dddddd" width="100%" height="100%">
					<tr>
						<td valign="middle">
							Nome
						</td>
						<td class="valore" valign="middle">
							<input type="text" name ="nome" value="<%=res("nome")%>">
							<input type="hidden" name ="nomeOld" value="<%=res("nome")%>">
						</td>
						<td valign="middle">
							Cognome
						</td>
						<td class="valore" valign="middle">
							<input type="text" name ="cognome" value="<%=res("cognome")%>">
							<input type="hidden" name ="cognomeOld" value="<%=res("cognome")%>">
						</td>
						<td valign="middle">
							Codice Fiscale
						</td>
						<td class="valore" colspan="3" valign="middle">
							<input type="text" name ="codfiscale" value="<%=Ucase(res("cod_fisc"))%>">
							<input type="hidden" name ="codfiscaleOld" value="<%=res("cod_fisc")%>">
						</td>
					</tr>
					<!-- <tr>
					  <td>
						Mansione
					  </td>
					  <td>
						<select style="width: 100%" name="mansione" value="">
						</select>
					  </td>
					  <td>
						Settore
					  </td>
					  <td>
						<select style="width: 100%" name="settore" value="">
						</select>
					  </td>
					</tr> -->
					<tr> 
						<td align="left" colspan="2">
							<input type="checkbox" name="escludi">Escludi dall'operazione</checkbox>
							<input type="hidden" name="lista" value="1">
						</td>
					</tr> 
				</table>
			</td>
		</tr>
		<tr><td height="2" bgcolor="black" colspan="2"></td></tr>
		<%
				res.movenext
			loop
			
			res.close
			connection.close
		%>
		<tr>
			<td align="center"> 
				<a  href="javascript:conferma()"><img src="images/Ok.gif" alt="Conferma" border="0"/></a>
			</td>
			<td align="center">
				<a  href="javascript:annulla()"><img src="images/NoWay.gif" alt="Annulla" border="0"/></a>
			</td>
		</tr>
		</table>
		</form>
		</td></tr></table>
	</body>
</html>




	
