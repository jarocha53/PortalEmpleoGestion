
function isEmpty(inputStr) {
	if (inputStr == null || inputStr == "") {
		return true
	}
	return false
}

function annulla() {
		parent.frameLettere.location = "Lettere.asp"
		parent.headerInterno.location = "HeaderTabella.asp"
		parent.barraBottoni.location = "Bottoni.asp"
		parent.mainInterno.location = "Visualizzazione.asp"
}

function validaData(d) {
	data = d.value
	if (isEmpty(d.value)) {
		return		
	}
	igiorno = data.indexOf("/")
	imese   = data.indexOf("/", igiorno+1)
	
	giorno  = parseInt(data.substring(0, igiorno))
	mese    = parseInt(data.substring(igiorno+1, imese))
	anno    = parseInt(data.substring(imese+1, data.length))
	
	if(isNaN(giorno) || isNaN(mese) || isNaN(anno))  {
		alert("Data non valida")
		d.select()
		d.focus()
		return
	}
	if ((anno < 1900) || (mese < 0 || mese > 12) || (giorno < 0 || giorno > 31)) {
		alert("Data non valida")
		d.select()
		d.focus()
		return
	} 
	
	date = new Date(anno, mese - 1, giorno)
	today = new Date()
	if(date.getTime() > today.getTime()) {
		alert("Non � possibile specificare una data successiva alla data odierna")
		d.select()
		d.focus()
		return
	}
}
