<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%@ LANGUAGE=VBSCRIPT %>

<!-- #include file="adovbs.inc" -->

<%	
	'''
	'' ---------------> CONTROLLO VALIDITA' SESSIONE
	'
	if(isNull(Session)) then
		Server.transfer("Redirect.html")
	end if
	
	Set connection = Server.createObject("ADODB.Connection")
	connection.open(session("connectString"))
		Dim username
	username = Request.form("usr")
	password = Request.form("pwd")
	
	' test vale zero se il login non � valido
	'           uno  se � un operatore
	'           da due a cinque se � un supervisore
	test = isValidUser(username, password)
	
	if test > 0 then
		'**************************************************************************
		'FIXME qui i valori andranno impostati in base alla profilazione degli enti
		'	   e degli operatori!!!!
		
		Session("ente") = username
		' questo � l'utente che pu� fare modifiche sul database
		if(test = 1) then
			' dobbiamo controllare che l'utente non sia gi� connesso
			if (isNull(Application(Cstr(username)))or Application(Cstr(username)) = "") then
				Application.lock()
				Application(Cstr(Session("ente"))) = username
				Application.unlock()
				Session("tipoUtente") = "operatore"
				Session("livelloUtente") = 1
			else
				session("ErrorMessage") = "Utente gi� connesso"
				Server.transfer("Login.html")
			end if
		else
				Session("tipoUtente") = "supervisore"
				Session("livelloUtente") = test
		end if

		if(session("ente") = "minlav") then 
		 session("ente") = 0 
		end if

		'**************************************************************************
		session.contents.remove("ErrorMessage")
		Server.transfer("MainPage.html")
	else
		connection.close()
		session("ErrorMessage") = "Utente sconosciuto"
		Server.transfer("Login.html")
	end if

	Function isValidUser(u, p)
		Set res = Server.createObject("ADODB.Recordset")
		VariableSQL = "SELECT TIPO_OPERATORE OP FROM LOGIN WHERE USERNAME = '" & u & "' AND PASSWORD = '" & p & "'"
	'PL-SQL * T-SQL
		VariableSQL = TransformPLSQLToTSQL(VariableSQL)
		Set res = connection.execute(VariableSQL)
		if (not res.EOF) then
			op = res("op")
		    if(op = 0) then
				isValidUser = 1 // codice operatore
			elseif(op = 1) then
				isValidUser = 2 // codice ENTE UTILIZZATORE supervisore
			elseif(op = 2) then
				isValidUser = 3 // codice PROVINCIA supervisore
			elseif(op = 3) then
				isValidUser = 4 // codice REGIONE supervisore
			elseif(op = 4) then
				isValidUser = 5 // codice MINISTERO supervisore
			end if
		else
			isValidUser = 0
		end if
		res.close()
	End Function
%>
