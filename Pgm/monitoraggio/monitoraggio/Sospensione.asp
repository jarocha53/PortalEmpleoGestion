<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%@ LANGUAGE=VBSCRIPT %>
<!-- #include file="db/connessione.asp" -->

<% response.expires = - 1500 %>

<%
	codici = split(Request.Form("codice"), ",")
	
	'IPOTESI: possono essere sospesi solo gli LSU attivi
	query = "SELECT P.ID_PERSONA, P.NOME, P.COGNOME, P.COD_FISC, PS.DT_INIZIO_SOSP " & _
	        "FROM PERSONA P, PERS_ENTE PE, PERS_SOSP PS " & _
			"WHERE P.ID_PERSONA (+) = PS.ID_PERSONA AND P.ID_PERSONA = PE.ID_PERSONA AND PE.FL_STATO_PERSONA = 'A' AND " & _
			"P.ID_PERSONA IN ("
	for i = lbound(codici) to ubound(codici) 
		query = query & "'"&codici(i)&"'"
		if (i < ubound(codici)) then 
			query = query & ", "
		else
			query = query & ")"
		end if
	next
	
	Set res = Server.createObject("ADODB.Recordset")
'PL-SQL * T-SQL  
QUERY = TransformPLSQLToTSQL (QUERY) 
	Set res = connection.execute(query)
	
	if res.EOF then
		' FIXME nessuno degli LSU selezionati pu� essere sospeso
		Server.transfer("MonitoraggioMain.asp")
	end if
%>



<html>
	<head>
<style type="text/css">
<!--
	 td {
	 		font-family: tahoma;
			font-size: 12;
			font-weight: bold;
	 }
	 td.valore {
	 		font-family: tahoma;
			font-size: 14;
			font-weight: bolder
	 }
	 td.header {
	 		font-family: tahoma;
			font-size: 20;
			font-weight: bolder;
			text-align: center;
	 }
	 td.codfis {
	 		font-family: courier new;
			font-size: 20;
			font-weight: bolder;
			text-align: center;
	 }
-->
</style>
	</head>
	<body>
		<script src="utils/utility.js"></script>
				
		<script>
				   
			function conferma() {
				checks = document.forms[0].escludi
				codici = document.forms[0].codice
				inizio = document.forms[0].inizioSospensione
				lista  = document.forms[0].lista
				mat    = document.forms[0].maternita
				lmat   = document.forms[0].listamaternita
				
				for (k = 1; k < codici.length; k++) {
					if (mat[k].checked){ // flag maternit�
						lmat[k].value = 1
					}
					if (!checks[k].checked)  { // l'LSU non � stato escluso dall'operazione
						if (isEmpty(inizio[k].value))
							alert("La data di inizio sospensione � obbligatoria")
							inizio[k].select()
							inizio[k].focus
							return	
						}
						dataI = new Date(inizio[k].value)
					} else {
						// escludiamo l'LSU dal salvataggio
						lista[k].value="0";
					}
				}
				document.forms[0].submit()
			}
			
		</script>
		<table width="100%" height="100%">
		<tr>
      		<td class="header">
				Per confermare la sospensione....
			</td>
		</tr>
		<tr><td valign="middle" align="center">
		<form method="post" action="SalvataggioSospensione.asp">
			<table style="border: 1 solid black; border-right: 2 solid black" width="100%" border="0" align="center" valign="middle" cellpadding="0" cellspacing="0">
				
				<!-- per fare in modo che sia sempre un array -->
				<input type="hidden" name="codice" value="0">
				<input type="hidden" name="inizioSospensione" value="0">
				<input type="hidden" name="fineSospensione" value="0">
				<input type="hidden" name="lista" value="0">
				<input type="hidden" name="listamaternita" value="0">
				<input type="hidden" name="maternita">
				
				<input type="hidden" name="escludi" value="">
				
			<%
				do while not res.EOF 
			%>
					<input type="hidden" name="codice" value="<%=res("id_persona")%>">
					<tr>
						<td>
							<table style="border-right: 1 solid black" border="0" cellpadding="6" bgcolor="#dddddd" width="100%" height="100%">
								<tr>
									<td valign="middle" width="100">
										Cognome e Nome
									</td>
									<td class="valore" valign="middle">
										<%=res("cognome")%>&nbsp;<%=res("nome")%>
									</td>
									<td valign="middle" width="100">
										Codice Fiscale
									</td>
									<td class="valore" colspan="3" valign="middle">
										<%=res("cod_fisc")%>
									</td>
								</tr>
								<tr>
									<td align="left" colspan="4">
										<input type="hidden" name="lista" value="1">
										<input type="checkbox" name="escludi">Escludi dall'operazione</checkbox>
									</td>
								</tr>
							</table>
						</td>
						<td>
							<table border="0" cellpadding="6" bgcolor="#ccccff" width="100%" height="100%">
								<tr>
									<td valign="middle">
										Inizio Sospensione
									</td>
									<td class="valore" valign="middle">
										<input style="width:70" cols="8" type="text" name="inizioSospensione"">
									</td>
									<td valign="middle">
										Fine Sospensione
									</td>
									<td class="valore" valign="middle">
										<input style="width:70" cols="8" type="text" name="fineSospensione">
									</td>
								</tr>
								<tr>
									<td colspan="3" class="valore" valign="middle">
										<input type="checkbox" name="maternita">Sospensione per maternit�</checkbox>
										<input type="hidden" name="listamaternita" value="0">
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr><td height="2" bgcolor="black" colspan="2"></td></tr>
					<%
						res.movenext
						loop
					%>
					<tr>
						<td align="center"> 
							<a  href="javascript:conferma()"><img src="images/Ok.gif" alt="Conferma" border="0"/></a>
						</td>
						<td align="center">
							<a  href="javascript:annulla()"><img src="images/NoWay.gif" alt="Annulla" border="0"/></a>
						</td>
				</tr>
				</table>
			</form>
		</td></tr></table>
	</body>
</html>

<%
	res.close
	connection.close
%>
