<%@ LANGUAGE=VBSCRIPT %>

<% 
	'''
	'' ---------------> CONTROLLO VALIDITA' SESSIONE
	'
	if(isNull(session)) then
		Server.transfer("Redirect.html")
	end if
	
	'if (session("tipoUtente") = "operatore") then
		' rimuoviamo il lock per il login dell'utente operatore
		Application.lock()
	    Application.contents.remove(Cstr(session("ente")))
	    Application.unlock()
	'end if
   session.abandon()
   
   Server.transfer("Login.html")
%>