<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%@ LANGUAGE=VBSCRIPT %>
<% response.expires = -1500 %>
<!-- #include file="adovbs.inc" -->
<%
	'''
	'' ---------------> CONTROLLO VALIDITA' SESSIONE
	'
	if(isNull(Session)) then
		Server.transfer("Redirect.html")
	end if
	
	'''
	'' --------------->   CONTROLLO AUTENTICAZIONE UTENTE
	'
	if (Session("ente") = null or Session("ente") = "") then 
		Response.redirect("Login.html")
		return
	end if
%>

<%
	if(Session("tipoUtente") = "supervisore") then
		Server.transfer("ErroreTipoUtente.asp")
		return
	end if
%>
<%
	Set connection = Server.createObject("ADODB.Connection")
	connection.open(session("connectString"))
	modo = Request.querystring("mode")
	'#########
	'#######
	'#####   ATTENZIONE
	'####
	'###     Oltre ai codici che arrivano dalla form bisogna rendere pure quelli
	'##      che stanno in sessione !!! quindi controllare pure il campo
	'#               parent.mainInterno.document.FormDati.count.value;
%>
<html>
<style>
  a.gen {
    font-family: tahoma;
    font-size: 12;
	font-weight: bold;
	color: white;
	text-align: center;
	border: 1 solid black;
	background-color: #006796;
	text-decoration: none;
	padding: 2;
	margin: 0;
	vertical-align: middle;
	width: 80;
  }
  a.miniBottone {
	font-family: tahoma;
	font-size: 8;
	font-weight: bold;
	text-decoration: none;
	vertical-align: middle;
	border: 1 solid black;
	color: black;
 }
 
 a.bottoneDisabilitato {
	font-family: tahoma;
	font-size: 12;
	font-weight: bold;
	color: white;
	text-align: center;
	border: 1 solid black;
	background-color: #666666;
	text-decoration: none;
	padding: 2;
	margin: 0;
	vertical-align: middle;
	width: 80;
 }
 

 td {
	font-family: tahoma;
	font-size: 12;
    font-weight: bold;
	vertical-align: middle;
 }
 input {
	font-family: tahoma;
	font-size: 12;
	font-weight: bold;
 }
</style>
<script src="utils/utility.js"></script>
<script language="JavaScript" type="text/javascript">

	// FUNZIONI GENERALI

	function mostraCalendario() {
		    finestra = window.open("Calendar.html", "Calendario", "width=250,height=210,noresize,nomaximize");
	}

	// FUNZIONI PER USCITA

	function segnalaUscita() {
		options = parent.mainInterno.document.FormDati.codice
		count = parent.mainInterno.document.FormDati.count.value;
		for (i = 1; i < options.length; i++)  {
			if (options[i].checked) {
				count++
			}
		}
		if (count > 0)  {
		    parent.headerInterno.location = "HeaderTabella.asp?mode=uscita"
			parent.barraBottoni.location = "Bottoni.asp?mode=uscita"
			parent.frameLettere.location = "Blank.asp"
			parent.mainInterno.document.FormDati.target = "mainInterno"
			parent.mainInterno.document.FormDati.action = "ListaUscita.asp"
			parent.mainInterno.document.FormDati.submit()
		} else {
			alert("Attenzione: nessun nominativo selezionato per l'uscita")
			return
		}
	}

	function confermaUscita() {
		checks = parent.mainInterno.document.forms[0].escludi
		codici = parent.mainInterno.document.forms[0].codice
		data   = document.forms[0].data
		causale= document.forms[0].causale
		lista  = parent.mainInterno.document.forms[0].lista
		parent.mainInterno.document.forms[0].dataUscita.value = data.value
		if (isEmpty(data.value) ) {
			alert("Specificare la data di uscita")
			return
		} else if (isEmpty(causale.value)) {
			alert("Specificare la causale di uscita")
			return
		} else  {
		    parent.mainInterno.document.forms[0].causale.value = causale.value
		}
		d = data.value
		if (!isEmpty(d)) {
			igiorno = d.indexOf("/")
			imese   = d.indexOf("/", igiorno+1)
			
			giorno  = parseInt(d.substring(0, igiorno))
			mese    = parseInt(d.substring(igiorno+1, imese))
			anno    = parseInt(d.substring(imese+1, d.length))
			
			if (isNaN(giorno) || isNaN(mese) || isNaN(anno)) {
				alert("Data non valida")
				return
				data.select()
				data.focus()
			}
		   today = new Date()
		   // alert("Oggi  = "+today.getDate()+"/"+(today.getMonth()+1)+"/"+today.getYear())
		   // controlliamo se la data di uscita si riferisce ad un periodo di certificazione
		   // precendente a quello corrente, cio� se il mese della data di uscita � precedente 
		   // allo scorso mese o se � proprio uguale allo scorso mese ma il giorno di uscita �
		   // precedente all'11 di tale mese
//		   if (anno < today.getYear() || mese < (today.getMonth() + 1) || (mese == (today.getMonth() +1) && giorno < 11)) {
//		     if (!confirm("Attenzione: la data di uscita impostata si riferisce ad un periodo di certificazione precedente a quello corrente. Procedere ugualmente?")) {
//		   	 	return
//		   	 }
//		   }
		}
		for (k = 1; k < codici.length; k++) {
			if (!checks[k].checked)  { // l'LSU non � stato escluso dall'operazione
				// escludiamo l'LSU dal salvataggio
				lista[k].value="0";
			} 
		}
		parent.titoloInterno.location = "TitoloInterno.asp"
		parent.frameLettere.location = "Lettere.asp"
		parent.headerInterno.location = "HeaderTabella.asp"
		parent.barraBottoni.location = "Bottoni.asp"
		parent.mainInterno.document.forms[0].submit()
	}
	
	// FUNZIONI PER SOSPENSIONE

	function sospensione() {
		options = parent.mainInterno.document.FormDati.codice
		count = parent.mainInterno.document.FormDati.count.value;
		for (i = 0; i < options.length; i++)  {
			if (options[i].checked) {
				count++
			}
		}
		if (count > 0)  {
			parent.frameLettere.location = "Blank.asp"
			parent.headerInterno.location = "HeaderTabella.asp?mode=sospensione"
			parent.barraBottoni.location = "Bottoni.asp?mode=sospensione"
			parent.mainInterno.document.FormDati.target = ""
			parent.mainInterno.document.FormDati.action = "ListaSospensione.asp"
			parent.mainInterno.document.FormDati.submit()
		} else {
			alert("Attenzione: nessun nominativo selezionato per la sospensione")	
			return
		}
	}
	
	function confermaSospensione() {
		checks = parent.mainInterno.document.forms[0].escludi
		codici = parent.mainInterno.document.forms[0].codice
		inizio = document.forms[0].data
		lista  = parent.mainInterno.document.forms[0].lista
		mat    = document.forms[0].maternita
		if (isEmpty(inizio.value)) {
			alert("Specificare la data di inizio sospensione ")
			inizio.select()
			inizio.focus
			return	
		} else {
			data = inizio.value
			igiorno = data.indexOf("/")
			imese   = data.indexOf("/", igiorno+1)
			giorno  = parseInt(data.substring(0, igiorno))
			mese    = parseInt(data.substring(igiorno+1, imese))
			anno    = parseInt(data.substring(imese+1, data.length))
			if (isNaN(giorno) || isNaN(mese) || isNaN(anno)) {
				alert("Data non valida")
				inizio.select()
				inizio.focus()
				return
			}
			today = new Date()
		// alert("Oggi  = "+today.getDate()+"/"+(today.getMonth()+1)+"/"+today.getYear())
		// controlliamo se la data di uscita si riferisce ad un periodo di certificazione
		// precendente a quello corrente, cio� se il mese della data di uscita � precedente 
		// allo scorso mese o se � proprio uguale allo scorso mese ma il giorno di uscita �
		// precedente all'11 di tale mese
//			if (anno < today.getYear() || mese < (today.getMonth() + 1) || (mese == (today.getMonth() +1) && giorno < 11)) {
//				if (!confirm("Attenzione: la data di inizio sospensione impostata si riferisce ad un periodo di certificazione precedente a quello corrente. Procedere ugualmente?")) {
//					return
//				}
//			}
			parent.mainInterno.document.forms[0].inizioSospensione.value = inizio.value
			if (mat.checked) {
				parent.mainInterno.document.forms[0].maternita.value = '1'
			} else {
				parent.mainInterno.document.forms[0].maternita.value = '0'
			}
		}
		for (k = 1; k < codici.length; k++) {
			if (!checks[k].checked)  { // l'LSU non � stato escluso dall'operazione
				// escludiamo l'LSU dal salvataggio
				lista[k].value="0";
			}
		}
		parent.titoloInterno.location = "TitoloInterno.asp"
		parent.frameLettere.location = "Lettere.asp"
		parent.headerInterno.location = "HeaderTabella.asp"
		parent.barraBottoni.location = "Bottoni.asp"
		parent.mainInterno.document.forms[0].submit()
	}
			
	// FUNZIONI PER ESCLUSIONE

	function esclusione() {
		options = parent.mainInterno.document.FormDati.codice
		count = parent.mainInterno.document.FormDati.count.value;
		for (i = 0; i < options.length; i++)  {
			if (options[i].checked) {
				count++
			}
		}
		if (count > 0)  {
			parent.frameLettere.location = "Blank.asp"	
			parent.headerInterno.location = "HeaderTabella.asp?mode=esclusione"
			parent.barraBottoni.location = "Bottoni.asp?mode=esclusione"		
			parent.mainInterno.document.FormDati.target = ""
			parent.mainInterno.document.FormDati.action = "ListaEsclusione.asp"
			parent.mainInterno.document.FormDati.submit()
		} else {
			alert("Attenzione: nessun nominativo selezionato per l'esclusione")	
			return
		}
	}
	
	function confermaEsclusione() {
		checks = parent.mainInterno.document.forms[0].escludi
		codici = parent.mainInterno.document.forms[0].codice
		lista  = parent.mainInterno.document.forms[0].lista
		for (k = 1; k < codici.length; k++) {
			if (!checks[k].checked)  { 
				// escludiamo l'LSU dal salvataggio
				lista[k].value="0";
			}
		}
		parent.titoloInterno.location = "TitoloInterno.asp"
		parent.frameLettere.location = "Lettere.asp"	
		parent.headerInterno.location = "HeaderTabella.asp"
		parent.barraBottoni.location = "Bottoni.asp"
		parent.mainInterno.document.forms[0].submit()
	}
			
	// FUNZIONI PER RIATTIVAZIONE
	function riattivazione() {
		options = parent.mainInterno.document.FormDati.codice
		count = parent.mainInterno.document.FormDati.count.value;
		for (i = 0; i < options.length; i++)  {
			if (options[i].checked) {
				count++
			}
		}
		if (count > 0)  {
			parent.frameLettere.location = "Blank.asp"	
			parent.headerInterno.location = "HeaderTabella.asp?mode=riattivazione"
		    parent.barraBottoni.location = "Bottoni.asp?mode=riattivazione"	
			parent.mainInterno.document.FormDati.target = ""
			parent.mainInterno.document.FormDati.action = "ListaRiattivazione.asp"
			parent.mainInterno.document.FormDati.submit()	
				
		} else {
			alert("Attenzione: nessun nominativo selezionato per la riattivazione")	
			return
		}
	}
	
	function confermaRiattivazione() {
		checks = parent.mainInterno.document.forms[0].escludi
		codici = parent.mainInterno.document.forms[0].codice
		lista  = parent.mainInterno.document.forms[0].lista
		inizio = parent.mainInterno.document.forms[0].inizioSospensione
		fine   = document.forms[0].data
		if (isEmpty(fine.value)) {
			alert("Specificare la data di fine sospensione")
			fine.select()
			fine.focus
			return	
		} else {
			parent.mainInterno.document.forms[0].fineSospensione.value = document.forms[0].data.value
		}
		dataFine = fine.value
		
		giorno  = parseInt(dataFine.substring(0, igiorno))
		mese    = parseInt(dataFine.substring(igiorno+1, imese))
		anno    = parseInt(dataFine.substring(imese+1, data.length))
		
		if(isNaN(giorno) || isNaN(mese) || isNaN(anno))  {
			alert("Data non valida")
			d.select()
			d.focus()
			return
		}
		
		endDate = new Date(anno, mese - 1, giorno)
				
		for (k = 1; k < codici.length; k++) {
			if (!checks[k].checked)  { 
				// escludiamo l'LSU dal salvataggio
				lista[k].value="0";
			} else {
				dataInizio = inizio[k].value
				
				giornoi  = parseInt(dataInizio.substring(0, igiorno))
				mesei    = parseInt(dataInizio.substring(igiorno+1, imese))
				annoi    = parseInt(dataInizio.substring(imese+1, data.length))
				
				startDate = new Date(annoi, mesei - 1, giornoi)
				if (endDate.getTime() < startDate.getTime())  {
					 	alert("La data di fine sospensione non pu� essere precedente alla data di inizio sospensione")
					fine.select()
					fine.focus()
					return	  	
				}
			}
		}
		parent.titoloInterno.location = "TitoloInterno.asp"
		parent.frameLettere.location = "Lettere.asp"	
		parent.headerInterno.location = "HeaderTabella.asp"
        parent.barraBottoni.location = "Bottoni.asp"
		parent.mainInterno.document.forms[0].submit()
	}
	
	
	// FUNZIONI VARIAZIONE DATI
	
	function variazione() {
		options = parent.mainInterno.document.FormDati.codice
		count = parent.mainInterno.document.FormDati.count.value;
		for (i = 0; i < options.length; i++)  {
			if (options[i].checked) {
				count++
			}
		}
		if (count > 0)  {
			parent.frameLettere.location = "Blank.asp"	
			parent.headerInterno.location = "HeaderTabella.asp?mode=variazione"
			parent.barraBottoni.location = "Bottoni.asp?mode=variazione"		
			parent.mainInterno.document.FormDati.target = ""
			parent.mainInterno.document.FormDati.action = "ListaVariazione.asp"
			parent.mainInterno.document.FormDati.submit()
		} else {
			alert("Attenzione: nessun nominativo selezionato per la variazione dati")	
			return
		}
	}
		
	function confermaVariazione() {
		checks = parent.mainInterno.document.forms[0].escludi
		codici = parent.mainInterno.document.forms[0].codice
		mansioni = parent.mainInterno.document.forms[0].mansione
		settori  = parent.mainInterno.document.forms[0].settore
		for (k = 1; k < codici.length; k++) {
			if (checks[k].checked)  { // l'LSU non � stato escluso dall'operazione
				if (isEmpty(mansioni[k].value)) {
					alert("Tutti i campi sono obbligatori")
					mansioni[k].select()
					mansioni[k].focus()
					return
				}
				if (isEmpty(settori[k].value)) {
					alert("Tutti i campi sono obbligatori")
					settori[k].select()
					settori[k].focus()
					return
				}
			} else {
				// escludiamo l'LSU dal salvataggio
				lista[k].value="0";
			}
		}

		parent.frameLettere.location = "Lettere.asp"
		parent.headerInterno.location = "HeaderTabella.asp"
		parent.barraBottoni.location = "Bottoni.asp"
		parent.mainInterno.document.forms[0].submit()
	}
			
	// FUNZIONI PER L'INSERIMENTO

	function inserimento() {
		parent.frameLettere.location = "Blank.asp"
		parent.mainInterno.location = "ListaInserimento.asp"
		parent.headerInterno.location = "Blank.asp"
		parent.barraBottoni.location = "Bottoni.asp?mode=inserimento"
	}

	function confermaInserimento() {
//		codice  = parent.mainInterno.document.forms[0].codfiscale
//		nome    = parent.mainInterno.document.forms[0].nome
//		cognome = parent.mainInterno.document.forms[0].cognome
//		mansione= parent.mainInterno.document.forms[0].mansione
//		settore = parent.mainInterno.document.forms[0].settore
//		if (isEmpty(nome.value) || isEmpty(cognome.value) || isEmpty(codice.value)||
//			isEmpty(mansione.value) || isEmpty(settore.value))  {
//			alert("Tutti i campi sono obbligatori")
//			nome.select()
//			nome.focus()
//			return
//		}
//		if(confirm("Vuoi inserire un altro LSU?")){
//			parent.mainInterno.document.forms[0].ancora.value = "si"
//		} else {
//			parent.mainInterno.document.forms[0].ancora.value = "no"
//			parent.frameLettere.location = "Lettere.asp"
//			parent.headerInterno.location = "HeaderTabella.asp"
//			parent.barraBottoni.location = "Bottoni.asp"
//		}
//		parent.mainInterno.document.forms[0].submit()
	}
			
	</script>
<head>
</head>
<body bgcolor="white" leftmargin="0" rightmargin="0" topmargin="0">
<%
   if modo = "" then
%>
<table width="100%" height="100%" cellspacing="0" cellpadding="0">
  <tr>
	<td align="center"><a class="gen" href="javascript:segnalaUscita()">Uscita</a></td>
	<td align="center"><a class="gen" href="javascript:sospensione()">Sospensione</a></td>
	<td align="center"><a class="gen" href="javascript:riattivazione()">Riattivazione</a></td>
	<td align="center"><a class="gen" href="javascript:variazione()">Modifica</a></td>
	<% if (session("certificazione") = "enabled") then %>
		<td align="center"><a class="gen" href="javascript:inserimento()">Inserimento</a></td>
	<% else %>
		<td align="center"><a class="bottoneDisabilitato" href="javascript:alert('Operazione disponibile solo nella fase di validazione iniziale dei dati')">Inserimento</a></td>
	<% end if%>
	<% if (session("certificazione") = "enabled") then %>
		<td align="center"><a class="gen" href="javascript:esclusione()">Esclusione</a></td>
	<% else %>
		<td align="center"><a class="bottoneDisabilitato" href="javascript:alert('Operazione disponibile solo nella fase di validazione iniziale dei dati')">Esclusione</a></td>
	<% end if%>
  </tr>
</table>
<%
   elseif modo = "uscita" then
%>
<form>
<table width="100%" height="100%" cellspacing="0" cellpadding="0">
  <tr>
	<td align="left">
		Data di uscita (gg/mm/aaaa)<input type="text" name="data" onBlur="javascript:validaData(this)" maxlength="10">
		<a class="miniBottone" href="javascript:mostraCalendario()" >...</a>
	</td>
	<td align="left">Causale <input type="text" name="causale" value="" maxlength="50"></td>
    <td align="center"><a class="gen" href="javascript:confermaUscita()">Conferma</a></td>
	<td align="center"><a class="gen" href="javascript:annulla()">Indietro</a></td>
  </tr>
</table>
</form>
<%
   elseif modo = "sospensione" then
%>
<form>
<table width="100%" height="100%" cellspacing="0" cellpadding="0">
  <tr>
    <td align="left">
		Inizio Sospensione (gg/mm/aaaa)<input type="text" name="data" onBlur="javascript:validaData(this)" maxlength="10">
		<a class="miniBottone" href="javascript:mostraCalendario()" >...</a>
	</td>
    <td align="left">Sospensione per maternita' <input type="checkbox" name="maternita"></td>
    <td align="center"><a class="gen" href="javascript:confermaSospensione()">Conferma</a></td>
	<td align="center"><a class="gen" href="javascript:annulla()">Indietro</a></td>
  </tr>
</table>
</form>
<%
   elseif modo = "riattivazione" then
%>
<form>
<table width="100%" height="100%" cellspacing="0" cellpadding="0">
  <tr>
    <td align="left">
		Fine sospensione (gg/mm/aaaa)<input type="text" name="data" onBlur="javascript:validaData(this)" maxlength="10">
		<a class="miniBottone" href="javascript:mostraCalendario()" >...</a>
	</td>
    <td align="center"><a class="gen" href="javascript:confermaRiattivazione()">Conferma</a></td>
	<td align="center"><a class="gen" href="javascript:annulla()">Indietro</a></td>
  </tr>
</table>
</form>
<%
   elseif modo = "esclusione" then
%>
<table width="100%" height="100%" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center"><a class="gen" href="javascript:confermaEsclusione()">Conferma</a></td>
	<td align="center"><a class="gen" href="javascript:annulla()">Indietro</a></td>
  </tr>
</table>
<%
   elseif modo = "variazione" then
%>
<table width="100%" height="100%" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center"><a class="gen" href="javascript:confermaVariazione()">Conferma</a></td>
	<td align="center"><a class="gen" href="javascript:annulla()">Indietro</a></td>
  </tr>
</table>
<%
   elseif modo = "inserimento" then
%>
<table width="100%" height="100%" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center"><a class="gen" href="javascript:confermaInserimento()">Conferma</a></td>
	<td align="center"><a class="gen" href="javascript:annulla()">Indietro</a></td>
  </tr>
</table>
<%
   elseif modo = "errore" then
%>
<table width="100%" height="100%" cellspacing="0" cellpadding="0">
  <tr>
	<td align="center"><a class="gen" href="javascript:annulla()">Indietro</a></td>
  </tr>
</table>
<%
   end if
%>
</body>
</html>
