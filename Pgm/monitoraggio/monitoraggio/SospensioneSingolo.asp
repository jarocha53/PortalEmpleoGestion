<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%@ LANGUAGE=VBSCRIPT %>

<!-- #include file="adovbs.inc" -->

<%	
	'''
	'' ---------------> CONTROLLO VALIDITA' SESSIONE
	'
	if(isNull(Session)) then
		Server.transfer("Redirect.html")
	end if
	
	Set connection = Server.createObject("ADODB.Connection")
		connection.open(session("connectString"))
		
%>

<%
	'''
	'' --------------->   CONTROLLO AUTENTICAZIONE UTENTE
	'
	if (Session("ente") = null or Session("ente") = "") then 
		Response.redirect("Login.html")
		return
	end if
%>
<%
	codice = request.queryString("codice")
	
	query = "SELECT P.ID_PERSONA, P.NOME, P.COGNOME, P.COD_FISC, PS.DT_INIZIO_SOSP, PS.DT_FINE_SOSP, PS.FL_MATERNITA " & _
	        "FROM PERSONA P, TMP_PERS_SOSP PS " & _
			"WHERE PS.ID_PERSONA = P.ID_PERSONA AND  PS.PROGRESSIVO = (SELECT MAX(PROGRESSIVO) FROM TMP_PERS_SOSP WHERE ID_PERSONA = " & codice & ") AND " & _
			"P.ID_PERSONA = " & codice
			
	Set res = Server.createObject("ADODB.Recordset")
'PL-SQL * T-SQL  
QUERY = TransformPLSQLToTSQL (QUERY) 
	Set res = connection.execute(query)
	
%>
<html>
<style type="text/css">
<!--
	a.miniBottone {
		font-family: tahoma;
		font-size: 8;
		font-weight: bold;
		text-decoration: none;
		vertical-align: middle;
		border: 1 solid black;
		color: black;
	 }
	 a.gen {
	    font-family: tahoma;
	    font-size: 12;
		font-weight: bold;
		color: white;
		text-align: center;
		border: 1 solid black;
		background-color: #006796;
		text-decoration: none;
		padding: 2;
		margin: 0;
		vertical-align: middle;
		width: 80;
	}
	td {
	 		font-family: tahoma;
			font-size: 10;
			font-weight: bold;
	 }
	 
	 td.cfis {
	 		font-family: tahoma;
			font-size: 10;
	 }
	 
	 select {
	        border: 1 solid black;
			width: 120;
			font-family: tahoma;
			font-size: 10;
			font-weight: bold;
			padding: 2;
			background: white;
			margin: 0;
	 }
	 
	 input {
			width: 80;
			font-family: tahoma;
			font-size: 10;
			font-weight: bold;
			padding: 2;
			background: white;
			margin: 0;
	 }	 
-->
</style>
	<head>
	</head>
	<script src="utils/utility.js"></script>
	<script>
		
		function mostraCalendario() {
		    finestra = window.open("Calendar.html", "Calendario", "width=250,height=210,noresize,nomaximize");
		}
		
		function mostraCalendario2() {
		    finestra = window.open("Calendar2.html", "Calendario", "width=250,height=210,noresize,nomaximize");
		}
		
		function confermaSospensione() {
			codici = document.forms[0].codice
			inizio = document.forms[0].data
			fine   = document.forms[0].data2
			lista  = document.forms[0].lista
			mat    = document.forms[0].maternita
			
			if (isEmpty(inizio.value)) {
				alert("Specificare la data di inizio sospensione ")
				inizio.select()
				inizio.focus
				return	
			} 
			d = inizio.value
			if (!isEmpty(d)) {
				igiorno = d.indexOf("/")
				imese   = d.indexOf("/", igiorno+1)
				
				giorno  = parseInt(d.substring(0, igiorno))
				mese    = parseInt(d.substring(igiorno+1, imese))
				anno    = parseInt(d.substring(imese+1, d.length))
				
				if (isNaN(giorno) || isNaN(mese) || isNaN(anno)) {
					alert("Data non valida")
					return
					data.select()
					data.focus()
				}
				today = new Date()
				// controlliamo se la data di uscita si riferisce ad un periodo di certificazione
				// precendente a quello corrente, cio� se il mese della data di uscita � precedente 
				// allo scorso mese o se � proprio uguale allo scorso mese ma il giorno di uscita �
				// precedente all'11 di tale mese
//				if (anno < today.getYear() || mese < (today.getMonth() + 1) || (mese == (today.getMonth() +1) && giorno < 11)) {
//					if (!confirm("Attenzione: la data di inizio sospensione impostata si riferisce ad un periodo di certificazione precedente a quello corrente. Procedere ugualmente?")) {
//						return
//					}
//				}
			}
			// FIXME rivedere l'istanziazione delle date
			dataI = new Date(inizio.value)
			dataF = new Date(fine.value)
			if (dataF - dataI <= 0)  {
				alert("La data di fine sospensione non pu� essere precedente alla data di inizio sospensione")
				fine.select()
				fine.focus()
				return	  	
			}
			document.forms[0].inizioSospensione.value = inizio.value
			document.forms[0].fineSospensione.value = fine.value
			if (document.forms[0].flag_maternita.checked) {
				document.forms[0].maternita.value = '1'
			} else {
				document.forms[0].maternita.value = '0'
			}
			
			parent.titoloInterno.location = "TitoloInterno.asp"
			parent.frameLettere.location = "Lettere.asp"
			parent.headerInterno.location = "HeaderTabella.asp"
	        parent.barraBottoni.location = "Bottoni.asp"
			document.forms[0].submit()
		}
		
		function annullaRegistrazione()	{
			parent.titoloInterno.location = "TitoloInterno.asp"
			parent.frameLettere.location = "Lettere.asp"
			parent.headerInterno.location = "HeaderTabella.asp"
	  		parent.barraBottoni.location = "Bottoni.asp"
			document.forms[0].action = "CancellazioneSospensione.asp"
			document.forms[0].submit()
		}
		
		function mostraCalendario() {
		    finestra = window.open("Calendar.html", "Calendario", "width=250,height=210,noresize,nomaximize");
		}
		
		function mostraCalendario2() {
			    finestra = window.open("Calendar2.html", "Calendario", "width=250,height=210,noresize,nomaximize");
		}
		
	</script>
	<body bgcolor="white" leftmargin="0" rightmargin="0" topmargin="0">
	    <script src="utils/utility.js"></script>
		<form method="post" action="SalvataggioSospensione.asp">
		<table border="0" cellpadding="2" style="border-bottom: 1 solid black; border-left: 1 solid black; border-right: 1 solid black" width="100%" cellspacing="0" cellpadding="0">
		
		<%
		'il campo dopoOperazione viene messo a 'no' solo dalle funzioni
		'javascript per la selezione dei filtri. Se vale 'si' la sessione
		'viene ripulita al caricamento di questa pagina. Questo evita il
		'problema della propagazione dei dati della form.
		%>
		<input type="hidden" name="dopoOperazione" value="si"/>
		
			<!-- per fare in modo che sia sempre un array -->
				<input type="hidden" name="codice" value="0">
				<input type="hidden" name="lista" value="0">		
		<%
			if not res.EOF then
		%>
			<tr>
			    <input type="hidden" name="codice" value="<%=res("id_persona")%>">
				<input type="hidden" name="maternita" value="0">
				<input type="hidden" name="inizioSospensione" value="<%=res("dt_inizio_sosp")%>">
				<input type="hidden" name="fineSospensione" value="<%=res("dt_fine_sosp")%>">

				<td width="120" align="left"><%=res("cognome")%></td>				
				<td width="120" align="left"><%=res("nome")%></td>
				<td width="120" align="left"><%=Ucase(res("cod_fisc"))%></td>
				<td width="100" align="center" valign="middle">
					<input type="text" name="data" maxlength="10" value="<%=res("dt_inizio_sosp")%>">
					<a class="miniBottone" href="javascript:mostraCalendario()" >...</a>
				</td>
				<td width="100" align="center" valign="middle">
					<input type="text" name="data2" maxlength="10" value="<%=res("dt_fine_sosp")%>">
					<a class="miniBottone" href="javascript:mostraCalendario2()" >...</a>
				</td>
				<% if (res("fl_maternita") = 1) then %>
					<td width="50" align="center"><input name="flag_maternita" type="checkbox" checked="true"></td>
				<% else %>
					<td width="50" align="center"><input name="flag_maternita" type="checkbox"></td>						
				<% end if %>
				<input type="hidden" name="lista" value="1">
			</tr>
		
		<%
		    end if
		%>
		</table>
		<form>
		<br>
		<table align="right" cellspacing="20" cellpadding="0">
		  <tr>
		    <td align="center"><a class="gen" href="javascript:confermaSospensione()">Conferma</a></td>
			<td align="center">
				<a class="gen" href="javascript:annullaRegistrazione()">Cancella</a>
			</td>
			<td align="center"><a class="gen" href="javascript:annulla()">Indietro</a></td>
		  </tr>
		</table>
		</form>
	</body>
</html>
<%
	res.close
	connection.close
%>
