<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%@ LANGUAGE=VBSCRIPT %>

<% response.expires = -1500 %>



<!-- #include file="adovbs.inc" -->

<%	
	'''
	'' ---------------> CONTROLLO VALIDITA' SESSIONE
	'
	if(isNull(Session)) then
		Server.transfer("Redirect.html")
	end if
	
	Set connection = Server.createObject("ADODB.Connection")
		connection.open(session("connectString"))
		
%>
<%
	'''
	'' --------------->   CONTROLLO AUTENTICAZIONE UTENTE
	'
	if (Session("ente") = null or Session("ente") = "") then 
		Response.redirect("Login.html")
		return
	end if
%>


<html>
	<head>
		<style type="text/css">
			 td {
			 		font-family: tahoma;
					font-size: 12;
					font-weight: bold;
			 }
			 
			 td.cfis {
			 		font-family: arial;
					font-size: 12;
					font-weight: bold;
			 }
			 
			 a.miniBottone {
			 		font-family: tahoma;
					font-size: 8;
					font-weight: bold;
					text-decoration: none;
					vertical-align: middle;
					border: 1 solid black;
					color: black;
			 }
		</style>
	</head>
	<body bgcolor="white" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
		<table border="0" cellpadding="1" style="border-left: 1 solid black; border-right: 1 solid black" width="100%" cellspacing="0">
		<tr>
			<td>Cognome</td>
			<td>Nome</td>
			<td>Cod. Fis</td>
		<%
			if(filtro = "attivi") then
		%>
			<td>Sospeso</td>
			<td>Inzio sospensione</td>
		<%
			else if (filtro = "sospesi") then
		%>
			<td>Inzio sospensione</td>
			<td>Fine sospensione</td>
		<%
			else
		%>
			<td>Data uscita</td>
			<td>Causale uscita</td>
		<%
			end if
		%>
		</tr>


		<% 
			Dim filtro
			filtro = Request.queryString("filtro")
			if(filtro = null or filtro = "") then
				filtro = "attivi"
			end if

			if(filtro = "sospesi") then
				query = "SELECT P.ID_PERSONA, P.NOME, P.COGNOME, P.COD_FISC, " & _		
					" PS.DT_INIZIO_SOSP, PS.DT_FINE_SOSP " & _
					"FROM   PERSONA P, PERS_ENTE PE, PERS_SOSP PS " & _
					"WHERE  PE.ID_IMPRESA = " & Request("vedi") & " AND PE.ID_PERSONA = P.ID_PERSONA AND " & _
					       "PE.FL_STATO_PERSONA = 'S' AND PE.CERTIFICATO = 'S'"
			elseif(filtro = "usciti") then
				query = "SELECT P.ID_PERSONA, P.NOME, P.COGNOME, P.COD_FISC, " & _		
					"PE.DT_USCITA, PE.CAUSALE_USCITA " & _
					"FROM   PERSONA P, PERS_ENTE PE " & _
					"WHERE  PE.ID_IMPRESA = " & Request("vedi") & " AND PE.ID_PERSONA = P.ID_PERSONA AND " & _
					       "PE.FL_STATO_PERSONA = 'U' AND PE.CERTIFICATO = 'S'"
			elseif(filtro = "attivi") then
				query = "SELECT P.ID_PERSONA, P.NOME, P.COGNOME, P.COD_FISC, " & _		
					"'S' SOSPESO, 'DATA' DATA" & _
					"FROM   PERSONA P, PERS_ENTE PE " & _
					"WHERE  PE.ID_IMPRESA = " & Request("vedi") & " AND PE.ID_PERSONA = P.ID_PERSONA AND " & _
					       "PE.FL_STATO_PERSONA IN ('A','S') AND PE.CERTIFICATO = 'S' AND "
			end if
			Set res = Server.createObject("ADODB.Recordset")
'PL-SQL * T-SQL  
QUERY = TransformPLSQLToTSQL (QUERY) 
			Set res = connection.execute(query)
	
			Do while NOT res.eof
				codice = res("id_persona")
				nome = res("nome")
				cognome = res("cognome")
				cdfiscale = res("cod_fisc")
		%>
			<tr>
				<td width="180" align="left" bgcolor="<%=colore%>"><%=cognome%></td>
				<td width="180" align="left" bgcolor="<%=colore%>"><%=nome%></td>
				<td width="150" class="cfis" bgcolor="<%=colore%>" align="left"><%=Ucase(cdfiscale)%></td>
				<%
					if(filtro = "attivi") then
				%>
					<td width="180" align="left" bgcolor="<%=colore%>"><%=res("sospeso")%></td>
					<td width="180" align="left" bgcolor="<%=colore%>"><%=res("data")%></td>
				<%
					else if (filtro = "sospesi") then
				%>
					<td width="180" align="left" bgcolor="<%=colore%>"><%=res("dt_inizio_sosp")%></td>
					<td width="180" align="left" bgcolor="<%=colore%>"><%=res("dt_fine_sosp")%></td>
				<%
					else
				%>
					<td width="180" align="left" bgcolor="<%=colore%>"><%=res("dt_uscita")%></td>
					<td width="180" align="left" bgcolor="<%=colore%>"><%=res("causale_uscita")%></td>
				<%
					end if
				%>
			</tr>
			<tr>
			   <td colspan="5" height="1" bgcolor="black"></td>
			</tr>

		<%
			    res.Movenext
				Loop
			res.Close 
			connection.close
		%>

		</table>
	</body>
</html>
