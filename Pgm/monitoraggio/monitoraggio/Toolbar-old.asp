<%
	mode = session("mode")
	' la certificazione � abilitata solo dal 5 al 10 del mese
	  today = Date()
	  if datepart("d", today) >= 5 and datepart("d", today) <= 10 then
	  	certifica = true
	  else
	  	certifica = false
	  end if
%>

<table bgcolor="#006699" border="0" style="border: 1 solid black" cellpadding="2" width="100%" width="20" cellspacing="0" cellpadding="0">
	<tr>
		<td class="header" align="center">
			<%
			  if not(mode = "read_only") and certifica then 
			%>
			    <a href="javascritp:certificaDati()"><img src="images/certifica.jpg" border="0" alt="Certifica i dati"></a>
			<% 
			  else
			%>
			  	<img src="images/certificaOff.jpg" border="0" alt="Certifica i dati">
			<%
			  end if
			%>
		</td>
		<td class="header" align="center">
			<%
			  if not(mode = "read_only") then 
			%>
			    <a href="javascritp:segnalaUscita()"><img src="images/uscita.jpg" border="0" alt="Segnala l'uscita"></a>
			<% 
			  else
			%>
			  	<img src="images/uscitaOff.jpg" border="0" alt="Segnala l'uscita">
			<%
			  end if
			%>
		</td>
		<td class="header" align="center">
		  	<%
			  if not(mode = "read_only") then 
			%>
			    <a href="javascritp:segnalaSospensione()"><img src="images/sospensione.jpg" border="0" alt="Segnala la sospensione"></a>
			<% 
			  else
			%>
			  	<img src="images/sospensioneOff.jpg" border="0" alt="Segnala la sospensione">
			<%
			  end if
			%>
		</td>
		<td class="header" align="center">
			<%
			  if not(mode = "read_only") then 
			%>
			    <a href="javascritp:segnalaSconosciuto()"><img src="images/esclusione.jpg" border="0" alt="Segnala LSU sconosciuto"></a>
			<% 
			  else
			%>
			  	<img src="images/sconosciutoOff.jpg" border="0" alt="Segnala LSU sconosciuto">
			<%
			  end if
			%>
		</td>
		<td class="header" align="center">
		  	<%
			  if not(mode = "read_only") then 
			%>
			    <a href="javascritp:segnalaVariazione()"><img src="images/variazione.jpg" border="0" alt="Variazione dati"></a>
			<% 
			  else
			%>
			  	<img src="images/variazioneOff.jpg" border="0" alt="Variazione dati">
			<%
			  end if
			%>
		</td>
		<td class="header" align="center">
		  	<%
			  if not(mode = "read_only") then 
			%>
			    <a href="javascritp:segnalaInserimento()"><img src="images/inserimento.jpg" border="0" alt="Inserimento LSU"></a>
			<% 
			  else
			%>
			  	<img src="images/inserimentoOff.jpg" border="0" alt="Inserimento LSU">
			<%
			  end if
			%>
		</td>
	</tr>
</table>