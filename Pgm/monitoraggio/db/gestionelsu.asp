<%
Dim LetterKeys(25)
	LetterKeys(0) = "A"
	LetterKeys(1) = "B"
	LetterKeys(2) = "C"
	LetterKeys(3) = "D"
	LetterKeys(4) = "E"
	LetterKeys(5) = "F"
	LetterKeys(6) = "G"
	LetterKeys(7) = "H"
	LetterKeys(8) = "I"
	LetterKeys(9) = "J"
	LetterKeys(10) = "K"
	LetterKeys(11) = "L"
	LetterKeys(12) = "M"
	LetterKeys(13) = "N"
	LetterKeys(14) = "O"
	LetterKeys(15) = "P"
	LetterKeys(16) = "Q"
	LetterKeys(17) = "R"
	LetterKeys(18) = "S"
	LetterKeys(19) = "T"
	LetterKeys(20) = "U"
	LetterKeys(21) = "V"
	LetterKeys(22) = "W"
	LetterKeys(23) = "X"
	LetterKeys(24) = "Y"
	LetterKeys(25) = "Z"

	Function contaLSUInSessione()
		contaLSUInSessione = 0
		Dim tmp
		tmp = session("AZ")
		if(not isNull(tmp) and not isEmpty(tmp) and isArray(tmp)) then
			contaLSUInSessione = UBound(tmp) - LBound(tmp)
		else
			for each le in LetterKeys
				tmp = session(le)
				if(not isNull(tmp) and not isEmpty(tmp) and isArray(tmp)) then
					contaLSUInSessione = contaLSUInSessione + UBound(tmp) - LBound(tmp)
				end if
			Next
		end if
	end Function

	Sub stampaStato()
		Response.write("+-------------------<br>Stato<br>")
		Dim tmp
		tmp = session("AZ")
		if(not isNull(tmp) and not isEmpty(tmp) and isArray(tmp)) then
			Response.write("Numero lsu in AZ: " & (UBound(tmp) - LBound(tmp)) & "<br>")
		else
			for each le in LetterKeys
				tmp = session(le)
				if(not isNull(tmp) and not isEmpty(tmp) and isArray(tmp)) then
					Response.write("Numero lsu in " & le & ": " & (UBound(tmp) - LBound(tmp)) & "<br>")
				end if
			Next
		end if
		Response.write("+-------------------<br>")
	End Sub

	Sub rimuoviAZ()
		session.contents.remove("AZ")
		session("AZ") = null
	End Sub

	Sub rimuoviLettere()
		For Each le in LetterKeys
			session.contents.remove(le)
			session(le) = null
		Next
	End Sub
%>