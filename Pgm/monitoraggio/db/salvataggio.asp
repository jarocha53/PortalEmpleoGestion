<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%
	connectString = "Provider=MSDAOra;user id=lsu; password=lsu; Data Source=data source = ejobtest"
'	"DSN=pdb;UID=lsu;PWD=lsu"
	'############################################################################
	'							VARIAZIONE DATI
	'############################################################################
	sub salvaVariazioni
	
		Set connection = Server.createObject("ADODB.Connection")
			connection.open(connectString)
	   
		codici    = split(request.Form("codice"), ",")
		lista     = split(request.Form("lista"), ",")
		
		mansioni  = split(request.Form("mansione"), ",")
		settori  = split(request.Form("settore"), ",")
		
		Set res = Server.createObject("ADODB.Recordset")
		
		 for k = Lbound(codici)+1 to Ubound(codici)
			if (lista(k) > 0) then
				query  = "SELECT ID_PERSONA, COD_SETTORE_LSU, COD_MANSIONE_LSU " & _
		                 "FROM PERS_ENTE " &_
				         "WHERE ID_PERSONA = " & codici(k) & " AND ID_IMPRESA = " & session("ente")
				res.open query, connection, 3, 3
				
				res("cod_mansione_lsu") = mansioni(k)
				res("cod_settore_lsu")    = settori(k)
				res.update()
				res.close()
			
			end if
		next
		Set res = nothing
		connection.close()
		Set connection = nothing
	
	end sub
	
   
   
   '############################################################################
   '							INSERIMENTO
   '############################################################################
   sub segnalaInserimenti
   	Set connection = Server.createObject("ADODB.Connection")
		connection.open(connectString)
	   
		nome      = request.Form("nome")
		cognome   = request.Form("cognome")
		cdfiscale = request.Form("codfiscale")
		mansione  = request.Form("mansione")
		settore   = request.Form("settore")
		
		query  = "SELECT ID_IMPRESA, ID_SEDE, TIPO_RICHIESTA, DT_RICHIESTA, " & _
				 "COGNOME_NEW, NOME_NEW, COD_FISC_NEW " & _
				 "FROM RICH_VAR_ANAG "
		
		Set res = Server.createObject("ADODB.Recordset")
			res.open query, connection, 3, 3
		
				res.addNew()
				res("id_impresa") = session("ente")
				res("id_sede")    = session("ente")
				res("tipo_richiesta") = "I"
				res("dt_richiesta")   = Date()
				res("cognome_new")    = cognome
				res("nome_new")       = nome
				res("cod_fisc_new")   = cdfiscale
		
		res.update()
		res.close()
		Set res = nothing
		connection.close()
		Set connection = nothing
   end sub
%>
