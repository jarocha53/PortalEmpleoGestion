<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%@ LANGUAGE=VBSCRIPT %>

<%
	'''
	'' --------------->   CONTROLLO AUTENTICAZIONE UTENTE
	'
	if (Session("ente") = null or Session("ente") = "") then 
		Response.redirect("Login.html")
		return
	end if
	if(session("livelloUtente") > 4) then
		' QUESTO NON DEVE VEDERE QUI !
	end if
%>
<html>
	<head>
		<style type="text/css">
			 td {
			 		font-family: tahoma;
					font-size: 12;
					font-weight: bold;
			 }

			 td.cfis {
			 		font-family: arial;
					font-size: 12;
					font-weight: bold;
			 }

			 a.miniBottone {
			 		font-family: tahoma;
					font-size: 8;
					font-weight: bold;
					text-decoration: none;
					vertical-align: middle;
					border: 1 solid black;
					color: black;
			 }
		</style>
	</head>
	<body onload="parent.headerInterno.location = 'HeaderTabellaA.asp'">
		<table border="0" cellpadding="1" style="border-left: 1 solid black; border-right: 1 solid black" width="100%" cellspacing="0">
		<%

			Dim guardato
			guardato = Request.queryString("guarda")
			if(isNull(guardato) or isEmpty(guardato) or trim(guardato) = "") then
				guardato = session("ente")
			end if
			actualDate = Date()
			thisMonth = Month(actualDate)

			Set connection = Server.createObject("ADODB.Connection")
			connection.open(session("connectString"))
			query = "SELECT DISTINCT PE.ID_PERSONA, P.NOME, P.COGNOME, P.COD_FISC " & _
			        "FROM PERSONA P, PERS_ENTE PE " & _
					"WHERE P.ID_PERSONA = PE.ID_PERSONA AND " & _
					"      PE.ID_IMPRESA = " & guardato & " AND " & _
					"      PE.FL_STATO_PERSONA <> 'E' AND PE.CERTIFICATO = 'S' ORDER BY P.COGNOME, P.NOME ASC"
'PL-SQL * T-SQL  
QUERY = TransformPLSQLToTSQL (QUERY) 
			Set res = connection.execute(query)

			do while not res.eof
				codice = res("id_persona")
				nome = res("nome")
				cognome = res("cognome")
				cdfiscale = res("cod_fisc")
		%>
			<tr>
				<td width="200" align="left" bgcolor="<%=colore%>"><%=cognome%></td>
				<td width="200" align="left" bgcolor="<%=colore%>"><%=nome%></td>
				<td width="150" class="cfis" bgcolor="<%=colore%>" align="left"><%=Ucase(cdfiscale)%></td>
				<%
					queryStati = "SELECT DT_RIFERIMENTO, FL_STATO_PERSONA FROM STATO_CERTIFICATO WHERE ID_PERSONA = " & codice & " ORDER BY DT_RIFERIMENTO DESC"
					Set resStati = Server.createObject("ADODB.Recordset")
					resStati.open queryStati, connection, adOptionDynamic
					Redim bStati(11)
					do while not resStati.EOF
						for k = 0 to 11
							prec = resStati("dt_riferimento")
							if(month(prec) - 1 = k) then
								bStati(k) = resStati("fl_stato_persona")
								exit for
							end if
						next
						resStati.movenext
					loop
					resStati.close()
					Set resStati = nothing
					for k = LBound(bStati) to UBound(bStati)
						select case bStati(k)
							case "A": %>
								<td width="20" height="20" align="center" bgcolor="white">
									<img width="20" height="15" src="images/verde.gif">
								</td>
						<%	case "S": %>
								<td width="20" height="20" align="center" bgcolor="white">
									<img width="20" height="15" src="images/giallo.gif">
								</td>
						<%	
							case "U": %>
								<td width="20" height="20" align="center" bgcolor="white">
									<img width="20" height="15" src="images/rosso.gif">
								</td>
						<%	case default: %>
						
								<td width="20" height="20" align="center" bgcolor="white">
								
									<%if(k > 0 and k < thisMonth) then
										bStati(k) = bStati(k-1)
										select case bStati(k)
											case "A": %>
											
												<img width="20" height="15" src="images/verde.gif">
										<%	case "S": %>
										
												<img width="20" height="15" src="images/giallo.gif">
										<%	
											case "U": %>
											
												<img width="20" height="15" src="images/rosso.gif">
										<%	
											case default: %>
											
									<%	end select
									end if%>
								</td>
						<%
						end select
					next
					%>
			</tr>
			<tr>
			   <td colspan="15" height="1" bgcolor="black"></td>
			</tr>
		<%
				res.Movenext
				Loop
			res.Close() 
			Set res = nothing
			connection.close()
			Set connection = nothing
		%>
		</table>
	</body>
</html>
