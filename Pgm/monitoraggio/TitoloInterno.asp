<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%@ LANGUAGE=VBSCRIPT %>

<!-- #include file="adovbs.inc" -->

<% response.expires = -1500 %>

<%	
	Set connection = Server.createObject("ADODB.Connection")
		connection.open(session("connectString"))
		
%>

<%
	'''
	'' --------------->   CONTROLLO AUTENTICAZIONE UTENTE
	'
	if (Session("ente") = null or Session("ente") = "") then 
		session("ErrorMessage") = "Utente non autenticato"
		Server.transfer("Login.html")
		return
	end if
%>
<%
	Dim ente
	ente = request.querystring("guarda")
	Dim guardanull
	guardanull = 0
	if(isNull(ente) or isEmpty(ente) or ente = "") then
		ente = session("ente")
		guardanull = 1
	end if
	
	Dim entiSottoposti
	Dim count
		count = 0
	if (session("livelloUtente") > 0) then
		queryEnti = "SELECT FIGLIO FROM ASSOCIAZIONI WHERE PADRE = " & ente & " " & _
		            "UNION " & _
					"SELECT FIGLIO FROM ASSOCIAZIONI WHERE PADRE IN (SELECT FIGLIO FROM ASSOCIAZIONI WHERE PADRE = " & ente & ")"
'PL-SQL * T-SQL  
'QUERYENTI = TransformPLSQLToTSQL (QUERYENTI) 
		Set res = connection.execute(queryEnti)
		do while not(res.EOF)
			codice = res("figlio")
			if (isNull(entiSottoposti) or isEmpty(entiSottoposti)) then
				entiSottoposti = codice
			else
				count = count + 1
				entiSottoposti = entiSottoposti & ", " & codice
			end if
			res.moveNext()
		loop
		res.close()
	end if

	' Selezione degli LSU in carico
	Dim lsuAttivi
		lsuAttivi = 0
	if (session("livelloUtente") = 1) then
		queryAttivi = "SELECT COUNT(ID_PERSONA) PERSONE " & _
					  "FROM PERS_ENTE " & _
					  "WHERE FL_STATO_PERSONA IN ('A','S') AND ID_IMPRESA = " & ente
	elseif (count = 0) then
	 	queryAttivi = "SELECT COUNT(ID_PERSONA) PERSONE " & _
					  "FROM PERS_ENTE " & _
					  "WHERE FL_STATO_PERSONA IN ('A','S') AND CERTIFICATO = 'S' AND ID_IMPRESA = " & ente
	else
	 	queryAttivi = "SELECT COUNT(ID_PERSONA) PERSONE " & _
					  "FROM PERS_ENTE " & _
					  "WHERE FL_STATO_PERSONA IN ('A','S') AND CERTIFICATO = 'S' AND ID_IMPRESA IN ( " & entiSottoposti & ")"
	 end if

'PL-SQL * T-SQL  
QUERYATTIVI = TransformPLSQLToTSQL (QUERYATTIVI) 
	 Set res = connection.execute(queryAttivi)
	 if (not res.EOF) then
	 	lsuAttivi = res("persone")
	 end if
	 res.close

	 'Selezione degli LSU sospesi
	 Dim lsuSospesi
	 	lsuSospesi = 0
	 if (session("livelloUtente") = 1) then
		 querySospesi = "SELECT COUNT(ID_PERSONA) PERSONE " & _
		                "FROM PERS_ENTE " & _
						"WHERE FL_STATO_PERSONA = 'S' AND ID_IMPRESA = " & ente
	 elseif (count = 0) then
	 	querySospesi = "SELECT COUNT(ID_PERSONA) PERSONE " & _
					  "FROM PERS_ENTE " & _
					  "WHERE FL_STATO_PERSONA = 'S' AND CERTIFICATO = 'S' AND ID_IMPRESA = " & ente
	 else
	 	querySospesi = "SELECT COUNT(ID_PERSONA) PERSONE " & _
		                "FROM PERS_ENTE " & _
						"WHERE FL_STATO_PERSONA = 'S' AND CERTIFICATO = 'S' AND ID_IMPRESA IN ( " & entiSottoposti & ")"
	 end if
	 
'PL-SQL * T-SQL  
QUERYSOSPESI = TransformPLSQLToTSQL (QUERYSOSPESI) 
	 Set res = connection.execute(querySospesi)
	 if (not res.EOF) then
	 	lsuSospesi = res("persone")
	 end if
	 res.close
	 
	 'Selezione degli LSU usciti
	 Dim lsuUsciti
	 	lsuUsciti = 0
	 if (session("livelloUtente") = 1) then
		 queryUsciti = "SELECT COUNT(ID_PERSONA) PERSONE " & _
		               "FROM PERS_ENTE " & _
					   "WHERE FL_STATO_PERSONA = 'U' AND ID_IMPRESA = " & ente
	 elseif(count = 0) then
	 	queryUsciti = "SELECT COUNT(ID_PERSONA) PERSONE " & _
		               "FROM PERS_ENTE " & _
					   "WHERE FL_STATO_PERSONA = 'U' AND CERTIFICATO = 'S' AND ID_IMPRESA = " & ente
	 else
	 	queryUsciti = "SELECT COUNT(ID_PERSONA) PERSONE " & _
		               "FROM PERS_ENTE " & _
					   "WHERE FL_STATO_PERSONA = 'U' AND CERTIFICATO = 'S' AND ID_IMPRESA IN ( " & entiSottoposti & ")"
	 end if
	 
'PL-SQL * T-SQL  
QUERYSOSPESI = TransformPLSQLToTSQL (QUERYSOSPESI) 
	 Set res = connection.execute(querySospesi)
	 if (not res.EOF) then
	 	lsuUsciti = res("persone")
	 end if
	 res.close
	 
	 Dim ultimaCertificazione
	 
	 query = "SELECT MAX(DT_RIFERIMENTO ) DATARIF FROM CERTIFICAZIONE WHERE ID_IMPRESA = " & ente
'PL-SQL * T-SQL  
QUERY = TransformPLSQLToTSQL (QUERY) 
	 Set res = connection.execute(query)
	 if not res.EOF then
	 	ultimaCertificazione = res("datarif")
	end if	  

	if (isNull(ultimaCertificazione) or ultimaCertificazione = "") then
		ultimaCertificazione = "n.a."
	end if
	
	
	res.close()
	
	Dim dataCertificazione
	Dim today
	    today = Date()
	Dim mese
	giorno = day(today)
	if (giorno > 15) then
		mese = month(today)
	else
		mese = month(today) - 1
	end if 
		
	dataCertificazione = "15/"&mese&"/"&year(today)
%>
<html>
<style>
  td.infoEnte {
    font-family: tahoma;
    font-size: 11;
	font-weight: bold;
	color: #1234cc;
  }
  td.ente {
    font-family: tahoma;
    font-size: 18;
	font-weight: bolder;
	color: #6789cc;
	text-align: left;
  }
</style>
<head>
</head>
<body bgcolor="white" alink="#003366" leftmargin="0" link="#003366" rightmargin="0" topmargin="0" vlink="#003366">
<script>
	function guarda(cod) {
		parent.titoloInterno.location = "TitoloInterno.asp?guarda="+cod
		parent.headerInterno.location = "HeaderTabellaA.asp"
		parent.mainInterno.location = "MascheraA.asp?guarda="+cod
	}
</script>
<table width="100%" height="100%" cellspacing="0" cellpadding="0">
  <tr>
    <td class="ente">
	<%
		VariableSQL = "SELECT RAG_SOC FROM IMPRESA WHERE ID_IMPRESA = " & ente
	'PL-SQL * T-SQL
		VariableSQL = TransformPLSQLToTSQL(VariableSQL)  
		Set outer = connection.execute(VariableSQL)
	%>
		<%=outer("rag_soc")%>
	<%
		outer.close
		connection.close()
	%>
	</td>
	  <%
  		if(not(guardanull = 1 and session("livelloUtente") = 5)) then
		  if (session("livelloUtente") = 0) then %>
			<td align="right">
			  <table width="100%" height="100%" cellspacing="0" cellpadding="0">
			  	<tr>
					<td class="infoEnte" align="right">
						Data di ultima certificazione: <%=ultimaCertificazione%>
					</td>
				</tr>
			  </table>
			</td>
	<% end if
	 end if %>
  </tr>
<%
  			if(not(guardanull = 1 and session("livelloUtente") = 5)) then
	  %>
  <tr>
    <td class="infoEnte" align="left">
	  Numero di LSU in carico: <%=lsuAttivi%> di cui sospesi: <%=lsuSospesi%>. Numero di LSU usciti: <%=lsuUsciti%>
	</td>
	<% if (session("livelloUtente") = 0) then %>
	<td align="right">
		  <table width="100%" height="100%" cellspacing="0" cellpadding="0">
			<tr>
				<td class="infoEnte" align="right">
					Delibera n� xxxxx del xx/xx/xxxx; data di fine delibera: xx/xx/xxxx
				</td>
			</tr>
		  </table>
		</td>
	<% else %>
		<td align="right">
		  <table width="100%" height="100%" cellspacing="0" cellpadding="0">
			<tr>
				<td class="infoEnte" align="right">
					Dati al <%= dataCertificazione %>
				</td>
			</tr>
		  </table>
		</td>
	<% end if %>
  </tr>
  <% end if %>
</table>
</body>
</html>
