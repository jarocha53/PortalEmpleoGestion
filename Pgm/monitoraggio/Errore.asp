
<% response.expires = -1500 %>


<html>
<head>
	<title>Errore</title>
</head>
<style>
  td.err {
    font-family: tahoma;
    font-size: 18;
	font-weight: bold;
	color: white;
	text-align: center;
	border: 1 solid black;
	background-color: #006796;
	text-decoration: none;
	padding: 2;
	margin: 0;
	vertical-align: middle;
  }
  td.tit {
    font-family: tahoma;
    font-size: 14;
	font-weight: bold;
	color: white;
	text-align: center;
	border: 1 solid black;
	background-color: #006796;
	text-decoration: none;
	padding: 2;
	margin: 0;
	vertical-align: middle;
  }
</style>
<body>
<script>
   parent.barraBottoni.location = "Bottoni.asp?mode=errore"
</script>
<table width="100%" height="100%"><tr><td>
<table bgcolor="#006699" border="0" style="border: 1 solid black" cellpadding="2" width="100%" width="20" cellspacing="0" cellpadding="0">
	<tr>
		<td class="tit" align="center" width="10%">
		Attenzione
		</td>
	<tr>
	</tr>
		<td class="err" align="middle">
		<%=Session("messaggioErrore")%>
		</td>
	</tr>
</table>
</td></tr></table>
</body>
</html>
