<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%@ LANGUAGE=VBSCRIPT %>
<% response.expires = -1500 %>
<!-- #include file="adovbs.inc" -->
<!-- #include file="db/gestionelsu.asp" -->
<%
	'''
	'' --------------->   CONTROLLO AUTENTICAZIONE UTENTE
	'
	if (Session("ente") = null or Session("ente") = "") then 
		Response.redirect("Login.html")
		return
	end if
%>
<%	
	Set connection = Server.createObject("ADODB.Connection")
	connection.open(session("connectString"))

	Dim codici
	actualLetter = Request.Form("letteraCliccata") ' LA LETTERA CHE FILTRAVA
	                                               ' QUANDO E' STATO PREMUTO IL
												   ' TASTO FUNZIONE
	
	codici = split(Request.Form("codice"), ",")
	if(actualLetter = "AZ") then
		' NON SAREBBE NECESSARIO perch� se actual � AZ
		' allora la schermata che si vede � quella AZ
		' quindi i dati li prendiamo dalla form direttamente
		tmparray = session("AZ")
		if(not isNull(tmparray) and not isEmpty(tmparray) and isArray(tmparray)) then
			indice = UBound(codici)
			ReDim Preserve codici(indice + UBound(tmparray))
			for k = indice to UBound(codici)
				codici(k) = tmparray(k - indice)
			next
		end if
	else
		for each le in LetterKeys
			tmparray = session(le)
			if(not isNull(tmparray) and not isEmpty(tmparray) and isArray(tmparray)) then
				indice = UBound(codici)
				ReDim Preserve codici(indice + UBound(tmparray))
				for k = indice + 1  to UBound(codici)
					codici(k) = tmparray(k - indice)
				next
			end if
		next
	end if

	'codici = split(Request.Form("codice"), ",") COMMENTATO IN FAVORE DI qui sopra ^
	
	'IPOTESI: possono essere sospesi solo gli LSU attivi con dato certificato, cio� che non siano gi� stati sospesi e poi riattivati nel periodo; 
	'non possono essere effettuate modifiche alla data di inizio sospensione
	query = "SELECT DISTINCT P.ID_PERSONA, P.NOME, P.COGNOME, P.COD_FISC " & _
	        "FROM PERSONA P, PERS_ENTE PE " & _
			"WHERE P.ID_PERSONA = PE.ID_PERSONA AND PE.FL_STATO_PERSONA = 'A' AND PE.CERTIFICATO = 'S' AND " & _
			"P.ID_PERSONA IN ("
	for i = lbound(codici) to ubound(codici) 
		query = query &codici(i)
		if (i < ubound(codici)) then 
			query = query & ", "
		else
			query = query & ")"
		end if
	next
	
	'puliamo la sessione....
	rimuoviAZ()
	rimuoviLettere()
	
	Set res = Server.createObject("ADODB.Recordset")
'PL-SQL * T-SQL  
QUERY = TransformPLSQLToTSQL (QUERY) 
	Set res = connection.execute(query)
	
	if res.EOF then
		Session("messaggioErrore") = "Operazione non disponibile per nessuno degli LSU selezionati"
		Server.transfer("Errore.asp")
	end if
%>
<html>
<style type="text/css">
<!--
			 td {
			 		font-family: tahoma;
					font-size: 10;
					font-weight: bold;
			 }
			 
			 td.cfis {
			 		font-family: tahoma;
					font-size: 10;
			 }
			 
			 select {
			        border: 1 solid black;
					width: 120;
					font-family: tahoma;
					font-size: 10;
					font-weight: bold;
					padding: 2;
					background: white;
					margin: 0;
			 }
			 
			 input {
					width: 120;
					font-family: tahoma;
					font-size: 10;
					font-weight: bold;
					padding: 2;
					background: white;
					margin: 0;
			 }
-->
</style>
	<head>
	</head>
	<body bgcolor="white" leftmargin="0" rightmargin="0" topmargin="0">
	    <script src="utils/utility.js"></script>
		<form method="post" action="SalvataggioSospensione.asp">
		<table border="0" cellpadding="2" style="border-left: 1 solid black; border-right: 1 solid black" width="100%" cellspacing="0" cellpadding="0">
		
		<%
		'il campo dopoOperazione viene messo a 'no' solo dalle funzioni
		'javascript per la selezione dei filtri. Se vale 'si' la sessione
		'viene ripulita al caricamento di questa pagina. Questo evita il
		'problema della propagazione dei dati della form.
		%>
		<input type="hidden" name="dopoOperazione" value="si"/>
		
			<!-- per fare in modo che sia sempre un array -->
				<input type="hidden" name="codice" value="0">
				<input type="hidden" name="lista" value="0">
				<input type="hidden" name="escludi" value="">
		<%
			Do while NOT res.eof
		%>
			<tr>
			    <input type="hidden" name="codice" value="<%=res("id_persona")%>">
				<td width="20" align="left">
				   <input style="width: 14; height: 14; margin: 0" type="checkbox" name="escludi" checked>
				</td>
				<td width="200" align="left"><%=res("cognome")%></td>
				<td width="200" align="left"><%=res("nome")%></td>
				<td width="120" align="left"><%=Ucase(res("cod_fisc"))%></td>
				<input type="hidden" name="lista" value="1">
			</tr>
			<tr>
			   <td colspan="10" height="1" bgcolor="black"></td>
			</tr>
			
		<%
		    res.Movenext
			Loop
		%>
		<input type="hidden" name="inizioSospensione">
		<input type="hidden" name="maternita">
		</table>
		</form>
	</body>
</html>
<%
	res.close
	connection.close
%>
