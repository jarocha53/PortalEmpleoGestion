<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #Include Virtual="/strutt_testa2.asp" -->
<!--#include virtual = "/include/OpenConn.asp"-->
<!--#include virtual="/util/portallib.asp"-->
<%
	if ValidateService(session("idutente"),"UNI_VISUNITORG", CC) <> "true" then 
		response.redirect "/util/error_login.asp"
	end if
%>

<script language="javascript">
	<!--#include virtual = "/Include/help.inc"-->

	function AssegnaVal(val)
	{
		document.Frm2.txtPagNum.value = val;
		document.Frm2.submit()
	}
</script>

<%
'Dichiarazioni
dim Rs
dim sSql
dim totale_record		'totale record della tabella
dim txtPagNum			'numero di pagina corrente
dim i, intero, ind		'ciclo
dim pageSize			'numero record per pagina
dim pageTot				'numero totale di pagine

'Conto i record della tabella
sSql = "select count(*) as totale from UNITA_ORGANIZZATIVA"
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
Set Rs = CC.Execute (sSql)
	totale_record = Rs.Fields ("totale")
Rs.Close

'Paginazione
pageSize = 15
totale_record = CInt(totale_record)
IF (totale_record > pageSize) THEN
	If (totale_record mod pageSize) = 0 then
		pageTot = totale_record \ pageSize
	Else
		pageTot = (totale_record \ pageSize)+1
	End if
ELSE
	pageTot = 1
END IF

'Controllo se ho passato il parametro pagina:
If request("txtPagNum")="" then
	txtPagNum=1
Else
	txtPagNum=CLng(request("txtPagNum"))
End if
%>

<center>
<table border="0" width="520" height="81" cellspacing="0" cellpadding="0">
	<tr>
		<td width="500" height="81" background="<%=Session("Progetto")%>/images/titoli/strumenti2b.gif" valign="bottom" align="right">
			<table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
				<tr>
					<td width="100%" valign="top" align="right">
						<b class="tbltext1a">Unidad Funcional</b>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<br>

<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
	<tr height="18">
		<td class="sfondomenu" width="67%" height="18"><span class="tbltext0"><b>&nbsp;GESTION UNIDADES FUNCIONALES</b></span></td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
		<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
	</tr>
</table>

<!--table border="0" width="500" CELLPADDING="0" cellspacing="0">	<tr height="18" class="tblcomm">		<td align="left" class="sfondomenu">			<span class="tbltext0"><b>&nbsp;GESTIONE UNITA' ORGANIZZATIVA</b></span>		</td>		<td width="25" valign="bottom" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">		<img border="0" src="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>		<td width="140" valign="bottom" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">&nbsp;</td>	</tr></table -->

<table border="0" width="500" CELLPADDING="0" cellspacing="0">
    <tr>
		<td class="sfondocomm" align="left" class="tbltext1">
			En la tabla se puede visualizar las unidades funcionales existentes en el sistema. 
			<br>Para modificar el dato presionar sobre el <b>Identificador</b> de inter�s.
			<br>Para ingresar una nueva Unidad Funcional presionar sobre <b>Ingresar Unidad Funcional</b>.	
		</td>
		<td class="sfondocomm" valign="bottom">
			<a onmouseover="javascript: window.status=' '; return true" href="Javascript:Show_Help('/Pgm/Help/UnitaOrga/UNI_VisUnitOrg')">
				<img src="<%=Session("Progetto")%>/images/Help.gif" border="0" align="right" alt="Help">
			</a>
		</td>
    </tr>
	<tr height="2">
		<td class="sfondocomm" width="100%" colspan="2" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
	</tr>
</table>	

<%
sSql= "SELECT ID_UORG, DESC_UORG" &_
	" FROM UNITA_ORGANIZZATIVA" &_
	" ORDER BY ID_UORG"
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
Set Rs = CC.Execute (sSql)

'ciclo
For ind=1 to (txtPagNum-1) * pageSize
	Rs.MoveNext 
Next

Response.Write "<table width='500' border=0 cellspacing=1 cellpadding=1>"
	Response.Write "<tr>"
		Response.Write "<td align='center'><br>"
			Response.Write "<a href=""UNI_InsUnitOrg.asp"" onmouseover=""javascript: window.status=' '; return true"" class='textred' >"
			Response.Write "<b>"
			Response.write "Ingresar Unidad Funcional</a>"
		Response.Write "</td>"
	Response.Write "</tr>"
	Response.Write "<tr><td>&nbsp;</td></tr>"
Response.Write "</table>"
	
IF NOT rs.eof THEN 

	Response.Write "<table width='500' border=0 cellspacing=1 cellpadding=1 >"
		Response.Write "<tr class='sfondocomm'>"
			Response.write "<td  align='left' width='20%'><b>Identificador </b></td>"
			Response.write "<td align='left'><b>Descripci�n</b </td>"
		Response.Write "</tr>"
		
		i=0
		intero=1
		Do while not rs.eof
			i = i + 1
%>
			<form name="FrmVisUO<%=i%>" action="UNI_ModUnitOrg.asp" method="post">
				<input type="hidden" name="hdnID" value="<%=rs("ID_UORG")%>">
			</form>
	
			<tr class="tblsfondo">
				<td class="tbldett">
					<b><a class="tblAgg" HREF="Javascript:FrmVisUO<%=i%>.submit();" onmouseover="javascript:window.status='' ; return true">
						<%=rs("ID_UORG")%>
					</a></b>
				</td>														
				<td class="tbldett"> <%=rs("DESC_UORG")%></td>					
			</tr>
		<% 
        rs.movenext 
        intero= intero + 1
				if intero > pageSize then exit do
		Loop 
			
	Response.Write "</table>"
%>
	<form name="Frm2" action="UNI_VisUnitOrg.asp" method="post">
		<input type="hidden" id="txtPagNum" name="txtPagNum" value="<%=txtPagNum%>">		
		
		<table border="0" width="500">
			<tr>
<%			if txtPagNum > 1 then
%>			
				<td align="right" width="480">
					<a href="Javascript:AssegnaVal('<%=(txtPagNum - 1)%>')"><img src="<%=Session("Progetto")%>/images/precedente.gif" border="0" id="image1" name="image1">
				</td>
<%			end if
			if txtPagNum < pageTot then
%>			
				<td align="right">
					<a href="Javascript:AssegnaVal('<%=(txtPagNum + 1)%>')"><img src="<%=Session("Progetto")%>/images/successivo.gif" border="0" id="image1" name="image1">
				</td>
<%			end if
%>		
			</tr>
			<!--tr>				<td align="center" colspan="2">					<a href="UNI_DefAmbito.asp" class="textred" onmouseover="javascript: window.status=' '; return true">						<b>Ambiti Unita' Organizzative</b>					</a>				</td>			</tr-->
		</table>
		
	</form>

<%		
ELSE

	Response.Write "<table width='500' border=0 cellspacing=1 cellpadding=0 >"
		Response.Write "<tr>"
			Response.Write "<td  class='tbltext3'  align='center'>"
				Response.write "No existen Unidades Funcionales registradas."
			Response.Write "</td>"
		Response.Write "</tr>"
		Response.Write "<tr><td >&nbsp;</td></tr>"
	Response.Write "</table>"

END IF
	
Rs.Close
set Rs=nothing
%>
</center>
<!--#include Virtual = "/include/CloseConn.asp"-->
<!-- #include Virtual="/strutt_coda2.asp" -->    
