<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include virtual ="/Strutt_testa2.asp"-->
<!--#include virtual = "/Include/OpenConn.asp"-->
<!--#include virtual = "Include/HTMLEncode.asp"-->
<!--#include virtual="/util/portallib.asp"-->
<%
	if ValidateService(session("idutente"),"UNI_VISUNITORG", CC) <> "true" then 
		response.redirect "/util/error_login.asp"
	end if

Dim iValCmb 
Dim Rs
Dim Sql, sSQL
Dim CC
Dim sProgetto
Dim bPresenzaBando, hPresenzaBando
Dim Record, nIndElem
Dim nActPagina
Dim nTotPagina
Dim nTamPagina		
Dim nTotRecord


nTamPagina=5
If Request("pagina")= "" Then
	nActPagina=1
Else
	nActPagina=Clng(Request("pagina"))
End If

iValCmb=Request("CmbRicUorg")
sProgetto = mid(session("Progetto"), 2)
%>

<html>
	<head>
		<script LANGUAGE="javascript">
			<!--#include virtual = "/Include/ControlDate.inc"-->	
			<!--#include Virtual = "/Include/help.inc"-->
			<!--#include virtual = "/Include/ControlNum.inc"-->
			<!--#include Virtual = "/Include/ControlString.inc"-->
			
			//PER LA PAGINAZIONE
			function AssegnaVal(val){
				document.FrmPagina.pagina.value = val;
				document.FrmPagina.submit()
			}
			
//================================******=============================			
			function Aggiungi(){
				if (document.frmInserimento.CmbRicUorg2.value == ""){
				alert('Selezionare una Unit� Organizzativa')
				document.frmInserimento.CmbRicUorg2.focus();
				return false;
				}
				 
				if (document.frmInserimento.cmbAreaterr.value == ""){
				alert('Seleccionar Area Territorial')
				document.frmInserimento.cmbAreaterr.focus()
				return false
				}
						
				if (document.frmInserimento.hPresenzaBando.value== 'True'){
					if (document.frmInserimento.cmbBando.value == "") {
						alert('Selezionare un Bando')
						document.frmInserimento.cmbBando.focus()
						return false					
					}
					return true	
				}
			}
			
//================================******=============================			
			function ControllaDati(){
				var ok
				var nrec
				var chkbox
				
				ok = 0 
				nrec= (document.frmElimina.txtNumElementi.value)
				for (i=1; i<=nrec; i++){
					chkbox = eval("document.frmElimina.ckFlag" + i)
					if (chkbox.checked){
						ok = 1
						break
					}
				}
				if (ok == 0){
					alert ('Selezionare almeno una voce dalla tabella!')
					return false
				}
				document.frmElimina.submit();
			}
		</script>
	</head>

	<body>
	<table border="0" width="520" height="81" cellspacing="0" cellpadding="0">
		<tr>
			<td width="500" height="81" background="<%=Session("Progetto")%>/images/titoli/strumenti2b.gif" valign="bottom" align="right">
				<table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
					<tr>
						<td width="100%" valign="top" align="right">
							<b class="tbltext1a">Unidad Funcional</b>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<br>
	<table cellpadding="0" cellspacing="0" width="500" border="0">
		<tr height="18">
			<td class="sfondomenu" height="18" width="60%">
				<span class="tbltext0">
					<b>DEFINICION DEL AMBITO DE LA UNIDAD ORGANIZATIVA</b>
				</span>
			</td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">
			</td>
			<td valign="middle" align="right" width="35%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1">
			</td>
		</tr>
		<tr width="371" class="SFONDOCOMM">
			<td colspan="3">
				Para visualizar la asociaci�n de una Unidad Funcional,  
				Area Territorial y Proyecto (BA, FIAT) seleccionar la Unidad Organizativa  
				y presionar <b>Busqueda</b>.<br>
				Dejando vacio el campo puede efectuar una busqueda sin filtros.<br>
				Tambi�n se puede efectuar el <b>Ingreso</b> y la <b>Eliminaci�n</b>.
				<a href="Javascript:Show_Help('/Pgm/help/UnitaOrga/UNI_DefAmbito')" name="prov3" onmouseover="javascript:window.status='' ; return true">
					<img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0">
				</a>
			</td>
		</tr>
		<tr height="2">
			<td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>

<!--FORM DI RICERCA-->
	<form name="frmRicerca" action="UNI_DefAmbito.asp" method="post">
	<table border="0" CELLPADDING="1" CELLSPACING="2" width="500">
		<tr>
			<td class="tbltext1" width="150" align="left">
				<b>Unidad Funcional</b>
			</td>
			<td align="left">
				<%
				Sql=	"SELECT ID_UORG, DESC_UORG FROM UNITA_ORGANIZZATIVA " &_
						"ORDER BY DESC_UORG"
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
				Set Rs = CC.Execute (Sql)
					Response.Write "<SELECT class='textblack' name='CmbRicUorg'>"
					Response.Write "<option value=''></option>"
					If Not Rs.eof then
						DO
							Response.Write "<option value=" & Rs("ID_UORG") & ">" & Rs("DESC_UORG") & "</option>"
							Rs.MOVENEXT
						LOOP UNTIL Rs.EOF
					End If
				Rs.close
				Set Rs = Nothing
				Response.Write "</SELECT>"
				%>
			</td>
			<td align="left">
				<input type="image" name="Cerca" value="Ricerca" src="<%=Session("Progetto")%>/images/lente.gif" border="0">		
			</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
		</tr>
		<tr>
		    <td height="2" align="left" colspan="4" background="/images/separazione.gif">
		    </td>
	    </tr>
	</table>				
	</form>
	
<!--FORM DI INSERIMENTO-->
	<form name="frmInserimento" action="UNI_CnfDefAmbito.asp" method="post" onsubmit="return Aggiungi(this);">
	<table width="500" border="0" cellspacing="1" cellpadding="1">
		<tr> 
			<td class="tbltext1" align="left">
				<b>Unidad Funcional</b>
			</td>
			<td align="left" COLSPAN="2">
				<%
				Sql= "SELECT ID_UORG, DESC_UORG FROM UNITA_ORGANIZZATIVA "
				IF iValCmb <> "" then
					Sql= Sql & " WHERE ID_UORG=" & iValCmb
				ELSE			
					Sql = Sql & " ORDER BY DESC_UORG"
				END IF
				
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
				Set Rs = CC.Execute (Sql)
				Response.Write "<SELECT class='textblack' name='CmbRicUorg2'>"
					IF iValCmb <> "" then
						Response.Write "<option value=" & iValCmb & ">" & Rs("DESC_UORG") & "</option>"				
					ELSE
						Response.Write "<option value=''></option>"
						If Not Rs.eof then
							DO
								Response.Write "<option value=" & Rs("ID_UORG") & ">" & Rs("DESC_UORG") & "</option>"
								Rs.MOVENEXT
							LOOP UNTIL Rs.EOF
						End If
					END IF
				Response.Write "</SELECT>"
				Rs.close
				Set Rs = Nothing
				%>
			</td>
		</tr>			
		<tr>
			<td class="tbltext1" align="left">
				<b>Area Territorial</b>
			</td>
			<td align="left">
				<%
				Sql=	"SELECT ID_AREATERR, DESC_AREATERR FROM AREA_TERR " &_
						"ORDER BY DESC_AREATERR"
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
				Set Rs = CC.Execute (Sql)
					Response.Write "<SELECT class='textblack' name='cmbAreaterr'>"
					Response.Write "<option value=''></option>"
					If Not Rs.eof then
						DO
							Response.Write "<option value=" & Rs("ID_AREATERR") & ">" & strHTMLEncode (Rs("DESC_AREATERR")) & "</option>"
							Rs.MOVENEXT
						LOOP UNTIL Rs.EOF
					End If
				Rs.close
				Set Rs = Nothing
				Response.Write "</SELECT>"
				%>	
			</td>
		</tr>
		<tr>
			<td class="tbltext1" align="left">
				<b>Bando</b>
			</td>
			<td align="left">
				<%
				Sql =	"SELECT ID_BANDO, DESC_BANDO FROM BANDO B, PROGETTO P WHERE AREA_WEB = '" & sProgetto & "'" &_
						" AND B.ID_PROJ = P.ID_PROJ" &_
						" ORDER BY DESC_BANDO"
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
				Set Rs = CC.Execute (Sql)
				
				If Not Rs.eof then
					bPresenzaBando = true
					Response.Write "<SELECT class='textblack' name='cmbBando'>"
					Response.Write "<option value=''></option>"
					DO
						Response.Write "<option value=" & Rs("ID_BANDO") & ">" & strHTMLEncode(Rs("DESC_BANDO")) & "</option>"
						Rs.MOVENEXT
					LOOP UNTIL Rs.EOF
					Response.Write "</SELECT>"
					Rs.close
					Set Rs = Nothing
				Else	
					bPresenzaBando = false
					Response.Write "<SELECT class='textblack' name='cmbBando' disabled>"
					Response.Write "<option value=''></option>"
					Response.Write "</SELECT>"	
				End if								
				%>
				<input type="hidden" name="hPresenzaBando" value="<%=bPresenzaBando%>">
			</td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td align="center" colspan="2">
				<input type="image" src="<% session("Progetto") %>/images/aggiungi.gif" border="0" value="Aggiungi" align="top" id="Aggiungi" name="Aggiungi">
			</td>
		</tr>
	</table>
	</form>
   
<% 	on error resume next 
   	set rsUnit= server.CreateObject("ADODB.Recordset")

   	If iValCmb= "" Then
		sSQL =	"SELECT AB.ID_AREATERR, AB.ID_BANDO, AB. ID_UORG, desc_areaterr, desc_uorg, desc_bando" &_
				" FROM area_terr ar, area_bando ab, bando b, unita_organizzativa uo" &_
				" WHERE ab.id_areaterr = ar.id_areaterr AND ab.id_uorg= uo.id_uorg AND" &_
				" Ab.id_bando = b.id_bando (+)" &_
				" ORDER BY desc_uorg, desc_areaterr, desc_bando"
    Else
		sSQL =	"SELECT AB.ID_AREATERR, AB.ID_BANDO, desc_areaterr, desc_bando" &_
				" FROM area_terr ar, area_bando ab, bando b" &_
				" WHERE ab.id_areaterr = ar.id_areaterr and" &_
				" ab.id_uorg =" & iValCmb & " and ab.id_bando=b.id_bando (+)" &_
				" ORDER BY desc_areaterr, desc_bando"
    End If				
	
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	rsUnit.Open sSQL,CC,3
	If rsUnit.eof Then
		Response.Write "<table align=center>"
		Response.Write"<tr>"
		Response.Write"<td class=tbltext3><br>"
		Response.Write"<strong>Non sono presenti occorrenze nella tabella.</strong>"
		Response.Write"</td>"
		Response.Write"</tr>"
		Response.Write"</table>"	
	Else
		rsUnit.PageSize = nTamPagina
		rsUnit.CacheSize = nTamPagina
		nTotPagina = rsUnit.PageCount 
		If nActPagina < 1 Then
			nActPagina = 1
		End If
		If nActPagina > nTotPagina Then
			nActPagina = nTotPagina
		End If
		rsUnit.AbsolutePage= nActPagina
		nTotRecord=0
		nIndElem=0	'conta il numero totale di record ricavati
%>
<!--FORM DI ELIMINAZIONE-->			
		<form name="frmElimina" method="post" action="UNI_CanDefAmbito.asp">
		<input type="hidden" value="<%=iValCmb%>" name="hValCmb">
		<table width="500" cellspacing="1" cellpadding="1" align="center" border="0">
			<tr class="sfondocomm">
				<td width="40" align="center">
					<b>Elimina</b>
				</td>
				<%
				If iValCmb= "" Then%>
				<td align="center" width="150">
					<b>Unidad Funcional</b>
				</td>
				<%
				End if%>
				<td align="center">
					<b>Area Territorial</b>
				</td>
				<td width="100" align="center">
					<b>Bando</b>
				</td>
			</tr>
		</table>			
		<table WIDTH="500" cellspacing="1" cellpadding="1" align="center" BORDER="0">
		<%
		While rsUnit.EOF <> True And nTotRecord < nTamPagina
			nIndElem = nIndElem + 1 %>
			<tr class="tblsfondo">
				<td width="40" align="center">
					<input type="checkbox" name="ckFlag<%=nIndElem%>">
				</td>
				<%
				If iValCmb= "" Then%>
				<td class="tblDett" width="150">
					<%=strHTMLEncode (rsUnit("DESC_UORG"))%>
					<input type="hidden" value="<%=rsUnit("ID_UORG")%>" name="txtUorg<%=nIndElem%>">
				</td>
				<%
				End if%>
				<td class="tblDett">
					<%=strHTMLEncode (rsUnit("DESC_AREATERR"))%>
					<input type="hidden" value="<%=rsUnit("ID_AREATERR")%>" name="txtAterr<%=nIndElem%>">
				</td>
				<td class="tblDett" width="100">
					<%=rsUnit("DESC_BANDO")%>
					<input type="hidden" value="<%=rsUnit("ID_BANDO")%>" name="txtBando<%=nIndElem%>">
				</td>
			</tr>
		<%
		nTotRecord=nTotRecord + 1
		rsUnit.MoveNext
		Wend%>
		<input type="hidden" value="<%=nIndElem%>" name="txtNumElementi">
		</table>
		</form>

<!--PAGINAZIONE-->
		<form name="FrmPagina" method="post" action="UNI_DefAmbito.asp">
		<input type="hidden" id="pagina" name="pagina" value="<%=nActPagina%>">
		<%
		if iValCmb <> "" then%>		
		<input type="hidden" name="CmbRicUorg" value="<%=iValCmb%>">	
		<%
		end if%>			
		<table border="0" width="500">
			<tr>
			<%
			if nActPagina > 1 then%>		
				<td align="right" width="480">
					<a href="Javascript:AssegnaVal('<%=(nActPagina - 1)%>')">
						<img src="<%=Session("Progetto")%>/images/precedente.gif" border="0" id="image1" name="image1">
					</a>
				</td>
			<%
			end if
			if nActPagina < nTotPagina then%>
				<td align="right">
					<a href="Javascript:AssegnaVal('<%=(nActPagina + 1)%>')">
						<img src="<%=Session("Progetto")%>/images/successivo.gif" border="0" id="image1" name="image1">
					</a>
				</td>
			<%
			end if%>		
			</tr>
		</table>
		</form>
				
		<table border="0" cellspacing="0" cellpadding="0" width="500">
			<tr>
				<td align="middle">
					<input type="image" src="<%=session("Progetto")%>/images/elimina.gif" border="0" value="Elimina" align="top" id="image1" name="image1" onclick="Javascript:ControllaDati();">
				</td>
			</tr>
		</table>

<%		rsUnit.Close
		Set rsUnit = Nothing					
	End If
%>
	</body>
</html>

<!--#include virtual ="/include/closeconn.asp"-->
<!--#include virtual ="/Strutt_coda2.asp"-->
