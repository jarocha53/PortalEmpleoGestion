<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%
'  ACCIONES DE BUSQUEDA DE TRABAJO
'	Funciones: Visualizacion, Modificacion, Eliminaci�n
'	Tablas: PERS_ACCIO  Campos: id_persona, cod_accion, id_figprof, fecha, establecimiento, 
'								direccion, telefono, contacto, act_llamado act_fila, 
'								act_formulario, act_prueba, act_entrevista, tiene_comp, 
'								muestra_comp, resultado, observacion, id_accion
'
'	Parametros de Entrada:	id_Accion, id_Persona, Cod_Accion
'
'	Salida RETROCESO
'
'	Salida MODIFICAR ACT=MOD
'
'	Salida AGREGA    ACT=INS
'
'	Salida ELIMINA   ACT=DEL
'
'	Para todos los casos utiliza el mismo formulario
'		Formulario: frmModAccion   
'			Parametros: ACT, nIdAcc, nIdPers, nCodAcc, txtfecha, txtestablecimiento, txtdireccion, 
'						txttelefono, txtcontacto, txtSpecifico, txtTipoSpecifico, chkAct_llamado, 
'						chkAct_fila, chkAct_formulario, chkAct_prueba, chkAct_entrevista, 
'						OpTiene_comp, OpMuestra_comp, cmbResultado, txtobservaciones
'
'
'
%>

<!--#include Virtual = "/strutt_testa2.asp"-->
<!-- #include Virtual = "/Include/DecCod.asp" -->
<!-- #include Virtual = "/Include/OpenConn.asp" -->
<!-- #Include Virtual="/Include/ControlDateVB.asp" -->

<script language="Javascript">
<!--#include virtual = "/Include/ControlDate.inc"-->
<!--#include virtual = "/include/ControlString.inc"-->
<!--#include virtual = "/Include/ControlNum.inc"-->
<!--#include virtual = "/Include/help.inc"-->

function ApriFigProf()
	{
		document.frmModAccion.CriterioRicerca.value=TRIM(document.frmModAccion.CriterioRicerca.value);
 		/*if (document.frmModAccion.CriterioRicerca.value== "")
 		{
 			alert("Ingresar una funcion o puesto.");
 			document.frmModAccion.CriterioRicerca.focus();
 			return;
 		}*/
		CriterioRicerca = frmModAccion.CriterioRicerca.value;
		URL = "CodiceIstat.asp?VISMENU=NO" + "&RICERCA="+ CriterioRicerca;
		opt = "address=no,status=no,width=800,height=320,top=150,left=15";
		window.open (URL,"",opt);
	}
function indietro()
{
	document.frmModAccion.action='ACC_VisAcciones.asp';
	document.frmModAccion.onsubmit =''
	document.frmModAccion.submit()
}
function elimina()
{
	//alert("PASA POR ACA")
	document.frmModAccion.ACT.value = "DEL"
	document.frmModAccion.action='ACC_CnfAcciones.asp';
	document.frmModAccion.onsubmit =''
	document.frmModAccion.submit()
}

function valida(Obj)
{	
 	//alert("Paso por aca")
	// Validacion de la Accion
	Obj.cmbId_accion.value=TRIM(Obj.cmbId_accion.value)
	if (Obj.cmbId_accion.value == "")
	{
		alert("El campo Accion es obligatorio");
		Obj.cmbId_accion.focus();
		return false
	}
	
	// Validacion de la fecha
	Obj.txtfecha.value=TRIM(Obj.txtfecha.value)
	if (Obj.txtfecha.value == "")
	{
		alert("El campo Fecha es obligatorio");
		Obj.txtfecha.focus();
		return false
	}
	else
	{
		if (ValidateInputDate(Obj.txtfecha.value)== false)
		{
			Obj.txtfecha.focus();
			return false
		}
		else
		{
			if (ValidateRangeDate(Obj.txtfecha.value,Obj.txtoggi.value)==false)
			{
				alert("La fecha debe ser anterior a la fecha actual")
				Obj.txtfecha.focus();
				return false
			}	
		}
	}
	
	// Validacion del Establecimiento
	Obj.txtestablecimiento.value=TRIM(Obj.txtestablecimiento.value)
	/* Damian quita obligatoriedad del campo Establecimiento */
	if (Obj.txtestablecimiento.value == "")
	{
		alert("El campo Lugar/Nombre es obligatorio porque a trav�s de �l se acceder� a la informaci�n");
		Obj.txtestablecimiento.focus();
		return false
	}/* Damian no quita obligatoriedad del campo Establecimiento */
	// Validacion del Puesto
	Obj.txtTipoSpecifico.value=TRIM(Obj.txtTipoSpecifico.value)
	/* Damian quita obligatoriedad del campo Puesto
	if (Obj.txtTipoSpecifico.value == "")
	{
		alert("El campo Puesto es obligatorio");
		Obj.CriterioRicerca.focus();
		return false
	}*/
	
	// Validacion de la Actividad

	if (!Obj.chkAct_llamado.checked == false) 
	{
		Obj.chkAct_llamado.value = "01"
	}
	if (!Obj.chkAct_fila.checked == false)
	{
		Obj.chkAct_fila.value = "01"
	}
	if (!Obj.chkAct_formulario.checked == false)
	{
		Obj.chkAct_formulario.value = "01"
	}
	if (!Obj.chkAct_prueba.checked == false)
	{
		Obj.chkAct_prueba.value = "01"
	}
	if (!Obj.chkAct_entrevista.checked == false)
	{
		Obj.chkAct_entrevista.value = "01"
	}

	if ((Obj.chkAct_llamado.checked == false) && (Obj.chkAct_fila.checked == false) && (Obj.chkAct_formulario.checked == false)&& (Obj.chkAct_prueba.checked == false) && (Obj.chkAct_entrevista.checked == false))
	{
		alert("Debe seleccionar al menos una Actividad");
		Obj.chkAct_llamado.focus();
		return false
	}

	// Validacion del Resultado
	Obj.cmbResultado.value=TRIM(Obj.cmbResultado.value)
	if (Obj.cmbResultado.value == "")
	{
		alert("El campo Resultado es obligatorio");
		Obj.cmbResultado.focus();
		return false
	}
}


</script>
<script language="javascript" src="Controlli.js">
</script>

<%	
Dim nIdAcc, nIdPers, nCodAcc
Dim sSql , RS
	
nIdAcc	= Request.Form("id_Accion")
nIdPers	= Request.Form("id_Persona")
nCodAcc	= Request.Form("Cod_Accion")
sACT = Request("ACT")

if nIdPers <> "" then
	nIdP = clng(nIdPers)
		
	if session("Progetto") <> "/PLAVORO" then
		sIdpers = nIdPers
		%>
		<br><!--#include virtual ="/pgm/iscr_utente/menu.asp"-->
		<%
	end if
else
	nIdPers= session("creator")
end if
Err.Clear

if sACT = "MOD" then

	sSQL = "SELECT id_persona, cod_accion, id_figprof, fecha, " &_ 
			"establecimiento, direccion, telefono, contacto, act_llamado, " &_ 
			"act_fila, act_formulario, act_prueba, act_entrevista, tiene_comp, " &_ 
			"muestra_comp, resultado, observacion, id_accion FROM pers_accion " &_ 
			"WHERE id_accion = " & nIdAcc

	set RS = server.CreateObject("ADODB.recordset")
'PL-SQL * T-SQL  
'SSQL = TransformPLSQLToTSQL (SSQL) 
	RS.Open sSQL,CC

	If not rs.eof then
		nId_accion		= RS("id_accion")
		nId_persona		= RS("id_persona")
		sCod_accion		= RS("cod_accion")
		nId_figprof		= RS("id_figprof")
		dFecha			= RS("fecha")
		sEstablec		= RS("establecimiento")
		sDireccion		= RS("direccion")
		sTelefono		= RS("telefono")
		sContacto		= RS("contacto")
		sAct_llamado	= RS("act_llamado")
		sAct_fila		= RS("act_fila")
		sAct_formulario	= RS("act_formulario")
		sAct_prueba		= RS("act_prueba")
		sAct_entrevista	= RS("act_entrevista")
		sTiene_comp	    = RS("tiene_comp")
		sMuestra_comp	= RS("muestra_comp")
		sResultado		= RS("resultado")
		sObservacion	= RS("observacion")
	End If 
	rs.close
	set rs=nothing

	set RS2 = server.CreateObject("ADODB.recordset")
	sSQL2 = "SELECT  ci.codice,  ci.mansione, fp.id_figprof, fp.denominazione FROM codici_istat ci INNER JOIN figureprofessionali fp ON ci.codice = fp.cod_ej WHERE fp.id_figprof= '" & nId_figprof & "'"
'PL-SQL * T-SQL  
'SSQL2 = TransformPLSQLToTSQL (SSQL2) 
	RS2.Open sSQL2,CC


	If not rs2.eof then
		sNom_figprof = RS2("denominazione") & " (" & RS2("mansione") & "(" & RS2("codice") & "))"
	End If 
	rs2.close
	set rs2=nothing

end if

'Response.Write "ACT: " & sACT 


%>
<form name="frmModAccion" method="POST" action="ACC_CnfAcciones.asp" OnSubmit="return valida(this);">
	<input type="hidden" name="ACT" value="<%=sACT%>">
	<input type="hidden" name="id_Accion" value="<%=nIdAcc%>">
	<input type="hidden" name="idPers" value="<%=nIdPers%>">
	<input type="hidden" name="Cod_Accion" value="<%=nCodAcc%>">
    <input type="hidden" name="txtoggi" value="<%=ConvDateToString(Date())%>">

<table border="0" width="500" cellspacing="0" cellpadding="0" align="center">
	<tr>
		<td class="sfondomenu" width="67%" height="18"><span class="tbltext0"><b>&nbsp;ACCIONES DE BUSQUEDA DE TRABAJO</b></span></td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
		<td width="30%" valign="middle" align="right" class="tbltext1" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">(*) campo obligatorio</td>
	</tr>
	<tr>
	    <td colspan="3" align="left" class="sfondocomm"> 
			Para ingresar o modificar los datos completar los campos relacionados. 
			Presionar enviar para guardar la modificaci�n. <a href="Javascript:Show_Help('/Pgm/help/Richieste/RIC_VisContatti')"><img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a> 
		</td>
	</tr>
	<tr height="2">
		<td class="sfondocommaz" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
	</tr>
</table>
<br>
<table border="0" width="500" cellspacing="6" cellpadding="2" align="center" style="text-align:left;">
	<tr>
		<td align="left" class="tbltext1" width="140">
 			<b>Accion:*</b>
 		</td>
		<td>
			<%
			UtCod_stat_Freq= "ACCIO|0|" & date & "|"& sCod_accion & "|cmbId_accion' style=width:390|ORDER BY DESCRIZIONE"			
			CreateCombo(UtCod_stat_Freq)
			%>
		</td>
	</tr>
	<tr>
		<td align="left" class="tbltext1" width="140">
 			<b>Fecha:*</b>
 		</td>
		<td valign="center">
			<input type="text" class="textblacka" maxlength="10" size="11" name="txtfecha" value="<%=dFecha%>">
			<a class="tbltext1">&nbsp;(dd/mm/aaaa)</a>
		</td>
	</tr>
	<tr>
		<td align="left" class="tbltext1" width="140">
 			<b>Lugar/Nombre:</b>
 		</td>
		<td>
			<input type="text" class="textblacka" maxlength="99" size="55" name="txtestablecimiento" value="<%=sEstablec%>">
		</td>
	</tr>
	<tr>
		<td align="left" class="tbltext1" width="140">
 			<b>Direcci�n:</b>
 		</td>
		<td>
			<input type="text" class="textblacka" maxlength="99" size="55" name="txtdireccion" value="<%=sDireccion%>">
		</td>
	</tr>
	<tr>
		<td align="left" class="tbltext1" width="140">
 			<b>Tel�fono:</b>
 		</td>
		<td>
			<input type="text" class="textblacka" maxlength="19" size="20" name="txttelefono" value="<%=sTelefono%>">
		</td>
	</tr>
	<tr>
		<td align="left" class="tbltext1" width="140">
 			<b>Nombre del Contacto:</b>
 		</td>
		<td>
			<input type="text" class="textblacka" maxlength="99" size="30" name="txtcontacto" value="<%=sContacto%>">
		</td>
	</tr>
  	<tr>
        <td height="2" align="left" colspan="2" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
    </tr>
	<tr>
		<td colspan="2" align="center">
			<br>
			<table cellpadding=2 cellspacing=0 width="90%" border=1 bordercolor="#006699"><tr><td colspan="2">		
				<tr bordercolor="white">
					<td colspan="2" align="justify" class="textblack">
						<small>Ingresar a continuaci�n la informaci�n relacionada con la <b>Funci�n Desempe�ada</b> 
							<!--Tambi�n puede ingresar el <b>C�digo de Ocupaci�n</b> para efectuar la b&uacute;squeda, o bien utilizar simultaneamente ambos criterios de b&uacute;squeda.		-->
							Para acceder al diccionario interno c&oacute;digos ocupacionales puede presionar el bot&oacute;n <b>B&uacute;squeda</b>.
						</small>
					</td>
				</tr>		
				<tr valign="bottom" align=left bordercolor="White">
					<td class="tbltext1" width="160"><b>Funci�n Desempe�ada: </b></td>
					<td>
						<input style="TEXT-TRANSFORM: uppercase; width:250px;" type="text" class="textblacka"  maxlength="50" name="CriterioRicerca">&nbsp;&nbsp;
						<a href="javascript:ApriFigProf()"><img src="<%=Session("Progetto")%>/images/leggi.gif" title="Buscar Ocupaci�n" border=0 align=bottom onmouseover="javascript: window.status=' '"></a>
					</td>
				</tr>
			</table>
			<br>
		</td>
	</tr>
	<tr>
		<td align="left" class="tbltext1" width="140">
			<b>Puesto:</b>
		</td>
		<td class="tbltext1"><b>
			<textarea cols=62 rows=4 style="text-transform: uppercase;" class="textblacka" readonly name="txtSpecifico"><%=trim(sNom_figprof)%>
			</textarea>
			<!--input type="text" class="textblacka" name="txtSpecifico" readonly value="<%=sNom_figprof%>" type="text" maxlength="99" size="55" -->
			<input type="hidden" name="txtTipoSpecifico" value="<%=nId_figprof%>">
			<input type="hidden" name="txtCodFigProf" value>
			<input type="hidden" name="txtFigProf" value>
			</b>
		</td>
	</tr>	
  	<tr>
        <td height="2" align="left" colspan="2" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
    </tr>
	<tr>
		<td align="left" class="tbltext1" width="140">
			<b>Actividad Realizada:* </b>
		</td>
		<td class="tbltext1"><b>
			<input type="checkbox" id="chkAct_llamado" value="<%=sAct_llamado%>" <% if (sAct_llamado)="01" then Response.Write  " checked" %> name="chkAct_llamado">&nbsp;<b>Llamado Telef�nico</b><br>
			<input type="checkbox" id="chkAct_fila" value="<%=sAct_fila%>" <% if (sAct_fila)="01" then Response.Write  " checked" %> name="chkAct_fila">&nbsp;<b>Fila de espera</b><br>
			<input type="checkbox" id="chkAct_formulario" value="<%=sAct_formulario%>" <% if (sAct_formulario)="01" then Response.Write  " checked" %> name="chkAct_formulario">&nbsp;<b>Llenado de Formulario</b><br>
			<input type="checkbox" id="chkAct_prueba" value="<%=sAct_prueba%>" <% if (sAct_prueba)="01" then Response.Write  " checked" %> name="chkAct_prueba">&nbsp;<b>Toma de Test / Prueba</b><br>
			<input type="checkbox" id="chkAct_entrevista" value="<%=sAct_entrevista%>" <% if (sAct_entrevista)="01" then Response.Write  " checked" %> name="chkAct_entrevista">&nbsp;<b>Entrevista</b><br>
		</td>
	</tr>	
	<tr>
		<td align="left" class="tbltext1" width="140">
			<strong>�Le dieron comprobante?</strong> 
		</td>
		<td>
			<input type="radio" id="OpTiene_comp" name="OpTiene_comp" value="S" checked><span CLASS="TBLTEXT">SI</span>&nbsp;
			<input type="radio" id="OpTiene_comp" name="OpTiene_comp" value="N"<% if ((sTiene_comp)="N" OR (sTiene_comp)="") then Response.Write  " checked"%>><span CLASS="TBLTEXT">NO</span>
		</td>
	</tr>
	<tr>
		<td align="left" class="tbltext1" width="140">
			<strong>�Muestra Comprobante?</strong> 
		</td>
		<td>
			<input type="radio" id="OpMuestra_comp" name="OpMuestra_comp" value="S" checked><span CLASS="TBLTEXT">SI</span>&nbsp;
			<input type="radio" id="OpMuestra_comp" name="OpMuestra_comp" value="N"<% if ((sMuestra_comp)="N" OR (sMuestra_comp)="") then Response.Write  " checked"%>><span CLASS="TBLTEXT">NO</span>
		</td>
	</tr>
  	<tr>
        <td height="2" align="left" colspan="2" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
    </tr>

	<tr>
		<td align="left" class="tbltext1" width="140">
 			<b>Resultado de la Accion:*</b>
 		</td>
		<td>
			<%
			UtCod_stat_Freq= "RESUL|0|" & date & "|"& sResultado & "|cmbResultado|ORDER BY DESCRIZIONE"			
			CreateCombo(UtCod_stat_Freq)
			%>
		</td>
	</tr>
	<tr>
		<td align="left" class="tbltext1" width="140">
 			<b>Observaciones:</b>
 		</td>
		<td>
			<input type="text" class="textblacka" maxlength="199" size="60" name="txtobservaciones" value="<%=sObservacion%>">
		</td>
	</tr>
  	<tr height="2">
        <td colspan="2" height="2" align="left" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
    </tr>
	<tr>
		<td colspan="2" class="tbltext1" align="center"><br>
			<input type="image" title="" src="<%=Session("Progetto")%>/images/indietro.gif" id="image1" name="image1" onclick="indietro()">
			<input type="image" title="" src="<%=Session("Progetto")%>/images/conferma.gif" id="image2" name="image2">
			<%
			if sACT <> "INS" then
				%>
				<input type="image" title="" src="<%=Session("Progetto")%>/images/elimina.gif" id="image3" name="image3" onclick="elimina()">
				<%
			end if
			%>
		</td>                                                                                                      
	</tr>
</table>
</form> 


<%




'Response.End 



%>



<!--#include Virtual = "/include/closeconn.asp"-->
<!--#include Virtual="/strutt_coda2.asp" -->

