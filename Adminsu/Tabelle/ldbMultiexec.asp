<HTML>
<HEAD>
<LINK REL=STYLESHEET TYPE="text/css" HREF='fogliostile.css'>
<BODY>
<br>
<TABLE width=580 border=0 cellspacing=2 cellpadding=1 class="sfondocomm">
	<tr> 
		<td colspan=2 align=middle class="tbltext2">
			<b>Risultato dell'esecuzione comandi sulla tabella <%=UCase(sNomeTab)%> appartenente a <%=mid(session("Progetto"),2)%></b>
		</td>
	</tr>
</TABLE>
<br>
<br>		
<%
	dim sql, contatore, nRec

	nRec = 0
	contatore = 0
	sMulti = split(Request.Form("sMultiQuery"),"<-->")

	If IsArray(sMulti) then
		Dim strConn, sOper
%>	
	<!--#include virtual ="/include/openconn.asp"-->
<%	
		on error resume next
		CC.Begintrans
		for i = 0 to ubound(sMulti)
			CC.Execute (sMulti(i)), nRec
			if Err.number <> "0" then
				exit for
			end if
			contatore = contatore + nRec
		next
		if Err.number <> "0" then
			CC.RollBackTrans
			contatore = 0
		else
			CC.CommitTrans
		end if
		Err.Clear 
%>
		<TABLE BORDER="0" CELLSPACING="2" CELLPADDING="1" width="500" class="sfondocomm">
			<tr class="tblsfondo"> 
				<td><b class="tbltext" >COMANDO ESEGUITO !</td>
			</tr>
			<tr class="tblsfondo">
				<td>
				<b class="tbltext" >Numero Righe Aggiornate = <%=contatore%></b>
				</td>
			</tr>
		</TABLE>
		<!--#include virtual ="/include/closeconn.asp"-->
		<!--#include file = "ldbLog.asp"-->
<%
		LDBLog Request.ServerVariables("REMOTE_ADDR"), sql, contatore
	End If	
%>
	
	