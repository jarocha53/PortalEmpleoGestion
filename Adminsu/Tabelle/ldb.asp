<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<HTML>
<HEAD>
<LINK REL=STYLESHEET TYPE="text/css" HREF='fogliostile.css'>
<%
	Response.Buffer =true
	on error resume next
	sChkSession="" & Session("ok")

	on error goto 0

	if sChkSession<>"" then
		set	Session("ok")=nothing
	else
		Response.Redirect  "login.htm"
	end if	

	Response.ExpiresAbsolute = Now() - 1 
	Response.AddHeader "pragma","no-cache"
	Response.AddHeader "cache-control","private"
%>
<script language="Javascript" src="/include/ControlString.inc"></script>
<script language="Javascript">
	function Validator1()
	{
		if (TRIM(document.form1.sTabelle.value) == "") 
		{		alert("Inserire il nome della Tabella!");
				document.form1.sTabelle.focus();
				return (false);
		}
		return (true);
	}
	
	function Validator2()
	{
		if (TRIM(document.form2.sQuery.value) == "") 
		{		alert("Inserire la query!");
				document.form2.sQuery.focus();
				return (false);
		}
		sSQLV2 = TRIM(document.form2.sQuery.value);
		sSQLV2 = sSQLV2.toUpperCase();
		sSQLWord2 = sSQLV2.split(" ")[0];
		if ((sSQLWord2 == "DELETE") || (sSQLWord2 == "UPDATE") || (sSQLWord2 == "INSERT")) {
				alert("Questa e' utilizz solo per le select!");
				document.form2.sQuery.focus();
				return (false);
		}
		return (true);
	}
	
	function Validator3()
	{
		if (TRIM(document.form3.sNoQuery.value) == "") 
		{		alert("Inserire il comando!");
				document.form3.sNoQuery.focus();
				return (false);
		}
		sSQLV3 = TRIM(document.form3.sNoQuery.value);
		sSQLV3 = sSQLV3.toUpperCase();
		sSQLWord3 = sSQLV3.split(" ")[0];
		if (sSQLWord3 == "SELECT")  {
				alert("Questa non e' utilizzabile per le select!");
				document.form2.sQuery.focus();
				return (false);
		}
		return (true);
	}
	function Validator4()
	{
		if (TRIM(document.form4.sMultiQuery.value) == "") 
		{		alert("Inserire il comando!");
				document.form4.sMultiQuery.focus();
				return (false);
		}
		sSQLV4 = TRIM(document.form4.sMultiQuery.value);
		sSQLV4 = sSQLV4.toUpperCase();
		sSQLWord4 = sSQLV4.split(" ")[0];
		if (sSQLWord4 == "SELECT")  {
				alert("Questa non e' utilizzabile per le select!");
				document.form2.sQuery.focus();
				return (false);
		}
		return (true);
	}
</script>
</HEAD>
  <BODY>
	<br>
	<TABLE width="580" border="0" cellspacing="2" cellpadding="1" class="sfondocomm">
		<tr > 
			<td colspan="2" align=middle class="tbltext2">
				<b>Lista contenuto tabelle del progetto <%=mid(session("Progetto"),2)%></b>
			</td>
		</tr>
		<tr> 
			<td colspan="2" align="middle">&nbsp;</td>
		</tr>
		<tr>
			<td	 colspan="2" align="left">
			<b class="tbltext1">
			<FORM name="form1" method="post" onSubmit="return Validator1()" action="ldbquery.asp">
					<input id=desc type="checkbox" name="desc">DESC
					<input name="sTabelle" size="30" style="WIDTH: 218px; HEIGHT: 22px">
					<input type="submit" value="Leggi Tabella" size="5" name="pTabelle"></b> 
			</FORM></B>
			<td></td>
		<tr>
			<td	 colspan="2" align="left">
			<FORM method="post" name="form2" onSubmit="return Validator2()" action="ldbquery.asp">
				<textarea style="WIDTH: 538px; HEIGHT: 96px" name="sQuery" cols="40"></TEXTAREA>
				<input type="submit" value="Esegui Query" size="5" name="pQuery"> 
			</FORM></B>
			</td>
		</tr>
		<tr>			
			<td>
				<FORM method="post" name="form3" onSubmit="return Validator3()" action="ldbexec.asp">
					<textarea style="WIDTH: 540px; HEIGHT: 95px" name="sNoQuery" cols="40"></TEXTAREA>
					<input type="submit" value="Execute (no Query)" size="5" name="pNoQuery">
				</FORM>
			</td>
		</tr>
		<tr>			
			<td>
				<FORM method="post" name="form4" onSubmit="return Validator4()" action="ldbMultiexec.asp">
					<textarea style="WIDTH: 540px; HEIGHT: 95px" name=sMultiQuery cols=40></TEXTAREA>
					<input type="submit" value="Execute Multi (no Query)" size="5" name="pMultiQuery">
				</FORM>
			</td>
		</tr>
	</TABLE>
</BODY>
</HTML>
