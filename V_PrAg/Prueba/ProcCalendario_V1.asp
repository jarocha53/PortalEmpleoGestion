<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%@ Language=VBScript %>
<!-- #INCLUDE FILE="Utils.asp" -->

<%

Class Calendario

'####FUNCION QUE ACTUA COMO INTERFAZ (RESCATA LA INFO NECESARIA DE LA BASE Y
'PASA LOS PARAMETROS CORRESPONDIENTES A LA FUNCION QUE GENERA EL CALENDARIO####


public Function CrearCalendario(Mes,Anio)

	CrearCalendario = ""

	dim cn
	dim rs

	set cn = server.CreateObject("adodb.connection")
	set rs = server.CreateObject("adodb.recordset")

'PL-SQL * T-SQL  
	cn.open "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=C:\Inetpub\wwwroot\V_PrAg\Agenda.mdb;Persist Security Info=False"

	Mes = "01"
	Anio = "2006"

	dim consulta
	dim cadena

	consulta = "Select * from AGENDA where mes = '" & Mes & "' and anio = '" & Anio & "'"

'PL-SQL * T-SQL  
CONSULTA = TransformPLSQLToTSQL (CONSULTA) 
	rs.Open consulta,CN

	separador = "|"
	Atributos = ""
	Saltos = ""

	if not rs.EOF then 
		do while not rs.EOF 
			Atributos = Atributos & rs("dia") & separador & rs("atributo") & separador
			Saltos = Saltos & rs("dia") & separador & rs("salto") & separador
			rs.MoveNext 
		loop
	end if

	if right(Atributos,1) = "|" then
		Atributos = left(Atributos,len(Atributos) - 1)
	end if

	if right(Saltos,1) = "|" then
		Saltos = left(Saltos,len(Saltos) - 1)
	end if

	CrearCalendario = ProcesarCalendario (Mes,Anio,Atributos,Saltos) 

	set rs = nothing
	set cn = nothing

end function


'####FUNCION QUE GENERA EL CALENDARIO####

public function ProcesarCalendario (MesAMostrar,AnioAMostrar,Atributos,Saltos)

	ProcesarCalendario = ""

	'#Armo arrays con los parametros #

	ArrAtributos = split(Atributos,"|") '#01/F/05/F#
	ArrSaltos = split(Saltos,"|")		'#20/E/05/M/07/F#

	'#Defino los distintos colores que se van a usar#

	DiaActual = ""						'DiaActual = ""  
	DiaNormal = "LightYellow" 			'DiaNormal = "#E4EBFC"  
	DiaApuntado = "LightBlue" 			'DiaApuntado = "#E0D6A9" 
	DiaFeriado = "Khaki" 				'DiaFeriado = "#DEBCBD" 
	DiaCompleto = "LightSalmon"			'D�aCompleto 

	Fase = weekday("1/" & MesAMostrar & "/" & AnioAMostrar,2) 'obtengo el primer dia del mes elegido

	'#Armo la tabla#

	ProcesarCalendario = ProcesarCalendario & "<table bordercolor='#578489' border=1 cellspacing=5 style='font-family: Verdana; font-size: 7pt'>"

	'#Escribo los nombres de los dias de la semana#

	ProcesarCalendario = ProcesarCalendario  & "<tr CLASS='sfondocomm'>"
	ProcesarCalendario = ProcesarCalendario & EscribirNombresDias()
	ProcesarCalendario = ProcesarCalendario & "</tr>"


	'#Escribo en blanco los casilleros vacios del mes anterior#

	ProcesarCalendario = ProcesarCalendario & "<tr>"
	ProcesarCalendario = ProcesarCalendario & EscribirVaciosMesAnterior(Fase,DiaNormal) 'dia y color
	
	i = 1
	col = Fase-1
	rig = 0

	do
		ActDate = i & "/" & MesAMostrar & "/" & AnioAMostrar

		if IsDate(ActDate) then
		
		SysData = Dateserial(AnioAMostrar,MesAMostrar,i)
			col = col + 1
			'#Defino el color#
			
				bg = DiaNormal
				
				'#Sabados y domingos# 
				'va a estar condicionado a los dias establecidos en la base como laborables y no laborables..
				
				if col > 5 then bg = DiaFeriado 
				if SysData = Date  then
					DiaActual = "style='border: 2; border-style: ridge; border-color: #FF0000'"
				else
					DiaActual = " "
				end if

			if EncontrarDia(Atributos,i)= "A" then
				bg = DiaApuntado
				ProcesarCalendario = ProcesarCalendario & "<td " & DiaActual & " align=center bgcolor='" & bg & "'><b><a href='" & EncontrarDia(Saltos,i) & "?data=" & ActDate & "'>" & i & "</a></b></td>" '### Scrivo i giorni
			elseif EncontrarDia(Atributos,i)= "F" then
				bg = DiaFeriado
				ProcesarCalendario = ProcesarCalendario & "<td " & DiaActual & " align=center bgcolor='" & bg & "'><b><a href='" & EncontrarDia(Saltos,i) & "?data=" & ActDate & "'>" & i & "</b></td>" '### Scrivo i giorni
			elseif EncontrarDia(Atributos,i)= "L" then
				bg = DiaNormal
				ProcesarCalendario = ProcesarCalendario & "<td " & DiaActual & " align=center bgcolor='" & bg & "'><b><a href='" & EncontrarDia(Saltos,i) & "?data=" & ActDate & "'>" & i & "</b></td>" '### Scrivo i giorni
			elseif EncontrarDia(Atributos,i)= "C" then
				bg = DiaCompleto
				ProcesarCalendario = ProcesarCalendario & "<td " & DiaActual & " align=center bgcolor='" & bg & "'><b><a href='" & EncontrarDia(Saltos,i) & "?data=" & ActDate & "'>" & i & "</b></td>" '### Scrivo i giorni			
			'#no iria#
			else
				ProcesarCalendario = ProcesarCalendario & "<td " & DiaActual & " align=center bgcolor='" & bg & "'>" & i & "</td>" '### Scrivo i giorni
			end if
					
			
			if weekday(ActDate) = 1 then
				ProcesarCalendario = ProcesarCalendario & "</tr><tr>"
				col = 0
				rig = rig + 1
			end if
		end if
	i = i + 1
	loop until (i > 31) or not IsDate(ActDate)


	'#Sabados y domingos#
	for j = col+1 to 7
		ProcesarCalendario = ProcesarCalendario & "<td "
		if j > 5 then 
			ProcesarCalendario = ProcesarCalendario & "bgcolor='" & DiaFeriado & "' "
		else
			ProcesarCalendario = ProcesarCalendario & "bgcolor='" & DiaNormal & "' "
		end if
		ProcesarCalendario = ProcesarCalendario & ">&nbsp;</td>"
	next

	ProcesarCalendario = ProcesarCalendario & "</tr>"

	'#rig marca la ultima fila que completa con espacios si no hay dias que ocupen la fila nro 6#

	if rig < 5 then
		ProcesarCalendario = ProcesarCalendario & "<tr>"
		ProcesarCalendario = ProcesarCalendario & EscribirFilaFinalVacia(DiaFeriado,DiaNormal)
		ProcesarCalendario = ProcesarCalendario &  "</tr>"
	end if

	ProcesarCalendario = ProcesarCalendario &  "</table>"
	
end function
		
		
private function EncontrarDia (A,N)

	'#Busca el d�a y retorna el atributo (si es feriado, libre, etc.)#
	
	Arr = split(A,"|") 

	dim r
	for r = 0 to 9  
		if cint(N) = r then 
			N = cstr("0" & r) 
		else
			N = cstr(N)
		end if
	next

	dim x
	for x = lbound(Arr) to ubound(Arr)
		if Arr(x) = N then 
			EncontrarDia = Arr(x+1)
			exit for
		end if
	next 

end function

private function ObtenerNombreMes(MesAMostrar)

	'#Obtiene el nombre del mes a mostrar (en castellano)#
	
	dim y

	dim r
	for r = 0 to 9  
		if cint(MesAMostrar) = r then 
			MesAMostrar = cstr("0" & r) 
		else
			MesAMostrar = cstr(MesAMostrar)
		end if
	next

	Meses = "01|Enero|02|Febrero|03|Marzo|04|Abril|05|Mayo|06|Junio|07|Julio|08|Agosto|09|Septiembre|10|Octubre|11|Noviembre|12|Diciembre"

	ArrMeses = split(Meses,"|")

	for y = lbound(ArrMeses) to ubound(ArrMeses)
		if ArrMeses(y) = MesAMostrar then 
			ObtenerNombreMes = ArrMeses(y+1)
			exit for
		end if 
	next

end function 


private function EscribirNombresDias()
	
	for k = 0 to 6
		EscribirNombresDias = EscribirNombresDias & "<td bgcolor ='DarkKhaki' bordercolor='Black'><b>" & DN(k) & "</b></td>" 
	next
	
end function


private function EscribirVaciosMesAnterior(Mes,Color)
	
	for j = 1 to Mes -1
		EscribirVaciosMesAnterior = EscribirVaciosMesAnterior & "<td bgcolor='" & Color & "'>&nbsp;</td>" 
	next
	
end function


private function EscribirFilaFinalVacia(DiaFeriado,DiaNormal)
	
	for k = 1 to 7 
		EscribirFilaFinalVacia = EscribirFilaFinalVacia & "<td "
		if k > 5 then 
			EscribirFilaFinalVacia = EscribirFilaFinalVacia & "bgcolor='" & DiaFeriado & "' "
		else
			EscribirFilaFinalVacia = EscribirFilaFinalVacia & "bgcolor='" & DiaNormal & "' "
		end if
		EscribirFilaFinalVacia = EscribirFilaFinalVacia & ">&nbsp;</td>"
	next
	
end function


public function CrearComboAgenda(Nombre,Clase,Validacion,Tipo,Valor)
	dim retorna
	
	Tipo = ucase(Tipo)
	
	retorna = ""
	retorna = retorna & "<select class='" & Clase & "' name='" & Nombre & "' " & Validacion & ">"

	select case Tipo
		case "MESES"
			for I = 1 to 12
				retorna = retorna & "<option value='" & I & "' "
					if I = Int(Valor) then retorna = retorna & "Selected"
					retorna = retorna & " >" & mesi(I-1) & "</option>"
			next
		case "ANIOS"
			for I = year(date)-10 to year(date)+10
				retorna = retorna & "<option value='" & I & "' "
				if I = Int(Valor) then retorna = retorna & "Selected"
				retorna = retorna & " >" & I & "</option>"
			next
		case "HORAS"
			for horas=0 to 23
				for minutos=0 to 59 step 5
					select case horas
						case "0","1","2","3","4","5","6","7","8","9"
							ValHoras = 0 & horas
						case else
							ValHoras = horas
					end select

					if minutos=0 then
						ValMinutos="00"
					elseif minutos=5 then
						ValMinutos = "05"
					else
						ValMinutos= minutos
					end if
					Horario= ValHoras & ":" & ValMinutos
					retorna = retorna & "<option value='" & Horario & "'>" & Horario & "</option>"
				next
			next
	end select

	retorna = retorna & "</select>"
	
	CrearComboAgenda = retorna
	
end function


public function ObtenerMes(Mes,Tipo)
	Tipo = ucase(Tipo)
	
	Select case Tipo
		case "PRE"
			If Mes = 1 then
				ObtenerMes = 12
				if ObtenerMes<10 then ObtenerMes= "0" & ObtenerMes
			else
				ObtenerMes = Mes -1
				if ObtenerMes<10 then ObtenerMes= "0" & ObtenerMes
			end if

		case "POS"
			If Mes = 12 then
				ObtenerMes = 1
				if ObtenerMes<10 then ObtenerMes= "0" & ObtenerMes
			else
				ObtenerMes = Mes +1
				if ObtenerMes<10 then ObtenerMes= "0" & ObtenerMes
			end if
		case "ACT"
			if Mes<10 then 
				ObtenerMes="0" & Mes
			else
				ObtenerMes = Mes
			end if 
	end select
end function


public function ObtenerAnio(Mes,Anio,Tipo)
	Tipo = ucase(Tipo)
	
	select case Tipo
		case "PRE"
			If Mes = 1 then
				ObtenerAnio = Anio -1
			else
				ObtenerAnio = Anio
			end if
		case "POS"
			If Mes = 12 then
				ObtenerAnio = Anio +1
			else
				ObtenerAnio = Anio
			end if
	end select
end function
 















public function CrearComboAgenda2(Nombre,Clase,Validacion,Tipo,Valor)
	dim retorna
	
	Tipo = ucase(Tipo)
	
	retorna = ""
	retorna = retorna & "<select class='" & Clase & "' name='" & Nombre & "' " & Validacion & ">"

	select case Tipo
		case "MESES"
			for I = 1 to 12
				retorna = retorna & "<option value='" & I & "' "
					if I = Int(Valor) then retorna = retorna & "Selected"
					retorna = retorna & " >" & mesi(I-1) & "</option>"
			next
		case "ANIOS"
			for I = year(date)-10 to year(date)+10
				retorna = retorna & "<option value='" & I & "' "
				if I = Int(Valor) then retorna = retorna & "Selected"
				retorna = retorna & " >" & I & "</option>"
			next
		case "HORAS"
		
		'dim ArrValor()

		
		ArrValor = Split(Valor,"|")
		
		cantidad = cint(ubound(ArrValor))

		
		dim ArrHorasDesde()
		dim ArrMinutosDesde()
		dim ArrHorasHasta()
		dim ArrMinutosHasta()

		redim ArrHorasDesde(cantidad)  
		redim ArrMinutosDesde(cantidad)
		redim ArrHorasHasta(cantidad)  
		redim ArrMinutosHasta(cantidad)

		'ej franjas: "750,810|840,1110"
		
		
		for f = lbound(ArrValor) to ubound(ArrValor) 
			'ArrAuxP = split(cstr(ArrValor(f)),",")
			
			'ValorPrimario = ArrAuxP(0)
			'ValorSecundario = ArrAuxP(1)
			
			ValorPrimario = left(ArrValor(f),(instr(1,ArrValor(f),",")-1)) / 60
			ValorSecundario = right(ArrValor(f),(len(ArrValor(f))-instr(1,ArrValor(f),","))) / 60

		
			if instr(1,cstr(ValorPrimario),",") = 0 then
				HorasDesde = cstr(ValorPrimario)
				MinutosDesde = "00"
			else
				HorasDesde = cstr(left(ValorPrimario,(instr(1,cstr(ValorPrimario),",")-1)))
				MinutosDesde = cstr(cint(round(cdbl(mid(ValorPrimario,(instr(1,cstr(ValorPrimario),",")))),2)*60))
			end if
			
			ValorDesde = HorasDesde & ":" & MinutosDesde
			
			if instr(1,cstr(ValorSecundario),",") = 0 then
				HorasHasta = cstr(ValorSecundario)
				MinutosHasta = "00"
			else
				HorasHasta = cstr(left(ValorSecundario,(instr(1,cstr(ValorSecundario),",")-1)))
				MinutosHasta = cstr(cint(round(cdbl(mid(ValorSecundario,(instr(1,cstr(ValorSecundario),",")))),2)*60))
			end if
			ValorHasta = HorasHasta & ":" & MinutosHasta
			
			ArrHorasDesde(f) = cint(HorasDesde)
			ArrMinutosDesde(f) = cint(MinutosDesde)
			ArrHorasHasta(f) = cint(HorasHasta)
			ArrMinutosHasta(f)= cint(MinutosHasta)
			
			'ValoresConvertidos(f) 
			'Response.Write ValorPrimario & "&nbsp;"
			'Response.Write ValorDesde & "&nbsp;"			
			'Response.Write HorasDesde & "&nbsp;"
			'Response.Write MinutosDesde & "&nbsp;"
			'Response.Write ValorSecundario & "&nbsp;"
			'Response.Write ValorHasta & "&nbsp;"			
			'Response.Write HorasHasta & "&nbsp;"
			'Response.Write MinutosHasta & "&nbsp;"
		next 


		for y = lbound(ArrHorasDesde) to ubound(ArrHorasDesde)


			HoraInicial = ArrHorasDesde(y) & ":" & ArrMinutosDesde(y)
			
			HoraFinal = ArrHorasHasta(y) & ":" & ArrMinutosHasta(y)
				
			minutos = cint(ArrMinutosDesde(y))
			horaact = cint(ArrHorasDesde(y))	
			
			do until (HoraFinal = Horario)
				
				if minutos=0 then
					ValMinutos="00"
				elseif minutos=5 then
					ValMinutos = "05"
				else
					ValMinutos = cstr(minutos)
				end if
				
				select case horaact
					case 0,1,2,3,4,5,6,7,8,9
						ValHoras = "0" & cstr(horaact)
					case else
						ValHoras = cstr(horaact)
				end select
				
				Horario = ValHoras & ":" & ValMinutos
				
				if minutos = 55 then
					horaact = horaact + 1
					minutos = 0 
				else				
					minutos = minutos + 5
				end if
				
				retorna = retorna & "<option value='" & Horario & "'>" & Horario & "</option>"
			loop
		next
	end select

	retorna = retorna & "</select>"
	
	CrearComboAgenda2 = retorna
	
end function

  
end Class
%>
