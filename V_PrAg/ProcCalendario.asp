<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->

<!-- #INCLUDE FILE="Utils.asp" -->

<%

Class Calendario

'####FUNCION QUE ACTUA COMO INTERFAZ (RESCATA LA INFO NECESARIA DE LA BASE Y
'PASA LOS PARAMETROS CORRESPONDIENTES A LA FUNCION QUE GENERA EL CALENDARIO####


public Function CrearCalendario(Mes,Anio)

	CrearCalendario = ""

	dim cn
	dim rs

	set cn = server.CreateObject("adodb.connection")
	set rs = server.CreateObject("adodb.recordset")

'PL-SQL * T-SQL  
	cn.open "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=C:\Inetpub\wwwroot\V_PrAg\Agenda.mdb;Persist Security Info=False"

	Mes = "01"
	Anio = "2006"

	dim consulta
	dim cadena

	consulta = "Select * from AGENDA where mes = '" & Mes & "' and anio = '" & Anio & "'"

'PL-SQL * T-SQL  
CONSULTA = TransformPLSQLToTSQL (CONSULTA) 
	rs.Open consulta,CN

	separador = "|"
	Atributos = ""
	Saltos = ""

	if not rs.EOF then 
		do while not rs.EOF 
			Atributos = Atributos & rs("dia") & separador & rs("atributo") & separador
			Saltos = Saltos & rs("dia") & separador & rs("salto") & separador
			rs.MoveNext 
		loop
	end if

	if right(Atributos,1) = "|" then
		Atributos = left(Atributos,len(Atributos) - 1)
	end if

	if right(Saltos,1) = "|" then
		Saltos = left(Saltos,len(Saltos) - 1)
	end if

	CrearCalendario = ProcesarCalendario (Mes,Anio,Atributos,Saltos) 

	set rs = nothing
	set cn = nothing

end function


'####FUNCION QUE GENERA EL CALENDARIO####

public function ProcesarCalendario (MesAMostrar,AnioAMostrar,Atributos,Saltos)

	ProcesarCalendario = ""

	'#Armo arrays con los parametros #

	ArrAtributos = split(Atributos,"|") '#01/F/05/F#
	ArrSaltos = split(Saltos,"|")		'#20/E/05/M/07/F#

	'#Defino los distintos colores que se van a usar#

	DiaActual = ""						'DiaActual = ""  
	DiaNormal = "LightYellow" 			'DiaNormal = "#E4EBFC"  
	DiaApuntado = "LightBlue" 			'DiaApuntado = "#E0D6A9" 
	DiaFeriado = "Khaki" 				'DiaFeriado = "#DEBCBD" 
	DiaCompleto = "LightSalmon"			'D�aCompleto 

	Fase = weekday("1/" & MesAMostrar & "/" & AnioAMostrar,2) 'obtengo el primer dia del mes elegido

	'#Armo la tabla#

	ProcesarCalendario = ProcesarCalendario & "<table bordercolor='#578489' border=1 cellspacing=5 style='font-family: Verdana; font-size: 7pt'>"

	'#Escribo los nombres de los dias de la semana#

	ProcesarCalendario = ProcesarCalendario  & "<tr CLASS='sfondocomm'>"
	ProcesarCalendario = ProcesarCalendario & EscribirNombresDias()
	ProcesarCalendario = ProcesarCalendario & "</tr>"


	'#Escribo en blanco los casilleros vacios del mes anterior#

	ProcesarCalendario = ProcesarCalendario & "<tr>"
	ProcesarCalendario = ProcesarCalendario & EscribirVaciosMesAnterior(Fase,DiaNormal) 'dia y color
	
	i = 1
	col = Fase-1
	rig = 0

	do
		ActDate = i & "/" & MesAMostrar & "/" & AnioAMostrar

		if IsDate(ActDate) then
		
		SysData = Dateserial(AnioAMostrar,MesAMostrar,i)
			col = col + 1
			'#Defino el color#
			
				bg = DiaNormal
				
				'#Sabados y domingos# 
				'va a estar condicionado a los dias establecidos en la base como laborables y no laborables..
				
				'if col > 5 then bg = DiaFeriado 
				if SysData = Date  then
					DiaActual = "style='border: 2; border-style: ridge; border-color: #FF0000'"
				else
					DiaActual = " "
				end if

			if EncontrarDia(Atributos,i)= "A" then
				bg = DiaApuntado
				ProcesarCalendario = ProcesarCalendario & "<td " & DiaActual & " align=center bgcolor='" & bg & "'><b><a href='" & EncontrarDia(Saltos,i) & "?data=" & ActDate & "'>" & i & "</a></b></td>" '### Scrivo i giorni
			elseif EncontrarDia(Atributos,i)= "F" then
				bg = DiaFeriado
				ProcesarCalendario = ProcesarCalendario & "<td " & DiaActual & " align=center bgcolor='" & bg & "'><b><a href='" & EncontrarDia(Saltos,i) & "?data=" & ActDate & "'>" & i & "</b></td>" '### Scrivo i giorni
			elseif EncontrarDia(Atributos,i)= "L" then
				bg = DiaNormal
				ProcesarCalendario = ProcesarCalendario & "<td " & DiaActual & " align=center bgcolor='" & bg & "'><b><a href='" & EncontrarDia(Saltos,i) & "?data=" & ActDate & "'>" & i & "</b></td>" '### Scrivo i giorni
			elseif EncontrarDia(Atributos,i)= "C" then
				bg = DiaCompleto
				ProcesarCalendario = ProcesarCalendario & "<td " & DiaActual & " align=center bgcolor='" & bg & "'><b><a href='" & EncontrarDia(Saltos,i) & "?data=" & ActDate & "'>" & i & "</b></td>" '### Scrivo i giorni			
			'#no iria#
			else
				ProcesarCalendario = ProcesarCalendario & "<td " & DiaActual & " align=center bgcolor='" & bg & "'>" & i & "</td>" '### Scrivo i giorni
			end if
					
			
			if weekday(ActDate) = 1 then
				ProcesarCalendario = ProcesarCalendario & "</tr><tr>"
				col = 0
				rig = rig + 1
			end if
		end if
	i = i + 1
	loop until (i > 31) or not IsDate(ActDate)


	'#Sabados y domingos#
	for j = col+1 to 7
		ProcesarCalendario = ProcesarCalendario & "<td "
		'if j > 5 then 
			ProcesarCalendario = ProcesarCalendario & "bgcolor='" & bg & "' "
			'ProcesarCalendario = ProcesarCalendario & "bgcolor='" & DiaFeriado & "' "
		'else
		'	ProcesarCalendario = ProcesarCalendario & "bgcolor='" & bg & "' "
		'	ProcesarCalendario = ProcesarCalendario & "bgcolor='" & DiaNormal & "' "
		'end if
		ProcesarCalendario = ProcesarCalendario & ">&nbsp;</td>"
	next

	ProcesarCalendario = ProcesarCalendario & "</tr>"

	'#rig marca la ultima fila que completa con espacios si no hay dias que ocupen la fila nro 6#

	if rig < 5 then
		ProcesarCalendario = ProcesarCalendario & "<tr>"
		ProcesarCalendario = ProcesarCalendario & EscribirFilaFinalVacia(bg)
		'ProcesarCalendario = ProcesarCalendario & EscribirFilaFinalVacia(DiaFeriado,DiaNormal)
		ProcesarCalendario = ProcesarCalendario &  "</tr>"
	end if

	ProcesarCalendario = ProcesarCalendario &  "</table>"
	
end function
		
		
private function EncontrarDia (A,N)

	'#Busca el d�a y retorna el atributo (si es feriado, libre, etc.)#
	
	Arr = split(A,"|") 

	dim r
	for r = 0 to 9  
		if cint(N) = r then 
			N = cstr("0" & r) 
		else
			N = cstr(N)
		end if
	next

	dim x
	for x = lbound(Arr) to ubound(Arr)
		if Arr(x) = N then 
			EncontrarDia = Arr(x+1)
			exit for
		end if
	next 

end function

private function ObtenerNombreMes(MesAMostrar)

	'#Obtiene el nombre del mes a mostrar (en castellano)#
	
	dim y

	dim r
	for r = 0 to 9  
		if cint(MesAMostrar) = r then 
			MesAMostrar = cstr("0" & r) 
		else
			MesAMostrar = cstr(MesAMostrar)
		end if
	next

	Meses = "01|Enero|02|Febrero|03|Marzo|04|Abril|05|Mayo|06|Junio|07|Julio|08|Agosto|09|Septiembre|10|Octubre|11|Noviembre|12|Diciembre"

	ArrMeses = split(Meses,"|")

	for y = lbound(ArrMeses) to ubound(ArrMeses)
		if ArrMeses(y) = MesAMostrar then 
			ObtenerNombreMes = ArrMeses(y+1)
			exit for
		end if 
	next

end function 

private function EscribirNombresDias()
	
	for k = 0 to 6
		EscribirNombresDias = EscribirNombresDias & "<td bgcolor ='DarkKhaki' bordercolor='Black'><b>" & DN(k) & "</b></td>" 
	next
	
end function


private function EscribirVaciosMesAnterior(Mes,Color)
	
	for j = 1 to Mes -1
		EscribirVaciosMesAnterior = EscribirVaciosMesAnterior & "<td bgcolor='" & Color & "'>&nbsp;</td>" 
	next
	
end function


private function EscribirFilaFinalVacia(Color)
	
	for k = 1 to 7 
		EscribirFilaFinalVacia = EscribirFilaFinalVacia & "<td "
		'if k > 5 then 
			EscribirFilaFinalVacia = EscribirFilaFinalVacia & "bgcolor='" & Color & "' "
			'EscribirFilaFinalVacia = EscribirFilaFinalVacia & "bgcolor='" & DiaFeriado & "' "
		'else
			'EscribirFilaFinalVacia = EscribirFilaFinalVacia & "bgcolor='" & DiaNormal & "' "
		'end if
		EscribirFilaFinalVacia = EscribirFilaFinalVacia & ">&nbsp;</td>"
	next
	
end function


public function ObtenerMes(Mes,Tipo)
	Tipo = ucase(Tipo)
	
	Select case Tipo
		case "PRE"
			If Mes = 1 then
				ObtenerMes = 12
				if ObtenerMes<10 then ObtenerMes= "0" & ObtenerMes
			else
				ObtenerMes = Mes -1
				if ObtenerMes<10 then ObtenerMes= "0" & ObtenerMes
			end if

		case "POS"
			If Mes = 12 then
				ObtenerMes = 1
				if ObtenerMes<10 then ObtenerMes= "0" & ObtenerMes
			else
				ObtenerMes = Mes +1
				if ObtenerMes<10 then ObtenerMes= "0" & ObtenerMes
			end if
		case "ACT"
			if Mes<10 then 
				ObtenerMes="0" & Mes
			else
				ObtenerMes = Mes
			end if 
	end select
end function


public function ObtenerAnio(Mes,Anio,Tipo)
	Tipo = ucase(Tipo)
	
	select case Tipo
		case "PRE"
			If Mes = 1 then
				ObtenerAnio = Anio -1
			else
				ObtenerAnio = Anio
			end if
		case "POS"
			If Mes = 12 then
				ObtenerAnio = Anio +1
			else
				ObtenerAnio = Anio
			end if
	end select
end function
 

public function CrearComboAgenda(Nombre,Clase,Validacion,Tipo,Valor,ValorElegido)
	dim retorna
	dim ArrMeses
	
	ArrMeses = split("Enero,Febrero,Marzo,Abril,Mayo,Junio,Julio,Agosto,Septiembre,Octubre,Noviembre,Diciembre",",")
	
	Tipo = ucase(Tipo)
	
	retorna = ""
	retorna = retorna & "<select class='" & Clase & "' name='" & Nombre & "' " & Validacion & ">"

	select case Tipo
		case "MESES"
			for I = 1 to 12
				retorna = retorna & "<option value='" & I & "' "
					if ValorElegido <> "" then
						if I = Int(ValorElegido) then 
							retorna = retorna & "Selected"
						end if
					end if
					retorna = retorna & " >" & ArrMeses(I-1) & "</option>"
			next
		case "ANIOS"
			for I = year(date)-10 to year(date)+10
				retorna = retorna & "<option value='" & I & "' "
				if ValorElegido <> "" then
					if I = Int(ValorElegido) then 
						retorna = retorna & "Selected"
					end if
				end if
				retorna = retorna & " >" & I & "</option>"
			next
		case "HORAS"
		
			ArrValor = Split(Valor,"|")
		
			cantidad = cint(ubound(ArrValor))

			dim ArrHorasDesde()
			dim ArrMinutosDesde()
			dim ArrHorasHasta()
			dim ArrMinutosHasta()

			redim ArrHorasDesde(cantidad)  
			redim ArrMinutosDesde(cantidad)
			redim ArrHorasHasta(cantidad)  
			redim ArrMinutosHasta(cantidad)

			'ej franjas: "750,810|840,1110"
				
			for f = lbound(ArrValor) to ubound(ArrValor) 
				
				ValorPrimario = ConvertirRangoADecimal(ArrValor(f),"I")
				ValorSecundario = ConvertirRangoADecimal(ArrValor(f),"D")

				if instr(1,cstr(ValorPrimario),",") = 0 then
					HorasDesde = cstr(ValorPrimario)
					MinutosDesde = "00"
				else
					HorasDesde = ObtenerHoras(ValorPrimario)
					MinutosDesde = ObtenerMinutos(ValorPrimario)
				end if
				
				ValorDesde = HorasDesde & ":" & MinutosDesde
				
				if instr(1,cstr(ValorSecundario),",") = 0 then
					HorasHasta = cstr(ValorSecundario)
					MinutosHasta = "00"
				else
					HorasHasta = ObtenerHoras(ValorSecundario)
					MinutosHasta = ObtenerMinutos(ValorSecundario)
				end if
				ValorHasta = HorasHasta & ":" & MinutosHasta
				
				'Response.Write ValorDesde
				'Response.Write ValorHasta
				
				''cargo los arrays con las franjas horarias separadas en horas y minutos
				
				ArrHorasDesde(f) = cint(HorasDesde)
				ArrMinutosDesde(f) = cint(MinutosDesde)
				ArrHorasHasta(f) = cint(HorasHasta)
				ArrMinutosHasta(f)= cint(MinutosHasta)

			next 

			 

			for y = lbound(ArrHorasDesde) to ubound(ArrHorasDesde)
					HD = ArrHorasDesde(y)
				    HH = ArrHorasHasta(y)
				    MD = ArrMinutosDesde(y)
				    MH = ArrMinutosHasta(y)
				        
				        If MD = 0 Then
				            MD = "00"
				        ElseIf MD = 5 Then
				            MD = "05"
				        Else
				            MD = CStr(MD)
				        End If
				        
				        If MH = 0 Then
				            MH = "00"
				        ElseIf MH = 5 Then
				            MH = "05"
				        Else
				            MH = CStr(MH)
				        End If
				        
				        Select Case HD
				            Case 0, 1, 2, 3, 4, 5, 6, 7, 8, 9
				                HD = "0" & CStr(HD)
				            Case Else
				                HD = CStr(HD)
				        End Select
				        
				        Select Case HH
				            Case 0, 1, 2, 3, 4, 5, 6, 7, 8, 9
				                HH = "0" & CStr(HH)
				            Case Else
				                HH = CStr(HH)
				        End Select
				            
				HoraInicial = HD & ":" & MD
				HoraFinal = HH & ":" & MH
					
				minutos = cint(ArrMinutosDesde(y))
				horaact = cint(ArrHorasDesde(y))	
				
				do until (HoraFinal = Horario)
					
					if minutos=0 then
						ValMinutos="00"
					elseif minutos=5 then
						ValMinutos = "05"
					else
						ValMinutos = cstr(minutos)
					end if
					
					select case horaact
						case 0,1,2,3,4,5,6,7,8,9
							ValHoras = "0" & cstr(horaact)
						case else
							ValHoras = cstr(horaact)
					end select
					
					Horario = ValHoras & ":" & ValMinutos
					
					if horaact = 23 and minutos = 55 then 
						horaact = 0
						minutos = 0 
					else 
						if minutos = 55 then
							horaact = horaact + 1
							minutos = 0 
						else				
							minutos = minutos + 5
						end if
					end if
					
					if ValorElegido <> "" and (ValorElegido = Horario) then 
						retorna = retorna & "<option value='" & Horario & "' selected>" & Horario & "</option>"
					else
						retorna = retorna & "<option value='" & Horario & "'>" & Horario & "</option>"
					end if
					
					
				loop
			next
	end select

	retorna = retorna & "</select>"
	
	CrearComboAgenda = retorna
	
end function

public function ObtenerRangoPosible(HorarioElegido,Rangos)

		ArrValor = Split(Rangos,"|")
		cantidad = cint(ubound(ArrValor))

		dim ArrHorasDesde()
		dim ArrMinutosDesde()
		dim ArrHorasHasta()
		dim ArrMinutosHasta()

		redim ArrHorasDesde(cantidad)  
		redim ArrMinutosDesde(cantidad)
		redim ArrHorasHasta(cantidad)  
		redim ArrMinutosHasta(cantidad)

			'ej franjas: "750,810|840,1110"
				
		for f = lbound(ArrValor) to ubound(ArrValor) 
				
			ValorPrimario = ConvertirRangoADecimal(ArrValor(f),"I")
			ValorSecundario = ConvertirRangoADecimal(ArrValor(f),"D")
		
			if instr(1,cstr(ValorPrimario),",") = 0 then
				HorasDesde = cstr(ValorPrimario)
				MinutosDesde = "00"
			else
				HorasDesde = ObtenerHoras(ValorPrimario)
				MinutosDesde = ObtenerMinutos(ValorPrimario)
			end if
				
			ValorDesde = HorasDesde & ":" & MinutosDesde
			
			if instr(1,cstr(ValorSecundario),",") = 0 then
				HorasHasta = cstr(ValorSecundario)
				MinutosHasta = "00"
			else
				HorasHasta = ObtenerHoras(ValorSecundario)
				MinutosHasta = ObtenerMinutos(ValorSecundario)
			end if
			ValorHasta = HorasHasta & ":" & MinutosHasta
				
			ArrHorasDesde(f) = cint(HorasDesde)
			ArrMinutosDesde(f) = cint(MinutosDesde)
			ArrHorasHasta(f) = cint(HorasHasta)
			ArrMinutosHasta(f)= cint(MinutosHasta)

		next 
		
		for y = lbound(ArrHorasDesde) to ubound(ArrHorasDesde)				
		            HD = ArrHorasDesde(y)
				    HH = ArrHorasHasta(y)
				    MD = ArrMinutosDesde(y)
				    MH = ArrMinutosHasta(y)
				        
				        If MD = 0 Then
				            MD = "00"
				        ElseIf MD = 5 Then
				            MD = "05"
				        Else
				            MD = CStr(MD)
				        End If
				        
				        If MH = 0 Then
				            MH = "00"
				        ElseIf MH = 5 Then
				            MH = "05"
				        Else
				            MH = CStr(MH)
				        End If
				        
				        Select Case HD
				            Case 0, 1, 2, 3, 4, 5, 6, 7, 8, 9
				                HD = "0" & CStr(HD)
				            Case Else
				                HD = CStr(HD)
				        End Select
				        
				        Select Case HH
				            Case 0, 1, 2, 3, 4, 5, 6, 7, 8, 9
				                HH = "0" & CStr(HH)
				            Case Else
				                HH = CStr(HH)
				        End Select
				            
				HoraInicial = HD & ":" & MD
				HoraFinal = HH & ":" & MH
			
			if HoraInicial = cstr(HorarioElegido) then
				ObtenerRangoPosible = ConvertirHorarioAMinutos(HoraInicial) & "," & ConvertirHorarioAMinutos(HoraFinal)
			elseif HoraFinal = cstr(HorarioElegido) then
				ObtenerRangoPosible = ConvertirHorarioAMinutos(HoraInicial) & "," & ConvertirHorarioAMinutos(HoraFinal)
			else

					
				minutos = cint(ArrMinutosDesde(y))
				horaact = cint(ArrHorasDesde(y))	
				
				do until (HoraFinal = Horario)
					
					if minutos=0 then
						ValMinutos="00"
					elseif minutos=5 then
						ValMinutos = "05"
					else
						ValMinutos = cstr(minutos)
					end if
					
					select case horaact
						case 0,1,2,3,4,5,6,7,8,9
							ValHoras = "0" & cstr(horaact)
						case else
							ValHoras = cstr(horaact)
					end select
					
					Horario = ValHoras & ":" & ValMinutos
					
					if horaact = 23 and minutos = 55 then 
						horaact = 0
						minutos = 0 
					else 
						if minutos = 55 then
							horaact = horaact + 1
							minutos = 0 
						else				
							minutos = minutos + 5
						end if
					end if
					
					if Horario = HorarioElegido then 
						ObtenerRangoPosible = ConvertirHorarioAMinutos(HoraInicial) & "," & ConvertirHorarioAMinutos(HoraFinal)
						exit do
					end if
				loop
				
			end if
		
		next


end function

  
private function ConvertirHorarioAMinutos(Horario)
	dim retorna
	dim horas
	dim minutos
	
	retorna = ""
	
	'"12:00"
	
	posicion = instr(1,Horario,":")
	
	if posicion <> 0 then 
		horas = left(Horario,posicion - 1)
		minutos = right(Horario,len(Horario)-posicion)
		retorna = (cint(Horas) * 60) + cint(minutos)
	end if
	
	ConvertirHorarioAMinutos = retorna

end function

public function ConvertirRangoADecimal(Rango,Posicion)

	dim retorna
	
	retorna = ""
	
	Posicion = ucase(Posicion)
	
	select case Posicion
	
	case "I"
		retorna = left(Rango,(instr(1,Rango,",")-1)) / 60
	case "D"
		retorna = right(Rango,(len(Rango)-instr(1,Rango,","))) / 60
	end select
	
	ConvertirRangoADecimal = retorna
	
end function 

public function ObtenerMinutosTotales(Valor)
''paso el valor decimal

	dim retorna
	
	retorna = ""
	if instr(1,cstr(Valor),",") <> 0 then
		retorna = cstr(cint(round(cdbl(mid(Valor,(instr(1,cstr(Valor),",")))),2)*60))
		retorna = retorna + ((left(Valor,(len(Valor)-(len(Valor) - instr(1,cstr(Valor),",")+1))))*60)
	else
		retorna = Valor * 60
	end if
	
	ObtenerMinutosTotales = retorna
end function

public function ObtenerMinutos(Valor)
''paso el valor decimal

	dim retorna
	
	retorna = ""
	if instr(1,cstr(Valor),",") <> 0 then
		retorna = cstr(cint(round(cdbl(mid(Valor,(instr(1,cstr(Valor),",")))),2)*60))
	else
		retorna = Valor * 60
	end if
	
	ObtenerMinutos = retorna
end function

public function ObtenerHoras(Valor)
''paso el valor decimal

	dim retorna
	
	retorna = ""
	
	retorna = cstr(left(Valor,(instr(1,cstr(Valor),",")-1)))
	
	ObtenerHoras = retorna
end function

public function ConvertirRangoAHorario(Rango,Posicion)
	dim retorna
	dim iterm
	
	retorna = ""
	interm = ConvertirRangoADecimal(Rango,Posicion)
	
	retorna = ObtenerHoras(interm) & ":" & ObtenerMinutos(interm)
	
	ConvertirRangoAHorario = retorna
end function

public function ConvertirMinutosAHorario(Minutos)
	dim retorna
	dim iterm
	
	retorna = ""
	interm = Minutos / 60
	
	retorna = ObtenerHoras(interm) & ":" & ObtenerMinutos(interm)
	
	ConvertirMinutosAHorario = retorna
end function

public function EsBetweenIgual(Valor1,Valor2,Valor3)
	if (Valor1 <= Valor2) and (Valor2 <= Valor3) then 
		EsBetweenIgual = true 
	else
		EsBetweenIgual = false
	end if
	'Response.Write EsBetweenIgual
end function

public function EsMenorIgual(Valor1,Valor2)
	if Valor1 <= Valor2 then 
		EsMenorIgual = true 
	else
		EsMenorIgual = false
	end if
end function 

public function EsMayorIgual(Valor1,Valor2)
	if Valor1 >= Valor2 then 
		EsMayorIgual = true 
	else
		EsMayorIgual = false
	end if
end function 




private function ObtenerNombreDia(DiaAMostrar)

	'#Obtiene el nombre del dia a mostrar (en castellano)#
	
	dim y

	dim r
	for r = 0 to 7  
		if cint(DiaAMostrar) = r then 
			DiaAMostrar = cstr("0" & r) 
		else
			DiaAMostrar = cstr(DiaAMostrar)
		end if
	next

	Dias = "01|Lunes|02|Martes|03|Miercoles|04|Jueves|05|Viernes|06|Sabado|07|Domingo"

	ArrDias = split(Dias,"|")

	for y = lbound(ArrDias) to ubound(ArrDias)
		if ArrDias(y) = DiaAMostrar then 
			ObtenerNombreDia = ArrDias(y+1)
			exit for
		end if 
	next

end function 
end Class
%>
