<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #include Virtual="/strutt_testa2.asp" -->
<!-- #INCLUDE FILE="Utils.asp" -->

<script language="javascript" src="/Include/ControlString.inc">
</script>
<%
Id = request("Id")

Set conn = Server.CreateObject("ADODB.Connection")
Conn.Open strconn
%>


<script language="Javascript">
<!--
function apriRub(URL)
{
	URL += "?VISMENU=NO"										//Se la modalit� � popUp non deve comparire il menu
	URL += "&Mod_Fun=3"											//Questo Parametro Indica il modo di unzionamento della popUp di ricerca utenti
	URL += "&Nome_ListaIDUTENTE=inserisci.ElencoIdUtente"		//Questo parametro indica il nome del campo nel quale inserire gli IDUTENTE
	URL += "&Nome_ListaNominativo=inserisci.ElencoPersone"		//Questo Parametro indica il nome del campo nel quale inserire i Nominativi delle persone
	URL += "&Nome_ListaLogin=niente"							//Questo Parametro indica il nome del campo nel quale inserire le login degli utenti
	
	window.open (URL,"Rubrica","Status=no,toolbar=no,width=525,height=390,top=20,left=100")
}

function apriSelGruppi(URL)
{
	URL += "?VISMENU=NO"										//Se la modalit� � popUp non deve comparire il menu
	URL += "&Nome_ListaIDGRUPPO=inserisci.ElencoGruppi"			//Questo parametro indica il nome del campo nel quale inserire gli IDGRUPPO
	URL += "&Nome_ListaDesgruppo=inserisci.ElencoPersone"		//Questo Parametro indica il nome del campo nel quale inserire Le Descrizioni del gruppo

	window.open (URL,"Gruppi","Status=no,toolbar=no,width=525,height=200,top=20,left=100")
}

function chkFrm(frm)
{
	if((frm.Oggetto.value == "") || (frm.Testo.value == ""))
	{
		alert("Completar los campos 'Asunto' y 'Texto'")
		return false;
	}
	
	
	var stringa = frm.Testo.value
	var LenStringa = stringa.length
	if ((LenStringa > 255))
	{
		alert("En el campo 'Texto' \n se pueden ingresar como m�ximo \n 255 caracteres")
		return false;
	}

	
	g = frm.gg.value
	m = frm.mm.value
	a = frm.aaaa.value
	
	if(!checkDate(g,m,a))
	{
		alert("La fecha ingresada no es una fecha v�lida")
		return false;
	}
}


//-->
</script>
</head>

<form name="inserisci" action="salva.asp" method="POST" OnSubmit="return chkFrm(this)">

<br>
<table border="0" width="500" CELLPADDING="0" cellspacing="0">
	<tr class="tbltext0">
		<td class="sfondomenu" align="left"> 
		<span class="tbltext0">
		<b>AGENDA</b></span>
		</td>
		<td width="25" valign="bottom" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">
		<img border="0" src="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
		<td width="278" valign="MIDDLE" align="right" class="tbltext1" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">
		</td>
	</tr>
</table>

<table border="0" width="500" CELLPADDING="0" cellspacing="0">
	<tr class="sfondocomm">
		<td>
		Complete los siguientes campos para ingresar una cita.
		</td>
	   	<td class="sfondocomm">
		<a href="Javascript:Show_Help('/Pgm/Help/ProjectSite/Agenda/InsAppuntamento/')">
		 <img src="<%=Session("Progetto")%>/images/Help.gif" border="0" align="right" alt="Help">
		</a>
		</td>		
    </tr>
    <tr height="2">
		<td colspan="2" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
    </tr>
</table>

<br>



<table border="0" width="500">
<tr class="sfondocomm">
	<td valign="top" width="500" align="center"><b><%=Session("Nome")%>&nbsp;<%=session("Cognome")%>  - Registros en la agenda</b></td>
</tr>
</table>
<br>


<table border="0" cellspacing="0" cellpadding="1" width="500">
<input type="hidden" name="Id" value="<%=Id%>">
<input type="hidden" name="Tipo" value="NEW">
<input type="hidden" name="Mittente" value="<%= session("UserName")%>">

<!--###### COMBO DATA  ###### -->
<tr>
	<td CLASS="tbltext1"><b>Fecha</b></td>
	<td>
       <select Name="gg" CLASS="My">
	   <%
	   for gg = 1 to 31
	   	if gg=day(date) then
	   		response.write "<option selected value='" & gg & "'>" & gg & "</option>"
	   	else
	   		response.write "<option value='" & gg & "'>" & gg & "</option>"
	   	end if
	   next
	   %>
	   </select>
	  
	  <select Name="mm" CLASS="My">
	   <%
	   for mm = 1 to 12
	   	if mm = month(date) then
	   		response.write "<option selected value='" & mm & "'>" & mesi(mm-1) & "</option>"
	   	else
	   		response.write "<option value='" & mm & "'>" & mesi(mm-1) & "</option>"
	   	end if
	   next
	   %>
	   </select>
	
		<select Name="aaaa" CLASS="My">
	   		<option selected value="<%= year(date)%>"> <%= year(date)%> </option>
	   		<option value="<%= year(date)+1%>"> <%= year(date)+1%> </option>
			<option value="<%= year(date)+2%>"> <%= year(date)+2%> </option>
		</select>

		<select size="1" name="Ora" CLASS="My">
		<%
			' composizione combo Ore con orario preimpostato.
			for ore=8 to 20
				for minuti=0 to 30 step 30

					if ore= "8" or  ore= "9" then
						vore=0 & ore
					else
						vore= ore
					end if

					if minuti=0 then
						ValMinuti="00"
					else
						ValMinuti= minuti
					end if
					Valore= vore & "." & ValMinuti
					Response.write "<option value='" & Valore & "'>" & valore & "</option>"
				next
			next
		%>
        </select>
	</td>
</tr>

 <tr>
	<td CLASS="tbltext1"><b>Asunto*</b></td>
	<td>
		<input CLASS="MyTextBox" type="text" name="Oggetto" size="51" value maxlength="255">
	</td>
 </tr>

 <tr>
	<td CLASS="tbltext1"><b>Texto*</font></b></td>
	<td>
          <textarea OnKeyUp="JavaScript:CheckLenTxArea(document.inserisci.Testo,document.inserisci.textNumCaratteri,255);" rows="4" CLASS="MyTextBox" name="Testo" cols="50"></textarea>
          &nbsp;
          <input type="text" name="textNumCaratteri" value="255" size="3" readonly>
	</td>
</tr>
<tr>
	<td CLASS="tbltext1"><b>Notificar con</b></td>
	<td CLASS="tbltext1">
		<input type="checkbox" CLASS="MyTextBox" name="msgEmail" Value="True"> email
		<input type="checkbox" CLASS="MyTextBox" name="msgInter" Value="True" checked> mensaje interno
	</td>
</tr>
<tr><td colspan="2">&nbsp;</td></tr>
<tr>
	<td CLASS="tbltext1"><b>Opciones</b></td>
	<td>
		<p align = 'left'>
		<input type="Button" Value="Convocar persona" OnClick="apriRub(&quot;../Rubrica/PopUp_FormCerca.asp&quot;)" Class="My">
		<input type="Button" Value="Convocar grupo" OnClick="apriSelGruppi(&quot;../Rubrica/PopUp_SelGruppi.asp&quot;)" Class="My">
		</p>
	</td>
</tr>

<tr>
	<td CLASS="tbltext1"><b>Lista de Personas</font></b></td>
	<td>
          <textarea rows="4" readonly OnClick="apriRub(&quot;../Rubrica/PopUp_FormCerca.asp&quot;)" CLASS="MyTextBox" name="ElencoPersone" cols="50" maxlength="500"></textarea>
          <input Type="hidden" Name="ElencoIdUtente">
          <input Type="hidden" Name="ElencoGruppi">
	</td>
</tr>

<tr><td colspan="2">&nbsp;</td></tr>

<tr>
<td colspan="2" align="center">
	<table width="100%" border="0"><tr>

		<td align="center">
			<!--<input type="Button" OnClick="document.location=&quot;Default.asp?S=6&quot;" value="Indietro" Class="My">-->
			<a href="JavaScript:document.location=&quot;Default.asp&quot;" OnMouseOver="JavaScript:window.status=''; return true;">
				<img src="<%=Session("progetto")%>/images/indietro.gif" border="0">
			</a>
		</td>
		<td align="center">
			<!--<input type="submit" value="Conferma" Class="My"></td>-->
			<input type="image" src="<%=Session("progetto")%>/images/conferma.gif" title="Conferma">
		<td align="center">
			<!--<input type="reset" value="Annulla" Class="MyBluBianco">-->
			<a href="JavaScript:document.inserisci.reset();">
				<img src="<%=Session("progetto")%>/images/annulla.gif" border="0">
			</a>
		</td>
	</tr></table>
<tr>
<!--<td colspan="3" bgcolor="#3399CC"></td>-->
</tr>
</tr>
</table>
</form>
<!-- #include Virtual="/strutt_coda2.asp" -->
