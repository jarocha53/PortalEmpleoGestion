<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #INCLUDE FILE="Utils.asp" -->

<%

Set conn = Server.CreateObject("ADODB.Connection")
Conn.Open strconn

OraDate = "TO_DATE('" & request("gg") & "/" & request("mm") & "/" & request("aaaa") & " " & Request("Ora") & "','DD/MM/YYYY HH24.MI')"

msgInter = False or request("msgInter")
msgEmail = False or request("msgEmail")
TestoUT  = cleanXsql(request("Testo"))

Testo    = "El evento del d�a " & request("Data") & " se ha cancelado." & chr(13) & chr(10) & TestoUT

Sql = ""
Sql = Sql & "SELECT DISTINCT UTENTE.IDUTENTE, UTENTE.EMAIL "
Sql = Sql & "FROM UTENTE, PERS_AGENDA "
Sql = Sql & "WHERE "
Sql = Sql & "UTENTE.IDUTENTE = PERS_AGENDA.ID_PERSONA AND "
Sql = Sql & "PERS_AGENDA.ID_AGENDA = " & request("ID_AGENDA")
		
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
Set RsUsr = Conn.Execute (Sql)
if not RsUsr.eof then Utenti = RsUsr.getrows

SqlDel = ""
SqlDel = SqlDel & "DELETE AGENDA WHERE ID_AGENDA = " & request("ID_AGENDA")


if isArray(Utenti) then

SubjMes = " Cancelaci�n de evento en agenda..."
SubjMes = SubjMes & Request("Oggetto")

	for I = lbound(Utenti, 2) to Ubound(Utenti, 2)
		if clng(0 & Utenti(0,I)) <> IDP then
			
			if msgInter then
			
				if isEmpty(ID_COMUNICAZIONE) then
					Sql = ""
					Sql = Sql & "INSERT INTO COMUNICAZIONE "
					Sql = Sql & "(IDUTENTE, OGGETTO, TESTO_COM, DT_TMST,DT_INS_COM,DT_INVIO_COM) "
					Sql = Sql & "VALUES "
					Sql = Sql & "(" & IDP & ",'"& SubjMes & "','" & Testo & "', SYSDATE,SYSDATE,SYSDATE) "
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
					conn.Execute Sql
'PL-SQL * T-SQL  
					Set Rs = Conn.Execute ("SELECT Max(ID_COMUNICAZIONE) FROM COMUNICAZIONE WHERE IDUTENTE=" & IDP)
					ID_COMUNICAZIONE = Rs(0)
					Set Rs = Nothing			
				end if
				
				Sql = ""
				Sql = Sql & "INSERT INTO COM_PERSONA "
				Sql = Sql & "(ID_COMUNICAZIONE, IDUTENTE, DT_TMST,FL_LETTO) "
				Sql = Sql & "VALUES "
				Sql = Sql & "(" & ID_COMUNICAZIONE & ", " & Utenti(0,I) & ", SYSDATE,'N')"
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
				conn.Execute Sql
				
			end if
		end if
	next

	if msgEmail then

		Set objSession = CreateObject ("CDONTS.Session")

		'objSession.LogonSMTP "Scrivania di Progetto", "ejobplace@consiel.it"
		objSession.LogonSMTP session ("cognome")&" " & session ("nome"), session ("email")
		objSession.MessageFormat = 0
			
		Set objOutbox = objSession.GetDefaultFolder(2)
		Set objMessage =  objOutbox.Messages.Add
			
		objMessage.Subject = SubjMes
		objMessage.Text = Testo
		
		objMessage.Importance = 1
			
		for I = Lbound(Utenti, 2) to Ubound(Utenti, 2)
				objMessage.Recipients.Add Utenti(1,I), Utenti(1,I), 1
		next

		objMessage.Send
		objSession.Logoff()
	end if
end if

'PL-SQL * T-SQL  
SQLDEL = TransformPLSQLToTSQL (SQLDEL) 
Conn.Execute SqlDel

set Conn = Nothing

Response.Redirect "default.asp"
%>
