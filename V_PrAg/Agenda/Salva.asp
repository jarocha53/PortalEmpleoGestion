<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #INCLUDE FILE="Utils.asp" -->
<!-- #include virtual="/include/OpenConn.asp" -->


<%
'Set conn = Server.CreateObject("ADODB.Connection")
'Conn.Open strconn

OraDate = "TO_DATE('" & request("gg") & "/" & request("mm") & "/" & request("aaaa") & " " & Request("Ora") & "','DD/MM/YYYY HH24.MI')"

msgInter     = False or request("msgInter")
msgEmail     = False or request("msgEmail")
Oggetto      = cleanXsql(request("Oggetto"))
Testo        = cleanXsql(request("Testo"))

if request("ElencoIdUtente") <> "" then ElencoIdUtente = split(left(request("ElencoIdUtente"),len(request("ElencoIdUtente"))-2),";")
if request("ElencoGruppi") <> "" then ElencoGruppi = split(request("ElencoGruppi"),";")

	Sql = ""
	Sql = Sql & "SELECT DISTINCT UTENTE.IDUTENTE, UTENTE.EMAIL "
	Sql = Sql & "FROM UTENTE, GRUPPO "
	Sql = Sql & "WHERE "
	Sql = Sql & "UTENTE.IDGRUPPO = GRUPPO.IDGRUPPO AND ( "
	if isArray(ElencoGruppi) then
		Sql = Sql & "GRUPPO.IDGRUPPO IN ("
		for I = lbound(ElencoGruppi) to Ubound(ElencoGruppi)-2
			Sql = Sql & ElencoGruppi(I) & ","
		next
		Sql = Sql & ElencoGruppi(Ubound(ElencoGruppi)-1)
		Sql = Sql & ") "
	else
		Sql = Sql & " (1 = 0) "
	end if
	if isArray(ElencoIdUtente) then
		Sql = Sql & "OR UTENTE.IDUTENTE IN ("
		for I = lbound(ElencoIdUtente) to Ubound(ElencoIdUtente)-1
			Sql = Sql & ElencoIdUtente(I) & ","
		next
		Sql = Sql & ElencoIdUtente(Ubound(ElencoIdUtente))
		Sql = Sql & ") "
	end if
	Sql = Sql & ") "
	
	
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
	Set RsUsr = CC.Execute (Sql)
	
	if not RsUsr.eof then
		Utenti = RsUsr.getrows
		RsUsr.close
		set RsUsr=nothing
	end if
	


SqlIns = ""
SqlIns = SqlIns & "INSERT INTO AGENDA "
SqlIns = SqlIns & "(OGGETTO, TESTO, PROPRIETARIO, DATA) "
SqlIns = SqlIns & "VALUES "
SqlIns = SqlIns & "('" & cleanXSql(Request("Oggetto")) & "','" & cleanXSql(request("Testo")) & "', "
SqlIns = SqlIns & IDP & ", " & OraDate & ")"


CC.BeginTrans 
'PL-SQL * T-SQL  
SQLINS = TransformPLSQLToTSQL (SQLINS) 
	CC.Execute SqlIns
'PL-SQL * T-SQL  
	Set Rs = CC.Execute ("SELECT Max(ID_AGENDA) FROM AGENDA WHERE PROPRIETARIO=" & IDP)
	ID_AGENDA = Rs(0)
	Rs.close
	Set Rs = Nothing
	Sql = ""
	Sql = Sql & "INSERT INTO PERS_AGENDA (ID_AGENDA, ID_PERSONA, CANCELLATO, ACCETTATO, LETTO) "
	Sql = Sql & "VALUES (" & ID_AGENDA & ", " & IDP & ", 'N','S','S')"
	
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
	CC.Execute Sql
CC.CommitTrans 

if isArray(Utenti) then

SubjMes = "Novedades en la agenda:  " 
SubjMes =  SubjMes & ObjMes & cleanXSql(Request("Oggetto"))

TextMes = "...Tiene una nueva cita en la agenda del portal de empleo para el d�a : " 
TextMes = TextMes & request("gg") & "/" & mesi(request("mm")-1) & "/" & request("aaaa") & chr(13) & chr(10)  
TextMes = TextMes & cleanXSql(request("Testo")) 


	for I = lbound(Utenti, 2) to Ubound(Utenti, 2)
		if clng(0 & Utenti(0,I)) <> IDP then
			Sql = ""
			Sql = Sql & "INSERT INTO PERS_AGENDA (ID_AGENDA, ID_PERSONA, CANCELLATO, ACCETTATO, LETTO) "
			Sql = Sql & "VALUES (" & ID_AGENDA & ", " & Utenti(0,I) & ", 'N','N','N')"
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
			CC.Execute Sql
			
			if msgInter then
				if isEmpty(ID_COMUNICAZIONE) then
					Sql = ""
					Sql = Sql & "INSERT INTO COMUNICAZIONE "
					Sql = Sql & "(IDUTENTE, OGGETTO, TESTO_COM,DT_INS_COM,DT_INVIO_COM) "
					Sql = Sql & "VALUES "
					Sql = Sql & "(" & IDP & ",'" & SubjMes & "','" & TextMes &   "'"
					Sql = Sql & ",SYSDATE,SYSDATE)"
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
				CC.Execute Sql
				
'PL-SQL * T-SQL  
				Set Rs = CC.Execute ("SELECT Max(ID_COMUNICAZIONE) FROM COMUNICAZIONE WHERE IDUTENTE=" & IDP)
					ID_COMUNICAZIONE = Rs(0)
		

					Rs.close
					Set Rs = Nothing			
				end if
				
				Sql = ""
				Sql = Sql & "INSERT INTO COM_PERSONA "
				Sql = Sql & "(ID_COMUNICAZIONE, IDUTENTE, DT_TMST,FL_LETTO) "
				Sql = Sql & "VALUES "
				Sql = Sql & "(" & ID_COMUNICAZIONE & ", " & Utenti(0,I) & ", SYSDATE ,'N')"
				'Response.Write Sql
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
				CC.Execute Sql
				
			end if
		end if
	next

	if msgEmail then

		Set objSession = CreateObject ("CDONTS.Session")

		' "Scrivania di Progetto", "ejobplace@consiel.it"
		objSession.LogonSMTP session ("cognome")&" " & session ("nome"), session ("email")
		objSession.MessageFormat = 0
		Set objOutbox = objSession.GetDefaultFolder(2)
		Set objMessage =  objOutbox.Messages.Add
			
		objMessage.Subject = SubjMes
		objMessage.Text = TextMes
		objMessage.Importance = 1
			
		for I = Lbound(Utenti, 2) to Ubound(Utenti, 2)
				objMessage.Recipients.Add Utenti(1,I), Utenti(1,I), 1
		next

		objMessage.Send
		objSession.Logoff()
	end if

end if

'set Conn = Nothing
%>
<!-- #include virtual="/include/CloseConn.asp" -->


<%

Response.Redirect "default.asp"
%>
