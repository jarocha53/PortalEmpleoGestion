<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #include Virtual="/strutt_testa2.asp" -->
<!-- #INCLUDE FILE="Utils.asp" -->

	<br>
	<table border="0" width="500" CELLPADDING="0" cellspacing="0">
		<tr>
			<td align="left" class="sfondomenu" width="199">
				<span class="tbltext0">
				<b>&nbsp;AGENDA</b></span>
			</td>
			<td width="25" valign="bottom" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">
				<img border="0" src="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
			<td width="30%" valign="middle" align="right" class="tbltext1" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
		</tr>
	</table>
		
	<table border="0" width="500" CELLPADDING="0" cellspacing="0">
	    <tr>
			<td class="sfondocomm" align="left" class="tbltext1">
			Desde aqui puede organizar sus citas.
			</td>
			<td class="sfondocomm">
				<a href="Javascript:Show_Help('/Pgm/Help/ProjectSite/Agenda/default/')">
				<img src="<%=Session("Progetto")%>/images/Help.gif" border="0" align="right" alt="Help">
			</td> 
	    </tr>

		<tr height="2">
			<td class="sfondocomm" width="100%" colspan="2" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>


<br>

<%
sub Calendario (M, Y)

set conn= server.createobject("adodb.connection")
'PL-SQL * T-SQL  
STRCONN = TransformPLSQLToTSQL (STRCONN) 
conn.open StrConn

Sql = " SELECT to_number(to_char(AGENDA.Data,'DD')) Day "
Sql = Sql &  " From PERS_AGENDA, AGENDA "
Sql = Sql &  " WHERE "
Sql = Sql &  " PERS_AGENDA.CANCELLATO <> 'S' AND "
Sql = Sql &  " AGENDA.ID_AGENDA = PERS_AGENDA.ID_AGENDA AND "
Sql = Sql &  " to_char(AGENDA.Data,'MM') = '" & M & "' AND "
Sql = Sql &  " to_char(AGENDA.Data,'YYYY') = '" & Y & "' AND "
Sql = Sql &  " PERS_AGENDA.ID_PERSONA = " & IDP

Set Rs = Server.CreateObject ("ADODB.Recordset")

'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
 Rs.Open Sql,Conn,3,3,1
if not Rs.eof then
	AllDate = Rs.GetString(,,"","~","")
	AllDate = "~" & AllDate
else
	AllDate = "~"
end if

Rs.Close 
Set Rs = Nothing
Set conn = nothing

Cnorm = "#E4EBFC"
Cgior = ""
Cappu = "#E0D6A9"
Cfest = "#DEBCBD"

sfasa = weekday("1/" & monthname(M) & "/" & Y,2)

Response.Write "<table bordercolor='#578489' border=1 cellspacing=1 style='font-family: Verdana; font-size: 7pt'>"
Response.Write "<tr CLASS='sfondocomm'>"

for k = 0 to 6
	Response.Write "<td><b>" & DN(k) & "</b></td>"
next
Response.Write "</tr>"
Response.Write "<tr>"
for j = 1 to sfasa -1
	Response.Write "<td bgcolor='" & Cnorm & "'>&nbsp;</td>"
next
i = 1
col = sfasa-1
rig = 0
do
	ActDate = i & "/" & monthname(M) & "/" & Y
	if IsDate(ActDate) then
	SysData = Dateserial(Y,M,i)
		col = col + 1
		'### Definisco i colori
			bg = Cnorm
			if col > 5 then bg = Cfest
			if SysData = Date  then
				Cgior = "style='border: 2; border-style: ridge; border-color: #FF0000'"
			else
				Cgior = " "
			end if
					
		if instr(AllDate,"~"&i&"~") > 0 then
			bg = Cappu
			Response.Write "<td " & Cgior & " align=center bgcolor='" & bg & "'><a href='Impegni.asp?data=" & ActDate & "'>" & i & "</a></td>" '### Scrivo i giorni
		else
			Response.Write "<td " & Cgior & " align=center bgcolor='" & bg & "'>" & i & "</td>" '### Scrivo i giorni
		end if
		if weekday(ActDate) = 1 then
			Response.Write "</tr><tr>"
			col = 0
			rig = rig + 1
		end if
	end if
i = i + 1
loop until (i > 31) or not IsDate(ActDate)
for j = col+1 to 7
	Response.Write "<td "
	if j > 5 then 
		Response.write "bgcolor='" & Cfest & "' "
	else
		Response.write "bgcolor='" & Cnorm & "' "
	end if
	Response.Write ">&nbsp;</td>"
next
Response.Write "</tr>"

if rig < 5 then
Response.Write "<tr>"
	for k = 1 to 7 
		Response.Write "<td "
		if k > 5 then 
			Response.write "bgcolor='" & Cfest & "' "
		else
			Response.write "bgcolor='" & Cnorm & "' "
		end if
		Response.Write ">&nbsp;</td>"
	next
Response.Write "</tr>"
end if
Response.Write "</table>"
end sub
		
%>
<body bgcolor="#FFFFFF" topmargin="1">
<center>

<table width="80%" border="0">
<tr>
	<form name="Sposta" Action="Default.asp" method="POST">
	<td colspan="3" align="center">
	<%
	Mese = Request("Mese")

	If Mese = "" then Mese = Month(date)

	Response.Write "<select style='background-color: #E4EBFC; font-family: Verdana; font-size: 8pt; font-weight: bold; color: #000000' name='Mese' OnChange='this.form.submit()'>"

	for I = 1 to 12
		Response.Write "<option value='" & I & "' "
		if I = Int(Mese) then Response.Write "Selected"
		Response.Write " >" & mesi(I-1) & "</option>"
	next
	Response.Write "</select>"
	
	Anno = Request("Anno")
		
	If Anno = "" then Anno = Year(date)
	
	Response.Write "<select style='background-color: #E4EBFC; font-family: Verdana; font-size: 8pt; font-weight: bold; color: #000000' name='Anno' OnChange='this.form.submit()'>"
	for I = year(date)-10 to year(date)+10
		Response.Write "<option value='" & I & "' "
		if I = Int(Anno) then Response.Write "Selected"
		Response.Write " >" & I & "</option>"
	next
	Response.Write "</select>"
	
	
	%>
	</td>
	</form>
</tr>
<tr>
	<%
		If Mese = 1 then
			MesePre = 12
			AnnoPre = Anno -1
			if MesePre<10 then MesePre= "0" & MesePre
		else
			MesePre = Mese -1
			AnnoPre = Anno
			if MesePre<10 then MesePre= "0" & MesePre
		end if
		
		If Mese = 12 then
			MeseSuc = 1
			AnnoSuc = Anno +1
			if MeseSuc<10 then MeseSuc= "0" & MeseSuc
		else
			MeseSuc = Mese +1
			AnnoSuc = Anno
			if MeseSuc<10 then MeseSuc= "0" & MeseSuc
		end if
		if mese<10 then 
			mese="0" & mese
		end if 
	%>
	<td colspan="3" align="center"><% Calendario mese, Anno %></td>
</tr>
<tr Class="sfondocomm">
	<td align="left"><%= mesi(Int(MesePre)-1) %></td>
	<td width="65" bgcolor="#FFFFFF">&nbsp;</td>
	<td align="right"><%= mesi(Int(MeseSuc) -1) %></td>
</tr>
<tr>
	<td align="left"><% Calendario MesePre , AnnoPre %></td>
	<td width="45">&nbsp;</td>
	<td align="right"><% Calendario MeseSuc , AnnoSuc %></td>
</tr>
<tr>
<td colspan="3" align="center">
	<input type="button" value="Ingresar en la Agenda" class="My" OnClick="document.location='InsAppuntamento.asp'">
</td>
</tr>
<tr>
<td colspan="3" bgcolor="#3399CC"></td>
</tr>
</table>
</center>
<!-- #include Virtual="/strutt_coda2.asp" -->
