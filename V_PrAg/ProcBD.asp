<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include virtual="Include/Openconn.asp"-->
<!--#include file="ConversionTipos.asp"-->
<!--#include file="ProcCalendario.asp"-->

<%
Class BD

Public function DevolverHorarioDia(Fecha)

''Esta funcion toma como parametros el dia,mes,anio y tipo de dia y busca 
''los rangos horarios establecidos, si no encuentra no es un dia laborable en ppio
''en la tabla AgDiasLaborales..

	dim rs
	dim sql
	dim x 
	dim retorna 

	DiaSem	= weekday(Fecha,2)

	FechaBD = FechaABD(Fecha)	
	
	set rs = server.CreateObject("adodb.recordset")
	sql = ""

	sql = "Select HoraDesde, HoraHasta, Descripcion from AgDiasLaborales as DL, "
	sql = sql & "AgDiasSemana as DS where " & FechaBD
	sql = sql & " between VigenciaDesde and VigenciaHasta "
	sql = sql & "and DS.DiaSemana = DL.DiaSemana and DS.Descripcion ='" & ObtenerNombreDia(DiaSem) & "'"

'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
	set rs = conn.execute(sql) 


	retorna = ""
	
	if not rs.EOF then 
		do while not rs.EOF 
			for each x in rs.Fields
				if x.name <> "Descripcion" then  
					retorna = retorna & x.value & ","
				end if
			next
			retorna = left(retorna,len(retorna) - 1)
			rs.MoveNext 
			retorna = retorna & "|"
		loop
		retorna = left(retorna,len(retorna) - 1)	
	else
		retorna = ""
	end if 

	DevolverHorarioDia = retorna	

	set rs = nothing
end function


public function VerificarFeriado(Fecha)

''esta funcion permite verificar si el dia es feriado o no 
''si es feriado laborable devuelve un 1 y el rango 
''si es feriado no laborable devuelve un 0 y el rango.
''si es el dia completo devuelve solo 1 o 0

	dim rs
	dim sql
	dim x 
	dim retorna 
	
	sql = ""
	retorna = ""
	
	FechaBD = FechaABD(Fecha)	
	
	set rs = server.CreateObject("adodb.recordset")
	
	sql = "select HoraDesde, HoraHasta, Laborable from AgDiasExcepciones as DE, "
	sql = sql & "AgDiasExcepcionesTipos as DET where "
	sql = sql & "DE.Fecha = " & FechaBD & " and DET.Tipo = DE.Tipo"
	
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
	set rs = conn.execute(sql)

	
	if not rs.EOF then 
		rs.movefirst
		retorna = retorna & rs("Laborable") & "*"
		
		do while not rs.EOF 
			for each x in rs.Fields
				if x.name <> "Laborable" then
					retorna = retorna & x.value & ","
				end if
			next
			if right(retorna,1) = "," then 
				retorna = left(retorna,len(retorna) - 1)
			end if
			rs.MoveNext 
			retorna = retorna & "|"
		loop
		if right(retorna,1) = "|" then 
			retorna = left(retorna,len(retorna) - 1)
		end if
	else
		retorna = ""
	end if 
	
	VerificarFeriado = retorna 
	
	set rs = nothing

end function 


public function VerificarCitas(Fecha)

	''esta funcion permite verificar las citas agendadas para una fecha en particular..
	''si esta cancelada se considera libre el horario y no se incluye..
	

	dim rs
	dim sql
	dim x 
	dim retorna 
	
	sql = ""
	retorna = ""

	FechaBD = FechaABD(Fecha)	
	
	set rs = server.CreateObject("adodb.recordset")
	
	sql = sql & "select HoraDesde,HoraHasta from AgCitas as CT, AgCitaEstados as CE where "
	sql = sql & "Fecha = " & FechaBD & " and CT.Estado = CE.Estado and CE.CitaActiva = 1"
	
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
	set rs = conn.execute(sql)
	
	if not rs.eof then 
		do while not rs.EOF 
			for each x in rs.Fields
				retorna = retorna & x.value & ","
			next
			if right(retorna,1) = "," then 
				retorna = left(retorna,len(retorna) - 1)
			end if
			rs.MoveNext 
			retorna = retorna & "|"
		loop
		if right(retorna,1) = "|" then 
			retorna = left(retorna,len(retorna) - 1)
		end if
	else
		retorna = ""
	end if 
	
	VerificarCitas = retorna 

	set rs = nothing

end function



Public function ObtenerFranjasLibresNoVA(Fecha)
	dim C
	dim FFeriados
	
	set C = new Calendario
	
	retorna = "Horario Tabajo: " & DevolverHorarioDia(Fecha) & "<br>"  
	retorna = retorna & "Feriados: " & VerificarFeriado(Fecha) & "<br>" 
	retorna = retorna & "Citas Activas: " & VerificarCitas(Fecha)
	ObtenerFranjasLibres = retorna

	FFeriados = VerificarFeriado(Fecha)
	intFeriado = mid(FFeriados,3,len(FFeriados) -2)

	
	Response.Write EsLaborable
	
			ArrLaborable = Split(DevolverHorarioDia(Fecha),"|")
			ArrFeriado = split(intFeriado,"|")
			ArrCitas = split(VerificarCitas(Fecha),"|")
		
			cantLab = cint(ubound(ArrLaborable))
			cantFer = cint(ubound(ArrFeriado))
			cantCit = cint(ubound(ArrCitas))
			
			dim ArrHorasDesdeLab(),ArrMinutosDesdeLab(),ArrHorasHastaLab(),ArrMinutosHastaLab()

			redim ArrHorasDesdeLab(cantLab)  
			redim ArrMinutosDesdeLab(cantLab)
			redim ArrHorasHastaLab(cantLab)  
			redim ArrMinutosHastaLab(cantLab)

			dim ArrHorasDesdeFer(),ArrMinutosDesdeFer(),ArrHorasHastaFer(),ArrMinutosHastaFer()

			redim ArrHorasDesdeFer(cantFer)  
			redim ArrMinutosDesdeFer(cantFer)
			redim ArrHorasHastaFer(cantFer)  
			redim ArrMinutosHastaFer(cantFer)
			
			dim ArrHorasDesdeCit(),ArrMinutosDesdeCit(),ArrHorasHastaCit(),ArrMinutosHastaCit()

			redim ArrHorasDesdeCit(cantCit)  
			redim ArrMinutosDesdeCit(cantCit)
			redim ArrHorasHastaCit(cantCit)  
			redim ArrMinutosHastaCit(cantCit)
			
			'ej franjas: "750,810|840,1110"
			dim nombres(3)
			
			nombres(0) = "ArrLaborable"
			nombres(1) = "ArrFeriado"
			nombres(2) = "ArrCitas"
			
		for i = 0 to 2
				if nombres(i) = "ArrLaborable" then 
					ini = lbound(ArrLaborable)
					fin = ubound(ArrLaborable)
				elseif nombres(i) = "ArrFeriado" then 
					ini = lbound(ArrFeriado)
					fin = ubound(ArrFeriado)	
				elseif nombres(i) = "ArrCitas" then 
					ini = lbound(ArrCitas)
					fin = ubound(ArrCitas)		
				end if
				
			for f = ini to fin
				if nombres(i) = "ArrLaborable" then 
					ValorPrimario = C.ConvertirRangoADecimal(ArrLaborable(f),"I")
					ValorSecundario = C.ConvertirRangoADecimal(ArrLaborable(f),"D")
				elseif nombres(i) = "ArrFeriado" then 
					ValorPrimario = C.ConvertirRangoADecimal(ArrFeriado(f),"I")
					ValorSecundario = C.ConvertirRangoADecimal(ArrFeriado(f),"D")	
				elseif nombres(i) = "ArrCitas" then 
					ValorPrimario = C.ConvertirRangoADecimal(ArrCitas(f),"I")
					ValorSecundario = C.ConvertirRangoADecimal(ArrCitas(f),"D")	
				end if


				if instr(1,cstr(ValorPrimario),",") = 0 then
					HorasDesde = cstr(ValorPrimario)
					MinutosDesde = "00"
				else
					HorasDesde = C.ObtenerHoras(ValorPrimario)
					MinutosDesde = C.ObtenerMinutosTotales(ValorPrimario)
				end if
				
				ValorDesde = HorasDesde & ":" & MinutosDesde
				
				if instr(1,cstr(ValorSecundario),",") = 0 then
					HorasHasta = cstr(ValorSecundario)
					MinutosHasta = "00"
				else
					HorasHasta = C.ObtenerHoras(ValorSecundario)
					MinutosHasta = C.ObtenerMinutosTotales(ValorSecundario)
				end if
				ValorHasta = HorasHasta & ":" & MinutosHasta
				
				''cargo los arrays con las franjas horarias separadas en horas y minutos
				if nombres(i) = "ArrLaborable" then 
					ArrHorasDesdeLab(f) = cint(HorasDesde)
					ArrMinutosDesdeLab(f) = cint(MinutosDesde)
					ArrHorasHastaLab(f) = cint(HorasHasta)
					ArrMinutosHastaLab(f)= cint(MinutosHasta)
				elseif nombres(i) = "ArrFeriado" then 
					ArrHorasDesdeFer(f) = cint(HorasDesde)
					ArrMinutosDesdeFer(f) = cint(MinutosDesde)
					ArrHorasHastaFer(f) = cint(HorasHasta)
					ArrMinutosHastaFer(f)= cint(MinutosHasta)				
				elseif nombres(i) = "ArrCitas" then 
					ArrHorasDesdeCit(f) = cint(HorasDesde)
					ArrMinutosDesdeCit(f) = cint(MinutosDesde)
					ArrHorasHastaCit(f) = cint(HorasHasta)
					ArrMinutosHastaCit(f)= cint(MinutosHasta)				
				end if
			next 
		next

end function 













Public function ObtenerFranjasLibres(Fecha)
	dim C
	dim FFeriados
	
	set C = new Calendario
	
	retorna = "Horario Tabajo: " & DevolverHorarioDia(Fecha) & "<br>"  
	retorna = retorna & "Feriados: " & VerificarFeriado(Fecha) & "<br>" 
	retorna = retorna & "Citas Activas: " & VerificarCitas(Fecha)
	'ObtenerFranjasLibres = retorna

	FFeriados = VerificarFeriado(Fecha)
	intFeriado = mid(FFeriados,3,len(FFeriados) -2)
	if FFeriados <> "" then
		EsLaborable = left(FFeriados,1)
	end if 
	
			ArrLaborable = Split(DevolverHorarioDia(Fecha),"|")
			ArrFeriado = split(intFeriado,"|")
			ArrCitas = split(VerificarCitas(Fecha),"|")
		
			cantLab = cint(ubound(ArrLaborable))
			cantFer = cint(ubound(ArrFeriado))
			cantCit = cint(ubound(ArrCitas))
			
			dim ArrDesdeLab(),ArrHastaLab()

			redim ArrDesdeLab(cantLab)  
			redim ArrHastaLab(cantLab)

			dim ArrDesdeFer(),ArrHastaFer()

			redim ArrDesdeFer(cantFer)  
			redim ArrHastaFer(cantFer)
			
			dim ArrDesdeCit(),ArrHastaCit()

			redim ArrDesdeCit(cantCit)  
			redim ArrHastaCit(cantCit)
			
			'ej franjas: "750,810|840,1110"
			dim nombres(3)
			
			nombres(0) = "ArrLaborable"
			nombres(1) = "ArrFeriado"
			nombres(2) = "ArrCitas"
			
		for i = 0 to 2
				if nombres(i) = "ArrLaborable" then 
					'ini = lbound(ArrLaborable)
					fin = ubound(ArrLaborable)
				elseif nombres(i) = "ArrFeriado" then 
					'ini = lbound(ArrFeriado)
					fin = ubound(ArrFeriado)	
				elseif nombres(i) = "ArrCitas" then 
					'ini = lbound(ArrCitas)
					fin = ubound(ArrCitas)		
				end if
				
				'Response.Write ini & "," & fin & "<br>"
				
			for f = ini to fin
				if nombres(i) = "ArrLaborable" then 
					ValorPrimario = C.ObtenerMinutosTotales(C.ConvertirRangoADecimal(ArrLaborable(f),"I"))
					ValorSecundario = C.ObtenerMinutosTotales(C.ConvertirRangoADecimal(ArrLaborable(f),"D"))
				elseif nombres(i) = "ArrFeriado" then 
					ValorPrimario = C.ObtenerMinutosTotales(C.ConvertirRangoADecimal(ArrFeriado(f),"I"))
					ValorSecundario = C.ObtenerMinutosTotales(C.ConvertirRangoADecimal(ArrFeriado(f),"D"))	
				elseif nombres(i) = "ArrCitas" then 
					ValorPrimario = C.ObtenerMinutosTotales(C.ConvertirRangoADecimal(ArrCitas(f),"I"))
					ValorSecundario = C.ObtenerMinutosTotales(C.ConvertirRangoADecimal(ArrCitas(f),"D"))	
				end if

				'Response.Write ValorPrimario & "<br>"
				'Response.Write ValorSecundario & "<br>"

				''cargo los arrays con las franjas horarias separadas en horas y minutos
				if nombres(i) = "ArrLaborable" then 
					'Response.Write "Lab"
					ArrDesdeLab(f) = cint(ValorPrimario)
					'Response.Write "Desde"
					'Response.Write f
					'Response.Write ArrDesdeLab(f) & "<br>"
					ArrHastaLab(f) = cint(ValorSecundario)
					'Response.Write "Hasta"
					'Response.Write f
					'Response.Write ArrHastaLab(f)
				elseif nombres(i) = "ArrFeriado" then 
					'Response.Write "Fer"
					ArrDesdeFer(f) = cint(ValorPrimario)
					'Response.Write "Desde"
					'Response.Write f
					'Response.Write ArrDesdeFer(f) & "<br>"
					ArrHastaFer(f) = cint(ValorSecundario)				
					'Response.Write "Hasta"
					'Response.Write f
					'Response.Write ArrHastaFer(f)
				elseif nombres(i) = "ArrCitas" then 
					'Response.Write "Cit"
					ArrDesdeCit(f) = cint(ValorPrimario)
					'Response.Write "Desde"
					'Response.Write f
					'Response.Write ArrDesdeCit(f) & "<br>"
					ArrHastaCit(f) = cint(ValorSecundario)			
					'Response.Write "Hasta"
					'Response.Write f
					'Response.Write ArrHastaCit(f)
				end if
			next 
		next

		dim ArrNuevo
		
		'Response.Write EsLaborable
		for k = lbound(ArrDesdeFer) to ubound(ArrDesdeFer)
			if EsLaborable <> "" and EsLaborable = 0 then 
				for w = lbound(ArrDesdeLab) to ubound(ArrDesdeLab)
					'Response.Write ArrDesdeFer(k) & "<br>"
					verif = EsBetweenIgual(ArrDesdeLab(w),ArrDesdeFer(k),ArrHastaLab(w))
					if verif = true then 
						'Response.Write RecalcularFranjas(480,720,600,650)& "<br>"
						'exit for
						D = cstr(ArrDesdeLab(w))
						H = cstr(ArrHastaLab(w))
						DR = cstr(ArrDesdeFer(k))
						HR = cstr(ArrHastaFer(k))
	
						Nuevo = Nuevo & RecalcularFranjas(D,H,DR,HR)
						exit for
					else
						if instr(1,FInamovibles,(ArrDesdeLab(w) & "," & ArrHastaLab(w)))= 0 then 
							if instr(1,Nuevo,ArrDesdeLab(w)) = 0 then
							'Response.Write instr(1,FInamovibles,(ArrDesdeLab(u) & "," & ArrHastaLab(u)))
								FInamovibles = FInamovibles & ArrDesdeLab(w) & "," & ArrHastaLab(w) & "|"
							end if
						end if 
					end if
				next
			end if 
		next
		
		Nuevo = Nuevo & FInamovibles
				
		if right(Nuevo,1) = "|" then 
			Nuevo = left(Nuevo,(len(Nuevo)-1))
		end if 
		
		Nuevo = LimpiarFranjas(cstr(Nuevo))
		
		ArrNuevo = split(Nuevo,"|")
		
		ncant = ubound(ArrNuevo)
		
		redim ArrLaborable(ncant)
		redim ArrDesdeLab(ncant)
		redim ArrHastaLab(ncant)
		
		for p = lbound(ArrNuevo) to ubound(ArrNuevo)
			ArrLaborable(p) = ArrNuevo(p)
		next
		
		for p = lbound(ArrLaborable) to ubound(ArrLaborable)
			'Response.Write ArrLaborable(p)
			'Response.End 
			ValorPrimario = C.ObtenerMinutosTotales(C.ConvertirRangoADecimal(ArrLaborable(p),"I"))
			ValorSecundario = C.ObtenerMinutosTotales(C.ConvertirRangoADecimal(ArrLaborable(p),"D"))
			ArrDesdeLab(p) = ValorPrimario
			ArrHastaLab(p) = ValorSecundario
			'Response.Write ValorPrimario
			'Response.Write ValorSecundario
		next
		
		
		FInamovibles = ""
		
		for m = lbound(ArrDesdeCit) to ubound(ArrDesdeCit)
			for u = lbound(ArrDesdeLab) to ubound(ArrDesdeLab)
					'Response.Write ArrDesdeFer(k) & "<br>"
				verif = EsBetweenIgual(ArrDesdeLab(u),ArrDesdeCit(m),ArrHastaLab(u))
				if verif = true then 
						'Response.Write RecalcularFranjas(480,720,600,650)& "<br>"
						'exit for
					D = cstr(ArrDesdeLab(u))
					H = cstr(ArrHastaLab(u))
					DR = cstr(ArrDesdeCit(m))
					HR = cstr(ArrHastaCit(m))
	
					Response.Write D & "<br>" & H & "<br>" & DR  & "<br>" & HR  & "<br>"
					FNueva = FNueva & RecalcularFranjas(D,H,DR,HR)
					exit for
				else
					if instr(1,FInamovibles,(ArrDesdeLab(u) & "," & ArrHastaLab(u)))= 0 then 
						if instr(1,FNueva,ArrDesdeLab(u)) = 0 then
							'Response.Write instr(1,FInamovibles,(ArrDesdeLab(u) & "," & ArrHastaLab(u)))
							FInamovibles = FInamovibles & ArrDesdeLab(u) & "," & ArrHastaLab(u) & "|"
						end if
					end if 
				end if
			next
		next	
		
		FNueva = FNueva & FInamovibles
		
		if right(FNueva,1) = "|" then 
			FNueva = left(FNueva,(len(FNueva)-1))
		end if 
		
		FNueva = LimpiarFranjas(cstr(FNueva))

		ObtenerFranjasLibres = FNueva
		
end function 


Public Function LimpiarFranjas(Valor)
		
	dim Limp
	dim ArrSuc
	
	Limp = ""
	
	ArrSuc = split(Valor,"|")
	cant = ubound(ArrSuc)
	
	redim ArrLimp(cant)
	
	for f = lbound(ArrSuc) to ubound(ArrSuc)
		ValorPrimario = C.ObtenerMinutosTotales(C.ConvertirRangoADecimal(ArrSuc(f),"I"))
		ValorSecundario = C.ObtenerMinutosTotales(C.ConvertirRangoADecimal(ArrSuc(f),"D"))
		if ValorPrimario <> ValorSecundario then 
			Limp = Limp & ValorPrimario & "," & ValorSecundario & "|"
		end if 
	next

	if right(Limp,1) = "|" then 
		Limp = left(Limp,(len(Limp)-1))
	end if 

	LimpiarFranjas = Limp
end function 

Public function RecalcularFranjas(Desde,Hasta,DesdeARestar,HastaARestar)
	dim retorna
	
	retorna = ""
	
	retorna = cstr(Desde) & "," & cstr(DesdeARestar) & "|" & cstr(HastaARestar) & "," & cstr(Hasta) & "|"

	RecalcularFranjas = retorna
	
end function

public function EsBetweenIgual(Valor1,Valor2,Valor3)
	if (Valor1 <= Valor2) and (Valor2 <= Valor3) then 
		EsBetweenIgual = true 
	else
		EsBetweenIgual = false
	end if
	'Response.Write EsBetweenIgual
end function

public function EsMenorIgual(Valor1,Valor2)
	if Valor1 <= Valor2 then 
		EsMenorIgual = true 
	else
		EsMenorIgual = false
	end if
end function 

public function EsMayorIgual(Valor1,Valor2)
	if Valor1 >= Valor2 then 
		EsMayorIgual = true 
	else
		EsMayorIgual = false
	end if
end function 




private function ObtenerNombreDia(DiaAMostrar)

	'#Obtiene el nombre del dia a mostrar (en castellano)#
	
	dim y

	dim r
	for r = 0 to 7  
		if cint(DiaAMostrar) = r then 
			DiaAMostrar = cstr("0" & r) 
		else
			DiaAMostrar = cstr(DiaAMostrar)
		end if
	next

	Dias = "01|Lunes|02|Martes|03|Miercoles|04|Jueves|05|Viernes|06|Sabado|07|Domingo"

	ArrDias = split(Dias,"|")

	for y = lbound(ArrDias) to ubound(ArrDias)
		if ArrDias(y) = DiaAMostrar then 
			ObtenerNombreDia = ArrDias(y+1)
			exit for
		end if 
	next

end function 

End Class
%>
