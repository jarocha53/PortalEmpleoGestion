<!-- #INCLUDE FILE="ProcCalendario.asp" -->
<html>
	<head>
		<link rel="stylesheet" href="EstiloAgenda.css" type="text/css">
	</head>

	<body bgcolor="#FFFFFF" topmargin="1">
		<center>
		
		<table border="0" width="500" CELLPADDING="0" cellspacing="0">
			<tr>
				<td align="left" class="sfondomenu" width="199">
					<b class="tbltext0">&nbsp;AGENDA</b>
				</td>
				<td width="25" valign="bottom" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">
					<img border="0" src="<%=Session("Progetto")%>/images/tondo_linguetta.gif">
				</td>
				<td width="30%" valign="middle" align="right" class="tbltext1" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">
				</td>
			</tr>
		</table>
			
		<table border="0" width="500" CELLPADDING="0" cellspacing="0">
			<tr height="2">
				<td class="sfondocomm" width="100%" colspan="2" background="<%=Session("Progetto")%>/images/separazione.gif">
				</td>
			</tr>
		</table>

		<br>
			<%
				dim Cl
			
				set Cl = new Calendario
			%>
			
		<table width="50%" border="0">
			<tr>
				<form name="Sposta" Action="Agenda.asp" method="POST">
					<td colspan="3" align="center">
						<%
							Mes = Request("Mes")

							If Mes = "" then Mes = Month(date)

							Response.write Cl.CrearComboAgenda("Mes","EstiloComboFecha","OnChange='this.form.submit()'","Meses","",Mes)
						%>
						&nbsp;&nbsp;
						<%
							Anio = Request("Anio")
						
							If Anio = "" then Anio = Year(date)
		
							Response.write Cl.CrearComboAgenda("Anio","EstiloComboFecha","OnChange='this.form.submit()'","Anios","",Anio)
						%>
					</td>
				</form>
			</tr>
			<tr>
				<td colspan="3" align="center">
					<br>
					<% 
						Response.write Cl.ProcesarCalendario (Cl.ObtenerMes(Mes,"Act"),Anio,"01|F|05|L|06|A|16|L|21|A|25|C|27|C","01|Feriado.asp|05|Libre.asp|06|Apuntado.asp|16|Libre.asp|21|Completo.asp|25|Completo.asp|27|Completo.asp") 
					%>
				</td>
			</tr>

			<tr Class="sfondocomm">
				<td align="left">
					<b><%= mesi(Int(Cl.ObtenerMes(Mes,"Pre"))-1) %></b>
				</td>
				<td width="40" bgcolor="#FFFFFF">
					&nbsp;
				</td>
				<td align="right">	
					<b><%= mesi(Int(Cl.ObtenerMes(Mes,"Pos")) -1) %></b>
				</td>
			</tr>
			
			<tr>
				<td align="left">
					<% = Cl.ProcesarCalendario (Cl.ObtenerMes(Mes,"Pre"),Cl.ObtenerAnio(Mes,Anio,"Pre"),"01|F|05|L|06|A","01|Libre.asp|02|Apuntado.asp")%>
				</td>
				<td width="40">
					&nbsp;
				</td>
				<td align="right">
					<% = Cl.ProcesarCalendario (Cl.ObtenerMes(Mes,"Pos"),Cl.ObtenerAnio(Mes,Anio,"Pos"),"01|F|05|L|06|A","01|Libre.asp|02|Apuntado.asp")%>
				</td>
			</tr>
		</table>
	</body>	
	<%
		set Cl = nothing
	%>
</html>


