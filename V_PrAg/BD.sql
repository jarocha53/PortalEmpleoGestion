if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_AGCITAS_ESTADO_AGCITAES]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[AGCITAS] DROP CONSTRAINT FK_AGCITAS_ESTADO_AGCITAES
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_AGCITAMO_TIPOMOVIM_AGCITAMO]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[AGCITAMOVIMIENTOS] DROP CONSTRAINT FK_AGCITAMO_TIPOMOVIM_AGCITAMO
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_AGCITAMO_MOTIVO_AGCITAMO]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[AGCITAMOVIMIENTOSTIPOSMOTIVOS] DROP CONSTRAINT FK_AGCITAMO_MOTIVO_AGCITAMO
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_AGCITAS_CITATIPO_AGCITATI]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[AGCITAS] DROP CONSTRAINT FK_AGCITAS_CITATIPO_AGCITATI
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_AGCITATI_CITA_TIPO_AGCITATI]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[AGCITATIPOSPROXIMASCITAS] DROP CONSTRAINT FK_AGCITATI_CITA_TIPO_AGCITATI
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_AGCITATI_CITATIPO_AGCITATI]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[AGCITATIPOSPROXIMASCITAS] DROP CONSTRAINT FK_AGCITATI_CITATIPO_AGCITATI
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_AGDIASEX_TIPO_AGDIASEX]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[AGDIASEXCEPCIONES] DROP CONSTRAINT FK_AGDIASEX_TIPO_AGDIASEX
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_AGDIASLA_DIASEMANA_AGDIASSE]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[AGDIASLABORALES] DROP CONSTRAINT FK_AGDIASLA_DIASEMANA_AGDIASSE
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_AGDIASEX_TURNO_AGDIASTU]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[AGDIASEXCEPCIONES] DROP CONSTRAINT FK_AGDIASEX_TURNO_AGDIASTU
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_AGDIASLA_TURNO_AGDIASTU]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[AGDIASLABORALES] DROP CONSTRAINT FK_AGDIASLA_TURNO_AGDIASTU
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_AGCITAS_NROENTREV_AGENTREV]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[AGCITAS] DROP CONSTRAINT FK_AGCITAS_NROENTREV_AGENTREV
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_AGENTREV_RESULTADO_ENTREVIS]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[AGENTREVISTAS] DROP CONSTRAINT FK_AGENTREV_RESULTADO_ENTREVIS
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_AGCITAMO_REFEXTERN_REFEXTER]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[AGCITAMOVIMIENTOSTIPOS] DROP CONSTRAINT FK_AGCITAMO_REFEXTERN_REFEXTER
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[AGAGENDAESTADO]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[AGAGENDAESTADO]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[AGCITAESTADOS]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[AGCITAESTADOS]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[AGCITAMOVIMIENTOS]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[AGCITAMOVIMIENTOS]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[AGCITAMOVIMIENTOSTIPOS]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[AGCITAMOVIMIENTOSTIPOS]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[AGCITAMOVIMIENTOSTIPOSMOTIVOS]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[AGCITAMOVIMIENTOSTIPOSMOTIVOS]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[AGCITAS]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[AGCITAS]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[AGCITATIPOS]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[AGCITATIPOS]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[AGCITATIPOSPROXIMASCITAS]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[AGCITATIPOSPROXIMASCITAS]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[AGDIASEXCEPCIONES]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[AGDIASEXCEPCIONES]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[AGDIASEXCEPCIONESTIPOS]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[AGDIASEXCEPCIONESTIPOS]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[AGDIASLABORALES]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[AGDIASLABORALES]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[AGDIASSEMANA]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[AGDIASSEMANA]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[AGDIASTURNOS]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[AGDIASTURNOS]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[AGENTREVISTAS]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[AGENTREVISTAS]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[AGPUESTOS]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[AGPUESTOS]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ENTREVISTASRESULTADOS]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[ENTREVISTASRESULTADOS]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[REFEXTERNASTIPOS]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[REFEXTERNASTIPOS]
GO

CREATE TABLE [dbo].[AGAGENDAESTADO] (
	[OFICINA] [int] NOT NULL ,
	[FECHA] [datetime] NOT NULL ,
	[NROPUESTO] [int] NOT NULL ,
	[TIEMPODEATENCION] [int] NULL ,
	[TIEMPOASIGNADO] [int] NULL ,
	[TIEMPODISPMASLARGO] [int] NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[AGCITAESTADOS] (
	[ESTADO] [int] NOT NULL ,
	[DESCRIPCION] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[CITAACTIVA] [int] NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[AGCITAMOVIMIENTOS] (
	[NROMOVIMIENTO] [int] NOT NULL ,
	[OFICINA] [int] NULL ,
	[TIPOMOVIMIENTO] [int] NULL ,
	[FECHA] [datetime] NULL ,
	[CITAANTERIORNRO] [int] NULL ,
	[CITANRO] [int] NULL ,
	[CUIL] [varchar] (11) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[NROENTREVISTA] [int] NULL ,
	[MOTIVO] [int] NULL ,
	[MOTIVODESC] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[NROMOVIMIENTOASOCIADO] [int] NULL ,
	[REFEXTERNATIPO] [int] NULL ,
	[REFEXTERNANRO] [int] NULL ,
	[USUARIO] [int] NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[AGCITAMOVIMIENTOSTIPOS] (
	[TIPOMOVIMIENTO] [int] NOT NULL ,
	[DESCRIPCION] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[CITAANTERIORREQUIERE] [int] NULL ,
	[CITAANTERIORTIPO] [int] NULL ,
	[CITAANTERIORESTADO] [int] NULL ,
	[CITAANTERIORESTADOFINAL] [int] NULL ,
	[CITATIPO] [int] NULL ,
	[CITAESTADOFINAL] [int] NULL ,
	[INDICACUIL] [int] NULL ,
	[REFEXTERNATIPO] [int] NULL ,
	[INDICAENTREVISTA] [int] NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[AGCITAMOVIMIENTOSTIPOSMOTIVOS] (
	[TIPOMOVIMIENTO] [int] NOT NULL ,
	[MOTIVO] [int] NULL ,
	[DESCRIPCION] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[AGCITAS] (
	[NROCITA] [int] NOT NULL ,
	[OFICINA] [int] NOT NULL ,
	[FECHA] [datetime] NULL ,
	[HORADESDE] [int] NULL ,
	[HORAHASTA] [int] NULL ,
	[CUIL] [varchar] (11) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[CITATIPO] [int] NULL ,
	[PUESTO] [int] NULL ,
	[ESTADO] [int] NULL ,
	[ENTREVISTANRO] [int] NULL ,
	[ULTNROMOVIMIENTO] [int] NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[AGCITATIPOS] (
	[CITATIPO] [int] NOT NULL ,
	[DESCRIPCION] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[DURACION] [int] NULL ,
	[PROXIMACITADIAS] [int] NULL ,
	[INDICAENTREVISTA] [int] NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[AGCITATIPOSPROXIMASCITAS] (
	[CITATIPO] [int] NOT NULL ,
	[CITATIPOPROXIMA] [int] NOT NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[AGDIASEXCEPCIONES] (
	[OFICINA] [int] NOT NULL ,
	[FECHA] [datetime] NOT NULL ,
	[TIPO] [int] NULL ,
	[TURNO] [int] NULL ,
	[HORADESDE] [int] NOT NULL ,
	[HORAHASTA] [int] NULL ,
	[MOTIVO] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[INGRESOFECHA] [datetime] NULL ,
	[INGRESOUSUARIO] [int] NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[AGDIASEXCEPCIONESTIPOS] (
	[TIPO] [int] NOT NULL ,
	[DESCRIPCION] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[LABORABLE] [int] NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[AGDIASLABORALES] (
	[OFICINA] [int] NOT NULL ,
	[DIASEMANA] [int] NOT NULL ,
	[TURNO] [int] NULL ,
	[HORADESDE] [int] NOT NULL ,
	[HORAHASTA] [int] NULL ,
	[VIGENCIADESDE] [datetime] NULL ,
	[VIGENCIAHASTA] [datetime] NULL ,
	[INGRESOFECHA] [datetime] NULL ,
	[INGRESOUSUARIO] [int] NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[AGDIASSEMANA] (
	[DIASEMANA] [int] NOT NULL ,
	[DESCRIPCION] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[AGDIASTURNOS] (
	[TURNO] [int] NOT NULL ,
	[DESCRIPCION] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[DURACION] [int] NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[AGENTREVISTAS] (
	[ENTREVISTANRO] [int] NOT NULL ,
	[ENTREVISTADOR] [int] NULL ,
	[CUIL] [char] (11) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[CITANRO] [int] NULL ,
	[RESULTADO] [int] NULL ,
	[NOTA] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[AGPUESTOS] (
	[OFICINA] [int] NOT NULL ,
	[FECHADESDE] [datetime] NOT NULL ,
	[FECHAHASTA] [datetime] NULL ,
	[TOTALPUESTOS] [int] NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[ENTREVISTASRESULTADOS] (
	[RESULTADO] [int] NOT NULL ,
	[DESCRIPCION] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[REFEXTERNASTIPOS] (
	[REFEXTERNATIPO] [int] NOT NULL ,
	[DESCRIPCION] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[CONTADOR] [int] NULL 
) ON [PRIMARY]
GO

