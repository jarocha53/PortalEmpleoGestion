<!-- #INCLUDE FILE="ProcCalendario.asp" -->

<HTML>
<head>
	<link rel="stylesheet" href="EstiloAgenda.css" type="text/css">
</head>
<BODY>
<script language="javascript" src="FuncionesAgenda.js">
</script>
<script>
function Cambiar(Obj)
{
	document.location.href = "Libre.asp?ValorElegido=" + Obj.value + "&Data=<%=request("data")%>"
}
</script>
<div align="center">
<table width="80%" border="0" cellspacing="1">
  <tr>
    <td>
	
	<div align="center">
	<table class="EstiloTabla" width="60%" border="1" cellspacing="5">
	<form name = "frmLibre" action="SalvarLibre.asp" method="post">
      <tr>
        <td colspan="2" bgcolor="Thistle" bordercolor="#FFFFCC"><div align="center"><font size="2"><strong>Ingresar Cita</strong></font></div></td>
        </tr>
      <tr>
        <td width = "46%" bordercolor="LightYellow"><strong>Fecha</strong></td>
        <td width = "54%" bordercolor="LightYellow"><B>&nbsp;<%Response.Write request("data")%></B></td>
      </tr>
      <tr>
        <td class="EstiloFila"><strong>Persona Convocada </strong></td>
        <td class="EstiloFila">
			<input class="EstiloCasilla" name="txtPersConv" type="text" id="txtPersConv">
			<input name="txtPersConvH" type="hidden" id="txtPersConvH">
			<input class="EstiloBoton" type="button" onClick="IrA('MostrarPersonas.asp')" Value="|>|">
        </td>
      </tr>
      <tr>
        <td class="EstiloFila"><strong>Motivo</strong></td>
        <td class="EstiloFila"><input class="EstiloCasilla" name="txtMotivo" type="text" id="txtMotivo"></td>
      </tr>
      <tr>
        <td class="EstiloFila"><strong>Texto</strong></td>
        <td class="EstiloFila"><textarea class="EstiloCasilla" name="txtTexto" id="txtTexto"></textarea></td>
      </tr>
      <tr>
        <td class="EstiloFila"><strong>Desde</strong></td>
        <td class="EstiloFila"><strong></strong>
		<%
		dim cl
		set cl = new Calendario
		ValorElegido = Request("ValorElegido")
		
		response.write Cl.CrearComboAgenda ("HDesde","EstiloComboFecha","onchange='Cambiar(this)'","Horas","750,810|840,1110|1175,1210|1260,1370",ValorElegido) 

		%>
		</td>
      </tr>
      <tr>
        <td class="EstiloFila"><strong>Hasta</strong></td>
        <td class="EstiloFila">
		<%
		'response.write Cl.CrearComboAgenda ("HHasta","EstiloComboFecha","","Horas","750,810|840,1110|1175,1210|1260,1370")
		
		if ValorElegido = "" then 
			ValorElegido = Cl.ConvertirRangoAHorario("750,810","I")
		end if
			
		Valor = Cl.ObtenerRangoPosible(ValorElegido,"750,810|840,1110|1175,1210|1260,1370")
		
		response.write Cl.CrearComboAgenda ("HHasta","EstiloComboFecha","","Horas",Valor,ValorElegido)

		set cl = nothing
		%>
		</td>
      </tr>
        <tr>
        <td align="left" bordercolor="#FFFFCC"><input class="EstiloBoton" type="button" onClick="javascript:document.location.href='Agenda.asp'" value="<< Volver"></td>
        <td align="right" bordercolor="#FFFFCC"><input class="EstiloBoton" type="submit" onClick="return EvaluarRangoHora(HDesde,HHasta)" value="Enviar >>"></td>
      </tr>
	  </form>
    </table>
    </div>
	
	</td>
  </tr>
</table>
</div>
<P>&nbsp;</P>

</BODY>
</HTML>

