<%
' Funzione:	MakeGraph(aValori)
'			Genera un grafico (Istogramma orizzontale)
' Input:	un array a 3 dimensioni (Etichetta, Valore, ID CORSO), una stringa (Titolo del Grafico)
' Output: 	una stringa contenente la costruzione del grafico
' Autore:	Alessio Mign�
' Data:		15-01-2003
' Modifica:	

Function MakeGraph(aValori, sTitolo)
const nWidthGraph = 500
const nWidthLabel = 250
dim nUnit
dim sGraph
dim nScorri, nMax

	'Scorrimento di tutto il vettore per cercare il max
	nMax = -1
	for nScorri = 0 to ubound(aValori,2)
		if cint(nMax) < cint(aValori(1,nScorri)) then
		   nMax = aValori(1,nScorri)
		end if
	next
	'Proporzionalizzo il max a nWidthGraph - larghezza colonna etichetta
	if nMax = 0 then
		nUnit = 0
		nMAx = 1
	else
		nUnit = cint((cint(nWidthGraph) - cint(nWidthLabel))/ cint(nMax))
	end if 
	'Costruisco il grafico
	%>
	<table border="0" width="500" cellpadding="1" cellspacing="2">
		<tr class="sfondocomm" height="23">
			<td width=<%=nWidthLabel%>><b><%=sTitolo%></b></td>
	    </tr>
	</table>	
	<table border="0" width="500" cellpadding="1" cellspacing="2">
	<tr class="sfondocomm" height="23">
		<!-- <td width="10"><b>#</b></td> -->
		
    <td width="250"><b>Titulo del Curso</b></td>
	    
    <td width="250"><b>Grado de Terminaci&oacute;n</b></td>
	</tr>
	</TABLE>
    <table border=0 width=<%=nWidthGraph%>>
    
	<%
	for nScorri = 1 to ubound(aValori,2)
		%>
		    <tr>
		    <!--<td class="tbldett">
		    <%=nScorri%>
		    </td>-->
			<td class="tblAgg" width=<%=nWidthLabel%>>     
				<a href="javascript:VisRis(<%=aValori(2,nScorri)%>,'C')" 
				class="tblAgg">
				<%=aValori(0,nScorri)%>
				</a>
			</td>
			<td class=TextBlack width=<%=nWidthGraph - nWidthLabel%>>  
			<%
			if CInt(aValori(1,nScorri))= CInt(nMax) then 
			   nPerc1 = cdbl(cint(nMax)*100/cint(nMax))
			   colore = "/images/formazione/1.gif"  'blu
			   'colore = "2.gif"   'verde 
			else 
				nPerc = cdbl( cint(aValori(1,nScorri))*100 ) / CDbl(nMax)
				if nPerc =< 50 then
				   colore = "/images/formazione/2.gif"  'verde
				   'colore = "0.gif"   'rosso
				else
				   colore = "/images/formazione/3.gif"  'viola
				   'colore = "6.gif" 'giallo	   
				end if
			end if
			%>	  
				<img src=<%=colore%> height=15 
					width=<%=cint(aValori(1,nScorri)) * nUnit%> 
					border=0><B>&nbsp;  <%=cint(aValori(1,nScorri))%>%</B>
			</td>
		</tr>
		<%
	next
	%>	
	</table>
	<%
	MakeGraph = sGraph 
End Function
%>