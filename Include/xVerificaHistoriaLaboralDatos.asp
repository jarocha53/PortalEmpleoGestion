<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%
'====================================================================
'	**** Verificaciones Varias ****
'
'	Verificaciones sobre la Historia Laboral
'					 pertenencia a Jefes de Hogar
'					 Prerequisitos para Convenios de Adhesion
'====================================================================

public function VerificaEntrevistaACerrar(IdPersona, CUIL, OficinaID, UsuarioID, Status, Mensaje)
'=====================================================================
'	Verifica si la persona cumple para cerrar un entrevista
'
'	Si no cumple algun item se indica en los mensajes de salida
'
'Necesita:
'	IdPersona	ID de la persona en la Tabla Persona
'	CUIL		o Nro de CUIL que esta Persona.Cod_Fis
'	
'
'Devuelve:
'	Status		Codigo de Retorno 1: OK
'	Mensaje		Mensaje de Error	
'	Funcion     1: Esta todo OK 
'=====================================================================
	dim Resultado
	
	VerificaEntrevistaACerrar = 0
	
	Resultado = 0
	' Historia Laboral datos basicos
	Resultado = VerificaHistoriaLaboralDatosBAsicos(IdPersona, CUIL, OficinaID, UsuarioID, Status, Mensaje)
	
	' Estado en Jefes y proxima citacion
	if Resultado = 1 then
		Resultado = VerificaPersonaEstado(CUIL, -10, Status, Mensaje)
	end if
	
	'--------------------------------------------
	' Salida
	'--------------------------------------------
	if Resultado = 1 then
		VerificaEntrevistaACerrar = 1	
	end if
	STATUS = STATUS
	Mensaje = Mensaje

end function


public function VerificaSeguroPrecondiciones(IdPersona, CUIL, OficinaID, UsuarioID, Status, Mensaje)
'=====================================================================
'	Verifica si la persona cumple con los prerequisitos del seguro
'
'	Si no cumple algun item se indica en los mensajes de salida
'
'Necesita:
'	IdPersona	ID de la persona en la Tabla Persona
'	CUIL		o Nro de CUIL que esta Persona.Cod_Fis
'	
'
'Devuelve:
'	Status		Codigo de Retorno 1: OK
'	Mensaje		Mensaje de Error	
'	Funcion     1: Esta todo OK 
'=====================================================================
	dim Resultado
	
	VerificaSeguroPrecondiciones = 0
	
	Resultado = 0
	' Historia Laboral datos basicos
	Resultado = VerificaHistoriaLaboralDatosBAsicos(IdPersona, CUIL, OficinaID, UsuarioID, Status, Mensaje)
	
	' Estado en Jefes y proxima citacion
	if Resultado = 1 then
		Resultado = VerificaPersonaEstado(CUIL, 1, Status, Mensaje)
	end if
	
	'--------------------------------------------
	' Salida
	'--------------------------------------------
	if Resultado = 1 then
		VerificaSeguroPrecondiciones = 1	
	end if
	STATUS = STATUS
	Mensaje = Mensaje

end function

public function VerificaHistoriaLaboralDatosBAsicos(IdPersona, CUIL, OficinaID, UsuarioID, Status, Mensaje)
'=====================================================================
'	Verifica si estan cargados los datos Basicos de una Historia Laboral
'=====================================================================
'	Si no cumple algun item se indica en los mensajes de salida
'
'Necesita:
'	IdPersona	ID de la persona en la Tabla Persona
'	CUIL		o Nro de CUIL que esta Persona.Cod_Fis
'	
'
'Devuelve:
'	Status		Codigo de Retorno 1: OK
'	Mensaje		Mensaje de Error	
'	Funcion     1: Esta todo OK 
'=====================================================================
	dim sSQL, sCUIL, sDireccion, sxIdPersona, sEstOcupacional, sNivelEstudio, sPostulaciones
		
	VerificaHistoriaLaboralDatosBAsicos = 0
	
	sCUIL = "": sDireccion = "": sxIdPersona = 0: sEstOcupacional = 0: sNivelEstudio = 0: sPostulaciones = 0
	Set rsBusca = Server.CreateObject("adodb.recordset")

	Status = 1	
	'-------------------------------------------
	'	Datos personales necesarios - ORACLE
	'-------------------------------------------
	if Status = 1 then
		' Cuil y Direccion base
		sSQL = "SELECT COD_FISC, Ind_Res FROM Persona WHERE ID_PErsona =" & IdPersona
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
		set rsBusca = cc.execute (sSQL)
		if not rsBusca.eof then
			sCUIL = rsBusca("COD_FISC")
			sDireccion = rsBusca("Ind_Res")
		end if
		rsBusca.close
		
		if sCUIL = "" then
			Status = 200
			Mensaje	= "Persona NO valida"
		elseif sDireccion = "" then
			Status = 210
			Mensaje	= "La direcci�n de la Persona No est� definida"
		end if
	end if
	
	'-------------------------------------------
	'	Estado ocupacional
	'-------------------------------------------
	if Status = 1 then
		sxIdPersona = 0
		sSQL = "SELECT	Id_Persona	FROM Stato_Occupazionale WHERE ID_Persona = " & IdPersona & " and Ind_Status = 0 and ID_CIMPIEGO = "  & OficinaID
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
		set rsBusca = cc.execute (sSQL)
		if not rsBusca.eof then
			sxIdPersona = rsBusca.Fields(0).Value
		end if
		rsBusca.close
		
		sxIdPersona = clng(sxIdPersona) + 0
		if clng(sxIdPersona) = 0 then
			Status = 220
			Mensaje	= "La persona NO tiene Situaci�n Laboral definida"
		end if
	end if
	
	'-------------------------------------------
	'	Nivel de Estudio
	'-------------------------------------------
	if Status = 1 then
		sEstOcupacional = 0
		sSQL = "SELECT	Count(*) FROM TISTUD WHERE ID_Persona = " & IdPersona 
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
		set rsBusca = cc.execute (sSQL)
		if not rsBusca.eof then
			sEstOcupacional = rsBusca.Fields(0).Value
		end if
		rsBusca.close

		sEstOcupacional = cint(sEstOcupacional) + 0
		if cint(sEstOcupacional) = 0 then
			Status = 230
			Mensaje	= "La persona NO tiene Nivel de Estudios definidos"
		end if
	end if
	
	'-------------------------------------------
	'	Postulaciones
	'-------------------------------------------
	if Status = 1 then
		sPostulaciones = 0
		sSQL = "SELECT Count(*) FROM Pers_figProf WHERE ID_Persona = " & IdPersona & " and INCROCIO='S' and IND_STATUS= 0"
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
		set rsBusca = cc.execute (sSQL)
		if not rsBusca.eof then
			sPostulaciones = rsBusca.Fields(0).Value
		end if
		rsBusca.close
		
		sPostulaciones = cint(sPostulaciones) + 0
		if cint(sPostulaciones) = 0 then
			Status = 240
			Mensaje	= "La persona NO tiene Postulaciones ingresadas"
		end if
	end if
	
	set rsBusca = nothing 

	'--------------------------------------------
	' Salida
	'--------------------------------------------
	if STATUS = 1 then
		VerificaHistoriaLaboralDatosBAsicos = 1	
	end if
	STATUS = STATUS
	Mensaje = Mensaje

end function

		

public function VerificaPersonaEstado(CUIL, LIQUIPLAN, Status, Mensaje)
'=====================================================================
'	Verifica el estado de la persona En EmpleoPersonas y cita proxima 
'
'	Si no cumple algun item se indica en los mensajes de salida
'
'Necesita:
'	CUIL		Nro de CUIL que esta Persona.Cod_Fis
'	LIQUIPLAN	Nro de liquipan a observar 
'
'Devuelve:
'	Status		Codigo de Retorno 1: OK
'	Mensaje		Mensaje de Error	
'	Funcion     1: Esta todo OK 
'=====================================================================
	VerificaPersonaEstado = 0
	Status = 0

	'-------------------------------------------
	'	Es Beneficiario de Jefes de Hogar - SQL
	'	Cita proxima conveniada	- SQL
	'-------------------------------------------
	Status = 0
	Set cmdVerificar = Server.CreateObject("adodb.command")
	with cmdVerificar
		.activeconnection = connlavoro
		.Commandtext = "SeguroConvenioControlRequisitos"
		.commandtype = adCmdStoredProc 
		'--------------------------------------------
		' CArga parametros
		'--------------------------------------------
		For Each Parameter In .Parameters
			If Parameter.Direction And adParamInput Then
				Select case ucase(Mid(Parameter.Name, 2))
					CASE "CUIL"
						Parameter.value = CUIL
					CASE "LIQUIPLAN"
						Parameter.value = LIQUIPLAN
							
					CASE "STATUS"
						if len(Status) = 0 then 
							Status = 0
						end if
						Parameter.value = Status
					CASE "MENSAJE"
						Parameter.value = "" & Mensaje	
					CASE ELSE
						' ????
				END SELECT
			End If
		Next
		' Ejecuta
'PL-SQL * T-SQL  
		.Execute 0, , adexecutenorecords
		'--------------------------------------------
		' Devuelve parametros
		'--------------------------------------------
		For Each Parameter In .Parameters
			If Parameter.Direction And (adParamOutput Or adParamReturnValue) Then
				Select case ucase(Mid(Parameter.Name, 2))
					case "STATUS"
						STATUS = trim(Parameter.value)
					case "MENSAJE"
						MENSAJE = trim(Parameter.value)
					case else
						' ???
				end select
			End If
		Next
	end with
	' cierra objetos	
	set cmdVerificar = nothing
	
	'--------------------------------------------
	' Salida
	'--------------------------------------------
	if STATUS = 1 then
		VerificaPersonaEstado = 1	
	end if
	STATUS = STATUS
	Mensaje = Mensaje

end function
		
		
%>
