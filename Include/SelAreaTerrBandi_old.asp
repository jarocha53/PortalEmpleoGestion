<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%
'************************************************************************
'La funzione crea una combo che visualizza i bandi disponibili per 
'una Unit� Organizzativa
'*************************************************************************
function CreateBandi(sStringa)
dim sSql
dim sValoriArray
	sValoriArray = Split(sStringa, "|")

'	Struttura variabile [ sValoriArray ]:
'		sValoriArray(0) = Unit� organizzativa
'		sValoriArray(1) = Valore da selezionare
'		sValoriArray(2) = Nome del Combo
'		sValoriArray(3) = Azione

if trim (sValoriArray(0)) = "" or trim(sValoriArray(0)) = "0" then
	Response.Write "Non � disponibile nessuna unit� organizzativa"
	CreateBandi = "ERRORE"
	exit function
end if 



sSql="SELECT ID_BANDO,DESC_BANDO FROM BANDO A WHERE EXISTS" &_
     "(SELECT ID_BANDO FROM AREA_BANDO B WHERE A.ID_BANDO=B.ID_BANDO AND ID_UORG = " & sValoriArray(0) & ")" &_
     " ORDER BY DESC_BANDO"

if sValoriArray(1)= "" then
   sValoriArray(1)= 0
end if

'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
set rsBando = cc.execute(sSql)

if not rsBando.eof then
    Response.Write "<SELECT ID='" & sValoriArray(2) & "' class=textblack" &_
          " name='" & sValoriArray(2) & "'" & sValoriArray(3) &_
          "><OPTION value=></OPTION>"'

    do until rsBando.eof
       if clng(sValoriArray(1)) = clng(rsBando("ID_BANDO")) then
           sel="selected"
       else
           sel = ""
       end if    
       Response.Write "<option value='" & rsBando("ID_BANDO") & "' " & sel &">" &_
       rsBando("DESC_BANDO") & "</option>"
       rsBando.movenext
    loop

    Response.Write "</select>"
else
    Response.Write "<input type=hidden name=" & sValoriArray(2)& " value=''>"
	CreateBandi = "ERRORE"
	exit function
end if

set rsBando = nothing

end function

'************************************************************************
'La funzione crea una combo che visualizza i bandi disponibili per 
'una Unit� Organizzativa (condizionabile)
'*************************************************************************
function CreateBandiSel(sStringa)
dim sSql
dim sValoriArray
	sValoriArray = Split(sStringa, "|")

'	Struttura variabile [ sValoriArray ]:
'		sValoriArray(0) = Unit� organizzativa
'		sValoriArray(1) = Valore da selezionare
'		sValoriArray(2) = Nome del Combo
'		sValoriArray(3) = Eventuale Condizione Filtro/Ordinamento
'		sValoriArray(4) = Azione

if trim (sValoriArray(0)) = "" or trim(sValoriArray(0)) = "0" then
	Response.Write "Non � disponibile nessuna unit� organizzativa"
	CreateBandiSel = "ERRORE"
	exit function
end if 



sSql="SELECT ID_BANDO,DESC_BANDO FROM BANDO A WHERE EXISTS" &_
     "(SELECT ID_BANDO FROM AREA_BANDO B WHERE A.ID_BANDO=B.ID_BANDO AND ID_UORG = " & sValoriArray(0) & ")"

if sValoriArray(3)="" then
	sFiltro=" ORDER BY DESC_BANDO"
else
    sFiltro=" " & sValoriArray(3)
end if

sSql=sSql & sFiltro

if sValoriArray(1)= "" then
   sValoriArray(1)= 0
end if

'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
set rsBando = cc.execute(sSql)

if not rsBando.eof then
    Response.Write "<SELECT ID='" & sValoriArray(2) & "' class=textblack" &_
          " name='" & sValoriArray(2) & "'" & sValoriArray(4) &_
          "><OPTION value=></OPTION>"'

    do until rsBando.eof
       if clng(sValoriArray(1)) = clng(rsBando("ID_BANDO")) then
           sel="selected"
       else
           sel = ""
       end if    
       Response.Write "<option value='" & rsBando("ID_BANDO") & "' " & sel &">" &_
       rsBando("DESC_BANDO") & "</option>"
       rsBando.movenext
    loop

    Response.Write "</select>"
else
    Response.Write "Non ci sono bandi disponibili"
	CreateBandiSel = "ERRORE"
	exit function
end if

set rsBando = nothing

end function

'---------------------------------------------------------------------
'Questa funzione permette di reperire tutte le province che sono 
'associate ad un area territoriale in formato stringa. Per ottenere 
'tale informazione dovr� essere passato l'identificativo dell'area
'che interessa. Restituisce una stringa vuota in caso che non ci siano
'province associate all'area territoriale indicata. 
'---------------------------------------------------------------------
function SetProvAreaTerr(idArea)
    dim sProvArea
    dim sSQL
    
    sSQL="SELECT PRV FROM DETT_AREA WHERE ID_AREATERR=" & idArea &_
         " ORDER BY PRV" 
    
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
    set rsSetArea =cc.execute(sSQL)
    if not rsSetArea.eof then
        
        do while not rsSetArea.eof 
	        sProvArea = sProvArea & rsSetArea("PRV")
	        rsSetArea.movenext
		loop   
	   
	else
	    sProvArea=""	
    end if 
    set rsSetArea= nothing 
    SetProvAreaTerr = sProvArea     
end function

'---------------------------------------------------------------------
'Questa funzione permette di reperire tutte le province che sono  
'associate alle aree territoriali appartenenti alla stessa unita 
'organizzativa�in formato stringa. Per ottenere tale informazione 
'dovr� essere passato l'identificativo dell'unit� organizzativa che  
'interessa. Restituisce una stringa vuota in caso che non ci siano 
'province associate all'unit� organizzativa indicata. 
'---------------------------------------------------------------------

function SetProvUorg(idUorg)
    dim sProv
    dim sSQL
    
    sSQL="SELECT distinct(PRV) FROM DETT_AREA WHERE ID_AREATERR IN " &_
         "(SELECT ID_AREATERR FROM AREA_BANDO WHERE " &_ 
         "ID_UORG = " & idUorg & " GROUP BY ID_AREATERR)" &_
         " ORDER BY PRV" 
         
            
    'Response.Write  ssql
    'Response.End 
    
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
    set rsSetArea =cc.execute(sSQL)
    if not rsSetArea.eof then
        
        'do while not rsSetArea.eof 
	     '   sProv = sProv & rsSetArea("PRV")
	      '  rsSetArea.movenext
		'loop   
	    '''''''''''
			rsSetArea.movefirst
        do while not rsSetArea.eof 
        
	        sProv = sProv & "," & rsSetArea("PRV")
	        
	        rsSetArea.movenext
		loop 
		if right(sProv,1) = "," then 
			sProv = left(sProv,len(sProv)-1)
		end if 
		if left(sProv,1) = "," then 
			sProv = mid(sProv,2,len(sProv))
		end if 
		'''''''''''
		'Response.Write sProv
	else
	    sProv=""	
    end if 
    set rsSetArea= nothing 
    SetProvUorg = sProv    
end function

'---------------------------------------------------------------------
'Questa funzione permette di reperire tutte le province che sono  
'associate ad un bando appartenente ad una unita'organizzativa�in 
'formato stringa. Per ottenere tale informazione dovr� essere passato
'l'identificativo dell'unit� organizzativa e l'identificativo del bando 
'che interessa. Restituisce una stringa vuota in caso che non ci siano
'province associate all'unit� organizzativa e bando indicati. 
'---------------------------------------------------------------------
function SetProvBando(idBando,idUorg)
    dim sProv
    dim sSQL
    
    sSQL="SELECT distinct(PRV) FROM DETT_AREA WHERE ID_AREATERR IN " &_
         "(SELECT ID_AREATERR FROM AREA_BANDO WHERE " &_ 
         "ID_BANDO = " & idBando & " AND " &_
         "ID_UORG = " & idUorg & " GROUP BY ID_AREATERR)" &_
         " ORDER BY PRV" 
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
    set rsSetArea =cc.execute(sSQL)
    if not rsSetArea.eof then
        
        do while not rsSetArea.eof 
	        sProv = sProv & rsSetArea("PRV")
	        rsSetArea.movenext
		loop   
	   
	else
	    sProv=""	
    end if 
    set rsSetArea= nothing 
    SetProvbando = sProv 
end function 

' *******************************************************************
' function DecBando(Long) 
'			Decodifica il bando. Accetta in INPUT l'identificativo.
'			del bando.
' Return 	la descrizione del bando 
' Autore	EPI 
' Date		24/02/2003			
' Update	EPI 24/02/2003
function DecBando(nIdBando)
	dim sSQLDecBando,rsDecBando
	
	sSQLDecBando = "SELECT DESC_BANDO FROM BANDO " &_
		"WHERE ID_BANDO = " & CLng(nIdBando)
'PL-SQL * T-SQL  
SSQLDECBANDO = TransformPLSQLToTSQL (SSQLDECBANDO) 
	set rsDecBando = CC.Execute(sSQLDecBando)
	if not rsDecBando.eof then
		DecBando = rsDecBando("DESC_BANDO")
	else
		DecBando = ""
	end if
	rsDecBando.close
	set rsDecBando = nothing 

end function
' **********************************************************************
' function DecFaseBando(Long,String)
'			Decodifica la sessione di un dato Bando. 
'			Accetta in INPUT l'identificativo del bando e della sessione.
'			del bando.
' Return 	la descrizione della sessione 
' Autore	EPI
' Date		24/02/2003			
' Update	EPI 24/02/2003
function DecFaseBando(nIdBando,sCodSessione)
	dim sSQLDecBando,rsDecBando
	
	sSQLDecFaseBando = "SELECT DESC_FASE FROM FASE_BANDO " &_
		"WHERE ID_BANDO = " & CLng(nIdBando) &_
		" AND COD_SESSIONE='" & sCodSessione & "'"	
'PL-SQL * T-SQL  
SSQLDECFASEBANDO = TransformPLSQLToTSQL (SSQLDECFASEBANDO) 
	set rsDecFaseBando = CC.Execute(sSQLDecFaseBando)
	if not rsDecFaseBando.eof then
		DecFaseBando = rsDecFaseBando("DESC_FASE")
	else
		DecFaseBando = ""
	end if
	rsDecFaseBando.close
	set rsDecFaseBando = nothing 

end function
' *****************************************************
function setIdBando(sIdUorg)
	dim appoBando
	dim sSQL
	
	sSQL="SELECT ID_BANDO FROM BANDO A WHERE EXISTS" &_
     "(SELECT ID_BANDO FROM AREA_BANDO B WHERE A.ID_BANDO=B.ID_BANDO AND ID_UORG = " & sIdUorg &_
     ") ORDER BY DESC_BANDO"

'PL-SQL * T-SQL  
'SSQL = TransformPLSQLToTSQL (SSQL) 
	set rsBando = cc.execute(sSql)

	if not rsBando.eof then
		do while not rsBando.eof
			appoBando = appoBando & rsBando("ID_BANDO") & ","
		rsBando.movenext
		loop
		appoBando = mid(appoBando,1,len(appoBando)- 1)
	else
		appoBando = ""	
	end if

	set rsBando = nothing
	
	setIdBando = appoBando
	
end function
 %>
