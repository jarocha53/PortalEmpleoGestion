<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%
'
' Restituisce la concatenazione di:
'		Descrizione e Cod.Ateco
' per il settore (idSett) richiesto.
'
function DecCodSettori(idSett)

	dim rsRec
	dim sSQL
		
	sSQL = "SELECT Denominazione, cod_Ateco FROM SETTORI"&_
		   " WHERE ID_SETTORE = " & idSett

	if IsObject(CX) then
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
		set rsRec = CX.Execute(sSQL)
	else
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
		set rsRec = CC.Execute(sSQL)
	end if 

	if not rsRec.Eof then
		DecCodSettori=rsRec.Fields("Denominazione") & "|" & rsRec.Fields("cod_Ateco")
		rsRec.Close
	else
		DecCodSettori="|"
	end if

	set rsRec = Nothing

end function
'
' Restituisce la descrizione del Primo Livello del Codice Ateco (cod_Ateco) richiesto.
'
function DecCodAteco(cod_Ateco)
	DecCodAteco=" "
	select case mid(cod_Ateco,1,1)
		case "A"
			DecCodAteco= "Agricoltura, caccia e silvicoltura"
		case "B" 
			DecCodAteco= "Pesca, piscicoltura e servizi connessi"
		case "C"	
			DecCodAteco= "Estrazione di minerali"
		case "D"	
			DecCodAteco= "Attivit� manufattiere"
		case "E"	
			DecCodAteco= "Produzione e distribuzione di energia elettrica, gas e acqua"
		case "F"	
			DecCodAteco= "Costruzioni"
		case "G"	
			DecCodAteco= "Commercio all' ingrosso e al dettaglio; riparazioni di autoveicoli, motocicli e di beni personali per la casa"
		case "H"	
			DecCodAteco= "Alberghi e Ristoranti"
		case "I"	
			DecCodAteco= "Trasporti, magazzinaggio e comunicazioni"
		case "J"	
			DecCodAteco= "Attivit� finanziarie"
		case "K"	
			DecCodAteco= "Attivit� immobiliari, noleggio, informatica, ricerca, servizi alle imprese"
		case "L"	
			DecCodAteco= "Amministrazione pubblica"
		case "M"	
			DecCodAteco= "Istruzione"
		case "N"	
			DecCodAteco= "Sanit� e assistenza sociale"
		case "O"	
			DecCodAteco= "Altri servizi pubblici, sociali e personali"	
		case "P"	
			DecCodAteco= "Attivit� svolte da famiglie e convivenze"
		case "Q"	
			DecCodAteco= "Organizzazione ed organismi extraterritoriali"		
	end select 

end function
%>
