<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%

' Controllo per poter vedere i dati personali
' delle persone iscritte al CPI della provincia 
' del CPI che effettua la selezione.
 
function CheckProvinciale(nIdCimpiegoUteIscr,sPRVCPI)
	dim sNomeFile,sCondizione,sql

	sql = sql & "SELECT ID_SEDE FROM SEDE_IMPRESA "
	sql = sql & "WHERE PRV ='" & sPRVCPI & "' "								
	sql = sql & " AND ID_SEDE = " & clng(nIdCimpiegoUteIscr)

'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
	set rsCheckProv = cc.Execute(sql)
	if rsCheckProv.eof then
		CheckProvinciale = 0
	else
		CheckProvinciale = 1
	end if
	rsCheckProv.close
	set rsCheckProv = nothing

end function

function CheckAreaTerr()
	
	dim sValore,rsCheck,sSQL
	dim aProgetto
	
	aProgetto = decodTadesToArray("CPROJ",date," CODICE = '" &_
		Right(Session("Progetto"), (Len (Session("Progetto")) - 1 ) ) & "'" ,0,0)
	 

	 
	if aProgetto(0,0,0) <> "" then

		sValore = aProgetto(0,1,1)
		
		if len (sValore) < 2 then

			CheckAreaTerr = false		

		else
			iCheck = InStr(2,sValore,"S") 
			if iCheck = 0 then
				CheckAreaTerr = false
			else
				CheckAreaTerr = true 
			end if 
		end if
	else
		CheckAreaTerr = false
		
	end if
	
	Erase aProgetto

end function

' ****************************************************************
' Tale funzione controlla se all'interno della cartella 
' CONF esista la il file che contiene il tipo di abilitazione.
' La funzione ritorna 0 oppure 1 se si � abilitati, altrimenti "X".
' 0 --> Ricerca le persone del proprio CPI;
' 1 --> Ricerca le persone su tutto il territorio.
' ****************************************************************

function FiltroCPI(bAbil)
	dim sConfCPI
	dim oFileSystemObject, oFileCurriculum
	
	sConfCPI = SERVER.MapPath("\") & session("Progetto") &_
	 "\Conf\" & Session("idutente") & "_AmbitoTerr.txt"
	
	set oFileSystemObject = Server.CreateObject("Scripting.FileSystemObject")
	
	if oFileSystemObject.FileExists(sConfCPI) then

		set oFileCurriculum = oFileSystemObject.OpenTextFile(sConfCPI,1)

		if CheckAreaTerr then
			FiltroCPI		= oFileCurriculum.ReadLine()
		else
			FiltroCPI = "X"
		end if
		oFileCurriculum.close
		set oFileCurriculum = nothing	

	else
		if bAbil = true then
		
				set oFileCurriculum = oFileSystemObject.CreateTextFile(sConfCPI)
	
			oFileCurriculum.WriteLine("0")
			oFileCurriculum.close
			FiltroCPI = "0"
		else
			FiltroCPI		= "X"
		end if
	end if

	set oFileSystemObject = nothing
	

end function


' Funzione che mi costruisce la stringa SQL con le regole.
' Prende come INPUT l'id_richiesta sulla quale prendere 
' eventuali filtri e l'ID della connessione.

function CreaCriteri(nIdRic,CC)
	Dim aOpLog	
	Dim memProgr

	sql1	= ""

	sql1 = "select a.ID_TAB, a.ID_CAMPO, a.COD_OPLOG, a.VALORE, a.PRG_REGOLA, " &_
		" a.PRG_REGOLA_RIF, a.DT_TMST, b.Tipo_campo " &_
		" from RICHIESTA_REGOLE a, DIZ_DATI b " &_
		" Where a.id_richiesta=" & clng(nIdRic) &_
		" And a.Id_tab = b.Id_tab " &_
		" And a.Id_campo = b.ID_Campo " &_
		" Order by a.ID_TAB, a.PRG_REGOLA"

	set RR1 = server.CreateObject("ADODB.recordset")
	set RRCero = server.CreateObject("ADODB.recordset")
	
	'Response.Write sql1
	'response.write "<br><br><br>" & sql1	
	'PL-SQL * T-SQL  
	SQL1 = TransformPLSQLToTSQL (SQL1) 
	RR1.Open sql1,CC,1
	sql = ""
	ban = 0
	if not RR1.EOF then

		aOpLog = decodTadesToArray("OPLOG",Date(),"",1,0)		
		memProgr = 0
		esCero = ""
		corrida = 0

		do until RR1.EOF
			
			'Guarda los parametros del filtro de experiencia laboral para incluir la funci�n que lo calcula
			corrida = corrida + 1
			tabla = RR1.Fields("ID_TAB")
			if tabla = "ESPRO" then
				select case corrida
					case 1
						valor1 = Trasforma(RR1.Fields("VALORE"), RR1.Fields("Tipo_campo"), searchIntoArray(RR1.Fields("COD_OPLOG"),aOpLog,1))
					case 2 
						valor2 = Trasforma(RR1.Fields("VALORE"), RR1.Fields("Tipo_campo"), searchIntoArray(RR1.Fields("COD_OPLOG"),aOpLog,1))
						signo = searchIntoArray(RR1.Fields("COD_OPLOG"),aOpLog,1)
				end select 
			end if
			'Fin del procesamiento de los valores

			if tabla <> "ESPRO" then

				sqlCero	= ""
				if RR1.Fields("ID_TAB") = "ESPRO" then
					sqlCero = "SELECT * FROM richiesta_regole WHERE id_richiesta ='" & clng(nIdRic) &_
						"' AND id_tab = 'ESPRO' AND cod_oplog = '01' AND valore ='0' "
					
					'PL-SQL * T-SQL  
					SQLCERO = TransformPLSQLToTSQL (SQLCERO) 
					'response.write SQLCERO
					
					RRCero.Open sqlCero,CC,1
					if RRCero.recordcount > 0 then
						esCero = "not "
					End if
					RRCero.close
				End if	


				if clng(RR1.Fields("PRG_REGOLA_RIF")) <> 0 and clng(RR1.Fields("PRG_REGOLA_RIF")) = memProgr then
					if esCero = "not " then
						esCero = ""
					Else
						'if signo = "<=" then
						'  signo = "me"
						'else
						'  signo = "ma"
						'end if	
						sql = sql & " and  " & RR1.Fields("ID_CAMPO") &_
							 " " & searchIntoArray(RR1.Fields("COD_OPLOG"),aOpLog,1) & Trasforma(RR1.Fields("VALORE"), RR1.Fields("Tipo_campo"), searchIntoArray(RR1.Fields("COD_OPLOG"),aOpLog,1)) 
						'Si es el filtro de experiencia laboral incluye la pregunta con la funci�n de calculo	 
						'if tabla = "ESPRO" then
						'	sql = sql & " or exists (select dbo.f_sumaexperiencia(p.id_persona,"&valor1&","&valor2&",'"&signo&"')) "
						'end if

						sql = sql & "  And id_persona = P.id_persona "
							 
							 
					End if
				else
					sql = sql & " and " & esCero & "exists(select id_persona from " & RR1.Fields("ID_TAB") &_
						" Where " & RR1.Fields("ID_CAMPO") &_
						" " & searchIntoArray(RR1.Fields("COD_OPLOG"),aOpLog,1) & " " &_
						Trasforma(RR1.Fields("VALORE"), RR1.Fields("Tipo_campo"), searchIntoArray(RR1.Fields("COD_OPLOG"),aOpLog,1)) &_
						" And id_persona = P.id_persona" 
						tempManzione = RR1.Fields("VALORE")
					' EPILI 17/06/2001
					select case RR1.Fields("ID_TAB")	 
						case "PERS_CONOSC","PERS_CAPAC","PERS_DISPON","PERS_COMPOR","STATO_OCCUPAZIONALE","PERS_VINCOLI"
							sql = sql & " and IND_STATUS ='0'"
						case "TISTUD"
							'Damian quita esta condicion el 15/07/2008
							'sql = sql & " and COD_STAT_STUD ='0'"
					end select

					memProgr = clng(RR1.Fields("PRG_REGOLA"))
				end if
			else
			  if corrida = 2 then
                  sql = sql & "and exists (select id_persona "&_
                "from (select id_persona, abs(meses/12) meses from ("&_
                "    select id_persona,sum(( ABS(FLOOR(DATEDIFF(MM,[dbo].[NVL](DT_FIN_ESP_PRO,[dbo].[SYSDATE]()), "&_
                    "DT_INI_ESP_PRO))))) as meses "&_
                          "from ESPRO  "&_
                           "Where COD_MANSIONE = "&valor1&" "&_
                            " And id_persona =  p.id_persona  "&_
                          "group by id_persona  ) as yy ) as tt "&_
                "where meses "&signo&" "&valor2&"   "
              end if 
			end if
			
			'Response.Write "<br><br><br>sql: " & sql & "<br>"
			RR1.MoveNext

			if not RR1.EOF then
				if clng(RR1.Fields("PRG_REGOLA_RIF")) = 0 then 	sql = sql & ")"
			end if
			
		loop
		sql = sql & ")"
		Erase aOpLog
	end if
'	Response.Write "<BR>Errore : " & Err.number & "<BR>"
	RR1.Close
	set RR1 = nothing
	sql1	= ""

	'Response.Write sql
	'Response.End 
	CreaCriteri = sql
	


end function
'**********************************************************************************************************************************************************************************	

function CreaCriteriStage(nIdRic,CC)
	Dim aOpLog	
	Dim memProgr

	sql1	= ""

	sql1 = "select a.ID_TAB, a.ID_CAMPO, a.COD_OPLOG, a.VALORE, a.PRG_REGOLA, " &_
		" a.PRG_REGOLA_RIF, a.DT_TMST, b.Tipo_campo " &_
		" from REGOLE_STAGE a, DIZ_DATI b " &_
		" Where a.id_richstage=" & clng(nIdRic) &_
		" And a.Id_tab = b.Id_tab " &_
		" And a.Id_campo = b.ID_Campo " &_
		" Order by a.ID_TAB, a.PRG_REGOLA"

	set RR1 = server.CreateObject("ADODB.recordset")
'PL-SQL * T-SQL  
SQL1 = TransformPLSQLToTSQL (SQL1) 
	RR1.Open sql1,CC,1
	sql = ""
	if not RR1.EOF then

		aOpLog = decodTadesToArray("OPLOG",Date(),"",1,0)		
		memProgr = 0

		do until RR1.EOF
			if clng(RR1.Fields("PRG_REGOLA_RIF")) <> 0 and clng(RR1.Fields("PRG_REGOLA_RIF")) = memProgr then
				sql = sql & " and " & RR1.Fields("ID_CAMPO") &_
					 " " & searchIntoArray(RR1.Fields("COD_OPLOG"),aOpLog,1) & Trasforma(RR1.Fields("VALORE"), RR1.Fields("Tipo_campo"), searchIntoArray(RR1.Fields("COD_OPLOG"),aOpLog,1)) &_
					 " And id_persona = P.id_persona" 
			else
				sql = sql & " and exists(select id_persona from " & RR1.Fields("ID_TAB") &_
					" Where " & RR1.Fields("ID_CAMPO") &_
					" " & searchIntoArray(RR1.Fields("COD_OPLOG"),aOpLog,1) &_
					Trasforma(RR1.Fields("VALORE"), RR1.Fields("Tipo_campo"), searchIntoArray(RR1.Fields("COD_OPLOG"),aOpLog,1)) &_
					" And id_persona = P.id_persona" 

				' EPILI 17/06/2001
				select case RR1.Fields("ID_TAB")	 
					case "PERS_CONOSC","PERS_CAPAC","PERS_DISPON","PERS_COMPOR","STATO_OCCUPAZIONALE","PERS_VINCOLI"
						sql = sql & " and IND_STATUS ='0'"
					case "TISTUD"
						sql = sql & " and COD_STAT_STUD ='0'"
				end select

				memProgr = clng(RR1.Fields("PRG_REGOLA"))
			end if
			RR1.MoveNext

			if not RR1.EOF then
				if clng(RR1.Fields("PRG_REGOLA_RIF")) = 0 then 	sql = sql & ")"
			end if

		loop
		sql = sql & ")"
		Erase aOpLog
	end if
'	Response.Write "<BR>Errore : " & Err.number & "<BR>"
	RR1.Close
	set RR1 = nothing
	sql1	= ""

'	Response.Write sql
	CreaCriteriStage = sql

end function
'**********************************************************************************************************************************************************************************	
Function Trasforma(valore,tipo,operatore)
  	if operatore = "IN" or operatore = "NOT IN" then
  		if instr(Valore,"$")>0 then
			Trasforma =  "(" & Replace(Valore,"$","'") & ")"
		else
			if (tipo="A" or tipo="D") then
				Trasforma = "('" & Valore & "')"
			else
				Trasforma = "(" & Valore & ")"
			end if
		end if
	else
		select case tipo
			case "D"
				if operatore = "BETWEEN" then
					aVal = Split(valore,",",-1,1)
					Trasforma = " " & ConvDateToDbS(aVal(0)) & " and " & ConvDateToDbS(aVal(1)) & " "
				else
					Trasforma = ConvDateToDbS(valore)
				end if
			case "A"
				Trasforma = "'" & valore & "'"
			case else
				Trasforma = valore
		end select
	end if
			
End Function 
'**********************************************************************************************************************************************************************************	








%>
