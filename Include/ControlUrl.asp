<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%
Sub GetUser(LOGON_USER, LOGON_PASSWORD)
  Dim UP, Pos, Auth
  Auth = Request.ServerVariables("HTTP_AUTHORIZATION")
  LOGON_USER = ""
  LOGON_PASSWORD = ""
  If LCase(Left(Auth, 5)) = "basic" Then
    UP = Base64Decode(Mid(Auth, 7))
    Pos = InStr(UP, ":")
    If Pos > 1 Then
      LOGON_USER = Left(UP, Pos - 1)
      LOGON_PASSWORD = Mid(UP, Pos + 1)
    End If
  End If
End Sub

' Decodes a base-64 encoded string.
Function Base64Decode(base64String)
	'Const Base64CodeBase =  "!�$%&/(){}̀?�^���*@�����abcdefghijklmnopqrstuvwxyz0123456789+/"
	Const Base64CodeBase =  "abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz0123456789+/"
	Dim dataLength, Out, groupBegin
	dataLength = Len(base64String)

	Out = ""
'  Response.Write dataLength Mod 3
'  If dataLength Mod 4 <> 0 Then
'    Err.Raise 1, "Base64Decode", "Bad Base64 string."
'    Exit Function
'  End If

  ' Now decode each group:
  For groupBegin = 1 To dataLength Step 4
    Dim numDataBytes, CharCounter, thisChar, thisData, groupData
    ' Each data group encodes up To 3 actual bytes.
    numDataBytes = 3
    groupData = 0

    For CharCounter = 0 To 3
      ' <B>Convert</B> each character into 6 bits of data, And add it To
      ' an integer For temporary storage.  If a character is a '=', there
      ' is one fewer data byte.  (There can only be a maximum of 2 '=' In
      ' the whole string.)

      thisChar = Mid(base64String, groupBegin + CharCounter, 1)

      If thisChar = "=" Then
        numDataBytes = numDataBytes - 1
        thisData = 0
      Else
        thisData = InStr(Base64CodeBase, thisChar) - 1
      End If
      
      If thisData = -1 Then
        Err.Raise 2, "Base64Decode", "Bad character In Base64 string."
        Exit Function
      End If
	
	groupData = 64 * groupData + thisData
    Next

    ' Convert 3-byte integer into up To 3 characters
    Dim OneChar
    For CharCounter = 1 To numDataBytes
      Select Case CharCounter
        Case 1
            OneChar = groupData \ 65536
        Case 2
            OneChar = (groupData And 65535) \ 256
        Case 3
            OneChar = (groupData And 255)
      End Select
      Out = Out & Chr(OneChar)
    Next
  Next
  Base64Decode = Out
  'Response.write "<BR> Decodificato = (" & Out & ")<BR>"
End Function


Function Base64Encode(InString)

'Const Base64CodeBase = "!�$%&/(){}̀?�^���*@�����abcdefghijklmnopqrstuvwxyz0123456789+/"
Const Base64CodeBase = "abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz0123456789+/"
Dim i, j, inData, outStr, blockNum, spareChars
Dim tmpChar1, tmpChar2, tmpChar3
inData = InString
inData = Replace(inData, Chr(13), "")   'removing CRL
inData = Replace(inData, Chr(10), "")
blockNum = CInt(Len(inData) \ 3)
spareChars = Len(inData) Mod 3
outStr = ""
'Response.write "<BR>Da codificare = " & inData & "<BR>"

' main cicle, block encoding
For i = 1 To (blockNum*3)-1 step 3
        tmpChar1 = Asc (Mid(inData, i, 1))
        tmpChar2 = Asc (Mid(inData, i + 1, 1))
        tmpChar3 = Asc (Mid(inData, i + 2, 1))
        N16bit = tmpChar1 \ (2 ^ 2)
        C16bit = Mid(Base64CodeBase, N16bit + 1, 1)
        N26bit = ((tmpChar1 And 3) * (2 ^ 4)) Or (tmpChar2 \ (2 ^ 4))
	    C26bit = Mid(Base64CodeBase, N26bit + 1, 1)
        N36bit = ((tmpChar2 And 15) * (2 ^ 2)) Or ((tmpChar3 And 192) \ (2 ^ 6))
        C36bit = Mid(Base64CodeBase, N36bit + 1, 1)
        N46bit = tmpChar3 And 63
        C46bit = Mid(Base64CodeBase, N46bit + 1, 1)
        outStr = outStr & C16bit & C26bit & C36bit & C46bit
Next
Select Case spareChars

Case 2
        tmpChar1 = Asc(Mid(inData, blockNum*3+1, 1))
        tmpChar2 = Asc(Mid(inData, (blockNum*3) + 2, 1))
        N16bit = (tmpChar1 And 252) \ (2 ^ 2)
        C16bit = Mid(Base64CodeBase, N16bit + 1, 1)
        N26bit = ((tmpChar1 And 3) * (2 ^ 4)) Or ((tmpChar2 And 240) \ (2 ^ 4))

        C26bit = Mid(Base64CodeBase, N26bit + 1, 1)
        N36bit = ((tmpChar2 And 15) * (2 ^ 2)) 'Or ((tmpChar3 And 192) \ (2 ^ 6))
        C36bit = Mid(Base64CodeBase, N36bit + 1, 1)
        outStr = outStr & C16bit & C26bit & C36bit & "="
Case 1
        tmpChar1 = Asc(Mid(inData, blockNum*3+1, 1))
        N16bit = (tmpChar1 And 252) \ (2 ^ 2)
        C16bit = Mid(Base64CodeBase, N16bit + 1, 1)
        N26bit = ((tmpChar1 And 3) * (2 ^ 4))
        C26bit = Mid(Base64CodeBase, N26bit + 1, 1)
        outStr = outStr & C16bit & C26bit & "=="
End Select

Base64Encode = outStr

'Response.write "<BR>Codificato= (" & outStr & ") <BR>"

End Function
%>
