
<!--#include file ="DecCod.asp"-->
<!--#include Virtual ="/Util/DBUtil.asp"-->
<!--#include Virtual = "/includeMTSS/adovbs.asp"-->
<%

    function EseguiNoCTrace(Progetto, Chiave, Tabella, Utente,Ident,TipoPers, Oper, sSQL, Trace, Tmst,CnCon)
    '---------------------------------------------------
    'funcion q actua como interfaz para llamar a EseguiNoCTrace sin los dos parametros. Para mantener la compatibilidad
    '---------------------------------------------------
    
         EseguiNoCTrace = EseguiNoCTraceIdentificador(Progetto, Chiave, Tabella, Utente, Ident, TipoPers, Oper, sSQL, Trace, Tmst,CnCon, 0, 0)
         
    end function 
    
	function EseguiNoCTraceIdentificador(Progetto, Chiave, Tabella, Utente,Ident,TipoPers, Oper, sSQL, Trace, Tmst,CnCon, ExisteIdentity, ValorIdentity)
    '---------------------------------------------------
    'Funcion que realiza el insert o update enviado y graba el log.
    '---------------------------------------------------
		dim msg_errore
		dim decod_errore
		dim desc_errore
        on error resume next

       
        'response.write sSQL

		'if Oper <> "INS" and Tmst<>"" then
		'   sSQL=sSQL & " AND DT_TMST=" & ConvDateToDB(Tmst)
		'end if

		EseguiNoCTraceIdentificador = 0

        'response.write SSQL

		'''Llamada al sp para ingresar el registro. Vany
        dim cmdInsert 
        dim rsInsert
		
        set cmdInsert = server.CreateObject("adodb.command")
        set rsInsert = server.CreateObject("adodb.recordset")
		
        cmdInsert.ActiveConnection = cc
        cmdInsert.CommandType = adCmdStoredProc  
        cmdInsert.CommandText = "InsertRegistroEnTabla"
		
        For Each Parameter In cmdInsert.Parameters
            If Parameter.Direction And adParamInput Then
                Select case ucase(Mid(Parameter.Name, 2))
	                case "STRSQL"
		                Parameter.value = sSQL
	                case else
                end select
            end if
        next            
		
        set RsInsert = cmdInsert.Execute(XX)
		
        if not RsInsert.eof then
            RsInsert.MoveFirst()
            ExisteIdentity = RsInsert.Fields(0)
            ValorIdentity = RsInsert.Fields(1)
            nRec = 1 'actualizo el registro.
        end if
    'response.Write Err.number 
    'response.Write Err.Description 
    'response.End 
	              
		'response.Write "ExisteIdentity: " & ExisteIdentity & "<br>"
		'response.Write "ValorIdentity: " & ValorIdentity & "<br>" 
		'response.End 
''''Fin llamada al sp '' vany

		if (Err.Number <> 0 or nRec=0) and Oper <> "DEL" then

			if nRec=0 and Oper <> "INS" and Err.Number = 0 then
				EseguiNoCTraceIdentificador="<BR>Registro actualizado por otro usuario."
			else
				msg_errore=cstr(Err.Number)
				desc_errore=Err.Description
				'CnCon.RollbackTrans
				Err.Clear
				decod_errore=DecCodVal("ERROR", 0,"", msg_errore, 1)
				if decod_errore="" then
					EseguiNoCTraceIdentificador="<BR>Errore cod=" & cstr(msg_errore) & "<BR>" & desc_errore
				else
					EseguiNoCTraceIdentificador=decod_errore
				end if
				sClose = 0
			end if
		else

    		if Trace <> "0" then

    		    'M3 comienzo modificacion
    			if(Ident = "")then
		            Ident = 0
		        end if
		        'M3 fin modificacion}

			    sSql="INSERT INTO LOG_APP (Progetto,ID_REC, NOME_TAB, IDUTENTE,IDENT,TIPO_PERS, TIPO_OP, DT_TMST) "
				sSql=sSql & "VALUES ('" & Progetto & "','" & Chiave & "','" & Tabella & "','" & Utente & "','" & Ident & "','" & TipoPers & "','" & Oper & "'," & convdatetodb(Now()) & ")"

				CnCon.Execute sSql
				if Err.Number <> 0 then
					msg_errore=cstr(Err.Number)
					desc_errore=Err.Description
					'CnCon.RollbackTrans
					Err.Clear
					decod_errore=DecCodVal("ERROR", 0,"", msg_errore, 1)
					if decod_errore="" then
						EseguiNoCTraceIdentificador="<BR>Errore cod=" & cstr(msg_errore) & "<BR>" & desc_errore
					else
						EseguiNoCTraceIdentificador=decod_errore
					end if
					sClose = 0
				end if
			end if

		end if
	end function
%>

<%
    function EseguiNoC(sSQL,CnCon)
    '---------------------------------------------------
    'Funcion que realiza el insert o update enviado. Para mantener la compatibilidad
    '---------------------------------------------------
    'response.Write ssql
        EseguiNoC = EseguiNoCIdentificador(sSQL, CnCon, 0, 0)
    
    end function
    
	function EseguiNoCIdentificador(sSQL, CnCon, ExisteIdentity, ValorIdentity)
	'---------------------------------------------------
    'Funcion que realiza el insert o update enviado. 
    '---------------------------------------------------
		dim msg_errore
		dim decod_errore
		dim desc_errore
        on error resume next

		EseguiNoCIdentificador = 0
		'CnCon.Execute sSQL
		
'''Llamada al sp para ingresar el registro. Vany
        dim cmdInsert 
        dim rsInsert
		
        set cmdInsert = server.CreateObject("adodb.command")
        set rsInsert = server.CreateObject("adodb.recordset")
		
        cmdInsert.ActiveConnection = cc
        cmdInsert.CommandType = adCmdStoredProc  
        cmdInsert.CommandText = "InsertRegistroEnTabla"
		
        For Each Parameter In cmdInsert.Parameters      
               If Parameter.Direction And adParamInput Then    
                Select case ucase(Mid(Parameter.Name, 2))
	                case "STRSQL"
		                Parameter.value = sSQL
	                case else
                end select
            end if
        next            

        set RsInsert = cmdInsert.Execute(XX)
		
		
        if not RsInsert.eof then
            RsInsert.MoveFirst()
            ExisteIdentity = RsInsert.Fields(0)
            ValorIdentity = RsInsert.Fields(1)
            'nRec = 1 'actualizo el registro.
        end if

''''Fin llamada al sp '' vany

		if Err.Number <> 0 then
			msg_errore=cstr(Err.Number)
			desc_errore=Err.Description
			'CnCon.RollbackTrans
			Err.Clear
			decod_errore=DecCodVal("ERROR", 0,"", msg_errore, 1)
			if decod_errore="" then
				EseguiNoCIdentificador="<BR>Errore cod=" & cstr(msg_errore) & "<BR>" & desc_errore
			else
				EseguiNoCIdentificador=decod_errore
			end if
			sClose = 0
		end if
	end function
%>

<%

    function Esegui(Chiave, Tabella, Utente, Oper, StrSQL, Trace, Tmst)
    '---------------------------------------------------
    'Funcion que realiza el insert o update enviado. Para mantener la compatibilidad
    '---------------------------------------------------
        Esegui = EseguiIdentificador(Chiave, Tabella, Utente, Oper, StrSQL, Trace, Tmst,0,0)
    end function 	
	
	Function EseguiIdentificador(Chiave, Tabella, Utente, Oper, StrSQL, Trace, Tmst,ExisteIdentity,ValorIdentity)
	'---------------------------------------------------
    'Funcion que realiza el insert o update enviado. 
    '---------------------------------------------------	
		dim sSql			'stringa SQL
		dim nRec			'numero record(s) modificati
		dim msg_errore
		dim decod_errore
		dim desc_errore

		on error resume next
'		Response.Write "<BR>" & StrSQL
		CC.BeginTrans

		if Oper <> "INS" and Tmst<>"" then
		   StrSql=StrSql & " AND DT_TMST=" & ConvDateToDB(Tmst)
		end if

'		Response.Write "<BR>" & strsql & "<br>"
		'CC.Execute StrSql, nRec

'''Llamada al sp para ingresar el registro. Vany
        dim cmdInsert 
        dim rsInsert
		
        set cmdInsert = server.CreateObject("adodb.command")
        set rsInsert = server.CreateObject("adodb.recordset")
		
        cmdInsert.ActiveConnection = cc
        cmdInsert.CommandType = adCmdStoredProc  
        cmdInsert.CommandText = "InsertRegistroEnTabla"
		
        For Each Parameter In cmdInsert.Parameters
            If Parameter.Direction And adParamInput Then
                Select case ucase(Mid(Parameter.Name, 2))
	                case "STRSQL"
		                Parameter.value = STRSQL
	                case else
                end select
            end if
        next            
		
        set RsInsert = cmdInsert.Execute(XX)
		
        if not RsInsert.eof then
            RsInsert.MoveFirst()
            ExisteIdentity = RsInsert.Fields(0)
            ValorIdentity = RsInsert.Fields(1)
            nRec = 1 'actualizo el registro.
        end if
    'response.Write Err.number 
    'response.Write Err.Description 
    'response.End 
	                
''''Fin llamada al sp '' vany


'		Response.Write "<BR>nRec : " & nRec & "<br>"
'		Response.Write "<BR>Err.Description : " & Err.Description
'		Response.Write "<BR>OPER : " & OPER & "<br>"
'		Response.Write "<BR>Err.Number : " & Err.Number & "<br>"

		if Err.Number <> 0 or nRec=0 then
			msg_errore=cstr(Err.Number)
			desc_errore=Err.Description
			if nRec=0 and Oper <> "INS" and Err.Number = 0 then
				EseguiIdentificador="<BR>Registro actualizado por otro usuario."
			else
				CC.RollbackTrans
				Err.Clear
				decod_errore=DecCodVal("ERROR", 0,"", msg_errore, 1)
				if decod_errore="" then
					EseguiIdentificador="<BR>Errore cod=" & cstr(msg_errore) & "<BR>" & desc_errore
				else
					EseguiIdentificador=decod_errore
				end if
			end if
		else
			CC.CommitTrans
			If Trace <> "0" then
				sSql="INSERT INTO LOG_APP (ID_REC, NOME_TAB, ID_PERSONA, TIPO_OP, DATA) "
				sSql=sSql & "VALUES ('" & Chiave & "','" & Tabella & "','" & Utente & "','" & Oper & "'," & ConvDateToDB(Date()) & ")"
				'Response.Write sSql
			end if
			EseguiIdentificador="0"
		End if
		'response.write "Esegui= " & Esegui
	End Function

	Function IIF(bCond,sTrue,sFalse)
		if bCond then IIF=sTrue else IIF=sFalse
	End Function
%>
