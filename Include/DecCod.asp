<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include Virtual="/util/dbUtil.asp"-->
<%
' Data una stringa formatta con quattro valori ed una condizione 
' crea un Combo Box con all'interno l'elenco delle descrizioni
' dei record trovati. 


sub  CreateComboSQL(sSQL)    
	dim rsRec
	
'	Struttura variabile [ sValoriArray ]:
'		sValoriArray(0) = Nome della tabella
'		sValoriArray(1) = Isa 
'		sValoriArray(2) = Data di validit�
'		sValoriArray(3) = Valore da selezionare
'		sValoriArray(4) = Nome del Combo
'		sValoriArray(5) = Condizione


'PL-SQL * T-SQL  
' Tolta per ottimizzare l'uso di risorse sql
'SSQL = TransformPLSQLToTSQL (SSQL) 
	set rsRec = CC.Execute(sSQL)
	
	' creo la Combo Box con i dati che query ha ritornato.
	Response.Write "<SELECT ID='" & _
			"cmbAzione" & "' class=textblack name='" & "cmbAzione" & "'><OPTION value=''></OPTION>"

	do until rsRec.EOF
		
		Response.Write "<OPTION "
		Response.write "value ='" & rsRec.Fields("estadodestino") & _
			"'> " & rsRec.Fields("descripcion")  & "</OPTION>"
		response.Write value
		rsRec.MoveNext 
	loop
	
	Response.Write "</SELECT>"
	rsRec.Close
	set rsRec = Nothing
end sub


sub  CreateCombo(sStringa)    
	dim sValoriArray
	dim rsRec
	dim sSQL
	dim sIsa
	
	'Response.Write sstringa
	sValoriArray = Split(sStringa, "|", -1, 1)

    

'	Struttura variabile [ sValoriArray ]:
'		sValoriArray(0) = Nome della tabella / Name of Table
'		sValoriArray(1) = Isa / ??
'		sValoriArray(2) = Data di validit� / Data of validation
'		sValoriArray(3) = Valore da selezionare / Selected value
'		sValoriArray(4) = Nome del Combo / Name of combobox
'		sValoriArray(5) = Condizione / Conditions

	if sValoriArray(1) = "0" then
		sIsa = " Isa = '0'"
	else
		sIsa = " Isa in ('0','" & sValoriArray(1) & "')"
	end if
	
	' Se non passo la decorrenza la imposta con la data di sistema
	' if you don't send the date, it's auto asign to system date.
	if sValoriArray(2) = "" then
		sValoriArray(2) = date
	end if

	
	if sValoriArray(0)="PRVEX" then
		sSQL = "SELECT Nome_Tabella, Codice, Decorrenza,Scadenza, " &_
			"Descrizione,Valore FROM TADES with (nolock) WHERE Nome_Tabella in ('PROV','PRVEX')" &_
			" AND" & sIsa & " AND " & ConvDateToDB(sValoriArray(2)) &_
			" BETWEEN Decorrenza AND Scadenza"
	else
		sSQL = "SELECT Nome_Tabella, Codice, Decorrenza,Scadenza, " &_
			"Descrizione,Valore FROM TADES with (nolock) WHERE Nome_Tabella ='" &_
			sValoriArray(0) &  "' AND" & sIsa & " AND " & ConvDateToDB(sValoriArray(2)) &_
			" BETWEEN Decorrenza AND Scadenza"
	
	end if


	if Trim(sValoriArray(5)) <> "" then
		sSQL = sSQL & " " & sValoriArray(5) 
	end if
	
	
'Response.Write "Query " & sSQL
'Response.Write("atrray " & sValoriArray(3))
'Response.end


'PL-SQL * T-SQL  
' Tolta per ottimizzare l'uso di risorse sql'
'SSQL = TransformPLSQLToTSQL (SSQL) 

	set rsRec = CC.Execute(sSQL)
	'response.Write ssql
	'Response.Write("codice " & rsRec.Fields("Codice"))

	' creo la Combo Box con i dati che query ha ritornato.
    onch=""
    if instr(sValoriArray(4),"onchange") <> 0 then
        onch=mid(sValoriArray(4), instr(sValoriArray(4),"onchange") )
        sValoriArray(4) = trim(left(sValoriArray(4),instr(sValoriArray(4),"onchange")-1 ))
        'response.write sValoriArray(4) & "----" & onch
        Response.Write "<SELECT ID='" & sValoriArray(4) & " " & onch & "' class=textblack name='" & sValoriArray(4) & "><OPTION value=''></OPTION>"
    else 
        Response.Write "<SELECT ID='" & sValoriArray(4) & "' class=textblack name='" & sValoriArray(4) & "'><OPTION value=''></OPTION>"
    end if

	do until rsRec.EOF
		
		
		Response.Write "<OPTION "

		' Ricerca il record della combo da evidenziare.
		if sValoriArray(3) = rsRec.Fields("Codice") then
			Response.Write "selected "
		end if
		
		Response.write "value ='" & rsRec.Fields("Codice") & _
			"'> " & rsRec.Fields("Descrizione")  & "</OPTION>"
		response.Write value
		rsRec.MoveNext 
	loop
	
	Response.Write "</SELECT>"
		'response.Write (sSQL)
	rsRec.Close
	set rsRec = Nothing
end sub

' Restituisce la decodifica cio� la descrizione del codice relativo alla tabella
' per l'isa richiesto alla data richiesta
'
' flag_conn = 1 --> non devo aprire la connessione
'

sub  CreateComboSoloLectura(sStringa)   ' es igual al CreateCombo,  lo ideal seria agregarlo como parametro
	dim sValoriArray
	dim rsRec
	dim sSQL
	dim sIsa
	
	sValoriArray = Split(sStringa, "|", -1, 1)

'	Struttura variabile [ sValoriArray ]:
'		sValoriArray(0) = Nome della tabella
'		sValoriArray(1) = Isa 
'		sValoriArray(2) = Data di validit�
'		sValoriArray(3) = Valore da selezionare
'		sValoriArray(4) = Nome del Combo
'		sValoriArray(5) = Condizione

	if sValoriArray(1) = "0" then
		sIsa = " Isa = '0'"
	else
		sIsa = " Isa in ('0','" & sValoriArray(1) & "')"
	end if
	
	' Se non passo la decorrenza la imposta con la data di sistema
	if sValoriArray(2) = "" then
		sValoriArray(2) = date
	end if
	
	if sValoriArray(0)="PRVEX" then
		sSQL = "SELECT Nome_Tabella, Codice, Decorrenza,Scadenza, " &_
			"Descrizione,Valore FROM TADES with (nolock) WHERE Nome_Tabella in ('PROV','PRVEX')" &_
			" AND" & sIsa & " AND " & ConvDateToDB(sValoriArray(2)) &_
			" BETWEEN Decorrenza AND Scadenza"
	else
		sSQL = "SELECT Nome_Tabella, Codice, Decorrenza,Scadenza, " &_
			"Descrizione,Valore FROM TADES with (nolock) WHERE Nome_Tabella ='" &_
			sValoriArray(0) &  "' AND" & sIsa & " AND " & ConvDateToDB(sValoriArray(2)) &_
			" BETWEEN Decorrenza AND Scadenza"
	
	end if
	
	if Trim(sValoriArray(5)) <> "" then
		sSQL = sSQL & " " & sValoriArray(5) 
	end if

'PL-SQL * T-SQL  
' Tolta per ottimizzare l'uso di risorse sql
'SSQL = TransformPLSQLToTSQL (SSQL) 
	'response.Write ssql
	set rsRec = CC.Execute(sSQL)
	
	' creo la Combo Box con i dati che query ha ritornato.
	Response.Write "<SELECT disabled=""disabled"" ID='" & _
			sValoriArray(4) & "' class=textblack name='" & sValoriArray(4) & "'><OPTION value=''></OPTION>"

	do until rsRec.EOF
		
		
		Response.Write "<OPTION "

		' Ricerca il record della combo da evidenziare.
		if sValoriArray(3) = rsRec.Fields("Codice") then
			Response.Write "selected "
		end if
		
		Response.write "value ='" & rsRec.Fields("Codice") & _
			"'> " & rsRec.Fields("Descrizione")  & "</OPTION>"
		response.Write value
		rsRec.MoveNext 
	loop
	
	Response.Write "</SELECT>"
	rsRec.Close
	set rsRec = Nothing
end sub


' Restituisce la decodifica cio� la descrizione del codice relativo alla tabella
' per l'isa richiesto alla data richiesta
'
' flag_conn = 1 --> non devo aprire la connessione
'
function DecCodVal(nome_tabella, isa, dt_decor, codice, flag_conn)

	dim rsRec
	dim sSQL
	dim sIsa
	dim dt_decorrenza
	
	If dt_decor = ""  or isnull(dt_decor) then
		dt_decorrenza = date
	else
		dt_decorrenza = dt_decor
	end if
		
	if isa = "0" then
		sIsa = " and Isa = '0'"
	elseif isa = "" then
		sIsa = " "		
	else
		sIsa = " Isa IN ('0','" & isa & "')"
	end if

	if isnull(Codice) then 
		codice = ""
	end if 

'Response.write "nome_tabella: " & nome_tabella & "<br>"
'Response.write "sIsa: " & sIsa & "<br>"
'Response.write "dt_decorrenza: " & dt_decorrenza & "<br>"
'Response.write "codice: " & codice & "<br>"
'Response.write "flag_conn: " & flag_conn & "<br>"


	if nome_tabella="PRVEX" then
		sSQL = ""
		sSQL = sSQL & "SELECT Descrizione FROM TADES with (nolock) WHERE Nome_Tabella in ('PROV','PRVEX')" 
		sSQL = sSQL & " AND codice = '" & codice & "' AND " & ConvDateToDBs(dt_decorrenza) 
		sSQL = sSQL & " between Decorrenza AND Scadenza " & sIsa & " order by Descrizione asc"
	else
		sSQL = ""
		sSQL = sSQL & "SELECT Descrizione FROM TADES with (nolock) WHERE" 
		sSQL = sSQL & " Nome_Tabella ='" & nome_tabella & "' AND codice = '" & codice & "' AND " & ConvDateToDBs(dt_decorrenza)
		sSQL = sSQL & " between Decorrenza AND Scadenza" & sIsa 
	end if
	
	'response.write ssql
	'Response.End 


	if IsObject(CX) then
'PL-SQL * T-SQL  
' Tolta per ottimizzare l'uso di risorse sql
'SSQL = TransformPLSQLToTSQL (SSQL) 
		set rsRec=CX.Execute(sSQL)
	else
'PL-SQL * T-SQL  
' Tolta per ottimizzare l'uso di risorse sql
'SSQL = TransformPLSQLToTSQL (SSQL) 
		set rsRec = CC.Execute(sSQL)
	end if 

	if not rsRec.Eof then
		DecCodVal=rsRec("Descrizione")
		rsRec.Close
	else
		DecCodVal=""
	end if

	set rsRec = Nothing

end function


function DecCodLocalidad(nome_tabella, isa, dt_decor, codice, flag_conn)

	dim rsRec
	dim sSQL
	dim sIsa
	dim dt_decorrenza
	
	
		
	if isa = "0" then
		sIsa = " Isa = '0'"
	else
		sIsa = " Isa IN ('0','" & isa & "')"
	end if

		sSQL = "SELECT Descripcion FROM Localidad WHERE CODLOC ='"& codice 
			

	'response.write ssql

	if IsObject(CX) then
'PL-SQL * T-SQL  
' Tolta per ottimizzare l'uso di risorse sql
'SSQL = TransformPLSQLToTSQL (SSQL) 
		set rsRec=CX.Execute(sSQL)
	else
'PL-SQL * T-SQL  
' Tolta per ottimizzare l'uso di risorse sql
'SSQL = TransformPLSQLToTSQL (SSQL) 
		set rsRec = CC.Execute(sSQL)
	end if 

	if not rsRec.Eof then
		DecCodVal=rsRec.Fields("Descrizione")
		rsRec.Close
	else
		DecCodVal=""
	end if

	set rsRec = Nothing

end function













' Restituisce la decodifica cio� la descrizione del codice relativo alla tabella
' per l'isa richiesto alla data richiesta e l'indicazione di validit�
' del codice stesso
'  0 - valido
'  1 - chiuso
'
function DecCodValidita(nome_tabella, isa, dt_decor, codice, flag_conn)

	dim rsRec
	dim sSQL
	dim sIsa
	dim dt_decorrenza
	
	If dt_decor = ""  or isnull(dt_decor) then
		dt_decorrenza = date
	else
		dt_decorrenza = dt_decor
	end if
		
	if isa = "0" then
		sIsa = " Isa = '0'"
	else
		sIsa = " Isa IN ('0','" & isa & "')"
	end if

	if nome_tabella="PRVEX" then
		sSQL = "SELECT Descrizione,scadenza FROM TADES with (nolock) WHERE Nome_Tabella in ('PROV','PRVEX')" &_
			" AND " & sIsa & " AND codice = '" & codice & "' AND " & ConvDateToDBs(dt_decorrenza) &_
			" between Decorrenza AND Scadenza"	
	else
		sSQL = "SELECT Descrizione,scadenza FROM TADES with (nolock) WHERE Nome_Tabella ='" &_
			nome_tabella & "' AND " & sIsa & " AND codice = '" & codice & "' AND " & ConvDateToDBs(dt_decorrenza) &_
			" between Decorrenza AND Scadenza"
	end if
	
	if IsObject(CX) then
'PL-SQL * T-SQL  
' Tolta per ottimizzare l'uso di risorse sql
'SSQL = TransformPLSQLToTSQL (SSQL) 
		set rsRec=CX.Execute(sSQL)
	else
'PL-SQL * T-SQL  
' Tolta per ottimizzare l'uso di risorse sql
'SSQL = TransformPLSQLToTSQL (SSQL) 
		set rsRec = CC.Execute(sSQL)
	end if 

	if not rsRec.Eof then
		if rsRec.Fields("Scadenza")< date() then
			DecCodValidita=rsRec.Fields("Descrizione") & "|1"
		else
			DecCodValidita=rsRec.Fields("Descrizione") & "|0"
		end if
		rsRec.Close
	else
		DecCodValidita=""
	end if

	set rsRec = Nothing

end function

function decodTadesToArray(sTabella,dData,sCondizione,nOrder,Isa)
' sTabella		-- > Nome della Tabella
' dData			-- > Data validit� della query.
' sCondizione	-- > Eventuale condizione per la query
'				-- La stringa deve contenere [CAMPO = VALORE AND ......]
' nOrder		-- > Tipo di ordinamento pu� assumere questi 3 valori :
'				0 -- per descrizione;
'				1 -- per codice;
'				2 -- per valore;
'-----------------------------------------------------------------------
' L'array conterra sar� strutturato cos� : CODICE, DESCRIZIONE, VALORE
' Nel caso non ci siano elementi sar� impostato decodTadesToArray(0,0,0)=null'
'-----------------------------------------------------------------------
'-----------------------------------------------------------------------
' Ricordarsi sempre di distruggere l'oggetto con la funzione Erase 
' dopo averne terminato l'utilizzo nella pagina che utilizza questa funzione
' Esempio : Erase nome_array
'-----------------------------------------------------------------------
	dim i, j, k
	
	MatrizIsa = decodTadesToArrayIsa(sTabella,dData,sCondizione,nOrder,Isa)
	'..MatrizIsa = (0,0,0,0) --> (0,0,0,0)
	
	' dimensiones de la matriz original
	Dim1 = ubound(MatrizIsa, 1)
	Dim2 = ubound(MatrizIsa, 2)
	Dim3 = ubound(MatrizIsa, 3)
	
	'Response.Write Dim1 & Dim2 & Dim3
	'Response.end
	
	' Copia la matrix hallada 
	redim outputArray (Dim1, Dim2, Dim3)
	for I = 0 to (Dim1 -1)
		outputArray(i, i, i)	= MatrizIsa(i, i, i, i)
		outputArray(i, i+1, i)	= MatrizIsa(i, i+1, i, i)
		outputArray(i, i+1, i+1)= MatrizIsa(i, i+1, i+1, i)
	next

	' salida 
	decodTadesToArray = outputArray
		
end function

function decodTadesToArrayAll(sTabella,dData,sCondizione,nOrder,Isa)
' sTabella		-- > Nome della Tabella
' dData			-- > Data validit� della query.
' sCondizione	-- > Eventuale condizione per la query
'				-- La stringa deve contenere [CAMPO = VALORE AND ......]
' nOrder		-- > Tipo di ordinamento pu� assumere questi 3 valori :
'				0 -- per descrizione;
'				1 -- per codice;
'				2 -- per valore;
'-----------------------------------------------------------------------
' L'array conterra sar� strutturato cos� : CODICE, DESCRIZIONE, VALORE
' Nel caso non ci siano elementi sar� impostato decodTadesToArray(0,0,0)=null'
'-----------------------------------------------------------------------
'-----------------------------------------------------------------------
' Ricordarsi sempre di distruggere l'oggetto con la funzione Erase 
' dopo averne terminato l'utilizzo nella pagina che utilizza questa funzione
' Esempio : Erase nome_array
'-----------------------------------------------------------------------
	dim i, j, k
	
	MatrizIsa = decodTadesToArrayIsaAll(sTabella,dData,sCondizione,nOrder,Isa)
	'..MatrizIsa = (0,0,0,0) --> (0,0,0,0)
	
	' dimensiones de la matriz original
	Dim1 = ubound(MatrizIsa, 1)
	Dim2 = ubound(MatrizIsa, 2)
	Dim3 = ubound(MatrizIsa, 3)
	
	'Response.Write Dim1 & Dim2 & Dim3
	'Response.end
	
	' Copia la matrix hallada 
	redim outputArray (Dim1, Dim2, Dim3)
	for I = 0 to (Dim1 -1)
		outputArray(i, i, i)	= MatrizIsa(i, i, i, i)
		outputArray(i, i+1, i)	= MatrizIsa(i, i+1, i, i)
		outputArray(i, i+1, i+1)= MatrizIsa(i, i+1, i+1, i)
	next
	
	' salida 
	decodTadesToArrayAll = outputArray
		
end function

function decodTadesToArrayIsa(sTabella,dData,sCondizione,nOrder,Isa)
' sTabella		-- > Nome della Tabella
' dData			-- > Data validit� della query.
' sCondizione	-- > Eventuale condizione per la query
'				-- La stringa deve contenere [CAMPO = VALORE AND ......]
' nOrder		-- > Tipo di ordinamento pu� assumere questi 3 valori :
'				0 -- per descrizione;
'				1 -- per codice;
'				2 -- per valore;
'-----------------------------------------------------------------------
' L'array conterra sar� strutturato cos� : CODICE, DESCRIZIONE, VALORE, ISA
' Nel caso non ci siano elementi sar� impostato decodTadesToArray(0,0,0,0)=null'
'-----------------------------------------------------------------------
'-----------------------------------------------------------------------
' Ricordarsi sempre di distruggere l'oggetto con la funzione Erase 
' dopo averne terminato l'utilizzo nella pagina che utilizza questa funzione
' Esempio : Erase nome_array
'-----------------------------------------------------------------------

	dim sIsa
	dim sSQL
	dim sOrder
	dim outputArray()
	dim nEle
'	Response.Write sTabella & "-" & dData & "-" & sCondizione & "-" & nOrder & "-" & Isa
	select case nOrder
		case 0
			sOrder = " ORDER BY DESCRIZIONE"
		case 1
			sOrder = " ORDER BY CODICE"
		case 2
			sOrder = " ORDER BY VALORE"
		case 3 
			sOrder = " ORDER BY ISA, CODICE"
	end select

	if isa = "0" then
		sIsa = " and Isa = '0'"
	elseif isa = "" then
		sIsa = " "		
	else
		sIsa = " Isa IN ('0','" & isa & "')"
	end if

	
	if sTabella="PRVEX" then
		sSQL = "SELECT CODICE,DESCRIZIONE,VALORE,ISA FROM TADES with (nolock) WHERE NOME_TABELLA  in ('PROV','PRVEX')" &_
			 " AND " & ConvDateToDBs(dData) & " BETWEEN DECORRENZA AND SCADENZA" & sIsa
		select case nOrder
			case 0
				sOrder = " ORDER BY NOME_TABELLA, DESCRIZIONE"
			case 1
				sOrder = " ORDER BY NOME_TABELLA, CODICE"
			case 2
				sOrder = " ORDER BY NOME_TABELLA, VALORE"
			case 3 
				sOrder = " ORDER BY ISA, CODICE"
		end select
			
	else

		sSQL = "SELECT CODICE,DESCRIZIONE,VALORE,ISA FROM TADES with (nolock) WHERE NOME_TABELLA ='" &_
			sTabella & "' AND " & ConvDateToDBs(dData) & " BETWEEN DECORRENZA AND SCADENZA" & sIsa
			
' IF Agregado para que ordene los dias en Disponibilidad,  el original esta dentro del ELSE
		if sTabella = "TDISP" then
			select case nOrder
					case 0
						sOrder = " ORDER BY NOME_TABELLA, DESCRIZIONE"
					case 1
						sOrder = " ORDER BY NOME_TABELLA, CODICE"
					case 2
						sOrder = " ORDER BY NOME_TABELLA, VALORE, CODICE"
					case 3 
						sOrder = " ORDER BY ISA, CODICE"
			end select			
		Else						
			select case nOrder
					case 0
						sOrder = " ORDER BY NOME_TABELLA, DESCRIZIONE"
					case 1
						sOrder = " ORDER BY NOME_TABELLA, CODICE"
					case 2
						sOrder = " ORDER BY NOME_TABELLA, VALORE"
					case 3 
						sOrder = " ORDER BY ISA, CODICE"
			end select		
		End if
				
	end if
	' Se indico la condizione la applico alla query.
	if sCondizione <> "" then
		sSQL = sSQL & " AND " & sCondizione 
	end if

	sSQL = sSQL & sOrder
 	set rsDecTades = Server.CreateObject("ADODB.Recordset")
 	
 	'Response.Write sSql & "<BR>"
	'Response.Write SSQL 
	'Response.End 
			
'PL-SQL * T-SQL  
' Tolta per ottimizzare l'uso di risorse sql
'Response.Write ssql & "<br><br><br><br>"
SSQL = TransformPLSQLToTSQL (SSQL)
'Response.Write SSQL
	rsDecTades.open sSQL,CC
	nEle = rsDecTades.RecordCount
	
	if not rsDecTades.EOF then
		redim outputArray(nEle,nEle,nEle,nEle)
		i = 0
		do while not rsDecTades.EOF
			outputArray(i, i, i, i)		= trim(rsDecTades("CODICE"))
			outputArray(i,i+1,i, i)	= trim(rsDecTades("DESCRIZIONE"))
			outputArray(i,i+1,i+1, i)	= trim(rsDecTades("VALORE"))
			outputArray(i, i+1 ,i+1, i+1)	= trim(rsDecTades("ISA"))
			i=i+1
			rsDecTades.MoveNext()
		loop
	else
		redim outputArray(0,0,0,0)
		outputArray(0,0,0, 0) = null
	end if 
	rsDecTades.Close()
	set rsDecTades = nothing

	decodTadesToArrayIsa = outputArray
	Erase outputArray

end function

function searchIntoArray(sValore,aArray,sTiDato)
'sArray            --> � un array tipo decodTadesToArray a 3 dimensioni
'sValore(string)   --> � il codice da ricercare nelle posizioni (i,i,i)
'sTiDato(numerico) --> cosa deve restituire la funzione
'                      0 -- la cella (i,i+1,i)   "la DESCRIZIONE"
'                      1 -- la cella (i,i+1,i+1) "il VALORE"
'NOTA: la funzione restituisce -1 se non trova sValore in aArray(i,i,i)
	dim t 
	for t=0 to ubound(aArray) -1
		if sValore = aArray(t,t,t) then
			select case sTiDato
				case 0 
				searchIntoArray = aArray(t,t+1,t)			
				case 1
				searchIntoArray = aArray(t,t+1,t+1)
			end select			
			exit for
		end if
		searchIntoArray = "-1"
	next
end function

function decGradoConosc(iValore)

decGradoConosc = DecCodValDizDati("PERS_CONOSC","COD_GRADO_CONOSC",iValore)


' DATO UN VALORE DEL LIVELLO DI CONOSCENZA RESTITUISCE LA DESCRIZIONE DI TALE VALORE
' ACCETTA UN *INTERO* COME PARAMETRO
' RESTITUISCE UNA *STRINGA*
'	dim sDescrizione
'	select case iValore
'		case 0
'			sDescrizione = "Nulo"
'		case 1
'			sDescrizione = "Suficiente"
'		case 2
'			sDescrizione = "Discreto"
'		case 3
'			sDescrizione = "Bueno"
'		case 4
'			sDescrizione = "Optimo"
'	end select
'	decGradoConosc = sDescrizione

end function

function decodTadesToArrayCod(sTabella,dData,sCondizione,nOrder,Isa)
' sTabella		-- > Nome della Tabella
' dData			-- > Data validit� della query.
' sCondizione	-- > Eventuale condizione per la query
'				-- La stringa deve contenere [CAMPO = VALORE AND ......]
'-----------------------------------------------------------------------
' L'array conterra sar� strutturato cos� : CODICE.
' Nel caso non ci siano elementi sar� impostato a null'.
'-----------------------------------------------------------------------
'-----------------------------------------------------------------------
' Ricordarsi sempre di distruggere l'oggetto con la funzione Erase 
' dopo averne terminato l'utilizzo nella pagina che utilizza questa funzione
' Esempio : Erase nome_array
'-----------------------------------------------------------------------

	dim sIsa
	dim sSQL
	dim sOrder
	dim outputArray()
	dim nEle

	select case nOrder
		case 0
			sOrder = " ORDER BY DESCRIZIONE"
		case 1
			sOrder = " ORDER BY CODICE"
		case 2
			sOrder = " ORDER BY VALORE"
	end select

	if isa = "0" then
		sIsa = " Isa = '0'"
	else
		sIsa = " Isa IN ('0','" & isa & "')"
	end if

	if sTabelle="PRVEX" then
		sSQL = "SELECT CODICE FROM TADES with (nolock) WHERE NOME_TABELLA in ('PROV','PRVEX')" &_
			" AND " & ConvDateToDBs(dData) & " BETWEEN DECORRENZA AND SCADENZA" &_
			" AND " & sIsa
		select case nOrder
			case 0
				sOrder = " ORDER BY NOME_TABELLA, DESCRIZIONE"
			case 1
				sOrder = " ORDER BY NOME_TABELLA, CODICE"
			case 2
				sOrder = " ORDER BY NOME_TABELLA, VALORE"
		end select	
			
	else
		sSQL = "SELECT CODICE FROM TADES with (nolock) WHERE NOME_TABELLA ='" &_
			sTabella & "' AND " & ConvDateToDBs(dData) & " BETWEEN DECORRENZA AND SCADENZA" &_
			" AND " & sIsa
			
		select case nOrder
			case 0
				sOrder = " ORDER BY NOME_TABELLA, DESCRIZIONE"
			case 1
				sOrder = " ORDER BY NOME_TABELLA, CODICE"
			case 2
				sOrder = " ORDER BY NOME_TABELLA, VALORE"
		end select	
	end if
	
	' Se indico la condizione la applico alla query.
	if sCondizione <> "" then
		sSQL = sSQL & " AND " & sCondizione 
	end if

	sSQL = sSQL & sOrder

	set rsDecTades = Server.CreateObject("ADODB.Recordset")
'PL-SQL * T-SQL  
' Tolta per ottimizzare l'uso di risorse sql

'SSQL = TransformPLSQLToTSQL (SSQL) 

	'response.write ssql
    'response.end
	rsDecTades.open sSQL,CC,3
	nEle = rsDecTades.RecordCount

	if not rsDecTades.EOF then
		redim outputArray(nEle)
		i = 0
		do while not rsDecTades.EOF
			outputArray(i) = rsDecTades("CODICE")
 			i=i+1
			rsDecTades.MoveNext()
		loop
	else
		redim outputArray(0)
		outputArray(0) = null
	end if 
	rsDecTades.Close()
	set rsDecTades = nothing

	decodTadesToArrayCod = outputArray
	Erase outputArray

end function

'*********** inizio 30/10/2003
function decodTadesToArrayString(sTabella,dData,sCondizione,nOrder,Isa)

' sTabella		-- > Nome della Tabella
' dData			-- > Data validit� della query.
' sCondizione	-- > Eventuale condizione per la query
'			-- La stringa deve contenere [CAMPO = VALORE AND ......]
' nOrder		-- > Tipo di ordinamento pu� assumere questi 3 valori :
'				0 -- per descrizione;
'				1 -- per codice;
'				2 -- per valore;
'-----------------------------------------------------------------------
' La Stringa sar� strutturata cos� : CODICE, DESCRIZIONE, VALORE
'-----------------------------------------------------------------------

	dim sIsa
	dim sSQL
	dim sOrder
		
	select case nOrder
		case 0
			sOrder = " ORDER BY DESCRIZIONE"
		case 1
			sOrder = " ORDER BY CODICE"
		case 2
			sOrder = " ORDER BY VALORE"
	end select

	if isa = "0" then
		sIsa = " Isa = '0'"
	else
		sIsa = " Isa IN ('0','" & isa & "')"
	end if

	if sTabella="PRVEX" then
		sSQL = "SELECT CODICE,DESCRIZIONE,VALORE FROM TADES with (nolock) WHERE NOME_TABELLA in('PROV','PRVEX')" &_
				" AND " & ConvDateToDBs(dData) & " BETWEEN DECORRENZA AND SCADENZA" &_
				" AND " & sIsa

						
		select case nOrder
			case 0
				sOrder = " ORDER BY NOME_TABELLA, DESCRIZIONE"
			case 1
				sOrder = " ORDER BY NOME_TABELLA, CODICE"
			case 2
				sOrder = " ORDER BY NOME_TABELLA, VALORE"
		end select
	else
		sSQL = "SELECT CODICE,DESCRIZIONE,VALORE FROM TADES with (nolock) WHERE NOME_TABELLA ='" &_
				sTabella & "' AND " & ConvDateToDBs(dData) & " BETWEEN DECORRENZA AND SCADENZA" &_
				" AND " & sIsa
				
		select case nOrder
			case 0
				sOrder = " ORDER BY NOME_TABELLA, DESCRIZIONE"
			case 1
				sOrder = " ORDER BY NOME_TABELLA, CODICE"
			case 2
				sOrder = " ORDER BY NOME_TABELLA, VALORE"
		end select		
	end if

	' Se indico la condizione la applico alla query.
	if sCondizione <> "" then
		sSQL = sSQL & " AND " & sCondizione 
	end if

	sSQL = sSQL & sOrder
	
	set rsDecTades = Server.CreateObject("ADODB.Recordset")
'PL-SQL * T-SQL  
' Tolta per ottimizzare l'uso di risorse sql
'SSQL = TransformPLSQLToTSQL (SSQL) 
	rsDecTades.open sSQL,CC,3
	
	sStringa = ""
	if not rsDecTades.EOF then
		conta = 0
		do while not rsDecTades.EOF
		if conta = 0 then
		   sSTringa = sSTringa &  trim(rsDecTades("CODICE")) & "|" & trim(rsDecTades("DESCRIZIONE")) & "|" & trim(rsDecTades("VALORE"))
		   conta = conta + 1
		else
		   sSTringa = sSTringa & "#" & trim(rsDecTades("CODICE")) & "|" & trim(rsDecTades("DESCRIZIONE")) & "|" & trim(rsDecTades("VALORE"))		
		end if	
			rsDecTades.MoveNext()
		loop
	end if 
	
	decodTadesToArrayString = sStringa
	rsDecTades.Close()
	set rsDecTades = nothing
end function


'************ fine 30/10/2003
function decodTadesToArrayIsaAll(sTabella,dData,sCondizione,nOrder,Isa)
' sTabella		-- > Nome della Tabella
' dData			-- > Data validit� della query.
' sCondizione	-- > Eventuale condizione per la query
'				-- La stringa deve contenere [CAMPO = VALORE AND ......]
' nOrder		-- > Tipo di ordinamento pu� assumere questi 3 valori :
'				0 -- per descrizione;
'				1 -- per codice;
'				2 -- per valore;
'-----------------------------------------------------------------------
' L'array conterra sar� strutturato cos� : CODICE, DESCRIZIONE, VALORE, ISA
' Nel caso non ci siano elementi sar� impostato decodTadesToArray(0,0,0,0)=null'
'-----------------------------------------------------------------------
'-----------------------------------------------------------------------
' Ricordarsi sempre di distruggere l'oggetto con la funzione Erase 
' dopo averne terminato l'utilizzo nella pagina che utilizza questa funzione
' Esempio : Erase nome_array
'-----------------------------------------------------------------------

	dim sIsa
	dim sSQL
	dim sOrder
	dim outputArray()
	dim nEle
'	Response.Write sTabella & "-" & dData & "-" & sCondizione & "-" & nOrder & "-" & Isa
	select case nOrder
		case 0
			sOrder = " ORDER BY DESCRIZIONE"
		case 1
			sOrder = " ORDER BY CODICE"
		case 2
			sOrder = " ORDER BY VALORE"
		case 3 
			sOrder = " ORDER BY ISA, CODICE"
	end select

	if isa = "0" then
		sIsa = " and Isa = '0'"
	elseif isa = "" then
		sIsa = " "		
	else
		sIsa = " Isa IN ('0','" & isa & "')"
	end if

	
	if sTabella="PRVEX" then
		sSQL = "SELECT CODICE,DESCRIZIONE,VALORE,ISA FROM TADES with (nolock) WHERE NOME_TABELLA  in ('PROV','PRVEX')" &_
			 " " & sIsa
		select case nOrder
			case 0
				sOrder = " ORDER BY NOME_TABELLA, DESCRIZIONE"
			case 1
				sOrder = " ORDER BY NOME_TABELLA, CODICE"
			case 2
				sOrder = " ORDER BY NOME_TABELLA, VALORE"
			case 3 
				sOrder = " ORDER BY ISA, CODICE"
		end select
			
	else

		sSQL = "SELECT CODICE,DESCRIZIONE,VALORE,ISA FROM TADES with (nolock) WHERE NOME_TABELLA ='" &_
			sTabella & "' " & sIsa
			
' IF Agregado para que ordene los dias en Disponibilidad,  el original esta dentro del ELSE
		if sTabella = "TDISP" then
			select case nOrder
					case 0
						sOrder = " ORDER BY NOME_TABELLA, DESCRIZIONE"
					case 1
						sOrder = " ORDER BY NOME_TABELLA, CODICE"
					case 2
						sOrder = " ORDER BY NOME_TABELLA, VALORE, CODICE"
					case 3 
						sOrder = " ORDER BY ISA, CODICE"
			end select			
		Else						
			select case nOrder
					case 0
						sOrder = " ORDER BY NOME_TABELLA, DESCRIZIONE"
					case 1
						sOrder = " ORDER BY NOME_TABELLA, CODICE"
					case 2
						sOrder = " ORDER BY NOME_TABELLA, VALORE"
					case 3 
						sOrder = " ORDER BY ISA, CODICE"
			end select		
		End if
				
	end if
	' Se indico la condizione la applico alla query.
	if sCondizione <> "" then
		sSQL = sSQL & " AND " & sCondizione 
	end if

	sSQL = sSQL & sOrder
 	set rsDecTades = Server.CreateObject("ADODB.Recordset")
 	
 	'Response.Write sSql & "<BR>"
	'Response.Write SSQL 
	'Response.End 
			
'PL-SQL * T-SQL  
' Tolta per ottimizzare l'uso di risorse sql
'SSQL = TransformPLSQLToTSQL (SSQL)
'Response.Write ssql
	rsDecTades.open sSQL,CC
	nEle = rsDecTades.RecordCount
	
	if not rsDecTades.EOF then
		redim outputArray(nEle,nEle,nEle,nEle)
		i = 0
		do while not rsDecTades.EOF
			outputArray(i, i, i, i)		= trim(rsDecTades("CODICE"))
			outputArray(i,i+1,i, i)	= trim(rsDecTades("DESCRIZIONE"))
			outputArray(i,i+1,i+1, i)	= trim(rsDecTades("VALORE"))
			outputArray(i, i+1 ,i+1, i+1)	= trim(rsDecTades("ISA"))
			i=i+1
			rsDecTades.MoveNext()
		loop
	else
		redim outputArray(0,0,0,0)
		outputArray(0,0,0, 0) = null
	end if 
	rsDecTades.Close()
	set rsDecTades = nothing

	decodTadesToArrayIsaAll = outputArray
	Erase outputArray

end function


''''agregado VAL, 29 de diciembre 2005
sub  CreateComboDisabled(sStringa)
	dim sValoriArray
	dim rsRec
	dim sSQL
	dim sIsa
	
	sValoriArray = Split(sStringa, "|", -1, 1)

'	Struttura variabile [ sValoriArray ]:
'		sValoriArray(0) = Nome della tabella
'		sValoriArray(1) = Isa 
'		sValoriArray(2) = Data di validit�
'		sValoriArray(3) = Valore da selezionare
'		sValoriArray(4) = Nome del Combo
'		sValoriArray(5) = Condizione

	if sValoriArray(1) = "0" then
		sIsa = " Isa = '0'"
	else
		sIsa = " Isa in ('0','" & sValoriArray(1) & "')"
	end if
	
	' Se non passo la decorrenza la imposta con la data di sistema
	if sValoriArray(2) = "" then
		sValoriArray(2) = date
	end if
	
	if sValoriArray(0)="PRVEX" then
		sSQL = "SELECT Nome_Tabella, Codice, Decorrenza,Scadenza, " &_
			"Descrizione,Valore FROM TADES with (nolock) WHERE Nome_Tabella in ('PROV','PRVEX')" &_
			" AND" & sIsa & " AND " & ConvDateToDB(sValoriArray(2)) &_
			" BETWEEN Decorrenza AND Scadenza"
	else
		sSQL = "SELECT Nome_Tabella, Codice, Decorrenza,Scadenza, " &_
			"Descrizione,Valore FROM TADES with (nolock) WHERE Nome_Tabella ='" &_
			sValoriArray(0) &  "' AND" & sIsa & " AND " & ConvDateToDB(sValoriArray(2)) &_
			" BETWEEN Decorrenza AND Scadenza"
	end if
	
	if Trim(sValoriArray(5)) <> "" then
		sSQL = sSQL & " " & sValoriArray(5) 
	end if

'PL-SQL * T-SQL  
' Tolta per ottimizzare l'uso di risorse sql
'SSQL = TransformPLSQLToTSQL (SSQL) 
	set rsRec = CC.Execute(sSQL)
	
	' creo la Combo Box con i dati che query ha ritornato.
	Response.Write "<SELECT ID='" & _
			sValoriArray(4) & "' disabled class=textblack name='" & sValoriArray(4) & "'><OPTION value=></OPTION>"

	do until rsRec.EOF
		Response.Write "<OPTION "

		' Ricerca il record della combo da evidenziare.
		if sValoriArray(3) = rsRec.Fields("Codice") then
			Response.Write "selected "
		end if
		
		Response.write "value ='" & rsRec.Fields("Codice") & _
			"'> " & rsRec.Fields("Descrizione")  & "</OPTION>"
		rsRec.MoveNext 
	loop
	
	Response.Write "</SELECT>"
	rsRec.Close
	set rsRec = Nothing
end sub


sub  CreateComboProvinciaComp(sStringa)
	dim sValoriArray
	dim rsRec
	dim sSQL
	dim sIsa
	
	sValoriArray = Split(sStringa, "|", -1, 1)

'	Struttura variabile [ sValoriArray ]:
'		sValoriArray(0) = Nome della tabella
'		sValoriArray(1) = Isa 
'		sValoriArray(2) = Data di validit�
'		sValoriArray(3) = Valore da selezionare
'		sValoriArray(4) = Nome del Combo
'		sValoriArray(5) = Condizione

	if sValoriArray(1) = "0" then
		sIsa = " Isa = '0'"
	else
		sIsa = " Isa in ('0','" & sValoriArray(1) & "')"
	end if
	
	' Se non passo la decorrenza la imposta con la data di sistema
	if sValoriArray(2) = "" then
		sValoriArray(2) = date
	end if
	
	if sValoriArray(0)="PRVEX" then
		sSQL = "SELECT Nome_Tabella, Codice, Decorrenza,Scadenza, " &_
			"Descrizione,Valore FROM TADES with (nolock) WHERE Nome_Tabella in ('PROV','PRVEX')" &_
			" AND" & sIsa & " AND " & ConvDateToDB(sValoriArray(2)) &_
			" BETWEEN Decorrenza AND Scadenza"
	else
		sSQL = "SELECT Nome_Tabella, Codice, Decorrenza,Scadenza, " &_
			"Descrizione,Valore FROM TADES with (nolock) WHERE Nome_Tabella ='" &_
			sValoriArray(0) &  "' AND" & sIsa & " AND " & ConvDateToDB(sValoriArray(2)) &_
			" BETWEEN Decorrenza AND Scadenza"
	end if
	
	if Trim(sValoriArray(5)) <> "" then
		sSQL = sSQL & " " & sValoriArray(5) 
	end if

'PL-SQL * T-SQL  
' Tolta per ottimizzare l'uso di risorse sql
'SSQL = TransformPLSQLToTSQL (SSQL) 
	set rsRec = CC.Execute(sSQL)
	
	' creo la Combo Box con i dati che query ha ritornato.
	if sValoriArray(3) = "TODAS" then 
		seleccionado = "selected"
	else 
		seleccionado = ""
	end if 
	
	Response.Write "<SELECT ID='" & _
			sValoriArray(4) & "' class=textblack name='" & sValoriArray(4) & "'><OPTION value=''></OPTION><OPTION value='TODAS' " & seleccionado & ">TODAS</OPTION>"

	do until rsRec.EOF
		Response.Write "<OPTION "

		' Ricerca il record della combo da evidenziare.
		if sValoriArray(3) = rsRec.Fields("Codice") then
			Response.Write "selected "
		end if
		
		Response.write "value ='" & rsRec.Fields("Codice") & _
			"'> " & rsRec.Fields("Descrizione")  & "</OPTION>"
		rsRec.MoveNext 
	loop
	
	Response.Write "</SELECT>"
	rsRec.Close
	set rsRec = Nothing
end sub


sub CreateComboDizDati(Tabla,Campo,Nombre,Formato,Validacion,Seleccion)

	Dim mirs
	Dim consulta 
	Dim mistring
	Dim miarray 
	Dim y 
					
					
	set mirs = server.CreateObject("adodb.recordset")
				
	consulta = "Select ID_CAMPO_DESC from DIZ_DATI with (nolock) where ID_TAB = '" & Tabla & "' and ID_CAMPO = '" & Campo & "'"
				
 
	
'PL-SQL * T-SQL  
' Tolta per ottimizzare l'uso di risorse sql
'CONSULTA = TransformPLSQLToTSQL (CONSULTA) 
	set mirs = CC.execute(consulta)
					
	if not mirs.EOF then 
		mistring = mirs("ID_CAMPO_DESC")
		miarray = split(mistring,"|")
		Response.Write "<select name='" & Nombre & "' class='" & Formato & "' onchange='" & Validacion & "'>"
		for y = lbound(miarray) to ubound(miarray) step 2
			if Seleccion = miarray(y) then 
				Response.Write "<option value = '" & miarray(y) & "' selected>" & miarray(y + 1) & "</option>"			
			else 
				Response.Write "<option value = '" & miarray(y) & "'>" & miarray(y + 1) & "</option>"
			end if
		next	
		Response.Write "</select>"
	end if 
		
	set mirs = nothing

end sub
sub CreateComboDizDatiNull(Tabla,Campo,Nombre,Formato,Validacion,Seleccion)

	Dim mirs
	Dim consulta 
	Dim mistring
	Dim miarray 
	Dim y 
					
					
	set mirs = server.CreateObject("adodb.recordset")
				
	consulta = "Select ID_CAMPO_DESC from DIZ_DATI with (nolock) where ID_TAB = '" & Tabla & "' and ID_CAMPO = '" & Campo & "'"
				
 
	
'PL-SQL * T-SQL  
' Tolta per ottimizzare l'uso di risorse sql
'CONSULTA = TransformPLSQLToTSQL (CONSULTA) 
	set mirs = CC.execute(consulta)
					
	if not mirs.EOF then 
		mistring = mirs("ID_CAMPO_DESC")
		miarray = split(mistring,"|")
		Response.Write "<select name='" & Nombre & "' class='" & Formato & "' onchange='" & Validacion & "'>"
		Response.Write "<option value = ''></option>"			
		for y = lbound(miarray) to ubound(miarray) step 2
			if Seleccion = miarray(y) then 
				Response.Write "<option value = '" & miarray(y) & "' selected>" & miarray(y + 1) & "</option>"			
			else 
				Response.Write "<option value = '" & miarray(y) & "'>" & miarray(y + 1) & "</option>"
			end if
		next	
		Response.Write "</select>"
	end if 
		
	set mirs = nothing

end sub


function DecCodValDizDati(Tabla,Campo,Codigo)

	DecCodValDizDati = ""
		
	Dim mirs
	Dim consulta 
	Dim mistring
	Dim miarray 
	Dim y 

	set mirs = server.CreateObject("adodb.recordset")
				
	consulta = "Select ID_CAMPO_DESC from DIZ_DATI with (nolock) where ID_TAB = '" & Tabla & "' and ID_CAMPO = '" & Campo & "'"
				
	
'PL-SQL * T-SQL  
' Tolta per ottimizzare l'uso di risorse sql
'CONSULTA = TransformPLSQLToTSQL (CONSULTA) 
	set mirs = CC.execute(consulta)
					
	if not mirs.EOF then 
		mistring = mirs("ID_CAMPO_DESC")
		miarray = split(mistring,"|")
		
		for y = lbound(miarray) to ubound(miarray) step 2
			if cstr(miarray(y)) = Cstr(Codigo) then 
				DecCodValDizDati = miarray(y+1)
				exit for
			end if
		next
	end if 
		
	set mirs = nothing
end function


sub  CreateComboSinBlanco(sStringa)
	dim sValoriArray
	dim rsRec
	dim sSQL
	dim sIsa
	
	sValoriArray = Split(sStringa, "|", -1, 1)

'	Struttura variabile [ sValoriArray ]:
'		sValoriArray(0) = Nome della tabella
'		sValoriArray(1) = Isa 
'		sValoriArray(2) = Data di validit�
'		sValoriArray(3) = Valore da selezionare
'		sValoriArray(4) = Nome del Combo
'		sValoriArray(5) = Condizione

	if sValoriArray(1) = "0" then
		sIsa = " Isa = '0'"
	else
		sIsa = " Isa in ('0','" & sValoriArray(1) & "')"
	end if
	
	' Se non passo la decorrenza la imposta con la data di sistema
	if sValoriArray(2) = "" then
		sValoriArray(2) = date
	end if
	
	if sValoriArray(0)="PRVEX" then
		sSQL = "SELECT Nome_Tabella, Codice, Decorrenza,Scadenza, " &_
			"Descrizione,Valore FROM TADES with (nolock) WHERE Nome_Tabella in ('PROV','PRVEX')" &_
			" AND" & sIsa & " AND " & ConvDateToDB(sValoriArray(2)) &_
			" BETWEEN Decorrenza AND Scadenza"
	else
		sSQL = "SELECT Nome_Tabella, Codice, Decorrenza,Scadenza, " &_
			"Descrizione,Valore FROM TADES with (nolock) WHERE Nome_Tabella ='" &_
			sValoriArray(0) &  "' AND" & sIsa & " AND " & ConvDateToDB(sValoriArray(2)) &_
			" BETWEEN Decorrenza AND Scadenza"
	
	end if
	
	if Trim(sValoriArray(5)) <> "" then
		sSQL = sSQL & " " & sValoriArray(5) 
	end if

'PL-SQL * T-SQL  
' Tolta per ottimizzare l'uso di risorse sql
'SSQL = TransformPLSQLToTSQL (SSQL) 
	set rsRec = CC.Execute(sSQL)
	
	' creo la Combo Box con i dati che query ha ritornato.
	Response.Write "<SELECT ID='" & _
			sValoriArray(4) & "' class=textblack name='" & sValoriArray(4) & "'>"

	do until rsRec.EOF
		
		
		Response.Write "<OPTION "

		' Ricerca il record della combo da evidenziare.
		if sValoriArray(3) = rsRec.Fields("Codice") then
			Response.Write "selected "
		end if
		
		Response.write "value ='" & rsRec.Fields("Codice") & _
			"'> " & rsRec.Fields("Descrizione")  & "</OPTION>"
		response.Write value
		rsRec.MoveNext 
	loop
	
	Response.Write "</SELECT>"
	rsRec.Close
	set rsRec = Nothing
end sub


''''




sub  CreateComboDesdeSQL(ssql,nombrecombo,estilo,seleccion,funcion,tamanio,conexion)    

'''vany 09042007

    dim rsRec
    
    set rsRec = server.CreateObject("adodb.recordset")
    
    set xconex = conexion
    
	set rsRec = xconex.Execute(sSQL)
	
	' creo la Combo Box con i dati che query ha ritornato.
	Response.Write "<SELECT style='width:" & tamanio & "' ID='" & nombrecombo & "' class='" & estilo & "' name='" & nombrecombo & "'" & funcion & "><OPTION value=''></OPTION>"

	do until rsRec.EOF
		Response.Write "<OPTION "

		' Ricerca il record della combo da evidenziare.
		if cstr(seleccion) = cstr(rsRec.Fields(0)) then
			Response.Write "selected "
		end if
		
		Response.write "value ='" & rsRec.Fields(0) & _
			"'> " & rsRec.Fields(1)  & "</OPTION>"
		rsRec.MoveNext 
	loop
	
	Response.Write "</SELECT>"
	rsRec.Close
	set rsRec = Nothing
end sub
%>
