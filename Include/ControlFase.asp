<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%

'-----------------------------------------------------------------
'Questa funzione restituisce la fase che il vincitore di un bando
'sta svolgendo in un determinato periodo 
'-----------------------------------------------------------------
function SelPrgFase1(idPers)
    
    dim sSelezione,sSQLFase
    dim rsFase

'    sSQLFase = "SELECT D.COD_SESSIONE,PRG_FASE FROM " &_
'		"COMPONENTI_CLASSE A, CLASSE_PERCORSO b, EDIZIONE C," &_
'       "FASE_BANDO D WHERE ID_PERSONA =" & idPers &_
'       " AND NVL(DT_RUOLOAL,SYSDATE) <= SYSDATE " &_
'       "AND A.ID_CLASSE=B.ID_CLASSE " &_
'       "AND NVL(DT_RUOLOAL,SYSDATE) BETWEEN DT_INISESS AND DT_FINSESS " &_
'       "AND B.ID_EDIZIONE=C.ID_EDIZIONE " &_ 
'       "AND D.ID_BANDO=C.ID_BANDO" 

'
' Mario Leonetti 26/10/2004 Gestione errata data odierna
'
    sSQLFase = "SELECT D.COD_SESSIONE,PRG_FASE FROM " &_
		"COMPONENTI_CLASSE A, CLASSE_PERCORSO b, EDIZIONE C," &_
        "FASE_BANDO D WHERE ID_PERSONA =" & idPers &_
        " AND NVL(DT_RUOLOAL," & ConvDateToDbs(date()) & ") <= " & ConvDateToDbs(date()) &_
        " AND A.ID_CLASSE=B.ID_CLASSE " &_
        " AND NVL(DT_RUOLOAL," & ConvDateToDbs(date()) & ") BETWEEN DT_INISESS AND DT_FINSESS " &_
        " AND B.ID_EDIZIONE=C.ID_EDIZIONE " &_ 
        " AND D.ID_BANDO=C.ID_BANDO" 
    

	set rsFase = Server.CreateObject("ADODB.Recordset")
'PL-SQL * T-SQL  
SSQLFASE = TransformPLSQLToTSQL (SSQLFASE) 
	rsFase.Open sSQLFase,CC,3
    if not rsFase.EOF then
		sSelezione = rsFase("PRG_FASE")
	else
		sSelezione = 0 
    end if 
    rsFase.close
    set rsFase= nothing 
    SelPrgFase = sSelezione     

end function

'-----------------------------------------------------------------
'Questa funzione restituisce la fase che il vincitore di un bando
'sta svolgendo in un determinato periodo 
'-----------------------------------------------------------------
function SelPrgFase(idPers)
    
    dim sSelezione,sSQLFase
    dim aSelezione(3)
    dim rsFase

'    sSQLFase = "SELECT D.COD_SESSIONE,PRG_FASE FROM " &_
'        "COMPONENTI_CLASSE A, CLASSE_PERCORSO b, EDIZIONE C," &_
'       "FASE_BANDO D WHERE ID_PERSONA =" & idPers &_
'         " AND NVL(DT_RUOLOAL,SYSDATE) <= SYSDATE " &_
'         "AND A.ID_CLASSE=B.ID_CLASSE " &_
'         "AND NVL(DT_RUOLOAL,SYSDATE) BETWEEN DT_INISESS AND DT_FINSESS " &_
'         "AND B.ID_EDIZIONE=C.ID_EDIZIONE " &_ 
'         "AND D.ID_BANDO=C.ID_BANDO" 
    
'   sSQLFase = " SELECT D.COD_SESSIONE,PRG_FASE,DT_INISESS,DT_FINSESS "
'	sSQLFase = sSQLFase &  " FROM COMPONENTI_CLASSE A, "
'	sSQLFase = sSQLFase &  "  CLASSE_PERCORSO B, "
'	sSQLFase = sSQLFase &  "  CLASSE C,"
'	sSQLFase = sSQLFase &  "  FASE_BANDO D "
'	sSQLFase = sSQLFase &  " WHERE ID_PERSONA = " & idPers
'	sSQLFase = sSQLFase &  " AND NVL( DT_RUOLOAL, SYSDATE ) <= SYSDATE "
'	sSQLFase = sSQLFase &  " AND A.ID_CLASSE = B.ID_CLASSE "
'	sSQLFase = sSQLFase &  " AND NVL( DT_RUOLOAL, SYSDATE ) BETWEEN DT_INISESS AND DT_FINSESS "
'	sSQLFase = sSQLFase &  " AND SYSDATE BETWEEN DT_INISESS AND DT_FINSESS"
'	sSQLFase = sSQLFase &  " AND B.ID_CLASSE = C.ID_CLASSE "
'	sSQLFase = sSQLFase &  " AND D.ID_BANDO=C.ID_BANDO"
'	sSQLFase = sSQLFase &  " AND B.COD_SESSIONE=D.COD_SESSIONE"

	dOggi = ConvDateToDbs(date())

	sSQLFase = "  SELECT B.COD_SESSIONE,PRG_FASE,DT_INISESS,DT_FINSESS "
	sSQLFase = sSQLFase &  " FROM COMPONENTI_CLASSE A, CLASSE_PERCORSO B, CLASSE C "
	sSQLFase = sSQLFase &  " WHERE ID_PERSONA = " & idPers
	sSQLFase = sSQLFase &  " AND NVL(DT_RUOLOAL," & dOggi & " ) <= " & dOggi
	sSQLFase = sSQLFase &  " AND A.ID_CLASSE = B.ID_CLASSE "
	sSQLFase = sSQLFase &  " AND NVL(DT_RUOLOAL," & dOggi & ") BETWEEN DT_INISESS AND DT_FINSESS "
	sSQLFase = sSQLFase &  " AND " & dOggi & " BETWEEN DT_INISESS AND DT_FINSESS"
	sSQLFase = sSQLFase &  " AND B.ID_CLASSE = C.ID_CLASSE  ORDER BY PRG_FASE DESC"

	set rsFase = Server.CreateObject("ADODB.Recordset")
'PL-SQL * T-SQL  
SSQLFASE = TransformPLSQLToTSQL (SSQLFASE) 
	rsFase.Open sSQLFase,CC,3
    if not rsFase.EOF then
'       sSelezione= clng(rsFase("PRG_FASE"))
		aSelezione(0) = CLng(rsFase("PRG_FASE")) 
		aSelezione(1) = rsFase("COD_SESSIONE")
		aSelezione(2) = rsFase("DT_INISESS")
		aSelezione(3) = rsFase("DT_FINSESS")
	else
  		aSelezione(0) = 0
		aSelezione(1) = ""
		aSelezione(2) = ""
		aSelezione(3) = ""
    end if 
    rsFase.close
    set rsFase= nothing 
    SelPrgFase = aSelezione     

end function

'-----------------------------------------------------------------
'Questa funzione restituisce la fase che il vincitore di un bando
'dovr� svolgere alla data odierna.
'-----------------------------------------------------------------
function SelPrgFaseSucc(idPers)
    
    dim sSelezione,sSQLFase
    dim aSelezione(3)
    dim rsFase
'
' Mario Leonetti 26/10/2004 Gestione errata data odierna
'
'	sSQLFase = "SELECT D.COD_SESSIONE,B.PRG_FASE,DT_INISESS,DT_FINSESS "
'	sSQLFase = sSQLFase &  "  FROM COMPONENTI_CLASSE A, "
'	sSQLFase = sSQLFase &  "   CLASSE_PERCORSO B, "
'	sSQLFase = sSQLFase &  "   CLASSE C,"
'	sSQLFase = sSQLFase &  "   FASE_BANDO D "
'	sSQLFase = sSQLFase &  "  WHERE ID_PERSONA = " & idPers
'	sSQLFase = sSQLFase &  "  AND NVL( DT_RUOLOAL, SYSDATE ) <= SYSDATE "
'	sSQLFase = sSQLFase &  "  AND A.ID_CLASSE = B.ID_CLASSE "
'	sSQLFase = sSQLFase &  "  AND NVL( DT_RUOLOAL, SYSDATE ) < DT_INISESS "
'	sSQLFase = sSQLFase &  "  AND NVL( DT_RUOLOAL, SYSDATE ) < DT_FINSESS "
'	sSQLFase = sSQLFase &  "  AND B.ID_CLASSE = C.ID_CLASSE "
'	sSQLFase = sSQLFase &  "  AND D.ID_BANDO=C.ID_BANDO"
'	sSQLFase = sSQLFase &  "  AND B.COD_SESSIONE=D.COD_SESSIONE"
'	sSQLFase = sSQLFase &  "  ORDER BY DT_FINSESS"

	sSQLFase = "SELECT D.COD_SESSIONE,B.PRG_FASE,DT_INISESS,DT_FINSESS "
	sSQLFase = sSQLFase &  " FROM COMPONENTI_CLASSE A, CLASSE_PERCORSO B, CLASSE C, FASE_BANDO D "
	sSQLFase = sSQLFase &  " WHERE ID_PERSONA = " & idPers
	sSQLFase = sSQLFase &  " AND NVL(DT_RUOLOAL," & ConvDateToDbs(date()) & ") <=" &  ConvDateToDbs(date())
	sSQLFase = sSQLFase &  " AND A.ID_CLASSE = B.ID_CLASSE "
	sSQLFase = sSQLFase &  " AND NVL(DT_RUOLOAL," & ConvDateToDbs(date()) & ") < DT_INISESS"
	sSQLFase = sSQLFase &  " AND NVL(DT_RUOLOAL," & ConvDateToDbs(date()) & ") < DT_FINSESS"
	sSQLFase = sSQLFase &  " AND B.ID_CLASSE = C.ID_CLASSE "
	sSQLFase = sSQLFase &  " AND D.ID_BANDO=C.ID_BANDO"
	sSQLFase = sSQLFase &  " AND B.COD_SESSIONE=D.COD_SESSIONE"
	sSQLFase = sSQLFase &  " ORDER BY DT_FINSESS"

	set rsFase = Server.CreateObject("ADODB.Recordset")
'PL-SQL * T-SQL  
SSQLFASE = TransformPLSQLToTSQL (SSQLFASE) 
	rsFase.Open sSQLFase,CC,3
    if not rsFase.EOF then
'       sSelezione= clng(rsFase("PRG_FASE"))
		aSelezione(0) = CLng(rsFase("PRG_FASE"))
		aSelezione(1) = rsFase("COD_SESSIONE")
		aSelezione(2) = rsFase("DT_INISESS")
		aSelezione(3) = rsFase("DT_FINSESS")
	else
  		aSelezione(0) = 0
		aSelezione(1) = ""
		aSelezione(2) = ""
		aSelezione(3) = ""
    end if 
    rsFase.close
    set rsFase= nothing 
    SelPrgFaseSucc = aSelezione     

end function

'-----------------------------------------------------------------
'Questa funzione restituisce la fase che il vincitore di un bando
'sta svolgendo in un determinato periodo 
'-----------------------------------------------------------------
function SelPrgFaseClasse(idClasse)
    
    dim sSelezione,sSQLFase
    dim aSelezione(3)
    dim rsFase

'
' Mario Leonetti 26/10/2004 Gestione errata data odierna
'
'	sSQLFase = "  SELECT B.COD_SESSIONE,PRG_FASE,DT_INISESS,DT_FINSESS "
'	sSQLFase = sSQLFase &  "  FROM COMPONENTI_CLASSE A, "
'	sSQLFase = sSQLFase &  "   CLASSE_PERCORSO B, "
'	sSQLFase = sSQLFase &  "   CLASSE C "
'	sSQLFase = sSQLFase &  "  WHERE C.ID_CLASSE = " & idClasse
'	sSQLFase = sSQLFase &  "  AND NVL( DT_RUOLOAL, SYSDATE ) <= SYSDATE "
'	sSQLFase = sSQLFase &  "  AND A.ID_CLASSE = B.ID_CLASSE "
'	sSQLFase = sSQLFase &  "  AND NVL( DT_RUOLOAL, SYSDATE ) BETWEEN DT_INISESS AND DT_FINSESS "
'	sSQLFase = sSQLFase &  "  AND SYSDATE BETWEEN DT_INISESS AND DT_FINSESS"
'	sSQLFase = sSQLFase &  "  AND B.ID_CLASSE = C.ID_CLASSE "

	sSQLFase = "  SELECT B.COD_SESSIONE,PRG_FASE,DT_INISESS,DT_FINSESS "
	sSQLFase = sSQLFase &  " FROM COMPONENTI_CLASSE A, CLASSE_PERCORSO B, CLASSE C "
	sSQLFase = sSQLFase &  " WHERE C.ID_CLASSE = " & idClasse
	sSQLFase = sSQLFase &  " AND NVL(DT_RUOLOAL," & ConvDateToDbs(date())& ") <=" & ConvDateToDbs(date())
	sSQLFase = sSQLFase &  " AND A.ID_CLASSE = B.ID_CLASSE "
	sSQLFase = sSQLFase &  " AND NVL(DT_RUOLOAL," & ConvDateToDbs(date()) & ") BETWEEN DT_INISESS AND DT_FINSESS"
	sSQLFase = sSQLFase &  " AND " & ConvDateToDbs(date()) & " BETWEEN DT_INISESS AND DT_FINSESS"
	sSQLFase = sSQLFase &  " AND B.ID_CLASSE = C.ID_CLASSE "

	set rsFase = Server.CreateObject("ADODB.Recordset")
'PL-SQL * T-SQL  
SSQLFASE = TransformPLSQLToTSQL (SSQLFASE) 
	rsFase.Open sSQLFase,CC,3
    if not rsFase.EOF then
'       sSelezione= clng(rsFase("PRG_FASE"))
		aSelezione(0) = CLng(rsFase("PRG_FASE")) 
		aSelezione(1) = rsFase("COD_SESSIONE")
		aSelezione(2) = rsFase("DT_INISESS")
		aSelezione(3) = rsFase("DT_FINSESS")
	else
  		aSelezione(0) = 0
		aSelezione(1) = ""
		aSelezione(2) = ""
		aSelezione(3) = ""
    end if 
    rsFase.close
    set rsFase= nothing 
    SelPrgFaseClasse = aSelezione     

end function
%>
