<%
function DecCampoDesc(sIdTab,sIdCampo)
	dim sSQL
	dim rst 

	sSQL="SELECT ID_CAMPO_DESC " &_
		"FROM DIZ_DATI " &_
		"WHERE " &_
		"ID_TAB='" & sIdTab & "' AND ID_CAMPO='" & sIdCampo & "'"
	set rst=CC.Execute(sSQL)
	if not Rst.eof then
		DecCampoDesc=split(Rst("ID_CAMPO_DESC"),"|")
	else
		DecCampoDesc(0) = null
	end if	
	rst.close
	set rst= nothing
	
end function

sub CreateComboCampoDesc(sIdTab,sIdCampo,sNCombo,sEle,sOnCLick)
	dim sSQL
	dim rst 
	dim i

	sSQL="SELECT ID_CAMPO_DESC " &_
		"FROM DIZ_DATI " &_
		"WHERE " &_
		"ID_TAB='" & sIdTab & "' AND ID_CAMPO='" & sIdCampo & "'"
	set Rst=CC.Execute(sSQL)
	Response.Write "<Select class='textblack' name='" & sNCombo & "'"
	if not IsNull(sOnCLick) then Response.Write " onclick='" & sOnCLick & "'" 
	Response.Write ">" 
	Response.Write "<Option></Option>" 
	if not Rst.eof then
		aDesc=split(Rst("ID_CAMPO_DESC"),"|")
		for i = 0 to UBound(aDesc) step 2
			Response.Write "<Option value='" & aDesc(i) & "'" 
			if aDesc(i) = sEle then Response.Write " selected " 
			Response.Write ">" & aDesc(i+1) & "</Option>" 
		next
	end if	
	Response.Write "</Select>" 
	Rst.close
	set rst= nothing
	erase aDesc
end sub

sub CreateComboDesc(sIdTab,sIdCampo,sNCombo,sEle,sOnCLick,sCodGes)
	dim sSQL
	dim rst 
	dim i

	sSQL="SELECT ID_CAMPO_DESC " &_
		"FROM DIZ_DATI " &_
		"WHERE " &_
		"ID_TAB='" & sIdTab & "' AND ID_CAMPO='" & sIdCampo & "'"
	set Rst=CC.Execute(sSQL)
	Response.Write "<Select class='textblack' name='" & sNCombo & "'"
	if not IsNull(sOnCLick) then Response.Write " onclick='" & sOnCLick & "'" 
	Response.Write ">" 
	Response.Write "<Option></Option>" 
	if not Rst.eof then
		aDesc=split(Rst("ID_CAMPO_DESC"),"|")
		for i = 0 to UBound(aDesc) step 2
			Response.Write "<Option value='" & aDesc(i) & "'" 
			if aDesc(i) = TRIM(sCodGes)then Response.Write " selected " 
			Response.Write ">" & aDesc(i+1) & "</Option>" 
		next
	end if	
	Response.Write "</Select>" 
	Rst.close
	set rst= nothing
	erase aDesc
end sub

function DecodificaCampoDesc(sCodGes)	
	dim sSQL
	dim rst 
	dim i
	DecodificaCampoDesc = ""
	sSQL="SELECT ID_CAMPO_DESC " &_
		"FROM DIZ_DATI " &_
		"WHERE " &_
		"ID_TAB='IMPRESA' AND ID_CAMPO='COD_GESTIONE'"
	set Rst=CC.Execute(sSQL)	
	if not Rst.eof then
		aDesc=split(Rst("ID_CAMPO_DESC"),"|")
		for i = 0 to UBound(aDesc) 
			if aDesc(i) = TRIM(sCodGes)then 
				DecodificaCampoDesc = aDesc(i+1)
			end if	  
		next
	end if
	
	Rst.close
	set rst= nothing
	erase aDesc
end function

%>