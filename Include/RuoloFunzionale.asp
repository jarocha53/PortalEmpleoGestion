<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%
'------------------------------------------------------------------
'questa funzione crea una combo con contenente i ruoli funzionali
'esistenti.
'------------------------------------------------------------------

function CreateRuolo(sStringa)
dim sel
dim sValoriArray
	sValoriArray = Split(sStringa, "|")

'	Struttura variabile [ sValoriArray ]:
'		sValoriArray(0) = Nome del Combo
'       sValoriArray(1) = Opzione(valori ammessi: 0=tutti 1=senza DI)
'		sValoriArray(2) = Valore da selezionare
'		sValoriArray(3) = Azione

if sValoriArray(1)= "" then
   sValoriArray(1)= 0
end if

sValoriArray(2) =  UCase(sValoriArray(2))

Response.Write "<SELECT ID='" & sValoriArray(0) & "' class=textblack" &_
       " name='" & sValoriArray(0) & "'" & sValoriArray(3) &_
       "><OPTION value=></OPTION>"'

if sValoriArray(2) = "CD" then
     sel="selected"
else
     sel =""    
end if    
Response.Write "<option value='CD' " & sel &">CODOCENTE</option>"

if cint(sValoriArray(1))= 0 then
     if sValoriArray(2) = "DI" then
         sel="selected"
     else
         sel =""    
     end if  

     Response.Write "<option value='DI' " & sel &">DISCENTE</option>"
end if

if sValoriArray(2) = "DO" then
     sel="selected"
else
     sel =""    
end if    
Response.Write "<option value='DO' " & sel &">DOCENTE</option>"

if sValoriArray(2) = "ES" then
     sel="selected"
else
     sel =""    
end if    
Response.Write "<option value='ES' " & sel &">ESPERTO</option>"

if sValoriArray(2) = "ME" then
     sel="selected"
else
     sel =""    
end if    
Response.Write "<option value='ME' " & sel &">MENTOR</option>"

if sValoriArray(2) = "TU" then
     sel="selected"
else
     sel =""    
end if    
Response.Write "<option value='TU' " & sel &">TUTOR</option>"

if sValoriArray(2) = "TA" then
     sel="selected"
else
     sel =""    
end if    
Response.Write "<option value='TA' " & sel &">TUTOR AZIENDALE</option>"

if sValoriArray(2) = "TS" then
     sel="selected"
else
     sel =""    
end if    
Response.Write "<option value='TS' " & sel &">TUTOR STAGE</option>"

if sValoriArray(2) = "BK" then
     sel="selected"
else
     sel =""    
end if    
Response.Write "<option value='BK' " & sel &">BACK OFFICE</option>"

if sValoriArray(2) = "PL" then
     sel="selected"
else
     sel =""    
end if    
Response.Write "<option value='PL' " & sel &">OPERATORE PLACEMENT</option>"


Response.Write "</select>"

end function

'------------------------------------------------------------------
'questa funzione restituisce la descrizione del ruolo funzionale
'richiesto
'------------------------------------------------------------------

function DecRuolo(id)
dim Desc

sID = Ucase(id)

Desc = ""

select case sID
   case "CD" 
      Desc = "CODOCENTE"
   case "DI" 
      Desc = "DISCENTE"   
   case "DO" 
      Desc = "DOCENTE"
   case "ES" 
      Desc = "ESPERTO"
   case "ME" 
      Desc = "MENTOR"
   case "TU" 
      Desc = "TUTOR"
   case "TA" 
      Desc = "TUTOR AZIENDALE"     
   case "TS" 
      Desc = "TUTOR STAGE"   
   case "BK" 
      Desc = "BACK OFFICE"   
   case "PL" 
      Desc = "PLACEMENT"        
end select

DecRuolo = Desc   
              
end function


sub CreateGruppo( aValoriArray )
	dim sSQL

'-----------------------------------------------------------------------
' La funzione creare una combobox con la descrizione ed il codice
' del gruppo. La funzione accetta un array che contiene i seguenti valori:
' - aValoriArray(0) = Nome del Combo
' - aValoriArray(1) = Valore da selezionare
' - aValoriArray(2) = Azione
' - aValoriArray(3) = Ruolo Funzionale
'-----------------------------------------------------------------------

	
	sSQL = "SELECT IDGRUPPO,DESGRUPPO FROM GRUPPO WHERE COD_RUOFU = '" & aValoriArray(3) & "'"
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	set rsRuolo = cc.execute(sSQL)
	if not rsRuolo.eof then%>
		<select name=<%=aValoriArray(0)%> class="textBlack" <%=aValoriArray(2)%> >
			<option></option>			
	<%	do while not rsRuolo.eof%>
			<option <% if clng(rsRuolo("IDGRUPPO")) = aValoriArray(1) then  Response.Write " selected " end if %>value="<%=clng(rsRuolo("IDGRUPPO"))%>"><%=rsRuolo("DESGRUPPO")%></option>			
			<%rsRuolo.MoveNext()
		loop%>
		</select>
<%
	else%>
Ning&uacute;n Grupo Asociado 
<%end if
	
	rsRuolo.close
	set rsRuolo = nothing 
	
end sub %>
