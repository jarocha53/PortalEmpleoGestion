<%
' Funzione:	Function CaricaArrayPercorso(sQuery)
'			Costruisce un array a 3 dimensioni partendo da una query
' Input: 	una stringa (sql)
' Output: 	un array a due dimensioni
' Autore:	Marco Attenni
' Data:		25-11-2002
' Modifica:	11-12-2002
Function CaricaArrayPercorso(sQuery)

	n = 1
	dim rsPerc
	dim aPerc()
	dim nPos
	dim nElemSql

	redim aPerc(n,0)

	set rsPerc = CC.execute(sQuery)
	do while not rsPerc.eof
		nPos = nPos +1
		nElemSql = 0
		redim preserve aPerc(n, nPos)
		for i = 0 to n
			if i < n then
				aPerc(i, nPos-1) = rsPerc(nElemSql) &_
					"|" & rsPerc(nElemSql + 1)
			else
				aPerc(i, nPos-1) = rsPerc(nElemSql) &_
					"|" & rsPerc(nElemSql + 1) &_
					"|" & rsPerc(nElemSql + 2)
			end if
			nElemSql = nElemSql + 2
		next 
		rsPerc.movenext
	loop
' Epili 30/01/2003 Modifica per creare un array dimanico 
' in base al numero di elementi della query.

'	do while not rsPerc.eof
'		nPos = nPos +1
'		redim preserve aPerc(3, nPos)
'		aPerc(0, nPos-1) = rsPerc(0) & "|" & rsPerc(1)
'		aPerc(1, nPos-1) = rsPerc(2) & "|" & rsPerc(3) 
'		aPerc(2, nPos-1) = rsPerc(4) & "|" & rsPerc(5) & "|" & rsPerc(6)
'		rsPerc.movenext
'	loop

	rsPerc.close
	set rsPerc = nothing
	CaricaArrayPercorso = aPerc
	
End Function
'************************************************************************************************
' Funzione:	sub Alberatura(aAlbero,fLink)
'			Crea l'alberatura con un dato array e specificando se avere i link 
'			per la eseguzione del corso ed i link per i progressi fatti in un dato
'			corso .
'			fLink = S con link -- fLink = N senza Link; 
'			fInfo = 0 con Link Informativo 1 senza. 
' Input: 	una stringa (sql)
' Output: 	Nulla 
' Autore:	Enrico Pili
' Data:		07-01-2003
' Modifica:	08-01-2003

sub Alberatura(aAlbero,fLink,fInfo)%>

	<table border="0" cellpadding="0" cellspacing="0" width="100%"><%		
	for i = 0 to UBound(aAlbero,2)-1
		if split(aAlbero(1,i),"|")(2) = "T" then
			sTipoCorso = "Test"
		else
			sTipoCorso = "Corso"
		end if
		if aAlbero(0,i) <> sPercFormativo then%>
			<tr>
				<td class="txtLiv1" colspan="2">
				<%=strHTMLEncode(split(aAlbero(0,i),"|")(1))%>
				</td>
			</tr>
		<%end if%>
		<tr>
		<%if ubound(aAlbero,2)-1 > i then%>
			<td WIDTH="6%" background="/images/formazione/albero/accoda.gif">
			&nbsp;</td>
		<%else%>
			<td WIDTH="6%" background="/images/formazione/albero/prossimo.gif">
			&nbsp;</td>
		<%end if%>
			<td class="txtLiv3NoLink">
				&nbsp;&nbsp;

		    <%if fInfo = 0 then%>
				<a class="txtLiv3Link" onmouseover="javascript:window.status='' ; return true" href="javascript:VisRis(<%=split(aAlbero(1,i),"|")(0)%>,'<%=split(aAlbero(1,i),"|")(2)%>')"><img src="<%=Session("Progetto")%>/images/Info.gif" border="0"></a>
			<%end if%>

			<%if fLink = "S" then %>

				<%if split(aAlbero(1,i),"|")(2) = "T" then%>
					<a class="txtLiv3Link" onmouseover="javascript:window.status='' ; return true" href="javascript:frmTest<%=i%>.submit()"><%=strHTMLEncode(split(aAlbero(1,i),"|")(1))%></a>&nbsp;(<%=sTipoCorso%>)
				<%else%>
					<a class="txtLiv3Link" href="Javascript:manda(<%=split(aAlbero(1,i),"|")(0)%>)"><%=strHTMLEncode(split(aAlbero(1,i),"|")(1))%></a>&nbsp;(<%=sTipoCorso%>)
				<%end if
			  else %>
				<%=strHTMLEncode(split(aAlbero(1,i),"|")(1))%>&nbsp;(<%=sTipoCorso%>)
			<%end if  %>						
			</td>
		</tr>
		<form name="frmTest<%=i%>" action="/pgm/formazione/questionari/Que_questionario.asp" method="post">
		<input type="hidden" name="idque" value="<%=split(aAlbero(1,i),"|")(0)%>">
		<input type="hidden" name="QModo" value="0">
		</form>		
		<%
		sPercFormativo = aAlbero(0,i)
		sSezionePercF  = aAlbero(1,i)
	next%>
	</table>
<%end sub

' **************************************************************************************
' **************************************************************************************
' *********************Dinamico ..... **************************************************
' **************************************************************************************

sub AAAAlberatura(aAlbero,fLink,fInfo)

	dim nLivelli
	' ****************************
	' Di che dimensione � l'array.
	' ****************************
	nLivelli = UBound(aAlbero,1)
	Response.Write "<BR>" & nLivelli%>

	<table border="0" cellpadding="0" cellspacing="0" width="100%"><%		
	
	
	
	
	
	for i = 0 to UBound(aAlbero,2)-1
		if split(aAlbero(UBound(aAlbero,1)-1,i),"|")(2) = "T" then
			sTipoCorso = "Test"
		else
			sTipoCorso = "Corso"
		end if
		if aAlbero(0,i) <> sPercFormativo then%>
			<tr>
				<td class="txtLiv1" colspan="<%=nLivelli%>">
				<%=strHTMLEncode(split(aAlbero(0,i),"|")(1))%>
				</td>
			</tr>
		<%end if
		
		
		for z = 0 to nLivelli		

			if aAlbero(1,i) <> sSezionePercF then
			
			
			end if 
		
		next 
		if aAlbero(1,i) <> sSezionePercF then%>
			
			<tr>
				<%if aAlbero(1,ubound(aAlbero,2)-1) <> aAlbero(1,i) then%>
					<td WIDTH="6%" background="/images/formazione/albero/accoda.gif">
						&nbsp;
					</td>
				<%else
					if nFine = 1 then%>
						<td WIDTH="6%">
							&nbsp;
						</td>						
					<%else%>
						<td WIDTH="6%" background="/images/formazione/albero/prossimo.gif">
							&nbsp;
						</td>
					<%end if
					nFine = 1 
				end if%>
				<td valign="bottom" width="94%" class="txtLiv2" colspan="2">&nbsp;<%=strHTMLEncode(split(aAlbero(1,i),"|")(1))%>
				</td>
			</tr>
			<tr>
				<%if nFine = 1 then%>
					<td WIDTH="6%">
						&nbsp;
					</td>
					<%if aAlbero(1,i) <> aAlbero(1,i+1) then%>						
						<td WIDTH="6%" valign="bottom" align="right" background="/images/formazione/albero/prossimo.gif">
							&nbsp;&nbsp;
					<%else%>
						<td WIDTH="6%" valign="bottom" align="right" background="/images/formazione/albero/accoda.gif">
							&nbsp;&nbsp;
					<%end if%>
				<%else%>
					<td WIDTH="6%" background="/images/formazione/albero/continua.gif">
						&nbsp;
					</td>
					<%if aAlbero(1,i) <> aAlbero(1,i+1) then%>						
						<td WIDTH="6%" valign="bottom" align="right" background="/images/formazione/albero/prossimo.gif">
							&nbsp;&nbsp;
					<%else%>
						<td WIDTH="6%" valign="bottom" align="right" background="/images/formazione/albero/accoda.gif">
							&nbsp;&nbsp;
					<%end if%>
				<%end if%>
				
			    <%
				if fInfo = 0 then%>
					<a class="txtLiv3Link" onmouseover="javascript:window.status='' ; return true" href="javascript:VisRis(<%=split(aAlbero(2,i),"|")(0)%>,'<%=split(aAlbero(2,i),"|")(2)%>')"><img src="<%=Session("Progetto")%>/images/Info.gif" border="0"></a>
				<%end if%>

				</td>
				<%if fLink = "S" then %>
					<form name="frmTest<%=i%>" action="/pgm/formazione/questionari/Que_questionario.asp" method="post">
					<input type="hidden" name="idque" value="<%=split(aAlbero(2,i),"|")(0)%>">
					<input type="hidden" name="QModo" value="0">
					</form>
					<td valign="bottom" width="94%" class="txtLiv3NoLink">&nbsp;
					<%if split(aAlbero(2,i),"|")(2) = "T" then%>
						<a class="txtLiv3Link" onmouseover="javascript:window.status='' ; return true" href="javascript:frmTest<%=i%>.submit()"><%=strHTMLEncode(split(aAlbero(2,i),"|")(1))%></a>&nbsp;(<%=sTipoCorso%>)
					<%else%>
						<a class="txtLiv3Link" href="Javascript:manda(<%=split(aAlbero(2,i),"|")(0)%>)"><%=strHTMLEncode(split(aAlbero(2,i),"|")(1))%></a>&nbsp;(<%=sTipoCorso%>)
					<%end if%>
				<%else %>
					<td valign="bottom" width="94%" class="txtLiv3NoLink">&nbsp;
					<%=strHTMLEncode(split(aAlbero(2,i),"|")(1))%>&nbsp;(<%=sTipoCorso%>)
				<%end if  %>						
			
				</td>
			</tr>
		<%else%>
			<tr>
				<% if nFine = 1 then%>
					<td WIDTH="6%">
						&nbsp;
					</td>
				<%else%>
					<td WIDTH="6%" background="/images/formazione/albero/continua.gif">
						&nbsp;
					</td>
				<%end if
				if ubound(aAlbero,2)-1 = i then %>
					<td WIDTH="6%" align="right" background="/images/formazione/albero/prossimo.gif">
						&nbsp;
					<%if fInfo = 0 then%>
						<a class="txtLiv3Link" onmouseover="javascript:window.status='' ; return true" href="javascript:VisRis(<%=split(aAlbero(2,i),"|")(0)%>,'<%=split(aAlbero(2,i),"|")(2)%>')"><img src="<%=Session("Progetto")%>/images/Info.gif" border="0"></a>
					<%end if%>
							
					</td>
				<%else
					if aAlbero(1,i) <> aAlbero(1,i+1) then %>
						<td WIDTH="6%" align="right" background="/images/formazione/albero/prossimo.gif">
							&nbsp;
						<%if fInfo = 0 then%>
							<a class="txtLiv3Link" onmouseover="javascript:window.status='' ; return true" href="javascript:VisRis(<%=split(aAlbero(2,i),"|")(0)%>,'<%=split(aAlbero(2,i),"|")(2)%>')"><img src="<%=Session("Progetto")%>/images/Info.gif" border="0"></a>
						<%end if%>

						</td>
					<%else%>		
						<td WIDTH="6%" align="right" background="/images/formazione/albero/accoda.gif">
							&nbsp;			
							<%
							if fInfo = 0 then%>
								<a class="txtLiv3Link" onmouseover="javascript:window.status='' ; return true" href="javascript:VisRis(<%=split(aAlbero(2,i),"|")(0)%>,'<%=split(aAlbero(2,i),"|")(2)%>')"><img src="<%=Session("Progetto")%>/images/Info.gif" border="0"></a>
							<%end if%>
						</td>
					<%end if
				end if%>
				<%if fLink = "S" then %>
					<form name="frmTest<%=i%>" action="/pgm/formazione/questionari/Que_questionario.asp" method="post">
					<input type="hidden" name="idque" value="<%=split(aAlbero(2,i),"|")(0)%>">
					<input type="hidden" name="QModo" value="0">
					</form>
					<td valign="bottom" width="94%" class="txtLiv3NoLink">&nbsp;
					<%if split(aAlbero(2,i),"|")(2) = "T" then%>
						<a class="txtLiv3Link" onmouseover="javascript:window.status='' ; return true" href="javascript:frmTest<%=i%>.submit()"><%=strHTMLEncode(split(aAlbero(2,i),"|")(1))%></a>&nbsp;(<%=sTipoCorso%>)
					<%else%>
						<a class="txtLiv3Link" href="Javascript:manda(<%=split(aAlbero(2,i),"|")(0)%>)"><%=strHTMLEncode(split(aAlbero(2,i),"|")(1))%></a>&nbsp;(<%=sTipoCorso%>)
					<%end if%>
				<%else %>
					<td valign="bottom" width="94%" class="txtLiv3NoLink">&nbsp;
					<%=strHTMLEncode(split(aAlbero(2,i),"|")(1))%>&nbsp;(<%=sTipoCorso%>)
				<%end if  %>
				</td>
			</tr>
		<%end if
		sPercFormativo = aAlbero(0,i)
		sSezionePercF  = aAlbero(1,i)
	next%>
	</table>
<%end sub%>
