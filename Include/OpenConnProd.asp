<%

'====================================================================================
'	Esta es la Unica pagina que tiene las conexiones a las BASES sean SQL u ORACLE
'====================================================================================

server.ScriptTimeout = 300

'Creazione Oggetti ADODB.-----------------------------
If  (Session("Progetto") & "*") = "*" Then
	Response.Redirect "/util/error_login.asp"
Else 
	'Progetto = LCase(mid(Session("Progetto"),2))
	Set CC = Server.CreateObject("ADODB.Connection") 'Objeto de conexion al Oracle
	Set CX = Server.CreateObject("ADODB.Connection") 'Objeto de conexion al Oracle que se utiliza en otras paginas
	Set gConnSeguridad = Server.CreateObject("ADODB.Connection") 'Objeto de conexion a la Base Seguridad
	Set Conn = Server.CreateObject("ADODB.Connection")	' Objeto de Conexion a la Base EmpleoPersonas
	Set connLavoro = Server.CreateObject("adodb.connection") ' Objeto de Conexion a la Base EmpleoLavoro
	Set connProyectos = Server.CreateObject("adodb.connection") ' Objeto de Conexion a la Base EmpleoProyectos

'..	' proyecto fijo
	Progetto = "ba"
	
	'MODIFICA PWD DI CONNESSIONE
	pwd = "dp_" & Progetto & "_il" 

	Hosting = ucase(Request.ServerVariables("SERVER_NAME")) 	
	select case Hosting

		'======================================================================
		' Sitio en espa�ol Ambiente de Producci�n
		'======================================================================
		case "PORTALEMPLEO", "WWW.EMPLE.GOV.AR", "GESTION.EMPLE.GOV.AR"
			'Server Name de Produccion
			
			'conexion a la base Seguridad del SQL
            Application(Hosting & "_PortalConnectionString") = "Provider=MSDASQL;Password=98760;Persist Security Info=True;User ID=Extranet;Extended Properties=""DSN=Seguridad;UID=Extranet;DATABASE=Seguridad"";Initial Catalog=Seguridad;"
            Application(Hosting & "_PortalConnectionTimeout") = 60
		    Application(Hosting & "_PortalCommandTimeout") = 180
		    Application(Hosting & "_PortalCursorLocation") = 3
		    Application(Hosting & "_PortalRuntimeUserName") = "Extranet"
		    Application(Hosting & "_PortalRuntimePassword") = "98760"
		    
		    'conexion a la base EmpleoPersonas del SQL
		    Application(Hosting & "_JefesConnectionString") = "Provider=MSDASQL;Password=98760;Persist Security Info=True;User ID=Extranet;Extended Properties=""DSN=EmpleoPersonas;UID=Extranet;DATABASE=EmpleoPersonas"";Initial Catalog=EmpleoPersonas;"
			Application(Hosting & "_JefesConnectionTimeout") = 60
			Application(Hosting & "_JefesCommandTimeout") = 180
			Application(Hosting & "_JefesCursorLocation") = 3 'adUseServer
			Application(Hosting & "_JefesRuntimeUserName") = "Intranet"
			Application(Hosting & "_JefesRuntimePassword") = "98760"
			'Application(Hosting & "_DTSImportacion") = "\\ODISEA\dtcimp08538$"
			
			'conexion a la base EmpleoLavoro del SQL
		    Application(Hosting & "_AgendaConnectionString") = "Provider=MSDASQL.1;Password=1234*;Persist Security Info=True;User ID=EmpleoLavoro;Data Source=EmpleoLavoro;Extended Properties=""DSN=EmpleoLavoro;Description=EmpleoLavoro;UID=EmpleoLavoro;DATABASE=EmpleoLavoro"";Initial Catalog=EmpleoLavoro;User Id=EmpleoLavoro;PASSWORD=1234*;"
			Application(Hosting & "_AgendaConnectionTimeout") = 60
			Application(Hosting & "_AgendaCommandTimeout") = 180
			Application(Hosting & "_AgendaCursorLocation") = 3
			Application(Hosting & "_AgendaRuntimeUserName") = "EmpleoLavoro"
			Application(Hosting & "_AgendaRuntimePassword") = "1234*"
			
			'conexion a la base EmpleoProyectos del SQL
			Application(Hosting & "_ProyConnectionString") = "Provider=MSDASQL;Password=1234*;Persist Security Info=True;User ID=EmpleoLavoro;Extended Properties=""DSN=EmpleoProyectos;UID=PagoDirectoProcesos;DATABASE=EmpleoProyectos"";Initial Catalog=EmpleoProyectos;"
			Application(Hosting & "_ProyConnectionTimeout") = 60
			Application(Hosting & "_ProyCommandTimeout") = 180
			Application(Hosting & "_ProyCursorLocation") = 3
			Application(Hosting & "_ProyRuntimeUserName") = "EmpleoLavoro"
			Application(Hosting & "_ProyRuntimePassword") = "1234*"
			
		    'Conexion a la base Portal Oracle
		    Application(Hosting & "_PortalConnectionStringOracle") = "Provider=OraOLEDB.Oracle.1;user id=" &  Progetto & "; password=" &  pwd & "; data source=prod_poseidon"
            Application(Hosting & "_PortalConnectionTimeoutOracle") = 60
		    Application(Hosting & "_PortalCommandTimeoutOracle") = 180
		    Application(Hosting & "_PortalCursorLocationOracle") = 3
		    
		    'Nombre del sitio Web para el link de salto
			Application(Hosting & "_PortalSitioProyectos") = "GestionEmpleo/empleo"		
			
			'Nombre del sitio Web para el link de salto
			Application(Hosting & "_PortalSitioPersonas") = "GestionEmpleo/Jefes"    
			
			' Conexion a la base del Datawarehouse en SQL
			Application(Hosting & "_DataWareHouseConnectionString") = "Provider=MSDASQL;Password=consulta;Persist Security Info=True;User ID=OlapReader;Extended Properties=""DSN=EmpleoDW;UID=OlapReader;DATABASE=Empleo_DW"";Initial Catalog=Empleo_DW;"
			Application(Hosting & "_DataWareHouseConnectionTimeout") = 60
			Application(Hosting & "_DataWareHouseCommandTimeout") = 180
			Application(Hosting & "_DataWareHouseCursorLocation") = 3
			Application(Hosting & "_DataWareHouseRuntimeUserName") = "OlapReader"
			Application(Hosting & "_DataWareHouseRuntimePassword") = "consulta"

		    ' Reporting Service - LLamada al servicio (link de salto)
			Application(Hosting & "_ReportingService") = "http://reportes.trabajo.gov.ar/ReportServerInternet/Pages/ReportViewer.aspx?"

		'======================================================================
		' sitio no definido
		'======================================================================
		case else 
			Response.Write "<br>Entorno o Hosting No Definido  - Error en OpenConn <br> ServerName: " & Request.ServerVariables("SERVER_NAME")
			Response.end
		
		
	end select 
	
	' Determina ServerName
    If Len(ServerName) = 0 Then
	    ServerName = Request.ServerVariables("SERVER_NAME")
	    Application("SERVER_NAME") = ServerName
    End If
    ServerName = Request.ServerVariables("SERVER_NAME")

    
    'ServerWeb = "http://" & ServerName
    
    'Session("Server") = ServerWeb
'---------------------------------------
'	Coneccion a Abrir: Base Seguridad
'---------------------------------------
'Response.Write Request.ServerVariables("SERVER_NAME")
'Response.Write Application(ServerName & "_PortalConnectionString")	
'Response.end
	
	With gConnSeguridad
	.ConnectionString = Application(ServerName & "_PortalConnectionString")	
	.ConnectionTimeout = Application(ServerName & "_PortalConnectionTimeout")
	.CommandTimeout = Application(ServerName & "_PortalCommandTimeout")
	
	'Response.Write Application(ServerName & "_PortalCursorLocation")
	'Response.end
	
	.CursorLocation = Application(ServerName & "_PortalCursorLocation")
	.Open ,Application(ServerName & "_PortalRuntimeUserName"), Application(ServerName & "_PortalRuntimePassword")
	''Response.Write .ConnectionString
    End With
    
    
    
'------------------------------------------
'	Coneccion a Abrir: Base EmpleoPersonas
'------------------------------------------
	With Conn
		.ConnectionString = Application(ServerName & "_JefesConnectionString")	
		.ConnectionTimeout = Application(ServerName & "_JefesConnectionTimeout")
		.CommandTimeout = Application(ServerName & "_JefesCommandTimeout")
		.CursorLocation = Application(ServerName & "_JefesCursorLocation")
		.Open ,Application(ServerName & "_JefesRuntimeUserName"), Application(ServerName & "_JefesRuntimePassword")
		'Carpeta = Application(ServerName & "_DTSImportacion")
		'conn.Open "Provider=SQLOLEDB.1;Password=98760;Persist Security Info=False;User ID=Intranet;Initial Catalog=EmpleoPersonas;Data Source=RAEL"
		'Response.Write .ConnectionString
	End With
	
	
'----------------------------------------
'	Coneccion a Abrir: Base EmpleoLavoro
'----------------------------------------	
	with connLavoro
	    .ConnectionString = Application(ServerName & "_AgendaConnectionString")	
	    .ConnectionTimeout = Application(ServerName & "_AgendaConnectionTimeout")
	    .CommandTimeout = Application(ServerName & "_AgendaCommandTimeout")
	    .CursorLocation = Application(ServerName & "_AgendaCursorLocation")
	    .Open ,Application(ServerName & "_AgendaRuntimeUserName"), Application(ServerName & "_AgendaRuntimePassword")
	end with

'------------------------------------------
'	Coneccion a Abrir: Base EmpleoProyectos
'------------------------------------------	
	with connProyectos
	    .ConnectionString = Application(ServerName & "_ProyConnectionString")	
	    .ConnectionTimeout = Application(ServerName & "_ProyConnectionTimeout")
	    .CommandTimeout = Application(ServerName & "_ProyCommandTimeout")
	    .CursorLocation = Application(ServerName & "_ProyCursorLocation")
	    .Open ,Application(ServerName & "_ProyRuntimeUserName"), Application(ServerName & "_ProyRuntimePassword")
	end with
	
'---------------------------------------
'	Coneccion a Abrir: Base Oracle
'---------------------------------------   
	'..Response.Write Application(ServerName & "_PortalConnectionStringOracle")	
	'..Response.End
	
    With CC
		.ConnectionString = Application(ServerName & "_PortalConnectionStringOracle")	
		.ConnectionTimeout = Application(ServerName & "_PortalConnectionTimeoutOracle")
		.CommandTimeout = Application(ServerName & "_PortalCommandTimeoutOracle")
		.CursorLocation = Application(ServerName & "_PortalCursorLocationOracle")
		.Open ',Application(ServerName & "_PortalRuntimeUserName"), Application(ServerName & "_PortalRuntimePassword")
		''Response.Write .ConnectionString
	End With
	
	Set CX = CC
	GOracleConnString = Application(ServerName & "_PortalConnectionStringOracle")

'----------------------------------------------------------------------------------------------------
'	Coneccion a Abrir: Base DataWareHouse SQL (** Se usa solo para reportes estadisticos ***)
'		No se habre la conexion aca, solo en los reportes estadisticos
'----------------------------------------------------------------------------------------------------
'	Set connEmpleoDataWareHouse = Server.CreateObject("adodb.connection") ' Objeto de Conexion a la Base EmpleoProyectos
'	with connEmpleoDataWareHouse
'	    .ConnectionString = Application(ServerName & "_DataWareHouseConnectionString")	
'	    .ConnectionTimeout = Application(ServerName & "_DataWareHouseConnectionTimeout")
'	    .CommandTimeout = Application(ServerName & "_DataWareHouseCommandTimeout")
'	    .CursorLocation = Application(ServerName & "_DataWareHouseCursorLocation")
'	    .Open ,Application(ServerName & "_DataWareHouseRuntimeUserName"), Application(ServerName & "_DataWareHouseRuntimePassword")
'	end with

'---------------------------------------
'	Links Varios
'---------------------------------------   
	Application("GPortalSitioProyectos") = Application(ServerName & "_PortalSitioProyectos")
	Application("LinkReportingService") = Application(ServerName & "_ReportingService")
	    
	'Response.Write ucase(Request.ServerVariables("SERVER_NAME"))
	'Response.Write Progetto
End If

%>