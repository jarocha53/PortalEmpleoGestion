<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%
'''funcion que devuelve si esta dentro del horario laboral para la oficina de empleo
'''vani

public function VerificaHorarioOficina()
	
	Set SpHorario = Server.CreateObject("adodb.command")
	
	with SpHorario
		.activeconnection = connLavoro
		.Commandtext = "AgVerificaHorarioEntrevista"
		.commandtype = adCmdStoredProc
		
		For Each Parameter In .Parameters
			If Parameter.Direction And adParamInput Then
				Select case ucase(Mid(Parameter.Name, 2))
					case "OFICINA"
						Parameter.value = Session("creator")	
					CASE "STATUS"
						if len(Status) = 0 then 
							Status = 0
						end if
						Parameter.value = Status
					CASE "MENSAJE"
						Parameter.value = "" & MENSAJE	
					CASE ELSE
						' ????
				END SELECT
			End If
		Next
		'--------------------------------------------
		' Ejecuta
		'--------------------------------------------
'PL-SQL * T-SQL  
XXX = TransformPLSQLToTSQL (XXX)  
		.Execute XXX, , adexecutenorecords
		
		'--------------------------------------------
		' Devuelve parametros
		'--------------------------------------------
		For Each Parameter In .Parameters
			If Parameter.Direction And (adParamOutput Or adParamReturnValue) Then
				Select case ucase(Mid(Parameter.Name, 2))
					case "STATUS"
						STATUS = trim(Parameter.value)
					case "MENSAJE"
						MENSAJE = trim(Parameter.value)
					case else
						' ???
				end select
			End If
		Next
	end with
	
	if STATUS <> 1 then 
		VerificaHorarioOficina = MENSAJE
	else
		VerificaHorarioOficina = cstr(STATUS)
	end if 
	
	set SpHorario = nothing


end function
%>
