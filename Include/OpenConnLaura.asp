<%

'====================================================================================
'	Esta es la Unica pagina que tiene las conexiones a las BASES sean SQL u ORACLE
'====================================================================================

server.ScriptTimeout = 300

'Creazione Oggetti ADODB.-----------------------------
If  (Session("Progetto") & "*") = "*" Then
	Response.Redirect "/util/error_login.asp"
Else 
	

	'Progetto = LCase(mid(Session("Progetto"),2))
	Set CC = Server.CreateObject("ADODB.Connection") 'Objeto de conexion al Oracle
	Set CX = Server.CreateObject("ADODB.Connection") 'Objeto de conexion al Oracle que se utiliza en otras paginas
	Set gConnSeguridad = Server.CreateObject("ADODB.Connection") 'Objeto de conexion a la Base Seguridad
	Set gConnSeguridadReplica = Server.CreateObject("ADODB.Connection") 'Objeto de conexion a la Base Seguridad
	Set ConnEmpleoPersonas = Server.CreateObject("ADODB.Connection")	' Objeto de Conexion a la Base EmpleoPersonas
	Set ConnEmpleoPersonasReplica = Server.CreateObject("ADODB.Connection")	' Objeto de Conexion a la Base EmpleoPersonas
	Set connLavoro = Server.CreateObject("adodb.connection") ' Objeto de Conexion a la Base EmpleoLavoro
	Set connProyectos = Server.CreateObject("adodb.connection") ' Objeto de Conexion a la Base EmpleoProyectos

'..	' proyecto fijo
	Progetto = "ba"
	
	'MODIFICA PWD DI CONNESSIONE
	pwd = "dp_" & Progetto & "_il" 

	Hosting = ucase(Request.ServerVariables("SERVER_NAME")) 	
	select case Hosting
	    '======================================================================
	    ' Sitio en espa�ol Ambiente de Desarrollo
	    '======================================================================
		case "PORTALDESARROLLOGESTION", "PORTALDESARROLLO"  
			'------------------------------------------------
            'Server Name de desarrollo
			'------------------------------------------------
			'conexion a la base Seguridad del SQL (online)
            Application(Hosting & "_PortalConnectionString") = "Provider=MSDASQL;Password=98760;Persist Security Info=True;User ID=Extranet;Extended Properties=""DSN=Seguridad;UID=Extranet;DATABASE=Seguridad"";Initial Catalog=Seguridad;"
            Application(Hosting & "_PortalConnectionTimeout") = 60
		    Application(Hosting & "_PortalCommandTimeout") = 180
		    Application(Hosting & "_PortalCursorLocation") = 3
		    Application(Hosting & "_PortalRuntimeUserName") = "Extranet"
		    Application(Hosting & "_PortalRuntimePassword") = "98760"
		    
		    'conexion a la base Seguridad del SQL (replica)
            Application(Hosting & "_PortalReplicaConnectionString") = "Provider=MSDASQL;Password=98760;Persist Security Info=True;User ID=Extranet;Extended Properties=""DSN=Seguridad;UID=Extranet;DATABASE=Seguridad"";Initial Catalog=Seguridad;"
            Application(Hosting & "_PortalReplicaConnectionTimeout") = 60
		    Application(Hosting & "_PortalReplicaCommandTimeout") = 180
		    Application(Hosting & "_PortalReplicaCursorLocation") = 3
		    Application(Hosting & "_PortalReplicaRuntimeUserName") = "Extranet"
		    Application(Hosting & "_PortalReplicaRuntimePassword") = "98760"
		    
		    'conexion a la base EmpleoPersonas del SQL on line
		    Application(Hosting & "_JefesConnectionString") = "Provider=MSDASQL;Password=98760;Persist Security Info=True;User ID=Extranet;Extended Properties=""DSN=EmpleoPersonas;UID=Extranet;DATABASE=EmpleoPersonas"";Initial Catalog=EmpleoPersonas;"
			Application(Hosting & "_JefesConnectionTimeout") = 60
			Application(Hosting & "_JefesCommandTimeout") = 180
			Application(Hosting & "_JefesCursorLocation") = 3 'adUseServer
			Application(Hosting & "_JefesRuntimeUserName") = "Intranet"
			Application(Hosting & "_JefesRuntimePassword") = "98760"
			Application(Hosting & "_JefesDTSImportacion") = "\\Dory\dtcimp08538$"

		    'conexion a la base EmpleoPersonas del SQL replica
		    Application(Hosting & "_JefesReplicaConnectionString") = "Provider=MSDASQL;Password=98760;Persist Security Info=True;User ID=Extranet;Extended Properties=""DSN=EmpleoPersonas;UID=Extranet;DATABASE=EmpleoPersonas"";Initial Catalog=EmpleoPersonas;"
			Application(Hosting & "_JefesReplicaConnectionTimeout") = 60
			Application(Hosting & "_JefesReplicaCommandTimeout") = 180
			Application(Hosting & "_JefesReplicaCursorLocation") = 3 'adUseServer
			Application(Hosting & "_JefesReplicaRuntimeUserName") = "Intranet"
			Application(Hosting & "_JefesReplicaRuntimePassword") = "98760"
			Application(Hosting & "_JefesReplicaDTSImportacion") = "\\Dory\dtcimp08538$"

			'conexion a la base EmpleoLavoro del SQL
		    Application(Hosting & "_AgendaConnectionString") = "Provider=MSDASQL.1;Password=1234*;Persist Security Info=True;User ID=EmpleoLavoro;Data Source=EmpleoLavoro;Extended Properties=""DSN=EmpleoLavoro;Description=EmpleoLavoro;UID=EmpleoLavoro;;DATABASE=EmpleoLavoro"";Initial Catalog=EmpleoLavoro;User Id=EmpleoLavoro;PASSWORD=1234*;"
			Application(Hosting & "_AgendaConnectionTimeout") = 60
			Application(Hosting & "_AgendaCommandTimeout") = 180
			Application(Hosting & "_AgendaCursorLocation") = 3
			Application(Hosting & "_AgendaRuntimeUserName") = "EmpleoLavoro"
			Application(Hosting & "_AgendaRuntimePassword") = "1234*"
						
			'conexion a la base EmpleoProyectos del SQL
			Application(Hosting & "_ProyConnectionString") = "Provider=MSDASQL;Password=1234*;Persist Security Info=True;User ID=EmpleoLavoro;Extended Properties=""DSN=EmpleoProyectos;UID=PagoDirectoProcesos;DATABASE=EmpleoProyectos"";Initial Catalog=EmpleoProyectos;"
			Application(Hosting & "_ProyConnectionTimeout") = 60
			Application(Hosting & "_ProyCommandTimeout") = 180
			Application(Hosting & "_ProyCursorLocation") = 3
			Application(Hosting & "_ProyRuntimeUserName") = "EmpleoLavoro"
			Application(Hosting & "_ProyRuntimePassword") = "1234*"
		    
		    'Conexion a la base Portal Oracle
		    Application(Hosting & "_PortalConnectionStringOracle") = "Provider=OraOLEDB.Oracle.1;user id=" &  Progetto & "; password=" &  pwd & "; data source=portal_rael"
            Application(Hosting & "_PortalConnectionTimeoutOracle") = 60
		    Application(Hosting & "_PortalCommandTimeoutOracle") = 180
		    Application(Hosting & "_PortalCursorLocationOracle") = 3
		    
		    'Nombre del sitio Web para el link de salto
			Application(Hosting & "_PortalSitioProyectos") = "http://DesarrolloEmpleo/empleo"

		    'Nombre del sitio Web para el link de salto
			Application(Hosting & "_PortalSitioPersonas") = "DesarrolloEmpleo/Jefes"
					    		    
			' Conexion a la base del Datawarehouse en SQL
			Application(Hosting & "_DataWareHouseConnectionString") = "Provider=MSDASQL;Password=consulta;Persist Security Info=True;User ID=OlapReader;Extended Properties=""DSN=EmpleoDW;UID=OlapReader;DATABASE=Empleo_DW"";Initial Catalog=Empleo_DW;"
			Application(Hosting & "_DataWareHouseConnectionTimeout") = 60
			Application(Hosting & "_DataWareHouseCommandTimeout") = 180
			Application(Hosting & "_DataWareHouseCursorLocation") = 3
			Application(Hosting & "_DataWareHouseRuntimeUserName") = "OlapReader"
			Application(Hosting & "_DataWareHouseRuntimePassword") = "consulta"

		    ' Reporting Service - LLamada al servicio (link de salto)
			Application(Hosting & "_ReportingService") = "http://flawless/ReportServer/Pages/ReportViewer.aspx?"
			
'	Response.Write Application(Hosting & "_PortalConnectionStringOracle")		    		    
'	Response.end
			
		'======================================================================
		' Sitio en espa�ol Ambiente de Prueba
		'======================================================================
           
           case "PORTALPRUEBA",  "PORTALPRUEBAGESTION"

           'Server Name de Prueba

           'conexion a la base Seguridad del SQL

            Application(Hosting & "_PortalConnectionString") = "Provider=MSDASQL;Password=98760;Persist Security Info=True;User ID=Extranet;Extended Properties=""DSN=Seguridad;UID=Extranet;DATABASE=Seguridad"";Initial Catalog=Seguridad;"
            Application(Hosting & "_PortalConnectionTimeout") = 60
            Application(Hosting & "_PortalCommandTimeout") = 180
            Application(Hosting & "_PortalCursorLocation") = 3
            Application(Hosting & "_PortalRuntimeUserName") = "Extranet"
			Application(Hosting & "_PortalRuntimePassword") = "98760"

           'conexion a la base Seguridad del SQL (REPLICA)

            Application(Hosting & "_PortalReplicaConnectionString") = "Provider=MSDASQL;Password=98760;Persist Security Info=True;User ID=Extranet;Extended Properties=""DSN=Seguridad;UID=Extranet;DATABASE=Seguridad"";Initial Catalog=Seguridad;"
            Application(Hosting & "_PortalReplicaConnectionTimeout") = 60
            Application(Hosting & "_PortalReplicaCommandTimeout") = 180
            Application(Hosting & "_PortalReplicaCursorLocation") = 3
            Application(Hosting & "_PortalReplicaRuntimeUserName") = "Extranet"
			Application(Hosting & "_PortalReplicaRuntimePassword") = "98760"
			
            'conexion a la base EmpleoPersonas del SQL (ONLINE)

            Application(Hosting & "_JefesConnectionString") = "Provider=MSDASQL;Password=98760;Persist Security Info=True;User ID=Extranet;Extended Properties=""DSN=EmpleoPersonas;UID=Extranet;DATABASE=EmpleoPersonas"";Initial Catalog=EmpleoPersonas;"
            Application(Hosting & "_JefesConnectionTimeout") = 60
            Application(Hosting & "_JefesCommandTimeout") = 180
            Application(Hosting & "_JefesCursorLocation") = 3 'adUseServer
            Application(Hosting & "_JefesRuntimeUserName") = "Intranet"
            Application(Hosting & "_JefesRuntimePassword") = "98760"
            Application(Hosting & "_JefesDTSImportacion") = "\\Dummy\dtcimp08538$"

 

            'conexion a la base EmpleoPersonas del SQL (REPLICA)

            Application(Hosting & "_JefesReplicaConnectionString") = "Provider=MSDASQL;Password=98760;Persist Security Info=True;User ID=Extranet;Extended Properties=""DSN=EmpleoPersonas;UID=Extranet;DATABASE=EmpleoPersonas"";Initial Catalog=EmpleoPersonas;"
            Application(Hosting & "_JefesReplicaConnectionTimeout") = 60
            Application(Hosting & "_JefesReplicaCommandTimeout") = 180
            Application(Hosting & "_JefesReplicaCursorLocation") = 3 'adUseServer
            Application(Hosting & "_JefesReplicaRuntimeUserName") = "Intranet"
            Application(Hosting & "_JefesReplicaRuntimePassword") = "98760"
            Application(Hosting & "_JefesReplicaDTSImportacion") = "\\Dummy\dtcimp08538$"

                                

            'conexion a la base EmpleoLavoro del SQL

			Application(Hosting & "_AgendaConnectionString") = "Provider=MSDASQL.1;Password=1234*;Persist Security Info=True;User ID=EmpleoLavoro;Data Source=EmpleoLavoro;Extended Properties=""DSN=EmpleoLavoro;Description=EmpleoLavoro;UID=EmpleoLavoro;DATABASE=EmpleoLavoro"";Initial Catalog=EmpleoLavoro;User Id=EmpleoLavoro;PASSWORD=1234*;"
            Application(Hosting & "_AgendaConnectionTimeout") = 60
            Application(Hosting & "_AgendaCommandTimeout") = 180
            Application(Hosting & "_AgendaCursorLocation") = 3
            Application(Hosting & "_AgendaRuntimeUserName") = "EmpleoLavoro"
            Application(Hosting & "_AgendaRuntimePassword") = "1234*"

            'conexion a la base EmpleoProyectos del SQL

            Application(Hosting & "_ProyConnectionString") = "Provider=MSDASQL;Password=1234*;Persist Security Info=True;User ID=EmpleoLavoro;Extended Properties=""DSN=EmpleoProyectos;UID=PagoDirectoProcesos;DATABASE=EmpleoProyectos"";Initial Catalog=EmpleoProyectos;"
            Application(Hosting & "_ProyConnectionTimeout") = 60
            Application(Hosting & "_ProyCommandTimeout") = 180
            Application(Hosting & "_ProyCursorLocation") = 3
            Application(Hosting & "_ProyRuntimeUserName") = "EmpleoLavoro"
            Application(Hosting & "_ProyRuntimePassword") = "1234*"

                                

            'Conexion a la base Portal Oracle

            Application(Hosting & "_PortalConnectionStringOracle") = "Provider=OraOLEDB.Oracle.1;user id=" &  Progetto & "; password=" &  pwd & "; data source=portal_poseidon"
			Application(Hosting & "_PortalConnectionTimeoutOracle") = 60
            Application(Hosting & "_PortalCommandTimeoutOracle") = 180
            Application(Hosting & "_PortalCursorLocationOracle") = 3


			'Nombre del sitio Web para el link de salto

            Application(Hosting & "_PortalSitioProyectos") = "http://PruebaEmpleo/empleo"
                                

            'Nombre del sitio Web para el link de salto

            Application(Hosting & "_PortalSitioPersonas") = "PruebaEmpleo/Jefes"

 

            ' Conexion a la base del Datawarehouse en SQL
            Application(Hosting & "_DataWareHouseConnectionString") = "Provider=MSDASQL;Password=consulta;Persist Security Info=True;User ID=OlapReader;Extended Properties=""DSN=EmpleoDW;UID=OlapReader;DATABASE=Empleo_DW"";Initial Catalog=Empleo_DW;"
            Application(Hosting & "_DataWareHouseConnectionTimeout") = 60
            Application(Hosting & "_DataWareHouseCommandTimeout") = 180
            Application(Hosting & "_DataWareHouseCursorLocation") = 3
            Application(Hosting & "_DataWareHouseRuntimeUserName") = "OlapReader"
            Application(Hosting & "_DataWareHouseRuntimePassword") = "consulta"

 

            ' Reporting Service - LLamada al servicio (link de salto)

            Application(Hosting & "_ReportingService") = "http://flawless/ReportServer/Pages/ReportViewer.aspx?"


			
		'======================================================================
		' Sitio en espa�ol Ambiente de Prueba externo para usuarios de las oficinas
		'======================================================================
		case "PORTALUSUARIOS" 
			'Server Name de Prueba
			
			'conexion a la base Seguridad del SQL (ON LINE)
            Application(Hosting & "_PortalConnectionString") = "Provider=MSDASQL;Password=98760;Persist Security Info=True;User ID=Extranet;Extended Properties=""DSN=Seguridad_capacita;UID=Extranet;DATABASE=Seguridad"";Initial Catalog=Seguridad;"
            Application(Hosting & "_PortalConnectionTimeout") = 60
		    Application(Hosting & "_PortalCommandTimeout") = 180
		    Application(Hosting & "_PortalCursorLocation") = 3
		    Application(Hosting & "_PortalRuntimeUserName") = "Extranet"
		    Application(Hosting & "_PortalRuntimePassword") = "98760"

			'conexion a la base Seguridad del SQL (REPLICA)
            Application(Hosting & "_PortalReplicaConnectionString") = "Provider=MSDASQL;Password=98760;Persist Security Info=True;User ID=Extranet;Extended Properties=""DSN=Seguridad_capacita;UID=Extranet;DATABASE=Seguridad"";Initial Catalog=Seguridad;"
            Application(Hosting & "_PortalReplicaConnectionTimeout") = 60
		    Application(Hosting & "_PortalReplicaCommandTimeout") = 180
		    Application(Hosting & "_PortalReplicaCursorLocation") = 3
		    Application(Hosting & "_PortalReplicaRuntimeUserName") = "Extranet"
		    Application(Hosting & "_PortalReplicaRuntimePassword") = "98760"
	    
		    'conexion a la base EmpleoPersonas (ON LINE)
		    Application(Hosting & "_JefesConnectionString") = "Provider=MSDASQL;Password=98760;Persist Security Info=True;User ID=Extranet;Extended Properties=""DSN=EmpleoPersonas_capacita;UID=Extranet;DATABASE=EmpleoPersonas"";Initial Catalog=EmpleoPersonas;"
			Application(Hosting & "_JefesConnectionTimeout") = 60
			Application(Hosting & "_JefesCommandTimeout") = 180
			Application(Hosting & "_JefesCursorLocation") = 3 'adUseServer
			Application(Hosting & "_JefesRuntimeUserName") = "Intranet"
			Application(Hosting & "_JefesRuntimePassword") = "98760"
			'Application(Hosting & "_DTSImportacion") = "\\Dummy\dtcimp08538$"
			
			'conexion a la base EmpleoPersonas (REPLICA)
		    Application(Hosting & "_JefesReplicaConnectionString") = "Provider=MSDASQL;Password=98760;Persist Security Info=True;User ID=Extranet;Extended Properties=""DSN=EmpleoPersonas_capacita;UID=Extranet;DATABASE=EmpleoPersonas"";Initial Catalog=EmpleoPersonas;"
			Application(Hosting & "_JefesReplicaConnectionTimeout") = 60
			Application(Hosting & "_JefesReplicaCommandTimeout") = 180
			Application(Hosting & "_JefesReplicaCursorLocation") = 3 'adUseServer
			Application(Hosting & "_JefesReplicaRuntimeUserName") = "Intranet"
			Application(Hosting & "_JefesReplicaRuntimePassword") = "98760"
			'Application(Hosting & "_DTSImportacion") = "\\Dummy\dtcimp08538$"
			
			'conexion a la base EmpleoLavoro del SQL
		    Application(Hosting & "_AgendaConnectionString") = "Provider=MSDASQL.1;Password=1234*;Persist Security Info=True;User ID=EmpleoLavoro;Data Source=EmpleoLavoro_capacita;Extended Properties=""DSN=EmpleoLavoro;Description=EmpleoLavoro;UID=EmpleoLavoro;DATABASE=EmpleoLavoro"";Initial Catalog=EmpleoLavoro;User Id=EmpleoLavoro;PASSWORD=1234*;"
			Application(Hosting & "_AgendaConnectionTimeout") = 60
			Application(Hosting & "_AgendaCommandTimeout") = 180
			Application(Hosting & "_AgendaCursorLocation") = 3
			Application(Hosting & "_AgendaRuntimeUserName") = "EmpleoLavoro"
			Application(Hosting & "_AgendaRuntimePassword") = "1234*"
			
			'conexion a la base EmpleoProyectos del SQL
			Application(Hosting & "_ProyConnectionString") = "Provider=MSDASQL;Password=1234*;Persist Security Info=True;User ID=EmpleoLavoro;Extended Properties=""DSN=EmpleoProyectos;UID=PagoDirectoProcesos;DATABASE=EmpleoProyectos"";Initial Catalog=EmpleoProyectos;"
			Application(Hosting & "_ProyConnectionTimeout") = 60
			Application(Hosting & "_ProyCommandTimeout") = 180
			Application(Hosting & "_ProyCursorLocation") = 3
			Application(Hosting & "_ProyRuntimeUserName") = "EmpleoLavoro"
			Application(Hosting & "_ProyRuntimePassword") = "1234*"
			
		    'Conexion a la base Portal Oracle
		    Application(Hosting & "_PortalConnectionStringOracle") = "Provider=OraOLEDB.Oracle.1;user id=" &  Progetto & "; password=" &  pwd & "; data source=usuarios_poseidon"
            Application(Hosting & "_PortalConnectionTimeoutOracle") = 60
		    Application(Hosting & "_PortalCommandTimeoutOracle") = 180
		    Application(Hosting & "_PortalCursorLocationOracle") = 3
		    
		    'Nombre del sitio Web para el link de salto
			Application(Hosting & "_PortalSitioProyectos") = "PruebaEmpleo/empleo"
			
			'Nombre del sitio Web para el link de salto
			Application(Hosting & "_PortalSitioPersonas") = "PruebaEmpleo/Jefes"
			
			' Conexion a la base del Datawarehouse en SQL
			Application(Hosting & "_DataWareHouseConnectionString") = "Provider=MSDASQL;Password=consulta;Persist Security Info=True;User ID=OlapReader;Extended Properties=""DSN=EmpleoDW;UID=OlapReader;DATABASE=Empleo_DW"";Initial Catalog=Empleo_DW;"
			Application(Hosting & "_DataWareHouseConnectionTimeout") = 60
			Application(Hosting & "_DataWareHouseCommandTimeout") = 180
			Application(Hosting & "_DataWareHouseCursorLocation") = 3
			Application(Hosting & "_DataWareHouseRuntimeUserName") = "OlapReader"
			Application(Hosting & "_DataWareHouseRuntimePassword") = "consulta"

		    ' Reporting Service - LLamada al servicio (link de salto)
			Application(Hosting & "_ReportingService") = "http://reportes.trabajo.gov.ar/ReportServerInternet/Pages/ReportViewer.aspx?"

		'======================================================================
		' Sitio en espa�ol Ambiente de Producci�n
		'======================================================================
		case "GESTION.EMPLE.GOV.AR"
			'Server Name de Produccion
			
			'conexion a la base Seguridad del SQL (ON LINE)
            Application(Hosting & "_PortalConnectionString") = "Provider=MSDASQL;Password=98760;Persist Security Info=True;User ID=Extranet;Extended Properties=""DSN=Seguridad;UID=Extranet;DATABASE=Seguridad"";Initial Catalog=Seguridad;"
            Application(Hosting & "_PortalConnectionTimeout") = 300
		    Application(Hosting & "_PortalCommandTimeout") = 300
		    Application(Hosting & "_PortalCursorLocation") = 3
		    Application(Hosting & "_PortalRuntimeUserName") = "Extranet"
		    Application(Hosting & "_PortalRuntimePassword") = "98760"
		    
		    'conexion a la base Seguridad del SQL (REPLICA)
            Application(Hosting & "_PortalReplicaConnectionString") = "Provider=MSDASQL;Password=98760;Persist Security Info=True;User ID=Extranet;Extended Properties=""DSN=Seguridad;UID=Extranet;DATABASE=Seguridad"";Initial Catalog=Seguridad;"
            Application(Hosting & "_PortalReplicaConnectionTimeout") = 300
		    Application(Hosting & "_PortalReplicaCommandTimeout") = 300
		    Application(Hosting & "_PortalReplicaCursorLocation") = 3
		    Application(Hosting & "_PortalReplicaRuntimeUserName") = "Extranet"
		    Application(Hosting & "_PortalReplicaRuntimePassword") = "98760"
		    
		    'conexion a la base EmpleoPersonas del SQL (ON LINE)
		    Application(Hosting & "_JefesConnectionString") = "Provider=MSDASQL;Password=98760;Persist Security Info=True;User ID=Extranet;Extended Properties=""DSN=EmpleoPersonas;UID=Extranet;DATABASE=EmpleoPersonas"";Initial Catalog=EmpleoPersonas;"
			Application(Hosting & "_JefesConnectionTimeout") = 300
			Application(Hosting & "_JefesCommandTimeout") = 300
			Application(Hosting & "_JefesCursorLocation") = 3 'adUseServer
			Application(Hosting & "_JefesRuntimeUserName") = "Intranet"
			Application(Hosting & "_JefesRuntimePassword") = "98760"
			'Application(Hosting & "_DTSImportacion") = "\\ODISEA\dtcimp08538$"
			
		    'conexion a la base EmpleoPersonas del SQL (REPLICA)
		    Application(Hosting & "_JefesReplicaConnectionString") = "Provider=MSDASQL;Password=98760;Persist Security Info=True;User ID=Extranet;Extended Properties=""DSN=EmpleoPersonas;UID=Extranet;DATABASE=EmpleoPersonas"";Initial Catalog=EmpleoPersonas;"
			Application(Hosting & "_JefesReplicaConnectionTimeout") = 300
			Application(Hosting & "_JefesReplicaCommandTimeout") = 300
			Application(Hosting & "_JefesReplicaCursorLocation") = 3 'adUseServer
			Application(Hosting & "_JefesReplicaRuntimeUserName") = "Intranet"
			Application(Hosting & "_JefesReplicaRuntimePassword") = "98760"
			'Application(Hosting & "_JefesReplicaDTSImportacion") = "\\ODISEA\dtcimp08538$"

			'conexion a la base EmpleoLavoro del SQL
		    Application(Hosting & "_AgendaConnectionString") = "Provider=MSDASQL.1;Password=1234*;Persist Security Info=True;User ID=EmpleoLavoro;Data Source=EmpleoLavoro;Extended Properties=""DSN=EmpleoLavoro;Description=EmpleoLavoro;UID=EmpleoLavoro;DATABASE=EmpleoLavoro"";Initial Catalog=EmpleoLavoro;User Id=EmpleoLavoro;PASSWORD=1234*;"
			Application(Hosting & "_AgendaConnectionTimeout") = 300
			Application(Hosting & "_AgendaCommandTimeout") = 300
			Application(Hosting & "_AgendaCursorLocation") = 3
			Application(Hosting & "_AgendaRuntimeUserName") = "EmpleoLavoro"
			Application(Hosting & "_AgendaRuntimePassword") = "1234*"
			
			'conexion a la base EmpleoProyectos del SQL
			Application(Hosting & "_ProyConnectionString") = "Provider=MSDASQL;Password=1234*;Persist Security Info=True;User ID=EmpleoLavoro;Extended Properties=""DSN=EmpleoProyectos;UID=PagoDirectoProcesos;DATABASE=EmpleoProyectos"";Initial Catalog=EmpleoProyectos;"
			Application(Hosting & "_ProyConnectionTimeout") = 300
			Application(Hosting & "_ProyCommandTimeout") = 300
			Application(Hosting & "_ProyCursorLocation") = 3
			Application(Hosting & "_ProyRuntimeUserName") = "EmpleoLavoro"
			Application(Hosting & "_ProyRuntimePassword") = "1234*"
			
		    'Conexion a la base Portal Oracle
		    Application(Hosting & "_PortalConnectionStringOracle") = "Provider=OraOLEDB.Oracle.1;user id=" &  Progetto & "; password=" &  pwd & "; data source=prod_cucho"
            Application(Hosting & "_PortalConnectionTimeoutOracle") = 300
		    Application(Hosting & "_PortalCommandTimeoutOracle") = 300
		    Application(Hosting & "_PortalCursorLocationOracle") = 3
		    
		    'Nombre del sitio Web para el link de salto
			Application(Hosting & "_PortalSitioProyectos") = "GestionEmpleo/empleo"		
			
			'Nombre del sitio Web para el link de salto
			Application(Hosting & "_PortalSitioPersonas") = "GestionEmpleo/Jefes"    
			
			' Conexion a la base del Datawarehouse en SQL
			Application(Hosting & "_DataWareHouseConnectionString") = "Provider=MSDASQL;Password=consulta;Persist Security Info=True;User ID=OlapReader;Extended Properties=""DSN=EmpleoDW;UID=OlapReader;DATABASE=Empleo_DW"";Initial Catalog=Empleo_DW;"
			Application(Hosting & "_DataWareHouseConnectionTimeout") = 300
			Application(Hosting & "_DataWareHouseCommandTimeout") = 300
			Application(Hosting & "_DataWareHouseCursorLocation") = 3
			Application(Hosting & "_DataWareHouseRuntimeUserName") = "OlapReader"
			Application(Hosting & "_DataWareHouseRuntimePassword") = "consulta"

		    ' Reporting Service - LLamada al servicio (link de salto)
			Application(Hosting & "_ReportingService") = "http://reportes.trabajo.gov.ar/ReportServerInternet/Pages/ReportViewer.aspx?"

		'======================================================================
		' sitio no definido
		'======================================================================
		case else 
			Response.Write "<br>Entorno o Hosting No Definido  - Error en OpenConn <br> ServerName: " & Request.ServerVariables("SERVER_NAME")
			Response.end
		
		'======================================================================
		'	Sitios anteriores
		'======================================================================
		'	select case ucase(Progetto)
		'	   case  "DEMO"
		'			CC.Open "Provider=SQLOLEDB.1;User ID=BA; password=BA; Initial Catalog=Portale; Data Source=Rael"
		'	   case else
		'	        CC.Open "Provider=OraOLEDB.Oracle.1;user id=" &  Progetto & "; password=" &  pwd & "; data source=portale_baggins"
		'	end select
		
		'===============================================================================================
		'Agregar las conexiones desde Internet e intranet cuando se hallan decidido los nombres
		'===============================================================================================
		'Ejemplo
		'Hosting = "cuilpublico"
		'Application(Hosting & "_JefesCPConnectionString") = "Provider=MSDASQL;Password=98760;Persist Security Info=True;User ID=Extranet;Extended Properties=""DSN=EmpleoPersonas;UID=Extranet;DATABASE=EmpleoPersonas"";Initial Catalog=EmpleoPersonas;"
		'Application(Hosting & "_JefesCPConnectionTimeout") = 60
		'Application(Hosting & "_JefesCPCommandTimeout") = 180
		'Application(Hosting & "_JefesCPCursorLocation") = 3
		'Application(Hosting & "_JefesCPRuntimeUserName") = "Extranet"
		'Application(Hosting & "_JefesCPRuntimePassword") = "98760"
		
	end select 
	
	' Determina ServerName
    If Len(ServerName) = 0 Then
	    ServerName = Request.ServerVariables("SERVER_NAME")
	    Application("SERVER_NAME") = ServerName
    End If
    ServerName = Request.ServerVariables("SERVER_NAME")

    
    'ServerWeb = "http://" & ServerName
    
    'Session("Server") = ServerWeb

'------------------------------------------------
'	Coneccion a Abrir: Base Seguridad (ON LINE)
'-------------------------------------------------
	With gConnSeguridad
	.ConnectionString = Application(ServerName & "_PortalConnectionString")	
	.ConnectionTimeout = Application(ServerName & "_PortalConnectionTimeout")
	.CommandTimeout = Application(ServerName & "_PortalCommandTimeout")
	.CursorLocation = Application(ServerName & "_PortalCursorLocation")
	.Open ,Application(ServerName & "_PortalRuntimeUserName"), Application(ServerName & "_PortalRuntimePassword")
	End With
    
'-----------------------------------------------------
'	Coneccion a Abrir: Base Seguridad (REPLICA)
'------------------------------------------------------
	With gConnSeguridadReplica
	.ConnectionString = Application(ServerName & "_PortalReplicaConnectionString")	
	.ConnectionTimeout = Application(ServerName & "_PortalReplicaConnectionTimeout")
	.CommandTimeout = Application(ServerName & "_PortalReplicaCommandTimeout")
	.CursorLocation = Application(ServerName & "_PortalReplicaCursorLocation")
	.Open ,Application(ServerName & "_PortalReplicaRuntimeUserName"), Application(ServerName & "_PortalReplicaRuntimePassword")
	End With
    
    
'--------------------------------------------------------------
'	Coneccion a Abrir: Base EmpleoPersonas (ONLINE)
'--------------------------------------------------------------
	With ConnEmpleoPersonas
		.ConnectionString = Application(ServerName & "_JefesConnectionString")	
		.ConnectionTimeout = Application(ServerName & "_JefesConnectionTimeout")
		.CommandTimeout = Application(ServerName & "_JefesCommandTimeout")
		.CursorLocation = Application(ServerName & "_JefesCursorLocation")
		.Open ,Application(ServerName & "_JefesRuntimeUserName"), Application(ServerName & "_JefesRuntimePassword")
		'Carpeta = Application(ServerName & "_DTSImportacion")
		'conn.Open "Provider=SQLOLEDB.1;Password=98760;Persist Security Info=False;User ID=Intranet;Initial Catalog=EmpleoPersonas;Data Source=RAEL"
		'Response.Write .ConnectionString
	End With
	

'--------------------------------------------------------------
'	Coneccion a Abrir: Base EmpleoPersonas (REPLICA)
'--------------------------------------------------------------
	With ConnEmpleoPersonasReplica
		.ConnectionString = Application(ServerName & "_JefesReplicaConnectionString")	
		.ConnectionTimeout = Application(ServerName & "_JefesReplicaConnectionTimeout")
		.CommandTimeout = Application(ServerName & "_JefesReplicaCommandTimeout")
		.CursorLocation = Application(ServerName & "_JefesReplicaCursorLocation")
		.Open ,Application(ServerName & "_JefesReplicaRuntimeUserName"), Application(ServerName & "_JefesReplicaRuntimePassword")
		'Carpeta = Application(ServerName & "_DTSImportacion")
		'conn.Open "Provider=SQLOLEDB.1;Password=98760;Persist Security Info=False;User ID=Intranet;Initial Catalog=EmpleoPersonas;Data Source=RAEL"
		'Response.Write .ConnectionString
	End With
	
	
'----------------------------------------
'	Coneccion a Abrir: Base EmpleoLavoro
'----------------------------------------	
	with connLavoro
	    .ConnectionString = Application(ServerName & "_AgendaConnectionString")	
	    .ConnectionTimeout = Application(ServerName & "_AgendaConnectionTimeout")
	    .CommandTimeout = Application(ServerName & "_AgendaCommandTimeout")
	    .CursorLocation = Application(ServerName & "_AgendaCursorLocation")
	    .Open ,Application(ServerName & "_AgendaRuntimeUserName"), Application(ServerName & "_AgendaRuntimePassword")
	end with

'------------------------------------------
'	Coneccion a Abrir: Base EmpleoProyectos
'------------------------------------------	
	with connProyectos
	    .ConnectionString = Application(ServerName & "_ProyConnectionString")	
	    .ConnectionTimeout = Application(ServerName & "_ProyConnectionTimeout")
	    .CommandTimeout = Application(ServerName & "_ProyCommandTimeout")
	    .CursorLocation = Application(ServerName & "_ProyCursorLocation")
	    .Open ,Application(ServerName & "_ProyRuntimeUserName"), Application(ServerName & "_ProyRuntimePassword")
	end with
	
'---------------------------------------
'	Coneccion a Abrir: Base Oracle
'---------------------------------------   
	'..Response.Write Application(ServerName & "_PortalConnectionStringOracle")	
	'..Response.End
	
    With CC
		.ConnectionString = Application(ServerName & "_PortalConnectionStringOracle")	
		.ConnectionTimeout = Application(ServerName & "_PortalConnectionTimeoutOracle")
		.CommandTimeout = Application(ServerName & "_PortalCommandTimeoutOracle")
		.CursorLocation = Application(ServerName & "_PortalCursorLocationOracle")
		.Open ',Application(ServerName & "_PortalRuntimeUserName"), Application(ServerName & "_PortalRuntimePassword")
		''Response.Write .ConnectionString
	End With
	
	Set CX = CC
	GOracleConnString = Application(ServerName & "_PortalConnectionStringOracle")

'----------------------------------------------------------------------------------------------------
'	Coneccion a Abrir: Base DataWareHouse SQL (** Se usa solo para reportes estadisticos ***)
'		No se habre la conexion aca, solo en los reportes estadisticos
'----------------------------------------------------------------------------------------------------
'	Set connEmpleoDataWareHouse = Server.CreateObject("adodb.connection") ' Objeto de Conexion a la Base EmpleoProyectos
'	with connEmpleoDataWareHouse
'	    .ConnectionString = Application(ServerName & "_DataWareHouseConnectionString")	
'	    .ConnectionTimeout = Application(ServerName & "_DataWareHouseConnectionTimeout")
'	    .CommandTimeout = Application(ServerName & "_DataWareHouseCommandTimeout")
'	    .CursorLocation = Application(ServerName & "_DataWareHouseCursorLocation")
'	    .Open ,Application(ServerName & "_DataWareHouseRuntimeUserName"), Application(ServerName & "_DataWareHouseRuntimePassword")
'	end with

'---------------------------------------
'	Links Varios
'---------------------------------------   
	Application("GPortalSitioProyectos") = Application(ServerName & "_PortalSitioProyectos")
	Application("LinkReportingService") = Application(ServerName & "_ReportingService")
	    
	'Response.Write ucase(Request.ServerVariables("SERVER_NAME"))
	'Response.Write Progetto
End If

%>