<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include virtual ="/include/SelAreaTerrBandi.asp"-->

<%
'**********************************************
' Crea l'elenco con i componenti di una classe
'**********************************************

sub ElencoComponentiClassi(nIdPers,nIdClasse,sDesAula,sBando,sSessione)
	dim sSQLCompClassi
	dim rsComClasse
	%>
	<table border="0" cellspacing="2" cellpadding="1" width="500">
		<tr align="center">
			<td align="middle" class="tbltext3" colspan="2">
				<b><%=sDesAula%></b>
			</td>
		</tr>
	</Table><BR>
	<table border="0" cellspacing="2" cellpadding="1" width="500">
		<tr align="center">
			<td align="left" class="tbltext1" width="150">
				<b>Bando : </b>
			</td>
			<td align="left" class="textBlack" width="350">
				<b><%=DecBando(sBando)%></b>
			</td>
		</tr>
		<tr align="center">
			<td align="left" class="tbltext1" width="150">
				<b>Sessione : </b>
			</td>
			<td align="left" class="textBlack" width="350">
				<b><%=DecFaseBando(sBando,sSessione)%></b>
			</td>
		</tr>
	</table>
	<br>		
	<table width="500" cellspacing="1" cellpadding="1" border="0">
		<tr class="sfondocomm">	
			<td align="left"><b>Cognome</b></td>
			<td align="left"><b>Nome</b></td>
			<!--td align="left"><b>Data di nascita</b></td-->
			<td align="left"><b>Codice fiscale</b></td>
		</tr>
	<%sSQLCompClassi = "SELECT A.COD_FISC,A.ID_PERSONA,A.COGNOME,A.NOME,A.DT_NASC FROM PERSONA A, COMPONENTI_CLASSE B WHERE " &_
			" A.ID_PERSONA = B.ID_PERSONA AND B.ID_CLASSE = " & nIdClasse &_
			" AND ( B.DT_RUOLOAL IS NULL OR SYSDATE <= B.DT_RUOLOAL ) " &_
			" AND B.COD_RUOLO ='DI' ORDER BY A.COGNOME,A.NOME,A.DT_NASC"
'PL-SQL * T-SQL  
SSQLCOMPCLASSI = TransformPLSQLToTSQL (SSQLCOMPCLASSI) 
	set rsComClasse = CC.Execute(sSQLCompClassi)
	do while not rsComClasse.EOF%>
		<tr class="tblsfondo">
			<td class="tblDett" width="173">
				<%if clng(rsComClasse("ID_PERSONA")) = clng(nIdPers) then 
					Response.Write "<Font color=red><B>"& rsComClasse("COGNOME") & "</B></Font>"
				else
					Response.Write rsComClasse("COGNOME")
				end if%>
			</td>
			<td class="tblDett" width="200">
				<%if clng(rsComClasse("ID_PERSONA")) = clng(nIdPers) then 
					Response.Write "<Font color=red><B>" & rsComClasse("NOME") & "</B></Font>"
				else
					Response.Write rsComClasse("NOME")
				end if%>
			</td>
			<!--td class="tblDett" width="197"-->
				<%'if clng(rsComClasse("ID_PERSONA")) = clng(nIdPers) then 
				'	Response.Write "<Font color=red><B>" & ConvDateToString(rsComClasse("DT_NASC")) & "</B></Font>"
				'else
			'		Response.Write ConvDateToString(rsComClasse("DT_NASC"))
			'	end if%>						
			<!--/td-->
			<td class="tblDett" width="197">
				<%if clng(rsComClasse("ID_PERSONA")) = clng(nIdPers) then 
					Response.Write "<Font color=red><B>" & rsComClasse("COD_FISC") & "</B></Font>"
				else
					Response.Write rsComClasse("COD_FISC")
				end if%>						
			</td>
		</tr>
		<%rsComClasse.MoveNext
	loop 
	rsComClasse.Close
	set rsComClasse = nothing%>				
	</table>

<%end sub%>
