<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%
' Data una stringa formatta con due valori ed una condizione 
' crea un Combo Box con all'interno l'elenco delle descrizioni
' dei record trovati. 
sub  CreateComboProj(sStringa)

	dim sValoriArray
	dim rsRec

	sValoriArray = Split(sStringa, "|", -1, 1)

'	Struttura variabile [ sValoriArray ]:
'		sValoriArray(0) = Valore da selezionare 
'		sValoriArray(1) = Nome del Combo		
'		sValoriArray(2) = Condizione di FILTRO/Ordinamento

	sSQL = "SELECT COD_PROG, DESCRIZIONE FROM PROGETTI_IL"
			
	if Trim(sValoriArray(2)) <> "" then
		sSQL = sSQL & " " & sValoriArray(2) 
	end if
	
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	set rsRec = CC.Execute(sSQL)
	
	' creo la Combo Box con i dati che query ha ritornato.
	'Response.Write "<SELECT ID='" & _
	'		sValoriArray(1) & "' class=textblack name='" & sValoriArray(1) & "'><OPTION value=></OPTION>"
	Response.Write "<SELECT ID='" & _
			sValoriArray(1) & "' class=textblack name='" & sValoriArray(1) & "'><OPTION value='%'>Tutti i Progetti</OPTION>"

	do until rsRec.EOF
		Response.Write "<OPTION "

		' Ricerca il record della combo da evidenziare.
		if sValoriArray(0) = rsRec.Fields("COD_PROG") then
			Response.Write "selected "
		end if
		
		Response.write "value ='" & rsRec.Fields("COD_PROG") & _
			"'> " & rsRec.Fields("COD_PROG")  & "</OPTION>"
		rsRec.MoveNext 
	loop
	
	Response.Write "</SELECT>"
	rsRec.Close
	set rsRec = Nothing
end sub
'
' Restituisce la decodifica cio� la descrizione del codice relativo alla tabella
'
function DecCodValProj(cod_prog)
	dim rsRec
	dim sSQL
	
	sSQL = "SELECT  COD_PROG, DESCRIZIONE FROM PROGETTI_IL " &_
		   " WHERE COD_PROG = '" & COD_PROG & "'"
	
	if IsObject(CX) then
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
		set rsRec=CX.Execute(sSQL)
	else
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
		set rsRec = CC.Execute(sSQL)
	end if 

	if not rsRec.Eof then
		DecCodValProj=rsRec.Fields("DESCRIZIONE")
		rsRec.Close
	else
		DecCodValProj=""
	end if

	set rsRec = Nothing

end function
%>
