<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->

<%
Function ControlStatoGrad()
dim sSQL
dim RsStatoGrad
set RsStatoGrad = server.CreateObject("ADODB.Recordset")
	sSQL = "SELECT STATUS_GRAD" &_
		" FROM DOMANDA_ISCR A, BANDO B, PROGETTO C, BANDO_POSTI D " &_
		" WHERE ID_PERSONA = " & Session("creator") &_
		" AND A.ID_BANDO=B.ID_BANDO" &_
		" AND B.ID_BANDO=D.ID_BANDO" &_
		" AND B.ID_PROJ=C.ID_PROJ" &_
		" AND A.ID_SEDE = D.ID_SEDE " &_
		" AND AREA_WEB='" & mid(Session("Progetto"),2) & "'"
		'Response.Write sSQL
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	RsStatoGrad.open sSql,CC, 3
	If not RsStatoGrad.eof then
		ControlStatoGrad = RsStatoGrad.Fields ("STATUS_GRAD")
	else
		ControlStatoGrad = ""
	end if
	RsStatoGrad.Close 
	set RsStatoGrad = nothing
end function

function ControlStatoUtente()
'Serve per controllare l'ESITO_GRAD di un utente per stabilere se � stato ammesso
dim sSQL
dim RsStato
set RsStato = server.CreateObject("ADODB.Recordset")
	sSQL = "SELECT ESITO_GRAD" &_
		" FROM DOMANDA_ISCR A, BANDO B, PROGETTO C" &_
		" WHERE ID_PERSONA = " & Session("creator") &_
		" AND A.ID_BANDO=B.ID_BANDO" &_
		" AND B.ID_PROJ=C.ID_PROJ" &_
		" AND AREA_WEB='" & mid(Session("Progetto"),2) & "'"
		'Response.Write sSQL
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	RsStato.open sSql,CC, 3
	If not RsStato.eof then
		ControlStatoUtente = RsStato.Fields ("ESITO_GRAD")
	else
		ControlStatoUtente = ""
	end if
	RsStato.Close 
	set RsStato = nothing
end function


function ControlEsclusi()
'Serve per controllare l'ESITO_GRAD di un utente per stabilere se � stato ammesso
dim sSQL
dim RsEsclusi
set RsEsclusi = server.CreateObject("ADODB.Recordset")
	sSQL = "SELECT COD_ESITO" &_
		" FROM DOMANDA_ISCR A, BANDO B, PROGETTO C" &_
		" WHERE ID_PERSONA = " & Session("creator") &_
		" AND A.ID_BANDO=B.ID_BANDO" &_
		" AND B.ID_PROJ=C.ID_PROJ" &_
		" AND AREA_WEB='" & mid(Session("Progetto"),2) & "'"
		'Response.Write sSQL
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	RsEsclusi.open sSql,CC, 3
	If not RsEsclusi.eof then
		ControlEsclusi = RsEsclusi.Fields ("COD_ESITO")
	else
		ControlEsclusi = ""
	end if
	RsEsclusi.Close 
	set RsEsclusi = nothing
end function
	
function CtlBandoAttivo(idpers)
dim sSQL
dim RsBando
set RsBando = server.CreateObject("ADODB.Recordset")
	sSQL = "SELECT a.ID_BANDO" &_
		" FROM DOMANDA_ISCR A, BANDO B, PROGETTO C" &_
		" WHERE ID_PERSONA = " & idpers &_
		" AND A.ID_BANDO=B.ID_BANDO" &_
		" AND B.ID_PROJ=C.ID_PROJ" &_
		" AND AREA_WEB='" & mid(Session("Progetto"),2) & "'" &_
		" AND ESITO_GRAD='S'"

'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	RsBando.open sSql,CC, 3
	If not RsBando.eof then
		CtlBandoAttivo = RsBando.Fields ("ID_BANDO")
	else
		CtlBandoAttivo = 0
	end if
	RsBando.Close 
	set RsBando = nothing

end function
%>
