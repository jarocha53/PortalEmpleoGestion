		
// function ValidateInputNumber(String) 
// 			verifica se la stringa "String" sia di tipo numerico 0-9
// Return 	false se la stringa e' del tipo richiesto
//			true 
// Autore   Undo 
// Date		??-??-1999			
// Update   MDF 11-03-1999

// function function ValidateInputIndirizzi(InputString)
// 			verifica se la stringa contiene lettere, numeri,
//			lettere accentate + (apostropo  spazio  punto virgola e / )
// Return 	false se la stringa e' del tipo richiesto
//			true 
// Autore   Alemarti 
// Date		15-03-1999			
// Update   ??? ??-??-1999
					
// function function ValidateInputAlfaNum(String)
// 			verifica se la stringa contiene lettere, numeri,
//			lettere accentate.
// Return 	false se la stringa e' del tipo richiesto
//			true 
// Autore   Undo 
// Date		??-??-1999			
// Update   MDF 11-03-1999

// function function ValidateInputStringWithNumber(String)
// 			verifica se la stringa contiene lettere,numeri,
//			lettere accentate + (apostropo  spazio  punto)
// Return 	false se la stringa e' del tipo richiesto
//			true 
// Autore   MDF
// Date		11-03-1999			
// Update   

// function function ValidateInputStringWithNumbereChar(String)
// 			verifica se la stringa contiene lettere,numeri,
//			lettere accentate + (apostrofo  spazio  punto virgola trattino : ; () [] )
// Return 	false se la stringa e' del tipo richiesto
//			true 
// Autore   MDF
// Date		11-03-1999			
// Update   
	
// function function ValidateInputStringWithOutNumber(String)
// 			verifica se la stringa contiene lettere, 
//			lettere accentate + (apostropo spazio)
// Return 	false se la stringa e' del tipo richiesto
//			true 
// Autore   Undo 
// Date		??-??-1999			
// Update   MDF 11-03-1999

// function ValidateInputStringMail(String)
// 			verifica se la stringa contiene lettere, numeri, 
//			lettere accentate + (apostropo spazio)
// Return 	false se la stringa e' del tipo richiesto
//			true 
// Autore   MDF
// Date		11-03-1999			
// Update   

// function ChechEmptyString(String)
// 			verifica se la stringa e' vuota
// Return 	false se la stringa e' del tipo richiesto
//			true 
// Autore   MDF
// Date		11-03-1999			
// Update   	
					
// function ChechSingolChar(InputString,Carattere)
// 			verifica se all'interno della stringa "InputString" 
//          vi sia ilcarattere "Carattere" richiesto
// Return 	false se la stringa e' del tipo richiesto
//			true 
// Autore   MDF
// Date		11-03-1999			
// Update   

// function ChechMultiChar(InputString,Caratteri)
// 			verifica se all'interno della stringa "InputString"
//           vi sia almeno uno dei caratteri di "Caratteri".
// Return 	false se la stringa e' del tipo richiesto
//			true 
// Autore   MDF
// Date		11-03-1999			
// Update   

// function TRIM(value)
// 			Toglie gli spazi di una stringa all'inizio ,alla fine e 
//			compatta pi� spazi vuoti consecutivi in uno 
// Return 	Stringa
// Autore   SE
// Date		23-07-2001			
// Update 
  
// function ValidateInputStringIsValid(InputString)
//			verifica se all'interno della stringa "InputString" 
//          vi siano lettere accentate o caratteri non permessi(\/*.<>?:).
// Return 	false se la stringa e' del tipo richiesto
//			true 
// Autore	PCG
// Date		20-06-2002	
// Update 

// function CheckLenTxArea(ObjTxArea,ObjTxNumCar,MaxLen)
//			Contolla che nell'oggetto ObjTxArea il numero di caratteri digitati
//			non superi il valore massimo pari a MaxLen, e nell'oggetto ObjTxNumCar
//			compare il numero di caratteri che possono essere ancora digitati. 
//	ObjTxArea		: oggetto TextArea
//	ObjTxNumCar	: oggetto Input type="text"
//	MaxLen			: intero
//
//	NOTA: la funzione deve essere richiamata nell'oggetto TextArea attraverso
//      l'evento OnKeyUp="JavaScript:CheckLenTxArea(nome_oggetto_textarea,
//										nome_oggetto_text,num_max_caratteri)"
//      
// 
//	Autore  Sacco Michele
//	Date	09-08-2002	
 


// function CheckLenTextArea(ObjTxArea,ObjLabelNumCar,MaxLen)
//			Contolla che nell'oggetto ObjTxArea il numero di caratteri digitati
//			non superi il valore massimo pari a MaxLen, e nell'oggetto ObjLabelNumCar
//			compare il numero di caratteri che possono essere ancora digitati. 
//	ObjTxArea		: oggetto TextArea
//	ObjLabelNumCar	: oggetto Label
//	MaxLen			: intero
//
//	NOTA: la funzione deve essere richiamata nell'oggetto TextArea attraverso
//      l'evento OnKeyUp="JavaScript:CheckLenTextArea(nome_oggetto_textarea,
//										nome_oggetto_label,num_max_caratteri)"
//  Attenzione che nel chiamare la funzione inserire nome_oggetto_label e NO
//			   document.nomeForm.nome_oggetto_label che non viene riconosciuto
//			   come un oggetto del form
//	NOTA: per l'oggetto Label � importante dichiarare l'id
// 
//  Ex HTML
//         Max <label id="nome_oggetto_label">255</label> caratteri
//         inoltre sempre nel codice HTML il tag di apertura e chiusura di
//         TEXTAREA inserirlo tutto sulla stessa riga, altrimenti pu� dare alcuni problemi:
//				non inizia a scrivere dal primo carattere.
//	
//	Autore  Sacco Michele
//	Date	19-09-2002	

// function ValidatePath(InputString)
// 			verifica se la stringa contiene lettere, numeri,
//			ed i seguenti caratteri . _ / :
// Return 	true se la stringa e' del tipo richiesto
//			false in caso contrario 
// Autore   Sacco Michele 
// Date		20/10/2002			

// function ValidateNameUploadFileIsValid(InputString)
//			verifica se all'interno della stringa "InputString"
//			il nome del file da inviare tramite Upload
//          contenga lettere accentate.
// Return 	false se la stringa e' del tipo richiesto
//			true 
// Autore	PCG
// Date		20-06-2002	
// Update 


//---------------------------------Start function--------------------------------------------- //
function ChechSingolChar(InputString,Carattere)
{
    var anyString = InputString;
    var anyCarattere = Carattere;
	for ( var i=0; i<=anyString.length-1; i++ ){
		if ((anyString.charAt(i) == anyCarattere)) {
			return(false);
		}
	}
	return(true);
}
//---------------------------------Start function--------------------------------------------- //
function ChechMultiChar(InputString,Caratteri)
{
    var anyString = InputString;
    var anyCaratteri = Caratteri;
	for ( var i=0; i<=anyString.length-1; i++ ){
		for ( var k=0; k<=anyCaratteri.length-1; k++ ){
			if ((anyString.charAt(i) == anyCaratteri.charAt(k))) {
				return(false);
			}
		}			
	}
	return(true);
}



//---------------------------------Start function--------------------------------------------- //
function ChechEmptyString(InputString)
{
    var anyString = InputString;
		if  (anyString == ""){
			return(true);
		}
		else{
			return(false);
		}
}

//---------------------------------Start function--------------------------------------------- //
function ValidateInputNumber(InputString)
{
    var anyString = InputString;
	for ( var i=0; i<=anyString.length-1; i++ ){
		if ( (anyString.charAt(i) >= "0") && (anyString.charAt(i) <= "9") ){
		}
		else{
			return(false);
		}
	}
	return(true);
}


//---------------------------------Start function--------------------------------------------- //
function ValidateInputAlfaNum(InputString)
{
    var anyString = InputString;
	
	for ( var i=0; i<=anyString.length-1; i++ ){
		if ( ((anyString.charAt(i) >= "A") && (anyString.charAt(i) <= "Z")) || 
	         ((anyString.charAt(i) >= "a") && (anyString.charAt(i) <= "z")) || 
			 ( (anyString.charAt(i) >= "0") && (anyString.charAt(i) <= "9") )  || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "?") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "!") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "i") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�")  || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == ".") ||
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�")) {
		}
		else{
			return(false);
		}
	}
	return(true);
}

//---------------------------------Start function--------------------------------------------- //
function ValidateInputStringWithNumber(InputString)
{
    var anyString = InputString;
	
	for ( var i=0; i<=anyString.length-1; i++ ){
		//alert (anyString.charAt(i));
		if ( ((anyString.charAt(i) >= "A") && (anyString.charAt(i) <= "Z")) || 
	         ((anyString.charAt(i) >= "a") && (anyString.charAt(i) <= "z")) || 
			 ( (anyString.charAt(i) >= "0") && (anyString.charAt(i) <= "9") )  || 
			 (anyString.charAt(i) == " ") || (anyString.charAt(i) == "'") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "?") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "!") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "i") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�")  || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == ".") ||
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�")){
		}
		else{
			return(false);
		}
	}
	return(true);
}


//---------------------------------Start function--------------------------------------------- //
function ValidateInputStringWithNumbereChar(InputString)
{
    var anyString = InputString;
	
	for ( var i=0; i<=anyString.length-1; i++ ){
		if ( ((anyString.charAt(i) >= "A") && (anyString.charAt(i) <= "Z")) || 
	         ((anyString.charAt(i) >= "a") && (anyString.charAt(i) <= "z")) || 
			 ( (anyString.charAt(i) >= "0") && (anyString.charAt(i) <= "9") )  || 
			 (anyString.charAt(i) == " ") || (anyString.charAt(i) == "'") || 
	 		 (anyString.charAt(i) == ",") || (anyString.charAt(i) == "-") || 
	 		 (anyString.charAt(i) == ":") || (anyString.charAt(i) == ";") || 
	 		 (anyString.charAt(i) == "(") || (anyString.charAt(i) == ")") || 
 	 		 (anyString.charAt(i) == "[") || (anyString.charAt(i) == "]") || 
			 			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "?") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "!") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "i") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�")  || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == ".") ||
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == ".") ){
		}
		else{
			return(false);
		}
	}
	return(true);
}

//---------------------------------Start function--------------------------------------------- //
function ValidateInputIndirizzi(InputString)
{
    var anyString = InputString;
	
	for ( var i=0; i<=anyString.length-1; i++ ){
		if ( ((anyString.charAt(i) >= "A") && (anyString.charAt(i) <= "Z")) || 
	         ((anyString.charAt(i) >= "a") && (anyString.charAt(i) <= "z")) || 
			 ( (anyString.charAt(i) >= "0") && (anyString.charAt(i) <= "9") )  || 
			 (anyString.charAt(i) == " ") || (anyString.charAt(i) == "'") || 
			 			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "!") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "i") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == ".") ||
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == ".") ||
			 (anyString.charAt(i) == "/") || (anyString.charAt(i) == ",") || (anyString.charAt(i) == "�")){
		}
		else{
			return(false);
		}
	}
	return(true);
}



//---------------------------------Start function--------------------------------------------- //
function ValidateInputNote(InputString)
{
    var anyString = InputString;
	
	for ( var i=0; i<=anyString.length-1; i++ ){
		if ( ((anyString.charAt(i) >= "A") && (anyString.charAt(i) <= "Z")) || 
	         ((anyString.charAt(i) >= "a") && (anyString.charAt(i) <= "z")) || 
			 ( (anyString.charAt(i) >= "0") && (anyString.charAt(i) <= "9") )  || 
			 (anyString.charAt(i) == " ") || (anyString.charAt(i) == "'") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "?") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "!") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "i") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�")  || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == ".") ||
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == ".") ||
			 (anyString.charAt(i) == ";") || (anyString.charAt(i) == ":") ||
			 (anyString.charAt(i) == "/") || (anyString.charAt(i) == ",")){
		}
		else{
			return(false);
		}
	}
	return(true);
}


//---------------------------------Start function--------------------------------------------- //
function ValidateInputStringWithOutNumber(InputString)
{
    var anyString = InputString;
	
	for ( var i=0; i<=anyString.length-1; i++ ){
		if ( ((anyString.charAt(i) >= "A") && (anyString.charAt(i) <= "Z")) || 
	         ((anyString.charAt(i) >= "a") && (anyString.charAt(i) <= "z")) || 
			 (anyString.charAt(i) == " ") || (anyString.charAt(i) == "'") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "?") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "!") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "i") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�")  || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == ".") ||
			 (anyString.charAt(i) == "�") || (anyString.charAt(i) == "�"))      {
		}
		else{
			return(false);
		}
	}
	return(true);
	
}

//---------------------------------Start function--------------------------------------------- //
function ValidateInputStringMail(InputString)
{
    var anyString = InputString;
	
	for ( var i=0; i<=anyString.length-1; i++ ){
		if ( ((anyString.charAt(i) >= "A") && (anyString.charAt(i) <= "Z")) || 
	         ((anyString.charAt(i) >= "a") && (anyString.charAt(i) <= "z")) || 
	         (anyString.charAt(i) >= "0") && (anyString.charAt(i) <= "9") ||
			 (anyString.charAt(i) == "@") || (anyString.charAt(i) == ".") 
			 ){
		}
		else{
			return(false);
		}
	}
	return(true);
	
}

//---------------------------------Start function--------------------------------------------- //
function StringUpper(String)
{
	string.toUpperCase()
}

//---------------------------------Start function--------------------------------------------- //
function StringLower(String)
{
	string.toLowerCase()
}


//---------------------------------Start function--------------------------------------------- //

function TRIM(value) {
	var outString
	var outAppoggio
	var nPosFin
	var atPos
	outAppoggio = ""
	outString = ""
	nPosFin = 0
      //  alert (value.length)
	if (value.length > 0 ) {
		for (i=0;i<value.length;i++) {
			if ((value.charAt(i) == " ") && (i == 0)) {
				z=0
				while (value.charAt(z)==" ") {
					z++
				} 
				i = z
			}
			if ( (value.charAt(i) == " ") && (i == (value.length-1))  ) {
				z=value.length-1
				while (value.charAt(z)== " ") {
					nPosFin++
					z--
				} 
				
			}
		
			if (value.charAt(i) == " ") {
				if (value.charAt(i+1)!= " "){
				  outAppoggio = outAppoggio + value.charAt(i)
				 } 
			} else
				
			  outAppoggio = outAppoggio + value.charAt(i)
			

		}
		//alert ("*" + outAppoggio + "*" + outAppoggio.length + nPosFin)
                if (nPosFin > 0) {
                    
		    for (i=0;i<(outAppoggio.length)-1;i++) {
			  outString = outString + outAppoggio.charAt(i)

		//	alert (outString)
		    }
		 } else 
                    outString=outAppoggio
		
	} else
		outString=""
	return outString 
}
//---------------------------------Start function--------------------------------------------- //
function ValidateEmail(String)
{
		var anyString = String;
		var invalidChars= " /:,;'";
				
		
		for (i=0; i<invalidChars.length; i++){
			badChar = invalidChars.charAt(i)
			if (anyString.indexOf(badChar,0) > -1) {
				//alert("Email obbligatoria")
				return false
			}
		}			
		atPos = (anyString.indexOf("@",1))
		if (atPos == -1) {
			//alert("Inserire nel campo E-Mail un valore valido")
			return false
		}
		if (anyString.indexOf("@",atPos+1) > -1){
			//alert("Inserire nel campo E-Mail un valore valido")
			return false
		}
		periodPos = (anyString.indexOf(".",atPos))
		if (periodPos == -1){
			//alert("Inserire nel campo E-Mail un valore valido")
			return false
		}
		if (periodPos+3 > (anyString.length)){
			//alert("Inserire nel campo E-Mail un valore valido")
			
			return false
		}
		return(true);
	
}

//---------------------------------Start function--------------------------------------------- //
function ValidateInputStringIsValid(InputString)
{
    var anyString = InputString;
	
	for ( var i=0; i<=anyString.length-1; i++ ){
	
		if ((anyString.charAt(i) == ":") || (anyString.charAt(i) == "*") ||
			(anyString.charAt(i) == "?") || (anyString.charAt(i) == "|") ||
			(anyString.charAt(i) == "<") || (anyString.charAt(i) == ">") ||
			(anyString.charAt(i) == "/") || (anyString.charAt(i) == "&") ||
			(anyString.charAt(i) == "\\") || (anyString.charAt(i) == '"') ||  
			(anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			(anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			(anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			(anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			(anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			(anyString.charAt(i) == "�") || (anyString.charAt(i) == ".") )
			{
			alert ("No esta permitida la elecci�n de un nombre \nque comprenda los caracteres: ': * \\ / . ? | < >' \n ni las letras acentuadas.");
			return(false)
			}
	}
	return(true);
}

//---------------------------------Start function--------------------------------------------- //
function CheckLenTxArea(ObjTxArea,ObjTxNumCar,MaxLen)
{
var NumCaratt;
	if (ObjTxArea.value.length > MaxLen)
	{
		alert("No puede ingresar mas de " + MaxLen + " caracteres");
		ObjTxArea.value = ObjTxArea.value.substring(0,MaxLen);
		NumCaratt = 0;
	}
	else
	{
		NumCaratt = MaxLen - ObjTxArea.value.length;
	}
	ObjTxNumCar.value = NumCaratt;
	return(true);
}

//---------------------------------Start function--------------------------------------------- //
function CheckLenTextArea(ObjTxArea,ObjLabelNumCar,MaxLen)
{
var NumCaratt;
	if (ObjTxArea.value.length > MaxLen)
	{
		alert("No puede ingresar mas de " + MaxLen + " caracteres");
		ObjTxArea.value = ObjTxArea.value.substring(0,MaxLen);
		NumCaratt = 0;
	}
	else
	{
		NumCaratt = MaxLen - ObjTxArea.value.length;
	}
	ObjLabelNumCar.innerHTML = NumCaratt;
}
//---------------------------------Start function--------------------------------------------- //
function ValidatePath(InputString)
{
    var anyString = InputString;
	
	for ( var i=0; i<=anyString.length-1; i++ )
	{
		if ( ((anyString.charAt(i) >= "A") && (anyString.charAt(i) <= "Z")) || 
	         ((anyString.charAt(i) >= "a") && (anyString.charAt(i) <= "z")) || 
			 ((anyString.charAt(i) >= "0") && (anyString.charAt(i) <= "9")) || 
			 (anyString.charAt(i) == ".") || (anyString.charAt(i) == "/") ||
			 (anyString.charAt(i) == "_") || (anyString.charAt(i) == ":")  ||
			 (anyString.charAt(i) == " ") || (anyString.charAt(i) == "\\") )
		{
		}
		else
		{
			return(false);
		}
	}
	return(true);
}

//---------------------------------Start function--------------------------------------------- //
function ValidateNameUploadFileIsValid(InputString)
{
	var sSepInput = InputString.split("\\");
	sFile = sSepInput[sSepInput.length-1];
	var anyString = sFile;
	
	for ( var i=0; i<=anyString.length-1; i++ ){
	
		if ((anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			(anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			(anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			(anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			(anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			(anyString.charAt(i) == "�") )
			{
			alert ("No esta permitido el envio de un documento cuyo nombre contenga caracteres acentuados.");
			return(false)
			}
	}
	return(true);
}