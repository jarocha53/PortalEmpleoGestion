
<%
function ValInfPers(sIdPers,sConn)
'
	valInfPers = 0
'
' Impostazione della data di rilevazione dei dati per la persona
' ed eventuale aggiornamento dell'Ente Certificatore
'
	if Session("Progetto")="/PLAVORO" then
		sSQLval="UPDATE PERSONA" &_
				" SET DT_ULT_RIL =" & convdatetodb(now()) & _
				" , ID_SEDE=NULL " & _
				" WHERE ID_PERSONA = " & sIdPers
	else
		sSQLval="UPDATE PERSONA" &_
				" SET DT_ULT_RIL =" & convdatetodb(now()) & _
				" WHERE ID_PERSONA = " & sIdPers
	end if
'
' Eseguo l'aggiornamento
'
	Errore = EseguiNoC(sSQLval,sConn)
'
	if Errore <> "0" then
		valInfPers=Errore
	end if
'
end function
%>
