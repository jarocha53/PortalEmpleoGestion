function ControllCodFisc(TipoPers,CodFisc)
{
//
// Se alla funzione passiamo il parametro relativo alla Tipologia della persona
// verifico la congruenza con i primi caratteri
// Per tutti i casi fa comunque il controllo sul CHECKDIGIT.
//	
	//Argentina
	var txtPrefijo
	var txtSep1
	var txtSep2
	var txtDoc
	var txtLenCodFisc

	//Verifica lunghezza
	
	txtLenCodFisc=CodFisc.length
	 
	if (txtLenCodFisc!=11)
		{
			alert("La longitud De Cuil debe ser igual a 11");
			return false;
		}
	
	//verifica primi due digit [20, 23, 24, 27, 30]
	txtPrefijo=CodFisc.substr(0,2) 
		//Prefijo: Non numerico
		if (isNaN(txtPrefijo))
			{
				alert("El Prefijo de Cuil debe ser numerico");
				return false;
			}
		//Prefijo: Valore non ammesso
		if ((txtPrefijo!="20")&&(txtPrefijo!="23")&&(txtPrefijo!="24")&&(txtPrefijo!="27")&&(txtPrefijo!="30"))
			{
				alert("Prefijo de Cuil Erroneo");
				return false;
			}
	// verifica congruenza Prefijo con Tipologia (ma non duplicato)
	//if ((TipoPers>"") && (txtPrefijo!="24"))
		
		if (TipoPers>"")
			{ 
			if ((txtPrefijo!="24")&&(txtPrefijo!="23"))
			{
				if (TipoPers=="M")
				{
					if (txtPrefijo!="20")
					{
						alert("Prefijo de Cuil no corresponde con la tipologia");
						return false;
						}
					}
					
				if (TipoPers=="F")
				{
					if (txtPrefijo!="27")
					{
						alert("Prefijo de Cuil no corresponde con la tipologia");
						return false;
						}
					}
	
					if (TipoPers=="E")
				{
					if (txtPrefijo!="30")
					{
						alert("Prefijo de Cuil no corresponde con la tipologia");
						return false;
						}
					}			
						
				/*if ((TipoPers=="M" && txtPrefijo!="20")||(TipoPers=="F" && txtPrefijo!="27")||(TipoPers=="X" && txtPrefijo!="23")||(TipoPers=="E" && txtPrefijo!="30"))
				{
				alert("Prefijo de Cuil no corresponde con la tipologia");
				return false;
				}*/
			}
			}
		
	// N.ro documento [numerico]
	txtDoc=CodFisc.substr(2,8)  
		if (isNaN(txtDoc))
			{
				alert("CUIL/CUIT Erroneo");
				return false;
			}
	
	txtCheck=CodFisc.substr(10,1)
	
	//alert ("Prefijo "+ txtPrefijo)
	//alert ("Nro Doc "+ txtDoc)
	//alert ("Check   "+ txtCheck)		

	if (!ControllChkCodFisc(CodFisc))
		{
			return false;
		}

    return true
}

function ControllChkCodFisc(CodFisc){

	//Argentina
	var txtVal
	var nValore = 0;
	var nFattore = 0;
	var nResto = 0;
	var nResto1 = 0;			
	var txtCheckDigit;
	
	for (i = 0; i < 10; i++){
		txtVal=CodFisc.substr(i,1)
		if (i<4){
			nFattore=(6-(i+1))
		}	
		else
		{
			nFattore=(10-(i+1))+2
		}

		nValore=nValore + (txtVal * nFattore)
	}
	
	nResto = nValore % 11 
	
	txtCheckDigit=CodFisc.substr(10,1)
	
	if (nResto==0){
		if(txtCheckDigit!=0){
			alert("Digito Verificador de Cuil Erroneo")
			return false
		}
	}
	else
	{
		nResto1 = (11 - nResto)
		if (txtCheckDigit != nResto1)
			{
			alert("Digito Verificador de Cuil Erroneo")
			return false
			}
	}	
    return true
}



function ControlaCuil(Cuil)
{
//
// Se alla funzione passiamo il parametro relativo alla Tipologia della persona
// verifico la congruenza con i primi caratteri
// Per tutti i casi fa comunque il controllo sul CHECKDIGIT.
//	
	//Argentina
	var txtPrefijo
	var txtSep1
	var txtSep2
	var txtDoc
	var txtLenCodFisc

	//Verifica lunghezza
	
	txtLenCodFisc=CodFisc.length
	 
	if (txtLenCodFisc!=11)
		{
			alert("La longitud De Cuil debe ser igual a 11");
			return false;
		}
	
	//verifica primi due digit [20, 23, 24, 27, 30]
	txtPrefijo=CodFisc.substr(0,2) 
		//Prefijo: Non numerico
		if (isNaN(txtPrefijo))
			{
				alert("El Prefijo de Cuil debe ser numerico");
				return false;
			}
		//Prefijo: Valore non ammesso
		if ((txtPrefijo!="20")&&(txtPrefijo!="23")&&(txtPrefijo!="24")&&(txtPrefijo!="27")&&(txtPrefijo!="30"))
			{
				alert("Prefijo de Cuil Erroneo");
				return false;
			}
	// verifica congruenza Prefijo con Tipologia (ma non duplicato)
		/*if ((TipoPers>"") && (txtPrefijo!="24"))
			{ if ((TipoPers=="M" && txtPrefijo!="20")||(TipoPers=="F" && txtPrefijo!="27")||(TipoPers=="X" && txtPrefijo!="23")||(TipoPers=="E" && txtPrefijo!="30"))
				{
				alert("Prefijo de Cuil no corresponde con la tipologia");
				return false;
				}
			}*/
		
	// N.ro documento [numerico]
	txtDoc=CodFisc.substr(2,8)  
		if (isNaN(txtDoc))
			{
				alert("CUIL/CUIT Erroneo");
				return false;
			}
	
	txtCheck=CodFisc.substr(10,1)
	
	//alert ("Prefijo "+ txtPrefijo)
	//alert ("Nro Doc "+ txtDoc)
	//alert ("Check   "+ txtCheck)		

	if (!ControllChkCodFisc(CodFisc))
		{
			return false;
		}

    return true
}