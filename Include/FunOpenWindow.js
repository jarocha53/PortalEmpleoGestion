	//Funzione che apre una popup delle notizie
	//I parametri che devono essere passati a questa funzione hanno il seguente significato:
	//idnews = id della notizia
	//prj = nome del progetto 
	function Apri(idnews,prj) {
		var urlnews; 
		urlnews = "/" + prj + "/pgm/news/new_leggi.asp?id=" + idnews
		  var hWnd=window.open(urlnews,"News","toolbar=no,width=430,left=1,top=1,height=550,directories=no,location=no,status=no,statusbar=no,resizable=no,menubar=no,scrollbars=yes");
		  if(!hWnd.opener) hWnd.opener=self;
		  if(hWnd.focus!=null) hWnd.focus();
	}
	
	//Funzione che apre una popup secondo determinate dimensioni
	//I parametri che devono essere passati a questa funzione hanno il seguente significato:
	//URL = Path della pagina da richiamare			ES. "/PROGETTO/home.asp"
	//sName Optional = Stringa che specifica il nome della finestra. 
	//                Il nome � usato come valore per l'attributo TARGET.
	//                _blank si apre in una nuova finestra . 
	//                _media  si apre nell'area HTML della Media Bar. Solo in IE6 or superiori. 
	//                _parent si apre nel frame corrente del parent. Se non esiste � uguale a _self. 
	//                _search si apre nella parte di browser riservata alla ricerca. Solo in IE5 or superiori. 
	//                _self si apre nella stessa finestra finestra . 
	//                _top si apre in ogni framesets che viene caricato. Se non esiste � uguale a _self. 
	//nWidth= la larghezza della pagina in pixel	ES. "300px"
	//nHeight= la lunghezza della pagina in pixel	ES. "200px"
	//bScroll= la scelta di visualizzare o meno la	ES. "yes" se si desidera la barra (scritto minuscolo)
	//			barra di scorrimento					"no" se non si desidera la barra (scritto minuscolo)
	//bResize= la scelta di poter ridimensionare o 	ES. "yes" se si pu� fare (scritto minuscolo)
	//			meno la finestra					        "no" se non si pu� fare (scritto minuscolo)

	function OpenWindow(sURL,sName,nWidth,nHeight,bScroll,bResize) {   
		  var f,formatta;
		  formatta = "toolbar=no,width=" + nWidth + ",height=" + nHeight + ",scrollbars=" + bScroll  + ",resizable=" + bResize + ",left=1,top=1,directories=no,location=no,status=no,statusbar=no,menubar=no"
		  f=window.open(sURL,sName,formatta);
		  if(!f.opener) f.opener=self;
		  if(f.focus!=null) f.focus();
	}
	
	//Funzione che apre una popup del documentale
	//I parametri che devono essere passati a questa funzione hanno il seguente significato:
	//file = indirizzo completo del file ES. "/PROGETTO/Testi/SistDoc/Documentale/Cartella/nomefile.doc"
	//titolo = titolo da dare alla popup ES. "Normativa"

	function Leggi(file,titolo) { 
		var aPrjdoc = file.split("/");		
		var sPrjdoc = aPrjdoc[1]
		var urldoc; 
		urldoc = "/" + sPrjdoc + "/pgm/Documentale/DOC_Leggi.asp?file=" + file + "&tit=" + titolo
		//alert(urldoc);
		var DochWnd=window.open(urldoc,"Doc","toolbar=no,width=620,left=1,top=1,height=550,directories=no,location=no,status=no,statusbar=no,resizable=yes,menubar=no,scrollbars=yes");
		  if(!DochWnd.opener) DochWnd.opener=self;
		  if(DochWnd.focus!=null) DochWnd.focus();
	}
	
	//Funzione che apre una popup da cui si puo effettuare la scelta
	//di visualizzare o scaricare il documento
	//Il parametro che devono viene passato a questa funzione ha il seguente significato:
	//nfile = indirizzo completo del file ES. "/PROGETTO/Testi/nomefile.doc"

	function Scarica(nfile) {
		var urldown;   
		urldown = "/Pgm/Documentale/DOC_Download.asp?nfile=" + nfile
		var DochSCR=window.open(urldown,"Down","toolbar=no,width=350,left=1,top=1,height=250,directories=no,location=no,status=no,statusbar=no,resizable=no,menubar=no,scrollbars=no");
	    if(!DochSCR.opener) DochSCR.opener=self;
	    if(DochSCR.focus!=null) DochSCR.focus();
	}
	
	//Funzione che apre una popup con il report selezionato
	//Il parametro che devono viene passato a questa funzione ha il seguente significato:
	//nfile = indirizzo completo del file ES. "/PROGETTO/DocPers/Estrattore/nomefile.xls"

	function ScaricaReport(sIndRep)
	{
		var sIndRep;
		fin=window.open(sIndRep,"","toolbar=0;location=0,directories=0,status=0,scrollbars=1,resizable=1,copyhistory=0,width=450,height=340,screenX=200,screenY200");
	}
