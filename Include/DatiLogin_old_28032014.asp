<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include Virtual="/include/openconn.asp"-->
<!--#include Virtual="/Util/Dbutil.asp"-->

<% on error resume next

%>
<TABLE border="0" width="500" cellspacing="0" cellpadding="0" valign="top" align="center">
	<%
	' Va a crear la tabla solo cuando entra por primera vez, la variable de session "LastAccess" 
	' se blanquea al final de este asp.  siempre. En el primer Ingreso si tiene un valor.
	if session ("LastAccess") <> ""  then
		sAccesso = split (session("LastAccess")," ")
		sData = sAccesso(0)
		sOra  = sAccesso(1)
		%>
		<tr>
			<td align="center" class="tbltext">
			   <br><br>
			</td>
		</tr>
		<tr>
			<td align="center" class="tbltext"> &Uacute;ltimo acceso efectuado el<b class="textred"> 
				<%=sData%></b> a las <b class="textred"><%=sOra%></b>
			</td>
		</tr>
		<tr>
			<td align="center" class="tbltext">
			   <br>
			</td>
		</tr>
		<%
		'----------------------------------------------------------------------------
		
		err.number = 0
		nCodTimpr = 3
		if  session("creator")<> "" and Session("tipopers")="S" and Session("rorga")="OP" and nCodTimpr = 3 then
			sOPECI = "SI"
		end if

		'----------------------------------------------------------------------------
		'Comienza a buscar los datos consultando las bases
		if  sOPECI="SI" then
			if  session ("creator") <> 0 then
				'-----------------------------------------------------------
				'HISTORIAS LABORALES OFICINA: Personas con Nivel Educativo o Postulaciones 
				Set rsIscritti = Server.CreateObject("ADODB.RECORDSET")
				SQL = "SELECT count(distinct p.id_persona) FROM persona p with (nolock), stato_occupazionale so with (nolock) "
				SQL = SQL & "WHERE p.id_persona=so.id_persona "
				SQL = SQL & "AND  so.id_cimpiego = " & session("creator") & " "
				SQL = SQL & "AND (ind_status = '0' OR (ind_status = '2' "
				SQL = SQL & "AND ind_status not in ( SELECT ind_status "
				SQL = SQL & "FROM STATO_OCCUPAZIONALE with (nolock) WHERE p.id_persona = id_persona "
				SQL = SQL & "AND ind_status = '0' ))) "

				rsIscritti.open SQL, CC, 3, 3
				'Response.Write "<br><br>" & SQL

				StampaIscritti =  err.number 
				err.number = 0
				'-----------------------------------------------------------
				' EMPLEADORES OFICINA: Sedes de empresas correspondientes a la oficina,  como utiliza el campo
				' id_cimpiego en el join excluye las oficinas
				Set rsAziendeE = Server.CreateObject("ADODB.RECORDSET")
				sqlAziendeE = "SELECT count(sri.rol) FROM sede_impresa si WITH(nolock) " &_
							 "INNER JOIN (SELECT DISTINCT id_impresa, rol FROM impresa_rolesservicios WITH(nolock)) sri " &_
							 "ON sri.id_impresa = si.id_impresa " &_
							 "WHERE sri.rol = 1 AND  si.id_cimpiego = " & session("creator") 
				'response.write sqlAziendeE
				rsAziendeE.open sqlAziendeE, CC, 3, 3
				StampaAziendeE = err.number
				err.number = 0 
				'-----------------------------------------------------------
				' PRESTADOREs OFICINA: Sedes de empresas correspondientes a la oficina,  como utiliza el campo
				' id_cimpiego en el join excluye las oficinas
				'Set rsAziendeP = Server.CreateObject("ADODB.RECORDSET")
				'sqlAziendeP = "SELECT count(sri.rol) FROM sede_impresa si WITH(nolock)" &_
				'			 "INNER  JOIN  (SELECT DISTINCT id_impresa, rol FROM impresa_rolesservicios WITH(nolock)) sri " &_
				'			 "ON sri.id_impresa = si.id_impresa " &_
				'			 "WHERE sri.rol = 2 AND  si.id_cimpiego = " & session("creator") 
				'rsAziendeP.open sqlAziendeP, CC, 3, 3
				'StampaAziendeP = err.number
				'err.number = 0 
				'-----------------------------------------------------------
				' AVISOS OFICINA: Avisos Vigentes correspoindientes a la oficina por el campo Cod_oficina
				Set rsAnnunci = Server.CreateObject("ADODB.RECORDSET")
				sqlAnnunci = "SELECT rs.cod_tipo_richiesta, irs.rol, COUNT(*) " &_
							 "FROM richiesta_sede rs WITH(nolock) INNER JOIN impresa_rolesserviciostipos irs  WITH(nolock) ON rs.servicio = irs.servicio " &_
							 " WHERE rs.COD_OFICINA = " & session("creator")&_ 
							 " AND rs.DT_finpublicacion > = " & convdatetodb(date)&_
							 " AND rs.FL_PUBBLICATO = 'S'" &_
							 " GROUP BY  rs.cod_tipo_richiesta, irs.rol "
				'Response.Write sqlAnnunci
				rsAnnunci.open sqlannunci, CC, 3, 3
				StampaAnnunci = err.number
				SelecLab = 0
				SelecCap = 0
				AvisosLab = 0
				AvisosCap = 0
				do while rsAnnunci.eof = false
					if rsAnnunci.fields(0)= 0 then			' Si es seleccion
						if rsAnnunci.fields(1)= 1 then			'si es Laboral
							SelecLab = rsAnnunci.fields(2) 
						else									' si es de capacitación
							SelecCap = rsAnnunci.fields(2) 
						end if
					else									' Si es Aviso
						if rsAnnunci.fields(1)= 1 then			' si es laboral	
							AvisosLab = rsAnnunci.fields(2)
						else									' si es de capacitación
							AvisosCap = rsAnnunci.fields(2)
						end if
					end if
					rsAnnunci.movenext
				loop
				err.number = 0
				
				
				'-----------------------------------------------------------
				' Convenios de Adhesión Firmados del Seguro de Capacitacion y Empleo
				'Set rsConvFirm = Server.CreateObject("ADODB.RECORDSET")
				'sqlConvFirm = " SELECT count(*) FROM seguroconvenios WITH(nolock)" &_
				'			 " WHERE estado = 6 AND oficina = " & session("creator") '&_ 
				
			'	rsConvFirm.open sqlConvFirm, connLavoro, 3, 3
			'	StampaConvFirm = err.number
			'	err.number = 0
				
				'-----------------------------------------------------------
				'Set rsBenefSeg = Server.CreateObject("ADODB.RECORDSET")
				'sqlBenefSeg = " SELECT count(*)  FROM cuilestado ce WITH(nolock)" &_
				'			 " INNER JOIN cuil c WITH(nolock) ON ce.cuil = c.cuil" &_ 
				'			 " INNER JOIN beneficiario b WITH(nolock) ON	b.cuilid = c.cuilid" &_
				'			 " WHERE ce.liquiplan = 6 AND ce.estadobeneficiarioid = 6 AND b.oficinaid = " & session("creator")
				'rsBenefSeg.open sqlBenefSeg, ConnEmpleoPersonas, 3, 3
				'StampaBenefSeg = err.number
			'	err.number = 0
				
				
			else ' consulta todas las oficinas porque el usuario ve todo el pais

						
				'HISTORIAS LABORALES PAIS : Personas con Nivel Educativo o Postulaciones 
				Set rsIscritti = Server.CreateObject("ADODB.RECORDSET")
				sqlIscritti = "SELECT count(distinct p.id_persona) FROM persona p with (nolock), stato_occupazionale so with (nolock) "
				sqlIscritti = sqlIscritti & "WHERE p.id_persona=so.id_persona "
				sqlIscritti = sqlIscritti & "AND  so.id_cimpiego is not null "
				sqlIscritti = sqlIscritti & "AND (ind_status = '0' OR (ind_status = '2' "
				sqlIscritti = sqlIscritti & "AND ind_status not in ( SELECT ind_status "
				sqlIscritti = sqlIscritti & "FROM STATO_OCCUPAZIONALE with (nolock) WHERE p.id_persona = id_persona "
				sqlIscritti = sqlIscritti & "AND ind_status = '0' ))) "

				'response.write sqlIscritti
				rsIscritti.open sqlIscritti, CC, 3, 3
				StampaIscritti =  err.number 
				err.number = 0
				'-----------------------------------------------------------
				' EMPLEADORES PAIS: Sedes de empresas correspondientes a todo el Pais
				' utiliza el campo id_cimpiego = null para excluir las oficinas
				Set rsAziendeE = Server.CreateObject("ADODB.RECORDSET")
				sqlAziendeE = "SELECT count(sri.rol) FROM sede_impresa si WITH(nolock) " &_
							 "INNER  JOIN  (SELECT DISTINCT id_impresa, rol FROM impresa_rolesservicios WITH(nolock)) sri " &_
							 "ON sri.id_impresa = si.id_impresa " &_
							 "WHERE sri.rol = 1 and id_cimpiego is not null"
				rsAziendeE.open sqlAziendeE, CC, 3, 3
				StampaAziendeE = err.number
				err.number = 0 
				'-----------------------------------------------------------
				' PRESTADORES PAIS: Sedes de empresas correspondientes a todo el Pais
				' utiliza el campo id_cimpiego = null para excluir las oficinas
				'Set rsAziendeP = Server.CreateObject("ADODB.RECORDSET")
				'sqlAziendeP = "SELECT count(sri.rol) FROM sede_impresa si WITH(nolock) " &_
				'			 "INNER  JOIN  (SELECT DISTINCT id_impresa, rol FROM impresa_rolesservicios WITH(nolock)) sri " &_
				'			 "ON sri.id_impresa = si.id_impresa " &_
				'			 "WHERE sri.rol = 2 and id_cimpiego is not null" 
				'rsAziendeP.open sqlAziendeP, CC, 3, 3
				'StampaAziendeP = err.number
				'err.number = 0 
				'-----------------------------------------------------------
				' AVISOS TOTALES: Avisos Vigentes correspoindientes a la oficina por el campo Cod_oficina
				Set rsAnnunci = Server.CreateObject("ADODB.RECORDSET")
				sqlAnnunci = "SELECT rs.cod_tipo_richiesta, irs.rol, COUNT(*) " &_
							 "FROM richiesta_sede rs WITH(nolock) INNER JOIN impresa_rolesserviciostipos irs  WITH(nolock) ON rs.servicio = irs.servicio " &_
							 " WHERE rs.DT_finpublicacion > " & convdatetodb(date)&_
							 " AND rs.FL_PUBBLICATO = 'S'" &_
							 " GROUP BY  rs.cod_tipo_richiesta, irs.rol "
				rsAnnunci.open sqlannunci, CC, 3, 3
				StampaAnnunci = err.number
				SelecLab = 0
				SelecCap = 0
				AvisosLab = 0
				AvisosCap = 0
				do while rsAnnunci.eof = false
					if rsAnnunci.fields(0)= 0 then			' Si es seleccion
						if rsAnnunci.fields(1)= 1 then			'si es Laboral
							SelecLab = rsAnnunci.fields(2) 
						else									' si es de capacitación
							SelecCap = rsAnnunci.fields(2) 
						end if
					else									' Si es Aviso
						if rsAnnunci.fields(1)= 1 then			' si es laboral	
							AvisosLab = rsAnnunci.fields(2)
						else									' si es de capacitación
							AvisosCap = rsAnnunci.fields(2)
						end if
					end if
					rsAnnunci.movenext
				loop
				err.number = 0
				
				
				'-----------------------------------------------------------
				' Convenios de Adhesión Firmados del Seguro de Capacitacion y Empleo
				'Set rsConvFirm = Server.CreateObject("ADODB.RECORDSET")
				'sqlConvFirm = " SELECT count(*) FROM seguroconvenios WITH(nolock)" &_
			''				 " WHERE estado = 6" 
				
			'	rsConvFirm.open sqlConvFirm, connLavoro, 3, 3
			'	StampaConvFirm = err.number
			'	err.number = 0
				
				'-----------------------------------------------------------
				'Set rsBenefSeg = Server.CreateObject("ADODB.RECORDSET")
				'sqlBenefSeg = " SELECT count(*)  FROM cuilestado ce WITH(nolock)" &_
				'			 " INNER JOIN cuil c WITH(nolock) ON ce.cuil = c.cuil" &_ 
				'			 " INNER JOIN beneficiario b WITH(nolock) ON	b.cuilid = c.cuilid" &_
				'			 " WHERE ce.liquiplan = 6 AND ce.estadobeneficiarioid = 6"
				'rsBenefSeg.open sqlBenefSeg, ConnEmpleoPersonas, 3, 3
				'StampaBenefSeg = err.number
				'err.number = 0

			end if
			'======================================================================
			%>
			<tr>
				<td align="center" class="tbltext">
					A continuaci&oacute;n se visualiza un resumen de la actividad desarrollada
					<%
					if session ("creator") <> 0 then
						%>
						por su Centro de Empleo.
						<%
					end if
					%>
								
				</td>
			</tr>
			<tr>
				<td align="center" class="tbltext">
				   <br><br>
				</td>
			</tr>
			<%
			'----------------------------------------------------------------------
			if clong(StampaIscritti) = 0 then
				%>
				<tr>
					<td align="left" class="tbltext">
						Total de Historias Laborales
					</td>
					<td align=right class="tbltext">
						<%=rsIscritti(0)%>
					</td>			
				</tr>
				<!--tr>
					<td align="center" class="tbltext">
					   <br>
					</td>
				</tr-->
				<%
			end if
			'----------------------------------------------------------------------
			if clong(StampaAziendeE) = 0 then
				%>
				<tr>
					<td align="left" class="tbltext">
						Total de Empleadores
					</td>
					<td align="right" class="tbltext">
						<%=rsAziendeE(0)%>
					</td>			
				</tr>
				<tr>
					<td align="center" class="tbltext">
					   <br>
					</td>
				</tr>
				<%
			end if
			'----------------------------------------------------------------------
	'11/10/2007 inizio
	'		if clong(StampaAziendeP) = 0 then
				%>
				<!--tr>
					<td align="left" class="tbltext">
						Total de Prestadores
					</td>
					<td align="left" class="tbltext"-->
						<%'=rsAziendeP(0)%>
					<!--/td>			
				</tr>
				<tr>
					<td align="center" class="tbltext">
					   <br>
					</td>
				</tr-->
				<%
	'		end if
	'11/10/2007 fine
			'----------------------------------------------------------------------
			if clong(StampaAnnunci) = 0 then
				%>
				<tr>
					<td align="left" class="tbltext">
						Total de Ofertas Laborales
					</td>
					<td align="right" class="tbltext">
						<%=AvisosLab%>
					</td>			
				</tr>
				<!--<tr>
					<td align="left" class="tbltext">
						Total de Vacantes de Capacitación
					</td>
					<td align="right" class="tbltext">
						<%=AvisosCap%>
					</td>			
				</tr-->
				<!--tr>
					<td align="left" class="tbltext">
						Total de Selecciones Laborales
					</td> 
					<td align="right" class="tbltext">	
						<%=SelecLab%>
					</td>			
				</tr-->
				<!--tr>
					<td align="left" class="tbltext">
						Total de Selecciones de Capacitación
					</td> 
					<td align="right" class="tbltext">	
						<%=SelecCap%>
					</td>			
				</tr-->
				<tr>
					<td align="center" class="tbltext">
					   <br>
					</td>
				</tr>
				<%
			end if
			'----------------------------------------------------------------------
		'11/10/2007 inizio
		'	if clong(StampaComunic) = 0 then
				%>
				<!--tr>
					<td align="left" class="tbltext">
						Total de Convenios de Adhesión Firmados del SCyE
					</td>
					<td align="left" class="tbltext">
						<%'=rsConvFirm(0)%>
					</td>			
				</tr-->
				<%
		'	end if
			'----------------------------------------------------------------------
		'	if clong(StampaBenefSeg) = 0 then
				%>
				<!--tr>
					<td align="left" class="tbltext">
						Total de Beneficiarios del SCyE
					</td>
					<td align="left" class="tbltext">
						<%'=rsBenefSeg(0)%>
					</td>			
				</tr-->
				<%
		'	end if
	' 11/10/2007 fine

			set rsIscritti = nothing
			set rsAziendeE = nothing
			set rsAziendeP = nothing
			set rsAnnunci = nothing
			set rsConvFirm = nothing
			set rsBenefSeg = nothing

		end if	
		%>
		<!--#include Virtual="/include/closeconn.asp"-->
		<%
	end if

	session ("LastAccess")=""
	%>
</TABLE>
