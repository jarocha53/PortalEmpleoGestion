<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%if session("progetto")<>"" then%>
	<!--#include virtual = "/include/openconn.asp" -->
	<!--#include Virtual = "/include/DecCod.asp"-->
	<!--#include Virtual = "/include/ControlDateVB.asp"-->
	<!--#include virtual ="/include/DecComun.asp"-->

	<html>
	<head>
	<title>Sedes</title>
	<link REL="STYLESHEET" TYPE="text/css" HREF="<%=session("progetto")%>/fogliostile.css">

	<script language="javascript">
		<!-- #include virtual="/include/help.inc" -->
	
		function InviaSelezione(ValIdSede,ValDescSede,NomeForm,NomeTxtIdSede,NomeTxtDescSede){
			NomeCampoIdSedeHid = eval("opener.document." + NomeForm + "."+ NomeTxtIdSede)
			NomeCampoIdSedeHid.value = ValIdSede
			NomeCampoIdSede = eval("opener.document." + NomeForm + "." + NomeTxtDescSede);
			NomeCampoIdSede.value = ValDescSede;
			self.close()
		}

		function Destro(e) {
			if (navigator.appName == 'Netscape' && 
				(e.which == 3 || e.which == 2))
				return false;
			else if (navigator.appName == 'Microsoft Internet Explorer' && 
					(event.button == 2 || event.button == 3))
				 {
					alert("El bot�n derecho del mouse esta deshabilitado");
					return false;
				  }
			return true;
		}

		document.onmousedown=Destro;
		if (document.layers) window.captureEvents(Event.MOUSEDOWN);
		window.onmousedown=Destro;

	</script>
	</head>
	<%
	NomeForm = Request.QueryString("NomeForm")
	ValCfAzienda = Request.QueryString("TxtCfAzienda")
	ValPiAzienda = Request.QueryString("TxtPiAzienda")
	NomeCampoIdSede = Request.QueryString("NomeTxtIdSede")
	NomeCampoDescSede = Request.QueryString("NomeTxtDescSede")
	RS = Request.QueryString("RS")
	%>

	<body class="sfondocentro">
	<form name="frmSelSede" method="get" action"SelSedeImpresa.asp">
		<input type="hidden" name="txtCfAzienda">
		<input type="hidden" name="txtPiAzienda">
		<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
			<tr height="17">
				<td class="sfondomenu" width="67%" height="18"><span class="tbltext0">
					<b>&nbsp;SELECCIONAR SEDE</b>
				</td>
				<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
				<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
			</tr>
			<tr>
				<td class="sfondocommaz" width="57%" colspan="3">
					Seleccionar la sede de la lista. <br>
					Presionar <b>Cerrar</b> para no seleccionar nada.
					<a title="Help" href="Javascript:Show_Help('/Pgm/help/Include/SelSedeImpresa')">
					<img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0" onmouseover="javascript:window.status='' ; return true"></a> 
				</td>
			</tr>
			<tr height="2">
				<td class="sfondocommaz" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
			</tr>
		</table>
		<br>
		<table border="0" CELLPADDING="2" CELLSPACING="2" width="500">
			<%
			if ValCfAzienda <> "" or ValPiAzienda <> "" OR RS <> "" then
				sSQL = "SELECT ID_IMPRESA, RAG_SOC FROM IMPRESA "
				if ValPiAzienda  <> "" then
					sSQL = sSQL + " WHERE NUM_INPS = '" & ValPiAzienda & "'"
				elseif ValCfAzienda <> "" then 
					sSQL = sSQL + " WHERE COD_FISC = '" & ValCfAzienda & "'"
				elseif RS <> "" then 
					sSQL = sSQL + " WHERE RAG_SOC = '" & RS & "'"		
				end if 
				
				if Session("tipopers")="S" and clng(Session("Creator"))<> 0 then
					sSQL = sSQL + " AND COD_TIMPR <> '03'"	
				end if

'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
				Set rsAzienda = CC.Execute(sSQL)
				if not rsAzienda.EOF then%>
					<tr>
						<td class="tbltext1" align="center" colspan="4">
							<b>Empleador:&nbsp;<%=trim(rsAzienda("RAG_SOC"))%></b>
							<br><br>
						</td>
					</tr>
					<%
					sSQL = "SELECT SI.ID_SEDE, SI.DESCRIZIONE, SI.INDIRIZZO, SI.COMUNE, SI.PRV FROM SEDE_IMPRESA SI, IMPRESA I"	
					sSQL = sSQL + " WHERE SI.ID_IMPRESA = I.ID_IMPRESA"		
					sSQL = sSQL + " AND I.ID_IMPRESA = " & rsAzienda("ID_IMPRESA")
					sSQL = sSQL + " AND SI.ID_CIMPIEGO = " & Session("Creator")
					
					'Response.Write ssql
					
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
					Set rsSede = CC.Execute(sSQL)		
					
					'Response.Write ssql
					if not rsSede.EOF then%>
						<tr>
							<td align="middle" valign="center" width="150" class="tbltext1"><b>Sede</b></td>
							<td align="middle" valign="center" width="150" class="tbltext1"><b>Direcci�n</b></td>
							<td align="middle" valign="center" width="120" class="tbltext1"><b>Municipio</b></td>
							<td align="middle" valign="center" width="40" class="tbltext1"><b>Departamento</b></td>
						</tr>
						<%Do While Not rsSede.EOF%>
							<tr class="tblsfondo">
								<td nowrap width="150" class="tblAgg">
									<a class="tblAgg" title="Seleziona Sede" href="Javascript:InviaSelezione('<%=rsSede("ID_SEDE")%>','<%=rsSede("DESCRIZIONE")%>','<%=NomeForm%>','<%=NomeCampoIdSede%>','<%=NomeCampoDescSede%>')">
										<b><%=ucase(rsSede("DESCRIZIONE"))%></b>
									</a>
								</td>
								<td nowrap width="150" class="tbldett">
									<%=ucase(rsSede("INDIRIZZO"))%>
								</td>
								<td nowrap width="120" class="tbldett">
									<%=DescrComune(rsSede("COMUNE"))%>
								</td>
								<td nowrap width="40" class="tbldett">
									<%=rsSede("PRV")%>
								</td>
								<%rsSede.MoveNext%>
							</tr>
						<%loop
						rsSede.Close
						set rsSede = nothing
						rsAzienda.Close
						set rsAzienda = nothing%>
						<tr>
							<td colspan="4" align="center">
								&nbsp;
							</td>
						</tr>
						<tr>
							<td colspan="4" align="center">
								<a class="textred" title="Rimuovi la sede selezionata" href="Javascript:InviaSelezione('','','<%=NomeForm%>','<%=NomeCampoIdSede%>','<%=NomeCampoDescSede%>')"><b>Remover Selecci�n</b></a>
							</td>
						</tr>
					<%else%>
						<tr>
							<td class="tbltext3" colspan="4" align="center">
								<b>No hay sedes para el empleador seleccionado</b>
							</td>
						</tr>
					<%end if
				else%>
					<tr>
						<td class="tbltext3" colspan="4" align="center">
							<b>Empleador no censado o fuera de competencia <br>en el Centro de Empleo</b>
						</td>
					</tr>
				<%end if
			end if%>	
		</table>
	</form>

	<table width="500" cellspacing="2" cellpadding="1" border="0">
		<tr align="center">
			<td>
				<a href="javascript:self.close()"><img src="<%=Session("progetto")%>/images/chiudi.gif" title="Chiudi la pagina" border="0" align="absBottom"></a>
			</td>
		</tr>		
	</table>
	</body>
	</html>
	<!-- #include virtual="/include/closeconn.asp" -->
<%else%>
	<script>
		alert("La sesi�n ha caducado")
		self.close()
	</script>
<%end if%>
