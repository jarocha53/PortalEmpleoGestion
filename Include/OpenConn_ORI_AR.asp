
<%

'====================================================================================
'	Esta es la Unica pagina que tiene las conexiones a las BASES 
'====================================================================================

server.ScriptTimeout = 1000

'Creacion de Objeto ADODB.
If  (Session("Progetto") & "*") = "*" Then
	Response.Redirect "/util/error_login.asp"
Else 
	

	Set CC = Server.CreateObject("ADODB.Connection") 'Objeto de conexion a EmpleoLaboral de 2005
	Set CX = Server.CreateObject("ADODB.Connection") 'Objeto de conexion a EmpleoLaboral de 2005 
	Set gConnSeguridad = Server.CreateObject("ADODB.Connection") 'Objeto de conexion a la Base Seguridad
	Set gConnSeguridadReplica = Server.CreateObject("ADODB.Connection") 'Objeto de conexion a la Base Seguridad
	Set ConnEmpleoPersonas = Server.CreateObject("ADODB.Connection")	' Objeto de Conexion a la Base EmpleoPersonas
	Set ConnEmpleoPersonasReplica = Server.CreateObject("ADODB.Connection")	' Objeto de Conexion a la Base EmpleoPersonas
	Set connLavoro = Server.CreateObject("adodb.connection") ' Objeto de Conexion a la Base EmpleoLavoro
	Set connProyectos = Server.CreateObject("adodb.connection") ' Objeto de Conexion a la Base EmpleoProyectos

	Progetto = "ba"
	pwd = "dp_" & Progetto & "_il" 

	Hosting = ucase(Request.ServerVariables("SERVER_NAME")) 	
	select case Hosting
	    '======================================================================
	    ' Ambiente de Desarrollo
	    '======================================================================
		case "PORTALDESARROLLOGESTION"
	
			'conexion a la base Seguridad del SQL (online)
            Application(Hosting & "_PortalConnectionString") = "Provider=MSDASQL;Persist Security Info=True;Extended Properties=""DSN=Seguridad;DATABASE=Seguridad"";Initial Catalog=Seguridad;"
            Application(Hosting & "_PortalConnectionTimeout") = 60
		    Application(Hosting & "_PortalCommandTimeout") = 180
		    Application(Hosting & "_PortalCursorLocation") = 3
		    Application(Hosting & "_PortalRuntimeUserName") = "ExtranetPortal"
		    Application(Hosting & "_PortalRuntimePassword") = "98760"
		    
		    'conexion a la base Seguridad del SQL (replica)
            Application(Hosting & "_PortalReplicaConnectionString") = "Provider=MSDASQL;Persist Security Info=True;Extended Properties=""DSN=Seguridad;DATABASE=Seguridad"";Initial Catalog=Seguridad;"
            Application(Hosting & "_PortalReplicaConnectionTimeout") = 60
		    Application(Hosting & "_PortalReplicaCommandTimeout") = 180
		    Application(Hosting & "_PortalReplicaCursorLocation") = 3
		    Application(Hosting & "_PortalReplicaRuntimeUserName") = "ExtranetPortal"
		    Application(Hosting & "_PortalReplicaRuntimePassword") = "98760"
		    
		    'conexion a la base EmpleoPersonas del SQL on line
		    Application(Hosting & "_JefesConnectionString") = "Provider=MSDASQL;Persist Security Info=True;Extended Properties=""DSN=EmpleoPersonas;DATABASE=EmpleoPersonas"";Initial Catalog=EmpleoPersonas;"
			Application(Hosting & "_JefesConnectionTimeout") = 60
			Application(Hosting & "_JefesCommandTimeout") = 180
			Application(Hosting & "_JefesCursorLocation") = 3 'adUseServer
			Application(Hosting & "_JefesRuntimeUserName") = "IntranetPortal"
			Application(Hosting & "_JefesRuntimePassword") = "98760"
			Application(Hosting & "_JefesDTSImportacion") = "\\Dory\dtcimp08538$"

		    'conexion a la base EmpleoPersonas del SQL replica
		    Application(Hosting & "_JefesReplicaConnectionString") = "Provider=MSDASQL;Persist Security Info=True;Extended Properties=""DSN=EmpleoPersonas;DATABASE=EmpleoPersonas"";Initial Catalog=EmpleoPersonas;"
			Application(Hosting & "_JefesReplicaConnectionTimeout") = 60
			Application(Hosting & "_JefesReplicaCommandTimeout") = 180
			Application(Hosting & "_JefesReplicaCursorLocation") = 3 'adUseServer
			Application(Hosting & "_JefesReplicaRuntimeUserName") = "IntranetPortal"
			Application(Hosting & "_JefesReplicaRuntimePassword") = "98760"
			Application(Hosting & "_JefesReplicaDTSImportacion") = "\\Dory\dtcimp08538$"

			'conexion a la base EmpleoLavoro del SQL
		    Application(Hosting & "_AgendaConnectionString") = "Provider=MSDASQL.1;Persist Security Info=True;Extended Properties=""DSN=EmpleoLavoro;Description=EmpleoLavoro;DATABASE=EmpleoLavoro"";Initial Catalog=EmpleoLavoro;"
			Application(Hosting & "_AgendaConnectionTimeout") = 60
			Application(Hosting & "_AgendaCommandTimeout") = 180
			Application(Hosting & "_AgendaCursorLocation") = 3
			Application(Hosting & "_AgendaRuntimeUserName") = "EmpleoLavoro"
			Application(Hosting & "_AgendaRuntimePassword") = "1234*"
						
			'conexion a la base EmpleoProyectos del SQL
			Application(Hosting & "_ProyConnectionString") = "Provider=MSDASQL;Persist Security Info=True;Extended Properties=""DSN=EmpleoProyectos;DATABASE=EmpleoProyectos"";Initial Catalog=EmpleoProyectos;"
			Application(Hosting & "_ProyConnectionTimeout") = 60
			Application(Hosting & "_ProyCommandTimeout") = 180
			Application(Hosting & "_ProyCursorLocation") = 3
			Application(Hosting & "_ProyRuntimeUserName") = "EmpleoLavoro"
			Application(Hosting & "_ProyRuntimePassword") = "1234*"
			
			'conexion a la base EmpleoLaboral del SQL2005
			Application(Hosting & "_PortalConnectionStringLaboral") = "Provider=MSDASQL;Persist Security Info=True;Extended Properties=""DSN=DSNEmpleoLaboral;DATABASE=EmpleoLaboral"";Initial Catalog=EmpleoLaboral;"
			Application(Hosting & "_PortalConnectionTimeoutLaboral") = 60
			Application(Hosting & "_PortalCommandTimeoutLaboral") = 180
			Application(Hosting & "_PortalCursorLocationLaboral") = 3
			Application(Hosting & "_PortalRuntimeUserNameLaboral") = "ba"
			Application(Hosting & "_PortalRuntimePasswordLaboral") = "dp_ba_il"
            
			'Nombre del sitio Web para el link de salto
			Application(Hosting & "_PortalSitioProyectos") = "http://DesarrolloEmpleo/empleo"

		    'Nombre del sitio Web para el link de salto
			Application(Hosting & "_PortalSitioPersonas") = "DesarrolloEmpleo/Jefes"
					    		    
			' Conexion a la base del Datawarehouse en SQL
			Application(Hosting & "_DataWareHouseConnectionString") = "Provider=MSDASQL;Persist Security Info=True;Extended Properties=""DSN=EmpleoDW;DATABASE=Empleo_DW"";Initial Catalog=Empleo_DW;"
			Application(Hosting & "_DataWareHouseConnectionTimeout") = 60
			Application(Hosting & "_DataWareHouseCommandTimeout") = 180
			Application(Hosting & "_DataWareHouseCursorLocation") = 3
			Application(Hosting & "_DataWareHouseRuntimeUserName") = "OlapReader"
			Application(Hosting & "_DataWareHouseRuntimePassword") = "consulta"

		    ' Reporting Service - LLamada al servicio (link de salto)
			Application(Hosting & "_ReportingService") = "http://flawless/ReportServer/Pages/ReportViewer.aspx?"
			

			
		'======================================================================
		' Ambiente de Prueba
		'======================================================================
		case "PORTALPRUEBA"
			'Server Name de Prueba
			
			'conexion a la base Seguridad del SQL (online)
            Application(Hosting & "_PortalConnectionString") = "Provider=MSDASQL;Password=98760;Persist Security Info=True;Extended Properties=""DSN=Seguridad;DATABASE=Seguridad"";Initial Catalog=Seguridad;"
            Application(Hosting & "_PortalConnectionTimeout") = 60
		    Application(Hosting & "_PortalCommandTimeout") = 180
		    Application(Hosting & "_PortalCursorLocation") = 3
		    Application(Hosting & "_PortalRuntimeUserName") = "ExtranetPortal"
		    Application(Hosting & "_PortalRuntimePassword") = "98760"
		    
		    'conexion a la base Seguridad del SQL (replica)
            Application(Hosting & "_PortalReplicaConnectionString") = "Provider=MSDASQL;Password=98760;Persist Security Info=True;Extended Properties=""DSN=Seguridad;DATABASE=Seguridad"";Initial Catalog=Seguridad;"
            Application(Hosting & "_PortalReplicaConnectionTimeout") = 60
		    Application(Hosting & "_PortalReplicaCommandTimeout") = 180
		    Application(Hosting & "_PortalReplicaCursorLocation") = 3
		    Application(Hosting & "_PortalReplicaRuntimeUserName") = "ExtranetPortal"
		    Application(Hosting & "_PortalReplicaRuntimePassword") = "98760"
		    
		    'conexion a la base EmpleoPersonas del SQL on line
		    Application(Hosting & "_JefesConnectionString") = "Provider=MSDASQL;Persist Security Info=True;Extended Properties=""DSN=EmpleoPersonas;DATABASE=EmpleoPersonas"";Initial Catalog=EmpleoPersonas;"
			Application(Hosting & "_JefesConnectionTimeout") = 60
			Application(Hosting & "_JefesCommandTimeout") = 180
			Application(Hosting & "_JefesCursorLocation") = 3 'adUseServer
			Application(Hosting & "_JefesRuntimeUserName") = "IntranetPortal"
			Application(Hosting & "_JefesRuntimePassword") = "98760"
			Application(Hosting & "_JefesDTSImportacion") = "\\Dory\dtcimp08538$"

		    'conexion a la base EmpleoPersonas del SQL replica
		    Application(Hosting & "_JefesReplicaConnectionString") = "Provider=MSDASQL;Persist Security Info=True;Extended Properties=""DSN=EmpleoPersonas;DATABASE=EmpleoPersonas"";Initial Catalog=EmpleoPersonas;"
			Application(Hosting & "_JefesReplicaConnectionTimeout") = 60
			Application(Hosting & "_JefesReplicaCommandTimeout") = 180
			Application(Hosting & "_JefesReplicaCursorLocation") = 3 'adUseServer
			Application(Hosting & "_JefesReplicaRuntimeUserName") = "IntranetPortal"
			Application(Hosting & "_JefesReplicaRuntimePassword") = "98760"
			Application(Hosting & "_JefesReplicaDTSImportacion") = "\\Dory\dtcimp08538$"
			
			'conexion a la base EmpleoLavoro del SQL
		    Application(Hosting & "_AgendaConnectionString") = "Provider=MSDASQL.1;Persist Security Info=True;Extended Properties=""DSN=EmpleoLavoro;Description=EmpleoLavoro;DATABASE=EmpleoLavoro"";Initial Catalog=EmpleoLavoro;"
			Application(Hosting & "_AgendaConnectionTimeout") = 60
			Application(Hosting & "_AgendaCommandTimeout") = 180
			Application(Hosting & "_AgendaCursorLocation") = 3
			Application(Hosting & "_AgendaRuntimeUserName") = "EmpleoLavoro"
			Application(Hosting & "_AgendaRuntimePassword") = "1234*"
						
			'conexion a la base EmpleoProyectos del SQL
			Application(Hosting & "_ProyConnectionString") = "Provider=MSDASQL;Persist Security Info=True;Extended Properties=""DSN=EmpleoProyectos;DATABASE=EmpleoProyectos"";Initial Catalog=EmpleoProyectos;"
			Application(Hosting & "_ProyConnectionTimeout") = 60
			Application(Hosting & "_ProyCommandTimeout") = 180
			Application(Hosting & "_ProyCursorLocation") = 3
			Application(Hosting & "_ProyRuntimeUserName") = "EmpleoLavoro"
			Application(Hosting & "_ProyRuntimePassword") = "1234*"
			
			'conexion a la base EmpleoLaboral del SQL2005
			Application(Hosting & "_PortalConnectionStringLaboral") = "Provider=MSDASQL;Persist Security Info=True;Extended Properties=""DSN=DSNEmpleoLaboral;DATABASE=EmpleoLaboral"";Initial Catalog=EmpleoLaboral;"
			Application(Hosting & "_PortalConnectionTimeoutLaboral") = 60
			Application(Hosting & "_PortalCommandTimeoutLaboral") = 180
			Application(Hosting & "_PortalCursorLocationLaboral") = 3
			Application(Hosting & "_PortalRuntimeUserNameLaboral") = "ba"
			Application(Hosting & "_PortalRuntimePasswordLaboral") = "dp_ba_il"
		    
		    'Nombre del sitio Web para el link de salto
			Application(Hosting & "_PortalSitioProyectos") = "http://PruebaEmpleo/empleo"
			
			'Nombre del sitio Web para el link de salto
			Application(Hosting & "_PortalSitioPersonas") = "PruebaEmpleo/Jefes"

			' Conexion a la base del Datawarehouse en SQL
			Application(Hosting & "_DataWareHouseConnectionString") = "Provider=MSDASQL;Persist Security Info=True;Extended Properties=""DSN=EmpleoDW;DATABASE=Empleo_DW"";Initial Catalog=Empleo_DW;"
			Application(Hosting & "_DataWareHouseConnectionTimeout") = 60
			Application(Hosting & "_DataWareHouseCommandTimeout") = 180
			Application(Hosting & "_DataWareHouseCursorLocation") = 3
			Application(Hosting & "_DataWareHouseRuntimeUserName") = "OlapReader"
			Application(Hosting & "_DataWareHouseRuntimePassword") = "consulta"

		    ' Reporting Service - LLamada al servicio (link de salto)
			Application(Hosting & "_ReportingService") = "http://flawless/ReportServer/Pages/ReportViewer.aspx?"
			
			
		'======================================================================
		' Ambiente de Capacitacion
		'======================================================================
		case "PORTALUSUARIOS", "PORTALUSUARIOS.EMPLE.GOV.AR"
			
			'conexion a la base Seguridad del SQL
            Application(Hosting & "_PortalConnectionString") = "Provider=MSDASQL;Persist Security Info=True;Extended Properties=""DSN=Seguridad_capacita;DATABASE=Seguridad"";Initial Catalog=Seguridad;"
            Application(Hosting & "_PortalConnectionTimeout") = 60
		    Application(Hosting & "_PortalCommandTimeout") = 180
		    Application(Hosting & "_PortalCursorLocation") = 3
		    Application(Hosting & "_PortalRuntimeUserName") = "ExtranetPortal"
		    Application(Hosting & "_PortalRuntimePassword") = "98760"
		
			'conexion a la base Seguridad Replica del SQL
            Application(Hosting & "_PortalReplicaConnectionString") = "Provider=MSDASQL;Persist Security Info=True;Extended Properties=""DSN=Seguridad_capacita;DATABASE=Seguridad"";Initial Catalog=Seguridad;"
            Application(Hosting & "_PortalReplicaConnectionTimeout") = 60
		    Application(Hosting & "_PortalReplicaCommandTimeout") = 180
		    Application(Hosting & "_PortalReplicaCursorLocation") = 3
		    Application(Hosting & "_PortalReplicaRuntimeUserName") = "ExtranetPortal"
		    Application(Hosting & "_PortalReplicaRuntimePassword") = "98760"
		        
		    'conexion a la base EmpleoPersonas del SQL
		    Application(Hosting & "_JefesConnectionString") = "Provider=MSDASQL;Persist Security Info=True;Extended Properties=""DSN=EmpleoPersonas_capacita;DATABASE=EmpleoPersonas"";Initial Catalog=EmpleoPersonas;"
			Application(Hosting & "_JefesConnectionTimeout") = 60
			Application(Hosting & "_JefesCommandTimeout") = 180
			Application(Hosting & "_JefesCursorLocation") = 3 'adUseServer
			Application(Hosting & "_JefesRuntimeUserName") = "IntranetPortal"
			Application(Hosting & "_JefesRuntimePassword") = "98760"
			
		    'conexion a la base EmpleoPersonas Replica del SQL
		    Application(Hosting & "_JefesReplicaConnectionString") = "Provider=MSDASQL;Persist Security Info=True;Extended Properties=""DSN=EmpleoPersonas_capacita;DATABASE=EmpleoPersonas"";Initial Catalog=EmpleoPersonas;"
			Application(Hosting & "_JefesReplicaConnectionTimeout") = 60
			Application(Hosting & "_JefesReplicaCommandTimeout") = 180
			Application(Hosting & "_JefesReplicaCursorLocation") = 3 'adUseServer
			Application(Hosting & "_JefesReplicaRuntimeUserName") = "IntranetPortal"
			Application(Hosting & "_JefesReplicaRuntimePassword") = "98760"
			'Application(Hosting & "_DTSImportacion") = "\\Dummy\dtcimp08538$"		
			
			'conexion a la base EmpleoLavoro del SQL
		    Application(Hosting & "_AgendaConnectionString") = "Provider=MSDASQL.1;Persist Security Info=True;Extended Properties=""DSN=EmpleoLavoro_Capacita;Description=EmpleoLavoro;DATABASE=EmpleoLavoro"";Initial Catalog=EmpleoLavoro;"
			Application(Hosting & "_AgendaConnectionTimeout") = 60
			Application(Hosting & "_AgendaCommandTimeout") = 180
			Application(Hosting & "_AgendaCursorLocation") = 3
			Application(Hosting & "_AgendaRuntimeUserName") = "EmpleoLavoro"
			Application(Hosting & "_AgendaRuntimePassword") = "1234*"
			
			'conexion a la base EmpleoProyectos del SQL
			Application(Hosting & "_ProyConnectionString") = "Provider=MSDASQL;Persist Security Info=True;Extended Properties=""DSN=EmpleoProyectos_capacita;DATABASE=EmpleoProyectos"";Initial Catalog=EmpleoProyectos;"
			Application(Hosting & "_ProyConnectionTimeout") = 60
			Application(Hosting & "_ProyCommandTimeout") = 180
			Application(Hosting & "_ProyCursorLocation") = 3
			Application(Hosting & "_ProyRuntimeUserName") = "PagoDirectoProcesos"
			Application(Hosting & "_ProyRuntimePassword") = "*Empleo/2004Pago*"
			
			'conexion a la base EmpleoLaboral del SQL2005
			Application(Hosting & "_PortalConnectionStringLaboral") = "Provider=MSDASQL;Persist Security Info=True;Extended Properties=""DSN=DSNEmpleoLaboral_capacita;DATABASE=EmpleoLaboral"";Initial Catalog=EmpleoLaboral;"
			Application(Hosting & "_PortalConnectionTimeoutLaboral") = 60
			Application(Hosting & "_PortalCommandTimeoutLaboral") = 180
			Application(Hosting & "_PortalCursorLocationLaboral") = 3
			Application(Hosting & "_PortalRuntimeUserNameLaboral") = "ba"
			Application(Hosting & "_PortalRuntimePasswordLaboral") = "dp_ba_il"
		    
		    'Nombre del sitio Web para el link de salto
			Application(Hosting & "_PortalSitioProyectos") = "PruebaEmpleo/empleo"
			
			'Nombre del sitio Web para el link de salto
			Application(Hosting & "_PortalSitioPersonas") = "PruebaEmpleo/Jefes"
			
			'Conexion a la base del Datawarehouse en SQL
			Application(Hosting & "_DataWareHouseConnectionString") = "Provider=MSDASQL;Persist Security Info=True;Extended Properties=""DSN=EmpleoDW;DATABASE=Empleo_DW"";Initial Catalog=Empleo_DW;"
			Application(Hosting & "_DataWareHouseConnectionTimeout") = 60
			Application(Hosting & "_DataWareHouseCommandTimeout") = 180
			Application(Hosting & "_DataWareHouseCursorLocation") = 3
			Application(Hosting & "_DataWareHouseRuntimeUserName") = "OlapReader"
			Application(Hosting & "_DataWareHouseRuntimePassword") = "consulta"

		    'Reporting Service - LLamada al servicio (link de salto)
			Application(Hosting & "_ReportingService") = "http://flawless/ReportServer/Pages/ReportViewer.aspx?"

		
		'======================================================================
		' Ambiente de Producción
		'======================================================================
		case "GESTION.EMPLE.GOV.AR"
			'Server Name de Produccion
			
			'conexion a la base Seguridad del SQL (online)
            Application(Hosting & "_PortalConnectionString") = "Provider=MSDASQL;Persist Security Info=True;Extended Properties=""DSN=SeguridadOnLine;DATABASE=Seguridad"";Initial Catalog=Seguridad;"
            Application(Hosting & "_PortalConnectionTimeout") = 300
		    Application(Hosting & "_PortalCommandTimeout") = 300
		    Application(Hosting & "_PortalCursorLocation") = 3
		    Application(Hosting & "_PortalRuntimeUserName") = "ExtranetPortal"
		    Application(Hosting & "_PortalRuntimePassword") = "98760"
		    
		    'conexion a la base Seguridad del SQL (replica)
            Application(Hosting & "_PortalReplicaConnectionString") = "Provider=MSDASQL;Persist Security Info=True;Extended Properties=""DSN=SeguridadOnLine;DATABASE=Seguridad"";Initial Catalog=Seguridad;"
            Application(Hosting & "_PortalReplicaConnectionTimeout") = 300
		    Application(Hosting & "_PortalReplicaCommandTimeout") = 300
		    Application(Hosting & "_PortalReplicaCursorLocation") = 3
		    Application(Hosting & "_PortalReplicaRuntimeUserName") = "ExtranetPortal"
		    Application(Hosting & "_PortalReplicaRuntimePassword") = "98760"
		    
		    'conexion a la base EmpleoPersonas del SQL (ONLINE)
		    Application(Hosting & "_JefesConnectionString") = "Provider=MSDASQL;Persist Security Info=True;Extended Properties=""DSN=EmpleoPersonasOnLine;DATABASE=EmpleoPersonas"";Initial Catalog=EmpleoPersonas;"
			Application(Hosting & "_JefesConnectionTimeout") = 300
			Application(Hosting & "_JefesCommandTimeout") = 300
			Application(Hosting & "_JefesCursorLocation") = 3 'adUseServer
			Application(Hosting & "_JefesRuntimeUserName") = "IntranetPortal"
			Application(Hosting & "_JefesRuntimePassword") = "98760"
			'Application(Hosting & "_DTSImportacion") = "\\ODISEA\dtcimp08538$"
			
		    'conexion a la base EmpleoPersonas del SQL (REPLICA)
		    Application(Hosting & "_JefesReplicaConnectionString") = "Provider=MSDASQL;Persist Security Info=True;Extended Properties=""DSN=EmpleoPersonasOnLine;DATABASE=EmpleoPersonas"";Initial Catalog=EmpleoPersonas;"
			Application(Hosting & "_JefesReplicaConnectionTimeout") = 300
			Application(Hosting & "_JefesReplicaCommandTimeout") = 300
			Application(Hosting & "_JefesReplicaCursorLocation") = 3 'adUseServer
			Application(Hosting & "_JefesReplicaRuntimeUserName") = "IntranetPortal"
			Application(Hosting & "_JefesReplicaRuntimePassword") = "98760"
			'Application(Hosting & "_JefesReplicaDTSImportacion") = "\\ODISEA\dtcimp08538$"

			'conexion a la base EmpleoLavoro del SQL
		    Application(Hosting & "_AgendaConnectionString") = "Provider=MSDASQL.1;Persist Security Info=True;Extended Properties=""DSN=EmpleoLavoroOnLine;Description=EmpleoLavoroOnLine;DATABASE=EmpleoLavoro"";Initial Catalog=EmpleoLavoro;"
			Application(Hosting & "_AgendaConnectionTimeout") = 300
			Application(Hosting & "_AgendaCommandTimeout") = 300
			Application(Hosting & "_AgendaCursorLocation") = 3
			Application(Hosting & "_AgendaRuntimeUserName") = "EmpleoLavoro"
			Application(Hosting & "_AgendaRuntimePassword") = "1234*"
			
			'conexion a la base EmpleoProyectos del SQL
			Application(Hosting & "_ProyConnectionString") = "Provider=MSDASQL;Persist Security Info=True;Extended Properties=""DSN=EmpleoProyectosOnLine;DATABASE=EmpleoProyectos"";Initial Catalog=EmpleoProyectos;"
			Application(Hosting & "_ProyConnectionTimeout") = 300
			Application(Hosting & "_ProyCommandTimeout") = 300
			Application(Hosting & "_ProyCursorLocation") = 3
			Application(Hosting & "_ProyRuntimeUserName") = "EmpleoLavoro"
			Application(Hosting & "_ProyRuntimePassword") = "1234*"
			
		    'conexion a la base EmpleoLaboral del SQL2005
			Application(Hosting & "_PortalConnectionStringLaboral") = "Provider=MSDASQL;Persist Security Info=True;Extended Properties=""DSN=DSNEmpleoLaboral;DATABASE=EmpleoLaboral"";Initial Catalog=EmpleoLaboral;"
			Application(Hosting & "_PortalConnectionTimeoutLaboral") = 60
			Application(Hosting & "_PortalCommandTimeoutLaboral") = 180
			Application(Hosting & "_PortalCursorLocationLaboral") = 3
			Application(Hosting & "_PortalRuntimeUserNameLaboral") = "ba"
			Application(Hosting & "_PortalRuntimePasswordLaboral") = "dp_ba_il"
		    
		    'Nombre del sitio Web para el link de salto
			Application(Hosting & "_PortalSitioProyectos") = "gestionempleo.trabajo.gov.ar/Empleo"		
			
			'Nombre del sitio Web para el link de salto
			Application(Hosting & "_PortalSitioPersonas") = "gestionempleo.trabajo.gov.ar/Jefes"    
			
			' Conexion a la base del Datawarehouse en SQL
			Application(Hosting & "_DataWareHouseConnectionString") = "Provider=MSDASQL;Persist Security Info=True;Extended Properties=""DSN=EmpleoDW;DATABASE=Empleo_DW"";Initial Catalog=Empleo_DW;"
			Application(Hosting & "_DataWareHouseConnectionTimeout") = 300
			Application(Hosting & "_DataWareHouseCommandTimeout") = 300
			Application(Hosting & "_DataWareHouseCursorLocation") = 3
			Application(Hosting & "_DataWareHouseRuntimeUserName") = "OlapReader"
			Application(Hosting & "_DataWareHouseRuntimePassword") = "consulta"

		    ' Reporting Service - LLamada al servicio (link de salto)
			Application(Hosting & "_ReportingService") = "http://reportes.trabajo.gov.ar/ReportServerInternet/Pages/ReportViewer.aspx?"

		'======================================================================
		' sitio no definido
		'======================================================================
		case else 
			Response.Write "<br>Entorno o Hosting No Definido  - Error en OpenConn <br> ServerName: " & Request.ServerVariables("SERVER_NAME")
			Response.end
			
	end select 
	
	' Determina ServerName
    If Len(ServerName) = 0 Then
	    ServerName = Request.ServerVariables("SERVER_NAME")
	    Application("SERVER_NAME") = ServerName
    End If
    ServerName = Request.ServerVariables("SERVER_NAME")



'------------------------------------------------
'	Coneccion a Abrir: Base Seguridad (ON LINE)
'-------------------------------------------------
	With gConnSeguridad
	.ConnectionString = Application(ServerName & "_PortalConnectionString")	
	.ConnectionTimeout = Application(ServerName & "_PortalConnectionTimeout")
	.CommandTimeout = Application(ServerName & "_PortalCommandTimeout")
	.CursorLocation = Application(ServerName & "_PortalCursorLocation")
	.Open ,Application(ServerName & "_PortalRuntimeUserName"), Application(ServerName & "_PortalRuntimePassword")
	End With
    
'-----------------------------------------------------
'	Coneccion a Abrir: Base Seguridad (REPLICA)
'------------------------------------------------------
	With gConnSeguridadReplica
	.ConnectionString = Application(ServerName & "_PortalConnectionString")	
	.ConnectionTimeout = Application(ServerName & "_PortalConnectionTimeout")
	.CommandTimeout = Application(ServerName & "_PortalCommandTimeout")
	.CursorLocation = Application(ServerName & "_PortalCursorLocation")
	.Open ,Application(ServerName & "_PortalRuntimeUserName"), Application(ServerName & "_PortalRuntimePassword")
	End With
    
    
'--------------------------------------------------------------
'	Coneccion a Abrir: Base EmpleoPersonas (ONLINE)
'--------------------------------------------------------------
	With ConnEmpleoPersonas
		.ConnectionString = Application(ServerName & "_JefesConnectionString")	
		.ConnectionTimeout = Application(ServerName & "_JefesConnectionTimeout")
		.CommandTimeout = Application(ServerName & "_JefesCommandTimeout")
		.CursorLocation = Application(ServerName & "_JefesCursorLocation")
		.Open ,Application(ServerName & "_JefesRuntimeUserName"), Application(ServerName & "_JefesRuntimePassword")
		'Carpeta = Application(ServerName & "_DTSImportacion")
		'conn.Open "Provider=SQLOLEDB.1;Password=98760;Persist Security Info=False;User ID=Intranet;Initial Catalog=EmpleoPersonas;Data Source=RAEL"
		'Response.Write .ConnectionString
	End With
	

'--------------------------------------------------------------
'	Coneccion a Abrir: Base EmpleoPersonas (REPLICA)
'--------------------------------------------------------------
	With ConnEmpleoPersonasReplica
		.ConnectionString = Application(ServerName & "_JefesReplicaConnectionString")	
		.ConnectionTimeout = Application(ServerName & "_JefesReplicaConnectionTimeout")
		.CommandTimeout = Application(ServerName & "_JefesReplicaCommandTimeout")
		.CursorLocation = Application(ServerName & "_JefesReplicaCursorLocation")
		.Open ,Application(ServerName & "_JefesReplicaRuntimeUserName"), Application(ServerName & "_JefesReplicaRuntimePassword")
		'Carpeta = Application(ServerName & "_DTSImportacion")
		'conn.Open "Provider=SQLOLEDB.1;Password=98760;Persist Security Info=False;User ID=Intranet;Initial Catalog=EmpleoPersonas;Data Source=RAEL"
		'Response.Write .ConnectionString
	End With
	
	
'----------------------------------------
'	Coneccion a Abrir: Base EmpleoLavoro
'----------------------------------------	
	with connLavoro
	    .ConnectionString = Application(ServerName & "_AgendaConnectionString")	
	    .ConnectionTimeout = Application(ServerName & "_AgendaConnectionTimeout")
	    .CommandTimeout = Application(ServerName & "_AgendaCommandTimeout")
	    .CursorLocation = Application(ServerName & "_AgendaCursorLocation")
	    .Open ,Application(ServerName & "_AgendaRuntimeUserName"), Application(ServerName & "_AgendaRuntimePassword")
	end with

'------------------------------------------
'	Coneccion a Abrir: Base EmpleoProyectos
'------------------------------------------	
	with connProyectos
	    .ConnectionString = Application(ServerName & "_ProyConnectionString")	
	    .ConnectionTimeout = Application(ServerName & "_ProyConnectionTimeout")
	    .CommandTimeout = Application(ServerName & "_ProyCommandTimeout")
	    .CursorLocation = Application(ServerName & "_ProyCursorLocation")
	    .Open ,Application(ServerName & "_ProyRuntimeUserName"), Application(ServerName & "_ProyRuntimePassword")
	end with
	
'------------------------------------------
'	Coneccion a Abrir: Base EmpleoLaboral
'------------------------------------------   

    With CC
   		.ConnectionString = Application(ServerName & "_PortalConnectionStringLaboral")	
	    .ConnectionTimeout = Application(ServerName & "_PortalConnectionTimeoutLaboral")
	    .CommandTimeout = Application(ServerName & "_PortalCommandTimeoutLaboral")
	    .CursorLocation = Application(ServerName & "_PortalCursorLocationLaboral")
	    .Open ,Application(ServerName & "_PortalRuntimeUserNameLaboral"), Application(ServerName & "_PortalRuntimePasswordLaboral")

	End With
	
	' Fin Modificacion M3
	Set CX = CC
	GOracleConnString = Application(ServerName & "_PortalConnectionStringLaboral")


'---------------------------------------
'	Links Varios
'---------------------------------------   
	Application("GPortalSitioProyectos") = Application(ServerName & "_PortalSitioProyectos")
	Application("LinkReportingService") = Application(ServerName & "_ReportingService")
	    
	'Response.Write ucase(Request.ServerVariables("SERVER_NAME"))
	'Response.Write Progetto
End If

%>
