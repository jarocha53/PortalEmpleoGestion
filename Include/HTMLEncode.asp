<%
' Funzione:	Function strHTMLEncode(sStringa)
'			Verifica che la stringa passata sia valorizzata prima di 
'			applicare la Server.HTMLEncode. 
' Ritorna: 	una valore di tipo stringa
' Autore:	Marco Attenni
' Data:		25-11-2002		
' Modifica:	??-??-????

Function strHTMLEncode(sStringa)

	if sStringa > "" then
		strHTMLEncode = server.HTMLEncode(sStringa)
	end if

End Function
%>