<!-- #include Virtual = "/Include/ControlNum.inc"-->

function ValidateRangeDate(sData1,sData2){
	/* La funzione restituisce
		TRUE  : sData1<=sData2
		FALSE : sData1>sData2 */
	//Se siamo entrati qui dentro per controllare due date, devo 
	//accertarmi che le due date che mi vengono passate non siano vuote
	/*
	//DATA1 obbligatoria
		if (sData1 == ""){
			alert("Fecha obligatoria")
			return false
		}
		
	//DATA2 obbligatoria
		if (sData2 == ""){
			alert("Fecha obligatoria")
			return false
		}
	*/
		if (sData2.substr(6,4) >= sData1.substr(6,4)){
			if (sData2.substr(6,4) == sData1.substr(6,4)){
				if (sData2.substr(3,2) >= sData1.substr(3,2)){
					if (sData2.substr(3,2) == sData1.substr(3,2)){
						if (sData2.substr(0,2) < sData1.substr(0,2)){
							//alert("Giorno - Data Fine minore della Data Inizio")
							return false
						}
					}
				}
				else {
					//alert("Mese - Data Fine minore della Data Inizio")
					return false
				}
			}
		}
		else {
			//alert("Anno - Data Fine minore della Data Inizio")
			return false
		}
		return true
	}

	function ValidateInputDate(sData){
		//controllo lunghezza stringa in Input
			if (sData.length != 10) {
				alert("Formato de fecha err�neo, el formato debe ser del tipo dd/mm/aaaa")
				return false
			}
		//alla posizione 2 e 5 ci devono essere solo /
			if ((sData.substr(2,1) != "/") || 
				(sData.substr(5,1) != "/")){
				alert("Formato de fecha err�neo, el formato debe ser del tipo dd/mm/aaaa")
				return false
			}
		//il giorno/mese della DATA non possono essere zero
			if ( ((sData.substr(0,2) == "00"))  || 
				((sData.substr(3,2) == "00")) ){
				alert("Fecha no aceptable")
				return false
			}
		//la DATA deve essere numerica
			if ( (!IsNum(sData.substr(0,2)))  || 
				(!IsNum(sData.substr(3,2)))   || 
    			 (!IsNum(sData.substr(6,4))) ){
				alert("Fecha no num�rica")
				return false
			}
		//il giorno della DATA non deve essere superiore a 31
			if (sData.substr(0,2) > "31"){
				alert("El d�a indicado es err�neo")
				return false
			}
		//il mese della DATA non deve essere superiore a 12
			if (sData.substr(3,2) > "12"){
				alert("El mes indicado es err�neo")
				return false
			}
		//l'anno della DATA deve essere superiore al 1900
			if (sData.substr(6,4) < 1900){
				alert("El a�o de la fecha debe ser superior al 1900\n o la fecha no est� en el formato dd/mm/aaaa.")
				return false
			}
		//Controllo che i mesi 4, 6, 9, 11 siano di 30 giorni
			if ( (sData.substr(3,2) == "04") ||
				(sData.substr(3,2) == "06") ||
				(sData.substr(3,2) == "09") ||
				(sData.substr(3,2) == "11") ){	
				if (sData.substr(0,2) > "30"){
					alert("El d�a indicado es err�neo")
					return false
				}
			}
		//Se sono in febbraio controllo la bisestilit� dell'anno della DATA
			if (sData.substr(3,2) == "02"){
				if (leapYear (sData.substr(6,4))==1){
					if (sData.substr(0,2) > "29"){
						alert ("El d�a indicado es err�neo - (A�o bisiesto)")
						return false
					}
				}
				else {
					if (sData.substr(0,2) > "28"){
						alert ("El d�a indicado es err�neo - (A�o no bisiesto)")
						return false
					}
				}
			}	
		return true
	}
	
	//Calcolo per l'anno bisestile
	function leapYear (Year) {
		if (((Year % 4)==0) && ((Year % 100)!=0) || ((Year % 400)==0))
			return (1);
		else
			return (0);
	}

	function ValidateInputDateA(sData){
		//if (sData.substr(6,4) < 1900){
		if (sData < 1900){
			alert("El a�o de la fecha debe superior al 1900")
			return false
		}
		
	return true	
	}

	function ValidateInputDateMA(sData){
		//alla posizione 2 e 5 ci devono essere solo /
			if ((sData.substr(2,1) != "/") || 
				(sData.substr(5,1) != "/")){
				alert("Formato de fecha err�neo el formato debe ser del tipo mm/aaaa")
				return false
			}
		//il giorno/mese della DATA non possono essere zero
			if ( ((sData.substr(0,2) == "00"))  || 
				((sData.substr(3,2) == "00")) ){
				alert("Fecha no aceptable")
				return false
			}
		//la DATA deve essere numerica
			if ( (!IsNum(sData.substr(0,2)))  || 
				(!IsNum(sData.substr(3,2)))   || 
    			 (!IsNum(sData.substr(6,4))) ){
				alert("Fecha no num�rica")
				return false
			}
		//il giorno della DATA non deve essere superiore a 31
			if (sData.substr(0,2) > "31"){
				alert("El d�a indicado es err�neo")
				return false
			}
		//il mese della DATA non deve essere superiore a 12
			if (sData.substr(3,2) > "12"){
				alert("El mes indicado es err�neo")
				return false
			}
		//l'anno della DATA deve essere superiore al 1900
			if (sData.substr(6,4) < 1900){
				alert("El a�o de la fecha debe ser superior al 1900")
				return false
			}
		//Controllo che i mesi 4, 6, 9, 11 siano di 30 giorni
			if ( (sData.substr(3,2) == "04") ||
				(sData.substr(3,2) == "06") ||
				(sData.substr(3,2) == "09") ||
				(sData.substr(3,2) == "11") ){	
				if (sData.substr(0,2) > "30"){
					alert("El d�a indicado es err�neo")
					return false
				}
			}
		//Se sono in febbraio controllo la bisestilit� dell'anno della DATA
			if (sData.substr(3,2) == "02"){
				if (leapYear (sData.substr(6,4))==1){
					if (sData.substr(0,2) > "29"){
						alert ("El d�a indicado es err�neo - (A�o bisiesto)")
						return false
					}
				}
				else {
					if (sData.substr(0,2) > "28"){
						alert ("El d�a indicado es err�neo - (A�o no bisiesto)")
						return false
					}
				}
			}	
		return true
	}
	
	//Calcolo per l'anno bisestile
	function leapYear (Year) {
		if (((Year % 4)==0) && ((Year % 100)!=0) || ((Year % 400)==0))
			return (1);
		else
			return (0);
	}


function ValidateRangeDateAvisos(sData1,sData2){
	/* La funzione restituisce
		TRUE  : sData1<=sData2
		FALSE : sData1>sData2 */
	//Se siamo entrati qui dentro per controllare due date, devo 
	//accertarmi che le due date che mi vengono passate non siano vuote
	/*
	//DATA1 obbligatoria
		if (sData1 == ""){
			alert("Fecha obligatoria")
			return false
		}
		
	//DATA2 obbligatoria
		if (sData2 == ""){
			alert("Fecha obligatoria")
			return false
		}
	*/
		if (sData2.substr(6,4) >= sData1.substr(6,4)){
			if (sData2.substr(6,4) == sData1.substr(6,4)){
				if (sData2.substr(3,2) >= sData1.substr(3,2)){
					if (sData2.substr(3,2) == sData1.substr(3,2)){
						if (sData2.substr(0,2) <= sData1.substr(0,2)){
							//alert("Giorno - Data Fine minore della Data Inizio")
							return false
						}
					}
				}
				else {
					//alert("Mese - Data Fine minore della Data Inizio")
					return false
				}
			}
		}
		else {
			//alert("Anno - Data Fine minore della Data Inizio")
			return false
		}
		return true
	}