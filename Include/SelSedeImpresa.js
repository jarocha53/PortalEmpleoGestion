//RICERCA DELLE SEDI 
function SelSedeImpresa(NomeForm, NomeCampoCfAzienda, NomeCampoPiAzienda, NomeCampoIdSede, NomeCampoDescSede, NomeRS) {

	AppoValoreCfAzienda = eval(NomeForm + "." + NomeCampoCfAzienda);
	AppoValorePiAzienda = eval(NomeForm + "." + NomeCampoPiAzienda);
	AppoValoreRS = eval(NomeForm + "." + NomeRS);

	ValoreCfAzienda = AppoValoreCfAzienda.value;
	ValorePiAzienda = AppoValorePiAzienda.value;	
	RS = AppoValoreRS.value;

    if (TRIM(ValoreCfAzienda) == '' && TRIM(ValorePiAzienda) == '' && TRIM(RS) == '') {
		alert("Ingrese el campo REGICE, CUIT o RAZON SOCIAL");
    }else{
	    if (TRIM(ValoreCfAzienda) > '' && TRIM(ValorePiAzienda) > '' && TRIM(RS) == '') {
			alert("Los campos REGICE, CUIT o RAZON SOCIAL de la empresa son alternativos");
		}else{
			window.open ('/include/SelSedeImpresa.asp?NomeForm=' + NomeForm + '&TxtCfAzienda=' + ValoreCfAzienda + '&TxtPiAzienda=' + ValorePiAzienda + '&NomeTxtIdSede=' + NomeCampoIdSede + '&NomeTxtDescSede=' + NomeCampoDescSede + "&RS=" + RS,'Info','width=550,height=450,Resize=No,Scrollbars=yes');
		}
	}
}
