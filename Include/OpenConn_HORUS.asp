<%

'====================================================================================
'	Esta es la Unica pagina que tiene las conexiones a las BASES sean SQL u ORACLE
'====================================================================================

'Creazione Oggetti ADODB.-----------------------------
If  (Session("Progetto") & "*") = "*" Then
	Response.Redirect "/util/error_login.asp"
Else 
	Progetto = LCase(mid(Session("Progetto"),2))
	Set CC = Server.CreateObject("ADODB.Connection") 'Objeto de conexion al Oracle
	Set gConnSeguridad = Server.CreateObject("ADODB.Connection") 'Objeto de conexion a la Base Seguridad
	Set Conn = Server.CreateObject("ADODB.Connection")	' Objeto de Conexion a la Base EmpleoPersonas
	Set connLavoro = Server.CreateObject("adodb.connection") ' Objeto de Conexion a la Base EmpleoLavoro

	'MODIFICA PWD DI CONNESSIONE
	pwd = "dp_" & Progetto & "_il" 

	select case ucase(Request.ServerVariables("SERVER_NAME")) 
	
	    '======================================================================
	    ' Sitio en espa�ol Ambiente de Desarrollo
	    '======================================================================
		case "PORTALDESARROLLO" 
			' proyectos BA en oracle
    		'CC.Open "Provider=OraOLEDB.Oracle.1;user id=" &  Progetto & "; password=" &  pwd & "; data source=portal_rael"

			' proyectos BA en oracle en prueba
    		'CC.Open "Provider=OraOLEDB.Oracle.1;user id=" &  Progetto & "; password=" &  pwd & "; data source=portal_poseidon"
            
            'Server Name de desarrollo
			Hosting = "PORTALDESARROLLO"
			
			'conexion a la base Seguridad del SQL
            Application(Hosting & "_PortalConnectionString") = "Provider=MSDASQL;Password=98760;Persist Security Info=True;User ID=Extranet;Extended Properties=""DSN=Seguridad;UID=Extranet;DATABASE=Seguridad"";Initial Catalog=Seguridad;"
            Application(Hosting & "_PortalConnectionTimeout") = 60
		    Application(Hosting & "_PortalCommandTimeout") = 180
		    Application(Hosting & "_PortalCursorLocation") = 3
		    Application(Hosting & "_PortalRuntimeUserName") = "Extranet"
		    Application(Hosting & "_PortalRuntimePassword") = "98760"
		    
		    'conexion a la base EmpleoPersonas del SQL
		    Application(Hosting & "_JefesConnectionString") = "Provider=MSDASQL;Password=98760;Persist Security Info=True;User ID=Extranet;Extended Properties=""DSN=EmpleoPersonas;UID=Extranet;DATABASE=EmpleoPersonas"";Initial Catalog=EmpleoPersonas;"
			Application(Hosting & "_JefesConnectionTimeout") = 60
			Application(Hosting & "_JefesCommandTimeout") = 180
			Application(Hosting & "_JefesCursorLocation") = 3 'adUseServer
			Application(Hosting & "_JefesRuntimeUserName") = "Intranet"
			Application(Hosting & "_JefesRuntimePassword") = "98760"
			Application(Hosting & "_DTSImportacion") = "\\Dory\dtcimp08538$"

			'conexion a la base EmpleoLavoro del SQL
		    Application(Hosting & "_AgendaConnectionString") = "Provider=MSDASQL.1;Password=1234*;Persist Security Info=True;User ID=EmpleoLavoro;Data Source=EmpleoLavoro;Extended Properties=""DSN=EmpleoLavoro;Description=EmpleoLavoro;UID=EmpleoLavoro;APP=????4?????4?????T0???????;WSID=PC0297;DATABASE=EmpleoLavoro"";Initial Catalog=EmpleoLavoro;User Id=EmpleoLavoro;PASSWORD=1234*;"
			Application(Hosting & "_AgendaConnectionTimeout") = 60
			Application(Hosting & "_AgendaCommandTimeout") = 180
			Application(Hosting & "_AgendaCursorLocation") = 3
			Application(Hosting & "_AgendaRuntimeUserName") = "EmpleoLavoro"
			Application(Hosting & "_AgendaRuntimePassword") = "1234*"
	
		    'Conexion a la base Portal Oracle
		    Application(Hosting & "_PortalConnectionStringOracle") = "Provider=OraOLEDB.Oracle.1;user id=" &  Progetto & "; password=" &  pwd & "; data source=portal_rael"
            Application(Hosting & "_PortalConnectionTimeoutOracle") = 60
		    Application(Hosting & "_PortalCommandTimeoutOracle") = 180
		    Application(Hosting & "_PortalCursorLocationOracle") = 3
		    		    
'	Response.Write Application(Hosting & "_PortalConnectionStringOracle")		    		    
'	Response.end
	
		'======================================================================
		' Sitio en espa�ol Ambiente de Prueba
		'======================================================================
		case "PORTALPRUEBA" 
			' proyectos BA en oracle
    		'CC.Open "Provider=OraOLEDB.Oracle.1;user id=" &  Progetto & "; password=" &  pwd & "; data source=portal_poseidon"
					
			'CC.Open "Provider=OraOLEDB.Oracle.1;user id=PLAVORO; password=" &  pwd & "; data source=portal_baggins"
			
			
			'Server Name de Prueba
			Hosting = "PORTALPRUEBA"
			
			'conexion a la base Seguridad del SQL
            Application(Hosting & "_PortalConnectionString") = "Provider=MSDASQL;Password=98760;Persist Security Info=True;User ID=Extranet;Extended Properties=""DSN=Seguridad;UID=Extranet;DATABASE=Seguridad"";Initial Catalog=Seguridad;"
            Application(Hosting & "_PortalConnectionTimeout") = 60
		    Application(Hosting & "_PortalCommandTimeout") = 180
		    Application(Hosting & "_PortalCursorLocation") = 3
		    Application(Hosting & "_PortalRuntimeUserName") = "Extranet"
		    Application(Hosting & "_PortalRuntimePassword") = "98760"
		    
		    'conexion a la base EmpleoPersonas del SQL
		    Application(Hosting & "_JefesConnectionString") = "Provider=MSDASQL;Password=98760;Persist Security Info=True;User ID=Extranet;Extended Properties=""DSN=EmpleoPersonas;UID=Extranet;DATABASE=EmpleoPersonas"";Initial Catalog=EmpleoPersonas;"
			Application(Hosting & "_JefesConnectionTimeout") = 60
			Application(Hosting & "_JefesCommandTimeout") = 180
			Application(Hosting & "_JefesCursorLocation") = 3 'adUseServer
			Application(Hosting & "_JefesRuntimeUserName") = "Intranet"
			Application(Hosting & "_JefesRuntimePassword") = "98760"
			Application(Hosting & "_DTSImportacion") = "\\Dummy\dtcimp08538$"
			
			'conexion a la base EmpleoLavoro del SQL
		    Application(Hosting & "_AgendaConnectionString") = "Provider=MSDASQL.1;Password=1234*;Persist Security Info=True;User ID=EmpleoLavoro;Data Source=EmpleoLavoro;Extended Properties=""DSN=EmpleoLavoro;Description=EmpleoLavoro;UID=EmpleoLavoro;APP=????4?????4?????T0???????;WSID=PC0297;DATABASE=EmpleoLavoro"";Initial Catalog=EmpleoLavoro;User Id=EmpleoLavoro;PASSWORD=1234*;"
			Application(Hosting & "_AgendaConnectionTimeout") = 60
			Application(Hosting & "_AgendaCommandTimeout") = 180
			Application(Hosting & "_AgendaCursorLocation") = 3
			Application(Hosting & "_AgendaRuntimeUserName") = "EmpleoLavoro"
			Application(Hosting & "_AgendaRuntimePassword") = "1234*"
			
		    'Conexion a la base Portal Oracle
		    Application(Hosting & "_PortalConnectionStringOracle") = "Provider=OraOLEDB.Oracle.1;user id=" &  Progetto & "; password=" &  pwd & "; data source=portal_poseidon"
            Application(Hosting & "_PortalConnectionTimeoutOracle") = 60
		    Application(Hosting & "_PortalCommandTimeoutOracle") = 180
		    Application(Hosting & "_PortalCursorLocationOracle") = 3
		    
		'======================================================================
		' Sitio en espa�ol Ambiente de Producci�n
		'======================================================================
		case "PORTALEMPLEO" , "WWW.EMPLE.GOV.AR" 
		    ' proyectos BA en oracle
    		'CC.Open "Provider=OraOLEDB.Oracle.1;user id=" &  Progetto & "; password=" &  pwd & "; data source=portal_baggins"
			
			'Server Name de Produccion
			Hosting = ucase(Request.ServerVariables("SERVER_NAME"))
		 			
			'conexion a la base Seguridad del SQL
            Application(Hosting & "_PortalConnectionString") = "Provider=MSDASQL;Password=98760;Persist Security Info=True;User ID=Extranet;Extended Properties=""DSN=Seguridad;UID=Extranet;DATABASE=Seguridad"";Initial Catalog=Seguridad;"
            Application(Hosting & "_PortalConnectionTimeout") = 60
		    Application(Hosting & "_PortalCommandTimeout") = 180
		    Application(Hosting & "_PortalCursorLocation") = 3
		    Application(Hosting & "_PortalRuntimeUserName") = "Extranet"
		    Application(Hosting & "_PortalRuntimePassword") = "98760"
		    
		    'conexion a la base EmpleoPersonas del SQL
		    Application(Hosting & "_JefesConnectionString") = "Provider=MSDASQL;Password=98760;Persist Security Info=True;User ID=Extranet;Extended Properties=""DSN=EmpleoPersonas;UID=Extranet;DATABASE=EmpleoPersonas"";Initial Catalog=EmpleoPersonas;"
			Application(Hosting & "_JefesConnectionTimeout") = 60
			Application(Hosting & "_JefesCommandTimeout") = 180
			Application(Hosting & "_JefesCursorLocation") = 3 'adUseServer
			Application(Hosting & "_JefesRuntimeUserName") = "Intranet"
			Application(Hosting & "_JefesRuntimePassword") = "98760"
			'Application(Hosting & "_DTSImportacion") = "\\ODISEA\dtcimp08538$"
			
			'conexion a la base EmpleoLavoro del SQL
		    Application(Hosting & "_AgendaConnectionString") = "Provider=MSDASQL.1;Password=1234*;Persist Security Info=True;User ID=EmpleoLavoro;Data Source=EmpleoLavoro;Extended Properties=""DSN=EmpleoLavoro;Description=EmpleoLavoro;UID=EmpleoLavoro;APP=????4?????4?????T0???????;WSID=PC0297;DATABASE=EmpleoLavoro"";Initial Catalog=EmpleoLavoro;User Id=EmpleoLavoro;PASSWORD=1234*;"
			Application(Hosting & "_AgendaConnectionTimeout") = 60
			Application(Hosting & "_AgendaCommandTimeout") = 180
			Application(Hosting & "_AgendaCursorLocation") = 3
			Application(Hosting & "_AgendaRuntimeUserName") = "EmpleoLavoro"
			Application(Hosting & "_AgendaRuntimePassword") = "1234*"
			
		    'Conexion a la base Portal Oracle
		    Application(Hosting & "_PortalConnectionStringOracle") = "Provider=OraOLEDB.Oracle.1;user id=" &  Progetto & "; password=" &  pwd & "; data source=portal_baggins"
            Application(Hosting & "_PortalConnectionTimeoutOracle") = 60
		    Application(Hosting & "_PortalCommandTimeoutOracle") = 180
		    Application(Hosting & "_PortalCursorLocationOracle") = 3		    
		' Sitios anteriores 
		'case else 
		'	select case ucase(Progetto)
		'	   case  "DEMO"
		'			CC.Open "Provider=SQLOLEDB.1;User ID=BA; password=BA; Initial Catalog=Portale; Data Source=Rael"
		'	   case else
		'	        CC.Open "Provider=OraOLEDB.Oracle.1;user id=" &  Progetto & "; password=" &  pwd & "; data source=portale_baggins"
		'	end select
		
		'===============================================================================================
		'Agregar las conexiones desde Internet e intranet cuando se hallan decidido los nombres
		'===============================================================================================
		'Ejemplo
		'Hosting = "cuilpublico"
		'Application(Hosting & "_JefesCPConnectionString") = "Provider=MSDASQL;Password=98760;Persist Security Info=True;User ID=Extranet;Extended Properties=""DSN=EmpleoPersonas;UID=Extranet;DATABASE=EmpleoPersonas"";Initial Catalog=EmpleoPersonas;"
		'Application(Hosting & "_JefesCPConnectionTimeout") = 60
		'Application(Hosting & "_JefesCPCommandTimeout") = 180
		'Application(Hosting & "_JefesCPCursorLocation") = 3
		'Application(Hosting & "_JefesCPRuntimeUserName") = "Extranet"
		'Application(Hosting & "_JefesCPRuntimePassword") = "98760"
		
	end select 
	
	' Determina ServerName
    If Len(ServerName) = 0 Then
	    ServerName = Request.ServerVariables("SERVER_NAME")
	    Application("SERVER_NAME") = ServerName
    End If
	
'---------------------------------------
'	Coneccion a Abrir: Base Seguridad
'---------------------------------------
	With gConnSeguridad
	.ConnectionString = Application(ServerName & "_PortalConnectionString")	
	.ConnectionTimeout = Application(ServerName & "_PortalConnectionTimeout")
	.CommandTimeout = Application(ServerName & "_PortalCommandTimeout")
	.CursorLocation = Application(ServerName & "_PortalCursorLocation")
	.Open ,Application(ServerName & "_PortalRuntimeUserName"), Application(ServerName & "_PortalRuntimePassword")
	''Response.Write .ConnectionString
    End With
    
    
'------------------------------------------
'	Coneccion a Abrir: Base EmpleoPersonas
'------------------------------------------
	With Conn
		.ConnectionString = Application(ServerName & "_JefesConnectionString")	
		.ConnectionTimeout = Application(ServerName & "_JefesConnectionTimeout")
		.CommandTimeout = Application(ServerName & "_JefesCommandTimeout")
		.CursorLocation = Application(ServerName & "_JefesCursorLocation")
		.Open ,Application(ServerName & "_JefesRuntimeUserName"), Application(ServerName & "_JefesRuntimePassword")
		'Carpeta = Application(ServerName & "_DTSImportacion")
		'conn.Open "Provider=SQLOLEDB.1;Password=98760;Persist Security Info=False;User ID=Intranet;Initial Catalog=EmpleoPersonas;Data Source=RAEL"
		'Response.Write .ConnectionString
	End With
	
	
'----------------------------------------
'	Coneccion a Abrir: Base EmpleoLavoro
'----------------------------------------	
	with connLavoro
	    .ConnectionString = Application(ServerName & "_AgendaConnectionString")	
	    .ConnectionTimeout = Application(ServerName & "_AgendaConnectionTimeout")
	    .CommandTimeout = Application(ServerName & "_AgendaCommandTimeout")
	    .CursorLocation = Application(ServerName & "_AgendaCursorLocation")
	    .Open ,Application(ServerName & "_AgendaRuntimeUserName"), Application(ServerName & "_AgendaRuntimePassword")
	end with
	
'---------------------------------------
'	Coneccion a Abrir: Base Oracle
'---------------------------------------   
    With CC
	.ConnectionString = Application(ServerName & "_PortalConnectionStringOracle")	
	.ConnectionTimeout = Application(ServerName & "_PortalConnectionTimeoutOracle")
	.CommandTimeout = Application(ServerName & "_PortalCommandTimeoutOracle")
	.CursorLocation = Application(ServerName & "_PortalCursorLocationOracle")
	.Open ',Application(ServerName & "_PortalRuntimeUserName"), Application(ServerName & "_PortalRuntimePassword")
	''Response.Write .ConnectionString
	End With
	
	GOracleConnString = Application(ServerName & "_PortalConnectionStringOracle")
    
	'Response.Write ucase(Request.ServerVariables("SERVER_NAME"))
	'Response.Write Progetto
End If

%>