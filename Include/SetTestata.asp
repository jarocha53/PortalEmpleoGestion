
<script LANGUAGE="JavaScript" src="/Include/Help.inc">
</script>

<%
'Questo include permette di visualizzare la testata delle pagine;
'Nel programma chiamante occorre definire e valorizzare le seguenti variabili:
' - "sFunzione"   --> Nome della funzione
' - "sTitolo"     --> Titolo della pagina
' - "sCommento"   --> Commento della pagina
' - "bCampiObbl"  --> Flag per visualizzare la dicitura "Campi obbligatori"
' - "sHelp"       --> Nome della pagina contenente l'help (non attivo)
%>
<!--table border="0" width="520" cellspacing="0" cellpadding="0" height="81">   <tr>     <td width="500" background="<%=Session("Progetto")%>/images/titoli/strumenti2b.gif" height="81" valign="bottom" align="right">       <table border="0" width="260" height="30" cellspacing="0" cellpadding="0">         <tr>           <td width="100%" valign="top" align="right"><b class="tbltext1a"><%=sFunzione%></span></b></td>         </tr>       </table>     </td>   </tr></table-->
<br>
<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
	<tr height="18">
		<td class="sfondomenu" width="67%" height="18"><span class="tbltext0"><b>&nbsp;<%=sTitolo%></b></span></td>
		<td><img src="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
		<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">
<%			
			if bCampiObbl then
				Response.Write "(*) campi obbligatori"
			end if
%>
		</td>
	</tr>
</table>
<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
	<tr>
		<td align="left" class="sfondocomm" width="95%">
			<%=sCommento%>
		</td>
		<td valign="bottom" class="sfondocomm" width="5%">
			<%
			if sHelp="" then
			%>
			    <img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0" onmouseover="javascript:window.status='' ; return true">
			<%
			else
			%>
				<a href="Javascript:Show_Help('<%=sHelp%>')">
					<img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0" onmouseover="javascript:window.status='' ; return true">
				</a>
			<%
			end if
			%>
			<!--	<img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0" onmouseover="javascript:window.status='' ; return true"><!--/a-->
		</td>
	</tr>
	<tr height="2">
		<td class="sfondocomm" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif">
		</td>
	</tr>
</table>
<br>
