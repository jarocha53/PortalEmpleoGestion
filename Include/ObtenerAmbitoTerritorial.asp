<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%
public function DeterminarAmbitoTerritorialSQL()

''*********************************************''
''vani posibles roles admitidos determinan el 
''ambito territorial para los casos distintos de 
''selecciones en las cuales se determina en la base

''devuelve la condicion necesaria para agregar al query 
''que filtra el ambito territorial

'entrevistador con seguro  17
'entrevistador sin seguro 23
'entrevistador gerencia 27
'supervisor basico 25
'supervisor con seguro 24
'consultor oficina 22
'consultor provincia 21
'consultor pais 18

dim Retorna
dim RsProv

set RsProv = server.CreateObject("adodb.recordset")

if Session("Creator") <> "" and Session("Creator") <> 0 then

    VariableSQL = "Select prv from sede_impresa where id_sede = " & Session("Creator")
    'PL-SQL * T-SQL
    VariableSQL = TransformPLSQLToTSQL(VariableSQL)

	Set RsProv = cc.execute(VariableSQL)

	Provincia = RsProv("Prv")

	set RsProv = nothing
	Rol = Session("idgruppo")
else
	Rol = 18
end if 



select case Rol 
	case 17,23,27,25,24,22 ''ven solo la oficina
		Retorna = Retorna & " and id_cimpiego = " & Session("Creator")
	case 21 ''ven solo la provincia
		Retorna = Retorna & " AND ID_CIMPIEGO IN (SELECT DISTINCT STATO_OCCUPAZIONALE.ID_CIMPIEGO FROM STATO_OCCUPAZIONALE, SEDE_IMPRESA WHERE STATO_OCCUPAZIONALE.ID_CIMPIEGO = SEDE_IMPRESA.ID_SEDE AND SEDE_IMPRESA.PRV = " & Provincia & ") "	
		'" AND SO.ID_CIMPIEGO IN (SELECT DISTINCT STATO_OCCUPAZIONALE.ID_CIMPIEGO FROM STATO_OCCUPAZIONALE, SEDE_IMPRESA WHERE STATO_OCCUPAZIONALE.ID_CIMPIEGO = SEDE_IMPRESA.ID_SEDE AND SEDE_IMPRESA.PRV = " & Provincia & ") "
		
	case 18 ''ven todo el pais
		Retorna = Retorna & ""
	case else

end select 

if Rol = 18 and (Session("Creator") <> "" and Session("Creator") <> "0") then
		Retorna = Retorna & " and id_cimpiego = " & Session("Creator")
end if 

DeterminarAmbitoTerritorialSQL = Retorna

'Response.Write Retorna
''*********************************************''
end function
%>
