<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%
'---------------------------------------------------------
'Parametri di Input
'	Codice Sessione
'   Codice Aula
'	Codice Classe
'   Data inizio periodo
'   Data fine periodo
'   descrizione del bando
'   descrizione della sessione formativa
'
'Se il Report viene creato 
'	La funzione ritorna il path del report
'
'Se non viene creato il Report
'	La funzione ritorna "KO"
'---------------------------------------------------------
function creaRepAssenze(sSess, sAula, nIdClasse, sDtinizio, sDtfine, sDescbando, sDescSessione)

	dim FileAssenze, lPath, sPathFile
	dim sDescAula
	dim sSql, dt_app, dt_dal, dt_Al
'	dim dtIniSess, dtFinSess
		
	creaRepAssenze = "KO"
	
	FileAssenze =   Session("idutente") & "_" & nIdClasse & "_" & sSess &"_Assenze.xls"

	lPath= Server.MapPath("/") & session("progetto") & "/DocPers/Temp/" 

	sPathFile = session("progetto") & "/DocPers/Temp/"	
	
	sSQL="select a.desc_aula from classe_percorso cp,aula a" & _
		 " where a.id_aula=cp.id_aula and cp.id_aula=" & sAula
    'Response.Write sSQL
	
	set rsClasse = server.CreateObject("ADODB.Recordset")
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	rsClasse.Open sSql, CC, 3
    
    if not rsClasse.EOF then
		sDescAula=rsClasse("desc_aula")
	end if	   

	rsClasse.close
	set rsClasse=nothing
	
' A.Orlando  06/07/04  INIZIO
'
' Si imposta una condizione in base alle persone che trovo sulla tabella componenti_classe:
' se la situazione e' normale e non ci sono stati spostamenti di classe a sessione gia' 
' iniziata, si imposta la condizione che c'era gia' in precedenza (DT_RUOLODAL <= DT_INISESS);
' se c'e' qualcuno che ha una dt_ruolodal superiore alla dt_inisess si imposta l'altra 
' condizione (AND DT_RUOLODAL <= DT_FINSESS) in modo che i report siano corretti sempre
' (la condizione serve quando faccio il report per la sessione precedente (es. sessione 1)
' di una classe di destinazione nella cui sessione successiva (es. sessione 2) sono state 
' spostate una o piu' persone)
' 
'Response.Write sDtinizio
'Response.Write sDtfine
'Response.End
	
	sSQL="SELECT ID_PERSONA, DT_RUOLODAL, DT_RUOLOAL FROM COMPONENTI_CLASSE "  & _
		 " WHERE ID_CLASSE= "& nIdClasse & " AND DT_RUOLODAL > '" & sDtinizio & "'" 
    'Response.Write sSQL
	
	set rsSposta = server.CreateObject("ADODB.Recordset")
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	rsSposta.Open sSql, CC, 3
    
    if rsSposta.EOF then
		sCond= " AND CC.DT_RUOLODAL <= CP.DT_INISESS " 
	else
		sCond= " AND CC.DT_RUOLODAL <= CP.DT_FINSESS "
	end if	   

	rsSposta.close
	set rsSposta=nothing
	
' 06/07/04 FINE
'-------------
	
	sSql = " SELECT P.ID_PERSONA,P.COGNOME,CP.DT_INISESS, CP.DT_FINSESS,CC.DT_RUOLOAL, P.NOME, P.DT_NASC,P.COD_FISC" & _ 
			" FROM PERSONA P,CLASSE_PERCORSO CP,COMPONENTI_CLASSE CC " & _ 
			" WHERE CP.COD_SESSIONE='" & sSess & "'" & _ 
			" AND CP.ID_CLASSE =" & nIdClasse & _
			" AND CC.ID_CLASSE = CP.ID_CLASSE " & _
	        " AND CC.ID_PERSONA = P.ID_PERSONA " & _ 
			" AND CC.COD_RUOLO='DI'" & _ 
			" AND (CC.DT_RUOLOAL >= CP.DT_INISESS or CC.DT_RUOLOAL is null) " &_ 
			sCond &_
			" ORDER BY COGNOME,NOME"
	
	'Response.Write sSql
		
	set rs = server.CreateObject("ADODB.Recordset")
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
        rs.Open sSql, CC, 3
		
	If not rs.eof Then
		creaRepAssenze = sPathFile & FileAssenze

		Set fso = CreateObject("Scripting.FileSystemObject")
		Set a = fso.CreateTextFile(lPath & FileAssenze, True)
									
		a.WriteLine "Resoconto ore di assenza - Situazione al " & day(Now) & "/" & month(now) & "/" & year(now) & " Ore " & hour(now) & ":" & minute(now)
		a.WriteLine "Bando: " & sDescbando &  " - Sessione: " & sDescSessione & " - Dal " & ConvDateToString(sDtinizio) & " - Al " & ConvDateToString(sDtFine) &  " - Classe: " & sDescAula 
		a.WriteLine ""

		dt_dal = cdate(sDtinizio)
		dt_Al = cdate(sDtFine)

		dt_app = dt_dal

		testo=  " " & chr(9) &"Cognome" & chr(9) & "Nome" &  chr(9)	& "In classe fino al" & chr(9)
		
		do until dt_app > dt_Al
			testo = testo &  chr(9) & ConvDateToString(dt_app)
			dt_app=dt_app+1
		loop
					
		a.WriteLine	testo			
           
        ID = 1
            
        do until rs.EOF
	        Testo=ID & chr(9) & rs("COGNOME") & chr(9)& rs("NOME") & chr(9) & ConvDateToString(rs("DT_RUOLOAL")) & chr(9)
	             
			Ssql="SELECT NUM_ORE,MINUTI,DT_ASSEN FROM REG_CLASSE" & _
				 " WHERE id_persona = " & rs("ID_PERSONA")&_
				 " AND COD_SESSIONE='" & sSess & "'" &_
				 " AND ID_CLASSE=" & nIdClasse &_
				 " AND DT_ASSEN BETWEEN " & ConvDateToDbs(sDtInizio) & " and " & ConvDateToDbs(sDtFine) &_
				 " ORDER BY DT_ASSEN "
'				 " AND DT_ASSEN BETWEEN " & ConvDateToDBs(sDtInizio)& " and " & ConvDateToDBs(sDtFine) &_

			'Response.Write ssql
			'Response.end
			

			set rsAssen = server.CreateObject("ADODB.Recordset")
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
			rsAssen.Open sSql, CC, 3

			If not rsAssen.eof Then	
				dt_app = dt_dal
				Testo = Testo & chr(9)

				do until rsAssen.EOF
					do until dt_app = rsAssen("DT_ASSEN")
						testo = testo &  chr(9) 
						dt_app=dt_app + 1	
					loop	
					Testo= Testo &  rsAssen("NUM_ORE") & ":" & rsAssen("MINUTI")
					rsAssen.MoveNext
				loop								
			end if
			
			rsAssen.Close
			set rsAssen=nothing	 								
			
			a.WriteLine Testo		
            
            id = id + 1             
			rs.MoveNext
		loop
	           
	    rs.Close
	    set rs=nothing
	        
		a.Close
   end if	    

end function
%>
