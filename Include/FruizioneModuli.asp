<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%
' *************************************************************************
' La seguenti utiliti permettono la verifica della regola di dipendenza per
' la fruibilit� o meno del corso.
' La funzione accett in INPUT due parameri : ID del corso e ID del modulo,
' ritona TRUE e FALSE.
' *************************************************************************
' function function function ControllaFruizioneModulo(long ,long)
' 			verifica della regola di dipendenza per
'			la fruibilit� o meno del corso.

' Return 	se il modulo � fruibile  ritorna true altrimenti false.

' Autore   Enrico Pili 
' Date	   19-12-2002			
' Update   EPI 11-03-2002
' *************************************************************************
  
function ControllaFruizioneModulo(nIdCorso,nIdLezione)
	dim sSQL,rsRegDipen,sDipendenza

	ControllaFruizioneModulo = false
	
	sSQL = "SELECT ELE_PREC, ELE_SUCC FROM REGOLA_DIPENDENZA " & _
			"WHERE ID_CORSO = " & CLng(nIdCorso) & _
			" AND ID_ELEMENTO = " & CLng(nIdLezione)
	set rsRegDipen = Server.CreateObject("ADODB.Recordset")
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	rsRegDipen.Open sSQL, CC, 3
	
	if not rsRegDipen.EOF then
		if rsRegDipen("ELE_PREC") <> "" then
			sModuloPrecedente = clng(rsRegDipen("ELE_PREC"))
			sModuloSuccessivo = rsRegDipen("ELE_SUCC")

			' Se ha un elemento precedente devo controllare 
			' se ha passato il corso.
			
'MDIFICA PER MALFUNZIONAMENTO 11/02/2004 TUGNOLI----------------------------------------------
'			sSQL = "SELECT ELE_PREC, ELE_SUCC, DIPENDENZE, PUNTEGGIO, STATO_LEZIONE, STATO_OBIETTIVO " &_
'					" FROM REGOLA_DIPENDENZA WHERE ID_CORSO = " & CLng(nIdCorso) & _
'					" AND ID_ELEMENTO = " & sModuloPrecedente


			sSQL = "SELECT ELE_PREC, ELE_SUCC, DIPENDENZE, PUNTEGGIO, STATO_LEZIONE, STATO_OBIETTIVO " &_
					" FROM REGOLA_DIPENDENZA WHERE ID_CORSO = " & CLng(nIdCorso) & _
					" AND ID_ELEMENTO = " & CLng(nIdLezione)
'---------------------------------------------------------------------------------------------

			set rsRegDipPrec = Server.CreateObject("ADODB.Recordset")
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
			rsRegDipPrec.Open sSQL, CC, 3
			
			if not rsRegDipPrec.EOF THEN
				sDipendenza	= rsRegDipPrec("DIPENDENZE")
				if sDipendenza = "" then
					sDipendenza = null
				end if
				sPunteggio		= rsRegDipPrec("PUNTEGGIO")
				sStatoLezione	= rsRegDipPrec("STATO_LEZIONE")
				sStatoObbiettivo= rsRegDipPrec("STATO_OBIETTIVO") 
			else
				sDipendenza = null
			end if
			
			rsRegDipPrec.close
			set rsRegDipPrec = nothing
		else
			' Non ha elemento precedente per cui pu� essere effettuato
			sDipendenza = null   
		end if
	else
		' Non esiste l'occorrenza in regole dipendenza.
		' Questa situazione non pu� succedere, ma comunque
		' la controllo ugualemente.
		sDipendenza = null
	end if
	rsRegDipen.Close
	set rsRegDipen = nothing
	
	if IsNull(sDipendenza)then
		ControllaFruizioneModulo = true	
	else
		aRegDip			= GetRegDip(sDipendenza)
		aScore			= Split(sPunteggio,",")
		aSLezione		= Split(sStatoLezione,",")
		aSObbiettivo	= Split(sStatoObbiettivo,",")
		
		'ControllaFruizioneModulo = ControlloFruibilita(aRegDip(UBound(aRegDip)-1), aScore, aSLezione, aSObbiettivo,nIdCorso)
		ControllaFruizioneModulo = ControlloFruibilita(aRegDip(UBound(aRegDip)), aScore, aSLezione, aSObbiettivo,nIdCorso)
	end if
end function

'-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------  

function GetRegDip(sDipendenza)
	dim t,aDipendenza,aCampoValore,aNomeValore()
	aDipendenza = split(sDipendenza,",")
'	nNumEle = (UBound(aDipendenza)+1) * 2
'	ReDim aNomeValore(nNumEle)
'	t = 0
'	for i = 0 to nNumEle-1 step 2 
'		aNomeValore(i)		= Split(aDipendenza(t),"[")(0)
'		aNomeValore(i+1)	= Left(Split(aDipendenza(t),"[")(1),len(Split(aDipendenza(t),"[")(1))-1)
'		t = t + 1 
'	next 	
'	GetRegDip = aNomeValore
	GetRegDip = aDipendenza
end function

'-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------  

function ControllaVincolo(sStrutVincoli,aSeguenzaEle,aScore, aSLezione, aSObbiettivo,nIdCorso)
	dim i,nLung,bPrimo,sRegDip2
	dim sRisult
	
	ControllaVincolo = true
	bPrimo = false
	' *************************************************
	' Costruisco la stringa con tutti gli ID dei moduli 
	' da controllare separati da una virgola
	' **************************************************
	for i = 0 to len(sStrutVincoli)
		if not IsNumeric(mid(sStrutVincoli,i+1,1)) then
			if bPrimo then
				sStrutVincoli2 = sStrutVincoli2 & ","
			end if
			if (i+2) < nLung then
				if not IsNumeric(mid(sStrutVincoli,i+2,1)) then
					bPrimo = false
				end if
			end if
		else
			sStrutVincoli2 = sStrutVincoli2 & mid(sStrutVincoli,i+1,1)
			bPrimo = true
		end if
	next
	' ***********************************************************
	' Elimino il carattere "," dalla fine della stringa costruita
	' ***********************************************************
	if Mid(sStrutVincoli2,len(sStrutVincoli2),1) = "," then
		sStrutVincoli2 = Left(sStrutVincoli2,len(sStrutVincoli2)-1)
	end if
	' ***********************************
	' Array con i moduli da controllare
	' ***********************************
	aStrutVincoli = Split(sStrutVincoli2,",")
	sAppo = sStrutVincoli
	if not IsNumeric(mid(sAppo,1,1)) then
		sAppo = Right(sAppo,len(sAppo)-1)
	end if
	
	for i = 0 to UBound(aStrutVincoli)
		' *******************************************************
		' Cerco l'indice relativo al modulo per i valori relativi 
		' al punteggio, stato della lezione e lo stato obbiettivo
		' *******************************************************
		for t = 0 to UBound(aSeguenzaEle) 
			if aSeguenzaEle(t) = aStrutVincoli(i) then
'				****************************************************************
'				Devo controllare nelle tabelle STUDENT_AGENT, OBJECTIVES_STATUS.
'				****************************************************************
				bPassed = CheckPunteggio(aScore(t),aStrutVincoli(i),nIdCorso)
				if bPassed then
					bPassed = CheckSLezione(aSLezione(t),aStrutVincoli(i),nIdCorso)
				end if
				if bPassed then
					bPassed = CheckSObbiettivo(aSObbiettivo(t),aStrutVincoli(i),nIdCorso)
				end if
			'	***************************************************
				' Mi serve per capire qual'� l'ultimo questionario
				' trattato per poterlo azzerare nella lista.
				nFound=t 
			'	***************************************************
			end if 
		next
		aSeguenzaEle(nFound) = "0"
		' *********************
		' Assegno esito esito :
		' N -> non passato .
		' P -> passato.
		' *********************
		if not bPassed then
			sAppo = Replace(sAppo,aStrutVincoli(i),"N")
		else
			sAppo = Replace(sAppo,aStrutVincoli(i),"P")
		end if
	next

	nPosErrore = InStr(1,sAppo,"N")
	select case nPosErrore
		case "0"	' Se non ci sono N vuol dire che il vincolo � passato ...
			ControllaVincolo = true
		case else	' ... altrimenti controllo i vari OR[|] oppure AND[&].
			nLun = Len(sAppo)
			sRisult = sAppo
			for i = 0 to nLun
				sValore1 = Mid(sAppo,i+1,1)
				if (i+2) < nLun then
					sOperatore	= Mid(sAppo,i+2,1)
					sValore2	= Mid(sAppo,i+3,1)
					select case sOperatore
						case "&"
							if sValore1 = "N" or sValore2 = "N" then 
								' Se in caso di AND di valori cesiste un N 
								' non � verificato il vincolo.
								sRisult = Replace(sRisult,sValore1 &_
									 sOperatore & sValore2,"N",i+1,3)
							else
								sRisult = Replace(sRisult,sValore1 &_
									 sOperatore & sValore2,"P",i+1,3)
							end if
						case "|"
							if sValore1 = "P" or sValore2 = "P" then
								' Se in caso di | di valori cesiste un P 
								' � verificato il vincolo.
								sRisult = Replace(sRisult,sValore1 &_
									 sOperatore & sValore2,"P",1,3)
							else
								sRisult = Replace(sRisult,sValore1 &_
									 sOperatore & sValore2,"N",1,3)
							end if
					end select 
				end if 
			next
			if sRisult = "P" then
				ControllaVincolo = true
			else
				ControllaVincolo = false
			end if	
	end select
end function

'-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------  

function ControlloFruibilita(sRegDip, aScore, aSLezione, aSObbiettivo,nIdCorso)
	dim sVincolo
	dim sRisult
	dim sAppo
	dim aSeguenzaEle
	
	sFruibile = false	
	sRisult = sRegDip
	aSeguenzaEle = SeguenzaModuli(sRegDip) 
	
	do while InStr(sRegDip,"(") > 0 
		' ********************************************************
		' Prendo un vincolo selezionandolo il primo fra parentesi 
		' (se esiste) oppure tutto il vincolo completo.
		' ********************************************************
		sVincolo = GetFunzione(sRegDip)
		' ********************************************************
 		if ControllaVincolo(sVincolo,aSeguenzaEle,aScore, aSLezione, aSObbiettivo,nIdCorso) then
			sVerifica = "P"
		else
			sVerifica = "N"
		end if
		if not IsNumeric(Mid(sVincolo,1,1)) then
			oldVincolo = sVincolo
			for i = 0 to len(sRisult)
				if Mid(sRisult,i+1,2) = sVincolo then
					if i > 0 then
						if not IsNumeric(Mid(sRisult,i,1)) then
							oldVincolo = sVincolo
							sVincolo = Mid(sRisult,i,1) & sVincolo 
							sRisult = Replace(sRisult, sVincolo, Mid(sVincolo,1,2) & sVerifica)
						end if			
					end if
				end if
			next 
			sVincolo = oldVincolo
		else
			sRisult= Replace(sRisult,"(" & sVincolo & ")",sVerifica)
		end if
		nPosizioneSost = InStr(sRegDip,"(" & sVincolo & ")")
		if nPosizioneSost > 1 then
			sSostituzione = Mid(sRegDip,nPosizioneSost-1,1)
		else
			sSostituzione = Mid(sRegDip,nPosizioneSost,1)
		end if
		if sSostituzione = "&" or sSostituzione = "|" then
			sRegDip = Replace(sRegDip,sSostituzione & "(" & sVincolo & ")","")
		else
			sRegDip = Replace(sRegDip,"(" & sVincolo & ")","")
		end if
		sRegDip = Replace(sRegDip,"()","")
	loop
	' ***********************************************************
	' Se l'ultimo carattere di � "sRegDip" numerico vuol dire che 
	' esiste ancora un vincolo da verificare.
	' ***********************************************************
	if IsNumeric(Right(sRegDip,1)) then
		if ControllaVincolo(sRegDip,aSeguenzaEle,aScore, aSLezione, aSObbiettivo,nIdCorso) then
			sVerifica = "P"
		else
			sVerifica = "N"
		end if
		if not IsNumeric(Mid(sRegDip,1,1)) then
			sRisult= Replace(sRisult,sRegDip,Mid(sRegDip,1,1) & sVerifica)
		else
			sRisult= Replace(sRisult,"(" & sRegDip & ")",sVerifica)
		end if
		sRegDip = Replace(sRegDip,"(" & sRegDip & ")","")
	end if
	
	sRisult = Replace(sRisult,"N","0")
	sRisult = Replace(sRisult,"P","1")
	sRisult = Replace(sRisult,"&"," and ")
	sRisult = Replace(sRisult,"|"," or ")
	If IsNumeric(sRisult) and sVerifica = "N" then
	'si verifica solo se la regola di dipendenza consiste di un solo elemento
		ControlloFruibilita = false
		exit function
	End If
	if Eval(sRisult) = 0 then
		ControlloFruibilita = false
	else
		ControlloFruibilita = true
	end if	
end function

'-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------  

function GetFunzione(sStrutVincoli)
	dim i,nParTonde, nPosUlTonda, nPosParChiusa, sOperatore,nLung
	nLung = len(sStrutVincoli)
	
	for i = 0 to nLung
		if Mid(sStrutVincoli,i+1,1) = "(" then 
			nParTonde = nParTonde + 1 
			nPosUlTonda = i+1
		end if
	next 
'	******************************************
'	nPosParChiusa = InStr(1,sStrutVincoli,")")
'	******************************************
'	Modificato 18/12/2002
'	******************************************
	for i = 0 to nLung
		if Mid(sStrutVincoli,i+1,1) = ")" then 
			nParTonde = nParTonde + 1 
			nPosParChiusa = i+1
			if nPosUlTonda < nPosParChiusa then 
				' ******************************
				' Esco perch� � la parentesi 
				' chiua riferita a quella aperta
				' *******************************
				exit for
			end if
		end if
	next 
	sOperatore = Mid(sStrutVincoli,nPosUlTonda+1,nPosParChiusa-(nPosUlTonda+1))
	GetFunzione = sOperatore

end function

'-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------  

function SeguenzaModuli(sRegDip)
	dim aSeguenzaEle
	' *****************************************	
	' Crea la seguenza dei moduli da controlare 
	' *****************************************	
	nLung = len(sRegDip)
	for i = 0 to nLung
		if not IsNumeric(mid(sRegDip,i+1,1)) then
			if bPrimo then
				sRegDip2 = sRegDip2 & ","
			end if
			if (i+2) < nLung then
				if not IsNumeric(mid(sRegDip,i+2,1)) then
					bPrimo = false
				end if
			end if
		else
			sRegDip2 = sRegDip2 & mid(sRegDip,i+1,1)
			bPrimo = true
		end if
	next
	if mid(sRegDip2,len(sRegDip2),1) = "," then
		sRegDip2 = Left(sRegDip2,len(sRegDip2)-1)
	end if
	aSeguenzaEle = Split(sRegDip2,",")
	' *****************************************	
	SeguenzaModuli = aSeguenzaEle

end function
' ------------------------------------------------------------------------------------
' Controlli relativi al superamento dei vincoli.
' ------------------------------------------------------------------------------------
function CheckPunteggio(nScore,nIdLezione,nIdCorso)
	if nScore <> "#" then
		sSQL = "SELECT TRY_SCORE FROM STUDENT_DATA WHERE STUDENT_ID = '" &_
			Session("idutente") & "' AND COURSE_ID='" & nIdCorso & "'" &_
			" AND LESSON_ID = '" & nIdLezione & "'"

'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
		set rsScore = CC.Execute(sSQL)
		if not rsScore.EOF then
			if rsScore("TRY_SCORE") <> "" then
				nTScore = clng(rsScore("TRY_SCORE"))
			else
				nTScore = 0 
			end if
		else
			nTScore = 0 
		end if
		rsScore.Close
		set rsScore = nothing 
		if cint(nTScore) >= cint(nScore) then
			CheckPunteggio = true
		else
			CheckPunteggio = false
		end if
	else
		CheckPunteggio = true
	end if
end function

'-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------  

function CheckSLezione(sSLezione,nIdLezione,nIdCorso)
	if sSLezione <> "#" then
		snIdCorso = "C" & nIdCorso
		snIdElemento = "E" & nIdLezione
'temporaneamente non utilizzata per Sport2job bisogna ragionare su quali controlli fare
'		sSQL = "SELECT LESSON_STATUS FROM STUDENT_DATA WHERE STUDENT_ID = '" &_
'			Session("idutente") & "' AND COURSE_ID='" & snIdCorso & "'" &_
'			" AND LESSON_ID = '" & snIdElemento & "'"
		sSQL = "SELECT LESSON_STATUS FROM CORE WHERE STUDENT_ID = '" &_
			Session("idutente") & "' AND COURSE_ID='" & snIdCorso & "'" &_
			" AND LESSON_ID = '" & snIdElemento & "'"

'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
		set rsLStatus = CC.Execute(sSQL)
		if not rsLStatus.EOF then
			sStatus = rsLStatus("LESSON_STATUS")
		else
'			set rsCStatus = CC.Execute(sSQLCore)
'			if not rsCStatus.EOF then
'				sStatus = rsCStatus("LESSON_STATUS")
'			end if
			
			sStatus = 0 
		end if
		rsLStatus.Close
		set rsLStatus = nothing
		
		if Ucase(sStatus) = Ucase(sSLezione) then
			CheckSLezione = true
		else
			CheckSLezione = false
		end if
	else
		CheckSLezione = true
	end if
end function

'-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------  

function CheckSObbiettivo(sSObbiettivo,nIdLezione,nIdCorso)

	if sSObbiettivo <> "#" then
		sSQL = "SELECT J_STATUS FROM OBJECTIVES_STATUS WHERE STUDENT_ID = '" &_
			Session("idutente") & "' AND COURSE_ID='" & nIdCorso & "'" &_
			" AND LESSON_ID = '" & nIdLezione & "'"
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
		set rsSObbiettivo = CC.Execute(sSQL)
		if not rsSObbiettivo.EOF then
			sStatus = rsSObbiettivo("J_STATUS")
		else
			sStatus = 0 
		end if
		rsSObbiettivo.Close
		set rsLStatus = nothing 
		if sStatus = sSObbiettivo then
			CheckSObbiettivo = true
		else
			CheckSObbiettivo = false
		end if
	else
		CheckSObbiettivo = true
	end if
end function
' ------------------------------------------------------------------------------------
' ------------------------------------------------------------------------------------
' *************************************************************************
' la funzione ritorna una stringa contenente la sequenza dei moduli del corso
' Parametri input :
' aInpMod =
'   - il codice del modulo precedente
'   - il codice del modulo successivo
'   - il codice del modulo corrente
'
' numEle = numero di elementi dell'array
' *************************************************************************
' function function function ControllaFruizioneModulo(long ,long)
' 			verifica della regola di dipendenza per
'			la fruibilit� o meno del corso.

' Return 	se il modulo � fruibile  ritorna true altrimenti false.

' Autore   Quirox, Pili 
' Date	   04/02/2003			
' Update   04/02/2003
' *************************************************************************


Function ordinaModuli(ainpmod, numEle)
    Dim iNext       ' memorizza il codice del modulo successivo
    Dim iEle        ' contatore del numero di elementi dell'array verificati
    Dim iMod        ' memorizza la posizione del modulo successivo
    Dim strSort     ' stringa ritornata dalla funzione
    Dim aChk()

    iEle = 0
    iNext = 0
    iMod = 0
    ReDim aChk(numEle)
    
    Do While iEle < numEle
        If iNext = 0 Then
           ' elemento iniziale con precedente <> 0
            If iEle = 0 And ainpmod(iNext, 0) <> 0 Then
                ordinaModuli = ordineCronologico("F", numEle)
                Exit Function
            End If
            
            ' se iNext = 0 e (iEle > 0 e iMod = 0) vuol dire che nella sequenza ci
            ' sono moduli con successivo = 0 e successivo <> 0
            If iEle > 0 And iMod = 0 Then
                ordinaModuli = ordineCronologico("Z", numEle)
                Exit Function
            End If
            
            ' memorizzo la sequenza dei moduli
            strSort = strSort & CStr(iMod) & "|"
            ' se l'elemento di aChk puntato da iMod � vuoto la sequenza � corretta...
			
            If aChk(iMod) = 0 Then
                aChk(iMod) = iMod
            Else
            ' ... altrimenti vuol dire che esistono pi� moduli che hanno moduli
            ' successivi uguali
                ordinaModuli = ordineCronologico("M", numEle)
                Exit Function
            End If
            
            ' acquisisco il codice del modulo successivo
            iNext = ainpmod(iMod, 1)
            iEle = iEle + 1
            
            ' elemento finale con successivo <> 0
            If iEle = numEle Then
                If ainpmod(iMod, 1) <> 99999 Then
                    ordinaModuli = ordineCronologico("L", numEle)
                    Exit Function
                End If
            End If

            iMod = 0
        Else
            iMod = findNext(iNext, ainpmod, numEle)
            ' se il modulo successivo � il primo della lista, la sequenza ha dei
            ' riferimenti circolari
            
            If iMod = 0 And iNext <> 0 or iMod = -1 Then
                ordinaModuli = ordineCronologico("R", numEle)
                Exit Function
            End If
            iNext = 0
        End If
    Loop
    
    ordinaModuli = strSort

End Function

'-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------  

Private Function findNext(codEle, ainpmod, numEle)
    Dim iEle
    
    findNext = -1
    For iEle = 0 To numEle - 1
        If ainpmod(iEle, 2) = codEle Then
            findNext = iEle
            Exit Function
        End If
    Next 
    
End Function

'-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------  
' funzione che ritorna la sequenza dei moduli cos� come viene passata in input
' alla funzione di ordinaModuli
' questa funzione viene richiamata quando la sequenza dei moduli non � corretta
' Parametri =
' codErr : codice errore indicante quale errore si � verificato
'           M = sequenza con moduli successivi ripetuti
'           F = sequenza senza moduli aventi modulo precedente = 0 (manca il primo)
'           L = sequenza senza moduli aventi modulo successivo = 0 (manca l'ultimo)
'           Z = sequenza con moduli aventi modulo successivo = 0 e modulo succ <> 0
'
' numEle : numero di elementi dell'array
'-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------  

Private Function ordineCronologico(codErr, numEle)
    Dim i
    Dim strCron
    
    strCron = "#" & codErr & "#"
    For i = 0 To numEle - 1
        strCron = strCron & CStr(i) & "|"
    Next 
    
    ordineCronologico = strCron
    
End Function
'-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------  
' Passando l'array contenente i moduli e la seguenza degli stessi,
' ritorna l'elenco dei moduli ordinati.
'-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------  
Function Sort(aElem,sSequence)
	dim aSort()
	
	ReDim aSort( ubound(aElem,1), ubound(aElem,2) )
	aSequence = Split(sSequence,"|")

	for i = 0 to ubound(aElem,2)
		for z = 0 to ubound(aElem,1)
			aSort(z,i) = aElem(z, aSequence(i))
		next
	next
	Sort = aSort

End Function

%>
