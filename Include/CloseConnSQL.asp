<%
'-----------------------------------------------
' CIerra las conexiones a las bases de SQL
'-----------------------------------------------

'Conexion a EmpleoLavoro
if connLavoro.State=1 then
	connLavoro.Close
	Set connLavoro = NoThing
end if

'Conexion a EmpleoPersonas (OnLine)
if ConnEmpleoPersonas.State=1 then
	ConnEmpleoPersonas.Close
	Set ConnEmpleoPersonas = NoThing
end if

'Conexion a EmpleoPersonas (Replica)
if ConnEmpleoPersonasReplica.State=1 then
	ConnEmpleoPersonasReplica.Close
	Set ConnEmpleoPersonasReplica = NoThing
end if

'Conexion a Seguridad
if gConnSeguridad.State=1 then
	gConnSeguridad.Close
	Set gConnSeguridad = NoThing
end if

'Fin-----------------------------------------------
%>