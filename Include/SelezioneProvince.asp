<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%
'---------------------------------------------------------------------
'Questa funzione permette di reperire la sigla della provincia o
'delle province relative alla regione di appartenenza di un 
'operatore,ognuna delle quali tra apici e separate da una virgola.
'ritorna stringa vuota(es. '') nel caso in cui non ci sia una 
'provincia valida oppure venga passato un valore non valido. 
'I valori che possono essere passati sono:
'  2  per la provincia singola
'  3  per le province di una regione
'---------------------------------------------------------------------

function SetProvince(iVal)
    dim sNomiprov
    
    sSQL="SELECT PRV FROM SEDE_IMPRESA WHERE ID_SEDE=" & SESSION("CREATOR")
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
    set rsSetProv =cc.execute(sSQL)
    if not rsSetProv.eof then
        nomeProv = rsSetProv("PRV")
        nomeProvDef = "'" & nomeProv & "'" 
		select case (iVal)
			case(2)
			    SetProvince = nomeProvDef 
			
			case(3) 
			    sql1="SELECT VALORE FROM TADES WHERE NOME_TABELLA='PROV' AND CODICE=" & nomeProvDef &_
			         "AND ISA='0' AND " & ConvDateToDB(date) & " between Decorrenza AND Scadenza"  
	           
'PL-SQL * T-SQL  
SQL1 = TransformPLSQLToTSQL (SQL1) 
	            set rsSelezione =cc.execute(SQL1)
	            if not rsSelezione.eof then
	                sValore = rsSelezione("VALORE")
	                sql2="SELECT CODICE FROM TADES WHERE NOME_TABELLA='PROV' AND VALORE='" & sValore & "'" &_
	                     " AND ISA='0' AND " & ConvDateToDB(date) & " between Decorrenza AND Scadenza" 
	               
'PL-SQL * T-SQL  
SQL2 = TransformPLSQLToTSQL (SQL2) 
	                set rsSelezione =cc.execute(SQL2)
	                cont = 0
	                do while not rsSelezione.eof 
	                    if cont = 0 then
			              sNomiProv = "'" & rsSelezione("CODICE") & "'"
			              cont = 1
			            else
			              sNomiProv = sNomiprov & ",'" & rsSelezione("CODICE") & "'"  
			            end if
			            rsSelezione.movenext
			        loop   
	            else
	                sNomiProv = "''" 
	            end if 
	            set rsSelezione = nothing
	            SetProvince = sNomiProv 
			
			case else
			    SetProvince="''"    
		end select 
	else
	    SetProvince="''"	
    end if 
     set rsSetProv= nothing     
end function
%>
