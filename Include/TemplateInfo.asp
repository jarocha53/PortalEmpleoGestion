<% 
'Setto la variabile salign di default a right se non 
'espressamente richiesto nella info che la richiama
If sAlignTitolo = "" Then
	sAlignTitolo = "right"
End If
%>
<LINK REL=STYLESHEET TYPE="text/css" HREF='<%=session("progetto")%>/fogliostile.css'>
<div align="center">
<TABLE border="0" width="100%" cellpadding="0" cellspacing="0">
	<tr>
		<td height="10"></td>	
	</tr>
	<tr>
		<td valign="top">
			<center>
			<TABLE border="0" width="100%" cellspacing="0" valign="top" cellpadding="0">
				<tr>
					<td width="100%" height="25" align="right" valign="top" >
						<TABLE border='0' <%=sStruttable%> width='100%' height='10' cellspacing='0' cellpadding='0' align="center">
							<tr>
								<td width="100%" valign="top" align="<%=sAlignTitolo%>"><b class="tbltext1"><%=stitoloinfo%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></td>
							</tr>
						</TABLE>
					</td>
				</tr>
			</TABLE>
			</center>
  
			<TABLE border="0" width="100%" cellspacing="0" cellpadding="0" height="81">
				<tr>
					<td width="100%"><center>
						<TABLE border="0" width="100%" cellspacing="0" cellpadding="0">
							<tr>
								<td width="100%">
								<%
									on error resume next
									Server.Execute(sPathFileEdit)
								%>
								<!-- #include virtual='/include/MsgErrInc.asp' -->
								</td>
							</tr>
						</TABLE></center>
					</td>
				</tr>
			</TABLE>
		</td>
	</tr>
</TABLE>
</div>
