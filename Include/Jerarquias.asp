<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%
Dim RsJerarquias 
Dim SqlJerarquias

Set RsJerarquias = server.CreateObject("adodb.recordset")



Function ObtenerServiciosPorRol(RolEmpresa,Nombre,Estilo,Validacion,Seleccion)
''''OBTENGO LOS POSIBLES SERVICIOS SEGUN EL ROL DE LA EMPRESA.
SqlJerarquias = ""
SqlJerarquias = SqlJerarquias & "select Servicio, Descripcion from impresa_rolesserviciostipos where rol = " & RolEmpresa 

'PL-SQL * T-SQL  
SQLJERARQUIAS = TransformPLSQLToTSQL (SQLJERARQUIAS) 
Set RsJerarquias = cc.execute(SqlJerarquias)

if not RsJerarquias.EOF then
	ObtenerServiciosPorRol = ObtenerServiciosPorRol & "<Select name='" & Nombre & "' class='" & Estilo & "' onchange='" & Validacion & "'>"
	Do while not RsJerarquias.EOF
		if cstr(RsJerarquias("Servicio")) = Seleccion then
			Seleccionado = " selected "
		else
			Seleccionado = "" 
		end if 
		
		ObtenerServiciosPorRol = ObtenerServiciosPorRol & "<Option value='" & RsJerarquias("Servicio") & "'" & Seleccionado & ">" & RsJerarquias("Descripcion") & "</Option>"
		RsJerarquias.MoveNext 
	Loop
	ObtenerServiciosPorRol = ObtenerServiciosPorRol & "</Select>"
end if
	
End function



Function ObtenerAreaGrupoPorEmpresa(RolEmpresa,ServicioEmpresa)
''''OBTENGO LAS POSIBLES AREAS SEGUN EL ROL DE LA EMPRESA.
SqlJerarquias = ""
SqlJerarquias = SqlJerarquias & "select ag.AreaGrupo, ao.Descripcion TablaEntrada from impresa_rolesserviciostipos irst inner join area_grupos ag on irst.areagrupo = ag.areagrupo "
SqlJerarquias = SqlJerarquias & " inner join area_origen ao on ag.areaorigen = ao.areaorigen "
SqlJerarquias = SqlJerarquias & " where rol = " & RolEmpresa & " and Servicio = " & ServicioEmpresa

'PL-SQL * T-SQL  
SQLJERARQUIAS = TransformPLSQLToTSQL (SQLJERARQUIAS) 
Set RsJerarquias = cc.execute(SqlJerarquias)

if not RsJerarquias.EOF then
	ObtenerAreaGrupoPorEmpresa = "AREAGRUPO|" & RsJerarquias("AreaGrupo") & "|TABLAENTRADA|" & RsJerarquias("TablaEntrada") 
end if

End function



Set RsJerarquias = nothing
%>


