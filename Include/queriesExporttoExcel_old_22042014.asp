<!-- #include virtual="/include/Base64.asp" -->
<%

	function generateQueryCurrentDealsExporttoExcel(reportFilterDataBase64)

		sSQLCurrentDeals = ""
		sSQLCurrentDeals = "SELECT richiesta_sede.id_richiesta AS Referencia," &_ 
					"im.RAG_SOC+' ('+SEDE_IMPRESA.DESCRIZIONE+')' as Empleador," &_ 
					"richiesta_sede.offer_title as 'T�tulo de la Oferta'," &_
					"f.denominazione as 'Cargo Equivalente', a.denominazione as 'Grupo Ocupacional', " &_
					"richiesta_sede.nro_vacantes as 'Numero de Vacantes', " &_
					"(SELECT COUNT(RICHIESTA_CANDIDATO_HIST.ID_RICHIESTA) AS NUMERO " &_
						"FROM RICHIESTA_CANDIDATO_HIST " &_
						"WHERE RICHIESTA_CANDIDATO_HIST.COD_ESITO_SEL IN (1,14) " &_
						"AND RICHIESTA_CANDIDATO_HIST.ID_RICHIESTA = richiesta_sede.id_richiesta " &_
						"AND ( " &_
							"SELECT top 1 rch.DT_INS " &_
							"FROM RICHIESTA_CANDIDATO_HIST rch " &_
							"WHERE rch.ID_RICHIESTA = RICHIESTA_CANDIDATO_HIST.ID_RICHIESTA " &_
										"AND rch.ID_PERSONA = RICHIESTA_CANDIDATO_HIST.ID_PERSONA " &_
							"ORDER BY rch.DT_INS DESC " &_
						") = RICHIESTA_CANDIDATO_HIST.DT_INS " &_
					") AS 'Num Postulantes', " &_
					"(SELECT count(DISTINCT(RICHIESTA_CANDIDATO_HIST.ID_PERSONA)) " &_
						"FROM RICHIESTA_CANDIDATO_HIST " &_
						"WHERE RICHIESTA_CANDIDATO_HIST.COD_ESITO_SEL IN (1,14) " &_
						"AND RICHIESTA_CANDIDATO_HIST.ID_RICHIESTA = richiesta_sede.id_richiesta) 'Num Postulantes old'," &_
					" sim.DESCRIZIONE as 'Centro de Atenci�n', " &_
					" sim.ID_SEDE as 'Id Centro de Atenci�n', " &_
					"convert(varchar(25),richiesta_sede.DT_FINPUBLICACION, 103) as 'Fecha Vencimiento', " &_
					"convert(varchar(25),richiesta_sede.DT_INGRESO, 103) as 'Fecha Inicio', " &_
					"(SELECT COUNT(RICHIESTA_CANDIDATO_HIST.COD_ESITO_SEL) AS NUMERO " &_
						"FROM RICHIESTA_CANDIDATO_HIST " &_
						"WHERE RICHIESTA_CANDIDATO_HIST.COD_ESITO_SEL IN (1,14) " &_
								"AND RICHIESTA_CANDIDATO_HIST.COD_ESITO_SEL = 1 " &_
						"AND RICHIESTA_CANDIDATO_HIST.ID_RICHIESTA = richiesta_sede.id_richiesta  " &_
						"AND ( " &_
							"SELECT top 1 rch.DT_INS " &_
							"FROM RICHIESTA_CANDIDATO_HIST rch " &_
							"WHERE rch.ID_RICHIESTA = RICHIESTA_CANDIDATO_HIST.ID_RICHIESTA " &_
										"AND rch.ID_PERSONA = RICHIESTA_CANDIDATO_HIST.ID_PERSONA " &_
							"ORDER BY rch.DT_INS DESC " &_
						") = RICHIESTA_CANDIDATO_HIST.DT_INS " &_
						"GROUP BY CAST(RICHIESTA_CANDIDATO_HIST.COD_ESITO_SEL AS int) " &_
					") AS TOTAL_COD_UNO, " &_
					"(SELECT COUNT(RICHIESTA_CANDIDATO_HIST.COD_ESITO_SEL) AS NUMERO " &_
						"FROM RICHIESTA_CANDIDATO_HIST " &_
						"WHERE RICHIESTA_CANDIDATO_HIST.COD_ESITO_SEL IN (1,14) " &_
								"AND RICHIESTA_CANDIDATO_HIST.COD_ESITO_SEL = 14 " &_
						"AND RICHIESTA_CANDIDATO_HIST.ID_RICHIESTA = richiesta_sede.id_richiesta  " &_
						"AND ( " &_
							"SELECT top 1 rch.DT_INS " &_
							"FROM RICHIESTA_CANDIDATO_HIST rch " &_
							"WHERE rch.ID_RICHIESTA = RICHIESTA_CANDIDATO_HIST.ID_RICHIESTA " &_
										"AND rch.ID_PERSONA = RICHIESTA_CANDIDATO_HIST.ID_PERSONA " &_
							"ORDER BY rch.DT_INS DESC " &_
						") = RICHIESTA_CANDIDATO_HIST.DT_INS " &_
						"GROUP BY CAST(RICHIESTA_CANDIDATO_HIST.COD_ESITO_SEL AS int) " &_
					") AS TOTAL_COD_CATORCE " &_
					"FROM SEDE_IMPRESA " &_
					"inner join IMPRESA im on im.id_impresa = SEDE_IMPRESA.id_impresa " &_
					"inner join richiesta_sede on richiesta_sede.id_sede = SEDE_IMPRESA.id_sede " &_
					"inner join FIGUREPROFESSIONALI f on richiesta_sede.id_figprof = f.id_figprof " &_
					"inner join impresa_rolesserviciostipos irs ON richiesta_sede.servicio = irs.servicio " &_
					"inner join AREE_PROFESSIONALI a on a.id_areaprof = f.id_areaprof " &_
					"inner join SEDE_IMPRESA sim on sim.id_sede = richiesta_sede.COD_OFICINA " &_
					"WHERE	richiesta_sede.DT_finpublicacion > = convert(datetime,CONVERT(varchar(10), GETDATE(), 103),103) AND richiesta_sede.FL_PUBBLICATO = 'S' " &_
						"AND richiesta_sede.cod_tipo_richiesta = 1 "

		
		reportFilterDataSplit = Split(Base64Decode(reportFilterDataBase64),",")
		
		for i=0 to ubound(reportFilterDataSplit)	  			
	  		
	  		reportFilterData = Split(reportFilterDataSplit(i),":")
	  		columnName = reportFilterData(0)  
	  		valueColumn = reportFilterData(1)
	  		
			if valueColumn <> "" then
				if columnName = "empleador" then					
					sSQLCurrentDeals = sSQLCurrentDeals & " AND im.RAG_SOC LIKE '%" & valueColumn & "%' "
				end if
				if columnName = "titulo_oferta" then					
					sSQLCurrentDeals = sSQLCurrentDeals & " AND richiesta_sede.offer_title LIKE '%" & valueColumn & "%' "
				end if
				if columnName = "centro_atencion" then					
					sSQLCurrentDeals = sSQLCurrentDeals & " AND richiesta_sede.COD_OFICINA = " & valueColumn
				end if
				if columnName = "fecha_publicacion" then
					sSQLCurrentDeals = sSQLCurrentDeals & " AND convert(datetime,convert(varchar(25),richiesta_sede.DT_INGRESO, 103), 103) = convert(datetime,convert(varchar(25),'" & valueColumn & " 00:00:00', 103), 103) " 
				end if
				if columnName = "fecha_vencimiento_from" then	
					fecha_vencimiento_toSplit = Split(reportFilterDataSplit(i+1),":")
			  		fecha_vencimiento_to = fecha_vencimiento_toSplit(1)

					sSQLCurrentDeals = sSQLCurrentDeals & " AND convert(datetime,richiesta_sede.DT_FINPUBLICACION, 103) BETWEEN convert(datetime,convert(varchar(25),'" & valueColumn & " 00:00:00', 103), 103) AND convert(datetime,convert(varchar(25),'" & fecha_vencimiento_to & " 00:00:00', 103),103) "
				end if
				if columnName = "nroVacantesVsPostulantes" then
					if valueColumn = "Mayor" then
						sSQLCurrentDeals = sSQLCurrentDeals & " AND richiesta_sede.nro_vacantes > (SELECT count(DISTINCT(RICHIESTA_CANDIDATO_HIST.ID_PERSONA)) FROM RICHIESTA_CANDIDATO_HIST WHERE RICHIESTA_CANDIDATO_HIST.COD_ESITO_SEL IN (1,14) AND RICHIESTA_CANDIDATO_HIST.ID_RICHIESTA = richiesta_sede.id_richiesta)"
					end if
					if valueColumn = "Menor" then
						sSQLCurrentDeals = sSQLCurrentDeals & " AND richiesta_sede.nro_vacantes < (SELECT count(DISTINCT(RICHIESTA_CANDIDATO_HIST.ID_PERSONA)) FROM RICHIESTA_CANDIDATO_HIST WHERE RICHIESTA_CANDIDATO_HIST.COD_ESITO_SEL IN (1,14) AND RICHIESTA_CANDIDATO_HIST.ID_RICHIESTA = richiesta_sede.id_richiesta)"
					end if
					if valueColumn = "Igual" then
						sSQLCurrentDeals = sSQLCurrentDeals & " AND richiesta_sede.nro_vacantes = (SELECT count(DISTINCT(RICHIESTA_CANDIDATO_HIST.ID_PERSONA)) FROM RICHIESTA_CANDIDATO_HIST WHERE RICHIESTA_CANDIDATO_HIST.COD_ESITO_SEL IN (1,14) AND RICHIESTA_CANDIDATO_HIST.ID_RICHIESTA = richiesta_sede.id_richiesta)"
					end if
				end if
			end if
	  		
		next
		sSQLCurrentDeals = sSQLCurrentDeals & " Order by richiesta_sede.DT_FINPUBLICACION asc"

		generateQueryCurrentDealsExporttoExcel = sSQLCurrentDeals 
	end function


	function generateQueryExpiredDealsExporttoExcel(reportFilterDataBase64)

		sSQLExpiredDeals = ""
		sSQLExpiredDeals = "SELECT richiesta_sede.id_richiesta AS Referencia," &_ 
					"im.RAG_SOC+' ('+SEDE_IMPRESA.DESCRIZIONE+')' as Empleador," &_ 
					"richiesta_sede.offer_title as 'T�tulo de la Oferta'," &_
					"f.denominazione as 'Cargo Equivalente', a.denominazione as 'Grupo Ocupacional', " &_
					"richiesta_sede.nro_vacantes as 'Numero de Vacantes', " &_
					"(SELECT COUNT(RICHIESTA_CANDIDATO_HIST.ID_RICHIESTA) AS NUMERO " &_
						"FROM RICHIESTA_CANDIDATO_HIST " &_
						"WHERE RICHIESTA_CANDIDATO_HIST.COD_ESITO_SEL IN (1,14) " &_
						"AND RICHIESTA_CANDIDATO_HIST.ID_RICHIESTA = richiesta_sede.id_richiesta " &_
						"AND ( " &_
							"SELECT top 1 rch.DT_INS " &_
							"FROM RICHIESTA_CANDIDATO_HIST rch " &_
							"WHERE rch.ID_RICHIESTA = RICHIESTA_CANDIDATO_HIST.ID_RICHIESTA " &_
										"AND rch.ID_PERSONA = RICHIESTA_CANDIDATO_HIST.ID_PERSONA " &_
							"ORDER BY rch.DT_INS DESC " &_
						") = RICHIESTA_CANDIDATO_HIST.DT_INS " &_
					") AS 'Num Postulantes', " &_
					"(SELECT count(DISTINCT(RICHIESTA_CANDIDATO_HIST.ID_PERSONA)) " &_
						"FROM RICHIESTA_CANDIDATO_HIST " &_
						"WHERE RICHIESTA_CANDIDATO_HIST.COD_ESITO_SEL IN (1,14) " &_
						"AND RICHIESTA_CANDIDATO_HIST.ID_RICHIESTA = richiesta_sede.id_richiesta) 'Num Postulantes old'," &_
					" sim.DESCRIZIONE as 'Centro de Atenci�n', " &_
					" sim.ID_SEDE as 'Id Centro de Atenci�n', " &_
					"convert(varchar(25),richiesta_sede.DT_FINPUBLICACION, 103) as 'Fecha Vencimiento', " &_
					"convert(varchar(25),richiesta_sede.DT_INGRESO, 103) as 'Fecha Inicio', " &_
					"(SELECT COUNT(RICHIESTA_CANDIDATO_HIST.COD_ESITO_SEL) AS NUMERO " &_
						"FROM RICHIESTA_CANDIDATO_HIST " &_
						"WHERE RICHIESTA_CANDIDATO_HIST.COD_ESITO_SEL IN (1,14) " &_
								"AND RICHIESTA_CANDIDATO_HIST.COD_ESITO_SEL = 1 " &_
						"AND RICHIESTA_CANDIDATO_HIST.ID_RICHIESTA = richiesta_sede.id_richiesta  " &_
						"AND ( " &_
							"SELECT top 1 rch.DT_INS " &_
							"FROM RICHIESTA_CANDIDATO_HIST rch " &_
							"WHERE rch.ID_RICHIESTA = RICHIESTA_CANDIDATO_HIST.ID_RICHIESTA " &_
										"AND rch.ID_PERSONA = RICHIESTA_CANDIDATO_HIST.ID_PERSONA " &_
							"ORDER BY rch.DT_INS DESC " &_
						") = RICHIESTA_CANDIDATO_HIST.DT_INS " &_
						"GROUP BY CAST(RICHIESTA_CANDIDATO_HIST.COD_ESITO_SEL AS int) " &_
					") AS TOTAL_COD_UNO, " &_
					"(SELECT COUNT(RICHIESTA_CANDIDATO_HIST.COD_ESITO_SEL) AS NUMERO " &_
						"FROM RICHIESTA_CANDIDATO_HIST " &_
						"WHERE RICHIESTA_CANDIDATO_HIST.COD_ESITO_SEL IN (1,14) " &_
								"AND RICHIESTA_CANDIDATO_HIST.COD_ESITO_SEL = 14 " &_
						"AND RICHIESTA_CANDIDATO_HIST.ID_RICHIESTA = richiesta_sede.id_richiesta  " &_
						"AND ( " &_
							"SELECT top 1 rch.DT_INS " &_
							"FROM RICHIESTA_CANDIDATO_HIST rch " &_
							"WHERE rch.ID_RICHIESTA = RICHIESTA_CANDIDATO_HIST.ID_RICHIESTA " &_
										"AND rch.ID_PERSONA = RICHIESTA_CANDIDATO_HIST.ID_PERSONA " &_
							"ORDER BY rch.DT_INS DESC " &_
						") = RICHIESTA_CANDIDATO_HIST.DT_INS " &_
						"GROUP BY CAST(RICHIESTA_CANDIDATO_HIST.COD_ESITO_SEL AS int) " &_
					") AS TOTAL_COD_CATORCE " &_
					"FROM SEDE_IMPRESA " &_
					"inner join IMPRESA im on im.id_impresa = SEDE_IMPRESA.id_impresa " &_
					"inner join richiesta_sede on richiesta_sede.id_sede = SEDE_IMPRESA.id_sede " &_
					"inner join FIGUREPROFESSIONALI f on richiesta_sede.id_figprof = f.id_figprof " &_
					"inner join impresa_rolesserviciostipos irs ON richiesta_sede.servicio = irs.servicio " &_
					"inner join AREE_PROFESSIONALI a on a.id_areaprof = f.id_areaprof " &_
					"inner join SEDE_IMPRESA sim on sim.id_sede = richiesta_sede.COD_OFICINA " &_
					"WHERE	richiesta_sede.DT_finpublicacion < convert(datetime,CONVERT(varchar(10), GETDATE(), 103),103) AND richiesta_sede.FL_PUBBLICATO = 'S' " &_
						"AND richiesta_sede.cod_tipo_richiesta = 1 "

		
		reportFilterDataSplit = Split(Base64Decode(reportFilterDataBase64),",")
		
		for i=0 to ubound(reportFilterDataSplit)	  			
	  		
	  		reportFilterData = Split(reportFilterDataSplit(i),":")
	  		columnName = reportFilterData(0)  
	  		valueColumn = reportFilterData(1)
	  		
			if valueColumn <> "" then
				if columnName = "empleador" then					
					sSQLExpiredDeals = sSQLExpiredDeals & " AND im.RAG_SOC LIKE '%" & valueColumn & "%' "
				end if
				if columnName = "titulo_oferta" then					
					sSQLExpiredDeals = sSQLExpiredDeals & " AND richiesta_sede.offer_title LIKE '%" & valueColumn & "%' "
				end if
				if columnName = "centro_atencion" then					
					sSQLExpiredDeals = sSQLExpiredDeals & " AND richiesta_sede.COD_OFICINA = " & valueColumn
				end if
				if columnName = "fecha_publicacion" then
					sSQLExpiredDeals = sSQLExpiredDeals & " AND convert(datetime,convert(varchar(25),richiesta_sede.DT_INGRESO, 103), 103) = convert(datetime,convert(varchar(25),'" & valueColumn & " 00:00:00', 103), 103) " 
				end if
				if columnName = "fecha_vencimiento_from" then	
					fecha_vencimiento_toSplit = Split(reportFilterDataSplit(i+1),":")
			  		fecha_vencimiento_to = fecha_vencimiento_toSplit(1)

					sSQLExpiredDeals = sSQLExpiredDeals & " AND convert(datetime,richiesta_sede.DT_FINPUBLICACION, 103) BETWEEN convert(datetime,convert(varchar(25),'" & valueColumn & " 00:00:00', 103), 103) AND convert(datetime,convert(varchar(25),'" & fecha_vencimiento_to & " 00:00:00', 103),103) "
				end if
				if columnName = "nroVacantesVsPostulantes" then
					if valueColumn = "Mayor" then
						sSQLExpiredDeals = sSQLExpiredDeals & " AND richiesta_sede.nro_vacantes > (SELECT count(DISTINCT(RICHIESTA_CANDIDATO_HIST.ID_PERSONA)) FROM RICHIESTA_CANDIDATO_HIST WHERE RICHIESTA_CANDIDATO_HIST.COD_ESITO_SEL IN (1,14) AND RICHIESTA_CANDIDATO_HIST.ID_RICHIESTA = richiesta_sede.id_richiesta)"
					end if
					if valueColumn = "Menor" then
						sSQLExpiredDeals = sSQLExpiredDeals & " AND richiesta_sede.nro_vacantes < (SELECT count(DISTINCT(RICHIESTA_CANDIDATO_HIST.ID_PERSONA)) FROM RICHIESTA_CANDIDATO_HIST WHERE RICHIESTA_CANDIDATO_HIST.COD_ESITO_SEL IN (1,14) AND RICHIESTA_CANDIDATO_HIST.ID_RICHIESTA = richiesta_sede.id_richiesta)"
					end if
					if valueColumn = "Igual" then
						sSQLExpiredDeals = sSQLExpiredDeals & " AND richiesta_sede.nro_vacantes = (SELECT count(DISTINCT(RICHIESTA_CANDIDATO_HIST.ID_PERSONA)) FROM RICHIESTA_CANDIDATO_HIST WHERE RICHIESTA_CANDIDATO_HIST.COD_ESITO_SEL IN (1,14) AND RICHIESTA_CANDIDATO_HIST.ID_RICHIESTA = richiesta_sede.id_richiesta)"
					end if
				end if
			end if
	  		
		next
		sSQLExpiredDeals = sSQLExpiredDeals & " Order by richiesta_sede.DT_FINPUBLICACION asc"

		generateQueryExpiredDealsExporttoExcel = sSQLExpiredDeals 
	end function

	function generateQueryRegisteredCompaniesExporttoExcel(reportFilterDataBase64)

		sSQLRegisteredCompanies = "SELECT i.RAG_SOC as 'NOMBRE DE LA EMPRESA', " &_ 
								"si.DESCRIZIONE as SEDE, " &_ 
								"dep.DESCRIZIONE AS 'DEPARTAMENTO', " &_
								"Ciudad.DESCOM AS 'CIUDAD'," &_
								"si.E_MAIL_SEDE as 'Correo Electr�nico', " &_
								"si.NUM_TEL_SEDE as 'N�mero Telef�nico 1', " &_
								"si.NUM_TEL as 'N�mero Telef�nico 2', " &_
								"convert(varchar(25),i.DT_TMST, 103) as 'FECHA DE INSCRIPCI�N' " &_				
								"FROM sede_impresa si WITH(nolock) " &_
								"INNER JOIN (SELECT DISTINCT id_impresa, rol FROM impresa_rolesservicios WITH(nolock)) sri ON sri.id_impresa = si.id_impresa " &_
								"INNER JOIN IMPRESA I ON si.ID_IMPRESA =i.ID_IMPRESA " &_
								"INNER JOIN (select CODICE, DESCRIZIONE from tades where nome_tabella = 'PROV') Dep ON si.PRV =dep.CODICE " &_
								"INNER JOIN (SELECT CODCOM,DESCOM from COMUNE) Ciudad ON si.COMUNE=Ciudad.CODCOM " &_
								"WHERE sri.rol = 1 " 

		reportFilterDataSplit = Split(Base64Decode(reportFilterDataBase64),",")
		
		for i=0 to ubound(reportFilterDataSplit)	  			
	  		
	  		reportFilterData = Split(reportFilterDataSplit(i),":")
	  		columnName = reportFilterData(0)  
	  		valueColumn = reportFilterData(1)


			if valueColumn <> "" then

				if columnName = "nombre_empresa" then
					sSQLRegisteredCompanies = sSQLRegisteredCompanies & "AND i.RAG_SOC LIKE '%" & valueColumn & "%' "
				end if
				if columnName = "sede_empresa" then
					sSQLRegisteredCompanies = sSQLRegisteredCompanies & "AND si.DESCRIZIONE LIKE '%" & valueColumn & "%' "
				end if
				if columnName = "departamento"then
					sSQLRegisteredCompanies = sSQLRegisteredCompanies & "AND dep.CODICE = '" & valueColumn & "' "
				end if
				if columnName = "ciudad" then
					sSQLRegisteredCompanies = sSQLRegisteredCompanies & "AND Ciudad.CODCOM = '" & valueColumn & "' "
				end if
				if columnName = "fecha_inscripcion_from" then		
					fecha_vencimiento_toSplit = Split(reportFilterDataSplit(i+1),":")
				  	fecha_vencimiento_to = fecha_vencimiento_toSplit(1)			
				  	
					sSQLRegisteredCompanies = sSQLRegisteredCompanies & " AND convert(datetime,i.DT_TMST, 103) BETWEEN convert(datetime,convert(varchar(25),'" & valueColumn & " 00:00:00', 103), 103) AND convert(datetime,convert(varchar(25),'" & fecha_vencimiento_to & " 00:00:00', 103),103) " 
				end if

			end if

		next
					
		sSQLRegisteredCompanies = sSQLRegisteredCompanies & "ORDER BY si.ID_SEDE"

		'response.Write sSQLRegisteredCompanies

		generateQueryRegisteredCompaniesExporttoExcel = sSQLRegisteredCompanies
	end function

	function generateQueryRegisteredPeopleExporttoExcel(reportFilterDataBase64)
		centro_atencion = ""
		
		reportFilterDataSplit = Split(Base64Decode(reportFilterDataBase64),",")
					  				  		
		reportFilterData = Split(reportFilterDataSplit(0),":")
		columnName = reportFilterData(0)  
		valueColumn = reportFilterData(1)


		if valueColumn <> "" then

			if columnName = "centro_atencion" then
				centro_atencion = valueColumn
			end if
		end if 
		
		sSQLRegisteredPeopleTimas = "SELECT TI.ID_PERSONA, MAX(TITAD.VALORE) VALORE INTO ##TIMAS9870 " &_
								"FROM TISTUD TI with(nolock) " &_
								"INNER JOIN (SELECT * FROM TADES with(nolock)  WHERE NOME_TABELLA= 'LSTUD') TITAD " &_
         						"ON TI.COD_LIV_STUD = TITAD.CODICE " &_
 								"GROUP BY TI.ID_PERSONA " 
	 
		sSQLRegisteredPeopleCurson = "SELECT P.ID_PERSONA, P.SESSO, ISNULL(TIMAS.VALORE,'ND') AS VALORE INTO ##CURSON49857 " &_
									"FROM PERSONA P with(nolock) LEFT JOIN  ##TIMAS9870 TIMAS " &_
	                				"ON P.ID_PERSONA = TIMAS.ID_PERSONA " &_
									"WHERE P.ID_PERSONA IN (SELECT DISTINCT ID_PERSONA FROM STATO_OCCUPAZIONALE with(nolock) WHERE ID_CIMPIEGO is not null)" &_
									"ORDER BY P.ID_PERSONA "
	
		sSQLRegisteredPeople = "SELECT p.ID_PERSONA, " &_
								"p.COD_FISC AS 'IDENTIFICACION', " &_
								"replace(p.NOME,'&#209;','�') AS 'PRIMER NOMBRE', " &_
								"replace(p.SECONDO_NOME,'&#209;','�') AS 'SEGUNDO NOMBRE', " &_
								"replace(p.COGNOME,'&#209;','�') AS 'PRIMER APELLIDO', " &_
								"replace(p.SECONDO_COGNOME,'&#209;','�') AS 'SEGUNDO APELLIDO', " &_
								"floor(datediff(MONTH, p.DT_NASC, getdate()) / 12) as 'EDAD', " &_
								"case when p.SESSO='F' then 'FEMENINO' else 'MASCULINO' end as 'SEXO', " &_
								"dep.DESCRIZIONE AS 'DEPARTAMENTO DE RESIDENCIA', " &_
								"Ciudad.DESCOM AS 'CIUDAD DE RESIDENCIA', " &_
								"p.FRAZIONE_RES AS 'BARRIO DE RESIDENCIA', " &_
								"P.NUM_TEL AS 'TELEFONO FIJO', " &_
								"P.NUM_TEL_DOM AS 'TELEFONO CELULAR', " &_
								"P.E_MAIL AS 'CORREO ELECTRONICO', " &_
								"sim.DESCRIZIONE 'CENTRO DE ATENCION', " &_
								"TITAD.DESCRIZIONE 'MAXIMO NIVEL EDUCATIVO', " &_
								"t.DESCRIZIONE  as 'SITUACION LABORAL ACTUAL', " &_
								"min(convert(varchar(25), SO.DT_DICH_STATUS, 103)) AS 'FECHA DE INSCRIPCION', " &_
								"floor(DATEDIFF(day,SO.DT_DICH_STATUS,GETDATE())) AS 'DIAS DE INSCRIPCION', " &_
								"REPLACE(REPLACE(substring(isnull(z.JOB_PROFILE,'NO REGISTRA'),1,DATALENGTH(isnull(z.JOB_PROFILE,'NO REGISTRA'))),CHAR(13),''),CHAR(10),'') AS 'PERFIL', " &_
								"FROM persona p with (nolock) left join PERSONAS_JOB_PROFILE z with (nolock) on p.id_persona=z.id_persona " &_
								"left join stato_occupazionale so with (nolock) on p.id_persona=so.id_persona " &_
								"left join tades t on so.cod_stdis=t.CODICE and NOME_TABELLA ='STDIS' " &_
								"left join SEDE_IMPRESA sim with (nolock) on sim.id_sede = so.ID_CIMPIEGO " &_
								"left join (select CODICE, DESCRIZIONE from tades where nome_tabella = 'PROV') Dep " &_
										"on p.PRV_RES =dep.CODICE " &_
								"left join (SELECT CODCOM,DESCOM from COMUNE) Ciudad on p.COM_RES=Ciudad.CODCOM " &_
								"left join (SELECT pe.ID_PERSONA, pe.sesso, ISNULL(y.VALORE,'ND') as valore " &_
								"		FROM PERSONA pe with(nolock) " &_
								"		LEFT JOIN (select TI.ID_PERSONA, MAX(TITAD.VALORE) VALORE " &_
								"			from TISTUD TI with(nolock) " &_
								" 			INNER JOIN (SELECT * FROM TADES with(nolock)  WHERE NOME_TABELLA= 'LSTUD') TITAD " &_
								"   		ON TI.COD_LIV_STUD = TITAD.CODICE " &_
								"			GROUP BY TI.ID_PERSONA ) as y " &_
								"			on pe.id_persona = y.ID_PERSONA " &_
								"			WHERE pe.ID_PERSONA IN (SELECT DISTINCT f.ID_PERSONA FROM STATO_OCCUPAZIONALE f with(nolock) WHERE f.ID_CIMPIEGO is not null) " &_
								"			) C  " &_
								"on C.ID_PERSONA = p.ID_PERSONA  " &_
								"left JOIN (SELECT VALORE, DESCRIZIONE FROM TADES with(nolock) WHERE NOME_TABELLA= 'LSTUD' UNION( SELECT 'ND', 'No Disponible')) TITAD on c.valore = TITAD.valore " 
							if centro_atencion<>"" then	
		sSQLRegisteredPeople = 	sSQLRegisteredPeople & " WHERE so.id_cimpiego = "&centro_atencion &_
								"AND (ind_status = '0' OR (ind_status = '2' " &_
								"AND P.ID_PERSONA not in ( SELECT ID_PERSONA FROM STATO_OCCUPAZIONALE WHERE p.id_persona = id_persona AND ind_status = '0' ))) " &_
								"AND SO.DT_DICH_STATUS >= convert(datetime,'01/05/2013 10:0:0',103) " &_
								"group BY p.ID_PERSONA, p.COD_FISC,replace(p.NOME,'&#209;','�') , replace(p.SECONDO_NOME,'&#209;','�'), replace(p.COGNOME,'&#209;','�'), replace(p.SECONDO_COGNOME,'&#209;','�'), floor(datediff(MONTH, p.DT_NASC, getdate()) / 12),p.SESSO, floor(DATEDIFF(day,SO.DT_DICH_STATUS,GETDATE())),dep.DESCRIZIONE,Ciudad.DESCOM,P.NUM_TEL,P.NUM_TEL_DOM, P.E_MAIL, P.FRAZIONE_RES, sim.DESCRIZIONE,t.DESCRIZIONE,TITAD.DESCRIZIONE, z.JOB_PROFILE " &_
								"ORDER BY 'PRIMER NOMBRE', 'SEGUNDO NOMBRE', 'PRIMER APELLIDO', 'SEGUNDO APELLIDO' " 
								'"DROP TABLE ##TIMAS9870 DROP TABLE ##CURSON49857 "
						    else
		sSQLRegisteredPeople = 	sSQLRegisteredPeople & " WHERE (ind_status = '0' OR (ind_status = '2' " &_
								"AND P.ID_PERSONA not in ( SELECT ID_PERSONA FROM STATO_OCCUPAZIONALE WHERE p.id_persona = id_persona AND ind_status = '0' ))) " &_
								"AND SO.DT_DICH_STATUS >= convert(datetime,'01/05/2013 10:0:0',103) " &_
								"group BY p.ID_PERSONA, p.COD_FISC,replace(p.NOME,'&#209;','�') , replace(p.SECONDO_NOME,'&#209;','�'), replace(p.COGNOME,'&#209;','�'), replace(p.SECONDO_COGNOME,'&#209;','�'), floor(datediff(MONTH, p.DT_NASC, getdate()) / 12),p.SESSO, floor(DATEDIFF(day,SO.DT_DICH_STATUS,GETDATE())),dep.DESCRIZIONE,Ciudad.DESCOM,P.NUM_TEL,P.NUM_TEL_DOM, P.E_MAIL, P.FRAZIONE_RES, sim.DESCRIZIONE,t.DESCRIZIONE,TITAD.DESCRIZIONE, z.JOB_PROFILE " &_
								"ORDER BY 'PRIMER NOMBRE', 'SEGUNDO NOMBRE', 'PRIMER APELLIDO', 'SEGUNDO APELLIDO' " 
								'"DROP TABLE ##TIMAS9870 DROP TABLE ##CURSON49857 "					
							end if 
		

		sSQLRegisteredPeopleComplete = "SET NOCOUNT ON; " & sSQLRegisteredPeople
		'response.Write sSQLRegisteredPeopleComplete
		generateQueryRegisteredPeopleExporttoExcel = sSQLRegisteredPeopleComplete
	end function


function generateQueryRegisteredPeopleByCenterExporttoExcel(reportFilterDataBase64)
		centro_atencion = ""
		
		reportFilterDataSplit = Split(Base64Decode(reportFilterDataBase64),",")
					  				  		
		reportFilterData = Split(reportFilterDataSplit(0),":")
		columnName = reportFilterData(0)  
		valueColumn = reportFilterData(1)


		if valueColumn <> "" then

			if columnName = "centro_atencion" then
				centro_atencion = valueColumn
			end if
		end if 

		'sSQLRegisteredPeopleTimas = "SELECT TI.ID_PERSONA, MAX(TITAD.VALORE) VALORE INTO ##TIMAS9870 " &_
		''						"FROM TISTUD TI with(nolock) " &_
		''						"INNER JOIN (SELECT * FROM TADES with(nolock)  WHERE NOME_TABELLA= 'LSTUD') TITAD " &_
         ''						"ON TI.COD_LIV_STUD = TITAD.CODICE " &_
 		''						"GROUP BY TI.ID_PERSONA " 
	 
		'sSQLRegisteredPeopleCurson = "SELECT P.ID_PERSONA, P.SESSO, ISNULL(TIMAS.VALORE,'ND') AS VALORE INTO ##CURSON49857 " &_
		''							"FROM PERSONA P with(nolock) LEFT JOIN  ##TIMAS9870 TIMAS " &_
	     ''           				"ON P.ID_PERSONA = TIMAS.ID_PERSONA " &_
		''							"WHERE P.ID_PERSONA IN (SELECT DISTINCT ID_PERSONA FROM STATO_OCCUPAZIONALE with(nolock) WHERE ID_CIMPIEGO = " & session("Creator") & ") " &_
		''							"ORDER BY P.ID_PERSONA "
	


		sSQLRegisteredPeople =   "SELECT p.ID_PERSONA,    " &_
								 "p.COD_FISC AS 'IDENTIFICACION',    " &_
								 "replace(p.NOME,'&#209;','�') AS 'PRIMER NOMBRE',    " &_
								 "replace(p.SECONDO_NOME,'&#209;','�') AS 'SEGUNDO NOMBRE',    " &_
								 "replace(p.COGNOME,'&#209;','�') AS 'PRIMER APELLIDO',    " &_
								 "replace(p.SECONDO_COGNOME,'&#209;','�') AS 'SEGUNDO APELLIDO',    " &_
								 "floor(datediff(MONTH, p.DT_NASC, getdate()) / 12) as 'EDAD',    " &_
								 "case when p.SESSO='F' then 'FEMENINO' else 'MASCULINO' end as 'SEXO',    " &_
								 "dep.DESCRIZIONE AS 'DEPARTAMENTO DE RESIDENCIA',    " &_
								 "Ciudad.DESCOM AS 'CIUDAD DE RESIDENCIA',    " &_
								 "p.FRAZIONE_RES AS 'BARRIO DE RESIDENCIA',    " &_
								 "P.NUM_TEL AS 'TELEFONO FIJO',    " &_
								 "P.NUM_TEL_DOM AS 'TELEFONO CELULAR',    " &_
								 "P.E_MAIL AS 'CORREO ELECTRONICO',    " &_
								 "sim.DESCRIZIONE 'CENTRO DE ATENCION',    " &_
								 "TITAD.DESCRIZIONE 'MAXIMO NIVEL EDUCATIVO',    " &_
								 "t.DESCRIZIONE as 'SITUACION LABORAL ACTUAL',     " &_
								 "min(convert(varchar(25), SO.DT_DICH_STATUS, 103)) AS 'FECHA DE INSCRIPCION',    " &_
								 "floor(DATEDIFF(day,SO.DT_DICH_STATUS,GETDATE())) AS 'DIAS DE INSCRIPCION',    " &_
						 		 "REPLACE(REPLACE(isnull(CAST ( h.job_profile AS VARCHAR( 2000 ) ),'NO REGISTRA'),CHAR(10),''),CHAR(13),'') AS 'PERFIL', " &_								 
								 "isnull(DER1.CUENTA,0) as 'INTERMEDIACION',    " &_
								 "isnull(DER2.CUENTA,0) as 'EXTERNAS - CAPACITACION',    " &_
								 "isnull(DER4.CUENTA,0) as 'INTERNAS - ENTREVISTA DE ORIENTACION INDIVIDUAL',    " &_
								 "isnull(DER6.CUENTA,0) as 'INTERNAS - TALLERES',    " &_
								 "isnull(DER8.CUENTA,0) as 'EXTERNAS - SERVICIOS DE SALUD',    " &_
								 "isnull(DER11.CUENTA,0) as 'EXTERNAS - OTROS PROGRAMAS SOCIALES'    " &_
								 "FROM stato_occupazionale so    " &_
								 "INNER join (SELECT ID_PERSONA, MAX(DT_DICH_STATUS) DT_DICH_STATUS FROM STATO_OCCUPAZIONALE WITH (NOLOCK) GROUP BY ID_PERSONA ) soUNI    " &_
								 "ON SO.ID_PERSONA = soUNI.ID_PERSONA    " &_
								 "AND SO.DT_DICH_STATUS = SOUNI.DT_DICH_STATUS    " &_
								 "INNER JOIN persona p with(nolock)    " &_
								 "on p.id_persona=so.id_persona    " &_
								 "left join personas_job_profile as h " &_
  								 "on p.id_persona = h.id_persona " &_
								 "left join tades t with(nolock)     " &_
								 "on so.cod_stdis=t.CODICE and NOME_TABELLA ='STDIS'     " &_
								 "left join SEDE_IMPRESA sim with(nolock)    " &_
								 "on sim.id_sede = so.ID_CIMPIEGO    " &_
								 "left join (select CODICE, DESCRIZIONE from tades with(nolock) where nome_tabella = 'PROV') Dep    " &_
								 "on p.PRV_RES =dep.CODICE    " &_
								 "left join (SELECT CODCOM,DESCOM from COMUNE with(nolock) ) Ciudad    " &_
								 "on p.COM_RES=Ciudad.CODCOM    " &_
								 "left join (SELECT pe.ID_PERSONA, pe.sesso, ISNULL(y.VALORE,'ND') as valore    " &_
								 		"FROM PERSONA pe with(nolock)    " &_
								 		"LEFT JOIN (select TI.ID_PERSONA, MAX(TITAD.VALORE) VALORE    " &_
								 			"from TISTUD TI with(nolock)    " &_
								  			"INNER JOIN (SELECT * FROM TADES with(nolock)  WHERE NOME_TABELLA= 'LSTUD') TITAD    " &_
								    		"ON TI.COD_LIV_STUD = TITAD.CODICE    " &_
								 			"GROUP BY TI.ID_PERSONA ) as y    " &_
								 			"on pe.id_persona = y.ID_PERSONA    " &_
								 			"WHERE pe.ID_PERSONA IN (SELECT DISTINCT f.ID_PERSONA FROM STATO_OCCUPAZIONALE f with(nolock) WHERE f.ID_CIMPIEGO is not null)    " &_
								 			") C     " &_
								 "on C.ID_PERSONA = p.ID_PERSONA     " &_
								 "LEFT JOIN (SELECT ID_PERSONA, COD_DERIV, COUNT(ID_PERSONA) AS CUENTA FROM dbo.PERS_DERIVACION PEDE WHERE COD_DERIV = 'DER1' GROUP BY ID_PERSONA, COD_DERIV) der1 ON P.ID_PERSONA = der1.ID_PERSONA    " &_
								 "LEFT JOIN (SELECT ID_PERSONA, COD_DERIV, COUNT(ID_PERSONA) AS CUENTA FROM dbo.PERS_DERIVACION PEDE WHERE COD_DERIV = 'DER2' GROUP BY ID_PERSONA, COD_DERIV) DER2 ON P.ID_PERSONA = DER2.ID_PERSONA    " &_
								 "LEFT JOIN (SELECT ID_PERSONA, COD_DERIV, COUNT(ID_PERSONA) AS CUENTA FROM dbo.PERS_DERIVACION PEDE WHERE COD_DERIV = 'DER4' GROUP BY ID_PERSONA, COD_DERIV) DER4 ON P.ID_PERSONA = DER4.ID_PERSONA    " &_
								 "LEFT JOIN (SELECT ID_PERSONA, COD_DERIV, COUNT(ID_PERSONA) AS CUENTA FROM dbo.PERS_DERIVACION PEDE WHERE COD_DERIV = 'DER6' GROUP BY ID_PERSONA, COD_DERIV) DER6 ON P.ID_PERSONA = DER6.ID_PERSONA    " &_
								 "LEFT JOIN (SELECT ID_PERSONA, COD_DERIV, COUNT(ID_PERSONA) AS CUENTA FROM dbo.PERS_DERIVACION PEDE WHERE COD_DERIV = 'DER8' GROUP BY ID_PERSONA, COD_DERIV) DER8 ON P.ID_PERSONA = DER8.ID_PERSONA    " &_
								 "LEFT JOIN (SELECT ID_PERSONA, COD_DERIV, COUNT(ID_PERSONA) AS CUENTA FROM dbo.PERS_DERIVACION PEDE WHERE COD_DERIV = 'DER11' GROUP BY ID_PERSONA, COD_DERIV) DER11 ON P.ID_PERSONA = DER11.ID_PERSONA    " &_
								 "INNER JOIN (SELECT VALORE, DESCRIZIONE FROM TADES with(nolock) WHERE NOME_TABELLA= 'LSTUD' UNION( SELECT 'ND', 'No Disponible')) TITAD on c.valore = TITAD.valore    " &_
								 "WHERE so.id_cimpiego =  " & centro_atencion & " " &_
								 "AND SO.DT_DICH_STATUS >= convert(datetime,'01/05/2013 10:0:0',103)    " &_
								 "group BY p.ID_PERSONA , p.COD_FISC,p.NOME, p.SECONDO_NOME, p.COGNOME, p.SECONDO_COGNOME, floor(datediff(MONTH, p.DT_NASC, getdate()) / 12),p.SESSO,dep.DESCRIZIONE,Ciudad.DESCOM,p.FRAZIONE_RES,P.NUM_TEL,P.NUM_TEL_DOM, P.E_MAIL, sim.DESCRIZIONE,TITAD.DESCRIZIONE,t.DESCRIZIONE,SO.DT_DICH_STATUS,CAST ( h.job_profile AS VARCHAR( 2000 ) ),DER1.CUENTA, DER2.CUENTA, DER4.CUENTA, DER6.CUENTA, DER8.CUENTA, DER11.CUENTA,SO.DT_DICH_STATUS     " &_
								 "ORDER BY 'PRIMER NOMBRE', 'SEGUNDO NOMBRE', 'PRIMER APELLIDO', 'SEGUNDO APELLIDO'   " 

		'sSQLRegisteredPeople =   "SELECT p.ID_PERSONA,    " &_
		''						 "p.COD_FISC AS 'IDENTIFICACION',    " &_
		''						 "replace(p.NOME,'&#209;','�') AS 'PRIMER NOMBRE',    " &_
		''						 "replace(p.SECONDO_NOME,'&#209;','�') AS 'SEGUNDO NOMBRE',    " &_
		''						 "replace(p.COGNOME,'&#209;','�') AS 'PRIMER APELLIDO',    " &_
		''						 "replace(p.SECONDO_COGNOME,'&#209;','�') AS 'SEGUNDO APELLIDO',    " &_
		''						 "floor(datediff(MONTH, p.DT_NASC, getdate()) / 12) as 'EDAD',    " &_
		''						 "case when p.SESSO='F' then 'FEMENINO' else 'MASCULINO' end as 'SEXO',    " &_
		''						 "dep.DESCRIZIONE AS 'DEPARTAMENTO DE RESIDENCIA',    " &_
		''						 "Ciudad.DESCOM AS 'CIUDAD DE RESIDENCIA',    " &_
		''						 "p.FRAZIONE_RES AS 'BARRIO DE RESIDENCIA',    " &_
		''						 "P.NUM_TEL AS 'TELEFONO FIJO',    " &_
		''						 "P.NUM_TEL_DOM AS 'TELEFONO CELULAR',    " &_
		''						 "P.E_MAIL AS 'CORREO ELECTRONICO',    " &_
		''						 "sim.DESCRIZIONE 'CENTRO DE ATENCION',    " &_
		''						 "TITAD.DESCRIZIONE 'MAXIMO NIVEL EDUCATIVO',    " &_
		''						 "t.DESCRIZIONE  as 'SITUACION LABORAL ACTUAL',    " &_
		''						 "min(convert(varchar(25), SO.DT_DICH_STATUS, 103)) AS 'FECHA DE INSCRIPCION',  " &_  
		''						 "floor(DATEDIFF(day,SO.DT_DICH_STATUS,GETDATE())) AS 'DIAS DE INSCRIPCION',   " &_ 
		''						 "'' AS 'PERFIL', " &_
		''						 "isnull(DER1.CUENTA,0) as 'INTERMEDIACION',    " &_
		''						 "isnull(DER2.CUENTA,0) as 'EXTERNAS - CAPACITACION',    " &_
		''						 "isnull(DER4.CUENTA,0) as 'INTERNAS - ENTREVISTA DE ORIENTACION INDIVIDUAL',    " &_
		''						 "isnull(DER6.CUENTA,0) as 'INTERNAS - TALLERES',    " &_
		''						 "isnull(DER8.CUENTA,0) as 'EXTERNAS - SERVICIOS DE SALUD',    " &_
		''						 "isnull(DER11.CUENTA,0) as 'EXTERNAS - OTROS PROGRAMAS SOCIALES'     " &_
		''						 "FROM persona p    " &_
		''						 "inner join stato_occupazionale so on p.id_persona=so.id_persona    " &_
		''						 "inner join tades t on so.cod_stdis=t.CODICE and NOME_TABELLA ='STDIS'     " &_
		''						 "inner join SEDE_IMPRESA sim on sim.id_sede = so.ID_CIMPIEGO    " &_
		''						 "inner join (select CODICE, DESCRIZIONE from tades where nome_tabella = 'PROV') Dep     " &_
		''						"		 on p.PRV_RES =dep.CODICE     " &_
		''						 "inner join (SELECT CODCOM,DESCOM from COMUNE) Ciudad on p.COM_RES=Ciudad.CODCOM     " &_
		''						 "left join (SELECT pe.ID_PERSONA, pe.sesso, ISNULL(y.VALORE,'ND') as valore " &_
		''						 "				FROM PERSONA pe with(nolock)  " &_
		''						"				LEFT JOIN (select TI.ID_PERSONA, MAX(TITAD.VALORE) VALORE  " &_
		''						"							 from TISTUD TI with(nolock)  " &_
		''						"						 INNER JOIN (SELECT * FROM TADES with(nolock)  WHERE NOME_TABELLA= 'LSTUD') TITAD  " &_
		''						"							 ON TI.COD_LIV_STUD = TITAD.CODICE " &_
		''						"							GROUP BY TI.ID_PERSONA ) as y " &_
		''						"			on pe.id_persona = y.ID_PERSONA  " &_
		''						"			WHERE pe.ID_PERSONA IN (SELECT DISTINCT f.ID_PERSONA FROM STATO_OCCUPAZIONALE f with(nolock) WHERE f.ID_CIMPIEGO is not null) " &_
		''						"			) as C on C.ID_PERSONA = p.ID_PERSONA   " &_
		''						 "LEFT JOIN (SELECT ID_PERSONA, COD_DERIV, COUNT(ID_PERSONA) AS CUENTA FROM dbo.PERS_DERIVACION PEDE WHERE COD_DERIV = 'DER1' GROUP BY ID_PERSONA, COD_DERIV) der1 ON P.ID_PERSONA = der1.ID_PERSONA    " &_
		''						 "LEFT JOIN (SELECT ID_PERSONA, COD_DERIV, COUNT(ID_PERSONA) AS CUENTA FROM dbo.PERS_DERIVACION PEDE WHERE COD_DERIV = 'DER2' GROUP BY ID_PERSONA, COD_DERIV) DER2 ON P.ID_PERSONA = DER2.ID_PERSONA    " &_
		''						 "LEFT JOIN (SELECT ID_PERSONA, COD_DERIV, COUNT(ID_PERSONA) AS CUENTA FROM dbo.PERS_DERIVACION PEDE WHERE COD_DERIV = 'DER4' GROUP BY ID_PERSONA, COD_DERIV) DER4 ON P.ID_PERSONA = DER4.ID_PERSONA    " &_
		''						 "LEFT JOIN (SELECT ID_PERSONA, COD_DERIV, COUNT(ID_PERSONA) AS CUENTA FROM dbo.PERS_DERIVACION PEDE WHERE COD_DERIV = 'DER6' GROUP BY ID_PERSONA, COD_DERIV) DER6 ON P.ID_PERSONA = DER6.ID_PERSONA    " &_
		''						 "LEFT JOIN (SELECT ID_PERSONA, COD_DERIV, COUNT(ID_PERSONA) AS CUENTA FROM dbo.PERS_DERIVACION PEDE WHERE COD_DERIV = 'DER8' GROUP BY ID_PERSONA, COD_DERIV) DER8 ON P.ID_PERSONA = DER8.ID_PERSONA    " &_
		''						 "LEFT JOIN (SELECT ID_PERSONA, COD_DERIV, COUNT(ID_PERSONA) AS CUENTA FROM dbo.PERS_DERIVACION PEDE WHERE COD_DERIV = 'DER11' GROUP BY ID_PERSONA, COD_DERIV) DER11 ON P.ID_PERSONA = DER11.ID_PERSONA    " &_
		''						 "INNER JOIN (SELECT VALORE, DESCRIZIONE FROM TADES with(nolock) WHERE NOME_TABELLA= 'LSTUD' UNION( SELECT 'ND', 'No Disponible')) TITAD on c.valore = TITAD.valore    " &_
		''						 "WHERE so.id_cimpiego =  "& SESsION("CREATOR") &"     " &_
		''						 "AND (ind_status = '0' OR (ind_status = '2'    " &_
		''						 "AND P.ID_PERSONA not in ( SELECT ID_PERSONA FROM STATO_OCCUPAZIONALE WHERE p.id_persona = id_persona AND ind_status = '0' )))    " &_
		''						 "AND SO.DT_DICH_STATUS >= convert(datetime,'01/05/2013 10:0:0',103)    " &_
		''						 "group BY p.ID_PERSONA, p.COD_FISC,replace(p.NOME,'&#209;','�') , replace(p.SECONDO_NOME,'&#209;','�'), replace(p.COGNOME,'&#209;','�'), replace(p.SECONDO_COGNOME,'&#209;','�'), floor(datediff(MONTH, p.DT_NASC, getdate()) / 12),p.SESSO, floor(DATEDIFF(day,SO.DT_DICH_STATUS,GETDATE())),dep.DESCRIZIONE,Ciudad.DESCOM,P.NUM_TEL,P.NUM_TEL_DOM, P.E_MAIL, P.FRAZIONE_RES, sim.DESCRIZIONE,t.DESCRIZIONE,TITAD.DESCRIZIONE,DER1.CUENTA, DER2.CUENTA, DER4.  CUENTA, DER6.CUENTA, DER8.CUENTA, DER11.CUENTA    " &_
		''						 "ORDER BY 'PRIMER NOMBRE', 'SEGUNDO NOMBRE', 'PRIMER APELLIDO', 'SEGUNDO APELLIDO'    " 
								 
								

								
		sSQLRegisteredPeopleComplete = "SET NOCOUNT ON; " & sSQLRegisteredPeople

		generateQueryRegisteredPeopleByCenterExporttoExcel = sSQLRegisteredPeopleComplete
	end function


	function generateQueryCurrentDealsByCenterExporttoExcel(reportFilterDataBase64)
		
		sSQLCurrentDealsByCenter = "SELECT richiesta_sede.id_richiesta AS Referencia," &_ 
									"im.RAG_SOC+' ('+SEDE_IMPRESA.DESCRIZIONE+')' as Empleador," &_ 
									"richiesta_sede.offer_title as 'T�tulo de la Oferta'," &_
									"f.denominazione as 'Cargo Equivalente', a.denominazione as 'Grupo Ocupacional', " &_
									"richiesta_sede.nro_vacantes as 'Numero de Vacantes', " &_
										"(SELECT COUNT(RICHIESTA_CANDIDATO_HIST.ID_RICHIESTA) AS NUMERO " &_
										"FROM RICHIESTA_CANDIDATO_HIST " &_
										"WHERE RICHIESTA_CANDIDATO_HIST.COD_ESITO_SEL IN (1,14) " &_
										"AND RICHIESTA_CANDIDATO_HIST.ID_RICHIESTA = richiesta_sede.id_richiesta " &_
										"AND ( " &_
											"SELECT top 1 rch.DT_INS " &_
											"FROM RICHIESTA_CANDIDATO_HIST rch " &_
											"WHERE rch.ID_RICHIESTA = RICHIESTA_CANDIDATO_HIST.ID_RICHIESTA " &_
														"AND rch.ID_PERSONA = RICHIESTA_CANDIDATO_HIST.ID_PERSONA " &_
											"ORDER BY rch.DT_INS DESC " &_
										") = RICHIESTA_CANDIDATO_HIST.DT_INS " &_
									") AS 'Num Postulantes', " &_
									"(SELECT count(DISTINCT(RICHIESTA_CANDIDATO_HIST.ID_PERSONA)) " &_
										"FROM RICHIESTA_CANDIDATO_HIST " &_
										"WHERE RICHIESTA_CANDIDATO_HIST.COD_ESITO_SEL IN (1,14) " &_
										"AND RICHIESTA_CANDIDATO_HIST.ID_RICHIESTA = richiesta_sede.id_richiesta) 'Num Postulantes old'," &_
									" sim.DESCRIZIONE as 'Centro de Atenci�n', " &_
									" sim.ID_SEDE as 'Id Centro de Atenci�n', " &_
									"convert(varchar(25),richiesta_sede.DT_FINPUBLICACION, 103) as 'Fecha Vencimiento', " &_
									"convert(varchar(25),richiesta_sede.DT_INGRESO, 103) as 'Fecha Inicio', " &_
									"(SELECT COUNT(RICHIESTA_CANDIDATO_HIST.COD_ESITO_SEL) AS NUMERO " &_
										"FROM RICHIESTA_CANDIDATO_HIST " &_
										"WHERE RICHIESTA_CANDIDATO_HIST.COD_ESITO_SEL IN (1,14) " &_
												"AND RICHIESTA_CANDIDATO_HIST.COD_ESITO_SEL = 1 " &_
										"AND RICHIESTA_CANDIDATO_HIST.ID_RICHIESTA = richiesta_sede.id_richiesta  " &_
										"AND ( " &_
											"SELECT top 1 rch.DT_INS " &_
											"FROM RICHIESTA_CANDIDATO_HIST rch " &_
											"WHERE rch.ID_RICHIESTA = RICHIESTA_CANDIDATO_HIST.ID_RICHIESTA " &_
														"AND rch.ID_PERSONA = RICHIESTA_CANDIDATO_HIST.ID_PERSONA " &_
											"ORDER BY rch.DT_INS DESC " &_
										") = RICHIESTA_CANDIDATO_HIST.DT_INS " &_
										"GROUP BY CAST(RICHIESTA_CANDIDATO_HIST.COD_ESITO_SEL AS int) " &_
									") AS TOTAL_COD_UNO, " &_
									"(SELECT COUNT(RICHIESTA_CANDIDATO_HIST.COD_ESITO_SEL) AS NUMERO " &_
										"FROM RICHIESTA_CANDIDATO_HIST " &_
										"WHERE RICHIESTA_CANDIDATO_HIST.COD_ESITO_SEL IN (1,14) " &_
												"AND RICHIESTA_CANDIDATO_HIST.COD_ESITO_SEL = 14 " &_
										"AND RICHIESTA_CANDIDATO_HIST.ID_RICHIESTA = richiesta_sede.id_richiesta  " &_
										"AND ( " &_
											"SELECT top 1 rch.DT_INS " &_
											"FROM RICHIESTA_CANDIDATO_HIST rch " &_
											"WHERE rch.ID_RICHIESTA = RICHIESTA_CANDIDATO_HIST.ID_RICHIESTA " &_
														"AND rch.ID_PERSONA = RICHIESTA_CANDIDATO_HIST.ID_PERSONA " &_
											"ORDER BY rch.DT_INS DESC " &_
										") = RICHIESTA_CANDIDATO_HIST.DT_INS " &_
										"GROUP BY CAST(RICHIESTA_CANDIDATO_HIST.COD_ESITO_SEL AS int) " &_
									") AS TOTAL_COD_CATORCE " &_
									"FROM SEDE_IMPRESA " &_
									"inner join IMPRESA im on im.id_impresa = SEDE_IMPRESA.id_impresa " &_
									"inner join richiesta_sede on richiesta_sede.id_sede = SEDE_IMPRESA.id_sede " &_
									"inner join FIGUREPROFESSIONALI f on richiesta_sede.id_figprof = f.id_figprof " &_
									"inner join impresa_rolesserviciostipos irs ON richiesta_sede.servicio = irs.servicio " &_
									"inner join AREE_PROFESSIONALI a on a.id_areaprof = f.id_areaprof " &_
									"inner join SEDE_IMPRESA sim on sim.id_sede = richiesta_sede.COD_OFICINA " &_
									"WHERE	richiesta_sede.DT_finpublicacion > = convert(datetime,CONVERT(varchar(10), GETDATE(), 103),103) AND richiesta_sede.FL_PUBBLICATO = 'S' " &_
										"AND richiesta_sede.cod_tipo_richiesta = 1 " &_
										"AND richiesta_sede.COD_OFICINA = " & session("Creator") & " "

		
		reportFilterDataSplit = Split(Base64Decode(reportFilterDataBase64),",")
		
		for i=0 to ubound(reportFilterDataSplit)	  			
	  		
	  		reportFilterData = Split(reportFilterDataSplit(i),":")
	  		columnName = reportFilterData(0)  
	  		valueColumn = reportFilterData(1)
	  		
			if valueColumn <> "" then
				if columnName = "empleador" then					
					sSQLCurrentDealsByCenter = sSQLCurrentDealsByCenter & " AND im.RAG_SOC LIKE '%" & valueColumn & "%' "
				end if
				if columnName = "titulo_oferta" then					
					sSQLCurrentDealsByCenter = sSQLCurrentDealsByCenter & " AND richiesta_sede.offer_title LIKE '%" & valueColumn & "%' "
				end if
				if columnName = "fecha_publicacion" then
					sSQLCurrentDealsByCenter = sSQLCurrentDealsByCenter & " AND convert(datetime,convert(varchar(25),richiesta_sede.DT_INGRESO, 103), 103) = convert(datetime,convert(varchar(25),'" & valueColumn & " 00:00:00', 103), 103) " 
				end if
				if columnName = "fecha_vencimiento_from" then	
					fecha_vencimiento_toSplit = Split(reportFilterDataSplit(i+1),":")
			  		fecha_vencimiento_to = fecha_vencimiento_toSplit(1)

					sSQLCurrentDealsByCenter = sSQLCurrentDealsByCenter & " AND convert(datetime,richiesta_sede.DT_FINPUBLICACION, 103) BETWEEN convert(datetime,convert(varchar(25),'" & valueColumn & " 00:00:00', 103), 103) AND convert(datetime,convert(varchar(25),'" & fecha_vencimiento_to & " 00:00:00', 103),103) "
				end if
				if columnName = "nroVacantesVsPostulantes" then
					if valueColumn = "Mayor" then
						sSQLCurrentDealsByCenter = sSQLCurrentDealsByCenter & " AND richiesta_sede.nro_vacantes > (SELECT count(DISTINCT(RICHIESTA_CANDIDATO_HIST.ID_PERSONA)) FROM RICHIESTA_CANDIDATO_HIST WHERE RICHIESTA_CANDIDATO_HIST.COD_ESITO_SEL IN (1,14) AND RICHIESTA_CANDIDATO_HIST.ID_RICHIESTA = richiesta_sede.id_richiesta)"
					end if
					if valueColumn = "Menor" then
						sSQLCurrentDealsByCenter = sSQLCurrentDealsByCenter & " AND richiesta_sede.nro_vacantes < (SELECT count(DISTINCT(RICHIESTA_CANDIDATO_HIST.ID_PERSONA)) FROM RICHIESTA_CANDIDATO_HIST WHERE RICHIESTA_CANDIDATO_HIST.COD_ESITO_SEL IN (1,14) AND RICHIESTA_CANDIDATO_HIST.ID_RICHIESTA = richiesta_sede.id_richiesta)"
					end if
					if valueColumn = "Igual" then
						sSQLCurrentDealsByCenter = sSQLCurrentDealsByCenter & " AND richiesta_sede.nro_vacantes = (SELECT count(DISTINCT(RICHIESTA_CANDIDATO_HIST.ID_PERSONA)) FROM RICHIESTA_CANDIDATO_HIST WHERE RICHIESTA_CANDIDATO_HIST.COD_ESITO_SEL IN (1,14) AND RICHIESTA_CANDIDATO_HIST.ID_RICHIESTA = richiesta_sede.id_richiesta)"
					end if
				end if
			end if
	  		
		next
		sSQLCurrentDealsByCenter = sSQLCurrentDealsByCenter & " Order by richiesta_sede.DT_FINPUBLICACION asc"

		generateQueryCurrentDealsByCenterExporttoExcel = sSQLCurrentDealsByCenter 
	end function

	function generateQueryExpiredDealsByCenterExporttoExcel(reportFilterDataBase64)
		sSQLExpiredDealsByCenter = ""
		sSQLExpiredDealsByCenter = "SELECT richiesta_sede.id_richiesta AS Referencia," &_ 
									"im.RAG_SOC+' ('+SEDE_IMPRESA.DESCRIZIONE+')' as Empleador," &_ 
									"richiesta_sede.offer_title as 'T�tulo de la Oferta'," &_
									"f.denominazione as 'Cargo Equivalente', a.denominazione as 'Grupo Ocupacional', " &_
									"richiesta_sede.nro_vacantes as 'Numero de Vacantes', " &_
										"(SELECT COUNT(RICHIESTA_CANDIDATO_HIST.ID_RICHIESTA) AS NUMERO " &_
										"FROM RICHIESTA_CANDIDATO_HIST " &_
										"WHERE RICHIESTA_CANDIDATO_HIST.COD_ESITO_SEL IN (1,14) " &_
										"AND RICHIESTA_CANDIDATO_HIST.ID_RICHIESTA = richiesta_sede.id_richiesta " &_
										"AND ( " &_
											"SELECT top 1 rch.DT_INS " &_
											"FROM RICHIESTA_CANDIDATO_HIST rch " &_
											"WHERE rch.ID_RICHIESTA = RICHIESTA_CANDIDATO_HIST.ID_RICHIESTA " &_
														"AND rch.ID_PERSONA = RICHIESTA_CANDIDATO_HIST.ID_PERSONA " &_
											"ORDER BY rch.DT_INS DESC " &_
										") = RICHIESTA_CANDIDATO_HIST.DT_INS " &_
									") AS 'Num Postulantes', " &_
									"(SELECT count(DISTINCT(RICHIESTA_CANDIDATO_HIST.ID_PERSONA)) " &_
										"FROM RICHIESTA_CANDIDATO_HIST " &_
										"WHERE RICHIESTA_CANDIDATO_HIST.COD_ESITO_SEL IN (1,14) " &_
										"AND RICHIESTA_CANDIDATO_HIST.ID_RICHIESTA = richiesta_sede.id_richiesta) 'Num Postulantes old'," &_
									" sim.DESCRIZIONE as 'Centro de Atenci�n', " &_
									" sim.ID_SEDE as 'Id Centro de Atenci�n', " &_
									"convert(varchar(25),richiesta_sede.DT_FINPUBLICACION, 103) as 'Fecha Vencimiento', " &_
									"convert(varchar(25),richiesta_sede.DT_INGRESO, 103) as 'Fecha Inicio', " &_
									"(SELECT COUNT(RICHIESTA_CANDIDATO_HIST.COD_ESITO_SEL) AS NUMERO " &_
										"FROM RICHIESTA_CANDIDATO_HIST " &_
										"WHERE RICHIESTA_CANDIDATO_HIST.COD_ESITO_SEL IN (1,14) " &_
												"AND RICHIESTA_CANDIDATO_HIST.COD_ESITO_SEL = 1 " &_
										"AND RICHIESTA_CANDIDATO_HIST.ID_RICHIESTA = richiesta_sede.id_richiesta  " &_
										"AND ( " &_
											"SELECT top 1 rch.DT_INS " &_
											"FROM RICHIESTA_CANDIDATO_HIST rch " &_
											"WHERE rch.ID_RICHIESTA = RICHIESTA_CANDIDATO_HIST.ID_RICHIESTA " &_
														"AND rch.ID_PERSONA = RICHIESTA_CANDIDATO_HIST.ID_PERSONA " &_
											"ORDER BY rch.DT_INS DESC " &_
										") = RICHIESTA_CANDIDATO_HIST.DT_INS " &_
										"GROUP BY CAST(RICHIESTA_CANDIDATO_HIST.COD_ESITO_SEL AS int) " &_
									") AS TOTAL_COD_UNO, " &_
									"(SELECT COUNT(RICHIESTA_CANDIDATO_HIST.COD_ESITO_SEL) AS NUMERO " &_
										"FROM RICHIESTA_CANDIDATO_HIST " &_
										"WHERE RICHIESTA_CANDIDATO_HIST.COD_ESITO_SEL IN (1,14) " &_
												"AND RICHIESTA_CANDIDATO_HIST.COD_ESITO_SEL = 14 " &_
										"AND RICHIESTA_CANDIDATO_HIST.ID_RICHIESTA = richiesta_sede.id_richiesta  " &_
										"AND ( " &_
											"SELECT top 1 rch.DT_INS " &_
											"FROM RICHIESTA_CANDIDATO_HIST rch " &_
											"WHERE rch.ID_RICHIESTA = RICHIESTA_CANDIDATO_HIST.ID_RICHIESTA " &_
														"AND rch.ID_PERSONA = RICHIESTA_CANDIDATO_HIST.ID_PERSONA " &_
											"ORDER BY rch.DT_INS DESC " &_
										") = RICHIESTA_CANDIDATO_HIST.DT_INS " &_
										"GROUP BY CAST(RICHIESTA_CANDIDATO_HIST.COD_ESITO_SEL AS int) " &_
									") AS TOTAL_COD_CATORCE " &_
									"FROM SEDE_IMPRESA " &_
									"inner join IMPRESA im on im.id_impresa = SEDE_IMPRESA.id_impresa " &_
									"inner join richiesta_sede on richiesta_sede.id_sede = SEDE_IMPRESA.id_sede " &_
									"inner join FIGUREPROFESSIONALI f on richiesta_sede.id_figprof = f.id_figprof " &_
									"inner join impresa_rolesserviciostipos irs ON richiesta_sede.servicio = irs.servicio " &_
									"inner join AREE_PROFESSIONALI a on a.id_areaprof = f.id_areaprof " &_
									"inner join SEDE_IMPRESA sim on sim.id_sede = richiesta_sede.COD_OFICINA " &_
									"WHERE	richiesta_sede.DT_finpublicacion < convert(datetime,CONVERT(varchar(10), GETDATE(), 103),103) AND richiesta_sede.FL_PUBBLICATO = 'S' " &_
										"AND richiesta_sede.cod_tipo_richiesta = 1 " &_
										"AND richiesta_sede.COD_OFICINA = " & session("Creator") & " "

		
		reportFilterDataSplit = Split(Base64Decode(reportFilterDataBase64),",")
		
		for i=0 to ubound(reportFilterDataSplit)	  			
	  		
	  		reportFilterData = Split(reportFilterDataSplit(i),":")
	  		columnName = reportFilterData(0)  
	  		valueColumn = reportFilterData(1)
	  		
			if valueColumn <> "" then
				if columnName = "empleador" then					
					sSQLExpiredDealsByCenter = sSQLExpiredDealsByCenter & " AND im.RAG_SOC LIKE '%" & valueColumn & "%' "
				end if
				if columnName = "titulo_oferta" then					
					sSQLExpiredDealsByCenter = sSQLExpiredDealsByCenter & " AND richiesta_sede.offer_title LIKE '%" & valueColumn & "%' "
				end if
				if columnName = "fecha_publicacion" then
					sSQLExpiredDealsByCenter = sSQLExpiredDealsByCenter & " AND convert(datetime,convert(varchar(25),richiesta_sede.DT_INGRESO, 103), 103) = convert(datetime,convert(varchar(25),'" & valueColumn & " 00:00:00', 103), 103) " 
				end if
				if columnName = "fecha_vencimiento_from" then	
					fecha_vencimiento_toSplit = Split(reportFilterDataSplit(i+1),":")
			  		fecha_vencimiento_to = fecha_vencimiento_toSplit(1)

					sSQLExpiredDealsByCenter = sSQLExpiredDealsByCenter & " AND convert(datetime,richiesta_sede.DT_FINPUBLICACION, 103) BETWEEN convert(datetime,convert(varchar(25),'" & valueColumn & " 00:00:00', 103), 103) AND convert(datetime,convert(varchar(25),'" & fecha_vencimiento_to & " 00:00:00', 103),103) "
				end if
				if columnName = "nroVacantesVsPostulantes" then
					if valueColumn = "Mayor" then
						sSQLExpiredDealsByCenter = sSQLExpiredDealsByCenter & " AND richiesta_sede.nro_vacantes > (SELECT count(DISTINCT(RICHIESTA_CANDIDATO_HIST.ID_PERSONA)) FROM RICHIESTA_CANDIDATO_HIST WHERE RICHIESTA_CANDIDATO_HIST.COD_ESITO_SEL IN (1,14) AND RICHIESTA_CANDIDATO_HIST.ID_RICHIESTA = richiesta_sede.id_richiesta)"
					end if
					if valueColumn = "Menor" then
						sSQLExpiredDealsByCenter = sSQLExpiredDealsByCenter & " AND richiesta_sede.nro_vacantes < (SELECT count(DISTINCT(RICHIESTA_CANDIDATO_HIST.ID_PERSONA)) FROM RICHIESTA_CANDIDATO_HIST WHERE RICHIESTA_CANDIDATO_HIST.COD_ESITO_SEL IN (1,14) AND RICHIESTA_CANDIDATO_HIST.ID_RICHIESTA = richiesta_sede.id_richiesta)"
					end if
					if valueColumn = "Igual" then
						sSQLExpiredDealsByCenter = sSQLExpiredDealsByCenter & " AND richiesta_sede.nro_vacantes = (SELECT count(DISTINCT(RICHIESTA_CANDIDATO_HIST.ID_PERSONA)) FROM RICHIESTA_CANDIDATO_HIST WHERE RICHIESTA_CANDIDATO_HIST.COD_ESITO_SEL IN (1,14) AND RICHIESTA_CANDIDATO_HIST.ID_RICHIESTA = richiesta_sede.id_richiesta)"
					end if
				end if
			end if
	  		
		next
		sSQLExpiredDealsByCenter = sSQLExpiredDealsByCenter & " Order by richiesta_sede.DT_FINPUBLICACION asc"

		generateQueryExpiredDealsByCenterExporttoExcel = sSQLExpiredDealsByCenter 
	end function

	function generateQueryRegisteredCompaniesByCenterExporttoExcel(reportFilterDataBase64)

		sSQLRegisteredCompaniesByCenter = "SELECT i.RAG_SOC as 'NOMBRE DE LA EMPRESA', " &_ 
								"si.DESCRIZIONE as SEDE, " &_ 
								"dep.DESCRIZIONE AS 'DEPARTAMENTO', " &_
								"Ciudad.DESCOM AS 'CIUDAD'," &_
								"si.E_MAIL_SEDE as 'Correo Electr�nico', " &_
								"si.NUM_TEL_SEDE as 'N�mero Telef�nico 1', " &_
								"si.NUM_TEL as 'N�mero Telef�nico 2', " &_
								"si.indirizzo as 'Direcci�n', " &_
								"convert(varchar(25),i.DT_TMST, 103) as 'FECHA DE INSCRIPCI�N', " &_
								"cs.NOMBRE_CONTACTO + ' ' + cs.APELLIDO_CONTACTO AS 'Persona de Contacto', " &_ 
								"cs.num_tel as 'Tel�fono del Contacto', " &_ 
								"cs.email_contacto as 'E-mail del Contacto' " &_	
								"FROM sede_impresa si WITH(nolock) " &_
								"INNER JOIN (SELECT DISTINCT id_impresa, rol FROM impresa_rolesservicios WITH(nolock)) sri ON sri.id_impresa = si.id_impresa " &_
								"INNER JOIN IMPRESA I ON si.ID_IMPRESA =i.ID_IMPRESA " &_
								"INNER JOIN (select CODICE, DESCRIZIONE from tades where nome_tabella = 'PROV') Dep ON si.PRV =dep.CODICE " &_
								"INNER JOIN (SELECT CODCOM,DESCOM from COMUNE) Ciudad ON si.COMUNE=Ciudad.CODCOM " &_
								"LEFT JOIN CONTACTO_SEDE CS ON CS.id_sede = si.id_sede " &_
								"WHERE sri.rol = 1 "  &_		
									"AND si.id_cimpiego = " & session("Creator") & " "

		reportFilterDataSplit = Split(Base64Decode(reportFilterDataBase64),",")
		
		for i=0 to ubound(reportFilterDataSplit)	  			
	  		
	  		reportFilterData = Split(reportFilterDataSplit(i),":")
	  		columnName = reportFilterData(0)  
	  		valueColumn = reportFilterData(1)


			if valueColumn <> "" then

				if columnName = "nombre_empresa" then
					sSQLRegisteredCompaniesByCenter = sSQLRegisteredCompaniesByCenter & "AND i.RAG_SOC LIKE '%" & valueColumn & "%' "
				end if
				if columnName = "sede_empresa" then
					sSQLRegisteredCompaniesByCenter = sSQLRegisteredCompaniesByCenter & "AND si.DESCRIZIONE LIKE '%" & valueColumn & "%' "
				end if
				if columnName = "departamento"then
					sSQLRegisteredCompaniesByCenter = sSQLRegisteredCompaniesByCenter & "AND dep.CODICE = '" & valueColumn & "' "
				end if
				if columnName = "ciudad" then
					sSQLRegisteredCompaniesByCenter = sSQLRegisteredCompaniesByCenter & "AND Ciudad.CODCOM = '" & valueColumn & "' "
				end if
				if columnName = "fecha_inscripcion_from" then		
					fecha_vencimiento_toSplit = Split(reportFilterDataSplit(i+1),":")
				  	fecha_vencimiento_to = fecha_vencimiento_toSplit(1)			
				  	
					sSQLRegisteredCompaniesByCenter = sSQLRegisteredCompaniesByCenter & " AND convert(datetime,i.DT_TMST, 103) BETWEEN convert(datetime,convert(varchar(25),'" & valueColumn & " 00:00:00', 103), 103) AND convert(datetime,convert(varchar(25),'" & fecha_vencimiento_to & " 00:00:00', 103),103) " 
				end if

			end if

		next
					
		sSQLRegisteredCompaniesByCenter = sSQLRegisteredCompaniesByCenter & "ORDER BY si.ID_SEDE"

		'response.Write sSQLRegisteredCompaniesByCenter

		generateQueryRegisteredCompaniesByCenterExporttoExcel = sSQLRegisteredCompaniesByCenter
	end function

	function generateQueryPostudEstadExporttoExcel(reportFilterDataBase64)


		sSQLPostuEstad ="SELECT rs.ID_RICHIESTA as 'vacante', COD_FISC as 'IDENTIFICACION', FLOOR(DATEDIFF(month, A.DT_NASC, GETDATE()) / CONVERT(FLOAT, 12)) AS Edad, " &_
			    "(select DESCRIZIONE from TADES where NOME_TABELLA = 'PROV' AND CODICE = a.PRV_RES ) AS 'Departamento', " &_
			    "(select DESCOM from COMUNE where CODCOM = a.COM_RES ) AS 'Ciudad', " &_
			    " A.nome + ' ' + A.cognome as 'Nombre', " &_
			    "A.frazione_res as Barrio, A.ind_res as direccion, A.num_tel_dom as telefono,  " &_
			    "(select DESCRIZIONE from TADES where NOME_TABELLA = 'STDIS' AND CODICE = C.COD_STDIS ) AS 'situacionlaboral', " &_
			    "CONVERT(VARCHAR(23), D.DT_INS, 103) AS 'fechapostulacion',  " &_
			    "(select DESCRIZIONE from TADES  " &_
			    " where NOME_TABELLA = 'essel' " &_
			    " AND CODICE = D.COD_ESITO_SEL " &_
			    ") AS 'ESTADOCONTACTO', " &_
			    "LOWER(isnull((Select top 1 ta.DESCRIZIONE From TISTUD ti inner join tades ta on ti.COD_LIV_STUD = ta.CODICE where id_persona = a.id_persona and ta.NOME_TABELLA = 'LSTUD' Order by AA_stud desc, dt_tmst desc),'-')) 'NivelEducativo',  " &_
				"RS.OFFER_TITLE AS 'TITOFERTA'  " &_
				"FROM PERSONA A  " &_
				"LEFT OUTER JOIN STATO_OCCUPAZIONALE C ON A.ID_PERSONA = C.ID_PERSONA AND C.IND_STATUS = 0  " &_
				"INNER JOIN RICHIESTA_CANDIDATO D ON D.ID_PERSONA = A.ID_PERSONA " &_
				"INNER JOIN RICHIESTA_SEDE RS ON RS.ID_RICHIESTA = D.ID_RICHIESTA  " &_
				"WHERE  D.ID_PERSONA = A.ID_PERSONA " &_
				"and A.ID_PERSONA not in (select ID_PERSONA from PERSONAS_JOB_PROFILE where AUTHORIZATIONOFDATAVISUALIZATION =3 ) " 

		reportFilterDataSplit = Split(Base64Decode(reportFilterDataBase64),",")
		fecini = ""		
		for i=0 to ubound(reportFilterDataSplit)	  			
	  		
	  		reportFilterData = Split(reportFilterDataSplit(i),":")
	  		columnName = reportFilterData(0)  
	  		valueColumn = reportFilterData(1)
			

			if valueColumn <> "" then
			
				if columnName = "doc_identidad" then
					sSQLPostuEstad = sSQLPostuEstad & "AND cod_fisc LIKE '%" & valueColumn & "%' "
				end if
				if columnName = "nombre" then
					sSQLPostuEstad = sSQLPostuEstad & "AND A.nome + ' ' + A.cognome LIKE '%" & valueColumn & "%' "
				end if				
				if columnName = "vacante"then
					sSQLPostuEstad = sSQLPostuEstad & "AND D.COD_ESITO_SEL = " & valueColumn & " "
				end if				
				if columnName = "estado"then
					sSQLPostuEstad = sSQLPostuEstad & "AND D.COD_ESITO_SEL = '" & valueColumn & "%' "
				end if		
				if columnName = "centro_atencion"then
					sSQLPostuEstad = sSQLPostuEstad & " AND RS.COD_OFICINA = " & valueColumn 
				end if
				if columnName = "fecini" then
					fecini =  valueColumn 
				end if
				if columnName = "fecfin" then
					sSQLPostuEstad = sSQLPostuEstad & " and convert(datetime,convert(varchar(25),DT_INS, 103), 103) >= convert(datetime,convert(varchar(25),'" & fecini & " 00:00:00', 103), 103) and " &_
					            " convert(datetime,convert(varchar(25),DT_INS, 103), 103) <= convert(datetime,convert(varchar(25),'" & valueColumn & " 00:00:00', 103), 103) " 					
				end if				
			end if

		next

					
		sSQLPostuEstad = sSQLPostuEstad & " ORDER BY IDENTIFICACION"

		'response.Write sSQLPostuEstad&"   <----Br/>"
		'response.end

		generateQueryPostudEstadExporttoExcel = sSQLPostuEstad

	end function
	
	function generateQueryOferenSinCentroExporttoExcel(reportFilterDataBase64)


		sSQLOferenSinCentro ="select cod_fisc, cognome, nome, isnull(DESCRIZIONE,'NO REGISTRA') DESCRIZIONE, ISNULL(DESCOM,'NO REGISTRA') DESCOM, NUM_TEL, E_MAIL   " &_
				"from ( " &_
				"select id_persona, cod_fisc, cognome, nome, com_res, prv_res, NUM_TEL, E_MAIL " &_
				"	from persona as a " &_
				"where not EXISTS ( select * " &_
				"													from STATO_OCCUPAZIONALE as u " &_
				"												 where u.id_persona = a.id_persona)) as per " &_
				"LEFT JOIN (select CODICE, DESCRIZIONE from tades where nome_tabella = 'PROV') as Dep ON per.prv_res = Dep.CODICE " &_
				"LEFT JOIN (SELECT CODCOM,DESCOM from COMUNE) as Ciudad ON per.com_res=Ciudad.CODCOM " 

		reportFilterDataSplit = Split(Base64Decode(reportFilterDataBase64),",")
		ban = false
		for i=0 to ubound(reportFilterDataSplit)	  			
	  		
	  		reportFilterData = Split(reportFilterDataSplit(i),":")
	  		columnName = reportFilterData(0)  
	  		valueColumn = reportFilterData(1)


			if valueColumn <> "" then
				if not ban then
					sSQLOferenSinCentro = sSQLOferenSinCentro & " where "
				end if
				if columnName = "documento" then
					sSQLOferenSinCentro = sSQLOferenSinCentro & " per.COD_FISC LIKE '%" & valueColumn & "%' "
					ban = true
				end if
				if columnName = "nombre" then
				  if ban then
					sSQLOferenSinCentro = sSQLOferenSinCentro & "AND per.nome LIKE '%" & valueColumn & "%' "
				  else
				    sSQLOferenSinCentro = sSQLOferenSinCentro & " per.nome LIKE '%" & valueColumn & "%' "
				  end if
				  ban = true
				end if
				if columnName = "apellido"then
				  if ban then
					sSQLOferenSinCentro = sSQLOferenSinCentro & "AND per.cognome like '%" & valueColumn & "%' "
				  else 
					sSQLOferenSinCentro = sSQLOferenSinCentro & " per.cognome like '%" & valueColumn & "%' "
			      end if
				  ban = true
				end if				
				if columnName = "departamento"then
				  if ban then
					sSQLOferenSinCentro = sSQLOferenSinCentro & "AND per.prv_res = " & valueColumn & " "
				  else
				    sSQLOferenSinCentro = sSQLOferenSinCentro & " per.prv_res = " & valueColumn & " "
				  end if 
				  ban = true
				end if				
				if columnName = "ciudad"then
				  if ban then
					sSQLOferenSinCentro = sSQLOferenSinCentro & "AND per.com_res = '" & valueColumn & "' "
				  else 
				    sSQLOferenSinCentro = sSQLOferenSinCentro & " per.com_res = '" & valueColumn & "' "
				  end if
				end if		

			end if

		next

					
		sSQLOferenSinCentro = sSQLOferenSinCentro & "ORDER BY PER.COGNOME"
		
		'Response.Write sSQLOferenSinCentro
		'Response.End

		'response.Write sSQLRegisteredCompaniesByCenter

		generateQueryOferenSinCentroExporttoExcel = sSQLOferenSinCentro

	end function
	
	function generateQueryListadoCarEquivalenteExporttoExcel()

		sSQLlistadoCarEquivalente =" select * from figureprofessionali order by denominazione" 
		
		'Response.Write sSQLOferenSinCentro
		'Response.End

		'response.Write sSQLRegisteredCompaniesByCenter

		generateQueryListadoCarEquivalenteExporttoExcel = sSQLlistadoCarEquivalente

	end function
	
	function generateQueryListadoRamasActExporttoExcel()

		sSQLlistadoRamasAct =" select * from settori order by denominazione" 
		
		'Response.Write sSQLOferenSinCentro
		'Response.End

		'response.Write sSQLRegisteredCompaniesByCenter

		generateQueryListadoRamasActExporttoExcel = sSQLlistadoRamasAct

	end function
	
	function generateQueryListadoNivelEducativoExporttoExcel()

		sSQLlistadoNivelEducativo ="select * from (  " &_
									"select case valore " &_
									"	   when 'TLA' then 'T�CNICA LABORAL' " &_
									"	   when 'CBU' then 'T�CNICA PROFESIONAL' " &_
									"	   when 'POL' then 'TECNOL�GICA' " &_ 
									"	   when 'NBC' then 'UNIVERSITARIA' " &_
									"	   when 'ESP' then 'ESPECIALIZACI�N' " &_
									"	   when 'MAE' then 'MAESTR�A' " &_
									"	   when 'DOC' then 'DOCTORADO'  " &_
									"	   end categoria, " &_
									"	   descrizione " &_
									"  from tades  " &_
									" where nome_tabella = 'TSTUD' " &_
									"   and valore is not null " &_
									"   and descrizione is not null " &_
									" group by valore, descrizione " &_
									" ) as r where r.categoria is not null " &_
									" order by categoria,descrizione " 
		
		'Response.Write sSQLOferenSinCentro
		'Response.End

		'response.Write sSQLRegisteredCompaniesByCenter

		generateQueryListadoNivelEducativoExporttoExcel = sSQLlistadoNivelEducativo

	end function
	
	
	function generateQueryListadoGrupoOcupacionalExporttoExcel()

		sSQLlistadoGrupoOcupacional =" select * from aree_professionali order by denominazione " 
		
		'Response.Write sSQLOferenSinCentro
		'Response.End

		'response.Write sSQLRegisteredCompaniesByCenter

		generateQueryListadoGrupoOcupacionalExporttoExcel = sSQLlistadoGrupoOcupacional

	end function
	
	function generateQueryEntrevistasExporttoExcel(reportFilterDataBase64)

		sSQLEntrevistas ="Select 	CUIL Identificacion,  " &_
				"U.nome + '-' + U.cognome as 'Entrevistador', " &_
				"ENTREVISTANRO as 'Entrevista', " &_
				"CITANRO , " &_ 
				"agEE.Descripcion as ESTADO, " &_
				"agEt.Descripcion as TAREA, " &_
				"convert(varchar,agE.FECHAALTA,103) + ' ' + convert(varchar,agE.FECHAALTA,108) as 'FECINICIO', " &_
				"FECHAULTMOD " &_
				"From agEntrevistas agE with(nolock) " &_
				"Left Outer Join agEntrevistaEstados agEE with(nolock) " &_
				"On agEE.Estado = agE.Estado " &_
				"Left Outer join agEntrevistaTareas agET with(nolock) " &_
				"On agET.Tarea = agE.TareaUlt " &_
				"Left Outer join PLavoro.dbo.utente U with(nolock) " &_
				"On u.IDUtente = agE.Usuario WHERE "

		reportFilterDataSplit = Split(Base64Decode(reportFilterDataBase64),",")
		fecini = ""		
		ban = false
		for i=0 to ubound(reportFilterDataSplit)	  			
	  		
	  		reportFilterData = Split(reportFilterDataSplit(i),":")
	  		columnName = reportFilterData(0)  
	  		valueColumn = reportFilterData(1)


			if valueColumn <> "" then
				if columnName = "fecini" then
					fecini =  valueColumn 
				end if
				if columnName = "fecfin" then
					if ban then
						sSQLEntrevistas = sSQLEntrevistas & " and convert(datetime,convert(varchar(25),agE.FECHAALTA, 103), 103) >= convert(datetime,convert(varchar(25),'" & fecini & " 00:00:00', 103), 103) and " &_
					            " convert(datetime,convert(varchar(25),agE.FECHAALTA, 103), 103) <= convert(datetime,convert(varchar(25),'" & valueColumn & " 00:00:00', 103), 103) "
					else
						sSQLEntrevistas = sSQLEntrevistas & " convert(datetime,convert(varchar(25),agE.FECHAALTA, 103), 103) >= convert(datetime,convert(varchar(25),'" & fecini & " 00:00:00', 103), 103) and " &_
					            " convert(datetime,convert(varchar(25),agE.FECHAALTA, 103), 103) <= convert(datetime,convert(varchar(25),'" & valueColumn & " 00:00:00', 103), 103) "
					end if
				end if
				if columnName = "centro_atencion" then
					sSQLEntrevistas = sSQLEntrevistas & " oficina = " & valueColumn
					ban = true
				end if	
			end if

		next

					
		sSQLEntrevistas = sSQLEntrevistas & "ORDER BY ENTREVISTANRO"
		
		'Response.Write sSQLOferenSinCentro
		'Response.End

		'response.Write sSQLRegisteredCompaniesByCenter

		generateQueryEntrevistasExporttoExcel = sSQLEntrevistas

	end function

%>