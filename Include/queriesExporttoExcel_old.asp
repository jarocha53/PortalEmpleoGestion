<!-- #include virtual="/include/Base64.asp" -->
<%

	function generateQueryCurrentDealsExporttoExcel(reportFilterDataBase64)

		sSQLCurrentDeals = ""
		sSQLCurrentDeals = "SELECT richiesta_sede.id_richiesta AS Referencia," &_ 
					"im.RAG_SOC+' ('+SEDE_IMPRESA.DESCRIZIONE+')' as Empleador," &_ 
					"richiesta_sede.offer_title as 'T�tulo de la Oferta'," &_
					"f.denominazione as 'Cargo Equivalente', a.denominazione as 'Grupo Ocupacional', " &_
					"richiesta_sede.nro_vacantes as 'Numero de Vacantes', " &_
					"(SELECT COUNT(RICHIESTA_CANDIDATO_HIST.ID_RICHIESTA) AS NUMERO " &_
						"FROM RICHIESTA_CANDIDATO_HIST " &_
						"WHERE RICHIESTA_CANDIDATO_HIST.COD_ESITO_SEL IN (1,14) " &_
						"AND RICHIESTA_CANDIDATO_HIST.ID_RICHIESTA = richiesta_sede.id_richiesta " &_
						"AND ( " &_
							"SELECT top 1 rch.DT_INS " &_
							"FROM RICHIESTA_CANDIDATO_HIST rch " &_
							"WHERE rch.ID_RICHIESTA = RICHIESTA_CANDIDATO_HIST.ID_RICHIESTA " &_
										"AND rch.ID_PERSONA = RICHIESTA_CANDIDATO_HIST.ID_PERSONA " &_
							"ORDER BY rch.DT_INS DESC " &_
						") = RICHIESTA_CANDIDATO_HIST.DT_INS " &_
					") AS 'Num Postulantes', " &_
					"(SELECT count(DISTINCT(RICHIESTA_CANDIDATO_HIST.ID_PERSONA)) " &_
						"FROM RICHIESTA_CANDIDATO_HIST " &_
						"WHERE RICHIESTA_CANDIDATO_HIST.COD_ESITO_SEL IN (1,14) " &_
						"AND RICHIESTA_CANDIDATO_HIST.ID_RICHIESTA = richiesta_sede.id_richiesta) 'Num Postulantes old'," &_
					" sim.DESCRIZIONE as 'Centro de Atenci�n', " &_
					" sim.ID_SEDE as 'Id Centro de Atenci�n', " &_
					"convert(varchar(25),richiesta_sede.DT_FINPUBLICACION, 103) as 'Fecha Vencimiento', " &_
					"convert(varchar(25),richiesta_sede.DT_INGRESO, 103) as 'Fecha Inicio', " &_
					"(SELECT COUNT(RICHIESTA_CANDIDATO_HIST.COD_ESITO_SEL) AS NUMERO " &_
						"FROM RICHIESTA_CANDIDATO_HIST " &_
						"WHERE RICHIESTA_CANDIDATO_HIST.COD_ESITO_SEL IN (1,14) " &_
								"AND RICHIESTA_CANDIDATO_HIST.COD_ESITO_SEL = 1 " &_
						"AND RICHIESTA_CANDIDATO_HIST.ID_RICHIESTA = richiesta_sede.id_richiesta  " &_
						"AND ( " &_
							"SELECT top 1 rch.DT_INS " &_
							"FROM RICHIESTA_CANDIDATO_HIST rch " &_
							"WHERE rch.ID_RICHIESTA = RICHIESTA_CANDIDATO_HIST.ID_RICHIESTA " &_
										"AND rch.ID_PERSONA = RICHIESTA_CANDIDATO_HIST.ID_PERSONA " &_
							"ORDER BY rch.DT_INS DESC " &_
						") = RICHIESTA_CANDIDATO_HIST.DT_INS " &_
						"GROUP BY CAST(RICHIESTA_CANDIDATO_HIST.COD_ESITO_SEL AS int) " &_
					") AS TOTAL_COD_UNO, " &_
					"(SELECT COUNT(RICHIESTA_CANDIDATO_HIST.COD_ESITO_SEL) AS NUMERO " &_
						"FROM RICHIESTA_CANDIDATO_HIST " &_
						"WHERE RICHIESTA_CANDIDATO_HIST.COD_ESITO_SEL IN (1,14) " &_
								"AND RICHIESTA_CANDIDATO_HIST.COD_ESITO_SEL = 14 " &_
						"AND RICHIESTA_CANDIDATO_HIST.ID_RICHIESTA = richiesta_sede.id_richiesta  " &_
						"AND ( " &_
							"SELECT top 1 rch.DT_INS " &_
							"FROM RICHIESTA_CANDIDATO_HIST rch " &_
							"WHERE rch.ID_RICHIESTA = RICHIESTA_CANDIDATO_HIST.ID_RICHIESTA " &_
										"AND rch.ID_PERSONA = RICHIESTA_CANDIDATO_HIST.ID_PERSONA " &_
							"ORDER BY rch.DT_INS DESC " &_
						") = RICHIESTA_CANDIDATO_HIST.DT_INS " &_
						"GROUP BY CAST(RICHIESTA_CANDIDATO_HIST.COD_ESITO_SEL AS int) " &_
					") AS TOTAL_COD_CATORCE " &_
					"FROM SEDE_IMPRESA " &_
					"inner join IMPRESA im on im.id_impresa = SEDE_IMPRESA.id_impresa " &_
					"inner join richiesta_sede on richiesta_sede.id_sede = SEDE_IMPRESA.id_sede " &_
					"inner join FIGUREPROFESSIONALI f on richiesta_sede.id_figprof = f.id_figprof " &_
					"inner join impresa_rolesserviciostipos irs ON richiesta_sede.servicio = irs.servicio " &_
					"inner join AREE_PROFESSIONALI a on a.id_areaprof = f.id_areaprof " &_
					"inner join SEDE_IMPRESA sim on sim.id_sede = richiesta_sede.COD_OFICINA " &_
					"WHERE	richiesta_sede.DT_finpublicacion > = convert(datetime,CONVERT(varchar(10), GETDATE(), 103),103) AND richiesta_sede.FL_PUBBLICATO = 'S' " &_
						"AND richiesta_sede.cod_tipo_richiesta = 1 "

		
		reportFilterDataSplit = Split(Base64Decode(reportFilterDataBase64),",")
		
		for i=0 to ubound(reportFilterDataSplit)	  			
	  		
	  		reportFilterData = Split(reportFilterDataSplit(i),":")
	  		columnName = reportFilterData(0)  
	  		valueColumn = reportFilterData(1)
	  		
			if valueColumn <> "" then
				if columnName = "empleador" then					
					sSQLCurrentDeals = sSQLCurrentDeals & " AND im.RAG_SOC LIKE '%" & valueColumn & "%' "
				end if
				if columnName = "titulo_oferta" then					
					sSQLCurrentDeals = sSQLCurrentDeals & " AND richiesta_sede.offer_title LIKE '%" & valueColumn & "%' "
				end if
				if columnName = "centro_atencion" then					
					sSQLCurrentDeals = sSQLCurrentDeals & " AND richiesta_sede.COD_OFICINA = " & valueColumn
				end if
				if columnName = "fecha_publicacion" then
					sSQLCurrentDeals = sSQLCurrentDeals & " AND convert(datetime,convert(varchar(25),richiesta_sede.DT_INGRESO, 103), 103) = convert(datetime,convert(varchar(25),'" & valueColumn & " 00:00:00', 103), 103) " 
				end if
				if columnName = "fecha_vencimiento_from" then	
					fecha_vencimiento_toSplit = Split(reportFilterDataSplit(i+1),":")
			  		fecha_vencimiento_to = fecha_vencimiento_toSplit(1)

					sSQLCurrentDeals = sSQLCurrentDeals & " AND convert(datetime,richiesta_sede.DT_FINPUBLICACION, 103) BETWEEN convert(datetime,convert(varchar(25),'" & valueColumn & " 00:00:00', 103), 103) AND convert(datetime,convert(varchar(25),'" & fecha_vencimiento_to & " 00:00:00', 103),103) "
				end if
				if columnName = "nroVacantesVsPostulantes" then
					if valueColumn = "Mayor" then
						sSQLCurrentDeals = sSQLCurrentDeals & " AND richiesta_sede.nro_vacantes > (SELECT count(DISTINCT(RICHIESTA_CANDIDATO_HIST.ID_PERSONA)) FROM RICHIESTA_CANDIDATO_HIST WHERE RICHIESTA_CANDIDATO_HIST.COD_ESITO_SEL IN (1,14) AND RICHIESTA_CANDIDATO_HIST.ID_RICHIESTA = richiesta_sede.id_richiesta)"
					end if
					if valueColumn = "Menor" then
						sSQLCurrentDeals = sSQLCurrentDeals & " AND richiesta_sede.nro_vacantes < (SELECT count(DISTINCT(RICHIESTA_CANDIDATO_HIST.ID_PERSONA)) FROM RICHIESTA_CANDIDATO_HIST WHERE RICHIESTA_CANDIDATO_HIST.COD_ESITO_SEL IN (1,14) AND RICHIESTA_CANDIDATO_HIST.ID_RICHIESTA = richiesta_sede.id_richiesta)"
					end if
					if valueColumn = "Igual" then
						sSQLCurrentDeals = sSQLCurrentDeals & " AND richiesta_sede.nro_vacantes = (SELECT count(DISTINCT(RICHIESTA_CANDIDATO_HIST.ID_PERSONA)) FROM RICHIESTA_CANDIDATO_HIST WHERE RICHIESTA_CANDIDATO_HIST.COD_ESITO_SEL IN (1,14) AND RICHIESTA_CANDIDATO_HIST.ID_RICHIESTA = richiesta_sede.id_richiesta)"
					end if
				end if
			end if
	  		
		next
		sSQLCurrentDeals = sSQLCurrentDeals & " Order by richiesta_sede.DT_FINPUBLICACION asc"

		generateQueryCurrentDealsExporttoExcel = sSQLCurrentDeals 
	end function


	function generateQueryExpiredDealsExporttoExcel(reportFilterDataBase64)

		sSQLExpiredDeals = ""
		sSQLExpiredDeals = "SELECT richiesta_sede.id_richiesta AS Referencia," &_ 
					"im.RAG_SOC+' ('+SEDE_IMPRESA.DESCRIZIONE+')' as Empleador," &_ 
					"richiesta_sede.offer_title as 'T�tulo de la Oferta'," &_
					"f.denominazione as 'Cargo Equivalente', a.denominazione as 'Grupo Ocupacional', " &_
					"richiesta_sede.nro_vacantes as 'Numero de Vacantes', " &_
					"(SELECT COUNT(RICHIESTA_CANDIDATO_HIST.ID_RICHIESTA) AS NUMERO " &_
						"FROM RICHIESTA_CANDIDATO_HIST " &_
						"WHERE RICHIESTA_CANDIDATO_HIST.COD_ESITO_SEL IN (1,14) " &_
						"AND RICHIESTA_CANDIDATO_HIST.ID_RICHIESTA = richiesta_sede.id_richiesta " &_
						"AND ( " &_
							"SELECT top 1 rch.DT_INS " &_
							"FROM RICHIESTA_CANDIDATO_HIST rch " &_
							"WHERE rch.ID_RICHIESTA = RICHIESTA_CANDIDATO_HIST.ID_RICHIESTA " &_
										"AND rch.ID_PERSONA = RICHIESTA_CANDIDATO_HIST.ID_PERSONA " &_
							"ORDER BY rch.DT_INS DESC " &_
						") = RICHIESTA_CANDIDATO_HIST.DT_INS " &_
					") AS 'Num Postulantes', " &_
					"(SELECT count(DISTINCT(RICHIESTA_CANDIDATO_HIST.ID_PERSONA)) " &_
						"FROM RICHIESTA_CANDIDATO_HIST " &_
						"WHERE RICHIESTA_CANDIDATO_HIST.COD_ESITO_SEL IN (1,14) " &_
						"AND RICHIESTA_CANDIDATO_HIST.ID_RICHIESTA = richiesta_sede.id_richiesta) 'Num Postulantes old'," &_
					" sim.DESCRIZIONE as 'Centro de Atenci�n', " &_
					" sim.ID_SEDE as 'Id Centro de Atenci�n', " &_
					"convert(varchar(25),richiesta_sede.DT_FINPUBLICACION, 103) as 'Fecha Vencimiento', " &_
					"convert(varchar(25),richiesta_sede.DT_INGRESO, 103) as 'Fecha Inicio', " &_
					"(SELECT COUNT(RICHIESTA_CANDIDATO_HIST.COD_ESITO_SEL) AS NUMERO " &_
						"FROM RICHIESTA_CANDIDATO_HIST " &_
						"WHERE RICHIESTA_CANDIDATO_HIST.COD_ESITO_SEL IN (1,14) " &_
								"AND RICHIESTA_CANDIDATO_HIST.COD_ESITO_SEL = 1 " &_
						"AND RICHIESTA_CANDIDATO_HIST.ID_RICHIESTA = richiesta_sede.id_richiesta  " &_
						"AND ( " &_
							"SELECT top 1 rch.DT_INS " &_
							"FROM RICHIESTA_CANDIDATO_HIST rch " &_
							"WHERE rch.ID_RICHIESTA = RICHIESTA_CANDIDATO_HIST.ID_RICHIESTA " &_
										"AND rch.ID_PERSONA = RICHIESTA_CANDIDATO_HIST.ID_PERSONA " &_
							"ORDER BY rch.DT_INS DESC " &_
						") = RICHIESTA_CANDIDATO_HIST.DT_INS " &_
						"GROUP BY CAST(RICHIESTA_CANDIDATO_HIST.COD_ESITO_SEL AS int) " &_
					") AS TOTAL_COD_UNO, " &_
					"(SELECT COUNT(RICHIESTA_CANDIDATO_HIST.COD_ESITO_SEL) AS NUMERO " &_
						"FROM RICHIESTA_CANDIDATO_HIST " &_
						"WHERE RICHIESTA_CANDIDATO_HIST.COD_ESITO_SEL IN (1,14) " &_
								"AND RICHIESTA_CANDIDATO_HIST.COD_ESITO_SEL = 14 " &_
						"AND RICHIESTA_CANDIDATO_HIST.ID_RICHIESTA = richiesta_sede.id_richiesta  " &_
						"AND ( " &_
							"SELECT top 1 rch.DT_INS " &_
							"FROM RICHIESTA_CANDIDATO_HIST rch " &_
							"WHERE rch.ID_RICHIESTA = RICHIESTA_CANDIDATO_HIST.ID_RICHIESTA " &_
										"AND rch.ID_PERSONA = RICHIESTA_CANDIDATO_HIST.ID_PERSONA " &_
							"ORDER BY rch.DT_INS DESC " &_
						") = RICHIESTA_CANDIDATO_HIST.DT_INS " &_
						"GROUP BY CAST(RICHIESTA_CANDIDATO_HIST.COD_ESITO_SEL AS int) " &_
					") AS TOTAL_COD_CATORCE " &_
					"FROM SEDE_IMPRESA " &_
					"inner join IMPRESA im on im.id_impresa = SEDE_IMPRESA.id_impresa " &_
					"inner join richiesta_sede on richiesta_sede.id_sede = SEDE_IMPRESA.id_sede " &_
					"inner join FIGUREPROFESSIONALI f on richiesta_sede.id_figprof = f.id_figprof " &_
					"inner join impresa_rolesserviciostipos irs ON richiesta_sede.servicio = irs.servicio " &_
					"inner join AREE_PROFESSIONALI a on a.id_areaprof = f.id_areaprof " &_
					"inner join SEDE_IMPRESA sim on sim.id_sede = richiesta_sede.COD_OFICINA " &_
					"WHERE	richiesta_sede.DT_finpublicacion < convert(datetime,CONVERT(varchar(10), GETDATE(), 103),103) AND richiesta_sede.FL_PUBBLICATO = 'S' " &_
						"AND richiesta_sede.cod_tipo_richiesta = 1 "

		
		reportFilterDataSplit = Split(Base64Decode(reportFilterDataBase64),",")
		
		for i=0 to ubound(reportFilterDataSplit)	  			
	  		
	  		reportFilterData = Split(reportFilterDataSplit(i),":")
	  		columnName = reportFilterData(0)  
	  		valueColumn = reportFilterData(1)
	  		
			if valueColumn <> "" then
				if columnName = "empleador" then					
					sSQLExpiredDeals = sSQLExpiredDeals & " AND im.RAG_SOC LIKE '%" & valueColumn & "%' "
				end if
				if columnName = "titulo_oferta" then					
					sSQLExpiredDeals = sSQLExpiredDeals & " AND richiesta_sede.offer_title LIKE '%" & valueColumn & "%' "
				end if
				if columnName = "centro_atencion" then					
					sSQLExpiredDeals = sSQLExpiredDeals & " AND richiesta_sede.COD_OFICINA = " & valueColumn
				end if
				if columnName = "fecha_publicacion" then
					sSQLExpiredDeals = sSQLExpiredDeals & " AND convert(datetime,convert(varchar(25),richiesta_sede.DT_INGRESO, 103), 103) = convert(datetime,convert(varchar(25),'" & valueColumn & " 00:00:00', 103), 103) " 
				end if
				if columnName = "fecha_vencimiento_from" then	
					fecha_vencimiento_toSplit = Split(reportFilterDataSplit(i+1),":")
			  		fecha_vencimiento_to = fecha_vencimiento_toSplit(1)

					sSQLExpiredDeals = sSQLExpiredDeals & " AND convert(datetime,richiesta_sede.DT_FINPUBLICACION, 103) BETWEEN convert(datetime,convert(varchar(25),'" & valueColumn & " 00:00:00', 103), 103) AND convert(datetime,convert(varchar(25),'" & fecha_vencimiento_to & " 00:00:00', 103),103) "
				end if
				if columnName = "nroVacantesVsPostulantes" then
					if valueColumn = "Mayor" then
						sSQLExpiredDeals = sSQLExpiredDeals & " AND richiesta_sede.nro_vacantes > (SELECT count(DISTINCT(RICHIESTA_CANDIDATO_HIST.ID_PERSONA)) FROM RICHIESTA_CANDIDATO_HIST WHERE RICHIESTA_CANDIDATO_HIST.COD_ESITO_SEL IN (1,14) AND RICHIESTA_CANDIDATO_HIST.ID_RICHIESTA = richiesta_sede.id_richiesta)"
					end if
					if valueColumn = "Menor" then
						sSQLExpiredDeals = sSQLExpiredDeals & " AND richiesta_sede.nro_vacantes < (SELECT count(DISTINCT(RICHIESTA_CANDIDATO_HIST.ID_PERSONA)) FROM RICHIESTA_CANDIDATO_HIST WHERE RICHIESTA_CANDIDATO_HIST.COD_ESITO_SEL IN (1,14) AND RICHIESTA_CANDIDATO_HIST.ID_RICHIESTA = richiesta_sede.id_richiesta)"
					end if
					if valueColumn = "Igual" then
						sSQLExpiredDeals = sSQLExpiredDeals & " AND richiesta_sede.nro_vacantes = (SELECT count(DISTINCT(RICHIESTA_CANDIDATO_HIST.ID_PERSONA)) FROM RICHIESTA_CANDIDATO_HIST WHERE RICHIESTA_CANDIDATO_HIST.COD_ESITO_SEL IN (1,14) AND RICHIESTA_CANDIDATO_HIST.ID_RICHIESTA = richiesta_sede.id_richiesta)"
					end if
				end if
			end if
	  		
		next
		sSQLExpiredDeals = sSQLExpiredDeals & " Order by richiesta_sede.DT_FINPUBLICACION asc"

		generateQueryExpiredDealsExporttoExcel = sSQLExpiredDeals 
	end function

	function generateQueryRegisteredCompaniesExporttoExcel(reportFilterDataBase64)

		sSQLRegisteredCompanies = "SELECT i.RAG_SOC as 'NOMBRE DE LA EMPRESA', " &_ 
								"si.DESCRIZIONE as SEDE, " &_ 
								"dep.DESCRIZIONE AS 'DEPARTAMENTO', " &_
								"Ciudad.DESCOM AS 'CIUDAD'," &_
								"si.E_MAIL_SEDE as 'Correo Electr�nico', " &_
								"si.NUM_TEL_SEDE as 'N�mero Telef�nico 1', " &_
								"si.NUM_TEL as 'N�mero Telef�nico 2', " &_
								"convert(varchar(25),i.DT_TMST, 103) as 'FECHA DE INSCRIPCI�N' " &_				
								"FROM sede_impresa si WITH(nolock) " &_
								"INNER JOIN (SELECT DISTINCT id_impresa, rol FROM impresa_rolesservicios WITH(nolock)) sri ON sri.id_impresa = si.id_impresa " &_
								"INNER JOIN IMPRESA I ON si.ID_IMPRESA =i.ID_IMPRESA " &_
								"INNER JOIN (select CODICE, DESCRIZIONE from tades where nome_tabella = 'PROV') Dep ON si.PRV =dep.CODICE " &_
								"INNER JOIN (SELECT CODCOM,DESCOM from COMUNE) Ciudad ON si.COMUNE=Ciudad.CODCOM " &_
								"WHERE sri.rol = 1 " 

		reportFilterDataSplit = Split(Base64Decode(reportFilterDataBase64),",")
		
		for i=0 to ubound(reportFilterDataSplit)	  			
	  		
	  		reportFilterData = Split(reportFilterDataSplit(i),":")
	  		columnName = reportFilterData(0)  
	  		valueColumn = reportFilterData(1)


			if valueColumn <> "" then

				if columnName = "nombre_empresa" then
					sSQLRegisteredCompanies = sSQLRegisteredCompanies & "AND i.RAG_SOC LIKE '%" & valueColumn & "%' "
				end if
				if columnName = "sede_empresa" then
					sSQLRegisteredCompanies = sSQLRegisteredCompanies & "AND si.DESCRIZIONE LIKE '%" & valueColumn & "%' "
				end if
				if columnName = "departamento"then
					sSQLRegisteredCompanies = sSQLRegisteredCompanies & "AND dep.CODICE = '" & valueColumn & "' "
				end if
				if columnName = "ciudad" then
					sSQLRegisteredCompanies = sSQLRegisteredCompanies & "AND Ciudad.CODCOM = '" & valueColumn & "' "
				end if
				if columnName = "fecha_inscripcion_from" then		
					fecha_vencimiento_toSplit = Split(reportFilterDataSplit(i+1),":")
				  	fecha_vencimiento_to = fecha_vencimiento_toSplit(1)			
				  	
					sSQLRegisteredCompanies = sSQLRegisteredCompanies & " AND convert(datetime,i.DT_TMST, 103) BETWEEN convert(datetime,convert(varchar(25),'" & valueColumn & " 00:00:00', 103), 103) AND convert(datetime,convert(varchar(25),'" & fecha_vencimiento_to & " 00:00:00', 103),103) " 
				end if

			end if

		next
					
		sSQLRegisteredCompanies = sSQLRegisteredCompanies & "ORDER BY si.ID_SEDE"

		'response.Write sSQLRegisteredCompanies

		generateQueryRegisteredCompaniesExporttoExcel = sSQLRegisteredCompanies
	end function

	function generateQueryRegisteredPeopleExporttoExcel()

		sSQLRegisteredPeopleTimas = "SELECT TI.ID_PERSONA, MAX(TITAD.VALORE) VALORE INTO ##TIMAS9870 " &_
								"FROM TISTUD TI with(nolock) " &_
								"INNER JOIN (SELECT * FROM TADES with(nolock)  WHERE NOME_TABELLA= 'LSTUD') TITAD " &_
         						"ON TI.COD_LIV_STUD = TITAD.CODICE " &_
 								"GROUP BY TI.ID_PERSONA " 
	 
		sSQLRegisteredPeopleCurson = "SELECT P.ID_PERSONA, P.SESSO, ISNULL(TIMAS.VALORE,'ND') AS VALORE INTO ##CURSON49857 " &_
									"FROM PERSONA P with(nolock) LEFT JOIN  ##TIMAS9870 TIMAS " &_
	                				"ON P.ID_PERSONA = TIMAS.ID_PERSONA " &_
									"WHERE P.ID_PERSONA IN (SELECT DISTINCT ID_PERSONA FROM STATO_OCCUPAZIONALE with(nolock) WHERE ID_CIMPIEGO is not null)" &_
									"ORDER BY P.ID_PERSONA "
	
		sSQLRegisteredPeople = "SELECT p.ID_PERSONA, " &_
								"p.COD_FISC AS 'IDENTIFICACION', " &_
								"replace(p.NOME,'&#209;','�') AS 'PRIMER NOMBRE', " &_
								"replace(p.SECONDO_NOME,'&#209;','�') AS 'SEGUNDO NOMBRE', " &_
								"replace(p.COGNOME,'&#209;','�') AS 'PRIMER APELLIDO', " &_
								"replace(p.SECONDO_COGNOME,'&#209;','�') AS 'SEGUNDO APELLIDO', " &_
								"floor(datediff(MONTH, p.DT_NASC, getdate()) / 12) as 'EDAD', " &_
								"case when p.SESSO='F' then 'FEMENINO' else 'MASCULINO' end as 'SEXO', " &_
								"dep.DESCRIZIONE AS 'DEPARTAMENTO DE RESIDENCIA', " &_
								"Ciudad.DESCOM AS 'CIUDAD DE RESIDENCIA', " &_
								"p.FRAZIONE_RES AS 'BARRIO DE RESIDENCIA', " &_
								"P.NUM_TEL AS 'TELEFONO FIJO', " &_
								"P.NUM_TEL_DOM AS 'TELEFONO CELULAR', " &_
								"P.E_MAIL AS 'CORREO ELECTRONICO', " &_
								"sim.DESCRIZIONE 'CENTRO DE ATENCION', " &_
								"TITAD.DESCRIZIONE 'MAXIMO NIVEL EDUCATIVO', " &_
								"t.DESCRIZIONE  as 'SITUACION LABORAL ACTUAL', " &_
								"min(convert(varchar(25), SO.DT_DICH_STATUS, 103)) AS 'FECHA DE INSCRIPCION', " &_
								"floor(DATEDIFF(day,SO.DT_DICH_STATUS,GETDATE())) AS 'DIAS DE INSCRIPCION' " &_
								"FROM persona p " &_
								"inner join stato_occupazionale so on p.id_persona=so.id_persona " &_
								"inner join tades t on so.cod_stdis=t.CODICE and NOME_TABELLA ='STDIS' " &_
								"inner join SEDE_IMPRESA sim on sim.id_sede = so.ID_CIMPIEGO " &_
								"inner join (select CODICE, DESCRIZIONE from tades where nome_tabella = 'PROV') Dep " &_
										"on p.PRV_RES =dep.CODICE " &_
								"inner join (SELECT CODCOM,DESCOM from COMUNE) Ciudad on p.COM_RES=Ciudad.CODCOM " &_
								"inner join ##CURSON49857 C on C.ID_PERSONA = p.ID_PERSONA " &_
								"INNER JOIN (SELECT VALORE, DESCRIZIONE FROM TADES with(nolock) WHERE NOME_TABELLA= 'LSTUD' UNION( SELECT 'ND', 'No Disponible')) TITAD on c.valore = TITAD.valore " &_
								"WHERE so.id_cimpiego is not null " &_
								"AND (ind_status = '0' OR (ind_status = '2' " &_
								"AND P.ID_PERSONA not in ( SELECT ID_PERSONA FROM STATO_OCCUPAZIONALE WHERE p.id_persona = id_persona AND ind_status = '0' ))) " &_
								"AND SO.DT_DICH_STATUS >= convert(datetime,'01/05/2013 10:0:0',103) " &_
								"group BY p.ID_PERSONA, p.COD_FISC,replace(p.NOME,'&#209;','�') , replace(p.SECONDO_NOME,'&#209;','�'), replace(p.COGNOME,'&#209;','�'), replace(p.SECONDO_COGNOME,'&#209;','�'), floor(datediff(MONTH, p.DT_NASC, getdate()) / 12),p.SESSO, floor(DATEDIFF(day,SO.DT_DICH_STATUS,GETDATE())),dep.DESCRIZIONE,Ciudad.DESCOM,P.NUM_TEL,P.NUM_TEL_DOM, P.E_MAIL, P.FRAZIONE_RES, sim.DESCRIZIONE,t.DESCRIZIONE,TITAD.DESCRIZIONE " &_
								"ORDER BY 'PRIMER NOMBRE', 'SEGUNDO NOMBRE', 'PRIMER APELLIDO', 'SEGUNDO APELLIDO' " &_
								"DROP TABLE ##TIMAS9870 DROP TABLE ##CURSON49857 "

		sSQLRegisteredPeopleComplete = "SET NOCOUNT ON; " & sSQLRegisteredPeopleTimas & " " & sSQLRegisteredPeopleCurson & " " & sSQLRegisteredPeople

		generateQueryRegisteredPeopleExporttoExcel = sSQLRegisteredPeopleComplete
	end function


function generateQueryRegisteredPeopleByCenterExporttoExcel()

		sSQLRegisteredPeopleTimas = "SELECT TI.ID_PERSONA, MAX(TITAD.VALORE) VALORE INTO ##TIMAS9870 " &_
								"FROM TISTUD TI with(nolock) " &_
								"INNER JOIN (SELECT * FROM TADES with(nolock)  WHERE NOME_TABELLA= 'LSTUD') TITAD " &_
         						"ON TI.COD_LIV_STUD = TITAD.CODICE " &_
 								"GROUP BY TI.ID_PERSONA " 
	 
		sSQLRegisteredPeopleCurson = "SELECT P.ID_PERSONA, P.SESSO, ISNULL(TIMAS.VALORE,'ND') AS VALORE INTO ##CURSON49857 " &_
									"FROM PERSONA P with(nolock) LEFT JOIN  ##TIMAS9870 TIMAS " &_
	                				"ON P.ID_PERSONA = TIMAS.ID_PERSONA " &_
									"WHERE P.ID_PERSONA IN (SELECT DISTINCT ID_PERSONA FROM STATO_OCCUPAZIONALE with(nolock) WHERE ID_CIMPIEGO = " & session("Creator") & ") " &_
									"ORDER BY P.ID_PERSONA "
	
		sSQLRegisteredPeople = "SELECT p.ID_PERSONA, " &_
								"p.COD_FISC AS 'IDENTIFICACION', " &_
								"replace(p.NOME,'&#209;','�') AS 'PRIMER NOMBRE', " &_
								"replace(p.SECONDO_NOME,'&#209;','�') AS 'SEGUNDO NOMBRE', " &_
								"replace(p.COGNOME,'&#209;','�') AS 'PRIMER APELLIDO', " &_
								"replace(p.SECONDO_COGNOME,'&#209;','�') AS 'SEGUNDO APELLIDO', " &_
								"floor(datediff(MONTH, p.DT_NASC, getdate()) / 12) as 'EDAD', " &_
								"case when p.SESSO='F' then 'FEMENINO' else 'MASCULINO' end as 'SEXO', " &_
								"dep.DESCRIZIONE AS 'DEPARTAMENTO DE RESIDENCIA', " &_
								"Ciudad.DESCOM AS 'CIUDAD DE RESIDENCIA', " &_
								"p.FRAZIONE_RES AS 'BARRIO DE RESIDENCIA', " &_
								"P.NUM_TEL AS 'TELEFONO FIJO', " &_
								"P.NUM_TEL_DOM AS 'TELEFONO CELULAR', " &_
								"P.E_MAIL AS 'CORREO ELECTRONICO', " &_
								"sim.DESCRIZIONE 'CENTRO DE ATENCION', " &_
								"TITAD.DESCRIZIONE 'MAXIMO NIVEL EDUCATIVO', " &_
								"t.DESCRIZIONE  as 'SITUACION LABORAL ACTUAL', " &_
								"min(convert(varchar(25), SO.DT_DICH_STATUS, 103)) AS 'FECHA DE INSCRIPCION', " &_
								"floor(DATEDIFF(day,SO.DT_DICH_STATUS,GETDATE())) AS 'DIAS DE INSCRIPCION' " &_
								"FROM persona p " &_
								"inner join stato_occupazionale so on p.id_persona=so.id_persona " &_
								"inner join tades t on so.cod_stdis=t.CODICE and NOME_TABELLA ='STDIS' " &_
								"inner join SEDE_IMPRESA sim on sim.id_sede = so.ID_CIMPIEGO " &_
								"inner join (select CODICE, DESCRIZIONE from tades where nome_tabella = 'PROV') Dep " &_
										"on p.PRV_RES =dep.CODICE " &_
								"inner join (SELECT CODCOM,DESCOM from COMUNE) Ciudad on p.COM_RES=Ciudad.CODCOM " &_
								"inner join ##CURSON49857 C on C.ID_PERSONA = p.ID_PERSONA " &_
								"INNER JOIN (SELECT VALORE, DESCRIZIONE FROM TADES with(nolock) WHERE NOME_TABELLA= 'LSTUD' UNION( SELECT 'ND', 'No Disponible')) TITAD on c.valore = TITAD.valore " &_
								"WHERE so.id_cimpiego =" & session("Creator") & " " &_
								"AND (ind_status = '0' OR (ind_status = '2' " &_
								"AND P.ID_PERSONA not in ( SELECT ID_PERSONA FROM STATO_OCCUPAZIONALE WHERE p.id_persona = id_persona AND ind_status = '0' ))) " &_
								"AND SO.DT_DICH_STATUS >= convert(datetime,'01/05/2013 10:0:0',103) " &_
								"group BY p.ID_PERSONA, p.COD_FISC,replace(p.NOME,'&#209;','�') , replace(p.SECONDO_NOME,'&#209;','�'), replace(p.COGNOME,'&#209;','�'), replace(p.SECONDO_COGNOME,'&#209;','�'), floor(datediff(MONTH, p.DT_NASC, getdate()) / 12),p.SESSO, floor(DATEDIFF(day,SO.DT_DICH_STATUS,GETDATE())),dep.DESCRIZIONE,Ciudad.DESCOM,P.NUM_TEL,P.NUM_TEL_DOM, P.E_MAIL, P.FRAZIONE_RES, sim.DESCRIZIONE,t.DESCRIZIONE,TITAD.DESCRIZIONE " &_
								"ORDER BY 'PRIMER NOMBRE', 'SEGUNDO NOMBRE', 'PRIMER APELLIDO', 'SEGUNDO APELLIDO' " &_
								"DROP TABLE ##TIMAS9870 DROP TABLE ##CURSON49857 "

		sSQLRegisteredPeopleComplete = "SET NOCOUNT ON; " & sSQLRegisteredPeopleTimas & " " & sSQLRegisteredPeopleCurson & " " & sSQLRegisteredPeople

		generateQueryRegisteredPeopleByCenterExporttoExcel = sSQLRegisteredPeopleComplete
	end function


	function generateQueryCurrentDealsByCenterExporttoExcel(reportFilterDataBase64)
		sSQLCurrentDealsByCenter = ""
		sSQLCurrentDealsByCenter = "SELECT richiesta_sede.id_richiesta AS Referencia," &_ 
									"im.RAG_SOC+' ('+SEDE_IMPRESA.DESCRIZIONE+')' as Empleador," &_ 
									"richiesta_sede.offer_title as 'T�tulo de la Oferta'," &_
									"f.denominazione as 'Cargo Equivalente', a.denominazione as 'Grupo Ocupacional', " &_
									"richiesta_sede.nro_vacantes as 'Numero de Vacantes', " &_
										"(SELECT COUNT(RICHIESTA_CANDIDATO_HIST.ID_RICHIESTA) AS NUMERO " &_
										"FROM RICHIESTA_CANDIDATO_HIST " &_
										"WHERE RICHIESTA_CANDIDATO_HIST.COD_ESITO_SEL IN (1,14) " &_
										"AND RICHIESTA_CANDIDATO_HIST.ID_RICHIESTA = richiesta_sede.id_richiesta " &_
										"AND ( " &_
											"SELECT top 1 rch.DT_INS " &_
											"FROM RICHIESTA_CANDIDATO_HIST rch " &_
											"WHERE rch.ID_RICHIESTA = RICHIESTA_CANDIDATO_HIST.ID_RICHIESTA " &_
														"AND rch.ID_PERSONA = RICHIESTA_CANDIDATO_HIST.ID_PERSONA " &_
											"ORDER BY rch.DT_INS DESC " &_
										") = RICHIESTA_CANDIDATO_HIST.DT_INS " &_
									") AS 'Num Postulantes', " &_
									"(SELECT count(DISTINCT(RICHIESTA_CANDIDATO_HIST.ID_PERSONA)) " &_
										"FROM RICHIESTA_CANDIDATO_HIST " &_
										"WHERE RICHIESTA_CANDIDATO_HIST.COD_ESITO_SEL IN (1,14) " &_
										"AND RICHIESTA_CANDIDATO_HIST.ID_RICHIESTA = richiesta_sede.id_richiesta) 'Num Postulantes old'," &_
									" sim.DESCRIZIONE as 'Centro de Atenci�n', " &_
									" sim.ID_SEDE as 'Id Centro de Atenci�n', " &_
									"convert(varchar(25),richiesta_sede.DT_FINPUBLICACION, 103) as 'Fecha Vencimiento', " &_
									"convert(varchar(25),richiesta_sede.DT_INGRESO, 103) as 'Fecha Inicio', " &_
									"(SELECT COUNT(RICHIESTA_CANDIDATO_HIST.COD_ESITO_SEL) AS NUMERO " &_
										"FROM RICHIESTA_CANDIDATO_HIST " &_
										"WHERE RICHIESTA_CANDIDATO_HIST.COD_ESITO_SEL IN (1,14) " &_
												"AND RICHIESTA_CANDIDATO_HIST.COD_ESITO_SEL = 1 " &_
										"AND RICHIESTA_CANDIDATO_HIST.ID_RICHIESTA = richiesta_sede.id_richiesta  " &_
										"AND ( " &_
											"SELECT top 1 rch.DT_INS " &_
											"FROM RICHIESTA_CANDIDATO_HIST rch " &_
											"WHERE rch.ID_RICHIESTA = RICHIESTA_CANDIDATO_HIST.ID_RICHIESTA " &_
														"AND rch.ID_PERSONA = RICHIESTA_CANDIDATO_HIST.ID_PERSONA " &_
											"ORDER BY rch.DT_INS DESC " &_
										") = RICHIESTA_CANDIDATO_HIST.DT_INS " &_
										"GROUP BY CAST(RICHIESTA_CANDIDATO_HIST.COD_ESITO_SEL AS int) " &_
									") AS TOTAL_COD_UNO, " &_
									"(SELECT COUNT(RICHIESTA_CANDIDATO_HIST.COD_ESITO_SEL) AS NUMERO " &_
										"FROM RICHIESTA_CANDIDATO_HIST " &_
										"WHERE RICHIESTA_CANDIDATO_HIST.COD_ESITO_SEL IN (1,14) " &_
												"AND RICHIESTA_CANDIDATO_HIST.COD_ESITO_SEL = 14 " &_
										"AND RICHIESTA_CANDIDATO_HIST.ID_RICHIESTA = richiesta_sede.id_richiesta  " &_
										"AND ( " &_
											"SELECT top 1 rch.DT_INS " &_
											"FROM RICHIESTA_CANDIDATO_HIST rch " &_
											"WHERE rch.ID_RICHIESTA = RICHIESTA_CANDIDATO_HIST.ID_RICHIESTA " &_
														"AND rch.ID_PERSONA = RICHIESTA_CANDIDATO_HIST.ID_PERSONA " &_
											"ORDER BY rch.DT_INS DESC " &_
										") = RICHIESTA_CANDIDATO_HIST.DT_INS " &_
										"GROUP BY CAST(RICHIESTA_CANDIDATO_HIST.COD_ESITO_SEL AS int) " &_
									") AS TOTAL_COD_CATORCE " &_
									"FROM SEDE_IMPRESA " &_
									"inner join IMPRESA im on im.id_impresa = SEDE_IMPRESA.id_impresa " &_
									"inner join richiesta_sede on richiesta_sede.id_sede = SEDE_IMPRESA.id_sede " &_
									"inner join FIGUREPROFESSIONALI f on richiesta_sede.id_figprof = f.id_figprof " &_
									"inner join impresa_rolesserviciostipos irs ON richiesta_sede.servicio = irs.servicio " &_
									"inner join AREE_PROFESSIONALI a on a.id_areaprof = f.id_areaprof " &_
									"inner join SEDE_IMPRESA sim on sim.id_sede = richiesta_sede.COD_OFICINA " &_
									"WHERE	richiesta_sede.DT_finpublicacion > = convert(datetime,CONVERT(varchar(10), GETDATE(), 103),103) AND richiesta_sede.FL_PUBBLICATO = 'S' " &_
										"AND richiesta_sede.cod_tipo_richiesta = 1 " &_
										"AND richiesta_sede.COD_OFICINA = " & session("Creator") & " "

		
		reportFilterDataSplit = Split(Base64Decode(reportFilterDataBase64),",")
		
		for i=0 to ubound(reportFilterDataSplit)	  			
	  		
	  		reportFilterData = Split(reportFilterDataSplit(i),":")
	  		columnName = reportFilterData(0)  
	  		valueColumn = reportFilterData(1)
	  		
			if valueColumn <> "" then
				if columnName = "empleador" then					
					sSQLCurrentDealsByCenter = sSQLCurrentDealsByCenter & " AND im.RAG_SOC LIKE '%" & valueColumn & "%' "
				end if
				if columnName = "titulo_oferta" then					
					sSQLCurrentDealsByCenter = sSQLCurrentDealsByCenter & " AND richiesta_sede.offer_title LIKE '%" & valueColumn & "%' "
				end if
				if columnName = "fecha_publicacion" then
					sSQLCurrentDealsByCenter = sSQLCurrentDealsByCenter & " AND convert(datetime,convert(varchar(25),richiesta_sede.DT_INGRESO, 103), 103) = convert(datetime,convert(varchar(25),'" & valueColumn & " 00:00:00', 103), 103) " 
				end if
				if columnName = "fecha_vencimiento_from" then	
					fecha_vencimiento_toSplit = Split(reportFilterDataSplit(i+1),":")
			  		fecha_vencimiento_to = fecha_vencimiento_toSplit(1)

					sSQLCurrentDealsByCenter = sSQLCurrentDealsByCenter & " AND convert(datetime,richiesta_sede.DT_FINPUBLICACION, 103) BETWEEN convert(datetime,convert(varchar(25),'" & valueColumn & " 00:00:00', 103), 103) AND convert(datetime,convert(varchar(25),'" & fecha_vencimiento_to & " 00:00:00', 103),103) "
				end if
				if columnName = "nroVacantesVsPostulantes" then
					if valueColumn = "Mayor" then
						sSQLCurrentDealsByCenter = sSQLCurrentDealsByCenter & " AND richiesta_sede.nro_vacantes > (SELECT count(DISTINCT(RICHIESTA_CANDIDATO_HIST.ID_PERSONA)) FROM RICHIESTA_CANDIDATO_HIST WHERE RICHIESTA_CANDIDATO_HIST.COD_ESITO_SEL IN (1,14) AND RICHIESTA_CANDIDATO_HIST.ID_RICHIESTA = richiesta_sede.id_richiesta)"
					end if
					if valueColumn = "Menor" then
						sSQLCurrentDealsByCenter = sSQLCurrentDealsByCenter & " AND richiesta_sede.nro_vacantes < (SELECT count(DISTINCT(RICHIESTA_CANDIDATO_HIST.ID_PERSONA)) FROM RICHIESTA_CANDIDATO_HIST WHERE RICHIESTA_CANDIDATO_HIST.COD_ESITO_SEL IN (1,14) AND RICHIESTA_CANDIDATO_HIST.ID_RICHIESTA = richiesta_sede.id_richiesta)"
					end if
					if valueColumn = "Igual" then
						sSQLCurrentDealsByCenter = sSQLCurrentDealsByCenter & " AND richiesta_sede.nro_vacantes = (SELECT count(DISTINCT(RICHIESTA_CANDIDATO_HIST.ID_PERSONA)) FROM RICHIESTA_CANDIDATO_HIST WHERE RICHIESTA_CANDIDATO_HIST.COD_ESITO_SEL IN (1,14) AND RICHIESTA_CANDIDATO_HIST.ID_RICHIESTA = richiesta_sede.id_richiesta)"
					end if
				end if
			end if
	  		
		next
		sSQLCurrentDealsByCenter = sSQLCurrentDealsByCenter & " Order by richiesta_sede.DT_FINPUBLICACION asc"

		generateQueryCurrentDealsByCenterExporttoExcel = sSQLCurrentDealsByCenter 
	end function

	function generateQueryExpiredDealsByCenterExporttoExcel(reportFilterDataBase64)
		sSQLExpiredDealsByCenter = ""
		sSQLExpiredDealsByCenter = "SELECT richiesta_sede.id_richiesta AS Referencia," &_ 
									"im.RAG_SOC+' ('+SEDE_IMPRESA.DESCRIZIONE+')' as Empleador," &_ 
									"richiesta_sede.offer_title as 'T�tulo de la Oferta'," &_
									"f.denominazione as 'Cargo Equivalente', a.denominazione as 'Grupo Ocupacional', " &_
									"richiesta_sede.nro_vacantes as 'Numero de Vacantes', " &_
										"(SELECT COUNT(RICHIESTA_CANDIDATO_HIST.ID_RICHIESTA) AS NUMERO " &_
										"FROM RICHIESTA_CANDIDATO_HIST " &_
										"WHERE RICHIESTA_CANDIDATO_HIST.COD_ESITO_SEL IN (1,14) " &_
										"AND RICHIESTA_CANDIDATO_HIST.ID_RICHIESTA = richiesta_sede.id_richiesta " &_
										"AND ( " &_
											"SELECT top 1 rch.DT_INS " &_
											"FROM RICHIESTA_CANDIDATO_HIST rch " &_
											"WHERE rch.ID_RICHIESTA = RICHIESTA_CANDIDATO_HIST.ID_RICHIESTA " &_
														"AND rch.ID_PERSONA = RICHIESTA_CANDIDATO_HIST.ID_PERSONA " &_
											"ORDER BY rch.DT_INS DESC " &_
										") = RICHIESTA_CANDIDATO_HIST.DT_INS " &_
									") AS 'Num Postulantes', " &_
									"(SELECT count(DISTINCT(RICHIESTA_CANDIDATO_HIST.ID_PERSONA)) " &_
										"FROM RICHIESTA_CANDIDATO_HIST " &_
										"WHERE RICHIESTA_CANDIDATO_HIST.COD_ESITO_SEL IN (1,14) " &_
										"AND RICHIESTA_CANDIDATO_HIST.ID_RICHIESTA = richiesta_sede.id_richiesta) 'Num Postulantes old'," &_
									" sim.DESCRIZIONE as 'Centro de Atenci�n', " &_
									" sim.ID_SEDE as 'Id Centro de Atenci�n', " &_
									"convert(varchar(25),richiesta_sede.DT_FINPUBLICACION, 103) as 'Fecha Vencimiento', " &_
									"convert(varchar(25),richiesta_sede.DT_INGRESO, 103) as 'Fecha Inicio', " &_
									"(SELECT COUNT(RICHIESTA_CANDIDATO_HIST.COD_ESITO_SEL) AS NUMERO " &_
										"FROM RICHIESTA_CANDIDATO_HIST " &_
										"WHERE RICHIESTA_CANDIDATO_HIST.COD_ESITO_SEL IN (1,14) " &_
												"AND RICHIESTA_CANDIDATO_HIST.COD_ESITO_SEL = 1 " &_
										"AND RICHIESTA_CANDIDATO_HIST.ID_RICHIESTA = richiesta_sede.id_richiesta  " &_
										"AND ( " &_
											"SELECT top 1 rch.DT_INS " &_
											"FROM RICHIESTA_CANDIDATO_HIST rch " &_
											"WHERE rch.ID_RICHIESTA = RICHIESTA_CANDIDATO_HIST.ID_RICHIESTA " &_
														"AND rch.ID_PERSONA = RICHIESTA_CANDIDATO_HIST.ID_PERSONA " &_
											"ORDER BY rch.DT_INS DESC " &_
										") = RICHIESTA_CANDIDATO_HIST.DT_INS " &_
										"GROUP BY CAST(RICHIESTA_CANDIDATO_HIST.COD_ESITO_SEL AS int) " &_
									") AS TOTAL_COD_UNO, " &_
									"(SELECT COUNT(RICHIESTA_CANDIDATO_HIST.COD_ESITO_SEL) AS NUMERO " &_
										"FROM RICHIESTA_CANDIDATO_HIST " &_
										"WHERE RICHIESTA_CANDIDATO_HIST.COD_ESITO_SEL IN (1,14) " &_
												"AND RICHIESTA_CANDIDATO_HIST.COD_ESITO_SEL = 14 " &_
										"AND RICHIESTA_CANDIDATO_HIST.ID_RICHIESTA = richiesta_sede.id_richiesta  " &_
										"AND ( " &_
											"SELECT top 1 rch.DT_INS " &_
											"FROM RICHIESTA_CANDIDATO_HIST rch " &_
											"WHERE rch.ID_RICHIESTA = RICHIESTA_CANDIDATO_HIST.ID_RICHIESTA " &_
														"AND rch.ID_PERSONA = RICHIESTA_CANDIDATO_HIST.ID_PERSONA " &_
											"ORDER BY rch.DT_INS DESC " &_
										") = RICHIESTA_CANDIDATO_HIST.DT_INS " &_
										"GROUP BY CAST(RICHIESTA_CANDIDATO_HIST.COD_ESITO_SEL AS int) " &_
									") AS TOTAL_COD_CATORCE " &_
									"FROM SEDE_IMPRESA " &_
									"inner join IMPRESA im on im.id_impresa = SEDE_IMPRESA.id_impresa " &_
									"inner join richiesta_sede on richiesta_sede.id_sede = SEDE_IMPRESA.id_sede " &_
									"inner join FIGUREPROFESSIONALI f on richiesta_sede.id_figprof = f.id_figprof " &_
									"inner join impresa_rolesserviciostipos irs ON richiesta_sede.servicio = irs.servicio " &_
									"inner join AREE_PROFESSIONALI a on a.id_areaprof = f.id_areaprof " &_
									"inner join SEDE_IMPRESA sim on sim.id_sede = richiesta_sede.COD_OFICINA " &_
									"WHERE	richiesta_sede.DT_finpublicacion < convert(datetime,CONVERT(varchar(10), GETDATE(), 103),103) AND richiesta_sede.FL_PUBBLICATO = 'S' " &_
										"AND richiesta_sede.cod_tipo_richiesta = 1 " &_
										"AND richiesta_sede.COD_OFICINA = " & session("Creator") & " "

		
		reportFilterDataSplit = Split(Base64Decode(reportFilterDataBase64),",")
		
		for i=0 to ubound(reportFilterDataSplit)	  			
	  		
	  		reportFilterData = Split(reportFilterDataSplit(i),":")
	  		columnName = reportFilterData(0)  
	  		valueColumn = reportFilterData(1)
	  		
			if valueColumn <> "" then
				if columnName = "empleador" then					
					sSQLExpiredDealsByCenter = sSQLExpiredDealsByCenter & " AND im.RAG_SOC LIKE '%" & valueColumn & "%' "
				end if
				if columnName = "titulo_oferta" then					
					sSQLExpiredDealsByCenter = sSQLExpiredDealsByCenter & " AND richiesta_sede.offer_title LIKE '%" & valueColumn & "%' "
				end if
				if columnName = "fecha_publicacion" then
					sSQLExpiredDealsByCenter = sSQLExpiredDealsByCenter & " AND convert(datetime,convert(varchar(25),richiesta_sede.DT_INGRESO, 103), 103) = convert(datetime,convert(varchar(25),'" & valueColumn & " 00:00:00', 103), 103) " 
				end if
				if columnName = "fecha_vencimiento_from" then	
					fecha_vencimiento_toSplit = Split(reportFilterDataSplit(i+1),":")
			  		fecha_vencimiento_to = fecha_vencimiento_toSplit(1)

					sSQLExpiredDealsByCenter = sSQLExpiredDealsByCenter & " AND convert(datetime,richiesta_sede.DT_FINPUBLICACION, 103) BETWEEN convert(datetime,convert(varchar(25),'" & valueColumn & " 00:00:00', 103), 103) AND convert(datetime,convert(varchar(25),'" & fecha_vencimiento_to & " 00:00:00', 103),103) "
				end if
				if columnName = "nroVacantesVsPostulantes" then
					if valueColumn = "Mayor" then
						sSQLExpiredDealsByCenter = sSQLExpiredDealsByCenter & " AND richiesta_sede.nro_vacantes > (SELECT count(DISTINCT(RICHIESTA_CANDIDATO_HIST.ID_PERSONA)) FROM RICHIESTA_CANDIDATO_HIST WHERE RICHIESTA_CANDIDATO_HIST.COD_ESITO_SEL IN (1,14) AND RICHIESTA_CANDIDATO_HIST.ID_RICHIESTA = richiesta_sede.id_richiesta)"
					end if
					if valueColumn = "Menor" then
						sSQLExpiredDealsByCenter = sSQLExpiredDealsByCenter & " AND richiesta_sede.nro_vacantes < (SELECT count(DISTINCT(RICHIESTA_CANDIDATO_HIST.ID_PERSONA)) FROM RICHIESTA_CANDIDATO_HIST WHERE RICHIESTA_CANDIDATO_HIST.COD_ESITO_SEL IN (1,14) AND RICHIESTA_CANDIDATO_HIST.ID_RICHIESTA = richiesta_sede.id_richiesta)"
					end if
					if valueColumn = "Igual" then
						sSQLExpiredDealsByCenter = sSQLExpiredDealsByCenter & " AND richiesta_sede.nro_vacantes = (SELECT count(DISTINCT(RICHIESTA_CANDIDATO_HIST.ID_PERSONA)) FROM RICHIESTA_CANDIDATO_HIST WHERE RICHIESTA_CANDIDATO_HIST.COD_ESITO_SEL IN (1,14) AND RICHIESTA_CANDIDATO_HIST.ID_RICHIESTA = richiesta_sede.id_richiesta)"
					end if
				end if
			end if
	  		
		next
		sSQLExpiredDealsByCenter = sSQLExpiredDealsByCenter & " Order by richiesta_sede.DT_FINPUBLICACION asc"

		generateQueryExpiredDealsByCenterExporttoExcel = sSQLExpiredDealsByCenter 
	end function

	function generateQueryRegisteredCompaniesByCenterExporttoExcel(reportFilterDataBase64)

		sSQLRegisteredCompaniesByCenter = "SELECT i.RAG_SOC as 'NOMBRE DE LA EMPRESA', " &_ 
								"si.DESCRIZIONE as SEDE, " &_ 
								"dep.DESCRIZIONE AS 'DEPARTAMENTO', " &_
								"Ciudad.DESCOM AS 'CIUDAD'," &_
								"si.E_MAIL_SEDE as 'Correo Electr�nico', " &_
								"si.NUM_TEL_SEDE as 'N�mero Telef�nico 1', " &_
								"si.NUM_TEL as 'N�mero Telef�nico 2', " &_
								"convert(varchar(25),i.DT_TMST, 103) as 'FECHA DE INSCRIPCI�N' " &_				
								"FROM sede_impresa si WITH(nolock) " &_
								"INNER JOIN (SELECT DISTINCT id_impresa, rol FROM impresa_rolesservicios WITH(nolock)) sri ON sri.id_impresa = si.id_impresa " &_
								"INNER JOIN IMPRESA I ON si.ID_IMPRESA =i.ID_IMPRESA " &_
								"INNER JOIN (select CODICE, DESCRIZIONE from tades where nome_tabella = 'PROV') Dep ON si.PRV =dep.CODICE " &_
								"INNER JOIN (SELECT CODCOM,DESCOM from COMUNE) Ciudad ON si.COMUNE=Ciudad.CODCOM " &_
								"WHERE sri.rol = 1 "  &_		
									"AND si.id_cimpiego = " & session("Creator") & " "

		reportFilterDataSplit = Split(Base64Decode(reportFilterDataBase64),",")
		
		for i=0 to ubound(reportFilterDataSplit)	  			
	  		
	  		reportFilterData = Split(reportFilterDataSplit(i),":")
	  		columnName = reportFilterData(0)  
	  		valueColumn = reportFilterData(1)


			if valueColumn <> "" then

				if columnName = "nombre_empresa" then
					sSQLRegisteredCompaniesByCenter = sSQLRegisteredCompaniesByCenter & "AND i.RAG_SOC LIKE '%" & valueColumn & "%' "
				end if
				if columnName = "sede_empresa" then
					sSQLRegisteredCompaniesByCenter = sSQLRegisteredCompaniesByCenter & "AND si.DESCRIZIONE LIKE '%" & valueColumn & "%' "
				end if
				if columnName = "departamento"then
					sSQLRegisteredCompaniesByCenter = sSQLRegisteredCompaniesByCenter & "AND dep.CODICE = '" & valueColumn & "' "
				end if
				if columnName = "ciudad" then
					sSQLRegisteredCompaniesByCenter = sSQLRegisteredCompaniesByCenter & "AND Ciudad.CODCOM = '" & valueColumn & "' "
				end if
				if columnName = "fecha_inscripcion_from" then		
					fecha_vencimiento_toSplit = Split(reportFilterDataSplit(i+1),":")
				  	fecha_vencimiento_to = fecha_vencimiento_toSplit(1)			
				  	
					sSQLRegisteredCompaniesByCenter = sSQLRegisteredCompaniesByCenter & " AND convert(datetime,i.DT_TMST, 103) BETWEEN convert(datetime,convert(varchar(25),'" & valueColumn & " 00:00:00', 103), 103) AND convert(datetime,convert(varchar(25),'" & fecha_vencimiento_to & " 00:00:00', 103),103) " 
				end if

			end if

		next
					
		sSQLRegisteredCompaniesByCenter = sSQLRegisteredCompaniesByCenter & "ORDER BY si.ID_SEDE"

		'response.Write sSQLRegisteredCompaniesByCenter

		generateQueryRegisteredCompaniesByCenterExporttoExcel = sSQLRegisteredCompaniesByCenter
	end function

%>