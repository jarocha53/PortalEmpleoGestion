<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%

Function MostrarTablaFiltros(nRic)
'==================================================
'
'
'
'==================================================
	dim i, SQL, nFiltros, z, aOpLog, sDescrTab, memId, lung
	dim sDecNomTab, sTipoCampo, sDescNomeCampo, sDenominazione

%>
	<table border="1" bordercolor="#006699" cellpadding="8" cellspacing="0" width="520" align="center">
		<tr  bordercolor= "white">
			<td colspan="2" align="center" class="tbltext1">
				<b>ESPECIFICACIONES DEL PERFIL SOLICITADO</b>
			</td>
		</tr>
		<tr  bordercolor= "white">
			<td colspan="2">
				<%
					 MostrarFiltros(nRic)
				%>
			</td>
		</tr>
	</table>
	<br>

	<%

	'Response.Write "<br>MostrarFiltros:" & MostrarFiltros
	'Response.Write "<br>MostrarTablaFiltros:" & MostrarTablaFiltros

	'MostrarTablaFiltros = MostrarFiltros
	'Response.Write "<br>MostrarTablaFiltros:" & MostrarTablaFiltros

End Function



Function MostrarFiltros(nRic)
'=================================================================================================
' MostrarFiltros:  Esta funcion construye una tabla con el detalle de los filtros correspondientes
' a el aviso o seleccion que se le pasa como parametro.
'
'=================================================================================================
	dim i, SQL, nFiltros, z, aOpLog, sDescrTab, memId, lung
	dim sDecNomTab, sTipoCampo, sDescNomeCampo, sDenominazione

%>
	<br>
	<table border="0" cellspacing="1" cellpadding="1" width="500" align="center">
	<%
		sSQL =  "SELECT a.ID_TAB,a.ID_CAMPO,a.COD_OPLOG,a.VALORE,a.PRG_REGOLA,a.PRG_REGOLA_RIF, b.DESC_TAB" &_
				" FROM RICHIESTA_REGOLE a, DIZ_TAB b " &_
				" WHERE ID_RICHIESTA=" & nRic &_
				" AND a.ID_TAB = b.ID_TAB " &_
				" ORDER BY ID_TAB,PRG_REGOLA,PRG_REGOLA_RIF"
'response.Write sSQL
        'PL-SQL * T-SQL
        SSQL = TransformPLSQLToTSQL (SSQL)

'response.Write sSQL
		set rsCriteri = CC.Execute(sSQL)
'response.Write rsCriteri.count
		if not rsCriteri.EOF then
		'response.Write "Aca"
			nFiltros = 1
			z = 0
			aOpLog = decodTadesToArray("OPLOG",date,"",1,0)
			'response.Write "Aca"
			'response.Write aOpLog
			sDescrTab= rsCriteri("DESC_TAB")
			memId =  rsCriteri.Fields("ID_TAB")
			%>
			<tr>
				<td colspan="3">
					<table border="0" CELLPADDING="0" CELLSPACING="0" width="500" align="center">
						<tr height="18">
							<td class="sfondomenu" width="350" height="18"><span class="tbltext0"><b>&nbsp;<%=sDescrTab%>   </b></span></td>
							<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
							<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
						</tr>
					</table>
				</td>
			</tr>
			<%
			do while not rsCriteri.EOF
				if rsCriteri.Fields("ID_TAB") <> memId then
					sDescrTab= rsCriteri("DESC_TAB")
					memId =  rsCriteri.Fields("ID_TAB")
					%>
					<tr>
						<td colspan="3">
							<table border="0" CELLPADDING="0" CELLSPACING="0" width="500" align="center">
								<tr height="18">
									<td class="sfondomenu" width="350" height="18"><span class="tbltext0"><b>&nbsp;<%=sDescrTab%>   </b></span></td>
									<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
									<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
								</tr>
							</table>
						</td>
					</tr>
					<%
				end if
				%>
				<tr class="tblsfondo">
					<td class="textblack" align="left" valign="center" width="26%">
						<%
						sSQL = "SELECT ID_DESC,ID_CAMPO,DESC_CAMPO,ID_CAMPO_RIF, ID_TAB_RIF,TIPO_CAMPO,ID_CAMPO_DESC " &_
								" FROM DIZ_DATI " &_
								" WHERE ID_TAB='" & rsCriteri("ID_TAB")	& "'" &_
								" AND ID_CAMPO='" & rsCriteri("ID_CAMPO") & "'"

								'Response.Write " ----- " & sSQL & " ----- " 


                        'PL-SQL * T-SQL
                        SSQL = TransformPLSQLToTSQL (SSQL)
						set rsDecod = CC.Execute(sSQL)


						'**************************************************
                        if not rsDecod.EOF then
                            Response.Write ucase(rsDecod("DESC_CAMPO"))
                            sDecNomTab		= rsDecod("ID_TAB_RIF")
						    sTipoCampo		= rsDecod("Tipo_campo")
						    sDescNomeCampo  = rsDecod("ID_CAMPO_DESC")
						    sDenominazione  = rsDecod("ID_DESC")
						    if isnull(rsDecod("ID_CAMPO_rif")) then
							     sDecNomCampo	= rsDecod("ID_CAMPO")
						    else
						       sDecNomCampo	= rsDecod("ID_CAMPO_RIF")
						    end if
                        end if
      
                        '**************************************************


						rsDecod.Close
						set rsDecod = Nothing%>
					</td>
					<td class="textblack" align="left" valign="center" width="20%">
						<%=(UCASE(searchIntoArray(rsCriteri("COD_OPLOG"),aOpLog,0)))%>
					</td>
					<td class="textblack" align="left" valign="center" width="54%">
						<%
						'Response.Write ucase(sDecNomTab)
						if sDecNomTab <> "" then
							'Response.Write sDecNomTab
							select case mid(sDecNomTab,1,4)
							case "TAB_"
								aValore = Split(rsCriteri("Valore") ,",",-1,1)
								lung = Ubound(aValore)
								sTab = ""

								Isa = 0
								if sDecNomTab = "TAB_INTE" then
									Isa = ""
								end if
								for i = 0 to lung
									sTab = sTab & ucase(DecCodVal(mid(sDecNomTab,5), Isa, date, replace(aValore(i),"$",""),0)) & ","
								next
								Response.Write left(sTab,len(sTab) - 1)
							case "COMU"
								aValore = Split(rsCriteri("Valore"),",",-1,1)
								lung = Ubound(aValore)
								sCom=""
								if lung > 0 then
									for i = 0 to lung
										sSQL = "SELECT DESCOM FROM " & sDecNomTab & " WHERE " &_
										sDecNomCampo & " = " & Replace(aValore(i),"$","'")
'PL-SQL * T-SQL
SSQL = TransformPLSQLToTSQL (SSQL)
										'Response.write sSQL
										set rsDecod = CC.Execute(sSQL)
										sCom = sCom  &  ucase(rsDecod("DESCOM")) & ","
										rsDecod.Close
										set rsDecod = Nothing
									next
									Response.Write left(sCom,len(sCom) - 1)
								else
									sSQL = "SELECT DESCOM FROM " & sDecNomTab & " WHERE " &_
									sDecNomCampo & " ='" & aValore(0) & "'"
'PL-SQL * T-SQL
'SSQL = TransformPLSQLToTSQL (SSQL)
									set rsDecod = CC.Execute(sSQL)
									'response.write sSQL
									Response.write ucase(rsDecod("DESCOM"))
									rsDecod.Close
									set rsDecod = Nothing
								end if
							case else

								if (rtrim(ucase(sDecNomTab)) <> "FIGUREPROFESSIONALI" and rtrim(ucase(sDecNomTab)) <> "AREA_CORSO_SUB") then
									aValore = Split(rsCriteri("Valore"),",",-1,1)
									lung = Ubound(aValore)
									Denom = ""
									for i = 0 to cint(lung)
										sSQL = "SELECT " & sDenominazione & " FROM " & sDecNomTab & " WHERE " &_
											sDecNomCampo & " = " & Trasforma(replace(aValore(i),"$",""),sTipoCampo)

'PL-SQL * T-SQL
SSQL = TransformPLSQLToTSQL (SSQL)
										set rsDecod = CC.Execute(sSQL)
										Denom = Denom & ucase(rsDecod(0)) & ","
										rsDecod.Close
										set rsDecod = Nothing
									next
									Response.Write left(Denom,len(Denom) - 1)
								else
								''''si es post laborales o areas de interes''''
								''''vany''''


									aValore = Split(rsCriteri("Valore"),",",-1,1)

									'Response.Write rsCriteri("Valore")
									'Response.Write trim(ucase(sDecNomTab))

									dim vsql

									if trim(ucase(sDecNomTab)) = "AREA_CORSO_SUB" then
									''''areas de interes



											'sql = ""
											vsql = "SELECT irst.descripcion as denoPrestacion, "
											vsql = vsql & "ac.denominazione as denoAree, "
											vsql = vsql & "acs.DESCRIPCION as denoFigure "
											vsql = vsql & "FROM "
											vsql = vsql & "area_corso ac, "
											vsql = vsql & "area_corso_sub acs, "
											vsql = vsql & "area_grupodetalle agd, "
											vsql = vsql & "area_grupos ag, "
											vsql = vsql & "impresa_rolesserviciostipos irst "
											vsql = vsql & "where "
											vsql = vsql & "acs.id_subarea = "  &  rsCriteri("Valore") & " "  '& Trasforma(replace(aValore(i),"$",""),sTipoCampo) & " "
											vsql = vsql & "and acs.id_area = ac.id_area "
											vsql = vsql & "and agd.id_area = ac.id_area "
											vsql = vsql & "and agd.areagrupo = ag.areagrupo "
											vsql = vsql & "and irst.areagrupo = ag.areagrupo "
											vsql = vsql & "and ag.areaorigen = 2 "

									end if

									if trim(ucase(sDecNomTab)) = "FIGUREPROFESSIONALI" Then
										'Response.Write "aca"
									''''postulaciones laborales


											'sql = ""
											'vsql = "SELECT irst.descripcion as denoPrestacion, "
											'vsql = vsql & "ac.denominazione as denoAree, "
											'vsql = vsql & "acs.denominazione as denoFigure "
											'vsql = vsql & "FROM "
											'vsql = vsql & "aree_professionali ac, "
											'vsql = vsql & "figureprofessionali acs, "
											'vsql = vsql & "area_grupodetalle agd, "
											'vsql = vsql & "area_grupos ag, "
											'vsql = vsql & "impresa_rolesserviciostipos irst "
											'vsql = vsql & "where "
											'vsql = vsql & "acs.id_figprof = " &  rsCriteri("Valore") & " "  '& Trasforma(replace(aValore(i),"$",""),sTipoCampo) & " "
											'vsql = vsql & " and acs.id_areaprof = ac.id_areaprof "
											'vsql = vsql & "and agd.id_area = ac.id_areaprof "
											'vsql = vsql & "and agd.areagrupo = ag.areagrupo "
											'vsql = vsql & "and irst.areagrupo = ag.areagrupo "
											'vsql = vsql & "and ag.areaorigen = 1 "

											vsql ="select DENOMINAZIONE from figureprofessionali where ID_FIGPROF = " & rsCriteri("Valore")

									End If

									'Response.Write "vsql " & vsql
									'Response.End
									'Response.Write ucase(sDecNomTab)


'PL-SQL * T-SQL
VSQL = TransformPLSQLToTSQL (VSQL)
									set rsDecod = CC.Execute(vsql)


									if not rsDecod.eof then
										if trim(ucase(sDecNomTab)) = "AREA_CORSO_SUB" then
											Response.Write "<b><u>Prestación:</u> " & lcase(rsDecod("denoPrestacion")) & "<br><u>Tipolog&iacute;a:</u> " & lcase(rsDecod("denoAree")) & "<br><u>Subtipolog&iacute;a:</u> " & lcase(rsDecod("denoFigure")) & "</b>"
											Response.Write "<hr>"
										ElseIf trim(ucase(sDecNomTab)) = "FIGUREPROFESSIONALI" Then
											'Response.Write "<b><u>Prestación:</u> " & lcase(rsDecod("denoPrestacion")) & "<br><u>Grupo Ocupacional:</u> " & lcase(rsDecod("denoAree")) & "<br><u>Puesto:</u> " & lcase(rsDecod("denoFigure")) & "</b>"
											Denom = Denom & ucase(rsDecod(0)) & ","
											Response.Write left(Denom,len(Denom) - 1)
											Response.Write "<hr>"
										end if
									end if

									rsDecod.Close
									set rsDecod = Nothing
									'Response.Write sDecNomTab
									'Response.write "Working..."
								end if

							end select
						else
							if sDescNomeCampo <> "" then
								aVal = split(sDescNomeCampo,"|")
								for i=0 to ubound(aVal) step 2
									if aVal(i) = replace(rsCriteri("VALORE"),"$","") then
										Response.Write server.HTMLEncode(UCASE(aVal(i+1)))
									end if
								next
							else
								Response.Write server.HTMLEncode(replace(rsCriteri("VALORE"),"$",""))
							end if
						end if
						%>
					</td>
				</tr>
				<%
				rsCriteri.MoveNext
				z=z+1
			loop
			Erase aOpLog
		Else
			nFiltros = 0
			%>
			<tr height="17">
				<td colspan="4" class="tbltext3" align="middle" valign="center">
					<strong>No se registraron filtros para la b&uacute;squeda seleccionada</strong>
					<b><br><%=sDescTab%></b>
				</td>
			</tr>
			<%
		End if
		%>
	</table>
	<%
	rsCriteri.close
	set rsCriteri = nothing

	MostrarFiltros = nFiltros

End Function


'**********************************************************************************************************************************************************************************
Function Trasforma(valore,tipo)
	select case tipo
		case "D"
			Trasforma = ConvDateToDb(valore)
		case "A"
			Trasforma = "'" & Valore & "'"
		case else
			Trasforma = valore
	end select
End Function


'**********************************************************************************************************************************************************************************
'**********************************************************************************************************************************************************************************
'**********************************************************************************************************************************************************************************
'**********************************************************************************************************************************************************************************
Function MostrarTablaDetalles(nRic)
'==================================================
'
'
'
'==================================================
	dim i, SQL, nFiltros, z, aOpLog, sDescrTab, memId, lung
	dim sDecNomTab, sTipoCampo, sDescNomeCampo, sDenominazione

%>
	<table border="1" bordercolor="#006699" cellpadding="8" cellspacing="0" width="520" align="center">

		<tr  bordercolor= "white">
			<td colspan="2" align="center" class="tbltext1">
				<b>DETALLES DEL OFRECIMIENTO</b>
			</td>
		</tr>
		<tr  bordercolor= "white">
			<td colspan="2">
				<%
					 MostrarDetallesRic(nRic)
				%>
			</td>
		</tr>
	</table>
	<br>

	<%

End Function




Function MostrarDetalles(nRic)
'==================================================
'
'
'
'==================================================
	dim i, SQL, nFiltros, z, aOpLog, sDescrTab, memId, lung
	dim sDecNomTab, sTipoCampo, sDescNomeCampo, sDenominazione

%>
	<br>
	<table border="0" cellspacing="1" cellpadding="1" width="500" align="center">
	<%
		sSQL =  "SELECT ro.nome_tab, ro.codice, ro.nota FROM richiesta_offerte ro " &_
				"WHERE id_richiesta =" & nRic & " ORDER BY ro.nome_tab"
'PL-SQL * T-SQL
SSQL = TransformPLSQLToTSQL (SSQL)
		set rsDetalle = CC.Execute(sSQL)
		%>
		<tr>
			<td colspan="3">
				<table border="0" CELLPADDING="0" CELLSPACING="0" width="500" align="center">
					<tr height="18">
						<td class="sfondomenu" width="350" height="18"><span class="tbltext0"><b>&nbsp;<%=sDescrTab%>   </b></span></td>
						<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
						<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
					</tr>
				</table>
			</td>
		</tr>
		<%
		if not rsDetalle.EOF then
			do while not rsDetalle.EOF
				' ??????????????
				sSQLDet = "SELECT Descrizione FROM tades WHERE valore = '" & rsDetalle("nome_tab") & "'"
'PL-SQL * T-SQL
SSQLDET = TransformPLSQLToTSQL (SSQLDET)
				set rsTitDet = CC.Execute(sSQLDet)
				sNomeTab	= rsTitDet("Descrizione")
				rsTitDet.close()
				set rsTitDet = nothing

				' ??????????????
				sSQLDet = "SELECT Descrizione FROM tades WHERE  nome_tabella = '" &_
							 rsDetalle("nome_tab") & "' AND codice = " & rsDetalle("codice")

				'response.write ssqldet
				'Response.End
'PL-SQL * T-SQL
SSQLDET = TransformPLSQLToTSQL (SSQLDET)
				set rsTitDet = CC.Execute(sSQLDet)
				sCodice		= rsTitDet("Descrizione")
				rsTitDet.close()
				set rsTitDet = nothing



				'sSQLDet1 =	"SELECT it.descrizione as categoria,  td.descrizione as Valor " &_
				'			"FROM tades td INNER JOIN idtab it ON td.nome_tabella = it.nome_tabella " &_
				'			"WHERE td.nome_tabella = '" & rsDetalle("nome_tab") & "' AND td.codice = " & rsDetalle("codice")
				' ??????????????
				sNota		= rsDetalle("nota")
				%>
				<tr class="tblsfondo">
					<td class="textblack" align="left" valign="center" width="180">
						<%=sNomeTab%>
					</td>
					<td class="textblack" align="left" valign="center" width="160">
						<%=sCodice%>
					</td>
					<td class="textblack" align="left" valign="center" width="100">
						<%=sNota%>
					</td>
				</tr>
				<%
				rsDetalle.MoveNext
			loop
		Else
			%>
			<tr height="17">
				<td colspan="4" class="tbltext3" align="middle" valign="center">
					<strong>No se registraron detalles para la b&uacute;squeda seleccionada</strong>
					<b><br><%=sDescTab%></b>
				</td>
			</tr>
			<%
		End if
		%>
	</table>
	<%
	rsDetalle.close
	set rsDetalle = nothing

	MostrarFiltros = nFiltros

End Function

Function MostrarDetallesRic(nRic)
'==================================================
'
'
'
'==================================================
	dim sSQL, nFiltros, sCategDetalle
	dim sDecNomTab, sTipoCampo, sDescNomeCampo, sDenominazione, z, aOpLog, sDescrTab, memId, lung

%>
	<table border="0" cellspacing="1" cellpadding="1" width="500" align="center">
	<%
		sSQLRichOff =  "SELECT ro.nome_tab, ro.codice, ro.nota FROM richiesta_offerte ro " &_
				"WHERE id_richiesta =" & nRic & " ORDER BY ro.nome_tab"
'PL-SQL * T-SQL
SSQLRICHOFF = TransformPLSQLToTSQL (SSQLRICHOFF)
		set rsDetalle = CC.Execute(sSQLRichOff)
		 sCategDetalle = ""

		if not rsDetalle.EOF then
			do while not rsDetalle.EOF
				' ??????????????
				sSQLDet = "SELECT Descrizione FROM tades WHERE valore = '" & rsDetalle("nome_tab") & "'"
'PL-SQL * T-SQL
SSQLDET = TransformPLSQLToTSQL (SSQLDET)
				set rsTitDet = CC.Execute(sSQLDet)
				sNomeTab	= rsTitDet("Descrizione")
				rsTitDet.close()
				set rsTitDet = nothing
				'Response.Write "<br>sNomeTab:" & sNomeTab
				'Response.Write "<br>sCategDetalle:" & sCategDetalle
				if  sCategDetalle <> sNomeTab then
					%>
					<tr>
						<td colspan="3">
							<table border="0" CELLPADDING="0" CELLSPACING="0" width="500" align="center">
								<tr height="18">
									<td class="sfondomenu" width="350" height="18"><span class="tbltext0"><b>&nbsp;<%=sNomeTab%>   </b></span></td>
									<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
									<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
								</tr>
							</table>
						</td>
					</tr>
					<%
					sCategDetalle = sNomeTab
				End if

				' ??????????????
				sSQLDet = "SELECT Descrizione FROM tades WHERE  nome_tabella = '" &_
							 rsDetalle("nome_tab") & "' AND codice = " & rsDetalle("codice")

				'response.write ssqldet
				'Response.End
'PL-SQL * T-SQL
SSQLDET = TransformPLSQLToTSQL (SSQLDET)
				set rsTitDet = CC.Execute(sSQLDet)
				sCodice		= rsTitDet("Descrizione")
				rsTitDet.close()
				set rsTitDet = nothing



				'sSQLDet1 =	"SELECT it.descrizione as categoria,  td.descrizione as Valor " &_
				'			"FROM tades td INNER JOIN idtab it ON td.nome_tabella = it.nome_tabella " &_
				'			"WHERE td.nome_tabella = '" & rsDetalle("nome_tab") & "' AND td.codice = " & rsDetalle("codice")
				' ??????????????
				sNota		= rsDetalle("nota")
				%>
				<tr class="tblsfondo">
					<td class="textblack" align="left" valign="center" width="180">
						<%'=sNomeTab%>
					</td>
					<td class="textblack" align="left" valign="center" width="60%">
						<%=sCodice%>
					</td>
					<td class="textblack" align="left" valign="center" width="40%">
						<%=sNota%>
					</td>
				</tr>
				<%
				rsDetalle.MoveNext
			loop
		Else
			%>
			<tr height="17">
				<td colspan="4" class="tbltext3" align="middle" valign="center">
					<strong>No se registraron detalles para la b&uacute;squeda seleccionada</strong>
					<b><br><%=sDescTab%></b>
				</td>
			</tr>
			<%
		End if
		%>
	</table>
	<%

	rsDetalle.close()
	set rsDetalle = nothing

End Function

%>
