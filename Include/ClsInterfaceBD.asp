<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#INCLUDE VIRTUAL= "IncludeMtss/adovbs.asp"-->
<!--#INCLUDE VIRTUAL= "Util/dbutil.asp"-->
<!--#INCLUDE VIRTUAL= "/Include/OpenConn.asp"-->

<%
class InterfaceBD

'dim conn
Public Function GenerarComboTipoMovimientosCuil(Nombre,Clase,Validacion,Seleccion,CUIL)
	Dim Rs
	Dim Sp
	Dim Retorna 
	Dim Consulta
	
	'InicioBD()
		 		 		 		 		 
	Set Rs = Server.CreateObject("adodb.recordset")
	Set Sp = Server.CreateObject("adodb.command")
	
	Sp.ActiveConnection = connLavoro 'InicioBD()
	
	Sp.CommandText = "AgCitaDefineTiposMovimientos('" & Cuil & "',,,'')"

	Sp.CommandType = 4 

'PL-SQL * T-SQL  
	Set Rs = Sp.execute()
	
	Retorna = ""

	If not Rs.eof then 
		Retorna = Retorna & "<Select Name='" & Nombre & "' Class='" & Clase & "' OnChange=" & Validacion & ">"
		Retorna = Retorna & "<option></option>"
		Do while not Rs.eof
			If cstr(Rs("TipoMovimiento")) = cstr(Seleccion) then 
				Seleccionado = "Selected"
			else 
				Seleccionado = ""
			end if 
			
			Retorna = Retorna & "<option value='" & Rs("TipoMovimiento") & "' " & Seleccionado & ">" & Rs("Descripcion") & "</option>"		
			Rs.movenext
		Loop
		Retorna = Retorna & "</Select>"
	end if 
	GenerarComboTipoMovimientosCuil = Retorna 
	
	'FinBD()
	
	Set Rs = Nothing
End Function


Public Function GenerarComboTipoMovimientos(Nombre,Clase,Validacion,Seleccion,Agrupacion)
	Dim Rs
	Dim Consulta
	Dim Retorna 
	
	'InicioBD()
	
	Set Rs = Server.CreateObject("adodb.recordset")
	
	Consulta = "Select TipoMovimiento, Descripcion from AgCitaMovimientosTipos where TIPOAGRUPADOR =" & cint(Agrupacion)

'PL-SQL * T-SQL  
' Conexi�n a SQL2000 - CONSULTA = TransformPLSQLToTSQL (CONSULTA) 
	Set Rs = connLavoro.execute(Consulta)
	
	Retorna = ""

	If not Rs.eof then 
		Retorna = Retorna & "<Select Name='" & Nombre & "' Class='" & Clase & "' OnChange=" & Validacion & ">"
		Retorna = Retorna & "<option></option>"
		Do while not Rs.eof
			If cstr(Rs("TipoMovimiento")) = cstr(Seleccion) then 
				Seleccionado = "Selected"
			else 
				Seleccionado = ""
			end if 
			
			Retorna = Retorna & "<option value='" & Rs("TipoMovimiento") & "' " & Seleccionado & ">" & Rs("Descripcion") & "</option>"		
			Rs.movenext
		Loop
		Retorna = Retorna & "</Select>"
	end if 
	GenerarComboTipoMovimientos = Retorna 
	
'	FinBD()
	
	Set Rs = Nothing
End Function

Public Function GenerarComboTipoCitas(Nombre,Clase,Validacion,Seleccion)
	Dim Rs
	Dim Consulta
	Dim Retorna 
	
'	InicioBD()
	
	Set Rs = Server.CreateObject("adodb.recordset")
	
	Consulta = "Select CitaTipo, Descripcion from AgCitaTipos "
	
'	Set Rs = connLavoro.execute(Consulta)
'PL-SQL * T-SQL  
' Conexi�n a SQL2000 - CONSULTA = TransformPLSQLToTSQL (CONSULTA) 
	Set Rs = connLavoro.execute(Consulta)
	
	Retorna = ""
	
	If not Rs.eof then 
		Rs.movefirst
		Retorna = Retorna & "<Select Name='" & Nombre & "' Class='" & Clase & "' OnChange=" & Validacion & ">"
		Retorna = Retorna & "<option></option>"
		Do while not Rs.eof
			If cstr(Rs("CitaTipo")) = cstr(Seleccion) then 
				Seleccionado = "Selected"
			else 
				Seleccionado = ""
			end if 
			Retorna = Retorna & "<option value='" & Rs("CitaTipo") & "' " & Seleccionado & ">" & Rs("Descripcion") & "</option>"		
			Rs.movenext
		Loop
		Retorna = Retorna & "</Select>"
	end if 
	
	GenerarComboTipoCitas = Retorna 
	
'	FinBD()
	
	Set Rs = Nothing
End Function


Public Function GenerarComboEstados(Nombre,Clase,Validacion,Seleccion)
	Dim Rs
	Dim Consulta
	Dim Retorna 
	
	'InicioBD()
	
	Set Rs = Server.CreateObject("adodb.recordset")
	
	Consulta = "Select Estado, Descripcion from AgCitaEstados"
	
'PL-SQL * T-SQL  
' Conexi�n a SQL2000 - CONSULTA = TransformPLSQLToTSQL (CONSULTA) 
	Set Rs = connLavoro.execute(Consulta)
	
	Retorna = ""
	
	If not Rs.eof then 
		Rs.movefirst
		Retorna = Retorna & "<Select Name='" & Nombre & "' Class='" & Clase & "' OnChange=" & Validacion & ">"
		Retorna = Retorna & "<option></option>"
		Do while not Rs.eof
			If cstr(Rs("Estado")) = cstr(Seleccion) then 
				Seleccionado = "Selected"
			else 
				Seleccionado = ""
			end if 
			Retorna = Retorna & "<option value='" & Rs("Estado") & "' " & Seleccionado & ">" & Rs("Descripcion") & "</option>"		
			Rs.movenext
		Loop
		Retorna = Retorna & "</Select>"
	end if 
	
	GenerarComboEstados = Retorna 
	
'	FinBD()
	
	Set Rs = Nothing
End Function

Public Function GenerarComboDuracionCita(Nombre,Clase,TipoCita,Validacion,Salto,MaxDur)
	Dim Rs
	Dim Consulta
	Dim Retorna 
	'MaxLong,Tamanio,
'	InicioBD()
	
	Set Rs = Server.CreateObject("adodb.recordset")
	
	Consulta = "Select Descripcion, Duracion from AgCitaTipos where CitaTipo =" & cint(TipoCita)
	
'PL-SQL * T-SQL  
' Conexi�n a SQL2000 - CONSULTA = TransformPLSQLToTSQL (CONSULTA) 
	Set Rs = connLavoro.execute(Consulta)
	
	Retorna = ""
	
	If not Rs.eof then 
		Rs.movefirst
		Valor = Rs("Duracion")
		'Retorna = Retorna & "<input name='" & Nombre & "' type='text' Class='" & Clase & "' maxlength='" & MaxLong & "' size='" & Tamanio & "' value='" & Rs("Duracion") & "'>"
	end if 	
	
	Retorna = Retorna & "<select name='" & Nombre & "' onchange='" & Validacion & "' class='" & Clase & "'>"
	
	Seleccionado = ""
	
	for i = Salto to MaxDur step Salto
		if cint(i) = cint(Valor) then 
			Seleccionado = "Selected"
		else
			Seleccionado = ""
		end if
		Retorna = Retorna & "<option value ='" & i & "' " & Seleccionado & ">" & i & " Min</option>"
	next
	

	
	Retorna = Retorna & "</select>"
	
	GenerarComboDuracionCita = Retorna 
	
'	FinBD()
	
	Set Rs = Nothing
End Function 

Public Function ObtenerCitaParaMovimiento(TipoMov)
	Dim Rs
	Dim Consulta
	Dim Retorna 
	
'	InicioBD()
	
	Set Rs = Server.CreateObject("adodb.recordset")
	
	Consulta = "Select CitaTipo from AgCitaMovimientosTipos where TipoMovimiento =" & cint(TipoMov)
	

'PL-SQL * T-SQL  
' Conexi�n a SQL2000 - CONSULTA = TransformPLSQLToTSQL (CONSULTA) 
	Set Rs = connLavoro.execute(Consulta)
	
	Retorna = ""
	
	If not Rs.eof then 
		Retorna = Rs("CitaTipo")
	end if 
	
	ObtenerCitaParaMovimiento = Retorna 
	
'	FinBD()
	
	Set Rs = Nothing
End Function 

Public Function MostrarDisponibilidad(Oficina,Fecha)
	Dim Rs
	Dim Retorna 
	
	Retorna = ""
	
'	InicioBD()
		 	
	
	'Consulta = "Select OFICINA, FECHA, NROPUESTO AS [N� PUESTO], TIEMPODEATENCION AS [T. DE ATENCION], TIEMPOASIGNADO AS [T.ASIGNADO], TIEMPODISPMASLARGO AS [T. DISP. MAS LARGO] from AgAgendaEstado A where A.Fecha =" & ConvDateToDBsSQL(Fecha) & " and A.oficina  = " & Oficina 

	Consulta = "Select NROPUESTO AS [N� PUESTO], TIEMPODEATENCION AS [TIEMPO DE ATENCION], TIEMPOASIGNADO AS [TIEMPO ASIGNADO], TIEMPODISPMASLARGO AS [TIEMPO DISPONIBLE MAS LARGO] from AgAgendaEstado A where A.Fecha =" & ConvDateToDBsSQL(Fecha) & " and A.oficina  = " & Oficina 

	Set Rs = Server.CreateObject("adodb.recordset")
	
'PL-SQL * T-SQL  
' Conexi�n a SQL2000 - CONSULTA = TransformPLSQLToTSQL (CONSULTA) 
	Set Rs = connLavoro.execute(Consulta)
	
	If not Rs.eof then 
			Retorna = Retorna & "<tr>"
			for each x in Rs.fields
				Retorna = Retorna & "<td nowrap width = '25%' align='center' class='tbltext1' bgcolor='LightYellow'><b>" & x.name & "</b></td>"
			next
			Retorna = Retorna & "</tr>"	
		
		do while not Rs.eof
			Retorna = Retorna & "<tr>"
			for each x in Rs.fields
				select case x.name
				case "TIEMPO DE ATENCION", "TIEMPO ASIGNADO", "TIEMPO DISPONIBLE MAS LARGO"
					totX = ObtenerHoras(x.value) & " hs. " & ObtenerMinutos(x.value) & " min."
					
					ValorX = totX
				case else
					ValorX = x.value
				end select
				
				Retorna = Retorna & "<td align='center' class='tbltext1' bgcolor='LightYellow'>" & ValorX & "</td>"				
			next
			Retorna = Retorna & "</tr>"	
			Rs.movenext
		loop		
	end if 
	
'	FinBD()

	set Rs = nothing
	
	MostrarDisponibilidad = Retorna 
End Function

Public Function GenerarFilasPorEstado(Oficina,Fecha,Estado)
	Dim Rs
	Dim Retorna 
	
	Retorna = ""
	
'	InicioBD()
		 	
	
	Consulta = "Select A.NROCITA, A.OFICINA, A.FECHA, A.HORADESDE, A.HORAHASTA, B.DESCRIPCION as ESTADO, C.DESCRIPCION as [TIPO DE CITA], A.CUIL from AgCitas A, AgCitaEstados B, AgCitaTipos C where A.Estado = B.Estado and A.CitaTipo = C.CitaTipo and A.Fecha =" & ConvDateToDBsSQL(Fecha) & " and A.Estado = " & Estado & " and A.oficina  = " & Oficina & " ORDER BY HORADESDE ASC"

	Set Rs = Server.CreateObject("adodb.recordset")
	
'PL-SQL * T-SQL  
' Conexi�n a SQL2000 - CONSULTA = TransformPLSQLToTSQL (CONSULTA) 
	Set Rs = connLavoro.execute(Consulta)
	
	If not Rs.eof then 
			Retorna = Retorna & "<tr>"
			for each x in Rs.fields
				if ucase(x.name) <> "HORADESDE" and ucase(x.name) <> "HORAHASTA" then 
					Retorna = Retorna & "<td width = '25%' align='center' class='tbltext1' bgcolor='LightYellow'><b>" & x.name & "</b></td>"
				else
					if ucase(x.name) = "HORADESDE" then
						nom = "HORA DESDE"
					else
						nom = "HORA HASTA"
					end if
					
					Retorna = Retorna & "<td align='center' class='tbltext1' bgcolor='LightYellow'><b>" & nom & "</b></td>"
				end if 
			next
			Retorna = Retorna & "</tr>"	
		
		do while not Rs.eof
			Retorna = Retorna & "<tr>"
			for each x in Rs.fields
				if ucase(x.name) <> "HORADESDE" and ucase(x.name) <> "HORAHASTA" then 
					Retorna = Retorna & "<td align='center' class='tbltext1' bgcolor='LightYellow'>" & x.value & "</td>"
				else
					Retorna = Retorna & "<td align='center' class='tbltext1' bgcolor='LightYellow'>" & ConvertirMinutosAHorario(x.value) & "</td>"
				end if 
			next
			Retorna = Retorna & "</tr>"	
			Rs.movenext
		loop		
	end if 
	
'	FinBD()

	set Rs = nothing
	
	GenerarFilasPorEstado = Retorna 
End Function

Public Function GenerarFilasRangosPosibles(Oficina,Fecha,Opciones,Puesto,Duracion)
	Dim Rs
	Dim Sp
	Dim Retorna 
	
'	InicioBD()
		 		 		 		 		 
	Set Rs = Server.CreateObject("adodb.recordset")
	Set Sp = Server.CreateObject("adodb.command")
	
	Sp.ActiveConnection = connLavoro 'InicioBD()
	
	Sp.CommandText = "AgAgendaEstadoConsultaDia(" & Oficina & ",'" & Fecha & "'," & Opciones & "," & Puesto & "," & Duracion & ",0,'')"

	Sp.CommandType = 4 

'PL-SQL * T-SQL  
	Set Rs = Sp.Execute()
	
	Retorna = ""

	If not Rs.eof then 
			Retorna = Retorna & "<tr>"
			for each x in Rs.fields
				if x.name <> "HoraDesde" and x.name <> "HoraHasta" and x.name <> "x" then 
					Retorna = Retorna & "<td width = '25%' align='center' class='tbltext1' bgcolor='LightYellow'><b>" & x.name & "</b></td>"
				end if 
			next
			Retorna = Retorna & "</tr>"	
		
		do while not Rs.eof
			Retorna = Retorna & "<tr>"
			for each x in Rs.fields
				if x.name <> "HoraDesde" and x.name <> "HoraHasta" and x.name <> "x" then 
					Retorna = Retorna & "<td align='center' class='tbltext1' bgcolor='LightYellow'>" & x.value & "</td>"
				end if 
			next
			Retorna = Retorna & "</tr>"	
			Rs.movenext
		loop		
	end if 
	
	GenerarFilasRangosPosibles = Retorna 
	
'	FinBD()
	
	Set Rs = Nothing
	Set Sp = Nothing
End Function 

Public Function GenerarStringFranjasDisponibles(Oficina,Fecha,Opciones,Puesto,Duracion)
	Dim Rs
	Dim Sp
	Dim Retorna 
	
'	InicioBD()
	
	Set Rs = Server.CreateObject("adodb.recordset")
	Set Sp = Server.CreateObject("adodb.command")
	
	Sp.ActiveConnection = connLavoro 'InicioBD()
	
	Sp.CommandText = "AgAgendaEstadoConsultaDia(" & Oficina & ",'" & Fecha & "'," & Opciones & "," & Puesto & "," & Duracion & ",0,'')"

	Sp.CommandType = 4 

'PL-SQL * T-SQL  
	Set Rs = Sp.Execute()
	
	Retorna = ""

	If not Rs.eof then 
		do while not Rs.eof
			for each x in Rs.fields
				if x.name = "HoraDesde" or x.name = "HoraHasta" then 
					Retorna = Retorna & x.value & ","
				end if 
			next
			if right(Retorna,1) = "," then 
				Retorna = left(Retorna,len(Retorna)-1)
			end if
			Retorna = Retorna & "|"
			Rs.movenext
		loop		
	end if 
	
	if right(Retorna,1) = "|" then 
		Retorna = left(Retorna,len(Retorna)-1)
	end if
	
	GenerarStringFranjasDisponibles = Retorna 
	
'	FinBD()
	
	Set Rs = Nothing
	Set Sp = Nothing
End Function 


Public Function GenerarArrayTipoDia(Oficina,Anio,Mes,Duracion,Cuil,TipoMov,TipoCita)

	Dim Retorna 
	
	Stat = 1
	Ms = "OK"
	
	Retorna = ""
	
			Set Sp 				= Server.CreateObject("ADODB.Command")
			Set Rec 			= Server.CreateObject("ADODB.Recordset")
			
			Sp.ActiveConnection = connLavoro 'InicioBD()
			
		if Cuil = "" and TipoMov = "" and TipoCita = "" then 
			Sp.CommandText = "AgAgendaEstadoConsultaMes(" & cint(Oficina) & ", "  & cint(Anio) & ", " & cint(Mes) & ", " & cint(Duracion) & ", " & cint(Stat) & ", '" & cstr(Ms) & "')"
		else		
			Sp.CommandText = "AgAgendaEstadoConsultaMesCuil(" & cint(Oficina) & ", "  & cint(Anio) & ", " & cint(Mes) & ", " & cint(Duracion) & ",'" & Cuil & "'," & cint(TipoMov) & "," & cint(TipoCita) & ", " & cint(Stat) & ", '" & cstr(Ms) & "')"
		end if
			
			'..Response.Write Sp.CommandText
			'..Response.end

			Sp.CommandType 		= 4 'adCmdStoredProc
			
'PL-SQL * T-SQL  
			Set Rec = Sp.Execute()
			Set Sp = Nothing
'			
'			'ACA LO PUEDO PASAR A UN ARRAY O USAR DIRECTAMENTE EL RECORDSET QUE CONTIENE LOS DATOS..
			
			If Not Rec.EOF then 
				Do While not Rec.EOF 
					DiaTipo = Rec("DiaTipo")
					DispMasLargo = Rec("DisponibleMasLargo")
					TiempoAsignado = Rec("TotTiempoAsignado")
					TiempoAtencion = Rec("TotTiempoDeAtencion")
					'DiaPresente    = Rec("DiaPresente")
					
					HayDisponible = Rec("HayDisponible")
					
					select case HayDisponible
							
						case 1 ' Dia no laborable
							TipoDia = "F"
						case 2 ' Dia anterior a la fecha de hoy
							TipoDia = "P"
						case 3 ' No hay Disponible para la duracion
							if DispMasLargo = 0 then
								TipoDia = "C"
							else
								TipoDia = "C" 'era P
							end if
						case 4 ' Hay uno solo disponible (aproximadamente 1 por puesto habilitado)
							TipoDia = "M"
						case 5 ' Hay bastante  (aproximadamente mas de 1 por puesto habilitado)
							if DispMasLargo > 0 and TiempoAsignado = 0 then 
								TipoDia = "L"
							else
								TipoDia = "A"
							end if
						case else
							TipoDia = "F"	 												
					end select
					
					Select case cstr(Rec("Dia"))
					case 0,1,2,3,4,5,6,7,8,9
						Dia = "0" & cstr(Rec("Dia"))
					case else
						Dia = cstr(Rec("Dia"))
					end select
					
					Retorna = Retorna & "|" & Dia & "|" & TipoDia
					
					Rec.movenext					
				Loop
			End If
		
			If right(Retorna,1) = "|" then 
				Retorna = left(Retorna,len(Retorna)-1)
			End if 
			if left(Retorna,1) = "|" then 
				Retorna = right(Retorna,len(Retorna)-1)
			end if 
			
			GenerarArrayTipoDia = Retorna 
			
'		FinBD()
End Function


Public Function GenerarArraySaltos(Oficina,Anio,Mes,Duracion,SoloMuestra,Cuil,TipoMov,TipoCita)

	Dim Retorna 
	
	if SoloMuestra = false then 
		Salto1 = "MostrarCalendario.asp"
		Salto2 = "IngresarDatosCita.asp"
	else	
		Salto1 = "DispCitasMes.asp"
		Salto2 = "MostrarDisponibilidadMes.asp"
	end if 
	
	Stat = 1
	Ms = "OK"
	
	Retorna = ""
	
			Set Sp 				= Server.CreateObject("ADODB.Command")
			Set Rec 			= Server.CreateObject("ADODB.Recordset")

			Sp.ActiveConnection = connLavoro
		
		if Cuil = "" and TipoMov = "" and TipoCita = "" then 
			Sp.CommandText = "AgAgendaEstadoConsultaMes(" & Oficina & "," & Anio & "," & Mes & "," & Duracion & "," & Stat & ",'" & Ms & "')"
		else
			Sp.CommandText = "AgAgendaEstadoConsultaMesCuil(" & Oficina & "," & Anio & "," & Mes & "," & Duracion & ",'" & Cuil & "'," & cint(TipoMov) & "," & cint(TipoCita) & ", " & Stat & ",'" & Ms & "')"
		end if

			
			Sp.CommandType = 4 'adCmdStoredProc
			
			
'PL-SQL * T-SQL  
			Set Rec = Sp.Execute()
			Set Sp = Nothing
'			
'			'ACA LO PUEDO PASAR A UN ARRAY O USAR DIRECTAMENTE EL RECORDSET QUE CONTIENE LOS DATOS..
			
			If Not Rec.EOF then 
				Do While not Rec.EOF 
					DiaTipo = Rec("DiaTipo")
					DispMasLargo = Rec("DisponibleMasLargo")
					TiempoAsignado = Rec("TotTiempoAsignado")
					TiempoAtencion = Rec("TotTiempoDeAtencion")
					'DiaPresente = Rec("DiaPresente")
					HayDisponible = Rec("HayDisponible")

					select case HayDisponible
							
						case 1 ' Dia no laborable
							TipoDia = Salto1
						case 2 ' Dia anterior a la fecha de hoy
							TipoDia = Salto1
						case 3 ' No hay Disponible para la duracion
							if DispMasLargo = 0 then
								TipoDia = Salto1
							else
								TipoDia = Salto1
							end if
						case 4 ' Hay uno solo disponible (aproximadamente 1 por puesto habilitado)
							TipoDia = Salto2
						case 5 ' Hay bastante  (aproximadamente mas de 1 por puesto habilitado)
							if DispMasLargo > 0 and TiempoAsignado = 0 then 
								TipoDia = Salto2
							else
								TipoDia = Salto2
							end if
						case else
							TipoDia = Salto1	 												
					end select
							
					Select case cstr(Rec("Dia"))
					case 0,1,2,3,4,5,6,7,8,9
						Dia = "0" & cstr(Rec("Dia"))
					case else
						Dia = cstr(Rec("Dia"))
					end select
					
					Retorna = Retorna & "|" & Dia & "|" & TipoDia
					
					Rec.movenext
				Loop
			End If
		
			If right(Retorna,1) = "|" then 
				Retorna = left(Retorna,len(Retorna)-1)
			End if 
			if left(Retorna,1) = "|" then 
				Retorna = right(Retorna,len(Retorna)-1)
			end if 
			
			GenerarArraySaltos = Retorna
			
'		FinBD()
End Function

Public Function GenerarComboMotivos(Nombre,Clase,Validacion,Seleccion,TipoMovimiento)
	Dim Rs
	Dim Consulta
	Dim Contador
	Dim Retorna 
	
'	InicioBD()
	
	Set Rs = Server.CreateObject("adodb.recordset")
	
	Consulta = "Select Motivo, Descripcion from AgCitaMovimientosTiposMotivos where TipoMovimiento = " & cint(TipoMovimiento) & " order by Motivo, Descripcion"
	
'PL-SQL * T-SQL  
' Conexi�n a SQL2000 - CONSULTA = TransformPLSQLToTSQL (CONSULTA) 
	Set Rs = connLavoro.execute(Consulta)
	
	Retorna = ""
	
	Contador = 0
	
	If not Rs.eof then 
		Rs.movefirst
		Retorna = Retorna & "<Select Name='" & Nombre & "' Class='" & Clase & "' OnChange=" & Validacion & ">"
		Retorna = Retorna & "<option></option>"
		Do while not Rs.eof
			If cstr(Rs("Motivo")) = cstr(Seleccion) then 
				Seleccionado = "Selected"
			else 
				Seleccionado = ""
			end if 			
			IdMotivo = Rs("Motivo")
			Retorna = Retorna & "<option value='" & Rs("Motivo") & "' " & Seleccionado & ">" & Rs("Descripcion") & "</option>"		
			Rs.movenext
			Contador = Contador + 1
		Loop
		Retorna = Retorna & "</Select>"
		
		If Contador = 1 and cint(IdMotivo) = 0 then 
			Retorna = ""
		end if 	
	else
		Retorna = ""
	end if
	
	GenerarComboMotivos = Retorna 
	
'	FinBD()
	
	Set Rs = Nothing
End Function

Public Function ObtenerDuracionCita(NroCita)
	Dim Rs
	Dim Consulta
	Dim Retorna 
	
'	InicioBD()
	
	Set Rs = Server.CreateObject("adodb.recordset")
	
	Consulta = "Select HoraDesde, HoraHasta from AgCitas where NroCita = " & cint(NroCita)
	
'PL-SQL * T-SQL  
' Conexi�n a SQL2000 - CONSULTA = TransformPLSQLToTSQL (CONSULTA) 
	Set Rs = connLavoro.execute(Consulta)
	
	Retorna = ""
	
	If not Rs.eof then 
		Retorna = cint(rs("HoraHasta")) - cint(rs("HoraDesde"))
	end if 
	
	ObtenerDuracionCita = Retorna 
	
'	FinBD()
	
	Set Rs = Nothing
End Function

Public Function GrabarMovimiento(Oficina, TipoMovimiento, CitaAnteriorNro, CitaNro, TipoCita, FechaCita, HoraDesde, Duracion, Cuil, NroEntrevista, Motivo, MotivoDesc, NroMovimientoAsociado, RefExternaTipo, RefExternaNro, Usuario)
'---------------------------------------------------------------------------------------------------
' Que hace ??? Las ideas
'
' que parametros son de entrada
'NECESITA:

' "OFICINA"
' "TIPOMOVIMIENTO" 
' "CITAANTERIOR"
' "CITANRO"
' "TIPOCITA"
' "FECHACITA"
' "HORADESDE"
' "HORAHASTA"
' "CUIL"
' "NROENTREVISTA"
' "MOTIVO"
' "MOTIVODESC"
' "NROMOVIMIENTOSASOCIADO"
' "REFEXTERNATIPO"
' "NREFEXTERNANRO"
' "USUARIO"
'
' que parametros son de salida
'DEVUELVE:

' "NROMOVIMIENTO"
' "PUESTO"
' "STATUS"
' "MENSAJE"
'---------------------------------------------------------------------------------------------------

	Dim Spx
	Dim Retorna 
	Dim HoraHasta
	Dim Horas
	Dim Minutos


	if len(HoraDesde) > 0 then 
		Horas = cint(left(HoraDesde,2)) * 60
		Minutos = cint(right(HoraDesde,2))
		 
		HoraDesde = Horas + Minutos
		HoraHasta = Horas + Minutos + cint(Duracion)
	end if 
	
	'''
	NroEntrevista = 0
	'''
	
	Set Spx = Server.CreateObject("adodb.command")
	with Spx
		.activeconnection = connLavoro
		.Commandtext = "AgAgendaMovimientoGraba"
		.commandtype = adCmdStoredProc
		'--------------------------------------------
		' CArga parametros
		'--------------------------------------------
		For Each Parameter In .Parameters
			If Parameter.Direction And adParamInput Then
				Select case ucase(Mid(Parameter.Name, 2))
					case "OFICINA"
						Parameter.value = Oficina	
					CASE "TIPOMOVIMIENTO" 
						Parameter.value = TipoMovimiento	
					CASE "CITAANTERIORNRO"
						if len(CitaAnteriorNro) = 0 then 
							CitaAnteriorNro = null 
						end if 
						Parameter.value = CitaAnteriorNro	
					CASE "CITANRO"
						if len(CitaNro) = 0 then 
							CitaNro = 0
						end if
						Parameter.value = CitaNro	
					CASE "TIPOCITA"
						Parameter.value = TipoCita	
					CASE "FECHACITA"
						Parameter.value = FechaCita	
					CASE "HORADESDE"
						if len(HoraDesde) = 0 then 
							HoraDesde = null
						end if 
						Parameter.value = HoraDesde	
					CASE "HORAHASTA"
						if len(HoraHasta) = 0 then
							HoraHasta = null
						end if 
						Parameter.value = HoraHasta	
					CASE "CUIL"
						Parameter.value = Cuil	
					CASE "NROENTREVISTA"
						Parameter.value = NroEntrevista	
					CASE "MOTIVO"
						if len(Motivo) = 0 then 
							Motivo = null 
						end if 
						Parameter.value = Motivo	
					CASE "MOTIVODESC"
						Parameter.value = MotivoDesc	
					CASE "NROMOVIMIENTOSASOCIADO"
						Parameter.value = NroMovimientoAsociado	
					CASE "REFEXTERNATIPO"
						if len(RefExternaTipo) = 0 then 
							RefExternaTipo = 0
						end if
						Parameter.value = RefExternaTipo	
					CASE "NREFEXTERNANRO"
						if len(RefExternaNro) = 0 then 
							RefExternaNro = 0
						end if
						Parameter.value = RefExternaNro	
					CASE "USUARIO"
						Parameter.value = Usuario	
					'CASE "NROMOVIMIENTO"
					'	Parameter.value = Oficina	
					'CASE "PUESTO"
					'	Parameter.value = Oficina	
					CASE "STATUS"
						if len(Status) = 0 then 
							Status = 0
						end if
						Parameter.value = Status
					CASE "MENSAJE"
						Parameter.value = "" & MENSAJE	
					CASE ELSE
						' ????
				END SELECT
			End If
		Next
		'--------------------------------------------
		' Ejecuta
		'--------------------------------------------
'PL-SQL * T-SQL  
XXX = TransformPLSQLToTSQL (XXX) 
		.Execute XXX, , adexecutenorecords
		
		'--------------------------------------------
		' Devuelve parametros
		'--------------------------------------------
		For Each Parameter In .Parameters
			If Parameter.Direction And (adParamOutput Or adParamReturnValue) Then
				Select case ucase(Mid(Parameter.Name, 2))
					case "STATUS"
						STATUS = trim(Parameter.value)
					case "MENSAJE"
						MENSAJE = trim(Parameter.value)
					case "CITANRO"
						CITANRO = trim(Parameter.value)
					case else
						' ???
				end select
			End If
		Next
	end with
	
	'--------------------------------------------
	' Prepara salida
	'--------------------------------------------
	if STATUS <> 1 then
		Retorna = "Error: " & STATUS & "<br> Mensaje: " & MENSAJE
	else
		Retorna = CITANRO
	end if

	'retorna el nro de cita que genera..
	GrabarMovimiento = Retorna 
	
'	FinBD()
	
	Set Spx = Nothing
End Function


Public function ListarReservas(Oficina,Usuario,Lote,CantEntrev,Observaciones,DirSede)

Oficina = Oficina           
RefExternaTipo = 1         
RefExternaNro = Lote        
CantEntrevistadores = CantEntrev    
CantFormHojas = 3          
Observaciones = Observaciones      
Usuario = Usuario
               
Secuencia = 1           
NroCopia = 1            
Status = 1              
Mensaje = "OK" 


DirSede = DirSede


'InicioBD()

	Dim Sp
	Dim Retorna 
	
	Retorna = "" 
	
	Set Sp = Server.CreateObject("adodb.command")

	Set Rec = Server.CreateObject("ADODB.Recordset")

	Sp.ActiveConnection = connLavoro 'InicioBD()

	Sp.CommandText = "AgReservasListado(" & Oficina & "," & RefExternaTipo & "," & RefExternaNro & "," & CantEntrevistadores & "," & CantFormHojas & ",'" & Observaciones & "'," & Usuario & "," & Secuencia & "," & NroCopia & "," & Status & ",'" & Mensaje & "')"
			
	Sp.CommandType = 4 'adCmdStoredProc
			

'PL-SQL * T-SQL  
	Set Rec = Sp.Execute()
	Set Sp = Nothing


if Rec.state = 1 then
	if not Rec.eof then
		Contador = 0
		
		Rec.movefirst
		
		Retorna = Retorna & "<br>"

		Retorna = Retorna & "<div align='center'>"
		Retorna = Retorna & "	<table border='0' cellspacing='0' cellpadding='0'>"
		
		do while not Rec.eof 
			
			Contador = Contador + 1
			
			if (Rec.fields(10) = 1) or (Contador = 1) then  'PaginaSalto
			'	'Retorna = Retorna & "<P align='right'>P�gina " & Rec.fields(8) & " de " & Rec.fields(9) & "</P>" 'PaginaNro y PaginaTotal
				
				'Retorna = Retorna & "<tr><td colspan=2>SALTO</td></tr>"
				if Contador <> 1 then 
					Retorna = Retorna & "<tr style='PAGE-BREAK-AFTER: always'><td colspan=2>&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>"
				end if 

				Retorna = Retorna & " <tr><td colspan=4>"
				Retorna = Retorna & " <table cellpadding=0 cellspacing=0 width = '100%' border='0'>"
				Retorna = Retorna & "    <tr>"
				Retorna = Retorna & "	<td align='left'>"
				'Retorna = Retorna & "		"
				Retorna = Retorna & "		<b class='tbltext1'>Lote Nro: " & Lote & "</b><br>"
				Retorna = Retorna & "		<b class='tbltext1'>Observaciones: " & ucase(Observaciones) & "</b><br>"
				Retorna = Retorna & "		<b class='tbltext1'>Entrevistador Nro.: " & Rec.fields(6) & "</b><br>" 'Entrevistador
				Retorna = Retorna & "	</td>"
				Retorna = Retorna & "	</tr>"
				Retorna = Retorna & "    <tr>"
				Retorna = Retorna & "    <td class='tbltext1' align='left'><b>Hoja " & Rec.Fields(8) & " de " & Rec.fields(9) & "</b>" 'PaginaNro y PaginaTotal
				Retorna = Retorna & "    </td>"
				Retorna = Retorna & "    </tr>"

				Retorna = Retorna & "	<tr height='2'>"
				Retorna = Retorna & "		<td colspan='3'><br></td>"
				Retorna = Retorna & "	</tr>"

				'Retorna = Retorna & "	<tr height='2'>"
				'Retorna = Retorna & "		<td colspan='3'>"
				'Retorna = Retorna & "		<td colspan='3' class='SFONDOCOMMAZ' background='" & Session("Progetto") & "/images/separazione.gif'>"
				'Retorna = Retorna & "		</td>"
				'Retorna = Retorna & "	</tr>"
				Retorna = Retorna & "</table>"
				Retorna = Retorna & "</td></tr>"
			end if		
				
			
			if (Rec.fields(10) = 0) and (Contador <> 1) then
				Retorna = Retorna & "<tr><td valign='top' colspan=4>..............................................................................................................................................................</td></tr>"
				Retorna = Retorna & "<tr><td colspan=4><br></td></tr>"
			end if 
			
			Retorna = Retorna & "<tr>"
			
			for i = 1 to 2 

				Retorna =   Retorna & "<td width=220>"
			
				Retorna	=	Retorna	&	"<table	border=1 cellpadding=1 cellspacing=1 bordercolor=black class='textblack'>"
				Retorna	=	Retorna	&	"	<tr	bordercolor=white>"
				''''giffffffffffffffffff
				Retorna	=	Retorna	&	"		<td	width='10%'><img src=" & Session("Progetto") & "/Images/logonotif.gif></img></td>"
				Retorna	=	Retorna	&	"		<td	colspan=3><b>Seguro	de Capacitaci�n	y	Empleo <br>Citaci�n	para Entrevista	Inicial	<br><br></b></td>"
				Retorna	=	Retorna	&	"	</tr>"
																																																		
				Retorna	=	Retorna	&	"	<tr	bordercolor=white>"
				Retorna	=	Retorna	&	"		<td	colspan=4>Me comprometo	a	concurrir	a	la entrevista	inicial	el d�a <b>" & Rec.fields(1) 'CitaFecha
				Retorna	=	Retorna	&	"</b> a	la hora	<b>" & Rec.fields(2) & "</b> en la	Oficina	de Empleo	local	situada	en <b>" & DirSede & "</b>" 'CitaHora
				'Retorna	=	Retorna	&	"		xxxxxxx	xxxx xxxx	xxxxx	xxxx xxx xxx xx	xxx	xxxxx	xxxx xxxx"
				Retorna	=	Retorna	&	"		</td>"
				Retorna	=	Retorna	&	"	</tr>"

				Retorna	=	Retorna	&	"	<tr	bordercolor=white>"
				Retorna	=	Retorna	&	"		<td	height=20 width='25%'><b>Nombre:</b></td>"
				Retorna	=	Retorna	&	"		<td	height=20	colspan=3>...........................................................</td>"
				Retorna	=	Retorna	&	"	</tr>"
					
				Retorna	=	Retorna	&	"	<tr	bordercolor=white>"
				Retorna	=	Retorna	&	"		<td	height=15	width='25%'><b>C.I.:</b></td>"
				Retorna	=	Retorna	&	"		<td	height=15	colspan=3>...........................................................</td>"
				Retorna	=	Retorna	&	"	</tr>"

				Retorna	=	Retorna	&	"	<tr	bordercolor=white>"
				Retorna =   Retorna &   "	<td height=15 colspan=4>"
				Retorna =   Retorna &   "	<table border=0 cellpadding=0 cellspacing=0 width='100%' class='textblack'>"
				Retorna =   Retorna &   "	<tr>"
				Retorna	=	Retorna	&	"		<td	width='20%' nowrap height=15><b>Tipo Doc.:</b></td>"
				Retorna	=	Retorna	&	"		<td	width='5%'  height=15>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;...............</td>"
				Retorna	=	Retorna	&	"		<td	width='20%' height=15><b>N�mero:</b></td>"
				Retorna	=	Retorna	&	"		<td	width='45%' height=15>................................</td>"
				Retorna =   Retorna &   "	</tr>"
				Retorna =   Retorna &   "	</table>"
				Retorna	=	Retorna	&	"	</td>"
				Retorna =   Retorna &   "	</tr>"

				Retorna	=	Retorna	&	"	<tr	height=10	bordercolor=white>"
				Retorna	=	Retorna	&	"		<td	colspan=4>Si por alguna	raz�n	no pudiera concurrir en	la fecha indicada,"
				Retorna	=	Retorna	&	"		me remitir�	a	la Oficina de	Empleo para	definir	una	nueva	cita."
				Retorna	=	Retorna	&	"		</td>"
				Retorna	=	Retorna	&	"	</tr>"
					
				Retorna	=	Retorna	&	"	<tr	bordercolor=white>"
				Retorna	=	Retorna	&	"		<td	colspan=4>"
				Retorna	=	Retorna	&	"			<table width='100%'	cellpadding=0 cellspacing=0 class='textblack'>"
				Retorna	=	Retorna	&	"				<tr	height=15>"
				Retorna	=	Retorna	&	"					<td	width='50%'	align='center'>........................................</td>"
				Retorna	=	Retorna	&	"					<td	width='50%'	align='center'>........................................</td>"
				Retorna	=	Retorna	&	"				</tr>"
				Retorna	=	Retorna	&	"				<tr	height=4>"
				Retorna	=	Retorna	&	"					<td	align='center'><b>Firma	Entrevistador</b></td>"
				Retorna	=	Retorna	&	"					<td	align='center'><b>Firma	Beneficiario</b></td>"
				Retorna	=	Retorna	&	"				</tr>"
				Retorna	=	Retorna	&	"			</table>"
				Retorna	=	Retorna	&	"		</td>"
				Retorna	=	Retorna	&	"	</tr>"

				Retorna	=	Retorna	&	"	<tr	bordercolor=white>"
				Retorna	=	Retorna	&	"		<td	colspan=4>"
				Retorna	=	Retorna	&	"			<table cellpadding=1 cellspacing=1 width=100%	class='textblack'>"
				Retorna	=	Retorna	&	"				<tr height=3>" 
				Retorna	=	Retorna	&	"					<td	width=35%   align='left'>" & Rec.fields(11) & "</td>" 'FechaHoraEmision
				Retorna	=	Retorna	&	"					<td	width=20%	align='right'><b>N�	Lote:</b></td>"
				Retorna	=	Retorna	&	"					<td	width=10%	align='left'>" & Rec.fields(4) & "</td>" 'Nrolote
				Retorna	=	Retorna	&	"					<td	width=20%	align='right'><b>N�	Cita:</b></td>"
				Retorna	=	Retorna	&	"					<td	width=15%	align='left'>" & Rec.fields(0) & "</td>" 'CitaNro
				Retorna	=	Retorna	&	"				</tr>"
				Retorna	=	Retorna	&	"			</table>"
				Retorna	=	Retorna	&	"		</td>"
				Retorna	=	Retorna	&	"	</tr>"

				Retorna	=	Retorna	&	"	<tr	bordercolor=white height=3>"
				Retorna	=	Retorna	&	"		<td	colspan=4>"
				Retorna	=	Retorna	&	"			<table width=100% cellpadding=1 cellspacing=1 class='textblack'>"
				Retorna	=	Retorna	&	"				<tr>"
				Retorna	=	Retorna	&	"					<td>"
			
			
				if Rec.fields(5) > 1 then 
					IMPR = "Reimpresi�n"
				else
					IMPR = "Impresi�n"
				end if 
			
				Retorna	=	Retorna	&	IMPR  & " N�mero: " & Rec.fields(5) 'CopiaNro
				Retorna	=	Retorna	&	"					</td>"
				Retorna	=	Retorna	&	"					<td>"
				
				if i = 1 then 
					V1 = "Original "
					V2 = "Oficina de Empleo"
				else
					V1 = "Duplicado "
					V2 = "Beneficiario"				
				end if 
				
				Retorna	=	Retorna	& V1 & V2
				Retorna	=	Retorna	&	"					</td>"
				Retorna	=	Retorna	&	"				</tr>"
				Retorna	=	Retorna	&	"	</table>"
				Retorna	=	Retorna	&	"	</td>"
				Retorna	=	Retorna	&	"	</tr>"
				Retorna	=	Retorna	&	"</table>" 
			
				Retorna = Retorna & "</td>"
				Retorna = Retorna & "<td colspan=2>&nbsp;&nbsp;&nbsp;&nbsp;</td>"	

			next
			Retorna = Retorna & "</tr>"		
			
			Rec.movenext		
		loop
		
		Retorna = Retorna & "		</table>"
		Retorna = Retorna & "</div>"
	end if
end if 
	

		
FinBD()
	
	ListarReservas = Retorna

	Set Rec = nothing
	
end function


Public Function GrabarReservas(Oficina, TipoMovimiento, FechaBase, CantReservas, Motivo, MotivoDesc, Usuario)
'---------------------------------------------------------------------------------------------------
' Que hace ??? Las ideas
'
' que parametros son de entrada
'NECESITA:

' Oficina
' TipoMovimiento
' FechaBase
' CantReservas
' Motivo
' MotivoDesc
' Usuario
'
' que parametros son de salida
'DEVUELVE:

' "Nrolotereservas"
' "Status"
' "Mensaje"
'---------------------------------------------------------------------------------------------------

	Dim Spx
	Dim Retorna 
	
	Set Spx = Server.CreateObject("adodb.command")
	with Spx
		.activeconnection = connLavoro 'InicioBD()
		.Commandtext = "AgReservasGenera"
		.commandtype = adCmdStoredProc
		'--------------------------------------------
		' CArga parametros
		'--------------------------------------------
		For Each Parameter In .Parameters
			If Parameter.Direction And adParamInput Then
				Select case ucase(Mid(Parameter.Name, 2))
					case "OFICINA"
						Parameter.value = Oficina	
					CASE "TIPOMOVIMIENTO" 
						Parameter.value = TipoMovimiento	
					CASE "FECHABASE"
						Parameter.value = FechaBase	
					CASE "CANTRESERVAS"
						if len(CantReservas) = 0 then 
							CantReservas = 0
						end if
						Parameter.value = "" & CantReservas	
					CASE "MOTIVO"
						if len(Motivo) = 0 then 
							Motivo = 0
						end if 
						Parameter.value = Motivo	
					CASE "MOTIVODESC"
						Parameter.value = MotivoDesc	
					CASE "USUARIO"
						Parameter.value = Usuario	
					'CASE "NROLOTERESERVAS"
					'	Parameter.value = Oficina	
					CASE "STATUS"
						if len(Status) = 0 then 
							Status = 0
						end if
						Parameter.value = Status
					CASE "MENSAJE"
						Parameter.value = "" & Mensaje	
					CASE ELSE
						' ????
				END SELECT
			End If
		Next
		'--------------------------------------------
		' Ejecuta
		'--------------------------------------------
'PL-SQL * T-SQL  
XXX = TransformPLSQLToTSQL (XXX) 
		.Execute XXX, , adexecutenorecords
		
		'--------------------------------------------
		' Devuelve parametros
		'--------------------------------------------
		For Each Parameter In .Parameters
			If Parameter.Direction And (adParamOutput Or adParamReturnValue) Then
				Select case ucase(Mid(Parameter.Name, 2))
					case "STATUS"
						STATUS = trim(Parameter.value)
					case "MENSAJE"
						MENSAJE = trim(Parameter.value)
					case "NROLOTERESERVAS"
						NROLOTERESERVAS = trim(Parameter.value)
					case else
						' ???
				end select
			End If
		Next
	end with
	
	'--------------------------------------------
	' Prepara salida
	'--------------------------------------------
	if STATUS <> 1 then
		Retorna = "Error: " & STATUS & "<br> Mensaje: " & MENSAJE
	else
		Retorna = NROLOTERESERVAS
	end if

	'retorna el nro de cita que genera..
	GrabarReservas = Retorna 
	
'	FinBD()

	Set Spx = Nothing
End Function

Public Function AnularReservas(Oficina, TipoMovimiento, ReservaTipo, NroLoteReservas, Motivo, MotivoDesc, Usuario)
'---------------------------------------------------------------------------------------------------
' Que hace ??? Las ideas
'
' que parametros son de entrada
'NECESITA:
' Oficina
' TipoMovimiento
' ReservaTipo
' NroLoteReservas
' Motivo
' MotivoDesc
' Usuario
'
' que parametros son de salida
'DEVUELVE:

' "CantReservasAnuladas"
' "Status"
' "Mensaje"
'---------------------------------------------------------------------------------------------------

	Dim Spx
	Dim Retorna 
	
	Set Spx = Server.CreateObject("adodb.command")
	with Spx
		.activeconnection = connLavoro 'InicioBD()
		.Commandtext = "AgReservasAnula"
		.commandtype = adCmdStoredProc
		'--------------------------------------------
		' CArga parametros
		'--------------------------------------------
		For Each Parameter In .Parameters
			If Parameter.Direction And adParamInput Then
				Select case ucase(Mid(Parameter.Name, 2))
					case "OFICINA"
						Parameter.value = Oficina	
					CASE "TIPOMOVIMIENTO" 
						Parameter.value = TipoMovimiento	
					CASE "RESERVATIPO"
						Parameter.value = ReservaTipo	
					CASE "NROLOTERESERVAS"
						if len(NroLoteReservas) = 0 then 
							NroLoteReservas = 0
						end if
						Parameter.value = "" & NroLoteReservas	
					CASE "MOTIVO"
						if len(Motivo) = 0 then 
							Motivo = 0
						end if 
						Parameter.value = Motivo	
					CASE "MOTIVODESC"
						Parameter.value = MotivoDesc	
					CASE "USUARIO"
						Parameter.value = Usuario		
					CASE "CANTRESERVASANULADAS"
						if len(CantReservasAnuladas) = 0 then 
							CantReservasAnuladas = 0
						end if						
						Parameter.value = CantReservasAnuladas
					CASE "STATUS"
						if len(Status) = 0 then 
							Status = 0
						end if
						Parameter.value = Status
					CASE "MENSAJE"
						Parameter.value = "" & Mensaje	
					CASE ELSE
						' ????
				END SELECT
			End If
		Next
		'--------------------------------------------
		' Ejecuta
		'--------------------------------------------
'PL-SQL * T-SQL  
XXX = TransformPLSQLToTSQL (XXX) 
		.Execute XXX, , adexecutenorecords
		
		'--------------------------------------------
		' Devuelve parametros
		'--------------------------------------------
		For Each Parameter In .Parameters
			If Parameter.Direction And (adParamOutput Or adParamReturnValue) Then
				Select case ucase(Mid(Parameter.Name, 2))
					case "STATUS"
						STATUS = trim(Parameter.value)
					case "MENSAJE"
						MENSAJE = trim(Parameter.value)
					case "CANTRESERVASANULADAS"
						CANTRESERVASANULADAS = trim(Parameter.value)
					case else
						' ???
				end select
			End If
		Next
	end with
	
	'--------------------------------------------
	' Prepara salida
	'--------------------------------------------
	if STATUS <> 1 then
		Retorna = "Error: " & STATUS & "<br> Mensaje: " & MENSAJE
	else
		Retorna = CANTRESERVASANULADAS
	end if

	'retorna el nro de cita que genera..
	AnularReservas = Retorna 
	
'	FinBD()

	Set Spx = Nothing
End Function


Public Function GenerarComboHorario(Oficina,Fecha,Opciones,Puesto,Duracion,Nombre,Clase,Validacion)
	Dim Rs
	Dim Sp
	Dim Retorna 
	
'	InicioBD()
		 		 		 		 		 
	Set Rs = Server.CreateObject("adodb.recordset")
	Set Sp = Server.CreateObject("adodb.command")
	
	Sp.ActiveConnection = connLavoro 'InicioBD()
	
	Sp.CommandText = "AgAgendaEstadoConsultaDia(" & Oficina & ",'" & Fecha & "'," & Opciones & "," & Puesto & "," & Duracion & ",0,'')"

	Sp.CommandType = 4 

'PL-SQL * T-SQL  
	Set Rs = Sp.Execute()
	
	Retorna = ""

	
	If not Rs.eof then 
		Retorna = Retorna & "<select name='" & Nombre & "' Onchange='" & Validacion & "' Class='" & Clase & "'>"
		do while not Rs.eof
			If rs.fields(6) <> 0 then 
				Retorna = Retorna & "<option value ='" & Rs.fields(3) & "'>" & Rs.fields(3) & "</option>"
			end if
			Rs.movenext
		loop		
	end if 
	
	Retorna = Retorna & "</select>"		


	GenerarComboHorario = Retorna 
	
'	FinBD()
	
	Set Rs = Nothing
	Set Sp = Nothing
End Function 


Public Function ObtenerValor(Tabla,Identificador)
	Dim Rs
	Dim Consulta
	Dim Retorna 
	Dim campoid
		
'	InicioBD()
	
	Set Rs = Server.CreateObject("adodb.recordset")
	
	select case ucase(Tabla)
		case "AGCITATIPOS"
			campoid = "CitaTipo"
		case "AGCITAMOVIMIENTOSTIPOS"
			campoid = "TipoMovimiento"
		case else
		
	end select
	
	Consulta = "Select Descripcion from " & Tabla & " where " & campoid & "=" & Cint(Identificador)
	
'PL-SQL * T-SQL  
' Conexi�n a SQL2000 - CONSULTA = TransformPLSQLToTSQL (CONSULTA) 
	Set Rs = connLavoro.execute(Consulta)
	
	Retorna = ""
	
	If not Rs.eof then 
		Rs.movefirst
		Retorna = Rs("Descripcion")
	end if
	
	ObtenerValor = Retorna 
	
'	FinBD()
	
	Set Rs = Nothing
End function 

Public Function MostrarDatosCita(Oficina, TipoMovimiento, NroLoteReservas, NroCita)
'---------------------------------------------------------------------------------------------------
' Que hace ??? Las ideas
'
' que parametros son de entrada
'NECESITA:
' @Oficina		 	int,
' @TipoMovimiento	int = 0, 
' @NroLoteReservas	int = 0, 
' @NroCita	 		int = 0,
'
' que parametros son de salida
'DEVUELVE:

'	@CitaFecha			varchar(10) output,
'	@HoraDesdeMin		int			output,
'	@HoraHastaMin		int			output,
'	@HoraDesdeHs		varchar(5)	output,
'	@HoraHastaHs		varchar(5)	output,
'	@CUIL				varchar(11) output,
'	@CitaEstado			int			output,
'	@citaEstadoDesc		varchar(50) output,
'	@Puesto				int			output,
'	@NroEntrevista		int			output,
'	@UltNroMovimento	int			output,
'	
'	@Status				int			OUTPUT,	-- Status = 1 --> OK
'	@Mensaje			varchar(255)OUTPUT	-- Mensaje de Error
    
'---------------------------------------------------------------------------------------------------

	Dim Spx
	Dim Retorna 
		
	Retorna = ""
	
	Set Rs = server.CreateObject("adodb.recordset")
	Set Spx = Server.CreateObject("adodb.command")
	with Spx
		.activeconnection = connLavoro 'InicioBD()
		.Commandtext = "AgCitaDatos"
		.commandtype = adCmdStoredProc
		'--------------------------------------------
		' CArga parametros
		'--------------------------------------------
		For Each Parameter In .Parameters
			If Parameter.Direction And adParamInput Then
				Select case ucase(Mid(Parameter.Name, 2))
					case "OFICINA"
						Parameter.value = Oficina	
					CASE "TIPOMOVIMIENTO" 
						Parameter.value = TipoMovimiento	
					CASE "NROLOTERESERVAS"
						Parameter.value = NroLoteReservas	
					CASE "NROCITA"
						if len(NroCita) = 0 then 
							NroCita = 0
						end if
						Parameter.value = "" & NroCita	
					CASE "STATUS"
						if len(Status) = 0 then 
							Status = 0
						end if
						Parameter.value = Status
					CASE "MENSAJE"
						Parameter.value = "" & Mensaje	
					CASE ELSE
						' ????
				END SELECT
			End If
		Next
		'--------------------------------------------
		' Ejecuta
		'--------------------------------------------
'PL-SQL * T-SQL  
XXX = TransformPLSQLToTSQL (XXX) 
		.Execute XXX, , adexecutenorecords
		
		'--------------------------------------------
		' Devuelve parametros
		'--------------------------------------------
		For Each Parameter In .Parameters
			If Parameter.Direction And (adParamOutput Or adParamReturnValue) Then
				Select case ucase(Mid(Parameter.Name, 2))
					case "STATUS"
						STATUS = trim(Parameter.value)
					case "MENSAJE"
						MENSAJE = trim(Parameter.value)
					case "CITAFECHA"
						CITAFECHA = trim(Parameter.value)
					case "HORADESDEMIN"
						HORADESDEMIN = trim(Parameter.value)
					case "HORAHASTAMIN"
						HORAHASTAMIN = trim(Parameter.value)
					case "HORADESDEHS"
						HORADESDEHS = trim(Parameter.value)
					case "HORAHASTAHS"
						HORAHASTAHS = trim(Parameter.value)
					case "CUIL"
						CUIL = trim(Parameter.value)
					case "CITAESTADO"
						CITAESTADO = trim(Parameter.value)
					case "CITAESTADODESC"
						CITAESTADODESC = trim(Parameter.value)
					case "PUESTO"
						PUESTO = trim(Parameter.value)
					case "NROENTREVISTA"
						NROENTREVISTA = trim(Parameter.value)
					case "ULTNROMOVIMIENTO"
						ULTNROMOVIMIENTO = trim(Parameter.value)																																																
					case else
						' ???
				end select
			End If
		Next
	end with
	
	'--------------------------------------------
	' Prepara salida
	'--------------------------------------------
	if STATUS <> 1 then
		Retorna = "Error: " & STATUS & "<br> Mensaje: " & MENSAJE
	else
        Retorna = Retorna & "<input type='hidden' name='TxtCitaFecha' value='" & CITAFECHA & "'>"
        Retorna = Retorna & "<input type='hidden' name='TxtHoraDesdeMin' value='" & HORADESDEMIN & "'>"
        Retorna = Retorna & "<input type='hidden' name='TxtHoraHastaMin' value='" & HORAHASTAMIN & "'>"
        Retorna = Retorna & "<input type='hidden' name='TxtHoraDesdeHs' value='" & HORADESDEHS & "'>"
        Retorna = Retorna & "<input type='hidden' name='TxtHoraHastaHs' value='" & HORAHASTAHS & "'>"
       ' Retorna = Retorna & "<input type='hidden' name='TxtCUIL' value='" & CUIL & "'>"
        Retorna = Retorna & "<input type='hidden' name='TxtCitaEstado' value='" & CITAESTADO & "'>"
        Retorna = Retorna & "<input type='hidden' name='TxtcitaEstadoDesc' value='" & CITAESTADODESC & "'>"
        Retorna = Retorna & "<input type='hidden' name='TxtPuesto' value='" & PUESTO & "'>"
        Retorna = Retorna & "<input type='hidden' name='TxtNroEntrevista' value='" & NROENTREVISTA & "'>"
        Retorna = Retorna & "<input type='hidden' name='TxtUltNroMovimento' value='" & ULTNROMOVIMIENTO & "'>"
		
		Retorna = Retorna & "	<table width='100%' border=2 bordercolor='MidnightBlue'>"                                                                                                                                                            
		Retorna = Retorna & "			<tr>"
		Retorna = Retorna & "				<th align='center' width=10% class='tbltext1' bgcolor='#D9D9AE'><b>Fecha</b></td>"
		Retorna = Retorna & "				<th align='center' width=10% class='tbltext1' bgcolor='#D9D9AE'><b>Hora Inicio</b></td>"
		Retorna = Retorna & "				<th align='center' width=10% class='tbltext1' bgcolor='#D9D9AE'><b>Hora Fin</b></td>"
		Retorna = Retorna & "				<th align='center' width=30% class='tbltext1' bgcolor='#D9D9AE'><b>C.I.</b></td>"
		Retorna = Retorna & "				<th align='center' width=40% class='tbltext1' bgcolor='#D9D9AE'><b>Apellido y Nombre</b></td>"
		Retorna = Retorna & "			</tr>"
		Retorna = Retorna & "			<tr>"
		Retorna = Retorna & "				<td align='center' class='tbltext1' bgcolor='LightYellow'><b>" & CITAFECHA & "</b></td>"
		Retorna = Retorna & "				<td align='center' class='tbltext1' bgcolor='LightYellow'><b>" & HORADESDEHS & "</b></td>"
		Retorna = Retorna & "				<td align='center' class='tbltext1' bgcolor='LightYellow'><b>" & HORAHASTAHS & "</b></td>"
		Retorna = Retorna & "				<td align='center' class='tbltext1' bgcolor='LightYellow'>"
		Retorna = Retorna & "					<input name='TxtCuilNH' type='text' class='textblack' style='TEXT-TRANSFORM:uppercase' maxlength='11' size='20'>"
		Retorna = Retorna & "					<input name='TxtCuil' type='hidden'>"
		Retorna = Retorna & "					<a href=Javascript:IrA('../BuscarPostulante.asp') ID='imgPunto1' name='imgPunto1' onmouseover=javascript:window.status='';return true><img border='0' src='" & Session("Progetto") & "/images/bullet1.gif'></a>"
		Retorna = Retorna & "				<td align='center' class='tbltext1' bgcolor='LightYellow'>"
		Retorna = Retorna & "				<input name='TxtNombre' type='text' size='40' disabled class='textblack' style='TEXT-TRANSFORM:uppercase'>"
		Retorna = Retorna & "					<input name='TxtNom' type='hidden'>"
		Retorna = Retorna & "				</td>"
		Retorna = Retorna & "				</td>"
		Retorna = Retorna & "			</tr>"
		Retorna = Retorna & "		</table>"
		Retorna = Retorna & "		<table width='90%'>"
		Retorna = Retorna & "			<tr>"
		Retorna = Retorna & "				<td align='right'>"
		Retorna = Retorna & "					<br><input type='submit' class='my' value='Nominar' id='submit'1 name='submit'1>"
		Retorna = Retorna & "				</td>"
		Retorna = Retorna & "			</tr>"
		Retorna = Retorna & "		</table>"
	end if


	'retorna el nro de cita que genera..
	MostrarDatosCita = Retorna 
	

	
'	FinBD()

	Set Spx = Nothing
End Function

Public Function InicioBD()

	'================================================================================================
	' Esto estaba hasta el 27/03/2006 cambios realizados por Hern�n
	'================================================================================================
	'Hosting = "PORTALDESARROLLO"
    'Application(Hosting & "_AgendaConnectionString") = "Provider=MSDASQL.1;Password=1234*;Persist Security Info=True;User ID=EmpleoLavoro;Data Source=EmpleoLavoro;Extended Properties=""DSN=EmpleoLavoro;Description=EmpleoLavoro;UID=EmpleoLavoro;APP=????4?????4?????T0???????;WSID=PC0297;DATABASE=EmpleoLavoro"";Initial Catalog=EmpleoLavoro;User Id=EmpleoLavoro;PASSWORD=1234*;"
	'Application(Hosting & "_AgendaConnectionTimeout") = 60
	'Application(Hosting & "_AgendaCommandTimeout") = 180
	'Application(Hosting & "_AgendaCursorLocation") = 3
	'Application(Hosting & "_AgendaRuntimeUserName") = "EmpleoLavoro"
	'Application(Hosting & "_AgendaRuntimePassword") = "1234*"
	
	'Hosting = "PORTALPRUEBA"
    'Application(Hosting & "_AgendaConnectionString") = "Provider=MSDASQL.1;Password=1234*;Persist Security Info=True;User ID=EmpleoLavoro;Data Source=EmpleoLavoro;Extended Properties=""DSN=EmpleoLavoro;Description=EmpleoLavoro;UID=EmpleoLavoro;APP=????4?????4?????T0???????;WSID=PC0297;DATABASE=EmpleoLavoro"";Initial Catalog=EmpleoLavoro;User Id=EmpleoLavoro;PASSWORD=1234*;"
	'Application(Hosting & "_AgendaConnectionTimeout") = 60
	'Application(Hosting & "_AgendaCommandTimeout") = 180
	'Application(Hosting & "_AgendaCursorLocation") = 3
	'Application(Hosting & "_AgendaRuntimeUserName") = "EmpleoLavoro"
	'Application(Hosting & "_AgendaRuntimePassword") = "1234*"

	'Hosting = "PORTALEMPLEO"
    'Application(Hosting & "_AgendaConnectionString") = "Provider=MSDASQL.1;Password=1234*;Persist Security Info=True;User ID=EmpleoLavoro;Data Source=EmpleoLavoro;Extended Properties=""DSN=EmpleoLavoro;Description=EmpleoLavoro;UID=EmpleoLavoro;APP=????4?????4?????T0???????;WSID=PC0297;DATABASE=EmpleoLavoro"";Initial Catalog=EmpleoLavoro;User Id=EmpleoLavoro;PASSWORD=1234*;"
	'Application(Hosting & "_AgendaConnectionTimeout") = 60
	'Application(Hosting & "_AgendaCommandTimeout") = 180
	'Application(Hosting & "_AgendaCursorLocation") = 3
	'Application(Hosting & "_AgendaRuntimeUserName") = "EmpleoLavoro"
	'Application(Hosting & "_AgendaRuntimePassword") = "1234*"
	
	' Determina ServerName
	'If Len(ServerName) = 0 Then
	'	ServerName = Request.ServerVariables("SERVER_NAME")
	'	Application("SERVER_NAME") = ServerName
	'End If

	'Set conn = Server.CreateObject("adodb.connection")
	
	'with conn
	 '   .ConnectionString = Application(ServerName & "_AgendaConnectionString")	
	 '   .ConnectionTimeout = Application(ServerName & "_AgendaConnectionTimeout")
	 '   .CommandTimeout = Application(ServerName & "_AgendaCommandTimeout")
	 '   .CursorLocation = Application(ServerName & "_AgendaCursorLocation")
'PL-SQL * T-SQL  
OPEN = TransformPLSQLToTSQL (OPEN) 
	 '   .Open ,Application(ServerName & "_AgendaRuntimeUserName"), Application(ServerName & "_AgendaRuntimePassword")
	'end with

	'set InicioBD = conn
	
	'===============================================================================================
	' Este es el cambio que se introdujo el 27/03/2006
	'===============================================================================================
	set InicioBD = connLavoro		
End Function


Public Function GenerarTablaMostrarCita(TipoMov,TipoCita,Cuil,Nombre,Estado,Clase,FuncionJS,EsHistorial)
	Dim Rs
	Dim Consulta
	Dim Retorna 
	
'	InicioBD()
	
	Set Rs = Server.CreateObject("adodb.recordset")
	
	if TipoMov = "" and TipoCita = "" then
		if EsHistorial = 0 then 
			Consulta = "Select NroCita as [Nro. de Cita],Fecha,HoraDesde as [Hora Inicial],HoraHasta as [Hora Final],Cuil " ',Puesto,EntrevistaNro as [Nro. de Entrevista],UltNroMovimiento as [Nro. Ult. Mov.], " 
			'Consulta = Consulta & " CT.Descripcion as [Tipo de Cita], CE.Descripcion as [Estado Cita] "
			Consulta = Consulta & " from AgCitas C, "
			Consulta = Consulta & " AgCitaEstados CE where "
			Consulta = Consulta & " C.Cuil = '" & Cuil & "' and C.Estado = " & Estado
			Consulta = Consulta & " and C.Estado = CE.Estado "
		elseif EsHistorial = 1 then
			Consulta = "Select NroCita as [Nro. de Cita],Fecha,HoraDesde as [Hora Inicial],HoraHasta as [Hora Final],Cuil, " ',Puesto,EntrevistaNro as [Nro. de Entrevista],UltNroMovimiento as [Nro. Ult. Mov.], " 
			Consulta = Consulta & " CT.Descripcion as [Tipo de Cita], CE.Descripcion as [Estado Cita],Oficina "
			Consulta = Consulta & " from AgCitas C, "
			Consulta = Consulta & " AgCitaEstados CE, AgCitaTipos CT where "
			Consulta = Consulta & " C.Cuil = '" & Cuil & "'"
			Consulta = Consulta & " and C.Estado = CE.Estado and C.CitaTipo = CT.CitaTipo"
		end if	
	else
		if EsHistorial = 0 then 
			Consulta = "Select NroCita as [Nro. de Cita],Fecha,HoraDesde as [Hora Inicial],HoraHasta as [Hora Final],Cuil " ',Puesto,EntrevistaNro as [Nro. de Entrevista],UltNroMovimiento as [Nro. Ult. Mov.], " 
			'Consulta = Consulta & " CT.Descripcion as [Tipo de Cita], CE.Descripcion as [Estado Cita] "
			Consulta = Consulta & " from AgCitas C, "
			Consulta = Consulta & " AgCitaEstados CE, AgCitaTipos CT where "
			Consulta = Consulta & " C.CitaTipo = " & TipoCita & " and C.Cuil = '" & Cuil & "' and C.Estado = " & Estado
			Consulta = Consulta & " and C.Estado = CE.Estado and C.CitaTipo = CT.CitaTipo"
		elseif EsHistorial = 1 then
			Consulta = "Select NroCita as [Nro. de Cita],Fecha,HoraDesde as [Hora Inicial],HoraHasta as [Hora Final],Cuil, " ',Puesto,EntrevistaNro as [Nro. de Entrevista],UltNroMovimiento as [Nro. Ult. Mov.], " 
			Consulta = Consulta & " CT.Descripcion as [Tipo de Cita], CE.Descripcion as [Estado Cita],Oficina "
			Consulta = Consulta & " from AgCitas C, "
			Consulta = Consulta & " AgCitaEstados CE, AgCitaTipos CT where "
			Consulta = Consulta & " C.CitaTipo = " & TipoCita & " and C.Cuil = '" & Cuil & "'"
			Consulta = Consulta & " and C.Estado = CE.Estado and C.CitaTipo = CT.CitaTipo"
		end if
	end if
	
'PL-SQL * T-SQL  
' Conexi�n a SQL2000 - CONSULTA = TransformPLSQLToTSQL (CONSULTA) 
	Set Rs = connLavoro.execute(Consulta)
	
	Retorna = ""
	
	If not Rs.eof then 
		Retorna = Retorna & "<Table width = 80% class='" & Clase & "' border=2 bordercolor='MidnightBlue'>"
		Rs.movefirst
		Retorna = Retorna & "<tr>"			
		for each x in Rs.fields 
			Retorna = Retorna & "<th align='center' bgcolor='#D9D9AE' width='20%'><b>" & x.name & "</b></th>"
		next
		Retorna = Retorna & "</tr>"
		Do while not Rs.eof
			HoraInicial = ConvertirMinutosAHorario(Rs.fields(2))
			HoraFinal = ConvertirMinutosAHorario(Rs.fields(3))
			
			Retorna = Retorna & "<tr>"
			for each x in Rs.fields
				if x.name = "Nro. de Cita" then 
					If FuncionJS <> "" then 
						if instr(1,FuncionJS,"(") <> 0 then 
							if Nombre <> "" then 
								Nombre = replace(Nombre," ","_")
							end if
							
							if Nombre <> "" then 
								Retorna = Retorna & "<td align='center' bgcolor='LightYellow'><b><a href=" & FuncionJS & "," & TipoMov & "," & TipoCita & "," & x.value & "," & Cuil & ",'" & Nombre & "')>" & x.value & "</a></b></td>"					
							else
								Retorna = Retorna & "<td align='center' bgcolor='LightYellow'><b><a href=" & FuncionJS & "," & TipoMov & "," & TipoCita & "," & x.value & "," & Cuil & ")>" & x.value & "</a></b></td>"												
							end if
						else
							if Nombre <> "" then 
								Retorna = Retorna & "<td align='center' bgcolor='LightYellow'><b><a href=" & FuncionJS & "(" & TipoMov & "," & TipoCita & "," & x.value & "," & Cuil & ",'" & Nombre & "')>" & x.value & "</a></b></td>"					
							else
								Retorna = Retorna & "<td align='center' bgcolor='LightYellow'><b><a href='" & FuncionJS & "(" & TipoMov & "," & TipoCita & "," & x.value & "," & Cuil & ")'>" & x.value & "</a></b></td>"												
							end if 
						end if 
					else 
						Retorna = Retorna & "<td align='center' bgcolor='LightYellow'><b>" & x.value & "</b></td>"
					end if
				else
					if x.name = "Hora Inicial" then 
						Retorna = Retorna & "<td align='center' bgcolor='LightYellow'>" & HoraInicial & "</td>"
					elseif x.name = "Hora Final" then 
						Retorna = Retorna & "<td align='center' bgcolor='LightYellow'>" & HoraFinal & "</td>"
					else
						Retorna = Retorna & "<td align='center' bgcolor='LightYellow'>" & x.value & "</td>"
					end if
				end if 
			next
			Retorna = Retorna & "</tr>"
			Rs.movenext
		Loop
		Retorna = Retorna & "</Table>"
	end if 
	
	
	GenerarTablaMostrarCita = Retorna 
	
'	FinBD()
	
	Set Rs = Nothing
End Function

Public Function ConvertirMinutosAHorario(Minutos)
	Dim retorna
	
	retorna = ""
	
	Hs=ObtenerHoras(Minutos) 
	Mins=ObtenerMinutos(Minutos)
	
	retorna =Hs & ":" & Mins
	
	ConvertirMinutosAHorario = retorna
End Function

Public Function ObtenerHoras(Valor)

	Dim retorna
	'Response.Write Valor & "<br>"
	
	Valor = clng(Valor)/60
	
	retorna = ""
	
	pos=instr(1,cstr(Valor),",")
	
	if pos = 0 then
		retorna = Valor
	else
		retorna = cstr(left(Valor,pos-1))
	end if
	
	ObtenerHoras = retorna
End Function

Public Function ObtenerMinutos(Valor)

	Dim retorna
	'Valor = clng(Valor)/60	
	
	pos =instr(1,cstr(Valor),",")
	retorna = ""
	
	
	If pos <> 0 Then	
		inter = round(mid(Valor,pos)  * 60 )
		retorna = inter
	Else
		retorna = 0
	End If
	
	select case retorna 
	
	case 0,1,2,3,4,5,6,7,8,9
		retorna = "0" & retorna
	case else
	
	end select
	
	ObtenerMinutos = retorna
End Function


Public function ExistePostulante(Cuil)

	Dim RsBusca
	Dim SqlBusca

	ExistePostulante = ""
	
	set RsBusca = server.CreateObject("Adodb.recordset")

	SqlBusca = "Select nome,secondo_nome,cognome,secondo_cognome from Persona with (nolock) where cod_fisc='" & cstr(Cuil) & "'"
	
'PL-SQL * T-SQL  
SQLBUSCA = TransformPLSQLToTSQL (SQLBUSCA) 
	set RsBusca = CC.execute(SqlBusca)
	
	if not RsBusca.EOF then
		RsBusca.MoveFirst 
		ExistePostulante = RsBusca.Fields(0) & " " & RsBusca.fields(1) & " " & RsBusca.fields(2) & " " & RsBusca.fields(3)
	else
		ExistePostulante = ""
	end if
	
	set RsBusca = nothing
End function


Public Function FinBD()
	Set connLavoro = nothing
End Function


Public Function GenerarTablaEntrevistasPendientes(Oficina,Operador,Estado,Tarea,SPNAME,Clase,Fecha)
	Dim Rs
	Dim cmdConsulta
	Dim Retorna
	Dim Cuil
	Dim NroEntrevista 
	
	'msgbox Oficina
	'msgbox Tarea
	'msgbox SPNAME
	
'	InicioBD()
	Set cmdConsulta = Server.CreateObject("adodb.command")
	Set Rs = Server.CreateObject("adodb.recordset")

	'Response.Write "SPNAME " & SPNAME
	'Response.write "Operador " & Operador
	'Response.write "Oficina " & Oficina
	'Response.write "Tarea " & Tarea
	'Response.write "fecha " & fecha
	'Response.write "estado " & estado
	'Response.End 
	
		With cmdConsulta
		    .ActiveConnection = ConnLavoro
		    .CommandTimeout = 1200
		    .CommandText = SPNAME
		    .CommandType = 4
		    .Parameters("@Oficina").Value = Oficina
		    .Parameters("@Operador").Value = Operador
		    .Parameters("@Estado").Value = Estado
		    if estado=10  then
				Fecha=""
		    end if
		    .Parameters("@Fecha").Value = Fecha
		    
		    
		    
'PL-SQL * T-SQL  
		    Set Rs = .Execute()
		End With
		
	Retorna = ""
	ExisteColCuil = 0
	

'''paginacion
	if rs.state = 1 then		
		if not rs.eof then	
			Retorna = Retorna & "<Table width = 100% class='" & Clase & "' border=2 bordercolor='white'>"

		nActPagina	=Request("txtPagNum")
		 
		Record=0
		nTamPagina=7

		If nActPagina = "" Then
			nActPagina=1
		Else
			nActPagina=Clng(Request("txtPagNum"))
		End If
	
		rs.PageSize = nTamPagina
		  
		rs.CacheSize = nTamPagina

		nTotPagina = rs.PageCount
		 	 
		If nActPagina < 1 Then
			nActPagina = 1
		End If

		If nActPagina > nTotPagina Then
			nActPagina = nTotPagina
		End If

		rs.AbsolutePage=nActPagina
		nTotRecord=0
		

		'''paginacion	
		'Rs.movefirst
			While rs.EOF <> True And nTotRecord < nTamPagina
		  		if Record = 0 then	
					Retorna = Retorna & "<tr>"			
					for i = 1 to rs.fields.count - 1 
					''''' SM inizio
					if trim(ucase(rs.fields(i).name)) = "CUIL" then
						Retorna = Retorna & "<th align='center' bordercolor='MidnightBlue' bgcolor='#D9D9AE' width='15%'><b>Identificaci�n</b></th>"
					else
						Retorna = Retorna & "<th align='center' bordercolor='MidnightBlue' bgcolor='#D9D9AE' width='15%'><b>" & rs.fields(i).name & "</b></th>"
					end if
						if trim(ucase(rs.fields(i).name)) = "CUIL" then
							ExisteColCuil = 1 
							Retorna = Retorna & "<th align='center' bordercolor='MidnightBlue' bgcolor='#D9D9AE' width='110'><b>Nombre Completo</b></th>"
						end if 
					next
			
					'if ExisteColCuil = 1 then 
						'Retorna = Retorna & "<th align='center' bgcolor='#D9D9AE' width='100'><b>Apellido y Nombre</b></th>"
					'end if 
					Retorna = Retorna & "</tr>"			
					record = 1  
				end if	

				Cuil = ""
				NroEntrevista = 0
				Cuil = Rs.fields(1).value
				NroEntrevista = Rs.fields(2).value
					
				Retorna = Retorna & "<tr bordercolor='MidnightBlue'>"
				for i = 1 to rs.fields.count - 1
					if estado=10 then				
						Retorna = Retorna & "<td align='center' bgcolor='LightYellow'><b><a class='tbltext1' href='EntrevistaAccion.asp?" & RS.FIELDS(0).VALUE & "'>" & rs.fields(i) & "</a></b></td>"					
					end if
					if estado=20 then				
						Retorna = Retorna & "<td align='center' bgcolor='LightYellow'><b>" & rs.fields(i) & "</b></td>"					
					end if
					if trim(ucase(rs.fields(i).name)) = "CUIL" then
						if estado=10 then
							Retorna = Retorna & "<td align='center' bgcolor='LightYellow'><b><a class='tbltext1' href='EntrevistaAccion.asp?" & RS.FIELDS(0).VALUE & "'>" & ExistePostulante(rs.fields("CUIL")) & "</a></b></td>"						
						end if
						if estado=20 then
							Retorna = Retorna & "<td align='center' bgcolor='LightYellow'><b>" &  ExistePostulante(rs.fields("CUIL")) & "</a></b></td>"						
						end if
					end if
						'ExisteColCuil = 1 
				next
			
				'if ExisteColCuil = 1 then 
					'Retorna = Retorna & "<td align='center' bgcolor='LightYellow'><b><a class='tbltext1' href='EntrevistaAccion.asp?" & RS.FIELDS(0).VALUE & "'>" & ExistePostulante(rs.fields("CUIL")) & "</a></b></td>"						
				'end if 
			
				nTotRecord=nTotRecord + 1	
				rs.MoveNext

			wend
				Retorna = Retorna & "<tr bordercolor=white><td colspan=4>"
				Retorna = Retorna & "<table border=0 width=480>"
				Retorna = Retorna & "<tr>"
				if nActPagina > 1 then
					Retorna = Retorna & "<td align='right' width='480'>"
						Retorna = Retorna & "<a href=EntrevistasPendientesResultados.asp?txtPagNum=" & (nActPagina - 1) & "&txtOficina=" & Oficina & "&estado=" & estado & "&TxtFecha=" & sFecha & "><img src='" & Session("Progetto") & "/images/precedente.gif' border='0' id='image1' name='image1'>"
					Retorna = Retorna & "</td>"				
				end if
				if nActPagina < nTotPagina then
					Retorna = Retorna & "<td align='right'>"
						Retorna = Retorna & "<a href=EntrevistasPendientesResultados.asp?txtPagNum=" & (nActPagina + 1) & "&txtOficina=" & Oficina &  "&estado=" & estado & "&TxtFecha=" & sFecha & "><img src='" & Session("Progetto") & "/images/successivo.gif' border='0' id='image1' name='image1'>"
					Retorna = Retorna & "</td>"
				end if		
				Retorna = Retorna & "</tr>"
				Retorna = Retorna & "</table>"
				Retorna = Retorna & "</td></tr>"		
		Retorna = Retorna & "</Table>"
	end if 
	end if 
	

	GenerarTablaEntrevistasPendientes = Retorna 
	
'	FinBD()
	
	Set Rs = Nothing
End Function

Public Function ArmaTablaGenerico(Stored,ASPLink,Clase)
	
	Retorna = ""
	salto = ""
	
	Set Rec = Server.CreateObject("ADODB.Recordset")

	EjecutaComandoSQLRS Stored, Rec
	
	
	
		If not Rec.eof then
			Arr = GetRows(Rec)
		End if
		
		If IsArray(Arr) Then
		
			RecordCount = UBound(Arr, 2)
			' Titulos
				
			Retorna = Retorna & "<table class=""results"" cellspacing=""1"">"
				
			Retorna = Retorna & "<tr class=""Header"">"
			For FieldIndex = 1 To Rec.Fields.Count - 1
				Retorna = Retorna & "<td align=""center"">"
				Retorna = Retorna & Server.HTMLEncode(Rec.Fields(FieldIndex).Name)
				Retorna = Retorna & "</td>"
				'Indices para el salto
				'select case ucase(Rec.Fields(FieldIndex).Name)
				'	case "NRO. VISITA":	IndexNroVisita = FieldIndex
				'	case else
				'end select
			Next
			Retorna = Retorna & "</tr>"
			' Lineas de Datos
			For RecordIndex = 0 To RecordCount
				Retorna = Retorna & "<tr class=""Row"
				Retorna = Retorna & RecordIndex Mod 2
				Retorna = Retorna & """>"
				' Referencia para el salto
				'salto = "editar.asp?NroPlan=" & Arr(IndexPlan, RecordIndex) _
				'		& "&Pcia=" & Arr(IndexPcia, RecordIndex) _
				'		& "&Muni=" & Arr(IndexMuni, RecordIndex) 
				
				'if 	IndexNroVisita >0 then						
				'	salto = "editar.asp?NroVisita=" & Arr(IndexNroVisita, RecordIndex) 
				'	salto = salto & "&"
				'end if
				'salto = salto & "funcion=" & fields("funcion") & "&opcion=" & fields("opcion")
				' Emite celdas
				For FieldIndex = 1 To Ubound(Arr, 1)
				    Retorna = Retorna & "<td nowrap>" 
				    If Len(Fields("Excel")) = 0 Then 
				    	    Retorna = Retorna & "<a href=" & salto & ">"
				    end if 	    
				    ' Dato de la celda
				    Retorna = Retorna & Server.HTMLEncode(""  & Arr(FieldIndex, RecordIndex))
				    If Len(Fields("Excel")) = 0 Then 
						Retorna = Retorna & "</a>"
					end if	
				    Retorna = Retorna & "</td>"
				Next
				
'				For FieldIndex = 1 To Ubound(Arr, 1)
'				    Response.Write "<td nowrap><a href=" & salto & ">"
'				    ' Dato de la celda
'				    Response.Write Server.HTMLEncode(Arr(FieldIndex, RecordIndex))
'				    Response.Write "</a></td>"
'				Next
			Next
			Retorna = Retorna & "</table><BR>"
		End If
		
		ArmaTablaGenerico = Retorna
		
End Function

End Class
%>
