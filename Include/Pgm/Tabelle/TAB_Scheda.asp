<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include Virtual = "/strutt_testa2.asp"-->
<!--#include virtual = "/Include/OpenConn.asp"-->
<!--#include virtual = "/Include/ControlDateVB.asp"-->
<!--#include virtual = "/util/dbutil.asp"-->
<!--#include virtual ="/include/ckProfile.asp"-->
<!--#include virtual = "/util/portallib.asp"-->
<%
if ValidateService(session("idutente"),"TAB_ELENCOTABELLE",cc) <> "true" then 
	response.redirect "/util/error_login.asp"
end if
%>

<SCRIPT LANGUAGE="Javascript">
//include del file per fare i controlli sulla validit� delle date
<!--#include virtual = "/Include/ControlDate.inc"-->
//include del file per fare i controlli sulla validit� delle stringhe
<!--#include virtual = "/Include/ControlString.inc"-->
//include del file per fare i controlli sulla numericit� dei campi
<!--#include virtual = "/Include/ControlNum.inc"-->
<!--#include Virtual = "/Include/help.inc"-->

	function Delete(sCodice,sTabella,sData, sIsaCodice){	
		if (confirm("Sei sicuro di voler chiudere la validit� di questo Codice alla data di oggi?")){
			location.href = "TAB_CnfCodici.asp?Action=Can&Codice=" + 
			sCodice + "&Tabella=" + sTabella + "&Data=" + sData +
			"&IsaCodice=" + sIsaCodice
		}		
	}

	//Questa funzione mi permette di aggiungere spazi fino 
	//alla fine della lunghezza massima degli attributi
	function elaboraAttributi(numero_elementi){
		var sAttributi
		sAttributi = ""

//		for (i=4; i<numero_elementi; i++){	
		for (i=6; i<numero_elementi; i++){	
			if (formDati.elements[i].name != ",") {
				nLunghMaxAttrib = formDati.elements[i].maxLength
				valore			= formDati.elements[i].value
				infLeng			= valore.length
			}else {
				nLunghMaxAttrib = 0
				valore			= ""
				infLeng			= 0
			}
			
/*			alert (nLunghMaxAttrib)
			alert (valore)
			alert (infLeng)

			alert (nLunghMaxAttrib-infLeng)
			return false
*/
			for (j=0; j<(nLunghMaxAttrib-infLeng);j++){
				valore = valore + " "
			}
			sAttributi = sAttributi + valore
		}
//		alert(sAttributi)

	return sAttributi
	}
	
	function Inserimento(sLungDescr,sTabella,sNumeroValori){	
		var sAttributi

		numero_elementi = (formDati.length)
		if (ControllaDati(sLungDescr,numero_elementi)){	
			sCodice		 = formDati.txtCodice.value 
			sDecorrenza  = formDati.txtDecorrenza.value
			sScadenza	 = formDati.txtScadenza.value
			if  (sScadenza == "")
			{
				sScadenza = "31/12/9999"
			}
			sDescrizione = formDati.txtDescrizione.value
					
			// Ritroviamo la lunghezza massima degli attributi
			sAttributi	 = elaboraAttributi(numero_elementi)
			ciccio = "TAB_CnfCodici.asp?Action=Ins&Tabella=" + 
				sTabella + 
				"&Codice=" + sCodice +
				"&Decorrenza=" + sDecorrenza +
				"&Scadenza=" + sScadenza + 
				"&Descrizione=" + sDescrizione +
				"&Attributi=" + sAttributi	
			
			location.href = ciccio
		}
		else
			return false
	}

	function Modifica(sLungDescr, sTabella,sNumeroValori,sIsaCodice, sScadenzaCodice, sDecorrenzaCodice,sDecorrenzaOld){	
		var sAttributi

		//alert(formDati.length)

		//numero_elementi = (formDati.length)-2
		numero_elementi = (formDati.length)
		
		if (ControllaDati(sLungDescr,numero_elementi)){
			sCodice = formDati.txtCodice.value 
			sDecorrenza = formDati.txtDecorrenza.value
			sDecorrenzaOld=formDati.txtDecorrenzaOld.value
			sScadenza = formDati.txtScadenza.value
			if  (sScadenza == "")
			{
				sScadenza = "31/12/9999"
			}
			sDescrizione = formDati.txtDescrizione.value

					
			// Ritroviamo la lunghezza massima degli attributi
			sAttributi = elaboraAttributi(numero_elementi)

			ciccio = "TAB_CnfCodici.asp?Action=Mod&Tabella=" + 
				sTabella + 
				"&Codice=" + sCodice +
				"&Decorrenza=" + sDecorrenza +
				"&Scadenza=" + sScadenza + 
				"&Descrizione=" + sDescrizione +
				"&Attributi=" + sAttributi +
				"&IsaCodice=" + sIsaCodice + 
				"&ScadenzaCodice=" + sScadenzaCodice + 
				"&DecorrenzaCodice=" + sDecorrenzaCodice +
				"&DecorrenzaOld=" + sDecorrenzaOld

			location.href = ciccio
		}
		else
			return false
	}

	//Funzione per i controlli dei campi 
	function ControllaDati(sLungDescr,numero_elementi){
		var Decorrenza
		var Scadenza

		//CODICE obbligatorio
		formDati.txtCodice.value=TRIM(formDati.txtCodice.value)
		if (formDati.txtCodice.value == ""){
			alert("Codice obbligatorio")
			formDati.txtCodice.focus() 
			return false
		}
		//Formato del CODICE (Numerico/Alfanumerico)
		if (formDati.txtCodice.name == "N"){
			if (!IsNum(formDati.txtCodice.value)){
				alert("Il Codice deve essere numerico")
				formDati.txtCodice.focus() 
				return false
			}
		}
	
		//Controllo della validit� della DECORRENZA
		Decorrenza = formDati.txtDecorrenza.value
		if (!ValidateInputDate(Decorrenza)) 
		{
			formDati.txtDecorrenza.focus() 
			return false
		}
		
		//Controllo della validot� della SCADENZA
		Scadenza = formDati.txtScadenza.value
		if (Scadenza != ""){
			if (!ValidateInputDate(Scadenza)) {
				formDati.txtScadenza.focus() 
				return false
			}
					
			if (!ValidateRangeDate(Decorrenza,Scadenza)) {
				alert("Controllare l'intervallo di validit� indicato")
				formDati.txtScadenza.focus() 
				return false
			}

			if (!ValidateRangeDate(formDati.txtDataSys.value,Scadenza)) {
				alert("La scadenza non pu� essere inferiore alla data odierna")			
				formDati.txtScadenza.focus() 
				return false
			}
		}
		
		//DESCRIZIONE obbligatoria
		formDati.txtDescrizione.value=TRIM(formDati.txtDescrizione.value)
		if (formDati.txtDescrizione.value == ""){
			alert("Descrizione obbligatoria")
			formDati.txtDescrizione.focus() 
			return false
		}
		
		//DESCRIZIONE lunghezza max = 50
		if (formDati.txtDescrizione.value.length > sLungDescr ){
			alert("Descrizione troppo lunga, si accettano al massimo " + sLungDescr + " caratteri")
			formDati.txtDescrizione.focus() 
			return false
		}
		// Controllo del TIPO e della OBBLIGATORIETA' ATTRIBUTI
		var sInfo
		var sObbligatorieta
		var sTipoDati
		l = 0 
//		for (l=4; l<numero_elementi; l++){	
		for (l=6; l<numero_elementi; l++){	
			sInfo = formDati.elements[l].name  // = f,a
			sObbligatorieta = sInfo.substr(0,1)
			sTipoDati = sInfo.substr(2,1)
			
			if (sObbligatorieta == "O"){
				if (formDati.elements[l].value == ""){	
					alert("Valore obbligatorio")
					formDati.elements[l].focus()
					return false
				}
			}
			if (sTipoDati == "N"){	
				if (!IsNum(formDati.elements[l].value)){
					alert("Il Valore deve essere Numerico")
					formDati.elements[l].focus()
					return false
				}
			}
			//Se il TIPO dell'ATTRIBUTO � D, allora nel valore 
			//ci deve essere una DATA 
			if (sTipoDati == "D"){
				if (!ValidateInputDate(formDati.elements[l].value)){
					formDati.elements[l].focus() 
					return false
				}
			}
		}
		//Se non c'� SCADENZA metto per default 31/12/9999
	/*	if  (formDati.txtScadenza.value == ""){
			formDati.txtScadenza.value = "31/12/9999"
		}
	*/	return true
	}
</SCRIPT>
<%
Inizio()
'on error resume next 

dim rsTabella
dim sAtt
dim i, j, k 
dim arrayInputString(10)
dim cursore

dim CnConn
dim rsTabelle
dim sSQL
dim sDecorrenza
dim sScadenza
dim sDecorrenzaCodice
dim sScadenzaCodice
dim sDescrizione
dim sValore

dim sTabella
dim sAction
dim	sCodice
dim sIsa
dim sData
dim sProfilo
dim sIsaCodice
						
'sIsaCodice = request("IsaCodice")

'if mid(Session("UserProfiles"),1,1) = "0" then
'	sIsa = "0"
'else
'	sIsa = mid(Session("UserProfiles"),1,2)
'end if			
'sProfilo = mid(Session("UserProfiles"),3,1)

if mid(Session("mask"),1,1) = "0" then
	sIsaCodice = "0"
else
	sIsaCodice = mid(Session("mask"),1,2)
end if
sProfilo = mid(Session("mask"),3,2)

sTabella = Request ("NomeTab")
sAction = Request ("Action")
sData = Request ("Data")
sCodice = Request ("Codice")

'sIsa = 0
'sProfilo = 3
					
sSQL = "SELECT Nome_Tabella, Descrizione, Decorrenza, Scadenza, Lunghezza_Codice , Caratteristiche, Lunghezza_desc, Aut_Supporto, Aut_funzionale, Aut_Decisionale, Attributi FROM IDTAB WHERE Nome_Tabella = '" & sTabella & "'"					 
set rsTabelle = Server.CreateObject("ADODB.Recordset")
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
rsTabelle.Open sSQL, CC, 3
				
value = rsTabelle("Descrizione")

DecorrenzaT = ConvDateToString(rsTabelle("Decorrenza"))
ScadenzaT = ConvDateToString(rsTabelle("Scadenza"))
rsTabelle.Close
set rsTabelle = nothing

%>

<TABLE border="0" CELLPADDING="0" CELLSPACING="0" width="500">
<tr height="17">

	<td class="sfondomenu" width="67%" height="18"><span class="tbltext0">
		&nbsp;<B>INSERIMENTO/MODIFICA CODICI</b>
	</td>
	<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">
	</td>
	<td valign="middle" align="right" class="tbltext1" width="30%" 
		background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">(*) campi obbligatori</td>
</tr>
<tr>
	<td class="sfondocomm" width="57%" colspan="3">
		<BR>Gestione 
<%	
	select case sAction 
		case "MOD"
			Response.Write " modifica/chiusura validita' del codice selezionato." 
		case "INS" 
			Response.Write " inserimento di un nuovo codice."
		
	end select	
%> 

		<!--<IMG align=right src="<%=Session("Progetto")%>/images/help.gif" border=0>-->
	<a href="Javascript:Show_Help('../Help/Tab_Scheda.htm')" >
	<IMG align=right src="<%=Session("Progetto")%>/images/help.gif" border=0></a>

	
	</td>
</tr>
<tr height=2>
	<td class="sfondocomm" width="100%" colspan="3" 
		background="<%=Session("Progetto")%>/images/separazione.gif">
	</td>
</tr>
</table>
<BR>
<TABLE border="0" cellpadding="2" cellspacing="2" width="500" >
   <tr>
		<td align="left" colspan="2" nowrap class="tbltext1" width=20>
				<strong>Tabella:</strong></span>
		</td>
		<td align="left" colspan="2" width=500 class="textblack" >
<%
			Response.Write sTabella
%>		
		</b></td>
    </tr>
   <tr>
		<td align="left" colspan="2" nowrap class="tbltext1" width=20>
				<strong>Descrizione:</strong></span>
		</td>
		<td align="left" colspan="2" width=500 class="textblack">
<%
			Response.Write value
%>		
		<B></td>
    </tr>
   <tr>
		<td align="left" colspan="2" nowrap class="tbltext1">
				<strong>Valida dal</strong></span>
		</td>
		<td align="left" colspan="2" width=480 class="textblack">
<%
			Response.Write DecorrenzaT
%>		
		</B></td>
    
    <% if ScadenzaT <> "31/12/9999" then
		%>
			<td></td>
			<td align="left" colspan="2" nowrap class="tbltext1" width=20>
				<strong>al</strong></span>
			</td>
			<td align="left" colspan="2" width=500 class="textblack">
		<%
			Response.Write ScadenzaT
		%>		
			</B></td></tr>
		
	<% END IF %>

</TABLE><BR>
<TABLE border="0" cellpadding="2" cellspacing="2" width="500">
    <tr>
		<td height="2" align="left" colspan="4" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
    </tr>    
</TABLE>

<FORM name=formDati action="TAB_CnfCodici.asp">
	<TABLE cellspacing=0 cellpadding=0 align=center width=500 border=0>
		<INPUT type="hidden" name=txtDataSys value="<%=ConvDateToString(date())%>">
		<tr>
		<td></TD>
		</TR>
	</TABLE>
	
<%
	sSQL = "SELECT Nome_Tabella, Descrizione, Decorrenza, Scadenza, Lunghezza_Codice, Caratteristiche, Lunghezza_desc, Aut_Supporto, Aut_funzionale, Aut_Decisionale, Attributi FROM IDTAB WHERE Nome_Tabella = '" & sTabella & "'"

	set rsTabelle = Server.CreateObject("ADODB.Recordset")
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	rsTabelle.Open sSQL, CC, 3
	
	LungCodice = rsTabelle("Lunghezza_Codice")
	TipoCodice = rsTabelle("Caratteristiche")
	LungDesc = rsTabelle("Lunghezza_desc")
	
	rsTabelle.Close
	set rsTabelle = nothing
%>

	<TABLE cellspacing=2 cellpadding=1 align=center width=500 border=0>
		<TR>
		<TD class="tblText1" width=150 align=left>
			<B>Codice*</B>
		</TD>
		<TD colspan=3>
	<%
		select case sAction
			case "MOD"
				Response.Write "<INPUT style=TEXT-TRANSFORM:uppercase name=" &_
				 TipoCodice & " id=txtCodice maxlength=" & LungCodice &_
				 " style='HEIGHT: 22px; WIDTH: 166px' readOnly value=" &_
				  sCodice & ">"
			case "INS" 
				Response.Write "<INPUT style=TEXT-TRANSFORM:uppercase name=" &_
				 TipoCodice & " id=txtCodice maxlength=" & LungCodice &_
				  " style='HEIGHT: 22px; WIDTH: 166px'>"
		end select
	%>
		</TD>
		</TR>
		<TR>
		<TD class="tblText1" width=150 align=left>
			<B>Decorrenza* <br>(gg/mm/aaaa)</B>
		</TD>
		<TD>
<%	
					
	select case sAction 
		case "MOD"
			dim rsCodici
'			if sIsa = "0" then
				sSQL = "SELECT ISA, Nome_tabella, Codice, Decorrenza, Scadenza, Descrizione, Valore FROM TADES WHERE Nome_Tabella = '" & sTabella & _
						"' AND ISA = '"& sIsaCodice & _
						"' AND Codice = '" & sCodice & _
						"' AND " & ConvDateToDBs(ConvStringToDate(sData)) & " BETWEEN Decorrenza AND Scadenza"
'			else
'				sSQL = "SELECT ISA, Nome_tabella, Codice, Decorrenza, Scadenza, Descrizione, Valore FROM TADES WHERE Nome_Tabella = '" & sTabella & _
'						"' AND Codice = '" & sCodice & _
'						"' AND ISA ='" & sIsa & _
'						"' AND " & ConvDateToDBs(ConvStringToDate(sData)) & " BETWEEN Decorrenza AND Scadenza"
'			end if
			'Response.Write sSQL
			set rsCodici = Server.CreateObject("ADODB.Recordset")
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
			rsCodici.Open sSQL, CC
							
			sDecorrenza = ConvDateToString(rsCodici("Decorrenza"))		
			sDecorrenzaCodice = sDecorrenza
			sDecorrenzaOld=sDecorrenza 
			'Se la DECORRENZA sul DB � 01/01/1900 non la visualizzo
			If sDecorrenza = "01/01/1900" then
				sDecorrenza= ""
			end if

			sScadenza = ConvDateToString(rsCodici("Scadenza"))
			sScadenzaCodice = sScadenza
			'Se la SCADENZA sul DB � 31/12/9999 non la visualizzo
			if sScadenza = "31/12/9999" then
				sScadenza= ""
			end if
							
			sDescrizione = rsCodici("Descrizione")
			sValore = rsCodici("Valore")
	'		Response.Write "valore=#" & sValore & "#"
												
			rsCodici.Close
			set rsCodici = nothing

		case "INS"
			sDecorrenza = ""
			sScadenza =  ""
			sDescrizione = ""
			sValore = ""
	end select	
%>
		<INPUT id=txtDecorrenza dataformatas=dd/mm/yyyy maxlength=10 size=10 
			value=<%=sDecorrenza%>>
		<INPUT id=txtDecorrenzaOld type=hidden value=<%=sDecorrenza%>>			
		</TD>
	
		<TD width=100 class="tbltext1" align=left>
				<B>Scadenza <br>(gg/mm/aaaa)</B>
		</TD>
		<TD align=left>
			<INPUT id=txtScadenza maxlength=10 size=10 value=<%=sScadenza%>> 
		</TD>
		</TR>
		<TR>
		<TD width=150 class="tbltext1" align=left>
			<B>Descrizione*</B>
		</TD>
		<TD colspan=3>
			<TEXTAREA class=textblacka cols=30 ROWS=3 style="TEXT-TRANSFORM:uppercase" id=txtDescrizione><%=sDescrizione%></TEXTAREA> 
		</TD>
		</TR>
	</TABLE>
	<br>
	<TABLE cellspacing=2 cellpadding=1 align=center width=500 border=0>
		<TR>
		<TD background="<%=Session("Progetto")%>/images/separazione.gif">
		</TD>
		</TR>
	</TABLE>
	<br>
	<TABLE cellspacing=0 cellpadding=0 align=center width=500 border=0>
		<tr align=center class="tbltext3">
			<td>
			Attributi:
			</td>
		</tr>
	</table>
	<br>
<%
					
	sSQL = "SELECT Nome_Tabella, Descrizione, Decorrenza, Scadenza, Lunghezza_Codice, Caratteristiche, Lunghezza_desc, Aut_Supporto, Aut_funzionale, Aut_Decisionale, Attributi FROM IDTAB WHERE Nome_Tabella = '" & sTabella & "'"

	set rsTabella = Server.CreateObject("ADODB.Recordset")
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	rsTabella.Open sSQL, CC, 3
			
	sAtt = rsTabella("Attributi")
	
	'inizializzazione delle variabili
	j = 0
	k = 0						
	i = 0

	for j = 1 to 10
		i = (j -1) * 24 +1
		
		if mid(trim(sAtt), i, 1) <> "" and _
			mid(trim(sAtt), i, 1) <> "0" then
			arrayInputString (j) = mid(sAtt,i,24) 		
			k = j
		else
			j = 10
		end if
	next 
		
	rsTabella.Close
	set rsTabella = nothing
			
	cursore = 0

	if clng(k) <> 0 then %>
		<TABLE cellspacing=0 cellpadding=0 align=center width="500" border=0>
		<TR class="tblsfondo3" align=middle width="500">
		<td height=15 width="15" valign="middle"><IMG SRC="/Plavoro/images/righinaPSX.jpg"></td> 
		<td  class="tbltext0" width=40 align=center><b>Num.</b></td>
		<td  class="tbltext0" width=70 align=center><b>Nome</b></td>
		<td  class="tbltext0" width=30 align=center><b>LL</b></td>
		<td  class="tbltext0" width=110 align=center><b>Tipo</b></td>
		<td  class="tbltext0" width=100 align=center><b>Obbligatoriet�</b></td>
		<td  class="tbltext0" width=120 align=center><b>Valore</b></td>
		<td height=15 width="15" valign="middle"><IMG SRC="/Plavoro/images/righinaPDX.jpg"></td> 
		</TR>
		</table>		
<%else%>
		<TABLE cellspacing=0 cellpadding=0 align=center width="500" border=0>
		<tr>
		<td align="Center" class="tbltext3">Non ci sono Attributi da visualizzare.</TD></TR>
		</table>
<%	end if

	for i = 1 to K
		if i = 1 then
			Response.Write "<TABLE cellspacing=1 cellpadding=1 align=center width=500 border=0>"
		end if

%>		
		<TR class="tblsfondo">
			<TD class="sfondocentro" width="15" align="middle">&nbsp;</TD>
			<TD class="textBlack" width="40" align="middle">
				<b><%=i%></b>
			</TD>
			<TD class="textBlack" width="70" align="center">
				&nbsp;<%=mid(arrayInputstring(i), 4, 20)%>
			</TD>
			<TD class="textBlack" width="30" align="center">
				&nbsp;<%=mid(arrayInputstring(i), 2, 2)%>
			</TD>
			<TD class="textBlack" width="110"  align="center">
<%
			select case mid(arrayInputstring(i), 1, 1) 
				case "A"
					Response.Write "Alfanumerico"
				case "N"
					Response.Write "Numerico"
				case "D"
					Response.Write "Data"
				case else 
					Response.Write "&nbsp;"
			end select
%>
			</td>
			<TD class="textBlack" width="100" align="center">
<%
			select case mid(arrayInputstring(i), 24, 1) 
				case "O"
					Response.Write "<FONT face='Verdana, Arial, Helvetica, sans-serif' size=2 color=red> Obbligatorio </FONT>"
				case "F"
					Response.Write "Facoltativo"
				case else 
					Response.Write "&nbsp;"
			end select
%>
			</TD>
			<TD class="textBlack" width="120" align="center">
				<INPUT size=12 style=TEXT-TRANSFORM:uppercase name='<%
					Response.write mid(arrayInputstring(i), 24, 1) & "," & mid(arrayInputstring(i), 1, 1)%>' 
					id=txtValore  maxlength="<%=mid(arrayInputstring(i), 2, 2)%>" 
					value="<%=trim(mid(sValore, cursore+1, mid(arrayInputstring(i), 2, 2)))%>">
					<%cursore = cursore + mid(arrayInputstring(i), 2, 2)%>
			</TD>
			<TD class="sfondocentro" width="15" align="middle"> &nbsp;</td>
		</TR>
	<%
		if i = k then
			Response.Write "</TABLE>"
		end if
		
		next
	%>
	

	<br>
	<TABLE cellspacing=0 cellpadding=0 width="500" border=0 align=right>
		<tr>
			<td align="Center" class="tbltext3">
<%
	select case sAction
		case "MOD" 
			'La MODIFICA � possibile solo a chi entra con PROFILO = '2' oppure '3';
			'per il profilo '1' e '0' non la visualizzo

			if ckProfile(sProfilo,4)=true then
'			if sProfilo = "2" or _ 
'				sProfilo = "3" then
				
					'Response.Write "<INPUT type='IMAGE' onclick=""return Inserimento('" & _
					'	sTabella & "','" & (i-1) & "')"" id=cmdSalva src=""" & session("progetto") & "/images/conferma.gif"">"
				
				Response.Write "<IMG onclick=""return Modifica(" & LungDesc & ",'" & _
					sTabella & "','" & (i-1) & _
					"','" & sIsaCodice & "','" & _
					sScadenzaCodice & "','" & sDecorrenzaCodice & _
				    "'," & sDecorrenzaOld & ")"" id=cmdModifica src=""" & session("progetto") & "/images/conferma.gif"">"
				
				'Response.Write "<a href=""Javascript:return Modifica('" & _
					'sTabella & "','" & (i-1) & _
					'"','" & sIsaCodice & "','" & _
					'sScadenzaCodice & "','" & sDecorrenzaCodice & _
					'"')"" id=cmdModifica><Img src='" & session("Progetto") &_
				'"/images/conferma.gif' value='Chiudi validit�' border=0></IMG></A>"
				'Response.Write "&nbsp;"
			end if 								
		
		if (ConvStringToDate(sScadenza) =< Date()) or (ConvStringToDate(sDecorrenza) >= Date()) then
		else
			'La CANCELLAZIONE � possibile solo per il PROFILO = '3'
			if ckProfile(sProfilo,8)=true then
'			if sProfilo = "3" then				
				Response.Write "<a href=""javascript:Delete('" & _
				sCodice & "','" & sTabella & "','" & sData & _
				"','" & sIsaCodice & "')"" id=cmdCancella><Img src='" & session("Progetto") &_
				"/images/chiudi.gif' value='Chiudi validit�' border=0></IMG></A>"
			else
				%>
				<CENTER>
				<B>
				<A HREF='TAB_VisCodici.asp?Data=<%=date()%>&NomeTab=<%=sTabella%>' 
					class="textRed">Torna all'Elenco dei Codici</A>
				</B>
				</CENTER>
				<%
			end if
		end if
		
		case "INS" 
			' Se siamo nel PROFILO = '0' � possibile solo la visualizzazione della tabella
			if ckProfile(sProfilo,2)=true then
'			if (sProfilo <> "0") then
					Response.Write "<IMG onclick=""return Inserimento(" & LungDesc & ",'" & _
						sTabella & "','" & (i-1) & "')"" id=cmdSalva src=""" & session("progetto") & "/images/conferma.gif"">"
			end if
	end select
%>
	<A class=textred HREF='javascript:onclick=history.back()'><img src="<% session("Progetto") %>/Plavoro/images/indietro.gif" border=0></A>
			</td>
		</tr>
	</table>
</FORM>
<!--#include virtual = "/Include/CloseConn.asp"-->
<!--#include Virtual="/strutt_coda2.asp"-->		

<%Sub Inizio()%>

<table border="0" width="520" cellspacing="0" cellpadding="0" height="81">
   <tr>
     <td width="500"  background="<%=Session("Progetto")%>/images/titoli/strumenti2b.gif" height="81" valign="bottom" align="right">
       <table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
         <tr>
           <td width="100%" valign="top" align="right"><b class="tbltext1a">Gestione Codici Tabelle</span></b></td>
         </tr>
       </table>
     </td>
   </tr>
</table><br>

<%End Sub%>
