<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include virtual ="/strutt_testa2.asp"-->
<!--#include virtual = "/Include/ControlDateVB.asp"-->
<!--#include virtual ="/include/openconn.asp"-->
<!--#include virtual = "/util/portallib.asp"-->
<!--#include virtual ="/include/ckProfile.asp"-->
<%
if ValidateService(session("idutente"),"TAB_VISIDTAB",cc) <> "true" then 
	response.redirect "/util/error_login.asp"
end if
%>
<SCRIPT LANGUAGE="Javascript">

//include del file per fare i controlli sulla validit� delle date
<!--#include virtual ="/Include/ControlDate.inc"-->
<!--#include Virtual = "/Include/ControlString.inc"-->


//include del file per fare i controlli sulla numericit� dei campi
<!--#include virtual ="/Include/ControlNum.inc"-->
<!--#include virtual ="/Include/help.inc"-->
//Questa Funzione viene richiamata sull'onclick del pulsante CHIUDI VALIDITA'
	function Delete(sNomeTab, sData){	
		if (confirm("Sei sicuro di voler chiudere la validit� di questa Tabella alla data di oggi?")){
			location.href = "TAB_CnfIdtab.asp?Action=CAN&NomeTab=" + sNomeTab
		}		
	}

	function elaboraAttributi(numero_elementi){
		var sAttributi
		var nLunghMaxAttrib
		var infLeng
		var valore
		var sAtt
		
		nLunghMaxAttrib = 0
		infLeng = 0
		
		sAttributi = ""
		sAtt = ""
		
	//	i=16
		i=18
		for (k=1; k<=10; k++){	
			if (formDati.elements[i].value != ""){
				//TIPO Attributo
				if (formDati.elements[i].value == "Alfanumerico"){
					sTipo = "A"
				}
				else if (formDati.elements[i].value == "Numerico"){
						sTipo = "N"
						}
						else if (formDati.elements[i].value == "Data"){
							sTipo = "D"
							}
							else{
								sTipo = ""
							}
				//LUNGHEZZA Attributo (Se la lunghezza � di
				// un solo byte gli metto uno zero davanti)
				if (formDati.elements[i+1].value.length == 1){
					sLL = "0" + formDati.elements[i+1].value
				}
				else {
					sLL = formDati.elements[i+1].value
				}
				//NOME Attributo
				nLunghMaxAttrib = formDati.elements[i+2].maxLength
				valore = formDati.elements[i+2].value
				infLeng = valore.length
				for (j=0; j<(nLunghMaxAttrib-infLeng);j++){
					valore = valore + " "
				}
				sNome = valore
				//OBBLIGATORIETA' Attributo
				if (formDati.elements[i+3].value == "Obbligatorio"){
					sObb = "O"
				}
				else if (formDati.elements[i+3].value == "Facoltativo"){
						sObb = "F"
					}
					else{
						sObb = ""
					}
			
				sAttributi = sTipo + sLL + sNome + sObb
				sAtt = sAtt + sAttributi
				}
			i = i + 4 
			}
	//alert(sAtt)
	return sAtt
}	

//Questa Funzione viene richiamata sull'onclick del pulsante SALVA
	function Inserimento(){	
		var sAtt
		
		numero_elementi = (formDati.length)-1
	
		if (ControllaDati(numero_elementi)){	
			sNomeTab = formDati.txtNomeTab.value 
			sDecorrenza = formDati.txtDecorrenza.value
			sScadenza = formDati.txtScadenza.value
			if  (sScadenza == "")
			{
				sScadenza = "31/12/9999"
			}
			sDescrizione = formDati.txtDescrizione.value
			sLungCodice = formDati.txtLungCodice.value
			if (formDati.cmbCaratt.value == "Alfanumerico"){
				sCaratt = "A"
				}
			else{
				sCaratt = "N"
			}
			sLungDesc = formDati.txtLungDescCodice.value
			if (formDati.optSupportoN.checked){
				sSupporto = "N"
			}
			if (formDati.optSupportoI.checked){
				sSupporto = "I"
			}
			if (formDati.optSupportoG.checked){
				sSupporto = "G"
			}
			if (formDati.optFunzionaleN.checked){
				sFunzionale = "N"
			}
			if (formDati.optFunzionaleI.checked){
				sFunzionale = "I"
			}
			if (formDati.optFunzionaleG.checked){
				sFunzionale = "G"
			}
			if (formDati.optDecisionaleN.checked){
				sDecisionale = "N"
			}
			if (formDati.optDecisionaleI.checked){
				sDecisionale = "I"
			}
			if (formDati.optDecisionaleG.checked){
				sDecisionale = "G"
			}			
			// Ritroviamo la lunghezza massima degli attributi
			sAtt = elaboraAttributi(numero_elementi)
			
			location.href = "TAB_CnfIdtab.asp?Action=INS&NomeTab=" + sNomeTab + 
				"&Decorrenza=" + sDecorrenza +
				"&Scadenza=" + sScadenza + 
				"&Descrizione=" + sDescrizione +
				"&lunghezza_codice=" + sLungCodice +
				"&caratteristiche=" + sCaratt +
				"&lunghezza_desc=" + sLungDesc +
				"&aut_supporto=" + sSupporto +
				"&aut_funzionale=" + sFunzionale +
				"&aut_decisionale=" + sDecisionale + 
				"&Attributi=" + sAtt
				
		}
		else
			return false
			
	}

//Questa Funzione viene richiamata sull'onclick del pulsante MODIFICA
	function Modifica(sNomeTab){	
		var sAtt

		numero_elementi = (formDati.length)-2

		if (ControllaDati(numero_elementi)){
			sNomeTab = formDati.txtNomeTab.value 
			sDecorrenza = formDati.txtDecorrenza.value
			sScadenza = formDati.txtScadenza.value
			if  (sScadenza == "")
			{
				sScadenza = "31/12/9999"
			}
			sDescrizione = formDati.txtDescrizione.value
			sLungCodice = formDati.txtLungCodice.value
			if (formDati.cmbCaratt.value == "Alfanumerico"){
				sCaratt = "A"
				}
			else{
				sCaratt = "N"
			}
			sLungDesc = formDati.txtLungDescCodice.value 
			if (formDati.optSupportoN.checked){
				sSupporto = "N"
			}
			if (formDati.optSupportoI.checked){
				sSupporto = "I"
			}
			if (formDati.optSupportoG.checked){
				sSupporto = "G"
			}
			if (formDati.optFunzionaleN.checked){
				sFunzionale = "N"
			}
			if (formDati.optFunzionaleI.checked){
				sFunzionale = "I"
			}
			if (formDati.optFunzionaleG.checked){
				sFunzionale = "G"
			}
			if (formDati.optDecisionaleN.checked){
				sDecisionale = "N"
			}
			if (formDati.optDecisionaleI.checked){
				sDecisionale = "I"
			}
			if (formDati.optDecisionaleG.checked){
				sDecisionale = "G"
			}
						
			// Ritroviamo la lunghezza massima degli attributi
			sAtt = elaboraAttributi(numero_elementi)
			
			ciccio = "TAB_CnfIdtab.asp?Action=MOD&NomeTab=" + sNomeTab + 
				"&Decorrenza=" + sDecorrenza +
				"&Scadenza=" + sScadenza + 
				"&Descrizione=" + sDescrizione +
				"&lunghezza_codice=" + sLungCodice +
				"&caratteristiche=" + sCaratt +
				"&lunghezza_desc=" + sLungDesc +
				"&aut_supporto=" + sSupporto +
				"&aut_funzionale=" + sFunzionale +
				"&aut_decisionale=" + sDecisionale + 
				"&Attributi=" + sAtt
			//alert(ciccio)
			location.href = ciccio
		}
		else
			return false
	}

//Funzione per i controlli dei campi 
	function ControllaDati(numero_elementi){
		var Decorrenza
		var Scadenza
		
		//NOME TABELLA obbligatorio
		formDati.txtNomeTab.value=TRIM(formDati.txtNomeTab.value)
		if (formDati.txtNomeTab.value == ""){
			alert("Nome Tabella obbligatorio")
			formDati.txtNomeTab.focus() 
			return false
		}

		//DECORRENZA obbligatoria
		if (formDati.txtDecorrenza.value == ""){
			alert("Decorrenza obbligatoria")
			formDati.txtDecorrenza.focus() 
			return false
		}
		
		//Controllo della validit� della DECORRENZA
		Decorrenza = formDati.txtDecorrenza.value
		if (!ValidateInputDate(Decorrenza)) 
		{
			formDati.txtDecorrenza.focus() 
			return false
		}
		
		//Controllo della validit� della SCADENZA
		Scadenza = formDati.txtScadenza.value
		if (Scadenza != ""){
			if (!ValidateInputDate(Scadenza)) {
				formDati.txtScadenza.focus() 
				return false
			}
					
			if (!ValidateRangeDate(Decorrenza,Scadenza)) {
				alert("Controllare il periodo di validit�")			
				formDati.txtScadenza.focus() 
				return false
			}
					
			if (!ValidateRangeDate(formDati.txtDataSys.value,Scadenza)) {
				alert("La scadenza non pu� essere inferiore alla data odierna")			
				formDati.txtScadenza.focus() 
				return false
			}
		}
		
		//DESCRIZIONE obbligatoria
		formDati.txtDescrizione.value =TRIM(formDati.txtDescrizione.value)
		if (formDati.txtDescrizione.value == ""){
			alert("Descrizione obbligatoria")
			formDati.txtDescrizione.focus() 
			return false
		}
		
		//LUNGHEZZA CODICE obbligatoria
		if (formDati.txtLungCodice.value == ""){
			alert("Lunghezza Codice obbligatoria")
			formDati.txtLungCodice.focus() 
			return false
		}
		
		//LUNGHEZZA CODICE deve essere numerica
		if (!IsNum(formDati.txtLungCodice.value)){
			alert("La Lunghezza del Codice deve essere Numerica")
			formDati.txtLungCodice.focus()
			return false
		}
		
		//LUNGHEZZA CODICE deve essere diversa da zero
		if (eval(formDati.txtLungCodice.value) == 0){
			alert("La Lunghezza del Codice non pu� essere nulla")
			formDati.txtLungCodice.focus()
			return false
		}
		
		//LUNGHEZZA CODICE deve essere minore di 12
		if (eval(formDati.txtLungCodice.value) > 12){
			alert("La Lunghezza del Codice deve essere minore di 12")
			formDati.txtLungCodice.focus()
			return false
		}
		
		//CARATTERISTICHE CODICE obbligatorie
		if (formDati.cmbCaratt.value == ""){
			alert("Caratteristiche Codice obbligatorie")
			formDati.cmbCaratt.focus() 
			return false
		}
		
		//LUNGHEZZA DESCRIZIONE CODICE obbligatoria
		if (formDati.txtLungDescCodice.value == ""){
			alert("Lunghezza Descrizione Codice obbligatoria")
			formDati.txtLungDescCodice.focus() 
			return false
		}
	
		//LUNGHEZZA DESCRIZIONE deve essere numerica
		if (!IsNum(formDati.txtLungDescCodice.value)){
			alert("La Lunghezza della Descrizione del Codice deve essere Numerica")
			formDati.txtLungDescCodice.focus()
			return false
		}
		
		//LUNGHEZZA DESCRIZIONE deve essere diversa da zero
		if (eval(formDati.txtLungDescCodice.value) == 0){
			alert("La Lunghezza della Descrizione del Codice non pu� essere nulla")
			formDati.txtLungDescCodice.focus()
			return false
		}
		
		//LUNGHEZZA DESCRIZIONE deve essere minore di 50
		if (eval(formDati.txtLungDescCodice.value) > 50){
			alert("La Lunghezza della Descrizione del Codice deve essere minore di 50")
			formDati.txtLungDescCodice.focus()
			return false
		}
		
		//Controlli sugli ATTRIBUTI
		var i
		var j
		var somma
		
		somma = 0
		
		//Ciclo per il controllo delle LUNGHEZZE degli ATTRIBUTI
		//for (i=16; i<=55; i=i+4){	
			for (i=18; i<=57; i=i+4){	
			//Le LUNGHEZZE degli ATTRIBUTI, se digitate, devono essere numeriche
			if (formDati.elements[i+1].value != ""){
				//alert(formDati.elements[i].name)
				if (!IsNum(formDati.elements[i+1].value)){
					alert("La Lunghezza deve essere numerica")
					formDati.elements[i+1].focus()
					return false
				}
			}
			//Le LUNGHEZZE degli ATTRIBUTI, se digitate, non devono essere nulle
			if (formDati.elements[i+1].value != ""){
				if (formDati.elements[i+1].value == 0){
					alert("La Lunghezza non pu� essere nulla")
					formDati.elements[i+1].focus()
					return false
				}
			}
			//Le LUNGHEZZE degli ATTRIBUTI, se digitate, non possono essere complessivamente maggiori di 50
			if (formDati.elements[i+1].value != ""){
				somma = somma +	eval(formDati.elements[i+1].value)
				if (eval(somma) > 50){
					alert("La Somma delle Lunghezze non pu� essere superiore a 50")
					formDati.elements[i+1].focus()
					return false
				}
			}
		}
		
		//Ciclo per il controllo dei TIPI degli ATTRIBUTI
		//	for (i=16; i<=55; i=i+4){	
			for (i=18; i<=57; i=i+4){
			
			//formDati.elements[i].value=TRIM(formDati.elements[i].value)	
			
			
			//Se TIPO � Data la LUNGHEZZA deve essere 10
			if ((formDati.elements[i].value == "Data") && 
				(eval(formDati.elements[i+1].value) != 10)){
				alert("La Lunghezza della Data deve essere 10")
				formDati.elements[i+1].focus()
				return false
			}
			//Se � digitato almeno un campo degli ATTRIBUTI,
			// i restanti campi sono obbligatori
			/*	alert(formDati.elements[i].name + " elemento i")  
				alert(formDati.elements[i+1].name + " elemento i+1") 
				alert(formDati.elements[i+2].name + " elemento i+2")  
				alert(formDati.elements[i+3].name + " elemento i+3") 
				*/	
					
				formDati.elements[i+1].value=TRIM(formDati.elements[i+1].value)	
				formDati.elements[i+2].value=TRIM(formDati.elements[i+2].value)
				formDati.elements[i+3].value=TRIM(formDati.elements[i+3].value)
						
		if (((formDati.elements[i].value != "") && 
				(formDati.elements[i+1].value != "") &&
				(formDati.elements[i+2].value != "") && 
				(formDati.elements[i+3].value != "")) ||
				((formDati.elements[i].value == "") && 
				(formDati.elements[i+1].value == "") &&
				(formDati.elements[i+2].value == "") && 
				(formDati.elements[i+3].value == ""))){
				}
				
			else{
				alert("Relazione tra i dati errata")
				formDati.elements[i].focus()
				return false
			}
		}
		
		//Se non c'� SCADENZA metto per default 31/12/9999
	/*	if  (formDati.txtScadenza.value == ""){
			formDati.txtScadenza.value = "31/12/9999"
		}
*/	return true
	}
</SCRIPT>
<FORM name=formDati action=TAB_CnfIdtab.asp>
<%
	dim sAction
	dim sNomeTab
	
	sAction = Request ("Action")
	
	sNomeTab = Request ("NomeTab")
	
	if mid(Session("mask"),1,1) = "0" then
		sIsa = "0"
	else
		sIsa = mid(Session("mask"),1,2)
	end if
	sProfilo = mid(Session("mask"),3,2)	
	
%>
<table border="0" width="520" cellspacing="0" cellpadding="0" height="81">
   <tr>
     <td width="500"  background="<%=Session("Progetto")%>/images/titoli/strumenti2b.gif" height="81" valign="bottom" align="right">
       <table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
         <tr>
           <td width="100%" valign="top" align="right"><b class="tbltext1a">Amministrazione Tabelle </span></b></td>
         </tr>
       </table>
     </td>
   </tr>
</table><br>
<TABLE border="0" CELLPADDING="0" CELLSPACING="0" width="500">
<tr height="17">

	<td class="sfondomenu" width="67%" height="18"><span class="tbltext0">
		<b>&nbsp;INSERIMENTO/MODIFICA NUOVA TABELLA </b>
	</td>
	<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">
	</td>
	<td valign="middle" align="right" class="tbltext1" width="30%" 
		background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">(*) campi obbligatori</td>
</tr>
<tr>
	<td class="sfondocomm" width="57%" colspan="3">
		<BR>
			Compilare il form con i dati per l'inserimento o la modifica della tabella.
			<!--<img align=right src="<%=Session("Progetto")%>/images/help.gif">-->
			<a href="Javascript:Show_Help('../Help/TAB_ElencoTabelle.htm')" >
			<IMG align=right src="<%=Session("Progetto")%>/images/help.gif" border=0></a>
	</td>
</tr>
<tr height=2>
	<td class="sfondocomm" width="100%" colspan="3" 
		background="<%=Session("Progetto")%>/images/separazione.gif">
	</td>
</tr>
</table>
<br>
<!--TABLE cellspacing=2 cellpadding=1 align=center width=500  border=1>
	<TR bgcolor='#363a90'>
	<TD>
		<B><CENTER>
		<FONT face="Verdana, Arial, Helvetica, sans-serif" size=4 color='#b1f312'>
		<%	
			select case sAction 
				case "MOD"
					Response.Write "Modifica/Chiusura validit� della Tabella" 
				case "INS" 
					Response.Write "Inserimento di una nuova Tabella"
			end select	
		%>
		</FONT>
		</CENTER>
		</B>
	</TD>
	</TR>
</TABLE!-->

<!--FONT color=red face="Verdana, Arial, Helvetica, sans-serif" size=2>
<P align=center>
(I campi in rosso sono Obbligatori)
</P>
</FONT>
</STRONG!-->

	<TABLE border="0" CELLPADDING="1" CELLSPACING="2" width="500" align=center>
		<tr height="17">
		<TD width=170 class="tbltext1">
			<B>	Nome Tabella*</B>
		</TD>
		<TD width=330 class="tbltext1">
			<!--INPUT type="text" name=txtArea style="width=160px"-->
			<% select case sAction 
				case "MOD"
						Response.Write "<INPUT type=text name=txtNomeTab readonly value='" & sNomeTab & "' style='width=160px'>"
				case "INS"
						Response.Write "<INPUT type=text name=txtNomeTab style='width=160px' maxlength=5>"
				end select %>			
			<INPUT type="hidden" name=txtTipoArea value="">
			<INPUT type="hidden" name=txtDataSys value="<%=convdatetostring(date())%>">
		</TD>
	</TABLE>

	<TABLE border="0" CELLPADDING="1" CELLSPACING="2" width="500">
		<TR  height="17" align=center>
		<TD align=left width=165 class="tbltext1">
			<B>Decorrenza*<BR>(gg/mm/aaaa)</B>
		</TD>
		<TD width=90>
		<%	select case sAction 
				case "MOD"
					dim rsCodici
					dim sSQL
					dim CnConn
					dim sDecorrenza
					dim sScadenza
					dim sDescrizione						
					dim sLungCodice
					dim sLungDesc
					dim sSupporto
					dim sFunzionale
					dim sDecisionale
					dim sAtt
						
					sSQL = "SELECT Nome_tabella, Decorrenza, Scadenza, Descrizione, lunghezza_codice, caratteristiche, lunghezza_desc, aut_supporto, aut_funzionale, aut_decisionale, attributi FROM IDTAB WHERE Nome_Tabella = '" & sNomeTab & "'"
			
					set rsCodici = Server.CreateObject("ADODB.Recordset")

'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
					rsCodici.Open sSQL, CC
					
					sDecorrenza = ConvDateToString(rsCodici("Decorrenza"))
					sScadenza = ConvDateToString(rsCodici("Scadenza"))
					'Se la SCADENZA sul DB � 31/12/9999 non visualizzo nulla
					if sScadenza = "31/12/9999" then
						sScadenza= ""
					end if
					sDescrizione = rsCodici("Descrizione")
					sLungCodice = rsCodici("lunghezza_codice")
					sLungDesc = rsCodici("lunghezza_desc")
					sSupporto = rsCodici("aut_supporto")
					sFunzionale = rsCodici("aut_funzionale")
					sDecisionale = rsCodici("aut_decisionale")
					sAtt = rsCodici("attributi")
						
				case "INS"
						
			end select	%>
			<INPUT id=txtDecorrenza dataformatas=dd/mm/yyyy 
				maxlength=10 size=10  value=<%=sDecorrenza%>> 
		</TD>
		<TD width=100 class="tbltext1">
			<B>Scadenza<BR>	(gg/mm/aaaa) </B>
		</td>
		<td width=130>
			<INPUT id=txtScadenza maxlength=10 size=10 value=<%=sScadenza%>> 
		</TD>
		</TR>
	</TABLE>

	<TABLE border="0" CELLPADDING="1" CELLSPACING="2" width="500">
<tr height="17">
		<TD width=170 class="tbltext1">
			<B>Descrizione*</B>
		</TD>
		<TD width=330>
			<TEXTAREA maxlength=30 cols=30 style=TEXT-TRANSFORM:uppercase id=txtDescrizione class=textblacka><%=sDescrizione%></TEXTAREA> 
		</TD>
	</TR>
	</TABLE>
	
	<TABLE border="0" CELLPADDING="1" CELLSPACING="2" width="500">
	<tr height="17">
		<TD width=170 class="tbltext1">
			<B>Lunghezza Codice*</B>
		</TD>
		<TD width=330>
			<INPUT id=txtLungCodice maxlength=2 size=10 value=<%=sLungCodice%>>  
		</TD>
		</TR>
	</TABLE>
	
	<TABLE border="0" CELLPADDING="1" CELLSPACING="2" width="500">
	<tr height="17">
		<TD width=170 class="tbltext1">
			<B>Caratteristiche Codice*</B>
		</TD>
		<TD width=330>
			<SELECT id=cmbCaratt name=cmbCaratt><%
			select case sAction
				case "INS"%><OPTION selected>
					<OPTION value=Alfanumerico>Alfanumerico
					<OPTION value=Numerico>Numerico
					</OPTION>
			<%  case "MOD"
					if rsCodici("caratteristiche") = "A" then%><OPTION>
						<OPTION value=Alfanumerico selected>Alfanumerico
						<OPTION value=Numerico>Numerico
						</OPTION><%else%><OPTION>
						<OPTION value=Alfanumerico>Alfanumerico
						<OPTION value=Numerico selected>Numerico
						</OPTION>
					<%end if
			end select%>
			</SELECT>
		</TD>
		</TR>
	</TABLE>
	
	<TABLE border="0" CELLPADDING="1" CELLSPACING="2" width="500">
	<tr height="17">
		<TD width=170 class="tbltext1">
			<B>Lunghezza Desc. Codice*</B>
		</TD>
		<TD width=330>
			<INPUT id=txtLungDescCodice maxlength=2 size=10 value=<%=sLungDesc%> >  
		</TD>
		</TR>
	</TABLE>
	<BR>
	<TABLE border="0" CELLPADDING="1" CELLSPACING="2" width="500" >
	<tr height="17" class="sfondocomm">
		<TD width=200 align=center><B>Autorizzazione*</B></TD>
		<TD width=200 align=center><B>Nessuna</B></TD>
		<TD width=200 align=center><B>Interrogazione</B></TD>
		<TD width=200 align=center><B>Gestione</B></TD>
	</tr>
	<tr class=tblsfondo>
		<TD width=200 class="tbltext1"><B>Supporto</B></TD>
		<TD>
			<CENTER>
			<%
				select case sAction
					case "INS"%><INPUT CHECKED type="radio" id=optSupportoN name=optSupporto value=N>
				<% 	case "MOD"
						if rsCodici("aut_supporto") = "N" then%>
							<INPUT CHECKED type="radio" id=optSupportoN name=optSupporto value=N>
				<%		else%>
							<INPUT type="radio" id=optSupportoN name=optSupporto>
				<%		end if
				end select%>
			</CENTER>	
		</TD>
		<TD>
			<CENTER>
			<%
				select case sAction
					case "INS"%><INPUT type="radio" id=optSupportoI name=optSupporto>
				<%	case "MOD"
						if rsCodici("aut_supporto") = "I" then%>
							<INPUT CHECKED type="radio" id=optSupportoI name=optSupporto value=I>
				<%		else%>
							<INPUT type="radio" id=optSupportoI name=optSupporto>
				<%		end if 
				end select%>
			</CENTER>	
		</TD>
		<TD>
			<CENTER>
			<%
				select case sAction
					case "INS"%><INPUT type="radio" id=optSupportoG name=optSupporto>
				<%	case "MOD"
						if rsCodici("aut_supporto") = "G" then%>
							<INPUT CHECKED type="radio" id=optSupportoG name=optSupporto value=G> 
				<%		else%>
							<INPUT type="radio" id=optSupportoG name=optSupporto>
				<%		end if 
				end select%>
			</CENTER>	
		</TD>
	</TR>
	<TR class=tblsfondo>
		<TD width=200 class="tbltext1"><B>Funzionale</B></TD>
		<TD>
			<CENTER>
				<% 
				select case sAction
					case "INS"%><INPUT CHECKED type="radio" id=optFunzionaleN name=optFunzionale value=N>
				<%	case "MOD"
						if rsCodici("aut_funzionale") = "N" then %>
							<INPUT CHECKED type="radio" id=optFunzionaleN name=optFunzionale value=N>
				<%		else%>
							<INPUT type="radio" id=optFunzionaleN name=optFunzionale>
				<%		end if
				end select%>
			</CENTER>	
		</TD>
		<TD>
			<CENTER>
				<%
				select case sAction
					case "INS"%><INPUT type="radio" id=optFunzionaleI name=optFunzionale>
				<%	case "MOD"
						if rsCodici("aut_funzionale") = "I" then%>
							<INPUT CHECKED type="radio" id=optFunzionaleI name=optFunzionale value=I>
				<%		else%>
							<INPUT type="radio" id=optFunzionaleI name=optFunzionale>
				<%		end if
				end select%>
			</CENTER>	
		</TD>
		<TD>
			<CENTER>
				<%
				select case sAction
					case "INS"%><INPUT type="radio" id=optFunzionaleG name=optFunzionale>
				<%	case "MOD"
						if rsCodici("aut_funzionale") = "G" then%>
							<INPUT CHECKED type="radio" id=optFunzionaleG name=optFunzionale value=G>
				<%		else%>
							<INPUT type="radio" id=optFunzionaleG name=optFunzionale>
				<%		end if
				end select%>
			</CENTER>	
		</TD>
	</TR>
	<TR class=tblsfondo>
		<TD width=200 class="tbltext1"><B>Decisionale</B></TD>
		<TD>
			<CENTER>
				<%
				select case sAction
					case "INS"%><INPUT CHECKED type="radio" id=optDecisionaleN name=optDecisionale value=N>
				<%	case "MOD"
						if rsCodici("aut_decisionale") = "N" then%>
							<INPUT CHECKED type="radio" id=optDecisionaleN name=optDecisionale value=N>
				<%		else%>
							<INPUT type="radio" id=optDecisionaleN name=optDecisionale>
				<%		end if
				end select%>
			</CENTER>	
		</TD>
		<TD>
			<CENTER>
				<%
				select case sAction
					case "INS"%><INPUT type="radio" id=optDecisionaleI name=optDecisionale>
				<%	case "MOD"
						if rsCodici("aut_decisionale") = "I" then%>
							<INPUT CHECKED type="radio" id=optDecisionaleI name=optDecisionale value=I>
				<%		else%>
							<INPUT type="radio" id=optDecisionaleI name=optDecisionale>
				<%		end if
				end select%>
			</CENTER>	
		</TD>
		<TD>
			<CENTER>
				<%
				select case sAction
					case "INS"%><INPUT type="radio" id=optDecisionaleG name=optDecisionale>
				<%	case "MOD"
						if rsCodici("aut_decisionale") = "G" then%>
							<INPUT CHECKED type="radio" id=optDecisionaleG name=optDecisionale value=G>
				<%		else%>
							<INPUT type="radio" id=optDecisionaleG name=optDecisionale>
				<%		end if
				end select%>
			</CENTER>	
		</TD>
	</TR>
	</TBODY>
	</TABLE>
	<br>
	<STRONG class=tbltext3><P align=center>Attributi:</P></STRONG>
	
<%
	on error resume next 
	dim i, j, k 
	dim arrayInputString(10)
	dim cursore
			
	'inizializzazione delle variabili
	j = 0
	k = 0						
	i = 0

	for j = 1 to 10
		i = (j -1) * 24 +1
		if mid(trim(sAtt), i, 1) <> " " and _
			mid(trim(sAtt), i, 1) <> "0" then
			arrayInputString (j) = mid(sAtt,i,24) 		
			k = j
		else
			j = 10
		end if
	next 
		
	cursore = 0
	
	'if k <> 0 then
	%>
	<TABLE cellspacing=0 cellpadding=0 align=center width="500" border=0>
		<tr class="sfondocomm" align=middle width="500"> 
		<td width=50 align=center><b>Num.</b></td>
		<td width=150 align=center><b>Tipo</b></td>
		<td width=50 align=center><b>LL</b></td>
		<td width=125 align=center><b>Nome</b></td>
		<td width=125 align=center><b>Obbligatoriet�</b></td>
		</tr>
	</TABLE>
	<TABLE cellspacing=2 cellpadding=1 align=center width="500" border="0">

		<!--/table>
		<TABLE cellspacing=2 cellpadding=1 align=center width="500" border=0>
		<table cellspacing="0" cellpadding="0" bordercolor=#3399CC align=center width="500" border=1 class=tbltext1>
		<TR class=tbltext1>
		<TD align=center>
			<B>	Num.</B>
		</TD>
		<TD align=center>
			<B>Tipo</B>
		</TD>
		<TD align=center>
			<B>LL</B>
		</TD>
		<TD align=center>
			<B>Nome</B>
		</TD>
		<TD align=center>
			<B>Obbligatoriet�</B>
		</TD>
		</TR-->
		
	<%'else 
	'	Response.Write "<CENTER>"
	'	Response.Write "<FONT face='Verdana, Arial, Helvetica, sans-serif' size=2 color='#363a90'>"
	'	Response.Write "Non ci sono Attributi da visualizzare."
	'	Response.Write "</FONT>"
	'	Response.Write "</CENTER>"
	'end if

	  for i = 1 to 10
		'Numero
		Response.Write "<TR class=tblsfondo ALIGN=CENTER>" &_
						"<TD class=tbltext1 align=center width=50>" &_
							"<B>" & i & "</B>" &_
						"</TD>"
		'Tipo
		Response.Write "<TD class=tbltext1 align=center width=150>" &_
							"<SELECT id=cmbTipo name=cmbTipo>"
		select case mid(arrayInputstring(i), 1, 1) 
			case "A"
				Response.Write "<OPTION>"
				Response.Write "<OPTION value=Alfanumerico selected>Alfanumerico" 
				Response.Write "<OPTION value=Numerico>Numerico" 
			    Response.Write "<OPTION value=Data>Data"
				Response.Write "</OPTION>"
			case "N"
				Response.Write "<OPTION>"
				Response.Write "<OPTION value=Alfanumerico>Alfanumerico" 
				Response.Write "<OPTION value=Numerico selected>Numerico" 
			    Response.Write "<OPTION value=Data>Data"
				Response.Write "</OPTION>"
			case "D"
				Response.Write "<OPTION>"
				Response.Write "<OPTION value=Alfanumerico>Alfanumerico" 
				Response.Write "<OPTION value=Numerico>Numerico" 
			    Response.Write "<OPTION value=Data selected>Data"
				Response.Write "</OPTION>"
			case else
				Response.Write "<OPTION>"
				Response.Write "<OPTION value=Alfanumerico>Alfanumerico" 
				Response.Write "<OPTION value=Numerico>Numerico" 
			    Response.Write "<OPTION value=Data>Data"
				Response.Write "</OPTION>"
		end select
		
'*************************************************************************************	
		' fa parte del select case qu� SOPRA
		
'				Response.Write "<OPTION>"
'				Response.Write "<OPTION value=Alfanumerico selected>" &_ 
'									"Alfanumerico" &_
'								"</OPTION>" &_
'								"<OPTION value=Numerico>" &_
'									"Numerico" &_
'								"</OPTION>" &_
'								"<OPTION value=Data>" &_
'									"Data" &_
'								"</OPTION>"
'			case "N"
'				Response.Write "<OPTION>"
'				Response.Write "<OPTION value=Alfanumerico>" &_
'									"Alfanumerico" &_
'								"</OPTION>" &_
'								"<OPTION value=Numerico selected>" &_
'									"Numerico" &_
'								"</OPTION>" &_
'								"<OPTION value=Data>" &_
'									"Data" &_
'								"</OPTION>"
'			case "D"
'				Response.Write "<OPTION>"
'				Response.Write "<OPTION value=Alfanumerico>" &_
'									"Alfanumerico" &_
'								"</OPTION>" &_
'								"<OPTION value=Numerico>" &_ 
'									"Numerico" &_
'								"</OPTION>" &_
'								"<OPTION value=Data selected>" &_ 
'									"Data" &_
'								"</OPTION>"
'			case else
'				Response.Write "<OPTION>"
'				Response.Write  "<OPTION selected> </OPTION>" &_
'								"<OPTION value=Alfanumerico>" &_ 
'									"Alfanumerico" &_
'								"</OPTION>" &_
'								"<OPTION value=Numerico >" &_ 
'									"Numerico" &_
'								"</OPTION>" &_
'								"<OPTION value=Data>" &_ 
'									"Data" &_
'								"</OPTION>"
'		end select
'*****************************************************************************************
		

		
		Response.Write "</SELECT>" 
		Response.Write "</TD>"
		'Lunghezza
		Response.Write "<TD CLASS=tbltext1 align=center width=50>"
		Response.write "<INPUT id=txtLL name=txtLL value='" & mid(arrayInputstring(i), 2, 2) & "' style='HEIGHT: 22px; WIDTH: 23px' maxlength=2>"
		Response.Write "</TD>"
		'Nome
		Response.Write "<TD CLASS=tbltext1 align=center width=125>"
		prova = mid(arrayInputstring(i), 4, 20)
		Response.write "<INPUT id=txtNome style=TEXT-TRANSFORM:uppercase name=txtNome value='" & prova & "' style='HEIGHT: 22px; WIDTH: 74px' maxlength=20>"
		Response.Write "</TD>"
		'Obbligatoriet�
		Response.Write "<TD CLASS=tbltext1 align=center width=125>"
		Response.Write "<SELECT id=cmbObblig name=cmbObblig>"
		select case mid(arrayInputstring(i), 24, 1) 
			case "F"
				Response.Write "<OPTION>"
				Response.Write "<OPTION value=Facoltativo selected>Facoltativo"
				Response.Write "<OPTION value=Obbligatorio>Obbligatorio"
				Response.Write "</OPTION>"
			case "O"
				Response.Write "<OPTION>"
				Response.Write "<OPTION value=Facoltativo>Facoltativo"
				Response.Write "<OPTION value=Obbligatorio selected>Obbligatorio"
				Response.Write "</OPTION>"
			case else
				Response.Write "<OPTION selected>"
				Response.Write "<OPTION value=Facoltativo>Facoltativo"
				Response.Write "<OPTION value=Obbligatorio>Obbligatorio"
				Response.Write "</OPTION>"
		end select
		Response.Write "</SELECT>"
		Response.Write "</TD></TR>"
		
		cursore = cursore + mid(arrayInputstring(i), 2, 2)
		
	next	
%>

</TABLE>
<br>
<table width=500 border=0 cellspacing=2 cellpadding=1>
	<tr>
		<td align=center>
	<!--<P align="middle">-->
	<%
	select case sAction
		case "MOD" 
		 
			if ckProfile(sProfilo,4)=true then
				Response.Write "<IMG class=tbltext1 onclick=""return Modifica('" & _
					sNomeTab & "')"" src='" & session("Progetto") &_
					"/images/conferma.gif' id=cmdModifica >"  'align ='absBottom'
				Response.Write "&nbsp;"
					
				if sScadenza= "" then
					sScadenza = "31/12/9999"
				end if

				if (ConvStringToDate(sScadenza) <= date()) or (ConvStringToDate(sDecorrenza) >= date()) then
				else
					Response.Write "<a href=""javascript:onclick=Delete('" & _
						sNomeTab & "','" & sData & "')"" id=cmdCancella value='Chiudi validit�'>"
					Response.Write "<img src='" & session("Progetto") & "/images/chiudivalidita.gif' border=0 align ='absBottom'></A>"
				end if
			end if
		
		case "INS" 
			 Response.Write"<img src=" & Session("progetto") & "/images/conferma.gif title='Inserisci nuova tabella'  border='0'  id=image1 name=image1 align ='absBottom' onclick=Inserimento()>"
			 'Response.Write "<INPUT type='button' onclick=Inserimento() id=cmdSalva value=Salva>"
	end select
	
	%>
			<A HREF='javascript:onclick=history.back()'><img src="<% session("Progetto") %>/Plavoro/images/indietro.gif" border="0" align ='top'></A>
	
				<!--</p>-->
	</td>
</tr>
<!--tr>
	<td align="middle">
		<B>
		<A class=textred HREF='TAB_VisIdtab.asp'>Torna all'Elenco delle Tabelle</A>
		</B>
	</td>
</tr-->
</table>

<%	
	'Chiudo il RecordSet				
	rsCodici.Close
	set rsCodici = nothing
	
	'Chiudo la connessione al DB
'	CnConn.Close
'	set CnConn = nothing

%>

</FORM>
<script> 
	//document.formDati.txtDecorrenza.focus();
</script>

<!--#include virtual ="/include/closeconn.asp"-->
<!--#include virtual = "/strutt_coda2.asp"-->
