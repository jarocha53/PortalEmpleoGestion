<% cls = Request("close") %>

<script language="JavaScript">
function winclose(){
	
	window.close();
}	
</script>


<html>
<head>
	<title>Condizioni di utilizzo della Chat di ItaliaLavoro</title>
	<link rel="stylesheet" type="text/css" href="<%=Session("Progetto")%>/fogliostile.css">
</head>
<BODY class=sfondocomm> 
<IMG SRC="<%=Session("Progetto")%>/images/logo_italia_lavoro.gif">
<p class=tbltext3 align=center>Condizioni di utilizzo della Chat di ItaliaLavoro</p><br><br>
<B class=tbltext>Qui trovi i termini e le condizioni d'uso del Servizio di Chat che ItaliaLavoro 
ti mette a disposizione.</B><BR><BR><span class=tbltext>Nel partecipare a discussioni nelle 
<B>Chat</B> rooms ospitate da <B><BR>ItaliaLavoro</B>, gli utenti 
<B>NON</B> devono:<BR>
<P></P>
<TABLE border=0 width="100%" class=tbltext>
<TR >
<TD>
      <OL>
        <LI><B>Includere frasi che possono offendere la 
        comunit� di Internet incluse espressioni sfacciate di fanatismo, 
        razzismo, odio o irriverenza</LI></OL></B></TD></TR>
<TR>
<TD>
      <OL>
        <LI><B>Promuovere o fornire informazioni che 
        istruiscano su attivit� illegali, o propongano danni fisici o ingiurie 
        ai danni di qualsiasi gruppo o individuo</LI></OL></B></TD></TR>
<TR>
<TD>
      <OL>
        <LI><B>Includere frasi per promuovere 
attivit� commerciali, incluse ma non limitate alle seguenti:</B><BR></LI></OL></TD></TR>
<TR>
<TD>
      <UL>
        <UL>
        <LI><B>  Offrire in vendita qualsiasi prodotto o 
servizio </B></LI></UL></UL> </TD></TR>
<TR>
<TD>
      <UL>
        <UL>
        <LI><B>  Propagandare o 
sponsorizzare </B></LI></UL></UL></TD></TR>
<TR>
<TD>
      <UL>
        <UL>
        <LI><B> Promuovere o sollecitare qualsiasi 
        partecipazione ad iniziative che abbiano fini esclusivamente 
        commerciali</LI></UL></UL></B></TD></TR>
<TR>
<TD>
      <UL>
        <UL>
        <LI><B> Trattare argomenti di carattere pornografico o 
        erotico</LI></UL></UL></B></TD></TR>
<TR>
<TD>
      <UL>
        <UL>
        <LI><B>  Trattare argomenti che possano essere non adatti a 
ragazzi di et� inferiore ai 18 anni  <BR></LI></UL></UL></B></TD></TR></TABLE>
<P><B>Limitazioni di Responsabilit� e Garanzia<BR></B><BR>L'uso di questi 
servizi e` interamente a proprio rischio. <BR>I servizi sono accessibili senza 
garanzia di alcun genere, sia esplicite che implicite. <BR>Il servizio di chat, 
per sua natura, comporta l'interazione con le diverse persone contemporaneamente 
connesse al servizio. <BR>ItaliaLavoro non pu� svolgere un controllo puntuale 
e continuo relativamente agli argomenti trattati e alla loro pertinenza rispetto 
al tema della stanza, n� pu� efficacemente impedire comportamenti scorretti da 
parte di alcuni utenti collegati. <BR>ItaliaLavoro ricorda che il servizio di 
<B>CHAT</B> potrebbe presentare contenuti non adatti ai minori. <BR>ItaliaLavoro consiglia che i collegamenti effettuati dai minori di et� alla <B>CHAT</B> 
avvengano sempre in presenza dei genitori. <BR>Ogni utente che ha accesso 
alla<B> CHAT</B> di Italialavoro, sottoscrive l'osservazione dei termini e 
condizioni di uso, pertanto accetta la piena responsabilit� individuale dei 
testi inseriti in <B>CHAT</B>.<BR><BR><B>Chiusura del Servizio<BR></B><BR>ItaliaLavoro si riserva il diritto di rimuovere dal proprio sito, senza preavviso, la 
registrazione di utenti insieme al materiale da essi prodotto qualora fossero 
violate le regole e le condizioni d'utilizzo del sito descritte sopra da parte 
dell'utente.<BR><BR><BR></P></span>
<p align=center><a href="javascript:winclose();"><IMG SRC="<%=Session("Progetto")%>/images/chiudi.gif" border=0></a>
</BODY>
</html>
