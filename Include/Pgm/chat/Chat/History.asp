<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #include file="constants.inc" -->

<%
If Session("Login") & "*" = "*" Then
%>		<script language="JavaScript">
			top.location.href = "Expired.asp";
		</script>
<%
End if
Dim ChatID, Nick, ChatRoom, Nuova, imag
	ChatID = Request("ChatID")
	Nuova = Request("Nuova")
	ChatRoom = Request("ChatRoom")
	If len(ChatRoom) = 1 Then
		ChatRoom = "0" & ChatRoom
	End if  

	If ( (ChatID = "") Or (Not Application.StaticObjects.Item("ASPChat").Exists(ChatID)) ) Then
		Response.Redirect "Expired.asp"
		Response.End
	End If
%>
<html>
<head>
	<meta HTTP-EQUIV="Refresh" CONTENT="7; URL=History.asp?ChatID=<%=ChatID%>&ChatRoom=<%=ChatRoom%>&Imag=<%=Imag%>">
	<title>Chat</title>
	<link rel="stylesheet" type="text/css" href="<%=Session("Progetto")%>/fogliostile.css">
</head>

<script language="JavaScript">
function posiziona()
{
	 window.location.href="#a"
}
</script>

<body text="#ccccff" onload="posiziona()" bgcolor=#ffffff> 
<script language="JavaScript">
// hide from challenged browsers
// Call this to refresh the frames
if (parent.frames.length!=0)
{
	RefreshFrame("Chatters", "Chatters.asp?ChatID=<%=ChatID%>&ChatRoom=<%=ChatRoom%>&Imag=<%=imag%>");
}
function RefreshFrame(FrameName, PageName)
{
	eval("parent." + FrameName + ".location='" + PageName + "'");
}

</script>

<table border="0" cellpadding="0" cellspacing="0" width="500" >
<%
on error resume next 

Dim chatName
If (Application.StaticObjects.Item("ASPChat").Exists(CStr(ChatID))) Then
	chatName = Application.StaticObjects.Item("ASPChat").Item(CStr(ChatID))
Else
	chatName = "Guest"
End If
	
' user wants to logoff, so we will have to notify all other users 
' about this by printing some kind of 'user X is now logging off' 
' message.
If ( Len(Request("logoff.x")) > 0) Then
	Dim x
	Application.Lock
	' add a leaving message to chatroom
	For x = 1 To MESSAGES Step + 1
		Application("chatline_" & x) = Application("chatline_" & x+1)
	Next
	if mid(chatName,1,2) = ChatRoom then 
		Application("chatline_" & MESSAGES) = "<tr><td colspan='2' class='Name' align='left' ><span class=textred >  <b>" & ChatName & "</b> esce alle : " & FormatDateTime(Now, 4) & "</span></td></tr>"
	end if
	' remove user
	Application.StaticObjects.Item("ASPChat").Remove(CStr(ChatID))
	Application.StaticObjects.Item("ASPChatTime").Remove(CStr(ChatID))
	Application.StaticObjects.Item("ASPChatIP").Remove(CStr(ChatID))
	Application.StaticObjects.Item("ASPChatIMG").Remove(CStr(ChatID))
	Application.StaticObjects.Item("ASPChatRoom").Remove(CStr(ChatID))
	Application.StaticObjects.Item("ASPChatORA").Remove(CStr(ChatID))
	Application.StaticObjects.Item("ASPChatMIN").Remove(CStr(ChatID))
	'Application("ASPChatMod")= ""
	Application.UnLock
%>		
<script language="javascript">
   	top.location.href = "Default.asp?Uscita=ESCI";
</script>
<%
Else
	' a new message has been send to chat. we want this message to 
	' be added our list of message, indicating which user send it.
	If ( Request("mode") = "message" ) Then
		Dim textMessage
		textMessage = Server.HTMLEncode(Request("message"))
		' do not add message, if it is empty
		If (Len(textMessage) > 0) Then
			'Now check for profanity
			textMessage = CheckProfanity(textMessage)
    		Dim i, text
    		Dim PVar
   		
			Application.Lock
						
			If Request.Form("G") = "off" Then
				textMessage = "<b>" & textMessage & "</b>"
			End If
			If Request.Form("C") = "off" Then
				textMessage = "<i>" & textMessage & "</i>"
			End If
			If Request.Form("S") = "off" Then
				textMessage = "<u>" & textMessage & "</u>"
			End If
			' move all messages one item down in queue

			For i = 1 To MESSAGES Step + 1
				Application("chatline_" & i) = Application("chatline_" & i+1)
			Next
			' build new message
			If mid(chatName,1,2) = ChatRoom Then 
				IF len(textMessage) > 50 Then
					TXT1 = mid(textMessage,1,50)
					TXT2 = mid(textMessage,51)
					IF len(TXT2) > 70 Then
						TXT3 = mid(TXT2,1,70)
						TXT4 = mid(TXT2,71)
						TXT5 = TXT3 & "<br>" & TXT4
					Else
						TXT5 = TXT2
					End if					
				textMessage = TXT1 & "<br>" & TXT5
				End if

				text = "<tr><td align='left' colspan='2' valign='top'         class='tbltext1' id='" & ChatID & "'><b>" & ChatName & " </b> dice alle : " & FormatDateTime(Now, 4) & " : <font color='000000'>" & textMessage & "</font></td></tr>" & vbCrLf  
			End If
			' add message as first message in queue
			Application("chatLine_" & MESSAGES) = text 
			' update users timestamp
			Application.StaticObjects.Item("ASPChatTime").Item(ChatID) = CStr(Now())
    		PVar = Application.StaticObjects.Item("ASPChatTime").Item(ChatID)
			Application.UnLock
			%>
			<script language="JavaScript">
			// hide from challenged browsers
			// Call this to refresh the frames
			if (parent.frames.length!=0)
			{
				RefreshFrame("Message", "Message.asp?ChatID=<%=ChatID%>&ChatRoom=<%=ChatRoom%>");
			}
			function RefreshFrame(FrameName, PageName)
			{
				eval("parent." + FrameName + ".location='" + PageName + "'");
			}
			</script>
			<%
		End If
	End If
End If
	
' Check if Users need to be dropped.
Dim arUserTimes, timeIdx, curTimestamp
arUserTimes  = Application.StaticObjects.Item("ASPChatTime").Keys
curTimestamp = Now()
For timeIdx = 0 To Application.StaticObjects.Item("ASPChatTime").Count-1
	If (DateDiff("s", CDate(Application.StaticObjects.Item("ASPChatTime").Item(arUserTimes(timeIdx))), curTimestamp) > TIMEOUT) Then
'	Drop the user
		Application.Lock
		' Broadcast User has left
		If mid(chatName,1,2) = ChatRoom Then 
		' Occorre indicare la parola [ alle : ] altimenti la chat va in errore .....
			Application("chatline_" & MESSAGES) = "<tr><td colspan=2 class='textred' align='left'><span class='textred'>  <b>" & ChatName & "</b> Timeout alle : " & FormatDateTime(Now, 4) & "</span></td></tr>"
		End If
		Application.StaticObjects.Item("ASPChat").Remove(arUserTimes(timeIdx))
		Application.StaticObjects.Item("ASPChatTime").Remove(arUserTimes(timeIdx))
		Application.StaticObjects.Item("ASPChatRoom").Remove(arUserTimes(timeIdx))
		Application.StaticObjects.Item("ASPChatIP").Remove(arUserTimes(timeIdx))
		Application.StaticObjects.Item("ASPChatIMG").Remove(arUserTimes(timeIdx))
		Application.StaticObjects.Item("ASPChatORA").Remove(arUserTimes(timeIdx))
		Application.StaticObjects.Item("ASPChatMIN").Remove(arUserTimes(timeIdx))
		'Application("ASPChatMod")= ""
		Application.UnLock
	End If
Next

If ( Application.StaticObjects.Item("ASPChat").Count = 0 ) Then
	' clear all messages
	Application.Lock
	For i = 1 To MESSAGES
		Application("chatline_" & i) = ""
	Next
	Application.UnLock
End If


Dim arUserNames, arUserIDs, arUserORA, arUserMIN, n, stt, nk, chn, r, st1, a, b, arUserImage, ChatImage, px, pv
arUserIDs = Application.StaticObjects.Item("ASPChat").Keys
arUserNames = Application.StaticObjects.Item("ASPChat").Items
arUserImage = Application.StaticObjects.Item("ASPChatIMG").Items
arUserORA = Application.StaticObjects.Item("ASPChatORA").Items
arUserMIN = Application.StaticObjects.Item("ASPChatMIN").Items
For i = 1 To MESSAGES
	For n = 0 To Application.StaticObjects.Item("ASPChat").Count-1
	chatName = arUserNames(n)
	chatImage = arUserImage(n)
	chatora = arUserORA(n)
	chatmin = arUserMIN(n)
	
	If (ChatID = arUserIDs(n)) Then
		stt = Application("chatline_" & i )
		If len(Trim(stt)) > 1 Then

		  hh = InStr (1,stt,"alle :")
		  hr = Mid (stt,hh+7,2)
		  mn = mid(stt,hh+10,2)	

		  If cint(hr) >= cint(chatora) and cint(mn) >= cint(chatmin) Then

			If mid(stt,80,9) = "Messaggio" Then
				pv = instr(1,stt," da ")
				chn = Replace(mid(stt,pv+3,11)," ","")
				px = instr(1,stt,"per ")
				nk = Replace(mid(stt,px+3,11)," ","")
			
				If nk = mid(chatName,3) or chn = mid(chatName,3) Then
					Response.Write stt
				End If
			Else
				If mid(stt,90,2) = ChatRoom Then ' dice
					a = mid(stt,1,89)
					b = mid(stt,92)
					stt = a & b
					Response.Write stt 
					
				Else
					If mid(stt,89,2) = ChatRoom Then ' dice
						a = mid(stt,1,88)
						b = mid(stt,91)
						stt = a & b
						Response.Write stt 
					Else
						If mid(stt,74,2) = ChatRoom Then
							a = mid(stt,1,73)
							b = mid(stt,76)
							stt = a & b
							Response.Write stt 	 		
						Else
							If mid(stt,60,2) = ChatRoom Then 'entra
								a = mid(stt,1,59)
								b = mid(stt,62)
								stt = a & b
								Response.Write stt 	 
							Else
								If mid(stt,61,2) = ChatRoom Then 'entra
									a = mid(stt,1,60)
									b = mid(stt,63)
									stt = a & b
									Response.Write stt 	 
								Else
									If mid(stt,62,2) = ChatRoom Then 'entra
										a = mid(stt,1,61)
										b = mid(stt,64)
										stt = a & b
										Response.Write stt 	 
									Else
										If mid(stt,75,2) = ChatRoom Then 'esce
											a = mid(stt,1,74)
											b = mid(stt,77)
											stt = a & b
											Response.Write stt 							
										Else
											If mid(stt,76,2) = ChatRoom Then 'esce
												a = mid(stt,1,75)
												b = mid(stt,78)
												stt = a & b
												Response.Write stt 							
											Else 
												'Response.Write stt
											End if
										End if
									End if
								End if
							End if
						End if
					End if
				End if
			End if


			End if
			End if
		End if
	Next
Next
%>
</table><font color=ffffff><a name="a">_</a></font>
</body>
</html>
<%
Function CheckProfanity( Text )
	Dim arrSwear, x, n
	'Get the Arrays
	arrSwear = Application("SwearWords")
	Text = Split(Text, " ")
	For x  = LBound(Text) To UBound(Text)
		'Check if it is profane
		For n = LBound(arrSwear) To UBound(arrSwear)
			If InStr(UCase(Text(x)), arrSwear(n)) > 0 Then
				Text(x) = "[Censurata]"
				Exit For
			End If
		Next
	Next
	CheckProfanity = Join(Text, " ")
End Function
%>

