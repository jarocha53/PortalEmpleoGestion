<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #INCLUDE FILE="../Utils.asp" -->
<TITLE>DETALLE COMPETENCIA</TITLE>
<LINK REL=STYLESHEET TYPE="text/css" HREF='<%=session("progetto")%>/fogliostile.css'>
<body class=sfondocentro topmargin="10" leftmargin="0">
<%


Set Conn = cc
'Conn.open strConn

	Sql = ""
	Sql = Sql & "SELECT "
	Sql = Sql & "COMPETENZE.OUTPUT, COMPETENZE.ATTIVITA, COMPETENZE.DENOMINAZIONE, "
	Sql = Sql & "COMPETENZE.NOTA, VERBI.DENOMINAZIONE Verbo "
	Sql = Sql & "FROM "
	Sql = Sql & "COMPETENZE, VERBI "
	Sql = Sql & "WHERE "
	Sql = Sql & "COMPETENZE.ID_VERBO = VERBI.ID_VERBO "
	Sql = Sql & "AND "
	Sql = Sql & "COMPETENZE.ID_COMPETENZA = " & request("ID_COMPETENZA")
	Sql = Sql & "ORDER BY UPPER(COMPETENZE.DENOMINAZIONE)"

'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
	Set Rs = Conn.Execute (Sql)
	if not rs.eof then COMPET = Rs.getrows()

Sql = ""
	Sql = Sql & "SELECT "
	Sql = Sql & "CONOSCENZE.DENOMINAZIONE, CONOSCENZE.ID_CONOSCENZA, "
	Sql = Sql & "VALIDAZIONE.ID_VALID, VALIDAZIONE.FL_VALID, VALIDAZIONE.ID_PERS_INS, COMPET_CONOSC.GRADO_COMP_CON "
	Sql = Sql & "FROM "
	Sql = Sql & "CONOSCENZE, COMPET_CONOSC, VALIDAZIONE "
	Sql = Sql & "WHERE "
	Sql = Sql & "COMPET_CONOSC.ID_VALID = VALIDAZIONE.ID_VALID "
	Sql = Sql & "AND "
	Sql = Sql & "COMPET_CONOSC.ID_CONOSCENZA = CONOSCENZE.ID_CONOSCENZA "
	Sql = Sql & "AND "
	Sql = Sql & "COMPET_CONOSC.ID_COMPETENZA = " & request("ID_COMPETENZA")
	Sql = Sql & "ORDER BY UPPER(CONOSCENZE.DENOMINAZIONE)"

'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
	Set Rs = Conn.Execute (Sql)
	if not rs.eof then CONOSC = Rs.getrows()

	Sql = ""
	Sql = Sql & "SELECT "
	Sql = Sql & "CAPACITA.DENOMINAZIONE, CAPACITA.ID_CAPACITA, "
	Sql = Sql & "VALIDAZIONE.ID_VALID, VALIDAZIONE.FL_VALID, VALIDAZIONE.ID_PERS_INS, COMPET_CAPAC.GRADO_COMP_CAP "
	Sql = Sql & "FROM "
	Sql = Sql & "CAPACITA, COMPET_CAPAC, VALIDAZIONE "
	Sql = Sql & "WHERE "
	Sql = Sql & "COMPET_CAPAC.ID_VALID = VALIDAZIONE.ID_VALID "
	Sql = Sql & "AND "
	Sql = Sql & "COMPET_CAPAC.ID_CAPACITA = CAPACITA.ID_CAPACITA "
	Sql = Sql & "AND "
	Sql = Sql & "COMPET_CAPAC.ID_COMPETENZA = " & request("ID_COMPETENZA")
	Sql = Sql & "ORDER BY UPPER(CAPACITA.DENOMINAZIONE)"

'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
	Set Rs = Conn.Execute (Sql)
	if not rs.eof then CAPAC = Rs.getrows()

Sql =""
	Sql = Sql &  "SELECT "
	Sql = Sql &  "   FIG.DENOMINAZIONE PROFILO, FIG.IND_MAPPA, COM.DENOMINAZIONE COMPETENZA,  "
	Sql = Sql &  "   COM_FP.GRADO_FP_COMP, VAL.FL_VALID"
	Sql = Sql &  " FROM "
	Sql = Sql &  "   COMPET_FP COM_FP, "
	Sql = Sql &  "   COMPETENZE COM, "
	Sql = Sql &  "   FIGUREPROFESSIONALI FIG, "
	Sql = Sql &  "   VALIDAZIONE VAL"
	Sql = Sql &  " WHERE "
	Sql = Sql &  " ( COM_FP.ID_VALID=VAL.ID_VALID ) AND "
	Sql = Sql &  "   ( COM.ID_COMPETENZA=COM_FP.ID_COMPETENZA ) AND "
	Sql = Sql &  "   ( FIG.ID_FIGPROF=COM_FP.ID_FIGPROF ) and"
	Sql = Sql &  "   COM_FP.ID_COMPETENZA =" & request("ID_COMPETENZA") & " and "
	Sql = Sql &  "   VAL.FL_VALID=1"
	Sql = Sql & "ORDER BY UPPER(FIG.DENOMINAZIONE )"

'Response.Write SQL
'Response.End

'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
	Set Rs = Conn.Execute (Sql)
	if not rs.eof then FIGPROF = Rs.getrows()


set Rs = Nothing
Conn.Close
Set conn = Nothing

%>

<SCRIPT LANGUAGE="JavaScript">
function stp(){
bb=(document.layers)?document.layers['aa']:(document.getElementById)?document.getElementById('aa').style:document.all['aa'].style;
bb.visibility="hidden"
self.print();
bb.visibility="visible"
}
</SCRIPT>


<CENTER>
<table border=0 cellspacing=0 cellpadding=0 width='500'>
	<tr CLASS=tbltext0>
		<td bgcolor='#3399CC' align=left width='350'>
		<b><B>DETALLE COMPETENCIA</b>
		</td>
		<td width="25" valign='bottom' background="<%=session("progetto")%>/images/sfondo_linguetta.gif" >
		<img border="0" src="<%=session("progetto")%>/images/tondo_linguetta.gif"></td>
		<td width="278" valign='bottom' background="<%=session("progetto")%>/images/sfondo_linguetta.gif" >&nbsp;</td>
	</tr>
	<tr class="sfondocomm">
		<td align="left" colspan="3">
		Se define competencia como �El ser capaz de integrar los <B>Conocimientos</B>, el 
		<B>Desempe�o</B>, y <B>valores/Comportamientos</B> qu� permiten realizar una actividad 
		requerida en una situaci�n especifica�. Durante an�lisis de un proceso productivo, 
		la <B>Competencia</B> califica al operador qu� es capaz de realizar la salida de 
		una actividad principal/intermedia (una salida, por lo tanto, que incluso siendo 
		intermedia para el <B>Proceso</B>, tiene una propia completitud, una propia autonom�a,
		un propio valor mensurable en t�rminos econ�micos). 		
		</td>
	</tr>
	<tr>
		<td colspan=3 bgcolor='#3399CC'></td>
	</tr>
	<tr>
		<td colspan="3" align="center"><div id="aa">
		<input type='button' value='Cerrar' OnClick='top.close()' CLASS='My' id='button'1 name='button'1>
		<input type='button' value='Imprimir' OnClick='stp()' CLASS='My' id='button'1 name='button'1>
		</div></td>
	</tr>
</table>

<table border=0 width=500 cellspacing=1 cellpadding=1>
	<tr>
		<td valign=top align='left' width='20%' CLASS=tbltext1><B>Actividad&nbsp;</b></td>
		<td CLASS=tbltext1><%= COMPET(1,0)%></td>
	</tr>
	<tr>
		<td valign=top align='left' width='20%' CLASS=tbltext1><B>Salida&nbsp;</b></td>
		<td CLASS=tbltext1><%= COMPET(0,0)%></td>
	</tr>
	<tr>
		<td valign=top align='left' width='20%' CLASS=tbltext1><B>Palabra&nbsp;</b></td>
		<td CLASS=tbltext1><%= COMPET(4,0)%></td>
	</tr>
	<tr>
		<td valign=top align='left' width='20%' CLASS=tbltext1><B>Denominaci�n&nbsp;</b></td>
		<td CLASS=tbltext1><%= COMPET(2,0)%></td>
	</tr>
	<tr>
		<td valign=top align='left' width='20%' CLASS=tbltext1><b>Descripci�n&nbsp;</b></td>
		<td CLASS=tbltext1><%= COMPET(3,0)%></td>
	</tr>
</table>

<br>
<%
if IsArray(CONOSC) then
	Response.Write "<table border=0 cellspacing=1 cellpadding=1 width=500>"
	Response.Write "<tr class=sfondocomm>"
		Response.Write "<td><B>Conocimientos Asociados</b></font></td>"
		Response.Write "<td align=center><b>Niv. Esperado</b></font></td>"
	Response.Write "</tr>"
		for I = lbound(CONOSC,2) to Ubound(CONOSC,2)
			Response.Write "<tr>"
				Response.Write "<td CLASS=tbltext1>" & CONOSC(0,I) & "</td>"
				Response.Write "<td align='center' widht='15%' CLASS=tbltext1><b>" & CONOSC(5,I) & "</b></td>"
			Response.Write "</tr>"
		next
		Response.Write "</table>"
	end if
%>
<br>
<%
	if IsArray(CAPAC) then
	Response.Write "<table border=0 cellspacing=1 cellpadding=1 width=500>"
	Response.Write "<tr class=sfondocomm>"
		Response.Write "<td><B>Desempe�o Asociado</b></td>"
		Response.Write "<td align='center'><b>Niv. Esperado</b></td>"
	Response.Write "</tr>"
		for I = lbound(CAPAC,2) to Ubound(CAPAC,2)
			Response.Write "<tr>"
				Response.Write "<td class=tbltext1>" & CAPAC(0,I) & "</td>"
				Response.Write "<td align='center' widht='15%' class=tbltext1><b>" & CAPAC(5,I) & "</b></td>"
			Response.Write "</tr>"
		next
		Response.Write "</table>"
	end if
%>
	<br>
<%
	if IsArray(FIGPROF) then
	Response.Write "<table border=0 width='500' cellspacing=1 CELLPADDING=1>"
	Response.Write "<tr class=sfondocomm>"
	Response.Write "<td ><b>Figuras Profesionales</b></td>"
	Response.Write "<td align='center' widht='15%'><b>Niv. Esperado</b></td>"
	Response.Write "</tr>"

		for I = lbound(FIGPROF,2) to Ubound(FIGPROF,2)
			Response.Write "<tr>"
				Response.Write "<td class=tbltext1>" & FIGPROF(0,I) & "</td>"
				Response.Write "<td class=tbltext1 align='center' widht='20%'><b>" & FIGPROF(3,I) & "</b></td>"
			Response.Write "</tr>"
		next
	Response.Write "</table>"
	end if
%>
<br>

<table WIDTH='500' CELLPADDING=1 cellspacing=1>
<td align='center'>
<form name='indietro'><div id="aa">
<input type='button' value='Cerrar' OnClick='top.close()' CLASS='My'>
<input type='button' value='Imprimir' OnClick='stp()' CLASS='My'></div>
</form>
</td>
</tr>
		<tr>
			<td colspan=3 bgcolor='#3399CC'></td>
		</tr>
</table>
</CENTER>
