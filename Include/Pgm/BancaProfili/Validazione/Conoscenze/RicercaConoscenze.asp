<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #include Virtual="/strutt_testa2.asp" -->
<!-- #INCLUDE FILE="../Utils.asp" -->

<script language='Javascript'>
	<!--
		function tit()
			{
				obj=document.CercaFP;
				testo = obj.ID_AREA.options[obj.ID_AREA.selectedIndex];
				obj.TitoloArea.value = testo.title
			}

		function chkfrm(frm)
		{
				if ((frm.testo.value=="")  && (frm.ID_AREA.options[frm.ID_AREA.selectedIndex].value==""))
				{
					alert("Ingresar al menos un criterio de busqueda");
					return false;
				}
		}

	//-->
	</script>

<CENTER>
<table border=0 cellspacing=0 cellpadding=0 width='500'>
	<tr>
		<td bgcolor='#3399CC' align=left class='tbltext0' width='199'>
			<b>CONOCIMIENTO</b>
		</td>
		<td width="25" valign='bottom' background="<%=session("progetto")%>/images/sfondo_linguetta.gif" >
			<img border="0" src="<%=session("progetto")%>/images/tondo_linguetta.gif"></td>
		<td width="278" valign='MIDDLE' align="right" class=tbltext1 background="<%=session("progetto")%>/images/sfondo_linguetta.gif" >(*) Campo obligatorio</td>
	</tr>
</table>
<table border=0 cellspacing=0 cellpadding=0 width='500'>			
	<tr class='sfondocomm'>
		<td align="left" colspan="3">
			Es uno de los elementos de la <b>Competencia</b>. 
			Denota preponderantemente la adquisici�n y memorizaci�n de un contenido 
			cognoscitivo (hechos, reglas, normas, conceptos, teor�as, etc.); 
			es un dominio mental, formal, de por s� abstracto de la operatividad.
			Se relaciona con el saber, y tambi�n con el saber como hacer (habilidad), no al hacer, ni al ser.
		</td>
		<td class=sfondocomm>
			<a href="Javascript:Show_Help('<%=Session("Progetto")%>/HelpBP/validazione/Conoscenze/RicercaConoscenze.htm')">
			<img src="<%=Session("Progetto")%>/images/Help.gif" border='0'  align=right alt='Help'></a>
		</td>
	</tr>
	<tr>
		<td colspan=4 bgcolor='#3399CC'></td>
	</tr>
</table>

<form name='CercaFP' Action='ResultConoscenze.asp' Method='POST' OnSubmit="return chkfrm(this)">
<input type="hidden" name="IdPers" value="<%=sIdPers%>">
<input type="hidden" name="IND_FASE" value="<%=IND_FASE%>">

<table border=0 width='500' cellspacing=1 cellpadding=1>
	<tr>
		<td align=left class='tbltext1'><b>Texto de Busqueda*&nbsp;</b></td>
		<td><input type='text' size=58 maxlength=60 name='testo' value='' class='MyTextBox'></td>
	</tr>
	<tr>
		<td align=left class='tbltext1'><b>Area de Conocimiento&nbsp;</b></td>
	<td>
		<%
			Sql = "SELECT ID_AREACONOSCENZA, DENOMINAZIONE FROM AREA_CONOSCENZA ORDER BY UPPER(DENOMINAZIONE)"
			set Conn = CC
			'set Conn = server.CreateObject ("ADODB.Connection")
			'Conn.Open strConn
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
			Set Rs = Conn.Execute (Sql)
			if not Rs.eof then AREE = Rs.getrows
			Set Rs = nothing
			Set Conn = Nothing
			if isArray(AREE) then
				Response.Write "<select size ='1' name='ID_AREA' OnChange='tit()' class='MY'>"
				Response.Write "<option value=''></option>"
					for I = lbound(AREE,2) to Ubound(AREE,2)
						Response.Write "<option value='" & AREE(0,I) & "' title='" & AREE(1,I) & "'>" & AREE(1,I) & "</option>"
					next
				Response.Write "</select>"
			end if
			Response.Write "<input type='hidden' name='TitoloArea' value=''>"
		%>
		</td>
	</tr>
	<tr>
		<td align=left class='tbltext1'><b>Solo elementos no validados?&nbsp;</B></td>
		<td class='tbltext1'>
			<input type='radio' name='Validati' value='1'> Si &nbsp;&nbsp;&nbsp;
			<input type='radio' name='Validati' value='0'checked> No
		</td>
	</tr>
	<tr>
		<td colspan=2 align=right>
			<input type='Submit' value='Buscar' CLASS='My'>
		</td>
	</tr>
	<tr>
		<td colspan=3 bgcolor='#3399CC'></td>
	</tr>
</table>
</form>
<!-- #include Virtual="/strutt_coda2.asp" -->
