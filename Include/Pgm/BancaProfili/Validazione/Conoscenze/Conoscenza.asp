<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #INCLUDE FILE="../Utils.asp" -->
<title>Nuevo Conocimiento</title>
<LINK REL=STYLESHEET TYPE="text/css" HREF='<%=session("progetto")%>/fogliostile.css'>
<body class=sfondocentro topmargin="10" leftmargin="0">
<script language='Javascript'>
<!--
	function valida(Obj)
	{
		if (Obj.Denominazione.value == "" || Obj.ID_AREACONOSCENZA.options[Obj.ID_AREACONOSCENZA.selectedIndex].value == "")
		{
			alert("El campo Denominaci�n del Area de Conocimiento es obligatorio")
			return false
		}
	}
//-->
</script>
<table border=0 cellspacing=1 cellpadding=0 width='500'>
<tr>
<td align=center>
<form name='Conoscenze' Action='SalvaConoscenza.asp' Method='POST' OnSubmit='return valida(this)'>
<input type="hidden" name="IdPers" value="<%=sIdPers%>">
<input type="hidden" name="IND_FASE" value="<%=IND_FASE%>">
<input type='hidden' name='ID_COMPETENZA' value='<%=request("ID_COMPETENZA")%>'>
<input type='hidden' name='DEN_COMPETENZA' value='<%=request("DENOMINAZIONE")%>'>
<input type='hidden' name='VISMENU' value='NO'>
<center>


<table border=0 width='500' CELLPADDING=0 cellspacing=0>
	<tr class="tbltext0">
		<td bgcolor='#3399CC'class=tbltext0 align=left >
		<b>DETALLE DEL CONOCIMIENTO</b>
		</td>
		<td width="25" valign='bottom' background="<%=session("progetto")%>/images/sfondo_linguetta.gif" >
		<img border="0" src="<%=session("progetto")%>/images/tondo_linguetta.gif"></td>
		<td width="278" valign='MIDDLE' align=right class=tbltext1 background="<%=session("progetto")%>/images/sfondo_linguetta.gif" >&nbsp;</td>
	</tr>
</table>
<table border=0 width='500' CELLPADDING=0 cellspacing=0>
	<tr class="sfondocomm">
		<td>
			Es uno de los elementos de la <b>Competencia</b>. Denota principalmente la eventual adquisici�n y
			memorizaci�n de un contenido cognitivo (hechos, reglas, normas, conceptos, teor�as, etc.);
			y un dominio mental para abstraerse de la operatividad.
			Se relaciona con el saber � y despu�s tambi�n al saber como hacer (habilidad)�
			no al hacer, ni al ser.
		</td>
	   	<td class=sfondocomm>
	   	<a href="Javascript:Show_Help('<%=Session("Progetto")%>/HelpBP/Placement/Competenze/Conoscenza.htm')">
		<img src="<%=Session("Progetto")%>/images/Help.gif" border='0'  align=right alt='Help'></a>
		</td>
    </tr> 
    <tr>
		<td colspan=2 bgcolor='#3399CC'></td>
    </tr>
</table>

		<table border=0 width='500' cellspacing=1 cellpadding=1>
			<tr>
			<td class="tbltext1" align=left><b>Denominaci�n&nbsp;</b></td>
			<td bgcolor='#ffffff'><input type='text' class='MyTextBox' maxlength=60 size=60 name='Denominazione' value=''></td>
		</tr>
		<tr>
			<td class="tbltext1" align=left><b>Area del Conocimiento&nbsp;</b></td>
			<td bgcolor='#ffffff'>
			<%
			Sql = "SELECT ID_AREACONOSCENZA, DENOMINAZIONE FROM AREA_CONOSCENZA ORDER BY UPPER(DENOMINAZIONE)"
			set Conn = server.CreateObject ("ADODB.Connection")
			Conn.Open strConn
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
			Set Rs = Conn.Execute (Sql)
			if not Rs.eof then AREE = Rs.getrows
			Set Rs = nothing
			Set Conn = Nothing
			if isArray(AREE) then
				Response.Write "<select class='MY' name='ID_AREACONOSCENZA'>"
				Response.Write "<option value=''></option>"
					for I = lbound(AREE,2) to Ubound(AREE,2)
						Response.Write "<option value='" & AREE(0,I) & "'>" & AREE(1,I) & "</option>"
					next
				Response.Write "</select>"
			end if
			%>
			</td>
		</tr>
		<tr>
			<td class="tbltext1" align=left><b>Grado de Posesi�n&nbsp;</b></td>
			<td bgcolor='#ffffff'>
			<select class='MY' name='Grado'>
				<option value='1'>1</option>
				<option value='2' selected>2</option>
				<option value='3'>3</option>
				<option value='4'>4</option>
			</select>
			</td>
		</tr>
		<tr>
			<td bgcolor='#ffffff' colspan=2 align=right>
				<input type='Submit' value='Guardar' CLASS='My'>
			</td>
		</tr>
	<tr>
		<td colspan=3 bgcolor='#3399CC'></td>
	</tr>
	</table>
</form>
</td>
</tr>
	
</table>
