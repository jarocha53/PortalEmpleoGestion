<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #include Virtual="/strutt_testa2.asp" -->
<!-- #INCLUDE FILE="../Utils.asp" -->
<%
Set Conn = server.CreateObject ("ADODB.Connection")
Conn.open strConn

Sql = ""
Sql = Sql & "SELECT "
Sql = Sql & "CONOSCENZE.DENOMINAZIONE, VALIDAZIONE.ID_PERS_INS, VALIDAZIONE.DT_TMST, "
Sql = Sql & "PERSONA.COGNOME, PERSONA.NOME, VALIDAZIONE.ID_VALID, COMPET_CONOSC.GRADO_COMP_CON, "
Sql = Sql & "AREA_CONOSCENZA.DENOMINAZIONE AreaCon "
Sql = Sql & "FROM "
Sql = Sql & "CONOSCENZE, COMPET_CONOSC, VALIDAZIONE, PERSONA, AREA_CONOSCENZA "
Sql = Sql & "WHERE "
Sql = Sql & "CONOSCENZE.ID_AREACONOSCENZA = AREA_CONOSCENZA.ID_AREACONOSCENZA "
Sql = Sql & "AND "
Sql = Sql & "CONOSCENZE.ID_CONOSCENZA = COMPET_CONOSC.ID_CONOSCENZA "
Sql = Sql & "AND "
Sql = Sql & "VALIDAZIONE.ID_VALID = COMPET_CONOSC.ID_VALID "
Sql = Sql & "AND "
Sql = Sql & "PERSONA.ID_PERSONA (+) = VALIDAZIONE.ID_PERS_INS "
Sql = Sql & "AND "
Sql = Sql & "COMPET_CONOSC.ID_COMPETENZA = " & request("ID_COMPETENZA")
Sql = Sql & "AND "
Sql = Sql & "COMPET_CONOSC.ID_CONOSCENZA = " & request("ID_CONOSCENZA")

'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
Set Rs = Conn.Execute (Sql)
if not rs.eof then CONOSC = Rs.getrows()

Conn.Close
Set conn = Nothing

%>

<script language='Javascript'>
	function regola()
	{
		URL = "RegolaGradoConosc.asp?"
		URL = URL + 'ID_COMPETENZA=<%=request("ID_COMPETENZA")%>&'
		URL = URL + 'DENOMINAZIONE=<%=request("DENOMINAZIONE")%>&'
		URL = URL + 'ID_VALID=<%=CONOSC(5,0)%>&'
		URL = URL + 'ID_CONOSCENZA=<%=request("ID_CONOSCENZA")%>' + "&VISMENU=NO"

		opt = "address=no,status=no,width=600,height=150,top=150,left=35"

		window.open (URL,"",opt)
	}
</script>

<form name='DettagliCompetenza' Method=POST Action='SalvaDettagliConoscenza.asp'>
<input type="hidden" name="IdPers" value="<%=sIdPers%>">
<input type="hidden" name="IND_FASE" value="<%=IND_FASE%>">
<input type="hidden" name="VISMENU" value="NO">


<table border=0 width='500' CELLPADDING=0 cellspacing=0>
	<tr>
		<td bgcolor='#3399CC' class="tbltext0" align=left width='500'>
		<b>COMPETENCIA: <%=ucase(request("DENOMINAZIONE"))%></b>
		</td>
		<td width="25" valign='bottom' background="<%=session("progetto")%>/images/sfondo_linguetta.gif" >
		<img border="0" src="<%=session("progetto")%>/images/tondo_linguetta.gif"></td>
		<td width="278" valign='bottom' background="<%=session("progetto")%>/images/sfondo_linguetta.gif" ></td>
	</tr>
<table>
<table border=0 width='500' CELLPADDING=0 cellspacing=0>
	<tr class='sfondocomm'>
		<td align="left"  colspan="3" >
			Esta pagina permite validar la asociación entre el <b>Conocimiento</b> y la <b>Competencia</b> 
			cuya denominación se muestra aqui.    
		</td>
		<td class=sfondocomm>
			<a href="Javascript:Show_Help('<%=Session("Progetto")%>/HelpBP/validazione/Competenze/DettagliConoscenza.htm')">
			<img src="<%=Session("Progetto")%>/images/Help.gif" border='0'  align=right alt='Help'></a>
		</td>
	</tr>
	<tr>
		<td colspan=4 bgcolor='#3399CC'></td>
	</tr>
</table>
<br>
<%
	if IsArray(CONOSC) then
		Response.Write "<input type='hidden' name='ID_COMPETENZA' value='" & request("ID_COMPETENZA") & "'>"
		Response.Write "<input type='hidden' name='DENOMINAZIONE' value='" & request("DENOMINAZIONE") & "'>"
		Response.Write "<input type='hidden' name='ID_CONOSCENZA' value='" & request("ID_CONOSCENZA") & "'>"
		Response.Write "<input type='hidden' name='ID_VALID' value='" & CONOSC(5,0) & "'>"
		
		Response.Write "<table border=0 width='500' cellspacing=1 cellpadding=1>"
				Response.Write "<tr class=sfondocomm >"
				Response.Write "<td colspan='2'>"
				Response.Write "<b>Detalle conocimiento</B>"
				Response.Write "</td>"
				Response.Write "</tr>"
				Response.Write "<tr>"
					Response.Write "<td align=left class=tbltext1><b>Competencia</b></td>"
					Response.Write "<td class=tbltext1>" & request("DENOMINAZIONE") & "</td>"
				Response.Write "</tr>"
				Response.Write "<tr>"
						Response.Write "<td align=left class=tbltext1><b>Conocimiento</b></td>"
						Response.Write "<td class=tbltext1>" & CONOSC(0,0) & "</td>"
					Response.Write "</tr>"
					Response.Write "<tr>"
						Response.Write "<td align=left class=tbltext1><b>Area Conocimiento</b></td>"
						Response.Write "<td class=tbltext1>" & CONOSC(7,0) & "</td>"
					Response.Write "</tr>"
					Response.Write "<tr>"
						Response.Write "<td align=left class=tbltext1><b>Grado de posesión</b></td>"
						Response.Write "<td class=tbltext1>" & CONOSC(6,0) & " &nbsp;&nbsp;&nbsp;"
						Response.Write "<input type='button' value='Modificar' OnClick='regola()' CLASS='My'>"
						Response.Write "</td>"
					Response.Write "</tr>"
					Response.Write "<tr>"
						Response.Write "<td align=left class=tbltext1><b>Asociación efectuada el</b></td>"
						Response.Write "<td class=tbltext1>" & CONOSC(3,0) & " " & CONOSC(4,0) & "</td>"
					Response.Write "</tr>"
					Response.Write "<tr>"
						Response.Write "<td align=left class=tbltext1><b>Ultima Operación <br>efectuada el</b></td>"
						Response.Write "<td class=tbltext1>" & CONOSC(2,0) & "</td>"
					Response.Write "</tr>"
					Response.Write "<tr>"
						Response.Write "<td align='left'>"
							Response.Write "<input type='button' value='Cerrar' OnClick='goToPage(""RiepilogoCompet.asp?ID_COMPETENZA=" & request("ID_COMPETENZA") & "&DENOMINAZIONE=" & request("DENOMINAZIONE") & """)' CLASS='My'>&nbsp;&nbsp;&nbsp;</td>"
							Response.Write "<td align='right'><input type='submit' name='Valida' value='Validar' CLASS='My'>&nbsp;&nbsp;&nbsp;"
							Response.Write "<input type='submit' name='Elimina' value='Eliminar' CLASS='My'>"
					Response.Write "</td>"
				Response.Write "</tr>"
				Response.Write "<tr>"
		Response.Write "<td colspan=3 bgcolor='#3399CC'></td>"
	Response.Write "</tr>"
		Response.Write "</table>"
		
	end if
%>
</form>
<!-- #include Virtual="/strutt_coda2.asp" -->
