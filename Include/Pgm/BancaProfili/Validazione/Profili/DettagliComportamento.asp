<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #include Virtual="/strutt_testa2.asp" -->
<!-- #INCLUDE FILE="../Utils.asp" -->
<%
Set Conn = server.CreateObject ("ADODB.Connection")
Conn.open strConn

Sql = ""
Sql = Sql & "SELECT "
Sql = Sql & "COMPORTAMENTI.DENOMINAZIONE, VALIDAZIONE.ID_PERS_INS, VALIDAZIONE.DT_TMST, "
Sql = Sql & "PERSONA.COGNOME, PERSONA.NOME, VALIDAZIONE.ID_VALID, "
Sql = Sql & "AREA_COMPORTAMENTI.DENOMINAZIONE areaComp, COMPOR_FP.GRADO_FP_COMPOR "
Sql = Sql & "FROM "
Sql = Sql & "COMPORTAMENTI, COMPOR_FP, VALIDAZIONE, PERSONA, AREA_COMPORTAMENTI "
Sql = Sql & "WHERE "
Sql = Sql & "COMPORTAMENTI.ID_AREACOMPORTAMENTO = AREA_COMPORTAMENTI.ID_AREACOMPORTAMENTO "
Sql = Sql & "AND "
Sql = Sql & "COMPORTAMENTI.ID_COMPORTAMENTO = COMPOR_FP.ID_COMPORTAMENTO "
Sql = Sql & "AND "
Sql = Sql & "VALIDAZIONE.ID_VALID = COMPOR_FP.ID_VALID "
Sql = Sql & "AND "
Sql = Sql & "PERSONA.ID_PERSONA (+) = VALIDAZIONE.ID_PERS_INS "
Sql = Sql & "AND "
Sql = Sql & "COMPOR_FP.ID_FIGPROF = " & request("ID_FIGPROF")
Sql = Sql & "AND "
Sql = Sql & "COMPOR_FP.ID_COMPORTAMENTO = " & request("ID_COMPORTAMENTO")

'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
Set Rs = Conn.Execute (Sql)
if not rs.eof then COMPORT = Rs.getrows()

Conn.Close
Set conn = Nothing

%>

<script language='Javascript'>
	function regola()
	{
		URL = "RegolaGradoCompor.asp?"
		URL = URL + 'ID_FIGPROF=<%=request("ID_FIGPROF")%>&'
		URL = URL + 'DENOMINAZIONE=<%=request("DENOMINAZIONE")%>&'
		URL = URL + 'ID_VALID=<%=COMPORT(5,0)%>&'
		URL = URL + 'ID_COMPORTAMENTO=<%=request("ID_COMPORTAMENTO")%>' + "&VISMENU=NO"

		opt = "address=no,status=no,width=600,height=150,top=150,left=35"

		window.open (URL,"",opt)
	}
</script>

<form name='DettagliComport' Method=POST Action='SalvaDettagliComportamento.asp'>
<input type="hidden" name="IdPers" value="<%=sIdPers%>">
<input type="hidden" name="IND_FASE" value="<%=IND_FASE%>">
<input type="hidden" name="VISMENU" value="NO">

<table border=0 width='500' CELLPADDING=0 cellspacing=0>
	<tr class="tbltext0">
		<td bgcolor='#3399CC'class=tbltext0 align=left width='199'>
			<B>DETTAGLIO COMPORTAMENTO</b>
		</td>
		<td width="25" valign='bottom' background="<%=session("progetto")%>/images/sfondo_linguetta.gif" >
			<img border="0" src="<%=session("progetto")%>/images/tondo_linguetta.gif"></td>
		<td width="278" valign='bottom' background="<%=session("progetto")%>/images/sfondo_linguetta.gif" ></td>
	</tr>
</table>
<table border=0 width='500' CELLPADDING=0 cellspacing=0>

	 <tr class="sfondocomm">
		<td>In questa pagina potrai visualizzare il dettaglio del <b>Comportamento</b> associato
		 al <b>Profilo</b> selezionato.</td>
	    <td>
		   	<a href="Javascript:Show_Help('<%=Session("Progetto")%>/HelpBP/Validazione/Profili/DettagliComportamento.htm')">
			<img src="<%=Session("Progetto")%>/images/Help.gif" border='0'  align=right alt='Help'></a>
		</td>
	</tr>
    <tr>
		<td colspan=3 bgcolor='#3399CC'></td>
    </tr>
</table>

<br>
<%
	if IsArray(COMPORT) then
		Response.Write "<input type='hidden' name='ID_FIGPROF' value='" & request("ID_FIGPROF") & "'>"
		Response.Write "<input type='hidden' name='DENOMINAZIONE' value='" & request("DENOMINAZIONE") & "'>"
		Response.Write "<input type='hidden' name='ID_COMPORTAMENTO' value='" & request("ID_COMPORTAMENTO") & "'>"
		Response.Write "<input type='hidden' name='ID_VALID' value='" & COMPORT(5,0) & "'>"
		Response.Write "<table border=0 width='500' cellspacing=1 cellpadding=1 >"
		Response.Write "<tr class='sfondocomm'>"
		Response.Write "	<td align='left'>"
		Response.Write "		<b>Dettaglio Comportamento</B>"
		Response.Write "	</td>"
		Response.Write "</tr>"
		Response.write "<tr>"
		Response.Write "<td>"
			Response.Write "<table border=0 cellspacing=1  CELLPADDING=1 width=500>"
			Response.Write "<tr>"
				Response.Write "<td align=left class='tbltext1'><b>Figura Professionale&nbsp;&nbsp;</b></td>"
				Response.Write "<td class='tbltext1'>" & request("DENOMINAZIONE") & "</td>"
			Response.Write "</tr>"

			Response.Write "<tr>"
				Response.Write "<td align=left class='tbltext1'><b>Comportamento</b></td>"
				Response.Write "<td class='tbltext1'>" & COMPORT(0,0) & "</td>"
			Response.Write "</tr>"
			Response.Write "<tr>"
				Response.Write "<td align=left class='tbltext1'><b>Area Comportamento</b></td>"
				Response.Write "<td class='tbltext1'>" & COMPORT(6,0) & "</td>"
			Response.Write "</tr>"
			Response.Write "<tr>"
				Response.Write "<td align=left class='tbltext1'><b>Grado di Possesso</b></td>"
				Response.Write "<td class='tbltext1'>" & COMPORT(7,0) & " &nbsp;&nbsp;&nbsp;"
				Response.Write "<input type='button' value='Modifica' OnClick='regola()' CLASS='My'>"
				Response.Write "</td>"
			Response.Write "</tr>"
			Response.Write "<tr>"
				Response.Write "<td align=left class='tbltext1't><b>Associazione effettuata da</b></td>"
				Response.Write "<td class='tbltext1'>" & COMPORT(3,0) & " " & COMPORT(4,0) & "</td>"
			Response.Write "</tr>"
			Response.Write "<tr>"
				Response.Write "<td align=left class='tbltext1'><b>Ultima Operazione effettuata il</b></td>"
				Response.Write "<td class='tbltext1'>" & COMPORT(2,0) & "</td>"
			Response.Write "</tr>"
			Response.Write "<tr>"
				Response.Write "<td  align='left'>"
					Response.Write "<input type='button' value='Esci' OnClick='goToPage(""RiepilogoProfilo.asp?ID_FIGPROF=" & request("ID_FIGPROF") & "&DENOMINAZIONE=" & request("DENOMINAZIONE") & """)' CLASS='My'>&nbsp;&nbsp;&nbsp;</td>"
					Response.Write "<td colspan='2' align='right'>"
					Response.Write "<input type='submit' name='Valida' value='Valida' CLASS='My'>&nbsp;&nbsp;&nbsp;"
					Response.Write "<input type='submit' name='Elimina' value='Elimina' CLASS='My'></td>"
				Response.Write "</td>"
			Response.Write "</tr>"
			Response.Write "<tr>"
		Response.Write "<td colspan=3 bgcolor='#3399CC'></td>"
        Response.Write "</tr>"
		Response.Write "</table>"
		Response.Write "</td>"
		Response.Write "</tr>"
		Response.Write "</table>"
	end if
%>
</form>
<!-- #include Virtual="/strutt_coda2.asp" -->

