<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #include Virtual="/strutt_testa2.asp" -->
<!-- #INCLUDE FILE="../Utils.asp" -->
<%
Set Conn = server.CreateObject ("ADODB.Connection")
Conn.open strConn

Sql = ""
Sql = Sql & "SELECT "
Sql = Sql & "COMPETENZE.DENOMINAZIONE, VALIDAZIONE.ID_PERS_INS, VALIDAZIONE.DT_TMST, "
Sql = Sql & "PERSONA.COGNOME, PERSONA.NOME, VALIDAZIONE.ID_VALID, COMPET_FP.GRADO_FP_COMP "
Sql = Sql & "FROM "
Sql = Sql & "COMPETENZE, COMPET_FP, VALIDAZIONE, PERSONA "
Sql = Sql & "WHERE "
Sql = Sql & "COMPETENZE.ID_COMPETENZA = COMPET_FP.ID_COMPETENZA "
Sql = Sql & "AND "
Sql = Sql & "VALIDAZIONE.ID_VALID = COMPET_FP.ID_VALID "
Sql = Sql & "AND "
Sql = Sql & "PERSONA.ID_PERSONA (+) = VALIDAZIONE.ID_PERS_INS "
Sql = Sql & "AND "
Sql = Sql & "COMPET_FP.ID_FIGPROF = " & request("ID_FIGPROF")
Sql = Sql & "AND "
Sql = Sql & "COMPET_FP.ID_COMPETENZA = " & request("ID_COMPETENZA")

'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
Set Rs = Conn.Execute (Sql)
if not rs.eof then COMPET = Rs.getrows()

Conn.Close
Set conn = Nothing

%>

<script language='Javascript'>
	function regola()
	{
		URL = "RegolaGradoCompet.asp?"
		URL = URL + 'ID_FIGPROF=<%=request("ID_FIGPROF")%>&'
		URL = URL + 'DENOMINAZIONE=<%=request("DENOMINAZIONE")%>&'
		URL = URL + 'ID_VALID=<%=COMPET(5,0)%>&'
		URL = URL + 'ID_COMPETENZA=<%=request("ID_COMPETENZA")%>' + "&VISMENU=NO"

		opt = "address=no,status=no,width=600,height=150,top=150,left=35"

		window.open (URL,"",opt)
	}
</script>

<form name='DettagliCompetenza' Method=POST Action='SalvaDettagliCompetenza.asp'>
<input type="hidden" name="IdPers" value="<%=sIdPers%>">
<input type="hidden" name="IND_FASE" value="<%=IND_FASE%>">
<input type="hidden" name="VISMENU" value="NO">


<table border=0 width='500' CELLPADDING=0 cellspacing=0>
	<tr class="tbltext0">
		<td bgcolor='#3399CC'class=tbltext0 align=left width='199'>
			<b>DETTAGLIO COMPETENZA</b>
		</td>
		<td width="25" valign='bottom' background="<%=session("progetto")%>/images/sfondo_linguetta.gif" >
			<img border="0" src="<%=session("progetto")%>/images/tondo_linguetta.gif"></td>
		<td width="278" valign='bottom' background="<%=session("progetto")%>/images/sfondo_linguetta.gif"></td>
	</tr>
</table>

<table border=0 width='500' CELLPADDING=0 cellspacing=0>
	 <tr class="sfondocomm">
	 <td>In questa pagina potrai visualizzare il dettaglio della <b>Competenza</b> associata
	  al <b>Profilo</b> selezionato.</td>
	 <td>
	   <a href="Javascript:Show_Help('<%=Session("Progetto")%>/HelpBP/Validazione/Profili/DettagliCompetenza.htm')">
	   <img src="<%=Session("Progetto")%>/images/Help.gif" border='0'  align=right alt='Help'></a>
	 </td>
	</tr>

    <tr>
		<td colspan=3 bgcolor='#3399CC'></td>
    </tr>
</table>



<!--table border=0 cellspacing=1 cellpadding=0 width='500' style="font-family: Verdana; font-size: 12pt; color:#ffffff">
<tr><td background='../imgs/sfmnsmall.jpg' align=center><font color='#000000'>
	<B><small>Figura Professionale: <%=request("DENOMINAZIONE")%></small></B>
</td></tr>
</table-->
<br>
<%
	if IsArray(COMPET) then
		Response.Write "<input type='hidden' name='ID_FIGPROF' value='" & request("ID_FIGPROF") & "'>"
		Response.Write "<input type='hidden' name='DENOMINAZIONE' value='" & request("DENOMINAZIONE") & "'>"
		Response.Write "<input type='hidden' name='ID_COMPETENZA' value='" & request("ID_COMPETENZA") & "'>"
		Response.Write "<input type='hidden' name='ID_VALID' value='" & COMPET(5,0) & "'>"
		Response.Write "<table border=0 width='500' cellspacing=1 cellpadding=1>"
		Response.Write "<tr class='sfondocomm'>"
		Response.Write "		<td align='left'><B>Dettaglio Competenza</B></td>"
		Response.Write "</tr>"


			Response.Write "<table border=0 cellspacing=1 cellpadding=1 width=500>"
			Response.Write "<tr>"
				Response.Write "<td align='left' class='tbltext1'><b>Figura Professionale&nbsp;&nbsp;</b></td>"
				Response.Write "<td class='tbltext1'>" & request("DENOMINAZIONE") & "</td>"
			Response.Write "</tr>"

			Response.Write "<tr>"
				Response.Write "<td align='left' class='tbltext1'><b>Competenza&nbsp;&nbsp;</b></td>"
				Response.Write "<td class='tbltext1'>" & COMPET(0,0) & "</td>"
			Response.Write "</tr>"
			Response.Write "<tr>"
				Response.Write "<td align='left' class='tbltext1'><b>Grado di possesso&nbsp;&nbsp;</b></td>"
				Response.Write "<td class='tbltext1'>" & COMPET(6,0) & " &nbsp;&nbsp;&nbsp;"
				Response.Write "<input type='button' value='Modifica' OnClick='regola()' CLASS='My'>"
				Response.Write "</td>"
			Response.Write "</tr>"
			Response.Write "<tr>"
				Response.Write "<td align='left' class='tbltext1'><b>Associazione effettuata da&nbsp;&nbsp;</b></td>"
				Response.Write "<td class='tbltext1'>" & COMPET(3,0) & " " & COMPET(4,0) & "</td>"
			Response.Write "</tr>"
			Response.Write "<tr>"
				Response.Write "<td align='left' class='tbltext1'><b>Ultima Operazione effettuata il&nbsp;&nbsp;</b></td>"
				Response.Write "<td class='tbltext1'>" & COMPET(2,0) & "</td>"
			Response.Write "</tr>"
			Response.Write "<tr>"
				Response.Write "<td align='left'>"
					Response.Write "<input type='button' value='Esci' OnClick='goToPage(""RiepilogoProfilo.asp?ID_FIGPROF=" & request("ID_FIGPROF") & "&DENOMINAZIONE=" & request("DENOMINAZIONE") & """)' CLASS='My'>&nbsp;&nbsp;&nbsp;</td>"
					Response.Write "<td align='right'><input type='submit' name='Valida' value='Valida' CLASS='My'>&nbsp;&nbsp;&nbsp;"
					Response.Write "<input type='submit' name='Elimina' value='Elimina' CLASS='My'>"
				Response.Write "</td>"
			Response.Write "</tr>"
			Response.Write "<tr>"
				Response.Write "<td colspan=3 bgcolor='#3399CC'></td>"
		    Response.Write "</tr>"
			Response.Write "</table>"

		Response.Write "</table>"
	end if
%>
</form>

<!-- #include Virtual="/strutt_coda2.asp" -->

