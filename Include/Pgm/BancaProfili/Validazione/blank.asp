<!-- #include virtual='/util/portallib.asp' -->
<!-- #include file='utils.asp' -->

<html>

<head>
	<LINK REL=STYLESHEET TYPE="text/css" HREF="../fogliostile.css">
	<title>Progetto IN - Banca Profili</title>
</head>

<body bgcolor="#ffffff" text="#000000">
<TABLE BORDER=0 cellspacing=0 cellpadding=1 WIDTH='570'>
<TR>
	<TD valign='middle' align='right'>
        <p align="center"><font FACE="Verdana" size="2"><b>Gestione Banca
        Profili - Progetto&nbsp;</b></font>
        </p>
	</TD>
	<td align=left>
		<IMG src="Imgs\In.gif">
    </td>
</TR>
<tr>
	<td colspan=2>
      <font face="Verdana" size="2"><span style="mso-bidi-font-size: 12.0pt; mso-fareast-font-family: Times New Roman; mso-bidi-font-family: Times New Roman; mso-ansi-language: IT; mso-fareast-language: IT; mso-bidi-language: AR-SA">Le
      funzionalit� che andrai ad utilizzare ti consentiranno di codificare e
      gestire le complessit� dei vari livelli di analisi delle Figure
      Professionali, contestualizzandole sul territorio ed in azienda. Ti
      consentiranno di legare aree e profili professionali con obiettivi di
      incrocio di rilevazione, analisi e incrocio di domanda e offerta di
      competenze.L'ambiente � strutturato in :</span></font>
      <ul>
        <li>
          <p class="MsoNormal" style="margin-left:.25in;text-indent:-.25in;mso-list:l801 level1 lfo1896;
tab-stops:list .25in"><font face="Verdana" size="2"><span style="mso-bidi-font-size: 12.0pt"><b>un
          percorso metodologico che segue il percorso di analisi</b>:<o:p>
          </o:p>
          </span></font></li>
      </ul>
      <blockquote>
        <ul>
          <li>
            <p class="MsoNormal" style="margin-left:.25in;text-indent:-.25in;mso-list:l971 level1 lfo1898;
tab-stops:list .25in"><font face="Verdana" size="2"><span style="mso-bidi-font-size: 12.0pt">individuazione
            di output di competenza;</span></font></li>
          <li>
            <p class="MsoNormal" style="margin-left:.25in;text-indent:-.25in;mso-list:l971 level1 lfo1898;
tab-stops:list .25in"><font face="Verdana" size="2"><span style="mso-bidi-font-size: 12.0pt">costruzione
            di profili professionali;<o:p>
            </o:p>
            </span></font></li>
          <li>
            <p class="MsoNormal" style="margin-left:.25in;text-indent:-.25in;mso-list:l971 level1 lfo1898;
tab-stops:list .25in"><font face="Verdana" size="2"><span style="mso-bidi-font-size: 12.0pt">identificazione
            delle capacit�/conoscenze necessarie alla produzione di output
            professionale allocabili in processi;<o:p>
            </o:p>
            </span></font></li>
          <li>
            <p class="MsoNormal" style="margin-left:.25in;text-indent:-.25in;mso-list:l971 level1 lfo1898;
tab-stops:list .25in"><font face="Verdana" size="2"><span style="mso-bidi-font-size: 12.0pt">associazione
            di profili ad aggregati territoriali, settoriali e di aree
            professionali;<o:p>
            </o:p>
            </span></font></li>
        </ul>
      </blockquote>
      <ul>
        <li>
          <p class="MsoNormal" style="margin-left:.25in;text-indent:-.25in;mso-list:l801 level1 lfo1896;
tab-stops:list .25in"><font face="Verdana" size="2"><span style="mso-bidi-font-size: 12.0pt"><b>un
          sistema di creazione ordinata di dizionari di denominazioni e
          descrizioni di :</b></span></font></li>
      </ul>
      <blockquote>
        <ul>
          <li>
            <p class="MsoNormal" style="margin-left:.25in;text-indent:-.25in;mso-list:l1302 level1 lfo1899;
tab-stops:list .25in"><font face="Verdana" size="2"><span style="mso-bidi-font-size: 12.0pt">&nbsp;profili
            professionali;</span></font></li>
          <li>
            <p class="MsoNormal" style="margin-left:.25in;text-indent:-.25in;mso-list:l1302 level1 lfo1899;
tab-stops:list .25in"><font size="2"><span style="mso-bidi-font-size: 12.0pt"><font face="Verdana">&nbsp;competenze
            professionali;</font></span></font></li>
          <li>
            <p class="MsoNormal" style="margin-left:.25in;text-indent:-.25in;mso-list:l1302 level1 lfo1899;
tab-stops:list .25in"><font face="Verdana" size="2"><span style="mso-bidi-font-size: 12.0pt; mso-fareast-font-family: Times New Roman; mso-bidi-font-family: Times New Roman; mso-ansi-language: IT; mso-fareast-language: IT; mso-bidi-language: AR-SA">&nbsp;elementi
            di competenza (capacit� e conoscenze);</span></font></li>
          <li>
            <p class="MsoNormal" style="margin-left:.25in;text-indent:-.25in;mso-list:l1302 level1 lfo1899;
tab-stops:list .25in"><font face="Verdana" size="2"><span style="mso-bidi-font-size: 12.0pt; mso-fareast-font-family: Times New Roman; mso-bidi-font-family: Times New Roman; mso-ansi-language: IT; mso-fareast-language: IT; mso-bidi-language: AR-SA">comportamenti</span></font></li>
        </ul>
      </blockquote>
      <font FACE="Verdana" size="2">
      <p>Ti consigliamo di procedere seguendo l�ordine di compilazione
      previsto poich� alcune pagine si generano dinamicamente, cio� si
      completano in base a informazioni inserite in passi antecedenti del
      percorso.<br><br>
      Cliccando su questo simbolo <IMG src="imgs/imghelp.gif">  potrai in qualsiasi
      momento trovare un aiuto utile a comprendere cosa ti viene richiesto, le
      modalit� di inserimento e logica di base della richiesta di informazioni.
      </font>
      </p>
    </td>
</tr>
</TABLE>

<!-- #INCLUDE FILE="../Footer.asp" -->
</body>

</html>
