<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #include Virtual="/strutt_testa2.asp" -->
<!-- #INCLUDE FILE="Utils.asp" -->

<%
Set Conn = server.CreateObject ("ADODB.Connection")
Conn.open strConn

	Sql = ""
	Sql = Sql & "SELECT "
	Sql = Sql & "ELEMENTI_USR.DENOMINAZIONE, ELEMENTI_USR.ID_PERSONA, ELEMENTI_USR.DT_TMST, "
	Sql = Sql & "PERSONA.COGNOME, PERSONA.NOME, ELEMENTI_USR.POSSESSO, ELEMENTI_USR.FL_TIPO, "
	Sql = Sql & "ELEMENTI_USR.ID_AREA "
	Sql = Sql & "FROM "
	Sql = Sql & "ELEMENTI_USR, PERSONA "
	Sql = Sql & "WHERE "
	Sql = Sql & "PERSONA.ID_PERSONA (+) = ELEMENTI_USR.ID_PERSONA "
	Sql = Sql & "AND "
	Sql = Sql & "ELEMENTI_USR.ID_ELEMENTO = " & request("ID_ELEMENTO")

'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
	Set Rs = Conn.Execute (Sql)
	if not rs.eof then CONOSC = Rs.getrows()

	Sql = ""
	Sql = Sql & "SELECT ID_AREACONOSCENZA, DENOMINAZIONE "
	Sql = Sql & "FROM AREA_CONOSCENZA "
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
	Set Rs = Conn.Execute (Sql)
	if not rs.eof then AREE = Rs.getrows()

Set Rs = Nothing
Conn.Close
Set conn = Nothing

%>

<CENTER>
<form name='Ver_Conoscenza_USR' Method=POST Action='SalvaVer_Conoscenza_USR.asp'>



<table border=0 cellspacing=1 cellpadding=0 width='500' style="font-family: Verdana; font-size: 8pt; color:#ffffff">
	<tr>
	<td align=center>
		<table border=0 width='500' CELLPADDING=0 cellspacing=0>
			<tr>
				<td bgcolor='#3399CC' align=left bordercolor="#C2E0FF" width='199'>
					<b><font face=verdana SIZE='1' color='#ffffff'>DETTAGLIO CONOSCENZA (USR)</b></font>
				</td>
			<td width="25" valign='bottom' background="../../images/sfondo_linguetta.gif" >
				<img border="0" src="../../images/tondo_linguetta.gif"></td>
			<td width="278" valign='bottom' background="../../images/sfondo_linguetta.gif" >&nbsp;</td>
			</tr>
		    <tr>
				<td align="left" colspan="3" bgcolor='#C6DFFF' >
				<font face=Verdana color=#000000 size=1>
			<small>Validazione Conoscenza (utente) :<i> <%=request("DENOMINAZIONE")%><i></small>
				</font></td>
		    </tr>
		    <tr>
				<td colspan=3 bgcolor='#3399CC'></td>
	    </tr>

<br>
<%
	if IsArray(CONOSC) then
		Response.Write "<input type='hidden' name='ID_PERSONA' value='" & CONOSC(1,0) & "'>"
		Response.Write "<input type='hidden' name='POSSESSO' value='" & CONOSC(5,0) & "'>"
		Response.Write "<input type='hidden' name='ID_ELEMENTO' value='" & request("ID_ELEMENTO") & "'>"
		Response.Write "<table border=0 width='500' cellspacing=1 cellpadding=0 style='font-family: Verdana; font-size: 10pt; color:#000000'>"
		Response.write "<tr>"
		Response.Write "<td>"
			Response.Write "<table border=0 cellspacing=1 width=500 style='font-size: 8pt; font-family: verdana'>"
			Response.Write "<tr>"
				Response.Write "<td bgcolor='#FFFFFF' align=LEFT><FONT COLOR='#3196CE' size=1><b>Denominazione&nbsp;&nbsp;</B></font></td>"
				Response.Write "<td bgcolor='#FFFFFF'>"
					Response.Write "<input type='text' name='Denominazione' value='" & trim(request("DENOMINAZIONE")) & "' size=50 >"
				Response.Write "</td>"
			Response.Write "</tr>"
			Response.Write "<tr>"
				Response.Write "<td bgcolor='#FFFFFF' align=LEFT><FONT COLOR='#3196CE' size=1><b>Area Conoscenza&nbsp;&nbsp;</B></font></td>"
				Response.Write "<td bgcolor='#FFFFFF'>"
					if IsArray(AREE) then
						Response.Write "<select name='ID_AREACONOSCENZA'>"
						for J = lbound(AREE,2) to Ubound(AREE,2)
							Response.Write "<option value='" & AREE(0,J) & "' "
							if int(0 & AREE(0,J)) = Int(0 & CONOSC(7,0)) then Response.Write "selected"
							Response.Write " >" & AREE(1,J) & "</option>"
						next
						Response.Write "</select>"
					end if
				Response.Write "</td>"
			Response.Write "</tr>"
			Response.Write "<tr>"
				Response.Write "<td bgcolor='#FFFFFF' align=LEFT><FONT COLOR='#3196CE' size=1><b>Inserita da&nbsp;&nbsp;</B></font></td>"
				Response.Write "<td bgcolor='#FFFFFF'><small>" & CONOSC(3,0) & " " & CONOSC(4,0) & "</small></td>"
			Response.Write "</tr>"
			Response.Write "<tr>"
				Response.Write "<td bgcolor='#FFFFFF' align=LEFT><FONT COLOR='#3196CE' size=1><b>Ultima Operazione&nbsp;&nbsp;</B></font></td>"
				Response.Write "<td bgcolor='#FFFFFF'><small>" & CONOSC(2,0) & "</small></td>"
			Response.Write "</tr>"
			Response.Write "<tr>"
				Response.Write "<td bgcolor='#FFFFFF' colspan='2' align='right'>"
					Response.Write "<input type='button' value='<< esci' OnClick='document.location=""Conoscenze_USR.asp""' CLASS='My'>&nbsp;&nbsp;&nbsp;"
					Response.Write "<input type='submit' name='SalvaValida' value='salva e valida' CLASS='My'>&nbsp;&nbsp;&nbsp;"
				Response.Write "</td>"
			Response.Write "</tr>"
			Response.Write "</table>"
		Response.Write "</td>"
		Response.Write "</tr>"
		Response.Write "</table>"
	end if
%>
</form>
<br>
<table width=500 border=0 cellspacing=0 cellpadding=0>
	<tr>
		<td class="copyright">Copyright � 2001-2003 Consorzio
		<span class="ejob"><b>ejob-pl@ce</b></span>
		Tutti i diritti riservati
		</td>
	</tr>
</table>
</CENTER>
<!-- #include Virtual="/strutt_coda2.asp" -->
