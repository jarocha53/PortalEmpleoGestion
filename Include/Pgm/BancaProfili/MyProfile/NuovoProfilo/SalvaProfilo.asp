<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #INCLUDE FILE="../Utils.asp" -->

<%
Set Conn = server.CreateObject ("ADODB.Connection")
Conn.open strConn
OraDate=ConvDateToDB(Now())

Conn.BeginTrans

	'Creazione di un nuovo record di validazione
	LstCampi = "ID_PERS_INS,ID_PERS_LAV,FL_VALID,DT_TMST"

	LstVal = ""
	LstVal = LstVal & IDSedeAz & "|"
	LstVal = LstVal & IDSedeAz & "|"
	LstVal = LstVal & "0" & "|"
	LstVal = LstVal & OraDate

	Sql = ""
	Sql = QryIns("VALIDAZIONE",LstCampi,LstVal)
	
	'Response.Write sql & "<br><br>"
	
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
	Conn.Execute Sql

	'Recupero del massimo ID di validazione inserito
	Sql = ""
	Sql = "SELECT Max(ID_VALID) FROM VALIDAZIONE WHERE ID_PERS_INS = " & IdSedeAz
	
	
	
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
	Rs = Conn.Execute(Sql)
	ID_VALID = Rs(0)
	
	'Response.Write sql & "<br><br>"
	
	'Response.Write id_valid & "<br><br>" 
	
	set Rs = Nothing

	'Inserimento informazioni sulla nuova Fig Prof
	LstCampi = "DENOMINAZIONE,ID_AREAPROF,DESCRIZIONE,BREVE_DESC,ID_VALID,COD_EJ"

	LstVal = ""
	LstVal = LstVal & "'" & trim(CleanXsql(left(request("DENOMINAZIONE"),60))) & "'" & "|"
	LstVal = LstVal & request("ID_AREAPROF") & "|"
	LstVal = LstVal & "'" & TRIM(CleanXsql(left(request("DESCRIZIONE"),2000))) & "'" & "|"
	LstVal = LstVal & "'" & TRIM(CleanXsql(left(request("BREVEDESCR"),200))) & "'" & "|"
	LstVal = LstVal & ID_VALID & "|"
	LstVal = LstVal & "'" & TRIM(CleanXsql(request("COD_ISTAT"))) & "'"

	Sql = ""
	Sql = QryIns("FIGUREPROFESSIONALI",LstCampi,LstVal)
	
	'Response.Write sql & "<br><br>"
	
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
	Conn.Execute Sql


	' Recupero l'ID della fig. Profess. appena inserita
	set rs=nothing
	Sql=""
	Sql= "SELECT ID_FIGPROF FROM FIGUREPROFESSIONALI WHERE ID_VALID=" & ID_VALID
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
	Rs = Conn.Execute(Sql)
	ID_FIGPROF = Rs(0)
	set Rs = Nothing

	'Response.Write sql & "<br><br>" 
	
	'Response.Write id_figprof & "<br><br>" 

'### Associazione all'Azienda che lo sta inserendo 
	'Response.Write LstCampi
	
		LstCampi = "ID_SEDE,ID_FIGPROF,DT_TMST"
		LstVal = ""
		LstVal = LstVal & IDSedeAz & "|"
		LstVal = LstVal & ID_FIGPROF  & "|"
		LstVal = LstVal & OraDate 
		Sql = ""
		Sql = QryIns("IMPRESA_FP",LstCampi,LstVal)
		
		'Response.Write Sql & "<br><br>"
		
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
		Conn.Execute Sql
'#### Fine associazione azienda 
		

'Associazione automatica PROFILO - PROGETTO

	Sql=""
	Sql= "INSERT INTO FIGPROF_PRJ (ID_FIGPROF,COD_PRJ) VALUES(" & ID_FIGPROF & ",'" & Progetto & "')"
	
	'Response.Write sql & "<br><br>"
	
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
	Rs = Conn.Execute(Sql)
'### Fine associaizone
		
' Associazione SETTORE Profilo
	Sql = ""
	Sql = " SELECT SETTORI.ID_SETTORE "
	Sql = Sql &  " FROM SETTORI, SEDE_IMPRESA, IMPRESA "
	Sql = Sql &  " WHERE"
	Sql = Sql &  " IMPRESA.ID_SETTORE = SETTORI.ID_SETTORE "
	Sql = Sql &  " AND IMPRESA.ID_IMPRESA = SEDE_IMPRESA.ID_IMPRESA "
	Sql = Sql &  " AND  SEDE_IMPRESA.ID_SEDE = " & IdSedeAz

	'Response.Write sql & "<br><br>"
	
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
	Rs = Conn.Execute(Sql)
	
	
	ID_SETTORE = CleanXsql(Rs(0))
	set Rs = Nothing

	'if ID_SETTORE = "" then 
		'Response.Write "nada"
	'else
		'Response.Write ID_SETTORE & "<br><br>"
	'end if 
	
	SqlSettore = ""
	SqlSettore = SqlSettore & " INSERT INTO SETTORI_FIGPROF " 
	SqlSettore = SqlSettore & " (ID_SETTORE,ID_FIGPROF,ID_VALID) "
	SqlSettore = SqlSettore & " VALUES "
	SqlSettore = SqlSettore & "(" & ID_SETTORE & "," & Id_Figprof & "," & Id_Valid & ")" 

	'Response.Write SqlSettore & "<br><br>" 
	
'PL-SQL * T-SQL  
SQLSETTORE = TransformPLSQLToTSQL (SQLSETTORE) 
	conn.execute(SqlSettore)


   	LogTrace ucase(mid(session("progetto"),2)),IdSedeAz , "S", IdSedeAz ,"FIGUREPROFESSIONALI", "" & IDSedeAz & "", "INS"

Conn.CommitTrans
Conn.Close
Set conn = Nothing
%>
<script>
<!--
	goToPage("../Default.asp")
//-->
</script>
