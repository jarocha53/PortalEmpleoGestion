<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #include Virtual="/strutt_testa2.asp" -->
<!-- #INCLUDE FILE="../Utils.asp" -->
<%

Set Conn = CC
'Set Conn = server.CreateObject ("ADODB.Connection")
'Conn.open strConn

Sql = ""
Sql = Sql & "SELECT "
Sql = Sql & "COMPETENZE.DENOMINAZIONE, COMPETENZE.ID_VERBO, "
Sql = Sql & "COMPETENZE.OUTPUT, COMPETENZE.ID_COMPETENZA, COMPETENZE.ID_VALID "
Sql = Sql & "FROM "
Sql = Sql & "COMPETENZE, VALIDAZIONE "
Sql = Sql & "WHERE "
Sql = Sql & "COMPETENZE.ID_VALID = VALIDAZIONE.ID_VALID "
Sql = Sql & "AND "
Sql = Sql & "VALIDAZIONE.ID_PERS_INS = " & IDP
Sql = Sql & " AND "
Sql = Sql & "VALIDAZIONE.FL_VALID <> 1"
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
Set Rs = Conn.Execute (Sql)
if not rs.eof then COMPETENZE = Rs.getrows()

Sql = ""
Sql = Sql & "SELECT VERBI.ID_VERBO, VERBI.DENOMINAZIONE "
Sql = Sql & "FROM VERBI"
'Commentato perch� i verbi non necessitano di Validazione
'Sql = Sql & ", VALIDAZIONE "
'Sql = Sql & "WHERE "
'Sql = Sql & "VERBI.ID_VALID = VALIDAZIONE.ID_VALID "
'Sql = Sql & "AND "
'Sql = Sql & "(VALIDAZIONE.FL_VALID = 1 "
'Sql = Sql & "OR "
'Sql = Sql & "VALIDAZIONE.ID_PERS_INS = " & IDP & ")"
Sql = Sql & " ORDER BY UPPER(VERBI.DENOMINAZIONE)"

'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
Set Rs = Conn.Execute (Sql)
if not rs.eof then VERBI = Rs.getrows()

Set Rs = Nothing
Conn.Close
Set conn = Nothing
%>

<script language='Javascript'>
<!--

function valida(Obj)
{
	if ((Obj.Attivita.value == "") || (Obj.Output.value == "") || (Obj.ID_VERBO.value == "") || (Obj.Denominazione.value == ""))
	{
		alert("Los campos: Actividad, Output, Verbo y Denominaci�n son obligatorios")
		return false
	}
	else
	{
		if (confirm("Confirma el ingreso de la Competencia?") == false)
		{
			return false;
		}
		else
		{
			return true;
		}
	}
}


	function proponi(frm)
	{
		frm.Denominazione.value = frm.ID_VERBO.options[frm.ID_VERBO.selectedIndex].title + " " + frm.Output.value
	}

	function verbi(frm)
	{
		URL = "Verbi.asp?"
		URL = URL + "ATTIVITA=" + frm.Attivita.value + "&"
		URL = URL + "OUTPUT=" + frm.Output.value + "&"
		URL = URL + "DESCRIZIONE=" + frm.Note.value + "&VISMENU=NO"

		opt = "address=no,status=no,width=520,height=200,top=150,left=35"

		window.open (URL,"",opt)
	}

	function FocusCompet(ID_COMPETENZA)
	{
		URL = "FocusCompet.asp?"
		URL = URL + "ID_COMPETENZA=" + ID_COMPETENZA + "&VISMENU=NO"

		opt = "address=no,status=no,width=600,height=500,top=150,left=35,scrollbars=yes"

		window.open (URL,"",opt)
	}

function elimina(A,B)
{
	if (confirm("Seguro desea ELIMINAR definitivamente esta competencia?"))
	{
		goToPage("CancCompet.asp?ID_COMPETENZA=" + A + "&ID_VALID=" + B)
	}
}
//-->
</script>

<center>


<table border=0 width='500' CELLPADDING=0 cellspacing=0>
	<tr class="tbltext0">
		<td bgcolor='#3399CC'class=tbltext0 align=left >
		<b>INGRESO DE COMPETENCIA</b>
		</td>
		<td width="25" valign='bottom' background="<%=session("progetto")%>/images/sfondo_linguetta.gif" >
		<img border="0" src="<%=session("progetto")%>/images/tondo_linguetta.gif"></td>
		<td width="278" valign='MIDDLE' align=right class=tbltext1 background="<%=session("progetto")%>/images/sfondo_linguetta.gif" >(*) campo obligatorio</td>
	</tr>
</table>
<table border=0 width='500' CELLPADDING=0 cellspacing=0>
	<tr class="sfondocomm">
		<td>
		En esta p�gina puede agregar una nueva <b>Competencia</b> al diccionario de datos.
		</td>
	   	<td class=sfondocomm>
	   	<a href="Javascript:Show_Help('<%=Session("Progetto")%>/HelpBP/Placement/Competenze/Add_Competenze.htm')">
		<img src="<%=Session("Progetto")%>/images/Help.gif" border='0'  align=right alt='Help'></a>
		</td>
    </tr>
    <tr>
		<td colspan=2 bgcolor='#3399CC'></td>
    </tr>
</table>


<table border=0 cellspacing=1 cellpadding=1 width='500'>
<tr>
<td align=center>
<form name='Competenze' Action='SalvaCompet.asp?VISMENU=NO' Method='POST' OnSubmit='return valida(this)'>
	<input type="hidden" name="IdPers" value="<%=sIdPers%>">
	<input type="hidden" name="IND_FASE" value="<%=IND_FASE%>">
	<input type="hidden" name='DEN' value='<%=request("DENOMINAZIONE")%>'>
	<input type='hidden' name='ID_COMPETENZA' value='<%=request("ID_COMPETENZA")%>'>
	<input type='hidden' name='VISMENU' value='NO'>
	<table border=0 width='500' cellspacing=1 cellpadding=0>
	</font></td></tr>
		<tr>
			<td class='tbltext1' align=LEFT><b>Actividad*&nbsp;</td>
			<td bgcolor='#FFFFFF'><input type='text' class='MyTextBox' maxlength='60' size='57' name='Attivita' value='<%=request("ATTIVITA")%>'></td>
		</tr>
		<tr>
			<td class='tbltext1' align=LEFT><b>Output*&nbsp;</td>
			<td bgcolor='#FFFFFF'><input type='text' class='MyTextBox' maxlength='80' size='57' name='Output' value='<%=request("OUTPUT")%>'></td>
		</tr>
		<tr>
			<td class='tbltext1' align=LEFT><b>Verbo*&nbsp;</td>
			<td bgcolor='#FFFFFF'>
			<%
			if isArray(VERBI) then
			Response.Write "<select onchange='proponi(this.form)' class='MY' Name='ID_VERBO'>"
			Response.Write "<option  value=''></option>"
			for I = lbound(VERBI,2) to Ubound(VERBI,2)
				Response.Write "<option "
				if VERBI(1,I) = request("VERBO") then Response.Write " selected "
				Response.write " title='" & VERBI(1,I) & "' value='" & VERBI(0,I) & "'>" & VERBI(1,I) & "</option>"
			next
			Response.Write "</select>"
			end if
			%>
			<input type='button' value='Nuevo Verbo' Class='My' OnClick='verbi(this.form)'>
			</td>
		</tr>
		<tr>
			<td class='tbltext1' align=LEFT><b>Denominaci�n*&nbsp;</td>
			<!--<td bgcolor='#FFFFFF'><input OnFocus='proponi(this.form)' class='MyTextBox' type='text' name='Denominazione' readonly maxlength='100' size='57' value=''></td>-->
			<td bgcolor='#FFFFFF'><input class='MyTextBox' type='text' name='Denominazione' readonly maxlength='100' size='57' value=''></td>

		</tr>
		<tr>
			<td class='tbltext1' align=LEFT><b>Descripci�n&nbsp;</td>
			<td bgcolor='#FFFFFF'><TEXTAREA cols='42' class='MyTextBox' rows='3' name='Note'><%=request("DESCRIZIONE")%></TEXTAREA></td>
		</tr>
		<tr>
			<td bgcolor='#FFFFFF' align=left>
				<input type='Button' value='Volver' OnCLick='javascript: goToPage("Competenze.asp")'CLASS='My'>
			</td>
			<td bgcolor='#FFFFFF' align=right>
				<input type='Submit' value='Guardar' CLASS='My'>
			</td>
		</tr>
		<tr>
		<td colspan=3 bgcolor='#3399CC'></td>
    </tr>
	</table>
</form>
</td>
</tr>
</table>
<!-- #include Virtual="/strutt_coda2.asp" -->
