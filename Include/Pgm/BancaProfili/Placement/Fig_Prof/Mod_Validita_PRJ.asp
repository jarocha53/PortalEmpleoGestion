<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #INCLUDE FILE="../Utils.asp" -->
<title>Modifica Area di validit� del Profilo</title>
<LINK REL=STYLESHEET TYPE="text/css" HREF='<%=session("progetto")%>/fogliostile.css'>
<body class=sfondocentro topmargin="10" leftmargin="0">

<%

Set Conn = server.CreateObject ("ADODB.Connection")
Conn.open strConn

Sql = ""
Sql = Sql & "SELECT CODICE, DESCRIZIONE "
Sql = Sql & "FROM TADES "
Sql = Sql & "WHERE ISA = '0' AND NOME_TABELLA = 'REGIO'"

'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
Set Rs = Conn.Execute (Sql)
if not rs.eof then REGIONI = Rs.getrows()

Sql = ""
Sql = Sql & "SELECT CODICE, DESCRIZIONE "
Sql = Sql & "FROM TADES "
Sql = Sql & "WHERE ISA = '0' AND NOME_TABELLA = 'PROV'"

'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
Set Rs = Conn.Execute (Sql)
if not rs.eof then PROVINCE = Rs.getrows()

Sql = ""
Sql = Sql & "SELECT CODICE, DESCRIZIONE "
Sql = Sql & "FROM TADES "
Sql = Sql & "WHERE ISA = '0' AND NOME_TABELLA = 'SPI'"

'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
Set Rs = Conn.Execute (Sql)
if not rs.eof then SPI = Rs.getrows()


Set Rs = Nothing
Conn.Close
Set conn = Nothing
%>

<script language='Javascript'>
<!--
function valida(Obj)
{

	CodVis = "NONSPEC";
	
	for (i=0; i <= 3; i++)
	{
		if (Obj.COD_VISIBILITA[i].checked)
		{
			CodVis = Obj.COD_VISIBILITA[i].value
		}
	}
	
	switch (CodVis)
	{
		case "REGIONALE":
		{
			if (Obj.COD_REGIONE.options[Obj.COD_REGIONE.selectedIndex].value == "")
			{
				alert("Se la visibilt� � regionale � necessario specificare la regione")
				return false;
			}
			break;
		}
		case "PROVINCIALE":
		{
			if (Obj.COD_PRV.options[Obj.COD_PRV.selectedIndex].value == "")
			{
				alert("Se la visibilt� � provinciale � necessario specificare la provincia")
				return false;
			}
			break;
		}
		case "SPI":
		{
			if (Obj.COD_SPI.options[Obj.COD_SPI.selectedIndex].value == "")
			{
				alert("Se la visibilt� � SPI � necessario specificare il Centro per l'impiego")
				return false;
			}
			break;
		}
		case "NONSPEC":
		{
			alert("E' necessario specificare l'area di validit� del profilo")
			return false;
			break;
		}
		
	}
}
//-->
</script>


<CENTER>

<table border=0 width='500' CELLPADDING=0 cellspacing=0>
	<tr>
		<td  class=tbltext0 bgcolor='#3399CC' align=left bordercolor="#C2E0FF" width='199'>
			<b>Area de Validaci�n de un Perfil</b>
		</td>
	<td width="25" valign='bottom' background="<%=session("progetto")%>/images/sfondo_linguetta.gif" >
		<img border="0" src="<%=session("progetto")%>/images/tondo_linguetta.gif"></td>
	<td width="278" valign='MIDDLE' align=right class=tbltext1 background="<%=session("progetto")%>/images/sfondo_linguetta.gif" >(*) campi obbligatori</td>
	</tr>
    <tr>
		<td class=tbltext1 align="left" colspan="3" bgcolor='#C6DFFF' >
		
	Definisce dove il profilo s'intende valido bla bla bla .....
		</td>
	</tr>
	<tr>
		<td colspan=3 bgcolor='#3399CC'></td>
    </tr>
</table>


<table border=0 cellspacing=2 cellpadding=1 width='500'>
<tr>
<td align=center>
<form name='AreaValidita' Action='Salva_Mod_Validita_PRJ.asp' Method='POST' OnSubmit="return valida(this)">
<input type='hidden' name='ID_VALID' value='<%=request("ID_VALID")%>'>
<input type='hidden' name='ID_FIGPROF' value='<%=request("ID_FIGPROF")%>'>

	<table border=0 width='500' cellspacing=1 cellpadding=0 >
		<tr>
			<td class=tbltext1 align=LEFT><B>Area di Validita</B></td>
			<td class=tbltext1 ><input type='radio' name='COD_VISIBILITA' value='NAZIONALE'> Nazionale</td>
			<td>&nbsp;</td>
		</tr>		
		<tr>
			<td class=tbltext1 align=LEFT><B>&nbsp;</B></td>
			<td class=tbltext1 ><input type='radio' name='COD_VISIBILITA' value='REGIONALE'> Regionale</td>
			<td>
			<%
			if isArray(REGIONI) then
				Response.Write "<select class=MY name='COD_REGIONE'>"
				Response.Write "<option value=''></option>"
				for K = lbound(REGIONI,2) to Ubound(REGIONI,2)
					Response.Write "<option value='" & REGIONI(0,K) & "'>" & REGIONI(1,K) & "</option>"
				next
				Response.Write "</select>"
			end if
			%>
			</td>
		</tr>
		<tr>
			<td class=tbltext1 align=LEFT><B>&nbsp;</B></td>
			<td class=tbltext1 ><input type='radio' name='COD_VISIBILITA' value='PROVINCIALE'> Provinciale</td>
			<td>
			<%
			if IsArray(PROVINCE) then
				Response.Write "<select class=MY name='COD_PRV'>"
				Response.Write "<option value=''></option>"
				for K = lbound(PROVINCE,2) to Ubound(PROVINCE,2)
					Response.Write "<option value='" & PROVINCE(0,K) & "'>" & PROVINCE(1,K) & "</option>"
				next
				Response.Write "</select>"
			end if
			%>
			</td>
		</tr>
		<tr>
			<td class=tbltext1 align=LEFT><B>&nbsp;</B></td>
			<td class=tbltext1 ><input type='radio' name='COD_VISIBILITA' value='SPI'> SPI</td>
			<td>
			<%
			if isArray(SPI) then
				Response.Write "<select class=MY name='COD_SPI'>"
				Response.Write "<option value=''></option>"
				for K = lbound(SPI,2) to Ubound(SPI,2)
					Response.Write "<option value='" & SPI(0,K) & "'>" & SPI(1,K) & "</option>"
				next
				Response.Write "</select>"
			end if
			%>
			</td>
		</tr>
		
	</table>

</td></tr>
<tr>
			<td align='right' ><input type='submit' Class='My' value='Salva'></td>
</tr>
    <tr>
		<td colspan=3 bgcolor='#3399CC'></td>
    </tr>
</table>
</form>
</center>
