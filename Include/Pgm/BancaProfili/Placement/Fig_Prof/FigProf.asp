<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #include Virtual="/strutt_testa2.asp" -->
<!-- #INCLUDE FILE="../Utils.asp" -->
<!--#include virtual="/include/openconn.asp"-->
<%


Sql = ""
Sql = Sql & "SELECT "
Sql = Sql & " FIGUREPROFESSIONALI.DENOMINAZIONE, FIGUREPROFESSIONALI.DESCRIZIONE, "
Sql = Sql & " FIGUREPROFESSIONALI.ID_FIGPROF, FIGUREPROFESSIONALI.ID_VALID, "
Sql = Sql & " AREE_PROFESSIONALI.DENOMINAZIONE Area, VALIDAZIONE.FL_STATOLAV ,FIGUREPROFESSIONALI.BREVE_DESC "
Sql = Sql & " FROM "
Sql = Sql & " FIGUREPROFESSIONALI, VALIDAZIONE, AREE_PROFESSIONALI "
Sql = Sql & " WHERE "
Sql = Sql & " FIGUREPROFESSIONALI.ID_VALID = VALIDAZIONE.ID_VALID "
Sql = Sql & " AND "
Sql = Sql & " FIGUREPROFESSIONALI.ID_AREAPROF = AREE_PROFESSIONALI.ID_AREAPROF "
Sql = Sql & " AND "
Sql = Sql & " VALIDAZIONE.ID_PERS_INS = " & IDP
Sql = Sql & " AND "
Sql = Sql & " VALIDAZIONE.FL_VALID <> 1"
Sql = Sql & " ORDER BY UPPER(FIGUREPROFESSIONALI.DESCRIZIONE)"

'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
Set Rs = Cc.Execute (Sql)
if not rs.eof then FIGPROF = Rs.getrows()

Sql = ""
Sql = Sql & "SELECT ID_AREAPROF, DENOMINAZIONE "
Sql = Sql & "FROM AREE_PROFESSIONALI "

'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
Set Rs = Cc.Execute (Sql)
if not rs.eof then AREEPROF = Rs.getrows()

Set Rs = Nothing
%>
<script language='Javascript'>
<!--
function valida(Obj)
{
	if ((Obj.Denominazione.value == "") || (Obj.ID_AREAPROF.value == "") || (Obj.COD_ISTAT.value == ""))
	{
		alert("Los campos:\n'Denominación' , 'Area Profesional' e 'Codigo Istat'\n son obligatorios")
		return false
	}
}

function elimina(A,B)
{
	if (confirm("Seguro desea eliminar definitivamente esta figura profesional?"))
	{
		goToPage("CancFigProf.asp?ID_FIGPROF=" + A + "&ID_VALID=" + B)
	}
}

function AbilitaValidazione(A,B,C)
{
		goToPage("AbilitaValidazione.asp?ID_FIGPROF=" + A + "&FL_STATOLAV=" + B + "&ID_VALID=" + C)
}

function ApriCodIstat(frm)
{
	URL = "CodiceIstat.asp?VISMENU=NO"
	opt = "address=no,status=no,width=650,height=240,top=150,left=15"

	window.open (URL,"",opt)
}

function addFigProf()
{
	goToPage("Add_FigProf.asp")
}

//-->
</script>

<CENTER>
<table border=0 width='500' CELLPADDING=0 cellspacing=0>
	<tr class="tbltext0">
		<td bgcolor='#3399CC'class=tbltext0 align=left >
		<b>FIGURA PROFESIONAL</b>
		</td>
		<td width="25" valign='bottom' background="<%=session("progetto")%>/images/sfondo_linguetta.gif" >
		<img border="0" src="<%=session("progetto")%>/images/tondo_linguetta.gif"></td>
		<td width="278" valign='bottom' background="<%=session("progetto")%>/images/sfondo_linguetta.gif" >&nbsp;</td>
	</tr>
</table>

<table border=0 width='500' CELLPADDING=0 cellspacing=0>
	<tr class="sfondocomm">
		<td>
		Define un oficio en termino de tareas, salida y procedimientos (o la fase del proceso) asignado al area de su  responsabilidad profesional.<br>El oficio vuelve a llamar para cada <b>Figura Profesional</b>,
		<b>Competencia</b> definen las salidas de una figura agregada.</td>
	   	<td class=sfondocomm>
	   	<a href="Javascript:Show_Help('<%=Session("Progetto")%>/HelpBP/Placement/Fig_Prof/FigProf.htm')">
		<img src="<%=Session("Progetto")%>/images/Help.gif" border='0'  align=right alt='Help'></a>
		</td>
    </tr>


<%

	Response.Write "<table width='500' border=0 cellspacing=1 cellpadding=1>"
	Response.Write "<tr>"
		Response.Write "<td align=center colspan=4><input type='button' OnClick='addFigProf()' value='Agregar Figura Profesional' class='My'></td>"
	Response.Write "</tr>"
	Response.Write "<tr><td colspan=4>&nbsp;</td></tr>"
	Response.Write "<tr class=sfondocomm>"
	Response.Write "	<td valign=top align='center' colspan='4'>"
	Response.write "		<b>Figuras Profesionales ingresadas y no validadas al momento </b>"
	Response.Write "	</td>"
	Response.Write "</tr>"
	Response.Write "<tr class=sfondocomm>"
	Response.write "	<td colspan=2><b>Descripción</b></td>"
	Response.write "	<td width='20%' colspan=2 align=right><b>Opciones&nbsp;</b></td>"
	Response.Write "</tr>"

		if IsArray(FIGPROF) then

			for I = lbound(FIGPROF,2) to Ubound(FIGPROF,2)

			Response.Write "<form name='associa' method=POST Action='DettFigProf.asp'>"
			Response.Write "<input type='hidden' name='ID_FIGPROF' value='" & FIGPROF(2,I) & "'>"
			Response.Write "<input type='hidden' name='DENOMINAZIONE' value='" & FIGPROF(0,I) & "'>"
			Response.Write "<input type='hidden' name='AREAPROF' value='" & FIGPROF(4,I) & "'>"
			Response.Write "<input type='hidden' name='BREVE_DESC' value='" & FIGPROF(6,I) & "'>"
			DescrizioneFP=FIGPROF(1,I)

				Response.Write "<tr>"
					Response.Write "<td class='tbltext1' align=center rowspan=2><b><small>" & I+1 & "</small></b></td>"
					Response.Write "<td class='tbltext1' title='Descrizione : " & trim(DescrizioneFP) & "'><b><small>" & FIGPROF(0,I) & "</small></b></td>"
					Response.Write "<td class='tbltext1' align='center'> "
						Response.Write "<input type='submit' value='Asociar' CLASS='My'>" & chr(13) & chr(10)
					Response.Write "</td>"
					Response.Write "<td class='tbltext1' align='center'> "
						Response.Write "<input type='button' value='Eliminar' OnClick='elimina(" & FIGPROF(2,I) & "," & FIGPROF(3,I) & ")' CLASS='My' >" & chr(13) & chr(10)
					Response.Write "</td>"
				Response.Write "</tr>"
				Response.Write "<tr>"
					Response.Write "<td class=tbltext1 title='Descrizione :" & trim(DescrizioneFP) & "'><small>" & FIGPROF(4,I) & "</small></td>"
					Response.Write "<td class='tbltext1' align='center'> "
						Response.Write "<input type='button' value='Ver Detalle' OnClick='goToPage(""RiepilogoFigProf.asp?ID_FIGPROF=" & FIGPROF(2,I) & "&DENOMINAZIONE=" & FIGPROF(0,I)& "&BREVE_DESC=" & FIGPROF(6,I) & "&AREAPROF=" & FIGPROF(4,I) & """)' CLASS='My' >" & chr(13) & chr(10)
					Response.Write "</td>"
					Response.Write "<td class='tbltext1' align='center'> "
						Response.Write "<input type='button' value='Validar' OnClick='AbilitaValidazione(" & FIGPROF(2,I) & "," & FIGPROF(5,I) & "," & FIGPROF(3,I) & ")' CLASS='My'><small>" & chr(13) & chr(10)
					Response.Write "</td>"
				Response.Write "</tr>"
				Response.Write "</form>"
	 			Response.Write "<tr>"
					Response.Write "<td colspan=4 bgcolor='#3399CC'></td>"
    			Response.Write "</tr>"
			next
			Response.Write "<tr><td colspan=4>&nbsp;</td></tr>"
			Response.Write "<tr>"
				Response.Write "<td align=center colspan=4><input type='button' OnClick='addFigProf()' value='Agregar Figura Profesional' class='My'></td>"
			Response.Write "</tr>"

	else
			Response.Write "<tr>"
				Response.Write "<td align=center colspan=4>"
				Response.Write "<p align=center class='tbltext3'> &nbsp;&nbsp;No hay elementos a visualizar ...</P></td>"
			Response.Write "</tr>"

	end if
%>
</table>
<!-- #include Virtual="/strutt_coda2.asp" -->
<!--#include virtual="/include/closeconn.asp"-->
