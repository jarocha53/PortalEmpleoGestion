<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #include Virtual="/strutt_testa2.asp" -->
<!-- #INCLUDE FILE="../Utils.asp" -->
<!--#include virtual="/include/openconn.asp"-->

<%


Sql = ""
Sql = Sql & "SELECT "
Sql = Sql & "COMPORTAMENTI.DENOMINAZIONE,  "
Sql = Sql & "COMPORTAMENTI.ID_COMPORTAMENTO, COMPORTAMENTI.ID_VALID, "
Sql = Sql & "AREA_COMPORTAMENTI.DENOMINAZIONE Area,  AREA_COMPORTAMENTI.ID_AREACOMPORTAMENTO IdArea "
Sql = Sql & "FROM "
Sql = Sql & "COMPORTAMENTI, VALIDAZIONE, AREA_COMPORTAMENTI "
Sql = Sql & "WHERE "
Sql = Sql & "COMPORTAMENTI.ID_VALID = VALIDAZIONE.ID_VALID "
Sql = Sql & "AND "
Sql = Sql & "COMPORTAMENTI.ID_AREACOMPORTAMENTO = AREA_COMPORTAMENTI.ID_AREACOMPORTAMENTO "
'Sql = Sql & "AND "
'Sql = Sql & "VALIDAZIONE.ID_PERS_INS = " & IDP
Sql = Sql & " AND "
Sql = Sql & "VALIDAZIONE.FL_VALID <> 1 "
Sql = Sql & "ORDER BY UPPER(AREA_COMPORTAMENTI.ID_AREACOMPORTAMENTO)"


'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
Set Rs = Cc.Execute (Sql)
if not rs.eof then COMPOR = Rs.getrows()

%>


<script language='Javascript'>
<!--
function valida(Obj)
{
	if ((Obj.Denominazione.value == "") || (Obj.ID_AREACOMPORTAMENTO.options[Obj.ID_AREACOMPORTAMENTO.selectedIndex].value == ""))
	{
		alert("Los campos:\n'Denominación' y 'Area Comportamiento'\nson obligatorios")
		return false
	}
}
function addComportamento()
{
	goToPage("Add_Comportamenti.asp")
}

function elimina(A,B)
{
	if (confirm("Seguro desea eliminar definitivamente esta competencia?"))
	{
		goToPage("CancCompor.asp?ID_COMPORTAMENTO=" + A + "&ID_VALID=" + B)
	}
}
//-->
</script>

<table border=0 width='500' CELLPADDING=0 cellspacing=0>
	<tr class="tbltext0">
		<td bgcolor='#3399CC'class=tbltext0 align=left >
		<b>COMPORTAMIENTO</b>
		</td>
		<td width="25" valign='bottom' background="<%=session("progetto")%>/images/sfondo_linguetta.gif" >
		<img border="0" src="<%=session("progetto")%>/images/tondo_linguetta.gif"></td>
		<td width="278" valign='bottom' background="<%=session("progetto")%>/images/sfondo_linguetta.gif" >&nbsp;</td>
	</tr>
</table>
<table border=0 width='500' CELLPADDING=0 cellspacing=0>
	<tr class="sfondocomm">
		<td>
		Es uno de los elementos de la <b>Competencia</b> (juntamente con el conocimiento y el desempeño).
		Conjuga dotes y aptitudes personales con la necesidad expresada por la organización y
		por las personas clientes/usuarios. Implica estar en condiciones de comunicar, operar,
		interactuar, etc. En coherencia con un contexto ambiental específico 
		y con sus valores de referencia. Se relaciona con el saber ser.
		</td>
	   	<td class=sfondocomm>
	   	<a href="Javascript:Show_Help('<%=Session("Progetto")%>/HelpBP/Placement/Comportamenti/Comportamenti.htm')">
		<img src="<%=Session("Progetto")%>/images/Help.gif" border='0'  align=right alt='Help'></a>
		</td>
    </tr>


<%

	Response.Write "<table width='500' border=0 cellspacing=1 cellpadding=1>"
		Response.Write "<tr>"
		Response.Write "<td align=center colspan=3><input type='button' OnClick='addComportamento()' value='Agregar Comportamiento' class='My' id='button'1 name='button'1><br>&nbsp;</td>"
		Response.Write "</tr>"
		Response.Write "<tr class=sfondocomm>"
		Response.Write "<td valign=top width='500' align='center' colspan='3'>"
		Response.write "<b>Comportamientos ingresados y no validados al momento </b>"
		Response.Write "</td>"
		Response.Write "</tr>"
		Response.Write "<tr class=sfondocomm>"
		Response.write "<td colspan='2'><b>Descripción</b></td>"
		Response.write "<td align=right><b>Opciones</b></td>"
		Response.Write "</tr>"

if IsArray(COMPOR) then
	Response.Write "<table width='500' border=0 cellspacing=0 cellpadding=1>"
	OldIdArea = -1
	for I = lbound(COMPOR,2) to Ubound(COMPOR,2)
		Response.Write "<tr>"
		Response.Write " <td colspan=3 bgcolor='#3399CC'></td>"
		Response.Write "</tr>"
		Response.Write "<form name='associa' method=POST>"
		Response.Write "<input type='hidden' name='ID_COMPORTAMENTO' value='" & COMPOR(1,I) & "'>"
		Response.Write "<input type='hidden' name='DENOMINAZIONE' value='" & COMPOR(0,I) & "'>"
		Response.Write "<input type='hidden' name='AREACOMPOR' value='" & COMPOR(3,I) & "'>"
		if OldIdArea <> int(0 & COMPOR(4,I)) then
			Response.Write "<tr>"
			Response.Write "<td  class=tbltext1 " & bg2 & " colspan='3'><small>" & COMPOR(3,I) & "</small></td>"
			Response.Write "</tr>"
		end if
		Response.Write "<tr>"
			Response.Write "<td class=tbltext1  Width='10%'>" & I+1 & " </td>"
			Response.Write "<td class=tbltext1  Width='85%'><b>" & COMPOR(0,I) & "</b></td>"
			Response.Write "<td  class=tbltext1  Width='5%'> "
			Response.Write "<input type='button' value='Eliminar' OnClick='elimina(" & COMPOR(1,I) & "," & COMPOR(2,I) & ")' CLASS='My'>" & chr(13) & chr(10)
			Response.Write "</td>"
		Response.Write "</tr>"
	Response.Write "</form>"
	OldIdArea = int( 0 & COMPOR(4,I))
	next
%>
    <tr>
		<td colspan=3 bgcolor='#3399CC'></td>
    </tr>
<%
	Response.Write "<tr>"
		Response.Write "<td align=center colspan=3><br><input type='button' OnClick='addComportamento()' value='Agregar Comportamiento' class='My' id='button'1 name='button'1></td>"
else
	Response.Write "<td colspan=3><p align=center class='tbltext3'>&nbsp;&nbsp;No se encontraron elementos para visualizar...</p></td>"
end if
Response.Write "</tr>"
Response.Write "</table>"
%>
</td>
</tr>
</table>

<!-- #include Virtual="/strutt_coda2.asp" -->
<!--#include virtual="/include/closeconn.asp"-->
