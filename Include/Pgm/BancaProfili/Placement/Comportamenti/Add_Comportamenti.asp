<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #include Virtual="/strutt_testa2.asp" -->
<!-- #INCLUDE FILE="../Utils.asp" -->

<%

Set Conn = CC
'Set Conn = server.CreateObject ("ADODB.Connection")
'Conn.open strConn

Sql = ""
Sql = Sql & "SELECT ID_AREACOMPORTAMENTO, DENOMINAZIONE "
Sql = Sql & "FROM AREA_COMPORTAMENTI ORDER BY UPPER(DENOMINAZIONE)"

'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
Set Rs = Conn.Execute (Sql)
if not rs.eof then AREACOMPOR = Rs.getrows()

Set Rs = Nothing
Conn.Close
Set conn = Nothing
%>


<script language='Javascript'>
<!--
function valida(Obj)
{
	if ((Obj.Denominazione.value == "") || (Obj.ID_AREACOMPORTAMENTO.options[Obj.ID_AREACOMPORTAMENTO.selectedIndex].value == ""))
	{
		alert("Los campos:\n'Denominación' y 'Area de comportamiento'\nson obligatorios")
		return false
	}
}
//-->
</script>

<center>


<table border=0 width='500' CELLPADDING=0 cellspacing=0>
	<tr class="tbltext0">
		<td bgcolor='#3399CC'class=tbltext0 align=left >
		<b>COMPORTAMIENTO</b>
		</td>
		<td width="25" valign='bottom' background="<%=session("progetto")%>/images/sfondo_linguetta.gif" >
		<img border="0" src="<%=session("progetto")%>/images/tondo_linguetta.gif"></td>
		<td width="278" valign='MIDDLE' align=right class=tbltext1 background="<%=session("progetto")%>/images/sfondo_linguetta.gif" >(*) campo obligatorio</td>
	</tr>
</table>
<table border=0 width='500' CELLPADDING=0 cellspacing=0>
	<tr class="sfondocomm">
		<td>
		Es uno de los elementos de la <b>Competencia</b> (juntamente con el conocimiento y el desempeño).
		Conjuga dotes y aptitudes personales con la necesidad expresada por la organización y
		por las personas clientes/usuarios. Implica estar en condiciones de comunicar, operar,
		interactuar, etc. En coherencia con un contexto ambiental específico 
		y con sus valores de referencia. Se relaciona con el saber ser.
		</td>
	   	<td class=sfondocomm>
	   	<a href="Javascript:Show_Help('<%=Session("Progetto")%>/HelpBP/Placement/Comportamenti/Add_Comportamenti.htm')">
		<img src="<%=Session("Progetto")%>/images/Help.gif" border='0'  align=right alt='Help'></a>
		</td>
    </tr>
    <tr>
		<td colspan=2 bgcolor='#3399CC'></td>
    </tr>
</table>

<table border=0 cellspacing=1 cellpadding=1 width='500'>
<tr>
<td align=center>
<form name='FigureProfessionali' Action='SalvaComportamenti.asp' Method='POST' OnSubmit='return valida(this)'>
<input type="hidden" name="IdPers" value="<%=sIdPers%>">
<input type="hidden" name="IND_FASE" value="<%=IND_FASE%>">
<input type='hidden' name='VISMENU' value='NO'>
	<table border=0 width='500' cellspacing=1 cellpadding=1>

		<tr>
			<td  class=tbltext1  align=LEFT><b>Denominación*&nbsp;</td>
			<td  class=tbltext1><input type='text' class='MyTextBox' maxlength='60' size='49' name='Denominazione' value=''></td>
		</tr>
		<tr>
			<td class=tbltext1 align=LEFT><b>Area Comportamiento*&nbsp;</td>
			<td>
			<%
			Response.Write "<select CLASS='MY' name='ID_AREACOMPORTAMENTO'>"
			Response.Write "<option value='' > </option>"
			for K = lbound(AREACOMPOR,2) to Ubound(AREACOMPOR,2)
				Response.Write "<option value='" & AREACOMPOR(0,K) & "'>" & AREACOMPOR(1,K) & "</option>"
			next
			Response.Write "</select>"
			%>
			</td>
		</tr>
		<tr>
			<td align=left>
				<input type='Button' value='Volver' CLASS='My' OnClick='goToPage("Comportamenti.asp")'>
			</td>

			<td align=right>
				<input type='Submit' value='Guardar' CLASS='My'>
			</td>
    <tr>
	 <td colspan=3 bgcolor='#3399CC'></td>
      </tr>
		</tr>
	</table>
</form>
</td>
</tr>
</table>

<!-- #include Virtual="/strutt_coda2.asp" -->
