<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #INCLUDE FILE="../Utils.asp" -->
<!--#include virtual="/include/openconn.asp"-->

<%


Sql = ""
Sql = Sql & "SELECT VERBI.ID_VERBO, VERBI.DENOMINAZIONE "
Sql = Sql & "FROM VERBI "
Sql = Sql & "ORDER BY UPPER(VERBI.DENOMINAZIONE)"

'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
Set Rs = Cc.Execute (Sql)
if not rs.eof then VERBI = Rs.getrows()

Set Rs = Nothing

%>

<script language='Javascript'>
<!--

function valida(Obj)
{
	if ((Obj.Attivita.value == "") || (Obj.Output.value == "") || (Obj.ID_VERBO.value == "") || (Obj.Denominazione.value == ""))
	{
		alert("Los campos: 'Actividad', 'Salida' , 'Nivel' y 'Denominación' son Obligatorios")
		return false
	}
}


	function proponi(frm)
	{
		frm.Denominazione.value = frm.ID_VERBO.options[frm.ID_VERBO.selectedIndex].title + " " + frm.Output.value
	}

	function verbi(frm)
	{
		URL = "Verbi.asp?"
		URL = URL + "ATTIVITA=" + frm.Attivita.value + "&"
		URL = URL + "OUTPUT=" + frm.Output.value + "&"
		URL = URL + "DESCRIZIONE=" + frm.Note.value + "&VISMENU=NO"

		opt = "address=no,status=no,width=520,height=200,top=150,left=35"

		window.open (URL,"",opt)
	}

	function FocusCompet(ID_COMPETENZA)
	{
		URL = "FocusCompet.asp?"
		URL = URL + "ID_COMPETENZA=" + ID_COMPETENZA + "&VISMENU=NO"

		opt = "address=no,status=no,width=600,height=500,top=150,left=35,scrollbars=yes"

		window.open (URL,"",opt)
	}

function elimina(A,B)
{
	if (confirm("Esta Seguro que desea eliminar esta competencia?"))
	{
		goToPage("CancCompet.asp?ID_COMPETENZA=" + A + "&ID_VALID=" + B)
	}
}
//-->
</script>


<title>Detalle de la Figura Profesional</title>

<LINK REL=STYLESHEET TYPE="text/css" HREF='<%=session("progetto")%>/fogliostile.css'>

<body class=sfondocentro topmargin="10" leftmargin="0">
<center>

<table border=0 width='500' CELLPADDING=0 cellspacing=0>
	<tr class="tbltext0">
		<td bgcolor='#3399CC'class=tbltext0 align=left >
		<b>DUPLICAR ELEMENTO DE COMPETENCIA</b>
		</td>
		<td width="25" valign='bottom' background="<%=session("progetto")%>/images/sfondo_linguetta.gif" >
		<img border="0" src="<%=session("progetto")%>/images/tondo_linguetta.gif"></td>
<td width="278" valign='MIDDLE' align=right class=tbltext1 background="<%=session("progetto")%>/images/sfondo_linguetta.gif" >(*) campi obbligatori</td>	</tr>
</table>
<table border=0 width='500' CELLPADDING=0 cellspacing=0>
	<tr class="sfondocomm">
		<td>
		En esta página puede duplicar el <b>Elemento de Competencia</b> del que ha visualizado el detalle.
		</td>
	   	<td class=sfondocomm>
	   	<a href="Javascript:Show_Help('<%=Session("Progetto")%>/HelpBP/Placement/Ricerche/Duplica_Compet.htm')">
		<img src="<%=Session("Progetto")%>/images/Help.gif" border='0'  align=right alt='Help'></a>
		</td>
    </tr> 
    <tr>
		<td colspan=2 bgcolor='#3399CC'></td>
    </tr>
</table>

<form name='indietro' Action='SalvaDuplica_Compet.asp' method="POST" OnSubmit="return valida(this)">

<table border=0 cellspacing=1 cellpadding=1 width='500'>
<tr>
<td align=center>

	<table border=0 width='500' cellspacing=1 cellpadding=0>
	</font></td></tr>
		<tr>
			<td class='tbltext1' align=LEFT><b>Actividad*&nbsp;</td>
			<td bgcolor='#FFFFFF'><input type='text' class='MyTextBox' maxlength='60' size='57' name='Attivita' value='<%=request("ATTIVITA")%>'></td>
		</tr>
		<tr>
			<td class='tbltext1' align=LEFT><b>Salida*&nbsp;</td>
			<td bgcolor='#FFFFFF'><input type='text' maxlength='80' class='MyTextBox'  size='57' name='Output' value='<%=request("OUTPUT")%>'></td>
		</tr>
		<tr>
			<td class='tbltext1' align=LEFT><b>Grado*&nbsp;</td>
			<td bgcolor='#FFFFFF'>
			<%
			if isArray(VERBI) then
			Response.Write "<select class='MY' Name='ID_VERBO'>"
			Response.Write "<option  value=''></option>"
			for I = lbound(VERBI,2) to Ubound(VERBI,2)
				Response.Write "<option "
				if VERBI(1,I) = request("VERBO") then Response.Write " selected "
				Response.write " title='" & VERBI(1,I) & "' value='" & VERBI(0,I) & "'>" & VERBI(1,I) & "</option>"
			next
			Response.Write "</select>"
			end if
			%>
			<input type='button' value='Nueva Palabra' Class='My' OnClick='verbi(this.form)' id='button'1 name='button'1>
			</td>
		</tr>
		<tr>
			<td class='tbltext1' align=LEFT><b>Denominación*&nbsp;</td>
			<td bgcolor='#FFFFFF'><input OnFocus='proponi(this.form)' type='text' name='Denominazione' maxlength='100' size='57' value=''></td>
		</tr>
		<tr>
			<td class='tbltext1' align=LEFT><b>Descripción&nbsp;</td>
			<td bgcolor='#FFFFFF'><TEXTAREA cols='42'  rows='3' class='MyTextBox' name='Note'><%=request("DESCRIZIONE")%></TEXTAREA></td>
		</tr>
		<tr>
			<td bgcolor='#FFFFFF' colspan=2 align=right>
				<input type='Submit' value='Guardar' CLASS='My'>
			</td>
		</tr>
		<tr>
		<td colspan=3 bgcolor='#3399CC'></td>
    </tr>
	</table>
</td>
</tr>
</table>
</center>
	
	<input type="hidden" value="<%=request("lst_ID_CONOSCENZA")%>" name="lst_ID_CONOSCENZA">
	<input type="hidden" value="<%=request("lst_ID_CAPACITA")%>" name="lst_ID_CAPACITA">
	<input type='hidden' name='VISMENU' value='NO'>
</form>
</div>
<!--#include virtual="/include/closeconn.asp"-->
