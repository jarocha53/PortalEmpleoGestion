<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #include virtual="/util/portallib.asp"-->
<!--#include virtual="/include/openconn.asp"-->
<!--#include Virtual="/strutt_testa3.asp"-->

<%
if ValidateService(session("idutente"),"com_selprov",cc) <> "true" then 
	response.redirect "/util/error_login.asp"
end if
%>
<script LANGUAGE="Javascript">
<!--#include virtual = "/Include/help.inc"-->

function InviaComune(){
	appoComune=frmComune.cmbComune.value
	if (appoComune == ""){
		alert("Selezionare il Comune")
	}else
		frmComune.submit();
	}

//Funzione per i controlli dei campi da modificare 
	function ControllaDati(frmRicComune){
		//Comune obbligatorio
		if ((frmRicComune.txtComune.value == "")||
			(frmRicComune.txtComune.value == " ")){
			alert("Comune obbligatorio")
			frmRicComune.txtComune.focus() 
			return false
		}
	return true
	}
</script>

<body>
<table border="0" width="520" cellspacing="0" cellpadding="0" height="81">
   <tr>
     <td width="500" background="<%=Session("Progetto")%>/images/titoli/strumenti2b.gif" height="81" valign="bottom" align="right">
       <table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
         <tr>
           <td width="100%" valign="top" align="right"><b class="tbltext1a">Gestione Comuni</span></b></td>
         </tr>
       </table>
     </td>
   </tr>
</table><br>
<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
<tr height="17">

	<td class="sfondomenu" width="67%" height="18"><span class="tbltext0">
		<b>&nbsp;Gestione Tabella Comune </b>
	</td>
	<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">
	</td>
	<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
</tr>
<tr>
	<td class="sfondocomm" width="57%" colspan="3">
		<br>Selezionare uno dei comuni ritrovati.
		<a href="Javascript:Show_Help('/pgm/help/Comuni/COM_SelComune')"><img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
	</td>
</tr>
<tr height="2">
	<td class="sfondocomm" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif">
	</td>
</tr>
</table>
<br>

<%
sModo = Request("MOD")

'dim sMask
sMask = GetSectionVar("COM_SELPROV","MASK") 

'Response.Write "MASK=" & sMask
'Titolo della pagina
'Response.Write "<table width=500 border=0 cellspacing=2 cellpadding=1>"
'Response.Write "<tr>"
'Response.Write "<td colspan=2 align=middle background=../../images/sfmnsmall.jpg>"
'Response.Write "<b>"
'Response.Write "<span class='titolo'>"
'Response.Write "Gestione Tabella Comune"
'Response.Write "</span>"
'R'esponse.Write "</b>"
'Response.Write "</td>"
'Response.Write "</tr>"
'Response.Write "</table>"
'Response.Write "<BR>"

select case sModo
	'E' la prima volta che entro, inputbox Comune
	case 0
%>	
   <br>
	<table width="210" align="center" border="0" cellspacing="1" cellpadding="0">
	 <tr>
	  <td valign="top" colspan="4">
	   <span class="tbltext1">
		<b>
		 <center>Ricerca un Comune</center>
		 </b>
		</span>
	   </td>
	  </tr>
	 </table>
	 
		
  <form method="POST" action="COM_SelComune.asp?MOD=1" name="frmRicComune" onsubmit="return ControllaDati(this)">
	<table width="210" align="center" border="0" cellspacing="1" cellpadding="0">
	 <tr>
	  <td align="left" colspan="2" width="60%">
	   <p align="center">
		<input style="TEXT-TRANSFORM:uppercase" name="txtComune" size="22" maxlength="35">
		</p>
	   </td>
	  </tr>
	 </table>
	 <br>
	 
	 <table width="210" border="0" cellspacing="1" cellpadding="0" align="center">
	  <tr>
	   <td align="left" colspan="2" width="60%">
		<p align="left">
		 <input type="reset" name="Annulla" value="Annulla">&nbsp;
		  <input type="submit" name="Invia" value="Conferma">
		  </p>
		 </td>
		</tr>
	   </table>
	  </form>
<%	  
	'E' la seconda volta che entro, combo COMUNI
	case 1
		sComuneRic = request("txtComune")
		sComuneRic = UCase(sComuneRic)
		sComune = Replace(sComuneRic,"'","''")
%>		
<!--	<BR>	<table width=500 border=0 cellspacing=2 cellpadding=1>	 <tr>	  <td>	   <center>		<b>		 <span class=tbltext1>		Comune richiesto : <%=sComuneRic%>		 </span>		</b>	   </center>	  </td>	 </tr>	</table> --> 		
    <table width="500" border="0" cellspacing="1" cellpadding="0">
	 <tr>
	  <td align="center" colspan="2" width="60%">
	   <p align="center">
<%	   
		    dim rsComuni
		   sSQL = "SELECT CodCom, DesCom from COMUNE WHERE DesCom like '" & sComune & "%' ORDER BY descom"

'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
		    set rsComuni = CC.Execute(sSQL)
					
		    if rsComuni.eof then
			'qui ci si entra solo nel caso in cui non ci sono COMUNI 
			'per la provincia selezionata
%>			
	 <br>
	 <table border="0" cellpadding="0" cellspacing="0" width="500">
	  <tr>
	   <td align="center" colspan="0" width="60%">
	    <span class="tbltext">&nbsp;
		</span>
	   </td>
	  </tr>
	  <tr>
	   <td align="center" colspan="0" width="60%">
		<span class="tbltext3">
			Non esistono Comuni corrispondenti ai dati di ricerca inseriti 
	    </span>
	   </td>
	  </tr>
	  <tr>
	   <td align="center" colspan="0" width="60%">
		<span class="tbltext">&nbsp;
		</span>
	   </td>
	  </tr>
	  <tr>
	   <td align="center" colspan="0" width="60%">
			<a href="COM_SelProv.asp"><br>
				<img src="<%=Session("progetto")%>/images/indietro.gif" border="0">
			</a>
	   </td>
	  </tr>
	 </table>
	 <br>
<%	 
		else
			'Ho pi� comuni quindi creo il combo
%>			
		<table width="500" border="0">
		 <tr>
		  <td align="center" width="250">
		   <a href="COM_InsComune.asp">
			<b>
			 <span class="textRed">
			  <center>
			Inserisci un nuovo Comune
			  </center>
			 </font>
			</b>
		   </a>
		  </td>

		  <td align="center" width="250">
		   <a href="COM_SelProv.asp">
			<b>
			 <span class="textRed">
			  <center>
			Torna all'inizio
			  </center>
			 </font>
			</b>
		   </a>
		  </td>

         </tr>
		</table>


	  <br>
	  <form method="POST" action="COM_VisComune.asp" name="frmComune">
	  <table width="500" border="0" cellspacing="1" cellpadding="0">
	   <tr>
		<td align="left" width="180">
		 <span class="tbltext1">
		  <b>
		Selezionare un Comune :
		  </b>
		 </span>
		</td>
		<td align="left">
	   <select class="textblacka" ID="cmbComune" name="cmbComune" onchange="InviaComune()"><option value></option>
	   
<%	   
			i = 0
			do while not rsComuni.EOF
				i = i + 1
				Response.Write "<OPTION value ='" & rsComuni("codcom") &_
					 "'> " & rsComuni("descom")  & "</OPTION>"
				sCod = rsComuni("codcom")
				rsComuni.MoveNext
			loop
			
			Response.Write "</SELECT>"
						
			rsComuni.Close
			%><input type="hidden" name="NumVal" value="<%=i%>">
			<%if i = 1 then%>
				<script>
					frmComune.cmbComune.value = frmComune.cmbComune.options[1].value
					frmComune.submit()
				</script>
						
			<%end if%>
	
		<%end if%>
		</td>
		</tr>
		</table>
		</form>
		</p>
		</td>
		</tr>
		</table></table>
		
<%		
end select
%>
<!--#include file ="../../include/closeconn.asp"-->
<!--#include Virtual="/strutt_coda2.asp"-->
