<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include Virtual="/strutt_testa2.asp"-->
<!--#include virtual ="/include/openconn.asp"-->
<!-- #include virtual="/util/portallib.asp"-->
<!--#include Virtual="/include/DecCod.asp"-->
<!--#include Virtual="/include/SysFunction.asp"-->
<!--#include Virtual="/include/ckProfile.asp"-->
<%
if ValidateService(session("idutente"),"com_selprov",cc) <> "true" then 
	response.redirect "/util/error_login.asp"
end if
%>

<script LANGUAGE="javascript">

function Indietro(){
	history.back() 	
}

</script>
<body>
<table border="0" width="520" cellspacing="0" cellpadding="0" height="81">
   <tr>
     <td width="500" background="<%=Session("Progetto")%>/images/titoli/strumenti2b.gif" height="81" valign="bottom" align="right">
       <table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
         <tr>
           <td width="100%" valign="top" align="right"><b class="tbltext1a">Gestión de Municipios</span></b></td>
         </tr>
       </table>
     </td>
   </tr>
</table><br>
<br>


<%

sAction = Request("ACTION")

'Prelevo dalla pagina precedente i dati digitati dall'utente
sDesc = Request.Form ("txtDesc")
sDesc = Replace(sDesc,"'","''")
sProv = Request.Form ("cmbProv")
sCap  = Request.Form ("txtCap")
sPref = Request.Form ("txtPref")
sComunita = Request.Form ("radComunita")

if sProv = "" or sProv = "EE" or sProv = "EX" then
   sProv = sComunita
end if   

'Trasformo i campi di testo in MAIUSCOLO
sDesc = UCase(sDesc)

select case sAction
	case "MOD"
		'Prelevo dalla pagina precedente il codice del comune che si sta modificando
		sCodComune = Request ("CodComune")

		'Ricavo la descrizione del comune che si vuole modificare dalla tabella COMUNE
		dim rsDescComune

		sSQL = "SELECT DesCom from COMUNE WHERE CODCOM<>'" & sCodComune &_
		"' AND DESCOM = '" & sDesc & "' AND PRV='" & sProv & "'"

'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
		set rsDescComune = CC.Execute(sSQL)

'		sDescCom = rsDescComune("Descom")
		
		if rsDescComune.EOF then

			'imposto il titolo della pagina
			CC.beginTrans

			'faccio l'UPDATE su db per il comune selezionato
			sSQL = "UPDATE COMUNE SET " & _
					"DesCom ='" & sDesc & "', " & _
					"Prv ='" & sProv & "', " & _
					"Cap ='" & sCap & "', " & _
					"Prefisso ='" & sPref & "' " & _
					"WHERE CodCom ='" & sCodComune & "'"

			Errore=EseguiNoC(sSQL,CC)
		    
			if Errore = "0" then
				CC.CommitTrans
				Response.Write "<br><br>"
				Response.Write "<table border=0 cellspacing=2 cellpadding=1 width='500' >"
				Response.Write "<tr align=middle>"
				%>
				<form name="frmIndietro1" action="COM_SelComune.asp" method="post">
				       <script>
					     alert("Modificación del municipio correctamente efectuada")
					     frmIndietro1.submit();
				    </script>
			    </form>
				<%
				
	
				Response.Write "</tr>"
				Response.Write "</table>"
			else
				CC.RollbackTrans
				Response.Write "<br><br>"
				Response.Write "<table border=0 cellspacing=2 cellpadding=1 width='500' >"
				Response.Write "<tr align=middle>"
				Response.Write "<td class='tbltext3'>"
				Response.Write "<span class='size'>"
				Response.Write "Modificación de municipio no efectuada"
				Response.Write "</span>"
				Response.Write "</td>"
				Response.Write "</tr>"
				Response.Write "<tr align=middle>"
				Response.Write "<td	class='tbltext3'>"
				Response.Write "<span class='size'>"
				Response.Write "(" & Errore & ")"
				Response.Write "</span>"
				Response.Write "</td>"
				Response.Write "</tr>"
				Response.Write "</table>"
				Response.Write "<br><br>"
				Response.Write "<table width='500'>"
				Response.Write "<tr>" 
				Response.Write "<td align=left>" 
				Response.Write "<b>"
				Response.Write "<span class='tbltext'>"
				Response.Write "<CENTER>"
				Response.Write "<A href='JAVASCRIPT:history.back()'>"
				Response.Write "<img src=""/images/indietro.gif"" border=0>"
				Response.Write "</A>"
				Response.Write "</CENTER>"
				Response.Write "</span></b></A>"
				Response.Write "</td>"
				Response.Write "</tr>"
				Response.Write "</table>"
			end if 
		else
				Response.Write "<br><br>"
				Response.Write "<table border=0 cellspacing=2 cellpadding=1 width='500'>"
				Response.Write "<tr align=middle>"
				Response.Write "<td class='tbltext3'>"
				Response.Write "<span class='size'>"
				Response.Write "El municipio indicado ya esta presente en la departamento"
				Response.Write "</span>"
				Response.Write "</td>"
				Response.Write "</tr>"
				Response.Write "</table>"
%>
				<br><br>
				<table width="500">
				<tr> 
				<td align="left">
				<b>
				<span class="tbltext">
				<center>
				<a href="JAVASCRIPT:history.back()">
				<img src="<%=Session("Progetto")%>/images/indietro.gif" border="0">
				</a>
				</center>
				</span></b></a>
				</td>
				</tr>
				</table>
<%		end if 
		rsDescComune.Close
		
	case "INS"
		sCodCom = Request.Form ("txtCodice")		
		sCodCom = UCase(sCodCom)
		
		
		'Controllo che non esista gia' in tabella un comune con quel codice
		dim sContatore
		dim rsCodCom
		
		sContatore = 0

		sSQL = "SELECT COUNT(*) AS Count from COMUNE WHERE CODCOM='" & sCodCom & "'"
						
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
		set rsCodCom = CC.Execute(sSQL)
	
		sContatore = cint(rsCodCom("Count"))
		
		rsCodCom.close
		
		if sContatore <> 0 then
			'ERRORE
			Response.Write "<br><br>"
			Response.Write "<table border=0 cellspacing=2 cellpadding=1 width='500'>"
			Response.Write "<tr align=middle>"
			Response.Write "<td class='tbltext3'>"
			Response.Write "<span class='size'>"
			Response.Write "Ingreso no efectuado "
			Response.Write "<BR>el codigo del municipio ya existe "
			Response.Write "</td>"
			Response.Write "</tr>"
			Response.Write "</table>"
			Response.Write "<BR>"
%>
			<table width="500">
			<tr> 
			<td align="left"> 
			<b>
			<span class="tbltext">
			<center>
			<a href="JAVASCRIPT:history.back()">
			<img src="<%=Session("Progetto")%>/images/indietro.gif" border="0">
			</a>
			</center>
			</span></b></a>
			</td>
			</tr>
			</table>
<%
		else
	
			sCodProv = sProv
			'controllo che non esista un'altro record con la stessa provincia e comune
			dim sContaRec
			dim rsRecCom
		
			sContaRec = 0

			sSQL = "SELECT COUNT(*) AS Count from COMUNE WHERE PRV='" & sCodProv & "' and DESCOM='" & sDesc & "'"
							
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
			set rsRecCom = CC.Execute(sSQL)
			sContaRec = cint(rsRecCom("Count"))
		
			rsRecCom.close
			
			if sContaRec <> 0 then %>
				<br><br>
				<table border="0" cellspacing="2" cellpadding="1" width="500">
				<tr align="middle">
				<td class="tbltext3">
				El ingreso del municipio no puedo efectuarse:<br>
				ya existe para el departamento
				</td>
				</tr>
				</table>
				<br>
				<table width="500">
				<tr> 
				<td align="left"> 
				<b>
				<span class="tbltext">
				<center>
				<a href="JAVASCRIPT:history.back()">
				<img src="<%=Session("Progetto")%>/images/indietro.gif" border="0"> 
				</a>
				</center>
				</span></b></a>
				</td>
				</tr>
				</table>
<%			else
				'I controlli sono andati bene, quindi proseguo con l'INSERT
				CC.beginTrans

				'faccio l'INSERT su db
				sSQL = "INSERT INTO COMUNE " & _
						"(CODCOM," &_
						"DESCOM," &_
						"PRV," &_
						"CAP," &_
						"PREFISSO) VALUES " &_	
						"('" & sCodCom  & _
						"','" & sDesc & _
						"','" & sCodProv & _
						"','" & sCap & _
						"','" & sPref & "')"
						
				Errore=EseguiNoC(sSQL,CC)
				
				
				if Errore = "0" then
					CC.CommitTrans
					Response.Write "<br><br>"
					Response.Write "<table border=0 cellspacing=2 cellpadding=1 width='500' >"
					Response.Write "<tr align=middle>"
					%>
					<form name="frmIndietro" action="COM_SelComune.asp" method="post">
				       <script>
					     alert("Inserimento del Comune correttamente effettuato")
					     frmIndietro.submit();
						</script>
			        </form>
					<%
					Response.Write "</tr>"
					Response.Write "</table>"
					
				else %>
					CC.RollbackTrans
					<br><br>
					<table border="0" cellspacing="2" cellpadding="1" width="500">
					<tr align="middle">
					<td class="tbltext3">
					<span class="size">
					Inserimento del Comune non effettuabile
					</span>
					</td>
					</tr>
					<tr align="middle"><td class="tbltext3">  
					</td>
					</tr>
					</table>
					<br><br>
					<table width="500">
					<tr> 
					<td align="left"> 
					<b>
					<span class="tbltext">
					<center>
					<a href="JAVASCRIPT:history.back()">
					<img src="<%=Session("Progetto")%>/images/indietro.gif" border="0">
					</a>
					</center>
					</span></b></a>
					</td>
					</tr>
					</table>
		<%		end if
			end if
		end if
end select
%>
<!--#include Virtual="/include/closeconn.asp"-->
<!--#include Virtual="/strutt_coda2.asp"-->
