<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #include virtual="/include/openconn.asp"-->
<!-- #include virtual="/util/portallib.asp"-->
<!--#include virtual="/include/DecCod.asp"-->
<!--#include virtual="/include/ControlDateVB.asp"-->
<!--#include Virtual="/strutt_testa3.asp"-->
<%
if ValidateService(session("idutente"),"com_selprov",cc) <> "true" then 
	response.redirect "/util/error_login.asp"
end if
%>
<script LANGUAGE="Javascript">
//include del file per fare i controlli sulla numericit� dei campi
<!--#include virtual="/Include/ControlNum.inc"-->
<!--#include virtual="/Include/ControlString.inc"-->
<!--#include virtual = "/Include/help.inc"-->

//Funzione per i controlli dei campi da inserire 
	function ControllaDati(frmInsComune){
	//-------- Controlli di OBBLIGATORIETA' --------
	//-------- Controlli FORMALI            --------
	//-------- Controlli di RELAZIONE       --------
		//Codice obbligatorio
		frmInsComune.txtCodice.value = TRIM(frmInsComune.txtCodice.value)
		if ((frmInsComune.txtCodice.value == "")||
			(frmInsComune.txtCodice.value == " ")){
			alert("Codice del Comune obbligatorio")
			frmInsComune.txtCodice.focus() 
			return false
		}
		//Codice obbligatoriamente lungo 4 byte
		if (frmInsComune.txtCodice.value != ""){
			if (frmInsComune.txtCodice.value.length != 4){
				alert("Il Codice del Comune deve essere composto da 4 caratteri")
				frmInsComune.txtCodice.focus() 
				return false
			}
		}
		frmInsComune.txtDesc.value = TRIM(frmInsComune.txtDesc.value)
		//Descrizione obbligatoria
		if ((frmInsComune.txtDesc.value == "")||
			(frmInsComune.txtDesc.value == " ")){
			alert("Descrizione del Comune obbligatoria")
			frmInsComune.txtDesc.focus() 
			return false
		}
		//Provincia obbligatoria
		if (frmInsComune.cmbProv.value == ""){
			alert("Departamento obbligatoria")
			frmInsComune.cmbProv.focus() 
			return false
		}	
		//CAP obbligatorio
		if ((frmInsComune.txtCap.value == "")||
			(frmInsComune.txtCap.value == " ")){
			alert("CAP obbligatorio")
			frmInsComune.txtCap.focus() 
			return false
		}
		//CAP deve essere numerico
		if (frmInsComune.txtCap.value != ""){
			if (!IsNum(frmInsComune.txtCap.value)){
				alert("Il CAP deve essere Numerico")
				frmInsComune.txtCap.focus() 
				return false
			}
		}
		//CAP deve essere diverso da zero
		if (frmInsComune.txtCap.value != ""){
			if (eval(frmInsComune.txtCap.value) == 0){
				alert("Il CAP non pu� essere zero")
				frmInsComune.txtCap.focus()
				return false
			}
		}
		//CAP deve essere di 5 caratteri
		if (frmInsComune.txtCap.value != ""){
			if (frmInsComune.txtCap.value.length != 5){
				alert("Il CAP deve essere composto da 5 caratteri")
				frmInsComune.txtCap.focus() 
				return false
			}
		}
		//Prefisso obbligatorio
		if ((frmInsComune.txtPref.value == "")||
			(frmInsComune.txtPref.value == " ")){
			alert("Prefisso obbligatorio")
			frmInsComune.txtPref.focus() 
			return false
		}
		//Prefisso deve essere numerico
		if (frmInsComune.txtPref.value != ""){
			if (!IsNum(frmInsComune.txtPref.value)){
				alert("Il Prefisso deve essere Numerico")
				frmInsComune.txtPref.focus() 
				return false
			}
		}
		//Prefisso deve essere diverso da zero
		if (frmInsComune.txtPref.value != ""){
			if (eval(frmInsComune.txtPref.value) == 0){
				alert("Il Prefisso non pu� essere zero")
				frmInsComune.txtPref.focus()
				return false
			}
		}
	return true
	}
</script>
<body>
<table border="0" width="520" cellspacing="0" cellpadding="0" height="81">
   <tr>
     <td width="500" background="<%=Session("Progetto")%>/images/titoli/strumenti2b.gif" height="81" valign="bottom" align="right">
       <table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
         <tr>
           <td width="100%" valign="top" align="right"><b class="tbltext1a">Gestione Comuni</span></b></td>
         </tr>
       </table>
     </td>
   </tr>
</table><br>

<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
<tr height="17">

	<td class="sfondomenu" width="67%" height="18"><span class="tbltext0">
		<b>&nbsp;Gestione Tabella Comune </b>
	</td>
	<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">
	</td>
	<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">(*) campi obbligatori</td>
</tr>
<tr>
	<td class="sfondocomm" width="57%" colspan="3">
		Inserire i valori del comune che si vuole aggiungere e premere <b>Invia</b> per confermare. 
		<br>
		<a href="Javascript:Show_Help('/pgm/help/Comuni/COM_InsComune')"><img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
	</td>
</tr>
<tr height="2">
	<td class="sfondocomm" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif">
	</td>
</tr>
</table>
<br>
<%
sAction = "INS"

Response.Write "<form method='post' name='frmInsComune' onsubmit='return ControllaDati(this)' action='COM_CnfComune.asp?Prov=" & sProv & "&MOD=1&ACTION=" & sAction & "'>"
Response.Write "<table border='0' cellpadding='0' cellspacing='1' width='500'>"
Response.Write "<tr>"
Response.Write "<td align='center' colspan='2' align='left' width='60%'>"
Response.Write "<b>"
Response.Write "<A class=textred href='COM_SelProv.asp'>Torna all'inizio"
response.write  "</a>"
Response.Write "</b>"
Response.Write "</Td>"
Response.Write "</tr>"
Response.Write "</table>"
Response.Write "<br>"

Response.Write "<table>"
Response.Write "<tr>"
Response.Write "<td align='left' colspan='2'>&nbsp;</td>"
Response.Write "<td align='left' colspan='2' width='60%'>&nbsp;</td>"
Response.Write "</tr>"
Response.Write "</table>"

'titolo della tabella
Response.Write "<table border='0' cellpadding='0' cellspacing='0' width='500'>"
Response.Write "<tr>"
'Response.Write "<td valign='top' width='23'>"
'Response.Write "<IMG height=26 src='../../images/angolosin.jpg' width=23></td>"
Response.Write "<td align=left>"
Response.Write "<STRONG><span class='tbltext1'></span></STRONG>"
Response.Write "</td>"
Response.Write "<td align='right'  width='50%'>"
'Response.Write "<i><span class='tbltext1'>(* = dati obbligatori)</span></i>"
Response.Write "</td>"
Response.Write "</tr>"
Response.Write "</table>"

'prima riga della tabella
Response.Write "<table border='0' cellpadding='0' cellspacing='1' width='500' style='WIDTH: 500px'>"
Response.Write "<tr>"
Response.Write "<td align='middle' colspan='2' nowrap class='tbltext1'>"
Response.Write "<p align='left'>"
'Response.Write "<span class='tbltext1'>"
'Response.Write "<strong>_</strong>"
'Response.Write "</span>"
Response.Write "<strong>Codice*</strong>"
Response.Write "<span class='tbltext1'>"
'Response.Write "<strong>_</strong>"
'Response.Write "</span>"
Response.Write "</p>"
Response.Write "</td>"
Response.Write "<td align='left' colspan='2' width='60%'>"
Response.Write "<p align='left'>"
Response.Write "<input class=textblacka style=TEXT-TRANSFORM:uppercase name='txtCodice' size='22' maxlength='4'>"
Response.Write "</p>"
Response.Write "</td>"
Response.Write "</tr>"
'seconda riga della tabella
Response.Write "<tr>"
Response.Write "<td align='middle' colspan='2' nowrap class='tbltext1'>"
Response.Write "<p align='left'>"
Response.Write "<span class='tbltext1'>"
'Response.Write "<strong>_</strong>"
'Response.Write "</span>"
Response.Write "<strong>Descrizione*</strong>"
Response.Write "<span class='tbltext1'>"
'Response.Write "<strong>_</strong>"
'Response.Write "</span>"
Response.Write "</p>"
Response.Write "</td>"
Response.Write "<td align='left' colspan='2' width='60%'>"
Response.Write "<p align='left'>"
Response.Write "<input class=textblacka style=TEXT-TRANSFORM:uppercase name='txtDesc' size='40' maxlength='35'>"
Response.Write "</p>"
Response.Write "</td>"
Response.Write "</tr>"
'terza riga della tabella
Response.Write "<tr>"
Response.Write "<td align='middle' colspan='2' nowrap >"
Response.Write "<p align='left'>"
Response.Write "<span class='tbltext1'>"
'Response.Write "<strong>_</strong>"
'Response.Write "</span>"
Response.Write "<strong>Departamento*</strong>"
Response.Write "<span class='tbltext1'>"
'Response.Write "<strong>_</strong>"
'Response.Write "</span>"
Response.Write "</p>"
Response.Write "</td>"
Response.Write "<td align='left' colspan='2' width='60%'>"
Response.Write "<p align='left'>"
		sSQL = " Select distinct PRV  from COMUNE where PRV is not null order by PRV "
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
		set rsProv = CC.Execute(sSQL)
		
		Response.Write "<select name='cmbProv' class='textblacka'>" &_
							"<option value=''></option>"
		do until rsProv.eof
				if rsProv.Fields("PRV") = sProv then
					Response.Write  "<option selected value='" & rsProv.Fields("PRV") & "'>" &_
										rsProv.Fields("PRV")&_
									"</option>"
				else
					Response.Write  "<option value='" & rsProv.Fields("PRV") & "'>" &_
										rsProv.Fields("PRV")&_
									"</option>"
				end if
				rsProv.Movenext
		loop
		Response.Write "</select>"
		rsProv.close
		set rsProv = nothing



'dim rsProv
'sSQL = "SELECT descrizione from tades where nome_tabella='PROV' order by descrizione"
'set rsProv = CC.Execute(sSQL)
'Response.Write "<SELECT class=textblacka ID=cmbProv name=cmbProv><OPTION value=></OPTION>"
'do while not rsProv.EOF
'	Response.Write "<OPTION value ='" & rsProv("descrizione") & "'> " & rsProv("descrizione")  & "</OPTION>"
'	rsProv.MoveNext
'loop
'Response.Write "</SELECT>"
'rsProv.Close
Response.Write "</p>"
Response.Write "</td>"
Response.Write "</tr>"
'quarta riga della tabella
Response.Write "<tr>"
Response.Write "<td align='middle' colspan='2' nowrap class='tbltext1' >"
Response.Write "<p align='left'>"
Response.Write "<span class='tbltext1'>"
Response.Write "</span>"
Response.Write "<strong>CAP*</strong>"
Response.Write "<span class='tbltext1'>"
'Response.Write "<strong>_</strong>"
'Response.Write "</span>"
Response.Write "</p>"
Response.Write "</td>"
Response.Write "<td align='left' colspan='2' width='60%'>"
Response.Write "<p align='left'>"
Response.Write "<input class=textblacka style=TEXT-TRANSFORM:uppercase name='txtCap' size='22' maxlength='5'>"
Response.Write "</p>"
Response.Write "</td>"
Response.Write "</tr>"
'quinta riga della tabella
Response.Write "<tr>"
Response.Write "<td align='middle' colspan='2' nowrap>"
Response.Write "<p align='left'>"
Response.Write "<span class='tbltext1'>"
Response.Write "<strong>Prefisso*</strong>"
Response.Write "<span class='tbltext3'>"
'Response.Write "<strong>_</strong>"
'Response.Write "</span>"
Response.Write "</p>"
Response.Write "</td>"
Response.Write "<td align='left' colspan='2' width='60%'>"
Response.Write "<p align='left'>"
Response.Write "<input class=textblacka style=TEXT-TRANSFORM:uppercase name='txtPref' size='22' maxlength='4'>"
Response.Write "</p>"
Response.Write "</td>"
Response.Write "</tr>"
Response.Write "</table>"
%>
<br>
<table border="0" cellpadding="1" cellspacing="0" width="500">
	<tr height="23">
		<td width="100">&nbsp;</td>
		<td align="rigth" width="150">
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="image" src="<%=Session("progetto")%>/images/conferma.gif" name="Invia" border="0" align="absBottom">
		</td>
<!--		<td align="left" width=80>			<a id=Annulla name='Annulla' href="javascript:frmInsComune.reset();" >			 <IMG src="<%'=Session("progetto")%>/images/annulla.gif" border=0>			</a>		</TD>-->
		<td align="rigth" width="170">
			<a id="Annulla" name="Annulla" href="javascript:history.back();">
			 <img src="<%=Session("progetto")%>/images/indietro.gif" border="0">
			</a>
		</td>
	</tr>
</table>
</form>
<!--#include file ="../../include/closeconn.asp"-->
<!--#include Virtual="/strutt_coda2.asp"-->



