<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include Virtual = "/strutt_testa2.asp"-->
<!--#include Virtual = "/include/DecCod.asp"-->
<!--#include Virtual = "/include/Comunicazione.asp"-->
<!--#include Virtual = "/include/ImpostaProgetto.asp"-->

<%
response.buffer  = true
Response.Expires = 0
Response.Expiresabsolute = Now() - 1
Response.AddHeader "pragma","no-cache"
Response.AddHeader "cache-control","private"
Response.CacheControl = "no-cache"
%>

<!--#include Virtual = "/include/DecComun.asp"-->
<!--#include Virtual = "/include/ControlDateVB.asp"-->
<!--#include Virtual = "/include/OpenConn.asp"-->
<!--#include Virtual = "/util/dbUtil.asp"-->
<!--#include file = "Assunzione.asp"-->
<!--#include file = "COM_Inizio.asp"-->
<br>
<script language="javascript">
	function LeggiContratto(sFileName){
		windowArea = window.open (sFileName,'Info','width=785,height=600,top=15,left=15,Resize=No,Scrollbars=yes,screenX=w,screenY=h')
	}
</script>

<%
dim nIdPersona ,nIdEspro,dDataIniEsprDal
dim dDataFinEsprDal,nNumOreSett
dim nSettore,sCodIstat
dim sRagioneSociale,sCodCCNL,sCodForma,nIdCPI,nIdSede

dim sMobilita
dim sPartitaIva
dim sDLCodFisc
dim sDLSettore
dim sDLATECO
dim sDLCCNL
dim sDLSedeLegale
dim sDLIndirizzoSL
dim sDLTelSL
dim sDLFaxSL
dim sDLEmailSL
dim sDLSedeLavoro
dim sDLIndirizzoSLAV
dim sDLTelSLAV
dim sDLFaxSLAV

dim sCognomeLAV
dim sNomeLAV
dim sCodFiscLAV
dim sCittadiLAV
dim sLuogoNascitaLAV
dim sDataNascitaLAV
dim sSessoLAV
dim sDomicilioLuogoLAV
dim sDomicilioViaLAV
dim sTistudLAV
dim sCpiLAV
dim sDataIscrCpiLAV

dim sIspLavoro
dim sDataIspettorato
dim sDataVisitaMedica
dim nDurataApprendistato
dim sCorsiPostDipl
dim sOreCorsi
dim sTUTCognome
dim sTUTNome
dim sNascTutore
dim sLuogoTutore
dim sTutoreCODFISC
dim sTitolare
dim sTutoreEsperienza
dim sTutoreQualifica
dim sTutoreLivello
dim sProtocollo 

dim dDataConDal
dim sIscrMatr
dim dDataConAl
dim sLavoDomicilio
dim nOreMedie
dim sAreaProf
dim sMansione
dim sConGrado
dim sConLivello
dim sTrattaEconomico

dim sTipoProf
dim sCFLDurataMesi
dim sCFLLII
dim sCFLLIF
dim sCFLAutMinNum
dim dCFLAutMinData
dim sCFLAppNum
dim dCFLAppDel
dim sCFLProgRif
dim sCFLDicDel
dim fCFLDichiaraz
dim fCFL2640
dim sIscLavDom
dim dIscLavDel
dim sTipoLavorazione
dim sDescTipContratto
dim sErrore
dim aMitt(1)
dim aDest(1)
dim sTesto
dim aLink(1)
dim sOggetto
dim nIdCPIAzienda
dim sDescCPI
dim sAttEcon
dim dDataFinEsprAl
dim sCessazione,dCessazione,dContCessaz,fApprendista
dim	sIscAlbo	
dim	sIscCCI		
dim	sCodSede	
dim	sPAInail	
dim	sUSL		
dim	sArt70		
dim	sCOniufato  
dim	sCodProfPrec	
dim	sUltAttSvolta	
dim	sUltAttCessata	
dim	sRappLavoro		
dim	sOreMedie		
dim	nNumMatricola	
dim	nNumContratti	
dim	dDataInizioCon	
dim	dDataFineCon	
dim	sTotSoggetti	
dim	sDenominazione	
dim	fDitArt		
dim	sCodFiscUtil
dim	sIvaUtil	
dim	sCodComunUtil	
dim	sViaUtil	
dim	sRappUtil	
dim	sCodAtecoUtil	
dim	sAmpDip		
dim	sMotUtil		
dim	sTipologiaLavTemp	
dim	dDataDallaTemp	
dim	dDataAllaTemp	
dim	sBenRichiesti	
dim	sAreaFunzionale	
dim	sLuogoLAvoro	
dim	sPAIUtil
dim	sCSEDEUtil
dim	sUSLUtil
dim	sTASSOUtil
dim	sAsbesUtil
dim	sSiliUtil
dim sSQL
dim sValuta
dim nTratEco
dim sInquadramento
dim nInquadramento 

nInquadramento = Request.Form("CmbLccnl")

sValuta = Request.Form("cmbValuta")
if sValuta <> "" then
	sValuta = DecCodVal("VALUT","0",date,sValuta,0) 
end if

nTratEco = Request.Form ("txtTEconomico") & " (" & sValuta & ") annui lordi"
if nInquadramento <> "" then
	sInquadramento = DecCodVal("LCCNL","0",date,nInquadramento,0) 
end if
 
sCessazione = Request.Form ("chkCessazione")
dCessazione = Request.Form ("txtDataCesDAl")
dContCessaz = Request.Form ("txtDataCesAl")
fApprendista =  Request.Form ("rdApp")
sProtocollo = ucase(Trim(Request.Form ("txtProt")))
nIdSede = clng(Request.Form("IDSEDE"))

sErrore = "0"
sDescTipContratto = DecCodVal("TESPR","0",date,Request.Form("chkContratto"),1)
sIscLavDom		= Request.Form("txtIscLavDom")
dIscLavDel		= Request.Form("txtIscLavDomDel")
sTipoLavorazione= Request.Form("txtTipoLavorazione")
sPartitaIva		= Request.Form("txtPIVA")
sDLCodFisc		= Request.Form("txtDLCodFisc")
sDLATECO		= Request.Form("txtATECO")
sDLCCNL			= DecCodVal("TCCNL","0",date, Request.Form("cmbCCNL"), 0)
sDLSedeLegale	= Request.Form("txtSedeLeg")
sDLIndirizzoSL	= Request.Form("txtDLIndirizzoSL")
sDLTelSL		= Request.Form("txtDLTelSL")
sDLFaxSL		= Request.Form("txtDLFaxSL")
sDLEmailSL		= Request.Form("txtDLEmailSL")
sDLSedeLavoro	= Request.Form("txtSedeLavoro")
sDLIndirizzoSLAV = Request.Form("txtIndirizzoSLAV")
sDLTelSLAV		= Request.Form("txtTelSLAV")
sDLFaxSLAV		= Request.Form("txtFaxSLAV")
sCognomeLAV		= Request.Form("txtCognomeLavoratore")
sNomeLAV		= Request.Form("txtNomeLavoratore")
sCodFiscLAV		= Request.Form("txtCODFISCLavoratore")
sCittadiLAV		= Request.Form("txtCittadLavoratore")
sLuogoNascitaLAV = Request.Form("txtNatoLavoratore")
sDataNascitaLAV = Request.Form("txtDataNascitaLavoratore")
sSessoLAV		= Request.Form("txtSessoLavoratore")
sDomicilioLuogoLAV = Request.Form("txtDomiciliatoLavoratore")
sDomicilioViaLAV = Request.Form("txtViaLavoratore")
sTistudLAV		= Request.Form("txtTSTUDLavoratore")
sCpiLAV			= Request.Form("txtCPIDESCLavoratore")
sDataIscrCpiLAV = Request.Form("txtCpiDalLavoratore")
sTipoContratto = Request.Form("Contratto")
nIdPersona		= clng(Request.Form("idPers"))
nIdCPIAzienda   = Request.Form("txtIDCPIAZIENDA")
sRagioneSociale = Request.Form("txtRagSoc")
sAreaProf		= Request.Form("cmbAreaProf")
sMansione		= Request.Form("txtMansioni")
sConGrado		= Request.Form("txtGrado")
sConLivello		= Request.Form("txtLivello")
'nSettore		= clng(Request.Form("txtEconomica"))
sTipoIns        = Request.Form("TipoIns")

if Request.Form("Interinale") = "" then ' Per CPI oppure aziende
	sIspLavoro		= Request.Form("txtIspLavoro")
	sDataIspettorato = Request.Form("txtILData")
	sDataVisitaMedica = Request.Form("txtVMData")
	nDurataApprendistato = Request.Form("txtDurataApp")
	sCorsiPostDipl	= Request.Form("txtCorsiPostDipl")
	sOreCorsi		= Request.Form("txtOreCorsi")
	sTUTCognome		= Request.Form("txtTCognome")
	sTUTNome		= Request.Form("txtTNome")
	sNascTutore		= Request.Form("txtTutoreIL")
	sLuogoTutore	= Request.Form("txtTutoreNato")
	sTutoreCODFISC	= Request.Form("txtTutoreCODFISC")
	sTitolare		= Request.Form("rTitolare")
	sTutoreEsperienza = Request.Form("txtTutoreEspLav")
	sTutoreQualifica = Request.Form("txtTutoreQualifica")
	sTutoreLivello	= Request.Form("txtTutoreLivello")
	dDataConDal		= Request.Form("txtDataDal")
	sIscrMatr		= Request.Form("txtIscrMatr")
	dDataConAl		= Request.Form("txtFinoAl")
	if Request.Form("chkLavDom") = "on" then
		sLavoDomicilio	= "S"
	else
		sLavoDomicilio	= "N"
	end if
	nOreMedie		= Request.Form("txtOreMedie")
	sTrattaEconomico = Request.Form("txtTEconomico")
	sTipoProf		= Request.Form("tipoProfes")
	sCFLDurataMesi	= Request.Form("txtDurata")
	sCFLLII			= Request.Form("txtLII")
	sCFLLIF			= Request.Form("txtLIF")
	sCFLAutMinNum	= Request.Form("txtAutMinNum")
	dCFLAutMinData	= Request.Form("txtAutMinData")
	sCFLAppNum		= Request.Form("txtAppNum")
	dCFLAppDel		= Request.Form("txtAppDel")
	sCFLProgRif		= Request.Form("txtProgRif")
	sCFLDicDel		= Request.Form("txtDicDel")
	fCFLDichiaraz	= Request.Form("chkDichiaraz")
	fCFL2640		= Request.Form("chk2460")
	nIdEspro		= Request.Form("chkContratto")
	sAttEcon		= Request.Form("txtDescSett")
	if Request.Form("txtDataDAl") <> "" then
'OLD	dDataIniEsprDal = ConvDateToDb(ConvStringToDate(Request.Form("txtDataDAl")))
		dDataIniEsprDal = Request.Form("txtDataDAl")
	end if
	if Request.Form("txtFinoAl") <> "" then
'OLD	dDataFinEsprAl = ConvDateToDb(ConvStringToDate(Request.Form("txtFinoAl")))
		dDataFinEsprAl = Request.Form("txtFinoAl")
	else
		dDataFinEsprAl = "null"
	end if
	sCodIstat		= Request.Form("txtCodMansione")
	sCodCCNL		= Request.Form("cmbCCNL")
	sCodForma		= Request.Form("txtCodForma")
	nIdCPI			= Request.Form("txtCPILavoratore")
	
else 
	' Carico i dati per agenzie interinali
	sIscAlbo		= Request.Form ("txtIscAlbo") 	
	sIscCCI			= Request.Form ("txtCCI")	
	sCodSede		= Request.Form ("txtCodSede")
	sPAInail		= Request.Form ("txtInal")
	sUSL			= Request.Form ("txtUsl")
	sArt70			= Request.Form ("rART70")
	sCOniufato		= Request.Form ("rCon")
	sCodProfPrec	= Request.Form ("cmbCodProfPrec") 
	sUltAttSvolta	= Request.Form ("cmbUltAttSvolta") 
	sUltAttCessata	= Request.Form ("cmbUltAttCessata")
	sRappLavoro		= Request.Form ("cmbRappLavoro") 
	sOreMedie		= Request.Form ("txtOraMedSett")
	nNumMatricola	= Request.Form ("txtNumMat")
	nNumContratti	= Request.Form ("txtNumContratto")
	dDataInizioCon	= Request.Form ("txtIniContratto")
	dDataFineCon	= Request.Form ("txtFinContratto")
	sTotSoggetti	= Request.Form ("txtTotSoggetti")
	sDenominazione	= Request.Form ("txtDenominazione")
	fDitArt			= Request.Form ("chDitArtigiana") 
	if fDitArt = "" then
		fDitArt = "No"
	end if
	sCodFiscUtil	= Request.Form ("txtCodFiscUtil")
	sIvaUtil		= Request.Form ("txtIvaUtil")
'	sCodComunUtil	= Request.Form ("txtComUtil")
'	sComunUtil		= Request.Form ("txtComune")
	sComunUtil		= Request.Form ("txtComUtil")
	sViaUtil		= Request.Form ("txtViaUtil")	
	sProvinciaUtil	= DecCodVal("PROV","0",date, Request.Form ("txtProvUtil"), 0)
	sCCNLAppl		= Request.Form ("cmbCCNLAppl")
	sCCNLAppl		= DecCodVal("TCCNL","0",date, sCCNLAppl , 0)
	sRappUtil		= Request.Form ("txtRappresUtil")
	sCodAtecoUtil	= Request.Form ("txtAtecoUtil")
	sAmpDip			= Request.Form ("cmbAmpDip")
	sMotUtil		= Request.Form ("cmbMotUtil")
	sPAIUtil		= Request.Form ("txtPainailUtil")
	sCSEDEUtil		= Request.Form ("txtCodSedeUtil")
	sUSLUtil		= Request.Form ("txtUSLUtil")
	sTASSOUtil		= Request.Form ("txtTassoPAUtil")
	sAsbesUtil		= Request.Form ("txtAsbesUtil")
	sSiliUtil		= Request.Form ("txtSilicosiUtil")
	sTipologiaLavTemp	= Request.Form ("cmbTipologia")
	if Request.Form("txtDallaData") <> "" then
'		dDataIniEsprDal = ConvDateToDb(ConvStringToDate(Request.Form("txtDallaData")))
		dDataIniEsprDal = Request.Form("txtDallaData")
	end if
	if Request.Form("txtAllaData") <> "" then
'		dDataFinEsprAl = ConvDateToDb(ConvStringToDate(Request.Form("txtAllaData")))
		dDataFinEsprAl = Request.Form("txtAllaData")
	else
		dDataFinEsprAl = "null"
	end if
'	dDataIniEsprDal	= Request.Form ("txtDallaData")
'	dDataFinEsprDal	= Request.Form ("txtAllaData")
	sCodIstat		= Request.Form ("txtCodMansione")
	sBenRichiesti	= Request.Form ("txtBenRic")
	sAreaFunzionale	= Request.Form ("txtAreaFun")
	sConGrado		= Request.Form ("txtGrado")
	sConLivello		= Request.Form ("txtLivello")
	sLuogoLAvoro	= Request.Form ("txtLuogoLavoro")	
end if
' **********************************************
' MI prendo la descricione del CPI dell'azienda
' **********************************************
if IsNumeric(nIdCPIAzienda) then
	sSQL = "SELECT DESCRIZIONE,INDIRIZZO  FROM SEDE_IMPRESA WHERE ID_SEDE = " & CLNG(nIdCPIAzienda)
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	set rsDescCPI = CC.Execute(sSQL)
	if not rsDescCPI.eof then
		sDescCPI = rsDescCPI("DESCRIZIONE") & " - " & rsDescCPI("INDIRIZZO")
	else
		sDescCPI = "-"
	end if
	rsDescCPI.close
	set rsDescCPI = nothing 
else
	if session("Progetto") <> "/PLAVORO" and session("Progetto") <> "/INTERINALE" then
		' E' un CPI che f� le richieste per conto proprio.
		nIdCPIAzienda = Session("Creator")
	end if
	sDescCPI = "-"
end if
' **********************************************
nNumOreSett = "null"
if Request.Form("txtOreMedie") <> "" then
	nNumOreSett	= Request.Form("txtOreMedie")
end if

CC.BeginTrans
sCodContratto = Mid(UCase(sTipoContratto),1,1)

' ****************************************************
' Epili 07/10/2002
' In base alle richieste della 
' riunione del 05/10/2002 non inserisco dato in ESPRO.
' ****************************************************
'select case ucase(sTipoContratto) 
'	case "ASSUNZIONE"
'		sCodContratto = "A"
'		sSQL = "INSERT INTO ESPRO (ID_PERSONA,DT_INI_ESP_PRO,DT_FIN_ESP_PRO," &_
'				"ID_SETTORE,COD_ESPERIENZA,COD_MANSIONE,RAG_SOC,COD_FORMA," &_
'				"COD_CCNL,NUM_ORE_SETT,DT_TMST)" &_
'				" VALUES(" & nIdPersona & "," &_
'				dDataIniEsprDal & "," &_
'				dDataFinEsprAl & "," &_
'				nSettore & ",'" &_
'				nIdEspro & "','" &_
'				sCodIstat & "','" &_
'				sRagioneSociale & "','" &_
'				sCodForma & "','" &_
'				sCodCCNL & "'," &_
'				nNumOreSett & ",SYSDATE)"
'		sErrore = EseguiNoCTrace(ucase(mid(session("progetto"),2)), Chiave, "ESPRO", session("idutente"),nIdPersona,"P", "INS", sSQL,"1", now(),CC)
		
'		aLink(0) = "Premi qui per visionare la comunicazione di assunzione."
'		aLink(1) = sFilePathLett & " "" target=_new"

'		sOggetto = "Aggiornamento stato occupazionale"
'		sTesto = "Comunicazione di " &_
'			 sTipoContratto & " dell''azienda " & sRagioneSociale &_
'			 " per il candidato " & sCognomeLAV & " " & sNomeLAV 
'		sTesto = sTesto & "|| Comunicazione di " &_
'			 sTipoContratto & " dell'azienda " & sRagioneSociale &_
'			 " per il candidato " & sCognomeLAV & " " & sNomeLAV  
'	case "CESSAZIONE"
'		sCodContratto = "C"
'		aLink(0) = "Premi qui per visionare la comunicazione di cessazione."
'		aLink(1) = sFilePathLett & " "" target=_new"
'		sOggetto = "Aggiornamento stato occupazionale"
'		sTesto = "Comunicazione di " &_
'			 sTipoContratto & " dell''azienda " & sRagioneSociale &_
'			 " per il candidato " & sCognomeLAV & " " & sNomeLAV 
'		sTesto = sTesto & "|| Comunicazione di " &_
'			 sTipoContratto & " dell'azienda " & sRagioneSociale &_
'			 " per il candidato " & sCognomeLAV & " " & sNomeLAV  
'	case "TRASFORMAZIONE"
'		sCodContratto = "T"
'		aLink(0) = "Premi qui per visionare la comunicazione di trasformazione."
'		aLink(1) = sFilePathLett & " "" target=_new"
'		sOggetto = "Aggiornamento stato occupazionale"
'		sTesto = "Comunicazione di " &_
'			 sTipoContratto & " dell'azienda " & sRagioneSociale &_
'			 " per il candidato " & sCognomeLAV & " " & sNomeLAV 
'		sTesto = sTesto & "|| Comunicazione di " &_
'			 sTipoContratto & " dell''azienda " & sRagioneSociale &_
'			 " per il candidato " & sCognomeLAV & " " & sNomeLAV  
'	case else
'end select 

' ********************************* 
' Aggiorno la RICHIESTA_CANDIDATO 
' ********************************* 
' Controllo comunuque se proviene da incrocio 
' o da assunzione diretta
' ********************************* 
if Request.Form ("IdRichiesta") <> "" then
	sIdRichiesta = clng(Request.Form ("IdRichiesta"))
else
	sIdRichiesta = 0
end if


' ****************************************
' Epili 07/10/2002
' In base alle richieste della 
' riunione del 05/10/2002 non invio la 
' comunicazione con la mailbox.
' ****************************************
' Invia la comunicazione 
' ********************************* 
'if sErrore = "0" then
'	aMitt(0) = "S"
'	aMitt(1) = Session("Creator")
'	aDest(0) = "S"
'	aDest(1) = nIdCPIAzienda
'	if InviaCom(aMitt,aDest,sTesto,aLink,sOggetto) then
'		sErrore = 0
'	else
'		sErrore = 1
'	end if
'end if
' ********************************* 



'''vani
'if Request.Form("TIPORICHIESTA") <> "" then 
'	stipobusqueda = "0" ''aviso
'else
'	stipobusqueda = "1" ''seleccion
'end if 

sidbusqueda = Request.Form("IdRichiesta")

'''vani


' ************************************** 
' Verifico se � una impresa utilizzatrice.
' **************************************
nIdUtil = Request.Form("IDSEDEUTIL")
if Request.Form("IDSEDEUTIL") = "" then 
	nIdUtil = "null"
else
	nIdUtil = clng(Request.Form("IDSEDEUTIL"))
end if
' **************************************
' Creo il file html con la comunicazione
' ************************************** 
if sErrore = "0" then

	Select case sCodContratto 
		case "A" 
			if dDataFinEsprAl <> "null" then
				dDataFinEsprAl = ConvDateToDBs(dDataFinEsprAl)
			end if 
			if sTipoIns ="D" then
				DtComunContr = Request.Form("DtComunicazione")
			else
				DtComunContr = date
			end if	
			DtComunContr = ConvDateToDBs(ConvStringToDate(DtComunContr))
			sSQLSEDECOM =	"INSERT INTO COM_SEDE " &_
							"(COD_LIVELLO,COD_ESPERIENZA,ID_CIMPIEGO,ID_SEDE,ID_PERSONA,DT_INVIO," &_
							"TIPO_COM,ID_IMPUT,PROT_SEDE," &_
							"PATH_COM,DT_FIN_ESP_PRO,DT_TMST,ID_BUSQUEDA) " &_
							"VALUES('" & nInquadramento & "','" & nIdEspro & "'," & nIdCPIAzienda & "," & nIdSede & "," &_
							nIdPersona & "," & DtComunContr & ",'" & sCodContratto & "'," & nIdUtil & ",'" & sProtocollo & "','" &_
							sFilePathLett & "'," & dDataFinEsprAl &  ",SYSDATE," & sidbusqueda & ")"
			sErrore = CheckValidita(nIdCPIAzienda,nIdSede,nIdPersona,DtComunContr,sCodContratto)
		case "C"
			sSQLSEDECOM = "INSERT INTO COM_SEDE " &_
				"(DT_FIN_ESP_PRO,DT_VAR_RLAV,ID_CIMPIEGO,ID_SEDE,ID_PERSONA,DT_INVIO," &_
				"TIPO_COM,ID_IMPUT,PROT_SEDE," &_
				"PATH_COM,DT_TMST,ID_BUSQUEDA) " &_
				"VALUES(" & ConvDateToDBs(ConvStringToDate(dCessazione)) & "," & ConvDateToDBs(ConvStringToDate(dCessazione)) & "," & nIdCPIAzienda & "," & nIdSede & "," &_
				nIdPersona & ",SYSDATE,'" & sCodContratto & "'," & nIdUtil & ",'" & sProtocollo & "','" &_
				sFilePathLett & "',SYSDATE," & sidbusqueda & ")"
				sErrore = CheckValidita(nIdCPIAzienda,nIdSede,nIdPersona,ConvDateToDBs(ConvStringToDate(dCessazione)),sCodContratto)
		case "T"
			if dDataFinEsprAl <> "null" then
				dDataFinEsprAl = ConvDateToDBs(dDataFinEsprAl)
			end if 
			sSQLSEDECOM = "INSERT INTO COM_SEDE " &_
				"(DT_FIN_ESP_PRO,COD_LIVELLO,COD_ESPERIENZA,DT_VAR_RLAV,ID_CIMPIEGO,ID_SEDE,ID_PERSONA,DT_INVIO," &_
				"TIPO_COM,ID_IMPUT,PROT_SEDE," &_
				"PATH_COM,DT_TMST,ID_BUSQUEDA) " &_
				"VALUES(" & dDataFinEsprAl & ",'" & nInquadramento & "','" & Request.Form("chkContrattoA") &  "'," & ConvDateToDBs(ConvStringToDate(Request.Form("txtDataTrasfDAl"))) & "," & nIdCPIAzienda & "," & nIdSede & "," &_
				nIdPersona & ",SYSDATE,'" & sCodContratto & "'," & nIdUtil & ",'" & sProtocollo & "','" &_
				sFilePathLett & "',SYSDATE," & sidbusqueda & ")"
				sErrore = CheckValidita(nIdCPIAzienda,nIdSede,nIdPersona,ConvDateToDBs(ConvStringToDate(Request.Form("txtDataTrasfDAl"))),sCodContratto)
	end select

'Response.Write sSQLSEDECOM

	if sErrore = "0" then
		sErrore=EseguiNoC(sSQLSEDECOM,CC)
	end if

	if sErrore = "0" then
		sSQL =	"SELECT MAX(ID_COMSEDE) AS idcom FROM COM_SEDE" &_
				" WHERE ID_SEDE = " & nIdSede &_
				" AND ID_PERSONA = " & nIdPersona &_
				" AND ID_CIMPIEGO = " & nIdCPIAzienda &_
				" AND TIPO_COM = '" & sCodContratto & "'" 
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
		set rsIdCom = cc.Execute(sSQL)
		if not rsIdCom.eof then
			nIDCom = CLng(rsIdCom("idcom"))
		else
			nIDCom = 0
			sErrore = "1"
		end if
		rsIdCom.close
		set rsIdCom = nothing
	end if
	if sErrore = "0" then
		sName =  nIDCom & ".htm"
		
		sFilePath = SERVER.MapPath("\") & ImpostaProgetto & "/DocPers/Comunicazioni/" &_
				sTipoContratto & "/" & sName
		sFilePathLett = ImpostaProgetto & "/DocPers/Comunicazioni/" &_
				sTipoContratto & "/" & sName
		
		sSQLSEDECOM = "UPDATE COM_SEDE SET PATH_COM = '" & sFilePathLett & "'" &_
			" WHERE ID_COMSEDE = " & nIDCom

		sErrore=EseguiNoC(sSQLSEDECOM,CC)

	end if
	if sErrore = "0" then
	' *************************************************
	' IPOTESI : [IDCOM].htm
	' *************************************************
	' sName =  nIdPersona & "_" & nIdSede & "_" & nIDCom & ".htm"
	'Response.Write sFilePath
		set oFileSystemObject = Server.CreateObject("Scripting.FileSystemObject")
		set oFileCurriculum = oFileSystemObject.CreateTextFile(sFilePath,true)
		select case UCASE(sTipoContratto) 
			case "ASSUNZIONE"
				sAsCFL = Request.Form ("txtTipoContratto")
				FoglioStile(oFileCurriculum)
				select case Request.Form ("Interinale")
					case ""
						Intestazione(oFileCurriculum)
						DatoreDILavoro(oFileCurriculum)
						Lavoratore(oFileCurriculum)
						Assunzione(oFileCurriculum)
						select case sAsCFL
							case "07" ' Apprendistato
								apprendistato(oFileCurriculum)
							case "09" ' Contratto Formazione Lavoro
								CFL(oFileCurriculum)
							case else ' Modello C/ASS/AG
								' Devo controllare se � iscritto a particolari liste 
								' di mobilit�.
								sSQL = "SELECT COD_STDIS,DT_TMST FROM STATO_OCCUPAZIONALE " &_
									" WHERE ID_PERSONA = " & nIdPersona &_
									" AND IND_STATUS='0'"
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
								set rsSTDIS = CC.execute(sSQL)
								if not rsSTDIS.eof then
									aSTDIS = decodTadesToArray("STDIS",rsSTDIS("DT_TMST")," CODICE = '" &_
									 rsSTDIS("COD_STDIS") & "'",1,"0")
									sAttributo = aSTDIS(0,1,1)
									sMobilita = aSTDIS(0,1,0)
									erase aSTDIS
								else
									sAttributo = ""
								end if
								rsSTDIS.Close
								set rsSTDIS = nothing
								if sAttributo = "S" then
									ModC_ASS_G(oFileCurriculum)
								end if
						end select 
						if sLavoDomicilio = "S" then
							LavoroDomicilio(oFileCurriculum)		
						end if
						Computabilita(oFileCurriculum)
						Didascalie(oFileCurriculum)
					case "Interinale"
						IntestazioneInterinale(oFileCurriculum)
						ImpresaFornitrice(oFileCurriculum)	
						if IsNumeric(nIdUtil) then
							ImpresaUtilizzatrice(oFileCurriculum)
						end if
	'					LavoratorePrimaImmissione(oFileCurriculum)
	'					RapportoLavoro(oFileCurriculum)																
						LavoroTemporaneo(oFileCurriculum)
						Lavoratore(oFileCurriculum)		
				end select 
			case "CESSAZIONE"
				sCessazione = DecCodVal("MCESS","0",date,sCessazione,1)
				FoglioStile(oFileCurriculum)
				if session("Progetto") = "/INTERINALE" then
					IntestazioneInterinale(oFileCurriculum)
				else
					Intestazione(oFileCurriculum)
				end if
				DatoreDILavoro(oFileCurriculum)
				Lavoratore(oFileCurriculum)
				Cessazione(oFileCurriculum)
			case "TRASFORMAZIONE"
				FoglioStile(oFileCurriculum)
				if session("Progetto") <> "/INTERINALE" then
					sContrattatoDa = DecCodVal("TESPR","0",date,Request.Form("chkContrattoDa"),1)
					sContrattatoA = DecCodVal("TESPR","0",date,Request.Form("chkContrattoA"),1)	
					Intestazione(oFileCurriculum)
				else
					sContrattatoDa = Request.Form("chkContrattoDa")
					sContrattatoA = Request.Form("chkContrattoA")
					IntestazioneInterinale(oFileCurriculum)
				end if
				DatoreDILavoro(oFileCurriculum)
				Lavoratore(oFileCurriculum)		
				Trasformazione(oFileCurriculum)
			case else
				sErrore = "1"
		end select
		bottoni(oFileCurriculum)
		oFileCurriculum.close
		set oFileSystemObject = nothing
		set oFileCurriculum = nothing
	' **********************************************************************
	end if
end if
if sErrore = "0" then
	'*****************    
	'PER DEBUG    
	CC.CommitTrans    
	'CC.RollBackTrans
	'Response.End 
	%>
	<table width="500px" border="0" cellspacing="0" cellpadding="0" height="70" align="center">
		<tr>
			<td width="500" align="center" class="tbltext3">
			 La comunicaci�n  <em><%=sTipoContratto%> </em>ha sido <br>correctamente enviada a la Oficina de Empleo.
			</td>
			</tr>
	</table> 
	
	<%
	select case ucase(Session("DeDonde"))
	case "OE"
		Session("ADonde") = "/Pgm/Iscr_Utente/UTE_VisStatOcc.asp?SinMenu=S&IdPers=" & nIdPersona 
	case "EM"
		Session("ADonde") = "../../BA/Home.asp"
	case else
	
	end select 	
	
	Response.Redirect Session("ADonde")
	%>
	
	<%'if sTipoIns ="D" then%>
		<!--<meta http-equiv="Refresh" content="3; URL=Javascript:location.replace('/Pgm/Comunicazioni/COM_VisComunicazioni.asp')">-->
	<%'else%>
		<!--<meta http-equiv="Refresh" content="3; URL=Javascript:location.replace('/Pgm/Comunicazioni/COM_SelContratto.asp')">-->
	<%'end if%>
	
	
<%else
	CC.RollBackTrans%>
	<table width="500px" border="0" cellspacing="0" cellpadding="0" height="70" align="center">
		<tr>
			<td width="500" align="center" class="tbltext3">
				<%=sErrore%>
			</td>
		</tr>
	</table> 
	<br>
	<table width="500px" border="0" cellspacing="0" cellpadding="0" height="70" align="center">
		<tr>
			<td width="500" align="center" class="tbltext3">
				<!--a href="Javascript:history.back()"><img border="0" 					src="<%'=Session("progetto")%>/images/indietro.gif"></a-->
				<a href="Javascript:history.go(-2)"><img border="0" src="<%=Session("progetto")%>/images/indietro.gif"></a>
			</td>
		</tr>
	</table> 
<% end if %>
<form Name="visdoc1" Method="post" Action="/pgm/documentale/DOC_VisDocumenti.asp"><input type="hidden" name="Tema" value="71"><input type="hidden" name="progetto" value="/PLAVORO">
</form>
<!--#include Virtual = "/include/CloseConn.asp"-->
<!--#include Virtual="/strutt_coda2.asp"-->		

<%
function CheckValidita(nIdCimpiego,nIdSede,nIdPersona,dDtVarLav,sTipoCom)

	dim sSQL
	dim rsControllo

	sSQL = "SELECT COUNT(ID_COMSEDE) as Conta FROM COM_SEDE " &_
		" WHERE ID_PERSONA= " & nIdPersona &_
		" AND ID_CIMPIEGO= " & nIdCimpiego &_
		" AND ID_SEDE= " & nIdSede 
		if 	sCodContratto <>  "A" then
			sSQL = sSQL & " AND DT_VAR_RLAV = " & dDtVarLav
		else
			sSQL = sSQL & " AND DT_INVIO = " & dDtVarLav
		end if
		sSQL = sSQL & " AND TIPO_COM='" & sTipoCom & "'"
		
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	set rsControllo = CC.Execute(sSQL)	
	if clng(rsControllo("Conta")) > 0  then
		CheckValidita = "No se pueden enviar m�s comunicaciones relativas a un mismo candidato, de la misma empresa al mismo centro para el empleo, en el mismo d�a para una misma candidatura"
	else
		CheckValidita = "0"
	end if
	rsControllo.Close
	set rsControllo = nothing
	
end function 
%>
