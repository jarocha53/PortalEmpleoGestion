function Controlli(){
	if (document.frmContratto.cmbTipologia.value == "")	{
		alert ("Es necesario indicar la tipolog�a del contrato.");
		document.frmContratto.cmbTipologia.focus();
		return false;
	}
	if (document.frmContratto.txtDallaData.value == "")	{
		alert ("Es necesario indicar la fecha de inicio del informe de trabajo.");
		document.frmContratto.txtDallaData.focus();
		return false;
	}else{
		if (frmContratto.txtDallaData.value != "") 	{
			if (!ValidateInputDate(frmContratto.txtDallaData.value)){
				frmContratto.txtDallaData.focus();	
				return false;
			}
		}
	}
	if (document.frmContratto.txtAllaData.value != ""){
		if (!ValidateInputDate(frmContratto.txtAllaData.value)) {
			frmContratto.txtAllaData.focus();	
			return false;
		}else{
			if (!ValidateRangeDate(frmContratto.txtDallaData.value,frmContratto.txtAllaData.value)){
				frmContratto.txtDallaData.focus();	
				alert("La fecha de inicio del contrato debe ser inferior a la fecha de fin.");
				return false;
			}
		}
	}
	document.frmContratto.txtBenRic.value = TRIM(document.frmContratto.txtBenRic.value)
	document.frmContratto.txtAreaFun.value = TRIM(document.frmContratto.txtAreaFun.value)
	document.frmContratto.cmbCCNLAppl.value = TRIM(document.frmContratto.cmbCCNLAppl.value)
	if (document.frmContratto.cmbCCNLAppl.value == ""){
		alert ("Es necesario indicar el contrato aplicado.");
		document.frmContratto.cmbCCNLAppl.focus();
		return false;
	}
	frmContratto.txtGrado.value = TRIM(frmContratto.txtGrado.value)
	frmContratto.txtLivello.value = TRIM(frmContratto.txtLivello.value)
	return true;
}