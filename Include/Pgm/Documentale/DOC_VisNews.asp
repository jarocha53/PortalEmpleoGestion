<%@ Language=VBScript %>
<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #include Virtual="/strutt_testa2.asp" -->
<!-- #include virtual="/util/portallib.asp" -->
<!-- #include virtual="/include/OpenConn.asp" -->
<!-- #include virtual="/include/ckProfile.asp" -->
<!-- #include virtual="/include/ControlDateVB.asp" -->

<script language="javascript" src="/Include/help.inc"></script>

<base target="_top">
<%
If (Not ValidateService(Session("IdUtente"),"Gestione Notizie",cc) And Not ValidateService(Session("IdUtente"),"Sistema Documentale",cc) ) Then 
	response.redirect "/util/error_login.asp"
End If

Idtema = Request.Form("Tema")

Dim DirScelta, Posbarra, Pathname
Dim fso, f
' Dato id  ricavo dati della categoria corrispondente
Set rstCategorie = Server.CreateObject("ADODB.Recordset")
SQLcategorie = "SELECT Id_Categoria, Descrizione, Percorso, Abstract, Padre, Tipo FROM Categoria WHERE Id_Categoria =" & IdTema
'PL-SQL * T-SQL  
SQLCATEGORIE = TransformPLSQLToTSQL (SQLCATEGORIE) 
rstCategorie.open SQLcategorie,CC,1,3

nIdCatScelta = rstCategorie("Id_Categoria")

If rstCategorie("Tipo") <> "N" Then
	Response.Redirect "/pgm/documentale/DOC_initnews.asp"
End if

	DirScelta = rstCategorie("Percorso")
	Pathname = Session("DirInit") & DirScelta
' Controllo la posizione della directory
	Set fso = CreateObject("Scripting.FileSystemObject")
	on error resume next
	Set f = fso.GetFolder(Pathname)
	If err.number <> 0 Then
		Response.Write "<b class='tbltext'><br>En este momento no se puede visualizar la p�gina"
		Response.Write "<p>Contactar el Grupo Asistencia Portal Italia Lavoro <br>"
		Response.Write "a la direcci�n <a href='mailto:po-assistenza@italialavoro.it'>po-assistenza@italialavoro.it</a></p></b>"
		Response.End
	End If 
' Diverse query a seconda che cerchi solo le notizie pubblicate
If Request.Form("P") = "" Then
    Set rstNotizie = Server.CreateObject("ADODB.Recordset")
	SQLnotizie = "SELECT Id_Notizie, Titolo, Abstract, Data_Ins, Id_Categoria, Nome_Notizie, Fl_Pubblicato FROM Notizie WHERE Id_Categoria="& IdTema & " ORDER BY Id_Notizie Desc"
'PL-SQL * T-SQL  
SQLNOTIZIE = TransformPLSQLToTSQL (SQLNOTIZIE) 
	rstNotizie.open SQLnotizie, CC, 1, 3
	
	Trovato=False	
	Do While not rstNotizie.EOF
		If (Cint(rstNotizie("Fl_Pubblicato")) = 0) Then
			Trovato=True
			Exit do
		End If
		rstNotizie.MoveNext
	Loop	
Else
	Set rstNotizie = Server.CreateObject("ADODB.Recordset")
	SQLnotizie = "SELECT Id_Notizie, Titolo, Abstract, Data_Ins, Id_Categoria, Nome_Notizie, Fl_Pubblicato FROM Notizie WHERE Id_Categoria="& IdTema & " And Fl_Pubblicato=0 ORDER BY Id_Notizie Desc"
'PL-SQL * T-SQL  
SQLNOTIZIE = TransformPLSQLToTSQL (SQLNOTIZIE) 
	rstNotizie.open SQLnotizie, CC, 1, 3
End If

	If rstNotizie.RecordCount < 10 Then
		TotNews = (rstnotizie.RecordCount*3)
	Else
		TotNews = 30
	End If
	
Srv = Request.ServerVariables("SERVER_NAME")
FileEdit = Session("DirInitEdit") & DirScelta & "\" & Session("IdUtente") & ".htm"	

%>
<br>
<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
	<tr height="18">
		<td class="sfondomenu" width="67%" height="18"><span class="tbltext0"><b>&nbsp;AREA REDACCIONAL - <%=Session("Intestazione")%> </b></span></td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
		<td valign="middle" align="right" class="tbltext1" width="40%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
	</tr>
</table>
<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
	<form id="PubbDoc" method="POST" action="DOC_VisNews.asp">
		<input type="hidden" name="Tema" value="<%=Idtema%>">
		<input type="hidden" name="P" value="Ok">
	</form>	
	<tr>
		<td class="sfondocomm" width="57%">
		Para ingresar, cancelar o modificar una Noticia en una sub categor�a seleccionar la sub-carpeta correspondiente en la secci�n <b>Las Ultimas Noticias</b> en el argumento para modificar la descripci�n de las categor�as corrientes o para eliminar la categor�a presionar sobre <b>Modifica</b> o sobre <b>Elimina</b>. Para visualizar todas las Noticias de la categor�a corriente utilizar <b>Visualiza Noticias</b>.
		<a href="Javascript:Show_Help('/Pgm/help/Documentale/DOC_VisNews')" name onmouseover="javascript:status='' ; return true">
		<img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0" width="18" height="18" alt="Para mayores informaciones"></a>
		</td>
<%		If Trovato And Session("GruppoDoc")=0 Then %>
		<td valign="top" class="sfondocomm" width="5%" align="right">
			<br><img src="/images/sistdoc/PubbOnline.gif">&nbsp;
			<a class="tbltext1" href="<%=Session("Progetto")%>" onClick="Javascript:PubbDoc.submit();return false">
			<br><b>Noticias para Publicar</b></a>
		</td>
<%		End If %>
	</tr>
	<tr height="17">
		<td class="sfondocomm" width="100%" colspan="2" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
	</tr>
</table>

<!-- #include file="DOC_VisBarra.asp" -->

<table WIDTH="500" BORDER="0" CELLPADDING="2" CELLSPACING="2">
	<tr>
        <td class="sfondocomm" align="center" width="500"><b>Las �ltimas noticias sobre el argumento <i><%=rstCategorie("Descrizione")%></i></b></td>
	</tr>
	<tr>
		<td width="500">
			<table width="500" border="2" CELLPADDING="2" CELLSPACING="2" align="left">
              <tr>
                <td width="350" height="160" align="left" class="tbltext1">
<% 
                If rstNotizie.RecordCount > 0 Then
					rstNotizie.MoveFirst
%>
					<applet code="VertTextScroller.class" width="350" height="125" id="Applet1" align="middle" VIEWASTEXT>
                      <param name="numberOfLines" value="<%=TotNews%>">
                      <param name="background" value="white">
                      <param name="scrollDelay" value="30">
                      <param name="lineSpace" value="17">
                      <param name="linkFrame" value="_top">
                      <param name="linkColor" value="red">
                      <param name="manualPause" value="0">
                      <param name="linkClick" value="1">
                      <param name="sizeDefault" value="12">
                      <param name="lineDefault" value="  ">
                      <param name="colorDefault" value="black">
                      <param name="fontDefault" value="TimesRoman">
                      <param name="styleDefault" value="PLAIN">
                      <param name="linkDefault" value="  ">
                      <param name="pauseValueDefault" value="0">
                      <param name="lineOffsetDefault" value="0">
<%					For indice = 1 To TotNews %>
                      <param name="line<%=indice%>" value="<%=rstNotizie("Titolo")%>">
                      <param name="link<%=indice%>" value="http://<%=Srv%>/Pgm/documentale/DOC_LeggiNews.asp?Nr=<%=rstNotizie("Id_Notizie")%>">
                      <param name="color<%=indice%>" value="navy">
                      <param name="pauseValue<%=indice%>" value="2500">
                      <param name="lineOffset<%=indice%>" value="10">
                      <param name="line<%=(indice+1)%>" value="<%=rstNotizie("Abstract")%>">
                      <param name="link<%=(indice+1)%>" value="http://<%=Srv%>/Pgm/documentale/DOC_LeggiNews.asp?Nr=<%=rstNotizie("Id_Notizie")%>">
                      <param name="color<%=(indice+1)%>" value="blue">
                      <param name="pauseValue<%=(indice+1)%>" value="2500">
                      <param name="lineOffset<%=(indice+1)%>" value="10">
<% 
						indice = indice + 2
						rstNotizie.MoveNext 
					Next 
%>
                     </applet>  
<%				Else %>
					<p align="center"><b>El argumento seleccionado non contiene noticias</b>
<%				End If %>
                </td>           
                <td width="150" align="center" class="tbltext1">
<%
' Query per sarpere se vi sono sottodirectory
				SQLSubCategorie = "SELECT Id_Categoria, Descrizione FROM Categoria WHERE Padre=" & Idtema & "ORDER BY Descrizione"
'PL-SQL * T-SQL  
SQLSUBCATEGORIE = TransformPLSQLToTSQL (SQLSUBCATEGORIE) 
				Set rstSubCategorie = CC.Execute(SQLSubCategorie)
			
				If rstSubCategorie.BOF Then	
					Response.Write "<B>No existen sub-argumentos</B>"
				Else
					nIndSubCat = 0
					Do While Not rstSubCategorie.EOF
						nIndSubCat = nIndSubCat + 1 
						Response.Write "<FORM name='frmsubcat" & nIndSubCat & "' method='POST' action='DOC_VisNews.asp'>"
						Response.Write "<input type='hidden' name='Tema' value='" & rstSubCategorie("Id_Categoria") & "'>"
						Response.Write "<TABLE width='150' border='0'>"
						Response.Write "<tr><td width='17'><img src='/images/icons/cartella.gif' width='17' height='17'>"
						Response.Write "</td><td width='130' class='tbltext1'>"
						Response.Write "<a class='tbltext1' href='" & Session("Progetto") & "' onClick='Javascript:frmsubcat" & nIndSubCat & ".submit();return false'>" 
						Response.Write "<b>" & rstSubCategorie("Descrizione") & "</b></a></td></tr></TABLE>"
						Response.Write "</FORM>"
						rstSubCategorie.MoveNext
					Loop
				End If 
%>
                </td>
              </tr>
            </table>
		</td>
	</tr>
</table>
<%
rstCategorie.Close
Set rstCategorie = nothing 
rstSubCategorie.Close
Set rstSubCategorie = nothing 
rstNotizie.Close
Set rstNotizie = nothing 
%>
<!-- #include virtual="/include/closeconn.asp" -->
<!-- #include virtual="/strutt_coda2.asp"-->

