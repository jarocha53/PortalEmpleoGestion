<%@ Language=VBScript %>
<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #include virtual="/strutt_testa2.asp" -->
<!-- #include virtual="/util/portallib.asp" -->
<!-- #include virtual="/include/OpenConn.asp" -->
<%
	If Not ValidateService(Session("IdUtente"),"Sistema Redazionale",cc) Then 
		response.redirect "/util/error_login.asp"
	End if

IdNews = Request.QueryString("Nr")
' Dato id ricavo i dati della notizia corrispondente
Set rstNotizie = Server.CreateObject("ADODB.Recordset")
SQLNotizie = "SELECT Nome_Notizie, Id_Categoria FROM Notizie WHERE Id_Notizie=" & IdNews 
'PL-SQL * T-SQL  
SQLNOTIZIE = TransformPLSQLToTSQL (SQLNOTIZIE) 
rstNotizie.open SQLNotizie, CC, 1, 3

NomeNews = rstNotizie("Nome_Notizie")
IdCat = rstNotizie("Id_Categoria")	
' Dato idcategoria ricavo il percorso dove si trova la notizia
Set rstCategoria = Server.CreateObject("ADODB.Recordset")
SQLCategoria = "SELECT Id_Categoria, Percorso FROM Categoria WHERE Id_Categoria=" & IdCat 
'PL-SQL * T-SQL  
SQLCATEGORIA = TransformPLSQLToTSQL (SQLCATEGORIA) 
rstCategoria.open SQLCategoria, CC, 1, 3

Dim DirScelta, Posbarra, Pathname, PathDir
Dim fso, f, fl
	DirScelta = rstCategoria("Percorso")

' Controllo che il file sia comune o relativo ad un progetto in quanto cambia la directory
sSepDirScelta = split(DirScelta,"\")
If UCase(sSepDirScelta(0)) = "TESTI" Then
	PathDir = Server.MapPath("\") & "\" & DirScelta
Else
	PathDir = Session("DirInit") & DirScelta
End If

	PathFile = PathDir & "\" & NomeNews & ".htm"
	DestFinale = PathDir & "\" & Mid(NomeNews,2) & ".htm"
' Controllo che il file esista
	Set fso = CreateObject("Scripting.FileSystemObject")
	on error resume next
	Set fl = fso.GetFile(PathFile) 
	If err.number <> 0 Then
		Response.Write "<b class='tbltext'><br>En este  momento no se puede salvar la p�gina "
		Response.Write "<p>Contactar el Grupo Asistencia Portal Italia Lavoro<br>"
		Response.Write "a la direcci�n <a href='mailto:po-assistenza@italialavoro.it'>po-assistenza@italialavoro.it</a></p></b>"
		Response.End
	End If
	fl.Copy (DestFinale)
	
   FileBak = PathFile & ".bak"
   
 If fso.FileExists(FileBak) Then	
	fso.DeleteFile(FileBak)
 End if 
%>
<BODY>
<TABLE border="0" CELLPADDING="2" CELLSPACING="2" width="510">
  <tr>
  <%
' Ritorno il messaggio corrispondente
	Response.Write "<TABLE widht=510 border=0 CELLPADDING=2 CELLSPACING=2>"
	Response.Write "<FORM name='copia' method='post' action='DOC_VisRedazionale.asp'>"
	Response.Write "<input type='hidden' name='Tema' value='" & IdCat & "'>" 
	Response.Write "</FORM>"
	Response.Write "<tr><td align='center'><img src='/images/sistdoc/Upload/ModNewsOK.gif' widht='400' height='260' alt='Modifica riuscita'></td></tr>"
	Response.Write "<tr><td colspan='2' class='tbltext3' align='center'><b>La p�gina con el t�tulo &nbsp;<br><i class='textredb'>" & Mid(NomeNews,2)
	Response.Write "&nbsp;</i><br>ha sido publicada.</b></td></tr>"
	Response.Write "<tr><td align='center'><a class='tbltext1' href='" & Session("Progetto") & "' onClick='Javascript:copia.submit();return false'>"
	Response.Write "<img alt='Atr�s' border='0' src='" & Session("Progetto") & "/images/indietro.gif'></a></td></tr>"
	Response.Write "</TABLE>"
	
	rstCategoria.Close
	Set rstCategoria = nothing 
	rstNotizie.Close
	Set rstNotizie = nothing 
  %>
  <!-- #include virtual="/strutt_coda2.asp" -->
  </tr>
</TABLE>
