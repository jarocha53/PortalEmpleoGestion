<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<script LANGUAGE="JavaScript">
<!--
<!--#include Virtual = "/Include/help.inc"-->
<!--#include virtual = "/Include/ControlDate.inc"-->
<!--#include virtual = "/Include/ControlNum.inc"-->

function ControllaDateGruppo(ind,Oggi) {
	var nGrupElem;
	var i;
	var nElemChecked;

	nGrupElem = 1;
	if ( eval("document.form1.chkIdGru" + ind )) {
		if ( eval("document.form1.chkIdGru" + ind + ".length")) 
			nGrupElem = eval("document.form1.chkIdGru" + ind + ".length")
		// Quali sono ceccati.
		nElemChecked = 0;
		if (nGrupElem > 1) {
			for ( i=0; i<nGrupElem; i++ ) {
				if (eval("document.form1.chkIdGru" + ind + "[" + i + "].checked")) {
					nElemChecked = nElemChecked + 1;
					
					// Prima controllo se data valida.
					if (!ValidateInputDate(eval("document.form1.txtDtDalGru" + ind + "[" + i + "].value ") ) ) {
						eval("document.form1.txtDtDalGru" + ind + "[" + i + "].focus()") ;
						return false;
					}
					// Controllo se maggiore della data odierna.
					if (ValidateRangeDate(eval("document.form1.txtDtDalGru" + ind + "[" + i + "].value "),Oggi)) {
						eval("document.form1.txtDtDalGru" + ind + "[" + i + "].focus()") ;
						alert("La fecha debe ser posterior a la fecha actual."); 
						return false;
					}
					
					// Controllo se esiste la data fine.
					if (eval("document.form1.txtDtAlGru" + ind + "[" + i + "].value ") != "" ) {

						// Controllo che la data fine sia una data valida.
						if (!ValidateInputDate(eval("document.form1.txtDtAlGru" + ind + "[" + i + "].value ") ) ) {
							eval("document.form1.txtDtAlGru" + ind + "[" + i + "].focus()") ;
							return false;
						}
						// Controllo l'integrit� con la data inizio.
						if (!ValidateRangeDate(eval("document.form1.txtDtDalGru" + ind + "[" + i + "].value "), eval("document.form1.txtDtAlGru" + ind + "[" + i + "].value "))) {
							eval("document.form1.txtDtDalGru" + ind + "[" + i + "].focus()") ;
							alert("Controlar el intervalo entre las fechas indicadas."); 
							return false;
						}
					}
				} else {
					if (eval("document.form1.txtDtDalGru" + ind + "[" + i + "].value ") != "") {
						alert("Si se ha indicado la fecha es necesario seleccionar tambi�n el grupo."); 
						eval("document.form1.txtDtDalGru" + ind + "[" + i + "].focus()");
						return false;
					}
					if (eval("document.form1.txtDtDalGru" + ind + "[" + i + "].value ") != "") {
						alert("Si se ha indicado la fecha de fin es necesario seleccionar tambi�n el grupo."); 
						eval("document.form1.txtDtDalGru" + ind + "[" + i + "].focus()");
						return false;
					}
				}
			}
		} else {
			if (eval("document.form1.chkIdGru" + ind + ".checked")) {
				nElemChecked = 1;

				// Prima controllo se data valida.
				if (!ValidateInputDate(eval("document.form1.txtDtDalGru" + ind + ".value ") ) ) {
					eval("document.form1.txtDtDalGru" + ind + ".focus()") ;
					return false;
				}
				// Controllo se maggiore della data odierna.
				if (ValidateRangeDate(eval("document.form1.txtDtDalGru" + ind + ".value "),Oggi)) {
					eval("document.form1.txtDtDalGru" + ind + ".focus()") ;
					alert("La fecha debe ser posterior a la fecha actual."); 
					return false;
				}
					
				
				if (eval("document.form1.txtDtAlGru" + ind + ".value ") != "" ) {
					// Prima controllo se data valida.
					if (!ValidateInputDate(eval("document.form1.txtDtAlGru" + ind + ".value ") ) ) {
						eval("document.form1.txtDtAlGru" + ind + ".focus()") ;
						return false;
					}
					// Controllo validit� l'omogenit� tra data fine e data inizio
					if (!ValidateRangeDate(eval("document.form1.txtDtDalGru" + ind + ".value "), eval("document.form1.txtDtAlGru" + ind + ".value "))) {
						eval("document.form1.txtDtDalGru" + ind + ".focus()") ;
						alert("Controlar el intervalo entre las fechas indicadas."); 
						return false;
					}
				}
			} else {
				if (eval("document.form1.txtDtDalGru" + ind + ".value") != "") {
					alert("Si ha sido indicada la fecha de inicio es necesario seleccionar el grupo."); 
					eval("document.form1.txtDtDalGru" + ind + ".focus()");
					return false;
				}
				if (eval("document.form1.txtDtAlGru" + ind + ".value") != "") {
					alert("Si ha sido indicada la fecha de fin es necesario seleccionar el grupo."); 
					eval("document.form1.txtDtAlGru" + ind + ".focus()");
					return false;
				}
							
			}
		
		}

	}
	return true
}
		
function ControllaDateRorga(ind,Oggi) {

	if (eval("document.form1.txtDtDal" + ind + ".value") == "")	{
		alert ("Indicar la fecha de inicio del periodo de utilizaci�n del grupo");
		eval("document.form1.txtDtDal" + ind + ".focus()");
		return false;
	} else {
		if (ValidateRangeDate(eval("document.form1.txtDtDal" + ind + ".value"), Oggi)) {
			alert ("Fecha de inicio periodo de utilizaci�n del grupo inferior a la fecha de hoy");
			eval("document.form1.txtDtDal" + ind + ".focus()");
			return false;
		}
		if (!ValidateInputDate(eval("document.form1.txtDtDal" + ind + ".value"))) {
			eval("document.form1.txtDtDal" + ind + ".focus()");
			return false;
		}
	}
							
	if (eval("document.form1.txtDtAl" + ind + ".value") == "") {
	/*	alert ("Indicare la data di fine periodo di fruizione del gruppo");
		eval("document.form1.txtDtAl" + i + ".focus()");
		return false;
	*/	
	} else {
		if (!ValidateInputDate(eval("document.form1.txtDtAl" + ind + ".value "))) {
			eval("document.form1.txtDtAl" + ind + ".focus()") ;
			return false;
		}
	}

	if (eval("document.form1.txtDtDal" + ind + ".value ") != "" && eval("document.form1.txtDtAl" + ind + ".value ") != "")	{
		if (!ValidateRangeDate(eval("document.form1.txtDtDal" + ind + ".value "), eval("document.form1.txtDtAl" + ind + ".value "))) {
			eval("document.form1.txtDtAl" + ind + ".focus()") ;
			alert("Controlar el intervalo entre las fechas indicadas."); 
			return false;
		}
	}
	return true

}
			
	function controllo_date(n , Oggi) {		
		var ind;

		for ( ind = 0; ind < n; ind++ ) {		
			if(eval("document.form1.chkRuo" + ind + ".checked")) {
				if (!eval("document.form1.chkIdGru" + ind )) { // Controlla esistenza dei sottogruppi.
					if (!ControllaDateRorga(ind,Oggi)) 
						return false
				} else {
					// Controllo se � stato selezionato un sottogruppo,
					// se � selezionato verifico la esistenza della data inizio.
					nGrupElem = 1;
					if ( eval("document.form1.chkIdGru" + ind + ".length")) 
						nGrupElem = eval("document.form1.chkIdGru" + ind + ".length")
					if (nGrupElem == 1 ) {
					
						if (eval("document.form1.chkIdGru" + ind + ".checked") ) {
							if ((!eval("document.form1.chkRuo" + ind + ".disabled")) && (eval("document.form1.txtDtDalGru" + ind + ".value") != "")) {
								if (!ControllaDateGruppo(ind,Oggi)) 
									return false
							} else {
								if (eval("document.form1.txtDtDalGru" + ind + ".value ") == "") { 
									alert("1 - Seleccionar un rol organizativo o un grupo.")
									eval("document.form1.txtDtDalGru" + ind + ".focus()")
									return false
								} else {
									if (!ControllaDateGruppo(ind,Oggi)) 
										return false
								}
								
								
							}
						} else {
							if 	((eval("document.form1.txtDtDalGru" + ind + ".value") != "") && ( !eval("document.form1.chkIdGru" + ind + ".disabled")) ){
								alert("3- Seleccionar un rol organizativo o un grupo.")
								eval("document.form1.txtDtDalGru" + ind + ".focus()")
								return false
							} else {
								if (!ControllaDateRorga(ind,Oggi))
									return false
							}
						}
					} else {
						for (nEle=0 ; nEle<nGrupElem; nEle++) {
							if (eval("document.form1.chkIdGru" + ind + "[" + nEle + "].checked") ) {
								if ((eval("document.form1.chkRuo" + ind + ".disabled")) && (eval("document.form1.txtDtDalGru" + ind + "[" + nEle + "].value") != "")) {
									if (!ControllaDateGruppo(ind,Oggi)) 
										return false
								} else {
									if (eval("document.form1.txtDtDalGru" + ind + "[" + nEle + "].value ") == "") { 
										alert("2 - Seleccionar un rol organizativo o un grupo.")
										eval("document.form1.txtDtDalGru" + ind + "[" + nEle + "].focus()")
										return false
									} else {
										if (!ControllaDateGruppo(ind,Oggi)) 
											return false
									}
								}
							} else {
	
								if 	((eval("document.form1.txtDtDalGru" + ind + "[" + nEle + "].value") != "") && (!eval("document.form1.chkRuo" + ind + ".disabled"))) {
									alert("4 - Seleccionar un rol organizativo o un grupo.")
									eval("document.form1.txtDtDalGru" + ind + "[" + nEle + "].focus()")
								} else {
									if (!ControllaDateRorga(ind,Oggi))
										return false
								}
							
							}
						}
					}
				}	
				
			} else {
				if (!ControllaDateGruppo(ind,Oggi))
					return false
			}
		}
		return true
		//return false;
	}
				
			
	function validateIt(n, Oggi) { 
	// si controlla il numero dei ruoli selezionati per la fruizione del forum
		var R = 0	//indicatore del numero dei ruoli
		
		// Non gestiamo pi� l'inferiorit� della data
		// odierna perch� nella modifica/inserimento 
		// canale il controllo non permetteva di insierire 
		// un nuovo canale.
		Oggi = "01/01/1901"
		for (var ind=0 ; ind<n;ind++) {	
			if(eval("document.form1.chkRuo" + ind +".checked"))	{
				R = R + 1;
			}			
		}
		// Si controlla se l'utente vuole passare da un tipo di fruizione pubblica a una particolare
				
		if (document.form1.chkPS.value!="Pubblico")	//chkPs � != Pubblico se il forum  � pubblico
		{	
			if (R != 0 ) {
		//se l'utente seleziona uno dei canali disponibili essendo il forum di tipo pubblico
		//la sua scelta comporter� l'eliminazione del canale pubblico dal forum
				if (confirm("Seleccionando el siguiente canal ser� eliminado el canal P�blico de los usuarios de este foro. �Se desea proceder?")) {
					if (controllo_date(n-1, Oggi))	{
						 // document.form1.submit();
						return true;
					}else
						return false;
				
				} else
					return false;	
			} else {
				if (document.form1.txtDtDalPS.value == "")	{ 
					alert ("Indicar la fecha de inicio del periodo de utilizaci�n del grupo");
					document.form1.txtDtDalPS.focus();
					return false;
				}else	{ 
					if (ValidateRangeDate(document.form1.txtDtDalPS.value, Oggi)) {
						alert ("Fecha de inicio periodo de utilizaci�n del grupo inferior a la fecha de hoy");
						document.form1.txtDtDalPS.focus() ;
						return false;
					}
					if (!ValidateInputDate(document.form1.txtDtDalPS.value)) {
						document.form1.txtDtDalPS.focus() ;
						return false;
					}
				}
				if (document.form1.txtDtAlPS.value == "") {	
			/*		alert ("Indicare la data di fine periodo di fruizione del gruppo");
					document.form1.txtDtAlPS.focus();
					return false;
			*/	} else {	
					if (!ValidateInputDate(document.form1.txtDtAlPS.value)) {
		 				document.form1.txtDtAlPS.focus();
						return false;
					}
				}
				if (document.form1.txtDtDalPS.value != "" && document.form1.txtDtAlPS.value != "") {	
					if (!ValidateRangeDate(document.form1.txtDtDalPS.value, document.form1.txtDtAlPS.value)) {
						document.form1.txtDtAlPS.focus();
						alert("Controlar el intervalo entre las fechas indicadas."); 
						return false;
					}
				}	
			}
		}
				
		if (document.form1.chkPS.value =="Pubblico") //chkPs � = Pubblico se il forum  non � pubblico
		{
		//se l'utente seleziona il canale pubblico essendo il forum di tipo particolare
		//la sua scelta comporter� l'eliminazione dei canali dal forum
			if (document.form1.chkPN.checked) {
				if (confirm("Si se selecciona el canal �P�blico� ser�n eliminados todos los otros canales de uso disponibles. �Se desea proceder?")) {	
						
					if (document.form1.txtDtDalPN.value == "")	{
						alert ("Indicar la fecha de inicio del periodo de utilizaci�n del grupo");
						document.form1.txtDtDalPN.focus();
						return false;
					}
					else {
						if (ValidateRangeDate(document.form1.txtDtDalPN.value, Oggi))
						{
							alert ("Fecha de inicio periodo de utilizaci�n del grupo inferior a la fecha de hoy");
							document.form1.txtDtDalPN.focus() ;
							return false;
						}
							
						if (!ValidateInputDate(document.form1.txtDtDalPN.value)) {
							document.form1.txtDtDalPN.focus() ;
							return false;
							}
					}
							
					if (document.form1.txtDtAlPN.value == "") {
					/*		alert ("Indicare la data di fine periodo di fruizione del gruppo");
							document.form1.txtDtAlPN.focus();
							return false;
					*/	
					} else {
						if (!ValidateInputDate(document.form1.txtDtAlPN.value)) {
							document.form1.txtDtAlPN.focus();
							return false;
						}
					}
					return true;	
				}
				else
				{
					return false;
				}
			// se entrambi i campi data sono riempiti faccio un controllo per verificare il range della data.
				if (document.form1.txtDtDalPN.value != "" && document.form1.txtDtAlPN.value != "") {
					if (!ValidateRangeDate(document.form1.txtDtDalPN.value, document.form1.txtDtAlPN.value)) {
						document.form1.txtDtAlPN.focus();
						alert("Controlar el intervalo entre las fechas indicadas."); 
						return false;
					}
					else {
						//document.form1.submit();
						return true;
					}
				} else
					return false;	
			}
			
		}
				
		for (var ind=0 ; ind<n;ind++) {	
			if (!eval("document.form1.chkRuo" + ind +".checked")) { 
				if ((eval("document.form1.txtDtDal" + ind + ".value") != "" || eval("document.form1.txtDtAl" + ind + ".value") != "")) { 
						alert("Selezionare il canale di fruizione relativo alla data inserita")
						eval("document.form1.txtDtDal" + ind +".focus()");
						return false;
				}			
			}
		}

		if(!document.form1.chkPN.checked && (document.form1.txtDtDalPN.value != "" || document.form1.txtDtAlPN.value != ""))
			{ 
				alert("Selezionare il canale di fruizione relativo alla data inserita")
				document.form1.chkPN.focus();
				return false;
			}	
		if (controllo_date(n, Oggi)) {	
			//document.form1.submit();
			//return true;
		} else {
			return false;
		}
		
	return true;
}	
//--------------------------------------------------------------------------	
	function chktutte(ind)	{
	
		// Controllo se ci sono sotto elementi.
		if (eval("document.form1.chkIdGru" + ind )) { 

			if (eval("document.form1.chkRuo" + ind + ".checked"))
				chk = true
			else
				chk = false
				 
			// Controllo se i sotto elementi sono pi� di uno.
			if (eval("document.form1.chkIdGru" + ind + ".length")) {
				nNumEle = eval("document.form1.chkIdGru" + ind + ".length")
				for(i=0;i<nNumEle;i++){
					pippo = eval("document.form1.chkIdGru" + ind + "[" + i + "]")
					if ((!pippo.disabled) && (chk))
					pippo.checked = false
					//pippo.disabled = chk
				}		
			}/* else {
				pippo = eval("document.form1.chkIdGru" + ind )
//				if (!pippo.disabled)
//					pippo.checked = chk
				//pippo.disabled = chk
			}*/
		}			

	}
	
//-->
</script>
	<!-- FINE BLOCCO SCRIPT	-->
</head>
<!--	FINE BLOCCO HEAD	-->

<!--	BLOCCO ASP			-->
<%Sub Inizio()
	
	If not ValidateService(Session("IdUtente"),"USE_DOCUMENTALE",cc) Then 
		response.redirect "/util/error_login.asp"
	End If%>
<br>
<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
	<tr height="18">
		<td class="sfondomenu" width="67%" height="18"><span class="tbltext0"><b>&nbsp;Habilitaci�n documental-modifica</b></span></td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
		<td valign="middle" align="right" class="tbltext1" width="40%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
	</tr>
	<% if sModo = ""  Then %>
	<tr>
		<td class="sfondocomm" width="57%" colspan="3"><br>Usa el siguiente formato para modificar los derechos de la categor�a
			<a href="Javascript:Show_Help('/Pgm/help/Documentale/DOC_ModIscrizione')" name onmouseover="javascript:status='' ; return true">
		  <img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
      
		</td>
	</tr>
	<% End If %>
	<tr height="17">
		<td class="sfondocomm" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
	</tr>
</table>
<br>
<%End Sub

'--------------------------------------------------------------------------------------------------------------------------------------------------------->

Sub ImpostaPag()
	Dim rstCategoria, SQLCateg, n, i, SQLCountTades, rstCountTades
		Dim nID, sDescArea, sTitoloArea, sNomeCat, nProfCat, sAbstract
		
		nID = Request.Form("idA")
		
		SQLCateg = "SELECT Percorso, Descrizione, Abstract FROM Categoria WHERE Id_Categoria =" & nID 	
		Set rstCategoria = Server.CreateObject("ADODB.RECORDSET")
'PL-SQL * T-SQL  
SQLCATEG = TransformPLSQLToTSQL (SQLCATEG) 
		rstCategoria.open SQLCateg, CC, 2, 2
		
	If rstCategoria.eof Then
%>		<table border="0" cellspacing="0" cellpadding="0" width="570">
			<tr align="center"> 
				<td class="tbltext3"><b>P�gina momentaneamente no dsiponible.</b>
	            </td>
			</tr>
		</table>
<%			Exit Sub
	Else
			nProfCat = Ubound(split(rstCategoria("Percorso"),"\"))
			sNomeCat = split(rstCategoria("Percorso"),"\")(nProfCat)
			sDescArea = rstCategoria("Descrizione")
			sTitoloArea = sNomeCat
			sAbstract = rstCategoria("Abstract")
	End If
		
		rstCategoria.close
		Set rstCategoria = nothing
	
	aArray = decodTadesToArray("RORGA",Date(),"CODICE <> '" & Session("Rorga") & "'",1,"0")
	n = ubound(aArray) ' Rappresenta il numero di RORGA presenti.

	%>
<form Name="form1" ACTION="FOR_CnfIscrizione.asp" METHOD="POST" onsubmit="javascript:return validateIt(<%=n%>,'<%=Date() - 1%>')">
	<input type="hidden" name="Modo" value="1">
	<input type="hidden" name="n" value="<%=n%>">
	<input type="hidden" name="cat" value="<%=nID%>">
<table border="0" cellspacing="0" cellpadding="0" width="500">
	<tr>
		<td height="15" width="15"><img src="<%=Session("Progetto")%>/images/righinaPSX.jpg"></td> 
		<td align="left" class="tblsfondo3" width="150"><b class="tbltext0" VALIGN="MIDDLE">Nombre Canal&nbsp;</b> 
		</td>
		<td height="15" width="15"><img src="<%=Session("Progetto")%>/images/righinaPDX.jpg"></td> 
	     <td align="left" class="tbltext">&nbsp; <%=sTitoloArea%></td>
    </tr>
	<tr>
		<td colspan="4" height="2" width="15" valign="middle" background="<%=Session("Progetto")%>/images/separarighe.gif"></td> 
    </tr>
	<tr>
		<td height="15" width="15"><img src="<%=Session("Progetto")%>/images/righinaPSX.jpg"></td> 
		<td align="left" class="tblsfondo3" width="150">
			<b class="tbltext0" VALIGN="MIDDLE">Descripci�n Canal&nbsp;</b>
		</td>
		<td height="15" width="15"><img src="<%=Session("Progetto")%>/images/righinaPDX.jpg"></td> 
        <td align="left" class="tbltext">&nbsp;<%=sDescArea%></td>
	</tr>
</table>
<table border="0" cellspacing="0" cellpadding="0" width="500">
	<tr>
		<td colspan="4" height="2" width="15" valign="middle" background="<%=Session("Progetto")%>/images/separarighe.gif"></td> 
	</tr>
	<tr> 
		<td height="15" width="15"><img src="<%=Session("Progetto")%>/images/righinaPSX.jpg"></td> 
        <td class="tblsfondo3" height="23" align="center" width="470" colspan="2"><b class="tbltext0">Usuario del canal&nbsp; </b></td>
		<td height="15" width="15"><img src="<%=Session("Progetto")%>/images/righinaPDX.jpg"></td> 
	</tr>
</table>
<table border="0" cellspacing="2" cellpadding="1" width="500">
<%	if isArray(aArray) then
		for ind = 0 to (ubound(aArray) -1)
			sDtDal		 = ""
			sDtAl		 = ""
			sDtDalG		 = ""
			sDtAlG		 = ""
			sDisabled	 = ""
			sHiddenInput = ""
			
			sqlFrmIscr = "SELECT DT_DAL, DT_AL,IDGRUPPO FROM FRM_ISCRIZIONE " &_
			      " WHERE COD_RUOLO='" & aArray(ind,ind,ind) & "'" &_
			      " AND ID_FRM_AREA =" & nId
			set rsFrmIscr = server.createobject("adodb.recordset")
'PL-SQL * T-SQL  
SQLFRMISCR = TransformPLSQLToTSQL (SQLFRMISCR) 
			rsFrmIscr.Open sqlFrmIscr, CC, 2, 2
			if not rsFrmIscr.EOF then
				aFrmIscr = rsFrmIscr.GetRows() 
			else
				redim aFrmIscr(0,0)
				aFrmIscr(0,0) = null
			end if	
			sBotton = ""
			if not IsNull(aFrmIscr(0,0)) then
				if aFrmIscr(2,0) <> "" then ' E' solo relativo al rorga
					bRorga = false
					'sBotton = "readonly"

				else
					sDisabled		= " checked disabled "
					sDtDal = ConvDateToString(aFrmIscr(0,0))
					if isDate(aFrmIscr(1,0)) then
						sDtAl = ConvDateToString(aFrmIscr(1,0))
					end if
					bRorga = true		
				end if

				sHiddenInput	= "<input type='hidden' name='HchkRuo" &_
					 ind & "' value='" & aArray(ind,ind,ind) & "'>"
			else
				bRorga = true
			end if
%>		
		<tr>
			<td align="right" class="tblsfondo" width="20">
			<!--onclick="javascript:chktutte(<%'=ind%>)"-->
				<input type="checkbox" onclick="javascript:chktutte(<%=ind%>)" name="chkRuo<%=ind%>" id="chkRuo<%=ind%>" value="<%=aArray(ind,ind,ind)%>" <%=sDisabled%>>
					<%=sHiddenInput%>
			</td>
			<td align="left" class="tblsfondo" width="480" colspan="2">
				<span class="tbltext">&nbsp; <b><%=aArray(ind,ind+1,ind)%></b>&nbsp;</span>
			</td>
		
		</tr>
<%			sSql=" SELECT IDGRUPPO,DESGRUPPO,COD_RORGA FROM GRUPPO " &_
				"WHERE COD_RORGA = '" & aArray(ind,ind,ind) & "' ORDER BY DESGRUPPO"
			set rsRorga = Server.CreateObject("ADODB.Recordset")
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
			rsRorga.open sSql, CC, 2, 2

			sIDG = ""
			if not rsRorga.eof then 
				%>
				<input type="hidden" name="RorgaGru<%=ind%>" value="<%=aArray(ind,ind,ind)%>">									
				<%
				nConta = 0 
				do while not rsRorga.eof
					nConta = nConta + 1
					sDTDalG = ""
					sDTAlG  = ""
					sChecked = ""				
					if not bRorga then
						for i = 0 to UBound(aFrmIscr,2)
							if clng(aFrmIscr(2,i)) = clng(rsRorga("idgruppo")) then
								sDTDalG =  aFrmIscr(0,i)
								if IsDate(aFrmIscr(1,i)) then
									sDTAlG  =  ConvDateToString(aFrmIscr(1,i))
								end if
								sChecked = " checked disabled "
							end if
						next 
									
					end if

					nIdG = rsRorga("idgruppo")
					sDesGru = rsRorga("DESGRUPPO")%>						
					<input type="hidden" name="GruPos<%=nIdG%>" value="<%=nConta%>">									
					<tr align="right">
						<td width="10" class="tblsfondo">&nbsp;</td>
						<td align="right" class="tblsfondo" width="10">
							<input <%=sChecked%> type="checkbox" name="chkIdGru<%=ind%>" value="<%=nIdG%>">									
							<% if sDTDalG <> "" then%>
								<input type="hidden" name="CheckGru<%=ind%>" value="<%=nIdG%>">									
							<%end if%>
						</td>												
						<td align="left" class="tblsfondo" width="480" nowrap>
							<span class="tbltext">&nbsp;<%=sDesGru%>&nbsp;</span>
						</td>
					</tr>											
<%					sIDG = sIDG & nIdG & ","
					rsRorga.MoveNext()
				loop%>
					<input type="hidden" name="Gruppo<%=ind%>" value="<%=left(sIDG,len(sIDG)-1)%>">									

<%
			end if 
			rsFrmIscr.Close 
		next
		set rsFrmIscr = nothing
		erase aArray
	end if
	sql = "SELECT DT_DAL, DT_AL FROM FRM_ISCRIZIONE WHERE COD_RUOLO IS NULL " &_
		"AND ID_FRM_AREA = " & nId 

	set RR = server.createobject("ADODB.recordset")
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
	RR.Open sql, CC ,3
	if not RR.EOF then%>
		<tr>
			<td align="right" class="tblsfondo" width="20">
				<input type="checkbox" name="chkPN" value="XX" checked disabled>
				<input type="hidden" name="chkPS" value="XX">
			</td>
			<td colspan="2" align="left" class="tblsfondo" width="230"><span class="tbltext">&nbsp; P�blico&nbsp;</span></td>
		</tr>
	<%else%>
		<tr>
			<td align="right" class="tblsfondo" width="20">
				<input type="checkbox" name="chkPN" value="XX">
				<input type="hidden" name="chkPS" value="Pubblico">
			</td>
			<td align="left" colspan="2" class="tblsfondo" width="230"><span class="tbltext">&nbsp; P�blico&nbsp;</span></td>
		</tr>
	<%end if
	RR.Close
	set RR = nothing
	sql = ""%>
	</table>
	<br>	
	<table border="0" cellspacing="2" cellpadding="1" width="570">
		<tr> 
			<td colspan="3" align="center">
				<input type="image" src="<%=Session("progetto")%>/images/conferma.gif" title="Conferma" border="0" align="absBottom">
						<!--input type="button" value="Conferma" onClick="return validateIt(<%=n%>);" name="button"-->
			</td>
		</tr>
	</table>
	</form>
	<table border="0" cellspacing="0" cellpadding="0" width="570">
		<tr> 
			<td align="center"> 
				<a class="textred" href="/Pgm/formazione/forum/FOR_VisCanali.asp" onmouseover="window.status =' '; return true">
				<b>Vuelve a las Categor�as</b></a>
			</td>
		</tr>
	</table>
<%End Sub%>
<!--------------------------------------------------------------------------------------------------------------------------------------------------------->
<!--	FINE BLOCCO ASP		-->
<!--		MAIN			-->
<!--#include virtual ="/util/portallib.asp"-->
<!--#include virtual ="/strutt_testa2.asp"-->
<!--#include Virtual="/include/openconn.asp"-->
<!--#include Virtual="/include/ckProfile.asp"-->
<!--#include Virtual="/include/DecCod.asp"-->
<!--#include Virtual="/include/ControlDateVb.asp"-->
<%
Dim strConn, Rs, sql
Dim sDescr, sOggetto, n, nId

nID = clng(request("cat"))
Inizio()
ImpostaPag()%>	
<!--#include Virtual="/include/closeconn.asp"-->
<!--#include Virtual="/strutt_coda2.asp"-->
<!--	FINE BLOCCO MAIN	-->
<!--------------------------------------------------------------------------------------------------------------------------------------------------------->
