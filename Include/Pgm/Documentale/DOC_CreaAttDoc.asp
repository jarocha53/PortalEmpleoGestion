<%@ Language=VBScript %>
<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #include virtual="/strutt_testa2.asp"-->
<!-- #include virtual="/util/portallib.asp" -->
<!-- #include virtual="/include/OpenConn.asp" -->
<!-- #include virtual="/include/ckProfile.asp" -->
<!-- #include virtual="/include/ControlDateVB.asp" -->
<!-- #include virtual="/util/dbutil.asp" -->
<script language="javascript" src="/Include/help.inc"></script>

<%
	If ((Not ValidateService(Session("IdUtente"),"Gestione Notizie",CC)) And (Not ValidateService(Session("IdUtente"),"Sistema Documentale",CC))) Then 
		response.redirect "/util/error_login.asp"
	End If
	
	Server.ScriptTimeout = 90
%>
<!-- include del file per fare i controlli sulla validit� delle date -->
<script LANGUAGE="Javascript" src="/Include/ControlDate.inc"></script>
<script LANGUAGE="Javascript" src="/Include/ControlNum.inc"></script>
<script LANGUAGE="Javascript" src="/Include/ControlString.inc"></script>
<script LANGUAGE="Javascript">

function InitInvio() {
	InvioForm.Titolo.focus();	
}

function ControlloTitolo() {
	var i, codice_car;
	var codice = InvioForm.Titolo.value;
	var CtrlString = false;
	 for (i=0; i< codice.length; i++) {
	 codice_car = codice.charAt(i)
		 if (codice_car != " ") {
			CtrlString = true;
			break;
		}
	}
}

function ControllaCheck() {
if (!TRIM(InvioForm.Titolo.value)) {
	alert ("No es permitido el env�o sin haber elegido un t�tulo");
	InvioForm.Titolo.focus();
	return false 
	}
if (InvioForm.Titolo.value.length > 150){
	alert ("No es permitido el env�o de las noticias con un t�tulo mayor de 100 caracteres.");
	InvioForm.Titolo.focus();
	return false 
	}
if (InvioForm.Abstract.value.length > 400) {
	alert("El resumen no puede superar los 400 caracteres.\nEs largo " + InvioForm.Abstract.value.length + " caracteres");
	InvioForm.Abstract.focus();
	return false 
	}
sData = InvioForm.DataPubb.value
if (!ValidateInputDate(sData)){
		InvioForm.DataPubb.focus() 
		return false
	}
}
</script>
<%
IdCat = Request.QueryString("Id")
DocInviati = Request.QueryString("F")
aSepDocInviati = Split(DocInviati,".")	
If (DocInviati <> "") Then
	ExtDoc = aSepDocInviati(Ubound(aSepDocInviati))
	NomeDoc	= Left(DocInviati,Len(DocInviati)-(Len(ExtDoc)+1))
Else
	Response.Redirect("javascript:window.history.back()")
End If

If Request.QueryString("Up") <> "Ok" Then

	Set rstCategorie = Server.CreateObject("ADODB.Recordset")
	SQLCategoria = "SELECT Id_Categoria, Descrizione, Percorso FROM Categoria WHERE Id_Categoria=" & IdCat
'PL-SQL * T-SQL  
SQLCATEGORIA = TransformPLSQLToTSQL (SQLCATEGORIA) 
	rstCategorie.open SQLCategoria, CC, 1, 3

%>
<br>
<table border="0" cellpadding="0" cellspacing="0" width="500">
	<tr height="18">
		<td class="sfondomenu" width="67%" height="18"><span class="tbltext0"><b>&nbsp;AREA REDACCIONAL - <%=Session("Intestazione")%> </b></span></td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
		<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">(*) campi obbligatori</td>
	</tr>
	<tr>
		<td class="sfondocomm" width="57%" colspan="3"><br>Formulario para ingresar el documento <%=DocInviati%> en la categor�a <br><b><%=rstCategorie("Descrizione")%></b>. <br>
		Ingresar la descripci�n del documento.
		<a href="Javascript:Show_Help('/pgm/help/Documentale/DOC_CreaAttDoc')"><img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0" width="18" height="18" alt="Para mayores informaciones"></a>
	</td>
	</tr>
	<tr height="17">
		<td class="sfondocomm" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
	</tr>
</table>
<form Name="InvioForm" Method="post" Action="DOC_CreaAttDoc.asp?Up=Ok&amp;F=<%=DocInviati%>" onSubmit="return ControllaCheck()">
<table WIDTH="510" BORDER="0" CELLPADDING="2" CELLSPACING="2">
	<tr>
		<td WIDTH="150" class="tbltext1"><b>T�tulo*</b></td>	
		<td WIDTH="360"><input class="textblacka" Type="text" Name="Titolo" Size="50" Maxlength="50" onblur="ControlloTitolo()"></td>	
	</tr>
	<tr>
		<td WIDTH="150" class="tbltext1"><b>Resumen</b></td>	
		<td WIDTH="360"><textarea class="textblacka" Name="Abstract" rows="5" cols="49">Ingresar aqu� el texto</textarea></td>	
	</tr>
	<tr>
		<td WIDTH="150" class="tbltext1"><b>Fecha de publicaci�n</b></td>	
		<td WIDTH="360"><input class="textgray" Type="text" Name="DataPubb" Size="11" Value="<%=ConvDateToString(Date)%>" Maxlength="10" readonly></td>	
	</tr>
	<tr>
		<td WIDTH="150" class="tbltext1"><b>Fuente</b></td>	
		<td WIDTH="360"><input class="textblacka" Type="text" Name="Fonte" Size="30" Maxlength="30"></td>	
	</tr>
	<tr>
		<td WIDTH="150" class="tbltext1"><b>Autor</b></td>	
		<td WIDTH="360"><input class="textblacka" Type="text" Name="Autore" Size="30" Maxlength="30" Value="<%=Session("NomeUtente")%>" readonly></td>	
	</tr>
	<tr>
		<td colspan="2" WIDTH="510" align="center">
			<input type="image" src="<%=Session("Progetto")%>/images/conferma.gif" name="CreaNews"><br>
		</td>
	</tr>
	<tr>	
		<td><input Type="hidden" Name="TargetURL" Value="<%=rstCategorie("Percorso")%>"></td>
		<td><input Type="hidden" Name="Categoria" Value="<%=IdCat%>"></td>
	</tr>
</table>
</form>
<%
	rstCategorie.Close
%>
	<!-- #include virtual = "/include/closeconn.asp" -->
<%
Else	
' Ricavo id del tipo immagine relativa al tipo file		
	Set rstTipoDoc = Server.CreateObject("ADODB.Recordset")
		SQLTipoDoc= "SELECT Id_TipoDoc FROM TipoDoc WHERE Tipo = '" & Lcase(ExtDoc) & "'"
'PL-SQL * T-SQL  
SQLTIPODOC = TransformPLSQLToTSQL (SQLTIPODOC) 
		rstTipoDoc.open SQLTipoDoc, CC, 1, 3

	dtDataPubb = ConvStringToDate(Request.Form("DataPubb"))
	IdCat = Request.Form("Categoria")
	Titolo = Replace(Trim(Request.Form("Titolo")),"'","''")
	Abstract = Replace(Trim(Request.Form("Abstract")),"'","''")
	NomeDoc = Replace(Trim(NomeDoc),"'","''")
	DataIns = ConvDateToDbs(ConvDateToString(Date()))
	DataPubb = ConvDateToDbs(dtDataPubb)
	Fonte = Replace(Trim(Request.Form("Fonte")),"'","''")
	Autore = Session("NomeUtente")
	IdTipoDoc= rstTipoDoc("Id_TipoDoc")
' Aggiorno il DB 
	SQLNotizie = "INSERT INTO Notizie ( Id_Categoria, Titolo, Abstract, Data_Ins, Data_Pubb, Fonte, Autore, Nome_Notizie, Id_Tipodoc, FL_Pubblicato )" 
	SQLNotizie = SQLNotizie & "VALUES (" & IdCat & ",'" & Titolo & "','" & Abstract & "'," & DataIns & "," & DataPubb & ",'" & Fonte & "','" & Autore & "','" & NomeDoc & "','" & IdTipoDoc & "',0)" 
	on error resume next	
'PL-SQL * T-SQL  
SQLNOTIZIE = TransformPLSQLToTSQL (SQLNOTIZIE) 
	Set rstNotizie = CC.Execute(SQLNotizie)
	
	Response.Write "<FORM id='FormRis' Method='post' Action='DOC_Vis" & Session("Intestazione") & ".asp'>"
	Response.Write "<input type='hidden' name='Tema' value='" & Request.Form("Categoria") & "'>"
' Se non ho errore ritorno il messaggio corrispondente
' se ho errore cancello anche il file
	If Err.number = 0 Then		
			Response.Write"<TABLE width=510 border=0 cellpadding=2 cellspacing=2>"
			Response.Write"<tr><td align='center'><img src='/images/sistdoc/Upload/DocumentoOK.gif' width='400' height='260' alt='Registrazione Documento OK'></td></tr>"
			Response.Write"<tr><td colspan='2' class='tbltext3' align='center'><b>El documento &nbsp;<i class='textredb'>" & NomeDoc
			Response.Write"&nbsp;</i>ha sido registrado.</b></td></tr>"
			Response.Write "<tr><td align='center'><a href='" & Session("Progetto") & "' onClick='javascript:FormRis.submit();return false'>"
			Response.Write"<img alt='Atr�s' border='0' src='" & Session("Progetto") & "/images/indietro.gif'></a></td></tr>"
			Response.Write"</TABLE>"
			rstTipoDoc.Close
	Else
			Response.Write"<TABLE width=510 border=0 cellpadding=2 cellspacing=2>"
			Response.Write"<tr><td align='center'><img src='/images/sistdoc/Upload/DocumentoNOK.gif' WIDTH='400' HEIGHT='260' ALT='Registrazione Documento NOT OK'></td></tr>"
			Response.Write"<tr><td colspan='2' class='tbltext3' align='center'><b>El documento &nbsp;<i class='textredb'>" & NomeDoc
			Response.Write"&nbsp;</i>no ha sido registrado,<br> se ruega reenviarlo.</b></td></tr>"
			Response.Write"<tr><td colspan='2' class='tbltext' align='center'>Causa del error " & Err.description & "</td></tr>"
			Response.Write "<tr><td align=center><a href='" & Session("Progetto") & "' onClick='javascript:FormRis.submit();return false'>"
			Response.Write"<img alt='Atr�s' border='0' src='" & Session("Progetto") & "/images/indietro.gif'></a></td></tr>"
			Response.Write"</TABLE>"
			
			sDirCancella = Session("DirInit") & Request.Form ("TargetURL") 
			sFileCancella = Replace(sDirCancella,"/","\") & "\" & NomeDoc & "." & ExtDoc
			Dim fso, f		
			Set fso = CreateObject("Scripting.FileSystemObject")
			fso.DeleteFile(sFileCancella)
	End If	
	Response.Write"</FORM>"	
End If
%>
<!--#include virtual="/strutt_coda2.asp"-->
