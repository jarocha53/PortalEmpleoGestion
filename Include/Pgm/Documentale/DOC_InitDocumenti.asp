<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #include virtual="/util/portallib.asp" -->
<!-- #include virtual="/include/OpenConn.asp" -->
<!-- #include virtual="/include/ckProfile.asp" -->
<!-- #include virtual="/include/DecCod.asp" -->
<!-- #include virtual="/strutt_testa2.asp" -->
<%
If Request("progetto") > "" Then
	Session("Progetto") = Request("progetto")
End If
%>

<script language="javascript" src="/Include/help.inc"></script>
<%
sVisitor = False

If (Not ValidateService(Session("IdUtente"),"SISTEMA DOCUMENTALE",cc)) Then 
	If Session("login") = "" And ( session("rorga") <> "UT" Or session("rorga") <> "AZ") Then
		response.redirect "/util/error_login.asp"
	End If
	sVisitor = True
End If

Dim DirScelta, Posbarra, Pathname
Dim fso, f
' Dal mask ricavo il Tipo di categoria ed i permessi
TipoCatScelta = right(Session("mask"),1)
IdGruppo = left(Session("mask"),1)

If TipoCatScelta = "" or Session("mask") = "15" Then
	TipoCatScelta = "D"
End If
Session("Tipo") = TipoCatScelta
Session("GruppoDoc") = IdGruppo


'Response.Write "TipoCatScelta=" & TipoCatScelta
'Response.Write "Session mask =" & Session("mask")




If Session("Tipo") = "X" Then
	response.redirect "/util/error_login.asp"
End If

If sVisitor = True Then 
%>
<br>
	<table border="0" width="500" cellspacing="0" cellpadding="0" height="50">
		<tr>
			<td width="500" background="<%=session("progetto")%>/images/titoli/titolo.gif" height="50" valign="bottom" align="right">
				<table border="0" background width="450" height="30" cellspacing="0" cellpadding="0">
					<tr>
						<td width="100%" valign="top" align="right"><b class="tbltext1a">Documentaci�n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<br>
	<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
		<tr height="18">
			<td class="sfondomenu" width="67%" height="18"><span class="tbltext0"><b>&nbsp;DOCUMENTACION</b></span></td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
			<td valign="middle" align="right" class="tbltext1" width="40%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
		</tr>
		<tr>
			<td class="sfondocomm" width="57%" colspan="3"><br>Presionar el simbolo  
			del documento para poder consultar la documentacion  
			de los dem�s proyectos, de otro modo presionar sobre el nombre de la categor�a
			relativa al documento del proyecto en que se encuentra.<br>
			<a href="<%=Session("Progetto")%>" onclick="Javascript:Show_Help('/Pgm/help/Documentale/DOC_InitDocumenti'); return false">
			<img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0" width="18" height="18" alt="per maggiori informazioni"></a>
			</td>
		</tr>
		<tr height="17">
			<td class="sfondocomm" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
		</tr>
	</table><br>

<% Else %>
	<br>
	<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
		<tr height="18">
			<td class="sfondomenu" width="67%" height="18"><span class="tbltext0"><b>&nbsp;�REA EDITORIAL - <%=Session("Intestazione")%> </b></span></td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
			<td valign="middle" align="right" class="tbltext1" width="40%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
		</tr>
		<tr>
			<td class="sfondocomm" width="57%" colspan="3"><br>Es posible mandar, borrar o modificar documentos en el sistema documental. <br>
			Para ingresar, cancelar o modificar un documento en una categoria ya existente seleccionar la categoria del documento de la '<b>Lista de categorias</b>. <br>
			Para a�adir una nueva categor�a de documentos presionar <b>Crear Nueva Categoria</b>.<br>
			<a href="<%=Session("Progetto")%>" onclick="Javascript:Show_Help('/Pgm/help/Documentale/DOC_InitDocumenti'); return false">
			<img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a></td>
		</tr>
		<tr height="17">
			<td class="sfondocomm" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
		</tr>
	</table><br>
<% 
End If 

sProgettoORIGINAL = Session("Progetto")

If sVisitor = True Then
%>

	<table border="0" CELLPADDING="0" CELLSPACING="0" width="500" align="center">
		<tr height="18">
			<td class="sfondomenu" width="67%" height="18"><span class="tbltext0"><b>&nbsp;Lista de Proyectos con Documentacion</b></span></td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
			<td valign="middle" align="right" class="tbltext1" width="30%"></td>
		</tr>
	</table>
		
<%		
	aProgetto = decodTadesToArray("CPROJ",Date,"VALORE = 'S'",0,0)
	For i = 0 To UBound(aProgetto)-1%>
		<%aProgetto(i,i,i) = "/" & aProgetto(i,i,i)
		Session("Progetto") = aProgetto(i,i,i)%>	
		<table border="0" width="500" CELLPADDING="1" CELLSPACING="1">		
		<form name="frm<%=i%>" Method="POST" Action="DOC_InitDocumenti.asp">
			<input type="hidden" value="<%=aProgetto(i,i,i)%>" name="progetto">
			<tr class="tbltext1" height="18">
				<td colspan="3" width="500"><a href="<%=Session("Progetto")%>" onclick="Javascript:frm<%=i%>.submit();return false"><img src="<%=Session("Progetto")%>/images/txt.gif" border="0"></a> - <b>Documenti pubblici 
					relativi al sito <%=aProgetto(i,i+1,i)%></b>
				</td>
			</tr>	
		</form>
		</table>	
	<%
	Next
	Erase aProgetto
	Session("Progetto") = sProgettoORIGINAL
' Setto le directory di partenza
	PathDirInit = Server.MapPath("\") & Request("progetto") & "/Testi/sistdoc/Documentale/"
	DirPath = Session("Progetto") 
	Session("DirInit") = PathDirInit 
	Session("DirInitEdit") = DirPath & "/Testi/sistdoc/Documentale/"
	Session("Intestazione") = "Documenti"
	
	Set rstTipoCat = Server.CreateObject("ADODB.Recordset")
	SQLTipoCat = "SELECT Id_Categoria, Descrizione, Percorso, Abstract, Gif_categoria, Padre, Tipo FROM Categoria where Padre=0 and Tipo='D' And id_categoria in (SELECT id_categoria FROM Ruolo_Categoria WHERE Cod_rorga in ('XX','"& Session("Rorga") &"'))"
'PL-SQL * T-SQL  
SQLTIPOCAT = TransformPLSQLToTSQL (SQLTIPOCAT) 
	rstTipoCat.open SQLTipoCat, CC, 1, 3
	
	Session("NomeUtente") = Session("Nome") & "&nbsp;" & Session("Cognome")
	Session("Progetto") = sProgettoORIGINAL

	aProgetto = decodTadesToArrayCod("CPROJ",Date,"DESCRIZIONE= '" & Session("TIT") & "'",1,0)
	if isArray(aProgetto) then
		if not isNull(aProgetto(0)) then
			P = "/" & aProgetto(0)
		end if
		Erase aProgetto
	end if
%>
	<br>
	<table border="0" width="500" CELLPADDING="1" CELLSPACING="1">
		<tr height="18">
			<td colspan="3" align="right" width="500"><b><a class="textreda" href="<%=P%>/home.asp">Torna a <%=Session("TIT")%></a></b></td>
		</tr>
		<tr class="sfondocomm" height="18">
			<td colspan="3" width="500"><b>Lista de Categorias:</b></td>
		</tr>	
		<tr height="18" class="sfondocomm">
			<td width="215" colspan="2" height="18" align="center"><b>Nombre</b></td>
			<td width="285" height="18" align="center"><b>Descripcion</b></td>		
		</tr>		  
	<%
	  If rstTipoCat.EOF Then
		Response.Write "<tr class=tblsfondo height='18'><td align=center colspan=2><img src=/Images/SistDoc/bullet1.gif>&nbsp;&nbsp;&nbsp;</td>"
		Response.Write "<td class=tbltext><b>Nessuna categoria presente</b></td></tr>"
	  Else
		i = 0
	    Do While Not rstTipoCat.EOF
			i = i + 1
			IdCat = rstTipoCat("Id_Categoria")
			If isnull(rstTipoCat("Gif_categoria")) Then
			Response.Write "<tr class=tblsfondo><td width=50 align=center><img src=/Images/SistDoc/Bullet1.gif>"
			Else
			Response.Write "<tr class=tblsfondo><td width=50 align=center><img width=50 height=50 src="& Session("Progetto") & "/Images/Categorie/" & rstTipoCat("Gif_categoria") & ">&nbsp;"
			End If
	
	        Response.Write "</td><FORM Name='vidoc" & i & "' Method='post' Action='DOC_VisDocumenti.asp'>"
	        Response.Write "<input type='hidden' name='Tema' value='" & IdCat & "'>" 
	        Response.Write "<input type='hidden' name='progetto' value='" & Session("progetto") & "'>" 
	        Response.Write "<td width=170><a class=tbltext href='" & Session("Progetto") & "' onClick='Javascript:vidoc" & i & ".submit();return false'><b>" & rstTipoCat("Descrizione") & "</b></a></td></FORM>"
	    	Response.Write "<td width=280 class=tbltext>" & rstTipoCat("Abstract") & "</td></tr>"
	
			rstTipoCat.MoveNext
		Loop
	  End If
	  %>
	</table>
		
	<%

	rstTipoCat.Close
	Set rstTipoCat = nothing	
	
Else

	PathDirInit = Server.MapPath("\") & Session("Progetto") & "/Testi/sistdoc/Documentale/"

	DirPath = Session("Progetto") 

	Session("DirInit") = PathDirInit 
	Session("DirInitEdit") = DirPath & "/Testi/sistdoc/Documentale/"
	Session("Intestazione") = "Documenti"
	Set rstTipoCat = Server.CreateObject("ADODB.Recordset")
	SQLTipoCat = "SELECT Id_Categoria, Descrizione, Percorso, Abstract, Gif_categoria, Padre, Tipo FROM Categoria WHERE Padre=0 And Tipo='" & TipoCatScelta & "' ORDER BY Descrizione"

'PL-SQL * T-SQL  
SQLTIPOCAT = TransformPLSQLToTSQL (SQLTIPOCAT) 
	rstTipoCat.open SQLTipoCat, CC, 1, 3
	Session("NomeUtente") = Session("Nome") & "&nbsp;" & Session("Cognome")
	
%>
	<table border="0" width="500" CELLPADDING="1" CELLSPACING="1">
		<tr class="sfondocomm" height="18">
			<td colspan="4" width="500"><b>Lista de Categorias:</b></td>
		</tr>	
		<tr height="18" class="sfondocomm">
			<td width="160" colspan="2" height="18" align="center"><b>Nombre</b></td>
			<td width="260" height="18" align="center"><b>Descripcion</b></td>
			<td width="80" height="18" align="center"><b>Documentos Publicados</b></td>		
		</tr>
  <%
	If rstTipoCat.EOF Then
		Response.Write "<tr class=tblsfondo height='18'><td align=center colspan=2><img src=/Images/SistDoc/bullet1.gif>&nbsp;&nbsp;&nbsp;</td>"
		Response.Write "<td class=tbltext><b>Nessuna categoria presente</b></td><td>&nbsp;</td></tr>"
	Else
		i = 0
		Set rstNotizie = Server.CreateObject("ADODB.Recordset")
		Do While Not rstTipoCat.EOF
			IdCat = rstTipoCat("Id_Categoria")
			i = i + 1
			
			SQLnotizie = "SELECT Count(Id_Notizie) As NUM " &_ 
					 " FROM Notizie " &_
					 " WHERE Id_Categoria=" & rstTipoCat.Fields("Id_Categoria") &_
					 " AND Fl_Pubblicato = 0" 
'PL-SQL * T-SQL  
SQLNOTIZIE = TransformPLSQLToTSQL (SQLNOTIZIE) 
			rstNotizie.open SQLnotizie, CC, 1, 3
			nNonPubblicate= rstNotizie.Fields("NUM")
			rstNotizie.Close
			
			If isnull(rstTipoCat("Gif_categoria")) Then
				Response.Write "<tr class=tblsfondo><td width=50 align=center><img src=/Images/SistDoc/Bullet1.gif>"
			Else
				Response.Write "<tr class=tblsfondo><td width=50 align=center><img width=50 height=50 src="& Session("Progetto") & "/Images/Categorie/" & rstTipoCat("Gif_categoria") & ">&nbsp;"
			End If
				
			Response.Write "</td><form Name='visdoc" & i & "' Method='post' Action='DOC_VisDocumenti.asp'>"

			Response.Write "<input type=hidden name=Tema value=" & IdCat & ">" 
			Response.Write "<input type=hidden name=progetto value=" & Session("progetto") & ">" 

			Response.Write "<td width=160><a class=tbltext href='" & Session("Progetto") & "' onClick='Javascript:visdoc" & i & ".submit();return false'><b>" & rstTipoCat("Descrizione") & "</b></a></td>"
			Response.Write "<td width=260 class=tbltext>" & rstTipoCat("Abstract") & "</td></FORM>" 
			Response.Write "<td width='80' class='tbltext' align=center>" 
			if clng(nNonPubblicate) <> 0 then
				Response.Write nNonPubblicate 
			else
				Response.Write "&nbsp;"
			end if
			Response.Write "</TD></TR>"
			rstTipoCat.MoveNext
		Loop
		set rstNotizie = nothing
			
	End If
%>
	</table>
<%
	If TipoCatScelta <> "" and IdGruppo <> "" Then
%>

		<table border="0" width="500"> 
			<tr height="18" class="sfondocomm">
				<td colspan="3"><b>Carpetas de Administraci�n</b></td>
			</tr>
			<tr class="tblsfondo" height="20">
				<td width="50" align="center"><img src="/images/sistdoc/Bullet1.gif"></td>
				<td width="450" colspan="2"><a class="tbltext" href="DOC_CreaCartella.asp"><b>Crear Nueva Categoria</b></a></td>
			</tr>
		</table>
<%
	End if
	rstTipoCat.Close
	Set rstTipoCat = nothing	
End If
%>

<!-- #include virtual="/strutt_coda2.asp" -->
