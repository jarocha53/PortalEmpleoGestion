<%@ Language=VBScript %>
<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #include virtual="/strutt_testa2.asp"s-->
<!-- #include virtual="/util/portallib.asp" -->
<!-- #include virtual="/util/dbutil.asp" -->
<!-- #include virtual="/include/OpenConn.asp" -->
<!-- #include virtual="/include/ckProfile.asp" -->
<%
	If ((Not ValidateService(Session("IdUtente"),"Gestione Notizie",cc)) And (Not ValidateService(Session("IdUtente"),"Sistema Documentale",cc))) Then 
		response.redirect "/util/error_login.asp"
	End If
%>
<br>
<TABLE border="0" CELLPADDING="0" CELLSPACING="0" width="500">
	<tr height="18">
		<td class="sfondomenu" width="67%" height="18"><span class="tbltext0"><b>&nbsp;AREA REDACCIONAL - <%=Session("Intestazione")%> </b></span></td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
		<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
	</tr>
	<tr height=17>
		<td class="sfondocomm" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
	</tr>
</TABLE>
<br>
<%

IdNews = Request.Form("Nr")  
' Dato id ricavo i dati della notizia corrispondente
Set rstNotizie = Server.CreateObject("ADODB.Recordset")
	SQLnotizie = "SELECT Id_Notizie, Titolo, Abstract, Data_Pubb, Id_Categoria, Nome_Notizie, Autore, Id_Tipodoc FROM Notizie WHERE Id_Notizie=" & IdNews
'PL-SQL * T-SQL  
SQLNOTIZIE = TransformPLSQLToTSQL (SQLNOTIZIE) 
	rstNotizie.open SQLnotizie, CC, 1, 3

	IdCat = rstNotizie("Id_Categoria")
	NomeNews = rstNotizie("Nome_Notizie")
' Dato idcategoria ricavo il percorso dove si trova la notizia
	SQLCat = "SELECT Id_Categoria, Descrizione, Percorso FROM Categoria WHERE Id_Categoria=" & IdCat
'PL-SQL * T-SQL  
SQLCAT = TransformPLSQLToTSQL (SQLCAT) 
	Set rstCat = CC.Execute(SQLCat)	
	Campo = rstCat("Descrizione")
	sDirScelta = rstCat("Percorso")
	aSepDirScelta = split(sDirScelta,"\")
	
	rstCat.Close
	Set rstCat = nothing
	
Response.Write "<FORM id='FormRis' method='post' action='DOC_Vis" & Session("Intestazione") & ".asp'>"
Response.Write "<input type='hidden' name='Tema' value='" & IdCat & "'>"	
	
If Request.Form("P") <> "Ok" Then
	Dim DirCancella, sDirAsp
	Dim fso, f, fd, sf, f1, f2

    DirCancella = Session("DirInit") & sDirScelta
	sDirAsp= server.MapPath ("\") & session("progetto") & "\testi\info\infoprimolivello\"
	Set fso = CreateObject("Scripting.FileSystemObject")
' Controllo il tipo di file da eliminare
	If Session("Tipo") = "D" Then
		Set rstTipo = Server.CreateObject("ADODB.Recordset")
		SQLTipo = "SELECT Id_TipoDoc, Tipo FROM TipoDoc WHERE Id_TipoDoc="& rstNotizie("Id_Tipodoc")
'PL-SQL * T-SQL  
SQLTIPO = TransformPLSQLToTSQL (SQLTIPO) 
		rstTipo.open SQLTipo, CC, 1, 3
		sPathFile = DirCancella & "\" & NomeNews & "." & rstTipo("Tipo")
		sNomeGif = "DelFile.gif"	
		sTesto = "Ha sido eliminado el documento"
		rstNotizie.Close
		Set rstNotizie = nothing
	Else
		sPnomenews = mid(NomeNews,2)
' Controllo che il file sia comune o relativo ad un progetto in quanto cambia la directory
		If UCase(aSepDirScelta(0)) = "TESTI" then
			sPathFile = Server.MapPath("\") & "\" & sDirScelta & "\" & NomeNews & ".htm"
			sPathPFile = Server.MapPath("\") & "\" & sDirScelta & "\" & sPnomenews & ".htm"
			sPathAsp= Server.MapPath("\") & "\" & sDirScelta & "\" & sPnomenews & ".asp"
		Else
			sPathFile = DirCancella & "\" & NomeNews & ".htm"
			sPathPFile = DirCancella & "\" & sPnomenews & ".htm"
			sPathAsp= sDirAsp & sPnomenews & ".asp"
		End If
		sNomeGif = "DelNews.gif"
		sTesto = "Ha sido eliminada la noticia"
	End If
	sPathFileBak = sPathFile & ".bak"
' Inizio dei controlli per eliminare il file
	on error resume next
' Se redazionale devo eliminare anche la parte asp
	If session("Tipo")="R" or session("Tipo")="L" Then
		fso.DeleteFile(sPathAsp)
		fso.DeleteFile(sPathPFile)
	End If
' Se esiste la copia la elimino
	If fso.FileExists(sPathFileBak) Then 
		fso.DeleteFile(sPathFileBak)	
	End if 
' Elimino il file
	fso.DeleteFile(sPathFile)
	If err.number <> 0 Then
		Response.Write "<b class='tbltext'><br>Al momento no se puede cancelar la p�gina"
		Response.Write "<p>Contactar el Grupo Asistencia Portal Italia Lavoro<br>"
		Response.Write "a la direcci�n <a href='mailto:po-assistenza@italialavoro.it'>po-assistenza@italialavoro.it</a></p></b>"
		Response.End
	End If
' Eliminare il record sul DB
	SQLElNews = "DELETE FROM Notizie WHERE Id_Notizie=" & IdNews	
'PL-SQL * T-SQL  
SQLELNEWS = TransformPLSQLToTSQL (SQLELNEWS) 
	Set rstElNews = CC.Execute(SQLElNews)	
' Ritorno il messaggio corrispondente
	Response.Write "<TABLE WIDTH=510 CELLPADDING=2 CELLSPACING=2>"
	Response.Write "<tr><td align='center'><img src='/images/SistDoc/Upload/" & sNomeGif & "' Border='0' width='400' height='260' alt='Cancelaci�n news OK'></td></tr>"
	If session("Tipo")="R" Then
		Response.Write "<tr><td align='center' class='tbltext3'><b>" & sTesto & "<br><i class='textredb'>" & sPnomenews & ".asp</i></b></td></tr>"
	Else
		Response.Write "<tr><td align='center' class='tbltext3'><b>" & sTesto & "<br><i class='textredb'>" & NomeNews & "</i></b></td></tr>"
	End If
	Response.Write "<tr><td align='center'><a href='" & Session("Progetto") & "' onClick='javascript:FormRis.submit();return false'>"
	Response.Write "<img alt='Vuelve a' border='0' src='" & Session("Progetto") & "/images/indietro.gif'></a></td></tr>"
	Response.Write "</TABLE>"
	Set rstElNews = nothing
Else
' Aggiorno  il record per rendere pubbliche le modifiche o il file
	SQLPubblica = "UPDATE Notizie SET FL_Pubblicato = 1, Data_Pubb = " & ConvDateToDBs(Now()) & " WHERE Id_Notizie = " & IdNews
'PL-SQL * T-SQL  
SQLPUBBLICA = TransformPLSQLToTSQL (SQLPUBBLICA) 
	Set rstModNews = CC.Execute(SQLPubblica)
' Ritorno il messaggio corrispondente
	Response.Write "<TABLE WIDTH=510 CELLPADDING=2 CELLSPACING=2>"
	Response.Write "<tr><td align='center'><img src='/images/SistDoc/Upload/NewsPubblOK.gif' Border='0' width='400' height='260' alt='Pubblicazione News OK'></td></tr>"
	Response.Write "<tr><td align='center' class='tbltext3'><b>Ha sido publicada la noticia <i class='textredb'>" & NomeNews & "</i></b></td></tr>"
	Response.Write "<tr><td align='center'><a href='" & Session("Progetto") & "' onClick='javascript:FormRis.submit();return false'>"
	Response.Write "<img alt='Atr�s' border=0 src='" & Session("Progetto") & "/images/indietro.gif'></a></td></tr>"
	Response.Write "</TABLE>"
	Set rstModNews = nothing
End If
Response.Write "</FORM>"	
%>	
<!-- #include virtual="/include/closeconn.asp" -->
<!-- #include virtual="/strutt_coda2.asp" -->

