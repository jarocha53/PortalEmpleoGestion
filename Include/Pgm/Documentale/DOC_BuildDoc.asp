<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include Virtual="/strutt_testa1.asp"-->
<!-- #include virtual ="/util/portallib.asp" -->
<!-- #include virtual ="/include/OpenConn.asp" -->

<%
If Not ValidateService(Session("IdUtente"),"Sistema Redazionale",cc) Then 
	response.redirect "/util/error_login.asp"
End if
%>
<script language="javascript" src="/Include/help.inc"></script>
<script language="javascript" src="/Include/ControlString.inc"></script>
<script>
	function controllaDati() {
	var sNomedoc = document.creadoc.filename.value

		if (ChechEmptyString(sNomedoc)) {
			alert("Es necesario indicar el nombre del archivo")
			document.creadoc.filename.focus()
			return false
		}

		if (!ValidateInputStringIsValid(sNomedoc)){			
			return false
		}
		
		for ( var i=0; i<=sNomedoc.length-1; i++){
			if (sNomedoc.charAt(i) == " ") {
				alert("No est� permitido un nombre de archivo que contenga un espacio")
				return(false);
			}
		}
		return true  
	}
</script>
<%
' Dato idcategoria ricavo dati della categoria che passo con campi nascosti
IdTema = Request.Form ("D")
Set rstCategorie = Server.CreateObject("ADODB.Recordset")
SQLcategorie = "SELECT Id_Categoria, Descrizione, Percorso, Padre, Tipo FROM Categoria WHERE Id_Categoria =" & IdTema
'PL-SQL * T-SQL  
SQLCATEGORIE = TransformPLSQLToTSQL (SQLCATEGORIE) 
rstCategorie.open SQLcategorie,CC,1,3
sPath = rstCategorie("Percorso")
sSepPath = split(sPath,"/")
sDirName = UCase (sSepPath(Ubound(sSepPath)))
%>
<div align="center">
<br>
<table border="0" CELLPADDING="0" align="center" CELLSPACING="0" width="743">
	<tr height="18">
		<td class="sfondomenu" width="47%" height="18"><span class="tbltext0"><b>&nbsp;CREACION GUIADA NUEVO DOCUMENTO </b></span></td>
		<td width="2%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
		<td valign="middle" align="right" class="tbltext1" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
	</tr>
	<tr>
		<td class="sfondocomm" width="100%" colspan="3">
			Seleccionar la estructura deseada, indicar el nombre del file que contendr� el documento. 
			y despu�s presionar <b>env�a</b>.<br>
			Autom�ticamente al nombre del archivo ser� agregado el prefijo <b>info_</b> que caracteriza todas las p�ginas de explicaci�n del portal<br>
			La p�gina ser� visible en el portal en la direcci�n:<br>
			<b>http://<%=Request.ServerVariables("Server_Name")%><%=Session("Progetto")%>/Testi/Info/infoprimolivello/info_nomefile.asp</b>.
			<a href="Javascript:Show_Help('/pgm/help/Documentale/DOC_BuildDoc')" name onmouseover="javascript:status='' ; return true">
			<img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0" width="18" height="18" alt="Para mayores informaciones"></a>
		</td>
	</tr>
	<tr height="17">
		<td class="sfondocomm" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
	</tr>
</table>

<form method="post" id="docwiz" name="creadoc" onsubmit="return controllaDati()" action="/pgm/documentale/DOC_Building.asp">
<input type="hidden" name="dirpath" value="<%=sPath%>">
<input type="hidden" name="iddir" value="<%=IdTema%>">
<table class="tblsfondo" border="0" width="740" height="50" cellspacing="0" cellpadding="0">
	<tr>
		<td class="tbltext1" colspan="3" height="30"><b>La p�gina se ingresar� en la carpeta <%=sDirName%></b></td>
	</tr>
	<tr>
        <td align="middle" width="33%"><img src="/images/SistDoc/st1.gif" WIDTH="150" HEIGHT="137" alt="Modelo estructura 1"></td>
        <td align="middle" width="33%"><img src="/images/SistDoc/st2.gif" WIDTH="150" HEIGHT="137" alt="Modelo estructura 2"></td>
        <td align="middle" width="33%"><img src="/images/SistDoc/st3.gif" WIDTH="150" HEIGHT="137" alt="Modelo estructura 3"></td>
	</tr>	
	<tr>
		<td class="tbltext1" align="middle" width="33%" height="50"><b>Estructura  tipo 1</b><br><input checked type="radio" name="Str" value="ST1"></td>
        <td class="tbltext1" align="middle" width="33%" height="50"><b>Estructura  tipo 2</b><br><input type="radio" name="Str" value="ST2"></td>
        <td class="tbltext1" align="middle" width="33%" height="50"><b>Estructura  tipo 3</b><br><input type="radio" name="Str" value="ST3"></td>
	</tr> 
</table>

<table class="tblsfondo" border="0" width="740" height="50" cellspacing="0" cellpadding="0">
	<tr>
		<td width="500" class="tbltext1" align="center" vAlign="middle" height="50">
			<b>Ingresar el nombre del archivo:&nbsp;&nbsp;info_</b><input type="text" size="40" name="filename">
		</td>
		<td align="center">
			<input type="image" src="<%=Session("Progetto")%>/images/conferma.gif" alt="crea il file">&nbsp;&nbsp;
			<a href="Javascript:creadoc.reset()"><img src="<%=Session("Progetto")%>/images/annulla.gif" border="0" alt="anula la selecci�n del nombre"></a>&nbsp;&nbsp;
			<a href="Javascript:history.back()"><img src="<%=Session("Progetto")%>/images/indietro.gif" border="0" alt="Vuelve atr�s"></a>
		</td>
	</tr>
</table>
</form>	
</div>
<%
rstCategorie.Close
Set rstCategorie = nothing 
%>
<!-- #include virtual="/include/closeconn.asp" -->
<!--#include Virtual="/strutt_coda1.asp"-->

