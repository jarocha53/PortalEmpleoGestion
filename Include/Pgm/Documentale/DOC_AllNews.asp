<%@ Language=VBScript %>
<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #include virtual="/strutt_testa2.asp" -->
<!-- #include virtual="/util/portallib.asp" -->
<!-- #include virtual="/include/OpenConn.asp" -->
<!-- #include virtual="/include/ckProfile.asp" -->
<!-- #include virtual="/include/ControlDateVB.asp" -->

<script language="javascript" src="/include/help.inc"></script>
<%
If ((Not ValidateService(Session("IdUtente"),"Gestione Notizie",cc)) And (Not ValidateService(Session("IdUtente"),"Sistema Documentale",cc))) Then 
	response.redirect "/util/error_login.asp"
End If

Dim nActPagina
Dim nTotPagina
Dim nTamPagina		
Dim nTotRecord
'Variabile per i numeri di notizie da visualizzare per pagina
nTamPagina = 5
'Variabile per i numeri pagina
If Request.Form("Page") = "" Then
	nActPagina = 1
Else
	nActPagina = Clng(Request.Form("Page"))
End If
	
Idtema = Request.Form("Id")

Dim DirScelta, Posbarra, Pathname
Dim fso, f
' Dato idcategoria ricavo dati della categoria
Set rstCategorie = Server.CreateObject("ADODB.Recordset")
SQLcategorie = "SELECT Id_Categoria, Descrizione, Percorso, Padre, Tipo FROM Categoria WHERE Id_Categoria =" & IdTema
'PL-SQL * T-SQL  
SQLCATEGORIE = TransformPLSQLToTSQL (SQLCATEGORIE) 
rstCategorie.open SQLcategorie,CC,1,3

If rstCategorie("Tipo") <> "N" Then
	Response.Redirect "/pgm/documentale/DOC_InitNews.asp"
End if
	
	DirScelta = rstCategorie("Percorso")
	Pathname = Session("DirInit") & DirScelta
	
	FileEdit = Session("DirInitEdit") & DirScelta & "\" & Session("IdUtente") & ".htm"	

	Set fso = CreateObject("Scripting.FileSystemObject")
	on error resume next
	Set f = fso.GetFolder(Pathname) 
	If err.number <> 0 Then
		Response.Write "<b class='tbltext'><br>En este momento no se puede visualizar la p�gina"
		Response.Write "<p>Contactar el Grupo Asistencia Portal Italia Lavoro<br>"
		Response.Write "a la direcci�n <a href='mailto:po-assistenza@italialavoro.it'>po-assistenza@italialavoro.it</a></p></b>"
		Response.End
	End If
' Dato idcategoria ricavo dati della notizia
    Set rstNotizie = Server.CreateObject("ADODB.Recordset")
	SQLnotizie = "SELECT Id_Notizie, Titolo, Abstract, Data_Ins, Id_Categoria, Nome_Notizie, Gif_HP, Fl_Pubblicato FROM Notizie WHERE Id_Categoria="& IdTema & " ORDER BY Id_Notizie Desc"
'PL-SQL * T-SQL  
SQLNOTIZIE = TransformPLSQLToTSQL (SQLNOTIZIE) 
	rstNotizie.open SQLnotizie, CC, 1, 3
	
%>
<br>
<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
	<tr height="18">
		<td class="sfondomenu" width="67%" height="18"><span class="tbltext0"><b>&nbsp;AREA REDACCIONAL </b></span></td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
		<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
	</tr>
	<tr>
		<td class="sfondocomm" width="57%" colspan="3"><br> Para ingresar una nueva noticia presionar sobre <b>Nueva Noticia</b>. <br>
		Para visualizar o modificar una Noticia ya existente presionar sobre el <b>T�tulo</b> correspondiente. </span>
		<a href="Javascript:Show_Help('/pgm/help/Documentale/DOC_AllNews')">
		<img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0" width="18" height="18" alt="Para mayores informaciones"></a></td>
	</tr>
	<tr height="17">
		<td class="sfondocomm" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
	</tr>
</table>

<!-- #include file="DOC_VisBarra.asp" -->
<table WIDTH="510" BORDER="0" CELLPADDING="2" CELLSPACING="2">
	<tr>
        <td class="sfondocomm" align="center" colspan="4"><b>Todas la Noticias  sobre el  argumento <%=rstCategorie("Descrizione")%></b></td>
	</tr>
	<tr class="sfondocomm">
        <td width="40" align="center"><b>Foto</b></td>
        <td width="200" align="center"><b>Titulo</b></td>
        <td width="250" align="center"><b>Resumen</b></td>
        <td width="20" align="center"><b>Pubb</b></td>
	</tr>
<% 
 ' Gestione paginazione
    rstNotizie.PageSize = nTamPagina
	rstNotizie.CacheSize = nTamPagina

	nTotPagina = rstNotizie.PageCount 
 
	If nActPagina < 1 Then
		nActPagina = 1
	End If
	If nActPagina > nTotPagina Then
		nActPagina = nTotPagina
	End If

	rstNotizie.AbsolutePage=nActPagina
	nTotRecord=0
	While rstNotizie.EOF <> True And nTotRecord < nTamPagina 
    	
		PathGif = Session("DirInitEdit") & DirScelta & "\" & rstNotizie("Gif_HP")
    %>
    <tr class="tblsfondo">
        <td class="tbltext">
			<% If isnull(rstNotizie("Gif_HP")) Then %>
				&nbsp;
			<% Else %>
				<img src="<%=Replace(PathGif,"\","/")%>" width="40" height="40">
			<% End If %>
        </td>
        <td class="tbltext"><a class="tbltext" href="DOC_LeggiNews.asp?Nr=<%=rstNotizie("Id_Notizie")%>"><b><%=rstNotizie("Titolo")%></b></a></td>
        <td class="tbltext"><%=rstNotizie("Abstract")%></td>
        <td align="center">
			<% If cint(rstNotizie("Fl_Pubblicato")) = 1 Then %>
				<img src="<%=session("Progetto")%>/images/visto.gif" width="18" height="18" alt="notizia pubblicata">
			<% Else %>
				&nbsp;
			<% End If %>
        </td>
    </tr>
    <% 
   
	 nTotRecord=nTotRecord + 1	
	 rstNotizie.MoveNext
 Wend
	%>
</table>
<%	
Response.Write "<TABLE border=0 width=500>"
Response.Write "<tr>"
If nActPagina > 1 Then
	Response.Write "<FORM Name='PageDown' Method='post' Action='DOC_AllNews.asp'>"
	Response.Write "<input type='hidden' name='Id' value='" & Idtema & "'>"
	Response.Write "<input type='hidden' name='Page' value='" & nActPagina-1 & "'>"
	Response.Write "</FORM>"
	Response.Write "<td align='right' width='480'>"
	Response.Write "<a class=tbltext href='" & Session("Progetto") & "' onClick='Javascript:PageDown.submit();return false'>"
	Response.Write "<img border='0' alt='P�gina anterior' src='" & Session("Progetto") & "/images/precedente.gif' alt='Ir a la p�gina anterior'></a></td>"
End If
If nActPagina < nTotPagina Then
	Response.Write "<FORM Name='PageUp' Method='post' Action='DOC_AllNews.asp'>"
	Response.Write "<input type='hidden' name='Id' value='" & Idtema & "'>"
	Response.Write "<input type='hidden' name='Page' value='" & nActPagina+1 & "'>"
	Response.Write "</FORM>"
	Response.Write "<td align='right'>"
	Response.Write "<a class=tbltext href='" & Session("Progetto") & "' onClick='Javascript:PageUp.submit();return false'>"
	Response.Write "<img border='0' alt='P�gina sucesiva' src='" & Session("Progetto") & "/images/successivo.gif' alt='Ir a la p�gina sucesiva'></a></td>"
End If
Response.Write "</tr></TABLE>"
	
rstCategorie.Close
Set rstCategorie = nothing 
rstNotizie.Close
Set rstNotizie = nothing 
%>
<!-- #include virtual="/include/closeconn.asp" -->
<!-- #include virtual="/strutt_coda2.asp"-->
