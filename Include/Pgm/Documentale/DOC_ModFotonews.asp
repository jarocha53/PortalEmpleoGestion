<%@ Language=VBScript %>
<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #include virtual="/strutt_testa2.asp" -->
<!-- #include virtual="/util/portallib.asp" -->
<!-- #include virtual="/include/OpenConn.asp" -->
<!-- #include virtual="/include/ckProfile.asp" -->

<%
	If((Not ValidateService(Session("IdUtente"),"Gestione Notizie",cc)) And (Not ValidateService(Session("IdUtente"),"Sistema Documentale",cc))) Then 
		response.redirect "/util/error_login.asp"
	End If

IdNews = Request.QueryString("Nr")
' Dato id ricavo dati sulla notizia specifica
SQLnotizie = "SELECT Nome_Notizie FROM Notizie WHERE Id_Notizie=" & IdNews 
'PL-SQL * T-SQL  
SQLNOTIZIE = TransformPLSQLToTSQL (SQLNOTIZIE) 
Set rstNotizie = CC.Execute(SQLnotizie)

If Request.QueryString("Dir") = "" then

NumFoto = Request.Form("ModFoto")
IdCat = Request.Form("Categoria")
DirScelta = Request.Form("TArgetUrl") & "\"
PathDir = replace(Session("DirInit") & DirScelta, "/", "\")
%>
<script LANGUAGE="Javascript" src="/Include/help.inc"></script>
<script language="Javascript">

	function CheckFile()
	{
		nNumFoto = FotoForm.txtNumFoto.value
		switch (nNumFoto){
		case ("0"):
			if ((FotoForm.F1.value == "") || (FotoForm.F2.value == "")) 
			{	
				alert ("Atenci�n! Debe seleccionar ambas fotos");
				return false
			}
			break;
		case ("1"):
			if ((FotoForm.F1.value == "")) 
			{	
				alert ("Atenci�n! Debe seleccionar la foto");
				return false
			}
			break;
		
		case ("2"):
			if ((FotoForm.F2.value == "")) 
			{	
				alert ("Atenci�n! Debe seleccionar la foto");
				return false
			}
			break;
		}
		return true
	}	
</script>
<br>
<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
	<tr height="18">
		<td class="sfondomenu" width="67%" height="18"><span class="tbltext0"><b>&nbsp;AREA REDACCIONAL - <%=Session("Intestazione")%></b></span></td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
		<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
	</tr>
	<tr>
		<td class="sfondocomm" width="57%" colspan="3">
			<br>Sustituir las fotos de las noticias <b><%=rstNotizie("Nome_Notizie")%></b>.<br>
			Presionar <b>Env�a</b> para confirmar los cambios.
			<a href="Javascript:Show_Help('/Pgm/help/Documentale/DOC_ModFotoNews')" name onmouseover="javascript:status='' ; return true">
			<img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0" width="18" height="18" alt="Para mayores informaciones"></a>
       </td>
	</tr>
	<tr height="17">
		<td class="sfondocomm" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
	</tr>
</table>
<form enctype="multipart/form-data" method="post" name="FotoForm" onsubmit="return CheckFile()" action="DOC_ModFotonews.asp?Dir=<%=PathDir%>&amp;Nr=<%=IdNews%>&amp;Cod=<%=NumFoto%>">
<input type="hidden" name="txtNumFoto" value="<%=NumFoto%>">
<table BORDER="0" CELLPADDING="2" CELLSPACING="2" width="500">		
<%	If (NumFoto = 1) Or (NumFoto = 0) Then %>
	<tr class="sfondocomm">
		<td VALIGN="TOP" COLSPAN="2">
			<br><b>N.B.: La foto enviada debe satisfacer las siguientes caracter�sticas:</b>
		    <ul>
				<li>Formatos permitidos: jpg o gif.
				<li>Dimensiones: 50x50 pixel.
				<li>Tama�o m�ximo: 100 Kb.
			</ul>
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td WIDTH="100" class="tbltext1"><b>Foto Peque�a:</b></td>
		<td WIDTH="450"><input SIZE="40" TYPE="FILE" NAME="F1"></td>
	</tr>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
<%	End If
	If (NumFoto = 2) Or (NumFoto = 0) Then %>
	 <tr class="sfondocomm">
		<td VALIGN="TOP" COLSPAN="2">
			<br><b>N.B.: La foto enviada debe satisfacer las siguientes caracter�sticas:</b>
		    <ul>
				<li>Formatos permitidos: jpg o gif.
				<li>Dimensiones: 200x100 pixel.
				<li>Tama�o m�ximo: 100 Kb.
			</ul>
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td WIDTH="100" class="tbltext1"><b>Foto Grande:</b></td>
		<td WIDTH="450"><input SIZE="40" TYPE="FILE" NAME="F2"></td>
	</tr>
<%	End If %>
	<tr>
		<td VALIGN="TOP" ALIGN="CENTER" COLSPAN="2">
			<table BORDER="0" width="500">
				<tr>
					<td width="250" align="right">
						<input type="image" src="<%=Session("progetto")%>/images/conferma.gif" title="Invia" name="submit" border="0">
					</td>
					<td>
						<a href="<%=Session("Progetto")%>" onClick="javascript:history.back();return false"><img src="<%=Session("progetto")%>/images/indietro.gif" border="0"></a>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</form>
<%
Else
' Eseguo invio file
	Set upl = Server.CreateObject("Dundas.Upload.2")

		upl.UseUniqueNames = False
		upl.Save Request.QueryString("Dir")

	For Each objFile in upl.Files
		SizeFileUp = objFile.size
		If SizeFileUp > 110000 Then	
			objFile.delete
			Response.Write "<TABLE width='510' border='0' cellpadding='2' cellspacing='2'>"
			Response.Write "<tr><td align='center'><img src='/images/sistdoc/Upload/FotoNOK.gif' width='400' height='260'></td></tr>"
			Response.Write "<tr><td colspan='2' class='tbltext3' align='center'><b>La foto enviada tiene una dimensi�n mayor de la permitida</b></td></tr>"
			Response.Write "<tr><td align='center'><a href='javascript:history.back()'>"
			Response.Write "<img alt='Atr�s' border='0' src='" & Session("Progetto") & "/images/indietro.gif'></a></td></tr>"
			Response.Write "</TABLE>"
			Response.End
		Else
			NomeDoc = upl.GetFileName(objFile.OriginalPath)
			NomeDoc = Replace(NomeDoc,"'","''")
			Fotoinviate = Fotoinviate & "$" & NomeDoc
		End if
	Next
	
	Set upl = Nothing

	SQLnotizie = "SELECT Nome_Notizie FROM Notizie WHERE Id_Notizie=" & IdNews 
'PL-SQL * T-SQL  
SQLNOTIZIE = TransformPLSQLToTSQL (SQLNOTIZIE) 
	Set rstNews = CC.Execute(SQLnotizie)
' Aggiorno il DBa
	TIpoScelta = Request.QueryString("Cod")
	aSepFotoinviate = Split(Fotoinviate,"$")
	If TipoScelta = 1 Then
	   NomeFoto = aSepFotoinviate(1)
	   SqlModAtt = "UPDATE Notizie SET Gif_HP ='" & NomeFoto & "' WHERE ID_Notizie = " & IdNews
'PL-SQL * T-SQL  
SQLMODATT = TransformPLSQLToTSQL (SQLMODATT) 
	   Set rstModNews = CC.Execute(SqlModAtt)
	End if
	
	If TipoScelta = 2 Then
	   NomeFoto = aSepFotoinviate(1)
	   SqlModAtt = "UPDATE Notizie SET Gif_Notizie ='" & NomeFoto & "' WHERE ID_Notizie = " & IdNews
'PL-SQL * T-SQL  
SQLMODATT = TransformPLSQLToTSQL (SQLMODATT) 
	   Set rstModNews = CC.Execute(SqlModAtt)
	End if
	
	If TipoScelta = 0 Then
	   NomeFoto1 = aSepFotoinviate(1)
	   NomeFoto2 = aSepFotoinviate(2)
	   SqlModAtt = "UPDATE Notizie SET Gif_HP ='" & NomeFoto1 &_
				    "', Gif_Notizie ='" & NomeFoto2 & "' WHERE Id_Notizie = " & IdNews
'PL-SQL * T-SQL  
SQLMODATT = TransformPLSQLToTSQL (SQLMODATT) 
	   Set rstModNews = CC.Execute(SqlModAtt)
	End if
' Ritorno il messaggio
	Response.Write "<TABLE width='500' border='0' cellpadding='2' cellspacing='2'>"
	Response.Write "<tr><td align='center'><img src='/images/sistdoc/Upload/ModNewsOK.gif' width='400' height='260'></td></tr>"
	Response.Write "<tr><td colspan=2 align=center class=tbltext3><b>La foto relativa a la noticia&nbsp;<i class='textredb'>" & rstNews("Nome_Notizie")
	Response.Write "&nbsp;</i>ha sido modificada.</b></td></tr>"
	Response.Write "<tr><td align='center'><a href=DOC_LeggiNews.asp?Nr=" & IdNews
	Response.Write "><img alt='Atr�s' border='0' src='" & Session("Progetto") & "/images/indietro.gif'></a></td></tr>"
	Response.Write "</TABLE>"
	
End if
%>
<!--#include virtual="/strutt_coda2.asp"-->
