<%@ Language=VBScript %>
<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #include virtual="/strutt_testa2.asp" -->
<!-- #include virtual="/util/portallib.asp" -->
<!-- #include virtual="/include/OpenConn.asp" -->
<!-- #include virtual="/include/ckProfile.asp" -->

<script language="javascript" src="/Include/help.inc"></script>

<%
	If ((Not ValidateService(Session("IdUtente"),"Gestione Notizie",cc)) And (Not ValidateService(Session("IdUtente"),"Sistema Documentale",cc))) Then 
		response.redirect "/util/error_login.asp"
	End If
%>

<script language="javascript">

function CtrlForm() {
	if (ModDir.Descrizione.value == ""){
		alert ("No est� permitido el env�o sin haber ingresado la descripci�n del argumento.");
		return false 
	}
	if (ModDir.Descrizione.value.length > 50){
	alert("La descripci�n del argumento no puede ser mayor de 50 caracteres.\nPor ahora es larga " + ModDir.Descrizione.value.length + " caracteres.");
	ModDir.Descrizione.focus();
	return false 
	}
	if (ModDir.AbstractDir.value.length > 160) {
	alert("El resumen no puede superar los 160 caracteres.\nEs largo " + ModDir.AbstractDir.value.length + " caracteres");
	ModDir.AbstractDir.focus();
	return false 
	}
}
</script>
<%
dim sGifCat,sAltCat

nIdCat = Request.Form("Id")
' Dato id ricavo i dati della categoria corrispondente
SqlCat = "SELECT Id_Categoria, Descrizione, Percorso, Abstract, Padre, Gif_categoria,alt_categoria FROM Categoria WHERE Id_Categoria=" & nIdCat
'PL-SQL * T-SQL  
SQLCAT = TransformPLSQLToTSQL (SQLCAT) 
Set rstCat = CC.Execute(SqlCat)

	sGifCat = rstCat("Gif_categoria")
	sAltCat = rstCat("alt_categoria")
	nIdPadre = rstCat("Padre")	
	sCampo = rstCat("Descrizione")
	sSintesi = rstCat("Abstract")
	sDirScelta = rstCat("Percorso")
	aSepDir = Split(sDirScelta,"\")
	nProfDir = Ubound(aSepDir)
	sDirName = aSepDir(nProfDir)
%>
<%
'-------------selezione pagina help----------------------------
sTipoDoc=Session("Tipo")
'Response.Write sTipoDoc
Select Case sTipoDoc

	Case "N" sNomCart="doc_modcartella"
	
	Case "D" sNomCart="doc_modcartella_1"
	
	Case "R" sNomCart="doc_modcartella_2"
	
End Select
'-------------------------------------------------------------
%>

<br>
<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
	<tr height="18">
		<td class="sfondomenu" width="67%" height="18"><span class="tbltext0"><b>&nbsp;AREA REDACCIONAL - <%=Session("Intestazione")%> </b></span></td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
		<td valign="middle" align="right" class="tbltext1" width="40%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
	</tr>
	<tr>
		<td class="sfondocomm" width="57%" colspan="3"><br>Modifica descripci�n de la categor�a <b><%=rstCat("Descrizione")%></b>. <br> 
		Presionar sobre <b>Agrega</b> para ingresar o cambiar la imagen asociada a la categor�a.
		<a href="Javascript:Show_Help('/Pgm/help/Documentale/<%=sNomCart%>')" name onmouseover="javascript:status='' ; return true">
		<img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0" width="18" height="18" alt="Para mayores informaciones"></a></td>
	</tr>
	<tr height="17">
		<td class="sfondocomm" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
	</tr>
</table>
<br>

<%
If (Request.Form("Ch") <> "Ok") Then
' Controllo che la cartella sia in modifica oppure in eliminazione
	If (Request.Form("Cod") = "M") Then	    
		If (nIdCat <> 0) Then
			Response.Write "<TABLE border='0' width='500' cellspacing='0' cellpadding='0'>"
			Response.Write "<tr><td width=150 class='tbltext1'><b>Ubicaci�n Actual:</b></td>"
			Response.Write "<td width='350' class='textblacka'>" & sDirScelta & "</td></tr></TABLE>"
		End If    
%>
		<form Method="POST" Action="DOC_ModCartella.asp" id="ModDir" onSubmit="return CtrlForm()">
		<input type="hidden" name="Id" value="<%=nIdCat%>">
		<input type="hidden" name="Ch" value="Ok">		
		<table BORDER="0" width="500">
			<tr>
				<td width="150" class="tbltext1"><b>Descripci�n Argumento</b></td>	
				<td width="350">
					<input class="textblacka" type="text" name="Descrizione" size="40" maxlength="50" value="<%=sCampo%>">
				</td>
			</tr>
			<tr>
				<td width="150" class="tbltext1"><b>Resumen Argumento:</b></td>	
				<td width="350"><textarea class="textblacka" name="AbstractDir" rows="4" cols="40"><%=sSintesi%></textarea></td>	
			</tr>
			<%if sGifCat <> "" then%>
			<tr>
				<td width="150" class="tbltext1"><b>Texto foto </b></td>	
				<td><input class="textblacka" Type="text" Name="txtAltFoto" Size="40" Maxlength="50" value="<%=sAltCat%>"></td>
			</tr>
			<%end if%>
			<tr>
				<td width="500" align="center" colspan="2">
					<table BORDER="0" width="500">
						<tr>
							<td width="250" align="right">
								<input type="image" src="<%=Session("progetto")%>/images/conferma.gif" title="Modifica carpeta" name="submit" border="0">
							</td>
							<td>
								<a href="<%=Session("Progetto")%>" onClick="javascript:history.back();return false"><img src="<%=Session("progetto")%>/images/indietro.gif" border="0"></a>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		</form>
<%	' Controllo che la categoria non sia di tipo redazionale in quanto non ha immagine associata	
		If Session("Tipo") <> "R" Then %>
			<form id="InsImg" Method="POST" Action="DOC_InviaImgDoc.asp?Id=<%=nIdCat%>">
			<table WIDTH="500">
				<tr>
					<td align="center" class="tbltext3" width="400">Asocia/Modifica imagen de la Categor�a</td>
					<td align="left">
						<input type="image" src="<%=Session("progetto")%>/images/aggiungi.gif" name="aggiungi">
					</td>
				</tr>
				<tr>
					<td align="center"><input Type="Hidden" Name="DirAggior" Value="<%=sDirScelta%>"></td>
				</tr>
			</table>
			</form>
			<%If not IsNull(sGifCat) Then%>
			<form id="ElImg" Method="post" Action="DOC_ElImgDoc.asp">
			<table WIDTH="500">
				<input type="hidden" name="idCat" value="<%=nIdCat%>">	
				<tr>
					<td align="center" class="tbltext3" width="400">Elimina imagen de la categor�a</td>
					<td align="left">
						<input type="image" src="<%=Session("progetto")%>/images/elimina.gif" name="aggiungi">
					</td>
				</tr>
			</table>
			</form>
			<%
			End If
		End If	 
	Else
		Dim sDirCancella
		Dim fso, f, fd, sf, f1, f2
		
		aSepDirScelta = split(sDirScelta,"\")
' Controllo che la categoria sia capostipite e sia comune o relativa ad un progetto			
		If (nIdCat <> 0) then
			If UCase(aSepDirScelta(0)) <> "TESTI" then
				sDirCancella = Session("DirInit") & sDirScelta
			Else
				sDirCancella = Server.MapPath("\") & "\" & sDirScelta 
			End If
		Else
			If UCase(aSepDirScelta(0)) <> "TESTI" then
				sDirCancella = Session("DirInit")
			Else
				sDirCancella = Server.MapPath("\") & "\" & sDirScelta 
			End If
		End if
		
' Controllo che la directory sia presente sul server			
		Set fso = CreateObject("Scripting.FileSystemObject")
		on error resume next
		Set fd = fso.GetFolder(sDirCancella)
		If err.number <> 0 Then
			Response.Write "<b class='tbltext'><br>Carpeta no cancelable "
			Response.Write "<p>contactar el Grupo Asistencia Portal Italia Lavoro<br>"
			Response.Write "a la direcci�n <a href='mailto:po-assistenza@italialavoro.it'>po-assistenza@italialavoro.it</a></p></b>"
			Response.End
		End If
		
		Set sf = fd.SubFolders
		
		Trovato = false
'Controlla l'esistenza di sottocartelle
		For Each f1 in sf
			s = s & f1.name
			Trovato = True
		  Exit For 
		Next
		
 ' Se esistono sottodirectory non posso cancellarla	
		If (Trovato) Then	
			Response.Write "<FORM id='NoDel' Method='post' Action='DOC_Vis" & Session("Intestazione") & ".asp'>"
			Response.Write "<input type='hidden' name='Tema' value='" & nIdCat & "'>"
			Response.Write "<TABLE width='500' CELLPADDING='2' CELLSPACING='2'>"
			Response.Write "<tr><td align='center'><img src='/images/sistdoc/Upload/DirNotEmpty.gif' Border='0' width='400' height='260' alt='Eliminaci�n Carpeta Not OK'></td></tr>"
			Response.Write "<tr><td align='center' class='tbltext3'><b>La Carpeta <br><i class='textredb'>" & sDirName
			Response.Write "</i><br> no est� vac�a.<br>Non es posible eliminarla.</b></td></tr>"
			Response.Write "<tr><td align='center'><a href='" & Session("Progetto") & "' onClick='javascript:NoDel.submit();return false'>"
			Response.Write "<img alt='Atr�s' border='0' src='" & Session("Progetto") & "/images/indietro.gif'></a></td></tr>"
			Response.Write "</TABLE>"
			Response.Write "</FORM>"
		Else	
			fso.DeleteFolder(sDirCancella)
			
'07/04/2005 inizio
' Seleziono il nome del file da cancellare (immagine gif o jpg) se esiste
			sSelNomeGif = "SELECT Gif_Categoria FROM Categoria WHERE Id_Categoria = " & nIdCat 
'PL-SQL * T-SQL  
SSELNOMEGIF = TransformPLSQLToTSQL (SSELNOMEGIF) 
			set rsSelGif = CC.Execute(sSelNomeGif)
			
			if rsSelGif("Gif_Categoria") <> "" then
				sNomeGif = rsSelGif("Gif_Categoria")
			
			'cancello l'immagine associata alla cartella
				PathDirInit = Server.MapPath("\")
				PathDir = PathDirInit & Session("Progetto") & "\images\Categorie\" & sNomeGif
				PathDir = replace(PathDir,"/","\")
				'Response.Write PathDir
				set objFso = CreateObject("Scripting.FileSystemObject")  
				set objFile = objFso.GetFile(PathDir)
				objFile.Delete
				set objFso = nothing
				Response.Write Err.Description
			end if	
			set rsSelGif = nothing		
'07/04/05 fine			
			
			
' Aggiorno il DB
			SqlElNot = "DELETE FROM Notizie WHERE Id_Categoria=" & nIdCat
			SqlElNot = TransformPLSQLToTSQL (SqlElNot) 
			Set SqlElNot = CC.Execute(SqlElNot)	
			
			SqlElCat = "DELETE FROM Categoria WHERE Id_Categoria=" & nIdCat
'PL-SQL * T-SQL  
SQLELCAT = TransformPLSQLToTSQL (SQLELCAT) 
			Set rstElCat = CC.Execute(SqlElCat)	
			
							
			SqlElRuoloCat = "DELETE FROM Ruolo_Categoria WHERE Id_Categoria=" & nIdCat
'PL-SQL * T-SQL  
SQLELRUOLOCAT = TransformPLSQLToTSQL (SQLELRUOLOCAT) 
			Set rstElRuoloCat = CC.Execute(SqlElRuoloCat)
				
			If cint(nIdPadre)=0 Then
				Response.Write "<FORM id='DelCart' Method='post' Action='DOC_Init" & Session("Intestazione") & ".asp'>"
			Else
				Response.Write "<FORM id='DelCart' Method='post' Action='DOC_Vis" & Session("Intestazione") & ".asp'>"
			End If
' Ritorno il messaggio		
			Response.Write "<TABLE width=500 CELLPADDING=2 CELLSPACING=2>"
			Response.Write "<input type='hidden' name='Tema' value='" & nIdPadre & "'>"
			Response.Write "<tr><td align='center'><img src='/images/sistdoc/Upload/DelCartella.gif' Border='0' width='400' height='260' alt='Eliminaci�n Carpeta OK'></td></tr>"
			Response.Write "<tr><td align='center' class='tbltext3'><b>Ha sido eliminada la carpeta <br><i class=textredb>" & sDirName
			Response.Write "</i></b></td></tr>"
			Response.Write "<tr><td align='center'><a href='" & Session("Progetto") & "' onClick='Javascript:DelCart.submit();return false'>"	
			Response.Write "<img alt='Atr�s' border='0' src='" & Session("Progetto") & "/images/indietro.gif'></a></td></tr>"
			Response.Write "</TABLE>"
			Response.Write "</FORM>"
		End If
	End If
	
Else
	sDescr = Request.Form("Descrizione")
	sDescr = Replace(sDescr,"'","''")
	sAbst = Request.Form("AbstractDir")
	sAbst = Replace(sAbst,"'","''")
	sAlt = Trim(Replace(Request.Form("txtAltFoto"),"'","''"))

' Aggiorno il DB con le modifiche apportate
'	SqlModCat = "UPDATE Categoria SET Descrizione = '" & sDescr &_
'	            "', Abstract = '" & sAbst &_
'	            "', WHERE ID_Categoria = " & nIdCat
	        
	SqlModCat = "UPDATE Categoria SET Descrizione = '" & sDescr &_
	            "', Abstract = '" & sAbst & "',alt_categoria='" & sAlt &_
	            "' WHERE ID_Categoria = " & nIdCat            
'PL-SQL * T-SQL  
SQLMODCAT = TransformPLSQLToTSQL (SQLMODCAT) 
	Set rstModCat = CC.Execute(SqlModCat)
	
	aSepDirScelta = split(sDirScelta,"\")
' Controllo che la categoria sia comune o relativa ad un progetto	
	If UCase(aSepDirScelta(0)) <> "TESTI" then
		sDirModificata = Session("DirInit") & sDirScelta
	Else
		sDirModificata = Server.MapPath("\") & "\" & sDirScelta 
	End If
	
	Set fso = CreateObject("Scripting.FileSystemObject")
	
	on error resume next
	Set fd = fso.GetFolder(sDirModificata)
	If err.number <> 0 Then
		Response.Write "<b class='tbltext'><br>Carpeta no modificable"
		Response.Write "<p>Contactar el Grupo Asistencia Portal Italia Lavoro <br>"
		Response.Write "a la direcci�n <a href='mailto:po-assistenza@italialavoro.it'>po-assistenza@italialavoro.it</a></p></b>"
		Response.End
	End If
	
	If cint(nIdPadre)=0 Then
		Response.Write "<FORM id='UpCart' Method='post' Action='DOC_Init" & Session("Intestazione") & ".asp'>"
	Else
		Response.Write "<FORM id='UpCart' Method='post' Action='DOC_Vis" & Session("Intestazione") & ".asp'>"
	End If
' Ritorno il messaggio	
	Response.Write "<TABLE width='500'>"
	Response.Write "<input type='hidden' name='Tema' value='" & nIdPadre & "'>"
	Response.Write "<tr><td align='center'><img src='/images/sistdoc/Upload/ModCartella.gif' border='0' width='400' height='260' alt='Modifica Cartella OK'></td></tr>"
	Response.Write "<tr><td align='center' class='tbltext3'><b>Ha sido modificada la descripci�n de la carpeta <br><i class='textredb'>" & sCampo
	Response.Write "</i></b></td></tr>"
	Response.Write "<tr><td align='center'><a href='" & Session("Progetto") & "' onClick='Javascript:UpCart" & i & ".submit();return false'>"
	Response.Write "<img alt='Atr�s' border='0' src='" & Session("Progetto") & "/images/indietro.gif'></a></td></tr>"
	Response.Write "</TABLE>"
End If
%>	
<!-- #include virtual="/include/closeconn.asp" -->
<!-- #include virtual="/strutt_coda2.asp" -->

