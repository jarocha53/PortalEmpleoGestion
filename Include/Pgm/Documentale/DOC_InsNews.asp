<%@ Language=VBScript %>
<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #include virtual="/strutt_testa2.asp" -->
<!-- #include virtual="/util/dbutil.asp" -->
<!-- #include virtual="/util/portallib.asp" -->
<!-- #include virtual="/include/OpenConn.asp" -->
<!-- #include virtual="/include/ckProfile.asp" -->
<!-- #include virtual="/include/ControlDateVB.asp" -->
<!-- #include virtual="/include/SysFunction.asp"-->
<%
If ( (Not ValidateService(Session("IdUtente"),"Gestione Notizie",cc)) And (Not ValidateService(Session("IdUtente"),"Sistema Documentale",cc)) ) Then 
	response.redirect "/util/error_login.asp"
End If

Dim fso, f
' Ricavo dati neccessari passati da campi nascosti
sPathNews = Request.Form("TargetURL")
sNomeNews = Trim(Request.Form("NomeNews"))
sTitolo = Trim(Request.Form("Titolo"))
sBuild = Request.Form ("Build")

' Controllo per sapere se la notizia inviata o creata.
If sBuild <> "" Then

    sNomeNews = Replace(sBuild,".asp","")
    aPathNews = split(sPathNews,"\")
' Controllo per sapere se notizia comune o relativa ad un progetto.
    If UCase(aPathNews(0)) <> "TESTI" Then
		DestFinale = Session("DirInit") & sPathNews & "\" & Replace(sBuild,".asp",".htm")
		DestFinaleInfo = Session("DirInit") & sPathNews & "\" & Replace(sBuild,".asp",".htm") 
		DestFinaleInfo = replace(DestFinaleInfo,"Pinfo","info")	
		FileDel = Server.MapPath("/") & Session("Progetto") &  "\Testi\SistDoc\build\" & Replace(sBuild,".asp",".htm")
	Else
		DestFinale = Server.MapPath("/") & "\" & sPathNews & "\" & Replace(sBuild,".asp",".htm")
		DestFinaleInfo = Server.MapPath("/") & "\" & sPathNews & "\" & Replace(sBuild,".asp",".htm") 
		DestFinaleInfo = replace(DestFinaleInfo,"Pinfo","info")	
		FileDel = Server.MapPath("/") & "\Testi\build\" & Replace(sBuild,".asp",".htm")
	End If
	FileBak = FileDel & ".bak"
	
Else
	
	DestFinale = Session("DirInit") & sPathNews & "\" & sNomeNews & ".htm"
	FileDel = Session("DirInit") & sPathNews & "\" & Session("IdUtente") & ".htm"
	FileBak = FileDel & ".bak"
	
End if 

Set fso = CreateObject("Scripting.FileSystemObject")
On error resume next
' Controllo se la notizia esiste.
Set f = fso.GetFile(FileDel)
If Err.number > O Then
	Response.Write (Err.description)
	Response.Write "<b class='tbltext'><br>Pagina al momento non disponibile"
	Response.Write "<p>Contattare il Gruppo Assistenza Portale Italia Lavoro<br>"
	Response.Write "all'indirizzo <a href='mailto:po-assistenza@italialavoro.it'>po-assistenza@italialavoro.it</a></p></b>"
	Response.End
End If
If fso.FileExists(DestFinale) Then
	Response.Write "<TABLE WIDTH=510 border='0' CELLPADDING='2' CELLSPACING='2'>"
	Response.Write "<tr><td align='center'><img src=/Images/SistDoc/Upload/CreazioneNewsNotOK.gif WIDTH='400' HEIGHT='260'></td></tr>"
	Response.Write "<tr><td colspan='2' class='tbltext3' align='center'><b>La notizia dal titolo <i class='textredb'>" & sNomeNews
	Response.Write "&nbsp;</i>non puo' essere modificata.<br>Esiste gi� una notizia con il medesimo nome.</b></td></tr>"
	Response.Write "<tr><td align='center'><a href='javascript:history.back()'>"
	Response.Write "<img alt='Indietro' border='0' src='" & Session("Progetto") & "/images/indietro.gif'></a></td></tr>"
	Response.Write "</TABLE>"
    Response.End
End if 

	f.Copy (DestFinale)

'	Solo per la costruzione delle pagine di info copia il file
	If sBuild <> "" Then	
		fso.MoveFile FileDel,DestFinaleInfo
	end if

	
	fso.DeleteFile(FileDel)	
	fso.DeleteFile(FileBak)


if 	Request.Form("DataIns") <> "" then
	dtDataIns = ConvStringToDate(Request.Form("DataIns"))
else
	dtDataIns = ConvStringToDate(date)
end if

IdCat = Request.Form("Categoria")
sAbstract = Trim(Request.Form("Abstract"))
sAbstract = Replace(sAbstract,"'","''")
dtDataIns = ConvDateToDbs(dtDataIns)
dtDataPubb = ConvDateToDbs(ConvDateToString(Date()))
sFonte = Trim(Request.Form("Fonte"))
sFonte = Replace(sFonte,"'","''")
sAutore = Trim(Request.Form("Autore"))
sAutore = Replace(sAutore,"'","''")
sGifBig= Trim(Request.Form("FotoApp"))
sGifSmall = Trim(Request.Form("FotoHp"))
sTitolo = Replace(sTitolo,"'","''")
sNomeNews = Replace(sNomeNews,"'","''")
sAltHp = Trim(Replace(Request.Form("txtAltFotoHp"),"'","''"))
sAltApp = Trim(Replace(Request.Form("txtAltFotoApp"),"'","''"))

' Aggiorno il DB in base alla notizia inviata o creata.
SQLNotizie = "INSERT INTO Notizie ( Id_Categoria, Titolo, Abstract, Data_Ins, Data_Pubb, Fonte, Autore, Nome_Notizie, Gif_Notizie, Gif_HP, Fl_Pubblicato, Id_Tipodoc,alt_hp,alt_notizie )" 

If sBuild = "" Then
	SQLNotizie = SQLNotizie & "VALUES (" & IdCat & ",'" & sTitolo & "','" & sAbstract & "'," & dtDataIns & "," & dtDataPubb & ",'" & sFonte & "','" & sAutore & "','" & sNomeNews & "','" & sGifBig & "','" & sGifSmall & "', 0, 6,'" & sAltHp & "','" & sAltApp & "')" 
Else
	SQLNotizie = SQLNotizie & "VALUES (" & IdCat & ",'" & sTitolo & "','" & sAbstract & "'," & dtDataIns & "," & dtDataPubb & ",'" & sFonte & "','" & sAutore & "','" & sNomeNews & "','" & sGifBig & "','" & sGifSmall & "', 0, 11,'" & sAltHp & "','" & sAltApp & "')" 
End if
'Response.Write "INSERT " & SQLNotizie
'Response.END

'PL-SQL * T-SQL  
SQLNOTIZIE = TransformPLSQLToTSQL (SQLNOTIZIE) 
Set rstNotizie = CC.Execute(SQLNotizie)
Response.Write "<FORM name='CreaDoc' method='post' action='DOC_Vis" & Session("Intestazione") & ".asp'>"
Response.Write "<input type='hidden' name='Tema' value='" & Request.Form("Categoria") & "'>" 
Response.Write "</FORM>" 
If sBuild = "" Then   
Response.Write "<TABLE WIDTH=510 border=0 CELLPADDING=2 CELLSPACING=2>"
Response.Write "<tr><td align=center><img Src=/Images/SistDoc/Upload/NewsOK.gif WIDTH=400 HEIGHT=260></td></tr>"
Response.Write "<tr><td colspan=2 class=tbltext3 align=center><b>La nuova notizia dal titolo &nbsp;<i class=textredb>" & Request.Form("Titolo")
Response.Write "</i>&nbsp;e' stata creata.</b></td></tr>"
Response.Write "<tr><td align='center'><a class='tbltext' href='" & Session("Progetto") & "' onClick='Javascript:CreaDoc.submit();return false'>"
Response.Write "<img alt=Indietro border=0 src=" & Session("Progetto") & "/images/indietro.gif></a></td></tr>"
Response.Write "</TABLE>"
Else
Response.Write "<TABLE WIDTH=510 border=0 CELLPADDING=2 CELLSPACING=2>"
Response.Write "<tr><td align=center><img Src=/Images/SistDoc/Upload/NewsOK.gif WIDTH=400 HEIGHT=260></td></tr>"
Response.Write "<tr><td colspan=2 class=tbltext3 align=center><b>La nuova pagina dal titolo &nbsp;<i class=textredb>" & Request.Form("Titolo")
Response.Write "</i>&nbsp;e' stata creata.</b></td></tr>"
Response.Write "<tr><td align='center'><a class='tbltext' href='" & Session("Progetto") & "' onClick='Javascript:CreaDoc.submit();return false'>"
Response.Write "<img alt=Indietro border=0 src=" & Session("Progetto") & "/images/indietro.gif></a></td></tr>"
Response.Write "</TABLE>"
End if

%>
<!-- #include virtual="/include/closeconn.asp" -->
<!-- #include virtual="/strutt_coda2.asp" -->
