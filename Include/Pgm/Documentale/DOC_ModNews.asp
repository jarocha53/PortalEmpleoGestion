<%@ Language=VBScript %>
<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #include virtual="/strutt_testa2.asp" -->
<!-- #include virtual="/util/dbutil.asp" -->
<!-- #include virtual="/util/portallib.asp" -->
<!-- #include virtual="/include/OpenConn.asp" -->
<!-- #include virtual="/include/ckProfile.asp" -->
<!-- #include virtual="/include/ControlDateVB.asp" -->
<!-- #include virtual="/include/SysFunction.asp" -->
<%
	If ((Not ValidateService(Session("IdUtente"),"Gestione Notizie",cc)) And (Not ValidateService(Session("IdUtente"),"Sistema Documentale",cc))) Then 
		response.redirect "/util/error_login.asp"
	End If

IdNews = Request.QueryString("Nr")
' Ricavo id notizia da modificare
Dim fso, f
PathNews = Request.Form("TargetURL")
NomeNews = Request.Form("NomeNews")
' Ricavo la posizione della notizia da modificare
DestFinale = Session("DirInit") & PathNews & "\" & NomeNews & ".htm"
FileDel = Session("DirInit") & PathNews & "\" & Request.Form("OldFile") & ".htm"
' Controllo se ho modificato il nome della notizia 
If DestFinale <> FileDel Then
	Set fso = CreateObject("Scripting.FileSystemObject")
    on error resume next 
' Controllo che sia raggiungibile la notizia vecchia
	Set f = fso.GetFile(FileDel)
	If err.number <> 0 Then
		Response.Write "<b class='tbltext'><br>Noticia no modificable"
		Response.Write "<p>Contactar el Grupo Asistencia Portal Italia Lavoro <br>"
		Response.Write "direcci�n <a href='mailto:po-assistenza@italialavoro.it'>po-assistenza@italialavoro.it</a></p></b>"
		Response.End
	End If
' Controllo che non esista una notizia con il nome appena scelto
    If fso.FileExists(DestFinale) Then   
		Response.Write "<TABLE width='510' border='0' cellpadding='2' cellspacing='2'>"
		Response.Write "<tr><td align='center'><img src='/images/sistdoc/Upload/CreazioneNewsNotOK.gif' width='400' height='260'></td></tr>"
		Response.Write "<tr><td colspan='2' class='tbltext3' align='center'><b>La notizia dal titolo &nbsp;<i class='textredb'>" & NomeNews
		Response.Write "&nbsp;</i>non puo' essere modificata.<br>Esiste gi� una notizia con il medesimo nome.</b></td></tr>"
		Response.Write "<tr><td align='center'><a href='DOC_InsDoc.asp?Nr=" & IdNews & "&Cod=MA"
		Response.Write "'><img alt='Atr�s' border='0' src='" & Session("Progetto") & "/images/indietro.gif'></a></td></tr>"
		Response.Write "</TABLE>"
		Response.End
    End if 
    
	f.Copy (DestFinale)
	fso.DeleteFile(FileDel)
End if

If DataPubb <> "" Then
	DataPubb = ConvStringToDate(Request.Form("DataPubb"))
Else
	DataPubb = Date()
End If
TitoloUp = Trim(Replace(Request.Form("Titolo"),"'","''"))
AbstractUp = Replace(Request.Form("Abstract"),"'","''")
NomeNewsUp = Trim(Replace(Request.Form("NomeNews"),"'","''"))
FonteUp = Trim(Replace(Request.Form("Fonte"),"'","''"))
AutoreUp = Trim(Replace(Request.Form("Autore"),"'","''"))
AltHp = Trim(Replace(Request.Form("txtAltFotoHp"),"'","''"))
AltApp = Trim(Replace(Request.Form("txtAltFotoApp"),"'","''"))

' Aggiorno il DB
SqlModAtt = "UPDATE Notizie SET Titolo ='" & TitoloUp & "', Abstract = '" & AbstractUp &_
			"', Nome_Notizie = '" & NomeNewsUp & "', Data_Pubb = " & ConvDateToDbs(DataPubb) &_ 
			", Fonte = '" & FonteUp & "', Autore = '" & AutoreUp &_
			"',ALT_HP='" & AltHp & "', ALT_NOTIZIE='" & AltApp & "' WHERE ID_Notizie = " & IdNews
				
'PL-SQL * T-SQL  
SQLMODATT = TransformPLSQLToTSQL (SQLMODATT) 
Set rstModAtt = CC.Execute(SqlModAtt)
' Ritorno il messaggio	
Response.Write "<TABLE width='510' border='0' cellpadding='2' cellspacing='2'>"
Response.Write "<tr><td align='center'><img src='/images/sistdoc/Upload/ModNewsOK.gif' width='400' height='260'></td></tr>"
Response.Write "<tr><td colspan='2' class='tbltext3' align='center'><b>La noticia con el t�tulo &nbsp;<i class='textredb'>" & Request.Form("Titolo")
Response.Write "&nbsp;</i>ha sido modificada.</b></td></tr>"
Response.Write "<tr><td align='center'><a href='DOC_LeggiNews.asp?Nr=" & IdNews
Response.Write "'><img alt='Atr�s' border='0' src='" & Session("Progetto") & "/images/indietro.gif'></a></td></tr>"
Response.Write "</TABLE>"

Set rstModAtt = nothing
%>
<!-- #include virtual="/include/closeconn.asp" -->
<!-- #include virtual="/strutt_coda2.asp" -->
