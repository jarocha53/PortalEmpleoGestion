<%@ Language=VBScript %>
<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #include virtual = "/strutt_testa2.asp" -->
<!-- #include virtual = "/util/portallib.asp" -->
<!-- #include virtual = "/include/OpenConn.asp" -->
<!-- #include virtual = "/include/ckProfile.asp" -->
<script language="javascript" src="/Include/help.inc"></script>
<%
' Pagina utilizzata per un utenza che agisce solo sul GLOSSARIO
If Not ValidateService(Session("IdUtente"),"Sistema Redazionale",cc) Then 
	Response.Redirect "/util/error_login.asp"
End If

' Dal mask ricavo il Tipo di categoria ed i permessi
TipoCatScelta = right(Session("mask"),1)
IdGruppo = left(Session("mask"),1)

Session("Tipo") = TipoCatScelta
Session("GruppoDoc") = IdGruppo

If Session("Tipo") = "X" Then
	response.redirect "/util/error_login.asp"
End If
' Setto le directory di partenza
PathDirInit = Server.MapPath("\") & Session("Progetto") & "/Testi/sistdoc/"
DirPath = Session("Progetto") 

	Session("DirInit") = PathDirInit 
	Session("DirInitEdit") = DirPath & "/Testi/sistdoc/"
	Session("Intestazione") = "Redazionale"
	
Dim DirScelta, Posbarra, Pathname
Dim fso, f

Set rstTipoCat = Server.CreateObject("ADODB.Recordset")
SQLTipoCat = "SELECT Id_Categoria, Descrizione, Percorso, Gif_categoria, Padre, Tipo FROM Categoria WHERE Id_categoria=165"
SQLTipoCat = UCase(SQLTipoCat)
'PL-SQL * T-SQL  
SQLTIPOCAT = TransformPLSQLToTSQL (SQLTIPOCAT) 
rstTipoCat.open SQLTipoCat, CC, 1, 3

Session("NomeUtente") = Session("Nome") & "&nbsp;" & Session("Cognome")
%>
<br>
<table BORDER="0" WIDTH="500" CELLPADDING="0" CELLSPACING="0">
	<tr height="18">
		<td class="sfondomenu" width="67%" height="18"><span class="tbltext0"><b>&nbsp;AREA REDACCIONAL - <%=Session("Intestazione")%></b></span></td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
		<td valign="middle" align="right" class="tbltext1" width="40%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
	</tr>
	<tr>
		<td class="sfondocomm" width="57%" colspan="3"><br>Desde aqu� es posible modificar o crear las p�ginas del sistema redaccional. <br>
		Presionar sobre <b>Sistema redaccional</b> para visualizar el elenco de las p�ginas modificables.
		Presionar sobre <b>Crea Documento para</b> crear una nueva p�gina.
		<a href="Javascript:Show_Help('/Pgm/help/Documentale/DOC_InitRedazionale')" name onmouseover="javascript:status='' ; return true">
		<img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0" width="18" height="18" alt="Para mayores informaciones"></a></td>
	</tr>
	<tr height="17">
		<td class="sfondocomm" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
	</tr>
</table>
<br><br>
<table BORDER="0" WIDTH="500" CELLPADDING="1" CELLSPACING="1">
	<form id="NewDoc" method="POST" action="DOC_VisRedazionale.asp">
		<input type="hidden" name="Tema" value="2">							
	</form>	
	<tr height="18">
		<td width="500" colspan="2" height="18" class="sfondocomm"><b>Listado de  Categor�as</b></td>
	</tr>	
<% 
	If rstTipoCat.EOF Then
		Response.Write "<tr class='tblsfondo'><td align='center' width='50'><img src='/Images/SistDoc/bullet1.gif'></td>"
		Response.Write "<td class='tbltext' width='450'><b>Ninguna categor�a presente</b></td></tr>"	
	Else
		iIndCat = 0
		Do While Not rstTipoCat.EOF
			iIndCat = iIndCat + 1
			Response.Write "<FORM name='subcat" & iIndCat & "' method='post' action='DOC_VisRedazionale.asp'>"
			Response.Write "<input type='hidden' name='Tema' value='" & rstTipoCat("Id_Categoria") & "'>" 
			Response.Write "</FORM>"
			Response.Write "<tr class='tblsfondo'><td width='20' align='center'><img src='/images/sistdoc/bullet1.gif'>&nbsp;</td>"
			Response.Write "<td width='480'><a class='tbltext' href='" & Session("Progetto") & "' onClick='Javascript:subcat" & iIndCat & ".submit();return false'>"
			Response.Write "<b>" & rstTipoCat("Descrizione") & "</b></a></td></tr>"
			rstTipoCat.movenext
		Loop
	End If	

Response.Write "</TABLE>"
rstTipoCat.Close
Set rstTipoCat = nothing
%>
<!-- #include virtual = "/include/closeconn.asp" -->
<!-- #include virtual="/strutt_coda2.asp" -->

