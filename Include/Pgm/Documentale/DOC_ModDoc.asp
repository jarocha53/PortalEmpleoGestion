<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #include virtual="/strutt_testa2.asp" -->
<!-- #include virtual="/util/portallib.asp" -->
<!-- #include virtual="/include/OpenConn.asp" -->
<!-- #include virtual="/include/ckProfile.asp" -->
<!-- #include virtual="/include/ControlDateVB.asp" -->
<!-- #include virtual="/include/SysFunction.asp" -->
<%
	If ((Not ValidateService(Session("IdUtente"),"Gestione Notizie",cc)) And (Not ValidateService(Session("IdUtente"),"Sistema Documentale",cc))) Then 
		response.redirect "/util/error_login.asp"
	End If
%>
<script LANGUAGE="Jscript" src="/Include/help.inc"></script>
<script LANGUAGE="Javascript" src="/Include/ControlDate.inc"></script>
<script LANGUAGE="Javascript" src="/Include/ControlNum.inc"></script>
<script LANGUAGE="Javascript" src="/Include/ControlString.inc"></script>
<script LANGUAGE="Javascript">
//include del file per fare i controlli sulla validit� delle date

function InitInvio() {
	InvioForm.Titolo.focus();	
}

function ControlloTitolo() {
	var i, codice_car;
	var codice = InvioForm.Titolo.value;
	var CtrlString = false;
	 for (i=0; i< codice.length; i++) {
	 codice_car = codice.charAt(i)
	 if (codice_car != " ") {
		CtrlString = true;
		break;
	}
	}
}

function ControllaCheck() {
if (!TRIM(InvioForm.Titolo.value)) {
	alert ("No es permitido el env�o sin haber elegido un t�tulo");
	InvioForm.Titolo.focus();
	return false 
	}
if (InvioForm.Titolo.value.length > 100){
	alert ("No est� permitido el env�o de la noticia con un t�tulo mayor de 100 caracteres.\nAhora es largo " + InvioForm.Titolo.value.length + " caracteres");
	InvioForm.Titolo.focus();
	return false 
	}
if (InvioForm.Abstract.value.length > 400) {
	alert("El resumen no puede superar los 400 caracteres.\nEs largo " + InvioForm.Abstract.value.length + " caracteres");
	InvioForm.Abstract.focus();
	return false 
	}
}
</script>
<%
IdNews = Request.Form("Nr")
If Request.QueryString("Up") <> "Ok" Then
	Set rstNotizie = Server.CreateObject("ADODB.Recordset")
	SQLNotizie = "SELECT Id_Notizie, Id_Categoria, Titolo, Abstract, Data_Ins, Data_Pubb, Id_Tipodoc, Nome_Notizie, Autore, Fl_Pubblicato, Fonte FROM Notizie Where Id_Notizie=" & IdNews
'PL-SQL * T-SQL  
SQLNOTIZIE = TransformPLSQLToTSQL (SQLNOTIZIE) 
	rstNotizie.open SQLNotizie, CC, 1, 3
%>
<br>
<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
	<tr height="18">
		<td class="sfondomenu" width="67%" height="18"><span class="tbltext0"><b>&nbsp;AREA REDACCIONAL - <%=Session("Intestazione")%> </b></span></td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
		<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">(*) campos obligatorios</td>
	</tr>
	<tr>
		<td class="sfondocomm" width="57%" colspan="3">
		  <br>Form di modifica degli attributi del documento <%=rstNotizie("Nome_Notizie")%>
		  <a href="Javascript:Show_Help('/Pgm/help/Documentale/DOC_ModDoc')" name onmouseover="javascript:status='' ; return true">
		  <img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0" width="18" height="18" alt="Para mayores informaciones"></a>
		</td>
	</tr>
	<tr height="17">
		<td class="sfondocomm" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
	</tr>
</table>

<form Name="InvioForm" Method="post" Action="DOC_ModDoc.asp?Up=Ok&amp;F=<%=DocInviati%>" onSubmit="return ControllaCheck()">
<input type="hidden" name="Up" value="Ok">
<input type="hidden" name="F" value="<%=DocInviati%>">	
<table width="500" BORDER="0" CELLPADDING="2" CELLSPACING="2">
	<tr>
		<td width="200" class="tbltext1"><b>Titulo*</b></td>	
		<td width="300">
			<input class="textblacka" Type="text" Name="Titolo" Size="50" Value="<%=rstNotizie("Titolo")%>" onblur="ControlloTitolo()">
		</td>	
	</tr>
	<tr>
		<td width="200" class="tbltext1"><b>Resumen</b></td>	
		<td width="300">
			<textarea class="textblacka" Name="Abstract" rows="5" cols="49"><%=rstNotizie("Abstract")%></textarea>
		</td>	
	</tr>
	
<%	If Cint (rstNotizie("Fl_Pubblicato")) = 1 then %>
	
	<tr>
		<td width="200" class="tbltext1"><b>Fecha de publicaci�n</b></td>	
		<td width="300">
			<input class="textgray" Type="text" Name="DataPubb" Size="11" Value="<%=ConvDateToString(rstNotizie("Data_Pubb"))%>" Maxlength="10" ReadOnly>
		</td>	
	</tr>
<% Else %>	
	<tr><td><input class="textgray" type="hidden" Name="DataPubb" Size="11" Value="<%=ConvDateToString(rstNotizie("Data_Pubb"))%>" Maxlength="10"></td></tr>
<%  End If%>	
	<tr>
		<td width="200" class="tbltext1"><b>Fuente</b></td>	
		<td width="300">
			<input class="textblacka" Type="text" Name="Fonte" Size="40" Maxlength="50" Value="<%=rstNotizie("Fonte")%>">
		</td>	
	</tr>
	<tr>
		<td width="200" class="tbltext1"><b>Autor</b></td>	
		<td width="300">
			<input class="textgray" Type="text" Name="Autore" Size="40" Maxlength="50" Value="<%=rstNotizie("Autore")%>" readonly>
		</td>	
	</tr>
	<tr>
		<td colspan="2" width="500" align="center">
			<input type="image" src="/images/conferma.gif" Name="CreaNews" WIDTH="55" HEIGHT="55"><br>
		</td>
	</tr>
	<tr>	
		<td><input Type="hidden" Name="Nr" Value="<%=IdNews%>"></td>
		<td><input Type="hidden" Name="Categoria" Value="<%=rstNotizie("Id_Categoria")%>"></td>
	</tr>
</table>
</form>
		
<%
	rstNotizie.Close
	Set rstNotizie = nothing
Else	
	DataPubb = ConvStringToDate(Request.Form("DataPubb"))

	TitoloUp = Trim(Replace(Request.Form("Titolo"),"'","''"))
	AbstractUp = Replace(Request.Form("Abstract"),"'","''")
	FonteUp = Trim(Replace(Request.Form("Fonte"),"'","''"))
	AutoreUp = Trim(Replace(Request.Form("Autore"),"'","''"))

	SqlModAtt = "UPDATE Notizie SET Titolo ='" & TitoloUp &_
				"', Abstract = '" & AbstractUp &_
				"', Data_Pubb = " & ConvDateToDbs(DataPubb) &_ 
				", Fonte = '" & FonteUp &_
				"', Autore = '" & AutoreUp & "' WHERE ID_Notizie =" & IdNews	
'PL-SQL * T-SQL  
SQLMODATT = TransformPLSQLToTSQL (SQLMODATT) 
	Set rstModAtt = CC.Execute(SqlModAtt)
	
	Response.Write "<FORM id='FormRis' method='post' action='DOC_VisDocumenti.asp'>"
	Response.Write "<input type='hidden' name='Tema' value='" & Request.Form("Categoria") & "'>"
	Response.Write "<TABLE width='500' border='0' CELLPADDING='2' CELLSPACING='2'>"
	Response.Write "<tr><td align='center'><img src='/images/sistdoc/Upload/ModNewsOK.gif' width='400' height='260'></td></tr>"
	Response.Write "<tr><td colspan='2' class='tbltext3' align='center'><b>La noticia con el t�tulo &nbsp;<i class='textredb'>" & Request.Form("Titolo")
	Response.Write "&nbsp;</i>ha sido modificada.</b></td></tr>"
	Response.Write "<tr><td align='center'><a href='" & Session("Progetto") & "' onClick='javascript:FormRis.submit();return false'>"
	Response.Write "<img alt='Atr�s' border='0' src='" & Session("Progetto") & "/images/indietro.gif'></a></td></tr>"
	Response.Write "</TABLE>"
	Response.Write"</FORM>"	
End if		
%>
<!-- #include virtual="/include/closeconn.asp" -->
<!-- #include virtual="/strutt_coda2.asp" -->
