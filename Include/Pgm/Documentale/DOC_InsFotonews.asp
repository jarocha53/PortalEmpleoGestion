<%@ Language=VBScript %>
<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #include virtual="/strutt_testa2.asp" -->
<!-- #include virtual="/util/portallib.asp" -->
<!-- #include virtual="/include/OpenConn.asp" -->
<!-- #include virtual="/include/ckProfile.asp" -->
<script language="javascript" src="/include/help.inc"></script>

<%
If (Not ValidateService(Session("idutente"),"Gestione Notizie",cc)  And  Not ValidateService(Session("idutente"),"Sistema Documentale",cc) ) then 
	Response.Redirect "/util/error_login.asp"
End If

sBuild = Request.QueryString("Build")	
if sBuild <> "" then
	sBuild = "&Build=" & sBuild
else
	sBuild = ""
end if
	
IdCat = Request.QueryString("Id")

If Session("Tipo")="D" Then
	Response.Redirect "Doc_InsDoc.asp?Id=" & IdCat
End If
' Controlllo per vedere se devo associarle le foto alla notizia o fare l'upload
If Request.QueryString("Dir") = "" Then

	Set rstCategorie = Server.CreateObject("ADODB.Recordset")
	SQLCategoria = "SELECT Id_Categoria, Descrizione, Percorso FROM Categoria WHERE Id_Categoria=" & IdCat
'PL-SQL * T-SQL  
SQLCATEGORIA = TransformPLSQLToTSQL (SQLCATEGORIA) 
	rstCategorie.open SQLCategoria, CC, 1, 3
	DirScelta = rstCategorie("Percorso") & "\"
	PathDir = Replace(Session("DirInit") & DirScelta, "/", "\")
%>
<br>
<table border="0" cellpadding="0" cellspacing="0" width="500">
	<tr height="18">
		<td class="sfondomenu" width="67%" height="18"><span class="tbltext0"><b>&nbsp;AREA REDAZIONALE</b></span></td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
		<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
	</tr>
	<tr>
		<td class="sfondocomm" width="57%" colspan="3">
			<br>Per associare una foto alla notizia utilizzare il pulsante <b>Sfoglia</b> e selezionare il file dell'immagine nel proprio computer. <br>
			Premere <b>Invia</b> per confermare l'inserimento. <br>
			Se non si desidera inserire una foto premere direttamente <b>Invia</b> per passare alla fase successiva.
			<a href="Javascript:Show_Help('/pgm/help/Documentale/Doc_InsFotoNews')" name onmouseover="javascript:status='' ; return true">
		    <img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0" width="18" height="18" alt="per maggiori informazioni"></a></td>
	</tr>
	<tr height="17">
		<td class="sfondocomm" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">
		</td>
	</tr>
</table>
<%	
PathDir = Replace(PathDir,"\","/")

%>
<script language="Javascript">
	function CheckFile()
	{ 
		/*
		if ((FotoForm.F1.value != "") && (FotoForm.F2.value != "") && (FotoForm.F2.value == FotoForm.F1.value)) 
		{	
			alert ("Attenzione! Sono state indicate due foto con il medesimo nome.\nSi prega di cambiare il nome di una delle due foto.");
			return false
		}
		*/	
		
		if ((FotoForm.F1.value == "") && (FotoForm.F2.value != "")) 
		{	
			alert ("Attenzione!Non e' permesso inviare una foto di dettaglio (Foto Grande) senza una Piccola.");
			return false
		}
		
					 
		if ((FotoForm.F1.value != "") || (FotoForm.F2.value != "")){
			document.FotoForm.action = "DOC_InsFotonews.asp?Dir=<%=PathDir%>&Id=<%=IdCat%><%=sBuild%>" 
		}
		else {
			document.FotoForm.action = "DOC_InsDoc.asp?Id=<%=IdCat%><%=sBuild%>"
		}
		document.FotoForm.submit();
		return false
	}	
</script>

<form enctype="multipart/form-data" method="post" action name="FotoForm" onSubmit="return CheckFile()">
<table border="0" cellpadding="2" cellspacing="3" width="500">
	<tr>
		<td class="sfondocomm" valign="top" colspan="2">
			<br><b>N.B.: La foto inviata deve soddisfare le seguenti caratteristiche:</b>
		    <ul>
				<li>Formati permessi: jpg o gif.
				<li>Dimensioni: 50x50 pixel.
				<li>Grandezza massima: 100 Kb.
			</ul>
		</td>
	</tr>
	<tr>
		<td width="100" class="tbltext1"><b>Foto Piccola:</b></td>
		<td width="410"><input class="textblacka" size="40" type="FILE" name="F1"></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td class="sfondocomm" valign="top" colspan="2">
			<br><b>N.B.: La foto inviata deve soddisfare le seguenti caratteristiche:</b>
		    <ul>
				<li>Formati permessi: jpg o gif.
				<li>Dimensioni: 200x100 pixel.
				<li>Grandezza massima: 100 Kb.
			</ul>
		</td>
	</tr>
	<tr>
		<td width="100" class="tbltext1"><b>Foto Grande:</b></td>
		<td width="410"><input class="textblacka" size="40" type="FILE" name="F2"></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td valign="top" colspan="2" align="center">
			<input TYPE="image" src="<%=Session("Progetto")%>/images/conferma.gif" name="avanti">
		</td>
	</tr>
</table>
</form>
<br>
<%
rstCategorie.Close
Set rstCategorie = nothing
Else

	Set upl = Server.CreateObject("Dundas.Upload.2")
		'upl.MaxFileSize = 11000
		upl.UseUniqueNames = False
		upl.Save Request.QueryString("Dir")
		
	For Each objFile in upl.Files
		SizeFileUp = objFile.size
		If SizeFileUp > 110000 Then	
			objFile.delete
			Response.Write "<TABLE width='510' border='0' cellpadding='2' cellspacing='2'>"
			Response.Write "<tr><td align='center'><img src='/images/sistdoc/Upload/FotoNOK.gif' width='400' HEIGHT='260'></td></tr>"
			Response.Write "<tr><td colspan='2' class='tbltext3' align='center'><b>La foto inviata ha una dimensione maggiore di quella permessa.</b></td></tr>"
			Response.Write "<tr><td align='center'><a href=javascript:history.back()"
			Response.Write "><img alt='Indietro' border='0' src='" & Session("Progetto") & "/images/indietro.gif'></a></td></tr>"
			Response.Write "</TABLE>"
			Response.End
		Else
			NomeDoc = upl.GetFileName(objFile.OriginalPath)
			Fotoinviate = Fotoinviate & "$" & NomeDoc
		End if
	Next

	Set upl = Nothing
	Response.Redirect "Doc_InsDoc.asp?Id=" & IdCat & "&Foto=" & Fotoinviate & sBuild
End If
%>
<!-- #include virtual="/include/closeconn.asp" -->
<!-- #include virtual="/strutt_coda2.asp" -->
