<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #include virtual="/strutt_testa2.asp" -->
<!-- #include virtual="/include/OpenConn.asp" -->
<!-- #include virtual="/util/portallib.asp" -->
<!-- #include virtual="/util/globalsub.asp" -->
<!-- #include virtual="/include/RuoloFunzionale.asp" -->
<script language="javascript">
<!--#include virtual = "/Include/help.inc"-->
</script>
<%
If Not ValidateService(Session("IdUtente"),"Gestione Gruppi",cc)  Then 
	Response.Redirect "/util/error_login.asp"
End If

	Session("ck_idgruppo")=""
	Session("ck_desgruppo")=""
	Session("ck_acronimo")=""
	Session("ck_oldidgruppo")=""
	Session("ck_aut_log")=""
	Session("ck_cod_ruofu")=""
	
 valgruppo= Request.QueryString("idgru")
   if valgruppo <> "" then
      Session("ck_idgruppo")= valgruppo
   end if 
  
   
   
    
%>
<script>
<%
if Session("Msg_Error") <> "" then
		response.write "alert('"&Session("Msg_Error")&"');"
		Session("Msg_Error")=""
  end if
%>

	function checkField(frm){
	
             if (frm.ndesgruppo.value==""){
				alert("Il campo Desgruppo � obbligatorio");
				frm.ndesgruppo.focus();
				return false;
			}
	
	        if (frm.nacronimo.value==""){
				alert("Il campo Nome cartella � obbligatorio");
				frm.nacronimo.focus();
				return false;
			}
			
			 if (frm.ncod_rorga.value == 0){
				alert("Il campo Ruolo Organizzativo � obbligatorio");
				frm.ncod_rorga.focus();
				return false;
			}
	
			return confirm("Confermi la modifica del gruppo: "+frm.ndesgruppo.value+"?");
		
	}
	function notValidChar(cadena){
		var cars="\\/\"?<>|:*";
		var i, longitud;
		longitud = cadena.length;
		if (longitud==0) return true;
		for (i=0;i<longitud;i++){
			if (cars.indexOf(cadena.charAt(i)) > -1){
				alert ("Il campo Descrizione non pu� contenere i seguenti caratteri : "+cars);
				return true;
			}
		}
		return false;
	}
	
function reload(Form){
	 val= gruppo.oldidgruppo.value
	 location.href = "pag1_modgru.asp?idgru=" + val
	}

</script>
<br>
<form method="post" action="pag1fun_modgru.asp" name="gruppo" onsubmit="return checkField(this);">
<%
If Session("ck_idgruppo") = "" Then
 %>

<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
	<tr height="18">
		<td class="sfondomenu" width="67%" height="18"><span class="tbltext0"><b>&nbsp;Configuraci�n del Sistema - Modificar Grupo</b></span></td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
		<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
	</tr>
	<tr>
		<td class="sfondocomm" width="100%" colspan="3"><br>Con esta aplicaci�n es posible modificar<br> un grupo ya existente.</b>
		</td>
	</tr>
	<tr>
		<td class="sfondocomm" width="57%" colspan="3" height="5" align="left">
			<a href="Javascript:Show_Help('/pgm/help/Gruppi/ModificaGruppo/Pag1_ModGru')"><img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>	
		</td>
	</tr>
	<tr height="17">
		<td class="sfondocomm" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
	</tr>
</table>
<br>
<table border="0" cellpadding="2" cellspacing="2" width="500">
	<tr>
		<td class="textblack" colspan="2"><b>Seleccionar un grupo de los existentes.</b></td>
	</tr>
	<tr>
		<td height="25" class="tbltext1" width="100"><b>Grupos existentes</b></td>
		<td>
			<select name="oldidgruppo" onchange="reload(this)" class="TextBlack">
			   <option value="0"></option>
<%  
				Set rstGruppo = Server.CreateObject("ADODB.RECORDSET")	
				SQLGruppo = "SELECT idgruppo,desgruppo FROM gruppo ORDER BY desgruppo"
				SQLGruppo = UCase(SQLGruppo)
'PL-SQL * T-SQL  
SQLGRUPPO = TransformPLSQLToTSQL (SQLGRUPPO) 
				rstGruppo.open SQLGruppo,CC,1,3
				rstGruppo.MoveFirst
 
				While Not rstGruppo.EOF
				    response.write "<option value='" & rstGruppo("idgruppo") & "'>" & rstGruppo("desgruppo") & "</option>"
				    rstGruppo.MoveNext
				Wend
				rstGruppo.close
				Set rstGruppo = nothing
 %>
			</select>
		</td>
	</tr>
	
	
	
	
	
</table>	
<%
   End If
If Session("ck_idgruppo")<> "" Then
	  Set rstGruppo = Server.CreateObject("ADODB.RECORDSET")	
      SQLGruppo = "SELECT idgruppo,desgruppo,webpath,cod_rorga,cod_ruofu,aut_log FROM gruppo WHERE idgruppo=" & Session("ck_idgruppo") 
      SQLGruppo = UCase(SQLGruppo)
'PL-SQL * T-SQL  
SQLGRUPPO = TransformPLSQLToTSQL (SQLGRUPPO) 
	  rstGruppo.open SQLGruppo,CC,1,3
	  'Response.Write SQLGruppo
	  'response.end
      rstGruppo.MoveFirst
      Session("ck_desgruppo")= (rstGruppo("desgruppo"))
      Session("ck_acronimo") = (rstGruppo("webpath"))
      Session("ck_cod_rorga")= (rstGruppo("cod_rorga"))
      Session("ck_cod_ruofu")= (rstGruppo("cod_ruofu"))
      Session("ck_aut_log")  = (rstGruppo("aut_log"))

	  rstGruppo.close
      Set rstGruppo = nothing    
           
%>
<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
	<tr height="18">
		<td class="sfondomenu" width="67%" height="18"><span class="tbltext0"><b>&nbsp;Configuraci�n del Sistema - Modificar Grupo</b></span></td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
		<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">(*) campi obbligatori</td>
	</tr>
	<tr>
		<td class="sfondocomm" width="100%" colspan="3"><br>En esta secci�n es posible modificar la descripci�n del grupo y el rol organizativo asociado.
		<br>Tener en cuenta que el campo Indentificador(login) es �nico y no puede ser modificado. 
		</td>
	</tr>
	<tr>
		<td class="sfondocomm" width="57%" colspan="3" height="5" align="left">
			<a href="Javascript:Show_Help('/pgm/help/Gruppi/ModificaGruppo/Pag1_ModGru_1')"><img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>	
		</td>
	</tr>
	<tr height="17">
		<td class="sfondocomm" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
	</tr>
</table>
<br>
<table border="0" cellpadding="2" cellspacing="2" width="500">
	<tr>
		<td height="25" class="tbltext1" width="100"><b>Descripci�n*</b></td>
		<td width="400"><input class="textblack" type="text" name="ndesgruppo" size="50" Value="<%=Session("ck_desgruppo")%>"></td>
	</tr>
	<tr>
		<td height="25" class="tbltext1" width="100"><b>Indentificador*</b></td>
		<td width="400"><input class="textgray" type="text" name="nacronimo" size="50" Value="<%=mid(Session("ck_acronimo"),2)%>" readonly></td>
	</tr>
	
	<tr>
		<td height="25" class="tbltext1" width="100"><b>Rol Organizativo*</b></td>
		<td>
			<select name="ncod_rorga" class="textblack">
			<option value="0"></option>
		<%        
		'	Set rstCod= Server.CreateObject("ADODB.RECORDSET")
		'		SQLCodice = "SELECT NOME_TABELLA,CODICE,DESCRIZIONE FROM TADES WHERE NOME_TABELLA='RORGA'"
'PL-SQL * T-SQL  
SQLCODICE = TransformPLSQLToTSQL (SQLCODICE) 
		'		rstCod.open SQLCodice, CC, 1, 3

		'	While rstCod.EOF <> True
		'	     Response.Write "<option value="& rstCod("codice")
		'	  If cstr(Session("ck_cod_rorga")) =  cstr(rstCod("codice")) Then
		'		 Response.Write " selected"
		'	  End If
		'	     Response.Write ">" & rstCod("descrizione") & "</option>"
		'	     rstCod.MoveNext
		'	Wend   			
			sCondizione = ""
			sTabella = "RORGA"
			dData	 = Date()
			nOrder   = 0
			Isa	 = 0
			aArray = decodTadesToArray(sTabella,dData,sCondizione,nOrder,Isa)	
			if isArray(aArray) then
				for i = 0 to ubound(aArray) - 1
					Response.Write "<option value="& aArray(i,i,i)
					If cstr(Session("ck_cod_rorga")) =  cstr(aArray(i,i,i)) Then
						 Response.Write " selected"
					End If
					Response.Write ">" & aArray(i,i+1,i) & "</option>"
				next	
				erase aArray
			end if
		%>
			</select>
		</td>
	</tr>
	<tr>
			
			<td align="left" nowrap class="tbltext1">
				<b>Ruolo Funzionale</b>
			</td>			
	        <td>
	       	        		        
<%
				sInt = "cmbruolo|0|" & Session("ck_cod_ruofu") & "|"			
				CreateRuolo (sInt)
%>
			</td>
	
	</tr>
	
	</select>
	<!--tr> 
		<td align="left" nowrap class="tbltext1"><b>Monitoraggio Funzioni</b></td-->
	<%	
		'if Session("ck_aut_log") = "S" then %>
        <!--td align="left"><input type="checkbox" name="aut_log" Value="S" style="WIDTH: 14px; HEIGHT: 20px" checked></td-->
         
     <% 'else%>
     <!--td align="left"><input type="checkbox" name="aut_log" Value="S" style="WIDTH: 14px; HEIGHT: 20px"></td-->

<%'end if%>
		
	<!--/tr-->
	
	
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2" align="center">
		    <input type="hidden" name="olddesgruppo" value>
		    <a href="pag1_modgru.asp"><img border="0" alt="Indietro" src="<%=Session("Progetto")%>/images/indietro.gif"></a>
			<input type="image" alt="Invia" src="<%=Session("Progetto")%>/images/conferma.gif" id="image1" name="image1">
		</td>
	</tr>
</table>  
<% End If %> 
<br>
<table border="0" cellpadding="2" cellspacing="1" width="500">
	<tr>
	   <td width="420"></td>
	   <% If Session("ck_idgruppo")<> "" Then %>
	   <td height="25" class="sfondocomm" align="center"><b>Paso 2/2</b></td>
	   <% Else %>
	   <td height="25" class="sfondocomm" align="center"><b>Paso 1/2</b></td>
	   <% End If %>
	</tr>
</table>
</form>
<!-- #include virtual="/include/CloseConn.asp" -->
<!-- #include virtual="/strutt_coda2.asp" -->
