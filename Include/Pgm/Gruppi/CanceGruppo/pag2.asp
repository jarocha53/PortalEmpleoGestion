<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #include virtual="/strutt_testa2.asp" -->
<!-- #include virtual="/include/OpenConn.asp" -->
<!-- #include virtual="/util/portallib.asp" -->
<!-- #include virtual="/util/globalsub.asp" -->
<script language="javascript">
<!--#include virtual = "/Include/help.inc"-->
</script>
<%
If Not ValidateService(Session("IdUtente"),"Gestione Gruppi",cc)  Then 
	Response.Redirect "/util/error_login.asp"
End If

	If Session("ck_idgruppo") = "" Then
		response.redirect "pag1.asp"
	End If
%>
<script language="javascript">
<!--
    function contrcan(varc){
      if (varc ==1){
	  if (funzioni.can_fun.checked == false){
	    alert("Il gruppo ha una o pi�  funzioni associate: Impossibile cancellare");
		return false;
	}
	 }
	else{
	  return true;
	 }
}	  

	function selcan(){
		if (funzioni.can_fun.checked){
			funzioni.cmdfun.value = "Del";
		}
		else{
			funzioni.cmdfun.value = "";
		}
	}
//-->
</script>
<br>
<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
	<tr height="18">
		<td class="sfondomenu" width="67%" height="18"><span class="tbltext0"><b>&nbsp;AMMINISTRAZIONE PORTALE - Cancellazione Gruppo</b></span></td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
		<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
	</tr>
	<tr>
		<td class="sfondocomm" width="100%" colspan="3"><br>Gruppo: <%=Session("ck_desgruppo")%>
		</td>
	</tr>
	<tr>
		<td class="sfondocomm" width="57%" colspan="3" height="5" align="left">
			<a href="Javascript:Show_Help('/pgm/help/Gruppi/CancellaGruppo/Pag2')"><img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>	
		</td>
	</tr>
	<tr height="17">
		<td class="sfondocomm" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
	</tr>
</table>
<br>
<form name="funzioni" action="pag2fun.asp" method="GET">  
<table border="0" cellpadding="1" cellspacing="1" width="500">
	<tr class="sfondocomm" height="20"> 
	     <td width="200" align="center" valign="middle">&nbsp;<b>Funzione</b></td>
	     <td width="70" align="center" valign="middle">&nbsp;<b>Namevar</b></td>
	     <td width="230" align="center" valign="middle">&nbsp;&nbsp;<b>Valore</b></td>
	</tr>
</table>
    
<table border="0" cellpadding="1" cellspacing="1" width="500">   
<%
 Dim nf
 Dim varc
 nf=0
 SqlStr ="SELECT b.desgruppo, c.desfunzione ,a.idgruppo, a.idfunzione, a.namevar, a.valore " & _
         "FROM gruppofun a, gruppo b, funzione c " & _
         "WHERE a.idgruppo=b.idgruppo and a.idfunzione=c.idfunzione and b.idgruppo="& Session("ck_idgruppo") & _
		 "ORDER BY b.desgruppo, c.desfunzione"
'PL-SQL * T-SQL  
UCASESQLSTR = TransformPLSQLToTSQL (UCASESQLSTR) 
 Set rstGruppoFun = CC.Execute(ucase(SqlStr))
 If rstGruppoFun.EOF <>True Then rstGruppoFun.MoveFirst
	While rstGruppoFun.EOF <> True
		nf = nf + 1
		Response.write "<tr class=tblsfondo>"
		Response.write "<td width='200' class='tbltext1'><b>" & FixDbField(rstGruppoFun("desfunzione")) & "</td>"
		Response.write "<td class='tbltext1' width='70' align='center'><b>" & FixDbField(rstGruppoFun("namevar")) & "</td>"
		Response.write "<td width='230' align='center'>"
		If FixDbField(rstGruppoFun("valore")) <> "" Then
			Response.write "<textarea class='textblack' cols=35 rows=2 id=textarea1 name=textarea1 readonly>" + FixDbField(rstGruppoFun("valore")) + CrLf + "</textarea></td >"
		End If
		Response.write "</tr>"
		rstGruppoFun.movenext
	Wend
	rstGruppoFun.Close
	Set rstGruppoFun = Nothing
%>
</table>

<table border="0" cellpadding="0" cellspacing="2" width="500">
<%
If nf > 0 Then
%>
	<br>
	<tr height="2">
		<td colspan="3" width="100%" background="<%=Session("progetto")%>/images/separazione.gif"></td>
	</tr>
	<tr>
		<td colspan="3" align="center" class="tbltext1">
			Cancellare le funzioni associate?: <input type="checkbox" name="can_fun" checked onclick="selcan()">
			<input type="hidden" name="cmdfun" value="Del">
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>	
<%
    varc=1
Else
%>
	<tr>
		<td colspan="3" width="100%" background="<%=Session("progetto")%>/images/separazione.gif"></td>
	</tr>
	<tr>
		<td align="center" colspan="3" class="tbltext1">Non ci sono funzioni associate al gruppo</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>	
<%
    varc=0
End if
%>
	<tr>
		<td colspan="3" align="center">
		    <input type="hidden" name="olddesgruppo" value>
		    <a href="pag1.asp"><img border="0" alt="Indietro" src="<%=Session("Progetto")%>/images/indietro.gif"></a>
			<input type="image" alt="Invia" src="<%=Session("Progetto")%>/images/conferma.gif" onclicK="return contrcan(<%=varc %>);" id="image1" name="image1">
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>	
</table>
<br>
<table border="0" cellpadding="2" cellspacing="1" width="500">
	<tr>
	   <td width="420"></td>	
	   <td height="25" class="sfondocomm" align="center"><b>Step 2/4</b></td>
	</tr>
</table>
</form>
<!-- #include virtual="/include/CloseConn.asp" -->
<!-- #include virtual="/strutt_coda2.asp" -->

