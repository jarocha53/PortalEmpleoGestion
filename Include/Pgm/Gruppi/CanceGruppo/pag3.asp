<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #include virtual="/strutt_testa2.asp" -->
<!-- #include virtual="/include/OpenConn.asp" -->
<!-- #include virtual="/util/portallib.asp" -->
<!-- #include virtual="/util/globalsub.asp" -->
<script language="javascript">
<!--#include virtual = "/Include/help.inc"-->
</script>
<%
If Not ValidateService(Session("IdUtente"),"Gestione Gruppi",cc)  Then 
	Response.Redirect "/util/error_login.asp"
End If

	If Session("ck_idgruppo") = "" Then
		Response.Redirect "pag1.htm"
	End If
	Session("ck_delute")=""	
%>
<script language="javascript">
<!--
	function selng(form){
	
		if (form.exute.value == "no") return true;
		var i,valor;
		i=form.nidgruppo.selectedIndex;
		valor=form.nidgruppo.options[i].text;
		if (form.nidgruppo.value == 0){
			if (confirm("Cancellare tutti gli utenti? :")) return true;
			else return false;
		}
		if (confirm("Aggiungere gli utenti al gruppo: " + valor +"?")) {
			form.ndesgruppo.value = valor;
			return true;
		}
		return false;
	}
//-->
</script>
<br>
<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
	<tr height="18">
		<td class="sfondomenu" width="67%" height="18"><span class="tbltext0"><b>&nbsp;AMMINISTRAZIONE PORTALE - Cancellazione Gruppo</b></span></td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
		<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
	</tr>
	<tr>
		<td class="sfondocomm" width="100%" colspan="3"><br>Gruppo: <%=Session("ck_desgruppo")%>
		</td>
	</tr>
	<tr>
		<td class="sfondocomm" width="57%" colspan="3" height="5" align="left">
			<a href="Javascript:Show_Help('/pgm/help/Gruppi/CancellaGruppo/Pag3')"><img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>	
		</td>
	</tr>
	<tr height="17">
		<td class="sfondocomm" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
	</tr>
</table>
<br>
<form name="funzioni" action="pag3fun.asp" method="GET" onsubmit="return selng(this)">
<table border="0" cellpadding="0" cellspacing="0" width="500">
	<tr class="sfondocomm" height="20&quot;"> 
	     <td width="150" align="left" valign="middle">&nbsp;<b>Login</b></td>
	     <td width="170" align="left" valign="middle">&nbsp;<b>Cognome</b></td>
	     <td width="180" align="left" valign="middle">&nbsp;<b>Nome</b></td>
	</tr>
</table>
<table border="0" cellpadding="0" cellspacing="2" width="500"> 
<%
Set rstUtente = Server.CreateObject("ADODB.RECORDSET")
	SQLUtente = "SELECT login,cognome,nome FROM utente WHERE idgruppo=" & Session("ck_idgruppo") & " ORDER BY cognome,nome"
	SQLUtente = UCase(SQLUtente)
'PL-SQL * T-SQL  
SQLUTENTE = TransformPLSQLToTSQL (SQLUTENTE) 
	rstUtente.open SQLUtente, CC, 1, 3

If not rstUtente.EOF  Then 
	rstUtente.MoveFirst
	 
	While not rstUtente.EOF
	Response.write "<tr>"+CrLf
	Response.write "<td width='150' class=tbltext1><b>" & FixDbField(rstUtente("login")) & "</b></td>"
	Response.write "<td width='170' align='left' class=tbltext1><b>" & FixDbField(rstUtente("cognome")) & "</b></td>"
	Response.write "<td width='180' align='left' class=tbltext1><b>"+FixDbField(rstUtente("nome")) & "</b></td>"
	Response.write "</tr>"
	rstUtente.movenext
	Wend
	rstUtente.Close
	
%> 
</table>
<br>
<table border="0" cellpadding="0" cellspacing="2" width="500">
	<tr height="2">
		<td colspan="3" width="100%" background="<%=Session("progetto")%>/images/separazione.gif"></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>	
	<tr>
		<td colspan="3" align="center" class="tbltext1">
			Spostare gli utenti nel gruppo : 
			<select name="nidgruppo">
		   	<option value="0"></option>
<%   
			Set rstGruppo = Server.CreateObject("ADODB.RECORDSET")
				SQLGruppo = "SELECT IDGRUPPO,DESGRUPPO FROM GRUPPO ORDER BY DESGRUPPO"
				SQLGruppo = UCase(SQLGruppo)
'PL-SQL * T-SQL  
SQLGRUPPO = TransformPLSQLToTSQL (SQLGRUPPO) 
				rstGruppo.open SQLGruppo, CC, 1, 3
				rstGruppo.MoveFirst
			While not rstGruppo.EOF
			 	if Session("ck_idgruppo") <> FixDbField(rstGruppo("IDGRUPPO")) then 
					response.write "<option value='" & rstGruppo("IDGRUPPO") & "'>" & rstGruppo("DESGRUPPO") & "</option>"
				end if
			  rstGruppo.MoveNext
			Wend
			rstGruppo.close
			Set rstGruppo = nothing
%>   
  			</select> 
			<input type="hidden" name="exute" value="si">
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>	
<% Else %>
	<tr>
		<input type="hidden" name="exute" value="no">
		<td colspan="3"></td>
	</tr>
	<tr>
		<td align="center" colspan="3" class="tbltext1">Non ci sono utenti associati al gruppo</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>	
<%
  End If
  Set rstUtente = nothing
%>
	<tr>
		<td colspan="3" align="center">
			<input type="hidden" name="ndesgruppo" value>
			<a href="pag2.asp"><img border="0" alt="Indietro" src="<%=Session("Progetto")%>/images/indietro.gif"></a>
			<input type="image" alt="Invia" src="<%=Session("Progetto")%>/images/conferma.gif" id="image1" name="image1">
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>	
</table>
<br>
<table border="0" cellpadding="2" cellspacing="1" width="500">
	<tr>
	   <td width="420"></td>	
	   <td height="25" class="sfondocomm" align="center"><b>Step 3/4</b></td>
	</tr>
</table>
</form>
<!-- #include virtual="/include/CloseConn.asp" -->
<!-- #include virtual="/strutt_coda2.asp" -->
