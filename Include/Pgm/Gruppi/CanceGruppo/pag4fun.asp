<!-- #include virtual="/util/asisfun.asp"-->
<%
If Not ValidateService(Session("IdUtente"),"Gestione Gruppi",cc)  Then 
	Response.Redirect "/util/error_login.asp"
End If

	deleteCartella CartellaGruppo(Session("ck_idgruppo"),CC),CC
	
	deleteGruppo Session("ck_idgruppo"),CC

	if Session("ck_delfun") = "Del" then
		deleteGruppoFunzioni Session("ck_idgruppo"),CC
	end if
	
	if Session("ck_delute") = "Del" then
		deleteGruppoUtenti Session("ck_idgruppo"),CC
	end if
	
	if Session("ck_delute") = "Mov" then
		moveUtentiToGruppo Session("ck_idgruppo"),Session("ck_nidgruppo"),CC
	end if

	'Gruppo cancellare
	Session("ck_idgruppo") = ""
	Session("ck_desgruppo") = ""
	Session("ck_nidgruppo") = ""
	Session("ck_ndesgruppo") = ""
	Session("ck_delfun") = ""
	Session("ck_delute") = ""
	
	response.redirect "../pagend.asp"
%>