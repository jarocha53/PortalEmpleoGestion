<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #include Virtual="/strutt_testa2.asp" -->
<!-- #include virtual="/util/portallib.asp" -->
<!-- #include virtual="/include/OpenConn.asp" -->
<!-- #include virtual="/util/globalsub.asp" -->

<%
If Not ValidateService(Session("IdUtente"),"Gestione Wizard",cc)  Then 
	Response.Redirect "/util/error_login.asp"
End If

nValGruppo = Request.QueryString("idgr")
If nValGruppo <> "" Then
	Session("ck_idgruppo") = nValGruppo  
End If  

If Session("Msg_Error") = "" then
	Session("ck_nome")=""
	Session("ck_cognome")=""
	Session("ck_email")=""
	Session("ck_password")=""
	Session("ck_login")=""
End If	
	Session("ck_idpersona")=""
	Session("ck_funute")=""	
	Session("Msg_Success")=""
%>
<script Language="javascript">
<!--#include virtual = "/Include/ControlString.inc"-->
<!--#include Virtual = "/Include/help.inc"-->

function Controlla(frm) {
	if  (FormUtente.cognome.value != "")  {
		alert("E' stato riempito il campo Cognome: non e' possibile selezionare il pulsante avanti ");
		FormUtente.cognome.focus();
		return false;
	}
	if  (FormUtente.nome.value != "")  {
		alert("E' stato riempito il campo Nome: non e' possibile selezionare il pulsante avanti ");
		FormUtente.nome.focus();
		return false;
	}
	if  (FormUtente.email.value != "")  {
		alert("E' stato riempito il campo Email: non e' possibile selezionare il pulsante avanti ");
		FormUtente.cognome.focus();
		return false;
	}
	if  (FormUtente.login.value != "")  {
		alert("E' stato riempito il campo Login: non e' possibile selezionare il pulsante avanti ");
		FormUtente.login.focus();
		return false;
	}
	if  (FormUtente.password.value != "")  {
		alert("E' stato riempito il campo Password: non e' possibile selezionare il pulsante avanti ");
		FormUtente.password.focus();
		return false;
	}
	
	/*if (FormUtente.password.value.length < 8){
		alert("La nuova password deve essere di almeno 8 caratteri");
		FormUtente.password.focus();
		return false;
	}*/
}

function checkField(Form){
	if (Form.cognome.value == ""){
		alert("Il campo Cognome � obbligatorio");
		Form.cognome.focus();
		return(false);
	}			
	if (Form.nome.value == "") {
		alert("Il campo Nome � obbligatorio");
		Form.nome.focus();
		return(false);
	}
	if (Form.email.value == "") {
		alert("Il campo Email � obbligatorio");
		Form.email.focus();
		return(false);
	}
	if (!ValidateEmail(Form.email.value)){
		alert("Il campo Email � formalmente errato!");
		Form.email.focus();
		return false
	}
	if (Form.login.value == "") {
		alert("Il campo Login � obbligatorio");
		Form.login.focus();
		return(false);
	}
	//inizio 24/10/2003
	appoApice = Form.login.value
	if (ChechSingolChar(appoApice, "'")== false ) {
		alert("Il campo Login � formalmente errato");
		Form.login.focus();
		return false;
	}
	//fine 24/10/2003
	if  (Form.login.value.length >15){
		alert("La lunghezza massima del campo login � 15 caratteri");
		FormUtente.login.focus();
		return false;
	}	
	
	if (Form.password.value == "") {
		alert("Il campo Password � obbligatorio");
		Form.password.focus();
		return(false);
	}
	
	if (Form.password.value.length < 8){
	alert("La nuova password deve essere di almeno 8 caratteri");
	Form.password.focus();
	return false;
	}
}

function reload(Form){
	 val= FormUtente.oldidgruppo.value
	 location.href = "pag3.asp?idgr=" + val
	}
<%
if Session("Msg_Error") <> "" then
		response.write "alert('"&Session("Msg_Error")&"');"
		Session("Msg_Error")=""
end if
%>
//-->
</script>
<body>
<br>
<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
	<tr height="18">
		<td class="sfondomenu" width="67%" height="18"><span class="tbltext0"><b>&nbsp;Configuraci�n del Sistema - Gesti�n de Grupos</b></span></td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
		<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
	</tr>
	<tr>
		<td class="sfondocomm" width="57%" colspan="3"><br>
			<p> Si desea asociar a un usuario una funci�n ya creada presione &quot;Avanzar&quot;.</p>
		</td>
	</tr>
	<tr height="17">
		<td class="sfondocomm" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
	</tr>
</table>
<br>
<form method="POST" action="pag3fun.asp" name="FormUtente" onSubmit="return checkField(this);">
<% 
If Session("ck_desgruppo")="" Then
   If nValGruppo="" Then 
%>
<table border="0" width="500" class="sfondocomm">
	<tr height="30">
		<td width="480" align="left" valign="middle">&nbsp;<b>Buscar Grupo</b></td>
		<td align="right" valign="middle">
		<!--img src="<%=Session("Progetto")%>/images/help.gif" border="0"-->
		<a href="Javascript:Show_Help('/Pgm/help/Wizard/creagruppo/pag3/')" name onmouseover="javascript:status='' ; return true">
     	<img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
		</td>  
	</tr>   
</table> 

<table border="0" width="500">
	<tr height="20">
		<td align="left" colspan="4" class="textblack">
		<b>Seleccionar un grupo de la lista desplegable</b></td>
	</tr>	
    <tr>
		<td height="25" class="tbltext1" align="left">&nbsp;Grupos Existentes</td>
		<td><select name="oldidgruppo" onchange="reload(this)">
			<option value="0"></option>
<%
			Set rstGruppo = Server.CreateObject("ADODB.RECORDSET")
				SQLGruppo = "SELECT IDGRUPPO,DESGRUPPO FROM GRUPPO ORDER BY 2"
'PL-SQL * T-SQL  
SQLGRUPPO = TransformPLSQLToTSQL (SQLGRUPPO) 
				rstGruppo.open SQLGruppo, CC, 1, 3
				rstGruppo.MoveFirst
 
				While rstGruppo.EOF <> True 
				    response.write "<option value='" & rstGruppo.fields(0) & "'>" & rstGruppo.fields(1) & "</option>"
				    rstGruppo.MoveNext
				Wend
				rstGruppo.close
				set rstGruppo=nothing
%>
			</select>
		</td>
	</tr>
</table>
<%
   Else
		Set rstGruppoVal = Server.CreateObject("ADODB.RECORDSET")
		SQLGruppoVal = "SELECT IDGRUPPO, DESGRUPPO FROM GRUPPO WHERE IDGRUPPO = " & nValGruppo & "ORDER BY 2"
'PL-SQL * T-SQL  
SQLGRUPPOVAL = TransformPLSQLToTSQL (SQLGRUPPOVAL) 
		rstGruppoVal.open SQLGruppoVal, CC, 1, 3
		rstGruppoVal.MoveFirst
		Session("c_desgruppo") = (rstGruppoVal.fields(1))
   End If
End If

If (Session("c_desgruppo") <> "" or Session("ck_desgruppo") <> "") Then 
%> 	
<br>
<table border="0" cellspacing="0" cellpadding="0" width="500" class="sfondocomm">
    <tr height="30"> 
   
       <td width="370" align="left" valign="middle">&nbsp;<b>Gestione utenti del gruppo:<%=Session("c_desgruppo")%><%=Session("ck_desgruppo")%></b></td>
		<td align="right" valign="middle">
		<!--img src="<%=Session("Progetto")%>/images/help.gif" border="0"-->
		<a href="Javascript:Show_Help('/Pgm/help/Wizard/creagruppo/pag3_1/')" name onmouseover="javascript:status='' ; return true">
     	<img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
		</td>  
	</tr>
</table>
<table border="0" cellspacing="0" cellpadding="0" width="500">
	<tr height="30">
		<td align="left" colspan="4" width="500" class="textblack">
		<b>En esta secci�n podr� ingresar los datos del usuario que pertenecer� al grupo seleccionado.</b>
    </tr>
	<tr>
		<td height="25" width="60" class="tbltext1" align="left">&nbsp;Apellido*</td>
		<td><input type="text" name="cognome" size="50" Value="<%=Session("ck_cognome")%>" class="textblack"></td>
	</tr>
	<tr>
		<td height="25" class="tbltext1">&nbsp;Nombre*</td>
		<td><input type="text" name="nome" size="50" Value="<%=Session("ck_nome")%>" class="textblack"></td>
	</tr>
	<tr>
		<td height="25" class="tbltext1">&nbsp;Email*</td>
		<td><input type="text" name="email" size="30" Value="<%=Session("ck_email")%>" class="textblack"></td>
	</tr>
	<tr>
		<td height="25" class="tbltext1">&nbsp;Usuario*</td>
		<td><input type="text" name="login" size="50" Value="<%=Session("ck_login")%>" class="textblack"></td>
	</tr>
	<tr>
		<td height="25" class="tbltext1">&nbsp;Contrase�a*</td>
		<td><input type="text" name="password" size="50" Value="<%=Session("ck_password")%>" class="textblack"></td>
	</tr>
	
			
<%
			Set rstUorg = Server.CreateObject("ADODB.RECORDSET")
				SQLUorg = "SELECT ID_UORG,DESC_UORG FROM UNITA_ORGANIZZATIVA ORDER BY DESC_UORG"
'PL-SQL * T-SQL  
SQLUORG = TransformPLSQLToTSQL (SQLUORG) 
				rstUorg.open SQLUorg, CC, 1, 3
				
				
%>
			<tr>
			   <span>
				    <td height="25" class="tbltext1">&nbsp;Unidad Funcional</td>
<%				IF not rstUorg.eof then 	  %>
				    <td><select name="cmbuorg" type="text" class="textblack">
				  <option value="0"></option>
			   </span>
			
	<%				
													
				Do until rstUorg.EOF
					Response.Write "<OPTION "
					response.write "<option value='" & rstUorg("ID_UORG") & "'>" & rstUorg("DESC_UORG") & "</option>"
						
				rstUorg.MoveNext 
				Loop
								
				Response.Write "</SELECT></td>"
		ELSE %>
                 

			<td align="left" class="tbltext">
				<b>No han sido definidos para el proyecto.</b>
			</td>
<%		END IF 
		rstUorg.Close
		set rstUorg=nothing %>	
		
		</select>
		
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	
	<tr>
		<td colspan="4" align="center">
			<a href="pag2.asp"><img alt="Indietro" border="0" src="<%=Session("Progetto")%>/images/indietro.gif"></a>
			<input type="image" alt="Invia" border="0" src="<%=Session("Progetto")%>/images/conferma.gif">
			<a href="pag31.asp"><img alt="Avanti" border="0" src="<%=Session("Progetto")%>/images/avanti.gif" onclick="return Controlla(this)"></a>
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>	
</table>
<% Else %>
<br>
<table border="0" cellspacing="0" cellpadding="0" width="500">
	<tr>
		<td colspan="4" align="center">
			<a href="pag2.asp"><img alt="Indietro" border="0" src="<%=Session("Progetto")%>/images/indietro.gif"></a>
			<a href="pag31.asp"><img alt="Avanti" border="0" src="<%=Session("Progetto")%>/images/avanti.gif"></a>
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>	
</table>
<% End if %> 
<br>
<table border="0" cellpadding="2" cellspacing="1" width="500">
	<tr>
		<td width="420"></td>	
		<td height="25" class="sfondocomm" align="center"><b>Paso 3/4</b></td>
	</tr>
</table>
</form>
<!-- #include virtual="/include/closeconn.asp" -->
<!-- #include Virtual="/strutt_coda2.asp" -->
