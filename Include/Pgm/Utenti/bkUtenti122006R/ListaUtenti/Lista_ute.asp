<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #include virtual="/strutt_testa1.asp" -->
<!-- #include virtual="/include/OpenConn.asp" -->
<!-- #include virtual="/util/portallib.asp" -->
<!-- #include virtual="/util/globalsub.asp" -->
<%
If Not ValidateService(Session("IdUtente"),"Gestione Utenti",cc) Then 
	Response.Redirect "/util/error_login.asp"
End If

Dim nActPagina
Dim nTotPagina
Dim nTamPagina		
Dim nTotRecord

nTamPagina = 25

If Request.Querystring("pagina") = "" Then
	nActPagina = 1
Else
	nActPagina = Clng(Request.Querystring("pagina"))
End If
%>
<script>
<!--#include Virtual = "/Include/help.inc"-->
</script>

<br>
<table align="center" border="0" CELLPADDING="0" CELLSPACING="0" width="740">
	<tr height="18">
		<td class="sfondomenu" width="67%" height="18"><span class="tbltext0"><b>&nbsp;Configuración del Sistema - Listado de Usuarios</b></span></td>
		<td width="2%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">
		<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
	</tr>
	<tr height="18">
	    <td class="sfondocomm" width="57%" colspan="3">
		   <a href="Javascript:Show_Help('/Pgm/help/Utenti/ListaUtenti/Lista_ute/')" name onmouseover="javascript:status='' ; return true">
		     <img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></td>
	</tr>
	<tr height="2">
		<td class="sfondocomm" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
	</tr>
	
	
</table>
<br>
<br>
<table align="center" border="0" cellpadding="1" cellspacing="1" width="740">
	<tr class="sfondocomm"> 
		<td align="left" height="10" valign="middle"><b>Id</b></td>
		<td align="left" valign="middle"><b>Apellido</b></td>
		<td align="left" valign="middle"><b>Nombre</b></td>
		<td align="left" valign="middle"><b>Usuario</b></td>
		<td align="left" valign="middle"><b>Oficina de Empleo</b></td>
		<td align="left" valign="middle"><b>Grupo</b></td>
		<!--<td align="left" valign="middle"><b>Unidad Funcional</b></td>
		<td align="left" valign="middle"><b>Ref.</b></td>
		<td align="left" valign="middle"><b>Creador</b></td>-->
	</tr>
<%


Set rstUtente = Server.CreateObject("ADODB.RECORDSET")
	SQLUtente = "SELECT a.idutente, a.cognome, a.nome, a.login, a.email, a.password, a.idgruppo, a.id_uorg, a.tipo_pers, b.desgruppo, c.desc_uorg, a.creator " & _
				"FROM utente a, gruppo b, unita_organizzativa c " & _
				"WHERE a.idgruppo=b.idgruppo and a.id_uorg=c.id_uorg(+) " & _
				"ORDER BY a.cognome, a.nome, a.idutente, b.desgruppo "
 
 

 				
'PL-SQL * T-SQL  
SQLUTENTE = TransformPLSQLToTSQL (SQLUTENTE) 
	rstUtente.open SQLUtente, CC, 1, 3
	
 rstUtente.PageSize = nTamPagina
 rstUtente.CacheSize = nTamPagina

 nTotPagina = rstUtente.PageCount
 
 If nActPagina < 1 Then
	nActPagina = 1
 End If
 If nActPagina > nTotPagina Then
	nActPagina = nTotPagina
 End If

 rstUtente.AbsolutePage=nActPagina
 nTotRecord=0
 
 While rstUtente.EOF <> True And nTotRecord < nTamPagina 
 %>
	<tr class="tblsfondo"> 
	    <td align="left" class="tbltext1">
	        <input type="hidden" value="<%=rstUtente("IDUTENTE")%>" name="txtidute">
	   		<%=LCase(rstUtente("IDUTENTE"))%>
	    </td>
	    <td align="left" class="tbltext1">
		    <input type="hidden" value="<%=rstUtente("COGNOME")%>" name="txtcog">
			<%=lcase(rstUtente("COGNOME"))%>
		</td>
		<td align="left" class="tbltext1">
		    <input type="hidden" value="<%=rstUtente("NOME")%>" name="txtcog">
			<%=lcase(rstUtente("NOME"))%>
		</td>
		<td align="left" class="tbltext1">
			<input type="hidden" value="<%=rstUtente("LOGIN")%>" name="txtlog">
			<%=lcase(rstUtente("LOGIN"))%>
		</td>
		<td align="left" class="tbltext1">
		    <input type="hidden" value="<%=rstUtente("PASSWORD")%>" name="txtpas">
		<%
			dim rsSede 
			set rsSede = server.CreateObject("adodb.recordset")
			
			
'PL-SQL * T-SQL  
			set rsSede = cc.execute("select descrizione from sede_impresa where id_sede = " & rstUtente("CREATOR"))
			
			if not rsSede.EOF then 
				Response.Write lcase(rsSede("descrizione"))
			end if 
			
			set rsSede = nothing
		%>

		</td>
		<!--<td align="left" class="tbltext1">
		    <input type="hidden" value="<%=rstUtente("PASSWORD")%>" name="txtpas">
			<%=lcase(rstUtente("PASSWORD"))%>
		</td>-->
		<td align="left" class="tbltext1">
			<input type="hidden" value="<%=rstUtente("DESGRUPPO")%>" name="txtdgr">
			<%=lcase(rstUtente("DESGRUPPO"))%>
		</td>
		<!--<td align="left" class="tbltext1">
		    <input type="hidden" value="<%=rstUtente("DESC_UORG")%>" name="txtno">
			<%=lcase(rstUtente("DESC_UORG"))%>
		</td>
		
		<td align="left" class="tbltext1">
		    <input type="hidden" value="<%=rstUtente("TIPO_PERS")%>" name="txtno">
			<%=lcase(rstUtente("TIPO_PERS"))%>
		</td>
		<td align="left" class="tbltext1">
			<input type="hidden" value="<%=rstUtente("CREATOR")%>" name="txtcre">
			<%=lcase(rstUtente("CREATOR"))%>
		</td>-->
	</tr>	
<%
 nTotRecord=nTotRecord + 1	
 rstUtente.MoveNext
 Wend
 rstUtente.Close
 Set rstUtente = Nothing
%> 
</table>
<br>
<%
Response.Write "<TABLE align='center' border='0' width='740'>"
Response.Write "<tr>"
If nActPagina > 1 Then
	Response.Write "<td align='right' width='720'><a href='Lista_ute.asp?pagina="& nActPagina-1 & "'>"
	Response.Write "<img alt='Pagina precedente' border='0' src='" & Session("Progetto") & "/images/precedente.gif'></A></td>"
End If
If nActPagina < nTotPagina Then
	Response.Write "<td align='right'><a href='Lista_ute.asp?pagina=" & nActPagina+1 & "'>"
	Response.Write "<img alt='Pagina successiva' border='0' src='" & Session("Progetto") & "/images/successivo.gif'></A></td>"
End If
Response.Write "</tr></TABLE>"
%>
<table border="0" cellpadding="0" cellspacing="1" width="740">
	<tr>
		<td align="middle" colspan="2" width="60%"><b>
			<a HREF="javascript:history.back()"><img SRC="<%=Session("Progetto")%>/images/indietro.gif" border="0">
		</td>
	</tr>
</table>
<!-- #include virtual="/include/closeconn.asp" -->
<!-- #include Virtual="/strutt_coda1.asp" -->
