<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #include virtual="/strutt_testa2.asp" -->
<!-- #include virtual="/include/openconn.asp" -->
<!-- #include virtual="/util/portallib.asp" -->
<!-- #include virtual="/util/globalsub.asp" -->
<%
If Not ValidateService(Session("IdUtente"),"Gestione Utenti",cc) Then 
	Response.Redirect "/util/error_login.asp"
End If

	If Session("ck_idpersona") = "" Then
		Response.Redirect "pag1.asp"
	End If
%>

<script language="javascript">
<!--#include Virtual = "/Include/help.inc"-->

<!--
    function contrcan(varc){
      if (varc ==1){
	  if (utente.can_fun.checked == false){
	    alert("L'utente ha una o pi�  funzioni associate: Impossibile cancellare");
		return false;
	}
	 }
	else{
	  return true;
	 }
}	  

	function selcan(){
		if (utente.can_fun.checked){
			utente.cmdfun.value="Del";
		}
		else{
			utente.cmdfun.value="";
		}
	}
//-->
</script>
<br>
<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
	<tr height="18">
		<td class="sfondomenu" width="67%" height="18"><span class="tbltext0"><b>&nbsp;AMMINISTRAZIONE PORTALE - Cancellazione Utente</b></span></td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
		<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
	</tr>
	<tr>
		<td class="sfondocomm" width="57%" colspan="3">
		<br><p>Utente da eliminare: <%=Session("ck_cognome")%>  &nbsp; <%=Session("ck_nome")%></p> 
		</td>
	</tr>
	<tr height="18">
	    <td class="sfondocomm" width="57%" colspan="3">
		   <a href="Javascript:Show_Help('/Pgm/help/Utenti/CanceUtente/Pag3')" name onmouseover="javascript:status='' ; return true">
		     <img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></td>
	</tr>
	<tr height="17">
		<td class="sfondocomm" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
	</tr>
</table>
<br>

<table border="0" cellspacing="0" cellpadding="0" width="500">
	<tr height="30" class="sfondocomm"> 
		<td width="480" align="left" valign="middle"><b>Elenco Funzioni associate all'utente</b></td>  
	</tr>
</table>
<br>
<form name="utente" action="pag3fun.asp" method="GET"> 
<table border="0" cellpadding="2" cellspacing="0" width="500">
	<tr class="sfondocomm"> 
	     <td height="26" width="15"></td>
	     <td align="left" valign="middle"><b>Funzione</b></td>
	     <td align="left" valign="middle"><b>Nome variabile</b></td>
	     <td align="left" valign="middle"><b>Valore</b></td>
	</tr>    
<%
Dim nf
Dim varc
	nf=0
	Set rstUtente = Server.CreateObject("ADODB.RECORDSET")
		SQLUtente ="SELECT b.idutente, b.cognome, c.desfunzione ,a.idutente, a.idfunzione, a.namevar, a.valore " & _
				   "FROM utefun a, utente b, funzione c " & _
				   "where a.idutente=b.idutente and a.idfunzione=c.idfunzione and b.idutente="& Session("ck_idpersona") & _
				   "order by b.cognome, c.desfunzione"
	SQLUtente = UCase(SQLUtente)
'PL-SQL * T-SQL  
SQLUTENTE = TransformPLSQLToTSQL (SQLUTENTE) 
	rstUtente.open SQLUtente,CC,1,3
	
	If rstUtente.EOF <> True Then rstUtente.MoveFirst
		While rstUtente.EOF <> True
		nf = nf + 1
		Response.write "<tr class='tblsfondo'>"
		Response.write "<td></td>"
		Response.write "<td class='tbltext1'>" & FixDbField(rstUtente("desfunzione"))& "</td>"
		Response.write "<td class='tbltext1'>" & FixDbField(rstUtente("namevar"))& "</td>"
		Response.write "<td class='tbltext1'>" & FixDbField(rstUtente("valore")) & "</td>"
		Response.write "</tr>"
		rstUtente.movenext
		Wend
		rstUtente.Close
		Set rstUtente = Nothing

	If nf > 0 Then
%>
	<tr>
		<td width="15"></td>
		<td colspan="3"><hr></td>
	</tr>
	<tr>
		<td></td>
		<td colspan="3" align="center" class="tbltext">
			Cancellare le funzioni associate? : <input type="checkbox" name="can_fun" checked onclick="selcan()">
			<input type="hidden" name="cmdfun" value="Del"></td>
	</tr>
<%
    varc=1
	Else
%>
	<tr>
		<td> </td>
		<td align="center" colspan="3" class="tbltext">Non ci sono funzioni associate all'utente</td>
	</tr>
<%
    varc=0
	End If
%>
	<tr>
		<td colspan="4" align="center"><br>
		<a href="pag2.asp"><img name="oi" border="0" alt="Indietro" src="<%=Session("Progetto")%>/images/indietro.gif"></a>
		<input type="image" alt="Invia" src="<%=Session("Progetto")%>/images/conferma.gif" name="conferma" onclick="return contrcan(<%=varc %>);">
		</td>
	</tr>		
</table>
<br>
<table border="0" cellpadding="2" cellspacing="1" width="500">
	<tr>
	   <td width="420"></td>	
	   <td height="25" class="sfondocomm" align="center"><b>Step 4/5</b></td>
	</tr>
</table>
</form>
<!-- #include virtual="/include/closeconn.asp" -->
<!-- #include virtual="/strutt_coda2.asp" -->

