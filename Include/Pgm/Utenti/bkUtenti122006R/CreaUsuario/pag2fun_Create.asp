<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%@Language=VBScript%>
<!-- #include virtual="/strutt_testa2.asp" -->
<!-- #include virtual="/util/portallib.asp" -->
<!-- #include virtual="/include/OpenConn.asp" -->
<!-- #include virtual="/includeMTSS/Common.asp" -->
<!-- #include virtual="/include/ControlDateVB.asp" -->
<!-- #include virtual="/util/dbutil.asp" -->
<%
If Not ValidateService(Session("IdUtente"),"Gestione Utenti",cc) Then 
	Response.Redirect "/util/error_login.asp"
End If

Dim sDesLogin
Dim iGrupApp
Dim iVarPres
 
iGrupApp= Request.Form("idgruppo")

'If CLng(iGrupApp) <> CLng(Session("ck_idgruppo")) Then 
'	Set rstUteFun = Server.CreateObject("ADODB.RECORDSET")
'		SQLUtente = "DELETE FROM utefun WHERE idutente='" & Session("ck_idpersona") & "'"
'		rstUteFun.open SQLUtente,CC,1,3	  
'End If 
    
'Campi da modificare
' la replace dell'apice per nome e cognome e' del 15/04/05 (problemi per OPLA)
	 
Session("ck_cognome") = Request.Form("cognome")
'Session("ck_cognome") = replace(Session("ck_cognome"),"'","''")

Session("ck_nome") = Request.Form("nome")
'Session("ck_nome") = replace(Session("ck_nome"),"'","''")

Session("ck_email") = Request.Form("email")
Session("ck_login") = Request.Form("login")
Session("ck_IdTipoDoc") = Request.Form("IdTipoDoc")
Session("ck_NDoc") = Request.Form("NDoc")
Session("ck_IdSexo") = Request.Form("IdSexo")
VecchiaPWD = Request.Form("VecchiaPWD")
Session("ck_password") = Request.Form("password")
Session("ck_idOficina") = Request.Form("idOficina")
Session("ck_idgruppo") = Request.Form("idgruppo")
Session("ck_grupoid") = Request.Form("grupoid")
Session("ck_creator") = Request.Form("IdUtente")
Session("cmbUorg1") = Request.Form("cmbUorg")
DataIscr = ConvStringToDate(Request.Form("dtiscrizione"))
Session("ck_dt_iscrizione") = DataIscr
Session("ck_actor") = Request.Form("actor")
NivelId = request("nivelid")

If Request.Form("dtaccesso") <> "" Then
	DataAccesso = ConvStringToDate(Request.Form("dtaccesso"))
	Session("ck_dt_accesso") = DataAccesso
Else
   Session("ck_dt_accesso") = ""
End if

Session("ck_progetto") = Request.Form("progetto")
Session("ck_ind_abil") = Request.Form("indabil")

'---------------------------------------------------------
'	Doy de alta el Usuario en la base de Seguridad
'---------------------------------------------------------
Dim cmdUsuario, status, mensaje, UsuarioId,dt_pwd, sMunicipio

'dt_pwd = dateadd(d,180,Session("ck_dt_iscrizione"))

'---------------------------------------------------------
'	Busco el Municipio de la oficina
'---------------------------------------------------------
    Set rstQryOficinaMuni = server.CreateObject("ADODB.RECORDSET")
    SqlBuscaMuni = "Select Comune From SEDE_IMPRESA Where ID_SEDE =" & Session("ck_idOficina")
    SqlBuscaMuni = UCase(SqlBuscaMuni)
'PL-SQL * T-SQL  
SQLBUSCAMUNI = TransformPLSQLToTSQL (SQLBUSCAMUNI) 
	rstQryOficinaMuni.Open SqlBuscaMuni, cc 
	
	If not rstQryOficinaMuni.EOF then
		sMunicipio = rstQryOficinaMuni("Comune").Value
		'Response.Write sMunicipio
		'Response.Write Session("ck_idOficina")
		'Response.End
	End If
'---------------------------------------------------------
'	Busco si la Oficina tiene multiples oficinas asociadas
'---------------------------------------------------------
	Set rstQryOficinaMulti = server.CreateObject("ADODB.RECORDSET")
    SqlBuscaOficina = "Select OficinaPadre From OficinasRelacion Where OficinaHija =" & Session("ck_idOficina")
    SqlBuscaOficina = UCase(SqlBuscaOficina)
'PL-SQL * T-SQL  
SQLBUSCAOFICINA = TransformPLSQLToTSQL (SQLBUSCAOFICINA) 
	rstQryOficinaMulti.Open SqlBuscaOficina, cc 
	
	If not rstQryOficinaMulti.EOF then
		sIdOficina = rstQryOficinaMulti("OficinaPadre")	
	Else
		sIdOficina = Session("ck_idOficina")
	End If

Set cmdUsuario = Server.CreateObject("ADODB.Command")
With cmdUsuario
    .ActiveConnection = gConnSeguridad
	.CommandTimeout = 1200
	.CommandText = "spUsuarioNuevoGenerico"
	.CommandType = 4
		
	.Parameters("@Nombre").Value = Session("ck_nome")
	.Parameters("@Apellido").Value = Session("ck_cognome")
	
	.Parameters("@UserName").Value = Session("ck_login")
	.Parameters("@Password").Value = Session("ck_password")
	.Parameters("@TipoDocId").Value = Session("ck_IdTipoDoc")
	.Parameters("@DocNumero").Value = Session("ck_Ndoc")
	.Parameters("@SexoId").Value = Session("ck_IdSexo")
	.Parameters("@Habilitado").Value = 1
	.Parameters("@UsuarioEstadoID").Value = 1
	.Parameters("@UsuarioCreador").Value = Session("IdUtente")
	.Parameters("@MunicipioID").Value = sMunicipio
	.Parameters("@GrupoID").Value = Session("ck_grupoid")
	.Parameters("@Actor").Value = Session("ck_actor")
	.Parameters("@NivelID").value = NivelId
	'--------------------------------------------------------
	'Oficina asociada al usuario, que puede ser una unica
	' oficina o la dependiente de otra
	'-------------------------------------------------------
	.Parameters("@OficinaDePertenencia").value = sIdOficina
	
		'--------------------------------------------
		' Carga parametros
		'--------------------------------------------
		'For Each Parameter In .Parameters
		'	response.write Parameter.Name & "=" & Parameter.value & "<BR>"
		'Next	
		'Response.End
	 	
	'Response.Write Session("ck_nome") & "<BR>"
	'Response.Write Session("ck_cognome") & "<BR>"
	'Response.Write Session("ck_login") & "<BR>"
	'Response.Write Session("ck_password") & "<BR>"
	'Response.Write Session("ck_Ndoc") & "<BR>"
	'Response.End
	
'PL-SQL * T-SQL  
	.Execute()
	
	'Parametros de Output
	'Session("ck_idpersona") 
	UsuarioId = .Parameters("@UsuarioId").value
	Status = .Parameters("@Status").value
	Mensaje = .Parameters("@Mensaje").value
'	Response.End
End With    
'Response.Write UsuarioId & "<BR>"
'Response.Write Status & "<BR>"
'Response.Write Mensaje & "<BR>"
'Response.End

'-----------------------------------------------
'	Analizo resultados del alta en Seguridad
'-----------------------------------------------
If Status = 200 then
	'..Response.Write Session("ck_login") & "<br>"		' FABIO
	'------------------------------------------------------------------------------------------------------
    ' El usuario existe en la base de seguridad, me fijo en la base del portal si ya esta dado de alta
    '------------------------------------------------------------------------------------------------------
    Set rstQryUtente = server.CreateObject("ADODB.RECORDSET")
    SqlBuscaUsuario = "Select * From BA.Utente Where Login = '" & Session("ck_login") & "'"
    SqlBuscaUsuario = UCase(SqlBuscaUsuario)
    
    '..Response.Write SqlBuscaUsuario 
    '..Response.End
    
'PL-SQL * T-SQL  
SQLBUSCAUSUARIO = TransformPLSQLToTSQL (SQLBUSCAUSUARIO) 
    rstQryUtente.Open SqlBuscaUsuario, cc 
    
    '..Response.Write rstQryUtente("Login").Value
    '..Response.End
    '..and len(rstQryUtente("Login").Value) > 0 
    
    'No Existe el Usuario
    If rstQryUtente.EOF then
        'Si no existe el usuario en el portal, busco los datos en seguridad y lo doy de alta
		Status = 1	' simulo que hizo el alta en seguridad
		
		'..Response.Write " NO EXISTE  <br>"		' FABIO
		'..REsponse.End
		    
		rstQryUtente.Close
		Set rstQryUtente = nothing
		
		Set cmdUsuarioBusca = Server.CreateObject("ADODB.Command")
		With cmdUsuarioBusca
		    .ActiveConnection = gConnSeguridad
		    .CommandTimeout = 1200
		    .CommandText = "qryUsuarioDatos"
		    .CommandType = 4
		    	
		    .Parameters("@UserName").Value = Session("ck_login")
'PL-SQL * T-SQL  
RECORDCOUNT = TransformPLSQLToTSQL (RECORDCOUNT) 
		    Set rstQryUtente = .Execute(RecordCount)
		End With
		
		UsuarioId = rstQryUtente("UsuarioId").Value

        Session("ck_cognome") = rstQryUtente("Apellido").Value
        '..Session("ck_cognome") = replace(Session("ck_cognome"),"'","''")

        Session("ck_nome") = rstQryUtente("Nombre").Value
        '..Session("ck_nome") = replace(Session("ck_nome"),"'","''")

        Session("ck_email") = rstQryUtente("Direccion").Value
        
        Session("ck_login") = rstQryUtente("UserName").Value
        '..Session("ck_login") = Session("ck_cognome")  
        
        Session("ck_password") = rstQryUtente("Password").Value
	
	    rstQryUtente.Close
	    Set rstQryUtente = nothing
	    
        
        '..Response.Write SqlBuscaUsuario & rstQryUtente.RecordCount &  "<br>"		' FABIO
                
    'Existe el Usuario    
    Else
		Status = 1000
        Mensaje = "El c�digo de usuario ya existe en el sistema"
     End If
End If

'..Response.Write Status  & "<br>" 
'..Response.Write UsuarioId  & "<br>" & Session("ck_cognome")  & "<br>" & Session("ck_nome")  & "<br>" & Session("ck_email")  & "<br>" & Session("ck_login")  & "<br>" & Session("ck_password")
'..REsponse.End
'..Status =1		' Fabio

If Status = 1 then
	'------------------------------------------------------------------------------------------------------
	' Dio de alta bien en seguridad
	' Va a dar el alta en el portal 
	'------------------------------------------------------------------------------------------------------
	'REsponse.Write "Entro Alta en Portal" & "<br>"  'Hern�n
	'REsponse.End
	
	'Meto fija la fecha de caducacion de Usuario
	dt_pwd = "31/12/2099"
	
        Set rstInsUtente = server.CreateObject("ADODB.RECORDSET")
  		sql = " INSERT INTO UTENTE (IDUTENTE, NOME, COGNOME, EMAIL, "
  		
  		'if uorg <> "*" then 
	  		sql = sql & "ID_UORG, "
  		'end if 
  		
  		sql = sql & "IDGRUPPO, LOGIN, CREATOR, PROGETTO, IND_ABIL, dt_iscrizione,"
  		sql = sql & "dt_pwd,"
  		sql = sql & "DT_TMST"
        
        sql = sql & ") VALUES ('" & UsuarioId & "', '"
        sql = sql & Session("ck_nome") & "', '"
        sql = sql & Session("ck_cognome") & "', '"
        sql = sql & Session("ck_email") & "', '"
        sql = sql & Session("cmbUorg1") & "', '"
        sql = sql & Session("ck_idgruppo") & "', '"
        sql = sql & Session("ck_login") & "', "
        sql = sql & Session("ck_idOficina") & ", '"
        sql = sql & Session("ck_progetto") & "', '"
        sql = sql & Session("ck_ind_abil") & "', "
        sql = sql & ConvDateToDbs(Session("ck_dt_iscrizione")) & ", "
        sql = sql & ConvDateToDbs(dt_pwd) & ","
        sql = sql & ConvDateToDbs(Session("ck_dt_iscrizione")) & ")"
        
        sql = UCase(sql)
        
        'Response.Write sql		
        'Response.End
        
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
        Set rstInsUtente = CC.Execute(sql)
        
		' borra variables de session        
        Session("ck_nome") = ""
        Session("ck_cognome") = ""
        Session("ck_email") = ""
        Session("cmbUorg1") = ""
        Session("ck_idgruppo") = 0
        Session("ck_grupoid") = 0
        Session("ck_login") = ""
        Session("ck_idOficina") = ""
        Session("ck_dt_iscrizione") = ""
        Session("ck_progetto") = ""
        Session("ck_ind_abil") = ""
        Session("ck_IdTipoDoc") = ""
        Session("ck_password") = ""
        Session("ck_IdSexo") = ""
        Session("ck_Ndoc") = ""
        
        Session("Msg_Error")="Usuario creado correctamente"
	    Response.Redirect "pag2_create.asp"
	    
	    Set rstInsUtente = Nothing
  Else
        Session("ck_nome") = ""
        Session("ck_cognome") = ""
        Session("ck_email") = ""
        Session("cmbUorg1") = ""
        Session("ck_idgruppo") = 0
        Session("ck_grupoid") = 0
        Session("ck_idOficina") = ""
        Session("ck_login") = ""
        Session("ck_dt_iscrizione") = ""
        Session("ck_progetto") = ""
        Session("ck_ind_abil") = ""
        Session("ck_IdTipoDoc") = ""
        Session("ck_password") = ""
        Session("ck_IdSexo") = ""
        Session("ck_Ndoc") = ""
	'-------------------------------------------------------------------------------------
	' Mensaje al usuario ???
	'-------------------------------------------------------------------------------------
	%>
	<br>
	<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
		<tr height="18">
			<td class="sfondomenu" width="67%" height="18"><span class="tbltext0"><b>&nbsp;Configuraci�n del Sistema - Alta de Usuario</b></span></td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
			<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">(*) </td>
		</tr>
		<tr>
			<td class="sfondocomm" width="57%" colspan="3">
			<br><p>
			El proceso de alta de usuario no se completo exitosamente ver errores mas abajo.<br>             
					
			<a href="Javascript:Show_Help('/Pgm/help/Utenti/Modificautente/pag2_modute/')" name onmouseover="javascript:status='' ; return true">
			<img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></p>
			</td>
		</tr>
		<tr height="2">
			<td class="sfondocomm" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
		</tr>
	</table>
	<br>
	<table border="0" cellpadding="2" cellspacing="2" width="500">
		<tr height="30">
		   <td height="25" class="tbltext1" align="left">&nbsp;<b>Status:</b></td>
		   <td height="25" class="tbltext1" align="left">&nbsp;<b><%=Status%></b></td>
		</tr>
		<tr height="30">
		   <td height="25" class="tbltext1" align="left">&nbsp;<b>Mensaje:</b></td>
		   <td height="25" class="tbltext1" align="left">&nbsp;<b><%=Mensaje%></b></td>
		</tr>
<%
end if
%>

<br>
<table border="0" cellpadding="2" cellspacing="1" width="500">
	<tr>
	   <td width="420"></td>	
	   <td height="25" class="sfondocomm" align="center"><b>Paso 2/2</b></td>
	</tr>
</table>
</form>
<!-- #include virtual="/include/CloseConn.asp" -->
<!-- #include virtual="/strutt_coda2.asp" -->
