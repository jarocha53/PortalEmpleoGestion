<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include Virtual="/strutt_testa2.asp"-->
<!-- #include virtual="/util/portallib.asp" -->
<!-- #include virtual="/include/OpenConn.asp" -->
<!-- #include virtual="/include/ControlDateVB.asp" -->
<!-- #include virtual="/util/dbutil.asp" -->
<%
If Not ValidateService(Session("IdUtente"),"Gestione Utenti",cc) Then 
	Response.Redirect "/util/error_login.asp"
End If

Dim sDesLogin
Dim iGrupApp
Dim iVarPres
 
iGrupApp= Request.Form("idgruppo")

If CLng(iGrupApp) <> CLng(Session("ck_idgruppo")) Then 
	Set rstUteFun = Server.CreateObject("ADODB.RECORDSET")
		SQLUtente = "DELETE FROM utefun WHERE idutente='" & Session("ck_idpersona") & "'"
'PL-SQL * T-SQL  
SQLUTENTE = TransformPLSQLToTSQL (SQLUTENTE) 
		rstUteFun.open SQLUtente,CC,1,3	  
End If 
    
'Campi da modificare
' la replace dell'apice per nome e cognome e' del 15/04/05 (problemi per OPLA)
	 
Session("ck_cognome") = Request.Form("cognome")
Session("ck_cognome") = replace(Session("ck_cognome"),"'","''")

Session("ck_nome") = Request.Form("nome")
Session("ck_nome") = replace(Session("ck_nome"),"'","''")

Session("ck_email") = Request.Form("email")
Session("ck_IdTipoDoc") = Request.Form("IdTipoDoc")
Session("ck_NDoc") = Request.Form("NDoc")
Session("ck_IdSexo") = Request.Form("IdSexo")
Session("ck_idoficina") = Request.Form("IdOficina")
Session("ck_login") = Request.Form("login")
VecchiaPWD = Request.Form("VecchiaPWD")
Session("ck_password") = Request.Form("password")
Session("ck_idgruppo") = Request.Form("idgruppo")
Session("ck_creator") = Request.Form("creator")
Session("cmbUorg1") = Request.Form("cmbUorg")
DataIscr = ConvStringToDate(Request.Form("dtiscrizione"))
Session("ck_dt_iscrizione") = DataIscr

Session("ck_GrupoId") = Request.Form("grupoId")
Session("ck_actor") = Request.Form("actor")
NivelId = request("nivelid")

If Request.Form("dtaccesso") <> "" Then
	DataAccesso = ConvStringToDate(Request.Form("dtaccesso"))
	Session("ck_dt_accesso") = DataAccesso
Else
   Session("ck_dt_accesso") = ""
End if

Session("ck_progetto") = Request.Form("progetto")
Session("ck_ind_abil") = Request.Form("indabil")

Dim dt_pwd
dt_pwd = "31/12/2099"

SQLUtente = "UPDATE utente SET "
SQLUtente = SQLUtente & "cognome='" & Session("ck_cognome") & "', "
SQLUtente = SQLUtente & "nome='" & Session("ck_nome") & "', "
SQLUtente = SQLUtente & "email='" & Session("ck_email") & "', "
SQLUtente = SQLUtente & "login='" & Session("ck_login") & "', "

SQLUtente = SQLUtente & "idgruppo='" & Session("ck_idgruppo") & "', "
SQLUtente = SQLUtente & "creator='" & Session("ck_idoficina") & "', "
SQLUtente = SQLUtente & "id_uorg='" & Session("cmbUorg1") & "', "
If Session("ck_dt_iscrizione") <> "" Then
	SQLUtente = SQLUtente & "dt_iscrizione=" & ConvDateToDbs(Session("ck_dt_iscrizione")) & ", "
End if	
If Session("ck_dt_accesso") <> "" Then
	SQLUtente = SQLUtente & "dt_accesso=" & ConvDateToDbs(Session("ck_dt_accesso")) & ", "
End If
SQLUtente = SQLUtente & "progetto='" & Session("ck_progetto") & "', "
SQLUtente = SQLUtente & "ind_abil='" & Session("ck_ind_abil") & "', "
SQLUtente = SQLUtente & "dt_pwd=" & ConvDateToDbs(dt_pwd) & ", "

SQLUtente = Mid(SQLUtente, 1, Len(SQLUtente) - 2)
SQLUtente = SQLUtente & " WHERE idutente=" & Session("ck_idpersona")     
SQLUtente = UCase(SQLUtente)

'Response.Write SQLUtente
'Response.End

'PL-SQL * T-SQL  
SQLUTENTE = TransformPLSQLToTSQL (SQLUTENTE) 
Set rstModUtente = CC.Execute(SQLUtente)


'---------------------------------------------------------
'	Busco el Municipio de la oficina
'---------------------------------------------------------
    Set rstQryOficinaMuni = server.CreateObject("ADODB.RECORDSET")
    SqlBuscaMuni = "Select Comune From SEDE_IMPRESA Where ID_SEDE =" & Session("ck_idOficina")
    SqlBuscaMuni = UCase(SqlBuscaMuni)
'PL-SQL * T-SQL  
SQLBUSCAMUNI = TransformPLSQLToTSQL (SQLBUSCAMUNI) 
	rstQryOficinaMuni.Open SqlBuscaMuni, cc 
	
	If not rstQryOficinaMuni.EOF then
		sMunicipio = rstQryOficinaMuni("Comune").Value
		'Response.Write sMunicipio
		'Response.Write Session("ck_idOficina")
		'Response.End
	End If
'-------------------------------------------------------------------------------------
' Modifico los datos de usuario en Seguridad
'-------------------------------------------------------------------------------------
'---------------------------------------------------------
'	Doy de alta el suario en la base de Seguridad
'---------------------------------------------------------
Dim cmdUsuarioUpdate, status, mensaje, UsuarioId

'Response.Write request("GrupoId") 

'Response.Write Session("IdUtente")
'Response.End 

Set cmdUsuario = Server.CreateObject("ADODB.Command")
With cmdUsuario
    .ActiveConnection = gConnSeguridad
	.CommandTimeout = 1200
	.CommandText = "SpUsuarioActualizaDatos"
	.CommandType = 4
	
	
	
	'Parametros de Input Obligatorios
	.Parameters("@UsuarioId").Value = Session("ck_idpersona")	
	.Parameters("@UserName").Value = Session("ck_login")
	.Parameters("@UsuarioCreador").Value = Session("IdUtente")
	'Parametros de Input Opcionales
	.Parameters("@Nombre").Value = Session("ck_nome")
	.Parameters("@Apellido").Value = Session("ck_cognome")
    .Parameters("@Password").Value = Session("ck_password")
	.Parameters("@TipoDocId").Value = Session("ck_IdTipoDoc")
	.Parameters("@DocNumero").Value = Session("ck_Ndoc")
	.Parameters("@SexoId").Value = Session("ck_IdSexo")
	.Parameters("@Habilitado").Value = Session("ck_ind_abil")
	.Parameters("@MunicipioId").Value = sMunicipio
	.Parameters("@GrupoId").Value = request("GrupoId") 'Session("ck_GrupoId")
	.Parameters("@Actor").Value = Session("ck_actor")
	.Parameters("@NivelId").Value = NivelId
 
		'--------------------------------------------
		' Carga parametros
		'--------------------------------------------
		'For Each Parameter In .Parameters
		'	response.write Parameter.Name & "=" & Parameter.value & "<BR>"
		'Next	
		'Response.End
	'Response.Write Session("ck_nome") & "<BR>"
	'Response.Write Session("ck_cognome") & "<BR>"
	'Response.Write Session("ck_login") & "<BR>"
	'Response.Write Session("ck_password") & "<BR>"
	'Response.Write Session("ck_Ndoc") & "<BR>"
	'Response.End
	
'PL-SQL * T-SQL  
	.Execute()
	
	'Parametros de Output
	'Session("ck_idpersona") 
	Status = .Parameters("@Status").value
	Mensaje = .Parameters("@Mensaje").value
'	Response.End
End With    


'-------------------------------------------------------------------------------------
'
'
if Status = 1 then
 %>
	
<table border="0" cellspacing="1" cellpadding="1" width="100%">
	<tr height="15"> 
		<td align="center" class="tbltext3"></td>
	</tr>
	<tr height="15"> 
		<td align="middle" class="tbltext3">
			<b>Modificación correctamente efectuada</b>
		</td>
	</tr>
	<tr height="15"> 
		<td align="center" class="tbltext3"></td>
	</tr>
	
</table>

</div>

<%Else%>

<table border="0" cellspacing="1" cellpadding="1" width="100%">
	<tr height="15"> 
		<td align="center" class="tbltext3"></td>
	</tr>
	<tr height="15"> 
		<td align="middle" class="tbltext3">
			<b>A causa de inconvenientes técnicos la contraseña no ha sido cambiada</b>
			<p><b>Contactar al Grupo de Asistencia<br>
	  		<a HREF="mailto:soportesistemas@trabajo.gov.ar">
            soportesistemas@trabajo.gov.ar 
			<%=Mensaje%></a></b></p>
		</td>
	</tr>
	<tr height="15"> 
		<td align="center" class="tbltext3"></td>
	</tr>
	
</table>

</div>

<%end if
'-------------------------------------------------------------------------------------
%>

<!--#include virtual="/include/CloseConn.asp"-->		

