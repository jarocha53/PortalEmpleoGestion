<!-- #include virtual="/strutt_testa2.asp" -->
<!-- #include virtual="/util/portallib.asp" -->
<!-- #include virtual="/include/OpenConn.asp" -->
<%
If Not ValidateService(Session("IdUtente"),"Gestione Utenti",cc) Then 
	Response.Redirect "/util/error_login.asp"
End If
%>
<!-- #include virtual="/include/CloseConn.asp" -->

<script language="javascript">
<!--#include Virtual = "/Include/help.inc"-->

<!--
	function sel_end(){
		if (confirm("Cancellare Utente ?")){
		  <%Session("ck_delute")="Del"%>;
		  return true;
		}  
		   
		return false;
	}
//-->
</script>
<br>
<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
	<tr height="18">
		<td class="sfondomenu" width="67%" height="18"><span class="tbltext0"><b>&nbsp;AMMINISTRAZIONE PORTALE - Cancellazione Utente</b></span></td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
		<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
	</tr>
	<tr>
		<td class="sfondocomm" width="57%" colspan="3">
		<br><p>Pagina riepilogativa delle operazioni da effettuare premendo il tasto &quot;Invia&quot; </p> 
		</td>
	</tr>
	
	<tr height="17">
	    <td class="sfondocomm" width="57%" colspan="3">
		   <a href="Javascript:Show_Help('/Pgm/help/Utenti/CanceUtente/Pag4')" name onmouseover="javascript:status='' ; return true">
		     <img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></td>
	</tr>
	<tr height="17">
		<td class="sfondocomm" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
	</tr>
</table>
<br><br>
<form name="utente" action="pag4fun.asp" method="GET" onsubmit="return sel_end()">

<table border="0" cellpadding="2" cellspacing="0" width="90%">
	<tr>
		<td></td>
		<td class="tbltext1">Utente da eliminare:  <%=Session("ck_cognome")%> &nbsp; <%=Session("ck_nome")%></td>
	</tr>
	<tr>
		<td></td>
		<td class="tbltext1">
			<% If Session("ck_delfun") <> "" Then %>
				Cancellare funzioni associate all'utente : <img src="<%=Session("Progetto")%>/images/visto.gif" border="0">
			<% End if %>		
		</td>
	</tr>
	<tr>
		<td colspan="2" align="center"><br>
			<a href="pag3.asp"><img name="oi" border="0" alt="Indietro" src="<%=Session("Progetto")%>/images/indietro.gif"></a>
			<input type="image" alt="Invia" src="<%=Session("Progetto")%>/images/conferma.gif" name="conferma">
	</td>
	</tr>
</table>
</form>
<br>
<table border="0" cellpadding="2" cellspacing="1" width="500">
	<tr>
	   <td width="420"></td>	
	   <td height="25" class="sfondocomm" align="center"><b>Step 5/5</b></td>
	</tr>
</table>
<!-- #include virtual="/strutt_coda2.asp" -->

