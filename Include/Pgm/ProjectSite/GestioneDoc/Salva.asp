<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#INCLUDE FILE="Utils.asp" -->
<!--#include virtual="/include/openconn.asp"-->

<%

'#### Upload file
Set objUpload = Server.CreateObject("Dundas.Upload.2")
	
	'#### Impostazioni
	if sTipoDoc = "1" then
		objUpload.UseUniqueNames = true
		objUpload.ProgressID = request("ProgressID")
	else
		objUpload.UseUniqueNames = false
	end if
	
	objUpload.MaxFileCount = 1
	objUpload.UseVirtualDir = true
	'#### Salvataggio
	
	
	

	objUpload.Save DocsPath
	
	if objUpload.Files.Count < 1 then
		Response.Redirect "InsDoc.asp?Err=1"
	end if
	
	if objUpload.Files(0).Size <= 0 then
		objUpload.files(0).AllowAccess "Everyone", &H10000000
		objUpload.files(0).Delete 
		Response.Redirect "InsDoc.asp?Err=2"
	end if

	'#### Dati del Form	
	Oggetto      = cleanXsql(objUpload.Form("Oggetto"))
	Testo        = cleanXsql(objUpload.Form("Testo"))

	FileUploadedName = objUpload.GetFileName ( objUpload.Files(0).Path )

	if sTipoDoc = "0" then
'		FileUploadedName = Replace(FileUploadedName,"'","''")
		sNewFileName = Session("idutente") & "_" & mid(Session("Progetto"),2) &_
			 "_" & Oggetto & "_" & Session("Cognome") & " " &_
			 Session("Nome") &"_" & FileUploadedName
		objUpload.files(0).Move DocsPath & "\" & sNewFileName,false
	end if

	if objUpload.Form("ElencoIdUtente") <> "" then ElencoIdUtente = split(left(objUpload.Form("ElencoIdUtente"),len(objUpload.Form("ElencoIdUtente"))-2),";")
	if objUpload.Form("ElencoGruppi") <> "" then ElencoGruppi = split(objUpload.Form("ElencoGruppi"),";")
	
Set objUpload = Nothing
'#### E N D Upload file

'############################################
' EPILI 07/11/2002 
' Se il documento � di tipo privato, deve 
' registrare l'occorrenza nel DB, altimenti 
' solo l'Upload del documento.
'############################################

if sTipoDoc = "1" then
	
'#### Salvataggio in DB
	'Set conn = Server.CreateObject("ADODB.Connection")
	'Conn.Open strconn

		Sql = ""
		Sql = Sql & "SELECT DISTINCT UTENTE.IDUTENTE, UTENTE.EMAIL "
		Sql = Sql & "FROM UTENTE, GRUPPO "
		Sql = Sql & "WHERE "
		Sql = Sql & "UTENTE.IDGRUPPO = GRUPPO.IDGRUPPO AND ( "
		if isArray(ElencoGruppi) then
			Sql = Sql & "GRUPPO.IDGRUPPO IN ("
			for I = lbound(ElencoGruppi) to Ubound(ElencoGruppi)-2
				Sql = Sql & ElencoGruppi(I) & ","
			next
			Sql = Sql & ElencoGruppi(Ubound(ElencoGruppi)-1)
			Sql = Sql & ") "
		else
			Sql = Sql & " (1 = 0) "
		end if
		if isArray(ElencoIdUtente) then
			Sql = Sql & "OR UTENTE.IDUTENTE IN ("
			for I = lbound(ElencoIdUtente) to Ubound(ElencoIdUtente)-1
				Sql = Sql & ElencoIdUtente(I) & ","
			next
			Sql = Sql & ElencoIdUtente(Ubound(ElencoIdUtente))
			Sql = Sql & ") "
		end if
		Sql = Sql & ") "
		
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
		Set RsUsr = cc.Execute (Sql)
		
		if not RsUsr.eof then
			Utenti = RsUsr.getrows
		end if
		

	SqlIns = ""
	SqlIns = SqlIns & "INSERT INTO PUBBLICAZIONI "
	SqlIns = SqlIns & "(OGGETTO, TESTO, PATH_ALLEGATO, PROPRIETARIO, DT_TMST) "
	SqlIns = SqlIns & "VALUES "
	SqlIns = SqlIns & "('" & Oggetto & "','" & Testo & "', "
	SqlIns = SqlIns & "'" & Replace(FileUploadedName,"'","''") & "', "
	SqlIns = SqlIns & IDP & ", sysdate)"
	FileUploadedName = Replace(FileUploadedName,"''","'")

	cc.BeginTrans 
'PL-SQL * T-SQL  
SQLINS = TransformPLSQLToTSQL (SQLINS) 
		cc.Execute SqlIns
'PL-SQL * T-SQL  
		Set Rs = cc.Execute ("SELECT Max(ID_PUBBLICAZIONE) FROM PUBBLICAZIONI WHERE PROPRIETARIO=" & IDP)
		ID_PUBBLICAZIONE = Rs(0)
		Set Rs = Nothing
		Sql = ""
		Sql = Sql & "INSERT INTO PERS_PUBBLICAZIONI (ID_PUBBLICAZIONE, ID_PERSONA) "
		Sql = Sql & "VALUES (" & ID_PUBBLICAZIONE & ", " & IDP & ")"
		
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
		cc.Execute Sql
		
	cc.CommitTrans 

	if isArray(Utenti) then
		for I = lbound(Utenti, 2) to Ubound(Utenti, 2)
			if clng(0 & Utenti(0,I)) <> IDP then
				Sql = ""
				Sql = Sql & "INSERT INTO PERS_PUBBLICAZIONI (ID_PUBBLICAZIONE, ID_PERSONA) "
				Sql = Sql & "VALUES (" & ID_PUBBLICAZIONE & ", " & Utenti(0,I) & ")"
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
				cc.Execute Sql
			end if
		next
	End if

	'set Conn = Nothing
end if
if sTipoDoc = "1" then
	Response.Redirect "default.asp"
else
	Response.Redirect "default2.asp"
end if
%>
