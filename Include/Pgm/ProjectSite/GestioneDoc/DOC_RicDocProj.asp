<!-- #include Virtual="/strutt_testa2.asp" -->
<!-- #include virtual="/include/OpenConn.asp" -->
<!-- #include virtual="/include/DecCodProj.asp" -->
<!-- #include virtual="/include/ControlDateVB.asp" -->
<script LANGUAGE="JavaScript">
	<!--#include Virtual = "/Include/ControlNum.inc"-->
	<!--#Include Virtual = "/Include/ControlString.inc" -->
	<!--#include virtual = "/Include/help.inc"-->
</script>
<script LANGUAGE="JavaScript">
	function ControllaDati() {
		// Controllo obbligatorietà della combo.
			if (frmDati.cmbProj.value == "") {
				alert("Selezionare il Progetto")
				frmDati.cmbProj.focus();
			return false;
		}
		// Controllo obbligatorietà dell'anno.
			if (frmDati.txtannorif.value == "") {
				alert("Indicare l'anno riferimento")
				frmDati.txtannorif.focus();
			return false;
		}
		// Controlli formali sull'anno.
		if (!frmDati.txtannorif.value == "") 
			{
				if (!IsNum(frmDati.txtannorif.value))
				{
					alert("Il campo Anno deve essere numerico")
					frmDati.txtannorif.focus();
					return false;
				}
				if (frmDati.txtannorif.value < 1900)
				{
					alert("Il campo Anno deve essere superiore al 1900")
					frmDati.txtannorif.focus();					
					return false;
				}
				if (frmDati.txtannorif.value  > <%=year(Date)%>)
					{
					alert("Il campo Anno non può essere superiore all'anno corrente")
					frmDati.txtannorif.focus(); 
					return false;	
					}	
			}		
					
		return true;
	}
</script>	
<table cellpadding="0" cellspacing="0" width="488" border="0">
	   <tr height="18">
			<td class="sfondomenu" height="18" width="45%">
			<span class="tbltext0"><b>&nbsp;SELEZIONE DEL PROGETTO</b></span></td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
			<td valign="middle" align="right" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1"></td>
	   </tr>
	   <tr height="2">
			<td width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif">	</td>     
		</tr>
	   <tr width="371" class="SFONDOCOMM">
			<td colspan="3">QQQ
				<a href="Javascript:Show_Help('/Pgm/Help/ProjectSite/GestioneDoc/DOC_RicDocProj')" name="prov3" onmouseover="javascript:window.status='' ; return true"><img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
			</td>
		</tr>
		<tr height="2">
			<td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>
	<br>
	<form name="frmDati" method="post" action="DOC_LisDocProj.asp" onsubmit="return ControllaDati()">	
		<table cellpadding="0" cellspacing="0" width="488" border="0">
			<tr>
				<td width="200" align="left"><b class="tbltext1">Progetto:* </b></td>
				<td align="left">
<%
		sProgetto="|cmbProj|ORDER BY COD_PROG"

		CreateComboProj(sProgetto)
	
%>
				</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td width="200" class="tbltext1" align="left"><b>Anno di Riferimento*</b>&nbsp;&nbsp;(aaaa)</td>	
				<td align="left">
					<input maxlength="4" type="text" size="10" name="txtannorif">	
				</td>
			</tr>
		</table>
		<br><br>
		<table cellpadding="0" cellspacing="0" width="488" border="0">
		<tr>
			<td nowrap align="center"><input type="image" name="Conferma" src="<%=Session("progetto")%>/images/conferma.gif"></td value="Conferma"><td>
		</tr>
	</table>
	
	</form>		

<!-- #include Virtual="/include/CloseConn.asp" -->
<!-- #include Virtual="/strutt_coda2.asp" -->


