<!-- #include Virtual="/strutt_testa2.asp" -->
<!-- #include file="Utils.asp" -->

<%
dim objProgress
dim ProgressID

set objProgress = server.CreateObject("Dundas.UploadProgress")
ProgressID = objProgress.GetNewProgressID
if IsEmpty(ProgressID) then
	Response.redirect "Default.asp"
end if
sTipo = Request.Form("tipo")
%>
<html>
<head>
	<title>Form Upload Documentos</title>
<script language="Javascript">

<!-- #include Virtual="/include/ControlString.inc" -->

var nTipoDocVal;
nTipoDocVal = 1;

function apriRub(URL)
{
	URL += "?VISMENU=NO"										//Se la modalit� � popUp non deve comparire il menu
	URL += "&Mod_Fun=3"											//Questo Parametro Indica il modo di unzionamento della popUp di ricerca utenti
	URL += "&Nome_ListaIDUTENTE=Documenti.ElencoIdUtente"		//Questo parametro indica il nome del campo nel quale inserire gli IDUTENTE
	URL += "&Nome_ListaNominativo=Documenti.ElencoPersone"		//Questo Parametro indica il nome del campo nel quale inserire i Nominativi delle persone
	URL += "&Nome_ListaLogin=niente"							//Questo Parametro indica il nome del campo nel quale inserire le login degli utenti
	
	//window.open (URL,"Rubrica","Status=yes,toolbar=no,width=525,height=390,top=20,left=100")
	window.open (URL,"Rubrica","Status=no,toolbar=no,width=525,height=390,top=20,left=100")
}

function apriSelGruppi(URL)
{
	URL += "?VISMENU=NO"										//Se la modalit� � popUp non deve comparire il menu
	URL += "&Nome_ListaIDGRUPPO=Documenti.ElencoGruppi"			//Questo parametro indica il nome del campo nel quale inserire gli IDGRUPPO
	URL += "&Nome_ListaDesgruppo=Documenti.ElencoPersone"		//Questo Parametro indica il nome del campo nel quale inserire Le Descrizioni del gruppo

	//window.open (URL,"Gruppi","Status=yes,toolbar=no,width=525,height=200,top=20,left=100")
	window.open (URL,"Gruppi","Status=no,toolbar=no,width=525,height=200,top=20,left=100")
	
}

function chkFrm(frm)
{	
	if (frm.Testo.value.length > 255)
	{
		alert("No es posible ingresar en Descripci�n mas de 255 caracteres " )
		return false
	}
	
	if (frm.Oggetto.value == "")
	{
		alert("Ingresar Asunto")
		frm.Oggetto.focus();
		return false
	}
	if (frm.Testo.value == "")
	{
		alert("Ingresar una breve descripci�n del documento en el campo Descripci�n")
		frm.Testo.focus();
		return false
	}
	if (frm.Path.value == "")
	{
		alert("Seleccionar el documento a compartir")
		frm.Path.focus();
		return false
	}
	
	if (!ValidateNameUploadFileIsValid(frm.Path.value)) {
		return false;
	}

	document.Documenti.action = document.Documenti.action + "&Tipo=" + nTipoDocVal

	ProgressID=<%=ProgressID%>;
	if (ProgressID != -1){	
		//Param = "SCROLLBARS=no,RESIZABLE=no, TOOLBAR=no,STATUS=yes,MENUBAR=no,WIDTH=400,HEIGHT=100";
		Param = "SCROLLBARS=no,RESIZABLE=no, TOOLBAR=no,STATUS=yes,MENUBAR=no,WIDTH=400,HEIGHT=100";
		Param += ",TOP=" + String(window.screen.Height/2 - 50);
		Param += ",LEFT=" + String(window.screen.Width/2 - 200);
		window.open("ProgressBar.asp?ProgressID=<%=ProgressID%>", null, Param);
	}	
}

	function Pubblico(nIndice){
	

		nTipoDocVal =  nIndice		
//		nTipoDocVal = Documenti.TipoDoc[nIndice].value;

/*		if (nTipoDocVal == "0") {
			Documenti.btCper.disabled			= true;
			Documenti.btCgrp.disabled			= true;
			Documenti.ElencoIdUtente.disabled	= true;			
			Documenti.ElencoGruppi.disabled		= true;	
			Documenti.ElencoPersone.disabled	= true;
		}else {
			Documenti.btCper.disabled			= false;
			Documenti.btCgrp.disabled			= false;
			Documenti.ElencoIdUtente.disabled	= false;			
			Documenti.ElencoGruppi.disabled		= false;	
			Documenti.ElencoPersone.disabled	= false;
		
		}		
*/		
	}	


</script>
</head>
<center>
<%if sTipo = "0" then%>
<br>
<table border="0" width="500" CELLPADDING="0" cellspacing="0">
	<tr>
		<td align="left" class="sfondomenu" width="199">
			<span class="tbltext0">
			<b>&nbsp;PANEL PUBLICO</b></span>
		</td>
		<td width="25" valign="bottom" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">
			<img border="0" src="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
		<td width="30%" valign="middle" align="right" class="tbltext1" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
	</tr>
</table>
		
<table border="0" width="500" CELLPADDING="0" cellspacing="0">
    <tr>
		<td class="sfondocomm" align="left" class="tbltext1">
		A continuaci�n se muestran todos los documentos presentes en el area publica
		a disposici�n de todos los usuarios.<br>
		En esta area se podran distribuir y compartir documentos de cualquier tipo.
		</td>
		<td class="sfondocomm">
			<a href="Javascript:Show_Help('/Pgm/Help/ProjectSite/GestioneDoc/InsDoc_2/')">
			<img src="<%=Session("Progetto")%>/images/Help.gif" border="0" align="right" alt="Help">
		</td> 
    </tr>

	<tr height="2">
		<td class="sfondocomm" width="100%" colspan="2" background="<%=Session("Progetto")%>/images/separazione.gif">
		</td>
	</tr>
</table>
<%else%>

	<br>
	<table border="0" width="500" CELLPADDING="0" cellspacing="0">
		<tr>
			<td align="left" class="sfondomenu" width="199">
				<span class="tbltext0">
				<b>&nbsp;PANEL DE DOCUMENTOS</b></span>
			</td>
			<td width="25" valign="bottom" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">
				<img border="0" src="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
			<td width="30%" valign="middle" align="right" class="tbltext1" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
		</tr>
	</table>
			
	<table border="0" width="500" CELLPADDING="0" cellspacing="0">
	    <tr>
			<td class="sfondocomm" align="left" class="tbltext1">
			A continuaci�n se reportan todos los documentos disponibles para el operador.
			En esta area se puede distribuir y compartir documentos de distinto tipo.
			</td>
			<td class="sfondocomm">
				<a href="Javascript:Show_Help('/Pgm/Help/ProjectSite/GestioneDoc/InsDoc/')">
				<img src="<%=Session("Progetto")%>/images/Help.gif" border="0" align="right" alt="Help">
			</td> 
	    </tr>

		<tr height="2">
			<td class="sfondocomm" width="100%" colspan="2" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>
<%end if%>

	<br>
	<table border="0" width="500">
		<tr class="sfondocomm">
			<td valign="top" width="500" align="center" colspan="3"><b>Publicar un documento</b></td>
		</tr>
	</table>
	<br>
<% 
	if Int(request("Err")) > 0 then Response.Write "<b><font style='color:red'>!!! ATENCI�N !!!<br></font></b><font style='color: red' Class='tbltext1'><b>El archivo ingresado no existe</br></font>"
%>
	<br>
<input type="hidden" name="TipoDoc" value="<%=sTipo%>">
<form name="Documenti" id="Documenti" method="POST" action="Salva.asp?VISMENU=NO&amp;ProgressID=<%=ProgressID%>" ENCTYPE="multipart/form-data" Onsubmit="return chkFrm(this)">
	<table cellpadding="0" cellspacing="0" border="0" bgcolor="#FFFFFF">

		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td CLASS="tbltext1"><b>Asunto*</b></td>
			<td><input type="text" size="30" Name="Oggetto" CLASS="MyTextBox" maxlength="255"></td>
		</tr>
<%if sTipo = "1" then%>
		<tr>
			<td>&nbsp;</td>
			<td class="tbltext1" valign="bottom">&nbsp;&nbsp;
				Utilizar un maximo de
				<label name="NumCaratteri" id="NumCaratteri">255</label>
				caracteres.
			</td>
		</tr>
		<tr>
			<td CLASS="tbltext1"><b>Descripci�n*</b></td>
			<td align="left" valign="middle">
			<textarea onKeyup="JavaScript:CheckLenTextArea(Testo,NumCaratteri,255)" CLASS="MyTextBox" cols="47" rows="4" name="Testo"></textarea>
			</td>
		</tr>
<%else%>
	<input type="hidden" name="Testo" value="r">
<%end if%>
		<tr>
			<td CLASS="tbltext1"><b>Documento*</b> </td>
			<td><input CLASS="MyTextBox" TYPE="FILE" NAME="Path" SIZE="35">
			</td>
		</tr>
		<tr><td colspan="2">&nbsp;</td></tr>
<%if sTipo = "1" then%>
		<tr>
			<td CLASS="tbltext1"><b>Opciones</b></td>
			<td>
				<input Name="btCper" type="Button" Value="Agrega Persona" OnClick="apriRub(&quot;../Rubrica/PopUp_FormCerca.asp&quot;)" Class="My">
				<input Name="btCgrp" type="Button" Value="Agrega Grupo" OnClick="apriSelGruppi(&quot;../Rubrica/PopUp_SelGruppi.asp&quot;)" Class="My"><br>
			</td>
		</tr>

		<tr>
			<td CLASS="tbltext1"><b>Compartir con :&nbsp;</font></b></td>
			<td>
		          <textarea rows="4" readonly OnClick="apriRub(&quot;../Rubrica/PopUp_FormCerca.asp&quot;)" CLASS="MyTextBox" name="ElencoPersone" cols="50" maxlength="500"></textarea>
		          <input Type="hidden" Name="ElencoIdUtente">
		          <input Type="hidden" Name="ElencoGruppi">
			</td>
		</tr>
<%end if%>
	</table>
	<br>
		<table border="0" width="500" CELLPADDING="0" cellspacing="0">

		<tr>
		
			<td align="right" width="50%">
			<!--<input type="Button" value="Indietro" Class="My" OnClick="document.location = &quot;default.asp&quot;">-->
			<a href="JavaScript:Indietro.submit()" OnMouseOver="JavaScript:window.status=''; return true">
			<img src="<%=Session("Progetto")%>/images/Indietro.gif" border="0">
			</a>
			&nbsp;&nbsp;&nbsp;
			</td>
		
			<td align="left" width="50%">
			&nbsp;&nbsp;&nbsp;
			<!--<input type="submit" class="My" Value="Inserisci">-->
			<input type="image" src="<%=Session("Progetto")%>/images/Conferma.gif" alt="Condividi">
			</td>
		</tr>
		<!--<tr>		<td bgcolor="#3399CC" colspan="2"></td>		</tr>-->
	</table>
</form>
<%
if sTipo = 0 then
	sIndietro = "Default2.asp"	
else
	sIndietro = "Default.asp"	
end if
%>
<form method="post" action="<%=sIndietro%>" name="Indietro">
</form>
<!-- #include Virtual="/strutt_coda2.asp" -->
<script language="javascript">
	Pubblico(<%=sTipo%>);
</script>