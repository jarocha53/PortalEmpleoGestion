<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #INCLUDE FILE="Utils.asp" -->
<%

sub Calendario (M, Y)

set conn= server.createobject("adodb.connection")
'PL-SQL * T-SQL  
STRCONN = TransformPLSQLToTSQL (STRCONN) 
conn.open strconn

Sql = " SELECT substr(to_char(Data,'dd/mm/yyyy'),1,2) day "
Sql = Sql &  " From Agenda "
Sql = Sql &  " WHERE "
Sql = Sql &  " substr(to_char(Data,'dd/mm/yyyy'),4,2) = '" & M & "' AND "
Sql = Sql &  " substr(to_char(Data,'dd/mm/yyyy'),7,4) = '" & Y & "'"

'Sql = "SELECT Day(Data) From Agenda WHERE Month(Data) = " & M & " AND Year(Data) = " & Y
Set Rs = Server.CreateObject ("ADODB.Recordset")
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
Rs.Open Sql,Conn,3,3,1
if not Rs.eof then
	AllDate = Rs.GetString(,,"","~","")
else
	AllDate = "~"
end if
Rs.Close 
Set Rs = Nothing
Set conn = nothing

Cnorm = "#E4EBFC"
Cgior = ""
Cappu = "#E0D6A9"
Cfest = "#DEBCBD"


sfasa = weekday("1/" & monthname(M) & "/" & Y,2)


Response.Write "<table bordercolor='#578489' border=1 cellspacing=1 style='font-family: Verdana; font-size: 7pt'>"
Response.Write "<tr>"
for k = 0 to 6
	Response.Write "<td bgcolor='Navy'><font color='#FFFFFF'><b>" & DN(k) & "</b></font></td>"
next
Response.Write "</tr>"
Response.Write "<tr>"
for j = 1 to sfasa -1
	Response.Write "<td bgcolor='" & Cnorm & "'>&nbsp;</td>"
next
i = 1
col = sfasa-1
rig = 0
do
	ActDate = i & "/" & monthname(M) & "/" & Y
	if IsDate(ActDate) then
	SysData = Dateserial(Y,M,i)
		col = col + 1
		'### Definisco i colori
			bg = Cnorm
			if col > 5 then bg = Cfest
			if SysData = Date  then
				Cgior = "style='border: 2; border-style: ridge; border-color: #FF0000'"
			else
				Cgior = " "
			end if
		if instr(AllDate,i&"~") > 0 then
			bg = Cappu
			Response.Write "<td " & Cgior & " align=center bgcolor='" & bg & "'><a target='Dx' href='Impegni.asp?data=" & ActDate & "'>" & i & "</a></td>" '### Scrivo i giorni
		else
			Response.Write "<td " & Cgior & " align=center bgcolor='" & bg & "'>" & i & "</td>" '### Scrivo i giorni
		end if
		if weekday(ActDate) = 1 then
			Response.Write "</tr><tr>"
			col = 0
			rig = rig + 1
		end if
	end if
i = i + 1
loop until (i > 31) or not IsDate(ActDate)
for j = col+1 to 7
	Response.Write "<td "
	if j > 5 then 
		Response.write "bgcolor='" & Cfest & "' "
	else
		Response.write "bgcolor='" & Cnorm & "' "
	end if
	Response.Write ">&nbsp;</td>"
next
Response.Write "</tr>"

if rig < 5 then
Response.Write "<tr>"
	for k = 1 to 7 
		Response.Write "<td "
		if k > 5 then 
			Response.write "bgcolor='" & Cfest & "' "
		else
			Response.write "bgcolor='" & Cnorm & "' "
		end if
		Response.Write ">&nbsp;</td>"
	next
Response.Write "</tr>"
end if
Response.Write "</table>"
end sub
%>
<html>
<body bgcolor="#FFFFFF">
<center>
<table width=80% border=0>
<tr>
	<form name="Sposta" Action="Calendar.asp" method="POST">
	<td colspan="3" align="center">
	<%
	Mese = Request("Mese")
	If Mese = "" then Mese = Month(date)
	
	Response.Write "<select style='background-color: #E4EBFC; font-family: Verdana; font-size: 8pt; font-weight: bold; color: #000000' name='Mese' OnChange='this.form.submit()'>"
	for I = 1 to 12
		Response.Write "<option value='" & I & "' "
		if I = Int(Mese) then Response.Write "Selected"
		Response.Write " >" & mesi(I) & "</option>"
	next
	Response.Write "</select>"
	Anno = Request("Anno")
	If Anno = "" then Anno = Year(date)
	
	Response.Write "<select style='background-color: #E4EBFC; font-family: Verdana; font-size: 8pt; font-weight: bold; color: #000000' name='Anno' OnChange='this.form.submit()'>"
	for I = year(date)-10 to year(date)+10
		Response.Write "<option value='" & I & "' "
		if I = Int(Anno) then Response.Write "Selected"
		Response.Write " >" & I & "</option>"
	next
	Response.Write "</select>"
	%>
	</td>
	</form>
</tr>
<tr>
	<%
		If Mese = 1 then
			MesePre = 12
			AnnoPre = Anno -1
		else
			MesePre = Mese -1
			AnnoPre = Anno
		end if
		
		If Mese = 12 then
			MeseSuc = 1
			AnnoSuc = Anno +1
		else
			MeseSuc = Mese +1
			AnnoSuc = Anno
		end if
	%>
	<td align="left"><% Calendario MesePre , AnnoPre %></td>
	<td align="center"><% Calendario mese, Anno %></td>
	<td align="right"><% Calendario MeseSuc , AnnoSuc %></td>
</tr>
<tr>
<td colspan=3 align="center">
	<a href="InsAppuntamento.asp" target='Dx' >
		<img src="Images/Nuovo_appuntamento.gif" border=0>
	</a>
</td>
</tr>
</table>
</center>
</body>
</html>
