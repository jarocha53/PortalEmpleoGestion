<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include Virtual="/strutt_testa2.asp"-->
<!--#include virtual = "/include/SysFunction.asp"-->
<!--#include virtual = "/include/OpenConn.asp"-->
<!--#include virtual="/util/dbUtil.asp"-->
<!--#include virtual = "/include/ControlDateVB.asp"-->
<%
'--------------------'
Sub Inizio()
%>
	<table border="0" width="520" height="81" cellspacing="0" cellpadding="0">
		<tr>
			<td width="500" height="81" background="<%=Session("Progetto")%>/images/titoli/strumenti2b.gif" valign="bottom" align="right">
				<table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
					<tr>
						<td width="100%" valign="top" align="right">
							<b class="tbltext1a">Unita' Organizzativa</b>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<br>
	<table border="0" width="500" CELLPADDING="0" cellspacing="0">
		<tr class="tblcomm">
			<td align="left" class="sfondomenu">
				<span class="tbltext0"><b>&nbsp;GESTIONE UNITA' ORGANIZZATIVA</b></span>
			</td>
			<td width="25" valign="bottom" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">
			<img border="0" src="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
			<td width="140" valign="bottom" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">&nbsp;</td>
		</tr>
	</table>

	<table border="0" width="500" CELLPADDING="0" cellspacing="0">
	    <tr>
			<td class="sfondocomm" align="left" class="tbltext1">
				Conferma unit� organizzativa.
			</td>
	    </tr>
		<tr height="2">
			<td class="sfondocomm" width="100%" colspan="2" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
		</tr>
	</table>	
<%
End Sub
'--------------------'

dim sDescUO
dim vData
dim sSQL
dim rs
dim chiave
dim Errore

sDescUO = server.HTMLEncode(UCASE(TRIM(Request.Form("txtDescUO"))))
sDescUO = Replace(sDescUO, "'", "''")
vData = ""

'Controllo che non abbia gia inserito quella descrizione
sSQL=""
sSQL= "SELECT DESC_UORG FROM UNITA_ORGANIZZATIVA " & _
	  "WHERE DESC_UORG = '" & sDescUO & "'"
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
Set rs = CC.Execute(sSQL)

IF NOT rs.EOF THEN
	Inizio()
%>
	<br><br>
	<table border="0" cellspacing="1" cellpadding="1" width="500">
		<tr align="middle">
			<td class="tbltext3">
				Inserimento non effettuabile.<br><br>
				Descrizione Unit� Organizzativa gi� presente.
			</td>
		</tr>
	</table>
	<br>				
	<table border="0" cellpadding="0" cellspacing="1" width="500">
		<tr>
			<td align="middle" colspan="2" width="60%"><b>
				<a HREF="javascript:history.back()"><img SRC="<%=Session("Progetto")%>/images/indietro.gif" border="0">
			</td>
		</tr>
	</table>
	<!--#include Virtual="/strutt_Coda2.asp"-->
<%
ELSE

	Errore="0" 
	
	'Inserimento UNITA_ORGANIZZATIVA
	sSQL=""
	sSQL = "INSERT INTO UNITA_ORGANIZZATIVA " &_
		   "(DESC_UORG, DT_TMST) " &_
		   "VALUES " &_
		   "('" & sDescUO & "'," & convdatetodb(Now())& ")"
		   
	chiave=sDescUO
	Errore=Esegui(chiave,"UNITA_ORGANIZZATIVA",session("idutente"),"INS", sSQL, 0, vData)

	If Errore="0" then
%>
		<form name="frmIndietro" method="post" action="UNI_VisUnitOrg.asp">
			<input type="hidden" name="hdnDescUO" value="<%=sDescUO%>">
		</form>
		
			<script>
				alert("Inserimento correttamente effettuato");
				frmIndietro.submit()
			</script>
<%
	Else
		Inizio()
%>
		<br><br>
		<table border="0" cellspacing="1" cellpadding="1" width="500">
			<tr align="middle">
				<td class="tbltext3">
					Inserimento non effettuabile.
				</td>
			</tr>
			<tr align="middle">
				<td class="tbltext3">
					<%Response.Write(Errore)%> 
				</td>
			</tr>
		</table>
		<br>				
		<table border="0" cellpadding="0" cellspacing="1" width="500">
			<tr>
				<td align="middle" colspan="2" width="60%"><b>
				 <a HREF="javascript:history.back()"><img SRC="<%=Session("Progetto")%>/images/indietro.gif" border="0">
				</td>
			</tr>
		</table>
		<!--#include Virtual="/strutt_Coda2.asp"-->
<%
	End if
END IF
rs.Close
Set rs = nothing
%>
<!--#include Virtual = "/include/CloseConn.asp"-->	

