<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include Virtual = "/strutt_testa2.asp"-->
<!--#include Virtual = "/include/OpenConn.asp"-->
<!--#include Virtual = "/include/ControlDateVB.asp"-->
<!--#include Virtual="/util/dbUtil.asp"-->
<!--#include virtual="/util/portallib.asp"-->
<%
	if ValidateService(session("idutente"),"UNI_VISUNITORG", CC) <> "true" then 
		response.redirect "/util/error_login.asp"
	end if
%>

<script LANGUAGE="Javascript">
	<!--#include Virtual = "/Include/help.inc"-->
	<!--#include Virtual = "/include/ControlString.inc"-->

	function ControllaDati()
	{
		FrmModUO.txtDescUO.value=TRIM(FrmModUO.txtDescUO.value)
		if (FrmModUO.txtDescUO.value =='')
		{
			alert("La Descrizione Unit� Organizzativa � obbligatoria")
			FrmModUO.txtDescUO.focus()
			return false
		}
	}
</script>

<%
dim nIdUO
dim rsUO

nIdUO =Request.Form ("hdnID")
%>
<form name="frmIndietro" action="UNI_VisUnitOrg.asp" method="post">
	<input type="hidden" name="hdnIdUO" value="<%=nIdUO%>">			
</form>

<table border="0" width="520" height="81" cellspacing="0" cellpadding="0">
	<tr>
		<td width="500" height="81" background="<%=Session("Progetto")%>/images/titoli/strumenti2b.gif" valign="bottom" align="right">
			<table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
				<tr>
					<td width="100%" valign="top" align="right">
						<b class="tbltext1a">Unidad Funcional</b>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<br>
<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
	<tr height="18">
		<td class="sfondomenu" width="67%" height="18"><span class="tbltext0">
			<b>&nbsp;MODIFICAR UNIDAD FUNCIONAL</b></span>
		</td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
		<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">(*) campo obligatorio</td>
	</tr>
</table>
<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
	<tr>
		<td align="left" class="sfondocomm">
			En esta secci�n se podr� modificar la Unidad Funcional ya registrada. <br>
			Presione <b>Enviar</b> para guardar la modificaci�n. 
			<a href="Javascript:Show_Help('/Pgm/Help/UnitaOrga/UNI_ModUnitOrg')">
			<img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
		</td>
	</tr>
	<tr height="2">
		<td class="sfondocomm" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif">
		</td>
	</tr>
</table>
	
<%
sSQL = "SELECT ID_UORG, DESC_UORG, DT_TMST FROM UNITA_ORGANIZZATIVA" &_
		" WHERE ID_UORG=" & nIdUO
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
set rsUO = CC.Execute(sSQL)

if rsUO.EOF then
%>
	<br>
	<table border="0" cellspacing="2" cellpadding="1" width="500" align="center">
		<tr align="middle">
			<td class="tbltext3">
				<b>No fue posible acceder a la informaci�n buscada</b>
			</td>
		</tr>
	</table>
	<br>
	<table border="0" cellpadding="1" cellspacing="1">
		<tr>
			<td align="center">
				<a href="Javascript:frmIndietro.submit();"><img src="<%=Session("Progetto")%>/images/indietro.gif" border="0"></a>
		    </td>
		</tr>
	</table>
<%
else
%>
	<form method="post" name="FrmModUO" action="UNI_CnfModUnitOrg.asp" onsubmit="return ControllaDati(this)">
		<input type="hidden" name="txtIdUO" value="<%=nIdUO%>">
		<input type="hidden" name="txtTMST" value="<%=rsUO("DT_TMST")%>">
		
		<table border="0" cellpadding="0" cellspacing="1" width="500">
			<tr>
				<td align="middle" colspan="2" nowrap class="tbltext1">
					<p align="left">
						<strong>&nbsp;</strong>
						<strong>Identificador</strong>
						<strong>&nbsp;</strong>
					</p>
				</td>
				<td align="left" colspan="2" width="60%">
					<span class="textblack"><b><%=nIdUO%></b></span>
				</td>
			</tr>
			<tr>
				<td>
					&nbsp;
				</td>
			</tr>   
			<tr>
				<td align="middle" colspan="2" nowrap class="tbltext1">
					<p align="left">
						<strong>&nbsp;</strong>
						<strong>Descripci�n de la Unidad Funcional*</strong>
						<strong>&nbsp;</strong>
					</p>
				</td>
				<td align="left" colspan="2" width="60%">
					<span class="tbltext">
						<input class="textblacka" style="TEXT-TRANSFORM: uppercase" maxlength="50" type="text" name="txtDescUO" value="<%=UCase(rsUO("DESC_UORG"))%>" size="40">	
					</span>
				</td>
		    </tr>
		    <tr>
				<td>
					&nbsp;
				</td>
			</tr>
		</table>

		<table border="0" cellpadding="1" cellspacing="1">
			<tr>
				<td align="middle">
					<a href="Javascript:frmIndietro.submit();"><img src="<%=Session("Progetto")%>/images/indietro.gif" border="0">
					</a>
					<input type="image" src="<%=Session("Progetto")%>/images/conferma.gif" border="0" value="Modifica" id="image1" name="image1">
			    </td>
			</tr>
		</table>
	</form>
<%
end if
rsUO.Close
set rsUO = nothing
%>
<!--#include virtual = "/include/CloseConn.asp"-->
<!--#include Virtual = "/strutt_coda2.asp"-->
