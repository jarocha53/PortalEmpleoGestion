<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include Virtual="/strutt_testa2.asp"-->
<!--#include virtual = "/include/SysFunction.asp"-->
<!--#include virtual = "/include/OpenConn.asp"-->
<!--#include virtual="/util/dbUtil.asp"-->
<!--#include virtual = "/include/ControlDateVB.asp"-->
<%
'--------------------'
Sub Inizio()
%>
		<table border="0" width="520" height="81" cellspacing="0" cellpadding="0">
		<tr>
			<td width="500" height="81" background="<%=Session("Progetto")%>/images/titoli/strumenti2b.gif" valign="bottom" align="right">
				<table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
					<tr>
						<td width="100%" valign="top" align="right">
							<b class="tbltext1a">Unita' Organizzativa</b>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<br>
	<table cellpadding="0" cellspacing="0" width="500" border="0">
		<tr height="18">
			<td class="sfondomenu" height="18" width="60%">
				<span class="tbltext0">
					<b>DEFINIZIONE AMBITO DELL'UNIT� ORGANIZZATIVA</b>
				</span>
			</td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">
			</td>
			<td valign="middle" align="right" width="35%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1">
			</td>
		</tr>
		<tr width="371" class="SFONDOCOMM">
			<td colspan="3">
				Conferma Ambito dell'unit� organizzativa.
			</td>
		</tr>
		<tr height="2">
			<td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>
<%
End Sub
'--------------------'

dim ValUorg, ValAreaTer, ValBando
dim vData
dim sSQL
dim rs
dim chiave
dim Errore

ValUorg= Request.Form("CmbRicUorg2") 
ValAreaTer= Request.Form("cmbAreaterr") 
ValBando= Request.Form("cmbBando")
vData = ""
%>

<form name="frmIndietro" method="post" action="UNI_DefAmbito.asp">	
	<input type="hidden" name="CmbRicUorg" value="<%=ValUorg%>">
</form>	

<%
'Controllo che non abbia gia inserito quel record
if ValBando <> "" then
	sSQL= "SELECT ID_AREATERR, ID_BANDO, ID_UORG FROM AREA_BANDO " & _
		  "WHERE ID_AREATERR = " & ValAreaTer  &_
		  " AND ID_BANDO = " & ValBando 
else
	sSQL= "SELECT ID_AREATERR, ID_BANDO, ID_UORG FROM AREA_BANDO " & _
		  "WHERE ID_AREATERR = " & ValAreaTer  &_
		  " AND ID_BANDO is null" 
end if

'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
Set rs = CC.Execute(sSQL)

IF NOT rs.EOF THEN
	Inizio()
%>
	<br>
	<table border="0" cellspacing="1" cellpadding="1" width="500">
		<tr align="middle">
			<td class="tbltext3">
				Inserimento non effettuabile. <br>
				Record gi� presente in tabella o <br>
				associazione Area Territoriale - Bando gi� inserita.
			</td>
		</tr>
	</table>
	<br>				
	<table border="0" cellpadding="0" cellspacing="1" width="500">
		<tr>
			<td align="middle" colspan="2" width="60%"><b>
				<a HREF="javascript:frmIndietro.submit()"><img SRC="<%=Session("Progetto")%>/images/indietro.gif" border="0">
			</td>
		</tr>
	</table>
	<!--#include Virtual="/strutt_Coda2.asp"-->
<%
ELSE

	Errore="0" 
	
	'Inserimento AREA_BANDO
	if ValBando  = "" then
	   ValBando = "''"
	end if   
	
	sSQL = "INSERT INTO AREA_BANDO " &_
		   "(ID_AREATERR, ID_BANDO, ID_UORG, DT_TMST) " &_
		   "VALUES " &_
		   "(" & ValAreaTer & ", " & ValBando & ", " & ValUorg & ", " & convdatetodb(Now())& ")"	   
	chiave=""
	Errore=Esegui(chiave,"AREA_BANDO",session("idutente"),"INS", sSQL, 0, vData)

	If Errore="0" then
%>
		<script>
			alert("Inserimento correttamente effettuato");		
			frmIndietro.submit()
		</script>
			
<%
	Else
		Inizio()
%>	
		<br><br>
		<table border="0" cellspacing="1" cellpadding="1" width="500">
			<tr align="middle">
				<td class="tbltext3">
					Inserimento non effettuabile.
				</td>
			</tr>
			<tr align="middle">
				<td class="tbltext3">
					<%Response.Write(Errore)%> 
				</td>
			</tr>
		</table>
		<br>				
		<table border="0" cellpadding="0" cellspacing="1" width="500">
			<tr>
				<td align="middle" colspan="2" width="60%"><b>
				 <a HREF="javascript:frmIndietro.submit()"><img SRC="<%=Session("Progetto")%>/images/indietro.gif" border="0"></a>
				</td>
			</tr>
		</table>
		<!--#include Virtual="/strutt_Coda2.asp"-->
<%
	End if	
END IF
rs.Close
Set rs = nothing
%>
<!--#include Virtual = "/include/CloseConn.asp"-->	

