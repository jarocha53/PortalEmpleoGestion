<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include Virtual = "/include/openconn.asp"-->
<!--#include virtual = "/include/DecCod.asp"-->
<!--#include virtual = "/include/ControlDateVB.asp"-->
<!--#include virtual = "/util/portallib.asp"-->
<%
'if ValidateService(session("idutente"),"RIC_RICRICHIESTA",cc) <> "true" then 
'	response.redirect "/util/error_login.asp"
'end if
%>
<html>
<head>
<title>Certificación</title>
<link REL="STYLESHEET" TYPE="text/css" HREF="<%=session("progetto")%>/fogliostile.css">
<script LANGUAGE="Javascript">
<!--#include virtual = "/Include/help.inc"-->
</script>

<%
'********************************
'*********** MAIN ***************

'	if ValidateService(session("idutente"),"RIC_RICRICHIESTA",cc) <> "true" then 
'		response.redirect "/util/error_login.asp"
'	end if

Dim nIdPers,sTipoProf

'sTipoProf	=	Request.QueryString ("TipoProf")
nIdPers		=	CLng(Request.QueryString ("idPer"))

Inizio()
ImpostaPag()
Chiudi()

'********************************
'********************************
'**********************************************************************************************************************************************************************************
'**********************************************************************************************************************************************************************************
Sub Inizio()%>
	<table width="520px" border="0" cellspacing="0" cellpadding="0" height="70" align="center">
		<tr>
			<td width="500" background="<%=session("progetto")%>/images/titoli/servizi2g.gif" valign="bottom" align="right">
				<table border="0" background width="500" height="23" cellspacing="0" cellpadding="0">
					<tr>
						<td width="100%" valign="top" align="right"><b CLASS="tbltext1a">Certificación</b></td>
					</tr>
				</table>
			</td>
		</tr>
	</table> 
	<br>
	<table border="0" CELLPADDING="0" CELLSPACING="0" width="500" align="center">
		<tr height="18">
			<td class="sfondomenu" width="67%" height="18"><span class="tbltext0"><b>&nbsp;CERTIFICACION</b></span></td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
			<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
		</tr>
	</table>
	<!-- Commento -->
	<table border="0" CELLPADDING="0" CELLSPACING="0" width="500" align="center">
		<tr>
			<td align="left" class="sfondocommaz">
				En la siguiente tabla se visualiza los datos del postulante seleccionado y que ha certificado sus competencias.<br> 
				<br>
			</td>
		</tr>
		<tr height="2">
			<td class="sfondocommaz" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>
	<br>
	
<%End Sub

sub ImpostaPag()
	
	dim sSQL, dDataUltRil, sDescCertificatore
	
	sSQL = "SELECT DT_ULT_RIL, RAG_SOC||'('||DESCRIZIONE||')' as Certificatore," &_
		" COD_TIMPR FROM PERSONA P, SEDE_IMPRESA SI, IMPRESA I " &_
		" WHERE ID_PERSONA = " & nIdPers &_
		" AND P.ID_SEDE = SI.ID_SEDE" &_
		" AND I.ID_IMPRESA = SI.ID_IMPRESA"

'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	set rsPersona = cc.execute(sSQL)
	
	if not rsPersona.eof then
		if not isnull(rsPersona("DT_ULT_RIL")) then
			dDataUltRil			= ConvDateToString(rsPersona("DT_ULT_RIL"))
		else
			dDataUltRil			= "-"
		end if
		sTipoSede =DecCodVal("TIMPR", 0,"" , rsPersona("COD_TIMPR"),"" ) 
		sDescCertificatore	= rsPersona("Certificatore")
	else
		dDataUltRil			= "-"
		sDescCertificatore	= "-"
	end if  
	
	rsPersona.Close
	set rsPersona = nothing
	

%>
	<table WIDTH="500" BORDER="0" CELLPADDING="2" CELLSPACING="2" align="center">
		<tr>
	        <td class="tbltext1" width="150">
				<b>Datos actualizados al</b>
			</td>
	        <td class="textblack"> 
				<b><%=dDataUltRil%></b>
			</td>
		</tr>
		<tr>
	        <td class="tbltext1" width="150">
				<b>Certificados en </b>
			</td>
	        <td class="textblack"> 
				<b><%=sTipoSede%> - <%=sDescCertificatore%></b>
			</td>
		</tr>
	</table>
<%	
end sub

'**********************************************************************************************************************************************************************************
Sub Chiudi()%>
	<br><br>
	<table border="0" CELLPADDING="0" CELLSPACING="0" width="500" align="center">
		<tr>
			<td align="center"> 
				<a href="javascript:self.close()"><img title="Chiudi" src="<%=Session("Progetto")%>/images/chiudi.gif" border="0"></a>
			</td>
		</tr>
	</table>
<%End Sub
'**********************************************************************************************************************************************************************************	
%>
<!--#include Virtual ="/Include/closeconn.asp"-->
