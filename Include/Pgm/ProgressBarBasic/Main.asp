<%@ENABLESESSIONSTATE=FALSE%>
<% 'We disable maintaining session state so that more than one ASP 
    '     page can be processed at the same time 
 Response.Buffer = true %>
<HTML>
<HEAD>
<META NAME="GENERATOR" Content="Microsoft Visual Studio 6.0">
</HEAD>
<body color="black" bgcolor="white">
<table><tr><td><IMG border=0 height=36 src="../images/clouds.jpg" width=550></td></tr></table>
<span style="LEFT: 574px; POSITION: absolute; TOP: 20px; Z-INDEX: -1">
<a href="http://www.dundas.com">
<img align="right" border="0" src="../images/dundaslogo.gif" hspace="5" WIDTH="160" HEIGHT="18">
</a>
</span> <IMG border=0 height=50 src="../images/headline_upload.gif" width=390> 
<%
'most methods throw an exception if unsuccessful
on error resume next


%>
<p>
<IMG align=absMiddle border=0 height=14 src="../images/scenario.gif" width=82>
</p>
<%
'now we must create an instance of the UploadProgress component and retrieve a new state (progress) id
dim objProgress			'ProgressBar component instance
dim ProgressID			'new progress (state) id, used to uniquely identify this particular upload operation

set objProgress = server.CreateObject("Dundas.UploadProgress")

ProgressID = objProgress.GetNewProgressID   'used in the client side "Upload" function

if IsEmpty(ProgressID) = true then
	Response.Write "<font color=blue><b>The State Server is not currently running, so a Progress ID can not be obtained.<br>"
	Response.Write "<br>Please run the StateServer.exe and try this demo again.</font></b>"
	Response.End
end if
%>
<font size="3"><i>This sample application demonstrates the use of a progress bar.
<br><br>
Please note that the maximum allowable size of the file to be uploaded is 2 MBytes.
</i>
</font>
<%
'look for Result Querystring item, if it exists then output the success/failure message
'  of the upload operation
if Request.QueryString("Success") <> "" then
	Response.Write "<br><br><b><center><font color=blue>" & Request.QueryString("Success") & "</font></center></b>"
end if
%>
<br>
<Script language="javascript">
<!--
ProgressID=<%=ProgressID%>;
function Upload()
{ 
	if (ProgressID != -1){	
	//only open progressbar.asp window if there is a valid id.  We will center this progress bar as well.
		//window.document.cookie = "RefreshProgressID=TRUE";
		Param = "SCROLLBARS=no,RESIZABLE=no, TOOLBAR=no,STATUS=no,MENUBAR=no,WIDTH=400,HEIGHT=100";
		Param += ",TOP=" + String(window.screen.Height/2 - 50);
		Param += ",LEFT=" + String(window.screen.Width/2 - 200);
		window.open("ProgressBar.asp?ProgressID=<%=ProgressID%>", null, Param);
	}	
	
	//now that progress bar window is open submit the form data to Process.asp, passing
	//   the ProgressID as a querystring parameter
	document.frmMain.action = "Process.asp?ProgressID=<%=ProgressID%>"
	document.frmMain.submit();
}
//-->
</Script><center>
<form name="frmMain" action="Process.asp" enctype="multipart/form-data" method="post" style="BORDER-BOTTOM: thin; BORDER-RIGHT-STYLE: none">
<p align="center" style="BACKGROUND-COLOR: #6495ed; BORDER-BOTTOM: thin solid; BORDER-LEFT: thin solid; BORDER-RIGHT: thin solid; BORDER-TOP: thin solid; WIDTH: 400px">
<font color="#ffffff">&nbsp;&nbsp;UPLOAD INFORMATION</font>
<table width="500" border="0" cellPadding="3" cellSpacing="3" style="BACKGROUND-COLOR: #ffffe0" align="center">
  <tr>
	<td width="250">Please choose a file to be uploaded: </td>
    <td width="250"><input type="file" name="File1"></td>
  </tr>
</table></p>
<p align="center">
<!--Note that we use a standard button and not a submit button.  We do this so that
      we can open the progress bar window (Progress.asp) as well as pass the submitted
      data to the page which processes the submitted form data -->
<input type="button" name="submit1" value="Upload File" OnClick="Upload()">
<br>
</form></center>
</BODY>
</HTML>
