<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include Virtual = "/strutt_testa2.asp"-->
<!--#include Virtual = "/Include/OpenConn.asp"-->
<!--#include virtual = "/include/ControlDateVB.asp"-->
<!--#include virtual = "/include/SysFunction.asp"-->

<%
'-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

sub ImpostaPag()
%>
	<table border="0" width="520" cellspacing="0" cellpadding="0" height="81">
	   <tr>
	     <td width="500" background="<%=Session("Progetto")%>/images/titoli/strumenti2b.gif" height="81" valign="bottom" align="right">
	       <table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
	         <tr>
	           <td width="100%" valign="top" align="right"><b class="tbltext1a">Iscrizione Batch</span></b></td>
	         </tr>
	       </table>
	     </td>
	   </tr>
	</table>

	<br>
	<table cellpadding="0" cellspacing="0" width="500" border="0">
		<tr height="18">
			<td class="sfondomenu" height="18" width="67%">
				<span class="tbltext0"><b>&nbsp;GESTIONE SELEZIONE</b></span></td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
			<td valign="middle" align="right" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1"></td>
		</tr>
		<!--tr height="2">			<td colspan="3" class="SFONDOCOMMAZ" background="<%=Session("Progetto")%>/images/separazione.gif">			</td>		</tr-->
	</table>
	<br>
	<!-- Commento -->
	<table border="0" CELLPADDING="0" CELLSPACING="0" width="500" align="center">
		<tr>
			<td align="left" class="sfondocomm">
				Risultato della Selezione
			</td>
		</tr>
		<tr height="2">
			<td class="sfondocommaz" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>
	<br>

	<br><br>
	<table width="500" align="center" border="0" cellspacing="2" cellpadding="1">
		<tr height="20"> 
		  	<td class="tbltext3" align="middle" width="500">
				<b><%=sErrore%></b>
			</td>
		</tr>
	</table>
	<br><br>
	<table WIDTH="500" BORDER="0" CELLPADDING="0" CELLSPACING="0" align="center">
		<tr align="center">
			<td>
				<input type="image" src="<%=Session("progetto")%>/images/Indietro.gif" title="Torna all'elenco delle richieste" border="0" align="absBottom" id="image1" name="image1" onclick="javascript:history.go(-1);">
			</td>
		</tr>
	</table>
<%
end sub
'---------------------------- MAIN ---------------------------------------
dim sErrore

	sErrore = ""

'-----Inserimento TABELLE -----
	sStringaSelect=Request.Form ("txtSelect")
	sIndGraduatoria=Request.Form ("txtIndGrad")

	sWhere = mid(sStringaSelect, 20)

	if sIndGraduatoria = "N" then
	   sEsitoGrad =", 'S' "
	else
	   sEsitoGrad = ", NULL "  
	end if

	CC.BeginTrans 

	sSQL = "INSERT INTO DOMANDA_ISCR(ID_PERSONA, ID_BANDO, ID_SEDE, " & _
			"DT_DOM, DT_TMST, ESITO_GRAD) " &_
			"SELECT A.ID_PERSONA, " & Session("idbando") & " , 0, SYSDATE, " & _
			"SYSDATE " & sEsitoGrad & sWhere

	'Response.Write Ssql
	'Response.End 
	sErrore = EseguiNoC(sSQL,CC)

	if sErrore <> "0" then
		ImpostaPag()
		Response.End
	end if
	
'-----Inserimento nella tabella PERS_PROJ-----
	sSQL = ""
	sSQL = "INSERT INTO PERS_PROJ(ID_PERSONA, COD_CPROJ, DT_ISCRIZIONE, " & _
			"ID_UTENTE,DT_TMST) " &_
			"SELECT ID_PERSONA,'" & mid(Session("Progetto"),2) & _
			"', DT_DOM, " & Session("idutente") & ",SYSDATE " &_
			" FROM DOMANDA_ISCR DI WHERE ID_BANDO = " & Session("idbando") &_
			" AND NOT EXISTS (SELECT ID_PERSONA FROM PERS_PROJ PP " & _
					" WHERE PP.ID_PERSONA= DI.ID_PERSONA " & _
					" AND COD_CPROJ = '" & mid(Session("Progetto"),2) & "')"
		
'	Response.Write Ssql
	'Response.End 
	sErrore = EseguiNoC(sSQL,CC)

'Response.Write sErrore

	if sErrore <> "0" then
		ImpostaPag()
		Response.End
	end if	
	
	sDtOggi = Day(Now) & Month(Now) & Year(Now)
	'sDtOggi = Now
	'RIVEDERE--------------------------------
	sPath = Session("progetto") & "/DocPers/CreaLogin/" & Session("idbando") & _
			"_CreazioneLogin_" & sDtOggi & ".xls"
		
	'----------------------------------------
	sNomeFile = Server.MapPath("/") & sPath		
			
	set objLogin = Server.CreateObject("FuncLogin.clsLogin")
				
'	Response.Write "idbando: " & Session("idbando") & "<br>"
'	Response.Write "progetto: " & Session("progetto") & "<br>"
'	Response.Write "Session.SessionID: " & Session.SessionID & "<br>"
'	Response.Write "sNomeFile: " & sNomeFile & "<br>"
'	Response.End
		
	'esecuzione dll di assegnazione automatica login e password	     
			
	sRisultato = objLogin.Elabora(Session("idbando"), _
									Session("progetto"), _
									Trim(Session.SessionID), _
									sNomeFile, CC)	

	'se la dll va in errore lo visualizzo e forma l'elaborazione
	if trim(sRisultato) = "" then
		sErrore = "0"
	else
		sErrore = "Errore DLL: " & sRisultato
		'sErrore = "Errore durante l'assegnazione automatica di LOGIN e PASSWORD"
		'ImpostaPag()
		'Response.End
	end if
	
	if sErrore <> "0" then
		CC.RollbackTrans  
		ImpostaPag()      		
	else
	     CC.CommitTrans
		%><script> 
             alert("Inserimento correttamente effettuato")
             location.href ="SEL_CreaEstrattore.asp"
          </script><%
	end if	    
%>
<!--#include virtual = "/include/closeconn.asp"-->
<!--#include Virtual = "/strutt_coda2.asp"-->

