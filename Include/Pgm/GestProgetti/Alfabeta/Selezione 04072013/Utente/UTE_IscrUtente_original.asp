<%'Option Explicit 
  ' Response.ExpiresAbsolute = Now() - 1 
  ' Response.AddHeader "pragma","no-cache"
  ' Response.AddHeader "cache-control","private"
  ' Response.CacheControl = "no-cache"
%>
<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #include virtual="/strutt_testa2.asp"-->
<!--#include virtual ="/include/openconn.asp"-->
<!--#include virtual ="/util/portallib.asp"-->

<%Session("menu")= ""%>
<!-- ************** Javascript Inizio ************ -->
<!--include del file per fare i controlli sulle stringhe-->
<script LANGUAGE="Javascript" src="/Include/ControlString.inc"></script>
<script LANGUAGE="Javascript" src="/Include/ControlloCI.inc"></script>
<!--include del file per fare i controlli sulla validit� delle date-->
<script LANGUAGE="Javascript" src="/Include/ControlDate.inc"></script>
<!--include del file per fare i controlli sulla numericit� dei campi-->
<script LANGUAGE="Javascript" src="/Include/ControlNum.inc"></script>

<script LANGUAGE="Javascript" src="/Include/help.inc"></script>
<script LANGUAGE="Javascript" src="/include/SelComune.js"></script>
<script LANGUAGE="Javascript" src="Controlli.js"></script>
<script LANGUAGE="Javascript" src="ControlliBac.js"></script>
<script LANGUAGE="Javascript" src="ControlliBac2.js"></script>
<script LANGUAGE="Javascript" src="ControlliBac3.js"></script>
<!-- ************** ASP Inizio *************** -->

<!--#include virtual = "/include/DecCod.asp"-->
<!--#include virtual = "/include/ControlDateVB.asp"-->
<!--#include virtual = "/util/dbutil.asp"-->

<script LANGUAGE="Javascript">



	function PulisciLivStud()
	{
		document.frmDomIscri.txtSpecifico.value = ""
		document.frmDomIscri.txtTipoSpecifico.value = ""
	}

	function PulisciRes()
	{
		document.frmDomIscri.txtComuneRes.value = ""
		document.frmDomIscri.txtComRes.value = ""
				
	}
	
	function ActivInputFields()
{
	if (document.frmDomIscri.COD_STAT_STUD.value == "0")
	{
		document.frmDomIscri.AA_STUD_UltAnno.value ="";
		document.frmDomIscri.AA_STUD_UltAnno.disabled = true;
		document.frmDomIscri.txtAnnoStud.disabled = false;
		
	}	
	
	if ((document.frmDomIscri.COD_STAT_STUD.value == "2")
		   || (document.frmDomIscri.COD_STAT_STUD.value == "1"))
			
			 {
				document.frmDomIscri.AA_STUD_UltAnno.disabled = false;
				document.frmDomIscri.txtAnnoStud.value= "";
				document.frmDomIscri.txtAnnoStud.disabled = true;
				
			}	
			
			
			if (document.frmDomIscri.COD_STAT_STUD.value == "4")
			{
				document.frmDomIscri.AA_STUD_UltAnno.value ="";
				document.frmDomIscri.AA_STUD_UltAnno.disabled = true;
				document.frmDomIscri.txtAnnoStud.value ="";
				document.frmDomIscri.txtAnnoStud.disabled = true;
				
			}			
	}	
	
				
//Funzione per i controlli dei campi da inserire 
	function ControllaDati()
	{		
		// SM inizio- controllo C.I.
		
		document.frmDomIscri.txtCodFisc.value=TRIM(document.frmDomIscri.txtCodFisc.value)
		if (document.frmDomIscri.txtCodFisc.value != "")
		{
				blank = " ";
				if (!ChechSingolChar(document.frmDomIscri.txtCodFisc.value,blank))
				{
			
					alert("El campo C.I. no puede contener espacios en blanco")
					document.frmDomIscri.txtCodFisc.focus()
					return false;
				}
				
				if ((document.frmDomIscri.txtCodFisc.value.length != 8))
				{
					alert("El campo C.I. debe ser de 8 caracteres")
					document.frmDomIscri.txtCodFisc.focus() 
					return false
				}			
				if (!ValidateInputStringWithNumber(document.frmDomIscri.txtCodFisc.value))
				{
					alert("El campo C.I. es err�neo")
					document.frmDomIscri.txtCodFisc.focus() 
					return false
				}
				
				if (!IsNum(document.frmDomIscri.txtCodFisc.value))
				{
					alert("El campo C.I. debe ser num�rico")
					document.frmDomIscri.txtCodFisc.focus() 
					return false
				}		
		
				if (!ControlloCI(eval(document.frmDomIscri.txtCodFisc.value)))
				{
				     document.frmDomIscri.txtCodFisc.focus()
				     return false
				}
				
		}		
		// SM fine
		
				
		//Nome
		document.frmDomIscri.txtNome.value=TRIM(document.frmDomIscri.txtNome.value)
		if (document.frmDomIscri.txtNome.value == "")
		{	
			alert("El campo nombre es obligatorio!")
			document.frmDomIscri.txtNome.focus() 
			return false
		}
		sNome=ValidateInputStringWithOutNumber(document.frmDomIscri.txtNome.value)	
		if  (sNome==false){
			alert("El campo nombre no es correcto.")
			document.frmDomIscri.txtNome.focus() 
			return false
		}
		
		//Cognome
		document.frmDomIscri.txtCognome.value=TRIM(document.frmDomIscri.txtCognome.value)
		if (document.frmDomIscri.txtCognome.value == "")
		{
			alert("El campo apellido es obligatorio!")
			document.frmDomIscri.txtCognome.focus() 
			return false
		}
		sCognome=ValidateInputStringWithOutNumber(document.frmDomIscri.txtCognome.value)
		if  (sCognome==false){
			alert("El campo apellido no es correcto.")
			document.frmDomIscri.txtCognome.focus() 
			return false
		}

	 	//Data di Nascita
		if (document.frmDomIscri.txtDataNascita.value == "")
		{
			alert("El campo fecha de nacimiento es obligatorio!")
			document.frmDomIscri.txtDataNascita.focus() 
			return false
		}
		//Controllo della validit� della Data di Nascita
		DataNascita = document.frmDomIscri.txtDataNascita.value
		DataOdierna = frmDomIscri.txtOggi.value
		if (!ValidateInputDate(DataNascita))
		{
			document.frmDomIscri.txtDataNascita.focus() 
			return false
		}
		if (ValidateRangeDate(DataNascita,DataOdierna)==false){
			//alert("La data di nascita deve essere precedente alla data odierna!")
			alert("El campo fecha de nacimiento debe ser anterior a la fecha actual!")
			frmDomIscri.txtDataNascita.focus()
			return false
		}
		
	   //Sesso
	   Sesso=document.frmDomIscri.cmbSesso.value
		if (Sesso == "")
		{
			alert("El campo Sexo es obligatorio!")
			document.frmDomIscri.cmbSesso.focus() 
			return false
		}	
  
		
        if (document.frmDomIscri.txtCodFisc.value == "")
		         {
			      alert("Debe ingresar un C.I.")
			      document.frmDomIscri.txtCodFisc.focus() 
			      return false
		      }
		     
		     
		     
       //n. telefono
       if (document.frmDomIscri.txtTelefono.value == ""){
			alert("El campo num�ro de Tel�fono es obligatorio!")
			document.frmDomIscri.txtTelefono.focus() 
			return false
		}  
		      
		if (!IsNum(document.frmDomIscri.txtTelefono.value)) {
			alert("El campo num�ro de Tel�fono debe ser num�rico")
			document.frmDomIscri.txtTelefono.focus()
			return false
		}
		if (eval(document.frmDomIscri.txtTelefono.value) == 0){
			alert("El num�ro de tel�fono no puede ser cero")
			document.frmDomIscri.txtTelefono.focus()
			return false
		}
			
		if (!IsNum(document.frmDomIscri.txtCellulare.value)) {
			alert("El campo num�ro de Tel�fono celular debe ser num�rico")
			document.frmDomIscri.txtCellulare.focus()
			return false
		}
		if (eval(document.frmDomIscri.txtCellulare.value) == 0){
			alert("El num�ro de tel�fono celular no puede ser cero")
			document.frmDomIscri.txtCellulare.focus()
			return false
		}
					      
	   //Email
	   /*if (document.frmDomIscri.txtEmail.value == "")
		      {
			      alert("El campo email es obligatorio!")
			      document.frmDomIscri.txtEmail.focus() 
			      return false
		      }  
	   */
	   
	   if (document.frmDomIscri.txtEmail.value != ""){
		   pippo=ValidateEmail(document.frmDomIscri.txtEmail.value)
		
		   if  (pippo==false)
		   {
			   alert("El campo email no es correcto.")
			   document.frmDomIscri.txtEmail.focus() 
			   return false
		   }
		}
		//return true
		
		//Controlli Residenza
		
		 Indirizzo=document.frmDomIscri.txtIndirizzo.value
		if ((Indirizzo == "")||
			(Indirizzo == " "))
		{
			alert("La direcci�n de la Residencia es obligatoria!")
			document.frmDomIscri.txtIndirizzo.focus() 
			return false
		}	
						
		//Se la Provincia di Residenza � vuota, il Comune � obbligatorio
		if ((document.frmDomIscri.cmbProvRes.value == "")||
			(document.frmDomIscri.cmbProvRes.value == " ")) {
				alert("El Departamento de Residencia es obligatorio!")
				document.frmDomIscri.cmbProvRes.focus() 
				return false
			
		}		
		
		//Se Comune di Residenza � vuoto, il Comune � obbligatorio
		if ((document.frmDomIscri.txtComuneRes.value == "")||
			(document.frmDomIscri.txtComuneRes.value == " ")) {
				alert("La Localidad de Residencia es obligatoria!")
				document.frmDomIscri.txtComuneRes.focus() 
				return false
			
		}	
		//Se Comune di Residenza � digitato, la Provincia � obbligatoria
		if (document.frmDomIscri.txtComuneRes.value != ""){
			if (document.frmDomIscri.cmbProvRes.value == ""){
				//alert("Se viene selezionato il Comune di Residenza, anche il campo Provincia di Residenza � obbligatorio!")
				alert("Si ha seleccionado el campo Localidad, tambi�n el campo Departamento de Residencia es obligatorio!")
				document.frmDomIscri.cmbProvRes.focus() 
				return false
			}
		}
	    //Se la Provincia di Residenza � digitata, il Comune � obbligatorio
		if ((document.frmDomIscri.txtComuneRes.value == "")||
			(document.frmDomIscri.txtComuneRes.value == " ")) {
			if (document.frmDomIscri.cmbProvRes.value != ""){
				//alert("Se viene selezionata la Provincia di Residenza, il campo Comune di Residenza � obbligatorio!")
				alert("Si ha seleccionado el Departamento de Residencia, el campo Localidad de Residencia es obligatorio!")
				document.frmDomIscri.txtComuneRes.focus() 
				return false
			}
		}
		
		//controllo titolo studio
		
		if ((document.frmDomIscri.cmbLivStud.value == "")||
			(document.frmDomIscri.cmbLivStud.value == " "))
		{
			alert("�El nivel de estudios es obligatorio!")
			document.frmDomIscri.cmbLivStud.focus() 
			return false
		}	
		
		if ((document.frmDomIscri.txtSpecifico.value == "")||
			(document.frmDomIscri.txtSpecifico.value == " "))
		{
			alert("�El t�tulo de estudio es obligatorio!")
			document.frmDomIscri.txtSpecifico.focus() 
			return false
		}	
		
		
		//VALIDACION A REALIZAR SI NO ES NTS (NINGUN TITULO DE ESTUDIO)
			if ((document.frmDomIscri.cmbLivStud.value != "NLE") && (document.frmDomIscri.cmbLivStud.value != "SLE"))
		{
			if (document.frmDomIscri.COD_STAT_STUD.value == "4")
			{
				alert("El valor no corresponde en el estado del estudio solo puede ser indicado si no asisti� a la escuela");
				return false;
			}
		}

		if ((document.frmDomIscri.cmbLivStud.value == "NLE") || (document.frmDomIscri.cmbLivStud.value == "SLE"))
		{
			if (document.frmDomIscri.COD_STAT_STUD.value != "4")
			{
				alert("El valor no corresponde es el �nico aceptado para el estado si no asisti� a la escuela");
				return false;
			}
		}
	if (document.frmDomIscri.cmbLivStud.value != "NTS")
{
		if (document.frmDomIscri.COD_STAT_STUD.value == "0")
	        {
		 
				if (document.frmDomIscri.txtAnnoStud.value == "") { 
					alert("El campo A�o de frecuencia es obligatorio")
					 document.frmDomIscri.txtAnnoStud.focus()
				    return false
				}  
		
				if (!IsNum(document.frmDomIscri.txtAnnoStud.value)) {
					alert("El campo A�o debe ser num�rico")
					document.frmDomIscri.txtAnnoStud.focus()
					return false
				}
		 
				if (eval(document.frmDomIscri.txtAnnoStud.value == 0)) {			
					alert("El campo A�o no puede ser cero")
					 document.frmDomIscri.txtAnnoStud.focus()
				    return false
				}  
		   
				if (eval(document.frmDomIscri.txtAnnoStud.value > document.frmDomIscri.txtAnnoCorrente.value)) {			
					alert("El campo A�o no puede ser mayor del a�o corriente")
					 document.frmDomIscri.txtAnnoStud.focus()
				    return false
				}
			
					
					if (eval(DataNascita.substr(6,4) > document.frmDomIscri.txtAnnoStud.value )){
					alert("El campo a�o debe ser superior al a�o de nacimiento")
				    document.frmDomIscri.txtAnnoStud.focus()
					return false
				}
		
		   }
			   
			if ((document.frmDomIscri.COD_STAT_STUD.value == "2")
			   || (document.frmDomIscri.COD_STAT_STUD.value == "1"))
				
				 {
				 
				 if (document.frmDomIscri.AA_STUD_UltAnno.value == "") { 
						alert("El campo Ultimo a�o aprobado es obligatorio")
						 document.frmDomIscri.AA_STUD_UltAnno.focus()
					    return false;
					}  
				 
				 
				 if (!IsNum(document.frmDomIscri.AA_STUD_UltAnno.value)) {
						alert("El campo Ultimo a�o aprobado debe ser num�rico")
						document.frmDomIscri.AA_STUD_UltAnno.focus()
						return false;
					}
			 
				 if (eval(document.frmDomIscri.AA_STUD_UltAnno.value == 0)) {			
						alert("El campo Ultimo a�o aprobado no puede ser cero")
						 document.frmDomIscri.AA_STUD_UltAnno.focus()
					    return false;
					}  
		   } 
		
		 }	
			
		// consenso trattamento dati
//			if (document.frmDomIscri.radConsenso[0].checked == false) {
//				alert("El consentimiento al tratamiento de datos de car�cter personal es obligatorio")
//				document.frmDomIscri.radConsenso[0].focus()
//				return false
//			}	
		
	return true
}

</script>

<%
	sub CreaComboCOD_STAT_STUD(codice)
	dim stringa_frequenza,aFrequenza

		sSql="SELECT ID_CAMPO_DESC " &_
			"FROM DIZ_DATI " &_
			"WHERE " &_
			"ID_TAB='TISTUD' AND ID_CAMPO='COD_STAT_STUD'"

'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
		set Rst=CC.Execute(sSql)
		stringa_frequenza=Rst("ID_CAMPO_DESC")
		aFrequenza=split(stringa_frequenza,"|")
	%>
		<select name="COD_STAT_STUD" class="textblack" id="COD_STAT_STUD" onChange="return ActivInputFields(this.value);">

	<%	for i=0 to Ubound(aFrequenza) step 2
			if aFrequenza(i) <> "3" then
				Response.Write "<option value='" & aFrequenza(i) & "'"
				if aFrequenza(i)=Codice then Response.Write " selected"
				Response.Write ">" & Ucase((aFrequenza(i+1))) & "</option>"
			end if
		next
	%>	
	</select>
	<%	Rst.Close
		set Rst=nothing
	end sub
%>

<%sub Inizio()%>
	<table cellpadding="0" cellspacing="0" width="500" border="0">
			<tr height="18">
				<td class="sfondomenu" height="18" width="67%">
				<span class="tbltext0"><b>&nbsp;PEDIDO DE INSCRIPCI�N</b></span></td>
				<td width="3%" background="/Ba/images/tondo_linguetta.gif">&nbsp;</td>
				<td valign="middle" align="right" class="tbltext1" width="50%" background="/Ba/images/sfondo_linguetta.gif">(*) campos obligatorios</td>
			</tr>
			<tr>
				<td class="sfondocomm" width="57%" colspan="3">
				    Rellenar la ficha con los datos y presionar <b>Envia</b> para confirmar la inserci�n.<br>
				    En caso de dificultad en la utilizaci�n del procedimiento autom�tizado <a href="mailto:dgutierrez@italialavoro.it">cont�ctanos</a>
				<!--a href="Javascript:Show_Help('/Ba/Pgm/Help/GestProgetti/Alfabeta/Selezione/Utente/UTE_IscrUtente')" onmouseover="javascript:window.status=' '; return true"><img align="right" src="/Ba/images/help.gif" border="0"></a-->
				</td>
			</tr>
			<tr height="2">
				<td colspan="3" class="sfondocomm" background="/BA/images/separazione.gif">
				</td>
			</tr>
	</table>
<%end sub%>	



<%Sub Msgetto(Msg)%>
	<br>
	<table align="center">
		<tr>
			<td class="tbltext3"><b><%=Msg%></b></td>
		</tr>	
		<tr>
			<td>&nbsp;</td>
		</tr>	
		<tr>
			<td align="center"><!--a HREF="javascript:history.back()"-->
			<a HREF="javascript:history.back()">
			<img SRC="/Ba/images/indietro.gif" border="0" WIDTH="55" HEIGHT="40"></a></td>
		</tr>
	</table>
<%End Sub%>


<%sub ImpPagina()%>
<form action="UTE_CnfDomIscr.asp" method="post" name="frmDomIscri" id="frmDomIscri"  onsubmit="return ControllaDati(this)">
	<input type="hidden" name="progetto" value="<%=Session("Progetto")%>">
	<input type="hidden" name="txtcodproj" value="<%=sProgetto%>">
	<input type="hidden" name="txtIdBando" value="<%=nBando%>">
	<input type="hidden" name="txtDescTimpr" value="<%=sDescTimprI%>">
<br>
<table border="0" cellpadding="0" cellspacing="0" width="500">
    <tr height="18">
		<td class="sfondomenu" width="67%" height="18">
			<span class="tbltext0"><b>&nbsp;DATOS DEL CANDIDATO</b></span></td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
		<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
    </tr>
    <tr height="2">
			<!--td width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif">	</td-->     
	</tr>
 </table>
 <br>
<table border="0" cellpadding="2" cellspacing="2" width="500">
  	<tr>
        <td align="left" colspan="2" class="tbltext1">
				<span class="tbltext1">
				<strong>C.I.* </strong>(solo numeros, sin puntos ni guion) &nbsp; por ej. CI. 2.877.654-3, ingresarla 28776543
				</span>
        </td>
        <td align="left" colspan="2" width="60%">
				<input class="textblacka" style="TEXT-TRANSFORM: uppercase; WIDTH:   167px; HEIGHT: 22px" size="11" maxlength="8" name="txtCodFisc">
        </td>
    </tr>

    <tr>
		<td align="left" colspan="2" nowrap class="tbltext1">
			<span class="tbltext1">
				<strong>Nombres*</strong></span>
		</td>
		<td align="left" colspan="2" width="30%">
			<input class="textblacka" style="TEXT-TRANSFORM: uppercase; HEIGHT: 22px" size="35" maxlength="50" name="txtNome">
		</td>
    </tr>
   <tr>
		<td align="left" colspan="2" nowrap class="tbltext1" width="100%">
			<span class="tbltext1">
				<strong>Apellidos*</strong></span>
		</td>
		<td align="left" colspan="2">
				<input class="textblacka" style="TEXT-TRANSFORM: uppercase; WIDTH: 220px;
					 HEIGHT: 22px" size="35" maxlength="50" name="txtCognome">
		</td>
    </tr>
    <tr>
		<td align="left" colspan="2" nowrap class="tbltext1">	
				<span class="tbltext1">					
				<strong>Fecha de Nacimiento*</strong>(dd/mm/aaaa)								
		</td>	
		<td align="left" colspan="2" width="60%">
				<input class="textblacka" style="TEXT-TRANSFORM: uppercase; WIDTH: 82px; HEIGHT: 22px" size="50" maxlength="10" name="txtDataNascita">			
		         <input type="hidden" name="txtOggi" value="<%=ConvDateToString(Date())%>">

		</td>
    </tr>
    <!--tr>
		<td height="2" align="left" colspan="4" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
    </tr-->
    
	
	<tr>
		<td align="middle" colspan="2" nowrap class="tbltext1">
				<p align="left">
					<strong>Sexo*</strong>
					<strong>&nbsp;</strong>
				</p>
			</td>
			<td align="left" colspan="2" width="60%">
				<select class="textblack" id="cmbSesso" name="cmbSesso">
					<option selected>
					<option value="F">FEMENINO
					<option value="M">MASCULINO
					</option>
				</select>
	        </td>
	</tr>	
<!--    <tr height="2">
			<td  colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif">	</td>     
	</tr>
	<tr>
		<td class="sfondocomm" width="60%" colspan="3">
			Indique un n�mero de Tel�fono al cual los operadores puedan llamar directamente para 
			comunicaciones urgentes. Es necesario escribir los n�meros que lo componen sin espacios u 
			otros separadores. Indique su direcci�n de correo electr�nico personal en modo 
			de facilitar los contactos.  
		</td>
	</tr>-->
    <tr>
        <td align="left" colspan="2" class="tbltext1">
				<span class="tbltext1">
				<strong>N. Tel�fono*</strong>
				</span>
        </td>
        <td align="left" colspan="2" width="60%">
				<input class="textblacka" style="WIDTH:167px; HEIGHT: 22px" size="50" maxlength="15" name="txtTelefono">
        </td>
    </tr>
    <tr>
        <td align="left" colspan="2" class="tbltext1">
				<span class="tbltext1">
				<strong>N. Tel�fono celular</strong>
				</span>
        </td>
        <td align="left" colspan="2" width="60%">
				<input class="textblacka" style="WIDTH:167px; HEIGHT: 22px" size="50" maxlength="15" name="txtCellulare">
        </td>
    </tr>
    <tr>
        <td align="left" colspan="2" class="tbltext1">
				<span class="tbltext1">
				<strong>Email</strong>
				</span>
        </td>
        <td align="left" colspan="2" width="60%">
				<input class="textblacka" style="WIDTH:167px; HEIGHT: 22px" size="50" maxlength="100" name="txtEmail">
        </td>
    </tr>
    </table> <br>
    <!--inizio blocco RESIDENZA-->
    <Table border="0" cellpadding="0" cellspacing="0" width="500">
    <tr height="18">
		<td class="sfondomenu" width="67%" height="18">
			<span class="tbltext0"><b>&nbsp;RESIDENCIA</b></span></td>
		<td width="15%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
		<td valign="middle" align="right" class="tbltext1" width="18%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
		
    </tr>
    <tr height="2">
			<td width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif">	</td>     
	</tr>
        <td align="left" colspan="2" class="tbltext1">
				<span class="tbltext1">
				<strong>Direcci�n*</strong>
				</span>
        </td>
        <td align="left" colspan="2" width="60%">
				<input class="textblacka" style="WIDTH:167px; HEIGHT: 22px" size="70" maxlength="50" name="txtIndirizzo">
        </td>
    </tr>
    <tr>
        <td align="left" colspan="2" class="tbltext1">
				<span class="tbltext1">
				<strong>Barrio</strong>
				</span>
        </td>
        <td align="left" colspan="2" width="60%">
				<input class="textblacka" style="WIDTH:167px; HEIGHT: 22px" size="70" maxlength="100" name="txtFrazioneIndirizzo">
        </td>
    </tr>
	<tr>    
        <td align="left" colspan="2" class="tbltext1">
			<span class="tbltext1">
				<strong>Departamento*</strong>
			 </span>
		</td>
        <td align="left" colspan="2" width="60%">
		<%
			'''27/08/2003
			'''sInt = "PROV|0|" & date & "| |cmbProvRes' onchange='PulisciRes()|ORDER BY DESCRIZIONE"			
			sInt = "PROV|0|" & date & "| |cmbProvRes' onchange='PulisciRes(0)|AND CODICE NOT IN ('EE','EX') ORDER BY DESCRIZIONE"			
			'''FINE 27/08/2003
			CreateCombo(sInt)
		%>
        </td>
    </tr>
    <tr>		
		<td align="left" colspan="2" nowrap class="tbltext1">
			<b>Localidad*</b>
		</td>
		<td nowrap>
			<span class="tbltext">
			<input type="text" name="txtComuneRes" style="TEXT-TRANSFORM: uppercase;" class="textblacka" size="35" readonly value="<%=DescrComuneResid%>">
			<input type="hidden" id="txtComRes" name="txtComRes" value="<%=COM_RES%>">
<%
			NomeForm="frmDomIscri"
			CodiceProvincia="cmbProvRes"
			NomeComune="txtComuneRes"
			CodiceComune="txtComRes"
			Cap="NO"
			'Cap="txtCap"
%>
			<a href="Javascript:SelComune('<%=NomeForm%>','<%=CodiceProvincia%>','<%=NomeComune%>','<%=CodiceComune%>','<%=Cap%>')" ID="imgPunto1" name="imgPunto1" onmouseover="javascript:window.status='';return true"><img border="0" src="<%=Session("Progetto")%>/images/bullet1.gif"></a>
			</span>				
		</td>
	</tr>
	
		 <tr>
			<td align="left" colspan="2">
				<p align="left">
					<span class="tbltext1"><b><B>C�digo Postal</b></span>
				</p>
            </td>            
			<td align="left" colspan="2"> 
				<p align="left">					
					<input style="TEXT-TRANSFORM:uppercase" WIDTH="82px" class="textblack" size="10" maxlength="5" name="txtCap">
					</font>
		        </p>
            </td>
        </tr>         
	</Table><br>
	<%end sub%>
	<%Sub Curriculum ()%>
	<!--inizio am blocco CARATTERISTICHE CURRICULARI-->
    <Table border="0" cellpadding="0" cellspacing="0" width="500">
    <tr height="18">
		<td class="sfondomenu" width="67%" height="18">
			<span class="tbltext0"><b>&nbsp;NIVEL EDUCATIVO</b></span></td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
		<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
    </tr>
     <tr>
		<td class="sfondocomm" width="57%" colspan="3"> Si ha seleccionado "Complet�", indicar el a�o en que alcanz� el t�tulo de estudio.<br>
														Si abandon� sus estudios o a�n sigue cursando complete el campos "Ultimo a�o aprobado".</td>
	</tr>
    </table>
    <Table border="0" cellpadding="2" cellspacing="2" width="500">
	<tr>	
		<!--td align="left" class="tbltext1">
			<p align="left">
			<strong>Livello*</strong>
			</p>
		</td>
		<td align="left" width="60%">
			<p align="left"-->
			

	<tr>
		<td align="left" class="tbltext1">
			<b>Nivel de estudios*</b>
		</td>
		<td align="left">
				<%
				sInt = "LSTUD|0|" & date & "| |cmbLivStud' onchange='PulisciLivStud()|AND codice not in ('ELE','NES','SPC','UNI','POS','UNI','POL','TER','PRO','ESP') ORDER BY VALORE " 'DESCRIZIONE
				CreateCombo(sInt)
				%>
	    </td>
	</tr>
	<tr height="20">
		<td class="tbltext1" align="left">
			<p align="left"><b>Especificar el titulo de estudios*</b>
		</td>
		<td colspan="2">
				<input class="textblacka" readonly type="text" name="txtSpecifico" style="width=250px">
				<input type="hidden" name="txtTipoSpecifico">
				<a href="Javascript:SelSpecificoStudio()" ID="imgPunto2" name="imgPunto2"><img border="0" src="<%=Session("Progetto")%>/images/bullet1.gif"></a>
		</td>
	</tr>
	<!--tr height="20">
		<td align="left">
			<p align="left" class="tbltext1"><strong>Status*</strong>
		</td>
		<td>
				<input type="radio" id="cmbFre" name="cmbFre" value="T"><span CLASS="TBLTEXT">Terminado</span>
				<input type="radio" id="cmbFre" name="cmbFre" value="I"><span CLASS="TBLTEXT">En curso</span>
		</td>
    </tr-->
    
    <tr height="20">
		<td align="left" colspan="1">
			<p align="left" class="tbltext1"><strong>Estado del Estudio</strong>
		</td>
		<td>
		<%CreaComboCOD_STAT_STUD("0")%>
			
		</td>
    </tr>
   	    <tr>
		<td class="tbltext1"><b>A�o Finalizaci�n&nbsp;</b> (aaaa)
		</td>
		<td class="tbltext1">
			<input class="textblack" size="5" maxlength=4" name="txtAnnoStud">
			<input type="hidden" name="txtAnnoCorrente" value="<%=Year(Date)%>">
      	</td>					
    </tr>
<!--    <tr height="2">
			<td width="100%" colspan="4" background="<%=Session("Progetto")%>/images/separazione.gif">	</td>     
	</tr>-->
	
	 <tr height="20">
		<td class="tbltext1" align="left" colspan="1">
			<b>Ultimo a�o aprobado</b><br>
		</td>

		<td align="left" class="tbltext1" colspan="2" width="60%">
				<input disabled class="textblacka" size="2" maxlength="1" name="AA_STUD_UltAnno">
				&nbsp;
		</td>
    </tr>
    
    <tr height="20">
		<td class="tbltext1" align="left" colspan="1">
			<b>Lugar donde curs� el �ltimo a�o aprobado</b><br>
		</td>

		<td align="left" class="tbltext1" colspan="2" width="60%">
				<input class="textblacka" size="50" maxlength="100" name="AA_STUD_LUltAnno">
				&nbsp;
		</td>
    </tr>
    
	</table><br>
	
<table cellpadding="0" cellspacing="0" width="500" border="0">
		<tr height="18">
			<td class="sfondomenu" height="18" width="67%">
			<span class="tbltext0"><b>&nbsp;Declaraciones</b></span></td>
			<td width="3%" background="/Ba/images/tondo_linguetta.gif">&nbsp;</td>
			<td valign="middle" align="right" class="tbltext1" width="50%" background="/Ba/images/sfondo_linguetta.gif"></td>
		</tr>
		<tr height="2">
			<td colspan="3" class="sfondocomm" background="/Ba/images/separazione.gif">
			</td>
		</tr>
</table>
<table width="500" border="0" cellspacing="2" cellpadding="1">     
        <tr>
	        <td align="CENTER" class="tbltext" colspan=3>
<!--	        PARA PARTICIPAR EN EL PROGRAMA DEL PROYECTO <BR>"PLAN DE EQUIDAD" -->
La informaci�n relevada en este formulario podr� ser utilizada con fines 
estad�sticos tal como lo establece la ley N� 16616 del 11/10/94	       
<br>
Al hacer clic en "ACEPTAR", se aceptan las condiciones de participaci�n en el Programa, 
vigentes a la fecha cumpliendo �ntegramente los requisitos establecidos.
Declaraci�n Jurada: Art. 239 del C�digo Penal. El que con motivo del otrogamiento o formalizaci�n 
de un documento p�blico, prestare una declaraci�n falsa sobre su identidad o estado o cualquier otra
circunstancia de hecho, ser� castigado con tres a venticuatro meses de prisi�n
			</td>
		</tr>
<!--		<tr><td align="CENTER" class="tbltext" colspan=3><b>DECLARA</b></td></tr>
		<tr>
	        <td align="left" class="tbltext" width="30%" colspan=3>
	        <OL> 
				<LI>Tener ciudadan�a urug o poder documentar la ascendencia italiana
				<LI>Ser residente en Argentina
				<LI>Ser mayor de 18 a�os
				<LI>Estar desocupado 
				<LI>No percibir prestaciones previsionales o seguros de desempleo o capacitaci�n laboral 
				del Ministerio de Trabajo, Empleo y Seguridad de la Naci�n, as� como de otros programas 
				de empleo nacionales o provinciales argentinos. 
	        </OL>
	        </td>
		</tr>
-->		
</table>
<br>
<table width="520">
	<tr height="2">
				<td colspan="3" class="sfondocomm" background="/Ba/images/separazione.gif">
				</td>
			</tr>   
	<tr>
		<td colspan="2" class="tbltext"><p align="justify">
		
<%
			err.Clear 
			on error resume next 
			'PathFileEdit = "/Ba/Testi/Autorizzazioni/info_privacy.htm"
			'Server.Execute(PathFileEdit)
			
			If err.number <> 0 Then
				Response.Write "Error en la visualizaci�n."
				Response.Write "Contactar el Grupo Asistencia Portal "
				Response.Write "a la direcci�n dgutierrez@italialavoro.it "
			End If				
%>
			</p>
		</td>
	</tr>
</table>
<!--<table width="520">	
		<tr>
			<td colSpan="2"><br>
				<center>
					<input type="radio" value="0" id="radConsenso" name="radConsenso"><span class="tbltext">ACEPTO</span>
					<input type="radio" value="1" CHECKED id="radConsenso" name="radConsenso"><span class="tbltext">NO ACEPTO</span>
					<input type="hidden" name="sControllo" value="2">
				</center>
			</td>
		</tr>
	</table>-->
	<input type="hidden" id="radConsenso" name="radConsenso" value="0">
<p>
<table border="0" cellpadding="1" cellspacing="1" width="370" align="center">
	 <tr>
        <td align="middle" colspan="2">
        <!--input type="image" name="Invia" src="<%=Session("Progetto")%>/images/conferma.gif" border="0" value="Conferma" onclick="return ControllaDati(this)"--> 
        <input type="image" name="Invia" src="<%=Session("Progetto")%>/images/conferma.gif" border="0" value="Conferma"> 
        <input type="hidden" name="graduatoria" value="<%=sGraduatoria%>">
                 <input type="hidden" name="bando" value="<%=nBando%>">
                 <input type="hidden" name="gruppo" value="<%=sGruppo%>">
	            <input type="hidden" name="descbando" value="<%=sDescBando%>">
	            <!--input type="text" name="txtDescTimpr" value="<%=sDescTimprI%>"-->

        </td>
        <td nowrap align="left">
        <%'16/10/2007 GESTIONE TASTO INDIETRO IN AREA PUBBLICA
        
        IF session("login") = "" then %>
			<a HREF="javascript:history.back()">
        <%ELSE %>
						<a HREF="javascript:history.back()">

		<%END IF %>
			<img SRC="<%=Session("Progetto")%>/images/indietro.gif" border="0">
		</td>
     </tr>
</table>
</p>
</form>
<%end sub
function ctrlProj()
	ctrlProj = ""
	sSQL = "SELECT COD_TIMPR FROM PROGETTO WHERE ID_PROJ = " & CLng(nIdProgetto)
	
	'PL-SQL * T-SQL  
    sSQL = TransformPLSQLToTSQL (sSQL)
	set rsProj = CC.Execute(sSQL)
	
	if rsProj.EOF then
		Msgetto("Progetto inesistente")	
		ctrlProj = "KO"
	else
		sCodTimpr = rsProj("COD_TIMPR")
		
		ctrlProj = "OK"
	end if
	
end function

sub ctrlBando()

	'	Se la data di sistema non � compresa nel periodo di 
	'   acquisizione domande non devo caricare la domanda.
	set rsAbil = Server.CreateObject("ADODB.Recordset")

	sSQL = " SELECT B.ID_PROJ, B.DESC_BANDO, B.IDGRUPPO, P.IND_GRAD, COD_CPROJ, COD_TIMPR  FROM BANDO B, PROGETTO P " & _
		   "WHERE B.ID_PROJ = P.ID_PROJ "	 &_
		   "AND B.ID_BANDO = "& nBando & " AND " & nTime & _
		   " BETWEEN B.DT_INI_ACQ_DOM AND B.DT_FIN_ACQ_DOM"
	'-----------------------------------------------------------------------------
	' Questo controllo � stato inserito per permettere l'iscrizione di nominativi
	' anche a bando(sport2job) chiuso tale operazione puo essere eseguita solo dagli
	' utenti appartenenti al gruppo ISCR tramite la funzione ISCRIZIONE
	if ucase(nBypassdata) = "SI" then
		sSQL = " SELECT B.ID_PROJ, B.DESC_BANDO, B.IDGRUPPO, P.IND_GRAD  FROM BANDO B, PROGETTO P " & _
			   " WHERE B.ID_PROJ = P.ID_PROJ "	 &_
			   " AND B.ID_BANDO = "& nBando
				
	end if
    '----------------------------------------------------------------------------
    
    'PL-SQL * T-SQL  
    sSQL = TransformPLSQLToTSQL (sSQL)
   	rsAbil.Open sSQL, CC, 3

	if  rsAbil.EOF  then
		Msgetto("Per�odo de Adquisici�n preguntas no valido")
		sEsitoBando = "KO"
	else
		sDescBando = rsAbil("DESC_BANDO")
	    nIdProgetto = rsAbil("ID_PROJ")
	    sEsitoBando = "OK"
	    sGruppo =rsAbil("IDGRUPPO")
	    sGraduatoria =rsAbil("IND_GRAD")
	    sProgetto=rsAbil("COD_CPROJ")
	    sDescTimprI=rsAbil("COD_TIMPR")
	end if 
	rsAbil.close
	set rsAbil = nothing	 

end sub
%>
<%sub Fine()%>
<!--#include virtual="/strutt_coda2.asp"-->
<!--#include Virtual = "/include/CloseConn.asp"-->	
<%end sub%>
<!-- ************** ASP Fine *************** -->

<!-- ************** MAIN Inizio ************ -->
<%
dim sDataIsc, sEsitoBando, nCont
dim nIdProgetto,sGruppo,sDescBando, sProgetto
dim nBando, nTime, nBypassdata, lGrado, sGraduatoria, DataCateg, DataOdierna, sDescTimprI

sDataIsc = Now()

nBando=clng(Request("txtIdBando"))
nTime = convdatetodb(Date)
nBypassdata = Request("Bypassdata")
lGrado = Request("lGrado")

		Dim aMenu(0)
		aMenu(0) = Session("Progetto") & "/home.asp"
		Session("Indice") = aMenu
		Session("Menu") = aMenu(0)
		nPos = 0

ctrlBando()
if  sEsitoBando = "OK" then
    Inizio()
    if Trim(ctrlProj()) = "OK" then
		sDescTimpr = DecCodVal("TIMPR", "0", "", sCodTimpr, 1)  
		sDataSis = ConvDateToString(Now())
		ImpPagina()		
		Curriculum()
	end if	

end if

Fine()
%>
<!-- ************** MAIN Fine ************ -->

