<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->


<!--#include Virtual = "/strutt_testa2.asp"-->
<!--#include Virtual = "/include/OpenConn.asp"-->
<!--#include Virtual = "/include/ControlDateVB.asp"-->
<!--#include Virtual = "/util/portallib.asp"-->
<!--#include Virtual= "/include/SysFunction.asp"-->
<!--#include Virtual = "/util/dbutil.asp"-->

<script LANGUAGE="Javascript">

//include del file per fare i controlli sulla validit delle date
<!--#include Virtual = "/Include/ControlDate.inc"-->

//include del file per fare i controlli sulla numericit dei campi
<!--#include Virtual = "/Include/ControlNum.inc"-->

//include del file per fare i controlli sulla validit del CF
<!--#include Virtual = "/Include/ControlCodFisc.inc"-->

//include del file per fare i controlli sulle stringhe
<!--#include Virtual = "/Include/ControlString.inc"-->
<!--#include Virtual = "/Include/help.inc"-->

</script>

<!--#include Virtual = "/include/DecCod.asp"-->
<!--#include Virtual="/util/dbUtil.asp"-->

<%
'******************************
sub ctrlEsistenzaPersonaDomIscr()

EsitoEsistenzaDomanda = "OK"

set rsDomanda = Server.CreateObject("ADODB.Recordset")


 sSQL = "SELECT DT_DOM,DESC_BANDO" &_
		" FROM DOMANDA_ISCR A, BANDO B, PROGETTO C" &_
		" WHERE ID_PERSONA = " & IdPers &_
		" AND A.ID_BANDO=B.ID_BANDO" &_
		" AND B.ID_PROJ=C.ID_PROJ" &_
		" AND AREA_WEB='" & mid(Session("Progetto"),2) & "'"

sSQL = TransformPLSQLToTSQL (sSQL) 
set rsDomanda = CC.Execute(sSQL)

if rsDomanda.EOF  then 
   EsitoEsistenzaDomanda = "KO"
   rsDomanda.Close
   set rsDomanda = nothing
   exit sub   
end if

sDtDom = ConvDateToString(rsDomanda("DT_DOM"))
sDecod = rsDomanda("DESC_BANDO")

rsDomanda.Close
set rsDomanda = nothing

end sub
'************************************
sub InserimentoDomandaIscrizione()

    sEsitoDomandaIscrizione ="OK"

    sSQL = "INSERT INTO DOMANDA_ISCR " &_
	       "(ID_PERSONA,"              &_
	        "ID_SEDE,"                &_
	        "ID_BANDO,"                &_
	        "DT_DOM,"                  &_ 
	        "DT_TMST,"                 &_
	        "ESITO_GRAD)"              &_
	        " VALUES ("                &_
			 IdPers & ","            &_ 
		     IdSede & ","            &_ 
		     sIdBando & ","              &_ 
		     ConvDateToDb(Now()) & "," &_ 
		     ConvDateToDb(Now()) 
		     
	if sGraduatoria = "S" then
	    sSQL =sSQL &  ", NULL" 
	else
 		sSQL =sSQL &  ",'S'"
	end if
	sSQL =sSQL &  ")"

'PL-SQL * T-SQL  
sSQL = TransformPLSQLToTSQL (sSQL) 
						
	Errore=EseguiNoC(sSQL,CC)
	
    if Err <> "0" then
        CC.RollbackTrans
	    MessaggioErrori= Err.Description 
	    sEsitoDomandaIscrizione ="KO"
    end if

end sub
'*********************************
sub crtlEsistenzaDatiBando()
dim sSQL, sSqlTab, ContaRecord, x, sIdTab, sSQLRis

EsistenzaDatiBando = "OK"

sSQL = "SELECT NOME_SK, PRG_SK, ID_TAB " &_
		"FROM " &_
		"AREA_ISCRIZIONE WHERE ID_BANDO = " & sIdBando &_
		" AND ID_TAB<>'DOMANDA_ISCR'"
		
				
sSQL = TransformPLSQLToTSQL (sSQL) 
set rstLista = CC.Execute(sSQL)

	if not rstLista.EOF then
		x = 0
		do until rstLista.EOF
			
			sIdTab = rstLista("ID_TAB")
		
			IF sIdTab ="IQ_RISULTATO" THEN
			
					sSQLRis =	"SELECT a.id_infoquest, i.desc_quest, i.fl_repeat, a.cod_destinatario"
					sSQLRis = sSQLRis & " FROM info_quest i, ass_quest a"
					sSQLRis = sSQLRis &	" WHERE (a.cod_ruofu ='" & Session("Ruofu") & "' or a.cod_ruofu is null)" &_
									" AND (a.cod_rorga ='" & Session("rorga") & "' or a.cod_rorga is null)" &_
									" AND a.id_infoquest=i.id_infoquest and cod_destinatario is not null"	
					sSQLRis =	sSQLRis & " AND a.id_bando = " & sIdBando 					
					sSQLRis =	sSQLRis & " ORDER BY a.id_infoquest"	
					
					'Response.Write "sSQLRis " & sSQLRis
					'Response.end

					sSQLRis = TransformPLSQLToTSQL (sSQLRis) 
					set rsQuest = CC.Execute(sSQLRis)

						IF NOT rsQuest.eof THEN
		
							do until rsQuest.EOF
			
								sSqlTab = "Select id_destinatario, id_infoquest from " & sIdTab & " where id_infoquest = " & rsQuest("id_infoquest") & " and id_destinatario = " & IdPers

								
								sSqlTab = TransformPLSQLToTSQL (sSqlTab)
								 
								set rsConta = CC.Execute(sSqlTab)

								ContaRecord = rsConta.Recordcount
		
								If ContaRecord = 0 then
									EsistenzaDatiBando = "KO"
									if x = 0 then
										Mensaje = "Le informazioni ancora da inserire al fine di consentire l'iscrizione al bando sono: " & sDescr 
									else
										Mensaje = Mensaje & ", " & sDescr 
									end if
									x = x + 1
									
								end if
			
								rsConta.Close 
								set rsConta = nothing

			
								rsQuest.movenext
							loop

						END IF
			else
				sSqlTab = "Select id_persona from " & sIdTab & " where id_persona = " & IdPers
				
				sSqlTab = TransformPLSQLToTSQL (sSqlTab) 
				set rsConta = CC.Execute(sSqlTab)

				ContaRecord = rsConta.Recordcount
			
				If ContaRecord = 0 then
					EsistenzaDatiBando = "KO"
					rsConta.Close
					set rsConta = nothing
					rstLista.Close 
					set rstLista = nothing
					exit sub	
				end if
			
				rsConta.Close 
				set rsConta = nothing
			
			end if
			
			
			rstLista.movenext
		loop
		
		rstLista.Close 
		set rstLista = nothing
	end if
	
end sub
'***************************************
sub VisualizzaMessaggi()


if EsitoEsistenzaDomanda = "KO" then
	if MessaggioErrori <> 0 then 
	%>
			<br>
				<br>	
				<table border="0" cellspacing="2" cellpadding="1" width="500">
					<tr align="middle">
						<td class="tbltext3" width="500">
							Ingreso de datos no efectuado. 
						</td>
					</tr>
					<tr align="middle">
						<td class="tbltext3">
							<%
							Response.Write ("Ha ocurrido un error")
							Response.Write "<br>" & MessaggioErrori 
							%> 
						</td>
					</tr>
				</table>
				<br>
				<table WIDTH="500" BORDER="0" CELLPADDING="0" CELLSPACING="0">
					<tr align="center">
						<td>
							<a href="javascript:history.back()"><img src="<%=Session("progetto")%>/images/Indietro.gif" border="0"></a>
						</td>
					</tr>
				</table>
				
	<%
		CC.RollbackTrans
	else
		
	%>
				<br>
				<br>		
				<br>
				<form name="Refresh" method="post" action="UTE_VisBando.asp">
	 	
					<table border="0" cellspacing="2" cellpadding="1" width="500">
					  <tr>
					 	<td align="center"> 
								<input type="hidden" name="IdPers" value="<%=IdPers%>">
					 	</td>
					  </tr>
					</table>
					
					<script>
						alert("Ingreso correctamente efectuado");
						Refresh.submit()
									
					</script>	
				 </form>						
	<%	

	end if
else 
%>
				<form name="Volver" method="post" action="UTE_VisBando.asp">
	 	
					<table border="0" cellspacing="2" cellpadding="1" width="500">
					  <tr>
					 	<td align="center"> 
								<input type="hidden" name="IdPers" value="<%=IdPers%>">
					 	</td>
					  </tr>
					</table>
					
					<script>
						alert("Registro existente. \nNo se ha ingresado el registro porque crear�a duplicados en la base de datos");
						Volver.submit()
									
					</script>	
				 </form>	
<%
end if


end sub
'****************************
sub CorpoProgramma()

ctrlEsistenzaPersonaDomIscr()

if EsitoEsistenzaDomanda = "OK" then exit sub
	
	crtlEsistenzaDatiBando()
   
	if EsistenzaDatiBando="KO" then exit sub
	
	CC.beginTrans	
	
	InserimentoDomandaIscrizione()
	
	if sEsitoDomandaIscrizione ="KO" then exit sub
	

CC.CommitTrans

end sub
'***************MAIN**************************
dim IdPers, sCognome,sNome, sCodFisc, sIdBando, IdSede, sGraduatoria
dim EsistenzaDatiBando, sEsitoDomandaIscrizione, EsitoEsistenzaDomanda
dim MessaggioErrori


IdPers=Request("IdPersona")
sIdBando = Request("IdBando")
IdSede = Request("cmbEnteOcc")

'''''''sgraduatoria''''''
sSQL = " SELECT B.ID_PROJ, B.DESC_BANDO, B.IDGRUPPO, P.IND_GRAD  FROM BANDO B, PROGETTO P " & _
			   " WHERE B.ID_PROJ = P.ID_PROJ "	 &_
			   " AND B.ID_BANDO = "& sIdBando
			  
sSQL = TransformPLSQLToTSQL (sSQL) 
set rsAbil = CC.Execute(sSQL)
	if not rsAbil.EOF then
		sGraduatoria = rsAbil("IND_GRAD")
	end if
	
rsAbil.close
set rsAbil = nothing	 
'''''''''''''''''''''''''''''''''''''''
CorpoProgramma()
VisualizzaMessaggi()

  %><!--#include virtual = "/include/CloseConn.asp"-->
