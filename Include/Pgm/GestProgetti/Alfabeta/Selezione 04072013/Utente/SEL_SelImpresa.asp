<%if session("progetto")="" then %>
     <script>
		alert("Sessione scaduta")
		self.close()
	</script>
<%  Response.end 
 end if
%>

<!-- #include virtual="/util/dbutil.asp" -->
<!-- #include virtual="/include/openconn.asp" -->
<!--#include Virtual = "/include/DecCod.asp"-->

<html>
<head>
<title>Selezione sede</title>
<link REL="STYLESHEET" TYPE="text/css" HREF="<%=session("progetto")%>/fogliostile.css">

<script language="javascript">
<!-- #include virtual="/include/help.inc" -->



function InviaSelezione(idArea,Denominazione,sNomeCampo,Ragione){
		
		
	//TestoRagione = frmFinalizarBando.cbbRagione.options[frmSelImpresa.cbbRagione.selectedIndex].text
		
	opener.document.frmFinalizarBando.cmbEnteOcc.value = idArea;
	
	var sDen = Denominazione.replace("$", "'");
  
	//opener.document.frmFinalizarBando.txtDescArea.value = TestoRagione + "-" + sDen;
	opener.document.frmFinalizarBando.txtDescArea.value = sDen;
	
	self.close()
}

function RimuoviSelezione()
{
    opener.document.frmFinalizarBando.txtDescArea.value ="";
    opener.document.frmFinalizarBando.cmbEnteOcc.value ="";
}



function Chiudi()
{
	self.close()
}

function ControllaSocMedia(){
	if (frmSelImpresa.cbbSocMedia.value!="")
	{
		frmSelImpresa.cbbAgeInter.disabled=true;
	}
	else
	{
		frmSelImpresa.cbbAgeInter.disabled=false;	
	}
}

function ControllaAgeInter(){
	if (frmSelImpresa.cbbAgeInter.value!="")
	{
		frmSelImpresa.cbbSocMedia.disabled=true;
	}
	else
	{
		frmSelImpresa.cbbSocMedia.disabled=false;	
	}
}

</script>
</head>
<%
sCodTimpr= Request("Timpr")
sDescTimpr= Request("DescTimpr")

sArea= Request("Area")
sCampo= Request("sTxtArea")
sNomeCampo= Request("NomeCampo")
sNomeCampo2= Request("NomeCampo2")
nbando=Request("nBando")
%>

<body class="sfondocentro"> 
<form name="frmSelImpresa" method="post" action"SEL_SelImpresa.asp">

<input type="hidden" name="Area" value="<%=sArea%>">
<input type="hidden" name="sTxtArea" value="<%=sCampo%>">
<input type="hidden" name="NomeCampo" value="<%=sNomeCampo%>">
<input type="hidden" name="txtProv" value="<%=sProv%>">
<input type="hidden" name="txtScelta" value="<%=Scelta%>">
<input type="hidden" name="txtRagione" value="<%=Ragione%>">
<input type="hidden" name="NomeCampo2" value="<%=sNomeCampo2%>">

<table border="0" CELLPADDING="0" CELLSPACING="0" width="380">

<tr height="17">
	<td class="sfondomenu" width="67%" height="18"><span class="tbltext0">
		<b>&nbsp;SELEZIONE SEDE</b>
	</td>
	<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">
	</td>
	<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
</tr>
<tr>
	<td class="sfondocommaz" width="57%" colspan="3">
		Selezionare un'opzione per i campi sottostanti e 
		scegliere l'Impresa dall'elenco che verr� visualizzato.<br>
		Premere <b>Chiudi</b> per non selezionare nulla. 
		<a href="Javascript:Show_Help('/Pgm/help/GestProgetti/Alfabeta/Selezione/SEL_SelImpresa')">
		<img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0" onmouseover="javascript:window.status='' ; return true"></a> 
	</td>
</tr>
<tr height="2">
	<td class="sfondocommaz" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif">
	</td>
</tr>
</table>

<table border="0" CELLPADDING="2" CELLSPACING="2" width="380">
	<tr>
		<td>
		&nbsp;
		</td>
	</tr>
	<tr>
		<td class="tbltext1" colspan="2" align="center" nowrap>
			<b><%=sDescTimpr%></b>
		</td>
	</tr>
	<tr>
		<td align="center" colspan="2" class="textblacka">
		</td>
	</tr>
	
	<tr>
		<td class="tbltext1" colspan="1">
			<b>Sede:</b>
		</td>
	</tr>

<%

	sSQL = "SELECT ID_SEDE,DESCRIZIONE FROM SEDE_IMPRESA A, IMPRESA B WHERE A.ID_IMPRESA=B.ID_IMPRESA AND COD_TIMPR='" & sCodTimpr & "' " &_
		       " AND EXISTS (SELECT B.ID_SEDE " &_
							" FROM SEDE_IMPRESA B, BANDO_POSTI C " &_
							" WHERE B.ID_SEDE=C.ID_SEDE" &_
							" AND B.ID_SEDE=A.ID_SEDE" &_
							" AND ID_BANDO=" & nBando & ") " &_	
							" ORDER BY DESCRIZIONE"
	'Response.Write sSQL
							
	Set rsArea = CC.Execute(sSQL)
	

	Do While Not rsArea.EOF %>
	<tr>
		<td nowrap width="190" class="tblsfondo">
			<a class="textblack" href="Javascript:InviaSelezione('<%=rsArea("ID_SEDE")%>','<%=replace(rsArea("DESCRIZIONE"),"'","$")%>','<%=sNomeCampo%>','<%=Ragione%>')"><b><%=ucase(rsArea("DESCRIZIONE"))%></b></a>
		</td>
		<%rsArea.MoveNext%>
		<td nowrap width="190" class="tblsfondo">
		<%if not rsArea.EOF then%>
			<a class="textblack" href="Javascript:InviaSelezione('<%=rsArea("ID_SEDE")%>','<%=replace(rsArea("DESCRIZIONE"),"'","$")%>','<%=sNomeCampo%>','<%=Ragione%>')"><b><%=ucase(rsArea("DESCRIZIONE"))%></b></a>
				<%rsArea.MoveNext%>
		<%else%>
			&nbsp;
		<%end if%>
		</td>
	</tr>
<%
   loop
   rsArea.Close
   set rsArea = nothing	


%>	
	<tr>
		<td colspan="2" align="center">
			<a class="textred" href="Javascript:RimuoviSelezione()"><b>Rimuovi Selezione</b></a>
		</td>
	</tr>
</table>
</form>

<table width="380" cellspacing="2" cellpadding="1" border="0">
	<tr align="center">
		<td>
			<a href="javascript: Chiudi()"><img src="<%=Session("progetto")%>/images/chiudi.gif" title="Chiudi la pagina" border="0" align="absBottom"></a>
		</td>
	</tr>		
</table>
</body>
</html>
<!-- #include virtual="/include/closeconn.asp" -->
