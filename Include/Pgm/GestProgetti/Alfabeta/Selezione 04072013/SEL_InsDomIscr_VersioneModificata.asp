<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%'Option Explicit 
  ' Response.ExpiresAbsolute = Now() - 1 
  ' Response.AddHeader "pragma","no-cache"
  ' Response.AddHeader "cache-control","private"
  ' Response.CacheControl = "no-cache"
%>
<!-- #include virtual="/strutt_testa2.asp"-->
<!--#include virtual ="/include/openconn.asp"-->
<!--#include virtual ="/util/portallib.asp"-->
<%
'if ValidateService(session("idutente"),"SEL_INSDOMISCR", CC) <> "true" then 
'	response.redirect "/util/error_login.asp"
'end if
Session("menu")= ""
%>
<!-- ************** Javascript Inizio ************ -->

<script LANGUAGE="Javascript">

<!--#include virtual="/Include/help.inc"-->

<!--#include virtual = "/include/SelComune.js"-->

//include del file per fare i controlli sulla validit� delle date
<!--#include virtual = "/Include/ControlDate.inc"-->

//include del file per fare i controlli sulla numericit� dei campi
<!--#include virtual = "/Include/ControlNum.inc"-->

//include del file per fare i controlli sulla validit� del codice fiscale
<!--#include virtual = "/Include/ControlCodFisc.inc"-->

//include del file per fare i controlli sulle stringhe
<!--#include virtual = "/Include/ControlString.inc"-->

var windowArea

function SelImpresa(sCdTimp,sDesCdTimp,sArea,sCampo,sNomeCampo,sNomeCampo2,nBando){
		windowArea = window.open ('SEL_SelImpresa.asp?Timpr=' + sCdTimp + '&DescTimpr=' + sDesCdTimp + '&Area=' + sArea + '&sTxtArea=' + sCampo +'&NomeCampo=' + sNomeCampo +'&NomeCampo2=' + sNomeCampo2+'&nBando='+ nBando,'Info','width=425,height=450,Resize=No,Scrollbars=yes');	
}

function PulisciCom()
{
	document.frmDomIscri.txtComuneNascita.value = ""
	document.frmDomIscri.txtComune.value = ""
}

function PulisciRes()
{
	document.frmDomIscri.txtComuneRes.value = ""
	document.frmDomIscri.txtComRes.value = ""
}

//----------------------------------------------------------------------------------------------------------------------------------------------------------


//Funzione per i controlli dei campi da inserire 

//-------- Controlli di OBBLIGATORIETA' --------
//-------- Controlli FORMALI            --------
//-------- Controlli di RELAZIONE       --------

	function ControllaDati(frmDomIscri){
	
        //Legge privacy
        if (windowArea != null ) 
	    {
		windowArea.close(); 
	    }

        if (frmDomIscri.radConsenso[1].checked) 
	    {	
		f = '/ISI/Pgm/Iscr_Utente/UTE_Leg_Privacy.asp';
		windowArea=window.open(f, '','width=520,height=300,left=250,top=250,Resize=no,scrolling=no');
		window.event.returnValue = false;
		return false
	    }

		//Cognome
		frmDomIscri.txtCognome.value=TRIM(frmDomIscri.txtCognome.value)
		if (frmDomIscri.txtCognome.value == ""){
			alert("Cognome obbligatorio")
			frmDomIscri.txtCognome.focus() 
			return false
		}
		//Nome
		frmDomIscri.txtNome.value=TRIM(frmDomIscri.txtNome.value)
		if (frmDomIscri.txtNome.value == ""){
			alert("Nome obbligatorio")
			frmDomIscri.txtNome.focus() 
			return false
		}
		//Data di Nascita
		DataNascita = TRIM(frmDomIscri.txtDataNascita.value)
		if (DataNascita == ""){
			alert("Data di Nascita obbligatoria")
			frmDomIscri.txtDataNascita.focus() 
			return false
		}
		//Controllo della validit� della Data di Nascita
		if (!ValidateInputDate(DataNascita)){
			frmDomIscri.txtDataNascita.focus() 
			return false
		}
		//Comune di Nascita e Provincia di Nascita obbligatori
		//e in alternativa con Nazione di Nascita
		frmDomIscri.txtComuneNascita.value=TRIM(frmDomIscri.txtComuneNascita.value)
		if (frmDomIscri.txtComuneNascita.value == ""){ 
			if (frmDomIscri.cmbProvinciaNascita.value == ""){
				if (frmDomIscri.cmbNazioneNascita.value == ""){
					alert("Comune e Provincia di Nascita o Nazione di Nascita obbligatori")
					frmDomIscri.cmbProvinciaNascita.focus() 
					return false
				}
			}
		}
       
		if (frmDomIscri.txtComuneNascita.value != ""){ 
			if (frmDomIscri.cmbProvinciaNascita.value != ""){
				if(frmDomIscri.cmbNazioneNascita.value != ""){
					alert("Indicare solo il Comune e la Provincia di Nascita oppure la Nazione di Nascita")
					frmDomIscri.cmbNazioneNascita.focus() 
					return false
				}
			}	
		}
		//Se Comune di Nascita � digitato, la Provincia � obbligatoria
		if (frmDomIscri.txtComuneNascita.value != ""){
			if (frmDomIscri.cmbProvinciaNascita.value == ""){
				alert("Provincia di Nascita obbligatoria")
				frmDomIscri.cmbProvinciaNascita.focus() 
				return false
			}
		}
		//Se la Provincia di Nascita � digitata, il Comune � obbligatorio
		if  (frmDomIscri.txtComuneNascita.value == ""){
			if (frmDomIscri.cmbProvinciaNascita.value != ""){
				alert("Comune di Nascita obbligatorio")
				frmDomIscri.txtComuneNascita.focus() 
				return false
			}
		}
		//Sesso
		if (frmDomIscri.cmbSesso.value == ""){
			alert("Sesso obbligatorio")
			frmDomIscri.cmbSesso.focus() 
			return false
		}
		//Codice Fiscale
		frmDomIscri.txtCodFisc.value = TRIM(frmDomIscri.txtCodFisc.value)
		if (frmDomIscri.txtCodFisc.value == ""){
			alert("Codice Fiscale obbligatorio")
			frmDomIscri.txtCodFisc.focus() 
			return false
		}
		//Codice Fiscale deve essere di 16 caratteri
		if (frmDomIscri.txtCodFisc.value.length != 16){
			alert("Formato del Codice Fiscale errato")
			frmDomIscri.txtCodFisc.focus() 
			return false
		}
		//Contollo la validit� del Codice Fiscale
		var contrCom=""
		DataNascita = frmDomIscri.txtDataNascita.value;
		CodFisc = frmDomIscri.txtCodFisc.value.toUpperCase();
		Sesso = frmDomIscri.cmbSesso.value;
		
		if (frmDomIscri.txtComuneNascita.value != ""){
		          contrCom =frmDomIscri.txtComune.value;   
		}
		else{
		          contrCom = frmDomIscri.cmbNazioneNascita.value
		}
		
		if (!ControllCodFisc(DataNascita,CodFisc,Sesso,contrCom)){
			alert("Codice Fiscale Errato");
			frmDomIscri.txtCodFisc.focus(); 
			return false
		}
		//Cittadinanza
		if (frmDomIscri.cmbCittadinanza.value == ""){
			alert("Cittadinanza obbligatoria")
			frmDomIscri.cmbCittadinanza.focus() 
			return false
		}
		
		//Recapito Telefonico
		if (frmDomIscri.txtNumTel.value == ""){
			alert("Recapito Telefonico obbligatorio")
			frmDomIscri.txtNumTel.focus() 
			return false
		}
		//Recapito Telefonico deve essere numerico
		if (!IsNum(frmDomIscri.txtNumTel.value)){
			alert("Il Recapito Telefonico deve essere Numerico")
			frmDomIscri.txtNumTel.focus() 
			return false
		}
		//Recapito Telefonico deve essere diverso da zero
		if (eval(frmDomIscri.txtNumTel.value) == 0){
			alert("Il Recapito Telefonico non pu� essere zero")
			frmDomIscri.txtNumTel.focus()
			return false
		}
		//Controllo Formale EMail
          frmDomIscri.txtEMail.value = TRIM(frmDomIscri.txtEMail.value);
          if (frmDomIscri.txtEMail.value.length	> 0) {
              if (ValidateEmail(frmDomIscri.txtEMail.value) != true) {
                 alert("L'indirizzo EMail non e' formalmente corretto.")
			     frmDomIscri.txtEMail.focus()
			     return false
              }
          }	
		
		//Indirizzo di Residenza
		frmDomIscri.txtIndirizzo.value=TRIM(frmDomIscri.txtIndirizzo.value)
		if  (frmDomIscri.txtIndirizzo.value == ""){
			alert("Indirizzo obbligatorio")
			frmDomIscri.txtIndirizzo.focus() 
			return false
		}
		frmDomIscri.txtFrazione.value=TRIM(frmDomIscri.txtFrazione.value)
		//Comune di Residenza
		frmDomIscri.txtComRes.value=TRIM(frmDomIscri.txtComRes.value)
		if (frmDomIscri.txtComRes.value == ""){
			alert("Comune di Residenza obbligatorio")
			frmDomIscri.cmbProvRes.focus() 
			return false
		}
		//Provincia di Residenza
		if (frmDomIscri.cmbProvRes.value == ""){
			alert("Provincia di Residenza obbligatoria")
			frmDomIscri.cmbProvRes.focus() 
			return false
		}
		//CAP obbligatorio
		frmDomIscri.txtCAP.value = TRIM(frmDomIscri.txtCAP.value) 
		if (frmDomIscri.txtCAP.value == ""){
			alert("CAP obbligatorio")
			frmDomIscri.txtCAP.focus() 
			return false
		}
		//CAP deve essere numerico
		if (frmDomIscri.txtCAP.value != ""){
			if (!IsNum(frmDomIscri.txtCAP.value)){
				alert("Il CAP deve essere Numerico")
				frmDomIscri.txtCAP.focus() 
				return false
			}
		}
		//CAP deve essere diverso da zero
		if (frmDomIscri.txtCAP.value != ""){
			if (eval(frmDomIscri.txtCAP.value) == 0){
				alert("Il CAP non pu� essere zero")
				frmDomIscri.txtCAP.focus()
				return false
			}
		}
		//CAP deve essere di 5 caratteri
		if (frmDomIscri.txtCAP.value != ""){
			if (frmDomIscri.txtCAP.value.length != 5){
				alert("Formato del CAP errato")
				frmDomIscri.txtCAP.focus() 
				return false
			}
		}
		
		//Data di Iscrizione non deve essere superiore alla Data Odierna
		sOggi = frmDomIscri.datasis.value;

		//Titolo di Studio
		if (frmDomIscri.cmbTitStud.value == ""){
			alert("Titolo di Studio obbligatorio")
			frmDomIscri.cmbTitStud.focus() 
			return false
		}
		//Anno Rilascio Titolo obbligatorio
		if ((frmDomIscri.txtAnnoRilascio.value == "")){
			alert("L'Anno di Rilascio del Titolo e' obbligatorio")
			frmDomIscri.txtAnnoRilascio.focus()
			return false
		}
		//Anno Rilascio Titolo numerico
		if (!IsNum(frmDomIscri.txtAnnoRilascio.value)){
			alert("L'Anno di Rilascio del Titolo deve essere numerico")
			frmDomIscri.txtAnnoRilascio.focus()
			return false
		}
		//Anno Rilascio Titolo deve essere di 4 caratteri
		if (frmDomIscri.txtAnnoRilascio.value.length != 4){
			alert("Indicare l'Anno di Rilascio del Titolo nel formato AAAA")
			frmDomIscri.txtAnnoRilascio.focus() 
			return false
			}
		//Anno Rilascio Titolo deve essere diverso da zero 
		if (eval(frmDomIscri.txtAnnoRilascio.value) == 0){
			alert("L'Anno di Rilascio del Titolo non pu� essere zero")
			frmDomIscri.txtAnnoRilascio.focus()
			return false
		}
		//Anno Rilascio Titolo deve essere superiore all'anno di nascita
		sAnno = frmDomIscri.txtDataNascita.value
		sAnno = sAnno.substring(6,10)
		sAnno1 = frmDomIscri.txtAnnoRilascio.value
		if (!(sAnno1 > sAnno)){
			alert("L'Anno di Rilascio del Titolo deve essere superiore all'Anno di Nascita")
			frmDomIscri.txtAnnoRilascio.focus()
			return false
		}
		//Anno Rilascio Titolo deve essere minore all'anno-oggi
		sOggi = frmDomIscri.datasis.value;
		sAnno = sOggi.substring(6,10)
		//alert ("Anno sistema - anno rilascio titolo=" + sAnno)
		//anno = new Date();
		//sAnno = anno.getYear();
		if (frmDomIscri.txtAnnoRilascio.value > sAnno){
			alert("L'Anno di Rilascio del Titolo non pu� essere superiore all'Anno corrente")
			frmDomIscri.txtAnnoRilascio.focus()
			return false
		}
				
		if (frmDomIscri.txtCodTimpr.value !="" ) {
			frmDomIscri.txtDescArea.value = TRIM(frmDomIscri.txtDescArea.value);
		    if (frmDomIscri.txtDescArea.value == "") {
				var DescTI = frmDomIscri.txtDescTimpr.value;
				alert("Selezionare '" +  DescTI + "'");
				return false;
			}	
		}	
			
		//Controlli su LOGIN: posso inserire solo numeri e / o lettere
		   frmDomIscri.txtLogin.value=TRIM(frmDomIscri.txtLogin.value)
		var anyString = frmDomIscri.txtLogin.value;
		  //  anyString =TRIM(anyString)
		if (frmDomIscri.txtLogin.value == ""){
			alert("Login obbligatoria")
			frmDomIscri.txtLogin.focus() 
			return false
		}
					
		for (var i=0; i<=anyString.length-1; i++){
			if(((anyString.charAt(i) >= "A") && (anyString.charAt(i) <= "Z")) || 
				((anyString.charAt(i) >= "a") && (anyString.charAt(i) <= "z")) || 
				((anyString.charAt(i) >= "0") && (anyString.charAt(i) <= "9")))
			{
			}
			else
			{		
				frmDomIscri.txtLogin.focus();
			 	alert("Inserire nel campo Login un parametro valido!");
				return false
			}		
		}
		
		if (frmDomIscri.txtPassword.value == "")
		{
			alert("Il campo Password � obbligatorio!")
			frmDomIscri.txtPassword.focus() 
			return false
		}
		
		if (frmDomIscri.txtConfPassword.value == "")
		{
			alert("Il campo Conferma Password � obbligatorio!")
			frmDomIscri.txtConfPassword.focus() 
			return false
		}
		
		if (frmDomIscri.txtPassword.value != frmDomIscri.txtConfPassword.value)
		{
			alert("Il campo Conferma Password deve essere uguale al campo Password")
			frmDomIscri.txtPassword.focus() 
			return false
		}
			
			
				
						
	return true
	}

//----------------------------------------------------------------------------------------------------------------------------------------------------------	
	
</script>

<!-- ************** Javascript Fine   ************ -->

<!-- ************** ASP Inizio *************** -->

<!--#include virtual = "/include/DecCod.asp"-->
<!--#include virtual = "/include/ControlDateVB.asp"-->
<!--#include virtual = "/util/dbutil.asp"-->

<%	
'-------------------------------------------------------------------------------------------------------------------------------
Sub Titolo(sTitolo,sDescTitolo) %>
    <table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
		<tr height="18">
			<td class="sfondomenu" width="67%" height="18"><span class="tbltext0"><b>&nbsp;<%=sTitolo%></b></span></td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
			<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
		</tr>
	</table>
	<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
		<tr>
			<td align="left" class="sfondocomm"><%=sDescTitolo%></td>
		</tr>
		<tr height="2">
			<td class="sfondocomm" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>
	<br>
<%
end sub

Sub Msgetto(Msg)
%>
	<br>
	<table align="center">
		<tr>
			<td class="tbltext3"><b><%=Msg%></b></td>
		</tr>	
	</table>
<%
End Sub

'-------------------------------------------------------------------------------------------------------------------------------

sub Inizio()
%>
	<!--table border="0" width="520" cellspacing="0" cellpadding="0" height="81">	   <tr>	     <td width="500" background="<%=Session("Progetto")%>/images/titoli/strumenti2b.gif" height="81" valign="bottom" align="right">	       <table border="0" width="260" height="30" cellspacing="0" cellpadding="0">	         <tr>	           <td width="100%" valign="top" align="right">	           <b class="tbltext1a">Domando di iscrizione</b></td>	         			</tr>	       </table>	     </td>	   </tr>	</table-->
	<br>
	<table cellpadding="0" cellspacing="0" width="500" border="0">
		<tr height="18">
			<td class="sfondomenu" height="18" width="67%">
			<span class="tbltext0"><b>&nbsp;DOMANDA DI ISCRIZIONE</b></span></td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
			<td valign="middle" align="right" class="tbltext1" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">(*) campi obbligatori</td>
		</tr>
		<tr>
			<td class="sfondocomm" width="57%" colspan="3">
				Modulo di iscrizione 
			<%if  sEsitoBando = "OK" then %>
			      per il bando&nbsp;<b><%=sDescBando%></b><br>  
			      Premere <b>Invia</b> per salvare. 
			<%end if%>
			<a href="Javascript:Show_Help('/Pgm/help/GestProgetti/Alfabeta/Selezione/SEL_InsDomIscr')" onmouseover="javascript:window.status=' '; return true"><img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
			</td>
		</tr>
		<tr height="2">
			<td colspan="3" class="sfondocomm" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>
	
	<form method="post" name="frmDomIscri" onsubmit="return ControllaDati(this)" action="SEL_CnfDomIscr.asp">
<%
end sub

'------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

sub ImpPagina()
    dim sInt 
%>
	<!--table width="500" border="0" cellspacing="2" cellpadding="1">		<tr> 			<td class="tbltext3" colspan="2">				<b>Modulo di Iscrizione per il bando <%=sDescBando%></b>			</td>		</tr>	    <tr><td>&nbsp;</td></tr>	</table -->
	    <% 
	       sCommento =  "Presta molta attenzione a quanto inserisci:<br>" &_ 
				        "Indica di seguito i tuoi dati anagrafici."  
				        
	       call Titolo("Informazioni Personali",sCommento)
	    %>
	    <!--tr>	        <td align="left" class="tbltext3" colspan="2">	        	<b>Informazioni Personali</b>			</td>	        	    </tr>	    <tr>			<td align="left" class="textblack" colspan="2">				Presta molta attenzione a quanto inserisci: 				i tuoi dati anagrafici e occupazionali verranno confrontati con 				quelli in possesso del Centro per l'Impiego a cui sei iscritto.				<br>Ogni incongruenza puo' comportare la tua esclusione dalla 				graduatoria del progetto.			</td>	    </tr-->
	
	
		<table width="500" border="0" cellspacing="2" cellpadding="1">
		<tr>
			<td align="left" class="tbltext1" width="30%">
				<b>Cognome*</b>
			</td>
			<td align="left">
				<input style="TEXT-TRANSFORM: uppercase;" size="30" maxlength="30" class="textblack" name="txtCognome">
			</td>
	    </tr>
	    <tr>
			<td align="left" class="tbltext1" width="30%">
				<b>Nome*</b>
			</td>
			<td align="left">
				<input style="TEXT-TRANSFORM: uppercase;" size="30" maxlength="30" class="textblack" name="txtNome">
			</td>
	    </tr>

	    <tr>
			<td align="left" class="tbltext1" width="30%">
				<b>Data di Nascita*</b> (gg/mm/aaaa)
			</td>	
			<td align="left">
				<input style="TEXT-TRANSFORM: uppercase;" size="11" maxlength="10" class="textblack" name="txtDataNascita">
			</td>
	    </tr>
	    <tr><td>&nbsp;</td></tr>
	    <tr>
			<td align="left" class="textblack" colspan="2">
				Inserisci qui di seguito il comune e la provincia di nascita, se sei nato in Italia.
			</td>
	    </tr>
	    
		<tr>		
			<td align="left" class="tbltext1" width="30%">
				<b>Provincia di Nascita*</b>
			</td>			
			<td align="left">				
<%
			sInt = "PROV|0|" & date & "| |cmbProvinciaNascita' onchange='PulisciCom()|ORDER BY DESCRIZIONE"			
			CreateCombo(sInt)
						
%>
			</td>
		</tr>		
		<tr>		
			<td align="left" class="tbltext1" width="30%">
				<b>Comune di Nascita*</b>
			</td>
			<td nowrap>
				<span class="tbltext">
				<input type="text" name="txtComuneNascita" style="TEXT-TRANSFORM: uppercase;" class="textblack" size="35" readonly value="<%=DescrComuneNascita%>">
				<input type="hidden" id="txtComune" name="txtComune" value="<%=COM_NASC%>">
<%
				NomeForm="frmDomIscri"
				CodiceProvincia="cmbProvinciaNascita"
				NomeComune="txtComuneNascita"
				CodiceComune="txtComune"
				Cap="NO"
%>
				<a href="Javascript:SelComune('<%=NomeForm%>','<%=CodiceProvincia%>','<%=NomeComune%>','<%=CodiceComune%>','<%=Cap%>')" ID="imgPunto1" name="imgPunto1" onmouseover="javascript:window.status='';return true"><img border="0" src="<%=Session("Progetto")%>/images/bullet1.gif"></a>
				</span>				
			</td>
		</tr>		
		<tr>
			<td class="textblack" colspan="2">
				Per <b>i residenti all'estero</b> invece compilare lo stato di residenza. 
			</td>
		</tr>
		
	    <tr>
			<td align="left" class="tbltext1" width="30%">
				<b>Nazione di Nascita</b>
	        </td>
			<td align="left">
				<%
				set rsComune = Server.CreateObject("ADODB.Recordset")
				sSQL = "SELECT CODCOM,DESCOM from COMUNE WHERE CODCOM like 'Z%' " &_
						"ORDER BY DESCOM"

'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
				rsComune.Open sSQL, CC
				
				%>
				<select class="textblack" ID="cmbNazioneNascita" name="cmbNazioneNascita">
				<option></option>
				<%
				do while not rsComune.EOF 
					Response.Write "<OPTION "

					Response.write "value ='" & rsComune("CODCOM") & _
						"'> " & rsComune("DESCOM")  & "</OPTION>"
					
					rsComune.MoveNext 
				loop

				rsComune.Close	
				%>
				</select>
	        </td>
		</tr>
		<tr>
			<td align="left" class="tbltext1" width="30%">
				<b>Sesso*</b>
	        </td>
			<td align="left">
				<select class="textblack" id="cmbSesso" name="cmbSesso">
					<option selected>
					<option value="F">FEMMINILE
					<option value="M">MASCHILE
					</option>
				</select>
	        </td>
	  	</tr>
	  	<tr>
			<td align="left" class="tbltext1" width="30%">
				<b>Codice Fiscale*</b>
	        </td>
			<td align="left">
				<input style="TEXT-TRANSFORM: uppercase;" size="22" maxlength="16" class="textblack" name="txtCodFisc">
	        </td>
	    </tr>
		<tr>
			<td align="left" class="tbltext1" width="30%">
				<b>Cittadinanza*</b>
	        </td>
			<td align="left">
				<%	sInt = "STATO|0|" & date & "| |cmbCittadinanza|ORDER BY DESCRIZIONE"
					CreateCombo(sInt)	%>
	        </td>
	    </tr>		
		<td><td>&nbsp;</td></tr>  
	    <tr>
			<td align="left" class="textblack" colspan="2">
			Specifica un recapito telefonico presso cui gli operatori possono contattarti direttamente per comunicazioni urgenti.
			Indica di seguito tutti i numeri che lo compongono, senza aggiungere spazi o altri separatori.
			</td>
	    </tr>
	    <tr>
			<td align="left" class="tbltext1" width="30%">
				<b>Recapito Telefonico*</b>
	        </td>
	        <td align="left">
				<input style="TEXT-TRANSFORM: uppercase;" size="25" maxlength="20" class="textblack" name="txtNumTel">
	        </td>
	    </tr>

	    <tr>
			<td align="left" class="textblack" colspan="2">
				e il tuo eventuale indirizzo di posta elettronica personale
			</td>
	    </tr>
	    <tr>
			<td align="left" class="tbltext1" width="30%">
	   			<b>E-Mail</b>
	        </td>
	        <td align="left">
		   		<input size="35" maxlength="30" class="textblack" name="txtEMail">
	        </td>
	    </tr>

	
	    <!--tr>			<td height="2" align="left" colspan="2" background="<%=Session("Progetto")%>/images/separazione.gif"></td>	    </tr-->

		<tr><td>&nbsp;</td></tr>
	    </table>
	     <%  
	       sCommento = "Specifica i tuoi dati di residenza."
	       call Titolo("Residenza",sCommento)%>
	    <!--tr>	        <td align="left" class="tbltext3" colspan="2">	        	<b>RESIDENZA</b>			</td>				    </tr>	    <tr>			<td align="left" class="textblack" colspan="2">			Specifica i tuoi dati di residenza. Ti ricordiamo che per partecipare devi essere residente nelle 8 regioni del Mezzogiorno			</td>	    </tr-->
		
	    <table width="500" border="0" cellspacing="2" cellpadding="1">
	    <tr>
			<td align="left" class="tbltext1" width="30%">
				<b>Indirizzo*</b>
	        </td>
	        <td align="left">
				<input size="30" maxlength="50" name="txtIndirizzo" class="textblack" style="TEXT-TRANSFORM: uppercase;">
	        </td>
	    </tr>
			
	    <tr>
			<td align="left" class="tbltext1" width="30%">
				<b>Frazione</b>
	        </td>
	        <td align="left">
				<input size="30" maxlength="50" name="txtFrazione" class="textblack" style="TEXT-TRANSFORM: uppercase;">
	        </td>
	    </tr>
	    
		<tr>		
			<td align="left" class="tbltext1" width="30%">
				<b>Provincia di Residenza*</b>
			</td>			
			<td align="left">				
<%
			sInt = "PROV|0|" & date & "||cmbProvRes' onchange='PulisciRes()| ORDER BY DESCRIZIONE"			
			CreateCombo(sInt)	
%>
			</td>
		</tr>		
		<tr>		
			<td align="left" class="tbltext1" width="30%">
				<b>Comune di Residenza*</b>
			</td>
			<td nowrap>
				<span class="tbltext">
				<input type="text" name="txtComuneRes" style="TEXT-TRANSFORM: uppercase;" class="textblack" size="35" readonly value="<%=DescrComuneResid%>">
				<input type="hidden" id="txtComRes" name="txtComRes" value="<%=COM_RES%>">
<%
				NomeForm="frmDomIscri"
				CodiceProvincia="cmbProvRes"
				NomeComune="txtComuneRes"
				CodiceComune="txtComRes"
				Cap="txtCAP"
%>
				<a href="Javascript:SelComune('<%=NomeForm%>','<%=CodiceProvincia%>','<%=NomeComune%>','<%=CodiceComune%>','<%=Cap%>')" ID="imgPunto1" name="imgPunto1" onmouseover="javascript:window.status='';return true"><img border="0" src="<%=Session("Progetto")%>/images/bullet1.gif"></a>
				</span>				
			</td>
		</tr>		
		<tr>
			<td align="left" nowrap class="tbltext1" width="30%">
				<b>C.A.P.*</b>
			</td>
			<td align="left">
				<input type="text" name="txtCAP" class="textblack" value="<%=CAP_RES%>" size="5" maxlength="5">
			</td>
		</tr>	    
	    <!--tr>			<td height="2" align="left" colspan="2" background="<%=Session("Progetto")%>/images/separazione.gif"></td>	    </tr-->
	    
	    <tr><td>&nbsp;</td></tr>
	</table>    
	   
<!-- ----------------------------------------------------------- -->
 <%
	if trim(sCodTimpr) = "" or isnull(sCodTimpr) or sCodTimpr="00" or sCodTimpr="0" then
			%><input type="hidden" name="cmbEnteOcc" value="0">
			  <input type="hidden" name="txtCodTimpr" value>
			<%
	else		
			sSQL = "SELECT SI.ID_SEDE, SI.DESCRIZIONE, SI.PRV " & _
					"FROM SEDE_IMPRESA SI, IMPRESA I " & _
					"WHERE I.COD_TIMPR = '" & sCodTimpr & "' " & _
					"AND I.ID_IMPRESA = SI.ID_IMPRESA " & _
					"ORDER BY SI.PRV, SI.DESCRIZIONE"
			set rsImpresa = Server.CreateObject("ADODB.Recordset")
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
			rsImpresa.Open sSQL, CC, 3

			if not rsImpresa.EOF then
				
				   call Titolo(sDescTimpr,"Seleziona la struttura di appartenenza.")
				%>	    
    
            <!--table width="500" border="0" cellspacing="2" cellpadding="1">			        <tr>				<td align="left" class="tbltext3">	        		<b><%=sDescTimpr%></b>				</td>			</tr-->
	
			<table width="500" border="0" cellspacing="2" cellpadding="1">
			<tr>
				<td align="left" colspan="2" width="60%" nowrap>
					<span class="tbltext">
					<input type="hidden" name="cmbEnteOcc">
					<input type="hidden" name="txtCodTimpr" value="<%=sCodTimpr%>">	
					<input type="hidden" name="txtDescTimpr" value="<%=sDescTimpr%>">	
										
					<textarea rows="4" cols="40" name="txtDescArea" readonly class="textblacka"></textarea>
				<a href="Javascript:SelImpresa('<%=sCodTimpr%>','<%=sDescTimpr%>','SEDE_IMPRESA','ID_SEDE','txtImpresa','txtRagione','<%=nBando%>')" ID="imgPunto2" name="imgPunto2"><img border="0" src="<%=Session("Progetto")%>/images/bullet1.gif" onmouseover="javascript:window.status='';return true"></a>
				</span>
				</td>
			
			</tr>
			<!--tr>				<td height="2" align="left" colspan="2" background="<%=Session("Progetto")%>/images/separazione.gif"></td>	        </tr-->
    </table>
    <br>
  <%end if
end if  
  %>  
<!-- ----------------------------------------------------------- -->	    	    
	<% 
	    call Titolo("Login e Password","Inserisci di seguito una Login ed una password.")
	%>
	
    <table width="500" border="0" cellspacing="2" cellpadding="1">     
        <tr>
	        <td align="left" class="tbltext1" width="30%">
					<b>Login*</b>
	        </td>
	        <td align="left">
	             <input size="34" class="textblack" maxlength="15" name="txtLogin" style="TEXT-TRANSFORM: uppercase;">
	        </td>
	    </tr>
	    <tr>
	        <td align="left" class="tbltext1" width="30%">
					<b>Password*</b>
	        </td>
	        <td align="left">
	             <input type="password" size="34" class="textblack" maxlength="15" name="txtPassword" style="TEXT-TRANSFORM: uppercase;">
	        </td>
	    </tr>
	    <tr>
	        <td align="left" class="tbltext1" width="30%">
					<b>Conferma Password*</b>
	        </td>
	        <td align="left">
	             <input type="password" size="34" class="textblack" maxlength="15" name="txtConfPassword" style="TEXT-TRANSFORM: uppercase;">
	        </td>
	    </tr>  
	</table>
	<br>
	<table width="520">		
	<tr>
		<td colspan="2" align="center" class="tbltext1" width="500">
			<b>Consenso ai sensi dell'art.11 della legge 31 dicembre 1996<br> n. 675/96</b><br><br>
			Consenso relativo alla legge sulla privacy: vi chiediamo gentilmente di leggere l'informativa relativa 
			alla privacy riportata di seguito e di esprimere<br>il 
			vostro consenso o meno al trattamento dei dati.
		</td>
	</tr>
	<tr>
		<td align="middle" colspan="2">&nbsp;
		</td>
   </tr>
   </table>
<table width="520">   
	<tr>
		<td colspan="2" class="tbltext"><p align="justify">
		
<%
			on error resume next
			'riga sottostante originale
			PathFileEdit = "/ISI/Testi/SistDoc/Sistema Redazionale/Autorizzazioni/info_privacy.htm"
			'PathFileEdit = "/PLAVORO/Testi/SistDoc/Sistema Redazionale/Autorizzazioni/info_privacy.htm"
'PL-SQL * T-SQL  
PATHFILEEDIT = TransformPLSQLToTSQL (PATHFILEEDIT) 
			Server.Execute(PathFileEdit)
			If err.number <> 0 Then
				Response.Write "Errore nella visualizzazione."
				Response.Write " Contattare il Gruppo Assistenza Portale Italia Lavoro"
				Response.Write " all'indirizzo po-assistenza@italialavoro.it"
			End If				
%>
			</p>
		</td>
	</tr>
</table>

<table>	
	<tr>
		<td colSpan="2"><br>
			<center>
			<input type="radio" value="0" id="radConsenso" name="radConsenso"><span class="tbltext">ACCETTO</span>
			<input type="radio" value="1" CHECKED id="radConsenso" name="radConsenso"><span class="tbltext">NON ACCETTO</span>
			<input type="hidden" name="sControllo" value="2">
			</center>
		</td>
	</tr>
</table>
	<br><br>

	<table cellpadding="0" cellspacing="0" width="500" border="0">	
		<tr align="center">
			<td nowrap align="right"><input type="image" name="Conferma" src="<%=Session("progetto")%>/images/conferma.gif"></td value="Conferma">
			<td nowrap align="center"><a href="javascript:document.frmDomIscri.reset()"><img border="0" src="<%=Session("progetto")%>/images/annulla.gif"></td>
			<td nowrap align="left"><a HREF="javascript:history.back()"><img SRC="<%=Session("Progetto")%>/images/indietro.gif" border="0"></td>
		    <td> 
				<input type="hidden" name="graduatoria" value="<%=sGraduatoria%>">
				<input type="hidden" name="gruppo" value="<%=sGruppo%>">
	           	               
	            <input type="hidden" name="IniIsc" value="<%=sDataIsc%>">
	            <input type="hidden" name="datasis" value="<%=sDataSis%>">
	            <input type="hidden" name="bando" value="<%=nBando%>">
	            <input type="hidden" name="descbando" value="<%=sDescBando%>">
	        </td>
		</tr>
	</table>
</form>
	
<%
end sub

'------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

function ctrlProj()
	ctrlProj = ""
	sSQL = "SELECT COD_TIMPR FROM PROGETTO WHERE ID_PROJ = " & CLng(nIdProgetto)
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	set rsProj = CC.Execute(sSQL)
	
	if rsProj.EOF then
		Msgetto("Progetto inesistente")	
		ctrlProj = "KO"
	else
		sCodTimpr = rsProj("COD_TIMPR")
		ctrlProj = "OK"
	end if
	
end function

'------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

sub ctrlBando()

	'	Se la data di sistema non � compresa nel periodo di 
	'   acquisizione domande non devo caricare la domanda.
	set rsAbil = Server.CreateObject("ADODB.Recordset")

	sSQL = " SELECT B.ID_PROJ, B.DESC_BANDO, B.IDGRUPPO, P.IND_GRAD  FROM BANDO B, PROGETTO P " & _
		   "WHERE B.ID_PROJ = P.ID_PROJ "	 &_
			"AND B.ID_BANDO = "& nBando & " AND " & nTime & _
			" BETWEEN B.DT_INI_ACQ_DOM AND B.DT_FIN_ACQ_DOM"
	
	'-----------------------------------------------------------------------------
	' Questo controllo � stato inserito per permettere l'iscrizione di nominativi
	' anche a bando(sport2job) chiuso tale operazione puo essere eseguita solo dagli
	' utenti appartenenti al gruppo ISCR tramite la funzione ISCRIZIONE
	if ucase(nBypassdata) = "SI" then
		sSQL = " SELECT B.ID_PROJ, B.DESC_BANDO, B.IDGRUPPO, P.IND_GRAD  FROM BANDO B, PROGETTO P " & _
			   " WHERE B.ID_PROJ = P.ID_PROJ "	 &_
			   " AND B.ID_BANDO = "& nBando
			    
	end if
    '----------------------------------------------------------------------------
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
   	rsAbil.Open sSQL, CC, 3

	if  rsAbil.EOF  then
		Msgetto("Bando Inesistente o Periodo di Acquisizione domande Scaduto")
		sEsitoBando = "KO"
	else
		sDescBando = rsAbil("DESC_BANDO")
	    nIdProgetto = rsAbil("ID_PROJ")
	    sEsitoBando = "OK"
	    sGruppo =rsAbil("IDGRUPPO")
	    sGraduatoria =rsAbil("IND_GRAD")
	end if  
	rsAbil.close
	set rsAbil = nothing	 

end sub

'------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

sub Fine()
%>
<!--#include virtual = "/include/closeconn.asp"-->
<!--#include virtual = "strutt_coda2.asp"-->
<%
end sub
%>
<!-- ************** ASP Fine *************** -->

<!-- ************** MAIN Inizio ************ -->
<%

dim sDataIsc
dim sSQL
dim CnConn
dim rsAbil

dim nBando
dim nIdProgetto
dim sDescBando
dim sEsitoBando

dim sCodTimpr
dim sDescTimpr

dim sGraduatoria
dim sGruppo, sCommento
sDataIsc = Now()

nBando=clng(Request("txtIdBando"))
nTime = convdatetodb(Now())
nBypassdata = Request("Bypassdata")


ctrlBando()
if  sEsitoBando = "OK" then
    Inizio()

	if Trim(ctrlProj()) = "OK" then
		sDescTimpr = DecCodVal("TIMPR", "0", "", sCodTimpr, 1)  
		sDataSis = ConvDateToString(Now())
		ImpPagina()
	end if
end if

Fine()
%>
<!-- ************** MAIN Fine ************ -->
