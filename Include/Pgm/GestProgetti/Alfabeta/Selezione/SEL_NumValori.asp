<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<html>
<head>
<title>Area</title>
<link REL="STYLESHEET" TYPE="text/css" HREF="<%=session("progetto")%>/fogliostile.css">
<script language="javascript">
<!--#include virtual="/include/help.inc" -->
<!--#include virtual = "/Include/ControlNum.inc"-->

function Campi(sCompresa) {
	var nSelTab
	var nSelCampo
	var sDecod
	
	nSelCampo	= opener.document.frmInsCriteri.elements[2].selectedIndex
	nOper		= opener.document.frmInsCriteri.elements[3].selectedIndex
	if (document.frmSelArea.txtNumValori.value == ""){
		alert("Devi indicare un valore")
		return false
	}
	if (!IsNum(document.frmSelArea.txtNumValori.value)){
		alert("Devi indicare un valore numerico")
		return false
	}

	if (document.frmSelArea.txtNumValori.value <= 1){
		alert("Devi indicare almeno un valore maggiore/uguale a 2")
		return false
	}
	if (document.frmSelArea.txtNumValori.value <= 10)  {

		if (sCompresa == "0") {
	 //   opener.document.frmInsCriteri.cmbTipoArea.value= sTipoArea
        //    opener.document.frmInsCriteri.cmbTipoProf.value= sTipoProf
            opener.document.frmInsCriteri.prVolta.value= "No"
            opener.document.frmInsCriteri.Campo.value= opener.document.frmInsCriteri.elements[2].options[nSelCampo].value 
	        opener.document.frmInsCriteri.OpeLog.value= opener.document.frmInsCriteri.elements[3].options[nOper].value
	        opener.document.frmInsCriteri.AREA.value = "DIZ_DATI" 
	        opener.document.frmInsCriteri.SPECIFICO.value = "DIZ_TAB"
	        opener.document.frmInsCriteri.NumValori.value = document.frmSelArea.txtNumValori.value
	        opener.document.frmInsCriteri.action="SEL_VisCriteri.asp"
	        opener.document.frmInsCriteri.submit()
			 
/*			opener.document.location.href ="SEL_VisCriteri.asp?prVolta=No&Campo=" + 
				opener.document.frmInsCriteri.elements[2].options[nSelCampo].value +
				"&OpeLog=" + opener.document.frmInsCriteri.elements[3].options[nOper].value +
				"&IDTAB=" + opener.document.frmInsCriteri.IDTAB.value + 
				"&AREA=DIZ_DATI&SPECIFICO=DIZ_TAB&DESCTAB=" + opener.document.frmInsCriteri.DESCTAB.value + 
				"&IDRIC=" + opener.document.frmInsCriteri.IDRIC.value + 
				"&IDTAB1=" + opener.document.frmInsCriteri.IDTAB1.value +
				"&NumValori=" + document.frmSelArea.txtNumValori.value +
				"&cmbTipoArea=" + opener.document.frmInsCriteri.cmbTipoArea.value +
				"&cmbTipoProf=" + opener.document.frmInsCriteri.cmbTipoProf.value 
*/
		}else {
		//    opener.document.frmInsCriteri.cmbTipoArea.value= sTipoArea
        //    opener.document.frmInsCriteri.cmbTipoProf.value= sTipoProf
            opener.document.frmInsCriteri.prVolta.value= "No"
            opener.document.frmInsCriteri.Campo.value= opener.document.frmInsCriteri.elements[2].options[nSelCampo].value + sCompresa
	        opener.document.frmInsCriteri.OpeLog.value= opener.document.frmInsCriteri.elements[3].options[nOper].value
	        opener.document.frmInsCriteri.AREA.value = "DIZ_DATI" 
	        opener.document.frmInsCriteri.SPECIFICO.value = "DIZ_TAB"
	        opener.document.frmInsCriteri.NumValori.value = document.frmSelArea.txtNumValori.value
	        opener.document.frmInsCriteri.action="SEL_VisCriteri.asp"
	        opener.document.frmInsCriteri.submit()
		
/*			opener.document.location.href ="RIC_VisCriteri.asp?prVolta=No&Campo=" + 
				opener.document.frmInsCriteri.elements[2].options[nSelCampo].value + sCompresa +
				"&OpeLog=" + opener.document.frmInsCriteri.elements[3].options[nOper].value +
				"&IDTAB=" + opener.document.frmInsCriteri.IDTAB.value + 
				"&AREA=DIZ_DATI&SPECIFICO=DIZ_TAB&DESCTAB=" + 
				opener.document.frmInsCriteri.DESCTAB.value +
				"&IDRIC=" + opener.document.frmInsCriteri.IDRIC.value + 
				"&IDTAB1=" + opener.document.frmInsCriteri.IDTAB1.value +
				"&NumValori=" + document.frmSelArea.txtNumValori.value + 
				"&cmbTipoArea=" + opener.document.frmInsCriteri.cmbTipoArea.value +
				"&cmbTipoProf=" + opener.document.frmInsCriteri.cmbTipoProf.value 
	*/
		}
	}else{
		alert("Massimo 10 valori")
		return false
	}
	return true
}


function Invia()
{
	if (Campi(0)) {
		self.close()
		return true
	}
	return false
}

function Destro(e) 
{
	if (navigator.appName == 'Netscape' && 
		(e.which == 3 || e.which == 2))
		return false;
	else if (navigator.appName == 'Microsoft Internet Explorer' && 
			(event.button == 2 || event.button == 3))
		 {
			alert("Spiacenti, il tasto destro del mouse e' disabilitato");
			return false;
		  }
	return true;
}

// disattivazione del tasto destro sel mouse
document.onmousedown=Destro;
if (document.layers) window.captureEvents(Event.MOUSEDOWN);
window.onmousedown=Destro;

</script>
</head>
<body onload="javascript:document.frmSelArea.txtNumValori.focus();">
<form name="frmSelArea" onsubmit="return Invia()">


<table border="0" CELLPADDING="0" CELLSPACING="0" width="210" align="center">

<tr height="17">
	<td class="sfondomenu" width="67%" height="18"><span class="tbltext0">
		<b>&nbsp;NUMERO VALORI</b>
	</td>
	<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">
	</td>
	<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
</tr>
<tr>
	<td class="sfondocommaz" width="57%" colspan="3">
		<br>Indicare il numero di valori
	</td>
</tr>
<tr height="2">
	<td class="sfondocommaz" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif">
	</td>
</tr>
</table>
<br>		
<table border="0" CELLPADDING="0" CELLSPACING="0" width="210" align="center">
	<tr align="center">
	<td class="tbltext1">
		<b>Num. Valori Max(10)<b>
	</td>
	<td>
		<input class="textblacka" size="2" name="txtNumValori">	
	</td>
	
	</tr>
</table>
<br>		
<table width="210" cellspacing="2" cellpadding="1" border="0" align="center">
	<tr align="center">
	    
		<td>
			<a href="javascript: self.close()"><img src="<%=Session("progetto")%>/images/chiudi.gif" title="Chiudi la pagina" border="0" align="absBottom"></a>
		</td>
		<td>
		<input type="image" title="Invia Richiesta" src="<%=Session("progetto")%>/images/conferma.gif" id="image1" name="image1">
<!--		<a href="javascript:Invia()"><img src="<%'=Session("progetto")%>/images/conferma.gif" title="Chiudi la pagina"  border="0" align ="absBottom"></a>-->
	    </td>
	</tr>		
</table>
</form>

</body>
</html>
