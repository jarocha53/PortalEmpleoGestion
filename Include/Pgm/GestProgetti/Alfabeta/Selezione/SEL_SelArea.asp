<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #include virtual="/util/dbutil.asp" -->
<!-- #include virtual="/include/openconn.asp" -->
<!-- #include virtual="/include/DecCod.asp" -->
<%
dim sArea
dim sSQL
dim rsArea
dim rsTitolo

sArea	= Request.QueryString ("Area")
sCampo	= Request.QueryString ("sTxtArea")

sSQL = "SELECT DESC_TAB FROM DIZ_TAB WHERE ID_TAB ='" & Request.QueryString("sTab") & "'"

'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
set rsTitolo = CC.Execute(sSQL)
sTitolo = rsTitolo("DESC_TAB")
rsTitolo.Close
set rsTitolo = Nothing
%>
<html>
<head>
<title><%=sTitolo%></title>
<link REL="STYLESHEET" TYPE="text/css" HREF="<%=session("progetto")%>/fogliostile.css">
<script language="javascript">



<!-- #include virtual="/include/help.inc" -->

	function InviaSelezione(idArea,Denominazione)
	{
		opener.document.frmInsCriteri.txtTipoArea.value = idArea
		var sDenom = Denominazione.replace("$", "'")
	
		opener.document.frmInsCriteri.txtArea.value = sDenom.toUpperCase()
		opener.document.frmInsCriteri.txtTipoSpecifico.value = ""
		opener.document.frmInsCriteri.txtSpecifico.value = ""
		self.close()
	}
	function Chiudi()
	{
		self.close()
	}
	function Destro(e) 
	{
		if (navigator.appName == 'Netscape' && 
			(e.which == 3 || e.which == 2))
			return false;
		else if (navigator.appName == 'Microsoft Internet Explorer' && 
				(event.button == 2 || event.button == 3))
			 {
				alert("Spiacenti, il tasto destro del mouse e' disabilitato");
				return false;
			  }
		return true;
	}

	document.onmousedown=Destro;
	if (document.layers) window.captureEvents(Event.MOUSEDOWN);
		window.onmousedown=Destro;
	
</script>
</head>
<body>
<form name="frmSelArea">


<table border="0" CELLPADDING="0" CELLSPACING="0" width="460" align="center">

<tr height="17">
	<td class="sfondomenu" width="67%" height="18"><span class="tbltext0">
		<b>&nbsp;SELEZIONE AREA</b>
	</td>
	<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">
	</td>
	<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
</tr>
<tr>
	<td class="sfondocommaz" width="57%" colspan="3">
		<br>Selezionare l'area di interesse o premere <b>chiudi</b> per non selezionare nulla.
	</td>
</tr>
<tr height="2">
	<td class="sfondocommaz" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif">
	</td>
</tr>
</table>

		<br><p class="tbltext1" align="center"><b>Selezionare l'area per <%=sTitolo%></b></p>
<br>

<table border="0" CELLPADDING="2" CELLSPACING="2" width="460" align="center">
<%
	if sArea <> "LSTUD" and sArea <> "CL_VD" then
		sSQL = "select id_desc from diz_dati where id_campo = '" &_
		sCampo & "' AND ID_DESC IS NOT NULL"

'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
		set rsDenom = CC.Execute(sSQL)
		sDenominazione = rsDenom("ID_DESC")
		rsDenom.Close
		set rsSpec = nothing
	
		sSQL = "SELECT " & sCampo & " ," & sDenominazione & " FROM " & sArea &_
		 " ORDER BY " & sDenominazione
		 
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
		 set rsArea = CC.Execute(sSQL)

		do while not rsArea.EOF%>
			<tr>
				<td nowrap width="230" class="tblsfondo">
					<a class="textblack" href="Javascript:InviaSelezione('<%=rsArea(0)%>','<%=server.HTMLEncode (replace(rsArea(1),"'","$"))%>')"><b><%=server.HTMLEncode (ucase(rsArea(1)))%></b></a>
				</td>
				<%rsArea.MoveNext%>
				<td nowrap width="230" class="tblsfondo">
				<%if not rsArea.EOF then%>
					<a class="textblack" href="Javascript:InviaSelezione('<%=rsArea(0)%>','<%=server.HTMLEncode (replace(rsArea(1),"'","$"))%>')"><b><%=server.HTMLEncode (ucase(rsArea(1)))%></b></a>
					<%rsArea.MoveNext%>
				<%else%>
					&nbsp;
				<%end if%>
				</td>
			</tr>
	<%	loop
		rsArea.Close
	else
	'VV	sSQL = "SELECT CODICE,DESCRIZIONE FROM TADES WHERE NOME_TABELLA ='" & sArea &_
	'VV	 "' AND " & ConvDateToDBs(date) & " BETWEEN DECORRENZA AND SCADENZA ORDER BY DESCRIZIONE"
		
		sCondizione = ""
		sTabella = sArea
		dData	 = Date()
		nOrder   = 0
		Isa		 = 0
		aArray = decodTadesToArray(sTabella,dData,sCondizione,nOrder,Isa)
		if isArray(aArray) then
			for i = 0 to ubound(aArray) - 1 step 2
				j = i + 1	
%>			<tr>
				<td nowrap width="230" class="tblsfondo">
					<a class="textblack" href="Javascript:InviaSelezione('<%=aArray(i,i,i)%>','<%=server.HTMLEncode (replace(aArray(i,i+1,i),"'","$"))%>')"><b><%=server.HTMLEncode (ucase(aArray(i,i+1,i)))%></b></a>
				</td>
				<td nowrap width="230" class="tblsfondo">
<%				if j <= (ubound(aArray) - 1) then
%>					 <a class="textblack" href="Javascript:InviaSelezione('<%=aArray(j,j,j)%>','<%=server.HTMLEncode (replace(aArray(j,j+1,j),"'","$"))%>')"><b><%=server.HTMLEncode (ucase(aArray(j,j+1,j)))%></b></a>
<%				else
%>					&nbsp;
<%				end if
%>
				</td>
			</tr>
<%			next
			Erase aArray
		end if
	end if

%>
</table>
</form>
<br>
<table width="460" cellspacing="2" cellpadding="1" border="0" align="center">
	<tr align="center">
		<td>
			<a href="javascript: Chiudi()"><img src="<%=Session("progetto")%>/images/chiudi.gif" title="Chiudi la pagina" border="0" align="absBottom"></a>
		</td>
	</tr>		
</table>
</body>
</html>
<%
' Da aggiornare.
function ColsName(sArea)
	sSQL = "SELECT ID_CAMPO FROM DIZ_DATI WHERE ID_TAB = '" & sArea &  "' ORDER BY TIPO_CAMPO DESC"
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	set rsDec = CC.Execute(sSQL)
	do while not rsDec.EOF
		Response.Write "<BR>" & rsDec("ID_CAMPO")
		rsDec.MoveNext
	loop
	
	rsDec.Close
	set rsDec = nothing
	Response.Write "<BR>" & sSQL
	Response.End 

end function

%>
<!-- #include virtual="/include/closeconn.asp" -->
