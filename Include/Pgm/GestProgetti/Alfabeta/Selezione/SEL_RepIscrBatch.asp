<%'Option Explicit 
   Response.ExpiresAbsolute = Now() - 1 
   Response.AddHeader "pragma","no-cache"
   Response.AddHeader "cache-control","private"
   Response.CacheControl = "no-cache"
%>

<!-- #include virtual="/strutt_testa2.asp"-->
<!--#include virtual = "/include/openconn.asp"-->
<!--#include virtual="/util/portallib.asp"-->

<!-- ************** Javascript inizio ************ -->
<%
if ValidateService(session("idutente"),"SEL_REPISCRBATCH", CC) <> "true" then 
	response.redirect "/util/error_login.asp"
end if
%>

<script LANGUAGE="JavaScript">

<!--#include Virtual = "/Include/help.inc"-->


function VaiInizio(){
	location.href = "SEL_RepIscrBatch.asp";
}	

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

function Stampa(){
	frmstampa.submit();                                                    
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

function Scarica(report){	
	
	fin=window.open(report,"","toolbar=0;location=0,directories=0,status=0,menubar=1,scrollbars=1,resizable=1,copyhistory=0,width=450,height=340,screenX=200,screenY200");
}

</script>

<!-- ******************  Javascript Fine *********** -->
<!-- ************** ASP inizio *************** -->

<!--#include virtual ="/include/controldatevb.asp"-->
<%
'----------------------------------------------------------------------------------------------------------------------------------------------------  

sub Inizio()
%>
	<table border="0" width="520" cellspacing="0" cellpadding="0" height="81">
	   <tr>
	     <td width="500" background="<%=Session("Progetto")%>/images/titoli/strumenti2b.gif" height="81" valign="bottom" align="right">
	
	       <table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
	         <tr>
	             <td width="100%" valign="top" align="right">
	                 <b class="tbltext1a">Gestione Progetti</span></b>
	             </td>
	         </tr>
	       </table>
	     </td>
	   </tr>
	</table>
	<br>
	<table cellpadding="0" cellspacing="0" width="500" border="0">
		<tr height="18">
			<td class="sfondomenu" height="18" width="67%">
			<span class="tbltext0"><b>REPORT ISCRITTI BATCH</b></span></td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
			<td valign="middle" align="right" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1"></td>
		</tr>
		<tr width="371" class="SFONDOCOMM">
			<td colspan="3">Per la visualizzazione del report cliccare sui link sottostanti.
			<a href="Javascript:Show_Help('/Pgm/help/GestProgetti/Alfabeta/Selezione/SEL_RepIscrBatch')" onmouseover="javascript:window.status=' '; return true"><img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
			</td>
		</tr>
		<tr height="2">
			<td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>
	<br>
<%
end sub

'----------------------------------------------------------------------------------------------------------------------------------------------------  

sub ImpostaPag()

Dim fsDir, FileList, sFileName

	Set fso = CreateObject("Scripting.FileSystemObject")

	sPath= Session("progetto") & "/DocPers/CreaLogin/"
	fsDir= replace(Server.MapPath("\") & sPath, "/", "\")
'Response.Write fsDir

	Set f = fso.GetFolder(fsDir)
	Set FileList = f.Files

	if CLng(f.Files.Count) = 0 then
		%>
		<br><br>
		<table width="500" align="center" border="0" cellspacing="2" cellpadding="1">
			<tr height="20"> 
			  	<td class="tbltext3" align="middle" width="500">
					<b>Non ci sono report da visualizzare.</b>
				</td>
			</tr>
		</table>
		<br><br>
		<%
	else
		%>
		<table width="500" cellspacing="2" cellpadding="1" border="0">
			<tr class="sfondocomm">
				<td class="tbltext1"><b>NOME DEL DOCUMENTO</b></td>
				<td class="tbltext1"><b>REPORT</b></td>
			</tr>
		<%
		For Each sFileName in FileList
			sNomeFile = sPath & trim(sFileName.Name)
			
			%>	
			<tr class="sfondocomm">
				<td>
					<%=sFileName.Name%>
				</td>	
				<td align="center">
					<a href="javascript:Scarica('<%=sNomeFile%>')" class="tblDett">
						<img border="0" src="<%=Session("Progetto")%>/images/scheda.gif" alt="Visualizza il file report">
					</a>
				</td>
			</tr>
			<%	
		Next 'sFileName
		%>
		</table>
		<%	
	end if
	
end sub

'-----------------------------------------------------------------------------------------------------------------------------------------------------------

sub Fine()
%>
<!--#include virtual = "/include/closeconn.asp"-->
<!--#include virtual = "strutt_coda2.asp"-->
<%
end sub

%>
<!-- ************** ASP Fine *************** -->

<!-- ************** MAIN Inizio ************ -->
<%

Inizio()
ImpostaPag()
Fine()

%>