<%'Option Explicit 
  ' Response.ExpiresAbsolute = Now() - 1 
  ' Response.AddHeader "pragma","no-cache"
  ' Response.AddHeader "cache-control","private"
  ' Response.CacheControl = "no-cache"
%>
<%if session("login") = "" then%>
	<!-- #include virtual="/progettomda/strutt_testaMDA1.asp"-->
<%else%>
	<!-- #include virtual="/strutt_testa1.asp"-->
<%end if%>
<!--#include virtual ="/include/openconn.asp"-->
<!--#include virtual ="/util/portallib.asp"-->

<%

Session("menu")= ""
%>
<!-- ************** Javascript Inizio ************ -->
<!--include del file per fare i controlli sulle stringhe-->
<script LANGUAGE="Javascript" src="/Include/ControlString.inc"></script>
<!--include del file per fare i controlli sulla validit� delle date-->
<script LANGUAGE="Javascript" src="/Include/ControlDate.inc"></script>

<!--include del file per fare i controlli sulla numericit� dei campi-->
<script LANGUAGE="Javascript" src="/Include/ControlNum.inc"></script>

<!--include del file per fare i controlli sulla validit� del codice fiscale-->
<script LANGUAGE="Javascript" src="/Include/ControlCodFisc.inc"></script>

<script LANGUAGE="Javascript" src="/Include/help.inc"></script>
<script LANGUAGE="Javascript" src="/include/SelComune.js"></script>
<script LANGUAGE="Javascript" src="/ProgettoMdA/include/SelComuneRes.js"></script>
<script LANGUAGE="Javascript" src="Controlli.js"></script>

<script LANGUAGE="Javascript">
<!-- ************** ASP Inizio *************** -->

<!--#include virtual = "/include/DecCod.asp"-->
<!--#include virtual = "/include/ControlDateVB.asp"-->
<!--#include virtual = "/util/dbutil.asp"-->

function SelCameraCom()
{ 
 windowArea = window.open ('SelCameraCom.asp' ,'Info','width=422,height=450,Resize=No,Scrollbars=yes')	
 }

function PulisciRes()
{
	document.frmDomIscri.txtComuneRes.value = ""
}

function IsNumIscReg(sNumIscReg) {
	var t
	
	if (sNumIscReg.length != 11) {
		alert("Il Numero del registro deve essere di 11 caratteri")
		return false
	}
	for (t=0; t<11;t++) {

		if (t==6){
			if (sNumIscReg.charAt(t)!= "/"){
				alert("Il formato corretto del Numero del registro � : 999999/9999")
				return false
			}
			t++ 
		}
		if (!IsNum(sNumIscReg.charAt(t))) {
				alert("Il Numero del registro deve essere numerico")
				return false
		}
	}
	return true
}

function ControllaDati(){

			if(document.frmDomIscri.cmbCodTimpr.value == ""){
				alert("Il campo Tipologia Impresa � obbligatorio")
				document.frmDomIscri.cmbCodTimpr.focus()
				return false
			}
			if (TRIM(document.frmDomIscri.txtRagSoc.value) == ""){
				alert("Il campo Ragione Sociale � obbligatorio")
				document.frmDomIscri.txtRagSoc.focus()
				return false
			}
			if (document.frmDomIscri.cmbCodSet.value == ""){
				alert("Il campo Codice Settore � obbligatorio")
				document.frmDomIscri.cmbCodSet.focus()
				return false
			}
			if (TRIM(document.frmDomIscri.txtPartIva.value) == ""){
				alert("E' obbligatorio compilare la Partita Iva")
				document.frmDomIscri.txtPartIva.focus()
				return false
			}
			if (document.frmDomIscri.txtPartIva.value.length != 11){
				alert("Il campo Partita Iva � formalmente errato")
				document.frmDomIscri.txtPartIva.focus()
				return false
			} 
			if (!ValidateInputNumber(document.frmDomIscri.txtPartIva.value) == true){
				alert("Il campo Partita Iva � formalmente errato")
				document.frmDomIscri.txtPartIva.focus()
				return false
			} 
			if (document.frmDomIscri.txtCodFis.value.length != 0 && document.frmDomIscri.txtCodFis.value.length != 16){
				alert("Il campo Codice Fiscale � formalmente errato")
				document.frmDomIscri.txtCodFis.focus()
				return false
			} 
			/*if (document.frmDomIscri.txtCodFis.value.length == 16 && ControllChkCodFisc(TRIM(document.frmDomIscri.txtCodFis.value)) == false){
				alert("Il Codice Fiscale non � valido")
				document.frmDomIscri.txtCodFis.focus()
				return false
			}*/
			blank = " ";
			if (!ChechSingolChar(document.frmDomIscri.txtCodFis.value,blank))
			{
				alert("Il campo Codice Fiscale non pu� contenere spazi bianchi")
				document.frmDomIscri.txtCodFis.focus();
				return false;
			}
			
			if (!ValidateInputStringWithNumber(document.frmDomIscri.txtCodFis.value))
			{
				alert("Il campo Codice fiscale � formalmente errato")
				document.frmDomIscri.txtCodFis.focus()
				return false;
			}
			
			
			
			if(document.frmDomIscri.cmbCodSede.value == ""){
				alert("Il campo Tipo Sede � obbligatorio")
				document.frmDomIscri.cmbCodSede.focus()
				return false
			}
			if(TRIM(document.frmDomIscri.txtIndirizzo.value) == ""){
				alert("Il campo Indirizzo � obbligatorio")
				document.frmDomIscri.txtIndirizzo.focus()
				return false
			}
			if(document.frmDomIscri.cmbProvRes.value == ""){
				alert("Il campo Provincia � obbligatorio")
				document.frmDomIscri.cmbProvRes.focus()
				return false
			}
			if(TRIM(document.frmDomIscri.txtComuneRes.value) == ""){
				alert("Il campo Comune � obbligatorio")
				document.frmDomIscri.txtComuneRes.focus()
				return false
			}
			 
			//telefono	
		
		if (TRIM(document.frmDomIscri.txtNumTel.value) == "") {
			alert("Il campo Telefono � obbligatorio")
			document.frmDomIscri.txtNumTel.focus() 
			return false
		}
		if (TRIM(document.frmDomIscri.txtNumTel.value) != "") {
			if (!ValidateInputNumber(document.frmDomIscri.txtNumTel.value)) {
				alert("Il campo Telefono deve essere numerico")
				document.frmDomIscri.txtNumTel.focus() 
				return false
			}
			if (eval(document.frmDomIscri.txtNumTel.value) == 0){
				alert("Il numero di telefono non pu� essere zero")
				document.frmDomIscri.txtNumTel.focus()
				return false
			}
		}
		
		// email
		

		if (document.frmDomIscri.txtEMail.value != "") {
			if (!ValidateEmail(document.frmDomIscri.txtEMail.value)) {
				alert("Il campo E-mail � formalmente errato")
				document.frmDomIscri.txtEMail.focus() 
				return false
			}
		}
		
			if(TRIM(document.frmDomIscri.txtRichied.value) == ""){
				alert("Il campo Descrizione del Richiedente � obbligatorio")
				document.frmDomIscri.txtRichied.focus()
				return false
			}

		// servizio
		
		var y = 0;
			for (i = 0; i < document.frmDomIscri.chkServizio.length; i++) {
				if (document.frmDomIscri.chkServizio[i].checked) {
					y++
				}
			}
			if (y==0){
				alert("E' obbligatorio selezionare un Settore di Appartenenza")
				return false
			}
			
				
<!-- ----------------------------------------------------------- -->	   

			if(TRIM(document.frmDomIscri.txtObiettivo.value) == ""){
				alert("Il campo Descrizione dell'Obiettivo � obbligatorio")
				document.frmDomIscri.txtObiettivo.focus()
				return false
			}

			var y = 0;
			for (i = 0; i < document.frmDomIscri.ContObiettivi.value; i++) {
				if (eval("document.frmDomIscri.chkObiettivo" + i + ".checked")) {
					y++;
					if (eval("document.frmDomIscri.txtPrioObiettivo" + i + ".value")== "") {
						alert("E' obbligatorio immettere la Priorit� dell'Obiettivo")
						eval("document.frmDomIscri.txtPrioObiettivo" + i + ".focus()")
						return false
					}
					if (!ValidateInputNumber(eval("document.frmDomIscri.txtPrioObiettivo" + i + ".value")) == true){
						alert("Il campo Priorit� dell'Obiettivo deve essere numerico")
						eval("document.frmDomIscri.txtPrioObiettivo" + i + ".focus()")
						return false
					} 
					if (eval("document.frmDomIscri.chkObiettivo" + i + ".value")=="99") {
						if (document.frmDomIscri.DescObiettivo.value == "") {
							alert("Il campo Descrizione dell'Obiettivo deve essere compilato")
							document.frmDomIscri.DescObiettivo.focus()
							return false
						}
					}
				}
				
				//simona 
				if (eval("document.frmDomIscri.txtPrioObiettivo" + i + ".value")!= "") {
						if (eval("document.frmDomIscri.chkObiettivo" + i + ".checked")==false) {
							alert("Se si indica la Priorit� dell'Obiettivo selezionare la Tipologia dell'Obiettivo corrispondente")
							eval("document.frmDomIscri.txtPrioObiettivo" + i + ".focus()")
							return false
						}
				}
				
				
			}
			if (y==0){
				alert("E' obbligatorio selezionare almeno un Obiettivo")
				document.frmDomIscri.chkObiettivo0.focus()
				return false
			}
			
			
			//----------SM priorit� Obiettivi
			
			
			//verifico il numero degli elementi checked
			var w = 0;
			for (x = 0; x < document.frmDomIscri.ContObiettivi.value; x++) {
		
				if (eval("document.frmDomIscri.chkObiettivo" + x + ".checked")) {
					w++
				
				
				}
			}
				
			//verifico che non abbiano digitato un vlore alla priorit� non 
			// consentito
			var i = 0;
			for (i = 0; i < document.frmDomIscri.ContObiettivi.value; i++) {
		
				if (eval("document.frmDomIscri.chkObiettivo" + i + ".checked")) {
				
					if (eval("document.frmDomIscri.txtPrioObiettivo" + i + ".value") <1) {
						alert("Non si pu� imputare lo zero come priorit�")
						return false
					
					}
					
				
					if (eval("document.frmDomIscri.txtPrioObiettivo" + i + ".value") > w){
						alert("Non si pu� imputare una priorit� maggiore del numero di Tipologia di Obiettivi selezionati")
						return false
					
					}
				}
					
			}
				
			
			////////controllo del grado di priorit�
			
			var z = 0;
			var i = 0;
			for (i = 0; i < document.frmDomIscri.ContObiettivi.value; i++) {
			
				if (eval("document.frmDomIscri.chkObiettivo" + i + ".checked")) {
								
						for (z = 0; z < i; z++) {
							
								if ((eval("document.frmDomIscri.txtPrioObiettivo" + i + ".value")) == (eval("document.frmDomIscri.txtPrioObiettivo" + z + ".value"))){
										
									alert("Non si pu� imputare la medesima Priorit� a due o piu Tipologie di Obiettivi") 
									return false
							
								}
						}	
					
				}
					
			}
			//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
			
			var y = 0;
			for (i = 0; i < document.frmDomIscri.ContConsul.value; i++) {
				if (eval("document.frmDomIscri.chkConsul" + i + ".checked")) {
					y++;
					if (eval("document.frmDomIscri.txtPrioConsul" + i + ".value")== "") {
						alert("E' obbligatorio immettere la Priorit� del Tipo di Consulenza")
						eval("document.frmDomIscri.txtPrioConsul" + i + ".focus()")
						return false
					}
					if (!ValidateInputNumber(eval("document.frmDomIscri.txtPrioConsul" + i + ".value")) == true){
						alert("Il campo Priorit� del Tipo di Consulenza deve essere numerico")
						eval("document.frmDomIscri.txtPrioConsul" + i + ".focus()")
						return false
					} 
					if (eval("document.frmDomIscri.chkConsul" + i + ".value")=="99") {
						if (document.frmDomIscri.DescConsul.value == "") {
							alert("Il campo Descrizione della Consulenza deve essere compilato")
							document.frmDomIscri.DescConsul.focus()
							return false
						}
					}
				}
				
				//simona 
				if (eval("document.frmDomIscri.txtPrioConsul" + i + ".value")!= "") {
						if (eval("document.frmDomIscri.chkConsul" + i + ".checked")==false) {
							alert("Se si indica il Grado di Priorit� della scelta selezionare la Tipologia di Consulenza Specialistica corrispondente")
							eval("document.frmDomIscri.txtPrioConsul" + i + ".focus()")
							return false
						}
				}
			}
			if (y==0){
				alert("E' obbligatorio selezionare almeno un Tipo di Consulenza")
				document.frmDomIscri.chkConsul0.focus()
				return false
			}


<!-- ----------------------------------------------------------- -->	   
	//----------SM priorit� Consulenze
			
			
			//verifico il numero degli elementi checked
			var w = 0;
			for (x = 0; x < document.frmDomIscri.ContConsul.value; x++) {
		
				if (eval("document.frmDomIscri.chkConsul" + x + ".checked")) {
					w++
				
				
				}
			}
				
			//verifico che non abbiano digitato un vlore alla priorit� non 
			// consentito
			var i = 0;
			for (i = 0; i < document.frmDomIscri.ContConsul.value; i++) {
		
				if (eval("document.frmDomIscri.chkConsul" + i + ".checked")) {
				
					if (eval("document.frmDomIscri.txtPrioConsul" + i + ".value") <1) {
						alert("Non si pu� imputare lo zero come priorit�")
						return false
					
					}
					
				
					if (eval("document.frmDomIscri.txtPrioConsul" + i + ".value") > w){
						alert("Non si pu� imputare una priorit� maggiore del numero di Tipologia di Consulenze selezionate")
						return false
					
					}
				}
					
			}
				
			
			////////controllo del grado di priorit�
			
			var z = 0;
			var i = 0;
			for (i = 0; i < document.frmDomIscri.ContConsul.value; i++) {
			
				if (eval("document.frmDomIscri.chkConsul" + i + ".checked")) {
								
						for (z = 0; z < i; z++) {
							
								if ((eval("document.frmDomIscri.txtPrioConsul" + i + ".value")) == (eval("document.frmDomIscri.txtPrioConsul" + z + ".value"))){
										
									alert("Non si pu� imputare la medesima Priorit� a due o piu Tipologie di Consulenze") 
									return false
							
								}
						}	
					
				}
					
			}

<!-- ----------------------------------------------------------- -->	   

		//	if (TRIM(document.frmDomIscri.txtComCameraCom.value) == ""){
			if (TRIM(document.frmDomIscri.txtPrvCameraCom.value) == ""){	
				alert("E' obbligatorio specificare la Camera di Commercio di appartenenza")
		//		document.frmDomIscri.txtComCameraCom.focus()
				document.frmDomIscri.txtPrvCameraCom.focus()		
				return false
			} 
			// controllo correttezza formale del numero registro
			document.frmDomIscri.txtNumReg.value = TRIM(document.frmDomIscri.txtNumReg.value)		
			if (!document.frmDomIscri.txtNumReg.value == "")	
			{
				if (document.frmDomIscri.txtNumReg.value != "")
				{
					if (!IsNumIscReg(document.frmDomIscri.txtNumReg.value))
					{
						document.frmDomIscri.txtNumReg.focus()
						return false				
					}
				}
			}
			
	/*		if(document.frmDomIscri.cmbCodForm.value !="16"){
				if(document.frmDomIscri.cmbCodCcnl.value == ""){
					alert("Il campo Codice CCNL � obbligatorio")
					document.frmDomIscri.cmbCodCcnl.focus()
					return false
				}
			}
	*/		
			if(document.frmDomIscri.txtRefSede.value == ""){
				alert("Il campo Referente Sede � obbligatorio")
				document.frmDomIscri.txtRefSede.focus()
				return false
			}
			if (document.frmDomIscri.radConsenso[0].checked == false) {
				alert("Per poter concludere la transazione � necessario Accettare il `Consenso al trattamento dei dati personali`")
				document.frmDomIscri.radConsenso[0].focus()
				return false
			}
		//	alert ("SCRIVE!")
}

</script>
<%	
'-------------------------------------------------------------------------------------------------------------------------------


Sub Titolo(sTitolo,sDescTitolo) %>
    <table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
		<tr height="18">
			<td class="sfondomenu" width="67%" height="18"><span class="tbltext0"><b>&nbsp;<%=sTitolo%></b></span></td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
			<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
		</tr>
	</table>
	<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
		<tr>
			<td align="left" class="sfondocomm"><%=sDescTitolo%></td>
		</tr>
		<tr height="2">
			<td class="sfondocomm" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>
	<br>
<%
end sub

Sub Msgetto(Msg)

%>
	<br>
	<table align="center">
		<tr>
			<td class="tbltext3"><b><%=Msg%></b></td>
		</tr>	
		<tr>
			<td>&nbsp;</td>
		</tr>	
		<tr>
			<td align="center"><a HREF="javascript:history.back()"><img SRC="/progettomda/images/indietro.gif" border="0" WIDTH="55" HEIGHT="40"></a></td>
		</tr>
	</table>
<%
End Sub

'-------------------------------------------------------------------------------------------------------------------------------

sub Inizio()
%>
	
	<table cellpadding="0" cellspacing="0" width="500" border="0">
		<tr height="18">
			<td class="sfondomenu" height="18" width="67%">
			<span class="tbltext0"><b>&nbsp;DOMANDA DI ISCRIZIONE</b></span></td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
			<td valign="middle" align="right" class="tbltext1" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">(*) campi obbligatori</td>
		</tr>
		<tr>
			<td class="sfondocomm" width="57%" colspan="3">
				Modulo di iscrizione 
			<%if  sEsitoBando = "OK" then %>
			      per il bando&nbsp;<b><%=sDescBando%></b><br>  
			      Premere <b>Invia</b> per salvare. 
			<%end if%>
			<a href="Javascript:Show_Help('/Pgm/help/GestProgetti/Alfabeta/Selezione/SEL_InsDomIscr')" onmouseover="javascript:window.status=' '; return true"><img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
			</td>
		</tr>
		<tr height="2">
			<td colspan="3" class="sfondocomm" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>
	
	<form method="post" name="frmDomIscri" onsubmit="return ControllaDati(this)" action="AZI_CnfAllegato2.asp">
	<input type="hidden" name="COD_STDIS" value="<%=CodiceStato%>">
	<input type="hidden" name="txtScadenzaOld" value="<%=sSCadenza%>">
	<input type="hidden" name="txtoggi" value="<%=ConvDateToString(Date())%>">
<%
end sub

'------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

sub ImpPagina()
'	Response.Write "AMBRA " & Session("Progetto")
    dim sInt 
    
	       sCommento =  "Presta <b>molta attenzione</b> a quanto inserisci:<br>" &_ 
				        "Indicare di seguito i dati dell'azienda."  
				        
	       call Titolo("Dati Azienda",sCommento)
	    %>
	   
		<table width="500" border="0" cellspacing="2" cellpadding="1">
		<!-------------dati simo--->
		
		<tr>
			<td align="middle" colspan="2" nowrap>
				<p align="left">
					<span class="tbltext1" Style="CURSOR:HELP" title="Scegliere la tipologia d'impresa che corrisponde all'azienda"><b>Tipologia Impresa*<b></span>
				</p>
			</td>
			<td align="left">
<%
					sTimpr = "TIMPR|0|" & date & "|" & sTipoImp & "|cmbCodTimpr|AND CODICE <> '03' ORDER BY DESCRIZIONE"
		'		else
		'			sTimpr = "TIMPR|0|" & date & "|" & sTipoImp & "|cmbCodTimpr' onchange='InviaProv(this.value)' '|ORDER BY DESCRIZIONE"
		'		end if									
				CreateCombo(sTimpr)				
%>	
			</td>
		</tr>    
        <tr>
			<td align="middle" colspan="2" nowrap>
				<p align="left">
				<span class="tbltext1" Style="CURSOR:HELP" title="Inserire Denominazione Azienda"><b>Ragione sociale*<b></span>
				</p>
			</td>
            <td align="left" colspan="2" width="57%"> 
				<p align="left">
					<input style="TEXT-TRANSFORM:uppercase" WIDTH="220px" class="textblack" size="40" maxlength="50" name="txtRagSoc">					
				</p>
            </td>
		</tr>
        <tr>
			<td align="middle" colspan="2" nowrap>
				<p align="left">
				<span class="tbltext1" Style="CURSOR:HELP" title="Inserire forma giuridica che si adatta alla tipologia dell'attivit� esercitata."><b>Forma Giuridica*</b></span>&nbsp;
				</p>
            </td>
            
			<td align="left" colspan="2" width="57%"> 
				<p align="left">
				<span class="table">
<%
				sInt = "FGIUR|" & sIsa & "|" & date() & "| |cmbCodForm | ORDER BY DESCRIZIONE"						
				CreateCombo(sInt)
%>
				</span>
				</p>
            </td>
		</tr>
        <tr>
			<td align="left" colspan="2">
				<p align="left">
					<span class="tbltext1" Style="CURSOR:HELP" title="Indicare classificazione delle attivit� economiche secondo estratto Istat."><b>Settore*</b></span>					
				</p>
			</td>

			<td align="left" colspan="2" width="57%"> 
				<p align="left">
					<span class="table">				
<%
				sSQL = "SELECT ID_SETTORE,subStr(DENOMINAZIONE,1,62) As DENOMINAZIONE from Settori ORDER BY DENOMINAZIONE"

				set rsSett = CC.Execute(sSQL)
				Response.Write "<SELECT ID='cmbCodSet' name='cmbCodSet' class='textblack'><OPTION value=></OPTION>"

				do while not rsSett.EOF 

					Response.Write "<OPTION "

					Response.write "value ='" & clng(rsSett("ID_SETTORE")) & _
						"'> " & rsSett("DENOMINAZIONE")  & "</OPTION>"
					rsSett.MoveNext 
				loop
	
				Response.Write "</SELECT>"

				rsSett.Close	
				set rsSett = nothing
%>					
					</span>
				</p>
			</td>
		</tr>
		
		<tr height="2">
		
			<td class="sfondocomm" colspan="4" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
		</tr>
		<tr>
			<td colspan="4" class="textblack">
				 <br>
			</td>
		</tr>
        <tr>
			<td align="left" colspan="2">
				<p align="left">
					<span class="tbltext1"><b>Partita IVA*</b></span>
				</p>
            </td>            
			<td align="left" colspan="2" width="57%"> 
				<p align="left">					
					<input class="textblack" size="16" maxlength="11" name="txtPartIva">					
				</p>
            </td>
        </tr>
        <tr>
			<td align="left" colspan="2">
				<p align="left">
					<span class="tbltext1"><b>Codice fiscale</b></span>					 
				</p>
            </td>            
			<td align="left" colspan="2" width="57%"> 
				<p align="left">
					<input class="textblack" size="22" maxlength="16" name="txtCodFis" onkeyup="this.value=this.value.toUpperCase()">					
				</p>
            </td>
        </tr>
		</table>
		<table width="500" border="0" cellspacing="2" cellpadding="1">	    
		<tr>
			<td align="left" class="textblack" colspan="1">
				Indicare i dati delle Unit� Produttive con cui l'azienda partecipa all'avviso pubblico (Rif. dell' art.4 dell'avviso pubblico)
			</td>
	    </tr>
	    </table>
	    <table width="500" border="0" cellspacing="2" cellpadding="1">	    
        <tr>
			<td align="middle" colspan="2" nowrap>
				<p align="left">
					<span class="tbltext1" Style="CURSOR:HELP" title="Indicare che funzione si svolge nella sede indicata."><b>Tipo sede*</b></span>
				</p>		
            </td>            
			<td align="left" colspan="2"> 
				<p align="left">
					<span class="table">
<%
					sInt = "TSEDE|" & sisa & "|" & date & "|" & sCodSede & "|cmbCodSede| ORDER BY DESCRIZIONE"			
					CreateCombo(sInt)
%>
				</p>
            </td>
        </tr>
        <tr>
			<td align="left" colspan="2">
				<p align="left">
					<span class="tbltext1"><b>Indirizzo*</b></span>
				</p>
			</td>			
			<td align="left" colspan="2"> 
				<p align="left">				
					<input style="TEXT-TRANSFORM:uppercase" WIDTH="220px" class="textblack" size="40" maxlength="50" name="txtIndirizzo">
				</p>
			</td>
        </tr>               
        </table>
 <%
'	if trim(sCodTimpr) = "" then
' dovrebbe essere testato il solo codice 00

'17/05/2006 inizio
'	if trim(sCodTimpr) = "" or isnull(sCodTimpr) or sCodTimpr="00" or sCodTimpr="0" then
			%>
			  <!--input type="hidden" name="cmbEnteOcc" value="0">			  <input type="hidden" name="txtCodTimpr" value-->
			<%
'	else		
'			sSQL = "SELECT SI.ID_SEDE, SI.DESCRIZIONE, SI.PRV " & _
'					"FROM SEDE_IMPRESA SI, IMPRESA I " & _
'					"WHERE I.COD_TIMPR = '" & sCodTimpr & "' " & _
'					"AND I.ID_IMPRESA = SI.ID_IMPRESA " & _
'					"ORDER BY SI.PRV, SI.DESCRIZIONE"
'					
'					Response.Write sSQL
'				'	Response.END
'			set rsImpresa = Server.CreateObject("ADODB.Recordset")
'			rsImpresa.Open sSQL, CC, 3
'
'			if not rsImpresa.EOF then
'				
'				   call Titolo(sDescTimpr,"Seleziona la regione di residenza solo se vi risiedi da almeno sei mesi.")
				%>	    
    
		<!--table width="500" border="0" cellspacing="2" cellpadding="1">			<tr>				<td align="left" colspan="2" width="60%" nowrap>					<span class="tbltext">					<input type="hidden" name="cmbEnteOcc" value>					<input type="hidden" name="txtCodTimpr" value="<%'=sCodTimpr%>">						<input type="hidden" name="txtDescTimpr" value="<%'=sDescTimpr%>">																<textarea rows="4" cols="40" name="txtDescArea" readonly class="textblacka"></textarea>					<a href="Javascript:SelImpresa('<%'=sCodTimpr%>','<%'=sDescTimpr%>','SEDE_IMPRESA','ID_SEDE','txtImpresa','txtRagione','<%=nBando%>')" ID="imgPunto2" name="imgPunto2"><img border="0" src="<%=Session("Progetto")%>/images/bullet1.gif" onmouseover="javascript:window.status='';return true"></a>					</span>				</td>						</tr>		 </table>    <br-->
  <%	'end if
	'end if  
 
	      %>
	    
	    <table width="500" border="0" cellspacing="2" cellpadding="1">
	    
	    <tr>		
			<td align="left" class="tbltext1" colspan="2">
				<b>Provincia*&nbsp;</b>
			</td>			
			<td align="left">
							
<%
	
		set rsProvRes = Server.CreateObject("ADODB.Recordset")

		sSQLProvRes = " SELECT distinct (prv) " &_
					" from COMUNE_PARCHI " 
				
			rsProvRes.Open sSQLProvRes, CC, 3

			if not rsProvRes.EOF then
				
			Response.Write "<Select class='textblack' name='cmbProvRes' onchange='PulisciRes()'"
					Response.Write ">" 
					Response.Write "<Option></Option>" 
				do while not rsProvRes.EOF
				sProv = DecCodVal("PROV", "0", "", rsProvRes("prv"), 1)
				
				
					
					Response.Write "<OPTION "
					Response.write "value ='" & rsProvRes("prv") & _
					"'> " & sProv  & "</OPTION>"
					
				
				
				
				
				 rsProvRes.movenext
				loop 
				Response.Write "</Select>" 
			end if
%>
			</td>
		</tr>		
		<!--tr>					<td align="left" class="tbltext1" width="30%">				<b>Provincia di Residenza*</b>			</td>						<td align="left"-->				
<%



	'		sInt = "PROV|0|" & date & "||cmbProvRes' onchange='PulisciRes()| ORDER BY DESCRIZIONE"			
	'		CreateCombo(sInt)	
%>
			<!--/td>		</tr-->		
		<tr>		
			<td align="left" class="tbltext1" colspan="2">
				<b>Comune*&nbsp;</b>
			</td>		
			
		
			<td nowrap>
				<span class="tbltext">
				<input type="text" name="txtComuneRes" style="TEXT-TRANSFORM: uppercase;" class="textblack" size="35" readonly value="<%=DescrComuneResid%>">
				<input type="hidden" id="txtComRes" name="txtComRes" value="<%=COM_RES%>">
<%
				NomeForm="frmDomIscri"
				CodiceProvincia="cmbProvRes"
				NomeComune="txtComuneRes"
				CodiceComune="txtComRes"
				Cap="txtCapRes"	

				
%>
				<input type="HIDDEN" name="txtCapRes" class="textblack" value="<%=CAP_RES%>" size="5" maxlength="5">
				<a href="Javascript:SelComuneRes('<%=NomeForm%>','<%=CodiceProvincia%>','<%=NomeComune%>','<%=CodiceComune%>','<%=Cap%>')" ID="imgPunto1" name="imgPunto1" onmouseover="javascript:window.status='';return true"><img border="0" src="<%=Session("Progetto")%>/images/bullet1.gif"></a>
				</span>				
			</td>
		</tr>		
	    
	</table>
	
	   <table width="500" border="0" cellspacing="2" cellpadding="1">
	    <tr>
			<td align="left" class="textblack" colspan="2">
			Indicare un recapito telefonico presso cui gli operatori possono contattare direttamente per comunicazioni urgenti.
			E' necessario scrivere di seguito tutti i numeri che lo compongono, senza aggiungere spazi o altri separatori.<br>
			Segnalaci il tuo indirizzo di posta elettronica personale, se lo hai, in modo da agevolare i contatti.
			</td>
	    </tr>
	    <tr>
			<td align="left" class="tbltext1" width="23%">
				<b>N� Telefono*</b>
	        </td>
	        <td align="left">
				<input style="TEXT-TRANSFORM: uppercase;" size="25" maxlength="20" class="textblack" name="txtNumTel">
	        </td>
	    </tr>
	    <tr>
			<td align="left" class="tbltext1" width="23%">
	   			<b>E-Mail</b>
	        </td>
	        <td align="left">
		   		<input size="25" maxlength="100" class="textblack" name="txtEMail">
	        </td>
	    </tr>
	</table>

			
<!-- ----------------------------------------------------------- -->	   
	<table width="500" border="0" cellspacing="2" cellpadding="1">   
	<hr>
	<tr>
		<td colspan="4" class="textblack">
			<b> RICHIEDENTE </b>
		</td>
	</tr>
		<tr>
			<td colspan="4" class="textblack">
				

				<br>DESCRIZIONE DELLA STORIA DEL SOGGETTO, IL SETTORE IN CUI OPERA, IL TIPO DI ATTIVITA'
				SVOLTA, I PRODOTTI, ETC.
			</td>
		</tr>
		
	</table>
	
	<!-- ----------------------------------------------------------- -->	   
	<br>
	<table width="500" border="0" cellspacing="2" cellpadding="1" align="center">   
			<tr> 
					<span class="tbltext1">- Utilizzabili 
					<b><label name="NumCaratteri" id="NumCaratteri1">600</label></b>
					 caratteri  -
					</span>
				</td>
			</tr>
			<tr> 
				<td align="left" valign="middle" colspan="4">
					<textarea name="txtRichied" onKeyup="JavaScript:CheckLenTextArea(document.frmDomIscri.txtRichied,NumCaratteri1,600)" CLASS="boxcommunity" cols="60" rows="10"></textarea>
				</td>
			</tr>
			
	</table>
	<br>	
	<table width="500" border="0" cellspacing="2" cellpadding="1" align="center">   
	<tr>
		<td colspan="4" class="textblack">
	<hr>
			<b> SETTORE DI APPARTENENZA </b>
		</td>
	</tr>
	   
	</table>  
   
<!--    <table width="500" border="0" cellspacing="2" cellpadding="1">					<tr>			<td class="tbltext1" valign="middle" align="left"><br>				<input type="checkbox" id="chkAgro" name="chkAgro">						</td>			<td class="tbltext1" valign="middle" align="left"><br>			Agroalimentare				</td>		</tr>				<tr>			<td class="tbltext1" valign="middle" align="left">				<input type="checkbox" id="chkTurismo" name="chkTurismo">						</td>			<td class="tbltext1" valign="middle" align="left">			Turismo rurale			</td>		</tr>				<tr>			<td class="tbltext1" valign="middle" align="left">				<input type="checkbox" id="chkArti" name="chkArti">						</td>			<td class="tbltext1" valign="middle" align="left">			Artigianato tipico locale			</td>		</tr>			</table>-->	
	<!--inizio simona gab-->
	<table width="500" border="0" cellspacing="2" cellpadding="1" align="center">   
	<tr>
		<td colspan="4" class="textblack">
	<%	
   		set rsDecServizio = Server.CreateObject("ADODB.Recordset")

		sSQLDecServizio = " SELECT CODICE, DESCRIZIONE" &_
					" from DESC_PROD_SERV " &_
					" WHERE  " & nTime & " between decorrenza and scadenza "
			
			rsDecServizio.Open sSQLDecServizio, CC, 3

		if not rsDecServizio.EOF then
			nContServ = 0
			do while not rsDecServizio.EOF
				sCodiceServizio  = rsDecServizio("codice")
				sDescServizio= rsDecServizio("descrizione")
		
				nContServ = nContServ + 1
		%>
		<tr>
				<td class="tbltext1" valign="middle" align="left">
				<input type="radio" id="chkServizio" name="chkServizio" value="<%=sCodiceServizio%>">								
			</td>
			
			<td class="tbltext1" valign="middle" align="left">
				<%=sDescServizio%>	<br>			
				<input type="hidden" name="txtServizio<%=nContServ%>" value="<%=sCodiceServizio%>">				
			</td>
			
		 <%	 
			
			rsDecServizio.movenext
			loop
		 end if
		 %>
		 
			<input type="hidden" name="ContServ" value="<%=nContServ%>"> 

		</table>

<!-- ----------------------------------------------------------- -->	   
	<table width="500" border="0" cellspacing="2" cellpadding="1">   
	<hr>
	<tr>
		<td colspan="4" class="textblack">
			<b> OBIETTIVI </b>
		</td>
	</tr>
		<tr>
			<td colspan="4" class="textblack">
				

				<br>ESPORRE CON CHIAREZZA L'OBIETTIVO / GLI OBIETTIVI CHE IL SOGGETTO INTENDE PERSEGUIRE.
				INDICARE INOLTRE LE TIPOLOGIE DEGLI OBIETTIVI PER IL CUI RAGGIUNGIMENTO E' RICHIESTO
				L'INTERVENTO DI CONSULENZA SPECIALISTICA E/O ASSISTENZA TECNICA. NEL CASO IN CUI VENGANO
				INDICATI PIU' OBIETTIVI, SPECIFICARNE IL GRADO DI PRIORITA' (IN ORDINE DECRESCENTE)
			</td>
		</tr>
		
	</table>
	<br>
	<table width="500" border="0" cellspacing="2" cellpadding="1" align="center">   
			<tr> 
					<span class="tbltext1">- Utilizzabili 
					<b><label name="NumCaratteri" id="NumCaratteri">600</label></b>
					 caratteri  -
					</span>
				</td>
			</tr>
			<tr> 
				<td align="left" valign="middle" colspan="4">
					<textarea name="txtObiettivo" onKeyup="JavaScript:CheckLenTextArea(document.frmDomIscri.txtObiettivo,NumCaratteri,600)" CLASS="boxcommunity" cols="60" rows="10"></textarea>
				</td>
			</tr>
			
	</table>

<!-- OBIETTIVI ---------------------------------------------------------------->

		<table width="500" border="0" cellspacing="2" cellpadding="1" align="center">   

	<%	
   		set rsObiettivi = Server.CreateObject("ADODB.Recordset")

		sSQLObiettivi = " SELECT CODICE, DESCRIZIONE" &_
					" from DESC_TIPO_OBIETTIVI " &_
					" WHERE  " & nTime & " between decorrenza and scadenza "
			
			rsObiettivi.Open sSQLObiettivi, CC, 3

		if not rsObiettivi.EOF then
			nContServ = 0
			do while not rsObiettivi.EOF
				sCodiceObiettivo  = rsObiettivi("codice")
				sDescObiettivo= rsObiettivi("descrizione")
		
		%>
		<tr>
				<td class="tbltext1" valign="middle" align="left">
				<input type="checkbox" value="<%=sCodiceObiettivo%>" id="chkObiettivo<%=nContServ%>" name="chkObiettivo<%=nContServ%>">								
			</td>
			
			<td class="tbltext1" valign="middle" align="left">
			
		<%	if sCodiceObiettivo = "99" then	%>
				<input class="textblack" size="60" maxlength="50" name="DescObiettivo">
		<%	else		%>
				<%=sDescObiettivo%>
		<%  end if		%>

<!--				<input type="hidden" name="txtCodObiettivo<%=nContServ%>" value="<%=sCodiceObiettivo%>">	-->
			</td>
			
			<td width="20%" class="tbltext1" valign="middle" align="left">
		<%  if nContServ = 0 then  %>	
				Priorit�
		<%	end if  %>
		
					<input class="textblack" size="20" maxlength="2" name="txtPrioObiettivo<%=nContServ%>">					
			</td>
		 <%	 
		 
			nContServ = nContServ + 1
			rsObiettivi.movenext
			loop
		 end if
		 
		 set rsObiettivi = nothing
		 %>
		 
			<input type="hidden" name="ContObiettivi" value="<%=nContServ%>"> 

		</table>

<!-- ----------------------------------------------------------- -->	   
	<table width="500" border="0" cellspacing="2" cellpadding="1">   
	<hr>
	<tr>
		<td colspan="4" class="textblack">
			<b> TIPO DI CONSULENZA SPECIALISTICA E/O ASSISTENZA TECNICA RICHIESTA <br>
			    (al massimo tre scelte) </b>
		</td>
	</tr>
		<tr>
			<td colspan="4" class="textblack">
				

				<br>SPECIFICARE IL TIPO DI CONSULENZA SPECIALISTICA RICHIESTA.<br>
				NEL CASO IN CUI VENGANO INDICATE PIU' SCELTE, SPECIFICARNE IL GRADO DI PRIORITA'&nbsp;
				(IN ORDINE DECRESCENTE)
			</td>
		</tr>
		
	</table>

<!-- CONSULENZA ---------------------------------------------------------------->

		<table width="500" border="0" cellspacing="2" cellpadding="1" align="center">   

	<%	
   		set rsConsul = Server.CreateObject("ADODB.Recordset")

		sSQLConsul = " SELECT CODICE, DESCRIZIONE" &_
					" from DESC_TIPO_CONS " &_
					" WHERE  " & nTime & " between decorrenza and scadenza "
			
			rsConsul.Open sSQLConsul, CC, 3

		if not rsConsul.EOF then
			nContServ = 0
			do while not rsConsul.EOF
				sCodiceConsul  = rsConsul("codice")
				sDescConsul= rsConsul("descrizione")
		
		%>
		<tr>
				<td class="tbltext1" valign="middle" align="left">
				<input type="checkbox" id="chkConsul<%=nContServ%>" value="<%=sCodiceConsul%>" name="chkConsul<%=nContServ%>">								
			</td>
			
			<td class="tbltext1" valign="middle" align="left">
		
		<%	if sCodiceConsul = "99" then	%>
				<input class="textblack" size="60" maxlength="50" name="DescConsul">
		<%	else		%>
				<%=sDescConsul%>
		<%  end if		%>
		
				<br>			
<!--				<input type="hidden" name="txtCodConsul<%=nContServ%>" value="<%=sCodiceConsul%>">	-->	
			</td>
			
			<td width="20%" class="tbltext1" valign="middle" align="left">
			<%  if nContServ = 0 then  %>	
				Priorit�
			<%	end if  %>

					<input class="textblack" size="20" maxlength="2" name="txtPrioConsul<%=nContServ%>">					
			</td>
		 <%	 
			
			nContServ = nContServ + 1
			rsConsul.movenext
			loop
		 end if
		 %>
		 
			<input type="hidden" name="ContConsul" value="<%=nContServ%>"> 

		</table>

<!-- ----------------------------------------------------------- -->	   
<table width="500" border="0" cellspacing="2" cellpadding="1">   
		<tr>
			<td colspan="4" class="tbltext1">
			<hr>
				
				<br>AI FINI DELLA RICHIESTA DI SERVIZI DI CONSULENZA SPECIALISTICA E/O ASSISTENZA TECNICA  PREVISTI DAL 
				PROGRAMMA &quot;MARCHI D'AREA - STRUMENTI PER LO SVILUPPO DELL'OCCUPAZIONE 
				NEL SETTORE &quot;AGRO-ALIMENTARE&quot; E NELLA PIENA CONSAPEVOLEZZA DELLA RESPONSABILITA'
				PENALE CUI PUO' ANDARE INCONTRO IN CASO DI AFFERMAZIONI MENDACI AI SENSI 
				DELL'ART. '4 DELLA LEGGE 04.01.1968 N.15 E SUCCESSIVE MODIFICHE (L. 191/98 E D.P.R.
				403/98)
				
			</td>
		</tr>
		
		<tr>
			<td colspan="1" align="center" class="tbltext1">
				<b>DICHIARA</b><br><br>
			</td>
		</tr>
		
		<tr>
			<td colspan="1" align="left" class="tbltext1" style="cursor:help" title="Se il Numero Camera di Commercio � inferiore alle 6 cifre, farle precedere da zero es: '001234/0000'; per le ultime 4 cifre inserire l'anno di iscrizione oppure tutti zeri">
				1) di essere iscritto alla Camera di Commercio di  <input type="text" name="txtPrvCameraCom" style="TEXT-TRANSFORM: uppercase;" class="textblack" size="30" readonly>
				<a title="Seleziona Sede" href="Javascript:SelCameraCom()" ID="imgPunto1" name="imgPunto1" onmouseover="javascript:window.status='';return true"><img border="0" src="<%=Session("Progetto")%>/images/bullet1.gif"></a>
				<input type="hidden" name="txtComCameraCom" style="TEXT-TRANSFORM: uppercase;" class="textblack" size="4">
				<!--input type="hidden" id="txtCodComCameraCom" name="txtCodComCameraCom"-->
				<input type="hidden" id="txtCodProvReg" name="txtCodProvReg">
				<br>&nbsp;&nbsp;&nbsp;al numero <input style="TEXT-TRANSFORM:uppercase" class="textblack" size="16" maxlength="11" name="txtNumReg" id="txtNumReg">
				(in caso di erogazione dell'incentivo si impegna a fornire &nbsp;&nbsp; originale del certificato di iscrizione alla 
				CCIIAA non anteriore a due mesi dalla data &nbsp;&nbsp; &nbsp;&nbsp; di pubblicazione dell'avviso, dichiarando che nelle more 
				non sono intervenute &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;modificazioni)
			</td>
		</tr>
		<tr>
			<td colspan="1" align="left" class="tbltext1">
				2) di essere in regola con gli adempimenti previsti dal D. Lgs. 626/94 e successive
				&nbsp;&nbsp;&nbsp;&nbsp;modificazioni in merito al piano di sicurezza e di coordinamento;
			</td>
		</tr>
		<tr>
			<td colspan="1" align="left" class="tbltext1">
			 3) di essere in regola con l'applicazione del CCNL applicato che � il seguente:<br><br>
			&nbsp;&nbsp;&nbsp;&nbsp;
	<%
					EleCcnl = "TCCNL|0|" & date & "||cmbCodCcnl| ORDER BY DESCRIZIONE"
					CreateCombo(EleCcnl)
	%>
			<br>	
			</td>
		</tr>
		<tr>
			<td colspan="1" align="left" class="tbltext1">
				4) di essere in regola con il versamento degli obblighi contributivi ed assicurativi;
			</td>
		</tr>
		<tr>
			<td colspan="1" align="left" class="tbltext1">
				5) di essere in regola con le norme che disciplinano il diritto al lavoro dei disabili;
			</td>
		</tr>
		<tr>
			<td colspan="1" align="left" class="tbltext1">
				6) di aver preso visione dell'avviso e accettarne il contenuto;
			</td>
		</tr>
		<tr>
			<td colspan="1" align="left" class="tbltext1">
				7) di essere disponibile a fornire tutte le altre informazioni, documenti e notizie utili,
				&nbsp;&nbsp;&nbsp;&nbsp;designando a tal fine <input style="TEXT-TRANSFORM:uppercase" class="textblack" size="40" maxlength="50" name="txtRefSede">,
				la persona da &nbsp;&nbsp;&nbsp;&nbsp;contattare presso la sede dell'impresa.
			</td>
		</tr>
		<tr>
			<td colspan="1" align="left" class="tbltext1">
				8) di non aver licenziato negli ultimi 6 mesi, senza giustificato motivo, personale con le stesse &nbsp;&nbsp;
				qualifiche per le quali si richiede l'intervento di consulenza specialistica e/o assistenza tecnica.
			</td>
		</tr>
	</table>


<!------------------------------------------------------------------------------>
<!------------------------------------------------------------------------------>
<!-- ----------------------------------------------------------- -->	   

	
	<table width="520">		
	<tr>
		<td colspan="2" align="center" class="tbltext1" width="500">
			<!--b>Consenso ai sensi dell'art.11 della legge 31 dicembre 1996<br> n. 675/96</b><br><br-->
			<b>Consenso ai sensi dell'art.13 del D. Lgs. <br> 196/2003 </b><br><br>
			Consenso relativo alla legge sulla privacy: vi chiediamo gentilmente di leggere l'informativa relativa 
			alla privacy riportata di seguito e di esprimere<br>il 
			vostro consenso o meno al trattamento dei dati.
		</td>
	</tr>
	<tr>
		<td align="middle" colspan="2">&nbsp;
		</td>
   </tr>
   </table>
<table width="520">   
	<tr>
		<td colspan="2" class="tbltext"><p align="justify">
		
<%
			on error resume next 
			PathFileEdit = "/ProgettoMDA/Testi/Autorizzazioni/info_privacyaz.htm"
			Server.Execute(PathFileEdit)
			If err.number <> 0 Then
				Response.Write "Errore nella visualizzazione. "
				Response.Write "Contattare il Gruppo Assistenza Portale Italia Lavoro "
				Response.Write "all'indirizzo po-assistenza@italialavoro.it "
			End If				
%>
			</p>
		</td>
	</tr>
</table>

<table>	
	<tr>
		<td colSpan="2"><br>
			<center>
			<input type="radio" value="0" id="radConsenso" name="radConsenso"><span class="tbltext">&nbsp;ACCETTO&nbsp;&nbsp;&nbsp;&nbsp;</span>
			<input type="radio" value="1" CHECKED id="radConsenso" name="radConsenso"><span class="tbltext">&nbsp;NON ACCETTO</span>
			<input type="hidden" name="sControllo" value="2">
			</center>
		</td>
	</tr>
</table>
	<br><br>

	<table cellpadding="0" cellspacing="0" width="500" border="0">	
		<tr align="center">
			<td nowrap align="right"><input type="image" name="Conferma" src="<%=Session("progetto")%>/images/conferma.gif"></td value="Conferma">
			<td nowrap align="center"><a href="javascript:document.frmDomIscri.reset()"><img border="0" src="<%=Session("progetto")%>/images/annulla.gif"></td>
			<td nowrap align="left"><a HREF="javascript:history.back()"><img SRC="<%=Session("Progetto")%>/images/indietro.gif" border="0"></td>
		    <td> 
				<input type="hidden" name="graduatoria" value="<%=sGraduatoria%>">
				<input type="hidden" name="gruppo" value="<%=sGruppo%>">
	           	               
	            <input type="hidden" name="IniIsc" value="<%=sDataIsc%>">
	            <input type="hidden" name="datasis" value="<%=sDataSis%>">
	            <input type="hidden" name="bando" value="<%=nBando%>">
	            <input type="hidden" name="descbando" value="<%=sDescBando%>">
	        </td>
		</tr>
	</table>
</form>

<%
end sub

'------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

function ctrlProj()
	ctrlProj = ""
	sSQL = "SELECT COD_TIMPR FROM PROGETTO WHERE ID_PROJ = " & CLng(nIdProgetto)
	set rsProj = CC.Execute(sSQL)
	
	if rsProj.EOF then
		Msgetto("Progetto inesistente")	
		ctrlProj = "KO"
	else
		sCodTimpr = rsProj("COD_TIMPR")
		ctrlProj = "OK"
	end if
	
end function

'------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

sub ctrlBando()

	'	Se la data di sistema non � compresa nel periodo di 
	'   acquisizione domande non devo caricare la domanda.
	set rsAbil = Server.CreateObject("ADODB.Recordset")

	sSQL = " SELECT B.ID_PROJ, B.DESC_BANDO, B.IDGRUPPO, P.IND_GRAD  FROM BANDO B, PROGETTO P " & _
		   "WHERE B.ID_PROJ = P.ID_PROJ "	 &_
			"AND B.ID_BANDO = "& nBando & " AND " & nTime & _
			" BETWEEN B.DT_INI_ACQ_DOM AND B.DT_FIN_ACQ_DOM"
	
	'-----------------------------------------------------------------------------
	' Questo controllo � stato inserito per permettere l'iscrizione di nominativi
	' anche a bando(sport2job) chiuso tale operazione puo essere eseguita solo dagli
	' utenti appartenenti al gruppo ISCR tramite la funzione ISCRIZIONE
	if ucase(nBypassdata) = "SI" then
		sSQL = " SELECT B.ID_PROJ, B.DESC_BANDO, B.IDGRUPPO, P.IND_GRAD  FROM BANDO B, PROGETTO P " & _
			   " WHERE B.ID_PROJ = P.ID_PROJ "	 &_
			   " AND B.ID_BANDO = "& nBando
				
	end if
	' Response.Write "sSQL " & sSQL
    '----------------------------------------------------------------------------
   	rsAbil.Open sSQL, CC, 3

	if  rsAbil.EOF  then
		Msgetto("Periodo di Acquisizione domande non valido")
		sEsitoBando = "KO"
	else
		sDescBando = rsAbil("DESC_BANDO")
	    nIdProgetto = rsAbil("ID_PROJ")
	    sEsitoBando = "OK"
	    sGruppo =rsAbil("IDGRUPPO")
	    sGraduatoria =rsAbil("IND_GRAD")
	end if  
	rsAbil.close
	set rsAbil = nothing	 

end sub

'------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

sub Fine()
%>
<!--#include virtual = "/include/closeconn.asp"-->
<%if session("login") = "" then%>
	<!--#include virtual = "/progettomda/struttura_codaMDA1.asp"-->
<%else%>
	<!--#include virtual = "/strutt_coda1.asp"-->
<%end if%>		
<%
end sub
%>
<!-- ************** ASP Fine *************** -->

<!-- ************** MAIN Inizio ************ -->
<%

dim sDataIsc
dim sSQL
dim CnConn
dim rsAbil

dim nBando
dim nIdProgetto
dim sDescBando
dim sEsitoBando

dim sCodTimpr
dim sDescTimpr

dim sGraduatoria
dim sGruppo, sCommento
dim sCondizione, sCategoria, sDisabilita

dim sProv, nCont ,sDescSvantaggio

sDataIsc = Now()

nBando=clng(Request("txtIdBando"))
'nTime = convdatetodb(Now())
nTime = convdatetodb(Date)
nBypassdata = Request("Bypassdata")

ctrlBando()
if  sEsitoBando = "OK" then
    Inizio()

	if Trim(ctrlProj()) = "OK" then
		sDescTimpr = DecCodVal("TIMPR", "0", "", sCodTimpr, 1)  
		sDataSis = ConvDateToString(Now())
		ImpPagina()
	end if
end if

Fine()
%>
<!-- ************** MAIN Fine ************ -->