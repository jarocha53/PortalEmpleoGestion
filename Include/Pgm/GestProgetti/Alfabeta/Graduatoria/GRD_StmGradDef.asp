<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include virtual = "/strutt_testa2.asp"-->
<!--#include virtual ="/include/openconn.asp"-->
<!--#include virtual = "util/portallib.asp"-->
<!--#include virtual = "util/dbutil.asp"-->
<!--#include virtual = "include/DecCod.asp"-->
<!--#include virtual = "include/SelAreaTerrBandi.asp"-->
<!--#include virtual = "/include/ControlDateVB.asp"-->
<%
if ValidateService(session("idutente"),"GRD_STMGRADDEF", CC) <> "true" then 
	response.redirect "/util/error_login.asp"
end if
%>

<script LANGUAGE="Javascript">
<!--#include Virtual = "/Include/help.inc"-->

function VaiInizio(){
	location.href = "GRD_StmGradDef.asp";
}	

function validator(theForm) {
	appoCImp=frmVisAreaGeoCombo.cmbSede.value
	if (appoCImp == ""){
		alert("Selezionare la sede")
		return false
	}
	return true
}

function PagSucc(){
	var Valore
	
	Valore=document.frmVisAreaGeoCombo.cmbSede.value
	
	if (Valore==""){
		alert('Devi selezionare almeno una sede.')
	}else{
		document.frmVisAreaGeoCombo.action="GRD_RepGrad.asp";
		document.frmVisAreaGeoCombo.submit();		
	}
}

function PagSucc1(){
	
		document.frmVisAreaGeoCombo.action="GRD_RepGrad.asp";
		document.frmVisAreaGeoCombo.submit();		
	
}

function Invia(){
	if (document.frmVisAreaGeoCombo.cmbBando.value==""){
		alert('Devi selezionare un Bando.')
	}else{		
		document.frmVisAreaGeoCombo.action = "GRD_StmGradDef.asp";
		document.frmVisAreaGeoCombo.submit();				
	}
}
</script>
<%sub inizio()%>
<table border="0" width="520" cellspacing="0" cellpadding="0" height="81">
	   <tr>
	     <td width="500" background="<%=Session("Progetto")%>/images/titoli/strumenti2b.gif" height="81" valign="bottom" align="right">
	       <table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
	         <tr>
	           <td width="100%" valign="top" align="right"><b class="tbltext1a">Gestione Progetti</span></b></td>
	         </tr>
	       </table>
	     </td>
	   </tr>
	</table>

	<br>
	<table cellpadding="0" cellspacing="0" width="500" border="0">
		<tr height="18">
			<td class="sfondomenu" height="18" width="67%">
			<span class="tbltext0"><b>&nbsp;STAMPA DELLA GRADUATORIA</b></span></td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
			<td valign="middle" align="right" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1"></td>
		</tr>
		<tr width="371" class="SFONDOCOMM">
			<td colspan="3">
				<br>Per visualizzare e stampare i dati della graduatoria inserire i campi richiesti.
				
				
				<a href="Javascript:Show_Help('/Pgm/help/Gestprogetti/alfabeta/graduatoria/GRD_StmGradDef')">
				<img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
			</td>
		</tr>
		<tr height="2">
			<td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>
	<br><br>
<%end sub%>


<%sub ImpBandi()%>
<form name="frmVisAreaGeoCombo" action method="post">
<table cellpadding="1" cellspacing="0" width="400" border="0">	
	<input type="hidden" name="Valore" value="1">	
<%		
	if nIdBando <> "" then
%>
	<input type="hidden" name="cmbBando" value="<%=nIdBando%>">
	<tr>
		<td class="tbltext1" align="left" width="100">
			<b>Bando</b>
		</td>
		<td class="tbltext" align="left" width="300">
			<b><%=sDescBando%></b>
		</td>
	</tr>
<%
	else 
%>
	<tr>
		<td class="tbltext1" align="left" width="100">
			<b>Bando</b>
		</td>
		<td align="left" width="300">
		 <span class="tbltext3">
<%
              strcombo= session("iduorg") & "||cmbBando|onchange='Javascript:Invia()'" 
			  CreateBandi strcombo			
%>				
         </span>
		</td>
       </tr> 
<%	end if%>	
     </table>
<%end sub

sub CodTimpr()
%>
	 <script>
		frmVisAreaGeoCombo.action ='GRD_RepGrad.asp';
		frmVisAreaGeoCombo.submit() 	
	</script>	
<%end sub		
	
sub ImpProvincia()
%>	
	
<table cellpadding="1" cellspacing="0" width="400" border="0">	
<%
	if sProv <> "" then
%>
	<tr align="left">	
		<td class="tbltext1" align="left" width="100">
			<b>Provincia </b>
		</td>
		<td class="tbltext" align="left" width="300">
				
				<b><option value="<%=sProv%>"><%=DecCodVal("PROV", 0, "", sProv,"")%></option></b>
		</td>
	</tr>
		
<%
	else
	'Response.Write sProv
	sMask = SetProvBando(nIdBando,session("idUorg"))
%>	
		<tr>	
			<td align="left" class="tbltext1" width="100">
			<b>Provincia</b>
			</td>
			<td align="left" width="300">			 	 
				<select NAME="cmbProvBan" class="textblack" onchange="Invia()">			 
				<option>&nbsp;</option>
<%				nProvSedi = len(sMask)
			      if nProvSedi <> 0 then
	                pos_ini = 1 	                
	                    for i=1 to nProvSedi/2
	                   		sTarga = mid(sMask,pos_ini,2)										' DecCodVal(nome_tabella, isa, dt_decor, codice, flag_conn)
%>
		                   <option value="<%=sTarga%>"><%=DecCodVal("PROV", 0, "", sTarga,"")%></option>
<%	                    
	                    pos_ini = pos_ini + 2
	                    next
                    end if
%>
			         </select>
				</td>
		</tr>
		
	<%
		end if	%>	
		</table>
	<%end sub
	
'------------------------------------------------------------------------------------------------------------------------------
sub ImpSede()
		sStatus = 1
		sIntestazione = ""
%>	
	
	<table cellpadding="0" cellspacing="0" width="400" border="0">
	<input type="hidden" name="MOD" value="3">
	<input type="hidden" name="cmbProvBan" value="<%=sProv%>">
	
	<input type="hidden" name="Status" value="<%=sStatus%>">
	<input type="hidden" name="Intestazione" value="<%=sIntestazione%>">	
		<%		

		sSQL = "select si.id_sede,si.descrizione " & _ 
				" from sede_impresa si,impresa i, bando_posti p " & _
                " where si.id_impresa = i.id_impresa " & _ 
                " and si.id_sede = p.id_sede " & _ 
                " and p.id_bando =" & nIdBando & " and prv= '" & sProv & "'"
		'Response.Write ssql
		'Response.End 
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
			set rsSede = CC.Execute(sSQL)
			'sDescProj=rsSede("desc_proj")
								
		if rsSede.eof then	%>
			<br>
			<tr>
				<td class="tbltext3" align="center" colspan="2">
					<input type="hidden" name="DescProgetto" value="<%=sDescProj%>">
					<b>Non ci sono sedi per la provincia selezionata</b>
				</td>
			</tr>	
				<td>
					<br>
				</td>
			<tr align="center">
				<td nowrap colspan="2">
					<a href="javascript:VaiInizio()" onmouseover="javascript:window.status=' '; return true"><img src="<%=session("Progetto")%>/images/indietro.gif" border="0" name="imgPunto2"></a>
				</td>
			</tr>
<%			
		else
%>		
			<tr>
				<td align="left" width="100" class="tbltext1">
				<input type="hidden" name="DescProgetto" value="<%=sDescProj%>">
					<b>Sede</b>
				</td>
				<td align="left" width="300">
					<select ID="cmbSede" name="cmbSede" class="textblacka">
						<option value></option>
<%
					do while not rsSede.EOF
%>
						<option value="<%=rsSede("ID_SEDE") & "|" & rsSede("DESCRIZIONE")%>"><%=rsSede("DESCRIZIONE")%></option>
<%
						rsSede.MoveNext
					loop
%>
					</select>
				</td>
			<tr>	
				<td>
					<br>
				</td>	
			</tr>
<%
			impammessi(1)
		end if
%>
		</table>
		
<%end sub%>	
<% sub Impammessi(contr)%>		
		
		<br><br>
	<table width="400" border="0" cellspacing="1" cellpadding="0">
		<tr> 
			<td width="200" align="left">
				<select ID="cmbStatus" name="cmbStatus" class="textblacka">
				<option value="1">Ammessi</option>
				<option value="0">Non Ammessi</option>
				</select>
			</td>
			<td width="200" align="left">
				<select ID="cmbOrder" name="cmbOrder" class="textblacka">
				<option value="0">Ordine Alfabetico</option>
				<option value="1">Posizione in graduatoria</option>
				</select>
			</td>
		</tr>
	</table>
	<br><br>
		<table cellpadding="0" cellspacing="0" width="300" border="0">	
			<tr align="left">
	<%		if contr=1 then %>
				<td nowrap width="150"><a href="javascript:PagSucc()"><img border="0" src="<%=Session("progetto")%>/images/conferma.gif"></td>			
	<% else %>
	            <td nowrap width="150"><a href="javascript:PagSucc1()"><img border="0" src="<%=Session("progetto")%>/images/conferma.gif"></td> 
	           <% end if %>
				<td nowrap><a href="javascript:VaiInizio()" onmouseover="javascript:window.status=' '; return true"><img src="<%=session("Progetto")%>/images/indietro.gif" border="0" name="imgPunto2"></a><td>
		    </tr>
		</table>
	</form>	
<%end sub%>	
<% sub Fine()%>
<!--#include virtual = "/strutt_coda2.asp"-->
<!--#include virtual ="/include/closeconn.asp"-->
<% end sub%>
				<!---------------------MAIN------------------------------>
	
<%	dim nIdBando
	dim sDescBando
	dim sBando
	dim sProv
	dim sStatus
	dim sIntestazione
	dim sIdSede

	nIdBando = Request("cmbBando")
	sProv    = Request("cmbProvBan")
	sStatus  = Request("sStatus")
	sIntestazione = Request("Intestazione")
	sIdSede  = Request("cmbSede")
	DescProj= Request("DescProgetto")
	
	if nIdBando <> "" then		
		
		sDescBando= DecBando(nIdBando)
				
	'if sBando <> "" then		
	'	nIdBando=Mid(sBando,1 ,InStr(1, sBando,"|") - 1)
	'	sDescBando=Mid(sBando,InStr(1, sBando,"|") + 1)
	end if	
	inizio()
	ImpBandi()
	
	'sSql="SELECT COD_TIMPR FROM PROGETTO WHERE AREA_WEB='" & mid(session("progetto"),2) & "'"
	if nIdBando <> "" then	 
	sSql="SELECT COUNT(*) AS AGGREGAZIONE FROM BANDO_POSTI WHERE ID_BANDO = " & nIdBando & " AND ID_SEDE <> 0"
	'Response.Write SSQL
		set rsCodTimpr=Server.CreateObject("ADODB.recordset")
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
			rsCodTimpr.Open sSql,CC,3
			'if 	rsCodTimpr.eof then
			'	sCod = ""
			'	else	
			sCod = rsCodTimpr("AGGREGAZIONE")
			'end if
			'Response.Write "scod" & sCod
			rsCodTimpr.close
			set rsCodTimpr=nothing
		if sCod <> "0" then		
			ImpProvincia()
				if sProv<>"" then
					ImpSede()
'					Impammessi(1)
				end if
		else			
			Impammessi(2)			
		end if			
	end if
	Fine()
%>			

	
