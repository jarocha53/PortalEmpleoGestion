<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%'Option Explicit 
   Response.ExpiresAbsolute = Now() - 1 
   Response.AddHeader "pragma","no-cache"
   Response.AddHeader "cache-control","private"
   Response.CacheControl = "no-cache"
%>

<!-- #include virtual="/strutt_testa2.asp"-->
<!--#include virtual = "/include/openconn.asp"-->
<!--#include virtual="/util/portallib.asp"-->
<%
'if ValidateService(session("idutente"),"GRD_STMGRADPROV", CC) <> "true" then 
'	response.redirect "/util/error_login.asp"
'end if
%>
<!-- ************** Javascript inizio ************ -->
<script LANGUAGE="JavaScript">
<!--#include virtual = "/Include/help.inc"-->


function VaiInizio(){
	location.href = "GRD_StmGradProv.asp";
}	

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

function Stampa(){
	if (confirm("La tabella sta per essere stampata")){
			window.print();                                                    
	}
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

function Scarica(report){
	f="temp\\"+ report;
	fin=window.open(f,"","toolbar=0;location=0,directories=0,status=0,menubar=1,scrollbars=1,resizable=1,copyhistory=0,width=450,height=340,screenX=200,screenY200");
}

</script>

<!-- ******************  Javascript Fine *********** -->
<!-- ************** ASP inizio *************** -->

<!--#include virtual ="/include/controldatevb.asp"-->

<%
'------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

sub ImpVariabili()

	sStatus =		Request("Status")
	sCentroImpiego =Request("cmbCImp")
	sBando =		Request("Bando")
	sIntestazione =	Request("Intestazione")

	sPosizione = InStr(1,sCentroImpiego, "|")
	sIdCImpiego = Mid(sCentroImpiego, 1, sPosizione - 1)
	sDescImpiego = Mid(sCentroImpiego, sPosizione + 1)

	if sStatus="0" then
		sTitolo = "Lista ammissibili al progetto Alfabetizzazione per il centro di Impiego di " & sDescImpiego
		sInfo= "La Lista, disposta in ordine alfabetico, riporta i nominativi di tutti i candidati ammissibili al progetto Alfabetizzazione. L�inclusione nelle liste di partecipazione � avvenuta in seguito al riscontro positivo dei requisiti disposti dal bando e attinenti ai criteri di selezione."
	'	sTipoGrad="Provvisoria"
		sOrder = " ORDER BY COGNOME,NOME,DT_NASC"
	else
		sTitolo = "Graduatoria definitiva per il centro di Impiego di " & sDescImpiego 
		sOrder = " ORDER BY DECODE(C.ESITO_GRAD,'S',2,'X',1,0) DESC, C.PUNTEGGIO DESC , C.DT_DOM"
	'	sTipoGrad="Definitiva"
		sInfo= "La graduatoria, disposta in ordine progressivo, riporta i nominativi degli ammessi. L�inclusione nelle liste di partecipazione � avvenuta in seguito al riscontro positivo dei requisiti disposti dal bando e attinenti ai criteri di selezione. La graduatoria definitiva resa pubblica, � stata curata da Italia Lavoro S.p.A in collaborazione con i Centri per l�Impiego provinciali."
	end if
	
end sub

'----------------------------------------------------------------------------------------------------------------------------------------------------  

sub Inizio()
%>
	<table border="0" width="520" cellspacing="0" cellpadding="0" height="81">
	   <tr>
	     <td width="500" background="<%=Session("Progetto")%>/images/titoli/strumenti2b.gif" height="81" valign="bottom" align="right">
	
	       <table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
	         <tr>
	             <td width="100%" valign="top" align="right">
	                 <b class="tbltext1a">Gestione Progetti</span></b>
	             </td>
	         </tr>
	       </table>
	     </td>
	   </tr>
	</table>
	<br>
	<table cellpadding="0" cellspacing="0" width="500" border="0">
		<tr height="18">
			<td class="sfondomenu" height="18" width="67%">
			<span class="tbltext0"><b>Lista ammissibili al progetto Alfabetizzazione</b></span></td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
			<td valign="middle" align="right" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1"></td>
		</tr>
		<tr width="371" class="SFONDOCOMM">
			<td colspan="3">Testo di prova<a href="Javascript:Show_Help('/Pgm/help/GestProgetti/Alfabeta/Graduatoria/GRD_StmGrad')" onmouseover="javascript:window.status=' '; return true"><img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
			</td>
		</tr>
		<tr height="2">
			<td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>
	<br>
<%	
if sIntestazione <> "" then
%>
	<table width="500" border="0" cellpadding="0" cellspacing="0">
		<tr>
	        <td width="80%" align="middle">
				<b><font face="Verdana" size="2" Color="#008caa">
				<%=sIntestazione%></font></b>
			</td>
	    </tr>
	</table>	
<%
end if
%>
<br>
<table width="500" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td align="center" class="tbltext3"><%=sTitolo%></td>
    </tr>
</table>
<%
	
end sub

'------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

sub ImpostaPag()

	set rsRR = server.CreateObject("ADODB.Recordset")

	sSQL="SELECT a.COD_FISC, a.COGNOME||' '||a.NOME AS NOMINATIVO, " & _
	     "a.SESSO, TO_CHAR(a.DT_NASC,'DD/MM/YYYY') AS DT_NASC, " & _
	     "TO_CHAR(d.DT_DEC_DIS,'DD/MM/YYYY') AS DT_DEC_DIS, " & _
	     "TADES.DESCRIZIONE AS TITSTUD, " & _
	     "b.DESCOM ||' ('|| PRV_RES ||')' AS RESIDENZA, a.NUM_TEL " & _
	     "FROM PERSONA a, COMUNE b, DOMANDA_ISCR c, " & _
	     "STATO_OCCUPAZIONALE d, TISTUD e, TADES " & _
	     "WHERE c.COD_ESITO IS NULL AND " & _
	     "c.ID_PERSONA=a.id_persona And c.ID_PERSONA=d.id_persona " & _
	     "And c.ID_PERSONA=e.id_persona AND a.COM_RES=b.codcom AND " & _
	     "TADES.ISA='0' AND TADES.Nome_Tabella='TSTUD' AND TADES.Codice=cod_tit_stud " & _
	     "AND d.ID_CIMPIEGO=" & sIdCImpiego & sOrder

	'Response.Write "sSQL: " & sSQL & "<br>"
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	rsRR.open sSQL,CC, 3

	if rsRR.EOF then
		%>
		<br>
		<table align="center">
			<tr>
				<td class="tbltext3"><b>Non ci sono candidati ammissibili</b></td>
			</tr>	
		</table>

		<%
		exit sub
	end if
	n = 1
	%>
	<br>
	<span class="tbltext"><%=sInfo%></span>
	<br>
	<br>
	<table width="500" border="0" cellpadding="1" cellspacing="1">
		<tr class="sfondocomm">
			<td align="center">N.</td>
			<td align="center">Cod. Fiscale</td>
			<td align="center">Nominativo</td>
			<td align="center">Sesso</td>
			<td align="center">Data Nasc.</td>
			<td align="center">Dec. Disocc.</td>
			<td align="center">Tit. Studio</td>
			<td align="center">Residenza</td>
			<td align="center">Rec. Tel.</td>
		</tr>
	<%
	
	lPath = Server.MapPath("/") & "/pgm/GestProgetti/AlfaBeta/Graduatoria/TEMP/"
	sNomeFile=Session("login") & "_" & sBando & "_" & sDescImpiego & ".xls"

	Set fso = CreateObject("Scripting.FileSystemObject")
	Set a = fso.CreateTextFile(lPath & sNomeFile, True)
'	a.writeline "Graduatoria " & sTipoGrad & " per il centro di impiego di " & sDescImpiego
	a.writeline sTitolo 
	a.WriteLine ""
	a.WriteLine "Codice Fiscale" & chr(9) & "Nominativo" &  chr(9) & _
				"Sesso" & chr(9) & "Data Nascita" & chr(9) & _
				"Dec. Disoccupazione" & chr(9)& "Titolo di Studio" & chr(9) & _
				"Residenza" & chr(9) & "Rec. Telefonico"	        
    rptIscritti = rsRR.GetString (2, , chr(9),chr(13), " ")
	a.WriteLine rptIscritti
	a.Close
	
	rsRR.MoveFirst

	Do while  not rsRR.EOF 
		sCodFisc = rsRR.Fields("COD_FISC")
		sNominat = rsRR.Fields("NOMINATIVO")	
		sSesso = rsRR.Fields("SESSO")	
'		dDataNasc = convdatetostring(rsRR.Fields("DT_NASC"))
'		dDecDisocc = convdatetostring(rsRR.Fields("DT_DEC_DIS"))
		dDataNasc = rsRR.Fields("DT_NASC")
		dDecDisocc = rsRR.Fields("DT_DEC_DIS")
		sTitStudio = rsRR.Fields("TITSTUD")	
		sRedidenza = rsRR.Fields("RESIDENZA")	
		sNumTel = rsRR.Fields("NUM_TEL")
	%>
		<tr class="tblsfondo">
			<td class="tbltext1"><%=n%></td>
			<td class="tbltext1"><%=sCodFisc%></td>
			<td class="tbltext1"><%=sNominat%></td>
			<td class="tbltext1"><%=sSesso%></td>
			<td class="tbltext1"><%=dDataNasc%></td>
			<td class="tbltext1"><%=dDecDisocc%></td>
			<td class="tbltext1"><%=sTitStudio%></td>
			<td class="tbltext1"><%=sRedidenza%></td>
			<td class="tbltext1"><%=sNumTel%></td>
		</tr>
	<%
		rsRR.MoveNext
		n = n + 1	
	loop
	rsRR.Close
	set rsRR = nothing
%>

</table>

<p align="center">
	<a href="javascript:Stampa()" class="textred">
		<b>Stampa lo Schema</b>
	</a>
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	<a href="javascript:Scarica('<%=sNomeFile%>')" class="textred">
		<b>Visualizza File Report</b>
	</a>
</p>
<%
end sub

'------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

sub Fine()
%>
<br><br>
<table cellpadding="0" cellspacing="0" width="300" border="0">	
	<tr align="center">
		<td nowrap><a href="javascript:VaiInizio()" onmouseover="javascript:window.status=' '; return true"><img src="<%=session("Progetto")%>/images/indietro.gif" border="0" name="imgPunto2"></a><td>
    </tr>
</table>

<!--#include virtual = "/include/closeconn.asp"-->
<!--#include virtual = "strutt_coda2.asp"-->
<%
end sub

%>
<!-- ************** ASP Fine *************** -->

<!-- ************** MAIN Inizio ************ -->

<%

dim sStatus
dim sCentroImpiego
dim sBando
dim sIntestazione

dim sPosizione
dim sIdCImpiego
dim sDescImpiego

dim sTitolo
dim sInfo
'dim sTipoGrad
dim sOrder

dim sSQL, rsRR

dim lPath
dim sNomeFile
dim fso, a
dim rptIscritti

dim sCodFisc
dim sNominat
dim sSesso
dim dDataNasc
dim dDecDisocc
dim sTitStudio
dim sRedidenza
dim sNumTel

ImpVariabili()

Inizio()

ImpostaPag()

Fine()

%>
<!-- ************** MAIN Fine ************* -->
