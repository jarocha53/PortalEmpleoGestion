<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #include virtual="/strutt_testa2.asp"-->
<!--#include virtual="/util/portallib.asp"-->
<!--include file = "../../include/ckProfile.asp"-->
<!--#include virtual = "include/SelAreaTerrBandi.asp"-->
<!--#include virtual = "include/openconn.asp"-->

<%
If Not ValidateService(session("idutente"),"GRD_SELSITUAZ", CC) then 
	response.redirect "/util/error_login.asp"
End If
%>

<script LANGUAGE="Javascript">
<!--#include Virtual = "/include/ControlString.inc"-->
<!--#include Virtual = "/include/ControlCodFisc.inc"-->
<!--#include Virtual = "/Include/help.inc"-->

//CARICAMENTO PAGINA//
function Reload(n)
{
	var nSel
	nSel = frmProvincia.cmbProv.selectedIndex
	nProvincia = frmProvincia.cmbProv.options[nSel].value

	if (nProvincia == "")
	{
		alert("Selezionare una Provincia")
		frmProvincia.cmbProv.focus();
		return false;
	}
	else
		document.frmProvincia.submit();	
}


function ControllaDati()
{

document.frmRicerca.txtNome.value=TRIM(document.frmRicerca.txtNome.value)
document.frmRicerca.txtCognome.value=TRIM(document.frmRicerca.txtCognome.value)
document.frmRicerca.txtDataN.value=TRIM(document.frmRicerca.txtDataN.value)
document.frmRicerca.txtCodFisc.value=TRIM(document.frmRicerca.txtCodFisc.value)

if ((document.frmRicerca.txtCognome.value=="")&&
	(document.frmRicerca.txtNome.value=="")&&
	(document.frmRicerca.txtDataN.value=="")&&
	(document.frmRicerca.txtCodFisc.value==""))
	{
		alert('Non puoi effettuare la ricerca se non compili alcun campo!')
		document.frmRicerca.txtCognome.focus()
		return false
	} 
	
if (((document.frmRicerca.txtCognome.value!="")||
	 (document.frmRicerca.txtNome.value!="")||
	 (document.frmRicerca.txtDataN.value!="")
	) &&
	(document.frmRicerca.txtCodFisc.value!="")
   ){	
	   var apice
	   apice="'"
	   alert('Inserire in alternativa i Dati Anagrafici o il Codice Fiscale')
	    document.frmRicerca.txtCodFisc.focus()
	   return false	
	 }
	
// Controlli su ricerca con dati anagrafici	
if (document.frmRicerca.txtCodFisc.value ==""){ 
	 // Controlli Cognome 
	 if (document.frmRicerca.txtCognome.value=="")
	    {
			alert('Campo Cognome obbligatorio')
			document.frmRicerca.txtCognome.focus()
			return false
		}
		 
	 sCognome=ValidateInputStringWithOutNumber(document.frmRicerca.txtCognome.value)
     if  (sCognome==false)
		{
			alert("Cognome formalmente errato.")
			document.frmRicerca.txtCognome.focus() 
			return false
		}	 
	
	 // Controlli Nome 	 
	  if (document.frmRicerca.txtNome.value!="") {
	  
			sNome=ValidateInputStringWithOutNumber(document.frmRicerca.txtNome.value)
			if  (sNome==false)
				{
					alert("Nome formalmente errato.")
		    		document.frmRicerca.txtNome.focus() 
					return false
				}	 
	   }
	 
	 // Controllo Data Nascita
		var sDataNascita
		sDataNascita=document.frmRicerca.txtDataN.value	 
		
		if (sDataNascita !="") 
		{ 
		    
			if (!ValidateInputDate(sDataNascita))
				{
					document.frmRicerca.txtDataN.focus() 
					return false
				}
		
	//controllo che l'anno di nascita sia inferiore all'anno odierno     
		    var intAnnoOdierno
			var sDataOdierna
			var sDataCompleta
			var intAnno

			sDataCompleta=document.frmRicerca.txtDataN.value

			intAnno=sDataCompleta.substr(6,4)

			sDataOdierna = new Date();
			intAnnoOdierno= sDataOdierna.getFullYear();

			if (intAnno >=intAnnoOdierno){
				alert ("L'anno di nascita deve essere precedente all'anno della data odierna!")
				return false
			} 
       }
}
else {
     // Controlli su ricerca con codice fiscale
      //Contollo la validit� del Codice Fiscale
	CodFisc = document.frmRicerca.txtCodFisc.value.toUpperCase()
	//Codice Fiscale deve essere di 16 caratteri
	if ((document.frmRicerca.txtCodFisc.value.length != 16) &&
		(document.frmRicerca.txtCodFisc.value != ""))
		{
			alert("Formato del Codice Fiscale errato.")
			document.frmRicerca.txtCodFisc.focus() 
		return false
		}
	if ((!ControllChkCodFisc(CodFisc)) &&
		(document.frmRicerca.txtCodFisc.value != ""))
		{
			alert("Codice Fiscale Errato.")
			document.frmRicerca.txtCodFisc.focus()
			return false
		}		
}
	return true		
}		

</script>
</head>

<body>
	<table border="0" width="520" cellspacing="0" cellpadding="0" height="81">
	   <tr>
	     <td width="500" background="<%=Session("Progetto")%>/images/titoli/strumenti2b.gif" height="81" valign="bottom" align="right">
	       <table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
	         <tr>
	           <td width="100%" valign="top" align="right"><b class="tbltext1a">Gestione Progetti</b></td>
	         </tr>
	       </table>
	     </td>
	   </tr>
	</table>
	<br>
	<table cellpadding="0" cellspacing="0" width="500" border="0">
		<tr height="18">
			<td class="sfondomenu" height="18" width="67%">
			<span class="tbltext0"><b>&nbsp;FASCICOLO PERSONALE</b></span></td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
			<td valign="middle" align="right" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1">(*) campi obbligatori</td>
		</tr>
		<tr width="371" class="SFONDOCOMM">
			<td colspan="3">
				Selezionare una Provincia per accedere alla pagina di ricerca dei nominativi degli iscritti al bando.
				Per effettuare la ricerca compilare i campi obbligatori <b>Cognome/Nome</b> o in alternativa il <b>Codice Fiscale</b>.
				Nel primo caso � possibile digitare anche parzialmente i dati anagrafici, mentre il codice fiscale
				va riportato per intero.
				<a href="Javascript:Show_Help('/Pgm/help/Gestprogetti/alfabeta/Graduatoria/GRD_SelSituaz')">
				<img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0">
				</a>
			</td>
		</tr>
		<tr height="2">
			<td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>

<%
dim sMask

sModo = Request("MOD")
select case sModo
	case 0
%>
	<br><br>
	<form method="POST" action="GRD_SelSituaz.asp" name="frmProvincia">
	<input type="hidden" VALUE="1" name="MOD">
	<table width="500" border="0" cellspacing="2" cellpadding="1">
<%		sMask= SetProvUorg(session("idUorg"))
		If sMask = "" then
%>		<tr>
			<td COLSPAN="2">
				&nbsp;
			</td>
		</tr>
		<tr>
			<td class="tbltext3" align="center" COLSPAN="2">
				<b>Non risultano province associate all'Unita' Organizzativa.</b>
			</td>
		</tr>
<%		Else
%>		<tr>
			<td class="tbltext1" width="35%">
				<b>Selezionare una Provincia</b>
			</td>
			<td align="left">
				<select NAME="cmbProv" class="textblack" onchange="Reload(0)">			 
					<option>&nbsp;</option>
					<option value="ALL">-Tutte-</option>
<%					nProvSedi = len(sMask)
					if nProvSedi <> 0 then
						pos_ini = 1 	                
						for i=1 to nProvSedi/2
							sTarga = mid(sMask,pos_ini,2)
							
							Response.Write	"<OPTION "
							Response.write	"value ='" & sTarga & "'"
							'if sProv = sTarga then
							'	Response.Write " selected"
							'end if
							Response.write  "> " & DecCodVal("PROV", 0, "", sTarga,"") & "</OPTION>"
							
							pos_ini = pos_ini + 2
						next
					end if
%>
				</select>
			</td>
		</tr>
<%		End If
%>	
	</table>
	</form>
<%		
	case 1

	sProvSel = Request.Form("cmbProv")
	If sProvSel="ALL" then
		sProvincia="TUTTE"
	else
		sProvincia=DecCodVal("PROV", 0, "", sProvSel,"")
	end if
%>
	<br>
	<table width="500" border="0" cellspacing="2" cellpadding="1">
		<tr>
			<td class="tbltext1" width="35%">
				<b>Provincia selezionata</b>
			</td>
			<td class="tbltext">
				<b><%=sProvincia%>
				</b>
			</td>
		</tr>
	</table>
<%
	'sSQL = "SELECT COUNT(ID_BANDO) as ContaBandi" &_
	'" FROM BANDO,TADES WHERE substr(ID_BANDO,1,2) IN ('" & sProvSel & "')" &_
	'" AND NOME_TABELLA='PROV' AND CODICE=SUBSTR(ID_BANDO,1,2)" &_
	'" AND VALORE IS NOT NULL ORDER BY ID_BANDO"
					
	'sSQL = "SELECT ID_BANDO from BANDO WHERE SIGLA_PRG='" & Session("progetto") & "' AND substr(ID_BANDO,1,2) IN ('" & sProvSel & "') ORDER BY ID_BANDO"
%>					
	<form method="POST" action="GRD_VisSituaz.asp" name="frmRicerca">
	<input TYPE="HIDDEN" NAME="hdnProvincia" value="<%=sProvSel%>">
	<table border="0" CELLPADDING="1" CELLSPACING="2" width="500">
		<tr height="17">
			<td width="35%" class="tbltext1">
				<b>Cognome</b>*
			</td>
			<td class="tbltext1">
				<b><input class="textblack" type="text" name="txtCognome" style="TEXT-TRANSFORM: uppercase" size="35" value></b>
			</td>
		</tr>
		<tr height="17">
			<td width="35%" class="tbltext1">
				<b>Nome</b>
			</td>
			<td class="tbltext1">
				<b><input class="textblack" type="text" name="txtNome" style="TEXT-TRANSFORM: uppercase" size="35" value></b>
			</td>
		</tr>
		<tr height="17">
			<td width="200" class="tbltext1">
				<b>Data di Nascita</b> 
				<br>(gg/mm/aaaa)
			</td>
			<td width="400" class="tbltext1">
				<b><input class="textblacka" type="text" name="txtDataN" maxlength="10" size="10">
			</td>
		</tr> 
		<tr height="17">
			<td colspan="2">
				&nbsp;
			</td>
		</tr>
		<tr height="2">
			<td width="100%" colspan="2" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
		</tr>
		<tr height="2">
			<td width="100%" colspan="2" background>&nbsp;</td>
		</tr>
		<tr height="17">
			<td colspan="2">
				&nbsp;
			</td>
		</tr>
		<tr height="17">
			<td width="35%" class="tbltext1">
				<b>Codice Fiscale</b>*
			</td>
			<td class="tbltext1">
				<b><input class="textblack" type="text" name="txtCodFisc" style="TEXT-TRANSFORM: uppercase" maxlength="16" size="25" value>
			</td>
		</tr>		
	</table>
	<br>
	<br>
	<table width="500" border="0">
		<tr align="center">
			<td><a href="GRD_SelSituaz.asp" onmouseover="javascript:window.status=' '; return true"><img src="<%=session("Progetto")%>/images/indietro.gif" border="0" name="imgPunto2"></a></td>
			<td><input type="image" name="Invia" src="<%=Session("Progetto")%>/images/lente.gif" border="0" value="Conferma" onclick="return ControllaDati()"></td>
		</tr>
	</table> 	 
	</form>
<%	
end select%>
<!--#include virtual = "include/closeconn.asp"-->
<!--#include virtual = "/strutt_coda2.asp"-->
</body>


