<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%'Option Explicit 
   Response.ExpiresAbsolute = Now() - 1 
   Response.AddHeader "pragma","no-cache"
   Response.AddHeader "cache-control","private"
   Response.CacheControl = "no-cache"
%>

<!-- #include virtual="/strutt_testa2.asp"-->
<!--#include virtual = "/include/openconn.asp"-->
<!--#include virtual="/util/portallib.asp"-->
<%
'if ValidateService(session("idutente"),"GRD_SELVALIDGRAD", CC) <> "true" then 
'	response.redirect "/util/error_login.asp"
'end if
%>
<!-- ************** Javascript inizio ************ -->
<script LANGUAGE="Javascript">

function VaiInizio(){
	location.href = "GRD_SelValidGrad.asp";
}	

</script>

<!-- ******************  Javascript Fine *********** -->
<!-- ************** ASP inizio *************** -->

<!--#include virtual = "/include/ControlDateVB.asp"-->
<!--#include virtual = "/include/SysFunction.asp"-->

<%
'----------------------------------------------------------------------------------------------------------------------------------------------------  

sub Msgetto(Msg)
%>
	<br>
	<table align="center">
		<tr>
			<td class="tbltext3"><b><%=Msg%></b></td>
		</tr>	
	</table>
<%
end Sub

'-------------------------------------------------------------------------------------------------------------------------------

sub Inizio()
%>
	<table border="0" width="520" cellspacing="0" cellpadding="0" height="81">
	   <tr>
	     <td width="500" background="<%=Session("Progetto")%>/images/titoli/strumenti2b.gif" height="81" valign="bottom" align="right">
	
	       <table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
	         <tr>
	             <td width="100%" valign="top" align="right">
	                 <b class="tbltext1a">Gestione Progetti</span></b>
	             </td>
	         </tr>
	       </table>
	     </td>
	   </tr>
	</table>
	<br>
	<table cellpadding="0" cellspacing="0" width="500" border="0">
		<tr height="18">
			<td class="sfondomenu" height="18" width="67%">
			<span class="tbltext0"><b>Validazione ammissibili</b></span></td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
			<td valign="middle" align="right" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1"></td>
		</tr>
		<tr width="371" class="SFONDOCOMM">
			<td colspan="3">Testo di prova<a href="Javascript:Show_Help('/Pgm/help/GestProgetti/Alfabeta/Graduatoria/GRD_CnfValidGrad')" onmouseover="javascript:window.status=' '; return true"><img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
			</td>
		</tr>
		<tr height="2">
			<td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>
	<br>
<%
end sub

'-----------------------------------------------------------------------------------------------------------------------------------------------------------

sub Elabora()

	sBando = Request("cmbBando")

	nChk = Request("txtnChk") 
	nChk = nChk - 1

	CC.beginTrans

	for i = 1 to nChk
		sApCh	= "chkSelez" & i
		sChk	= Request(sApCh)
		if sChk > "0" then
			'prendo solo i dati dei check selezionati
			'Faccio l'aggiornamento sulla tabella DOM_ISCR mettendo
			' N nell'Esito della Graduatoria e X nel Codice Esito
			dim sTimeTemp 
			dim sIDPers
			dim sErrore, sErrore1 
			dim sEsitoGrad		
			
			sErrore = ""
			
			sIDPers = clng(Request("IDPers" & i))
			'response.write "IDPERSONA=" & sIDPers

			sTimeTemp = Request("TimeTemp" & i)
			sEsitoGrad = Request("Esito" & i)
			
			sEsito = "N"
			sCodEsito = "X"
			
			sSQL = "UPDATE DOMANDA_ISCR SET " & _
					"ESITO_GRAD='" & sEsito & "', " & _
					"COD_ESITO='" & sCodEsito & "' " & _
					"WHERE ID_PERSONA =" & sIDPers 
																								
			Errore = EseguiNoCTrace(session("Progetto"),sIDPers,"DOMANDA_ISCR",session("idutente"), "", "","MOD", sSQL, 1,sTimeTemp,CC)

			if Errore <> "0" then
				sErrore = Errore
				exit for
			end if
		end if
	next

	if len(sErrore) <> 0 then
		CC.RollbackTrans 
		Msgetto("Modifica Lista ammissibili non effettuabile.<br>" & sErrore)
		exit sub	
	end if
	
	sAutorizz = Request("chkAutorizz")
	if sAutorizz = "S" then
		
		sApCImp = trim(cstr(Request("cmbCImp")))
		if sApCImp <> "" then
			sCImpiego = CInt(Mid(sApCImp, 1, InStr(1, sApCImp, "|") - 1))
			sDescCImpiego = Trim(Mid(sApCImp, InStr(1, sApCImp, "|") + 1))
		end if
		
		sMittente		= session("idutente")
		sTesto			= "Si richiede l'elaborazione della graduatoria " & _
							"per il bando " & sBando & " e il centro dell'" & _
							"impiego di " & sDescCImpiego
		sTesto			= Replace(sTesto,"'","''")
		sTipo			= "1"
		sCodRuolo		= "SM"
		'Response.Write "DescCentroImpiego = " & sDescCImpiego
		'Response.Write "Mittente= " & sMittente & "<br>"
		'Response.Write "Testo= " & sTesto & "<br>"
		'Response.Write "Tipo= " & sTipo & "<br>"
		'Response.Write "CodRuolo= " & sCodRuolo & "<br>"

'NON PUO' ANDARE BENE: I CAMPI NON ESISTONO!!!!!!!!!!!!!!!!!!!!!!!!!
'DI QUESTI RIMANGONO SOLO: TESTO_COM, DT_INS_COM, DT_TMST
		sSQL = "INSERT INTO COMUNICAZIONE " & _
		        "(MITTENTE," & _
				"TESTO_COM," & _
				"TIPO_COM," & _
				"DT_INS_COM," & _
				"COD_RUOLO," & _
				"DT_TMST) VALUES " & _
				"("	& sMittente & _
				",'" & sTesto & _
				"','" & sTipo & _
				"'," & ConvDateToDb(Now()) & _
				",'" & sCodRuolo & _
				"'," & ConvDateToDb(Now()) & ")"
'!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		'Response.Write sSQL
				
		Errore=EseguiNoC(sSQL, CC)
				
		if Errore <> "0" then
			sErrore1 = Errore
		end if
	end if
			
	if len(sErrore1) = 0 then
		CC.CommitTrans
		Msgetto("Modifica/Validazione Lista ammissibili correttamente effettuata.")
	else
		CC.RollbackTrans
		Msgetto("Modifica/Validazione Lista ammissibili non effettuabile.<br>" & sErrore1)
	end if

	%>
	<br><br>
	<table cellpadding="0" cellspacing="0" width="300" border="0">	
		<tr align="center">
			<td nowrap><a href="javascript:VaiInizio()" onmouseover="javascript:window.status=' '; return true"><img src="<%=session("Progetto")%>/images/indietro.gif" border="0" name="imgPunto2"></a><td>
	    </tr>
	</table>
	<%
end sub

'-----------------------------------------------------------------------------------------------------------------------------------------------------------

sub Fine()
%>
<!--#include virtual = "/include/closeconn.asp"-->
<!--#include virtual = "strutt_coda2.asp"-->
<%
end sub

%>
<!-- ************** ASP Fine *************** -->

<!-- ************** MAIN Inizio ************ -->
<%

Inizio()
Elabora()
Fine()

%>
<!-- ************** MAIN Fine ************* -->
