<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%'Option Explicit 
   Response.ExpiresAbsolute = Now() - 1 
   Response.AddHeader "pragma","no-cache"
   Response.AddHeader "cache-control","private"
   Response.CacheControl = "no-cache"
%>

<!-- #include virtual="/strutt_testa2.asp"-->
<!--#include virtual = "/include/openconn.asp"-->
<!--#include virtual="/util/portallib.asp"-->
<%
'if ValidateService(session("idutente"),"GRD_SELVALIDGRAD", CC) <> "true" then 
'	response.redirect "/util/error_login.asp"
'end if
%>
<!-- ************** Javascript inizio ************ -->

<script LANGUAGE="Javascript">
<!--#include virtual = "/Include/help.inc"-->

function VaiInizio(){
	location.href = "GRD_SelValidGrad.asp";
}	

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

function Controlla(nConta, myForm){
//devo controllare che almeno un check sia selezionato
	var k;
	var NumChk = 0;

	for (k = 1; k <= nConta; k++){
		 if (eval("myForm.chkSelez" + nConta + ".checked") == true){
			NumChk++; 
		}
	}
	//se il contatore � zero non ho selezionato nulla e mando l'errore
	if ((NumChk == 0) && (myForm.chkAutorizz.checked  == false)){
		alert("Nessuno elemento selezionato");
		return false;
	}
	return true;
}

</script>

<!-- ******************  Javascript Fine *********** -->
<!-- ************** ASP inizio *************** -->


<!--#include virtual = "/include/ckProfile.asp"-->
<!--#include virtual = "/include/ControlDateVB.asp"-->
<%
'----------------------------------------------------------------------------------------------------------------------------------------------------  

sub Inizio()
%>
	<table border="0" width="520" cellspacing="0" cellpadding="0" height="81">
	   <tr>
	     <td width="500" background="<%=Session("Progetto")%>/images/titoli/strumenti2b.gif" height="81" valign="bottom" align="right">
	
	       <table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
	         <tr>
	             <td width="100%" valign="top" align="right">
	                 <b class="tbltext1a">Gestione Progetti</span></b>
	             </td>
	         </tr>
	       </table>
	     </td>
	   </tr>
	</table>
	<br>
	<table cellpadding="0" cellspacing="0" width="500" border="0">
		<tr height="18">
			<td class="sfondomenu" height="18" width="67%">
			<span class="tbltext0"><b>Validazione ammissibili</b></span></td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
			<td valign="middle" align="right" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1"></td>
		</tr>
		<tr width="371" class="SFONDOCOMM">
			<td colspan="3">Testo di prova<a href="Javascript:Show_Help('/Pgm/help/GestProgetti/Alfabeta/Graduatoria/GRD_VisValidGrad')" onmouseover="javascript:window.status=' '; return true"><img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
			</td>
		</tr>
		<tr height="2">
			<td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>
	<br>
<%
end sub

'-----------------------------------------------------------------------------------------------------------------------------------------------------------

sub ImpVariabili()

	sProv = Request("cmbProv")
	sBando = Request("cmbBando")

	sApCImp = trim(cstr(Request("cmbCImp")))
	if sApCImp <> "" then
		sCImpiego = CInt(Mid(sApCImp, 1, InStr(1, sApCImp, "|") - 1))
		sDescCImpiego = Trim(Mid(sApCImp, InStr(1, sApCImp, "|") + 1))
	end if

	sCognome = Trim(UCase(Request("txtCognome")))
	sNome = Trim(UCase(Request("txtNome")))

	sCognome= Replace(sCognome,chr(34),"'")
	sNome	= Replace(sNome,chr(34),"''")

	sCognome= Replace(sCognome,"'","''")
	sNome	= Replace(sNome,"'","''")

end sub

'-----------------------------------------------------------------------------------------------------------------------------------------------------------

sub ImpostaPag()

	'select su centro_impiego per sapere lo status di elab. della graduatoria
	'e il numero di posti
	sSQL = "SELECT NUM_POSTI, STATUS_GRAD " & _
			"FROM CENTRO_IMPIEGO WHERE ID_CIMPIEGO =" & sCImpiego
	'response.write sSQL
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	set rsCentro = CC.Execute(sSQL)

	if rsCentro.eof then
		%>
		<table width="500" border="0" cellspacing="1" cellpadding="0">
			<tr>
				<td class="tbltext3" align="center" colspan="2">
					<b>Non ci sono Centri dell'Impiego per il Bando richiesto</b>
				</td>
			</tr>	
		</table>
		<br>
		<table cellpadding="0" cellspacing="0" width="300" border="0">	
			<tr align="center">
				<td nowrap><a href="javascript:VaiInizio()" onmouseover="javascript:window.status=' '; return true"><img src="<%=session("Progetto")%>/images/indietro.gif" border="0" name="imgPunto2"></a><td>
		    </tr>
		</table>
		<%
		
		exit sub
		
	end if
	
	sStatus = cint(rsCentro("STATUS_GRAD"))
	sPosti = cint(rsCentro("NUM_POSTI"))
	%>
	<table width="500" border="0" cellspacing="1" cellpadding="0">
		<tr class="tbltext1">
			<td align="left" width="30%">
				<b>Provincia </b>
			</td>
			<td align="left">
				<font color="red"><b><%=sProv%></b></font>
			</td>
		</tr>
		<tr class="tbltext1">
			<td align="left" width="30%">
				<b>Bando </b>
			</td>
			<td>
				<font color="red"><b><%=sBando%></b></font>
			</td>
		</tr>
		<tr class="tbltext1">
			<td align="left" width="30%">
				<b>Centro dell'Impiego </b>
			</td>
			<td>
				<font color="red"><b><%=sDescCImpiego%></b></font>
			</td>
		</tr>
	</table>
	<%
	if sStatus <> 0 then
	'sStatus = 1 o 2 la Graduatoria � gi� stata definita: mando il messaggio 
	%>
		<br>
		<table width="500" border="0" cellspacing="1" cellpadding="0">			
			<tr>
				<td class="tbltext3" align="center" colspan="2">
					La graduatoria per questo Centro dell'Impiego � gi� stata Definita
				</td>
			</tr>
		</table>
		<br>
		<table cellpadding="0" cellspacing="0" width="300" border="0">	
			<tr align="center">
				<td nowrap><a href="javascript:VaiInizio()" onmouseover="javascript:window.status=' '; return true"><img src="<%=session("Progetto")%>/images/indietro.gif" border="0" name="imgPunto2"></a><td>
		    </tr>
		</table>
		<%
	
		exit sub 
	
	end if
	
	'Se sStatus = 0 la Graduatoria � da definire:
	'mostro la graduatoria provvisoria dando la possibilit� di validarla
	ElabPag()

	rsCentro.close
	set rsCentro = nothing
	%>
	</form>
	<%
end sub

'-----------------------------------------------------------------------------------------------------------------------------------------------------------

sub ElabPag()

dim sContatore

dim rsCImp
dim sIDPers
dim sCodFisc
dim sNome
dim sCognome
dim sDataNascita
dim sEsitoGrad
dim sTimeTemp

dim nChk	

	sSQL = "SELECT D.ESITO_GRAD, D.DT_TMST, P.ID_PERSONA, P.COD_FISC, " & _
			"P.COGNOME, P.NOME, P.DT_NASC " & _
			"FROM DOMANDA_ISCR D,PERSONA P,STATO_OCCUPAZIONALE SO" & _
			" WHERE ID_BANDO = '" & sBando & "' AND D.ID_PERSONA = P.ID_PERSONA AND" & _
			" P.ID_PERSONA = SO.ID_PERSONA AND SO.ID_CIMPIEGO = " & sCImpiego & " AND" & _
			" D.COD_ESITO IS NULL "
	if sCognome <> "" and sNome = "" then
		sSQL = sSQL & "AND P.COGNOME LIKE '" & sCognome & "%'"
	end if 
	if sCognome <> "" and sNome <> "" then
		sSQL = sSQL & "AND P.COGNOME LIKE '" & sCognome & "%' AND P.NOME LIKE '" & sNome & "%'"
	end if 
	sSQL = sSQL & " ORDER BY P.COGNOME, P.NOME, P.DT_NASC"
	'Response.Write sSQL & ";" & "<br>"
	
	set rsCImp = Server.CreateObject("ADODB.Recordset")
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	rsCImp.Open sSQL, CC, 3

	sContatore = CInt(rsCImp.RecordCount)
	
	if sContatore = 0 then
		%>
		<br>
		<table width="500" border="0" cellspacing="1" cellpadding="0">
			<tr>
				<td class="tbltext3" align="center" colspan="2">
			<%
			sSQL = "SELECT COUNT(*) AS PRESENTE FROM PERSONA " & _
					" WHERE COGNOME LIKE '" & sCognome & "%' AND NOME LIKE '" & sNome & "%'"
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
			set rsPer = CC.ExeCute(sSQL)
			if clng(rsPer("PRESENTE")) <> 0 then
				if trim(sCognome) = "" and trim(sNome)= "" then
					Response.Write "Non esistono Persone per i criteri selezionati"
				else
					Response.Write "La persona � gi� stata esclusa"
				end if
			else
				Response.Write "Non esistono Persone per i criteri selezionati"
			end if
			rsPer.Close
			set rsPer = nothing%>
				</td>
			</tr>
		</table>
		<br>
		<table cellpadding="0" cellspacing="0" width="300" border="0">	
			<tr align="center">
				<td nowrap><a href="javascript:VaiInizio()" onmouseover="javascript:window.status=' '; return true"><img src="<%=session("Progetto")%>/images/indietro.gif" border="0" name="imgPunto2"></a><td>
		    </tr>
		</table>
		<%
		rsCImp.Close
		set rsCImp = nothing
		
		exit sub
		
	end if%>

<form method="post" name="frmVisValidGrad" onsubmit="return Controlla('<%=sContatore%>', this)" action="GRD_CnfValidGrad.asp">

	<input type="hidden" name="cmbBando" value="<%=sBando%>">
	<input type="hidden" name="CImpiego" value="<%=sApCImp%>">

	<table width="500" border="0" cellspacing="1" cellpadding="0">
		<tr class="tbltext1">
			<td align="left" width="70%">
				<b>Numero domande valide in base ai criteri selezionati: </b>
			</td>
			<td>
				<font color="red"><b><%=sContatore%></b></font>
			</td>
		</tr>
		<tr class="tbltext1">
			<td align="left" width="70%">
				<b>Numero Posti Disponibili: </b>
			</td>
			<td>
				<font color="red"><b><%=sPosti%></b></font>
			</td>
		</tr>
		<br>
	</table>			
		
	<%if ckProfile(Session("Diritti"),4) then%>
		<table width="500" border="0" cellspacing="1" cellpadding="0">
		    <tr height="20">
				<td align="left" colspan="4"><font color="#ffffff">___</font>
				<font face="Verdana" color="#003843" size="1">
				L&#146;elenco riporta i nominativi dei candidati che hanno presentato la domanda di partecipazione al corso di Alfabetizzazione informatica e di lingua inglese.
				Le richieste di iscrizioni che seguono sono state accettate dal software appositamente predisposto che per mezzo di un sistema di elaborazione automatizzato ha riconosciuto la congruit� dei dati immessi trattenendo solo le schede contenenti i requisiti idonei.
				E&#146; fondamentale che verifichiate l&#146;esattezza dei dati forniti dai candidati con quelli in vostro possesso.
				</font></td>
			</tr>
		</table>
		<br>
		<table width="500" border="0" cellspacing="1" cellpadding="0">
			<tr class="tbltext1">
				<td align="left" width="40%">
					<b>Autorizzazione all' elaborazione della Graduatoria</b>
				</td>
				<td align="left" colspan="2" width="60%">
					<input type="checkbox" id="chkAutorizz" name="chkAutorizz" value="S">
				</td>
		    </tr>
		</table>
	<%end if%>
			
	<br>
	<table border="0" width="500">
		<tr class="sfondocomm">
			<td align="center" colspan="2" width="180">
				<b>Codice Fiscale</b>
			</td>
			<td align="center" colspan="2" width="250">
				<b>Cognome e Nome</b>
			</td>
			<td align="center" colspan="2" width="70">
				<b>Nascita</b>
			</td>
		</tr>
	</table>
	<%
	nChk = 1
	do while not rsCImp.EOF
		sIDPers = CLng(rsCImp("ID_PERSONA"))
		sCodFisc = rsCImp("COD_FISC")
		sNome = rsCImp("NOME")
		sCognome = rsCImp("COGNOME")
		sDataNascita = rsCImp("DT_NASC")
		sEsitoGrad = rsCImp("ESITO_GRAD")
		sTimeTemp = rsCImp("DT_TMST")%>
		<table width="500" border="0" cellspacing="1" cellpadding="0">
			<tr>
				<%if ckProfile(Session("Diritti"),4) then%>
				<td align="center" colspan="2" width="10" class="tbltext1">
					<input type="checkbox" id="chkSelez<%=nChk%>" name="chkSelez<%=nChk%>" value="<%=sCodFisc%>">
				</td>
				<%end if%>
				<td align="center" colspan="2" width="190" class="tbltext1">
					<b><%=sCodFisc%></b>
				</td>
							
				<td align="left" colspan="2" width="270" class="tbltext1">
					<a HREF="../PERSONA/PER_ModDatiPers.asp?txtCFPersona=<%=sCodFisc%>&amp;txtBando=<%=sBando%>"><%=sCognome%>&nbsp;<%=sNome%></a>
				</td>
				<td align="left" colspan="2" width="90" class="tbltext1">
						<b><%=ConvDateToString(sDataNascita)%></b>
				</td>
			</tr>
							
			<input type="hidden" name="TimeTemp<%=nChk%>" value="<%=sTimeTemp%>">
			<input type="hidden" name="IDPers<%=nChk%>" value="<%=sIDPers%>">
			<input type="hidden" name="Esito<%=nChk%>" value="<%=sEsitoGrad%>">
		</table>
		<%
		rsCImp.MoveNext
		nChk = nChk + 1
	loop
	%>
	<br>
	<input type="hidden" name="txtnChk" value="<%=nChk%>">
	<%
	if ckProfile(Session("Diritti"),4) then%>
		<br>
		<table cellpadding="0" cellspacing="0" width="300" border="0">	
			<tr align="center">
				<td nowrap><input type="image" name="Conferma" src="<%=Session("progetto")%>/images/conferma.gif"></td value="Registra">
				<td nowrap><a href="javascript:document.frmVisValidGrad.reset()"><img border="0" src="<%=Session("progetto")%>/images/annulla.gif"></td>
				<td nowrap><a href="javascript:VaiInizio()" onmouseover="javascript:window.status=' '; return true"><img src="<%=session("Progetto")%>/images/indietro.gif" border="0" name="imgPunto2"></a><td>
		    </tr>
		</table>
	<%  
	else%>
		<br>
		<table cellpadding="0" cellspacing="0" width="300" border="0">	
			<tr align="center">
				<td nowrap><a href="javascript:VaiInizio()" onmouseover="javascript:window.status=' '; return true"><img src="<%=session("Progetto")%>/images/indietro.gif" border="0" name="imgPunto2"></a><td>
		    </tr>
		</table>
	<%
	end if

	rsCImp.close
	set rsCImp= nothing

end sub

'-----------------------------------------------------------------------------------------------------------------------------------------------------------

sub Fine()
%>
<!--#include virtual = "/include/closeconn.asp"-->
<!--#include virtual = "strutt_coda2.asp"-->
<%
end sub

%>
<!-- ************** ASP Fine *************** -->

<!-- ************** MAIN Inizio ************ -->
<%

dim sProv
dim sBando
dim sCImpiego
dim sDescCImpiego
dim sCognome
dim sNome
dim sStatus
dim sPosti
dim rsCentro
dim sContatore

Inizio()
ImpVariabili()
ImpostaPag()
Fine()

%>
<!-- ************** MAIN Fine ************* -->
