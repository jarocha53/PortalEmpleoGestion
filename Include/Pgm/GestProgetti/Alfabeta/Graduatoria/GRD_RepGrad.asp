<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%'Option Explicit 
   Response.ExpiresAbsolute = Now() - 1 
   Response.AddHeader "pragma","no-cache"
   Response.AddHeader "cache-control","private"
   Response.CacheControl = "no-cache"
%>

<!-- #include virtual="/strutt_testa2.asp"-->
<!--#include virtual = "/include/openconn.asp"-->
<!--#include virtual="/util/portallib.asp"-->
<!--#include virtual = "include/SelAreaTerrBandi.asp"-->

<!-- ************** Javascript inizio ************ -->
<%
If Not ValidateService(session("idutente"),"GRD_STMGRADDEF", CC) then 
	response.redirect "/util/error_login.asp"
End If
%>

<script LANGUAGE="JavaScript">

<!--#include Virtual = "/Include/help.inc"-->


function VaiInizio(){
	location.href = "GRD_StmGradDef.asp";
}	

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

function Stampa(){
	frmstampa.submit();                                                    
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

function Scarica(report){	
	
	fin=window.open(report,"","toolbar=0;location=0,directories=0,status=0,menubar=1,scrollbars=1,resizable=1,copyhistory=0,width=450,height=340,screenX=200,screenY200");
}

</script>

<!-- ******************  Javascript Fine *********** -->
<!-- ************** ASP inizio *************** -->

<!--#include virtual ="/include/controldatevb.asp"-->
<%
'----------------------------------------------------------------------------------------------------------------------------------------------------  

sub Inizio()
%><form name="frmstampa" method="post" action="GRD_Anteprima.asp">
       <input type="hidden" name="cmbStatus" value="<%=sStatus%>">
       <input type="hidden" name="cmbProvBan" value="<%=sProv%>">
       <input type="hidden" name="cmbBando" value="<%=nIdBando%>">
       <input type="hidden" name="cmbSede" value="<%=sSede%>">
       <input type="hidden" name="Intestazione" value="<%=sIntestazione%>">
       <input type="hidden" name="cmbOrder" value="<%=sOrdinamento%>">
</form>
<body topmargin="0" onafterprint="javascript:buttom.style.visibility='visible';" onbeforeprint="javascript:buttom.style.visibility='hidden'">

	<table border="0" width="520" cellspacing="0" cellpadding="0" height="81">
	   <tr>
	     <td width="500" background="<%=Session("Progetto")%>/images/titoli/strumenti2b.gif" height="81" valign="bottom" align="right">
	
	       <table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
	         <tr>
	             <td width="100%" valign="top" align="right">
	                 <b class="tbltext1a">Gestione Progetti</span></b>
	             </td>
	         </tr>
	       </table>
	     </td>
	   </tr>
	</table>
	<br>
	<table cellpadding="0" cellspacing="0" width="500" border="0">
		<tr height="18">
			<td class="sfondomenu" height="18" width="67%">
			<span class="tbltext0"><b>STAMPA DELLA GRADUATORIA</b></span></td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
			<td valign="middle" align="right" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1"></td>
		</tr>
		<tr width="371" class="SFONDOCOMM">
			<td colspan="3">Per la visualizzazione del report o per la stampa della lista dei candidati cliccare sui link sottostanti.<a href="Javascript:Show_Help('/Pgm/help/GestProgetti/Alfabeta/Graduatoria/GRD_RepGrad')" onmouseover="javascript:window.status=' '; return true"><img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
			</td>
		</tr>
		<tr height="2">
			<td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>
	<br>
<%
end sub

sub ImpostaPag()
desbando=DecBando(nIdBando)
dim sCodFisc	
dim sNominativo
dim sSesso
dim sDtNasc

	if sStatus="0" then
		sTitolo1 = "Graduatoria al " & ConvDateToString(now())
		if ssede <> "" then
		sTitolo = "Lista non Ammessi al progetto " & DescProj & " - " & desbando & " - " & " per la sede di " & sDescSede
		else
		sTitolo = "Lista non Ammessi al progetto " & DescProj & " - " & desbando & " - " & " per il territorio nazionale"
		 end if
	'	sInfo= "La Lista, disposta in ordine alfabetico, riporta i nominativi di tutti i candidati non ammessi al progetto Alfabetizzazione. L�inclusione nelle liste di partecipazione � avvenuta in seguito al riscontro positivo dei requisiti disposti dal bando e attinenti ai criteri di selezione."
	'	sInfo= "La graduatoria, disposta in ordine alfabetico, riporta i nominativi dei non ammessi e degli esclusi. L�esclusione dalle liste di partecipazione � avvenuta in seguito al riscontro dei requisiti disposti dal bando e attinenti ai criteri di selezione. La graduatoria definitiva resa pubblica, � stata curata da Italia Lavoro S.p.A in collaborazione con i Centri per l�Impiego provinciali."
	'	sTipoGrad="Provvisoria"
		sWhere = " AND (ESITO_GRAD = 'N' OR COD_ESITO IS NOT NULL) "
	'	sOrder = " ORDER BY DECODE(ESITO_GRAD,'N',1,0) DESC ,decode(cod_esito,null,0,1),COGNOME,NOME,DT_NASC"
		sApNomeFile ="_NonAmmessi_"
	else
		sTitolo1 = "Graduatoria al " & ConvDateToString(now())
		if ssede <> "" then
		sTitolo = sTitolo & " Lista Ammessi al progetto " & DescProj & " - " & desbando & "-" & " per la sede di " & sDescSede
		else
		sTitolo = "Lista  Ammessi al progetto " & DescProj & " - " & desbando & " - " & " per il territorio nazionale"
		end if
	'	sTitolo = "Graduatoria definitiva per il centro di Impiego di " & sDescCImpiego 
	'	sTipoGrad="Definitiva"
	'	sOrder = " ORDER BY DECODE(C.ESITO_GRAD,'S',2,'X',1,0) desc, c.PUNTEGGIO DESC , c.DT_DOM"
		sWhere = " AND (COD_ESITO IS NULL AND ESITO_GRAD IN ('S','X')) "
	'	sOrder = " ORDER BY COGNOME,NOME,DT_NASC"	
	'	sInfo= "La graduatoria, disposta in ordine alfabetico, riporta i nominativi degli ammessi. L�inclusione nelle liste di partecipazione � avvenuta in seguito al riscontro positivo dei requisiti disposti dal bando e attinenti ai criteri di selezione. La graduatoria definitiva resa pubblica, � stata curata da Italia Lavoro S.p.A in collaborazione con i Centri per l�Impiego provinciali."
		sApNomeFile ="_Ammessi_"
	end if
     
	if sOrdinamento="0" then
		sOrder= "ORDER BY COGNOME, NOME, DT_NASC"
		if sStatus="0" then
			sInfo= "La graduatoria, disposta in ordine alfabetico, riporta i nominativi dei non ammessi e degli esclusi. L�esclusione dalle liste di partecipazione � avvenuta in seguito al riscontro dei requisiti disposti dal bando e attinenti ai criteri di selezione."
		else
			sInfo= "La graduatoria, disposta in ordine alfabetico, riporta i nominativi degli ammessi. L�inclusione nelle liste di partecipazione � avvenuta in seguito al riscontro positivo dei requisiti disposti dal bando e attinenti ai criteri di selezione."
		end if
	else 
		sOrder="ORDER BY PUNTEGGIO DESC, DT_DOM "
		if sStatus="0" then
			sInfo= "La graduatoria, disposta in ordine di punteggio, riporta i nominativi dei non ammessi e degli esclusi. L�esclusione dalle liste di partecipazione � avvenuta in seguito al riscontro dei requisiti disposti dal bando e attinenti ai criteri di selezione."
		else
			sInfo= "La graduatoria, disposta in ordine di punteggio, riporta i nominativi degli ammessi. L�inclusione nelle liste di partecipazione � avvenuta in seguito al riscontro positivo dei requisiti disposti dal bando e attinenti ai criteri di selezione."
		end if
	end if  

	'sempre vuota!!!!
	if sIntestazione <> "" then
	%>
		<!--table border="0" cellpadding="0" cellspacing="0" width="630">			<tr>		        <td width="80%" align="middle">					<b><font face="Verdana" size="2" Color="#008caa"><%=sIntestazione%></font></b>				</td>		    </tr>		</table-->
	<%
	end if

	set rsRR = server.CreateObject("ADODB.Recordset")
	
	sSQL="SELECT P.COD_FISC, P.COGNOME||' '||P.NOME AS Nominativo, " & _
	     " P.SESSO, TO_CHAR(P.DT_NASC,'DD/MM/YYYY') DT_NASC" & _
	     " FROM PERSONA P, DOMANDA_ISCR D WHERE " & _
		 " D.ID_BANDO=" & nIdBando & " AND D.ID_PERSONA = P.ID_PERSONA " & _
	       sqlsede & sWhere & sOrder
'Response.Write Ssql	
	'Response.End 	

'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	rsRR.open sSQL,CC, 3
	if rsRR.EOF then
		%>
		<table width="500" border="0" cellspacing="1" cellpadding="0">
			<tr>
				<td class="tbltext3" align="center">
					<b>Non ci sono Candidati per i criteri selezionati</b>
				</td>
			</tr>	
		</table>
		<br>
		<table cellpadding="0" cellspacing="0" width="300" border="0">	
			<tr align="center">
				<td nowrap><a href="javascript:history.go(-1)" onmouseover="javascript:window.status=' '; return true"><img src="<%=session("Progetto")%>/images/indietro.gif" border="0" name="imgPunto2"></a><td>
			</tr>
		</table>
		<%	
		exit sub
	end if
	n = 1
	%>
	<br>
	<table width="500" border="0" cellspacing="1" cellpadding="0">
	    <tr>
	        <td width="80%" class="tbltext3">
				<%=sTitolo%>
			</td>
	    </tr>
	</table>
	<br>
	<br>
	<table width="500" border="0" cellspacing="1" cellpadding="0">
		<tr> 
			<td>
			 <span class="tbltext1">
				<p align="justify">
				La presente graduatoria si aggiorna costantemente anche in base
				alla modifica dei dati sensibili  causati dalla variazione dei dati individuali.
				Pertanto potrebbe differire dalla graduatoria ufficiale pubblicata per il bando.
			  </p>
			 </span>
			</td>
		</tr>
		<tr><td>&nbsp;</td></tr>
		<tr>
			<td class="tbltext1"><p align="justify"><%=sInfo%></p>
			</td>
		</tr>
	</table>
	<br>
	<table width="500" border="0" cellspacing="1" cellpadding="0">
		<tr class="sfondocomm">
			<td align="center"><b>N.</b></td>
			<td align="center"><b>Codice fiscale</b></td>
			<td align="center"><b>Nominativo</b></td>
			<td align="center"><b>Sesso</b></td>
			<td align="center"><b>Data di nascita</b></td>
		</tr>
<%		lPath = Server.MapPath("/") & session("progetto") & "/DocPers/Temp/" &_
	    Session("IdUtente") & sApNomeFile & nIdBando & "_" & sIdSede & ".xls"
		
		sNomeFile = session("progetto") & "/DocPers/Temp/" &_
	    Session("IdUtente") & sApNomeFile & nIdBando & "_" & sIdSede & ".xls"

	Set fso = CreateObject("Scripting.FileSystemObject")
	Set a = fso.CreateTextFile(lPath, True)
'	a.writeline "Graduatoria " & sTipoGrad & " per il centro di impiego di " & sDescCImpiego
	a.writeline sTitolo1
	a.WriteLine ""
	a.writeline sTitolo 
	a.WriteLine ""
	a.WriteLine "Codice Fiscale" & chr(9) & "Nominativo" &  chr(9)& "Sesso" & chr(9) & "Data Nascita"
    rptIscritti=rsRR.GetString (2,, chr(9),chr(13), " ")
	a.WriteLine rptIscritti
	a.Close
	
	rsRR.MoveFirst

	Do while  not rsRR.EOF 
		
		sCodFisc = rsRR("COD_FISC")
		sNominativo = rsRR("NOMINATIVO")	
		sSesso = rsRR("SESSO")
		sDtNasc = rsRR("DT_NASC")
'		ESG = rsRR.Fields(4)
'		ESC = rsRR.Fields(5)		
		%>
		<tr class="tblsfondo">
			<td align="center" class="tbltext1"><%=n%></td>
			<td align="center" class="tbltext1"><%=sCodFisc%></td>
			<td align="center" class="tbltext1"><%=sNominativo%></td>
			<td align="center" class="tbltext1"><%=sSesso%></td>
			<td align="center" class="tbltext1"><%=ConvDateToString(sDtNasc)%></td>
		</tr>
		<%
		rsRR.MoveNext
		n = n + 1	
	loop
	rsRR.Close
	set rsRR = nothing
	%>
	</table>
	<br>
	<label id="buttom">	
	<p align="center">
		<a href="javascript:Stampa()" class="textred">
			<b>Anteprima di Stampa</b>
		</a>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<a href="javascript:Scarica('<%=sNomeFile%>')" class="textred">
			<b>Visualizza Report</b>
		</a>
	</p>

	<br>
	<table cellpadding="0" cellspacing="0" width="300" border="0">	
		<tr align="center">
			<td nowrap><a href="javascript:VaiInizio()" onmouseover="javascript:window.status=' '; return true"><img src="<%=session("Progetto")%>/images/indietro.gif" border="0" name="imgPunto2"></a><td>
	    </tr>
	</table>
	</label>
<%
end sub
'-----------------------------------------------------------------------------------------------------------------------------------------------------------

sub Fine()
%>
<!--#include virtual = "/include/closeconn.asp"-->
<!--#include virtual = "strutt_coda2.asp"-->
<%
end sub

%>
<!-- ************** ASP Fine *************** -->

<!-- ************** MAIN Inizio ************ -->
<%

dim sStatus
dim sIntestazione
dim sTipoGrad
dim sOrdinamento

dim sSede
dim sIdSede
dim sDescSede

dim sBando
dim nIdBando
dim sDescBando
dim DescProj


dim sNomeFile
dim sApNomeFile
'--------------------------------Mi prendo la descrizione del progetto ----------------
Ssql="select desc_proj from progetto where area_web= '" & Mid(session("progetto"),2) & "'"
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	set rsProg = cc.execute(Ssql)
	'Response.Write Ssql
	DescProj=rsProg("desc_proj")
	

'------------------------------------------------------------------------------------------
sStatus	= Request("cmbStatus")
sProv = Request("cmbProvBan")
nIdBando = Request("cmbBando")

'DescProj= Request("DescProgetto")
'Response.Write "DescProj;" & DescProj & "<br>"

sSede = trim(cstr(Request("cmbSede")))
if sSede <> "" then
	sIdSede = CLNG(Mid(sSede, 1, InStr(1, sSede, "|") - 1))
	
	sDescSede = Trim(Mid(sSede, InStr(1, sSede, "|") + 1))
	sqlsede = "AND D.ID_sede=" & sIdSede
	else
	sqlsede = ""
end if

if nIdBando <> "" then		
		
		sDescBando= DecBando(nIdBando)

'if sBando <> "" then		
'		nIdBando=Mid(sBando,1 ,InStr(1, sBando,"|") - 1)
'		sDescBando=Mid(sBando,InStr(1, sBando,"|") + 1)
end if	
'Response.Write "sDescSede;" & sDescSede & "<br>"	
sIntestazione = Request("Intestazione")
sOrdinamento = Request("cmbOrder")

Inizio()
ImpostaPag()
Fine()

%>
