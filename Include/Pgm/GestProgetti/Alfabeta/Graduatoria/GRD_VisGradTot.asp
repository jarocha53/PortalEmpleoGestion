<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%'Option Explicit 
   Response.ExpiresAbsolute = Now() - 1 
   Response.AddHeader "pragma","no-cache"
   Response.AddHeader "cache-control","private"
   Response.CacheControl = "no-cache"
%>
<!-- #include virtual="/strutt_testa2.asp"-->
<!--#include virtual ="/include/ckProfile.asp"-->
<!--#include virtual="/include/ControlDateVB.asp"-->
<!--#include virtual="/include/openconn.asp"-->
<!--#include virtual="/util/portallib.asp"-->
<% 
'if ValidateService(session("idutente"),"grd_selgradtot") <> "true" then 
'	response.redirect "/util/error_login.asp"
'end if
%>
<!-- ************** Javascript inizio ************ -->
<script language="Javascript">
<!--#include virtual = "/Include/help.inc"-->
</script>
<!-- ******************  Javascript Fine *********** -->
<!-- ************** ASP inizio ************ -->
<%sub Messaggio(sMessaggio)%>
<br>
<table width="500">
	<tr><td align="center" class="tbltext3"><b><%=sMessaggio%></b></td></tr>	
</table>
<br>
<%end sub%>
<!---------------------------------->
<%sub Fine()%>
	<!--#include virtual ="/include/closeconn.asp"-->
	<!--#include virtual = "strutt_coda2.asp"-->
<%end sub %>
<!------------>

<!-- ******************  ASP Fine *********** -->
<!-- ******************  MAIN INIZIO *********** -->

<form method="post" name="frmVisValidGrad" target="principale">
<%
   dim sCentroImp, sBando, sProv, sSQL, rsCentro


' CENTRO DI IMPIEGO e il BANDO della pagina precedente
sCentroImp = cdbl(Request.Form ("cmbCImp"))
sBando = Request.Form ("Bando")
sProv = Mid(sBando,1,2)
%>
<input type="hidden" name="CImpiego" value="<%=sCentroImp%>">
<%
'Mi ricavo la descrizione del centro di impiego per scrivere il titolo
sSQL = "SELECT ID_CIMPIEGO,DESC_CIMPIEGO FROM CENTRO_IMPIEGO WHERE ID_CIMPIEGO = " & sCentroImp

'PL-SQL * T-SQL  
SSQL%> = TransformPLSQLToTSQL (SSQL%>) 
set rsCentro = CC.Execute(sSQL)%>

<table width="500" border="0" cellspacing="2" cellpadding="1">
	<tr> 
		<td align=center class="tbltext1"><b>Provincia di <%=sProv%></b></td>
	</tr>
		<td align=center class="tbltext1"><b>Bando <%=sBando%></b></td>
	</tr>
</table>
<%if	rsCentro.eof then
		Messaggio(	"Non ci sono iscrizioni alla data odierna")
  else
		sDescCentro = rsCentro("DESC_CIMPIEGO")%>
		<br>
		<table width="500" border="0" cellspacing="1" cellpadding="0">
			<tr>
				<td valign="top"  class="tbltext1" align=center>
					<b>Centro dell'Impiego selezionato: <%=sDescCentro%></b>
				</td>
			</tr>
			<tr>
				<td class="tbltext1" align=center>
					<b>Data: <%=ConvDateToString(Now())%></b>
				</td>
			</tr>
		</table>
		<br>
<%
rsCentro.close

dim sContatore

sContatore = 0

'mi trovo il numero delle domande d'iscrizione valide
sSQL = "SELECT COUNT(*) AS Count FROM DOMANDA_ISCR A,PERSONA B,STATO_OCCUPAZIONALE C" & _
		" WHERE ID_BANDO = '" & sBando & "' AND A.ID_PERSONA = B.ID_PERSONA AND" & _
		" B.ID_PERSONA = C.ID_PERSONA AND C.ID_CIMPIEGO = " & sCentroImp & " AND" & _
		" A.COD_ESITO IS NULL"

'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
set rsCount = CC.Execute(sSQL)
	
sContatore = cdbl(rsCount("Count"))

rsCount.close

dim sContatore2

sContatore2 = 0

'mi trovo il numero degli iscritti
sSQL = "SELECT COUNT(*) AS Count FROM DOMANDA_ISCR A,PERSONA B,STATO_OCCUPAZIONALE C" & _
		" WHERE ID_BANDO = '" & sBando & "' AND A.ID_PERSONA = B.ID_PERSONA AND" & _
		" B.ID_PERSONA = C.ID_PERSONA AND C.ID_CIMPIEGO = " & sCentroImp

'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
set rsCount2 = CC.Execute(sSQL)
	
sContatore2 = cdbl(rsCount2("Count"))

rsCount2.close

sNon = sContatore2 - sContatore%>
<br>
	<table border="0" height="30" cellspacing="1" cellpadding="2" width="500">
		<tr class="sfondocomm"> 
			<th class="tbllabel" align="center" valign="center" width="190">Numero Iscritti</th>
			<th class="tbllabel" align="center" valign="center" width="190">Iscrizioni Valide</th>
			<th class="tbllabel" align="center" valign="center" width="190">Iscrizioni Non Valide</th>
		</tr>
		<tr class="tblsfondo">
			<td align="center" width="190" class="tbltext1"><%=sContatore2%></td>
			<td align="center" width="190" class="tbltext1"><%=sContatore%>	</td>
			<td align="center"  width="190" class="tbltext1"><%=sNon%></td>
		</tr>
	</table>		
<%end if%>
</form>
<%Fine()%>
<!-- ******************  MAIN FINE *********** -->
