<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->

<!-- #include virtual="/strutt_testa2.asp"-->
<!--#include virtual = "/include/openconn.asp"-->
<!--#include virtual="/util/portallib.asp"-->
<!--#include virtual = "/Include/DecComun.asp"-->

<!-- ************** Javascript inizio ************ -->


<script LANGUAGE="Javascript">

//include del file per fare i controlli sulla numericitÓ dei campi
<!--#include virtual = "/Include/ControlNum.inc"-->
<!--#include virtual = "/Include/ControlString.inc"-->
<!--#include virtual = "/Include/Help.inc"-->

function SelProv(NomeForm,IdUorg,idBando,txtProvHid)
{
	windowArea = window.open ('/include/SelProvince.asp?NomeForm=' + NomeForm + '&idBando=' +  idBando + '&IdUorg=' +  IdUorg,'Info','width=422,height=450,Resize=No,Scrollbars=yes')	
    
}

//-------------------------------------------------------------------

function SelCentroForm(sProv,sComune,NomeForm,IdUorg,idBando,NomeCampo,NomeCampoCF)
{
	windowArea = window.open ('/include/SelCentroForm.asp?comune='+ sComune + '&Prov=' + sProv + '&NomeForm=' + NomeForm + '&idBando=' +  idBando + '&IdUorg=' +  IdUorg + '&NomeCampo=' + NomeCampo + '&NomeCampoCF=' + NomeCampoCF ,'Info','width=422,height=450,Resize=No,Scrollbars=yes')	
}	

//-------------------------------------------------------------------

function VaiInizio(nIdCF, sRi, sPrv){	
	location.href = "CFO_VisAule.asp?idCForm=" +  nIdCF + "&cmbProv=" + sPrv;
	
}	

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

function Elimina(idAula,idCform) {
	if (confirm("Sei sicuro di voler cancellare l'aula?")) 
		location.href = "CFO_CfnAule.asp?MODO=CANC&idAula=" + idAula + "&iDForm=" + idCform;
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

function ControllaDati(myForm){
	if (frmModAula.txtDenominazione.value == "") {
		alert("Descrizione obbligatoria")
		frmModAula.txtDenominazione.focus() 
		return false
	}
	frmModAula.txtDenominazione.value=TRIM(frmModAula.txtDenominazione.value);

//	if (frmModAula.txtNumPosti.value == "" || myForm.txtNumPosti.value == "0"){
	if (frmModAula.txtNumPosti.value == "" ){
		alert("Numero Posti obbligatorio")
		frmModAula.txtNumPosti.focus() 
		return false
	}

	if (!IsNum(frmModAula.txtNumPosti.value)){
		alert("Il valore deve essere numerico")
		frmModAula.txtNumPosti.focus() 
		return false
	}
	if (myForm.cmbTipoClasse.value == "") {
		alert("Indicare il tipo della classe")
		myForm.cmbTipoClasse.focus() 
		return false
	}
	return true
}
</script>

<!-- ******************  Javascript Fine *********** -->

<!-- ************** ASP inizio *************** -->

<!--#include virtual = "/include/ckProfile.asp"-->
<!--#include virtual = "/util/dbutil.asp"-->
<%
sub Inizio()

%>
	<table border="0" width="500" cellspacing="0" cellpadding="0" height="81">
	   <tr>
	     <td width="500" background="<%=Session("Progetto")%>/images/titoli/strumenti2b.gif" height="81" valign="bottom" align="right">
	       <table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
	         <tr>
	           <td width="100%" valign="top" align="right">
	           <b class="tbltext1a">Gestione Progetti</span></b></td>
	         </tr>
	       </table>
	     </td>
	   </tr>
	</table>
	<br>
	<table cellpadding="0" cellspacing="0" width="500" border="0">
		<tr height="18">
			<td class="sfondomenu" height="18" width="67%">
			<span class="tbltext0"><b>&nbsp;DISPONIBILITA' AULE</b></span></td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
			<td valign="middle" align="right" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1"></td>
		</tr>
		<tr width="371" class="SFONDOCOMM">
			<td colspan="3">
			Modifica delle caratteristiche dell'Aula. <br>
			Premere <b>Invia</b> per salvare le modifiche. 
			<a title="Help" href="Javascript:Show_Help('/Pgm/help/GestProgetti/Alfabeta/CFormazione/CFO_ModAule')" onmouseover="javascript:window.status=' '; return true"><img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
			</td>
		</tr>
		<tr height="2">
			<td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>
	<br>
	
<%
end sub

'----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

sub ImpostaPag()

sIdAula = Request.form ("idAula")

sSQL=	"SELECT ID_CFORM,ID_AULA,TIPOCLASSE, DESC_AULA, " &_
		"NUM_POSTI,PRG_AULA,DT_TMST " &_
		"FROM AULA WHERE ID_AULA=" & sIdAula 

'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
set rsAula = CC.Execute(sSQL)
if rsAula("ID_CFORM") <> "" then
	nIdCform = clng(rsAula("ID_CFORM"))
end if
sTipoClasse =  rsAula("TIPOCLASSE")

%>
<form name="frmModAula" onsubmit="return ControllaDati(this)" method="post" action="CFO_CfnAule.asp">

	<input type="hidden" name="txtAction" value="MOD">
	<input type="hidden" name="txtBando" value="<%=nIdBando%>">
	<input type="hidden" name="txtIDAula" value="<%=sIdAula%>"> 
	<input type="hidden" name="txtDT_TMST" value="<%=rsAula("DT_TMST")%>"> 
	<input type="hidden" name="txtIdCFormOLD" value="<%=nIdCform%>">
	<input type="hidden" name="txtIdCForm" value="<%=nIdCform%>">
	<input type="hidden" name="txtPrgAula" value="<%=rsAula("PRG_AULA")%>">
	<input type="hidden" name="txtDescBando" value="<%=sDescBando%>">

	<table border="0" cellpadding="1" cellspacing="2" width="500">
		<tr>
			<td align="left" class="tbltext1">
				<b>Descrizione*</b>
			</td>
			<td align="left">
				<input style="TEXT-TRANSFORM: uppercase;" size="30" maxlength="30" class="textblacka" name="txtDenominazione" value="<%=rsAula("DESC_AULA")%>">
			</td>
		</tr>
		<tr class="tbltext1">
  			<td align="left">
	  			<b>Numero posti*</b>
			</td>
  			<td align="left">
				<input style="TEXT-TRANSFORM: uppercase;" size="3" maxlength="2" class="textblack" name="txtNumPosti" value="<%=rsAula("NUM_POSTI")%>">
			</td>
		</tr>		
		<tr class="tbltext1">
  			<td align="left" width="150">
  				<b>DisponibilitÓ classe*</b>
			</td>
  			<td align="left">
				<select name="cmbTipoClasse" class="textblack">
					<option></option>
					<option Value="M" <%if sTipoClasse = "M" then Response.Write " selected " %>>Mattino</option>
					<option Value="P" <%if sTipoClasse = "P" then Response.Write " selected " %>>Pomeriggio</option>
				</select>
			</td>
		</tr>		
		<tr class="tbltext1">
  			<td align="left">
	  			<b>Presso il C.F.</b>
			</td>
  			<td nowrap>				
	<%
				if not isnull(rsAula("id_cform")) then
				
					sql="SELECT PRV,COMUNE,DESC_CFORM FROM CENTRO_FORMAZIONE WHERE ID_CFORM= " &_
						CLng(rsAula("id_cform"))
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
					SET rsCFORM = cc.execute(sql) 
					if not rsCFORM.eof then
						sProv=rsCFORM("PRV")
						sComune = rsCFORM("COMUNE")
						Sdesccform = rsCFORM("DESC_CFORM")
					end if
					rsCFORM.close
					set rsCFORM = nothing
				end if
				NomeForm="frmModAula"
				IdUorg=session("iduorg")
				 			
				
%>			<span class="tbltext">
<%			 if  descrcomune(sComune) <>"0" then%>	
				<textarea readonly class="textblacka" style="TEXT-TRANSFORM: uppercase;" rows="4" cols="35" id="txtcentroformazione" name="txtcentroformazione"><%=SdESCcform & "(" & descrcomune(sComune) & " - " & sProv & " )"%></textarea>
			<%else%>	
				<textarea readonly class="textblacka" style="TEXT-TRANSFORM: uppercase;" rows="4" cols="35" id="txtcentroformazione" name="txtcentroformazione"></textarea>
			<%end if%>	
				<!--a href="Javascript:SelCentroForm('<%'=sProv%>','<%'=sComune%>','<%'=NomeForm%>','<%'=IdUorg%>','<%'=nIdBando%>','txtcentroformazione','txtIdCForm')" 					ID="imgPunto1" name="imgPunto1" 					onmouseover="javascript:window.status='';return true">								<img border="0" src="<%'=Session("Progetto")%>/images/bullet1.gif"></a-->
				<input type="hidden" name style="TEXT-TRANSFORM: uppercase;" class="textblacka" size="35" readonly value>
				<input type="hidden" name style="TEXT-TRANSFORM: uppercase;" class="textblacka" size="35" readonly value>				
				</span>			
			</td>
		</tr>
<%'-----------------------------------------------------------------------	%>	
		
	</table>
	<br>
	<!--impostazione dei comandi-->
	<table cellpadding="0" cellspacing="0" width="300" border="0">	
		<tr align="center">
			<td nowrap><input title="Conferma" type="image" name="Conferma" src="<%=Session("progetto")%>/images/conferma.gif"></td>
			<td nowrap><a title="Annulla" href="javascript:document.frmModAula.reset()"><img border="0" src="<%=Session("progetto")%>/images/annulla.gif"></td>
			<td nowrap><a title="Indietro" href="javascript:Indietro.submit()"><img src="<%=session("Progetto")%>/images/indietro.gif" border="0" name="imgPunto2" onmouseover="javascript:window.status=' '; return true"></a></td>
		</tr>
	</table>
</form>
<form name="Indietro" method="post" action="CFO_VisAule.asp">
	<input type="hidden" name="CmbDescBando" value="<%=sDescBando%>|<%=nIdBando%>">
</form>

<%
	rsAula.Close
	set rsAula =nothing

end sub

'-----------------------------------------------------------------------------------------------------------------------------------------------------------

sub Fine()
%>
<!--#include virtual = "/include/closeconn.asp"-->
<!--#include virtual = "strutt_coda2.asp"-->
<%
end sub%>

<!-- ************** ASP Fine *************** -->

<!-- ************** MAIN Inizio ************ -->

<%
if ValidateService(session("idutente"),"CFO_VisAule",CC) <> "true" then 
	response.redirect "/util/error_login.asp"
end if


dim sIdAula
dim sIdCenForm
dim sRientro
dim sCForm
dim sProv
dim sdesccform
dim sDescBando

sDescBando = Request.Form("descBando")
sIdCenForm = Request.Form ("iDCForm")
sRientro = Request.Form ("Rientro")
sProv = Request("cmbProv")
nIdBando=Request.Form ("idBan")
'Response.Write nIdBando & "=nIdBando"
sdesccform=Request("desc_cform")
sPrv = Request.Form("txtprovincia")
'Response.Write sPrv & "=sPrv"
if len(sProv)>2 then
	nProv= Mid(sProv,3,2)
else
	nProv = sProv
	'Response.Write nProv & "=nProv"
end if			
Inizio()
ImpostaPag()
Fine()

%>

<!-- ************** MAIN Fine ************ -->
