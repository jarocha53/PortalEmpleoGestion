<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #include virtual="/strutt_testa2.asp"-->
<!--#include virtual ="/include/openconn.asp"-->
<!--#include virtual="/include/SelAreaTerrBandi.asp"-->
<!--#include virtual = "util/dbutil.asp"-->
<!--#include virtual = "include/DecComun.asp"-->


<!-- ************** Javascript inizio ************ -->
<script LANGUAGE="Javascript">
<!--#include virtual = "/Include/help.inc"-->

function Carica_Pagina(npagina) {
	document.frmPagina.pagina.value = npagina;
	document.frmPagina.submit();
}

function Invia() {
	if (document.frmVisAreaGeoCombo.CmbDescBando.value=="")
	{
		alert('Devi selezionare un Bando.')
	}
	else
	{		
		document.frmVisAreaGeoCombo.action = "CFO_VisAule.asp";
		//document.frmVisAreaGeoCombo.value = idbando;		
		document.frmVisAreaGeoCombo.submit();				
	}
}
</script>
<!-- ******************  Javascript Fine *********** -->
<!-- ************** ASP inizio *************** -->


<% sub Intestazione() %>
	<table border="0" width="520" cellspacing="0" cellpadding="0" height="81">
	   <tr>
	     <td width="500" background="<%=Session("Progetto")%>/images/titoli/strumenti2b.gif" height="81" valign="bottom" align="right">
	       <table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
	         <tr>
	           <td width="100%" valign="top" align="right"><b class="tbltext1a">Gestione Progetti</span></b></td>
	         </tr>
	       </table>
	     </td>
	   </tr>
	</table>
	<br>
	<table cellpadding="0" cellspacing="0" width="500" border="0">
		<tr height="18">
			<td class="sfondomenu" height="18" width="67%">
			<span class="tbltext0"><b>&nbsp;DISPONIBILITA' AULE</b></span></td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
			<td valign="middle" align="right" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1"></td>
		</tr>
		<tr width="371" class="SFONDOCOMM">
			<td colspan="3">
			<%if sDescBando <> "" then%>
				Per inserire una nuova aula selezionare <b>Inserisci Nuova Aula</b>.
				Se si desidera modificare un aula selezionare l'identificativo
				dell'aula stessa. 
			<%else%>
				Selezionare un bando per ottenere la visualizzazione delle aule.
			<%end if%>
			
			<a title="Help" href="Javascript:Show_Help('/Pgm/help/GestProgetti/Alfabeta/CFormazione/CFO_VisAule')" onmouseover="javascript:window.status=' '; return true"><img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a></td>
		</tr>
		<tr height="2">
			<td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>
	<br>
<%end sub%>
<!---------------------------------------------------------------->

<%'SetProvAreaTerr(idArea)%>
<%sub CaricaCombo() 
	dim RecDescBando%>

	<form name="frmVisAreaGeoCombo" action method="post">
		<table cellpadding="0" cellspacing="0" width="500" border="0">	
			<tr>
	<%if nIdBando <> "" then%>
		<tr>
			<td class="tbltext1" align="left" width="150">
				<b>Bando:</b>
			</td>
			<td class="tbltext" align="left" width="350">
				<b><%=sDescBando%></b>
			</td>
		</tr>
	<%else %>
			<td class="tbltext1" align="left" width="150"><b>Bando:</b></td>
			<td class="tbltext3" align="left">
	<%	 sSql=	"SELECT ID_BANDO,DESC_BANDO FROM BANDO " & _
				"WHERE ID_BANDO IN (SELECT ID_BANDO FROM " &_
				"AREA_BANDO  WHERE ID_UORG=" & session("iduorg") & ")"
				
		set RecDescBando=Server.CreateObject("ADODB.recordset")
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
		RecDescBando.Open sSql,CC,3
	
		if not RecDescBando.EOF	then
	%>		<select ID="CmbDescBando" class="textblack" name="CmbDescBando" onchange="javascript:Invia()">
			<option selected></option>
	<%
			do until RecDescBando.EOF
				appo=replace(RecDescBando("DESC_BANDO"),"'","$")
				appo=mid(appo,1,70)
				appo=replace(appo,"$","'")
				
				Response.Write "<OPTION "
				Response.write "<OPTION  value ='" & replace(RecDescBando("DESC_BANDO"),"'","$") & "|" & RecDescBando("ID_BANDO")& "'> " &_
					appo  & "</OPTION>"
									
				RecDescBando.MoveNext 				
			loop			
			Response.Write "</SELECT>"	
						
			RecDescBando.Close
			set RecDescBando = nothing		
		else%>
			Non ci sono bandi disponibili	
		<%end if%>
			</td>
		</tr>  
		<tr><td>&nbsp;</td>
		</tr>	
	<%end if%>
	</table>
	</form>

<%end sub
'----------------------------------------------------------------------------
sub ImpostaTab() 
	dim i
	
	Record=0
	nTamPagina=15

	If Request.Form ("pagina")= "" Then
		nActPagina=1
	Else
		nActPagina=Clng(Request.Form("pagina"))
	End If
	if Err.number <> "0" then
		nActPagina = 1
		Err.Clear
	end if%>
	<form name="frmInsAula" method="Post" action="CFO_InsAule.asp">
	<input type="Hidden" name="IdBan" value="<%=nIdBando%>">
	<input type="Hidden" name="DescBando" value="<%=sDescBando%>">
	</form>
	<table width="500" border="0">
		<form name="frmPagina" method="post" action="CFO_VisAule.asp">
		    <input type="hidden" name="pagina" value>
			<input type="Hidden" name="CmbDescBando" value="<%=sDescBando%>|<%=nIdBando%>">
		</form>		
		<tr> 
			<td align="center"> 
				<a title="Inserisci nuova aula" href="Javascript:frmInsAula.submit()" onmouseover="javascript:window.status=' '; return true" class="textred">
				<b>Inserisci nuova Aula</b></a>
			</td>
		</tr>
	</table>
	<br>
	<%	
	nCont = 0 
		
	sSQL =  "SELECT ID_CFORM, DESC_AULA, NUM_POSTI, TIPOCLASSE, ID_AULA " &_
			" FROM AULA WHERE ID_BANDO=" & nIdBando & _
			" AND ID_UORG = " & Session("iduorg") &	" ORDER BY ID_AULA"
		
	set rsAula = Server.CreateObject("ADODB.Recordset")
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	rsAula.Open sSQL,CC,3
	
	if rsAula.EOF then%>
		<input type="hidden" name="PRV" value="1">		
		<table width="500" border="0">
			<tr>
				<td class="tbltext3" align="center">
					<b>Non ci sono Aule per il Bando selezionato.
					</b>
				</td>
			</tr>	
		</table>
<%	else %>
		<table border="0" width="500">
			<tr class="sfondocomm"> 
				<td align="left" width="50">
					<b>Id Aula</b>
				</td>
				<td align="left" width="150">
					<b>Denominazione Aula</b>
				</td>
				<td align="left" width="50">
					<b>Posti</b>
				</td>
				<td align="left" width="250">
					<b>Centro Formazione</b>
				</td>
			</tr>
<%
		'cicla fino alla fine del recor [do while not]  
		rsAula.PageSize  = nTamPagina
		rsAula.CacheSize = nTamPagina	

		nTotPagina = rsAula.PageCount		
		If nActPagina < 1 Then
			nActPagina = 1
		End If
		If nActPagina > nTotPagina Then
			nActPagina = nTotPagina
		End If

		rsAula.AbsolutePage = nActPagina
		nTotRecord=0

		i = 1 
		While not rsAula.EOF  And nTotRecord < nTamPagina		
			nCont = 1
			sDescCform = " - " 

			if rsAula("ID_CFORM") <> "" then
			
				nIdCForm = clng(rsAula("ID_CFORM"))
				sSQL = "SELECT DESC_CFORM,PRV,COMUNE FROM CENTRO_FORMAZIONE WHERE ID_CFORM = " & nIdCForm
				
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
				set rsCform = CC.Execute(sSQL)
				if not rsCform.eof then
					sDescCform = rsCform("DESC_CFORM") & "(" & DescrComune(rsCform("COMUNE")) & " - " & rsCform("PRV") & ")"
				end if
				rsCform.Close
				set rsCform = nothing
				
			end if
			
%> 
			<tr class="tblsfondo">
				<td class="tblDett">
					<a title="Seleziona aula" class="tblAgg" href="javascript:frmModAula<%=i%>.submit()" onmouseover="javascript:window.status=' '; return true"><%=clng(rsAula("ID_AULA"))%></a>
				</td>
				<td class="tblDett">
					<%=rsAula("DESC_AULA")%>
				</td>
				<td class="tblDett">
					<%=rsAula("NUM_POSTI")%>
				</td>
				<td class="tblDett" align="center" width="300">
					<%=sDescCform%>
				</td>
			</tr>
			<form name="frmModAula<%=i%>" method="Post" action="CFO_ModAule.asp">
				<input type="hidden" name="idAula" value="<%=clng(rsAula("ID_AULA"))%>">
				<input type="hidden" name="descBando" value="<%=sDescBando%>">
				<input type="hidden" name="idBan" value="<%=nIdBando%>">
			</form>			
		<%	i = i + 1
			rsAula.MoveNext 
			nTotRecord=nTotRecord + 1
		wend%>
	</table>
<%		Response.Write "<TABLE border=0 width=470 align=center>"
		Response.Write "<tr>"
		if nActPagina > 1 then
			Response.Write "<td align=right width=480>"
			Response.Write "<A title=""Precedente"" HREF=""javascript:Carica_Pagina('" & nActPagina-1 & "')"">"
			Response.Write "<img border=0 src=" & Session("Progetto") & "/images/precedente.gif></A></td>"
		end if
		if nActPagina < nTotPagina then
			Response.Write "<td align=right>"
			Response.Write "<A title=""Successivo"" HREF=""javascript:Carica_Pagina('" & nActPagina+1 & "')"">"		
			Response.Write "<img border=0 src=" & Session("Progetto") & "/images/successivo.gif></A></td>"
		end if
		Response.Write "</tr></TABLE>"		
		
	end if
			
	rsAula.Close 
	set rsAula = nothing%> 
	<br>
	<!--impostazione dei comandi-->
	<table cellpadding="0" cellspacing="1" width="500" border="0">	
		<tr>
			<td align="center" colspan="3">
			<a title="Indietro" href="CFO_VisAule.asp"><img src="<%=session("Progetto")%>/images/indietro.gif" border="0" name="imgPunto2" onmouseover="javascript:window.status=' '; return true"></a></td>
		</tr>
	</table>	

<%end sub%>

<!----------------------------------------------------------------->
<%sub Fine()%>
	
	<!--#include virtual ="/include/closeconn.asp"-->
	<!--#include virtual = "strutt_coda2.asp"-->
<%end sub %>
<!-- ************** ASP Fine *************** -->

<%	
'*************** INIZIO MAIN ***********
if ValidateService(session("idutente"),"CFO_VisAule",CC) <> "true" then 
	response.redirect "/util/error_login.asp"
end if

dim sProv
dim i
dim nIdBando
dim sDescBando
dim sDescrComune
dim sBando
dim nActPagina

sProv=request("")	
sBando=Request("CmbDescBando")
sBando=Replace(sBando,"$","'")


if sBando <> "" then
	sDescBando	=Mid(sBando,1 ,InStr(1, sBando,"|") - 1)
	nIdBando	=Mid(sBando,InStr(1, sBando,"|") + 1)
end if	


Intestazione()
CaricaCombo()

if nIdBando <> "" then
	ImpostaTab()
end if		

Fine()
'*************** FINE   MAIN ***********	
%>		
