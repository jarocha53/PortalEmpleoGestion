<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include virtual="/strutt_testa2.asp"-->
<!--#include virtual="/include/openconn.asp"-->
<!--#include virtual="/util/portallib.asp"-->

<!-- ************** Javascript inizio ************ -->

<script LANGUAGE="Javascript">
<!--#include virtual = "/Include/help.inc"-->

function VaiInizio(nIdCF, sRi, sPrv){
	//location.href = "CFO_VisCentroFormazione.asp";
	location.href = "CFO_VisAule.asp?idCForm=" +  nIdCF + "&Rientro=" + sRi + "&cmbProv=" + sPrv;
}	

</script>

<!-- ************** Javascript Fine ************ -->

<!-- ************** ASP inizio *************** -->

<!--#include virtual = "/include/ckProfile.asp"-->
<!--#include virtual = "/util/dbutil.asp"-->
<!--#include virtual = "/include/SysFunction.asp"-->
<%
Sub Msgetto(Msg)
%>
	<br>
	<table align="center">
		<tr>
			<td class="tbltext3"><b><%=Msg%></b></td>
		</tr>	
	</table>
<%
End Sub

function PrgAulaMax(nIdCenForm,nIdBando)
	sSQL = "SELECT NVL(MAX(PRG_AULA),0)+1 AS ID FROM AULA " & _
		   " WHERE ID_CFORM=" & nIdCenForm & _
		   " AND ID_BANDO = " & nIdBando &_
		   " AND ID_UORG = " & Session("iduorg")
	
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	set rsPrgAula = CC.Execute(sSQL)
	PrgAulaMax = rsPrgAula("ID")
	rsPrgAula.Close
	set rsPrgAula = nothing
end function

function AggDescAula(Param,nIDAula)
	dim Errore
	sSQL = "UPDATE AULA "	&_
			"SET DESC_AULA='"	& sDenom & "'," &_
			" TIPOCLASSE='" & sTipoClasse &_
			"',NUM_POSTI ="		& sNumPos &_
			" ,DT_TMST ="		& ConvDateToDb(Now()) & _
			Param &	" WHERE ID_AULA="	& nIDAula
	Errore = EseguiNoC(sSQL,CC)
	AggDescAula = Errore
end function

function AllineaPrg(nIdBando,nIdCform,nPrgAula)
	sSQL = "UPDATE AULA SET PRG_AULA = (PRG_AULA-1)" &_
			" WHERE ID_CFORM=" & nIdCform &_
			" AND PRG_AULA > " & nPrgAula &_
			" AND ID_BANDO=" & nIdBando &_
			" AND ID_UORG= " & Session("iduorg")
	Errore=EseguiNoC(sSQL,CC)	
	AllineaPrg = Errore	
end function

function ModNumAuleCF(nAgg,nIdCF)
	sSQL = "UPDATE CENTRO_FORMAZIONE SET NUM_AULE = (NUM_AULE " & nAgg &_
			") WHERE ID_CFORM=" & nIdCF
	Errore=EseguiNoC(sSQL,CC)	
	ModNumAuleCF=Errore
end function


sub Modifica()
	nIDAula		= CLng(Request.Form("txtIDAula"))
	CC.BeginTrans
	sSQL = "SELECT COUNT(*) AS CONTA FROM AULA WHERE DESC_AULA = '" &_
		 sDenom & "' AND ID_AULA <> " & nIDAula
		 
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	set rsConta = CC.Execute(sSQL)
	
	if  CInt(rsConta("CONTA")) = "0" then

		Errore = "0"
		sDtTmst		= Request.Form("txtDT_TMST")
		if Request.Form("txtIdCFormOLD") <> "" then
			nIdCformOld = CLng(Request.Form("txtIdCFormOLD"))
		else
			nIdCformOld = 0 
		end if

		if (nIdCformOld = nIdCenForm) then
			' St� semplicemente modificando la descrizione dell'aula
			' Response.Write "<Br> CASO -1" 
			Errore=AggDescAula("",nIDAula)
		else
			 select case nIdCformOld
				case 0 
					' Response.Write "<Br> CASO 0" 
					' Modifico un aula senza centro di formazione assegnato
					' asseganndo ad essa un centro di formazione.
					nPrgAula = PrgAulaMax(nIdCenForm,nIdBando) ' Ho preso il Max PRG
					'Response.Write "<BR> nPrgAula" & nPrgAula
					Errore=AggDescAula(", PRG_AULA=" & nPrgAula & ",ID_CFORM=" & nIdCenForm ,nIDAula)
					if Errore = "0" then
						Errore=ModNumAuleCF("+ 1",nIdCenForm) 
					end if
	
				case else
					if nIdCformOld > 1 and  nIdCenForm = 0 then
						' Response.Write "<Br> CASO 1" 
						' Modifico un aula con centro di formazione assegnato
						' eliminando l'associazione con il Centro di formazione.
						nPrgAula = Request.Form("txtPrgAula") 
						' Devo pulire tutti i prg_aule successivi
						Errore=AggDescAula(",ID_CFORM=NULL, PRG_AULA=0",nIDAula)
						if Errore = "0" then
							Errore=ModNumAuleCF("- 1",nIdCformOld) 
						end if
						if Errore = "0" then	
							Errore = AllineaPrg(nIdBando,nIdCformOld,nPrgAula)
						end if	
					else   'if nIdCformOld > 1 and  nIdCenForm <> nIdCformOld then
						' Response.Write "<Br> CASO 2" 
						' Modifico un aula con centro di formazione assegnato
						' associando un altro Centro di formazione.
						nPrgAula	= Request.Form("txtPrgAula") 
						nPrgAulaNew = PrgAulaMax(nIdCenForm,nIdBando) ' Ho preso il Max PRG
						Errore=AggDescAula(",ID_CFORM=" & nIdCenForm & ",PRG_AULA=" & nPrgAulaNew,nIDAula)
						if Errore = "0" then
							Errore=ModNumAuleCF("- 1",nIdCformOld) 
						end if
						if Errore = "0" then
							Errore=ModNumAuleCF("+ 1",nIdCenForm) 
						end if
						if Errore = "0" then	
							Errore = AllineaPrg(nIdBando,nIdCformOld,nPrgAula)
						end if				
					end if
			end select 
		end if  
	else
		Errore = "Esiste gi� una classe per questo Bando<br> con la stessa denominazione"
	end if
	rsConta.Close
	set rsConta = nothing
	if Errore = "0" then
		CC.CommitTrans
		TornaIndietro()
	else
		CC.RollBackTrans
		Msgetto(Errore)
	end if
	
end sub


'-------------------------------------------------------------------------------------------------------------------------------	

sub TornaIndietro()
'Response.Write sProv & "sProv"

	%>
	<form name="frmIndietro" method="post" action="CFO_VisAule.asp">
		<input type="hidden" name="CmbDescBando" value="<%=Request.Form("txtDescBando")%>|<%=nIdBando%>">
	</form>

	<script>
		alert("Operazione correttamente effettuata");
		frmIndietro.submit()
	</script>
	<%

end sub

'-------------------------------------------------------------------------------------------------------------------------------	

sub Inizio()
%>
	<table border="0" width="520" cellspacing="0" cellpadding="0" height="81">
	   <tr>
	     <td width="500" background="<%=Session("Progetto")%>/images/titoli/strumenti2b.gif" height="81" valign="bottom" align="right">
	       <table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
	         <tr>
	           <td width="100%" valign="top" align="right">
	           <b class="tbltext1a">Gestione Centri Di Formazione</b></td>	         
			</tr>
	       </table>
	     </td>
	   </tr>
	</table>

	<br>
	<table cellpadding="0" cellspacing="0" width="500" border="0">
		<tr height="18">
			<td class="sfondomenu" height="18" width="67%">
			<span class="tbltext0"><b>&nbsp;DISPONIBILITA' AULE</b></span></td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
			<td valign="middle" align="right" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1"></td>
		</tr>
		<tr width="371" class="SFONDOCOMM">
			<td colspan="3">Pagina di conferma disponibilita' aule.<a href="Javascript:Show_Help('/Pgm/help/GestProgetti/Alfabeta/CFormazione/CFO_CfnAule')" onmouseover="javascript:window.status=' '; return true"><img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
			</td>
		</tr>
		<tr height="2">
			<td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>
	<br>
<%

end sub

'------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

sub GetParam()

	nIdUorg = Session("iduorg")
	nIdBando = clng(Request.Form ("txtBando"))
	sDenom	= UCase(Request.Form ("txtDenominazione"))
	sDenom	= Replace (sDenom,Chr(34),"'")
	sDenom	= Replace (sDenom,"'","''")

	sNumPos		= Request.Form ("txtNumPosti")
	sAction		= Request.Form ("txtAction")
	if Request.Form("txtIdCForm") <> "" then
		nIdCenForm = CLng(Request.Form  ("txtIdCForm"))
	else
		nIdCenForm = 0
	end if
	
	sTipoClasse = Request.Form("cmbTipoClasse")

	sModo		= Request.QueryString ("MODO")
	if sModo = "CANC" then
		sAction = "CANC"
	end if

end sub

'------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

sub Inserisci()
	
	sSQL = "SELECT COUNT(*) AS CONTA FROM AULA WHERE DESC_AULA = '" &_
		 sDenom & "'" & " AND ID_BANDO = " & nIdBando
'	Response.Write sSQl
	'Response.End 	 

'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	set rsConta = CC.Execute(sSQL)
	
	CC.BeginTrans
	if  clng(rsConta("CONTA")) = "0" then

		
		if nIdCenForm > 0 then
			sSQL = "SELECT NVL(MAX(PRG_AULA),0)+1 AS ID " & _
				   "FROM AULA WHERE ID_CFORM = " & nIdCenForm
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
			set rsAula = CC.Execute(sSQL)
			nPrgAula = rsAula("ID")
			rsAula.Close
			set rsAula = nothing
			sSQL = "INSERT INTO AULA " &_
					"(ID_BANDO,ID_UORG,ID_CFORM,DESC_AULA," &_
					" NUM_POSTI,PRG_AULA,TIPOCLASSE,DT_TMST) VALUES " &_
					"("		& nIdBando	&_
					","		& nIdUorg	&_
					","		& nIdCenForm	&_
					",'"	& sDenom			&_
					"',"	& sNumPos		&_
					","		& nPrgAula &_
					",'"    & sTipoClasse &_
					"',"	& ConvDateToDb(Now()) & ")" 
		else
			sSQL = "INSERT INTO AULA " &_
					"(ID_BANDO,ID_UORG,DESC_AULA,NUM_POSTI,TIPOCLASSE,DT_TMST) VALUES " &_
					"("	&   nIdBando			&_
					","	&   nIdUorg			&_
					",'"	& sDenom			&_
					"',"	& sNumPos		&_
					",'"    & sTipoClasse &_
					"',"		& ConvDateToDb(Now()) & ")" 
		end if
%>
<br>
<%		
'		Response.Write sSQL
'		Errore=Esegui("prova","AULA",Session("idutente"),"INS", sSQL, 1,sDtTmst)
'		Response.Write sSQL
		Errore=EseguiNoC(sSQL,CC)
	else
	
		Errore = "Esiste un'aula per questo Bando<br> con la stessa denominazione."
			
	end if 

	if Errore = "0" then
		if nIdCenForm > 0 then
			sSQL = "SELECT NUM_AULE FROM CENTRO_FORMAZIONE " & _
				   "WHERE ID_CFORM=" & nIdCenForm

'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
			set rsCForm = CC.Execute(sSQL)
			sNumAula = CInt(rsCForm("NUM_AULE"))
	
			sNumAula = sNumAula + 1
			rsCForm.Close
			set rsCForm = nothing
			sSQL = "UPDATE CENTRO_FORMAZIONE SET NUM_AULE = " & sNumAula &_
					" WHERE ID_CFORM=" & nIdCenForm
			Errore=EseguiNoC(sSQL,CC)			      	
		end if
		
		if Errore = "0" then
			CC.CommitTrans
			TornaIndietro()
		else 
			Msgetto (Errore)
			CC.RollbackTrans
		end if 
	else
		Msgetto (Errore)
		CC.RollbackTrans
	end if
		      	
end sub

'------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

sub Elimina()

	nIdCenForm	= Request.QueryString ("IdCenForm")
	'Response.Write nIdCenForm & "=centro formazione"
	nIDAula = Request.QueryString ("idAula")
		
	sSQL = "SELECT COUNT(*) as CONTA FROM CLASSE " & _
			"WHERE ID_AULA = " & clng(nIDAula)
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	set rsConta = CC.Execute(sSQL)
	nConta = clng(rsConta("CONTA"))
	rsConta.CLose
	set rsConta = nothing
	
	if nConta	= 0 then 
		CC.BeginTrans
		sSQL = "DELETE FROM AULA WHERE ID_AULA = " & clng(nIDAula)
		Errore	= EseguiNoC(sSQL,CC)
		
		sSQL = "SELECT PRG_AULA FROM AULA " & _
				"WHERE ID_CFORM = " & clng(nIdCenForm) & _
				" ORDER BY PRG_AULA"
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
		set rsPrgAula = CC.Execute(sSQL)
'		Response.Write sSQL

		t = 1
		do while not rsPrgAula.EOF 
			if clng(rsPrgAula("PRG_AULA")) <> t then
				sSQL = "UPDATE AULA SET PRG_AULA=" & clng(t) &_
						" WHERE ID_CFORM =" & clng(nIdCenForm) &_
						" AND PRG_AULA = " & clng(rsPrgAula("PRG_AULA"))
				Errore = EseguiNoC(sSQL,CC)
'					Response.Write sSQL
				if 	Errore <> "0" then
					exit do
				end if 
			end if 
			t = t + 1
			rsPrgAula.MoveNext
		loop
		
		if Errore = "0" then
			sSQL = "UPDATE CENTRO_FORMAZIONE SET NUM_AULE = " &_
					"(NUM_AULE-1) WHERE ID_CFORM = " & clng(nIdCenForm)
			Errore = EseguiNoC(sSQL,CC)
		end if 
	else
		Errore	= "Non � possibile eliminare un'aula per la quale sono state programmate delle classi" 	
	end if
	
	if Errore = "0" then
		CC.CommitTrans
		TornaIndietro()
	else
		CC.RollBackTrans
		Msgetto (Errore)
	end if

end sub

'------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

sub Fine()
%>
	<br>
	<br>
	<table border="0" cellpadding="0" cellspacing="1" width="300">
		<tr>			
			<td align="center">
				<a title="Indietro" href="javascript:history.back()">					
					<img src="<%=session("Progetto")%>/images/indietro.gif" border="0" name="imgPunto2" onmouseover="javascript:window.status=' '; return true">				
					</a>
				<!--<a href="javascript:VaiInizio('<%'=nIdCenForm%>','<%'=sRientro%>','<%'=sProv%>')">					<img src="<%'=session("Progetto")%>/images/indietro.gif" 					border="0" name="imgPunto2" 					onmouseover="javascript:window.status=' '; return true">				</a-->
			</td>
		</tr>
	</table>
	<br>
<!--#include virtual = "/include/closeconn.asp"-->
<!--#include virtual = "strutt_coda2.asp"-->
<%

end sub
%>
<!-- ************** ASP Fine *************** -->

<!-- ************** MAIN Inizio ************ -->
<%

if ValidateService(session("idutente"),"CFO_VisAule",CC) <> "true" then 
	response.redirect "/util/error_login.asp"
end if

dim sAction
dim sDenom
dim nIdCenForm
dim sRientro
dim nIDAula
dim sNumPos
dim sSQL
dim sTipoClasse
dim nIdUorg, nIdBando 
dim sProv

dim sDtTmst


	Inizio()
	GetParam()
	
	select case sAction
		case "INS"
			Inserisci()
			
		case "MOD"
			Modifica()
			
		case "CANC"
			Elimina()
			
	end select
	
	Fine()
%>
