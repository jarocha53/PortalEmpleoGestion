<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include virtual="/strutt_testa2.asp"-->
<!--#include virtual = "/util/dbutil.asp"-->
<!--#include virtual = "/include/ControlDateVB.asp"-->
<!--#include virtual = "/include/SysFunction.asp"-->
<!--#include virtual = "/include/openconn.asp"-->
	
<%
	dim sParam, sAction, nBando, sFase, sRuolo, sValida
	dim sSQL, rs
	
	sAction= Request.Form("txtAction")
	
	nBando=  Request.Form("txtBando")
	sFase=   Request.Form("txtFase")
	sRuolo=  Request.Form("txtRuolo")
	sCmbFun= Request.Form("CmbFun")
	
	sParam=  server.HTMLEncode(UCASE(TRIM(Request.Form("txtParam"))))
	sParam=	 Replace(sParam,"'","''")
	sValida= server.HTMLEncode(UCASE(TRIM(Request.Form("txtValida"))))
	sValida= Replace(sValida,"'","''")
%>
	<form name="FrmBack" action="PPR_VisStrumenti.asp" method="post">
		<input type="hidden" name="hIDBando" value="<%=nBando%>">
		<input type="hidden" name="hCodSess" value="<%=sFase%>">
		<input type="hidden" name="cmbRuoFun" value="<%=sRuolo%>">
	</form>
<%
	select case sAction
		case "INS"
			Inserisci()
		case "MOD"
			Modifica()	
	end select

'-------------------'
Sub Inizio() %>
<table border="0" width="520" cellspacing="0" cellpadding="0" height="81">
	<tr>
	  <td width="500" background="<%=Session("Progetto")%>/images/titoli/strumenti2b.gif" height="81" valign="bottom" align="right">
	    <table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
	      <tr>
	        <td width="100%" valign="top" align="right"><b class="tbltext1a">Gestione Progetti</b></td>
	      </tr>
	    </table>
	  </td>
	</tr>
</table>
<br>
<table cellpadding="0" cellspacing="0" width="500" border="0">
	<tr height="18">
		<td class="sfondomenu" height="18" width="67%">
		<span class="tbltext0"><b>&nbsp;CONFERMA PERCORSO PROGETTUALE</b></span></td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
		<td valign="middle" align="right" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1"></td>
	</tr>
	<tr width="371" class="SFONDOCOMM">
		<td colspan="3">
			Conferma Associazione Strumenti.
		</td>
	</tr>
	<tr height="2">
		<td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif">
		</td>
	</tr>
</table>
<br><br>
<%	
End Sub

'-------------------'
Sub Inserisci()
	dim rsVer, nVerifica 
	
	CC.BeginTrans
	Errore = "0"
		
	sSQL =	"INSERT INTO FASE_STRUMENTO (ID_BANDO,COD_SESSIONE, COD_RUOFU, IDFUNZIONE,PARAM,FUN_VAL,DT_TMST)" &_
			" VALUES(" & nBando & ",'" & sFase & "','" & sRuolo & "'," & sCmbFun &_
			",'" & sParam & "','" & sValida & "'," & ConvDateToDB(now()) & ")"  
				
	Errore=EseguiNoC(sSQL,CC)		
	
	if Errore = "0" then
		CC.CommitTrans
%>
		<script>
			alert("Inserimento correttamente eseguito");
			FrmBack.submit()
		</script>
<%
	else
		CC.RollbackTrans
		Inizio()
%>
		<table border="0" cellspacing="1" cellpadding="1" width="500">
			<tr align="middle">
				<td class="tbltext3">
					Inserimento non effettuabile.
				</td>
			</tr>
			<tr align="middle">
				<td class="tbltext3">
					<%Response.Write(Errore)%> 
				</td>
			</tr>
		</table>
		<br>
						
		<table border="0" cellpadding="0" cellspacing="1" width="500">
			<tr>
				<td align="middle" colspan="2"><b>
				 <a HREF="javascript:history.back()"><img SRC="<%=Session("Progetto")%>/images/indietro.gif" border="0">
				</td>
			</tr>
		</table>
	<%			
	end if
End Sub

'-------------------'
Sub Modifica()
	dim sSQL, rsVer 
	
	dTimeStamp=	Request.Form("txtTMST")

	Errore = "0"
	
	sSQL =	"UPDATE FASE_STRUMENTO SET"			&_
			" PARAM='" & sParam & "',"			&_
			" FUN_VAL='" & sValida & "',"		&_
			" DT_TMST="	& ConvDateToDB(now())	&_
			" WHERE ID_BANDO=" & nBando			&_
			" AND COD_SESSIONE='" & sFase & "'" &_
			" AND COD_RUOFU='" & sRuolo & "'"	&_
			" AND IDFUNZIONE=" & sCmbFun		

	chiave=""
	Errore= Esegui(chiave,"FASE_STRUMENTO",session("idutente"),"MOD", sSQL, 0, dTimeStamp)
	
	if Errore = "0" then
%>
		<script>
			alert("Modifica correttamente effettuata.");
			FrmBack.submit()
		</script>
<%		
	else
		Inizio()
	%>
		<table border="0" cellspacing="1" cellpadding="1" width="500">
			<tr align="middle">
				<td class="tbltext3">
					Modifica non effettuabile.
				</td>
			</tr>
			<tr align="middle">
				<td class="tbltext3">
					<%Response.Write(Errore)%> 
				</td>
			</tr>
		</table>
		<br>				
		<table border="0" cellpadding="0" cellspacing="1" width="500">
			<tr>
				<td align="middle" colspan="2"><b>
				 <a HREF="javascript:history.back()"><img SRC="<%=Session("Progetto")%>/images/indietro.gif" border="0">
				</td>
			</tr>
		</table>
<%
	End if
End Sub	

'-------------------'
%>
<!--#include virtual ="/include/CloseConn.asp"-->
<!--#include virtual = "/strutt_coda2.asp"-->
