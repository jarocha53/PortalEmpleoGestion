<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<script LANGUAGE="Javascript">
<!--#include Virtual = "/Include/help.inc"-->

//PAGINAZIONE//
function AssegnaVal(val){
	document.FrmPagina.pagina.value = val;
	document.FrmPagina.submit()
}

//03-04-2003: commentata funzione che rende obbligatoria 
//la selezione di un R. Funz., poich� la combo � momentaneamente disabilitata.
/*function Reload()
{
	var nSel
	nSel = frmRuoFun.cmbRuoFun.selectedIndex
	sRuolo = frmRuoFun.cmbRuoFun.options[nSel].value

	if (sRuolo == "")
	{
		alert("Selezionare un Ruolo Funzionale")
	}
	else
	{
		document.forms[0].submit();
	}
}*/
</script>

<%
'-------------------'
Sub Inizio()%>

	<table border="0" width="520" cellspacing="0" cellpadding="0" height="81">
	   <tr>
	     <td width="500" background="<%=Session("Progetto")%>/images/titoli/strumenti2b.gif" height="81" valign="bottom" align="right">
	       <table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
	         <tr>
	           <td width="100%" valign="top" align="right"><b class="tbltext1a">Gestione Progetti</b></td>
	         </tr>
	       </table>
	     </td>
	   </tr>
	</table>
	<br>
	<table cellpadding="0" cellspacing="0" width="500" border="0">
		<tr height="18">
			<td class="sfondomenu" height="18" width="67%">
			<span class="tbltext0"><b>&nbsp;PERCORSO PROGETTUALE</b></span></td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
			<td valign="middle" align="right" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1"></td>
		</tr>
		<tr width="371" class="SFONDOCOMM">
			<td colspan="3">
				<!--Selezionare un Ruolo Funzionale per visualizzarne le Funzioni associate.-->
				La pagina visualizza l'elenco delle Funzioni associate al Discente per la sessione e il bando selezionati.
				<br>Per visualizzare e/o modificare il dettaglio di una Funzione premere sulla <b>descrizione</b> relativa. 
				Per inserire una Funzione non ancora registrata premere sul tasto <b>Aggiungi</b>.
				<a href="Javascript:Show_Help('/Pgm/help/Gestprogetti/alfabeta/PercProj/PPR_VisStrumenti')">
				<img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0">
				</a>
			</td>
		</tr>
		<tr height="2">
			<td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>
<%
End Sub

'-------------------'
Sub SelRuoFun(sSelez)
	dim sStringa
%>
	<form name="frmRuoFun" method="post" action="PPR_VisStrumenti.asp">
	<input type="hidden" name="hIdBando" value="<%=nBando%>">
	<input type="hidden" name="hCodSess" value="<%=sFase%>">
	<br>
	<table width="500" border="0" cellspacing="2" cellpadding="1">
		<tr height="20">
			<td class="tbltext1" width="20%">
				<b>Bando</b>
			</td>
			<td class="tbltext">
				<b><%=sDescBando%></b>
			</td>
		</tr>
		<tr height="20">
			<td class="tbltext1" width="20%">
				<b>Sessione</b>
			</td>
			<td class="tbltext">
				<b><%=sFase%> - <%=sDescFase%></b>
			</td>
		</tr>
		<tr height="20"> 
			<td align="left" width="30%">	
				<span class="tbltext1">
					<b>
					<!--Selezionare un Ruolo-->
					Ruolo
					</b>
				</span>
			</td>
			<td align="left" class="tbltext">
<%				'03-04-03: Controllo da riattivare quando si ripristina la combo del Ruolo.
				'if sModo=0 then
				'	sStringa="cmbRuoFun|0|DI|onchange='Javascript:Reload()'"
				'else
				'	sStringa="cmbRuoFun|0|" & sSelez & "|onchange='Javascript:Reload()'"
				'end if
				'CreateRuolo(sStringa)
%>				<b><%=DecRuolo("DI")%></b>
			</td>
		</tr>
	</table>
	</form>
<%
End Sub

'-------------------'
Sub VisFunzioni()

	dim nBando, sCodSess, sDescFase, rs, sSQL
	
	nBando= Request.Form("hIdBando")
	sFase=	Request.Form("hCodSess")
	
	'03-04-03: Controllo da riattivare quando si ripristina la combo del Ruolo.
	'if sModo=0 then
		sRuolo="DI"
	'else
	'	sRuolo=	Request.Form("cmbRuoFun")
	'end if
	
	set rs = Server.CreateObject("ADODB.Recordset")	
	sSQL=	"select fs.idfunzione, f.desfunzione from FASE_STRUMENTO FS, FUNZIONE F" &_
			" where FS.id_bando=" & nBando &_
			" and FS.cod_sessione='" & sFase &_
			"' and FS.cod_ruofu='" & sRuolo &_
			"' and FS.idfunzione=F.idfunzione" &_
			" order by F.desfunzione"
			
	rs.Open	sSQL,cc,3 
		
	If rs.EOF then%>		
	   <br>
	   <table align="center">
			<tr>
				<td class="tbltext3" align="center">
					<b>Non risultano Strumenti/Servizi per il Ruolo Funzionale.</b>
				</td>
			</tr>	
		</table>
<%	Else
		rs.PageSize = nTamPagina
		rs.CacheSize = nTamPagina
		nTotPagina = rs.PageCount 
		If nActPagina < 1 Then
			nActPagina = 1
		End If
		If nActPagina > nTotPagina Then
			nActPagina = nTotPagina
		End If
		rs.AbsolutePage= nActPagina
%>
		<br>
		<table border="0" cellpadding="1" cellspacing="2" width="500">
			<tr class="sfondocomm">
				<td align="center">
					<b>Strumenti/Servizi</b>
				</td>			
			</tr>
<%		i=0
		nTotRecord=0
		do while not rs.EOF and nTotRecord < nTamPagina
			nIdFun = rs("idfunzione")
			sDescFun = rs("desfunzione")
			i = i + 1
%>
			<form name="FrmVisStrum<%=i%>" action="PPR_ModStrumenti.asp" method="post">
				<input type="hidden" name="hBando" value="<%=nBando%>">
				<input type="hidden" name="hSessione" value="<%=sFase%>">
				<input type="hidden" name="hRuolo" value="<%=sRuolo%>">
				<input type="hidden" name="hFun" value="<%=nIdFun%>">
			</form>
			
			<tr class="tblsfondo">
				<td class="tbldett">
					<a class="tblAgg" HREF="Javascript:FrmVisStrum<%=i%>.submit();" onmouseover="javascript:window.status='' ; return true">
						<b><%=sDescFun%></b>
					</a>
				</td>														
			</tr>					
<%		nTotRecord = nTotRecord + 1
		rs.MoveNext
		loop %>
		</table>

<!--PAGINAZIONE-->
		<form name="FrmPagina" method="post" action="PPR_VisStrumenti.asp">
		<input type="hidden" id="pagina" name="pagina" value="<%=nActPagina%>">
		<input type="hidden" name="hIdBando" value="<%=nBando%>">
		<input type="hidden" name="hCodSess" value="<%=sFase%>">
		<input type="hidden" name="cmbRuoFun" value="<%=sRuolo%>">
		
		<table border="0" width="500">
			<tr>
			<%
			if nActPagina > 1 then%>		
				<td align="right" width="480">
					<a href="Javascript:AssegnaVal('<%=(nActPagina - 1)%>')">
						<img src="<%=Session("Progetto")%>/images/precedente.gif" border="0" id="image1" name="image1">
					</a>
				</td>
			<%
			end if
			if nActPagina < nTotPagina then%>
				<td align="right">
					<a href="Javascript:AssegnaVal('<%=(nActPagina + 1)%>')">
						<img src="<%=Session("Progetto")%>/images/successivo.gif" border="0" id="image1" name="image1">
					</a>
				</td>
			<%
			end if%>		
			</tr>
		</table>
		</form>			
		
<%	rs.Close
	set rs=nothing	
	End if%>
	
	<form name="FrmInsNuovo" action="PPR_InsStrumenti.asp" method="post">
		<input type="hidden" name="hBando" value="<%=nBando%>">
		<input type="hidden" name="hSessione" value="<%=sFase%>">
		<input type="hidden" name="hRuolo" value="<%=sRuolo%>">	
	</form>
	
	<form name="FrmIndietro" action="PPR_ModFaseBando.asp" method="post">
		<input type="hidden" name="hdnID" value="<%=nBando%>">
		<input type="hidden" name="hdnFaseB" value="<%=sFase%>">
	</form>
	
	<br>	
	<table width="500" border="0">
		<tr align="center">
			<td><a href="javascript:FrmIndietro.submit();" onmouseover="javascript:window.status=' '; return true"><img src="<%=session("Progetto")%>/images/indietro.gif" border="0" name="imgPunto2"></a></td>
   			<td><a href="Javascript:FrmInsNuovo.submit();" onmouseover="javascript:window.status=' '; return true"><img src="<%=session("Progetto")%>/images/aggiungi.gif" border="0" name="imgPunto2"></a></td>
	    </tr>
	</table>  
<%
End Sub

'-------------------'
Sub Fine()
%>
	<!--#include virtual ="/include/closeconn.asp"-->
	<!--#include virtual = "/strutt_coda2.asp"-->
<%
End sub 

'-------------------------------------------------' 
'-------------------' M A I N '-------------------' 
%>
	<!-- #include virtual="/strutt_testa2.asp"-->
	<!--#include virtual ="/include/openconn.asp"-->
	<!--#include virtual="/util/portallib.asp"-->
	<!--#include virtual ="/include/SelAreaTerrBandi.asp"-->
	<!--#include virtual ="/include/RuoloFunzionale.asp"-->
<%
	if ValidateService(session("idutente"),"PPR_VISFASEBANDO", CC) <> "true" then 
		response.redirect "/util/error_login.asp"
	end if

	dim nBando, sFase, sDescBando, sDescFase
	dim Record, nIndElem
	dim nActPagina, nTamPagina
	dim nTotPagina, nTotRecord
	
	nTamPagina=10
	If Request("pagina")= "" Then
		nActPagina=1
	Else
		nActPagina=Clng(Request("pagina"))
	End If

	nBando=		Request.Form("hIdBando")
	sDescBando=	DecBando(nBando)
	sFase=		Request.Form("hCodSess")
	sDescFase=	DecFaseBando(nBando, sFase)
	
	'03-04-03: Controllo da riattivare quando si ripristina la combo del Ruolo.
	'if Request("cmbRuoFun") = "" then
	'	sModo=0
	'else
	'	sModo=1
	'	sSelez=request("cmbRuoFun")
	'end if

	Inizio()
	SelRuoFun(sSelez)
	VisFunzioni()
	Fine()
%>


