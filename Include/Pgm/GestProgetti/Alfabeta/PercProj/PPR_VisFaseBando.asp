<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<script LANGUAGE="Javascript">
<!--#include Virtual = "/Include/help.inc"-->

function Reload()
{
	var nSel
	nSel = frmVisBandi.cmbBando.selectedIndex
	nBando = frmVisBandi.cmbBando.options[nSel].value

	if (nBando == "")
	{
		alert("Selezionare un Bando")
	}
	else
	{
	
		document.frmVisBandi.submit();
	}
}
</script>

<%
'-------------------'
Sub Inizio()%>

	<table border="0" width="520" cellspacing="0" cellpadding="0" height="81">
	   <tr>
	     <td width="500" background="<%=Session("Progetto")%>/images/titoli/strumenti2b.gif" height="81" valign="bottom" align="right">
	       <table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
	         <tr>
	           <td width="100%" valign="top" align="right"><b class="tbltext1a">Gestione Progetti</b></td>
	         </tr>
	       </table>
	     </td>
	   </tr>
	</table>
	<br>
	<table cellpadding="0" cellspacing="0" width="500" border="0">
		<tr height="18">
			<td class="sfondomenu" height="18" width="67%">
			<span class="tbltext0"><b>&nbsp;PERCORSO PROGETTUALE</b></span></td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
			<td valign="middle" align="right" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1"></td>
		</tr>
		<tr width="371" class="SFONDOCOMM">
			<td colspan="3">
				Selezionare un Bando per visualizzarne le Fasi attuative.
				<br>Per visualizzare e/o modificare il dettaglio di una Fase premere sul<b> Codice Sessione</b> relativo. 
				<br>Per inserire una Fase attuativa non ancora registrata premere su <b> 
				Inserisci nuova Fase</b>.
				Se risultano presenti Domande di iscrizione al Bando <b>non</b> saranno possibili n� Inserimento n� Modifica.
				<a href="Javascript:Show_Help('/Pgm/help/Gestprogetti/alfabeta/PercProj/PPR_VisFaseBando')">
				<img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0">
				</a>
			</td>
		</tr>
		<tr height="2">
			<td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>
<%
End Sub

'-------------------'
Sub SelBando()
%>
	<form name="frmVisBandi" method="post" action="PPR_VisFaseBando.asp">
	<br>
	<table width="500" border="0" cellspacing="2" cellpadding="1">
		<tr> 
<%			
		sSQL="SELECT B.id_bando, B.desc_bando from BANDO B, PROGETTO P" & _
			 " where B.id_proj=P.id_proj and P.AREA_WEB= '" & sSessProj & "'" 
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
		set rsBando = CC.Execute(sSQL)				
		IF not rsBando.eof then 
%>
			<td align="left">	
				<span class="tbltext1">
					<b>Selezionare un Bando</b>
				</span>
			</td>
			<td align="left">
<%				
				Response.Write "<SELECT class='textblack' ID=cmbBando name=cmbBando onchange='Javascript:Reload()'>"
				Response.Write "<OPTION></OPTION>"
									
				Do until rsBando.EOF
					Response.Write "<OPTION "
					Response.write "value ='" & rsBando("ID_BANDO") & _
						"'> " &  rsBando("desc_bando")  & "</OPTION>"
				rsBando.MoveNext 
				Loop
								
				Response.Write "</SELECT>"
%>				
			</td>
<%		ELSE %>
			<td align="center" class="tbltext3">
				<b>Non esistono Bandi associati al presente progetto.</b>
			</td>
<%		END IF 
		rsBando.Close
		set rsBando=nothing %>	
		</tr>
	</table>
	</form>
<%
End Sub

'-------------------'
Sub VisSessioni(bDomIscr)

	dim nBando, sCodSess, sDescFase, rsSess, sSQL
	
	nBando = Request.Form("cmbBando")
	sSQL="select cod_sessione, desc_fase, desc_bando from FASE_BANDO FB, BANDO B where FB.id_bando=B.id_bando and B.id_bando= " & nBando & " order by prg_fase"
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	set rsSess = CC.Execute(sSQL)	
	
	If rsSess.EOF then%>		
	   <table align="center">
			<tr>
				<td class="tbltext3" align="center">
					<b>Non risulta definito un percorso progettuale per il bando selezionato.</b>
				</td>
			</tr>	
		</table>
<%	Else%>
		<table border="0" cellpadding="0" cellspacing="0" width="500">
			<tr>
				<td class="tbltext1" width="20%">
					<b>Bando: </b>
				</td>
				<td class="tbltext">
					<b><%=rsSess("DESC_BANDO")%></b>
				</td>
			</tr>
		</table>
		<br>
		<table border="0" cellpadding="1" cellspacing="2" width="500">
			<tr class="sfondocomm">
				<td align="center" width="20%">
					<b>Codice Sessione</b>
				</td>
				<td align="center">
					<b>Descrizione</b>
				</td>			
			</tr>
<%		i=0
		do while not rsSess.EOF
			sCodSess = rsSess("cod_sessione")
			sDescFase = rsSess("desc_fase")
			i = i + 1
%>
			<form name="FrmVisFaseB<%=i%>" action="PPR_ModFaseBando.asp" method="post">
				<input type="hidden" name="hdnID" value="<%=nBando%>">
				<input type="hidden" name="hdnFaseB" value="<%=sCodSess%>">
				<input type="hidden" name="hDomIscr" value="<%=bDomIscr%>">
			</form>
			
			<tr class="tblsfondo">
				<td class="tbldett">
					<a class="tblAgg" HREF="Javascript:FrmVisFaseB<%=i%>.submit();" onmouseover="javascript:window.status='' ; return true">
						<b><%=sCodSess%></b>
					</a>
				</td>														
				<td class="tbldett"><%=sDescFase%></td>					
			</tr>
						
<%		rsSess.MoveNext
		loop %>
		</table>
		
<%	rsSess.Close
	set rsSess=nothing	
	End if%>

	<br>	
	<table width="500" border="0">
		<tr align="center">
			<td><a href="PPR_VisFaseBando.asp" onmouseover="javascript:window.status=' '; return true"><img src="<%=session("Progetto")%>/images/indietro.gif" border="0" name="imgPunto2"></a></td>
	    </tr>
	</table>  
<%
End Sub

'-------------------'
Function InsSess()%>
	
<%	
	Dim nBando, rsDom, sSQL
	
	nBando = Request.Form("cmbBando")
	sSQL="SELECT id_persona from DOMANDA_ISCR where id_bando=" & nBando
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	set rsDom = CC.Execute(sSQL)
		
	If Not rsDom.EOF then
	'esiste almeno 1 domanda iscr. al bando; non si pu� 
	'inserire nuova fase n� modificare una esistente. 
		InsSess=false 
%>
		<table border="0" cellpadding="0" cellspacing="1" width="500">
			<tr>
				<td align="center">&nbsp;</td>
			</tr>
		</table>
		<br>
<%	Else
	'non esiste domanda iscr. al bando; si pu� 
	'inserire nuova fase e modificare una esistente.
		InsSess=true
%>	
		<br>
		<form name="FrmInsNuovo" action="PPR_InsFaseBando.asp" method="post">
				<input type="hidden" name="hdnID" value="<%=nBando%>">
		</form>
		
		<table border="0" cellpadding="0" cellspacing="1" width="500">
			<tr>
				<td align="center">
					<a class="textred" HREF="Javascript:FrmInsNuovo.submit();" onmouseover="javascript:window.status='' ; return true">
						<b>Inserisci nuova Fase</b>
					</a>
				</td>																		
			</tr>
		</table>
		<br>
<%	End if
	rsDom.Close
	set rsDom = nothing
End Function

'-------------------'
Sub Fine()
%>
	<!--#include virtual ="/include/closeconn.asp"-->
	<!--#include virtual = "/strutt_coda2.asp"-->
<%
End sub 

'-------------------------------------------------' 
'-------------------' M A I N '-------------------' 
%>
	<!-- #include virtual="/strutt_testa2.asp"-->
	<!--#include virtual ="/include/openconn.asp"-->
	<!--#include virtual="/util/portallib.asp"-->
<%
	if ValidateService(session("idutente"),"PPR_VISFASEBANDO", CC) <> "true" then 
		response.redirect "/util/error_login.asp"
	end if

	dim sSessProj, bDomIscr

	sSessProj=mid(session("progetto"),2)

	Inizio()

	if Request.Form("cmbBando") = "" then
		SelBando()
		Fine()	
	else
		bDomIscr=InsSess()
		VisSessioni(bDomIscr)	
		Fine()
	end if
%>


