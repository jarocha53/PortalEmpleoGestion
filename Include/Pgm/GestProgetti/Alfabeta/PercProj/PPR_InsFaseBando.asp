<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #include virtual="/strutt_testa2.asp"-->
<!--#include virtual ="/include/openconn.asp"-->
<!--#include virtual="/util/portallib.asp"-->
<!--#include virtual="/include/SelAreaTerrBandi.asp"-->
<%
if ValidateService(session("idutente"),"PPR_VISFASEBANDO", CC) <> "true" then 
	response.redirect "/util/error_login.asp"
end if
%>

<html>
<head>
<title>Gestione Progetti</title>
<link REL="STYLESHEET" TYPE="text/css" HREF="../../fogliostile.css">
<script language="Javascript">
<!--#include virtual = "/Include/help.inc"-->
<!--#include Virtual = "/Include/ControlString.inc"-->

function ControllaDati(myForm) 
{
	frmInsFaseB.txtCodSess.value=TRIM(frmInsFaseB.txtCodSess.value)
	if (frmInsFaseB.txtCodSess.value == "") 
	{
		alert ("Il campo Codice Sessione � obbligatorio")
		frmInsFaseB.txtCodSess.focus()
		return false
	}
	
	frmInsFaseB.txtDescr.value=TRIM(frmInsFaseB.txtDescr.value)
	if (frmInsFaseB.txtDescr.value == "") 
	{
		alert ("Il campo Descrizione � obbligatorio")
		frmInsFaseB.txtDescr.focus()
		return false
	}
	
	frmInsFaseB.txtProgr.value=TRIM(frmInsFaseB.txtProgr.value)
	if (frmInsFaseB.txtProgr.value == "") 
	{
		alert ("Il campo Progressivo Sessione � obbligatorio")
		frmInsFaseB.txtProgr.focus()
		return false
	}
	
	if (isNaN(frmInsFaseB.txtProgr.value))
	{
		alert("Il campo progressivo sessione deve essere numerico")
		frmInsFaseB.txtProgr.focus() 
		return false;
	}
	
	if ((frmInsFaseB.txtControllo.value == "SI") && (frmInsFaseB.txtProgr.value == "0") ){
	    alert ("la Sessione zero � gia' stata definita")
		frmInsFaseB.txtProgr.focus()
		return false
	}
	
	frmInsFaseB.txtOre.value=TRIM(frmInsFaseB.txtOre.value)
	if (frmInsFaseB.txtProgr.value == "0"){
	    if (frmInsFaseB.txtOre.value != "") 
	       {
		    alert ("Il campo Ore Sessione deve essere vuoto in caso di progressivo uguale a zero")
		    frmInsFaseB.txtOre.focus()
		    return false
	    }
    }
    else {
        if (frmInsFaseB.txtOre.value == "") 
	       {
		    alert ("Il campo Ore Sessione � obbligatorio")
		    frmInsFaseB.txtOre.focus()
		    return false
	    }
	
	    if (isNaN(frmInsFaseB.txtOre.value))
	       {
		    alert("Il campo ore sessione deve essere numerico")
		    frmInsFaseB.txtOre.focus() 
		    return false;
	    }
	    //inizio 21/10/03
	        if (frmInsFaseB.txtOre.value == 0) 
	           {
		        alert ("Il campo Ore Sessione deve essere maggiore di zero")
		        frmInsFaseB.txtOre.focus()
		        return false
	       }
	     //fine 21/10/2003
	    
    }

	return true

}

</script>
</head>

<%
dim nBando
dim sControllo

nBando = Request.Form ("hdnID")
descBando=decBando(nbando)
'Response.Write descbando
sSQL = "SELECT COD_SESSIONE FROM FASE_BANDO " &_
       "WHERE ID_BANDO =" & nBando &_
       " AND PRG_FASE = 0"
  
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
set rsFase = cc.execute(sSQL)
if not rsFase.eof then
    sControllo = "SI"
else
    sControllo = "NO"       
end if  
     
set rsFase = nothing       
%>

<body>
	<table border="0" width="520" cellspacing="0" cellpadding="0" height="81">
		   <tr>
		     <td width="500" background="<%=Session("Progetto")%>/images/titoli/strumenti2b.gif" height="81" valign="bottom" align="right">
		       <table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
		         <tr>
		           <td width="100%" valign="top" align="right"><b class="tbltext1a">Gestione Progetti</b></td>
		         </tr>
		       </table>
		     </td>
		   </tr>
	</table>
	<br>
	
	<table cellpadding="0" cellspacing="0" width="500" border="0">
		<tr height="18">
			<td class="sfondomenu" height="18" width="67%">
			<span class="tbltext0"><b>&nbsp;INSERIMENTO PERCORSO PROGETTUALE</b></span></td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
			<td valign="middle" align="right" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1">(*) campi obbligatori</td>
		</tr>
		<tr width="371" class="SFONDOCOMM">
			<td colspan="3">
				Inserimento delle Fasi attuative del Bando.
				<br>Per inserire una nuova Fase attuativa per 
				il bando selezionato compilare la scheda sottostante e premere <b>Invia</b>. 
				<a href="Javascript:Show_Help('/Pgm/help/Gestprogetti/alfabeta/PercProj/PPR_InsFaseBando')">
				<img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
			</td>
		</tr>
		<tr height="2">
			<td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>
	<br><br>

	<form name="FrmBack" action="PPR_VisFaseBando.asp" method="post">
		<input type="hidden" name="cmbBando" value="<%=nBando%>">
	</form>
	
	<form method="post" name="frmInsFaseB" onsubmit="return ControllaDati(this)" action="PPR_CnfFaseBando.asp">	
		<input type="hidden" name="txtControllo" value="<%=sControllo%>">
		<input type="hidden" name="txtAction" value="INS">
		<table border="0" cellpadding="0" cellspacing="1" width="500">
			<tr height="20">
				<td align="left" class="tbltext1" width="35%">
					<b>Bando</b>
				</td>		
				<td align="left" class="tbltext"> 
					<input type="hidden" name="txtIdBando" value="<%=nBando%>">					
					<b><%=descbando%></b>
				</td>
			</tr>
			<tr class="tbltext1">
				<td align="left">
					<b>Codice Sessione*</b>
				</td>
				<td>					
					<input type="text" name="txtCodSess" maxlength="3" size="12" align="left" class="textblack" style="TEXT-TRANSFORM:uppercase;">
				</td>
			</tr>
			<tr class="tbltext1">
				<td align="left">
					<b>Descrizione*</b>
				</td>
				<td>			
					<input type="text" name="txtDescr" maxlength="50" size="50" align="left" class="textblack" style="TEXT-TRANSFORM:uppercase;">			
				</td>
			</tr>
			<tr class="tbltext1">
				<td align="left">
					<b>Progressivo Sessione*</b>
				</td>
				<td>			
					<input type="text" name="txtProgr" maxlength="3" size="12" align="left" class="textblack" style="TEXT-TRANSFORM:uppercase;">			
				</td>
			</tr>
			<tr class="tbltext1">
				<td align="left">
					<b>Ore Sessione*</b>
				</td>
				<td>			
					<input type="text" name="txtOre" maxlength="4" size="12" align="left" class="textblack" style="TEXT-TRANSFORM:uppercase;">			
				</td>
			</tr>
			<tr class="tbltext1">
				<td align="left">
					<b>Tipologia Sessione*</b>
				</td>
				<td>			
					<select name="cmbTipoS" class="textblack" ID="cmbTipoS">
						<option value="S">Presenza</option>
						<option value="N">Distanza</option>
					</select>			
				</td>
			</tr>
		</table>
		<br>
	
		<table cellpadding="0" cellspacing="0" width="500" border="0">
			<tr align="center">
				<td nowrap><input type="image" name="Conferma" value="Registra" src="<%=Session("progetto")%>/images/conferma.gif"></td>
				<td nowrap><a href="javascript:FrmBack.submit()" onmouseover="javascript:window.status=' '; return true"><img src="<%=session("Progetto")%>/images/indietro.gif" border="0" name="imgPunto2"></a></td>
			</tr>
		</table>    
	</form>
	
<!--#include virtual ="/include/closeconn.asp"-->
<!--#include virtual = "/strutt_coda2.asp"-->
