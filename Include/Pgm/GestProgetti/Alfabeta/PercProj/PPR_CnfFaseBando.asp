<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #include virtual="/strutt_testa2.asp"-->
<!--#include virtual = "/util/dbutil.asp"-->
<!--#include virtual = "/include/ControlDateVB.asp"-->
<!--#include virtual = "/include/SysFunction.asp"-->
<!--#include virtual = "/include/openconn.asp"-->
<%
	dim nBando, sAction, nDomanda
	dim sSQL, rs

	nBando=Request.Form("txtIdBando")
	sAction=Request.Form("txtAction")
%>
	<form name="FrmBack" action="PPR_VisFaseBando.asp" method="post">
		<input type="hidden" name="cmbBando" value="<%=nBando%>">
	</form>
<% 
	sSQL = "Select count(*) Dom_XBando from DOMANDA_ISCR where id_bando=" & nBando
	
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	set rs = CC.Execute(sSQL)
	nDomanda = clng(rs("Dom_XBando"))
	rs.Close
	set rs =nothing
	
	if nDomanda > 0 then
		Inizio()
%>		
		<table border="0" cellspacing="1" cellpadding="1" width="500">
			<tr align="middle">
				<td class="tbltext3">
					Inserimento/Modifica non effettuabile.<br>
					Per il bando sono presenti domande di iscrizione.
				</td>
			</tr>
		</table>
		<br><br>
		
		<table border="0" cellpadding="0" cellspacing="1" width="500">
			<tr>
				<td align="middle" colspan="2"><b>
					<a HREF="javascript:FrmBack.submit()"><img SRC="<%=Session("Progetto")%>/images/indietro.gif" border="0">
				</td>
			</tr>
		</table>
<%
	else
		select case sAction
			case "INS"
				Inserisci()
			case "MOD"
				Modifica()	
		end select
	end if	

'-------------------'
Sub Inizio() %>
	<table border="0" width="520" cellspacing="0" cellpadding="0" height="81">
		<tr>
		  <td width="500" background="<%=Session("Progetto")%>/images/titoli/strumenti2b.gif" height="81" valign="bottom" align="right">
		    <table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
		      <tr>
		        <td width="100%" valign="top" align="right"><b class="tbltext1a">Gestione Progetti</b></td>
		      </tr>
		    </table>
		  </td>
		</tr>
	</table>
	<br>
	<table cellpadding="0" cellspacing="0" width="500" border="0">
		<tr height="18">
			<td class="sfondomenu" height="18" width="67%">
			<span class="tbltext0"><b>&nbsp;CONFERMA PERCORSO PROGETTUALE</b></span></td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
			<td valign="middle" align="right" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1"></td>
		</tr>
		<tr width="371" class="SFONDOCOMM">
			<td colspan="3">
				Conferma del percorso progettuale.
			</td>
		</tr>
		<tr height="2">
			<td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>
	<br><br>
<%	
End Sub

'-------------------'
Sub Inserisci()
	dim rsVer, nVerifica 
	dim sDescrFase, sSessione, sTFase
	dim nProgressivo, nOre

	sSessione		= server.HTMLEncode(UCASE(TRIM(Request.Form("txtCodSess"))))
	sSessione		= Replace(sSessione,"'","''")
	sDescrFase		= server.HTMLEncode(UCASE(TRIM(Request.Form("txtDescr"))))
	sDescrFase		= Replace(sDescrFase,"'","''")
	nProgressivo	= Request.Form("txtProgr")
	nOre			= Request.Form("txtOre")
	sTFase			= Request.Form("cmbTipoS")

	sSQL =	"SELECT COUNT(*) Verifica FROM FASE_BANDO" &_
			" WHERE ID_BANDO =" & nBando &_
			" AND COD_SESSIONE='" & sSessione & "'"

'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	set rsVer = CC.Execute(sSQL)
	nVerifica = clng(rsVer("Verifica"))
	rsVer.Close
	set rsVer=nothing	

	CC.BeginTrans
	Errore = "0"
		
	If nVerifica > 0 then
		Errore = "<BR>Codice sessione gi� presente per questo Bando."
	Else
		sSQL =	"INSERT INTO FASE_BANDO (ID_BANDO,COD_SESSIONE, DESC_FASE, PRG_FASE, COD_TFASE ,DT_TMST, NUM_ORESESS)" &_
				" VALUES(" & nBando & ",'" & sSessione & "','" & sDescrFase & "'," & nProgressivo &_
				",'" & sTFase & "'," & ConvDateToDB(now()) & "," & nOre & ")"  
				
		Errore=EseguiNoC(sSQL,CC)		
	End if 
	
	if Errore = "0" then
		CC.CommitTrans%>
		<script>
			alert("Inserimento correttamente eseguito");
			FrmBack.submit()
		</script>
	<%
	else
		CC.RollbackTrans
		Inizio()
	%>
		<table border="0" cellspacing="1" cellpadding="1" width="500">
			<tr align="middle">
				<td class="tbltext3">
					Inserimento non effettuabile.
				</td>
			</tr>
			<tr align="middle">
				<td class="tbltext3">
					<%Response.Write(Errore)%> 
				</td>
			</tr>
		</table>
		<br>
						
		<table border="0" cellpadding="0" cellspacing="1" width="500">
			<tr>
				<td align="middle" colspan="2"><b>
				 <a HREF="javascript:history.back()"><img SRC="<%=Session("Progetto")%>/images/indietro.gif" border="0">
				</td>
			</tr>
		</table>
	<%			
	end if
End Sub

'-------------------'
Sub Modifica()
	dim sSQL, rsVer 
	dim sSessione, sDescrFase, sTFase
	dim nProgressivo, nOre, nVerifica

	sSessione	 = Request.Form("hdnCodSess")
	sDescrFase	 = server.HTMLEncode(UCASE(TRIM(Request.Form("txtDescr"))))
	sDescrFase   = Replace(sDescrFase,"'","''")
	nProgressivo = Request.Form("txtProgr")
	nOre		 = Request.Form("txtOre")
	sTFase       = Request.Form("cmbTipoS")
	dTMST		 = Request.Form("txtTMST")

	sSQL =	"SELECT COUNT(*) Verifica FROM FASE_BANDO" &_
			" WHERE ID_BANDO =" & nBando &_
			" AND COD_SESSIONE='" & sSessione & "'"

'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	set rsVer = CC.Execute(sSQL)
	nVerifica = clng(rsVer("Verifica"))
	rsVer.Close
	set rsVer=nothing	
	
	Errore = "0"
	
	If nVerifica = 0 then
		Errore = "<BR>Il record che si vuole modificare non � stato trovato."
	Else
		sSQL =	"UPDATE FASE_BANDO SET" &_
				" DESC_FASE='" & sDescrFase & "',"		&_
				" PRG_FASE=" & nProgressivo & ","		&_
				" COD_TFASE='" & sTFase & "',"			&_
				" DT_TMST="	& ConvDateToDB(now()) & ","	&_
				" NUM_ORESESS=" & nOre 					&_
				" WHERE ID_BANDO=" & nBando				&_
				" AND COD_SESSIONE='" & sSessione & "'"

		chiave=""
		Errore= Esegui(chiave,"FASE_BANDO",session("idutente"),"MOD", sSQL, 0, dTMST)
	End If
	
	if Errore = "0" then
%>
		<script>
			alert("Modifica correttamente effettuata.");
			FrmBack.submit()
		</script>
<%		
	else
		Inizio()
	%>
		<table border="0" cellspacing="1" cellpadding="1" width="500">
			<tr align="middle">
				<td class="tbltext3">
					Modifica non effettuabile.
				</td>
			</tr>
			<tr align="middle">
				<td class="tbltext3">
					<%Response.Write(Errore)%> 
				</td>
			</tr>
		</table>
		<br>				
		<table border="0" cellpadding="0" cellspacing="1" width="500">
			<tr>
				<td align="middle" colspan="2"><b>
				 <a HREF="javascript:history.back()"><img SRC="<%=Session("Progetto")%>/images/indietro.gif" border="0">
				</td>
			</tr>
		</table>
<%
	End if
End Sub	

'-------------------'
%>
<!--#include virtual ="/include/CloseConn.asp"-->
<!--#include virtual = "/strutt_coda2.asp"-->
