<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include virtual="/util/portallib.asp"-->
<!--#include Virtual = "/strutt_testa1.asp"-->
<!--#include Virtual = "/include/ControlDateVB.asp"-->
<!--#include Virtual = "/include/DecComun.asp"-->
<!--#include Virtual = "/include/DecCod.asp"-->
<!--#include Virtual = "/include/ElabRegole.asp"-->
<!--#include Virtual = "/include/LeggiConf.asp"-->
<!--#include Virtual ="/Include/openconn.asp"-->
<!-- #include virtual="/include/SelezioneProvince.asp" -->
<!--#include File ="EST_GestioneFile.asp"-->

<script LANGUAGE="Javascript">
<!--#include virtual = "/Include/help.inc"-->

var windowQuadro

function Chiudi(){
	if (windowQuadro != null ){
		windowQuadro.close(); 
	}
	return true
}

function Prossimita(n,descr,idp){	
	document.frmProssimita.Denominazione.value = descr
	document.frmProssimita.Id_figprof.value = n
	document.frmProssimita.IdPers.value = idp
	document.frmProssimita.submit();
}

function Occupazione(pth){	
	windowOcc = window.open (pth,'StatoOccupazionale', 'width=560, height=420, scrollbars=yes,directories=0')	
}

function DatiPersonali(Rif,FlAna){	
	windowPersona = window.showModelessDialog('/pgm/richieste/RIC_DatiPersonali.asp?IdPers=' + Rif + '&FlAna=' + FlAna ,"InfoPersona","dialogWidth: 780px; dialogHeight: 500px; center: Yes; help: No; resizable: No; status: No");
}	 

function Validate(){
	var num = 0
			
	for (var i=0 ; i<document.frm1.length;i++){
		if(document.frm1.elements[i].checked){
					num = num + 1;
		}			
	}
	if (num == 0){
		alert("Indicare almeno una persona")
		return false;
	}
	Chiudi();
	return true;
}	

</script>
<a name="inizio"></a>
<%	
'********************************
'*********** MAIN ***************

	Dim sModo, nIdRic, sIdSede
	Dim nTipoRicerca, sTipoArea, sTipoProf, nPosti, sDtDal, sDtAl, sDtAvv, sDt_Tmst
	dim nIdProfilo,sPRVCPI
	
	sModo = Request("Modo")
	nIdRic= Request("IdRic")	
	sIdsede=Request("Idsede")
			
	Err.Clear

	Inizio()
	Imposta_Pag()
		
'**********************************************************************************************************************************************************************************	
	Sub Imposta_Pag()
		Dim sql, RR		'recordset delle persone che soddisfano i parametri
		Dim sql1, RR1	'recordset dei parametri di ricerca
		Dim sFromRicerca
		Dim sCondizione
		
		Err.number = 0
		
		sOutput		 = Request("chkStampa")
		sFromRicerca =  ""
		sCondizione  =  ""
		sGrado		 =  ""

	' Devo prima di tutto capire se � una azienda che ricerca personale 
	' oppure un utente che vuole controllare se qualche aziende cerca personale
	' del suo profilo.
		if Session("Creator") <> 0 then
			sql= "SELECT PRV FROM SEDE_IMPRESA SI, " &_
					"IMPRESA I WHERE I.ID_IMPRESA = SI.ID_IMPRESA AND SI.ID_SEDE=" &_
					Session("Creator") & "AND I.COD_TIMPR = '03'" 

			set RR = server.CreateObject("ADODB.recordset")
'PL-SQL * T-SQL  
'SQL = TransformPLSQLToTSQL (SQL) 
			RR.Open sql,CC,3
				
			if RR.eof then
				bCImpiego = false
				sPRVCPI = ""
			else
				sPRVCPI = RR.Fields("PRV")
				bCImpiego = true
			end if
			RR.close
		else
			bCImpiego = false
		end if
		
		sql = "select  FLOOR(MONTHS_BETWEEN(SYSDATE,A.DT_ULT_RIL)) As Rilascio, FLOOR(MONTHS_BETWEEN(SYSDATE,A.DT_NASC)/12) As Eta, a.id_persona, a.prv_res, a.stat_cit As nazionalita, a.com_res, " &_ 
			" c.cod_stdis  As categoria, c.ID_Cimpiego As Id_Impiego " &_
			 " , a.cod_fisc ,c.dt_tmst" &_
			" from PERSONA a, STATO_OCCUPAZIONALE c " & sFromRicerca &_
			" where a.id_persona = c.id_persona(+)" &_
			" And c.ind_status = '0'"
'			" And d.id_persona = a.id_persona "  & sCondizione &_
'			" And d.incrocio = 'S'" &_
'			" And d.Ind_status(+) = '0' " 
		
		if bCImpiego then
			
			sAbilitazione = FiltroCPI(false)
			select case sAbilitazione
				case "X","0" ' Vuol dire che pu� vedere solo gli utenti del proprio CPI
					sql = sql & " and c.id_cimpiego = " & clng(Session("Creator"))								
				
				case "1" ' Tutto il territorio

				case "2" ' Tutta la provincia di appartenenza
					sNomeFile = SERVER.MapPath("\") & "\" & mid(session("progetto"),2) & "\Conf\" &_
							mid(session("progetto"),2) & ".txt"
		
					sCondizione = mid(Legge(sNomeFile),4)

					sql = sql & " and exists (select id_sede " &_
						" from sede_impresa " &_
						" where id_sede = c.id_cimpiego " &_
						" and prv in(" & sCondizione & ")) "								
	
				case "3" ' Tutta la regione di appartenenza
					sProvReg =SetProvince(3)
					
					sCondizionePrv = "PRV IN (" & sProvReg & ")"
					
					sql = sql & " and exists (select id_sede " &_
						" from sede_impresa " &_
						" where id_sede = c.id_cimpiego " &_
						" and " & sCondizionePrv & " )" 								

				case else
					sql = sql & " and c.id_cimpiego = " & clng(Session("Creator"))								
			end select 
		end if		
		'Funzione che costruisce la regola.
		sql = sql & CreaCriteriDaFile(CC)		

		sql = sql & " and exists(select id_persona from dichiaraz " &_
			" where id_persona = a.id_persona and  aut_incrocio = 'S')"
			'" order by " & sGrado & " DESC"

		if sOutput = "N" then
			Testata()
			StampaVideo sql,bCImpiego
		else
			StampaFile sql,bCImpiego
		end if
	%>
		<br><br>
		<table width="743" border="0" cellpadding="0" cellspacing="0" align="center">
			<tr ALIGN="CENTER"> 
				</form>
				<form method="post" name="FrmAR" action="EST_SelFiltri.asp" onsubmit="javascript:Chiudi()">
				<input type="hidden" name="IdRic" value="<%=nIDRic%>">
				<input type="hidden" name="dtTmst" value="<%=sDt_Tmst%>">
				<input type="hidden" name="Idsede" value="<%=sIdsede%>">						
				<td>
					<input type="image" src="<%=Session("progetto")%>/images/Indietro.gif" title="Pagina precedente" border="0" align="absBottom" id="image1" name="image1">
				</td>
				</form>
			</tr>
		</table>
		<form name="frmQuadro" action="/pgm/BilancioCompetenze/Quadro/Default.asp" method="post" target="_new">
			<input type="hidden" name="IdPers" value>
			<input type="hidden" name="Vismenu" value="NO">
			<input type="hidden" name="VisAnag" value="NO">
		</form>
		<form name="frmProssimita" action="/pgm/BilancioCompetenze/Bilancio_Pross/reportfigprof.asp" method="post" target="_new">
			<input type="hidden" name="Id_figprof" value>
			<input type="hidden" name="IdPers" value>
			<input type="hidden" name="Denominazione" value>
			<input type="hidden" name="Vismenu" value="NO">
			<input type="hidden" name="VisAnag" value="NO">
		</form>
	<%	
		End Sub 
	'**********************************************************************************************************************************************************************************	
		Sub Indietro()
	%>
		<form method="post" name="FrmAR" action="EST_SelFiltri.asp" onsubmit="javascript:Chiudi()">
			<input type="hidden" name="IdRic" value="<%=nIDRic%>">
			<input type="hidden" name="Idsede" value="<%=sIdsede%>">				
			<table WIDTH="743" BORDER="0" CELLPADDING="0" CELLSPACING="0" align="center">
				<tr align="center">
					<td>
						<input type="image" src="<%=Session("progetto")%>/images/Indietro.gif" title="Pagina precedente" border="0" align="absBottom" id="image1" name="image1">
					</td>
				</tr>
			</table>
		</form>
<%	End Sub 
'**********************************************************************************************************************************************************************************	
	Sub Inizio()
%>
	<table border="0" width="743" cellspacing="0" cellpadding="0" height="70" align="center">
		<tr>
			<td width="720" background="<%=session("progetto")%>/images/titoli/servizi1g.gif" height="70" valign="bottom" align="right">
				<table border="0" background width="720" height="23" cellspacing="0" cellpadding="0">
					<tr>
						<td width="100%" valign="top" align="right"><b CLASS="tbltext1a">Interrogazioni
						  </b></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<br>
<%
	End Sub
'**********************************************************************************************************************************************************************************	
	Sub Testata()
%>	
	<table border="0" CELLPADDING="0" CELLSPACING="0" width="743" align="center">
		<tr height="18">
			<td class="sfondomenu" width="67%" height="18"><span class="tbltext0"><b>&nbsp;RISULTATO INTERROGAZIONE </b></span></td>
			<td width="2%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
			<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
		</tr>
	</table>
	<table border="0" CELLPADDING="0" CELLSPACING="0" width="743" align="center">
		<tr>
			<td align="left" class="sfondocommaz">
			Le persone presenti nell'elenco soddisfano i criteri di ricerca scelti. 
			<a href="Javascript:Show_Help('/Pgm/help/Monitor/Report/Estrattore/EST_AvvioRicerca')"><img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
			</td>
		</tr>
		<tr height="2">
			<td class="sfondocommaz" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>
	<br>
<% 	
	End Sub
'**********************************************************************************************************************************************************************************
	Sub Messaggio(Testo)
%>		
		<br><br>
		<table width="743" align="center" border="0" cellspacing="2" cellpadding="1">
			<tr height="20"> 
			  	<td class="tbltext3" align="middle" width="743">
					<b><%=Testo%></b>
				</td>
			</tr>
		</table>
		<br><br>
<%				
	End Sub

' ************************************************************************************************
	sub StampaVideo(sql,bCImpiego)
		
		set RR = server.CreateObject("ADODB.recordset")

'PL-SQL * T-SQL  
'SQL = TransformPLSQLToTSQL (SQL) 
		RR.Open sql,CC,3
		sErrore = 0
		if sErrore <> "0" then
			Messaggio(" Si � verificato un errore nell'elaborazione." & sErrore)
			Indietro()
			Err.number = 0
			exit sub
		end if
				
		if RR.EOF then
			Messaggio(" Nessuna persona presenta le caratteristiche richieste.")
			Err.number = 0
			exit sub
		end if 
          if not bCImpiego then%>	
			<table WIDTH="743" BORDER="0" CELLPADDING="2" CELLSPACING="2" align="center">
				<tr align="left" height="15px">
			       <td valign="top" rowspan="4" class="tbltext1" WIDTH="10">
						<b>N.B.:</b>
					</td>
					<td class="tbltext1">
						<b>Ogni richiesta prevede la visualizzazione delle
						 prime 40 persone che soddisfano con maggiore prossimit� </b>					
					</td>
				</tr>
				<tr height="15">
					<td class="tbltext1"><b>
						i criteri selezionati. </b>
					</td>
				</tr>
				<tr height="15">
					<td class="tbltext1"><b>
						L'eventuale riferimento <span class="textred">rosso</span>
						 � relativo ai candidati che non hanno aggiornato i 
						propri dati negli ultimi 3 mesi. </b>
					</td>
				</tr>
				<tr height="15">					
					<td class="tbltext1"><b>Premendo sul riferimento del candidato 
					� possibile accedere ai suoi dati personali.
					</td>
				</tr>
				
			</table>
		<%else%>
			<table WIDTH="743" CELLPADDING="2" CELLSPACING="2" align="center" border="0">
				<tr align="left" height="15px">
			       <td valign="top" rowspan="5" class="tbltext1" WIDTH="10">
						<b>N.B.:</b>
					</td>
					<td class="tbltext1">
						<b>
						Ogni richiesta prevede la visualizzazione delle prime 40 persone che soddisfano con maggiore 
						prossimit� </b>					
					</td>
				</tr>
				<tr height="15">
					<td class="tbltext1"><b>
						i criteri selezionati. </b>
					</td>
				</tr>
				<tr height="15">
					<td class="tbltext1"><b>
						L'eventuale riferimento <span class="textred">rosso</span>
						 � relativo ai candidati che non hanno aggiornato i 
						dati negli ultimi 3 mesi. </b>
					</td>
				</tr>
				<tr height="15">					
					<td class="tbltext1"><b>Premendo sul riferimento del candidato 
					� possibile accedere ai suoi dati personali.
					</td>
				</tr>
			</table>
		<%end if%>	
		<form method="post" name="frm1" action="EST_AvvioRicerca.asp" onsubmit="javascript:return Validate()">
		<input type="hidden" name="Modo" value="1">
		<input type="hidden" name="IdRic" value="<%=nIdRic%>">
		<input type="hidden" name="Idsede" value="<%=sIdsede%>">		
		<table width="743" border="0" cellpadding="1" cellspacing="2" align="center">
			<tr height="23" class="sfondocommaz"> 
				<td align="center" width="115">
					<b class="tbltext1">Rif.</b>
				</td>
				<td align="center" width="20">
					<b class="tbltext1">Et�</b>
				</td>
				<td align="center" width="140"> 
					<b class="tbltext1">Titolo di studio</b>
				</td>
				<td align="center" width="80"> 
					<b class="tbltext1">Residenza</b>
				</td>
				<td align="center" width="114"> 
					<b class="tbltext1">Nazione</b>
				</td>
				<td align="center" width="180"> 
					<b class="tbltext1">Status</b>
				</td>
		<!--		<td align="center" width="70"> 					<b class="tbltext1">Prossimit�</b>				</td>		-->	</tr>	
<% 		n = 1 ' contatore: ogni 20 richieste si mette un link all'inizio della pag.
		p = 1 ' contatore: vengono visualizzate solo le prime 40 persone per prossimit� ai criteri di ricerca
		
		Dim RRT, sqlT 'recordset per titolo di studio
		
		do until RR.Eof or p > 40
			sqlT = "Select cod_liv_stud As titolo From TISTUD " &_ 
					" where id_persona  =" & RR.fields("id_persona") &_ 
					" And cod_stat_stud = 0 " &_
					" Order by AA_stud desc, dt_tmst desc"
			
'PL-SQL * T-SQL  
'SQLT = TransformPLSQLToTSQL (SQLT) 
			Set RRT = CC.Execute(sqlT)
			
			if RRT.Eof then
				sTitolo = "-"
			else
				RRT.Movefirst
				sTitolo = RRT.fields("titolo")
			end if
			
			RRT.Close
			set RRT = nothing
			sqlT = ""
			
			if n = 21 then
				n = 1
				Response.Write	"<TR> " &_
				  "<TD align=right colspan=7> " &_
	    		     "<a href='#inizio' title='Inizio pagina'><IMG align=right src='" & Session("Progetto") & "/images/sopra.gif'  border=0></a>" &_	
				  "</td>" &_
			   "</tr>"
			end if
%>				
			<tr height="23" class="tblsfondo">
			
				<td align="center" Valign="middle" width="115">
					<table border="0" width="115">
						<tr>
						<%	bBlanck = True

							if not isnull(RR.Fields("Id_Impiego")) then
								sSQLEmail = "SELECT E_MAIL_SEDE FROM SEDE_IMPRESA WHERE ID_SEDE = " &_
									 clng(RR.Fields("Id_Impiego"))

'PL-SQL * T-SQL  
'SSQLEMAIL = TransformPLSQLToTSQL (SSQLEMAIL) 
								set rsEMAIL = CC.Execute(sSQLEmail) 
									
								if not isnull(rsEMAIL) then
									sSEDEEmail = rsEMAIL(0)
								else
									sSEDEEmail = ""
								end if
								rsEMAIL.Close
								set rsEMAIL = nothing
						%>
								<input type="hidden" name="IdCPI<%=RR.fields("Id_persona")%>" value="<%=clng(RR.Fields("Id_Impiego"))%>">
								<input type="hidden" name="CPIEmail<%=RR.fields("Id_persona")%>" value="<%=sSEDEEmail%>">
							<%end if

							if bCImpiego then
								 if not isnull(RR.Fields("Id_Impiego")) then
									if clng(RR.Fields("Id_Impiego")) = clng(Session("Creator")) then
										bBlanck = false
									end if
									ANAFla = CheckProvinciale(clng(RR.Fields("Id_Impiego")),sPRVCPI)
								else
									ANAFla = 0
								end if
							else
									ANAFla = 0
							end if%>
							<td width="50">&nbsp;</td>					
							
							<%if clng(RR.Fields("Rilascio")) > 1 then%>
						
								<td width="65"><b><a class="textred" onmouseover="javascript: window.status=' '; return true" href="javascript:DatiPersonali(<%=clng(RR.fields("id_persona"))%>,<%=ANAFla%>)" onmouseover="javascript: window.status=' '; 
									return true"><%=clng(RR.fields("Id_persona"))%></a>
									</td>
							<% else%>
								<td width="65"><b><a class="tbltext1" onmouseover="javascript: window.status=' '; return true" href="javascript:DatiPersonali(<%=clng(RR.fields("id_persona"))%>,<%=ANAFla%>)" onmouseover="javascript: window.status=' '; 
									return true"><%=clng(RR.fields("Id_persona"))%></a>
									</td>
							<%end if%>
						</tr>
					</table>
				</td>
				<td align="left" class="textblack">
<%						Response.Write "&nbsp;" & RR.Fields("Eta") 
%>				</td>
				<td align="left" class="textblack">
					&nbsp;
<%					if sTitolo <> "-" then
						sTitolo = lcase(DecCodVal("LSTUD",0,"",sTitolo,""))
					end if
					Response.Write sTitolo
%>
				</td>
				<td align="left" class="textblack">
					&nbsp;
					
					<%  
						sComune = DescrComune(RR.fields("com_res"))
						if  sComune <> "0" then
							Response.Write sComune & " (" & RR.fields("prv_res") & ")"
						else
							Response.Write "-"
						end if	
					%> 
				</td>
				<td align="left" class="textblack">
					&nbsp;<%=LEFT(DecCodVal("STATO",0,"",RR.fields("nazionalita"),""),15)%>
				</td>
				<td align="left" class="textblack">
					&nbsp;
				<%	if RR.fields("categoria") <> "" then
						Response.Write "<a class=tbltext1 onmouseover=""javascript:window.status= ' '; return true"" href=""javascript:Occupazione('/Pgm/Richieste/RIC_VisOccupazione.asp?id=" & RR.fields("id_persona") & "')""><b>" & lcase(DecCodVal("STDIS",0,RR.fields("dt_TMST"),RR.fields("categoria"),"")) & "</b></a>"
					else
						Response.Write "-"
					end if
				%> 
				</td>
			</tr>	
<%			RR.Movenext
			n = n + 1
			p = p + 1
		loop			
		RR.Close
		set RR = nothing
%>				
		</table>
<%	end sub
		
' ************************************************************************************************
	sub StampaFile(sql,bCImpiego)
	    
	    sFileOutPut =  Session("progetto") & "\DocPers\Interrogazioni\" & session("LOGIN") & ".xls"
	    
        set RR = server.CreateObject("ADODB.recordset")
'PL-SQL * T-SQL  
'SQL = TransformPLSQLToTSQL (SQL) 
		RR.Open sql,CC,3
		sErrore = 0
		if sErrore <> "0" then
			Messaggio(" Si � verificato un errore nell'elaborazione." & sErrore)
			Indietro()
			Err.number = 0
			exit sub
		end if
				
		if RR.EOF then
			Messaggio(" Nessuna persona presenta le caratteristiche richieste.")
			Err.number = 0
			exit sub
		end if 

		set oFileSystemObject = Server.CreateObject("Scripting.FileSystemObject")

		if oFileSystemObject.FileExists(SERVER.MapPath("\") & sFileOutPut) then
			set oFileCurriculum = oFileSystemObject.OpenTextFile(SERVER.MapPath("\") & sFileOutPut,2)
		else
			set oFileCurriculum = oFileSystemObject.CreateTextFile(SERVER.MapPath("\") & sFileOutPut,true)
		end if				

		oFileCurriculum.WriteLine "Elenco delle persone selezionate."
		oFileCurriculum.WriteLine "Elaborato in data : " & ConvDateTimeToString(Now)

		ElencoCriteri(oFileCurriculum)
		
		oFileCurriculum.WriteLine ""

		oFileCurriculum.WriteLine "Rif." & chr(9) & "Codice Fiscale" & chr(9) &_
					"Et�" & chr(9) & _
					"Titolo di studio" &  chr(9) & _
					"Residenza" & chr(9) & _ 
					"Nazione" & chr(9) & _  
					"Status" & chr(9)' & _
					'"Prossimit�" 
	        				
		oFileCurriculum.WriteLine("")
%>	
		<form method="post" name="frm1" action="EST_AvvioRicerca.asp" onsubmit="javascript:return Validate()">

<% 		n = 1 ' contatore: ogni 20 richieste si mette un link all'inizio della pag.
		p = 1 ' contatore: vengono visualizzate solo le prime 40 persone per prossimit� ai criteri di ricerca
		
		Dim RRT, sqlT 'recordset per titolo di studio	
		
		do until RR.Eof 'or p > 40
			sqlT = "Select cod_liv_stud As titolo From TISTUD " &_ 
					" where id_persona  =" & RR.fields("id_persona") &_ 
					" And cod_stat_stud = 0 " &_
					" Order by AA_stud desc, dt_tmst desc"
			
'PL-SQL * T-SQL  
'SQLT = TransformPLSQLToTSQL (SQLT) 
			Set RRT = CC.Execute(sqlT)
			
			if RRT.Eof then
				sTitolo = "-"
			else
				RRT.Movefirst
				sTitolo = RRT.fields("titolo")
			end if
			
			RRT.Close
			set RRT = nothing
			sqlT = ""
			
			if n = 21 then
				n = 1
			end if

			sComune = DescrComune(RR.fields("com_res"))
			if  sComune <> "0" then
				sDescComune =  sComune & " (" & RR.fields("prv_res") & ")"
			else
				sDescComune = "-"
			end if	

			'if RR.Fields("GRADO") > "0" then	
			'	sGrado = RR.Fields("GRADO")
			'else
			'	sGrado = "-" 
			'end if

			if sTitolo <> "-" then
				sTitolo = lcase(DecCodVal("TSTUD",0,"",sTitolo,""))
			else
				sTitolo = "-"
			end if

			bBlanck = True
			
			if bCImpiego then
				 if not isnull(RR.Fields("Id_Impiego")) then
					if clng(RR.Fields("Id_Impiego")) = clng(Session("Creator")) then
						bBlanck = false
					end if
					ANAFla = CheckProvinciale(clng(RR.Fields("Id_Impiego")),sPRVCPI)
				else
					ANAFla = 0
				end if
			end if

			if bBlanck then
				sCodFisc = "****************"
			else
				sCodFisc = RR.Fields("cod_fisc")
			end if

			oFileCurriculum.WriteLine  RR.fields("Id_persona") & chr(9) &_
				sCodFisc & chr(9) &_
				RR.Fields("Eta") & chr(9) &_
				sTitolo & chr(9) & sDescComune & chr(9) &_
				LEFT(DecCodVal("STATO",0,"",RR.fields("nazionalita"),""),15) & chr(9) &_
				lcase(DecCodVal("STDIS",0,"",RR.fields("categoria"),"")) &_ 
				 chr(9) & sGrado

			RR.Movenext
			n = n + 1
			p = p + 1
		loop

		oFileCurriculum.WriteLine ""
		oFileCurriculum.WriteLine "NB: Se � visionabile il codice fiscale " &_
			"della persona questa appartiene al proprio Centro Di Impiego."
			
		RR.Close
		set RR = nothing

		oFileCurriculum.close
		set oFileSystemObject = nothing
		set oFileCurriculum = nothing
%>				
		<script>
			function Scarica(report){
				f = report;
				fin=window.open(f,"","toolbar=0;location=0,directories=0,status=0,menubar=1,scrollbars=1,resizable=1,copyhistory=0,width=640,height=480,screenX=200,screenY200");
			}	
		</script>

		<table align="center" width="730" border="0">
			<tr align="center">
				<td class="tbltext3">Il report � stato creato correttamente.</td>
			</tr>
			<tr align="center">
				<td class="tbltext3">&nbsp;</td>
			</tr>
			<tr align="center">
				<td>
				  <b>
		          <a onmouseover="javascript: window.status=' '; return true" class="textRed" href="javascript:Scarica('<%=replace(sFileOutPut,"\","\\")%>')">Apri il Report</a>&nbsp; 
				  </b>
				</td>
			</tr>
		</table>
<%	end sub	

' ************************************************************************************************
	sub ElencoCriteri(oFileCurriculum)

		aFile = OrderFile("")

		if IsArray(aFile) then
			z = 0
			aOpLog = decodTadesToArray("OPLOG",date,"",1,0)

			oFileCurriculum.WriteLine ("Elenco dei criteri indicati.")
            contrDesc = 0
			for xx = 0 to UBound(aFile)
				aRiga = Split(aFile(xx),"#")
				if 	aRiga(0) <> memId then
					sDescrTab= DecodIDTab(aRiga(0))
					memId = aRiga(0)
'***********************************
                    if sDescrTab <> "AREE PROF. DELLA PERSONA" then
				          contrDesc = 0
				    end if
					if contrDesc = 0 then
'***********************************					
					    oFileCurriculum.WriteLine ("") 
					    oFileCurriculum.WriteLine sDescrTab
					
'***********************************
                        if sDescrTab = "AREE PROF. DELLA PERSONA" then
				            contrDesc = 1
				        end if
				    end if    
'***********************************					
				end if
				sSQL = "SELECT ID_DESC,ID_CAMPO,DESC_CAMPO,ID_CAMPO_RIF, ID_TAB_RIF,TIPO_CAMPO,ID_CAMPO_DESC " &_ 
						" FROM DIZ_DATI " &_ 
						" WHERE ID_TAB='" & aRiga(0) & "'" &_ 
						" AND ID_CAMPO='" & aRiga(1) & "'" 
						
'PL-SQL * T-SQL  
'SSQL = TransformPLSQLToTSQL (SSQL) 
				set rsDecod = CC.Execute(sSQL)
				sDescrizioneCampo = ucase(rsDecod("DESC_CAMPO"))
				sDecNomTab		= rsDecod("ID_TAB_RIF")
				sTipoCampo		= rsDecod("Tipo_campo")
				sDescNomeCampo  = rsDecod("ID_CAMPO_DESC")
				sDenominazione  = rsDecod("ID_DESC")
				if isnull(rsDecod("ID_CAMPO_rif")) then
					sDecNomCampo	= rsDecod("ID_CAMPO")
				else
					sDecNomCampo	= rsDecod("ID_CAMPO_RIF")
				end if
				rsDecod.Close
				set rsDecod = Nothing
				if sDecNomTab <> "" then
					select case mid(sDecNomTab,1,4)
						case "TAB_"
							aValore = Split(aRiga(3),",",-1,1)
							lung = Ubound(aValore)
							sTab = ""
							for i = 0 to lung 
								sTab = sTab & ucase(DecCodVal(mid(sDecNomTab,5), 0, date, replace(aValore(i),"$",""),0)) & ","
							next
							sValorreDecodificato = left(sTab,len(sTab) - 1)
						case "COMU"
							aValore = Split(aRiga(3),",",-1,1)
							lung = Ubound(aValore)

							sCom=""
							if lung > 0 then 
								for i = 0 to lung 
									sSQL = "SELECT DESCOM FROM " & sDecNomTab & " WHERE " &_
									sDecNomCampo & " = " & Replace(aValore(i),"$","'")
'PL-SQL * T-SQL  
'SSQL = TransformPLSQLToTSQL (SSQL) 
									set rsDecod = CC.Execute(sSQL)
									sCom = sCom  &  ucase(rsDecod("DESCOM")) & ","
									rsDecod.Close
									set rsDecod = Nothing
								next
								sValorreDecodificato = left(sCom,len(sCom) - 1)
							else
								sSQL = "SELECT DESCOM FROM " & sDecNomTab & " WHERE " &_
								sDecNomCampo & " ='" & aValore(0) & "'"
'PL-SQL * T-SQL  
'SSQL = TransformPLSQLToTSQL (SSQL) 
								set rsDecod = CC.Execute(sSQL)
								sValorreDecodificato = ucase(rsDecod("DESCOM")) 
								rsDecod.Close
								set rsDecod = Nothing
							end if
						case else
							aValore = Split(aRiga(3),",",-1,1)
							lung = Ubound(aValore)
							Denom = ""
							for i = 0 to lung 
								sSQL = "SELECT " & sDenominazione & " FROM " & sDecNomTab & " WHERE " &_
								sDecNomCampo & " = " & Trasforma(replace(aValore(i),"$",""),sTipoCampo,aRiga(5))
										
'PL-SQL * T-SQL  
'SSQL = TransformPLSQLToTSQL (SSQL) 
								set rsDecod = CC.Execute(sSQL)
								Denom = Denom & ucase(rsDecod(0)) & ","
								rsDecod.Close
								set rsDecod = Nothing
							next
							sValorreDecodificato = left(Denom,len(Denom) - 1)
					end select
				else
					if sDescNomeCampo <> "" then
						aVal = split(sDescNomeCampo,"|")
						for i=0 to ubound(aVal) step 2
							if aVal(i) = replace(aRiga(3),"$","") then
								sValorreDecodificato = server.HTMLEncode(UCASE(aVal(i+1)))
							end if
						next
					else
						sValorreDecodificato = server.HTMLEncode(replace(aRiga(3),"$",""))
					end if
				end if
				z=z+1

				oFileCurriculum.WriteLine sDescrizioneCampo & chr(9) &_
					UCASE(searchIntoArray(aRiga(2),aOpLog,0)) & chr(9) & sValorreDecodificato
			next
			Erase aOpLog
		end if
	end sub
%>
<!--#include Virtual ="/Include/closeconn.asp"-->
<!--#include Virtual="/strutt_coda1.asp"-->	
