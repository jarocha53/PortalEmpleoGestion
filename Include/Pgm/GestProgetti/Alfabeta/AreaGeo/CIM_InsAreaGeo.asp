<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include virtual="/util/portallib.asp"-->
<%
if ValidateService(session("idutente"),"CIM_VISAREAGEO", CC) <> "true" then 
	response.redirect "/util/error_login.asp"
end if
%>
<html>
<head>
<!--#include file = "../../include/DecCod.asp"-->
<!--#include file = "../../include/ControlDateVB.asp"-->
<meta name="GENERATOR" content="Microsoft FrontPage Express 2.0">
<LINK REL=STYLESHEET TYPE="text/css" HREF="../../fogliostile.css">
<title>Inserimento Centro di Impiego</title>
</head>
<SCRIPT LANGUAGE="Javascript">
//include del file per fare i controlli sulla validitÓ delle date
<!--#include file = "../../Include/ControlDate.inc"-->
//include del file per fare i controlli sulla numericitÓ dei campi
<!--#include file = "../../Include/ControlNum.inc"-->

//Funzione per i controlli dei campi da inserire 
	function ControllaDati(frmInsCentro){
	//-------- Controlli di OBBLIGATORIETA' --------
	//-------- Controlli FORMALI            --------
	//-------- Controlli di RELAZIONE       --------
		//Descrizione del Centro di Impiego obbligatoria
		if ((frmInsCentro.txtDescCentro.value == "")||
			(frmInsCentro.txtDescCentro.value == " ")){
			alert("Descrizione del Centro di Impiego obbligatoria")
			frmInsCentro.txtDescCentro.focus() 
			return false
		}
		//Comune del Centro di Impiego obbligatorio
		if (frmInsCentro.cmbComune.value == ""){
			alert("Comune del Centro di Impiego obbligatorio")
			frmInsCentro.cmbComune.focus() 
			return false
		}
		//Numero Posti numerico
		if (!IsNum(frmInsCentro.txtNumPosti.value)){
			alert("Il Numero dei Posti essere Numerico")
			frmInsCentro.txtNumPosti.focus() 
			return false
		}
	return true
	}
	
</SCRIPT>
<body background="../../images/sfondo1.gif" >

<%
dim sProv

sProv = UCase(Request("Prov"))

set CnConn=Server.CreateObject("ADODB.Connection")
set rsProv = Server.CreateObject("ADODB.Recordset")
										
CnConn.Open Application("connessione")
sSQL = "SELECT Nome_Tabella,Codice,Descrizione from TADES WHERE Nome_Tabella = 'PROV' AND Codice = '" & sProv & "' ORDER BY Descrizione"
			
rsProv.Open sSQL, CnConn	
		
sDescProv = rsProv("Descrizione")
%>
<form method="post" name="frmInsCentro" target=principale onsubmit="return ControllaDati(this)" action='CIM_CnfCentri.asp?Prov=<%=sProv%>&MOD=1'>

<table width=570 border=0 cellspacing=2 cellpadding=1>
	<tr> 
		<td colspan=2 align=middle background=../../images/sfmnsmall.jpg>
		<b>
		<span class="titolo">
			Inserimento Centro di Impiego per la Provincia di <%=sDescProv%>.
		</span>
		</b>
		</td>
	</tr>
</table>

<table>
    <tr>
        <td align="left" colspan="2">&nbsp;</td>
        <td align="left" colspan="2" width="60%">&nbsp;</td>
    </tr>
</table>   

<table border="0" cellpadding="0" cellspacing="0" width="570" class="tbllabel1">
    <tr>
		<td valign="top" width="23">
			<IMG height=26 src="../../images/angolosin.jpg" width=23></td>
        <td align=left>
        	<STRONG><span class="tbllabel1">DATI CENTRO DI IMPIEGO</span></STRONG>
		</td>
        <td align="right" colspan="2" width="50%">
			<i><span class="tbllabel1">(* = dati obbligatori)</span></i>
		</td>
    </tr>
</table>

<table border="0" cellpadding="0" cellspacing="1" width="570" style="WIDTH: 570px">
	<tr>
		<td align="middle" colspan="2" nowrap class="tbllabel1">
			<p align="left">
			<span class="tbltext1">
					<strong>_</strong>
				</span>
				<strong>Descrizione*</strong>
				<span class="tbltext1">
					<strong>_</strong>
				</span>
			</p>
		</td>
		<td align="left" colspan="2" width="80%" class="tblsfondo">
			<p align="left">
				<input style="TEXT-TRANSFORM: uppercase; WIDTH: 440px;
					 HEIGHT: 22px" size="30" maxlength="50" name='txtDescCentro'>
			</p>
		</td>
    </tr>
    <tr>
		<td align="middle" colspan="2" nowrap class="tbllabel1">
			<p align="left">
			<span class="tbltext1">
					<strong>_</strong>
				</span>
				<strong>Comune*</strong>
				<span class="tbltext1">
					<strong>_</strong>
				</span>
			</p>
		</td>
		<td align="left" colspan="2" width="80%" class="tblsfondo">
			<p align="left">
				<%
				set CnConn=Server.CreateObject("ADODB.Connection")
				set rsComune = Server.CreateObject("ADODB.Recordset")
											
				CnConn.Open Application("connessione")
				sSQL = "SELECT CODCOM,DESCOM,PRV from COMUNE WHERE PRV = '" & sProv &"' ORDER BY DESCOM"

				rsComune.Open sSQL, CnConn	
			
				Response.Write "<SELECT ID=cmbComune name=cmbComune><OPTION value=></OPTION>"

				do while not rsComune.EOF 
					Response.Write "<OPTION "

					Response.write "value ='" & rsComune("CODCOM") & _
						"'> " & rsComune("DESCOM")  & "</OPTION>"
						rsComune.MoveNext 
				loop
	
				Response.Write "</SELECT>"

				rsComune.Close	
				CnConn.Close
			%>
			</p>
		</td>
    </tr>
    <tr>
		<td align="middle" colspan="2" nowrap class="tbllabel1">
			<p align="left">
			<span class="tbltext1">
					<strong>_</strong>
				</span>
				<strong>Numero Posti</strong>
				<span class="tbltext1">
					<strong>_</strong>
				</span>
			</p>
		</td>
		<td align="left" colspan="2" width="80%" class="tblsfondo">
			<p align="left">
				<input style="TEXT-TRANSFORM: uppercase; WIDTH: 80px;
					 HEIGHT: 22px" size="30" maxlength="10" name='txtNumPosti'>
			</p>
		</td>
    </tr>
</table>

<table>
	 <tr>
        <td align="middle" colspan="2">&nbsp;</td>
        <td align="middle" colspan="2" width="60%">&nbsp;</td>
    </tr>
</table>

<table>
	<tr>
        <td align="left" colspan="2">&nbsp;</td>
        <td align="center" colspan="2" width="60%">
			<p align="center">
				<input type='submit' name='Invia' value='Conferma'>
					&nbsp;
				<input type="reset" name="Annulla" value="Annulla">
			</p>
        </td>
    </tr>
</table>
<!--#include file = "../../include/Fine.asp"-->
</form>
</body>
</html>
