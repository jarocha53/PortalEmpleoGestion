<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include virtual = "/strutt_testa2.asp"-->
<!--#include virtual ="/include/openconn.asp"-->
<!--#include virtual = "util/portallib.asp"-->
<!--#include virtual = "util/dbutil.asp"-->
<!--#include virtual = "include/DecCod.asp"-->
<!--#include virtual = "include/SelAreaTerrBandi.asp"-->
<%
if ValidateService(session("idutente"),"DTC_VISDISTTERR", CC) <> "true" then 
	response.redirect "/util/error_login.asp"
end if
%>

<script LANGUAGE="Javascript">
<!--#include Virtual = "/Include/help.inc"-->

function controllo()
{
	if (document.frmVisAreaGeoCombo.CmbDescBando.value=="")
	{
		alert('Devi selezionare un Bando.')
		document.frmVisAreaGeoCombo.CmbDescBando.focus();
		return false;
	}
	
	if (document.frmVisAreaGeoCombo.cmbSituazione.value=="")
	{
		alert('Devi selezionare una situazione.')
		document.frmVisAreaGeoCombo.cmbSituazione.focus();
		return false;
	}
}
</script>
<%

'-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

sub Inizio()
%>
<table border="0" width="520" cellspacing="0" cellpadding="0" height="81">
	<tr>
		<td width="500" background="<%=Session("Progetto")%>/images/titoli/strumenti2b.gif" height="81" valign="bottom" align="right">
			<table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
				<tr>
					<td width="100%" valign="top" align="right"><b class="tbltext1a">Gestione Progetti</span></b></td>
				</tr>
			</table>
	   </td>
	</tr>
</table>
<br>
<table cellpadding="0" cellspacing="0" width="500" border="0">
	<tr height="18">
		<td class="sfondomenu" height="18" width="67%">
			<span class="tbltext0"><b>&nbsp;DISTRIBUZIONE TERRITORIALE AMMESSI</b></span></td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
		<td valign="middle" align="right" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1"></td>
	</tr>
	<tr width="371" class="SFONDOCOMM">
		<td colspan="3">
			Selezionare il bando  e la situazione relativa per visualizzare la distribuzione sul territorio dei 
			candidati ammessi a partecipare.						
			<a href="Javascript:Show_Help('/Pgm/help/Gestprogetti/Alfabeta/Areageo/DTC_VisDistTerr')">
			<img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
		</td>
	</tr>
	<tr height="2">
		<td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif">
		</td>
	</tr>
</table>
<br><br>
<%
end sub

'-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

sub ImpostaPag()

%>
<form name="frmVisAreaGeoCombo" action="DTC_InsDistTerr.asp" method="post" onsubmit="return controllo(this)">
	<table cellpadding="0" cellspacing="0" width="500" border="0">
	<!--<table cellpadding="0" cellspacing="0" width="500" height="30" border="3">-->
			<tr><td class="tbltext1" align="left" width="150"><b>Bando:</b></td>
			<!--<tr height="30">-->
			<td align="left" width="350">
				<span class="tbltext3">
					<%
					strcombo= session("iduorg") & "||CmbDescBando|" 
					apErrore = CreateBandi (strcombo)
					%>				
				</span>
			</td>
		</tr>
		<tr><td>&nbsp;</td>
		<%
		
		if apErrore = "ERRORE" then
			
			%>
			</table>
			</form>
			<%
			
			exit sub
			
		end if
		
		%>
		<tr>
			<td class="tbltext1" align="left" width="150">
				<b>Situazione relativa a:</b>
			</td>
			<td align="left" width="400">
				<select ID="cmbSituazione" name="cmbSituazione" class="textblack">
		          <option value></option>
		          <option value="AMM">Candidati ammessi</option>
		          <option value="AMMPI">Candidati ammessi da pianificare</option>
		          
		       </select>   
		   </td>
		</tr>
	</table>
	<br><br>
    <table>
	<tr>
        <td align="left" colspan="2">&nbsp;</td>
        <td align="center" colspan="2" width="60%">
			<input type="image" name="Conferma" src="<%=Session("progetto")%>/images/conferma.gif">	
		</td>
    </tr>
</table>
</form>
<%
end sub

'------------------------------------------------------ MAIN -----------------------------------------------------------------------------------------------------------------------------------------------


	Inizio()
	
	ImpostaPag()


%>
<!--#include virtual ="/include/closeconn.asp"-->
<!--#include virtual = "/strutt_coda2.asp"-->

