
<!-- #include virtual="/strutt_testa2.asp"-->
<!--#include virtual = "/include/openconn.asp"-->
<!--#include virtual="/util/portallib.asp"-->
<!--#include virtual = "/include/ControlDateVB.asp"-->

<%
if ValidateService(session("idutente"),"BAN_VISBANDI", CC) <> "true" then 
	response.redirect "/util/error_login.asp"
end if
%>

<!-- ************** Javascript Inizio ************ -->

<script LANGUAGE="Javascript">
<!--#include Virtual = "/Include/help.inc"-->
//include del file per fare i controlli sulla validit� delle date
<!--#include virtual = "/Include/ControlDate.inc"-->
//include del file per fare i controlli sulla numericit� dei campi
<!--#include virtual = "/Include/ControlNum.inc"-->
<!--#include Virtual = "/Include/ControlString.inc"-->


//Funzione per i controlli dei campi da inserire 
	function ControllaDati(frmModBando){
	    
	    frmModBando.txtCodBando.value=TRIM(frmModBando.txtCodBando.value)
		if (frmModBando.txtCodBando.value == ""){
			alert("Codice del Bando obbligatorio")
			frmModBando.txtCodBando.focus() 
			return false
		}   
        //Descrizione Bando obbligatoria
        frmModBando.txtDescBando.value=TRIM(frmModBando.txtDescBando.value)
		if (frmModBando.txtDescBando.value == ""){
			alert("Descrizione del Bando obbligatoria")
			frmModBando.txtDescBando.focus() 
			return false
		}   
	
		//Data di Pubblicazione del Bando obbligatoria
		if (frmModBando.txtDataPub.value == ""){
			alert("Data Pubblicazione del Bando obbligatoria")
			frmModBando.txtDataPub.focus() 
			return false
		}
		
		//Data di Pubblicazione del Bando corretta
		sDataPub = frmModBando.txtDataPub.value
		if (!ValidateInputDate(sDataPub)){
			frmModBando.txtDataPub.focus() 
			return false
		}
		
		if (ValidateRangeDate(frmModBando.txtInizioP.value,frmModBando.txtDataPub.value)!=true){
			alert("La data di pubblicazione del bando e' minore della data di inizio validita' del progetto")
			frmModBando.txtDataPub.focus()
			return false
		}	
		
		if (ValidateRangeDate(frmModBando.txtDataPub.value,frmModBando.txtFineP.value)==false){
			alert("La data di pubblicazione del bando e' maggiore della data di fine validita' del progetto")
			frmModBando.txtDataPub.focus()
			return false
		}
		
		frmModBando.txtIscr.value = TRIM(frmModBando.txtIscr.value);
		//Data di Inizio Acquisizione della Domanda obbligatoria
		if(frmModBando.txtIscr.value !="I" ){
		   if (frmModBando.txtDataIniAcq.value == ""){
			   alert("Data di Inizio Acquisizione della Domanda obbligatoria")
			   frmModBando.txtDataIniAcq.focus() 
			   return false
		   }
		}   
		   //Data di Inizio Acquisizione della Domanda corretta
		   sDataIni = frmModBando.txtDataIniAcq.value
		if (frmModBando.txtDataIniAcq.value != ""){
		   if (!ValidateInputDate(sDataIni)){
			   frmModBando.txtDataIniAcq.focus() 
			   return false
		   }
		   //Data di Inizio Acquisizione della Domanda deve essere superiore
		   // o uguale alla Data di Pubblicazione
		   if (!ValidateRangeDate(sDataPub,sDataIni)){
			   alert("La Data di Inizio Acquisizione della Domanda non puo' essere inferiore alla Data di Pubblicazione del Bando")
			   frmModBando.txtDataIniAcq.focus()
			   return false
		   }
        }	
		
		if(frmModBando.txtIscr.value !="I" ){
		   //Data di Fine Acquisizione della Domanda obbligatoria
		   if (frmModBando.txtDataFineAcq.value == ""){
			   alert("Data di Fine Acquisizione della Domanda obbligatoria")
			   frmModBando.txtDataFineAcq.focus() 
			   return false
		   }
		}   
		   //Data di Fine Acquisizione della Domanda corretta
		   sDataFine = frmModBando.txtDataFineAcq.value
		if (frmModBando.txtDataFineAcq.value != ""){
		   if (!ValidateInputDate(sDataFine)){
			  frmModBando.txtDataFineAcq.focus() 
			  return false
		   }
		}
		
		if((frmModBando.txtDataIniAcq.value != "")&& (frmModBando.txtDataFineAcq.value != "")){   
		   //Data di Fine Acquisizione della Domanda deve essere superiore
		   // o uguale alla Data di Inizio Acquisizione della Domanda
		   if (!ValidateRangeDate(sDataIni,sDataFine)){
			  alert("La Data di Fine Acquisizione della Domanda non puo' essere inferiore alla Data di Inizio Acquisizione della Domanda")
			  frmModBando.txtDataFineAcq.focus()
			  return false
		   }
		}   
		//Data di Pubblicazione dei Risultati della Selezione obbligatoria
		if (frmModBando.txtDataPubRis.value == ""){
			alert("Data di Pubblicazione dei Risultati della Selezione obbligatoria")
			frmModBando.txtDataPubRis.focus() 
			return false
		}
		//Data di Pubblicazione dei Risultati della Selezione obbligatoria
		sDataSel = frmModBando.txtDataPubRis.value
		if (!ValidateInputDate(sDataSel)){
			frmModBando.txtDataPubRis.focus() 
			return false
		}
		//Data di Pubblicazione dei Risultati della Selezione deve essere superiore
		// o uguale alla Data di Fine Acquisizione della Domanda
		if((frmModBando.txtIscr.value !="I" )||(frmModBando.txtDataFineAcq.value != "")){
		   if (ValidateRangeDate(sDataSel,sDataFine)==true){
			  alert("La Data di Pubblicazione dei Risultati della Selezione deve essere superiore alla Data di Fine Acquisizione della Domanda")
			  frmModBando.txtDataPubRis.focus()
			  return false
		   }
		}   
	return true
	}

</script>
<!-- ************** Javascript Fine   ************ -->
<%
dim sIscr, sSQL, sProgBando, sTimeTemp, rsBando,sCodiceBando

dim sDescBando, sBando
dim sDataPub, sDataRis
dim sDataInizio, sDataFine

sBando = UCase(Request("Bando"))




Inizio()

'accesso alla tabella BANDO per il prelevamento dei dati del bando selezionato
sSQL = "SELECT COD_BANDO,DESC_BANDO,DT_PUB,DT_INI_ACQ_DOM,DT_FIN_ACQ_DOM,DT_PUB_RIS_SEL,DT_TMST " & _
	   "from BANDO WHERE ID_BANDO = '" & sBando & "'"

set rsBando = CC.Execute(sSQL)

sProgBando = mid(sBando,3,1)
if rsBando("DT_PUB") <> "" then
	sDataPub = ConvDateToString(rsBando("DT_PUB"))
else
	sDataPub = ""
end if
if rsBando("COD_BANDO") <> "" then
	sCodiceBando = rsBando("COD_BANDO")
else
	sCodiceBando = ""
end if

if rsBando("DT_INI_ACQ_DOM") <> "" then
	sDataInizio = ConvDateToString(rsBando("DT_INI_ACQ_DOM"))
else
	sDataInizio = ""
end if 
if rsBando("DT_FIN_ACQ_DOM") <> "" then
	sDataFine = ConvDateToString(rsBando("DT_FIN_ACQ_DOM"))
else
	sDataFine = ""
end if 
if rsBando("DT_PUB_RIS_SEL") <> "" then
	sDataRis = ConvDateToString(rsBando("DT_PUB_RIS_SEL"))
else
	sDataRis = ""
end if

sTimeTemp = rsBando("DT_TMST")
sDescBando = rsBando("DESC_BANDO")

rsBando.close
set rsBando = nothing

ImpostaPag()

Fine()

sub Inizio ()
%>

	<table border="0" width="520" cellspacing="0" cellpadding="0" height="81">
	   <tr>
	     <td width="500" background="<%=Session("Progetto")%>/images/titoli/strumenti2b.gif" height="81" valign="bottom" align="right">
	       <table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
	         <tr>
	           <td width="100%" valign="top" align="right"><b class="tbltext1a">Gestione Progetti</span></b></td>
	         </tr>
	       </table>
	     </td>
	   </tr>
	</table>
	<br>
	<table cellpadding="0" cellspacing="0" width="500" border="0">
		<tr height="18">
			<td class="sfondomenu" height="18" width="67%">
			<span class="tbltext0"><b>&nbsp;GESTIONE BANDI</b></span></td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
			<td valign="middle" align="right" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1">(*) campi obbligatori</td>
		</tr>
		<tr width="371" class="SFONDOCOMM">
			<td colspan="3">
			Modifica dei dati gi� inseriti. <br>
			Premere <b>Invia</b> per salvare le modifiche. 
			<a href="Javascript:Show_Help('/Pgm/help/GestProgetti/Alfabeta/Bandi/BAN_ModBandi')" onmouseover="javascript:window.status=' '; return true"><img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a></td>
		</tr>
		<tr height="2">
			<td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>
	<br><br>
<%
end sub

'----------------------------------------------------------------------------------
sub ImpostaPag()

    sSQL ="SELECT IND_ISCR,DT_INI_PROJ,DT_FIN_PROJ FROM PROGETTO WHERE AREA_WEB='" & MID(SESSION("PROGETTO"),2) & "'"
    SET rsIscr = CC.execute(sSQL)
    if not rsIscr.eof then
        sIscr = rsIscr("IND_ISCR") 
        dtInizio = rsIscr("DT_INI_PROJ")
        dtFine = rsIscr("DT_FIN_PROJ")  
    end if
   
   %>
<form method="post" name="frmModBando" onsubmit="return ControllaDati(this)" action="BAN_CnfBandi.asp">
    <input type="hidden" name="MOD" value="2"> 
	<input type="hidden" name="txtIscr" value="<%=sIscr%>">
	<input type="hidden" name="txtInizioP" value="<%=dtInizio%>">
	<input type="hidden" name="txtFineP" value="<%=dtFine%>">
	<input type="hidden" name="txtidBando" value="<%=sBando%>">
	<input type="hidden" name="txtTimeTemp" value="<%=sTimeTemp%>">
	
	<table border="0" cellpadding="2" cellspacing="0" width="500">
		<tr>
		   <td align="left" width="250" class="tbltext1">
				<b>Codice Bando*&nbsp;</b>
		   </td>
		   <td align="left" width="250">
				<input type="text" style="TEXT-TRANSFORM:uppercase;" class="textblack" size="13" maxlength="3" name="txtCodBando" value="<%=sCodiceBando%>" readonly>
		   </td>
		</tr>
		<tr>
		    <td align="left" class="tbltext1">
		        <b>Descrizione del Bando*</b>
		    </td>
		    <td>    
		        <input type="text" style="TEXT-TRANSFORM:uppercase;" class="textblack" size="45" maxlength="50" name="txtDescBando" value="<%=sDescBando%>">
		   </td>
		</tr>
		<tr class="tbltext1">
			<td nowrap>
				<b>Data della Pubblicazione<br> del Bando*</b>&nbsp;(gg/mm/aaaa)
			</td>
			<td>
				<input style="TEXT-TRANSFORM: uppercase;" class="textblack" size="13" maxlength="10" name="txtDataPub" value="<%=sDataPub%>">
			</td>
		</tr>
		<tr class="tbltext1">
			<td nowrap>
				<b>Data di Inizio Acquisizione<br> della Domanda</b>&nbsp;(gg/mm/aaaa)
			</td>
			<td>
				<input style="TEXT-TRANSFORM: uppercase;" class="textblack" size="13" maxlength="10" name="txtDataIniAcq" value="<%=sDataInizio%>">
			</td>
		</tr>
		<tr class="tbltext1">
			<td nowrap>
				<b>Data di Fine Acquisizione<br> della Domanda</b>&nbsp;(gg/mm/aaaa)
			</td>
			<td>
				<input style="TEXT-TRANSFORM: uppercase;" class="textblack" size="13" maxlength="10" name="txtDataFineAcq" value="<%=sDataFine%>">
			</td>
		</tr>
		<tr class="tbltext1">
			<td nowrap>
				<b>Data della Pubblicazione dei<br> Risultati della Selezione*</b>&nbsp;(gg/mm/aaaa)
			</td>
			<td>
				<input style="TEXT-TRANSFORM: uppercase;" class="textblack" size="13" maxlength="10" name="txtDataPubRis" value="<%=sDataRis%>">
			</td>
		</tr>
	</table>
	<br><br>
<%'tabella con i due pulsanti%>
	<table cellpadding="0" cellspacing="0" width="300" border="0">	
		<tr align="center">
			<td nowrap><input type="image" name="Conferma" src="<%=Session("progetto")%>/images/conferma.gif"></td value="Registra">
			<td nowrap><a href="javascript:history.go(-1)" onmouseover="javascript:window.status=' '; return true"><img src="<%=session("Progetto")%>/images/indietro.gif" border="0" name="imgPunto2"></a><td>
	    </tr>
	</table>
</form>		
<%
end sub

sub Fine()%>
	<!--#include virtual = "/include/closeconn.asp"-->
	<!--#include virtual = "strutt_coda2.asp"-->
<%end sub%>



