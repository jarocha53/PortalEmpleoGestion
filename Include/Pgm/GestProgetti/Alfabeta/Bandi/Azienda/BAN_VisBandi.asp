<!-- #include virtual="/strutt_testa2.asp"-->
<!--#include virtual ="/include/openconn.asp"-->
<!--#include virtual="/util/portallib.asp"-->
<!--#include virtual = "/include/ControlDateVB.asp"-->
<!--#include virtual = "include/SelAreaTerrBandi.asp"-->

<%
if ValidateService(session("idutente"),"BAN_VISBANDI", CC) <> "true" then 
	response.redirect "/util/error_login.asp"
end if
%>

<!-- ************** Javascript inizio ************ -->
<script LANGUAGE="Javascript">

<!--#include Virtual = "/Include/help.inc"-->
<!--#include Virtual = "/Include/ControlDate.inc"-->
	
function ChiamaBandi(dtData,sBando,flag)
{

var	NomePag
	if (flag == 1 ){
		document.frmInsBandi.submit();    
	}
	else{
	    if(ValidateRangeDate(document.frmDettBandi.txtOggi.value,dtData)==false){
	       document.frmDettBandi.Bando.value = sBando; 
	       document.frmDettBandi.action = "BAN_ModBandi.asp";   
		   document.frmDettBandi.submit(); 
		}
		else{
		   document.frmDettBandi.Bando.value = sBando;   
		   document.frmDettBandi.submit(); 
		}   		
	}
}	
	
</script>
<!-- ******************  Javascript Fine *********** -->

<%
	
	Inizio()

	ImpostaTab()    

	Fine()
'-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

sub Inizio()
%>
	<table border="0" width="520" cellspacing="0" cellpadding="0" height="81">
	   <tr>
	     <td width="500" background="<%=Session("Progetto")%>/images/titoli/strumenti2b.gif" height="81" valign="bottom" align="right">
	       <table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
	         <tr>
	           <td width="100%" valign="top" align="right"><b class="tbltext1a">Gestione Progetti</span></b></td>
	         </tr>
	       </table>
	     </td>
	   </tr>
	</table>

	<br>
	<table cellpadding="0" cellspacing="0" width="500" border="0">
		<tr height="18">
			<td class="sfondomenu" height="18" width="67%">
			<span class="tbltext0"><b>&nbsp;GESTIONE BANDI</b></span></td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
			<td valign="middle" align="right" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1"></td>
		</tr>
		<tr width="371" class="SFONDOCOMM">
			<td colspan="3">
				Visualizzazione dei bandi.
				<br>Per visualizzare e/o modificare i dati di un bando premere sul<b> Codice Bando</b>. 
				<br>Per inserire un bando non ancora registrato premere su<b> 
				Inserisci un nuovo Bando
				<a href="Javascript:Show_Help('/Pgm/help/GestProgetti/Alfabeta/Bandi/BAN_VisBandi')" onmouseover="javascript:window.status=' '; return true"><img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
			</td>
		</tr>
		<tr height="2">
			<td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>
	<br>
<%
end sub
'-------------------------------------------------------------------
 sub ImpostaTab()
     dim sBando, sDataPub, SIniAcq, sFineAcq, sDescBando
%>  
    <form method="POST" action="BAN_InsBandi.asp" name="frmInsBandi">
    <table border="0" cellpadding="0" cellspacing="1" width="500">
		<tr>
			<td align="center" colspan="2">
				<a href="Javascript:ChiamaBandi('','<%=sBando%>','<%=1%>')" class="textred" onmouseover="javascript:window.status=' '; return true">
					<b>Inserisci un nuovo Bando</b>
				</a>
			</td>
		</tr>
	</table>
	</form>
	<br>
<%
	'Accesso alla tabella BANDO per il prelevamento dei dati relativi all'Area Web in esame

	sSQL ="SELECT ID_BANDO,COD_BANDO,DESC_BANDO,DT_PUB," & _
	      "DT_INI_ACQ_DOM,DT_FIN_ACQ_DOM,DT_PUB_RIS_SEL FROM BANDO A, PROGETTO B, gruppo c" &_
	      " WHERE A.ID_PROJ=B.ID_PROJ AND AREA_WEB='" & MID(Session("Progetto"),2) & "'" &_
	      " and a.idgruppo = c.idgruppo and webpath = '/AZIENDA' and cod_rorga = 'AZ' "
		
	set rsBando = CC.Execute(sSQL)
	if rsBando.EOF then
%>		
	   <table align="center">
			<tr><td class="tbltext3"><b>Non ci sono Bandi per l'area selezionata.</b></td></tr>	
		</table>
<%	else
%>      <form method="POST" action="BAN_ModBandi.asp" name="frmDettBandi">
        <input type="hidden" name="Bando" value>
        <input type="hidden" name="txtOggi" value="<%=ConvDateToString(Date())%>">
       	<table border="0" cellpadding="1" cellspacing="2" width="500">
			<tr class="sfondocomm">
				<td align="center">
					<b>Codice<br>Bando</b>
				</td>
				<td align="center">
					<b>Descrizione<br>Bando</b>
				</td>
				<td align="center" width="15%">
					<b>Data<br>Pubblicazione</b>
				</td>
				<td align="center" width="15%">
					<b>Inizio<br>Iscrizione</b>
				</td>
				<td align="center" width="15%">
					<b>Fine<br>Iscrizione</b>
				</td>
			</tr>
	
<%      

		do while not rsBando.EOF
			sBando = rsBando("ID_BANDO")
			
			if rsBando("COD_BANDO") <> "" then
				sCodBando = rsBando("COD_BANDO")
			else
				sCodBando = ""
			end if
			
			if rsBando("DESC_BANDO") <> "" then
				sDescBando = trim(rsBando("DESC_BANDO"))
			else
				sDescBando = ""
			end if
			
			if rsBando("DT_PUB") <> "" then
				sDataPub = ConvDateToString(CSTR(rsBando("DT_PUB")))
			else
				sDataPub = ""
			end if
				
			if rsBando("DT_INI_ACQ_DOM") <> "" then
				sIniAcq = ConvDateToString(rsBando("DT_INI_ACQ_DOM"))
			else
				sIniAcq = ""
			end if
				
			if rsBando("DT_FIN_ACQ_DOM") <> "" then
				sFineAcq = ConvDateToString(rsBando("DT_FIN_ACQ_DOM"))
			else
				sFineAcq = ""
			end if
			if rsBando("DT_PUB_RIS_SEL") <> "" then
			    dtPubblicazione = rsBando("DT_PUB_RIS_SEL")
			else
			    dtPubblicazione = ConvDateToString(Date())    
			end if
	%>		
	       <tr class="tblsfondo">
				<td class="tblDett">
					<a class="tblAgg" HREF="Javascript:ChiamaBandi('<%=dtPubblicazione%>','<%=sBando%>','<%=2%>')" onmouseover="javascript:window.status=' '; return true">
						<%=sCodBando%>
					</a>
				</td>
				<td class="tblDett">
					<%=sDescBando%>
				</td>
				<td class="tblDett">
					<%=sDataPub%>
				</td>
				<td class="tblDett">
					<%=SIniAcq%>
				</td>
				<td class="tblDett">
					<%=sFineAcq%>
				</td>
			</tr>
			
<%				
		rsBando.MoveNext
		loop
%>		
        </table>
        </form>	
		<br>
<%
	end if
	
	rsBando.Close
	set rsBando = nothing	
end sub
'-------------------------------------------------------------------
 sub Fine()%>
	<!--#include virtual ="/include/closeconn.asp"-->
	<!--#include virtual = "strutt_coda2.asp"-->
<%end sub %>



