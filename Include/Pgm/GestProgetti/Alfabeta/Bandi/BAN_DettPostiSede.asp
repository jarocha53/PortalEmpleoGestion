<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<html>
<head>
<title>Gestione Progetti</title>
<link REL="STYLESHEET" TYPE="text/css" HREF="../../fogliostile.css">

<script language="Javascript">
<!--#include Virtual = "/Include/help.inc"-->

function ControllaDati(myForm) 
{
	if (myForm.txtPosti.value == "") 
	{
		alert ("Il campo N. Posti � obbligatorio")
		myForm.txtPosti.focus()
		return false
	}
		
	if (!Numerico(myForm.txtPosti))
	{
		return false;
	}
	
	return true
}

//controllo numericit� dei campi
function Numerico(campo)
{			
	var dim=campo.size;
	var anyString = campo.value;

	for ( var i=0; i<=anyString.length-1; i++ )
	{
		if ( ( (i>0) && (anyString.charAt(i) >= "0") && (anyString.charAt(i) <= "9") ) || ((i == 0) && (anyString.charAt(i) > "0") && (anyString.charAt(i) <= "9") ) ) 
		{
		}
		else
		{
			alert("Attenzione: Il valore immesso deve essere numerico e superiore a zero!");
			campo.focus();
			return(false);
		}
	}
	return true;
}

</script>
</head>

<%
'-------------------'
Sub Inizio()%>
<body>
	<table border="0" width="520" cellspacing="0" cellpadding="0" height="81">
	   <tr>
	     <td width="500" background="<%=Session("Progetto")%>/images/titoli/strumenti2b.gif" height="81" valign="bottom" align="right">
	       <table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
	         <tr>
	           <td width="100%" valign="top" align="right"><b class="tbltext1a">Gestione Progetti</b></td>
	         </tr>
	       </table>
	     </td>
	   </tr>
	</table>

	<br>
	<table cellpadding="0" cellspacing="0" width="500" border="0">
		<tr height="18">
			<td class="sfondomenu" height="18" width="67%">
			<span class="tbltext0"><b>&nbsp;MODIFICA DISPONIBILITA' POSTI / SEDE</b></span></td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
			<td valign="middle" align="right" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1">(*) campi obbligatori</td>
		</tr>
		<tr width="371" class="SFONDOCOMM">
			<td colspan="3">
				Modifica del numero di Posti disponibili per la Sede selezionata.
				<br>Modificare il valore del campo <b>N. Posti</b> 
				e premere <b>Invia</b>.
				<a href="Javascript:Show_Help('/Pgm/help/Gestprogetti/alfabeta/Bandi/BAN_DettPostiSede')">
				<img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
			</td>
		</tr>
		<tr height="2">
			<td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>
	<br><br>
<%
End Sub

'-------------------'
Sub Dettaglio()

	sSQL =	"SELECT NUM_POSTI, DT_TMST FROM BANDO_POSTI" &_
			" WHERE ID_BANDO=" & nBando &_
			" AND ID_SEDE=" & nSede
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	set rsPosti = CC.Execute(sSQL)

	If rsPosti.EOF then
%>
		<table cellpadding="0" cellspacing="0" width="500" border="0">
			<tr align="center">
				<td class="tbltext3">
					<b>Non risulta stabilito il numero di posti per la Sede selezionata.</b>
				</td>
			</tr>	
		</table>
		<br>
		<table cellpadding="0" cellspacing="0" width="500" border="0">
			<tr align="center">
				<td><a HREF="Javascript:FrmBack.submit();" onmouseover="javascript:window.status='' ; return true"><img src="<%=session("Progetto")%>/images/indietro.gif" border="0" name="imgPunto2"></a><td>
			</tr>
		</table>
<%
	Else
%>
		<form name="frmDettPostiSede" method="post" onsubmit="return ControllaDati(this)" action="BAN_CnfModPostiSede.asp">
		<input type="hidden" name="hdnIdBando" value="<%=nBando%>">					
		<input type="hidden" name="hdnIdSede" value="<%=nSede%>">
		<input type="hidden" name="hdnProv" value="<%=sProv%>">					
		<input type="hidden" name="hdnTMST" value="<%=rsPosti("DT_TMST")%>">
	
		<table border="0" cellpadding="0" cellspacing="1" width="500">
			<tr height="20">
				<td align="left" class="tbltext1" width="35%">
					<b>Bando</b>
				</td>		
				<td align="left" class="tbltext"> 
					<b><%=sDescBando%></b>
				</td>
			</tr>
			<tr height="20">
				<td align="left" class="tbltext1">
					<b>Provincia</b>
				</td>
				<td align="left" class="tbltext"> 
					<b><%=sDescProv%></b>
				</td>
			</tr>
    		<tr height="20">
				<td align="left" class="tbltext1">
					<b>Rag. Sociale</b>
				</td>
				<td align="left" class="tbltext"> 
					<b><%=aDescSede(0)%></b>
				</td>
			</tr>
			<tr height="20">
				<td align="left" class="tbltext1">
					<b>Sede</b>
				</td>
				<td align="left" class="tbltext"> 
					<b><%=aDescSede(1)%></b>
				</td>
			</tr>
			<tr class="tbltext1">
				<td align="left">
					<b>N. Posti*</b>
				</td>
				<td>			
					<input type="text" name="txtPosti" maxlength="4" size="6" align="left" class="textblack" style="TEXT-TRANSFORM:uppercase;" value="<%=rsPosti("num_posti")%>">			
				</td>
			</tr>
		</table>
		<br><br>
    
		<table cellpadding="0" cellspacing="0" width="500" border="0">
			<tr align="center">
				<td nowrap><input type="image" name="Conferma" value="Registra" src="<%=Session("progetto")%>/images/conferma.gif"></td>
				<td><a HREF="Javascript:FrmBack.submit();" onmouseover="javascript:window.status='' ; return true"><img src="<%=session("Progetto")%>/images/indietro.gif" border="0" name="imgPunto2"></a><td>
		    </tr>
		</table>    
	</form>
<%	End If
	rsPosti.Close
	set rsPosti=nothing
End Sub	

'----------------F U N C T I O N -----------------'
Function DescSede(nIdS)
	
	dim rsSede, sSQL
	
	sSQL=	"SELECT si.descrizione, i.rag_soc"	&_
			" FROM SEDE_IMPRESA si, IMPRESA i"			&_
			" WHERE si.id_sede= " & nIdS	&_
			" AND si.id_impresa= i.id_impresa"
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	set rsSede = CC.Execute(sSQL)
	
	IF NOT rsSede.eof THEN
		DescSede = rsSede("rag_soc") & "|" & rsSede("descrizione")
	ELSE
		DescSede = ""
	END IF
	
	rsSede.close
	set rsSede = nothing
		
End Function

'-------------------'
Sub Fine()
	
%>
	</body>
	</html>
	<!--#include virtual ="/include/closeconn.asp"-->
	<!--#include virtual = "strutt_coda2.asp"-->
	
<%
End Sub

'-------------------------------------------------'
'-------------------' M A I N '-------------------'
%>
	<!-- #include virtual="/strutt_testa2.asp"-->
	<!--#include virtual ="/include/openconn.asp"-->
	<!--#include virtual="/util/portallib.asp"-->
	<!--#include virtual="/include/SelAreaTerrBandi.asp"-->
<%
	if ValidateService(session("idutente"),"BAN_VISPOSTI", CC) <> "true" then 
		response.redirect "/util/error_login.asp"
	end if

	dim nBando, sProv, nSede
	dim sDescBando, sDescProv, sDescSede
	dim sSQL, rsSess

	nBando=		Request.Form("hdnIdBando")
	sDescBando=	DecBando(nBando)
	sProv=		Request.Form("hdnProv")
	sDescProv=	DecCodVal("PROV", 0, "", sProv,"")
	nSede=		Request.Form ("hdnIdSede")
	sDescSede=	DescSede(nSede)
	aDescSede=	split(sDescSede,"|")
%>
	<form name="FrmBack" action="BAN_ModPostiSede.asp" method="post">
		<input type="hidden" name="hIdBando" value="<%=nBando%>">
		<input type="hidden" name="cmbProv" value="<%=sProv%>">
	</form>
<%
	Inizio()
	Dettaglio()
	Fine()
%>



