<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #include virtual="/strutt_testa2.asp"-->
<!--#include virtual ="/include/openconn.asp"-->
<!--#include virtual="/util/portallib.asp"-->
<!--#include virtual = "include/ControlDateVB.asp"-->
<!--#include virtual = "include/SelAreaTerrBandi.asp"-->


<%
if ValidateService(session("idutente"),"BAN_VISPOSTI", CC) <> "true" then 
	response.redirect "/util/error_login.asp"
end if
%>
<!-- ************** Javascript Inizio ************ -->
<script LANGUAGE="Javascript">
<!--#include virtual="/Include/help.inc"-->
<!--#include virtual = "/Include/ControlDate.inc"-->
<!--#include virtual = "/Include/ControlNum.inc"-->
<!--#include virtual = "/Include/ControlString.inc"-->  
	
//Funzione per i controlli del campo da inserire 
function ControllaDato(frmInsBando)
{
		
	//Descrizione del Bando obbligatoria
	frmInsBando.txtNumPosti.value = TRIM(frmInsBando.txtNumPosti.value);
	if ((frmInsBando.txtNumPosti.value == "")){
		alert("Numero posti del Bando obbligatoria")
		frmInsBando.txtNumPosti.focus() 
		return false
		}
			
		/*var controlla
		controlla = document.frmInsBando.hPostiMod.value
		//alert (controlla)
		if (controlla == 0){
			frmInsBando.Action.value = "INS"
			}
		else 
			{
			frmInsBando.Action.value = "MOD"
			}	
		*/		
	//return true
	//}
	
	if (!Numerico(frmInsBando.txtNumPosti))
	{
		return false;
	}
	
return true;
}

//controllo numericitÓ dei campi
function Numerico(campo)
{			
	var dim=campo.size;
	var anyString = campo.value;

	for ( var i=0; i<=anyString.length-1; i++ )
	{
		if ( ( (i>0) && (anyString.charAt(i) >= "0") && (anyString.charAt(i) <= "9") ) || ((i == 0) && (anyString.charAt(i) > "0") && (anyString.charAt(i) <= "9") ) ) 
		{
		}
		else
		{
			alert("Attenzione: Il valore immesso deve essere numerico e superiore a zero!");
			campo.focus();
			return(false);
		}
	}
	return true;
}
</script>
<!-- ************** Javascript Fine   ************ -->
<%
dim sDesc, sBando
sBando =request("hIdBando")
sDesc= DecBando(sBando)
nNumPosti = Cint(request("hNumPosti"))

Intestazione()
PreparaPagina()
fine()

sub Intestazione() %>
<table border="0" width="520" cellspacing="0" cellpadding="0" height="81">
<tr>
  <td width="500" background="<%=Session("Progetto")%>/images/titoli/strumenti2b.gif" height="81" valign="bottom" align="right">
    <table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
      <tr>
        <td width="100%" valign="top" align="right"><b class="tbltext1a">Gestione Progetti</span></b></td>
      </tr>
    </table>
  </td>
</tr>
</table>
<br>
<table cellpadding="0" cellspacing="0" width="500" border="0">
<tr height="18">
	<td class="sfondomenu" height="18" width="67%">
	<span class="tbltext0"><b>&nbsp;DISPONIBILITA' POSTI</b></span></td>
	<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
	<td valign="middle" align="right" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1">(*) campi obbligatori</td>
</tr>
<tr width="371" class="SFONDOCOMM">
	<td colspan="3">
	Inserimento dato relativo al numero posti del Bando. <br>
	Premere <b>Invia</b> per salvare. 
	<a href="Javascript:Show_Help('/Pgm/help/GestProgetti/Alfabeta/Bandi/BAN_InsPosti')" onmouseover="javascript:window.status=' '; return true"><img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a></td>
</tr>
<tr height="2">
	<td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif">
	</td>
</tr>
</table>
	<br>		
<%end sub%>

<%sub PreparaPagina()%>
<form method="post" name="frmInsBando" onsubmit="return ControllaDato(this)" action="BAN_CnfInsPosti.asp">
<%
	sSQL = "select id_bando, dt_tmst from bando_posti where id_bando=" & sBando & "and id_sede=0"
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	SET rsSede = CC.execute(sSQL)

	if not rsSede.eof then
	    sDataTmst = rsSede("dt_tmst")
%>
	    <input type="hidden" name="Action" value="MOD">
<%	else %>
		<input type="hidden" name="Action" value="INS">
<%	end if

	rsSede.close
	set rsSede= nothing
%>
	<!--input type="hidden" name="hPostiMod" value="<%'=nNumPosti%>"-->
    <input type="hidden" name="hDescBando" value="<%=sDesc%>">
    <input type="hidden" name="hIdBando" value="<%=sBando%>">
    <input type="hidden" name="hDataTmst" value="<%=sDataTmst%>">
	<table border="0" cellpadding="2" cellspacing="0" width="500"> 
		<tr>
			<td align="left" class="tbltext1" width="40%">
			     <b>Descrizione del Bando</b>
			</td>
			<td class="tbltext">
			    <b><%=sDesc%><b>
			</td>
		</tr>
		<tr> 
		   <td align="left" nowrap class="tbltext1"> 
		       <b>Numero posti*</b> 
		   </td> 
		   <td align="left"> 
		       <input type="text" style="TEXT-TRANSFORM:uppercase;" class="textblack" size="13" maxlength="4" name="txtNumPosti" value="<%=nNumPosti%>"> 
		   </td> 
		</tr>
	</table> 
	<br>
	<br>
	<%'impostazione dei comandi%>
	<table cellpadding="0" cellspacing="0" width="300" border="0">	
	<tr align="center">
	  <td nowrap><input type="image" name="Conferma" src="<%=Session("progetto")%>/images/conferma.gif"></td value="Registra">
	  <td nowrap><a href="javascript:history.go(-1)"><img src="<%=session("Progetto")%>/images/indietro.gif" border="0" name="imgPunto2" onmouseover="javascript:window.status=' '; return true"></a></td>
	</tr>
	</table>
</form> 
<%end sub %>

<%sub fine()%>
<!--#include virtual ="/include/closeconn.asp"-->
<!--#include virtual = "strutt_coda2.asp"-->
<%end sub %>
