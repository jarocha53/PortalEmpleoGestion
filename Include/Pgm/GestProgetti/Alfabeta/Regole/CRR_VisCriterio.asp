<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->

<!-- #include virtual="/strutt_testa2.asp"-->
<!--#include virtual ="/include/openconn.asp"-->
<!--#include virtual="/util/portallib.asp"-->
<%
if ValidateService(session("idutente"),"CRR_VisCriterio", CC) <> "true" then 
	response.redirect "/util/error_login.asp"
end if
%>
<!-- ************** Javascript inizio ************ -->

<script LANGUAGE="Javascript">

<!--#include Virtual = "/Include/help.inc"-->

	function Reload(){
	    if (document.frmVisCriterio.cmbBando.value==""){
	    alert ("selezionare un bando")    
    }
	    else {
		    document.frmVisCriterio.submit();
		
	}
	}
	function Vai(){
	   
	    document.insCriterio.submit();
	     
	    
    }
    function VaiM(sIdRegola, strBando){
         location.href = "CRR_ModCriterio.asp?idRegola=" + sIdRegola + "&Bando=" + strBando
    }
</script>
<!-- ******************  Javascript Fine *********** -->
<!-- ************** ASP inizio *************** -->
<!--#include virtual = "/include/DecCod.asp"-->
<%sub Inizio()%>
	<table border="0" width="520" cellspacing="0" cellpadding="0" height="81">
	   <tr>
	     <td width="500" background="<%=Session("Progetto")%>/images/titoli/strumenti2b.gif" height="81" valign="bottom" align="right">
	
	       <table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
	         <tr>
	             <td width="100%" valign="top" align="right">
	                 <b class="tbltext1a">Gestione Progetti</span></b>
	             </td>
	         </tr>
	       </table>
	     </td>
	   </tr>
	</table>

	<br>
	<table cellpadding="0" cellspacing="0" width="500" border="0">
		<tr height="18">
			<td class="sfondomenu" height="18" width="67%">
			    <span class="tbltext0"><b>GESTIONE REGOLE</b></span></td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
			<td valign="middle" align="right" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1"></td>
		</tr>
		<tr width="371" class="SFONDOCOMM">
			<td colspan="3">
				Selezionare un bando e un tipo regola per ottenere la visualizzazione in tabella delle regole relative.
				<br>Per visualizzare e/o modificare i dati premere sul campo<b> Progressivo Regola</b>. 
				<br>Per inserire una regola non ancora registrata premere su 
				<b>Inserisci nuova Regola</b> 
			<a href="Javascript:Show_Help('/Pgm/help/GestProgetti/Alfabeta/Regole/CRR_VisCriterio')" onmouseover="javascript:window.status=' '; return true"><img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
			</td>
		</tr>
		<tr height="2">
			<td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>
	<br>
<%
end sub
'-----------------------------------------------------------------------------------------------------------------------------------------------------------
Sub ImpostazioneMASK()

		sMask = Session("mask") 

		Session("Diritti")= mid(sMask,1,2)
		Session("Filtro")= mid(sMask,3)	

		sMask = Session("Diritti")
	
		sProvSedi = Session("Filtro")
		nProvSedi = len(sProvSedi)
		
		if nProvSedi > 0 then
			'ritona quante provincie posso visionare
			nProvSedi = nProvSedi/2 
			'Ciclo per creare la combo delle provincie con le sole provincie di interesse
			sCondizione = " WHERE "
			for i=1 to nProvSedi
				if i <> 1 then
					sCondizione = sCondizione & " OR "
				end if

				pos_ini = 2 * (i-1)
				sCondizione = sCondizione & " ID_BANDO like '" & mid(Session("Filtro"),1+pos_ini,2) & "%' "
			next
		else 	
			sCondizione = ""
		end if
end sub

sub VisualizzaMessaggio(sMessaggio)
%>
    <br>
        <table width="500">
			<tr><td class="tbltext3" align="center"><b><%=sMessaggio%></b></td></tr>	
		</table>
    <br>		
<%			
end sub

sub OpzioneOperazioni()
%>   <br>

<form name="insCriterio" method="post" action="CRR_InsCriterio.asp">
      <input type="hidden" name="TipRegola" value="<%=sTipologia%>">
      <input type="hidden" name="Bando" value="<%=sBando%>">
      <br>
     <table width="150" border="0" align="center">
			<tr> 
				<td align="center"> 
					 <a href="Javascript:insCriterio.submit()" class="textred" onmouseover="javascript:window.status=' '; return true">
				     <b>Inserisci nuova Regola</b></a>
				</td>
		   </tr>		
	</table>
</form>	
<%
end sub	
	
sub Fine()
%>
<!--#include virtual = "/include/closeconn.asp"-->
<!--#include virtual = "strutt_coda2.asp"-->
<%
end sub
'-----------------------------------------------------------------------------------------------------------------------------------------------------------
sub ImpostaPag(sBando)

'sSQL = "Select ID_BANDO,DT_PUB,DT_INI_ACQ_DOM,DT_FIN_ACQ_DOM,SIGLA_PRG FROM BANDO" 
sSQL = "Select ID_BANDO,COD_BANDO,DESC_BANDO,DT_PUB,DT_INI_ACQ_DOM,DT_FIN_ACQ_DOM,DT_PUB_RIS_SEL,a.DT_TMST FROM BANDO A, PROGETTO B" &_
		" WHERE A.ID_PROJ=B.ID_PROJ AND AREA_WEB='" & mid(Session("Progetto"),2) & "'" &_
		" AND DT_PUB_RIS_SEL > SYSDATE ORDER BY DESC_BANDO"

'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
set rsBando = CC.Execute(sSQL)	
if rsBando.EOF then
   VisualizzaMessaggio("Non ci sono Bandi da selezionare")
   rsBando.Close
   set rsBando = nothing
   exit sub
 end if

%>
<form name="frmVisCriterio" method="post" action="CRR_VisCriterio.asp">
	 <table width="280" border="0" cellspacing="1" cellpadding="6" align="left">
			<tr>
				<td align="left">
					<span class="tbltext1"><b>Bando</b></span>
				</td>	
				<td align="left">
				

           			<select class="textblack" name="cmbBando" onchange="JAVASCRIPT:Reload()">
						
							  <option></option> 
							<%
							
							   if sBando = "0" then %>
							  <option value="0" selected>GENERALE</option> 
						<%	else %>
						    
							  <option value="0">GENERALE</option> 
	<%				    	  end if
		                  do while not rsBando.EOF
		                     if cint(sBando) = cint(rsBando("ID_BANDO")) then 
		                 	    pippo ="Selected"
		                 	 else
		                 	    pippo =""
			                 end if 
			                Response.Write "<OPTION value ='" & rsBando("ID_BANDO")
				      
			               Response.write  "' " & pippo & "> " & rsBando("DESC_BANDO")  & "</OPTION>"
			               	
			                rsBando.MoveNext 
		                  loop 
		                  rsBando.Close
	                      set rsBando = nothing
%>
		            </select>
				</td>
			</tr>
			<tr>     
				<td align="left" class="tbltext1"><b>Tipo Regola</b></td>	
            	<td>
			       <select class="textblack" name="cmbTipologia" <%
				if sBando <> "" then
					Response.Write (" onchange='JAVASCRIPT:Reload()'") 
				end if
			%>>
				
				       <option value="S" <%
					if sTipologia = "S" then 
						Response.Write (" selected ")
					end if
				%>>Selezione</option>
				       <option value="V" <%
					if sTipologia = "V" then 
						Response.Write (" selected ")
					end if
				%>>Valorizzazione</option>
				       <option value="A" <%
					if sTipologia = "A" then 
						Response.Write (" selected ")
					end if
				%>>Aggregazione</option>
			       </select>
			</b>
		        </td>
	        </tr>
     </table>
    <br><br><br>
</form>
<%
if sBando <> "" then
	set rsRegole = Server.CreateObject("ADODB.RecordSet")
	
	'sSQLL = "SELECT a.ID_BANDO, a.cod_bando, a.DT_PUB, a.DT_INI_ACQ_DOM, a.DT_FIN_ACQ_DOM, a.DT_PUB_RIS_SEL, a.DT_TMST, b.regola " & _
			'	"FROM bando a, regole b " & _
			'	"WHERE a.id_bando=b.id_bando  " & _
			'	"ORDER BY a.id_bando "
	
	
	sSQLL = "SELECT ID_REGOLE,REGOLA,DESC_REGOLA,PRG_REGOLA,ID_BANDO FROM REGOLE "

	if sTipologia <> "" then
		sSQLL = sSQLL & " WHERE TIPO_REGOLA ='" & sTipologia & "'"	
	else
		sSQLL = sSQLL & " WHERE TIPO_REGOLA ='S'"	
	end if 

	sSQLL = sSQLL & "AND (ID_BANDO = " & sBando & " OR ID_BANDO is null)"
	sSQLL = sSQLL & " ORDER BY PRG_REGOLA"
	'Response.Write (ssqll)
'PL-SQL * T-SQL  
SSQLL = TransformPLSQLToTSQL (SSQLL) 
	set rsRegole = CC.Execute(sSQLL)
		
	nFlagRegole=0
    
    if rsRegole.EOF  then
         VisualizzaMessaggio("Non esistono regole per le selezioni scelte")
 	     OpzioneOperazioni()
 	Else 
 	    if  sTipologia = "A" then 
             VisualizzaMessaggio("Non � possibile inserire altre regole di aggregazione")
 	    else
 	        OpzioneOperazioni()
 	    end if
 	%>
	
		<table border="0" width="500">
			<tr class="sfondocomm"> 
		        <td align="center" width="125">
					<b>Progressivo Regola</b>
				</td>
				<td align="center" width="100">
					<b>Codice Bando</b>
				</td>
				<td align="center" width="275">
					<b>Descrizione Regola</b>
				</td>
			</tr>

 <%    i = 0
	   do while not rsRegole.EOF
		  nFlagRegole = 1%>
		<form name="frmMod<%=i%>" method="post" action="CRR_ModCriterio.asp">
		<input type="hidden" name="idRegola" value="<%=rsRegole("ID_REGOLE")%>">
		<input type="hidden" name="Bando" value="<%=sBando%>">
		</form>
		<tr class="tblsfondo">
			<td class="tblDett">
			    <a class="tblAgg" href="Javascript:frmMod<%=i%>.submit()" onmouseover="javascript:window.status=' '; return true">
				  <%=rsRegole("PRG_REGOLA")%></a>
			</td>
			<td class="tblDett">
				<%pluto =rsRegole("ID_BANDO")
				if pluto <> "" and pluto <> "0" then
				ssql= "select cod_bando from bando where id_bando =" & pluto
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
				set rscodice = CC.Execute(ssql)
				Response.Write rscodice("COD_BANDO")
				else
				Response.Write ""
				end if%>
			</td>
			<td class="tblDett">
				<%=rsRegole("DESC_REGOLA")%>
			</td>
		</tr>
		
	<%  i = i+1
		rsRegole.MoveNext 
	loop
%>	
        </table>
<%end if 
 
  rsRegole.Close
  set rsRegole = nothing

end if 
%>
<br>
<%end sub%>
<!-- ************** ASP Fine *************** -->
<!-- ************** MAIN Inizio ************ -->
<%
	dim sSQL
	
	dim rsBando
	dim rsRegole
	dim sIsa
	dim sCondizione
	dim sBando
	dim sTipologia
	dim nProvSedi
	dim sProvSedi, nFlagRegole
	
	sBando		= Request ("cmbBando")
	sTipologia	= Request("cmbTipologia")
   ' scodbando = Request ("Codice")
	Inizio()
'	ImpostazioneMASK()
	ImpostaPag(sBando)
	Fine()
%>
<!-- ************** MAIN Fine ************ -->



