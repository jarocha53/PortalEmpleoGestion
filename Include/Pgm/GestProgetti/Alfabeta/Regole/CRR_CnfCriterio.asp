<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%'Option Explicit 
'   Response.ExpiresAbsolute = Now() - 1 
'   Response.AddHeader "pragma","no-cache"
'   Response.AddHeader "cache-control","private"
'   Response.CacheControl = "no-cache"
%>

<!--#include virtual="/strutt_testa2.asp"-->
<!--#include virtual="/include/openconn.asp"-->
<!--#include virtual="/util/portallib.asp"-->
<%
if ValidateService(session("idutente"),"CRR_VISCRITERIO", CC) <> "true" then 
	response.redirect "/util/error_login.asp"
end if
%>

<!-- ************** Javascript inizio ************ -->

<script language="JAVAScript">

	function VaiInizio(sBando, sTipo){
		location.href = "CRR_VisCriterio.asp?Bando=" + sBando + "&Tipologia=" + sTipo;
	}	

</script>

<!-- ************** Javascript Fine ************ -->

<!-- ************** ASP inizio *************** -->

	<!--#include virtual = "/util/dbutil.asp"-->

	<!--#include virtual = "/include/ControlDateVB.asp"-->
	<!--#include virtual = "/include/SysFunction.asp"-->
<%
Sub Msgetto(Msg)
%>
	<br>
	<table align="center">
		<tr>
			<td class="tbltext3"><b><%=Msg%></b></td>
		</tr>	
	</table>
<%
End Sub

'-------------------------------------------------------------------------------------------------------------------------------

sub TornaIndietro()

	%>
	<form name="frmIndietro" method="post" action="CRR_VisCriterio.asp">
		<input type="hidden" name="Bando" value="<%=sIdBando%>">
		<input type="hidden" name="Tipologia" value="<%=sTipRegola%>">
	</form>

	<script>
		alert("Operazione correttamente effettuata");
		frmIndietro.submit()
	</script>
	<%

end sub

'-------------------------------------------------------------------------------------------------------------------------------

sub Inizio()
%>
	<table border="0" width="520" cellspacing="0" cellpadding="0" height="81">
	   <tr>
	     <td width="500" background="<%=Session("Progetto")%>/images/titoli/strumenti2b.gif" height="81" valign="bottom" align="right">
	       <table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
	         <tr>
	           <td width="100%" valign="top" align="right">
	           <b class="tbltext1a">Gestione Regole</b></td>	         
			</tr>
	       </table>
	     </td>
	   </tr>
	</table>

	<br>
	<table cellpadding="0" cellspacing="0" width="500" border="0">
		<tr height="18">
			<td class="sfondomenu" height="18" width="67%">
			<span class="tbltext0"><b>&nbsp;GESTIONE REGOLE</b></span></td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
			<td valign="middle" align="right" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1">(*) campi obbligatori</td>
		</tr>
		<tr width="371" class="SFONDOCOMM">
			<td colspan="3">Testo prova<a href="Javascript:Show_Help('/Pgm/help/GestProgetti/Alfabeta/Regole/CRR_CnfCriterio')" onmouseover="javascript:window.status=' '; return true"><img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
			</td>
		</tr>
		<tr height="2">
			<td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>
	<br>
<%

end sub

'------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

sub GetParam()

	sAction		= Request("Action")
	sIdBando	= UCase(Request("Bando"))
	Response.Write sIdBando & "ssssss"
	sIdRegola	= Request("IdReg")
	sTipRegola	= Request("txtTipRegola")
	sPrgRegola	= Request("txtPrgRegola")
	sValore		= Request("txtValore")
	sTabRif		= Request("txtTabRif")
	sRegola		= Request("txtRegola")
	sDescRegola = Request("txtDescRegola")
	sCodRegola	= Request("txtCodRegola")

	sIdRegola	= UCase(Replace(sIdRegola, "'", "''"))
	sTipRegola	= UCase(Replace(sTipRegola, "'", "''"))
	sPrgRegola	= UCase(Replace(sPrgRegola, "'", "''"))
	sValore		= UCase(Replace(sValore, "'", "''"))
	'sTabRif		= UCase(Replace(sTabRif, "'", "''"))
	sTabRif		= UCase(sTabRif)
	'sRegola		= UCase(Replace(sRegola, "'", "''"))
	sRegola     = Ucase(sRegola)
	sDescRegola = UCase(Replace(sDescRegola, "'", "''"))
	sCodRegola	= UCase(Replace(sCodRegola, "'", "''"))

	if sCodRegola = "" then
		sCodRegola = " " 
	end if

end sub

'------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

function ControllaAcq()

	'controllo che il bando indicato non sia attivo
	if sIdBando=0 then
		sSQLBand = "SELECT COUNT(ID_BANDO) AS CONTATORE FROM BANDO a, PROGETTO B " &_
					"WHERE A.ID_PROJ=B.ID_PROJ " &_
					" and AREA_WEB = '" & MID(SESSION("Progetto"),2) & "'" &_
					" AND " & ConvDateToDB(Now()) &_
					" BETWEEN DT_INI_ACQ_DOM AND DT_FIN_ACQ_DOM"
	else
		sSQLBand = "SELECT COUNT(ID_BANDO) AS CONTATORE FROM BANDO " &_
					"WHERE ID_BANDO=" & sIdBando & " AND " & ConvDateToDB(Now()) &_
					" BETWEEN DT_INI_ACQ_DOM AND DT_FIN_ACQ_DOM"
	end if
	'Response.Write sSQLBand
						
'PL-SQL * T-SQL  
SSQLBAND = TransformPLSQLToTSQL (SSQLBAND) 
	set rsBando = CC.Execute(sSQLBand)

	ControllaAcq = rsBando("Contatore")

	rsBando.Close
	set rsBando = nothing

end function

'------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

sub Inserisci()

	'if clng(ControllaAcq()) <> 0 then
		'Msgetto("Al momento � in corso la fase di acquisizione delle domande.")
		'exit sub
	'end if 

	if sTipRegola = "S" then
		sValore = "NULL"
		sCodRegola = "'" & sCodRegola & "'"
	else
		sCodRegola = "''"
	end if	
	
	if clng(sIdBando)=0 then
		sIdBando="null"
	end if
	
	sSQL = "INSERT INTO REGOLE " &_
		"(TIPO_REGOLA," &_
		"ID_BANDO," & _
		"PRG_REGOLA," &_
		"TAB_RIF," &_
		"REGOLA," &_
		"DESC_REGOLA," &_
		"COD_REGOLA," &_
		"VALORE," &_
		"DT_TMST)" &_
		"VALUES (" &_
		"'" & sTipRegola & "'," &_
		sIdBando & _
		"," & sPrgRegola & "," &_
		"'" & sTabRif & "'," &_
		"'" & sRegola  & "'," &_
		"'" & sDescRegola & "'," &_
		sCodRegola & "," & _
		sValore & "," &_
		ConvDateToDb(Now()) & ")"

	Response.Write sSQL
				
	Errore=Esegui("","REGOLE",Session("idutente"),"INS", sSQL, 0,Now())

	if Errore <>  "0" then
		Msgetto("Errore Durante l'inserimento della regola." & Errore)
	else
		TornaIndietro()
	end if 

end sub

'------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

sub Modifica()

	sDtTmst	= Request("dtTmst")

	'if clng(ControllaAcq()) <> 0 then
	'	Msgetto("Al momento � in corso la fase di acquisizione delle domande.")
	'	exit sub
	'end if 

	if sTipRegola = "S" then
		sValore = "NULL"
	else
		sCodRegola = ""
	end if	
	
	if sIdBando=0 then
		sSQL = "UPDATE REGOLE SET " &_
				"TIPO_REGOLA='"	& sTipRegola & "'," &_
				"PRG_REGOLA="	& sPrgRegola & "," &_
				"TAB_RIF='"		& sTabRif & "'," &_
				"REGOLA='"		& sRegola  & "'," &_
				"DESC_REGOLA='" & sDescRegola & "'," &_
				"VALORE="		& sValore & "," &_
				"COD_REGOLA='"	& sCodRegola & "'," &_
				"DT_TMST="		& ConvDateToDb(Now())& _
				" WHERE ID_REGOLE = " & sIdRegola 
	else
		sSQL = "UPDATE REGOLE SET " &_
				"TIPO_REGOLA='"	& sTipRegola & "'," &_
				"ID_BANDO="	& sIdBando & "," & _
				"PRG_REGOLA="	& sPrgRegola & "," &_
				"TAB_RIF='"		& sTabRif & "'," &_
				"REGOLA='"		& sRegola  & "'," &_
				"DESC_REGOLA='" & sDescRegola & "'," &_
				"VALORE="		& sValore & "," &_
				"COD_REGOLA='"	& sCodRegola & "'," &_
				"DT_TMST="		& ConvDateToDb(Now())& _
				" WHERE ID_REGOLE = " & sIdRegola 
	end if
	
	'Response.Write sSQL
		
	Errore=Esegui("ID_REGOLA=" & sIdRegola ,"REGOLE" ,Session("idutente"), "MOD", sSQL, 0,sDtTmst)

	if Errore <>  "0" then
		Msgetto("Errore Durante la modifica della regola." & Errore)
	else
		TornaIndietro()
	end if 

end sub

'------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

function ControllaRegola()

	ControllaRegola = "0"
	err.number="0"

	'controllo che la regola indicata sia corretta
	sSQL = "SELECT 1 FROM " & sTabRif & " WHERE " & sRegola & " AND ROWNUM<2"
	'Response.Write sSQL
	
	on error resume next	
	
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	set rsProva = CC.Execute(sSQL)	
	
	if err.number <> "0" then
		ControllaRegola = "Errore"
	end if

end function

'------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

sub Fine()
%>
	<br>
	<br>
	<table border="0" cellpadding="0" cellspacing="1" width="500">
		<tr>
			<td align="center">
				<a href="javascript:history.back()">					<!--img src="<%=session("Progetto")%>/images/indietro.gif" border="0" name="imgPunto2" onmouseover="javascript:window.status=' '; return true">				</a-->
				
					<img src="<%=session("Progetto")%>/images/indietro.gif" border="0" name="imgPunto2" onmouseover="javascript:window.status=' '; return true">
				</a>
			</td>
		</tr>
	</table>
	<br>
<!--#include virtual = "/include/closeconn.asp"-->
<!--#include virtual = "strutt_coda2.asp"-->
<%
end sub

%>
<!-- ************** ASP Fine *************** -->

<!-- ************** MAIN Inizio ************ -->
<%

'		frmModCriterio.txtTipRegola		- Alfa di 1 -
'		frmModCriterio.txtIdBando		- Alfa di 3 -
'		frmModCriterio.txtPrgRegola		- Numerico - 
'		frmModCriterio.txtTabRif		- Alfa di 250 -
'		frmModCriterio.txtRegola		- Alfa di 250 -
'		frmModCriterio.txtValore		- Numerico -
'		frmModCriterio.txtDescRegola	- Alfa di 50 -
'		frmModCriterio.txtCodRegola		- Alfa di 2 -

dim sAction
dim sSQL

dim sDtTmst

dim sIdRegola
dim sIdBando

dim sTipRegola
dim sPrgRegola
dim sTabRif
dim sRegola
dim sValore
dim sCodRegola
dim sDescRegola

'	Inizio()
	
	'reperisco i dati inseriti nella form precedente
	GetParam()

	if ControllaRegola() = "0" then
		
		sRegola = Replace(sRegola,"'","''")

		select case sAction
				
			case "Ins"
				Inserisci()
			
			case "Mod" 
				Modifica()
		end select
	else		
		Msgetto("Errore nella verifica della regola.<BR>" & _
				"Verificare che la sintassi della regola sia corretta")
	end if
	
	Fine()
	
%>
<!-- ************** MAIN Fine ************ -->
