<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<html>
<head>
<LINK REL=STYLESHEET TYPE="text/css" HREF='<%=session("progetto")%>/fogliostile.css'>
<%'Option Explicit 
   Response.ExpiresAbsolute = Now() - 1 
   Response.AddHeader "pragma","no-cache"
   Response.AddHeader "cache-control","private"
   Response.CacheControl = "no-cache"
%>
<!--#include virtual = "/include/openconn.asp"-->
<!--#include virtual="/util/portallib.asp"-->
<%
if ValidateService(session("idutente"),"CRR_VISCRITERIO", CC) <> "true" then 
	response.redirect "/util/error_login.asp"
end if
%>

<!-- ************** Javascript inizio ************ -->

<SCRIPT LANGUAGE="Javascript">
//include del file per fare i controlli sulla validit� delle date
<!--#include virtual = "/Include/ControlDate.inc"-->

//include del file per fare i controlli sulla numericit� dei campi
<!--#include virtual = "/Include/ControlNum.inc"-->
<!--#include virtual = "/Include/ControlString.inc"-->

function ConvJavaStringToDate(sData) {
		
	var sGiorno
	var sMese
	var sAnno

	if (sData.charAt(0) == "0")
		sGiorno = sData.charAt(1)
	else
		sGiorno = sData.substr(0,2)
			
	if (sData.charAt(3) == "0")
		sMese = sData.charAt(4)
	else
		sMese = sData.substr(3,2)

	if (sData.charAt(6) == "0")
		sAnno = sData.charAt(7)
	else
		sAnno = sData.substr(6,4)

	sDataConv = sMese + "/" + sGiorno + "/" + sAnno 
	return sDataConv
}
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function Seleziona(nElem){
   
	nSelRic	= document.frmCreaRegole.elements[nElem].selectedIndex
	sSelRic = document.frmCreaRegole.elements[nElem].options[nSelRic].value

	window.resizeTo(340,450)

	document.frmCreaRegole.txtSel.value = sSelRic
	sSelRic = "&Valore=" + sSelRic
	Campi(sSelRic)

}
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function Ric(){
//	alert (frmCreaRegole.txtDecod.value)
	Campi("&Risultato=" + frmCreaRegole.txtDecod.value + "&Decod=YES")
	window.resizeTo(340,550)
}
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function RicCod(){
	Campi("&Decod=YES")
	window.resizeTo(340,500)
}
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function Diverso() {
	//prendo l'operatore logico [Maggiore,Minore,Uguale,Diverso]
	nOperLoc = document.frmCreaRegole.elements[2].selectedIndex
	sOperLoc = document.frmCreaRegole.elements[2].options[nOperLoc].value

	if (sOperLoc == "BETWEEN") {
		alert ("ricarico la pagina con due campi per le date")
		Campi("Yes")
	}
	else 
		Campi("No")
}
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function ControllaDati(sValore,sTipo){
	
	if (sValore == "") {
		return false
	}
	
	if (sTipo == "N") {
		if (!IsNum(sValore)) {
			alert("Il campo deve essere numerico")
			return false
		}		 
	}

	if (sTipo == "D") {
		if (!ValidateInputDate(sValore)) {
			return false
		}
	}
	return true
}	
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function Canc() {
	opener.document.frmModCriterio.txtRegola.value = ""
	opener.document.frmModCriterio.txtTabRif.value = ""
}
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function Uscita() {
		this.close()
}
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function selTabella() {
	var nSel
	nSel = document.frmCreaRegole.elements[0].selectedIndex
	nOper = document.frmCreaRegole.elements[2].selectedIndex
	window.resizeTo(340,450)
	document.location.href ="CRR_CreaRegola.asp?prVolta=No&Tabella=" + 
		document.frmCreaRegole.elements[0].options[nSel].value + "&OpeLog=" +
		document.frmCreaRegole.elements[2].options[nOper].value
}
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function Campi(sCompresa) {
	var nSelTab
	var nSelCampo
	var sDecod
	
	nSelTab		= document.frmCreaRegole.elements[0].selectedIndex
	nSelCampo	= document.frmCreaRegole.elements[1].selectedIndex
	nOper  = document.frmCreaRegole.elements[2].selectedIndex
   
	if (sCompresa == "0") {
		window.resizeTo(340,500);
		
		document.location.href ="CRR_CreaRegola.asp?prVolta=No&Tabella=" + 
			document.frmCreaRegole.elements[0].options[nSelTab].value + 
			"&Campo=" + document.frmCreaRegole.elements[1].options[nSelCampo].value +
			"&OpeLog=" +	document.frmCreaRegole.elements[2].options[nOper].value

	}else { 
		//window.resizeTo(340,500);
		document.location.href ="CRR_CreaRegola.asp?prVolta=No&Tabella=" + 
			document.frmCreaRegole.elements[0].options[nSelTab].value + 
			"&Campo=" + document.frmCreaRegole.elements[1].options[nSelCampo].value + sCompresa +
			"&OpeLog=" + document.frmCreaRegole.elements[2].options[nOper].value

	}
		//"&Between=" + sCompresa
}
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function Condizione(sSeparatore) {

	var nSelCampo
	var nOperLoc
	var sNomCampo
	var sOperLoc
	var sValore
	var nSelTab	
	var sSelTab 

	var sTabRif
	var arNomTab = new Array()
	
	if (document.frmCreaRegole.txtSel.value == ""){
		alert("Occorre insidare un valore di condizione")
		document.frmCreaRegole.txtSel.focus()
		return false
	}
	
	//prendo della tabella
	nSelTab	= document.frmCreaRegole.elements[0].selectedIndex
	sSelTab = document.frmCreaRegole.elements[0].options[nSelTab].value
	
	//prendo il campo della tabella
	nSelCampo = document.frmCreaRegole.elements[1].selectedIndex
	sNomCampo = document.frmCreaRegole.elements[1].options[nSelCampo].value
	
	//prendo l'operatore logico [Maggiore,Minore,Uguale,Diverso]
	nOperLoc = document.frmCreaRegole.elements[2].selectedIndex
	sOperLoc = document.frmCreaRegole.elements[2].options[nOperLoc].value

	if (sOperLoc == "") {
		alert ("Occorre indicare una condizione")
		document.frmCreaRegole.elements[2].focus()
		return false
	}
	
	//prendo l'operatore logico [AND,OR]
	nOperAndOr = document.frmCreaRegole.elements[3].selectedIndex
	sOperAndOr = " " + document.frmCreaRegole.elements[3].options[nOperAndOr].value


	//prendo il valore digitato
	sValore = document.frmCreaRegole.txtSel.value
	
	// Devo controllare che i dati indicati siano corretti.
	// Se il campo � una data occorre controllare che sia stata indicata una data
	// Se il campo � un numero occorre controllare la validit�.
	// Se non si indica nulla occorre avvertire l'utente che deve indicare un valore
	if (ControllaDati(sValore,sSeparatore)){
	
		// Controllo il tipo del dato ed aggancio al valore 
		// la codifica che SQL indica.
		if (sSeparatore == "N") {
			sSeparatore = ""
		}
		if (sSeparatore == "D") {
		// Trasformo la data in una data valida
			sValore = ConvJavaStringToDate(sValore)
			sSeparatore = "#"
		}
		if (sSeparatore == "A") {
			sSeparatore = "'"
		}

		sValore = sSeparatore + sValore + sSeparatore
		
		// Attacco al nome campo il Nome tabella.
		// Questo per evitare il problema di campi 
		// che hanno lo stesso nome ma presenti
		// in tabelle diverse (solo per campi che non siano calcolati).
		
		if (ChechSingolChar(sNomCampo,'(')) {
			sNomCampo = sSelTab + "." + sNomCampo
		}
									
		if (opener.document.frmModCriterio.txtRegola.value != "") {
			sCondizione	= sOperAndOr + " " + 
							sNomCampo + " " +  
							sOperLoc + " " + sValore
		}
		else {
			sCondizione	= sNomCampo + " " +  
							sOperLoc + " " + sValore
		}
		opener.document.frmModCriterio.txtRegola.value = opener.document.frmModCriterio.txtRegola.value +  
				sCondizione 

		// Oltre ad aggiungere la regola la procedure deve aggiungere anche 
		// le tabelle di interesse nel campo 'Tabelle di riferimento'. 
		// Mi devo prendere il valore contenuto nel campo 'Tabelle di riferimento'
		// e controllare se quella che st� aggiungendo gi� � presente.
		// Prendo il valore della regola dalla form.
		sTabRif = opener.document.frmModCriterio.txtTabRif.value 
		t=0
		arNomTab[0] = ""
		// Controllo la lunghezza della riga
		for (i = 0;i<sTabRif.length;i++){ 
			if (sTabRif.charAt(i) == ",") {
				t=t+1
				i=i+1
				arNomTab[t] = ""
			}
			//Aggiungo i valori nell'array
			arNomTab[t] = arNomTab[t] + sTabRif.charAt(i)		
		}
		
		nExist = 0
		//Se uno dei valori � presente nell'array non aggiungo la nuova tabella
		for (i=0;i<arNomTab.length;i++){
			if (arNomTab[i] == sSelTab) 
				nExist = 1
		}

		if (nExist != 1) { // Controllo se la tabella era gi� presente
			if (sTabRif.length != 0) // Se nel campo txtTabRif � presente un valore aggiungo la virgola.
				sSelTab = "," + sSelTab
			opener.document.frmModCriterio.txtTabRif.value = opener.document.frmModCriterio.txtTabRif.value +  
				sSelTab
		}
	}
}
</Script>

<!-- ******************  Javascript Fine *********** -->

<!-- ************** ASP inizio *************** -->
<!--#include virtual = "/include/DecCod.asp"-->
<!--#include virtual = "/include/ValDomIsc.asp"-->
<!--#include virtual = "/include/SysFunction.asp"-->
<%

'----------------------------------------------------------------------------------------------------------------------------------------------------  

sub ComponiCmbTabelle()
    dim sSelezione 
%>
    <SELECT class="textblack" name=cmbTabelle onchange='JAVASCRIPT:selTabella()'> 
 <%   
       do while not rsTabelle.EOF
             if sTabella = rsTabelle("ID_TAB") then
					sSelezione = " selected "
	    	 else	
					sSelezione =" "
		     end if
%>
             <OPTION <%=sSelezione%> value=<%=rsTabelle("ID_TAB")%>><%=rsTabelle("ID_TAB")%></OPTION>
<%
             rsTabelle.MoveNext 
      loop
      rsTabelle.Close 
%>
    </Select>
<%
end sub

'----------------------------------------------------------------------------------------------------------------------------------------------------  

Sub ComponiCmbCampiTabella()

    sSQL = "Select ID_TAB,ID_CAMPO,DESC_CAMPO,LEN_CAMPO," &_
	       "ID_CAMPO,TIPO_CAMPO,ID_TAB_RIF,ID_CAMPO_DESC,ID_CAMPO_RIF,ID_DESC " & _
	       "FROM DIZ_DATI WHERE ID_TAB='" & sTabella & "'"
	
    'Response.Write sSQL
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
    set rsSel = CC.Execute(sSQL)
%>
    <Select class="textblack" name='cmbCampi' onchange='JAVASCRIPT:Campi(0)'> 
<%
	sDesCampo = rsSel("DESC_CAMPO")
	sLenCampo = rsSel("LEN_CAMPO")
	sTipCampo = rsSel("TIPO_CAMPO")
	sIdCampo =	rsSel("ID_CAMPO")
	sTipoSorg = rsSel("ID_TAB_RIF")
	sIDCampoRif = rsSel("ID_CAMPO_RIF")
	sDesCampoRif= rsSel("ID_CAMPO_DESC")

	sIdDesc = rsSel("ID_DESC")

	dim SelezioneCampo
	do while not rsSel.EOF
		if sCampo = rsSel("ID_CAMPO") then
			sIdCampo =	rsSel("ID_CAMPO")
			sDesCampo = rsSel("DESC_CAMPO")
			sLenCampo = rsSel("LEN_CAMPO")
			sTipCampo = rsSel("TIPO_CAMPO")
			sTipoSorg = rsSel("ID_TAB_RIF")
			sIDCampoRif = rsSel("ID_CAMPO_RIF")
			sDesCampoRif= rsSel("ID_CAMPO_DESC")
	 
			sIdDesc = rsSel("ID_DESC")
	 
			SelezioneCampo =" selected "
		else
			SelezioneCampo =" "
		end if 
%>
		<OPTION <%=SelezioneCampo%> value='<%=rsSel("ID_CAMPO")%>'><%=rsSel("DESC_CAMPO")%></OPTION>
<%
		rsSel.MoveNext 
	loop
	rsSel.Close 
%>
    </Select>
<%
end sub

'----------------------------------------------------------------------------------------------------------------------------------------------------  

sub ComponiCmbCondizioniLogiche()
    dim sSeleziona 
    if sOpeLog = ">"  OR  sOpeLog = "<" OR sOpeLog = "=" OR sOpeLog = "<>" then
       sSeleziona=" selected "
    else
       sSeleziona=" "
    end if
 %> 
      <SELECT class="textblack" name=cmbCondizione > <!--onchange='JAVASCRIPT:Diverso()'>-->
			<OPTION value=">"  <%=sSeleziona%>>Maggiore</OPTION>
			<OPTION value="<"  <%=sSeleziona%>>Minore</OPTION> 
			<OPTION value="="  <%=sSeleziona%>>Uguale</OPTION>
			<OPTION value="<>" <%=sSeleziona%>>Diverso</OPTION>
<%
'	if sTipCampo = "D" then
%><!--			<OPTION value="BETWEEN">Compresa</OPTION>  -->
<%
'	end if 
%>
	  </SELECT>
	  <SELECT class="textblack" name=cmbCon >
	      <OPTION value='AND' selected>AND</OPTION>
    	  <OPTION value='OR'>OR</OPTION>
  	  </SELECT>
<%  
end sub

'----------------------------------------------------------------------------------------------------------------------------------------------------  

sub CaratteristicheCampoTabella()
%>    
    <table width="300" cellpadding=2 cellspacing=2 border=0 class="tblsfondo">
		<tr class="tbltext">
			<td width="30%" align=left nowrap>
				<b>Nome campo:</b>
			</td>
			<td class="tblText" width="70%" align=left>
				<%=sIdCampo%>
			</td>
		</tr>
		<tr class="tbltext">
			<td width="30%" align=left>
				<b>Lun.&nbsp;del&nbsp;campo:</b>
			</td>
			<td width="70%" align=left>
			<%	
			select case  sTipCampo 
				case "D"					
					Response.Write "10"
				case "N"
					Response.Write "11"
				case else
					Response.Write sLenCampo
			end select
			%>
			</td>
		</tr>
		<tr class="tbltext">
			<td width="30%" align=left>
				<b>Tipo&nbsp;campo:</b>
			</td>
			<td width="70%" align=left>
			<%select case sTipCampo
				case "D"
					Response.Write "Data (gg/mm/aaaa)"
				case "A"
					Response.Write "Alfanumerico"
				case "N"
					Response.Write "Numerico"
			end select
			%>
			</td>
		</tr>

	<%
	if sTipoSorg <> "" then%>
		<TR class="tblsfondo">
			<td width="30%" align=left class="tbltext">
				<b>Tab.&nbsp;riferimento:</b>
			</td>
			<td width="70%" align=left class="tbltext">
				<A  onclick="RicCod()">
				<FONT color=red> 
				<%
				if  mid(sTipoSorg,1,3) = "TAB" then
					Response.Write ("<U>" & mid(sTipoSorg,5) & "</U>")
				else
					Response.Write ("<U>" & sTipoSorg & "</U>")
				end if
				%>
				</Font>
				</A>
			</td>
		</TR>
		<TR>
			<td width="30%">&nbsp;</td>
			<td width="70%" align=Left class="tbltext">
			    <a href=JavaScript:RicCod()  class="textred"><b>Decodifica</b></a>
				<input type="hidden" name="RicercaD" value="Decod" onclick="RicCod()">
			</td>
		</TR>
		<%
	else 
		if sDesCampoRif <> "" then 
			sTipoSorg = "Combo"
		%>
		<tr>
			<td width="30%">&nbsp;</td>
			<td width="70%" align=Left  class="tbltext">
			    <a href=JavaScript:RicCod()  class="textred"><b>Decodifica</b></a>
				<input type="hidden" name="RicercaD" value="Decod" onclick="RicCod()">
			</td>
		</tr>
<%             
		end if 
	end if 
%>
	</table>
<%
end sub

'----------------------------------------------------------------------------------------------------------------------------------------------------  

sub LetturaParametri()

'    sSQL = "Select " & sIDCampoRif & "," & sDesCampoRif &_
'			" FROM " & sTipoSorg & _
'			" WHERE " & sDesCampoRif &_
'			" Like '%" & sRisultato & "%'"

    sSQL = "Select " & sIDCampoRif & "," & sIdDesc &_
			" FROM " & sTipoSorg & _
			" WHERE " & sIdDesc &_
			" Like '%" & sRisultato & "%'"
	'Response.Write sSql					  

'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
    set rsSel = CC.Execute(sSQL)
    
    if rsSel.EOF = true then
  		%>
  		<BR><BR>
  		<span class="tbltext1"><b><CENTER>Non esistono codici per la regola indicata</b></CENTER></span>
		<%   
		exit sub
    end if
    
	'Response.Write sCampo & "---"
	'Response.Write sIDCampoRif & "---"
	'Response.Write sIdDesc & "---"
  
	%>  
    <span class="tbltext1"><b><Center>Selezionare il codice desiderato</span></Center></b>
  		<CENTER>
  		<!--Select name='cmbRis' onchange='JAVASCRIPT:Seleziona(11)' -->
  		<Select class="textblack" name='cmbRis' onchange='JAVASCRIPT:Seleziona(9)'>
  		<OPTION selected></Option>
<%		
  		do while not rsSel.EOF

  			Response.Write "<OPTION "

  			if trim(cstr(sCampo)) = trim(cstr(rsSel(sIDCampoRif))) then
  				Response.Write " selected "
  			end if

  			Response.write "value ='" & rsSel(sIDCampoRif) &_
  					"'> " & rsSel(sIDCampoRif) & _
  					" - " & rsSel(sIdDesc) & _
  					"</OPTION>"
  			rsSel.MoveNext 
  		loop
%>			
  		</Select></CENTER></span>	
<%				  
end sub

'----------------------------------------------------------------------------------------------------------------------------------------------------  

sub LetturaTabellaDescrittiva() 

    dim strCondizione, i
    
    sIDCampoRif  = "CODICE"
    sDesCampoRif = "DESCRIZIONE"
			    
    strCondizione="DESCRIZIONE LIKE '%" & sRisultato & "%'"
    aTades= decodTadesToArray(mid(sTipoSorg,5),date,strCondizione,0,"0")
	  
    if isnull(aTades(0,0,0)) then %>
            <BR><BR>
  	    	<span class="tbltext1"><CENTER><b>Non esistono codici per la regola indicata</b></CENTER></span>  
			
 <%         EXIT SUB
    end if %>
    <span class="tbltext1"><b><Center>Selezionare il codice desiderato</span></Center></b>
    <CENTER>
    <!-- Select name='cmbRis' onchange='JAVASCRIPT:Seleziona(11)' -->
    <Select class="textblack" name='cmbRis' onchange='JAVASCRIPT:Seleziona(9)'>
  		<OPTION selected></Option> 
  	<%
  	    for i=0 to ubound(aTades)-1
  	        Response.Write "<OPTION "

  			if sCampo = aTades(i,i,i) then
  				Response.Write " selected "
  			end if
  			Response.write "value ='" & aTades(i,i,i) &_
  					"'> " & aTades(i,i,i) & " - " & aTades(i,i+1,i) & "</OPTION>"
  	    next
  	    erase aTades
  	    
end sub

'----------------------------------------------------------------------------------------------------------------------------------------------------  

  sub ComponiRisultato()
  %>
  	<span class="INPUT">
		<% 
		if mid(sTipoSorg,1,3) = "TAB" then
		       LetturaTabellaDescrittiva() 
		else
			   LetturaParametri()
			   rsSel.Close    
		end if
  end sub

'----------------------------------------------------------------------------------------------------------------------------------------------------  

%>    
<!-- ************** ASP fine   *************** -->

</head>
<body>

<FORM name=frmCreaRegole>
	<table width="300" border=0 cellspacing=2 cellpadding=1>
		<tr> 
			<td colspan=2 align=middle class="tbltext3"><b>Costruzione regole</b></td>
		</tr>
	</table>
	<BR>
<%
dim sOpeLog

dim sConf
dim sPrVolta
dim sTabella
dim sSQL
dim rsSel

dim sDecod
dim sRisultato

dim sTipoSorg 

dim sCampo
dim	sDesCampo 
dim	sLenCampo 
dim	sTipCampo 
dim sIDCampo
dim sIDCampoRif 
dim sDesCampoRif
dim sIdDesc

dim sValore
dim aTades

sPrVolta	= Request("prVolta")
sConf		= Replace(Request("Between"),"'","''")
sDecod		= Replace(Request("Decod"),"'","''")
sRisultato	= Replace(ucase(Request("Risultato")),"'","''")
sValore		= Replace(Request("Valore"),"'","''")
sOpeLog		= Replace(Request("OpeLog"),"'","''")

if sPrVolta <> "No" then
	sTabella = "PERSONA"
else
	sTabella = Request("Tabella")
	sCampo = Request("Campo")
end if

'tabelle su cui posso applicare le regole
sSQL = "SELECT DISTINCT ID_TAB FROM DIZ_DATI ORDER BY ID_TAB"
	
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
set rsTabelle = CC.Execute(sSQL)
%>
	<table width=300 cellpadding=1 cellspacing=0>
		<tr align=left class='tbltext1'>
			<td><b>Tabelle</b>
			</td>
		</tr>
		<tr align=left>
			<td><%ComponiCmbTabelle()%>
			</td>
		</tr>
		<tr align=left class="tbltext1">
			<td><b>Campi della tabella selezionata</b>
			</td>
		</tr>
		<tr>
			<td><%ComponiCmbCampiTabella()%></td>
		</tr>
	</table>
	<HR>
	<table width=300 cellpadding=1 cellspacing=0>
		<tr align=left class='tbltext1'>
			<td align=center><b>Condizioni logiche</b>
			</td>
		</tr>
		<tr>
			<td ALIGN=CENTER><%ComponiCmbCondizioniLogiche()%>
			</td>
		</tr>
	</table>
	<HR>
	
<%
CaratteristicheCampoTabella()
%>

<!--------------------------------------------------------------------------->
<%if sConf = "Yes" then%>
	 <B class='tbltext1'>fra</B> <input class="textblack" type="text" name="txtData1" size=8 maxlength=10>
	 <B class='tbltext1'> a</B>  <input class="textblack" type="text" name="txtData2" size=8 maxlength=10>
<%else%>
	<B class='tbltext1'>Valore</B> <input class="textblack" type="text" size=15 name="txtSel" value="<%=sValore%>">
<%end if%>

	<input type="button" name="Ok" value="Ok" onclick="JAVASCRIPT:Condizione('<%=sTipCampo%>')">
	<BR><BR>
	<div align=center>
    <a href="JAVASCRIPT:Uscita()" class="textred"><b>Chiudi Finestra</b></a>
	<!--input type="button" name="Esci" value="Esci" onclick='JAVASCRIPT:Uscita()'-->
	&nbsp;&nbsp;&nbsp;
	<a href="javascript:Canc()" class="textred"><b>Azzera Regola</b></a>
	<!--input type="button" name="Cancella" value="Azzera regola" onclick='JAVASCRIPT:Canc()'-->
	</div>
	<HR>

<%if sDecod = "YES" and sTipoSorg <> "Combo" then%>
	<span class="tbltext1"><b>Indicare una breve descrizione</span></b>
	<BR>
	<input type="text" class="textblack" name="txtDecod" style="TEXT-TRANSFORM: uppercase;" size=15 maxlength=15 value="<%=sRisultato%>">
	<input type="button" class="textblack" name="ritrova" value="Ricerca" onclick='JAVASCRIPT:Ric()'>
<%
 end if 	
 
 if  sTipoSorg = "Combo" and sDecod ="YES" then
  ' Costruisco la combo con i soli valori
%>
		<span class="tbltext1"><b><Center>Selezionare il codice desiderato</span></Center></b>
<%	
		dim sValoriArray
		sValoriArray = Split(sDesCampoRif, "|", -1, 1)
%>		
		<Center>
			<!-- Select name='cmbRis' onchange='JAVASCRIPT:Seleziona(9)' --> 
			<Select class="textblack" name='cmbRis' onchange='JAVASCRIPT:Seleziona(7)'> 
			<OPTION selected>
			</Option>
<%
		for z = 0 to (UBound(sValoriArray)-1)
			Response.Write ("<Option value='" & sValoriArray(z) & "'>" & sValoriArray(z+1) & "</Option>")
			z=z+1
		next
%>
			</Select>
		</Center> 
<%
end if

if sRisultato <> "" then
   ComponiRisultato()
end if 	

set rsTabelle = nothing
set rsSel = nothing
%>

</FORM>

</body>
</html>
