<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #include virtual="/strutt_testa2.asp"-->
<!--#include virtual = "/util/portallib.asp"-->
<!--#include virtual = "include/openconn.asp"-->
<!--#include virtual = "include/ControlDateVB.asp"-->
<!--#include virtual = "include/DecCod.asp"-->
<!--#include virtual = "util/dbutil.asp"-->
<!--#include virtual = "/include/SelAreaTerrBandi.asp"-->
<%
if ValidateService(session("idutente"),"EDI_VISEDIZIONE",cc) <> "true" then 
	response.redirect "/util/error_login.asp"
end if	
'Response.Write Session("MASK")
Session("Diritti")=Session("MASK")
%>
<html>
<head>
<title>Gestione Sessione </title>

<script language="Javascript">
//include del file per fare i controlli sulla validit� delle date
<!--#include virtual = "/Include/ControlDate.inc"-->

//include del file per fare i controlli sulla numericit� dei campi
<!--#include virtual = "/Include/ControlNum.inc"-->
<!--#include virtual = "/Include/help.inc"-->

function ControllaDati(MyForm){
	var nNumElem
	var sValue
	var nNumSessioni
    var dtIniEdi
	var dtFinEdi
	    
	nNumSessioni = document.frmCfnEdizione.txtNumRighe.value * 7;
	nNumSessioni = nNumSessioni + 2;
     
	for (z=0;z<nNumSessioni;z++){
	    var sNome
	    var data1
	    var data2
	    var indfase
	    sNome = document.frmCfnEdizione.elements[z].name;
	    sNome =sNome.substr(0,9);
	    if (sNome== "txtSesIni"){
		    if(document.frmCfnEdizione.elements[z].value==""){
				alert("La data inizio sessione � obbligatoria")
			    document.frmCfnEdizione.elements[z].focus();
				return false
		    }
		    if (!ValidateInputDate(document.frmCfnEdizione.elements[z].value)){
				document.frmCfnEdizione.elements[z].focus(); 
				return false
			}
	//		if ((!ValidateRangeDate(dtIniEdi,document.frmCfnEdizione.elements[z].value))){
	//			alert("La Data Inizio deve essere superiore alla data di inizio edizione")
	//			document.frmCfnEdizione.elements[z].focus(); 
	//			return false
	//		}
			if(z==5){
			   data1=document.frmCfnEdizione.elements[z].value;
			    indfase=document.frmCfnEdizione.elements[z-4].value;
			}
			else{
			    if(document.frmCfnEdizione.elements[z-4].value == indfase){
			        data2 =document.frmCfnEdizione.elements[z].value;
			        if(data1 != data2){
			            alert("La data inizio sessione deve essere uguale alla precedente")
			            document.frmCfnEdizione.elements[z].focus();
				        return false
		            }
		        }    
		        else{
		            if ((!ValidateRangeDate(data1,document.frmCfnEdizione.elements[z].value))){
   				          alert("La Data Inizio deve essere successiva a quella della Sessione precedente")
				          document.frmCfnEdizione.elements[z].focus(); 
				          return false
			        }
		            data1 =document.frmCfnEdizione.elements[z].value;
		            indfase=document.frmCfnEdizione.elements[z-4].value;
		        }
			}
	    }
	    if (sNome== "txtSesFin"){
		    if(document.frmCfnEdizione.elements[z].value==""){
				alert("La data fine sessione � obbligatoria")
			    document.frmCfnEdizione.elements[z].focus();
				return false
		    }
		    if (!ValidateInputDate(document.frmCfnEdizione.elements[z].value)){
				document.frmCfnEdizione.elements[z].focus(); 
				return false
			}
	//		if ((!ValidateRangeDate(document.frmCfnEdizione.elements[z].value,dtFinEdi))){
	//			alert("La Data fine non deve essere superiore alla data di fine edizione")
	//			document.frmCfnEdizione.elements[z].focus(); 
	//			return false
	//		}
			if ((!ValidateRangeDate(document.frmCfnEdizione.elements[z-1].value,document.frmCfnEdizione.elements[z].value))){
				alert("Data Fine minore della Data Inizio")
				document.frmCfnEdizione.elements[z].focus(); 
				return false
			}
	    }
	}    
	return true
}		
</script>
</head>
<body>

<form name="frmCfnEdizione" method="post" onsubmit="return ControllaDati(this)" action="EDI_CfnEdizione.asp">
	<table border="0" width="520" cellspacing="0" cellpadding="0" height="81">
	   <tr>
	     <td width="500" background="<%=Session("Progetto")%>/images/titoli/strumenti2b.gif" height="81" valign="bottom" align="right">
	       <table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
	         <tr>
	           <td width="100%" valign="top" align="right"><b class="tbltext1a">Gestione Progetti</span></b></td>
	         </tr>
	       </table>
	     </td>
	   </tr>
	</table>
	<br>
	<table cellpadding="0" cellspacing="0" width="500" border="0">
		<tr height="18">
			<td class="sfondomenu" height="18" width="67%">
			<span class="tbltext0"><b>&nbsp;GESTIONE SESSIONI</b></span></td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
			<td valign="middle" align="right" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1"></td>
		</tr>
		<tr width="371" class="SFONDOCOMM">
			<td colspan="3">
				Inserimento delle sessioni che compongono l'edizione.
				<a href="Javascript:Show_Help('/Pgm/help/Gestprogetti/alfabeta/edizione/EDI_InsEdizione')">
		<img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
			</td>
		</tr>
		<tr height="2">
			<td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>
	<br>
<%
dim sConferma
dim sEdizione
dim sbando

sConferma = false
sbando = Request.Form ("idBando")

dim z
dim sSQL

sSQL="SELECT DESC_FASE,FB.COD_SESSIONE,FB.NUM_ORESESS,PRG_FASE," &_
     "COD_TFASE FROM FASE_BANDO FB WHERE FB.ID_BANDO=" & sbando &_
     " ORDER BY PRG_FASE,FB.COD_SESSIONE"      

set rsSesFrm = Server.CreateObject("ADODB.RecordSet")

'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
rsSesFrm.Open sSQL,CC,3
	
z=0
	
if not rsSesFrm.EOF then
    sConferma = true
%>
	<br>
	<table border="0" cellpadding="0" cellspacing="0" width="500">
		<tr>
			<td class="tbltext1" width="20%">
				<b>Bando</b>
			</td>
			<td class="tbltext">
				<b><%=DecBando(sbando)%></b>
			</td>
		</tr>
	</table>
	<br>
	<table border="0" cellspacing="2" cellpadding="1" width="500">
		<tr class="sfondocomm"> 
			<td align="middle" valign="center"><b>SESSIONE&nbsp;&nbsp;</b></td>
		    <td align="middle" valign="center"><b>ORE</b></td>
		    <td align="middle" valign="center"><b>DATA&nbsp;INIZIO</b></td>
		    <td align="middle" valign="center"><b>DATA&nbsp;FINE</b></td>
	    </tr>
	        <% 
	  do while not rsSesFrm.EOF
	       
			z=z+1%>
			<input type="hidden" name="txtCodice<%=z%>" value="<%=rsSesFrm("COD_SESSIONE")%>">
			<input type="hidden" name="txtProFase<%=z%>" value="<%=rsSesFrm("PRG_FASE")%>">
			<input type="hidden" name="txtTFase<%=z%>" value="<%=rsSesFrm("COD_TFASE")%>">
			<tr class="tblsfondo">
			   <td>
			       <input class="textblacka" style="TEXT-TRANSFORM: uppercase;" size="45" maxlength="50" name="cmbsessioni<%=z%>" value="<%=rsSesFrm("DESC_FASE")%>" readonly>
		       </td>
			   <td width="30" align="middle">
			       <input class="textblacka" style="TEXT-TRANSFORM: uppercase;" size="3" maxlength="3" name="txtOreSessione<%=z%>" value="<%=rsSesFrm("NUM_ORESESS")%>" readonly>
	    	   </td>
			   <td width="91" align="middle">
			       <input class="textblacka" style="TEXT-TRANSFORM: uppercase;" size="10" maxlength="10" name="txtSesIni<%=z%>" value size="15" maxlength="10">
			   </td>
			   <td width="91" align="middle">
				   <input class="textblacka" style="TEXT-TRANSFORM: uppercase;" size="10" maxlength="10" name="txtSesFin<%=z%>" value size="15" maxlength="10">
			   </td>
			</tr>
			<%
			set rsData = nothing
			rsSesFrm.MoveNext
	  loop
%>	  
	<input type="hidden" name="txtNumRighe" value="<%=z%>">
	<input type="hidden" name="txtBando" value="<%=sbando%>">
</table>
<%
end if
rsSesFrm.Close
set rsSesFrm = nothing
%>	

<br>

<table border="0" cellspacing="0" cellpadding="0" width="500">
	<tr height="40">
		<td align="center" class="tblsfonfo"><span class="tbltext3">
			<% 
			if sConferma then
				%>
				<table cellpadding="0" cellspacing="0" width="300" border="0">
	                  <tr><td>&nbsp;</td></tr>
	                  <tr align="center">
	                     <td nowrap><input type="image" name="Conferma" src="<%=Session("progetto")%>/images/conferma.gif"></td value="Registra">
			             <td nowrap><a href="javascript:history.go(-1)" onmouseover="javascript:window.status=' '; return true"><img src="<%=session("Progetto")%>/images/indietro.gif" border="0" name="imgPunto2"></a><td>
	                  </tr>
	            </table> 
	        <%else%>
	            <table cellpadding="0" cellspacing="0" width="500" border="0">
	                  <tr><td>&nbsp;</td></tr>
	                  <tr align="center">
			             <td class="tbltext3">Non � stato definito il percorso progettuale per il bando selezionato<td>
			          </tr>
			          <tr><td>&nbsp;</tr></td>
			          <tr align="center"> 
			             <td nowrap><a href="javascript:history.go(-1)" onmouseover="javascript:window.status=' '; return true"><img src="<%=session("Progetto")%>/images/indietro.gif" border="0" name="imgPunto2"></a><td>
			          </tr>
	            </table>      
			<%end if%>
		</td>
	</tr>
</table>

</form>
<!--#include virtual = "include/closeconn.asp"-->
<!--#include virtual = "strutt_coda2.asp"-->
