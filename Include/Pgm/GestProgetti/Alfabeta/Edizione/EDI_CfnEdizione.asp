<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #include virtual="/strutt_testa2.asp"-->
<!--#include virtual = "/include/SysFunction.asp"-->
<!--#include virtual = "include/ControlDateVB.asp"-->
<!--#include virtual = "util/dbutil.asp"-->
<!--#include virtual = "/include/openconn.asp"-->

<%
dim Numrighe
dim idEdi
dim dt_ini
dim dt_fin
dim idBando

Numrighe=Request.Form ("txtNumRighe")
idBando = Request.Form ("txtBando")

CC.BeginTrans

sSQL="INSERT INTO EDIZIONE (ID_BANDO,ID_UORG,DT_TMST) VALUES (" &_
     idBando & "," & session("iduorg") & "," & ConvDateToDb(Now()) & ")"
    
Errore =EseguiNoC(sSQL,CC)

if Errore ="0" then 
    sSQL ="SELECT ID_EDIZIONE FROM EDIZIONE WHERE DT_INIZIO IS NULL" &_
          " AND DT_FINE IS NULL"
          
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
    set rsEdizione = cc.execute(sSQL)
    
    if not rsEdizione.eof then
       idEdi = rsEdizione("ID_EDIZIONE")
    else
       Errore = "inserimento non effettuabile"   
    end if   
    
    set rsEdizione = nothing
    
	for i =1 to Numrighe 
	    codses = Request.Form("txtCodice" & i)
	    codses = Replace(server.HTMLEncode(codses), "'", "''")
		dataIni = Request.Form("txtSesIni" & i) 
		dataFine = Request.Form ("txtSesFin" & i)
        codfase = Request.Form ("txtTFase" & i)
        numOre = Request.Form ("txtOreSessione" & i)	
		sSQL = "INSERT INTO SESS_FORM (ID_EDIZIONE,COD_SESSIONE," &_
		       "DT_INISESS,DT_FINSESS,FL_PRESENZA,NUM_ORESESS,DT_TMST)" &_
		       " VALUES(" & idEdi & ",'" & codses &_
		       "'," & ConvDateToDb(ConvStringToDate(dataIni)) &_
		       "," & ConvDateToDb(ConvStringToDate(dataFine)) &_
		       ",'" & codfase &_
               "'," & numOre &_
               "," & ConvDateToDb(Now()) & ")"
            
        if i = 1 then
            dt_ini = dataIni
            dt_fin = dataFine
        else
            if cdate(dt_ini) > cdate(dataIni) then
                dt_ini = dataIni 
            end if 
            if cdate(dt_fin) < cdate(dataFine) then
                dt_fin = dataFine 
            end if    
        end if        
		Errore =EseguiNoC(sSQL,CC)

		if Errore <> "0" then
				exit for
		end if 
	next
end if
	
if Errore = "0" then
   ssql = "UPDATE EDIZIONE SET DT_INIZIO =" & ConvDateToDb(ConvStringToDate(dt_ini)) &_
          " ,DT_FINE=" & ConvDateToDb(ConvStringToDate(dt_fin)) &_
          " WHERE ID_EDIZIONE =" & idEdi
    
   Errore =EseguiNoC(ssql,CC)       
end if	
	
if Errore = "0" then
	   CC.CommitTrans
	 
%>	   
	   <script>
	      alert("Inserimento correttamente effettuato")
	      location.href ="EDI_VisEdizione.asp"
	   </script>
<%
else
       CC.RollbackTrans
%>
       <script>
          alert("Inserimento non effettuabile")
	      history.go(-1);
       </script>	   
<%
end if
%>

<!--#include virtual ="/include/closeconn.asp"-->
<!--#include virtual = "/strutt_coda2.asp"-->
