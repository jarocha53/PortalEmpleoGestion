/* --- 


QUESTO FILE STABILISCE :
	IL FONT DELLE SCRITTE ED IL COLORE DELLO SFONDO
	LA POSIZIONE DEL MENU E LA GRANDEZZA	

geometry and timing of the menu 




--- */

var 	w=(screen.width-(screen.width/2))/2;	
var 	h=(screen.height-(screen.height/2))/2;




var MENU_POS1 = new Array();
	// RAPPRESENTA L'ALTEZZA DELLE CELLE DEL MENU' E SOTTOMENU'
	MENU_POS1['height'] = [25, 15];

	// RAPPRESENTA LA LARGHEZZA DELLE CELLE DEL MENU' E SOTTOMENU'
	MENU_POS1['width'] = [110, 370];

	// E'LA DISTANZA DAL MARGINE TOP DEL BROWSER
	MENU_POS1['block_top'] = [0, 30];

	// E' LA DISTANZA DEL MARGINE LEFT DEL BROWSER 
	MENU_POS1['block_left'] = [0, 10];
	
	// E' LA DISTANZA TRA UN UNA VOCE DEL SOTTOMENU' E L'ALTRA
	MENU_POS1['top'] = [0, 15];

	// E' LA DISTANZA TRA UNA VOCE E L'ALTRA DEL MENU PRINCIPALE
	MENU_POS1['left'] = [100, 0];


	// E' IL TEMPO CHE INTERCORRE,PER FAR SPARIRE IL SOTTOMENU, QUANDO IL MOUSE ESCE DALL'AREA DEL MENU
	MENU_POS1['hide_delay'] = [100, 100];
	
/* --- dynamic menu styles ---
note: you can add as many style properties as you wish but be not all browsers
are able to render them correctly. The only relatively safe properties are
'color' and 'background'.
*/
var MENU_STYLES1 = new Array();
	// default item state when it is visible but doesn't have mouse over
	MENU_STYLES1['onmouseout'] = [
		'color', ['#ffffff', '#000000', '#000000'], 
		'background', ['#ffffff', '#ffffff', '#993366'],
		'fontWeight', ['normal', 'normal', 'normal'],
		'textDecoration', ['none', 'none', 'none'],
	];
	// state when item has mouse over it
	MENU_STYLES1['onmouseover'] = [
		'color', ['#ffffff', '#336699', '#000000'], 
		'background', ['#ffffff', '#ffffff', '#cc6699'],
		'fontWeight', ['normal', 'bold', 'normal'],
		'textDecoration', ['underline', 'none', 'none'],
	];
	// state when mouse button has been pressed on the item
	MENU_STYLES1['onmousedown'] = [
		'color', ['#ffffff', '#000000', '#000000'], 
		'background', ['#ffffff', '#ffffff', '#CACCAB'],
		'fontWeight', ['normal', 'bold', 'normal'],
		'textDecoration', ['underline', 'none', 'none'],
	];
	
