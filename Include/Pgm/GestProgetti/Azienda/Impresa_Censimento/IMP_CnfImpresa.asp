<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include virtual="/strutt_testa2.asp"-->
<!--#include virtual="/util/dbutil.asp"-->
<!--#include virtual="/include/ControlDateVB.asp"-->
<!--#include virtual="/include/SysFunction.asp"-->
<!--#include virtual="/include/HTMLEncode.asp"-->

<table border="0" width="520" cellspacing="0" cellpadding="0" height="70">
   <tr>
     <td width="530" background="<%=Session("Progetto")%>/images/Titoli/strumenti2b.gif" height="81" valign="bottom" align="right">
       <table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
         <tr>
           <td width="100%" valign="top" align="right"><b class="tbltext1a">Gestione&nbsp;Azienda&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></b></td>
         </tr>
       </table>
      </td>
   </tr>
</table>

<!--#include virtual="/include/openconn.asp"-->

<%
sTipoImp = Request("cmbCodTimpr")
sRagSoc = strHTMLEncode(ucase(Replace(request("txtRagSoc"), "'", "''")))
sCodForm = Request("cmbCodForm")
sCodSet = Request("cmbCodSet")

sDtCost = Request("txtDtCost")
if sDtCost <> "" then
sDtCost = ConvDateToDB(ConvStringToDate(sDtCost))
else 
sDtCost = "null"
end if

sCapSoc = Request.Form ("txtCapSoc")
if sCapSoc = "" then 
	sCapSoc=0
end if	

sCodVal = Request("cmbCodVal")
sDescAtt = strHTMLEncode(ucase(Replace(request("txtDescAtt"), "'", "''")))
sNumIscReg = Request("txtNumIscReg")
sNumInps = Request("txtNumInps")
sPartIva = Request("txtPartIva")
sCodFis = ucase(Replace(request("txtCodFis"), "'", "''"))

sSitoWeb = strHTMLEncode(ucase(Replace(request("txtSitoWeb"), "'", "''")))
sDtTmst = Request ("txtoggi")
sSedeTel = Request("txtSedeTel")
sSedeFax = Request("txtSedeFax")
sSedeEmail = Request("txtSedeEmail")

sDescrizione = strHTMLEncode(ucase(Replace(request("txtDescrizione"), "'", "''")))

sCodSede	= Request("cmbCodSede")
sIndirizzo = strHTMLEncode(ucase(Replace(request("txtIndirizzo"), "'", "''")))
sCap		= Request("txtCap")
sComune		= Request("txtComuneHid")
sProv		= Request("cmbProv")

'sReferente		= UCase(Request.Form ("txtRef"))
'sReferente		= Replace(sReferente ,"'","''")
sReferente		= Request.Form ("txtRef")
'Controllo necessario perch� altrimenti, in caso di stringa vuota, le funzioni di pulitura restituiscono " "
if sReferente <> "" then 
sReferente = strHTMLEncode(ucase(Replace(request("txtRef"), "'", "''")))
end if

sRefRuolo	= UCase(Request.Form ("cmbRefRuo"))
sRefRuolo	= Replace(sRefRuolo ,"'","''")
sTelefono	= UCase(Request.Form ("txtTel"))
sFax		= UCase(Request.Form ("txtFAX"))
sEmail		= Request.Form ("txtEMail")

sLogin = strHTMLEncode(ucase(Replace(request("txtLogin"), "'", "''")))

sCentroImpiego = Request.Form("txtCentroImpiegoHid")

'''LC**:23/09/2003
'''Recupero la descrizione del Progetto (per le e-mail)---
sProgetto = mid(session("progetto"),2)
SqlProjDesc = "SELECT DESC_PROJ FROM PROGETTO WHERE AREA_WEB = '" & sProgetto & "'"
'PL-SQL * T-SQL  
SQLPROJDESC = TransformPLSQLToTSQL (SQLPROJDESC) 
Set rsProgetto = cc.Execute(SqlProjDesc)
	IF NOT rsProgetto.EOF THEN
		sProgettoDesc = rsProgetto("DESC_PROJ")
	ELSE
		sProgettoDesc = sProgetto
	END IF
rsProgetto.Close()
set rsProgetto=nothing
'''FINE LC**---


Errore =  "0"	

	'Controllo che nel db non esistano altri record con la stessa PARTITA IVA o CODICE FISCALE.
	if sPartIva <> "" then
		sSQL = "SELECT ID_IMPRESA FROM IMPRESA WHERE PART_IVA='" & _
				sPartIva & "'"
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
		set rsContaIva = CC.Execute(sSQL)
			if not rsContaIva.EOF then 
				Errore = "Partita Iva gi� presente in elenco" 
			end if					
		rsContaIva.close
		set rsContaIva = nothing	
	end if
	
	if sCodFis <> "" then
		sSQL = "SELECT ID_IMPRESA  FROM IMPRESA WHERE COD_FISC ='" & _
				sCodFis & "'"	
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
		set rsContaCodfis = CC.Execute(sSQL)						
			if not rsContaCodfis.EOF then 
				if errore <> "0"  then 
					Errore = "Partita Iva e Codice Fiscale gi� presenti in elenco"
				else
					Errore = "Codice Fiscale gi� presente in elenco"
				end if 
			end if
		rsContaCodfis.close
		set rsContaCodfis = nothing
	end if
	
	' Controllo il referente
	if sReferente <> "" then
		sSQL =	"SELECT IDUTENTE FROM UTENTE WHERE LOGIN = '" &_
				sLogin & "' AND TIPO_PERS='S' AND IND_ABIL='S' AND CREATOR=0" &_ 
				" AND TRIM(COGNOME||' '||NOME) = '" & sReferente & "'" & _
				" AND EMAIL= '" & sEmail & "'"
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
		set rsLogin = CC.Execute(sSQL)
			if not rsLogin.Eof then
				nIdUte = clng(rsLogin("IDUTENTE"))
			else
				Errore = "Login errata e/o Referente gia' associato ad una sede o non abilitato ad accedere"
			end if
		rsLogin.Close
		set rsLogin = nothing
	end if 


Cc.BeginTrans

	If Errore = "0" then
		sSQLImp = "INSERT INTO IMPRESA " &_
				"(COD_TIMPR, RAG_SOC, COD_FORMA,ID_SETTORE,DT_COST,CAP_SOC," &_
				"COD_VALUTA,DESC_ATT,NUM_ISC_REG,NUM_INPS,PART_IVA,COD_FISC,SITOWEB,DT_TMST)  VALUES " &_
				"('" & sTipoImp & _
				"','" & sRagSoc & _
				"','" & sCodForm & _
				"'," & sCodSet & _
				"," & sDtCost & _
				"," & sCapSoc & _
				",'" & sCodVal & _
				"','" & sDescAtt & _
				"','" & sNumIscReg & _
				"','" & sNumInps & _
				"','" & sPartIva & _
				"','" & sCodFis & _
				"','" & sSitoWeb & _
				"'," & ConvDateToDb(Now()) & ")"
		
		ExisteIdent = 0
		ValorIdent = 0			
		chiave= sRagSoc & "|" & Session("idutente")				
		Errore = EseguiNoCTraceIdentificador(UCase(Mid(Session("progetto"),2)), chiave, "IMPRESA", Session("idutente"), sCodImp, "S", "INS", sSQLImp, 1, sDtTmst, cc,ExisteIdent,ValorIdent)
	End If
	
	' Se non ci sono problemi nell'inserimento dell'impresa 
	' procedere con l'inserimento della sede.
	IF Errore = "0" then
		if ExisteIdent = 1 then 
		    sidImp = ValorIdent
		else
	        sSQL = "SELECT max(ID_IMPRESA) NEWID FROM IMPRESA "
            'PL-SQL * T-SQL  
            SSQL = TransformPLSQLToTSQL (SSQL) 
	        set rsIdImp = CC.Execute(sSQL)
		        if not rsIdImp.EOF then
			        sidImp = rsIdImp("NEWID")
		        end if
	        rsIdImp.Close
	        set rsIdImp = nothing
	    end if 
				
		sSQLSede = "INSERT INTO SEDE_IMPRESA " &_
				"(ID_IMPRESA,ID_CIMPIEGO, DESCRIZIONE,COD_SEDE,INDIRIZZO,COMUNE,PRV," &_
				"CAP,NUM_TEL_SEDE,FAX_SEDE,E_MAIL_SEDE,REF_SEDE,COD_RUO,NUM_TEL,FAX,E_MAIL,DT_TMST)  VALUES " &_
				"('" & sidImp & _
				"','" & sCentroImpiego & _
				"','" & sDescrizione & _
				"','" & sCodSede & _
				"','" & sIndirizzo & _
				"','" & sComune & _
				"','" & sProv & _
				"','" & sCap & _
				"','" & sSedeTel & _
				"','" & sSedeFax & _
				"','" & sSedeEmail & _
				"','" & sReferente & _
				"','" & sRefRuolo & _
				"','" & sTelefono & _
				"','" & sFax & _
				"','" & sEmail & _
				"'," & ConvDateToDb(Now()) & ")"
		
		chiave= sDescrizione & "|" & Session("idutente")
		ExisteIdent = 0
		ValorIdent = 0	
		Errore = EseguiNoCTraceIdentificador(UCase(Mid(Session("progetto"),2)), chiave, "SEDE_IMPRESA", Session("idutente"), sCodImp, "S", "INS", sSQLSede, 1, sDtTmst, cc,ExisteIdent,ValorIdent)
		
		if Errore = "0" then
		   If ExisteIdent = 1 then 
		        nidSedeImpresa = ValorIdent
		   else
		        ''cambia el select xq cambio la clave primaria, el id_sede es pk.
			    sSQL =	"SELECT max(ID_SEDE) NEWID_SEDE FROM SEDE_IMPRESA " '&_
					    '"WHERE ID_IMPRESA = " & CLng(sidImp)
                'PL-SQL * T-SQL  
                SSQL = TransformPLSQLToTSQL (SSQL) 
			    set rsIDSedeImpresa = cc.Execute(sSQL)
				    nidSedeImpresa = clng(rsIDSedeImpresa("NEWID_SEDE"))
			    rsIDSedeImpresa.Close
			    set rsIDSedeImpresa = nothing
			end if 
			
			'''LC**:12/09/2003
			''' Se non ci sono problemi nell'inserimento della sede 
			''' procedere con l'associazione al progetto.
			sSQLAziendaProj =	"INSERT INTO SEDE_PROJ " &_
								"(ID_IMPRESA,ID_SEDE,COD_CPROJ,DT_ISCRIZIONE,IDUTENTE,DT_TMST) VALUES " &_
								"('" & sidImp & _
								"','" & nidSedeImpresa & _
								"','" & mid(session("progetto"),2) & _
								"'," & ConvDateToDb(Now()) &_
								",'" & session("idutente") & _
								"'," & ConvDateToDb(Now()) & ")"
		
			chiave= sidImp &"|"& nidSedeImpresa &"|"& Session("idutente")
			Errore = EseguiNoCTrace(UCase(Mid(Session("progetto"),2)),chiave,"SEDE_PROJ",Session("idutente"),sCodImp,"S","INS",sSQLAziendaProj,1,sDtTmst,cc)

			If Errore = "0" then
			'''FINE LC**
				if sReferente <> "" then							
					sSQL =	"UPDATE UTENTE SET CREATOR = " &_
							CLng(nidSedeImpresa) &_
							" WHERE IDUTENTE = " & nIdUte
					Errore=EseguiNoC(sSQL,CC)
				end if
			End if
			
		end if 	
	
	END IF


	IF ERRORE="0" THEN
		CC.CommitTrans
		
		if sReferente <> "" then
			'Chiamata alla sub invio email
			InviaEmail sEmail,sReferente	
		end if
		%>
		<form name="frmIndietro" method="post" action="IMP_VisCensImpresa.asp">
			<input type="hidden" name="txtPartIva" value="<%=sPartIva%>">
			<input type="hidden" name="txtCodFiscale" value="<%=sCodFis%>">
			<input type="hidden" name="prVolta" value="No">
		</form>
		<script>
			alert("Inserimento correttamente effettuato");					
			frmIndietro.submit()				
		</script>
	<%
	ELSE
		CC.RollbackTrans
	%>
		<br>
		<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
			<tr height="18">
				<td class="sfondomenu" width="67%" height="18"><span class="tbltext0"><b>&nbsp;GESTIONE AZIENDA</b></span></td>
				<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
				<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
			</tr>
		</table>
		<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
			<tr>
				<td align="left" class="SFONDOCOMM">
					Pagina di conferma dell'inserimento di una nuova Azienda.
				</td>
			</tr>
			<tr height="2">
				<td class="SFONDOCOMM" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif">
				</td>
			</tr>
		</table>

		<br><br>
		<table border="0" cellspacing="1" cellpadding="1" width="500">
			<tr align="middle">
				<td class="tbltext3">
					Inserimento non effettuabile.
				</td>
			</tr>
			<tr align="middle">
				<td class="tbltext3">
					<%Response.Write(Errore)%> 
				</td>
			</tr>
		</table>
		<br>				
		<table border="0" cellpadding="0" cellspacing="1" width="500">
			<tr>
				<td align="middle" colspan="2" width="60%"><b>
				 <a HREF="javascript:history.back()"><img SRC="<%=Session("Progetto")%>/images/indietro.gif" border="0">
				</td>
			</tr>
		</table>
	<%
	END IF


Sub InviaEmail(sEmail,sReferente)
	dim miaMail
	dim HTML
	Dim sNomeServer, sLogoProj, sEmailProj

	sNomeServer = Request.ServerVariables("server_name")
	sLogoProj = "http://" & sNomeServer & "/" & sProgetto & "/images/barraarcobaleno.jpg"
	sEmailProj = "Info"& sProgetto &"@italialavoro.it"
	
	
	set miaMail = CreateObject("CDONTS.NewMail")

	HTML = HTML & "<HTML>"
	HTML = HTML & "<BODY bgColor=#f0f8ff>"
	HTML = HTML & "<div=center>"
	HTML = HTML & "<table border=0 width=95% align=center cellpadding=1 bgColor=#f0f8ff cellspacing=2 ><tr><td>"
	HTML = HTML & "<font size='3' color='#000080' face='Verdana'><br><i><b>"& sProgettoDesc &"</b></i></font><br><br>"
	HTML = HTML & "<IMG SRC='" & sLogoProj & "'><br><br>"
	HTML = HTML & "<font size='2' color='#000080' face='Verdana'><br>Gentile " & sReferente & ",<br><br>la informiamo"
	HTML = HTML & " che sono state validate le informazioni riguardanti la vostra azienda.<br>"
	HTML = HTML & "Da questo momento, collegandosi a " & sProgetto & ", potr� usufruire dei servizi offerti."
	HTML = HTML & "<br><br><br>"
	HTML = HTML & "A presto</br>"
	HTML = HTML & "<br><i>La Redazione di " & sProgettoDesc &"</i> ( <a href=http://www.ItaliaLavoro.it/" & sProgetto & ">www.ItaliaLavoro.it/" & sProgetto & "</a> )<br><br></br></td></tr></font>"
	HTML = HTML & "</table></div></BODY></HTML>"

	miaMail.From = sEmailProj
	miaMail.To = sEmail
	miaMail.Subject = "Conferma convalida dati azienda"
	miaMail.BodyFormat = 0
	miaMail.MailFormat = 0
	miaMail.Importance = 0
	miaMail.Body = HTML
	miaMail.Send

	Set miaMail = Nothing
End Sub
%>

<!--#include virtual="/include/CloseConn.asp"-->
<!--#include virtual="/strutt_coda2.asp"-->

