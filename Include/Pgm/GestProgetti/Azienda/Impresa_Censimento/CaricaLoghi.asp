<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include virtual="/util/portallib.asp"-->
<!--#include virtual="/strutt_testa2.asp"-->
<!--#include Virtual ="/include/openconn.asp"-->

<%
'------------------------------------------------------------------------
Sub Inizio() %>
	<html>
	<head>
		<link REL="STYLESHEET" TYPE="text/css" HREF="<%=session("progetto")%>/fogliostile.css">
		<meta NAME="GENERATOR" Content="Microsoft Visual Studio 6.0">
		<title>Caricamento Loghi Aziende</title>	
	</head>

	<script LANGUAGE="JavaScript">
		function Valida(frmUpload)
		{
			var n=0;
			if ((typeof frmUpload.txtUpload.length) == 'number') {
				for(i=0; i < frmUpload.txtUpload.length; i++) {
					//LC_77: 18/09/2003 - Aggiunti controlli
					if (frmUpload.txtUpload[i].value=="") {
						alert("Selezionare tutti i files")
						frmUpload.txtUpload[i].focus()
						return false
						break;
					}
					
					nomefile = frmUpload.txtUpload[i].value
					estens = nomefile.substr(nomefile.lastIndexOf(".")+1)
					if ((estens != 'gif')&&(estens != 'GIF')&&(estens != 'JPG')&&(estens != 'jpg')) {
						alert("Formato dei file errato. Sono accettati solo file con estensione .GIF o .JPG")
						return false;
						break;
					}
					//FINE LC_77
					for (j=i+1; j < frmUpload.txtUpload.length; j++) {
						if (frmUpload.txtUpload[j].value == frmUpload.txtUpload[i].value) {
							n++
						}
					}
				}		
				if (n != 0) {
					alert ("Sono presenti duplicati")
				}
			}
			else {
				if (frmUpload.txtUpload.value=="") {
					frmUpload.txtUpload.focus()
					alert("Selezionare un file")
					return false
				}
				
				//LC_77: 18/09/2003 - Aggiunti controlli
				nomefile = frmUpload.txtUpload.value
				estens = nomefile.substr(nomefile.lastIndexOf(".")+1)
				if ((estens != 'gif')&&(estens != 'GIF')&&(estens != 'JPG')&&(estens != 'jpg')) {
					alert("Formato dei file errato. Sono accettati solo file con estensione .GIF o .JPG")
					return false;
				}
				//FINE LC_77
			}
				
			return true
		}
	</script>


	<body>
	<p align="center">
<%
	dim sFunzione
	dim sTitolo
	dim sCommento
	dim bCampiObbl
	%>
		<table border="0" width="520" cellspacing="0" cellpadding="0" height="81">
		   <tr>
		     <td width="530" background="<%=Session("Progetto")%>/images/titoli/strumenti2b.gif" height="81" valign="bottom" align="right">
		       <table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
		         <tr>
		           <td width="100%" valign="top" align="right"><b class="tbltext1a">Upload Loghi &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></b></td>
		         </tr>
		       </table>
		     </td>
		   </tr>
		</table>
		<br>
	<%
	sFunzione = "CARICAMENTO LOGHI AZIENDALI"
	sTitolo = "CARICAMENTO LOGHI AZIENDALI"
	sCommento = "Caricamento Loghi delle Aziende partecipanti al progetto.<br>" &_ 
				" Per poter utilizzare il Logo dell'Azienda " &_
				" � necessario che il nome del file sia uguale alla <b>Ragione Sociale</b> dell'azienda e " &_
				" che il formato del file sia <b>.gif</b> o <b>.jpg</b>.<br>" &_
				" Inoltre le dimensioni del Logo devono essere di <b>110 x 80 pixel</b>."
	bCampiObbl = false
	sHelp = "/Pgm/help/GestProgetti/Azienda/Impresa_Censimento/CaricaLoghi"		
%>
	<!--#include virtual="/include/SetTestata.asp"-->
<%
End Sub
'------------------------------------------------------------------------
Sub Fine() %>
	<br>
	</p>		
	</body>
	</html>
<%
End Sub
'------------------------------------------------------------------------
Function EstraiDir(sPath)
	for nPos = 1 to Len(sPath)
		if mid(sPath,nPos,1) = "\" then
			nLastPos=nPos
		end if
	next
	EstraiDir = left(sPath, nLastPos)
End Function
'------------------------------------------------------------------------
Sub ImpostaPag()
	
dim nPos, nLastPos, folderspec
dim sPRel
dim nFile
dim sPercIcone
dim nContaDir
	
	on error resume next
	if sImg = "" then
		sPRel = sRoot
	else
		sPRel = EstraiDir(Replace(sImg,"/","\"))
	end if
	folderspec = server.mappath(sPRel)
	if err.number <> "0" then
		sPRel = sRoot
		folderspec = server.mappath(sPRel)
	end if
	on error goto 0
	
	Dim fso, f, f1, fc
	Set fso = CreateObject("Scripting.FileSystemObject")
	Set f = fso.GetFolder(folderspec)
	Set fc = f.Files
	Set fs = f.SubFolders
	
'S U B F O L D E R
%>
<br>
<table width="500" border="0">
	<tr height="25"> 
		<td align="left" class="sfondocomm" colspan="5">
			La cartella contiene&nbsp;<b><%=fc.count%></b> file
		</td>
	</tr>
	<tr>
<%
	nFile = 0
	For Each f1 in fc
		sImmagine=replace(sprel & f1.name, "\", "/")
		
		nFile = nFile + 1	
		
		sPercIcone = session("progetto")&"/images/Formazione/icons/"
		select case right(f1.name,4)
			case ".jpg"
				sIco = sPercIcone & "gif.gif"
			case ".gif"
				sIco = sPercIcone & "gif.gif"
			case else
				sIco = sPercIcone & "altro.gif"
		end select
%>
		<td align="left" valign="center">
			<img src="<%=sIco%>" border="0"><b class="tbltext1"><%=f1.name%></b>
		</td>
	
<%		if (nFile Mod 2) = 0 then
			Response.Write "</tr><tr>"
		end if	
	
	Next
%>
	</tr>
</table>
<br>
<%
End Sub
'------------------------------------------------------------------------
Sub Upload(sPImg)
%>
<table width="500" border="0">
	<tr height="25"> 
		<td align="left" class="sfondocomm" colspan="5">
			Upload del file
		</td>
	</tr>
	<tr>
		<td class="tbltext" width="500">
			<form METHOD="POST" name="frmNumUpl" action="CaricaLoghi.asp?modo=5&amp;img=<%=sPImg%>">
				Numero file da trasferire:
				<select name="cmbNumUpl" id="cmbNumUpl" onchange="javascript:frmNumUpl.submit()">
					<option value="1" <%if nInpFile = 1 then Response.Write "selected"%>>1</option>
					<option value="2" <%if nInpFile = 2 then Response.Write "selected"%>>2</option>
					<option value="3" <%if nInpFile = 3 then Response.Write "selected"%>>3</option>
					<option value="4" <%if nInpFile = 4 then Response.Write "selected"%>>4</option>
					<option value="5" <%if nInpFile = 5 then Response.Write "selected"%>>5</option>
					<option value="6" <%if nInpFile = 6 then Response.Write "selected"%>>6</option>
					<option value="7" <%if nInpFile = 7 then Response.Write "selected"%>>7</option>
					<option value="8" <%if nInpFile = 8 then Response.Write "selected"%>>8</option>
					<option value="9" <%if nInpFile = 9 then Response.Write "selected"%>>9</option>
					<option value="10" <%if nInpFile = 10 then Response.Write "selected"%>>10</option>
				</select>
			</form>
			<form METHOD="POST" encType="multipart/form-data" onsubmit="return Valida(this)" name="frmUpload" action="CaricaLoghi.asp?modo=2&amp;img=<%=sPImg%>">
				<%CostruisciUpl(nInpFile)%>
		</td>
	</tr>		
	<tr>
		<td class="tbltext" width="500" align="center">			
				<input type="image" id="cmdUpload" name="cmdUpload" src="<%=Session("Progetto")%>/images/Aggiungi.gif">
			</form>
		</td>
	</tr>	
</table>
<%
End Sub
'------------------------------------------------------------------------
Sub CostruisciUpl(nInpFile)
if nInpFile = "" then
	nInpFile = 1
end if

for i = 1 to nInpFile
	Response.Write "<input type='File' id='txtUpload' name='txtUpload' size=40><br>"
next

End Sub
'------------------------------------------------------------------------
Function SalvaFile(sPImg)

	if sPImg = "" then
		sPImg = sRoot
	end if
'************************************************************************	
'					V E R S I O N E   P R E C E D E N T E
'	On Error Resume Next
'
'	Set objUpload = Server.CreateObject("Dundas.Upload.2")
'	objUpload.UseUniqueNames = false
'	
'	PathSave = Server.MapPath("\") & replace(sPImg, "/", "\")
'	objUpload.Save PathSave
'
'	strName = objUpload.Form("txtUpload")
'
'	Set objUpload = Nothing
'************************************************************************	

	On Error Resume Next

	Set objUpload = Server.CreateObject ("Dundas.Upload.2")
	objUpload.UseUniqueNames = false
	PathSave = Server.MapPath("\") & replace(sPImg, "/", "\")
	objUpload.Save PathSave

	If Err.Number = 0 Then
		For Each objUploadedFile in objUpload.Files
		Next
		For Each objFormItem In objUpload.Form
		Next
	End If
	
	Set objUpload = Nothing

	if Err.number = 0 then
		SalvaFile = "File correttamente salvato"
	else
		SalvaFile = "Errore durante il salvataggio: " & Err.number & ": " & Err.Description
	end if
	
End Function
'------------------------------------------------------------------------
'M A I N

	if ValidateService(session("idutente"),"CARICALOGHI",cc) <> "true" then 
		response.redirect "/util/error_login.asp"
	end if

	dim nInpFile

	const sRootInit = "/Images/LogoAziende/"
	sRoot = Session("Progetto") & sRootInit
	sImg = Request("img")
	nModo = Request("modo")

	select case nModo
		case 1
			sImg = sImg & "/"
		case 2
			sPar = SalvaFile(sImg)
			%>
			<script language="javascript">
				alert("<%=sPar%>");
			</script>
			<%	
		case 5
			nInpFile = Request.Form ("cmbNumUpl")
	end select


	Inizio()
	ImpostaPag()
	Upload(sImg)

	Fine()
%>

<!--#include virtual ="/strutt_coda2.asp"-->
<!--#include Virtual ="/include/closeconn.asp"-->
