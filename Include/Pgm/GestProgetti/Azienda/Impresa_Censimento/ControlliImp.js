function SelPsw(sNome,sCognome,sLogin,sPassword){
	windowArea = window.open ('IMP_SelPsw.asp?P=aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa&Nome=' + sNome + '&Cognome=' + sCognome + '&Login=' + sLogin + '&Password=' + sPassword,'Info','width=535,height=450,Resize=No,Scrollbars=yes')	
}

function SelCentroImpiego(sArea,sCampo,sNomeCampo)
{
	sProv = document.frmImpresa.cmbProv.value
	if (sProv == "") {
		alert("Inserire prima la provincia")
	}
	else{
		windowArea = window.open ('IMP_SelCentroImp.asp?Area=' + sArea + '&sTxtArea=' + sCampo +'&NomeCampo=' + sNomeCampo + '&provincia=' + sProv ,'Info','width=425,height=450,Resize=No,Scrollbars=yes')	
	}
	
}

function SelCentroImpiegoMod(sNomeCampo)
{
	sProv = document.frmSedeImpresa.cmbProv.value
	if (sProv == "") {
		alert("Inserire prima la provincia")
	}
	else{
		//windowArea = window.open ('IMP_SelCentroImpMod.asp?Area=' + sArea + '&sTxtArea=' + sCampo +'&NomeCampo=' + sNomeCampo + '&provincia=' + sProv ,'Info','width=425,height=450,Resize=No,Scrollbars=yes')	
		windowArea = window.open ('IMP_SelCentroImpMod.asp?NomeCampo=' + sNomeCampo + '&provincia=' + sProv ,'Info','width=425,height=450,Resize=No,Scrollbars=yes')	
	}
	
}




	//Funzione per i controlli dei campi 
	function ControllaDati(frmImpresa)
	{
		//alert(frmImpresa.txtCentroImpiego.value)

		// -------- CONTROLLO VALORI INSERITI -------------------------		

		if (frmImpresa.cmbCodTimpr.value == 0)
		{
			alert("Il campo Tipologia impresa � obbligatorio")
			frmImpresa.cmbCodTimpr.focus() 
			return false
		}

		// ragione sociale
		frmImpresa.txtRagSoc.value = TRIM(frmImpresa.txtRagSoc.value)
		if (frmImpresa.txtRagSoc.value == "")
		{
			alert("Il campo Ragione sociale � obbligatorio")
			frmImpresa.txtRagSoc.focus() 
			return false
		}
		
		if (!ValidateInputStringWithNumber(frmImpresa.txtRagSoc.value))
		{
			alert("Il campo Ragione Sociale � formalmente errato")
			frmImpresa.txtRagSoc.focus()
			return false;
		}
		
		if (frmImpresa.cmbCodForm.value == 0)
		{
			alert("Il campo Forma giuridica � obbligatorio")
			frmImpresa.cmbCodForm.focus() 
			return false
		}
		
		if (frmImpresa.cmbCodSet.value == "")
		{
			alert("Il campo Settore � obbligatorio")
			frmImpresa.cmbCodSet.focus() 
			return false
		}
		
		frmImpresa.txtDtCost.value = TRIM(frmImpresa.txtDtCost.value)
		DataCostituzione = frmImpresa.txtDtCost.value
		if (!frmImpresa.txtDtCost.value == "")
		{
			//if (frmImpresa.txtDtCost.value == "")
			//{
			//alert("Il campo Data costituzione � obbligatorio")
			//frmImpresa.txtDtCost.focus() 
			//return false
			//}
			if (frmImpresa.txtDtCost.value != "")
			{
				if (!ValidateInputDate(frmImpresa.txtDtCost.value)) 
				{
					frmImpresa.txtDtCost.focus() 
					return false
				}
			}
			if (ValidateRangeDate(frmImpresa.txtDtCost.value,frmImpresa.txtoggi.value)==false)
			{
				alert("Il campo Data di costituzione deve essere minore della data odierna")
				frmImpresa.txtDtCost.focus()
				return false
			}				
		}
	
	
		// capitale sociale		
		frmImpresa.txtCapSoc.value = TRIM(frmImpresa.txtCapSoc.value)
		if (!frmImpresa.txtCapSoc.value == "")
		{
			//if (frmImpresa.txtCapSoc.value == "")
			//{
			//	alert("Il campo Capitale sociale � obbligatorio")
			//	frmImpresa.txtCapSoc.focus() 
			//	return false
			//}
			if (!IsNum(frmImpresa.txtCapSoc.value))
			{
				alert("Il campo Capitale sociale deve essere numerico")
				frmImpresa.txtCapSoc.focus() 
				return false
			}
			if (frmImpresa.cmbCodVal.value == "")
			{
				alert("Il campo Valuta � obbligatorio se � stato inserito il Capitale sociale")
				frmImpresa.cmbCodVal.focus() 
				return false
			}
		}
		
		
		if ((frmImpresa.txtCapSoc.value == "") && (!frmImpresa.cmbCodVal.value == ""))
		{
			alert("Selezionare la Valuta solo se � stato inserito il Capitale sociale")
			frmImpresa.cmbCodVal.focus() 
			return false
		}
		
		//descrizione attivit�
		frmImpresa.txtDescAtt.value = TRIM(frmImpresa.txtDescAtt.value)
		if (frmImpresa.txtDescAtt.value == "")
		{
			alert("Il campo Attivit� � obbligatorio")
			frmImpresa.txtDescAtt.focus() 
			return false
		}
				
		if (!ValidateInputStringWithNumber(frmImpresa.txtDescAtt.value))
		{
			alert("Il campo Attivit�  � formalmente errato")
			frmImpresa.txtDescAtt.focus()
			return false;
		}		
			
	
		// numero iscrizione registro
			//if (frmImpresa.txtNumIscReg.value == "")
			//		{
			//			alert("Il campo Numero di registro � obbligatorio")
			//			frmImpresa.txtNumIscReg.focus() 
			//			return false
			//		}
		frmImpresa.txtNumIscReg.value = TRIM(frmImpresa.txtNumIscReg.value)		
		if (!frmImpresa.txtNumIscReg.value == "")	
		{
			if (frmImpresa.txtNumIscReg.value != "")
			{
				if (!IsNumIscReg(frmImpresa.txtNumIscReg.value))
				{
					frmImpresa.txtNumIscReg.focus()
					return false				
				}
			}
		}
		
			
		// numero inps	
			//		if (frmImpresa.txtNumInps.value == "")
			//	{
			//		alert("Il campo Numero Inps � obbligatorio")
			//		frmImpresa.txtNumInps.focus() 
			//		return false
			//	}
		frmImpresa.txtNumInps.value = TRIM(frmImpresa.txtNumInps.value)		
		if (!frmImpresa.txtNumInps.value == "")
		{
			if (frmImpresa.txtNumInps.value != "")
			{
				if (!IsNum(frmImpresa.txtNumInps.value))
				{
					alert("Il valore del campo Numero Inps deve essere numerico")
					frmImpresa.txtNumInps.focus() 
					return false
				}
			}
		}		
		
		// partita iva o codice fiscale: uno dei due � obbligatorio
		frmImpresa.txtPartIva.value = TRIM(frmImpresa.txtPartIva.value);
		frmImpresa.txtCodFis.value = TRIM(frmImpresa.txtCodFis.value);
		if ((frmImpresa.txtPartIva.value == "") && (frmImpresa.txtCodFis.value == ""))
		{
			alert("E' obbligatorio inserire almeno uno dei due campi Partita iva o Codice fiscale")
			frmImpresa.txtPartIva.focus() 
			return false; 
		}
		
		
		// controllo partita iva 
		if (frmImpresa.txtPartIva.value != "")
		{
			if (!IsNum(frmImpresa.txtPartIva.value))
			{
				alert("Il campo Partita iva deve essere numerico")
				frmImpresa.txtPartIva.focus() 
				return false
			}
			if (frmImpresa.txtPartIva.value.length != 11)
			{
				alert("Il campo Partita iva deve essere di 11 caratteri")
				frmImpresa.txtPartIva.focus() 
				return false;
			}
		}
		
		
		// controllo codice fiscale
		if (frmImpresa.txtCodFis.value != "")
		{
			blank = " ";
			if (!ChechSingolChar(frmImpresa.txtCodFis.value,blank))
			{
				alert("Il campo Codice Fiscale non pu� contenere spazi bianchi")
				frmImpresa.txtCodFis.focus();
				return false;
			}
			if ((frmImpresa.txtCodFis.value.length != 11) && (frmImpresa.txtCodFis.value.length != 16))
			{
				alert("Il campo Codice fiscale deve essere di 11 o 16 caratteri")
				frmImpresa.txtCodFis.focus(); 
				return false;
			}	
			if (!ValidateInputStringWithNumber(frmImpresa.txtCodFis.value))
			{
				alert("Il campo Codice fiscale � formalmente errato")
				frmImpresa.txtCodFis.focus()
				return false;
			}
		}
		// fine con controlli p. iva /codice fiscale
		
		frmImpresa.txtSitoWeb.value = TRIM(frmImpresa.txtSitoWeb.value)
		

// ********************************** FINE CONTROLLI IMPRESA ********************************************
/*
 parte di controlli riservata alla sede
*/
// ********************************** INIZIO CONTROLLI SEDE ********************************************
		
		// descrizione sede 
		frmImpresa.txtDescrizione.value = TRIM(frmImpresa.txtDescrizione.value)
		if (frmImpresa.txtDescrizione.value == "")
		{
				alert("Il campo Descrizione Sede � obbligatorio")
			frmImpresa.txtDescrizione.focus() 
			return false
		}
		
		if (!ValidateInputStringWithNumber(frmImpresa.txtDescrizione.value))
			{
			alert("Il campo Descrizione Sede  � formalmente errato")
			frmImpresa.txtDescrizione.focus()
			return false;
			}

		// tipo sede
		if (frmImpresa.cmbCodSede.value == "")
		{
		alert("Il campo Tipo sede � obbligatorio")
		frmImpresa.cmbCodSede.focus()
		return false
		}
		
		
		
		// indirizzo
		frmImpresa.txtIndirizzo.value = TRIM(frmImpresa.txtIndirizzo.value)
		if (frmImpresa.txtIndirizzo.value == "") {
			alert("Il campo Indirizzo � obbligatorio")
			frmImpresa.txtIndirizzo.focus() 
			return false
		}
		/*if (!ValidateInputStringWithNumber(frmImpresa.txtIndirizzo.value)){
			alert("Il campo Indirizzo  � formalmente errato")
			frmImpresa.txtIndirizzo.focus()
			return false;
		}*/
		
		
		// provincia
		if (frmImpresa.cmbProv.value == "")	{
			alert("Il campo Provincia � obbligatorio")
			frmImpresa.cmbProv.focus()
			return false;
		}
		
		
		//Comune			
		frmImpresa.txtComune.value=TRIM(frmImpresa.txtComune.value)
		if (frmImpresa.txtComune.value == "") {
			alert("Il campo Comune � obbligatorio")
			//frmImpresa.txtComune.focus() 
			return false
		}
		
	
		// CAP
		frmImpresa.txtCap.value = TRIM(frmImpresa.txtCap.value)		

		if (frmImpresa.txtCap.value == "")
		{
			alert("Il campo CAP � obbligatorio")
			frmImpresa.txtCap.focus() 
			return false
		}

		if (!IsNum(frmImpresa.txtCap.value)){
			alert("Il campo CAP deve essere numerico")
			frmImpresa.txtCap.focus() 
			return false
		}

		if (eval(frmImpresa.txtCap.value) == 0){
			alert("Il campo CAP non pu� essere zero")
			frmImpresa.txtCap.focus()
			return false
		}
		
		if (frmImpresa.txtCap.value.length != 5){
			alert("Il campo CAP deve essere di 5 caratteri numerici")
			frmImpresa.txtCap.focus() 
			return false
		}

		
		//telefono	
		frmImpresa.txtSedeTel.value = TRIM(frmImpresa.txtSedeTel.value)
		/*if (frmImpresa.txtSedeTel.value == "") {
			alert("Il campo Telefono � obbligatorio")
			frmImpresa.txtSedeTel.focus() 
			return false
		}*/
		if (frmImpresa.txtSedeTel.value != "") {
			if (!IsNum(frmImpresa.txtSedeTel.value)) {
				alert("Il campo Telefono deve essere numerico")
				frmImpresa.txtSedeTel.focus() 
				return false
			}
			if (eval(document.frmImpresa.txtSedeTel.value) == 0){
				alert("Il numero di telefono non pu� essere zero")
				document.frmImpresa.txtSedeTel.focus()
				return false
			}
		}
		
		
		// fax
		if (frmImpresa.txtSedeFax.value != "")	{
			if (!IsNum(frmImpresa.txtSedeFax.value)) {
				alert("Il campo Numero di fax deve essere numerico")
				frmImpresa.txtSedeFax.focus() 
				return false
			}
			if (eval(document.frmImpresa.txtSedeFax.value) == 0){
				alert("Il numero di fax non pu� essere zero")
				document.frmImpresa.txtSedeFax.focus()
				return false
			}
		}
		
		
		// email
		frmImpresa.txtSedeEmail.value = TRIM(frmImpresa.txtSedeEmail.value)
		if (frmImpresa.txtSedeEmail.value != "") {
			pippo=ValidateEmail(frmImpresa.txtSedeEmail.value)
			if (pippo==false) {
				alert("Il campo Indirizzo email � formalmente errato")
				frmImpresa.txtSedeEmail.focus() 
				return false
			}
		}
		
		
		// centro per l'impiego
		/*==>10/07/2003: riattivata obbligatoriet� CPI per tutte le aziende, anche Enti Pubblici<==*/
		//if ((document.frmImpresa.txtCentroImpiego.value == "") && (document.frmImpresa.cmbCodForm.value != '08')) {
		if (document.frmImpresa.txtCentroImpiego.value == "") {
			alert("Il campo Centro Impiego � obbligatorio")
			return false
		}			
		
		
		// VERIFICARE IL SEGUENTE ?? marco
		//if ("<%=sTimpr%>" != "03" )
	//	{
	//		if (document.frmImpresa.cmbProv.value != document.frmImpresa.txtProvinciaOld.value){
	//				alert("Il centro per l'impiego non risiede in questa provincia")
	//				return false
	//		}			
	//	}
	
//
// Dati del referente
//
		// Se ci troviamo nel CPI non si inseriscono 
		// i dati del referente della sede e non occorre
		// fare i controlli formali.
		 //// 
		 
// Dati del referente
//
		// Se ci troviamo nel CPI non si inseriscono 
		// i dati del referente della sede e non occorre
		// fare i controlli formali.
		
		if (frmImpresa.Plavoro.value == "NO")
		{
			return true
		}

		frmImpresa.txtRef.value = TRIM(frmImpresa.txtRef.value)
		if (frmImpresa.txtRef.value == "") {
			alert("Il campo Referente � obbligatorio")
			frmImpresa.txtRef.focus() 
			return false
		}
		if (!ValidateInputStringWithOutNumber(frmImpresa.txtRef.value)) {
			alert("Il campo Referente � formalmente errato")
			frmImpresa.txtRef.focus()
			return false;
		}
		

		if (frmImpresa.cmbRefRuo.selectedIndex == 0) {
			alert ("Il campo Ruolo � obbligatorio")
			frmImpresa.cmbRefRuo.focus()
			return false
		}

		if (frmImpresa.txtTel.value != ""){
			if (!IsNum(frmImpresa.txtTel.value)){
				alert("Il campo Telefono  deve essere numerico")
				frmImpresa.txtTel.focus() 
				return false
			}
			if (eval(document.frmImpresa.txtTel.value) == 0){
				alert("Il numero di telefono non pu� essere zero")
				document.frmImpresa.txtTel.focus()
				return false
			}
		}

		if (frmImpresa.txtFAX.value != ""){
			if (!IsNum(frmImpresa.txtFAX.value)){
				alert("Il campo Fax deve essere numerico")
				frmImpresa.txtFAX.focus() 
				return false
			}
			if (eval(document.frmImpresa.txtFAX.value) == 0){
				alert("Il numero di fax non pu� essere zero")
				document.frmImpresa.txtFAX.focus()
				return false
			}
		}

		frmImpresa.txtEMail.value=TRIM(frmImpresa.txtEMail.value)
		if (frmImpresa.txtEMail.value == ""){
			alert("Il campo E-Mail � obbligatorio")
			frmImpresa.txtEMail.focus() 
			return false
		}		

		pippo=ValidateEmail(frmImpresa.txtEMail.value)
		if (pippo==false){
			alert("Il campo E-mail � formalmente errato")
			frmImpresa.txtEMail.focus() 
			return false
		}

		frmImpresa.txtLogin.value = TRIM(frmImpresa.txtLogin.value)
		var anyString = frmImpresa.txtLogin.value
		if (frmImpresa.txtLogin.value == ""){
			alert("Il campo Login � obbligatorio")
			frmImpresa.txtLogin.focus() 
			return false
		}
		/*if(!ValidateInputStringWithNumber(frmImpresa.txtLogin.value)) {
			alert("Il campo Login � formalmente errato")
			frmImpresa.txtLogin.focus() 
			return false
		}*/
		for (var i=0; i<=anyString.length-1; i++){
			if(((anyString.charAt(i) >= "A") && (anyString.charAt(i) <= "Z")) || 
				((anyString.charAt(i) >= "a") && (anyString.charAt(i) <= "z")) || 
				((anyString.charAt(i) >= "0") && (anyString.charAt(i) <= "9")) ||
				(anyString.charAt(i) == ".") || (anyString.charAt(i) == "-") ||
				(anyString.charAt(i) == "_"))
			{
			}
			else
			{		
				alert("Il campo Login � formalmente errato")
				frmImpresa.txtLogin.focus() 
				return false
			}		
		}
	
	
	return true
}


function IsNumIscReg(sNumIscReg) {
	var t
	
	if (sNumIscReg.length != 11) {
		alert("Il Numero del registro deve essere di 11 caratteri")
		return false
	}
	for (t=0; t<11;t++) {

		if (t==6){
			if (sNumIscReg.charAt(t)!= "/"){
				alert("Il formato corretto del Numero del registro � : 999999/9999")
				return false
			}
			t++ 
		}
		if (!IsNum(sNumIscReg.charAt(t))) {
				alert("Il Numero del registro deve essere numerico")
				return false
		}
	}
	return true
}


function userfocus()
{
//alert("ATTIVA!!")
//document.frmImpresa.txtRagSoc.focus()
//window.document.all["adr"].style.visibility = "hidden"
//window.document.all["adr1"].style.visibility = "hidden"
//window.opener.nomelabel.innerHTML = "ciao"
}
function CheckFGiur(CodiceForma) {
	if (CodiceForma == '08') {
		frmImpresa.ImgCPI.style.display = 'none'
	}
}

function InviaProv(CodiceTipo){
//	frmImpresa.cmbCodTimpro.value = CodiceTipo
//	return false
	frmImpresa.action = "IMP_InsImpresa.asp"
	frmImpresa.submit();
}
function Pulisci()
{
	frmImpresa.txtComune.value = ""
	frmImpresa.txtComuneHid.value = ""
	frmImpresa.txtCap.value = ""
	
	
	if (frmImpresa.txtCentroImpiego.value != "nessuno")
	{
	frmImpresa.txtCentroImpiego.value = ""
	}
}