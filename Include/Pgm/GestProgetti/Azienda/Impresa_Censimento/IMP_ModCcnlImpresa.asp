<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include virtual="/util/portallib.asp"-->
<!--#include virtual="/strutt_testa2.asp"-->
<!--#include virtual="/include/openconn.asp"-->
<!--#include virtual="/include/DecCod.asp"-->
<!-- #Include Virtual="/Include/ControlDateVB.asp" -->
<%
	if ValidateService(session("idutente"),"IMP_VISCENSIMPRESA",cc) <> "true" then 
		response.redirect "/util/error_login.asp"
	end if
%>
<script language="javascript" src="ControlliImp.js"></script>

<script LANGUAGE="Javascript">
//include del file per fare i controlli sulla validit� delle date
<!--#include virtual="/Include/ControlDate.inc"-->


//include del file per fare i controlli sulla numericit� dei campi
<!--#include virtual="/Include/ControlString.inc"-->
<!--#include virtual="/Include/help.inc"-->


// -------- CONTROLLO VALIDITA' DEI CAMPI -------------------------		
	function ControllaDati()
	{
	
		// Controllo Date
		frmInsCcnl.txtDataIniVal.value = TRIM(frmInsCcnl.txtDataIniVal.value)
		frmInsCcnl.txtDataFinVal.value = TRIM(frmInsCcnl.txtDataFinVal.value)
		DataInizio = frmInsCcnl.txtDataIniVal.value;
		DataFine = frmInsCcnl.txtDataFinVal.value;
		DataOggi=frmInsCcnl.txtoggi.value;
			
		
		// data Inizio
		if (DataInizio == "")
		{
		alert("Il campo Data Inizio Validit� � obbligatorio")
		frmInsCcnl.txtDataIniVal.focus() 
		return false
		}
				
		if (DataInizio != "")
		{
		
			if (!ValidateInputDate(DataInizio)) 
			{
				frmInsCcnl.txtDataIniVal.focus() 
				return false
			}
		
	
			if (DataOggi != DataInizio)
			{
				if (ValidateRangeDate(DataOggi,DataInizio))
				{
					alert("La data di Inizio validit� non pu� essere superiore della data odierna")
					frmInsCcnl.txtDataIniVal.focus()
					return false
				}			
			}
		
		}	
		//  data fine 	
		if (DataFine != "")
		{
		
			if (!ValidateInputDate(DataFine)) 
			{
				frmInsCcnl.txtDataFinVal.focus() 
				return false
			}
		
	
			if (ValidateRangeDate(DataFine,DataInizio))
			{
				alert("La data di Fine validit� deve essere superiore della data di Inzio validat�")
				frmInsCcnl.txtDataFinVal.focus()
				return false
			}				
		
		}
		 	
		
	// fine funzione controlla dati	
		return true;
	}			
		
	
	

	
	function userfocus()
	{
		document.frmInsCcnl.cmbCodCcnl.focus();
	}
	
</script>

<%
'Assegnazione  identificativi
CodCcnl = Request("CodCcnl")
sCodImp = Request("sCodImp")
RagioneSociale = Request("RagioneSociale")

sSQL = "SELECT COD_CCNL, DT_INIVAL, DT_FINVAL, DT_TMST FROM IMPRESA_CCNL WHERE ID_IMPRESA=" & sCodImp  
sSQL = sSQL & " AND COD_CCNL ='" &  CodCcnl & "'"

'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
Set rsCcnl = cc.execute(sSQL)
DT_INIVAL = rsCcnl("DT_INIVAL")
DT_FINVAL = rsCcnl("DT_FINVAL")
DT_TMST = rsCcnl("DT_TMST")

%>
<!-- MARCO nobody! <body onload="Javascript:return userfocus()">-->
<table border="0" width="520" cellspacing="0" cellpadding="0" height="81">
	<tr>
		<td width="530" background="<%=Session("Progetto")%>/images/titoli/strumenti2b.gif" height="81" valign="bottom" align="right">
			<table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
				<tr>
					<td width="100%" valign="top" align="right">
						<b class="tbltext1a">
						Gestione Azienda 
						</b>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<br>
<!--#include file = "IMP_Label.asp"-->
<br>

<table cellpadding="0" cellspacing="0" width="500" border="0">
	<tr height="18">
		<td class="sfondomenu" height="18" width="67%">
			<span class="tbltext0">
			<b>&nbsp;GESTIONE CONTRATTI APPLICATI</b>
			</span>
		</td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
		<td valign="middle" align="right" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1">
			(*) campi obbligatori
		</td>
	</tr>
	<tr width="371" class="SFONDOCOMM">
		<td colspan="3">
			La pagina consente di modificare i dati relativi al contratto Ccnl gia' inseriti in precedenza.
			<a href="Javascript:Show_Help('/Pgm/help/GestProgetti/Azienda/Impresa_Censimento/IMP_ModCcnlImpresa')" onMouseover="window.status = ''; return true;">
			<img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0">
			</a>
		</td>
	</tr>   
	<tr height="2">
		<td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif">
		</td>
	</tr>
</table>



<br>
<!--<table width="500" border="0">	<tr> 		<td align="center"> 			<a class="textRed" href="IMP_VisImpresa.asp">			<b>			Torna elenco aziende			</font></b></a>		</td>		<form method="post" action="IMP_VisSedeImpresa.asp" name="frmVisSedeImp">		<td align="center">			<input type="hidden" name="CodImp" value="<%=sIdImp%>"> 			<a class="textRed" href="javascript:frmVisSedeImp.submit()">			<b>	Torna elenco sedi</b></a>		</td>		</form>	</tr></table> -->

<form method="post" name="frmInsCcnl" onsubmit="return ControllaDati(this)" action="IMP_CnfModCcnl.asp" method="post">
	
	
	<input type="hidden" name="CodCcnl" value="<%=CodCcnl%>">
	<input type="hidden" name="sCodImp" value="<%=sCodImp%>">
	<input type="hidden" name="DT_TMST" value="<%=DT_TMST%>">	
	<input type="hidden" name="txtoggi" value="<%=ConvDateToString(Now())%>">

	<table border="0" cellpadding="0" cellspacing="1" width="500">
		<tr height="25">     
			<td align="left" nowrap class="tbltext1">
				<b>&nbsp;Ccnl</b>
				
			</td>
			<td align="left" class="textblack">
			<b><%=DecCodVal("TCCNL",sIsa,date,CodCcnl,"1")%></b>
	</td>
		</tr>
		
		<tr height="25">    
		<td align="left" class="tbltext1">
		<b>&nbsp;Data inizio validit�*</b><br>&nbsp;(gg/mm/aaaa)
		</td>	
		<td align="left">
			<input type="text" style="TEXT-TRANSFORM: uppercase" maxlength="10" name="txtDataIniVal" size="10" class="textblacka" value="<%=DT_INIVAL%>">
		</td>
	</tr>
	
		<tr height="25">     
		<td align="left" class="tbltext1">
		<b>&nbsp;Data fine validit�</b><br>&nbsp;(gg/mm/aaaa)
		</td>	
		<td align="left">
			<input type="text" style="TEXT-TRANSFORM: uppercase" maxlength="10" name="txtDataFinVal" size="10" class="textblacka" value="<%=DT_FINVAL%>">		
		</td>
	</tr>
		
   
        <tr>
            <td align="left" colspan="2">&nbsp;</td>
		</tr>
	</table>

	<table cellpadding="0" cellspacing="0" width="500" border="0">
		<tr>
		<td align="right" valign="bottom">			
		<a href="javascript:window.history.back()" onmouseover="javascript: window.status=' '; return true">
				<img alt="Indietro" border="0" src="<%=Session("Progetto")%>/images/indietro.gif">
				</a>					
		</td>
		</td><td width="5%"></td>
		<td align="left" valign="bottom">
		<input type="image" name="Invia" src="<%=Session("progetto")%>/images/conferma.gif">
		</td>
			
			
		</tr>
	</table>
<%'if Session("progetto") = "/PLAVORO" then%>
<!--	<input type="hidden" name="Plavoro" value="SI"> -->
<%'else%>
	<input type="hidden" name="Plavoro" value="NO">
<%'end if%>
</form>

<!--#include virtual="/strutt_coda2.asp"-->
<!--#include virtual="/include/closeconn.asp"-->
