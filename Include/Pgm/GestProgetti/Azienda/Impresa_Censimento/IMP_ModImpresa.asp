<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include virtual="/util/portallib.asp"-->
<!--#include virtual="/strutt_testa2.asp"-->
<!--#include virtual="/include/openconn.asp"-->
<%
if ValidateService(session("idutente"),"IMP_VISCENSIMPRESA",cc) <> "true" then 
	response.redirect "/util/error_login.asp"
end if
%>
<!--#include virtual = "/include/ckProfile.asp"-->
<!--#include virtual = "/include/ControlDateVB.asp"-->
<!--#include virtual = "/util/dbutil.asp"-->	
<!--#include virtual = "/include/DecCod.asp"-->
<script LANGUAGE="Javascript">
//include del file per fare i controlli sulla validit� delle date
<!--#include virtual = "/Include/ControlDate.inc"-->

//include del file per fare i controlli sulla numericit� dei campi
<!--#include virtual = "/Include/ControlNum.inc"-->

//include del file per fare i controlli sulla numericit� dei campi
<!--#include virtual = "/Include/ControlString.inc"-->
<!--#include virtual="/Include/help.inc"-->




function IsNumIscReg(sNumIscReg) {
	var t
	
	if (sNumIscReg.length != 11) {
		alert("Il numero del registro deve essere di 11 caratteri")
		return false
	}
	for (t=0; t<11;t++) {

		if (t==6){
			if (sNumIscReg.charAt(t)!= "/"){
				alert("Il formato corretto � : 999999/9999")
				return false
			}
			t++
		}
		
		// VEdere questa funzione se corretta
		if (!IsNum(sNumIscReg.charAt(t))) {
				alert("I dati devono essere numerici")
				return false
		}
	}
		
	return true
}


	//Funzione per i controlli dei campi 
	function ControllaDati(frmImpresa)
	{
		if (frmImpresa.cmbCodTimpr.value == 0){
			alert("Il campo Tipologia Impresa � obbligatorio")
			frmImpresa.cmbCodTimpr.focus() 
			return false
		}
			frmImpresa.txtRagSoc.value = TRIM(frmImpresa.txtRagSoc.value)
		if (frmImpresa.txtRagSoc.value == "")
		{
				alert("Il campo Ragione sociale � obbligatorio")
			frmImpresa.txtRagSoc.focus() 
			return false
		}
		if (!ValidateInputStringWithNumber(frmImpresa.txtRagSoc.value))
			{
			alert("Il campo Ragione Sociale  � formalmente errato")
			frmImpresa.txtRagSoc.focus()
			return false;
			}
		if (frmImpresa.cmbCodForm.value == 0){
			alert("Il campo Forma giuridica  � obbligatorio")
			frmImpresa.cmbCodForm.focus() 
			return false
		}
		if (frmImpresa.cmbCodSet.value == "")
		{
			alert("Il campo Settore � obbligatorio")
			frmImpresa.cmbCodSet.focus() 
			return false
			}
	
		frmImpresa.txtDtCost.value = TRIM(frmImpresa.txtDtCost.value)
		DataCostituzione = frmImpresa.txtDtCost.value
		if (!frmImpresa.txtDtCost.value == "")
		{
			//if (frmImpresa.txtDtCost.value == "")
			//{
			//alert("Il campo Data costituzione � obbligatorio")
			//frmImpresa.txtDtCost.focus() 
			//return false
			//}
					
			if (frmImpresa.txtDtCost.value != ""){
				if (!ValidateInputDate(frmImpresa.txtDtCost.value)) 
				{
					frmImpresa.txtDtCost.focus() 
					return false
				}
			}
		
			if (ValidateRangeDate(frmImpresa.txtDtCost.value,frmImpresa.txtoggi.value)==false)
			{
				alert("Il campo Data di costituzione deve essere minore della data odierna")
				frmImpresa.txtDtCost.focus()
				return false
			}				
		}
	
		// capitale sociale		
		frmImpresa.txtCapSoc.value = TRIM(frmImpresa.txtCapSoc.value)
		
		if (!frmImpresa.txtCapSoc.value == "")
		{
		//if (frmImpresa.txtCapSoc.value == "")
		//{
		//	alert("Il campo Capitale sociale � obbligatorio")
		//	frmImpresa.txtCapSoc.focus() 
		//	return false
		//}
			if (!IsNum(frmImpresa.txtCapSoc.value)){
				alert("Il campo Capitale sociale deve essere numerico")
				frmImpresa.txtCapSoc.focus() 
				return false
				}
		
			if (frmImpresa.cmbCodVal.value == "")
				{
				alert("Il campo Valuta � obbligatorio se � stato inserito il Capitale sociale")
				frmImpresa.cmbCodVal.focus() 
				return false
				}

		}
		
			
		// valuta ... se il capitale sociale � inserito
		if ((frmImpresa.txtCapSoc.value == "") && (!frmImpresa.cmbCodVal.value == ""))
				{
					alert("Il campo Valuta non deve essere inserito se non � compilato il campo Capitale sociale")
					frmImpresa.cmbCodVal.focus() 
					return false
				}
		
		// descrizione attivit�
			frmImpresa.txtDescAtt.value = TRIM(frmImpresa.txtDescAtt.value)
		if (frmImpresa.txtDescAtt.value == "")
		{
			alert("Il campo Attivit� � obbligatorio")
			frmImpresa.txtDescAtt.focus() 
			return false
		}
			
		if (!ValidateInputStringWithNumber(frmImpresa.txtDescAtt.value))
			{
			alert("Il campo Attivit�  � formalmente errato")
			frmImpresa.txtDescAtt.focus()
			return false;
			}		
			
		// numero iscrizione registro

		//if (frmImpresa.txtNumIscReg.value == "")
		//		{
		//			alert("Il campo Numero di registro � obbligatorio")
		//			frmImpresa.txtNumIscReg.focus() 
		//			return false
		//		}
		
		frmImpresa.txtNumIscReg.value = TRIM(frmImpresa.txtNumIscReg.value)		
		if (!frmImpresa.txtNumIscReg.value == "")	
		{
				if (frmImpresa.txtNumIscReg.value != ""){
					if (!IsNumIscReg(frmImpresa.txtNumIscReg.value)){
						frmImpresa.txtNumIscReg.focus()
						return false				
					}
				}

		}	
		// numero inps	
	
		//		if (frmImpresa.txtNumInps.value == "")
		//	{
		//		alert("Il campo Numero Inps � obbligatorio")
		//		frmImpresa.txtNumInps.focus() 
		//		return false
		//	}

		frmImpresa.txtNumInps.value = TRIM(frmImpresa.txtNumInps.value)		
		if (!frmImpresa.txtNumInps.value == "")
		{
				if (frmImpresa.txtNumInps.value != "")
				{
					if (!IsNum(frmImpresa.txtNumInps.value))
					{
						alert("Il valore del campo Numero Inps deve essere numerico")
						frmImpresa.txtNumInps.focus() 
						return false
					}
				}
				
		}		
		
		
		// partita iva o codice fiscale: uno dei due � obbligatorio
		frmImpresa.txtPartIva.value = TRIM(frmImpresa.txtPartIva.value);
		frmImpresa.txtCodFis.value = TRIM(frmImpresa.txtCodFis.value);
		
		if ((frmImpresa.txtPartIva.value == "") && (frmImpresa.txtCodFis.value == ""))
		{
		alert("E' obbligatorio almeno uno dei due campi Partita iva o Codice fiscale")
		frmImpresa.txtPartIva.focus() 
		return false; 
		}
		
		
		// controllo partita iva 
		if (frmImpresa.txtPartIva.value != "")
		{
			if (!IsNum(frmImpresa.txtPartIva.value))
			{
				alert("Il valore deve essere numerico")
				frmImpresa.txtPartIva.focus() 
				return false
			}
			
			if (frmImpresa.txtPartIva.value.length != 11)
			{
				alert("La partita Iva deve essere di 11 caratteri")
				frmImpresa.txtPartIva.focus() 
				return false;
			}
		}
		
		// controllo codice fiscale
		if (frmImpresa.txtCodFis.value != "")
		{
			blank = " ";
			if (!ChechSingolChar(frmImpresa.txtCodFis.value,blank))
			{
				alert("Il campo Codice Fiscale non pu� contenere spazi bianchi")
				frmImpresa.txtCodFis.focus();
				return false;
			}
			if ((frmImpresa.txtCodFis.value.length != 11) && (frmImpresa.txtCodFis.value.length != 16))
			{
				alert("Il codice Fiscale  deve essere di 11  o 16 caratteri")
				frmImpresa.txtCodFis.focus(); 
				return false;
			}	
	
			if (!ValidateInputStringWithNumber(frmImpresa.txtCodFis.value))
			{
			alert("il campo Codice fiscale � formalmente errato")
			frmImpresa.txtCodFis.focus()
			return false;
			}
		}
		
		// fine con controlli p. iva /codice fiscale
	frmImpresa.txtSitoWeb.value = TRIM(frmImpresa.txtSitoWeb.value)

		// if (!ValidateInputStringWithNumber(frmImpresa.txtSitoWeb.value))
		// 	{
		// 	alert("Il campo Sito web fiscale � formalmente errato")
		// 	frmImpresa.txtSitoWeb.focus()
		// 	return false;
		// 	}
		
			return true;	
	}
// fine controllo dati input/form di modifica

function HideVariables(idForm)
	{
		//document.forms.item('frmRicAzien' + idForm).action = sUrl		
		
		document.forms.item('frm' + idForm).submit();
	}
	
</script>
</head>


<%
dim sTipoImp
dim sCodImp 
dim CnConn
dim rsImpresa
dim sSQL
dim sRagSoc
dim sCodForm
dim sCodSet
dim sDtCost
dim sCapSoc
dim sCodVal
dim sDescAtt
dim sNumIscReg
dim sNumInps
dim sPartIva
dim sCodFis
dim sDataCess
dim sCodCess
dim sSitoWeb
dim sData
dim sIsa
dim sMask

sCodImp = Request("sCodImp")


	
' Controllo i diritti dell'utente connesso.
' La variabile di sessione mask � cos� formatta:
' 1 Byte abilitazione funzione Impresa
' 2 Byte abilitazione funzione Sede_impresa
' 3 Byte abilitazione funzione Val_impresa
' dal 4� byte in poi si visualizzano le provincie.
		
sMask = Session("Diritti")

if mid(Session("UserProfiles"),1,1) = "0" then
	sIsa = "0"
else
	sIsa = mid(Session("UserProfiles"),1,2)
end if

sSQL = "SELECT ID_IMPRESA, RAG_SOC, COD_TIMPR, COD_FORMA, ID_SETTORE, " &_
	" DT_COST, CAP_SOC, COD_VALUTA, DESC_ATT, NUM_ISC_REG, NUM_INPS, PART_IVA, " &_
	" COD_FISC,DT_CESS_ATTIVITA,COD_CESS_ATTIVITA,SITOWEB, DT_TMST  FROM IMPRESA WHERE ID_IMPRESA =" &_
	clng(sCodImp)


'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
set rsImpresa = CC.Execute(sSQL)


sRagSoc = rsImpresa("RAG_SOC") 
sTipoImp = rsImpresa("COD_TIMPR") 
sCodForm	= rsImpresa("COD_FORMA")
sCodSet =  rsImpresa("ID_SETTORE")

	
if rsImpresa("DT_COST") <> "" then
	sDtCost = ConvDateToString(rsImpresa("DT_COST"))
end if 

sCapSoc = rsImpresa("CAP_SOC")
if sCapSoc <> "" then 
sCapSoc	= FormatNumber(rsImpresa("CAP_SOC"),0,,,0)
end if 


sCodVal =  rsImpresa("COD_VALUTA")
sSitoWeb	= rsImpresa("SITOWEB")
sDescAtt	= rsImpresa("DESC_ATT")
sNumIscReg = rsImpresa("NUM_ISC_REG")

sNumInps	= rsImpresa("NUM_INPS")
sPartIva	= rsImpresa("PART_IVA")

if rsImpresa("DT_CESS_ATTIVITA") <> "" then
	sDataCess	= ConvDateToString(rsImpresa("DT_CESS_ATTIVITA"))
else 
	sDataCess = ""
end if
sCodCess	= rsImpresa("COD_CESS_ATTIVITA")
sCodFis = rsImpresa("COD_FISC")
sDtTmst = rsImpresa("DT_TMST")
rsImpresa.Close 
set rsImpresa = nothing


%>



<table border="0" width="520" cellspacing="0" cellpadding="0" height="81">
   <tr>
     <td width="530" background="<%=Session("Progetto")%>/images/titoli/strumenti2b.gif" height="81" valign="bottom" align="right">
       <table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
         <tr>
           <td width="100%" valign="top" align="right"><b class="tbltext1a">Gestione&nbsp;Azienda</span></b></td>
         </tr>
       </table>

     </td>
   </tr>
</table><br>


<br>
<table cellpadding="0" cellspacing="0" width="500" border="0">
	<tr height="18">
		<td class="sfondomenu" height="18" width="67%">
			<span class="tbltext0">
			<b>&nbsp;GESTIONE AZIENDA </b>
			</span>
		</td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
		<td valign="middle" align="right" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1">
			(*) campi obbligatori
		</td>
	</tr>
	<tr width="371" class="SFONDOCOMM">
		<td colspan="3">
			Inserire i dati relativi all'azienda.
			<a href="Javascript:Show_Help('/Pgm/help/GestProgetti/Azienda/Impresa_Censimento/IMP_ModImpresa')" onMouseover="window.status = ''; return true;">
			<img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0">
			</a>
		</td>
	</tr>   
	<tr height="2">
		<td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif">
		</td>
	</tr>
</table>

<form name="frmVisSedeImpresa" action="IMP_VisSedeImpresa.asp" method="post">
<input type="hidden" name="sCodImp" value="<%=sCodImp%>">
</form>
<form name="frmVisValImpresa" action="IMP_VisValImpresa.asp" method="post">
<input type="hidden" name="sCodImp" value="<%=sCodImp%>">
</form>

<form name="frmVisCcnl" action="IMP_VisCcnlImpresa.asp" method="post">
<input type="hidden" name="sCodImp" value="<%=sCodImp%>">
</form>
	


	

<table cellpadding="0" cellspacing="0" width="500" border="0">
	<tr> 
	
		<td width="170" align="center">
		<a class="textRed" href="Javascript:HideVariables('VisCcnl')" onMouseover="javascript: window.status='';  return true">
		<b> Contratti applicati</b></a>&nbsp;
		</td>
		
		<td width="170" align="center">
		<a class="textRed" href="Javascript:HideVariables('VisValImpresa')" onMouseover="javascript: window.status='';  return true">
		<b> Situazione</b></a>
		</td>
		
		<td width="170" align="center">
		<a class="textRed" href="Javascript:HideVariables('VisSedeImpresa')" onMouseover="javascript: window.status='';  return true">
		<b> Elenco Sedi</b></a>&nbsp;
		</td>
	</tr>
</table>
<form method="post" name="frmImpresa" onsubmit="return ControllaDati(this)" action="IMP_CnfModImpresa.asp">
<input type="hidden" name="sCodImp" value="<%=sCodImp%>">
<input type="hidden" name="txtoggi" value="<%=ConvDateToString(Date())%>">
<input type="hidden" name="sDtTmst" value="<%=sDtTmst%>">
    <table border="0" cellpadding="0" cellspacing="1" width="500">
        <tr>
			<td align="middle" colspan="2" nowrap>
				<p align="left" class="tbltext1">
					<b>Identificativo</b>
				</p>
            </td>
            <td align="left" colspan="2" width="60%" class="textblack" height="30">
				<p align="left"><b><%=sCodImp%></b></p>
				<!--<input style="TEXT-TRANSFORM: uppercase; HEIGHT: 22px" size="6" maxlength="6" name="txtIdi" value="<%=sCodImp%>" readonly>													-->		
		    </td>
        </tr>
		<tr>
			<td align="middle" colspan="2" nowrap>
				<p align="left">
				<span class="tbltext1" Style="Cursor:HELP" title="Scegliere la tipologia d'impresa che corrisponde all'azienda"><b>Tipologia Impresa*<b></span>
				</p>
			</td>
			<td align="left">
				<%
				'	if Session("progetto") <> "/PLAVORO" then
						sTimpr = "TIMPR|0|" & date & "|" & sTipoImp & "|cmbCodTimpr|AND CODICE <> '03' ORDER BY DESCRIZIONE"
				'	else
				'		sTimpr = "TIMPR|0|" & date & "|" & sTipoImp & "|cmbCodTimpr|ORDER BY DESCRIZIONE"
				'	end if									
					CreateCombo(sTimpr)
					'------------------------------------------------------------				
				%>	
			</td>
		</tr>        
        <tr>
			<td align="middle" colspan="2" nowrap>
				<p align="left">
				<span class="tbltext1" Style="Cursor:HELP" title="Inserire Denominazione Azienda"><b>Ragione Sociale*<b></span>
									
				</p>
            </td>
            <td align="left" colspan="2" width="60%">
				<p align="left">
					<input style="TEXT-TRANSFORM: uppercase; WIDTH: 220px;" class="textblack" size="40" maxlength="50" name="txtRagSoc" value="<%=sRagSoc%>">
				</p>
            </td>
        </tr>
        <tr>
			<td align="middle" colspan="2" nowrap>
				<p align="left">
				<span class="tbltext1" Style="Cursor:HELP" title="Inserire forma giuridica che si adatta alla tipologia dell'attivit� esercitata."><b>Forma Giuridica*<b></span>
				
				</p>
            </td>
            <td align="left" colspan="2" width="40%">
				<p align="left">
					<span class="table">
<%
					sInt = "FGIUR|" & sIsa & "|" & date & "|" & sCodForm & "|cmbCodForm| ORDER BY DESCRIZIONE "			
					CreateCombo(sInt)
%>
					</span>
				</p>
            </td>
        </tr>
        <tr>
			<td align="left" colspan="2">
				<p align="left">
					<span class="tbltext1" Style="Cursor:HELP" title="Indicare classificazione delle attivit� economiche secondo estratto Istat."><b>Settore*</b></span>
						
				</p>
			</td>
			<td align="left" colspan="2" width="40%">
				<p align="left">
				<span class="table">
<%

				sSQL = "SELECT ID_SETTORE,subStr(DENOMINAZIONE,1,30) As DENOMINAZIONE from Settori ORDER BY DENOMINAZIONE"

'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
				set rsSett = CC.Execute(sSQL)
				Response.Write "<SELECT ID=cmbCodSet name=cmbCodSet class=textblack><OPTION value=></OPTION>"

				do while not rsSett.EOF 
					Response.Write "<OPTION "
					' Ricerca il record della combo da evidenziare.
					if trim(sCodSet) = trim(rsSett("ID_SETTORE")) then
						Response.Write "selected "
					end if
					Response.write "value ='" & rsSett("ID_SETTORE") & _
						"'> " & rsSett("DENOMINAZIONE")  & "</OPTION>"
					rsSett.MoveNext 
				loop
				Response.Write "</SELECT>"
				rsSett.Close	
%>					
				</span>
				</p>
			</td>
        </tr>
        <tr>
            <td align="left" colspan="2">
				<p align="left">
					<span class="tbltext1"><b>Data costituzione</b> (gg/mm/aaaa)</span>
				</p>
            </td>
            <td align="left" colspan="2" width="40%">
				<p align="left">
					<span class="tabel">
						<input style="TEXT-TRANSFORM: uppercase;" class="textblack" size="16" maxlength="10" name="txtDtCost" value="<%=sDtCost%>"> 
					</span>
		        </p>
            </td>
        </tr>
        <tr>
            <td align="left" colspan="2">
				<p align="left">
					<span class="tbltext1"><b>Capitale sociale</b></span>
						
				</p>
            </td>
            <td align="left" colspan="2" width="40%">
				<p align="left">
				<span class="table">
				<input style="TEXT-TRANSFORM: uppercase; WIDTH: 219px; " class="textblack" size="40" name="txtCapSoc" value="<% if sCapSoc<> 0 then  Response.write sCapSoc%>">
				</span>
				</p>
            </td>
        </tr>
        <tr>
            <td align="left" colspan="2">
				<p align="left">
					<span class="tbltext1"><b>Valuta</b></span>
						
				</p>
            </td>
            <td align="left" colspan="2" width="40%">
				<p align="left">
					<span class="table">
<%				sInt = "VALUT|" & sIsa & "|" & date & "|" & sCodVal & "|cmbCodVal| ORDER BY DESCRIZIONE "			
				CreateCombo(sInt)%>
					</span>
				</p>
            </td>
        </tr>
        <tr>
            <td align="left" colspan="2">
				<p align="left">
					<span class="tbltext1" Style="Cursor:HELP" title="Inserire breve descrizione attivit� principale dell'azienda"><b>Attivit�*<b></span>
					
				</p>
            </td>
            <td align="left" colspan="2" width="40%">
				<p align="left">
					<font color="#ffffff" face="Verdana">
						<input style="TEXT-TRANSFORM:uppercase; WIDTH: 219px;" class="textblack" size="40" maxlength="50" name="txtDescAtt" value="<%=sDescAtt%>">
					</font>
		        </p>
            </td>
		</tr>
        <tr>
            <td align="left" colspan="2">
				<p align="left">
					<span class="tbltext1"><b>N� iscr. registro</b></span>
						
				</p>
            </td>
            <td align="left" colspan="2" width="40%">
				<p align="left">
					<span class="table">
					<input style="TEXT-TRANSFORM: uppercase;" class="textblack" size="16" maxlength="11" name="txtNumIscReg" value="<%=sNumIscReg%>">
					</span>
				</p>
            </td>
        </tr>
        <tr>
            <td align="left" colspan="2">
				<p align="left">
					<span class="tbltext1"><b>Numero Inps</b></span>
						
				</p>
            </td>
            <td align="left" colspan="2" width="40%">
				<p align>
					<span class="table">
						<input style="TEXT-TRANSFORM: uppercase;" class="textblack" size="16" maxlength="11" name="txtNumInps" value="<%=sNumInps%>">
					</span>
		        </p>
            </td>
		</tr>

        <tr>
            <td align="left" colspan="2">
				<p align="left">
					<span class="tbltext1"><b>Partita IVA</b></span>					
				</p>
            </td>
            <td align="left" colspan="2" width="40%">
				<p align="left">
					<span class="table">
					<input style="TEXT-TRANSFORM: uppercase;" class="textblack" size="16" maxlength="11" name="txtPartIva" value="<%=sPartIva%>">
				</span>
				</p>
            </td>
        </tr>
        <tr>
            <td align="left" colspan="2">
				<p align="left">
					<span class="tbltext1"><b>Codice fiscale</b></span>
					
				</p>
            </td>
            <td align="left" colspan="2" width="40%">
				<p align="left">
					<span class="tabel">
					<input style="TEXT-TRANSFORM: uppercase;" class="textblack" size="22" maxlength="16" name="txtCodFis" value="<%=sCodFis%>">
					</span>
				</p>
            </td>
        </tr>
	    <tr>
            <td align="left" colspan="2">
				<p align="left">
					<span class="tbltext1"><b>Sito Web</b></span>
					
				</p>
            </td>
            <td align="left" colspan="2" width="40%">
				<p align="left">
					<span class="tabel">
					<input style="TEXT-TRANSFORM: uppercase; WIDTH: 219px;" class="textblack" size="40" maxlength="255" name="txtSitoWeb" value="<%=sSitoWeb%>">
					</span>
				</p>
            </td>
        </tr>

		</table>
		
		

<table cellpadding="0" cellspacing="0" width="500" border="0">

        <tr>
            <td align="middle" colspan="3">&nbsp;</td>
        </tr>
        <tr>
        
        <td align="right">             
        <a class="textRed" href="IMP_VisCensImpresa.asp" onMouseover="window.status = ''; return true;">
               
		<img alt="Indietro" border="0" src="<%=Session("Progetto")%>/images/indietro.gif">
		</a>					
			</td>
			<td width="5%"></td>
			
            <td align="left">
			<% 
			if 	ckProfile(mid(sMask,1,2),4) then 
			%>
			<input type="image" src="<%=Session("Progetto")%>/Images/Conferma.gif" name="Invia" value="Conferma">
			<% 
			end if 
			%>					
            </td>
        </tr>
    </table>

</form>
<p>&nbsp;</p>
<!--#include virtual ="/strutt_coda2.asp"-->
<!--#include virtual ="/include/closeconn.asp"-->
