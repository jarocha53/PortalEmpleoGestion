<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include Virtual = "/strutt_testa2.asp"-->
<!--#include Virtual = "/include/OpenConn.asp"-->
<!--#include Virtual = "/include/DecCod.asp"-->
<!--#include Virtual = "/include/ControlDateVB.asp"-->
<!--#include Virtual = "/util/dbutil.asp"-->

<%
if ValidateService(session("idutente"),"IMP_VISCENSIMPRESA",cc) <> "true" then 
	response.redirect "/util/error_login.asp"
end if
%>

<script LANGUAGE="Javascript">

//include del file per fare i controlli sulla numericit� dei campi
<!--#include Virtual = "/Include/ControlNum.inc"-->

//include del file per fare i controlli sulla stringa
<!--#include Virtual = "/Include/ControlString.inc"-->
<!--#include virtual = "/Include/help.inc"-->

	
	
	//Funzione per i controlli dei campi 
	function ControllaDati(){
// -------- CONTROLLO VALORI INSERITI -------------------------		

/*
 Parte di controlli sui campi del referente
*/
		document.frmIscri.txtCognome.value=TRIM(document.frmIscri.txtCognome.value)
		sCognome=ValidateInputStringWithOutNumber(document.frmIscri.txtCognome.value)
		if  (sCognome==false){
			alert("Cognome formalmente errato.")
			document.frmIscri.txtCognome.focus() 
			return false
		}
			
		if (frmIscri.txtCognome.value == ""){
			alert("Il campo Cognome � obbligatorio!")
			frmIscri.txtCognome.focus() 
			return false
		}
		
		document.frmIscri.txtNome.value=TRIM(document.frmIscri.txtNome.value)
		sNome=ValidateInputStringWithOutNumber(document.frmIscri.txtNome.value)
		if  (sNome==false){
			alert("Nome formalmente errato.")
			document.frmIscri.txtNome.focus() 
			return false
		}		
		if (frmIscri.txtNome.value == ""){
			alert("Il campo Nome � obbligatorio!")
			frmIscri.txtNome.focus() 
			return false
		}
		
		//if (frmIscri.elements[2].selectedIndex == 0) {
		if (frmIscri.cmbRefRuo.selectedIndex == 0){
			alert ("Campo Ruolo obbligatorio")
			//frmIscri.elements[2].focus()
			frmIscri.cmbRefRuo.focus()
			return false
		}
		
		
		//Controlli su LOGIN: posso inserire solo numeri e / o lettere
		var anyString = document.frmIscri.txtLogin.value;
		document.frmIscri.txtLogin.value = TRIM(document.frmIscri.txtLogin.value)
		if (frmIscri.txtLogin.value == ""){
			alert("Campo Login obbligatorio")
			frmIscri.txtLogin.focus() 
			return false
		}
					
		for (var i=0; i<=anyString.length-1; i++){
			if(((anyString.charAt(i) >= "A") && (anyString.charAt(i) <= "Z")) || 
				((anyString.charAt(i) >= "a") && (anyString.charAt(i) <= "z")) || 
				((anyString.charAt(i) >= "0") && (anyString.charAt(i) <= "9")) ||
				(anyString.charAt(i) == ".") || (anyString.charAt(i) == "-") ||
				(anyString.charAt(i) == "_"))
			{
			}
			else
			{		
				document.frmIscri.txtLogin.focus();
			 	alert("Inserire nel campo Login un valore valido");
				return false
			}		
		}
		
		//Email
		document.frmIscri.txtMail.value=TRIM(document.frmIscri.txtMail.value)
		if (document.frmIscri.txtMail.value ==""){
			alert("Email obbligatoria")
			document.frmIscri.txtMail.focus() 
			return false
		}
		
		appoEmail=ValidateEmail(document.frmIscri.txtMail.value)
		
		if  (appoEmail==false){
			alert("Indirizzo Email formalmente errato")
			document.frmIscri.txtMail.focus() 
			return false
		}		
		

		return true
	}	



	//Cancella Dati
	function Cancella(frmIscri){
		document.frmIscri.reset();
		return false
	}

</script>
<% 
	dim sidsede
	dim simpresa

   sidsede=Request.Form("idsede")
   sidImp=Request.Form("idimpresa")
   
   %>
   <!--include virtual = "/include/RichLogin.asp"--><%

   ' sRil=clng(RichLogin(sidsede,"S"))
	sRil=0
   %>

<% 
sCodImp = sidImp
%>


<table border="0" width="520" cellspacing="0" cellpadding="0" height="81">
   <tr>
     <td width="530" background="<%=Session("Progetto")%>/images/titoli/strumenti2b.gif" height="81" valign="bottom" align="right">
       <table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
         <tr>
           <td width="100%" valign="top" align="right"><b class="tbltext1a">Gestione&nbsp;Azienda&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></b></td>
         </tr>
       </table>
     </td>
   </tr>
</table>
<br>
<!--#include File="IMP_Label.asp"-->
<br>
<table cellpadding="0" cellspacing="0" width="500" border="0">
   <tr height="18">
		<td class="sfondomenu" height="18" width="67%">
			<span class="tbltext0"><b>RICHIESTA LOGIN/PASSWORD</b></span></td>
			
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
		<td valign="middle" align="right" class="tbltext1" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">(*) campi obbligatori</td>
    </tr>
		
   <tr width="371" class="SFONDOCOMM">
   <td colspan="3">Per ottenere la password di accesso, inserire i dati richiesti e premere <b>Invia</b>.<br>
		La password, assegnata dal sistema, verr� inviata all'indirizzo di posta elettronica indicato.<br>
		In seguito potr� essere cambiata con una di propria scelta.	
	<a href="Javascript:Show_Help('/Pgm/help/GestProgetti/Impresa_Censimento/Azienda/IMP_RilLogin')"><img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
	</td>
	</tr>   
	<tr height="2">
		<td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif">
		</td>
	</tr>
</table><br>

<!--form action="IMP_CnfRichiesta.asp" method="post" name="frmIscri" id="frmIscri"-->
<form action="IMP_CnfRicLogin.asp" method="post" name="frmIscri" id="frmIscri">
<input type="hidden" name="idimpresa" value="<%=sIdImp%>">
<input type="hidden" name="idsede" value="<%=sidSede%>">



<% if sRil=0 then %> 
	
		<!---------- Inizio Tabella Inserimento ---------->
		<table border="0" width="350" cellpadding="2" cellspacing="1">   
		   <tr>
				<td COLSPAN="2" width="500">
					<span class="tbltext1a"><b>Referente:</b></span>
				</td>
			</tr>
			<tr>
				<td colspan="2"></td>
			</tr>     
			<tr>
				<td align="left" class="tbltext1" nowrap><b>
					&nbsp;&nbsp;Cognome </b>*	
				</td>
				<td align="left">
					<input style="TEXT-TRANSFORM: uppercase;" size="49" class="textblack" maxlength="50" name="txtCognome">
				</td>
		   </tr> 
			<tr>
				<td align="left" class="tbltext1" nowrap><b>
					&nbsp;&nbsp;Nome </b>*		
				</td>
				<td align="left">
					<input style="TEXT-TRANSFORM: uppercase;" size="49" class="textblack" maxlength="50" name="txtNome">
				</td>
		   </tr> 	
			<tr>
				<td align="left" class="tbltext1" nowrap><b>
					&nbsp;&nbsp;Ruolo </b>*	
				</td>
				<td align="left">
					<%
						'Richiamo funzione che popola la combo RUOLO
						'-----------------------------------------------			
						sRefRuo = "RUOLO|0|" & date & "|" & NULL & "|cmbRefRuo|ORDER BY DESCRIZIONE"			
						CreateCombo(sRefRuo)
						'-----------------------------------------------				
					%>		
				</td>
		   </tr>   
			<tr>
				<td align="left" class="tbltext1" width="85" nowrap><b>
					&nbsp;&nbsp;Login </b>*		
				</td>
				<td align="left">
					<input style="TEXT-TRANSFORM: uppercase;" size="37" maxlength="15" class="textblack" name="txtLogin">
				</td>
		   </tr>    
			<tr>
				<td align="left" class="tbltext1" nowrap><b>
					&nbsp;&nbsp;E-mail </b>*		
				</td>
				<td align="left">
					<input size="49" class="textblack" maxlength="100" name="txtMail">
				</td>
		   </tr>  

		   <tr>
				<td>&nbsp;</td>
		   </tr>

		</table>
	<br>     
		<table border="0" cellpadding="0" cellspacing="0" width="500" align="center">
			<tr>
				<td align="right">
				
<a href="javascript:history.back()"><img src="<%=Session("progetto")%>/images/Indietro.gif" border="0"></a>
				</td>
				<td width="5%"></td>
				<td align="left">
					
					<input type="image" name="Invia" src="<%=Session("Progetto")%>/images/conferma.gif" border="0" value="Conferma" onclick="return ControllaDati()">
				</td>
					</tr>
		</table>
	<%else %>
    <table border="0" width="500" cellpadding="1" cellspacing="1">   

    	<tr height="17"> 
			<td align="middle" class="tbltext3"><b>Login gia' rilasciata all'azienda.</b>
			</td>
		</tr>
	</table><br>
	
	<table WIDTH="500" BORDER="0" CELLPADDING="0" CELLSPACING="0">
					<tr align="center">
						<td>
							<a href="javascript:history.back()"><img src="<%=Session("progetto")%>/images/Indietro.gif" border="0"></a>
						</td>
					</tr>
				</table><%
	
end if %>

</form>

<!--#include Virtual = "/include/closeconn.asp"-->
<!--#include Virtual = "/strutt_coda3.asp"-->
