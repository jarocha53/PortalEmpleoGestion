<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include Virtual = "/strutt_testa2.asp"-->
<!--#include virtual="/util/portallib.asp"-->
<!--#include virtual="/include/openconn.asp"-->
<!--#include Virtual = "/util/dbutil.asp"-->
<!--#include Virtual = "/include/ckProfile.asp"-->
<!--#include Virtual = "/util/dbutil.asp"-->
<!--#include Virtual = "/include/DecCod.asp"-->
<!--#include Virtual = "/include/DecComun.asp"-->
<!--#include Virtual = "/include/SelAreaTerrBandi.asp"-->

<%
	if ValidateService(session("idutente"),"IMP_VISCENSIMPRESA",cc) <> "true" then 
		response.redirect "/util/error_login.asp"
	end if
%>
<script LANGUAGE="Javascript">
<!--#include virtual="/Include/help.inc"-->

function Reload(sCodImp) {
	document.frmVisSedeImpresa.sCodImp.value = sCodImp;
	document.frmVisSedeImpresa.Provincia.value =frmVisSedeImpresa.cmbProv.value;
	document.frmVisSedeImpresa.action = "IMP_VisSedeImpresa.asp";
	document.frmVisSedeImpresa.submit()
}

function ModificaSede(Sede,Impresa) {
	document.frmModSede.idSede.value = Sede;
	document.frmModSede.sCodImp.value = Impresa;
	document.frmModSede.submit();
}

function AssAziendaProj (idSede,idImpresa) {
	if (confirm('Si vuole eseguire la procedura di associazione della Sede al Progetto?')) {
		document.frmModSede.action = 'IMP_AssAziendaProj.asp';
		document.frmModSede.sCodImp.value = idImpresa;
		document.frmModSede.idSede.value = idSede;
		document.frmModSede.txtProvincia.value = frmVisSedeImpresa.cmbProv.value;
		document.frmModSede.submit();
	}
}
</script>
<%		
dim appoTarga

	sProv = Request("Provincia")
	if sProv = "" then
		sProv = mid(Session("Filtro"),1,2)
	end if
	sMask = Session("Diritti") 
	
	nProvSedi = len(Session("Filtro"))
	
	sCodImp = request("sCodImp")

	'if mid(Session("UserProfiles"),1,1) = "0" then
	'	sIsa = "0"
	'else
	'	sIsa = mid(Session("UserProfiles"),1,2)
	'end if
	sIsa = "0"
	
	sSQL = "SELECT RAG_SOC FROM IMPRESA WHERE ID_IMPRESA =" & clng(sCodImp)


'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	set rsImp = CC.Execute(sSQL)
	
%>	
<table border="0" width="520" cellspacing="0" cellpadding="0" height="81">
	<tr>
		<td width="530" background="<%=Session("Progetto")%>/images/titoli/strumenti2b.gif" height="81" valign="bottom" align="right">
			<table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
				<tr>
					<td width="100%" valign="top" align="right"><b class="tbltext1a">Gestione&nbsp;Azienda&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></b></td>
				</tr>
			</table>
		</td>
	</tr>
</table>

<!--#include file = "IMP_Label.asp"-->
<br>
<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
	<tr height="18">
		<td class="sfondomenu" width="67%" height="18"><span class="tbltext0"><b>&nbsp;GESTIONE SEDI </b></span></td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
		<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
	</tr>
</table>
<!-- Commento -->
<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
	<tr>
		<td align="left" class="SFONDOCOMM">
			Questa pagina consente l'inserimento e/o la modifica di una sede dell'azienda.<br>
			Se risultano gi� registrate sedi per l'azienda selezionata, � consentito l'accesso alla  
			modifica dei dati <b>solo</b> se la sede stessa � associata al progetto.<br>
			In caso contrario, per poter operare sui dati della sede verr� richiesto di effettuare 
			l'associazione della sede al progetto, cliccando sulla &quot;immagine&quot; relativa.
			<a href="Javascript:Show_Help('/Pgm/help/GestProgetti/Azienda/Impresa_Censimento/IMP_VisSedeImpresa')"><img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
		</td>
	</tr>
	<tr height="2">
		<td class="SFONDOCOMM" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif">
		</td>
	</tr>
</table>
<br>
<%
	rsImp.Close 		
	set rsImp=nothing
'Inizio AM 12/09/2003 
'*	i = 0	

	'Per testare il combobox delle province
	'	nProvSedi = 0
'*	if nProvSedi <> 0 then

'*		nProvSedi = nProvSedi/2 
		' Mi ritorna quante province posso visionare
		' Ciclo per creare la combo delle province 
		' con le sole province di interesse
'*		sCondizione = " AND Codice in ("

'*		for i=1 to nProvSedi
'*			if i <> 1 then
'*				sCondizione = sCondizione & ","
'*			end if
'*			pos_ini = 2 * (i-1)
'*			sCondizione = sCondizione & "'" & mid(Session("Filtro"),1+pos_ini,2) & "'"
'*		next
'*		sCondizione = sCondizione & ")"
'*	else 	
'*		sCondizione = ""
'*	end if
'Fine AM 12/09/2003	
	
	if sIsa  = "0" then
		sFil = " Isa like '%'"
	else
		sFil = " Isa in ('0','" & sIsa & "')"
	end if
	
	sTabella ="PROV"
	dData =Date ()
	sCond = sFil & sCondizione
	sCondizione = sCond
	nOrder = "" 
	Isa = 0

if 	ckProfile(mid(sMask,3,2),2) then 
		aArray=decodTadesToArray(sTabella,dData,sCondizione,"3","0")
		if isArray(aArray) then
			for i = 0 to (ubound(aArray) -1)	
				sProvincia = sProvincia & ",'" & aArray(i,i,i) & "'"
			next 
			sProvincia = mid(sProvincia,2)
			erase aArray
		end if
		
%>
		<form name="frmInsSede" method="post" action="IMP_InsSedeImpresa.asp">
			<input type="hidden" name="sCodImp" value="<%=sCodImp%>">
			<input type="hidden" name="txtProvS" value="<%=sProvincia%>">
			<input type="hidden" name="txtProvSelez" value="<%=sProv%>">
		</form>
		
<table width="500" border="0">
	<tr align="center">

		<td align="center">
			<a class="textred" href="javascript:frmInsSede.submit()">
			<b>Inserisci nuova sede azienda</b>
			</a>
		</td>

	</tr>
</table>
		
<%
end if 
%>


<br>
<form name="frmVisSedeImpresa" method="post" action>
<input type="hidden" name="Provincia" value>
<input type="hidden" name="sCodImp" value>
<table border="0" cellspacing="0" cellpadding="0" width="500">
	<tr>
		<td align="middle" valign="middle">
<%
'Inizio AM 12/09/2003

		'*aArray=decodTadesToArray(sTabella,dData,sCondizione,"3","0")

		'*Response.Write "<b class='tbltext1'>Provincia</B>  <SELECT ID=cmbProv name=cmbProv onchange='Reload("& sCodImp & ")'>"

		'do until rsRec.EOF
		'*if isArray(aArray) then
		'*	for i = 0 to (ubound(aArray) -1)	
		'*		Response.Write "<OPTION "	
		'*		if sProv = aArray(i,i,i) then
		'*			Response.Write "selected "
		'*		end if
		'*		Response.write "value ='" & aArray(i,i,i) & "'> " & aArray(i,i+1,i) & "</OPTION>"
		'*	next 
		'*	Response.Write "</SELECT>"
		'*	erase aArray
		'*end if
		
		sSetProv = SetProvUorg(Session("IdUorg")) 'ricavato IDUORG
		nProvSedi = len(sSetProv)
		appoTarga =""
%>	
				<select NAME="cmbProv" class="textblack" onchange="Reload(<%=sCodImp%>)">			 
						<option></option>
	    
						<% pos_ini = 1 	                
						    for i=1 to nProvSedi/2
						        sTarga = mid(sSetProv,pos_ini,2)
						if sProv = sTarga then															
						%>
							<option selected value="<%=sTarga%>"><%=DecCodVal("PROV", 0, "", sTarga,"")%>
						<%
						else 
						%> 
							<option value="<%=sTarga%>"><%=DecCodVal("PROV", 0, "", sTarga,"")%>
						<%							 
						end if
						%>
							</option>
                    
						<%  appoTarga = appoTarga & "'" &  sTarga & "'," 
						    pos_ini = pos_ini + 2
						next%>
	             </select>
<!--Fine AM 12/09/2003-->     
		</td>
	</tr>
</table>
</form>

<table width="500" align="center" border="0" cellspacing="2" cellpadding="1">

<%
	nCont = 0 
    if sProv <> "" then
		sSQL="SELECT ID_SEDE, DESCRIZIONE, COD_SEDE,COMUNE FROM SEDE_IMPRESA " &_
			 "WHERE PRV='" & sProv & "' AND ID_IMPRESA=" & _
			 clng(sCodImp) & " ORDER BY DESCRIZIONE"
	else
	    sSQL="SELECT ID_SEDE, DESCRIZIONE, COD_SEDE,COMUNE FROM SEDE_IMPRESA " &_
			 "WHERE PRV IN (" & mid(appoTarga,1,len(appoTarga)-1) & ") AND ID_IMPRESA=" & _
			 clng(sCodImp) & " ORDER BY DESCRIZIONE"
	end if
	
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	set rsImpSede = CC.Execute(sSQL)
	If not rsImpSede.EOF then
%>
		<tr>
			<td width="100" class="tblsfondo" align="center" colspan="2">
				<b class="tbltext1">Azienda/Sede</b>
			</td>
			<td width="150" class="tblsfondo" align="center">
				<b class="tbltext1">Denominazione</b>
			</td>
			<td width="60" class="tblsfondo" align="center">
				<b class="tbltext1">Tipo Sede</b>
			</td>
			<td width="190" class="tblsfondo" align="center">
				<b class="tbltext1">Comune</b>
			</td>
		</tr>
<%
	End if
	
	do while not rsImpSede.EOF
		nCont = 1
		
		'''LC.12/09/2003
		sSqlAzProj=	"SELECT ID_SEDE FROM SEDE_PROJ" &_
					" WHERE ID_IMPRESA=" & clng(sCodImp) &_
					" AND ID_SEDE=" & clng(rsImpSede("ID_SEDE")) &_
					" AND COD_CPROJ='" & mid(session("progetto"),2) & "'"
'PL-SQL * T-SQL  
SSQLAZPROJ = TransformPLSQLToTSQL (SSQLAZPROJ) 
		set rsAzProj= CC.Execute(sSqlAzProj)
		IF not rsAzProj.eof then
		'''FINE LC.
%> 
		<tr class="tblsfondo"> 
			<td width="20" class="tbltext1" align="left">
	  			<img alt="Sede associata al progetto" border="0" src="<%=Session("Progetto")%>/images/chiaveing1.jpg">
			</td>
			<td width="80" align="center" class="tbltext1">
	  				<a class="tblagg" title="Modifica i dati della Sede" href="javascript:ModificaSede(<%=clng(rsImpSede("ID_SEDE"))%>,<%=sCodImp%>)">
					<%
					if clng(rsImpSede("ID_SEDE")) > 9 then 
						Response.Write sCodImp &  " / " & clng(rsImpSede("ID_SEDE"))
					else
						Response.Write sCodImp &  " / &nbsp;" & clng(rsImpSede("ID_SEDE"))
					end if
					%>
					</a>
			</td>
		<%
		'''LC 12/09/2003
		ELSE %>
		<tr class="tblsfondo"> 
			<td width="20" class="tbltext1" align="left">
	  			<a href="javascript:AssAziendaProj(<%=clng(rsImpSede("ID_SEDE"))%>,<%=sCodImp%>)">
					<img alt="Associa la Sede al progetto" border="0" src="<%=Session("Progetto")%>/images/chiaveing2.jpg">
				</a>
			</td>
			<td width="80" align="left" class="tbltext1">
					<span class="tblDett">
					<%
					if clng(rsImpSede("ID_SEDE")) > 9 then 
						Response.Write sCodImp &  " / " & clng(rsImpSede("ID_SEDE"))
					else
						Response.Write sCodImp &  " / &nbsp;" & clng(rsImpSede("ID_SEDE"))
					end if
					%>
					</span>
			  </td>

		<%
		END IF	
		rsAzProj.close()
		set rsAzProj = nothing
		'''FINE LC.
		%>
		  <td width="150" align="center" class="tbldett"><%=rsImpSede("DESCRIZIONE")%>
		  </td>
		  <td width="60" class="tbldett">
				<%
				Response.Write DecCodVal("TSEDE","0",date,rsImpSede("COD_SEDE"),"1") 
				%> 
		  </td>
			<td width="190" class="tbldett" align="center">
				<%=DescrComune(rsImpSede("COMUNE"))%>
			</td>
		</tr>
<% 
	rsImpSede.MoveNext 
	Loop
	rsImpSede.Close 
	set rsImpSede = nothing
%> 
</table>
<form name="frmModSede" action="IMP_ModSedeImpresa.asp" method="post">
<input type="hidden" name="Action" value="Mod">
<input type="hidden" name="idSede" value>
<input type="hidden" name="sCodImp" value>
<input type="hidden" name="txtProvincia" value>
</form>
<br>

<% 
	if nCont = 0 then
%>
		<table width="500" cellpadding="0" cellspacing="0" border="0">
			<tr align="middle">
				<td>
					<span class="tbltext3">Non ci sono sedi associate
					</span>
				</td>
			</tr>
		</table> 
<%
	end if
%>
		<form name="frmModImpresa" method="post" action="IMP_ModImpresa.asp">
			<input type="hidden" name="IdPers" value="<%=sIdPers%>">
			<input type="hidden" name="sCodImp" value="<%=sCodImp%>">
		</form>
		
		<table width="500" cellpadding="0" cellspacing="0" border="0">
			<tr>
				<td align="center">
					<a class="textred" href="javascript:frmModImpresa.submit()" onMouseover="window.status = ''; return true;">
					<img alt="Indietro" border="0" src="<%=Session("Progetto")%>/images/indietro.gif">
					</a>
				</td>
			</tr>
		</table> 

<!--#include Virtual = "/include/closeconn.asp"-->
<!--#include Virtual = "/strutt_coda2.asp"-->
