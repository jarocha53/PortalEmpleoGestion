<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include virtual="/util/portallib.asp"-->
<!--#include virtual="/strutt_testa2.asp"-->
<!--#include virtual="/include/openconn.asp"-->
<!--#include virtual="/include/DecCod.asp"-->
<!--#include Virtual = "/include/SelAreaTerrBandi.asp"--> 
<%
	if ValidateService(session("idutente"),"IMP_VISCENSIMPRESA",cc) <> "true" then 
		response.redirect "/util/error_login.asp"
	end if
%>
<script language="javascript" src="ControlliImp.js"></script>

<script LANGUAGE="Javascript">
//include del file per fare i controlli sulla validit� delle date
<!--#include virtual="/Include/ControlDate.inc"-->
//include del file per l'inserimento dei comuni
<!--#include virtual = "/include/SelComune.js"-->

//include del file per fare i controlli sulla numericit� dei campi
<!--#include virtual="/Include/ControlNum.inc"-->
<!--#include virtual="/Include/ControlString.inc"-->
<!--#include virtual="/Include/help.inc"-->

function Pulisci()
{
	document.frmSedeImpresa.txtComune.value = ""
	document.frmSedeImpresa.txtComuneHid.value = ""
	document.frmSedeImpresa.txtCAPRes.value = ""
	
	document.frmSedeImpresa.txtCentroImpiegoHid.value = ""
	document.frmSedeImpresa.txtCentroImpiego.value = ""
	document.frmSedeImpresa.txtProvinciaOld.value = ""
	
}
	//Funzione per i controlli dei campi 
	function ControllaDati(frmSedeImpresa){
		var Decorrenza
		var Scadenza

// -------- CONTROLLO VALIDITA' DEI CAMPI -------------------------		
		//descrizione
		frmSedeImpresa.txtDescrizione.value = TRIM(frmSedeImpresa.txtDescrizione.value)
		if (frmSedeImpresa.txtDescrizione.value == ""){
			alert("Il campo Descrizione � obbligatorio")
			frmSedeImpresa.txtDescrizione.focus() 
			return false
		}
			if(!ValidateInputStringWithNumber(frmSedeImpresa.txtDescrizione.value))
		{
		alert("Il campo Descrizione � formalmente errato")
		frmSedeImpresa.txtDescrizione.focus() 
		return false
		}
		
		// tipo sede
		if (frmSedeImpresa.cmbCodSede.value == "") {
			alert ("Il campo Tipo sede � obbligatorio")
			frmSedeImpresa.cmbCodSede.focus()
			return false
		}
		
		// indirizzo
		frmSedeImpresa.txtIndirizzo.value = TRIM(frmSedeImpresa.txtIndirizzo.value)
		if (frmSedeImpresa.txtIndirizzo.value == "")
		{
			alert("Il campo Indirizzo � obbligatorio")
			frmSedeImpresa.txtIndirizzo.focus() 
			return false
		}
		/*if(!ValidateInputStringWithNumber(frmSedeImpresa.txtIndirizzo.value))
		{
		alert("Il campo Indirizzo � formalmente errato")
		frmSedeImpresa.txtIndirizzo.focus() 
		return false
		}*/
		

		//provincia
		if (frmSedeImpresa.cmbProv.value == "")
		 {
			alert ("Il campo Provincia � obbligatorio")
			frmSedeImpresa.cmbProv.focus()
			return false
		}

		//frmSedeImpresa.txtComune.value = TRIM(frmSedeImpresa.txtComune.value) ??? MARCO
		
		if (frmSedeImpresa.txtComune.value == "")
		{
			alert("Il campo Comune � obbligatorio")
			frmSedeImpresa.txtComune.focus() 
			return false
		}

		if (frmSedeImpresa.txtCAPRes.value == ""){
			alert("Il campo CAP � obbligatorio")
			frmSedeImpresa.txtCAPRes.focus() 
			return false
		}

		if (!IsNum(frmSedeImpresa.txtCAPRes.value)){
			alert("Il campo CAP deve essere numerico")
			frmSedeImpresa.txtCAPRes.focus() 
			return false
		}

		if (eval(frmSedeImpresa.txtCAPRes.value) == 0){
			alert("Il campo CAP non pu� essere zero")
			frmSedeImpresa.txtCAPRes.focus()
			return false
		}
		
		if (frmSedeImpresa.txtCAPRes.value.length != 5){
			alert("Il campo CAP deve essere di 5 caratteri numerici")
			frmSedeImpresa.txtCAPRes.focus() 
			return false
		}
		
		
		// Telefono e Fax
		frmSedeImpresa.txtSedeTel.value = TRIM(frmSedeImpresa.txtSedeTel.value)
		/*if (frmSedeImpresa.txtSedeTel.value == "")
			{
			alert("Il campo Telefono � obbligatorio")
			frmSedeImpresa.txtSedeTel.focus() 
			return false
			}		
		*/
		if (frmSedeImpresa.txtSedeTel.value != ""){
			if (!IsNum(frmSedeImpresa.txtSedeTel.value)){
				alert("Il campo Telefono deve essere numerico")
				frmSedeImpresa.txtSedeTel.focus() 
				return false
			}
			if (eval(frmSedeImpresa.txtSedeTel.value) == 0){
			alert("Il numero di telefono non pu� essere zero")
			frmSedeImpresa.txtSedeTel.focus()
			return false
			}
		}
				

		if (frmSedeImpresa.txtSedeFax.value != ""){
			if (!IsNum(frmSedeImpresa.txtSedeFax.value)){
				alert("Il numero di Fax deve essere Numerico")
				frmSedeImpresa.txtSedeFax.focus() 
				return false
			}
			if (eval(frmSedeImpresa.txtSedeFax.value) == 0){
			alert("Il numero di fax non pu� essere zero")
			frmSedeImpresa.txtSedeFax.focus()
			return false
			}
		}
		
		frmSedeImpresa.txtSedeEmail.value = TRIM(frmSedeImpresa.txtSedeEmail.value)
		if (frmSedeImpresa.txtSedeEmail.value != ""){
			pippo=ValidateEmail(frmSedeImpresa.txtSedeEmail.value)
			if (pippo==false){
				alert("Indirizzo Email formalmente errato");
				frmSedeImpresa.txtSedeEmail.focus();
				return false
			}
		}

		if (document.frmSedeImpresa.txtCentroImpiego.value == ""){
				alert("Il centro per l'impiego � obbligatorio");
				SelCentroImpiegoMod('txtCentroImpiego');
				return false
		}			
		
//
// Dati Referente
//
		if (frmSedeImpresa.Plavoro.value == "NO")
		{
			return true
		}

		frmSedeImpresa.txtRef.value = TRIM(frmSedeImpresa.txtRef.value)
		if (frmSedeImpresa.txtRef.value == ""){
			alert("Il campo Referente � obbligatorio")
			frmSedeImpresa.txtRef.focus() 
			return false
		}

		if (frmSedeImpresa.cmbRefRuo.selectedIndex == 0) {
			alert ("Il Campo Ruolo � obbligatorio")
			frmSedeImpresa.cmbRefRuo.focus()
			return false
		}
		
		// Telefono referente non �obbligatorio MARCO?
		// Telefono
		frmSedeImpresa.txtTel.value = TRIM(frmSedeImpresa.txtTel.value)
		//if (frmSedeImpresa.txtTel.value == "")
		//{
		//alert("Il campo Telefono  � obbligatorio")
		//frmSedeImpresa.txtTel.focus() 
		//return false 
		//}
		
		
		if (frmSedeImpresa.txtTel.value != ""){
			if (!IsNum(frmSedeImpresa.txtTel.value)){
				alert("Il campo Telefono deve essere numerico")
				frmSedeImpresa.txtTel.focus() 
				return false
			}
		}

		if (frmSedeImpresa.txtFAX.value != ""){
			if (!IsNum(frmSedeImpresa.txtFAX.value)){
				alert("Il numero di Fax deve essere Numerico")
				frmSedeImpresa.txtFAX.focus() 
				return false
			}
		}

		frmSedeImpresa.txtEMail.value = TRIM(frmSedeImpresa.txtEMail.value)
		if (frmSedeImpresa.txtEMail.value == ""){
			alert("Il Campo Email  � obbligatorio")
			frmSedeImpresa.txtEMail.focus() 
			return false
		}

		pippo=ValidateEmail(frmSedeImpresa.txtEMail.value)
		if (pippo==false){
			alert("Il campo Email  � formalmente errato")
			frmSedeImpresa.txtEMail.focus() 
			return false
		}

		frmSedeImpresa.txtLogin.value=TRIM(frmSedeImpresa.txtLogin.value)
		var anyString = frmSedeImpresa.txtLogin.value
		if (frmSedeImpresa.txtLogin.value  == ""){
			alert("Il campo Login � obbligatorio")
			frmSedeImpresa.txtLogin.focus() 
			return false
		}
		/*if(!ValidateInputStringWithNumber(frmSedeImpresa.txtLogin.value)) {
			alert("Il campo Login � formalmente errato")
			frmSedeImpresa.txtLogin.focus() 
			return false
		}*/
		for (var i=0; i<=anyString.length-1; i++){
			if(((anyString.charAt(i) >= "A") && (anyString.charAt(i) <= "Z")) || 
				((anyString.charAt(i) >= "a") && (anyString.charAt(i) <= "z")) || 
				((anyString.charAt(i) >= "0") && (anyString.charAt(i) <= "9")) ||
				(anyString.charAt(i) == ".") || (anyString.charAt(i) == "-") ||
				(anyString.charAt(i) == "_"))
			{
			}
			else
			{		
				alert("Il campo Login � formalmente errato")
				frmSedeImpresa.txtLogin.focus() 
				return false
			}		
		}
		
		
		return true
	}
	
	
	function userfocus()
	{
		document.frmSedeImpresa.txtDescrizione.focus()
	}
	function Reload(pippo){
	}
</script>

<!-- MARCO No body!! <body onload="Javascript:return userfocus()"> -->

        

<%
sCodImp = Request.Form("sCodImp")
sProv =	Request.Form("txtProvS")
sProvSelez= Request.Form("txtProvSelez")

sData =	date

sSQL = "SELECT COD_TIMPR FROM IMPRESA WHERE ID_IMPRESA=" & sCodImp

set rsSede = Server.CreateObject("ADODB.Recordset")

'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
rsSede.open sSQL,CC,3

sCodTimpr = rsSede("COD_TIMPR")
rsSede.Close()
set rsSede = nothing

%>

<form name="Indietro" action="IMP_VisSedeImpresa.asp" method="post">
	<input type="hidden" name="IdPers" value="<%=sIdPers%>">
	<input type="hidden" name="sCodImp" value="<%=sCodImp%>">
	<input type="hidden" name="Provincia" value="<%=sProvSelez%>">
</form>	
<table border="0" width="520" cellspacing="0" cellpadding="0" height="81">
	<tr>
		<td width="530" background="<%=Session("Progetto")%>/images/titoli/strumenti2b.gif" height="81" valign="bottom" align="right">
			<table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
				<tr>
					<td width="100%" valign="top" align="right">
						<b class="tbltext1a">
						Gestione Azienda 
						</b>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>

<!--#include file="IMP_Label.asp" -->

<br>
<table cellpadding="0" cellspacing="0" width="500" border="0">
	<tr height="18">
		<td class="sfondomenu" height="18" width="67%">
			<span class="tbltext0">
			<b>GESTIONE SEDE</b>
			</span>
		</td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
		<td valign="middle" align="right" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1">
			(*) campi obbligatori
		</td>
	</tr>
	<tr width="371" class="SFONDOCOMM">
		<td colspan="3">
			Indicare i dati da inserire relativi alla sede.
			<a href="Javascript:Show_Help('/Pgm/help/GestProgetti/Azienda/Impresa_Censimento/IMP_InsSedeImpresa')">
			<img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0">
			</a>
		</td>
	</tr>   
	<tr height="2">
		<td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif">
		</td>
	</tr>
</table>

<br>

<form method="post" name="frmSedeImpresa" onsubmit="return ControllaDati(this)" action="IMP_CnfSedeImpresa.asp" method="post">
	<input type="hidden" name="Action" value="Ins">
	<input type="hidden" name="sCodImp" value="<%=sCodImp%>">
	<table border="0" cellpadding="0" cellspacing="1" width="450">
		<tr>
			<td align="middle" colspan="2" nowrap>
				<p align="left" class="tbltext1" Style="Cursor:Help" title="Inserire una breve descrizione della sede">
				<b>Descrizione*</b>
				</p>
			</td>
			<td align="left" colspan="2" width="60%">
				<p align="left">
				<input name="txtDescrizione" class="textblack" type="text" style="TEXT-TRANSFORM:uppercase" size="40" maxlength="50">
				</p>
			</td>
		</tr>
		<tr>       
			<td align="middle" colspan="2" nowrap>
				<p align="left">
				<span class="tbltext1" Style="Cursor:HELP" title="Indicare che funzione si svolge nella sede indicata.">
				<b>Tipo sede*</b>
				</span>
				</p>
			</td>
			<td align="left" colspan="2" width="60%">
				<p align="left">
				<span>
<%
				' Devo richiamate la Combo per le provincie
				cmbCodSede = "TSEDE|0|" & date & "||cmbCodSede| ORDER BY DESCRIZIONE"			
				CreateCombo(cmbCodSede)
%>
				</span>
				</p>
            </td>
        </tr>
        <tr>
			<td align="left" colspan="2">
				<p align="left">
				    <span class="tbltext1">
				    <b>Indirizzo*</b>
				    </span>
				</p>
			</td>
			<td align="left" colspan="2" width="60%">
				<p align="left">
				<input name="txtIndirizzo" class="textblack" type="text" style="TEXT-TRANSFORM:uppercase" size="40" maxlength="50">
				</p>
			</td>
        </tr>
        <tr>
			<td align="left" colspan="2" class="tbltext1" width="120">
				<p align="left">
				<b><span>Provincia*</span></b>
				</p>
			</td>
			<td align="left" colspan="2" width="60%" width="400" nowrap>
				<span class="tbltext">
<%'inizio Am 
				'cbbProv = "PROV|0|" & date & "|" & sProvSelez & "|cmbProv' onchange='javascript:Pulisci()|AND CODICE IN(" & sProv & ") ORDER BY DESCRIZIONE"			
				'CreateCombo(cbbProv)				
						
		sSetProv = SetProvUorg(Session("IdUorg")) 'ricavato IDUORG
		sProv= sProvSelez
		nProvSedi = len(sSetProv)
		appoTarga =""
%>	
		<select NAME="cmbProv" class="textblack" onchange="javascript:Pulisci()">			 
					<option></option>
	    
					<% pos_ini = 1 	                
					    for i=1 to nProvSedi/2
					        sTarga = mid(sSetProv,pos_ini,2)
					if sProv = sTarga then															
					%>
						<option selected value="<%=sTarga%>"><%=DecCodVal("PROV", 0, "", sTarga,"")%>
					<%
					else 
					%> 
						<option value="<%=sTarga%>"><%=DecCodVal("PROV", 0, "", sTarga,"")%>
					<%							 
					end if
					%>
						</option>
                    
					<%  appoTarga = appoTarga & "'" &  sTarga & "'," 
					    pos_ini = pos_ini + 2
					next%>
	    </select>
<!--Fine AM 12/09/2003-->  
	
				</span>
			</td>
		</tr>
        <tr>
			<td align="middle" colspan="2" nowrap class="tbltext1" width="120">
				<p align="left">
				<b><span>Comune*</span></b>
				</p>
			</td>
			<td align="left" colspan="2" width="60%" width="400">
				<span class="tbltext">
				<input name="txtComune" class="textblack" type="text" style="TEXT-TRANSFORM:uppercase;" size="40" readonly>
				<input type="hidden" name="txtComuneHid">

<%
					NomeForm="frmSedeImpresa"
					CodiceProvincia="cmbProv"
					NomeComune="txtComune"
					CodiceComune="txtComuneHid"
					Cap="txtCAPRes"
%>
				<a href="Javascript:SelComune('<%=NomeForm%>','<%=CodiceProvincia%>','<%=NomeComune%>','<%=CodiceComune%>','<%=Cap%>')" ID="imgPunto1" name="imgPunto1" onmouseover="javascript:window.status='';return true"><img border="0" src="<%=Session("Progetto")%>/images/bullet1.gif"></a>
				</span>					
			</td>
		</tr>
		<tr>		
			<td align="left" colspan="2" nowrap class="tbltext1">
				<b>CAP*</b>
			</td>			
			<td align="left" colspan="2" width="60%">
				<input name="txtCAPRes" class="textblack" type="text" size="10" maxlength="5">
			</td>
		</tr>	
		<tr>       
            <td align="left" colspan="2">
				<p align="left">
				    <span class="tbltext1"><b>Telefono</b></span>
				</p>
            </td>
            <td align="left" colspan="2" width="60%">
				<p align="left">
				<input name="txtSedeTel" class="textblack" type="text" style="TEXT-TRANSFORM:uppercase" size="25" maxlength="20">
				</p>
            </td>
        </tr>
        <tr>
            <td align="left" colspan="2">
				<p align="left">
				<span class="tbltext1"><b>Fax</b></span>
				</p>
            </td>
            <td align="left" colspan="2" width="60%">
				<p align="left">
				<input name="txtSedeFax" class="textblack" type="text" style="TEXT-TRANSFORM:uppercase" size="25" maxlength="20">
				</p>
            </td>
        </tr>
        <tr>
            <td align="left" colspan="2">
				<p align="left">
				<span class="tbltext1"><b>E-Mail</b></span>
				</p>
            </td>
            <td align="left" colspan="2" width="60%">
				<p align="left">
				<input name="txtSedeEmail" class="textblack" type="text" style="TEXT-TRANSFORM:lowercase" size="40" maxlength="100">					
				</p>
            </td>
        </tr>


<%
	'Response.Write "<b>cod:" & sCodTimpr & "</b>"
	if sCodTimpr <> "03" then
%>
		<tr>
			<td align="left" class="tbltext1" nowrap colspan="2">
				<b>Centro Impiego*</b>
			</td>
			<td align="left" colspan="2" width="60%">
				<span>
				<input name="txtCentroImpiego" class="textblack" type="text" readonly size="40"> 
				<input type="hidden" name="txtCentroImpiegoHid">
				<input type="hidden" name="txtProvinciaOld">
				<a href="Javascript:SelCentroImpiegoMod('txtCentroImpiego')" onmouseover="javascript:window.status='';return true" ID="imgPunto1" name="imgPunto1"><img border="0" src="<%=Session("Progetto")%>/images/bullet1.gif"></a>
				</span>				
			</td>
		</tr>		
<%
	else
	
%>
		<input type="hidden" name="txtCentroImpiego" readonly size="30" value="� un centro impiego"> 
		<input type="hidden" name="txtCentroImpiegoHid" value="CentroImpiego">		
		<input type="hidden" name="txtProvinciaOld">
<%
	end if
	'if Session("progetto") = "/PLAVORO" then
%><!--        <tr>            <td align="left" colspan="2">&nbsp;</td>            <td align="left" colspan="2" width="60%">&nbsp;</td>        </tr>        <tr>            <td align="left" colspan="2">				<p align="left">				<span class="tbltext1">				<b>Referente*</b> (Cognome/Nome)				</span>				</p>            </td>            <td align="left" colspan="2" width="60%">				<p align="left">				<input name="txtRef" class="textblack" type="text" style="TEXT-TRANSFORM:uppercase" size="40" maxlength="50">							</p>            </td>        </tr>        <tr>            <td align="left" colspan="2">				<p align="left">				<span class="tbltext1"><b>Ruolo*</b></span>				</p>            </td>            <td align="left" colspan="2" width="60%">				<p align="left"><%				'Devo richiamate la Combo per le provincie	'			cmbRefRuo = "RUOLO|0|" & date & "|" & sCodRuo & "|cmbRefRuo| ORDER BY DESCRIZIONE"				'			CreateCombo(cmbRefRuo)%>				</span>		        </p>            </td>		</tr>        <tr>            <td align="left" colspan="2">				<p align="left">				<span class="tbltext1"><b>Telefono</b></span>				</p>            </td>            <td align="left" colspan="2" width="60%">				<p align="left">				<input name="txtTel" class="textblack" type="text" style="TEXT-TRANSFORM:uppercase" size="25" maxlength="20">	          </p>            </td>        </tr>        <tr>            <td align="left" colspan="2">				<p align="left">				   <span class="tbltext1"><b>Fax</b></span>				</p>            </td>            <td align="left" colspan="2" width="60%">				<p align="left">					<input name="txtFAX" class="textblack" type="text" style="TEXT-TRANSFORM:uppercase" size="25" maxlength="20">				</p>            </td>        </tr>        <tr>            <td align="left" colspan="2">				<p align="left">				    <span class="tbltext1"><b>E-Mail*</b></span>				</p>            </td>            <td align="left" colspan="2" width="60%">				<p align="left">				<input name="txtEMail" class="textblack" type="text" style="TEXT-TRANSFORM:lowercase" size="40">				</p>            </td>        </tr>        <tr>            <td align="left" colspan="2">				<p align="left">				<span class="tbltext1"><b>Login*</b></span>				</p>            </td>            <td align="left" colspan="2" width="60%">				<p align="left">				<input name="txtLogin" class="textblack" type="text" style="TEXT-TRANSFORM:uppercase" size="40" maxlength="20">		        </p>            </td>        </tr>     -->   
<%
	'end if
%>        
        <tr>
            <td align="left" colspan="4">&nbsp;</td>
		</tr>
	</table>
	<table cellpadding="0" cellspacing="0" width="500" border="0">
		<tr>
			<td align="right">
			<a href="javascript:Indietro.submit()">
			<img src="<%=Session("progetto")%>/images/indietro.gif" border="0">
			</a>
			</td>
			<td width="5%"></td>
			<td align="left">
			<input type="image" name="Invia" src="<%=Session("progetto")%>/images/conferma.gif" value="Registra">
			</td>
		</tr>
	</table>


<%'if Session("progetto") = "/PLAVORO" then%>
<!--	<input type="hidden" name="Plavoro" value="SI">   -->
<%'else%>
	<input type="hidden" name="Plavoro" value="NO">
<%'end if%>
</form>

<!--#include virtual="/strutt_coda2.asp"-->
<!--#include virtual="/include/closeconn.asp"-->
