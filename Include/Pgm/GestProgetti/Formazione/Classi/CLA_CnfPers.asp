<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%sub SpostamentoFasePrima()

	dim nDisClasse,dDisDTRuoloDal,nDisDTRuoloal
	dim nIdPersona,sDesAula,nIdClasse
	dim sDtIniSess,sDtFinSess 
	sErrore = "0"
'	dim sCForm

'	sCForm			= Request.Form("txtCForm")
'	sDIsAula		= Request.Form("txtDIsAula")
	nDisClasse		= CLng(Request.Form("txtDisClasse"))
	nDisDTRuoloal	= Request.Form("txtDisDTRuoloal")		
	dDisDTRuoloDal	= Request.Form("txtDisDTRuoloDal")	
	nIdPersona		= CLng(Request.Form("idpersona"))
	sDesAula		= Request.Form("descaula") 
	nIdClasse		= clng(Request.Form("idclasse"))
	sDtIniSess		= Request.Form("dti")
	sDtFinSess		= Request.Form("dtf")
	
	sDesAula = sDesAula & " (" & sDtIniSess & " - " & sDtFinSess & ")"

	dDisDTRuoloDal = ConvDateToString(dDisDTRuoloDal)
'	Response.Write "<BR>La persona [" & nIdPersona & "] selezionata si trova in altra classe e va spostata."
'	Response.Write "<BR>--------------------------------------------------"
'	Response.Write "<BR>La Sessione in cui si trova inizia :" & ConvStringToDate(ConvDateToString(dDisDTRuoloDal))
'	Response.Write "<BR>La Classe in cui si trova � :" & nDisClasse
'	Response.Write "<BR>--------------------------------------------------"
'	Response.Write "<BR>La Sessione in cui deve andare inizia :" & ConvStringToDate(sDtIniSess)
'	Response.Write "<BR> e finisce il  :" & ConvStringToDate(sDtFinSess)
'	Response.Write "<BR>La Classe in cui deve andare � :" & nIdClasse
'	Response.Write "<BR>--------------------------------------------------"

	CC.Begintrans
	if Date() < CDate(dDisDTRuoloDal) and Date() < CDate(ConvStringToDate(sDtIniSess))  then
		sCaso = "A"
		sSQL = "UPDATE COMPONENTI_CLASSE SET ID_CLASSE=" &_
			  nIdClasse & ", DT_RUOLODAL=TO_DATE('" & sDtIniSess & "','DD/MM/YYYY'),DT_TMST=SYSDATE  WHERE ID_PERSONA = " &_
			   nIdPersona & " AND ID_CLASSE = " & nDisClasse
		sErrore=EseguiNoC(sSQL,CC)
	end if 

	if Date <= CDate(dDisDTRuoloDal) and Date = ConvStringToDate(sDtIniSess) then
		sCaso = "B"
		sSQL = "SELECT ID_CLASSE FROM COMPONENTI_CLASSE " &_
			" WHERE ID_CLASSE = " & nIdClasse & " AND ID_PERSONA = " & nIdPersona
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
		set rsComClassi = CC.Execute(sSQL)
		if not rsComClassi.eof then
			sSQL = "DELETE FROM COMPONENTI_CLASSE " &_
				" WHERE ID_CLASSE = " & nIdClasse & " AND ID_PERSONA = " & nIdPersona
			sErrore=EseguiNoC(sSQL,CC)
		end if
		rsComClassi.Close
		set rsComClassi = nothing
		if sErrore = "0" then
			sSQL = "UPDATE COMPONENTI_CLASSE SET ID_CLASSE=" & nIdClasse &_
				   ", DT_RUOLODAL=" & convDateToDBs(date()) &_
				   ",DT_TMST=SYSDATE WHERE ID_PERSONA = " & nIdPersona &_
				   " AND ID_CLASSE = " & nDisClasse
			sErrore=EseguiNoC(sSQL,CC)
		end if
	end if 
	if CDate(Date) => CDate(dDisDTRuoloDal) and cdate(Date) <= cdate(ConvStringToDate(sDtIniSess)) then
		sCaso = "C"
		if cdate(ConvStringToDate(sDtIniSess)) = CDate(dDisDTRuoloDal) or CDate(Date) = CDate(dDisDTRuoloDal) then
			sCaso = "C1"
			sSQL = "UPDATE COMPONENTI_CLASSE SET ID_CLASSE = " & nIdClasse &_
					",DT_RUOLODAL=" & ConvDateToDbS(ConvStringToDate(sDtIniSess))& ",DT_TMST=SYSDATE WHERE ID_PERSONA = " & nIdPersona &_
					" AND ID_CLASSE = " & nDisClasse
			sErrore=EseguiNoC(sSQL,CC)
		else 
			sSQL = "UPDATE COMPONENTI_CLASSE SET COD_CHIUSURA='P' ,DT_RUOLOAL=" & ConvDateToDBs(Date - 1) &_
				   " ,DT_TMST=SYSDATE WHERE ID_PERSONA = " & nIdPersona & " AND ID_CLASSE = " &_
					 nDisClasse & " AND dt_ruoloal is null"
			sErrore=EseguiNoC(sSQL,CC)
			if sErrore = "0" then
				sSQL = "INSERT INTO COMPONENTI_CLASSE (ID_CLASSE,ID_PERSONA,COD_RUOLO,DT_RUOLODAL,DT_TMST)" &_
					" VALUES(" & nIdClasse & "," & nIdPersona & ",'DI',TO_DATE('" & sDtIniSess & "','DD/MM/YYYY') "&_
					",SYSDATE)"
				sErrore=EseguiNoC(sSQL,CC)
			end if 
		end if 
	end if 

	if cdate(date) > cdate(dDisDTRuoloDal) and cdate(date) > cdate(ConvStringToDate(sDtIniSess)) then
		sCaso = "D"
		sSQL = "SELECT COUNT(*) as conta FROM COMPONENTI_CLASSE WHERE ID_PERSONA = " &_
		 nIdPersona & " AND ID_CLASSE = " & nIdClasse & " AND DT_RUOLOAL IS NOT NULL"
		 
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
		set rsConta = CC.Execute(sSQL)
		' Se trova una classe la aggiorno con la nuova pianificazione ....
		if cint(rsConta("conta")) > 0 then
			sSQL = "UPDATE COMPONENTI_CLASSE SET ID_CLASSE=" & nIdClasse &_
				",DT_RUOLODAL=" & ConvDateToDbS(date) &_  
				",DT_TMST=SYSDATE WHERE ID_PERSONA = " & nIdPersona &_
				" AND ID_CLASSE = " & nDisClasse
			sErrore = "0" 
		else ' ... altrimenti chiudo ed inserisco la nuova occorrenza.
			sSQL = "UPDATE COMPONENTI_CLASSE SET COD_CHIUSURA='P',DT_RUOLOAL=" & ConvDateToDbS(date - 1) &_ 
				",DT_TMST=SYSDATE WHERE ID_PERSONA = " & nIdPersona &_
				" AND ID_CLASSE = " & nDisClasse
			sErrore=EseguiNoC(sSQL,CC)
			if sErrore ="0" then
				sSQL = "INSERT INTO COMPONENTI_CLASSE " &_
					" (ID_PERSONA,COD_RUOLO,ID_CLASSE,DT_RUOLODAL,DT_TMST) " &_
					" VALUES(" & nIdPersona & ",'DI'," & nIdClasse & "," & ConvDateToDbS(date) &_
					", SYSDATE)"
			end if

		end if
		rsConta.close
		set rsConta = nothing 
		if sErrore ="0" then
			sErrore=EseguiNoC(sSQL,CC)
		end if				
	end if 
'	Response.Write "<BR>--------------------------------------------------"
'	Response.Write "<BR>Data di oggi  : " & date
'	Response.Write "<BR>Caso in questione : " & sCaso
'	Response.Write "<BR>" & sSQL
'	Response.Write "<BR>--------------------------------------------------"
'	Response.end 

%>
	<br>
	<table border="0" cellpadding="0" cellspacing="0" width="500">
		<tr>
			<td align="center" class="tblText3">
				<%if sErrore = "0" then
					CC.CommitTrans
					Response.Write "Spostamento della Persona correttamente effettuato"
				else
					CC.RollbackTrans
					Response.Write sErrore 
				end if
				%>
			</td>
		</tr>
	</table>
	<br>
	<%if sErrore = "0" then
		ElencoComponentiClassi nIdPersona,nIdClasse,sDesAula,nBando,cmbSessioni
	End If%>
	<br>
	<meta http-equiv="Refresh" content="3; URL=Javascript:Indietro.submit()">
	
<%end sub
sub AggDiscRipes()

	dim nIDClasse,nIDPers
	dim sDesAula,dIniSess
	dim sSQLInsComponentiClasse
	dim sSQLUpdDomandaIscr
	dim sErrore
	dim sEsGrad
	
	sEsGrad	= Request.Form("esgrad")
	
	nIDClasse = CLng(Request.form("idclasse"))
	nIDPers   = CLng(Request.form("idpersona"))
	dIniSess  = ConvStringToDate(Request.form("DTI"))
	sDesAula  = Request.Form("descAula") 

	CC.BeginTrans	
	sSQLInsComponentiClasse = "INSERT INTO COMPONENTI_CLASSE " &_
		" (ID_CLASSE, ID_PERSONA,COD_RUOLO,DT_RUOLODAL,DT_TMST) " &_
		"VALUES ("& nIDClasse & "," & nIDPers & ",'DI'," &_
		ConvDateToDBs(dIniSess) & ",SYSDATE)"
						
	sErrore=EseguiNoC(sSQLInsComponentiClasse,CC)	
	
	
	
	if sErrore = "0" then
		if sEsGrad="N" then
			sSQLUpdDomandaIscr = "UPDATE DOMANDA_ISCR SET ESITO_GRAD='X',DT_TMST=SYSDATE " &_
							"WHERE ID_PERSONA = " & nIDPers & " AND ID_BANDO = " & nBando
			
			sErrore=EseguiNoC(sSQLUpdDomandaIscr,CC)
		end if	
	end if
	if sErrore = "0" then
		CC.CommitTrans%>
		<br>
		<table border="0" cellpadding="0" cellspacing="0" width="500">
			<tr>
				<td align="center" class="tbltext3">
					Spostamento della Persona correttamente effettuato
				</td>
			</tr>
		</table><br>
 		<%ElencoComponentiClassi nIDPers,nIDClasse,sDesAula,nBando,cmbSessioni%>		
		<br>
		<meta http-equiv="Refresh" content="3; URL=Javascript:Indietro.submit()">
	<%else
		CC.RollBackTrans%>
		<br>
		<table border="0" cellpadding="0" cellspacing="0" width="500">
			<tr>
				<td align="center" class="tblText3">
					<%Response.Write (sErrore)%> 
				</td>
			</tr>
		</table>
		<br>
		<%Indietro()
	end if

end sub


sub Indietro()%>

	<table width="500" cellspacing="0" cellpadding="0" border="0">
		<tr>
			<td align="center">
				<a onmouseover="window.status =' '; return true" title="Indietro" href="Javascript:Indietro.submit()"><img border="0" src="<%=Session("Progetto")%>/images/Indietro.gif"></a>
			</td>
		</tr>
	</table>

<%end sub
sub ImpostaPag()
	select case sModo
		case "1" 
			' ****************************************************************
			' La persona non ha classe assegnata per cui assegno semplicemente 
			' una classe di FASE 1 al discente. 
			AggDiscRipes()
			
		case "2" 
			' ***************************************************************
			' Spostamento di una persona che si trova in una classe in FASE 1
			SpostamentoFasePrima()
			
		case "3"
			' ***************************************************************
			' Sposta il discente da una sezione ad un altra che ha date diverse
			' fra la sessione in cui si trova il discente e quella verso cui 
			' � destinato.
			SpostamentoFaseNonPrimaDiverseDate()

		case "4" 
			' ***************************************************************
			' Sposta il discente da una sezione ad un altra che ha le stesse
			' date della sessione in cui si trova il discente.
			SpostamentoFaseNonPrima()
			
	end select
end sub

sub SpostamentoFaseNonPrima()
	sDesAula		= Request.Form("descaula")
	nDisClasse		= CLng(Request.Form("txtDisClasse"))
	nIdPersona		= CLng(Request.Form("idpersona"))
	nIdClasse		= CLng(Request.Form("idclasse"))
	sDtIniEdi		= Request.Form("dtIniEdi")
	sDtIniSess		= Request.Form("dti")
	sDtchiusura		= Request.Form("txtChiusura")
	sDTRuoloDal		= Request.Form("txtDisDTRuoloDal")
	
'	Response.Write "<BR><Center>sDTRuoloDal =  " & sDTRuoloDal & "</Center>"
'	Response.Write "<BR><Center>Data inizio Edizione verso cui � migrato = " & sDtIniEdi & "</Center>"
'	Response.Write "<BR><Center>Data inizio Sessione verso cui � migrato = " & sDtIniSess & "</Center>"
'	Response.Write "<BR><Center>Data Chiusura ultima Sessione svolta = " & sDtchiusura & "</Center>"

'	24/06/2004 
'	se la data di spostamento e' antecedente o coincide con la data di inizio sessione,
'   chiudo l'occorrenza precedente alla data di fine sessione (per questo mi serve la fase
'	della sessione precedente) e apro la nuova occorrenza alla data d'inizio sessione;
'	in pratica e' stata aggiunta l'if (prima c'era solo la condizione che ora e' nell'else)
'	
	nFase	=	CInt(Request.Form("numFase"))
	nPrgFase		= CInt(Request.Form("prgFase"))
	
	CC.BeginTrans
	if ConvStringToDate(sDtIniSess) >= cdate(date) then
		if ConvStringToDate(sDtIniSess) = cdate(date) then
			sSQL= "UPDATE COMPONENTI_CLASSE SET COD_CHIUSURA='P' ,"&_
				"DT_RUOLOAL=(SELECT MAX(DT_FINSESS) FROM CLASSE_PERCORSO WHERE ID_CLASSE = " & nDisClasse &_
				" AND PRG_FASE = " & ( nFase - 1 ) & " )" &_
				", DT_TMST=SYSDATE WHERE ID_CLASSE=" & nDisClasse &_
				" AND ID_PERSONA=" & nIdPersona
			sErrore=EseguiNoC(sSQL,CC)
			if sErrore = "0" then
				sSQL= "INSERT INTO COMPONENTI_CLASSE (COD_RUOLO,ID_CLASSE,DT_RUOLODAL,ID_PERSONA,DT_TMST)" &_
						" VALUES('DI'," & nIdClasse & ",TO_DATE('" & sDtIniSess & "','DD/MM/YYYY')," &_
						 nIdPersona &_
						",sysdate)"
				sErrore=EseguiNoC(sSQL,CC)
			end if
		else
			sSQL= "UPDATE COMPONENTI_CLASSE SET COD_CHIUSURA='P' ,"&_
				"DT_RUOLOAL=(SELECT MAX(DT_FINSESS) FROM CLASSE_PERCORSO WHERE ID_CLASSE = " & nDisClasse &_
				" AND PRG_FASE = " & ( nPrgFase - 1 ) & " )" &_
				", DT_TMST=SYSDATE WHERE ID_CLASSE=" & nDisClasse &_
				" AND ID_PERSONA=" & nIdPersona
			sErrore=EseguiNoC(sSQL,CC)
			if sErrore = "0" then
				sSQL= "INSERT INTO COMPONENTI_CLASSE (COD_RUOLO,ID_CLASSE,DT_RUOLODAL,ID_PERSONA,DT_TMST)" &_
						" VALUES('DI'," & nIdClasse & ",TO_DATE('" & sDtIniSess & "','DD/MM/YYYY')," &_
						 nIdPersona &_
						",sysdate)"
				sErrore=EseguiNoC(sSQL,CC)
			end if
		end if	
	else
		sSQL= "UPDATE COMPONENTI_CLASSE SET COD_CHIUSURA='P',DT_RUOLOAL=" &_
			ConvDateToDBs(date-1) & ", DT_TMST=" & ConvDateToDb(Now) &_
			" WHERE ID_CLASSE=" & nDisClasse &_
			" AND ID_PERSONA=" & nIdPersona &_
			" AND DT_RUOLOAL IS NULL" 
		sErrore = EseguiNoc(sSQL,CC)
		if sErrore = "0" then
			sSQL= "INSERT INTO COMPONENTI_CLASSE (DT_RUOLODAL,ID_CLASSE,COD_RUOLO,DT_TMST,ID_PERSONA) " &_
				 " VALUES(" & ConvDateToDbS(date) & "," & nIdClasse & ",'DI',SYSDATE," & nIdPersona & ")" 
			sErrore = EseguiNoc(sSQL,CC)
		end if
	end if
	%>
	<br>
	<table border="0" cellpadding="0" cellspacing="0" width="500">
		<tr>
			<td align="center" class="tbltext3">
				<%if sErrore = "0" then
					CC.CommitTrans
					Response.Write "Spostamento della Persona correttamente effettuato"
				else
					CC.RollbackTrans
					Response.Write sErrore 
				end if%>
			</td>
		</tr>
	</table><br>
	<%if sErrore = "0" then
		ElencoComponentiClassi nIdPersona,nIdClasse,sDesAula,nBando,cmbSessioni
	End If
	%>
	<meta http-equiv="Refresh" content="3; URL=Javascript:Indietro.submit()">

<%end sub 

sub SpostamentoFaseNonPrimaDiverseDate()

	sCForm			= Request.Form("txtCForm")
	sDesAula		= Request.Form("descaula")
	nDisClasse		= clng(Request.Form("txtDisClasse"))
	nIdPersona		= clng(Request.Form("idpersona"))
	nIdClasse		= clng(Request.Form("idclasse"))
	sDtIniSess		= Request.Form("dti")
	sDtFinSess		= Request.Form("dtf")
	sDtRuoloDal		= Request.Form("txtDisDTRuoloDal")
	sDtIniSessProv  = Request.Form("txtIniSessProv")
	nPrgFase		= CInt(Request.Form("prgFase"))
	
	if ConvStringToDate(sDtFinSess) > cdate(date) then
		sDtFinSess = ConvDateToDbs(date)
	else
		sDtFinSess = ConvDateToDbs(ConvStringToDate(sDtFinSess))
	end if	

'	Response.Write "<BR><Center>Data inizio Sessione verso cui � migrato = " & sDtIniSess & "</Center>"
'	Response.Write "<BR><Center>Data Chiusura ultima Sessione svolta = " & sDtchiusura & "</Center>"
	CC.BeginTrans

	if nPrgFase > 0  then ' Il discente non sta frequentando nessuna sessione formativa.
		if cdate(ConvStringToDate(sDtIniSessProv)) = cdate(ConvStringToDate(sDtRuoloDal)) then
			sSQL= "UPDATE COMPONENTI_CLASSE SET DT_TMST = SYSDATE, DT_RUOLODAL =" &_
				ConvDateToDBs(ConvStringToDate(sDtIniSess)) & ", ID_CLASSE =" & nIdClasse & " WHERE ID_CLASSE=" &_
				nDisClasse & " AND ID_PERSONA=" & nIdPersona
			sErrore=EseguiNoC(sSQL,CC)
		else
			sSQL= "UPDATE COMPONENTI_CLASSE SET COD_CHIUSURA='P' ,"&_
				"DT_RUOLOAL=(SELECT MAX(DT_FINSESS) FROM CLASSE_PERCORSO WHERE ID_CLASSE = " & nDisClasse &_
				" AND PRG_FASE = " & ( nPrgFase - 1 ) & " )" &_
				", DT_TMST=SYSDATE WHERE ID_CLASSE=" & nDisClasse &_
				" AND ID_PERSONA=" & nIdPersona
			sErrore=EseguiNoC(sSQL,CC)
			if sErrore = "0" then
				sSQL= "INSERT INTO COMPONENTI_CLASSE (COD_RUOLO,ID_CLASSE,DT_RUOLODAL,ID_PERSONA,DT_TMST)" &_
						" VALUES('DI'," & nIdClasse & ",TO_DATE('" & sDtIniSess & "','DD/MM/YYYY')," &_
						 nIdPersona &_
						",sysdate)"
				sErrore=EseguiNoC(sSQL,CC)
			end if
		end if
	else ' Il discente sta frequentando una sessione formativa
		if cdate(ConvStringToDate(sDtIniSessProv)) = cdate(ConvStringToDate(sDtRuoloDal)) and cdate(date) <= cdate(ConvStringToDate(sDtIniSessProv)) then
			sSQL= "UPDATE COMPONENTI_CLASSE SET DT_TMST = SYSDATE, DT_RUOLODAL =" &_
				ConvDateToDBs(ConvStringToDate(sDtIniSess)) & ", ID_CLASSE =" & nIdClasse & " WHERE ID_CLASSE=" &_
				nDisClasse & " AND ID_PERSONA=" & nIdPersona
			sErrore=EseguiNoC(sSQL,CC)
		else
			sSQL= "UPDATE COMPONENTI_CLASSE SET COD_CHIUSURA='P' ,DT_RUOLOAL=" & ConvDateToDbs( Date  - 1 ) &_
				  ", DT_TMST=SYSDATE WHERE ID_CLASSE=" & nDisClasse &_
				  " AND ID_PERSONA=" & nIdPersona 

			sErrore=EseguiNoC(sSQL,CC)
			if sErrore = "0" then
				sSQL= "INSERT INTO COMPONENTI_CLASSE (COD_RUOLO,ID_CLASSE,DT_RUOLODAL,ID_PERSONA,DT_TMST)" &_
						" VALUES('DI'," & nIdClasse & ",TO_DATE('" & sDtIniSess & "','DD/MM/YYYY')," &_
						 nIdPersona &_
						",sysdate)"
				sErrore=EseguiNoC(sSQL,CC)
			end if
		end if
	end if
%> 
	<br>
	<table border="0" cellpadding="0" cellspacing="0" width="500">
		<tr>
			<td align="center" class="tbltext3">
				<%if sErrore = "0" then
					CC.CommitTrans
					Response.Write "Spostamento della Persona correttamente effettuato"
				else
					CC.RollbackTrans
					Response.Write sErrore 
				end if%>
			</td>
		</tr>
	</table><br>
	<%if sErrore = "0" then
		ElencoComponentiClassi nIdPersona,nIdClasse,sDesAula,nBando,cmbSessioni 
	End If
	%>
	<meta http-equiv="Refresh" content="3; URL=Javascript:Indietro.submit()">

<%end sub

sub SpostamentoFaseNonPrimaStesseDate()
	sDesAula		= Request.Form("descaula")
	nDisClasse		= CLng(Request.Form("txtDisClasse"))
	nIdPersona		= CLng(Request.Form("idpersona"))
	nIdClasse		= CLng(Request.Form("idclasse"))
	sDtIniSess		= Request.Form("dti")
	sDTRuoloDal		= Request.Form("txtDisDTRuoloDal")
		
'	Response.Write "<BR><Center>sDTRuoloDal =  " & sDTRuoloDal & "</Center>"
'	Response.Write "<BR><Center>Data inizio Edizione verso cui � migrato = " & sDtIniEdi & "</Center>"
'	Response.Write "<BR><Center>Data inizio Sessione verso cui � migrato = " & sDtIniSess & "</Center>"
'	Response.Write "<BR><Center>Data Chiusura ultima Sessione svolta = " & sDtchiusura & "</Center>"
			
	sSQL= "UPDATE COMPONENTI_CLASSE SET DT_RUOLODAL=" &_
		ConvDateToDBs(ConvStringToDate(sDtIniSess)) &_
		", DT_TMST=" & ConvDateToDb(Now) & ",ID_CLASSE = " & nIdClasse &_
		" WHERE ID_CLASSE= " & nDisClasse &_
		" AND ID_PERSONA = " & nIdPersona &_
		" AND DT_RUOLODAL= " & ConvDateToDBs(sDTRuoloDal)
	' Response.Write "<BR> 9 " & sSQL
	sErrore = EseguiNoc(sSQL,CC)
	%>
	<br>
	<table border="0" cellpadding="0" cellspacing="0" width="500">
		<tr>
			<td align="center" class="tblText3">
				<%if sErrore = "0" then
					Response.Write "Spostamento della Persona correttamente effettuato"
				else
					Response.Write sErrore 
				end if%>
			</td>
		</tr>
	</table><br>
	<%if sErrore = "0" then
		ElencoComponentiClassi nIdPersona,nIdClasse,sDesAula,nBando,cmbSessioni
		Indietro()
	end if


end sub%>

<!-------------------------------------------------------------------------->
<!--	FINE BLOCCO ASP		-->
<!--		MAIN			-->
<!--#include virtual = "/Strutt_Testa2.asp"-->
<!--#include virtual = "/util/dbutil.asp"-->
<!--#include virtual = "/include/ControlDateVB.asp"-->
<!--#include virtual = "/include/SysFunction.asp"-->
<!--#include virtual = "/include/openconn.asp"-->
<!--#include virtual = "/include/ElencoClassi.asp"-->
<!--#include file = "CLA_Inizio.asp"-->
<%
if ValidateService(session("idutente"),"CLA_SelClassi",CC) <> "true" then 
	response.redirect "/util/error_login.asp"
end if

dim sModo,cmbSessioni,nBando

nBando		= clng(Request.Form ("cmbBando"))
cmbSessioni = Request.Form ("cmbSessioni") 
sModo		= Request.Form("modo")


Inizio()
ImpostaPag()%>
<!--#include virtual = "/include/closeconn.asp"-->
<!--#include virtual = "/Strutt_Coda2.asp"-->
<form name="Indietro" method="post" action="CLA_SelAule.asp">
	<input type="hidden" name="cmbBando" value="<%=nBando%>">
	<input type="hidden" name="cmbSessioni" value="<%=cmbSessioni%>">
</form>

<!--	FINE BLOCCO ASP		-->
<!--	FINE  MAIN			-->
<!-------------------------------------------------------------------------->
