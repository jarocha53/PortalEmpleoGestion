<!--#include virtual ="/Strutt_Testa2.asp"-->
<!--#include virtual ="/util/portallib.asp"-->
<!--#include virtual = "/include/ControlDateVB.asp"-->
<!--#include virtual = "/include/openconn.asp"-->
<!--#include virtual = "/include/SelAreaTerrBandi.asp"-->	
<!--#include file = "CLA_Inizio.asp"-->
<script LANGUAGE="JavaScript">
<!--#include virtual = "/include/help.inc"-->	
<!--#include virtual = "/include/ControlCodFisc.inc"-->
<!--#include virtual = "/include/ControlString.inc"-->
function ControllaDati(frmCodFisc){
		//Codice Fiscale Obbligatorio
		if (frmCodFisc.txtCodFisc.value == ""){
			alert("Codice Fiscale obbligatorio")
			frmCodFisc.txtCodFisc.focus() 
			return false
		}
		frmCodFisc.txtCodFisc.value = TRIM(frmCodFisc.txtCodFisc.value)
		//Codice Fiscale deve essere di 16 caratteri
		if (frmCodFisc.txtCodFisc.value.length != 16){
			alert("Il codice fiscale deve essere di 16 caratteri")
			frmCodFisc.txtCodFisc.focus() 
			return false
		}
		if (!ControllChkCodFisc(frmCodFisc.txtCodFisc.value.toUpperCase()) ) {
			alert("Formato del Codice Fiscale errato")
			frmCodFisc.txtCodFisc.focus() 
			return false
		}
	return true
}
</script>
<%

if ValidateService(session("idutente"),"CLA_SelClassi",CC) <> "true" then 
	response.redirect "/util/error_login.asp"
end if
 
dim sCodSess,sAula,nIdClasse
dim sDtIniSess,sDtFinSess
dim nBando,nPrgFase

sCodSess	= Request.Form ("sessione")
sAula		= Request.Form("Aula")
nIdClasse	= CLng(Request.Form("classe"))
sDtIniSess  = Request.Form("DTI")
sDtFinSess	= Request.Form("DTF")
nBando		= CLng(Request.Form ("Bando"))
nPrgFase	= CInt(Request.Form("PrgFase"))

Inizio()
InfoProc()
ImpostaPag()%>
<form name="Indietro" method="post" action="CLA_SelAule.asp">
	<input type="hidden" name="cmbBando" value="<%=nBando%>">
	<input type="hidden" name="cmbSessioni" value="<%=sCodSess%>">
</form>

<%sub ImpostaPag()%>
	<form method="post" action="CLA_VisPers.asp" onsubmit="return ControllaDati(this)" name="frmCodFisc">
	<table border="0" cellspacing="2" cellpadding="1" width="500">
		<tr>
			<td class="tbltext1" align="left" width="125">
				<b>Bando :</b>
			</td>	
			<td align="left" class="textBlack">
				<b><%=DecBando(nBando)%></b>
			</td>
		</tr>
		<tr>
			<td class="tbltext1" align="left" width="125">
				<b>Sessione Formativa :</b>
			</td>	
			<td align="left" class="textBlack">
				<b><%=DecFaseBando(nBando,sCodSess)%></b>
			</td>
		</tr>
		<tr align="center">
			<td class="tbltext1" align="left" width="150">
				<b>Classe :</b>
			</td>	
			<td align="left" class="textBlack" colspan="2">
				<b><%=sAula%> ( <%=sDtIniSess%> - <%=sDtFinSess%> )</b>
			</td>
		</tr>
	</table>
	<input type="hidden" name="PrgFase" value="<%=nPrgFase%>">
	<input type="hidden" name="Aula" value="<%=sAula%>">
	<input type="hidden" name="Classe" value="<%=nIDClasse%>">
	<input type="hidden" name="DTI" value="<%=sDtIniSess%>">
	<input type="hidden" name="DTF" value="<%=sDtFinSess%>">
	<input type="hidden" name="codsess" value="<%=sCodSess%>">
	<input type="hidden" name="txtBando" value="<%=nBando%>">
	<%if CDate(ConvDateToString(Date())) >= CDate(sDtIniSess) and CDate(ConvDateToString(Date())) <= CDate(sDtfSess) then%>
		<table border="1" cellspacing="2" cellpadding="1" width="500">
			<tr>
				<td align="left" class="tbltext1" align="center" width="300"><b>Data : </b></td>
				<td align="left" width="200">
					<input class="textBlack" name="DataRic" value="<%=ConvDateToString(Now())%>" type="text">
				</td>		  
			</tr>
		</table><br>
<%	end if%>
	<table border="0" cellspacing="2" cellpadding="1" width="500">
		<tr align="center">
			<td class="tbltext1" align="left" width="300">
				<b>Indicare il Codice Fiscale della Persona da includere : </b>
			</td>
			<td class="tbltext1" align="left">
				<input style="TEXT-TRANSFORM: uppercase" class="textBlack" size="22" maxlength="16" name="txtCodFisc">
			</td>
		</tr>
	</table><br>
	<table border="0" cellspacing="2" cellpadding="1" width="500">
		<tr>
			<td align="right" width="50%">
				<input type="image" src="<%=Session("Progetto")%>/images/Conferma.gif" value="Conferma" title="Conferma">
			</td>
			<td align="left" width="50%">
				<a onmouseover="window.status =' '; return true" href="JavaScript:Indietro.submit()" title="Indietro"><img border="0" src="<%=Session("Progetto")%>/images/indietro.gif"></b></a>
			</td>
		</tr>
	</table>
	</form>
<%end sub%>


<%sub InfoProc()%>

	<table cellpadding="0" cellspacing="0" width="500" border="0">
		<tr height="18">
			<td class="sfondomenu" height="18" width="50%">
				<span class="tbltext0"><b>&nbsp;GESTIONE COMPONENTI CLASSI</b>
				</span>
			</td>
			<td width="5%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
			<td valign="middle" align="right" width="40%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1"></td>
		</tr>
		<tr width="371" class="SFONDOCOMM">
			<td colspan="3">
				Indicare il codice fiscale del candidato che si vuole includere
				alla classe scelta.
				<a onmouseover="window.status =' '; return true" href="Javascript:Show_Help('/Pgm/Help/gestprogetti/Formazione/Classi/CLA_VisAule')">
				<img src="<%=Session("Progetto")%>/images/Help.gif" border="0" align="right" alt="Help">
			</td>
		</tr>
		<tr height="2">
			<td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>

<%end sub%>
<!--#include virtual ="/include/closeconn.asp"-->
<!--#include virtual ="/Strutt_Coda2.asp"-->
