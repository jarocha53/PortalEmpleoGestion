<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<script language="Javascript" src="/Include/help.inc"></script>
<%function CheckVincitore(nBando,nIDPersona)
	dim sSQL,rsDomanda
	dim aRisult(1)
	
	sSQL = "SELECT COD_ESITO, ESITO_GRAD FROM " &_
		"DOMANDA_ISCR WHERE ID_PERSONA=" & nIDPersona &_
		" AND ID_BANDO = " & nBando
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	set rsDomanda = CC.Execute(sSQL)
	if not rsDomanda.EOF then
		CheckVincitore = rsDomanda("COD_ESITO") & " | " & rsDomanda("ESITO_GRAD")
	else
		CheckVincitore = " | "
	end if	
	rsDomanda.close
	set rsDomanda = nothing 

end function 

function ClasseAppartenenza(nIdPer)
	dim sSQLClasse,rsClasse
	
	sSQLClasse = "SELECT ID_CLASSE FROM COMPONENTI_CLASSE WHERE " &_
		" ID_PERSONA = " & nIdPer & " AND DT_RUOLOAL IS NULL"  
'PL-SQL * T-SQL  
SSQLCLASSE = TransformPLSQLToTSQL (SSQLCLASSE) 
	set rsClasse = CC.Execute(sSQLClasse)
	if not rsClasse.eof then
		ClasseAppartenenza = clng(rsClasse("ID_CLASSE"))
	else
		ClasseAppartenenza = 0
	end if
	rsClasse.close
	set rsClasse = nothing
	
end function

%>
<!--#include virtual = "/include/SelAreaTerrBandi.asp"-->	
<!--#include virtual = "/Strutt_Testa2.asp"-->
<!--#include virtual = "/include/ControlDateVB.asp"-->
<!--#include virtual = "/include/openconn.asp"-->
<!--#include virtual = "/include/ControlFase.asp"-->
<!--#include file = "CLA_Inizio.asp"-->
<%

if ValidateService(session("idutente"),"CLA_SelClassi",CC) <> "true" then 
	response.redirect "/util/error_login.asp"
end if


dim sCF,nBando,sCodSess
dim sAula,nIDClasse,dDtIniSessDest
dim dDtFinSessDest,sDataRic
dim aVincitore()

dim nIDPersona, sCognome, sNome,sNascita 

dim nPrgFase		' Fase della classe selezionata
dim nPrgFaseIdPer	' Fase della classe assegnata alla persona ricercata.

sCF			= UCase(Request.Form("txtCodFisc"))
nBando		= CLng(Request.Form("txtBando"))
sCodSess	= Request.Form("codsess")
sAula		= Request.Form("Aula")
nIDClasse	= CLng(Request.Form("Classe"))
dDtIniSessDest	= Request.Form("dti")
dDtFinSessDest	= Request.Form("dtf")
sDataRic	= Request.Form("DataRic")
nPrgFase	= CInt(Request.Form("PrgFase"))

Inizio()
InfoProc()
ImpostaPag()%>

<form name="Indietro" method="post" action="CLA_VisAule.asp">
<input type="hidden" name="Bando" value="<%=nBando%>">
<input type="hidden" name="sessione" value="<%=sCodSess%>">
<input type="hidden" name="Aula" value="<%=sAula%>">
<input type="hidden" name="classe" value="<%=nIDClasse%>">
<input type="hidden" name="DTI" value="<%=dDtIniSessDest%>">
<input type="hidden" name="DTF" value="<%=dDtFinSessDest%>">
</form>
<!--#include virtual ="/Strutt_Coda2.asp"-->
<!--#include virtual = "/include/closeconn.asp"-->
<%sub Indietro%>
	<br>
	<table width="500px" cellspacing="0" cellpadding="0" border="0">
		<tr>
			<td align="center">
				<a onmouseover="window.status =' '; return true" title="Indietro" href="Javascript:Indietro.submit()"><img border="0" src="<%=Session("Progetto")%>/images/Indietro.gif"></a>
			</td>
		</tr>
	</table>
<%end sub%>

<%sub IndietroWidthSubmit%>
	<br>
	<table width="500px" cellspacing="0" cellpadding="0" border="0">
		<tr>
			<td align="right" width="40%">
				<input title="Conferma" type="image" src="<%=Session("Progetto")%>/images/conferma.gif" value="Conferma" id="image1" name="image1">
			</td>
			<td align="right" width="20%">&nbsp;</td>
			<td align="left" width="40%">
				<a onmouseover="window.status =' '; return true" title="Indietro" href="Javascript:Indietro.submit()"><img border="0" src="<%=Session("Progetto")%>/images/Indietro.gif"></a>
			</td>
		</tr>
	</table>
<%end sub%>


<%sub InfoProc()%>
	<table cellpadding="0" cellspacing="0" width="500" border="0">
		<tr height="18">
			<td class="sfondomenu" height="18" width="50%">
				<span class="tbltext0"><b>&nbsp;GESTIONE COMPONENTI CLASSI</b>
				</span>
			</td>
			<td width="5%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
			<td valign="middle" align="right" width="40%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1"></td>
		</tr>
		<tr width="371" class="SFONDOCOMM">
			<td colspan="3">
				Confermando la scelta il candidato ritrovato sar� spostato nella
				classe.
				<a onmouseover="window.status =' '; return true" href="Javascript:Show_Help('/Pgm/Help/gestprogetti/Formazione/Classi/CLA_VisPers')">
				<img src="<%=Session("Progetto")%>/images/Help.gif" border="0" align="right" alt="Help">
			</td>
		</tr>
		<tr height="2">
			<td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>

<%end sub

sub ImpostaPag
	dim nCheck,sMotiSosp%>
	<form method="post" action="CLA_CnfPers.asp" name="frmCodFisc">
	<input type="hidden" name="cmbBando" value="<%=nBando%>">
	<input type="hidden" name="cmbSessioni" value="<%=sCodSess%>">
	<input type="hidden" name="txtCForm" value="<%=sCentro%>">
	<table border="0" cellspacing="2" cellpadding="1" width="500px">
		<tr align="center">
			<td class="tbltext1" align="left" width="150">
				<b>Bando :</b>
			</td>	
			<td align="left" class="textBlack">
				<b><%=DecBando(nBando)%></b>
			</td>
		</tr>
		<tr align="center">
			<td class="tbltext1" align="left" width="150">
				<b>Sessione Formativa :</b>
			</td>	
			<td align="left" class="textBlack" colspan="2">
				<b><%=DecFaseBando(nBando,sCodSess)%></b>
			</td>
		</tr>
	</table>
	<%sSQLDatiPersona = "SELECT A.ID_PERSONA,A.COGNOME, A.NOME,A.DT_NASC from PERSONA " &_
			"A, DOMANDA_ISCR B WHERE A.COD_FISC='" & sCF &_ 
			"' AND B.ID_BANDO='" & nBAndo & "' AND A.ID_PERSONA = B.ID_PERSONA"
'PL-SQL * T-SQL  
SSQLDATIPERSONA = TransformPLSQLToTSQL (SSQLDATIPERSONA) 
	set rsPersona = CC.Execute(sSQLDatiPersona)
	if rsPersona.eof then%>
		<br>
		<table border="0" cellpadding="0" cellspacing="0" width="500">
			<tr>
				<td align="center" class="tblText3">
					Persona non presente in archivio oppure <br>non 
					appartenente al bando selezionato
				</td>
			</tr>
		</table>
		<br>
		<%Indietro()%>
	<%else
		nIDPersona	= CLng(rsPersona("ID_PERSONA"))
		sCognome	= rsPersona("COGNOME")
		sNome		= rsPersona("NOME")
		sNascita	= ConvDateToString(rsPersona("DT_NASC")) 

	' EPILI 25/02/2003 
	' Controllo semplicemente se la persona � presente in UTE_SOPSESI,
	' In questo caso non ha diritto allo spostamento perch� sospesa .

		sSQLSospeso = "SELECT COD_SOSP,DT_DAL FROM UTE_SOSPESI " &_
			" WHERE ID_PERSONA = " & nIDPersona &_ 
			" AND NVL(DT_DAL, SYSDATE) >= SYSDATE  "
'PL-SQL * T-SQL  
SSQLSOSPESO = TransformPLSQLToTSQL (SSQLSOSPESO) 
		set rsAssenza = CC.Execute(sSQLSospeso)
		if not rsAssenza.eof then
			nCount = 1
			sMotiSosp = DecCodVal("MSOSP", "0", Date(), rsAssenza("COD_SOSP"), 1)
			dDTSospDal = ConvDateToString(rsAssenza("DT_DAL"))
		else
			nCount = 0
		end if 
		rsAssenza.close
		set rsAssenza = nothing 
		
		'Response.Write sSQL
		'sSQL =  " SELECT DISTINCT RC.ID_PERSONA,SF.COD_SESSIONE" &_
		'	" FROM REG_CLASSE RC, SESS_FORM SF, COMPONENTI_CLASSE CC,CLASSE C " &_
		'	" WHERE RC.ID_PERSONA=" & sIDPersona &_
		'	" AND RC.COD_SESSFOR=" & sCodSess &_
		'	" AND CC.ID_PERSONA=RC.ID_PERSONA" &_
		'	" AND CC.ID_CLASSE=C.ID_CLASSE" &_
		'	" AND C.ID_EDIZIONE=SF.ID_EDIZIONE" &_
		'	" AND RC.DT_ASSEN>=SF.DT_INISESS" &_
		'	" AND RC.DT_ASSEN<=SF.DT_FINSESS" &_
		'	" HAVING SUM(NVL(NUM_ORE, 0)) * 60 + SUM(NVL(MINUTI, 0))> 240"			
		'	" GROUP BY RC.ID_PERSONA, RC.DT_ASSEN, SF.COD_SESSIONE"

		'-----------------------------------------------------------------------------------------------------
		'Commento di daniele ciocca 23/10/2002
		'questa select che prende solamente le assenze maggiori di 240 minuti tra data inizio e data fine
		' sessione formativa selezionata  dove l'assenza e' compresa
		'tra la data inizio e la data fine della sessione formativa e' momentaneamente sospesa.Attendo informazioni da Daniele Blasi			 
		'			" SELECT DISTINCT" &_ 
		'			" RC.ID_PERSONA, RC.DT_ASSEN, SF.COD_SESSIONE" &_
		'			" FROM REG_CLASSE RC, PERSONA P, COMPONENTI_CLASSE CC," &_ 
		'			" CLASSE C, EDIZIONE E, SESS_FORM SF" &_
		'			" WHERE RC.ID_PERSONA = P.ID_PERSONA" &_
		'			" AND P.ID_PERSONA = CC.ID_PERSONA" &_
		'			" AND CC.ID_CLASSE = C.ID_CLASSE" &_
		'			" AND C.ID_EDIZIONE = E.ID_EDIZIONE" &_
		'			" AND E.ID_EDIZIONE = SF.ID_EDIZIONE" &_
		'			" AND (RC.ID_PERSONA = '" & sIDPersona & "') AND (RC.COD_SESSFOR = '" & sCodSess & "')" &_ 
		'			" AND (SF.COD_SESSIONE = '" & sCodSess & "')" &_
		'			" AND (RC.DT_ASSEN >= SF.DT_INISESS" &_
		'			" OR (RC.DT_ASSEN <= SF.DT_FINSESS)" &_
		'------------------------------------------------------------------------------------------------------					
		if nCount > 0 then%>
			<br><br>
			<table border="0" width="500">
			<tr height="23">
				<td align="center" class="tblText3">
					Il discente � stato sospeso dal <%=dDTSospDal%> <br>
					per <%=sMotiSosp%> dalla classe assegnata.
				</td>
			</tr>
			</table>
			<br>
			<%Indietro()
		else
'			sSQL1 = "SELECT D.DT_INISESS,D.DT_FINSESS, B.ID_EDIZIONE,C.NOME, C.COGNOME,A.ID_CLASSE,A.DT_RUOLODAL, " &_
'				"A.DT_RUOLOAL,B.ID_AULA " &_
'				"FROM COMPONENTI_CLASSE A,CLASSE B ,PERSONA C,SESS_FORM D WHERE " &_
'				"A.ID_CLASSE=B.ID_CLASSE " &_
'				"AND C.ID_PERSONA = A.ID_PERSONA " &_
'				"AND B.ID_EDIZIONE = D.ID_EDIZIONE " &_
'				"AND D.COD_SESSIONE = '" & sCodSess & "'" &_
'				"AND C.ID_PERSONA=" & sIDPersona & " AND B.ID_BANDO = '" &_
'				sBando & "' AND DT_RUOLOAL IS NULL"
			
			aPrgFaseIdPer = SelPrgFase(nIDPersona)
			' aSelezione(0) = PRG_FASE
			' aSelezione(1) = COD_SESSIONE
			' aSelezione(2) = DT_INISESS
			' aSelezione(3) = DT_FINSESS
			dDataOdierna  = Date() 
			sSposta = ""
			nDisClasse = 0
			' La classe in cui � assegnata.
			nDisClasse = ClasseAppartenenza(nIDPersona) 
			' Response.Write "<BR> aPrgFaseIdPer(0) = " & aPrgFaseIdPer(0)
			select case aPrgFaseIdPer(0) ' Situazione della persona ( FASE )
				case 0 ' Al momento non sta facendo nessuna sessione
					' In questo caso devo capire quale sia la fase successiva.
					if nDisClasse > 0 then
						aPrgFaseProssimo = SelPrgFaseSucc(nIDPersona)
						if aPrgFaseProssimo(0) > 0 then ' Esiste una sessione successiva
							if nPrgFase = aPrgFaseProssimo(0) then
								if CDate(dDataOdierna) <= CDate(ConvStringToDate(dDtIniSessDest)) then
									sSposta = "Ok"
								else
									if CDate(ConvStringToDate(dDtFinSessDest)) = CDate(aPrgFaseIdPer(3)) and CDate(ConvStringToDate(dDtIniSessDest)) = CDate(aPrgFaseIdPer(2)) then
										sSposta = "Ok"
									else
										sSposta = "La sessione della classe selezionata � gi� cominciata"
									end if
								end if
							else
									sSposta = "La sessione della persona � diversa <BR>da quella della classe di destinazione"
							end if
						else ' Ha terminato il suo percorso formativo
							sSposta = "La persona ha concluso il suo percorso formativo"
						end if 
					else ' Se non ha una classe assegnata vuol dire che � un ripescato.
						if nPrgFase = "1" then
							sSposta = "Ok"
						else
							sSposta = "Il discente deve ancora effettuare la prima sessione formativa"
							sSposta = "Ok"
						end if
					end if	
				case 1 ' Si trova alla 1� Fase
					' Posso spostarla in qualsiasi altra edizione successiva alla data odierna.
					if CDate(dDataOdierna) <= CDate(ConvStringToDate(dDtIniSessDest)) and nPrgFase = 1 then
						sSposta = "Ok"
					else ' Se la sessione di destinazione ha la stessa 
						if CDate(ConvStringToDate(dDtIniSessDest)) = CDate(aPrgFaseIdPer(2)) then
							sSposta = "Ok"
						else
							if nPrgFase > 1 then 
								if CDate(ConvStringToDate(dDtFinSessDest)) = CDate(aPrgFaseIdPer(3)) and CDate(ConvStringToDate(dDtIniSessDest)) = CDate(aPrgFaseIdPer(2)) then
									sSposta = "Ok"
								else
									sSposta = "La sessione della classe selezionata � gi� cominciata"
								end if
							else
								sSposta = "Non � possibile spostare persone <BR>in sessioni iniziate in periodi diversi."
							end if
						end if
					end if
					 
				case else ' Si trova in fasi successive
					' La devo spostare in altre edizioni che abbiano la stessa fase.
					if nPrgFase = aPrgFaseIdPer(0) then
						if CDate(dDataOdierna) <= CDate(ConvStringToDate(dDtIniSessDest)) then
							sSposta = "Ok"
						else
							if CDate(ConvStringToDate(dDtFinSessDest)) = CDate(aPrgFaseIdPer(3)) and CDate(ConvStringToDate(dDtIniSessDest)) = CDate(aPrgFaseIdPer(2)) then
								sSposta = "Ok"
							else
								sSposta = "La sessione della classe selezionata � gi� cominciata"
							end if
						end if
					else
						sSposta = "La persona si trova in una fase diversa da quella appartente alla classe selezionata"
					end if
			end select%>
			<table border="0" cellspacing="2" cellpadding="1" width="500px">
				<tr align="center">
					<td colspan="2" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif">
					</td>
				</tr>
				<tr align="center">
					<td colspan="2">&nbsp;
					</td>
				</tr>
				<tr align="center">
					<td class="tbltext1" align="left" width="150">
						<b>Cognome :</b>
					</td>	
					<td align="left" class="textBlack">
						<b><%=sCognome%></b>
					</td>
				</tr>
				<tr align="center">
					<td class="tbltext1" align="left" width="150">
						<b>Nome :</b>
					</td>	
					<td align="left" class="textBlack">
						<b><%=sNome%></b>
					</td>
				</tr>
				<tr align="center">
					<td class="tbltext1" align="left" width="150">
						<b>Data di Nascita :</b>
					</td>	
					<td align="left" class="textBlack">
						<b><%=sNascita%></b>
					</td>
				</tr>
				<tr align="center">
					<td class="tbltext1" align="left" width="150">
						<b>Codice Fiscale :</b>
					</td>	
					<td align="left" class="textBlack">
						<b><%=sCF%></b>
					</td>
				</tr>
				<tr align="center">
					<td colspan="2" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif">
					</td>
				</tr>
				<tr align="center">
					<td colspan="2">&nbsp;
					</td>
				</tr>
			<%if nDisClasse <> 0 then
				sSQLClasse = "SELECT NVL(A.DESC_AULA,'AULA A DISTANZA') AS DESC_AULA,DT_INISESS,DT_FINSESS FROM AULA A, CLASSE_PERCORSO CP " &_
					" WHERE CP.ID_AULA = A.ID_AULA(+) AND CP.ID_CLASSE = " & nDisClasse &_
					" AND COD_SESSIONE = '" & sCodSess & "'"

'PL-SQL * T-SQL  
SSQLCLASSE = TransformPLSQLToTSQL (SSQLCLASSE) 
				set rsAula = CC.Execute(sSQLClasse) 
				if not rsAula.eof then
					sDescAula = rsAula("DESC_AULA")
					dDTIniSessProv = ConvDateToString(rsAula("DT_INISESS"))
					dDTFinSessProv = ConvDateToString(rsAula("DT_FINSESS"))
				else
					sDescAula = ""
				end if
				rsAula.close
				set rsAula= nothing%>
				<tr align="center">
					<td class="tbltext1" align="left" width="150">
						<b>Classe di provenienza :</b>
					</td>	
					<td align="left" class="textBlack">
						<b><%=sDescAula%> (<%=dDTIniSessProv%> - <%=dDTFinSessProv%>)</b>
					</td>
				</tr>
			<%end if%>
				<tr align="center">
					<td class="tbltext1" align="left" width="150">
						<b>Classe di destinazione :</b>
					</td>	
					<td align="left" class="textBlack" colspan="2">
						<b><%=sAula%> (<%=dDtIniSessDest%> - <%=dDtFinSessDest%>)</b>
					</td>
				</tr>
			</table>
		<%
			sSQL = "SELECT COD_ESITO, ESITO_GRAD FROM " &_
				"DOMANDA_ISCR WHERE ID_PERSONA=" & nIDPersona &_
				" AND ID_BANDO = " & nBando
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
			set rsDomanda = CC.Execute(sSQL)
			sCodEsito =	rsDomanda("COD_ESITO")
			sEsGrad   =	rsDomanda("ESITO_GRAD")
			rsDomanda.close
			set rsDomanda = nothing 
			if not sCodEsito = "" then%>
				<br><br>
				<table border="0" cellpadding="0" cellspacing="0" width="500">
					<tr>
						<td align="center" class="tblText3">
							La Persona non � ammessa a partecipare al progetto
						</td>
					</tr>
				</table>
				<br><br>
				<%Indietro()%>
				<br><br>
			<%else
				' Controllo che sia ammesso a partecipare al progetto oppure 
				' che sia stata ripescata ed assegnata ad una sessione formativa.
				'
				' 24/06/2004 quando la data inizio delle sessioni e' uguale (modo 4) aggiungo 
				' il campo nFase poiche' se la data inizio sessione successiva e' futura
				' o coincide con la data di spostamento, l'occorrenza sulla classe di provenienza 
				' si chiude alla data di fine sessione
				' 
				' if sEsGrad = "S" then ' or sEsiste <> "" then Non tengo conto del CPI.
				' 17/06/04 asterisco la riga sopra e in quella sotto
				' gestisco lo spostamento di classe dei discenti ammessi per sostituzione 
				' (sEsGrad="X") come quello dei discenti normali (sEsGrad="S") 
				if (sEsGrad = "S" or sEsGrad = "X") then 
					'verifico l'esistenza di questa persona in componenti_classe
					sSQL = "SELECT ID_PERSONA,DT_RUOLODAL,DT_RUOLOAL FROM COMPONENTI_CLASSE " &_
						" WHERE DT_RUOLOAL IS NULL AND ID_PERSONA=" & nIDPersona

'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
					set rsIDPersona = CC.Execute(sSQL)
					if rsIDPersona.eof then	%>
						<input type="hidden" name="modo" value="1">		
						<input type="hidden" name="idpersona" value="<%=nIDPersona%>">				
						<input type="hidden" name="descaula" value="<%=sAula%>">				
						<input type="hidden" name="idclasse" value="<%=nIDClasse%>">
						<input type="hidden" name="dti" value="<%=dDtIniSessDest%>">								
						<input type="hidden" name="prgFase" value="<%=nPrgFase%>">
						<input type="hidden" name="esgrad" value="<%=sEsGrad%>">
					<%else

						dDTIniSessDal = ConvDateToString(rsIDPersona("DT_RUOLODAL"))
						if rsIDPersona("DT_RUOLOAL") <> "" then
							dDTIniSessAl = ConvDateToString(rsIDPersona("DT_RUOLOAL"))
						end if						
						if nPrgFase = 1 then%>
							<input type="hidden" name="modo" value="2">
						<%else
							if cdate(ConvStringToDate(dDTIniSessProv)) <> cdate(ConvStringToDate(dDtIniSessDest)) then%>
								<input type="hidden" name="modo" value="3">
							<%else%>
								<input type="hidden" name="modo" value="4">
							<%end if
						end if%>
						<input type="hidden" name="txtDisClasse" value="<%=nDisClasse%>">
						<input type="hidden" name="txtIniSessProv" value="<%=dDTIniSessProv%>">
						<input type="hidden" name="txtFinSessProv" value="<%=dDTFinSessProv%>">	 					
						<input type="hidden" name="txtDisDTRuoloDal" value="<%=dDTIniSessDal%>">
						<input type="hidden" name="txtDisDTRuoloAl" value="<%=dDTIniSessAl%>">
						<input type="hidden" name="txtDIsAula" value="<%=sDIsAula%>">
						<input type="hidden" name="idpersona" value="<%=nIDPersona%>">				
						<input type="hidden" name="descaula" value="<%=sAula%>">				
						<input type="hidden" name="idclasse" value="<%=nIDClasse%>">
						<input type="hidden" name="dti" value="<%=dDtIniSessDest %>">	
						<input type="hidden" name="dtf" value="<%=dDtFinSessDest%>">		
						<input type="hidden" name="dtIniEdi" value="<%=sEdiInizio%>">
						<input type="hidden" name="esgrad" value="<%=sEsGrad%>">
						<%if aPrgFaseIdPer(0) = 0 then%>
							<input type="hidden" name="prgFase" value="<%=nPrgFase%>">
						<%else%>
							<input type="hidden" name="numFase" value="<%=nPrgFase%>">
							<input type="hidden" name="prgFase" value="0">
						<%end if
					end if
					rsIDPersona.close
					set rsIDPersona = nothing%> 
					<br>
					<%if nDisClasse = nIDClasse then%>
						<table border="0" cellpadding="0" cellspacing="0" width="500">
							<tr>
								<td class="tbltext3" align="center">
									Il discente e' gia' assegnato alla classe selezionata
								</td>
							</tr>
						</table><br>
						<%Indietro()
					else
						'if sCodSess <> "ORI" and sSposta = "Ok" then
						if sSposta = "Ok" or nPrgFase = 1 then
							' Se esiste la persona in componenti_classi
							' significa che ha il dt_ruoloal chiuso 
							' considerato che le query iniziale ricercava
							' la stessa persona con il dt_ruoloal nullo
							' senza risultato.
							nCheck = 0 ' Inizializzo la variabile di controllo
' 
							if sSposta <> "Ok" then 
								sSQL = "SELECT count(*) as Contatore FROM COMPONENTI_CLASSE " &_
									" WHERE ID_PERSONA = " & clng(nIDPersona)
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
								set rsCheck = CC.Execute(sSQL)
								nCheck = clng(rsCheck("Contatore"))
								rsCheck.Close
							end if
							'if sCheckVal = 1 then%> 
								<!--table border="0" cellpadding="0" cellspacing="0" width="500">									<tr>										<td align="middle" class="tbltext3">											Attenzione le date della nuova sessione vanno a sovrapporsi											alla sessione successiva che il discente ha in programma nella											attuale edizione assegnata.										</td>									</tr>								</table-->
							<%'end if
							if nCheck <> 0 then%>
								<table border="0" cellpadding="0" cellspacing="0" width="500">
									<tr>
										<td align="middle" class="tbltext3">
											<%=sSposta%>
											<!--Il discente non ha piu' il diritto 											a partecipare alla sessione formativa-->
										</td>
									</tr>
								</table>
								<%Indietro()
							else
								if nDisClasse > 0 or nPrgFase = 1 then 
									IndietroWidthSubmit()
								else%>
									<table border="0" cellpadding="0" cellspacing="0" width="500">
										<tr>
											<td align="middle" class="tbltext3">
												<%
												Response.Write "Il discente deve ancora effettuare la prima sessione formativa"%>
											</td>
										</tr>
									</table><br>							
									<%Indietro()				
								end if
							end if
						else%>
							<table border="0" cellpadding="0" cellspacing="0" width="500">
								<tr>
									<td align="middle" class="tbltext3">
										<%if nDisClasse <> 0 then
											if clng(nDisClasse) = clng(sIDClasse) then
												response.write "Il discente e' gia' assegnato alla classe selezionata"
											else
												Response.Write  sSposta
											end if
										else
											Response.Write "Il discente deve ancora effettuare la prima sessione formativa"
										end if%>
									</td>
								</tr>
							</table><br>
							<%Indietro()
						end if
					end if
				else ' Discente ripescato 
						'Response.Write sEsGrad & " - " & nPrgFase
					if sEsGrad = "N" and nPrgFase = 1 then
						'verifico l'esistenza di questa persona in componenti_classe: se non c'e' vuol dire che e'
						'un discente ripescato che viene inserito per la prima volta (da "N" sEsGrad diventa "X")
						sSQL = "SELECT ID_PERSONA,DT_RUOLODAL,DT_RUOLOAL FROM COMPONENTI_CLASSE WHERE ID_PERSONA=" &_
							nIDPersona
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
						set rsIDPersona = CC.Execute(sSQL)
						if rsIDPersona.eof then 
							nCheck=1%>
							<br>
							<table border="0" cellpadding="0" cellspacing="0" width="500">
								<tr>
									<td align="center" class="tbltext3">
										Attenzione si sta inserendo una persona <br>che non 
										rientra in graduatoria: in questo modo la persona <br>
										in questione verr� ammessa per sostituzione
									</td>
								</tr>
							</table>	
							<input type="hidden" name="modo" value="1">
							<!--input type="hidden" name="cmbBando " value="<%'=nBando%>"-->
							<!--input type="hidden" name="cmbSessioni" value="<%'=sCodSess%>"-->
							<input type="hidden" name="idpersona" value="<%=nIDPersona%>">				
							<input type="hidden" name="descaula" value="<%=sAula%>">				
							<input type="hidden" name="idclasse" value="<%=nIDClasse%>">
							<input type="hidden" name="dti" value="<%=dDtIniSessDest%>">								
							<input type="hidden" name="prgFase" value="<%=nPrgFase%>">
							<input type="hidden" name="esgrad" value="<%=sEsGrad%>">
						<%else
							sSQL = "select count(*) as CONTA from componenti_classe " &_
								"where id_persona = " & CLng(nIDPersona) &_
								" and (select count(*) from componenti_classe " &_
								" where id_persona = " & CLng(nIDPersona) & ") = " &_
								"(select count(*) from componenti_classe where " &_
								"id_persona = " & CLng(nIDPersona) & " and dt_ruoloal is null)" 
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
							set rcCheck = CC.Execute(sSQL)
							nCheck = CLng(rcCheck("CONTA"))
							rcCheck.Close
							set rcCheck = nothing
							if nCheck = 0 then%>
								<br>
								<table border="0" cellpadding="0" cellspacing="0" width="500">
									<tr>
										<td align="center" class="tbltext3">
											Il discente non ha piu' il diritto a partecipare alla sessione formativa
										</td>
									</tr>
								</table>
								<%Indietro()
							end if
							dDTIniSessProv = ConvDateToString(rsIDPersona("DT_RUOLODAL"))
							if rsIDPersona("DT_RUOLOAL") <> "" then
								dDTFinSessProv = ConvDateToString(rsIDPersona("DT_RUOLOAL"))
							end if
							
							%>
							<input type="hidden" name="modo" value="2">					
							<input type="hidden" name="txtDisClasse" value="<%=nDisClasse%>">
							<input type="hidden" name="txtDisDTRuoloal" value="<%=dDTFinSessProv%>">
							<input type="hidden" name="txtDisDTRuoloDal" value="<%=dDTIniSessProv%>">
							<input type="hidden" name="txtDIsAula" value="<%=sDIsAula%>">
							<input type="hidden" name="idpersona" value="<%=nIDPersona%>">				
							<input type="hidden" name="descaula" value="<%=sAula%>">				
							<input type="hidden" name="idclasse" value="<%=nIDClasse%>">
							<input type="hidden" name="dti" value="<%=dDtIniSessDest%>">	
							<input type="hidden" name="dtf" value="<%=dDtFinSessDest%>">		
							<input type="hidden" name="prgFase" value="<%=nPrgFase%>">
							<input type="hidden" name="esgrad" value="<%=sEsGrad%>">
							<%if aPrgFaseIdPer(0) = 0 then%>
								<input type="hidden" name="prgFase" value="<%=nPrgFase%>">
							<%else%>
								<input type="hidden" name="prgFase" value="0">
							<%end if
							
						end if 
						rsIDPersona.close
						set rsIDPersona = nothing
						if nDisClasse = nIDClasse then%>
							<br>
							<table border="0" cellpadding="0" cellspacing="0" width="500">
								<tr>
									<td align="center" class="tbltext3">
										Il discente e' gia' assegnato alla classe selezionata
									</td>
								</tr>
							</table><br>
							<%Indietro()
						else
							if nCheck <> 0 then
								IndietroWidthSubmit()
							end if
						end if
					else%>
						<br>
						<table border="0" cellpadding="0" cellspacing="0" width="500">
							<tr>
								<td align="center" class="tbltext3">
									Il discente non � ammesso al progetto per cui non 
									pu� essere ripescato ed inserito in una classe 
									che si trova in una fase successiva alla prima.
								</td>
							</tr>
						</table><br>
						<%Indietro()
					end if
				end if  
			end if
		end if		
	end if%>
	</form>
<%end sub%>
