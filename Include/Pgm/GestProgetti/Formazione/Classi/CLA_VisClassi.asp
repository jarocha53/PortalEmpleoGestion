<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--	BLOCCO HEAD			-->
<!-- JavaScript immediate script -->
<script LANGUAGE="JavaScript">

function Carica_Pagina(npagina) {
	document.frmPagina.pagina.value = npagina;
	document.frmPagina.submit();
}

<!--#include virtual = "/include/help.inc"-->
<!--#include virtual = "/Include/ControlDate.inc"-->

<!--
function VisSessione() {
	if (frmBandi.cmbBando.value == "")
		alert("Selezionare un bando per vedere le sessioni")
	else {
		frmSessioni.cmbBando.value = frmBandi.cmbBando.value;
		frmSessioni.submit(); 
	}
}

function Validator() {

	if (frmBandi.cmbBando.value == ""){
		document.frmBandi.cmbBando.focus();
		alert("Il campo Bando � obbligatorio.");
		return (false);
	}
	if (frmBandi.cmbSessioni.value == ""){
		document.frmBandi.cmbSessioni.focus();
		alert("Il campo sessione formativa � obbligatorio.");
		return (false);
	}

	if (frmBandi.txtDataRic.value != ""){
		if (!ValidateInputDate(frmBandi.txtDataRic.value)) {
			document.frmBandi.txtDataRic.focus();
			return (false);
		}
	}


	return (true);	
}

		
// -->
</script>
	
<%sub Indietro() %>
	<br>
	<table cellSpacing="0" cellPadding="0" width="500" border="0">
		<tr>
			<td align="center">
				
				<a onmouseover="window.status =' '; return true" title="Indietro" href="Javascript:Indietro.submit()"><img border="0" src="<%=Session("progetto")%>/images/indietro.gif"></a> 
			</td>
		</tr>
	</table>
	<br>
<%end sub

function SelSessione(nBando)
	dim sSQLSessioni,rsSessioni
	'Response.Write sSessione
'Mario 09/03/2004
	' Aggiunto ordinamento per descrizione della sessione
'Mario 09/03/2004	
	sSQLSessioni = "SELECT COD_SESSIONE,DESC_FASE " &_
					" FROM FASE_BANDO " &_
					" WHERE ID_BANDO = " & nBando &_
					" ORDER BY DESC_FASE"
					
	set rsSessioni = Server.CreateObject("ADODB.Recordset")
'PL-SQL * T-SQL  
SSQLSESSIONI = TransformPLSQLToTSQL (SSQLSESSIONI) 
	rsSessioni.Open sSQLSessioni,CC,3
	
	sSessione = Request.Form ("sessione") 
	
	if not rsSessioni.EOF then
		SelSessione = true%>
		<select name="cmbSessioni" class="textBlack">
			<option value></option>
			<%do while not rsSessioni.EOF
			%><option <%if sSessione = rsSessioni("COD_SESSIONE") then Response.Write " selected " %>value="<%=rsSessioni("COD_SESSIONE")%>"><%=rsSessioni("DESC_FASE")%></option><%
				rsSessioni.MoveNext() 
			loop%>
		</select>
	<%else
		SelSessione = false
	end if
	rsSessioni.Close()
	set rsSessioni = nothing
end function

Sub Sessioni()
	Dim sCombo,sSQLSessioni,rsSessioni,bSessione
	bSessione = false%>
	<!--table border="0" cellspacing="2" cellpadding="1" width="500"--> 
		<tr>
			<td class="tbltext1" align="left" width="150">
				<b>Sessione Formativa :</b>
			</td>
			<td class="tbltext3" align="left" width="350">
				<%bSessione = SelSessione(nBando)
				if not bSessione then
					Response.Write "Nessuna sessione formativa associata"
				end if%>
			</td>
		</tr>
		<tr>
			<td class="tbltext1" align="left" width="150">
				<b>Programmate dal :</b>
			</td>
			<td align="left" class="tbltext3">
				<input maxlength="10" size="12" class="textBlack" type="text" name="txtDataRic" value="<%=Request.Form("dtRicerca")%>">
			</td>
		</tr>
		<tr align="center">
			<td colspan="2">&nbsp;</td>
		</tr>
		<%if bSessione then%>
			<tr align="center">
				<td colspan="2">
					<input title="Conferma" type="image" src="<%=Session("Progetto")%>/images/Conferma.gif" value="Conferma" id="image1" name="image1">
				</td>
			</tr>
		<%end if%>
	<!--/table-->
<%End Sub%>	

<%sub InfoProc()%>
	<table cellpadding="0" cellspacing="0" width="500" border="0">
		<tr height="18">
			<td class="sfondomenu" height="18" width="67%">
				<span class="tbltext0"><b>&nbsp;GESTIONE CLASSI</b></span>
			</td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
			<td valign="middle" align="right" width="40%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1"></td>
		</tr>
		<tr width="371" class="SFONDOCOMM">
			<td colspan="3">
			<%if sSessione = "" then%>
				Selezionare un bando, di seguito scegliere una sessione formativa
				ed indicare una eventuale data per ricercare le classi.
			<%else%>
				Di seguito vengono riportate l'elenco delle classi che soddisfano i 
				criteri di ricerca scelti. Cliccare sulla classe che si desidera modificare oppure
				premere su <b>inserisci nuova classe</b> per crearne una nuova.
			<%end if%>
				<a onmouseover="window.status =' '; return true" href="Javascript:Show_Help('/Pgm/Help/gestprogetti/Formazione/Classi/cla_visclassi')">
				<img src="<%=Session("Progetto")%>/images/Help.gif" border="0" align="right" alt="Help">

			</td>
		</tr>
		<tr height="2">
			<td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>

<%end sub%>
<!--	FINE BLOCCO SCRIPT	-->
<!--	BLOCCO ASP			-->
<!--------------------------------------------------------------------------------------------------------------------------------------------------------->
<%	' Craezione della tabella con i dati sulle classi
sub TabClassi()

	dim dRicerca
	dim i
	
	dRicerca = Request.Form("txtDataRic")
	if dRicerca <> "" then
		dRicerca = " AND DT_INISESS >=" & ConvDateToDbs(ConvDateToString(dRicerca))
	end if
	Record=0
	nTamPagina=15

	If Request.Form ("pagina")= "" Then
		nActPagina=1
	Else
		nActPagina=Clng(Request.Form("pagina"))
	End If
	if Err.number <> "0" then
		nActPagina = 1
		Err.Clear
	end if	

'	sSQL = "SELECT B.ID_CFORM,A.ID_CLASSE,B.DESC_AULA,C.DT_INIZIO,C.DT_FINE FROM CLASSE A, AULA B,EDIZIONE C WHERE A.ID_AULA = B.ID_AULA " &_
'		" AND A.ID_BANDO =" & nBando & " AND A.ID_EDIZIONE = C.ID_EDIZIONE"
'	sSQL = sSQL & " AND C.DT_FINE IS NOT NULL and B.TIPO_AULA is NULL " &_
'	"AND EXISTS (SELECT ID_PERSONA FROM " &_
'	" COMPONENTI_CLASSE WHERE DT_RUOLOAL IS NULL AND A.ID_CLASSE = ID_CLASSE) ORDER BY C.DT_INIZIO"
'	sSQL = "SELECT A.ID_CLASSE,B.DESC_AULA,C.DT_INIZIO,C.DT_FINE " &_
'		" FROM CLASSE A, AULA B" &_
'		" WHERE A.ID_AULA = B.ID_AULA " &_
'		" AND A.ID_BANDO =" & nBando &_
'		" AND 
'		" AND B.TIPO_AULA is NULL " &_
'		" AND EXISTS (SELECT ID_PERSONA FROM COMPONENTI_CLASSE " &_
'		" WHERE DT_RUOLOAL IS NULL AND A.ID_CLASSE = ID_CLASSE) ORDER BY C.DT_INIZIO"
	sSQL = " SELECT A.ID_CLASSE,NVL(B.DESC_AULA,'AULA A DISTANZA') AS DESC_AULA,DT_INISESS,DT_FINSESS"
	sSQL = sSQL &  "  FROM CLASSE A, AULA B, CLASSE_PERCORSO CP"
	sSQL = sSQL &  "  WHERE CP.ID_AULA = B.ID_AULA (+)"
	sSQL = sSQL &  "  AND A.ID_BANDO = " & nBando
	sSQL = sSQL &  "  AND CP.ID_CLASSE = A.ID_CLASSE"
	sSQL = sSQL &  "  AND CP.COD_SESSIONE = '" & sSessione & "'"

	sSQL = sSQL & " AND A.ID_UORG = " & Session("iduorg") & " "

	sSQL = sSQL & dRicerca
	sSQL = sSQL &  "  AND DT_INISESS IS NOT NULL AND DT_FINSESS IS NOT NULL"
	sSQL = sSQL &  " ORDER BY DT_INISESS,A.ID_CLASSE"

'	sSQL = sSQL &  "  AND EXISTS ( SELECT ID_PERSONA FROM COMPONENTI_CLASSE "
'	sSQL = sSQL &  "  WHERE DT_RUOLOAL IS NULL AND A.ID_CLASSE = ID_CLASSE ) "

'	Response.Write "sSQL: " & sSQL & "<BR>"

	set rsClassi = Server.CreateObject("ADODB.Recordset")
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	rsClassi.Open sSQL, CC, 3
	i = 0 
	if not rsClassi.eof then%>
		<table border="0" cellspacing="2" cellpadding="1" width="500">
			<form name="frmPagina" method="post" action="CLA_VisClassi.asp">
			    <input type="hidden" name="pagina" value>
			    <input type="hidden" name="cmbBando" value="<%=nBando%>">
			    <input type="hidden" name="cmbSessioni" value="<%=sSessione%>">
			</form>	
			
			<tr>
				<td class="tbltext1" align="left" width="125">
					<b>Bando :</b>
				</td>	
				<td class="textBlack" align="left">
					<b><%=DecBando(nBando)%></b>
				</td>
			</tr>
			<tr>
				<td class="tbltext1" align="left" width="125">
					<b>Sessione Formativa :</b>
				</td>	
				<td class="textBlack" align="left" class="textBlack">
					<b><%=DecFaseBando(nBando,sSessione)%></b>
				</td>
			</tr>
		</table>
<%		InserisciNuovaClasse()	%>	
		<table width="500" cellspacing="2" cellpadding="1" border="0">
			<tr class="sfondoComm"> 
				<td align="left" width="400">
					<b>Classi</b>
				</td>
				<td align="left" width="100">
					<b>Status</b>
				</td>
			</tr>
		<!--/table>		<table width=500  cellspacing=2 cellpadding=1 border=1-->
		<%
		
		rsClassi.PageSize  = nTamPagina
		rsClassi.CacheSize = nTamPagina	

		nTotPagina = rsClassi.PageCount		
		If nActPagina < 1 Then
			nActPagina = 1
		End If
		If nActPagina > nTotPagina Then
			nActPagina = nTotPagina
		End If

		rsClassi.AbsolutePage = nActPagina
		nTotRecord=0
		
		do while not rsClassi.EOF And nTotRecord < nTamPagina
			s =0
			dOggi = date()
			
			if cdate(rsClassi("DT_INISESS")) <=  cdate(dOggi) then
				s=s+1
			end if 
			if cdate(rsClassi("DT_FINSESS")) < cdate(dOggi) then
				s=s+1
			end if
			dDTInizio	= ConvDateToString(rsClassi("DT_INISESS"))
			dDTFine		= ConvDateToString(rsClassi("DT_FINSESS"))
				%>
			<tr class="tblsfondo">
				<form method="Post" action="CLA_ModClassi.asp" name="frmModClassi<%=i%>">
					<input type="hidden" name="ID" value="<%=CLNG(rsClassi("ID_CLASSE"))%>">
					<input type="hidden" name="Stato" value="<%=s%>">
					<input type="hidden" name="Bando" value="<%=nBando%>">
					<input type="hidden" name="Sessione" value="<%=sSessione%>">

					<input type="hidden" name="dtInizio" value="<%=dDTInizio%>">
					<input type="hidden" name="dtFine" value="<%=dDTFine%>">
					<input type="hidden" name="descAula" value="<%=rsClassi("DESC_AULA")%> (<%=dDTInizio%> - <%=dDTFine%>)">
					
				</form>
				<td align="left" width="400" class="tblDett">
					<%'sSQL = "SELECT DESC_CFORM FROM " &_
					'	"CENTRO_FORMAZIONE WHERE ID_CFORM = " & CLNG(rsClassi("ID_CFORM"))
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
					 ' set rsCFOrm = CC.Execute(sSQL)
					 ' sDescCform = rsCFOrm("DESC_CFORM")

					 ' rsCFOrm.close%>

					<%'if s = 0 or s = 1 then %>
						<a title="Seleziona la classe" class="tblAgg" onmouseover="window.status =' '; return true" href="Javascript:frmModClassi<%=i%>.submit()">
							<%'=sDescCform%><%=rsClassi("DESC_AULA")%> 
							(<%=dDTInizio%>-<%=dDTFine%>)
						</a>
					<%'else%>
							<%'=sDescCform%><%'=rsClassi("DESC_AULA")%> 
							<%'=dDTInizio%><%'=dDTFine%>
					<%'end if%>
				</td>
				<td class="tblDett" align="center" width="100">
					<%select case s
						case 0
							Response.Write "Programmata"
						case 1
							Response.Write "In Corso"
						case 2
							Response.Write "Conclusa"
					end select%>
				</td>
			</tr>
			<%
			nTotRecord=nTotRecord + 1 			
			i = i + 1 
			rsClassi.MoveNext%>
		<%loop%>
		</table>
<%		Response.Write "<TABLE border=0 width=470 align=center>"
		Response.Write "<tr>"
		if nActPagina > 1 then
			Response.Write "<td align=right width=480>"
			Response.Write "<A title=""Precedente"" HREF=""javascript:Carica_Pagina('" & nActPagina-1 & "')"">"
			Response.Write "<img border=0 src=" & Session("Progetto") & "/images/precedente.gif></A></td>"
		end if
		if nActPagina < nTotPagina then
			Response.Write "<td align=right>"
			Response.Write "<A title=""Successivo"" HREF=""javascript:Carica_Pagina('" & nActPagina+1 & "')"">"		
			Response.Write "<img border=0 src=" & Session("Progetto") & "/images/successivo.gif></A></td>"
		end if
		Response.Write "</tr></TABLE>"
		%>		
		
<%	else
%>
		<table border="0" cellspacing="2" cellpadding="1" width="500">
			<tr>
				<td class="tbltext1" align="left" width="125">
					<b>Bando :</b>
				</td>	
				<td class="textBlack" align="left">
					<b><%=DecBando(nBando)%></b>
				</td>
			</tr>
			<tr>
				<td class="tbltext1" align="left" width="125">
					<b>Sessione Formativa :</b>
				</td>	
				<td class="textBlack" align="left" class="textBlack">
					<b><%=DecFaseBando(nBando,sSessione)%></b>
				</td>
			</tr>
		</table>
		<br>
<%		'''LC**:03/10/2003 - richiamata funzione x inserimento classi.
		InserisciNuovaClasse() %>
		<table width="500" cellspacing="2" cellpadding="1" border="0">
			<tr class="tbltext3"> 
				<td class="tbltext3" align="Center">
					Non sono state ritrovate classi
				</td>
			</tr>	
		</table>	
	<%end if
	rsClassi.Close
	set rsClassi = nothing
	Indietro()
end sub
%>
<!--------------------------------------------------------------------------------------------------------------------------------------------------------->
<%  ' Viene creato il combo/la label con le provincie/a di competenza dell'utente se 
sub Bandi()
	Dim RR, sql%>		
	<br>
	<table border="0" cellspacing="2" cellpadding="1" width="500">
		<tr>
			<td class="tbltext1" align="left" width="150">
				<b>Bando :</b>
			</td>
			<td class="tbltext3" align="left" width="350">
<%			sComboBandi = Session("iduorg") & "|" & nBando & "|cmbBando|onchange=Javascript:VisSessione()"
			CreateBandi(sComboBandi)%>
			</td>
		</tr>
	<!--/table-->
<%End sub%>	


<%sub InserisciNuovaClasse()%>		

	<table border="0" cellspacing="2" cellpadding="1" width="500">
		<tr>
			<td align="center">
				<a onmouseover="window.status =' '; return true" title="Inserisci Nuova Classe" class="textRed" href="Javascript:frmInserisci.submit()"><b>Inserisci Nuova Classe</b></a>
			</td>
		</tr>
	</table>
	<br>
<%End sub%>	

<!--------------------------------------------------------------------------------------------------------------------------------------------------------->
<!--#include virtual ="/Strutt_testa2.asp"-->
<!--#include virtual ="/util/portallib.asp"-->
<!--#include virtual ="/include/SelAreaTerrBandi.asp"-->
<!--#include virtual ="/include/ControlDateVB.asp"-->
<!--#include virtual ="/include/openconn.asp"-->
<!--#include virtual ="/Util/DBUtil.asp"-->
<!--#include file ="CLA_InizioClassi.asp"-->
<!--	FINE BLOCCO ASP		-->
<!--		MAIN			-->
<form method="post" action="CLA_VisCLassi.asp" onsubmit="return Validator()" name="frmBandi">
<%
Dim strConn, nBando, sSessione

if ValidateService(session("idutente"),"CLA_VisClassi",CC) <> "true" then 
	response.redirect "/util/error_login.asp"
end if
	
nBando		= clng(Request.Form ("cmbBando"))
sSessione	= Request.Form ("cmbSessioni")

Inizio()
InfoProc()

if sSessione = "" then

	Bandi()
	if nBando <> 0 then
		Sessioni()
	end if 
	Response.Write "</table>"
	Response.Write "</form>"
else
	Response.Write "</form>"
	
	TabClassi()
end if


%>
<!--#include virtual ="/Strutt_coda2.asp"-->
<!--#include virtual ="/include/closeconn.asp"-->

<form name="frmInserisci" method="Post" action="CLA_InsClassi.asp">
<input type="hidden" name="Bando" value="<%=nBando%>">
<input type="hidden" name="Sessione" value="<%=sSessione%>">
</form>

<form name="frmSessioni" method="Post" action="CLA_VisClassi.asp">
<input type="hidden" name="cmbBando">
</form>

<form name="Indietro" method="Post" action="CLA_VisClassi.asp">
<input type="hidden" name="cmbBando" value="<%=nBando%>">
<input type="hidden" name="Sessione" value="<%=sSessione%>">
<input type="hidden" name="dtRicerca" value="<%=Request.Form("txtDataRic")%>">
</form>


<!--	FINE BLOCCO MAIN	-->
