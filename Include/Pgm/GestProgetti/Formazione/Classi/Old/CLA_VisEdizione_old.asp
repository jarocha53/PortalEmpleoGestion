<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%'Option Explicit 
   Response.ExpiresAbsolute = Now() - 1 
   Response.AddHeader "pragma","no-cache"
   Response.AddHeader "cache-control","private"
   Response.CacheControl = "no-cache"
%>
<!-- #include virtual="/strutt_testa2.asp"-->
<!--#include virtual ="/include/openconn.asp"-->
<!--#include virtual ="/util/portallib.asp"-->
<!--#include virtual = "/util/dbutil.asp"-->

<!-- ************** Javascript Inizio ************ -->

<script LANGUAGE="Javascript">

function ReloadForGroup()
{
	var nSel
	nSel = frmVisEdizione.elements[0].selectedIndex
	if (frmVisEdizione.elements[0].options[nSel].value != "") 
		location.href ="CLA_VisEdizione.asp?Bando=" + frmVisEdizione.elements[0].options[nSel].value +
			"&Prov=1"
	else
		alert ("Occorre selezionare una provincia")
}

function GestioneAreaForGruop()
{
	location.href ="CLA_VisEdizione.asp?Bando=" + frmVisEdizione.elements[0].value + 
		"&prov=1"
}


function Reload()
{
	var nSel

	nSel = frmVisEdizione.elements[0].selectedIndex
	if (frmVisEdizione.elements[0].options[nSel].value != "") 
		document.forms[0].submit();
	else
		alert ("Occorre selezionare un Bando")
}

function GestioneArea()
{
		document.forms[0].submit();
}
</script>
<!-- ************** Javascript Fine   ************ -->
<!-- ************** ASP Inizio ************* -->

<%Sub Messaggio(Msg)%>
	<br>
	<table width="500" border="0" cellspacing="2" cellpadding="1">
		<tr><td class="tbltext3" align="center"><b><%=Msg%></b></td></tr>	
	</table>
<%End Sub%>

<% sub Inizio() %>
	<br>
	<table cellpadding="0" cellspacing="0" width="500" border="0">
		<tr height="18">
			<td class="sfondomenu" height="18" width="67%">
			<span class="tbltext0"><b>&nbsp;Gestione Edizioni/Classi</b></span></td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
			<td valign="middle" align="right" class="tbltext1" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">(*) campi obbligatori</td>
		</tr>
		<tr>
			<td class="sfondocomm" width="57%" colspan="3">
				Testo da inserire 
			<a href="Javascript:Show_Help('/Pgm/help/GestProgetti/Formazione/Classi/CLA_VisEdizione')" onmouseover="javascript:window.status=' '; return true"><img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
			</td>
		</tr>
		<tr height="2">
			<td colspan="3" class="sfondocomm" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
		</tr>
	</table>
	<br><br>
<%end sub %>

<%sub CaricaBando()
       sSQL = "SELECT B.ID_BANDO,B.COD_BANDO,B.DESC_BANDO FROM BANDO B,AREA_BANDO AB WHERE B.ID_BANDO=AB.ID_BANDO AND AB.ID_UORG=" & Session("iduorg") &_
			" and not exists (SELECT BP.ID_SEDE FROM BANDO_POSTI BP " &_
			" WHERE BP.ID_BANDO = B.ID_BANDO  AND BP.STATUS_GRAD = 0)"
     Response.Write sSQL
		set rsBando = server.CreateObject("ADODB.Recordset")
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
        rsBando.Open sSql, CC, 3
        
        IF rsBando.EOF = TRUE THEN
			Messaggio("Nessun bando trovato<BR>oppure non tutte le graduatorie sono definitive")
            rsBando.close
            set rsBando = nothing
          	EXIT SUB
        END IF
     
        if rsBando.recordcount = 1 then %>
				<input type="hidden" value="<%=rsBando.Fields("ID_BANDO")%>" name="cmbProv">
				<%rsBando.Close : set rsBando = nothing	%>
	
				<script Language="Javascript">GestioneAreaForGruop()</script>
      <%else%>
			 <table width="500" border="0" cellspacing="2" cellpadding="1">
				<td align="center" class="tbltext1"><b>Selezionare un Bando</b></td>
				<td>
					<select class="textblack" ID="cmbBando" name="cmbBando" onchange="ReloadForGroup()">
						<option></option>
                   <%	
						do while not rsBando.EOF
							Response.Write "<OPTION "
							Response.write "value ='" & rsBando.Fields("ID_BANDO") & _
							"'> " & rsBando.Fields("ID_BANDO")& " - " & rsBando("DESC_BANDO") & "</OPTION>"
							rsBando.MoveNext 
						loop   %>
					</select>
				</td>
			</table>	
		<%	rsBando.Close : set rsBando = nothing
        end if 
        
            
      
        
 end sub %>

<%sub TornaIndietro()%>
    <br>
	<table width="500" border="0">
			<tr align="center">
				<td nowrap><a href="javascript:history.go(-1)" onmouseover="javascript:window.status=' '; return true"><img src="/PLAVORO/images/indietro.gif" border="0" name="imgPunto2"></a><td>
	    </tr>
	</table>  

<%end sub%>

<%sub CaricaAreaGeografica()
	    sSQL = "SELECT DISTINCT ID_AREAGEO FROM CI_AREAGEO WHERE ID_BANDO=" & sBando &_
				" AND ID_UORG=" & session("iduorg")
		Response.Write sSQL
		set rsGruppo = server.CreateObject("ADODB.Recordset")
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
        rsGruppo.Open sSql, CC, 3
        'Response.Write ("Trovati " & rsGruppo.RecordCount )
        'Response.End 
        IF  rsGruppo.EOF = TRUE THEN
			Messaggio("Raggruppamento non disponibile per il Bando " & sBando)
            rsGruppo.close
            set  rsGruppo = nothing
          	EXIT SUB
        END IF

        if rsGruppo.recordcount = 1 then %>
                <input type="hidden" value="<%=sBando%>" name="txtBando">
				<input type="hidden" value="<%=rsGruppo("ID_AREAGEO")%>" name="txtAreaGeo">

				<%rsGruppo.Close : set rsGruppo = nothing%>
				<script Language="Javascript">GestioneArea()</script>
	<%	else %>
				
			<table width="500" border="0" cellspacing="2" cellpadding="1">	
			<tr>	
				<td align="center" class="tbltext1"><b>Bando selezionato :</b><%=sBando%></td>
			</tr>	
			<tr>	
				<td class="tbltext1"><b>Selezionare un gruppo</b></td>
						
				<td>
				
				<select class="textblack" ID="txtAreaGeo" name="txtAreaGeo" onchange="Reload()">&quot;
				<option></option>
			<%			
					do while not rsGruppo.EOF
						Response.Write "<OPTION "
						Response.write "value ='" & rsGruppo("ID_AREAGEO") & _
						"'> " & rsGruppo("ID_AREAGEO") & "</OPTION>"
						rsGruppo.MoveNext 
					loop
					Response.Write "</SELECT>"
					rsGruppo.Close : set rsgruppo = nothing
				%>
				<input type="hidden" value="<%=sBando%>" name="txtBando">
		   	</td>
	    </tr>
      </table>	
				
				
	<%end if %>
		
<%		
  end sub %>
  
<%sub Fine()%>
  	<!--#include virtual = "/include/closeconn.asp"-->
	<!--#include virtual = "strutt_coda2.asp"-->
<%end sub%>
<!-- ************** ASP Fine *************** -->

<!-- ************** MAIN Inizio ************ -->
<%

dim sSQL
dim sBando

sBando = Request("Bando")
Session("Bando")= sBando


Inizio()
%>
<form name="frmVisEdizione" method="post" action="CLA_InsEdizione.asp">
<%  
IF sBando = "" then
	CaricaBando()
ELSE
    CaricaAreaGeografica()
	TornaIndietro()   
END IF
%>
</form>
<%
Fine() %>
<!-- ************** MAIN Fine ************ -->

