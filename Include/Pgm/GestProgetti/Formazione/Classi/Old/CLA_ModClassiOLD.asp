<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%sub InfoProc()%>
	<table cellpadding="0" cellspacing="0" width="500" border="0">
		<tr height="18">
			<td class="sfondomenu" height="18" width="50%">
				<span class="tbltext0"><b>&nbsp;GESTIONE CLASSI</b>
				</span>
			</td>
			<td width="5%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
			<td valign="middle" align="right" width="40%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1"></td>
		</tr>
		<tr width="371" class="SFONDOCOMM">
			<td colspan="3">
			Selezionare la sessione formativa per poterne modificare il periodo 
			oppure selezionare il link relativo al ruolo per modificare o aggiungere un 
			ruolo funzionale alla classe.
			<a onmouseover="window.status =' '; return true" href="Javascript:Show_Help('/Pgm/Help/gestprogetti/Formazione/Classi/CLA_ModClassi')">
			<img src="<%=Session("Progetto")%>/images/Help.gif" border="0" align="right" alt="Help">

			</td>
		</tr>
		<tr height="2">
			<td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>
<%end sub%>

<!--#include virtual = "/Strutt_Testa2.asp"-->
<!--#include virtual = "/include/openconn.asp"-->
<!--#include virtual = "/include/ControlDateVB.asp"-->
<!--#include virtual = "/util/dbUtil.asp"-->
<!--#include file ="CLA_InizioClassi.asp"-->

<!--#include virtual = "/include/ControlFase.asp"-->
<!--#include virtual = "/include/SelAreaTerrBandi.asp"-->
<%
if ValidateService(session("idutente"),"CLA_VisClassi",CC) <> "true" then 
	response.redirect "/util/error_login.asp"
end if
%>

<script language="Javascript">
<!--#include virtual = "/Include/help.inc"-->

//	function InsRuolo(nIdClasse,sCodSessione){
//		var NewWin
//		NewWin = window.open('CLA_InsRuolo.asp?IdClasse=' + nIdClasse + '&sessione=' + sCodSessione ,'Info','width=520,height=280,Resize=No')
//	}

</script>
<%
dim nIdClasse
dim nBando
dim sSessione
dim nPrgFase
dim dOggi
dOggi = Date

nIdClasse	= CLng(Request.Form ("ID"))
sDescAula	= Request.Form ("descAula")
nBando		= CLng(Request.Form ("Bando"))
sSessione	= Request.Form ("Sessione")

Inizio()
InfoProc()
InfoArea()
PianificazioneClasse()

sub InfoArea()%>
	<br>
	<table border="0" cellspacing="2" cellpadding="1" width="500">
		<tr>
			<td class="tbltext1" align="left" width="50">
				<b>Bando :</b>
			</td>	
			<td class="textBlack" align="left" width="450">
				<b><%=DecBando(nBando)%></b>
			</td>
		</tr>
		<!--tr>			<td class="tbltext1" align="left" width="125">				<b>Sessione Formativa :</b>			</td>				<td class="textBlack" align="left" class="textBlack">				<b><%'=DecFaseBando(nBando,sSessione)%></b>			</td>		</tr-->
		<tr>
			<td class="tbltext1" align="left" width="50">
				<b>Aula :</b>
			</td>	
			<td class="textBlack" align="left" width="450">
				<b><%=sDescAula%></b>
			</td>
		</tr>
	</table>
<%end sub

sub PianificazioneClasse()
	dim sCodSessione,i,nPrgFase
	i = 0
	n = 0 
	nPrgFase = 0 
	%>
	<!--br>	<table border="0" cellspacing="2" cellpadding="1" width="500">		<tr>			<td align="center">				<a class="textRed" onmouseover="window.status =' '; return true" 				href="Javascript:InsRuolo(<%'=nIdClasse%>)"					Title="Modifica ruolo"><b>Modifica Ruoli</b></a>			</td>		</tr>	</table-->
	<br>
	<table border="0" cellspacing="2" cellpadding="1" width="500">
		<!--tr>			<td colspan=3 class="sfondoComm" align="center" >				<b>Classe [ informazioni aula ]</b>			</td>			</tr-->
		<tr class="sfondoComm">
		    <td align="left" width="50">
				<b>Progr. Fase</b>
			</td>
			<td align="left" width="10">
				<b>Cod. Sessione</b>
			</td>	
			<td align="left" width="250">
				<b>Sessione formativa</b>
			</td>	
			<td align="left" width="95">
				<b>Dal </b>
			</td>	
			<td align="left" width="95">
				<b>Al</b>
			</td>	
			<td align="left" width="50">
				<b>Ruoli</b>
			</td>	
		</tr>
		<%sSQL = "SELECT NVL(ID_AULA,0) AS ID_AULA,a.PRG_FASE,a.COD_SESSIONE,DT_INISESS,DT_FINSESS,b.DESC_FASE " &_
			" FROM CLASSE_PERCORSO a, fase_bando b WHERE ID_CLASSE = " & nIdClasse &_
			" and a.cod_sessione=b.cod_sessione" &_
			" ORDER BY a.PRG_FASE,DT_INISESS,b.desc_fase"

		set rsPianClasse = Server.CreateObject("ADODB.Recordset")
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
		rsPianClasse.Open sSQL, CC, 3			
		n = rsPianClasse.RecordCount
		if not rsPianClasse.eof then
			do while not rsPianClasse.EOF
				dDtPrec = ""
				dDtSucc = ""
				
				nIDAula = CLng(rsPianClasse("Id_AULA"))%>
				<form name="frmModPianificazione<%=i%>" method="post" action="CLA_ModPianificazione.asp">
					<input type="hidden" name="Sessione" value="<%=sSessione%>">
				<%if i = 0 then%>
					<input type="hidden" name="txtDtAlPrec" value>
				<%else
					count = 0
					rsPianClasse.MovePrevious()
					' Se le sessione precedente ha lo stesso progressivo fase
					' devo tornare indietro fino a trovare una sessione
					' precedente con un diverso progressivo fase.
					do while not rsPianClasse.BOF 
					   
					    if nPrgFase <> CInt(rsPianClasse("PRG_FASE")) then
							if not IsNull(rsPianClasse("DT_FINSESS")) then
								dDtPrec = ConvDateToString(rsPianClasse("DT_FINSESS"))
							end if
							exit do
						end if
						
						rsPianClasse.MovePrevious()
						count = count + 1
					loop
					%>
					<input type="hidden" name="txtDtAlPrec" value="<%=dDtPrec%>">
				<%	
					for fwd = 0 to count
						rsPianClasse.MoveNext() 
					next
				end if
				if n = (i+1) then%>
					<input type="hidden" name="txtDtDalSucc" value>
				<%else
					count = 0
					rsPianClasse.MoveNext()
					' Se le sessione successiva ha lo stesso progressivo fase
					' devo andare avanti fino a trovare una sessione
					' successiva con un diverso progressivo fase.
					do while not rsPianClasse.EOF 
						if nPrgFase <> CInt(rsPianClasse("PRG_FASE")) then
							if not IsNull(rsPianClasse("DT_INISESS")) then
								dDtSucc = ConvDateToString(rsPianClasse("DT_INISESS"))
							end if
							exit do
						end if
						count = count + 1
						rsPianClasse.MoveNext()
					loop%>
					<input type="hidden" name="txtDtDalSucc" value="<%=dDtSucc%>">
			<%		
					for bck = 0 to count
						rsPianClasse.MovePrevious() 
					next
				end if
				if not IsNull(rsPianClasse("DT_INISESS")) then
					dDtDal = ConvDateToString(rsPianClasse("DT_INISESS"))
				else
					dDtDal = ""
				end if
				if not IsNull(rsPianClasse("DT_INISESS")) then
					dDtAl  = ConvDateToString(rsPianClasse("DT_FINSESS"))
				else
					dDtAl  = ""
				end if
				sCodSessione=rsPianClasse("DESC_FASE")
				'sCodSessione = DecFaseBando(nBando,rsPianClasse("COD_SESSIONE"))%>
				<input type="hidden" name="cmbBando" value="<%=nBando%>">
				<input type="hidden" name="PrgFase" value="<%=rsPianClasse("PRG_FASE")%>">
				<input type="hidden" name="txtClasse" value="<%=nIdClasse%>">
				<input type="hidden" name="cmbSessioni" value="<%=rsPianClasse("COD_SESSIONE")%>">
				<input type="hidden" name="txtDtDal" value="<%=dDtDal%>">
				<input type="hidden" name="txtDtAl" value="<%=dDtAl%>">
				<input type="hidden" name="idAula" value="<%=nIDAula%>">
				<input type="hidden" name="descAula" value="<%=sDescAula%>">

				</form>
				<form method="post" name="InsRuolo<%=i%>" action="CLA_VisRuolo.asp">
					<input type="hidden" name="txtDtDal" value="<%=dDtDal%>">
					<input type="hidden" name="txtDtAl" value="<%=dDtAl%>">
					<input type="hidden" name="IdClasse" value="<%=nIdClasse%>">
					<input type="hidden" name="sessione" value="<%=rsPianClasse("COD_SESSIONE")%>">
					<input type="hidden" name="cmbBando" value="<%=nBando%>">
					<input type="hidden" name="descAula" value="<%=sDescAula%>">
					<input type="hidden" name="cmbSessioni" value="<%=sSessione%>">					
						
				</form>
<%			if clng(rsPianClasse("PRG_FASE")) = 0 then
%>				<tr class="sfondoComm">
				    <td class="tblDett" align="left" width="50">
						<%=rsPianClasse("PRG_FASE")%>
					</td>
					<td class="tblDett" align="left" width="50">
						<%=rsPianClasse("COD_SESSIONE")%>
					</td>	
					<td class="tblDett" align="left" width="300">
						<%=sCodSessione%>
					</td>	
					<td class="tblDett" align="left" width="100">
						<%=dDtDal%>
					</td>	
					<td class="tblDett" align="left" width="100">
						<%=dDtAl%>
					</td>	
					<td align="left" width="50" class="tblDett">
						-
					</td>	
				</tr>
<%			else	%>
                <tr class="sfondoComm">
				    <td class="tblDett" align="left" width="50">
						<%=rsPianClasse("PRG_FASE")%>
					</td>
					<td class="tblDett" align="left" width="50">
						<%=rsPianClasse("COD_SESSIONE")%>
					</td>	
					<td class="tblDett" align="left" width="300">
						<%if dDtAl <> "" then
							if CDate(dOggi) <= CDate(rsPianClasse("DT_FINSESS")) then %>
								<a onmouseover="window.status =' '; return true" class="tblAgg" title="Modifica pianificazione della sessione : <%=sCodSessione%>" href="Javascript:frmModPianificazione<%=i%>.submit()"><b><%=sCodSessione%></b></a>
							<%else%>
								<%=sCodSessione%>
							<%end if
						else%>
							<a onmouseover="window.status =' '; return true" class="tblAgg" title="Modifica pianificazione della sessione : <%=sCodSessione%>" href="Javascript:frmModPianificazione<%=i%>.submit()"><b><%=sCodSessione%></b></a>
						<%end if%>
					</td>	
					<td class="tblDett" align="left" width="100">
						<%=dDtDal%>
					</td>	
					<td class="tblDett" align="left" width="100">
						<%=dDtAl%>
					</td>	
					<td align="left" width="50" class="tblDett">
						<% if dDtDal <> "" then %>
						   <a onmouseover="window.status =' '; return true" href="Javascript:InsRuolo<%=i%>.submit()" Title="Inserimento / modifica ruolo"><img border="0" src="<%=Session("Progetto")%>/images/Formazione/Raggruppa.gif"></a>
					    <% else %>
						   -
					    <% end if %>
					</td>	
				</tr>
<%			end if	
				rsPianClasse.movenext
				if not rsPianClasse.EOF then
					nPrgFase = cint(rsPianClasse("PRG_FASE"))
				end if
				i = i + 1
			loop
		else%>
			<tr>
				<td colspan="3" class="tblText3" align="center">
					&nbsp;
				</td>	
			</tr>
			<tr>
				<td colspan="3" class="tblText3" align="center">
					Non esitono corsi pianificati per la classe selezionata
				</td>	
			</tr>
		<%end if
		rsPianClasse.close
		set rsPianClasse = nothing
		%>
	</table>
	<br>
	<table border="0" cellspacing="2" cellpadding="1" width="500">
		<tr>
			<td align="center" width="50%">
				<a onmouseover="window.status =' '; return true" href="JavaScript:indietro.submit()" title="Indietro"><img border="0" src="<%=Session("Progetto")%>/images/indietro.gif"></b></a>
			</td>
		</tr>
	</table>
	
<%end sub%>
<form name="indietro" method="post" action="CLA_VisClassi.asp">
	<input type="hidden" name="cmbBando" value="<%=nBando%>">
	<input type="hidden" name="cmbSessioni" value="<%=sSessione%>">
</form>
<form name="elimina" method="post" action="CLA_CnfClassi.asp">
	<input type="hidden" name="Modo" value="Canc">
	<input type="hidden" name="cmbBando" value="<%=nBando%>">
	<input type="hidden" name="txtClasse" value="<%=nIdClasse%>">
	<input type="hidden" name="cmbSessioni" value="<%=sSessione%>">
</form>
<!--#include virtual ="/include/closeconn.asp"-->
<!--#include virtual ="/Strutt_coda2.asp"-->
