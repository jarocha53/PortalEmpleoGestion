<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->

<!--#include virtual = "/Strutt_testa2.asp"-->
<!--#include virtual = "/include/openconn.asp"-->
<!--#include virtual = "/util/dbutil.asp"-->
<!--#include virtual = "/include/ControlDateVB.asp"-->
<!--#include virtual = "/include/SysFunction.asp"-->
<!--#include file	 = "CLA_InizioClassi.asp"-->
<!--include file = "InvioMail_mod_INAIL.asp"-->
<script language="Javascript">
<!-- #include Virtual="/include/ControlString.inc" -->
</script>
<%

if ValidateService(Session("idutente"),"CLA_VisClassi",CC) <> "true" then 
	response.redirect "/util/error_login.asp"
end if

Inizio()

'on error resume next

dim sIdCForm
dim nEdizione
dim sIdAula
dim nBando
dim sTurno
dim dOggi
dim sIdClasse
dim sOlDEdizione
dim sOlDAula,sTesto

dOggi = (Now())
sModo = Request.Form("Modo")
nBando		= CLng(Request.Form("cmbBando"))
sSessione	= Request.Form("cmbSessioni")
sIdCForm	= Request.Form ("txtCForm")
nEdizione	= CLng(Request.Form ("cmbEdizione"))
nIdAula		= CLNG(Request.Form ("cmbAula"))
sDescAula   = Request.Form ("descAula")
nIdClasse = CLng(Request.Form ("txtClasse"))
sTesto     = Trim(Ucase(Replace(Request.Form ("testo"),"'","''")))

'nBando		= clng(Request.Form ("txtBando"))
'sTurno		= Request.Form ("cmbTipoClasse")

%>
<form name="indietro" method="post" action="CLA_VisClassi.asp">
	<input type="hidden" name="cmbBando" value="<%=nBando%>">
	<input type="hidden" name="cmbSessioni" value="<%=sSessione%>">
	<input type="hidden" name="descAula" value="<%=sDescAula%>">
</form>

<form name="indietro1" method="post" action="CLA_ModClassi.asp">
	<input type="hidden" name="Bando" value="<%=nBando%>">
	<input type="hidden" name="Sessione" value="<%=sSessione%>">
	<input type="hidden" name="ID" value="<%=nIdClasse%>">
	<input type="hidden" name="descAula" value="<%=sDescAula%>">
</form>

<%
select case sModo
	case "Pianificazione" 

		nIdClasse = CLng(Request.Form ("txtClasse"))

		dDtDal	 = ConvStringToDate(Request.Form("txtDtDal"))
		dDtAl	 = ConvStringToDate(Request.Form("txtDtAl"))
		dDtDal	 = ConvDateToDbs(dDtDal)
		dDtAl	 = ConvDateToDbs(dDtAl)
		sSessNav = Request.Form ("sessione")
		nIdAula  = CLng(Request.Form("cmbAula"))

		CC.BeginTrans
		
		sSQL = "UPDATE CLASSE_PERCORSO SET DT_INISESS=" & dDtDal &_
			", DT_FINSESS =" & dDtAl 
			
			If sTesto <> "" Then
				sSql = sSql & ",NOTE ='" & sTesto & "'" 
			End If
			
			sSQl = sSQl & " WHERE ID_CLASSE=" & nIdClasse & " AND COD_SESSIONE='" &_
			sSessione & "'"
		'Response.Write sSQl
		
		sErrore = EseguiNoC(sSQL,CC)
		
		if sErrore = "0" then
		   sSQL ="SELECT PRG_FASE FROM CLASSE_PERCORSO WHERE ID_CLASSE=" & nIdClasse
		   
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
		   set rsZero = cc.execute(sSQL)
		   if not rsZero.eof then
		       sSQL= "SELECT MIN(DT_INISESS) AS MINIMO, MAX(DT_FINSESS) AS MASSIMO " &_
		             "FROM CLASSE_PERCORSO WHERE ID_CLASSE=" & nIdClasse
		            
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
		       set rsDate = cc.execute(sSQL)
		       if not rsDate.eof then
		          sSQL = "UPDATE CLASSE_PERCORSO SET DT_INISESS =" & ConvDateToDbs(rsDate("MINIMO")) &_
		                  ",DT_FINSESS = " & ConvDateToDbs(rsDate("MASSIMO")) &_
		                  " WHERE ID_CLASSE =" & nIdClasse & " AND PRG_FASE =0"
		       '   Response.Write sSQL
		          sErrore = EseguiNoC(sSQL,CC)
		       end if     
		       set rsDate = nothing
		   end if
		   set rsZero = nothing
		end if  
		 
		if sErrore = "0" and Trim(Request.Form("txtDtInizioOld")) <> "" then
			' Devo controllare se qualche componente della classe � 
			' stato spostato in questa sessione formativa. Il controllo �
			' sul dt_ruolodal che deve coincidere con la vecchia data inizio
			' della sessione formativa.
			dDTIniSessOLD = ConvStringToDate( Request.Form("txtDtInizioOld") )
			dDTIniSessOLD = ConvDateToDbs(dDTIniSessOLD)
			
			sSQL = "UPDATE COMPONENTI_CLASSE SET DT_RUOLODAL=" & dDtDal &_
				" WHERE ID_CLASSE=" & nIdClasse & " AND DT_RUOLOAL IS NULL" &_
				" AND DT_RUOLODAL = " & dDTIniSessOLD 
			sErrore = EseguiNoC(sSQL,CC)
		end if
		
		
		if sErrore = "0" and Trim(Request.Form("txtDtInizioOld")) <> "" then  
			' Aggiorno anche il dt_ruolodal ed il dt_ruoloal del docente della classe.
			dDTFinSessOLD = ConvStringToDate( Request.Form("txtDtFineOld") )
			dDTFinSessOLD = ConvDateToDbs(dDTFinSessOLD)
			sSQL = "UPDATE CLASSE_RUOLO SET DT_RUOLODAL=" & dDtDal &_
				" WHERE ID_CLASSE=" & nIdClasse & " AND COD_SESSIONE='" &_
				sSessione & "' AND DT_RUOLODAL = " & dDTIniSessOLD 
			'Response.Write "<BR> 1 - " & sSQL
			sErrore = EseguiNoC(sSQL,CC)
			if sErrore = "0" then			
				sSQL = "UPDATE CLASSE_RUOLO SET DT_RUOLOAL =" & dDtAl & "WHERE " &_
					" ID_CLASSE=" & nIdClasse & " AND COD_SESSIONE='" &_
					sSessione & "' AND DT_RUOLOAL = " & dDTFinSessOLD 
			'	Response.Write "<BR> 2 - " & sSQL
				sErrore = EseguiNoC(sSQL,CC)
			end if
		end if
		
		'Response.End 
		if nIdAula <> 0 and sErrore = "0" then 
			' Lo spostamento � solo di aula e non di date.
			sSQL = "UPDATE CLASSE_PERCORSO SET ID_AULA = " & nIdAula &_
				" WHERE ID_CLASSE = " & nIdClasse & " AND ID_AULA IS NOT NULL"  &_
				" AND COD_SESSIONE='" &	sSessione & "'"
			sErrore = EseguiNoC(sSQL,CC)
		end if
		%>
		<table width="500" border="0" cellspacing="2" cellpadding="1">
			<tr> 
				<td class="tbltext3" align="center">
			  		<%if sErrore = "0" then
			  			CC.CommitTrans%>
						<script language="Javascript">
							indietro1.Sessione.value = '<%=sSessNav%>'
							alert("Operazione correttamente effettuata")
							indietro1.submit() 
							//indietro.submit()
						</script>
					<%else
			  			CC.RollBackTrans%>
						<script language="Javascript">
							indietro.cmbSessioni.value = '<%=sSessNav%>'
						</script>
						<%Response.Write sErrore
						Indietro()
					end if 
					%>
				</td>
			</tr>
		</table>
		<%		

	case "Canc" 
		nIdClasse = CLng(Request.Form ("txtClasse"))
		' Controllo che nella classe non ci siano discenti per poterla eliminare.
		sSQL = "SELECT COUNT(*) AS CONTA FROM COMPONENTI_CLASSE WHERE ID_CLASSE ="&_
			nIdClasse 
		Errore = 0 
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
		set rsConta = CC.Execute(sSQL)
		if CInt(rsConta("CONTA")) <> 0 then%>
			<br>
			<table width="500" border="0" cellspacing="2" cellpadding="1">
			<tr> 
				<td colspan="2" align="middle" class="tblText3">
		      		La classe non pu� essere eliminata, esistono discenti gia' associati
				</td>
			</tr>
			</table>
		<%else
			CC.BeginTrans
			sSQL = "DELETE CLASSE WHERE ID_CLASSE = " & nIdClasse
			sErrore = EseguiNoC(sSQL,CC)
			if sErrore = "0" then
				sSQL = "DELETE CLASSE_PERCORSO WHERE ID_CLASSE = " & nIdClasse
				sErrore = EseguiNoC(sSQL,CC)
			end if%>
			<br>
			<table width="500" border="0" cellspacing="2" cellpadding="1">
				<tr> 
					<td colspan="2" align="middle" class="tblText3">
				  		<%if sErrore="0" then
							CC.CommitTrans%>
							<script language="Javascript">
								alert("La classe e' stata eliminata correttamente")
								indietro.submit()
							</script>
				  		<%else
							CC.RollBackTrans				  			
				  			Response.Write sErrore
				  		end if%>
					</td>
				</tr>
			</table>
		<%end if
		rsConta.close
		set rsConta = nothing
		Indietro()
	case "Mod" 
		nIdClasse = CLng(Request.Form ("txtClasse"))
		if Request.Form("cmbEdizione") = "" then 
			' Lo spostamento � solo di aula e non di date.
			sSQL = "UPDATE CLASSE_PERCORSO SET ID_AULA = " & nIdAula &_
				" WHERE ID_CLASSE = " & nIdClasse & " AND ID_AULA IS NOT NULL"
			sErrore = EseguiNoC(sSQL,CC)%>
			<table width="500" border="0" cellspacing="2" cellpadding="1">
				<tr> 
					<td class="tbltext3" align="center">
				  		<%if sErrore = "0" then%>
							<script language="Javascript">
								alert("Operazione correttamente effettuata")
								indietro.submit()
							</script>
						<%else
							Response.Write sErrore
							Indietro()
						end if 
						%>
					</td>
				</tr>
			</table>
<%		else ' Cambio totalmente il percorso formativo della classe. 
			
			nIdClasseProv = CLng(Request.Form ("txtClasse"))
			nIdEdizione = CLng(Request.Form("cmbEdizione"))
			' Dalla edizione mi prendo tutte le sessioni formative 
			' che compongono l'edizione con le date di ognuna di essa.
			sSQL = "SELECT FB.COD_SESSIONE, DT_INISESS, DT_FINSESS, PRG_FASE FROM " &_
				"SESS_FORM SF, EDIZIONE E, FASE_BANDO FB WHERE SF.ID_EDIZIONE = E.ID_EDIZIONE " &_
				"AND E.ID_EDIZIONE = " & nIdEdizione &_
				" AND FB.COD_SESSIONE = SF.COD_SESSIONE " &_
				" AND E.ID_BANDO = FB.ID_BANDO " &_
				" ORDER BY PRG_FASE"
			
			set rsSessioni = Server.CreateObject("ADODB.Recordset")
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
			rsSessioni.Open sSQL,CC,3
			
			if not rsSessioni.eof then
				CC.BeginTrans
				' Creo la classe.
				sSQL = "INSERT INTO CLASSE (NUMDISCENTI,MOD_COMP,ID_BANDO,ID_UORG,DT_TMST)" &_
					" VALUES(0,'M'," & nBando & "," & Session("iduorg") & "," & dOggi & ")"

				sErrore = EseguiNoC(sSQL,CC)
				if sErrore = "0" then
					sSQL = "SELECT ID_CLASSE FROM CLASSE WHERE " &_
						" NUMDISCENTI = 0 " &_
						" AND MOD_COMP ='M' " &_
						" AND ID_BANDO = " & nBando &_
						" AND ID_UORG = " & Session("iduorg") &_
						" AND DT_TMST = " & dOggi
		
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
					set rsClasse = CC.Execute(sSQL)
					if not rsClasse.eof then
						nIdClasse = CLng(rsClasse("ID_CLASSE"))
					else
						sErrore = "Errore durante la creazione della classe nuova"
					end if
					rsClasse.close
					set rsClasse = nothing
				end if
				do while not rsSessioni.eof
					if sErrore <> "0" then
						exit do
					end if
					nPrgFase = rsSessioni("PRG_FASE")
					sSessione = rsSessioni("COD_SESSIONE")
					dIniSess = rsSessioni("DT_INISESS")
					dFinSess = rsSessioni("DT_FINSESS")
					' Creo le occorrenze in CLASSE_PERCORSO.
					sSQL = "INSERT INTO CLASSE_PERCORSO (PRG_FASE,ID_CLASSE,COD_SESSIONE,ID_EDIZIONE,DT_INISESS,DT_FINSESS,ID_AULA,DT_TMST)" &_
						" VALUES(" &_
						nPrgFase & "," &_
						nIdClasse & ",'" &_
						sSessione & "'," &_
						nIdEdizione & "," &_
						ConvDateToDbs(dIniSess) & "," &_
						ConvDateToDbs(dFinSess) & "," &_
						nIdAula & ",SYSDATE)"
					sErrore = EseguiNoC(sSQL,CC)
					rsSessioni.MoveNext()
				loop
				rsSessioni.MoveFirst()
				dIniSess = rsSessioni("DT_INISESS")				 		
				if sErrore = "0" then
					' Sposto tutti i discenti verso la classe nuova aggiornando i loro
					' dt_ruolodal.
					sSQL = "UPDATE COMPONENTI_CLASSE SET ID_CLASSE =" & nIdClasse &_
						", DT_RUOLODAL = " & ConvDateToDbs(dIniSess) &_
						" WHERE ID_CLASSE = " & nIdClasseProv &_
						" AND DT_RUOLOAL IS NULL "
					sErrore = EseguiNoC(sSQL,CC)
				end if

				%>
				<br>
				<table width="500" border="0" cellspacing="2" cellpadding="1">
				<tr> 
					<td colspan="2" align="middle" class="tbltext3">
					
				  		<%if sErrore = "0" then
			      			CC.CommitTrans
		'	      			CC.RollBackTrans%>
				<!------------------------------------------------>
				<!--mail: messaggio-->				       				        
				<!------------------------------------------------>	      		
							<script language="Javascript">
								alert("Operazione correttamente effettuata")
								indietro.submit()
							</script>
						<%else
			      			CC.RollBackTrans
							Response.Write "<BR>sErrore = " & sErrore
							Indietro()
						end if %>
					</td>
				</tr>
				</table>

<%			else%>

				<table width="500" border="0" cellspacing="2" cellpadding="1">
					<tr> 
						<td class="tbltext3" align="center">
							L'edizione scelta non ha sessioni formative associate.
						</td>
					</tr>
				</table>
<%				Indietro()
			end if
			rsSessioni.close
			set rsSessioni = nothing

		end if
	case else ' Inserimento di una nuova classe.
	 
		dDTDal = convStringToDate(Request.Form ("txtDtDal"))
		dDTAl  = convStringToDate(Request.Form ("txtDtAl"))
		nPrgFase = Request.Form ("prgFase")
		nIdAula	= CLNG(Request.Form("cmbAula"))
		' Controllo se l'aula � disponibile 
		
		if nIdAula = 0 then	
			sSQL = "SELECT COUNT(ID_CLASSE) conta FROM CLASSE_PERCORSO " &_
				" WHERE COD_SESSIONE = '" & Sessione & "'" &_
				" AND ID_AULA = " & nIdAula & " AND (" & ConvDateToDbs(dDTDal) &_
				" BETWEEN DT_INISESS AND DT_FINSESS OR " & ConvDateToDbs(dDTal) &_
				" BETWEEN DT_INISESS AND DT_FINSESS ) "
		else
			sSQL = "SELECT COUNT(ID_CLASSE) conta FROM CLASSE_PERCORSO " &_
				" WHERE COD_SESSIONE = '" & Sessione & "'" &_
				" AND (" & ConvDateToDbs(dDTDal) &_
				" BETWEEN DT_INISESS AND DT_FINSESS OR " & ConvDateToDbs(dDTal) &_
				" BETWEEN DT_INISESS AND DT_FINSESS ) "
		end if	
		
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
		set rsConta = CC.Execute(sSQL)
		
		if cint(rsConta("conta")) = 0 then
			CC.BeginTrans
	'		sSQL = "SELECT DT_INISESS,DT_FINSESS,C.PRG_FASE FROM " &_
	'			" EDIZIONE A, SESS_FORM B ,FASE_BANDO C WHERE A.ID_BANDO =" &_
	'			 nBando & " AND A.ID_EDIZIONE = B.ID_EDIZIONE AND " &_
	'			 " B.COD_SESSIONE ='" & sSessione & "'" &_
	'			 " AND ID_UORG =" & Session("iduorg") &_
	'			 " AND A.ID_EDIZIONE = " & nEdizione &_
	'			 " AND B.COD_SESSIONE = C.COD_SESSIONE " &_
	'			 " AND C.ID_BANDO = A.ID_BANDO "

			sSQL = "SELECT PRG_FASE,COD_SESSIONE,COD_TFASE FROM FASE_BANDO " &_
				" WHERE ID_BANDO =" & nBando 
			'	& " AND PRG_FASE >= " & nPrgFase
'			Response.Write sSQL
			
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
			set rsFaseBando = CC.Execute(sSQL)
			if not rsFaseBando.eof then
				dSysDate = ConvDateToDB(Now())
				sSQL = "INSERT INTO CLASSE (ID_UORG,ID_BANDO,MOD_COMP,NUMDISCENTI,DT_TMST) VALUES (" &_
						Session("iduorg") & "," & nBando & ",'M',0," & dSysDate &  ")"
				sErrore = EseguiNoC(sSQL,CC)
				if sErrore = "0" then
					sSQL = "SELECT ID_CLASSE FROM CLASSE WHERE " &_
						" ID_UORG = " & Session("iduorg") &_
						" AND ID_BANDO = " & nBando &_
						" AND MOD_COMP ='M'" &_
						" AND NUMDISCENTI = 0 " &_
						" AND DT_TMST = " & dSysDate
						
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
					set rsClasseNew = CC.Execute(sSQL)
					if not rsClasseNew.eof then
						nIdClasse = CLng(rsClasseNew("ID_CLASSE"))
						do while not rsFaseBando.eof
							' Associo la l'aula alla classe solo se 
							' la sessione � definita come in presenza.
							nIdAula	= CLNG(Request.Form("cmbAula"))
							
							if nIdAula = 0 then  
								nIdAula = "null"
							end if
							if rsFaseBando("COD_TFASE") = "N" then
								nIdAula = "null"
							end if
													
							if sSessione = rsFaseBando("COD_SESSIONE") then
								sSQL = "INSERT INTO CLASSE_PERCORSO(ID_CLASSE,COD_SESSIONE,DT_INISESS,DT_FINSESS,ID_AULA,PRG_FASE,DT_TMST,NOTE) VALUES (" &_
										nIdClasse & ",'" & rsFaseBando("COD_SESSIONE") & "'," & ConvDateToDBs(dDTDal)& "," & ConvDateToDBs(dDTAl)& "," & nIdAula &_
										"," & rsFaseBando("PRG_FASE") & "," & ConvDateToDB(Now()) & ",'"  & sTesto & "'" & ")"
							else
								sSQL = "INSERT INTO CLASSE_PERCORSO(ID_CLASSE,COD_SESSIONE,ID_AULA,PRG_FASE,DT_TMST,NOTE) VALUES (" &_
										nIdClasse & ",'" & rsFaseBando("COD_SESSIONE") & "'," & nIdAula & "," & rsFaseBando("PRG_FASE") &_
										"," & ConvDateToDB(Now()) & ",'')"
							end if

							sErrore = EseguiNoC(sSQL,CC)
							if sErrore <> "0" then
								exit do
							end if
							rsFaseBando.movenext
						loop
					else
						sErrore = "Errore durante la creazione della classe"
					end if
					rsClasseNew.close
					set rsClasseNew = nothing
				end if
			else
				sErrore ="Errore durante la creazione della classe"
			end if
			rsFaseBando.close
			set rsFaseBando = nothing
				%>
			<br>
			<table width="500" border="0" cellspacing="2" cellpadding="1">
			<tr> 
				<td colspan="2" align="middle" class="tbltext3">
			  		<%if sErrore = "0" then
		      			CC.CommitTrans
	'	      			CC.RollBackTrans%>
			<!------------------------------------------------------------------------>
			<!--mail: messaggio-->				       				        
			<!------------------------------------------------------------------------>	      		
						<script language="Javascript">
							alert("Operazione correttamente effettuata")
							indietro.submit()
						</script>
					<%else
		      			CC.RollBackTrans
						Response.Write "<BR>sErrore = " & sErrore
						Indietro()
					end if %>
				</td>
			</tr>
			</table>
		
<%		else %>
			<br>
			<table width="500" border="0" cellspacing="2" cellpadding="1">
			<tr> 
				<td colspan="2" align="middle" class="tbltext3">
				Aula non disponibile
				</td>
			</tr>
			</table>
		
<%			Indietro()
		end if
		rsConta.close
		set rsConta= nothing 
	
end select

sub Indietro%>
	<br>
	<table width="500" border="0" cellspacing="2" cellpadding="1">
		<tr> 
			<td align="center">
				<a onmouseover="window.status =' '; return true" title="Indietro" href="Javascript:indietro.submit()"><img border="0" src="<%=Session("Progetto")%>/images/indietro.gif"></a>
			</td>
		</tr>
	</table>
<%end sub%>


<!--#include virtual = "/include/closeconn.asp"-->
<!--#include virtual = "/Strutt_Coda2.asp"-->
