<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<script language="Javascript" src="/Include/help.inc"></script>
<script language="Javascript">

	//include del file per fare i controlli sulla validit� delle date
	<!--#include virtual = "/Include/ControlDate.inc"-->

	//include del file per fare i controlli sulla numericit� dei campi
	<!--#include virtual = "/Include/ControlNum.inc"-->
	<!--#include virtual = "/Include/ControlString.inc"-->

	function ControllaDati() {
		frmModSessione.txtDtPrec.value	= TRIM(frmModSessione.txtDtPrec.value)
		frmModSessione.txtDtSucc.value	= TRIM(frmModSessione.txtDtSucc.value)

//		frmModSessione.txtDtInizioOld.value = TRIM(frmModSessione.txtDtInizioOld.value)
//		frmModSessione.txtDtFineOld.value	= TRIM(frmModSessione.txtDtFineOld.value)

		// Controllo obbligatoriet� della data.
		if (frmModSessione.txtDtDal.value == "" ) {
			alert("Indicare la data inizio")
			frmModSessione.txtDtDal.focus();
			return false;
		}			
		// Controllo la validit� della data.
		if (!ValidateInputDate(frmModSessione.txtDtDal.value)) {
			frmModSessione.txtDtDal.focus();
			return false;
		}	
		// Controllo obbligatoriet� della data.
		if (frmModSessione.txtDtAl.value == "") {
			alert("Indicare la data fine")
			frmModSessione.txtDtAl.focus();
			return false;
		}
		
		// Controllo la validit� della data.
		if (!ValidateInputDate(frmModSessione.txtDtAl.value)) {
			frmModSessione.txtDtAl.focus();
			return false;
		}
		
		// Se non � vuoto controllo l'integrita con il precedente.
		if (frmModSessione.txtDtPrec.value != "") {
			if (!ValidateRangeDate(frmModSessione.txtDtPrec.value,frmModSessione.txtDtDal.value)) {
				alert("La data inizio sessione si sovrappone al \nperiodo della sessione precedente");
				return false;		
			}
		}
		// Se non � vuoto controllo l'integrita con il successivo.
		if (frmModSessione.txtDtSucc.value != "") {
			if (!ValidateRangeDate(frmModSessione.txtDtAl.value,frmModSessione.txtDtSucc.value)) {
				alert("La data fine sessione si sovrappone al \nperiodo della sessione successiva");
				return false;
			}
		}
		// Controllo validit� dell'intervallo.
		if (!ValidateRangeDate(frmModSessione.txtDtDal.value,frmModSessione.txtDtAl.value)) {
				alert("L'intervallo della sessione non � valido");
			return false;
		}
		 
		return true;
	}
	
// 3-11-03	con il tasto elimina annullo la programmazione della sessione formativa
	
function elimina() {
	if (confirm("Confermi l'annullamento della programmazione della sessione formativa?") == true) {
		document.frmModSessione.action = "CLA_DelSessione.asp"
		document.frmModSessione.submit();
	}
}	

</script>



<%sub InfoProc()%>
	<table cellpadding="0" cellspacing="0" width="500" border="0">
		<tr height="18">
			<td class="sfondomenu" height="18" width="50%">
				<span class="tbltext0"><b>&nbsp;GESTIONE CLASSI</b>
				</span>
			</td>
			<td width="5%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
			<td valign="middle" align="right" width="40%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1"></td>
		</tr>
		<tr width="371" class="SFONDOCOMM">
			<td colspan="3">
			Qui � possibile modificare il periodo di una sessione formativa e, nel caso
			sia una sessione in presenza, modificare l'aula che ospita la classe con un'altra
			disponibile.
			<a onmouseover="window.status =' '; return true" href="Javascript:Show_Help('/Pgm/Help/gestprogetti/Formazione/Classi/CLA_ModPianificazione')">
			<img src="<%=Session("Progetto")%>/images/Help.gif" border="0" align="right" alt="Help">

			</td>
		</tr>
		<tr height="2">
			<td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>
<%end sub%>


<!--#include virtual = "/Strutt_Testa2.asp"-->
<!--#include virtual = "/include/openconn.asp"-->
<!--#include virtual = "/include/ControlDateVB.asp"-->
<!--#include file	 = "CLA_InizioClassi.asp"-->
<!--#include virtual = "/include/SelAreaTerrBandi.asp"-->
<%
if ValidateService(session("idutente"),"CLA_VisClassi",CC) <> "true" then 
	response.redirect "/util/error_login.asp"
end if

dim nIdClasse,sCodSessione,nPrgFase
dim nIdBando,dDTDal,dDTAl
dim dDTDalSucc,dDTAlPrec,dOggi

dOggi = Date
Inizio()
InfoProc()

sSessioneNavig  = Request.Form("Sessione")
nIdClasse		= CLng(Request.Form("txtClasse"))
sCodSessione	= Request.Form("cmbSessioni")
nPrgFase		= CInt(Request.Form("PrgFase"))
nBando			= CLng(Request.Form("cmbBando"))
dDTDal			= Trim(Request.Form("txtDtDal"))
dDTAl			= Trim(Request.Form("txtDtAl"))
dDTDalSucc		= Request.Form("txtDtDalSucc")
dDTAlPrec		= Request.Form("txtDtAlPrec")
nIDAula			= CLng(Request.Form ("idAula"))

sDescAula 		= Request.Form ("descAula") 

sSQL = "SELECT MAX(DT_FINSESS) AS MASSIMO FROM CLASSE_PERCORSO WHERE " &_
       " ID_CLASSE = " & nIdClasse & " AND PRG_FASE <" & nPrgFase  & " AND PRG_FASE>0"

'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
set rsData = cc.execute(sSQL)
    if not rsData.eof then
        dDTAlPrec = rsData("MASSIMO")
    else
        dDTAlPrec = ""
    end if
set rsData = nothing

sSQL = "SELECT MIN(DT_INISESS) AS MINIMO FROM CLASSE_PERCORSO WHERE " &_
       " ID_CLASSE = " & nIdClasse & " AND PRG_FASE >" & nPrgFase  

'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
set rsData1 = cc.execute(sSQL)
    if not rsData1.eof then
        dDTDalSucc  = rsData1("MINIMO")
    else
        dDTDalSucc  = ""
    end if
set rsData1 = nothing
' Response.Write "<BR> dDTDalSucc = " & dDTDalSucc
' Response.Write "<BR> dDTAlPrec = " & dDTAlPrec
' Response.Write "<BR> nIdClasse = " & nIdClasse
' Response.Write "<BR> sCodSessione = " & sCodSessione
' Response.Write "<BR> nPrgFase = " & nPrgFase
%>
<form name="frmModSessione" onsubmit="return ControllaDati()" method="post" action="CLA_CnfClassi.asp">
<%
InfoArea()
if nIDAula <> 0 then
	Aula()
end if

Alert()
DatiSessione()

%>
<!--#include virtual ="/include/closeconn.asp"-->
<!--#include virtual ="/Strutt_coda2.asp"-->



<%sub Alert()%>
	<table border="0" cellspacing="2" cellpadding="1" width="500">
		<tr>
			<td class="tbltext3" align="center">
	<%
	
	    if dDTAlPrec <> "" then
			Response.Write " <BR>La sessione precedente termina il : " & dDTAlPrec & "."
		end if
		if dDTDalSucc <> "" then
			Response.Write " <BR> La sessione successiva � prevista il : " & dDTDalSucc
		end if
  
	%>
			</td>
		</tr>
	</table>		
	<br>
<%end sub

sub DatiSessione()

	'sSqlnote = " SELECT note FROM classe_percorso WHERE id_classe=" & nIdClasse &_
	'		   " AND cod_sessione='" & sCodSessione  & "'" & " AND id_aula=" & nIDAula

sSqlnote = " SELECT note FROM classe_percorso WHERE id_classe=" & nIdClasse &_
			   " AND cod_sessione='" & sCodSessione  & "'"


'PL-SQL * T-SQL  
SSQLNOTE = TransformPLSQLToTSQL (SSQLNOTE) 
	set rsNote = cc.Execute(sSqlnote)
	sNote=rsNote("note")
%>
	<input type="hidden" name="txtDtInizioOld" value="<%=dDTDal%>">
	<input type="hidden" name="txtDtFineOld" value="<%=dDTAl%>">
	<input type="hidden" name="sessione" value="<%=sSessioneNavig%>">
	<input type="hidden" name="descAula" value="<%=sDescAula%>">

	<input type="hidden" name="txtDtPrec" value="<%=dDTAlPrec%>">
	<input type="hidden" name="txtDtSucc" value="<%=dDTDalSucc%>">
	
	<input type="hidden" name="cmbBando" value="<%=nBando%>">
	<input type="hidden" name="cmbSessioni" value="<%=sCodSessione%>">
	<input type="hidden" name="txtClasse" value="<%=nIdClasse%>">
	<input type="hidden" name="Modo" value="Pianificazione">
	<!--table border="0" cellspacing="2" cellpadding="1" width="500">		<tr class="sfondocomm">			<td  align="center"><B>Dettagli sessione</B></td>			</tr>	</Table-->	
	<table border="0" cellspacing="2" cellpadding="1" width="500">
		<tr class="tblsfondo">
			<td class="tbltext1" align="left" width="50">
				<b>Dal </b>
			</td>	
			<td class="textBlack" align="left" width="200"><b>
			
				<%if dDTDal <> "" then
					if cdate(dOggi) <= cdate(ConvStringToDate(dDTDal)) then%>
						<input class="textBlack" maxlength="10" size="12" type="text" name="txtDtDal" value="<%=dDTDal%>">
					<%else%>
						<input maxlength="10" size="12" name="txtDtDal" type="hidden" value="<%=dDTDal%>">				
						<%=dDTDal%>
					<%end if%>
				<%else%>
						<input class="textBlack" maxlength="10" size="12" type="text" name="txtDtDal" value="<%=dDTDal%>">
				<%end if%>
				</b>
			</td>	
			<td class="tbltext1" align="left" width="50">
				<b>Al</b>
			</td>	
			<td class="textBlack" align="left" width="200"><b>
				<%
				if dDTAl <> "" then
					if cdate(dOggi) <= cdate(ConvStringToDate(dDTAl)) then%>
						<input class="textBlack" maxlength="10" size="12" type="text" name="txtDtAl" value="<%=dDTAl%>">
					<%else%>
						<input maxlength="10" size="12" name="txtDtAl" type="hidden" value="<%=dDTAl%>">				
						<%=dDTAl%>
					<%end if%>
				<%else%>
						<input class="textBlack" maxlength="10" size="12" type="text" name="txtDtAl" value="<%=dDTAl%>">
				<%end if%>
				</b>
			</td>	
		</tr>
		<tr class="tblsfondo">
		<td class="tbltext1" align="center" colspan="4">
				Utilizzabili 
				<label name="NumCaratteri" id="NumCaratteri">100</label>
				caratteri
			</td>
		</tr>	
		<tr class="tblsfondo">
			<td class="tbltext1" align="center"><b>Note</b></td>				
			<td align="left" colspan="3">
<%			
			If sNote <> "" Then%> 
				<textarea onKeyup="JavaScript:CheckLenTextArea(Testo,NumCaratteri,100)" cols="40" rows="5" name="Testo"><%=sNote%></textarea>
<%			Else  %>
				<textarea onKeyup="JavaScript:CheckLenTextArea(Testo,NumCaratteri,100)" cols="40" rows="5" name="Testo"></textarea>
<%			End If  %>
			</td>
		</tr>
	</table>
	<br>
	<table border="0" cellspacing="0" cellpadding="0" width="300">
		<tr>
			<td class="tbltext1" align="center">
				<a onmouseover="window.status =' '; return true" title="Indietro" href="Javascript:indietro.submit()"><img border="0" src="<%=Session("Progetto")%>/images/indietro.gif"></a>
			</td>	
			<td class="tbltext1" align="center">
				<input title="Conferma" type="image" src="<%=Session("Progetto")%>/images/conferma.gif">
			</td>	
			
			<%
				if dDTDal <> "" then
					if cdate(dOggi) < cdate(ConvStringToDate(dDTDal)) then%>
			<td class="tbltext1" align="center" width="100">
			<a onmouseover="window.status =' '; return true" title="Elimina" href="Javascript:elimina()"><img border="0" src="<%=Session("Progetto")%>/images/elimina.gif"></a>
					<%end if%>
				<%end if%>
			</td>
		</tr>
	</table>	
	</form>
<%end sub%>
	<form name="indietro" method="post" action="CLA_ModClassi.asp">
		<input type="hidden" name="descAula" value="<%=sDescAula%>">

		<input type="hidden" name="ID" value="<%=nIdClasse%>">
		<input type="hidden" name="Bando" value="<%=nBando%>">
		<input type="hidden" name="Sessione" value="<%=sSessioneNavig%>">
	</form>
<%sub InfoArea()%>

	<table border="0" cellspacing="2" cellpadding="1" width="500">
		<tr>
			<td class="tbltext1" align="left" width="125">
				<b>Bando :</b>
			</td>	
			<td class="textBlack" align="left">
				<b><%=DecBando(nBando)%></b>
			</td>
		</tr>
		<tr>
			<td class="tbltext1" align="left" width="125">
				<b>Sessione Formativa :</b>
			</td>	
			<td class="textBlack" align="left" class="textBlack">
				<b><%=DecFaseBando(nBando,sCodSessione)%></b>
			</td>
		</tr>
	</table>
<%end sub%>

<%sub Aula()

	sSQL = " SELECT PRV FROM CENTRO_FORMAZIONE CF, AULA A WHERE CF.ID_CFORM = A.ID_CFORM "
	sSQL = sSQL &  " AND A.ID_CFORM = CF.ID_CFORM AND ID_AULA = "	& nIDAula
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	set rsPrvForm = cc.Execute(sSQL)
	if not rsPrvForm.eof then
		sPrvCForm = rsPrvForm("PRV")%>
		<table border="0" cellpadding="2" cellspacing="1" width="500">
		    <tr>
				<td width="125" align="left" class="tblText1">
					<b>Aule disponibili :</b>
		        </td>
		        <%
				' **************************************************************************************
				' In base alla provincia del centro di formazione dell'aula a cui � assegnata la classe,
				' mi ricerco le aule disponibili (che non hanno una classe assegnata). 
				' Nella ricerca include anche l'aula a cui appartiene la classe in oggetto.
				' **************************************************************************************
				sSQL = " SELECT DESC_AULA,A.ID_AULA "
				sSQL = sSQL &  "    FROM AULA A,CENTRO_FORMAZIONE CF "
				sSQL = sSQL &  "    WHERE ID_UORG = " & Session("iduorg") & " AND PRV = '" & sPrvCForm & "'"
				sSQL = sSQL &  " 	  AND A.ID_CFORM = CF.ID_CFORM AND NUM_POSTI > 0 "
				'Mario 13/11/2003
				'sSQL = sSQL &  " 	  AND NOT EXISTS ("
				'sSQL = sSQL &  " 	  	  SELECT CP.ID_AULA FROM CLASSE_PERCORSO CP"
				'sSQL = sSQL &  " 	  	  WHERE SYSDATE-1 BETWEEN DT_INISESS AND DT_FINSESS "
				'sSQL = sSQL &  " 	  	  AND A.ID_AULA = CP.ID_AULA"
				'sSQL = sSQL &  " 		  AND A.ID_AULA <> " & nIDAula & ")"
				sSQL = sSQL & " order by DESC_AULA"
				'Mario 13/11/2003
				' **************************************************************************************
'PL-SQL * T-SQL  
SSQL%> = TransformPLSQLToTSQL (SSQL%>) 
				Set rsAula = CC.Execute(sSQL)%>
		        <td align="left" width="375">
					<select name="cmbAula" class="textBlack">
					<%Do While Not rsAula.EOF%>
						<option value="<%=CLng(rsAula("ID_AULA"))%>" <%if nIdAula = CLNG(rsAula("ID_AULA")) then%> selected <%end if%>><%=rsAula("DESC_AULA")%>
						</option>
						<%rsAula.MoveNext()
					Loop%>
					</select>
		        </td>
				<%rsAula.Close
				Set rsAula = Nothing%>
		    </tr>
		</table>

<%	else%>
		<input type="hidden" name="cmbAula" value="0">
<%	end if	
	rsPrvForm.close
	set rsPrvForm = nothing 
end sub%>
