<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%'Option Explicit 
   'Response.ExpiresAbsolute = Now() - 1 
   'Response.AddHeader "pragma","no-cache"
   'Response.AddHeader "cache-control","private"
   'Response.CacheControl = "no-cache"
%>
<!-- #include virtual="/strutt_testa2.asp"-->
<!--#include virtual ="/include/openconn.asp"-->
<!--#include virtual ="/util/portallib.asp"-->
<%
if ValidateService(session("idutente"),"CLA_VISEDIZIONE", CC) <> "true" then 
	response.redirect "/util/error_login.asp"
end if
%>
<!-- ************** Javascript Inizio ************ -->

<script language="Javascript">
<!--#include Virtual = "/Include/help.inc"-->
//include del file per fare i controlli sulla validit� delle date
<!--#include virtual = "/Include/ControlDate.inc"-->

//include del file per fare i controlli sulla numericit� dei campi
<!--#include virtual = "/Include/ControlNum.inc"-->

function ControllaDati() {
	var sFInEdi
	var t,n,z
	var nNumElem
	var aEdizione
	var aCentForm
	var nPrimoEle
	var nNumEdizioni
	var dtOggi
	
    dtOggi = new Date();
    a = dtOggi.getDate();
    if (a < 10)
        a= "0" + a;
    b = dtOggi.getMonth();
    if (b < 10)
       b = "0" + b;
    c = dtOggi.getYear();
    dtOggi = a + "/" + b + "/" + c; 
   
	nNumEdizioni = document.frmCfnEdizione.txtNumEdizione.value;
	nPrimoEle=0;
	aCentForm = new Array;
	t=0;
	while (document.frmCfnEdizione.elements[nPrimoEle].name != "txtSesIni1") 
	{
		
			if (document.frmCfnEdizione.elements[nPrimoEle].name.substr(0,11) == "txtCenForma"){
				aCentForm[t] = document.frmCfnEdizione.elements[nPrimoEle].value;
				t=t+1;
			}
		
		nPrimoEle = nPrimoEle + 1;
	}
	aEdizione = new Array;
	t=0;
	n = nPrimoEle;
	nEdizioniRichieste = 0;
	if (document.frmCfnEdizione.elements[n].value == "") {
	    alert("Occorre indicare almeno la prima data Inizio Edizione");
		document.frmCfnEdizione.elements[n].focus();
		return false;	
	}
	
	if (document.frmCfnEdizione.elements[n].value != "") {
	    if(!ValidateRangeDate(dtOggi,document.frmCfnEdizione.elements[n].value))
	        {  
	           alert("La data inizio non puo' essere inferiore alla data odierna");
				document.frmCfnEdizione.elements[n].focus(); 
				return false;
	    }
	}
	for (i=0;i<nNumEdizioni;i++) {
		if (document.frmCfnEdizione.elements[n].value != "") {
			aEdizione[t]		= document.frmCfnEdizione.elements[n].value;
			aEdizione[t+1]		= document.frmCfnEdizione.elements[n+1].value;
			nEdizioniRichieste	= nEdizioniRichieste + 1;
		}else {
		i = nNumEdizioni;
		}
		n = n + 2;
		t= t + 2;
	}
	// Controllare che le date indicare siano date valide.
	n = nPrimoEle; 
	t=0;
	sFInEdi = "No";
	for (z=0;z<nEdizioniRichieste;z++) {
		if (aEdizione[t] == "") {
		   alert ("La data inizio edizione � obbligatoria");
			return false;
		}
		if (sFInEdi != "No") {
			if (!ValidateRangeDate(sFInEdi,aEdizione[t]))
			{
				alert("Data Edizioni non congruenti");
				document.frmCfnEdizione.elements[n].focus(); 
				return false;
			}else {
				if (aEdizione[t].substr(0,2) == sFInEdi.substr(0,2) && 
					aEdizione[t].substr(3,2) <= sFInEdi.substr(3,2) &&
					aEdizione[t].substr(6,4) <= sFInEdi.substr(6,4))  
				{
					alert("Data Edizioni non congruenti");
					document.frmCfnEdizione.elements[n].focus(); 
					return false;
				}
			}
		}
		if (!ValidateInputDate(aEdizione[t]))
 			{
				document.frmCfnEdizione.elements[n].focus(); 
				return false;
			}
		if (aEdizione[t+1] == "") {
			alert ("La data fine edizione � obbligatoria");
			document.frmCfnEdizione.elements[n].focus(); 
			return false;
		}
		if (aEdizione[t+1] != "") {
			if (!ValidateInputDate(aEdizione[t+1]))
 				{
					document.frmCfnEdizione.elements[n+1].focus(); 
					return false;
				}
			if (!ValidateRangeDate(aEdizione[t],aEdizione[t+1]))
			{
				alert("Data Fine Edizione minore della Data Inizio Edizione");
				document.frmCfnEdizione.elements[n].focus(); 
				return false;
			}
			sFInEdi = aEdizione[t+1];
		}else {
			sFInEdi = aEdizione[t];
		}
		t = t + 2;
		n = n + 2;
	}

	document.frmCfnEdizione.txtCentForm.value = aCentForm;	
	document.frmCfnEdizione.txtEdizioni.value = aEdizione;

	return true;
}

//*****
function ControllaDatiFad() {

	var sFInEdi
	var t,n,z
	var nNumElem
	var aEdizione
	var nPrimoEle
	var nNumEdizioni

	nNumEdizioni = document.frmCfnEdizione.txtNumEdizione.value;
	nPrimoEle=0;
	aCentForm = new Array;
	t=0;
	while (document.frmCfnEdizione.elements[nPrimoEle].name != "txtSesIni1") 
	{			
		nPrimoEle = nPrimoEle + 1;
	}
	aEdizione = new Array;
	t=0;
	n = nPrimoEle;
	nEdizioniRichieste = 0;
	if (document.frmCfnEdizione.elements[n].value == "") {
		alert("Occorre indicare almeno la prima data Inizio Edizione");
		document.frmCfnEdizione.elements[n].focus();
		return false;	
	}
	for (i=0;i<nNumEdizioni;i++) {
		if (document.frmCfnEdizione.elements[n].value != "") {
			aEdizione[t]		= document.frmCfnEdizione.elements[n].value;
			aEdizione[t+1]		= document.frmCfnEdizione.elements[n+1].value;
			nEdizioniRichieste	= nEdizioniRichieste + 1;
		}else {
		i = nNumEdizioni;
		}
		n = n + 2;
		t= t + 2;
	}
	// Controllare che le date indicare siano date valide.
	n = nPrimoEle; 
	t = 0;
	sFInEdi = "No";
	for (z=0;z<nEdizioniRichieste;z++) {
		if (aEdizione[t] == "") {
			alert ("La data inizio edizione � obbligatoria");
			return false;
		}
		if (sFInEdi != "No") {
			if (!ValidateRangeDate(sFInEdi,aEdizione[t]))
			{
				alert("Data Edizioni non congruenti");
				document.frmCfnEdizione.elements[n].focus(); 
				return false;
			}else {
				if (aEdizione[t].substr(0,2) == sFInEdi.substr(0,2) && 
					aEdizione[t].substr(3,2) <= sFInEdi.substr(3,2) &&
					aEdizione[t].substr(6,4) <= sFInEdi.substr(6,4))  
				{
					alert("Data Edizioni non congruenti");
					document.frmCfnEdizione.elements[n].focus(); 
					return false;
				}
			}
		}
		if (!ValidateInputDate(aEdizione[t]))
 			{
				document.frmCfnEdizione.elements[n].focus(); 
				return false;
			}
		if (aEdizione[t+1] == "") {
			alert ("La data fine edizione � obbligatoria");
			document.frmCfnEdizione.elements[n].focus(); 
			return false;
		}
		if (aEdizione[t+1] != "") {
			if (!ValidateInputDate(aEdizione[t+1]))
 				{
					document.frmCfnEdizione.elements[n+1].focus(); 
					return false;
				}
			if (!ValidateRangeDate(aEdizione[t],aEdizione[t+1]))
			{
				alert("Data Fine Edizione minore della Data Inizio Edizione");
				document.frmCfnEdizione.elements[n].focus(); 
				return false;
			}
			sFInEdi = aEdizione[t+1];
		}else {
			sFInEdi = aEdizione[t];
		}
		t = t + 2;
		n = n + 2;
	}
	document.frmCfnEdizione.txtEdizioni.value = aEdizione;

	return true;
}
//*****
</script>
<!-- ************** Javascript Fine   ************ -->
<!-- ************** ASP Inizio ************* -->

<!-- ************** Inizio titolo ***************   -->
<br>
	<table cellpadding="0" cellspacing="0" width="500" border="0">
		<tr height="18">
			<td class="sfondomenu" height="18" width="67%">
			<span class="tbltext0"><b>&nbsp;Definizione Classi</b></span></td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
			<td valign="middle" align="right" class="tbltext1" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">(*) campi obbligatori</td>
		</tr>
		<tr>
			<td class="sfondocomm" width="57%" colspan="3">
				Inserire nei campi testo la data inizio e fine relativi alla sessione che si vuole programmare.
			<a href="Javascript:Show_Help('/Pgm/help/GestProgetti/Formazione/Classi/CLA_CnfEdizione')" onmouseover="javascript:window.status=' '; return true"><img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
			</td>
		</tr>
		<tr height="2">
			<td colspan="3" class="sfondocomm" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
		</tr>
	</table>
	<br>
<!-- ************** FINE titolo ***************   -->
<%sub InPresenza()
nNumCenSel = 0 ' Contatore elementi Ceccati

for i = 1 to cint(sNumElem)  

	sFlag			= Request.Form ("chkSel" & i)
	sCenForma		= Request.Form ("txtCForm" & i)
	sAule			= Request.Form ("txtNUMAULA" & i)
	sMaxPosti		= Request.Form ("txtMAX_POSTI" & i)

	if sFlag = "on" then
		nPostiSel = nPostiSel + sMaxPosti 
		nNumCenSel= nNumCenSel + 1
'		Response.Write "<BR>flag = " & sFlag
'		Response.Write "<BR>Centro forma = " & sCenForma
'		Response.Write "<BR>aule = " & sAule
'		Response.Write "<BR>classi = " & sMaxPosti
%>
		<input type="hidden" name="txtCenForma<%=nNumCenSel%>" value="<%=sCenForma%>">
		<input type="hidden" name="txtAule<%=nNumCenSel%>" value="<%=sAule%>">
		<input type="hidden" name="txtClassi<%=nNumCenSel%>" value="<%=sMaxPosti%>">
	  <%appoCenForm=appoCenForm & sCenForma & ","		
	end if
'	if cint(sNumPers) <= cint(nPostiSel) then 
'		exit for 
'	end if
next%>
<table width="500">
	<tr> 
		<td align="left"> 
			<span class="tbltext">
			<b>
			Bando:&nbsp;<%=DesBando%>
			</b>
			</span> 			
		</td> 
	</tr>
	<tr> 
		<td align="left"> 
			<span class="tbltext">
			<b>
			Area Geografica:&nbsp;<%=sGruppo%> 
			
			</b>
			</span>
		</td>
	</tr>
	<tr>
		<td align="left"> 
			<span class="tbltext">
			<b>
			Numero persone da pianificare:&nbsp;			
			</b>
			</span>
			<span class="tbltext">		
			<b>
			<%=sNumPers%> 
			</b>
			</span>
		</td> 
	</tr>
	<tr>
		<td align="left"> 
			<span class="tbltext">
			<b>
			Numero complessivo posti attualmente disponibili:&nbsp;	
			</b>
			</span>
			<span class="tbltext">
			<b>
			<%=nPostiSel%> 			
			</b>
			</span>
		</td> 
	</tr>
</table>


<table width="500" cellpadding="1" cellspacing="2" border="0">
	<tr> 
		<td align="left"> 
			<span class="tbltext"><b>
<%
		
			nEdizioni = int(clng(sNumPers)/clng(nPostiSel))
			if nEdizioni < (clng(sNumPers)/clng(nPostiSel)) then
			'Response.Write "nEdizioni" & nEdizioni & "<br>"
			'Response.Write "sNumPers" & sNumPers & "<br>"
			'Response.Write "nPostiSel" & nPostiSel & "<br>"
				nEdizioni = nEdizioni + 1
			end if 
			Response.Write "<BR>Le Edizioni previste sono " &_
				nEdizioni				
			
			nResto = cdbl(sNumPers) mod cdbl(nPostiSel)
			
			 'Response.Write "Resto " & nresto
			'-------------	
			if nResto > 0 then
				if clng(sNumPers) <> clng(nPostiSel) then
				'Response.Write "nEdizioni" & nEdizioni & "<br>"
					Response.Write " di cui una incompleta"
					sPippo = " n.c."	
				end if 
			end if	
%>
			</b>
			</span>
		</td> 
	</tr>
</table>
<br><br>
<input type="hidden" name="txtNumCentriFormazione" value="<%=nNumCenSel%>">
<table border="0" cellspacing="2" cellpadding="1" width="500">
	<tr class="sfondocomm">	
        <!--td  align="left" width="23">&nbsp;</td-->
		<td align="center" width="166"><b>Edizioni Ipotizzate</b></td>
		<td align="center" width="127"><b>Posti Assegnati</b></td>
		<td align="center" width="127"><b>Data Inizio </b></td>
		<td align="center" width="127"><b>Data Fine </b></td>
	</tr>
<!--/table><table width="500" border="0" bordercolor="#000099" cellpadding="2" cellspacing="2"-->
	<% for t = 1 to nEdizioni%>
		<tr class="tblsfondo"> 
			<td width="189" align="middle">
				<span class="tbltext">
				Edizione numero <%=t%>
				</span>
			</td>
			<td width="127" align="middle">
			<p>
			<% 	
			if ((t) = nEdizioni) then
				Response.Write" <span class='tbltext'>" & (clng(sNumPers) - (clng(nPostiSel)*(t-1))) & "</span>"
			else
				Response.Write" <span class='tbltext'>" & clng(nPostiSel) & "</span>"
			end if%>
			</p>	
			</td>
			<td width="127" align="middle">
				<input name="txtSesIni<%=t%>" style="WIDTH: 110px; HEIGHT: 22px" size="15">
			</td>
			<td width="127" align="middle">
				<input name="txtSesFin<%=t%>" style="WIDTH: 110px; HEIGHT: 22px" size="15">
			</td>
		</tr>
	<%next%>
	<%nRighe = nRighe + 1%>
	
</table>
<br>
<br>
<input type="hidden" name="txtNumEdizione" value="<%=nEdizioni%>">
<input type="hidden" name="txtEdizioni">
<input type="hidden" name="txtCentForm" value="<%=appoCenForm%>">
<table border="0" cellspacing="0" cellpadding="0" width="500">
	<tr>
		<!--td align="middle">		<input type="submit" value="Conferma" name="Salva">&nbsp;		<input type="button" value="Indietro" name="torna" onclick="javascript:history.back()">		</td-->
		<td nowrap align="center"><a href="javascript:history.go(-1)" onmouseover="javascript:window.status=' '; return true">
			<img src="<%=Session("progetto")%>/images/indietro.gif" border="0" name="imgPunto2"></a><td>
		</td>
		<td nowrap align="center">
			<input type="image" value="pippo" name="pippo" src="<%=Session("progetto")%>/images/conferma.gif">
		</td>	
	</tr>
</table>
<br><br>
<%end sub%>


<%sub Fad()%>
<table width="500">
	<tr> 
		<td align="left"> 
			<span class="tbltext">
			<b>
			Bando:&nbsp;<%=DesBando%>
			</b>
			</span> 			
		</td> 
	</tr>
	<tr> 
		<td align="left"> 
			<span class="tbltext">
			<b>
			Area Geografica:&nbsp;<%=sGruppo%> 
			
			</b>
			</span>
		</td>
	</tr>
	<tr>
		<td align="left"> 
			<span class="tbltext">
			<b>
			Numero persone da pianificare:&nbsp;			
			</b>
			</span>
			<span class="tbltext">		
			<b>
			<%=sNumPers%>  
			</b>
			</span>
		</td> 
	</tr>
	<tr>
		<td align="left"> 
			<span class="tbltext">
			<b>
			Numero complessivo posti attualmente disponibili:&nbsp;
			</b>
			</span>
			<span class="tbltext">
			<b>
			<%=sPosti%> 			
			</b>
			</span>
		</td> 
	</tr>
</table>
<br><br>
<table width="500">
	<tr> 
		<td align="left"> 
			<span class="tbltext"><b>
<%
		
			nEdizioni = int(clng(sNumPers)/clng(sPosti))
			if nEdizioni < (clng(sNumPers)/clng(sPosti)) then
			'Response.Write "nEdizioni" & nEdizioni & "<br>"		
				nEdizioni = nEdizioni + 1
			end if 
			
			Response.Write "<BR>Le Edizioni previste sono " &_
				nEdizioni
			
			 nResto = cdbl(sNumPers) mod cdbl(sPosti)
			
			'if clng(sPosti) <> clng(sNumPers) then
			if nResto > 0 then
			'Response.Write sPosti & "<br>" & sNumPers & "<br>"
			'Response.Write "nEdizioni" & nEdizioni & "<br>"
				Response.Write " di cui una incompleta"
				sPippo = " n.c."	
			end if 
%>
			</b>
			</span>
		</td> 
	</tr>
</table>
<br><br>
<table border="0" cellspacing="2" cellpadding="1" width="500">
	<tr class="sfondocomm">	
        <!--td  align="left" width="23">&nbsp;</td-->
		<td align="center" width="166"><b>Edizioni Ipotizzate</b></td>
		<td align="center" width="127"><b>Posti Assegnati</b></td>
		<td align="center" width="127"><b>Data Inizio </b></td>
		<td align="center" width="127"><b>Data Fine </b></td>
	</tr>
<!--/table><table width="500" border="0" bordercolor="#000099" cellpadding="2" cellspacing="2"-->
	<%for t = 1 to nEdizioni%>
		<tr class="tblsfondo"> 
			<td width="189" align="middle">
				<span class="tbltext">
				Edizione numero <%=t%>
				</span>
			</td>
			<td width="127" align="middle">
			<p>
			<%	if ((t) = nEdizioni) then
					Response.Write  (clng(sNumPers) - (clng(sPosti)*(t-1))) 
				else
					Response.Write  clng(sPosti)
				end if
			%>
			</p>	
			</td>
			<td width="127" align="middle">
				<input name="txtSesIni<%=t%>" style="WIDTH: 110px; HEIGHT: 22px" size="15">
			</td>
			<td width="127" align="middle">
				<input name="txtSesFin<%=t%>" style="WIDTH: 110px; HEIGHT: 22px" size="15">
			</td>
		</tr>
	<%next%>
	<%nRighe = nRighe + 1%>
	
</table>
<br>
<br>
<input type="hidden" name="txtNumEdizione" value="<%=nEdizioni%>">
<input type="hidden" name="txtEdizioni">
<input type="hidden" name="txtNumPosti" value="<%=sPosti%>">


<table border="0" cellspacing="0" cellpadding="0" width="500">
	<tr>
		<!--td align="middle">		<input type="submit" value="Conferma" name="Salva">&nbsp;		<input type="button" value="Indietro" name="torna" onclick="javascript:history.back()">		</td-->
		<td nowrap align="center"><a href="javascript:history.go(-1)" onmouseover="javascript:window.status=' '; return true">
			<img src="<%=Session("progetto")%>/images/indietro.gif" border="0" name="imgPunto2"></a><td>
		</td>
		<td nowrap align="center">
			<input type="image" value="pippo" name="pippo" src="<%=Session("progetto")%>/images/conferma.gif">
		</td>	
	</tr>
</table>
<%end sub
'*********************************MAIN*************************************************
dim sFlag
dim sCenForma
dim appoCenForm
dim sAule
dim sMaxPosti
dim i,sNumElem
dim nRighe
dim nNumCenSel
dim nPostiSel
dim sNumPers
dim Controlli
dim nResto 
dim DesBando
 
appoCenForm=""
nPostiSel = 0
sNumElem	= Request.Form ("txtElementi")
nRighe		= Request.Form ("txtNumRighe")
sNumPers	= Request.Form ("txtTotGroup")
sModo       = Request.Form ("Modo")
sPosti      = Request.Form ("txtposti")
sBando      = Request.Form ("cmbBando")
sGruppo     = Request.Form ("txtAreaGeo")


if sModo=1 then
Controlli="ControllaDati"
else
Controlli="ControllaDatiFad"
end if
'Response.Write " sModo" &  sModo

%><br>

<form name="frmCfnEdizione" method="post" onsubmit="return <%=Controlli%>()" action="CLA_CfnEdizione2.asp">
<%sSQL = "SELECT DESC_BANDO FROM BANDO WHERE ID_BANDO= " & sBando
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	set rs = CC.Execute(sSQL)
if not  rs.eof then
	DesBando=rs("DESC_BANDO")
end if%>

<input type="hidden" name="cmbBando" value="<%=sBando%>">
<input type="hidden" name="DBando" value="<%=DesBando%>">
<input type="hidden" name="txtAreaGeo" value="<%=sGruppo%>">
<input type="hidden" name="Modo" value="<%=sModo%>">


<% 
if sModo= 1 then
	if nRighe = "" then
		nRighe = 1
	end if
	InPresenza()			
else
	fad()
end if%>
</form>
<br>

<!--#include virtual = "/include/closeconn.asp"-->
<!--#include virtual = "strutt_coda2.asp"-->
