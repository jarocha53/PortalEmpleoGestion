<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include virtual = "/strutt_testa2.asp"-->
<!--#include virtual ="/include/openconn.asp"-->
<!--#include virtual = "util/portallib.asp"-->
<!--#include virtual = "util/dbutil.asp"-->
<!--#include virtual = "include/DecCod.asp"-->
<!--#include Virtual="/include/SysFunction.asp"-->
<!--#include virtual = "include/SelAreaTerrBandi.asp"-->
<!--#include virtual = "include/ControlDateVB.asp"-->

<%
if ValidateService(session("idutente"),"PRD_RICPRESGIOR",cc) <> "true" then 
	response.redirect "/util/error_login.asp"
end if
%>
<script LANGUAGE="JavaScript">
<!--#include virtual = "/Include/ControlString.inc"-->
<!--#include virtual = "/Include/ControlDate.inc"-->
<!--#include Virtual = "/Include/help.inc"-->	

function ControllaDati(TheForm){
	var sDataApp;
	var sDataOggi;	
	
	TheForm.DtRicerca.value = TRIM(TheForm.DtRicerca.value);
	if (TheForm.DtRicerca.value == ""){
		alert("Il campo Data � obbligatorio.");
		TheForm.DtRicerca.focus(); 
		return false;
	}   

	//Data Ricerca corretta
	sDataApp = TheForm.DtRicerca.value;
	if (!ValidateInputDate(sDataApp)){
		TheForm.DtRicerca.focus(); 
		return false;
	}

	//La data ricerca deve essere minore
	// o uguale alla Data odierna
	sDataOggi = TRIM(TheForm.DtOdierna.value);
	if (!ValidateRangeDate(sDataApp,sDataOggi)){
	   alert("La Data riferimento periodo non deve essere maggiore della Data odierna'");
	   TheForm.DtRicerca.focus();
	   return false;
	}
	return (true);	
}

function  Ricarica(){   
	document.frmRicPresGior.action = "PRD_RicPresGior.asp";
	document.frmRicPresGior.submit();				
}
	
function VaiInizio(){
	document.frmRicPresGior.Modo.value = "";
	location.href = "PRD_RicPresGiorOLD.asp";
}	

</script>

	<!--	FINE BLOCCO SCRIPT	-->
	<!--	BLOCCO ASP			-->
<%
sub inizio()
%>
	<table border="0" width="520" cellspacing="0" cellpadding="0" height="81">
		<tr>
			<td width="500" background="<%=Session("Progetto")%>/images/titoli/strumenti2b.gif" height="81" valign="bottom" align="right">
				<table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
					<tr>
						<td width="100%" valign="top" align="right"><b class="tbltext1a">Gestione Classi</span></b></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<br>
	<table cellpadding="0" cellspacing="0" width="500" border="0">
		<tr height="18">
			<td class="sfondomenu" height="18" width="67%">
				<span class="tbltext0"><b>&nbsp;VERIFICA STATO REGISTRI</b></span></td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
			<td valign="middle" align="right" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1"></td>
		</tr>
		<tr width="371" class="SFONDOCOMM">
			<td colspan="3">
				Per visualizzare i dati sullo stato dei registri inserire il campo<b> Bando</b> 
				<br>selezionare la provincia e la data di riferimento.
				<a href="Javascript:Show_Help('/Pgm/help/Gestprogetti/formazione/pres_doc/PRD_RicPresGior')">
				<img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
			</td>
		</tr>
		<tr height="2">
			<td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
		</tr>
	</table>
	<br>
	<form name="frmRicPresGior" method="post" onsubmit="return ControllaDati(this)" action="PRD_RicPresGiorOLD.asp">
	<input type="hidden" name="DtOdierna" value="<%=ConvDateToString(Date())%>">	
<%
end sub
'----------------------------------------------------------------------------------------------------------------------------------------------------------
sub CaricaBandi()

	if sModo = "" then 
	%>
		<input type="hidden" name="Modo" value="1">	
		<table cellpadding="1" cellspacing="0" width="420" border="0">
			<tr>
				<td class="tbltext1" align="left" width="120">
					<b>Bando</b>
				</td>
				<td align="left" width="300" class="tbltext3">
					<%strcombo = session("iduorg") & "||CmbBando|onchange='Javascript:Ricarica()'" 
				      CreateBandi strcombo			
					%>				
				</td>
			</tr> 
		</table>
	<%
	else
	%>
		<input type="hidden" name="IdBando" value="<%=nIdBando%>">		
		<table cellpadding="1" cellspacing="0" width="420" border="0">	
			<tr>
				<td class="tbltext1" align="left" width="120">
					<b>Bando</b>
				</td>
				<td class="tbltext" align="left" width="300" name="txtBando" value="<%=nIdBando%>">
					<b><%=DecBando(nIdBando)%></b>
				</td>
			</tr> 
		</table>
		<br>
	<%
	end if
end sub
'----------------------------------------------------------------------------------------------------------------------------------------------------------
sub CaricaProv()

	if sModo = "1" then
		%>	
		<input type="hidden" name="Modo" value="2">	
		<table cellpadding="1" cellspacing="0" width="420" border="0">	
			<tr align="left">	
				<td class="tbltext1" align="left" width="120">
					<b>Provincia </b>
				</td>
				<%
				sMask = SetProvBando(nIdBando,session("idUorg"))
				%>	
				<td align="left">			 	 
					<select NAME="CmbProv" class="textblack" onchange="Ricarica()">			 
						<option>&nbsp;</option>
						<%
						nProvSedi = len(sMask)
						if nProvSedi > 2 then
							pos_ini = 1 	                
							for i = 1 to nProvSedi/2
								sTarga = mid(sMask,pos_ini,2)
								' DecCodVal(nome_tabella, isa, dt_decor, codice, flag_conn)
								%>
								<option value="<%=sTarga%>"><%=DecCodVal("PROV", 0, "", sTarga,"")%></option>
								<%	                    
								pos_ini = pos_ini + 2
							next	
						else
							if nProvSedi = 2 then
								%>
								<td class="tbltext" align="left" width="300">
									<b><option value="<%=sMask%>"><%=DecCodVal("PROV", 0, "", sMask,"")%></option></b>
								</td>
							<%
							end if
						end if
						%>
					</select>
				</td>
			</tr>
		</table>
		<br>
	<%
	else
		if sModo <> "" then
		%>
			<input type="hidden" name="IdProv" value="<%=sProv%>">		
			<table cellpadding="1" cellspacing="0" width="420" border="0">
				<tr align="left">	
					<td class="tbltext1" align="left" width="120">
						<b>Provincia </b>
					</td>
					<td class="tbltext" align="left" width="300" name="txtProv">
						<b><%=DecCodVal("PROV", 0, "", sProv,"")%></b>
					</td>
				</tr>
			</table>
			<br>
		<%
		end if
	end if
end sub
'----------------------------------------------------------------------------------------------------------------------------------------------------------
sub CaricaClassi(sDtRicerca)

'	sData = "TO_Date('" & sDtRicerca & "','dd/mm/yyyy')"

	sData =convdatetodbs(sDtRicerca)

'				" AND  CR.DT_RUOLODAL <= " & sData &_ 
'				" AND  CR.DT_RUOLOAL  >= " & sData &_ 				
'			    " AND  CP.DT_INISESS  <= " & sData &_ 
'			    " AND  CP.DT_FINSESS  >= " & sData &_

	sSQL	=	"SELECT U.IDUTENTE, CF.DESC_CFORM, A.DESC_AULA," &_ 
						" CP.COD_SESSIONE, U.COGNOME, U.NOME," &_
						" DECODE(RD.DT_PRESENZA, NULL, 'NON COMPILATO', " & sData & ", 'COMPILATO') AS PA " &_ 
				"FROM UTENTE U, CLASSE_RUOLO CR, REG_DOCENTE RD, AULA A, " &_
						"CENTRO_FORMAZIONE CF, CLASSE_PERCORSO CP " &_
				"WHERE RD.IDUTENTE      = U.IDUTENTE(+)" &_
				" AND DT_PRESENZA(+) = " & sData &_
				" AND  CR.ID_CLASSE    = RD.ID_CLASSE(+)" &_
				" AND " & sData & " BETWEEN CR.DT_RUOLODAL AND CR.DT_RUOLOAL" &_
				" AND  CP.ID_CLASSE    = CR.ID_CLASSE(+)" &_
				" AND  CP.COD_SESSIONE = CR.COD_SESSIONE(+)" &_
				" AND " & sData & " BETWEEN CP.DT_INISESS AND CP.DT_FINSESS" &_
				" AND   A.ID_AULA      = CP.ID_AULA" &_
				" AND   A.ID_BANDO     = " & nIdBando &_
				" AND  CF.ID_CFORM     = A.ID_CFORM" &_
				" AND CF.PRV= '" & sProv &"'" &_
				" ORDER BY DESC_CFORM, COGNOME, NOME, PA"
   
	Response.Write sSQL
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	set rsClassi = CC.Execute(sSQL)

	if not rsClassi.eof  then
		%> 
		<br>
		<table width="520" cellspacing="2" cellpadding="1" border="0">
			<tr class="sfondocomm">
			  <td width="120" align="center">Centro Formazione</td>
			  <td width="110" align="center">Aula</td>
			  <td width="30" align="center">S.For.</td>
			  <td width="130" align="center">Nominativo</td>
			  <td width="130" align="center">Compilato/Non Compilato</td>
			</tr>
			<%
			do while not rsClassi.eof
			%>
				<tr class="tblsfondo">
					<td class="tbltext1" width="120" align="left"><%=rsClassi("desc_cform")%></td> 
					<td class="tbltext1" width="110" align="left"><%=rsClassi("desc_aula")%></td>
					<td class="tbltext1" width="30" align="left"><%=rsclassi("cod_sessione")%></td>
					<td class="tbltext1" width="130" align="left"><%=rsClassi("Cognome")%>&nbsp;<%=rsClassi("Nome")%></td>
					<%
					sPresenza = rsClassi("PA")
					if sPresenza = "COMPILATO" then
					%>
						<td class="tbltext1" width="130" align="center"><b><font color="green"><%=sPresenza%></font></b></td>
					<%
					else
					%>
						<td class="tbltext1" width="130" align="center"><font color="#990000"><%=sPresenza%></font></td>
					<%
					end if
					%>
				</tr>
				<%
				rsClassi.MoveNext
			loop
			%>
		</table>
	<%
	else
	   Msgetto("Non esistono classi  nel periodo e per la sessione indicata")
	end if
	rsClassi.Close
end sub
'--------------------------------------------------------------------------------------------------------------------------------------------------------->
Sub Msgetto(Msg)
%>
	<br>
	<table align="center">
		<tr>
			<td class="tbltext3"><b><%=Msg%></b></td>
		</tr>	
	</table>
	<br>
<%
End Sub
'--------------------------------------------------------------------------------------------------------------------------------------------------------->
Sub Fine(sModo)
	if sModo <> "" then
		if sModo = "2" then	
		%>
			<br>
			<table cellpadding="0" cellspacing="0" width="300" border="0">	
				<tr align="center">
					<td nowrap><input type="image" name="Conferma" src="<%=Session("progetto")%>/images/conferma.gif"></td value="Registra">
					<td nowrap><a href="javascript:VaiInizio()" onmouseover="javascript:window.status=' '; return true"><img src="<%=session("Progetto")%>/images/indietro.gif" border="0" name="imgPunto2"></a><td>
			    </tr>
			</table>	
		<%
		else
		%>
			<br>
			<table cellpadding="0" cellspacing="0" width="300" border="0">	
				<tr align="center">
					<td nowrap><a href="javascript:VaiInizio()" onmouseover="javascript:window.status=' '; return true"><img src="<%=session("Progetto")%>/images/indietro.gif" border="0" name="imgPunto2"></a><td>
			    </tr>
			</table>	
		<%
		end if
	end if
	%>	
	</form>
<!--#include virtual = "/include/closeconn.asp"-->
<!--#include virtual = "/strutt_coda2.asp"-->
<%
End Sub
%>	
<!--------------------------------------------------------------------------------------------------------------------------------------------------------->
<!--------------------------------------------------------------------------------------------------------------------------------------------------------->
<!--------------------------------------------------------------------------------------------------------------------------------------------------------->
<!--------------------------------------------------------------------------------------------------------------------------------------------------------->
<!--------------------------------------------------------------------------------------------------------------------------------------------------------->
<%
	dim sModo	
	dim nIdBando
	dim sDescBando
	dim sProv
	dim sDtRicerca
	
	inizio()

	sModo		= Request("Modo")
	
	if sModo = "1" then 
   		nIdBando	= Request("CmbBando")
   	else	
   		nIdBando	= Request("IdBando")
	end if   		
	
	if sModo = "2" then
		sProv		= Request("CmbProv")
	else
   		sProv		= Request("IdProv")
	end if
		
	sDtRicerca	= Request("DtRicerca")	

	CaricaBandi()

	CaricaProv()

	if sModo = "2" then
		%>
		<input type="hidden" name="Modo" value="3">	
		<table cellpadding="1" cellspacing="0" width="420" border="0">	
			<tr align="left">
				<td class="tbltext1" align="left" width="120"><b>Data Riferimento</b>
				</td>
				<td align="left" class="tbltext">
					<input type="TEXT" name="DtRicerca" value="<%=ConvDateToString(date())%>">
				</td>
			</tr>
		</table>
		<br>			
	<%
	end if

	if sModo = "3" then
		%>
		<input type="hidden" name="Modo" value="4">	
		<table cellpadding="1" cellspacing="0" width="420" border="0">	
			<tr align="left">
				<td class="tbltext1" align="left" width="120"><b>Data Riferimento</b>
				</td>
				<td class="tbltext" align="left" width="300" name="txtDtRicerca" value="<%=sDtRicerca%>">
					<b><%=sDtRicerca%></b>
				</td>
			</tr>
		</table>			
		<%
		caricaClassi(sDtRicerca)
	end if
	
	fine(sModo)
	%>
