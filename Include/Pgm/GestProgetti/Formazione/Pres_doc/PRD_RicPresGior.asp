<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include virtual = "/strutt_testa2.asp"-->
<!--#include virtual ="/include/openconn.asp"-->
<!--#include virtual = "util/portallib.asp"-->
<!--#include virtual = "util/dbutil.asp"-->
<!--#include virtual = "include/DecCod.asp"-->
<!--#include Virtual="/include/SysFunction.asp"-->
<!--#include virtual = "include/SelAreaTerrBandi.asp"-->
<!--#include virtual = "include/ControlDateVB.asp"-->

<%
if ValidateService(session("idutente"),"PRD_RICPRESGIOR",cc) <> "true" then 
	response.redirect "/util/error_login.asp"
end if
%>
<script LANGUAGE="JavaScript">
<!--#include virtual = "/Include/ControlString.inc"-->
<!--#include virtual = "/Include/ControlDate.inc"-->
<!--#include Virtual = "/Include/help.inc"-->	

function ControllaDati(TheForm){
	var sDataApp;
	var sDataOggi;	
	
	TheForm.DtRicerca.value = TRIM(TheForm.DtRicerca.value);
	if (TheForm.DtRicerca.value == ""){
		alert("Il campo Data � obbligatorio.");
		TheForm.DtRicerca.focus(); 
		return false;
	}   

	//Data Ricerca corretta
	sDataApp = TheForm.DtRicerca.value;
	if (!ValidateInputDate(sDataApp)){
		TheForm.DtRicerca.focus(); 
		return false;
	}

	//La data ricerca deve essere minore
	// o uguale alla Data odierna
	sDataOggi = TRIM(TheForm.DtOdierna.value);
	if (!ValidateRangeDate(sDataApp,sDataOggi)){
	   alert("La Data riferimento periodo non deve essere maggiore della Data odierna'");
	   TheForm.DtRicerca.focus();
	   return false;
	}
	return (true);	
}

function  Ricarica(){   
	document.frmRicPresGior.action = "PRD_RicPresGior.asp";
	document.frmRicPresGior.submit();				
}
	
function VaiInizio(){
	document.frmRicPresGior.Modo.value = "";
	location.href = "PRD_RicPresGior.asp";
}	

</script>

	<!--	FINE BLOCCO SCRIPT	-->
	<!--	BLOCCO ASP			-->
<%
sub inizio()
%>
	<table border="0" width="520" cellspacing="0" cellpadding="0" height="81">
		<tr>
			<td width="500" background="<%=Session("Progetto")%>/images/titoli/strumenti2b.gif" height="81" valign="bottom" align="right">
				<table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
					<tr>
						<td width="100%" valign="top" align="right"><b class="tbltext1a">Gestione Classi</span></b></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<br>
	<table cellpadding="0" cellspacing="0" width="500" border="0">
		<tr height="18">
			<td class="sfondomenu" height="18" width="67%">
				<span class="tbltext0"><b>&nbsp;VERIFICA STATO REGISTRI</b></span></td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
			<td valign="middle" align="right" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1"></td>
		</tr>
		<tr width="371" class="SFONDOCOMM">
			<td colspan="3">
				Per visualizzare i dati sullo stato dei registri inserire il campo<b> Bando</b> 
				<br>selezionare la provincia e la data di riferimento.
				<a href="Javascript:Show_Help('/Pgm/help/Gestprogetti/formazione/pres_doc/PRD_RicPresGior')">
				<img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
			</td>
		</tr>
		<tr height="2">
			<td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
		</tr>
	</table>
	<br>
	<form name="frmRicPresGior" method="post" onsubmit="return ControllaDati(this)" action="PRD_VisPresGior.asp">
	<input type="hidden" name="DtOdierna" value="<%=ConvDateToString(Date())%>">	
<%
end sub
'----------------------------------------------------------------------------------------------------------------------------------------------------------
sub CaricaBandi()

	if sModo = "" then 
	%>
		<input type="hidden" name="Modo" value="1">	
		<table cellpadding="1" cellspacing="0" width="500" border="0">
			<tr>
				<td class="tbltext1" align="center" width="150">
					<b>Bando:</b>
				</td>
				<td align="left" width="350" class="tbltext3">
					<%strcombo = session("iduorg") & "||CmbBando|onchange='Javascript:Ricarica()'" 
				      CreateBandi strcombo			
					%>				
				</td>
			</tr> 
		</table>
	<%
	else
	%>
		<input type="hidden" name="IdBando" value="<%=nIdBando%>">		
		<table cellpadding="1" cellspacing="0" width="420" border="0">	
			<tr>
				<td class="tbltext1" align="left" width="120">
					<b>Bando:</b>
				</td>
				<td class="tbltext" align="left" width="300" name="txtBando" value="<%=nIdBando%>">
					<b><%=DecBando(nIdBando)%></b>
				</td>
			</tr> 
		</table>
		<br>
	<%
	end if
end sub
'----------------------------------------------------------------------------------------------------------------------------------------------------------
sub CaricaProv()

	if sModo = "1" then
		%>	
		<input type="hidden" name="Modo" value="2">	
		<table cellpadding="1" cellspacing="0" width="420" border="0">	
			<tr align="left">	
				<td class="tbltext1" align="left" width="120">
					<b>Provincia:</b>
				</td>
				<%
				sMask = SetProvBando(nIdBando,session("idUorg"))
				%>	
				<td align="left">			 	 
					<select NAME="CmbProv" class="textblack" onchange="Ricarica()">			 
						<option>&nbsp;</option>
						<%
						nProvSedi = len(sMask)
						if nProvSedi > 2 then
							pos_ini = 1 	                
							for i = 1 to nProvSedi/2
								sTarga = mid(sMask,pos_ini,2)
								' DecCodVal(nome_tabella, isa, dt_decor, codice, flag_conn)
								%>
								<option value="<%=sTarga%>"><%=DecCodVal("PROV", 0, "", sTarga,"")%></option>
								<%	                    
								pos_ini = pos_ini + 2
							next	
						else
							if nProvSedi = 2 then
								%>
							<!--	<td class="tbltext" align="left" width="300">									<b>--><option value="<%=sMask%>"><%=DecCodVal("PROV", 0, "", sMask,"")%></option>
								<!--	</b>								</td> -->
							<%
							end if
						end if
						%>
					</select>
				</td>
			</tr>
		</table>
		<br>
	<%
	else
		if sModo <> "" then
		%>
			<input type="hidden" name="IdProv" value="<%=sProv%>">		
			<table cellpadding="1" cellspacing="0" width="420" border="0">
				<tr align="left">	
					<td class="tbltext1" align="left" width="120">
						<b>Provincia:</b>
					</td>
					<td class="tbltext" align="left" width="300" name="txtProv">
						<b><%=DecCodVal("PROV", 0, "", sProv,"")%></b>
					</td>
				</tr>
			</table>
			<br>
		<%
		end if
	end if
end sub
'--------------------------------------------------------------------------------------------------------------------------------------------------------->
Sub Fine(sModo)
	if sModo <> "" then
		if sModo = "2" then	
		%>
			<br>
			<table cellpadding="0" cellspacing="0" width="300" border="0">	
				<tr align="center">
					<td nowrap><input type="image" name="Conferma" src="<%=Session("progetto")%>/images/conferma.gif"></td value="Registra">
					<td nowrap><a href="javascript:VaiInizio()" onmouseover="javascript:window.status=' '; return true"><img src="<%=session("Progetto")%>/images/indietro.gif" border="0" name="imgPunto2"></a><td>
			    </tr>
			</table>	
		<%
		else
		%>
			<br>
			<table cellpadding="0" cellspacing="0" width="300" border="0">	
				<tr align="center">
					<td nowrap><a href="javascript:VaiInizio()" onmouseover="javascript:window.status=' '; return true"><img src="<%=session("Progetto")%>/images/indietro.gif" border="0" name="imgPunto2"></a><td>
			    </tr>
			</table>	
		<%
		end if
	end if
	%>	
	</form>
<!--#include virtual = "/include/closeconn.asp"-->
<!--#include virtual = "/strutt_coda2.asp"-->
<%
End Sub
%>	
<!--------------------------------------------------------------------------------------------------------------------------------------------------------->
<!--------------------------------------------------------------------------------------------------------------------------------------------------------->
<!--------------------------------------------------------------------------------------------------------------------------------------------------------->
<!--------------------------------------------------------------------------------------------------------------------------------------------------------->
<!--------------------------------------------------------------------------------------------------------------------------------------------------------->
<%
	dim sModo	
	dim nIdBando
	dim sDescBando
	dim sProv
	dim sDtRicerca
	
	inizio()

	sModo		= Request("Modo")
	
	if sModo = "1" then 
   		nIdBando	= Request("CmbBando")
   	else	
   		nIdBando	= Request("IdBando")
	end if   		
	
	if sModo = "2" then
		sProv		= Request("CmbProv")
	else
   		sProv		= Request("IdProv")
	end if
		
	sDtRicerca	= Request("DtRicerca")	

	CaricaBandi()

	CaricaProv()

	if sModo = "2" then
		%>
		<input type="hidden" name="Modo" value="3">	
		<table cellpadding="1" cellspacing="0" width="420" border="0">	
			<tr align="left">
				<td class="tbltext1" align="left" width="120"><b>Data Riferimento:</b>
				</td>
				<td align="left" class="tbltext">
					<input type="TEXT" name="DtRicerca" value="<%=ConvDateToString(date())%>">
				</td>
			</tr>
		</table>
		<br>			
	<%
	end if

	if sModo = "3" then
		%>
		<input type="hidden" name="Modo" value="4">	
		<table cellpadding="1" cellspacing="0" width="420" border="0">	
			<tr align="left">
				<td class="tbltext1" align="left" width="120"><b>Data Riferimento</b>
				</td>
				<td class="tbltext" align="left" width="300" name="txtDtRicerca" value="<%=sDtRicerca%>">
					<b><%=sDtRicerca%></b>
				</td>
			</tr>
		</table>			
		<%
		'caricaClassi(sDtRicerca)
	end if
	
	fine(sModo)
	%>
