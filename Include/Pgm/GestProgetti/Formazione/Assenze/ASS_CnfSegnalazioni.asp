<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
 <%'Option Explicit
   Response.ExpiresAbsolute = Now() - 1 
   Response.AddHeader "pragma","no-cache"
   Response.AddHeader "cache-control","private"
   Response.CacheControl = "no-cache"
%>
<!--#include virtual = "/strutt_testa2.asp"-->
<!--#include virtual ="/include/openconn.asp"-->
<!--#include virtual="/util/portallib.asp"-->
<!--#include virtual="/util/dbUtil.asp"-->
<!--#include virtual = "/include/SysFunction.asp"-->
<!--#include virtual = "/include/ControlDateVB.asp"-->
<%
'if ValidateService(session("idutente"),"ASS_VISCLASSI", CC) <> "true" then 
'	response.redirect "/util/error_login.asp"
'end if
%>
<!-- ************** Javascript Inizio ************ -->

<script LANGUAGE="javascript">
<!--#include Virtual = "/Include/help.inc"-->

</script>

<!-- ************** Javascript Fine ************ -->
<%
Sub Msgetto(Msg)
%>
	<br>
	<table align="center">
		<tr>
			<td class="tbltext3"><b><%=Msg%></b></td>
		</tr>	
	</table>
	<br>
<%
End Sub
'-------------------------------------------------------------------------------------------------------------------------------
Sub Inizio()
%>
	<table border="0" width="520" cellspacing="0" cellpadding="0" height="81">
		<tr>
			<td width="500" background="<%=Session("Progetto")%>/images/titoli/strumenti2b.gif" height="81" valign="bottom" align="right">
				<table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
					<tr>
						<td width="100%" valign="top" align="right"><b class="tbltext1a">Gestione Progetti</span></b></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<br>
	<table border="0" width="500" CELLPADDING="0" cellspacing="0">
		<tr class="tblcomm">
			<td align="left" class="sfondomenu">
				<span class="tbltext0"><b>&nbsp;GESTIONE SEGNALAZIONI MALFUNZIONAMENTI</b></span>
			</td>
			<td width="25" valign="bottom" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">
			<img border="0" src="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
			<td width="140" valign="bottom" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">&nbsp;</td>
		</tr>
	</table>

	<table border="0" width="500" CELLPADDING="0" cellspacing="0">
	    <tr>
			<td class="sfondocomm" align="left" class="tbltext1">
				Conferma segnalazione malfunzionamento.
			</td>
	    </tr>
		<tr height="2">
			<td class="sfondocomm" width="100%" colspan="2" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
		</tr>
	</table>	
<%
End Sub
'-------------------------------------------------------------------------------------------------------------------------------
sub Inserimento()

    sSQL =	"INSERT INTO SEGNALAZIONI " &_
			"(IDUTENTE, ID_CLASSE, COD_SESSIONE, DT_SEGNAL," &_
			" TIPO_SEGNAL, DESCR_SEGNAL, DT_TMST) " &_
			"VALUES ( " & sPersona & "," &_
						 sIdClasse & "," &_
					"'" & sCodSessione & "'," &_
						sDtSegnal & "," &_
					"'" & sTipoSegnal & "'," &_
					"'" & sDescSegnal & "',sysdate)"
	
	'Response.Write sSql
	Errore = Esegui("ID_SEGNALAZIONI", "SEGNALAZIONI", Session("idutente"), "INS", sSQL, 1,"")
		
	if Errore <> "0" then
	   Inizio()
	   Msgetto("Errore nell'inserimento della Segnalazione:" & Errore)
		%>
		<br>				
		<table border="0" cellpadding="0" cellspacing="1" width="500">
			<tr>
				<td align="middle" colspan="2" width="60%"><b>
					<a HREF="javascript:history.go(-2)"><img SRC="<%=Session("Progetto")%>/images/indietro.gif" border="0">
				</td>
			</tr>
		</table>
		<!--#include Virtual="/strutt_Coda2.asp"-->
	<%	   
	else
	%>
		<script>
			alert("Inserimento correttamente effettuato");
			history.go(-2)
		</script>
	<%		
	end if   
	
End Sub
'-------------------------------------------------------------------------------------------------------------------------------
sub Modifica()
    
    sSQL =	"UPDATE SEGNALAZIONI SET" &_
			" TIPO_SEGNAL  = '" & sTipoSegnal & "'," &_
			" DESCR_SEGNAL = '" & sDescSegnal & "'," &_
			" DT_TMST      = sysdate " &_
			" WHERE ID_SEGNALAZIONI = " & sIdSegnalazione

	Errore = Esegui("ID_SEGNALAZIONI", "SEGNALAZIONI", Session("idutente"), "MOD", sSQL, 1, DtTmstOld)
		
	if Errore <> "0" then
	   Inizio()
	   Msgetto("Errore nell'aggiornamento della Segnalazione:" & Errore)
		%>
		<br>				
		<table border="0" cellpadding="0" cellspacing="1" width="500">
			<tr>
				<td align="middle" colspan="2" width="60%"><b>
					<a HREF="javascript:history.go(-2)"><img SRC="<%=Session("Progetto")%>/images/indietro.gif" border="0">
				</td>
			</tr>
		</table>
		<!--#include Virtual="/strutt_Coda2.asp"-->
	<%	   
	else
	%>
		<script>
			alert("Modifica correttamente effettuata");
			history.go(-2)
		</script>
	<%		
	end if   

End Sub
'-------------------------------------------------------------------------------------------------------------------------------
sub Cancellazione()
     
    sSQL =	"DELETE FROM SEGNALAZIONI" &_
			" WHERE ID_SEGNALAZIONI = " & sIdSegnalazione

	'Response.Write sql
	
	Errore = Esegui("ID_SEGNALAZIONI", "SEGNALAZIONI", Session("idutente"), "CAN", sSQL, 1, DtTmstOld)

	if Errore <> "0" then
	   Inizio()
	   Msgetto("Errore nella cancellazione della Segnalazione:" & Errore)
		%>
		<br>				
		<table border="0" cellpadding="0" cellspacing="1" width="500">
			<tr>
				<td align="middle" colspan="2" width="60%"><b>
					<a HREF="javascript:history.go(-2)"><img SRC="<%=Session("Progetto")%>/images/indietro.gif" border="0">
				</td>
			</tr>
		</table>
		<!--#include Virtual="/strutt_Coda2.asp"-->
	<%	   
	else
	%>
		<script>
			alert("Cancellazione correttamente effettuata");
			history.go(-2)
		</script>
	<%		
	end if   
	
End Sub


'---------------------------------- MAIN -------------------------------------

dim SOperazione, Errore

dim sIdSeganalazione
dim sPersona
dim sIdClasse
dim sCodSessione
dim sTipoSegnal
dim sDescSegnal
dim sDtSeganl
dim sDtTmst
dim sSQL

sOperazione		= Request("Operazione")
sIdSegnalazione = Request("IdSegnalazione")
sIdClasse		= Request("IdClasse")
sCodSessione	= Request("CodSess")
sTipoSegnal		= Request("TipoSegnal")
sDescSegnal		= UCase(Request("txtDescrizione"))
DtTmstOld		= Request("DtTmst")

sDescSegnal=replace(sDescSegnal,"'","''")

'Response.Write DtTmstOld

sDtSegnal	= ConvDateToDb(Date())
sPersona	= session("idutente")

select case sOperazione
	case "Ins" 
	Inserimento()

	case "Mod"
	Modifica()

	case "Can" 
	Cancellazione()

end select
%>
<!--#include virtual = "/include/closeconn.asp"-->


