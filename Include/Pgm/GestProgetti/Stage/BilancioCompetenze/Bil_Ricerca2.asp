<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #Include Virtual="/strutt_testa2.asp" -->
<!--#include virtual = "/Include/OpenConn.asp"-->
<!--#include virtual = "Include/HTMLEncode.asp"-->

<%
if ValidateService(session("idutente"),"STA_BILCOMP",cc) <> "true" then 
	response.redirect "/util/error_login.asp"
end if
%>

<script LANGUAGE="JAVASCRIPT">
<!-- #Include Virtual=/Include/ControlString.inc -->
<!-- #Include Virtual="/Include/ControlNum.inc" -->

function Verifica(Obj) {
	if (document.RicercaElementi.CriterioEle.value == "") {
		alert("Per eseguire la ricerca selezionare uno dei raggruppamenti : Capacita, Conoscenze o Comportamenti");
		return false;
	}
		
	if (document.RicercaElementi.CmbAreaProf.value == "") {
		if ((document.RicercaElementi.CriterioEle.value == 'Conoscenze') && (document.RicercaElementi.CmbConosco.value == "")) {
			alert("Selezionare Area Professionale e/o Area delle Conoscenze."); 
			return false;
		}
		if ((document.RicercaElementi.CriterioEle.value == 'Capacita') && (document.RicercaElementi.CmbCapacita.value == "")) {
			alert("Selezionare Area Professionale e/o Area delle Capacit�."); 
			return false;
		}
		if ((document.RicercaElementi.CriterioEle.value == 'Comportamenti') && (document.RicercaElementi.CmbComportamenti.value == "")) {
			alert("Selezionare Area Professionale e/o Area dei Comportamenti."); 
			return false;
		}
	}
	document.RicercaElementi.CriterioEle.value = "";
}

function Reload() {
	document.RicercaElementi.action = "Bil_Ricerca2.asp";
	document.RicercaElementi.onsubmit = "";
	document.RicercaElementi.submit();
}
</script>

<%
Dim sCriterio
Dim nArea, nRichStage

'Preleva l'Id Stage:
nRichStage = Request.Form("richstage")
	'Lo mantiene al reload della pagina:
	if nRichStage = "" then 
		nRichStage = request("IdRich")
	end if

nArea = request("CmbAreaProf")
sCriterio = request("rCriterio")
%>

<!--FORM DI RICERCA-->
<table border="0" width="520" height="81" cellspacing="0" cellpadding="0">
	<tr>
		<td width="500" height="81" background="<%=Session("Progetto")%>/images/titoli/strumenti2b.gif" valign="bottom" align="right">
			<table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
				<tr>
					<td width="100%" valign="top" align="right">
						<b class="tbltext1a">Bilancio Competenze</b>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>

<!--#INCLUDE Virtual="/pgm/Bilanciocompetenze/Utils.asp" -->

<table cellpadding="0" cellspacing="0" width="500" border="0">
	<tr height="18">
		<td class="sfondomenu" height="18" width="60%">
			<span class="tbltext0">
				<b>RICERCA DIZIONARIO COMPETENZE</b>
			</span>
		</td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">
		</td>
		<td valign="middle" align="right" width="35%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1">(*) campi obbligatori
		</td>
	</tr>
	<tr width="371" class="SFONDOCOMM">
		<td colspan="3">
			Questa pagina consente l'accesso alle funzioni di aggiornamento del Bilancio delle Competenze.<br>
			Per effettuare la ricerca � necessario selezionare uno dei raggruppamenti <b>Conoscenze</b>, <b>Capacit�</b>, <b>Comportamenti</b> 
			cui abbinare la selezione di un'Area Professionale e/o di un'Area specifica del raggruppamento scelto.
			<a href="Javascript:Show_Help('/Pgm/help/GestProgetti/Stage/BilancioCompetenze/Bil_Ricerca2')" name="prov3" onmouseover="javascript:window.status='' ; return true">
				<img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0">
			</a>
		</td>
	</tr>
	<tr height="2">
		<td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif">
		</td>
	</tr>
</table>

<form name="TornaIndietro" method="post" action="STA_VisBilComp.asp">
	<input type="hidden" name="txtIdRichiesta" value="<%=nRichStage%>">
</form>
	
<form name="RicercaElementi" Action="/pgm/GestProgetti/Stage/BilancioCompetenze/Dettagli.asp" Method="POST" OnSubmit="return Verifica()">
	<input type="hidden" name="back" value="<%=Request.ServerVariables("URL")%>">
	<input type="hidden" name="IdPers" value="<%=Request("IdPers")%>">
	<input type="hidden" name="CriterioEle" value="<%=sCriterio%>">
	<input type="hidden" name="VISMENU" value="NO">
	<input type="hidden" name="IdRich" value="<%=nRichStage%>">

	<body>
	<table border="0" CELLPADDING="1" CELLSPACING="2" width="500">
		<tr>
			<td colspan="2">&nbsp;</td>
	    </tr>
		<tr>
			<td class="tbltext1" width="150" align="left">
				<b>Area Professionale</b>
			</td>
			<td align="left">
				<%
				Sql= "SELECT ID_AREAPROF, DENOMINAZIONE FROM AREE_PROFESSIONALI " &_
					 "ORDER BY DENOMINAZIONE "

'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
				Set Rs = CC.Execute (Sql)
					Response.Write "<SELECT class='textblack' name='CmbAreaProf'>"
					Response.Write "<option value=''></option>"
					If Not Rs.eof then
						DO
							Response.Write "<option value=" & Rs("ID_AREAPROF")
							If nArea <> "" then
								if cint(nArea) = cint(Rs("ID_AREAPROF")) then 
									Response.Write " selected"
								end if
							End if
							Response.Write ">" & Rs("DENOMINAZIONE") & "</option>"
						Rs.MOVENEXT
						LOOP UNTIL Rs.EOF
					End If
				Rs.close
				Set Rs = Nothing
				Response.Write "</SELECT>"
				%>
			</td>
		</tr>
		<tr>
		    <td colspan="2">&nbsp;</td>
	    </tr>
		<tr>
		    <td height="2" align="left" colspan="2" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
	    </tr>
    	<tr>
		    <td colspan="2">&nbsp;</td>
	    </tr>
	</table>
				
    <table border="0" CELLPADDING="1" CELLSPACING="1" width="500">
		<tr>
			<td width="166">
				<table border="0" width="100%" CELLPADDING="1" CELLSPACING="1">
					<tr>
						<td class="textblacka" align="right">
							<input type="radio" id="rCriterio" name="rCriterio" value="Conoscenze" <%If sCriterio="Conoscenze" then Response.Write "checked"%> onClick="Reload();">
						</td>
						<td class="tbltext1" align="left">
							<b>Conoscenze*</b>
						</td>
					</tr>
				</table>
			</td>
			<td width="166">
				<table border="0" width="100%" CELLPADDING="1" CELLSPACING="1">
					<tr>
						<td class="textblacka" align="right">
							<input type="radio" id="rCriterio" name="rCriterio" value="Capacita" <%If sCriterio="Capacita" then Response.Write "checked"%> onClick="Reload();">
						</td>
						<td class="tbltext1" align="left">
							<b>Capacit�*</b>
						</td>
					</tr>
				</table>
			</td>
			<td width="166">
				<table border="0" width="100%" CELLPADDING="1" CELLSPACING="1">
					<tr>
						<td class="textblacka" align="right">
							<input type="radio" id="rCriterio" name="rCriterio" value="Comportamenti" <%If sCriterio="Comportamenti" then Response.Write "checked"%> onClick="Reload();">
						</td>
						<td class="tbltext1" align="left">
							<b>Comportamenti*</b>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

	<table border="0" CELLPADDING="1" CELLSPACING="1" width="500">
<%
	SELECT CASE sCriterio
		CASE "Conoscenze" %>
			<tr>
				<td colspan="2">&nbsp;</td>
			</tr>
			<tr>
				<td class="tbltext1" align="left" width="25%">
					<b>Seleziona l'Area Conoscenze</b>
				</td>
				<td align="left">
<%
					Sql= "SELECT ID_AREACONOSCENZA, DENOMINAZIONE FROM AREA_CONOSCENZA " &_
						 "ORDER BY DENOMINAZIONE "
'PL-SQL * T-SQL  
SQL %> = TransformPLSQLToTSQL (SQL %>) 
					Set Rs = CC.Execute (Sql) %>
					<select class="textblack" name="CmbConosco">
						<option value></option>
<%
						If Not Rs.eof then
							DO %>
								<option value="<%=Rs("ID_AREACONOSCENZA")%>"><%=Rs("DENOMINAZIONE")%></option>
<%			
							Rs.MOVENEXT
							LOOP UNTIL Rs.EOF
						End If
					Rs.close
					Set Rs = Nothing %>
					</select>
				</td>
			</tr>
<%		
		CASE "Capacita" %>
			<tr>
			    <td colspan="2">&nbsp;</td>
			</tr>
			<tr>
				<td class="tbltext1" align="left" width="25%">
					<b>Seleziona l'Area Capacit�</b>
				</td>
				<td align="left">
<%
					Sql= "SELECT ID_AREACAPACITA, DENOMINAZIONE FROM AREA_CAPACITA " &_
						 "ORDER BY DENOMINAZIONE "
'PL-SQL * T-SQL  
SQL %> = TransformPLSQLToTSQL (SQL %>) 
					Set Rs = CC.Execute (Sql) %>
					<select class="textblack" name="CmbCapacita">
						<option value></option>
<%
						If Not Rs.eof then
							DO %>
								<option value="<%=Rs("ID_AREACAPACITA")%>"><%=Rs("DENOMINAZIONE")%></option>
<%
							Rs.MOVENEXT
							LOOP UNTIL Rs.EOF
						End If
					Rs.close
					Set Rs = Nothing %>
					</select>
				</td>
			</tr>
<%
		CASE "Comportamenti" %>
			<tr>
			    <td colspan="2">&nbsp;</td>
			</tr>
			<tr>
				<td class="tbltext1" align="left" width="25%">
					<b>Seleziona l'Area Comportamenti</b>
				</td>
				<td align="left">
					<%
					Sql= "SELECT ID_AREACOMPORTAMENTO, DENOMINAZIONE FROM AREA_COMPORTAMENTI " &_
						 "ORDER BY DENOMINAZIONE "
'PL-SQL * T-SQL  
SQL %> = TransformPLSQLToTSQL (SQL %>) 
					Set Rs = CC.Execute (Sql) %>
					<select class="textblack" name="CmbComportamenti">
						<option value></option>
<%						
						If Not Rs.eof then
							DO %>
								<option value="<%=Rs("ID_AREACOMPORTAMENTO")%>"><%=Rs("DENOMINAZIONE")%></option>
<%								
							Rs.MOVENEXT
							LOOP UNTIL Rs.EOF
						End If
					Rs.close
					Set Rs = Nothing %>
					</select>
				</td>
			</tr>
<%
	END SELECT
%>	
	<!--pulsante di ricerca -->
		<tr>
		    <td colspan="2">&nbsp;</td>
	    </tr>
	    <tr>
		    <td colspan="2">&nbsp;</td>
	    </tr>
		<tr>
			<td align="center" colspan="2">
				<table border="0" width="100%" CELLPADDING="1" CELLSPACING="1">
					<tr>
						<td align="center" width="50%">
							<a href="javascript:document.TornaIndietro.submit();">
								<img src="<%=Session("progetto")%>/images/indietro.gif" border="0" alt="Torna alla pagina precedente">
							</a>
						</td>
						<td align="center" width="50%">
							<input type="image" name="Cerca" value="Ricerca" src="<%=Session("Progetto")%>/images/lente.gif" border="0" alt="Effettua la ricerca dei criteri selezionati">		
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
	</table>				
</form>

