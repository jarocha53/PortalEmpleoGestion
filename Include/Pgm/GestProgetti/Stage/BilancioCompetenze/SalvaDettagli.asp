<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include virtual = "/include/OpenConn.asp"-->
<!--#include virtual = "/include/SysFunction.asp"-->
<!--#include virtual="/util/dbUtil.asp"-->
<!--#include virtual = "/include/ControlDateVB.asp"-->
<!-- #INCLUDE Virtual="/pgm/Bilanciocompetenze/Utils.asp" -->

<%
Dim OraDate, nIdStage, chiave
Dim dTmst
Dim sCancellabile

nIdStage = Request.Form("IdRichStage")

OraDate = ConvDateToDB(Now())

chiave=	IDP
Errore="0"

CC.BeginTrans

'##### CAPACITA'
IF UCASE(request("hdnTipo")) = "CAPACITA" THEN
	for x = Int(request("Ini")) to Int(request("Limit"))
		'''Verifica esistenza capacit�
		SqlVer = ""
		SqlVer ="Select ID_CAPACITA, DT_TMST FROM PERS_CAPAC" &_
				" WHERE" &_
				" PERS_CAPAC.ID_PERSONA = " & IDP & " AND" &_
				" PERS_CAPAC.ID_CAPACITA = " & request("ID" & x)
'PL-SQL * T-SQL  
SQLVER = TransformPLSQLToTSQL (SQLVER) 
		Set rsVer = CC.Execute(SqlVer)
			if not rsVer.eof then
				dTmst = rsVer("DT_TMST")
				sCancellabile = "SI"
			else
				dTmst=""
				sCancellabile = "NO"
			end if 
		rsVer.close()
		Set rsVer=nothing
		'''
		
		LstCampi = "ID_PERSONA,GRADO_CAPACITA,ID_CAPACITA,DT_TMST,IND_STATUS"
		
		LstVal = ""
		LstVal = LstVal & IDP & "|"
		LstVal = LstVal & request("Grado" & x) & "|"
		LstVal = LstVal & request("ID" & x) & "|"
		LstVal = LstVal & OraDate 
		LstVal = LstVal & "|'0'"
				
		Sql = ""
		'''if request("exist" & x) = "True" then
		if request("exist" & x) = "Vero" OR request("exist" & x) = "True" then 
			Sql = QryUpd("PERS_CAPAC",LstCampi,LstVal)
			Sql = Sql & " WHERE "
			Sql = Sql & "PERS_CAPAC.ID_PERSONA = " & IDP & " AND "
			Sql = Sql & "PERS_CAPAC.ID_CAPACITA = " & request("ID" & x)
			sOper = "MOD"
		else
			Sql = QryIns("PERS_CAPAC",LstCampi,LstVal)
			sOper = "INS"
		end if
		
		if request("Grado" & x) <> "" then
			Errore=EseguiNoCTrace(UCASE(mid(session("progetto"),2)),chiave,"PERS_CAPAC",session("idutente"),"","",sOper,Sql,0,dTmst,CC)
		else
			if sCancellabile = "SI" then
				Sql = ""
				Sql = Sql & "DELETE PERS_CAPAC "
				Sql = Sql & " WHERE "
				Sql = Sql & " PERS_CAPAC.ID_PERSONA = " & IDP & " AND "
				Sql = Sql & " PERS_CAPAC.ID_CAPACITA = " & request("ID" & x)

				Errore=EseguiNoCTrace(UCASE(mid(session("progetto"),2)),chiave,"PERS_CAPAC",session("idutente"),"","","DEL",Sql,0,dTmst,CC)
			end if
		end if
		
		if Errore <> "0" then
			exit for
		end if
	next
END IF
'##### END CAPACITA'


'##### COMPORTAMENTI
IF UCASE(request("hdnTipo")) = "COMPORTAMENTI" THEN
	for x = Int(request("Ini")) to Int(request("Limit"))
		'''Verifica esistenza comportamenti
		SqlVer = ""
		SqlVer ="Select ID_COMPORTAMENTO, DT_TMST FROM PERS_COMPOR" &_
				" WHERE" &_
				" PERS_COMPOR.ID_PERSONA = " & IDP & " AND" &_
				" PERS_COMPOR.ID_COMPORTAMENTO = " & request("ID" & x)
'PL-SQL * T-SQL  
SQLVER = TransformPLSQLToTSQL (SQLVER) 
		Set rsVer = CC.Execute(SqlVer)
			if not rsVer.eof then
				dTmst = rsVer("DT_TMST")
				sCancellabile = "SI"
			else
				dTmst=""
				sCancellabile = "NO"
			end if 
		rsVer.close()
		Set rsVer=nothing
		'''
		
		LstCampi = "ID_PERSONA,GRADO_PERS_COMPOR,ID_COMPORTAMENTO,DT_TMST,IND_STATUS"
		
		LstVal = ""
		LstVal = LstVal & IDP & "|"
		LstVal = LstVal & request("Grado" & x) & "|"
		LstVal = LstVal & request("ID" & x) & "|"
		LstVal = LstVal & OraDate 
		LstVal = LstVal & "|'0'"
		
		Sql = ""
		'''if request("exist" & x) = "True" then
		if request("exist" & x) = "Vero" OR request("exist" & x) = "True" then
			Sql = QryUpd("PERS_COMPOR",LstCampi,LstVal)
			Sql = Sql & " WHERE "
			Sql = Sql & " PERS_COMPOR.ID_PERSONA = " & IDP & " AND "
			Sql = Sql & " PERS_COMPOR.ID_COMPORTAMENTO = " & request("ID" & x)
			sOper = "MOD"
		else
			Sql = QryIns("PERS_COMPOR",LstCampi,LstVal)
			sOper = "INS"
		end if
	
		if request("Grado" & x) <> "" then
			Errore=EseguiNoCTrace(UCASE(mid(session("progetto"),2)),chiave,"PERS_COMPOR",session("idutente"),"","",sOper,Sql,0,dTmst,CC)
		else
			if sCancellabile = "SI" then
				Sql = ""
				Sql = Sql & "DELETE PERS_COMPOR "
				Sql = Sql & " WHERE "
				Sql = Sql & " PERS_COMPOR.ID_PERSONA = " & IDP & " AND "
				Sql = Sql & " PERS_COMPOR.ID_COMPORTAMENTO = " & request("ID" & x)
				
				Errore=EseguiNoCTrace(UCASE(mid(session("progetto"),2)),chiave,"PERS_COMPOR",session("idutente"),"","","DEL",Sql,0,dTmst,CC)
			end if
		end if
		
		if Errore <> "0" then
			exit for
		end if
	next
END IF
'##### END COMPORTAMENTI


'##### CONOSCENZE
IF UCASE(request("hdnTipo")) = "CONOSCENZE" THEN
	for x = Int(request("Ini")) to Int(request("Limit"))
		'''Verifica esistenza comportamenti
		SqlVer = ""
		SqlVer ="Select ID_CONOSCENZA, DT_TMST FROM PERS_CONOSC" &_
				" WHERE" &_
				" PERS_CONOSC.ID_PERSONA = " & IDP & " AND" &_
				" PERS_CONOSC.ID_CONOSCENZA = " & request("ID" & x)
'PL-SQL * T-SQL  
SQLVER = TransformPLSQLToTSQL (SQLVER) 
		Set rsVer = CC.Execute(SqlVer)
			if not rsVer.eof then
				dTmst = rsVer("DT_TMST")
				sCancellabile = "SI"
			else
				dTmst=""
				sCancellabile = "NO"
			end if 
		rsVer.close()
		Set rsVer=nothing
		'''
		
		LstCampi = "ID_PERSONA, COD_GRADO_CONOSC, DT_TMST, DT_DICH_CONOSC, ID_CONOSCENZA, IND_STATUS"
		
		LstVal = ""
		LstVal = LstVal & IDP & "|"
		LstVal = LstVal & request("Grado" & x) & "|"
		LstVal = LstVal & OraDate & "|"
		LstVal = LstVal & OraDate & "|"
		LstVal = LstVal & request("ID" & x) & "|'0'"

		Sql = ""
		'''if request("exist" & x) = "True" then
		if request("exist" & x) = "Vero" OR request("exist" & x) = "True" then
			Sql = QryUpd("PERS_CONOSC",LstCampi,LstVal)
			Sql = Sql & " WHERE "
			Sql = Sql & "PERS_CONOSC.ID_PERSONA = " & IDP & " AND "
			Sql = Sql & "PERS_CONOSC.ID_CONOSCENZA = " & request("ID" & x)
			sOper = "MOD"
		else
			Sql = QryIns("PERS_CONOSC",LstCampi,LstVal)
			sOper = "INS"
		end if

		if request("Grado" & x) <> "" then
			Errore=EseguiNoCTrace(UCASE(mid(session("progetto"),2)),chiave,"PERS_CONOSC",session("idutente"),"","",sOper,Sql,0,dTmst,CC)
		else
			if sCancellabile = "SI" then
				Sql = ""
				Sql = Sql & "DELETE PERS_CONOSC "
				Sql = Sql & " WHERE "
				Sql = Sql & "PERS_CONOSC.ID_PERSONA = " & IDP & " AND "
				Sql = Sql & "PERS_CONOSC.ID_CONOSCENZA = " & request("ID" & x)
			
			Errore=EseguiNoCTrace(UCASE(mid(session("progetto"),2)),chiave,"PERS_CONOSC",session("idutente"),"","","DEL",Sql,0,dTmst,CC)
			end if		
		end if
		
		if Errore <> "0" then
			exit for
		end if
	next
END IF
'##### END CONOSCENZE

URL = request("back")
%>

<form name="Move" action="<%=request("back")%>" method="post">
	<input type="hidden" name="Limit" Value="<%=Request("Limit")%>">
	<input type="hidden" name="Ini" Value="<%=Request("Ini")%>">
	<input type="hidden" name="Back" value="<%=Request("Back")%>">
	<input type="hidden" name="Tipo" value="<%=request("hdnTipo")%>"> 
	<input type="hidden" name="CmbConosco" value="<%=request("AREA_CONOSCENZA")%>"> 
	<input type="hidden" name="CmbComportamenti" value="<%=request("AREA_COMPORTAMENTI")%>"> 
	<input type="hidden" name="CmbCapacita" value="<%=request("AREA_CAPACITA")%>"> 
	<input type="hidden" name="CmbAreaProf" value="<%=request("AREE_PROFESSIONALI")%>">
	<input type="hidden" name="IdPers" value="<%=sIdPers%>">
	<input type="hidden" name="VISMENU" value="NO">
	<input type="hidden" name="richstage" value="<%=nIdStage%>">
	<input type="hidden" name="IdRich" value="<%=nIdStage%>">
</form>

<%

if Errore = "0" then
	CC.CommitTrans %>	
	<script language="javascript">
		Move.action = "<%=request("back")%>"	
		<%
			if request("Pulsante") = "Indietro" then
			valLimit = Request("Ini")-10
			%>
			Move.action = "<%=request("DETTAGLI")%>"
			Move.Limit.value = <%=valLimit%>
			<%
			end if
			
			if request("Pulsante") = "Avanti" then
			%>
			Move.action = "<%=request("DETTAGLI")%>"
			<%
			end if
			
			if request("Pulsante") = "Salva" then
			%>
			alert("Operazione correttamente eseguita")
			Move.action = "<%=Request("Back")%>"
			<%
			end if 
		%>
		Move.submit()	
	</script>
<%
else 
	CC.RollbackTrans %>
	<!--#include virtual = "/include/CloseConn.asp"-->
	
	<!--#include Virtual="/strutt_testa2.asp"-->
	<table border="0" width="520" height="81" cellspacing="0" cellpadding="0">
		<tr>
			<td width="500" height="81" background="<%=Session("Progetto")%>/images/titoli/strumenti2b.gif" valign="bottom" align="right">
				<table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
					<tr>
						<td width="100%" valign="top" align="right">
							<b class="tbltext1a">Bilancio Competenze</b>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<br>
	<table cellpadding="0" cellspacing="0" width="500" border="0">
		<tr height="18">
			<td class="sfondomenu" height="18" width="60%">
				<span class="tbltext0">
					<b>STAGE, TIROCINI, APPRENDISTATO</b>
				</span>
			</td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
			<td valign="middle" align="right" width="35%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1"></td>
		</tr>
		<tr width="371" class="SFONDOCOMM">
			<td colspan="3">
				Analisi:&nbsp;<b><%=request("hdnTipo")%></b>
			</td>
		</tr>
		<tr height="2">
			<td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>
	
	<form name="frmIndietro" method="post" action="Bil_Ricerca2.asp">
		<input type="hidden" name="richstage" value="<%=nIdStage%>">	
		<input type="hidden" name="VISMENU" value="NO">		
	</form>
		
	<br><br>
	<table border="0" cellspacing="1" cellpadding="1" width="500">
		<tr align="middle">
			<td class="tbltext3">
				Impossibile effettuare la registrazione.
			</td>
		</tr>
		<tr align="middle">
			<td class="tbltext3">
				<%Response.Write(Errore)%> 
			</td>
		</tr>
	</table>
	<br>				
	<table border="0" cellpadding="0" cellspacing="1" width="500">
		<tr>
			<td align="middle" colspan="2" width="60%"><b>
			 <a HREF="javascript:frmIndietro.submit()"><img SRC="<%=Session("Progetto")%>/images/indietro.gif" border="0">
			</td>
		</tr>
	</table>
	<!--#include Virtual="/strutt_coda2.asp"-->
<%
end if

if Errore="0" then %>
	<!--#include virtual = "/include/CloseConn.asp"-->
<%
end if%>


