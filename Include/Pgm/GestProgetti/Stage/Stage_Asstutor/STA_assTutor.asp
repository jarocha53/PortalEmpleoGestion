<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include virtual="/util/portallib.asp"-->
<!--#include Virtual = "/strutt_testa2.asp"-->
<!--#include Virtual ="/Include/openconn.asp"-->
<!--#include virtual="/include/SelAreaTerrBandi.asp"-->

<script language="javascript">

<!--#include virtual = "/Include/ControlDate.inc"-->

//include del file per fare i controlli sulla numericitÓ dei campi
<!--#include virtual = "/Include/ControlNum.inc"-->
<!--#include virtual = "/Include/help.inc"-->


function AssegnaVal(val){
	document.FrmPagina.pagina.value = val;
	document.FrmPagina.submit()
}



function pippo(tu){
    document.frmtu.txtIdRichiesta.value = tu;
    document.frmtu.submit()   
}
</script>

<%	
'********************************
'*********** MAIN ***************

if ValidateService(session("idutente"),"STA_assTutor",cc) <> "true" then 
	response.redirect "/util/error_login.asp"
end if
dim nCont
Dim nActPagina
Dim nTotPagina
Dim nTamPagina		
Dim nTotRecord	
nTamPagina= 5

If Request("pagina")= "" Then
	nActPagina=1
Else
	nActPagina=Clng(Request("pagina"))
End If



Function ContaNonAssegnati(nRic)
	dim sSQL
	dim rsCNAss
	dim nConta

	sSQL= "SELECT count(*) as conta FROM PERSONA P,CAND_DOMSTAGE CD,DOMANDA_STAGE J,PERS_STAGE N WHERE P.ID_PERSONA = CD.ID_PERSONA AND P.ID_PERSONA = N.ID_PERSONA AND CD.ID_RICHSTAGE = " & nRic & " AND CD.ID_RICHSTAGE = J.ID_RICHSTAGE AND CD.ID_RICHSTAGE = N.ID_RICHSTAGE AND N.DT_SOSPENSIONE is null AND DT_CANDDAL <= " & convDateToDbs(now()) & " AND ID_TUTOR IS NULL"
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	set rsCNAss = cc.execute(sSQL)
		nConta = rsCNAss("conta")
	rsCNAss.close
	set rsCNAss = nothing
	
	if cint(nConta) > 0 then
		ContaNonAssegnati = "<a> <img alt='Non tutti i candidati sono associati al Tutor'  border=0 src='" & "/images/gruppogreen.gif' ></a>"
		
	else
		ContaNonAssegnati = "<a> <img  border=0 src='" & "/images/gruppored.gif' alt='Tutti i candidati sono associati al Tutor'></a>"
	end if
End Function

Inizio()

Tutor()

'**********************************************************************************************************************************************************************************	
Sub Inizio()
%>
<!-- Lingetta superiore 	Scrittura dell'intestazione della pagina-->
	
	<table border="0" width="520" cellspacing="0" cellpadding="0" height="70" align="center">
		<tr>
			<td width="500" background="<%=session("progetto")%>/images/titoli/strumenti2b.gif" height="70" valign="bottom" align="right">
				<table border="0" background width="500" height="23" cellspacing="0" cellpadding="0">
					<tr>
						<td width="100%" valign="top" align="right"><b CLASS="tbltext1a">Assegnazione Tutor
						  </b></td>
					</tr>
				</table>
			</td>
		</tr>
	</table> 
	<br>
	<table border="0" CELLPADDING="0" CELLSPACING="0" width="500" align="center">
		<tr height="18">
			<td class="sfondomenu" width="67%" height="18"><span class="tbltext0"><b>&nbsp;VISUALIZZAZIONE RICHIESTE STAGE </b></span></td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
		  	<td valign="middle" align="right" class="tbltext1" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
		</tr>
	</table>
	<!-- Commento -->
	<table border="0" CELLPADDING="0" CELLSPACING="0" width="500" align="center">
		<tr>
			<td align="left" class="sfondocomm">
			    Selezionare uno stage per visualizzare i candidati ammessi.
			    Nello <b>Status</b> viene indicata l'assegnazione di un Tutor per i candidati dello Stage
			    <a href="Javascript:Show_Help('/pgm/help/gestprogetti/stage/Stage_AssTutor/STA_assTutor')"><img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
			</td>
		</tr>
		<tr height="2">
			<td class="sfondocommaz" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>

<%	
End Sub

Sub Tutor()
	
	nCont = 0
	n = 0

	
	'''LC**:30/09/2003 - Aggiunto controllo su dt_sospensione = null e su proroghe.
	sSQL=	"select distinct(a.id_richstage),b.dt_inistage,b.dt_finstage,c.id_impresa,c.descrizione,d.rag_soc,e.DENOMINAZIONE as DescArea,FP.DENOMINAZIONE as DescFig,num_posti_disp,cod_tipo_ricerca,b.ID_AREAPROF,b.ID_FIGPROF " &_
			" from pers_stage a,domanda_stage b,sede_impresa c,impresa d,aree_professionali e,FIGUREPROFESSIONALI FP " &_ 
			" where a.id_richstage = b.id_richstage " &_ 
			" and b.id_sede =c.id_sede " &_ 
			" and c.id_impresa = d.id_impresa " &_ 
			" and b.id_areaprof= e.id_areaprof " &_
			" and b.id_figprof= FP.id_figprof " &_ 
			" and a.id_uorg= " & session("iduorg")  &_
			" and dt_sospensione is null" &_
			" and (" & _
					"B.DT_FINSTAGE >= " & convDateToDbs(Now) & _ 
					" OR (B.DT_FINSTAGE < " & convDateToDbs(Now) & _ 
					" AND A.ID_RICHSTAGE IN (" & _
						"SELECT DISTINCT(F.ID_RICHSTAGE) FROM PROROGA_STAGE F " & _
						"WHERE DT_FINEPRO >= " & convDateToDbs(Now) & _
						" AND F.ID_RICHSTAGE = A.ID_RICHSTAGE)" & _
					")" & _
				  ")" 

'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
    'SET rsRisultato = cc.execute(sSQL)
    SET rsRisultato = Server.CreateObject("ADODB.Recordset")
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	rsRisultato.Open sSQL, CC, 3   
   
    if not rsRisultato.Eof then
    
    
			rsRisultato.PageSize = nTamPagina
			rsRisultato.CacheSize = nTamPagina
			nTotPagina = rsRisultato.PageCount
			
			
			If nActPagina < 1 Then
				nActPagina = 1
			End If
			If nActPagina > nTotPagina Then
				nActPagina = nTotPagina
			End If
			if nTotPagina < 1 then
				nActPagina = 1
			end if
			rsRisultato.AbsolutePage= nActPagina
			nTotRecord=0
    
    
   
    
    'Response.Write sSQL
    'Response.end
    
     %>
       <form name="frmtu" method="post" action="/pgm/gestprogetti/stage/stage_asstutor/STA_VisTutor.asp">
           <input type="hidden" name="txtIdRichiesta">
      
		<table width="500" align="center" border="0" cellspacing="2" cellpadding="1" class="sfondocommaz">
            <tr height="20" class="tblsfondo"> 
			  	<td class="sfondocomm" align="middle" width="10">
					<b class="tbltext1">Rif. Rich.</b>
				</td>
				<td class="sfondocomm" align="middle" width="155">
					<b class="tbltext1">Denominazione Azienda</b>
				</td>
				<td class="sfondocomm" align="middle" width="260">
					<b class="tbltext1">Caratteristica stage</b>
				</td>
				<td class="sfondocomm" align="middle" width="100">
					<b class="tbltext1">Periodo Stage<br>dal/al</b>
				</td>
				<td class="sfondocomm" align="middle" width="10">
					<b class="tbltext1">Proroghe</b>
				</td>
				<td class="sfondocomm" align="middle" width="40">
					<b class="tbltext1">Status </b>
				</td>				
			</tr>
			
<%    
		do while not rsRisultato.eof and nTotRecord < nTamPagina
			
			sCodRic = rsRisultato("COD_TIPO_RICERCA")
			sCaratteristica=""
			if sCodRic = 0 then
				sCaratteristica = " per l'area """ & rsRisultato("DescArea") & """"
			else
				sCaratteristica = " per il profilo """ & rsRisultato("DescFig")  & """"
			end if 
			'''LC**:30/09/2003 - inserito conteggio proroghe.
			sSQL =	"SELECT count(ID_RICHSTAGE) as num FROM PROROGA_STAGE " & _
					"WHERE ID_RICHSTAGE = " & rsRisultato("id_richstage")

			SET rsApp = Server.CreateObject("ADODB.Recordset")
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
			rsApp.Open sSQL, CC, 3   
%>			
		      <tr height="20" class="tblsfondo">      
				<td class="tbldett" align="middle">
				    <a class="tblAgg" href="Javascript:pippo('<%=rsRisultato("ID_RICHSTAGE")%>')" onmouseover="javascript:window.status=' '; return true">
					<%=rsRisultato("id_richstage")%>
				</td>
				<td class="tbldett" align="middle">
					<%=rsRisultato("rag_soc")%>&nbsp;<%=rsRisultato("descrizione")%>
				</td>
				<td class="tbldett" align="middle">Richiesta di <%=rsRisultato("NUM_POSTI_DISP")%> 
					posti <%=sCaratteristica%>
				</td>
				<!--td class="tbldett" align="middle">					<%'=rsRisultato("denominazione")%>				</td-->
				<td class="tbldett" align="middle">
					<%=rsRisultato("dt_inistage")%>&nbsp;-<%=rsRisultato("dt_finstage")%>
				</td>
				<td class="tbldett" align="middle">
					<center><%=rsApp("num")%></center>
				</td>
				<td>
					<%=ContaNonAssegnati(rsRisultato("id_richstage"))%>
				</td>
			</tr>
<%      
			rsApp.Close
			set rsApp=nothing
			
			n= n +1
			nTotRecord=nTotRecord + 1
			rsRisultato.MoveNext
		loop
%>

		
		</table>
		</form>
	<form name="FrmPagina" method="post" action="STA_assTutor.asp">	
	<table border="0" width="500">
			<tr>
			<%
			if nActPagina > 1 then%>		
				<td align="right" width="480">
					<a href="Javascript:AssegnaVal('<%=(nActPagina - 1)%>')">
						<img src="<%=Session("Progetto")%>/images/precedente.gif" border="0" id="image1" name="image1" alt="tasto per pagina precedente" title="tasto per pagina precedente">
					</a>
				</td>
			<%
			end if
			if nActPagina < nTotPagina then%>
				<td align="right">
					<a href="Javascript:AssegnaVal('<%=(nActPagina + 1)%>')">
						<img src="<%=Session("Progetto")%>/images/successivo.gif" border="0" id="image1" name="image1" alt="tasto per pagina successiva" title="tasto per pagina successiva">
					</a>
				</td>
			<%
			end if%>		
			</tr>
			<input type="hidden" name="pagina" id="pagina">
		</table>	
		</form>
		
		
<%  else%>
         <br><br><br>
         <table width="500" align="center" border="0" cellspacing="1" cellpadding="0">
			
			<tr>
				<td align="center" class="tbltext3">
					<b>non ci sono Stage disponibili</b>
				</td>
			</tr>
		</table>


     
    
		
		
<%end if %>	
	
<%				
	End Sub
		
%>				

<!--#include Virtual ="/Include/closeconn.asp"-->
<!--#include Virtual="/strutt_coda2.asp"-->		
