<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
	<title>Calendario</title>

	<!--#include virtual = "/strutt_testa2.asp"-->
	<!--#include virtual ="/include/openconn.asp"-->
	<!--#include virtual = "/util/portallib.asp"-->
	<!--#include virtual = "/util/dbutil.asp"-->
	<!--#include virtual = "/include/DecCod.asp"-->
	<!--#include virtual = "/include/SelAreaTerrBandi.asp"-->
	<!--#include virtual = "/include/ControlDateVB.asp"-->
	
		<script LANGUAGE="JavaScript">
		<!--#include virtual = "/Include/help.inc"-->


		function Validator(TheForm, d)
		{
		if (d == "")
			{
			alert("Selezionare il giorno di riferimento.");
			return (false);
		  	}
		//Controllo se la data digitata dall'utente � maggiore della data odierna  	
		  		  	
		var now = new Date()
		var Ytoday = now.getYear()
		var Dtoday = now.getDate()
		var Mtoday = now.getMonth() + 1 	
		var Dasked = d
		var Masked = TheForm.cboMese.value
		var Yasked = TheForm.cboAnno.value
		
		if (Yasked > Ytoday)
			{
			document.frm1.cboAnno.focus();
			alert("La data richiesta - " + Dasked + "/" + Masked + "/" + Yasked + " - Risulta essere superiore alla data odierna.");
			return (false);
		  	}
		 else
			{
			if (Masked > Mtoday && Yasked == Ytoday)
				{
				document.frm1.cboMese.focus();
				alert("La data richiesta - " + Dasked + "/" + Masked + "/" + Yasked + " - Risulta essere superiore alla data odierna.");
				return (false);
		  		}
		  	else
		  		{
		  		if (Dasked > Dtoday && Masked == Mtoday && Yasked == Ytoday)
					{					
					alert("La data richiesta - " + Dasked + "/" + Masked + "/" + Yasked + " - Risulta essere superiore alla data odierna.");
					return (false);
		  			}
		  		}
		  	}	
		
		
		return (true);
		}
		function leapYear(year)
		{
			if (year % 4 == 0) // basic rule
				return true // is leap year
			/* else */ // else not needed when statement is "return"
				return false // is not leap year
		}

		function getDays(month, year) {
			// create array to hold number of days in each month
			var ar = new Array(12)
			ar[0] = 31 // January
			ar[1] = (leapYear(year)) ? 29 : 28 // February
			ar[2] = 31 // March
			ar[3] = 30 // April
			ar[4] = 31 // May
			ar[5] = 30 // June
			ar[6] = 31 // July
			ar[7] = 31 // August
			ar[8] = 30 // September
			ar[9] = 31 // October
			ar[10] = 30 // November
			ar[11] = 31 // December

			// return number of days in the specified month (parameter)
			return ar[month]
		}

		function getMonthName(month) {
			// create array to hold name of each month
			var ar = new Array(12)
			ar[0] = "Gennaio"
			ar[1] = "Febbraio"
			ar[2] = "Marzo"
			ar[3] = "Aprile"
			ar[4] = "Maggio"
			ar[5] = "Giugno"
			ar[6] = "Luglio"
			ar[7] = "Agosto"
			ar[8] = "Settembre"
			ar[9] = "Ottobre"
			ar[10] = "Novembre"
			ar[11] = "Dicembre"

			// return name of specified month (parameter)
			return ar[month]
		}

		function setCal(sel,sBando) {
			// standard time attributes
			
			var now = new Date()
			var iAnno = document.frm1.cboAnno.selectedIndex
			var Anno = document.frm1.cboAnno.options[iAnno].text
			var iMese = document.frm1.cboMese.selectedIndex
			var Mese = document.frm1.cboMese.options[iMese].value	
		
			var year =  Anno
			var month= (Mese - 1)
			var monthName = getMonthName(month)
			var date = now.getDate()
			now = null

			// create instance of first day of month, and extract the day on which it occurs
	
			var firstDayInstance = new Date(year, month, 01)
			var firstDay = firstDayInstance.getDay()
			firstDayInstance = null

			// number of days in current month
			var days = getDays(month, year)
				
			// call function to draw calendar
				drawCal(firstDay + 1, days, date, monthName, month + 1, year, sel, sBando)
		}

		function drawCal(firstDay, lastDate, date, monthName, month, year, sel ,sBando) 
		{
			
			// constant table settings
			var headerHeight = 20 // height of the table's header cell
			var border = 2 // 3D height of table's border
			var cellspacing = 2 // width of table's border
			var cellpadding = 2
			var headerColor = "#FFFFFF" // color of table's header
			var headerSize = "3" // size of tables header font
			var colWidth = 34 // width of columns in table
			var dayCellHeight = 18 // height of cells containing days of the week
			var dayColor = "#3163A0" // color of font representing week days
			var cellHeight = 20 // height of cells representing dates in the calendar
			var todayColor = "red" // color specifying today's date in the calendar
			var fontFace = "verdana, helvetica, arial, sans-serif"
			
			//Reperimento della data odierna
			var now = new Date()
			var Ytoday = now.getYear()
			var Mtoday = now.getMonth() + 1
			if (navigator.appName == "Netscape")
				{Ytoday = Ytoday + 1900
				}
		

			// create basic table structure
			var text = "" // initialize accumulative variable to empty string
			//text += '<CENTER>'
			text += '<TABLE BORDER=' + border + ' CELLSPACING=' + cellspacing  + ' CELLPADDING=' + cellpadding + ' bgcolor=#f1f3f3>' // table settings
			text += 	'<TH COLSPAN=7 HEIGHT=' + headerHeight + ' bgcolor=#c0e1fe>' // create table header cell
			text += 		'<FONT COLOR="' + headerColor + '" SIZE=' + headerSize + ' face=' + fontFace + '>' // set font for table header
			text += 			monthName + ' ' + year 
			text += 		'</FONT>' // close table header's font settings
			text += 	'</TH>' // close header cell

			// variables to hold constant settings
			var openCol = '<TD WIDTH=' + colWidth + ' HEIGHT=' + dayCellHeight + ' bgcolor=#dddddd>'
			openCol += '<b><FONT COLOR="' + dayColor + '" face=' + fontFace + '>'
			var closeCol = '</FONT></b></TD>'

			// create array of abbreviated day names
			var weekDay = new Array(7)
			weekDay[0] = "dom"
			weekDay[1] = "lun"
			weekDay[2] = "mar"
			weekDay[3] = "mer"
			weekDay[4] = "gio"
			weekDay[5] = "ven"
			weekDay[6] = "sab"
			
			// create first row of table to set column width and specify week day
			text += '<TR ALIGN="center" VALIGN="center">'
			for (var dayNum = 0; dayNum < 7; ++dayNum) {
				text += openCol + weekDay[dayNum] + closeCol 
			}
			text += '</TR>'
			
			// declaration and initialization of two variables to help with tables
			var digit = 1
			var curCell = 1
			
			for (var row = 1; row <= Math.ceil((lastDate + firstDay - 1) / 7); ++row) 
			{
				text += '<TR ALIGN="right" VALIGN="top">'
				for (var col = 1; col <= 7; ++col) 
				{
										
					var iAnno = document.frm1.cboAnno.selectedIndex
					var Anno = document.frm1.cboAnno.options[iAnno].text
					var iMese = document.frm1.cboMese.selectedIndex
					var Mese = document.frm1.cboMese.options[iMese].value
					
					var rif = 'ASS_CalendarioStage.asp?sBando='+ sBando + '&Modo=2&month=' + Mese + '&year=' + Anno + '&day=' + digit
				
					if (digit > lastDate)
						break
					if (curCell < firstDay)
					 {
						text += '<TD></TD>';
						curCell++
					 }
					else
					 {
						if (digit == date && year == Ytoday && month == Mtoday) 
							 // current cell represent today's date
							{
							if (digit != sel)
								{
								text += '<TD HEIGHT=' + cellHeight + ' bgcolor="#dddddd" bordercolor="#008caa" >'
								text += '<a href="' + rif + '"><b><FONT size=2 face=' + fontFace + ' color="#008caa">'+ digit + '</b></a>'
								text += '</FONT><BR>'
								text += '</TD>'
								}
							else
								{
								text += '<TD HEIGHT=' + cellHeight + '>'
								text += '<b><FONT  size=2 face=' + fontFace + ' color=' + todayColor + '>'+ digit + '</b>'
								text += '</FONT><BR>'
								text += '</TD>'
								}
							}
						else
							{
								if (digit == sel) 
								{ 
								text += '<TD HEIGHT=' + cellHeight + '>'
								text += '<b><FONT  size=2 face=' + fontFace + ' color=' + todayColor + '>'+ digit + '</b>'
								text += '</FONT><BR>'
								text += '</TD>'
								}
								else
								{
								text += '<TD HEIGHT=' + cellHeight + '>' 
								text +='<a href="' + rif + '"><FONT  size=2 face=' + fontFace + '><b>' + digit + '</b></font></a></TD>'
								}
							}
						digit++
					}
				}
				text += '</TR>'
			}
			
			text += '</TABLE>'
			text += '</CENTER>'

			document.write(text) 
			
		}
		function  Ricarica()
		{  
			var iAnno = document.frm1.cboAnno.selectedIndex
			var Anno = document.frm1.cboAnno.options[iAnno].text
			var iMese = document.frm1.cboMese.selectedIndex
			var Mese = document.frm1.cboMese.options[iMese].value
			
			location.href = 'ASS_CalendarioStage.asp?Modo=1&month=' + Mese + '&year=' + Anno 
		}
		function Show_Help(W2Show)
		{
			f=W2Show;
			w=(screen.width-(screen.width/2))/2;	
			h=(screen.height-(screen.height/2))/2;
			fin=window.open(f,"pippo","toolbar=0, location=0,directories=0,status=0,menubar=0,scrollbars=0,resizable=0,copyhistory=0,width=600,height=480,screenX=w,screenY=h");	
		}
		function Destro(e) 
		{
		if (navigator.appName == 'Netscape' && 
			(e.which == 3 || e.which == 2))
			return false;
		else if (navigator.appName == 'Microsoft Internet Explorer' && 
				(event.button == 2 || event.button == 3))
			 {
				alert("Spiacenti, il tasto destro del mouse e' disabilitato");
				return false;
			  }
		return true;
		}

		if (document.layers) window.captureEvents(Event.MOUSEDOWN);
		window.onmousedown=Destro;
		// -->
		</script>
	
	
	<!--	FINE BLOCCO SCRIPT	-->
	<style>A {
		COLOR: "#003843"; TEXT-DECORATION: none
	}
	A:hover {
		COLOR: "#ef0000"; FONT-WEIGHT: none
	}
	</style>
<%
	Sub Inizio()
%>	
	<table border="0" width="500" cellspacing="0" cellpadding="0" height="81">
	   <tr>
	     <td width="500" background="<%=Session("Progetto")%>/images/titoli/strumenti2b.gif" height="81" valign="bottom" align="right">
	       <table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
	         <tr>
	           <td width="100%" valign="top" align="right"><b class="tbltext1a">Registro Assenze</span></b></td>
	         </tr>
	       </table>
	     </td>
	   </tr>
	</table>

	<br>
	<table cellpadding="0" cellspacing="0" width="500" border="0">
		<tr height="18">
			<td class="sfondomenu" height="18" width="40%">
			<span class="tbltext0"><b>&nbsp;REGISTRO DI CLASSE</b></span></td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
			<td valign="middle" align="right" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1"></td>
		</tr>
		<tr width="371" class="SFONDOCOMM">
			<td colspan="3">Selezionare il giorno e premere <b>Invia</b> per visualizzare gli stage.
				
				<a href="Javascript:Show_Help('/Pgm/help/Gestprogetti/Stage/Assenze/ASS_CalendarioStage')">
		<img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
			</td>
		</tr>
		<tr height="2">
			<td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>
	<br><br>
<%
	End Sub
%>	
<!--------------------------------------------------------------------------------------------------------------------------------------------------------->
<%Sub ImpostaPag()
		
		Dim yyOggi, mYear, mmOggi
		Dim Arr(12), Rif
		Dim i
		Dim sModo, sMese, sAnno, Yappo
		
%>		<form name="frm1" action="ASS_VisStage.asp" method="post" onsubmit="return Validator(this,&quot;<%=nDay%>&quot;)">
			<input type="hidden" name="day" value="<%=nDay%>">
			<input type="hidden" name="month" value="<%=nMonth%>">
			<input type="hidden" name="year" value="<%=nYear%>">
			<table border="0" cellspacing="2" cellpadding="2" width="500">
<%	
			' Impostazione dei combo dell' anno e del mese di riferimento
			Arr(1) = "Gennaio"
			Arr(2) = "Febbraio"
			Arr(3) = "Marzo"
			Arr(4) = "Aprile"
			Arr(5) = "Maggio"
			Arr(6) = "Giugno"
			Arr(7) = "Luglio"
			Arr(8)	= "Agosto"	
			Arr(9) = "Settembre"
			Arr(10) = "Ottobre"	
			Arr(11) = "Novembre"
			Arr(12) = "Dicembre"
		
			sModo = Request.QueryString("Modo")
			sMese = Request.QueryString("month")
			sAnno = Request.QueryString("year")
			
			if sModo = 1  or sModo = 2 then
				yyOggi = sAnno
				mmOggi = sMese
			else
				yyOggi = Year(Date)
				mmOggi = Month(Date)
			end if
%>			
			<tr align="center">
				<td class="tbltext1" align="left" width="100">
					<b>Anno &nbsp;</b>
				</td>
				<td align="left" width="60">
					<span class="textblack">
					<select class="textblack" size="1" name="cboAnno" onchange="javascript:Ricarica(<%'=sBando%>)">
						<%  i = -4 
							mYear = Year(date)
							do until i > 6
								Yappo = mYear + i
								i = i + 1
								if cstr(Yappo) = cstr(yyOggi) then
						%><option selected value="<%=Yappo%>"><%=Yappo%></option><%		else
						%><option value="<%=Yappo%>"><%=Yappo%></option><%		end if
							loop 
						%></span>
					</select>
				</td>
			
				<td class="tbltext1" align="right" width="133">
					<b>Mese&nbsp;</b>
				</td>
				<td align="left">
					<span>
					&nbsp;&nbsp;<select class="textblack" size="1" name="cboMese" onchange="javascript:Ricarica(<%=sBando%>)"><%	For i = 1 to 12 
							if cstr(mmOggi) = cstr(i) then
					%><option selected value="<%=i%>"><%=Arr(i)%></option>
					<%		else
					%><option value="<%=i%>"><%=Arr(i)%></option><%		end if
						next
					%></span>
					</select>
				</td>
			</tr>
			</table>
			<br>
			<table border="0" cellspacing="2" cellpadding="2" width="500">
			<tr>
				<td class="tbltext1" align="left" valign="top" width="100">
					<b>Giorno &nbsp;</b>
				</td>
				<td align="left">
				<% if sModo = 2 then %>
					<script>setCal('<%=nDay%>','<%=sBando%>')</script>
				<% else%>
					<script>setCal(0,'<%=sBando%>')</script>
				<% end if%>
				</td>
			</tr>
			<tr><td>&nbsp;</td></tr>
			<tr align="center">
					<td colspan="5">
						<input type="image" name="Conferma" src="<%=Session("progetto")%>/images/conferma.gif">
					</td>
			</tr>
			</table>
		
		</form>
<%	End Sub
%>
<!--------------------------------------------------------------------------------------------------------------------------------------------------------->
<%
	Dim strConn, sOper
	Dim nDay, nMonth, nYear	

	if ValidateService(session("idutente"),"ASS_CALENDARIOSTAGE",cc) <> "true" then 
		response.redirect "/util/error_login.asp"
	end if
	
	nDay = Request.QueryString("day")
	nMonth = Request.QueryString("month")
	nYear = Request.QueryString("year")
	Modo = Request.QueryString("modo")
	
	if len (nDay) = 1 then 
		nDay = "0" & nDay
	end if
	if len (nMonth) = 1 then 
		nMonth = "0" & nMonth
	end if 
	
	Inizio()
	
	ImpostaPag()
%>
