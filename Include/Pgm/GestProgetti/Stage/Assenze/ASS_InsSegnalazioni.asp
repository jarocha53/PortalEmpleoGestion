<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include virtual = "/strutt_testa2.asp"-->
<!--#include virtual = "/include/openconn.asp"-->
<!--#include virtual = "/util/portallib.asp"-->
<!--#include virtual = "/util/dbutil.asp"-->
<!--#include virtual = "/include/DecCod.asp"-->
<!--#include Virtual = "/include/SysFunction.asp"-->

<%
if ValidateService(session("idutente"),"ASS_CALENDARIOSTAGE", CC) <> "true" then 
	response.redirect "/util/error_login.asp"
end if
%>

<script LANGUAGE="JavaScript">
<!--#include virtual = "/Include/ControlString.inc"-->	
<!--#include Virtual = "/Include/help.inc"-->

function ControllaDati(maxInput){
	var i;
	var n;
	var c = 0;

	frmSegnalazioni.TipoSegnal.value = "";
	n = frmSegnalazioni.NumCheck.value;
	//alert ("n=" + n)
	// TIPO DELL'ANOMALIA
	for (i=0; i<n; i++){
		if (frmSegnalazioni.chkSegn[i].checked) {
			c = c + 1; 
			frmSegnalazioni.TipoSegnal.value = frmSegnalazioni.TipoSegnal.value + 	
											   frmSegnalazioni.chkSegn[i].value;
		}
	}

	if (c == 0) {
		frmSegnalazioni.chkSegn[0].focus();
		alert("Indicare la natura dell'anomalia.");
		return false;
	 }else{
	 	if (frmSegnalazioni.txtDescrizione.value == ""){
	 	    alert("La Descrizione � obbligatoria");
	 	    frmSegnalazioni.txtDescrizione.focus();
			return false;
	 	 }
	}
	return true
}	

function Inserimento(MaxInput) {
	if (ControllaDati(MaxInput) == false) {
		return false;
	}
    frmSegnalazioni.Operazione.value  = 'Ins';
	frmSegnalazioni.action="ASS_CnfSegnalazioni.asp";
	frmSegnalazioni.submit();
	return true;
}

function Modifica(MaxInput) {
	if (ControllaDati(MaxInput) == false) {
		return false;
	}
	frmSegnalazioni.Operazione.value  = 'Mod';
	frmSegnalazioni.action="ASS_CnfSegnalazioni.asp";
	//frmSegnalazioni.submit();
	return true;
}

function Cancellazione(MaxInput) {
	frmSegnalazioni.Operazione.value  = 'Can';
	frmSegnalazioni.action="ASS_CnfSegnalazioni.asp";
	//frmSegnalazioni.submit();
}

function VaiIndietro(){
	location.href;
}
</script>

<!--------------------------------------------------------------------------------------------------------------------------------------------------------->
<!--	FINE BLOCCO SCRIPT	-->
<!--------------------------------------------------------------------------------------------------------------------------------------------------------->

<%

dim  sCodSess , nIdClasse , sIdSegnalazione , sDtSegnal
dim sSql, nNumTipSegn, sDescAula, i, j, n, s, nNumBottoni
dim aTipSegn(30)
dim maxInput
	
maxInput = 250	'lunghezza massima del campo TESTO		

sDescAula	= Request("Aula")
nIdClasse	= Request("Classe")
sCodSess	= Request("CodSess")
sDtSegnal = ConvDateToDB(Date())

'Si verifica l'esistenza sul db di una segnalazione per la classe selezionata
sSql =	"SELECT ID_SEGNALAZIONI, IDUTENTE, ID_CLASSE, COD_SESSIONE," &_
		" DT_SEGNAL, TIPO_SEGNAL, DESCR_SEGNAL, DT_TMST" &_
		" FROM SEGNALAZIONI" &_
		" WHERE ID_CLASSE = " & nIdClasse &_
		" AND   DT_SEGNAL = " & sDtSegnal 
				
'Response.Write sSql
set RsSegnalazioni=server.CreateObject("ADODB.Recordset")
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
RsSegnalazioni.open sSql, CC, 3		
		
if not RsSegnalazioni.EOF then
	nNumTipSegn = len(RsSegnalazioni.Fields("TIPO_SEGNAL"))
	for i = 1 to nNumTipSegn
		aTipSegn(i) = mid(RsSegnalazioni.Fields("TIPO_SEGNAL"), i ,1)
	next
	sIdSegnalazione = RsSegnalazioni.Fields("ID_SEGNALAZIONI")
	sDescr = RsSegnalazioni.Fields("DESCR_SEGNAL")
	sDtTmst = RsSegnalazioni.Fields("DT_TMST")
	' Al tasto "Conferma" si esegue l'Update
	nNumBottoni = 3
else
	' Al tasto "Conferma" si esegue l'Insert
	sIdSegnalazione = ""
	sDescr = ""
	sDtTmst = ""
	nNumBottoni = 2
end if
		
RsSegnalazioni.Close
set RsSegnalazioni = nothing

' Si reperisce il numero di tipi di segnalazioni possibili
sSql =	"SELECT COUNT(*)" &_
		" FROM TADES" &_
		" WHERE Nome_Tabella ='SIGNA'"
set RsTades = server.CreateObject("ADODB.Recordset")
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
RsTades.open sSql, CC, 3	
	
s = RsTades.fields(0)
	
RsTades.Close
set RsTades = nothing
%>

<table border="0" width="520" cellspacing="0" cellpadding="0" height="81">
	<tr>
		<td width="500" background="<%=Session("Progetto")%>/images/titoli/strumenti2b.gif" height="81" valign="bottom" align="right">
			<table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
				<tr>
					<td width="100%" valign="top" align="right"><b class="tbltext1a">Gestione Progetti</span></b></td>
				</tr>
			</table>
	     </td>
   </tr>
</table>

<br>

<table cellpadding="0" cellspacing="0" width="500" border="0">
	<tr height="18">
		<td class="sfondomenu" height="18" width="67%">
			<span class="tbltext0"><b>&nbsp;GESTIONE SEGNALAZIONI</b></span></td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
		<td valign="middle" align="right" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1"></td>
	</tr>
	<tr width="371" class="SFONDOCOMM">
		<td colspan="3">
		Inserire o modificare il testo della segnalazione. <br>
		Premere <b>Invia</b> per salvare la segnalazione. 
		<a href="Javascript:Show_Help('/Pgm/help/GestProgetti/stage/assenze/ASS_InsSegnalazioni')" onmouseover="javascript:window.status=' '; return true"><img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a></td>
	</tr>
	<tr height="2">
		<td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif">
		</td>
	</tr>
</table>

<br><br>

<form method="POST" name="frmSegnalazioni">
    <input type="hidden" name="Operazione" value> 
    <input type="hidden" name="IdSegnalazione" value="<%=sIdSegnalazione%>">    
    <input type="hidden" name="IdClasse" value="<%=nIdClasse%>">          
    <input type="hidden" name="CodSess" value="<%=sCodSess%>">     
    <input type="hidden" name="TipoSegnal" value>         
    <input type="hidden" name="DtTmst" value="<%=sDtTmst%>">
    <input type="hidden" name="NumCheck" value="<%=s%>">
	    	
	<table width="500" border="0" class="tblsfondo" cellspacing="0" cellpadding="0">
		<tr align="center">
			<td colspan="3"><span class="tbltext3">
				<b>Stage&nbsp;<%=sDescAula%></b></span>
			</td>
		</tr>
		<tr>
			<td colspan="3">&nbsp;&nbsp;</td>
		</tr>
		<tr align="left">
			<td colspan="3" align="center"><span class="tbltext3">
				<b>Indicare la natura e la descrizione dell' anomalia riscontrata 
				</b></span>
			</td>
		</tr>
		<tr>
			<td colspan="3">&nbsp;&nbsp;</td>
		</tr>
	</table>			
	<table width="500" border="0" class="tblsfondo" cellspacing="0" cellpadding="0">
		<%			
		sSql = "SELECT * FROM TADES WHERE Nome_Tabella ='SIGNA'"
		set RsTades = server.CreateObject("ADODB.Recordset")
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
		RsTades.open sSql, CC, 3
		' Si estraggono i codici delle possibili segnalazioni	
		' (reperiti sulla tabella TADES)
		' e se � presente un record di segnalazione nella tabella SEGNALAZIONI
		' si flaggano quelli gi� indicati
		n = 0
		Do until RsTades.EOF
		%>
			<tr>
			<%
			for i = 1 to 3	
				if not RsTades.EOF then
					n = n + 1
					%>
					<td align="left" width="160" class="tbltext3">
						<input type="checkbox" name="chkSegn" value="<%=RsTades.Fields("Codice")%>" <%
							for j = 1 to nNumTipSegn
								if aTipSegn(j) = RsTades.Fields("Codice") then
								%> checked>
								<%
								end if
							next
							%> &nbsp; <b><%=RsTades.Fields("Descrizione")%></b>
					</td>
					<%
					RsTades.MoveNext
				end if
			next
			%>
			</tr>
		<%
		loop						
		%>
		<tr height="17"> 
			<td colspan="4">&nbsp;</td>
		</tr>
	</table>			
	<table width="500" border="0" class="tblsfondo" cellspacing="0" cellpadding="0">
		<tr height="17"> 
			<td height="15" width="15" valign="middle"><img SRC="<%=Session("Progetto")%>/images/righinaPSX.jpg"></td> 
			<td valign="middle" width="250" align="left" class="tblsfondo3"><b class="tbltext0">&nbsp; Testo della Segnalazione</b></td>
			<td width="200" class="tblsfondo3">
				<span class="tbltext0">- Utilizzabili <b><label name="NumCaratteri" id="NumCaratteri">250</label></b> caratteri -</span>
			</td>
			<td height="15" width="15" valign="middle"><img SRC="<%=Session("Progetto")%>/images/righinaPDX.jpg"></td> 
		</tr>
		<tr> 
			<td width="15" class="tbltext"> &nbsp; </td>
			<td colspan="2" align="left" width="250" valign="middle"><textarea name="txtDescrizione" onKeyup="JavaScript:CheckLenTextArea(document.frmSegnalazioni.txtDescrizione,NumCaratteri,<%=maxInput%>)" CLASS="MyTextBox" cols="70" rows="6"><%=sDescr%></textarea><br></td>
			<td width="15" class="tbltext"> &nbsp; </td>
		</tr>				
		<tr height="17"> 
			<td colspan="4">&nbsp;</td>
		</tr>
	</table>
		
	<table cellpadding="0" cellspacing="0" width="300" border="0">	
		<tr align="center">
			<%
			if nNumBottoni = 2 then
			%>
				<td nowrap> <input type="image" name="Conferma" src="<%=Session("progetto")%>/images/conferma.gif" onclick="javascript:Inserimento('<%=maxInput%>'); return false"></td value="Registra">
				<td nowrap> <a href="javascript:history.back()" onmouseover="javascript:window.status=' '; return true"><img src="<%=session("Progetto")%>/images/indietro.gif" border="0" name="imgPunto2"></a><td>
			<%
			else		
			%>
				<td nowrap> <input type="image" name="Conferma" onclick="javascript:Modifica('<%=maxInput%>')" src="<%=Session("progetto")%>/images/conferma.gif"></td value="Registra">
				<td nowrap> <input type="image" name="Elimina" onclick="javascript:Cancellazione('<%=maxInput%>')" src="<%=session("Progetto")%>/images/elimina.gif"></td value="Elimina">
				<td nowrap> <a href="javascript:history.back()" onmouseover="javascript:window.status=' '; return true"><img src="<%=session("Progetto")%>/images/indietro.gif" border="0" name="imgPunto2"></a><td>
			<%
			end if
			%>					
		</tr>
	</table>
</form>

<!-- #include virtual="/include/closeconn.asp" -->
<!--#include virtual = "/strutt_coda2.asp"-->
