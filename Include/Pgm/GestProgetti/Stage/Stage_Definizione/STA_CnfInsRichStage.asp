<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include virtual="/util/portallib.asp"-->
<!--#include Virtual = "/strutt_testa2.asp"-->
<!--#include Virtual = "/include/SysFunction.asp"-->
<!--#include Virtual ="/Include/openconn.asp"-->
<!--#include Virtual ="/Include/ControlDateVB.asp"-->
<!--#include Virtual = "/include/DecCod.asp"-->
<!--#include Virtual = "/include/HTMLEncode.asp"-->
<!--#include virtual= "/util/dbutil.asp"-->

<script language="javascript" src="Controlli.js">
</script>

<%
'*********** MAIN ***************
Dim nIdSede, nIdAreaP, nPostiDisp
Dim sDescStage, sDDiretta, sTipoStage
Dim dDt_IniVal, dDt_IniStage, dDt_FinStage, dDt_Now
dim sSql, sql, RcdSet,nIdRic


nIdsede		= Request.Form("txtIdSede")
sDescStage	= strHTMLEncode(Trim(Request.Form("txtDescStage")))
sDescStage	= replace(sDescStage,"'","''")
nIdAreaP	= clng(Request.Form("cmbTipoArea"))
nIdTipoP	= clng(Request.Form("cmbTipoProf"))
nTipoRic	= clng(Request.Form("cmbTipoRicerca"))
nPostiDisp	= clng(Request.Form("txtPostiDisp"))

dDt_IniVal	= ConvDateToDB(ConvStringToDate(Request.Form("txtIniVal")))
dDt_IniStage= ConvDateToDB(ConvStringToDate(Request.Form("txtIniStage")))
dDt_FinStage= ConvDateToDB(ConvStringToDate(Request.Form("txtFinStage")))
dDt_Now	= ConvDateToDB(Now())

sDDiretta = Request.Form("rDomDiretta")
if sDDiretta <> "S" then
	sDDiretta = "N"
end if
sTipoStage=Request.Form("cmbTipoStage")


sSql =	"Insert into Domanda_Stage" &_
		" (NUM_POSTI_DISP, NUM_POSTI_ASS, DT_FINSTAGE, FL_DOMDIRETTA, DESC_STAGE, ID_SEDE" &_
		", DT_INISTAGE, ID_AREAPROF, DT_INIVAL,ID_FIGPROF,COD_TIPO_RICERCA,DT_TMST"
if sTipoStage <> "PUB" then
	sSql = sSql & ", PROGETTO"
end if
sSql =	sSql & ") values" &_
		" ("& nPostiDisp &",0,"& dDt_FinStage &",'"& sDDiretta &"','"& sDescStage &"',"& nIdsede &_
		"," & dDt_IniStage &","& nIdAreaP &","& dDt_IniVal & "," & nIdTipoP & "," & nTipoRic & "," & dDt_Now 
if sTipoStage <> "PUB" then
	sSql = sSql & ",'" & sTipoStage & "'"
end if		
sSql =	sSql & ")"

sErrore=Esegui("","Domanda_Stage",session("idutente"),"INS",sSql,0,dDt_Now)
		
IF sErrore="0" then
	sql = ""
	sql =	" Select id_richstage from domanda_stage where" &_
			" NUM_POSTI_DISP=" & nPostiDisp & " and DT_FINSTAGE=" & dDt_FinStage &_
			" and FL_DOMDIRETTA='" & sDDiretta & "' and DESC_STAGE='" & sDescStage & "'" &_
			" and ID_SEDE=" & nIdsede & " and DT_INISTAGE=" & dDt_IniStage &_
			" and ID_AREAPROF=" & nIdAreaP & " and DT_INIVAL=" & dDt_IniVal &_
			" and ID_FIGPROF=" & nIdTipoP &  " and COD_TIPO_RICERCA=" & nTipoRic & " and dt_tmst =" & dDt_Now
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
	set RcdSet = CC.Execute(sql)
		nIdRic = RcdSet.Fields("id_richstage")
	RcdSet.Close
	set RcdSet = nothing
	sql = ""



' ****************************************************
' Richiesta mappatura figura prof alla camera di regia
' ****************************************************

	sSQL = "SELECT IND_MAPPA FROM FIGUREPROFESSIONALI WHERE ID_FIGPROF = " & CLng(nIdTipoP)
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	set rsMAPPA = CC.Execute(sSQL)
	if rsMAPPA(0)="N" then
		sSQL = "SELECT DENOMINAZIONE,COD_EJ FROM FIGUREPROFESSIONALI WHERE ID_FIGPROF=" &_
			CLng(nIdTipoP) & " AND ID_AREAPROF = " & CLng(nIdAreaP)

'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
		set rsDenom = CC.Execute(sSQL)
		sDen = rsDenom("DENOMINAZIONE")
		if not IsNull(rsDenom("COD_EJ")) then
			sCodIstat = rsDenom("COD_EJ")
		else
			sCodIstat = "N/D"
		end if
		rsDenom.CLose
		set rsDenom = nothing
				
		sSQL = "SELECT RAG_SOC,DESCRIZIONE FROM IMPRESA A, SEDE_IMPRESA B " &_
			"WHERE A.ID_IMPRESA = B.ID_IMPRESA AND B.ID_SEDE = " & clng(nIdsede)
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
		set rsImpresa = cc.Execute(sSQL)

		sImpresa = "( " & rsImpresa("RAG_SOC") & " / " & rsImpresa("DESCRIZIONE") & " )"

		rsImpresa.Close
		set rsImpresa = nothing
				
		InfoFigProf nIdRic,sDen,sImpresa,sCodIstat

	end if
	rsMAPPA.CLose
	set rsMAPPA = nothing
' *******************************



%>
	<form name="frmIndietro" method="post" action="STA_ModRichStage.asp">
		<input type="hidden" name="nRichSta" value="<%=nIdRic%>">
	</form>
	<script>
		alert("Inserimento correttamente effettuato");
		frmIndietro.submit()
	</script>
<%		
ELSE
	Inizio()
%>
	<br><br>
	<table width="500" align="center" border="0" cellspacing="2" cellpadding="1">
		<tr height="20"> 
		  	<td class="tbltext3" align="middle" width="500">
				<b>Errore in fase di inserimento.</b>
			</td>
		</tr>
		<tr height="20"> 
		  	<td class="tbltext3" align="middle" width="500">
				<b><%=sErrore%></b>
			</td>
		</tr>
	</table>
	<br><br>
	<table width="500" cellspacing="2" cellpadding="1" border="0">
		<tr align="center">
			<td>
				<a href="javascript:history.back()"><img src="<%=Session("progetto")%>/images/Indietro.gif" title="Pagina precedente" border="0" align="absBottom"></a>
			</td>
		</tr>
	</table>
<%
END IF

'**********************************************************************************************************************************************************************************	
Sub Inizio()
%>
	<table border="0" width="520" cellspacing="0" cellpadding="0" height="70">
		<tr>
			<td width="500" background="<%=session("progetto")%>/images/titoli/strumenti2b.gif" height="70" valign="bottom" align="right">
				<table border="0" background width="500" height="23" cellspacing="0" cellpadding="0">
					<tr>
						<td width="100%" valign="top" align="right"><b CLASS="tbltext1a">Definizione Stage
						  </b></td>
					</tr>
				</table>
			</td>
		</tr>
	</table> 
	<br>
	<!-- Linguetta superiore -->
		<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
			<tr height="18">
				<td class="sfondomenu" width="67%" height="18"><span class="tbltext0"><b>&nbsp;CONFERMA INSERIMENTO DOMANDA STAGE </b></span></td>
				<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
				<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
			</tr>
		</table>
		<!-- Commento -->
		<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
			<tr>
				<td align="left" class="sfondocomm">
					Pagina di conferma dell'inserimento di un nuovo stage.
				</td>
			</tr>
			<tr height="2">
				<td class="sfondocomm" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif">
				</td>
			</tr>
		</table>
	<br>
<%
End Sub 

sub InfoFigProf(nIdRic,sDen,sImpresa,Istat)
	' Inviare una EMAIL a CentroRegia@italialavoro.it 
	' per far inserire la mappa mancante
	Dim objMail 
	Set objMail = Server.CreateObject("CDONTS.NewMail") 
	HTML = HTML & "<HTML>"
	HTML = HTML & "<BODY bgColor=#f0f8ff>"
	HTML = HTML & "<div=center>"
	HTML = HTML & "<TABLE border=0 width=95% align=center cellpadding=1 bgColor=#f0f8ff cellspacing=2 ><tr><td>"
	HTML = HTML & "<font size='3' color='#000080' face='Verdana'><br><i><b>Il Portale di ItaliaLavoro</b></i></font><br><br>"
	HTML = HTML & "<IMG SRC='http://194.177.112.23/PLavoro/images/logo_italia_lavoro.gif'><br><br>"
	HTML = HTML & "<font size='2' color='#000080' face='Verdana'><br>"
	HTML = HTML & " <br>"
	HTML = HTML & "La informiamo che la richiesta di stage Rif. " & nIdRic
	HTML = HTML & " effettuata dalla sede " & clng(sIdSede)
	HTML = HTML & " " & sImpresa & " in data : " & ConvDateToString(date()) &_
			 " -h " & Hour(Now) & ":" & Minute(Now)
	HTML = HTML & " ha coinvolto la figura professionale <B>" & sDen
	HTML = HTML & "</B>, codice istat <B>" & Istat & "</B> e che la relativa mappa capacit�/conoscenza/competenza non " 
	HTML = HTML & "� ancora stata sviluppata."
	HTML = HTML & "<BR><BR> Si prega di volerla procedere allo sviluppo della seddetta mappa."
	HTML = HTML & "<br><br>CORDIALI SALUTI<BR><i>La Redazione di ItaliaLavoro</i> ( <a href=http://www.ItaliaLavoro.it>www.ItaliaLavoro.it</a> )<br><br></br></td></tr></font>"
	HTML = HTML & "</TABLE></div></BODY></HTML>"
	objMail.From = "Il portale del lavoro<portale@italialavoro.it>"
	objMail.To = "e.pili@sdigroup.it"  
'	objMail.To = "CentroRegia@italialavoro.it"  
	objMail.Subject = "Implementazione mappa per figuraprogessionale" 
	'objMail.bcc = "m.anzellotti@sdigroup.it"
	objMail.BodyFormat = 0 
	objMail.MailFormat = 0 
	objMail.Body = HTML 
	objMail.Send 					
	
end sub
%>

<!--#include Virtual ="/Include/closeconn.asp"-->
<!--#include Virtual="/strutt_coda2.asp"-->	
