

function CheckWindow() {
	if (windowArea != null ) {
		windowArea.close(); 
	}
	return true
}


function SiaIn() {
	if (windowArea != null ) {
		windowArea.close(); 
	}
	windowArea = window.open ('STA_NumValori.asp','Info','width=250,height=200,Resize=No,Scrollbars=yes,screenX=w,screenY=h')
}


function GestOpLog() {
	w=(screen.width-(screen.width/2))/2;	
	h=(screen.height-(screen.height/2))/2;

	var sCondizione
	sCondizione = frmInsCriteri.cmbLogica.options[frmInsCriteri.cmbLogica.selectedIndex].value
	if (sCondizione == "08" || sCondizione == "10") {
		if (windowArea != null ) {
			windowArea.close(); 
		}
		Campi(0)
//windowArea = window.open ('RIC_NumValori.asp','Info','width=250,height=200,Resize=No,Scrollbars=yes,screenX=w,screenY=h')
		return true
	}
	if (sCondizione == "09") {
		if (windowArea != null ) {
			windowArea.close(); 
		}
		Campi1(0)
		return true
	}
	else {
		if (windowArea != null ) {
			windowArea.close(); 
		}
		Campi(0)
	}	
//if (document.frmInsCriteri.cmbValori[1] == "[object]")
}


function Campi1(sCompresa) {
	var nSelTab
	var nSelCampo
	var sDecod

	if (windowArea != null ) {
		windowArea.close(); 
	}
	
	nSelCampo	= document.frmInsCriteri.elements[0].selectedIndex
	nOper		= document.frmInsCriteri.elements[1].selectedIndex
	if (sCompresa == "0") {
		document.location.href ="STA_InsCriteriSel.asp?prVolta=No&Campo=" + 
			document.frmInsCriteri.elements[0].options[nSelCampo].value +
			"&OpeLog=" + document.frmInsCriteri.elements[1].options[nOper].value +
			"&IDTAB=" + document.frmInsCriteri.IDTAB.value + 
			"&AREA=DIZ_DATI&SPECIFICO=DIZ_TAB&DESCTAB=" + document.frmInsCriteri.DESCTAB.value + 
			"&IDRIC=" + document.frmInsCriteri.IDRIC.value + 
			"&IDTAB1=" + document.frmInsCriteri.IDTAB1.value +
			"&NumValori=2&IdSede=" + document.frmInsCriteri.IdSede.value
	}
	else {
		document.location.href ="STA_InsCriteriSel.asp?prVolta=No&Campo=" + 
			document.frmInsCriteri.elements[0].options[nSelCampo].value + sCompresa +
			"&OpeLog=" + document.frmInsCriteri.elements[1].options[nOper].value +
			"&IDTAB=" + document.frmInsCriteri.IDTAB.value + 
			"&AREA=DIZ_DATI&SPECIFICO=DIZ_TAB&DESCTAB=" + 
			document.frmInsCriteri.DESCTAB.value +
			"&IDRIC=" + document.frmInsCriteri.IDRIC.value + 
			"&IDTAB1=" + document.frmInsCriteri.IDTAB1.value +
			"&NumValori=2&IdSede=" + document.frmInsCriteri.IdSede.value
	}
}


function ControlloCheck() {
	if (windowArea != null ) {
		windowArea.close(); 
	}
	ok = 0
	i = 0
	while (document.frmCanCriteri.elements[i].type!="checkbox") {
		i++;
	}
	
	t=i
	while (document.frmCanCriteri.elements[i].type=="checkbox") {
		if (document.frmCanCriteri.elements[i].checked) {
			ok = 1
		}
		i = i + 5;
	}

	if (ok == 0) {
		alert ("Devi selezionare almeno un requisito.")
		document.frmCanCriteri.elements[t].focus()
		return false
	}
	else
		return true
}


function ChkAbil() {
	if (document.frmInsCriteri.ckbValore.checked) {
		document.frmInsCriteri.cmbValori.disabled = false;
		document.frmInsCriteri.cmbLogica.disabled = false;
	}
	else {	
		document.frmInsCriteri.cmbValori.disabled = true;
		document.frmInsCriteri.cmbLogica.disabled = true;
	} 
}


function ControlloDatiEspro() {
	if (windowArea != null ) {
		windowArea.close(); 
	}

	if (windowSpecifico != null ) {
		windowSpecifico.close(); 
	}

	if (document.frmInsCriteri.txtTipoSpecifico.value == "") {
		alert("E' obbligatorio indicare tutti i livelli");
		imgPunto3.focus();
		return false;
	} 

	if (document.frmInsCriteri.cmbLogica.value == "") {
		alert("E' obbligatorio indicare una condizione");
		document.frmInsCriteri.cmbLogica.focus();
		return false;
	} 

	if (document.frmInsCriteri.cmbValori.value == "") {
		alert("E' obbligatorio indicare l'anno di anzianit�");
		document.frmInsCriteri.cmbValori.focus();
		return false;
	}
	 
	if (!ControllaDati(document.frmInsCriteri.cmbValori.value,document.frmInsCriteri.txtTipoDato.value))
		return false
		
	return true;
}


function ControlloDati() {
	if (windowArea != null ) {
		windowArea.close(); 
	}

	if (windowSpecifico != null ) {
		windowSpecifico.close(); 
	}

	if (document.frmInsCriteri.IDTAB.value !=  "TISTUD") {
		if (document.frmInsCriteri.txtTipoSpecifico.value == "") {
			alert("E' obbligatorio indicare lo specifico dell'area di interesse!");
			imgPunto2.focus();
			return false;
		} 
	}
	else {
		if (document.frmInsCriteri.txtTipoArea.value == "") {
			alert("E' obbligatorio indicare almeno l'area di interesse");
			imgPunto1.focus();
			return false;
		} 
	}
	
	if (document.frmInsCriteri.txtCampoTab.value != "") {
		if (document.frmInsCriteri.ckbValore.checked == true) {
			if (document.frmInsCriteri.cmbLogica.value == "") {
				alert ("Occorre indicare una condizione!")
				document.frmInsCriteri.cmbLogica.focus()
				return false
			}

			if (document.frmInsCriteri.cmbValori.value == "") {
				alert ("Occorre indicare un valore!")
				document.frmInsCriteri.cmbValori.focus()
				return false
			}
			
			if (document.frmInsCriteri.txtTipoDato.value == "N") {
				if (!IsNum(document.frmInsCriteri.cmbValori.value)) {
					alert("Il valore deve essere numerico!")
					document.frmInsCriteri.cmbValori.focus()
					return false
				}
			} 

			if (document.frmInsCriteri.txtTipoDato.value == "D") {
				if (!ValidateInputDate(document.frmInsCriteri.cmbValori.value)) {
					alert("Il valore deve essere del formato GG/MM/AAAA!")
					document.frmInsCriteri.cmbValori.focus()
					return false
				}
				if (document.frmInsCriteri.cmbValori2.disabled == false) {
					if (!ValidateRangeDate(document.frmInsCriteri.cmbValori.value,document.frmInsCriteri.cmbValori2.value)) {
						alert("Si deve indicare un range valido!")
						document.frmInsCriteri.cmbValori.focus()
						return false
					}
				}
			} 
		}
	}
	return true
}


var windowSpecifico
var windowArea


function SelCodIstat(sArea,sCampo) {
	sOk = true
	if (windowSpecifico != null ) {
		windowSpecifico.close(); 
	}
		
	if (sCampo == "2" && document.frmInsCriteri.txtTipoArea.value == "") {
		alert("Per selezionare la professione si deve indicare prima la categoria!")
		sOk = false
	}		

	if (sCampo == "3" && document.frmInsCriteri.txtTipoProf.value == "") {
		alert("Per scegliere la mansione si deve indicare prima la professione!")
		sOk = false
	}		
	
	switch (sCampo) {
		case "1": 
			sCodice = ""
			break;
		case "2":
			sCodice = document.frmInsCriteri.txtTipoArea.value
			break;
		case "3":
			sCodice = document.frmInsCriteri.txtTipoProf.value
			break;
	}
	
	if (sOk)
	windowArea = window.open ('STA_SelCodIstat.asp?Area=' + sArea + '&sTxtArea=' +
		sCampo + '&codice=' + sCodice ,'Info','width=420,height=450,Resize=No,Scrollbars=yes')
}


function SelArea(sArea) {
	if (windowSpecifico != null ) {
		windowSpecifico.close(); 
	}

	sCampo = document.frmInsCriteri.txtCampoArea.value	
	sTab = document.frmInsCriteri.IDTAB.value	
	windowArea = window.open ('STA_SelArea.asp?Area=' + sArea + '&sTxtArea=' + sCampo + '&sTab=' + sTab ,'Info','width=500,height=450,Resize=No,Scrollbars=yes')
}


function SelSpecifico(sSpecifico) {	
	if (document.frmInsCriteri.txtTipoArea.value != "") {
		sTab = document.frmInsCriteri.IDTAB.value
		sIDArea = document.frmInsCriteri.txtTipoArea.value
		sCampoSpec = document.frmInsCriteri.txtCampoSpecifico.value
		sCampoArea = document.frmInsCriteri.txtCampoArea.value
		windowSpecifico = window.open ('STA_SelSpecifico.asp?Specifico=' + sSpecifico + '&Area=' + sIDArea + '&CampoSpec=' + sCampoSpec + '&sTab=' + sTab + '&sCampoArea=' + sCampoArea,'Info','width=500,height=450,Resize=No,Scrollbars=yes')
	}
	else {
		alert("Prima di scegliere lo specifico si deve indicare l'area di interesse!")
	}
}


//------------------------////------------------------//
function Seleziona(nElem) {
	nSelRic	= document.frmInsCriteri.elements[nElem].selectedIndex
	sSelRic = document.frmInsCriteri.elements[nElem].options[nSelRic].value
	document.frmInsCriteri.cmbValori.value = sSelRic
	sSelRic = "&Valore=" + sSelRic
	Campi(sSelRic)
}


function Ric() {
//alert (frmInsCriteri.txtDecod.value)
	Campi("&Risultato=" + frmInsCriteri.txtDecod.value + "&Decod=YES")
}


function RicCod() {
	Campi("&Decod=YES")
}


function Diverso() {
//Mi prendo l'operatore logico [Maggiore,Minore,Uguale,Diverso]
	nOperLoc = document.frmInsCriteri.elements[2].selectedIndex
	sOperLoc = document.frmInsCriteri.elements[2].options[nOperLoc].value

	if (sOperLoc == "BETWEEN") {
//alert ("ricarico la pagina con due campi per le date")
		Campi("Yes")
	}
	else 
		Campi("No")
}
//------------------------////------------------------//


// Controlla il tipo di dato
function ControllaDati(sValore,sTipo) {
	if (sValore == "") {
		return false
	}
	
	if (sTipo == "N") {
		if (!IsNum(sValore)) {
			alert("Il campo deve essere numerico!")
			return false
		}		 
	}

	if (sTipo == "D") {
		if (!ValidateInputDate(sValore)) {
			return false
		}
	}
	return true
}	


//------------------------////------------------------//
function Canc() {
	opener.document.frmModCriterio.txtRegola.value = ""
	opener.document.frmModCriterio.txtTabRif.value = ""
}


function Uscita() {
	this.close()
}


function selTabella() {
	var nSel
	nSel = document.frmInsCriteri.elements[0].selectedIndex
	nOper = document.frmInsCriteri.elements[2].selectedIndex

	document.location.href ="CRR_CreaRegola.asp?prVolta=No&Tabella=" + 
		document.frmInsCriteri.elements[0].options[nSel].value + "&OpeLog=" +
		document.frmInsCriteri.elements[2].options[nOper].value
}
//------------------------////------------------------//


function Campi(sCompresa) {
	var nSelTab
	var nSelCampo
	var sDecod

	if (windowArea != null ) {
		windowArea.close(); 
	}

//	nSelTab		= document.frmInsCriteri.elements[0].selectedIndex
//	nSelCampo	= document.frmInsCriteri.elements[1].selectedIndex
//	nOper		= document.frmInsCriteri.elements[2].selectedIndex
	
	nSelCampo	= document.frmInsCriteri.elements[0].selectedIndex
	nOper		= document.frmInsCriteri.elements[1].selectedIndex

	if (sCompresa == "0") {
		document.location.href ="STA_InsCriteriSel.asp?prVolta=No&Campo=" + 
			document.frmInsCriteri.elements[0].options[nSelCampo].value +
			"&OpeLog=" + document.frmInsCriteri.elements[1].options[nOper].value +
			"&IDTAB=" + document.frmInsCriteri.IDTAB.value + "&AREA=DIZ_DATI&SPECIFICO=DIZ_TAB&DESCTAB=" + document.frmInsCriteri.DESCTAB.value + 
			"&IDRIC=" + document.frmInsCriteri.IDRIC.value + "&IDTAB1=" + document.frmInsCriteri.IDTAB1.value +
			"&IdSede=" + document.frmInsCriteri.IdSede.value
	}
	else {
		document.location.href ="STA_InsCriteriSel.asp?prVolta=No&Campo=" + 
			document.frmInsCriteri.elements[0].options[nSelCampo].value + sCompresa +
			"&OpeLog=" + document.frmInsCriteri.elements[1].options[nOper].value +
			"&IDTAB=" + document.frmInsCriteri.IDTAB.value + "&AREA=DIZ_DATI&SPECIFICO=DIZ_TAB&DESCTAB=" + document.frmInsCriteri.DESCTAB.value +
			"&IDRIC=" + document.frmInsCriteri.IDRIC.value + "&IDTAB1=" + document.frmInsCriteri.IDTAB1.value +
			"&IdSede=" + document.frmInsCriteri.IdSede.value
	}
}


function Condizione() {

	var nSelCampo
	var nOperLoc
	var sNomCampo
	var sOperLoc
	var sValore
	var nSelTab	
	var sSelTab 

	var sTabRif
	var arNomTab = new Array()

	if (windowArea != null ) {
		windowArea.close(); 
	}
	
	sTipoCampo = document.frmInsCriteri.txtTipo.value

	if (isNaN(document.frmInsCriteri.cmbValori.length)) {
		if (document.frmInsCriteri.cmbValori.value == "") {
			alert("Occorre indicare un valore di condizione!")
			document.frmInsCriteri.cmbValori.focus()
			return false
		}
		else {
			if (document.frmInsCriteri.txtCodReg.value != "")
				sValue = document.frmInsCriteri.txtCodReg.value
			else
				sValue = document.frmInsCriteri.cmbValori.value
			
			if (!ControllaDati(sValue,sTipoCampo)){
				document.frmInsCriteri.cmbValori.focus()
				return false
			}
		}
	} 
	else {
		for(q=0;q<document.frmInsCriteri.cmbValori.length;q++) {
			if (document.frmInsCriteri.cmbValori[q].value == ""){
				alert("Occorre indicare un valore di condizione!")
				document.frmInsCriteri.cmbValori[q].focus()
				return false
			}
			else{
				/* ==> LC. 25/07/2003: commentato controllo e modificato come sotto <== */
				/* ==> if (!ControllaDati(document.frmInsCriteri.cmbValori[q].value,sTipoCampo)){ 
							document.frmInsCriteri.cmbValori[q].focus()
							return false
				} <==*/
				if (sTipoCampo == "N"){
					if (!ControllaDati(document.frmInsCriteri.txtCodReg[q].value,sTipoCampo)){
						document.frmInsCriteri.cmbValori[q].focus()
						return false
					}
				}
				else{
					if (!ControllaDati(document.frmInsCriteri.cmbValori[q].value,sTipoCampo)){
						document.frmInsCriteri.cmbValori[q].focus()
						return false
					}
				}
			}
		}
	}
	
// Mi prendo della tabella
//	nSelTab	= document.frmInsCriteri.elements[0].selectedIndex
//	sSelTab = document.frmInsCriteri.elements[0].options[nSelTab].value
	
// Mi prendo il campo della tabella
	nSelCampo = document.frmInsCriteri.elements[0].selectedIndex
	sNomCampo = document.frmInsCriteri.elements[0].options[nSelCampo].value
	
// Mi prendo l'operatore logico [Maggiore,Minore,Uguale,Diverso]
	nOperLoc = document.frmInsCriteri.elements[1].selectedIndex
	sOperLoc = document.frmInsCriteri.elements[1].options[nOperLoc].value

	if (sOperLoc == "") {
		alert ("Occorre indicare una condizione!")
		document.frmInsCriteri.elements[1].focus()
		return false
	}

	if (sNomCampo == "") {
		alert ("Occorre indicare il nome del campo!")
		document.frmInsCriteri.elements[0].focus()
		return false
	}

//	Mi prendo l'operatore logico [AND,OR]
//	nOperAndOr = document.frmInsCriteri.elements[3].selectedIndex
//	sOperAndOr = " " + document.frmInsCriteri.elements[3].options[nOperAndOr].value
	
// Devo controllare che i dati indicati siano corretti.
// Se il campo � una data occorre controllare che sia stata indicata una data
// Se il campo � un numero occorre controllare la validit�.
// Se non si indica nulla occorre avvertire l'utente che deve indicare un valore

	return true
}