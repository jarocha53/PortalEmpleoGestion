<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include virtual="/util/portallib.asp"-->
<!--#include virtual="/strutt_testa2.asp"-->
<!--#include virtual="/include/openconn.asp"-->
<!--#include virtual ="/include/SelAreaTerrBandi.asp"-->
<!--#include virtual ="/include/ckProfile.asp"-->
<!--#include virtual ="/include/DecCod.asp"-->
<!--#include virtual ="/include/DecComun.asp"-->
<%
	if ValidateService(session("idutente"),"STA_VISIMPRESE",cc) <> "true" then  
		response.redirect "/util/error_login.asp"
	end if

'paginazione
Dim Record, nIndElem
Dim nActPagina
Dim nTotPagina
Dim nTamPagina		
Dim nTotRecord
 
nTamPagina= 10
If Request("pagina")= "" Then
	nActPagina=1
Else
	nActPagina=Clng(Request("pagina"))
End If
	

%>
<script LANGUAGE="Javascript">
//include del file per fare i controlli sulla numericit� dei campi
<!--#include virtual = "/Include/ControlNum.inc"-->
<!--#include virtual="/Include/help.inc"-->
<!-- #Include Virtual="/Include/ControlString.inc" -->


//Inizio AM 09/10
function pulisci (){
	document.frmVisImpresa.cmbProv.value = ""
	document.frmVisImpresa.txtRagSoc.value = ""
	document.frmVisImpresa.txtPartIva.value = ""
	document.frmVisImpresa.txtCodFiscale.value = ""
}
//Fine AM 09/10

//paginazione
function AssegnaVal(val){
	document.FrmPagina.pagina.value = val;
	document.FrmPagina.submit()
}

function ControllaDati(frmVisImpresa) {
	
	if (frmVisImpresa.cmbProv.value.length == "") 
		{
			alert("Il campo Provincia � obbligatorio")
			frmVisImpresa.cmbProv.focus() 
			return false
		}
		
	frmVisImpresa.txtRagSoc.value=TRIM(frmVisImpresa.txtRagSoc.value)
	if 	(!ValidateInputStringWithNumber(frmVisImpresa.txtRagSoc.value)) {
		alert("Il campo  Ragione sociale  � formalmente errato")
		frmVisImpresa.txtRagSoc.focus();
		return false;
	}
		
	if (frmVisImpresa.txtPartIva.value.length > 0 )	{ 
		if (!IsNum(frmVisImpresa.txtPartIva.value)) {
			alert("Il campo Partita Iva deve essere numerico")
			frmVisImpresa.txtPartIva.focus() 
			return false
		}
		if (frmVisImpresa.txtPartIva.value.length != 11) 
		{
			alert("Il campo Partita Iva deve essere di 11 caratteri")
			frmVisImpresa.txtPartIva.focus() 
			return false
		}
	}
		
	frmVisImpresa.txtCodFiscale.value=TRIM(frmVisImpresa.txtCodFiscale.value)
	if 	(frmVisImpresa.txtCodFiscale.value.length > 0 )	{
		if 	(!ValidateInputStringWithNumber(frmVisImpresa.txtCodFiscale.value))	{
			alert("Il campo Codice Fiscale � formalmente errato")
			frmVisImpresa.txtCodFiscale.focus();
			return false;
		}
		blank = " ";
		if (!ChechSingolChar(frmVisImpresa.txtCodFiscale.value,blank))	{
			alert("Il campo Codice Fiscale non pu� contenere spazi bianchi")
			frmVisImpresa.txtCodFiscale.focus();
			return false;
		}
		if ((frmVisImpresa.txtCodFiscale.value.length != 11) && (frmVisImpresa.txtCodFiscale.value.length != 16)) {
			alert("Il codice Fiscale  deve essere di 11  o 16 caratteri")
			frmVisImpresa.txtCodFiscale.focus(); 
			return false;
		}
	}
			
	document.frmVisImpresa.prVolta.value="No";
	return true;
}


function HideVariables(sUrl,idForm) {
	document.forms.item('frmRicAzien' + idForm).action = sUrl		
	document.forms.item('frmRicAzien' + idForm).submit();
}
	
function userfocus() {
	document.frmVisImpresa.txtRagSoc.focus()
}

</script>
</head>

<body background="../../../images/sfondo1.gif" onload="return userfocus()">
<%
	'on error resume next 
	dim CnConn
	dim rsAziende
	dim sSQL
	dim sIsa
	dim sPrVolta
	dim sDescrizione, sDescrizioneApp, sPROV
	dim sPartIva
	dim sCodFiscale
	dim i
	dim nMaxElem
	dim sMaskProv
	dim nCont
	
	sDescrizioneApp= ucase(Request("txtRagSoc"))
	sDescrizione= Replace(sDescrizioneApp,"'","''")
	sPartIva = Request("txtPartIva")
	sCodFiscale = ucase(Request("txtCodFiscale"))
	sPROV = Request("cmbProv")
	sPrVolta = request("prVolta")
%>
<table border="0" width="520" cellspacing="0" cellpadding="0" height="81">
   <tr>
     <td width="520" background="<%=Session("Progetto")%>/images/titoli/strumenti2b.gif" height="81" valign="bottom" align="right">
       <table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
         <tr>
           <td width="100%" valign="top" align="right"><b class="tbltext1a">Definizione Stage &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></b></td>
         </tr>
       </table>
     </td>
   </tr>
</table><br>
<table cellpadding="0" cellspacing="0" width="500" border="0" align="center">
   <tr height="18">
		<td class="sfondomenu" height="18" width="67%">
		<span class="tbltext0"><b>&nbsp;RICERCA AZIENDA RICHIEDENTI</b></span></td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
		<td valign="middle" align="right" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1">(*)campo obbligatorio
		</td>
   </tr>
   <tr width="371" class="SFONDOCOMM">
		<td colspan="3">
			Indicare i dati per la ricerca delle sedi che effettuano Stage. <br>
			Dopo aver effettuato la ricerca, selezionare l'azienda che si desidera <br>per visionare 
			le possibili proposte di stage.
			<a href="Javascript:Show_Help('/Pgm/help/GestProgetti/Stage/Stage_Definizione/STA_VisImprese')">
				<img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0">
			</a>
		</td>
	</tr>
	<tr height="2">
		<td colspan="3" class="SFONDOCOMMAZ" background="<%=Session("Progetto")%>/images/separazione.gif">
		</td>
	</tr>
</table>
<br>
<%
	sMaskProv= SetProvUorg(session("idUorg"))
%>
<form method="post" name="frmVisImpresa" onsubmit="return ControllaDati(this)" action="STA_VisImprese.asp">
<input type="hidden" name="prVolta" value>
<table width="500" border="0" cellspacing="1" cellpadding="0">
	
	<tr height="17">
		<td width="150" class="tbltext1">
			<b>Provincia*</b>
		</td>
		<td width="350" class="tbltext1">
			<b>
	
				<select NAME="cmbProv" class="textblack">			 
					<option>&nbsp;</option>
		<%			nProvSedi = len(sMaskProv)
					if nProvSedi <> 0 then
						pos_ini = 1 	                
						for i=1 to nProvSedi/2
							sTarga = mid(sMaskProv,pos_ini,2)
														
							Response.Write	"<OPTION "
							Response.write	"value ='" & sTarga & "'"
							if sProv = sTarga then
								Response.Write " selected"
							end if
							Response.write  "> " & DecCodVal("PROV", 0, "", sTarga,"") & "</OPTION>"

							pos_ini = pos_ini + 2
						next
					end if
		%>
				</select>
	</td>
	</tr> 
			
	<tr align="left"> 
		<td nowrap width="75" class="tbltext1">
			<b>Rag. Sociale</b>
		</td>
		
		<td nowrap width="200" align="middle" class="tbltext">
			<span class="size">
<!--INIZIO 09/10  -->
			<%
			IF sDescrizione <> "" THEN %>
				<input style="TEXT-TRANSFORM: uppercase; WIDTH:254px;" name="txtRagSoc" class="textblack" size="32" maxlength="50" value="<%=sDescrizioneApp%>">
			<%
			else
			%>
				<input style="TEXT-TRANSFORM: uppercase; WIDTH:254px;" name="txtRagSoc" class="textblack" size="32" maxlength="50">
			<%
			END IF	
			%>
<!--FINE 09/10 -->
			</span>
		</td>
	</tr>	
	<tr align="left">
		<td nowrap width="75" class="tbltext1">
			<b>Part.Iva</b>
		</td>
		<td nowrap width="200" align="middle" class="tbltext">
			<span class="size">
			<%
			if 	sPartIva <> "" then %>
				<input style="TEXT-TRANSFORM: uppercase; WIDTH: 254px;" name="txtPartIva" class="textblack" size="32" value="<%=sPartIva%>">
			<%
			else
			%>
				<input style="TEXT-TRANSFORM: uppercase; WIDTH: 254px;" name="txtPartIva" class="textblack" size="32" maxlength="11">
			<%end if%>
			</span>
		</td>
	</tr>
	
	<tr align="left">
		<td nowrap width="75" class="tbltext1">
			<b>Cod.Fiscale</b>
		</td>
		<td nowrap width="200" align="middle" class="tbltext">
			<span class="size">
			<%
			if 	sCodFiscale <> "" then %>
				<input style="TEXT-TRANSFORM: uppercase; WIDTH: 254px;" name="txtCodFiscale" class="textblack" size="32" value="<%=sCodFiscale%>">
			<%
			else
			%>
				<input style="TEXT-TRANSFORM: uppercase; WIDTH: 254px;" name="txtCodFiscale" class="textblack" size="32" maxlength="16">
			<%end if%>
			</span>
		</td>
	</tr>
</table>
<br>

<table cellpadding="0" cellspacing="0" width="300" border="0">
	<tr align="center">
		<td nowrap>
			<input type="image" name="Conferma" src="<%=Session("progetto")%>/images/lente.gif">
		</td value="Registra">
		<td nowrap>
			<!--a href="javascript:document.frmVisImpresa.reset()"-->
			<a href="javascript:pulisci()">
			<img border="0" src="<%=Session("progetto")%>/images/annulla.gif" onLoad="return userfocus()"></a>
		</td>			
	</tr>
</table>
</form>
	<%
	nCont = 0
	n = 0
'AM 12/09 Modificato SQL inserendo campo cod_cproj deLla tabella impresa_proj E FACENDO CONTROLLO  ID_IMPRESA 
		set rsAziende = server.CreateObject ("ADODB.Recordset")
		sSQL = "SELECT  I.ID_IMPRESA, I.RAG_SOC, I.PART_IVA, I.COD_FISC, SI.DESCRIZIONE, SI.COD_SEDE, SI.COMUNE, SI.ID_SEDE, SI.PRV " &_
				"FROM IMPRESA I, SEDE_IMPRESA SI, SEDE_PROJ SP" &_
				" WHERE UPPER(RAG_SOC) Like '%" & UCASE(sDescrizione) & "%' and si.id_impresa = i.id_impresa " &_
		        " AND SP.id_impresa = i.id_impresa " &_
   		        " AND SP.ID_SEDE = SI.ID_SEDE " &_
		        " AND  SP.COD_CPROJ= '" & MID(SESSION("Progetto"),2) & "'" &_
		        " and PRV ='" & sPROV & "'"
		
			
		if sPartIva <> "" then
			sSQL = sSQL + " AND PART_IVA='" & sPartIva & "'"
		end if
		
		if sCodFiscale <> "" then
			sSQL = sSQL + " AND COD_FISC ='" & sCodFiscale & "'"
		end if
		
		sSQL = sSQL + " ORDER BY RAG_SOC "
		'Response.Write " sSQL " & sSql		
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
		rsAziende.Open sSQL, CC, 3
	if not rsAziende.Eof then
			rsAziende.PageSize = nTamPagina
			rsAziende.CacheSize = nTamPagina
			nTotPagina = rsAziende.PageCount
			If nActPagina < 1 Then
				nActPagina = 1
			End If
			If nActPagina > nTotPagina Then
				nActPagina = nTotPagina
			End If
			rsAziende.AbsolutePage= nActPagina
			nTotRecord=0
			
	%>
	<table width="500" cellpadding="1" cellspacing="2" border="0">
		<tr height="20" class="tblsfondo">			
			<td align="middle" valign="center" width="320" class="tbltext1"><b>Denominazione Azienda</b></td>
			<!--td align="middle" valign="center" width="130" class="tbltext1"><b>Part.Iva</b></td-->
			<td align="middle" valign="center" width="130" class="tbltext1"><b>Tipo sede</b></td>
			<td align="middle" valign="center" width="180" class="tbltext1"><b>&nbsp;Comune</b></td>
		</tr>
<%
		n = 1
		while  rsAziende.EOF <> True And nTotRecord < nTamPagina
%>
		<form name="frmRicAzien<%=n%>" method="post">
			<input type="hidden" id="txtRagSoc" name="txtRagSoc" value="<%=rsAziende("RAG_SOC")%>">
			<input type="hidden" id="txtIdImpresa" name="txtIdImpresa" value="<%=rsAziende("ID_IMPRESA")%>">
			<input type="hidden" id="txtSede" name="txtSede" value="<%=rsAziende("ID_SEDE")%>">
			<input type="hidden" id="txtDesc" name="txtDesc" value="<%=rsAziende("DESCRIZIONE")%>">
		  <tr class="tblsfondo"> 
			  <td align="LEFT" width="100">
					<!--span class="tbltext1"-->
					<%
						Response.write "<A class=""tblAgg""   onmouseover=""javascript: window.status=' '; return true""   "
						Response.Write "HREF="" javascript: HideVariables('STA_RicStage.asp','" & n & "')"">"
						Response.write  rsAziende("RAG_SOC")  & " - "
						Response.Write (rsAziende("DESCRIZIONE"))& "</A>"
					%>
					<!--/span-->
				</td>
				<!--inizio AM 19/09	td align="middle" width="130">						<span class="tbltext">						<%'Response.Write (rsAziende("PART_IVA")) %>					</span>								</td-->
				<!--td align="middle" width="130">					<span class="tbltext">											<%'Response.Write (rsAziende("COD_FISC"))%>										</span>								</td-->
				<td class="tbldett" width="130">
				<%
					Response.Write DecCodVal("TSEDE","0",date,rsAziende("COD_SEDE"),"1") 
				%> 
				</td>
				<!--Fine Am 19/09 -->
				<td width="270">
					<span class="tbldett">
					<%Response.Write DescrComune(rsAziende("COMUNE")) & "&nbsp;&nbsp;(" & rsAziende("PRV") & ")" %>
					</span>
				</td>
			</tr>
		</form>
		<%
				n= n +1
				nTotRecord=nTotRecord + 1
				rsAziende.MoveNext
		wend
	%>
	</table>
	<br>
	<!--PAGINAZIONE-->
	<form name="FrmPagina" method="post" action="STA_VisImprese.asp">
		<input type="hidden" id="pagina" name="pagina" value="<%=nActPagina%>">
		<input type="hidden" id="cmbProv" name="cmbProv" value="<%=sProv%>">
		<input type="hidden" id="txtRagSoc" name="txtRagSoc" value="<%=RAG_SOC%>">
		<input type="hidden" id="txtPartIva" name="txtPartIva" value="<%=PART_IVA%>">
		<input type="hidden" id="txtCodFiscale" name="txtCodFiscale" value="<%=COD_FISC%>">

		<table border="0" width="500">
			<tr>
			<%
			if nActPagina > 1 then%>		
				<td align="right" width="480">
					<a href="Javascript:AssegnaVal('<%=(nActPagina - 1)%>')">
						<img src="<%=Session("Progetto")%>/images/precedente.gif" border="0" id="image1" name="image1" alt="tasto per pagina precedente" title="tasto per pagina precedente">
					</a>
				</td>
			<%
			end if
			if nActPagina < nTotPagina then%>
				<td align="right">
					<a href="Javascript:AssegnaVal('<%=(nActPagina + 1)%>')">
						<img src="<%=Session("Progetto")%>/images/successivo.gif" border="0" id="image1" name="image1" alt="tasto per pagina successiva" title="tasto per pagina successiva">
					</a>
				</td>
			<%
			end if%>		
			</tr>
		</table>
	</form>
	<%
		else
			 if sPrVolta = "No" then
				  ' nCont = 0 
			%>
					<table width="371" cellpadding="0" cellspacing="0" border="0">
						<tr align="middle">
							<td>
								<span class="tbltext3">
								<% 
									response.write ("<STRONG>Non sono state individuate Aziende</STRONG>")
								%>
								</span>

							</td>
						</tr>
					</table> 
			<%
				end if
		end if
	rsAziende.Close 
	set rsAziende = nothing
		
%> 
		
<!--#include virtual ="/strutt_coda2.asp"-->
<!--#include virtual ="/include/closeconn.asp"-->

