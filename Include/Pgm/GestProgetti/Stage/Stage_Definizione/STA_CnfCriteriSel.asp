<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include Virtual = "/strutt_testa2.asp"-->
<!--#include Virtual = "/util/dbutil.asp"-->
<!--#include Virtual = "/include/Openconn.asp"-->
<!--#include Virtual = "/include/SysFunction.asp"-->
<!--#include Virtual = "/Include/decCod.asp"-->
<!--#include virtual = "/util/portallib.asp"-->

<%
dim sCondizione, sTab, sOpLog
dim sVal1, sVal2 
dim sCodArea, sCodSpec
dim nIdRic, sIdSede, nCheck2, nCheck, n
dim sCampoArea, sCampoSpec
dim sAction
dim aOpLog
DIM rsCheckRic, rsMaxPrgReg, rsPrgRegRif, RR1

sIdSede		= Request("IdSede")
sArea		= Request.Form("AREA")
sSpecifico	= Request.Form("SPECIFICO")
sCond		= Request.Form("CONDIZIONE")
sCondizione = Request.Form("txtCampoTab")
sTab		= Request.Form("IDTAB")
sOpLog		= Request.Form("cmbLogica")
sAction		= Request.QueryString("Action")


if  trim(replace(Request.Form("txtCodReg"),",","")) = "" then
	sNumValori	= Request.Form("cmbValori").Count
	sAppoggio	= Request.Form("cmbValori")
else
	sNumValori	= Request.Form("txtCodReg").Count
	sAppoggio	= Request.Form("txtCodReg")
end if

if sNumValori > 1 then
	sValue		= split(sAppoggio,",")
else
	sValue		= sAppoggio
end if

sTipoCampo = Request.Form("txtTipo")

if IsArray (sValue) then
	sAppoggio = ""
	if sTipoCampo <> "D" then
		for i = 0 to ubound(sValue)
			sValue(i) = replace(sValue(i),"'","''")
			if i < ubound(sValue) then
				sAppoggio = sAppoggio & trasforma(trim(sValue(i)),sTipoCampo) & ","
			else
				sAppoggio = sAppoggio & trasforma(trim(sValue(i)),sTipoCampo) 
			end if
		next
	else
		for i = 0 to ubound(sValue)
			if i < ubound(sValue) then
				sAppoggio = sAppoggio & sValue(i) & ","
			else
				sAppoggio = sAppoggio & sValue(i) 
			end if
		next
	end if
	sVal1 = sAppoggio
	sVal1 = replace(sAppoggio,"'","$")
else
''''''LC 25/07/2003: applicato <TRASFORMA> e aggiunto controllo su sTipoCampo
	'''sVal1 = replace(sAppoggio,"'","$")
	if sOpLog = "08" or sOpLog = "10" then
		sVal1 = replace(trasforma(sAppoggio, sTipoCampo),"'","$")
	else
		sVal1 = replace(sAppoggio,"'","$")
	end if
''''''FINE LC.
end if

'sVal1		= replace(ucase(Request.Form("cmbValori1")),"'","''")
'sVal2		= replace(ucase(Request.Form("cmbValori2")),"'","''")

sCodArea	= Request.Form("txtTipoArea") 
sCodSpec	= Request.Form("txtTipoSpecifico")
' *************************************************
' Per Vecchia struttura della tabella PERS_CONOSC
 sCampoArea	= Request.Form("txtCampoArea")
' *************************************************
' Per nuova struttura della tabella PERS_CONOSC
' sCampoArea	= Request.Form("txtCampoSpecifico")
' *************************************************
sCampoSpec	= Request.Form("txtCampoSpecifico")
nIdRic		= Request.Form("txtIDRic")

aOpLog = decodTadesToArray("OPLOG",date,"VALORE = '='",1,0)

if 	sValCod <> "" then
	sValore = sValCod
end if 

'Si effettuano le select count solo se si devono inserire occorrenze sul DB
select case sAction 
	case "INS" 
			if sTab = "VARIE" then
				sTab1 = Request.Form("IDTAB1")
				sSQL =	"SELECT COUNT(*) as REC FROM REGOLE_STAGE WHERE ID_TAB='" & sTab1 &_
						"' AND ID_CAMPO='" & sCodArea & "' AND ID_RICHSTAGE =" & clng(nIdRic) &_
						" AND PRG_REGOLA_RIF=0" &_
						" And cod_oplog= '" & sOplog & "'" &_
						" And valore = '" & sVal1 & "'"
				nCheck2 = 1
			elseif sCodSpec <> "" then
				sSQL =	"SELECT COUNT(*) as REC FROM REGOLE_STAGE WHERE ID_TAB='" & sTab &_
						"' AND ID_RICHSTAGE =" & clng(nIdRic) & " AND PRG_REGOLA_RIF=0" &_
						" And cod_oplog= '" & aOpLog(0,0,0) & "'" &_
						" And id_campo = '" & sCampoSpec & "'" &_
						" And valore = " & Trasforma(sCodSpec,sTipoCampo)
			else
				sSQL =	"SELECT COUNT(*) as REC FROM REGOLE_STAGE WHERE ID_TAB='" & sTab &_
				   		"' AND ID_RICHSTAGE =" & clng(nIdRic) & " AND PRG_REGOLA_RIF=0" &_
				   		" And cod_oplog= '" & aOpLog(0,0,0) & "'" &_
				   		" And id_campo = '" & sCampoArea & "'" &_
				   		" And valore = " & Trasforma(sCodArea,sTipoCampo)
			end if
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
			set rsCheckRic = CC.Execute(sSQL)
				nCheck = clng(rsCheckRic("REC"))
			rsCheckRic.Close
					
			if sCondizione <> "" then
				sSQL = "SELECT COUNT(*) as REC FROM REGOLE_STAGE WHERE ID_TAB='" & sTab &_
					 "' AND ID_RICHSTAGE =" & clng(nIdRic) & " AND ID_CAMPO='" & sCondizione & "'" &_
					 " And id_campo = '" & sCampoArea & "'" &_
					 " And valore = " & Trasforma(sCodArea,sTipoCampo)
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
				set rsCheckRic = CC.Execute(sSQL)
					nCheck2 = clng(rsCheckRic("REC"))
				rsCheckRic.Close
			end if  

	case "CAN"
			nCheck = 0
	end select

Errore = "0"
CC.BeginTrans

if (nCheck = 0) or (nCheck2 = 0 and sOpLog <> "") then
	SELECT CASE sAction 
		CASE "INS" 
			select case sTab
				case "VARIE"
					sTab1 = Request.Form("IDTAB1") 
					if nCheck = "0" then
						sSQL = "SELECT MAX(PRG_REGOLA) AS MAX FROM REGOLE_STAGE WHERE ID_RICHSTAGE=" & clng(nIdRic) 
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
						set rsMaxPrgReg = CC.Execute(sSQL)
							if isnull(rsMaxPrgReg("MAX")) then
								nMax = 1
							else					
								nMax = (CLng(rsMaxPrgReg("MAX")) +1)
							end if
						rsMaxPrgReg.Close
						set rsMaxPrgReg = nothing

						sSQL =	"INSERT INTO REGOLE_STAGE" &_
								" (ID_RICHSTAGE,ID_TAB,ID_CAMPO,COD_OPLOG,VALORE,PRG_REGOLA,PRG_REGOLA_RIF,DT_TMST)" &_
								" VALUES(" & clng(nIdRic) & ",'" & sTab1 & "','" &_
								sCodArea & "','"& sOpLog & "','" & UCASE(sVal1) & "'," & nMax & ",0," & ConvDateToDB(Now())& ")"
					end if 
					Errore = EseguiNoC(sSQL,CC)
					'''LC 22/7/2003: a che serve impostare manualmente Errore a "0" in questo punto???
					'''Errore = "0"
				
				case "TISTUD","PERS_VINCOLI","PERS_DISPON"
					' Sono nella costruzione della regola per Titolo di Studio.
					' Per cui devo prima di tutto controllare se � stato indicato lo specifico
					if sCodSpec <> "" then
						sValoreRif = Trasforma(sCodSpec,sTipoCampo)
						sIdCampoRif = sCampoSpec	
					else
						sValoreRif = Trasforma(sCodArea,sTipoCampo)
						sIdCampoRif = sCampoArea
					end if
					
					if nCheck = "0" then
						sSQL = "SELECT MAX(PRG_REGOLA) AS MAX FROM REGOLE_STAGE WHERE ID_RICHSTAGE=" & clng(nIdRic) 
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
						set rsMaxPrgReg = CC.Execute(sSQL)
							if isnull(rsMaxPrgReg("MAX")) then
								nMax = 1
							else					
								nMax = (CLng(rsMaxPrgReg("MAX")) +1)
							end if
						rsMaxPrgReg.Close
						set rsMaxPrgReg = nothing
						if sCodSpec <> "" then
							sSQL =	"INSERT INTO REGOLE_STAGE" &_
									" (ID_RICHSTAGE,ID_TAB,ID_CAMPO,COD_OPLOG,VALORE,PRG_REGOLA,PRG_REGOLA_RIF,DT_TMST)" &_
									" VALUES(" & clng(nIdRic) & ",'" & sTab & "','" &_
									sCampoSpec & "','"& aOpLog(0,0,0) & "'," & Trasforma(sCodSpec,sTipoCampo) & "," & nMax & ",0," & ConvDateToDB(Now())& ")"
						else
							sSQL =	"INSERT INTO REGOLE_STAGE" &_
									" (ID_RICHSTAGE,ID_TAB,ID_CAMPO,COD_OPLOG,VALORE,PRG_REGOLA,PRG_REGOLA_RIF,DT_TMST)" &_
									" VALUES(" & clng(nIdRic) & ",'" & sTab & "','" &_
									sCampoArea & "','"& aOpLog(0,0,0) & "'," & Trasforma(sCodArea,sTipoCampo) & "," & nMax & ",0," & ConvDateToDB(Now())& ")"
						end if	
					end if 
					Errore = EseguiNoC(sSQL,CC)
					
					if Errore = "0" then
						if sOpLog <> "" then	
							' Vuol dire che sono state assegnati dei valori di condizione.
							sSQL = "SELECT MAX(PRG_REGOLA) AS MAX FROM REGOLE_STAGE WHERE ID_RICHSTAGE=" & clng(nIdRic) 
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
							set rsMaxPrgReg = CC.Execute(sSQL)
								if isnull(rsMaxPrgReg("MAX")) then
									nMax = 1
								else					
									nMax = CLng(rsMaxPrgReg("MAX"))
								end if
							rsMaxPrgReg.Close
							set rsMaxPrgReg = nothing
							
							sSQL =	"SELECT PRG_REGOLA FROM REGOLE_STAGE WHERE ID_RICHSTAGE=" &_
									clng(nIdRic) & " AND PRG_REGOLA_RIF = 0 AND ID_TAB = '" & sTab & "'" &_
									" AND ID_CAMPO ='" & sIdCampoRif & "'" &_
									" AND VALORE = " & sValoreRif 
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
							set rsPrgRegRif = CC.Execute(sSQL)
								if isnull(rsPrgRegRif("PRG_REGOLA")) then
									 nRegRif = 1
								else					
									 nRegRif = CLng(rsPrgRegRif("PRG_REGOLA"))
								end if
							rsPrgRegRif.Close
							set rsPrgRegRif = nothing
							
							if sVal2 <> "" then 
							' Sicuramente e' una between
							' La between la utilizziamo solo per le date 
								sSQL =	"INSERT INTO REGOLE_STAGE" &_
										" (ID_RICHSTAGE,ID_TAB,ID_CAMPO,COD_OPLOG,VALORE,PRG_REGOLA,PRG_REGOLA_RIF,DT_TMST)" &_
										" VALUES(" & clng(nIdRic) & ",'" & sTab & "','" &_
										 sCondizione & "','" & sOpLog & "'," & Trasforma(sVal1,sTipoCampo) & ",'" & sVal2 & "'," & CLng(nMax+1) & "," & nRegRif & "," & ConvDateToDB(Now())& ")"
							else
								sSQL =	"INSERT INTO REGOLE_STAGE" &_
										" (ID_RICHSTAGE,ID_TAB,ID_CAMPO,COD_OPLOG,VALORE,PRG_REGOLA,PRG_REGOLA_RIF,DT_TMST)" &_
										" VALUES(" & clng(nIdRic) & ",'" & sTab & "','" &_
										sCondizione & "','" & sOpLog & "'," & Trasforma(sVal1,sTipoCampo) & "," & CLng(nMax+1) & "," & nRegRif & "," & ConvDateToDB(Now())& ")"
							end if
							Errore = EseguiNoC(sSQL,CC)
						end if			
					end if 
					sMessaggio = "OK"
				
				case "ESPRO"
					'Per nuova struttura della tabella...
					'sCampoArea	= Request.Form("txtCampoSpecifico")

					if sCodSpec <> "" then
						sValoreRif = Trasforma(sCodSpec,sTipoCampo)
					else
						sValoreRif = Trasforma(sCodArea,sTipoCampo)
					end if
				
					if nCheck = "0" then
						sSQL = "SELECT MAX(PRG_REGOLA) AS MAX FROM REGOLE_STAGE WHERE ID_RICHSTAGE=" & clng(nIdRic) 
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
						set rsMaxPrgReg = CC.Execute(sSQL)
							if isnull(rsMaxPrgReg("MAX")) then
								nMax = 1
							else					
								nMax = (CLng(rsMaxPrgReg("MAX")) +1)
							end if
						if sCodSpec <> "" then
							sSQL =	"INSERT INTO REGOLE_STAGE" &_
									" (ID_RICHSTAGE,ID_TAB,ID_CAMPO,COD_OPLOG,VALORE,PRG_REGOLA,PRG_REGOLA_RIF,DT_TMST)" &_
									" VALUES(" & clng(nIdRic) & ",'" & sTab & "','" &_
									sCampoSpec & "','"& aOpLog(0,0,0) & "'," & Trasforma(sCodSpec,sTipoCampo) & "," & nMax & ",0," & ConvDateToDB(Now())& ")"
						else
							sSQL =	"INSERT INTO REGOLE_STAGE" &_
									" (ID_RICHSTAGE,ID_TAB,ID_CAMPO,COD_OPLOG,VALORE,PRG_REGOLA,PRG_REGOLA_RIF,DT_TMST)" &_
									" VALUES(" & clng(nIdRic) & ",'" & sTab & "','" &_
									sCampoArea & "','"& aOpLog(0,0,0) & "'," & Trasforma(sCodArea,sTipoCampo) & "," & nMax & ",0," & ConvDateToDB(Now())& ")"
						end if			
					end if	
					Errore = EseguiNoC(sSQL,CC)
				
					if Errore = "0" then
						if sOpLog <> "" then	
							sSQL = "SELECT MAX(PRG_REGOLA) AS MAX FROM REGOLE_STAGE WHERE ID_RICHSTAGE=" & clng(nIdRic) 
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
							set rsMaxPrgReg = CC.Execute(sSQL)
								if isnull(rsMaxPrgReg("MAX")) then
									nMax = 1
								else					
									nMax = CLng(rsMaxPrgReg("MAX"))
								end if
							rsMaxPrgReg.Close
							set rsMaxPrgReg = nothing
						
							if sCodSpec <> "" then
								sSQL =	"SELECT PRG_REGOLA FROM REGOLE_STAGE WHERE ID_RICHSTAGE=" &_
										clng(nIdRic) & " AND PRG_REGOLA_RIF = 0 AND ID_TAB = '" & sTab & "'" &_
										" And id_campo ='" & sCampoSpec & "'" & " and cod_oplog='" & aOpLog(0,0,0) & "'" 
							else
								sSQL =	"SELECT PRG_REGOLA FROM REGOLE_STAGE WHERE ID_RICHSTAGE=" &_
										clng(nIdRic) & " AND PRG_REGOLA_RIF = 0 AND ID_TAB = '" & sTab & "'" &_
										" And id_campo ='" & sCampoArea & "'" & " and cod_oplog='" & aOpLog(0,0,0) & "'" 
							end if
						
							sSql = sSQL & " And valore = " & sValoreRif
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
							set rsPrgRegRif = CC.Execute(sSQL)
								if isnull(rsPrgRegRif("PRG_REGOLA")) then
									nRegRif = 1
								else					
									nRegRif = CLng(rsPrgRegRif("PRG_REGOLA"))
								end if
							rsPrgRegRif.Close
							set rsPrgRegRif = nothing
						
							if sVal2 <> "" then		'Sicuramente e' una between											 
								sSQL =	"INSERT INTO REGOLE_STAGE " &_
										"(ID_RICHSTAGE,ID_TAB,ID_CAMPO,COD_OPLOG,VALORE,PRG_REGOLA,PRG_REGOLA_RIF,DT_TMST)" &_
										" VALUES(" & clng(nIdRic) & ",'" & sTab & "','" &_
										sCondizione & "','" & sOpLog & "'," & Trasforma(sVal1,sTipoCampo) & " AND " & sVal2 & "," & CLng(nMax+1) & "," & nRegRif & "," & ConvDateToDB(Now())& ")"
							else
								sSQL =	"INSERT INTO REGOLE_STAGE " &_
										"(ID_RICHSTAGE,ID_TAB,ID_CAMPO,COD_OPLOG,VALORE,PRG_REGOLA,PRG_REGOLA_RIF,DT_TMST)" &_
										" VALUES(" & clng(nIdRic) & ",'" & sTab & "','" &_
										sCondizione & "','" & sOpLog & "'," & Trasforma(sVal1,sTipoCampo) & "," & CLng(nMax+1) & "," & nRegRif & "," & ConvDateToDB(Now())& ")"
							end if
							Errore = EseguiNoC(sSQL,CC)
						end if			
					end if 
					sMessaggio = "OK"
'************************
				
				case else
					'Per nuova struttura della tabella...
					'sCampoArea	= Request.Form("txtCampoSpecifico")

					if sCodSpec <> "" then
						sValoreRif = Trasforma(sCodSpec,sTipoCampo)
					else
						sValoreRif = Trasforma(sCodArea,sTipoCampo)
					end if
				
					if nCheck = "0" then
						sSQL = "SELECT MAX(PRG_REGOLA) AS MAX FROM REGOLE_STAGE WHERE ID_RICHSTAGE=" & clng(nIdRic) 
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
						set rsMaxPrgReg = CC.Execute(sSQL)
							if isnull(rsMaxPrgReg("MAX")) then
								nMax = 1
							else					
								nMax = (CLng(rsMaxPrgReg("MAX")) +1)
							end if
						rsMaxPrgReg.Close
						set rsMaxPrgReg = nothing
						
						if sCodSpec <> "" then
							sSQL =	"INSERT INTO REGOLE_STAGE" &_
									" (ID_RICHSTAGE,ID_TAB,ID_CAMPO,COD_OPLOG,VALORE,PRG_REGOLA,PRG_REGOLA_RIF,DT_TMST)" &_
									" VALUES(" & clng(nIdRic) & ",'" & sTab & "','" &_
									sCampoSpec & "','"& aOpLog(0,0,0) & "'," & Trasforma(sCodSpec,sTipoCampo) & "," & nMax & ",0," & ConvDateToDB(Now())& ")"
						else
							sSQL =	"INSERT INTO REGOLE_STAGE" &_
									" (ID_RICHIESTA,ID_TAB,ID_CAMPO,COD_OPLOG,VALORE,PRG_REGOLA,PRG_REGOLA_RIF,DT_TMST)" &_
									" VALUES(" & clng(nIdRic) & ",'" & sTab & "','" &_
									sCampoArea & "','"& aOpLog(0,0,0) & "'," & Trasforma(sCodArea,sTipoCampo) & "," & nMax & ",0," & ConvDateToDB(Now())& ")"
						end if			
					end if	
					Errore = EseguiNoC(sSQL,CC)
				
					if Errore = "0" then
						if sOpLog <> "" then	
							sSQL = "SELECT MAX(PRG_REGOLA) AS MAX FROM REGOLE_STAGE WHERE ID_RICHSTAGE=" & clng(nIdRic) 
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
							set rsMaxPrgReg = CC.Execute(sSQL)
								if isnull(rsMaxPrgReg("MAX")) then
									nMax = 1
								else					
									nMax = CLng(rsMaxPrgReg("MAX"))
								end if
							rsMaxPrgReg.Close
							set rsMaxPrgReg = nothing
						
							if sCodSpec <> "" then
								sSQL =	"SELECT PRG_REGOLA FROM REGOLE_STAGE WHERE ID_RICHSTAGE=" &_
										clng(nIdRic) & " AND PRG_REGOLA_RIF = 0 AND ID_TAB = '" & sTab & "'" &_
										" And id_campo ='" & sCampoSpec & "'" & " and cod_oplog='" & aOpLog(0,0,0) & "'" 
							else
								sSQL =	"SELECT PRG_REGOLA FROM REGOLE_STAGE WHERE ID_RICHSTAGE=" &_
										clng(nIdRic) & " AND PRG_REGOLA_RIF = 0 AND ID_TAB = '" & sTab & "'" &_
										" And id_campo ='" & sCampoArea & "'" & " and cod_oplog='" & aOpLog(0,0,0) & "'" 
							end if
							sSql = sSQL & " And valore = " & sValoreRif
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
							set rsPrgRegRif = CC.Execute(sSQL)
								if isnull(rsPrgRegRif("PRG_REGOLA")) then
									nRegRif = 1
								else					
									nRegRif = CLng(rsPrgRegRif("PRG_REGOLA"))
								end if
							rsPrgRegRif.Close
							set rsPrgRegRif = nothing
						
							if sVal2 <> "" then		'Sicuramente e' una between											 
								sSQL =	"INSERT INTO REGOLE_STAGE" &_
										" (ID_RICHSTAGE,ID_TAB,ID_CAMPO,COD_OPLOG,VALORE,PRG_REGOLA,PRG_REGOLA_RIF,DT_TMST)" &_
										" VALUES(" & clng(nIdRic) & ",'" & sTab & "','" &_
										sCondizione & "','" & sOpLog & "'," & Trasforma(sVal1,sTipoCampo) & " AND " & sVal2 & "," & CLng(nMax+1) & "," & nRegRif & "," & ConvDateToDB(Now())& ")"
							else
								sSQL =	"INSERT INTO REGOLE_STAGE " &_
										"(ID_RICHSTAGE,ID_TAB,ID_CAMPO,COD_OPLOG,VALORE,PRG_REGOLA,PRG_REGOLA_RIF,DT_TMST)" &_
										" VALUES(" & clng(nIdRic) & ",'" & sTab & "','" &_
										sCondizione & "','" & sOpLog & "'," & Trasforma(sVal1,sTipoCampo) & "," & CLng(nMax+1) & "," & nRegRif & "," & ConvDateToDB(Now())& ")"
							end if
							Errore = EseguiNoC(sSQL,CC)
						end if			
					end if 
					sMessaggio = "OK"
'************************
				end select
		CASE "CAN" 
			'Mi prendo il numero delle righe che sono state generate.
			'Ciclo per ognuna di esse per ritrovare quella ceccata.
			Errore = "0"
			nRighe = request.FORM("txtRighe")
			n=0
			for i = 0 to nRighe
				sChecked = Request.form("chkCan" & i)
				if sChecked = "on" then
					sTab		= Request.form("txtIdTab" & i)		'Nome Tabella
					sCampo		= Request.form("txtIdCam" & i)		'Nome Campo
					nIdRic		= Request.form("txtIdRich")			'Id Richiesta
					sPrgReg		= Request.form("txtPrgReg" & i)		'Progressivo regola
					sPrgRegRif	= Request.form("txtPrgRegRif" & i)	'Progressivo regola Riferimento
					
					select case sTab
						case "ESPRO"
							sSQL =	"DELETE FROM REGOLE_STAGE WHERE ID_RICHSTAGE =" &_
									clng(nIdRic) & " AND ID_TAB='" & sTab & "'" &_
									" And (prg_regola =" & CLng(sPrgReg) & " or prg_regola_rif = " & CLng(sPrgReg) & ")"
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
							set RRD = CC.Execute (sSql, nRec)
								Errore = Err.number 
								if Errore = 0 then
									n = n + nRec
								end if	
							set RRD = nothing
							sSQL = ""									
	
						case "TISTUD","PERS_VINCOLI","PERS_DISPON"
							sSQL =	"DELETE FROM REGOLE_STAGE WHERE ID_RICHSTAGE =" &_
									clng(nIdRic) & " AND ID_TAB='" & sTab & "'" &_
									" And (prg_regola =" & CLng(sPrgReg) & " or prg_regola_rif = " & CLng(sPrgReg) & ")"
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
							set RRD = CC.Execute (sSql, nRec)
								Errore = Err.number 
								if Errore = 0 then
									n = n + nRec
								end if	
							set RRD = nothing
							sSQL = ""									
							
						case else
							if Request.Form("IDTAB") <> "VARIE" then
								sSQL = "DELETE FROM REGOLE_STAGE WHERE ID_RICHSTAGE =" &_
										clng(nIdRic) & " AND ID_TAB='" & sTab & "'" &_
										" And (prg_regola =" & CLng(sPrgReg) & " or prg_regola_rif = " & CLng(sPrgReg) & ")"
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
								set RRD = CC.Execute (sSql, nRec)
									Errore = err.number 
									if Err.number = 0 then
										n = n + nRec
									end if	
								set RRD = nothing
								sSQL = ""									
							else
								sSQL =	"DELETE FROM REGOLE_STAGE WHERE ID_RICHSTAGE =" &_
										clng(nIdRic) & " AND ID_CAMPO='" & sCampo &_
										"' AND ID_TAB='" & sTab & "'" &_
										" And (prg_regola =" & CLng(sPrgReg) & " or prg_regola_rif = " & CLng(sPrgReg) & ")"
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
								set RRD = CC.Execute (sSql, nRec)
									Errore = Err.number
									if Err.number = 0 then
										n = n + nRec
									end if
								set RRD = nothing
								sSQL = ""									
							end if
					end select
				end if 
				if Errore <> "0" then
					exit for
				end if  
				Err.Clear
			next
			
			if Errore = "0" then
				Errore = AllineaProgressivo(nIdRic,sPrgReg,n)
			end if 
	END SELECT

else
	Errore="Esiste gi� un inserimento con le stesse caratteristiche"
end if		
%>
<br>
<table border="0" CELLPADDING="0" CELLSPACING="0" width="500" align="center">
	<tr>
		<td class="tbltext3" align="center" valign="middle">
<%
		if Errore = "0" then
			CC.CommitTrans %>				
			<form name="frmIndietro" method="post" action="STA_ModRichStage.asp">
			<table border="0" CELLPADDING="0" CELLSPACING="0" width="500" align="center">
				<tr>
					<td align="center"> 
						<input type="hidden" name="nRichSta" value="<%=nIdRic%>">
						<!--input type="hidden" name="IdSede" value="<%'=sIdSede%>"-->
						<!--input type="hidden" name="dtTmst" value="<%'=Now()%>"-->
					</td>
				</tr>
			</table>
			</form>
<%
			select case sAction
				case "CAN" %>
					<script>
						alert("Cancellazione correttamente effettuata.")
					</script>
<%
				case "INS" %>
					<script>
						alert("Inserimento correttamente effettuato.")
					</script>
<%
			end select%>
				<script>
					frmIndietro.submit()
				</script>
<%
			Response.End
		else
			Inizio()
			Response.Write Errore
			CC.RollBackTrans
		end if 
			%>
		</td>
	</tr>
	<tr>
		<td align="center" valign="middle" align="center">
			<br><br><br><br>
			<form name="frmIndietro1" method="POST" action="STA_InsCriteriSel.asp">
<%
			if (Request.Form("IDTAB") = "VARIE") and sAction ="CAN" then %>
				<input type="hidden" value="VARIE" name="IDTAB">
<%				
				sTab1 = sTab
			else %>
				<input type="hidden" value="<%=sTab%>" name="IDTAB">
<%
			end if %>
				<input type="hidden" value="<%=sTab1%>" name="IDTAB1">
				<input type="hidden" name="IdRic" value="<%=nIdRic%>">
				<input type="hidden" name="AREA" value="<%=sArea%>">
				<input type="hidden" value="<%=sIdSede%>" name="IdSede">
				<input type="hidden" name="SPECIFICO" value="<%=sSpecifico%>">
<%
			if sCond <> "" then %>
				<input type="hidden" name="CONDIZIONE" value="<%=sCond%>"> 
<%
			else %>
				<input type="hidden" name="CONDIZIONE" value="<%=sCondizione%>"> 
<%			
			end if %>
				<a href="Javascript:frmIndietro1.submit();" onmouseover="window.status=' '; return true"><img src="<%=session("progetto")%>/images/indietro.gif" border="0"></a>
			</form>
		</td>
	</tr>
</table>

<%
Erase aOpLog

'*******************************************
function AllineaProgressivo(nIdRic,sPrgReg,n)
	n=1
	
	set RR1 = server.CreateObject("ADODB.Recordset") 
	sSQL = "select prg_regola from regole_stage where id_richstage =" & clng(nIdRic) & " order by prg_regola"
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	RR1.Open sSQL,CC,3
	
		if not rr1.EOF then
			i=1
			do until RR1.EOF
				memo = RR1.Fields("prg_regola")
				if not clng(memo) = i  then
					sSQLU = "Update regole_stage set prg_regola = " & i &_
							" where id_richstage = " & clng(nIdRic) &_
							" And prg_regola = " & memo
					Errore = EseguiNoC(sSQLU,CC)
					if Errore = "0" then
						sSQLU = "Update regole_stage set prg_regola_rif = " & i  &_
								" where id_richstage = " & clng(nIdRic) &_
								" And prg_regola_rif = " & memo
						Errore = EseguiNoC(sSQLU,CC)
						if Errore <> "0" then
							exit do
						end if
					else
						exit do
					end if
				end if
			RR1.MoveNext
			i = i+1
			loop
		RR1.Close
		set RR1 = nothing
		end if
	
	Errore = 0
	AllineaProgressivo = Errore
End function

'*******************************************
Sub Inizio()%>
	<table width="520" border="0" cellspacing="0" cellpadding="0" height="81" align="center">
		<tr>
			<td width="500" height="81" background="<%=session("progetto")%>/images/titoli/strumenti2b.gif" valign="bottom" align="right">
				<table border="0" background width="500" height="30" cellspacing="0" cellpadding="0" align="center">
					<tr>
						<td width="100%" valign="top" align="right"><b CLASS="tbltext1a">Definizione Stage
						  </b></td>
					</tr>
				</table>
			</td>
		</tr>
	</table> 
	<br>
	<!-- Linguetta superiore -->
		<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
			<tr height="18">
				<td class="sfondomenu" width="67%" height="18"><span class="tbltext0"><b>&nbsp;CONFERMA REQUISITI CANDIDATI</b></span></td>
				<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
				<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
			</tr>
		</table>
		<!-- Commento -->
		<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
			<tr>
				<td align="left" class="sfondocomm">
					Pagina di conferma/cancellazione dei requisiti, relativi a un determinato settore, 
					richiesti per i candidati.
				</td>
			</tr>
			<tr height="2">
				<td class="sfondocomm" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif">
				</td>
			</tr>
		</table>
	<br>
<%
End Sub

'*******************************************
Function Trasforma(valore,tipo)
	select case tipo
'		case "D"
'			Trasforma = ConvDateToDbs(valore)
		case "A"
			Trasforma = "'" & Valore & "'"
		case else
			Trasforma = valore
	end select
End Function 
%>

<!--#include Virtual = "/include/CloseConn.asp"-->
<!--#include Virtual="/strutt_coda2.asp"-->		
