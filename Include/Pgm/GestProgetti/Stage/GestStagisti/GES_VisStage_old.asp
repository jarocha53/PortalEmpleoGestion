<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include virtual="/util/portallib.asp"-->
<!--#include Virtual = "/strutt_testa2.asp"-->
<!--#include Virtual ="/Include/openconn.asp"-->

<script language="javascript">

<!--#include virtual = "/Include/ControlDate.inc"-->

//include del file per fare i controlli sulla numericitÓ dei campi
<!--#include virtual = "/Include/ControlNum.inc"-->
<!--#include virtual = "/Include/help.inc"-->

</script>

<%	
'********************************
'*********** MAIN ***************

if ValidateService(session("idutente"),"GES_VISSTAGE",cc) <> "true" then 
	response.redirect "/util/error_login.asp"
end if

Inizio()

Imposta_Pag()

'**********************************************************************************************************************************************************************************	
	
Sub Inizio()
%>
	<table border="0" width="520" cellspacing="0" cellpadding="0" height="70" align="center">
		<tr>
			<td width="500" background="<%=session("progetto")%>/images/titoli/strumenti2b.gif" height="70" valign="bottom" align="right">
				<table border="0" background width="500" height="23" cellspacing="0" cellpadding="0">
					<tr>
						<td width="100%" valign="top" align="right"><b CLASS="tbltext1a">Gestione Stagisti
						  </b></td>
					</tr>
				</table>
			</td>
		</tr>
	</table> 
	<br>
	<table border="0" CELLPADDING="0" CELLSPACING="0" width="500" align="center">
		<tr height="18">
			<td class="sfondomenu" width="67%" height="18"><span class="tbltext0"><b>&nbsp;VISUALIZZAZIONE RICHIESTE STAGE </b></span></td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
		  	<td valign="middle" align="right" class="tbltext1" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
		</tr>
	</table>
	<!-- Commento -->
	<table border="0" CELLPADDING="0" CELLSPACING="0" width="500" align="center">
		<tr>
			<td align="left" class="sfondocomm">
                   Premere su <b>Rif. Rich.</b> per visualizzare le persone da gestire che partecipano allo stage selezionato.
        
				<a href="Javascript:Show_Help('/pgm/help/gestprogetti/stage/GestStagisti/GES_VisStage')"><img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
			</td>
		</tr>
		<tr height="2">
			<td class="sfondocommaz" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>

<%	
End Sub

Sub Imposta_Pag() 
	
	'''LC**:30/09/2003 - modificata query
	'''sSQL ="SELECT DISTINCT(PS.ID_RICHSTAGE),DS.DESC_STAGE,ID_SEDE " &_
	'''      "FROM DOMANDA_STAGE DS, PERS_STAGE PS WHERE " &_
	'''      " PS.ID_RICHSTAGE=DS.ID_RICHSTAGE" &_
	'''      " AND ID_UORG =" & session("IDUORG") &_
	'''      " AND (DT_FINE IS NULL OR DT_FINE >= " & convDateToDbs(now) & ")" &_
	'''      " ORDER BY ID_RICHSTAGE"
    sSQL =	"SELECT DISTINCT(PS.ID_RICHSTAGE), PS.ID_TUTOR, PS.ID_TUTORAZ, DS.DESC_STAGE, " &_
			" DS.ID_SEDE, DS.DT_INISTAGE, DS.DT_FINSTAGE,SI.ID_IMPRESA, SI.DESCRIZIONE, I.RAG_SOC " &_
			" FROM DOMANDA_STAGE DS, PERS_STAGE PS, SEDE_IMPRESA SI, IMPRESA I" &_
			" WHERE PS.ID_RICHSTAGE = DS.ID_RICHSTAGE" &_
			" AND DS.ID_SEDE=SI.ID_SEDE " &_ 
		    " AND SI.ID_IMPRESA= I.ID_IMPRESA " &_ 
			" AND ID_UORG =" & session("IDUORG") &_
			" AND ID_TUTOR =" & session("IDUTENTE") &_
			" AND" &_
			" (" &_
				" DT_FINE IS NULL " &_
				" OR " &_
				" DT_FINE >= " & convDateToDbs(now) &_
				" OR " &_
				" (" &_
					" DT_FINE < " & convDateToDbs(now) &_
					" AND " &_
					" PS.id_richstage IN " &_
					" (" &_
						" SELECT DISTINCT(id_richstage) FROM PROROGA_STAGE PRS" &_
						" WHERE dt_finepro >= " & convDateToDbs(now) &_
						" AND PRS.id_richstage = PS.id_richstage" &_
					" ) " &_
				" ) " &_
			" ) " &_
			" ORDER BY ID_RICHSTAGE"		
    '''FINE LC**    
   ' Response.Write sSQL  
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
    SET rsRisultato = cc.execute(sSQL)
    
    if not rsRisultato.eof then %>
       <br>
		<table width="500" align="center" border="0" cellspacing="2" cellpadding="1" class="sfondocommaz">
            <tr height="20"> 
			  	<td class="sfondocomm" align="middle" width="10">
					<b class="tbltext1">Rif. Rich.</b>
				</td>
				<td class="sfondocomm" align="left" width="180">
					<b class="tbltext1">Descrizione Stage</b>
				</td>
				<td class="sfondocomm" width="180">
					<b class="tbltext1">Denominazione Azienda</b>
				</td>
				<td class="sfondocomm" width="140">
					<b class="tbltext1">Periodo Stage<br>dal / al</b>
				</td>
				<td class="sfondocomm" width="10">
					<b class="tbltext1">Proroghe</b>
				</td>
			</tr>
<%      cont = 0
        do while not rsRisultato.eof 
			sSQL = "SELECT count(ID_RICHSTAGE) as num FROM PROROGA_STAGE " & _
				"WHERE ID_RICHSTAGE = " & rsRisultato("ID_RICHSTAGE")

			SET rsApp = Server.CreateObject("ADODB.Recordset")
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
			rsApp.Open sSQL, CC, 3  
            
%>			
           <form name="frmDett<%=cont%>" method="post" action="GES_VisStagisti.asp">
				<input type="hidden" name="txtIdRichiesta" value="<%=rsRisultato("ID_RICHSTAGE")%>">
				<input type="hidden" name="txtSede" value="<%=rsRisultato("ID_SEDE")%>">
			<!--'CONSUELO 07/04/2004 aggiunti campi hidden per dati utili -->	
				<input type="hidden" name="txtDescSede" value="<%=rsRisultato("DESCRIZIONE")%>">
				<input type="hidden" name="txtRagSoc" value="<%=rsRisultato("RAG_SOC")%>">
				<input type="hidden" name="txtIdTut" value="<%=rsRisultato("ID_TUTOR")%>">
				<input type="hidden" name="txtIdTutAz" value="<%=rsRisultato("ID_TUTORAZ")%>">
								
           </form>
           
           <tr height="20" class="tblsfondo"> 
			  	<td class="tbldett" align="middle">
			  	    <a class="tblAgg" HREF="Javascript:frmDett<%=cont%>.submit()" onmouseover="javascript:window.status=' '; return true">
					<b>&nbsp;<%=rsRisultato("ID_RICHSTAGE")%></b>
					</a>
				</td>
				<td class="tbldett" align="middle">
					&nbsp;<%=rsRisultato("DESC_STAGE")%>
				</td>
				<td class="tbldett">
					<%=rsRisultato("rag_soc")%> - <%=rsRisultato("descrizione")%>
				</td>
				<td class="tbldett">
					<%=rsRisultato("dt_inistage") & " - " & rsRisultato("dt_finstage")%>
				</td>
				<td class="tbldett">
					<center>
					<%Response.Write rsApp("num")%>
					</center>	
				</td>
			</tr>
			
<%      cont = cont +1
		rsApp.Close 
		set rsApp = nothing
        rsRisultato.movenext
        loop
%>			
		</table>
<%  else
       Testo = "Al momento non ci sono stage da gestire "
    
%>		
		<br><br>
		<table width="500" align="center" border="0" cellspacing="2" cellpadding="1" class="sfondocommaz">
			<tr height="20"> 
			  	<td class="tbltext3" align="middle" width="500">
					<b><%=Testo%></b>
				</td>
			</tr>
		</table>
<%end if 		

End Sub
	
%>	
<!--#include Virtual ="/Include/closeconn.asp"-->
<!--#include Virtual="/strutt_coda2.asp"-->		
