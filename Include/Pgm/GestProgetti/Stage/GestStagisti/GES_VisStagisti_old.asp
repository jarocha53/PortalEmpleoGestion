<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include virtual="/util/portallib.asp"-->
<!--#include Virtual = "/strutt_testa2.asp"-->
<!--#include Virtual ="/Include/openconn.asp"-->
<!--#include Virtual ="/Include/DecCod.asp"-->

<script language="javascript">

<!--#include virtual = "/Include/ControlDate.inc"-->

//include del file per fare i controlli sulla numericitÓ dei campi
<!--#include virtual = "/Include/ControlNum.inc"-->
<!--#include virtual = "/Include/help.inc"-->

function ApriModulo(NomeFile){

	window.open (NomeFile,"Moduli","Status=no,scrollbars=yes,toolbar=no,width=550,height=500,top=20,left=100");
	
}

</script>

<%	
'********************************
'*********** MAIN ***************

if ValidateService(session("idutente"),"GES_VISSTAGE",cc) <> "true" then 
	response.redirect "/util/error_login.asp"
end if

Inizio()

Imposta_Pag()

'-------------------------------------------------------------------------------------------------------------
	
Sub Inizio()
%>
	<table border="0" width="520" cellspacing="0" cellpadding="0" height="70" align="center">
		<tr>
			<td width="500" background="<%=session("progetto")%>/images/titoli/strumenti2b.gif" height="70" valign="bottom" align="right">
				<table border="0" background width="500" height="23" cellspacing="0" cellpadding="0">
					<tr>
						<td width="100%" valign="top" align="right"><b CLASS="tbltext1a">Gestione Stagisti
						  </b></td>
					</tr>
				</table>
			</td>
		</tr>
	</table> 
	<br>
	<table border="0" CELLPADDING="0" CELLSPACING="0" width="500" align="center">
		<tr height="18">
			<td class="sfondomenu" width="67%" height="18"><span class="tbltext0"><b>&nbsp;VISUALIZZAZIONE STAGISTI </b></span></td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
		  	<td valign="middle" align="right" class="tbltext1" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
		</tr>
	</table>
	<!-- Commento -->
	<table border="0" CELLPADDING="0" CELLSPACING="0" width="500" align="center">
		<tr>
			<td align="left" class="sfondocomm">
                   Premere su <b> Rif. Pers.</b> per visualizzare la situazione relativa allo stagista selezionato.     
				<a href="Javascript:Show_Help('/pgm/help/gestprogetti/stage/GestStagisti/GES_VisStagisti')"><img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
			</td>
		</tr>
		<tr height="2">
			<td class="sfondocommaz" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>

<%	
End Sub

'-------------------------------------------------------------------------------------------------------------

Sub Imposta_Pag()
 
    sIdRich= Request.Form("txtIdRichiesta") 
    sSede =Request.Form("txtSede")
'    Response.Write "sIdRich: " & sIdRich & "<br>"
'    Response.Write "sSede: " & sSede & "<br>"
    
    '''LC**:01/10/2003 - aggiunta la "distinct" per evitare nomi duplicati. 
    '''sSQL =	"SELECT A.ID_PERSONA" &_
    sSQL =	"SELECT distinct(A.ID_PERSONA),DT_INIZIO,DT_FINE,COD_ESPLA" &_
			" FROM PERS_STAGE A,PROROGA_STAGE B WHERE" &_
			" A.ID_PERSONA = B.ID_PERSONA(+) " &_ 
			" AND A.ID_RICHSTAGE=" & sIdRich &_
			" AND ID_TUTOR =" & session("IDUTENTE") &_
			" AND (DT_FINE IS NULL OR DT_FINE >= " & convDateToDbs(now) &_
			" OR DT_FINEPRO >= " & convDateToDbs(now) & ")" &_
			" ORDER BY ID_PERSONA"
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
    SET rsRisultato = cc.execute(sSQL)
    
    if not rsRisultato.eof then %>
        <br>
		<table width="500" border="0" cellspacing="2" cellpadding="1" class="sfondocommaz">
            <tr height="20"> 
			  	<td class="sfondocomm" align="middle" width="10">
					<b class="tbltext1">Rif. Pers.</b>
				</td>
				<td class="sfondocomm" align="left" width="200">
					<b class="tbltext1">Stagista</b>
				</td>
				<td class="sfondocomm" width="20%">
					<b class="tbltext1">Periodo Stage<br>dal / al</b>
				</td>
				<td class="sfondocomm" width="20%">
					<b class="tbltext1">Esito stage</b>
				</td>
				<td class="sfondocomm" width="20%" colspan="2">
					<b class="tbltext1">Modelli</b>
				</td>
			</tr>
<%      cont = 0
        do while not rsRisultato.eof 
			sSQL1 = "SELECT NOME, COGNOME, PRV_NASC, COM_NASC, DT_NASC, PRV_RES, " & _
					"COM_RES, IND_RES, COD_FISC " & _
					"FROM PERSONA WHERE ID_PERSONA = " & rsRisultato("ID_PERSONA")
'PL-SQL * T-SQL  
SSQL1 = TransformPLSQLToTSQL (SSQL1) 
             SET rsNome = cc.execute(sSQL1)
         
			Dim objFile
			Set objFile = Server.CreateObject ("Scripting.FileSystemObject")
			nomefile= session("progetto")& "/DOCPRG/" & rsRisultato("ID_PERSONA")& ".htm"
             
%>			
           <form name="frmDett<%=cont%>" method="post" action="GES_VisDettStagisti.asp">
				<input type="hidden" name="txtIdRichiesta" value="<%=sIdRich%>">
				<input type="hidden" name="txtSede" value="<%=sSede%>">
				<input type="hidden" name="txtIdPers" value="<%=rsRisultato("ID_PERSONA")%>">
                <input type="hidden" name="txtNome" value="<%=rsNome("NOME")%>">
                <input type="hidden" name="txtCognome" value="<%=rsNome("COGNOME")%>">

				<input type="hidden" name="txtDescSede" value="<%=Request.Form("txtDescSede")%>">
				<input type="hidden" name="txtRagSoc" value="<%=Request.Form("txtRagSoc")%>">
				<input type="hidden" name="txtIdTut" value="<%=Request.Form("txtIdTut")%>">
				<input type="hidden" name="txtIdTutAz" value="<%=Request.Form("txtIdTutAz")%>">
              
	       </form>
	       
	       <form name="frmmoduli<%=cont%>" method="post" action="GES_insdoc1.asp">
				<input type="hidden" name="txtIdRichiesta" value="<%=sIdRich%>">
				<input type="hidden" name="txtSede" value="<%=sSede%>">
				<input type="hidden" name="txtIdPers" value="<%=rsRisultato("ID_PERSONA")%>">				
                <input type="hidden" name="txtNome" value="<%=rsNome("NOME")%>">
                <input type="hidden" name="txtCognome" value="<%=rsNome("COGNOME")%>">
                
                <!--'CONSUELO 07/04/2004 aggiunti campi hidden per dati utili -->	
                <input type="hidden" name="txtDtIniStage" value="<%=rsRisultato("DT_INIZIO")%>">
				<input type="hidden" name="txtDtFinStage" value="<%=rsRisultato("DT_FINE")%>">				
				<input type="hidden" name="txtDescSede" value="<%=Request.Form("txtDescSede")%>">
				<input type="hidden" name="txtRagSoc" value="<%=Request.Form("txtRagSoc")%>">
				<input type="hidden" name="txtIdTut" value="<%=Request.Form("txtIdTut")%>">
				<input type="hidden" name="txtIdTutAz" value="<%=Request.Form("txtIdTutAz")%>">								

                <input type="hidden" name="txtPrvNasc" value="<%=rsNome("PRV_NASC")%>">
                <input type="hidden" name="txtComNasc" value="<%=rsNome("COM_NASC")%>">
                <input type="hidden" name="txtDtNasc" value="<%=rsNome("DT_NASC")%>">
                <input type="hidden" name="txtPrvRes" value="<%=rsNome("PRV_RES")%>">
                <input type="hidden" name="txtComRes" value="<%=rsNome("COM_RES")%>">
                <input type="hidden" name="txtIndRes" value="<%=rsNome("IND_RES")%>">
                <input type="hidden" name="txtCodFisc" value="<%=rsNome("COD_FISC")%>">                                				
				
	       </form>

	       <form name="frmEliminaHtm<%=cont%>" method="post" action="GES_Risultati.asp">
           		<input type="hidden" name="mod" value="D">
				<input type="hidden" name="txtIdRichiesta" value="<%=sIdRich%>">
				<input type="hidden" name="txtSede" value="<%=sSede%>">
				<input type="hidden" name="txtIdPers" value="<%=rsRisultato("ID_PERSONA")%>">
				<input type="hidden" name="txtNomeFile" value="<%=nomefile%>">
				<input type="hidden" name="txtDescSede" value="<%=Request.Form("txtDescSede")%>">
				<input type="hidden" name="txtRagSoc" value="<%=Request.Form("txtRagSoc")%>">
				<input type="hidden" name="txtIdTut" value="<%=Request.Form("txtIdTut")%>">
				<input type="hidden" name="txtIdTutAz" value="<%=Request.Form("txtIdTutAz")%>">					
	       </form>
	       	       
           <tr height="20" class="tblsfondo"> 
			  	<td class="tbldett" align="middle" width="10">
			  	    <a class="tblAgg" HREF="Javascript:frmDett<%=cont%>.submit()" onmouseover="javascript:window.status=' '; return true">
					<b>&nbsp;<%=rsRisultato("ID_PERSONA")%></b>
					</a>
				</td>
	
				<td class="tbldett" align="middle" width="220">
					&nbsp;<%=rsNome("NOME")%>&nbsp;<%=rsNome("COGNOME")%>
				</td>
				<td class="tbldett">
					<%=rsRisultato("dt_inizio") & " - " & rsRisultato("dt_fine")%>
				</td>
				<td class="tbldett">
				    <%sValore = DecCodVal("ESPLA",0,DATE,rsRisultato("cod_espla"),0)%>
					<%=sValore%>
				</td>
				<%
												
				if Request.Form("txtIdTutAz") <> "" then
					If objFile.FileExists (Server.MapPath(nomefile)) Then
					%>
					<td align="center">
						
						<a href="javascript:ApriModulo('<%=nomefile%>')">
							<img src="/images/leggi.gif" border="0" alt="Visualizza il modello" WIDTH="25" HEIGHT="25">
						</a>
					</td>
				
					<td align="center">	
						<a href="javascript:frmEliminaHtm<%=cont%>.submit()">
							<img src="/images/cestino.gif" border="0" alt="Elimina il modello" WIDTH="12" HEIGHT="12">
						</a>								
					</td>
					 
					<%Else%>
				
					<td align="center" colspan="2">
						<a HREF="Javascript:frmmoduli<%=cont%>.submit()" onmouseover="javascript:window.status=' '; return true">
							<img src="/images/compila.gif" border="0" WIDTH="19" HEIGHT="14" alt="compila">
						</a>
					</td>
					<%End If
					
				else%>
					<td align="center" colspan="2">
						-						
					</td>					
				<%end if	
				Set objFile = Nothing
				%> 

				
			</tr>
			
<%          set rsNome = nothing
        cont = cont +1
        rsRisultato.movenext
        loop
%>			
		</table>
<%  else
       Testo = "Al momento non ci sono persone da gestire per lo stage selezionato "
    
%>		
		<br><br>
		<table width="500" align="center" border="0" cellspacing="2" cellpadding="1" class="sfondocommaz">
			<tr height="20"> 
			  	<td class="tbltext3" align="middle" width="500">
					<b><%=Testo%></b>
				</td>
			</tr>
		</table>
<%end if 	%>	
        <br>
		<br>
		<table border="0" cellpadding="0" cellspacing="1" width="500">
			<tr>
				<td align="center">
					<a href="GES_VisStage.asp">
						<img src="<%=session("Progetto")%>/images/indietro.gif" border="0" name="imgPunto2" onmouseover="javascript:window.status=' '; return true">
					</a>
				</td>
			</tr>
		</table>
		<br>
<%
End Sub
'-------------------------------------------------------------------------------------------------------------	
%>	
<!--#include Virtual ="/Include/closeconn.asp"-->
<!--#include Virtual="/strutt_coda2.asp"-->		

