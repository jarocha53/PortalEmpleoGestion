<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include Virtual = "/include/DecCod.asp"-->
<!--#include Virtual = "/Include/OpenConn.asp"-->
<!-- #Include Virtual="/Include/ControlDateVB.asp" -->
<!-- #Include Virtual="/Include/ControlVall.asp" -->
<%
dim IDP,dDtInizio,dDtFine
dim sRic
sRic =Request.QueryString("Rich") 
IDP= Request.QueryString("ID") 
%>
<!-- #Include Virtual="/Pgm/BilancioCompetenze/ValidaDtNascita.asp" -->
<%

TIPOLOGIA = STAGE

						Select Case TIPOLOGIA
							Case "ESPRO"
								TITOLO = "<b>ESPERIENZE LAVORATIVE</b>"
								SOTTOTITOLO = "Inserimento esperienze lavorative.<br>	Indica di seguito le informazioni relative alle tue esperienze lavorative." & _
								"<br>Compila i campi sottostanti e premi <b>Invia</b> per salvare le modifiche. "
								CondizTespr = " AND substr(VALORE,2,1) = 'N'" 								
							Case "STAGE"
								TITOLO = "<b>STAGE, TIROCINI, APPRENDISTATO</b>"
								SOTTOTITOLO = "	Inserimento stage, tirocini, apprendistato. <br>Indica di seguito le informazioni relative a stage, tirocini, apprendistato." & _
								"<br>Compila i campi sottostanti e premi <b>Invia</b> per salvare le modifiche. "				
								CondizTespr = " AND substr(VALORE,2,1) = 'S'" 
							Case else
								TITOLO = "<b>ESPERIENZE PROFESSIONALI</b>"
								SOTTOTITOLO = "	Inserimento esperienze professionali. <br>Indica di seguito le informazioni relative alle esperienze professionali." & _
								"<br>Compila i campi sottostanti e premi <b>Invia</b> per salvare le modifiche. "
							End Select
%>
<title>Esperienze professionali</title>

<link REL="STYLESHEET" TYPE="text/css" HREF="<%=session("progetto")%>/fogliostile.css">

<script language="javascript">
	<!--
		
		<!--#include Virtual = "/Include/ControlNum.inc"-->
		<!--#include Virtual = "/Include/ControlDate.inc"-->
		<!-- #Include Virtual="/Include/ControlString.inc" -->
	
	function valida(Obj)
	{
	Obj.Rag_Soc.value=TRIM(Obj.Rag_Soc.value)
	
	
		if (Obj.txtDataInizio.value =="")
		
			{
				alert("La data del campo Iniziata Il � obbligatoria")
				Obj.txtDataInizio.focus()
				return false;				
			} 	
		
		DataInizio=Obj.txtDataInizio.value
		DataFine=Obj.txtDataFine.value
		DataOggi=Obj.txtoggi.value
		DataNascita=Obj.DataNascita.value


		if (!ValidateInputDate(DataInizio))
			{
			Obj.txtDataInizio.focus()
			return false;
			}

		
		if (ValidateRangeDate(DataInizio,DataOggi)==false)
			{
			alert("La data del campo Iniziata Il non deve essere superiore a quella odierna")
			Obj.txtDataInizio.focus()
			return false;
			}
		
		//DataNascita 
		if (ValidateRangeDate(DataNascita,DataInizio)==false)	
			{
			alert("La data del campo Iniziata Il deve essere successiva alla data di nascita") 
			Obj.txtDataInizio.focus()
			return false 
			}
	

		if (!Obj.txtDataFine.value =="")
		{
			if (!ValidateInputDate(DataFine))
				{
					Obj.txtDataFine.focus()
					return false
				}

			if (ValidateRangeDate(DataInizio,DataFine)==false)	
				{
				alert("La data del campo Conclusa Il deve essere successiva alla data del campo Iniziata il") 
				Obj.txtDataFine.focus()
				return false; 
				}

			if (ValidateRangeDate(DataFine,DataOggi)==false)
				{
					alert("La data del campo Conclusa Il non deve essere superiore a quella odierna")
					Obj.txtDataFine.focus()
					return false;
				}
			if ((Obj.Mcess.options[Obj.Mcess.selectedIndex].value == ""))
					{
						alert("Il campo Motivo del Termine � obbligatorio se si compila il campo Conclusa il")
						Obj.Mcess.focus()
						return false;
					}
		}
		
			if ((!Obj.Mcess.options[Obj.Mcess.selectedIndex].value == ""))
					{
						if (Obj.txtDataFine.value =="")
						{
						alert("Il campo Motivo del Termine si compila solo se � stata inserita la data del campo Conclusa Il")
						Obj.Mcess.focus()
						return false;
						}
		
					}

		if ((Obj.Settori.options[Obj.Settori.selectedIndex].value == ""))
		{
			alert("Il campo Settore Merceologico � obbligatorio")
			Obj.Settori.focus()
			return false;
		}

		
		if ((Obj.CmbCOD_ESPERIENZA.options[Obj.CmbCOD_ESPERIENZA.selectedIndex].value == ""))
		{
			alert("Il campo Tipologia  Esperienza � obbligatorio")
			Obj.CmbCOD_ESPERIENZA.focus()
			return false;
		}
				

		if  (!((Obj.CmbCOD_ESPERIENZA.options[Obj.CmbCOD_ESPERIENZA.selectedIndex].value == "01"))) 
				{	
				if 	((Obj.Rag_Soc.value == ""))	
				
					{
					alert("Il campo Presso � obbligatorio per la Tipologia Esperienza selezionata")
					Obj.Rag_Soc.focus() 
					return false;
					}
				}

		if ((!Obj.Rag_Soc.value == ""))
			{
			
			   if (!ValidateInputStringWithNumber(Obj.Rag_Soc.value))
				{
				alert("Il campo Presso � formalmente errato")
				Obj.Rag_Soc.focus() 
				return false;
				}
				if  ((Obj.CmbCOD_FORMA.options[Obj.CmbCOD_FORMA.selectedIndex].value == ""))
				{	
					alert("Il campo Forma giuridica obbligatorio quando viene specificato il campo Presso")
					Obj.CmbCOD_FORMA.focus()
					return false;
				}
				if  ((Obj.CmbDimaz.options[Obj.CmbDimaz.selectedIndex].value == ""))
				{	
					alert("Il campo Dimensioni Azienda obbligatorio quando viene specificato il campo Presso")
					Obj.CmbDimaz.focus()
					return false;
				}
			}

		if ((!Obj.CmbCOD_FORMA.options[Obj.CmbCOD_FORMA.selectedIndex].value == ""))
			{
				if  ((Obj.Rag_Soc.value == ""))
				{	
					alert("Il campo Presso � obbligatorio quando viene specificato il campo Forma Giuridica")
					Obj.Rag_Soc.focus()
					return false;
				}
			}

			if ((!Obj.CmbDimaz.options[Obj.CmbDimaz.selectedIndex].value == ""))
			{
				if  ((Obj.Rag_Soc.value == ""))
				{	
					alert("Il campo Presso � obbligatorio quando viene specificato il campo Dimensioni Azienda")
					Obj.Rag_Soc.focus()
					return false;
				}
			}

		if (Obj.Mansione.value == "")
		{

			alert("Utilizzare il tasto RICERCA per associare la mansione svolta")
			Obj.CriterioRicerca.focus()
			return false;
		}
}

	function ApriCodIstat()
	{
	//document.AddEspro.DescrMansione.value = "";
	document.AddEspro.CriterioRicerca.value=TRIM(document.AddEspro.CriterioRicerca.value)
 	document.AddEspro.RicCodIstat.value=TRIM(document.AddEspro.RicCodIstat.value)
   //document.AddEspro.Mansione.value = "";
			   if (!ValidateInputStringWithNumber(document.AddEspro.CriterioRicerca.value))
				{
				alert("Il criterio di ticerca nel campo Ruolo Ricoperto � formalmente errato")
				document.AddEspro.CriterioRicerca.focus() 
				return;
				}
				
				
		for (var i = 0; i < document.AddEspro.RicCodIstat.value.length;)
		{

			// Controllo se i caratteri sono mumerici
			if (!IsNum(document.AddEspro.RicCodIstat.value.substring(i,i+1)))
					{
				alert("Il carattere n." + (i+1) + " del Codice Istat da ricercare deve essere in formato numerico")
				return;
					}


			// Controllo posizione del punto

			if (i < 8)
			{

				if (document.AddEspro.RicCodIstat.value.substring(i+1,i+2) != "")
				{
				//alert(Obj.Codice.value.substring(i+1,i+2))
					if (document.AddEspro.RicCodIstat.value.substring(i+1,i+2) != ".")
					{
					alert("Il carattere n. " + (i+2) + "   del Codice Istat da ricercare deve essere un punto")
					return;
					}
				}
			}

			// Ultimo carattere


		i = i +2

		}

	if (document.AddEspro.RicCodIstat.value.length == 10)
				{
					if 	(!IsNum(document.AddEspro.RicCodIstat.value.substring(9,10)))
					{
					alert("Il carattere n." + "10" + " del Codice Istat da ricercare deve essere in formato numerico")
					return;
					}
				}

			
				
	//	else
	//	{
	
		CriterioRicerca = AddEspro.CriterioRicerca.value
		RicCodIstat = document.AddEspro.RicCodIstat.value
		URL = "CodiceIstat.asp?VISMENU=NO" + "&RICERCA="+ CriterioRicerca + "&RicCodIstat=" + RicCodIstat
		opt = "address=no,status=no,width=550,height=280,top=150,left=15"
		window.open (URL,"",opt)
	//	}
	}


function ActivInput(valore)
	{

	if ((valore != "01"))
			{
			document.AddEspro.Rag_Soc.disabled = false;
			document.AddEspro.CmbTicli.options.value= "";
			document.AddEspro.CmbTicli.options.disabled = true;
			document.AddEspro.CmbCOD_FORMA.options.disabled = false;
			document.AddEspro.CmbDimaz.options.disabled = false;
			}
			else 
			{
			document.AddEspro.CmbTicli.options.value ="";
			document.AddEspro.CmbTicli.options.disabled = false;
			document.AddEspro.Rag_Soc.disabled = true;
			document.AddEspro.Rag_Soc.value = "";
			document.AddEspro.CmbCOD_FORMA.options.value = "";
			document.AddEspro.CmbCOD_FORMA.options.disabled = true;
			document.AddEspro.CmbDimaz.options.value = "";
			document.AddEspro.CmbDimaz.options.disabled = true;
			
			}


	}

	//-->
</script>

<center>

<form name="AddEspro" Action="GES_SalvaEspro.asp" Method="post" OnSubmit="return valida(this)">
	<center>
	<input type="hidden" name="IdPers" value="<%=IDP%>">
	<input type="hidden" name="IdRich" value="<%=sRic%>">
	<input type="hidden" value="<%=Request("IND_FASE")%>" name="IND_FASE">
	<input type="hidden" value="<%=Request("LivMenu")%>" name="LivMenu">
	<input type="hidden" value="<%=Request("back")%>" name="back">
	<input type="hidden" value="<%=Request("TIPOLOGIA")%>" name="TIPOLOGIA">
	<input type="hidden" name="txtoggi" value="<%=ConvDateToString(Now())%>">
	<input type="hidden" name="DataNascita" value="<%=DT_NASC%>">



<table border="0" width="520" CELLPADDING="0" cellspacing="0">
	<tr class="tblcomm">
		<td align="left" class="sfondomenu">
			<span class="tbltext0"><b><%=TITOLO%></b></span>
		</td>
		<td width="25" valign="bottom" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">
			<img border="0" src="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
		<td width="200" valign="middle" align="right" class="tbltext1" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">&nbsp;(*) campi obbligatori</td>
	</tr>
</table>

<table border="0" width="520" CELLPADDING="0" cellspacing="0">
    <tr>
		<td class="sfondocomm" align="left" class="tbltext1">
			<%=SOTTOTITOLO%>
		</td> 
		<td class="sfondocomm">
			<a onmouseover="javascript: window.status=' '; return true; " href="Javascript:Show_Help('/pgm/help/gestprogetti/stage/GestStagisti/GES_DettEspro')">
			<img src="<%=Session("Progetto")%>/images/Help.gif" border="0" align="right" alt="Help">
			</a>
		</td>
    </tr>
    <tr>
		<td colspan="3" bgcolor="#3399CC"></td>
    </tr>
    <tr>
		<td colspan="3" height="15"></td>
    </tr>
</table>	
<%
sSQL1="SELECT DT_INIZIO,DT_FINE FROM PERS_STAGE WHERE" &_
	 " ID_RICHSTAGE=" & sRic & " AND ID_PERSONA=" & IDP

'PL-SQL * T-SQL  
SSQL1 = TransformPLSQLToTSQL (SSQL1) 
set rsDate = cc.execute(sSQL1)

dDtInizio = rsDate("DT_INIZIO")
dDtFine = rsDate("DT_FINE")

sSQL2="SELECT MAX(DT_FINEPRO) AS MASSIMO FROM PROROGA_STAGE WHERE" &_
	 " ID_RICHSTAGE=" & sRic & " AND ID_PERSONA=" & IDP

'PL-SQL * T-SQL  
SSQL2 = TransformPLSQLToTSQL (SSQL2) 
set rsDate = cc.execute(sSQL2)

if not rsDate.eof then
	sMassimo = rsDate("MASSIMO") & "*"
	if sMassimo <> "*" then
		dDtFine = rsDate("MASSIMO")
	end if
end if

set rsDate = nothing

Response.Write "<Center>"
	
Response.Write 	"<table border='0' cellpadding='0' cellspacing='1' width='500'>"
		Response.Write "<tr height='20'>"
			Response.Write "<td class='tbltext1' >"
			Response.Write "<b>Iniziata Il*</b><br>(gg/mm/aaaa)"
			Response.Write "</td>"
			Response.Write "<td>"
			Response.Write "<input type='text' style='TEXT-TRANSFORM: uppercase' class='textblacka' name='txtDataInizio' size='10' maxlength='10' value='" & dDtInizio & "' >"
		Response.Write "</td>"
		Response.Write "<td>&nbsp;"
		Response.Write "</td>"

		Response.Write "</tr>"
	
	
		Response.Write "<tr height=8>"
			Response.Write "<td colspan='3'>"
			Response.Write "</td>"
		Response.Write "</tr>"

		Response.Write "<tr>"
			Response.Write "<td colspan='3' height='2' bgcolor='#3399cc'><td>"
		Response.Write "</tr>"
	
		Response.Write "<tr height='15'>"
			Response.Write "<td colspan='3'><td>"
		Response.Write "</tr>"
	
		Response.Write "<tr>"
			Response.Write "<td colspan='3'>"
			Response.Write "<span class='textblack'>"
			Response.Write "Nel caso l'esperienza fosse attualmente in corso non compilare il campo <b>Conclusa il</b><br>e <b>Motivo del Termine."
			Response.Write "&nbsp; </b>Altrimenti compilare obbligatoriamente entrambi."
			Response.Write "</span>"
			Response.Write "</td>"
		Response.Write "</tr>"
		
		Response.Write "<tr heigth='15'>"
			Response.Write "<td colspan='3'>"
			Response.Write "</td>"
		Response.Write "</tr>"
		Response.Write "<tr>"
			Response.Write "<td class='tbltext1' ><b>Conclusa Il</b><br>(gg/mm/aaaa)</td>"
			Response.Write "<td align=left colspan='2'>"
			Response.Write "<input type='text' style='TEXT-TRANSFORM: uppercase' class='textblacka' name='txtDataFine' size='10' maxlength='10' value='" & dDtFine & "'>"
			Response.Write "</td>"
		Response.Write "</tr>"

			Response.Write "<tr height='15'>"
				Response.Write "<td colspan='3'>"
				Response.Write "</td>"
			Response.Write "</tr>"


		Response.Write "<tr>"

			Response.Write "<td  class='tbltext1' >"
				Response.Write "<b>Motivo del Termine"
			Response.Write "</td>"
			Response.Write "<td colspan='2'>"

				EMotivoCess = "MCESS|0|" & date & "||Mcess| ORDER BY DESCRIZIONE"			
				CreateCombo(EMotivoCess)
		
			Response.Write "</td>"
		Response.Write "</tr>"

			Response.Write "<tr height='15'>"
				Response.Write "<td colspan='3'>"
				Response.Write "</td>"
			Response.Write "</tr>"

			Response.Write "<tr>"
				Response.Write "<td colspan='3' height='2' bgcolor='#3399cc'><td>"
			Response.Write "</tr>"
			Response.Write "<tr height='15'>"
				Response.Write "<td colspan='3'><td>"
			Response.Write "</tr>"

		
	SqlSettori = " SELECT ID_SETTORE,DENOMINAZIONE FROM SETTORI ORDER BY DENOMINAZIONE"

				
'PL-SQL * T-SQL  
SQLSETTORI = TransformPLSQLToTSQL (SQLSETTORI) 
		Set Rs = Cc.Execute (SqlSettori)
		if not rs.eof then Settori = Rs.getrows()
		
	If IsArray(settori) then
			Response.Write "<tr  height='20'>"
			Response.Write "<td class='tbltext1'><b>Settore Merceologico*</td>"
			Response.Write "<td colspan='2'>"

				Response.Write "<SELECT class='textblack' name='Settori'>"
				Response.Write "<option value=''></option>"
				for x = 0 to ubound(settori,2)
					Response.Write "<option value='" & settori(0,x) & "'>" & left(settori(1,x),50) & "</option>"
				next
				Response.Write "</SELECT>"
			Response.Write "</td>"
			Response.Write "</tr>"
	End if
	rs.close
	Set rs=Nothing
		
			Response.Write "<tr  height='20'>"
			Response.Write "<td class='tbltext1'><b>Tipologia Esperienza* </td>"
			Response.Write "<td colspan='2'>"

				EEsperienza = "TESPR|0|" & date & "|03|CmbCOD_ESPERIENZA'disabled onChange='return ActivInput(this.value);'|" & CondizTespr &  " ORDER BY DESCRIZIONE"			
					CreateCombo(EEsperienza)
				
			Response.Write "</td>"
			Response.Write "</tr>"
		

	    	Response.Write "<tr>"
				Response.Write "<td class='tbltext1'><b>Presso</td>"
				Response.Write "<td colspan='2'>"
				Response.Write "<input type=text style='text-transform: uppercase; width:300px;' class='textblacka'   name='Rag_Soc' maxlength='50' >"
				Response.Write "</td>"
	    	Response.Write "</tr>"


			Response.Write "<tr  height='20'>"
				Response.Write "<td class='tbltext1'>"
				Response.Write "<b>Forma Giuridica</td>"
			Response.Write "<td colspan='2'>"


			EFormaGiuridica = "FGIUR|0|" & date & "||CmbCOD_FORMA' width='10|ORDER BY DESCRIZIONE"			
					CreateCombo(EFormaGiuridica)


			Response.Write "</td>"
			Response.Write "</tr>"

			Response.Write "<tr  height='20'>"
			Response.Write "<td class='tbltext1'><b>Dimensioni Azienda</td>"
			Response.Write "<td colspan='2'>"

			EDimensioneImpresa = "DIMAZ|0|" & date & "||CmbDimaz' width='10|ORDER BY CODICE"			
					CreateCombo(EDimensioneImpresa)


			Response.Write "</td>"
			Response.Write "</tr>"
				
		%>
		<tr>
			<td colspan="3">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="3" height="2" bgcolor="#3399CC"></td>
		</tr>
		<tr>
			<td colspan="3">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="3" class="textblack">
			<small>Inserisci di seguito parte della denominazione del <b>Ruolo ricoperto</b> 
			che intendi cercare nella classificaizone ISTAT.<br> 
			Puoi anche inserire  in <b>Codice Istat</b>  le cifre della classificazione Istat da ricercare.
			Puoi utilizzare contemporaneamente entrambi i criteri di ricerca.<br>		
			
			Per accedere all'intero dizionario delle classificazioni delle professioni � sufficiente 
			premere il pulsante <b>RICERCA</b> senza inserire niente nella casella di testo.
			
			</small></td>
		</tr>		
		<tr>
		<td height="10" colspan="3"></td>
		</tr>
	
		<tr>
			<td class="tbltext1"><b>Ruolo ricoperto</b></td>
			<td>
			<input style="TEXT-TRANSFORM: uppercase; width:300px;" type="text" class="textblacka" size="50" maxlength="50" name="CriterioRicerca">
			</td>
			<td align="left" rowspan="2">
			<a href="javascript:ApriCodIstat()">
			<img src="<%=Session("Progetto")%>/images/lente.gif" border="0">
			</a>		
			</td>
		</tr>
		
		
		<tr>
			<td class="tbltext1" align="LEFT"><b>Codice Istat&nbsp;</b></td>
			<td>
			<input type="text" class="textblacka" maxlength="10" size="10" name="RicCodIstat" value>
			<input type="hidden" class="textblacka" maxlength="15" size="15" name="Mansione" value>
			</td>
		</tr>
	    <tr>
			<td class="tbltext1"><b>Mansione*</td>
			<td colspan="2">
				<input type="text" style="text-transform: uppercase;" class="textblacka" readonly name="DescrMansione" maxlength="100" size="60">
			</td>
	    </tr>

	<%
		if TIPOLOGIA = "ESPRO" or TIPOLOGIA = "" then
			Response.Write "<tr  height='20'>"
			Response.Write "<td class='tbltext1'><b>Responsabilit� persona</td>"
			Response.Write "<td colspan='2'>"
			Response.Write "<input Name='resp_pers' type='checkbox' value='S'>"
			Response.Write "</td>"
			Response.Write "</tr>"
		end if 


		Response.Write "<tr height='15'>"
			Response.Write "<td colspan='3'><td>"
		Response.Write "</tr>"

		Response.Write "<tr>"
			Response.Write "<td colspan='3' height='2' bgcolor='#3399cc'><td>"
		Response.Write "</tr>"
	
		Response.Write "<tr height='15'>"
			Response.Write "<td colspan='3'><td>"
		Response.Write "</tr>"
		
		Response.Write "<tr>"
			Response.Write "<td colspan='3'>"
			Response.Write "<span class='textblack'>"
			Response.Write "Se conosciuti, indica il Contratto Collettivo Nazionale Applicato "
			Response.Write " e il Livello  di inquadramento."

			Response.Write "</span>"
			Response.Write "</td>"
		Response.Write "</tr>"
		
		Response.Write "<tr heigth='15'>"
			Response.Write "<td colspan='3'>"
			Response.Write "</td>"
		Response.Write "</tr>"
		
		Response.Write "<tr  height='20'>"
			Response.Write "<td class='tbltext1'><b>Contratto Nazionale</td>"
			Response.Write "<td colspan='2'>"

			EContrattoNazionale = "TCCNL|0|" & date & "||CmbTccnl|ORDER BY DESCRIZIONE"			
			CreateCombo(EContrattoNazionale)


			Response.Write "</td>"
		Response.Write "</tr>"

		Response.Write "<tr height='15'>"
				Response.Write "<td colspan='3'>"
				Response.Write "</td>"
		Response.Write "</tr>"
			
			Response.Write "<tr height='20'>"
			Response.Write "<td class='tbltext1'><b>Inquadramento</td>"
			Response.Write "<td colspan='2'>"


			EContrattoLivello = "LCCNL|0|" & date & "||CmbLccnl|ORDER BY DESCRIZIONE"			
			CreateCombo(EContrattoLivello)	

			Response.Write "</td>"
			Response.Write "</tr>"
		
	
	
			Response.Write "<tr>"
				Response.Write "<td colspan='3' height='2' bgcolor='#3399cc'><td>"
			Response.Write "</tr>"
		
		Response.Write "<tr height='15'>"
			Response.Write "<td colspan='3'><td>"
		Response.Write "</tr>"
		
	
			Response.Write "<tr>"
				Response.Write "<td colspan='3'>"
				Response.Write "<span class='textblack'>"
				Response.Write "Solo nel caso di lavoro autonomo o come libero professionista indicare la"
				Response.Write  " Tipologia prevalente di utenza. "
				Response.Write "</span>"
				Response.Write "</td>"
			Response.Write "</tr>"
		
			Response.Write "<tr heigth='15'>"
				Response.Write "<td colspan='3'>&nbsp;"
				Response.Write "</td>"
			Response.Write "</tr>"
	
	
		
				Response.Write "<tr height='20'>"
				Response.Write "<td class='tbltext1'><b>Tipologia Cliente&nbsp;</td>"
				Response.Write "<td colspan='2'>"

					ETipoCLiente = "TICLI|0|" & date & "||CmbTicli'disabled width='10 |ORDER BY DESCRIZIONE"			
					CreateCombo(ETipoCLiente)
		
				Response.Write "</td>"
				Response.Write "</tr>"
		
		
			Response.Write "<tr height='20'>"
			Response.Write "<td colspan='3'>&nbsp;</td>"
			Response.Write "</tr>"
			
	%>

	<tr>
		<td align="middle" colspan="3">
		<a href="javascript:self.close()" onmouseover="javascript: window.status=' '; return true"><img alt="Chiudi" border="0" src="<%=Session("Progetto")%>/images/chiudi.gif"></a>
		<input type="image" src="<%=Session("Progetto")%>/images/conferma.gif" border="0" value="Salva" id="image1" name="image1">
		</td>
	</tr>

</table>
</form>

</center>
<!--#include Virtual = "/include/closeconn.asp"-->
