<!--#include Virtual = "/strutt_testa2.asp"-->
<!-- #include virtual="/include/OpenConn.asp" -->
<!-- #include virtual="/include/DecCod.asp" -->
<script language="javascript" src="/include/help.inc"></script>

<%
'If Not ValidateService(Session("IdUtente"),"BDD_InsDoc",cc) Then 
	'response.redirect "/util/error_login.asp"
'End If
nomestagista = Request.Form("txtNome")
cognstagista=Request.Form("txtCognome")
idstagista=Request.Form("txtIdPers")
Response.Write "qqqqqqqtttt" & nomestagista & "gfgdfghdfgh"	& cognstagista
	SP = Session("Progetto")
	SP = replace(SP,"/","\")
Session("PosDoc") = Server.MapPath("\") & SP & "\DOCPRG\"
		
'	Response.Write Session("PosDoc")
%> 
<script language="Javascript" src="/include/ControlDate.inc"></script>
<script language="Javascript" src="/include/ControlNum.inc"></script>
<script language="Javascript" src="/include/ControlString.inc"></script>
<script language="Javascript" src="Controlli.js"></script>
 
<script language="Javascript">

function riempi()
	{
	document.forma.ragione.value = document.forma.NOMEAZOSP.value
	document.forma.nrag.value = document.forma.RAGSOC.value
	}

</script>

<br>

<!--- TITOLO PAGINA ---->

<center>
<form method="post" name="forma" onsubmit="return invia()" action="GES_Risultati.asp">
<input type="hidden" value="<%=nomestagista%>" name="hnomestagista">
<input type="hidden" value="<%=cognstagista%>" name="hcognstagista">
<input type="hidden" value="<%=idstagista%>" name="hidstagista">

<table cellSpacing="4" align="center" cellPadding="4" width="500" border="1"> 
<tr>
<td class="tbltext">
<p align="center">
MODELLO DI CONVENZIONE <br>
<b>CONVENZIONE DI TIROCINIO DI FORMAZIONE ED ORIENTAMENTO <br>
TRA</B>
</p>
<p align="justify">
Il/la&nbsp;<input class="textred" id="RAGSOC" size="15" name="RAGSOC" maxlength="15" onchange="javascript:riempi()" >&nbsp;(soggetto promotore)
con sede in <input class="textred" id="SEDE" size="35" name="SEDE" maxlength="35">,<br>
codice fiscale <input class="textred" id="CODFISC" size="16" name="CODFISC" maxlength="16">
d�ora in poi denominato "soggetto promotore", rappresentato/a dal sig.
<input class="textred" id="RAPCOGNOME" size="15" name="RAPCOGNOME" maxlength="15">(COGNOME)<input class="textred" id="RAPNOME" size="15" name="RAPNOME" maxlength="15">(NOME)
nato a <input class="textred" id="Born" size="25" name="Born" maxlength="35"> il <input class="textred" id="dataimm" name="dataimm" size="11" maxlength="10">;<br>
</p>
<p align="center">
<b>E</b>
</p>
<p align="justify">
<input class="textred" id="NOMEAZOSP" size="15" name="NOMEAZOSP" maxlength="15"  onchange="javascript:riempi()">(denominazione dell�azienda ospitante)
con sede legale in <input class="textred" id="SEDEAZOSP" size="15" name="SEDEAZOSP" maxlength="15">,
codice fiscale <input class="textred" id="CODFISCaz" size="16" name="CODFISCaz" maxlength="16"> 
d�ora in poi denominato "soggetto ospitante", rappresentato/a dal sig. 
<input class="textred" id="COGN" size="15" name="COGN" maxlength="15">&nbsp;(COGNOME)&nbsp;<input class="textred" id="NOME" size="15" name="NOME" maxlength="15">&nbsp;(NOME),<br>
nato a &nbsp;<input class="textred" id="NATO" size="25" name="NATO" maxlength="35"> il <input class="textred" id="datanasc" name="datanasc" size="11" maxlength="10">;<br>
</p>
<p align="center">
<b>Premesso</b>
</p>
<br>

che al fine di agevolare le scelte professionali mediante la conoscenza diretta 
del mondo del lavoro e realizzare momenti di alternanza tra studio e lavoro 
nell�ambito dei processi formativi i soggetti richiamati all�art. 18, comma 1, 
lettera a), della legge 24 giugno 1997, n. 196, possono promuovere tirocini di 
formazione ed orientamento in impresa a beneficio di coloro che abbiano gi� 
assolto l�obbligo scolastico ai sensi della legge 31 dicembre 1962, n. 1859.<br><br>

Si conviene quanto segue:<br>
<b>Art. 1.</b><br>
Ai sensi dell�art. 18 della legge 24 giugno 1997, n. 196 
la <input class="textred" id="ragione" size="15" name="ragione" maxlength="15">(riportare la denominazione dell�azienda ospitante)
si impegna ad accogliere presso le sue strutture soggetti in
tirocinio di formazione ed orientamento su proposta di <input class="textred" id="nrag" size="15" name="nrag" maxlength="15"> (riportare la denominazione del soggetto promotore),
 ai sensi dell�art. 5 del decreto attuativo dell�art. 18 della legge n. 196 del 1997.<br>

<b>Art. 2.</b><br>
Il tirocinio formativo e di orientamento, ai sensi 
all�art. 18, comma 1, lettera d), della legge n. 196
 del 1997 non costituisce rapporto di lavoro. 
Durante lo svolgimento del tirocinio l�attivit� di 
formazione ed orientamento � seguita e verificata da un 
tutore designato dal soggetto promotore in veste di responsabile 
didattico-organizzativo, e da un responsabile aziendale, indicato
 dal soggetto ospitante. 
Per ciascun tirocinante inserito nell�impresa ospitante in base 
alla presente Convenzione viene predisposto un progetto formativo. <br>

<b>Art. 3.</b><br>
Durante lo svolgimento del tirocinio formativo e
 di orientamento il tirocinante � tenuto a: 
q svolgere le attivit� previste dal progetto formativo
 e di orientamento; 
q rispettare le norme in materia di igiene e sicurezza e salute
 sui luoghi di lavoro; 
q mantenere la necessaria riservatezza per quanto attiene ai dati,
 informazioni o conoscenze in merito a processi produttivi e prodotti, 
 acquisiti durante lo svolgimento del tirocinio.<br>

<b>Art. 4.</b><br>
Il soggetto promotore assicura il/i tirocinante/i contro gli infortuni sul
 lavoro presso l�Inail, nonch� per la responsabilit� civile presso compagnie
  assicurative operanti nel settore. In caso di incidente durante lo svolgimento
   del tirocinio, il soggetto ospitante si impegna a segnalare l�evento, entro 
   i tempi previsti dalla normativa vigente, agli istituti assicurativi 
   (facendo riferimento al numero della polizza sottoscritta dal soggetto
    promotore) ed al soggetto promotore. 
Il soggetto promotore si impegna a far pervenire alla regione o 
alla provincia delegata, alle strutture provinciali del Ministero 
del lavoro e della previdenza sociale competenti per territorio in 
materia di ispezione, nonch� alle rappresentanze sindacali copia della
 Convenzione di ciascun progetto formativo e di orientamento. <br>

<b>Art. 5.</b><br>
Il datore di lavoro che ospita lo stagista si impegna a:
q garantire allo stagista l�assistenza e la formazione necessarie al buon esito dello stage; 
q rispettare le norme antinfortunistiche e di igiene sul lavoro; 
q consentire al tutor dell�ente promotore di contattare lo stagista e il tutor aziendale per verificare l�andamento dello stage e per la stesura della relazione finale; 
q informare l�ente promotore di qualsiasi incidente possa accadere al tirocinante. <br>

<b>Art. 6</b><br>
La presente convenzione decorre dalla data sottoindicata e ha durata di 6 mesi .<br><br>


(luogo)..............................., (data) ..............................................<br><br>


(Firma per il soggetto promotore) ............................................................<br>

(Firma per il soggetto ospitante) ..............................................................<br>

</p>
</td>
</tr>  
</table>

<!--- PULSANTE INVIA ---->
<br><br>
<table cellSpacing="4" cellPadding="4" width="500" align="center" border="0">
	<tr>
		<td align="middle" colspan="3">
			<input type="image" id="IMG1" alt src="<%=Session("Progetto")%>/images/conferma.gif" border="0" WIDTH="55" HEIGHT="40">
		</td>
	</tr>  
</table>
<br>
</form>


<!-- #include virtual="/include/closeconn.asp" -->
<!--#include Virtual = "/strutt_coda2.asp"-->
