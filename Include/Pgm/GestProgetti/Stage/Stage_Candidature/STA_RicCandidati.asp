<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include Virtual = "/strutt_testa2.asp"-->
<!--#include Virtual ="/Include/openconn.asp"-->
<!--#include virtual="/include/SelAreaTerrBandi.asp"-->
<!--#include Virtual = "/include/ElabRegole.asp"-->

<script language="javascript">

<!--#include virtual = "/Include/ControlDate.inc"-->

//include del file per fare i controlli sulla numericitÓ dei campi
<!--#include virtual = "/Include/ControlNum.inc"-->
<!--#include virtual = "/Include/help.inc"-->

function AssTutor(ID){
	document.frmAss.txtIdPers.value = ID;
	document.frmAss.submit(); 	  
}

function apriNote(IDRIC){
	codice = IDRIC   
	windowArea = window.open ('STA_InsNote.asp?idRic=' + codice ,'Info','width=750,height=500,Resize=No,Scrollbars=yes')	
  
}

function contrRicerca(){
	var num = document.frmSelezione.txtContatore.value;
    var valSel="0";
    for(i=1; i<=num;i++){
       ok=eval("document.frmSelezione.ckSta" + i + ".checked");
       if (ok==true){
          valSel= "1";
       }
    }  
    if (valSel=="0"){
	    alert("Selezionare almeno un candidato");
	    return false
    }
}
</script>

<%	
'********************************
'*********** MAIN ***************
	
dim sIdRich,sAreaProf,sBando,sProv,sSceltaArea,eleProv,nIdFigura,nTipo

sIdRich = Request.Form ("txtIdRichiesta")
sAreaProf = Request.Form ("txtArea")
sBando = Request.Form ("cmbBando")
sProv = Request.Form ("cmbProv")
sSceltaArea = Request.Form ("ckArea")

if sProv = "TUTTE" then
    sSetProv = SetProvUorg(Session("idUorg"))
	nProvSedi = len(sSetProv)
	eleProv=""
	pos_ini = 1 	                
	for i=1 to nProvSedi/2
		sTarga = mid(sSetProv,pos_ini,2)
		eleProv= eleProv & "'" & sTarga & "',"															
		pos_ini = pos_ini + 2
	next
	lung =len(eleProv)
    sProv= mid(eleProv,1,lung-1)
else
    sProv = "'" & sProv & "'"
end if

if sBando = "" then
	sSql="SELECT ID_BANDO FROM BANDO A WHERE EXISTS" &_
     "(SELECT ID_BANDO FROM AREA_BANDO B WHERE A.ID_BANDO=B.ID_BANDO AND ID_UORG = " & session("iduorg") & ")" &_
     " ORDER BY DESC_BANDO"

'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
    set rsId = cc.execute(sSQL)

    if not rsId.eof then
        sAppoId=""
        do while not rsId.eof
			sAppoId = sAppoId & rsId("ID_BANDO") & "," 
			rsId.movenext   
        loop
        lung =len(sAppoId)
        sBando= mid(sAppoId,1,lung-1)
    end if
    set rsId = nothing
end if

Inizio()
			
Imposta_Pag()

'**********************************************************************************************************************************************************************************	
	
Sub Inizio()
%>
<!-- Lingetta superiore 	Scrittura dell'intestazione della pagina-->
	
	<table border="0" width="520" cellspacing="0" cellpadding="0" height="70" align="center">
		<tr>
			<td width="500" background="<%=session("progetto")%>/images/titoli/strumenti2b.gif" height="70" valign="bottom" align="right">
				<table border="0" background width="500" height="23" cellspacing="0" cellpadding="0">
					<tr>
						<td width="100%" valign="top" align="right"><b CLASS="tbltext1a">Gestione Candidature
						  </b></td>
					</tr>
				</table>
			</td>
		</tr>
	</table> 
	<br>
	<table border="0" CELLPADDING="0" CELLSPACING="0" width="500" align="center">
		<tr height="18">
			<td class="sfondomenu" width="67%" height="18"><span class="tbltext0"><b>&nbsp;RICERCA CANDIDATI </b></span></td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
		  	<td valign="middle" align="right" class="tbltext1" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
		</tr>
	</table>
	<!-- Commento -->
	<table border="0" CELLPADDING="0" CELLSPACING="0" width="500" align="center">
		<tr>
			<td align="left" class="sfondocomm">
                     Elenco dei candidati che hanno dato la propria disponibilitÓ ad effettuare stage/tirocini. <br>
				<a href="Javascript:Show_Help('/pgm/help/gestprogetti/stage/Stage_Candidature/STA_VisCandidati')"><img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
			</td>
		</tr>
		<tr height="2">
			<td class="sfondocommaz" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>
	<br>
<%	
End Sub

Sub Imposta_Pag()
	
	'''LC**:26/09/2003 
	sqlDati =	"SELECT DS.DESC_STAGE, I.RAG_SOC, SI.DESCRIZIONE, AP.DENOMINAZIONE,DS.ID_FIGPROF,DS.COD_TIPO_RICERCA " &_
				"FROM DOMANDA_STAGE DS, IMPRESA I, SEDE_IMPRESA SI, AREE_PROFESSIONALI AP " &_
				"WHERE ID_RICHSTAGE=" & sIdRich &_
				" AND DS.ID_SEDE = SI.ID_SEDE" &_
				" AND SI.ID_IMPRESA = I.ID_IMPRESA" &_
				" AND DS.ID_AREAPROF = AP.ID_AREAPROF"
'PL-SQL * T-SQL  
SQLDATI = TransformPLSQLToTSQL (SQLDATI) 
	set rsDati = CC.Execute(sqlDati)
	if not rsDati.eof then %>
		<table width="500" align="center" border="0" cellspacing="2" cellpadding="2" class="sfondocommaz">
            <tr height="20"> 
			  	<td align="left" width="150">
					<b class="tbltext1">Denominazione Azienda</b>
				</td>
			  	<td class="textblack" align="left" width="350">
					<b><%=rsDati("RAG_SOC")%></b>&nbsp;-&nbsp;<%=rsDati("DESCRIZIONE")%>
				</td>
			</tr>
            <tr height="20"> 
			  	<td align="left" width="150">
					<b class="tbltext1">Riferimento Richiesta</b>
				</td>
			  	<td class="textblack" align="left" width="350">
					<b><%=sIdRich%></b>
				</td>
			</tr>
            <tr height="20"> 
			  	<td align="left" width="150">
					<b class="tbltext1">Descrizione Stage</b>
				</td>
			  	<td class="textblack" align="left" width="350">
					<%=rsDati("DESC_STAGE")%>
				</td>
			</tr>
			<tr height="20"> 
			  	<td align="left" width="150">
					<b class="tbltext1">Area Professionale</b>
				</td>
			  	<td class="textblack" align="left" width="350">
					<%=rsDati("DENOMINAZIONE")%>
				</td>
			</tr>
			 <tr height="20"> 
				<td align="left" width="150">
					<b class="tbltext1">Figura Professionale</b>
				</td>
				<td class="textblack" align="left" width="350">
				
					<%nIdFigura = clng(rsDati("ID_FIGPROF"))
		            
		            sql ="SELECT DENOMINAZIONE FROM FIGUREPROFESSIONALI WHERE ID_FIGPROF=" & nIdFigura

'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
					SET rs = cc.execute(sql)

					sDenominazione = rs("DENOMINAZIONE")

					set rs = nothing
		            
					Response.Write  sDenominazione%>				
				
			</td>						
		</tr>
		 <tr height="20"> 
			<td align="left" width="150">
				<b class="tbltext1">Tipo di Ricerca</b>
			</td>
			<td class="textblack" align="left" width="350">
				
					<%nTipo = clng(rsDati("COD_TIPO_RICERCA"))
					  if nTipo = 0 then 
							Response.Write "PER AREA PROFESSIONALE"
					  else
							Response.Write "PER PROFILO PROFESSIONALE"
					  end if		%>				
			
			</td>						
		</tr>
		</table>
<%
	end if
	rsdati.close
	set rsDati=nothing
	'''FINE LC**
	
	
	sSQL ="SELECT ID_PERSONA,COGNOME,NOME,E_MAIL,FLOOR(MONTHS_BETWEEN(SYSDATE,DT_NASC)/12) AS ETA" &_
	      " FROM PERSONA P WHERE PRV_RES IN(" & sProv & ")" &_
	      " AND ID_PERSONA NOT IN(SELECT ID_PERSONA FROM CAND_DOMSTAGE WHERE ID_RICHSTAGE=" & sIdRich & ")" &_
          " AND ID_PERSONA NOT IN(SELECT ID_PERSONA FROM PERS_STAGE WHERE DT_SOSPENSIONE IS NULL)" &_
          " AND ID_PERSONA IN(SELECT ID_PERSONA FROM PERS_DISPON WHERE ID_DISPON IN (59,29))"
   
    sSQL = sSQL & CreaCriteriStage(sIdRich,CC)

'    if sSceltaArea = "S" then
'        sSQL= sSQL & " AND ID_PERSONA IN (SELECT ID_PERSONA FROM PERS_AREAPROF" &_
'                    " WHERE ID_AREAPROF =" & sAreaProf & ")"
'    end if
   
 if sSceltaArea = "S" then  
    if nTipo = 1 then		'Ricerca per figura professionale
		sSQL= sSQL & " AND ID_PERSONA IN (SELECT ID_PERSONA FROM PERS_FIGPROF" &_
                     " WHERE ID_FIGPROF =" & nIdFigura & ")"
	else							'Ricerca per area professionale
		 sSQL= sSQL & " AND ID_PERSONA IN (SELECT ID_PERSONA FROM PERS_AREAPROF" &_
                      " WHERE ID_AREAPROF =" & sAreaProf & ")"
	end if
end if


    if sBando <> "" then
        sSQL= sSQL & " AND ID_PERSONA IN (SELECT ID_PERSONA FROM DOMANDA_ISCR" &_
                    " WHERE ESITO_GRAD='S' AND ID_BANDO IN(" & sBando & "))"
    end if
    
    sSQL = sSQL & " ORDER BY COGNOME"
    
   ' Response.Write  sSQL
 

'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
    SET rsRisultato = cc.execute(sSQL)
    
    if not rsRisultato.eof then %>
       <form name="frmDett" method="post" action="STA_VisDettStage.asp">
           <input type="hidden" name="txtIdRichiesta">
       </form>
       <form name="frmAss" method="post" action="/Pgm/Gestprogetti/Stage/Stage_Asstutor/STA_AssTutorStage.asp">
           <input type="hidden" name="txtIdPers">
       </form>
       <form name="frmQuadro" method="post" action="/Pgm/bilanciocompetenze/quadro/Default.asp">
			<input type="hidden" name="IdPers">
	   </form>
		<form name="frmSelezione" method="post" action="STA_CnfRicCandidati.asp" onsubmit="return contrRicerca()">
          <input type="hidden" name="txtIdRichiesta" value="<%=sIdRich%>">
      
		<table width="500" align="center" border="0" cellspacing="2" cellpadding="1" class="sfondocommaz">
            <tr height="20"> 
			  	<td class="sfondocomm" align="middle" width="5">
					<b class="tbltext1">&nbsp;</b>
				</td>
			  	<td class="sfondocomm" align="middle" width="220">
					<b class="tbltext1">Candidato</b>
				</td>
				<td class="sfondocomm" align="middle" width="10">
					<b class="tbltext1">Eta'</b>
				</td>
				<td class="sfondocomm" align="middle" width="200">
					<b class="tbltext1">Titolo di studio</b>
				</td>
				<td class="sfondocomm" align="middle" width="60">
					<b class="tbltext1">Prossimita'</b>
				</td>
				<td class="sfondocomm" align="middle" width="5">
					<b class="tbltext1">Rinunce cand.</b>
				</td>
				<td class="sfondocomm" align="middle" width="5">
					<b class="tbltext1">Interr. stage</b>
				</td>
				<td class="sfondocomm" align="middle" width="5">
					<b class="tbltext1">Note</b>
				</td>
			</tr>
<%       cont = 0
         do while not rsRisultato.eof 
            cont = cont +1
%>			<input type="hidden" name="txtIdPers<%=cont%>" value="<%=rsRisultato("ID_PERSONA")%>">
            <tr height="20" class="tblsfondo"> 
			  	<td class="tbldett" align="middle" width="5">
			  		<b><input TYPE="checkbox" name="ckSta<%=cont%>" value="S"></b>
				</td>
			  	<td class="tbldett" align="middle" width="220">
			  		<%=rsRisultato("COGNOME")%>&nbsp;<%=rsRisultato("NOME")%>
				</td>
				<td class="tbldett" align="middle" width="10">
					<%=rsRisultato("ETA")%>
				</td>
				<%sqlT = "Select cod_liv_stud As titolo From TISTUD " &_ 
			        " where id_persona (+) =" & clng(rsRisultato("ID_PERSONA")) &_ 
			        " And cod_stat_stud (+)= 0 " &_
			        " Order by AA_stud desc, dt_tmst desc"
			
'PL-SQL * T-SQL  
SQLT = TransformPLSQLToTSQL (SQLT) 
				Set RRT = CC.Execute(sqlT)
				if RRT.Eof then
					sTitolo = "-"
				else
					RRT.Movefirst
					sTitolo = RRT.fields("titolo")
				end if
				RRT.Close
				set RRT = nothing
				if sTitolo <> "-" then
				    sTitolo = lcase(DecCodVal("LSTUD",0,"",sTitolo,""))
				end if
				%>
				<td class="tbldett" align="middle" width="200">
					<%=sTitolo%>
				</td>
				<td class="tbldett" align="middle" width="60">
					<b>
				<%sqlGrado = " select Grado_area_pers "	&_
				             " from pers_areaprof " &_		
				             " Where id_persona = " &  clng(rsRisultato("ID_PERSONA")) &_
				             " and  Ind_status = 0 " 
				             
'PL-SQL * T-SQL  
SQLGRADO = TransformPLSQLToTSQL (SQLGRADO) 
				  set RRGrado = CC.Execute(sqlGrado)
						if not RRGrado.Eof then
						    if RRGrado.Fields("GRADO_AREA_PERS") > "0" then	
							    Response.Write	"<a href=""Javascript:document.frmQuadro.IdPers.value=" & clng(rsRisultato("ID_PERSONA")) & "; document.frmQuadro.submit()"" onmouseover=""javascript:window.status=' '; return true"">" &_
							     "<img src='" & session("Progetto") & "/images/GRAF.gif' border=0 alt='Bilancio Competenze'></a><b> " & RRGrado.Fields("GRADO_AREA_PERS") & "%</b>"
				            else
							    Response.Write "<span class=textblack> - </span> "
							end if
						else
							Response.Write "<span class=textblack> - </span> "
						end if
			
						RRGrado.Close
				  set RRGrado = nothing           
%>				
				   </b>
				</td>
<%
                '''LC**:26/09/2003 - Modificate le 2 query per conteggio "rinunce" e "interruzioni".
                '''sSQLC =	"SELECT COUNT(ID_RICHSTAGE) AS TOT FROM CAND_DOMSTAGE WHERE ID_PERSONA = " & rsRisultato("ID_PERSONA") &_
                '''			" AND DT_CANDAL IS NULL" 
				sSQLC =	"SELECT COUNT(ID_RICHSTAGE) AS TOT FROM CAND_DOMSTAGE WHERE ID_PERSONA = " & rsRisultato("ID_PERSONA") &_
						" AND DT_CANDAL IS NOT NULL" &_
						" AND COD_STPLA IN ('03','05')"
'PL-SQL * T-SQL  
SSQLC = TransformPLSQLToTSQL (SSQLC) 
				set rsCount = cc.execute(sSQLC)
%>				
				<td class="tbldett" align="middle" width="5">
					<%=rsCount("TOT")%>
				</td>
<%              set rsCount = nothing
                '''sSQLC =	"SELECT COUNT(ID_RICHSTAGE) AS TOT FROM PERS_STAGE WHERE ID_PERSONA = " & rsRisultato("ID_PERSONA") &_
                '''			" AND DT_FINE IS NOT NULL" 
				sSQLC =	"SELECT COUNT(ID_RICHSTAGE) AS TOT FROM PERS_STAGE WHERE ID_PERSONA = " & rsRisultato("ID_PERSONA") &_
						" AND DT_SOSPENSIONE IS NOT NULL" &_
						" AND COD_ESPLA IN ('01','02','03')"
'PL-SQL * T-SQL  
SSQLC = TransformPLSQLToTSQL (SSQLC) 
				set rsCount1 = cc.execute(sSQLC)
				'''FINE LC**.
%>					
				<td class="tbldett" align="middle" width="5">
				    <%=rsCount1("TOT")%>
				</td>
				<td align="center" width="80">
					<a title="Segnalazioni" href="javascript:apriNote('<%=rsRisultato("ID_PERSONA")%>')" ID="imgPunto1" name="imgPunto1" onmouseover="javascript:window.status='';return true"><img border="0" src="<%=Session("Progetto")%>/images/scheda.gif"></a>
				</td>	
			</tr>
<%          set rsCount1 = nothing
            rsRisultato.movenext
        loop
%>		<input type="hidden" name="txtContatore" value="<%=cont%>">
			
		</table>
		<br><br>
		<table border="0" cellpadding="0" cellspacing="1" width="300">
			<tr>
				<td nowrap>
			       <input type="image" name="Conferma" src="<%=Session("progetto")%>/images/conferma.gif">
			    </td value="Registra">
				<td align="center">
					<a href="javascript:history.go(-1)">
						<img src="<%=session("Progetto")%>/images/indietro.gif" border="0" name="imgPunto2" onmouseover="javascript:window.status=' '; return true">
					</a>
				</td>
			</tr>
		</table>
		</form>
<%  else
       Testo = "Non ci sono Candidati per lo stage selezionato"
    
%>		
		<br><br>
		<table width="500" align="center" border="0" cellspacing="2" cellpadding="1" class="sfondocommaz">
			<tr height="20"> 
			  	<td class="tbltext3" align="middle" width="500">
					<b><%=Testo%></b>
				</td>
			</tr>
		</table>
		<br>
		<br>
		<table border="0" cellpadding="0" cellspacing="1" width="500">
			<tr>
				<td align="center">
					<a href="javascript:history.go(-1)">
						<img src="<%=session("Progetto")%>/images/indietro.gif" border="0" name="imgPunto2" onmouseover="javascript:window.status=' '; return true">
					</a>
				</td>
			</tr>
		</table>
		<br>
<%end if %>		
		
<%				
	End Sub
%>	
<!--#include Virtual ="/Include/closeconn.asp"-->
<!--#include Virtual="/strutt_coda2.asp"-->		

