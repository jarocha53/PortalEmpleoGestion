<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%if session("progetto")<>"" then%>

	<!-- #include virtual="/include/openconn.asp" -->
	<!--#include virtual = "/include/ControlDateVb.asp"-->

	<html>
	<head>
	<title>Segnalazioni</title>
	<link REL="STYLESHEET" TYPE="text/css" HREF="<%=session("progetto")%>/fogliostile.css">
	<script language="javascript">
	<!-- #include virtual="/include/help.inc" -->
	
	function Chiudi()
	{
		self.close()
	}

    function Invia()
	{  
	   frmNote.submit()
	   self.close()
	}

	</script>
	</head>
	<% 
	sRich = Request.QueryString ("idRic")
	testo = Request.QueryString ("obiet")
		
	sFilePath = SERVER.MapPath("\") & session("progetto") & "\DocPers\Temp\" & mid(session("progetto"),2) & "_" & sRich & ".rf"
   

	function ScriviFile(testo)
		set oFileSystemObject = Server.CreateObject("Scripting.FileSystemObject")
		set oFileRepFas = oFileSystemObject.CreateTextFile(sFilePath,true)
		oFileRepFas.WriteLine(cstr(testo))
	end function

	function Legge()
		set oFileSystemObject = Server.CreateObject("Scripting.FileSystemObject")

		if oFileSystemObject.FileExists(sFilePath) then
			set oFileRepFas = oFileSystemObject.OpenTextFile(sFilePath,1)
			Do While Not oFileRepFas.AtEndOfStream
				Legge = Legge & Chr (13) & oFileRepFas.ReadLine
			loop
		end if
	end function
   
	if Request.QueryString ("Insert") = "true" then
	   ScriviFile testo
	   Response.Write("<script>self.close()</script>")
	else
		ValTesto = Legge()
	end if
%>
	<center>
	<body onContextMenu="return false;">

	<br>
	<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
		<tr height="17">
			<td class="sfondomenu" width="35%" height="18"><span class="tbltext0">
				<b>&nbsp;NOTE STAGE</b>
			</td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">
			</td>
			<td valign="middle" align="right" class="tbltext1" width="76%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
		</tr>
		<tr>
			<td class="sfondocomm" width="57%" colspan="3">
				Visualizzazione delle segnalazioni stage.<br>
				Premere <b>Chiudi</b> per uscire.
				<input type="image" align="right" src="/images/help.gif" border="0" onclick="Show_Help('/Pgm/help/gestprogetti/stage/Stage_Candidature/STA_InsNote')" id="image3" name="image3">
			</td>
		</tr>
		<tr height="2">
			<td class="sfondocommaz" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
		<tr>
			<td>
			&nbsp;
			</td>
		</tr>
		<tr>
			<td class="tbltext1" align="right" width="100%" colspan="4">
				<b>Situazione al:</b>
			</td>
		</tr>
		<tr>	
			<td class="textblack" align="right" width="100%" colspan="4">
				<b><%=ConvDateToString(date())%></b>
			</td>
		</tr>
	</table>

	<form name="frmNote" method="get" action"STA_InsNote.asp">

	<input type="hidden" id="idRic" name="idRic" value="<%=sRich%>">
	<input type="hidden" id="Insert" name="Insert" value="true">
		

	<table align="center">
		<tr>
			<td class="textdesciz">
				<textarea class="textblu" name="obiet" cols="50" rows="10"><%=ValTesto%></textarea>
			</td>
		</tr>
	</table>

	</form>

	<table width="100%" cellspacing="2" cellpadding="1" border="0">
		<tr align="center">
			<td>
				<input type="image" src="<%=Session("progetto")%>/images/chiudi.gif" title="Chiudi la pagina" border="0" align="center" onclick="self.close();" id="image1" name="image1">
				&nbsp;
				<input type="image" src="<%=Session("progetto")%>/images/Conferma.gif" title="Salva la pagina" border="0" align="center" onclick="Invia();" id="image2" name="image2">
			</td>
		</tr>		
	</table>

	</body>
	</center>
	</html>
	<!-- #include virtual="/include/closeconn.asp" -->
<%else%>
	<script>
		alert("Sessione scaduta")
		self.close()
	</script>
	
<%end if%>
