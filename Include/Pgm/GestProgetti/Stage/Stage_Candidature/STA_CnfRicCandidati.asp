<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include Virtual = "/strutt_testa2.asp"-->
<!--#include Virtual = "/include/SysFunction.asp"-->
<!--#include Virtual ="/Include/openconn.asp"-->
<!--#include Virtual ="/Include/ControlDateVB.asp"-->
<!--#include virtual= "/util/dbutil.asp"-->
<%
'---------------------------------------------------------------------------------------------------------------------------------------------------------------

Sub Inizio()
%>
	<table border="0" width="520" cellspacing="0" cellpadding="0" height="70">
		<tr>
			<td width="500" background="<%=session("progetto")%>/images/titoli/servizi2g.gif" height="70" valign="bottom" align="right">
				<table border="0" background width="500" height="23" cellspacing="0" cellpadding="0">
					<tr>
						<td width="100%" valign="top" align="right"><b CLASS="tbltext1a">Gestione Candidature
						  </b></td>
					</tr>
				</table>
			</td>
		</tr>
	</table> 
	<br>
	<!-- Linguetta superiore -->
		<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
			<tr height="18">
				<td class="sfondomenu" width="67%" height="18"><span class="tbltext0"><b>&nbsp;GESTIONE RICHIESTE STAGE </b></span></td>
				<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
				<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
			</tr>
		</table>
		<!-- Commento -->
		<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
			<tr>
				<td align="left" class="sfondocomm">
					candidatura allo stage.
				</td>
			</tr>
			<tr height="2">
				<td class="sfondocommaz" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif">
				</td>
			</tr>
		</table>
	<br>
<%
End Sub 

'---------------------------------------------------------------------------------------------------------------------------------------------------------------

Function InvioCom(nIdMitUtente, nIdPersona, sOggetto, sTesto, sLink, sTestoLink)
	dim dOggi,nIdCom,sqlI,sql1,sqlC,rsComunicazione,sErrore

	dOggi = ConvDateToDB(Now())

	' ************************************************
	' Sostituisco l'apice con due per l'INSERT nel DB.
	sOggetto	= Replace(sOggetto, "'", "''") 
	sTesto		= Replace(sTesto, "'", "''") 
	
	if trim(sLink) <> "" then
		sLink = Replace(sLink, "'", "''") 
	end if
	if trim(sTestoLink) <> "" then
		sTestoLink = Replace(sTestoLink, "'", "''") 
	end if
	
	' *************************************************************
	' Controllo la massima dimensione del testi da inserire nel DB. 
	sOggetto	= Left (sOggetto,99)
	sTesto		= Left (sTesto,1999) 

	sErrore = "0"

	' ********************************************
	' Inserisce gli estremi della comunicazione.		
	' *********************************************
	sqlI="INSERT INTO COMUNICAZIONE (IDUTENTE, OGGETTO, " &_
		"TESTO_COM, LINK_COM, TESTO_LINK_COM, DT_INS_COM, " &_
		"DT_INVIO_COM, DT_TMST)" & " VALUES (" & nIdMitUtente &_
		",'" & sOggetto & "','" & sTesto & "','" & sLink & "','" &_
		sTestoLink & "'," & dOggi & "," & dOggi &_
		"," & dOggi & ")"

	sErrore = EseguiNoC(SQLI, CC)

	if sErrore <> "0" then
		InvioCom = sErrore
		exit function
	end if
	
	' ********************************************
	' Si prende 'ID della comunicazione inserita.		
	' *********************************************
	sql1 = "SELECT ID_COMUNICAZIONE from COMUNICAZIONE " &_
			" WHERE DT_INS_COM = " & dOggi & " AND IDUTENTE = " &_
			CLng(nIdMitUtente)

'PL-SQL * T-SQL  
SQL1 = TransformPLSQLToTSQL (SQL1) 
	set rsComunicazione = CC.Execute(sql1)
	
	If not rsComunicazione.EOF then
		nIdCom = rsComunicazione("ID_COMUNICAZIONE")
	else 
		sErrore = "Errore InvioCom: non � stato possibile inserire la comunicazione."
	end if
	
	rsComunicazione.Close
	set rsComunicazione = nothing

	if sErrore = "0" then			
		sSQL = "SELECT IDUTENTE FROM UTENTE WHERE " & _
				"TIPO_PERS = 'P' AND CREATOR = " & nIdPersona
		
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
		set rsUtente = CC.Execute(sSQL)
		if not rsUtente.eof then
			nIdDesUtente = rsUtente("IDUTENTE")
		else 
			sErrore = "Errore InvioCom: non � stato possibile reperire IDUTENTE."
		end if
		
		if sErrore = "0" then			
			' ****************************************
			' Scrive la comunicazione all' utente.		
			' ****************************************
			sqlC = "INSERT INTO COM_PERSONA (ID_COMUNICAZIONE, IDUTENTE, " &_
					"DT_RICEZIONE, FL_LETTO , DT_TMST)" & " VALUES (" &_
					nIdCom & ", " & nIdDesUtente & ", " &_
					"" & dOggi & ", 'N', " & dOggi & ")"

			sErrore = EseguiNoC(sqlC, CC)
		end if
		
	end if

	if sErrore <> "0" then
		InvioCom = sErrore
	else
		InvioCom = "0"
	end if
	
End function

'--------------------- MAIN --------------------------------------------------------------------------------------------------------------------------------

inizio()

dim nCont,idRichStage,sDesc,sEMail,sNome,sCognome

nCont = Request.Form("txtContatore")
idRichStage = Request.Form("txtIdRichiesta")
 
sqlSt = "SELECT DESC_STAGE FROM DOMANDA_STAGE WHERE ID_RICHSTAGE = " & idRichStage

'PL-SQL * T-SQL  
SQLST = TransformPLSQLToTSQL (SQLST) 
SET rsSt= cc.execute(sqlSt)
sDesc= rsSt("DESC_STAGE")
set rsSt = nothing
%>
  <form name="frmIndietro" method="post" action="STA_VisDettStage.asp">
       <input type="hidden" name="txtIdRichiesta" value="<%=idRichStage%>">
  </form>
<%     
 
sErrore = "0"

CC.BeginTrans
 
Dim objMail 

sSQL = "SELECT EMAIL FROM UTENTE WHERE IDUTENTE = " & session("idutente")
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
set rsUtente = CC.Execute (sSQL)

if rsUtente.eof then
	sErrore = "Operatore non trovato su utente"
else
	sMailOpe = rsUtente ("EMAIL")
end if

for i = 1 to nCont 

	sErrore = "0"
	
	valore=Request.Form("ckSta" & i)
   
	if UCase(valore) = "S" then
		sql="INSERT INTO CAND_DOMSTAGE(ID_RICHSTAGE, ID_PERSONA, " & _
			"DT_CANDDAL,COD_STPLA,DT_TMST) " &_
			"VALUES(" & idRichStage & ", " & Request.Form("txtIdPers" & i) & _
			", " & convDateTodbs(now) & ", '01', " & convDateTodbs(now) & ")"
       
		sErrore = EseguinoC(sql,CC)       

		if sErrore <> "0" then
			exit for
		end if
		
        sqlDati = "SELECT NOME, COGNOME, E_MAIL FROM PERSONA WHERE ID_PERSONA=" & Request.Form("txtIdPers" & i)
'PL-SQL * T-SQL  
SQLDATI = TransformPLSQLToTSQL (SQLDATI) 
        set rsDati = cc.execute(sqlDati)
        if not rsDati.eof then
			'sEMail = rsDati("E_MAIL")
			sCognome = rsDati("COGNOME")
			sNome = rsDati("NOME")
        end if
        
        rsDati.close
        set rsDati = nothing
			
		sAppTesto= "Gentile " & sCognome & " " & sNome & " ti informiamo " & _
					"che sei stato selezionato per partecipare allo stage " & _
					sDesc & ". Ti invitiamo a contattare l'operatore all'indirizzo: " & _
					sMailOpe & " per confermare la tua " & _
					"disponibilita' a partecipare alla selezione. " & _
					"A presto. La Redazione di " & session("progetto")
			
		sLink = "/pgm/gestprogetti/stage/Stage_Candidature/STA_DettStage.asp?IdRichiesta=" & idRichStage
		sTestoLink = "Dettagli dello stage"
			
		sErrore = InvioCom (session("IDUTENTE"), Request.Form("txtIdPers" & i), "Candidatura Stage", sAppTesto, sLink, sTestoLink)

		if sErrore <> "0" then
			exit for
		end if
			
'            if sEMail <> "" then
'				Set objMail = Server.CreateObject("CDONTS.NewMail") 
'				HTML = HTML & "<HTML>"
'				HTML = HTML & "<BODY bgColor=#f0f8ff>"
'				HTML = HTML & "<div=center>"
'				HTML = HTML & "<table border=0 width=95% align=center cellpadding=1 bgColor=#f0f8ff cellspacing=2 ><tr><td>"
'				HTML = HTML & "<font size='3' color='#000080' face='Verdana'><br><i><b>Il Portale di ItaliaLavoro</b></i></font><br><br>"
'				HTML = HTML & "<IMG SRC='http://194.177.112.23/PLavoro/images/logo_italia_lavoro.gif'><br><br>"
'				HTML = HTML & "<font size='2' color='#000080' face='Verdana'><br>Gentile " & sCognome & " " & sNome & ",<br><br>ti informiamo"
'				HTML = HTML & " che sei stato selezionato per partecipare allo stage " & sDesc & ".<br>"
'				HTML = HTML & "Ti invitiamo a contattarci per confermare la tua"
'				HTML = HTML & " disponibilita' a partecipare alla selezione."
'				HTML = HTML & "<br><br>A presto</br>"
'				HTML = HTML & "<br><i>La Redazione di ItaliaLavoro</i></br></td></tr></font>"
'				HTML = HTML & "</table></div></BODY></HTML>"
'				objMail.From = "Il portale del lavoro<portale@italialavoro.it>"
'				objMail.To = sEMail 
'				objMail.Subject = "Candidatura Stage" 
'				objMail.BodyFormat = 0 
'				objMail.MailFormat = 0 
'				objMail.Body = HTML 
'				objMail.Send 					
'				'objMail.Send "<Il portale del lavoro>", sEMail, "Candidatura Stage", HTML
'				set objMail = nothing
'            end if
            
	end if
   
next
	
IF sErrore = "0" then
	CC.CommitTrans
%>	
	<script>
		alert("Inserimento correttamente effettuato");
		frmIndietro.submit()
	</script>
<%
else 
	CC.RollbackTrans
%>
	<br><br>
	<table width="500" align="center" border="0" cellspacing="2" cellpadding="1">
		<tr height="20"> 
		  	<td class="tbltext3" align="middle" width="500">
				<b>Errore in fase di inserimento.</b>
			</td>
		</tr>
		<tr height="20"> 
		  	<td class="tbltext3" align="middle" width="500">
				<b><%=sErrore%></b>
			</td>
		</tr>
	</table>
	<br><br>
	<table width="500" cellspacing="2" cellpadding="1" border="0">
		<tr align="center">
			<td>
				<a href="javascript:history.back()"><img src="<%=Session("progetto")%>/images/Indietro.gif" title="Pagina precedente" border="0" align="absBottom"></a>
			</td>
		</tr>
	</table>
<%
END IF

%>

<!--#include Virtual ="/Include/closeconn.asp"-->
<!--#include Virtual="/strutt_coda2.asp"-->	
