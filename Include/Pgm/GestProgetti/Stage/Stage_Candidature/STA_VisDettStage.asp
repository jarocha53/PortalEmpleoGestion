<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include Virtual = "/strutt_testa2.asp"-->
<!--#include Virtual ="/Include/openconn.asp"-->
<!--#include virtual="/include/SelAreaTerrBandi.asp"-->

<script language="javascript">

<!--#include virtual = "/Include/ControlDate.inc"-->

//include del file per fare i controlli sulla numericit� dei campi
<!--#include virtual = "/Include/ControlNum.inc"-->
<!--#include virtual = "/Include/help.inc"-->

function ApriModulo(NomeFile){

	window.open (NomeFile,"Moduli","Status=no,scrollbars=yes,toolbar=no,width=550,height=500,top=20,left=100");
	
}

//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

function Candidati(MODO){
	if(MODO =="RIC"){
	    if(document.frmDett.cmbProv.value ==""){
			alert("Selezionare la provincia")
	    }
	    else{
			document.frmDett.action ="STA_RicCandidati.asp";
			document.frmDett.submit();   
	    }
	}
	if(MODO =="VIS"){
		document.frmDett.action ="STA_VisCandidati.asp";
		document.frmDett.submit();   
	}
	if(MODO =="TUT"){
		document.frmDett.action ="STA_VisTutor.asp";
		document.frmDett.submit(); 
	}
	if(MODO =="FIL"){
		document.frmDett.action ="STA_VisFiltri.asp";
		document.frmDett.submit(); 
	}
	//LC**:26/09/2003
	if(MODO =="BAC"){
		document.frmDett.action ="STA_VisDettStage.asp";
		document.frmDett.submit();   
	}
	//FINE LC**
}

function apriNote(IDRIC){
	codice = IDRIC   
	windowArea = window.open ('STA_InsNote.asp?idRic=' + codice ,'Info','width=750,height=500,Resize=No,Scrollbars=yes')	
  
}

</script>

<%	
'**********************************************************************************************************************************************************************************	

Sub Inizio()
%>
<!-- Lingetta superiore 	Scrittura dell'intestazione della pagina-->
	
	<table border="0" width="520" cellspacing="0" cellpadding="0" height="70" align="center">
		<tr>
			<td width="500" background="<%=session("progetto")%>/images/titoli/strumenti2b.gif" height="70" valign="bottom" align="right">
				<table border="0" background width="500" height="23" cellspacing="0" cellpadding="0">
					<tr>
						<td width="100%" valign="top" align="right"><b CLASS="tbltext1a">Gestione Candidature
						  </b></td>
					</tr>
				</table>
			</td>
		</tr>
	</table> 
	<br>
	<table border="0" CELLPADDING="0" CELLSPACING="0" width="500" align="center">
		<tr height="18">
			<td class="sfondomenu" width="67%" height="18"><span class="tbltext0"><b>&nbsp;RICERCA CANDIDATI </b></span></td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
		  	<td valign="middle" align="right" class="tbltext1" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
		</tr>
	</table>
	<!-- Commento -->
	<table border="0" CELLPADDING="0" CELLSPACING="0" width="500" align="center">
		<tr>
			<td align="left" class="sfondocomm">
                     La ricerca pu� essere effettuata<br>  
                     - per Area Professionale, spuntando l'apposita casella;<br>
                     - per Bacino di Utenza, selezionando un Bando (se non si effettua questa selezione la<br>&nbsp;&nbsp;ricerca viene fatta su tutti i bandi);<br>
                     - per Provincia.<br> 
                     Premere <b> Ricerca </b> per visualizzare le persone da candidare allo stage.<br>
				     <b> Visualizza Candidati </b> mostra le persone candidate allo stage.<br>
				     <b> Visualizza Caratteristiche </b> mostra le caratteristiche richieste per lo stage.
				<a href="Javascript:Show_Help('/pgm/help/gestprogetti/stage/Stage_Candidature/STA_VisDettStage')"><img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
			</td>
		</tr>
		<tr height="2">
			<td class="sfondocommaz" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>

<%	
End Sub

'------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Sub Imposta_Pag()
	
	sSQL = "SELECT DESC_STAGE, ID_SEDE, ID_AREAPROF, NUM_POSTI_ASS, NUM_POSTI_DISP,ID_FIGPROF,COD_TIPO_RICERCA, " & _
			"DT_INISTAGE, DT_FINSTAGE, FL_DOMDIRETTA, DT_INIVAL, DT_FIN_VAL " &_
			"FROM DOMANDA_STAGE WHERE ID_RICHSTAGE= " & sIdRic &_
			" ORDER BY DESC_STAGE"
	  
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
    set rsRisultato = cc.execute(sSQL)
    
    if not rsRisultato.eof then
      
		'CT 15/03/2004
		if date() >= rsRisultato("DT_INIVAL") and _
			(date() <= rsRisultato("DT_FIN_VAL") or isnull (rsRisultato("DT_FIN_VAL"))) then
			dtValida = "S"
		end if
		
		'fine CT 15/03/2004

		Dim objFile
		Set objFile = Server.CreateObject ("Scripting.FileSystemObject")
		nomefile= session("progetto")& "/DOCPRG/" & rsRisultato("ID_SEDE")& ".htm"
		
       %>
		<form name="frmIndietro" method="post" action="STA_VisStage.asp">
           <input type="hidden" name="cmbProv" value="<%=sProvincia%>">
           <input type="hidden" name="txtSedeHid" value="<%=sSede%>">
           <input type="hidden" name="txtSede" value="<%=sDescSede%>">
           <input type="hidden" name="cmbAreaProf" value="<%=sAreaP%>">
           <input type="hidden" name="txtDal" value="<%=sDataDal%>">
           <input type="hidden" name="txtAl" value="<%=sDataAl%>">
		</form>
       
		<form name="frmEliminaHtm" method="post" action="STA_Risultati.asp">
			<input type="hidden" name="mod" value="D">
		 	<input type="hidden" name="txtIdRichiesta" value="<%=sIdRic%>">
		 	<input type="hidden" name="txtSedeHid" value="<%=sSede%>">
		 	<input type="hidden" name="txtNomeFile" value="<%=nomefile%>">		 	
			<input type="hidden" name="txtProvincia" value="<%=sProvincia%>">
			<input type="hidden" name="txtSede" value="<%=sDescSede%>">
			<input type="hidden" name="txtAreaP" value="<%=sAreaP%>">
			<input type="hidden" name="txtDataDal" value="<%=sDataDal%>">
			<input type="hidden" name="txtDataAl" value="<%=sDataAl%>">
			<input type="hidden" name="cmbBando" value="<%=sBacUte%>">
		</form>

<%
     sSQlImp="SELECT DESCRIZIONE,RAG_SOC FROM IMPRESA I,SEDE_IMPRESA SI" &_
               " WHERE I.ID_IMPRESA = SI.ID_IMPRESA AND ID_SEDE =" & rsRisultato("ID_SEDE")
      
'PL-SQL * T-SQL  
SSQLIMP = TransformPLSQLToTSQL (SSQLIMP) 
       SET rsSede = cc.execute(sSQlImp)
%>       
	<form name="frmDett" method="post" action="STA_insdoc2.asp">
           <input type="hidden" name="txtIdRichiesta" value="<%=sIdRic%>">
           <input type="hidden" name="txtArea" value="<%=rsRisultato("ID_AREAPROF")%>">
			<input type="hidden" name="txtSedeHid" value="<%=rsRisultato("ID_SEDE")%>">
			
		<table width="500" align="center" border="0" cellspacing="2" cellpadding="2" class="sfondocommaz">
            <tr height="20"> 
			  	<td class="sfondocomm" align="left" width="450">
					<b class="tbltext1">Denominazione azienda</b>
				</td>
				<td class="sfondocomm" align="left" width="15">
					<b class="tbltext1">Numero Proroghe</b>
				</td>
				<td class="sfondocomm" align="left" width="15">
					<b class="tbltext1">Segnalazioni</b>
				</td>
				<td class="sfondocomm" align="left" width="15" colspan="2">
					<b class="tbltext1">Modelli</b>
				</td>
			</tr>
			<tr height="20" class="tblsfondo"> 
			  	<td class="tbldett" align="middle" width="450">
			  		<%=rsSede("RAG_SOC")%>&nbsp;-&nbsp;<%=rsSede("DESCRIZIONE")%>
				</td>
		<%      SET rsSede =nothing 
		        sqlProro ="SELECT COUNT(ID_PROROGA) AS TOTALE FROM PROROGA_STAGE" &_
		                  " WHERE ID_RICHSTAGE =" & sIdRic
		        
'PL-SQL * T-SQL  
SQLPRORO = TransformPLSQLToTSQL (SQLPRORO) 
		        SET rsTot=cc.execute(sqlProro)
		        
		        %>
		
				<td class="tbldett" align="middle" width="15">
			  		<%=rsTot("TOTALE")%>
				</td>
				<td align="center" width="80">
					<a title="Segnalazioni" href="javascript:apriNote('Rich<%=sIdRic%>')" ID="imgPunto1" name="imgPunto1" onmouseover="javascript:window.status='';return true"><img border="0" src="<%=Session("Progetto")%>/images/scheda.gif"></a>
				</td>
				<%
				
				If objFile.FileExists (Server.MapPath(nomefile)) Then
				%>
				<td align="center">
					<a href="javascript:ApriModulo('<%=nomefile%>')">
						<img src="/images/leggi.gif" border="0" alt="Visualizza il modello" WIDTH="25" HEIGHT="25">
					</a>

					<!--a href="<%=nomefile%>" target="_new"><img src="/images/Leggi.gif" border="0" alt="Visualizza il modello" WIDTH="25" HEIGHT="25"></a-->
				</td>
				
				<td align="center">	
					<a href="javascript:frmEliminaHtm<%=cont%>.submit()">
						<img src="/images/cestino.gif" border="0" alt="Elimina il modello" WIDTH="12" HEIGHT="12">
					</a>								
				</td>
				 
				<%
				Else%>
				
				<td align="center" colspan="2">
					<a HREF="Javascript:frmDett.submit()" onmouseover="javascript:window.status=' '; return true">
						<img src="/images/compila.gif" border="0" WIDTH="19" HEIGHT="14" alt="Compila il modello">
					</a>
				</td>
				<%
				End If
				Set objFile = Nothing
				%> 
			</tr>
		</table>
		<br>
		<%set rsTot= nothing%>
		<table width="500" align="center" border="0" cellspacing="2" cellpadding="2" class="sfondocommaz">
            <tr height="20"> 
			  	<td class="textblacka" align="left" width="130">
					<b class="tbltext1">Riferimento Richiesta</b>
				</td>
			  	<td class="textblack" align="left" width="370">
					<b><%=sIdRic%></b>
				</td>
			</tr>
            <tr height="20"> 
			  	<td class="textblacka" align="left">
					<b class="tbltext1">Descrizione Stage</b>
				</td>
			  	<td class="textblack" align="left">
					<%=lcase(rsRisultato("DESC_STAGE"))%>
				</td>
			</tr>
			<tr>
				<td class="textblacka" align="left">
					<b class="tbltext1">Periodo Stage dal/al</b>
				</td>
				<td class="textblack" align="left">
					<%=rsRisultato("DT_INISTAGE")%>&nbsp;-&nbsp;<%=rsRisultato("DT_FINSTAGE")%>
				</td>
			</tr>
           
			<tr>
				<td class="textblacka" align="left">
					<b class="tbltext1">Numero Candidati</b>
				</td>
				<td class="textblack" align="left">
					<%=rsRisultato("NUM_POSTI_DISP")%>
				</td>
			</tr>
			<tr>	
				<td class="textblacka" align="left">
					<b class="tbltext1">Posti Assegnati</b>
				</td>
				<td class="textblack" align="left">
					<%=rsRisultato("NUM_POSTI_ASS")%>
				</td>
			</tr>
		
			 <tr height="20"> 
			  	<td class="textblacka" align="left">
					<b class="tbltext1">Area Professionale</b>
				</td>
				<td class="textblack" align="left">
					<%=lcase(DenominazioneArea(rsRisultato("ID_AREAPROF")))%>
				     
				</td>
			</tr>
			 <tr height="20"> 
				<td align="left" width="150">
					<b class="tbltext1">Figura Professionale</b>
				</td>
				<td class="textblack" align="left" width="350">
				
					<%nIdFigura = clng(rsRisultato("ID_FIGPROF"))
		            
		            sql ="SELECT DENOMINAZIONE FROM FIGUREPROFESSIONALI WHERE ID_FIGPROF=" & nIdFigura

'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
					SET rs = cc.execute(sql)

					sDenominazione = rs("DENOMINAZIONE")

					set rs = nothing
		            
					Response.Write  sDenominazione%>				
				
			</td>						
		</tr>
		 <tr height="20"> 
			<td align="left" width="150">
				<b class="tbltext1">Tipo di Ricerca</b>
			</td>
			<td class="textblack" align="left" width="350">
				
					<%nTipo = clng(rsRisultato("COD_TIPO_RICERCA"))
					  if nTipo = 0 then 
							Response.Write "PER AREA PROFESSIONALE"
					  else
							Response.Write "PER PROFILO PROFESSIONALE"
					  end if		%>				
			        &nbsp;<input TYPE="checkbox" name="ckArea" value="S">
			</td>						
		</tr>	
			<tr>	
				<td class="textblacka" align="left">
					<b class="tbltext1">Bacino di utenza</b>
				</td>
				<td class="tbldett" align="left">
					<span class="tbltext3">
				    <%
				    '''LC**:26/09/2003
					'''strcombo= session("iduorg") & "||cmbBando|" 
			        strcombo= session("iduorg") & "|" & sBacUte & "|cmbBando| onchange=javascript:Candidati('BAC')"
			        CreateBandi strcombo			
					%>				
					</span>
				</td>
			</tr>
			<tr>	
				<td class="textblacka" align="left">
					<b class="tbltext1">Della provincia di</b>
				</td>
				<td class="tbldett" align="left">
					<b><%
					'''LC**:26/09/2003 - inserito controllo su "sBacUte".
					if sBacUte = "" then
						sSetProv = SetProvUorg(Session("idUorg"))
					else
						sSetProv = SetProvBando(clng(sBacUte),Session("idUorg"))
					end if
					    nProvSedi = len(sSetProv)%>
					    <select NAME="cmbProv" class="textblack">			 
							<option></option>
							<option value="TUTTE">- TUTTE -</option>	
<%  						pos_ini = 1 	                
							for i=1 to nProvSedi/2
								sTarga = mid(sSetProv,pos_ini,2)
								if sTarga=sProvincia then															
%>									<option value="<%=sTarga%>" selected><%=DecCodVal("PROV", 0, "", sTarga,"")%></option>
<%								else %>    
									<option value="<%=sTarga%>"><%=DecCodVal("PROV", 0, "", sTarga,"")%></option>
<% 								end if
								pos_ini = pos_ini + 2
							next%>
	                    </select>
					</b>
				</td>
			</tr>
			<tr>	
				
				<td class="tbltext1" align="left" width="500" colspan="2">
<%				    if rsRisultato("FL_DOMDIRETTA") ="S" then
				        sFlag="Lo Stage si svolgera' presso una delle sedi operative dell'azienda"
				    else
				        sFlag="Lo Stage si svolgera' presso una azienda esterna"
				    end if
%>					<b><%=sFlag%></b>
				</td>
			</tr>
		</table>
	</form>
	
	
	<br>
	
		<table width="500" align="center" border="0" cellspacing="2" cellpadding="1" class="sfondocommaz">
			<tr height="20"> 
			  	<td align="middle" width="500">
					<a class="textred" HREF="Javascript:Candidati('VIS')" onmouseover="javascript:window.status=' '; return true">
					     <b>Visualizza Candidati</b>
				    </a>
				</td>
				<td align="middle" width="500">
					<a class="textred" HREF="Javascript:Candidati('FIL')" onmouseover="javascript:window.status=' '; return true">
					     <b>Visualizza Caratteristiche</b>
				    </a>
				</td>
				
			</tr>
		</table>
		<br>
		
			<%
		if dtValida = "S" then		
			%>
			<table width="200" border="0" cellspacing="1" cellpadding="1" class="sfondocommaz">
				<tr height="20"> 
				  	<td align="middle">
						<a class="textred" HREF="Javascript:Candidati('RIC')" onmouseover="javascript:window.status=' '; return true">
						     <img src="<%=Session("progetto")%>/images/lente.gif" id="image1" name="image1" border="0">
					    </a>
					</td>
				
					<td align="center">
						<a href="javascript:document.frmIndietro.submit()">
							<img src="<%=session("Progetto")%>/images/indietro.gif" border="0" name="imgPunto2" onmouseover="javascript:window.status=' '; return true">
						</a>
					</td>
				</tr>	
			</table>	
			<%
		else
			%>
			<table width="200" border="0" cellspacing="1" cellpadding="1" class="sfondocommaz">
				<tr height="20"> 
					<td align="center">
						<a href="javascript:document.frmIndietro.submit()">
							<img src="<%=session("Progetto")%>/images/indietro.gif" border="0" name="imgPunto2" onmouseover="javascript:window.status=' '; return true">
						</a>
					</td>
				</tr>	
			</table>
			<%
		end if
		%>
		<br><br>
<%  else
       Testo = "Non ci sono ulteriori dettagli per la richiesta selezionata"
    
%>		
		<br><br>
		<table width="500" align="center" border="0" cellspacing="2" cellpadding="1" class="sfondocommaz">
			<tr height="20"> 
			  	<td class="tbltext3" align="middle" width="500">
					<b><%=Testo%></b>
				</td>
			</tr>
		</table>
<%end if 			
End Sub

'-------------------------------------------------------------------------------------------------------------------------------------------
	
function DenominazioneArea(ID)
    sql1="SELECT DENOMINAZIONE FROM AREE_PROFESSIONALI WHERE ID_AREAPROF=" & ID
'PL-SQL * T-SQL  
SQL1 = TransformPLSQLToTSQL (SQL1) 
    set rs = cc.execute(sql1)
    appo = ""
    if not rs.eof then
        appo = rs("DENOMINAZIONE")
    end if
    DenominazioneArea= appo
end function

'-------------------------------- MAIN -----------------------------------------------------------------------------------------------------------

dim sIdRic,sProvincia,sSede,sDescSede,sAreaP,sDataDal,sDataAl
dim sBacUte

sIdRic=Request.Form ("txtIdRichiesta")
'Response.Write "sIdRic: " & sIdRic & "<br>"
sProvincia=Request.Form ("txtProvincia")
sSede=Request.Form ("txtSedeHid")
'Response.Write "sSede: " & sSede & "<br>"

sDescSede=Request.Form ("txtSede")
sAreaP=Request.Form ("txtAreaP")
sDataDal=Request.Form ("txtDataDal")
sDataAl=Request.Form ("txtDataAl")
'''LC**:26/09/2003
sBacUte = request("cmbBando")
'''FINE LC**

Inizio()

Imposta_Pag()

%>	

<!--#include Virtual ="/Include/closeconn.asp"-->
<!--#include Virtual="/strutt_coda2.asp"-->		
