<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%IF session("progetto") <> "" then%>

<!-- #include virtual="/include/OpenConn.asp" -->
<!-- #include virtual="/include/DecCod.asp" -->
<!--#include virtual = "/include/SelBandi.asp"-->
<!-- #include virtual="/include/ElabRegole.asp" -->
<!-- #include virtual="/include/SelezioneProvince.asp" -->

<script language="javascript" src="Util.js">
</script>

<title>Inscriptos por carga familiar</title>

<body topmargin="0" onafterprint="javascript:buttom.style.visibility='visible';" onbeforeprint="javascript:buttom.style.visibility='hidden'">
<%
	
	if trim(Request.QueryString("sAmb"))="" then
		Ambito = FiltroCPI(true)
	else 
		Ambito = Request.QueryString("sAmb")
	end if	
	
	sProv = Request.QueryString("sProv")
	
	ValProv = DecodTadesToArray("PROV",date,"CODICE='" & sProv & "'",0,0)
	sDescProv = ValProv(0,1,0)
	sDesc=ValProv(0,1,1)
	DescRegione = DecCodVal("REGIO",0,date,sDesc,0)

	if ucase(session("progetto"))= "/ARITES" then
		provdep = "la Provincia"
	else
		provdep = "el Departamento"
	end if
	
	Select case(ambito)
		case("x")
			titolo = "Distribuci�n por la Carga Familiar " & _ 
					 "de los inscriptos a " & _
					 Request.QueryString("CPI")
		case(0)
			titolo = "Distribuci�n por la Carga Familiar " & _ 
					 "de los inscriptos a " & _
					 Request.QueryString("CPI") 
		case(1)
			titolo = "Distribuci�n por la Carga Familiar " & _ 
					 "de los inscriptos en el Territorio Nacional"
		case(2)
			titolo = "Distribuci�n por la Carga Familiar " & _ 
					 "de los inscriptos en " & provdep & " de " & sDescProv 
		case(3)
			titolo = "Distribuci�n por la Carga Familiar " & _ 
					 "de los inscriptos en la Region " & DescRegione 				 					 
	End select	
	
	if trim(Request.QueryString("sBan"))<>"" then
		titolo = titolo & " - Inscriptos a : " & DecBando(Request.QueryString("sBan"))
	end if%>	
<link REL="STYLESHEET" TYPE="text/css" HREF="<%=session("progetto")%>/fogliostile.css">
<center>
<table border="0" width="488" cellspacing="0" cellpadding="0" height="81">
   <tr>
     <td width="520" background="<%=Session("Progetto")%>/images/titoli/strumenti2b.gif" height="81" valign="bottom" align="right">
       <table border="0" background width="260" height="30" cellspacing="0" cellpadding="0">
         <tr>
           <td width="100%" valign="top" align="right"><b class="tbltext1a">ESTADISTICAS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></td>
         </tr>
          <tr>
            <td width="100%" valign="top" align="right"><BR><BR></td>
          </tr>
       </table>
     </td>
   </tr>
</table>
<table cellpadding="0" cellspacing="0" width="488" border="0">
   <tr height="18">
		<td class="sfondomenu" height="18" width="45%">
		<span class="tbltext0"><b>&nbsp;INSCRIPTOS POR CARGA FAMILIAR</b></span></td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
		<td valign="middle" align="right" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1"></td>
   </tr>
   <tr height="2">
		<td width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif"></td>     
	</tr>
   <tr width="371" class="SFONDOCOMM">
		<td colspan="3"><%=titolo%></b>
		</td>
	</tr>
	<tr height="2">
		<td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif">
		</td>
	</tr>
</table>
<br>
<table>
	<tr width="488">
		<td width="488" class="textred" align="center"><b>&nbsp;situaci�n al : <%=date%></b></td> 
	</tr>
</table>	
<br>

<% 
sCpi=Request.QueryString("CPI")
dim aCarFam(3)
dim aDescReg(3)

aCarFam(0) = " = 1"
aCarFam(1) = " = 2"
aCarFam(2) = " = 3"
aCarFam(3) = " > 3"

aDescReg(0) = "1" 
aDescReg(1) = "2"
aDescReg(2) = "3"
aDescReg(3) = "M�s de 3"

sFilePath = Server.MapPath("/") & session("progetto") & "/DocPers/Statistiche/" &_
	        Session("IdUtente") & "_F.xls"

sFileNameJS = session("progetto") & "/DocPers/Statistiche/" &_
	Session("IdUtente") & "_F.xls"
	
set oFileSystemObject = Server.CreateObject("Scripting.FileSystemObject")

set oFileObj = oFileSystemObject.CreateTextFile(sFilePath,true)

' ********************************
' Scrivo la intestazione del file.
' ********************************
oFileObj.WriteLine ("Inscriptos por Carga Familiar")
oFileObj.WriteLine ("")
oFileObj.WriteLine (titolo)
'oFileObj.WriteLine ("Resoconto distribuzione relativo al carico familiare")
'oFileObj.WriteLine ("degli iscritti a: " & sCpi)
oFileObj.WriteLine ("")
oFileObj.WriteLine ("")
%>

<table border="0" width="488" align="center" cellspacing="2" cellpadding="2">
 <tr>
		<td width="114">&nbsp;  </td> 
		<td width="130" valign="top" align="center" class="sfondocomm"><b>Personas menores de 18 a�os a cargo</b></td>
		<td width="130" valign="top" align="center" class="sfondocomm"><b>Total</b></td>
		<td width="114">&nbsp;  </td>
 </tr>
<%
set rsCarico = Server.CreateObject("ADODB.recordset")
sReport = "NO"
	
	perSql=""			
	if trim(Request.QueryString("sBan"))<>"" then
		perSql = " AND p.ID_PERSONA IN (SELECT ID_PERSONA FROM DOMANDA_ISCR WHERE ID_BANDO='" & Request.QueryString("sBan") & "') "
	end if 	

for t=0 to ubound(aCarFam) 

 sSQL = " SELECT COUNT(P.ID_PERSONA) AS NTOT" &_	
		   " FROM PERSONA p , DICHIARAZ d " &_
		   " WHERE p.id_persona = d.id_persona " &_
	       perSql & " AND EXISTS (SELECT id_persona FROM STATO_OCCUPAZIONALE " &_
	       " WHERE p.id_persona = id_persona AND ind_status ='0'"
 
Select case(ambito)
	case(0)
	sSQL = sSQL & " AND id_cimpiego=" & session("creator") & ")" &_
				  " AND NVL((NUM_CARICO),0) " & aCarFam(t)
	       
	case(1)	       
	sSQL = sSQL & " AND id_cimpiego is not null)" &_
				  " AND NVL((NUM_CARICO),0) " & aCarFam(t)
	case(2)
	sSQL = " SELECT COUNT(P.ID_PERSONA) AS NTOT" &_	
		   " FROM PERSONA p , DICHIARAZ d " &_
		   " WHERE p.id_persona = d.id_persona AND " &_
		   " EXISTS (" &_
			 " SELECT id_persona FROM STATO_OCCUPAZIONALE SO, SEDE_IMPRESA SI" &_
			 " WHERE p.id_persona = SO.id_persona AND (ind_status='0' or " &_
						"(ind_status='2' and " &_
						" ind_status not in (select ind_status from STATO_OCCUPAZIONALE WHERE p.id_persona = id_persona AND id_cimpiego= " & session("creator") & " and ind_status='0'))) " &_
						" AND SO.id_CIMPIEGO in (" &_
				" SELECT A.ID_SEDE FROM sede_impresa a, impresa b " &_
				" where a.id_impresa = b.id_impresa" &_
				" AND a.prv ='" & sProv  & "' AND B.cod_timpr ='03'))" &_ 
				" AND NVL((NUM_CARICO),0) " & aCarFam(t)
	 case(3)
 	 valoreReg= SetProvince(ambito)
	 sSQL = " SELECT COUNT(P.ID_PERSONA) AS NTOT" &_	
		    " FROM PERSONA p , DICHIARAZ d " &_
		    " WHERE p.id_persona = d.id_persona AND " &_
		    " EXISTS (" &_
			  " SELECT id_persona FROM STATO_OCCUPAZIONALE SO" &_
			  " WHERE p.id_persona = SO.id_persona AND (ind_status='0' or " &_
						"(ind_status='2' and " &_
						" ind_status not in (select ind_status from STATO_OCCUPAZIONALE WHERE p.id_persona = id_persona AND id_cimpiego= " & session("creator") & " and ind_status='0'))) " &_
						" AND SO.id_CIMPIEGO in (" &_
				" SELECT A.ID_SEDE FROM sede_impresa A, impresa B " &_
				" WHERE A.id_impresa = B.id_impresa  AND B.cod_timpr ='03'" &_
				" AND A.prv  in (" & valoreReg  & ")))" &_ 
				" AND NVL((NUM_CARICO),0) " & aCarFam(t) 
'Response.Write sSql 				
	End select
	
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
'Response.Write ssql
	rsCarico.Open sSql,CC,3
	
	if not rsCarico.EOF then
	    if sReport="NO" then 
       
	    oFileObj.WriteLine ("PERSONAS FISCALMENTE A CARGO" & chr(9) & "TOTAL")
        end if
		
		sReport = "OK"
%>	
		<tr>
			<td width="114">&nbsp;  </td> 
			<td width="130" valign="top" align="center" class="tblsfondo"><b class="tbltext1"><%=aDescReg(t)%></b></td>
			<td width="130" valign="top" align="center" class="tblsfondo"><b class="tbltext1"><%=clng(rsCarico("NTOT"))%></b></td>
			<td width="114">&nbsp;  </td> 
		</tr>	
<%	
		oFileObj.WriteLine (aDescReg(t) & chr(9) & clng(rsCarico("NTOT")))
	else
%>		<tr>
			<td width="114">&nbsp;  </td> 
			<td width="130" valign="top" align="center" class="tblsfondo"><b class="tbltext1"><%=aDescReg(t)%></b></td>
			<td width="130" valign="top" align="center" class="tblsfondo"><b class="tbltext1">0</b></td>
			<td width="114">&nbsp;  </td> 
		</tr>	
<%
		oFileObj.WriteLine (aDescReg(t) &  (chr(9) &  "0"))
	
	end if
		
	rsCarico.Close
next

	set rsCarico = nothing 

	oFileObj.CLose
	set oFileObj = nothing
	set oFileSystemObject = nothing

%>  
</table>	
<br><br><br><br>
</center>
<%
	if sReport = "OK" then
		Fine()
	else
		Fine1()
	end if 
%>
</body>


<%sub Fine%> 
<label id="buttom">
  <table width="488" cellspacing="2" cellpadding="1" border="0" align="center">
	<tr align="center">
		<td>
			<a href="javascript:self.close()"><img src="<%=Session("progetto")%>/images/chiudi.gif" title="Chiudi la pagina" border="0" align="absBottom"></a>
		    <a href="javascript:print()"><input type="image" src="<%=Session("progetto")%>/images/stampa.gif" title="Stampa la pagina" border="0" align="center" onclick="self.print();" id="stampa" name="stampa"></a>
		</td>
	</tr>		
</table> 
<br>
<table width="460" cellspacing="2" cellpadding="1" border="0" align="center">
	<tr align="center">
		<td>
			<a class="textred" href="Javascript:Scarica('<%=sFileNameJS%>')"><b>Abrir Reporte</b></a>
		</td>
	</tr>
</table>
</label>  
<%end sub 

sub Fine1()%>

	<br><br>
	<table width="488" cellspacing="2" cellpadding="1" border="0" align="center">
		<tr align="center">
			<td class="tbltext3">
				Ningun elemento encontrado.
			</td>
		</tr>		
		<tr align="center">
			<td>&nbsp;</td>
		</tr>		
		<tr align="center">
			<td>
				<a href="javascript:self.close()"><img src="<%=Session("progetto")%>/images/chiudi.gif" title="Chiudi la pagina" border="0" align="absBottom"></a>
			</td>
		</tr>		
	</table>

<%end sub%>
<!-- #include virtual="/include/closeConn.asp" -->
<%ELSE%>
	<script>
		alert("La sesi�n ha caducado")
		self.close()
	</script>
<%END IF%>
