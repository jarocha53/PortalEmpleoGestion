<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%if session("progetto") <> "" then %> 
<!--#include Virtual = "/include/openconn.asp"-->
<!--#include Virtual = "/include/DecCod.asp"-->
<!--#include virtual = "/include/SelBandi.asp"-->
<!-- #include virtual="/include/ElabRegole.asp" -->
<!-- #include virtual="/include/SelezioneProvince.asp" -->

<html>
<title>Inscriptos por situaci�n Laboral, Edad y Sexo</title>
<link REL="STYLESHEET" TYPE="text/css" HREF="<%=session("progetto")%>/fogliostile.css">
<script language="javascript" src="Util.js"></script>
<center>
<body topmargin="0" onafterprint="javascript:buttom.style.visibility='visible';" onbeforeprint="javascript:buttom.style.visibility='hidden'">
<%
server.ScriptTimeout = server.ScriptTimeout * 10

dim Sql,rsFasciaA, i,t,j,n,nFemmine
dim sReport
dim sNomiProv
dim sProvReg
dim aFascia(5)
aFascia(0) = " < 18"
aFascia(1) = "BETWEEN 18 AND 29"
aFascia(2) = "BETWEEN 30 AND 49"
aFascia(3) = "BETWEEN 50 AND 65"
aFascia(4) = " > 65"

aST181 = decodTadesToArray("ST181",date,"",2,"0") 

sCondizione = ""
if not isnull(aST181(0,0,0)) then
	sCondizione = " AND COD_ST181 IN ("
	for j=0 to ubound(aST181)-1
		sCondizione = sCondizione & "'" & aST181(j,j,j) & "'"
		if j <> ubound(aST181)-1 then
			sCondizione = sCondizione & ","
		end if
	next
	sCondizione = sCondizione & ") "
end if

erase aST181
inizio()

	if trim(Request.QueryString("sAmb"))="" then
		Ambito = FiltroCPI(true)
	else 
		Ambito = Request.QueryString("sAmb")
	end if	
	
sProv = Request.QueryString("sProv")
ValProv = DecodTadesToArray("PROV",date,"CODICE='" & sProv & "'",0,0)
sDescProv = ValProv(0,1,0)
sDesc=ValProv(0,1,1)
DescRegione = DecCodVal("REGIO",0,date,sDesc,0)

titolo = "Distribuci�n relacionada de inscriptos por situacion laboral," &_
		 "por edad y sexo"

	if ucase(session("progetto"))= "/ARITES" then
		provdep = "la Provincia"
	else
		provdep = "el Departamento"
	end if
	
	select case(ambito)
		case("x")
			titolo = titolo & " a: " & Request.QueryString("CPI")
		case(0)
			titolo = titolo & " a: " & Request.QueryString("CPI")
		case(1)
			titolo = titolo & " del Territorio Nacional"
		case(2)
			titolo = titolo & " de " & provdep & " de " & sDescProv 
		case(3) 
		    sProvReg =SetProvince(3)
			titolo = titolo & " de la Region de " & DescRegione 				 					 
	end select
	if trim(Request.QueryString("sBan"))<>"" then
		titolo = titolo & " - Inscriptos a : " & DecBando(Request.QueryString("sBan"))
	end if%>
<table cellpadding="0" cellspacing="0" width="488" border="0">
	<tr height="18">
		<td class="sfondomenu" height="18" width="75%">
		<span class="tbltext0"><b>&nbsp;INSCRIPTOS POR SITUACION LABORAL, EDAD Y SEXO</b></span></td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
		<td valign="middle" align="right" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1"></td>
	</tr>
	<tr height="2">
		<td width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif"></td>     
	</tr>
	<tr width="371" class="SFONDOCOMM">
		<td colspan="3"><%=titolo%>
		</td>
	</tr>
	<tr height="2">
		<td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif">
		</td>
	</tr>
</table>
<br>
<table>
	<tr width="488">
		<td width="500" class="textred" align="center"><b>&nbsp;situaci�n al : <%=date%></strong></td> 
	</tr>
</table>	
<br>
<table border="0" width="488" cellspacing="2" cellpadding="2">
<%
aL181p = decodTadesToArray("STDIS",date,"",0,"0")

scpi = Request.QueryString("CPI")

sFileName = Server.MapPath("/") & session("Progetto") & "/Docpers/Statistiche/" &_
	Session("IdUtente") & "_LG181.xls"

sFileNameJS = session("Progetto") & "/DocPers/Statistiche/" &_
	Session("IdUtente") & "_LG181.xls"

set oFileSystemObject	= Server.CreateObject("Scripting.FileSystemObject")
set oFileObj = oFileSystemObject.CreateTextFile(sFileName)

' ********************************
' Scrivo la intestazione del file.
' ********************************
oFileObj.WriteLine ("INSCRIPTOS POR SITUACION LABORAL, POR EDAD Y SEXO")
oFileObj.WriteLine ("")
oFileObj.WriteLine ("")
oFileObj.WriteLine (titolo)
sReport = "NO"
	
	perSql=""			
	if trim(Request.QueryString("sBan"))<>"" then
		perSql = " AND p.ID_PERSONA IN (SELECT ID_PERSONA FROM DOMANDA_ISCR WHERE ID_BANDO='" & Request.QueryString("sBan") & "') "
	end if 	

for i = 0 to ubound(aFascia)-1
		sFascia = Replace(aFascia(i),"BETWEEN","ENTRE")
		sFascia = Replace(sFascia,"AND","Y")
		sFascia = sFascia & " A�OS"
        NomeFascia=aFascia(i)
	%>
		<tr class="tblsfondo">
			<td width="300" class="tbltext1"><b><%=sFascia%></b></td>
			<td width="100" class="tbltext1" align="center"><b>Femenino</b></td>
			<td width="100" class="tbltext1" align="center"><b>Masculino</b></td>		
		</tr>
	<%	oFileObj.WriteLine
		oFileObj.WriteLine (sFascia & chr(9) & "FEMMINE" & chr(9) & "MASCHI")
        oFileObj.WriteLine
	
	
	for ii=0 to UBound(aL181p)-1
	    sql="SELECT COUNT (p.id_persona) AS nper, p.sesso, so.cod_st181 " &_
	        "FROM persona p, stato_occupazionale so " &_
	        "WHERE p.id_persona = so.id_persona " &_
            perSql & " AND COD_STDIS ='" & aL181p(ii,ii,ii) & "'" &_
            " AND (ind_status='0' or " &_
						"(ind_status='2' and " &_
						" ind_status not in (select ind_status from STATO_OCCUPAZIONALE WHERE p.id_persona = id_persona AND id_cimpiego= " & session("creator") & " and ind_status='0'))) "
        select case(ambito)
           case(0)
               sql =sql & " and so.id_cimpiego=" & session("creator") 
           case(2)
               sql = sql & " and so.id_cimpiego in (SELECT id_sede from " &_
	                       "sede_impresa si,impresa i where si.id_impresa=i.id_impresa" &_ 
	                       " and i.cod_timpr='03' and si.prv='" & sProv & "')"
	       case(3)
               sql = sql & " and so.id_cimpiego in (SELECT id_sede from" &_
	                       " sede_impresa si,impresa i where si.id_impresa=i.id_impresa" &_
	                       " and i.cod_timpr='03' and si.prv in(" & sProvReg & "))"
	    end select    
	
	    sql = sql & " and FLOOR(MONTHS_BETWEEN(SYSDATE,DT_NASC)/12) " & aFascia(i) &_
	                " GROUP BY sesso,cod_st181 order by sesso" 
	     
	   set rsFasciaA = server.CreateObject("ADODB.recordset")
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
'Response.Write sql
'Response.Write aL181p(ii,ii,ii)
		rsFasciaA.Open Sql,cc,3
  		 
		rsCount = rsFasciaA.RecordCount		
		select case (rsCount)
		    case(1) 
		        if rsFasciaA("SESSO") = "M" THEN							
						nNuMpersF = 0
						nNuMpersM = rsFasciaA("NPER")
		        else
						nNuMpersM = 0 
						nNuMpersF = rsFasciaA("NPER")
				end if
			case(2)
                nNuMpersF = rsFasciaA("NPER")
                rsFasciaA.MoveNext 
                nNuMpersM = rsFasciaA("NPER") 
            case else 
                nNuMpersM = 0 
				nNuMpersF = 0
        end select %> 				
		<tr>
             <td class="tbltext1"><%=aL181p(ii,ii+1,ii)%></td>
             <td class="tbltext1" align="center"><%=nNuMpersF%></td>
		     <td class="tbltext1" align="center"><%=nNuMpersM%></td>
        </tr>
<%		oFileObj.WriteLine (aL181p(ii,ii+1,ii) & chr(9) & nNuMpersF & chr(9) & nNuMpersM)
         
	next
next

rsFasciaA.Close
set rsFasciaA = nothing
oFileObj.close
set oFileSystemObject = nothing
set oFileObj = nothing
erase al181p
%>
</table>
<%
	Fine()
%>
</body>
</html>
<!--#include Virtual = "/include/CloseConn.asp"-->
<%else%>
	<script>
		alert("La sesi�n ha caducado")
		self.close()
	</script>
<%end if

' *************************************************************************************
sub inizio()
%>
  <table border="0" width="488" cellspacing="0" cellpadding="0" height="81">
    <tr>
      <td width="500" background="<%=Session("Progetto")%>/images/titoli/strumenti2b.gif" height="81" valign="bottom" align="right">
        <table border="0" background width="260" height="30" cellspacing="0" cellpadding="0">
          <tr>
            <td width="100%" valign="top" align="right"><b class="tbltext1a">ESTADISTICAS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></b></td>
          </tr>
          <tr>
            <td width="100%" valign="top" align="right"><BR><BR></td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
<%
end sub
' ************************************************************************************
sub Fine()
	%>
	</center>
	<br><br>
	<label id="buttom">
	<table width="488" cellspacing="2" cellpadding="1" border="0" align="center">
		<tr align="center">
			<td>
				<a href="javascript:self.close()"><img src="<%=Session("progetto")%>/images/chiudi.gif" title="Chiudi la pagina" border="0" align="absBottom"></a>
				<a href="javascript:print()"><input type="image" src="<%=Session("progetto")%>/images/stampa.gif" title="Stampa la pagina" border="0" align="center" onclick="self.print();" id="stampa" name="stampa"></a>
			</td>
		</tr>		
		<tr align="center">
			<td>&nbsp;</td>
		</tr>		
		<tr align="center">
			<td>
				<a class="textred" href="javascript:Scarica('<%=sFileNameJS%>')"><b>Abrir reporte</b></a>
			</td>
		</tr>		
	</table>
	</label>
	

	
<%
server.ScriptTimeout = server.ScriptTimeout / 10
end sub%>
