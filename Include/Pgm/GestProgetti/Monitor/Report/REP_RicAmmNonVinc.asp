<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include virtual="/util/portallib.asp" -->
<!--#include virtual="/strutt_testa2.asp"-->
<!--#include virtual = "/include/SelAreaTerrBandi.asp"-->
<!--#include virtual = "/include/ControlDateVB.asp"-->
<!--#include virtual ="/include/openconn.asp"-->
<!--#include virtual ="/include/DecCod.asp"-->
<!--include virtual = "include/ComboBandi.asp"-->
<%
if ValidateService(session("idutente"),"REP_RICAMMNONVINC", CC) <> "true" then 
	response.redirect "/util/error_login.asp"
end if

' ************* MAIN  *************
Inizio()
CaricaCombo()
%>
<html>
<head>
<!--include file = "../../include/ckProfile.asp"-->
<link REL="STYLESHEET" TYPE="text/css" HREF="../../fogliostile.css">
<title>{</title>

<script LANGUAGE="Javascript">
<!--#include Virtual = "/Include/help.inc"-->

function InviaProv()
{
var appo
	
	appoProv=frmProvincia.cmbProv.value
	if (appoProv == "")
	{
		alert("Selezionare la Provincia")
	}
	else
	{
		appo= frmProvincia.hIdSede.value
		if (appo == 0)
		{
			document.frmProvincia.action='REP_VisAmmNonVinc.asp?Sel=0'
		}
		else
		{
			document.frmProvincia.action="REP_RicAmmNonVinc.asp?Mod=2"
		}
	}
	frmProvincia.submit();
}

function InviaBando(val)
{
	if (val == "")
	{
		alert("Selezionare il Bando")
	}
	else
	{
		frmBando.submit();
	}
}

function InviaCImp(){
	appoCImp=frmCImp.cmbSede.value
	if (appoCImp == ""){
		alert("Selezionare il Centro dell'Impiego")
	}else
		frmCImp.submit();
	}
</script>
</head>

<%SUB Inizio ()%>
<table border="0" width="500" cellspacing="0" cellpadding="0" height="81">
<tr>
	<td width="500" background="<%=Session("Progetto")%>/images/titoli/strumenti2b.gif" height="81" valign="bottom" align="right">
	  <table border="0" width="500" height="30" cellspacing="0" cellpadding="0">
	    <tr>
	      <td width="100%" valign="top" align="right"><b class="tbltext1a">Gestione Progetti </span></b>
	      </td>
	    </tr>
	  </table>
	</td>
</tr>
</table>
<br>
<table cellpadding="0" cellspacing="0" width="500" border="0">
<tr height="18">
	<td class="sfondomenu" height="18" width="67%">
	<span class="tbltext0"><b>&nbsp;ELENCO RISERVE</b></span></td>
	<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
	<td valign="middle" align="right" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1"></td>
</tr>
<tr width="371" class="SFONDOCOMM">
	<td colspan="3">
		Ricerca degli utenti ammessi ma non vincitori del Bando.
		<a href="Javascript:Show_Help('/Pgm/help/Gestprogetti/monitor/Report/REP_RicAmmNonVinc')">
		<img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0">
		</a>
	</td>
</tr>
<tr height="2">
	<td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif">
	</td>
</tr>
</table>
<br><br>
<%END SUB

SUB CaricaCombo()
dim sPrv, sModo
sModo = Request("MOD")
nIdentUorg = session("iduorg")

	SELECT CASE sModo
	CASE 0
%>
<form method="POST" action="REP_RicAmmNonVinc.asp?MOD=1" name="frmBando" id="frmBando">
<table width="500" border="0" cellspacing="0" cellpadding="2">
<tr>
	<td align="left" width="150" nowrap class="tbltext1">
		<b>Selezionare un Bando</b>	
	</td>
	<td align="left">
<%
	sCreoBandi= nIdentUorg & "||cmbBando|onchange='return InviaBando(this.value)'"
	CreateBandi sCreoBandi
%>
	</td>
</tr>
</table>
</form>
<%
	CASE 1
	dim sBando, sSQL
	sBando = Request.form("cmbBando")
		sSQL="select desc_bando from Bando where ID_BANDO= " & sBando
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
		set rsVal = CC.Execute(sSQL)
		sDescrBnd = rsVal.Fields("desc_bando")
		rsVal.Close	
		set rsVal = nothing	
	
		sSql="Select id_sede from bando_posti where id_bando= " & sBando 
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
		set rsSede = CC.Execute(sSql)
		nIdSede = rsSede.Fields("id_sede")
		rsSede.Close	
		set rsSede = nothing	
%>
<form method="POST" name="frmProvincia">	
<input type="hidden" name="hDescBnd" ID="hDescBnd" value="<%=sDescrBnd%>">
<input type="hidden" name="hIdSede" ID="hIdSede" value="<%=nIdSede%>">
<input type="hidden" name="hdnCmbBando" ID="hdnCmbBando" value="<%=sBando%>">
<table width="500" border="0" cellspacing="2" cellpadding="1">
<tr> 
	<td align="left" width="150" nowrap class="tbltext1">
		<b>Bando selezionato</b>
	</td>
	<td align="left" class="tbltext">
		<b><%=sDescrBnd%></b>
	</td>	
</tr>
</table>

<table cellpadding="1" cellspacing="2" width="500" border="0">	
<input type="hidden" name="cmbBando" value="<%=sBando%>">
<%
	if sProv <> "" then
%>
	<tr align="left">	
		<td class="tbltext1" align="left" width="150">
			<b>Provincia </b>
		</td>
		<td class="tbltext" align="left">	
			<b><option value="<%=sProv%>"><%=DecCodVal("PROV", 0, "", sProv,"")%></option></b>
		</td>
	</tr>
<%
	else
		sMask = SetProvBando(sBando,session("idUorg"))
%>	
		<tr>	
			<td align="left" class="tbltext1" width="150">
			<b>Provincia</b>
			</td>
			<td align="left">			 	 
				<select NAME="cmbProv" class="textblack" onchange="InviaProv()">			 
				<option>&nbsp;</option>
<%				nProvSedi = len(sMask)
			      if nProvSedi <> 0 then
	                pos_ini = 1 	                
	                    for i=1 to nProvSedi/2
	                   		sTarga = mid(sMask,pos_ini,2)											                   		
%>
		                   <option value="<%=sTarga%>"><%=DecCodVal("PROV", 0, "", sTarga,"")%></option>
<%	                    
	                    pos_ini = pos_ini + 2
	                    next
                    end if
%>
			    </select>
			</td>
		</tr>
	<%
	end if %>	
</table>
<br>
<br>
<br>
<table cellpadding="0" cellspacing="0" width="300" border="0">	
	<tr align="center">
	  <td nowrap><a href="javascript:history.go(-1)"><img src="<%=session("Progetto")%>/images/indietro.gif" border="0" name="imgPunto2" onmouseover="javascript:window.status=' '; return true"></a></td>
	</tr>
</table>	
</form>	

	<% 
	'E' la terza volta che entro, combo CENTRI DELL'IMPIEGO
	CASE 2
		sProv=request("cmbProv")
		sDescrBnd= request("hDescBnd")
		sBando=request("cmbBando")
	%>
	<form method="POST" action="REP_VisAmmNonVinc.asp?Sel=1" name="frmCImp" id="frmCImp">
	<input type="hidden" name="hdncmbProv1" ID="hdncmbProv1" value="<%=sProv%>">
	<input type="hidden" name="hdnCmbBando" ID="hdnCmbBando" value="<%=sBando%>">
	<input type="hidden" name="hdnIdSede" ID="hdnIdSede" value="<%=nIdSede%>">
	<table width="500" border="0" cellspacing="2" cellpadding="1">
	<tr> 
		<td align="left" width="200" nowrap class="tbltext1">
			<b>Bando selezionato</b>
		</td>
		<td align="left" width="300" class="tbltext">
			<b><%=sDescrBnd%></b>
		</td>	
	</tr>
	</table>
		<br>
	<table width="500" border="0" cellspacing="2" cellpadding="1">
	<tr>
		<td align="left" width="200" nowrap class="tbltext1">
			<b>Provincia di </b>
		</td>
		<td align="left" width="300" class="tbltext">
			<b><%=DecCodVal("PROV", 0, "", sProv,"")%></b>
		</td>
	</tr>
	</table>
	<br>
<%
dim sSel
	sSQL = "SELECT si.id_sede,si.descrizione " & _ 
			" FROM sede_impresa si,impresa i, bando_posti Bp " & _
            " WHERE si.id_impresa = i.id_impresa " & _ 
            " AND si.id_sede = Bp.id_sede " & _ 
            " AND Bp.id_bando =" & sBando & " AND prv= '" & sProv & "'"
		
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
		set rsCImpiego = CC.Execute(sSQL)
		if rsCImpiego.eof then%>
		<br>
			<table border="0" cellpadding="0" cellspacing="0" width="500">
			<tr>
				<td align="center" colspan="0" width="60%">
					<span class="tbltext">
						&nbsp;
					</span>
				</td>
			</tr>
			<tr>
				<td align="center" colspan="0" width="60%">
					<span class="tbltext3">
						Non ci sono Centri di aggregazione per il Bando richiesto
					</span>
				</td>
			</tr>
			<!--tr>				<td align="center" colspan="0" width="60%">					<span class="tbltext">						&nbsp;					</span>				</td>			</tr-->
			</table>
			<br>
			<br>
			<br>
			<table cellpadding="0" cellspacing="0" width="300" border="0">	
				<tr align="center">
				  <td nowrap><a href="javascript:history.go(-1)"><img src="<%=session("Progetto")%>/images/indietro.gif" border="0" name="imgPunto2" onmouseover="javascript:window.status=' '; return true"></a></td>
				</tr>
			</table>
		<%else%>		
			<table width="500" border="0" cellspacing="2" cellpadding="1">
			<input type="hidden" name="hdnIdSede" ID="hdnIdSede" value="<%=nIdSede%>">
			<tr>
				<td align="left" width="200" nowrap class="tbltext1">
					<b>Relativo a: </b>
				</td>
				<td align="left" width="300">
					<select ID="cmbSede" name="cmbSede" class="textblacka" onchange="InviaCImp()">
					<option value></option>
					<%
					do while not rsCImpiego.EOF
					%>
						<option value="<%=rsCImpiego("ID_SEDE") & "|" & rsCImpiego("DESCRIZIONE")%>"><%=rsCImpiego("DESCRIZIONE")%></option>
					<%
						rsCImpiego.MoveNext
					loop
					%>
					</select>
				</td>
				
			</tr>
			</table>
			<br>
			<br>
			<br>
			<table cellpadding="0" cellspacing="0" width="300" border="0">	
				<tr align="center">
				  <td nowrap><a href="javascript:history.go(-1)"><img src="<%=session("Progetto")%>/images/indietro.gif" border="0" name="imgPunto2" onmouseover="javascript:window.status=' '; return true"></a></td>
				</tr>
			</table>
		<%end if

rsCImpiego.Close	
set rsCImpiego = nothing	
%>
	</form>
<%
	END SELECT		
END SUB
%>
<!--#include virtual = "/include/closeconn.asp"-->
