<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include virtual = "/strutt_testa2.asp"-->
<!--#include virtual ="/include/openconn.asp"-->
<!--#include virtual = "util/portallib.asp"-->
<!--#include virtual = "util/dbutil.asp"-->
<!--#include virtual = "include/DecCod.asp"-->
<!--#include virtual = "include/SelAreaTerrBandi.asp"-->
<!--#include virtual = "include/ControlDateVB.asp"-->
<%
if ValidateService(session("idutente"),"REP_RICDISCENTI", CC) <> "true" then 
	response.redirect "/util/error_login.asp"
end if
%>

<script LANGUAGE="Javascript">
<!--#include virtual="/Include/help.inc"-->
<!--#include virtual = "/Include/ControlDate.inc"-->
<!--#include virtual = "/Include/ControlNum.inc"-->
<!--#include virtual = "/Include/ControlString.inc"-->  

function VaiInizio(){
	location.href = "REP_RicDiscenti.asp";
}

function Reload(){
	document.frmRicDiscenti.action = "REP_RicDiscenti.asp";
	document.frmRicDiscenti.submit();				
}

function ControllaDati(frmRicDiscenti){
	 
	var sDataApp;
	var sDataOggi;	
	
	frmRicDiscenti.Dt_Periodo.value = TRIM(frmRicDiscenti.Dt_Periodo.value);
	if (frmRicDiscenti.Dt_Periodo.value == ""){
		alert("Data riferimento periodo obbligatoria");
		frmRicDiscenti.Dt_Periodo.focus(); 
		return false;
	}   

	//Data Riferimento periodo corretta
	sDataApp = frmRicDiscenti.Dt_Periodo.value;
	if (!ValidateInputDate(sDataApp)){
		frmRicDiscenti.Dt_Periodo.focus(); 
		return false;
	}

	//La data riferimento periodo deve essere minore
	// o uguale alla Data odierna
	//sDataOggi = TRIM(frmRicDiscenti.Dt_Odierna.value);
	//if (!ValidateRangeDate(sDataApp,sDataOggi)){
	//   alert("La Data riferimento periodo non deve essere maggiore della Data odierna'");
	//   frmRicDiscenti.Dt_Periodo.focus();
	//   return false;
	//}
	return true;
}
</script>

<%
sub CaricaBandi()

	sSql =	"SELECT ID_BANDO,DESC_BANDO FROM BANDO " & _
			"WHERE ID_BANDO IN (SELECT ID_BANDO FROM " &_
			"AREA_BANDO WHERE ID_UORG=" & session("iduorg") & ")"
	'Response.Write sSql	  
			 
	dim RecDescBando
	set RecDescBando=Server.CreateObject("ADODB.recordset")
					
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	RecDescBando.Open sSql,CC,3
				
	if RecDescBando.EOF then
	%>	
	 <table border="0" cellspacing="2" cellpadding="2" width="500">
		<tr>
			<td class="tbltext1" align="center" width="150">
				<b>Bando</b>
			</td>
			<td class="tbltext3" align="left" width="350&quot;">
				<b>Non ci sono bandi disponibili</b>	
			</td>
		</tr>
	 </table>
	<%
	else
	%>
		<tr>
			<td class="tbltext1" align="left"><b>Bando:</b>
			</td>
			<td align="left">
				<select class="textblack" name="CmbBando" onchange="JAVASCRIPT:Reload()">
						<option selected></option>
						<%			
						RecDescBando.MoveFirst
						do until RecDescBando.EOF
															
							Response.write "<OPTION  value ='" & RecDescBando("ID_BANDO")& "|" & RecDescBando("DESC_BANDO") & "'> " &_
							mid(RecDescBando("DESC_BANDO"),1,70)  & "</OPTION>"
						
							RecDescBando.MoveNext 				
						loop			
			
						RecDescBando.Close
						%>
				</select>
			</td>
		</tr>
	<%
	end if
	%>
	</table>
<!--	<br><br>	<table cellpadding="0" cellspacing="0" width="300" border="0">			<tr align="center">			<td nowrap><a href="javascript:VaiInizio()" onmouseover="javascript:window.status=' '; return true"><img src="<%=session("Progetto")%>/images/indietro.gif" border="0" name="imgPunto2"></a><td>		</tr>	</table>-->
<%
end sub

'-----------------------------------------------------------

sub CaricaSessioniFormative()

	sSql =	"SELECT COD_SESSIONE, DESC_FASE FROM FASE_BANDO " &_
			"WHERE ID_BANDO = " & nIdBando
	'Response.Write sSql	  
			 
	dim RecDescSessForm
	set RecDescSessForm = Server.CreateObject("ADODB.recordset")
					
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	RecDescSessForm.Open sSql,CC,3
		
	if RecDescSessForm.EOF then	
	%>
		<tr>
			<td align="center" class="tbltext3" colspan="2">
				<b>Non ci sono Sessioni Formative</b>
			</td>
		</tr>	
	<%
	else
	%>
		<tr>
			<td class="tbltext1" align="left" width="120"><b>Sessione Formativa:</b>
			</td>
			<td align="left">
				<select class="textblack" name="CmbSessForm" onchange="JAVASCRIPT:Reload()">
					<option selected></option>
					<%			
					RecDescSessForm.MoveFirst					
					do until RecDescSessForm.EOF
																
						Response.write "<OPTION  value ='" & RecDescSessForm("COD_SESSIONE") & "|" &RecDescSessForm("DESC_FASE") & "'> " &_
						mid(RecDescSessForm("DESC_FASE"),1,50)  & "</OPTION>"
						
						RecDescSessForm.MoveNext 				
					loop			
			
					RecDescSessForm.Close
					%>
				</select>
			</td>
		</tr>  	
	<%
	end if
	%>
	</table>
	<br><br>
	<table cellpadding="0" cellspacing="0" width="300" border="0">	
		<tr align="center">
			<td nowrap><a href="javascript:VaiInizio()" onmouseover="javascript:window.status=' '; return true"><img src="<%=session("Progetto")%>/images/indietro.gif" border="0" name="imgPunto2"></a><td>
		</tr>
	</table>
<%
end sub

'----------------------- MAIN ------------------------------

%>

<table border="0" width="520" cellspacing="0" cellpadding="0" height="81">
	<tr>
		<td width="500" background="<%=Session("Progetto")%>/images/titoli/strumenti2b.gif" height="81" valign="bottom" align="right">
			<table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
				<tr>
					<td width="100%" valign="top" align="right"><b class="tbltext1a">Gestione Progetti</span></b></td>
				</tr>
			</table>
	   </td>
	</tr>
</table>
<br>
<table cellpadding="0" cellspacing="0" width="500" border="0">
	<tr height="18">
		<td class="sfondomenu" height="18" width="67%">
			<span class="tbltext0"><b>&nbsp;REPORT COMPONENTI CLASSE</b></span></td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
		<td valign="middle" align="right" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1"></td>
	</tr>
	<tr width="371" class="SFONDOCOMM">
		<td colspan="3">Selezionare il bando e definire i dettagli per la ricerca.						
			<a href="Javascript:Show_Help('/Pgm/help/Gestprogetti/Monitor/Report/REP_RicDiscenti')">
			<img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
		</td>
	</tr>
	<tr height="2">
		<td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif">
		</td>
	</tr>
</table>
<br><br>	

<form name="frmRicDiscenti" method="post" onsubmit="return ControllaDati(this)" action="REP_VisDiscenti.asp">
	<input type="HIDDEN" name="Dt_Odierna" value="<%=ConvDateToString(date())%>">
	<table cellpadding="0" cellspacing="0" width="500" border="0">	
		<%		
		sApBando = Trim(Request("CmbBando"))
		if sApBando = "" then
			CaricaBandi()
		else
			nIdBando = Mid(sApBando, 1, instr(1, sApBando, "|") - 1)
			sDescBando = Mid(sApBando, instr(1, sApBando, "|") + 1)
			%>
			<input type="hidden" name="CmbBando" value="<%=nIdBando & "|" & sDescBando%>">
			<tr>
				<td class="tbltext1" align="left" width="120"><b>Bando:</b></td>
				<td align="left" class="tbltext">
					<b><%=sDescBando%></b>
				</td>
			</tr>
			<tr>
				<td> &nbsp; </td>
			</tr>
			<%
			sApSessForm = Trim(Request("CmbSessForm"))
			if sApSessForm = "" then
				CaricaSessioniFormative()
			else
				sCodSessForm = Mid(sApSessForm, 1, instr(1, sApSessForm, "|") - 1)
				sDescSessForm = Mid(sApSessForm, instr(1, sApSessFOrm, "|") + 1)
				%>
				<input type="hidden" name="CmbSessForm" value="<%=sCodSessForm & "|" & sDescSessForm%>">
				<tr>
					<td class="tbltext1" align="left"><b>Sessione Formativa:</b>
					<td align="left" class="tbltext">
						<b><%=sDescSessForm%></b>
					</td>
				<tr>
					<td> &nbsp; </td>
				</tr>
				<tr>
					<td class="tbltext1" align="left" width="120"><b>Data Riferimento:</b>
					</td>
					<td align="left" class="tbltext">
						<input type="TEXT" name="Dt_Periodo" value="<%=ConvDateToString(date())%>">
					</td>
				</tr>
			</table>
			<br>
			<br>
			<table cellpadding="0" cellspacing="0" width="300" border="0">	
				<tr align="center">
					<td nowrap><input type="image" name="Conferma" src="<%=Session("progetto")%>/images/conferma.gif"></td value="Registra">
					<td nowrap><a href="javascript:VaiInizio()" onmouseover="javascript:window.status=' '; return true"><img src="<%=session("Progetto")%>/images/indietro.gif" border="0" name="imgPunto2"></a><td>
			    </tr>
			</table>	
			<%
			end if
		end if
		%>
	</form>
<!--#include virtual ="/include/closeconn.asp"-->
<!--#include virtual = "/strutt_coda2.asp"-->



