<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include Virtual = "/strutt_testa2.asp"-->
<!--#include Virtual = "/Include/openconn.asp"-->
<!--#include virtual = "/util/portallib.asp"-->
<!--#include Virtual = "/Include/ControlDateVB.asp"-->
<!--#include Virtual = "/include/DecCod.asp"-->
<!--#include Virtual = "/pgm/Curriculum/Cur_chkCurriculum.asp"-->
<!--#include virtual="/include/SelAreaTerrBandi.asp"-->

<script language="javascript">
<!--#include virtual = "/Include/ControlNum.inc"-->
<!--#include virtual="/Include/help.inc"-->

function ControllaDati() {

	if (frmRicerca.txtPIva.value.length > 0 ){ 

		if ((frmRicerca.txtRagSoc.value != "") || (frmRicerca.txtPrv.value != "")) {
			alert("Indicando el campo Ingresos Brutos no hace falta indicar la Raz�n Social o el departamento")
			frmRicerca.txtPIva.focus() 
			return false
		}

		if (!IsNum(frmRicerca.txtPIva.value)){
			alert("El valor debe ser num�rico")
			frmRicerca.txtPIva.focus() 
			return false
		}

		if (frmRicerca.txtPIva.value.length != 11) {
			alert("El campo Ingresos Brutos debe ser de 11 caracteres")
			frmRicerca.txtPIva.focus() 
			return false
		}
	}else {
		
		if (frmRicerca.txtPrv.value.length == "") {
			alert("Indicar el departamento")
			frmRicerca.txtPrv.focus() 
			return false
		}
		if (frmRicerca.txtRagSoc.value.length == "") {
			alert("Indicar la Raz�n Social")
			frmRicerca.txtRagSoc.focus() 
			return false
		}		
	}

return true
}


</script>
<%

if ValidateService(session("idutente"),"IMP_VISIMPRESA",cc) <> "true" AND ValidateService(session("idutente"),"EXP_EMPRESA",cc) <> "true" then 
	response.redirect "/util/error_login.asp"
end if

' Controllo i diritti dell'utente connesso.
' La variabile di sessione mask � cos� formatta:
' 1 Byte abilitazione funzione Impresa
' 2 Byte abilitazione funzione Sede_impresa
' 3 Byte abilitazione funzione Val_impresa
' dal 4� byte in poi si visualizzano le provincie.

Intestazione
Imposta_Pag()

Dim sFile, sFilePath, sCondizione, sSQL, rsProv


'function Legge(sFile)
'	set oFileSystemObject = Server.CreateObject("Scripting.FileSystemObject")
'	if oFileSystemObject.FileExists(sFilePath) then
'		set oFileArea = oFileSystemObject.OpenTextFile(sFilePath,1)
'		Legge = "'" 
'		Do While Not oFileArea.AtEndOfStream
'			Legge = Legge & "'," & Chr (13) & "'" & oFileArea.ReadLine
'		loop
'		Legge = Legge & "'"
'	end if
'end function

'*************************************************************************************************************************************************************************************************************************************************************************
sub Imposta_Pag()
	
'	sFile = mid(session("progetto"),2)
'	sFilePath = SERVER.MapPath("\") & "\"& sFile &"\Conf\" & sFile & ".txt"
	
'	if sFile <> "PLAVORO" then
'		'sCondizione = mid(Legge(sFile),4)
		'Response.write sCondizione
		'Response.Write ValTesto 
'	end if
	'Session("mask") = "151515RMNAMICTFI"
	
'	if sFile <> "PLAVORO" then
'		sSQL = "SELECT CODICE,DESCRIZIONE FROM TADES WHERE NOME_TABELLA ='PROV' AND CODICE IN (" & sCondizione & ") ORDER BY DESCRIZIONE"
'	else
'		sSQL = "SELECT CODICE,DESCRIZIONE FROM TADES WHERE NOME_TABELLA ='PROV' ORDER BY DESCRIZIONE"
'	end if
'	
'	set rsProv = CC.Execute(sSQL)
'	if sFile <> "PLAVORO" then
'    if sCondizione <> "" then
'		sCond = "codice IN (" & sCondizione & ")"
'	else
'		sCond = ""
'	end if
	
aProv=SetProvUorg(Session("idUorg"))

IF aProv="" THEN
%>
	<script>
		alert(" No es posible acceder a la informaci�n territorial !")
<%	
		response.write "top.location.href='" & Session("Progetto") & "/default.asp';"		
%>			
	</script>
<%
	Response.End 
end if

'nProvSedi = len(aProv)

'if nProvSedi <> 0 then
'	nProvSedi = nProvSedi/2'

	'sCondizione = "Codice in ("

	'for i=1 to nProvSedi
	'	if i <> 1 then
	'		sCondizione = sCondizione & ","
	'	end if
	'	pos_ini = 2 * (i-1)
	'	sCondizione = sCondizione & "'" & mid(aProv,1+pos_ini,2) & "'"
	'next				
	'sCondizione = sCondizione & ")"
if aProv <> "" then 
	Vector = split(aProv,",")
	aProv = ""
	for i = 1 to ubound(Vector)
		aProv = aProv & "'" & Vector(i) & "'" & ","
	next

	if right(aProv,1) = "," then 
		aProv = left(aProv,len(aProv)-1)
	end if 

	sCondizione = " Codice in (" & aProv & ")"
	sElenco = aProv
else 	
	sCondizione = ""
end if

	
	sTabella = "PRVEX"
	dData	 = Date()
	nOrder   = 0	'Descrizione
	Isa		 = 0
	aProv = decodTadesToArray(sTabella,dData,sCondizione,nOrder,Isa)
	if isArray(aProv) then
%>
		<form name="frmRicerca" method="POST" onsubmit="return ControllaDati()" action="FAS_RisListAzienda.asp">
		<br>
		<table border="0" CELLPADDING="1" CELLSPACING="2" width="500">
			<tr height="17">
				<td width="150" class="tbltext1">
					<b>Departamento</b>*
				</td>
				<td width="350" class="tbltext1">
					<b><select class=textblack name="txtPrv">
					<option value></option>
					<option value="1">Todas</option>
<%					for i = 0 to ubound(aProv) -1	
%>						<option value="<%=aProv(i,i,i)%>"><%=aProv(i,i+1,i)%></option>		
<%					next
					Erase aProv
%>				</td>
			</tr> 
			
			<tr height="17">
				<td width="150" class="tbltext1">
					<b>Raz�n social</b>*
				</td>
				<td width="350" class="tbltext1">
					<b><input class="textblacka" type="text" name="txtRagSoc" style="TEXT-TRANSFORM: uppercase" size="35" maxlength="50"></b>
				</td>
			</tr>
			<tr height="17">
				<td colspan="3">
					&nbsp;
				</td>
			</tr>
			<tr height="2">
				<td width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
			</tr>
			<tr height="2">
				<td width="100%" colspan="3" background></td>
			</tr>
			<tr height="17">
				<td colspan="3">
					&nbsp;
				</td>
			</tr>
			<tr height="17">
				<td width="150" class="tbltext1">
					<b>Ingresos Brutos</b>*
				</td>
				<td width="350" class="tbltext1">
					<b><input class="textblacka" type="text" name="txtPIva" style="TEXT-TRANSFORM: uppercase" maxlength="11" size="11">
				</td>
			</tr>		
		</table>
		<br>
		<br>
		<br>
		<table border="0" cellpadding="0" cellspacing="0" width="510" align="center">
			<tr>
				<td align="middle" colspan="2">
					<input type="image" name="Invia" src="<%=Session("Progetto")%>/images/lente.gif" border="0" value="Conferma"> <!--onclick="return ControllaDati()"-->		
				</td>
		   </tr>
			<tr>
				<td>&nbsp;</td>
		   </tr>
		</table>		
		</form>
<%	else
		Mensaje ("P�gina momentaneamente no disponible.")
	end if
end sub


'*************************************************************************************************************************************************************************************************************************************************************************
Sub Intestazione()
%>
	<table border="0" width="530" cellspacing="0" cellpadding="0" height="81">
	   <tr>
	     <td width="530" background="<%=Session("Progetto")%>/images/titoli/strumenti2g.gif" height="81" valign="bottom" align="right">
	       <table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
	         <tr>
	           <td width="100%" valign="top" align="right"><b class="tbltext1a">Expediente Empresa &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></b></td>
	         </tr>
	       </table>
	     </td>
	   </tr>
	</table><br>
		
	<table border="0" CELLPADDING="0" CELLSPACING="0" width="500" align="center">
		<tr height="18">
			<td class="sfondomenu" width="67%" height="18"><span class="tbltext0">
				<b>&nbsp;EXPEDIENTE EMPRESA</b>
			</td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">
			</td>
			<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
		</tr>
		<tr>
			<td class="sfondocomm" width="57%" colspan="3">
			Podr� efectuar una b�squeda de las empresas presentes sobre el territorio. 
	<br>Seleccione los par�metros de b�squeda. 
	Tenga presente que la b�squeda debe especificar obligatoriamente los campos <b>Departamento</b> y <b>Raz�n Social</b> (en forma parcial) u otra alternativa solamente el campo <b>Ingresos Brutos</b>.<a href="Javascript:Show_Help('/Pgm/help/Fasc_Azienda/FAS_RicAzienda')"><img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
			</td>
		</tr>
		<tr height="2">
			<td class="sfondocomm" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>
	<br>
<%
end sub
'*************************************************************************************************************************************************************************************************************************************************************************
Sub Messaggio(Testo)
%>		
		<br><br>
		<table width="500" align="center" border="0" cellspacing="2" cellpadding="1">
			<tr height="20"> 
			  	<td class="tbltext3" align="middle" width="743">
					<b><%=Testo%></b>
				</td>
			</tr>
		</table>
		<br><br>
<%				
	End Sub
'*************************************************************************************************************************************************************************************************************************************************************************
%>
<!--#include Virtual = "/Include/Closeconn.asp"-->
<!--#include Virtual="/strutt_coda2.asp"-->	
