<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include virtual="/util/portallib.asp"-->
<!--#include Virtual = "/strutt_testa2.asp"-->
<!--#include Virtual = "/include/SysFunction.asp"-->
<!--#include Virtual = "/include/DecComun.asp"-->
<!--#include Virtual = "/include/DecCod.asp"-->
<!--#include Virtual ="/Include/openconn.asp"-->

<%
'*****************************************************************************************************************
'*********** MAIN ************************************************************************************************
'*****************************************************************************************************************

	Dim nIdRic, sEmail, sEmailUt, sTipoProf, sTipoArea, sIdSede, sTitulo

	if session("Creator") = "" then
		response.redirect "/util/error_login.asp"
	end if

	nIdRic	     = Request("idRic")

	sql = ""
	sql = "select ID_FIGPROF, ID_AREAPROF, ID_SEDE, OFFER_TITLE from richiesta_sede where " &_
		  " id_richiesta = " & nIdRic	

	set RcdSet = CC.Execute(sql)
	sTipoProf = RcdSet("ID_FIGPROF")
	sTipoArea = RcdSet("ID_AREAPROF")
	sIdSede = RcdSet("ID_SEDE")
	sTitulo = RcdSet("OFFER_TITLE")

	if not RcdSet.EOF THEN
		sSQL = "SELECT DENOMINAZIONE FROM FIGUREPROFESSIONALI WHERE ID_FIGPROF=" &_
						CLng(sTipoProf) & " AND ID_AREAPROF = " & CLng(sTipoArea)
		set rsDenom = CC.Execute(sSQL)
		sDen = rsDenom("DENOMINAZIONE")
		rsDenom.CLose
		set rsDenom = nothing

		sSQL2 ="SELECT E_MAIL_SEDE FROM SEDE_IMPRESA WHERE ID_SEDE =" & sIdSede
		set rstDescr = cc.Execute(sSQL2)
		sEmail = rstDescr("E_MAIL_SEDE")
		rstDescr.Close
		set rstDescr = nothing

		sSQL3 = "SELECT RAG_SOC,DESCRIZIONE FROM IMPRESA A, SEDE_IMPRESA B " &_
				"WHERE A.ID_IMPRESA = B.ID_IMPRESA AND B.ID_SEDE = " & sIdSede
		set rsImpresa = cc.Execute(sSQL3)
		sImpresa = "" & rsImpresa("RAG_SOC") & " / " & rsImpresa("DESCRIZIONE") & ""
		rsImpresa.Close
		set rsImpresa = nothing

		sSQL4 = "SELECT EMAIL FROM UTENTE WHERE CREATOR = " & sIdSede
		set rsUtente = cc.Execute(sSQL4)
		sEmailUt = rsUtente("EMAIL")
		rsUtente.Close
		set rsUtente = nothing
	end if

	RcdSet.Close
	set RcdSet = nothing

	if sEmail <> "" or sEmailUt <> "" then 
		sEmail= sEmailUt & ";" & sEmail
		InfoFigProf nIdRic,sDen,sImpresa,sEmail,sTitulo
	else%>
		<script language="JavaScript">
			alert("El correo electr�nico de confirmaci�n no fue enviado a la Empresa. Por favor corrija la direcci�n electr�nica de la Empresa");
		</script><%
	end if

'**********************************************************************************************************************************************************************************	
sub InfoFigProf(nIdRic,sDen,sImpresa,EMail,sTitulo)

	Dim objMail 

	on error resume next

	Set objMail = CreateObject("CDO.Message")
	objMail.Subject = "Publicaci�n de Vacante Autorizada"
	objMail.From = "Servicio de Empleo<redempleo@mintrabajo.gov.co>"
	objMail.To =   Email
	objMail.bcc =   "redempleo.gov.co@gmail.com"
	objMail.Configuration.Fields.Item ("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2 'Send the message using the network (SMTP over the network).
	objMail.Configuration.Fields.Item ("http://schemas.microsoft.com/cdo/configuration/smtpserver") ="smtp.gmail.com"
	objMail.Configuration.Fields.Item ("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25
	objMail.Configuration.Fields.Item ("http://schemas.microsoft.com/cdo/configuration/smtpusessl") = True 'Use SSL for the connection (True or False)
	objMail.Configuration.Fields.Item ("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 60
	     
	' If your server requires outgoing authentication uncomment the lines bleow and use a valid email address and password.
	objMail.Configuration.Fields.Item ("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate") = 1 'basic (clear-text) authentication
	objMail.Configuration.Fields.Item ("http://schemas.microsoft.com/cdo/configuration/sendusername") ="soporte.tecnologia@serviciodeempleo.gov.co"
	objMail.Configuration.Fields.Item ("http://schemas.microsoft.com/cdo/configuration/sendpassword") ="UAESPE2014Soporte.Tecnologia"



	objMail.Configuration.Fields.Update

	HTML = "<!DOCTYPE HTML PUBLIC ""-//IETF//DTD HTML//IT"">" & NL 
	HTML = HTML & "<HTML>"
	HTML = HTML & "<BODY>"
	HTML = HTML & "<div=center>"
	HTML = HTML & "<table border=0 width=95% align=center cellpadding=1 cellspacing=2 ><tr><td>"
	HTML = HTML & "<font size='3' color='#000080' face='Verdana'><br><i><b>Publicaci�n de Vacante Autorizada</b></i></font><br><br>"
	HTML = HTML & "<font size='2' color='#000080' face='Verdana'><br>Se�ores,<br>"
	HTML = HTML & "<B>" & sImpresa & "</B><br><br>"
	HTML = HTML & "Les informamos que la publicaci&oacute;n de su vacante Ref. " & nIdRic
	HTML = HTML & " para el cargo <B>" & sTitulo & " </B>(" & sDen
	HTML = HTML & "), ha sido autorizada." 
	HTML = HTML & "<br><br>Cordialmente,<br></br>"
	HTML = HTML & "</td></tr></font>"
	HTML = HTML & "</table>"
	HTML = HTML  +"<table class=MsoTableGrid border=0 cellspacing=0 cellpadding=0"
	HTML = HTML  +" style='border-collapse:collapse;border:none'>"
	HTML = HTML  +" <tr>"
	HTML = HTML  +"  <td width=242 valign=top style='width:181.3pt;border:none;border-right:solid #404040 2.25pt;"
	HTML = HTML  +"  padding:0cm 5.4pt 0cm 5.4pt'>"
	HTML = HTML  +"  <p class=MsoNormal><a href='http://www.redempleo.gov.co/' target='_blank'><b><span"
	HTML = HTML  +"  style='font-size:10.0pt;font-family:'Verdana','sans-serif';color:blue'><img"
	HTML = HTML  +"  border=0 width=219 height=90 src='http://www.redempleo.gov.co:8080/ba/LogoSE_333x137.png'></span></b></a></p>"
	HTML = HTML  +"  </td>"
	HTML = HTML  +"  <td width=346 valign=top style='width:259.2pt;border:none;padding:0cm 5.4pt 0cm 5.4pt'>"
	HTML = HTML  +"  <p class=MsoNormal><a href='http://www.redempleo.gov.co'><span"
	HTML = HTML  +"  style='font-size:10.0pt;font-family:'Verdana','sans-serif''>www.redempleo.gov.co</span></a></p>"
	HTML = HTML  +"  <p class=MsoNormal><span class=letranormalmenor><span style='font-size:10.0pt'>Fuera"
	HTML = HTML  +"  de Bogot�: 018000513100 </span></span><span style='font-size:10.0pt'><br>"
	HTML = HTML  +"  <span class=letranormalmenor>En Bogot�: 4893900 Opci�n. 2 </span><br>"
	HTML = HTML  +"  <span class=letranormalmenor>Desde su celular marque 120</span><br>"
	HTML = HTML  +"  <span class=letranormalmenor>lunes a viernes de 7 am - 7 pm, s�bados. 8 - 11"
	HTML = HTML  +"  am</span><br>"
	HTML = HTML  +"  <span class=letranormalmenor>Buz�n 24 horas</span></span></p>"
	HTML = HTML  +"  </td>"
	HTML = HTML  +" </tr>"
	HTML = HTML  +"</table>"
	HTML = HTML  +"</div></BODY></HTML>"

	objMail.HTMLBody = HTML
	if session("idgruppo")="25" then
		objMail.Send 	'(attivare in produzione)	
	end if
end sub

%>



<!--#include Virtual ="/Include/closeconn.asp"-->
<!--#include Virtual="/strutt_coda2.asp"-->




