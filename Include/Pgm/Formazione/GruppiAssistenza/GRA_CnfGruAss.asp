<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include Virtual="/strutt_testa2.asp"-->
<!--#include virtual="/Include/OpenConn.asp"-->
<!--#include file="GRA_Inizio.asp"-->
<!--#include virtual="/Include/ControlDateVB.asp"-->
<!--#include virtual="/Include/SysFunction.asp"-->
<!--#include virtual="/Util/DButil.asp"-->

<%
dim aUtenti,dDtIniAss,sAction
dim sSQL,dDtFineAss,nNumUtenti
dim nIdGrupAss,d,dNow

dNow = ConvDateToDBs(Date)

Inizio()

nMenTut		= clng(Request.Form("cmbMenTut"))
sCodRuolo	= Request.Form ("Ruolo")
sAction		= Request.Form ("txtAction")

'	Response.Write "<BR>nNumUtenti = "	& nNumUtenti
'	Response.Write "<BR>nMenTut = "		& nMenTut
'	Response.Write "<BR>dDtIniAss = "	& dDtIniAss
'	Response.Write "<BR>dDtFineAss = "	& dDtFineAss
CC.BeginTrans
select case sAction
	case "INS"
		' *****************************************************
		aUtenti		= split(Request.Form("aUtenti"),", ")
		nNumUtenti	= Request.Form ("txtNumUte")
		dDtIniAss	= ConvDateToDBs(ConvDateToString(Request.Form ("txtDtIniAss")))
		dDtFineAss	= ConvDateToString(Request.Form ("txtDtFinAss"))
		' *****************************************************
		if dDtFineAss <> "" then
			dDtFineAss = ConvDateToDBs(dDtFineAss)
		else
			dDtFineAss = "null"
		end if		
		sSQL = "INSERT INTO ASSISTENZAGRUPPO " &_
			"(IDUTENTE,DT_INIASS,DT_FINEASS,NUM_ASSISTITI,COD_RUOLO,STATO_ASS)" &_
			" VALUES " &_
			"(" & nMenTut & "," &_
				 dDtIniAss & "," & dDtFineAss & "," & nNumUtenti & ",'" & sCodRuolo & "','A')"
		sErrore = EseguiNoC(sSQL,CC)
		if sErrore = "0" then
			sSQL = "SELECT ID_ASSGR FROM ASSISTENZAGRUPPO WHERE " &_
				" IDUTENTE = " & nMenTut &_
				"AND COD_RUOLO = '" & sCodRuolo & "'" &_
				"AND DT_INIASS = " & dDtIniAss &_
				"AND NUM_ASSISTITI = " & nNumUtenti &_
				" AND STATO_ASS ='A'"
			if dDtFineAss <> "null" then
				sSQL = sSQL & "AND DT_FINEASS = " & dDtFineAss
			else
				sSQL = sSQL & "AND DT_FINEASS IS NULL"
			end if
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
			set rsAssGrup = CC.Execute(sSQL)
			if not rsAssGrup.eof then
				nIdAssGrup = rsAssGrup("ID_ASSGR")
			else
				Response.Write "Errore"
				Response.End
			end if
			rsAssGrup.Close			
			set rsAssGrup = nothing 
			
			sSQL = "UPDATE ASSISTENZAGRUPPO SET ID_GRUTASS = " & CLng(nIdAssGrup) & " WHERE ID_ASSGR = " &_
				 CLng(nIdAssGrup)
			sErrore = EseguiNoC(sSQL,CC)

			if sErrore = "0" then
				for i = 0 to ubound(aUtenti)
					sSQL = "INSERT INTO ASSISTENZAUTENTE " &_
						"(ID_GRUTASS,IDUTENTE)" &_
						" VALUES " &_
						"(" & nIdAssGrup & "," & aUtenti (i) & ")"
						sErrore = EseguiNoC(sSQL,CC)
					if sErrore <> "0" then
						exit for 
					end if
				next 
			end if
		end if
	case "MOD" ' Modifico l'assegnazione del Tutore / Mentore
		nIdGrupAss = clng(Request.Form("txtGruppoAss"))
		nNumUtenti	= Request.Form ("txtNumUte")	
		dDtIniAss	= ConvDateToDBs(ConvStringToDate(Request.Form ("txtDtIniAss")))
		dDTFinAss	= ConvStringToDate(Request.Form ("txtDtFinAss"))
		
		sSQL = "SELECT IDUTENTE,ID_GRUTASS,NUM_ASSISTITI,COD_RUOLO FROM ASSISTENZAGRUPPO WHERE ID_ASSGR = " &_
			CLng(nIdGrupAss)
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
		set rsAssGr = CC.Execute(sSQL)
		if not rsAssGr.eof then
			nIdUteOld	= clng(rsAssGr("IDUTENTE"))
			nIdGrutAss	= clng(rsAssGr("ID_GRUTASS"))
			nNumAss		= clng(rsAssGr("NUM_ASSISTITI"))
			sCodRuolo	= rsAssGr("COD_RUOLO")
			sErrore = 0
		else
			sErrore = 1
		end if
		rsAssGr.Close
		set rsAssGr = nothing 	 
		if sErrore = "0" then
			if dDTFinAss <> "" then
				dDTFinAss = ConvDateToDbs(dDTFinAss)
			else
				dDTFinAss = "null"
			end if


			if nIdUteOld <> nMenTut then
				sSQL = "UPDATE ASSISTENZAGRUPPO SET STATO_ASS='R', " &_
					"DT_FINEASS = " & dNow & " WHERE ID_ASSGR = " &_
					CLng(nIdGrupAss)
				sErrore = EseguiNoC(sSQL,CC)
				if sErrore = "0" then
					sSQL = "INSERT INTO ASSISTENZAGRUPPO " &_
						" (IDUTENTE,DT_INIASS,DT_FINEASS,NUM_ASSISTITI,STATO_ASS,ID_GRUTASS,COD_RUOLO) " &_
						" VALUES (" & nMenTut & "," & dDtIniAss &_
						"," & dDTFinAss & "," & nNumAss & ",'A'," & nIdGrutAss & ",'" & sCodRuolo & "')"
					sErrore = EseguiNoC(sSQL,CC)
				end if

			else
				sSQL = "UPDATE ASSISTENZAGRUPPO SET DT_INIASS = " & dDtIniAss &_
					",DT_FINEASS = " & dDTFinAss & " WHERE ID_ASSGR = " &_
					CLng(nIdGrupAss)
				sErrore = EseguiNoC(sSQL,CC)
			end if
		end if
		
	case "CAN" ' Sciolgo il gruppo 
		
		nIdGruppo = clng(Request.Form("txtGruppoAss"))
		sSQL = "SELECT ID_GRUTASS FROM ASSISTENZAGRUPPO WHERE ID_ASSGR = " &_
			CLng(nIdGruppo)

'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
		set rsAssGr = CC.Execute(sSQL)
		if not rsAssGr.eof then
			nIdGrupAss	= clng(rsAssGr("ID_GRUTASS"))
			sErrore = 0
		else
			sErrore = 1
		end if
		rsAssGr.Close
		set rsAssGr = nothing 	 
		if sErrore = "0" then
			sSQL = "UPDATE ASSISTENZAGRUPPO SET STATO_ASS='C', DT_FINEASS = " & dNow &_
				" WHERE ID_ASSGR = " & CLng(nIdGruppo) 
			sErrore = EseguiNoC(sSQL,CC)
			if sErrore = "0" then
				sSQL = "DELETE FROM ASSISTENZAUTENTE WHERE ID_GRUTASS = " & CLng(nIdGrupAss)
				sErrore = EseguiNoC(sSQL,CC)
			end if
		end if
	case "ASS" ' Associazione gruppo manuale

		aMEUtenti		= split(Request.Form("aMEUtenti"),", ")
		aTUUtenti		= split(Request.Form("aTUUtenti"),", ")
		nIdGrutas		= clng(Request.Form("cmbGruppiAss"))

		sSQL = "SELECT COD_RUOLO FROM ASSISTENZAGRUPPO WHERE ID_GRUTASS = " & CLng(nIdGrutas)
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
		set rsCodGru = CC.Execute(sSQL)
		if not rsCodGru.eof  then
			if rsCodGru("COD_RUOLO") = "ME" then
				if Request.Form("aMEUtenti") <> ""  then
					for i = 0 to ubound(aMEUtenti)
						sSQL = "INSERT INTO ASSISTENZAUTENTE " &_
							"(ID_GRUTASS,IDUTENTE)" &_
							" VALUES " &_
							"(" & nIdGrutas & "," & aMEUtenti (i) & ")"
							sErrore = EseguiNoC(sSQL,CC)
						if sErrore <> "0" then
							exit for 
						end if
					next 
					if sErrore = "0" then
						sSQL = "UPDATE ASSISTENZAGRUPPO SET NUM_ASSISTITI = ( NUM_ASSISTITI + " & ubound(aMEUtenti)+1 &_
							") WHERE ID_GRUTASS = " & nIdGrutas
						sErrore = EseguiNoC(sSQL,CC)
					end if
				else
					sErrore = "Non ci sono persone disponibili da assegnare al gruppo mentor."			
				end if
			else
				if Request.Form("aTUUtenti") <> "" then
					for i = 0 to ubound(aTUUtenti)
						sSQL = "INSERT INTO ASSISTENZAUTENTE " &_
							"(ID_GRUTASS,IDUTENTE)" &_
							" VALUES " &_
							"(" & nIdGrutas & "," & aTUUtenti (i) & ")"
							sErrore = EseguiNoC(sSQL,CC)
						if sErrore <> "0" then
							exit for 
						end if
					next
					if sErrore = "0" then
						sSQL = "UPDATE ASSISTENZAGRUPPO SET NUM_ASSISTITI = ( NUM_ASSISTITI + " & ubound(aTUUtenti)+1 &_
							") WHERE ID_GRUTASS = " & nIdGrutas
						sErrore = EseguiNoC(sSQL,CC)
					end if
					
				else
					sErrore = "<Br>Non ci sono persone disponibili da assegnare al gruppo tutor."			
				end if

			end if
		else
		
		end if	
		rsCodGru.close
		set rsCodGru = nothing 	
	case "MAN"
		' *****************************************************
		nNumRec	= Request.Form ("txtNumRec")
		dDtIniAss	= ConvDateToDBs(ConvStringtoDate(Request.Form ("txtDtIniAss")))
		dDtFineAss	= Request.Form ("txtDtFinAss")
	' *****************************************************
		if dDtFineAss <> "" then
			dDtFineAss = ConvDateToDBs(ConvStringtodate(dDtFineAss))
		else
			dDtFineAss = "null"
		end if		
		nNumUtenti = 0
		for i = 0 to clng(nNumRec)
		     if Request.Form ("chk" & i) = "SI" then
				nNumUtenti = nNumUtenti +1
		     end if
		next
		
		sSQL = "INSERT INTO ASSISTENZAGRUPPO " &_
			"(IDUTENTE,DT_INIASS,DT_FINEASS,NUM_ASSISTITI,COD_RUOLO,STATO_ASS)" &_
			" VALUES " &_
			"(" & nMenTut & "," &_
				 dDtIniAss & "," & dDtFineAss & "," & nNumUtenti & ",'" & sCodRuolo & "','A')"
		sErrore = EseguiNoC(sSQL,CC)
		if sErrore = "0" then
			sSQL = "SELECT ID_ASSGR FROM ASSISTENZAGRUPPO WHERE " &_
				" IDUTENTE = " & nMenTut &_
				"AND COD_RUOLO = '" & sCodRuolo & "'" &_
				"AND DT_INIASS = " & dDtIniAss &_
				"AND NUM_ASSISTITI = " & nNumUtenti &_
				" AND STATO_ASS ='A'"
			if dDtFineAss <> "null" then
				sSQL = sSQL & "AND DT_FINEASS = " & dDtFineAss
			else
			sSQL = sSQL & "AND DT_FINEASS IS NULL"
			end if
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
			set rsAssGrup = CC.Execute(sSQL)
			if not rsAssGrup.eof then
				nIdAssGrup = rsAssGrup("ID_ASSGR")
			else
				Response.Write "Errore"
				Response.End
			end if
			rsAssGrup.Close			
			set rsAssGrup = nothing 
		
			sSQL = "UPDATE ASSISTENZAGRUPPO SET ID_GRUTASS = " & CLng(nIdAssGrup) & " WHERE ID_ASSGR = " &_
				 CLng(nIdAssGrup)
			sErrore = EseguiNoC(sSQL,CC)

			if sErrore = "0" then
				for Y = 0 to clng(nNumRec)
					if Request.Form ("chk" & Y) = "SI" then
			            sID =Request.form("txtID" & Y)
						sSQL = "INSERT INTO ASSISTENZAUTENTE " &_
							"(ID_GRUTASS,IDUTENTE)" &_
							" VALUES " &_
							"(" & nIdAssGrup & "," & sID & ")"
							sErrore = EseguiNoC(sSQL,CC)
						if sErrore <> "0" then
							exit for 
						end if
					end if
				next 
			end if
		end if
end select 
if sErrore <> "0" then
	CC.RollBackTrans
	%>
	<br>
	<table width="500" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td align="center" class="tblText3">
			 <br><%=sErrore%>
			</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td align="center">
				<a href="Javascript:history.back()">
				<img border="0" src="<%=session("Progetto")%>/images/indietro.gif">
				</a>
			</td>
		</tr>
	</table> 
<%else
'	CC.RollBackTrans
	CC.CommitTrans%>
	<br><br>
	<table width="500" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td align="center" class="tblText3">
			<script language="javascript">
				alert("Operazione eseguita con successo");
				location.href = ("GRA_VisGruAss.asp");
			</script>
			</td>
		</tr>
	</table> 
	<meta http-equiv="Refresh" content="3; URL=GRA_VisGruAss.asp">
<%end if%>
<!--#include virtual="/Include/CloseConn.asp"-->
<!--#include Virtual="/strutt_coda2.asp"-->
