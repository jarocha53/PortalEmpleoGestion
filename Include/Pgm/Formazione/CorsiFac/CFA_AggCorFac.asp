<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%@ Language=VBScript %>
<html>
<head>
<title> Aggiungi Corsi Facoltativi </title>
<link REL="STYLESHEET" TYPE="text/css" HREF="<%=session("progetto")%>/fogliostile.css">
<meta NAME="GENERATOR" Content="Microsoft Visual Studio 6.0">
<script Language="JavaScript">
function ControllaDati(Objchkbox)
{
	var n
	n=Objchkbox.length
	if (n==null)
	{
		if (!Objchkbox.checked)
		{
			alert('Selezionare il corso per aggiungerlo.')
			document.frmAreaTem.cmbAreaTem.disabled=false
			return false
		}
	}
	else
	{
		for (i=0;i<n;i++)
		{
			if (Objchkbox[i].checked)
			{
				return true
			}
		}
		alert('Selezionare almeno un corso.')
		document.frmAreaTem.cmbAreaTem.disabled=false
		return false
	}
return true	
}
</script>
</head>
<body class="sfondocentro">
<%
'-------------------------------------------------------------------------------
Sub Inizio
	dim sFunzione
	dim sTitolo
	dim sCommento
	dim bCampiObbl

	sFunzione = ""
	sTitolo = "Aggiungi CORSI FACOLTATIVI"
	sCommento = "Scegliere un' Area Tematica, quindi selezionare uno o pi� corsi e premere<br> il tasto Invia."
	bCampiObbl = false
	sHelp = "/Pgm/help/Formazione/CorsiFac/CFA_AggCorFac/"
	
%>	
	<!--#include virtual="/include/SetTestata.asp"-->

<%
End sub
'-------------------------------------------------------------------------------
Sub ImpostaPag
	dim Sql
	dim iIdGruppo,iIdArea
	dim EsisteCorsoDaSelezionare
	
	iIdGruppo=Request("UidGruppo")
	iIdArea=Request("cmbAreaTem")

	'Response.Write "iIdGruppo=" & iIdGruppo & "<br>"
	'Response.Write "iIdArea=" & iIdArea & "<br>"
	
	sSql="SELECT ID_AREA,TITOLOAREA " &_
		"FROM AREA " &_
		"ORDER BY TITOLOAREA"

	'Response.Write sSql & "<br>"
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	set RstArea=CC.execute (sSql)
	
	if not RstArea.eof then
%>		<form name="frmAreaTem" id="frmAreaTem" action="CFA_AggCorFac.asp">
		<table border="0" width="98%">
			<tr>
			<td width="30%" class="tbltext1BFad">Area tematica:&nbsp;
			</td>
			<td width="70%">
				<select id="cmbAreaTem" name="cmbAreaTem" class="textblackFad" OnChange="JavaScript:document.frmAreaTem.submit();">
				<option></option>
<%				do while not RstArea.eof
%>					<option value="<%=RstArea("ID_AREA")%>" <%if (cstr(RstArea("ID_AREA"))=cstr(iIdArea)) then Response.Write "selected" %>><%=RstArea("TITOLOAREA")%></option>
<%					RstArea.MoveNext
				loop			
%>				</select>
			</td>
			</tr>
		</table>
		<input type="hidden" name="UidGruppo" id="UidGruppo" value="<%=iIdGruppo%>">
		</form>
<%	end if
	RstArea.close
	set RstArea=nothing
	
	
	if (iIdArea<>"") then
	
		'Prendo tutti i corsi da CORSO (secondo certe restrizioni) e che non risultano su CATCORSI
		'(su CATCORSI sono presenti tutti i corsi accociati al gruppo con il flag obbligatorio o meno)
		'come obbligatori (quindi i facoltativi)per il gruppo prescelto, inoltre prendo l'id da CATCORSI se presente
		'(nota:avendo escluso quelli Obbligatori se ci sono compariranno solo i facoltativi)
		'solo quelli per� del gruppo scelto; i corsi che non sono presenti in CATCORSI avranno il 
		'campo ID_COR_PRESENTE del recordset, nullo 
		Sql =		" SELECT "
		Sql = Sql &  "   C.ID_CORSO,CATCORSI.ID_CORSO AS ID_COR_PRESENTE,"
		Sql = Sql &  "   C.TIT_CORSO,"
		Sql = Sql &  "   C.DESC_CORSO"
		Sql = Sql &  " FROM "
		Sql = Sql &  "   CORSO C,CATCORSI"
		Sql = Sql &  " WHERE "
		Sql = Sql &  "   C.ID_AREATEMATICA=" & iIdArea & " AND"
		Sql = Sql &  "   C.DT_DISPONIBILE<=" & ConvDateToDBs(Date()) & " AND"
		Sql = Sql &  "   (C.FL_PUBBLICO=0 OR C.ID_PROJOWNER='" & mid(session("progetto"),2) & "') AND"
		Sql = Sql &  "   C.ID_CORSO NOT IN (SELECT ID_CORSO"
		Sql = Sql &							" FROM CATCORSI"
		Sql = Sql &							" WHERE IDGRUPPO=" & iIdGruppo & " AND FL_OBBL=1) AND"
		Sql = Sql &  " CATCORSI.ID_CORSO(+)=C.ID_CORSO AND CATCORSI.IDGRUPPO(+)=" & iIdGruppo
		Sql = Sql &  " ORDER BY C.TIT_CORSO"
		'Response.Write Sql & "<br>"	
		'Response.End
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
		set RstCorFac=CC.execute(Sql)
		if not RstCorFac.eof then
%>			<br>
			<form name="frmAggCorFac" id="frmAggCorFac" action="CFA_CnfAggCorFac.asp" OnSubmit="JavaScript:return ControllaDati(document.frmAggCorFac.chkCorFac);" method="post">
			<table border="0" cellspacing="1" width="98%">
				<tr class="sfondoCommFad">
					<td width="5">&nbsp;
					</td>
					<td>&nbsp;<b>Titolo</b>
					</td>
					<td>&nbsp;<b>Descrizione</b>
					</td>
				</tr>		
<%				EsisteCorsoDaSelezionare=false
				do while not RstCorFac.eof
%>					<tr class="tblsfondoFad">
						<td width="5">
<%							if (isnull(RstCorFac.fields("ID_COR_PRESENTE"))) then
								EsisteCorsoDaSelezionare=true
								'Corso non presente
%>								<input type="checkbox" id="chkCorFac" name="chkCorFac" value="<%=RstCorFac("ID_CORSO")%>" onclick="JavaScript:document.frmAreaTem.cmbAreaTem.disabled=true;">
<%							else
								'Corso presente
%>								<img src="<%=session("progetto")%>/images/Okvr.gif">							
<%							end if
%>						</td>
						<td class="tblDettFad">&nbsp;<%=server.HTMLEncode(RstCorFac("TIT_CORSO"))%>
						</td>
						<td class="tblDettFad">&nbsp;<%=server.HTMLEncode(RstCorFac("DESC_CORSO"))%>
						</td>
					</tr>
<%					RstCorFac.MoveNext
				loop
%>			</table>
			<br>
			<br>
			<table border="0">
				<tr>
<%					if EsisteCorsoDaSelezionare then
%>						<td>
							<a href="JavaScript:self.close();">
							<img src="<%=session("progetto")%>/images/annulla.gif" border="0"></a>
						</td>
						<td width="10">&nbsp;</td>
						<td>
							<input type="image" src="<%=session("progetto")%>/images/conferma.gif" title="Conferma selezione" id="image1" name="image1">
						</td>
<%					else
%>						<td>
							<a href="JavaScript:self.close();">
							<img src="<%=session("progetto")%>/images/chiudi.gif" border="0"></a>
						</td>
<%					end if
%>				</tr>
			</table>
			<input type="hidden" name="hIdGruppo" value="<%=iIdGruppo%>">
			</form>
<%		else
%>			<br><br><br>
			<span class="tbltext3">
				Non Ci sono corsi disponibili per le scelte effettuate.
			</span>
			<br><br>
			<a href="JavaScript:self.close();">
				<img src="<%=session("progetto")%>/images/chiudi.gif" border="0">
			</a>
<%		end if
		RstCorFac.close
		set RstCorFac=nothing
	else
%>		<br><br>
		<a href="JavaScript:self.close();">
			<img src="<%=session("progetto")%>/images/chiudi.gif" border="0">
		</a>
<%	end if'se idArea<>""
End sub
'-------------------------------------------------------------------------------

'MAIN
%>
<!--#include virtual="/include/OpenConn.asp"-->
<!--#include virtual="/util/dbutil.asp"-->
<center>
<%
	call Inizio
	call ImpostaPag
%>
</center>
<!--#include virtual="/include/CloseConn.asp"-->
</body>
</html>
