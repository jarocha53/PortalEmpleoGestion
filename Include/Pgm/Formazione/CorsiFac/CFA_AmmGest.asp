<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<script LANGUAGE="JavaScript">
function ControllaDati()
{
	if (document.frmGruppo.cmbGruppo.selectedIndex==0)
	{
		alert('Selezionare un gruppo')
		document.frmGruppo.cmbGruppo.focus()
		return false
	}
		
return true	
}
	
function SubmPag(NomePag)
{
	if (ControllaDati())
	{
		document.frmGruppo.action=NomePag;
		document.frmGruppo.submit();
	}
}
//--------------------------------------------------------------------------	
var windowArea
function ApriFinestra(NomePag,idGruppo)
{	
	var IdDescGruppo
	
	if (windowArea!=null)
	{
		windowArea.close();
	}
	if (ControllaDati())
	{
		IdDescGruppo=document.frmGruppo.cmbGruppo.value
		windowArea=window.open(NomePag + '?UIdDescGruppo=' + IdDescGruppo + '&UidGruppo=' + idGruppo,'','toolbar=0,channelmode=0,location=0,menubar=0,status=0,width=550,height=450,Resize=No,Scrollbars=yes,Top=0,Left=0')
	}
}
</script>
<%

'-------------------------------------------------------------------------------
Sub Inizio
	dim sFunzione
	dim sTitolo
	dim sCommento
	dim bCampiObbl
	
	sFunzione = "GESTIONE CORSI FACOLTATIVI"
	sTitolo = "GESTIONE CORSI FACOLTATIVI"
	sCommento = "Selezionare il gruppo al quale associare uno o pi� corsi.<br>" &_
				"Dopo la selezione � possibile aggiungere corsi o eliminare quelli gi� esistenti."
	bCampiObbl = false
	sHelp = "/Pgm/help/Formazione/CorsiFac/CFA_AmmGest/"
%>
	<!--#include virtual="/include/SetTestata.asp"-->
<%
End sub
'-------------------------------------------------------------------------------
Sub ImpostaPag
	dim sSql
	dim sIdDescGruppo,aIdDescGruppo,iIdGruppo,sDescGruppo
	dim EsistonoCorsi
	EsistonoCorsi=false
		
	sIdDescGruppo=Request.Form("cmbGruppo")
	if (sIdDescGruppo<>"") then
		aIdDescGruppo=split(sIdDescGruppo,"|")
		iIdGruppo=clng(aIdDescGruppo(0))
		sDescGruppo=aIdDescGruppo(1)
	end if	
	
	'Response.Write "sIdDescGruppo=" & sIdDescGruppo & "<br>"
	'Response.Write "iIdGruppo=" & iIdGruppo & "<br>"
	'Response.Write "sDescGruppo=" & sDescGruppo & "<br>"
	
	sSql="SELECT IDGRUPPO,DESGRUPPO " &_
		"FROM GRUPPO " &_
		"WHERE COD_RUOFU='DI' " &_
		"ORDER BY DESGRUPPO"

	'Response.Write sSql & "<br>"

'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	set Rst=CC.execute (sSql)
	if not Rst.eof then
		
%>	<br>
	<form name="frmGruppo" id="frmGruppo" method="post">
	<table border=0 width=500>
	<tr>
		<td class="tbltext1BFad" width="20%">Gruppo:&nbsp;
		</td>
		<td width="80%">
			<SELECT id="cmbGruppo" name="cmbGruppo" class="textblackFad" onchange="JavaScript:document.frmGruppo.submit();">
			<OPTION></OPTION>
<%			do while not Rst.eof
%>				<OPTION value="<%=Rst("IDGRUPPO") & "|" & Rst("DESGRUPPO")%>" <%if (cstr(iIdGruppo)=cstr(Rst("IDGRUPPO"))) then Response.Write"selected"%>><%=strHTMLEncode(Rst("DESGRUPPO"))%></OPTION>
<%				Rst.MoveNext
			loop			
%>			</SELECT>
		</td>
	</tr>
	</table>
<%	
	if (sIdDescGruppo<>"") then
		sSql="SELECT C.ID_CORSO,C.TIT_CORSO,C.DESC_CORSO " &_
			"FROM CORSO C,CATCORSIFACOLTATIVI CF " &_
			"WHERE C.ID_CORSO=CF.ID_CORSO AND " &_
			"CF.IDGRUPPO=" & iIdGruppo &_
			" ORDER BY C.TIT_CORSO"
		'Response.Write sSql
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
		set RstCorFac=CC.execute(sSql)
		if not RstCorFac.eof then
			EsistonoCorsi=true
%>			<br>
			<table border="0" cellspacing="1" width="500">
				<tr class="sfondoCommFad">
					<td><b>Titolo</b>
					</td>
					<td><b>Descrizione</b>
					</td>
				</tr>
<%				do while not RstCorFac.eof
					sTitCor=server.HTMLEncode(RstCorFac("TIT_CORSO"))
					sDescCor=server.HTMLEncode(RstCorFac("DESC_CORSO"))
%>					<tr class="tblSfondoFad">
						<td class="tblDettFad"><%=sTitCor%>
						</td>
						<td class="tblDettFad"><%=sDescCor%>
						</td>
					</tr>
<%					RstCorFac.MoveNext
				loop
%>			</table>
<%		else'se non si sono corsi
			EsistonoCorsi=false
%>			<br><br>
			<span class="tbltext3">
				Per il gruppo selezionato non ci sono corsi facoltativi
			</span>
			<br><br>
<%		end if
	
		RstCorFac.close
		set RstCorFac=nothing
	end if

%>	<br>
	<table border=0 width=500>
		<tr>
			<td align="center">
				<a href="JavaScript:ApriFinestra('CFA_AggCorFac.asp','<%=iIdGruppo%>');" class="textred" OnMouseOver="JavaScript:window.status=''; return true;"><b>Aggiungi corso</b></a>
			</td>
<%			if (EsistonoCorsi) then		
%>				<td align="center">
					<a href="JavaScript:ApriFinestra('CFA_ModCorFac.asp','')" class="textred" OnMouseOver="JavaScript:window.status=''; return true;"><b>Elimina corsi</b></a>
				</td>
<%			end if
%>		</tr>
	</table>
	</form>
<%	else%>
		<br><br><br>
		<span class="tbltext3">
			Non vi sono Gruppi.
		</span>
<%	end if
	
	Rst.close
	set Rst=nothing
end sub

'-------------------------------------------------------------------------------------
'M A I N
%>
<!--#include virtual="/strutt_testa2.asp"-->
<!--#include virtual="/include/openconn.asp"-->
<!--#include virtual="/Include/HTMLEncode.asp"-->
<!--#include virtual="/include/EsistenzaCorsi.asp"-->
<center>
<%
call Inizio
sControlla = EsistenzaCorsi
if sControlla = "0" then
	call ImpostaPag()
else%>

    <table border="0" width="500" cellpadding="2" cellspacing="1">
	  <tr align="center"> 
	    <td class=tbltext3>
			<%=sControlla%>
		</td>
	  </tr>
	</table>


<%end if%>

</center>
<!--#include virtual="/include/closeconn.asp"-->
<!--#include virtual="/strutt_coda2.asp"-->
