<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--include virtual ="/Strutt_Testa2.asp"-->
<!--#include virtual ="/util/portallib.asp"-->
<html>
<head>
	<meta NAME="GENERATOR" Content="Microsoft Visual Studio 6.0">
	<link REL="STYLESHEET" TYPE="text/css" HREF="..\..\..\fogliostile.css">
	<title>Dettaglio Domande</title>
	<script LANGUAGE="JavaScript" type="text/javascript">
	</script>
</head>
<% Sub Inizio() %>	
	 
	<table width="520px" border="0" cellspacing="0" cellpadding="0" height="70" align="center">
		<tr>
			<td width="500" background="<%=session("progetto")%>/images/titoli/servizi2g.gif" valign="bottom" align="right">
				<table border="0" background width="500" height="23" cellspacing="0" cellpadding="0">
					<tr>
						<td width="100%" valign="top" align="right"><b CLASS="tbltext1a">Questionario
						  </b></td>
					</tr>
				</table>
			</td>
		</tr>
	</table> 
	<br>
<% End Sub
Sub ImpostaPag()

	set RRW=server.CreateObject("ADODB.Recordset")
'	sqlw="SELECT distinct ID_AREA_DOM, DESC_AREA_DOM, IMG_AREA_DOM, ID_QUESTIONARIO, AUD_AREA_DOM from AREA_DOM where ID_QUESTIONARIO = " & IDQue & " order by ID_AREA_DOM "
	sqlw="SELECT distinct ID_AREA_DOM, DESC_AREA_DOM, IMG_AREA_DOM, ID_QUESTIONARIO from AREA_DOM where ID_QUESTIONARIO = " & IDQue & " order by ID_AREA_DOM "

'PL-SQL * T-SQL  
SQLW = TransformPLSQLToTSQL (SQLW) 
	RRW.open sqlw, CC, 3
	If RRW.EOF then 
		%>
		<table border="0" cellspacing="2" cellpadding="1" width="500px">
			<tr>
				<td align="center">
					<span class="tbltext3"><span class="size">
					<b>Non sono presenti Aree</b>
					</span></span>
				</td>
			</tr>
		</table>
		<%	
   else
		idn = 1
%>
		<table border="0" cellspacing="2" cellpadding="1" width="500px">
		<tr>
			<td align="center">
				<span class="tbltext3"><b>TITOLO : &quot;<%=Titolo%>&quot;</b></span>
			</td>
		</tr>
		</table>
		<br>
		<table border="0" cellspacing="2" cellpadding="1" width="500" align="center">
			<tr align="right">
		    <td height="18"><b><a class="textRed" href="Javascript:history.back()" onmouseover="window.status =' '; return true">Torna indietro</a></b></td>
			</tr>
		</table>

		<%	

		Do While not RRW.Eof
		
			IDAREADOM  = RRW.Fields("ID_AREA_DOM")
			DESCAREADOM  = RRW.Fields("DESC_AREA_DOM")
			IMGAREADOM  = RRW.Fields("IMG_AREA_DOM")
			'AUDAREADOM  = RRW.Fields(4)					
			if cint(IDAREADOM) <> cint(MEMOAREADOM) then

%>				<br>
				<br>
				<table border="0" cellspacing="0" cellpadding="0" width="500px">
					<tr>
				        <!--td align=left width="23"><IMG src="../../../images/angolosin.jpg" border=0></td-->
						<td align="left" class="tbllabel1" width="100%"><b><%=DescAreaDom%></td>
					</tr>
					<%if IMGAREADOM > " " then%>		
						<tr>
						    <!--td align=left class="tblsfondo" width="23">&nbsp</td-->
							<td align="left" class="tbllabel1" width="100%"><img src="../<%=IMGAreaDom%>" border="0"></td>
						</tr>
					<%end if
					if AUDAREADOM > " " then
					%>
					<tr>
						<td align="left" class="tblsfondo" colspan="2"><a href="../<%=AUDAREADOM%>">
							<span class="tbltext1"><span class="size10">
							<img SRC="../audio.gif" border="0">ascolta l'audio</span></a>
						</td>
					</tr>
					<%end if%>
					
				</table>
<%			end if
			set RR=server.CreateObject("ADODB.Recordset")
			sql="SELECT distinct TXT_DOMANDA, NUM_RISPOSTE, ID_DOMANDA, ID_QUESTIONARIO, PESO_DOMANDA, IMG_DOMANDA, AUD_DOMANDA, ID_AREA_DOM from DOMANDA where ID_QUESTIONARIO = " & IDQue & " and ID_AREA_DOM = " & IDAREADOM
			sql=sql & " ORDER BY ID_DOMANDA"
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
			RR.open sql, CC, 3
			If RR.EOF then %>
				<table border="0" cellspacing="2" cellpadding="1" width="500px">
					<tr>
						<td align="center">
							<span class="tbltext3"><span class="size">
							<b>Non sono presenti domande</b>
							</span></span>
						</td>
					</tr>
				</table>
			<% else
				RR.MoveFirst
				nd = 1
				NDOM = NDOM * 1
				Do While not RR.Eof
					IDDM  = RR.Fields(2)
					TITL  = RR.Fields(0)
					NRip  = RR.Fields(1) 
					PESO  = RR.Fields(4)
					IMG   = RR.Fields(5)
					AUDIO = RR.Fields(6)%>
					<table border="0" cellspacing="2" cellpadding="1" width="500px">
						<tr height="23" class="sfondocomm">
							<td align="right" width="22" class="tbltext1">
								<b><%=idn%></b>
							</td>
							<td class="tbltext1">
								 <b><%=TITL%></b>
							</td>
						<%if TipoPagina = SenzaRisposte then

							sSQL =	"SELECT RISPOSTA.FL_ESATTO FROM RISPOSTA, RISULT_RISP " &_
									"WHERE RISPOSTA.ID_DOMANDA = RISULT_RISP.ID_DOMANDA AND " &_
									"RISPOSTA.ID_RISPOSTA = RISULT_RISP.ID_RISPOSTA AND " &_
									"RISULT_RISP.ID_RISULT_QUEST = " & nResQ & " AND " &_
									"RISPOSTA.ID_DOMANDA = " & IDDM
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
							set rsRisp = cc.execute(sSQL)
							if not rsRisp.eof then
								if rsRisp("FL_ESATTO") = "S" then%>
									<td class="tbltext1" width="25" align="center">
										<img src="/images/Formazione/okv.gif" border="0" WIDTH="17" HEIGHT="15">
									</td>						
								<%else%>
										<td class="tbltext1" width="25" align="center">
											<img src="/images/formazione/ko.gif" border="0" WIDTH="17" HEIGHT="15">
										</td>						
								<%end if
							end if
							rsRisp.close
						end if
						%>
						</tr>
					</table>
					<%if IMG > " " then%>
						<table border="0" cellspacing="2" cellpadding="1" width="500px">
							<tr>
								<td align="left" class="sfondocomm">
									<img SRC="../<%=IMG%>">
								</td>
							</tr>
						</table>
					<%end if
					if Audio > " " then	%>
						<table border="0" cellspacing="2" cellpadding="1" width="500px">
							<tr>
								<td align="left" class="tblsfondo">
									<a href="../<%=Audio%>">
										<span class="tbltext1"><span class="size10">
											<img SRC="../audio.gif" border="0">
											ascolta l'audio
										</span>
									</a>
								</td>
							</tr>
						</table>
					<% end if

					if TipoPagina = ConRisposte then
						set RRR=server.CreateObject("ADODB.Recordset")
						sql2="SELECT distinct TXT_RISPOSTA, ID_RISPOSTA, ID_DOMANDA, FL_ESATTO, IMG_RISPOSTA from RISPOSTA where ID_DOMANDA = '" & IDDM & "' order by ID_DOMANDA, ID_RISPOSTA "
'PL-SQL * T-SQL  
SQL2 = TransformPLSQLToTSQL (SQL2) 
						RRR.open sql2, CC, 3
						If RRR.EOF then 
							Response.Write "Non sono presenti risposte" 
						else
							RRR.MoveFirst%>
							<table border="0" cellspacing="2" cellpadding="1" width="500px" bordercolor="#dddddd">
							<%nrr = 0
							Do While not RRR.Eof
								TST  = RRR.Fields(0)
								IDRS = RRR.Fields(1)
								ESAT = RRR.Fields(3)
								IMAG = RRR.Fields(4)
								IDDO = RRR("ID_DOMANDA")%>	
								<tr>
									<td class="tblsfondo" width="22" align="center" height="23">
										<span class="tbltext">
										<%
										sSQL =	"SELECT id_risposta FROM risult_risp where " &_
												"risult_risp.ID_RISULT_QUEST = " & nResQ & " and " &_
												"risult_risp.ID_DOMANDA = " & IDDO & " and " &_
												"risult_risp.ID_RISPOSTA = " & IDRS 
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
										set rsApp = cc.execute(sSQL)
										if not rsApp.eof then
											if ESAT = "S" then
												Response.Write "<img src='/images/Formazione/okv.gif' border=0>"
											else
												sRispOK = "N"
												sSQL = "SELECT TITOLO_ELEMENTO,A.ID_ELEMENTO FROM DOMANDA A,ELEMENTO B "&_
												" WHERE A.ID_DOMANDA = " & IDDO & " AND A.ID_ELEMENTO = B.ID_ELEMENTO "
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
												set rsElemento = CC.Execute(sSQL)
												if not rsElemento.Eof then
													sTitoloLezione	= rsElemento("TITOLO_ELEMENTO")
													nIdElemento		= clng(rsElemento("ID_ELEMENTO"))
												else
													sTitoloLezione	= ""
													nIdElemento		= 0
												end if
												rsElemento.close
												set rsElemento = nothing
												Response.Write "<img src='/images/formazione/ko.gif' border=0>"
											end if
										
										end if
										rsApp.close%>
									</td>
									<td class="tblsfondo" width="548">
										<span class="tbltext">
										<%if IMAG > " " then%>
											<img SRC="../<%=IMAG%>"><br>
										<%end if%>
										    
									    <%if esat = "S" then %>
											<b><font color="#000080">
											<%=TST%></b></font></span>
										<%else%>
											<%=TST%> </span>
										<%end if%>
											
									</td>
								</tr>
								<%RRR.MoveNext 
								nrr = nrr + 1
							Loop
							RRR.CLOSE%>
							<%if sRispOK = "N" and sTitoloLezione <> "" then%>
							<tr>
								<td class="tblsfondo" colspan="2"><span class="tbltext"><b>Modulo di riferimento : </b><%=sTitoloLezione%></span>
								</td>
							</tr>
							<%end if
							sRispOK = ""%>
							
						</table>
						<%End if
					else
						if TipoPagina = ConRisposte1 then
							set RRR=server.CreateObject("ADODB.Recordset")
							sql2="SELECT distinct TXT_RISPOSTA, ID_RISPOSTA, ID_DOMANDA, FL_ESATTO, IMG_RISPOSTA from RISPOSTA where ID_DOMANDA = '" & IDDM & "' order by ID_DOMANDA, ID_RISPOSTA "
'PL-SQL * T-SQL  
SQL2 = TransformPLSQLToTSQL (SQL2) 
							RRR.open sql2, CC, 3
							If RRR.EOF then 
								Response.Write "Non sono presenti risposte" 
							else
								RRR.MoveFirst%>
								<table border="0" cellspacing="2" cellpadding="1" width="500px" bordercolor="#dddddd">
								<%nrr = 0
								Do While not RRR.Eof
									TST  = RRR.Fields("TXT_RISPOSTA")
									IDRS = RRR.Fields("ID_RISPOSTA")
									ESAT = RRR.Fields("FL_ESATTO")
									IMAG = RRR.Fields("IMG_RISPOSTA")
									IDDO = RRR("ID_DOMANDA")%>	
									<tr>
										<td class="tblsfondo" width="22" align="center" height="23">
											<span class="tbltext">
											<%
											sSQL =	"SELECT id_risposta FROM risult_risp where " &_
													"risult_risp.ID_RISULT_QUEST = " & nResQ & " and " &_
													"risult_risp.ID_DOMANDA = " & IDDO & " and " &_
													"risult_risp.ID_RISPOSTA = " & IDRS 
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
											set rsApp = cc.execute(sSQL)
											if not rsApp.eof then
												if ESAT = "S" then
													Response.Write "<img src='/images/Formazione/okv.gif' border=0>"
												else
													sRispOK = "N"
													sSQL = "SELECT TITOLO_ELEMENTO,A.ID_ELEMENTO FROM DOMANDA A,ELEMENTO B "&_
													" WHERE A.ID_DOMANDA = " & IDDO & " AND A.ID_ELEMENTO = B.ID_ELEMENTO "
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
													set rsElemento = CC.Execute(sSQL)
													if not rsElemento.Eof then
														sTitoloLezione	= rsElemento("TITOLO_ELEMENTO")
														nIdElemento		= clng(rsElemento("ID_ELEMENTO"))
													else
														sTitoloLezione	= ""
														nIdElemento		= 0
													end if
													rsElemento.close
													set rsElemento = nothing
													Response.Write "<img src='/images/Formazione/ko.gif' border=0>"
												end if
											end if
											rsApp.close%>
										</td>
										<td class="tblsfondo" width="548">
											<span class="tbltext">
											<%if IMAG > " " then%>
												<img SRC="../<%=IMAG%>"><br>
											<%end if%>
											<%=TST%></span>
										</td>
									</tr>
									<%RRR.MoveNext 
									nrr = nrr + 1
								Loop
								RRR.CLOSE%>
								<%if sRispOK = "N" and sTitoloLezione <> ""  then%>
								<tr>
									<td class="tblsfondo" colspan="2"><span class="tbltext"><b>Guarda il corso : </b><%=sTitoloLezione%></span>
									</td>
								</tr>
								<%end if%>
								</table>
								<%sRispOK = ""
							End if
						End if
					End if
					nd = nd + 1
					idn = idn + 1
					nr = nr + nrr 
					RR.MoveNext 
				Loop
				RR.CLOSE
			end if
			MEMOAREADOM = IDAREADOM
			RRW.MoveNext 
		Loop
		RRW.CLOSE
	end if
End Sub
%>
<!--#include virtual="include/openconn.asp"-->
<% 

Dim sqlQ, NDom, codArea, Titolo, IDQue, nd, nr
Dim sSQL, nResQ, rsApp
Dim TipoPagina, nApp
Const ConRisposte = 0
Const SenzaRisposte = 1
Const ConRisposte1 = 2

on error goto 0

nd = 0
nr = 0

'nResQ = Request.QueryString("ResQ")
	
'nApp = Request.QueryString("RisTest")
TipoPagina = SenzaRisposte
if (isnumeric(nApp)) then
	'if (nApp >= 0) and (nApp <= 1) then
	if (nApp >= 0) and (nApp <= 2) then
		TipoPagina = cint(Request.QueryString("RisTest"))
	end if
end if
'-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
'Modifica del 04/04/2002
'Marco Attenni
sSQL = "Select q.id_questionario from questionario q, risult_quest r where q.id_questionario = r.id_questionario and r.id_risult_quest = " & nResQ
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
set rsApp = cc.execute(sSQL)
if not rsApp.eof then
	IDQue = rsApp("id_questionario")
end if
rsApp.close
set rsApp = nothing
'-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+	
Inizio()
sqlQ = "Select * From QUESTIONARIO where ID_QUESTIONARIO =" & IDque
set RRQ = server.CreateObject("ADODB.Recordset")
'PL-SQL * T-SQL  
SQLQ = TransformPLSQLToTSQL (SQLQ) 
RRQ.open sqlQ, CC, 3		 
If not RRQ.EOF then		
	NDom = cint(RRQ.Fields("Num_Dom"))
	CodArea = RRQ.Fields("ID_AREA")
	Titolo = RRQ.Fields("Tit_Questionario")
end if	

RRQ.Close
set RRQ = nothing%>



<%ImpostaPag()%>


<!--include virtual ="/Strutt_Coda2.asp"-->
