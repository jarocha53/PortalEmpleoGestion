<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%
	Sub Inizio()
	dim sFunzione
	dim sTitolo
	dim sCommento
	dim bCampiObbl
	
	sFunzione = "GESTIONE QUESTIONARIO"
	sTitolo = "QUESTIONARIO"
	sCommento = "Cancellazione del<br>Questionario"
	bCampiObbl = false
%>	
	<!--#include virtual="include/setTestata.asp"-->
<%
	End Sub
'--------------------------------------------------------------------------------------
	Sub Cancella()
		dim sErrore, sSQL
		dim nIdQuest, dDtTmst
		dim rsCAree
		
		nIdQuest = Request.QueryString("id")
		dDtTmst = Request.QueryString("tmst")
		'Response.Write nIdQuest & "***" & dDtTmst 
		
		'Controllo che non esistano Aree associate al questionario
		sSQL =  " select count(*) as CAree" &_
				" from area_dom" &_
				" where id_questionario = " & nIdQuest
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
		set rsCAree = cc.execute(sSQL)
		if cint(rsCAree("CAree")) > 0 then
			%>
				<br><br>
				<table border=0 cellspacing=2 cellpadding=1 width="500">
					<tr align=middle>
						<td class="tbltext3">
							Non � possibile cancellare il Questionario.<br>
							Cancellare prima le Aree associate.
						</td>
					</tr>
				</table>		
				<br><br>
			<%
		else
			sSQL = "delete from questionario where id_questionario = " & nIdQuest
			sErrore=Esegui(idQuest ,"questionario",Session("persona"),"DEL",sSQL,1,dDtTmst)
			
			IF sErrore <> "0" then
			%>
				<br><br>
				<table border=0 cellspacing=2 cellpadding=1 width="500">
					<tr align=middle>
						<td class="tbltext3">
							Errore nella cancellazione. <%=sErrore%>
						</td>
					</tr>
				</table>		
				<br><br>
			<% else %>
				<br><br>
				<table border=0 cellspacing=2 cellpadding=1 width="500">
				<tr align=middle>
						<td class=tbltext3>
							Cancellazione correttamente effettuata
						</td>
					</tr>
				</table>
				<br><br>
				<%
			End If
		end if
		%>

		<table border=0 cellspacing=2 cellpadding=1 width="500">
			<tr align=center>
				<td>
					<%
						PlsLinkRosso "QUE_VisQuestionari.asp","Elenco Questionari"
		%>
				</td>
				</tr>
			</table>
		<br><br>
		<%
	End Sub
'---------------------------------------------------------------------------
'M A I N
%>
<!--#include virtual="/strutt_testa2.asp"-->
<!--#include virtual="/include/openconn.asp"-->
<!--#include virtual="/Util/DBUtil.asp"-->
<!--#include virtual="/Include/SysFunction.asp"-->
<!--#include virtual="/Include/SetPulsanti.asp"-->
<%
'	Inizio()
	Cancella()
%>			
<!--#include virtual="/include/closeconn.asp"-->
<!--#include virtual="/strutt_coda2.asp"-->
