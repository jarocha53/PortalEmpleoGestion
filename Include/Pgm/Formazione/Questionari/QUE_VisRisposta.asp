<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%
Sub Inizio()

	dim sFunzione
	dim sTitolo
	dim sCommento
	dim bCampiObbl
	
	sFunzione = "GESTIONE QUESTIONARIO"
	sTitolo = "QUESTIONARIO"
	'sCommento = "Elenco delle<br>risposte"
	sCommento = "Elenco risposte<br>Cliccando sul titolo � possibile modificare le caratteristiche della risposta. L'icona della colonna Immagine permette di visualizzare un'anteprima dell'immagine. Nella colonna Vera/Falsa viene visualizzato il valore della risposta."
	bCampiObbl = false
	sHelp = "/Pgm/help/Formazione/Questionari/QUE_VisRisposta/"	
	
%>
	<!--#include virtual="include/setTestata.asp"-->
<%
End Sub
'------------------------------------------------------------------------
Sub ElencoR

dim sSQL, nIdDom
dim rsDomanda, rsRisposta
dim OpSQL

IdQue = request("idq")
IdDom = request("idd")
IdArea = request("ida")

set RR=server.CreateObject("ADODB.Recordset")
sql="SELECT NUM_DOM, TIT_QUESTIONARIO from QUESTIONARIO where ID_Questionario = " & IDQUE
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
RR.open sql, CC, 3
TIT = RR.FIELDS("TIT_QUESTIONARIO")
RR.CLOSE
		
set RR=server.CreateObject("ADODB.Recordset")
sql="SELECT DESC_AREA_DOM, IMG_AREA_DOM from AREA_DOM where ID_AREA_DOM = " & IDAREA

'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
RR.open sql, CC, 3
TITAR = RR.FIELDS("DESC_AREA_DOM")
IMGAR = RR.FIELDS("IMG_AREA_DOM")
RR.CLOSE
%>
<table width="500" border="0">
	<tr><td class="tbltext3">Questionario: &quot;<%=TIT%>&quot; </td></tr>
</table>

<table width="500" border="0">
	<%
	if TITAR > " " then
	%>
	<tr><td class="tbltext3">Testo Area: &quot;<%=TITAR%>&quot; </td></tr>
	<%
	end if
	if IMGAR > " " then%>
	<tr><td class="tbltext3">Immagine Area:<br><img SRC="<%=IMGar%>"></td></tr>
	<%
	end if
	%>
</table>

<%		
sSQL = "Select Txt_Domanda, IMG_DOMANDA, NUM_RISPOSTE, AUD_DOMANDA from domanda where id_domanda = " & IdDom
'Response.Write sSQL
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
set rsDomanda = cc.execute(sSQL)
if not rsDomanda.eof then
ndoma = rsDomanda("NUM_RISPOSTE")
%>
<table width="500" border="0">
	<%
	if rsDomanda("txt_domanda") > " " then
	%>
	<tr><td class="tbltext3">Testo Domanda: &quot;<%=rsDomanda("txt_domanda")%>&quot;</td></tr>
	<%
	end if
	if rsDomanda("IMG_domanda") > " " then
	%>
	<tr><td class="tbltext3">Immagine Domanda:<br><img SRC="<%=rsDomanda("img_domanda")%>"></td></tr>
	<%
	end if
	if rsDomanda("AUD_domanda") > " " then
	%>
	<tr><td class="tbltext3">Audio Domanda: &quot;<a href="<%=rsDomanda("aud_domanda")%>">&quot;<img SRC="audio.gif"></a></td></tr>
	<%
	end if
	%>
</table>
<br>
<%	
end if
rsDomanda.close

sSQL = "Select ID_RISPOSTA, ID_DOMANDA, TXT_RISPOSTA, FL_ESATTO, IMG_RISPOSTA from risposta where id_domanda = " & IdDom
'Response.Write "<br>" & sSQL
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
set rsRisposta = CC.execute(sSQL)
%>

<table border="0" cellspacing="2" cellpadding="1" width="500">
	<tr class="sfondocomm">
	    <td width="20" align="center">#</td>
	    <td>Descrizione</td>
	    <td width="60" align="center">Immagine</td>
	    <td width="60" align="center">Vera/Falsa</td>
	</tr>
<%
c = 1
do while not rsRisposta.eof
'Response.Write rsRisposta("id_risposta")

OpSQL = "Aggiorna"
%>
	<tr class="tblsfondo">
		<td class="tbldett"><%=c%></td>
		<td class="tbldett">
			<a href="QUE_ModRisposta.asp?idr=<%=rsRisposta("ID_RISPOSTA")%>&amp;idq=<%=IdQue%>&amp;ida=<%=IdArea%>&amp;idd=<%=IdDom%>" class="tblagg"><%=rsRisposta("TXT_RISPOSTA")%></a></td>
		<td align="center"> 
		<% if rsRisposta("IMG_RISPOSTA") > " " then %>
			<a href="<%=rsRisposta("IMG_RISPOSTA")%>" target="_blank">
				<img src="<%=Session("progetto")%>/images/icons2/gif.gif" border="0" alt="Anteprima Immagine">
			</a>
		<% end if %> 
		</td>
		<td align="center">
			<%if rsRisposta("FL_ESATTO") = "S" then%>
				<img src="/images/Formazione/okv.gif" border="0" alt="Risposta Esatta">
			<%else%>
				<img src="/images/Formazione/ko.gif" border="0" alt="Risposta Errata" WIDTH="17" HEIGHT="15">
			<%end if%>
		</td>
	</tr>	

<%
	rsRisposta.movenext
	c = c + 1 
loop
rsRisposta.close
%>
</table>
<br>
<table width="500">
	<tr>
<%
if cint(ndoma) > cint(c-1) then 
%> 
		<td align="center">
		<%
			PlsLinkRosso "QUE_InsRisposta.asp?idq=" & IdQue & "&ida=" & IdArea & "&idd=" & IdDom, "Inserisci Risposta mancante"
		%>
		</td>
<% 
end if 
%>		
		<td align="center">
		<%
			PlsLinkRosso "QUE_VisDomanda.asp?idq=" & IdQue & "&ida=" &IdArea, "Elenco delle Domande"
		%>
		</td>
	</tr>
</table>
<br>
<%
End Sub
'---------------------------------------------------------------------------
'M A I N
%>
<!--#include virtual="/strutt_testa2.asp"-->
<!--#include virtual="/include/openconn.asp"-->
<!--#include virtual="/include/SetPulsanti.asp"-->
<%	
	Inizio()
	ElencoR()
%>
<!--#include virtual="/include/closeconn.asp"-->
<!--#include virtual="/strutt_coda2.asp"-->
