<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include Virtual="/strutt_testa2.asp"-->
<!--#include virtual ="/util/portallib.asp"-->
<!--#include virtual="include/SetTestataFAD.asp"-->
<%
' Session.Timeout = 120
' Response.ExpiresAbsolute = Now() - 1 
' Response.AddHeader "pragma","no-cache"
' Response.AddHeader "cache-control","private"
' Response.CacheControl = "no-cache"
%>

	<script LANGUAGE="JavaScript" type="text/javascript"><!--
		
		function Validate_Pwd(theForm)
		{
			if (theForm.txtpwd.value== "" )
				{ alert("Digitare la password");
					return(false);
				}
			return(true);
		}
		
		function chk()
		{
		var idn
		var nr
		idn = document.frmNumNr.idn.value		
		nr = document.frmNumNr.nr.value

		var k = 0 ;
		var tru = 0;

		nr = document.form1.length
		

		for (k = 15; k < nr-2 ; ++k)
			{	
				if (document.form1.elements[k+2].type == "radio") {
					if (document.form1.elements[k+2].checked == true)
					{	
						tru = tru + 1;
					}
				}
			}	 
			if ((tru+1) < (idn-1)) 
			{
				alert("Inserire tutte le risposte richieste");
			}
			else
			{
				document.form1.submit();
			}
			return false
		}  
		
		function chkOLDOLD(idn,nr)
		{
		var k;
		var tru = 0;
		for (k=7; k<=nr+1; ++k)
			{	
				if (document.forms[0].elements[k+2].checked == true)
				{	
					tru = tru + 1;
				}
			}	 
			if (tru < (idn-1)) 
			{
				alert("Inserire tutte le risposte richieste");
			}
			else
			{
				document.form1.submit();
			}
		return 1;
		}  

		//-->
	</script>
<%
Sub Inizio()
	dim sTitolo
	dim sCommento
	
	sTitolo = "Questionario"
	sCommento = "Esegui il questionario rispondendo a tutte le domande."
	sHelp = "/pgm/help/formazione/questionari/QUE_Questionario/"
	call TestataFAD(sTitolo, sCommento, false, sHelp)
	
End Sub
%>	
<!--------------------------------------------------------------------------------------------------------------------------------------------------------->
<%
	Sub Percorso()
		Dim sqlP
	
		sErrore = "0"
		sqlV = "Select * from PERCORSO "
		sqlV = sqlV & " Where ID_PERSONA =" & IDP &_
			 " And ID_ELEMENTO =" & nElem & " And ID_CORSO=" & nIDCorso 
		
'PL-SQL * T-SQL  
SQLV = TransformPLSQLToTSQL (SQLV) 
		set RRV = CC.Execute (sqlV)
	
		if RRV.EOF then
			sqlP = "INSERT INTO PERCORSO (ID_PERSONA,ID_ELEMENTO,ID_CORSO,dt_inizio, dt_tmst)"
			sqlP = sqlP & " VALUES (" & IDP & "," & nElem &","& nIDCorso &"," & ConvDateToDB(now()) & "," & ConvDateToDB(now()) & ")"	
			sErrore=Esegui(nElem,"PERCORSO", Session("IdPersona"),"INS",sqlP,1,"")
		else
			sqlP = "Update PERCORSO  set dt_inizio =" & ConvDateToDB(now()) & ", dt_tmst=" & ConvDateToDB(now())   
			sqlP = sqlP & " Where ID_PERSONA =" & IDP & " And ID_ELEMENTO =" & nElem & " And ID_CORSO=" & nIDCorso 	
'PL-SQL * T-SQL  
SQLP = TransformPLSQLToTSQL (SQLP) 
			CC.Execute sqlP
		end if		


		
		RRV.Close
		set RRV = nothing
		
		IF sErrore <> "0" then
			%>
			<br><br><table border="0" cellspacing="2" cellpadding="1" width="500" ALIGN="CENTER">
				<tr align="center">
					<td class="tbltext3"><span class="size"><b>Errore. cod = <%=sErrore%></b></span></td>
				<tr>
			</table><br><br>
			<%
			bErrore = True
		End If	 
	End Sub
%>	
<!--------------------------------------------------------------------------------------------------------------------------------------------------------->
<% 
	Sub Controllo()

		set RRS=Server.CreateObject("ADODB.Recordset")
	   	sqlS="SELECT count(*) as rc from RISULT_QUEST where ID_QUESTIONARIO = " & IDQue & " and ID_PERSONA = " & IDP
	
'PL-SQL * T-SQL  
SQLS = TransformPLSQLToTSQL (SQLS) 
		RRS.open sqlS, CC, 3
		If not RRS.EOF then
			rc = RRS.Fields("rc")
			If cint(rc) = 0 then
				Presente = "N"
			else
				Presente = "S"
			end if			
		end if
		RRS.CLOSE


		set RRS=Server.CreateObject("ADODB.Recordset")
	   	sqlS="SELECT distinct ID_QUESTIONARIO, ID_PERSONA, FL_ESEGUITO, NUM_ACCESSI, DT_ESEC, DT_TMST from RISULT_QUEST where ID_QUESTIONARIO = " & IDQue & " and ID_PERSONA = " & IDP
'		Response.Write sqlS
'PL-SQL * T-SQL  
SQLS = TransformPLSQLToTSQL (SQLS) 
		RRS.open sqlS, CC, 3
		If not RRS.EOF then
			Eseguito = RRS.Fields("FL_ESEGUITO")
			NAccessi = RRS.Fields("NUM_ACCESSI")
			DataEs = RRS.Fields("DT_ESEC")
			DtTmst = RRS.Fields("DT_TMST")
	
			DataEs = ConvDateToString(DataEs)
		else
			Eseguito = "N"
			DtTmst = now()
		end if
		RRS.CLOSE
'		Response.Write "ESEGUITO=" & Eseguito & "<br>"
		If ( Presente = "S" and ( Eseguito = "N" or bRipetibile = 1) )then
'		Response.Write "P = S e E = N" 
			NAccessi = cint(NAccessi) + 1
		
			sqlU="UPDATE RISULT_QUEST SET DT_ESEC = " & ora & " ,  NUM_ACCESSI = " & NAccessi & " where ID_Questionario=" & IDQue & " and ID_PERSONA = " & IDP
			sErrore=Esegui(IDQUE ,"RISULT_QUEST",IDP,"MOD",SQLU,1,dtTmst)

			IF sErrore <> "0" then
			%>
			<br><br><table border="0" cellspacing="2" cellpadding="1" width="500" ALIGN="CENTER">
				<tr align="center">
					<td class="tbltext3"><span class="size"><b>Errore nell'Update. cod = <%=sErrore%></b></span></td>
				<tr>
			</table><br><br>
			<%
			End If	
		End If

		If Presente = "S" and Eseguito = "S" and bRipetibile = 0 then

			VIS = "NO"
		%>
			<br><br><table border="0" cellspacing="2" cellpadding="1" width="500" align="center">
				<tr align="center">
					
    <td class="tbltext3"><span class="size"><b>Questionario gi� eseguito 
      il giorno <%=DataEs%></b></span></td>
				</tr>
			</table><br><br>

		<table border="0" cellspacing="2" cellpadding="1" width="500" ALIGN="CENTER">
				
		<form name="form2" action="/pgm/formazione/catalogo/ERO_VisArgomenti.asp" method="POST">
			<tr align="center">
			<input type="hidden" name="CodCorso" value="<%=sCodCorso%>">
			<input type="hidden" name="IDCorso" value="<%=nIdCorso%>">
			<input type="hidden" name="IDArgom" value="<%=nIdArgom%>">
			<input type="hidden" name="modo" value="=1">
			<input type="hidden" name="CodElem" value="<%=nElem%>">
				<td height="18"><b><a class="textRed" href="Javascript:history.back()" onmouseover="window.status =' '; return true"><img border="0" src="<%=session("progetto")%>/images/indietro.gif"></a>
						</b>
				</td>
			</tr>
		</table>
		</form>

		<%
		End If

		If Presente = "N" then
			oggi = ConvDateToDB(date())
			ora = ConvDateToDB(now())
			NAccessi = cint(NAccessi) + 1
			sqlI="INSERT INTO RISULT_QUEST (ID_QUESTIONARIO, ID_PERSONA, DT_ESEC, NUM_ACCESSI, FL_ESEGUITO, NUM_TENTATIVI,DT_TMST)"
			sqlI = sqlI & " VALUES (" & IDQUE & "," & IDP & "," & ora & "," & NAccessi & ",'N',0," & ORA & ")"
		
			sErrore=Esegui("RQUE","RISULT_QUEST",IDP,"INS",SQLI,1,"")

			IF sErrore <> "0" then
			%>
			<br><br><table border="0" cellspacing="2" cellpadding="1" width="500" ALIGN="CENTER">
				<tr align="center">
					<td class="tbltext3"><b>Errore nella Insert. cod = <%=sErrore%></b></span></td>
				<tr>
			</table><br><br>
			<%
			End If	
		end if
		
	End Sub
%>
<!--------------------------------------------------------------------------------------------------------------------------------------------------------->
<%
	Sub ImpostaPag()

	set RRW=server.CreateObject("ADODB.Recordset")
	sqlw="SELECT distinct ID_AREA_DOM, DESC_AREA_DOM, IMG_AREA_DOM, ID_QUESTIONARIO from AREA_DOM where ID_QUESTIONARIO = " & IDQue & " order by ID_AREA_DOM "
'PL-SQL * T-SQL  
SQLW = TransformPLSQLToTSQL (SQLW) 
	RRW.open sqlw, CC, 3
	If RRW.EOF then 
		%>
		<table border="0" cellspacing="2" cellpadding="1" width="500" ALIGN="CENTER">
		<tr>
			<td align="center">
				<span class="tbltext3">
				<b>Non sono presenti Aree</b>
				</span></span>
			</td>
		</tr>
		</table>
		<%	
   else
		idn = 1
%>
		<table border="0" cellspacing="2" cellpadding="1" width="500" ALIGN="CENTER">
		<tr>
			<td align="center">
				<span class="tbltext3"><b><%=Titolo%></b></span>
			</td>
		</tr>
		</table>
		<!--table border="0" cellspacing="2" cellpadding="1" width="500" ALIGN="CENTER">						<form name="form2" action="/pgm/formazione/catalogo/ERO_VisArgomenti.asp" method="POST">			<tr align="right">				<input type="hidden" name="CodCorso" value="<%=sCodCorso%>">				<input type="hidden" name="IDCorso" value="<%=nIdCorso%>">				<input type="hidden" name="IDArgom" value="<%=nIdArgom%>">				<input type="hidden" name="modo" value="=1">				<input type="hidden" name="CodElem" value="<%=nElem%>">				<td height="18"><b>				<!--a class="textRed" 				href="javascript:history.back()" 				onmouseover="window.status =' '; 				return true">Torna indietro</a-->
						<!--/b>				</td>			</tr>		</table>		</form-->
		<form action="/pgm/formazione/questionari/que_InvRisposte.asp" method="POST" onsubmit="return chk();" id="form1" name="form1">

		<input type="hidden" name="modo" value="1">
		<input type="hidden" name="Qmodo" value="<%=QModo%>">
		<input type="hidden" name="IDQUE" value="<%=IDQUE%>">
		<input type="hidden" name="NDOM" value="<%=NDOM%>">
		<input type="hidden" name="CODAREA" value="<%=CODAREA%>">
		<!--input type="hidden" name="CODAREA" value="<%'=SV%>"-->
		<input type="hidden" name="RQUEST" value="<%=CODAREA%>">


		<input type="hidden" name="Titolo" value="<%=Titolo%>">
		<input type="hidden" name="dtTmst" value="<%=dtTmst%>">
		<input type="hidden" name="TimeIni" value="<%=TimeIni%>">
		<input type="hidden" name="Area" value="<%=Area%>">
		<input type="hidden" name="Elem" value="<%=nElem%>">
		<input type="hidden" name="IdCorso" value="<%=nIdCorso%>">
		<input type="hidden" name="dt_tmst" value="<%=dt_Tmst%>">
		<input type="hidden" name="IdArgom" value="<%=nIdArgom%>">
		<input type="hidden" name="CodCorso" value="<%=sCodCorso%>">
		<%	

		Do While not RRW.Eof
		
		IDAREADOM  = RRW.Fields("ID_AREA_DOM")
		DESCAREADOM  = RRW.Fields("DESC_AREA_DOM")
		IMGAREADOM  = RRW.Fields("IMG_AREA_DOM")
		if cint(IDAREADOM) <> cint(MEMOAREADOM) then
				
%>			<br>
			<table border="0" cellspacing="0" cellpadding="0" width="500" ALIGN="CENTER">
				<tr class="SfondoCommFad">
				    <td height="23" align="left" width="2%">&nbsp;</td>
					<td align="left" width="95%"><b><%=DescAreaDom%></td>
				</tr>
				<%
					if IMGAREADOM > " " then
				%>		
				<tr>
				    <!--td align="left" width="2%">&nbsp;</td-->
					<td align="left" colspan="2"><img src="/pgm/formazione/questionari/<%=IMGAreaDom%>" border="0"></td>
				</tr>
			<%
				end if
			%>		
		</table>
			<!--br-->	
<%		end if
			set RR=server.CreateObject("ADODB.Recordset")
			sql="SELECT distinct TXT_DOMANDA, NUM_RISPOSTE, ID_DOMANDA, ID_QUESTIONARIO, PESO_DOMANDA, IMG_DOMANDA, AUD_DOMANDA, ID_AREA_DOM from DOMANDA where ID_QUESTIONARIO = " & IDQue & " and ID_AREA_DOM = " & IDAREADOM
			sql=sql & " ORDER BY ID_DOMANDA"
'			Response.Write sql
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
			RR.open sql, CC, 3
			If RR.EOF then 
				%>
				<table border="0" cellspacing="2" cellpadding="1" width="500" ALIGN="CENTER">
				<tr>
					<td align="center">
						<span class="tbltext3"><span class="size">
						<b>Non sono presenti domande</b>
						</span></span>
					</td>
				</tr>
				</table>
				<%	
			else
				RR.MoveFirst
				nd = 1
				NDOM = NDOM * 1

				Do While not RR.Eof
					IDDM  = RR.Fields("ID_DOMANDA")
					TITL  = RR.Fields("TXT_DOMANDA")
					NRip  = RR.Fields("NUM_RISPOSTE") 
					PESO  = RR.Fields("PESO_DOMANDA")
					IMG   = RR.Fields("IMG_DOMANDA")
					AUDIO = RR.Fields("AUD_DOMANDA")
							
					%>
				
					<table border="0" cellspacing="2" cellpadding="1" width="500" ALIGN="CENTER">
					<tr>
						<td align="left">
							<b class="tblText1Fad"><%=idn%>) <%=TITL%><b>
						</td>
					</tr>
					<%
					if IMG > " " then
					%>
					<tr>
						<td align="left">
							<img SRC="<%=IMG%>">
					</td>
					</tr>
					<%
					end if
					%>
					<%
					if Audio > " " then
					%>
					<tr>
						<td align="left"><a href="<%=Audio%>">
							<span class="tblText1Fad"><span class="size10">
							<img SRC="../audio.gif" border="0">ascolta l'audio</span></a>
					</td>
					</tr>
					<%
					end if
					%>
	
					</table>
					<%

					set RRR=server.CreateObject("ADODB.Recordset")
					sql2="SELECT distinct TXT_RISPOSTA, ID_RISPOSTA, ID_DOMANDA, FL_ESATTO, IMG_RISPOSTA from RISPOSTA where ID_DOMANDA = '" & IDDM & "' order by ID_DOMANDA, ID_RISPOSTA "
			        'Response.Write sql2
'PL-SQL * T-SQL  
SQL2 = TransformPLSQLToTSQL (SQL2) 
					RRR.open sql2, CC, 3
					If RRR.EOF then 
						Response.Write "Non sono presenti risposte" 
					else
						RRR.MoveFirst
								
						%>
						<table border="0" cellspacing="2" cellpadding="1" width="500" ALIGN="CENTER" bordercolor="#dddddd">
						<%
						nrr = 0
						Do While not RRR.Eof
							TST  = RRR.Fields("TXT_RISPOSTA")
							IDRS = RRR.Fields("ID_RISPOSTA")
							ESAT = RRR.Fields("FL_ESATTO")
							IMAG = RRR.Fields("IMG_RISPOSTA")

							%>	
							<tr>
								<td width="22" align="center">
									<span>
									<input type="radio" id="DOM<%=idn%>" name="DOM<%=idn%>" value="D<%=IDDM%>R<%=IDRS%>E<%=ESAT%>P<%=PESO%>"></span>
									</td>
									<td width="548">
										<b class="textblackFad">
									<%
										if IMAG > " " then
									%>
										<img SRC="<%=IMAG%>"><br>
									<%
										end if
									%>
									
										<%=TST%></b>
									</td>
							</tr>
							<%
							RRR.MoveNext 
							nrr = nrr + 1
							Loop
						RRR.CLOSE
						%>	
						</table>
					<%
					End if
					%><%
				nd = nd + 1
				idn = idn + 1
				nr = nr + nrr 
				RR.MoveNext 
					Loop
				RR.CLOSE
			end if

	MEMOAREADOM = IDAREADOM
	RRW.MoveNext 
    Loop
	RRW.CLOSE
    %> 
    <table border="0" cellspacing="2" cellpadding="1" width="500" ALIGN="CENTER">
			<tr>
				<td width="50%" align="right">
				<a class="textRed" href="javascript:history.back()" onmouseover="window.status =' '; 
				return true"><img border="0" src="<%=Session("progetto")%>/images/indietro.gif"></a>
				</td>
				<td width="50%" align="left">
					<input type="image" src="<%=Session("progetto")%>/images/conferma.gif" value="INVIA" id="INVIA" name="INVIA">
				</td>
			</tr>
		</table>
	</form>
		<form name="frmNumNr">
		<input type="hidden" name="nr" value="<%= nr + 5%>">
		<input type="hidden" name="idn" value="<%= idn%>">
		
		</form>
	
   <%
	end if
  End Sub
%>
<!--------------------------------------------------------------------------------------------------------------------------------------------------------->
<!--	FINE BLOCCO ASP		-->

<!--		MAIN			-->
	<!--#include Virtual ="/include/ControlDateVB.asp"-->
	<!--#include Virtual ="/include/openconn.asp"-->
	<!--#include Virtual ="/Util/DBUtil.asp"-->
	<!--#include Virtual ="/include/sysfunction.asp"-->
	<!--#include Virtual ="/include/ControlDateVB.asp"-->

<% 
'	if ValidateService(Session("idpersona"),"FAD_FRU_COR",cc) <> "true" then 
'		response.redirect "../../util/error_login.asp"
'	end if

 	Dim sql, Area, Titolo, IDQue, SRif, n, man, CONTA , bErrore ,nElem,nIdCorso
	dim DtTmst,IDP,bRipetibile
 	
 	bErrore = false
	if Request.form("QModo") = 1 then
		nElem = Request.form("Elem")
		nIDCorso = Request.form("IdCorso")
		dt_Tmst = Request.form("dtTmst")
		nIdArgom = Request.form("IdArgom")
		sCodCorso = Request.form("CodCorso")
	end if
	
	IDQue = Request.Form("IDQue")
	
	if IDQue = "" then
		IDQue = Request.Form("IDQue")	
	end if
	
	MODO = Request.form("modo")
	QMODO = Request.form("Qmodo")	

	VIS = "SI"
	TimeIni = now()
	OGGI = ConvDateToDB(date())
	ora = ConvDateToDBs(Now())
	nd = 0
	nr = 0
'	IDP = Session("creator")
	IDP = Session("IdUtente")
	
	Inizio()

	sqlQa = "Select count(*) from domanda  where ID_QUESTIONARIO =" & IDQue
	
	set RRQa = server.CreateObject("ADODB.Recordset")
'PL-SQL * T-SQL  
SQLQA = TransformPLSQLToTSQL (SQLQA) 
	RRQa.open sqlQa, CC, 3		 
	
	If not RRQa.EOF then		
		NDom = cint(RRQa.Fields(0))
	end if	

	sqlQ = "Select ID_AREA, Tit_Questionario,FL_REPEAT From QUESTIONARIO where ID_QUESTIONARIO =" & IDque
	set RRQ = server.CreateObject("ADODB.Recordset")
'PL-SQL * T-SQL  
SQLQ = TransformPLSQLToTSQL (SQLQ) 
	RRQ.open sqlQ, CC, 3		 
	
	If not RRQ.EOF then		
		CodArea = RRQ.Fields("ID_AREA")
		Titolo = RRQ.Fields("Tit_Questionario")
		bRipetibile = cint(RRQ.Fields("FL_REPEAT"))
	else 
		bErrore = true
	end if	

	RRQ.Close
	set RRQ = nothing
	
'	if QModo = 1 then
'		Percorso()
'	end if
		
	if not bErrore then
		Controllo()
		If VIS = "SI" then
			ImpostaPag()
		End if
	end if
%>
<!--#include Virtual ="/include/closeconn.asp"-->
<!--#include Virtual="/strutt_coda2.asp"-->
<!--	FINE BLOCCO MAIN	-->
