<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%
	Sub Inizio()
	dim sFunzione
	dim sTitolo
	dim sCommento
	dim bCampiObbl
	
	sFunzione = "GESTIONE QUESTIONARIO"
	sTitolo = "QUESTIONARIO"
	sCommento = "Cancellazione dell'Area"
	bCampiObbl = false
%>	
	<!--#include virtual="include/SetTestata.asp"-->
<%
	End Sub
'--------------------------------------------------------------------------------------
	Sub Cancella()
		dim sErrore, sSQL
		dim nIdArea, nIdQuest, dDtTmst
		dim rsCDom
		
		nIdArea = Request.QueryString("ida")
		nIdQuest = Request.QueryString("idq")
		dDtTmst = Request.QueryString("tmst")
		'Response.Write nIdArea & "***" & nIdQuest & "***" & dDtTmst 
		
		'Controllo che non esistano Domande associate al questionario
		sSQL =  " select count(*) as CDom" &_
				" from domanda" &_
				" where id_area_dom = " & nIdArea
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
		set rsCDom = cc.execute(sSQL)
		if cint(rsCDom("CDom")) > 0 then
			%>
				<br><br>
				<table border=0 cellspacing=2 cellpadding=1 width="500">
					<tr align=middle>
						<td class="tbltext3">
							Non � possibile cancellare l'Area.<br>
							Cancellare prima le Domande associate.
						</td>
					</tr>
				</table>		
				<br><br>
			<%
		else
			sSQL = "delete from area_dom where id_area_dom = " & nIdArea
			sErrore=Esegui(nIdArea ,"area",Session("persona"),"DEL",sSQL,1,dDtTmst)
			IF sErrore <> "0" then
			%>
				<br><br>
				<table border=0 cellspacing=2 cellpadding=1 width="500">
					<tr align=middle>
						<td class="tbltext3">
							Errore nella cancellazione. <%=sErrore%>
						</td>
					</tr>
				</table>		
				<br><br>
			<% else %>
				<br><br>
				<table border=0 cellspacing=2 cellpadding=1 width="500">
				<tr align=middle>
						<td class=tbltext3>
							Cancellazione correttamente effettuata
						</td>
					</tr>
				</table>
				<br><br>
				<%
			End If
		end if
		rsCDom.close
		set rsCDom = nothing
		%>
		<table border=0 cellspacing=2 cellpadding=1 width="500">
			<tr align=center>
				<td>
					<%
						PlsLinkRosso "QUE_VisArea.asp?id=" & nIdQuest,"Elenco delle Aree"
					%>
				</td>
				</tr>
			</table>
		<br><br>
		<%
	End Sub
'---------------------------------------------------------------------------
'M A I N
%>
<!--#include virtual="/strutt_testa2.asp"-->
<!--#include virtual="/include/openconn.asp"-->
<!--#include virtual="/Util/DBUtil.asp"-->
<!--#include virtual="/Include/SysFunction.asp"-->
<!--#include virtual="/Include/SetPulsanti.asp"-->
<%
	Inizio()
	Cancella()
%>			
<!--#include virtual="/include/closeconn.asp"-->
<!--#include virtual="/strutt_coda2.asp"-->
