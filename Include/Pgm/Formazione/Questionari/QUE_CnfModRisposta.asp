<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%
Sub Inizio()
	dim sFunzione
	dim sTitolo
	dim sCommento
	dim bCampiObbl
	
	sFunzione = "GESTIONE QUESTIONARIO"
	sTitolo = "QUESTIONARIO"
	sCommento = "Modifica della Risposta"
	bCampiObbl = false
%>
	<!--#include virtual="include/setTestata.asp"-->
<%
End Sub
'---------------------------------------------------------------------------
Sub Modifica()
	' SQL statement
	sql = "UPDATE RISPOSTA SET TXT_RISPOSTA = '" & Risposta 
	sql = sql & "' , IMG_Risposta = '" & IMMAGINE 
	sql = sql & "' , FL_ESATTO = '" & value 
	sql = sql & "' , ID_DOMANDA = " & IDDOM
	sql = sql & " WHERE ID_RISPOSTA = " & Idris
			
    'Response.Write sql
	sErrore=Esegui(idris ,"Risposta",Session("persona"),"MOD",sql,1,dtTmst)
	IF sErrore="0" then	
	%>
		<br>
		<table border=0 cellspacing=2 cellpadding=1 width="500">
			<tr align=middle>
				<td class="tbltext3">
					Modificación correctamente efectuada.<br><br>
				</td>
			</tr>
		</table>
		<br><br>
		<table border=0 cellspacing=2 cellpadding=1 width="500">
			<tr align=middle>
				<td>
					<%
					PlsLinkRosso "QUE_VisRisposta.asp?idd=" & IDDOM & "&idq=" & IDQUE & "&ida=" & IDAREA, "Elenco delle Risposte" 
					%>
				</td>
			</tr>
		</table>
		<br>

	<%
	else
	%>
	<br>
	<table border=0 cellspacing=2 cellpadding=1 width="500">
		<tr align=middle>
			<td class="tbltext3">
				Error en la modificación. <%=sErrore%>
			</td>
		</tr>
	</table>		
	<br>
	<table border=0 cellspacing=2 cellpadding=1 width="500">
		<tr align=middle>
			<td align=center>
				<%
				PlsIndietro()
				%>
			</td>
		</tr>
	</table>
	<br>
	<%
	End If
End Sub
'---------------------------------------------------------------------------------------------------------------------------------------------------------
Sub Inserisci()
				
	DTTMST = now() 
		
	sql = "INSERT INTO RISPOSTA (ID_DOMANDA, TXT_RISPOSTA, FL_ESATTO, IMG_RISPOSTA)"
	sql = sql & " VALUES (" & IDDOM 
	sql = sql & ",'" & RISPOSTA & "'"
	sql = sql & ",'" & VALUE & "'"
	sql = sql & ",'" & IMMAGINE & "')"
'		Response.Write sql
	sErrore=Esegui("pgm/formazione/questionari/que","RISPOSTA",Session("persona"),"INS",SQL,1,"")

	IF sErrore="0" then	
	%>
		<br>
		<table border=0 cellspacing=2 cellpadding=1 width="500">
			<tr align=middle>
				<td class="tbltext3">
					Ingreso correctamente efectuado.<br><br>
				</td>
			</tr>
		</table>
		<br><br>
		<table border=0 cellspacing=2 cellpadding=1 width="500">
			<tr align=middle>
				<td>
					<%
					PlsLinkRosso "QUE_VisRisposta.asp?idd=" & IDDOM & "&idq=" & IDQUE & "&ida=" & IDAREA, "Elenco delle Risposte" 
					%>
				</td>
			</tr>
		</table>
		<br>
	<%
	else
	%>
	<br>
	<table border=0 cellspacing=2 cellpadding=1 width="500">
		<tr align=middle>
			<td class="tbltext3">
				Error en el ingreso. <%=sErrore%>
			</td>
		</tr>
	</table>		
	<br>
	<table border=0 cellspacing=2 cellpadding=1 width="500">
		<tr align=middle>
			<td align=center>
				<%
				PlsIndietro()
				%>
			</td>
		</tr>
	</table>
	<br>
	<%
	End If	
		
End Sub
'--------------------------------------------------------------------------------------------------------------------------------------------------------->
'M A I N
	
	IDQUE=Request.QueryString("IDQUE")
	IDAREA=Request.QueryString("IDAREA")
	IDDOM=Request.QueryString("IDDOM")
	IDRIS=Request.QueryString("IDRIS")
	
	risposta=Replace(Request.Form("risposta"), "'", "''")
	IMMAGINE=Request.Form("txtMedia")
	value=Request.Form("value")
	DTTMST = Request.form("hDtTmst")
	IDP = Session("creator")
	modo = Request.QueryString("modo")

%>
<!--#include virtual="/strutt_testa2.asp"-->
<!--#include virtual="/include/openconn.asp"-->
<!--#include virtual="/Util/DBUtil.asp"-->
<!--#include virtual="/Include/SysFunction.asp"-->
<!--#include virtual="/Include/SetPulsanti.asp"-->
<%
	Inizio()
	if modo="ins" then
		Inserisci()
	else
		Modifica()
	end if
	
%>			
<!--#include virtual="/include/closeconn.asp"-->
<!--#include virtual="/strutt_coda2.asp"-->
