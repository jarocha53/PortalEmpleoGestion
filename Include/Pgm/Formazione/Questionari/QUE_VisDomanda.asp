<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%
Sub Inizio()
	dim sFunzione
	dim sTitolo
	dim sCommento
	dim bCampiObbl
	
	sFunzione = "GESTIONE QUESTIONARIO"
	sTitolo = "QUESTIONARIO"
	'sCommento = "Elenco delle domande"
	sCommento = "Elenco domande<br>Cliccando sul titolo � possibile modificare le caratteristiche della domanda. L'icona della colonna Immagine permette di visualizzare un'anteprima dell'immagine. L'icona della colonna Risposte visualizza l'elenco delle risposte associate alla domanda. Il link Inserisci Domanda mancante permette di inserire le domande mancanti."  
	bCampiObbl = false
	sHelp = "/Pgm/help/Formazione/Questionari/QUE_VisDomanda/"	
	
%>
	<!--#include virtual="include/SetTestata.asp"-->
<%
End Sub
'--------------------------------------------------------------------
Sub VisPagina()
%>
<table width="500" border="0" cellpadding="2" cellspacing="3">
<%
dim sSQL, nIdQuest, nIdArea
dim rsDomanda, rsQuestAreaArea

nIdQuest = request("idq")
nIdArea =  request("ida")
sSQL = "select q.TIT_QUESTIONARIO, a.DESC_AREA_DOM, a.num_dom_area, a.IMG_AREA_DOM  from AREA_DOM a, QUESTIONARIO q where q.ID_QUESTIONARIO = a.ID_QUESTIONARIO and a.ID_AREA_DOM = " & nIdArea & " and q.ID_QUESTIONARIO = " & nIdQuest
'Response.Write sSQL
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
set rsQuestArea = cc.execute(sSQL)
if not rsQuestArea.eof then
ndoma = rsQuestArea("num_dom_area")
if isnull(ndoma) then
	ndoma =0 
end if
%>

<table width="500" border="0">
	<tr>
		<td class="tbltext3">
			Test: &quot;<%=rsQuestArea("TIT_QUESTIONARIO")%>&quot;
		</td>
	</tr>
</table>

<table width="500" border="0">
	<%
	if rsQuestArea("DESC_AREA_DOM") > " " then
	%>
	<tr><td>&nbsp;</td></tr>
	<tr>
		<td class="tbltext3">
			Testo Area: &quot;<%=rsQuestArea("DESC_AREA_DOM")%>&quot;
		</td>
	</tr>
	<%
	end if

	if rsQuestArea("IMG_AREA_DOM") > " " then
	%>
	<tr><td>&nbsp;</td></tr>
	<tr>
		<td class="tbltext3">
			Immagine Area:<br>
			<img SRC="<%=rsQuestArea("IMG_AREA_DOM")%>">
		</td>
	</tr>
	<%
	end if
	%>
</table>
<br>

<table border="0" cellspacing="2" cellpadding="1" width="500">
	<tr class="sfondocomm">        
		<td align="middle" width="20">#</td>
		<td align="middle">Descrizione</td>
		<td align="middle" width="70">Immagine</td>
		<td align="middle" width="70">Risposte</td>
	</tr>

<%	
end if
rsQuestArea.close

sSQL = "select id_domanda, txt_domanda, img_domanda, aud_domanda, num_risposte from domanda where id_area_dom = " & nIdArea & " and id_questionario = " & nIdQuest & " order by id_domanda"
'Response.Write "<br>" & sSQL
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
set rsDomanda = CC.execute(sSQL)
c = 1
do while not rsDomanda.eof
	if rsDomanda("txt_domanda") > " " then
		sDomanda = rsDomanda("txt_domanda")
	else
		sDomanda = "Senza testo"
	end if
%>
	<tr class="tblsfondo">
		<td align="right" class="tblDett">
			<%=c%>
		</td>	
		<td>
			<a href="QUE_ModDomanda.asp?idd=<%=rsDomanda("id_domanda")%>&amp;idq=<%=nIdQuest%>&amp;ida=<%=nIdArea%>" class="tblAgg">
				<%=sDomanda%>
			</a>
		</td>
		<td align="center">
		<% 
			if rsDomanda("img_domanda") > " " then 
		%>
			<a href="<%=rsDomanda("img_domanda")%>" target="_blank">
				<img src="<%=Session("progetto")%>/images/icons2/gif.gif" border="0" alt="Anteprima Immagine">
			</a>
		<%
			else
		%> 		
			&nbsp;
		<%
			end if 
		%> 		
		</td>
		<td align="middle" width="70">
			<a href="QUE_VisRisposta.asp?idd=<%=rsDomanda("id_domanda")%>&amp;idq=<%=nIdQuest%>&amp;ida=<%=nIdArea%>">
				<img src="<%=Session("progetto")%>/images/re.gif" border="0" alt="Elenco delle Risposte">
			</a>
		</td>
	</tr>	
<%
	rsDomanda.movenext
	c = c + 1
loop
%>
</table>

<br>
<table width="500">
	<tr>
<%
if cint(ndoma) > cint(c-1) then 
%> 
	<td align="center">
	<%
		PlsLinkRosso "QUE_InsDomanda.asp?idq=" & nIdQuest & "&ida=" & nIdArea, "Inserisci Domanda mancante"
	%>
	</td>
<%
end if 
%>
	<td align="center">
	<%
		PlsLinkRosso "QUE_VisArea.asp?id=" & nIdQuest, "Elenco delle Aree"
	%>
	</td>
	</tr>
</table>
<br><br>
<%
End Sub
'---------------------------------------------------------------------
'M A I N
%>
<!--#include virtual="/strutt_testa2.asp"-->
<!--#include virtual="/include/openconn.asp"-->
<!--#include virtual="/include/SetPulsanti.asp"-->
<%	
	Inizio()
	VisPagina()
%>
<!--#include virtual="/include/closeconn.asp"-->
<!--#include virtual="/strutt_coda2.asp"-->
