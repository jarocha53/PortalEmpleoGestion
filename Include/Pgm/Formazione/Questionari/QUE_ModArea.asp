<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%
Sub ControlliJavaScript()
%>
	<script LANGUAGE="JavaScript">
		<!--
		function validateIt(frmArea)
		{
		if(document.form1.Areadom.value == "" || document.form1.Areadom.value == " ")
		  {
		  document.form1.Areadom.focus();
		  alert("Inserire il testo dell'Area.");
		  return false;
		  }
		if(document.form1.NDomande.value == "")
		  {
		  document.form1.NDomande.focus();
		  alert("Inserire il numero di domande per area.");
		  return false;
		  }
		if (isNaN(document.form1.NDomande.value))
		  {
		  document.form1.NDomande.focus();
		  alert("'Numero Domande' deve essere un valore numerico.");
		  return false;
		  }		  
		return true;
		}

		function ConfElimina(ida, idq, tmst)
		{
			if (confirm("Confermi la cancellazione dell'Area?") == true)
			{
				window.location = "QUE_DelArea.asp?ida="+ida+"&idq="+idq+"&tmst="+tmst
			}
		}

		var finestra
		function ListaImmagini(sImage)
		{
			if (finestra != null ) 
			{
				finestra.close(); 
			}
			f='QUE_VisImmagini.asp?img=' + sImage
			finestra=window.open(f,"","width=550,height=450,location=no,menubar=no,scrollbars=yes,resizable=no")
		}

		//-->
	</script>
<%
End Sub
'---------------------------------------------------------------------------
Sub Inizio()

	dim sFunzione
	dim sTitolo
	dim sCommento
	dim bCampiObbl
	
	sFunzione = "GESTIONE QUESTIONARIO"
	sTitolo = "QUESTIONARIO"
	'sCommento = "Modifica dell'area<br>del Questionario"
	sCommento = "Modifica area<br>Modificare le informazioni e premere Invia per memorizzarle. Premere Elimina per cancellare l'area."
	bCampiObbl = true
	sHelp = "/Pgm/help/Formazione/Questionari/QUE_ModArea/"	
%>
	<!--#include virtual="include/setTestata.asp"-->
<%
End Sub
'------------------------------------------------------------------------------------------------
Sub PaginaVuota()
%>
<table width="500" border="0">
	<tr>
		<td class="tbltext3" align="center">
			Errore durante la ricerca dell'Area.
		</td>
	</tr>
	<tr><td>&nbsp;</td></tr>
	<tr>
		<td align="center">
			<%
				PlsIndietro()
			%>
		</td>
	</tr>
</table>
<%
End Sub
'------------------------------------------------------------------------------------------------
Sub ImpostaPag()
	
	IdQue=Request("idq")
	IdArea=Request("ida")
	
		set Rs=server.CreateObject("ADODB.Recordset")
		sql1 = "SELECT DESC_AREA_DOM, IMG_AREA_DOM, NUM_DOM_AREA, DT_TMST " &_
			 "from Area_Dom where ID_Area_Dom = " & idArea

'PL-SQL * T-SQL  
SQL1 = TransformPLSQLToTSQL (SQL1) 
		Rs.open sql1, CC, 3
		if rs.EOF then
			PaginaVuota()
			exit sub
		end if

		xdescar = Rs.Fields("DESC_AREA_DOM") 
		ximagea = trim(Rs.Fields("IMG_AREA_DOM"))
		xnumdom = Rs.Fields("NUM_DOM_AREA")
		dtTmst  = Rs.Fields("DT_TMST")
		
		Rs.close
		set Rs = nothing 

		set RR=server.CreateObject("ADODB.Recordset")
		sql="SELECT TIT_QUESTIONARIO from QUESTIONARIO where ID_Questionario = " & IDQUE
 '		Response.Write sql
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
		RR.open sql, CC, 3
		TIT = RR.FIELDS("TIT_QUESTIONARIO")
'		Response.Write nQST
		RR.CLOSE
%>
	<table width="500" border="0">
		<tr><td class="tbltext3">Questionario: '<%=TIT%>'</td></tr>
	</table>		
	<br>		
	<form action="QUE_CnfModArea.asp" method="POST" id="form1" name="form1" onsubmit="return validateIt(this);">
	<input type="hidden" name="IDQUE" id="IDQUE" value="<%=IDQUE%>">
	<input type="hidden" name="IDAREA" id="IDAREA" value="<%=IDAREA%>">
	<input type="hidden" name="DTTMST" id="DTTMST" value="<%=DTTMST%>">
		<table border="0" width="500" cellspacing="2" cellpadding="1"> 
			<tr>
				<td class="tbltext1">
					<b>Area Domande*</b>
				</td>
				<td>
					<input type="text" id="Areadom" name="Areadom" size="40" maxlength="250" value="<%=xdescar%>" class="textblacka">
				</td>
			</tr>
		    <tr>
				<td class="tbltext1">
					<b>Immagine</b>
				</td>
				<td>
					<input name="txtMedia" id="txtMedia" value="<%=ximagea%>" class="textblacka" readonly>
					<span class="tbltext">
						<a href="javascript:ListaImmagini('<%=ximagea%>')">
							<img src="<%=Session("progetto")%>/images/bullet1.gif" border="0"><!-- onmouseover="javascript:window.status='' ; return true"-->
						</a>
					</span>
				</td>
		    </tr>
		    <tr>
				<td class="tbltext1">
					<b>Numero Domande*</b>
				</td>
				<td>
					<input type="text" size="2" cols="2" name="NDomande" id="NDomande" class="textblacka" maxlength="2" value="<%=xnumdom%>">
				</td>
		    </tr>
			<tr> 
				<td align="center">&nbsp;</td>
			</tr>
		</table>
		<table width="500">
		    <tr> 
				<td align="center">
				<%
					PlsIndietro()
					PlsInvia("Conferma")
					PlsElimina("javascript:ConfElimina(" & idarea & ", " & idque & ",'" & dtTmSt & "')")
				%>
				</td>
		    </tr>		
		</table>
	</form>
<%
End Sub
'--------------------------------------------------------------------------------
'M A I N
%>
<!--#include virtual="/strutt_testa2.asp"-->
<!--#include virtual="/include/openconn.asp"-->
<!--#include virtual="/include/SetPulsanti.asp"-->
<%	
	ControlliJavaScript()	
	Inizio()
	ImpostaPag()
%>			
<!--#include virtual="/include/closeconn.asp"-->
<!--#include virtual="/strutt_coda2.asp"-->
