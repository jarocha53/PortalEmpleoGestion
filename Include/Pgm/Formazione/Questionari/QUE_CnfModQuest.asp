<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%
	Sub Inizio()
	dim sFunzione
	dim sTitolo
	dim sCommento
	dim bCampiObbl 
	
	sFunzione = "GESTIONE QUESTIONARIO"
	sTitolo = "QUESTIONARIO"
	sCommento = "Modifica del<br>Questionario"
	bCampiObbl = false
%>	
	<!--#include virtual="include/setTestata.asp"-->
<%
	End Sub
'--------------------------------------------------------------------------------------
	Sub Inserisci()
		Dim sTitolo, nNumDomande, nTempo, sTipo, nId_Area, dTmStMod
		Dim sErrore, nMax
		
		sTitolo = Replace(Request.Form("QTitolo"), "'", "''")
		nId_Area = Request.Form("QArea")
		sTipo=Request.Form("QTipo")
		nNumdomande = Request.Form("QNumDomande")
		nTempo = Request.Form("QTempo")
		nSoglia = Request.Form("QSoglia")
		IdQuest = Request.QueryString("IdQuest")
		dTmStMod = Request.Form("hDTmSt")
		
		sIdProjOwn = Request.Form("cboProjOwner")
		sFlPubPri = Request.Form("cboFlPubblico")
		sFlRep = Request.Form("chkFlRepeat")
		
		if nTempo <= "0" then
			nTempo = 0
		end if
		
		if sIdProjOwn = "" then
			sIdProjOwn = mid(session("progetto"),2)
		end if
		
		if sFlRep = "on" then
			sFlRep = "1"
		else
			sFlRep = "0"
		end if

		DTTMST = ConvDateToDB(now())

		sql = "UPDATE QUESTIONARIO SET NUM_DOM = " & nNumdomande & ", " &_
				"TIP_QUESTIONARIO = '" & sTipo & "', " &_
				"ID_AREA = " & nId_Area & ", " &_
				"TEM_QUESTIONARIO = " & nTempo & ", " &_
				"TIT_QUESTIONARIO = '" & sTitolo & "', " &_
				"SGL_QUESTIONARIO = " & nSoglia & ", " &_
				"DT_TMST = " & DTTMST & ", " &_
				"ID_PROJOWNER = '" & sIdProjOwn & "', " &_
				"FL_PUBBLICO = " & sFlPubPri & ", " &_
				"FL_REPEAT = " & sFlRep & " " &_
				"WHERE ID_QUESTIONARIO = " & IdQuest
			
		'Si esegue la insert Chiave, Tabella, Utente, Oper, StrSQL
			
		'Response.Write sql
		sErrore=Esegui(idQuest ,"pgm/formazione/questionari/questionario",Session("persona"),"MOD",sql,1,dTmStMod)

		IF sErrore <> "0" then
		%>
			<br><br>
			<table border=0 cellspacing=2 cellpadding=1 width="500">
				<tr align=middle>
					<td class="tbltext3">
						Errore nella modifica. <%=sErrore%>
					</td>
				</tr>
			</table>		
			<br><br>
		<% else %>
			<br><br>
			<table border=0 cellspacing=2 cellpadding=1 width="500">
			<tr align=middle>
					<td class=tbltext3>
						Modifica correttamente effettuata
				</td>
				</tr>
			</table>
			<br><br>
			<%
		End If
		%>

		<table border=0 cellspacing=2 cellpadding=1 width="500">
			<tr align=center>
				<td>
					<%
						PlsLinkRosso "QUE_VisQuestionari.asp","Elenco Questionari"
					%>
				</td>
				</tr>
			</table>
		<br><br>
		<%
	End Sub
'---------------------------------------------------------------------------
'M A I N
%>
<!--#include virtual="/strutt_testa2.asp"-->
<!--#include virtual="/include/openconn.asp"-->
<!--#include virtual="/Util/DBUtil.asp"-->
<!--#include virtual="/Include/SysFunction.asp"-->
<!--#include virtual="/Include/SetPulsanti.asp"-->
<%
'	Inizio()
	Inserisci()
%>			
<!--#include virtual="/include/closeconn.asp"-->
<!--#include virtual="/strutt_coda2.asp"-->
