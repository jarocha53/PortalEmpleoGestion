<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%
	Sub Inizio()
	dim sFunzione
	dim sTitolo
	dim sCommento
	dim bCampiObbl
	
	sFunzione = "GESTIONE QUESTIONARIO"
	sTitolo = "QUESTIONARIO"
	sCommento = "Cancellazione della domanda"
	bCampiObbl = false
%>	
	<!--#include virtual="include/SetTestata.asp"-->
<%
	End Sub
'--------------------------------------------------------------------------------------
	Sub Cancella()
		dim sErrore, sSQL
		dim nIdDom, nIdArea, nIdQuest, dDtTmst
		dim rsCRisp
		
		nIdDom = Request.QueryString("idd")
		nIdArea = Request.QueryString("ida")
		nIdQuest = Request.QueryString("idq")
		dDtTmst = Request.QueryString("tmst")
		
		
		'Controllo che non esistano Risposte associate al questionario
		sSQL =  " select count(*) as CRisp" &_
				" from risposta" &_
				" where id_domanda = " & nIdDom 
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
		set rsCRisp = cc.execute(sSQL)
		if cint(rsCRisp("CRisp")) > 0 then
			%>
				<br><br>
				<table border=0 cellspacing=2 cellpadding=1 width="500">
					<tr align=middle>
						<td class="tbltext3">
							Non � possibile cancellare la Domanda.<br>
							Cancellare prima le Risposte associate.
						</td>
					</tr>
				</table>		
				<br><br>
			<%
		else
			sSQL = "delete from domanda where id_domanda = " & nIdDom
			sErrore=Esegui(nIdDom ,"domanda",Session("persona"),"DEL",sSQL,1,dDtTmst)
			IF sErrore <> "0" then
			%>
				<br><br>
				<table border=0 cellspacing=2 cellpadding=1 width="500">
					<tr align=middle>
						<td class="tbltext3">
							Errore nella cancellazione. <%=sErrore%>
						</td>
					</tr>
				</table>		
				<br><br>
			<% else %>
				<br><br>
				<table border=0 cellspacing=2 cellpadding=1 width="500">
				<tr align=middle>
						<td class=tbltext3>
							Cancellazione correttamente effettuata
						</td>
					</tr>
				</table>
				<br><br>
				<%
			End If
		end if
		rsCRisp.close
		set rsCRisp = nothing
		%>
		<table border=0 cellspacing=2 cellpadding=1 width="500">
			<tr align=center>
				<td>
					<%
						PlsLinkRosso "QUE_VisDomanda.asp?idq=" & nIdQuest & "&ida=" & nIdArea,"Elenco delle Domande"
					%>
				</td>
				</tr>
			</table>
		<br><br>
		<%
	End Sub
'---------------------------------------------------------------------------
'M A I N
%>
<!--#include virtual="/strutt_testa2.asp"-->
<!--#include virtual="/include/openconn.asp"-->
<!--#include virtual="/Util/DBUtil.asp"-->
<!--#include virtual="/Include/SysFunction.asp"-->
<!--#include virtual="/Include/SetPulsanti.asp"-->
<%
	Inizio()
	Cancella()
%>			
<!--#include virtual="/include/closeconn.asp"-->
<!--#include virtual="/strutt_coda2.asp"-->
