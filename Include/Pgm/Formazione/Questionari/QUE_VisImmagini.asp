<%
Sub Inizio()
%>
<html>
<head>
	<link REL="STYLESHEET" TYPE="text/css" HREF="<%=session("progetto")%>/fogliostile.css">
	<meta NAME="GENERATOR" Content="Microsoft Visual Studio 6.0">
	<title>Visualizza Immagini</title>	
</head>
<script LANGUAGE="JavaScript">
function InviaSelezione(Immagine)
{
	opener.document.form1.txtMedia.value = Immagine
	self.close()
}


if (!document.layers&&!document.all)
event="test"
function showtip2(current,e,img)
{
	if (document.all&&document.readyState=="complete")
	{
		var tab
		
		tab= '<table border=2 class=sfondomenu><tr><td><img src=' + img + ' border=0 onclick="hidetip2();"></td></tr></table>'
	
		//document.all.tooltip2.innerHTML='<img src=' + img + ' border=0>'
		document.all.tooltip2.innerHTML=tab
		document.all.tooltip2.style.pixelLeft=document.body.scrollLeft +10
		document.all.tooltip2.style.pixelTop=document.body.scrollTop +10
		document.all.tooltip2.style.visibility="visible"
	}
}
function hidetip2()
{
	if (document.all)
		document.all.tooltip2.style.visibility="hidden"
	else if (document.layers)
	{
		clearInterval(currentscroll)
		document.tooltip2.visibility="hidden"
	}
}

</script>
<body>
<div id="tooltip2" style="position:absolute; visibility:hidden; color:white">

</div>


 
<p align="center">
<%
	dim sFunzione
	dim sTitolo
	dim sCommento
	dim bCampiObbl
	
	sFunzione = "GESTIONE QUESTIONARIO"
	sTitolo = "IMMAGINI PER IL QUESTIONARIO"
	sCommento = "Visualizzazione Immagini<br>Questionario"
	bCampiObbl = false
	sHelp = "/Pgm/help/Formazione/Questionari/QUE_VisImmagini/"
%>
	<!--#include virtual="/include/SetTestata.asp"-->
<%
End Sub
'------------------------------------------------------------------------
Sub Fine()
%>
<br>
<table width="500">
	<tr>
		<td align="center">
			<a href="javascript:window.close()">
				<img src="<%=Session("progetto")%>/images/chiudi.gif" border="0" onmouseover="javascript:window.status='' ; return true">
			</a>
		</td>
	</tr>
</table>
</p>		
</body>
</html>
<%
End Sub
'------------------------------------------------------------------------
Function EstraiDir(sPath)
	for nPos = 1 to Len(sPath)
		if mid(sPath,nPos,1) = "\" then
			nLastPos=nPos
		end if
	next
	EstraiDir = left(sPath, nLastPos)
End Function
'------------------------------------------------------------------------
Sub ImpostaPag()
dim nPos, nLastPos, folderspec
dim sPRel
dim nFile

	if sImg = "" then
		sPRel = "images/"
	else
		sPRel = EstraiDir(Replace(sImg,"/","\"))
	end if
	folderspec = server.mappath(sPRel)
	
	Dim fso, f, f1, fc
	Set fso = CreateObject("Scripting.FileSystemObject")
	Set f = fso.GetFolder(folderspec)
	Set fc = f.Files
	Set fs = f.SubFolders
	
'S U B F O L D E R
%>

<table width="500" border="0">
	<tr> 
		<td align="left" class="sfondocomm">
			Selezione della cartella
		</td>
	</tr>
<%
	if sPRel <> "images/" then
	app = EstraiDir(Replace(mid(sprel,1,len(sprel)-1),"/","\"))
		if len(app) > 0 then 
		prec = mid(app,1,len(app)-1)
		%>
			<tr> 
				<td align="left">
					<a href="QUE_VisImmagini.asp?modo=1&amp;img=<%=prec%>" class="tblagg">..\</a>
				</td>
			</tr>
		<%
		end if
	end if
		%>
			<tr>
				<td align="left" class="tbldett">
					<%=sprel%>
				</td>
			</tr>
		<%
	for each f2 in fs
		if left(f2.name,1) <> "_" then
%>
			<tr>
				<td align="left">
					<a href="QUE_VisImmagini.asp?modo=1&amp;img=<%=sprel & f2.name%>" class="tblagg"><%=sprel & f2.name%></a><br>
				</td>
			</tr>
<%
		end if
	next
%>
</table>
<br>

	<table width="500" border="0">
		<tr> 
			<td align="left" class="sfondocomm" colspan="5">
				Selezione dell'immagine
			</td>
		</tr>

		<tr>
<%
'F I L E S
	nFile = 0
	For Each f1 in fc
		sImmagine=replace(sprel & f1.name, "\", "/")
		if (right(f1.name,4) = ".gif") or (right(f1.name,4) = ".jpg") then
			nFile = nFile + 1
			if sImmagine = sImg then
				sAttr = " class=sfondocomm"
			else
				sAttr = ""
			end if
%>
			<td align="center" <%=sattr%>><!-- onmouseover="javascript:showtip2(this,event,'<%=sImmagine%>');" onmouseout="hidetip2();" -->
				<a href="javascript:InviaSelezione('<%=sImmagine%>');" border="0">
					<span class="tbldett"><b><%=f1.name%></b></span>
					<br>
					<img src="<%=sImmagine%>" width="50" height="50" border="0">
				</a>
				<br>
				<a href="Javascript:showtip2(this,event,'<%=sImmagine%>');" class="sfondocomm">preview</a>
			</td>
<%
			if nFile mod 5 = 0 then
%>
		</tr>
			<td colspan="5">&nbsp;</td>
		<tr>
<%
			end if
		end if
	Next
%>
		</tr>
	</table>
<%

End Sub
'------------------------------------------------------------------------
'M A I N
dim sImg, nModo
sImg = Request.QueryString("img")
nModo = Request.QueryString("modo")
if nModo = 1 then
	sImg = sImg & "/"
end if
'Response.Write "***" & sImg & "***"

%>	
<!--#include Virtual ="/include/openconn.asp"-->
<%
Inizio()
ImpostaPag()
%>	
<!--#include Virtual ="/include/closeconn.asp"-->
<%
Fine()
%>