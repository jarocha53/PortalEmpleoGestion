<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%
Sub ControlliJavascript()
%>
	<script LANGUAGE="JavaScript">
		<!--
		function ValidateIt(frmMe)
		{
		if((document.form1.risposta.value == ""))
		  {
		  document.form1.risposta.focus();
		  alert("Inserire il testo della risposta. ");
		  return false;
		  }
		return true;
		}

		var finestra
		function ListaImmagini()
		{
			if (finestra != null ) 
			{
				finestra.close(); 
			}
			f='QUE_VisImmagini.asp'
			finestra=window.open(f,"","width=550,height=450,location=no,menubar=no,scrollbars=yes,resizable=no")
		}

		//-->
	</script>
<%
End Sub
'----------------------------------------------------------------------------			
Sub Inizio()
	dim sFunzione
	dim sTitolo
	dim sCommento
	dim bCampiObbl
	
	sFunzione = "GESTIONE QUESTIONARIO"
	sTitolo = "QUESTIONARIO"
	sCommento = "Inserimento della Risposta"
	bCampiObbl = true
	sHelp = "/Pgm/help/Formazione/Questionari/QUE_InsRisposta/"	
%>
	<!--#include virtual="include/setTestata.asp"-->
<%
End Sub
'----------------------------------------------------------------------------			
Sub ImpostaPag()

set RR=server.CreateObject("ADODB.Recordset")
sql="SELECT NUM_DOM, TIT_QUESTIONARIO from QUESTIONARIO where ID_Questionario = " & IDQUE
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
RR.open sql, CC, 3
TIT = RR.FIELDS("TIT_QUESTIONARIO")
RR.CLOSE
		
set RR=server.CreateObject("ADODB.Recordset")
sql="SELECT DESC_AREA_DOM, IMG_AREA_DOM from AREA_DOM where ID_AREA_DOM = " & IDAREA
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
RR.open sql, CC, 3
TITAR = RR.FIELDS("DESC_AREA_DOM")
IMGAR = RR.FIELDS("IMG_AREA_DOM")
RR.CLOSE
%>
<table width="500" border="0">
	<tr>
		<td class="tbltext3">
			Test: &quot;<%=TIT%>&quot;
		</td>
	</tr>
</table>
<table width="500" border="0">
<%
if TITAR > " " then
%>
	<tr>
		<td class="tbltext3">
			Testo Area: &quot;<%=TITAR%>&quot;
		</td>
	</tr>
<%
end if
if IMGAR > " " then
%>
	<tr>
		<td class="tbltext3">Immagine Area:<br>
			<img SRC="<%=IMGAR%>">
		</td>
	</tr>
<%
end if
%>
</table>
<%
sSQL = "Select Txt_Domanda, IMG_DOMANDA, NUM_RISPOSTE, AUD_DOMANDA from domanda where id_domanda = " & IdDom
'Response.Write sSQL
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
set rsDomanda = cc.execute(sSQL)
if not rsDomanda.eof then
ndoma = rsDomanda("NUM_RISPOSTE")
%>
	<table width="500" border="0">
	<%
	if rsDomanda("txt_domanda") > " " then
	%>
		<tr>
			<td class="tbltext3">
				Testo Domanda: &quot;<%=rsDomanda("txt_domanda")%>&quot;
			</td>
		</tr>
	<%
	end if
	if rsDomanda("IMG_domanda") > " " then
	%>
		<tr>
			<td class="tbltext3">
				Immagine Domanda:<br>
				<img SRC="<%=rsDomanda("img_domanda")%>">
			</td>
		</tr>
	<%
	end if
	if rsDomanda("AUD_domanda") > " " then
	%>
		<tr>
			<td class="tbltext3">
				Audio Domanda:<br>
				<a href="<%=rsDomanda("aud_domanda")%>"><img SRC="audio.gif"></a>
			</td>
		</tr>
	<%
	end if
	%>
</table>
<br>
<%	
end if
rsDomanda.close
%>
<form action="QUE_CnfModRisposta.asp?modo=ins&amp;IDQUE=<%=IDQUE%>&amp;IDAREA=<%=IDAREA%>&amp;IDDOM=<%=IDDOM%>&amp;IDRis=<%=IDRis%>" method="POST" id="form1" name="form1" onsubmit="return ValidateIt(this);">
<table border="0" width="500" cellspacing="2" cellpadding="1"> 
	<tr>
		<td class="tbltext1">
 			<b>Risposta*</b>
 		</td>
		<td>
			<input type="text" id="risposta" name="risposta" size="40" maxlength="250" value="<%=xtextri%>" class="textblacka">
		</td>
	</tr>
	<tr>
		<td class="tbltext1">
			<b>Immagine</b>
		</td>
		<td>
			<input name="txtMedia" id="txtMedia" class="textblacka">			
			<a href="javascript:ListaImmagini()">
				<img src="<%=Session("progetto")%>/images/bullet1.gif" border="0"><!-- onmouseover="javascript:window.status='' ; return true"-->
			</a>
		</td>
	</tr>
    <tr>
		<td class="tbltext1">
			<b>Valore*</b>
		</td>
		<td>
			<select size="1" id="value" name="value" class="textblacka">
				<option selected value="S">Esatta</option>
				<option value="N">Errata</option>
			</select>
		</td>
	</tr>
</table>
<table border="0" width="500" cellspacing="2" cellpadding="1"> 
    <tr>
		<td align="right">
			<%
				PlsIndietro()
			%>
		</td>
		<td>
			<%
				PlsInvia("Conferma")
			%>
		</td>
	</tr>
</table>
</form>
<br>

<%
End Sub
'----------------------------------------------------------------------------
'M A I N
%>
<!--#include virtual="/strutt_testa2.asp"-->
<!--#include virtual="/include/openconn.asp"-->
<!--#include virtual="/include/SetPulsanti.asp"-->
<%

	IdQue=Request.QueryString("IDQ")
	IdArea=Request.QueryString("IDA")
	Iddom=Request.QueryString("IDD")
	Idris=Request.QueryString("IDr")

	ControlliJavaScript()
	Inizio()
 	ImpostaPag()

%>
<!--#include virtual="/include/closeconn.asp"-->
<!--#include virtual="/strutt_coda2.asp"-->
