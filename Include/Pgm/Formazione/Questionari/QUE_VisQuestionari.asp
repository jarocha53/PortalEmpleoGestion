<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%
Sub ControlliJavaScript()
%>
<script LANGUAGE="Javascript">
	var finestra
	function PubblicaQ(nId)
	{
		if (finestra != null ) 
		{
			finestra.close(); 
		}
		f='QUE_ModPubQuest.asp?id=' + nId
		finestra=window.open(f,"","width=550,height=450,location=no,menubar=no,scrollbars=yes,resizable=no")
	}
</script>
<%
End Sub
'----------------------------------------------------------------------
Sub Inizio()
	dim sFunzione
	dim sTitolo
	dim sCommento
	dim bCampiObbl
	
	sFunzione = "GESTIONE QUESTIONARIO"
	sTitolo = "QUESTIONARIO"
	'sCommento = "Elenco dei Questionari"
	sCommento = "Elenco dei Questionari<br>Cliccando sul Titolo � possibile modificare le caratteristiche del questionario. Cliccando sull'icona presente sulla colonna Anteprima viene visualizzato il questionario. L'icona sulla colonna Aree permette di visualizzare l'elenco delle aree del questionario. Cliccando su Nuovo Questionario � possibile inserire un nuovo questionario." 	
	bCampiObbl = false
	sHelp = "/Pgm/help/Formazione/Questionari/QUE_VisQuestionari/"	
%>
	<!--#include virtual="Include/SetTestata.asp"-->
<%
End Sub
'----------------------------------------------------------------------
Sub ElencoQ()

dim sSQL
dim rsQuest

sProgetto = mid(Session("Progetto"),2)

sSql = "Select ID_QUESTIONARIO, TIT_QUESTIONARIO, TIP_QUESTIONARIO, ID_AREA from QUESTIONARIO WHERE (id_projowner='" & sProgetto & "') order by ID_AREA , ID_QUESTIONARIO"
'Response.Write sSQL
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
set rsQuest = CC.execute(sSQL)

 	If rsQuest.EOF then 
			%>
			<table border="0" cellspacing="0" cellpadding="0" width="500">
			<tr>
				<td align="middle">
					<span class="tbltext3"><b>Non sono presenti Questionari</b></span>
				</td>
			</tr>
			</table>
	<%	
	else 
		Dim Conn, sql1, sTitolo, Rs
		dim nAppoArea
		dim nProg
		nProg = 1
		
		nAppoArea = -1
		do while not rsQuest.eof
		if cint(nAppoArea) <> cint(rsQuest("ID_AREA")) then
			set Rs=server.CreateObject("ADODB.Recordset")
			sql1="SELECT TITOLOAREA from AREA where ID_AREA = " & rsQuest("ID_AREA")
			'Response.Write sql1
'PL-SQL * T-SQL  
SQL1 = TransformPLSQLToTSQL (SQL1) 
			Rs.open sql1, CC, 3
			fArea = Rs.Fields("TITOLOAREA")
			Rs.close
			set Rs = nothing
			if cint(nAppoArea) > -1 then
				Response.Write "</table>"
			end if
			nAppoArea = rsQuest("ID_AREA")
			nProg = 1
		%>	
			<br>
			<table border="0" cellspacing="0" cellpadding="0" width="500">
				<tr>
			        <td>
						<span class="tbltext3"><%=fArea%></span>
					</td>
				</tr>
			</table>
			<table border="0" cellspacing="1" cellpadding="2" width="500">
				<tr class="sfondocomm">
			        <td width="20" align="center"><b>#</b></td>
			        <td><b>Titolo</b></td>
			        <td width="60" align="center"><b>Tipo</b></td>
			        <td width="60" align="center"><b>Pubblica</b></td>
			        <td width="60" align="center"><b>Anteprima</b></td>
			        <td width="60" align="center"><b>Aree</b></td>
				</tr>
		<%
		end if	 
		%>
		<tr class="tblsfondo">
			<td width="20" class="tblDett">
				<%=nProg%>
			</td>
			<td align="left">
				<a href="QUE_ModQuest.asp?id=<%=rsQuest("id_questionario")%>" class="tblAgg" onmouseover="javascript:window.status='' ; return true">
					<%=rsQuest("TIT_QUESTIONARIO")%>
				</a>
			</td>
			<td align="center" class="tbldett">
				<% if rsQuest("tip_questionario") = "S" then%>
				Statico
				<% else%>
				Dinamico
				<% end if%>
			</td>
			<td align="center">
				<a href="Javascript:PubblicaQ(<%=rsQuest("id_questionario")%>)" class="tblAgg" onmouseover="javascript:window.status='' ; return true">
					<img src="<%=Session("progetto")%>/images/formazione/consulta.gif" border="0" alt="Pubblica il Questionario">
				</a>
			</td>
			<td align="center">
				<a href="QUE_VisAnteprimaQ.asp?id=<%=rsQuest("id_questionario")%>" class="tblAgg" onmouseover="javascript:window.status='' ; return true">
					<img src="<%=Session("progetto")%>/images/formazione/Icons/txt.gif" border="0" alt="Anteprima Questionario">
				</a>
			</td>
			<td align="center">
				<% if rsQuest("tip_questionario") = "S" then%>				
				<a href="QUE_VisArea.asp?id=<%=rsQuest("id_questionario")%>" class="tblAgg" onmouseover="javascript:window.status='' ; return true">
					<img src="<%=Session("progetto")%>/images/re.gif" border="0" alt="Elenco delle Aree">
				</a>
				<% else%>
				&nbsp;
				<% end if%>
			</td>
		</tr>	
		<%
			rsQuest.movenext
			nProg = nProg + 1
		loop
		%>
		</table>
		<%
	end if		
%>
<br><table width="500" border="0" cellpadding="2" cellspacing="2">
	<tr class="tbltext">
		<td align="center">
		<%
			PlsLinkRosso "QUE_InsQuest.asp","Nuovo Questionario"
		%>
		</td>
	</tr>
	<tr class="tbltext">
</table>
<%
rsQuest.close

End sub
'----------------------------------------------------------------------
'M A I N
%>
<!--#include virtual="/strutt_testa2.asp"-->
<!--#include virtual="/include/openconn.asp"-->
<!--#include virtual="/include/SetPulsanti.asp"-->
<%	
	ControlliJavaScript()
	Inizio()
	ElencoQ()
%>
<!--#include virtual="/include/closeconn.asp"-->
<!--#include virtual="/strutt_coda2.asp"-->
