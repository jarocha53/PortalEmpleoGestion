<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%
Sub Inizio()
	dim sFunzione
	dim sTitolo
	dim sCommento
	dim bCampiObbl
	
	sFunzione = "GESTIONE CORSI"
	Select case sOper 
	case "Ins"
		sTitolo = "INSERIMENTO"
		sCommento = "Commento all'inserimento del<br>corso"
	case "Mod"	
		sTitolo = "MODIFICA"
		sCommento = "Commento alla modifica del<br>corso"
	case else
		sTitolo = "CANCELLAZIONE"
		sCommento = "Commento alla cancellazione del<br>corso"
	end select

	sTitolo = sTitolo & " CORSO"
	bCampiObbl = false
%>
	<!--#include virtual="/include/SetTestata.asp"-->
<%
End Sub
'--------------------------------------------------------------
Sub Inserisci()
	dim nArea, sTitCorso, sDescr, sObiettivi, nLivCorso, sVerAICC
	dim nCredCorso, nFlErog, nDurata, dDisponibilita, sAuthoring
	dim sInfoProd, nProjOwn, nFlPubblico, sPrereqCorso, sDestinatari, bDispo
	dim sSQL, sSQL0, sSQL1, sSQL2, sSQL3
	
	nArea = Request.Form("cmbArea")
	sTitCorso = Replace(Trim(Request.Form("txtTitoloCorso")), "'", "''")
	sDescr = Replace(Trim(Request.Form("txtDescrizione")), "'", "''")
	sObiettivi = Replace(Trim(Request.Form("txtObiettivo")), "'", "''")
	nLivCorso = Request.Form("cboLivCorso")
	sVerAICC = Request.Form("txtVerCorsoAICC")
	nCredCorso = Request.Form("txtNumCredCorso")
	nFlErog = Request.Form("cboFlErogazione")
	nDurata = Request.Form("txtDurata")
	dDisponibilita = Request.Form("txtDisponibilitaDal")
	sAuthoring = Replace(Trim(Request.Form("txtAuthoring")),"'","''")
	sInfoProd = Replace(Trim(Request.Form("txtInfoProd")),"'","''")
	nProjOwn = Request.Form("cboProjOwn")
	nFlPubblico = Request.Form("cboFlPubblico")
	sPrereqCorso = Replace(Trim(Request.Form("txtPrereqCorso")),"'","''")
	sDestinatari = Replace(Trim(Request.Form("txtDestinatari")),"'","''")
	bDispo = Request.Form("chkDispo")
	
	sSQL0 = "INSERT INTO CORSO ("
	sSQL2 = ") VALUES ("
	sSQL1 = "ID_AREATEMATICA, DESC_CORSO, TIT_CORSO, FL_EROGAZIONE, FL_PUBBLICO, DT_TMST, DT_DISPONIBILE, "
	sSQL3 = nArea & ", '" & sDescr & "', '" & sTitCorso & "', " & nFlErog & ", " & nFlPubblico & ", " & ConvDateToDB(Now()) & ", "
	
	if bDispo = "on" then
		sSQL3 = sSQL3 & ConvDateToDB("31/12/9999") & ", "
	else
		sSQL3 = sSQL3 & ConvDateToDB(dDisponibilita) & ", "
	end if
	if sObiettivi > "" then
		sSQL1 = sSQL1 & "OBIETTIVO_CORSO, "
		sSQL3 = sSQL3 & "'" & sObiettivi & "', "
	end if
	if nLivCorso > "" then
		sSQL1 = sSQL1 & "LIV_CORSO, "
		sSQL3 = sSQL3 & "'" & nLivCorso & "', "
	end if
	if sVerAICC > "" then
		sSQL1 = sSQL1 & "VER_CORSOAICC, "
		sSQL3 = sSQL3 & sVerAICC & ", "
	end if
	if nCredCorso > "" then
		sSQL1 = sSQL1 & "NUMCREDIT_CORSO, "
		sSQL3 = sSQL3 & nCredCorso & ", "
	end if
	if nDurata > "" then
		nDurata = cint(mid(nDurata,1,2) * 60) + cint(mid(nDurata,4,2))
		sSQL1 = sSQL1 & "DURATA_CORSO, "
		sSQL3 = sSQL3 & nDurata & ", "
	end if	
	if sAuthoring > "" then
		sSQL1 = sSQL1 & "AUTHORING_CORSO, "
		sSQL3 = sSQL3 & "'" & sAuthoring & "', "
	end if	
	if sInfoProd > "" then
		sSQL1 = sSQL1 & "INFO_PRODUTTORE, "
		sSQL3 = sSQL3 & "'" & sInfoProd  & "', "
	end if
	if nProjOwn > "" then
		sSQL1 = sSQL1 & "ID_PROJOWNER, "
		sSQL3 = sSQL3 & "'" & nProjOwn  & "', "
	else
		sSQL1 = sSQL1 & "ID_PROJOWNER, "
		sSQL3 = sSQL3 & "'" & mid(session("progetto"),2) & "', "	
	end if
	if sPrereqCorso > "" then
		sSQL1 = sSQL1 & "PREREQ_CORSO, "
		sSQL3 = sSQL3 & "'" & sPrereqCorso  & "', "
	end if
	if sDestinatari > "" then
		sSQL1 = sSQL1 & "DESTINATARI_CORSO, "
		sSQL3 = sSQL3 & "'" & sDestinatari  & "', "
	end if
	
	sSQL1 = mid(sSQL1,1,len(sSQL1)-2)
	sSQL3 = mid(sSQL3,1,len(sSQL3)-2)
	sSQL = sSQL0 & sSQL1 & sSQL2 & sSQL3 & ")"

	'Response.Write sSQL
	
	sErrore = EseguiNoC(sSQL ,CC)
			
	IF sErrore = "0" then
%>				
			<br><br>
			<table border=0 cellspacing=2 cellpadding=1 width="500">
				<tr align=center>
					<td class="tbltext3">
						Inserimento Corso correttamente effettuato
					</td>
				<tr>
			</table>
			<br><br>
			<table border=0 cellspacing=2 cellpadding=1 width="500">
				<tr align=center>
						<td>
							<%
								PlsLinkRosso "COR_RicCorsi.asp","Nuova Ricerca"
							%>
						</td>
					</tr>
				</table>
				<br><br>
<%	else 
%>				<br><br>
				<table border=0 cellspacing=2 cellpadding=1 width="500">
					<tr align=center>
						<td class="tbltext3">
							Errore nell'inserimento <%=sErrore%>
						</td>
					<tr>
				</table>		
				<br><br>
				<table border=0 cellspacing=2 cellpadding=1 width="500">
					<tr align=center>
						<td>
							<%
							PlsIndietro()
							%>
						</td>
					</tr>
					<tr align=center>
						<td>
							<%
							plsLinkRosso "COR_RicCorsi.asp", "Nuova Ricerca"
							%>
						</td>
					</tr>
				</table>
				<br><br>
	<%
	end if		
End Sub
'----------------------------------------------------------------------
Sub Modifica()
	dim nArea, sTitCorso, sDescr, sObiettivi, nLivCorso, sVerAICC
	dim nCredCorso, nFlErog, nDurata, dDisponibilita, sAuthoring
	dim sInfoProd, nProjOwn, nFlPubblico, sPrereqCorso, sDestinatari, bDispo
	dim sSQL, sSQL1, sSQL2, sSQL3
	dim nIdCorsoA, dDtTmst
	
	nIdCorsoA = Request.Form("IDCorso")
	dDtTmst = Request.Form("dtTmst")
	nArea = Request.Form("cmbArea")
	sTitCorso = Replace(Trim(Request.Form("txtTitoloCorso")), "'", "''")
	sDescr = Replace(Trim(Request.Form("txtDescrizione")), "'", "''")
	sObiettivi = Replace(Trim(Request.Form("txtObiettivo")), "'", "''")
	nLivCorso = Request.Form("cboLivCorso")
	sVerAICC = Request.Form("txtVerCorsoAICC")
	nCredCorso = Request.Form("txtNumCredCorso")
	nFlErog = Request.Form("cboFlErogazione")
	nDurata = Request.Form("txtDurata")
	dDisponibilita = Request.Form("txtDisponibilitaDal")
	sAuthoring = Replace(Trim(Request.Form("txtAuthoring")),"'","''")
	sInfoProd = Replace(Trim(Request.Form("txtInfoProd")),"'","''")
	nProjOwn = Request.Form("cboProjOwn")
	nFlPubblico = Request.Form("cboFlPubblico")
	sPrereqCorso = Replace(Trim(Request.Form("txtPrereqCorso")),"'","''")
	sDestinatari = Replace(Trim(Request.Form("txtDestinatari")),"'","''")
	bDispo = Request.Form("chkDispo")
	
	if bDispo = "on" then
		dDisponibilita = ConvDateToDB("31/12/9999")
	else
		dDisponibilita = ConvDateToDB(dDisponibilita)
	end if

	sSQL1 = "UPDATE CORSO SET " &_
			"ID_AREATEMATICA = " & nArea & ", " &_
			"DESC_CORSO = '" & sDescr  & "', " &_
			"TIT_CORSO = '" & sTitCorso & "', " &_
			"FL_EROGAZIONE = " & nFlErog  & ", " &_
			"DT_DISPONIBILE = " & dDisponibilita  & ", " &_
			"FL_PUBBLICO = " & nFlPubblico & ", "
	
	sSQL2 = sSQL2 & "OBIETTIVO_CORSO = '" & sObiettivi & "', "
	sSQL2 = sSQL2 & "LIV_CORSO = '" & nLivCorso & "', "
	if sVerAICC > "" then
		sSQL2 = sSQL2 & "VER_CORSOAICC = " & sVerAICC & ", "
	else
		sSQL2 = sSQL2 & "VER_CORSOAICC = null, "
	end if
	if nCredCorso > "" then
		sSQL2 = sSQL2 & "NUMCREDIT_CORSO = " & nCredCorso & ", " 
	else
		sSQL2 = sSQL2 & "NUMCREDIT_CORSO = null, " 
	end if
	if nDurata > "" then
		nDurata = cint(mid(nDurata,1,2) * 60) + cint(mid(nDurata,4,2))
		sSQL2 = sSQL2 & "DURATA_CORSO = " & nDurata & ", "
	else
		sSQL2 = sSQL2 & "DURATA_CORSO = null, "
	end if
	sSQL2 = sSQL2 & "AUTHORING_CORSO = '" & sAuthoring & "', "
	sSQL2 = sSQL2 & "INFO_PRODUTTORE = '" & sInfoProd & "', "
	sSQL2 = sSQL2 & "ID_PROJOWNER = '" & nProjOwn & "', "
	sSQL2 = sSQL2 & "PREREQ_CORSO = '" & sPrereqCorso & "', "
	sSQL2 = sSQL2 & "DESTINATARI_CORSO = '" & sDestinatari & "', "
			
	sSQL3 = "DT_TMST = " & convdatetodb(Now) & " WHERE ID_CORSO = " & nIdCorsoA
	
	sSQL = sSQL1 & sSQL2 & sSQL3
		
	'Response.Write sSQL
	sErrore=Esegui(nIDCorso, "CORSO", Session("idPersona"), "MOD", sSQL, 1, dDtTmst)
			
	IF sErrore="0" then
%>			<br><br>
		<table border=0 cellspacing=2 cellpadding=1 width="500">
			<tr align=center>
				<td class="tbltext3">
					Modifica Corso correttamente effettuata
				</td>
			<tr>
		</table>
		<br><br>
		<table border=0 cellspacing=2 cellpadding=1 width="500">
			<tr align=center>
				<td>
					<%
						PlsLinkRosso "COR_RicCorsi.asp", "Nuova Ricerca"
					%>
				</td>
			</tr>
		</table>
		<br><br>
<%		else 
%>			<br><br>
		<table border=0 cellspacing=2 cellpadding=1 width="500">
			<tr align=center>
				<td class="tbltext3">
					Errore nella Modifica. <%=sErrore%>
				</td>
			<tr>
		</table>		
		<br><br>
		<table border=0 cellspacing=2 cellpadding=1 width="500">
			<tr align=center>
				<td>
					<%
						PlsIndietro()
					%>
				</td>
			</tr>
			<tr align=center>
				<td>&nbsp;</td>
			</tr>
			<tr align=center>
				<td>
					<%
						PlsLinkRosso "COR_RicCorsi.asp", "Nuova Ricerca"
					%>
				</td>
			</tr>
		</table>
		<br><br>
<%
	End If	
End Sub
'-----------------------------------------------------------------------------------
	Sub Cancella()
		Dim sErrore, dtTmst
		Dim sql,  RR
		
		dtTmst = Request.QueryString("DtTmst")	
		sql="DELETE FROM CORSO WHERE ID_CORSO = " & nIDCorso 
		sErrore = Esegui(nIDCorso,"ARGOMENTO",Session("idPersona"),"CAN",sql,1,dtTmst)
				
		IF sErrore="0" then
%>				
			<br><br>
			<table border=0 cellspacing=2 cellpadding=1 width="500">
				<tr align=center>
					<td class="tbltext3">
						Cancellazione Corso correttamente effettuata
					</td>
				<tr>
			</table>
			<br><br>
			<table border=0 cellspacing=2 cellpadding=1 width="500">
				<tr align=center>
					<td>
						<%
							PlsLinkRosso "COR_RicCorsi.asp", "Nuova Ricerca"
						%>
					</td>
				</tr>
			</table>
			<br><br>
<%			
		else
%>				
			<br><br>
			<table border=0 cellspacing=2 cellpadding=1 width="500">
				<tr align=center>
					<td class="tbltext3">
						Errore nella cancellazione: <%=sErrore%>
					</td>
				<tr>
			</table>		
			<br><br>
			<table border=0 cellspacing=2 cellpadding=1 width="500">
			<tr align=center>
				<td>
					<%
						 PlsLinkRosso "COR_RicCorsi.asp", "Nuova Ricerca"
					%>
				</td>
			</tr>
			<tr><td>&nbsp;</td></tr>
			<tr align=center>
				<td>
					<%
						PlsIndietro()
					%>
				</td>
			</tr>
			</table>
			<br><br>
<%
		end if
	End Sub
'-----------------------------------------------------------------------------
'M A I N
%>
<!--#include virtual="/strutt_testa2.asp"-->
<!--#include virtual="/include/openconn.asp"-->
<!--#include virtual="/Util/DBUtil.asp"-->
<!--#include virtual="/Include/SysFunction.asp"-->
<!--#include virtual="/Include/SetPulsanti.asp"-->
<%
	Dim strConn, sOper, nIDCorso

	sOper = Request("Oper")
	nIDCorso = Request("IDCorso")
	Inizio()
	Select case sOper
		case "Ins"
			Inserisci()
		case "Mod"
			Modifica()
		case "Can"
			Cancella()
		case else
%>		
		<table border=0 cellspacing=2 cellpadding=1 width="500">
			<tr align=center>
				<td class="tbltext3">
					Errore nel Passaggio dei parametri. (<%=sOper%>)
				</td>
			<tr>
		</table>
<%	
	End select
%>			
<!--#include virtual="/include/closeconn.asp"-->
<!--#include virtual="/strutt_coda2.asp"-->
