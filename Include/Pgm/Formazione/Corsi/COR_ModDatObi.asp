<script language="Javascript">
<!--#include virtual="/include/ControlString.inc"-->
function ControllaDati()
{
	document.frmObi.txtDescObi.value=TRIM(document.frmObi.txtDescObi.value)
	if (document.frmObi.txtDescObi.value=='')
	{
		alert('Il campo Obiettivo � obbligatorio.')
		document.frmObi.txtDescObi.focus()
		return false
	}
return true
}
//-------------------------------------------------------------------------------------
function ControllaElimina()
{
	if (confirm('Confermi la cancellazione dell`obiettivo?'))
	{
		return true
	}
	else
	{
		return false	
	}
}
</script>
<%
'-----------------------------------------------------------------------
Sub Inizio
	dim sFunzione
	dim sTitolo
	dim sCommento
	dim bCampiObbl
	
	sFunzione = ""
	sTitolo = "GESTIONE CORSO"
	sCommento = "Modificare la descrizione dell'obiettivo e premere <b>Invia</b> per confermare.<br>" &_
			"Premendo il tasto <b>Elimina</b> verr� eliminato l'obiettivo."
	bCampiObbl = true
%>
	<!--#include virtual="/include/SetTestata.asp"-->
<%
End Sub
'-----------------------------------------------------------------------
Sub ImpostaPag
	dim iIdCorso,sTitCorso
	dim iIdObi,sDescObi
	dim dtTmst
	
	iIdCorso=Request.Form("hIdCorso")
	sTitCorso=Request.Form("hTitCorso")
	iIdObi=Request.Form("hIdObi")
	sDescObi=Request.Form("hDescObi")
	sDescObi=Replace(sDescObi,"�","""")
	dtTmst=Request.Form("hTmst")
	
	'Response.Write "iIdCorso=" & iIdCorso & "<br>"
	'Response.Write "sTitCorso=" & sTitCorso & "<br>"
	'Response.Write "iIdObi=" & iIdObi & "<br>"
	'Response.Write "sDescObi=" & sDescObi & "<br>"
	'Response.Write "dtTmst=" & dtTmst & "<br>"
	

%>	<form name="frmObi" id="frmObi" method="post" onsubmit="JavaScript:return ControllaDati();" action="COR_CnfModDatObi.asp">
	<table border="0" width="500" cellspacing="4">
		<tr>
			<td class="tbltext1BFad" width="25%">Corso
			</td>
			<td class="tbltextBFad" width="75%"><%=strHTMLEncode(sTitCorso)%> 
			</td>
		</tr>
		<tr>
			<td class="tbltext1BFad">Obiettivo*
			</td>
			<td>
				<span class="tbltext1Fad">
					- Utilizzabili <label name="lblNumCar" id="lblNumCar"><%=4000-Len(sDescObi)%></label> caratteri -
				</span>
				<textarea name="txtDescObi" id="txtDescObi" class="textblackFad" rows="6" cols="50" style="text-transform:uppercase" OnKeyUp="JavaScript:CheckLenTextArea(document.frmObi.txtDescObi,lblNumCar,4000)"><%=strHTMLEncode(sDescObi)%></textarea>
			</td>
		</tr>
	</table>
	<br><br><br><br><br>
		
	<table border="0" width="250">
		<tr>
			<td align="center">
				<a href="JavaScript:history.back()"><img src="<%=session("progetto")%>/images/indietro.gif" border="0"></a>
			</td>
			<td align="center">
				<input type="hidden" name="hIdCorso" id="hIdCorso" value="<%=iIdCorso%>">
				<input type="hidden" name="hTitCorso" id="hTitCorso" value="<%=sTitCorso%>">
				<input type="hidden" name="hIdObi" id="hIdObi" value="<%=iIdObi%>">
				<input type="hidden" name="hTmst" id="hTmst" value="<%=dtTmst%>">
				<input type="image" src="<%=session("progetto")%>/images/conferma.gif">
			</td>
	</form>	
		
		<form name="frmElimObi" id="frmElimObi" onsubmit="JavaScript:return ControllaElimina();" action="COR_CnfElimObi.asp" method="post">
			<input type="hidden" name="hIdCorso" id="hIdCorso" value="<%=iIdCorso%>">
			<input type="hidden" name="hTitCorso" id="hTitCorso" value="<%=sTitCorso%>">
			<input type="hidden" name="hIdObi" id="hIdObi" value="<%=iIdObi%>">
			<input type="hidden" name="hTmst" id="hTmst" value="<%=dtTmst%>">
			<td align="center">
				<input type="image" src="<%=session("progetto")%>/images/elimina.gif" id="image1" name="image1">
			</td>
		</form>
		</tr>
	</table>
<%	
End Sub
'------------------------------------------------------------------------
%>
<!--#include virtual="/strutt_testa2.asp"-->
<!--#include virtual="/include/HTMLEncode.asp"-->
<center>
<%	
	call Inizio
	call ImpostaPag
%>
</center>
<!--#include virtual="/strutt_coda2.asp"-->