<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<script LANGUAGE="Javascript">
<!--#include virtual = "/Include/help.inc"-->
	function Carica_Pagina(npagina)
	{
		document.frmPagina.pagina.value = npagina;
		document.frmPagina.submit();
	}


</script>

<%
Sub Inizio()
	dim sFunzione
	dim sTitolo
	dim sCommento
	dim bCampiObbl
	 
	sHelp = "/Pgm/help/Formazione/Corsi/Regole/RDI_VisCorsi"
		 
	sFunzione = "STRUTTURA CORSO"
	sTitolo = "STRUTTURA CORSO"
	sCommento = "Elenco dei<br>Corsi ricercati"
	bCampiObbl = false
%>
	<!--#include virtual="/include/SetTestata.asp"-->
<%
End Sub
';););););););););););););););););););););););););););););););););););););););)
Sub ImpostaPag()
	Dim aTipo(2)
	Dim sSQL, ssSQLCond
	Dim sCondTitCorso
	dim sCondTitArea
	dim sCondDestinatari
	dim sCondObiettivi
	dim sCondModalita
	dim sCondDurata

	aTipo(0) = "Off Line"
	aTipo(1) = "On Line"
		
	sSQL = "SELECT * FROM Corso, Area WHERE Area.ID_AREA = Corso.ID_AREATEMATICA "
	sSQLCond = ""

	sCondTitCorso	= Request.Form("txtTitCorso")
	sCondTitArea	= CLng(Request.Form("txtTitArea"))
	sCondDestinatari = Request.Form("txtDestinatari")
	sCondObiettivi	= Request.Form("txtObiettivi")
	sCondModalita	= Request.Form("txtModalita")
	sCondDurata		= Request.Form("txtDurata")

	Record=0
	nTamPagina=15
	If Request.Form("pagina")= "" Then
		nActPagina=1
	Else
		nActPagina=Clng(Request.Form("pagina"))
	End If

	
	If sCondTitCorso <> "" Then	
		sSQLCond = sSQLCond & " AND TIT_CORSO LIKE '%" & _
		Replace(sCondTitCorso, "'", "''") & "%'"
	End If

	If sCondTitArea <> 0 Then
		sSQLCond = sSQLCond & " AND ID_AREA = " & sCondTitArea
	End If

	If sCondObiettivi <> "" Then
		sSQLCond = sSQLCond & " AND OBIETTIVO_CORSO LIKE '%" & _
		Replace(sCondObiettivi, "'", "''") & "%'"
	End If

	If sCondModalita <> "" AND sCondModalita <> "TUTTE" Then
		sSQLCond = sSQLCond & " AND  FL_EROGAZIONE ='" & sCondModalita & "'"
	End If

	If sCondDurata <> "" Then	
		sSQLCond = sSQLCond & " AND DURATA_CORSO LIKE '%" & _
		Replace(sCondDurata, "'", "''") & "%'"
	End If

	sSQL = sSQL & sSQLCond & " ORDER BY ID_CORSO, TIT_CORSO"

	Set rsCorsi = Server.CreateObject("ADODB.Recordset")
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	rsCorsi.Open sSQL, CC, 3
		
	If rsCorsi.EOF Then
	%>
		 <table border="0" width="500" cellpadding="0" cellspacing="0">
			<tr align="center">
				<td class="tbltext3"> 
					Non vi sono Corsi che soddisfano le condizioni di ricerca
				</td>
			</tr>
		 </table>
<%  

    ELSE
		If Not rsCorsi.EOF Then
		    
			rsCorsi.PageSize = nTamPagina
			rsCorsi.CacheSize = nTamPagina	

			nTotPagina = rsCorsi.PageCount		
			If nActPagina < 1 Then
				nActPagina = 1
			End If
			If nActPagina > nTotPagina Then
				nActPagina = nTotPagina
			End If

			rsCorsi.AbsolutePage=nActPagina
			nTotRecord=0		    
		    
		    %> 
			<table border="0" width="500">
				<form name="frmPagina" method="post" action="rdi_VisCorsi.asp">
					<input type="hidden" name="pagina" value>
					<input type="hidden" name="txtTitCorso" value="<%=sCondTitCorso%>">
					<input type="hidden" name="txtTitArea" value="<%=sCondTitArea%>">
					<input type="hidden" name="txtDestinatari" value="<%=sCondDestinatari%>">
					<input type="hidden" name="txtObiettivi" value="<%=sCondObiettivi%>">
					<input type="hidden" name="txtModalita" value="<%=sCondModalita%>">
					<input type="hidden" name="txtDurata" value="<%=sCondDurata%>">
				</form>
				<tr class="sfondocomm">
					<td><b>Titolo</b></td>
					<td width="45" align=center><b>Strutture</b></td>
				</tr>
			<%nPrgCorso = 0
			While rsCorsi.EOF <> True And nTotRecord < nTamPagina
			 
'				Do While Not rsCorsi.EOF
				nPrgCorso = nPrgCorso + 1
				sNomeForm = "FrmCorso" & nPrgCorso%>		
				<tr class="tblsfondo"> 
					<form METHOD="POST" name="<%=sNomeForm%>E" id="<%=sNomeForm%>E" action="RDI_VisElementi.asp">
						<input type=hidden name="cmbCorso" id="cmbCorso" 
							value="<%=clng(rsCorsi("ID_CORSO"))%>|<%=strHTMLEncode(rsCorsi("TIT_CORSO"))%>">
						<input type=hidden name="cmbElemento" id="cmbElemento" value="<%=clng(rsCorsi("ID_AREATEMATICA"))%>|Elemento">
					</form>
					<td align="left" class="tblDettFad">
							<%=strHTMLEncode(rsCorsi("TIT_CORSO"))%>
					</td>
					<td align="center">
						<a href="javascript:<%=sNomeForm%>E.submit()" 
							onmouseover="javascript:window.status='' ; return true"> 
						<img src="/images/formazione/creadoc.gif" width="20" 
							height="20" border="0" alt="Visualizza le strutture">
						</a>
					</td>
				</tr>
		    <%	rsCorsi.MoveNext
				nTotRecord=nTotRecord + 1	
			wend%> 
			</table>
		<%
		End If
		rsCorsi.Close
		
		Response.Write "<TABLE border=0 width=470 align=center>"
		Response.Write "<tr>"
		if nActPagina > 1 then
			Response.Write "<td align=right width=480>"
			Response.Write "<A HREF=""javascript:Carica_Pagina('" & nActPagina-1 & "')"">"
			Response.Write "<img border=0 src=" & Session("Progetto") & "/images/precedente.gif></A></td>"
		end if
		if nActPagina < nTotPagina then
			Response.Write "<td align=right>"
			Response.Write "<A HREF=""javascript:Carica_Pagina('" & nActPagina+1 & "')"">"		
			Response.Write "<img border=0 src=" & Session("Progetto") & "/images/successivo.gif></A></td>"
		end if
		Response.Write "</tr></TABLE>"
		
		%> 
		
		<br>
		<table border="0" width="500" cellpadding="0" cellspacing="0">
			<tr align="center">
				<td> 
					<%PlsLinkRosso "RDI_VisRegDip.asp", "Nuova ricerca"	%>
				</td>
			</tr>
		</table>
		<br>
<%
    End If

End Sub
';););););););););););););););););););););););););););););););););););););););)
'M A I N
%>
<!--#include virtual="/strutt_testa2.asp"-->
<!--#include virtual="/include/openconn.asp"-->
<!--#include virtual="/include/HTMLEncode.asp"-->
<!--#include virtual="/include/SetPulsanti.asp"-->
<%	
Inizio()
ImpostaPag()
%>
<!--#include virtual ="/include/closeconn.asp"-->
<!--#include virtual="/strutt_coda2.asp"-->
