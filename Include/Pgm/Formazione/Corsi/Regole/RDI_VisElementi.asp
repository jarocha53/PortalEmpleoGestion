<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->

<script language="JavaScript">
<!--#include virtual = "/Include/help.inc"-->

	function VaiModifica(){
		frmVisRegDip.action = "RDI_ModRegDip.asp"
		frmVisRegDip.submit()
	}

</script>
<%
Sub Inizio()
	dim sFunzione
	dim sTitolo
	dim sCommento
	dim bCampiObbl
	sHelp = "/Pgm/help/Formazione/Corsi/Regole/RDI_VisElementi"
	
	sFunzione = "STRUTTURA CORSO"
	sTitolo = "STRUTTURA CORSO"
	sCommento = "Elenco dei moduli del corso"
	bCampiObbl = false
%>
	<!--#include virtual="/include/SetTestata.asp"-->
<%
End sub
'------------------------------------------------------------------------------------------------------------------------------------------------------------------------

sub ImpRegole()

	dim aRegole
	dim i
	
	If not VerificaEsistenzaMod(sIdCorso) then
		%>
		<br>
		<table border="0" width="500" cellpadding="0" cellspacing="0">
		   <tr align="center">
		   	<td class="tbltext3"> 
		   		Non esistono Moduli per il corso
		   	</td>
		   </tr>
		</table>
		<%  
		exit Sub
	End If
	aRegole = CaricaRegoleDip(sIdCorso)
	if aRegole(0, 0) = "null" then
		Msgetto("Non esistono moduli associati")
	else%>
		<br>
		<table border="0" width="500">
			<tr class="sfondocomm"> 
			    <td class="tbllabel" align="left" valign="center" width="25%">
					<b>Modulo</b>
				</td>
				<td class="tbllabel" align="left" valign="center" width="10%">
					<b>Precedente</b>
				</td>
				<td class="tbllabel" align="left" valign="center" width="10%">
					<b>Successivo</b>
				</td>
				<td class="tbllabel" align="left" valign="center" width="55%">
					<b>Dipendenze</b>
				</td>
			</tr>

<%		for i = 0 to UBound(aRegole,2)
			if aRegole(1, i) <> "" then
				sSQL = "SELECT TITOLO_ELEMENTO FROM ELEMENTO WHERE ID_ELEMENTO = " & CLng(aRegole(1, i))
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
				set rsCorsoPrec = CC.Execute(sSQL)
				if not rsCorsoPrec.eof then
					sTitoloElemento = strHTMLEncode(rsCorsoPrec("TITOLO_ELEMENTO"))
				else
					sTitoloElemento = "Nessuno"
				end if
				rsCorsoPrec.Close
				set rsCorsoPrec = nothing
		
			end if
			if aRegole(6, i) <> "" then
				sSQL = "SELECT TITOLO_ELEMENTO FROM ELEMENTO WHERE ID_ELEMENTO = " & CLng(aRegole(6, i))
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
				set rsCorsoPrec = CC.Execute(sSQL)
				if not rsCorsoPrec.eof then
					sDescCorsoPrec = strHTMLEncode(rsCorsoPrec("TITOLO_ELEMENTO"))
				else
					sDescCorsoPrec = "Nessuno"
				end if
				rsCorsoPrec.Close
				set rsCorsoPrec = nothing
			else
					sDescCorsoPrec = "Nessuno"
			end if 

			if aRegole(7, i) <> "" then

				sSQL = "SELECT TITOLO_ELEMENTO FROM ELEMENTO WHERE ID_ELEMENTO = " & CLng(aRegole(7, i))
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
				set rsCorsoPrec = CC.Execute(sSQL)
				if not rsCorsoPrec.eof then
					sDescCorsoSucc = strHTMLEncode(rsCorsoPrec("TITOLO_ELEMENTO"))
				else
					sDescCorsoSucc = "Nessuno"
				end if
				rsCorsoPrec.Close
				set rsCorsoPrec = nothing
		
			else
				sDescCorsoSucc = "Nessuno"
			end if 
		
			if IsNull(aRegole(3, i)) then
				aRegole(3, i) = "Nessuna"
			end if
	
			%>

				<form method="post" name="frmVisRegDip<%=i%>" action="RDI_ModRegDip.asp">
					<input type="Hidden" name="IdCorso" value="<%=aRegole(0, i)%>|<%=sDescCorso%>">
					<input type="Hidden" name="cmbElemento" value="<%=aRegole(1, i)%>|<%=sTitoloElemento%>">
		
				</form>
				<tr class="tblsfondo">
					<td width="25%" align="left">
						<a class="tblAggFad" href="Javascript:frmVisRegDip<%=i%>.submit()" onmouseover="javascript:window.status=' '; return true">
						<%=sTitoloElemento%></a>
					</td>
					<td width="20%" align="left" class="tbldettFad">
						<%=sDescCorsoPrec%>
					</td>
					<td width="20%" align="left" class="tbldettFad">
						<%=sDescCorsoSucc%>
					</td>
					<td width="35%" align="left" class="tbldettFad">
						<%=aRegole(3, i)%>
					</td>
				</tr>
		<%next%>
 		</table>
		<br>
	<%end if%>

	<table border="0" width="500" cellpadding="0" cellspacing="0">
		<tr> 
			<td align="Center" nowrap><a href="RDI_VisRegDip.asp"><img src="<%=session("Progetto")%>/images/indietro.gif" border="0" name="imgPunto2" onmouseover="javascript:window.status=' '; return true"></a>
			</td>
		</tr>
	</table>	
<%end sub%>
<!--#include virtual="/strutt_testa2.asp"-->
<!--#include virtual="/include/openconn.asp"-->
<!--#include virtual="/include/HTMLEncode.asp"-->
<!--#include virtual="/include/SetPulsanti.asp"-->

<!--#include virtual="/include/FruizioneModuli.asp"-->

<%
dim sSQL
dim sIdCorso
dim sDescCorso
dim sIdElemento
dim sDescElemento
	
dim sAppo
	
sAppo	= Request("cmbCorso")
	
if trim(sAppo) <> "" then
	sIdCorso		= Mid(sAppo,1 ,InStr(1, sAppo, "|") - 1)
	sDescCorso		= Mid(sAppo, InStr(1, sAppo, "|") + 1)
end if
	
sAppo= ""
sAppo = Request("cmbElemento")

if trim(sAppo) <> "" then
	sIdElemento		= Mid(sAppo,1 ,InStr(1, sAppo, "|") - 1)
	sDescElemento	= Mid(sAppo, InStr(1, sAppo, "|") + 1)
end if

Inizio()
ImpCorsi
ImpRegole()
%>
<!--#include virtual ="/include/closeconn.asp"-->
<!--#include virtual="/strutt_coda2.asp"-->
<%

function CaricaElementi(sIdCorso)
	dim sSQL
	dim i
	dim rsElemento
	dim aOutArray
	dim nEle

	sSQL =	"SELECT A.ID_ELEMENTO, B.TITOLO_ELEMENTO " & _
			"FROM REGOLA_DIPENDENZA A, ELEMENTO B " &_ 
			"WHERE A.ID_CORSO = " & sIdCorso & " " & _
			"AND B.ID_ELEMENTO = A.ID_ELEMENTO"
	'Response.Write sSQL
	set rsElemento = Server.CreateObject("ADODB.Recordset")
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	rsElemento.open sSQL, CC, 3

	if rsElemento.EOF then
	   rsElemento.Close
	   set rsElemento = nothing
	   redim aOutArray (0,0)
	   aOutArray(0, 0) = "null"
	   CaricaElementi = aOutArray
	   exit function
	end if

	nEle = rsElemento.RecordCount	
	i = 0
	redim aOutArray(1, nEle - 1)
	do while not rsElemento.EOF
		aOutArray(0, i) = clng(rsElemento("ID_ELEMENTO"))
		aOutArray(1, i) = rsElemento("TITOLO_ELEMENTO")		
		i = i + 1
		rsElemento.MoveNext
	loop
	
	CaricaElementi = aOutArray

end function
function VerificaEsistenzaMod(sIdCorso)
	dim bVerifica
	bVerifica = false
	sSQL = "SELECT e.id_elemento, e.titolo_elemento, e.tipo_elemento, e.fl_erogazione " &_
			"FROM elemento e, regola_dipendenza r " &_
			"WHERE e.id_elemento = r.id_elemento and " &_
			"r.id_corso = " & sIdCorso
	set rsVerifica = Server.CreateObject("ADODB.Recordset")
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	rsVerifica.open sSQL, CC, 3
	If not rsVerifica.EOF Then
		bVerifica = true	
    End If
	rsVerifica.Close 
	set rsVerifica = Nothing
	VerificaEsistenzaMod = bVerifica
end function
function CaricaRegoleDip(sIdCorso)
	dim sSQL
	dim i
	dim rsRegola
	dim aOutArray
	dim nEle
	dim sDescCorso
	dim aElem
	
	'Response.Write sIdCorso
			
	sSQL =	"SELECT r.ID_CORSO, r.ID_ELEMENTO, r.ID_OBIETTIVO, r.DIPENDENZE, " & _
			"r.PUNTEGGIO, r.STATO_LEZIONE, r.STATO_OBIETTIVO, "& _
			"r.ELE_PREC, r.ELE_SUCC " &_
			"FROM REGOLA_DIPENDENZA r, ELEMENTO e " &_ 
			"WHERE e.ID_ELEMENTO = r.ID_ELEMENTO and ID_CORSO = " & sIdCorso & " ORDER BY ELE_PREC"
     
	set rsRegola = Server.CreateObject("ADODB.Recordset")
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	rsRegola.open sSQL, CC, 3

	if rsRegola.EOF then
	   rsRegola.Close
	   set rsRegola = nothing
	   redim aOutArray (0,0)
	   aOutArray(0, 0) = "null"
	   CaricaRegoleDip = aOutArray
	   exit function
	end if

	nEle = rsRegola.RecordCount	
	'Response.Write "**" & nele & "**"
	i = 0
	redim aOutArray(8, nEle - 1)

	redim aElem(nEle-1, 3)

	do while not rsRegola.EOF
		aOutArray(0, i) = clng(rsRegola("ID_CORSO"))
		aOutArray(1, i) = clng(rsRegola("ID_ELEMENTO"))
		aOutArray(2, i) = clng(rsRegola("ID_OBIETTIVO"))
		aOutArray(3, i) = rsRegola("DIPENDENZE")
		aOutArray(4, i) = rsRegola("PUNTEGGIO")
		aOutArray(5, i) = rsRegola("STATO_LEZIONE")
		aOutArray(6, i) = rsRegola("STATO_OBIETTIVO")
		if IsNull (rsRegola("ELE_PREC")) then
			aOutArray(6, i) = 0
		else
			aOutArray(6, i) = clng(rsRegola("ELE_PREC"))
		end if
		if IsNull (rsRegola("ELE_SUCC")) then
			aOutArray(7, i) = 0
		else
			aOutArray(7, i) = clng(rsRegola("ELE_SUCC"))
		end if
		aElem(i, 0) = aOutArray(6, i)
		aElem(i, 1) = aOutArray(7, i)
		aElem(i, 2) = clng(rsRegola("ID_ELEMENTO"))
		
		i = i + 1
		rsRegola.MoveNext
	loop

	sRisult = ordinaModuli(aElem, i)
	
	'Response.Write sRisult

	if InStr(sRisult,"#") <> 0 then
		if i > 1 then
			Msgetto("Sequenza dei moduli non corretta") 
		end if
	else
		aOutArray = Sort(aOutArray,sRisult)
	end if 
	
	CaricaRegoleDip = aOutArray

end function
sub ImpElementi()

	dim aElementi
	dim i

	aElementi = CaricaElementi(sIdCorso)
	if aElementi(0, 0) = "null" then
		Msgetto("Non ci sono Elementi")
		exit sub
	end if

	if trim(sIdElemento) <> "" then%>
		<input type="hidden" name="cmbElemento" value="<%=sIdElemento%>|<%=sDescElemento%>">
		<br>
		<table width="500" border="0" cellspacing="1" cellpadding="0">
			<tr class="tbltext1fad">
				<td align="left">
					<span class="tbltext1fad"><b>Elemento</b></span>
				</td>	
				<td align="left">
					<font color="red"><b><%=sDescElemento%></b></font>
				</td>
			</tr>
		</table>
	<%else%>
		<input type="hidden" name="Modo" value="2">
		<br>
		<table width="500" border="0" cellspacing="1" cellpadding="0">
			<tr>
				<td align="left">
					<span class="tbltext1fad"><b>Elemento</b></span>
				</td>	
				<td align="left">
	        		<select name="cmbElemento" onchange="JAVASCRIPT:Reload()">
					<%if sIdElemento = "" then	%>
						<option selected></option>
					<%else%>
						<option></option>
					<%end if		
							
					for i = 0 to ubound(aElementi, 2)
						Response.Write "<OPTION "
						if trim(sIdElemento) = trim(aElementi(0, i)) then 
							Response.Write ("Selected ")
						end if 
						Response.write "value ='" & aElementi(0, i) & "|" & aElementi(1, i) & _
						"'> " & aElementi(1, i) & "</OPTION>"
					next%>
					</select>
				</td>
			</tr>
		</table>
	<%end if
	
end sub
sub ImpCorsi()

	dim aCorsi
	dim i

	aCorsi = CaricaCorsi()
	if aCorsi(0, 0) = "null" then
		Msgetto("Non ci sono Corsi")
		exit sub
	end if
	if trim(sIdCorso) <> "" then%>
		<br>
		<input type="hidden" name="cmbCorso" value="<%=sIdCorso%>|<%=sDescCorso%>">
		<table width="500" border="0" cellspacing="1" cellpadding="0">
			<tr class="tbltext1fad">
				<td align="left">
					<span class="tbltext1fad"><b>Corso&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></span>
				</td>	
				<td align="left" class="textBlack">
					<b><%=sDescCorso%></b>
				</td>
			</tr>
		</table>
	<%else%>
		<input type="hidden" name="Modo" value="1">
		<br>
		<table width="500" border="0" cellspacing="1" cellpadding="0">
			<tr>
				<td align="left">
					<span class="tbltext1fad"><b>Corso&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></span>
				</td>	
				<td align="left">
		    		<select name="cmbCorso" onchange="JAVASCRIPT:Reload()">
					<%if sIdCorso = "" then	%>
						<option selected></option>
					<%else%>
						<option></option>
					<%end if		
					for i = 0 to ubound(aCorsi, 2)
						Response.Write "<OPTION "
						if trim(sIdCorso) = trim(aCorsi(0, i)) then 
							Response.Write ("Selected ")
						end if 
						Response.write "value ='" & aCorsi(0, i) & "|" & aCorsi(1, i) & _
						"'> " & aCorsi(1, i) & "</OPTION>"
					next%>
					</select>
				</td>
			</tr>
		</table>
	<%end if

end sub

function CaricaCorsi()
	dim sSQL
	dim i
	dim rsCorso
	dim aOutArray
	dim nEle

	sSQL =	"SELECT DISTINCT(A.ID_CORSO), B.DESC_CORSO " & _
			"FROM REGOLA_DIPENDENZA A, CORSO B " &_
			"WHERE A.ID_CORSO = B.ID_CORSO" 
    
    'Response.Write sSQL
	
	set rsCorso = Server.CreateObject("ADODB.Recordset")
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	rsCorso.open sSQL, CC, 3
	if rsCorso.EOF then
	   rsCorso.Close
	   set rsCorso = nothing
	   redim aOutArray (0,0)
	   aOutArray(0, 0) = "null"
	   CaricaCorsi = aOutArray
	   exit function
	end if
	nEle = rsCorso.RecordCount
	i = 0
	redim aOutArray(1, nEle - 1)
	do while not rsCorso.EOF
		aOutArray(0, i) = clng(rsCorso("ID_CORSO"))
		aOutArray(1, i) = strHTMLEncode(rsCorso("DESC_CORSO"))
		i = i + 1
		rsCorso.MoveNext
	loop
	CaricaCorsi = aOutArray

end function

sub Msgetto(sMessaggio)%>
	<br>
	    <table width="500">
			<tr><td class="tbltext3" align="center"><b><%=sMessaggio%></b></td></tr>	
		</table>
	<br>		
<%end sub%>

