<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%
Sub Inizio()
%>
<html>
<head>
	<link REL="STYLESHEET" TYPE="text/css" HREF="<%=session("progetto")%>/fogliostile.css">
	<meta NAME="GENERATOR" Content="Microsoft Visual Studio 6.0">
	<title>Selezione Questionario</title>	
</head>
<script LANGUAGE="JavaScript">
function InviaSelezione(nIdQuest, sDescQuest)
{
	
	opener.document.FormInserisciElementi.cmbQuestionario.value = sDescQuest
	opener.document.FormInserisciElementi.hIdQuest.value = nIdQuest
	self.close()
}
</script>
<body>
<p align="center">
<%
	dim sFunzione
	dim sTitolo
	dim sCommento
	dim bCampiObbl
	
	sFunzione = "GESTIONE CORSI"
	sTitolo = "SELEZIONE DEL QUESTIONARIO"
	sCommento = "Selezionare il Questionario da associare all'Elemento oppure " &_
				"fare click su " & chr(34) & "Chiudi" & chr(34)
	bCampiObbl = false
%>
	<!--#include virtual="/include/SetTestata.asp"-->
<%
End Sub
'------------------------------------------------------------------------
Sub Fine()
%>
<br>
<table width="500">
	<tr>
		<td align="center">
			<a href="javascript:window.close()">
				<img src="<%=Session("progetto")%>/images/chiudi.gif" border="0" onmouseover="javascript:window.status='' ; return true">
			</a>
		</td>
	</tr>
</table>
</p>		
</body>
</html>
<%
End Sub
'------------------------------------------------------------------------
Sub ImpostaPag()
dim sSQL, rsQuest

sSQL = "Select ID_QUESTIONARIO, TIT_QUESTIONARIO, RowNum from QUESTIONARIO " &_
	"WHERE (FL_PUBBLICO=0 OR ID_PROJOWNER='" & mid(session("progetto"),2) & "')" 
Set rsQuest = Server.CreateObject("ADODB.Recordset")
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
rsQuest.Open sSQL, CC, adOpenForwardOnly
%>
<table border="0" cellspacing="2" cellpadding="1" width="100%">
	<tr>
		<td align="center">
			<a onmouseover="javascript:window.status='' ; return true" href="javascript:InviaSelezione('', '')" class="textred">
				<b>Rimuovi Selezione</b>
			</a>
		</td>
	</tr>	
</table>
<br>
<table border="0" cellspacing="2" cellpadding="1" width="100%">
<%
do while not rsQuest.eof
dim bPari
'	Response.Write rsQuest("rownum")
'	Response.Write CInt(rsQuest("rownum")) mod 2
'	Response.Write "<br>"
	if CInt(rsQuest("rownum")) mod 2 = 1 then
		bPari = false
%>
	<tr height="20">
		<td class="tblsfondo" width="50%">
			<a href="javascript:InviaSelezione(<%=rsQuest("ID_QUESTIONARIO")%>, '<%=rsQuest("TIT_QUESTIONARIO")%>')" class="textblack">
				<b><%=rsQuest("TIT_QUESTIONARIO")%></b>
			</a>
		</td>
<%
	else
		bPari = true
%>
		<td class="tblsfondo" width="50%">
			<a href="javascript:InviaSelezione(<%=rsQuest("ID_QUESTIONARIO")%>, '<%=rsQuest("TIT_QUESTIONARIO")%>')" class="textblack">
				<b><%=rsQuest("TIT_QUESTIONARIO")%></b>
			</a>
		</td>
	</tr>
<%	
	end if
	rsQuest.MoveNext
	if rsQuest.eof then
		if bPari = false then
%>
		<td class="tblsfondo" width="50%">
			<span class="textblack">
				&nbsp;
			</span>
		</td>
	</tr>		
<%
		end if
	end if
loop
%>
</table>
<%
rsQuest.Close
set rsQuest = nothing
End Sub
'------------------------------------------------------------------------
'M A I N
%>
<!--#include Virtual ="/include/openconn.asp"-->
<%
Inizio()
ImpostaPag()
%>	
<!--#include Virtual ="/include/closeconn.asp"-->
<%
Fine()
%>
