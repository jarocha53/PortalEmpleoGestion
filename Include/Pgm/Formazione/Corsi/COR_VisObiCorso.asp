<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<script LANGUAGE="JavaScript">
var windowArea
function Dettaglio(IdObi)
{	if (windowArea!=null)
	{
		windowArea.close()
	}
	windowArea = window.open ('COR_VisDettObi.asp?UIdObi=' + IdObi ,'info','width=400,height=400,scrollbars=1,Top=0,Left=0')
}	
</script>
<%

'-------------------------------------------------------------------------------
Sub Inizio
	dim sFunzione
	dim sTitolo
	dim sCommento
	dim bCampiObbl
	
	sTitolo = "GESTIONE CORSO"
	sCommento = "Elenco degli Obiettivi"
	bCampiObbl = false
%>
	<!--#include virtual="/include/SetTestata.asp"-->
<%
End sub
'-------------------------------------------------------------------------------
Sub ImpostaPag
	dim iIdCorso,sTitCorso
	dim sDescObi
	dim	sDescObiInEle'� la descrizione da visualizzare nell'elenco, � troncata a 60 caratteri
	
	iIdCorso=Request.Form("hIdCorso")
	sTitCorso=Request.Form("hTitCorso")
	
	'iIdCorso=1
	'sTitCorso="Corso Mikele"
	
	'Response.Write "iIdCorso=" & iIdCorso & "<br>"
	'Response.Write "sTitCorso=" & sTitCorso & "<br>"
	
%>
	<table border="0" width="500">
		<tr>
			<td class="tbltext1BFad" width="20%">Corso
			</td>
			<td class="tbltextBFad" width="80%"><%=strHTMLEncode(sTitCorso)%> 
			</td>
		</tr>
	</table>
	<br>

<%	Sql =		 " SELECT"
	Sql = Sql &  "  ID_OBIETTIVO,"
	Sql = Sql &  "  DESC_OBIETTIVO,"
	Sql = Sql &  "  DT_TMST"
	Sql = Sql &  " FROM"
	Sql = Sql &  "  OBIETTIVO"
	Sql = Sql &  " WHERE"
	Sql = Sql &  "  ID_CORSO=" & iIdCorso
	Sql = Sql &  "ORDER BY DESC_OBIETTIVO"
	'Response.Write Sql & "<br>"

	'Response.End
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
	set Rst=CC.execute (Sql)
	if not Rst.eof then
%>	
		<table border="0" width="500">
			<tr class="sfondoCommBFad">
				<td width="1%">Dettaglio</td>
				<td>Descrizione
				</td>
			</tr>
<%		i=0
		do while not Rst.eof
			i=i+1
			sDescObi=Rst.fields("DESC_OBIETTIVO")
			'Response.Write Len(sDescObi)
			if Len(sDescObi)>60 then
				sDescObiInEle=Left(sDescObi,60) & "..."
			else
				sDescObiInEle=sDescObi
			end if
%>			
			<form name="frmObiettivo<%=i%>" id="frmObiettivo<%=i%>" action="COR_ModDatObi.asp" method="post">
			<tr class="tblSfondoFad">
				<td align="center">
					<a title="Visualizza dettaglio obiettivo" href="Javascript:Dettaglio(<%=Rst.fields("ID_OBIETTIVO")%>)">
					<img border="0" src="<%=Session("progetto")%>/images/bullet1.gif"></a>
				</td>
					<td>
						<a class="tblAggFad" href="JavaScript:document.frmObiettivo<%=i%>.submit();">
							<%=strHTMLEncode(sDescObiInEle)%>
						</a>
					</td>
					<input type="hidden" name="hIdCorso" id="hIdCorso" value="<%=iIdCorso%>">
					<input type="hidden" name="hTitCorso" id="hTitCorso" value="<%=sTitCorso%>">
					<input type="hidden" name="hIdObi" id="hIdObi" value="<%=Rst.fields("ID_OBIETTIVO")%>">
					<input type="hidden" name="hDescObi" id="hDescObi" value="<%=Replace(Rst.fields("DESC_OBIETTIVO"),"""","�")%>">
					<input type="hidden" name="hTmst" id="hTmst" value="<%=Rst.fields("DT_TMST")%>">
			</tr>
			</form>
			
<%			Rst.MoveNext
		loop
%>		</table>
<%	else
%>		<br><br><br>
		<span class="tbltext3">
			Non vi sono obiettivi.
		</span><br><br><br>
<%	end if
	Rst.Close
	set Rst=nothing	
	
%>	<br><br>
	<form name="frmInsObi" id="frmInsObi" action="COR_InsObi.asp" method="post">
		<input type="hidden" name="hIdCorso" id="hIdCorso" value="<%=iIdCorso%>">
		<input type="hidden" name="hTitCorso" id="hTitCorso" value="<%=sTitCorso%>">
		<a href="JavaScript:document.frmInsObi.submit();" class="textred">
			<b>Inserisci obiettivo</b></a>
	</form>
	<br><br>
	<form name="frmIndietro" id="frmIndietro" action="COR_ModCorsi.asp" method="post">
		<input type="hidden" name="IDCorso" id="IDCorso" value="<%=iIdCorso%>">
		<input type="image" src="<%=session("progetto")%>/images/indietro.gif">
	</form>
<%
end sub

'-------------------------------------------------------------------------------------
'M A I N
%>
<!--#include virtual="/strutt_testa2.asp"-->
<!--#include virtual="/include/openconn.asp"-->
<!--#include virtual="/include/HTMLEncode.asp"-->
<center>
<%
	call Inizio
	call ImpostaPag
%>
</center>
<!--#include virtual="/include/closeconn.asp"-->
<!--#include virtual="/strutt_coda2.asp"-->
