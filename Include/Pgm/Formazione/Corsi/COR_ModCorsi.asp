<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%
Sub ControlliJavaScript()
%>
<script language="Javascript">
	
	<!--#include virtual="/include/ControlDate.inc"-->
	<!--#include virtual="/include/ControlNum.inc"-->
	<!--#include virtual="/include/ControlString.inc"-->
function OreMinuti(valore)
{
	if (valore.substr(2,1) != ":")
	{
		return false;
	}
	if (isNaN(valore.substr(0,2)))
	{
		return false;
	}
	if (isNaN(valore.substr(3,2)))
	{
		return false;
	}
	if (valore.substr(3,2) > 60)
	{
		//alert ('ma de che')
		return false;
	}
	return true;	
}
		
function FormRegistra_Validator(frmCorso)
{
	if (frmCorso.cmbArea.value == "")
		{
		frmCorso.cmbArea.focus();
		alert("Il campo 'Area' � obbligatorio.");
		return (false);
	  	}
		
	if (frmCorso.txtTitoloCorso.value == "")
		{
		frmCorso.txtTitoloCorso.focus();
		alert("Il campo 'Titolo Corso' � obbligatorio.");
		return (false);
	  	}

	if (frmCorso.txtDescrizione.value == "")
		{
		frmCorso.txtDescrizione.focus();
		alert("Il campo 'Descrizione' � obbligatorio.");
		return (false);
	  	}

	if (frmCorso.cboFlErogazione.value == "")
		{
		frmCorso.cboFlErogazione.focus();
		alert("Il campo 'Modalit� di erogazione' � obbligatorio.");
		return (false);
	  	}

	if (frmCorso.txtDurata.value != "")
		{
		if (OreMinuti(frmCorso.txtDurata.value) == false)
			{
				frmCorso.txtDurata.focus();
				alert("Il campo 'Durata corso' deve essere in formato 'hh:mm'.");
				return (false);				
			}
		}

	if (frmCorso.chkDispo.checked == false)
	{
		if (!ValidateInputDate(frmCorso.txtDisponibilitaDal.value))
			{
				frmCorso.txtDisponibilitaDal.focus();
				return (false);
			}
	}	

	if (frmCorso.cboFlPubblico.value == "")
		    {
	 	       frmCorso.cboFlPubblico.focus();
		       alert("Il campo 'Pubblico/Privato' � obbligatorio.");
		       return (false);
		}			
    
	if (frmCorso.ChkCorsoAICC.checked==true)
	{
		
		if (frmCorso.txtInfoProd.value == "")
			{
			frmCorso.txtInfoProd.focus();
			alert("Il campo 'Informazioni Produttore' � obbligatorio.");
			return (false);
			}
		
		
		if (frmCorso.txtVerCorsoAICC.value=="")
			{
			frmCorso.txtVerCorsoAICC.focus();
			alert("Il campo 'Versione' � obbligatorio.");
			return (false);
			}

		//if (frmCorso.txtAuthoring.value == "")
		//	{
		//	frmCorso.txtAuthoring.focus();
		//	alert("Il campo 'Authoring' � obbligatorio.");
		//	return (false);
		//	}


	    if (frmCorso.cboLivCorso.value == "")
			{
			frmCorso.cboLivCorso.focus();
			alert("Il campo 'Livello Corso' � obbligatorio.");
			return (false);
		  	}

		if (frmCorso.txtNumCredCorso.value == "")
			{
			frmCorso.txtNumCredCorso.focus();
			alert("Il campo 'Crediti corso' � obbligatorio.");
			return (false);				
			}
		else
			{
			if (isNaN(frmCorso.txtNumCredCorso.value))
				{
					frmCorso.txtNumCredCorso.focus();
					alert("Il campo 'Crediti corso' � numerico.");
					return (false);				
				}
			}
		
	}
	return (true);

}
		

function Elimina(id,dt)	
{
	if (confirm("Conferma eliminazione del corso?"))
		{
			location.href = 'COR_CnfCorsi.asp?Oper=Can&IDCorso=' + id + '&dtTmst=' + dt
		}
}

function ImpFlag()
{
	//alert(FormInserisciCorso.chkDispo.checked);
	if (FormInserisci.chkDispo.checked == true)
		{
			FormInserisci.txtDisponibilitaDal.disabled = true
		}
	else
		{
			FormInserisci.txtDisponibilitaDal.disabled = false
			FormInserisci.txtDisponibilitaDal.focus()
		}
}
function ImpFlag2()
{
/*	if (FormInserisci.ChkCorsoAICC.checked == true)
		{
			FormInserisci.txtVerCorsoAICC.disabled = false
		}
	else
		{
			FormInserisci.txtVerCorsoAICC.value = ""
			FormInserisci.txtVerCorsoAICC.disabled = true
		}*/
	if (FormInserisci.ChkCorsoAICC.checked == true)
		{
			FormInserisci.txtVerCorsoAICC.disabled = false
			FormInserisci.cboLivCorso.disabled = false
			FormInserisci.txtNumCredCorso.disabled = false
			//FormInserisci.txtAuthoring.disabled = false
			FormInserisci.txtInfoProd.disabled = false
			document.all.VisObi.style.visibility="visible"
		}
	else
		{
			FormInserisci.txtVerCorsoAICC.disabled = true
			FormInserisci.cboLivCorso.disabled = true
			FormInserisci.txtNumCredCorso.disabled = true
			//FormInserisci.txtAuthoring.disabled = true
			//FormInserisci.txtInfoProd.disabled = true
			document.all.VisObi.style.visibility="hidden"
		}

}
		// end hiding -->
</script>
<%
End sub
';););););););););););););););););););););););););););););););););););););););)
Sub Testata()
	dim sFunzione
	dim sTitolo
	dim sCommento
	dim bCampiObbl
	
	sFunzione = "GESTIONE CORSI"
	sTitolo = "MODIFICA/ELIMINA CORSO"
	sCommento = "Compilare i campi sottostanti e premere il pulsante <b>Invia</b> per memorizzare le informazioni. Per eliminare il corso premere il pulsante <b>Elimina</b>"
	bCampiObbl = true
	sHelp = "/Pgm/help/Formazione/Corsi/COR_ModCorsi/"	
%>
	<!--#include virtual="/include/SetTestata.asp"-->
<%
End Sub
';););););););););););););););););););););););););););););););););););););););)
Function ConvHHMM(nMin)
dim nAppHH
dim nAppMM
	if nMin > "" then
		nAppHH = (cint(nMin) \ 60)											'Estraggo il numero delle ore
		nAppMM = (cint(nMin) Mod 60)										'Estraggo il numero dei minuti
		ConvHHMM = Right("0" & nAppHH, 2) & ":" & Right("0" & nAppMM, 2)	'Aggiungo uno zero per mantenere il formato HH:MM
	else
		ConvHHMM = ""
	end if
End Function
';););););););););););););););););););););););););););););););););););););););)
Sub PaginaNonModificabile(RR)
dim sMess
%>
	<table border="0" cellspacing="2" cellpadding="2" width="500">
		<tr> 
			<td align="left" nowrap class="tbltext1" width=150>
				<b>Area</b>
			</td>
			<td class="textblack" width=350>
				<b><%=strHTMLEncode(RR("TitoloArea"))%></b>
			</td>
		</tr>
	    <tr> 
			<td align="left" nowrap class="tbltext1">
				<b>Titolo Corso</b>
			</td>
			<td class="textblack">
				<b><%=strHTMLEncode(RR("TIT_CORSO"))%></b>
			</td>
	    </tr>
	    <tr> 
			<td align="left" nowrap class="tbltext1">
				<b>Descrizione</b>
			</td>
			<td class="textblack">
				<b><%=strHTMLEncode(RR("DESC_CORSO"))%></b>
			</td>
	    </tr>
	    <tr> 
			<td align="left" nowrap class="tbltext1">
				<b>Obiettivo</b>
			</td>
			<td class="textblack"> 
				<%=strHTMLEncode(RR("OBIETTIVO_CORSO"))%>
			</td>
	    </tr>
	    <tr> 
			<td align="left" nowrap class="tbltext1">
				<b>Modalit� di Erogazione</b>
			</td>
			<td class="textblack"> 
				<%
				select case cint(RR("FL_EROGAZIONE"))
					case 0
						sMess = "In presenza"
					case 1
						sMess = "A distanza"
					case else
						sMess = "Errore!!!"
				end select
				Response.Write sMess
				%>
			</td>
	    </tr>
	    <tr> 
			<td align="left" nowrap class="tbltext1">
				<b>Durata corso</b>&nbsp;(hh:mm)
			</td>
			<td class="textblack">
				<%=ConvHHMM(RR("DURATA_CORSO"))%>
			</td>
	    </tr>
	    <tr> 
			<td align="left" nowrap class="tbltext1">
				<b>Disponibilit� dal</b>
			</td>
			<td class="textblack"> 
				<b><%=RR("DT_DISPONIBILE")%></b>
			</td>
	    </tr>
	    <tr> 
			<td align="left" nowrap class="tbltext1">
				<b>Progetto proprietario</b>
			</td>
			<td class="textblack">
				<b><%=DecCodVal("CPROJ", 0, date(), RR("ID_PROJOWNER"),1)%></b>
			</td>
	    </tr>
	    <tr> 
			<td align="left" nowrap class="tbltext1">
				<b>Pubblico/Privato</b>
			</td>
			<td class="textblack"> 
				<%
				select case cint(RR("FL_PUBBLICO"))
					case 0
						sMess = "Pubblico"
					case 1
						sMess = "Privato"
					case else
						sMess = "Errore!!!"
				end select
				Response.Write sMess
				%>
			</td>
	    </tr>
	    <tr> 
			<td align="left" nowrap class="tbltext1">
				<b>Prerequisiti necessari</b>
			</td>
			<td class="textblack">
				<%=strHTMLEncode(RR("PREREQ_CORSO"))%>
			</td>
	    </tr>
	    <tr> 
			<td align="left" nowrap class="tbltext1">
				<b>Destinatari del Corso</b>
			</td>
			<td class="textblack"> 
				<%=strHTMLEncode(RR("DESTINATARI_CORSO"))%>
			</td>
	    </tr>
	    <tr> 
			<td align="left" nowrap class="tbltext1">
				<b>Authoring</b>
			</td>
			<td class="textblack">
				<%=strHTMLEncode(RR("AUTHORING_CORSO"))%>
			</td>
	    </tr>
	    <tr> 
			<td align="left" nowrap class="tbltext1">
				<b>Informazioni Produttore</b>
			</td>
			<td class="textblack">
				<%=strHTMLEncode(RR("INFO_PRODUTTORE"))%>
			</td>
	    </tr>
    <%
	if RR("VER_CORSOAICC") > "" then 
	%>
		<tr><td colspan=2><hr></td></tr>
	    <tr> 
			<td align="left" nowrap class="tbltext1">
				<b>Corso AICC</b>
			</td>
			<td class="textblack">
				<b>Si</b>
			</td>
	    </tr>
	    <tr> 
			<td align="left" nowrap class="tbltext1">
				<b>Versione</b>
			</td>
			<td class="textblack">
				<%=RR("VER_CORSOAICC")%>
			</td>
	    </tr>
	    <tr> 
			<td align="left" nowrap class="tbltext1">
				<b>Livello Corso</b>
			</td>
			<td class="textblack">
				<%=RR("LIV_CORSO")%>
			</td>
	    </tr>
	    <tr> 
			<td align="left" nowrap class="tbltext1">
				<b>Crediti corso</b>
			</td>
			<td class="textblack">
				<%=RR("NUMCREDIT_CORSO")%>
			</td>
	    </tr>
	    <!--<tr> 
			<td align="left" nowrap class="tbltext1">
				<b>Authoring</b>
			</td>
			<td class="textblack">
				<%=strHTMLEncode(RR("AUTHORING_CORSO"))%>
			</td>
	    </tr>
	    <tr> 
			<td align="left" nowrap class="tbltext1">
				<b>Informazioni Produttore</b>
			</td>
			<td class="textblack">
				<%=strHTMLEncode(RR("INFO_PRODUTTORE"))%>
			</td>
	    </tr>-->
	<%
	end if
	%>
	</table>
	<br>
	<table width="500" border="0" cellspacing="2" cellpadding="1">
		<tr align="center">
			<td>
				<%
					PlsIndietro()
				%>
			</td>
		</tr>
	</table> 
	<br>

<%
End Sub
';););););););););););););););););););););););););););););););););););););););)
Sub PaginaModificabile(RR)
%>
	<form method="POST" action="COR_CnfCorsi.asp" onsubmit="return FormRegistra_Validator(this)" name="FormInserisci">
	<input type=hidden name="Oper" id="Oper" value="Mod">
	<input type=hidden name="IDCorso" id="IDCorso" value="<%=nIDCorso%>">
	<input type=hidden name="dtTmst" id="dtTmst" value="<%=RR("DT_TMST")%>">
	<table border="0" cellspacing="2" cellpadding="2" width="500">
		<tr> 
			<td align="left" nowrap class="tbltext1">
				<b>Area</b>*
			</td>
			<td class="textblack">
<%					
					sql="SELECT DISTINCT TitoloArea, ID_AREA FROM AREA ORDER BY TitoloArea"
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
					SET RR1=CC.EXECUTE (sql)
%>					
					<select name="cmbArea" id="cmbArea" class="textblack"> 						
						<option VALUE></option>
						<% 
							DO WHILE NOT RR1.EOF 
								if cint(RR("ID_AREATEMATICA")) = cint(RR1("ID_AREA")) then
									sFlag = "selected"
								else
									sFlag = ""
								end if
								Response.Write "<option value=" & chr(34) & RR1.FIELDS("ID_AREA") & chr(34) & " " & sFlag & ">"
								Response.Write RR1.FIELDS("TitoloArea")
								Response.Write "</option>"
								RR1.MOVENEXT
							LOOP
%>						
					</select>
<%					
					RR1.CLOSE
%>		
				<input type="hidden" name="hCodArea" value="<%=RR("ID_AREATEMATICA")%>"> 
			</td>
		</tr>
	    <tr> 
			<td align="left" nowrap class="tbltext1">
				<b>Titolo Corso*</b>
			</td>
			<td>
				<input type="text" name="txtTitoloCorso" class="textblacka" size="50" id="txtTitoloCorso" value="<%=strHTMLEncode(RR("TIT_CORSO"))%>" maxlength="100">
			</td>
	    </tr>
	    <tr> 
			<td align="left" nowrap class="tbltext1">
				<b>Descrizione*</b>
			</td>
			<td>
				<span class="tbltext1">&nbsp;- Utilizzabili <label name="lblNumCar1" id="lblNumCar1">254</label> caratteri -</span>
				<textarea name="txtDescrizione" class="textblack" cols="50" rows="3" OnKeyUp="JavaScript:CheckLenTextArea(txtDescrizione,lblNumCar1,254)"><%=strHTMLEncode(RR("DESC_CORSO"))%></textarea>
			</td>
	    </tr>
	    <tr> 
			<td align="left" nowrap class="tbltext1">
				<b>Obiettivo</b>
			</td>
			<td> 
				<span class="tbltext1">&nbsp;- Utilizzabili <label name="lblNumCar2" id="lblNumCar2">254</label> caratteri -</span>
				<textarea name="txtObiettivo" class="textblack" cols="50" rows="3" OnKeyUp="JavaScript:CheckLenTextArea(txtObiettivo,lblNumCar2,254)"><%=strHTMLEncode(RR("OBIETTIVO_CORSO"))%></textarea>
			</td>
	    </tr>
	    <tr> 
			<td align="left" nowrap class="tbltext1">
				<b>Modalit� di Erogazione*</b>
			</td>
			<td> 
				<select name="cboFlErogazione" id="cboFlErogazione" class=textblack>
					<option value=""></option>
					<%
					if cint(RR("FL_EROGAZIONE")) = 0 then
						Response.Write "<option value='0' selected>In presenza</option>"
					else
						Response.Write "<option value='0'>In presenza</option>"
					end if
					if cint(RR("FL_EROGAZIONE")) = 1 then
						Response.Write "<option value='1' selected>A distanza</option>"
					else
						Response.Write "<option value='1'>A distanza</option>"
					end if
					%>
				</select>
			</td>
	    </tr>
	    <tr> 
			<td align="left" nowrap class="tbltext1">
				<b>Durata corso</b>&nbsp;(hh:mm)
			</td>
			<td> 
				<input name="txtDurata" type="text" class="textblacka" size=10 value="<%=ConvHHMM(RR("DURATA_CORSO"))%>">
			</td>
	    </tr>
	    <tr> 
			<td align="left" nowrap class="tbltext1">
				<b>Disponibilit� dal*</b>
			</td>
			<td class=tbltext1> 
				<%
				if RR("DT_DISPONIBILE") = "31/12/9999" then
				%>
				<input name="txtDisponibilitaDal" type="text" class="textblacka" size="10" maxlength=10 disabled>
				&nbsp;&nbsp;&nbsp;
				<input type="checkbox" name="chkDispo" id="chkDispo" onclick="javascript:ImpFlag()" checked>
				(non disponibile)
				<%
				else
				%>
				<input name="txtDisponibilitaDal" type="text" class="textblacka" size="10" maxlength=10 value="<%=RR("DT_DISPONIBILE")%>">
				&nbsp;&nbsp;&nbsp;
				<input type="checkbox" name="chkDispo" id="chkDispo" onclick="javascript:ImpFlag()">
				(non disponibile)
				<%
				end if
				%>
			</td>
	    </tr>
	    <tr height=20> 
			<td align="left" nowrap class="tbltext1">
				<b>Progetto proprietario</b>
			</td>
			<td class=textblack>
				<b>
				<%=DecCodVal("CPROJ", "0", date, RR("ID_PROJOWNER"), 1)%>
				</b>
			</td>
			<div id=dHideCombo1 style="visibility:hidden">
			<%
				CreateCombo("CPROJ|||" & RR("ID_PROJOWNER") & "|cboProjOwn||")
			%>
			</div>
	    </tr>
		<tr height=20> 
			<td align="left" nowrap class="tbltext1">
				<b>Pubblico/Privato</b>
			</td>
			<td class=textblack> 
				<b>
				<%
				select case cint(RR("FL_PUBBLICO"))
					case 0
						sMess = "Pubblico"
					case 1
						sMess = "Privato"
				end select
				Response.Write sMess
				%>
				</b>
			</td>
			<div id=dHideCombo2 style="visibility:hidden">
				<select name="cboFlPubblico" id="cboFlPubblico" class=textblack>
					<option value=""></option>
					<%
					if cint(RR("FL_PUBBLICO")) = 0 then
						Response.Write "<option value='0' selected>Pubblico</option>"
					else
						Response.Write "<option value='0'>Pubblico</option>"
					end if
					if cint(RR("FL_PUBBLICO")) = 1 then
						Response.Write "<option value='1' selected>Privato</option>"
					else
						Response.Write "<option value='1'>Privato</option>"
					end if
					%>
				</select>
			</div>
		</tr>
	    <tr> 
			<td align="left" nowrap class="tbltext1">
				<b>Prerequisiti necessari</b>
			</td>
			<td>
				<span class="tbltext1">&nbsp;- Utilizzabili <label name="lblNumCar4" id="lblNumCar4">2000</label> caratteri -</span>
				<textarea name="txtPrereqCorso" class="textblack" cols="50" rows="3" OnKeyUp="JavaScript:CheckLenTextArea(txtPrereqCorso,lblNumCar4,2000)"><%=RR("PREREQ_CORSO")%></textarea>
			</td>
	    </tr>
	    <tr> 
			<td align="left" nowrap class="tbltext1">
				<b>Destinatari del Corso</b>
			</td>
			<td>
				<span class="tbltext1">&nbsp;- Utilizzabili <label name="lblNumCar5" id="lblNumCar5">2000</label> caratteri -</span> 
				<textarea name="txtDestinatari" class="textblack" cols="50" rows="3" OnKeyUp="JavaScript:CheckLenTextArea(txtDestinatari,lblNumCar5,2000)"><%=RR("DESTINATARI_CORSO")%></textarea>
			</td>
	    </tr>
		<tr> 
			<td align="left" nowrap class="tbltext1">
				<b>Authoring</b>
			</td> 
			<td> 
				<span class="tbltext1">&nbsp;- Utilizzabili <label name="lblNumCar7" id="lblNumCar7">254</label> caratteri -</span>
				<textarea name="txtAuthoring" class="textblack" cols="50" rows="3" OnKeyUp="JavaScript:CheckLenTextArea(txtAuthoring,lblNumCar7,254)" ><%=RR("AUTHORING_CORSO")%></textarea>
			</td>
	    </tr>
		
		<tr> 
			<td align="left" nowrap class="tbltext1">
				<b>Informazioni<br>Produttore*</b>
			</td>
			<td> 
				<span class="tbltext1">&nbsp;- Utilizzabili <label name="lblNumCar3" id="lblNumCar3">2000</label> caratteri -</span>
				<textarea name="txtInfoProd" class="textblack" cols="50" rows="3" OnKeyUp="JavaScript:CheckLenTextArea(txtInfoProd,lblNumCar3,2000)" ><%=RR("INFO_PRODUTTORE")%></textarea>
			</td>
	    </tr>
				
		<tr><td colspan=2><hr></td></tr>
	    <tr> 
			<td align="left" nowrap class="tbltext1">
				<b>Corso AICC</b>
			</td>
			<td class="tbltext1">
				<%
				if RR("VER_CORSOAICC") > "" then
					bCheck = " checked"
					bText = ""
					bVisible = "visible"
				else
					bCheck = ""
					bText = " disabled"
					bVisible = "hidden"
				end if
				%>
				<input name="ChkCorsoAICC" type="checkbox" class="textblacka" onclick="javascript:ImpFlag2()" <%=bCheck%>>
			</td>
	    </tr>
	    <tr> 
			<td align="left" nowrap class="tbltext1">
				<b>Versione*</b>
			</td>
			<td align="left" nowrap class="tbltext1">
				<input name="txtVerCorsoAICC" size="3" maxlength="3" type="text" class="textblacka" value="<%=RR("VER_CORSOAICC")%>" <%=bText%>>
			</td>
	    </tr>
	    <tr> 
			<td align="left" nowrap class="tbltext1">
				<b>Livello Corso*</b>
			</td>
			<td> 
				<select name="cboLivCorso" id="cboLivCorso" class=textblack <%=bText%>>
					<option value=""></option>
				<%
				select case RR("LIV_CORSO") 
					case 1
					%>
						<option value=""></option>
						<option value="1" selected>1</option>
						<option value="2">2</option>
						<option value="3">3</option>
					<%	
					case 2
					%>
						<option value=""></option>
						<option value="1">1</option>
						<option value="2" selected>2</option>
						<option value="3">3</option>
					<%	
					case 3
					%>
						<option value=""></option>
						<option value="1">1</option>
						<option value="2">2</option>
						<option value="3" selected>3</option>
					<%	
					case else
					%>
						<option value="" selected></option>
						<option value="1">1</option>
						<option value="2">2</option>
						<option value="3">3</option>
					<%	
				end select
				%>
				</select>
			</td>
	    </tr>
	    <tr> 
			<td align="left" nowrap class="tbltext1">
				<b>Crediti corso*</b>
			</td>
			<td> 
				<input name="txtNumCredCorso" type="text" maxlength="3" size=3 class="textblacka" value="<%=RR("NUMCREDIT_CORSO")%>" <%=bText%>>
			</td>
	    </tr>
	    <!--<tr> 
			<td align="left" nowrap class="tbltext1">
				<b>Authoring</b>
			</td>
			<td> 
				<span class="tbltext1">&nbsp;- Utilizzabili <label name="lblNumCar7" id="lblNumCar7">254</label> caratteri -</span>
				<textarea name="txtAuthoring" class="textblack" cols="50" rows="3" OnKeyUp="JavaScript:CheckLenTextArea(txtAuthoring,lblNumCar7,254)" <%=bText%>><%=RR("AUTHORING_CORSO")%></textarea>
			</td>
	    </tr>
	    <tr> 
			<td align="left" nowrap class="tbltext1">
				<b>Informazioni<br>Produttore</b>
			</td>
			<td> 
				<span class="tbltext1">&nbsp;- Utilizzabili <label name="lblNumCar3" id="lblNumCar3">2000</label> caratteri -</span>
				<textarea name="txtInfoProd" class="textblack" cols="50" rows="3" OnKeyUp="JavaScript:CheckLenTextArea(txtInfoProd,lblNumCar3,2000)" <%=bText%>><%=RR("INFO_PRODUTTORE")%></textarea>
			</td>
	    </tr>-->
	    <tr>
			<td colspan=2>
				<div id="VisObi" name="VisObi" style="position:absolute; visibility:<%=bVisible%>;">
					<a href="javascript:frmObi.submit()" class="textred" onmouseover="javascript:window.status='' ; return true">
						<b>Obiettivi AICC</b>
					</a>
				</div>	
			</td>
		</tr>
		<tr><td colspan=2>&nbsp;</td></tr>
	</table>
	<br>
	<table width="500" border="0" cellspacing="2" cellpadding="1">
		<tr align="center">
			<td width="20%">
				&nbsp;
			</td>
			<td align="right" width="20%">
				<%
					PlsIndietro()
				%>
			</td>
			<td align="center" width="20%">
				<%
					PlsInvia("inserisciC")
				%>
			</td>
			<td align="left" width="20%">
				<%
					PlsElimina("javascript:Elimina(" & nIDCorso & ",'" & dtTmst & "')")
				%>
			 </td>
			<td width="20%">
				&nbsp;
			</td>
		</tr>
	</table>
</form>
<form id="frmObi" name="frmObi" method="POST" action="COR_VisObiCorso.asp">
	<input type=hidden name="hIdCorso" id="hIdCorso" value="<%=RR("ID_CORSO")%>">
	<input type=hidden name="hTitCorso" id="hTitCorso" value="<%=RR("TIT_CORSO")%>">
</form>	
<%

End Sub
';););););););););););););););););););););););););););););););););););););););)
Sub ImpostaPag()
	Dim sql, RR, RR2, sql2
	Dim aTp(2), aPb(2), Dispgg, Dispmm, Dispaa, Rif, i, dtTmst
		
	aTp(0) = "In presenza"
	aTp(1) = "A distanza"

	aPb(0) = "Pubblico"
	aPb(1) = "Privato"
	
	sql = "SELECT CORSO.*, AREA.TitoloArea" &_ 
			" FROM CORSO, AREA WHERE ID_CORSO =" & nIDCorso &_
			" And CORSO.ID_AREATEMATICA = AREA.ID_AREA "
			
	Set RR = Server.CreateObject("ADODB.Recordset")
	'Response.Write sql
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
	RR.Open sql, CC, 3

	If RR.EOF then
%>
		<br>
		<table border="0" width="500" cellpadding="0" cellspacing="0">
			<tr align="center">
				<td class="tbltext3">
					Pagina momentaneamente non disponibile
				</td>
			</tr>
		</table>	
		<br>
		<table width="500" border="0" cellspacing="2" cellpadding="1">
			<tr align="center">
				<td>
					<%
						PlsIndietro()
					%>
				</td>
			</tr>
		</table> 
		<br>
<%		
	else	
		dtTmst = RR.Fields("DT_TMST")
		if rr("DT_DISPONIBILE") > date() then
			call PaginaModificabile(RR)
		else
			call PaginaNonModificabile(RR)
		end if
	end if
End Sub
';););););););););););););););););););););););););););););););););););););););)
'M A I N
Dim nIDCorso', sCodArea
%>
<!--#include virtual="/strutt_testa2.asp"-->
<!--#include virtual="/include/openconn.asp"-->
<!--#include virtual="/include/DecCod.asp"-->
<!--#include virtual="/include/HTMLEncode.asp"-->
<!--#include virtual="/include/SetPulsanti.asp"-->
<%
	nIDCorso = Request("IDCorso")
	'sCodArea = Request.QueryString("CodArea")
	
	ControlliJavaScript()
	Testata()
	ImpostaPag()
%>
<!--#include virtual="/include/closeconn.asp"-->
<!--#include virtual="/strutt_coda2.asp"-->
