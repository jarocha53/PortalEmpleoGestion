<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%
Sub Inizio()
%>
<html>
<head>
	<link REL="STYLESHEET" TYPE="text/css" HREF="<%=session("progetto")%>/fogliostile.css">
	<meta NAME="GENERATOR" Content="Microsoft Visual Studio 6.0">
	<title>Inserimento Link</title>	
</head>
<script LANGUAGE="JavaScript">
function InviaSelezione(saPath)
{
	opener.document.FormInserisciElementi.txtLinkElemento.value = saPath
	self.close()
}
function Valida(frmSub)
{
	if (frmSub.txtUpload.value=="")
	{
		frmSub.txtUpload.focus()
		alert("Selezionare un file")
		return false
	}
	return true
}

function ChechMultiChar(InputString,Caratteri)
{
    var anyString = InputString;
    var anyCaratteri = Caratteri;
	for ( var i=0; i<=anyString.length-1; i++ ){
		for ( var k=0; k<=anyCaratteri.length-1; k++ ){
			if ((anyString.charAt(i) == anyCaratteri.charAt(k))) {
				return(false);
			}
		}			
	}
	return(true);
}

function NuovaCartella(sFold, sImg)
{
	var NomeCartella = ""
	NomeCartella = window.prompt("Inserisci il nome della cartella","Nuova Cartella")
	if (NomeCartella != null)
	{
		if ((NomeCartella.length >> 0) && (ChechMultiChar(NomeCartella,"\/:*?<>|'") == true))
		{
			location.href = "COR_SelLink.asp?modo=3&img="+sImg+"&sNF="+sFold+NomeCartella
		}
		else
		{
			alert('Nome della cartella non consentito')
		}
	}	
}

function EliminaCartella(sImg)
{
	if (window.confirm("Confermi l'eliminazione della cartella?"))
	{
		location.href = "COR_SelLink.asp?modo=4&img="+sImg
	}
}
</script>
<body>
<p align="center">
<%
	dim sFunzione
	dim sTitolo
	dim sCommento
	dim bCampiObbl
	
	sFunzione = "GESTIONE CORSO"
	sTitolo = "GESTIONE CORSO"
	sCommento = "Selezione Link"
	bCampiObbl = false
%>
	<!--#include virtual="/include/SetTestata.asp"-->
<%
End Sub
'------------------------------------------------------------------------
Sub Fine()
%>
<br>
<table width="500">
	<tr>
		<td align="center">
			<a href="javascript:window.close()">
				<img src="<%=Session("progetto")%>/images/chiudi.gif" border="0" onmouseover="javascript:window.status='' ; return true">
			</a>
		</td>
	</tr>
</table>
</p>		
</body>
</html>
<%
End Sub
'------------------------------------------------------------------------
Function EstraiDir(sPath)
	for nPos = 1 to Len(sPath)
		if mid(sPath,nPos,1) = "\" then
			nLastPos=nPos
		end if
	next
	EstraiDir = left(sPath, nLastPos)
End Function
'------------------------------------------------------------------------
Function AccorciaStringa(sStringa, nLunghezza)
dim nLenStrDx
	if len(sStringa) <= nLunghezza then
		AccorciaStringa = sStringa
	else
		nLenStrDx = nLunghezza - 8 '8 � dato dai primi 5 caratteri + i 3 punti
		AccorciaStringa = left(sStringa,5) & "..." & right(sStringa, nLenStrDx)
	end if
End Function
'------------------------------------------------------------------------
Sub ImpostaPag()
	
dim nPos, nLastPos, folderspec
dim sPRel
dim nFile
dim sPercIcone
dim nContaDir
	
	on error resume next
	if sImg = "" then
		sPRel = sRoot
	else
		sPRel = EstraiDir(Replace(sImg,"/","\"))
	end if
	folderspec = server.mappath(sPRel)
	if err.number <> "0" then
		sPRel = sRoot
		folderspec = server.mappath(sPRel)
	end if
	on error goto 0
	
	Dim fso, f, f1, fc
	Set fso = CreateObject("Scripting.FileSystemObject")
	Set f = fso.GetFolder(folderspec)
	Set fc = f.Files
	Set fs = f.SubFolders
	
'S U B F O L D E R
%>
<table border="0" cellspacing="2" cellpadding="1" width="100%">
	<tr>
		<td align="center">
			<a onmouseover="javascript:window.status='' ; return true" href="javascript:InviaSelezione('')" class="textred">
				<b>Rimuovi Selezione</b>
			</a>
		</td>
	</tr>	
</table>
<br>
<table width="500" border="0" class="sfondocomm">
	<tr height="25">
		<td align="left" class="sfondocomm">
			Cartella selezionata: <b><%=AccorciaStringa(sprel,40)%></b>
		</td>
		<td align="center" valign="center" width="30">
<%
	sPRel = Replace(sPRel,"\","/")
	if (sPRel <> sRoot) then
	
	app = EstraiDir(Replace(mid(sprel,1,len(sprel)-1),"/","\"))
		if len(app) > 0 then 
		prec = mid(app,1,len(app)-1)
		%>
			<a href="COR_SelLink.asp?modo=1&amp;img=<%=prec%>" class="tblagg">
				<img src="/images/formazione/UpFolder.gif" border="0" alt="Livello superiore" WIDTH="18" HEIGHT="16">
			</a>
		<%
		end if
	end if
%>
		</td>
		<td align="center" valign="center" width="30">
			<a href="javascript:NuovaCartella('<%=sPRel%>', '<%=sImg%>')">
				<img src="/images/formazione/NewFolder.gif" border="0" alt="Nuova Cartella" WIDTH="19" HEIGHT="18">
			</a>
		</td>		
		<td align="center" valign="center" width="30">
			<a href="javascript:EliminaCartella(&quot;<%=sPrel%>&quot;)">
				<img src="<%=session("progetto")%>/images/cestino.gif" border="0" alt="Elimina Cartella">
			</a>
		</td>		
	</tr>
</table>
<table width="500" border="0">
<%
	nContaDir = 0
	for each f2 in fs
		if left(f2.name,1) <> "_" then
		nContaDir = nContaDir + 1
%>
			<tr>
				<td align="left">
					<a href="COR_SelLink.asp?modo=1&amp;img=<%=sprel & f2.name%>" class="tblagg">
						<img src="/images/icons/cartellasm.gif" border="0" WIDTH="17" HEIGHT="14">
						&nbsp;<%=f2.name%>
					</a>
				</td>
			</tr>
<%
		end if
	next
	if nContaDir = 0 then
%>
		<tr>
			<td align="left" class="tbltextFAD">
				(nessuna cartella)
			</td>
		</tr>
<%
	end if
%>
</table>
<br>

<table width="500" border="0">
	<tr height="25"> 
		<td align="left" class="sfondocomm" colspan="5">
			Contenuto cartella: <%=fc.count%> file
		</td>
	</tr>
	<tr>
<%
	nFile = 0
	For Each f1 in fc
		sImmagine=replace(sprel & f1.name, "\", "/")
	
			nFile = nFile + 1
			if sImmagine = sImg then
				sAttr = " class=sfondocomm"
			else
				sAttr = ""
			end if
			sPercIcone = "/images/icons/"
			select case right(f1.name,4)
				case ".mdb"
					sIco = sPercIcone & "access.gif"
				case ".asp"
					sIco = sPercIcone & "asp.gif"
				case ".xls"
					sIco = sPercIcone & "excel.gif"
				case ".swf"
					sIco = sPercIcone & "flash.gif"
				case ".jpg"
					sIco = sPercIcone & "gif.gif"
				case ".gif"
					sIco = sPercIcone & "gif.gif"
				case ".hlp"
					sIco = sPercIcone & "help.gif"
				case ".htm"
					sIco = sPercIcone & "html.gif"
				case ".pdf"
					sIco = sPercIcone & "pdf.gif"
				case ".ppt"
					sIco = sPercIcone & "ppt.gif"
				case ".psd"
					sIco = sPercIcone & "psd.gif"
				case ".txt"
					sIco = sPercIcone & "txt.gif"
				case ".doc"
					sIco = sPercIcone & "word.gif"
				case ".zip"
					sIco = sPercIcone & "zip.gif"
				case else
					sIco = sPercIcone & "altro.gif"
			end select
			
%>
			<td align="left" valign="center" <%=sattr%>>
				<a href="javascript:InviaSelezione('<%=sImmagine%>');" border="0" class="tbldett">
					<img src="<%=sIco%>" border="0">
					<b><%=f1.name%></b>
				</a>
			</td>
		</tr>
<%
	Next
%>
	</table>
	<br>
<%
End Sub
'------------------------------------------------------------------------
Sub Upload(sPImg)
%>
<table width="500" border="0">
	<tr height="25"> 
		<td align="left" class="sfondocomm" colspan="5">
			Upload del file
		</td>
	</tr>
	<tr>
		<td>
			<form METHOD="POST" encType="multipart/form-data" onsubmit="return Valida(this)" name="frmUpload" action="COR_SelLink.asp?modo=2&amp;img=<%=sPImg%>">
				<input type="File" id="txtUpload" name="txtUpload">
				<input type="submit" id="cmdUpload" name="cmdUpload" value="Upload">
				<!--input type="hidden" id="modo" name="modo" value="2">				<input type="hidden" id="img" name="img" value="<%=sPImg%>"-->
			</form>
		</td>
	</tr>
</table>
<%
End Sub
'------------------------------------------------------------------------
Function SalvaFile(sPImg)

	if sPImg = "" then
		sPImg = "/testi/formazione/materiali/"
	end if
	
	On Error Resume Next

	Set objUpload = Server.CreateObject("Dundas.Upload.2")
	objUpload.UseUniqueNames = false
	
	PathSave = Server.MapPath("\") & replace(sPImg, "/", "\")
	objUpload.Save PathSave

	strName = objUpload.Form("txtUpload")

	Set objUpload = Nothing
	
	if Err.number = 0 then
		SalvaFile = "File correttamente salvato"
	else
		SalvaFile = "Errore durante il salvataggio: " & Err.number & ": " & Err.Description
	end if
	
End Function
'------------------------------------------------------------------------
Function CreaCartella()
dim sNuova, f

sNuova = Request.QueryString("sNF")

Set FSO = Server.CreateObject ("Scripting.FileSystemObject") 
'on error resume next 

path = Server.MapPath("\") & replace(sNuova, "/", "\")
if not fso.folderexists(path) then

	set f = fso.CreateFolder(path)

end if

set FSO = Nothing 

if Err.number = 0 then
	CreaCartella = "Cartella correttamente creata"
else
	CreaCartella = "Errore durante la creazione della cartella: " & Err.number & ": " & Err.Description
end if

End Function
'------------------------------------------------------------------------
Function EliminaCartella()

folderspec = server.MapPath(sImg)

	Dim fso, f, f1, fc, s
	Set fso = CreateObject("Scripting.FileSystemObject")
	Set f = fso.GetFolder(folderspec)
	Set fc = f.Files
	Set fs = f.SubFolders
	if fc.count = 0 and fs.count = 0 then
		fso.DeleteFolder(folderspec)
		if Err.number = 0 then
			sPRel = Replace(sImg,"\","/")
			if (sPRel <> sRoot) then
				app = EstraiDir(Replace(mid(sprel,1,len(sprel)-1),"/","\"))
				if len(app) > 0 then 
					sImg = mid(app,1,len(app)-1)
				else
					sImg = sRoot
				end if
			else
				sImg = sRoot
			end if
			EliminaCartella = "Cartella correttamente eliminata"
		else
			EliminaCartella = "Errore durante l'eliminazione della cartella: " & Err.number & ": " & Err.Description
		end if
	else
		EliminaCartella = "Impossibile cancellare una cartella non vuota"
	end if
End Function
'------------------------------------------------------------------------
'M A I N
const sRoot = "/testi/formazione/contenuti/"
sImg = Request("img")
nModo = Request("modo")

select case nModo
	case 1
		sImg = sImg & "/"
	case 2
		sPar = SalvaFile(sImg)
		%>
		<script language="javascript">
			alert("<%=sPar%>");
		</script>
		<%	
	case 3
		sPar = CreaCartella()
		%>
		<script language="javascript">
			alert("<%=sPar%>");
		</script>
		<%
	case 4
		sPar = EliminaCartella()
		%>
		<script language="javascript">
			alert("<%=sPar%>");
		</script>
		<%		
end select
%>
<!--#include Virtual ="/include/openconn.asp"-->
<%
Inizio()
ImpostaPag()
Upload(sImg)
%>	
<!--#include Virtual ="/include/closeconn.asp"-->
<%
Fine()
%>
