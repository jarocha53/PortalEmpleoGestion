<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->


<script language="Javascript">

	<!--#include virtual="/include/Help.inc"-->
</script>

<%
Sub Inizio()
	dim sFunzione
	dim sTitolo
	dim sCommento
	dim bCampiObbl
	
	sFunzione = "ASSOCIAZIONE TEST-CORSO"
	sTitolo = "ASSOCIAZIONE TEST-CORSO"
	sCommento = "La tabella sottostante, mostra tutti i corsi disponibili, ai quali" &_
				" � possibile associare dei questionari messi a disposizione." &_
				"<br>Selezionando il <b>Titolo</b>" &_
			    " del corso interessato, � possibile visualizzare il relativo " &_
				 "questionario associato. "
	
	bCampiObbl = false
	sHelp="/Pgm/help/formazione/corsi/Default.htm"
%>
	<!--#include virtual="/include/SetTestata.asp"-->
<%
End Sub
';););););););););););););););););););););););););););););););););););););););)
Sub ImpostaPag()
	Dim aTipo(2)
	Dim sSQL, ssSQLCond
	Dim sCondTitCorso
	dim sCondTitArea
	dim sCondDestinatari
	dim sCondObiettivi
	dim sCondModalita
	dim sCondDurata
	dim RecEle
	

	aTipo(0) = "Off Line"
	aTipo(1) = "On Line"
		
	sSQL = "SELECT * FROM Corso, Area WHERE Area.ID_AREA = Corso.ID_AREATEMATICA "

	sSQLCond = ""

	sCondTitCorso = Request.Form("txtTitCorso")
	sCondTitArea =  clng(Request.Form("txtTitArea"))
	sCondDestinatari = Request.Form("txtDestinatari")
	sCondObiettivi = Request.Form("txtObiettivi")
	sCondModalita = Request.Form("txtModalita")
	sCondDurata = Request.Form("txtDurata")

	'Response.Write "sCondObiettivi=" & sCondObiettivi & "<br>"
	'Response.Write "sCondModalita=" & sCondModalita & "<br>"

	If sCondTitCorso <> "" Then	
		sSQLCond = sSQLCond & " AND TIT_CORSO LIKE '%" & _
		Replace(sCondTitCorso, "'", "''") & "%'"
	End If

	If sCondTitArea <> 0 Then
		sSQLCond = sSQLCond & " AND ID_AREA = " & sCondTitArea
	End If

	If sCondObiettivi <> "" Then
		sSQLCond = sSQLCond & " AND OBIETTIVO_CORSO LIKE '%" & _
		Replace(sCondObiettivi, "'", "''") & "%'"
	End If

	If sCondModalita <> "" AND sCondModalita <> "TUTTE" Then
		sSQLCond = sSQLCond & " AND FL_EROGAZIONE ='" & sCondModalita & "'"
	End If

	If sCondDurata <> "" Then	
		sSQLCond = sSQLCond & " AND DURATA_CORSO LIKE '%" & _
		Replace(sCondDurata, "'", "''") & "%'"
	End If

	sSQL = sSQL & sSQLCond & " ORDER BY ID_CORSO, TIT_CORSO"

	Set rsCorsi = Server.CreateObject("ADODB.Recordset")
		
	'Response.Write sSQL
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	rsCorsi.Open sSQL, CC, 3
		
	If rsCorsi.BOF and rsCorsi.EOF Then
	%>
		 <table border="0" width="500" cellpadding="0" cellspacing="0">
			<tr align="center">
				<td class="tbltext3"> 
					Non vi sono Corsi che soddisfano le condizioni di ricerca
				</td>
			</tr>
		 </table>
<%  
	Else	
        rsCorsi.MoveLast
        rsCorsi.MoveFirst
       
    End If
		
	Set RR = Server.CreateObject("ADODB.Recordset")
	sSQL = "Select count(*) as ContaCorso FROM Corso, Area " &_
			"WHERE Area.ID_AREA= Corso.ID_AREATEMATICA " & sSQLCond
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	set RR = CC.Execute(sSQL)
	nTot = CINT(RR.Fields("ContaCorso"))
	RR.Close
	set RR = nothing
		
	If nTot > 25 Then
	%>
		<table border="0" width="500" cellpadding="0" cellspacing="0">
			<tr align="center">
				<td class="tbltext3"> 
					Troppi Corsi soddisfano le condizioni di ricerca
					<br>
					Raffinare la ricerca!
				</td>
			</tr>
		 </table>
		<br>
<% 
	Else
	    If Not rsCorsi.BOF Then
%>
			<table border="0" width="500">
				<tr class="sfondocomm">
					<td><b>Titolo</b></td>
					<td><b>Descrizione</b></td>
			
				</tr>
		 <%     
				nPrgCorso = 0
				Do While Not rsCorsi.EOF
				nPrgCorso = nPrgCorso + 1
				sNomeForm = "FrmCorso" & nPrgCorso
		 %>		<tr class="tblsfondo"> 
					<form METHOD="POST" name="<%=sNomeForm%>" id="<%=sNomeForm%>" action="COR_ModModuli.asp">
					<td>
							<input type=hidden name="CodArea" id="CodArea" value="<%=rsCorsi("ID_AREA")%>">
							<input type=hidden name="IdCorso" id="IdCorso" value="<%=rsCorsi("ID_CORSO")%>">
							
								<a HREF="javascript:<%=sNomeForm%>.submit()" class="tblAgg" onmouseover="javascript:window.status='' ; return true"> 
									<%=strHTMLEncode(rsCorsi("TIT_CORSO"))%>
								</a>
						
					</td>
					</form>
					<td class="tblDett">
						<%=strHTMLEncode(rsCorsi("Desc_CORSO"))%>
					</td>
				
				</tr>
	    <%		rsCorsi.MoveNext
				Loop
		%> 
			</table>
	<%	
		End If
	End If
	rsCorsi.Close
	%> 
	<br>
	<table border="0" width="500" cellpadding="0" cellspacing="0">
		<tr align="center">
			<td> 
				<%
					PlsLinkRosso "COR_RicAsso.asp", "Nuova ricerca"
				%>
			</td>
		</tr>
	</table>
	<br>
<%
End Sub

';););););););););););););););););););););););););););););););););););););););)
'M A I N
%>
<!--#include virtual="/strutt_testa2.asp"-->
<!--#include virtual="/include/openconn.asp"-->
<!--#include virtual="/include/HTMLEncode.asp"-->
<!--#include virtual="/include/SetPulsanti.asp"-->
<%
Inizio()
ImpostaPag()
%>
<!--#include virtual ="/include/closeconn.asp"-->
<!--#include virtual="/strutt_coda2.asp"-->
