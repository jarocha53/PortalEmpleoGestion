<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->

<!--#include virtual="/strutt_testa2.asp"-->
<!--#include virtual = "/include/SysFunction.asp"-->
<!--#include virtual="/include/openconn.asp"-->
<%

dim IdCorso
dim IdQuestionario
dim DescrizioneCorso
dim DT
dim Errore
dim sSql
dim IdEle
dim RimozioneTQ
dim RimozioneTC

CC.BeginTrans

RimozioneTQ=Request.Form("txtRimozineTestCorso")
RimozioneTC=Request.Form("TxtRimozioneTestCorso")

IdCorso=Request.Form("IdCorso")
VIdQuestC=Request.Form("VecchioIdQuest")'rappresenta il Vecchio ID Questionario per il CORSO !

IdQuestionario=Request.Form("NomeTxtIdQuest")'rappresenta il Nuovo ID Questionario per il CORSO !!!
DescrizioneCorso=Request.Form("NomeTxtDesc")

'CONTROLLI PER L'ASSOCIAZIONE DEL QUERSTIONARIO AL CORSO*********** 

if RimozioneTC<>"RIMUOVI" then
	'se il nuovo idQuestionario � vuoto, non tocca nulla.
	if IdQuestionario<>"" then
		
		if ControllaModCors(VIdQuestC,IdCorso)=false then
			call InsQCorso(IdQuestionario,IdCorso)
		else
			call ModQCorso(IdQuestionario,IdCorso)
		end if
		
	end if
else
	dim DelQC
	dim errDelQC
	if VIdQuestC<>"" then
		DelQC="delete from PROVEVALUTAZIONE where" &_
			  " ID_QUESTIONARIO=" & VIdQuestC & " and" &_
			  " ID_CORSO=" & IdCorso & " and ID_ELEMENTO=0"
			 
		errDelQC=EseguiNoC(DelQC,CC)
		if errDelQC<>"0" then
			call ErroreOperazione(errDelQC)
		end if
	end if
end if

'FINE CONTROLLI PER L'ASSOCIAZIONE DEL QUERSTIONARIO AL CORSO*******

'CONTROLLI PER L'ASSOCIAZIONE DEI QUERSTIONARI AI MODULI*********** 

contatore=Request.Form("txtContatore")

for i=0 to contatore-1
					
	IdQuesti=Request.Form("NomeTxtIdQuesti"& i)'nuovo idQuestionario
	IdEle=Request.Form("NomeTxtIdEle"& i)      'id elemento
					
	V_IdQuesti=Request.Form("VecchiaTxtIdQuesti"& i) 'vecchio IdQuestionario
	RimuoviTestQ=Request.Form("TxtRimozioneTestModulo"& i)	'Contieve il valore "RIMUOVI" se viene selezionata la voce RIMUOVI SELEZIONE dalla popup.	
			
	if RimuoviTestQ<>"RIMUOVI" then	'controllo se si intende rimuovere un questionario del modulo
			
					
			if	IdQuesti="" then 
				'se Il nuovo IdQuestionario="" va al prossimo
			else	
						
				if  ControllaModQuest(V_IdQuesti,IdEle,IdQuesti)=false  then
										
					'controllo se l'utente avesse fatto il refresh
						if RefreshModQuest(IdQuesti,IdEle)=false then
												
							call InsQuestio_Modulo (IdQuesti,IdEle)
													
						else

							sqlU="update PROVEVALUTAZIONE set ID_QUESTIONARIO=" & IdQuesti & " where ID_ELEMENTO=" & IdEle & " and ID_CORSO=0"
													
							Errorer=EseguiNoC(sqlU,cc)
										
						end if
				else	
				'modific
					call ModQuestioModulo (V_IdQuesti,IdQuesti,IdEle)
											
				end if
										
			end if
	else
		if V_IdQuesti<>"" then
					
			'elimino l'associazione del Questionario al Modulo
			dim sqlEliminaMT
			dim ErroreD
	
			sqlEliminaMT="delete from PROVEVALUTAZIONE" &_
					  " where ID_QUESTIONARIO=" & V_IdQuesti & " and" &_
					  " ID_ELEMENTO= " & IdEle & ""
					  
			 ErroreD=EseguiNoC(sqlEliminaMT,CC)
			if ErroreD<>"0" then
				call ErroreOperazione(ErroreD)
			end if
		end if
	end if	
next

' FINE CONTROLLI PER L'ASSOCIAZIONE DEI QUERSTIONARI AI MODULI*********** 

call NessunErrore()


'-----------------------------------------------------------------------------
function ControllaModCors(VIdQuestC,IdCorso)
	dim Esito
	dim sqlChekC
	dim recC
	if VIdQuestC="" then
		Esito=false
	else	
		set recC=Server.CreateObject("ADODB.Recordset")
		
		sqlChekC="select ID_ELEMENTO,ID_CORSO,ID_QUESTIONARIO from " &_
				 "PROVEVALUTAZIONE where ID_QUESTIONARIO=" & VIdQuestC & " and" &_
				 " ID_ELEMENTO=0 and ID_CORSO=" & IdCorso & ""
	
'PL-SQL * T-SQL  
SQLCHEKC = TransformPLSQLToTSQL (SQLCHEKC) 
		recC.Open sqlChekC,CC,3
		
		if recC.EOF then
			Esito=false
		else
			Esito=true
		end if
	end if
		
	ControllaModCors=Esito
end function
'-----------------------------------------------------------------------------
sub InsQCorso(IdQuestionario,IdCorso)
	dim sqlInsQC
	dim ErrInsQC
	sqlInsQC="insert into PROVEVALUTAZIONE " &_
			 "(ID_ELEMENTO,ID_CORSO,ID_QUESTIONARIO)" &_
			 "values(0," & IdCorso & "," & IdQuestionario & ")"
			 
	ErrInsQC=EseguiNoC(sqlInsQC,CC)
	if ErrInsQC<>"0" then
		call ErroreOperazione(ErrInsQC)
	end if
end sub
'-----------------------------------------------------------------------------
sub ModQCorso(IdQuestionario,IdCorso)
	dim	sqlUpdQC
	dim ErrUpdQC
	sqlUpdQC="update PROVEVALUTAZIONE set" &_
				" ID_QUESTIONARIO=" & IdQuestionario & " where" &_
				" ID_CORSO=" & IdCorso & " and ID_ELEMENTO=0"
	
	ErrUpdQC=EseguiNoC(sqlUpdQC,CC)
	if ErrUpdQC<>"0" then
		call ErroreOperazione(ErrUpdQC)
	end if
end sub
'-----------------------------------------------------------------------------
function ControllaModQuest(V_IdQuesti,IdEle,IdQuesti)
	DIM RecChek
	dim sqlChek
	dim sql	
	dim condizione

	if V_IdQuesti=""  then
		condizione=" and ID_QUESTIONARIO=" & IdQuesti & " "
	else
		condizione=" and ID_QUESTIONARIO=" & V_IdQuesti & " "
	end if
							
	sqlChek="select ID_ELEMENTO,ID_QUESTIONARIO" &_
				" from PROVEVALUTAZIONE where" &_
				" ID_ELEMENTO=" & IdEle & " and ID_CORSO=0 " & condizione

				  
		SET RecChek=Server.CreateObject("ADODB.recordset")
					
'PL-SQL * T-SQL  
SQLCHEK = TransformPLSQLToTSQL (SQLCHEK) 
		RecChek.Open sqlChek,cc,3
					
		IF RecChek.EOF then
			'per inserimento
			PIENO=false
		else
			PIENO=true
		end if
					
	ControllaModQuest=PIENO
end function
'-----------------------------------------------------------------------------
sub InsQuestio_Modulo(IdQuesti,IdEle)
					
	sqlIns="insert into PROVEVALUTAZIONE (ID_ELEMENTO,ID_CORSO,ID_QUESTIONARIO)" &_
	" values (" & IdEle & ",0," & IdQuesti & ")"
						
	Errore=EseguiNoC(sqlIns,cc)

	if Errore<>"0" then
		call ErroreOperazione(Errore)	
	end if	

end sub
'-----------------------------------------------------------------------------
sub ModQuestioModulo (V_IdQuesti,IdQuesti,IdEle)

	dim sqlMod
	if V_IdQuesti="" then
					
		sqlMod="update PROVEVALUTAZIONE set ID_QUESTIONARIO=" & IdQuesti & " where " &_
				"ID_ELEMENTO=" & IdEle & " and ID_QUESTIONARIO=" & IdQuesti & " "
						
	else
					
		sqlMod="update PROVEVALUTAZIONE set ID_QUESTIONARIO=" & IdQuesti & " " &_
				" where ID_ELEMENTO=" & IdEle & " and" &_
				" ID_QUESTIONARIO=" & V_IdQuesti & ""
					
	end if
					
	ErroreMod=EseguiNoC(sqlMod,cc)
	if ErroreMod<>"0" then
					
		call ErroreOperazione(ErroreMod)	
					
	end  if
end sub
'-----------------------------------------------------------------------------
function RefreshModQuest(IdQuesti,IdEle)

	dim refRec
	dim refSql
	dim refresh

	set refRec=Server.CreateObject("ADODB.Recordset")
	refSql="select ID_QUESTIONARIO,ID_ELEMENTO" &_
		" from PROVEVALUTAZIONE where " &_
		"ID_QUESTIONARIO= " & IdQuesti & " and" &_
		" ID_ELEMENTO=" & IdEle & ""		
					
'PL-SQL * T-SQL  
REFSQL = TransformPLSQLToTSQL (REFSQL) 
	refRec.Open refSql,cc,3
					
	if refRec.EOF then
		RefreshModQuest	=false
	else
		RefreshModQuest=true
	end if

end function
'-----------------------------------------------------------------------------
sub ErroreOperazione(Errore)%>
	<table BORDER="0" CELLSPACING="1" CELLPADDING="1" width="500">
		<tr align="middle">
			<td class="tbltext3">
			<br>
				<%
				CC.RollbackTrans
				Response.Write "Modifica dei dati non effettuabile. <br>"
				Response.Write Errore
				%>
			</td>
		<tr>
			<td align="middle">

			<a href="javascript:history.back()">
				<img src="<%=Session("progetto")%>/images/indietro.gif" border="0" onmouseover="javascript:window.status='' ; return true">
			</a>
			</td>
		</tr>
    </table>
<%
end sub
'-----------------------------------------------------------------------------
sub NessunErrore()
	CC.CommitTrans
%>	<br><br><br>
	<font class="tbltext3">
		Modifiche correttamente effettuate.
	</font>
	<br><br><br><br>
	<a href="JavaScript:location.href='COR_RicAsso.asp';" class="textred">
		<b>Nuova Ricerca</b>
	</a>	
<%
end sub
'-----------------------------------------------------------------------------
%>


<!--#include virtual="/include/closeconn.asp"-->
<!--#include virtual="/strutt_coda2.asp"-->
