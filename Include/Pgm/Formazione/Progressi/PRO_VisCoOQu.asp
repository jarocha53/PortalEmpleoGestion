<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%
Sub Inizio()
%>
<html>
<head>
	<link REL="STYLESHEET" TYPE="text/css" HREF="<%=session("progetto")%>/fogliostile.css">
	<meta NAME="GENERATOR" Content="Microsoft Visual Studio 6.0">
	<title>I tuoi progressi</title>	
</head>
<body>
<%
dim sTitolo
dim sCommento
dim sHelp

	sTitolo = "I tuoi progressi"
	sCommento = "Commento... I tuoi progressi"
	sHelp = ""
	call TestataFAD(sTitolo, sCommento, false, sHelp)
	
End Sub
';););););););););););););););););););););););););););););););););););););););)
Function Decodifica(sYoN)
dim sMess
select case sYoN
case "S"
	sMess = "Superato"
case "N"
	sMess = "Non Superato"
end select 
Decodifica = sMess

End Function
';););););););););););););););););););););););););););););););););););););););)
Function OreMinutiSecondi(nSecInput)
dim nOre, nMin, nSec
'Calcolo il numero delle ore
nOre = nSecInput \ 3600
nMin = (nSecInput - (nOre*3600)) \ 60
nSec = nSecInput - ((nOre*3600) + (nMin*60))
OreMinutiSecondi = nOre & "h " & nMin & "' " & nSec & "''"
End Function
';););););););););););););););););););););););););););););););););););););););)
Sub VisCorsi()
dim nId
dim sSQL, rsStuData
	nId = Request.QueryString("id")

	'sSQL =  "SELECT CORE.LESSON_ID, CORE.LESSON_STATUS, " &_
	'		"CORE.TRY_TIME, CORE.ATTEMPT_NUMBER, ELEMENTO.TITOLO_ELEMENTO " &_
	'		"FROM " &_
	'		"CORE, ELEMENTO " &_
	'		"WHERE " &_
	'		"STUDENT_DATA.LESSON_ID = ELEMENTO.ID_ELEMENTOAICC AND " &_
	'		"COURSE_ID = 'C" & nId & "' AND " &_
	'		"STUDENT_ID = '" & session("idutente") & "' " &_
	'		"ORDER BY LESSON_ID" 

	sSQL =  "SELECT CORE.LESSON_ID, CORE.LESSON_STATUS, " &_
			"CORE.CORE_TIME,  ELEMENTO.TITOLO_ELEMENTO " &_
			"FROM " &_
			"CORE, ELEMENTO " &_
			"WHERE " &_
			"CORE.LESSON_ID = ELEMENTO.ID_ELEMENTOAICC AND " &_
			"COURSE_ID = 'C" & nId & "' AND " &_
			"STUDENT_ID = '" & session("idutente") & "' " &_
			"ORDER BY LESSON_ID" 

'	Response.Write "<BR> sSQL = " & sSQL

'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	set rsStuData = CC.execute(sSQL)
	if rsStuData.eof then
		VisMessaggio("Nessuna Lezione")
	else
%>
	<table border="0" width="500" cellpadding="1" cellspacing="2">
		<tr class="sfondocomm" height="23">
			<td width="20"><b>#</b></td>
			<td width="120"><b>Titolo</b></td>
			<td width="120"><b>Stato</b></td>
			<td width="120"><b>Tempo</b></td>
			<td width="120"><b>Tentativi</b></td>
		</tr>
	<%
	nProg = 1
	do while not rsStuData.eof
	%>
		<tr class=tblsfondo>
			<td class=tbldett>
				<%=nProg%>
			</td>
			<td class=tbldett>
				<%=rsStuData("TITOLO_ELEMENTO")%>
			</td>
			<td class=tbldett>
				<%=rsStuData("LESSON_STATUS")%>
			</td>
			<td class=tbldett>
				<%=rsStuData("CORE_TIME")%>
			</td>
			<td class=tbldett>
				0<%'=rsStuData("ATTEMPT_NUMBER")%>
			</td>
		</tr>		
	<%
		nProg = nProg + 1
		rsStuData.movenext
	loop
%>
</table>
</br>
<%

	end if
End Sub
';););););););););););););););););););););););););););););););););););););););)
Sub VisQuestionari()
dim nId
dim sSQL
nId = Request.QueryString("id")

sSQL =  "SELECT RISULT_QUEST.ID_RISULT_QUEST, RISULT_QUEST.FL_ESEGUITO, " &_
		"RISULT_QUEST.NUM_PUNTI, RISULT_QUEST.NUM_TENTATIVI, QUESTIONARIO.TIT_QUESTIONARIO " &_
		"FROM RISULT_QUEST, QUESTIONARIO " &_
		"WHERE " &_
		"RISULT_QUEST.ID_QUESTIONARIO = QUESTIONARIO.ID_QUESTIONARIO AND " &_
		"QUESTIONARIO.ID_QUESTIONARIO = " & nId & " AND " &_
		"RISULT_QUEST.ID_PERSONA = " & session("idutente")
'Response.Write sSQL
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
set rsQuest = CC.execute(sSQL)
if not rsQuest.eof then
%>
<table border="0" cellspacing="2" cellpadding="1" width="500">
	<tr> 
		<td align="left" nowrap class="tbltext1" width="150">
			<b>Titolo Questionario</b>
		</td>
		<td class="textblack">
			<%=rsQuest("TIT_QUESTIONARIO")%>
		</td>
	</tr>
	<tr><td>&nbsp;</td></tr>
	<tr>
		<td align="left" nowrap class="tbltext1">
			<b>Stato</b>
		</td>
		<td class="textblack">
			<%=Decodifica(rsQuest("FL_ESEGUITO"))%>
		</td>
	</tr>
	<tr><td>&nbsp;</td></tr>
    <tr> 
		<td align="left" nowrap class="tbltext1">
			<b>Punti</b>
		</td>
		<td class="textblack">
			<%=rsQuest("NUM_PUNTI")%>
		</td>
    </tr>
	<tr><td>&nbsp;</td></tr>
	<tr>
		<td align="left" nowrap class="tbltext1">
			<b>Tentativi</b>
		</td>
		<td class="textblack">
			<%=rsQuest("NUM_TENTATIVI")%>
		</td>
	</tr>
</table>

<%
else
	VisMessaggio ("Errore strano")
end if

End Sub
';););););););););););););););););););););););););););););););););););););););)
Sub VisMessaggio(sMess)
%>
<br>
<table border="0" width="500" cellpadding="0" cellspacing="0">
   <tr align="center">
   	<td class="tbltext3"> 
   		<%=sMess%>
   	</td>
   </tr>
</table>
<br>
<%
End Sub
';););););););););););););););););););););););););););););););););););););););)
Sub VisChiusura()
%>
<br>
<table width="500" border="0" cellspacing="2" cellpadding="1">
	<tr align="center">
		<td align="center">			
			<%
				PlsChiudi()
			%>
		</td>
	</tr>
</table> 
<br>
<%
End Sub
';););););););););););););););););););););););););););););););););););););););)
'M A I N
%>
<!--#include virtual="/include/openconn.asp"-->
<!--#include virtual="/include/DecCod.asp"-->
<!--#include virtual="/include/SetPulsanti.asp"-->
<!--#include virtual="/include/ControlDateVB.asp"-->
<!--#include virtual="/Include/SysFunction.asp"-->
<!--#include virtual="/include/SetTestataFAD.asp"-->
<%
Inizio()
select case Request.QueryString("tipo")
case "C"
	VisCorsi()
case "T"
	VisQuestionari()
end select
VisChiusura()
%>
<!--#include virtual="/include/closeconn.asp"-->
