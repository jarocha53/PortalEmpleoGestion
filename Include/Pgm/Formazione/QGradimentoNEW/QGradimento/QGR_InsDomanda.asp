<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%
Sub ControlliJavaScript()
%>
<SCRIPT LANGUAGE="Javascript">
<!--#include virtual="include/ControlString.inc"-->
<!--#include virtual="include/ControlNum.inc"-->


function ValidateInputNoAccenti(InputString)
{
	var sSepInput = InputString.split("\\");
	sFile = sSepInput[sSepInput.length-1];
	var anyString = sFile;
	
	for ( var i=0; i<=anyString.length-1; i++ ){
	
		if ((anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			(anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			(anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			(anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			(anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			(anyString.charAt(i) == "�") )
			{
			alert ("Non indicare lettere accentate.");
			return(false)
			}
	}
	return(true);
}


function ControllaCampi(frmInsDomanda)
{
	frmInsDomanda.DescDomanda.value = TRIM(frmInsDomanda.DescDomanda.value)
	if (frmInsDomanda.DescDomanda.value == "") {
		alert("Inserire la 'Descrizione Domanda'")
		frmInsDomanda.DescDomanda.focus() 
		return false
	}
	

	if (!ValidateInputNoAccenti( frmInsDomanda.DescDomanda.value )){
		frmInsDomanda.DescDomanda.focus();
		return false
	}

	
	//__________Obbligatoriet� Tipo Selezione__________	
	/*
	if (frmInsDomanda.cmbTipoSel.value == "") {
		alert("Specificare il Tipo di Selezione: Escludente o Multipla")
		frmInsDomanda.cmbTipoSel.focus() 
		return false
	}
	*/

}

</SCRIPT>
<%
End Sub
'--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++
Sub Inizio()
	dim sFunzione
	dim sTitolo
	dim sCommento
	dim bCampiObbl
	
	sFunzione = "GESTIONE QUESTIONARI DI GRADIMENTO"
	sTitolo = "INSERIMENTO DOMANDA"
	sCommento = "In questa pagina puoi inserire le domande, eventualmente associate ad una sezione, del questionario che si sta aggiornando."
	bCampiObbl = true
	sHelp = "/Pgm/help/Formazione/QGradimento/QGR_InsDomanda/"	
%>
	<!--#include virtual="include/SetTestata.asp"-->
<%
End Sub
'--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++
Sub Inserimento()
dim sSQL, rsTades
dim nIdQuest, nIdArea, nIdDom, nIdBlocco, nIdElem

nIdDom = request("idd")
nIdArea = request("ida")
nIdBlocco = request("idb")
nIdQuest = request("idq")
nIdElem = request("idel")
'Response.Write "nIdDom = " & nIdDom & "<br>"
'

%>
<BR>
<form method=post action="QGR_CnfInsDomanda.asp" onsubmit="return ControllaCampi(this)" id=frmInsDomanda name=frmInsDomanda>
<TABLE border=0 width=500> 
	<TR>
		<TD class=tbltext1>
			<b>Testo Domanda*</b>
		</TD>
		<TD>
			<INPUT id=DescDomanda maxLength=255 name=DescDomanda size=50 class=textblacka> 
			<INPUT type=hidden id=IdArea name=IdArea value=<%=nIdArea%>>
			<INPUT type=hidden id=IdBlocco name=IdBlocco value=<%=nIdBlocco%>>
			<INPUT type=hidden id=Idquest name=Idquest value=<%=nIdQuest%>>
			<INPUT type=hidden id=IdDom name=IdDom value=<%=nIdDom%>>
			<INPUT type=hidden id=IdElem name=IdElem value=<%=nIdElem%>>
	    </TD>
	</TR>
	<%
	set rsDdati = server.CreateObject("ADODB.Recordset")
	sSQL =	"SELECT id_campo_desc FROM diz_dati WHERE id_tab='IQ_DOMANDA'" &_
			" AND id_campo='TIPO_SEL'"
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	rsDdati.Open sSQL, CC, 3
		sTSel = rsDdati("id_campo_desc")
		aTSel = split(sTSel,"|")
	rsDdati.close
	%>	
	<TR>
		<TD class=tbltext1>
			<b>Tipo Selezione*</b>
		</TD>
		<TD>
			<select name="cmbTipoSel" id="cmbTipoSel" class="textblacka">
				<option value="">&nbsp;</option>
				<%for i=0 to Ubound(aTSel) step 2%>
					<option value="<%=aTSel(i)%>"><%=aTSel(i+1)%></option>
				<%next%>
			</select> 
	    </TD>
	</TR>
</table>
<br><br>

<TABLE border=0 width=500> 
	<TR>
		<td align=center>
			<%		
			PlsChiudi()
			PlsInvia("Conferma")
			%>
		</td>
	</TR>
</table>
</form>
<%
End Sub
'--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++
'M A I N
%>
<HTML>
<HEAD>
<TITLE><%=Session("TIT")%> [ <%=Session("login")%> ] </TITLE>
<LINK REL=STYLESHEET TYPE="text/css" HREF='<%=session("progetto")%>/fogliostile.css'>
</head>
<body class=sfondocentro topmargin="2" leftmargin="0">
<center>

<!--#include virtual="/include/openconn.asp"-->
<!--#include virtual="include/SetPulsanti.asp"-->

<%
	ControlliJavaScript()
	Inizio()
	Inserimento()
%>
<!--#include virtual="/include/closeconn.asp"-->
</body></html>
