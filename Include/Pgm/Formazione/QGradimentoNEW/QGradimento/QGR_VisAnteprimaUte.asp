<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%@LANGUAGE = JScript %>
<!--#INCLUDE file = "include/openConnJS.asp"-->
<%

//---------------------------------------------------------------------------------------------------------------------------------------------------------------

// Imposta il numero di messaggi per pagina
//var quanti_per_pagina = 5;

function messaggio(id_elemento, tipo_elemento ,livello, testo_domanda, tipo_domanda, tipo_sel, id_domandaiq, tipo_risposta, testo_risposta, testo_max, testo_min, iq_min, iq_max, fl_tiprisp, risposta_ute, id_blocco)
{
	

//	this.id = id;
	this.livello = livello;
	this.id_elemento = id_elemento;
	this.tipo_elemento = tipo_elemento;
	this.testo_domanda = testo_domanda;
	this.tipo_sel = tipo_sel;
	this.id_domandaiq = id_domandaiq;
	this.tipo_domanda = tipo_domanda;
	this.tipo_risposta = tipo_risposta;
	this.txt_risposta = testo_risposta;
	this.testo_max = testo_max;
	this.testo_min = testo_min;
	this.iq_min = iq_min;
	this.iq_max = iq_max;
	this.fl_tiprisp = fl_tiprisp;
	this.risposta_ute = risposta_ute;
	this.id_blocco = id_blocco;  
	//Response.Write ("risposta_ute=" + risposta_ute + "<br>-------<br>");
  
}

//---------------------------------------------------------------------------------------------------------------------------------------------------------------

function leggiMessaggi(rt,liv) {
		
	// Funzione ricorsiva di lettura
	sql =  " SELECT ID_INFOQUEST, ID_AREAIQ, ID_BLOCCO, TIPO_ELEMENTO, ID_ELEMENTO, ID_ELEM_ORIGINE, DT_TMST " 
	sql += " FROM STRUTTURA_QUEST "
	sql += " WHERE ID_AREAIQ = " + IdArea + " AND ID_ELEM_ORIGINE = " + rt + " AND ID_INFOQUEST = " + idquest 
	sql += " ORDER BY ID_BLOCCO, ID_ELEMENTO";
	
	// Response.Write(sql);
 
'PL-SQL * T-SQL  
SQL; = TransformPLSQLToTSQL (SQL;) 
	i_ris[liv] = CC.Execute(sql);
  
 	var TESTO_DOMANDA;
	var TIPO_SEL;
 	var ID_DOMANDAIQ;
	var TIPO_RISPOSTA;
	var TIPO_DOMANDA;
	var TXT_RISPOSTA;
	var TESTO_MAX;
	var TESTO_MIN;
	var IQ_MIN;
	var IQ_MAX;
	var FL_TIPRISP;
	var ID_RISPOSTAIQ;
	var RISPOSTA_UTE;	
	var ID_BLOCCO;
 
  
	while (!i_ris[liv].EOF){
		//LETTURA DOMANDA
		if (String(i_ris[liv]("TIPO_ELEMENTO"))=="D"){
			
			sqlDom = "SELECT TESTO_DOMANDA, TIPO_DOMANDA, TIPO_SEL, ID_DOMANDAIQ "
			sqlDom += " FROM IQ_DOMANDA WHERE ID_DOMANDAIQ =" + parseInt(i_ris[liv]("ID_ELEMENTO"))
			
'PL-SQL * T-SQL  
SQLDOM; = TransformPLSQLToTSQL (SQLDOM;) 
			RRdom = CC.Execute(sqlDom);
			
			ID_DOMANDAIQ = String(RRdom("ID_DOMANDAIQ"))
			TESTO_DOMANDA =	String(RRdom("TESTO_DOMANDA"))
			TIPO_DOMANDA = String(RRdom("TIPO_DOMANDA"))
			TIPO_SEL =	String(RRdom("TIPO_SEL"))
			
		}
		//LETTURA RISPOSTA
		RISPOSTA_UTE = ""
		if (String(i_ris[liv]("TIPO_ELEMENTO"))=="R"){
			sqlRisp = "SELECT ID_DOMANDAIQ, TIPO, TXT_RISPOSTA, TESTO_MAX, TESTO_MIN, IQ_MIN, IQ_MAX, FL_TIPRISP "
			sqlRisp += " From IQ_RISPOSTA where ID_RISPOSTAIQ = " + parseInt(i_ris[liv]("ID_ELEMENTO"))
		
'PL-SQL * T-SQL  
SQLRISP; = TransformPLSQLToTSQL (SQLRISP;) 
			RRrisp = CC.Execute(sqlRisp);
		
			ID_DOMANDAIQ = String(RRrisp("ID_DOMANDAIQ"))
			TIPO_RISPOSTA = String(RRrisp("TIPO"))
			TXT_RISPOSTA = String(RRrisp("TXT_RISPOSTA"))
			TESTO_MAX = String(RRrisp("TESTO_MAX"))
			TESTO_MIN = String(RRrisp("TESTO_MIN"))
			IQ_MIN = String(RRrisp("IQ_MIN"))
			IQ_MAX = String(RRrisp("IQ_MAX"))
			FL_TIPRISP = String(RRrisp("FL_TIPRISP"))
			
		
			sqlDom1 = "SELECT TIPO_DOMANDA, TIPO_SEL "
			sqlDom1 += " FROM IQ_DOMANDA WHERE ID_DOMANDAIQ =" + ID_DOMANDAIQ
'PL-SQL * T-SQL  
SQLDOM1; = TransformPLSQLToTSQL (SQLDOM1;) 
			RRdom1 = CC.Execute(sqlDom1);
			
			TIPO_DOMANDA = String(RRdom1("TIPO_DOMANDA"))
			TIPO_SEL =	String(RRdom1("TIPO_SEL"))
			
			sqlRisp1 = "SELECT RT.ID_RISULTATO, RES.ID_RISPOSTAIQ, RES.VALORE_RISPOSTA, RES.TESTO_RISPOSTA " 
			sqlRisp1 +=  " FROM IQ_RISULTATO RT , IQ_RISPOSTE RES WHERE  RT.IDUTENTE = " + Session("idutente") + " AND RT.ID_RISULTATO = RES.ID_RISULTATO AND RES.ID_RISPOSTAIQ =  " + parseInt(i_ris[liv]("ID_ELEMENTO"))
'PL-SQL * T-SQL  
SQLRISP1; = TransformPLSQLToTSQL (SQLRISP1;) 
			RRrisp1 = CC.Execute(sqlRisp1);
			
			if (!RRrisp1.eof){ 		 
				
				if (RRrisp1("VALORE_RISPOSTA") > "")
				{
					RISPOSTA_UTE = parseInt(RRrisp1("VALORE_RISPOSTA"))
				}
				else
				{
					if (RRrisp1("TESTO_RISPOSTA") > "")
					{
						RISPOSTA_UTE = RRrisp1("TESTO_RISPOSTA")
					}
					else
					{
						RISPOSTA_UTE = "1"
					}
				}
			}
		}		

		lista[il++] = new messaggio(parseInt(i_ris[liv]("ID_ELEMENTO")),
									String(i_ris[liv]("TIPO_ELEMENTO")),
									liv,
									TESTO_DOMANDA,
									TIPO_DOMANDA,
									TIPO_SEL,
									ID_DOMANDAIQ,
									TIPO_RISPOSTA,
									TXT_RISPOSTA,
									TESTO_MAX,
									TESTO_MIN,
									IQ_MIN,
									IQ_MAX,
									FL_TIPRISP,
									RISPOSTA_UTE,
									parseInt(i_ris[liv]("ID_BLOCCO"))
									);                                                  
			
		leggiMessaggi(parseInt(i_ris[liv]("ID_ELEMENTO")), liv + 1);
		
    	i_ris[liv].MoveNext();
	}
}

//---------------------------------------- MAIN -----------------------------------------------------------------------------------------------------------------------


var lista = new Array();
var i_ris = new Array();
var il = 0;
var i = 0;
var conta = 0;
var last = 0;
var appo = "NO";
var RespName = " ";
var idquest
var IdArea
var ii


// ID Questionario
var idquest = Request("idq");

// Id dell'Area
var IdArea = Request("ida");
// Porta i messaggi interessati dal database all'Array lista
leggiMessaggi(0, 0);

if ( !i_ris[0].EOF) {
		i_ris[0].MoveNext();
		if (!i_ris[0].EOF)	appo="SI"; 
	}
%>

<script language="JavaScript">
<!--#include Virtual = "/Include/help.inc"-->


</script>


<%
	var descarea = 1
	var appoArea
	var numaree
	
	sSQL = "select count(*) as totaree from iq_area where id_infoiq = " + idquest + " order by id_areaiq"
	var rsArea = server.CreateObject ("adodb.recordset")
	rsArea.Open(sSQL, CC); 
	appoArea = rsArea("totaree")

	if(appoArea > 1 )
	{
		numaree = 2
	}
	
	sqlArea = "SELECT DESC_AREAIQ, ID_AREAIQ FROM IQ_AREA WHERE ID_AREAIQ =" + IdArea 
	RRArea  = Server.createObject("adodb.recordset");
	RRArea.Open(sqlArea, CC);
	
	if (!RRArea.eof){ 		 
			RespName = RRArea.Fields("DESC_AREAIQ")
	}		

	sqlQuest = "SELECT DESC_QUEST FROM INFO_QUEST WHERE ID_INFOQUEST =" + idquest 
	RRQuest  = Server.createObject("adodb.recordset");
	RRQuest.Open(sqlQuest, CC);
	
	if (!RRQuest.eof){ 		 
			RespQuest = RRQuest.Fields("DESC_QUEST")
	}		
	%> 
	<!--Epili 13/01/02 Per poter utilizzare le strutture standard.-->


	<script language="VBScript" runat="server"> 
'PL-SQL * T-SQL  
		Server.Execute ("/strutt_testa1.asp") 
	</script>
	<br>
	<p align="left"><span class="tbltext"><b>&nbsp;QUESTIONARI DI GRADIMENTO</b></span><br>
	<p align="left"><span class="tbltext"><b>&nbsp;QUESTIONARIO:</b> <%=Server.HTMLEncode(RespQuest)%></span><br>
<%	if (numaree==2){ %>
	<span class="tbltext"><b>&nbsp;SEZIONE:</b> <%=Server.HTMLEncode(RespName)%></span>
<%
		}
%>	</p>	

	<%
	var sm
	var tpr
	//sm = Session("mask")
	sm = "05"

    if (lista.length==0){	
       Response.Write(" <br><br><span class=textred><b>NON SONO PRESENTI DOMANDE</b></span><br><br><br><br> ");
    }else{	
		for (i=0;i<lista.length;i++) {
			 tpr = lista[i].id_domandaiq 
			 Response.Write("<div align=left id='div" + tpr + "' name='div" + tpr + "' style='visibility:visible'><table border=0 width=100% ><tr class=tblsfondo>")
		     Response.Write("<td width=2% height=1 class=textblack colspan=2>");
   				
		    
			     for (j=0;j<lista[i].livello;j++)
		     
					Response.Write("<img src='" + Session("Progetto") + "/images/blank_trasp.gif' width=20 height=16>")
					if (lista[i].tipo_elemento=="D"){
						Response.Write("</TD><TD class=textblack>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src='" + Session("Progetto") + "/images/re.gif' >")						   %> 
						   <b><%=lista[i].testo_domanda%></b>
						   <!-- - <%=lista[i].tipo_elemento%> - <%=lista[i].id_elemento%> - <%=lista[i].tipo_sel%> - <%=lista[i].tipo_domanda%>-->
						   <%
 					}else{
				 		Response.Write("</TD><TD class=textblack><img src='" + Session("Progetto") + "/images/blank_trasp.gif' width=30 height=16>")	
						%>
						<!--#INCLUDE file = "QGR_inc_antrisp.asp"-->
						<%
					}
					Response.Write("</td></tr></table></div>")
		}
	} 

  %>

<script language="VBScript" runat="server"> 
'PL-SQL * T-SQL  
	Server.Execute ("/Include/ckProfile.asp") 
</script>
<% 
if (sm != "01" ){ 
	%>
<br><br>	
  <table width="730px" cellspacing="0" cellpadding="0" border="0">
	<tr>
		<td align="center">
			<b class="textblack">
			<%
			var nPag
			if (IdArea == 0) 
			{
				nPag = 2
			}
			else
			{
				nPag = 1
			}
			%>
			<a onmouseover="javascript:window.status='' ; return true" href="javascript:history.go(-<%=nPag%>)">			
				<img src="<%=Session("progetto")%>/images/indietro.gif" border="0" alt="Torna alla pagina precedente">
			</a>
			<!--a class="textRed" href="javascript:history.back()" onmouseover="window.status =' '; return true">Pagina Precedente</a-->
			</b>
		</td>
	</tr>
</table>
	<%
} 			
%>	

<br><br>
<table border="0" width="750" height="8" cellpadding="0" cellspacing="0"> 
<tr height="1">
	<td width="100%" background="/DEMOGP/images/separazione.gif"></td>
</tr>
</table>

<br>


<form id="frmReload" name="frmReload" action="QGR_VisAnteprima.asp" method="post">
	<input type="hidden" name="txtTipoRisposta" id="txtTipoRisposta" value>
	<input type="hidden" name="ida" id="ida" value="<%=IdArea%>">
	<input type="hidden" name="idq" id="idq" value="<%=idquest%>">	
</form> 
   
<%    
// Chiude la connessione al database
CC.Close();
%>

<!--Epili 13/01/02 Per poter utilizzare le strutture standard.-->


