<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%
Sub Inizio()
	dim sFunzione
	dim sTitolo
	dim sCommento
	dim bCampiObbl
	
	sFunzione = "GESTIONE QUESTIONARI DI GRADIMENTO"
	sTitolo = "ELENCO DOMANDE"
	sCommento = "Elenco delle Domande riferite al Questionario di Gradimento.Per inserire la relativa risposta cliccare sull'icona relativa alla colonna <B>Risposte</B>.<BR>Per inserire una <B>nuova domanda</B> cliccare su il link sottostante."
	bCampiObbl = false
	sHelp = "/Pgm/help/Formazione/QGradimento/QGR_VisDomanda/"	
%>
	<!--#include virtual="include/SetTestata.asp"-->
<%
End Sub
'----------------------------------------------------------------------
Sub ElencoD()
dim sSQL, nIdQuest, nIdArea
dim rsDomanda, rsQuestAreaArea
dim nCRighe

nIdQuest = request("idq")
nIdArea =  request("ida")

sSQL = "select q.desc_quest, a.desc_areaiq from iq_area a, info_quest q where q.id_infoquest = a.id_infoiq and a.id_areaiq = " & nIdArea & " and q.id_infoquest = " & nIdQuest
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
set rsQuestArea = cc.execute(sSQL)
if not rsQuestArea.eof then
%>
	<table width="500" border="0">
		<tr><td class="tbltext3"><b>Questionario:</b> &quot;<%=rsQuestArea("desc_quest")%>&quot;</td></tr>
		<tr><td class="tbltext3"><b>Sezione:</b> &quot;<%=rsQuestArea("desc_areaiq")%>&quot;</td></tr>
	</table>
	<br>
<%	
end if
rsQuestArea.close
nCRighe = 0

sSQL = "select id_domandaiq, testo_domanda from iq_domanda where id_areaiq = " & nIdArea & " and id_infoquest = " & nIdQuest & " order by id_domandaiq"
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
set rsDomanda = CC.execute(sSQL)
%>
<table width="500" border="0">
	<tr class="sfondocomm">
        <td width="20" align="center"><b>#</b></td>
        <td><b>Domanda</b></td>
       <td width="60" align="center"><b>Risposte</b></td>
	</tr>
<%
do while not rsDomanda.eof
	nCRighe = nCRighe + 1
	
'''9/7/2003: PASSATI VALORI TRAMITE FORM ANZICHE' QUERYSTRING %>
	<form name="frmModDomanda<%=nCRighe%>" method="post" action="QGR_ModDomanda.asp">
		<input type="hidden" name="id" value="<%=rsDomanda("id_domandaiq")%>">
		<input type="hidden" name="idq" value="<%=nIdQuest%>">
		<input type="hidden" name="ida" value="<%=nIdArea%>">
	</form>
	<form name="frmRisposta<%=nCRighe%>" method="post" action="QGR_VisRisposta.asp">
		<input type="hidden" name="idq" value="<%=nIdQuest%>">
		<input type="hidden" name="ida" value="<%=nIdArea%>">
		<input type="hidden" name="idd" value="<%=rsDomanda("id_domandaiq")%>">
	</form>
<%'''FINE 9/7/2003%>
	<tr class="tblsfondo">
		<td class="tbldett">
			<%=nCRighe%>
		</td>
<%'''9/7/2003: PASSATI VALORI TRAMITE FORM ANZICHE' QUERYSTRING %>
		<td>
			<!--a href="QGR_ModDomanda.asp?id=<%'=rsDomanda("id_domandaiq")%>&amp;ida=<%'=nIdArea%>&amp;idq=<%'=nIdQuest%>" class="tblagg">
				<%'=rsDomanda("testo_domanda")%>
			</a-->
			<a href="javascript:frmModDomanda<%=nCRighe%>.submit();" class="tblagg">
				<%=rsDomanda("testo_domanda")%>
			</a>
		</td>
		<td align="center">
			<!--a href="QGR_VisRisposta.asp?idq=<%'=nIdQuest%>&amp;ida=<%'=nIdArea%>&amp;idd=<%'=rsDomanda("id_domandaiq")%>">
				<img src="<%'=Session("progetto")%>/images/formazione/re.gif" border="0" alt="Elenco delle Risposte" WIDTH="12" HEIGHT="16">
			</a-->
			<a href="javascript:frmRisposta<%=nCRighe%>.submit();">
				<img src="<%=Session("progetto")%>/images/formazione/re.gif" border="0" alt="Elenco delle Risposte" WIDTH="12" HEIGHT="16">
			</a>
		</td>
<%'''FINE 9/7/2003%>
	</tr>	
<%
	rsDomanda.movenext
loop
rsDomanda.close
%>
</table>
<br>
<%'''9/7/2003: PASSATI VALORI TRAMITE FORM ANZICHE' QUERYSTRING %>
	<form name="frmArea" method="post" action="QGR_VisArea.asp">
		<input type="hidden" name="id" value="<%=nIdQuest%>">
	</form>
	<form name="frmDomanda" method="post" action="QGR_InsDomanda.asp">
		<input type="hidden" name="idq" value="<%=nIdQuest%>">
		<input type="hidden" name="ida" value="<%=nIdArea%>">
	</form>
<%'''FINE 9/7/2003%>

<table width="500">
	<tr>
		<td align="middle" width="50%">
<%'''9/7/2003: PASSATI VALORI TRAMITE FORM ANZICHE' QUERYSTRING	
			'''PlsLinkRosso "QGR_InsDomanda.asp?idq=" & nIdQuest & "&ida=" & nIdArea, "Nuova Domanda" %>
			<a onmouseover="javascript:window.status='' ; return true" href="javascript:frmDomanda.submit();" class="textred">
				<b>Nuova Domanda</b>
			</a> 
		</td>
		<td align="middle" width="50%">
<%'''9/7/2003: PASSATI VALORI TRAMITE FORM ANZICHE' QUERYSTRING	
			'''PlsLinkRosso "QGR_VisArea.asp?id=" & nIdQuest, "Elenco delle Aree" %>		
			<a onmouseover="javascript:window.status='' ; return true" href="javascript:frmArea.submit();" class="textred">
				<b>Elenco delle Sezioni</b>
			</a> 
		</td>
	</tr>
</table>
<%
End Sub
'----------------------------------------------------------------------
'M A I N
%>
<HTML>
<HEAD>
<TITLE><%=Session("TIT")%> [ <%=Session("login")%> ] </TITLE>
<LINK REL=STYLESHEET TYPE="text/css" HREF='<%=session("progetto")%>/fogliostile.css'>
</head>
<body class=sfondocentro topmargin="2" leftmargin="0">
<center>
<!--#include virtual="/include/openconn.asp"-->
<!--#include virtual="include/SetPulsanti.asp"-->

<%
	Inizio()
	ElencoD()
%>
<!--#include virtual="/include/closeconn.asp"-->
</body>
</html>
