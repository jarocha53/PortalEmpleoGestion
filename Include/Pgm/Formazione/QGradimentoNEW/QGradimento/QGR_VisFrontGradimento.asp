<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<script language=javascript>
	function Carica_Pagina(npagina)
	{
		document.frmPagina.pagina.value = npagina;
		document.frmPagina.submit();
	}	
</script>
<%
'----------------------------------------------------------------------
Sub Inizio()
	dim sFunzione
	dim sTitolo
	dim sCommento
	dim bCampiObbl
	
	sFunzione = "QUESTIONARIO"
	sTitolo = "QUESTIONARIO"
	sCommento = "Le tue opinioni ci permettono di migliorare costantemente il nostro servizio. " &_
				"In questa pagina puoi trovare alcuni questionari relativi alle sezioni e funzioni del sito." &_
				"Fai click sul titolo del questionario e facci sapere che cosa ne pensi!"
	bCampiObbl = false
	sHelp="/Pgm/help/formazione/QGradimento/QGR_VisFrontGradimento"	
%>
	<!--#include virtual="include/SetTestata.asp"-->
<%
End Sub
'----------------------------------------------------------------------
Sub ElencoQ()

	dim sSQL
	dim rsQuest
	dim nProg, nViewTit

	nProg = 0
	nViewTit = 0

	IF SESSION("RUOFU") = "DI" THEN
		aFase = SelPrgFase(session("creator"))
	END IF
	
	Set rsQuest = Server.CreateObject("ADODB.Recordset")
		sSQL =	"SELECT a.id_infoquest, i.desc_quest, i.fl_repeat"
		
		IF SESSION("RUOFU") = "DI" THEN
			sSQL =	sSQL & ", a.fl_attivazione"
		END IF
		
		sSQL = sSQL & " FROM info_quest i, ass_quest a"
		 
		sSQL = sSQL &	" WHERE (a.cod_ruofu ='" & Session("Ruofu") & "' or a.cod_ruofu is null)" &_
						" AND (a.cod_rorga ='" & Session("rorga") & "' or a.cod_rorga is null)" &_
						" AND a.id_infoquest=i.id_infoquest"
		
		IF SESSION("RUOFU") = "DI" THEN
			sSQL =	sSQL &	" AND (a.cod_sessione ='" & aFase(1) & "' or a.cod_sessione is null)" &_
							" AND (a.id_bando in(select id_bando from domanda_iscr" &_
							" where id_persona = " & session("creator") & ") or a.id_bando is null)" 
		END IF
		
		sSQL =	sSQL & " ORDER BY a.id_infoquest"	

'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	rsQuest.Open sSQL, CC, 3
'''fine 25/6/2003.
	
	Record=0
	nTamPagina=5

	If Request("pagina")= "" Then
		nActPagina=1
	Else
		nActPagina=Clng(Request("pagina"))
	End If 
%>

	<table border="0" cellspacing="1" cellpadding="2" width="500">
		<tr class="sfondocommFad">
		    <td width="20" colspan="4" align="left">&nbsp;</td>
		</tr>
<%	
	IF NOT rsQuest.eof THEN
		rsQuest.PageSize	= nTamPagina
		rsQuest.CacheSize	= nTamPagina	
		nTotPagina			= rsQuest.PageCount		
		If nActPagina < 1 Then
			nActPagina = 1
		End If
		If nActPagina > nTotPagina Then
			nActPagina = nTotPagina
		End If
		rsQuest.AbsolutePage=nActPagina
		nTotRecord=0
		
		While rsQuest.EOF <> True And nTotRecord < nTamPagina
'''26/6/2003
           
			flRepeat = rsQuest("FL_REPEAT")
			sQuestAttiv = "no"
			
			IF Session("RUOFU") <> "DI" or isnull(Session("RUOFU"))then
				sQuestAttiv = "si"
			ELSE
				sFlAttiv= rsQuest("FL_ATTIVAZIONE")
				
				Select case sFlAttiv
					case "I"
						If cdate(date()) = cdate(aFase(2)) then
							sQuestAttiv="si"
						End if
					case "F"
						If cdate(date()) = cdate(aFase(3)) then
							sQuestAttiv="si"
						End if
					case "D"
						If cdate(date()) => cdate(aFase(2)) and cdate(date()) <= cdate(aFase(3)) then
							sQuestAttiv="si"
						End if
					case else
					    sQuestAttiv="si"	
				End select
			END IF
			
'''fine 26/6/2003.	
			sSQL = "select count(*) as totaree from iq_area where id_infoiq = " & rsQuest("id_infoquest") 
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
					set rsArea = CC.Execute(sSQL)
					if not rsArea.eof then
						appoArea = rsArea("totaree")
					end if	
					
			sSQL = "select count(*) as totInfo from IQ_RISULTATO WHERE ID_INFOQUEST = " & rsQuest("id_infoquest") 
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
					set rsInfo = CC.Execute(sSQL)
					if not rsInfo.eof then
						appoInfo = rsInfo("totInfo")
					end if
				
			if cint(appoArea) = cint(appoInfo) then	
					sContaEseguito = 0	
					sSQL =	"SELECT DT_ESEC, ESEGUITO FROM IQ_RISULTATO" &_
							" WHERE ID_INFOQUEST = " & rsQuest("id_infoquest") &_
							" AND IDUTENTE = " & SESSION("IDUTENTE")& " ORDER BY DT_ESEC "					
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
					set rsEsec = CC.Execute(sSQL)
						if not rsEsec.eof then
							do while not rsEsec.eof
								if rsEsec("ESEGUITO") = "S" then
									sContaEseguito = sContaEseguito + 1								
									sEseguito = rsEsec("ESEGUITO")
									sDtEsec = ConvDateToString(rsEsec("DT_ESEC"))								
								end if
								rsEsec.movenext
							loop
							if cint(sContaEseguito) <> cint(appoArea) then
								sEseguito = ""
								sDtEsec = ""							
							end if
							rsEsec.Close
							set rsEsec = nothing
						end if							
			else
				sEseguito = ""
				sDtEsec = ""
			end if					
			nProg = nProg + 1
			
'''11/7/2003	
		
			If sQuestAttiv="no" then
				nProg = nProg - 1
			Else
				nViewTit = nViewTit + 1
				IF nViewTit = 1 THEN %>
					<tr class="sfondocommFad">
					    <td width="20" align="left"><b>#</b></td>
					    <td width="253"><b>Titolo</b></td>
					    <td width="80" align="left"><b>Data esecuzione</b></td>
					    <td width="74" align="left"><B>Anteprima</B></td>
					    <td width="170" align="left"><B>Status</B></td>
					</tr>
<%				END IF 
'''fine 11//2003. %>
				<tr class="tblsfondoFad">
					<td align="left" class="tblDettFad">
						<%=nProg + (nActPagina * nTamPagina) - nTamPagina%>
					</td>
					<td>
						<%
						if sEseguito="S" and cint(flRepeat)=0 then%> 
							<span class="tblAggFad"><%=rsQuest("desc_quest")%></span>
						<%
						else%>
							<a onmouseover="window.status =' '; return true" 
								title="Seleziona questionario"
								href="Javascript:frmQuest<%=nProg%>.submit()" class="tblAggFad">
								<%=rsQuest("desc_quest")%>
							</a>			
						<%
						end if%>
					</td>
					<td align="left" class=tblDettFad>
						<%=sDtEsec%>&nbsp;
					</td>
					<td align="center">
					<%' if sDtEsec <> "" then %>				
							<a href="javascript:frmAnteprima<%=nProg%>.submit();" class="tblAgg">
					<%	'end if%>
							<img src="<%=Session("progetto")%>/images/formazione/txt.gif" border="0" alt="Anteprima Questionario di Gradimento" WIDTH="20" HEIGHT="21">
						</a>
				<%if sEseguito = "S" then %>
				   <%'Inizio ciclo per verifica ripetibilitÓ di un questionario%>
					<%if cint(flRepeat)=0 then %>
						<td align="left" class=tblDettFad>
							<img title="Questionario eseguito" src="<%=Session("progetto")%>/images/formazione/okv.gif" 
								border="0" WIDTH="17" HEIGHT="15">Eseguito
						</td>
					<%else%>	
						<td align="left" class=tblDettFad>
							<img title="Questionario eseguito" src="<%=Session("progetto")%>/images/formazione/okv.gif" 
								border="0" WIDTH="17" HEIGHT="15">Eseguito - ripetibile
						</td>
					<%end if %>	
				  <%'Fine ciclo per verifica ripetibilitÓ di un questionario%>	
				<%else%>
					<td align="left" class=tblDettFad>
						<img title="Questionario non eseguito" src="<%=Session("progetto")%>/images/formazione/ko.gif" border="0" 
							WIDTH="17" HEIGHT="15">Da eseguire
					</td>
				<%end if %>
				</tr>	
				<form method=post action="QGR_VisArea.asp" name="frmQuest<%=nProg%>">
					<input type=hidden name="id"	value="<%=clng(rsQuest("id_infoquest"))%>">
					<input type="hidden" name="mod" value="<%=3%>">
				</form>
				<form name="frmAnteprima<%=nProg%>" method="post" action="QGR_VisArea.asp">
					<input type="hidden" name="id" value="<%=rsQuest("id_infoquest")%>">
					<input type="hidden" name="mod" value="<%=4%>">
				</form>
<%			End if
		nTotRecord=nTotRecord + 1 
		rsQuest.movenext	
		Wend 
		
		If nViewTit = 0 then %>
			<tr>
				<TD align=center class="tblText3">
					Non ci sono questionari disponibili.
				</TD>
			</tr>
<%
		End if %>
	</table>
	
		<TABLE border=0 width=470 align=center>
			<form name="frmPagina" method="post" action="QGR_VisFrontGradimento.asp">
			    <input type="hidden" name="pagina" value>
			</form>	
			<tr>
				<%	
				if nActPagina > 1 then
					Response.Write "<td align=right width=480>"
					Response.Write "<A HREF=""javascript:Carica_Pagina('" & nActPagina-1 & "')"">"
					Response.Write "<img border=0 src=" & Session("Progetto") & "/images/precedente.gif></A></td>"
				end if
				if nActPagina < nTotPagina then
					Response.Write "<td align=right>"
					Response.Write "<A HREF=""javascript:Carica_Pagina('" & nActPagina+1 & "')"">"		
					Response.Write "<img border=0 src=" & Session("Progetto") & "/images/successivo.gif></A></td>"
				end if
				%>
			</tr>
		</TABLE>	
<%	ELSE %>	
		<tr>
			<TD align=center class="tblText3">
				Non ci sono questionari disponibili.
			</TD>
		</tr>
	</table>
<%	END IF %>
	<br>
<%
	rsQuest.close
End Sub
'----------------------------------------------------------------------
'M A I N
%>
<!--#include virtual="/strutt_testa2.asp"-->
<!--#include virtual="/include/openconn.asp"-->
<!--#include virtual="/include/ControlDateVB.asp"-->
<!--#include virtual="include/SetPulsanti.asp"-->
<!--#include Virtual = "/include/ControlFase.asp"-->

<%
if ValidateService(session("idutente"),"QGR_VisFrontGradimento", CC) <> "true" then 
	response.redirect "/util/error_login.asp"
end if
	
Inizio()
ElencoQ()
%>
<!--#include virtual="/include/closeconn.asp"-->
<!--#include virtual="/strutt_coda2.asp"-->
