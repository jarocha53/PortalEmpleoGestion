<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%
Sub ControlliJavaScript()
%>
<script LANGUAGE="Javascript">
<!--#include virtual="include/ControlString.inc"-->

function Congruenza(sTD,sT,sTS,sDTD,sDT,sDTS) {
	if ((sTD == "A") && (sTS != "")){
		alert('Attenzione: Una Domanda ' + sDTD + ' non pu� avere come Tipo Selezione <' + sDTS + '>. VERIFICARE!')
		return false
	}
	if (sTD == "C") {
		if ((sT == "T") && (sTS == "")) {
		alert('Attenzione: Per una Domanda ' + sDTD + ' deve essere specificato il <Tipo Selezione>. VERIFICARE!')
			return false
		}
		if ((sT == "V") && (sTS == "M")) {
		alert('Attenzione: Una Domanda ' + sDTD + ' con risposta di Tipo ' + sDT + ' non pu� avere come Tipo Selezione <' + sDTS + '>. VERIFICARE!')
			return false
		}
		if ((sT == "V") && (sTS == "")) {
		alert('Attenzione: Per una Domanda ' + sDTD + ' con risposta di Tipo ' + sDT + ' deve essere specificato il <Tipo Selezione>. VERIFICARE!')
			return false
		}
	}
	if ((sTD == "S") && (sTS == "M")){
		alert('Attenzione: Una Domanda ' + sDTD + ' non pu� avere come Tipo Selezione <' + sDTS + '>. VERIFICARE!')
		return false
	}
	if ((sTD == "S") && (sTS == "")){
		alert('Attenzione: Per una Domanda ' + sDTD + ' deve essere specificato il <Tipo Selezione>. VERIFICARE!')
		return false
	}
}

function ValidateInputNoAccenti(InputString)
{
	var sSepInput = InputString.split("\\");
	sFile = sSepInput[sSepInput.length-1];
	var anyString = sFile;
	
	for ( var i=0; i<=anyString.length-1; i++ ){
	
		if ((anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			(anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			(anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			(anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			(anyString.charAt(i) == "�") || (anyString.charAt(i) == "�") || 
			(anyString.charAt(i) == "�") )
			{
			alert ("Non indicare lettere accentate.");
			return(false)
			}
	}
	return(true);
}


//////////**********////////////////////**********//////////
function ControllaCampi() {
	if ((frmRisposta.rTipo[0].checked == false) && (frmRisposta.rTipo[1].checked == false)) {
		alert("Indicare il Tipo di Risposta")
		frmRisposta.rTipo[0].focus()
		return false
	}

	var Tipo
	if (document.frmRisposta.rTipo[0].checked == true){
		Tipo = document.frmRisposta.rTipo[0].value
	}
	else {
		Tipo = document.frmRisposta.rTipo[1].value
	}

	if (Tipo == "V") {
		frmRisposta.txtMin.value = TRIM(frmRisposta.txtMin.value)
		frmRisposta.txtMax.value = TRIM(frmRisposta.txtMax.value)

		if ((frmRisposta.txtMin.value == "") && (frmRisposta.txtMax.value == "")) {
			alert("Compilare i campi Testo Valore Min e Testo Valore Max")
			frmRisposta.txtMin.focus()
			return false
		}
		if ((frmRisposta.txtMin.value == "") || (frmRisposta.txtMax.value == "")) {
			alert("Compilare entrambi i campi Testo")
			frmRisposta.txtMin.focus()
			return false
		}	

		if (!ValidateInputNoAccenti( frmRisposta.txtMin.value )){
			frmRisposta.txtMin.focus()
			return false
		}
		if (!ValidateInputNoAccenti( frmRisposta.txtMax.value )){
			frmRisposta.txtMax.focus()
			return false
		}
		
		if ((frmRisposta.VMin.value == "") && (frmRisposta.VMax.value == "")) {
			alert("Compilare i campi Valore Min e Valore Max")
			frmRisposta.VMin.focus()
			return false
		}
		if ((frmRisposta.VMin.value == "") || (frmRisposta.VMax.value == "")) {
			alert("Inserire tutti i Valori")
			frmRisposta.VMin.focus()
			return false
		}
		if ( (!Numerico(frmRisposta.VMin)) || (!Numerico(frmRisposta.VMax)) ) {
			return false;
		}
		if (parseInt(frmRisposta.VMin.value,10) >= parseInt(frmRisposta.VMax.value,10)) {
			alert("Valore Min non pu� essere maggiore o uguale a Valore Max")
			frmRisposta.VMin.focus()
			return false
		}

		if (frmRisposta.txtTipoSelezione.value=="M"){
			alert('Una Domanda con risposta di Tipo <Valore> deve avere come Tipo Selezione <Escludente>. Modificare la domanda!')
			return false
		}
		
		if (frmRisposta.sAzione.value == "INS") {
			document.frmRisposta.action = "QGR_CnfInsRisp.asp"
		}
		else {
			document.frmRisposta.action = "QGR_CnfModRisp.asp"
		}
	}
	else {
		var nEl
		var radio
		var testo
		
		nEl = (document.frmRisposta.nElementi.value)
		//for (i=1; i<=nEl; i++){
		for (i=1; i=nEl; i++){
			radio = eval("document.frmRisposta.rFlTipoRisp" + i)
			testo = eval("document.frmRisposta.txtRisposta" + i)

			if (!ValidateInputNoAccenti( testo.value )){
				testo.focus();
				return false;
			}
			
			if ((radio[0].checked == false) && (radio[1].checked == false)) {
				alert("Indicare se la risposta � Chiusa o Aperta");
				radio[0].focus();
				return false;
			}
			
			testo.value = TRIM(testo.value)
			
			if ((radio[0].checked == true) && (testo.value == "")) {
				alert('Inserire il Testo della Risposta')
				testo.focus();
				return false;	
			}
			return true	
		}
	}
	return true
}

//////////**********////////////////////**********//////////
function Numerico(campo) {
	var dim=campo.size;
	var anyString = campo.value;

	for ( var i=0; i<=anyString.length-1; i++ ) {
		if ( (anyString.charAt(i) >= "0") && (anyString.charAt(i) <= "9") ) 
		{
		}
		else {
			alert("Attenzione: valore immesso non numerico!");
			campo.focus();
			return(false);
		}
	}
	return true;
}	
	
//////////**********////////////////////**********//////////
function Reload(sValue) {
	document.frmReload.txtTipoRisposta.value = sValue
	document.frmReload.submit()
}

//////////**********////////////////////**********//////////
function Elimina() {
	var nElem
	var ok
	var chkbox
	
	ok = 0
	
	if (document.frmRisposta.rTipo[0].checked == true) {
		nElem = ((document.frmRisposta.nElementi.value) - 1);
		for(i=1; i<=nElem; i++) {
			chkbox = eval("document.frmRisposta.chkElim" + i)
			if (chkbox.checked){
				ok = 1
				break
			}
		}
	}
	else {
		ok = 1 
	}
	
	if (ok == 0) {
		alert('Selezionare almeno un record!')
		return false
	}
	else {
		if (confirm("Confermi la cancellazione?") == true) {
			document.frmRisposta.action = "QGR_DelRisposta.asp"
			document.frmRisposta.submit()	
		}
		return false
	}
}

//////////**********////////////////////**********//////////
function ModRisposta(nId){
	var windowArea
	if (windowArea != null) {
		nomefinestra.close(); 
	}
	windowArea = window.open("QGR_ModRisposta.asp?P=.....&nId=" + nId,"Info","Height=350px; Width=520px; center: Yes; help: No; resizable: No; status: No");	
}
//////////////////////////////////////////////////////////
function ChiudiTutto()
{
	opener.document.frmReload.submit();
	self.close();

}
</script>
<%
End Sub
'--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++
Sub Inizio()
	dim sFunzione
	dim sTitolo
	dim sCommento
	dim bCampiObbl
	
	sFunzione = "GESTIONE QUESTIONARI DI GRADIMENTO"
	sTitolo = "ELENCO RISPOSTE"
	sCommento = "Elenco delle risposte relative al Questionario di Gradimento. Selezionando la  modalit� <B>Testo</B> si puo' optare tra una risposta chiusa o aperta.Con l'icona accanto si puo' modificare il testo, altrimenti selezionando il campo <B>Elimina</B> si toglie la relativa risposta.<BR>Selezionando il campo <B>Valore</B> si offre la possibilit� di inserire un range di valori.Per rendere piu' chiara l'esposizione � possibile inserire un testo.<BR>Per aggiungere le domande cliccare su <B>Aggiungi</B>."
	bCampiObbl = true
	sHelp = "/Pgm/help/Formazione/QGradimento/QGR_VisRisposta/"	
%>
	<!--#include virtual="include/SetTestata.asp"-->
<%
End Sub
'--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++--++
Sub ElencoR()

	dim sSQL
	dim  sTipoRisposta
	dim OpSQL

	nIdDom = request("idr")
	nIdArea = request("ida")
	nIdQuest = request("idq")
	nIdBlocco = request("idb")
	nIdElem = request("ide")
	
	sTipoRisposta = Request.Form("txtTipoRisposta")
	
	'----------QUERY SU DIZ_DATI----------'
		'Prendo i valori del campo TIPO della risposta (T|Testo|V|Valore)
		sSQL =	"SELECT id_campo_desc FROM diz_dati WHERE id_tab='IQ_RISPOSTA'" &_
				" AND id_campo='TIPO'"
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
		SET rsDdati = CC.Execute(sSQL)
			sTip = rsDdati("id_campo_desc")
			aTip = split(sTip,"|")
		rsDdati.close
		set rsDdati = nothing
	
		'Prendo i valori del campo TIPO_DOMANDA della domanda (A|Aperta|C|Chiusa|S|Semiaperta)
		sSQL =	"SELECT id_campo_desc FROM diz_dati WHERE id_tab='IQ_DOMANDA'" &_
				" AND id_campo='TIPO_DOMANDA'"
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
		SET rsDdati = CC.Execute(sSQL)
			sTipDom = rsDdati("id_campo_desc")
			aTipDom = split(sTipDom,"|")
		rsDdati.close
		set rsDdati = nothing
	
		'Prendo i valori del campo TIPO_SEL della domanda (E|Escludente|M|Multipla)
		sSQL =	"SELECT id_campo_desc FROM diz_dati WHERE id_tab='IQ_DOMANDA'" &_
				" AND id_campo='TIPO_SEL'"
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
		SET rsDdati = CC.Execute(sSQL)
			sTipSel = rsDdati("id_campo_desc")
			aTipSel = split(sTipSel,"|")
		rsDdati.close
		set rsDdati = nothing
	'----------FINE QUERY SU DIZ_DATI----------'

	sSQL = "Select Testo_Domanda from iq_domanda where id_domandaiq = " & nIdDom
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	set rsDomanda = cc.execute(sSQL)
		if not rsDomanda.eof then
%>
			<table width="500" border="0">
				<tr>
					<td class="tbltext3">
						Domanda: &quot;<%=rsDomanda("testo_domanda")%>&quot;
					</td>
				</tr>
			</table>
			<br>
<%	
		end if
	rsDomanda.close
%>	
	
	<form id="frmReload" name="frmReload" action="QGR_VisRisposta.asp" method="post">
		<input type="hidden" name="txtTipoRisposta" id="txtTipoRisposta" value>
		<input type="hidden" name="Idr" id="Idr" value="<%=nIdDom%>">
		<input type="hidden" name="Ida" id="Ida" value="<%=nIdArea%>">
		<input type="hidden" name="Idq" id="Idq" value="<%=nIdQuest%>">	
		<input type="hidden" id="Idb" name="Idb" value="<%=nIdBlocco%>">
		<input type="hidden" id="Ide" name="Ide" value="<%=nIdElem%>">

	</form>
<%
	sSql = "Select tipo_sel from iq_domanda where id_domandaiq=" & nIdDom
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	set rsCongruenza = CC.Execute(sSql)
		if not rsCongruenza.eof then
			sTipoSelezione = rsCongruenza("tipo_sel") 
		end if
	rsCongruenza.Close 
	
	
	sSQL =	"Select id_rispostaiq, tipo, testo_min, testo_max, iq_min, iq_max, txt_risposta, fl_tiprisp, dt_tmst" &_
			" from iq_risposta where id_domandaiq = " & nIdDom & " order by id_rispostaiq"
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	set rsRisposta = CC.execute(sSQL)
		IF not rsRisposta.eof then
		'Esiste risposta
			Risposte = "Si"
			sRsTip_Risp = rsRisposta("tipo")
			If sTipoRisposta = "" then
				sTipoRisposta = sRsTip_Risp
			End if
			
			'-----Controllo di congruenza-----'
			sSql = "Select tipo_domanda from iq_domanda where id_domandaiq=" & nIdDom
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
			set rsCongruenza = CC.Execute(sSql)
				if not rsCongruenza.eof then
					sTipoDomanda = rsCongruenza("tipo_domanda")
				end if
			rsCongruenza.Close 
			'OOOOO'
			for i=0 to Ubound(aTipDom) step 2
				if sTipoDomanda = aTipDom(i) then
					sDescTipoDomanda = aTipDom(i+1)
				end if
			next
			'OOOOO'
			for i=0 to Ubound(aTipsel) step 2
				if sTipoSelezione = aTipsel(i) then
					sDescTipoSelezione = aTipsel(i+1)
				end if
			next
			'OOOOO'
			for i=0 to Ubound(aTip) step 2
				if sRsTip_Risp = aTip(i) then
					sDescTipo = aTip(i+1)
				end if
			next
			'OOOOO' %>
			<script language="javascript">
				Congruenza('<%=sTipoDomanda%>','<%=sRsTip_Risp%>','<%=sTipoSelezione%>','<%=sDescTipoDomanda%>','<%=sDescTipo%>','<%=sDescTipoSelezione%>')
			</script>
<%			'----- Fine Controllo di congruenza-----'
		ELSE
		'Non esiste risposta
			Risposte="No"
			sRsTip_Risp = ""
		END IF
%>
	<form name="frmDomanda" method="post" action="QGR_Sessioni.asp">
		<input type="hidden" name="idq" value="<%=nIdQuest%>">
		<input type="hidden" name="ida" value="<%=nIdArea%>">
	</form>

	<form method="post" action="QGR_CnfInsRisp.asp" id="frmRisposta" name="frmRisposta">
	<table width="500" border="0">
		<tr>
			<td class="tbltext1" width="35%">
				<b>Tipo Risposta *</b>
				<input type="hidden" name="nIDdom" id="nIDdom" value="<%=nIdDom%>">
				<input type="hidden" name="nIdarea" id="nIdarea" value="<%=nIdArea%>">
				<input type="hidden" name="nIdquest" id="nIdquest" value="<%=nIdQuest%>">	
				<input type="hidden" id="nIdblocco" name="nIdblocco" value="<%=nIdBlocco%>">
				<input type="hidden" id="nIdelem" name="nIdelem" value="<%=nIdElem%>">
				<input type="hidden" name="txtTipoSelezione" id="txtTipoSelezione" value="<%=sTipoSelezione%>">
			</td>
			<td class="textblacka">
			<%
			for i=0 to Ubound(aTip) step 2
				Response.Write aTip(i+1)%>
				<input type="radio" id="rTipo" name="rTipo" value="<%=aTip(i)%>" onclick="JavaScript:Reload(this.value);" <%If sTipoRisposta = aTip(i) then Response.Write "checked"%><%If sRsTip_Risp <> aTip(i) and sRsTip_Risp <> "" then Response.Write "disabled"%>>&nbsp;&nbsp;&nbsp;
			<%
			next%>
			</td>
		</tr>
		<tr align="center"><td colspan="2">&nbsp;</td></tr>
<%
		Select case sTipoRisposta
			'case "V"
			case aTip(2)
				RispostaValore(Risposte)
			'case "T"
			case aTip(0)
				RispostaTesto(Risposte)
		End select
%>

		<tr align="center"><td colspan="2">&nbsp;</td></tr>
		<tr class="tbltext">
			<td colspan="2" align="center">
				<a href="javascript:ChiudiTutto()">
					<img src="<%=Session("progetto")%>/images/chiudi.gif" border="0" alt="Chiudi">
				</a>
				&nbsp;
				<%
				If sRsTip_Risp = aTip(2) then%>
					<input type="image" name="Conferma" id="Conferma" alt="Conferma le modifiche alla Risposta" src="<%=Session("progetto")%>/images/conferma.gif" border="0" onmouseover="javascript:window.status=''; return true" onclick="return ControllaCampi();">					
					&nbsp;
				<%
				Else %>
					<input type="image" name="Aggiungi" id="Aggiungi" alt="Registra la Risposta" src="<%=Session("progetto")%>/images/aggiungi.gif" border="0" onmouseover="javascript:window.status=''; return true" onclick="return ControllaCampi();">
					&nbsp;
				<%
				End if
				If sRsTip_Risp <> "" then %>
					<input type="image" name="Elimina" id="Elimina" alt="Elimina la Risposta" src="<%=Session("progetto")%>/images/elimina.gif" border="0" onmouseover="javascript:window.status=''; return true" onclick="return Elimina();">
				<%
				End if %>
			</td>
		</tr>	
	</table>
	</form>
	<%if sTipoSelezione = "M" and Risposte="No" and sTipoRisposta = "" then%>
		<script language="Javascript">
			document.frmRisposta.rTipo[0].checked = 'true';
			Reload(document.frmRisposta.rTipo[0].value);
		</script>
	<%end if%>
	<%if sTipoSelezione = "M"  then%>
		<script language="Javascript">
			document.frmRisposta.rTipo[1].disabled = 'true';
		</script>
	<%end if%>
	
	<%
	rsRisposta.close
	
End Sub
'----------------------------------------------------------------------
Sub RispostaValore(Risposte)

	If Risposte="No" then
%>
		<input type="hidden" name="sAzione" id="sAzione" value="INS">
		<tr>
			<td class="tbltext1">
				<b>Testo Valore Min *</b>
			</td>
			<td>
				<input type="text" maxlength="15" size="15" id="txtMin" name="txtMin" class="textblacka">
			</td>
		</tr>
		<tr>
			<td class="tbltext1">
				<b>Valore Min *</b>
			</td>
			<td>
				<input type="text" maxlength="2" size="5" id="VMin" name="VMin" class="textblacka">
			</td>
		</tr>

		<tr>
			<td class="tbltext1">
				<b>Valore Max *</b>
			</td>
			<td> 
				<input type="text" maxlength="2" size="5" id="VMax" name="VMax" class="textblacka">
			</td>
		</tr>
		<tr>
			<td class="tbltext1">
				<b>Testo Valore Max *</b>
			</td>
			<td>
				<input type="text" maxlength="15" size="15" id="txtMax" name="txtMax" class="textblacka">
			</td>
		</tr>
<%	Else %>
		<input type="hidden" name="nIdR" id="nIdR" value="<%=rsRisposta("id_rispostaiq")%>">
		<input type="hidden" name="dTMST" id="dTMST" value="<%=rsRisposta("dt_tmst")%>">
		<input type="hidden" name="sAzione" id="sAzione" value="MOD">
		<tr>
			<td class="tbltext1">
				<b>Testo Valore Min *</b>
			</td>
			<td>
				<input type="text" maxlength="15" size="15" id="txtMin" name="txtMin" class="textblacka" value="<%=rsRisposta("testo_min")%>">
			</td>
		</tr>
		<tr>
			<td class="tbltext1">
				<b>Valore Min *</b>
			</td>
			<td>
				<input type="text" maxlength="2" size="5" id="VMin" name="VMin" class="textblacka" value="<%=rsRisposta("iq_min")%>">
			</td>
		</tr>

		<tr>
			<td class="tbltext1">
				<b>Valore Max *</b>
			</td>
			<td> 
				<input type="text" maxlength="2" size="5" id="VMax" name="VMax" class="textblacka" value="<%=rsRisposta("iq_max")%>">
			</td>
		</tr>
		<tr>
			<td class="tbltext1">
				<b>Testo Valore Max *</b>
			</td>
			<td>
				<input type="text" maxlength="15" size="15" id="txtMax" name="txtMax" class="textblacka" value="<%=rsRisposta("testo_max")%>">
			</td>
		</tr>
<%	End If

End Sub
'----------------------------------------------------------------------
Sub RispostaTesto(Risposte)
	n=1
	
	'Prendo da DIZ_Dati i valori del campo FL_TIPRISP della risposta (C|Chiusa|A|Aperta)
	set rsDdati = server.CreateObject("ADODB.Recordset")
	sSQL =	"SELECT id_campo_desc FROM diz_dati WHERE id_tab='IQ_RISPOSTA'" &_
			" AND id_campo='FL_TIPRISP'"
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	rsDdati.Open sSQL, CC, 3
		sFlTRis = rsDdati("id_campo_desc")
		aFlTRis = split(sFlTRis,"|")
	rsDdati.close
	set rsDdati = nothing %>
	<tr>
		<td colspan="2">
			<table width="100%" border="0">
				<tr>
					<td width="10%" class="tbltext1" align="center">
						<b>Elimina</b>
					</td>
					<%
					for i=0 to Ubound(aFlTRis) step 2%>
					<td width="10%" class="tbltext1" align="center">
						<b><%=aFlTRis(i+1)%></b>
					</td>
					<%
					next%>
					<td class="tbltext1" align="center">
						<b>Testo Risposta</b>
					</td>
				</tr>
<%				If Risposte="No" then %>
					<tr class="sfondocomm">
						<td align="center">
							<input type="checkbox" name="chkElim<%=n%>" id="chkElim<%=n%>" disabled>
						</td>
						<%
						for i=0 to Ubound(aFlTRis) step 2%>
						<td align="center">
							<input type="radio" name="rFlTipoRisp<%=n%>" id="rFlTipoRisp<%=n%>" value="<%=aFlTRis(i)%>">
						</td>
						<%
						next%>
						<td align="center">
							<input type="text" name="txtRisposta<%=n%>" id="txtRisposta<%=n%>" value size="50" maxlength="250">
						</td>
					</tr>
<%				Else 
					Do while not rsRisposta.eof %>
						<tr>
							<td align="center">
								<input type="checkbox" name="chkElim<%=n%>" id="chkElim<%=n%>">
								<input type="hidden" name="nIdR<%=n%>" id="nIdR<%=n%>" value="<%=rsRisposta("id_rispostaiq")%>">
								<input type="hidden" name="dTMST<%=n%>" id="dTMST<%=n%>" value="<%=rsRisposta("dt_tmst")%>">
							</td>
							<%
							for i=0 to Ubound(aFlTRis) step 2%>
							<td align="center">
								<input type="radio" name="rFlTipoRisp<%=n%>" id="rFlTipoRisp<%=n%>" value="<%=aFlTRis(i)%>" <%If rsRisposta("fl_tiprisp")=aFlTRis(i) then Response.Write "checked" else Response.Write "disabled" end if%>>
							</td>
							<%
							next %>
							<td align="center">
								<input type="text" name="txtRisposta<%=n%>" id="txtRisposta<%=n%>" value="<%=rsRisposta("txt_risposta")%>" readonly size="50" maxlength="250">
							</td>
					</tr>
<%						n = n+1
					rsRisposta.movenext
					Loop %>
					<tr class="sfondocomm">
						<td align="center">
							<input type="checkbox" name="chkElim<%=n%>" id="chkElim<%=n%>" disabled>
						</td>
						<%
						for i=0 to Ubound(aFlTRis) step 2%>
						<td align="center">
							<input type="radio" name="rFlTipoRisp<%=n%>" id="rFlTipoRisp<%=n%>" value="<%=aFlTRis(i)%>">
						</td>
						<%
						next%>
						<td align="center">
							<input type="text" name="txtRisposta<%=n%>" id="txtRisposta<%=n%>" value size="50" maxlength="250">
						</td>
					</tr>
<%				End if%>
				<input type="hidden" name="nElementi" id="nElementi" value="<%=n%>">
			</table>
		</td>
	</tr>
<%	
End Sub
'----------------------------------------------------------------------
'M A I N
%>
<!--#include virtual="/chk_init.asp"-->
<!--#include virtual="/util/menufun.asp"-->

<html>
<head>
<title><%=Session("TIT")%> [ <%=Session("login")%> ] </title>
<link REL="STYLESHEET" TYPE="text/css" HREF="<%=session("progetto")%>/fogliostile.css">
</head>
<body class="sfondocentro" topmargin="2" leftmargin="0">
<center>
<!--#include virtual="/include/openconn.asp"-->
<!--#include virtual="include/SetPulsanti.asp"-->

<%

dim rsDomanda, rsRisposta
dim sTipoSelezione
dim nIdDom, nIdArea, nIdQuest, sTipoRisposta, nIdBlocco, nIdElem


	nIdDom = request("idr")
	nIdArea = request("ida")
	nIdQuest = request("idq")
	nIdBlocco = request("idb")
	nIdElem = request("ide")
	
	'Response.Write "nIdDom = " & nIdDom & "<br>"
	'Response.Write "nIdArea = " & nIdArea & "<br>"
	'Response.Write "nIdQuest = " & nIdQuest & "<br>"
	'Response.Write "nIdBlocco = " & nIdBlocco & "<br>"
	'Response.Write "nIdElem = " & nIdElem & "<br>"
	
	
	ControlliJavaScript()
	Inizio()
	ElencoR()
%>
<!--#include virtual="/include/closeconn.asp"-->
</body></html>
