<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%
Sub Inizio
	dim sFunzione
	dim sTitolo
	dim sCommento
	dim bCampiObbl
	
	sFunzione = "GESTIONE QUESTIONARI DI GRADIMENTO"
	sTitolo = "CANCELLAZIONE QUESTIONARIO"
	sCommento = "Conferma della cancellazione del Questionario"
	bCampiObbl = false
%>
	<!--#include virtual="include/SetTestata.asp"-->
<%
End Sub
'--------------------------------------------------------------------------
Sub Cancella()

	dim nIdQuest, dDtTmst
	dim sSQL, sSQL2, rsRisp


'''9/7/2003: RICEVUTI VALORI DA FORM ANZICHE' QUERYSTRING 
	'''nIdQuest = Request("id")
	'''dDtTmst = Request("tmst")
	nIdQuest = Request("IdQuest")
	dDtTmst = Request("dtTmst")
'''FINE 9/7/2003

	'Controllo che non esistano Aree associate al questionario
	sSQL =  " select count(*) as CAre" &_
			" from iq_area" &_
			" where id_infoiq = " & nIdQuest
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	set rsCAre = cc.execute(sSQL)
		nContaAree = cint(rsCAre("CAre"))
	rsCAre.close
	set rsCAre = nothing
	
	'Controllo che non esistano associazioni del questionario in ass_quest
	sSQL =  " select count(*) as CAsso" &_
			" from ass_quest" &_
			" where id_infoquest = " & nIdQuest
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	set rsCAsso = cc.execute(sSQL)
		nContaAsso = cint(rsCAsso("CAsso"))
	rsCAsso.close
	set rsCAsso = nothing
	
		if nContaAree > 0 or nContaAsso > 0 then
%>
			<br><br>
			<table border=0 cellspacing=2 cellpadding=1 width="500">
				<tr align=middle>
					<td class="tbltext3">
						Non � possibile cancellare il Questionario.<br>
						Cancellare prima le Aree associate e/o eliminare 
						l'associazione del Questionario al Ruolo e alla Sessione.
					</td>
				</tr>
				<tr align=middle>
					<td align="center">
						&nbsp;<br>
						<%PlsIndietro()%>
					</td>
				</tr>
			</table>		
			<br><br>
<%
		else
			sSQL = "delete from info_quest where id_infoquest = " & nIdQuest
			sErrore=Esegui(nIdQuest ,"info_quest",Session("persona"),"DEL",sSQL,0,dDtTmst)
		   
			
			IF sErrore <> "0" then
%>
				<br><br>
				<table border=0 cellspacing=2 cellpadding=1 width="500">
					<tr align=middle>
						<td class="tbltext3">
							Impossibile cancellare il Questionario.
							<BR>
							Errore: <%=sErrore%>
						</td>
					</tr>	
					<tr align=middle>	
						<td align="center">
							&nbsp;<br>
							<%PlsIndietro()%>
						</td>
					</tr>
				</table>		
				<br><br>
<% 
			ELSE %>
				<br><br>
				<table border=0 cellspacing=2 cellpadding=1 width="500">
				<tr align=middle>
						<td class=tbltext3>
							Cancellazione del Questionario correttamente effettuata
						</td>
					</tr>
				</table>
				<br><br>
<%
			END IF
		end if
	
%>
<br>
<table width=500>
	<tr>
		<td align=center>
			<%
			PlsLinkRosso "QGR_VisQuestionario.asp", "Elenco dei Questionari"
			%>
		</td>
	</tr>
</table>
<%
End Sub
'------------------------------------------------------------------------
'M A I N
%>
<!--#include virtual="/strutt_testa2.asp"-->
<!--#include virtual="/include/openconn.asp"-->
<!--#include virtual="/Util/DBUtil.asp"-->
<!--#include virtual="/Include/SysFunction.asp"-->
<!--#include virtual="include/SetPulsanti.asp"-->
<%	
	Inizio()
	Cancella()
%>
<!--#include virtual="/include/closeconn.asp"-->
<!--#include virtual="/strutt_coda2.asp"-->
