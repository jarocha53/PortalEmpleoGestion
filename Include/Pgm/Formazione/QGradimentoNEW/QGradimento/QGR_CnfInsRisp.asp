<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<script LANGUAGE="Javascript">
function Congruenza(sTS,sDTS) {
	if (sTS == "M"){
		alert('Attenzione: Una Domanda Chiusa con risposta di Tipo Valore non pu� avere come Tipo Selezione <' + sDTS + '>. VERIFICARE!')
		return false
	}
	if (sTS == ""){
		alert('Attenzione: Per una Domanda Chiusa con risposta di Tipo Valore deve essere specificato il <Tipo Selezione>. VERIFICARE!')
		return false
	}
}
</script>

<%

'--------------------------------------------------------------------------
Sub Inizio
	dim sFunzione
	dim sTitolo
	dim sCommento
	dim bCampiObbl
	
	sFunzione = "GESTIONE QUESTIONARI DI GRADIMENTO"
	sTitolo = "INSERIMENTO RISPOSTA"
	sCommento = "Conferma dell'inserimento della Risposta"
	bCampiObbl = false
%>
	<!--#include virtual="include/SetTestata.asp"-->
<%
End Sub
'--------------------------------------------------------------------------
Sub Conferma()

	dim nRispCh, nRispAp, nVMin, nVMax
	dim sTipo, sTxtRisp, sTxtMin, sTxtMax, sTipo_domanda
	dim sSQL, sql
	dim sErrore, sErrV, sErrT, rs

	sTipo	= Request.Form("rTipo")
	Adesso	="TO_DATE('" & day(now) & "/" & _
			   month(now) & "/" & year(now) & _
			   " " & hour(now) & ":" & minute(now) & ":" & second(now) & _
			   "','DD/MM/YYYY HH24:MI:SS')"	
			   
	'Verifico che non sia stata cancellata la domanda.
	sql= "Select id_domandaiq, tipo_sel from iq_domanda where id_domandaiq=" & nIdDom
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
	set rsVer = CC.Execute(sql)
	if not rsVer.eof then
		sTipoSelezione = rsVer("tipo_sel") 
		
		'----Prendo i valori del tipo_domanda da diz_dati----'
		sSQL =	"SELECT id_campo_desc FROM diz_dati WHERE id_tab='IQ_DOMANDA'" &_
				" AND id_campo='TIPO_DOMANDA'"
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
		SET rsDdati = CC.Execute(sSQL)
			sTipDom = rsDdati("id_campo_desc")
			aTipDom = split(sTipDom,"|")
		rsDdati.close
		set rsDdati = nothing
		'----'
		'----Prendo i valori del tipo_selezione da diz_dati----'
		sSQL =	"SELECT id_campo_desc FROM diz_dati WHERE id_tab='IQ_DOMANDA'" &_
				" AND id_campo='TIPO_SEL'"
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
		SET rsDdati = CC.Execute(sSQL)
			sTipSel = rsDdati("id_campo_desc")
			aTipSel = split(sTipSel,"|")
		rsDdati.close
		set rsDdati = nothing
		'----'
				
		CC.BeginTrans
		
		If sTipo = "V" then
		'Risposta di tipo Valore
			nVMin	= clng(Request.Form("VMin"))
			nVMax	= clng(Request.Form("VMax"))
			sTxtMin = strHTMLEncode(Trim(Request.Form("txtMin")))
			sTxtMin = replace(sTxtMin,"'","''")
			sTxtMax = strHTMLEncode(Trim(Request.Form("txtMax")))
			sTxtMax = replace(sTxtMax,"'","''")
			
			sSQL =	"INSERT INTO IQ_RISPOSTA" &_
					" (id_domandaiq, tipo, iq_min, iq_max, testo_min, testo_max, dt_tmst)"	&_
					" VALUES" &_
					" (" & nIdDom & ",'" & sTipo & "'," & nVMin & "," & nVMax & ",'" & sTxtMin & "','" & sTxtMax & "'," & Adesso & ")"
		Else
		'Risposta di tipo Testo
			nElem = Request.Form("nElementi")
			sFlTipoRisp = Request.Form("rFlTipoRisp" & nElem)
			sTxtRisp	= Trim(strHTMLEncode(Request.Form("txtRisposta" & nElem)))
			sTxtRisp	= replace(sTxtRisp,"'","''")
						
			if sTxtRisp = "&nbsp;" then
				sTxtRisp = ""
			end if
			
			sSQL =	"INSERT INTO IQ_RISPOSTA" &_
					" (id_domandaiq, tipo, txt_risposta, fl_tiprisp, dt_tmst)"	&_
					" VALUES" &_
						" (" & nIdDom & ",'" & sTipo & "','" & sTxtRisp & "','" & sFlTipoRisp & "',"  & Adesso & ")"
		End if
		
		sErrore = EseguiNoC(sSQL,CC)
		
		IF sErrore="0" THEN
		
			sSQL =	"Select ID_RISPOSTAIQ from IQ_RISPOSTA where DT_TMST =" &  Adesso
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
			set RRDom  = CC.Execute(sSQL)
			iddo = RRDom("ID_RISPOSTAIQ")
			sSQL =	"INSERT INTO STRUTTURA_QUEST (ID_INFOQUEST, ID_AREAIQ, ID_BLOCCO, TIPO_ELEMENTO, ID_ELEMENTO, ID_ELEM_ORIGINE, DT_TMST) values (" & nIdQuest & "," & nIdArea & "," & nIdBlocco & ",'R'," & iddo & "," & nIdElem & "," & Adesso & ")"
						
			sErrore = EseguiNoC(sSQL,CC)

		end if
		
		IF sErrore="0" THEN
			If sTipo= "V" then
				'sTipo_domanda = "C"
				sTipo_domanda = aTipDom(2)
				sql="UPDATE iq_domanda SET tipo_domanda='" & sTipo_domanda & "'" &_
					" WHERE id_domandaiq =" & nIdDom
				sErrV = EseguiNoC(sql,CC)
				if sErrV = "0" then
					CC.CommitTrans
					'----Verifica della congruenza----' 
					for i=0 to Ubound(aTipSel) step 2
						if sTipoSelezione=aTipSel(i) then
							sDescTipoSelezione = aTipSel(i+1)
						end if 
					next %>
					<script language="javascript">
						Congruenza('<%=sTipoSelezione%>','<%=sDescTipoSelezione%>')
					</script>
<%					'----Fine Verifica----'
					OkInsert()
				else
					CC.RollbackTrans
					KoInsert sErrV
				end if
			Else
				nRispCh = 0
				nRispAp = 0
				sql	= "SELECT fl_tiprisp FROM iq_risposta WHERE id_domandaiq=" & nIdDom
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
				set rs	= CC.execute(sql)
					'Almeno una risposta risulta inserita.
					Do While not rs.EOF
						if rs("fl_tiprisp") = "C" then
							nRispCh = nRispCh + 1
						else
							nRispAp = nRispAp +1
						end if
					rs.movenext
					Loop
				rs.close
				set rs=nothing
			
				If nRispCh = 0 then
					'sTipo_domanda = "A"
					sTipo_domanda = aTipDom(0)
				Elseif nRispAp = 0 then
					'sTipo_domanda = "C"
					sTipo_domanda = aTipDom(2)
				Elseif nRispCh > 0 and nRispAp > 0 then
					'sTipo_domanda = "S"
					sTipo_domanda = aTipDom(4)
				End if
			
				sql="UPDATE iq_domanda SET tipo_domanda='" & sTipo_domanda & "'" &_
					" WHERE id_domandaiq =" & nIdDom
				sErrT = EseguiNoC(sql,CC)
				if sErrT = "0" then

						 CC.CommitTrans
						%>
							<form name="frmRitorna" id="frmRitorna" action="QGR_VisRisposta.asp" method="post">
								<input type="hidden" name="txtTipoRisposta" id="txtTipoRisposta" value="<%=sTipo%>">
								<input type="hidden" name=idr id=Idr value="<%=nIdDom%>">
								<input type="hidden" name=Ida id=Ida value="<%=nIdArea%>">
								<input type="hidden" name=Idq id=Idq value="<%=nIdQuest%>">
								<input type="hidden" name=Idb id=Idb value="<%=nIdblocco%>">
								<input type="hidden" name=Ide id=Ide value="<%=nIdelem%>">
							</form>
							<script LANGUAGE="Javascript">
								frmRitorna.submit();
							</script>
						<%	

				else
					CC.RollbackTrans
					KoInsert sErrT
				end if
			End if
		ELSE
			CC.RollbackTrans
			KoInsert sErrore
		END IF
	else
		KoInsert "Non esiste la relativa Domanda."
	end if
	rsVer.close
	set rsVer=nothing
	
End Sub
'------------------------------------------------------------------------
Sub OkInsert()
%>
	<br><br>
	<table border=0 cellspacing=2 cellpadding=1 width='500'>
		<tr align=middle>
			<td class='tbltext3'>
				Inserimento della risposta correttamente effettuato.
			</td>
		</tr>
	</table>
	<br><br>
<%'''9/7/2003: PASSATI VALORI TRAMITE FORM ANZICHE' QUERYSTRING %>
	<form name="frmDomanda" method="post" action="QGR_Sessioni.asp">
		<input type="hidden" name="idq" value="<%=nIdQuest%>">
		<input type="hidden" name="ida" value="<%=nIdArea%>">
	</form>
<%'''FINE 9/7/2003%>

<script language="javascript">
	opener.document.frmReload.submit();
	self.close();
</script>
<%
End Sub
'------------------------------------------------------------------------
Sub KoInsert(sErrore)
%>
	<br><br>
	<table border=0 cellspacing=2 cellpadding=1 width='500'>
		<tr align=middle>
			<td class='tbltext3'>
				Errore nell'inserimento.
				<br>
				<%=sErrore%>
			</td>
		</tr>
	</table>
	<br><br>
<%'''9/7/2003: PASSATI VALORI TRAMITE FORM ANZICHE' QUERYSTRING %>
	<form name="frmDomandaKO" method="post" action="QGR_VisDomanda.asp">
		<input type="hidden" name="idq" value="<%=nIdQuest%>">
		<input type="hidden" name="ida" value="<%=nIdArea%>">
	</form>
<%'''FINE 9/7/2003%>
	<table width=500>
		<tr>
			<td align=center>
<%'''9/7/2003: PASSATI VALORI TRAMITE FORM ANZICHE' QUERYSTRING 
				'''PlsLinkRosso "QGR_VisDomanda.asp?ida=" & nIdArea & "&idq=" & nIdQuest, "Elenco delle Domande" %>
				<a onmouseover="javascript:window.status='' ; return true" href="javascript:frmDomandaKO.submit();" class="textred">
					<b>Elenco delle Domande</b>
				</a>
			</td>
		</tr>
	</table>
	<br><br>
<%
End Sub
'------------------------------------------------------------------------
'M A I N
%>
<HTML>
<HEAD>
<TITLE><%=Session("TIT")%> [ <%=Session("login")%> ] </TITLE>
<LINK REL=STYLESHEET TYPE="text/css" HREF='<%=session("progetto")%>/fogliostile.css'>
</head>
<body class=sfondocentro topmargin="2" leftmargin="0">
<center>
<!--#include virtual="/include/openconn.asp"-->
<!--#include virtual="/Util/DBUtil.asp"-->
<!--#include virtual="/Include/HTMLEncode.asp"-->
<!--#include virtual="/Include/SysFunction.asp"-->
<!--#include virtual="include/SetPulsanti.asp"-->
<%
	dim nIdArea, nIdQuest, nIdDom , nIdBlocco, nIdElem
	
	'Response.Write Request.Form("nIdArea") & " - "
	'Response.Write Request.Form("nIdQuest") & " - "
	'Response.Write Request.Form("nIdDom") & " - "
	'Response.Write Request.Form("nIdblocco") & " - "
	'Response.Write Request.Form("nIdelem") & " - "
	
	nIdArea	= clng(Request.Form("nIdArea"))
	nIdQuest= clng(Request.Form("nIdQuest"))
	nIdDom	= clng(Request.Form("nIdDom"))
	nIdblocco = clng(Request.Form("nIdblocco"))
	nIdelem	= clng(Request.Form("nIdelem"))
	
	
	Inizio()
	Conferma()

%>
<!--#include virtual="/include/closeconn.asp"-->
</body></html>
