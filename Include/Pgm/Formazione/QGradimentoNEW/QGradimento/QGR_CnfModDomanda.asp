<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<SCRIPT LANGUAGE="Javascript">
/*
function Congruenza(sTD,sT,sTS) {
	if ((sTD == "A") && (sTS != "")){
		alert('Attenzione: Il <Tipo Selezione> della Domanda risulta incongruente con la Risposta inserita!  VERIFICARE!')
		return false
	}
	if (sTD == "C") {
		if ((sT == "T") && (sTS == "")) {
		alert('Attenzione: Il <Tipo Selezione> della Domanda risulta incongruente con la Risposta inserita!  VERIFICARE!')
			return false
		}
		if ((sT == "V") && (sTS != "E")) {
		alert('Attenzione: Il <Tipo Selezione> della Domanda risulta incongruente con la Risposta inserita!  VERIFICARE!')
			return false
		}
	}
	if ((sTD == "S") && (sTS != "E")){
		alert('Attenzione: Il <Tipo Selezione> della Domanda risulta incongruente con la Risposta inserita!  VERIFICARE!')
		return false
	}
}
*/
</script>
<%
'--------------------------------------------------------------------------
Sub Inizio
	dim sFunzione
	dim sTitolo
	dim sCommento
	dim bCampiObbl
	
	sFunzione = "GESTIONE QUESTIONARI DI GRADIMENTO"
	sTitolo = "MODIFICA DOMANDA"
	sCommento = "Conferma della modifica della Domanda"
	bCampiObbl = false
%>
	<!--#include virtual="include/SetTestata.asp"-->
<%
End Sub
'--------------------------------------------------------------------------
Sub Conferma()
	dim sDescD, nIdQuest, nIdArea, nIdDom, dDtTmst, sTipo, nNumRisp
	dim sSQL


	sDescD = strHTMLEncode(Trim(Request.Form("DescDomanda")))
	sDescD = replace(sDescD,"'","''")

	nIdQuest = Request.Form("IdQuest")
	nIdArea = Request.Form("IdArea")
	nIdDom = Request.Form("IdDom")
	dDtTmst = Request.Form("dtTmst")
	
	Adesso ="TO_DATE('" & day(now) & "/" & _
		   month(now) & "/" & year(now) & _
		   " " & hour(now) & ":" & minute(now) & ":" & second(now) & _
		   "','DD/MM/YYYY HH24:MI:SS')"	

	sTipoSel = Request.Form("cmbTipoSel")
	
	sSQL =	"UPDATE iq_domanda SET" &_
			" testo_domanda = '" & sDescD & "'," &_
			" dt_tmst = " & Adesso & "," 
			if sTipoSel = "" then
				sSQL = sSQL & " tipo_sel= null"
			else
				sSQL = sSQL & " tipo_sel= '" & sTipoSel & "'"
			end if
			sSQL = sSQL & " where id_domandaiq = " & nIdDom
	
	sErrore = Esegui(nIdDom,"IQ_DOMANDA",Session("persona"),"MOD",sSQL,0,dDtTmst)

	if sErrore="0" then
		'____Controllo di congruenza____
		''sSql =	"Select IR.tipo, ID.tipo_domanda from iq_risposta IR, iq_domanda ID" &_
		''		" where ID.id_domandaiq=" & nIdDom &_
		''		" and ID.id_domandaiq=IR.id_domandaiq"
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
		''set rsCongruenza = CC.Execute(sSql)
		''	if not rsCongruenza.eof then
		''		sTipo = rsCongruenza("tipo")
		''		sTipoDomanda = rsCongruenza("tipo_domanda") %>
				<!--script language="javascript">
					//Congruenza('<%=sTipoDomanda%>','<%=sTipo%>','<%=sTipoSel%>')
				</script-->
<%		''	end if
		''rsCongruenza.Close %>
		<br><br>
		<table border=0 cellspacing=2 cellpadding=1 width='500'>
			<tr align=middle>
				<td class='tbltext3'>
					Modifica della domanda correttamente effettuata
				</td>
			</tr>
		</table>
<%
	else
%>
		<br><br>
		<table border=0 cellspacing=2 cellpadding=1 width='500'>
			<tr align=middle>
				<td class='tbltext3'>
					Errore nella modifica. <%=sErrore%>
				</td>
			</tr>
		</table>
<%
	end if
%>
	<br><br>
<%'''9/7/2003: PASSATI VALORI TRAMITE FORM ANZICHE' QUERYSTRING %>
	<form name="frmDomanda" method="post" action="QGR_Sessioni.asp">
		<input type="hidden" name="idq" value="<%=nIdQuest%>">
		<input type="hidden" name="ida" value="<%=nIdArea%>">
	</form>
<%'''FINE 9/7/2003%>
	<table width=500>
		<tr>
			<td align=center>
<%'''9/7/2003: PASSATI VALORI TRAMITE FORM ANZICHE' QUERYSTRING 
				'''PlsLinkRosso "QGR_VisDomanda.asp?idq=" & nIdQuest & "&ida=" & nIdArea, "Elenco delle Domande" %>
			<SCRIPT LANGUAGE="JAVASCRIPT">
					opener.document.frmReload.submit();
					self.close();
				</SCRIPT>	
			</td>
		</tr>
	</table>
	<br><br>
<%
End Sub
'------------------------------------------------------------------------
'M A I N
%>
<HTML>
<HEAD>
<TITLE><%=Session("TIT")%> [ <%=Session("login")%> ] </TITLE>
<LINK REL=STYLESHEET TYPE="text/css" HREF='<%=session("progetto")%>/fogliostile.css'>
</head>
<body class=sfondocentro topmargin="2" leftmargin="0">
<center>
<!--#include virtual="/include/openconn.asp"-->
<!--#include virtual="/Util/DBUtil.asp"-->
<!--#include virtual="/Include/HTMLEncode.asp"-->
<!--#include virtual="/Include/SysFunction.asp"-->
<!--#include virtual="include/SetPulsanti.asp"-->
<%	
	Inizio()
	Conferma()
%>
<!--#include virtual="/include/closeconn.asp"-->
</body></html>
