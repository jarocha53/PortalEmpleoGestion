<!--#include virtual="/strutt_testa2.asp"-->
<!--#include virtual="/include/openconn.asp"-->
<!--#include virtual="include/SetPulsanti.asp"-->
<!--#include virtual="/include/HTMLEncode.asp"-->

<%
' Progetto = impostaprogetto()
progetto = "/plavoro"
'<!--#include virtual="/include/OpenPar.asp"-->
%>


<%
if ValidateService(session("idutente"),"QGR_VisQuestionario", CC) <> "true" then 
	response.redirect "/util/error_login.asp"
end if	
%>


<script language="Javascript">

</script>

<%
'-------------------------------------------------------------------------------------------------------------------------------

sub TornaIndietro()

	%>
	<form name="frmIndietro" method="post" action="QGR_VisQuestionario.asp">
	</form>  

	<script>
		alert("Operazione correttamente effettuata");
		frmIndietro.submit()
	</script>
	<%

end sub

'-------------------------------------------------------------------------------------------------------------------------------

Sub Msgetto(Msg)
%>
	<br>
	<table align="center">
		<tr>
			<td class="tbltext3"><b><%=Msg%></b></td>
		</tr>	
	</table>
<%
End Sub

'-------------------------------------------------------------------------------------------------------------------------------

Sub Inizio()
	dim sFunzione
	dim sTitolo
	dim sCommento
	dim bCampiObbl
	
	sFunzione = "ESPORTAZIONE QUESTIONARI DI GRADIMENTO"
	sTitolo = "ESPORTAZIONE QUESTIONARI DI GRADIMENTO"
	sCommento = "La funzione richiesta st� elaborando i dati."
	bCampiObbl = false
	sHelp = "/Pgm/help/Formazione/QGradimento/QGR_EsportaQGrad/"	
	
%>
	<!--#include virtual="include/SetTestata.asp"-->
<%
End Sub

'----------------------------------------------------------------------

Sub ElabExport()

	Msgetto("Esportazione questionari in corso.")
	
	set objExp = Server.CreateObject("FuncQGrad.clsQGrad")

	' Response.Write "id quest: " & sIdInfoQ & "<br>"
	
	'nel caso di export le connessioni devono essere nell'ordine
	'- CC
	'- CX
	sRisultato = objExp.Elabora(sIdInfoQ, session("progetto"), "T", CC, CX)
	
	if sRisultato = "0" then
		TornaIndietro()
	else
		Msgetto("Errore durante l'elaborazione:<BR>" & sRisultato)
	end if

End Sub

'----------------------------------------------------------------------

'M A I N

dim sIdInfoQ

	sIdInfoQ = Request("idInfoQ")
	
	Inizio()
	ElabExport()
	
%>
<!--#include virtual="/include/closeconn.asp"-->
<!--#include virtual="/strutt_coda2.asp"-->