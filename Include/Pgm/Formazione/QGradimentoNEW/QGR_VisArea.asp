<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%
Sub Inizio()
	dim sFunzione
	dim sTitolo
	dim sCommento
	dim bCampiObbl
	
	sFunzione = "GESTIONE QUESTIONARI DI GRADIMENTO"
	sTitolo = "ELENCO SEZIONI"
	if nMod = 2 then
		sCommento = "In questa pagina sono presentate le sezioni costituenti il questionario selezionato.<BR>Per modificarne le domande/risposte associate, clicca sul relativo link <B>Componi</B>.  "
	else 
		if nMod = 1 or nMod = 4 then
			sCommento = "In questa pagina sono presentate le sezioni costituenti il questionario selezionato.<BR>Per visualizzarne le domande/risposte associate, clicca sul relativo link <B>Anteprima</B>.  "
		else
			if nMod = 3 then
				sCommento = "In questa pagina sono presentate le sezioni costituenti il questionario selezionato.<BR>Per rispondere alle domande associate, clicca sul relativo link <B>Rispondi al questionario</B>.  "
			end if
		end if
	end if
	bCampiObbl = false
	sHelp = "/Pgm/help/Formazione/QGradimento/QGR_VisArea/"		
%>
	<!--#include virtual="include/SetTestata.asp"-->
<%
End Sub
'----------------------------------------------------------------------
Sub ElencoA()
dim sSQL, sSQL2, sSQL3, nIdQuest

dim rsArea, rsQuest, rsQuest2, rsQuest3
dim nCRighe

nIdQuest = request("id")
'Response.Write "nMod = " & nMod & "<br>"
'+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-
'Ricerca dell'intestazione del questionario
'+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-



sSQL = "Select desc_quest, intest_quest from info_quest where id_infoquest = " & nIdQuest
	
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
set rsQuest = myConn.execute(sSQL)

nCRighe = 0
sSQL = "select id_areaiq, desc_areaiq from iq_area where id_infoiq = " & nIdQuest & " order by id_areaiq"
set rsArea = server.CreateObject ("adodb.recordset")

'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
rsArea.Open sSQL,myConn,3 

appoArea = rsArea.RecordCount 
'Response.Write "appoArea = "  appoArea & "<br>"
'Response.End 
%>
<table width="500" border="0" >
	<tr>
		<td class="tbltext3" align="center">
			<%=rsQuest("desc_quest")%>
		</td>
	</tr>
</TABLE>
<%
'Response.Write nMod
if nMod = 2 then%>
	<form name="frmSessioni" method="post" action="QGR_Sessioni.asp">
<%else
	if nMod = 1 then%>
		<form name="frmSessioni" method="post" action="QGR_VisAnteprima.asp">
		<input type="hidden" name="myProg" value="<%=progetto%>">
	<%else if nMod = 3 then
			sSQL2 = "Select cod_commessa,TESTO_PROJ  from progetto where cod_cproj = " & "'" & mid(session("progetto"),2) & "'"
			sSQL3 = "Select erogazione, fruizione from ass_quest a where id_infoquest = " & nIdQuest
						
			if session("ruofu") <> "" then
				' Mi prendo la sessione
				sSQL3 = sSQL3 & " AND COD_RUOFU ='" & session("ruofu") & "'" 
				if 	session("ruofu") = "DI" then
					aFase = SelPrgFase(session("creator")) 
					sSQL3 =	sSQL3 &	" AND (a.cod_sessione ='" & aFase(1) & "' or a.cod_sessione is null)" &_
						" AND (a.id_bando in(select b.id_bando from domanda_iscr b" &_
						" where id_persona = " & session("creator") & ") or a.id_bando is null)" 
				end if
		
			else
				sSQL3 = sSQL3 & " AND COD_RUOFU  is null"
			end if	
			
			 if session("rorga") <> "" then
				sSQL3 = sSQL3 & " AND COD_RORGA ='" & session("rorga") & "'" 
			else
				sSQL3 = sSQL3 & " AND COD_RORGA  is null"
			end if	

'Response.Write sSQL3
'PL-SQL * T-SQL  
SSQL2 = TransformPLSQLToTSQL (SSQL2) 
			set rsQuest2 = myConn.execute(sSQL2)
'PL-SQL * T-SQL  
SSQL3 = TransformPLSQLToTSQL (SSQL3) 
			set rsQuest3 = myConn.execute(sSQL3)
			if not rsQuest.eof then %>
			
			<table width="500" border="0" >
				
				<tr>
					<td class="tbltext3" align="center">&nbsp;</td>
				</tr>
				<tr>
					<td class="tbldett" align="center">
						<%=rsQuest("intest_quest")%>
					</td>
				</tr>
				<tr>
					<td class="tbltext1" align="center">&nbsp;</td>
				</tr>
				<tr>
					<td class="tbltext1" align="left">
						<b>Programma : <%=rsQuest2("TESTO_PROJ")%></b>
					</td>
				</tr>
				<tr>
					<td class="tbltext1" align="left">
						<b>Codice commessa del progetto:&nbsp;&nbsp;<%=rsQuest2("cod_commessa")%></b>
					</td>
				</tr>
				<% 
				sCodCommessa = rsQuest2("cod_commessa")
				sTestoProgetto = rsQuest2("TESTO_PROJ")
				if not rsQuest3.eof then %>
						  <% if rsQuest3("erogazione") <> "" then %>	
								<tr>
									<td class="tbltext1" align="left">
										<b>Erogazione:&nbsp;&nbsp;<%

								sSQL = "SELECT ID_CAMPO_DESC FROM DIZ_DATI WHERE " &_
									"ID_TAB='ASS_QUEST' AND ID_CAMPO='EROGAZIONE'" 
								
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
								set rsDizDati = cc.Execute(sSQL)
								bTipo = split(rsDizDati("ID_CAMPO_DESC"),"|")
								for i = 0 to ubound (bTipo) step 2
									if ( bTipo(i) = rsQuest3("erogazione")) then
										sErogazione = bTipo(i + 1)
										Response.Write bTipo(i + 1)
										exit for
									end if	
								next
								rsDizDati.close()
								set rsDizDati = nothing
										
										
										%></b>
									</td>
								</tr>
						  <% end if %>
						  <% if rsQuest3("fruizione") <> "" then %>
								<tr>
									<td class="tbltext1" align="left">
										<b>Fruizione:&nbsp;&nbsp;<%
								sSQL = "SELECT ID_CAMPO_DESC FROM DIZ_DATI WHERE " &_
									"ID_TAB='ASS_QUEST' AND ID_CAMPO='FRUIZIONE'" 
								
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
								set rsDizDati = cc.Execute(sSQL)
								bTipo = split(rsDizDati("ID_CAMPO_DESC"),"|")
								
								for i = 0 to ubound (bTipo) step 2
									if ( bTipo(i) = rsQuest3("fruizione")) then
										sFruizione = bTipo(i + 1)
										Response.Write bTipo(i + 1)
										exit for
									end if	
								next
								rsDizDati.close()
								
								set rsDizDati = nothing										
										
										%></b>
									</td>
								</tr>
						  <% end if
				   end if				
		end if
		rsQuest2.close
		rsQuest3.close
		set rsQuest2 = nothing
		set rsQuest3 = nothing
		
		
		intervento = getInterventoFormativo (Session("creator"),mid( session("progetto"),2) )
		if intervento <> "" then
		%>
				<tr>
					<td class="tbltext1" align="left"><B>Titolo intervento formativo : <%=intervento%></B></td>
				</tr>
		<%end if%>
			</table>
			
			<form name="frmSessioni" method="post" action="QGR_Questionario.asp">
				
				
		<% else if nMod = 4 then

			sSQL2 = "Select cod_commessa,TESTO_PROJ from progetto where cod_cproj = " & "'" & mid(session("progetto"),2) & "'"
			sSQL3 = "Select erogazione, fruizione from ass_quest a where id_infoquest = " & nIdQuest

			if session("ruofu") <> "" then
				' Mi prendo la sessione
				sSQL3 = sSQL3 & " AND COD_RUOFU ='" & session("ruofu") & "'" 
				if 	session("ruofu") = "DI" then
					aFase = SelPrgFase(session("creator")) 
					sSQL3 =	sSQL3 &	" AND (a.cod_sessione ='" & aFase(1) & "' or a.cod_sessione is null)" &_
						" AND (a.id_bando in(select id_bando from domanda_iscr" &_
						" where id_persona = " & session("creator") & ") or a.id_bando is null)" 
				end if
		
			else
				sSQL3 = sSQL3 & " AND COD_RUOFU  is null"
			end if	
			
			 if session("rorga") <> "" then
				sSQL3 = sSQL3 & " AND COD_RORGA ='" & session("rorga") & "'" 
			else
				sSQL3 = sSQL3 & " AND COD_RORGA  is null"
			end if	

'PL-SQL * T-SQL  
SSQL2 = TransformPLSQLToTSQL (SSQL2) 
			set rsQuest2 = myConn.execute(sSQL2)

'PL-SQL * T-SQL  
SSQL3 = TransformPLSQLToTSQL (SSQL3) 
			set rsQuest3 = myConn.execute(sSQL3)
			if not rsQuest.eof then %>
			<table width="500" border="0" >
				<tr>
					<td class="tbltext3" align="center">&nbsp;</td>
				</tr>
				<tr>
					<td class="tbldett" align="center">
						<%=rsQuest("intest_quest")%>
					</td>
				</tr>
				<tr>
					<td class="tbltext1" align="center">&nbsp;</td>
				</tr>
				<tr>
					<td class="tbltext1" align="left">
						<b>Programma : <%=rsQuest2("TESTO_PROJ")%></b>
					</td>
				</tr>
				<tr>
					<td class="tbltext1" align="left">
						<b>Codice commessa del progetto:&nbsp;&nbsp;<%=rsQuest2("cod_commessa")%></b>
					</td>
				</tr>
				<%
				sCodCommessa = rsQuest2("cod_commessa")
				sTestoProgetto = rsQuest2("TESTO_PROJ")
				if not rsQuest3.eof then %>
						  <% if rsQuest3("erogazione") <> "" then %>	
								<tr>
									<td class="tbltext1" align="left">
										<b>Erogazione:&nbsp;&nbsp;<%

								sSQL = "SELECT ID_CAMPO_DESC FROM DIZ_DATI WHERE " &_
									"ID_TAB='ASS_QUEST' AND ID_CAMPO='EROGAZIONE'" 
								
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
								set rsDizDati = cc.Execute(sSQL)
								bTipo = split(rsDizDati("ID_CAMPO_DESC"),"|")
								for i = 0 to ubound (bTipo) step 2
									if ( bTipo(i) = rsQuest3("erogazione")) then
										sErogazione = bTipo(i + 1)
										Response.Write bTipo(i + 1)
										exit for
									end if	
								next
								rsDizDati.close()
								set rsDizDati = nothing
										
										
										%></b>
									</td>
								</tr>
						  <% end if %>		
										
							 <% if rsQuest3("fruizione") <> "" then %>
								<tr>
									<td class="tbltext1" align="left">
										<b>Fruizione:&nbsp;&nbsp;<%
								sSQL = "SELECT ID_CAMPO_DESC FROM DIZ_DATI WHERE " &_
									"ID_TAB='ASS_QUEST' AND ID_CAMPO='FRUIZIONE'" 
								
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
								set rsDizDati = cc.Execute(sSQL)
								bTipo = split(rsDizDati("ID_CAMPO_DESC"),"|")
								
								for i = 0 to ubound (bTipo) step 2
									if ( bTipo(i) = rsQuest3("fruizione")) then
										sFruizione = bTipo(i + 1)
										Response.Write bTipo(i + 1)
										exit for
									end if	
								next
								rsDizDati.close()
								
								set rsDizDati = nothing										
										
										%></b>
									</td>
								</tr>
							<% end if
				   end if				
		end if		
		rsQuest2.close
		rsQuest3.close
		set rsQuest2 = nothing
		set rsQuest3 = nothing
		
		
		intervento = getInterventoFormativo (Session("creator"),mid( session("progetto"),2) )
		
		%>
					<form name="frmSessioni" method="post" action="QGR_VisAnteprimaUte.asp">
				<%end if
		end if
	end if		
 end if 
rsQuest.close
set rsQuest = nothing %>
<input type="hidden" name="idq" value="<%=nIdQuest%>">


<%if clng(appoarea) = 0 then %>
		<input type="hidden" name="ida" value="0">
<%end if %>
<%if clng(appoarea) = 1 then %>
		<input type="hidden" name="ida" value="<%=rsArea("id_areaiq")%>">
<%end if %>		
</form>

<%if clng(appoarea) < 1 then %>
	<script>
		document.frmSessioni.submit()  
	</script>
<%else %>
	<table width="500" border="0">
		<tr class="sfondocomm">
	        <td width="20" align="center"><b>#</b></td>
	        <td><b>Sezioni</b></td>
	       <% if nMod = 2 then%>
				<td width="60" align="center"><b>Componi</b></td>
	        <%else
				if nMod = 1 or nMod = 4 then %>
      			   <td width="60" align="center"><b>Anteprima</b></td>
      	        <%else 
      			  if nMod = 3 then %>  			   
					    <td width="120" align="left"><b>Data esecuzione</b></td>					    
					    <td width="30" align="left"><B>Status</B></td>
					    <td width="60" align="center"><b>Rispondi</b></td>
				<%end if
				end if	
			  end if%>
		</tr>
	<%
	do while not rsArea.eof
		nCRighe = nCRighe + 1 
		
	'''8/7/2003: PASSATI VALORI TRAMITE FORM ANZICHE' QUERYSTRING 
	if nMod = 2 then%>
		<form name="frmDomanda<%=nCRighe%>" method="post" action="QGR_Sessioni.asp">
			<input type="hidden" name="idq" value="<%=nIdQuest%>">
			<input type="hidden" name="ida" value="<%=rsArea("id_areaiq")%>">
			<input type="hidden" name="mod" value="<%=nMod%>">
		</form>
	<%else
		if nMod = 1 then%>
			<form name="frmDomanda<%=nCRighe%>" method="post" action="QGR_VisAnteprima.asp">
				<input type="hidden" name="idq" value="<%=nIdQuest%>">
				<input type="hidden" name="ida" value="<%=rsArea("id_areaiq")%>">
				<input type="hidden" name="myProg" value="<%=progetto%>">
			</form>
		<%else
				if nMod = 3 then%>
					<form name="frmDomanda<%=nCRighe%>" method="post" action="QGR_Questionario.asp">
						<input type="hidden" name="idq" value="<%=nIdQuest%>">						
						<input type="hidden" name="ida" value="<%=rsArea("id_areaiq")%>">
						<input type="hidden" name="ero"  value="<%=sErogazione%>">
						<input type="hidden" name="frui" value="<%=sFruizione%>">
						<input type="hidden" name="commessa" value="<%=sCodCommessa%>">
						<input type="hidden" name="progetto" value="<%=sTestoProgetto%>">
						<input type="hidden" name="intervento" value="<%=intervento%>">
					</form>
				<%		
				else
					if nMod = "4" then%>
						<form name="frmDomanda<%=nCRighe%>" method="post" action="QGR_VisAnteprimaUte.asp">
							<input type="hidden" name="idq" value="<%=nIdQuest%>">						
							<input type="hidden" name="ida" value="<%=rsArea("id_areaiq")%>">
							<input type="hidden" name="mod" value="<%=nMod%>">
							<input type="hidden" name="ero"  value="<%=sErogazione%>">
							<input type="hidden" name="frui" value="<%=sFruizione%>">
							<input type="hidden" name="commessa" value="<%=sCodCommessa%>">
							<input type="hidden" name="progetto" value="<%=sTestoProgetto%>">
							<input type="hidden" name="intervento" value="<%=intervento%>">
						</form>
					<%end if
				end if					
		end if							
	end if %>
		
	<%'''FINE 8/7/2003%>
		<tr class="tblsfondo">
			<td class="tblDett">
				<%=nCRighe%>
			</td>
			<td class="tblDett">

					<%
					if rsArea("desc_areaiq") > " " then
						Response.Write rsArea("desc_areaiq")
					else
						Response.Write "--- Senza Titolo ---"
					end if
					%>
				</a>
			</td>
		<form name="frmRisposta<%=nCRighe%>" method="post" action="QGR_VisRisultati.asp">
			<input type="hidden" name="idq" value="<%=nIdQuest%>">
			<input type="hidden" name="ida" value="<%=rsArea("id_areaiq")%>">
			<input type="hidden" name="mod" value="<%=nMod%>">
		</form>
				
			<%	if nMod = 2 then%>
					<td align="center">
					<a href="javascript:frmDomanda<%=nCRighe%>.submit();" class="tblagg">
					<img src="<%=Session("progetto")%>/images/formazione/re.gif" border="0" alt="Componi Sezione" WIDTH="12" HEIGHT="16">
			<%	else
					if nMod = 1 or nMod = 4 then%>
						<td align="center">
						<a href="javascript:frmDomanda<%=nCRighe%>.submit();" class="tblagg">
						<img src="<%=Session("progetto")%>/images/formazione/re.gif" border="0" alt="Visualizza" WIDTH="12" HEIGHT="16">
				<%	else
						if nMod = 3 then
							sSQL =	"SELECT DT_ESEC, ESEGUITO FROM IQ_RISULTATO" &_
								" WHERE ID_INFOQUEST = " & nIdQuest &_
								" AND ID_AREAIQ = " & rsArea("id_areaiq") &_	
								" AND IDUTENTE = " & SESSION("IDUTENTE") 		
		
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
							set rsEsec = myConn.Execute(sSQL)
							if not rsEsec.eof then
								sEseguito = rsEsec("ESEGUITO")
								sDtEsec = ConvDateToString(rsEsec("DT_ESEC")) 
							else
								sEseguito = ""
								sDtEsec = ""
							end if%>
							<td align="left" class=tblDettFad>							
								<%=sDtEsec%>&nbsp;
							</td>															
							<td align="left" class=tblDettFad>
							<%if sEseguito = "S" then %>
								<img title="Questionario non eseguito" src="<%=Session("progetto")%>/images/formazione/okv.gif" border="0" 
								WIDTH="17" HEIGHT="15">Eseguito
								<td>
								<a href="javascript:frmDomanda<%=nCRighe%>.submit();" class="tblagg">													
		     					<img src="<%=Session("progetto")%>/images/formazione/re.gif" border="0" alt="Rispondi" WIDTH="12" HEIGHT="16">
								</td>
								
							<%else %>	
								<img title="Questionario non eseguito" src="<%=Session("progetto")%>/images/formazione/ko.gif" border="0" 
								WIDTH="17" HEIGHT="15">Da eseguire
								<td>
								<a href="javascript:frmDomanda<%=nCRighe%>.submit();" class="tblagg">													
		     					<img src="<%=Session("progetto")%>/images/formazione/re.gif" border="0" alt="Rispondi" WIDTH="12" HEIGHT="16">
								</td>
							<%end if%>	
					<%	end if
					end if	
				end if%>
				</a>
			</td>
		</tr>	
	<%
		rsArea.movenext
	loop
end if	
rsArea.close
set rsArea = nothing
%>
</table>
<br>
<table width="500">
	<tr>
		<td align="center" width="50%">
		<%	if nMod = 3  or nMod = 4 then
				PlsLinkRosso "QGR_VisFrontGradimento.asp","Elenco dei Questionari di Gradimento"
			else 
				PlsLinkRosso apLinkIndietro, descrizioneLinkIndietro
			end if%>
		</td>
	</tr>
</table>
<%
End Sub
'----------------------------------------------------------------------
'M A I N
%>
<!--#include virtual="/strutt_testa2.asp"-->
<!--#include virtual="/include/openconn.asp"-->
<!--#include virtual="include/SetPulsanti.asp"-->
<!--#include virtual="/include/InterventoFormativo.asp"-->
<!--#include Virtual = "/include/ControlFase.asp"-->

<%
''if ValidateService(session("idutente"),"QGR_VisQuestionario", CC) <> "true" then 
''	response.redirect "/util/error_login.asp"
''end if
dim nMod
	nMod = request("mod")

dim sErogazione, sFruizione, sCodCommessa, sTestoProgetto, intervento

sErogazione = Request.Form ("ero")
sFruizione	= Request.Form ("frui")				 
sCodCommessa = Request.Form ("commessa")							 
sTestoProgetto = Request.Form ("progetto")					
intervento = Request.Form ("intervento")			



'Response.Write "nMod = " & nMod & "<br>"


'INIZIO PILI

	apLinkIndietro = Request("pagIndietro")
	if apLinkIndietro = "" then
		apLinkIndietro = "QGR_VisQuestionario.asp"
		descrizioneLinkIndietro = "Elenco dei Questionari di Gradimento"
	else
		descrizioneLinkIndietro = "Elenco dei Questionari da importare"
	end if


'dichiaro una connessione di comodo
dim myConn
progetto = Request("myProg")

if progetto <> "" then
'<!--#include virtual="/include/openpar.asp"-->
%>
	
<%
	set myConn = CX
else
	set myConn = CC
end if	
	
	Inizio()
	ElencoA()

if progetto <> "" then
%>
	<!--#include virtual="/include/closepar.asp"-->
<%
end if	

'FINE PILI




'	Inizio()
'	ElencoA()
%>
<!--#include virtual="/include/closeconn.asp"-->
<!--#include virtual="/strutt_coda2.asp"-->
