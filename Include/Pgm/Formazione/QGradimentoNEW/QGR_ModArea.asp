<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%
Sub ControlliJavaScript()
%>
<SCRIPT LANGUAGE="Javascript">
<!--#include virtual="include/ControlString.inc"-->

function ControllaCampi(frmInsArea)	{
	frmInsArea.DescArea.value = TRIM(frmInsArea.DescArea.value)
	if (frmInsArea.DescArea.value == "") {
		alert("Inserire la 'Descrizione Sezione'")
		frmInsArea.DescArea.focus() 
		return false
	}	
}

</SCRIPT>
<%
End Sub
'------------------------------------------------------------------------
Sub Inizio
	dim sFunzione
	dim sTitolo
	dim sCommento
	dim bCampiObbl
	
	sFunzione = "GESTIONE QUESTIONARI DI GRADIMENTO"
	sTitolo = "MODIFICA SEZIONE"
	sCommento = "Modifica della sezione<br>Questionari di Gradimento"
	bCampiObbl = true
	sHelp = "/Pgm/help/Formazione/QGradimento/QGR_ModArea/"		
%>
	<!--#include virtual="include/SetTestata.asp"-->
<%
End Sub
'------------------------------------------------------------------------
Sub Modifica()

	dim sSQL, rsArea, sDArea, dDtTmst
	dim nIdQuest, nIdArea

	nIdQuest = request("idq")
	nIdArea = request("ida")

	sSQL = "Select desc_areaiq, dt_tmst from iq_area where id_areaiq = " & nIdArea
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	set rsArea = cc.execute(sSQL)
		if not rsArea.eof then
			sDArea = rsArea("desc_areaiq")
			dDtTmst = rsArea("dt_tmst")
		end if
	rsArea.close
	set rsArea=nothing 
%>
	<form method=post action="QGR_CnfModArea.asp" onsubmit="return ControllaCampi(this)" id=frmInsArea name=frmInsArea>
	<TABLE border=0 width=500> 
		<TR>
			<TD class=tbltext1>
				<b>Descrizione Sezione*</b>
			</TD>
			<TD>
				<INPUT id=DescArea maxLength=255 size="40" name=DescArea value="<%=sDArea%>" class=textblacka> 
				<INPUT type=hidden id=IdArea name=IdArea value=<%=nIdArea%>>
				<INPUT type=hidden id=IdQuest name=IdQuest value=<%=nIdQuest%>>
				<INPUT type=hidden id=dtTmst name=dtTmst value="<%=dDtTmst%>">
		    </TD>
		</TR>
	</table>
	<br>
	<TABLE border=0 width=500> 
		<TR>
			<td align=center>
			<%
				PlsChiudi()
				PlsInvia("Conferma")
			%>
			</td>
		</TR>
	</TABLE>
	</FORM>
<%
End Sub
'------------------------------------------------------------------------
'M A I N
%>

<HTML>
<HEAD>
<TITLE><%=Session("TIT")%> [ <%=Session("login")%> ] </TITLE>
<LINK REL=STYLESHEET TYPE="text/css" HREF='<%=session("progetto")%>/fogliostile.css'>
</head>
<body class=sfondocentro topmargin="2" leftmargin="0">
<center>
<!--#include virtual="/include/openconn.asp"-->
<!--#include virtual="include/SetPulsanti.asp"-->

<%

	
	ControlliJavaScript()
	Inizio()
	Modifica()
%>
<!--#include virtual="/include/closeconn.asp"-->
</body></html>
