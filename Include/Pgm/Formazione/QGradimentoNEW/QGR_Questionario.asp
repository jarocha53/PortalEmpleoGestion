<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%@LANGUAGE = JScript %>
<!--#INCLUDE file = "include/openConnJS.asp"--> 
			
<%
//---------------------------------------------------------------------------------------------------------------------------------------------------------------
// Imposta il numero di messaggi per pagina
//var quanti_per_pagina = 5;

function messaggio(id_elemento, tipo_elemento ,livello, testo_domanda, tipo_domanda, tipo_sel, id_domandaiq, tipo_risposta, testo_risposta, testo_max, testo_min, iq_min, iq_max, fl_tiprisp, totale_risp, flagdom, id_blocco)
{
	
//	Response.Write ("id_elemento=" + id_elemento + "<br>");

//	this.id = id;
	this.livello = livello;
	this.id_elemento = id_elemento;
	this.tipo_elemento = tipo_elemento;
	this.testo_domanda = testo_domanda;
	this.tipo_sel = tipo_sel;
	this.id_domandaiq = id_domandaiq;
	this.tipo_domanda = tipo_domanda;
	this.tipo_risposta = tipo_risposta;
	this.txt_risposta = testo_risposta;
	this.testo_max = testo_max;
	this.testo_min = testo_min;
	this.iq_min = iq_min;
	this.iq_max = iq_max;
	this.fl_tiprisp = fl_tiprisp;  
	this.totale_risp = totale_risp;
	this.flagdom = flagdom;
	this.id_blocco = id_blocco;  
}

//---------------------------------------------------------------------------------------------------------------------------------------------------------------
function leggiMessaggi(rt,liv) {
		
	// Funzione ricorsiva di lettura
	sql =  " SELECT ID_INFOQUEST, ID_AREAIQ, ID_BLOCCO, TIPO_ELEMENTO, ID_ELEMENTO, ID_ELEM_ORIGINE, DT_TMST " 
	sql += " FROM STRUTTURA_QUEST "
	sql += " WHERE ID_AREAIQ = " + IdArea + " AND ID_ELEM_ORIGINE = " + rt + " AND ID_INFOQUEST = " + idquest 
	sql += " ORDER BY ID_BLOCCO, ID_ELEMENTO";
	
'PL-SQL * T-SQL  
SQL; = TransformPLSQLToTSQL (SQL;) 
	i_ris[liv] = CC.Execute(sql);
  
 	var TESTO_DOMANDA;
	var TIPO_SEL;
 	var ID_DOMANDAIQ;
	var TIPO_RISPOSTA;
	var TIPO_DOMANDA;
	var TXT_RISPOSTA;
	var TESTO_MAX;
	var TESTO_MIN;
	var IQ_MIN;
	var IQ_MAX;
	var FL_TIPRISP;
	var TOTALE_RISP;
	var FLAGDOM;	
	var ID_BLOCCO;
	
	while (!i_ris[liv].EOF){
		//LETTURA DOMANDA
		if (String(i_ris[liv]("TIPO_ELEMENTO"))=="D"){
			sql2=	"SELECT count (ID_RISPOSTAIQ)as TotRisp from IQ_RISPOSTA  WHERE ID_DOMANDAIQ =" + parseInt(i_ris[liv]("ID_ELEMENTO"))
'PL-SQL * T-SQL  
SQL2; = TransformPLSQLToTSQL (SQL2;) 
					RRrisp = CC.Execute(sql2);
			TOTALE_RISP = String(RRrisp("TotRisp"))	
							
		
			sqlDom = "SELECT TESTO_DOMANDA, TIPO_DOMANDA, TIPO_SEL, ID_DOMANDAIQ "
			sqlDom += " FROM IQ_DOMANDA WHERE ID_DOMANDAIQ =" + parseInt(i_ris[liv]("ID_ELEMENTO"))
			
'PL-SQL * T-SQL  
SQLDOM; = TransformPLSQLToTSQL (SQLDOM;) 
			RRdom = CC.Execute(sqlDom);
			
			ID_DOMANDAIQ = String(RRdom("ID_DOMANDAIQ"))
			TESTO_DOMANDA =	String(RRdom("TESTO_DOMANDA"))
			TIPO_DOMANDA = String(RRdom("TIPO_DOMANDA"))
			TIPO_SEL =	String(RRdom("TIPO_SEL"))		
			
		}
		//LETTURA RISPOSTA
		if (String(i_ris[liv]("TIPO_ELEMENTO"))=="R"){
			sqlRisp = "SELECT ID_DOMANDAIQ, TIPO, TXT_RISPOSTA, TESTO_MAX, TESTO_MIN, IQ_MIN, IQ_MAX, FL_TIPRISP "
			sqlRisp += " From IQ_RISPOSTA where ID_RISPOSTAIQ = " + parseInt(i_ris[liv]("ID_ELEMENTO"))
		
'PL-SQL * T-SQL  
SQLRISP; = TransformPLSQLToTSQL (SQLRISP;) 
			RRrisp = CC.Execute(sqlRisp);
		
			ID_DOMANDAIQ = String(RRrisp("ID_DOMANDAIQ"))
			TIPO_RISPOSTA = String(RRrisp("TIPO"))
			TXT_RISPOSTA = String(RRrisp("TXT_RISPOSTA"))
			TESTO_MAX = String(RRrisp("TESTO_MAX"))
			TESTO_MIN = String(RRrisp("TESTO_MIN"))
			IQ_MIN = String(RRrisp("IQ_MIN"))
			IQ_MAX = String(RRrisp("IQ_MAX"))
			FL_TIPRISP = String(RRrisp("FL_TIPRISP"))		
			
			TOTALE_RISP = 0	
			
			sqlRispDom = "SELECT ID_ELEMENTO"
			sqlRispDom += " FROM STRUTTURA_QUEST WHERE ID_ELEM_ORIGINE = " + parseInt(i_ris[liv]("ID_ELEMENTO"))

'PL-SQL * T-SQL  
SQLRISPDOM; = TransformPLSQLToTSQL (SQLRISPDOM;) 
			RRrispDom = CC.Execute(sqlRispDom);
			if (!RRrispDom.eof){ 		 
				FLAGDOM = 1
			}else{
				FLAGDOM = 0				
			}		
		
			sqlDom1 = "SELECT TIPO_DOMANDA, TIPO_SEL "
			sqlDom1 += " FROM IQ_DOMANDA WHERE ID_DOMANDAIQ =" + ID_DOMANDAIQ
'PL-SQL * T-SQL  
SQLDOM1; = TransformPLSQLToTSQL (SQLDOM1;) 
			RRdom1 = CC.Execute(sqlDom1);
			
			TIPO_DOMANDA = String(RRdom1("TIPO_DOMANDA"))
			TIPO_SEL =	String(RRdom1("TIPO_SEL"))
		}		

		lista[il++] = new messaggio(parseInt(i_ris[liv]("ID_ELEMENTO")),
									String(i_ris[liv]("TIPO_ELEMENTO")),
									liv,
									TESTO_DOMANDA,
									TIPO_DOMANDA,
									TIPO_SEL,
									ID_DOMANDAIQ,
									TIPO_RISPOSTA,
									TXT_RISPOSTA,
									TESTO_MAX,
									TESTO_MIN,
									IQ_MIN,
									IQ_MAX,
									FL_TIPRISP,
									TOTALE_RISP,
									FLAGDOM,
									parseInt(i_ris[liv]("ID_BLOCCO"))
									);                                                  
			
		leggiMessaggi(parseInt(i_ris[liv]("ID_ELEMENTO")), liv + 1);
    	i_ris[liv].MoveNext();
    	
	}
}

//---------------------------------------- MAIN -----------------------------------------------------------------------------------------------------------------------

var lista = new Array();
var i_ris = new Array();
var il = 0;
var i = 0;
var conta = 0;
var last = 0;
var appo = "NO";
var RespName = " ";
var idquest
var IdArea
var ii

// ID Questionario
var idquest = Request("idq");

// Id dell'Area
var IdArea = Request("ida");

// Porta i messaggi interessati dal database all'Array lista
leggiMessaggi(0, 0);

if ( !i_ris[0].EOF) {
		i_ris[0].MoveNext();
		if (!i_ris[0].EOF)	appo="SI"; 
	}
%>

<script language="JavaScript">
<!--#include Virtual = "/Include/help.inc"-->
<!--#include Virtual = "/Include/controlstring.inc"-->
var contadom = ""
var saltarisp = ""
var saltarispap = ""
var contadomap = ""
var ics = 0

function controlli (){


	if (document.form1.DOM113.value = "113_1015"){
		document.form1.DOM114.enabled;
	}else{
		document.form1.DOM114.disabled;
		document.form1.DOM115.disabled;
	}

	if (document.form1.DOM114.value = "114_1017"){
		document.form1.DOM115.enabled;
	}else{
		document.form1.DOM115.disabled;
	}


}



function chk() {
    var nd		//num. complessivo domande +1
    var conta	//verifica se c'� almeno una risp. per dom.
    var b
    var a		//num. risp. per domanda.
    var i
    
    var livDom
    var risposta
    var contarisp = 0  
    var NumDom
    var ii 
    var salvaz = 0
    var flc = 0
    var Ta
    var apconta = ""
    var apcontadom = ""
    var apsaltarisp = ""
    var apcontasaltarisp = ""
   
	contadom = ""
	saltarisp = ""
	saltarispap = ""
	contadomap = ""  
	// controlli()
   
    nd = document.form1.NDOM.value // Indica il numero delle domande ...
    //alert (nd)
	for(ics=1;ics<nd;ics++){ // Cicla per tutte le domande presenti....
		if (flc > 0){
			//ics = (ics - 1) // + flc 
			if (ics == nd){
				ics = ics + 1
				break ;
			}	
		flc = 0
		saltarispap = ""
		contadomap = ""
		}
		NumDom = eval("document.form1.numdom" + ics + ".value")
		i = NumDom // Contiene l'identificativo della domanda.
		livDom =  eval("document.form1.livello" + i + ".value")
		a = eval("document.form1.nRispXDom" + i + ".value")
		if (a != 0) {
			sel = eval("document.form1.tiposel" + i + ".value")
			if (sel != "") {
				if (a == 1) {
					b = eval("document.form1.tipocampo" + i + ".value")
				}
				else { 
					b = eval("document.form1.tipocampo" + i + "[1].value")
        		}					
				conta = NumDom
				contarisp = 0 
				for(z=0;z<a;z++) {	
					if (b == "radiobutton") {	
						if (a == 1) {
							tipo = eval("document.form1.tipo" + i + ".value")
						}
						else { 
							tipo = eval("document.form1.tipo" + i + "[0].value")
        				}					
						if (tipo == "V"){
							min = eval("document.form1.min" + i + ".value")
							max = eval("document.form1.max" + i + ".value")
							range = (max-min)
							risposta = eval("document.form1.flagcampo" + i  + ".value")
							for(x=0;x<=range;x++) {
								RbV = eval("document.form1.DOM" + i + "[x]")
								if (RbV.checked == true) {
									conta = "0"																	
									contarisp = parseInt(risposta)
									if (contarisp == 1){
										salvaz = z
										contadomap = (ControllaRisp())
										 apconta = contadomap.split(",")
										z = salvaz
									}			 
								}else{
									if (risposta == 1 ) {										
										salvaz = z	
										saltarispap = (SaltaRisposta ())
										apsaltarisp = saltarispap.split(",")
										z = salvaz 
									}
									else{ 
										salvaz = z
										saltarispap = saltarispap + saltarisp + ","							
										z = salvaz
									}	
								}
							}
						}
						else {
							Rb = eval("document.form1.DOM" + i + "[z]")
							if (Rb != "[object]") {
								Rb = eval("document.form1.DOM" + i )
								risposta = eval("document.form1.flagcampo" + i + ".value")
							} else {
								risposta = eval("document.form1.flagcampo" + i + "[z]" + ".value")

							}
//							alert ("ceccato : " + Rb.checked + " Domanda : " + i + " ha la risposta : " + risposta)
//							alert(i)
							numeroRadioButton = eval("document.form1.DOM" + i + ".length")
							
							if (Rb.checked == true) {  
								conta = "0"								
								contarisp = parseInt(risposta)
								if (contarisp == 1){
									salvaz = z
									contadomap = (ControllaRisp())
									apconta = contadomap.split(",")
									z = salvaz
								} 
								/*Rbbb = eval("document.form1.DOM" + i + "[0]")	
								risposta1= eval("document.form1.flagcampo" + i + "[0]" + ".value")
								contarisp = parseInt(risposta1)
								alert ( Rbbb )
								if (contarisp == 1){
									alert ( "verifico" )
									salvaz = z
									contadomap = (ControllaRisp())
									apconta = contadomap.split(",")
									z = salvaz
								} */
								
								
							}else{
								//alert (eval("document.form1.DOM" + i + ".length"))
								if (risposta == 1 ) {
									salvaz = z	
									saltarispap = (SaltaRisposta ())																	
       								apsaltarisp = saltarispap.split(",")
									z = salvaz 
								}
								
								else{ 
									//alert (eval("document.form1.DOM" + i + ".length"))
									salvaz = z
									saltarispap = saltarispap + saltarisp + ","							
									z = salvaz
								}	
							}
						}
					}
					if (b == "checkbox") {
						y = z+1
						Cb = eval("document.form1.chkMulti" + i + "_" + y)
						risposta = eval("document.form1.flagcampo" + i + "_" + y + ".value")
						if (Cb.checked == true) {
							conta = "0"
							contarisp = parseInt(risposta)
					
							if (contarisp == 1){
								salvaz = z
								contadomap = (ControllaRisp())
								apconta = contadomap.split(",")
								z = salvaz
							}			
						}else{	
							if (risposta == 1 ) {	
								salvaz = z	
								saltarispap = (SaltaRisposta ())
								apsaltarisp = saltarispap.split(",")
								z = salvaz  
							}
							else{ 
								salvaz = z
								saltarispap = saltarispap + saltarisp + ","							
								z = salvaz
							}										
						}
					}
				    if (b == "textarea") {
						y = z+1
						Ta = eval("document.form1.Altro" + i + "_" + y + ".value")
						risposta = eval("document.form1.flagcampo" + i + "_" + y + ".value")
						if (TRIM(Ta) != "") {
							conta = "0"
							contarisp = parseInt(risposta)
							if (contarisp == 1){
								salvaz = z
								contadomap = (ControllaRisp())
								apconta = contadomap.split(",")
								z = salvaz
							}
						}else{
							if (risposta == 1 ) {								
								salvaz = z	
								saltarispap = (SaltaRisposta ())
								apsaltarisp = saltarispap.split(",")
								z = salvaz 
							}
							else{ 
								salvaz = z
								saltarispap = saltarispap + saltarisp + ","							
								z = salvaz
							}	
						}
				    }
				    if (b == "indefinito") {
						conta = "0"
						break;
				    }
				}
				//alert("fine ciclo for (z) ")

				if (conta != "0") {
       				alert (' Selezionare almeno una risposta per ogni domanda!' + 'NumDom = ' + conta)       				
					return false;
       			}
				
				if (contadomap != ""){ 
					flc = flc + 1
					 apconta = contadomap.split(",")
					 for (hh=0;hh<apconta.length; hh++){
						if(apconta[hh]!= 0){
							alert (' Selezionare almeno una risposta per ogni domanda! NumDom = ' + apconta[hh])
							return false;
						}
					 }
				}
				
       			if (saltarispap != "") { 
       				flc = flc + 1   
       				apsaltarisp = saltarispap.split(",")
       				for (ss=0;ss<apsaltarisp.length; ss++){
						if (apsaltarisp[ss] != 0 ) {							
							alert (' Attenzione si sta rispondendo per una domanda non selezionata!' + 'NumDom = ' + apsaltarisp[ss])       				
							
						    return false;
						}
					}
				}
			}			
		}
    }
}

function ControllaRisp () {

	var NumDom
	var livDom
	var b

	ics = ics + 1
	
	NumDom = eval("document.form1.numdom" + ics + ".value")
	i = NumDom

	a = eval("document.form1.nRispXDom" + i + ".value")
	if (a != 0) {
		sel = eval("document.form1.tiposel" + i + ".value")
		if (sel != "") {
        	if (a == 1) {
				b = eval("document.form1.tipocampo" + i + ".value")
			}
			else {
        		b = eval("document.form1.tipocampo" + i + "[1].value")
        	}	
   					
			contadom = NumDom
			contarisp = 0 
			//ciclo sulle risposte per domanda
			for(z=0;z<a;z++) {
				//la soluzione � la riga qui sotto!!!
				i = NumDom
				if (b == "radiobutton") {
        			if (a == 1) 
						tipo = eval("document.form1.tipo" + i + ".value")
					else
						tipo = eval("document.form1.tipo" + i + "[0].value")
	
					if (tipo == "V"){
						min = eval("document.form1.min" + i + ".value")
						max = eval("document.form1.max" + i + ".value")
						range = (max-min)
						for(x=0;x<=range;x++) {
							RbV = eval("document.form1.DOM" + i + "[x]")
							//risposta = eval("document.form1.flagcampo" + i + "[x]" + ".value")
							if (RbV != "[object]") {
								RbV = eval("document.form1.DOM" + i )
								risposta = eval("document.form1.flagcampo" + i + ".value")
							} else {
								risposta = eval("document.form1.flagcampo" + i )
								if (risposta != "[object]")
									risposta = eval("document.form1.flagcampo" + i + "[x]" + ".value")
								else
									risposta = eval("document.form1.flagcampo" + i + ".value")
							}

							if (RbV.checked == true) {
								contarisp = parseInt(risposta)
								contadom = "0"								
								if (contarisp == 1){
									salvaz = z	
									ControllaRisp()
									z = salvaz														
								}else{ 
									salvaz = z
									z = salvaz
								}	
							}else{	
								if (risposta == 1 ) {
									salvaz = z		
									SaltaRisposta ()
									z = salvaz
								}
								else{ 
									if (a == 1){
										salvaz = z
										z = salvaz
									} 
								}										
							}
						}
									
					}else {
						Rb = eval("document.form1.DOM" + i + "[z]")

						if (Rb != "[object]") {
							Rb = eval("document.form1.DOM" + i )
							risposta = eval("document.form1.flagcampo" + i + ".value")
						} else {
							risposta = eval("document.form1.flagcampo" + i + "[z]" + ".value")
						}

						if (Rb.checked == true) {  
							contarisp = parseInt(risposta)
							contadom = "0"
							if (contarisp == 1){ 
								salvaz = z	
								ControllaRisp()
								z = salvaz															
							}else{ 
								salvaz = z
								z = salvaz
							}			
						}else{	
							if (risposta == 1 ) {
								salvaz = z		
								SaltaRisposta ()
								z = salvaz
							}
							else{ 
								if (a == 1){
									salvaz = z
									z = salvaz
								} 
							}										
						}
					}
							
				}
				if (b == "checkbox") {
					y = z+1
					Cb = eval("document.form1.chkMulti" + i + "_" + y)
					risposta = eval("document.form1.flagcampo" + i + "_" + y + ".value")


					if (Cb.checked == true) {
						contarisp = parseInt(risposta)
						contadom = "0"	
						if (contarisp == 1){
							salvaz = z
							ControllaRisp()
							z = salvaz															
						}else{ 
							salvaz = z
							z = salvaz
						}
					}else{	
						if (risposta == 1 ) {
							salvaz = z
							SaltaRisposta ()
							z = salvaz
						}
						else{ 
							if (a == 1){
								salvaz = z
								z = salvaz
							} 
						}	
							
					}
				}
			    if (b == "textarea") {
					y = z+1
					Ta = eval("document.form1.Altro" + i + "_" + y + ".value")
					risposta = eval("document.form1.flagcampo" + i + "_" + y + ".value")
					if (TRIM(Ta) != "") {
						contadom = "0"
						contarisp = parseInt(risposta)
						contadom = "0"
						if (contarisp == 1){
							salvaz = z	
							ControllaRisp()
							z = salvaz															
						}else{ 
							salvaz = z
							z = salvaz
						}	
					}else{	
						if (risposta == 1 ) {
							salvaz = z
							SaltaRisposta ()
							z = salvaz
						}
						else{ 
							if (a == 1){
								salvaz = z
								z = salvaz
								
							} 
						}
					}
				}
			    if (b == "indefinito") {
					contadom = "0"
					break;
				}
			}
		}
		contadomap  = contadomap + contadom +  ","	
		contadom = ""
	}
	//contadomap  = contadomap + contadom +  ","
	return contadomap;
}
function SaltaRisposta () {

var NumDom
var livDom
		
	ics = ics + 1	
		
	NumDom = eval("document.form1.numdom" + ics + ".value") 
	

	i = NumDom
		
	a = eval("document.form1.nRispXDom" + i + ".value")
	if (a != 0) {	
		sel = eval("document.form1.tiposel" + i + ".value")
		if (sel != "") {        
			if (a == 1) {
				b = eval("document.form1.tipocampo" + i + ".value")
			}
			else {
        		b = eval("document.form1.tipocampo" + i + "[1].value")
        	}					
        		
			contarisp = 0 
			saltarisp = "0"
			//ciclo sulle risposte per domanda
			for(z=0;z<a;z++) {
				i = NumDom
				if (b == "radiobutton") {	
        			if (a == 1) {
						tipo = eval("document.form1.tipo" + i + ".value")
					}
					else {
						tipo = eval("document.form1.tipo" + i + "[0].value")
        			}					
					if (tipo == "V"){
						min = eval("document.form1.min" + i + ".value")
						max = eval("document.form1.max" + i + ".value")
						range = (max-min)
						for(x=0;x<=range;x++) {
//							RbV = eval("document.form1.DOM" + i + "[x]")
//							risposta = eval("document.form1.flagcampo" + i + "[x]" + ".value")
							RbV = eval("document.form1.DOM" + i + "[x]")

							if (RbV != "[object]") {
								RbV = eval("document.form1.DOM" + i )
								risposta = eval("document.form1.flagcampo" + i + ".value")
							} else {
								risposta = eval("document.form1.flagcampo" + i )
								if (risposta != "[object]")
									risposta = eval("document.form1.flagcampo" + i + "[x]" + ".value")
								else
									risposta = eval("document.form1.flagcampo" + i + ".value")
							}



							if (RbV.checked == true) {
								RbV.checked = false
								saltarisp = NumDom
								contarisp = parseInt(risposta)
								if (risposta == 1 ) {	
									salvaz = z
									saltarispap = saltarispap + saltarisp + ","	
									SaltaRisposta ()									
									z = salvaz
								}
								else{
									salvaz = z
									saltarispap = saltarispap + saltarisp + ","							
									z = salvaz
								}	
							}
							else{
								if (risposta == 1 ) {
									salvaz = z	
									saltarispap = saltarispap + saltarisp + ","	
									SaltaRisposta ()
									z = salvaz
								}
								else{
									if (a == 1){
									salvaz = z
									z = salvaz
									} 								
								saltarispap = saltarispap + saltarisp + ","							
								}									
							}
						} 
										
					} else {
					
					// Inizio modifica 
						// Di quanti elemeni � composto il radio button?
						numEleChek = eval("document.form1.DOM" + i + ".length")
						// Devo controllare se sono ceccati e nel caso 
						// in cui lo siano devo inviare un messaggio perch� devono
						// essere dececcati (infatti la funzione si chiama salta risposta)
						vecchio = i
						for (cc=0;cc<numEleChek;cc++) {
							RbT = eval("document.form1.DOM" + vecchio + "[cc]")
							risposta = eval("document.form1.flagcampo" + vecchio + "[cc]" + ".value")
							if (RbT.checked == true) {
								
								RbT.checked = false
								saltarisp =  NumDom	
								contarisp = parseInt(risposta)
								if (risposta == 1 ) {
									salvaz = z
									saltarispap = saltarispap + saltarisp + ","		
									SaltaRisposta ()
									z = salvaz
								}else{
								salvaz = z
								saltarispap = saltarispap + saltarisp + ","							
								z = salvaz
								}	
							}else {
								if (risposta == 1 ) {
									salvaz = z	
									saltarispap = saltarispap + saltarisp + ","	
									SaltaRisposta ()
									z = salvaz
								}
								else{
									if (a == 1){
										salvaz = z
										z = salvaz
									} 							
								saltarispap = saltarispap + saltarisp + ","							
															
								}									
							}
						}
						
						
						
					// Fine modifica
					/*	Rb = eval("document.form1.DOM" + i + "[z]")
						if (Rb != "[object]") {
							Rb = eval("document.form1.DOM" + i )
							risposta = eval("document.form1.flagcampo" + i + ".value")
						} else {
							risposta = eval("document.form1.flagcampo" + i + "[z]" + ".value")

						}

						if (Rb.checked == true) {  
							Rb.checked = false
							saltarisp =  NumDom	
							contarisp = parseInt(risposta)
							if (risposta == 1 ) {
								salvaz = z
								saltarispap = saltarispap + saltarisp + ","		
								SaltaRisposta ()
								z = salvaz
							}else{
							salvaz = z
							saltarispap = saltarispap + saltarisp + ","							
							z = salvaz
							}	
						}
						else
						{
							if (risposta == 1 ) {
								salvaz = z	
								saltarispap = saltarispap + saltarisp + ","	
								SaltaRisposta ()
								z = salvaz
							}
							else{
								if (a == 1){
									salvaz = z
									z = salvaz
								} 							
							saltarispap = saltarispap + saltarisp + ","							
							
							}									
						}*/
					}
	
				}
				if (b == "checkbox") {
					y = z+1					
					Cb = eval("document.form1.chkMulti" + i + "_" + y)					
					risposta = eval("document.form1.flagcampo" + i + "_" + y + ".value")
					if (Cb.checked == true) {
						saltarisp =  NumDom
						contarisp = parseInt(risposta)
						if (risposta == 1 ) {	
							salvaz = z
							saltarispap = saltarispap + saltarisp + ","
							SaltaRisposta ()							
							z = salvaz								
						}else{
							salvaz = z
							saltarispap = saltarispap + saltarisp + ","							
							z = salvaz
						}					
					}else{
						if (risposta == 1 ) {
							salvaz = z
							saltarispap = saltarispap + saltarisp + ","
							SaltaRisposta ()
							z = salvaz
						}
						else{ if (a == 1){
								salvaz = z
								z = salvaz
								} 
							
						saltarispap = saltarispap + saltarisp + ","							
							
						}							
					}		
				}
			    if (b == "textarea") {
					y = z+1
					Ta = eval("document.form1.Altro" + i + "_" + y + ".value")					
					risposta = eval("document.form1.flagcampo" + i + "_" + y + ".value")
					if (Ta != "") {
						saltarisp =  NumDom
						contarisp = parseInt(risposta)
						if (risposta == 1 ) {	
							salvaz = z							
							saltarispap = saltarispap + saltarisp + ","
							SaltaRisposta ()
							z = salvaz	
							}else{ 
							salvaz = z
							saltarispap = saltarispap + saltarisp + ","							
							z = salvaz
							}	
					}else{ 
						if (risposta == 1 ) {
							salvaz = z
							saltarispap = saltarispap + saltarisp + ","
							SaltaRisposta ()
							z = salvaz
						}	
						else{ 
							if (a == 1){
								salvaz = z
								z = salvaz
								} 
							saltarispap = saltarispap + saltarisp + ","							
						}	
					}
			    }
			    
			    if (b == "indefinito") {
					saltarisp = "0"
			    }
			}
		}

    saltarispap  =  saltarispap + saltarisp + ","
	return saltarispap			
    
    }
    	
}
//---------------------------------------------------------------------------------------------------------------------------------------------------------------

</script>
<%
	var descarea = 1
	var appoArea
	var numaree
	var nAccessi
	var nIdRisultato
	var DtTmst
	
	sSQL = "select count(*) as totaree from iq_area where id_infoiq = " + idquest + " order by id_areaiq"
		
	var rsArea = server.CreateObject ("adodb.recordset")
	rsArea.Open(sSQL, CC); 
	appoArea = rsArea("totaree")

	if(appoArea > 1 )
	{
		numaree = 2
	}
	
	sqlArea = "SELECT DESC_AREAIQ, ID_AREAIQ FROM IQ_AREA WHERE ID_AREAIQ =" + IdArea 
	RRArea  = Server.createObject("adodb.recordset");
	RRArea.Open(sqlArea, CC);
	
	if (!RRArea.eof){ 		 
		RespName = RRArea.Fields("DESC_AREAIQ")
	}		

	sqlQuest = "SELECT DESC_QUEST,intest_quest FROM INFO_QUEST WHERE ID_INFOQUEST =" + idquest 
	RRQuest  = Server.createObject("adodb.recordset");
	RRQuest.Open(sqlQuest, CC);
	
	if (!RRQuest.eof){ 		 
		RespQuest = RRQuest.Fields("DESC_QUEST")
		IntestQuest = RRQuest.Fields("intest_quest")
	}		
	
	
	sql= "SELECT ID_RISULTATO, NUM_ACCESSI, DT_TMST FROM IQ_RISULTATO WHERE ID_INFOQUEST = " + idquest + " and IDUTENTE = " + Session("idutente") + " and ID_AREAIQ = " + IdArea 
	var RsControllo=Server.CreateObject("ADODB.Recordset");

	RsControllo.Open(sql, CC);
	if (!RsControllo.eof){
		DtTmst = RsControllo.Fields("DT_TMST")  
		nIdRisultato =RsControllo.Fields("ID_RISULTATO")
		nAccessi = RsControllo.Fields("NUM_ACCESSI")
		nAccessi = (nAccessi) + 1
		
		sqlUpdate= "UPDATE IQ_RISULTATO SET NUM_ACCESSI = " + nAccessi + ", DT_TMST = SYSDATE where ID_RISULTATO = " + nIdRisultato // + " and ID_AREA = " + IdArea 
		
		var RsRisul=Server.CreateObject("ADODB.Recordset");
		RsRisul.Open(sql, CC);
	}else{
		nAccessi = 1
		sql="INSERT INTO IQ_RISULTATO (ID_INFOQUEST,IDUTENTE,NUM_ACCESSI,ESEGUITO,DT_TMST,ID_AREAIQ) VALUES ("+ idquest + "," + session("idutente") + "," + nAccessi + ",'N',SYSDATE, " + IdArea + ")"
		
		var RsRisul=Server.CreateObject("ADODB.Recordset");
		RsRisul.Open(sql, CC);
	}

	RsControllo.CLOSE

%>	
	<!--Epili 13/01/02 Per poter utilizzare le strutture standard.-->

	<script language="VBScript" runat="server"> 
'PL-SQL * T-SQL  
		Server.Execute ("/strutt_testa1.asp") 
	</script>
	<form action="QGR_InvQuestionario.asp" method="POST" id="form1" onsubmit="return chk(this)" name="form1">
		<input type="hidden" name="IDQue" value="<%=idquest%>">
		<input type="hidden" name="DESCR" value="<%=RespQuest%>">
		<input type="hidden" name="DESCRAREA" value="<%=RespName%>">
		<input type="hidden" name="Ida" value="<%=IdArea%>">

		<br>
	<p align="left"><span class="tbltext"><b>&nbsp;<%=Request("frui")%></b></span><br>
	<p align="left"><span class="tbltext"><b>&nbsp;Titolo :</b> <%=Server.HTMLEncode(RespQuest)%></span><br>


	<table width="100%" border="0">
		<tr>
			<td width="100%" align="left" class="tbldett"><%=IntestQuest%></td>
		</tr>
	</table>	
	<table width="100%" border="0">
		<tr>
			<td width="100%" align="left" class="tbltext1">
			<b>Programma : <%=Request("progetto")%></b>
			</td>
		</tr>

		<tr>
			<td width="100%" align="left" class="tbltext1">
			<b>Codice commessa : <%=Request("commessa")%></b>
			</td>
		</tr>
		<tr>
			<td width="100%" align="left" class="tbltext1">
			<b>Erogazione : <%=Request("ero")%></b>
			</td>
		</tr>
		<!--tr>			<td width="100%" align="left" class="tbltext1">			<b>Fruizione : <%=Request("frui")%></b>			</td>		</tr-->
	<%	if ( Request("intervento")!="" ){ %>
		<tr>
			<td width="100%" align="left" class="tbltext1">
			<b>Titolo intervento formativo : <%=Request("intervento")%></b>
			</td>
		</tr>
	<%	}	%>	
	</table>	
	
	

<%	if (numaree==2){ %>
	<span class="tbltext"><b>&nbsp;SEZIONE:</b> <%=Server.HTMLEncode(RespName)%></span>
<%
		}
%>	</p>	

	<%
	var sm
	var tpr
	var cVerify
	var nrisp
	var nd
	var x
	var ndom
	var salvaiddom = ""
	var flagsalvadom = 1
	var salvax = 0
	var salvadominizio = 0
	var salvadom = " "

	cVerify = 1
	nrisp = 1
	nd = 1
	x = 1
	ndom = 0
	
	//sm = Session("mask")
	sm = "05"

    if (lista.length==0){	
       Response.Write(" <br><br><span class=textred><b>NON SONO PRESENTI DOMANDE</b></span><br><br><br><br> ");
    	%><input type="hidden" name="nRispXDom<%=ndom%>" value="<%=0%>"><%
    }else{
		for (i=0;i<lista.length;i++) {
			 tpr = lista[i].id_domandaiq 			
			 Response.Write("<div align=left id='div" + tpr + "' name='div" + tpr + "' style='visibility:visible'><table border=0 width=100% ><tr class=tblsfondo>")
		     Response.Write("<td width=100% height=1 class=textblack colspan=2>");
		     for (j=0;j<lista[i].livello;j++)
				Response.Write("<img src='" + Session("Progetto") + "/images/blank_trasp.gif' width=20 height=16>")
				if (lista[i].tipo_elemento=="D"){
					Response.Write("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src='" + Session("Progetto") + "/images/re.gif' >")
					nd = nd + 1 
					//ndom = ndom + 1 
					ndom = lista[i].id_domandaiq
					x = 1
					if (lista[i].livello == 0) {
						salvaiddom = ""
						flagsalvadom = 1
						salvadom = " "
						salvax = 0
					}
					if (lista[i].tipo_sel == "null")
						lista[i].tipo_sel = " "
					%> 
					   <b><%=lista[i].id_domandaiq%>&nbsp;&nbsp; <%=lista[i].testo_domanda%></b>					 
					   <input type="hidden" name="nRispXDom<%=ndom%>" value="<%=lista[i].totale_risp%>">
					    <input type="hidden" name="nDomanda" value="<%=lista[i].id_domandaiq%>">
					   <input type="hidden" name="tiposel<%=ndom%>" value="<%=lista[i].tipo_sel%>">
   					   <input type="hidden" name="livello<%=ndom%>" value="<%=lista[i].livello%>">
   					   <input type="hidden" name="tipoele<%=ndom%>" value="<%=lista[i].tipo_elemento%>">
   				 	   <input type="hidden" name="numdom<%=nd-1%>" value="<%=lista[i].id_domandaiq%>">   					  
					   <!-- - <%=lista[i].tipo_elemento%> - <%=lista[i].id_elemento%> - <%=lista[i].tipo_sel%> - <%=lista[i].tipo_domanda%>-->
					   <%					   		
				}else{
			 		Response.Write("<img src='" + Session("Progetto") + "/images/blank_trasp.gif' width=30 height=16>")	
			 		nrisp =  nrisp + 1	
			 		 %>	
					<!--#INCLUDE file = "QGR_inc_forrispostaut.asp"-->
					<%								
					}
					Response.Write("</td></tr></table></div>")

		}
	} 
  %>

<table border="0" cellspacing="1" cellpadding="1" width="630">
	
<script language="VBScript" runat="server"> 
'PL-SQL * T-SQL  
	Server.Execute ("/Include/ckProfile.asp") 
</script>

</table>
<br><br>

<table border="0" width="750" height="8" cellpadding="0" cellspacing="0"> 
<tr height="1">
	<td width="100%" background="/DEMOGP/images/separazione.gif"></td>
</tr>
</table>

<br>

<%			
if (nrisp > 1){ %>
		<br>
		<table border="0" cellspacing="2" cellpadding="1" width="500px">
			<tr>
				<td align="center">					
					<input type="hidden" name="NDOM" value="<%=nd%>">
					<input type="hidden" name="NREC" value="<%=nrisp%>">					
					&nbsp;&nbsp;&nbsp;
					
					<a onmouseover="javascript:window.status='' ; return true" href="javascript:history.back()">
					<img src="<%=Session("progetto")%>/images/indietro.gif" border="0" alt="Torna alla pagina precedente">
					</a>
					<input type="image" name="Conferma" src="<%=Session("Progetto")%>/images/conferma.gif" title="Invia questionario">
				</td>
			</tr>
		</table>		
<%	}
else {%>
	<a onmouseover="javascript:window.status='' ; return true" href="javascript:history.back()">
		<img src="<%=Session("progetto")%>/images/indietro.gif" border="0" alt="Torna alla pagina precedente">
	</a>
<% } %>  

</form>

<form id="frmReload" name="frmReload" action="QGR_Questionario.asp" method="post">
	<input type="hidden" name="idq" value="<%=idquest%>">						
	<input type="hidden" name="ida" value="<%=IdArea%>">		
</form> 
		
   
<%    
// Chiude la connessione al database
CC.Close();

%>

<!--Epili 13/01/02 Per poter utilizzare le strutture standard.-->


