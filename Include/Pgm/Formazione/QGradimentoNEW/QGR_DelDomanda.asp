<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%
Sub Inizio
	dim sFunzione
	dim sTitolo
	dim sCommento
	dim bCampiObbl
	
	sFunzione = "GESTIONE QUESTIONARI DI GRADIMENTO"
	sTitolo = "CANCELLAZIONE DOMANDA"
	sCommento = "Conferma della cancellazione della Domanda"
	bCampiObbl = false
%>
	<!--#include virtual="include/SetTestata.asp"-->
<%
End Sub
'--------------------------------------------------------------------------
Sub Cancella()
	dim id
	dim ida, idq
	dim sSQL, sSQL2

'''9/7/2003: RICEVUTI VALORI DA FORM ANZICHE' QUERYSTRING
	'''id = Request("idd")
	'''ida = Request("ida")
	'''idq = Request("idq")
	'''dDtTmst = request("tmst")
	id = Request("IdDom")
	ida = Request("IdArea")
	idq = Request("IdQuest")
	dDtTmst = request("dtTmst")
'''FINE 9/7/2003

	'Controllo che non esistano Risposte associate alla domanda
	sSQL =  " select count(*) as CRisp" &_
			" from iq_risposta" &_
			" where id_domandaiq = " & id 
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	set rsCRisp = cc.execute(sSQL)
		if cint(rsCRisp("CRisp")) > 0 then
%>
			<br><br>
			<table border=0 cellspacing=2 cellpadding=1 width="500">
				<tr align=middle>
					<td class="tbltext3">
						Non � possibile cancellare la Domanda.
						<br>
						Cancellare prima le Risposte associate.
					</td>
				</tr>
			</table>		
			<br><br>
<%
		else
			sSQL = "delete from iq_domanda where id_domandaiq = " & id
			sErrore=Esegui(id ,"iq_domanda",Session("persona"),"DEL",sSQL,1,dDtTmst)

			if sErrore = "0" then
%>
				<br><br>
				<table border=0 cellspacing=2 cellpadding=1 width='500'>
					<tr align=middle>
						<td class='tbltext3'>
							Cancellazione della Domanda correttamente effettuata
						</td>
					</tr>
				</table>
<%
			else
%>
				<br><br>
				<table border=0 cellspacing=2 cellpadding=1 width='500'>
					<tr align=middle>
						<td class='tbltext3'>
							Impossibile cancellare la domanda.
							<BR>
							Errore: <%=sErrore%>
						</td>
					</tr>
				</table>
<%
			end if
		end if
%>
	<br><br>
<%'''9/7/2003: PASSATI VALORI TRAMITE FORM ANZICHE' QUERYSTRING %>
	<form name="frmDomanda" method="post" action="QGR_VisDomanda.asp">
		<input type="hidden" name="idq" value="<%=idq%>">
		<input type="hidden" name="ida" value="<%=ida%>">
	</form>
<%'''FINE 9/7/2003%>
	<table width=500>
		<tr>
			<td align=center>
<%'''9/7/2003: PASSATI VALORI TRAMITE FORM ANZICHE' QUERYSTRING 
				'''PlsLinkRosso "QGR_VisDomanda.asp?ida=" & ida & "&idq=" & idq, "Elenco delle Domande" %>
				<a onmouseover="javascript:window.status='' ; return true" href="javascript:frmDomanda.submit();" class="textred">
					<b>Elenco delle Domande</b>
				</a>
			</td>
		</tr>
	</table>
	<br><br>

<%
End Sub
'------------------------------------------------------------------------
'M A I N
%>
<!--#include virtual="/strutt_testa2.asp"-->
<!--#include virtual="/include/openconn.asp"-->
<!--#include virtual="/Util/DBUtil.asp"-->
<!--#include virtual="/Include/SysFunction.asp"-->
<!--#include virtual="include/SetPulsanti.asp"-->
<%	
	Inizio()
	Cancella()
%>
<!--#include virtual="/include/closeconn.asp"-->
<!--#include virtual="/strutt_coda2.asp"-->
