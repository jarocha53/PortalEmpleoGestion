<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<style type="text/css">
	.ui-menu .ui-icon{
 		*left:-2em !important;
 	}
</style>
<script language="Javascript">
	function VisDomande(canale,area) {	
		document.frmVisDom.IdCanale.value = canale;
		document.frmVisDom.IdAreaTem.value = area;
		document.frmVisDom.submit();
	}
	
	function Visualizza(canale,area) {
		document.frmVis.IdCanale.value = canale;
		document.frmVis.IdAreaTem.value = area;
		document.frmVis.submit();
	}
		
	function Abilita(canale,area) {	
		document.frmIscr.IdCanale.value = canale;
		document.frmIscr.IdAreaTem.value = area;
		document.frmIscr.submit();
	}

	function Modifica(canale,area) {	
		document.frmMod.IdCanale.value = canale;
		document.frmMod.IdAreaTem.value = area;
		document.frmMod.submit();
	}
	
	function Inserisci(area) {	
		document.frmIns.IdAreaTem.value = area;
		document.frmIns.submit();
	}
	
</script>
<%

	Sub ImpostaPag()
		Dim rsArea, SQLArea, x, nTopicPresenti, rsNumDR, SQLNumDR
		
		Set rsArea=Server.CreateObject("ADODB.Recordset")

		' Chi pu� visualizzare e gestire tutti i tipi di forum deve avere impostato il 
		' mask a 15. Il caso da testare � il numero 8 [che indica cancellazione].
		
		If ckProfile(Session("mask"),8) Then
			SQLArea = "SELECT f.id_area, f.id_frm_area, f.desc_area, f.titolo_area, f.img_area, f.tipo, f.dt_tmst, a.titoloarea " &_
			          "FROM frm_area f, area a WHERE f.id_area = a.id_area AND tipo = 'F' ORDER BY f.id_area" 
		Else
			SQLArea = "SELECT f.id_area, f.id_frm_area, f.desc_area, f.titolo_area, f.img_area, f.tipo, f.dt_tmst, a.titoloarea " &_
			          "FROM frm_area f, area a WHERE tipo = 'F' AND f.id_area = a.id_area AND f.id_frm_area " &_
			          "IN (SELECT id_frm_area FROM frm_iscrizione WHERE  tipo = 'F' AND " &_
			          "((cod_ruolo='" & Session("Rorga") & "' AND idgruppo IS NULL) " &_
					  "OR (cod_ruolo IS NULL) OR (cod_ruolo='" & Session("Rorga") & "' AND idgruppo = " & Session("idgruppo") & ")) " &_
			          "AND " & ConvDateToDbS(Date()) & " BETWEEN DT_DAL AND nvl(DT_AL, to_date('31/12/9999', 'dd/mm/yyyy')) ) ORDER BY f.id_area"			
		End If
		
'PL-SQL * T-SQL  
SQLAREA = TransformPLSQLToTSQL (SQLAREA) 
		rsArea.Open SQLArea, CC,3

		If Not rsArea.EOF Then
			' rsArea.MoveFirst 
%>
			<table border="0" cellspacing="0" cellpadding="0" width="90%" align="center">
				<tr> 
					<td> 
						<h2 class="tbltext">
							Benvenuto nella sezione della community dell'area <%=Session("TIT")%> dedicata 
							ai dibattiti online. Qui potrai dire la tua, fare domande e confrontarti 
							con il parere di tante altre persone. Scegli la categoria che pi� ti 
							interessa e leggi i vari interventi.
						</h2>
						<h2 align="center" class="tbltext">
							Scegli un canale del forum
						</h2>
					</td>
				</tr>
			</table>

			<table border="0" cellspacing="0" cellpadding="0" width="100%" align="center">
<%			
			Do While Not rsArea.eof
				x = 1			
			Do While Not rsArea.eof And x <= 2
			
			nConfronta = CLng(rsArea("id_area"))
			If clng(nIdArea) <> clng(nConfronta) Then	
%>
				<tr>
					<td colspan="7" height="1">&nbsp;</td> 
				</tr>
				<tr>
					<td class="tbltext1" colspan="7">
						<b><%=Server.HTMLEncode(rsArea("titoloarea"))%></b>
<%						If ckProfile(Session("mask"),8) Then %>
							<a class="textred" href="javascript:Inserisci('<%=rsArea("id_area")%>')"><img align="middle" src="/images/SistDoc/CreaDoc.gif" alt="inserisci un nuovo canale" border="0" width="23" height="21"></a>			
<%						End If %> 
					</td>
				</tr>
				<tr>
					<td colspan="7" height="2">&nbsp;</td> 
				</tr>
<%
				nIdArea = rsArea("id_area")
			End if
					nIdFrmArea = clng(rsArea("id_frm_area"))
					
					Set rsNumTopic=Server.CreateObject("ADODB.Recordset")
					SQLNumTopic = "SELECT count(id_frm_domanda) as Topic FROM frm_domanda WHERE id_frm_area=" & nIdFrmArea
					'PL-SQL * T-SQL  
					SQLNUMTOPIC = TransformPLSQLToTSQL (SQLNUMTOPIC) 
					rsNumTopic.Open SQLNumTopic, CC, 3
					nTopicPresenti = 0
					If Not rsNumTopic.EOF Then
						nTopicPresenti = rsNumTopic("Topic")
					End If	
					rsNumTopic.Close
					set rsNumTopic=Nothing
					
					Set rsNumDR=Server.CreateObject("ADODB.Recordset")
					SQLNumDR = "SELECT count(id_frm_risposta) as Msg FROM frm_risposta a, frm_domanda b " &_
					           "WHERE a.id_frm_domanda=b.id_frm_domanda AND b.id_frm_area=" & nIdFrmArea
					
					'PL-SQL * T-SQL  
					SQLNUMDR = TransformPLSQLToTSQL (SQLNUMDR) 
					rsNumDR.Open SQLNumDR, CC, 3
					
					nDRPresenti = 0
					If Not rsNumDR.EOF Then
						nDRPresenti = rsNumDR("Msg")
					End If	
					rsNumDR.Close
					set rsNumDR=Nothing
					
					If ckProfile(Session("mask"),4) Then 
%>
						<tr>
						<td width="9%" align="center">
							<a href="javascript:Modifica('<%=nIdFrmArea%>','<%=nIdArea%>')"><img src="/images/SistDoc/ModDoc.gif" alt="modifica nome e descrizione del canale" border="0" width="23">
						</td>	
						<td width="9%" align="center">
							<a href="javascript:Abilita('<%=nIdFrmArea%>','<%=nIdArea%>')"><img src="/images/utente.gif" alt="modifica l'abilitazione al canale" border="0" WIDTH="20">
						</td>		
<%					Else  %>
						<tr>
						<td width="1%" align="center">&nbsp;</td>
						<td width="1%" align="center">&nbsp;</td>					
<%					End If  %>
					<td width="2%" valign="middle"><img src="<%=Session("Progetto")%>/images/righinaPSX.jpg"></td> 
					<td width="66%" class="tblsfondo3" nowrap> 
						<a class="tbltext0" href="javascript:VisDomande('<%=nIdFrmArea%>','<%=nIdArea%>')" onmouseover="window.status =' '; return true">
						<b><%=Server.HTMLEncode(rsArea("TITOLO_AREA"))%></b>
						<%If Not ISNULL(rsArea("IMG_AREA")) Then %>
							<%=rsArea("IMG_AREA")%>
						<%End If %>
						</a>
					</td>
					<td width="4%" align="center" nowrap class="tblsfondo3"><span class="tbltext0">&nbsp;<%=nTopicPresenti%> Topic &nbsp;&nbsp;</span></td>
					<td width="4%" align="center" nowrap class="tblsfondo3"><span class="tbltext0">&nbsp;<%=nDRPresenti%> msg &nbsp;</span></td>
					<td width="2%" valign="middle"><img src="<%=Session("Progetto")%>/images/righinaPDX.jpg"></td>
				</tr>
				<tr>
					<td width="1%" align="center">&nbsp;</td>
					<td width="1%" align="center">&nbsp;</td>
					<td colspan="5">
						<span>&nbsp; &nbsp;<a class="tbltext" href="javascript:VisDomande('<%=nIdFrmArea%>','<%=nIdArea%>')" onmouseover="window.status =' '; return true">
						<%= Server.HTMLEncode(rsArea("DESC_AREA")) %></a></span>
					</td>
				</tr>
				<tr>
					<td colspan="7" height="2" valign="middle" background="<%=Session("Progetto")%>/images/separarighe.gif"></td> 
		        </tr>
				<% rsArea.MoveNext()
				   x = x + 1
				Loop 
			Loop %> 
			</table>
<%		Else %>
			<br><br><br>
			<table border="0" cellspacing="0" cellpadding="0" width="100%" align="center">
				<tr align="center"> 
					<td class="tbltext3"><b class="size">
						Actualmente no existen foros habilitados</b>
					</td>
				</tr>
			</table>
			<br><br><br>
<%		End If 
		If ckProfile(Session("mask"),8) Then %>
			<br>
			<table border="0" cellspacing="0" cellpadding="0" width="100%">
				<tr align="center"> 
					<td>
						<a class="textred" href="/Pgm/formazione/forum/FOR_InsIscrizione.asp"><b> Inserisci Nuovo Canale</b></a>
					</td>
				</tr>
			</table><br>
<%	    End If %>
		<form name="frmVisDom" method="post" action="/Pgm/formazione/FORUM/FOR_VisDomande.asp">
			<input type="hidden" name="IdCanale">
			<input type="hidden" name="IdAreaTem">
			<input type="hidden" name="NomePage" value="FOR_VisDomande">
		</form>
		<form method="post" name="frmIscr" action="/Pgm/Formazione/FORUM/FOR_ModIscrizione.asp">
			<input type="hidden" name="IdCanale">
			<input type="hidden" name="IdAreaTem">
		</form>
		<form method="post" name="frmMod" action="/Pgm/Formazione/FORUM/FOR_Modifica.asp">
			<input type="hidden" name="IdCanale">
			<input type="hidden" name="IdAreaTem">
			<input type="hidden" name="oggetto" value="can">
		</form>	
		<form method="post" name="frmIns" action="/Pgm/Formazione/FORUM/FOR_InsIscrizione.asp">
			<input type="hidden" name="IdAreaTem">
		</form>	
		<table border="0" width="100%">
			<tr>
				<td align="center">
					<a href="javascript:document.location='<%=Session("Progetto")%>/home.asp'">
					<img src="<%=Session("Progetto")%>/images/indietro.gif" border="0" alt="ritorna alla aree"></a>
				</td>
			</tr>
		</table>		
<%	End Sub %>
<!--		          MAIN			     -->

<!--#include Virtual="/strutt_testa2.asp"-->
<!--#include Virtual="/include/openconn.asp"-->	
<!--#include Virtual="/include/ckProfile.asp"-->	
<!--#include Virtual="/Util/DBUtil.asp"-->

<br>
<table border="0" width="100%" cellspacing="0" cellpadding="0" height="73">
	<tr>
		<td width="500" background="<%=Session("Progetto")%>/images/titoli/Community2b.gif" height="73" valign="bottom" align="right">
			<table border="0" width="500" height="30" cellspacing="0" cellpadding="0">
				<tr>
					
          <td width="100%" valign="top" align="right"><b class="tbltext1">Foro de discusi&oacute;n</b></td>
				</tr>
				<tr>
					<td width="100%" align="right">	
						<a href="Javascript:Show_Help('/Pgm/help/Formazione/Forum/FOR_VisCanali')" name onmouseover="javascript:status='' ; return true">
						<img alt="per maggiori informazioni" align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
					</td>
				</tr>	
			</table>
		</td>
	</tr>
</table>
<%
If Session("login") = "" Then 
		response.redirect Session("Progetto") & "/Testi/nologin.asp"
End If
	
If Not ValidateService(Session("IdUtente"),"GESTIONE FORUM",cc) Then 
	response.redirect "/util/error_login.asp"
End If
	
ImpostaPag()
	
%>
<!--#include Virtual="/strutt_coda2.asp"-->
<!--#include Virtual="/include/closeconn.asp"-->
