<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<script LANGUAGE="JavaScript" src="/Include/Help.inc">
</script>
<script LANGUAGE="JavaScript">
function Azione(sQueScelti,evento)
{
	var n
	var posizione,PosVirgolaSucc
	var trova //codice da trovare
	var inElenco
	//� booleana ed indica se il check controllato � gi� in sQueScelti
	//AZIONI :1>E' in elenco e ora non risulta ceccato --> lo elimino da sQueScelti
	//		  2>Non � in elenco(sQueScelti) e ora risulta ceccato --> lo aggiungo a sQueScelti
	//Negli altri due casi la situazione di sQueScelti rimane invariata
	
	if (document.frmQuestionari!=undefined)//se c'� un elenco di corsi viene creato il form
	{
		n=document.frmQuestionari.chkCorsi.length
		//alert('INGRESSO sQueScelti=' + sQueScelti)
		
		if (sQueScelti!='')
		{	//servono come delimitatori per la ricerca nella stinga
			//alert('ora aggiungo virgole se mancanti')
			if (sQueScelti.substring(0,1)!=',')
			{
				sQueScelti=',' + sQueScelti
			}
			if (sQueScelti.substr(sQueScelti.length-1)!=',')
			{
				sQueScelti=sQueScelti + ','
			}
		}
		
		//alert('sQueScelti=' + sQueScelti)		
		
		//alert('n=' + n)
		if (n==undefined)
		{//� uno solo
				
				inElenco=false
				trova=',' + document.frmQuestionari.chkCorsi.value + ','
				//alert('trova=' + trova)
				posizione=sQueScelti.search(trova)
				//alert('posizione=' + posizione)
				if (posizione!=(-1))
				{
					inElenco=true
				}
				//alert('inElenco=' + inElenco)
				if ((!document.frmQuestionari.chkCorsi.checked) && inElenco )
				{//lo elimino da sQueScelti
					//alert('lo elimino')
					PosVirgolaSucc=sQueScelti.indexOf(',',posizione+1)
					//alert('PosVirgolaSucc=' + PosVirgolaSucc)
					
					sQueScelti=sQueScelti.substring(0,posizione) + sQueScelti.substr(PosVirgolaSucc)
					//alert('sQueScelti=' + sQueScelti)
				}
					
				if ((document.frmQuestionari.chkCorsi.checked) && (!inElenco) )
				{//lo aggiungo a sQueScelti
					//alert('lo aggiungo')
					sQueScelti=sQueScelti + document.frmQuestionari.chkCorsi.value + ","
					//alert('sQueScelti=' + sQueScelti)
				}
		}
		else
		{//+ di 1
			for (i=0;i<n;i++)
			{
				inElenco=false
				trova=',' + document.frmQuestionari.chkCorsi[i].value + ','
				//alert('trova=' + trova)
				posizione=sQueScelti.search(trova)
				//alert('posizione=' + posizione)
				if (posizione!=(-1))
				{
					inElenco=true
				}
				//alert('inElenco=' + inElenco)
				
				if ((!document.frmQuestionari.chkCorsi[i].checked) && inElenco )
				{//lo elimino da sQueScelti
					//alert('lo elimino')
					
					PosVirgolaSucc=sQueScelti.indexOf(',',posizione+1)
					//alert('PosVirgolaSucc=' + PosVirgolaSucc)
					
					sQueScelti=sQueScelti.substring(0,posizione) + sQueScelti.substr(PosVirgolaSucc)
					//alert('sQueScelti=' + sQueScelti)

				}
				
				if ((document.frmQuestionari.chkCorsi[i].checked) && (!inElenco) )
				{//lo aggiungo a sQueScelti
					//alert('lo aggiungo')
					sQueScelti=sQueScelti + document.frmQuestionari.chkCorsi[i].value + ","
					//alert('sQueScelti=' + sQueScelti)
				}
			}// fine for per controllo checkbox
			
		}// fine else se + di un checkbox
	}// se esiste frmCorsi
	
	
	if (sQueScelti.substring(0,1)==',')//se il primo carattere � virgola la elimina
	{
		sQueScelti=sQueScelti.substr(1)
	}
	if (sQueScelti.substr(sQueScelti.length-1)==',')//viene eliminata l'ultima virgola che rimane se si elimina dall'elenco sQueScelti l'ultimo codice
	{
		//alert('elimino virgola finele')
		//alert('sQueScelti=' + sQueScelti)
		sQueScelti=sQueScelti.substring(0,sQueScelti.length-1)		
	}
	
	//alert('USCITA sQueScelti=' + sQueScelti)
	
	if (evento=='Cambio Area Tematica')
	{
		document.frmAreaTem.UCorScel.value=sQueScelti
		document.frmAreaTem.submit()
	}
	if (evento=='Chiudi Finestra')
	{
		opener.document.frmPercForma.hQuestScelti.value=sQueScelti
		//opener.document.frmPercForma.action=""
		//non specificando l'action va sottomessa di default la pagina opener
		//ci� permette di utilizzate questa pagina con pi� finestre opener che la chiamano
		opener.document.frmPercForma.submit();
		self.close();
	}
}
</script>

<%'--------------------------------------------------------------------------------
Sub Inizio
	dim sTitolo
	dim sCommento
	dim bCampiObbl

	sTitolo = "Percorso Formativo"
	sCommento = "Scegliere l'area tematica, quindi selezionare i questionari da associare al Percorso Formativo " &_
				"e deselezionare i questionari che si vogliono eliminare.<br>" &_
				"Premere Invia per confermare le scelte."
	bCampiObbl = false
%>	<center>
	<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
	<tr height="18">
		<td class="sfondomenu" width="67%" height="18"><span class="tbltext0"><b>&nbsp;<%=sTitolo%></b></span></td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
		<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">
<%			
			if bCampiObbl then
				Response.Write "(*) campi obbligatori"
			end if
%>
		</td>
	</tr>
	</table>
	<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
		<tr>
			<td align="left" class="sfondocomm">
				<%=sCommento%>
			<a href="Javascript:Show_Help('/Pgm/help/Formazione/PercForma/PFO_ModQuePercForma/')">
			<img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0" onmouseover="javascript:window.status='' ; return true"></a>
			</td>
		</tr>
		<tr height="2">
			<td class="sfondocomm" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>
	</center>
	<br>
<%
End sub
'----------------------------------------------------------------------------------------------
Sub ImpostaPag
	dim sQuestScelti
	dim iIdAreaTem
	dim sQuestDellaSez'contiene i corsi appartenenti gi� alla sezione scelta
	dim sIdSezPercForm
	dim sIdPercForm
		
	sQuestScelti=Request("UCorScel")
		'la finestra opener gli passa i questionari eventualmente gi� selezionati

	sQuestDellaSez=Request("UCorDellaSez")
	iIdAreaTem=Request("cmbAreaTem")
	sIdSezPercForm=Request("UIdSezPercForm")
	sIdPercForm=Request("UIdPercForm")
	
	'Response.Write "sQuestDellaSez=" & sQuestDellaSez & "<br>"
	'Response.Write "iIdAreaTem=" & iIdAreaTem & "<br>"	
	'Response.Write "sIdSezPercForm=" & sIdSezPercForm & "<br>"	
	'Response.Write "sIdPercForm=" & sIdPercForm & "<br>"	
	



	'Response.Write "sQuestScelti prima=" & sQuestScelti & "<br>"
	if sQuestScelti<>"" then'aggiungo virgola iniziale e finala se mancanti per la ricerca in stringa
		if (mid(sQuestScelti,1,1)<>",") then
			sQuestScelti="," & sQuestScelti
		end if
		if (mid(sQuestScelti,len(sQuestScelti),1)<>",") then
			sQuestScelti=sQuestScelti & ","
		end if
	end if
	'Response.Write "sQuestScelti dopo=" & sQuestScelti & "<br>"

	
	sSql="SELECT ID_AREA,TITOLOAREA " &_
		"FROM AREA " &_
		"ORDER BY TITOLOAREA"
	'Response.Write sSql & "<br>"

'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	set Rst=CC.execute (sSql)
	if not Rst.eof then'se ci sono aree tematiche
%>		<form name="frmAreaTem" id="frmAreaTem" method="post" action="PFO_ModQuePerForm.asp">	
		<table border="0">
			<tr>
				<td class="tbltext1Fad">Area tematica:&nbsp;
				</td>
				<td>
					<select id="cmbAreaTem" name="cmbAreaTem" class="textblackFad" onChange="JavaScript:Azione('<%=sQuestScelti%>','Cambio Area Tematica');">
					<option value></option>
<%					do while not Rst.eof
%>						<option value="<%=Rst("ID_AREA")%>" <%if iIdAreaTem=cstr(Rst("ID_AREA")) then Response.Write "selected" %>><%=Rst("TITOLOAREA")%></option>
<%						Rst.MoveNext
					loop			
%>					</select>
				</td>
			</tr>
		</table>
		<input type="hidden" name="UCorScel" id="UCorScel">
		<input type="hidden" name="UCorDellaSez" id="UCorDellaSez" value="<%=sQuestDellaSez%>">
		<input type="hidden" name="UIdSezPercForm" id="UIdSezPercForm" value="<%=sIdSezPercForm%>">
		<input type="hidden" name="UIdPercForm" id="UIdPercForm" value="<%=sIdPercForm%>">
		</form>
<%	else
%>		<br><br><br>
		<span class="tbltext3">
			Non vi sono Aree tematiche disponibili.
		</span>
<%	end if
	
	
	if iIdAreaTem<>"" then 'scegli i questionari
		'la not in serve perch� non devono essere visualizzati i corsi gi� appartenenti alle altre sezioni
		sSql="SELECT ID_QUESTIONARIO,TIT_QUESTIONARIO " &_
			"FROM QUESTIONARIO " &_
			"WHERE ID_AREA=" & iIdAreaTem &_
			" AND FL_COMPLETO='S'" &_
			" AND (FL_PUBBLICO=0 OR ID_PROJOWNER='" & mid(session("progetto"),2) & "')" &_

			" ORDER BY TIT_QUESTIONARIO"
		
		'Response.Write sSql & "<br>"

'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
		set Rst=CC.execute(sSql)
		if not Rst.eof then
%>			<form name="frmQuestionari" id="frmQuestionari">
			<table border="0" width="98%">
				<tr class="sfondoCommBFad">
					<td width="1%">&nbsp;</td>
					<td width="99%">Titolo</td>
				</tr>
<%			Do while not Rst.eof 
%>				<tr class="tblSfondoFad">
					<td width="1%">
						<input type="checkbox" value="<%=Rst.fields("ID_QUESTIONARIO")%>" name="chkCorsi" id="chkCorsi" <%if InStr(sQuestScelti,"," & Rst.fields("ID_QUESTIONARIO") & ",") then Response.Write ("checked")%>>
					</td>
					<td class="tblDettFad"><%=strHTMLEncode(Rst("TIT_QUESTIONARIO"))%>&nbsp;</td>
				</tr>
<%				Rst.MoveNext		
			loop
%>		</table>
		<br><br>
		
		</form>
<%		else'Non vi sono questionari disponibili. per l'area scelta
%>			<br><br><br>
			<span class="tbltext3">
				Non vi sono questionari disponibili per l'area selezionata.
			</span>
			<br><br><br><br><br><br><br><br>
<%		end if

	else 'se iIdAreaTem==""
%>		<br><br><br><br><br><br><br><br><br><br><br>	
<%	end if 'se iIdAreaTem<>""
%>	
	<table border="0" width="250">
		<tr>
			<td align="center">
				<a href="JavaScript:self.close();">
				<img src="<%=session("progetto")%>/images/annulla.gif" border="0" alt="Annulla le scelte">
				</a>
			</td>
			<td align="center">
				<a href="JavaScript:Azione('<%=sQuestScelti%>','Chiudi Finestra');">
				<img src="<%=session("progetto")%>/images/conferma.gif" border="0" alt="Conferma le scelte">
				</a>
			</td>
		</tr>
	</table>
<%

	Rst.close
	set Rst=nothing

end sub
'----------------------------------------------------------------------------------------------

'Response.Write session("progetto")
'Response.End

if session("progetto")<> "" then 
'-----  MAIN  --------------
%>

<!--#include virtual="/include/openconn.asp"-->
<!--#include virtual="/include/HTMLEncode.asp"-->
<html>
<head>
<title>Scelta Questionari Percorso Formativo</title>
<link REL="STYLESHEET" TYPE="text/css" HREF="<%=session("progetto")%>/fogliostile.css">
</head>
<body class="sfondocentro">
<center>
<%	
	call Inizio
	call ImpostaPag
%>
</center>
</body>
</html>

<!--#include virtual="/include/closeconn.asp"-->
<%else	%>

	<script>
		alert("Sessione scaduta")
		self.close()
	</script>
<%end if%>
