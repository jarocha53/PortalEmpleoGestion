<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%	Response.ExpiresAbsolute=Now()-1
	Response.AddHeader "pragm","no-cache"
	Response.AddHeader "cache-control","private"
	Response.CacheControl = "no-cache"
%>
<script language="Javascript">
</script>
<%

'-----------------------------------------------------------------------
Sub Inizio
	dim sFunzione
	dim sTitolo
	dim sCommento
	dim bCampiObbl
	
	sFunzione = "GESTIONE PERCORSI FORMATIVI"
	sTitolo = "PERCORSO FORMATIVO"
	sCommento = "Inserire le informazioni e premere Invia per confermare l'inserimeto.<br>" &_
				"E` possibile rendere disponibile il Percorso Formativo solo dopo averlo completato."
	bCampiObbl = true
%>
	<!--#include virtual="/include/SetTestata.asp"-->
<%

End Sub
'-----------------------------------------------------------------------
sub MessaggioErrore(Errore)
%>	<br><br>
	<span class="tbltext3">
<%	if Errore<>"0" then
		Response.Write "Cancellazione non effettuata.<br>"
		Response.Write Errore
	end if
%>	</span><br>
<%
end sub
'-----------------------------------------------------------------------
Sub ImpostaPag
	dim iIdGruppo
	dim sDescPercForm
	dim sSql,dtOra
	dim dDataDispo

	sDescPercForm=Request("txtDescPercForm")
	sDescPercForm=Ucase(Trim(sDescPercForm))
	sDescPercForm=Replace(sDescPercForm,"'","''")
	dDataDispo=Request.Form("txtDataDisp")
	iIdGruppo=Request("cmbGruppo")

	'Response.Write "iIdGruppo=" & iIdGruppo & "<br>"
	'Response.Write "sDescPercForm=" & sDescPercForm & "<br>"
	'Response.Write "dDataDispo=" & dDataDispo & "<br>"
	
	SqlDtDisp1=""
	SqlDtDisp2=""
	if (dDataDispo<>"")then
		SqlDtDisp1=",DT_DISPONIBILE"
		SqlDtDisp2="," & ConvDateToDBs(ConvStringToDate(dDataDispo))
	end if
	dtOra=Now()
	sSql="INSERT INTO PERCORSOFORMATIVO" &_
		" (IDGRUPPO,DESC_PERCFORMATIVO,DT_TMST" & SqlDtDisp1 & ")" &_
		" VALUES" &_
		" (" & iIdGruppo & ",'" & sDescPercForm & "'," & ConvDateToDB(dtOra) & SqlDtDisp2 &")"
	'Response.Write sSql & "<br>"
	CodErrore=Esegui("", "", "", "INS", sSql, "0", "")'fa Commit
	if CodErrore<>"0" then
		call MessaggioErrore(CodErrore)
		exit sub
	end if
End Sub
'------------------------------------------------------------------------
sub ControlloErrore
	if CodErrore="0" then
%>		<script>
			alert('Inserimento correttamente effettuato.')
			location.href='PFO_VisPercForma.asp'
		</script>
<%	else
%>		<br><br><br><br>
		<a href='PFO_VisPercForma.asp'  class="textred">
			<b>Elenco Percorsi Formativi</b>
		</a>
<%	end if
end Sub
'------------------------------------------------------------------------
%>
<!--#include virtual="/strutt_testa2.asp"-->
<!--#include virtual="/include/OpenConn.asp"-->
<!--#include virtual="/util/dbutil.asp"-->
<!--#include virtual="/include/ControlDateVB.asp"-->
<!--#include virtual="/include/SysFunction.asp"-->
<%	dim CodErrore
	call Inizio
	call ImpostaPag
	call ControlloErrore
%>
<!--#include virtual="/include/CloseConn.asp"-->
<!--#include virtual="/strutt_coda2.asp"-->
