<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->

<script LANGUAGE="JavaScript" src="/Include/Help.inc">
</script>
<script LANGUAGE="JavaScript">
function Azione(sCorsiScelti,evento)
{
	var n
	var posizione,PosVirgolaSucc
	var trova //codice da trovare
	var inElenco
	//� booleana ed indica se il check controllato � gi� in sCorsiScelti
	//AZIONI :1>E' in elenco e ora non risulta ceccato --> lo elimino da sCorsiScelti
	//		  2>Non � in elenco(sCorsiScelti) e ora risulta ceccato --> lo aggiungo a sCorsiScelti
	//Negli altri due casi la situazione di sCorsiScelti rimane invariata
	
	if (document.frmCorsi!=null)//se c'� un elenco di corsi viene creato il form
	{//alert('sono qui')
		n=document.frmCorsi.chkCorsi.length
		//alert('INGRESSO sCorsiScelti=' + sCorsiScelti)
	
		if (sCorsiScelti!='')
		{	//servono come delimitatori per la ricerca nella stinga
			//alert('ora aggiungo virgole se mancanti')
			if (sCorsiScelti.substring(0,1)!=',')
			{
				sCorsiScelti=',' + sCorsiScelti
			}
			if (sCorsiScelti.substr(sCorsiScelti.length-1)!=',')
			{
				sCorsiScelti=sCorsiScelti + ','
			}
		}
		
		//alert('sCorsiScelti dopo eventuali virgole=' + sCorsiScelti)
		
		//alert('n=' + n)
		if (n==null)
		{//� uno solo
				
				inElenco=false
				trova=',' + document.frmCorsi.chkCorsi.value + ','
				//alert('trova=' + trova)
				posizione=sCorsiScelti.search(trova)
				//alert('posizione=' + posizione)
				if (posizione!=(-1))
				{
					inElenco=true
				}
				//alert('inElenco=' + inElenco)
				if ((!document.frmCorsi.chkCorsi.checked) && inElenco )
				{//lo elimino da sCorsiScelti
					//alert('lo elimino')
					PosVirgolaSucc=sCorsiScelti.indexOf(',',posizione+1)
					//alert('PosVirgolaSucc=' + PosVirgolaSucc)
					
					sCorsiScelti=sCorsiScelti.substring(0,posizione) + sCorsiScelti.substr(PosVirgolaSucc)
					//alert('sCorsiScelti=' + sCorsiScelti)
				}
					
				if ((document.frmCorsi.chkCorsi.checked) && (!inElenco) )
				{//lo aggiungo a sCorsiScelti
					//alert('lo aggiungo')
					sCorsiScelti=sCorsiScelti + document.frmCorsi.chkCorsi.value + ","
					//alert('sCorsiScelti=' + sCorsiScelti)
				}
				
		}
		else
		{//+ di 1
			for (i=0;i<n;i++)
			{
				inElenco=false
				trova=',' + document.frmCorsi.chkCorsi[i].value + ','
				//alert('trova=' + trova)
				posizione=sCorsiScelti.search(trova)
				//alert('posizione=' + posizione)
				if (posizione!=(-1))
				{
					inElenco=true
				}
				//alert('inElenco=' + inElenco)
				
				if ((!document.frmCorsi.chkCorsi[i].checked) && inElenco )
				{//lo elimino da sCorsiScelti
					//alert('lo elimino')
					
					PosVirgolaSucc=sCorsiScelti.indexOf(',',posizione+1)
					//alert('PosVirgolaSucc=' + PosVirgolaSucc)
					sCorsiScelti=sCorsiScelti.substring(0,posizione) + sCorsiScelti.substr(PosVirgolaSucc)
					
					//alert('sCorsiScelti=' + sCorsiScelti)
				}
				
				if ((document.frmCorsi.chkCorsi[i].checked) && (!inElenco) )
				{//lo aggiungo a sCorsiScelti
					//alert('lo aggiungo')
					sCorsiScelti=sCorsiScelti + document.frmCorsi.chkCorsi[i].value + ","
					//alert('sCorsiScelti=' + sCorsiScelti)
				}
			}// fine for per controllo checkbox
			
		}// fine else se + di un checkbox
	
	
	}// se esiste frmCorsi
	
	if (sCorsiScelti.substring(0,1)==',')//se il primo carattere � virgola la elimina
	{
		sCorsiScelti=sCorsiScelti.substr(1)
	}
	
	if (sCorsiScelti.substr(sCorsiScelti.length-1)==',')//viene eliminata l'ultima virgola che rimane se si elimina dall'elenco sCorsiScelti l'ultimo codice
	{
		//alert('elimino virgola finele')
		//alert('sCorsiScelti=' + sCorsiScelti)
		sCorsiScelti=sCorsiScelti.substring(0,sCorsiScelti.length-1)		
	}
	
	//alert('USCITA sCorsiScelti=' + sCorsiScelti)
	
	if (evento=='Cambio Area Tematica')
	{
		document.frmAreaTem.UCorScel.value=sCorsiScelti
		document.frmAreaTem.submit()
	}
	if (evento=='Chiudi Finestra')
	{
		opener.document.frmPercForma.hCorsiScelti.value=sCorsiScelti
		//opener.document.frmPercForma.action=""
		//non specificando l'action va sottomessa di default la pagina opener
		//ci� permette di utilizzate questa pagina con pi� finestre opener che la chiamano
		opener.document.frmPercForma.submit();
		self.close();
	}
}
</script>

<%
Sub Inizio
	dim sTitolo
	dim sCommento
	dim bCampiObbl

	sTitolo = "Percorso Formativo"
	sCommento = "Scegliere l'area tematica, quindi selezionare i corsi da associare al Percorso Formativo, " &_
				"e deselezionare i corsi che si vogliono eliminare.<br>" &_
				"Premere Invia per confermare le scelte."
	bCampiObbl = false
%>	<center>
	<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
	<tr height="18">
		<td class="sfondomenu" width="67%" height="18"><span class="tbltext0"><b>&nbsp;<%=sTitolo%></b></span></td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
		<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">
<%			
			if bCampiObbl then
				Response.Write "(*) campi obbligatori"
			end if
%>
		</td>
	</tr>
	</table>
	<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
		<tr>
			<td align="left" class="sfondocomm">
				<%=sCommento%>
			<a href="Javascript:Show_Help('/Pgm/help/Formazione/PercForma/PFO_ModCorPercForm/')">
			<img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0" onmouseover="javascript:window.status='' ; return true"></a>
			</td>
		</tr>
		<tr height="2">
			<td class="sfondocomm" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>
	</center>
	<br>
<%
End sub

'----------------------------------------------------------------------------------------------

Sub ImpostaPag
	dim sCorsiScelti
	dim iIdAreaTem
	dim sIdGruppo
	dim sCorDellaSez'contiene i corsi appartenenti gi� alla sezione scelta
	dim sSqlCondCorEsi'stringa contenente la condizione per la visualizzazione
		'dei corsi gi� inseriti ma che sono appartenenti alla sezione in questione
	
	sCorsiScelti=Request("UCorScel")
	'la finestra opener gli passa i corsi eventualmente gi� selezionati che dovranno essere ceccate
	sIdGruppo=Request("UIdGruppo")
	
	sCorDellaSez=Request("UCorDellaSez")
	iIdAreaTem=Request("cmbAreaTem")
	
	'Response.Write "sIdGruppo=" & sIdGruppo & "<br>"
	'Response.Write "sCorDellaSez=" & sCorDellaSez & "<br>"
	'Response.Write "iIdAreaTem=" & iIdAreaTem & "<br>"	

'-------------------------------------------------------------------------------------	
	if sCorDellaSez<>"" and iIdAreaTem<>"" then
		sSqlCondCorEsi="or (ID_CORSO in (" & sCorDellaSez & ") and ID_AREATEMATICA=" & iIdAreaTem & ") "
					
	else
		sSqlCondCorEsi=""
	end if
'-------------------------------------------------------------------------------------

	'Response.Write "sCorsiScelti prima=" & sCorsiScelti & "<br>"
	if sCorsiScelti<>"" then'aggiungo virgola iniziale e finala se mancanti per la ricerca in stringa
		if (mid(sCorsiScelti,1,1)<>",") then
			sCorsiScelti="," & sCorsiScelti
		end if
		if (mid(sCorsiScelti,len(sCorsiScelti),1)<>",") then
			sCorsiScelti=sCorsiScelti & ","
		end if
	end if
	'Response.Write "sCorsiScelti dopo=" & sCorsiScelti & "<br>"
	
	sSql="SELECT ID_AREA,TITOLOAREA " &_
		"FROM AREA " &_
		"ORDER BY TITOLOAREA"
	'Response.Write sSql & "<br>"

'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	set Rst=CC.execute (sSql)
	if not Rst.eof then'se ci sono aree tematiche
%>		<form name="frmAreaTem" id="frmAreaTem" method="post" action="PFO_ModCorPerForm.asp">	
			<table border="0">
			<tr>
				<td class="tbltext1Fad">Area tematica:&nbsp;
				</td>
				<td>
					<select id="cmbAreaTem" name="cmbAreaTem" class="textblackFad" onChange="JavaScript:Azione('<%=sCorsiScelti%>','Cambio Area Tematica');">
					<option value></option>
<%			 		do while not Rst.eof
%>						<option value="<%=Rst("ID_AREA")%>" <%if iIdAreaTem=cstr(Rst("ID_AREA")) then Response.Write "selected" %>><%=Rst("TITOLOAREA")%></option>
<%						Rst.MoveNext
					loop			
%>					</select>
				</td>
			</tr>
			</table>
			<input type="hidden" name="UCorScel" id="UCorScel">
			<input type="hidden" name="UIdGruppo" id="UIdGruppo" value="<%=sIdGruppo%>">
			<input type="hidden" name="UCorDellaSez" id="UCorDellaSez" value="<%=sCorDellaSez%>">
		</form>
<%	else
%>		<br><br><br>
		<span class="tbltext3">
			Non vi sono Aree tematiche disponibili.
		</span>
<%	end if

	if iIdAreaTem<>"" then 'scegli i corsi

		sSql="SELECT ID_CORSO,TIT_CORSO,DESC_CORSO,FL_EROGAZIONE " &_
			"FROM CORSO " &_
			"WHERE ID_AREATEMATICA=" & iIdAreaTem &_
			" AND DT_DISPONIBILE<=" & ConvDateToDBs(Date()) &_
			" AND (FL_PUBBLICO=0 OR ID_PROJOWNER='" & mid(session("progetto"),2) & "')" &_
			" AND ID_CORSO NOT IN " &_
				"(SELECT ID_CORSO FROM CATCORSI" &_
				" WHERE IDGRUPPO=" & sIdGruppo & " AND FL_OBBL=0) "  & sSqlCondCorEsi &_
			" ORDER BY TIT_CORSO"
			'il controllo in CATCORSI verifica che non sia un corso gi� inserito come facoltativo
		'Response.Write sSql

'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
		set Rst=CC.execute(sSql)
		if not Rst.eof then'se trova corsi
%>			<form name="frmCorsi" id="frmCorsi">
			<table border="0" width="98%">
				<tr class="sfondoCommBFad">
					<td width="1%">&nbsp;</td>
					<td width="49">Titolo</td>
					<td width="49%">Descrizione</td>
					<td width="1%">Modalit� di&nbsp;erogazione</b></td>
				</tr>
<%				Do while not Rst.eof 
%>					<tr class="tblSfondoFad">
						<td align="center">
							<input type="checkbox" value="<%=Rst.fields("ID_CORSO")%>" name="chkCorsi" id="chkCorsi" <%if InStr(sCorsiScelti,"," & Rst.fields("ID_CORSO") & ",") then Response.Write ("checked")%>>
						</td>
						<td class="tblDettFad"><%=strHTMLEncode(Rst("TIT_CORSO"))%>&nbsp;</td>
						<td class="tblDettFad"><%=strHTMLEncode(Rst("DESC_CORSO"))%>&nbsp;</td>
						<td class="tblDettFad" align="left">
<%							select case Rst("FL_EROGAZIONE")
								case "0"
									Response.Write "offline"
								case "1"
									Response.Write "online"
							end select
%>						</td>
					</tr>
<%					Rst.MoveNext		
				loop
%>			</table>
			<br><br>
			</form>
<%		else'Non vi sono corsi disponibili. per l'area scelta
%>			<br><br><br>
			<span class="tbltext3">
				Non vi sono corsi disponibili per l'area selezionata.
			</span>
			<br><br><br><br><br><br><br><br>
<%		end if
	
	else 'se iIdAreaTem==""
%>		<br><br><br><br><br><br><br><br><br><br><br>	
<%	end if 'se iIdAreaTem<>""
%>		
	<table border="0" width="250">
		<tr>
			<td align="center">
				<a href="JavaScript:self.close();">
				<img src="<%=session("progetto")%>/images/annulla.gif" border="0" alt="Annulla le scelte">
				</a>
			</td>
			<td align="center">
				<a href="JavaScript:Azione('<%=sCorsiScelti%>','Chiudi Finestra');">
				<img src="<%=session("progetto")%>/images/conferma.gif" border="0" alt="Conferma le scelte">
				</a>
			</td>
		</tr>
	</table>
<%
	Rst.close
	set Rst=nothing

end sub
'----------------------------------------------------------------------------------------------

'Response.Write session("progetto")
'Response.End

if session("progetto")<> "" then 
'-----  MAIN  --------------
%>

<!--#include virtual="/include/openconn.asp"-->
<!--#include virtual="/include/HTMLEncode.asp"-->
<!--#include virtual="/util/dbutil.asp"-->
<html>
<head>
<title>Corsi Percorso Formativo</title>
<link REL="STYLESHEET" TYPE="text/css" HREF="<%=session("progetto")%>/fogliostile.css">
</head>
<body class="sfondocentro">
<center>
<%	
	call Inizio
	call ImpostaPag
%>
</center>
</body>
</html>

<!--#include virtual="/include/closeconn.asp"-->
<%else	%>

	<script>
		alert("Sessione scaduta")
		self.close()
	</script>
<%end if%>
