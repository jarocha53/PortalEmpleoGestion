<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<script LANGUAGE="JavaScript">
function invia(sidcorso)
{
   document.frmSchedaCorso.titoli.value = sidcorso;
   document.frmSchedaCorso.hpag.value = 2
   document.frmSchedaCorso.submit()
}  
function Carica_Pagina(nPagJS)
{
	document.frmPagina.nPagina.value = nPagJS;
	document.frmPagina.submit();
}
</script>
<%
'-------------------------------------------------------------------------------
Sub Inizio
	dim sTitolo
	dim sCommento
	dim sHlp
	sTitolo = ""
	sCommento = "Il catalogo contiene l'elenco di tutti i corsi a te disponibili. " &_
				"L'elenco � suddiviso tra corsi gi� previsti dal percorso a te assegnato e " &_
				"corsi a te disponibili.Fai click sul titolo del corso per visualizzarne " &_
				"una scheda descrittiva."
	sHlp = "/Pgm/help/Formazione/Catalogo/CAT_VisCatalogo/"	
	call TestataFAD(sTitolo, sCommento, false, sHlp)
End sub
'-------------------------------------------------------------------------------
Function CercaPF()
dim sSQL, rsPF 

	sSQL =	" SELECT id_percformativo FROM PERCORSOFORMATIVO" &_
			" WHERE idgruppo = " & Session("idgruppo") & " AND dt_disponibile <= SYSDATE"
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	set rsPF = cc.execute(sSQL)
	'Response.Write sSQL
	If rsPF.eof Then
		CercaPF = false
	Else
		CercaPF = true
	End If
	rsPF.close
	set rsPF = nothing

End Function
'-------------------------------------------------------------------------------
Sub Intestazione(sDescr)
%>
<table border="0" width="500"> 
	<tr>
		<td colspan="4" class="tblText3"><%=strHTMLEncode(sDescr)%></td>
	</tr>
	<tr class="sfondoCommBFad">
		<td>Titolo</td>
		<td>Scheda</td>
		<td>Obbligatorio</td>
		<td>Questionario</td>
	</tr>
<%
End Sub
'-------------------------------------------------------------------------------
Sub StampaTabella()
	dim nPrecArea

	Record=0
	nTamPagina=5

	nPag = Request.Form("nPagina")
	If not(nPag > "") Then
		nPag = 1
	End If
	nPag = clng(nPag)
	nActPagina = nPag

	Sql = " SELECT CA.ID_CORSO, CA.FL_OBBL, CO.TIT_CORSO, CO.ID_AREATEMATICA, P.ID_QUESTIONARIO, A.DESCRIZIONEAREA"
	Sql = Sql &  " FROM CATCORSI CA, CORSO CO, PROVEVALUTAZIONE P, AREA A"
	Sql = Sql &  " WHERE CA.IDGRUPPO=" & Session("idgruppo") & " AND"
	Sql = Sql &  " dt_disponibile <= SYSDATE AND"
	Sql = Sql &  "   CA.ID_CORSO=CO.ID_CORSO AND CO.ID_AREATEMATICA=A.ID_AREA AND"
	If not CercaPF() Then	
		Sql = Sql &  "   CA.FL_OBBL = 0 AND"
	End If
	Sql = Sql &  "   CO.ID_CORSO=P.ID_CORSO(+) ORDER BY  A.DESCRIZIONEAREA"

	nPrecArea = -1

	Set RstCorso = Server.CreateObject("ADODB.Recordset")
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
	RstCorso.Open Sql, CC, 3

	nTotPagina = RstCorso.PageCount		
	If nActPagina < 1 Then
		nActPagina = 1
	End If
	If nActPagina > nTotPagina Then
		nActPagina = nTotPagina
	End If

	nTotRecord=0

    If not RstCorso.eof Then
	
		RstCorso.PageSize = nTamPagina
		RstCorso.CacheSize = nTamPagina	
		RstCorso.AbsolutePage=nActPagina
		bArea = true
		Do While not RstCorso.eof 'And nTotRecord < nTamPagina
			If nPrecArea <> clng(rstCorso("ID_AREATEMATICA")) Then
				If nPrecArea <> -1 Then Response.Write "</table><br>"
				nPrecArea = clng(rstCorso("ID_AREATEMATICA"))
				call Intestazione(rstCorso("DESCRIZIONEAREA"))
			End If
%>
			<tr class="tblSfondoFad" width="97%">
				<td class="tblDettFad">
					<%=strHTMLEncode(RstCorso.fields("TIT_CORSO"))%>
				</td>
				<td align="center" width="1%">
					<a href="javascript:invia('<%=RstCorso.fields("ID_CORSO")%>')">
						<img src="<%=Session("progetto")%>/images/formazione/modulo.gif" border="0" WIDTH="24" HEIGHT="26">	
					</a>
				</td>
				<td align="center" width="1%">
<%					If RstCorso.fields("FL_OBBL")="1" Then %>	
						<img src="<%=session("progetto")%>/images/Okvr.gif">			
<%					Else %>
						&nbsp;
<%					End If %>
				</td>
				<td align="center" width="1%">
<%					If not IsNull(RstCorso.fields("ID_QUESTIONARIO")) Then %>
						<img border="0" src="<%=Session("progetto")%>/images/Okvr.gif">
<%					Else %>
						&nbsp;
<%					End If %>	
				</td>
			</tr>
<%			
			nTotRecord=nTotRecord + 1
			RstCorso.MoveNext
		Loop
%>		
		</table><br>
<%	Else %>
		<table border="0" width="500">
			<tr>
				<td align="center" class="tbltext3">
					Non ci sono corsi a catalogo
				</td>
			</tr>
		</table>
<%
	End If
	RstCorso.close	
	set RstCorso = nothing
%>
<table border="0" width="500" align="center">
	<tr>
<%
	If nActPagina > 1 Then
%>
		<td align="right" width="480">
			<a HREF="javascript:Carica_Pagina(<%=nActPagina-1%>)">
				<img border="0" src="<%=session("progetto")%>/images/precedente.gif" alt="Pagina Precedente">
			</a>
		</td>
<%
	End If
	If nActPagina < nTotPagina Then
%>
		<td align="right">
			<a HREF="javascript:Carica_Pagina(<%=nActPagina+1%>)">
				<img border="0" src="<%=session("progetto")%>/images/successivo.gif" alt="Pagina Successiva">
			</a>
		</td>
<%	End If %>
	</tr>
</table>
<form name="frmPagina" method="post" action="CAT_VisCatalogo.asp">
	<input type="hidden" name="nPagina" id="nPagina" value="<%=nPag%>">
</form>
<form name="frmTipoCorso" id="frmTipoCorso" action="/pgm/formazione/catalogo/corsi/cor_visschedacorso.asp" method="post">
	<input type="hidden" name="titoli" id="titoli">
	<input type="hidden" name="hpag" id="hpag">
</form>
<form name="frmSchedaCorso" id="frmSchedaCorso" action="/pgm/formazione/catalogo/corsi/cor_visschedacorso.asp" method="post">
	<input type="hidden" name="titoli" id="titoli">
	<input type="hidden" name="hpag" id="hpag">
</form>
<%
End Sub
'-------------------------------------------------------------------------------
Sub ImpostaPag()
	dim Sql
	dim iIdArea,sTitArea
	dim sDescGruppo
	
	Sql = " SELECT ID_AREA,TITOLOAREA"
	Sql = Sql & " FROM AREA"
	Sql = Sql & " ORDER BY TITOLOAREA"
	
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
	set RstArea=CC.Execute(Sql)

	If RstArea.eof Then
%>
	
		<table border="0" width="500">
			<tr>
				<td class="tbltext3">
					Non vi sono Aree Tematiche disponibili.
				</td>
			</tr>
		</table>
<%		exit sub
	End If
		
	do while not RstArea.eof
		iIdArea=RstArea.fields("ID_AREA")
		sTitArea=RstArea.fields("TITOLOAREA")
		call StampaTabella(iIdArea,sTitArea,iIdGruppo)
		RstArea.MoveNext
	loop
	
	set RstCorso=nothing
	RstArea.close
	set RstArea=nothing	
	Response.Write bArea
	if not bArea then%>
		<table border="0" width="500">
			<tr>
				<td align="center" class="tbltext3">
					Non ci sono corsi a catalogo
				</td>
			</tr>
		</table>
	<%end if
	
%>	<form name="frmSchedaCorso" id="frmSchedaCorso" action="/pgm/formazione/catalogo/corsi/cor_visschedacorso.asp" method="post">
		<input type="hidden" name="titoli" id="titoli">
		<input type="hidden" name="hpag" id="hpag">
	</form>
<%
end sub

'-------------------------------------------------------------------------------------
'M A I N
%>
<!--#include virtual="/strutt_testa2.asp"-->
<!--#include virtual="/include/openconn.asp"-->
<!--#include virtual="/include/HTMLEncode.asp"-->
<!--#include virtual="/Include/settestatafad.asp"-->
<center>
<%
	dim bArea
	
	Inizio()
	'ImpostaPag()
	StampaTabella()
	bArea = false
%>
</center>
<!--#include virtual="/include/closeconn.asp"-->
<!--#include virtual="/strutt_coda2.asp"-->
