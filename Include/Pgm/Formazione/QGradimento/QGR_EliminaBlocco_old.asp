<%@LANGUAGE = JScript %>
<!--#INCLUDE file = "include/openConnJS.asp"-->
<%

function cancellaStruttura(rt,liv,idquest,IdArea) {
		
	// Funzione ricorsiva di lettura
	sql =  " SELECT ID_INFOQUEST, ID_AREAIQ, ID_BLOCCO, TIPO_ELEMENTO, ID_ELEMENTO, ID_ELEM_ORIGINE, DT_TMST " 
	sql += " FROM STRUTTURA_QUEST "
	sql += " WHERE ID_AREAIQ = " + IdArea + " AND ID_ELEM_ORIGINE = " + rt + " AND ID_INFOQUEST = " + idquest 
	sql += " ORDER BY ID_BLOCCO, ID_ELEMENTO";
	try { 
		i_ris[liv] = CC.Execute(sql);
  
		while (!i_ris[liv].EOF){
			sql =  " DELETE FROM STRUTTURA_QUEST "
			sql += " WHERE ID_AREAIQ = " + IdArea + " AND ID_ELEM_ORIGINE = " + rt + " AND ID_INFOQUEST = " + idquest 
			RRdom = CC.Execute(sql);
//			Response.Write ("<BR>" + sql);

			cancellaStruttura(parseInt(i_ris[liv]("ID_ELEMENTO")), liv + 1,idquest,IdArea);
			i_ris[liv].MoveNext();
		}
			sql =  " DELETE FROM STRUTTURA_QUEST "
			sql += " WHERE ID_AREAIQ = " + IdArea + " AND ID_ELEMENTO = " + rt + " AND ID_INFOQUEST = " + idquest 
//			Response.Write ("<BR>" + sql);
			RRdom = CC.Execute(sql);
	} catch (e) {
		Err = "Si � verificato un errore durante la procedura"
	}
}

function eliminaBloccoIntero(idquest,IdArea,idblocco){

	sql =  " DELETE FROM STRUTTURA_QUEST "
	sql += " WHERE ID_AREAIQ = " + IdArea + " AND ID_BLOCCO = " + idblocco + " AND ID_INFOQUEST = " + idquest 
//	Response.Write ("<BR>" + sql);
	try { 
		RRdom = CC.Execute(sql);
	} catch (e) {
		Err = "Si � verificato un errore durante la procedura"
	}
}

function verificaCancellazioneIntBlocco(idquest,IdArea,rt){
	sql =  " SELECT ID_ELEM_ORIGINE " 
	sql += " FROM STRUTTURA_QUEST "
	sql += " WHERE ID_AREAIQ = " + IdArea + " AND ID_ELEMENTO = " + rt + " AND ID_INFOQUEST = " + idquest; 
	try { 
		//response.write (sql);
		RRdom = CC.Execute(sql);
		//response.write (parseInt(RRdom("ID_ELEM_ORIGINE"),10));
		if ( parseInt(RRdom("ID_ELEM_ORIGINE"),10) > 0 )
			return false;
		else 		
			return true;
	} catch (e) {
		Err = "Si � verificato un errore durante la procedura"
		return false;
	}

}

function cancellaMessaggi(rt,liv,idquest,IdArea) {
		
	// Funzione ricorsiva di lettura
	sql =  " SELECT ID_INFOQUEST, ID_AREAIQ, ID_BLOCCO, TIPO_ELEMENTO, ID_ELEMENTO, ID_ELEM_ORIGINE, DT_TMST " 
	sql += " FROM STRUTTURA_QUEST "
	sql += " WHERE ID_AREAIQ = " + IdArea + " AND ID_ELEM_ORIGINE = " + rt + " AND ID_INFOQUEST = " + idquest 
	sql += " ORDER BY ID_BLOCCO, ID_ELEMENTO";	
	i_ris[liv] = CC.Execute(sql);
//	response.write (String(i_ris[liv]("TIPO_ELEMENTO")));
	try {	
		i_ris[liv] = CC.Execute(sql);
  
		while (!i_ris[liv].EOF){
			//LETTURA DOMANDA
			if (String(i_ris[liv]("TIPO_ELEMENTO"))=="D"){

				sqlDom = "DELETE FROM IQ_DOMANDA  "
				sqlDom += " WHERE ID_DOMANDAIQ =" + parseInt(i_ris[liv]("ID_ELEMENTO"))
				Response.Write ("sqlDom = " + sqlDom);
				//response.end
				RRdom = CC.Execute(sqlDom);
			}
			//LETTURA RISPOSTA
			if (String(i_ris[liv]("TIPO_ELEMENTO"))=="R"){

				sqlRisp = "DELETE FROM IQ_RISPOSTA "
				sqlRisp += " WHERE ID_RISPOSTAIQ = " + parseInt(i_ris[liv]("ID_ELEMENTO"))
				Response.Write ("sqlRisp = " + sqlRisp);
				//response.end;
				RRrisp = CC.Execute(sqlRisp);
			}		
			cancellaMessaggi(parseInt(i_ris[liv]("ID_ELEMENTO")), liv + 1,idquest,IdArea);
			i_ris[liv].MoveNext();
		}

		sqlDom = "DELETE FROM IQ_DOMANDA  "
		sqlDom += " WHERE ID_DOMANDAIQ =" + rt;
		//Response.Write ("sqlDom = " + sqlDom);
		RRdom = CC.Execute(sqlDom);

	} catch (e) {
//		Err = "Si � verificato un errore durante la procedura : " + e.description;
		Err = "Si � verificato un errore durante la procedura";
	}

}

%>
<!--------------------------------------------------------------------------------------------------------------------------------------------------------->
<!--		MAIN			-->
<script language="VBScript" runat="server"> 
	Server.Execute ("/strutt_testa1.asp") 
</script>
<%
var i_ris = new Array();
var IdQuest = Request("IdQuest")
var IdArea	= Request("IdArea")
var Iddom	= Request("Iddom")
var IdBlocco = Request("IdBlocco")
var IdElem	= Request("IdElem")
var Err ="0";
var sConn 
sConn = CC;

if (verificaCancellazioneIntBlocco(IdQuest,IdArea,Iddom)){
	cancellaMessaggi(Iddom,0,IdQuest,IdArea);
	CC.close();
	CC.open (sConn);
CC.BeginTrans();
	eliminaBloccoIntero(IdQuest,IdArea,IdBlocco);
} else {
	cancellaMessaggi(Iddom,0,IdQuest,IdArea);
	CC.close();
	CC.open (sConn);
CC.BeginTrans();
	cancellaStruttura(Iddom,0,IdQuest,IdArea);
}


if (Err == "0") {
	CC.CommitTrans();
	CC.Close()
%>
		<form name="frmDomanda" method="post" action="QGR_Sessioni.asp">
			<input type="hidden" name="idq" value="<%=IdQuest%>">
			<input type="hidden" name="ida" value="<%=IdArea%>">
		</form>
		<script language="JavaScript">
			document.frmDomanda.submit()
		</script>

<%} else {
	CC.RollBackTrans();
	CC.Close();
%>
	<br><br>
	<table border="0" cellspacing="0" cellpadding="0" width="730px">
		<tr>
			<td align="center" class="tbltext3">
				<span class="size">
				<b>Errore nella cancellazione<br>
					<%=Err%> </b>
				</span>
			<td>
		</tr>
	</table>
	<br><br>
	<table width="730px" cellspacing="0" cellpadding="0" border="0">
		<tr>
			<td align="center">
				<b class="textblack">
				<a class="textRed" href="javascript:history.back()" onmouseover="window.status =' '; return true">Pagina Precedente</a>
				</b>
			</td>
		</tr>
	</table>
<%}%>
<!--	FINE BLOCCO MAIN	-->