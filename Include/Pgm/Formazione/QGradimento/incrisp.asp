<!--#include virtual="/strutt_testa2.asp"-->
<%'<!--#include virtual="/pgm/formazione/menu/menu.asp"-->%>
<!--#include virtual ="/util/portallib.asp"-->

<script LANGUAGE="JavaScript" type="text/javascript">
<!--#include virtual ="/Include/Help.inc"-->
<!--#include virtual ="/Include/ControlString.inc"-->

function chk() {
    var nd		//num. complessivo domande +1
    var conta	//verifica se c'� almeno una risp. per dom.
    var b
    var a		//num. risp. per domanda.
    var i
	    
    nd = document.form1.NDOM.value 
				
	//ciclo sulle domande.
	for(i=1;i<nd;i++){
		a = eval("document.form1.nRispXDom" + i + ".value")
		if (a != 0) {
			sel = eval("document.form1.tiposel" + i + ".value")
			if (sel != "") {
        
				if (a == 1) {
					b = eval("document.form1.tipocampo" + i + ".value") 
				}
				else {
        			b = eval("document.form1.tipocampo" + i + "[1].value")  
				}
					
				conta = "0"
            
				//ciclo sulle risposte per domanda
				for(z=0;z<a;z++) {
			    
					if (b == "radiobutton") {
					
						tipo = eval("document.form1.tipo" + i + ".value")
						if (tipo == "V"){
							min = eval("document.form1.min" + i + ".value")
							max = eval("document.form1.max" + i + ".value")
							range = (max-min)
							for(x=0;x<=range;x++) {
								RbV = eval("document.form1.DOM" + i + "[x]")
								if (RbV.checked == true) {
									conta = "1"
									break;
								}
							}
						}
						else {
							Rb = eval("document.form1.DOM" + i + "[z]")
							if (Rb.checked == true) {
								conta = "1"
								break;
							}
						}
						
					}
					if (b == "checkbox") {
						y = z+1
						Cb = eval("document.form1.chkMulti" + i + "_" + y)
						if (Cb.checked == true) {
							conta = "1"
							break;
						}
				   }
				    if (b == "textarea") {
						y = z+1
						Ta = eval("document.form1.Altro" + i + "_" + y + ".value")
						if (TRIM(Ta) != "") {
							conta = "1"
							break;
						}
				   }
				   if (b == "indefinito") {
						conta = "1"
						break;
				   }
					   
       			}
       			if (conta == "0") {
       				alert ('Selezionare almeno una risposta per ogni domanda!')
					return false;
       			}
			}
		}
    }
}

function Show_Help(W2Show) {
	f=W2Show;
	w=(screen.width-(screen.width/2))/2;	
	h=(screen.height-(screen.height/2))/2;
	fin=window.open(f,"pippo","toolbar=0, location=0,directories=0,status=0,menubar=0,scrollbars=yes,resizable=0,copyhistory=0,width=600,height=480,screenX=w,screenY=h");	
}
		
</script>

<!--	-------BLOCCO ASP-------	-->
<%
Sub Inizio()%>	
	<br>
	<table width="520px" border="0" cellspacing="0" cellpadding="0" height="70" align="center">
		<tr>
			<td width="500" background="<%=session("progetto")%>/images/titoli/fad2b.gif" valign="bottom" align="right">
				<table border="0" background width="500" height="23" cellspacing="0" cellpadding="0">
					<tr>
						<td width="100%" valign="top" align="right">
							<b CLASS="tbltext1a">Questionario</b>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table> 
	<br>
<%
End Sub

'---------------------------------------------------------------------------------------------------------------------------------------------------------
Sub Controllo()
	
	set RsControllo=Server.CreateObject("ADODB.Recordset")
	sql=	"SELECT ID_RISULTATO, NUM_ACCESSI, DT_TMST" &_
			" FROM IQ_RISULTATO" &_
			" WHERE ID_INFOQUEST = " & IDQue &_
			" AND IDUTENTE = " & Session("idutente")
	RsControllo.open sql, CC, 3
	If not RsControllo.EOF then
		DtTmst = RsControllo.Fields("DT_TMST")  
		nIdRisultato =RsControllo.Fields("ID_RISULTATO")
		nAccessi = RsControllo.Fields("NUM_ACCESSI")
		
		nAccessi = cint(nAccessi) + 1
		
		sqlUpdate=	"UPDATE IQ_RISULTATO" &_
					" SET NUM_ACCESSI = " & nAccessi & ", DT_TMST = SYSDATE" &_
					" where ID_RISULTATO=" & nIdRisultato
		sErrore = Esegui(nIdRisultato,"IQ_RISULTATO",Session("persona"),"MOD",sqlUpdate,0,DtTmst)
	Else
		nAccessi = 1
		sql="INSERT INTO IQ_RISULTATO" &_
			" (ID_INFOQUEST,IDUTENTE,NUM_ACCESSI,ESEGUITO,DT_TMST)" &_
			" VALUES" &_
			" ("& IDQue &","& session("idutente") &","& nAccessi & ",'N',SYSDATE)"
		sErrore = EseguiNoC(sql,CC)
	End if
	RsControllo.CLOSE
End sub

'---------------------------------------------------------------------------------------------------------------------------------------------------------
Sub ImpostaPag()

	'IMPOSTA QUESTIONARIO
	set RRQ=server.CreateObject("ADODB.Recordset")
	sql="SELECT DESC_QUEST, INTEST_QUEST from INFO_QUEST where ID_INFOQUEST = " & IDQue

	RRQ.open sql, CC, 3
	If RRQ.EOF then %>
		<table border="0" cellspacing="2" cellpadding="1" width="500px">
			<tr>
				<td align="center">
					<span class="tblText1Fad"><span class="size">
						<b>Questionario non presente</b>
					</span></span>
				</td>
			</tr>
		</table>
<%	
	Else
		DESCR  = RRQ.Fields("DESC_QUEST")
		INTES  = RRQ.Fields("INTEST_QUEST") %>
		<table border="0" class="SfondoCommFad" cellspacing="2" cellpadding="1" width="500px">
			<tr>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td align="left">
					<b><%=INTES%></b>
					<br><br>
					I dati che ci stai gentilmente fornendo con la compilazione del questionario<br> 
					saranno memorizzati in forma anonima e verranno utilizzati esclusivamente a <br>
					fini statistici per la rilevazione dei dati di monitoraggio del progetto.<br>
					<a href="javascript:Show_Help('/Pgm/help/formazione/QGradimento/QGR_Questionario')">
						<img src="<%=Session("Progetto")%>/images/help.gif" border="0" align="right">
					</a>
				</td>
			</tr>
		</table>
		<br>
		
		<form action="QGR_InvQuestionario.asp" method="POST" id="form1" onsubmit="return chk()" name="form1">
			<%'campi hidden con id e descrizione questionario%>
			<input type="hidden" name="IDQue" value="<%=IDQue%>">
			<input type="hidden" name="DESCR" value="<%=DESCR%>">
<%
	End if
	RRQ.CLOSE
	
	'___IMPOSTA AREA___
	set RRA=server.CreateObject("ADODB.Recordset")
	sql0="SELECT distinct ID_AREAIQ, DESC_AREAIQ from IQ_AREA where ID_INFOIQ = " & IDQue
	RRA.open sql0, CC, 3
	IF RRA.EOF then %>
		<table border="0" cellspacing="2" cellpadding="1" width="500px">
			<tr>
				<td align="center">
					<span class="tblText1Fad"><span class="size">
						<b>Non sono presenti Aree</b>
					</span></span>
				</td>
			</tr>
		</table>
<%	
	ELSE
		RRA.MoveFirst
		naree = 1
		nd = 1		'--nd riporta il totale domande + 1.
		ndom = 1	'--ndom riporta il totale domande + 1.
		nrisp = 1	'--nrisp riporta il totale risposte + 1.
		x=1			'--contatore per il nome di checkbox e textarea; 
					   'incrementa ad ogni risposta e si riavvia ad ogni domanda.
		
		DO While not RRA.Eof
			IDAREA  = RRA.Fields("ID_AREAIQ")
			DESCAREA  = RRA.Fields("DESC_AREAIQ")%>
			<br>
			<table border="0" cellspacing="0" cellpadding="0" width="500px">
				<tr>
					<td align="left" class="SfondoCommFad" width="100%" height="23"><b>&nbsp;<%=DESCAREA%></b></td>
				</tr>
			</table>
<%
			'___IMPOSTA LE DOMANDE DELL'AREA IN QUESTIONE___
			set RRD=server.CreateObject("ADODB.Recordset")
			sql1 =	"SELECT DISTINCT ID_DOMANDAIQ, TESTO_DOMANDA, TIPO_DOMANDA, TIPO_SEL" &_
					" FROM IQ_DOMANDA where ID_INFOQUEST = " & IDQue &_
					" and ID_AREAIQ = " & IDAREA & " order by ID_DOMANDAIQ"
			RRD.open sql1, CC, 3
			If RRD.EOF then %>
				<table border="0" cellspacing="2" cellpadding="1" width="500px">
					<tr>
						<td align="center">
							<span class="tblText1Fad"><span class="size">
								<b>Non sono presenti domande</b>
							</span></span>
						</td>
					</tr>
				</table>
<%	
			Else
				RRD.MoveFirst
				
				Do While not RRD.Eof
					IDDOM  = RRD.Fields("ID_DOMANDAIQ")
					TESTODOM = RRD.Fields("TESTO_DOMANDA")
					TIPODOM = RRD.Fields("TIPO_DOMANDA")
					TIPOSEL = RRD.Fields("TIPO_SEL")
%>
					<table border="0" cellspacing="0" cellpadding="0" width="500px">
						<tr>
							<td align="left" class="tblText1Fad" width="100%">
								<b><%=ndom%>)&nbsp;<%=TESTODOM%></b>
							</td>
						</tr>
					</table>
					<input type="hidden" name="tiposel<%=ndom%>" value="<%=TIPOSEL%>">

<%
					'___IMPOSTA LE RISPOSTE ALLA DOMANDA IN QUESTIONE___
					set RRR=server.CreateObject("ADODB.Recordset")
					sql2=	"SELECT distinct ID_RISPOSTAIQ, TIPO, TXT_RISPOSTA, TESTO_MIN, TESTO_MAX, IQ_MIN, IQ_MAX, FL_TIPRISP" &_
							" from IQ_RISPOSTA where ID_DOMANDAIQ = " & IDDOM &_
							" order by ID_RISPOSTAIQ"
					RRR.open sql2, CC, 3
					if RRR.EOF then %>
						<table border="0" cellspacing="2" cellpadding="1" width="500px">
							<tr>
								<td align="center">
									<span class="tblText1Fad"><span class="size">
										<b>Non sono presenti risposte</b>
									</span></span>
								</td>
							</tr>
						</table>
						<%'campo hidden con numero di risposte per domanda %>
						<input type="hidden" name="nRispXDom<%=ndom%>" value="<%=cint(0)%>">												

<%	
					else
						RRR.MoveFirst
						cVerify=1 
						'campo hidden con numero di risposte per domanda %>
						<input type="hidden" name="nRispXDom<%=ndom%>" value="<%=RRR.RecordCount%>">												
<%										
						do While not RRR.Eof
							IDRIS = RRR.Fields("ID_RISPOSTAIQ")
							TIPOR = RRR.Fields("TIPO")
							FLTIPORISP = RRR.Fields("FL_TIPRISP")
							
							Select case TIPOR
								'RISPOSTA DI TIPO TESTO
								case "T"
									sTestoRisp = RRR.Fields("TXT_RISPOSTA")
									
									IF TIPODOM = "A" THEN
									'Non serve controllare il Tipo Selezione. %>

											<textarea name="Altro<%=ndom%>_<%=x%>" rows="2" cols="40" class="textblacka" value><%=lcase(sTestoRisp)%></textarea>

										<%'campo hidden con id_domanda e id_risposta %>
										<input type="hidden" name="AltroBis<%=ndom%>_<%=x%>" value="<%=IDDOM%>_<%=IDRIS%>">
										<%'campo hidden con il tipo di elemento visualizzato %>
										<input type="hidden" name="tipocampo<%=ndom%>" value="textarea">
<%										x=x+1
									END IF
									
									'Tipo Selezione = Escludente (E)
									IF TIPOSEL = "E" THEN 
										if TIPODOM = "C" then %>	
												<input type="radio" name="DOM<%=ndom%>" value="<%=IDDOM%>_<%=IDRIS%>"><%=lcase(sTestoRisp)%>

											<%'campo hidden con il tipo di elemento visualizzato%>
											<input type="hidden" name="tipocampo<%=ndom%>" value="radiobutton">
<%										end if
										if TIPODOM = "S" then %>
												<input type="radio" name="DOM<%=ndom%>" value="<%=IDDOM%>_<%=IDRIS%>"><%=lcase(sTestoRisp)%>
	<%													if FLTIPORISP = "A" then %>
															<textarea name="Altro<%=IDRIS%>" rows="1" cols="40" class="textblacka" value></textarea>
	<%													end if %>												
											<%'campo hidden con il tipo di elemento visualizzato%>
											<input type="hidden" name="tipocampo<%=ndom%>" value="radiobutton">
<%										end If 
									END IF
									
									'Tipo Selezione = Multipla (M)
									IF TIPOSEL = "M" THEN 
										if TIPODOM = "C" then %>

												<input type="checkbox" name="chkMulti<%=ndom%>_<%=x%>" value="<%=IDDOM%>_<%=IDRIS%>"><%=lcase(sTestoRisp)%>

											<%'campo hidden con il tipo di elemento visualizzato%>
											<input type="hidden" name="tipocampo<%=ndom%>" value="checkbox">
<%											x=x+1
										end if
										if TIPODOM = "S" then%>

												<input type="checkbox" name="chkMulti<%=ndom%>_<%=x%>" value="<%=IDDOM%>_<%=IDRIS%>"><%=lcase(sTestoRisp)%>
<%													if FLTIPORISP = "A" then %>
														<textarea name="Altro<%=idris%>" rows="1" cols="40" class="textblacka" value></textarea>
<%													end if %>	
											<%'campo hidden con il tipo di elemento visualizzato%>
											<input type="hidden" name="tipocampo<%=ndom%>" value="checkbox">
<%											x=x+1
										end if 
									END IF
									
									'Tipo Selezione = null.
									IF isnull(TIPOSEL) THEN 
										if TIPODOM = "C" OR TIPODOM = "S" then
											if cVerify=1 then %>

												<b>Tipo Selezione non definito!</b>
												<input type="hidden" name="tipocampo<%=ndom%>" value="indefinito">

<%												cVerify=cVerify+1
											end if
										end if
									END IF
									
								'RISPOSTA DI TIPO VALORE							
								case "V"
									
									sTestoMin = RRR.Fields("TESTO_MIN")								
									if not isnull(RRR.Fields("IQ_MIN")) then
										RMIN  = CLNG(RRR.Fields("IQ_MIN"))	' PQ 18-06-2001
									end if
									sTestoMax = RRR.Fields("TESTO_MAX")	
									if not isnull(RRR.Fields("IQ_MAX")) then
										RMAX  = CLNG(RRR.Fields("IQ_MAX")) ' PQ 18-06-2001 
									end if 

									'campi hidden con i valori min e max di ogni risposta VALORE %>
									<input type="hidden" name="min<%=ndom%>" value="<%=RMIN%>">
									<input type="hidden" name="max<%=ndom%>" value="<%=RMAX%>">									
<%									
									'Tipo Selezione = Escludente (E)
									If TIPOSEL = "E" then %>

												<span class="tblTextFad"><%=sTestoMin%></span>&nbsp;&nbsp;&nbsp;&nbsp;
<%												for r=RMIN to RMAX %>
													<span class="tblTextFad">
														<input type="radio" name="DOM<%=ndom%>" value="<%=IDDOM%>_<%=IDRIS%>_<%=r%>"><%=r%>
													</span>&nbsp;&nbsp;&nbsp;&nbsp;
<%												next %>
												<span class="tblTextFad"><%=sTestoMax%></span>&nbsp;&nbsp;&nbsp;&nbsp;

										<%'campo hidden con il tipo di elemento visualizzato %>
										<input type="hidden" name="tipocampo<%=ndom%>" value="radiobutton">
<%									End if
									
									'Tipo Selezione = Multipla (M)
									'===> Qui NON dovrebbe entrare MAI.
									If TIPOSEL = "M" then %>

												<span class="tblTextFad"><%=sTestoMin%></span>&nbsp;&nbsp;&nbsp;&nbsp;
<%												for r=RMIN to RMAX %>
													<span class="tblTextFad">
														<input type="checkbox" name="chkMulti<%=ndom%>_<%=r%>" value="<%=IDDOM%>_<%=IDRIS%>_<%=r%>"><%=r%>
													</span>&nbsp;&nbsp;&nbsp;&nbsp;
<%												next %>
												<span class="tblTextFad"><%=sTestoMax%></span>&nbsp;&nbsp;&nbsp;&nbsp;

										<%'campo hidden con il tipo di elemento visualizzato%>
										<input type="hidden" name="tipocampo<%=ndom%>" value="checkbox">
<%									End if

									'Tipo Selezione = null.
									If isnull(TIPOSEL) THEN %>

													<span class="tblText1Fad"><span class="size">
														<b>Tipo Selezione non definito!</b>
														<input type="hidden" name="tipocampo<%=ndom%>" value="indefinito">
													</span>
<%									End if
							End select
							 
							'campo hidden con il tipo di risposta a ogni domanda (T o V)%>
							<input type="hidden" name="tipo<%=ndom%>" value="<%=TIPOR%>">							
<%						nrisp = nrisp +1
						RRR.MoveNext 
						loop
    				end if
    				RRR.CLOSE
					'___FINE IMPOSTAZIONE RISPOSTE___
				
				x=1
				nd = nd + 1
				ndom = ndom + 1 
				RRD.MoveNext 
				Loop
			End if
			RRD.CLOSE
			'___FINE IMPOSTAZIONE DOMANDE___
		
		naree = naree + 1
		RRA.MoveNext 
		LOOP
    END IF
	RRA.CLOSE

	if nrisp > 1 then %>
		<br>
		<table border="0" cellspacing="2" cellpadding="1" width="500px">
			<tr>
				<td align="center">
					<input type="hidden" name="DTTMST" value="<%=dtTmst%>">
					<input type="hidden" name="NDOM" value="<%=nd%>">
					<input type="hidden" name="NREC" value="<%=nrisp%>">
					<a onmouseover="javascript:window.status='' ; return true" href="javascript:history.back()">
						<img src="<%=Session("progetto")%>/images/indietro.gif" border="0" alt="Torna alla pagina precedente">
					</a>
					&nbsp;&nbsp;&nbsp;
					<input type="image" name="Conferma" src="<%=Session("Progetto")%>/images/conferma.gif" title="Invia questionario">
				</td>
			</tr>
		</table>
<%	end if%>
		</form>

<%
End Sub
	
'------------- M A I N -------------
%>	
<!--#include virtual = "include/sysfunction.asp"-->
<!--#include virtual = "util/dbutil.asp"-->
<!--#include virtual = "include/openconn.asp"-->
<!--#include virtual = "include/ControlDateVB.asp"-->
<%
if ValidateService(session("idutente"),"QGR_VisFrontGradimento", CC) <> "true" then 
	response.redirect "/util/error_login.asp"
end if

Dim DtTmst, idC, pres, idque, datas, ORA, ABIL, VIS
dim RsControllo, nAccessi, sErrore, nIdRisultato
		 
	IDQue = Request("IDQue")
	IdC = Session("idutente")

	TimeIni = now()
	ORA = ConvDateToDBs(date())
	DATAS = "NO"
	PRES  = "NO"
	ABIL = "NO"
	VIS = "NO"
		
	Inizio()
	Controllo()

	'if ValidateService(session("idpersona"),"QUEGRADOR") <> "true" then 
	'	response.redirect "../../util/error_login.asp"
	'end if	
	'IDC = Session("creator")
		
	ImpostaPag()

	if ABIL = "SI" then
		if PRES = "NO" then
			%>
			<br><br><table border="0" cellspacing="2" cellpadding="1" width="500px" class="tblSfondoFad">
				<tr align="center">
					<td class="tblText1Fad"><span class="size"><b>QUESTIONARIO NON ATTIVABILE</b></span></td>
				<tr>
			</table><br><br>
			<%
		else		
			if DATAS = "NO" then
				%>
				<br><br><table border="0" cellspacing="2" cellpadding="1" width="500px" class="tblSfondoFad">
					<tr align="center">
						<td class="tblText1Fad"><span class="size"><b>QUESTIONARIO NON ABILITATO</b></span></td>
					<tr>
				</table><br><br>
				<%
			end if
		end if
	end if
%>
<br>

<!--#include Virtual="/include/closeconn.asp"-->
<!--#include virtual="/strutt_coda2.asp"-->