<%
Sub Inizio()

	dim sFunzione
	dim sTitolo
	dim sCommento
	dim bCampiObbl
	
	sFunzione = "GESTION CUESTIONARIOS"
	sTitolo = "LISTA DE RESPUESTAS"
	sCommento = "Lista de respuestas<br>Haciendo clic en el titulo es posible modificar las caracteristicas de la respuesta."
	bCampiObbl = false
	sHelp = "/Pgm/help/Formazione/QGradimento/QGR_VisElencoRisposte/"	
	
%>
	<!--#include virtual="include/setTestata.asp"-->
<%
End Sub
'------------------------------------------------------------------------
Sub ElencoR

dim sSQL, nIdDom
dim Rs, rsRisposta
dim OpSQL

IdQue = request("idq")
IdDom = request("idd")
IdArea = request("ida")

set Rs=server.CreateObject("ADODB.Recordset")
sql="Select A.DESC_QUEST, B.DESC_AREAIQ, C.TESTO_DOMANDA, C.NUM_RISPOSTA, C.TIPO_RISPOSTA" &_
	" from INFO_QUEST A, IQ_AREA B, IQ_DOMANDA C" &_
	" where A.ID_INFOQUEST=" & IdQue &_
	" and B.ID_AREAIQ=" & IdArea &_
	" and C.ID_DOMANDAIQ =" & IdDom &_
	" and A.ID_INFOQUEST = B.ID_INFOIQ" &_
	" and A.ID_INFOQUEST = C.ID_INFOQUEST" &_
	" and B.ID_AREAIQ = C.ID_AREAIQ"
Rs.open sql, CC, 3
	sDescQuest	= Rs.FIELDS("DESC_QUEST")
	sDescArea	= Rs.FIELDS("DESC_AREAIQ")
	sDescDomanda= Rs.FIELDS("TESTO_DOMANDA")
	sTipoRisp	= Rs.Fields("TIPO_RISPOSTA")
	nRis		= Rs.FIELDS("NUM_RISPOSTA")
Rs.CLOSE
		
%>

<table width="500" border="0">
	<tr><td class="tbltext3">Cuestionario: &quot;<%=sDescQuest%>&quot; </td></tr>
	<tr><td class="tbltext3">Area: &quot;<%=sDescArea%>&quot; </td></tr>
	<tr><td class="tbltext3">Pregunta: &quot;<%=sDescDomanda%>&quot; </td></tr>
</table>
<br>

<%	
sSQL =	"Select ID_RISPOSTAIQ, ID_DOMANDAIQ, TIPO, TXT_RISPOSTA, TESTO_MINMAX, IQ_MIN, IQ_MAX" &_
		" from IQ_RISPOSTA where ID_DOMANDAIQ = " & IdDom &_
		" order by ID_RISPOSTAIQ"
set rsRisposta = CC.execute(sSQL)
%>

<table border="0" cellspacing="2" cellpadding="1" width="500">
	<tr class="sfondocomm">
	    <td width="20" align="center">#</td>
	    <td>Descripción de la Respuesta</td>
	</tr>
<%
c = 1
do while not rsRisposta.eof
%>
	<tr class="tblsfondo">
		<td class="tbldett"><%=c%></td>
		<%
		If rsRisposta("TIPO") = "T" then%>
			<td class="tbldett">
				<a href="QGR_ModRisposta.asp?idr=<%=rsRisposta("ID_RISPOSTAIQ")%>&amp;idq=<%=IdQue%>&amp;ida=<%=IdArea%>&amp;idd=<%=IdDom%>" class="tblagg"><%=rsRisposta("TXT_RISPOSTA")%></a>
			</td>
		<%
		Elseif rsRisposta("TIPO") = "V" then%>
			<td class="tbldett">
				<a href="QGR_ModRisposta.asp?idr=<%=rsRisposta("ID_RISPOSTAIQ")%>&amp;idq=<%=IdQue%>&amp;ida=<%=IdArea%>&amp;idd=<%=IdDom%>" class="tblagg">Scegliere un valore compreso tra <%=rsRisposta("IQ_MIN")%>&nbsp;/&nbsp;<%=rsRisposta("IQ_MAX")%></a>
			</td>
		<%
		Else%>
			<td class="tbldett">
				<a href="QGR_ModRisposta.asp?idr=<%=rsRisposta("ID_RISPOSTAIQ")%>&amp;idq=<%=IdQue%>&amp;ida=<%=IdArea%>&amp;idd=<%=IdDom%>" class="tblagg">Risposta Libera (non contiene testo predefinito)</a>
			</td>
		<%
		End if%>
	</tr>	
<%
	rsRisposta.movenext
	c = c + 1 
loop
rsRisposta.close
%>
</table>
<br>
<table width="500">
	<tr>
<%
if cint(nRis) > cint(c-1) then 
%> 
		<td align="center">
		<%
			PlsLinkRosso "QGR_InsRisposta.asp?idq=" & IdQue & "&ida=" & IdArea & "&idd=" & IdDom, "Ingresa Respuesta faltante"
		%>
		</td>
<% 
end if 
%>		
		<td align="center">
		<%
			PlsLinkRosso "QGR_VisDomanda.asp?idq=" & IdQue & "&ida=" &IdArea, "Lista de las Preguntas"
		%>
		</td>
	</tr>
</table>
<br>
<%
End Sub
'---------------------------------------------------------------------------
'M A I N
%>
<!--#include virtual="/strutt_testa2.asp"-->
<!--#include virtual="/include/openconn.asp"-->
<!--#include virtual="/include/SetPulsanti.asp"-->
<%	
	Inizio()
	ElencoR()
%>
<!--#include virtual="/include/closeconn.asp"-->
<!--#include virtual="/strutt_coda2.asp"-->