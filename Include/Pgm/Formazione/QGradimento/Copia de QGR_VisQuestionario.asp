<!--#include virtual="/strutt_testa2.asp"-->
<!--#include virtual="/include/openconn.asp"-->

<script language="Javascript">

function Carica_Pagina(npagina)
{
	document.frmPagina.pagina.value = npagina;
	document.frmPagina.submit();
}


	
function EsportaQ (num) {
	var sIdInfo = "";
	var i = 1;
	for (i = 1; i <= num; i++) {

		if (eval("chkEsporta" + i + ".checked") == true){
			if (sIdInfo == ""){
				sIdInfo = eval("chkEsporta" + i + ".value");
			}else{
				sIdInfo = sIdInfo + "$" + eval("chkEsporta" + i + ".value");
			}
		}
		
	}
	if (sIdInfo == ""){
		alert("Selezionare almeno un questionario da esportare.");
	}else{
//		alert(sIdInfo);	
		frmEsporta.idInfoQ.value = sIdInfo;
		frmEsporta.submit();
		}
	
}

</script>

<%
'------ SM  funzione di controllo per i questionari che non hanno alcuna occorrenza sulla tabella Struttura_quest
function ControllaElementi(nIdQuest)
	dim sSQL
	
	sSQL = "SELECT COUNT(*) as conta FROM STRUTTURA_QUEST WHERE ID_INFOQUEST = " & nIdQuest & _
		" AND TIPO_ELEMENTO ='D'"
	
	set conta = CC.Execute(sSQL)
	
	if ( cint(conta("conta")) > 0 ) then
		ControllaElementi = true
	else
		ControllaElementi = false
	end if
	
end function
'---------fine-----------
function controllaUtilizzo(nIdQuest)
	dim sSQL
	
	sSQL = "SELECT COUNT(*) as conta FROM IQ_RISULTATO WHERE ID_INFOQUEST = " & nIdQuest
	
	set conta = CC.Execute(sSQL)
	
	if ( cint(conta("conta")) > 0 ) then
		controllaUtilizzo = true
	else
		controllaUtilizzo = false
	end if
	
end function

Sub Inizio()
	dim sFunzione
	dim sTitolo
	dim sCommento
	dim bCampiObbl
	
	sFunzione = "GESTION DE CUESTIONARIOS"
	sTitolo = "LISTA DE CUESTIONARIOS"
	sCommento = "En esta pagina se muestran todos los cuestionarios existentes. Para visualizar la composición, haga clic sobre el icono de vista previa correspondiente.<BR>Si desea modificar la composición, haga clic sobre la imagen asociada para verificar las secciones constituyentes. <BR>Para ingresar uno nuevo, haga clic en el link <B>Ingresa nuevo cuestionario</B>. <BR>Para publicar un cuestionario, seleccionelo y luego haga clic en <B>Exporta los cuestionari seleccionados </B>.   "
	bCampiObbl = false
	sHelp = "/Pgm/help/Formazione/QGradimento/QGR_VisQuestionario/"	
	
%>
	<!--#include virtual="include/SetTestata.asp"-->
<%
End Sub
'----------------------------------------------------------------------
Sub ElencoQ()

dim sSQL
dim rsQuest
dim nProg
dim verifica
dim enable

nProg = 0


if Progetto = "ITES" then
	sSql = "Select id_infoquest, Desc_quest,Intest_Quest " &_
	"from info_quest order by id_infoquest"
else
	sSql = "Select id_infoquest, Desc_quest,Intest_Quest " &_
	"from info_quest WHERE TIPO_QUEST <> 'T' OR TIPO_QUEST IS NULL order by id_infoquest"
end if
'set rsQuest = CC.execute(sSQL)
' Paginazione 18/02/2004 EPILI
Record=0
nTamPagina=15
If Request.Form("pagina")= "" Then
	nActPagina=1
Else
	nActPagina=Clng(Request.Form("pagina"))
End If

Set rsQuest = Server.CreateObject("ADODB.Recordset")
rsQuest.Open sSQL, CC, 3


if not rsQuest.eof then

	rsQuest.PageSize = nTamPagina
	rsQuest.CacheSize = nTamPagina	

	nTotPagina = rsQuest.PageCount		
	If nActPagina < 1 Then
		nActPagina = 1
	End If
	If nActPagina > nTotPagina Then
		nActPagina = nTotPagina
	End If

	rsQuest.AbsolutePage=nActPagina
	nTotRecord=0		    


	%>
	<table border="0" cellspacing="1" cellpadding="2" width="500">
		<tr class="sfondocomm">
	        <td width="20"><b>Exporta</b></td>
	        <td width="20" align="center"><b>#</b></td>
	        <td width="360"><b>Título</b></td>
	        <!--td width="60" align="center"><b>Tipo</b></td-->
	        <td width="60" align="center"><b>Vista Previa del Cuestionario</b></td>
	        <td width="60" align="center"><b>Composicion del Cuestionario</b></td>
		</tr>
		
		<form name="frmEsporta" method="post" action="QGR_EsportaQGrad.asp">
			<input type="hidden" name="idInfoQ" value>
		</form>

	<%
	while rsQuest.EOF <> True And nTotRecord < nTamPagina
	'do while not rsQuest.eof
		nProg = nProg + 1 
	'	Response.Write "nProg = " & nProg & "<BR>"

		' *********************************************************************
		' EPILI 17/02/2004
		' Per evitare che permetta di importare sempre lo stesso questionario e 
		' vada in errore la DLL per chiave duplicata.
		sSql = "SELECT COUNT(*) AS CONTA FROM INFO_QUEST WHERE " &_
			" DESC_QUEST = '" & Replace(rsQuest("DESC_QUEST"),"'","''") & "'" &_
			" AND INTEST_QUEST = '" & Replace(rsQuest("INTEST_QUEST"),"'","''") & "'"
	
		'set rsConta = CX.execute(sSQL)
		set rsConta = CC.execute(sSQL)
		verifica = cint(rsConta("CONTA"))
		rsConta.close
		set rsConta = nothing			
		if verifica > 0 then
			enable = "disabled title='Cuestionario ya compartido' Style='CURSOR:HELP'"	
		else
			enable = " title='Seleccionar para compartir el cuestionario' Style='CURSOR:HELP' "	
		end if
		' *********************************************************************
		modificabile = true
		nMod = 1 
		if not controllaUtilizzo(clng(rsQuest("id_infoquest"))) then
			modificabile = false
			nMod = 0 
		end if
%>

<%'''8/7/2003: PASSATI VALORI TRAMITE FORM ANZICHE' QUERYSTRING %>
		<form name="frmModifica<%=nProg%>" method="post" action="QGR_ModQuest.asp">
			<input type="hidden" name="id" value="<%=rsQuest("id_infoquest")%>">
			<input type="hidden" name="mod" value="<%=nMod%>">
		</form>
		<form name="frmAnteprima<%=nProg%>" method="post" action="QGR_VisArea.asp">
			<input type="hidden" name="id" value="<%=rsQuest("id_infoquest")%>">
			<input type="hidden" name="mod" value="<%=1%>">
			<input type="hidden" name="sFunz" value="<%=sFunz%>">
		</form>
		<form name="frmArea<%=nProg%>" method="post" action="QGR_VisArea.asp">
			<input type="hidden" name="id" value="<%=rsQuest("id_infoquest")%>">
			<input type="hidden" name="mod" value="<%=2%>">
		</form>
<%'''FINE 8/7/2003%>

		<tr class="tblsfondo">
			<td class="tblAgg">
			<%if verifica > 0 then%>
				<img SRC="/images/icons/Disabled.gif" title="Cuestionario ya compartido" Style="CURSOR:HELP" border="0" WIDTH="15" HEIGHT="15">	
				<input type="hidden" name="chkEsporta<%=nProg%>" value="false">
			<%else
			''''''''SM non vengono esportati i questionari che non hanno domande
					if ControllaElementi(clng(rsQuest("id_infoquest"))) then%>
						<input type="checkbox" <%=enable%> name="chkEsporta<%=nProg%>" value="<%=rsQuest("id_infoquest")%>">
					<%else%>
						<input type="checkbox" disabled name="chkEsporta<%=nProg%>" value="<%=rsQuest("id_infoquest")%>">	
						
					<%end if
			'''''''''   fine %>
			<%end if%>

			</td>

			<td class="tblDett">
				<%=nProg + (nActPagina * nTamPagina) - nTamPagina%>
			</td>
<%'''8/7/2003: PASSATI VALORI TRAMITE FORM ANZICHE' QUERYSTRING %>			
			<td>
				<!--a href="QGR_ModQuest.asp?id=<%'''=rsQuest("id_infoquest")%>" class="tblagg" onmouseover="javascript:window.status='' ; return true">					<%'''=rsQuest("desc_quest")%>				</a-->
				<a title="Modifica cuestionario" href="javascript:frmModifica<%=nProg%>.submit();" class="tblagg">
					<%=rsQuest("desc_quest")%>
				</a>			
			</td>
			<td align="center">
				<!--a href="QGR_VisAnteprima.asp?id=<%'''=rsQuest("id_infoquest")%>" class="tblAgg" onmouseover="javascript:window.status='' ; return true">					<img src="<%'''=Session("progetto")%>/images/formazione/txt.gif" border="0" alt="Anteprima Questionario di Gradimento" WIDTH="20" HEIGHT="21">				</a-->
				<a href="javascript:frmAnteprima<%=nProg%>.submit();" class="tblAgg">
					<img src="<%=Session("progetto")%>/images/formazione/txt.gif" border="0" alt="Vista Previa del Cuestionario" WIDTH="20" HEIGHT="21">
				</a>
			</td>
			<%
			if not modificabile then
			%>
				<td align="center">
					<!--a href="QGR_VisArea.asp?id=<%'''=rsQuest("id_infoquest")%>" class="tblAgg" onmouseover="javascript:window.status='' ; return true">						<img src="<%'''=Session("progetto")%>/images/formazione/re.gif" border="0" alt="Elenco delle Aree" WIDTH="12" HEIGHT="16">					</a-->
					<a href="javascript:frmArea<%=nProg%>.submit();" class="tblAgg">
						<img src="<%=Session("progetto")%>/images/formazione/re.gif" border="0" alt="Lista de las Secciones" WIDTH="12" HEIGHT="16">
					</a>
				</td>
			<%else%>
				<td align="center">
					-
				</td>
			<%end if%>
			
				
<%'''FINE 8/7/2003%>
		</tr>	
<%
		rsQuest.movenext
		nTotRecord=nTotRecord + 1
	wend 
	

	
	%>
	</table>
<%

	Response.Write "<TABLE border=0 width=470 align=center>"
	Response.Write "<tr>"
	if nActPagina > 1 then
		Response.Write "<td align=right width=480>"
		Response.Write "<A HREF=""javascript:Carica_Pagina('" & nActPagina-1 & "')"">"
		Response.Write "<img border=0 src=" & Session("Progetto") & "/images/precedente.gif></A></td>"
	end if
	if nActPagina < nTotPagina then
		Response.Write "<td align=right>"
		Response.Write "<A HREF=""javascript:Carica_Pagina('" & nActPagina+1 & "')"">"		
		Response.Write "<img border=0 src=" & Session("Progetto") & "/images/successivo.gif></A></td>"
	end if
	Response.Write "</tr></TABLE>"

%>
<br>
<table border="0" cellspacing="1" cellpadding="2" width="500">
	<tr>
		<td align="center">
			<%
				PlsLinkRosso "QGR_InsQuest.asp", "Ingresa nuevo cuestionario"
			%>
		</td>
		<td align="center">
			<%
			
			'if Session("Progetto")<>"/ITES" then
			'	PlsLinkRosso "javascript:EsportaQ(" & nProg & ")", "Esporta i questionari selezionati"
			'end if
			%>
		</td>
	</tr>
</table>
<%	
else%>
	
	<table border="0" cellspacing="1" cellpadding="2" width="500">
		<tr>
			<td align="center" class="tbltext3">
				No existen cuestionarios disponibles
			</td>
		</tr>
	</table>
	<br>
	<table border="0" cellspacing="1" cellpadding="2" width="500">
		<tr>
			<td align="center">
				<%
					PlsLinkRosso "QGR_InsQuest.asp", "Nuevo Cuestionario"
				%>
			</td>
		</tr>
	</table>
	
<%
end if%>
<form name="frmPagina" method="post" action="QGR_VisQuestionario.asp">
	<input type="hidden" name="pagina" value>
</form>

<%
rsQuest.close
End Sub
'----------------------------------------------------------------------
'M A I N
%>
<!--#include virtual="include/SetPulsanti.asp"-->
<!--#include virtual="/include/HTMLEncode.asp"-->

<%
'if ValidateService(session("idutente"),"QGR_VisQuestionario", CC) <> "true" then 
'	response.redirect "/util/error_login.asp"
'end if	


dim Progetto, sFunz


Progetto = "/ITES"
'  SM  imposto una variabile che mi segnala se vengo 
'dalla funz. definizione dei questionari
sFunz = "DEF"
'''''''''''fine''''
'Progetto = impostaprogetto()
%>
<!--#include virtual="/include/openpar.asp"-->

<%
Inizio()
ElencoQ()
%>
<!--#include virtual="/include/closepar.asp"-->
<!--#include virtual="/include/closeconn.asp"-->
<!--#include virtual="/strutt_coda2.asp"-->