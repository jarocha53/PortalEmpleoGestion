<!--#include virtual="/strutt_testa2.asp"-->
<!--#include virtual="/include/openconn.asp"-->
<!--#include virtual="include/SetPulsanti.asp"-->
<!--#include virtual="/include/HTMLEncode.asp"-->
<!--#include virtual="/include/RuoloFunzionale.asp"-->

<%
	if ValidateService(session("idutente"),"QGR_VisQuestionario", CC) <> "true" then 
		response.redirect "/util/error_login.asp"
	end if	
%>

<script language="Javascript">

function Carica_Pagina(npagina)
{
	document.frmPagina.pagina.value = npagina;
	document.frmPagina.submit();
}

//-----------------------------------------------------------------------------------------------------------------------------------	

function ImportaQ (num) {
	var sIdInfo = "";
	var i = 1;
	
	for (i = 1; i <= num; i++) {
		if (eval("chkImporta" + i + ".checked") == true){
			if (sIdInfo == ""){
				sIdInfo = eval("chkImporta" + i + ".value");
			}else{
				sIdInfo = sIdInfo + "$" + eval("chkImporta" + i + ".value");
			}
		}
	}
	if (sIdInfo == ""){
		alert("Seleccionar al menos un cuestionario para Importar.");
	}else{
//		alert(sIdInfo);	
		frmImporta.idInfoQ.value = sIdInfo;
		frmImporta.submit();
		}
	
}

</script>

<%
'-------------------------------------------------------------------------------------------------------------------------------

Sub Msgetto(Msg)
%>
	<br>
	<table align="center">
		<tr>
			<td class="tbltext3"><b><%=Msg%></b></td>
		</tr>	
	</table>
<%
End Sub

'-------------------------------------------------------------------------------------------------------------------------------

Sub Inizio()
	dim sFunzione
	dim sTitolo
	dim sCommento
	dim bCampiObbl
	
	sFunzione = "GESTION DE CUESTIONARIOS"
	sTitolo = "LISTA DE QUESTIONARIOS"
	sCommento = "Lista de los Cuestionarios que se pueden importrar. " & _
				"<BR>Para ver la vista previa del cuestionario " & _
				"haga clic sobre el icono correspondiente." & _
				"<BR>Para Importar un cuestionario seleccionarlo " & _
				"y hacer clic en el siguiente link:  <B>Importa cuestionarios seleccionados</B>"
	bCampiObbl = false
	sHelp = "/Pgm/help/Formazione/QGradimento/QGR_VisQuestionario/"	
	
%>
	<!--#include virtual="include/SetTestata.asp"-->
<%
End Sub

'----------------------------------------------------------------------

Private Function DecodROrga(sCodice)

Dim i
Dim swTrovato 
Dim rsApp
    
    swTrovato = 0

    For i = 1 To UBound(aROrga, 2)
        If aROrga(0, i) = sCodice Then
            DecodROrga = aROrga(1, i)
            swTrovato = 1
            Exit For
        End If
    Next

    If swTrovato = 0 Then
        sSQL = "SELECT DESCRIZIONE FROM TADES " & _
        "WHERE NOME_TABELLA = 'RORGA' " & _
        "AND CODICE = '" & sCodice & "'"

        set rsApp = CC.execute(sSQL)

        If rsApp.EOF Then
            DecodROrga = ""
            Exit Function
        Else
            DecodROrga = Trim(rsApp("DESCRIZIONE"))
            ReDim Preserve aROrga(1, UBound(aROrga, 2) + 1)
            aROrga(0, UBound(aROrga, 2)) = Trim(sCodice)
            aROrga(1, UBound(aROrga, 2)) = Trim(rsApp("DESCRIZIONE"))
        End If
        rsApp.Close
        Set rsApp = Nothing
    End If
    
End Function

'----------------------------------------------------------------------

Sub ElencoQ()

	dim sSQL
	dim rsQuest
	dim nProg

	nProg = 0

	sSql = "SELECT COUNT (*) AS CONTA " & _
			"FROM QGRAD_PROJ " & _
			"WHERE PROGETTO <> '" & UCase(mid(session("progetto"), 2)) & "'"

	set rsConta = CX.execute(sSQL)

	if Trim(rsConta("CONTA")) = "0" then
		Msgetto "No hay cuestionarios disponibles"
		exit sub
	end if


	sSql = "SELECT ID_INFOQUEST, DESC_QUEST, INTEST_QUEST " & _
			"FROM INFO_QUEST " & _
			"WHERE TIPO_QUEST = 'T' " & _
			"ORDER BY ID_INFOQUEST"

	Set rsQuest = Server.CreateObject("ADODB.Recordset")
	rsQuest.Open sSQL, CX, 3

	'aggiunta paginazione C.Tugnoli 30/04/2004
	nTamPagina=15
	If Request.Form("pagina") = "" Then
		nActPagina=1
	Else
		nActPagina=Clng(Request.Form("pagina"))
	End If

	if not rsQuest.eof then

	'Response.Write rsQuest.RecordCount

		rsQuest.PageSize = nTamPagina
		rsQuest.CacheSize = nTamPagina	

		nTotPagina = rsQuest.PageCount		

		If nActPagina < 1 Then
			nActPagina = 1
		End If

		If nActPagina > nTotPagina Then
			
			'Response.Write "entra "
			
			nActPagina = nTotPagina
		End If

		rsQuest.AbsolutePage = nActPagina
		nTotRecord=0		    

		%>
		<table border="0" cellspacing="1" cellpadding="2" width="500">
			<tr class="sfondocomm">
		        <td width="20"><b>Importa</b></td>
		        <td width="20" align="center"><b>#</b></td>
		        <td width="100"><b>Proyecto</b></td>
		        <td width="360"><b>Descripción</b></td>
		        <td width="40" align="center"><b>Vista Previa del Cuestionario</b></td>
			</tr>
			
			<form name="frmImporta" method="post" action="QGR_ImportaQGrad.asp">
				<input type="hidden" name="idInfoQ" value="">
			</form>

		<%

		ReDim aROrga(1, 0)

		while rsQuest.EOF <> True And nTotRecord < nTamPagina
			
			sSql = "SELECT PROGETTO, COD_RUOFU, COD_RORGA, DT_ESPORTAZIONE " & _
					" FROM QGRAD_PROJ " & _
					" WHERE ID_INFOQUEST = " & rsQuest("ID_INFOQUEST") 
			set rsQGrad = CX.execute(sSQL)

			' *********************************************************************
			' EPILI 17/02/2004
			' Per evitare che permetta di importare sempre lo stesso questionario e 
			' vada in errore la DLL per chiave duplicata.
			sSql = "SELECT COUNT(*) AS CONTA FROM INFO_QUEST WHERE " &_
				" DESC_QUEST = '" & Replace(rsQuest("DESC_QUEST"),"'","''") & "'" &_
				" AND INTEST_QUEST = '" & Replace(rsQuest("INTEST_QUEST"),"'","''") & "'"
		
		'Response.Write sSql
			
			set rsConta = CC.execute(sSQL)
			
			verifica = cint(rsConta("CONTA"))
			
			rsConta.close
			set rsConta = nothing
			' *********************************************************************


			if verifica = 0  then
				
				if UCase(rsQGrad("PROGETTO")) <> UCase(mid(session("progetto"), 2)) then
			
					nProg = nProg + 1 
					
					sRuoli = ""

					do while not rsQGrad.EOF
					
						sROrga = "" 
						sRuoFu = ""
					
						if not IsNull (rsQGrad("COD_RORGA")) then
							sROrga = DecodROrga(rsQGrad("COD_RORGA"))
							
						else
							sROrga = ""
						end if
					
						if not IsNull(rsQGrad("COD_RUOFU")) then
							sRuoFu = DecRuolo(trim(rsQGrad("COD_RUOFU")))
						else
							sRuoFu = ""
						end if

						'se entrambi pieni
						if sROrga <> "" and sRuoFu <> "" then
							if sRuoli <> "" then
								sRuoli = sRuoli & ", " & sROrga & " / " & sRuoFu
							else
								sRuoli = sROrga & " / " & sRuoFu
							end if
						else
							'se uno dei due pieno
							if sROrga <> "" or sRuoFu <> "" then
								if sRuoli <> "" then
									sRuoli = sRuoli & ", " & sROrga & sRuoFu
								else
									sRuoli = sROrga & sRuoFu
								end if
							end if
						end if
					
								
						sOldProg = trim(rsQGrad("PROGETTO"))
						dDtExp = rsQGrad("DT_ESPORTAZIONE")
						
						rsQGrad.MoveNext
					loop
					
					if sRuoli <> "" then
						sRuoli = ", creado por: " & sRuoli
					end if
					'QGR_VisAnteprima.asp
					%>
					<form name="frmAnteprima<%=nProg%>" method="post" action="QGR_VisArea.asp">
						<input type="hidden" name="id" value="<%=rsQuest("id_infoquest")%>">
						<input type="hidden" name="myProg" value="<%=Progetto%>">
						<input type="hidden" name="mod" value="1">
						<input type="hidden" name="pagIndietro" value="QGR_VisImpQuest.asp">
					</form>

					<!--form name="frmAnteprima<%'=nProg%>" method="post" action="QGR_VisArea.asp">
						<input type="hidden" name="id" value="<%'=rsQuest("id_infoquest")%>">
						<input type="hidden" name="myProg" value="/GPCS">
						<input type="hidden" name="mod" value="1">
						<input type="hidden" name="pagIndietro" value="QGR_VisImpQuest.asp">
					</form-->

					
					<tr class="tblsfondo">
						<td class="tblAgg">
							<input type="checkbox" name="chkImporta<%=nProg%>" value="<%=rsQuest("ID_INFOQUEST")%>">
						</td>

						<td class="tblDett">
							<%'=nProg%>
							<%=nProg + (nActPagina * nTamPagina) - nTamPagina%>
						</td>
						<td class="tblDett">
							<%=sOldProg%>
						</td>
						
						<td class="tblDett" style="cursor:help" title="Fecha exportación: <%=dDtExp&sRuoli%>">
							<%=rsQuest("DESC_QUEST")%>
						</td>
						
						<td align="center">
							<a href="javascript:frmAnteprima<%=nProg%>.submit();" class="tblAgg">
								<img src="<%=Session("progetto")%>/images/formazione/txt.gif" border="0" alt="Vista previa del Cuestionario" WIDTH="20" HEIGHT="21">
							</a>
						</td>
					</tr>	
					<%
				end if
			end if
			rsQuest.movenext
			nTotRecord=nTotRecord + 1
		wend

		%>
		</table>
	<%	

		Response.Write "<TABLE border=0 width=470 align=center>"
		Response.Write "<tr>"
		if nActPagina > 1 then
			Response.Write "<td align=right width=480>"
			Response.Write "<A HREF=""javascript:Carica_Pagina('" & nActPagina-1 & "')"">"
			Response.Write "<img border=0 src=" & Session("Progetto") & "/images/precedente.gif></A></td>"
		end if
		if nActPagina < nTotPagina then
			Response.Write "<td align=right>"
			Response.Write "<A HREF=""javascript:Carica_Pagina('" & nActPagina+1 & "')"">"		
			Response.Write "<img border=0 src=" & Session("Progetto") & "/images/successivo.gif></A></td>"
		end if
		Response.Write "</tr></TABLE>"

	else		
		Msgetto "No hay cuestionarios disponibles"
	end if

	if nProg > 0 then
	%>

	<br>
	<table border="0" cellspacing="1" cellpadding="2" width="500">
		<tr>
			<td align="center">
				<%
					PlsLinkRosso "javascript:ImportaQ(" & nProg & ")", "Importa los cuestionarios seleccionados"
				%>
			</td>
		</tr>
	</table>
	<%
	else
		Msgetto "No hay cuestionarios disponibles"
	end if

	%>
	<form name="frmPagina" method="post" action="QGR_VisImpQuest.asp">
		<input type="hidden" name="pagina" value>
	</form>
	<%
	rsQuest.close

End Sub
'----------------------------------------------------------------------
'M A I N

dim Progetto
	Progetto = "/ITES"
'	Progetto = ImpostaProgetto()

%>
<!--#include virtual="/include/openpar.asp"-->
<%	
'	Progetto = ImpostaProgetto()
	
dim aROrga

	Inizio()
	ElencoQ()
%>
<!--#include virtual="/include/closepar.asp"-->

<!--#include virtual="/include/closeconn.asp"-->
<!--#include virtual="/strutt_coda2.asp"-->