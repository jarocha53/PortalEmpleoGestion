<%'Option Explicit 
  ' Response.ExpiresAbsolute = Now() - 1 
  ' Response.AddHeader "pragma","no-cache"
  ' Response.AddHeader "cache-control","private"
  ' Response.CacheControl = "no-cache"
%>

<!-- #include virtual="/strutt_testa2.asp"-->
<!--#include virtual ="/include/openconn.asp"-->
<!--#include virtual ="/util/portallib.asp"-->
<%
'if ValidateService(session("idutente"),"SEL_INSDOMISCR", CC) <> "true" then 
'	response.redirect "/util/error_login.asp"
'end if


Session("menu")= ""
%>
<!-- ************** Javascript Inizio ************ -->
<!--include del file per fare i controlli sulle stringhe-->
<script LANGUAGE="Javascript" src="/Include/ControlString.inc"></script>
<!--include del file per fare i controlli sulla validit� delle date-->
<script LANGUAGE="Javascript" src="/Include/ControlDate.inc"></script>

<!--include del file per fare i controlli sulla numericit� dei campi-->
<script LANGUAGE="Javascript" src="/Include/ControlNum.inc"></script>

<!--include del file per fare i controlli sulla validit� del codice fiscale-->
<script LANGUAGE="Javascript" src="/Include/ControlCodFisc.inc"></script>

<script LANGUAGE="Javascript" src="/Include/help.inc"></script>
<script LANGUAGE="Javascript" src="/include/SelComune.js"></script>
<script LANGUAGE="Javascript" src="/include/SelComuneRes.js"></script>
<script LANGUAGE="JavaScript" src="/Include/Help.inc">
</script>
<!-- ************** ASP Inizio *************** -->

<!--#include virtual = "/include/DecCod.asp"-->
<!--#include virtual = "/include/ControlDateVB.asp"-->
<!--#include virtual = "/util/dbutil.asp"-->

<!--#include virtual="/include/SelAreaTerrBandi.asp"-->
<!--#include virtual="/include/DecDizDati.asp"-->

<script LANGUAGE="Javascript">

function Pulisci()
{
	document.frmDomIscri.txtComuneRes.value = ""
}


var windowArea

function SelImpresa(sCdTimp,sDesCdTimp,sArea,sCampo,sNomeCampo,sNomeCampo2,nBando){
		windowArea = window.open ('SEL_SelImpresa.asp?Timpr=' + sCdTimp + '&DescTimpr=' + sDesCdTimp + '&Area=' + sArea + '&sTxtArea=' + sCampo +'&NomeCampo=' + sNomeCampo +'&NomeCampo2=' + sNomeCampo2+'&nBando='+ nBando,'Info','width=425,height=450,Resize=No,Scrollbars=yes');	
}

function PulisciCom()
{
	document.frmDomIscri.txtComuneNascita.value = ""
	document.frmDomIscri.txtComune.value = ""
}

function PulisciRes()
{
	document.frmDomIscri.txtComuneRes.value = ""
	document.frmDomIscri.txtComRes.value = ""
	document.frmDomIscri.txtCapRes.value = ""
}




//Funzione per i controlli dei campi da inserire 

//-------- Controlli di OBBLIGATORIETA' --------
//-------- Controlli FORMALI            --------
//-------- Controlli di RELAZIONE       --------

	function ControllaDati(frmDomIscri){
	   
        //Legge privacy
/*        if (windowArea != null ) 
	    {
		windowArea.close(); 
	    }

        if (frmDomIscri.radConsenso[1].checked) 
	    {	
		f = '/PLAVORO/Pgm/Iscr_Utente/UTE_Leg_Privacy.asp';
		windowArea=window.open(f, '','width=520,height=300,left=250,top=250,Resize=no,scrolling=no');
		window.event.returnValue = false;
		return false
	    }
*/
		//Cognome
		frmDomIscri.txtCognome.value=TRIM(frmDomIscri.txtCognome.value)
		if (frmDomIscri.txtCognome.value == ""){
			alert("El campo Apellido es obligatorio");
			frmDomIscri.txtCognome.focus();
			return false
		}
		//Nome
		frmDomIscri.txtNome.value=TRIM(frmDomIscri.txtNome.value)
		if (frmDomIscri.txtNome.value == ""){
			alert("El campo Nombre es obligatorio"),
			frmDomIscri.txtNome.focus(); 
			return false
		}
		//Data di Nascita
		DataNascita = TRIM(frmDomIscri.txtDataNascita.value)
		if (DataNascita == ""){
			alert("El campo Fecha de Nacimiento es obligatoria");
			frmDomIscri.txtDataNascita.focus();
			return false
		}
		//Controllo della validit� della Data di Nascita
		if (!ValidateInputDate(DataNascita)){
			frmDomIscri.txtDataNascita.focus(); 
			return false
		}
		//Comune di Nascita e Provincia di Nascita obbligatori
		//e in alternativa con Nazione di Nascita
/*		frmDomIscri.txtComuneNascita.value=TRIM(frmDomIscri.txtComuneNascita.value)
		if (frmDomIscri.txtComuneNascita.value == ""){ 
			if (frmDomIscri.cmbProvinciaNascita.value == ""){
				if (frmDomIscri.cmbNazioneNascita.value == ""){
					alert("Comune e Provincia di Nascita o Nazione di Nascita obbligatori");
					frmDomIscri.cmbProvinciaNascita.focus();
					return false
				}
			}
		}
       
		if (frmDomIscri.txtComuneNascita.value != ""){ 
			if (frmDomIscri.cmbProvinciaNascita.value != ""){
				if(frmDomIscri.cmbNazioneNascita.value != ""){
					alert("Indicare solo il Comune e la Provincia di Nascita oppure la Nazione di Nascita");
					frmDomIscri.cmbNazioneNascita.focus(); 
					return false
				}
			}	
		}
		//Se Comune di Nascita � digitato, la Provincia � obbligatoria
		if (frmDomIscri.txtComuneNascita.value != ""){
			if (frmDomIscri.cmbProvinciaNascita.value == ""){
				alert("Provincia di Nascita obbligatoria");
				frmDomIscri.cmbProvinciaNascita.focus();
				return false
			}
		}
		//Se la Provincia di Nascita � digitata, il Comune � obbligatorio
		if  (frmDomIscri.txtComuneNascita.value == ""){
			if (frmDomIscri.cmbProvinciaNascita.value != ""){
				alert("Comune di Nascita obbligatorio");
				frmDomIscri.txtComuneNascita.focus();
				return false
			}
		}
	*/	//Sesso
		if (frmDomIscri.cmbSesso.value == ""){
			alert("El campo Sexo obligatorio");
			frmDomIscri.cmbSesso.focus(); 
			return false
		}
		//Codice Fiscale
/*		frmDomIscri.txtCodFisc.value = TRIM(frmDomIscri.txtCodFisc.value)
		if (frmDomIscri.txtCodFisc.value == ""){
			alert("Codice identificativo obbligatorio");
			frmDomIscri.txtCodFisc.focus(); 
			return false
		}
		//Codice Fiscale deve essere di 16 caratteri
		if (frmDomIscri.txtCodFisc.value.length != 16){
			alert("Formato del Codice Fiscale errato");
			frmDomIscri.txtCodFisc.focus(); 
			return false
		}
*/		//Contollo la validit� del Codice Fiscale
/*		var contrCom=""
		DataNascita = frmDomIscri.txtDataNascita.value;
		CodFisc = frmDomIscri.txtCodFisc.value.toUpperCase();
		Sesso = frmDomIscri.cmbSesso.value;
		
		if (frmDomIscri.txtComuneNascita.value != ""){
		          contrCom =frmDomIscri.txtComune.value;   
		}
		else{
		          contrCom = frmDomIscri.cmbNazioneNascita.value;
		}
		
		if (!ControllCodFisc(DataNascita,CodFisc,Sesso,contrCom)){
			alert("Codice Fiscale Errato");
			frmDomIscri.txtCodFisc.focus(); 
			return false
		}
*/		//Cittadinanza
/*		if (frmDomIscri.cmbCittadinanza.value == ""){
			alert("Cittadinanza obbligatoria");
			frmDomIscri.cmbCittadinanza.focus();
			return false
		}
*/ 
			
	return true

	}



</script>
<%	
'-------------------------------------------------------------------------------------------------------------------------------
Sub Titolo(sTitolo,sDescTitolo) %>
    <table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
		<tr height="18">
			<td class="sfondomenu" width="67%" height="18"><span class="tbltext0"><b>&nbsp;<%=sTitolo%></b></span></td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
			<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
		</tr>
	</table>
	<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
		<tr>
			<td align="left" class="sfondocomm"><%=sDescTitolo%></td>
		</tr>
		<tr height="2">
			<td class="sfondocomm" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>
	<br>
<%
end sub

Sub Msgetto(Msg)

%>
	<br>
	<table align="center">
		<tr>
			<td class="tbltext3"><b><%=Msg%></b></td>
		</tr>	
		<tr>
			<td>&nbsp;</td>
		</tr>	
		<tr>
			<td align="center"><a HREF="javascript:history.back()"><img SRC="<%=Session("Progetto")%>/images/indietro.gif" border="0" WIDTH="55" HEIGHT="40"></a></td>
		</tr>
	</table>
<%
End Sub

'-------------------------------------------------------------------------------------------------------------------------------

sub Inizio()
%>
<script LANGUAGE="javascript" FOR="window" >
	//if (window.frmDomIscri.cbbAppartenenza.value=="03") {
	//	window.frmDomIscri.cbbDisocc.disabled=false;
	//	}		
//	window.frmDomIscri.cbbAppartenenza.value="";
//	window.frmDomIscri.cbbAppartenenza.text="";
//	window.frmDomIscri.cbbDisocc.value="";
//	window.frmDomIscri.cbbDisocc.text="";
</script>
	
	<br>
	<table cellpadding="0" cellspacing="0" width="100%" border="0">
		<tr>
			<!--Inizio Ambra
			td class="sfondomenu" height="18" width="45%"  align="center">
			<span class="tbltext0"><b>&nbsp;ANAGRAFICA</b></span></td><td><img src="<%=Session("Progetto")%>/images/FrecciaBlu.gif" border ="0" height="18" width="50%"></td>
			<td class="sfondocommQuest" height="18" width="45%" align="center" >
			<span class="tbltext2"><b>&nbsp;Compilazione questionario</b><br></span></td><td><img src="/ITES/images/FrecciaGrigia.gif" border ="0" height="18"></td
			-->
			<td nowrap height="18"><img src="<%=Session("Progetto")%>/images/tab_blu_left.gif" border="0" height="18"></td>
			<td nowrap background="<%=Session("Progetto")%>/images/tab_blu_center.gif" height="18" title="Datos Basicos"><span
			    class="home_testo_bianco_b"><font color=White> Datos Personales</font></span></td>
			<td nowrap width="4%" height="18"><img src="<%=Session("Progetto")%>/images/tab_blu_right.gif" border="0" height="18"></td>
			<!--td nowrap height="18" width="1%"><img src="<%=Session("Progetto")%>/images/1x1.gif" border="0" height="18px" ></td-->
			<td nowrap height="18"><img src="<%=Session("Progetto")%>/images/tab_inactive_left.gif" border="0" height="18" ></td>
			<td nowrap background="<%=Session("Progetto")%>/images/tab_inactive_center.gif" height="18" title="Instrucciones para el llenado"><span class="home_testo_bianco_b">Cuestionario</span></td>
			<td nowrap height="18" width="80%"><img src="<%=Session("Progetto")%>/images/tab_inactive_right.gif" border="0" height="18"></td>
			<!--fine ambra-->
					
			<!--form method=post name=FrmCompilaQuest action='/pgm/Formazione/QGradimento/QGR_VisArea.asp'>
				<input type=hidden name="IdPers" value="<%=sIdpers%>">
				<TD height="18" width="50%"align=center valign=middle class=sfondomenu><a class=tbltext0 href="javascript:document.FrmCompilaQuest.submit();" ONMOUSEOVER="window.status = ' '; return true"><u><b>Compila Questionario</b></u></a></TD>
			</form-->			
		</tr>
		</table>
		<Table cellpadding="4" cellspacing="0" width="100%" border="0" style="border-collapse: collapse;">
		<Tr>
			<Td class="stroked">
				<table cellpadding="4" cellspacing="0" width="100%" border="0" >
				<tr>
					<td class="textblack" width="57%" colspan="6" >
						Insertar los datos personales de la persona.<br> Una vez ingresados, haga clic en "Siguiente" para llenar el cuestionario.
					<!--a href="Javascript:Show_Help('/Pgm/help/Formazione/QGradimento/Intermediati/INT_InsDatiUtente')" onmouseover="javascript:window.status=' '; return true"><img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a-->
					</td>
				</tr>
				<tr height="2">
					<!--td colspan="7" class="sfondocomm" background="<%=Session("Progetto")%>/images/separazione.gif">
					</td-->
				</tr>
				</table>
			
	
	
<%
end sub

'------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

sub ImpPagina()
	
    dim sInt 
%><!--form name="frmDomIscri" onsubmit="return ControllaDati(this)" method="post" action="<%=Session("Progetto")%>/pgm/Formazione/QGradimento/Intermediati/INT_CnfDatiUtente.asp"-->
	<form name="frmDomIscri" onsubmit="return ControllaDati(this)" method="post" action="INT_CnfDatiUtente.asp">
		<input type="hidden" name="txtTipoDest" value="P">
		<input type="hidden" name="txtIdProj" value="<%=nIdProgetto%>">		
		<input type="hidden" name="txtIdUtente" value="<%=Session("IdUtente")%>">
		<input type="hidden" name="id" value="<%=sIdQuest%>">
		<input type="hidden" name="mod" value="<%=3%>">
	
	   
		<table width="500" border="0" cellspacing="2" cellpadding="1">
		<tr>
			<td class="textblack" colspan="2">
				Ingresar la CI sin puntos ni guiones por ej. si la CI es 2.788.674-3 se debe ingresar 27886743. 
			</td>
		</tr>

	  	<tr>
			<td align="left" class="tbltext1" width="30%">
				<b>Cedula de Identidad*</b>
	        </td>
			<td align="left">
				<input style="TEXT-TRANSFORM: uppercase;" size="22" maxlength="16" class="textblack" name="txtCodFisc">
	        </td>
	    </tr>
	    <tr>
			<td align="left" class="tbltext1" width="30%">
				<b>Nombre*</b>
			</td>
			<td align="left">
				<input style="TEXT-TRANSFORM: uppercase;" size="30" maxlength="30" class="textblack" name="txtNome">
			</td>
	    </tr>
		<tr>
			<td align="left" class="tbltext1" width="30%">
				<b>Apellido*</b>
			</td>
			<td align="left">
				<input style="TEXT-TRANSFORM: uppercase;" size="30" maxlength="30" class="textblack" name="txtCognome">
			</td>
	    </tr>
	    <tr>
			<td align="left" class="tbltext1" width="30%">
				<b>Fecha de Nacimiento*</b> (dd/mm/aaaa)
			</td>	
			<td align="left">
				<input style="TEXT-TRANSFORM: uppercase;" size="11" maxlength="10" class="textblack" name="txtDataNascita">
			</td>
	    </tr>
		<tr>
			<td align="left" class="tbltext1" width="30%">
				<b>Sexo*</b>
	        </td>
			<td align="left">
				<select class="textblack" id="cmbSesso" name="cmbSesso">
					<option selected>
					<option value="F">FEMENINO
					<option value="M">MASCULINO
					</option>
				</select>
	        </td>
	  	</tr>
	    <tr><td>&nbsp;</td></tr>
	    <tr>
			<td align="left" class="textblack" colspan="2">
				Si naci� en Uruguay, debe ingresar a continuaci�n el departamento de nacimiento.
			</td>
	    </tr>
	    
		<tr>		
			<td align="left" class="tbltext1" width="30%">
				<b>Departamento de Nacimiento</b>
			</td>			
			<td align="left">				
<%
			sInt = "PROV|0|" & date & "| |cmbProvinciaNascita' onchange='PulisciCom()|ORDER BY DESCRIZIONE"			
			CreateCombo(sInt)
						
%>
			</td>
		</tr>		
		<tr>		
			<td align="left" class="tbltext1" width="30%">
				<b>Municipio de Nacimiento</b>
			</td>
			<td nowrap>
				<span class="tbltext">
				<input type="text" name="txtComuneNascita" style="TEXT-TRANSFORM: uppercase;" class="textblack" size="35" readonly value="<%=DescrComuneNascita%>">
				<input type="hidden" id="txtComune" name="txtComune" value="<%=COM_NASC%>">
<%
				NomeForm="frmDomIscri"
				CodiceProvincia="cmbProvinciaNascita"
				NomeComune="txtComuneNascita"
				CodiceComune="txtComune"
				Cap="NO"
%>
				<a href="Javascript:SelComune('<%=NomeForm%>','<%=CodiceProvincia%>','<%=NomeComune%>','<%=CodiceComune%>','<%=Cap%>')" ID="imgPunto1" name="imgPunto1" onmouseover="javascript:window.status='';return true"><img border="0" src="<%=Session("Progetto")%>/images/bullet1.gif"></a>
				</span>				
			</td>
		</tr>		
		<tr>
			<td class="textblack" colspan="2">
				Para <b>los extrangeros</b>, en cambio, deben completar el Pa�s de nacimiento. 
			</td>
		</tr>
		
	    <tr>
			<td align="left" class="tbltext1" width="30%">
				<b>Pa�s de Nacimiento</b>
	        </td>
			<td align="left">
				<%
				set rsComune = Server.CreateObject("ADODB.Recordset")
				sSQL = "SELECT CODCOM,DESCOM from COMUNE WHERE CODCOM like 'Z%' " &_
						"ORDER BY DESCOM"

				rsComune.Open sSQL, CC
				
				%>
				<select class="textblack" ID="cmbNazioneNascita" name="cmbNazioneNascita">
				<option></option>
				<%
				do while not rsComune.EOF 
					Response.Write "<OPTION "

					Response.write "value ='" & rsComune("CODCOM") & _
						"'> " & rsComune("DESCOM")  & "</OPTION>"
					
					rsComune.MoveNext 
				loop

				rsComune.Close	
				%>
				</select>
	        </td>
		</tr>
		<tr><td>&nbsp;</td></tr>
		
		<tr><td>&nbsp;</td></tr>
	    </table>
	     <table cellpadding="0" cellspacing="0" width="100%" border="0">	
		<tr>
			<td align="center">		
			 <!--a onmouseover="javascript:window.status='' ; return true" href="INT_VisFrontGradimento.asp"-->
			 <a onmouseover="javascript:window.status='' ; return true" href="javascript:history.go(-1)">								
					<img src="<%=Session("Progetto")%>/images/Indietro.gif" border="0" >
			</a>
			</td>

			<td align="center">
				<input type="image" name="avanti" src="<%=Session("progetto")%>/images/avanti.gif">
			</td>
		</tr>
	</table>
	<br> 
</form>
		</Td>
	</Tr>	
</Table>
 
<%
end sub

'------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

function ctrlProj()
	ctrlProj = ""
	sSQL = "SELECT COD_TIMPR FROM PROGETTO WHERE ID_PROJ = " & CLng(nIdProgetto)
	set rsProj = CC.Execute(sSQL)
	
	if rsProj.EOF then
		Msgetto("Progetto inesistente")	
		ctrlProj = "KO"
	else
		sCodTimpr = rsProj("COD_TIMPR")
		ctrlProj = "OK"
	end if
	
end function

'------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

sub ctrlBando()

	'	Se la data di sistema non � compresa nel periodo di 
	'   acquisizione domande non devo caricare la domanda.
	set rsAbil = Server.CreateObject("ADODB.Recordset")

	sSQL = " SELECT B.ID_PROJ, B.DESC_BANDO, B.IDGRUPPO, P.IND_GRAD  FROM BANDO B, PROGETTO P " & _
		   "WHERE B.ID_PROJ = P.ID_PROJ "	 &_
			"AND B.ID_BANDO = "& nBando & " AND " & nTime & _
			" BETWEEN B.DT_INI_ACQ_DOM AND B.DT_FIN_ACQ_DOM"
	
	'-----------------------------------------------------------------------------
	' Questo controllo � stato inserito per permettere l'iscrizione di nominativi
	' anche a bando(sport2job) chiuso tale operazione puo essere eseguita solo dagli
	' utenti appartenenti al gruppo ISCR tramite la funzione ISCRIZIONE
	if ucase(nBypassdata) = "SI" then
		sSQL = " SELECT B.ID_PROJ, B.DESC_BANDO, B.IDGRUPPO, P.IND_GRAD  FROM BANDO B, PROGETTO P " & _
			   " WHERE B.ID_PROJ = P.ID_PROJ "	 &_
			   " AND B.ID_BANDO = "& nBando
				
	end if
	'Response.Write "sSQL " & sSQL
    '----------------------------------------------------------------------------
   	rsAbil.Open sSQL, CC, 3

	if  rsAbil.EOF  then
		Msgetto("Periodo de Adquisicion de preguntas on valido")
		sEsitoBando = "KO"
	else
		sDescBando = rsAbil("DESC_BANDO")
	    nIdProgetto = rsAbil("ID_PROJ")
	    sEsitoBando = "OK"
	    sGruppo =rsAbil("IDGRUPPO")
	    sGraduatoria =rsAbil("IND_GRAD")
	end if  
	rsAbil.close
	set rsAbil = nothing	 

end sub

'------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

sub Fine()
%>
<!--#include virtual = "/include/closeconn.asp"-->
<%if session("login") = "" then%>
	<!--#include virtual = "/strutt_coda2.asp"-->
<%else%>
	<!--#include virtual = "/strutt_coda2.asp"-->
<%end if%>		
<%
end sub
%>
<!-- ************** ASP Fine *************** -->

<!-- ************** DAMIAN (passo automatico a QUESTIONARIO) ************ -->
<%
sub ControlPersona()

	sIdpers=Request("Idpers")
	IF sIdpers<>"" then
	   'Hago un select para tomar datos del candidato
	   	sSQL = " SELECT ID_PERSONA, COD_FISC, COGNOME, NOME, SESSO, CONVERT(NVARCHAR(10),DT_NASC,103) AS FECHA FROM PERSONA WHERE ID_PERSONA = '" & sIdpers & "' "
		'Response.Write "sSQL " & sSQL
	    '----------------------------------------------------------------------------
	   	set rsPers = Server.CreateObject("ADODB.Recordset")
	   	rsPers.Open sSQL, CC, 3

		if  not rsPers.EOF  then
			sApellido = rsPers("COGNOME")
			sNombre = rsPers("NOME")
			sFechaNac = rsPers("FECHA")
			sSexo = rsPers("SESSO")
			sCI = rsPers("COD_FISC")
		end if
		
	   Response.Redirect("/pgm/Formazione/QGradimento/QGR_VisAreaScheda.asp?Id=" & Request("txtIdQuest") & "&txtIdPers=" & sIdpers & "&Mod=3&txtTipoDest=" & sTipoDest) 
	   'Hago el include de los bloques
	   %>
	    <br>
		<!--#include virtual="/ba/pgm/iscr_utente/menu.asp"-->
		<br>
		<script LANGUAGE="Javascript">

		function Pulisci()
		{
			document.frmDomIscri.txtCognome.value = "<%=sApellido%>";
			document.frmDomIscri.txtNome.value = "<%=sNombre%>";
			document.frmDomIscri.DataNascita.value = "<%=sFechaNac%>";
			document.frmDomIscri.cmbSesso.value = "<%=sSexo%>";
			document.frmDomIscri.txtCodFisc.value = "<%=sCI%>";
			document.frmDomIscri.submit();
		}

		</script>
	   <%
	end if 
end sub %>
<!-- ************** fin DAMIAN ************ -->



<!-- ************** MAIN Inizio ************ -->
<%

dim sDataIsc
dim sSQL
dim CnConn
dim rsAbil
dim rsPers

dim nBando
dim nIdProgetto
dim sDescBando
dim sEsitoBando

dim sCodTimpr
dim sDescTimpr

dim sGraduatoria
dim sGruppo, sCommento
dim sCondizione, sCategoria, sDisabilita

dim sProv, nCont , sCodice, sDescSvantaggio
dim sIdQuest, sIdUtente, sTipoDest

sDataIsc = Now()

sTipoDest = UCase(Request("txtTipoDest"))
sIdUtente = Request("txtIdUtente")

sIdQuest = Request("txtIdQuest")

'sTipoPers = Request("txtTipoPers")
'08/05/2007 tolto controllo progetto
'nIdProgetto = Request("txtIdProj")
'nBando=clng(Request("txtIdBando"))
'nTime = convdatetodb(Now())
nTime = convdatetodb(Date)
nBypassdata = Request("Bypassdata")

'inizio am 25/01/07
		Dim aMenu(0)
		'aMenu(0) = Session("Progetto") & "/homeITES.asp"
		aMenu(0) = Session("Progetto") & "/"		
		Session("Indice") = aMenu
		Session("Menu") = aMenu(0)
		nPos = 0
	'fine am 25/01/07	

'ctrlBando()
'if  sEsitoBando = "OK" then
    ControlPersona()    
    
    Inizio()

	
'08/05/2007 tolto controllo progetto
'	if Trim(ctrlProj()) = "OK" then
'		sDescTimpr = DecCodVal("TIMPR", "0", "", sCodTimpr, 1)  
'		sDataSis = ConvDateToString(Now())
		ImpPagina()
'	end if
'end if

Fine()
%>
<!-- ************** MAIN Fine ************ -->