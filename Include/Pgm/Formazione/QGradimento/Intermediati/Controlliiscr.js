
var windowArea

function SelImpresa(sCdTimp,sDesCdTimp,sArea,sCampo,sNomeCampo,sNomeCampo2,nBando){
		windowArea = window.open ('SEL_SelImpresa.asp?Timpr=' + sCdTimp + '&DescTimpr=' + sDesCdTimp + '&Area=' + sArea + '&sTxtArea=' + sCampo +'&NomeCampo=' + sNomeCampo +'&NomeCampo2=' + sNomeCampo2+'&nBando='+ nBando,'Info','width=425,height=450,Resize=No,Scrollbars=yes');	
}

function PulisciCom()
{
	document.frmDomIscri.txtComuneNascita.value = ""
	document.frmDomIscri.txtComune.value = ""
}

function PulisciRes()
{
	document.frmDomIscri.txtComuneRes.value = ""
	document.frmDomIscri.txtComRes.value = ""
	document.frmDomIscri.txtCapRes.value = ""
}




//Funzione per i controlli dei campi da inserire 

//-------- Controlli di OBBLIGATORIETA' --------
//-------- Controlli FORMALI            --------
//-------- Controlli di RELAZIONE       --------

	function ControllaDati(frmDomIscri){
	   
        //Legge privacy
/*        if (windowArea != null ) 
	    {
		windowArea.close(); 
	    }

        if (frmDomIscri.radConsenso[1].checked) 
	    {	
		f = '/PLAVORO/Pgm/Iscr_Utente/UTE_Leg_Privacy.asp';
		windowArea=window.open(f, '','width=520,height=300,left=250,top=250,Resize=no,scrolling=no');
		window.event.returnValue = false;
		return false
	    }
*/
		//Cognome
		frmDomIscri.txtCognome.value=TRIM(frmDomIscri.txtCognome.value)
		if (frmDomIscri.txtCognome.value == ""){
			alert("Cognome obbligatorio");
			frmDomIscri.txtCognome.focus();
			return false
		}
		//Nome
		frmDomIscri.txtNome.value=TRIM(frmDomIscri.txtNome.value)
		if (frmDomIscri.txtNome.value == ""){
			alert("Nome obbligatorio"),
			frmDomIscri.txtNome.focus(); 
			return false
		}
		//Data di Nascita
		DataNascita = TRIM(frmDomIscri.txtDataNascita.value)
		if (DataNascita == ""){
			alert("Data di Nascita obbligatoria");
			frmDomIscri.txtDataNascita.focus();
			return false
		}
		//Controllo della validit� della Data di Nascita
		if (!ValidateInputDate(DataNascita)){
			frmDomIscri.txtDataNascita.focus(); 
			return false
		}
		//Comune di Nascita e Provincia di Nascita obbligatori
		//e in alternativa con Nazione di Nascita
		frmDomIscri.txtComuneNascita.value=TRIM(frmDomIscri.txtComuneNascita.value)
		if (frmDomIscri.txtComuneNascita.value == ""){ 
			if (frmDomIscri.cmbProvinciaNascita.value == ""){
				if (frmDomIscri.cmbNazioneNascita.value == ""){
					alert("Comune e Provincia di Nascita o Nazione di Nascita obbligatori");
					frmDomIscri.cmbProvinciaNascita.focus();
					return false
				}
			}
		}
       
		if (frmDomIscri.txtComuneNascita.value != ""){ 
			if (frmDomIscri.cmbProvinciaNascita.value != ""){
				if(frmDomIscri.cmbNazioneNascita.value != ""){
					alert("Indicare solo il Comune e la Provincia di Nascita oppure la Nazione di Nascita");
					frmDomIscri.cmbNazioneNascita.focus(); 
					return false
				}
			}	
		}
		//Se Comune di Nascita � digitato, la Provincia � obbligatoria
		if (frmDomIscri.txtComuneNascita.value != ""){
			if (frmDomIscri.cmbProvinciaNascita.value == ""){
				alert("Provincia di Nascita obbligatoria");
				frmDomIscri.cmbProvinciaNascita.focus();
				return false
			}
		}
		//Se la Provincia di Nascita � digitata, il Comune � obbligatorio
		if  (frmDomIscri.txtComuneNascita.value == ""){
			if (frmDomIscri.cmbProvinciaNascita.value != ""){
				alert("Comune di Nascita obbligatorio");
				frmDomIscri.txtComuneNascita.focus();
				return false
			}
		}
		//Sesso
		if (frmDomIscri.cmbSesso.value == ""){
			alert("Sesso obbligatorio");
			frmDomIscri.cmbSesso.focus(); 
			return false
		}
		//Codice Fiscale
		frmDomIscri.txtCodFisc.value = TRIM(frmDomIscri.txtCodFisc.value)
		if (frmDomIscri.txtCodFisc.value == ""){
			alert("Codice Fiscale obbligatorio");
			frmDomIscri.txtCodFisc.focus(); 
			return false
		}
		//Codice Fiscale deve essere di 16 caratteri
/*		if (frmDomIscri.txtCodFisc.value.length != 16){
			alert("Formato del Codice Fiscale errato");
			frmDomIscri.txtCodFisc.focus(); 
			return false
		}
*/		//Contollo la validit� del Codice Fiscale
/*		var contrCom=""
		DataNascita = frmDomIscri.txtDataNascita.value;
		CodFisc = frmDomIscri.txtCodFisc.value.toUpperCase();
		Sesso = frmDomIscri.cmbSesso.value;
		
		if (frmDomIscri.txtComuneNascita.value != ""){
		          contrCom =frmDomIscri.txtComune.value;   
		}
		else{
		          contrCom = frmDomIscri.cmbNazioneNascita.value;
		}
		
		if (!ControllCodFisc(DataNascita,CodFisc,Sesso,contrCom)){
			alert("Codice Fiscale Errato");
			frmDomIscri.txtCodFisc.focus(); 
			return false
		}
*/		//Cittadinanza
/*		if (frmDomIscri.cmbCittadinanza.value == ""){
			alert("Cittadinanza obbligatoria");
			frmDomIscri.cmbCittadinanza.focus();
			return false
		}
*/ 
				
	return true

	}


