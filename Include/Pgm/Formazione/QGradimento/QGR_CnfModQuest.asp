<%
Sub Inizio
	dim sFunzione
	dim sTitolo
	dim sCommento
	dim bCampiObbl
	
	sFunzione = "GESTION CUESTIONARIOS"
	sTitolo = "MODIFICACION DE LOS CUESTIONARIOS"
	sCommento = "Confirmaci�n de la modificaci�n del Cuestionario"
	bCampiObbl = false
%>
	<!--#include virtual="include/SetTestata.asp"-->
<%
End Sub
'------------------------------------------------------------------------
Sub Conferma()

dim nIdQuest, sDescQuest, sTipoQuest, sIntestQuest, sModExec, sCodSess,sRipetibile, dDtTmst
dim sErrore
dim sSQL

nIdQuest = request("IdQuest")

'sDescQuest = Ucase(strHTMLEncode(Trim(Request.Form("DescQuest"))))
sDescQuest = strHTMLEncode(Trim(Request.Form("DescQuest")))
sDescQuest = replace(sDescQuest,"'","''")

'sTipoQuest vuoto perche non pi� richiesto
sTipoQuest = ""

'sIntestQuest = Ucase(strHTMLEncode(Trim(Request.Form("IntQuest"))))
sIntestQuest = strHTMLEncode(Trim(Request.Form("IntQuest")))
sIntestQuest = replace(sIntestQuest,"'","''")

'sModExec vuoto perche non pi� richiesto
sModExec = ""

sCodSess = Request.Form("CodSessione")
sRipetibile=Request.Form("ckRipetibile")
dDtTmst = Request.Form("dtTmst")

if sRipetibile <> 1 then 
    sRipetibile= 0
end if

'Response.Write "-->" & dDtTmst & "<--"

Adesso = ConvDateToDB(now())

sSQL =  "UPDATE info_quest SET " &_
		"desc_quest = '" & Trim(sDescQuest) & "', " &_
		"intest_quest = '" & Trim(sIntestQuest) & "', " &_
		"fl_repeat = " & sRipetibile & "," &_
		"dt_tmst = " & Adesso &_
		" where id_infoquest = " & nIdQuest
		
'*** 02/07/03 Asteriscata parte relativa al campo COD_RUOLO (ORIGINARIAMENTE MESSO PRIMA DELLA WHERE)		
'	"cod_ruolo = '" & sCodRuolo & "', " &_

sErrore = Esegui(nIdQuest ,"Info_Quest",Session("persona"),"MOD",sSQL,1,dDtTmst)		

'if sErrore="0" then
'	sSQL="Select ID_AREAIQ FROM AREAIQ WHERE ID_INFOIQ=" & nIdQuest
'
'	sErrore = EseguiNoC(sSQL,cc)		

'	sSQL="DELETE FROM IQ_AREA WHERE ID_INFOIQ=" & nIdQuest
'	sErrore = EseguiNoC(sSQL,cc)		
'end if

'if sErrore="0" then
'	 sArea = Request.Form("txtAree")
	 'Response.Write "sArea = " & sArea & "<br>"
'   if sArea <> "" then
'      for i = 1 to sArea
'			DescA = strHTMLEncode(Trim(Request.Form("DescArea" & i)))
'			DescA = replace(DescA,"'","''")

'			Adesso="TO_DATE('" & day(now) & "/" & _
'					   month(now) & "/" & year(now) & _
'					   " " & hour(now) & ":" & minute(now) & ":" & second(now) & _
'					   "','DD/MM/YYYY HH24:MI:SS')"	

'			sSQL = "INSERT INTO IQ_AREA (DESC_AREAIQ, ID_INFOIQ, DT_TMST) " &_
'			       "VALUES ('" & Ucase(DescA) & "', " & nIdQuest & ", " & Adesso & ")"

'			sErrore = Esegui(nIdQuest ,"IQ_AREA",Session("persona"),"INS",sSQL,1,dDtTmst)
'		next
 '   end if		
'end if

if sErrore="0" then
%>
<br><br>
<table border=0 cellspacing=2 cellpadding=1 width='500'>
	<tr align=middle>
		<td class='tbltext3'>
			Modificaci�n del cuestionario '<%=sDescQuest%>' correctamente efectuada
		</td>
	</tr>
</table>
<%
else
%>
<br><br>
<table border=0 cellspacing=2 cellpadding=1 width='500'>
	<tr align=middle>
		<td class='tbltext3'>
			Error en la modificaci�n. <%=sErrore%>
		</td>
	</tr>
</table>
<%
end if
%>
<br>
<br>
<table width=500>
	<tr>
		<td align=center>
			<%
			PlsLinkRosso "QGR_VisQuestionario.asp", "Listado de Cuestionarios"
			%>
		</td>
	</tr>
</table>
<br><br>
<%
End Sub
'------------------------------------------------------------------------
'M A I N
%>
<!--#include virtual="/strutt_testa2.asp"-->
<!--#include virtual="/include/openconn.asp"-->
<!--#include virtual="/Util/DBUtil.asp"-->
<!--#include virtual="/Include/HTMLEncode.asp"-->
<!--#include virtual="/Include/SysFunction.asp"-->
<!--#include virtual="include/SetPulsanti.asp"-->
<%	
	Inizio()
	Conferma()
%>
<!--#include virtual="/include/closeconn.asp"-->
<!--#include virtual="/strutt_coda2.asp"-->
