<%
Sub Inizio 

	dim sFunzione
	dim sTitolo
	dim sCommento
	dim bCampiObbl
	
	sFunzione = "GESTION CUESTIONARIOS"
	sTitolo = "MODIFICACION DE LA RESPUESTA"
	sCommento = "Confirmación de la modificación de la Respuesta"
	bCampiObbl = false
%>
	<!--#include virtual="include/SetTestata.asp"-->
<%
End Sub
'--------------------------------------------------------------------------
Sub Conferma()

	dim nIdRisp, nIdArea, nIdQuest, nIdDom
	dim dDtTmst, sRisp, nVMin, nVMax
	dim sSQL
    
    nIdQuest= clng(Request.Form("IdQuest"))
	nIdArea = clng(Request.Form("IdArea"))
	nIdDom	= clng(Request.Form("IdDom"))
	nIdRisp = clng(Request.Form("nIdR"))
    
    ssql="select dt_tmst from iq_risposta  WHERE id_rispostaiq = " & nIdRisp
    set rsTime = cc.execute(ssql)
    if  not rsTime.eof then
       dDtTmst = rsTime("dt_tmst")
    end if
    
	'Prendo i valori del tipo_domanda da DIZ_DATI
	sSQL =	"SELECT id_campo_desc FROM diz_dati WHERE id_tab='IQ_DOMANDA'" &_
			" AND id_campo='TIPO_DOMANDA'"
	SET rsDdati = CC.Execute(sSQL)
		sTipDom = rsDdati("id_campo_desc")
		aTipDom = split(sTipDom,"|")
	rsDdati.close
	set rsDdati = nothing
			   
	CC.BeginTrans
	
	If sTipo = "V" then 
		nVMin = clng(Request.Form("VMin"))
		nVMax = clng(Request.Form("VMax"))
		sTxtMin = strHTMLEncode(Trim(Request.Form("txtMin")))
		sTxtMin = replace(sTxtMin,"'","''")
		sTxtMax = strHTMLEncode(Trim(Request.Form("txtMax")))
		sTxtMax = replace(sTxtMax,"'","''")
	'	dDtTmst = Request.Form("dTMST")
		
		sSQL =	"UPDATE iq_risposta SET" &_
				" iq_min=" & nVMin & ", iq_max=" & nVMax & "," &_
				" testo_min='" & sTxtMin & "', testo_max='" & sTxtMax & "'," &_
				" dt_tmst = " & convdatetodb(Now()) & " WHERE id_rispostaiq = " & nIdRisp
	Else
		sFlTipoRisp = Request.Form("rFlTipoRisp")
		sTxtRisp	= strHTMLEncode(Trim(Request.Form("txtRisposta")))
		sTxtRisp	= replace(sTxtRisp,"'","''")
	'	dDtTmst = Request.Form("dTmstT")
		
		if sTxtRisp = "&nbsp;" then
			sTxtRisp = ""
		end if
		
		sSQL =	"UPDATE iq_risposta SET" &_
				" txt_risposta='" & sTxtRisp & "', fl_tiprisp='" & sFlTipoRisp & "'," &_
				" dt_tmst = " & convdatetodb(Now()) & " WHERE id_rispostaiq = " & nIdRisp
	End if
	'sErrore = Esegui(nIdRisp,"IQ_RISPOSTA",Session("persona"),"MOD",sSQL,0,dDtTmst)
	'sErrore = EseguiNoC(sSQL,CC) 
	'sErrore= EseguiNoCTrace("",nIdRisp,"iq_risposta",Session("persona"),"","","MOD",sSQL,0,dDtTmst,CC)
	sErrore= EseguiNoCTrace(Mid(Session("progetto"),2),nIdRisp,"iq_risposta",Session("idutente"),session("idutente"),"P","MOD",sSQL,0,dDtTmst,CC)

	
	IF sTipo = "V" THEN
		Inizio()
		if sErrore="0" then
			CC.CommitTrans %>
			<br><br>
			<table border=0 cellspacing=2 cellpadding=1 width='500'>
				<tr align=middle>
					<td class='tbltext3'>
						Modificación de la respuesta correctamente efectuada.
					</td>
				</tr>
			</table>
<%		else 
			CC.RollBackTrans%>
			<br><br>
			<table border=0 cellspacing=2 cellpadding=1 width='500'>
				<tr align=middle>
					<td class='tbltext3'>
						Error en la modificación. <%=sErrore%>
					</td>
				</tr>
			</table>
<%		end if %>
		<br><br>
<%'''9/7/2003: PASSATI VALORI TRAMITE FORM ANZICHE' QUERYSTRING %>
	<form name="frmDomanda" method="post" action="QGR_VisDomanda.asp">
		<input type="hidden" name="idq" value="<%=nIdQuest%>">
		<input type="hidden" name="ida" value="<%=nIdArea%>">
	</form>
<%'''FINE 9/7/2003%>
		<table width=500>
			<tr>
				<td align=center>
<%'''9/7/2003: PASSATI VALORI TRAMITE FORM ANZICHE' QUERYSTRING 
					'''PlsLinkRosso "QGR_VisDomanda.asp?ida=" & nIdArea & "&idq=" & nIdQuest, "Elenco delle Domande" %>
					<a onmouseover="javascript:window.status='' ; return true" href="javascript:frmDomanda.submit();" class="textred">
						<b>CIERRA</b>
						<SCRIPT LANGUAGE="JAVASCRIPT">
							opener.document.frmReload.submit();
							self.close();
						</SCRIPT>
					</a>					
				</td>
			</tr>
		</table>
		<br><br>
<%	ELSE 
		if sErrore="0" then
			nRispCh = 0
			nRispAp = 0
			sql	= "SELECT fl_tiprisp FROM iq_risposta WHERE id_domandaiq=" & nIdDom
			set rs	= CC.execute(sql)
				Do While not rs.EOF
					if rs("fl_tiprisp") = "C" then
						nRispCh = nRispCh + 1
					else
						nRispAp = nRispAp +1
					end if
				rs.movenext
				Loop
			rs.close
			set rs=nothing
			
			if nRispCh = 0 then
				'sTipo_domanda = "A"
				sTipo_domanda = aTipDom(0)
			elseif nRispAp = 0 then
				'sTipo_domanda = "C"
				sTipo_domanda = aTipDom(2)
			elseif nRispCh > 0 and nRispAp > 0 then
				'sTipo_domanda = "S"
				sTipo_domanda = aTipDom(4)
			end if
			
			sql= "UPDATE iq_domanda SET tipo_domanda='" & sTipo_domanda & "' WHERE id_domandaiq =" & nIdDom
			sErrore = EseguiNoC(sql,CC)
			if sErrore = "0" then
				CC.CommitTrans %>
				<SCRIPT LANGUAGE="JAVASCRIPT">
					opener.document.frmReload.submit();
					self.close();
				</SCRIPT>
<%			else
				CC.RollbackTrans%>
				<form name="frmErrore" id="frmErrore" method="post" action="QGR_ModRisposta.asp">
					<input type="hidden" name="txtErrore" id="txtErrore" value="<%=sErrore%>">
					<input type="hidden" name="nId" id="nId" value="<%=nIdRisp%>">
				</form>	
				<SCRIPT LANGUAGE="JAVASCRIPT">
					frmErrore.submit();
				</SCRIPT>

<%			end if
		else 
			CC.RollbackTrans%>
			<form name="frmErrore" id="frmErrore" method="post" action="QGR_ModRisposta.asp">
				<input type="hidden" name="txtErrore" id="txtErrore" value="<%=sErrore%>">
				<input type="hidden" name="nId" id="nId" value="<%=nIdRisp%>">
			</form>	
			<SCRIPT LANGUAGE="JAVASCRIPT">
				frmErrore.submit();
			</SCRIPT>
<%		end if 
	END IF%>
	
	
<%
End Sub
'------------------------------------------------------------------------
'M A I N
dim sTipo

	sTipo = Request.Form("rTipo")
	
	%>
	<!--#include virtual="/include/openconn.asp"-->
	<!--#include virtual="/Util/DBUtil.asp"-->	
	<!--#include virtual="/Include/SysFunction.asp"-->
	<!--#include virtual="/Include/HTMLEncode.asp"-->
	<!--#include virtual="include/SetPulsanti.asp"-->
	<%
	If sTipo = "V" then %>
		<html>
			<head>
			<title><%=Session("TIT")%> [ <%=Session("login")%> ] </title>
			<link REL="STYLESHEET" TYPE="text/css" HREF="<%=session("progetto")%>/fogliostile.css">
			</head>
			<body class="sfondocentro" topmargin="2" leftmargin="0">
			<center>
<%	End if

	Conferma()
	
	If sTipo = "V" then %>
		<!--#include virtual="/include/closeconn.asp"-->
		<!--#include virtual="/strutt_coda2.asp"-->
<%	Else %>
		<!--#include virtual="/include/closeconn.asp"-->
<%	End if%>

</body></html>
