<%
Sub ControlliJavascript()
%>
<SCRIPT LANGUAGE="Javascript">
<!--#include virtual="include/ControlString.inc"-->

function ControllaCampi(frmInsArea)	{
	frmInsArea.DescArea.value = TRIM(frmInsArea.DescArea.value)
	if (frmInsArea.DescArea.value == "") {
		alert("Ingresar la 'Descripcion de la Seccion'")
		frmInsArea.DescArea.focus() 
		return false
	}	
}
</SCRIPT>
<%
End Sub
'-----------------------------------------------------------------------
Sub Inizio()
	dim sFunzione
	dim sTitolo
	dim sCommento
	dim bCampiObbl
	
	sFunzione = "GESTION DE CUESTIONARIOS"
	sTitolo = "INGRESO DE LA SECCION"
	sCommento = "Ingresar el nombre de la secci�n, relacionada al Cuestionario."
	bCampiObbl = true
	sHelp = "/Pgm/help/Formazione/QGradimento/QGR_InsArea/"	
%>
	<!--#include virtual="include/SetTestata.asp"-->
<%
End Sub
'-----------------------------------------------------------------------
Sub Inserimento()

dim sSQL, rsTades
dim nIdQuest

nIdQuest = request("idq")
'Response.Write "nIdQuest = " & nIdQuest & "<br>"
%>

<form method=post action="QGR_CnfInsArea.asp" onsubmit="return ControllaCampi(this)" id=frmInsArea name=frmInsArea>
<table border=0 width=500> 
	<TR class=tbltext>
		<TD>
			<b>Descripci�n de la Secci�n*</b>
		</TD>
		<TD>
			<INPUT id=DescArea maxLength=255 size="40" name=DescArea class=textblacka> 
			<INPUT type=hidden id=IdQuest name=IdQuest value=<%=nIdQuest%>>
		</TD>
	</TR>
</table>
<br>	
<table border=0 width=500>
	<TR>
		<td align=middle>
			<%
			PlsIndietro()
			PlsInvia("Conferma")
			%>
		</td>
	</TR>
</table>
</form>
<%
End Sub
'-----------------------------------------------------------------------
'M A I N
%>
<!--#include virtual="/strutt_testa2.asp"-->
<!--#include virtual="/include/openconn.asp"-->
<!--#include virtual="include/SetPulsanti.asp"-->

<%
if ValidateService(session("idutente"),"QGR_VisQuestionario", CC) <> "true" then 
	response.redirect "/util/error_login.asp"
end if
	
	ControlliJavaScript()
	Inizio()
	Inserimento()
%>
<!--#include virtual="/include/closeconn.asp"-->
<!--#include virtual="/strutt_coda2.asp"-->
