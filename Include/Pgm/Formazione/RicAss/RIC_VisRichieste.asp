<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%
Sub GestioneDivJS()
%>
<script language="javascript">
function CambiaDIV(dSource, dTarget)
{
	eval('document.all.'+dSource+'.style.visibility="hidden"')
	eval('document.all.'+dTarget+'.style.visibility="visible"')
}
var finestra
function VisRic(nIdR)
{
	if (finestra != null ) 
	{
		finestra.close(); 
	}
	f='RIC_VisRispRicAss.asp?idR=' + nIdR
	finestra=window.open(f,"","width=560,height=450,location=no,menubar=no,scrollbars=yes,resizable=no")
}

function Carica_PaginaTU(npagina)
{
	document.frmPaginaTU.paginaTU.value = npagina;
	document.frmPaginaTU.submit();
}

function Carica_PaginaTS(npagina)
{
	document.frmPaginaTS.paginaTS.value = npagina;
	document.frmPaginaTS.submit();
}

function Carica_PaginaME(npagina)
{
	document.frmPaginaME.paginaME.value = npagina;
	document.frmPaginaME.submit();
}

function Carica_PaginaES(npagina)
{
	document.frmPaginaES.paginaES.value = npagina;
	document.frmPaginaES.submit();
}


</script>
<%
End Sub
';););););););););););););););););););););););););););););););););););););););)
Sub Inizio()
	dim sFunzione
	dim sTitolo
	dim sCommento
	dim bCampiObbl
	
	sFunzione = "RICHIESTA DI ASSISTENZA"
	sTitolo = "ASSISTENZA"
	'sCommento = "Commento... Ric Ass<br>Versione con le DIV"
	sCommento = "In questa pagina sono visualizzate le richieste da te inoltrate " &_
				"al tuo tutor, al tuo mentor o agli esperti delle aree tematiche " &_
				"dei corsi che stai frequentando. Scegli l'assistente al quale " &_
				"vuoi inoltrare una richiesta di assistenza e fai click su " &_
				chr(34) & "<b>Nuova Richiesta</b>" & chr(34) & ""
	bCampiObbl = false
	sHelp = "/Pgm/Help/Formazione/RicAss/RIC_VisRichieste"
%>
	<!--#include virtual="/include/SetTestata.asp"-->
<%
End Sub
';););););););););););););););););););););););););););););););););););););););)
Function DecodArea(nArea)
Dim sSQL, rsArea
if isnull(nArea) then exit function
sSQL = " select DESCRIZIONEAREA from area where ID_AREA = " & nArea
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
set rsArea = CC.execute(sSQL)
if rsArea.eof then
	DecodArea = "Area non disponibile"
else
	DecodArea = rsArea("DESCRIZIONEAREA")
end if
rsArea.close
set rsArea = nothing

End Function
';););););););););););););););););););););););););););););););););););););););)
Sub TabellaRichieste(sCodRuo)

Record=0
nTamPagina=5

nPagTU = Request.Form("paginaTU")
if not(nPagTU > "") then
	nPagTU = 1
end if
nPagTS = Request.Form("paginaTS")
if not(nPagTS > "") then
	nPagTS = 1
end if
nPagME = Request.Form("paginaME")
if not(nPagME > "") then
	nPagME = 1
end if	
nPagES = Request.Form("paginaES")
if not(nPagES > "") then
	nPagES = 1
end if
nPagTU = clng(nPagTU)
nPagTS = clng(nPagTS)
nPagME = clng(nPagME)
nPagES = clng(nPagES)
select case sCodRuo
	case "TU"
		nActPagina = nPagTU
	case "TS"
		nActPagina = nPagTS	
	case "ME"
		nActPagina = nPagME
	case "ES"
		nActPagina = nPagES
end select 

sSQL = "SELECT id_ric_ass, cod_ruolo, cod_area, destinatario, oggetto, testo_dom, dt_invio, " &_
		"dt_risposta, testo_risp, dt_tmst FROM RIC_ASS " &_
		"WHERE idutente = " & session("idutente") & " and cod_ruolo = '" & sCodRuo & "' " &_
		"ORDER BY dt_risposta, cod_ruolo, dt_invio desc"
'Response.Write sSQL
Set rsRicAss = Server.CreateObject("ADODB.Recordset")
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
rsRicAss.Open sSQL, CC, 3
if rsRicAss.EOF then
	%>
	<table border="0" width="500" cellpadding="0" cellspacing="0" class="tabOn">
		<tr><td>&nbsp;</td></tr>
		<tr><td>&nbsp;</td></tr>
		<tr align="center">
	   		<td class="tbltext3"> 
	   			Non sono presenti Richieste di Assistenza
	   		</td>
		</tr>
		<tr><td>&nbsp;</td></tr>
		<tr><td>&nbsp;</td></tr>
	</table>
	<br>
	<%
else
	rsRicAss.PageSize = nTamPagina
	rsRicAss.CacheSize = nTamPagina	

	nTotPagina = rsRicAss.PageCount		
	If nActPagina < 1 Then
		nActPagina = 1
	End If
	If nActPagina > nTotPagina Then
		nActPagina = nTotPagina
	End If

	rsRicAss.AbsolutePage=nActPagina
	nTotRecord=0

	%>
	<table border="0" width="500" cellpadding="1" cellspacing="2">
		<tr class="sfondocomm" height="23">
			<td width="10"><b>#</b></td>
			<td width="170"><b>Oggetto</b></td>
			<td width="100"><b>Area</b></td>
			<td width="60"><b>Data Invio</b></td>
			<td width="60"><b>Risposta</b></td>
		</tr>
	<%
	nProg = 1
	do while not rsRicAss.EOF And nTotRecord < nTamPagina
	%>
		<tr class="tblsfondo">
			<td class="tblDett" width="10">
				<%=nProg+((nActPagina*nTamPagina)-nTamPagina)%>
			</td>
			<td class="tblDett" width="170">
				<%=strHTMLEncode(rsRicAss("Oggetto"))%>
			</td>
			<td class="tblDett" width="100">
				<%
				'DecCodVal("AREAT", 0, date, rsRicAss("cod_area"),1)
				Response.Write (DecodArea(rsRicAss("cod_area")))
				%>
			</td>
			<td class="tblDett" width="60">
				<%=ConvDateToString(rsRicAss("dt_invio"))%>
			</td>
			<td width="60" align="center">
				<%
				if rsRicAss("dt_risposta") > "" then
				%>
					<a href="javascript:VisRic(<%=rsRicAss("id_ric_ass")%>)" class="tblagg">
						<img src="<%=session("progetto")%>/images/Formazione/okv.gif" border="0" alt="Clicca per visualizzare la risposta" WIDTH="17" HEIGHT="15">
					</a>
				<%
				else
				%>
					<a href="javascript:VisRic(<%=rsRicAss("id_ric_ass")%>)" class="tblagg">
						<img src="<%=session("progetto")%>/images/formazione/ko.gif" border="0" alt="In attesa di risposta" WIDTH="17" HEIGHT="15">
					</a>
				<%
				end if
				%>
			</td>
		</tr>
	<%
		nProg = nProg + 1
		nTotRecord=nTotRecord + 1
		rsRicAss.MoveNext
	loop
	%>
	</table>
	<br>
	<table border="0" width="500" align="center">
		<tr>
	<%
	if nActPagina > 1 then
	%>
			<td align="right" width="480">
				<a HREF="javascript:Carica_Pagina<%=sCodRuo%>(<%=nActPagina-1%>)">
					<img border="0" src="<%=session("progetto")%>/images/precedente.gif">
				</a>
			</td>
	<%
	end if
	if nActPagina < nTotPagina then
	%>
			<td align="right">
				<a HREF="javascript:Carica_Pagina<%=sCodRuo%>(<%=nActPagina+1%>)">
					<img border="0" src="<%=session("progetto")%>/images/successivo.gif">
				</a>
			</td>
	<%
	end if
	%>
		</tr>
	</table>
<%
end if
rsRicAss.Close
NuovaRichiesta(sCodRuo)
%>
	<form name="frmPagina<%=sCodRuo%>" method="post" action="RIC_VisRichieste.asp">
		<input type="hidden" name="paginaTU" id="paginaTU" value="<%=nPagTU%>">
		<input type="hidden" name="paginaTS" id="paginaTS" value="<%=nPagTS%>">
		<input type="hidden" name="paginaME" id="paginaME" value="<%=nPagME%>">
		<input type="hidden" name="paginaES" id="paginaES" value="<%=nPagES%>">
		<!--input type="hidden" name="pagina" id="pagina" value-->
		<input type="hidden" name="codruo" id="codruo" value="<%="Vis" & sCodRuo%>">
	</form>
	<%
End Sub
';););););););););););););););););););););););););););););););););););););););)
Sub NuovaRichiesta(sCodRuo)
if sCodRuo = "ES" or CercaDest(sCodRuo) <> "" then
%>
	<table border="0" width="500" cellpadding="0" cellspacing="0">
		<tr align="center">
			<td>
				<%
	   			Call PlsLinkRosso ("javascript:frmDest" & sCodRuo & ".submit()", "Nuova Richiesta")
	   			%>
	   		</td>
		</tr>
	</table>
	<br>
	<form name="frmDest<%=sCodRuo%>" id="frmDest<%=sCodRuo%>" method="POST" action="RIC_InsRicAss.asp">
		<input type="hidden" name="hDest" id="hDest" value="<%=sCodRuo%>">
	</form>	
<%
end if
End Sub
';););););););););););););););););););););););););););););););););););););););)
Function CercaDest(Destinatario)
dim rsDest, sSQL1

if Destinatario = "TS" then
    sSQL1 = "SELECT ID_TUTOR FROM PERS_STAGE WHERE ID_PERSONA=" & SESSION("CREATOR")
else
	sSQL1 = "SELECT G.IDUTENTE FROM ASSISTENZAGRUPPO G, ASSISTENZAUTENTE U " &_
			"WHERE G.ID_GRUTASS = U.ID_GRUTASS AND G.STATO_ASS = 'A' AND " &_
			"U.IDUTENTE = " & Session("idutente") & " AND " &_
			"G.COD_RUOLO = '" & Destinatario & "'"
end if

Set rsDest = Server.CreateObject("ADODB.Recordset")
'PL-SQL * T-SQL  
SSQL1 = TransformPLSQLToTSQL (SSQL1) 
rsDest.Open sSQL1, CC, 3
if rsDest.EOF then
	CercaDest = ""
else
    if Destinatario = "TS" then
		CercaDest = rsDest("ID_TUTOR")
	ELSE
		CercaDest = rsDest("IDUTENTE")
	end if
end if
rsDest.Close
set rsDest = nothing

End Function
';););););););););););););););););););););););););););););););););););););););)
Sub ImpostaPag()
dim sSQL, rsRicAss
dim nProg, sOldCodRuo,sContr
dim sCR
sCR = Request.Form("codruo")

sqlContr="select ind_sta from progetto where area_web ='" & mid(session("progetto"),2) & "'"
'PL-SQL * T-SQL  
SQLCONTR = TransformPLSQLToTSQL (SQLCONTR) 
set rsContr = cc.execute(sqlContr)
sContr = rsContr("ind_sta")
set rsContr = nothing
%>
<table width="500">
<tr><td>
<div id="VisTU" name="VisTU" style="position:absolute; visibility:visible;">
	<table width="500" cellpadding="0" cellspacing="0">
		<tr>
			<td class="tabOn" width="20%">
				<b>&nbsp;Tutor</b>
				<%if CercaDest("TU") = "" then%>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<img src="<%=session("progetto")%>/images/formazione/accesso.gif" border="0" WIDTH="12" HEIGHT="12">
				<%end if%>
			</td>
			<td class="tabOn" align="left" valign="top">
				<img src="<%=session("progetto")%>/images/formazione/angaltodx.gif" border="0" WIDTH="17" HEIGHT="21">
			</td>
<%			if sContr="S" then %>
			<td class="tabOff" width="20%">
				&nbsp;<a href="Javascript:CambiaDIV('VisTU', 'VisTS')" class="tabOff">Tutor stage</a>
			</td>	
			<td class="tabOff" align="left" valign="top">
				<img src="<%=session("progetto")%>/images/formazione/angaltodx.gif" border="0" WIDTH="17" HEIGHT="21">
			</td>
<%          end if  %>					
			<td class="tabOff" width="20%">
				&nbsp;<a href="Javascript:CambiaDIV('VisTU', 'VisME')" class="tabOff">Mentor</a>
			</td>
			<td class="tabOff" align="left" valign="top">
				<img src="<%=session("progetto")%>/images/formazione/angaltodx.gif" border="0" WIDTH="17" HEIGHT="21">
			</td>			
			<td class="tabOff" width="20%">
				&nbsp;<a href="Javascript:CambiaDIV('VisTU', 'VisES')" class="tabOff">Esperto</a>
			</td>
			<td class="tabOff" align="left" valign="top">
				<img src="<%=session("progetto")%>/images/formazione/angaltodx.gif" border="0" WIDTH="17" HEIGHT="21">
			</td>			
			<td width="40%">
				&nbsp;
			</td>
		</tr>
	</table>
<%
TabellaRichieste("TU")
%>
</div>

<%
if sContr="S" then
%>

	<div id="VisTS" name="VisTS" style="position:absolute; visibility:hidden;">
		<table width="500" cellspacing="0" cellpadding="0">
			<tr>
				<td class="tabOff" width="20%">
					&nbsp;<a href="Javascript:CambiaDIV('VisTS', 'VisTU')" class="tabOff">Tutor</a>
				</td>
				<td class="tabOff" align="left" valign="top">
					<img src="<%=session("progetto")%>/images/formazione/angaltodx.gif" border="0" WIDTH="17" HEIGHT="21">
				</td>
				<td class="tabOn" width="20%">
				    <b>&nbsp;Tutor stage</b>
				    <%if CercaDest("TS") = "" then%>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<img src="<%=session("progetto")%>/images/formazione/accesso.gif" border="0" WIDTH="12" HEIGHT="12">
					<%end if%>
				</td>
				<td class="tabOn" align="left" valign="top">
					<img src="<%=session("progetto")%>/images/formazione/angaltodx.gif" border="0" WIDTH="17" HEIGHT="21">
				</td>			
				<td class="tabOff" width="20%">
					&nbsp;<a href="Javascript:CambiaDIV('VisTS', 'VisME')" class="tabOff">Mentor</a>
				</td>
				<td class="tabOff" align="left" valign="top">
					<img src="<%=session("progetto")%>/images/formazione/angaltodx.gif" border="0" WIDTH="17" HEIGHT="21">
				</td>			
				<td class="tabOff" width="20%">
					<b>&nbsp;Esperto</b>
				</td>
				<td class="tabOff" align="left" valign="top">
					<img src="<%=session("progetto")%>/images/formazione/angaltodx.gif" border="0" WIDTH="17" HEIGHT="21">
				</td>			
				<td width="40%">
					&nbsp;
				</td>
			</tr>
		</table>
	<%
	TabellaRichieste("TS")
	%>
	</div>
<%end if
%>

<div id="VisME" name="VisME" style="position:absolute; visibility:hidden;">
	<table width="500" cellpadding="0" cellspacing="0">
		<tr>
			<td class="tabOff" width="20%">
				&nbsp;<a href="Javascript:CambiaDIV('VisME', 'VisTU')" class="tabOff">Tutor
			</td>
			<td class="tabOff" align="left" valign="top">
				<img src="<%=session("progetto")%>/images/formazione/angaltodx.gif" border="0" WIDTH="17" HEIGHT="21">
			</td>
<%			if sContr="S" then %>			
			<td class="tabOff" width="20%">
				&nbsp;<a href="Javascript:CambiaDIV('VisME', 'VisTS')" class="tabOff">Tutor stage</a>
			</td>
			<td class="tabOff" align="left" valign="top">
				<img src="<%=session("progetto")%>/images/formazione/angaltodx.gif" border="0" WIDTH="17" HEIGHT="21">
			</td>				
<%          end if  %>					
			<td class="tabOn" width="20%">
				<b>&nbsp;Mentor</b>
				<%if CercaDest("ME") = "" then%>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<img src="<%=session("progetto")%>/images/formazione/accesso.gif" border="0" WIDTH="12" HEIGHT="12">
				<%end if%>
			</td>
			<td class="tabOn" align="left" valign="top">
				<img src="<%=session("progetto")%>/images/formazione/angaltodx.gif" border="0" WIDTH="17" HEIGHT="21">
			</td>			
			<td class="tabOff" width="20%">
				&nbsp;<a href="Javascript:CambiaDIV('VisME', 'VisES')" class="tabOff">Esperto</a>
			</td>
			<td class="tabOff" align="left" valign="top">
				<img src="<%=session("progetto")%>/images/formazione/angaltodx.gif" border="0" WIDTH="17" HEIGHT="21">
			</td>			
			<td width="40%">
				&nbsp;
			</td>
		</tr>
	</table>
<%
TabellaRichieste("ME")
%>
</div>

<div id="VisES" name="VisES" style="position:absolute; visibility:hidden;">
	<table width="500" cellspacing="0" cellpadding="0">
		<tr>
			<td class="tabOff" width="20%">
				&nbsp;<a href="Javascript:CambiaDIV('VisES', 'VisTU')" class="tabOff">Tutor</a>
			</td>
			<td class="tabOff" align="left" valign="top">
				<img src="<%=session("progetto")%>/images/formazione/angaltodx.gif" border="0" WIDTH="17" HEIGHT="21">
			</td>
<%			if sContr="S" then %>			
			<td class="tabOff" width="20%">
				&nbsp;<a href="Javascript:CambiaDIV('VisES', 'VisTS')" class="tabOff">Tutor stage</a>
			</td>
			<td class="tabOff" align="left" valign="top">
				<img src="<%=session("progetto")%>/images/formazione/angaltodx.gif" border="0" WIDTH="17" HEIGHT="21">
			</td>			
<%          end if  %>			
			<td class="tabOff" width="20%">
				&nbsp;<a href="Javascript:CambiaDIV('VisES', 'VisME')" class="tabOff">Mentor</a>
			</td>
			<td class="tabOff" align="left" valign="top">
				<img src="<%=session("progetto")%>/images/formazione/angaltodx.gif" border="0" WIDTH="17" HEIGHT="21">
			</td>			
			<td class="tabOn" width="20%">
				<b>&nbsp;Esperto</b>
			</td>
			<td class="tabOn" align="left" valign="top">
				<img src="<%=session("progetto")%>/images/formazione/angaltodx.gif" border="0" WIDTH="17" HEIGHT="21">
			</td>			
			<td width="40%">
				&nbsp;
			</td>
		</tr>
	</table>
<%
TabellaRichieste("ES")
%>
</div>
<br>
</td></tr></table>
<%
if sCR > "" then
	if sCR <> "VisTU" then
	%>
	<script language="javascript">
		CambiaDIV("VisTU", "<%=sCR%>")
	</script>
	<%
	else
	%>
	<script language="javascript">
		CambiaDIV("VisME", "<%=sCR%>")
	</script>
	<%
	end if	
end if 

End Sub
';););););););););););););););););););););););););););););););););););););););)
'M A I N
%>
<!--#include virtual="/strutt_testa2.asp"-->
<!--#include virtual="/include/openconn.asp"-->
<!--#include virtual="/include/ControlDateVB.asp"-->
<!--#include virtual="/include/DecCod.asp"-->
<!--#include virtual="/include/SetPulsanti.asp"-->
<!--#include virtual="/include/HTMLEncode.asp"-->
<%
if ValidateService(session("idutente"),"RIC_VISRICHIESTE",CC) <> "true" then 
	response.redirect "/util/error_login.asp"
end if
	
GestioneDivJS()
Inizio()
ImpostaPag()
%>
<!--#include virtual ="/include/closeconn.asp"-->
<!--#include virtual="/strutt_coda2.asp"-->
