<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%
Sub Inizio()
	dim sFunzione
	dim sTitolo
	dim sCommento
	dim bCampiObbl
	
	sFunzione = "RICHIESTA DI ASSISTENZA"
	sTitolo = "RICHIESTA DI ASSISTENZA"
	sCommento = "Visualizza la conferma dell'inserimento della richiesta"
	bCampiObbl = False
	sHelp="../../help/formazione/RicAss/RIC_CnfInsRicAss"
%>
	<!--#include virtual="/include/SetTestata.asp"-->
<%
End Sub
'-----------------------------------------------------------------------
Function CercaDest(Destinatario)
dim rsDest, sSQL1

if Destinatario = "TS" then
    sSQL1 = "SELECT ID_TUTOR FROM PERS_STAGE WHERE ID_PERSONA=" & SESSION("CREATOR")
else
	sSQL1 = "SELECT G.IDUTENTE FROM ASSISTENZAGRUPPO G, ASSISTENZAUTENTE U " &_
			"WHERE G.ID_GRUTASS = U.ID_GRUTASS AND G.STATO_ASS = 'A' AND " &_
			"U.IDUTENTE = " & Session("idutente") & " AND " &_
			"G.COD_RUOLO = '" & Destinatario & "'"
'Response.Write sSQL1
end if

Set rsDest = Server.CreateObject("ADODB.Recordset")
'PL-SQL * T-SQL  
SSQL1 = TransformPLSQLToTSQL (SSQL1) 
rsDest.Open sSQL1, CC, 3
if rsDest.EOF then
	CercaDest = ""
else
	if Destinatario = "TS" then
		CercaDest = rsDest("ID_TUTOR")
	ELSE
		CercaDest = rsDest("IDUTENTE")
	end if
end if
rsDest.Close
set rsDest = nothing
End Function
'-----------------------------------------------------------------------
Sub VisMessaggio(sMess)
%>
<br>
<table border=0 cellspacing=2 cellpadding=1 width="500">
	<tr align=center>
		<td class="tbltext3">
			<%=sMess%>
		</td>
	<tr>
</table>
<%
End Sub
'-----------------------------------------------------------------------
Sub ImpostaPag()
dim sDest, sArea, sOggetto, sTestoDomanda
dim sIdDest
dim sSQL, sErrore

sDest = Request.Form("radDest")
sArea = Request.Form("cmbArea")
sOggetto = Replace(Request.Form("txtOggetto"), "'", "''")
sTestoDomanda = Replace(Request.Form("txtTestoDomanda"), "'", "''")

if sDest = "ES" then
	sSQL = "INSERT INTO RIC_ASS (COD_RUOLO, COD_AREA, IDUTENTE, DESTINATARIO, OGGETTO, TESTO_DOM, DT_INVIO, DT_TMST) VALUES (" &_
			"'" & sDest & "', " &_
			"'" & sArea & "', " &_
			Session("idutente") & ", " &_
			"0, " &_
			"'" & sOggetto & "', " &_
			"'" & sTestoDomanda & "', " &_
			ConvDateToDB(Date()) & ", " &_
			ConvDateToDB(Now()) & ")"
else
	sIdDest = CercaDest(sDest)
	if sIdDest > "" then
		sSQL = "INSERT INTO RIC_ASS (COD_RUOLO, IDUTENTE, DESTINATARIO, OGGETTO, TESTO_DOM, DT_INVIO, DT_TMST) VALUES (" &_
				"'" & sDest & "', " &_
				Session("idutente") & ", " &_
				sIdDest & ", " &_
				"'" & sOggetto & "', " &_
				"'" & sTestoDomanda & "', " &_
				ConvDateToDB(Date()) & ", " &_
				ConvDateToDB(Now()) & ")"	
	end if
end if
if sSQL > "" then
	sErrore = EseguiNoC(sSQL ,CC)
	if sErrore = "0" then
		VisMessaggio("Richiesta di Assistenza correttamente inoltrata")
	else
		VisMessaggio("Impossibile inoltrare la Richiesta di assistenza <br>" & sErrore)
	end if
else
	VisMessaggio("Servizio di Assistenza non disponibile")
end if
%>
<br>
<table border="0" cellspacing="2" cellpadding="1" width="500">
	<tr>
		<td align=center>
   			<%
   			PlsLinkRosso "RIC_VisRichieste.asp", "Elenco delle Richieste"
   			%>		
		</td>
	</tr>
</table>
<%
End Sub
'------------------------------------------------------------------------
%>
<!--#include virtual="/strutt_testa2.asp"-->
<!--#include virtual="/include/openconn.asp"-->
<!--#include virtual="/Util/DBUtil.asp"-->
<!--#include virtual="/Include/SysFunction.asp"-->
<!--#include virtual="/include/SetPulsanti.asp"-->
<%	
	Inizio()
	ImpostaPag()
%>
<!--#include virtual="/include/closeconn.asp"-->
<!--#include virtual="/strutt_coda2.asp"-->
