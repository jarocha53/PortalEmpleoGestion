<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%
Sub GestioneRich()
%>
<script language="javascript">
var finestra
function EvReq(nIdR)
{
	if (finestra != null ) 
	{
		finestra.close(); 
	}
	f='RIC_InsRispRicAss.asp?idR=' + nIdR
	finestra=window.open(f,"","width=560,height=450,location=no,menubar=no,scrollbars=yes,resizable=no")
}
function Carica_Pagina(npagina)
{
	document.frmPagina.pagina.value = npagina;
	document.frmPagina.submit();
}

</script>
<%
End Sub
';););););););););););););););););););););););););););););););););););););););)
Sub Inizio()
	dim sFunzione
	dim sTitolo
	dim sCommento
	dim bCampiObbl
	
	sFunzione = "RICHIESTA DI ASSISTENZA"
	sTitolo = "EVASIONE RICHIESTE DI ASSISTENZA"
	sCommento = "Visualizza l'elenco delle richieste da evadere"
	bCampiObbl = false
	sHelp="/Pgm/help/formazione/RicAss/RIC_VisEvaRic"
%>
	<!--#include virtual="/include/SetTestata.asp"-->
<%
End Sub
';););););););););););););););););););););););););););););););););););););););)
Function DecodArea(nArea)
Dim sSQL, rsArea
if isnull(nArea) then exit function

sSQL = " select DESCRIZIONEAREA from area where ID_AREA = " & nArea
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
set rsArea = CC.execute(sSQL)
if rsArea.eof then
	DecodArea = "Area non disponibile"
else
	DecodArea = rsArea("DESCRIZIONEAREA")
end if
rsArea.close
set rsArea = nothing

End Function
';););););););););););););););););););););););););););););););););););););););)
Sub ImpostaPag()
dim sSQL, rsRicAss
dim nProg, sOldCodRuo

if session("ruofu") = "ES" then
	sSQL = "SELECT id_ric_ass, cod_ruolo, cod_area, destinatario, oggetto, testo_dom, dt_invio, " &_
			"dt_risposta, testo_risp " &_ 
			"FROM RIC_ASS, ESPERTOAREA " &_
			"WHERE RIC_ASS.COD_AREA = ESPERTOAREA.ID_AREATEMATICA AND " &_
			"SYSDATE BETWEEN ESPERTOAREA.DAL AND ESPERTOAREA.AL AND " &_
			"ESPERTOAREA.IDUTENTE = " & session("idutente") & " " &_
			"and dt_risposta is null " &_
			"ORDER BY cod_ruolo, dt_invio"
else
	sSQL = "SELECT id_ric_ass, cod_ruolo, cod_area, destinatario, oggetto, testo_dom, dt_invio, " &_
			"dt_risposta, testo_risp, dt_tmst FROM RIC_ASS " &_
			"WHERE destinatario = " & session("idutente") & " " &_
			"and dt_risposta is null " &_
			"ORDER BY cod_ruolo, dt_invio"
end if
'Response.Write sSQL
Set rsRicAss = Server.CreateObject("ADODB.Recordset")
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
rsRicAss.Open sSQL, CC, 3
if rsRicAss.EOF then
	%>
	<br>
	<table border="0" width="500" cellpadding="0" cellspacing="0">
	   <tr align="center">
	   	<td class="tbltext3"> 
	   		Non sono presenti Richieste di Assistenza
	   	</td>
	   </tr>
	</table>
	<br>
	<%
else
	
	Record=0
	nTamPagina=5
	nPag = Request.Form("pagina")
	if not(nPag > "") then
		nPag = 1
	end if
	
	nActPagina = clng(nPag)
		
	rsRicAss.PageSize = nTamPagina
	rsRicAss.CacheSize = nTamPagina	

	nTotPagina = rsRicAss.PageCount		
	If nActPagina < 1 Then
		nActPagina = 1
	End If
	
	If nActPagina > nTotPagina Then
		nActPagina = nTotPagina
	End If
	
	rsRicAss.AbsolutePage=nActPagina
	
	nTotRecord=0
	%>
	<br>
	<table border="0" width="500" cellpadding="1" cellspacing="2">
		<tr class="sfondocomm" height="23">
			<td width="10"><b>#</b></td>
			<td width="290"><b>Oggetto</b></td>
			<td width="100"><b>Area</b></td>
			<td width="100"><b>Data Ricezione</b></td>
		</tr>
	<%
	sOldCodRuo = ""
	nProg = 1
	do while not rsRicAss.EOF And nTotRecord < nTamPagina
	%>
		<tr class="tblsfondo">
			<td class="tblDett">
				<%=nProg+((nActPagina*nTamPagina)-nTamPagina)%>
			</td>
			<td class="tblDett">
				<a href="javascript:EvReq(<%=rsRicAss("id_ric_ass")%>)" class="tblagg">
				&nbsp;<%=strHTMLEncode(rsRicAss("Oggetto"))%>
				</a>
			</td>
			<td class="tblDett">
				<%
				'DecCodVal("AREAT", 0, date, rsRicAss("cod_area"),1)
				Response.Write (DecodArea(rsRicAss("cod_area")))
				%>
			</td>
			<td class="tblDett">
				<%=ConvDateToString(rsRicAss("dt_invio"))%>
			</td>
		</tr>
	<%
		nProg = nProg + 1
		nTotRecord=nTotRecord + 1
		rsRicAss.MoveNext
	loop
	%>
	</table>
	<br>
	<form name="frmPagina" method="post" action="RIC_VisEvaRic.asp">
		<input type="hidden" name="pagina" id="pagina" value="<%=nPag%>">
	</form>
	<table border="0" width="500" align="center">
		<tr>
	<%
	if nActPagina > 1 then
	%>
			<td align="right" width="480">
				<a HREF="javascript:Carica_Pagina(<%=nActPagina-1%>)">
					<img border="0" src="<%=session("progetto")%>/images/precedente.gif">
				</a>
			</td>
	<%
	end if
	if nActPagina < nTotPagina then
	%>
			<td align="right">
				<a HREF="javascript:Carica_Pagina(<%=nActPagina+1%>)">
					<img border="0" src="<%=session("progetto")%>/images/successivo.gif">
				</a>
			</td>
	<%
	end if
	%>
		</tr>
	</table>
<%
end if
rsRicAss.Close
set rsRicAss = nothing
%>
<table border="0" width="500" cellpadding="0" cellspacing="0">
   <tr align="center">
   	<td>
   		<%
   		PlsLinkRosso "RIC_VisRicEvase.asp", "Visualizza richieste evase"
   		%>
   	</td>
   </tr>
</table>
<%  
End Sub
';););););););););););););););););););););););););););););););););););););););)
'M A I N
%>
<!--#include virtual="/strutt_testa2.asp"-->
<!--#include virtual="/include/openconn.asp"-->
<!--#include virtual="/include/ControlDateVB.asp"-->
<!--#include virtual="/include/DecCod.asp"-->
<!--#include virtual="/include/SetPulsanti.asp"-->
<!--#include virtual="/include/HTMLEncode.asp"-->
<%
if ValidateService(session("idutente"),"RIC_VISRICHIESTE",CC) <> "true" then 
	response.redirect "/util/error_login.asp"
end if

GestioneRich()
Inizio()
ImpostaPag()
%>
<!--#include virtual ="/include/closeconn.asp"-->
<!--#include virtual="/strutt_coda2.asp"-->
