<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%'Option Explicit 
   Response.ExpiresAbsolute = Now() - 1 
   Response.AddHeader "pragma","no-cache"
   Response.AddHeader "cache-control","private"
   Response.CacheControl = "no-cache"
%>

<!-- #include virtual="/strutt_testa2.asp"-->
<!--#include virtual = "/include/openconn.asp"-->
<!--#include virtual="/util/portallib.asp"-->
<!--#include virtual="/util/DBUtil.asp"-->
<!--#include virtual = "/include/ControlDateVB.asp"-->


<%
'if ValidateService(session("idutente"),"ESP_VisEspArea", CC) <> "true" then 
'	response.redirect "/util/error_login.asp"
'end if
%>
<!-- ************** Javascript Inizio ************ -->

<script LANGUAGE="Javascript">
<!--#include virtual = "/Include/ControlDate.inc"-->
<!--#include virtual = "/Include/ControlNum.inc"-->
<!--#include Virtual = "/Include/ControlString.inc"-->
<!--#include virtual="/include/help.inc"-->



function Reload(){

	frmInsEspArea.action = "ESP_InsEspArea.asp"
	frmInsEspArea.submit()
}
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function VaiInizio(sDatiEsperto){
	location.href="ESP_VisEspArea.asp?cmbEsperto=" + sDatiEsperto + "&Modo=1";
}	

//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function ControllaDati(frmInsEspArea,DataOdierna) {
//		frmInsEspArea.cmbAreaTematica		- Alfanumerico - 
//		frmInsEspArea.txtDataDal			- Data - 
//		frmInsEspArea.txtDataAl				- Data -
	
	frmInsEspArea.cmbAreaTematica.value = TRIM(frmInsEspArea.cmbAreaTematica.value)
	if (frmInsEspArea.cmbAreaTematica.value == ""){
		alert("Campo 'Area Tematica' obbligatorio")
		frmInsEspArea.cmbAreaTematica.focus() 
		return false	
	}
	
	var appoDataDal;
	var appoDataAl;

	appoDataDal = TRIM(frmInsEspArea.txtDataDal.value)
	if (appoDataDal == ""){
		alert("Campo 'Validit� Dal' obbligatorio")
		frmInsEspArea.txtDataDal.focus() 
		return false	
	}

	if (!ValidateInputDate(appoDataDal)){
		frmInsEspArea.txtDataDal.focus()  
		return false
	}

	var data1;
	var data2;
		
	data1 = appoDataDal.substr(6,4) + appoDataDal.substr(3,2) + appoDataDal.substr(0,2);
	data2 = DataOdierna.substr(6,4) + DataOdierna.substr(3,2) + DataOdierna.substr(0,2);
		
	if (data1 < data2){
		alert("La Data di Inizio validit� non pu� essere inferiore alla Data Odierna")
		frmInsEspArea.txtDataDal.focus()  
		return false
	}

	if (TRIM(frmInsEspArea.txtDataAl.value) == ""){
//		alert("Campo 'Validit� Al' obbligatorio")
//		frmInsEspArea.txtDataAl.focus() 
//		return false	
		frmInsEspArea.txtDataAl.value = "31/12/9999"	
	}

	appoDataAl = TRIM(frmInsEspArea.txtDataAl.value)
	if (!ValidateInputDate(appoDataAl)){
		frmInsEspArea.txtDataAl.focus()  
		return false
	}
	
	data1 = appoDataAl.substr(6,4) + appoDataAl.substr(3,2) + appoDataAl.substr(0,2);

	if (data1 < data2){
		alert("La Data di Fine validit� non pu� essere inferiore alla Data Odierna")
		frmInsEspArea.txtDataAl.focus()  
		return false
	}	
	
	data1 = appoDataDal.substr(6,4) + appoDataDal.substr(3,2) + appoDataDal.substr(0,2);
	data2 = appoDataAl.substr(6,4) + appoDataAl.substr(3,2) + appoDataAl.substr(0,2);

	if (data2 < data1){
		alert("La Data di Fine Validit� non pu� essere inferiore alla Inizio Validit�")
		frmInsEspArea.txtDataAl.focus()  
		return false
	}	
	
	return true	
}

</script>
<!-- ************** Javascript Fine ************ -->

<!-- ************** ASP Inizio *************** -->
<%sub Inizio()%>
<!--	<table border="0" width="520" cellspacing="0" cellpadding="0" height="81">	   <tr>	     <td width="500" background="<%'=Session("Progetto")%>/images/titoli/strumenti2b.gif" height="81" valign="bottom" align="right">		       <table border="0" width="260" height="30" cellspacing="0" cellpadding="0">	         <tr>	             <td width="100%" valign="top" align="right">	                 <b class="TBLTEXT1FADa">Esperto-Area Tematica</span></b>	             </td>	         </tr>	       </table>	     </td>	   </tr>	</table>-->
	<br>
	<table cellpadding="0" cellspacing="0" width="500" border="0">
		<tr height="18">
			<td class="sfondomenu" height="18" width="67%">
			<span class="tbltext0"><b>Associazione Esperto/Area Tematica</b></span></td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
			<td valign="middle" align="right" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="TBLTEXT1FAD">(*) campi obbligatori</td>
		</tr>
		<tr width="371" class="SFONDOCOMMFAD">
			<td colspan="3">Testo di prova<a href="Javascript:Show_Help('/Pgm/help/Formazione/Assistenza/Esperto/ESP_InsEspArea')" onmouseover="javascript:window.status=' '; return true"><img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
			</td>
		</tr>
		<tr height="2">
			<td colspan="3" class="SFONDOCOMMFAD" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>
	<br>

	<form name="frmInsEspArea" method="post" onsubmit="return ControllaDati(this,'<%=dDataOdierna%>')" action="ESP_CnfEspArea.asp">
	
<%
end sub

'------------------------------------------------------------------------------------------------------------------------------------------------------------------------

sub Msgetto(sMessaggio)
%>
<tr><td>&nbsp;</td></tr>
		<tr><td class="tbltext3" colspan="2" align="center"><b><%=sMessaggio%></b></td></tr>	
<tr><td>&nbsp;</td></tr>
<%			
end sub

'------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	
sub Fine()
%>
</form>
<!--#include virtual = "/include/closeconn.asp"-->
<!--#include virtual = "strutt_coda2.asp"-->
<%
end sub

'------------------------------------------------------------------------------------------------------------------------------------------------------------------------

function CaricaArea()
dim sSQL
dim i
dim rsTades
dim aOutArray
dim nEle
dim dDataSql

	dDataSql=ConvDateToDB(dDataOdierna)

	sSQL =	"SELECT ID_AREA, TITOLOAREA" & _
			" FROM AREA" &_ 
			" WHERE ID_AREA NOT IN (SELECT ID_AREATEMATICA" &_
								" FROM ESPERTOAREA" &_
								" WHERE IDUTENTE = " & sIdUtente &_
								" AND AL >= " & dDataSql & ")"
				
'	Response.Write sSql
	
	set rsTades = Server.CreateObject("ADODB.Recordset")
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	rsTades.open sSQL, CC, 3

	if rsTades.EOF then
	   rsTades.Close
	   set rsTades = nothing
	   redim aOutArray (0,0)
	   aOutArray(0, 0) = "null"
	   CaricaArea = aOutArray
	   exit function
	end if

	nEle = rsTades.RecordCount	
	i = 0
	redim aOutArray(1, nEle - 1)
	do while not rsTades.EOF
		aOutArray(0, i) = rsTades("ID_AREA")
		aOutArray(1, i) = rsTades("TITOLOAREA")
		
		i = i + 1
		rsTades.MoveNext
	loop
	
	CaricaArea = aOutArray

end function

'------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function ImpArea()

dim aArea
dim i

	imparea = true
	
	aArea = CaricaArea()
	if aArea(0, 0) = "null" then
		Msgetto("Non ci sono Aree tematiche disponibili per l'esperto")
		imparea = false
		exit function
	end if
	%>
	<tr>
		<td align="left" width="35%">
			<span class="TBLTEXT1FAD"><b>Area Tematica *</b></span>
		</td>	
		<td align="left">
    		<select class="TEXTBLACKFAD" name="cmbAreaTematica">
				<option selected></option>
				<%
				for i = 0 to ubound(aArea, 2)
					Response.Write "<OPTION "
					Response.write "value ='" & aArea(0, i) & "|" & aArea(1, i)  & "'> " &_
					aArea(1, i) & "</OPTION>"
				next
				%>
			</select>
		</td>
	</tr>
	<%
end function
%>

<!-- ************** ASP Fine *************** -->

<!-- ************** MAIN Inizio ************ -->
<%
dim sSQL

dim sIdArea
dim sDatiEsperto
dim sIdUtente
dim sCognome
dim sNome
dim dDataOdierna

	dDataOdierna = Date
	sDatiEsperto = Request("cmbEsperto")

	if trim(sDatiEsperto) <> "" then
		aAppo = Split(sDatiEsperto,"|")
		sIdUtente	= aAppo(0)
		sCognome	= aAppo(1)
		sNome		= aAppo(2)
	end if

	inizio()

%>
	
	<input type="hidden" value="Ins" name="Action">
	<input type="hidden" value="<%=sDatiEsperto%>" name="DatiEsperto">	

	<table border="0" cellpadding="1" cellspacing="1" width="500">
		<tr>
			<td align="left" width="35%">
				<span class="TBLTEXT1FAD"><b>Esperto </b></span>
			</td>	
			<td align="left">
				<input class="TEXTblackFAD" type="text" disabled value="<%=sCognome &  " " & sNome%>" name="txtUtente" size="60">
			</td>
		</tr>
		<% 
		if ImpArea() = true then
			%>
			<tr>
				<td align="left" width="35%">
					<span class="TBLTEXT1FAD"><b>Validit� Dal *</b></span>
				</td>	
				<td align="left">
					<input class="TEXTblackFAD" type="text" value name="txtDataDal" size="12">
				</td>
			</tr>
			<tr>
				<td align="left" width="35%">
					<span class="TBLTEXT1FAD"><b>Validit� Al *</b></span>
				</td>	
				<td align="left">
					<input class="TEXTblackFAD" type="text" value name="txtDataAl" size="12">
				</td>
			</tr>
		</table>
	
		<!--impostazione dei comandi-->
		<table align="center" cellpadding="0" cellspacing="0" width="200" border="0">	
			<tr align="center">
				<td nowrap><input type="image" name="Conferma" src="<%=Session("progetto")%>/images/conferma.gif"></td value="Registra">
				<td nowrap><a href="javascript:VaiInizio('<%=sDatiEsperto%>')"><img src="<%=session("Progetto")%>/images/indietro.gif" border="0" name="imgPunto2" onmouseover="javascript:window.status=' '; return true"></a></td>
			</tr>
		</table>
		<br>
		<%
		else
		%>
		</table>

		<!--impostazione dei comandi-->
		<table align="center" cellpadding="0" cellspacing="0" width="200" border="0">	
			<tr align="center">
				<td nowrap><a href="javascript:VaiInizio('<%=sDatiEsperto%>')"><img src="<%=session("Progetto")%>/images/indietro.gif" border="0" name="imgPunto2" onmouseover="javascript:window.status=' '; return true"></a></td>
			</tr>
		</table>
		<br>
		<%
		end if
		%>
	<br>
	
<%
	Fine()
%>
<!-- ************** MAIN Fine ************ -->
