<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include virtual="/strutt_testa2.asp"-->
<!--#include virtual="/util/portallib.asp"-->

<script LANGUAGE="JavaScript" src="/Include/help.inc"></script>
<script LANGUAGE="JavaScript" src="/Include/ControlString.inc"></script>
<script LANGUAGE="JavaScript">

		function validateCan()
		{		 
		if(document.formcan.tit.value == "" || document.formcan.tit.value == " ")
		  {
		  document.formcan.tit.focus();
		  alert("Inserire il titolo. ");
		  return false;
		  }
		if(document.formcan.desc.value == "" || document.formcan.desc.value == " ")
		  {
		  document.formcan.desc.focus();
		  alert("Inserire la descrizione. ");
		  return false;
		  }
     	}		  


		function validateDom()
		{		 
		if(document.FormMod.txtTitle.value == "" || document.FormMod.txtTitle.value == " ")
		  {
		  document.FormMod.txtTitle.focus();
		  alert("Inserire l'oggetto. ");
		  return false;
		  }
		if(document.FormMod.txtQuestion.value == "" || document.FormMod.txtQuestion.value == " ")
		  {
		  document.FormMod.txtQuestion.focus();
		  alert("Inserire il messaggio. ");
		  return false;
		  }
     	}		      	
</script>
<%
Sub Inizio(tipohelp)
	
	If not ValidateService(Session("IdUtente"),"GESTIONE FAQ",cc) Then 
		response.redirect "/util/error_login.asp"
	End If

%>
<table border="0" width="100%" cellspacing="0" cellpadding="0" height="73">
	<tr>
		<td width="500" background="<%=Session("Progetto")%>/images/titoli/Community2b.gif" height="73" valign="bottom" align="right">
			<table border="0" width="500" height="30" cellspacing="0" cellpadding="0">
				<tr>
					<td valign="top" align="right"><b class="tbltext1">FAQ Tematiche</b></td>
					<td valign="top" align="left" width="18">
						<%If tipohelp =1 then%>
						<a href="Javascript:Show_Help('/Pgm/help/Formazione/Faq/FAQ_Modifica')" name onmouseover="javascript:status='' ; return true">
						<%Else%>
							<a href="Javascript:Show_Help('/Pgm/help/Formazione/FAQ/FAQ_Modifica_1')" name onmouseover="javascript:status='' ; return true">
						<%End If%>
						<img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0" width="18" height="18" alt="per maggiori informazioni"></a>
					</td>
				</tr>	
			</table>
		</td>
	</tr>
</table>
<br>
<%
End Sub

Sub ImpostaCan()
	Dim rsArea, sql, n, ind, sql1
		
	nIdCanale = clng(Request.Form("IdCanale"))
	nIdAreaTem = clng(Request.Form("IdAreaTem"))
		
	sqlArea = "SELECT a.titolo_area,a.desc_area,b.titoloarea FROM frm_area a, area b " &_
	          "WHERE id_frm_area =" & nIdCanale & " AND a.id_area = b.id_area"
	set rsArea = Server.CreateObject("adodb.recordset")
'PL-SQL * T-SQL  
SQLAREA = TransformPLSQLToTSQL (SQLAREA) 
	rsArea.Open sqlArea, CC, 3
	If rsArea.EOF Then
%>		
		<table border="0" cellspacing="0" cellpadding="0" width="100%">
			<tr align="center">
				<td class="tbltext3">
					<b>Pagina momentaneamente non disponibile.</b>
				</td>
			</tr>
		</table>
<%			exit Sub
	Else
		sAreaTem = rsArea("titoloarea")
		sTitoloArea = rsArea("titolo_area")
		sDescArea = rsArea("desc_area")
	End If
		
	rsArea.close
	set rsArea = nothing
		
%>
	<form Name="formcan" ACTION="/Pgm/formazione/Faq/FAQ_Modifica.asp" METHOD="POST" onsubmit="return validateCan();">
	    <table border="0" cellspacing="0" cellpadding="0" width="100%">
			<tr align="center">
				<td class="tbltext3">
					<b>Usa il seguente form per modificare il canale delle FAQ</b>
				</td>
			</tr>
		</table>
		<br><br><br>
		<table border="0" cellspacing="0" cellpadding="0" width="100%">
			<tr>
				<td height="15" width="2%"><img src="<%=Session("Progetto")%>/images/righinaPSX.jpg"></td> 
				<td align="left" class="tblsfondo3" width="20%"><b class="tbltext0" valign="middle">Area Tematica&nbsp;</b></td>
				<td height="15" width="2%"><img src="<%=Session("Progetto")%>/images/righinaPDX.jpg"></td> 
				<td align="left">&nbsp;<b class="tbltext3"><%=sAreaTem%></b></td>
		    </tr>
			<tr>
				<td colspan="4" height="2" width="15" valign="middle" background="<%=Session("Progetto")%>/images/separarighe.gif"></td> 
		    </tr>
		    <tr>
				<td height="15" width="2%"><img src="<%=Session("Progetto")%>/images/righinaPSX.jpg"></td> 
				<td align="left" class="tblsfondo3" width="20%"><b class="tbltext0" valign="middle">Nome Canale&nbsp;</b></td>
				<td height="15" width="2%"><img src="<%=Session("Progetto")%>/images/righinaPDX.jpg"></td> 
				<td align="left" class="tbltext">&nbsp;<input type="text" id="tit" name="tit" value="<%=sTitoloArea%>" size="50" maxlength="50"></td>
		    </tr>
			<tr>
				<td colspan="4" height="2" width="15" valign="middle" background="<%=Session("Progetto")%>/images/separarighe.gif"></td> 
		    </tr>
			<tr>
				<td height="15" width="2%"><img src="<%=Session("Progetto")%>/images/righinaPSX.jpg"></td> 
				<td align="left" class="tblsfondo3" width="20%"><b class="tbltext0" valign="middle">Descrizione Canale&nbsp; </b></td>
				<td height="15" width="2%"><img src="<%=Session("Progetto")%>/images/righinaPDX.jpg"></td> 
				<td align="left" class="tbltext">&nbsp;<input type="text" id="desc" name="desc" value="<%=sDescArea%>" size="60" maxlength="100"></td>
			</tr>
		</table>
		<br>	
		<table border="0" cellspacing="2" cellpadding="1" width="100%">
			<tr>
				<td colspan="3" align="center">
					<input type="image" src="<%=Session("progetto")%>/images/conferma.gif" title="Conferma" border="0" align="absBottom" id="image2" name="image2">
				</td>
			</tr>
		</table>
		<input type="hidden" name="IdCanale" value="<%=nIdCanale%>">
		<input type="hidden" name="Modo" value="mod">
		<input type="hidden" name="Oggetto" value="<%=oggetto%>"><br><br>
	</form>
	<table border="0" cellspacing="0" cellpadding="0" width="100%">
		<tr> 
			<td align="center"> 
				<a class="textred" href="/Pgm/formazione/FAQ/FAQ_VisCanali.asp" onmouseover="window.status =' '; return true">
				<b>Torna alle Aree </b></a>
			</td>
		</tr>
	</table>
	<br><br>
<%

End Sub

Sub ImpostaDom()
	Dim rsDomanda, sql, n, ind, sql1
		
	ID = request.form("mod1")
	ca = request.form("mod")
		
	sqlDomanda = "SELECT Mittente, Oggetto, Domanda FROM frm_domanda WHERE id_frm_domanda =" & ID 	
	set rsDomanda = Server.CreateObject("ADODB.RECORDSET")
'PL-SQL * T-SQL  
SQLDOMANDA = TransformPLSQLToTSQL (SQLDOMANDA) 
	rsDomanda.open sqlDomanda, CC, 3
	If rsDomanda.EOF Then
%>		<table border="0" cellspacing="0" cellpadding="0" width="100%">
			<tr align="center">
				<td class="tbltext3">
						<b>Pagina momentaneamente non disponibile.</b>
                </td>
            </tr>
		</table>
<%		exit Sub
	Else
		sMittente = rsDomanda("Mittente")
		sOggetto = rsDomanda("Oggetto")
		sDomanda = rsDomanda("Domanda")
	End If
		
	rsDomanda.close
	set rsDomanda = nothing

%>
	<form Name="FormMod" ACTION="/Pgm/formazione/Faq/FAQ_Modifica.asp" METHOD="POST" onsubmit="return validateDom();">
	    <input type="hidden" name="cat" value="<%=request(ID)%>">
	    <input type="hidden" name="IDAREA" value="<%=nIdAreaTem%>">
	    <br>
	    <table border="0" cellspacing="0" cellpadding="0" width="100%" align="center">
			<tr align="center"> 
				<td class="tbltext3">
					<b>Usa il seguente form per modificare il messaggio</b>
	            </td>
			</tr>
		</table>
		<br>
		<table align="center" border="0" cellspacing="0" cellpadding="0" width="100%">
			<tr>
				<td colspan="4" height="4" valign="middle" background="<%=Session("Progetto")%>/images/separarighe.gif"></td> 
	        </tr>
			<tr> 
				<td height="15" width="2%" valign="middle"><img src="<%=Session("Progetto")%>/images/righinaPSX.jpg"></td> 
				<td valign="middle" width="13%" align="left" class="tblsfondo3">
					<b class="tbltext0">&nbsp; Oggetto</b>
				</td>
				<td height="15" width="2%" valign="middle"><img src="<%=Session("Progetto")%>/images/righinaPDX.jpg"></td> 
				<td width="83%"> &nbsp; <input type="text" name="txtTitle" size="30" maxlength="100" class="tbltext" value="<%=SOggetto%>"></td>
			</tr>
			<tr>
				<td colspan="4" height="4" valign="middle" background="<%=Session("Progetto")%>/images/separarighe.gif"></td> 
	        </tr>
		</table>
		<table align="center" border="0" cellspacing="0" cellpadding="0" width="100%">
			<tr> 
				<td height="15" width="2%" valign="middle"><img src="<%=Session("Progetto")%>/images/righinaPSX.jpg"></td> 
				<td valign="middle" width="96%" align="left" class="tblsfondo3">
					<b class="tbltext0">&nbsp; Testo del Messaggio</b>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<span class="tbltext0">
					- Utilizzabile <b><label name="NumCaratteri" id="NumCaratteri">485</label></b>caratteri -</span>
				</td>
				<td height="15" width="2%" valign="middle"><img src="<%=Session("Progetto")%>/images/righinaPDX.jpg"></td> 
			</tr>
			<tr> 
				<td width="2%" class="tbltext"> &nbsp; </td>
				<td align="left" width="98%" valign="middle">
					<textarea name="txtQuestion" onKeyup="JavaScript:CheckLenTextArea(document.FormMod.txtQuestion,NumCaratteri,485)" CLASS="MyTextBox" cols="80" rows="8"><%=Sdomanda%></textarea>
				</td>
			</tr>				
			<tr height="17"> 
				<td colspan="4">&nbsp;</td>
			</tr>
		</table>	
	
	   <table border="0" align="center" cellspacing="0" cellpadding="0" width="100%">
			<tr> 
				<td colspan="2" align="center">
					<input type="image" src="<%=Session("progetto")%>/images/conferma.gif" title="Conferma" border="0" align="absBottom" id="image2" name="image2">					
				</td>
			</tr>
		</table>
		<input type="hidden" name="ID" value="<%=ID%>">
		<input type="hidden" name="can" value="<%=ca%>">
		<input type="hidden" name="Modo" value="mod">
		<input type="hidden" name="Oggetto" value="<%=oggetto%>"><br><br>
		
	</form>
		
<%

End Sub

Sub ImpostaRis()
	Dim rsRisposta, sql, n, ind, sql1
		
	nIdCanale = request.form("mod")
	ca = request.form("mod1")
	ris = request.form("mod2")
		
	sqlRisposta = "SELECT Mittente, Oggetto, Risposta FROM frm_risposta WHERE id_frm_risposta =" & ris 	
	Set rsRisposta = Server.CreateObject("ADODB.RECORDSET")
'PL-SQL * T-SQL  
SQLRISPOSTA = TransformPLSQLToTSQL (SQLRISPOSTA) 
	rsRisposta.open sqlRisposta, CC, 3
	If rsRisposta.EOF Then
%>		<table border="0" cellspacing="0" cellpadding="0" width="100%">
				<tr align="center"><td class="tbltext3">
					<b>Pagina momentaneamente non disponibile.</b>
				</td>
			</tr>
		</table>
<%		exit Sub
	Else
		sMittente = rsRisposta("Mittente")
		sOggetto = rsRisposta("Oggetto")
		sRisposta = rsRisposta("Risposta")
	End If
		
	rsRisposta.close
	set rsRisposta = nothing

%>
	<form Name="FormMod" ACTION="/Pgm/formazione/Faq/FAQ_Modifica.asp" METHOD="POST" onsubmit="return validateDom();">
		<input type="hidden" name="cat" value="<%=request(ID)%>">
		<br>
		<table border="0" cellspacing="0" cellpadding="0" width="100%" align="center">
			<tr align="center"> 
				<td class="tbltext3">
					<b>Usa il seguente form per modificare il messaggio</b>
		        </td>
			</tr>
		</table>
		<br>
		<table align="center" border="0" cellspacing="0" cellpadding="0" width="100%">
			<tr>
				<td colspan="4" height="4" valign="middle" background="<%=Session("Progetto")%>/images/separarighe.gif"></td> 
		    </tr>
			<tr> 
				<td height="15" width="2%" valign="middle"><img src="<%=Session("Progetto")%>/images/righinaPSX.jpg"></td> 
				<td valign="middle" width="13%" align="left" class="tblsfondo3">
					<b class="tbltext0">&nbsp; Oggetto</b>
				</td>
				<td height="15" width="2%" valign="middle"><img src="<%=Session("Progetto")%>/images/righinaPDX.jpg"></td> 
				<td width="83%"> &nbsp; <input type="text" name="txtTitle" size="30" maxlength="100" class="tbltext" value="<%=SOggetto%>"></td>
			</tr>
			<tr>
				<td colspan="4" height="4" valign="middle" background="<%=Session("Progetto")%>/images/separarighe.gif"></td> 
		    </tr>
		</table>
		<table align="center" border="0" cellspacing="0" cellpadding="0" width="100%">
			<tr> 
				<td height="15" width="2%" valign="middle"><img src="<%=Session("Progetto")%>/images/righinaPSX.jpg"></td> 
				<td valign="middle" width="96%" align="left" class="tblsfondo3">
					<b class="tbltext0">&nbsp; Testo del Messaggio</b>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<span class="tbltext0">
					- Utilizzabile <b><label name="NumCaratteri" id="NumCaratteri">485</label></b> caratteri - </span>
				</td>
				<td height="15" width="2%" valign="middle"><img src="<%=Session("Progetto")%>/images/righinaPDX.jpg"></td> 
			</tr>
			<tr> 
				<td width="2%" class="tbltext"> &nbsp; </td>
				<td align="left" width="98%" valign="middle">
					<textarea name="txtQuestion" onKeyup="JavaScript:CheckLenTextArea(document.FormMod.txtQuestion,NumCaratteri,485)" CLASS="MyTextBox" cols="80" rows="8"><%=Srisposta%></textarea>
			</td>
			</tr>				
			<tr height="17"> 
				<td colspan="4">&nbsp;</td>
			</tr>
		</table>	
	
	   <table border="0" align="center" cellspacing="0" cellpadding="0" width="100%">
				<tr> 
					<td colspan="2" align="center">
						<input type="image" src="<%=Session("progetto")%>/images/conferma.gif" title="Conferma" border="0" align="absBottom" id="image2" name="image2">					
					</td>
				</tr>
		</table>
		<input type="hidden" name="IdCanale" value="<%=ris%>">
		<input type="hidden" name="can" value="<%=ca%>">
		<input type="hidden" name="Modo" value="mod">
		<input type="hidden" name="Oggetto" value="<%=oggetto%>"><br><br>
	</form>
<%

End Sub
	
Sub Modifica()
		'Inserimento nella tabella FRM_AREA
	
	Dim nIdAreaTem, bCanale, sPDt_Dal, sPDt_Al
	Dim sErrore, sql, i, j

	nIdCanale = clng(Request.form("IdCanale"))
	oggetto = Request.Form("oggetto")
	If request.form("can") <> "" Then
		ca	= clng(request.form("can"))
	Else
		ca = ""
	End If
	select case oggetto	
		Case "can"
			sql = "UPDATE frm_area SET "
			sql = sql & " titolo_area ='" & tit & "', desc_area='" & desc & "'" 
			sql = sql & " WHERE id_frm_area =" & nIdCanale 
		Case "dom"
			sql = "UPDATE frm_domanda SET "
			sql = sql & " oggetto ='" & ogg & "', domanda='" & testo & "'" 
			sql = sql & " WHERE id_frm_domanda =" & nIdCanale 
		Case "risp"
			sql = "UPDATE frm_risposta SET "
			sql = sql & " oggetto ='" & ogg & "', risposta='" & testo & "'" 
			sql = sql & " WHERE id_frm_risposta =" & nIdCanale 
	End Select	

	strErrore = EseguiNoC(sql,CC)
%>		
	<br><br>
<%	If strErrore <> "0" Then %>
		<table border="0" cellspacing="0" cellpadding="0" width="100%">
			<tr align="center"> 
				<td class="tbltext3">
					<b>Errore. <%=strErrore%> </b>
		        </td>
			</tr>
		</table>		
		<br><br>
		<table border="0" cellspacing="0" cellpadding="0" width="100%">
			<tr> 
				<td align="center"> 
					<a class="textred" href="javascript:history.back()" onmouseover="window.status =' '; return true">
					<b>Pagina precedente </b></a>
				</td>
				<td align="center"> 
					<a class="textred" href="/Pgm/formazione/FAQ/FAQ_VisCanali.asp" onmouseover="window.status =' '; return true">
					<b>Torna alle Aree </b></a>
				</td>
			</tr>
		</table>
<%	Else	%>
		<table border="0" cellspacing="0" cellpadding="0" width="100%">
			<tr align="center"> 
				<td class="tbltext3">
					<b>Modifica correttamente effettuata.</b>
		        </td>
			</tr>
		</table>		
		<br><br>
<%      If Ca <> "" Then%>			
			<meta HTTP-EQUIV="Refresh" CONTENT="2; URL=/Pgm/formazione/FAQ/FAQ_VisDomande.asp?cat=<%=ca%>">
<%		Else%>
			<meta HTTP-EQUIV="Refresh" CONTENT="2; URL=/Pgm/formazione/FAQ/FAQ_VisCanali.asp">
<%		End If%>

<%	End If
			
End Sub
%>
<!--		MAIN			-->
<%
	Dim sql,sDescr, sOggetto, n, nId
%>	
	<!--#include Virtual="/include/openconn.asp"-->
	<!--#include Virtual="/include/ckProfile.asp"-->
	<!--#include Virtual="/Util/DBUtil.asp"-->	
	<!--#include Virtual="/include/DecCod.asp"-->
	<!--#include Virtual="/include/ControlDateVb.asp"-->
	<!--#include virtual="/include/SysFunction.asp"-->
<%
	
	n		= Request("n")
	sModo	= Request("Modo")
	tit		= replace(request.form("tit"),"'","''")
	desc	= replace(request.form("desc"),"'","''")
	ogg		= replace(request.form("txtTitle"),"'","''")
	testo	= replace(request.form("txtQuestion"),"'","''")
	oggetto = replace(request.form("oggetto"),"'","''")
	
	
	If sModo = "" Then
		Select Case oggetto	
			Case "can"
			Inizio(2)
			ImpostaCan()
			Case "dom"
			Inizio(1)
			ImpostaDom()
			Case "risp"
			ImpostaRis()
		End Select
	Else 	
		Modifica()
	End If
%>	
<!--#include Virtual="/include/closeconn.asp"-->
<!--#include Virtual="/strutt_coda2.asp"-->
<!--	FINE BLOCCO MAIN	-->
