<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include virtual ="/strutt_testa2.asp"-->
<!--#include virtual="util/dbutil.asp"-->
<!--#include virtual ="/include/openconn.asp"-->
<!--#include Virtual="/pgm/formazione/FAQ/FAQ_Intestazione.asp"-->

<script LANGUAGE="JavaScript" src="/Include/help.inc"></script>
<script LANGUAGE="JavaScript" src="/Include/ControlString.inc"></script>
<script LANGUAGE="JavaScript">
	function validateIt()
	 {
		 Valore = TRIM(document.form1.txtArgomento.value)
		 if(Valore.length == 0)
		  {
		  document.form1.txtArgomento.value = "";
		  document.form1.txtArgomento.focus();
		  alert("Inserire l'Oggetto. Grazie");
		  return false;
		  } 

		 if(document.form1.txtArgomento.value.length > 500)
		  {
		  document.form1.txtArgomento.focus();
		  alert("Attenzione!!!Hai superato la lunghezza massima di 500 caratteri nell'Oggetto.");
		  return false;
		  }
					
		  Valore = TRIM(document.form1.txtDomanda.value)
		  if(Valore.length == 0)
		  {
		  document.form1.txtDomanda.value = "";
		  document.form1.txtDomanda.focus();
		  alert("Inserire la Domanda. Grazie");
		  return false;
		  } 

		 if(document.form1.txtDomanda.value.length > 2000)
		  {
		    document.form1.txtDomanda.focus();
		    alert("Attenzione!!!Hai superato la lunghezza massima di 2000 caratteri.");
		    return false;
		  }
				 
		 Valore = TRIM(document.form1.txtRisposta.value)
		 if(Valore.length == 0)
		  {
		  document.form1.txtRisposta.value = "";
		  document.form1.txtRisposta.focus();
		  alert("Inserire la Risposta. Grazie");
		  return false;
		  } 

		 if(document.form1.txtRisposta.value.length > 2000)
		  {
		    document.form1.txtRisposta.focus();
		    alert("Attenzione!!!Hai superato la lunghezza massima di 2000 caratteri.");
		    return false;
		  }

		return true;
	}
</script>
<%
	If not ValidateService(Session("IdUtente"),"GESTIONE FAQ",cc) Then 
		response.redirect "/util/error_login.asp"
	End If

	Dim sModo, sArea, pag, n, R
		
	sModo= Request.Form("modo")
	sArea = Request.Form("sArea")
	pag = Request.Form("pag")
	n = Request.Form("n")
	R = Request.Form("R")
	
	sIdarea = Request.Form("IdCanale")

	'Inizio()
    If sModo = 1 Then
	   Risposta()
    Else 
	   ImpostaPag()
    End If

%>
	<script>
		function Invia(R)
		{	
			if (R == "SI")
				frmElenco.action = "FAQ_ElencoBackR.asp"
			else
				frmElenco.action = "FAQ_ElencoBack.asp"
			frmElenco.submit()
		}	
	</script>
	<form name="frmElenco" method="post" action>
		<input type="hidden" name="sArea" value="<%=sArea%>">
		<input type="hidden" name="R" value="<%=R%>">
		<input type="hidden" name="n" value="<%=n%>">
		<input type="hidden" name="pag" value="<%=pag%>">
		<input type="hidden" name="IdCanale" value="<%=sIdarea%>">
	</form>

	<!--#include virtual ="/include/closeconn.asp"-->
	<!--#include virtual ="/strutt_coda2.asp"-->
<%
	Sub ImpostaPag()
		Dim nIdf, sDomanda, sArgomento, sRisposta, sL1, sL2, sL3, sL4, sL5
		Dim sD1, sD2, sD3, sD4, sD5
		Dim RR, sql
		
		nIdf  = Request.Form("ID_FAQ")
		Set RR = Server.CreateObject("ADODB.Recordset")
		
		sql = "SELECT ID_FAQ, CODICEAREA, ARGOMENTO, DOMANDA, RISPOSTA," &_
				"LINK1,LINK2, LINK3, LINK4, LINK5," &_
				"DESCRIZIONE1, DESCRIZIONE2, DESCRIZIONE3, DESCRIZIONE4, DESCRIZIONE5," &_
				"SITO, DATAR, PROFILO FROM FAQ_UR WHERE ID_FAQ=" &  nIdf
		
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
		RR.open sql,CC,3
		If RR.Eof Then
			Messaggio("Pagina momentaneamente non disponibile.")
			exit sub
		End If
		
		sDomanda  = RR.Fields("DOMANDA")
		sArgomento  = RR.Fields("ARGOMENTO")
		sRisposta  = RR.Fields("Risposta")
		sL1  = RR.Fields("Link1")
		sL2  = RR.Fields("Link2")
		sL3  = RR.Fields("Link3")
		sL4  = RR.Fields("Link4")
		sL5  = RR.Fields("Link5")
		sD1  = RR.Fields("DESCRIZIONE1") & ""
		sD2  = RR.Fields("DESCRIZIONE2") & ""
		sD3  = RR.Fields("DESCRIZIONE3") & ""
		sD4  = RR.Fields("DESCRIZIONE4") & ""
		sD5  = RR.Fields("DESCRIZIONE5") & ""
		
		RR.Close
		set RR = nothing
		
		If sArea = "P" Then		'private
			Response.write "<br><br><TABLE width='100%' border='0'><tr><td align='center' class='tbltext1a'><b>Modifica Faq Privata</b></td></tr></TABLE>" 
		Else					'pubbliche
			Response.write "<br><br><TABLE width='100%' border='0'><tr><td align='center' class='tbltext1a'><b>Modifica Faq Pubblica</b></td></tr></TABLE>" 
		End If
%> 		
		<form action="FAQ_ModificaBack.asp" method="POST" id="form1" name="form1" onsubmit="return validateIt();">
			<input type="hidden" name="modo" value="1">
			<input type="hidden" name="sArea" value="<%=sArea%>">
			<input type="hidden" name="ID_FAQ" value="<%=nIdf%>">
			<input type="hidden" name="R" value="<%=R%>">
			<input type="hidden" name="pag" value="<%=pag%>">
			<input type="hidden" name="n" value="<%=n%>">
		    <input type="hidden" name="IdCanale" value="<%=sIdarea%>">
			 					
			<table align="center" border="0" cellspacing="0" cellpadding="0" width="100%">
				<tr height="16"> 
					<td height="15" width="2%" valign="middle"><img src="<%=Session("Progetto")%>/images/righinaPSX.jpg"></td> 
					<td colspan="2" width="96%" align="left" class="tblsfondo3"><b class="tbltext0">&nbsp; Oggetto</b>
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<span class="tbltext0">- Utilizzabili <b><label name="CaratteriOggetto" id="CaratteriOggetto">500</label></b> caratteri -</span>
					</td>
					<td height="15" width="2%" valign="middle"><img src="<%=Session("Progetto")%>/images/righinaPDX.jpg"></td> 
				</tr>
				<tr height="23"> 
					<td width="2%" class="tbltext"> &nbsp; </td>
					<td width="96%" class="tblsfondo" align="left" colspan="2" valign="middle">
						<textarea name="txtArgomento" onKeyup="JavaScript:CheckLenTextArea(document.form1.txtArgomento,CaratteriOggetto,500)" CLASS="MyTextBox" cols="80" rows="6"><%=server.HTMLEncode(sArgomento)%></textarea>
					</td>
					<td width="2%" class="tbltext"> &nbsp; </td>
				</tr>				
				<tr height="15"> 
					<td colspan="4" valign="middle" class="tbltext"></td> 
				</tr>
			</table>				
			<table align="center" border="0" cellspacing="0" cellpadding="0" width="100%">
				<tr height="17"> 
					<td height="15" width="2%" valign="middle"><img src="<%=Session("Progetto")%>/images/righinaPSX.jpg"></td> 
					<td colspan="2" width="96%" align="left" class="tblsfondo3"><b class="tbltext0">&nbsp; Testo della Domanda</b>
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<span class="tbltext0">- Utilizzabili <b><label name="CaratteriDomanda" id="CaratteriDomanda">2000</label></b> caratteri -</span>
					</td>
					<td height="15" width="2%" valign="middle"><img src="<%=Session("Progetto")%>/images/righinaPDX.jpg"></td> 
				</tr>
				<tr height="23"> 
					<td width="2%" class="tbltext"> &nbsp; </td>
					<td width="96%" class="tblsfondo" align="left" colspan="2" valign="middle">
						<textarea name="txtDomanda" onKeyup="JavaScript:CheckLenTextArea(document.form1.txtDomanda,CaratteriDomanda,2000)" CLASS="MyTextBox" cols="80" rows="6"><%=server.HTMLEncode(sdomanda)%></textarea>
					</td>
					<td width="2%" class="tbltext"> &nbsp; </td>
				</tr>				
				<tr height="15"> 
					<td colspan="4" valign="middle" class="tbltext"></td> 
				</tr>
			</table>	
			<table align="center" border="0" cellspacing="0" cellpadding="0" width="100%">
				<tr height="17"> 
					<td height="15" width="2%" valign="middle"><img src="<%=Session("Progetto")%>/images/righinaPSX.jpg"></td> 
					<td colspan="2" valign="middle" width="96%" align="left" class="tblsfondo3"><b class="tbltext0">&nbsp; Testo della Risposta</b>
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<span class="tbltext0">- Utilizzabili <b><label name="NumCaratteri" id="NumCaratteri">2000</label></b> caratteri -</span>
					</td>
					<td height="15" width="2%" valign="middle"><img src="<%=Session("Progetto")%>/images/righinaPDX.jpg"></td> 
				</tr>
				<tr> 
					<td width="2%" class="tbltext"> &nbsp; </td>
					<td align="left" width="96%" valign="middle">
						<textarea name="txtRisposta" onKeyup="JavaScript:CheckLenTextArea(document.form1.txtRisposta,NumCaratteri,2000)" CLASS="MyTextBox" cols="80" rows="8"><%=server.HTMLEncode(sRisposta)%></textarea>
					</td>
					<td width="2%" class="tbltext"></td>
				</tr>
				<tr height="17"> 
					<td colspan="4">&nbsp;</td>
				</tr>
			</table>	
			<table align="center" border="0" cellspacing="0" cellpadding="0" width="100%">
				<tr height="16"> 
					<td height="15" width="2%" valign="middle"><img src="<%=Session("Progetto")%>/images/righinaPSX.jpg"></td> 
					<td valign="middle" width="12%" align="left" class="tblsfondo3" colspan="2"><b class="tbltext0">&nbsp; Link 1</b></td>
					<td height="15" width="2%" valign="middle"><img src="<%=Session("Progetto")%>/images/righinaPDX.jpg"></td> 
					<td width="32%" class="tbltext"><input type="text" id="text1" name="link1" size="25" maxlength="100" value="<%=sL1%>"></td>
					<td height="15" width="2%" valign="middle"><img src="<%=Session("Progetto")%>/images/righinaPSX.jpg"></td> 
					<td valign="middle" width="16%" align="left" class="tblsfondo3" colspan="2"><b class="tbltext0">&nbsp; Descrizione</b></td>
					<td height="15" width="2%" valign="middle"><img src="<%=Session("Progetto")%>/images/righinaPDX.jpg"></td> 
					<td width="32%" class="tbltext"><input type="text" id="text1" name="desc1" size="20" maxlength="100" value="<%=server.HTMLEncode(sD1)%>"></td>
				</tr>
				<tr height="2"> 
					<td colspan="4" valign="middle" class="tbltext"><img src="<%=Session("Progetto")%>/images/separarighe.gif"></td> 
				</tr>
			</table>
			<table align="center" border="0" cellspacing="0" cellpadding="0" width="100%">
				<tr height="16"> 
					<td height="15" width="2%" valign="middle"><img src="<%=Session("Progetto")%>/images/righinaPSX.jpg"></td> 
					<td valign="middle" width="12%" align="left" class="tblsfondo3" colspan="2"><b class="tbltext0">&nbsp; Link 2</b></td>
					<td height="15" width="2%" valign="middle"><img src="<%=Session("Progetto")%>/images/righinaPDX.jpg"></td> 
					<td width="32%" class="tbltext"><input type="text" id="text1" name="link2" size="25" maxlength="100" value="<%=sL2%>"></td>
					<td height="15" width="2%" valign="middle"><img src="<%=Session("Progetto")%>/images/righinaPSX.jpg"></td> 
					<td valign="middle" width="16%" align="left" class="tblsfondo3" colspan="2"><b class="tbltext0">&nbsp; Descrizione</b></td>
					<td height="15" width="2%" valign="middle"><img src="<%=Session("Progetto")%>/images/righinaPDX.jpg"></td> 
					<td width="32%" class="tbltext"><input type="text" id="text1" name="desc2" size="20" maxlength="100" value="<%=server.HTMLEncode(sD2)%>"></td>
				</tr>
				<tr height="2"> 
					<td colspan="4" valign="middle" class="tbltext"><img src="<%=Session("Progetto")%>/images/separarighe.gif"></td> 
				</tr>
			</table>
			<table align="center" border="0" cellspacing="0" cellpadding="0" width="100%">
				<tr height="16"> 
					<td height="15" width="2%" valign="middle"><img src="<%=Session("Progetto")%>/images/righinaPSX.jpg"></td> 
					<td valign="middle" width="12%" align="left" class="tblsfondo3" colspan="2"><b class="tbltext0">&nbsp; Link 3</b></td>
					<td height="15" width="2%" valign="middle"><img src="<%=Session("Progetto")%>/images/righinaPDX.jpg"></td> 
					<td width="32%" class="tbltext"><input type="text" id="text1" name="link3" size="25" maxlength="100" value="<%=sL3%>"></td>
					<td height="15" width="2%" valign="middle"><img src="<%=Session("Progetto")%>/images/righinaPSX.jpg"></td> 
					<td valign="middle" width="16%" align="left" class="tblsfondo3" colspan="2"><b class="tbltext0">&nbsp; Descrizione</b></td>
					<td height="15" width="2%" valign="middle"><img src="<%=Session("Progetto")%>/images/righinaPDX.jpg"></td> 
					<td width="32%" class="tbltext"><input type="text" id="text1" name="desc3" size="20" maxlength="100" value="<%=server.HTMLEncode(sD3)%>"></td>
				</tr>
				<tr height="2"> 
					<td colspan="4" valign="middle" class="tbltext"><img src="<%=Session("Progetto")%>/images/separarighe.gif"></td> 
				</tr>
			</table>
			<table align="center" border="0" cellspacing="0" cellpadding="0" width="100%">
				<tr height="16"> 
					<td height="15" width="2%" valign="middle"><img src="<%=Session("Progetto")%>/images/righinaPSX.jpg"></td> 
					<td valign="middle" width="12%" align="left" class="tblsfondo3" colspan="2"><b class="tbltext0">&nbsp; Link 4</b></td>
					<td height="15" width="2%" valign="middle"><img src="<%=Session("Progetto")%>/images/righinaPDX.jpg"></td> 
					<td width="32%" class="tbltext"><input type="text" id="text1" name="link4" size="25" maxlength="100" value="<%=sL4%>"></td>
					<td height="15" width="2%" valign="middle"><img src="<%=Session("Progetto")%>/images/righinaPSX.jpg"></td> 
					<td valign="middle" width="16%" align="left" class="tblsfondo3" colspan="2"><b class="tbltext0">&nbsp; Descrizione</b></td>
					<td height="15" width="2%" valign="middle"><img src="<%=Session("Progetto")%>/images/righinaPDX.jpg"></td> 
					<td width="32%" class="tbltext"><input type="text" id="text1" name="desc4" size="20" maxlength="100" value="<%=server.HTMLEncode(sD4)%>"></td>
				</tr>
				<tr height="2"> 
					<td colspan="4" valign="middle" class="tbltext"><img src="<%=Session("Progetto")%>/images/separarighe.gif"></td> 
				</tr>
			</table>
			<table align="center" border="0" cellspacing="0" cellpadding="0" width="100%">
				<tr height="16"> 
					<td height="15" width="2%" valign="middle"><img src="<%=Session("Progetto")%>/images/righinaPSX.jpg"></td> 
					<td valign="middle" width="12%" align="left" class="tblsfondo3" colspan="2"><b class="tbltext0">&nbsp; Link 5</b></td>
					<td height="15" width="2%" valign="middle"><img src="<%=Session("Progetto")%>/images/righinaPDX.jpg"></td> 
					<td width="32%" class="tbltext"><input type="text" id="text1" name="link5" size="25" maxlength="100" value="<%=sL5%>"></td>
					<td height="15" width="2%" valign="middle"><img src="<%=Session("Progetto")%>/images/righinaPSX.jpg"></td> 
					<td valign="middle" width="16%" align="left" class="tblsfondo3" colspan="2"><b class="tbltext0">&nbsp; Descrizione</b></td>
					<td height="15" width="2%" valign="middle"><img src="<%=Session("Progetto")%>/images/righinaPDX.jpg"></td> 
					<td width="32%" class="tbltext"><input type="text" id="text1" name="desc5" size="20" maxlength="100" value="<%=server.HTMLEncode(sD5)%>"></td>
				</tr>
				<tr height="2"> 
					<td colspan="4" valign="middle" class="tbltext"><img src="<%=Session("Progetto")%>/images/separarighe.gif"></td> 
				</tr>
			</table>
			<br><br>
			<table border="0" cellspacing="0" cellpadding="0" width="100%">
				<tr> 
					<td align="center"> 
				       <input type="image" name="Invia" src="<%=Session("Progetto")%>/images/conferma.gif" border="0">
	                </td>
				</tr>
	        </table>
	        <br>
		</form>	
<%		
		Indietro()
	
	End Sub

	Sub Risposta()
		dim nIdf, sLink1, sLink2, sLink3, sLink4, sLink5
		dim sDesc1, sDesc2, sDesc3, sDesc4, sDesc5, sRisposta
		dim sql, sDomanda, sArgomento
	
		nIdf  = Request.Form("ID_FAQ")
		
		sLink1 = Replace(Request.Form("link1"), "'", "''")
		sLink2 = Replace(Request.Form("link2"), "'", "''")
		sLink3 = Replace(Request.Form("link3"), "'", "''")
		sLink4 = Replace(Request.Form("link4"), "'", "''")
		sLink5 = Replace(Request.Form("link5"), "'", "''")
		sDesc1 = Replace(Request.Form("desc1"), "'", "''")
		sDesc2 = Replace(Request.Form("desc2"), "'", "''")
		sDesc3 = Replace(Request.Form("desc3"), "'", "''")
		sDesc4 = Replace(Request.Form("desc4"), "'", "''")
		sDesc5 = Replace(Request.Form("desc5"), "'", "''")
		sRisposta = Replace(Request.Form("txtRisposta"), "'", "''")
		sArgomento = Replace(Request.Form("txtArgomento"), "'", "''")
		sDomanda = Replace(Request.Form("txtDomanda"), "'", "''")

		sql = "UPDATE FAQ_UR SET " &_ 
			"  ARGOMENTO = '" & sArgomento  & "'" &_
			", DOMANDA = '" & sDomanda  & "'" &_
			", RISPOSTA = '" & sRisposta  & "'" &_
		    ", DataR = " & ConvDateToDB(date) &_
			", link1 = '" & sLink1 & "'" &_
			", link2 = '" & sLink2 & "'" &_
			", link3 = '" & sLink3 & "'" &_
			", link4 = '" & sLink4 & "'" &_
			", link5 = '" & sLink5 & "'" &_
			", descrizione1 = '" & sDesc1 & "'" &_
		    ", descrizione2 = '" & sDesc2 & "'" &_
		    ", descrizione3 = '" & sDesc3 & "'" &_
		    ", descrizione4 = '" & sDesc4 & "'" &_
		    ", descrizione5 = '" & sDesc5 & "'" &_
		    ", Profilo = '" & sArea & "'" &_
		    " WHERE ID_FAQ = " & nIdf
		
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
		CC.Execute sql
  
  		if err.number = 0 then
			Messaggio("La risposta � stata modificata correttamente") 
%>			<meta http-equiv="Refresh" content="2; URL=javascript:Invia('<%=ucase(R)%>')">
<%		else
			Messaggio("Si � verificato un errore: " & Err.Description)
			Indietro()
		end if
	
	End Sub

	Sub Messaggio(Testo)
%>
	<br><br>
	<table WIDTH="100%" align="center" BORDER="0">
		<tr> 
			<td align="center" class="tbltext3"><%=Testo%></td>
		</tr>
	</table>
	<br><br>
<%
	End Sub

	Sub Indietro()
%>
	<table WIDTH="100%" align="center" BORDER="0">
		<tr> 
			<td align="center">
				<a class="textred" href="javascript:history.back()"><b>Pagina precedente</b></a>
			</td>
		</tr>
	</table>
	<br><br>
<%
	End Sub
%>



