<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include Virtual="/strutt_testa2.asp"-->
<!--#include Virtual="/include/openconn.asp"-->
<!--#include Virtual="/pgm/formazione/FAQ/FAQ_Intestazione.asp"-->
<script LANGUAGE="JavaScript" src="/Include/help.inc"></script>
<script LANGUAGE="JavaScript" src="/Include/ControlString.inc"></script>	
<script Language="JavaScript">
function Validator() {
				
	Form2.termine.value = TRIM(Form2.termine.value)
	if (Form2.termine.value.length > 100) {
		alert("Si accettano al massimo 100 caratteri nel campo \"Parola chiave\" grazie.");
		theForm.termine.focus();
		return (false);
	}
	if (Form2.termine.value.length <= 0) {
		alert("Digitare almeno un carattere nel campo\"Parola chiave\" grazie.");
		Form2.termine.focus();
		return (false);
	}
	var spazio = "                       "
	if (Form2.termine.value <= spazio) {	
	  	alert("Digitare almeno un carattere nel campo\"termine\" grazie.");
	  	Form2.termine.value='';
	  	Form2.termine.focus();
	  	return (false);
	  }
	
	var checkOK = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz \t\r\n\f";
	var checkStr = Form2.termine.value;
	var allValid = true;
	for (i = 0;  i < checkStr.length;  i++) {
		ch = checkStr.charAt(i);
		for (j = 0;  j < checkOK.length;  j++)
			if (ch == checkOK.charAt(j))
		    break;
			if (j == checkOK.length) {
			  allValid = false;
			  break;
			}
	}
	if (!allValid) {
		alert("Non digitare numeri o lettere accentate nel campo \"Parola chiave\" Grazie.");
		Form2.termine.focus();
		return (false);
	}
	return (true);
}
</script>
<%
	'Inizio()
	ImpostaPag()
%>	
<!--#include Virtual="/include/closeconn.asp"-->
<!--#include Virtual="/strutt_coda2.asp"-->

<%
Sub ImpostaPag()
	Dim sqlFaqUr, rstFaqUr
	Dim nConta, nConta2, sWhere
	nIdCanale = Request("idCanale")
		
	sWhere = ""
		
	If session("login") = "" Then
		sWhere = " AND PROFILO is null"
	End If
		
	sqlFaqUr = "SELECT COUNT(id_faq) AS RecCount FROM FAQ_UR " &_ 
		       "WHERE SITO='Si' " &_ 
		       "AND ID_FRM_AREA = " & nIdCanale &_
		       "AND RISPOSTA IS NOT NULL " & sWhere
		
	Set rstFaqUr=Server.CreateObject("ADODB.Recordset")
		
'PL-SQL * T-SQL  
SQLFAQUR = TransformPLSQLToTSQL (SQLFAQUR) 
	rstFaqUr.open sqlFaqUr, CC,3
	nConta = rstFaqUr("RecCount")
	rstFaqUr.Close
	
	Set rstFaqUr1=Server.CreateObject("ADODB.Recordset")	  
	sqlFaqUr1 = "SELECT COUNT(id_faq) AS RecCount2 FROM FAQ_UR " &_ 
		        "WHERE PROFILO IS NULL and  SITO='No' " & sWhere
		
'PL-SQL * T-SQL  
SQLFAQUR1 = TransformPLSQLToTSQL (SQLFAQUR1) 
	rstFaqUr1.open sqlFaqUr1, CC,3
	nConta2= rstFaqUr1("RecCount2")
	rstFaqUr1.Close
	
	Set rstFaqUr = Nothing
	Set rstFaqUr1= Nothing
%>
<br><br>		
<table border="0" width="100%" cellspacing="0" cellpadding="0">
	<tr> 
		<td>&nbsp;&nbsp;</td>
	</tr>
	<tr> 
		<td colspan="2" align="center" class="tbltext3">Domande pervenute <% =nConta %></td>
	</tr>
</table>
<br>

<form method="POST" action="FAQ_RisultatoFront.asp" name="Form2" ONSUBMIT="return Validator()">
<table border="0" width="100%" cellspacing="0" align="center">
		<tr>
			<td colspan="4" align="center" class="tbltext1a"><b><br>Ricerca per Parola chiave<br><br><br></b></td>
		</tr>
		<tr>
		    <td nowrap width="7%" align="center" class="tbltext1"><b>Parola chiave :&nbsp;</b></td>
			<td nowrap><input type="text" name="termine" size="50" maxlength="70"></td>
			<td nowrap width="5%" align="center"><input type="image" name="B1" src="<%=Session("Progetto")%>/images/conferma.gif" border="0" value="Conferma"></td>
        </tr>
</table>
		<input type="hidden" name="IdCanale" value="<%=nIdCanale%>">
		<input type="hidden" name="Page" value="FAQ_RisultatoFront">
</form>
<br><br>
<form name="frmDomanda" method="post" action="/Pgm/formazione/Faq/FAQ_DomandaFront.asp">
	<input type="hidden" name="IdCanale" value="<%=nIdCanale%>">
	<input type="hidden" name="Page" value="FAQ_DomandaFront">
</form>
<form name="frmLista" method="post" action="/Pgm/formazione/Faq/FAQ_ListaFront.asp">
	<input type="hidden" name="IdCanale" value="<%=nIdCanale%>">
	<input type="hidden" name="Page" value="FAQ_ListaFront">
</form>
<table width="100%" cellpadding="0" cellspacing="0">
	<tr> 
		<td colspan="2" width="50%" align="center">
			<a class="textred" href="javascript:frmDomanda.submit()"><b>Invia una nuova  domanda</b></a>
		</td>
		<td width="50%" height="17" align="center">
			<a class="textred" href="javascript:frmLista.submit()"><b>Torna alla lista delle domande</b></a>
		</td>
	</tr>
</table>
<br><br>
<%
End Sub
%>
