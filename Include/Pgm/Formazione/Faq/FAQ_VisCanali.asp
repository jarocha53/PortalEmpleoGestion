<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<style type="text/css">
	.ui-menu .ui-icon{
 		*left:-2em !important;
 	}
</style>
<script LANGUAGE="JavaScript" src="/Include/help.inc"></script>
<script language="Javascript">
	function VisDomande(canale,area) {	
		document.frmVisDom.IdCanale.value = canale;
		document.frmVisDom.IdAreaTem.value = area;
		document.frmVisDom.submit();
	}
	
	function Visualizza(canale,area) {
		document.frmVis.IdCanale.value = canale;
		document.frmVis.IdAreaTem.value = area;
		document.frmVis.submit();
	}
		
	function Abilita(canale,area) {	
		document.frmIscr.IdCanale.value = canale;
		document.frmIscr.IdAreaTem.value = area;
		document.frmIscr.submit();
	}

	function Modifica(canale,area) {	
		document.frmMod.IdCanale.value = canale;
		document.frmMod.IdAreaTem.value = area;
		document.frmMod.submit();
	}
	
	function Inserisci(area) {	
		document.frmIns.IdAreaTem.value = area;
		document.frmIns.submit();
	}
	
</script>
 
<%
Sub ImpostaPag()	

	Dim rsArea, SQLArea, x
	Set rsArea=Server.CreateObject("ADODB.Recordset")
	'********************************************************************
	' Chi pu� visualizzare tutti i tipi di FAQ deve avere impostato il 
	' mask a 15. Il caso da testare � il numero 8 [che indica cancellazione].
	'*******************************************************************
	
	If Session("login") = "" Then
		SQLArea = "SELECT f.id_area, f.id_frm_area, f.desc_area, f.titolo_area, f.img_area, f.tipo, f.dt_tmst, a.titoloarea " &_
			          "FROM frm_area f, area a WHERE f.id_area = a.id_area AND tipo = 'Q' AND f.id_frm_area " &_
			          "IN (SELECT f.id_frm_area FROM frm_iscrizione WHERE cod_ruolo IS NULL AND " &_
					  ConvDateToDbS(Date()) & " BETWEEN DT_DAL AND nvl(DT_AL, to_date('31/12/9999', 'dd/mm/yyyy'))) ORDER BY f.id_area"			
	Else
		If ckProfile(Session("mask"),8) Then
			SQLArea = "SELECT f.id_area, f.id_frm_area, f.desc_area, f.titolo_area, f.img_area, f.tipo, f.dt_tmst, a.titoloarea " &_
			          "FROM frm_area f, area a WHERE f.id_area = a.id_area AND tipo = 'Q' ORDER BY f.id_area" 
		Else
			SQLArea = "SELECT f.id_area, f.id_frm_area, f.desc_area, f.titolo_area, f.img_area, f.tipo, f.dt_tmst, a.titoloarea " &_
			          "FROM frm_area f, area a WHERE tipo = 'Q' AND f.id_area = a.id_area AND f.id_frm_area " &_
			          "IN (SELECT f.id_frm_area FROM frm_iscrizione WHERE tipo = 'Q' AND " &_
			          "((cod_ruolo='" & Session("Rorga") & "' AND idgruppo IS NULL) " &_
					  "OR (cod_ruolo IS NULL) OR (cod_ruolo='" & Session("Rorga") & "' AND idgruppo = " & Session("idgruppo") & ")) " &_
			          "AND " & ConvDateToDbS(Date()) & " BETWEEN DT_DAL AND nvl(DT_AL, to_date('31/12/9999', 'dd/mm/yyyy')) ) ORDER BY f.id_area"			
		End If
	End If
'PL-SQL * T-SQL  
SQLAREA = TransformPLSQLToTSQL (SQLAREA) 
	rsArea.Open SQLArea, CC,3

	If Not rsArea.EOF Then
		nIdArea = 0
%>
		<table border="0" cellspacing="0" cellpadding="0" width="100%" align="center">
			<tr> 
				<td> 
					<h2 class="tbltext" align="center"><br> 
						Benvenuto nella sezione della community dell'area <br> 
						<%=Session("TIT")%>
						dedicata alle FAQ.
					</h2><br>
					<h2 align="center" class="tbltext">SCEGLI UN CANALE</span>
					</h2>
				</td>
			</tr>
		</table>

		<table border="0" cellspacing="0" cellpadding="0" width="100%" align="center">
<%
		Do While Not rsArea.eof 
			nConfronta = CLng(rsArea("id_area"))	

			If clng(nIdArea) <> clng(nConfronta) Then	
%>
				<tr>
					<td colspan="6" height="1">&nbsp;</td> 
				</tr>
				<tr>
					<td class="tbltext1" colspan="6">
						<b><%=Server.HTMLEncode(rsArea("titoloarea"))%></b>
					
<%				If ckProfile(Session("mask"),8) Then %>
					<a class="textred" href="javascript:Inserisci('<%=rsArea("id_area")%>')"><img align="middle" src="/images/SistDoc/CreaDoc.gif" alt="inserisci un nuovo canale" border="0" width="23" height="21"></a>			
<%				End If %> 
					</td>
				</tr>
				<tr>
					<td colspan="6" height="2">&nbsp;</td> 
				</tr>
				<tr>
<%
				nIdArea = rsArea("id_area")
			End if

			nIdFrmArea = clng(rsArea("id_frm_area"))
			
			Set rsNumFAQ=Server.CreateObject("ADODB.Recordset")
			SQLNumFAQ = "SELECT count(id_faq) FROM faq_ur WHERE tipo = 'Q' AND risposta IS NULL AND id_frm_area = " & nIdFrmArea
			'PL-SQL * T-SQL  
			SQLAREA = TransformPLSQLToTSQL (SQLAREA) 
			rsNumFAQ.Open SQLArea, CC,3
			If Not rsNumFAQ.EOF Then
				nFaqPresenti = rsNumFAQ.RecordCount
			Else
				nFaqPresenti = 0
			End If			
			If ckProfile(Session("mask"),4) Then 
%>
				<td width="1%" align="center">
					<a href="javascript:Modifica('<%=nIdFrmArea%>','<%=nIdArea%>')"><img src="/images/SistDoc/ModDoc.gif" alt="modifica nome e descrizione del canale" border="0" width="23" height="21">
				</td>	
				<td width="1%" align="center">
					<a href="javascript:Abilita('<%=nIdFrmArea%>','<%=nIdArea%>')"><img src="/images/utente.gif" alt="modifica l'abilitazione al canale" border="0" width="20" height="20">
				</td>		
<%			Else  %>
				<td width="1%" align="center">&nbsp;</td>
				<td width="1%" align="center">&nbsp;</td>	
<%			End If  %>
				<td height="15" width="2%" valign="middle"><img src="<%=Session("Progetto")%>/images/righinaPSX.jpg"></td> 
					
				<td width="94%" class="tblsfondo3">	
<%          If ckProfile(Session("mask"),4) Then %>									 
				<a class="tbltext0" href="javascript:VisDomande('<%=nIdFrmArea%>','<%=nIdArea%>')" onmouseover="window.status =' '; return true">
<%			Else  %>
				<a class="tbltext0" href="javascript:Visualizza('<%=nIdFrmArea%>','<%=nIdArea%>')" onmouseover="window.status =' '; return true">							
<%			End If  %>
				<b><%=Server.HTMLEncode(rsArea("titolo_area"))%></b>
<%			If Not ISNULL(rsArea("img_area")) Then %>
				<%=rsArea("img_area")%>
<%			End If %>
					</a>
				<td height="15" width="2%" valign="middle"><img src="<%=Session("Progetto")%>/images/righinaPDX.jpg"></td>
			</tr>
			<tr>
				<td width="1%" align="center">&nbsp;</td>
				<td width="1%" align="center">&nbsp;</td>	
				<td width="98%" colspan="3">
<%          If ckProfile(Session("mask"),4) Then %>									 
				<span>&nbsp; &nbsp;<a class="tbltext" href="javascript:VisDomande('<%=nIdFrmArea%>','<%=nIdArea%>')" onmouseover="window.status =' '; return true"><%= Server.HTMLEncode(rsArea("desc_area")) %></a></span>
<%			Else  %>
				<span>&nbsp; &nbsp;<a class="tbltext" href="javascript:Visualizza('<%=nIdFrmArea%>','<%=nIdArea%>')" onmouseover="window.status =' '; return true"><%= Server.HTMLEncode(rsArea("desc_area")) %></a></span>
<%			End If  %>
				</td>
			</tr>
			<tr>
				<td colspan="6" height="2" valign="middle" background="<%=Session("Progetto")%>/images/separarighe.gif"></td> 
			</tr>
<% 
			rsArea.MoveNext()
		Loop
%> 
		</table>
<%	Else %>
	<br><br><br>
	<table border="0" cellspacing="0" cellpadding="0" width="100%" align="center">
		<tr align="center"> 
			<td class="tbltext3"><b class="size">
				Actualmente no se registran preguntas frecuentes</b>
			</td>
		</tr>
	</table>
	<br><br><br>
<%	End If 
	If ckProfile(Session("mask"),8) Then %>
		<br>
		<table border="0" cellspacing="0" cellpadding="0" width="100%">
			<tr align="center"> 
				<td>
					<a class="textred" href="/Pgm/formazione/Faq/FAQ_InsIscrizione.asp"><b> Inserisci Nuovo Canale</b></a>
				</td>
			</tr>
		</table><br>
<%	End If %>
	<form name="frmVisDom" method="post" action="/Pgm/formazione/Faq/FAQ_ElencoBack.asp">
		<input type="hidden" name="IdCanale">
		<input type="hidden" name="IdAreaTem">
		<input type="hidden" name="Page" value="FAQ_ElencoBack">
	</form>
	<form name="frmVis" method="post" action="/Pgm/formazione/Faq/FAQ_ListaFront.asp">
		<input type="hidden" name="IdCanale">
		<input type="hidden" name="IdAreaTem">
		<input type="hidden" name="Page" value="FAQ_ListaFront">
	</form>
	<form method="post" name="frmIscr" action="/Pgm/Formazione/Faq/FAQ_ModIscrizione.asp">
		<input type="hidden" name="IdCanale">
		<input type="hidden" name="IdAreaTem">
	</form>
	<form method="post" name="frmMod" action="/Pgm/Formazione/Faq/FAQ_Modifica.asp">
		<input type="hidden" name="IdCanale">
		<input type="hidden" name="IdAreaTem">
		<input type="hidden" name="oggetto" value="can">
	</form>	
	<form method="post" name="frmIns" action="/Pgm/Formazione/Faq/FAQ_InsIscrizione.asp">
		<input type="hidden" name="IdAreaTem">
	</form>	
	<table border="0" width="100%">
		<tr>
			<td align="center">
				<a href="javascript:document.location='<%=Session("Progetto")%>/home.asp'">
				<img src="<%=Session("Progetto")%>/images/indietro.gif" border="0"></a>
			</td>
		</tr>
	</table>			
<%
End Sub %>

<!--	FINE BLOCCO ASP		-->
<!--		MAIN			-->

<!--#include Virtual="/strutt_testa2.asp"-->
<!--#include Virtual="/include/openconn.asp"-->	
<!--#include Virtual="/include/ckProfile.asp"-->	
<!--#include Virtual="/Util/DBUtil.asp"-->
<%
If Session("login") <> "" Then
	If not ValidateService(Session("IdUtente"),"Gestione FAQ",cc) Then 
		response.redirect "/util/error_login.asp"
	End If
End If


%>
<br>
<table border="0" width="100%" cellspacing="0" cellpadding="0" height="73">
	<tr>
		<td width="500" background="<%=Session("Progetto")%>/images/titoli/Community2b.gif" height="73" valign="bottom" align="right">
			<table border="0" width="500" height="30" cellspacing="0" cellpadding="0">
				<tr>
					<td valign="top" align="right"><b class="tbltext1">FAQ</b></td>
					<td valign="top" align="left" width="18">
					<% 
						if ckProfile(Session("mask"),8) then
					%>		<a href="Javascript:Show_Help('/Pgm/help/Formazione/Faq/FAQ_VisCanali_1')" name onmouseover="javascript:status='' ; return true">
							<img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0" width="18" height="18" alt="per maggiori informazioni"></a>
					<%  else %>
							<a href="Javascript:Show_Help('/Pgm/help/Formazione/Faq/FAQ_VisCanali')" name onmouseover="javascript:status='' ; return true">
							<img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0" width="18" height="18" alt="per maggiori informazioni"></a>
					<%  end if %>
					</td>
				</tr>	
			</table>
		</td>
	</tr>
</table>
<%

ImpostaPag()
%>
<!--#include Virtual="/include/closeconn.asp"-->
<!--#include Virtual="/strutt_coda2.asp"-->
<!--FINE BLOCCO MAIN-->
