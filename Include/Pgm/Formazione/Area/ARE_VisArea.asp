<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%
Sub Inizio()
	dim sFunzione
	dim sTitolo
	dim sCommento
	dim bCampiObbl
	
	sFunzione = ""
	sTitolo = "AREE"
	sCommento = "<b>Elenco delle Aree Tematiche</b>.<br>Per inserire una nuova area utilizza il relativo collegamento, per modificare un'area fai clic sul titolo corrispondente.<BR>" 	
	bCampiObbl = false
	sHelp = "/Pgm/help/Formazione/Area/ARE_VisArea/"	
%>
	<!--#include virtual="Include/SetTestata.asp"-->
<%
End Sub
'----------------------------------------------------------------------
Sub ElencoA()

dim sSQL
dim rsArea
dim nArea
sSql = "Select ID_AREA, CODICEAREA, TITOLOAREA, DESCRIZIONEAREA from AREA order by TITOLOAREA"

'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
set rsArea = CC.execute(sSQL)

 	If rsArea.EOF then 
		%>
		<table border="0" cellspacing="0" cellpadding="0" width="500">
		<tr>
			<td align="middle">
				<span class="tbltext3"><b>Non sono presenti Aree</b></span>
			</td>
		</tr>
		</table>
		<%	
	else 
		nArea = 0
		%>
		<table border="0" cellspacing="1" cellpadding="2" width="500">
			<tr class="sfondocomm" height=20>
		        <td width=50 align=center><b>Codice</b></td>
		        <td width=150><b>Titolo</b></td>
		        <td width=300><b>Descrizione</b></td>
			</tr>
		<%
		do while not rsArea.eof
			nArea = nArea+1
		%>
			<tr class="tblsfondo">
				<td class="tblDett">
					<center><%=rsArea("CODICEAREA")%></center>
				</td>
				<form action="ARE_ModArea.asp" method=post name="frmArea<%=nArea%>" id="frmArea<%=nArea%>">
				<input type=hidden value="<%=rsArea("ID_AREA")%>" name="hIdArea" id="hIdArea">
				<td align="left">
					<a href="javascript:frmArea<%=nArea%>.submit();" class="tblAgg" onmouseover="javascript:window.status='' ; return true">
						<%=rsArea("TITOLOAREA")%>
					</a>
				</td>
				</form>
				<td class="tbldett">
					<%=rsArea("DESCRIZIONEAREA")%>
				</td>
			</tr>	
		<%
			rsArea.movenext
		loop
		%>
		</table>
		<%
	end if		
rsArea.close
set rsArea = nothing
%>
<br>
<table width="500" border="0" cellpadding="2" cellspacing="2">
	<tr class="tbltext">
		<td align="center">
		<%
			PlsLinkRosso "ARE_InsArea.asp","Nuova Area"
		%>
		</td>
	</tr>
</table>
<%
End sub
'----------------------------------------------------------------------
'M A I N
%>
<!--#include virtual="/strutt_testa2.asp"-->
<!--#include virtual="/include/openconn.asp"-->
<!--#include virtual="/include/SetPulsanti.asp"-->
<%	
	Inizio()
	ElencoA()
%>
<!--#include virtual="/include/closeconn.asp"-->
<!--#include virtual="/strutt_coda2.asp"-->
