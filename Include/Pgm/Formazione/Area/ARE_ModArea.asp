<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%
Sub ControlliJavaScript()
%>
<script language="Javascript">
function ValidaArea(frmArea)
	{
	if (frmArea.txtCodArea.value == "")
		{
		frmArea.txtCodArea.focus();
		alert("Inserire il Codice Area");
		return (false);
	  	}
	if (frmArea.txtTitArea.value == "")
		{
		frmArea.txtTitArea.focus();
		alert("Inserire il Titolo Area");
		return (false);
	  	}
	if (frmArea.txtDescArea.value == "")
		{
		frmArea.txtDescArea.focus();
		alert("Inserire la Descrizione Area");
		return (false);
	  	}

	return (true);
	}

</script>
<%
End Sub
'-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
Sub Inizio()
	dim sFunzione
	dim sTitolo
	dim sCommento
	dim bCampiObbl
	
	sFunzione = ""
	sTitolo = "AREA"
	sCommento = "<b>Modifica dell'Area Tematica</b>.<br>Puoi sovrascrivere i campi e fare clic su <B>Invia</B>.<br>"
	bCampiObbl = true
	sHelp = "/Pgm/help/Formazione/Area/ARE_ModArea/"	
%>
	<!--#include virtual="/include/SetTestata.asp"-->
<%
End Sub
';););););););););););););););););););););););););););););););););););););););)
Sub ImpostaPag()
dim nIdArea
dim sSQL, rsArea
		
	nIdArea = Request.Form("hIdArea")
	
	sSql =	"Select ID_AREA, CODICEAREA, TITOLOAREA, DESCRIZIONEAREA, DT_TMST from AREA " &_
			"Where ID_AREA = " & nIdArea & " " &_
			"order by TITOLOAREA"
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	set rsArea = cc.execute(sSQL)
	
	if rsArea.EOF then
		PagErrore()
	else
%>
<form METHOD="POST" onsubmit="return ValidaArea(this)" name="frmArea" action="ARE_CnfArea.asp">
<input type=hidden name="hIdArea" id="hIdArea" value="<%=rsArea("ID_AREA")%>">
<input type=hidden name="hDtTmst" id="hDtTmst" value="<%=rsArea("DT_TMST")%>">
<input type=hidden name="hMod" id="hMod" value="MOD">
	<table border="0" cellspacing="2" cellpadding="1" width="500">
		<tr> 
			<td align="left" nowrap class="tbltext1FAD">
				<b>Codice</b>*
			</td>
			<td>
				<input type="text" name="txtCodArea" id="txtCodArea" class="textblackaFAD" size="2" maxlength="2" value="<%=rsArea("CODICEAREA")%>">
			</td>
		</tr>
		<tr> 
			<td align="left" nowrap class="tbltext1FAD">
				<b>Titolo</b>*
			</td>
			<td>
				<input type="text" name="txtTitArea" id="txtTitArea" class="textblackaFAD" size="30" maxlength="30" value="<%=rsArea("TITOLOAREA")%>">
			</td>
		</tr>
		<tr> 
			<td align="left" nowrap class="tbltext1FAD">
				<b>Descrizione</b>*
			</td>
			<td>
				<input type="text" name="txtDescArea" id="txtDescArea" class="textblackaFAD" size="50" maxlength="100" value="<%=rsArea("DESCRIZIONEAREA")%>">
			</td>
		</tr>
		</tr>
	</table>
	<br>
	<table border="0" width="500" cellpadding="0" cellspacing="0">
		<tr>
			<td align="right" width="50%"> 
				<%
					PlsIndietro()
				%>
			</td>
			<td align="left" width="50%"> 
				<%
					PlsInvia("InviaM")
				%>
			</td>
		</tr>
	</table>
	<br>
</form>
<%
	End if
	rsArea.Close
	set rsArea = nothing
End Sub
';););););););););););););););););););););););););););););););););););););););)
'M A I N
%>
<!--#include virtual="/strutt_testa2.asp"-->
<!--#include virtual="/include/openconn.asp"-->
<!--#include virtual="/include/SetPulsanti.asp"-->
<%	
ControlliJavaScript()
Inizio()
ImpostaPag()
%>
<!--#include virtual ="/include/closeconn.asp"-->
<!--#include virtual="/strutt_coda2.asp"-->
