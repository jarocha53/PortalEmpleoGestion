<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%
Sub ControlliJavaScript()
%>
<script language="Javascript">
<!--#include virtual="/include/ControlString.inc"-->
function ValidaMediateca(frmMedia)
	{
	//Controllo dei campi obbligatori
	if (frmMedia.txtDescrizione.value == "")
		{
		frmMedia.txtDescrizione.focus();
		alert("Il campo 'Descrizione' � obbligatorio.");
		return (false);
	  	}
		
	if (frmMedia.cboFlAct.value == "")
		{
		frmMedia.cboFlAct.focus();
		alert("Il campo 'Azione' � obbligatorio.");
		return (false);
	  	}
	//Controllo che il PATH o il LINK sia inserito
	if ((frmMedia.txtPathMatDid.value == "") && (frmMedia.txtLinkMatDid.value == ""))
		{
		frmMedia.txtPathMatDid.focus();
		alert("Inserire il PATH o il LINK.");
		return (false);
	  	}

	if ((frmMedia.txtPathMatDid.value != "") && (frmMedia.txtLinkMatDid.value != ""))
		{
		frmMedia.txtPathMatDid.focus();
		alert("Inserire il PATH o il LINK.");
		return (false);
	  	}
	// Controllo che se � stato valorizzato il LINK il Flag non sia impostato su preleva o su consulta/preleva		
	if ((frmMedia.txtLinkMatDid.value != "") && (frmMedia.cboFlAct.value != "0"))
		{
		frmMedia.cboFlAct.focus();
		alert("Non � possibile selezionare un'azione se � stato indicato il LINK");
		return (false);
	  	}
	if ((frmMedia.txtPathMatDid.value != "") && (frmMedia.cboFlAct.value == "0"))
		{
		frmMedia.cboFlAct.focus();
		alert("Selezionare un'azione");
		return (false);
	  	}
	return (true);
	}

	function LinkMatDid()
	{
		if (frmInsMediateca.txtLinkMatDid.value != "")
		{
		f = 'http://'+frmInsMediateca.txtLinkMatDid.value
		MyNewWindow=window.open(f, "Finestra"); 
		}
		else
		{
			frmInsMediateca.txtLinkMatDid.focus();
			alert("Inserire il LINK da testare.");
		}
	}
	
	var finestra
	function PathMatDid()
	{
		if (finestra != null ) 
		{
			finestra.close(); 
		}
		f='MED_InsPathMatDid.asp'
		finestra=window.open(f,"","width=550,height=450,location=no,menubar=no,scrollbars=yes,resizable=no")
	}
</script>
<%
End Sub
'-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
Sub Inizio()
	dim sFunzione
	dim sTitolo
	dim sCommento
	dim bCampiObbl
	
	sFunzione = "GESTIONE CORSI"
	sTitolo = "MEDIATECA"
	sCommento = "Compilare i campi sottostanti e premere <b>invia</B> per memorizzare le informazioni."
	bCampiObbl = true
	sHelp = "/Pgm/help/Formazione/Mediateca/MED_InsMediateca/"		
%>
	<!--#include virtual="/include/SetTestata.asp"-->
<%
End Sub
';););););););););););););););););););););););););););););););););););););););)
Sub ImpostaPag()
dim nIdCorso
dim nIdElem
dim sSQL, rsMatDid
		
	nIdCorso = Request("idC")
	nIdElem = Request("idE")	
	sTitolo = Request("Desc")
%>
<form METHOD="POST" onsubmit="return ValidaMediateca(this)" name="frmInsMediateca" action="MED_CnfMediateca.asp">

	<input type="hidden" name="hIdCorso" id="hIdCorso" value="<%=nIdCorso%>">
	<input type="hidden" name="Desc" id="Desc" value="<%=sTitolo%>">
	
	<input type="hidden" name="hIdElemento" id="hIdElemento" value="<%=nIdElem%>">
	<input type="hidden" name="hOper" id="hOper" value="INS">
	<table border="0" width="500" cellpadding="0" cellspacing="0">
		<tr>
			<td width="100" class="tbltext1Fad"><b>Titolo</b></td>
			<td class="textBlack"><b><%=sTitolo%></b></td>
		</tr>
	</table>
	<br>
	<table border="0" cellspacing="2" cellpadding="1" width="500">
		<tr> 
			<td align="left" nowrap class="tbltext1FAD">
				<b>Descrizione</b>*
			</td>
			<td>
				<span class="tbltext1FAD">&nbsp;- Utilizzabili <label name="lblNumCar1" id="lblNumCar1">512</label> caratteri -</span><br>
				<textarea name="txtDescrizione" class="textblackFAD" cols="50" rows="3" OnKeyUp="JavaScript:CheckLenTextArea(txtDescrizione,lblNumCar1,512)"></textarea>
			</td>
		</tr>
		<tr> 
			<td align="left" nowrap class="tbltext1FAD">
				<b>Path</b>
			</td>
			<td>
				<input type="text" name="txtPathMatDid" id="txtPathMatDid" class="textblackaFAD" size="50" maxlength="512" readonly>
   				<a href="javascript:PathMatDid()">
   					<img src="<%=Session("progetto")%>/images/bullet1.gif" border="0" alt="Selezionare il PATH del materiale didattico" onmouseover="javascript:window.status='' ; return true">
   				</a>
			</td>
		</tr>
		<tr> 
			<td align="left" nowrap class="tbltext1FAD">
				<b>Link</b>
			</td>
			<td>
				<input type="text" name="txtLinkMatDid" id="txtLinkMatDid" class="textblackaFAD" size="50" maxlength="512">
   				<a href="javascript:LinkMatDid()">
   					<img src="<%=Session("progetto")%>/images/testlink.gif" border="0" alt="Testa il LINK del materiale didattico" onmouseover="javascript:window.status='' ; return true">
   				</a>
			</td>
		</tr>
		<tr> 
			<td align="left" nowrap class="tbltext1FAD">
				<b>Azione</b>*
			</td>
			<td>
				<select name="cboFlAct" id="cboFlAct" class="textblackaFAD">
					<option value="0"></option>
					<option value="1">Consulta</option>
					<option value="2">Preleva</option>
					<option value="4">Consulta e Preleva</option>
				</select>
			</td>
		</tr>

	</table>
	<br>
	<table border="0" width="500" cellpadding="0" cellspacing="0">
		<tr>
			<td align="right"> 
				<%
					PlsIndietro()
				%>
			</td>
			<td align="left"> 
				<%
					PlsInvia("InviaM")
				%>
			</td>
		</tr>
	</table>
	<br>
</form>
<%
End Sub
';););););););););););););););););););););););););););););););););););););););)
'M A I N
%>
<!--#include virtual="/strutt_testa2.asp"-->
<!--#include virtual="/include/openconn.asp"-->
<!--#include virtual="/include/SetPulsanti.asp"-->
<%	
ControlliJavaScript()
Inizio()
ImpostaPag()
%>
<!--#include virtual ="/include/closeconn.asp"-->
<!--#include virtual="/strutt_coda2.asp"-->
