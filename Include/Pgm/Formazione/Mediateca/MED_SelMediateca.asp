<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%
Sub ControlliJavaScript()
%>
<script language=javascript>
function Verifica(frmMed)
{
	if ((frmMed.idC.value == "") && (frmMed.idE.value == ""))
	{
		alert('Selezionare un Corso o un Modulo');
		frmMed.idC.focus();
		return false;
	}
	if ((frmMed.idC.value != "") && (frmMed.idE.value != ""))
	{
		alert('Selezionare un Corso o un Elemento');
		frmMed.idC.focus();
		return false;
	}
	return true;
}
</script>
<%
End Sub
';););););););););););););););););););););););););););););););););););););););)
Sub Inizio()
	dim sFunzione
	dim sTitolo
	dim sCommento
	dim bCampiObbl
	
	sFunzione = "GESTIONE MEDIATECA"
	sTitolo = "MEDIATECA"
	sCommento = "Selezionare il corso oppure il modulo al quale associare il materiale"
	bCampiObbl = false
	sHelp = "/Pgm/help/Formazione/Mediateca/MED_SelMediateca/"	
%>
	<!--#include virtual="/include/SetTestata.asp"-->
<%
End Sub
';););););););););););););););););););););););););););););););););););););););)
Sub ImpostaPagina()
dim sSQL
dim rsCorso, rsElemento
%>
<form action="MED_VisMediateca.asp" method="post" name="frmSelMediateca" onsubmit="return Verifica(this)">
	<table border="0" width="500" cellpadding="2" cellspacing="2">
		<tr> 
			<td align="left" class="tbltext1">
				<strong>Seleziona il Corso</strong>
			</td>
			<td align="left" width="60%" class="tbltext1">
<%
'sSQL =	"SELECT c.id_corso, c.tit_corso " &_
'		"FROM corso c, materialedidattico m " &_
'		"WHERE c.id_corso = m.id_corso " &_
'		"ORDER BY desc_materialedid"

			sSQL =	"SELECT c.id_corso, c.tit_corso " &_
					"FROM corso c"
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
			set rsCorso = CC.execute(sSQL)
%>
				<select name="idC" class="textblack" id="idC">
					<option value=""></option>
<%
				do while not rsCorso.EOF 
%>
					<option value="<%=rsCorso("id_corso")%>||<%=strHTMLEncode(mid(rsCorso("tit_corso"),1,40))%>"><%=strHTMLEncode(mid(rsCorso("tit_corso"),1,40))%></option>
<%						
					rsCorso.movenext
				loop
%>					
				</select>
<%		          	
			rsCorso.close
			set rsCorso = nothing
%> 
			</td>
		</tr>
		<tr> 
			<td align="left" class="tbltext1" colspan=2>
				&nbsp;
			</td>
		</tr>	
		<tr> 
			<td align="left" class="tbltext1">
				<strong>Seleziona il Modulo</strong>
			</td>
			<td align="left" width="60%" class="tbltext1">
<%
'sSQL =	"SELECT id_materialedid, desc_materialedid " &_
'		"FROM elemento e, materialedidattico m " &_
'		"WHERE e.id_elemento = m.id_elemento " &_
'		"ORDER BY desc_materialedid"
		sSQL =	"SELECT id_elemento, titolo_elemento " &_
				"FROM elemento e"

'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
			set rsElemento = CC.execute(sSQL)
%>
				<select name="idE" class="textblack" id="idE">
					<option value=""></option>
			<%do while not rsElemento.EOF 
%>
					<option value="<%=rsElemento("id_elemento")%>||<%=strHTMLEncode(mid(rsElemento("titolo_elemento"),1,40))%>"><%=strHTMLEncode(mid(rsElemento("titolo_elemento"),1,40))%></option>
<%						
					rsElemento.movenext
			loop%>					
				</select>
<%		          	
			rsElemento.close
			set rsElemento = nothing
%> 
			</td>
		</tr>
		<tr>
			<td align=center colspan=2>
				&nbsp;
			</td>
		</tr>
		<tr>
			<td align=center colspan=2>
				<% PlsInvia("Invia") %>				
			</td>
		</tr>
	</table>
</form>
<%

End Sub
';););););););););););););););););););););););););););););););););););););););)
';););););););););););););););););););););););););););););););););););););););)
'M A I N
%>
<!--#include virtual="/strutt_testa2.asp"-->
<!--#include virtual="/include/openconn.asp"-->
<!--#include virtual="/include/SetPulsanti.asp"-->
<!--#include virtual="/include/HTMLEncode.asp"-->
<!--#include virtual="/include/EsistenzaCorsi.asp"-->
<%
ControlliJavaScript()
Inizio()
sControlla = EsistenzaCorsi()
if  sControlla = "0" then
	sControlla = EsistenzaModuli()
	if sControlla = "0" then
		ImpostaPagina()
	else
%>
		<table border="0" width="500" cellpadding="2" cellspacing="1">
		  <tr align="center"> 
		    <td class=tbltext3>
				<%=sControlla%>
			</td>
		  </tr>
		</table>

<%
	end if
else%>

    <table border="0" width="500" cellpadding="2" cellspacing="1">
	  <tr align="center"> 
	    <td class=tbltext3>
			<%=sControlla%>
		</td>
	  </tr>
	</table>


<%end if%>

<!--#include virtual ="/include/closeconn.asp"-->
<!--#include virtual="/strutt_coda2.asp"-->
