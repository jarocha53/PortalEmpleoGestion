<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include virtual = "/include/openconn.asp" -->
	<html>
	<head>
	<title>Selezione del Materiale Didattico</title>
	<link REL="STYLESHEET" TYPE="text/css" HREF="<%=session("progetto")%>/fogliostile.css">

	<script language="javascript">
	<!-- #include virtual="/include/help.inc" -->

	function InviaSelezione(IdElemento,TestoElemento){
		opener.document.frmMatDid.txtIdElemento.value = IdElemento;
		opener.document.frmMatDid.txtCorsi.value = TestoElemento;
		self.close()
	}	
	</script>
	</head>
	<body class="sfondocentro">
	<%
	sIdCorso=Request.QueryString("sIdCorso")
	%>
	<form name="frmelementi" method="post" action="MED_Vismatdid.asp">
	<br>
<%
sTitolo = "Selezione del Materiale Didattico"
sCommento = "Selezionare il modulo del corso dall'elenco visualizzato. " &_
			"Premere <b>Chiudi</b> per non selezionare nulla."
sHelp = "/Pgm/help/Formazione/mediateca/modcorsi/"
%>
<!--#include virtual="/include/SetTestataFAD.asp"-->
<%
call TestataFAD(sTitolo, sCommento, false, sHelp)
smoduli = "Select E.Titolo_Elemento, E.id_Elemento " &_
		  "from Elemento E, Corso C, Regola_Dipendenza R " &_
		  "where C.id_corso =" & sIdCorso  & " AND C.id_Corso = R.id_Corso " &_
		  "AND R.ID_Elemento = E.id_Elemento"
'PL-SQL * T-SQL  
SMODULI = TransformPLSQLToTSQL (SMODULI) 
Set rsMod=CC.execute(smoduli)
If rsMod.eof then	
%>
	<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
		<tr align="center">
			<td>
				<b class="tbltextFad">
					Non ci sono materiali didattici associati al Corso
				</b>	
			</td>
		</tr>
	</table>	
<%
else
%>
	<table border="0" CELLPADDING="2" CELLSPACING="2" width="500">
<%
	dim nRiga
	nRiga = 0
	Do While Not rsMod.EOF
		nRiga = nRiga + 1
		if nRiga mod 2 = 1 then
%>
		</tr>
		<tr height="25">
<%
		end if
%>
			<td nowrap width="50%" class="tblsfondo">
				<a class="textblack" href="Javascript:InviaSelezione('<%=rsMod("id_Elemento")%>','<%=replace(rsMod("Titolo_Elemento"),"'","$")%>')">
					<b><%=ucase(rsMod("Titolo_Elemento"))%></b>
				</a>
			</td>
		</tr>
<%			
		rsMod.MoveNext
	loop
	rsMod.Close
%>
	</table>
<%
end if
%>
	</form>

	<table width="500" cellspacing="2" cellpadding="1" border="0">
		<tr align="center">
			<td>
				<a href="javascript:self.close()"><img src="<%=Session("progetto")%>/images/chiudi.gif" title="Chiudi la pagina" border="0" align="absBottom"></a>
			</td>
		</tr>		
	</table>
	</body>
	</html>
<!-- #include virtual="/include/closeconn.asp" -->


