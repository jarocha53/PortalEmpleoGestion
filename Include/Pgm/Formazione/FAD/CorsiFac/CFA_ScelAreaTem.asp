<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
	<script LANGUAGE="JavaScript">
	function Controlladati()
	{
		if (document.frmAreaTematica.cmbAreaTem.selectedIndex==0)
		{
			alert('Seleziona un`area tematica')
			document.frmAreaTematica.cmbAreaTem.focus() 
			return false
		}
	return true
	}
	
	function ApriFinestra(idGruppo)
	{	var idArea
		
		if (Controlladati())
		{
			idArea=document.frmAreaTematica.cmbAreaTem.value
			window.open('CFA_AggCorFac.asp?UidGruppo=' + idGruppo + '&UidArea=' + idArea,'','toolbar=0,channelmode=0,location=0,menubar=0,status=0,width=550,height=450,Resize=No,Scrollbars=yes')
		}
	}
	</script>
<%
'-------------------------------------------------------------------------------
Sub Inizio
	dim sFunzione
	dim sTitolo
	dim sCommento
	dim bCampiObbl
	
	sFunzione = "INSERIMENTO CORSI FACOLTATIVI"
	sTitolo = "INSERIMENTO CORSI FACOLTATIVI"
	sCommento = "Testo del commento<br>per l'inserimento Corsi facoltativi"
	bCampiObbl = false
%>
	<!--#include file="Testata.asp"-->
<%
End sub
'-------------------------------------------------------------------------------
Sub ImpostaPag
	dim sCodRorg
	dim sSql
	dim sIdDescGruppo,aIdDescGruppo,iIdGruppo,sDesGruppo
	sCodRorg="UT"
	
	sIdDescGruppo=Request("cmbGruppo")

	aIdDescGruppo=split(sIdDescGruppo,"|")
	iIdGruppo=clng(aIdDescGruppo(0))
	sDesGruppo=aIdDescGruppo(1)

'Response.Write "sIdDescGruppo=" & sIdDescGruppo & "<br>"
'Response.Write "iIdGruppo=" & iIdGruppo & "<br>"
'Response.Write "sDesGruppo=" & sDesGruppo & "<br>"
	
	sSql="SELECT ID_AREA,TITOLOAREA " &_
		"FROM AREA " &_
		"ORDER BY TITOLOAREA"

'Response.Write sSql & "<br>"

'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	set Rst=CC.execute (sSql)
	if not Rst.eof then
		
%>	<form name="frmAreaTematica" id="frmAreaTematica">
	<table border="0">
	<tr>
		<td class="tbltext1a"><b>Gruppo:&nbsp;</b></td>
		<td class="textblack"><b><%=sDesGruppo%>&nbsp;</b></td>
	</tr>
	<tr>
		<td class="tbltext1a"><b>Area tematica:</b>&nbsp;
		</td>
		<td>
			<select id="cmbAreaTem" name="cmbAreaTem" class="textblacka">
			<option></option>
<%			do while not Rst.eof
%>				<option value="<%=Rst("ID_AREA")%>"><%=Rst("TITOLOAREA")%></option>
<%				Rst.MoveNext
			loop			
%>			</select>
		</td>
	</tr>
	</table>
<%	end if
	
	Rst.close
	set Rst=nothing
%>	<br>
	<table border="0">
		<tr>
			<td>
				<a href="JavaScript:ApriFinestra(<%=iIdGruppo%>)" class="textred" OnMouseOver="JavaScript:window.status=''; return true;">
				<b>Scelta corso</b></a>
			</td>
		</tr>
	</table>
	</form>
	<a href="JavaScript:history.back();">
		<img src="<%=session("progetto")%>/images/indietro.gif" border="0">
	</a>
	
<%
end sub
'-------------------------------------------------------------------------------
'M A I N
%>
<!--#include virtual="/strutt_testa2.asp"-->
<!--#include virtual="/include/openconn.asp"-->
<center>
<%
	
	call Inizio
	call ImpostaPag
%>
</center>
<!--#include virtual="/include/closeconn.asp"-->
<!--#include virtual="/strutt_coda2.asp"-->
