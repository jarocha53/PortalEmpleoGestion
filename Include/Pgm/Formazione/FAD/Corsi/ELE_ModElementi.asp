<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%
Sub ControlliJavaScript()
%>
<script language="Javascript">
<!--#include virtual="/include/ControlDate.inc"-->
<!--#include virtual="/include/ControlNum.inc"-->
<!--#include virtual="/include/ControlString.inc"-->
function FormInserisciElemento_Validator(frmElemento)
	{
	if (frmElemento.txtTitoloElemento.value == "")
		{
		frmElemento.txtTitoloElemento.focus();
		alert("Il campo 'Titolo Elemento' � obbligatorio.");
		return (false);
	  	}
		
	if (frmElemento.cmbTipoElemento.value == "")
		{
		frmElemento.cmbTipoElemento.focus();
		alert("Il campo 'Tipo Elemento' � obbligatorio.");
		return (false);
	  	}

	if (frmElemento.cmbFlErogazione.value == "")
		{
		frmElemento.cmbFlErogazione.focus();
		alert("Il campo 'Modalit� di erogazione' � obbligatorio.");
		return (false);
	  	}

	if (frmElemento.txtLinkElemento.value == "")
		{
		frmElemento.txtLinkElemento.focus();
		alert("Il campo 'Link Elemento' � obbligatorio.");
		return (false);
	  	}
		
	if (frmElemento.txtMasterScore.value == "")
		{
		frmElemento.txtMasterScore.focus();
		alert("Il campo 'Master Score' � obbligatorio.");
		return (false);
	  	}

	if (frmElemento.txtInfoProd.value == "")
		{
		frmElemento.txtInfoProd.focus();
		alert("Il campo 'Informazioni Produttore' � obbligatorio.");
		return (false);
	  	}
	  	
	if (isNaN(frmElemento.txtMasterScore.value))
		{
			frmElemento.txtMasterScore.focus();
			alert("Il campo 'Master Score' � numerico.");
			return (false);				
		}

	if (frmElemento.txtInfoProd.value == "")
		{
		frmElemento.txtInfoProd.focus();
		alert("Il campo 'Informazioni Produttore' � obbligatorio.");
		return (false);
	  	}
	
	if (frmElemento.txtMinScore.value != "")
		{
		if (isNaN(frmElemento.txtMinScore.value))
			{
				frmElemento.txtMinScore.focus();
				alert("Il campo 'Min Score' � numerico.");
				return (false);				
			}
		}

	if (frmElemento.txtMaxScore.value != "")
		{
		if (isNaN(frmElemento.txtMaxScore.value))
			{
				frmElemento.txtMaxScore.focus();
				alert("Il campo 'Max Score' � numerico.");
				return (false);				
			}
		}

	if (frmElemento.txtMaxTime.value != "")
		{
		if (isNaN(frmElemento.txtMaxTime.value))
			{
				frmElemento.txtMaxTime.focus();
				alert("Il campo 'Max Time' � numerico.");
				return (false);				
			}
		}

	if (frmElemento.txtCreditElemento.value != "")
		{
		if (isNaN(frmElemento.txtCreditElemento.value))
			{
				frmElemento.txtCreditElemento.focus();
				alert("Il campo 'Credito' � numerico.");
				return (false);				
			}
		}
	if ((frmElemento.cmbTipoElemento.value == 'T') && (frmElemento.cmbQuestionario.value == ''))
		{
			frmElemento.cmbQuestionario.focus();
			alert("Selezionare un Questionario");
			return (false);				
		}
	
	return (true);
	}
	function AttivaCombo()
	{
		if (FormInserisciElementi.cmbTipoElemento.value == 'T')
		{
			FormInserisciElementi.cmbQuestionario.disabled = false;
		}
		else
		{
			FormInserisciElementi.cmbQuestionario.disabled = true;
		}		
	}
</script>
<%
End Sub
';););););););););););););););););););););););););););););););););););););););)
Sub Testata()
	dim sFunzione
	dim sTitolo
	dim sCommento
	dim bCampiObbl
	
	sFunzione = "GESTIONE CORSI"
	sTitolo = "MODIFICA/ELIMINA ELEMENTO"
	sCommento = "Commento alla modifica dell'elemento"
	bCampiObbl = true
%>
	<!--#include file="COR_Testata.asp"-->
<%
End Sub
';););););););););););););););););););););););););););););););););););););););)
'-----------------------------------------------------------------------
Sub CreaTipoElemento(sEleSel)
	dim sItem
	select case sEleSel
		case "L"
			sItem = "<option value='L' selected>Lezione</option>" &_
				"<option value='T'>Test</option>"
		case "T"
			sItem = "<option value='L'>Lezione</option>" &_
				"<option value='T' selected>Test</option>"
	end select
	Response.Write sItem 
End Sub
'-----------------------------------------------------------------------
Sub CreaFlErogazione(sEleSel)
	dim sItem
	select case sEleSel
		case "0"
			sItem = "<option value=0 selected>Off Line</option>" &_
					"<option value=1>On Line</option>"
		case "1"
			sItem = "<option value=0>Off Line</option>" &_
					"<option value=1 selected>On Line</option>"
	end select
	Response.Write sItem 
End Sub
'-----------------------------------------------------------------------
Function MenoUno(nVal)
	if IsNull(nVal) then
		MenoUno=-1
	else
		MenoUno = nVal
	end if
End Function
'-----------------------------------------------------------------------
Sub CreaQuestionario(nIdQuest)
dim rsQuest, sSQL
dim sMess, sFlSel
	
	Set rsQuest = Server.CreateObject("ADODB.Recordset")
	sSQL = "Select ID_QUESTIONARIO, TIT_QUESTIONARIO From Questionario order by ID_QUESTIONARIO"
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	rsQuest.Open sSQL, CC, adOpenForwardOnly
	do while not rsQuest.EOF
		if cint(menouno(rsQuest("ID_QUESTIONARIO"))) = cint(menouno(nIdQuest)) then
			sFlSel = " selected "
		else
			sFlSel = ""
		end if
		sMess = "<option value=" & rsQuest("ID_QUESTIONARIO") & sFlSel & ">" & rsQuest("TIT_QUESTIONARIO") & "</option>"
		Response.Write sMess
		rsQuest.MoveNext
	loop
	rsQuest.Close
	set rsQuest = nothing
End Sub
'-----------------------------------------------------------------------
Function DecodQuestionario(IdQuestDec)
dim rsQuest, sSQL
dim sMess, sFlSel
	
	Set rsQuest = Server.CreateObject("ADODB.Recordset")
	sSQL = "Select TIT_QUESTIONARIO From Questionario Where ID_QUESTIONARIO = " & IdQuestDec 
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	rsQuest.Open sSQL, CC, adOpenForwardOnly
	if not rsQuest.eof then
		DecodQuestionario = rsQuest("TIT_QUESTIONARIO")
	else
		DecodQuestionario = "Errore durante la ricerca del Questionario"
	end if
	rsQuest.Close
	set rsQuest = nothing
End Function
'-----------------------------------------------------------------------
Sub PaginaModificabile(RR)
dim nIdCorso
nIdCorso = Request("IdCorso")
%>
<br>
	<form METHOD="POST" onsubmit="return FormInserisciElemento_Validator(this)" name="FormInserisciElementi" action="Ele_CnfElementi.asp">
	<input type=hidden name="Oper" id="Oper" value="Mod">
	<input type=hidden name="hIdElem" id="hIdElem" value="<%=RR("ID_ELEMENTO")%>">
	<input type=hidden name="hIDCorso" id="hIdCorso" value="<%=nIdCorso%>">
	<input type=hidden name="hDtTmst" id="hDtTmst" value="<%=RR("DT_TMST")%>">
		<table border="0" cellspacing="2" cellpadding="1" width="500">
		    <tr> 
				<td align="left" nowrap class="tbltext1">
					<b>Titolo Elemento*</b>
				</td>
				<td>
					<input type="text" name="txtTitoloElemento" class="textblacka" size="50" id="txtTitoloElemento" maxlength="100" value="<%=RR("TITOLO_ELEMENTO")%>">
				</td>
		    </tr>
		    <tr> 
				<td align="left" nowrap class="tbltext1">
					<b>Tipo Elemento*</b>
				</td>
				<td>
					<select name="cmbTipoElemento" id="cmbTipoElemento" class="textblacka" onchange="javascript:AttivaCombo()">
						<option value=""></option>
						<%
							CreaTipoElemento(RR("TIPO_ELEMENTO"))
						%>
					</select>
					&nbsp;&nbsp;
					<select name="cmbQuestionario" id="cmbQuestionario" class="textblacka" 
					<%
						if RR("TIPO_ELEMENTO") = "L" then
							Response.Write "disabled"
						end if
					%>
					>
						<option value=""></option>
					<%
						CreaQuestionario(RR("ID_QUESTIONARIO"))
					%>
					</select>
				</td>
		    </tr>
			<tr> 
				<td align="left" nowrap class="tbltext1">
					<b>Modalit� di erogazione</b>*
				</td>
				<td>
					<select name="cmbFlErogazione" id="cmbFlErogazione" class="textblacka">
						<option value=""></option>
						<%
							CreaFlErogazione(RR("FL_EROGAZIONE"))
						%>
				</td>
			</tr>
		    <tr> 
				<td align="left" nowrap class="tbltext1">
					<b>Obiettivo Elemento</b>
				</td>
				<td>
					<span class="tbltext1">&nbsp;- Utilizzabili <label name="lblNumCar1" id="lblNumCar1">250</label> caratteri -</span>
					<textarea name="txtObiettivoElemento" id="txtObiettivoElemento" class="textblack" cols="50" rows="3" OnKeyUp="JavaScript:CheckLenTextArea(txtObiettivoElemento,lblNumCar1,250)"><%=RR("OBIETTIVO_ELEMENTO")%></textarea>
				</td>
		    </tr>
		    <tr> 
				<td align="left" nowrap class="tbltext1">
					<b>Link Elemento*</b>
				</td>
				<td>
					<input type="text" name="txtLinkElemento" class="textblacka" size="50" id="txtLinkElemento" maxlength="100" value="<%=RR("LINK_ELEMENTO")%>">
				</td>
		    </tr>
		    <tr> 
				<td align="left" nowrap class="tbltext1">
					<b>Master Score*</b>
				</td>
				<td>
					<input type="text" name="txtMasterScore" class="textblacka" size="10" id="txtMasterScore" maxlength="10" value="<%=RR("MASTER_SCORE")%>">
				</td>
		    </tr>
		    <tr> 
				<td align="left" nowrap class="tbltext1">
					<b>Min Score</b>
				</td>
				<td>
					<input type="text" name="txtMinScore" class="textblacka" size="10" id="txtMinScore" maxlength="10" value="<%=RR("MIN_SCORE")%>">
				</td>
		    </tr>
		    <tr> 
				<td align="left" nowrap class="tbltext1">
					<b>Max Score</b>
				</td>
				<td>
					<input type="text" name="txtMaxScore" class="textblacka" size="10" id="txtMaxScore" maxlength="10" value="<%=RR("MAX_SCORE")%>">
				</td>
		    </tr>
		    <tr> 
				<td align="left" nowrap class="tbltext1">
					<b>Max Time</b>
				</td>
				<td>
					<input type="text" name="txtMaxTime" class="textblacka" size="10" id="txtMaxTime" maxlength="10" value="<%=RR("MAX_TIME")%>">
				</td>
		    </tr>
		    <tr> 
				<td align="left" nowrap class="tbltext1">
					<b>Credito</b>
				</td>
				<td>
					<input type="text" name="txtCreditElemento" class="textblacka" size="10" id="txtCreditElemento" maxlength="10" value="<%=RR("CREDIT_ELEMENTO")%>">
				</td>
		    </tr>
		    <tr> 
				<td align="left" nowrap class="tbltext1">
					<b>Authoring</b>
				</td>
				<td> 
					<input name="txtAuthoring" type="text" size="50" class="textblacka" maxlength=254 value="<%=RR("AUTHOR_ELEMENTO")%>">
				</td>
		    </tr>
		    <tr> 
				<td align="left" nowrap class="tbltext1">
					<b>Informazioni<br>Produttore*</b>
				</td>
				<td>
					<span class="tbltext1">&nbsp;- Utilizzabili <label name="lblNumCar2" id="lblNumCar2">254</label> caratteri -</span>
					<textarea name="txtInfoProd" class="textblack" cols="50" rows="3" OnKeyUp="JavaScript:CheckLenTextArea(txtInfoProd,lblNumCar2,254)"><%=RR("INFO_PRODUTTORE")%></textarea>
				</td>
		    </tr>
		</table>
		<br>
		<table width="500" border="0" cellspacing="2" cellpadding="1">
			<tr align="center">
				<td align="right">			
					<%
						PlsIndietro()
					%>
				</td>
				<td align="center">
					<%
						PlsInvia("InserisciE")
					%>
				</td>
				<td align="left">
					<%
						PlsElimina("ELE_CnfElementi.asp?oper=Can&IdEle=" & nIDElemento & "&hIdCorso="& nIdCorso & "&dttmst='"& RR("DT_TMST") & "'")
					%>
				</td>
			</tr>
		</table> 
	</form> 
	<br>
<%
End Sub
'-----------------------------------------------------------------------
Sub PaginaNonModificabile(RR)
dim nIdCorso
nIdCorso = Request("IdCorso")
%>
<br>
<table border="0" cellspacing="2" cellpadding="1" width="500">
    <tr> 
		<td align="left" nowrap class="tbltext1" width=150>
			<b>Titolo Elemento</b>
		</td>
		<td class="textblack" width=350>
			<b><%=strHTMLEncode(RR("TITOLO_ELEMENTO"))%></b>
		</td>
    </tr>
    <tr> 
		<td align="left" nowrap class="tbltext1">
			<b>Tipo Elemento</b>
		</td>
		<td class="textblack">
			<b>
			<%
				select case RR("TIPO_ELEMENTO")
					case "L"
						Response.Write "Lezione"
					case "T"
						Response.Write "Test: <i>" & chr(34) & DecodQuestionario(RR("ID_QUESTIONARIO")) & chr(34) & "</i>"
				end select 
			%>
			</b>
			</select>
		</td>
    </tr>
	<tr> 
		<td align="left" nowrap class="tbltext1">
			<b>Modalit� di erogazione</b>
		</td>
		<td class="textblack">
			<%
			select case RR("FL_EROGAZIONE")
				case 0
					Response.Write "On Line"
				case 1
					Response.Write "Off Line"
			end select 
			%>
		</td>
	</tr>
    <tr> 
		<td align="left" nowrap class="tbltext1">
			<b>Obiettivo Elemento</b>
		</td>
		<td class="textblack">
			<b><%=strHTMLEncode(RR("OBIETTIVO_ELEMENTO"))%></b>
		</td>
    </tr>
    <tr> 
		<td align="left" nowrap class="tbltext1">
			<b>Link Elemento</b>
		</td>
		<td class="textblack">
			<%=strHTMLEncode(RR("LINK_ELEMENTO"))%>
		</td>
    </tr>
    <tr> 
		<td align="left" nowrap class="tbltext1">
			<b>Master Score</b>
		</td>
		<td class="textblack">
			<%=RR("MASTER_SCORE")%>
		</td>
    </tr>
    <tr> 
		<td align="left" nowrap class="tbltext1">
			<b>Min Score</b>
		</td>
		<td class="textblack">
			<%=RR("MIN_SCORE")%>
		</td>
    </tr>
    <tr> 
		<td align="left" nowrap class="tbltext1">
			<b>Max Score</b>
		</td>
		<td class="textblack">
			<%=RR("MAX_SCORE")%>
		</td>
    </tr>
    <tr> 
		<td align="left" nowrap class="tbltext1">
			<b>Max Time</b>
		</td>
		<td class="textblack">
			<%=RR("MAX_TIME")%>
		</td>
    </tr>
    <tr> 
		<td align="left" nowrap class="tbltext1">
			<b>Credito</b>
		</td>
		<td class="textblack">
			<%=RR("CREDIT_ELEMENTO")%>
		</td>
    </tr>
    <tr> 
		<td align="left" nowrap class="tbltext1">
			<b>Authoring</b>
		</td>
		<td class="textblack"> 
			<%=strHTMLEncode(RR("AUTHOR_ELEMENTO"))%>
		</td>
    </tr>
    <tr> 
		<td align="left" nowrap class="tbltext1">
			<b>Informazioni Produttore</b>
		</td>
		<td class="textblack">
			<%=strHTMLEncode(RR("INFO_PRODUTTORE"))%>
		</td>
    </tr>
</table>
<br>
<table width="500" border="0" cellspacing="2" cellpadding="1">
	<tr align="center">
		<td align="center">			
			<%
				PlsIndietro()
			%>
		</td>
	</tr>
</table> 
<br>
<%
End Sub
';););););););););););););););););););););););););););););););););););););););)
Sub ImpostaPag()
	Dim sql, RR, RR2, sql2
	Dim aTp(2), aPb(2), Dispgg, Dispmm, Dispaa, Rif, i, dtTmst
	Dim sSQL, rsConta
	sql = "SELECT * FROM ELEMENTO WHERE ID_ELEMENTO = " & nIDElemento
			
	Set RR = Server.CreateObject("ADODB.Recordset")
	'Response.Write sql
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
	RR.Open sql, CC, 3

	If RR.EOF then
%>
		<br>
	
		<table border="0" width="500" cellpadding="0" cellspacing="0">
			<tr align="center">
				<td class="tbltext3">
					Pagina momentaneamente non disponibile
				</td>
			</tr>
		</table>	
		<br>
		<table width="500" border="0" cellspacing="2" cellpadding="1">
			<tr align="center">
				<td>
					<%
						PlsIndietro()
					%>
				</td>
			</tr>
		</table> 
		<br>
<%		
	else
		'Controllo che l'elemento selezionato non sia parte di un corso gi� pubblicato.
		sSQL = " SELECT COUNT(*) AS Conta FROM REGOLA_DIPENDENZA, CORSO WHERE" &_
				" REGOLA_DIPENDENZA.ID_CORSO = CORSO.ID_CORSO AND" &_
				" CORSO.DT_DISPONIBILE > SYSDATE AND" &_
				" REGOLA_DIPENDENZA.ID_ELEMENTO = " & nIDElemento

		Set rsConta = Server.CreateObject("ADODB.Recordset")
		'Response.Write sql
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
		rsConta.Open sSQL, CC, 3
		if CInt(rsConta("Conta")) > 0 then
			call PaginaModificabile(RR)
		else
			call PaginaNonModificabile(RR)
		end if
		rsConta.Close
		set rsConta = nothing
		
	end if
	RR.Close
	set RR = nothing
End Sub
';););););););););););););););););););););););););););););););););););););););)
'M A I N
Dim nIDElemento
%>
<!--#include virtual="/strutt_testa2.asp"-->
<!--#include virtual="/include/openconn.asp"-->
<!--#include virtual="/include/DecCod.asp"-->
<!--#include virtual="/include/HTMLEncode.asp"-->
<!--#include file="COR_Pulsanti.asp"-->
<%
	nIDElemento = Request.QueryString("IDElem")
	ControlliJavaScript()
	Testata()
	ImpostaPag()
%>
<!--#include virtual="/include/closeconn.asp"-->
<!--#include virtual="/strutt_coda2.asp"-->
