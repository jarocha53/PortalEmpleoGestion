<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%
Sub ControlliJavaScript()
%>
	<script language="Javascript">
	
		<!--#include virtual="/include/ControlDate.inc"-->
		<!--#include virtual="/include/ControlNum.inc"-->
		<!--#include virtual="/include/ControlString.inc"-->
		
	function FormRegistra_Validator(frmCorso)
	{
	if (frmCorso.cmbArea.value == "")
		{
		frmCorso.cmbArea.focus();
		alert("Il campo 'Area' � obbligatorio.");
		return (false);
	  	}
		
	if (frmCorso.txtTitoloCorso.value == "")
		{
		frmCorso.txtTitoloCorso.focus();
		alert("Il campo 'Titolo Corso' � obbligatorio.");
		return (false);
	  	}

	if (frmCorso.txtDescrizione.value == "")
		{
		frmCorso.txtDescrizione.focus();
		alert("Il campo 'Descrizione' � obbligatorio.");
		return (false);
	  	}

	if (frmCorso.cboLivCorso.value == "")
		{
		frmCorso.cboLivCorso.focus();
		alert("Il campo 'Livello Corso' � obbligatorio.");
		return (false);
	  	}
		
	if (frmCorso.cboFlErogazione.value == "")
		{
		frmCorso.cboFlErogazione.focus();
		alert("Il campo 'Modalit� di erogazione' � obbligatorio.");
		return (false);
	  	}

	
	if (frmCorso.chkDispo.checked == false)
	{
		if (!ValidateInputDate(frmCorso.txtDisponibilitaDal.value))
			{
				frmCorso.txtDisponibilitaDal.focus();
				return (false);
			}
	}	

	if (frmCorso.txtInfoProd.value == "")
		{
		frmCorso.txtInfoProd.focus();
		alert("Il campo 'Informazioni Produttore' � obbligatorio.");
		return (false);
	  	}	

	if (frmCorso.cboFlPubblico.value == "")
		{
		frmCorso.cboFlPubblico.focus();
		alert("Il campo 'Pubblico/Privato' � obbligatorio.");
		return (false);
	  	}			
	if (frmCorso.txtDurata.value != "")
		{
		if (isNaN(frmCorso.txtDurata.value))
			{
				frmCorso.txtDurata.focus();
				alert("Il campo 'Durata corso' � numerico.");
				return (false);				
			}
		}
	return (true);
	}
		

		function Elimina(id,dt)	
		{
			if (confirm("Conferma eliminazione del corso?"))
				{
					location.href = 'COR_CnfCorsi.asp?Oper=Can&IDCorso=' + id + '&dtTmst=' + dt
				}
		}

function ImpFlag()
{
	//alert(FormInserisciCorso.chkDispo.checked);
	if (FormInserisci.chkDispo.checked == true)
		{
			FormInserisci.txtDisponibilitaDal.disabled = true
		}
	else
		{
			FormInserisci.txtDisponibilitaDal.disabled = false
			FormInserisci.txtDisponibilitaDal.focus()
		}
}
		// end hiding -->
		</script>
<%
End sub
';););););););););););););););););););););););););););););););););););););););)
Sub Testata()
	dim sFunzione
	dim sTitolo
	dim sCommento
	dim bCampiObbl
	
	sFunzione = "GESTIONE CORSI"
	sTitolo = "MODIFICA/ELIMINA CORSO"
	sCommento = "Commento alla modifica del<br>corso"
	bCampiObbl = true
%>
	<!--#include file="COR_Testata.asp"-->
<%
End Sub
';););););););););););););););););););););););););););););););););););););););)
Sub PaginaNonModificabile(RR)
dim sMess
%>
	<table border="0" cellspacing="2" cellpadding="2" width="500">
		<tr> 
			<td align="left" nowrap class="tbltext1" width=150>
				<b>Area</b>
			</td>
			<td class="textblack" width=350>
				<b><%=strHTMLEncode(RR("TitoloArea"))%></b>
			</td>
		</tr>
	    <tr> 
			<td align="left" nowrap class="tbltext1">
				<b>Titolo Corso</b>
			</td>
			<td class="textblack">
				<b><%=strHTMLEncode(RR("TIT_CORSO"))%></b>
			</td>
	    </tr>
	    <tr> 
			<td align="left" nowrap class="tbltext1">
				<b>Descrizione</b>
			</td>
			<td class="textblack">
				<b><%=strHTMLEncode(RR("DESC_CORSO"))%></b>
			</td>
	    </tr>
	    <tr> 
			<td align="left" nowrap class="tbltext1">
				<b>Obiettivo</b>
			</td>
			<td class="textblack"> 
				<%=strHTMLEncode(RR("OBIETTIVO_CORSO"))%>
			</td>
	    </tr>
	    <tr> 
			<td align="left" nowrap class="tbltext1">
				<b>Livello Corso</b>
			</td>
			<td class="textblack">
				<%=RR("LIV_CORSO")%>
			</td>
	    </tr>
	    <tr> 
			<td align="left" nowrap class="tbltext1">
				<b>Versione Corso AICC</b>
			</td>
			<td class="textblack">
				<%=RR("VER_CORSOAICC")%>
			</td>
	    </tr>
	    <tr> 
			<td align="left" nowrap class="tbltext1">
				<b>Crediti corso</b>
			</td>
			<td class="textblack">
				<%=RR("NUMCREDIT_CORSO")%>
			</td>
	    </tr>
	    <tr> 
			<td align="left" nowrap class="tbltext1">
				<b>Modalit� di Erogazione</b>
			</td>
			<td class="textblack"> 
				<%
				select case cint(RR("FL_EROGAZIONE"))
					case 0
						sMess = "Off Line"
					case 1
						sMess = "On Line"
					case else
						sMess = "Errore!!!"
				end select
				Response.Write sMess
				%>
			</td>
	    </tr>
	    <tr> 
			<td align="left" nowrap class="tbltext1">
				<b>Durata corso</b>&nbsp;(in ore)
			</td>
			<td class="textblack">
				<%=RR("DURATA_CORSO")%>
			</td>
	    </tr>
	    <tr> 
			<td align="left" nowrap class="tbltext1">
				<b>Disponibilit� dal</b>
			</td>
			<td class="textblack"> 
				<b><%=RR("DT_DISPONIBILE")%></b>
			</td>
	    </tr>
	    <tr> 
			<td align="left" nowrap class="tbltext1">
				<b>Authoring</b>
			</td>
			<td class="textblack">
				<%=strHTMLEncode(RR("AUTHORING_CORSO"))%>
			</td>
	    </tr>
	    <tr> 
			<td align="left" nowrap class="tbltext1">
				<b>Informazioni Produttore</b>
			</td>
			<td class="textblack">
				<%=strHTMLEncode(RR("INFO_PRODUTTORE"))%>
			</td>
	    </tr>
	    <tr> 
			<td align="left" nowrap class="tbltext1">
				<b>Progetto proprietario</b>
			</td>
			<td class="textblack">
				<b><%=DecCodVal("CPROJ", 0, date(), RR("ID_PROJOWNER"),1)%></b>
			</td>
	    </tr>
	    <tr> 
			<td align="left" nowrap class="tbltext1">
				<b>Pubblico/Privato</b>
			</td>
			<td class="textblack"> 
				<%
				select case cint(RR("FL_PUBBLICO"))
					case 0
						sMess = "Pubblico"
					case 1
						sMess = "Privato"
					case else
						sMess = "Errore!!!"
				end select
				Response.Write sMess
				%>
			</td>
	    </tr>
	    <tr> 
			<td align="left" nowrap class="tbltext1">
				<b>Prerequisiti necessari</b>
			</td>
			<td class="textblack">
				<%=strHTMLEncode(RR("PREREQ_CORSO"))%>
			</td>
	    </tr>
	    <tr> 
			<td align="left" nowrap class="tbltext1">
				<b>Destinatari del Corso</b>
			</td>
			<td class="textblack"> 
				<%=strHTMLEncode(RR("DESTINATARI_CORSO"))%>
			</td>
	    </tr>
	</table>
	<br>
	<table width="500" border="0" cellspacing="2" cellpadding="1">
		<tr align="center">
			<td>
				<%
					PlsIndietro()
				%>
			</td>
		</tr>
	</table> 
	<br>

<%
End Sub
';););););););););););););););););););););););););););););););););););););););)
Sub PaginaModificabile(RR)
	Rif = "COR_CnfCorsi.asp?Oper=Mod&IDCorso=" & RR("ID_CORSO") & "&dtTmst=" & RR.Fields("DT_TMST")	
%>
	<form method="POST" action="<%=Rif%>" onsubmit="return FormRegistra_Validator(this)" name="FormInserisci">
	<table border="0" cellspacing="2" cellpadding="2" width="500">
		<tr> 
			<td align="left" nowrap class="tbltext1">
				<b>Area</b>*
			</td>
			<td class="textblack">
<%					
					sql="SELECT DISTINCT TitoloArea, ID_AREA FROM AREA ORDER BY TitoloArea"
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
					SET RR1=CC.EXECUTE (sql)
%>					
					<select name="cmbArea" id="cmbArea" class="textblack"> 						
						<option VALUE></option>
						<% 
							DO WHILE NOT RR1.EOF 
								if cint(RR("ID_AREATEMATICA")) = cint(RR1("ID_AREA")) then
									sFlag = "selected"
								else
									sFlag = ""
								end if
								Response.Write "<option value=" & chr(34) & RR1.FIELDS("ID_AREA") & chr(34) & " " & sFlag & ">"
								Response.Write RR1.FIELDS("TitoloArea")
								Response.Write "</option>"
								RR1.MOVENEXT
							LOOP
%>						
					</select>
<%					
					RR1.CLOSE
%>		
				<input type="hidden" name="hCodArea" value="<%=RR("ID_AREATEMATICA")%>"> 
			</td>
		</tr>
	    <tr> 
			<td align="left" nowrap class="tbltext1">
				<b>Titolo Corso*</b>
			</td>
			<td>
				<input type="text" name="txtTitoloCorso" class="textblacka" size="50" id="txtTitoloCorso" value="<%=strHTMLEncode(RR("TIT_CORSO"))%>" maxlength="50">
			</td>
	    </tr>
	    <tr> 
			<td align="left" nowrap class="tbltext1">
				<b>Descrizione*</b>
			</td>
			<td>
				<span class="tbltext1">&nbsp;- Utilizzabili <label name="lblNumCar1" id="lblNumCar1">254</label> caratteri -</span>
				<textarea name="txtDescrizione" class="textblack" cols="50" rows="3" OnKeyUp="JavaScript:CheckLenTextArea(txtDescrizione,lblNumCar1,254)"><%=strHTMLEncode(RR("DESC_CORSO"))%></textarea>
			</td>
	    </tr>
	    <tr> 
			<td align="left" nowrap class="tbltext1">
				<b>Obiettivo</b>
			</td>
			<td> 
				<span class="tbltext1">&nbsp;- Utilizzabili <label name="lblNumCar2" id="lblNumCar2">254</label> caratteri -</span>
				<textarea name="txtObiettivo" class="textblack" cols="50" rows="3" OnKeyUp="JavaScript:CheckLenTextArea(txtObiettivo,lblNumCar2,254)"><%=strHTMLEncode(RR("OBIETTIVO_CORSO"))%></textarea>
			</td>
	    </tr>
	    <tr> 
			<td align="left" nowrap class="tbltext1">
				<b>Livello Corso*</b>
			</td>
			<td> 
				<select name="cboLivCorso" id="cboLivCorso" class=textblack>
					<option value=""></option>
				<%
				select case RR("LIV_CORSO") 
					case 1
					%>
						<option value=""></option>
						<option value="1" selected>1</option>
						<option value="2">2</option>
						<option value="3">3</option>
					<%	
					case 2
					%>
						<option value=""></option>
						<option value="1">1</option>
						<option value="2" selected>2</option>
						<option value="3">3</option>
					<%	
					case 3
					%>
						<option value=""></option>
						<option value="1">1</option>
						<option value="2">2</option>
						<option value="3" selected>3</option>
					<%	
				end select
				%>
				</select>
			</td>
	    </tr>
	    <tr> 
			<td align="left" nowrap class="tbltext1">
				<b>Versione Corso AICC</b>
			</td>
			<td> 
				<input name="txtVerCorsoAICC" size="3" maxlength="3" type="text" class="textblacka" value="<%=RR("VER_CORSOAICC")%>">
			</td>
	    </tr>
	    <tr> 
			<td align="left" nowrap class="tbltext1">
				<b>Crediti corso</b>
			</td>
			<td> 
				<input name="txtNumCredCorso" type="text" class="textblacka" value="<%=RR("NUMCREDIT_CORSO")%>">
			</td>
	    </tr>
	    <tr> 
			<td align="left" nowrap class="tbltext1">
				<b>Modalit� di Erogazione*</b>
			</td>
			<td> 
				<select name="cboFlErogazione" id="cboFlErogazione" class=textblack>
					<option value=""></option>
					<%
					if cint(RR("FL_EROGAZIONE")) = 0 then
						Response.Write "<option value='0' selected>Off Line</option>"
					else
						Response.Write "<option value='0'>Off Line</option>"
					end if
					if cint(RR("FL_EROGAZIONE")) = 1 then
						Response.Write "<option value='1' selected>On Line</option>"
					else
						Response.Write "<option value='1'>On Line</option>"
					end if
					%>
				</select>
			</td>
	    </tr>
	    <tr> 
			<td align="left" nowrap class="tbltext1">
				<b>Durata corso</b>&nbsp;(in ore)
			</td>
			<td> 
				<input name="txtDurata" type="text" class="textblacka" size=10 value="<%=RR("DURATA_CORSO")%>">
			</td>
	    </tr>
	    <tr> 
			<td align="left" nowrap class="tbltext1">
				<b>Disponibilit� dal*</b>
			</td>
			<td class=tbltext1> 
				<%
				if rr("DT_DISPONIBILE") = "31/12/9999" then
				%>
				<input name="txtDisponibilitaDal" type="text" class="textblacka" size="10" maxlength=10 disabled>
				&nbsp;&nbsp;&nbsp;
				<input type="checkbox" name="chkDispo" id="chkDispo" onclick="javascript:ImpFlag()" checked>
				(non disponibile)
				<%
				else
				%>
				<input name="txtDisponibilitaDal" type="text" class="textblacka" size="10" maxlength=10 value="<%=rr("DT_DISPONIBILE")%>">
				&nbsp;&nbsp;&nbsp;
				<input type="checkbox" name="chkDispo" id="chkDispo" onclick="javascript:ImpFlag()">
				(non disponibile)
				<%
				end if
				%>
				
			</td>
	    </tr>
	    <tr> 
			<td align="left" nowrap class="tbltext1">
				<b>Authoring</b>
			</td>
			<td> 
				<input name="txtAuthoring" type="text" size="50" class="textblacka" maxlength=254 value="<%=strHTMLEncode(RR("AUTHORING_CORSO"))%>">
			</td>
	    </tr>
	    <tr> 
			<td align="left" nowrap class="tbltext1">
				<b>Informazioni<br>Produttore*</b>
			</td>
			<td> 
				<span class="tbltext1">&nbsp;- Utilizzabili <label name="lblNumCar3" id="lblNumCar3">2000</label> caratteri -</span>
				<textarea name="txtInfoProd" class="textblack" cols="50" rows="3" OnKeyUp="JavaScript:CheckLenTextArea(txtInfoProd,lblNumCar3,2000)"><%=strHTMLEncode(RR("INFO_PRODUTTORE"))%></textarea>
			</td>
	    </tr>
	    <tr> 
			<td align="left" nowrap class="tbltext1">
				<b>Progetto proprietario</b>
			</td>
			<td> 
				<%
					CreateCombo("CPROJ|||" & RR("ID_PROJOWNER") & "|cboProjOwn||")
				%>
			</td>
	    </tr>
	    <tr> 
			<td align="left" nowrap class="tbltext1">
				<b>Pubblico/Privato*</b>
			</td>
			<td> 
				<select name="cboFlPubblico" id="cboFlPubblico" class=textblack>
					<option value=""></option>
					<%
					if cint(RR("FL_PUBBLICO")) = 0 then
						Response.Write "<option value='0' selected>Pubblico</option>"
					else
						Response.Write "<option value='0'>Pubblico</option>"
					end if
					if cint(RR("FL_PUBBLICO")) = 1 then
						Response.Write "<option value='1' selected>Privato</option>"
					else
						Response.Write "<option value='1'>Privato</option>"
					end if
					%>
				</select>
			</td>
	    </tr>
	    <tr> 
			<td align="left" nowrap class="tbltext1">
				<b>Prerequisiti necessari</b>
			</td>
			<td>
				<span class="tbltext1">&nbsp;- Utilizzabili <label name="lblNumCar4" id="lblNumCar4">2000</label> caratteri -</span>
				<textarea name="txtPrereqCorso" class="textblack" cols="50" rows="3" OnKeyUp="JavaScript:CheckLenTextArea(txtPrereqCorso,lblNumCar4,2000)"><%=strHTMLEncode(RR("PREREQ_CORSO"))%></textarea>
			</td>
	    </tr>
	    <tr> 
			<td align="left" nowrap class="tbltext1">
				<b>Destinatari del Corso</b>
			</td>
			<td>
				<span class="tbltext1">&nbsp;- Utilizzabili <label name="lblNumCar5" id="lblNumCar5">2000</label> caratteri -</span> 
				<textarea name="txtDestinatari" class="textblack" cols="50" rows="3" OnKeyUp="JavaScript:CheckLenTextArea(txtDestinatari,lblNumCar5,2000)"><%=strHTMLEncode(RR("DESTINATARI_CORSO"))%></textarea>
			</td>
	    </tr>
		</table>
		<br>
		<table width="500" border="0" cellspacing="2" cellpadding="1">
			<tr align="center">
				<td width="20%">
					&nbsp;
				</td>
				<td align="right" width="20%">
					<%
						PlsIndietro()
					%>
				</td>
				<td align="center" width="20%">
					<%
						PlsInvia("inserisciC")
					%>
				</td>
				<td align="left" width="20%">
					<%
						PlsElimina("javascript:Elimina(" & nIDCorso & ",'" & dtTmst & "')")
					%>
				 </td>
				<td width="20%">
					&nbsp;
				</td>
			</tr>
		</table>
		</form>
<%		

End Sub
';););););););););););););););););););););););););););););););););););););););)
Sub ImpostaPag()
	Dim sql, RR, RR2, sql2
	Dim aTp(2), aPb(2), Dispgg, Dispmm, Dispaa, Rif, i, dtTmst
		
	aTp(0) = "Off line"
	aTp(1) = "On line"

	aPb(0) = "Pubblico"
	aPb(1) = "Privato"
	
	sql = "SELECT CORSO.*, AREA.TitoloArea" &_ 
			" FROM CORSO, AREA WHERE ID_CORSO =" & nIDCorso &_
			" And CORSO.ID_AREATEMATICA = AREA.ID_AREA "
			
	Set RR = Server.CreateObject("ADODB.Recordset")
	'Response.Write sql
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
	RR.Open sql, CC, 3

	If RR.EOF then
%>
		<br>
		<table border="0" width="500" cellpadding="0" cellspacing="0">
			<tr align="center">
				<td class="tbltext3">
					Pagina momentaneamente non disponibile
				</td>
			</tr>
		</table>	
		<br>
		<table width="500" border="0" cellspacing="2" cellpadding="1">
			<tr align="center">
				<td>
					<%
						PlsIndietro()
					%>
				</td>
			</tr>
		</table> 
		<br>
<%		
	else	
		dtTmst = RR.Fields("DT_TMST")
		if rr("DT_DISPONIBILE") > date() then
			call PaginaModificabile(RR)
		else
			call PaginaNonModificabile(RR)
		end if
	end if
End Sub
';););););););););););););););););););););););););););););););););););););););)
'M A I N
Dim nIDCorso, sCodArea
%>
<!--#include virtual="/strutt_testa2.asp"-->
<!--#include virtual="/include/openconn.asp"-->
<!--#include virtual="/include/DecCod.asp"-->
<!--#include virtual="/include/HTMLEncode.asp"-->
<!--#include file="COR_Pulsanti.asp"-->
<%
	nIDCorso = Request.QueryString("IDCorso")
	sCodArea = Request.QueryString("CodArea")
	
	ControlliJavaScript()
	Testata()
	ImpostaPag()
%>
<!--#include virtual="/include/closeconn.asp"-->
<!--#include virtual="/strutt_coda2.asp"-->
