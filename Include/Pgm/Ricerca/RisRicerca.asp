<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include Virtual="/strutt_testa2.asp"-->
		
<script language="javascript" src="/Include/FunOpenWindow.js"></script>
		
<TABLE border="0" width="520" cellspacing="0" cellpadding="0">
	<tr>
		<td background="<%=session("progetto")%>/images/titoli/ricerca3.gif" width="370" height="50" valign="TOP" align="right">
			<TABLE border="0" width="520" cellspacing="0" cellpadding="0" height="50">
				<tr>
					<td width="520" valign="bottom" align="right">
						<b class="tbltext1a">Ricerca nel Portale&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b>
					</td>
				</tr>
			</TABLE>
		</td>
	</tr>
</TABLE>
<br>
<%
			
	Dim nActPagina, nTotPagina, nTamPagina, nTotRecord, nRecVis
	Dim sProgetto, sCatalogo, sWord, CompSearch
		
	If Request.Form("Catalogo") = "Link" Then
		sWord = Request.Form("Parola")
		sCatalogo = "Link"
	Else
		sWord = Request.Form("indirizzo")
		sProgetto = LCase(Mid(Session("Progetto"),2))
			
		Select case sProgetto 
			case "nsn","off","spi","scuolalav","stroccu","sns"
				 sCatalogo = "Info"
			case else
				 sCatalogo = "Info" & sProgetto	
		End Select
	End If

	nTamPagina = 10

	If Request.Querystring("Page") = "" Then
		nActPagina = 1
		nRecVis = 0 
	Else
		nActPagina = Clng(Request.Querystring("Page"))
		nRecVis = (nActPagina-1)*10 
	End If

	If sWord <> "" Then
		CompSearch = sWord
	Else
		CompSearch = Request.QueryString("search")
	End If 

	Response.Write "<TABLE width='470' border='0' callspacing='9' cellpadding='5'>"
	Response.Write "<tr><td width='20'></td>"
	Response.Write "<td class='textreda'>Hai richiesto la parola: <b><i>" & (CompSearch) & "</i></b><br></td></tr>"
		
	If Len(CompSearch) < 3 Then
		Response.Write "<tr><td width='20'></td><td class='tbltext3'><b>La parola cercata deve essere composta da almeno 3 caratteri</b></td></tr>"
		Response.End
	End If 

	'si crea un'istanza della classe Query
	Set Q = Server.CreateObject("ixsso.Query")
	'si fissa come criterio di ordinamento la raggiungibilità, discendente
	SortBy = "filename, rank[d]"
	Q.SortBy = SortBy
	' si specificano le proprietà dei documenti che vogliamo estrarre:
	' il nome del file, il suo virtual path, etc.
	Q.Columns = "DocTitle, FileName, vpath ,Path, Size, Characterization, A_Href,DocAuthor,DocThumbnail"
	' si definisce il catalogo su cui ricercare
	Q.Catalog = sCatalogo
	' si definisce la query
	If sCatalogo = "Link" Then
		Q.Query = "#FileName LK*.htm And @All " + CompSearch
	Else
		Q.Query = "#FileName info*.htm And @All " + CompSearch
	End If
	' si definisce il numero massimo di documenti che vogliamo siano estratti
	'Q.MaxRecords = 100

	On error resume next
	' viene effettuata la ricerca e viene creato il recordset dei risultati
	Set rstIndex = Q.CreateRecordSet("nonsequential")
	If err <> 0 Then
		'Response.Write "<TABLE width='470' border='0'>"
		'Response.Write "<tr><td><span class='tbltext1'>" & err.description & "-" & sCatalogo & "</span></td></tr>"
		Response.Write "<tr><td><span class='tbltext1'>La ricerca non puo' essere eseguita</span></td></tr>"
		Response.Write "<tr><td><span class='tbltext1'>Chiave di ricerca non significativa.<br> Eseguirla nuovamente in una differente modalita'</span><br></td></tr>"
		Response.Write "</TABLE>"
	Else
		npfile = 0
			 
		sPR_APPO = Session("Progetto")
		rstIndex.PageSize = nTamPagina
		rstIndex.CacheSize = nTamPagina

		nTotPagina = rstIndex.PageCount
			 
		If nActPagina < 1 Then
			nActPagina = 1
		End If
		If nActPagina > nTotPagina Then
			nActPagina = nTotPagina
		End If
			
		'mostra i risultati
		'Response.Write "<TABLE width='470' border='1' callspacing=9 cellpadding=5>"
		If Not rstIndex.Eof Then
				
			rstIndex.AbsolutePage = nActPagina
			nTotDocFind = rstIndex.Recordcount
			nTotRecord = 0 
			nInitDocVis = nRecVis + 1
			nEndDocVis = nRecVis + 10
			If nEndDocVis > nTotDocFind Then
				nEndDocVis = nTotDocFind
			End If
					
			Response.Write "<tr><td width='20'>&nbsp;</td>" 
			Response.Write "<td><b class='tbltext1'>La ricerca ha prodotto " & rstIndex.Recordcount & " risultati</b><br>"
			Response.Write "<b class='tbltext1'>Visualizzati da " & nInitDocVis & " a " & nEndDocVis & " su " & nTotDocFind & "</b><br>"
			Response.Write "</td></tr>"
				
			Set rstNotizie = Server.CreateObject("ADODB.Recordset")
			While Not rstIndex.Eof And nTotRecord < nTamPagina 
			
				sNomeFile = Split(rstIndex("Filename"),".")(0)
				sNomeFile = LCase(sNomeFile)
								
				sPreNomeFile = left (sNomeFile, 5)
				If sPreNomeFile = "info_" Then
					sNomeFile = "p" & sNomeFile
				End If
								
				sPathSrv = UCASE(Server.MapPath("\"))
				sPathFile = UCASE(rstIndex("Path"))
				sPathFileRel = Split(sPathFile,sPathSrv)(1)
				sProgFile = "/" & Split(sPathFileRel,"\")(1)
						
				Session("Progetto") = sProgFile								
%>
				<!-- #include virtual="/include/OpenConn.asp" -->
<%			
				sProgetto = Session("Progetto") 
				sDescrAzione = DecCodVal ("CPROJ",0,"",mid(Ucase(sProgetto),2),"")
				' Determino il titolo del progetto richiamato.
				
				sSQLNotizie = "SELECT id_notizie, Titolo, Abstract, Data_Ins, Autore, Nome_Notizie, Fl_Pubblicato, Data_Pubb, Fonte FROM  Notizie WHERE lower(Nome_Notizie) = '" & sNomeFile & "'"
'PL-SQL * T-SQL  
SSQLNOTIZIE = TransformPLSQLToTSQL (SSQLNOTIZIE) 
				rstNotizie.open sSQLnotizie, CC, 1, 3

				If (CLng(rstIndex("Size")) > 1024) Then
					lDimDoc = Round(CLng(rstIndex("Size"))/1024,1)
					sStrDimDoc = lDimDoc & " Kb"
				Else
					lDimDoc = CLng(rstIndex("Size"))
					sStrDimDoc = lDimDoc & " bytes"
				End If
				Proget = sDescrAzione	
				
				sPath = Replace(rstIndex("Path"),lCase(Server.MapPath("\")),"")
				sPath = Replace(sPath, "\", "/")
%>					
				<tr>
					<td class="tbltext" width="20" align="right" valign="top" class="tbltext"><b><%= nRecVis + 1 %></b></td>
					<td class="tbltext1" width="450" colspan=2>
<%						'If err.number = 0 Then
							If not rstNotizie.EOF Then 
								Titolo =  rstNotizie("titolo")
								Data = ConvDateToString(rstNotizie("Data_Ins"))
%>								<a class="tbltext1" href="javascript:OpenWindow('<%=sPath%>','','450','550','yes','no')"><b><%=Proget%></b></a>
								<span class="tbltext1"><br>
<%	
								Response.Write Titolo & "<br>"
								If isdate(Data) Then
									Response.Write " (" & Data & ")"
								End If
							Else
%>								<span class="textgray"><b><%=lcase(Proget)%></b><br>
								(<%=mid(sNomeFile,2)%>)---</span>
<%							End if
						'Else
%>							<!--span class="textgray"><b><%'=lcase(Proget)%></b><br>
							(<%'=mid(sNomeFile,2)%>)</span-->
<%						'End If
						
%>							</span>
							<br>
						</td>
					</tr>
<%					Session("Progetto") = sPR_APPO	
					nTotRecord = nTotRecord + 1	
					nRecVis = nRecVis + 1
					rstNotizie.Close

%>			
	<!-- #include virtual="/include/CloseConn.asp" -->
<%					
					
					rstIndex.MoveNext
				Wend
				set rstNotizie = nothing
			Else
			    Response.Write "<tr><td align='center'><br><span class=tbltext3><b>Non ci sono documenti <br>contenenti la parola richiesta!</b><br></span></td></tr>"
			End If

			Response.Write "</TABLE>"
			Response.Write "<TABLE border='0' width='470'>"
			Response.Write "<tr>"
			If nActPagina > 1 Then
				Response.Write "<td align='right' width='460'><a href='RisRicerca.asp?Page="& nActPagina-1 & "&search=" & CompSearch & "'>"
				Response.Write "<img border='0' alt='Pagina precedente' src=" & Session("Progetto") & "/images/precedente.gif></a></td>"
			End If
			If nActPagina < nTotPagina Then
				Response.Write "<td align='right'>"
				Response.Write "<a href='RisRicerca.asp?Page=" & nActPagina+1 & "&search=" & CompSearch & "'>"
				Response.Write "<img border='0' alt='Pagina successiva' src='" & Session("Progetto") & "/images/successivo.gif'></a></td>"
			End If
			Response.Write "</tr></TABLE>"

			rstIndex.Close
			Set rstIndex = nothing
			Session("Progetto") = sPR_APPO
		End If
		%>

		<!--#include Virtual="/strutt_coda2.asp"-->
