<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include Virtual = "/Include/openconn.asp"-->
<!--#include Virtual = "/include/DecCod.asp"-->

<html>
<head>
<script language="javascript">
function AbrirReporte(Ruta)
{
	window.open(Ruta,"","toolbar=0;location=0,directories=0,status=0,menubar=1,scrollbars=1,resizable=1,copyhistory=0,width=640,height=480,screenX=200,screenY200");
    this.close();
}
</script>
</head>
<body>
<%


    Sql = request("sql")
    TipoEmpresa = request("Tipo")
    Rol = request("Rol")
    
    set rsReporteEmpresas = Server.CreateObject("adodb.recordset")
	set oFileSystemObject = Server.CreateObject("Scripting.FileSystemObject")
	
        
     sArchivo = "\Reporte" & "_" & Rol & "_" & Session("idutente") & ".xls"

	if oFileSystemObject.FileExists(SERVER.MapPath("\BA\DocPers\ReportesEP\") & sArchivo) then
		set oArchivoSalida= oFileSystemObject.OpenTextFile(SERVER.MapPath("\BA\DocPers\ReportesEP\") & sArchivo,2)
	else
		set oArchivoSalida = oFileSystemObject.CreateTextFile(SERVER.MapPath("\BA\DocPers\ReportesEP\") & sArchivo,true)
	end if

	'Agrega el titulo y la fecha del informe
	oArchivoSalida.WriteLine "Listado de " & TipoEmpresa
	oArchivoSalida.WriteLine "Elaborado en fecha : " & date()


	oArchivoSalida.WriteLine ""
	'Agrega los titulos de los campos

    sTitulos = ""

    
    set rsReporteEmpresas = cc.execute(Sql)
    
    if not rsReporteEmpresas.EOF then 
        rsReporteEmpresas.MoveFirst 
        for each x in rsReporteEmpresas.Fields 
            sTitulos = sTitulos & x.name & chr(9)
        next
    end if 

	oArchivoSalida.WriteLine sTitulos
	oArchivoSalida.WriteLine("")

	' Recorre los registros y carga las variables con los datos
	sDatos = ""
	
	rsReporteEmpresas.MoveFirst 
	
	do while not rsReporteEmpresas.EOF 
	    for each y in rsReporteEmpresas.Fields 
			select case ucase(y.name)
			
				case "SEDE" ''columna de sede es link
				        sDatos = sDatos & y.value & Chr(9)
				case ucase(Empresa) ''columna de empresa es link si se lista x empresa si es x sede no se muestra.
					sDatos = sDatos & y.value & Chr(9)
				'18/08/2007 sostituito cuit con ruc
				'case "CUIT"
				case "RUC"
					if TipoListado <> "2" then
						if rsReporteEmpresas.Fields("SCYE") = "1"  then
						    'Seguro de Capacitacion y Empleo"
						end if 
	                    sDatos = sDatos & y.value & Chr(9)
					end if							
				case "PROVINCIA" ''columna de provincia busca denominacion en tades
						provincia = DecCodVal("PROV","0",date,y.value,0)
						sDatos = sDatos & provincia & Chr(9)
				case "PRESTACIONES" ''columna de prestaciones arma un string con las mismas
						dim RsPrestaciones 
						set RsPrestaciones = server.createObject("adodb.recordset")
		
						SQLPRESTACIONES = "select distinct descripcion from impresa_rolesserviciostipos irst, impresa_rolesservicios irs where id_impresa =" & rsReporteEmpresas.Fields(0) & " and irst.rol = irs.rol and irst.servicio = irs.servicio and irst.rol = " & Rol
                        'PL-SQL * T-SQL  
                        SQLPRESTACIONES = TransformPLSQLToTSQL (SQLPRESTACIONES) 
                       
						set RsPrestaciones = cc.execute(SQLPRESTACIONES)
		
						Prestaciones = ""
						If not RsPrestaciones.EOF then
							Do while not RsPrestaciones.EOF 
								Prestaciones = Prestaciones & RsPrestaciones("descripcion") & ";"  
								RsPrestaciones.MoveNext 
							loop 
						End if 
		
		                Prestaciones = mid(Prestaciones,1,len(Prestaciones)-1)
		                
						sDatos = sDatos & Prestaciones & Chr(9)
						set RsPrestaciones = nothing
				case "SCYE"
					if rsReporteEmpresas.Fields("SCYE") = "1" then
							'Seguro de Capacitacion y Empleo"
							sDatos = sDatos & "PARTICIPA" & Chr(9)
					else
						    sDatos = sDatos & "NO PARTICIPA" & Chr(9)
					end if  
				case "RENAE" ''columnas restantes
						if not isnull(y.value) and y.value <> "" then 
							sDatos = sDatos & mid(y.value,3) & Chr(9)
						else
						    sDatos = sDatos & chr(9)
						end if 
				case else ''columnas restantes
						sDatos = sDatos & y.value & Chr(9)
			
			end select 
	    next						
	    
        oArchivoSalida.WriteLine(sDatos)
        sDatos=""
	    
	    rsReporteEmpresas.MoveNext 
	loop
	
	oArchivoSalida.WriteLine("")



    RutaDeArchivo = "/BA/DocPers/ReportesEP/" & sArchivo

%>
<img src="/BA/Images/footer1.gif" height="1" width ="1" onload="AbrirReporte('<%=RutaDeArchivo%>')"></img>
</body>
</html>

