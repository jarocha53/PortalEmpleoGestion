<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include virtual="/strutt_testa2.asp"-->
<!--#include virtual="/util/portallib.asp"-->
<!--#include virtual="/include/openconn.asp"--> 
<!--#include virtual="/include/ckProfile.asp"-->
<!--#include virtual="/util/dbutil.asp"-->
<!--#include virtual="/include/DecCod.asp"-->
<!--#include virtual="/include/ControlDateVB.asp"-->
<%
	if ValidateService(session("idutente"),"IMP_VISIMPRESA",cc) <> "true" then 
		response.redirect "/util/error_login.asp"
	end if
%>
<script LANGUAGE="Javascript">
//include del file per fare i controlli sulla validit� delle date
<!--#include virtual="/Include/ControlDate.inc"-->

//include del file per fare i controlli sulla numericit� dei campi
<!--#include virtual="/Include/ControlNum.inc"-->
<!--#include virtual="/Include/help.inc"-->
<!--#include virtual="/Include/ControlString.inc"-->
	
	//Funzione per i controlli dei campi 
	function ControllaDati(frmValImpresa){

// -------- CONTROLLO VALIDITA' DEI DATI INDICATI ---------------		
		DataOggi=frmValImpresa.txtoggi.value;
		DataRilev = frmValImpresa.txtDataRilev.value

		// data rilevazione
		if (DataRilev == "")
		{
			alert("Il campo Data rivelazione � obbligatorio")
			frmValImpresa.txtDataRilev.focus() 
			return false
		}

		if (!ValidateInputDate(DataRilev)) 
		{
			frmValImpresa.txtDataRilev.focus() 
			return false
		}


		if (DataRilev != DataOggi)
			{
					if (ValidateRangeDate(DataOggi,DataRilev))
					{


						alert("Il campo Data rilevazione non pu� essere superiore alla data odierna")
						frmValImpresa.txtDataRilev.focus() 
						return false
					}				
			}


		// Bilancio	

		frmValImpresa.txtBilancio.value = TRIM(frmValImpresa.txtBilancio.value)
		if (frmValImpresa.txtBilancio.value == "")
		{
			alert("Il campo Bilancio � obbligatorio")
			frmValImpresa.txtBilancio.focus() 
			return false
		}

		if (!IsNum(frmValImpresa.txtBilancio.value)){
			alert("Il campo Bilancio deve essere numerico")
			frmValImpresa.txtBilancio.focus() 
			return false
		}
		// fatturato
		frmValImpresa.txtFat.value == TRIM(frmValImpresa.txtFat.value)
		if (frmValImpresa.txtFat.value == ""){
			alert("Il campo Fatturato � obbligatorio")
			frmValImpresa.txtFat.focus() 
			return false
		}
		if (!IsNum(frmValImpresa.txtFat.value)){
			alert("Il campo Fatturato deve essere numerico")
			frmValImpresa.txtFat.focus() 
			return false
		}
		//  valuta
		if (frmValImpresa.cmbValut.selectedIndex == 0) {
			alert ("Il campo Valuta � obbligatorio")
			frmValImpresa.cmbValut.focus()
			return false
		}
/*
		if (frmValImpresa.cmbValut.value == ""){
			alert("Campo obbligatorio")
			frmValImpresa.cmbValut.focus() 
			return false
		}
*/		
		// dimenzione azienda
		if (frmValImpresa.cmbDimAz.value == ""){
			alert("Il campo Dimensione azienda � obbligatorio")
			frmValImpresa.cmbDimAz.focus() 
			return false
		}
	
/*		if (!IsNum(frmValImpresa.txtDimAz.value)){
			alert("Il valore deve essere numerico")
			frmValImpresa.txtDimAz.focus() 
			return false
		}
*/
		return true
	}
</script>


<%
'on error resume next 

dim sSQL
dim rsValImp
dim cnConn

dim sCodImp
dim sDtRil
dim sNumBil
dim sNumFat
dim sCodVal
dim sDimAz
dim sDtTmst

dim sIsa

dim sMask
	
' Controllo i diritti dell'utente connesso.
' La variabile di sessione mask � cos� formatta:
' 1 Byte abilitazione funzione Impresa
' 2 Byte abilitazione funzione Sede_impresa
' 3 Byte abilitazione funzione Val_impresa
' dal 4� byte in poi si visualizzano le provincie.
		
sMask = Session("Diritti")

sCodImp	= Request("sCodImp")
sDtRil		= Request("DataRil")

if mid(Session("UserProfiles"),1,1) = "0" then
	sIsa = "0"
else
	sIsa = mid(Session("UserProfiles"),1,2)
end if

%>
    	<form name="frmIndietro" method="post" action="IMP_VisValImpresa.asp">
			<input type="hidden" name="IdPers" value="<%=sIdPers%>">
			<input type="hidden" name="sCodImp" value="<%=sCodImp%>">
		</form>


<table border="0" width="530" cellspacing="0" cellpadding="0" height="81">
   <tr>
     <td width="530" background="<%=Session("Progetto")%>/images/titoli/strumenti2g.gif" height="81" valign="bottom" align="right">
       <table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
         <tr>
           <td width="100%" valign="top" align="right"><b class="tbltext1a">Gestion Empresa&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></b></td>
         </tr>
       </table>
     </td>
   </tr>
</table>

<br>

<!--#include file="Imp_Label.asp" -->
<br>

<table cellpadding="0" cellspacing="0" width="500" border="0">
   <tr height="18">
		<td class="sfondomenu" height="18" width="67%">
			<span class="tbltext0"><b>GESTION EMPRESA</b></span></td>
			
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">	</td>
		
		<td valign="middle" align="right" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1">(*) campi obbligatori</td></td>
		
   </tr>
   
   <tr width="371" class="SFONDOCOMMAZ">
   <td colspan="3">Ingresar la informaci�n del balance de la empresa.
	<a href="Javascript:Show_Help('/Pgm/help/Imprese/IMP_ModValImpresa')"><img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
	</td>
	</tr>   
	<tr height="2">
		<td colspan="3" class="SFONDOCOMMAZ" background="<%=Session("Progetto")%>/images/separazione.gif">
		</td>
	</tr>
</table><br>




<br>
 
<%
							
sSQL = "SELECT ID_IMPRESA, DT_RIL, NUM_BIL, NUM_FATT, COD_VALUTA, DIM_IMPRESA, DT_TMST " &_
		" FROM VAL_IMPRESA WHERE ID_IMPRESA=" & clng(sCodImp) & " AND DT_RIL=" &_
			ConvDateToDb(ConvStringToDate(sDtRil))
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
set rsValImp = CC.Execute(sSQL)

sNumBil = CDbl (rsValImp("NUM_BIL"))
sNumFat	= CDbl (rsValImp("NUM_FATT"))
sCodVal	= rsValImp("COD_VALUTA")
sDimAz	= rsValImp("DIM_IMPRESA")
sDtTmst	= rsValImp("DT_TMST")
rsValImp.Close 
set rsValImp = nothing
%>
   
<form method="post" name="frmValImpresa" action="IMP_CnfValImpresa.asp" onsubmit="return ControllaDati(this)">
<input type="hidden" name="Action" value="Mod">
<input type="hidden" name="txtDtTmst" value="<%=sDtTmst%>">
<input type="hidden" name="sCodImp" value="<%=sCodImp%>">  
<input type="hidden" name="txtoggi" value="<%=ConvDateToString(Now())%>">
     
    <table border="0" cellpadding="0" cellspacing="1" width="500" style="WIDTH: 500px">
        <tr>
			<td align="middle" colspan="2">
				<p align="left">
				<span class="tbltext1"><b>Fecha del relavamiento(dd/mm/aaaa)*</b></span>
				</p>
            </td>
            <td align="left" colspan="2" width="60%">
				<p align="left">
					<input readonly class="textblack" size="15" maxlength="10" style="WIDTH: 78px;" name="txtDataRilev" value="<%=sDtRil%>">
					</p>
            </td>
        </tr>
        <tr>
			<td align="left" colspan="2">
				<p align="left">
					<span class="tbltext1"><b>Balance*</b></span>
					
				</p>
			</td>
			<td align="left" colspan="2" width="60%">
				<p align="left">
					<input style="TEXT-TRANSFORM: uppercase; WIDTH:	220px;" class="textblack" size="30" maxlength="40" name="txtBilancio" value="<%=sNumBil%>">
				</p>
			</td>
        </tr>
        <tr>
            <td align="left" colspan="2">
				<p align="left">
				    <span class="tbltext1"><b>Facturaci�n*</b></span>
										
				</p>
            </td>
            <td align="left" colspan="2" width="60%">
				<p align="left">
						<input style="TEXT-TRANSFORM: uppercase; WIDTH:	220px;" class="textblack" size="30" maxlength="40" name="txtFat" value="<%=sNumFat%>">
		        </p>
            </td>
        </tr>
        <tr>
            <td align="left" colspan="2">
				<p align="left">
				    <span class="tbltext1"><b>Valor*</b></span>
					
				</p>
            </td>
            <td align="left" colspan="2" width="60%">
				<p align="left">
					
						<%
							sInt = "VALUT|"& sIsa & "|" & date & "|" &_
										 sCodVal & "|cmbValut| "			
							CreateCombo(sInt)
						%>
					
				</p>
            </td>
        </tr>
        <tr>
            <td align="left" colspan="2">
				<p align="left">
				    <span class="tbltext1"><b>Dimensi�n de la Empresa*</b></span>
					
				</p>
            </td>
            <td align="left" colspan="2" width="60%">
				<p align="left">
					
						<%
							sInt = "DIMAZ|"& sIsa & "|" & date & "|" &_
										 sDimAz & "|cmbDimAz| "			
							CreateCombo(sInt)
						%>

					
		        </p>
            </td>
		</tr>
        <tr>
            <td align="middle" colspan="2">&nbsp;</td>
            <td align="middle" colspan="2" width="60%">&nbsp;</td>
        </tr>
        <tr>
            <td align="left" colspan="2"></td>
            <td align="left" colspan="2" width="60%">
				<p align="left">
				
					<% 
					'if 	ckProfile(mid(sMask,5,2),4) then 
					
						'<input type='submit' name='Invia' value='Conferma'>
					%>	
					<% 
					'end if 
					%>
				
				</p>
            </td>
        </tr>
    </table>	
    
     <table cellpadding="0" cellspacing="0" width="500" border="0">	
	
		
	<tr>
	<td align="right"><a href="javascript:document.frmIndietro.submit()">
	<img src="<%=Session("progetto")%>/images/indietro.gif" border="0">
	</a>
	</td>
	<td align="left">
	<input type="image" name="Conferma" src="<%=Session("progetto")%>/images/conferma.gif">
	</td>
	</tr>
</table>
    
</form>



<p>&nbsp;</p>
<!--#include virtual ="/strutt_coda2.asp"-->
<!--#include virtual ="/include/closeconn.asp"-->
