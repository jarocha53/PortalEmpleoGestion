<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include Virtual = "/strutt_testa3.asp"-->
<!--#include Virtual = "/include/OpenConn.asp"-->
<!--#include Virtual = "/include/DecCod.asp"-->
<!--#include Virtual = "/include/ControlDateVB.asp"-->
<!--#include Virtual = "/util/dbutil.asp"-->

<script LANGUAGE="Javascript">

//include del file per fare i controlli sulla numericit� dei campi
<!--#include Virtual = "/Include/ControlNum.inc"-->

//include del file per fare i controlli sulla stringa
<!--#include Virtual = "/Include/ControlString.inc"-->
<!--#include virtual = "/Include/help.inc"-->

	
	
	//Funzione per i controlli dei campi 
	function ControllaDati(){
// -------- CONTROLLO VALORI INSERITI -------------------------		

/*
 Parte di controlli sui campi del referente
*/
		document.frmIscri.txtCognome.value=TRIM(document.frmIscri.txtCognome.value)
		sCognome=ValidateInputStringWithNumber(document.frmIscri.txtCognome.value)
		if  (sCognome==false){
			alert("Cognome formalmente errato.")
			document.frmIscri.txtCognome.focus() 
			return false
		}
			
		if (frmIscri.txtCognome.value == ""){
			alert("Il campo Cognome � obbligatorio!")
			frmIscri.txtCognome.focus() 
			return false
		}
		
		document.frmIscri.txtNome.value=TRIM(document.frmIscri.txtNome.value)
		sNome=ValidateInputStringWithNumber(document.frmIscri.txtNome.value)
		if  (sNome==false){
			alert("Nome formalmente errato.")
			document.frmIscri.txtNome.focus() 
			return false
		}		
		if (frmIscri.txtNome.value == ""){
			alert("Il campo Nome � obbligatorio!")
			frmIscri.txtNome.focus() 
			return false
		}
		
		if (frmIscri.elements[2].selectedIndex == 0) {
			alert ("Campo Obbligatorio")
			frmIscri.elements[2].focus()
			return false
		}
		
		
		//Controlli su LOGIN: posso inserire solo numeri e / o lettere
		var anyString = document.frmIscri.txtLogin.value;
		document.frmIscri.txtLogin.value = TRIM(document.frmIscri.txtLogin.value)
		if (frmIscri.txtLogin.value == ""){
			alert("Campo obbligatorio")
			frmIscri.txtLogin.focus() 
			return false
		}
					
		for (var i=0; i<=anyString.length-1; i++){
			if(((anyString.charAt(i) >= "A") && (anyString.charAt(i) <= "Z")) || 
				((anyString.charAt(i) >= "a") && (anyString.charAt(i) <= "z")) || 
				((anyString.charAt(i) >= "0") && (anyString.charAt(i) <= "9")))
			{
			}
			else
			{		
				document.frmIscri.txtLogin.focus();
			 	alert("Inserire nel campo Login un valore valido");
				return false
			}		
		}
		
		//Email
		document.frmIscri.txtMail.value=TRIM(document.frmIscri.txtMail.value)
		if (document.frmIscri.txtMail.value ==""){
			alert("Email obbligatoria")
			document.frmIscri.txtMail.focus() 
			return false
		}
		
		appoEmail=ValidateEmail(document.frmIscri.txtMail.value)
		
		if  (appoEmail==false){
			alert("Indirizzo Email formalmente errato")
			document.frmIscri.txtMail.focus() 
			return false
		}		
		

		return true
	}	



	//Cancella Dati
	function Cancella(frmIscri){
		document.frmIscri.reset();
		return false
	}

</script>
<% 
	dim sidsede
	dim simpresa

   sidsede=Request.Form("idsede")
   sidImp=Request.Form("idimpresa")
   
   
   %>

<div align="center">

<form action="IMP_CnfRichiesta.asp" method="post" name="frmIscri" id="frmIscri">
<input type="hidden" name="idimpresa" value="<%=sIdImp%>">
<input type="hidden" name="idsede" value="<%=sidSede%>">

<table border="0" width="530" cellspacing="0" cellpadding="0" height="81">
   <tr>
     <td width="530" background="<%=Session("Progetto")%>/images/titoli/strumenti2g.gif" height="81" valign="bottom" align="right">
       <table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
         <tr>
           <td width="100%" valign="top" align="right"><b class="tbltext1a">Gesti�n Empresa &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></b></td>
         </tr>
       </table>

     </td>
   </tr>
</table><br>

<table cellpadding="0" cellspacing="0" width="500" border="0">
   <tr height="18">
		<td class="sfondomenu" height="18" width="67%">
			<span class="tbltext0"><b>Solicitud de Usuario/Contrase�a</b></span></td>
			
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
		<td valign="middle" align="right" class="tbltext1" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">(*) campi obbligatori</td>
    </tr>
		<td valign="middle" align="right" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1"></td>
		
   </tr>
   <tr width="371" class="SFONDOCOMMAZ">
   <td colspan="3">Para obtener la contrase�a, ingresar los datos solicitados y presione <b>Enviar</b>.<br>
		La contrase�a otorgada por el sistema,ser� enviada a la direcci�n de correo ingresada.<br>
		Posteriormente podra cambiarla si asi lo desea.	
	<a href="Javascript:Show_Help('/Pgm/help/Imprese/IMP_RichiestaLog')"><img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
	</td>
	</tr>   
	<tr height="2">
		<td colspan="3" class="SFONDOCOMMAZ" background="<%=Session("Progetto")%>/images/separazione.gif">
		</td>
	</tr>
</table><br>

	
<!---------- Inizio Tabella Inserimento ---------->
<table border="0" width="350" cellpadding="2" cellspacing="1">   
   <tr>
		<td COLSPAN="2" width="500">
			<span class="tbltext1a"><b>Representante:</b></span>
		</td>
	</tr>
	<tr>
		<td colspan="2"></td>
	</tr>     
	<tr>
		<td align="left" class="tbltext1" nowrap><b>
			&nbsp;&nbsp;Apellido </b>*	
		</td>
		<td align="left">
			<input style="TEXT-TRANSFORM: uppercase;" size="49" class="textblack" maxlength="50" name="txtCognome">
		</td>
   </tr> 
	<tr>
		<td align="left" class="tbltext1" nowrap><b>
			&nbsp;&nbsp;Nombre</b>*		
		</td>
		<td align="left">
			<input style="TEXT-TRANSFORM: uppercase;" size="49" class="textblack" maxlength="50" name="txtNome">
		</td>
   </tr> 	
	<tr>
		<td align="left" class="tbltext1" nowrap><b>
			&nbsp;&nbsp;Cargo </b>*	
		</td>
		<td align="left">
			<%
				'Richiamo funzione che popola la combo RUOLO
				'-----------------------------------------------			
				sRefRuo = "RUOLO|0|" & date & "|" & NULL & "|cmbRefRuo|ORDER BY DESCRIZIONE"			
				CreateCombo(sRefRuo)
				'-----------------------------------------------				
			%>		
		</td>
   </tr>   
	<tr>
		<td align="left" class="tbltext1" width="85" nowrap><b>
			&nbsp;&nbsp;Usuario </b>*		
		</td>
		<td align="left">
			<input style="TEXT-TRANSFORM: uppercase;" size="37" maxlength="16" class="textblack" name="txtLogin">
		</td>
   </tr>    
	<tr>
		<td align="left" class="tbltext1" nowrap><b>
			&nbsp;&nbsp;E-mail </b>*		
		</td>
		<td align="left">
			<input size="49" class="textblack" maxlength="50" name="txtMail">
		</td>
   </tr>     
   <tr>
		<td>&nbsp;</td>
   </tr>

</table>

<!---------- Fine Tabella Inserimento ---------->

<br>     
<table border="0" cellpadding="1" cellspacing="1" width="360" align="center">
	<tr>
		<td align="middle" colspan="2">
			<input type="image" name="Invia" src="<%=Session("Progetto")%>/images/conferma.gif" border="0" value="Conferma" onclick="return ControllaDati()">
		</td>
   </tr>
	<tr>
		<td>&nbsp;
		</td>
   </tr>
	
</table>
</form>
</div>
<!--#include Virtual = "/include/closeconn.asp"-->
<!--#include Virtual = "/strutt_coda3.asp"-->
