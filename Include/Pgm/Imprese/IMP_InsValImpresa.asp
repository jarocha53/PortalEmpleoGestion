<!--#include virtual="/util/portallib.asp"-->
<!--#include virtual="/strutt_testa2.asp"-->
<!--#include virtual="/include/openconn.asp"-->
<!--#include virtual="/util/dbutil.asp"-->
<!--#include virtual="/include/DecCod.asp"-->
<%
	if ValidateService(session("idutente"),"IMP_VISIMPRESA",cc) <> "true" then 
		response.redirect "/util/error_login.asp"
	end if
%>
<script LANGUAGE="Javascript">
//include del file per fare i controlli sulla validit� delle date
<!--#include virtual="/Include/ControlDate.inc"-->

//include del file per fare i controlli sulla numericit� dei campi
<!--#include virtual="/Include/ControlNum.inc"-->
<!--#include virtual="/Include/ControlString.inc"-->
<!--#include virtual="/Include/help.inc"-->


	
	//Funzione per i controlli dei campi 
	function ControllaDati(frmValImpresa){


// -------- CONTROLLO VALIDITA' DEI DATI INDICATI ---------------		


		// data rilevazione
		DataOggi=frmValImpresa.txtoggi.value;
		DataRilev = frmValImpresa.txtDataRilev.value
		
		if (DataRilev == ""){
				alert("Il campo Data rivelazione � obbligatorio")
			frmValImpresa.txtDataRilev.focus() 
			return false
		}
		
		

		if (!ValidateInputDate(DataRilev)) 
		{
			frmValImpresa.txtDataRilev.focus() 
			return false
		}

		if (DataRilev != DataOggi)
			{
					if (ValidateRangeDate(DataOggi,DataRilev))
					{
						alert("Il campo Data rilevazione non pu� essere superiore alla data odierna")
						frmValImpresa.txtDataRilev.focus() 
						return false
					}				
			}
		
		
		// bilancio
		frmValImpresa.txtBilancio.value = TRIM(frmValImpresa.txtBilancio.value)
		if (frmValImpresa.txtBilancio.value == ""){
			alert("Il campo Bilancio � obbligatorio")
			frmValImpresa.txtBilancio.focus() 
			return false
		}

		if (!IsNum(frmValImpresa.txtBilancio.value)){
			alert("Il campo Bilancio deve essere numerico")
			frmValImpresa.txtBilancio.focus() 
			return false
		}
		// fatturato
		frmValImpresa.txtFat.value = TRIM(frmValImpresa.txtFat.value)
		if (frmValImpresa.txtFat.value == ""){
			alert("Il campo Fatturato � obbligatorio")
			frmValImpresa.txtFat.focus() 
			return false
		}

		if (!IsNum(frmValImpresa.txtFat.value)){
			alert("Il campo Fatturato deve essere numerico")
			frmValImpresa.txtFat.focus() 
			return false
		}

		if (frmValImpresa.cmbValut.value == "") {
			alert ("Il campo Valuta � obbligatorio")
			frmValImpresa.cmbValut.focus()
			return false
		}
/*
		if (frmValImpresa.cmbValut.value == ""){
			alert("Campo obbligatorio")
			frmValImpresa.cmbValut.focus() 
			return false
		}
*/
		if (frmValImpresa.cmbDimAz.value == ""){
			alert("Il campo Dimensione azienda � obbligatorio")
			frmValImpresa.cmbDimAz.focus() 
			return false
		}
/*
		if (!IsNum(frmValImpresa.txtDimAz.value)){
			alert("Il valore deve essere numerico")
			frmValImpresa.txtDimAz.focus() 
			return false
		}
*/
		return true
	}
	
	function userfocus()
{
document.frmValImpresa.txtDataRilev.focus()
//document.forms(0).item("txtDescrizione").focus();
}
</script>
<%
'on error resume next 


dim sSQL
dim sIdImpresa
dim sIsa

sCodImp = Request("sCodImp")

if mid(Session("UserProfiles"),1,1) = "0" then
	sIsa = "0"
else
	sIsa = mid(Session("UserProfiles"),1,2)
end if

%>
<!--<body background="../../images/sfondo1.gif" onload="return userfocus()">-->

<table border="0" width="530" cellspacing="0" cellpadding="0" height="81">
   <tr>
     <td width="530" background="<%=Session("Progetto")%>/images/titoli/strumenti2g.gif" height="81" valign="bottom" align="right">
       <table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
         <tr>
           
          <td width="100%" valign="top" align="right"><b class="tbltext1a">Gestion 
            Empresa &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></b></td>
         </tr>
       </table>
     </td>
   </tr>
</table><br>

<!--#include file = "IMP_Label.asp"-->


<br>

<table cellpadding="0" cellspacing="0" width="500" border="0">
   <tr height="18">
		
    <td class="sfondomenu" height="18" width="67%"> <span class="tbltext0"><b>&nbsp;GESTION 
      EMPRESA </b></span></td>
			
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
		
		
    <td valign="middle" align="right" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1">(*) 
      campo obligatorioi</td>
    </td>
		
   </tr>
   <tr width="371" class="SFONDOCOMMAZ">
    <td colspan="3">Ingresar datos relacionados a la empresa. <a href="Javascript:Show_Help('/Pgm/help/Imprese/IMP_InsValImpresa')"><img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a> 
    </td>
	</tr>   
	<tr height="2">
		<td colspan="3" class="SFONDOCOMMAZ" background="<%=Session("Progetto")%>/images/separazione.gif">
		</td>
	</tr>
</table><br>

<form method="post" name="frmValImpresa" action="IMP_CnfValImpresa.asp" onsubmit="return ControllaDati(this)">
<input type="hidden" name="Action" value="Ins">
<input type="hidden" name="sCodImp" value="<%=sCodImp%>">
<input type="hidden" name="txtoggi" value="<%=ConvDateToString(Now())%>">
				
        
    <table border="0" cellpadding="0" cellspacing="1" width="500" style="WIDTH: 500px">
        <tr>
       
			
      <td align="left" colspan="2"> <span class="tbltext1"><b>Data Rilevazione</b> 
        (dd/mm/aaaa)*</span> </td>
            <td align="left" colspan="2" width="60%">
				<p align="left">
					<span class="table">
							<input type="text" style="TEXT-TRANSFORM: uppercase" maxlength="10" name="txtDataRilev" size="15" class="textblack" value="<%=ConvDateToString(Now())%>">
					
					</span>
				</p>
            </td>
        </tr>
        <tr>
			<td align="left" colspan="2">
				<span class="tbltext1"><b>Bilancio*</b></span>
            </td>
            <td align="left" colspan="2" width="60%">
				<p align="left">
					<input style="TEXT-TRANSFORM: uppercase; WIDTH: 220px;" class="textblack" size="30" maxlength="40" name="txtBilancio">
	          </p>
        </tr>
        <tr>
            
      <td align="left" colspan="2"> <span class="tbltext1"><b>Facturaci&oacute;n*</b></span> 
      </td>
            <td align="left" colspan="2" width="60%">
				<p align="left">
					<span class="table">
						<input class="textblack" size="30" maxlength="40" name="txtFat" style="TEXT-TRANSFORM: uppercase; WIDTH:	220px;">
					</span>
		        </p>
            </td>
        </tr>
        <tr>
            <td align="left" colspan="2">
					<span class="tbltext1"><b>Valuta*</b></span>
            </td>
            <td align="left" colspan="2" width="60%">
				<p align="left">
						<%
							sInt = "VALUT|"& sIsa & "|" & date & "|02|cmbValut| "			
							CreateCombo(sInt)
						%>
					
				</p>
            </td>
        </tr>
        <tr>
            
      <td align="left" colspan="2"> <span class="tbltext1"><b>Dimensi&oacute;n 
        de la Empresa*</b></span> </td>
            <td align="left" colspan="2" width="60%">
					<span Class="table">
						<%
							sInt = "DIMAZ|"& sIsa & "|" & date & "| |cmbDimAz| "			
							CreateCombo(sInt)
						%>
					</span>
            </td>
		</tr>
        <tr>
            <td align="middle" colspan="2">&nbsp;</td>
            <td align="middle" colspan="2" width="60%">&nbsp;</td>
        </tr>
        
    </table>
  
  <table cellpadding="0" cellspacing="0" width="500" border="0">	

	<tr>	
	<td align="right"><a href="javascript:window.history.back()">
	<img border="0" src="<%=Session("progetto")%>/images/indietro.gif">	
	</td>
	<td width="5%"></td>
	<td align="left">
	<input type="image" name="Conferma" src="<%=Session("progetto")%>/images/conferma.gif">
	</td>
	</tr>
</table>
</form>
<p>&nbsp;</p>
<!--#include virtual="/strutt_coda2.asp"-->
<!--#include virtual ="/include/closeconn.asp"-->
