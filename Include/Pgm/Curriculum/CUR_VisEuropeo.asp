<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<% 

Dim sqlEuropeo
Dim RREuropeo

set RREuropeo = Server.CreateObject("ADODB.Recordset")

aDati = Split(aCV(2),"|")
aSezioni = Split(aCV(3),"|")

nSezioni = ubound(aSezioni)

call getDatiDB(id_utente)
%>
	<tr>
		<td>&nbsp;</td>
	</tr>

	<tr>
		<td>
	<%if STAMPA = "SI" then%>			
	<table border="0" cellspacing="0" cellpadding="3" align="center" width="600">
	<%else%>
	<table border="0" cellspacing="0" cellpadding="3" width="100%">
	<%end if%>	
	<tr>
		<td background="/images/Curriculum/sfondomenudx1.gif" class="titoloEuropeo" align="LEFT" valign="top" width="150"><b>FORMATO EUROPEO PARA EL CURRICULUM VITAE</b>
			
		</td>
		<td width="60%">
			&nbsp;
		</td>
	</tr>
	<tr>
		<td background="/images/Curriculum/sfondomenudx1.gif"> &nbsp;</td>
		<td> &nbsp;</td>
	</tr>
	<tr>
		<td background="/images/Curriculum/sfondomenudx1.gif" class="textblubold" align="LEFT" valign="top" width="150"><b>INFORMACION PERSONAL</b></td>
		<td width="60%">
			&nbsp;
		</td>
	</tr>
	<tr>
		<td background="/images/Curriculum/sfondomenudx1.gif" class="textblu" valign="top" align="LEFT" width="150">Nombre</td>
		<td class="textblu">
			<table border="0">
				<tr>
					<td class="textblu" width="5">&nbsp;</td>
					<td class="textblu">
						<b><%=sCognome & " " & sNome %></b>
					</td>
				</tr>
			</table>	
		</td>
	</tr>
<%
	' RESIDENZA 
	if aDati(3) = "1" then
%>
	<tr>
		<td background="/images/Curriculum/sfondomenudx1.gif" class="textblu" valign="top" align="LEFT" width="150">Direcci�n</td>
		<td class="textblu">
			<table border="0">
				<tr>
					<td class="textblu" width="5">&nbsp;</td>
					<td class="textblu">
<%				if len(sInd_Res) > 0 then 
					Response.write sInd_Res & " - "
				end if 
				if len(sCap_Res) > 0 then
					Response.Write sCap_Res & " "
				end if 
				if len(DescrComune(sCom_Res)) > 1 then 
					Response.Write DescrComune(sCom_Res)
				end if
				if len(sPrv_Res) > 0 then 
					'Response.Write " ("  & sPrv_Res & ")"
				end if
%>					</td>
				</tr>
			</table>
		</td>
	</tr>
<% 
	end if
	
	' RECAPITO TELEFONICO 
	if aDati(5) = "1" and len(sNum_Tel) > 0 then
%>
	<tr>
		<td background="/images/Curriculum/sfondomenudx1.gif" class="textblu" valign="top" align="LEFT" width="150">Tel�fono</td>
		<td class="textblu">
			<table border="0">
				<tr>
					<td class="textblu" width="5">&nbsp;</td>
					<td class="textblu">
						<%=Server.HTMLEncode(sNum_Tel)%>
					</td>
				</tr>
			</table>
		</td>
	</tr>
<%
	end if

	' RECAPITO TELEFONICO ALTERNATIVO
	
sSQL = "SELECT NUM_TEL_DOM FROM PERSONA WHERE ID_PERSONA = '" + id_utente + "'"
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
set rsPER = cc.execute(sSQL)
if not rsPER.eof then sNum_Tel2 = rsPER(0)
set rsPER = nothing
	
	'if aDati(6) = "1" and len(aCV(4)) > 0 then
	if aDati(6) = "1" and len(sNum_Tel2) > 0 then
	
%>
	<tr>
		<td background="/images/Curriculum/sfondomenudx1.gif" class="textblu" valign="top" align="LEFT" width="150">Otro tel�fono</td>
		<td class="textblu">
			<table border="0">
				<tr>
					<td class="textblu" width="5">&nbsp;</td>
					<td class="textblu">
						<!--<%=Server.HTMLEncode(aCV(4))%>-->
						<%=Server.HTMLEncode(sNum_Tel2)%>						
					</td>
				</tr>
			</table>
		</td>
	</tr>
<%
	end if

	' FAX 
	if aDati(7) = "1" and len(aCV(5)) > 0 then
%>
	<tr>
		<td background="/images/Curriculum/sfondomenudx1.gif" class="textblu" valign="top" align="LEFT" width="150">Fax</td>
		<td class="textblu">
			<table border="0">
				<tr>
					<td class="textblu" width="5">&nbsp;</td>
					<td class="textblu">
						<%=Server.HTMLEncode(aCV(5))%>
					</td>
				</tr>
			</table>
		</td>
	</tr>
<%
	end if

	' EMAIL 
	if len(sE_Mail) > 0 and aDati(4) = "1" then
%>
	<tr>
		<td background="/images/Curriculum/sfondomenudx1.gif" class="textblu" valign="top" align="LEFT" width="150">E-mail</td>
		<td class="textblu">
			<table border="0">
				<tr>
					<td class="textblu" width="5">&nbsp;</td>
					<td class="textblu">
						<%=Server.HTMLEncode(sE_Mail)%>
					</td>
				</tr>
			</table>
		</td>
	</tr>
<%
	end if


	' NAZIONALITA' 
	sSCit = decCodVal("STATO","0","",sStat_Cit,"")
	if Len(sStat_Cit) > 0 and aDati(1) = "1" and len(sSCit) > 0 then
%>
	<tr>
		<td background="/images/Curriculum/sfondomenudx1.gif" class="textblu" valign="top" align="LEFT" width="150">Ciudadan�a</td>
		<td class="textblu">
			<table border="0">
				<tr>
					<td class="textblu" width="5">&nbsp;</td>
					<td class="textblu">
						<%=Trasforma(sSCit)%>
					</td>
				</tr>
			</table>
		</td>
	</tr>
<%
	end if

	' DATA DI NASCITA
	if aDati(2) = "1" and len(sDt_Nasc) > 0 then
%>
	<tr>
		<td background="/images/Curriculum/sfondomenudx1.gif" class="textblu" valign="top" align="LEFT" width="150">Fecha de Nacimiento</td>
		<td class="textblu">
			<table border="0">
				<tr>
					<td class="textblu" width="5">&nbsp;</td>
					<td class="textblu">
						<%=sDt_Nasc%>
					</td>
				</tr>
			</table>
		</td>
	</tr>
<%
	end if

	' LUOGO DI NASCITA
	' si indica il comune di nascita o lo stato di nascita.
	sSNasc = DescrComune(sStat_Nasc)
	sComNasc = DescrComune(sCom_Nasc)
	if aDati(2) = "1" and (len(sComNasc) > 1 or len(sSNasc) > 1) then
%>
	<tr>
		<td background="/images/Curriculum/sfondomenudx1.gif" class="textblu" valign="top" align="LEFT" width="150">Lugar de Nacimiento</td>
		<td class="textblu">
			<table border="0">
				<tr>
					<td class="textblu" width="5">&nbsp;</td>
					<td class="textblu">
<%						if len(sComNasc) > 1 then
							Response.Write Trasforma(sComNasc)
						else
							Response.Write Trasforma(sSNasc) 
						end if 
						if len(sPrv_Nasc) > 0 then
							'Response.Write " (" & sPrv_Nasc & ")"
						end if
%>	
					</td>
				</tr>
			</table>
		</td>
	</tr>
<%
	end if

	' STATO CIVILE 
	sSCiv = decCodVal("STCIV","0","",sStat_Civ,"")
	if len(sStat_Civ) > 0 and aDati(0) = "1" and len(sSCiv) > 0 then
%>
	<tr>
		<td background="/images/Curriculum/sfondomenudx1.gif" class="textblu" valign="top" align="LEFT" width="150">Estado Civil</td>
		<td class="textblu">
			<table border="0">
				<tr>
					<td class="textblu" width="5">&nbsp;</td>
					<td class="textblu">
						<%=Trasforma(sSCiv)%>
					</td>
				</tr>
			</table>
		</td>
	</tr>
<%
	end if
	
	' OBBLIGO DI LEVA
	if len(sPos_Mil) > 0 and aDati(8) = "1" then
%>
	<tr>
		<td background="/images/Curriculum/sfondomenudx1.gif" class="textblu" valign="top" align="LEFT" width="150">Servicio Militar</td>
		<td class="textblu">
			<table border="0">
				<tr>
					<td class="textblu" width="5">&nbsp;</td>
					<td class="textblu">
						<%=Trasforma(decCodVal("POMIL","0","",sPos_Mil,""))%>
					</td>
				</tr>
			</table>
		</td>
	</tr>
<%
	end if
	
	
	
	' ESPERIENZE PROFESSIONALI
if nSezioni > 2 then
	if aSezioni(3) = "1" and len(aCV(10)) > 0 then
		if IsArray(Split(aCV(10),"|")) and aCV(10) <> "0" then
			aPro = Split(aCV(10),"|")
			
			sIn = "("
			for cont = 0 to ubound(aPro)
				sMansione = Split(aPro(cont),"$",-1,1)(0)
				sIn = sIn & "'" &  sMansione & "',"
			next
			sIn = mid(sIn,1,len(sIn)-1)
			sIn = sIn & ")"
			
			sqlEuropeo = "SELECT ESPRO.RAG_SOC AS AZIENDA, ESPRO.DT_INI_ESP_PRO AS DI, nvl(ESPRO.DT_FIN_ESP_PRO,to_Date('31/12/9999','dd/mm/yyyy')) AS DF, ESPRO.COD_MANSIONE AS MAN " &_ 
						" FROM ESPRO " &_ 
						" WHERE ID_PERSONA = " & id_utente &_ 
						" AND ESPRO.COD_MANSIONE IN " & sIn  &_
						" ORDER BY DT_FIN_ESP_PRO desc, ESPRO.DT_INI_ESP_PRO desc "
'PL-SQL * T-SQL  
SQLEUROPEO = TransformPLSQLToTSQL (SQLEUROPEO) 
			RREuropeo.Open sqlEuropeo, CC
			if  not RREuropeo.eof then
%>				<tr>
					<td background="/images/Curriculum/sfondomenudx1.gif" class="textblubold" align="LEFT" valign="top" width="150"><b><br>ANTECEDENTES LABORALES</b></td>
					<td>
						&nbsp;
					</td>
				</tr>				
<%				do while not RREuropeo.eof
%>					<tr>
						<td background="/images/Curriculum/sfondomenudx1.gif" class="textblu" valign="top" align="LEFT" width="150">
							<li>del <%=mid(ConvDateToString(RREuropeo.Fields("DI").value),4)%>
<%							if RREuropeo.Fields("DF") <> ConvStringToDate("31/12/9999") then
								Response.write " al "  & mid(ConvDateToString(RREuropeo.Fields("DF").value),4)
							else
								Response.Write " a la fecha "
							end if

%>	
						</td>
						<td class="textblu">
							<table border="0"><tr><td valign="top" width="5"><li class="tratto">&nbsp;</li><td valign="top" class="textblu">
<%							if RREuropeo.Fields("MAN") <> "" then
								'02/01/2009 DAMIAN CAMBIA EL NOMBRE DEL CAMPO PARA QUE MUESTRE COHERENTEMENTE EL PUESTO				
								'sMansi = "SELECT                 MANSIONE FROM CODICI_ISTAT WHERE CODICE='" & RREuropeo.Fields("MAN") & "'"			
								sMansi = "SELECT DENOMINAZIONE as MANSIONE FROM CODICI_ISTAT c, figureprofessionali f WHERE f.cod_ej=c.CODICE and f.id_figprof='" & RRInformale.Fields("MAN").value & "'"															
'PL-SQL * T-SQL  
SMANSI = TransformPLSQLToTSQL (SMANSI) 
								set rsMansi = cc.execute(sMansi)
								if not rsMansi.eof then		
									Response.Write "como " & lcase(TRIM(rsMansi("MANSIONE")))
									rsMansi.close
								end if
								set rsMansi=nothing
							end if
							
%>							</td></tr></table>
						</td>
					</tr>				
<%					if RREuropeo.Fields("AZIENDA") <> ""  and ucase(RREuropeo.Fields("AZIENDA")) <> "NON DICHIARATA" then
%>					<tr>
						<td background="/images/Curriculum/sfondomenudx1.gif" class="textblu" valign="top" align="LEFT" width="150">
							<table width="120" align="center" border="0"><tr><td class="textblu">Empresa</td></tr></table>
						</td>
						<td class="textblu">
							<table border="0">
								<tr>
									<td width="18" class="textblu">&nbsp;</td>
									<td class="textblu"><%=Trasforma(RREuropeo.Fields("AZIENDA"))%></td>
								</tr>
							</table>
						</td>
					</tr>
<%					end if
					RREuropeo.MoveNext
				loop
			end if	
			RREuropeo.Close
			sqlEuropeo = ""
		end if
	end if
end if

' ISTRUZIONE E FORMAZIONE'
sCondizione = false
if nSezioni > 8 then
	if (aSezioni(9) = "1" and len(aCV(15)) > 0) OR (aSezioni(2) = "1" and len(aCV(9)) > 0) then
		sCondizione = true
	end if

elseif nSezioni > 1 then
	if (aSezioni(2) = "1" and len(aCV(9)) > 0) then
		sConsizione = true
	end if
end if

if sCondizione then
%>		<tr>
			<td background="/images/Curriculum/sfondomenudx1.gif" class="textblubold" align="LEFT" valign="top" width="150"><b><br>INSTRUCCION Y FORMACI�N</b></td>
			<td>
				&nbsp;
			</td>
		</tr>			
<%
	' TITOLI DI STUDIO
	if aSezioni(2) = "1" and len(aCV(9)) > 0 then
		if IsArray(Split(aCV(9),"|")) and aCV(9) <> "0" then
			aFor = Split(aCV(9),"|")
			sDatiTitoli = "'" & Replace(aCV(9),"|","','") & "'"
%>
			<tr>
				<td background="/images/Curriculum/sfondomenudx1.gif" class="textblu" valign="top" align="LEFT" width="150">NIVEL EDUCATIVO</td>
				<td class="textblu">&nbsp;</td>
			</tr>
	
<%			sqlEuropeo = "SELECT AA_STUD, COD_STAT_STUD, COD_TIT_STUD, DT_TMST" &_ 
						" FROM TISTUD" &_ 
						" WHERE ID_PERSONA = " & id_utente &_
						" AND COD_TIT_STUD IN (" & sDatiTitoli & ")" &_
						" ORDER BY AA_STUD DESC ,COD_STAT_STUD"
'PL-SQL * T-SQL  
SQLEUROPEO = TransformPLSQLToTSQL (SQLEUROPEO) 
			RREuropeo.Open sqlEuropeo, CC				
			nAnno = 0000
			if  not RREuropeo.eof then
				do while not RREuropeo.eof
					anio = RREuropeo.Fields("AA_STUD")
					if (not isnull(anio)) and (anio <> "") then 
						anio = clng(anio)
					end if 
					if anio <> clng(nAnno) then
						if clng(nAnno) <> 0000 then
							Response.Write "</td></tr>"
						end if
						Response.Write "<tr>" &_ 
										" <td background='/images/Curriculum/sfondomenudx1.gif' class='textblu' valign='top' align='LEFT' width='150'>" &_
										"	<li>" & RREuropeo.Fields("AA_STUD") & "</li>" &_
										" </td>" &_
										" <td class='textblu'>"
						nAnno = RREuropeo.Fields("AA_STUD")
					end if
					Response.Write "<TABLE border=0>" &_ 
									" <TR>" &_ 
									"  <TD valign=top width=5><li class=tratto>&nbsp;</LI></TD>" &_ 
									"  <TD valign=top  class=textblu>" &  Trasforma(DecCodVal("TSTUD","0",RREuropeo.Fields("DT_TMST"),RREuropeo.Fields("COD_TIT_STUD"),0))  &_ 
										"(" & decDiz_Dati(RREuropeo.Fields("COD_STAT_STUD"), "COD_STAT_STUD") & ")" &_
									"  </TD>" &_
									" </TR>" &_
									"</TABLE>"
					RREuropeo.MoveNext
				loop
			end if
			RREuropeo.Close
			sqlEuropeo = ""
		end if
%>
		</td>
	</tr>	
<%
	end if
end if



'FORMAZIONE
if nSezioni > 8 then
	if aSezioni(9) = "1" and len(aCV(15)) > 0 then
		if IsArray(Split(aCV(15),"|")) and aCV(15) <> "0" then
			aFormaz = Split(aCV(15),"|")
			sFormazione = "'" & Replace(aCV(15),"|","','") & "'"
%>
			<tr>
				<td background="/images/Curriculum/sfondomenudx1.gif" class="textblu" valign="top" align="LEFT" width="150">CAPACITACI�N Y FORMACI�N PROFESIONAL</td>
				<td class="textblu">&nbsp;</td>
			</tr>
	
<%			sqlEuropeo = "SELECT F.ID_FORMAZIONE, F.COD_TCORSO, F.AA_CORSO, F.COD_STATUS_CORSO, A.DENOMINAZIONE " & _
						" FROM FORMAZIONE F, AREA_CORSO A " & _
						" WHERE A.ID_AREA = F.ID_AREA " & _
						" AND ID_PERSONA = " & id_utente &_
						" AND ID_FORMAZIONE IN (" & sFormazione & ")" &_
						" ORDER BY F.AA_CORSO DESC"
'PL-SQL * T-SQL  
SQLEUROPEO = TransformPLSQLToTSQL (SQLEUROPEO) 
			RREuropeo.Open sqlEuropeo, CC
		
			if  not RREuropeo.eof then
				nAnno = "0000"
				do while not RREuropeo.eof	
					IF RREuropeo.Fields("AA_CORSO")	<> "" or not isnull(RREuropeo.Fields("AA_CORSO")) then 
						if cstr(RREuropeo.Fields("AA_CORSO")) <> nAnno then
							if nAnno <> "0000" then
								Response.Write "</td></tr>"
							end if
							Response.Write "<tr>" &_ 
											" <td background='/images/Curriculum/sfondomenudx1.gif' class='textblu' valign='top' align='LEFT' width='150'>" &_
											"	<li>" & RREuropeo.Fields("AA_CORSO") & "</li>" &_
											" </td>" &_
											" <td class='textblu'>"
							nAnno = RREuropeo.Fields("AA_CORSO")
						end if
					end if
					
					Response.Write "<TABLE border=0>" &_ 
									" <TR>" &_ 
									"  <TD valign=top width=5><li class=tratto>&nbsp;</LI></TD>" &_ 
									"  <TD valign=top  class=textblu>" &  Trasforma(RREuropeo.Fields("DENOMINAZIONE")) &_ 
									" - " & LCASE(RREuropeo.Fields("COD_TCORSO")) &_
									" ("
					select case RREuropeo.Fields("COD_STATUS_CORSO")
						case "0"
							Response.Write "Complet�"
						case "1"
							Response.Write "Abandon�"
						case "2"
							Response.Write "En Curso"
						case "3"
							Response.Write "Incompleto"
						case "4"
							Response.Write "No corresponde"						
						case else
	
					end select
					Response.Write ")" &_
									"  </TD>" &_
									" </TR>" &_
									"</TABLE>"
					
					RREuropeo.MoveNext
				loop
				Response.Write "</td></tr>"
			end if
			RREuropeo.Close
			sqlEuropeo = ""
%>				</td>
			</tr>	
<%		
		end if
	end if
End if
	
' ISCRIZIONE ALBO 
			if nSezioni > 9 then
				if aSezioni(10) = "1" and len(aCV(16)) > 0 then
					if IsArray(Split(aCV(16),"|")) and aCV(16) <> "0" then
						aFor = Split(aCV(16),"|")
						sDatiTitoli = "'" & Replace(aCV(16),"|","','") & "'"
%>			<tr>
			<td background="/images/Curriculum/sfondomenudx1.gif" class="textblubold" align="LEFT" valign="top" width="150"><b><br>MATRICULA PROFESIONAL</b></td>
			<td>
				&nbsp;
			</td>
		</tr>
<%
								sqlEuropeo = "SELECT COD_ALBO,DT_DEC_ISCR,PRV_ISCR,NUM_ISCR, DT_TMST " &_
											" FROM PERS_ALBO WHERE ID_PERSONA = " & id_utente &_ 
											" AND COD_ALBO IN (" & sDatiTitoli & ") " &_
											" ORDER BY DT_DEC_ISCR DESC "

'PL-SQL * T-SQL  
SQLEUROPEO = TransformPLSQLToTSQL (SQLEUROPEO) 
								RREuropeo.Open sqlEuropeo, CC
								
								if  not RREuropeo.eof then
									do while not RREuropeo.eof	
										Response.Write "<tr>" &_ 
											" <td background='/images/Curriculum/sfondomenudx1.gif' class='textblu' valign='top' align='LEFT' width='150'>" &_
											"	<li>" & Trasforma(DecCodVal("TALBO","0",RREuropeo.Fields("DT_TMST"),RREuropeo.Fields("COD_ALBO"),0)) & "</li>" &_
											" </td>" &_
											" <td class='textblu'>"
																														
										Response.Write "<TABLE border=0>" &_ 
											" <TR>" &_ 
											"  <TD valign=top width=5><li class=tratto>&nbsp;</LI></TD>" &_ 
											"  <TD valign=top  class=textblu>Iscrizione n� " & RREuropeo.Fields("NUM_ISCR") & " in data " & ConvDateToString(RREuropeo.Fields("DT_DEC_ISCR")) &_
												"(" & RREuropeo.Fields("PRV_ISCR") & ")" &_
											"  </TD>" &_
											" </TR>" &_
											"</TABLE>"

'										Response.Write "<table>" &_
'															"<tr>" &_
'																"<td width=5><li>&nbsp;</li></td>" &_
'																"<td class=textblu>Iscrizione n� " & RREuropeo.Fields("NUM_ISCR") & " all'albo dei " &  &_
'																" in data " & ConvDateToString(RREuropeo.Fields("DT_DEC_ISCR")) &_
'																" (" & RREuropeo.Fields("PRV_ISCR") & ")</TD>" &_
'															"</tr>" &_
'														"</table>"
										RREuropeo.MoveNext
									loop
									
								end if
								RREuropeo.close
								sqlEuropeo = ""
					end if
%>
		</td>
	</tr>
<%
				end if
			end if	
'CAPACITA' E COMPETENZE PERSONALI	
sCondizione = false
if nSezioni > 7 then
	if (aSezioni(1) = "1" and len(aCV(8)) > 0) or (aSezioni(6) = "1" and len(aCV(12)) > 0) or (aSezioni(7) = "1" and len(aCV(13)) > 0) or (aSezioni(8) = "1" and len(aCV(14)) > 0) then
		sCondizione = true
	end if

elseif nSezioni > 6 then
		if (aSezioni(1) = "1" and len(aCV(8)) > 0) or (aSezioni(6) = "1" and len(aCV(12)) > 0) or (aSezioni(7) = "1" and len(aCV(13)) > 0) then
			sConsizione = true
		end	if
	elseif nSezioni > 5 then
			if (aSezioni(1) = "1" and len(aCV(8)) > 0) or (aSezioni(6) = "1" and len(aCV(12)) > 0) then
				sConsizione = true
			end if
		elseif nSezioni > 0 then
				if (aSezioni(1) = "1" and len(aCV(8)) > 0)then
					sConsizione = true
				end if
	
end if

if sCondizione then
%>		<tr>
			<td background="/images/Curriculum/sfondomenudx1.gif" class="textblubold" align="LEFT" valign="top" width="150"><b><br>DESEMPE�O Y COMPETENCIAS</b></td>
			<td>
				&nbsp;
			</td>
		</tr>		
<%
	
	' LINGUE 
		if aSezioni(1) = "1" and len(aCV(8)) > 0 then
			if IsArray(Split(aCV(8),"|")) and aCV(8) <> "0" then
				aDatiLingue = Split(aCV(8),"|")
%>			<tr>
				<td background="/images/Curriculum/sfondomenudx1.gif" class="textblu" valign="top" align="LEFT" width="150">IDIOMAS</td>
				<td class="textblu">
<% 					sDatiLingue = Replace(aCV(8),"|",",")
					sqlEuropeo = "SELECT CONOSCENZE.DENOMINAZIONE, PERS_CONOSC.COD_GRADO_CONOSC " &_ 
								" FROM CONOSCENZE, PERS_CONOSC " &_ 
								" WHERE CONOSCENZE.ID_CONOSCENZA = PERS_CONOSC.ID_CONOSCENZA " &_ 
								" AND PERS_CONOSC.ID_PERSONA = " & id_utente &_ 
								" AND PERS_CONOSC.IND_STATUS = 0 " &_ 
								" AND PERS_CONOSC.ID_CONOSCENZA IN (" & sDatiLingue & ")" &_
								" ORDER BY CONOSCENZE.DENOMINAZIONE"
								
'PL-SQL * T-SQL  
SQLEUROPEO = TransformPLSQLToTSQL (SQLEUROPEO) 
					RREuropeo.Open sqlEuropeo, CC
					if not RREuropeo.eof then
						do while not RREuropeo.eof
							Response.Write "<TABLE border=0><TR><TD valign=top width=5><li class=tratto>&nbsp;</LI></TD><TD valign=top  class=textblu>"
							Response.Write Trasforma(RREuropeo.Fields("DENOMINAZIONE")) & " (" & DecCodValDizDati("PERS_CONOSC","COD_GRADO_CONOSC_LENG",Cstr(RREuropeo.Fields("COD_GRADO_CONOSC"))) & ")"
							Response.Write "</TD></TR></TABLE>"
							RREuropeo.MoveNext
						loop
					end if
					RREuropeo.Close
					sqlEuropeo = ""
%>				</td>
			</tr>
<%	
			end if
		end if
END IF
	
' CONOSCENZE INFORMATICHE
if nSezioni > 5 then
	if aSezioni(6) = "1" and len(aCV(12)) > 0 then
		if IsArray(Split(aCV(12),"|")) and aCV(12) <> "0" then
			aDatiLingue = Split(aCV(12),"|")
%>
		<tr>
			<td background="/images/Curriculum/sfondomenudx1.gif" class="textblu" valign="top" align="LEFT" width="150"><br>CONOCIMIENTOS INFORM�TICOS</td>
			<td class="textblu"><br>
<%
			sDatiLingue = Replace(aCV(12),"|",",")
			sqlEuropeo = "SELECT CONOSCENZE.DENOMINAZIONE, PERS_CONOSC.COD_GRADO_CONOSC " &_ 
						" FROM CONOSCENZE, PERS_CONOSC " &_ 
						" WHERE CONOSCENZE.ID_CONOSCENZA = PERS_CONOSC.ID_CONOSCENZA " &_ 
						" AND PERS_CONOSC.ID_PERSONA = " & id_utente &_ 
						" AND PERS_CONOSC.IND_STATUS = 0 " &_ 
						" AND PERS_CONOSC.ID_CONOSCENZA IN (" & sDatiLingue & ")" &_
						" ORDER BY CONOSCENZE.DENOMINAZIONE"
'PL-SQL * T-SQL  
SQLEUROPEO = TransformPLSQLToTSQL (SQLEUROPEO) 
			RREuropeo.Open sqlEuropeo, CC
			if not RREuropeo.eof then
				do while not RREuropeo.eof
					Response.Write "<TABLE border=0><TR><TD valign=top align=left width=5><li class=tratto>&nbsp;</LI></TD><TD valign=top  class=textblu>"
					Response.Write Trasforma(RREuropeo.Fields("DENOMINAZIONE")) & " (" & decGradoConosc(CInt(RREuropeo.Fields("COD_GRADO_CONOSC"))) & ")"
					Response.Write "</TD></TR></TABLE>"
					RREuropeo.MoveNext
				loop
			end if
			RREuropeo.Close
			sqlEuropeo = ""
%>			
			</td>
		</tr>
<%
		end if
	end if
end if

'PATENTI E CERTIFICAZIONI'
if nSezioni > 7 then
	if aSezioni(8) = "1" and len(aCV(14)) > 0 then
		if IsArray(Split(aCV(14),"|")) and aCV(14) <> "0" then
			aPat = Split(aCV(14),"|")
			sPatenti = "'" & Replace(aCV(14),"|","','") & "'"
%>
		<tr>
			<td background="/images/Curriculum/sfondomenudx1.gif" class="textblu" valign="top" align="LEFT" width="150"><br>LICENCIAS DE CONDUCCI�N</td>
			<td class="textblu"><br>
<%				sqlEuropeo = "SELECT COD_PERM, DT_TMST" & _
							" FROM AUTCERT " & _
							" WHERE ID_PERSONA = " & id_utente & _
							" AND COD_PERM IN (" & sPatenti & ")" &_
							" AND nvl(DT_SCADENZA, to_date('31/12/9999', 'dd/mm/yyyy')) >= " & convDateToDB(date)
				
'PL-SQL * T-SQL  
SQLEUROPEO = TransformPLSQLToTSQL (SQLEUROPEO) 
				RREuropeo.Open sqlEuropeo, CC		

				if  not RREuropeo.eof then
					do while not RREuropeo.eof
						Response.Write "<TABLE border=0><TR><TD valign=top width=5><li class=tratto>&nbsp;</LI></TD><TD valign=top  class=textblu>"
						Response.Write Trasforma(DecCodVal("TPERM","0",RREuropeo.Fields("DT_TMST"),RREuropeo.Fields("COD_PERM"),0))
						Response.Write "</TD></TR></TABLE>"
						RREuropeo.MoveNext
					loop
				end if
				RREuropeo.Close
				sqlEuropeo = ""
%>
			</td>
		</tr>
<%		
		end if
	end if
end if			
			
	' DISPONIBILITA' 
	
if nSezioni > 6 then	
	if aSezioni(7) = "1" and len(aCV(13)) > 0 then
		if IsArray(Split(aCV(13),"|")) and aCV(13) <> "0" then
			sDisponibilita = "'" & Replace(aCV(13),"|","','") & "'"
			sCondizione = "codice IN (" & sDisponibilita & ")"
			sTabella = "TDISP"
			dData	 = Date()
			nOrder   = 2
			Isa		 = 0
			aDisponibilita = decodTadesToArray(sTabella,dData,sCondizione,nOrder,Isa)
			if isArray(aDisponibilita) then
%>				<tr>
					<td background="/images/Curriculum/sfondomenudx1.gif" class="textblubold" align="LEFT" valign="top" width="150"><b><br>DISPONIBILIDADES/LIMITACIONES</b></td>
					<td>
						&nbsp;
					</td>
				</tr>	
		
<%				dim nCodArea, sbr
				nCodArea = 00
				
				for i = 0 to ubound(aDisponibilita) -1						
					if clng(aDisponibilita(i,i+1,i+1)) <> clng(nCodArea) then
							if clng(nCodArea) <> 00 then
								Response.Write "</td></tr>"
							end if
							Response.Write "<tr>" &_ 
											" <td background='/images/Curriculum/sfondomenudx1.gif' class='textblu' valign='top' align='LEFT' width='150'>" &_
											"	<br>" & Trasforma(DecCodVal("CL_VD", 0, Date(),aDisponibilita(i,i+1,i+1),0))  &_
											" </td>" &_
											" <td class='textblu'><br>"
							nCodArea = aDisponibilita(i,i+1,i+1)
						end if
						Response.Write "<TABLE border=0>" &_ 
										" <TR>" &_ 
										"  <TD valign=top width=5><li class=tratto>&nbsp;</LI></TD>" &_ 
										"  <TD valign=top  class=textblu>" &  Trasforma(aDisponibilita(i,i+1,i)) & "</TD>" &_
										" </TR>" &_
										"</TABLE>"
					next
				Response.Write "</td></tr>"
				Erase aDisponibilita
			end if
		end if
	end if
end if

			
' PRESENTAZIONE 
if nSezioni <> -1 then
	if aSezioni(0) = "1" and len(aCV(6)) > 0 then
%>
		<tr>
			<td background="/images/Curriculum/sfondomenudx1.gif" class="textblubold" align="LEFT" valign="top" width="150"><b><br>PRESENTACION</b></td>
			<td class="textblu">
				<table border="0">
					<td width="5" class="textblu">&nbsp;</td>
					<td class="textblu">
						<br><%=Server.HTMLEncode(aCV(6))%>
					</td>
				</table>
			</td>
		</tr>
<%
	end if
end if

' OBIETTIVI 
if nSezioni > 3 then
	if aSezioni(4) = "1" and len(aCV(7)) > 0 then
%>
		<tr>
			<td background="/images/Curriculum/sfondomenudx1.gif" class="textblubold" align="LEFT" valign="top" width="150"><b><br>OBJETIVOS</b></td>
			<td class="textblu">
				<table border="0">
					<td width="5" class="textblu">&nbsp;</td>
					<td class="textblu">
						<br><%=Server.HTMLEncode(aCV(7))%>
					</td>
				</table>
			</td>
		</tr>
<%
	end if
end if

' ALTRE INFO '
if nSezioni > 4 then
	if aSezioni(5) = "1" then
		if IsArray(Split(aCV(11),"|")) and len(aCV(11)) > 0 then
%>
		<tr>
			<td background="/images/Curriculum/sfondomenudx1.gif" class="textblubold" align="LEFT" valign="top" width="150"><b><br>INFORMACION ADICIONAL</b></td>
			<td class="textblu">
				&nbsp;
			</td>
		</tr>
		<tr>	
			<td background="/images/Curriculum/sfondomenudx1.gif" class="textblu" valign="top" align="LEFT" width="150">&nbsp;</td>
			<td class="textblu">
<%				aInfo = Split(aCV(11),"|")
				for cont = 0 to UBound(aInfo)
					Response.Write "<TABLE border=0><TR><TD valign=top width=5><li class=tratto>&nbsp;</LI></TD><TD valign=top  class=textblu>"
					Response.Write Server.HTMLEncode(aInfo(cont))
					Response.Write "</TD></TR></TABLE>"
				next
%>			</td>
		</tr>
<%
		end if
	end if
end if

' PRIVACY 
%>		<tr>
			<td class="textblu" colspan="2">
				&nbsp;
			</td>
		</tr>
		<tr>
			<td class="textblu" colspan="2">
				<b>
				
'PL-SQL * T-SQL  
<%			'Server.execute ("/PLAVORO/TESTI/SISTDOC/SISTEMA REDAZIONALE/AUTORIZZAZIONI/Info_Privacy.htm")
'PL-SQL * T-SQL  
			'Server.execute ("/TESTI/AUTORIZZAZIONI/Info_Privacy.htm")
%>					
				<b>
			</td>
		</tr>
 <%' FINE CAMPI %>
	</table>
	</td>
</tr>
<% 
	'Rilascio il recordset
	set RREuropeo = nothing

	if Request("mode").Item <> "stampa" then %>
	<!-- #include file="CUR_VisBottoni.asp" -->
<% end if %>
