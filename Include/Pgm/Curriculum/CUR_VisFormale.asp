<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%

Dim sqlFormale	'stringa sql di esexcuzione
Dim RRFormale	'recordset utilizzato per tutte le query

'''''''SM inizio
dim vData, DescProv 
'''''''SM fine																

set RRFormale = Server.CreateObject("ADODB.Recordset")

aDati = Split(aCV(2),"|")
aSezioni = Split(aCV(3),"|")

nSezioni = ubound(aSezioni)
call getDatiDB(id_utente)
%>
	<tr>
		<td align='center'>
			<SPAN class='textblutitolo'><br>Curriculum Vitae<br></SPAN>
			<SPAN class='textblubold'>de</SPAN>
			<SPAN class='textblunome'><br><%=sNome%>&nbsp;<%=sCognome%></P>
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>
<%		if STAMPA = "SI" then 
%>			<br><br><table border='0' cellspacing='2' cellpadding='3' align=center width=600>
<%		else 
%>			<table border='0' cellspacing='2' cellpadding='3' width=100%>
<%		end if 
		
		if aDati(0) = "1" or aDati(1) = "1" or aDati(2) = "1" or _
			aDati(3) = "1" or aDati(4) = "1" or aDati(5) = "1" or _
			aDati(6) = "1" or aDati(7) = "1" or aDati(8) = "1" then
		
		
		Response.Write "<tr>"
		if STAMPA = "SI" then 
%>			<td class='textblu' width=170 valign=top >INFORMACION PERSONAL</td>
<%		else 
%>			<td class='textblu' width=90 valign=top >INFORMACION PERSONAL</td>
<%		end if 
		Response.Write "<td class='textblu'>"
	'Response.Write "<ul>"
		
		
		' STATO CIVILE 
			sSCiv = decCodVal("STCIV","0","",sStat_Civ,"")
			if len(sStat_Civ) > 0 and aDati(0) = "1" and sSCiv <> "" then
%>				<table>
					<tr>
						<td  width=5><li>&nbsp;</li></td>
						<td class=textblu>Estado Civil : <%=lcase(sSCiv)%></td>
					</tr>
				</table>
<%			end if
			
		' NAZIONALITA' 
			sSCit = decCodVal("STATO","0","",sStat_Cit,"")
			if Len(sStat_Cit) > 0 and aDati(1) = "1" and sSCit <> "" then
%>				<table>
					<tr>
						<td  width=5><li>&nbsp;</li></td>
						<td class=textblu>Nacionalidad : <%=decCodVal("STATO","0","",sStat_Cit,"")%></td>
					</tr>
				</table>
<%			end if

		' DATA DI NASCITA 
			if aDati(2) = "1" and len(sDt_Nasc) > 0 then
%>				<table>
					<tr>
						<td width=5><li>&nbsp;</li></td>
						<td class=textblu>Fecha de Nacimiento : <%=sDt_Nasc%></td>
					</tr>
				</table>

<%			end if

		' LUOGO DI NASCITA 
			sSNasc = DescrComune(sStat_Nasc)
			sComNasc = DescrComune(sCom_Nasc)
			if aDati(2) = "1" and (len(sComNasc) > 1 or len(sSNasc) > 1) then
%>				<table>
					<tr>
						<td width=5><li>&nbsp;</li></td>
						<td class=textblu>Lugar de Nacimiento : 
<%							if len(sComNasc) > 1 then
								Response.Write Trasforma(sComNasc)
							else
								Response.Write Trasforma(sSNasc) 
							end if 
					
							if len(sPrv_Nasc) > 0 then 
								'Response.Write " (" & sPrv_Nasc & ")"
							end if 
				
%>						</td>
					</tr>
				</table>
<%			end if

		' RESIDENZA 
			if aDati(3) = "1" then
%>				<table>
					<tr>
						<td width=5><li>&nbsp;</li></td>
						<td class=textblu>Residencia : 
<%			 				if len(sInd_Res) > 0 then
								Response.Write Trasforma(sInd_Res) & " - "
							end if 
							if len(sCap_Res) > 0 then 
								Response.Write sCap_Res & " "
							end if 
							if len(DescrComune(sCom_Res)) > 1 then 
								Response.Write Trasforma(DescrComune(sCom_Res))
							end if 
							if len(sPrv_Res) then 
								'Response.Write " ("  & sPrv_Res & ")"
							end if
%>						</td>
					</tr>
				</table>
<% 			end if

		' RECAPITO TELEFONICO 
			if aDati(5) = "1" and len(sNum_Tel) > 0 then
%>				<table>
					<tr>
						<td width=5><li>&nbsp;</li></td>
						<td class=textblu>Nro. Tel�fono : <%=Server.HTMLEncode(sNum_Tel)%></td>
					</tr>
				</table>					
<%			end if

		' RECAPITO TELEFONICO ALTERNATIVO 


sSQL = "SELECT NUM_TEL_DOM FROM PERSONA WHERE ID_PERSONA = '" + cstr(id_utente) + "'"
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
set rsPER = cc.execute(sSQL)
if not rsPER.eof then sNum_Tel2 = rsPER(0)
set rsPER = nothing

			'if aDati(6) = "1" and len(aCV(4)) > 0 then
			if aDati(6) = "1" and len(sNum_Tel2) > 0 then			
%>				<table>
					<tr>
						<td width=5><li>&nbsp;</li></td>
						<!--<td class=textblu>Nro. Tel�fono Alternativo : <%=Server.HTMLEncode(aCV(4))%></td>-->
						<td class=textblu>Nro. Tel�fono Celular : <%=Server.HTMLEncode(sNum_Tel2)%></td>
					</tr>
				</table>
<%			end if

		' FAX 
			if aDati(7) = "1" and len(aCV(5)) > 0 then
%>				<table>
					<tr>
						<td width=5><li>&nbsp;</li></td>
						<td class=textblu>Fax : <%=Server.HTMLEncode(aCV(5))%></td>
					</tr>
				</table>			
<%			end if
	
		' EMAIL 
			if len(sE_Mail) > 0 and aDati(4) = "1" then
%>				<table>
					<tr>
						<td width=5><li>&nbsp;</li></td>
						<td class=textblu>E-Mail : <%=Server.HTMLEncode(sE_Mail)%></td>
					</tr>
				</table>			
<%			end if
	
		' OBBLIGO DI LEVA 
			if len(sPos_Mil) > 0 and aDati(8) = "1" then
%>				<table>
					<tr>
						<td width=5><li>&nbsp;</li></td>
						<td class=textblu>Servicio Militar : <%=Trasforma(decCodVal("POMIL","0","",sPos_Mil,""))%></td>
					</tr>
				</table>			
<%			end if
			Response.Write "</td>"
			Response.Write "</tr>"
		else
			Response.Write "<tr><td class='textblu' colspan=2>&nbsp;</td></tr>"
		end if
		
		
		' PRESENTAZIONE 
			if nSezioni <> -1 then
				if aSezioni(0) = "1" and len(aCV(6)) > 0 then
%>					<tr>
						<td class='textblu' valign=top >PRESENTACION</td>
						<td class='textblu'>
							<table>
								<tr>
									<td width=20>&nbsp;</td>
									<td class=textblu><%=Server.HTMLEncode(aCV(6))%></td>
								</tr>
							</table>			
						</td>
					</tr>
<%				end if
			end if
				
		' ESPERIENZE FORMATIVE 
			if nSezioni > 1 then
				if aSezioni(2) = "1" and len(aCV(9)) > 0 then
					if IsArray(Split(aCV(9),"|")) and aCV(9) <> "0" then
						aFor = Split(aCV(9),"|")
						sDatiTitoli = "'" & Replace(aCV(9),"|","','") & "'"
%>					<tr>
						<td class='textblu' valign=top >NIVEL EDUCATIVO</td>
							<td class='textblu'>
<%
								sqlFormale = "SELECT AA_STUD, COD_STAT_STUD, COD_TIT_STUD, COD_LIV_STUD, DT_TMST " &_
											" FROM TISTUD WHERE ID_PERSONA = " & id_utente &_ 
											" AND COD_TIT_STUD IN (" & sDatiTitoli & ") " &_
											" ORDER BY AA_STUD DESC ,COD_STAT_STUD" 

 
SQLFORMALE = TransformPLSQLToTSQL (SQLFORMALE) 
								RRFormale.Open sqlFormale, CC

								
								if  not RRFormale.eof then
									do while not RRFormale.eof		
										if RRformale.Fields("COD_STAT_STUD") = "0" then					
											Response.Write "<table>" &_
																"<tr>" &_
																	"<td width=5><li>&nbsp;</li></td>" &_
																	"<td class=textblu>" & DecCodVal("LSTUD","0",RRFormale.Fields("DT_TMST"),RRFormale.Fields("COD_LIV_STUD"),0) &_
																	    " - " & Trasforma(DecCodVal("TSTUD","0",RRFormale.Fields("DT_TMST"),RRFormale.Fields("COD_TIT_STUD"),0)) &_
																		" (" & decDiz_Dati(RRFormale.Fields("COD_STAT_STUD"), "COD_STAT_STUD") & " en " & RRFormale.Fields("AA_STUD") & ")" &_
																	"</td>" &_
																"</tr>" &_
															"</table>"
										else
											Response.Write "<table>" &_
																"<tr>" &_
																	"<td width=5><li>&nbsp;</li></td>" &_
																	"<td class=textblu>" & DecCodVal("LSTUD","0",RRFormale.Fields("DT_TMST"),RRFormale.Fields("COD_LIV_STUD"),0) &_
																	    " - " & Trasforma(DecCodVal("TSTUD","0",RRFormale.Fields("DT_TMST"),RRFormale.Fields("COD_TIT_STUD"),0)) &_
																		" (" & decDiz_Dati(RRFormale.Fields("COD_STAT_STUD"), "COD_STAT_STUD") & " - Nivel: " & RRFormale.Fields("AA_STUD") & ")" &_
																	"</td>" &_
																"</tr>" &_
															"</table>"
										End if					
'											Response.Write "<table>" &_
'																"<tr>" &_'
'																	"<td width=5><li>&nbsp;</li></td>" &_
'																	"<td class=textblu>" &  RRFormale.Fields("AA_STUD") & " - " & Trasforma(DecCodVal("TSTUD","0",RRFormale.Fields("DT_TMST"),RRFormale.Fields("COD_TIT_STUD"),0)) &_
'																		" (" & decDiz_Dati(RRFormale.Fields("COD_STAT_STUD"), "COD_STAT_STUD") & ")" &_
'																	"</td>" &_
'																"</tr>" &_
'															"</table>"
										RRFormale.MoveNext
									loop
									
								end if
								RRFormale.close
								sqlFormale = ""
					end if
%>
						</td>
					</tr>
<%
				end if
			end if

' ISCRIZIONE ALBO 
			if nSezioni > 9 then
				if aSezioni(10) = "1" and len(aCV(16)) > 0 then
					if IsArray(Split(aCV(16),"|")) and aCV(16) <> "0" then
						aFor = Split(aCV(16),"|")
						sDatiTitoli = "'" & Replace(aCV(16),"|","','") & "'"
%>					<tr>
						<td class='textblu' valign=top >MATRICULA PROFESIONAL</td>
							<td class='textblu'>
<%
								sqlFormale = "SELECT COD_ALBO,DT_DEC_ISCR,PRV_ISCR,NUM_ISCR, DT_TMST " &_
											" FROM PERS_ALBO WHERE ID_PERSONA = " & id_utente &_ 
											" AND COD_ALBO IN (" & sDatiTitoli & ") " &_
											" ORDER BY DT_DEC_ISCR DESC "

 
SQLFORMALE = TransformPLSQLToTSQL (SQLFORMALE) 
								RRFormale.Open sqlFormale, CC
								
								if  not RRFormale.eof then
									do while not RRFormale.eof	
									
									''''SM inizio
									vData=ConvDateToString(Date())
									DescProv = DecCodVal("PROV",0,vData,RRFormale.Fields("PRV_ISCR"),0)		
									''' SM fine				
										Response.Write "<table>" &_
															"<tr>" &_
																"<td width=5><li>&nbsp;</li></td>" &_
																"<td class=textblu>Inscripci�n n� " & RRFormale.Fields("NUM_ISCR") &_
																" del Registro de " &_
																Trasforma(DecCodVal("TALBO","0",RRFormale.Fields("DT_TMST"),RRFormale.Fields("COD_ALBO"),0)) &_
																" con Fecha " & ConvDateToString(RRFormale.Fields("DT_DEC_ISCR")) &_
																" (" & DescProv & ")</TD>" &_
															"</tr>" &_
														"</table>"
										RRFormale.MoveNext
									loop
									
								end if
								RRFormale.close
								sqlFormale = ""
					end if
%>
						</td>
					</tr>
<%
				end if
			end if
							
		' LINGUE 
			if nSezioni > 0 then
				if aSezioni(1) = "1" and len(aCV(8)) > 0 then
					if IsArray(Split(aCV(8),"|")) and aCV(8) <> "0" then
					'	aDatiLingue = Split(aCV(8),"|")
%>					<tr>
						<td class='textblu' valign=top >IDIOMAS</td>
						<td class='textblu'>
<%
						sDatiLingue = Replace(aCV(8),"|",",")
						sqlFormale = "SELECT CONOSCENZE.DENOMINAZIONE, PERS_CONOSC.COD_GRADO_CONOSC " &_ 
									" FROM CONOSCENZE, PERS_CONOSC " &_ 
									" WHERE CONOSCENZE.ID_CONOSCENZA = PERS_CONOSC.ID_CONOSCENZA " &_ 
									" AND PERS_CONOSC.ID_PERSONA = " & id_utente &_ 
									" AND PERS_CONOSC.IND_STATUS = 0 " &_ 
									" AND PERS_CONOSC.ID_CONOSCENZA IN (" & sDatiLingue & ") " &_
									" ORDER BY CONOSCENZE.DENOMINAZIONE" 
					'	Response.Write "<br>Lingue" & sqlFormale
 
SQLFORMALE = TransformPLSQLToTSQL (SQLFORMALE) 
						RRFormale.Open sqlFormale, CC

						if not RRFormale.eof then
							do while not RRFormale.eof
								Response.Write "<table>" &_
													"<tr>" &_
														"<td width=5><li>&nbsp;</li></td>" &_
														"<td class=textblu>" & Trasforma(RRFormale.Fields("DENOMINAZIONE")) & " (" & DecCodValDizDati("PERS_CONOSC","COD_GRADO_CONOSC_LENG",Cstr(RRFormale.Fields("COD_GRADO_CONOSC"))) & ")" &_
														"</td>" &_
													"</tr>" &_
												"</table>"																
								RRFormale.MoveNext
							loop
						end if
						RRFormale.Close
						sqlFormale = ""
					end if
%>
						</td>
					</tr>
<%
				end if
			end if

		' CONOSCENZE INFORMATICHE 
			if nSezioni > 5 then
				if aSezioni(6) = "1" and len(aCV(12)) > 0 then
					if IsArray(Split(aCV(12),"|")) and aCV(12) <> "0" then
					'	aDatiLingue = Split(aCV(12),"|")
%>					<tr>
						<td class='textblu' valign=top >CONOCIMIENTOS INFORM�TICOS</td>
						<td class='textblu'>
						
<%
						sConoscenze = Replace(aCV(12),"|",",")
						sqlFormale = "SELECT CONOSCENZE.DENOMINAZIONE, PERS_CONOSC.COD_GRADO_CONOSC " &_ 
									" FROM CONOSCENZE, PERS_CONOSC " &_ 
									" WHERE CONOSCENZE.ID_CONOSCENZA = PERS_CONOSC.ID_CONOSCENZA " &_ 
									" AND PERS_CONOSC.ID_PERSONA = " & id_utente &_
									" AND PERS_CONOSC.IND_STATUS = 0 " &_
									" AND PERS_CONOSC.ID_CONOSCENZA IN (" & sConoscenze & ")" &_
									" ORDER BY CONOSCENZE.DENOMINAZIONE"
						

SQLFORMALE = TransformPLSQLToTSQL (SQLFORMALE) 
						RRFormale.Open sqlFormale, CC
						
						if not RRFormale.eof then
							do while not RRFormale.eof
%>								<table>
									<tr>
										<td width=5><li>&nbsp;</li></td>
										<td class=textblu><%=Trasforma(RRFormale.Fields("DENOMINAZIONE")) & " (" & decGradoConosc(CInt(RRFormale.Fields("COD_GRADO_CONOSC"))) & ")"%></td>
									</tr>
								</table>
<%								RRFormale.MoveNext
							loop
						end if
						RRFormale.Close
						sqlFormale = ""
					end if
%>

						</td>
					</tr>
<%
				end if
			end if

		' ESPERIENZE PROFESSIONALI 
			if nSezioni > 2 then
				if aSezioni(3) = "1" and len(aCV(10)) > 0 then
					if IsArray(Split(aCV(10),"|")) and aCV(10) <> "0" then
						aPro = Split(aCV(10),"|")
						sIn = "("
						for cont = 0 to ubound(aPro)
							sMansione = Split(aPro(cont),"$",-1,1)(0)
							sIn = sIn & "'" &  sMansione & "',"
						next
						sIn = mid(sIn,1,len(sIn)-1)
						sIn = sIn & ")"
			
						sqlFormale = "SELECT ESPRO.RAG_SOC AS AZIENDA, ESPRO.DT_INI_ESP_PRO AS DI, nvl(ESPRO.DT_FIN_ESP_PRO,to_Date('31/12/9999','dd/mm/yyyy')) AS DF, ESPRO.COD_MANSIONE AS MAN " &_ 
									" FROM ESPRO " &_ 
									" WHERE ID_PERSONA = " & id_utente &_ 
									" AND ESPRO.COD_MANSIONE IN " & sIn  &_
									" ORDER BY DT_FIN_ESP_PRO desc, ESPRO.DT_INI_ESP_PRO desc "
 
SQLFORMALE = TransformPLSQLToTSQL (SQLFORMALE) 
						RRFormale.Open sqlFormale, CC
						if  not RRFormale.eof then
%>							<tr>
								<td class='textblu' valign=top >ANTECEDENTES LABORALES</p></td>
									<td class='textblu'>
									
<%
							do while not RRFormale.eof
%>								<table>
									<tr>
										<td width=5><li>&nbsp;</li></td>
										<td class=textblu> Del &nbsp; 
<%											Response.Write ConvDateToString(RRFormale.Fields("DI"))
											if RRFormale.Fields("DF") <> ConvStringToDate("31/12/9999") then
												Response.Write " al " & RRFormale.Fields("DF").value
											else
												Response.Write " a la fecha "
											end if
											Response.Write " - "
											if RRFormale.Fields("MAN").value <> "" then
											'''''''' SM inizio modifica per codifica della Mansione 
												'sMansi = "SELECT MANSIONE FROM CODICI_ISTAT WHERE CODICE='" & RRFormale.Fields("MAN").value & "'"			
												
												'02/01/2009 DAMIAN CAMBIA EL NOMBRE DEL CAMPO PARA QUE MUESTRE COHERENTEMENTE EL PUESTO				
												'sMansi = "SELECT MANSIONE FROM CODICI_ISTAT c, figureprofessionali f WHERE f.cod_ej=c.CODICE and f.id_figprof='" & RRFormale.Fields("MAN").value & "'"
												sMansi = "SELECT DENOMINAZIONE as MANSIONE FROM CODICI_ISTAT c, figureprofessionali f WHERE f.cod_ej=c.CODICE and f.id_figprof='" & RRFormale.Fields("MAN").value & "'"
											''''' SM fine	
 
SMANSI = TransformPLSQLToTSQL (SMANSI) 
												set rsMansi = cc.execute(sMansi)
												if not rsMansi.eof then		
													Response.Write "como " & lcase(TRIM(rsMansi("MANSIONE")))
												end if
												rsMansi.close
												set rsMansi=nothing								
											end if
											if RRFormale.Fields("AZIENDA").value <> "" and ucase(RRFormale.Fields("AZIENDA")) <> "NON DICHIARATA" then
												Response.Write " en la empresa " & RRFormale.Fields("AZIENDA")
											end if
%>										</td>
									</tr>
								</table>
<%								RRFormale.MoveNext
							loop
						end if
						RRFormale.Close
						sqlFormale = ""
						
%>						</td>
					</tr>
<%					end if
				end if
			end if
					
		' DISPONIBILITA' 
			if nSezioni > 6 then
				if aSezioni(7) = "1" and len(aCV(13)) > 0 then
					if IsArray(Split(aCV(13),"|")) and aCV(13) <> "0" then
				'		aDis = Split(aCV(13),"|")
						sDisponibilita = "'" & Replace(aCV(13),"|","','") & "'"
						sCondizione = "codice IN (" & sDisponibilita & ")"
						sTabella = "TDISP"
						dData	 = Date()
						nOrder   = 2
						Isa		 = 0
						aDisponibilita = decodTadesToArray(sTabella,dData,sCondizione,nOrder,Isa)
		
						nCodArea = 00
						if isArray(aDisponibilita) then
%>							<tr>
								<td class='textblu' valign=top >DISPONIBILIDADES/LIMITACIONES</p></td>
								<td class='textblu'>&nbsp;</td>
							</tr>	
<%							for i = 0 to ubound(aDisponibilita) -1	
								if clng(aDisponibilita(i,i+1,i+1)) <> clng(nCodArea) then		
									if i <> 0 then Response.Write "</td></tr>" end if
										Response.Write "<tr>" &_
														"<td class='textblu' valign=top >" &_ 
															Trasforma(DecCodVal("CL_VD", 0, Date(),aDisponibilita(i,i+1,i+1),0)) &_ 
														"</td>" &_
														"<td class='textblu'>"
										nCodArea = aDisponibilita(i,i+1,i+1)
								end if	
								Response.Write	"<table>" &_
												"	<tr>" &_
												"		<td width=5><li>&nbsp;</li></td>" &_
												"		<td class=textblu>" & Trasforma(aDisponibilita(i,i+1,i)) &_
												"		</td>" &_
												"	</tr>" &_
												"</table>"
							next
							Erase aDisponibilita
%>
								<br></td>
							</tr>
<%						end if
					end if
				end if
			end if

		'PATENTI'		
			if nSezioni > 7 then	
				if aSezioni(8) = "1" and len(aCV(14)) > 0 then
					if IsArray(Split(aCV(14),"|")) and aCV(14) <> "0" then
						aPat = Split(aCV(14),"|")
						sPatenti = "'" & Replace(aCV(14),"|","','") & "'"
%>					<tr>
						<td class='textblu' valign=top >LICENCIAS DE CONDUCCI�N</p></td>
						<td class='textblu'>	
	
<%							sqlFormale = "SELECT COD_PERM, DT_TMST " & _
										" FROM AUTCERT " & _
										" WHERE ID_PERSONA = " & id_utente & _
										" AND COD_PERM IN (" & sPatenti & ")" &_
										" AND nvl(DT_SCADENZA, to_date('31/12/9999', 'dd/mm/yyyy')) >= " &  convDateToDB(date)
							
 
SQLFORMALE = TransformPLSQLToTSQL (SQLFORMALE) 
							RRFormale.Open sqlFormale, CC

							if  not RRFormale.eof then
								do while not RRFormale.eof							
									Response.Write "<table>" &_
													"	<tr>" &_
													"		<td width=5><li>&nbsp;</li></td>" &_
													"		<td class=textblu>" & Trasforma(DecCodVal("TPERM","0",RRFormale.Fields("DT_TMST"),RRFormale.Fields("COD_PERM"),0)) &_
													"		</td>" &_
													"	</tr>" &_
													"</table>"
									RRFormale.MoveNext
								loop
							end if
							RRFormale.Close
							sqlFormale = ""
%>	
							</td>
						</tr>
<%		
					end if
				end if
			end if	
		
		'FORMAZIONE'
			if nSezioni > 8 then	
				if aSezioni(9) = "1" and len(aCV(15)) > 0 then
					if IsArray(Split(aCV(15),"|")) and aCV(15) <> "0" then
						aFormaz = Split(aCV(15),"|")
						sFormazione = "'" & Replace(aCV(15),"|","','") & "'"
%>					<tr>
						<td class='textblu' valign=top >CAPACITACI�N Y FORMACI�N PROFESIONAL</p></td>
							<td class='textblu'>
							
<%							sqlFormale = "SELECT F.ID_FORMAZIONE, F.COD_TCORSO, F.AA_CORSO, F.COD_STATUS_CORSO, A.DENOMINAZIONE " & _
										" FROM FORMAZIONE F, AREA_CORSO A " & _
										" WHERE A.ID_AREA = F.ID_AREA " & _
										" AND ID_PERSONA = " & id_utente &_
										" AND ID_FORMAZIONE IN (" & sFormazione & ")" &_
										" ORDER BY F.AA_CORSO DESC"

 
SQLFORMALE = TransformPLSQLToTSQL (SQLFORMALE) 
							RRFormale.Open sqlFormale, CC

							if  not RRFormale.eof then
								do while not RRFormale.eof							
									Response.Write "<table>" &_
													"	<tr>" &_
													"		<td width=5><li>&nbsp;</li></td>" &_
													"		<td class=textblu valign=middle>" &  RRFormale.Fields("AA_CORSO") & " - "  & RRFormale.Fields("DENOMINAZIONE") &_
																				"- " & LCASE(RRFormale.Fields("COD_TCORSO")) &_
																				" ("
													select case RRFormale.Fields("COD_STATUS_CORSO")
														case "0"
															Response.Write "Complet�"
														case "1"
															Response.Write "Abandon�"
														case "2"
															Response.Write "En Curso"
														case "3"
															Response.Write "Incompleto"
														case "4"
															Response.Write "No corresponde"						
														case else
													end select
															Response.Write ")" &_ 
													"		</td>" &_
													"	</tr>" &_
													"</table>"
									RRFormale.MoveNext
								loop
							end if
							RRFormale.Close
							sqlFormale = ""
%>						
							</td>
						</tr>
<%		
					end if
				end if
			end if
			
		' ALTRE INFO '
			if nSezioni > 4 then
				if aSezioni(5) = "1" then
					if IsArray(Split(aCV(11),"|")) and len(aCV(11)) > 0 then
%>					<tr>
						<td class='textblu' valign=top >INFORMACION ADICIONAL</td>
						<td class='textblu'>
	
<%
						aInfo = Split(aCV(11),"|")
						for cont = 0 to UBound(aInfo)
							Response.Write	"<table>" &_
											"	<tr>" &_
											"		<td width=5><li>&nbsp;</li></td>" &_
											"		<td class=textblu>" & Server.HTMLEncode(aInfo(cont)) &_
											"		</td>" &_
											"	</tr>" &_
											"</table>"	
						next
%>	
						</td>
					</tr>
<%
					end if
				end if
			end if
			
		' OBIETTIVI
			if nSezioni > 3 then
				if aSezioni(4) = "1" and len(aCV(7)) > 0 then
%>					<tr>
						<td class='textblu' valign=top >OBJETIVOS</td>
						<td class='textblu'>
							<table>
								<tr>
									<td class=textblu  width=20>&nbsp;</td>
									<td class=textblu><%=Server.HTMLEncode(aCV(7))%></td>
								</tr>
							</table>
						</td>
					</tr>
<%
				end if
			end if


		' PRIVACY 
%>
					<tr>
						<td class='textblu' colspan=2>
							&nbsp;
						</td>
					</tr>
					<tr>
						<td class='textblu' colspan=2>
							<B>
  
<%							'Server.execute ("/PLAVORO/TESTI/SISTDOC/SISTEMA REDAZIONALE/AUTORIZZAZIONI/Info_Privacy.htm")

							'Server.execute ("/TESTI/AUTORIZZAZIONI/Info_Privacy.htm")
%>							<B>
						</td>
					</tr>
			</table>
		</td>
	</tr>

<%	'Rilascio il recordset
	set RRFormale = nothing
	if Request("mode").Item <> "stampa" then 
%>
	<!-- #include file="CUR_VisBottoni.asp" -->
<%	end if 
%>
