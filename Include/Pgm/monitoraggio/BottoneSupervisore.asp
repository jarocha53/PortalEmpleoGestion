<%@ LANGUAGE=VBSCRIPT %>
<% response.expires = -1500 %>
<!-- #include file="adovbs.inc" -->
<%
	'''
	'' --------------->   CONTROLLO AUTENTICAZIONE UTENTE
	'
	if (Session("ente") = null or Session("ente") = "") then 
		Response.redirect("Login.html")
		return
	end if
%>

<html>
<head>
<style>
  a.gen {
    font-family: tahoma;
    font-size: 12;
	font-weight: bold;
	color: white;
	text-align: center;
	border: 1 solid black;
	background-color: #006796;
	text-decoration: none;
	padding: 2;
	margin: 0;
	vertical-align: middle;
	width: 200;
  }
</style>
</head>
<body>
	<table width="100%">
		<tr>
			<td align="center">
				<%
					mask = request.querystring("mask")
					guarda = request.queryString("guarda")
					if(isNull(mask) or isEmpty(mask) or isNull(guarda) or isEmpty(guarda) or mask = "" or guarda = "") then
						Server.transfer("Blank.asp")
					end if
				%>
					<a class="gen" href="javascript:parent.history.back()">Livello Superiore</a>
			</td>
		</tr>
	</table>
</body>
</html>