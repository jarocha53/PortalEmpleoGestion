<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->

<% response.expires = -1500 %>


<HTML>
	<HEAD>
		<TITLE>Inserimento</TITLE>
		<style type="text/css">
		<!--
			 td, input, select {
			 		font-family: tahoma;
					font-size: 12;
					font-weight: bold;
			 }
		-->
		</style>
		
	</HEAD>
	<BODY>
		<script>
		function mostraCalendario() {
		    finestra = window.open("Calendar.html", "Calendario", "width=250,height=210,noresize,nomaximize");
		}
		</script>
		<form method="post" action="SegnalazioneInserimento.asp">
			<input type="hidden" name="ancora" value="no">
			
			<table align="center" border="0" style="border: 1 solid; background-color: #6789af" cellspacing="0">
				<tr>
					<td>
						<table border="0" width="100%" style="border: 1 solid; background-color: white">
							<tr>
								<td style="; background-color: #cccccc; padding: 4">
									Dati Anagrafici
								</td>
							</tr>
							<tr>
								<td>
									<table width="100%" border="0">
										<tr>
											<td>
												Cognome
											</td>
											<td  colspan="2">
												<input type="text" name="cognome">
											</td>
											<td>
												Nome
											</td>
											<td colspan="2">
												<input type="text" name="nome">
											</td>
											<td>
												Codice Fiscale
											</td>
											<td>
												<input type="text" name="cod_fiscale" maxlength="16">
											</td>
										</tr>
										<tr>
											<td>
												Data di nascita
											</td>
											<td colspan="2">
												<input type="text" name="data" maxlength="10" style="width: 100">
												<a href="javascript:mostraCalendario()">...</a>
											</td>
											<td colspan="2">
												Comune di nascita
											</td>
											<td>
												<input type="text" name="comune">
											</td>
											<td>
												Provincia
											</td>
											<td>
												<input type="text" name="provincia">
											</td>
										</tr>
										<tr>
											<td>
												Stato di nascita
											</td>
											<td>
												<input type="text" name="stato" style="width: 100">
											</td>
											<td>
												Sesso
											</td>
											<td>
												<select name="sex">
													<option name="M" value="M">M</option>
													<option name="F" value="F">F</option>
												</select>
											</td>
											<td>
												Stato civile
											</td>
											<td>
												<select name="stato_civile">
													<option>Celibe</option>
													<option>Nubile</option>
													<option>Coniugato/a</option>
												</select>
											</td>
											<td>
												Pos. militare
											</td>
											<td>
												<input type="text" name="pos_militare">
											</td>
										</tr>
									</table>
								</td>  <!-- fine dati anagrafici -->
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td>
						<table border="0" width="100%" style="border: 1 solid; background-color: white">  <!-- Residenza -->
							<tr>
								<td style="; background-color: #cccccc; padding: 4">
									Residenza
								</td>
							</tr>
							<tr>
								<td>
									<table width="100%">
										<tr>
											<td>
												Indirizzo
											</td>
											<td>
												<input type="text" name="indirizzo_res">
											</td>
											<td>
												Frazione
											</td>
											<td>
												<input type="text" name="frazione_res">
											</td>
											<td>
												Comune
											</td>
											<td>
												<input type="text" name="comune_res">
											</td>
										</tr>
										<tr>
											<td>
												Provincia
											</td>
											<td>
												<input type="text" name="provincia_res">
											</td>
											<td>
												C.A.P.
											</td>
											<td>
												<input type="text" name="cap_res" maxlength="5">
											</td>
											<td>
												Numero di telefono
											</td>
											<td>
												<input type="text" name="telefono_res">
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>    <!-- fine Residenza -->
					</td>
				</tr>
				<tr>
					<td>
						<table border="0" width="100%" style="border: 1 solid; background-color: white">  <!-- Domicilio -->
							<tr>
								<td style="; background-color: #cccccc; padding: 4">
									Domicilio
								</td>
							</tr>
							<tr>
								<td>
									<table width="100%">
										<tr>
											<td>
												Indirizzo
											</td>
											<td>
												<input type="text" name="indirizzo_dom">
											</td>
											<td>
												Frazione
											</td>
											<td>
												<input type="text" name="frazione_dom">
											</td>
											<td>
												Comune
											</td>
											<td>
												<input type="text" name="comune_dom">
											</td>
										</tr>
										<tr>
											<td>
												Provincia
											</td>
											<td>
												<input type="text" name="provincia_dom">
											</td>
											<td>
												C.A.P.
											</td>
											<td>
												<input type="text" name="cap_dom" maxlength="5">
											</td>
											<td>
												Numero di telefono
											</td>
											<td>
												<input type="text" name="telefono_dom">
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>  <!-- fine Domicilio -->
					</td>
				</tr>
			</table>
		</form>
	</BODY>
</HTML>
