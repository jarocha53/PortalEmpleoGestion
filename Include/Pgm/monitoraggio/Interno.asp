<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%@ LANGUAGE=VBSCRIPT %>

<%
	'''
	'' --------------->   CONTROLLO AUTENTICAZIONE UTENTE
	'
	if (Session("ente") = null or Session("ente") = "") then 
		Response.redirect("Login.html")
		return
	end if
%>
<%
   modo = Request.querystring("mode")
   Dim linkalo
   linkalo = request.queryString("linkalo")
   if(isNull(linkalo) or isEmpty(linkalo) or linkalo = "") then
   		if(session("livelloUtente") < 5 and session("livelloUtente") > 2) then
			' L'UTENTE E' UNA REGIONE, PROVINCIA quindi gli
			' linko il titoletto
			linkalo = "si"
		end if
   end if
%>
<html>
<head>
	   <%
	     if ((isNull(modo) or isEmpty(modo) or modo = "internal") and session("tipoUtente") <> "supervisore") then
		 ' SIAMO NEL CASO OPERATORE DI UN ENTE
	   %>
			   <frameset rows="40,44,18,*,23" border="0">
			      <frame name="titoloInterno" src="TitoloInterno.asp" frameborder="0" marginwidth="0" marginheight="0" noresize scrolling="no">
				  <frame name="frameLettere" src="Lettere.asp" frameborder="0" marginwidth="0" marginheight="0" noresize scrolling="no">
			      <frame name="headerInterno" src="HeaderTabella.asp" frameborder="0" marginwidth="0" marginheight="0" noresize scrolling="no">
			      <frame name="mainInterno" src="Visualizzazione.asp" frameborder="0" marginwidth="0" marginheight="0" noresize scrolling="yes">
			      <frame name="barraBottoni" src="Bottoni.asp" frameborder="0" marginwidth="0" marginheight="0" noresize scrolling="no">
			   </frameset>
	   <%
	     elseif (modo = "external" or session("tipoUtente") = "supervisore") then
		 	mask = Request.queryString("mask")
			guarda = request.queryString("guarda")
			select case mask
				case "D"
					if(session("livelloUtente") < 5) then
						' ERRORE NON AUTORIZZATO !
						
					end if
				case "C"
					if(session("livelloUtente") < 4) then
						' ERRORE NON AUTORIZZATO !
						
					end if
				case "B"
					if(session("livelloUtente") < 3) then
						' ERRORE NON AUTORIZZATO !
						
					end if
				case "A"
					if(session("livelloUtente") > 4) then
						' ERRORE NON AUTORIZZATO !
						
					end if
				case default
			end select
		%>
			<% if(mask = "A") then %>
			<frameset rows="40,44,18,*,24" border="0">
			<% else %>
			<frameset rows="40,18,*,24" border="0">
			<% end if %>
			<frame name="titoloInterno" src="TitoloInterno.asp?guarda=<%=guarda%>&linkalo=<%=linkalo%>&mask=<%=mask%>" frameborder="0" marginwidth="0" marginheight="0" noresize scrolling="no">
			<% if(mask = "A") then %>
			<frame name="frameLettere" src="Lettere.asp" frameborder="0" marginwidth="0" marginheight="0" noresize scrolling="no">
			<% else %>
			<% end if %>
			<frame name="headerInterno" src="HeaderTabella<%=mask%>.asp" frameborder="0" marginwidth="0" marginheight="0" noresize scrolling="no">
			<frame name="mainInterno" src="Maschera<%=mask%>.asp?guarda=<%=guarda%>" frameborder="0" marginwidth="0" marginheight="0" noresize scrolling="yes">
			<frame name="bottonesup" src="BottoneSupervisore.asp?mask=<%=session("prevmask")%>&guarda=<%=guarda%>" frameborder="0" marginwidth="0" marginheight="0" noresize scrolling="yes">
			</frameset>
	   <%
	   		session("prevguarda") = guarda
			session("prevmask") = mask
	 	 else
	   %>
			<!--
				L'operatore non deve vedere gli esterni
			-->
	   <%
	     end if
	   %>
</head>
<body>
</body>
</html>
