<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%@ LANGUAGE=VBSCRIPT %>

<% response.expires = -1500 %>



<%
	'''
	'' --------------->   CONTROLLO AUTENTICAZIONE UTENTE
	'
	if (Session("ente") = null or Session("ente") = "") then 
		Response.redirect("Login.html")
		return
	end if
%>


<html>
	<style>
		td.head {
		    font-family: tahoma;
		    font-size: 10;
			font-weight: bold;
			color: white;
			text-align: left;
			background-color: #006796;
			text-decoration: none;
			vertical-align: middle;
			margin: 0;
			padding: 0;
		}
		a {
		    font-family: tahoma;
		    font-size: 12;
			font-weight: bold;
			color: white;
			text-align: center;
			border: 1 solid black;
			background-color: #006796;
			text-decoration: none;
			padding: 2;
			margin: 0;
			vertical-align: middle;
			width: 80;
		 }
		 td.titolo {
		    font-family: tahoma;
		    font-size: 14;
			font-weight: bolder;
			color: #6789cc;
			text-align: left;
	  	}
	</style>
<% 
	Dim datiSospesi
	Dim datiUsciti
	Dim datiEsclusi
	
	Set connection = Server.createObject("ADODB.Connection")
		connection.open(session("connectString"))
	Set res = Server.createObject("ADODB.Recordset")
	
	query  = "SELECT ID_PERSONA FROM PERS_ENTE WHERE CERTIFICATO = 'N' AND ID_IMPRESA = " & session("ente")
'PL-SQL * T-SQL  
QUERY = TransformPLSQLToTSQL (QUERY) 
	Set res = connection.execute(query)
	if (res.EOF) then
%>
		<table width="100%" eight="100%">
			<tr>
				<td>&nbsp;</td>
			</tr
			<tr>
				<td align="center" valign="middle">
					<center><font family="tahoma" size="10" color="#6789cc"><strong>
						Nessuna operazione da segnalare per il mese corrente
					</center></font></strong>
				</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
			</tr
			<tr>
				<td align="center" valing="middle"><a href="javascript:window.close()">Ok</a></td>
			</tr
		</table>
<%
		res.close()
		Set res = nothing
		connection.close()
		Set connection = nothing
		response.end()
	end if
	
	' NOTA: in TMP_PERS_SOSP pu� esserci per ogni periodo di certificazione un solo record per un dato LSU;
	'       perch� si assume che un LSU possa essere sospeso una sola volta in un periodo di certificazione
	queryS = "SELECT DISTINCT P.ID_PERSONA, P.NOME, P.COGNOME, P.COD_FISC, " & _		
			 "PS.DT_INIZIO_SOSP, PS.DT_FINE_SOSP, PS.FL_MATERNITA " & _
			 "FROM   PERSONA P, PERS_ENTE PE, TMP_PERS_SOSP PS " & _
			 "WHERE  P.ID_PERSONA = PE.ID_PERSONA AND P.ID_PERSONA = PS.ID_PERSONA AND " & _
			 "       PE.FL_STATO_PERSONA = 'S' AND PE.CERTIFICATO = 'N' AND PE.ID_IMPRESA = " & session("ente") & " " & _ 
			 "ORDER BY COGNOME, NOME"
			 
	queryR = "SELECT DISTINCT P.ID_PERSONA, P.NOME, P.COGNOME, P.COD_FISC, " & _		
			 "PS.DT_INIZIO_SOSP, PS.DT_FINE_SOSP, PS.FL_MATERNITA " & _
			 "FROM   PERSONA P, PERS_ENTE PE, TMP_PERS_SOSP PS " & _
			 "WHERE  P.ID_PERSONA = PE.ID_PERSONA AND P.ID_PERSONA = PS.ID_PERSONA AND " & _
			 "       PE.FL_STATO_PERSONA = 'A' AND PE.CERTIFICATO = 'N' AND PE.ID_IMPRESA = " & session("ente") & " " & _ 
			 "ORDER BY COGNOME, NOME"
			 
	queryU = "SELECT DISTINCT P.ID_PERSONA, P.NOME, P.COGNOME, P.COD_FISC, " & _		
			 "TU.DT_USCITA, TU.CAUSALE_USCITA " & _
			 "FROM   PERSONA P, PERS_ENTE PE, TMP_USCITA TU " & _
			 "WHERE  P.ID_PERSONA = PE.ID_PERSONA AND PE.ID_PERSONA = TU.ID_PERSONA AND PE.FL_STATO_PERSONA = 'U' AND PE.CERTIFICATO = 'N' AND PE.ID_IMPRESA = " & session("ente") & " " & _ 
			 "ORDER BY COGNOME, NOME"
		
	queryE = "SELECT DISTINCT P.ID_PERSONA, P.NOME, P.COGNOME, P.COD_FISC " & _		
			 "FROM   PERSONA P, PERS_ENTE PE " & _
			 "WHERE  P.ID_PERSONA = PE.ID_PERSONA AND PE.FL_STATO_PERSONA = 'E' AND " & _
			 "       PE.CERTIFICATO = 'N' AND PE.ID_IMPRESA = " & session("ente") & " " & _ 
			 "ORDER BY COGNOME, NOME"
%>

	<head>
		<style type="text/css">
		<!--
			 td {
			 		font-family: tahoma;
					font-size: 12;
					font-weight: bold;
			 }
			 
			 td.cfis {
			 		font-family: arial;
					font-size: 12;
					font-weight: bold;
			 }
			 
			 a.miniBottone {
			 		font-family: tahoma;
					font-size: 8;
					font-weight: bold;
					text-decoration: none;
					vertical-align: middle;
					border: 1 solid black;
					color: black;
			 }
		-->
		</style>
	</head>
	<body>
		<script language="javascript">
		
		    function conferma() {

				window.opener.parent.main.frameTabella.mainInterno.location = "SalvataggioCertificazione.asp"

				window.opener.parent.main.frameTabella.headerInterno.location = "HeaderTabella.asp"
				window.opener.parent.main.frameTabella.barraBottoni.location = "Bottoni.asp"
				window.opener.top.titolo.location = "Titolo.asp"
				window.close()
		    }
			 
			function annulla()  {
			 	window.close()
			}
			
		</script>
			
		<table>
			<tr>
				<td colspan="2" align="center">Attenzione: con questa operazione si certificheranno i seguenti dati</td>
			</tr>
			<tr>
				<td class="titolo" colspan="2">LSU sospesi</td>
			</tr>
			<tr>
				<td colspan="2">
					<% 
'PL-SQL * T-SQL  
QUERYS = TransformPLSQLToTSQL (QUERYS) 
						Set res = connection.execute(queryS)
					%>
					<table border="0" cellpadding="1" style="border-left: 1 solid black; border-right: 1 solid black" width="100%" cellspacing="0">
						<tr>
							<td class="head" align="left" width="200">Cognome</td>
							<td class="head" align="left" width="200">Nome</td>
							<td class="head" align="left" width="150" class="a">Codice Fiscale</td>
							<td class="head" align="left" width="150">Inizio sospensione</td>
							<td class="head" align="left" width="150">Fine sospensione</td>
							<td class="head" align="left" width="150">Maternita'</td>
						</tr>
					<%
						Do while NOT res.eof
							nome    = res("nome")
							cognome = res("cognome")
							codfisc = res("cod_fisc")
							inizio  = res("dt_inizio_sosp")
							fine    = res("dt_fine_sosp")
							mater   = res("fl_maternita")
						%>

							<tr>
								<td width="180" align="left"><%=cognome%></td>
								<td width="180" align="left"><%=nome%></td>
								<td width="150" class="cfis" align="left"><%=Ucase(codfisc)%></td>
								<td width="150" align="left"><%=inizio%></td>
								<td width="150" align="left"><%=fine%></td>
								<% if (mater = "1") then %>
									<td width="150" align="center"><input type="checkbox" checked="true" disabled="true"></td>
								<% else %>
									<td width="150" align="center">&nbsp;</td>							
								<% end if %>
							</tr>
							<tr>
							   <td colspan="8" height="1" bgcolor="black"></td>
							</tr>

						<%
						res.Movenext
						Loop
						res.close()
					%>
					</table>
				</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td class="titolo" colspan="2">LSU riattivati</td>
			</tr>
			<tr>
				<td colspan="2">
					<% 
'PL-SQL * T-SQL  
QUERYR = TransformPLSQLToTSQL (QUERYR) 
						Set res = connection.execute(queryR)
					%>
					<table border="0" cellpadding="1" style="border-left: 1 solid black; border-right: 1 solid black" width="100%" cellspacing="0">
						<tr>
							<td class="head" align="left" width="200">Cognome</td>
							<td class="head" align="left" width="200">Nome</td>
							<td class="head" align="left" width="150" class="a">Codice Fiscale</td>
							<td class="head" align="left" width="150">Inizio sospensione</td>
							<td class="head" align="left" width="150">Fine sospensione</td>
							<td class="head" align="left" width="150">Maternita'</td>
						</tr>
					<%
						Do while NOT res.eof
							nome    = res("nome")
							cognome = res("cognome")
							codfisc = res("cod_fisc")
							inizio  = res("dt_inizio_sosp")
							fine    = res("dt_fine_sosp")
							mater   = res("fl_maternita")
						%>

							<tr>
								<td width="180" align="left"><%=cognome%></td>
								<td width="180" align="left"><%=nome%></td>
								<td width="150" class="cfis" align="left"><%=Ucase(codfisc)%></td>
								<td width="150" align="left"><%=inizio%></td>
								<td width="150" align="left"><%=fine%></td>
								<% if (mater = "1") then %>
									<td width="150" align="center"><input type="checkbox" checked="true" disabled="true"></td>
								<% else %>
									<td width="150" align="center">&nbsp;</td>							
								<% end if %>
							</tr>
							<tr>
							   <td colspan="8" height="1" bgcolor="black"></td>
							</tr>

						<%
						res.Movenext
						Loop
						res.close()
					%>
					</table>
				</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td class="titolo" colspan="2">LSU in uscita</td>
			</tr>
			<tr>
				<td colspan="2">
					<%
'PL-SQL * T-SQL  
QUERYU = TransformPLSQLToTSQL (QUERYU) 
						Set res = connection.execute(queryU)
					%>
					<table border="0" cellpadding="1" style="border-left: 1 solid black; border-right: 1 solid black" width="100%" cellspacing="0">
						<tr>
							<td class="head" align="left" width="200">Cognome</td>
							<td class="head"  align="left" width="200">Nome</td>
							<td class="head" align="left" width="150" class="a">Codice Fiscale</td>
							<td class="head" align="left" width="150">Data uscita</td>
							<td class="head" align="left" width="150">Causale uscita</td>
						</tr>
					<%
						Do while NOT res.eof
							nome    = res("nome")
							cognome = res("cognome")
							codfisc = res("cod_fisc")
							data    = res("dt_uscita")
							causale = res("causale_uscita")
						%>

							<tr>
								<td width="180" align="left"><%=cognome%></td>
								<td width="180" align="left"><%=nome%></td>
								<td width="150" class="cfis" align="left"><%=Ucase(codfisc)%></td>
								<td width="150" align="left"><%=data%></td>
								<td width="150" align="left"><%=causale%></td>
							</tr>
							<tr>
							   <td colspan="8" height="1" bgcolor="black"></td>
							</tr>

						<%
						res.Movenext
						Loop
						res.close()
					%>

					</table>
				</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td class="titolo" colspan="2">LSU sconosciuti</td>
			</tr>
			<tr>
				<td colspan="2">
					<%
'PL-SQL * T-SQL  
QUERYE = TransformPLSQLToTSQL (QUERYE) 
						Set res = connection.execute(queryE)
					%>
					<table border="0" cellpadding="1" style="border-left: 1 solid black; border-right: 1 solid black" width="100%" cellspacing="0">
						<tr>
							<td class="head" align="left" width="200">Cognome</td>
							<td class="head"  align="left" width="200">Nome</td>
							<td class="head" align="left" width="150" class="a">Codice Fiscale</td>
						</tr>
					<%
						Do while NOT res.eof
							nome    = res("nome")
							cognome = res("cognome")
							codfisc = res("cod_fisc")
						%>

							<tr>
								<td width="180" align="left"><%=cognome%></td>
								<td width="180" align="left"><%=nome%></td>
								<td width="150" class="cfis" align="left"><%=Ucase(codfisc)%></td>
							</tr>
							<tr>
							   <td colspan="8" height="1" bgcolor="black"></td>
							</tr>

						<%
						res.Movenext
						Loop
						res.close()
					%>
					<%
						Set res = nothing
						connection.close()	
						Set connection = nothing	
					%>
					</table>
				</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td align="center">
					<a href="javascript:conferma()">Conferma</a>
				</td>
				<td align="center">
					<a href="javascript:annulla()">Indietro</a>
				</td>
			</tr>
		</table>
	</body>
</html>
