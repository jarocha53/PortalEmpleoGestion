<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%@ LANGUAGE=VBSCRIPT %>

<!-- #INCLUDE FILE="adovbs.inc" -->

<%
	'''
	'' --------------->   CONTROLLO AUTENTICAZIONE UTENTE
	'
	if (Session("ente") = null or Session("ente") = "") then 
		Response.redirect("Login.html")
		return
	end if
%>
<%	

   	Set connection = Server.createObject("ADODB.Connection")
		connection.open(session("connectString"))
	   
		codici    = split(request.Form("codice"), ",")
		lista     = split(request.Form("lista"), ",")
		
		nomi      = split(request.Form("nome"), ",")
		cognomi   = split(request.Form("cognome"), ",")
		cdfiscali = split(request.Form("codfisc"), ",")
		
		'Aggiornamento di RICH_VAR_ANAG
		queryRVA  = "SELECT ID_PERSONA, ID_IMPRESA, ID_SEDE, TIPO_RICHIESTA, DT_RICHIESTA, " & _
				    "COGNOME, NOME, COD_FISCALE " & _
				    "FROM RICH_VAR_ANAG "
		Set res2 = Server.createObject("ADODB.Recordset")
		Set res3 = Server.createObject("ADODB.Recordset")
		
		Set res = Server.createObject("ADODB.Recordset")
			res.open queryRVA, connection, adOpenDynamic, adLockOptimistic
			
			 for k = Lbound(codici)+1 to Ubound(codici)
				if (lista(k) > 0) then
					
					res.addNew()
						res("id_persona") = codici(k)
						res("id_impresa") = session("ente")
						res("id_sede")    = session("ente")
						res("tipo_richiesta") = "C"
						res("dt_richiesta")   = Date()
						res("cognome")    = cognomi(k)
						res("nome")       = nomi(k)
						res("cod_fiscale")   = cdfiscali(k)
					res.update()
					
					queryPE = "SELECT FL_STATO_PERSONA, CERTIFICATO FROM PERS_ENTE WHERE ID_PERSONA = " & codici(k)
					
					res2.open queryPE, connection, 3, 3
					if not res2.EOF then
						res2("fl_stato_persona") = "E"
						res2("certificato") = "N"
						res2.update()
					end if
					res2.close()
					
					'inserimento del record in AREA_LAVORO
					progressivo = 0
					queryPE = "SELECT MAX(PROGRESSIVO) PROGRESSIVO FROM AREA_LAVORO WHERE ID_PERSONA = " & codici(k)
					res3.open queryPE, connection
					if (not res3.EOF) then
						if (not isNull(res3("progressivo"))) then
							progressivo = res3("progressivo") + 1
						else
							progressivo = 1
						end if
					end if
					res3.close()
					
					queryPE = "SELECT * FROM AREA_LAVORO"
					res2.open queryPE, connection, 3, 3
					res2.addNew()
						res2("id_persona") = codici(k)
						res2("id_impresa") = session("ente")
						res2("id_sede") = session("ente")
						res2("progressivo") = progressivo
						res2("tipo_operazione") = "E"
					res2.update()
					res2.close()
				end if
			next
			res.close()

		Set res = nothing
		Set res2 = nothing
		
		connection.close()
		Set connection = nothing

	Server.transfer("Visualizzazione.asp")
%>
