<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%@ LANGUAGE=VBSCRIPT %>

<!-- #include file="adovbs.inc" -->
<!-- #include file="db/gestionelsu.asp" -->


<%	
	Set connection = Server.createObject("ADODB.Connection")
		connection.open(session("connectString"))
		
%>

<%
	'''
	'' --------------->   CONTROLLO AUTENTICAZIONE UTENTE
	'
	if (Session("ente") = null or Session("ente") = "") then 
		Response.redirect("Login.html")
		return
	end if
%>
<%
	Dim codice
	codice = request.queryString("codice")
	
	query = "SELECT P.ID_PERSONA, P.NOME, P.COGNOME, P.COD_FISC, PE.DT_USCITA, PE.CAUSALE_USCITA " & _
	        "FROM TMP_USCITA PE, PERSONA P " & _
			"WHERE P.ID_PERSONA  = " & codice & " " & _
			"  AND PE.ID_PERSONA = P.ID_PERSONA AND PE.PROGRESSIVO = (SELECT MAX(PROGRESSIVO) FROM TMP_USCITA WHERE ID_PERSONA = " & codice & ")"
	
	'puliamo la sessione....
	rimuoviAZ()
	rimuoviLettere()

	Set res = Server.createObject("ADODB.Recordset")
'PL-SQL * T-SQL  
QUERY = TransformPLSQLToTSQL (QUERY) 
	Set res = connection.execute(query)
	
%>
<html>
<style type="text/css">
<!--
			 td {
			 		font-family: tahoma;
					font-size: 10;
					font-weight: bold;
			 }
			 
			 td.cfis {
			 		font-family: tahoma;
					font-size: 10;
			 }
			 
			 select {
			        border: 1 solid black;
					width: 120;
					font-family: tahoma;
					font-size: 10;
					font-weight: bold;
					padding: 2;
					background: white;
					margin: 0;
			 }
			 
			 input {
					width: 120;
					font-family: tahoma;
					font-size: 10;
					font-weight: bold;
					padding: 2;
					background: white;
					margin: 0;
			 }
			 
			 a.gen {
				    font-family: tahoma;
				    font-size: 12;
					font-weight: bold;
					color: white;
					text-align: center;
					border: 1 solid black;
					background-color: #006796;
					text-decoration: none;
					padding: 2;
					margin: 0;
					vertical-align: middle;
					width: 80;
			 }
			 
			 a.miniBottone {
			 			   font-family: tahoma;
						   font-size: 8;
						   font-weight: bold;
						   text-decoration: none;
						   vertical-align: middle;
						   border: 1 solid black;
						   color: black;
			 }
-->
</style>
	<head>
	</head>
	<script src="utils/utility.js"></script>
	<script>
		
		function mostraCalendario(campo) {
		    finestra = window.open("Calendar.html", "Calendario", "width=250,height=210,noresize,nomaximize");
		}
		
		function confermaUscita() {
			data   = document.forms[0].data
			causale= document.forms[0].causale
			if ( isEmpty(data.value)) {
				alert("Specificare la data di uscita")
				return
			} else if (isEmpty(causale.value)) {
				alert("Specificare la causale di uscita")
				return
			} 
			d = data.value
			if (!isEmpty(d)) {
				igiorno = d.indexOf("/")
				imese   = d.indexOf("/", igiorno+1)
				
				giorno  = parseInt(d.substring(0, igiorno))
				mese    = parseInt(d.substring(igiorno+1, imese))
				anno    = parseInt(d.substring(imese+1, d.length))
				
				if (isNaN(giorno) || isNaN(mese) || isNaN(anno)) {
					alert("Data non valida")
					return
					data.select()
					data.focus()
				}
				today = new Date()
				// controlliamo se la data di uscita si riferisce ad un periodo di certificazione
				// precendente a quello corrente, cio� se il mese della data di uscita � precedente 
				// allo scorso mese o se � proprio uguale allo scorso mese ma il giorno di uscita �
				// precedente all'11 di tale mese
				if (anno < today.getYear() || mese < (today.getMonth() + 1) || (mese == (today.getMonth() +1) && giorno < 11)) {
					if (!confirm("Attenzione: la data di uscita impostata si riferisce ad un periodo di certificazione precedente a quello corrente. Procedere ugualmente?")) {
						return
					}
				}
			}
			parent.titoloInterno.location = "TitoloInterno.asp"
			parent.frameLettere.location = "Lettere.asp"
			parent.headerInterno.location = "HeaderTabella.asp"
	  		parent.barraBottoni.location = "Bottoni.asp"
			document.forms[0].dataUscita.value = data.value
			document.forms[0].action = "SalvataggioUscita.asp"
			document.forms[0].submit()
		}
		
		function annullaRegistrazione()	{
			parent.headerInterno.location = "HeaderTabella.asp"
			parent.titoloInterno.location = "TitoloInterno.asp"
			parent.frameLettere.location = "Lettere.asp"
	  		parent.barraBottoni.location = "Bottoni.asp"
	  		document.forms[0].action = "CancellazioneUscita.asp"
	  		document.forms[0].submit()
		}
		
	</script>
	<body bgcolor="white" leftmargin="0" rightmargin="0" topmargin="0">
	    <script src="utils/utility.js"></script>
		<form method="post" action="">
		
		<%
		'il campo dopoOperazione viene messo a 'no' solo dalle funzioni
		'javascript per la selezione dei filtri. Se vale 'si' la sessione
		'viene ripulita al caricamento di questa pagina. Questo evita il
		'problema della propagazione dei dati della form.
		%>
		<input type="hidden" name="dopoOperazione" value="si"/>
		
		
			<!-- per fare in modo che sia sempre un array -->
			<input type="hidden" name="codice" value="-1">
			<input type="hidden" name="lista" value="0">
			
		<table border="0" cellpadding="2" style="border-left: 1 solid black; border-right: 1 solid black" width="100%" cellspacing="0" cellpadding="0">
		<%
			if (not res.EOF) then
				codice = res("id_persona")
				nome = res("nome")
				cognome = res("cognome")
				cdfiscale = res("cod_fisc")
				datauscita = res("dt_uscita")
				causale = res("causale_uscita")
		%>	
			<input type="hidden" name="dataUscita" value="<%=datauscita%>"
			<tr>
				<td width="200" align="left"><%=cognome%></td>
				<td width="200" align="left"><%=nome%></td>
				<td width="120" align="left"><%=Ucase(cdfiscale)%></td>
				<td width="120" align="left"><input type="text" name="data" value="<%=datauscita%>" onBlur="javascript:validaData(this)" maxlenght="10"><a class="miniBottone" href="javascript:mostraCalendario()">...</a></td>
				<td width="120" align="left">
				<!--
					<select name="causale">
					  <option>ciao</option>
					  <option>ciao</option>
					  <option>ciao</option>
					  <option>ciao</option>
					  <option>ciao</option>
					</select>
				-->
					<input type="text" name="causale" value="<%=causale%>">
				</td>
				<input type="hidden" name="codice" value="<%=codice%>">
				<input type="hidden" name="lista" value="1">
			</tr>
			<tr>
			   <td colspan="7" height="1" bgcolor="black"></td>
			</tr>
			
		<%
			end if
		%>
		</table>
		<br>
		<table align="right" cellspacing="20" cellpadding="0">
		  <tr>
		    <td align="center">
				<a class="gen" href="javascript:confermaUscita()">Conferma</a>
			</td>
			<td align="center">
				<a class="gen" href="javascript:annullaRegistrazione()">Cancella</a>
			</td>
			<td align="center"><a class="gen" href="javascript:annulla()">Indietro</a></td>
		  </tr>
		</table>
		</form>
	</body>
</html>
<%
	res.close
	connection.close
%>
