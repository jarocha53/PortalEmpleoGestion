<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%@ LANGUAGE=VBSCRIPT %>
<!-- #include file="db/connessione.asp" -->

<% response.expires = - 1500 %>

<%
	codici = split(Request.Form("codice"), ",")
	
	'selezione degli LSU che possono essere messi in uscita => SOLO quelli attivi e quelli per cui � stata richiesta una variazione dati
	query = "SELECT P.ID_PERSONA, P.NOME, P.COGNOME, P.COD_FISC, PE.DT_USCITA, PE.CAUSALE_USCITA " & _
	        "FROM PERS_ENTE PE, PERSONA P " & _
			"WHERE PE.FL_STATO_PERSONA IN ('A', 'V') AND " & _
			"P.ID_PERSONA IN ("
	for i = lbound(codici) to ubound(codici) 
		query = query & "'"&codici(i)&"'"
		if (i < ubound(codici)) then 
			query = query & ", "
		else
			query = query & ")"
		end if
	next
	query = query & " AND PE.ID_PERSONA = P.ID_PERSONA"
	
	Set res = Server.createObject("ADODB.Recordset")
'PL-SQL * T-SQL  
QUERY = TransformPLSQLToTSQL (QUERY) 
	Set res = connection.execute(query)
	
	if res.EOF then
		' FIXME -> in questo caso non ci sono LSU da mettere in uscita
		Server.transfer("MonitoraggioMain.asp") 
	end if
%>

<html>
	<head>
		<style type="text/css">
		<!--
			 td {
			 		font-family: tahoma;
					font-size: 12;
					font-weight: bold;
			 }
			 td.valore {
			 		font-family: tahoma;
					font-size: 16;
					font-weight: bolder;
			 }
			 td.header {
			 		font-family: tahoma;
					font-size: 20;
					font-weight: bolder;
					text-align: center;
			 }
			 td.codfis {
			 		font-family: courier new;
					font-size: 20;
					font-weight: bolder;
					text-align: center;
			 }
		-->
		</style>
	</head>
	<body>
		<script src="utils/utility.js"></script>
		<script>
				   
			function conferma() {
				checks = document.forms[0].escludi
				codici = document.forms[0].codice
				date   = document.forms[0].dataUscita
				causali= document.forms[0].causale
				lista  = document.forms[0].lista
				
				for (k = 1; k < codici.length; k++) {
					if (! checks[k].checked)  { // l'LSU non � stato escluso dall'operazione
						if (isEmpty(date[k].value) || isEmpty(causali[k].value)) {
							alert("Tutti i dati sono obbligatori")
							date[k].select()
							date[k].focus()
							return
						}
					} else {
						// escludiamo l'LSU dal salvataggio
						lista[k].value="0";
					}
				}
				document.forms[0].submit()
			}
		</script>
		
		<table width="100%" height="100%">
			<tr>
	      	<td class="header">
				Per confermare l'uscita degli LSU selezionati, scegliere la causale ed inserire la data di uscita
				</td>
			</tr>
			<tr>
				<td valign="middle" align="center">
				<form method="post" action="SalvataggioUscita.asp">
					<table style="border: 1 solid black; border-right: 2 solid black" width="70%" border="0" align="center" valign="middle" cellpadding="0" cellspacing="0">
						
						<!-- per fare in modo che sia sempre un array -->
						<input type="hidden" name="codice" value="0">
						<input type="hidden" name="dataUscita" value="0">
						<input type="hidden" name="causale" value="0">
						<input type="hidden" name="escludi" value="0">
						<input type="hidden" name="lista" value="0">
						
					<%	
						do while not res.EOF 	
					%>
						<input type="hidden" name="codice" value="<%=res("id_persona")%>">
						<tr>
							<td>
								<table style="border-right: 1 solid black" border="0" cellpadding="4" bgcolor="#dddddd" width="100%" height="100%">
									<tr>
										<td valign="middle" width="100">
											Nome
										</td>
										<td class="valore" valign="middle">
											<%=res("nome")%>
										</td>
										<td valign="middle" width="100">
											Cognome
										</td>
										<td class="valore" valign="middle">
											<%=res("cognome")%>
										</td>
									</tr>
									<tr>
										<td valign="middle" width="100">
											Codice Fiscale
										</td>
										<td class="valore" colspan="3" valign="middle">
											<%=res("cod_fisc")%>
										</td>
									</tr>
									<tr>
										<td align="left" colspan="2">
											<input type="checkbox" name="escludi">Escludi dall'operazione</checkbox>
											<input type="hidden" name="lista" value="1">
										</td>
									</tr>
								</table>
							</td>
							<td>
								<table border="0" cellpadding="9" bgcolor="#ccccff" width="100%" height="100%">
									<tr>
										<td valign="middle">
											Data di uscita
										</td>
										<td class="valore" valign="middle">
											<input cols="8" type="text" name="dataUscita" value="<%=res("dt_uscita")%>" onBlur="validaData(this)">
										</td>
									</tr>
									<tr>
										<td valign="middle">
											Causale
										</td>
										<td class="valore" valign="middle">
											<input type="text" name="causale" value="<%=res("causale_uscita")%>">
											<!-- <select style="width: 100%" name="causale" value="<%=res("causale_uscita")%>">
												</select>
											-->
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr><td height="2" bgcolor="black" colspan="2"></td></tr>
					<%
						res.movenext
						loop
					%>	
						<tr>
							<td align="center"> 
								<a  href="javascript:conferma()"><img src="images/Ok.gif" alt="Conferma" border="0"/></a>
							</td>
							<td align="center">
								<a  href="javascript:annulla()"><img src="images/NoWay.gif" alt="Annulla" border="0"/></a>
							</td>
						</tr>
					</table>
				</form>
			</td>
		</tr></table>
	</body>
</html>

<%
	res.close
	connection.close
%>



	
