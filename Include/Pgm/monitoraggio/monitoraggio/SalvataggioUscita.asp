<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%@ LANGUAGE=VBSCRIPT %>

<!-- #INCLUDE FILE="adovbs.inc" -->
<!-- #INCLUDE FILE="db\gestionelsu.asp" -->


<%
	'''
	'' ---------------> CONTROLLO VALIDITA' SESSIONE
	'
	if(isNull(Session)) then
		Server.transfer("Redirect.html")
	end if
	
	'''
	'' --------------->   CONTROLLO AUTENTICAZIONE UTENTE
	'
	if (isNull(Session("ente")) or Session("ente") = "") then 
		Response.redirect("Login.html")
		return
	end if
%>
<%
	   Set connection = Server.createObject("ADODB.Connection")
	   	   connection.open(session("connectString"))
			
	   Set res = Server.createObject("ADODB.Recordset")
	   Set resSosp = Server.createObject("ADODB.Recordset")
	   Set resDate = Server.createObject("ADODB.Recordset")
	   
	   codici = split(Request.Form("codice"), ",")
	   cognomi= split(Request.Form("cognome"), ",")
	   nomi   = split(Request.Form("nome"), ",")
	   lista  = split(Request.Form("lista"), ",")
	   data   = Request.Form("dataUscita")
	   causale= Request.Form("causale")
	   
	   Dim erroreInSalvataggio
	   Dim maxProgressivo
		   maxProgressivo = 0
	   continue = true

	   for k = Lbound(codici)+1 to Ubound(codici)
			if (lista(k) > 0) then
				' recupero le informazioni da modificare per l'utente selezionato
				' le informazioni sullo stato (fl_stato_persona, certificato) si trovano in pers_ente
				' le informazioni sulla modifica e su data/causale uscita vanno salvate in AREA_LAVORO
				' e TMP_USCITA
				
				'query per estrarre la data di inizio sospensione di un LSU in uscita se questo � sospeso
				queryDate = "SELECT DT_INIZIO_SOSP FROM PERS_SOSP WHERE ID_PERSONA = " & codici(k) & " AND DT_FINE_SOSP IS NULL " & _
				            "UNION " & _
							"SELECT DT_INIZIO_SOSP FROM TMP_PERS_SOSP " & _
							"	WHERE ID_PERSONA = " & codici(k) & " " & _
				            "	AND PROGRESSIVO = (SELECT MAX(PROGRESSIVO) FROM AREA_LAVORO WHERE ID_PERSONA = " & codici(k) & ") "
				resDate.open queryDate, connection
				if (not resDate.EOF) then
					' testiamo se l'LSU pu� essere messo in uscita; questo � possibile solo se la sua data di 
					' inizio sospensione � precedente alla data di uscita che vogliamo attribuirgli
					dataSosp = Cdate(resDate("dt_inizio_sosp"))
					dataUsc  = Cdate(data)
					if(dataSosp > dataUsc) then
						continue = false
						' non possiamo salvare le info richieste per questo LSU
						if (erroreInSalvataggio = "") then
							erroreInSalvataggio = cognomi(k)&" "&nomi(k)
						else
							erroreInSalvataggio = erroreInSalvataggio & ", "&cognomi(k)&" "&nomi(k)
						end if
					else
						continue = true
					end if
				else
					continue = true
				end if
				resDate.close()	
				if (continue) then
					' info sullo stato
					queryStato  = "SELECT FL_STATO_PERSONA, CERTIFICATO " & _
						          "FROM PERS_ENTE " & _
							      "WHERE ID_PERSONA = " & codici(k)
						
					res.open queryStato, connection, adOpenDynamic, adLockOptimistic
					if (not res.EOF) then
						stato = res("fl_stato_persona")
						certificato = res("certificato")
						
						resSosp.open "SELECT MAX(PROGRESSIVO) PROGRESSIVO FROM AREA_LAVORO WHERE ID_PERSONA = "& codici(k), connection
						if (not resSosp.EOF) then
							if (not isNull(resSosp("progressivo"))) then
								maxProgressivo = resSosp("progressivo") 
							else
								maxProgressivo = 1
							end if
						end if
						resSosp.close()
						
						''''''''''''''''''''''''''''''''''''
						' salvataggio della data di uscita '
						''''''''''''''''''''''''''''''''''''
												
						if (stato <> "U") then
							resSosp.open "SELECT ID_PERSONA, ID_IMPRESA, ID_SEDE, PROGRESSIVO, TIPO_OPERAZIONE FROM AREA_LAVORO", connection, adOpenDynamic,adLockOptimistic
							resSosp.addNew()
								resSosp("id_persona") = codici(k)
								resSosp("id_impresa") = session("ente")
								resSosp("id_sede") = session("ente")
								resSosp("progressivo") = maxProgressivo + 1
								resSosp("tipo_operazione") = "U"
							resSosp.update()
							resSosp.close()
							
							resSosp.open "SELECT ID_PERSONA, ID_IMPRESA, ID_SEDE, PROGRESSIVO, DT_USCITA, CAUSALE_USCITA FROM TMP_USCITA", connection, adOpenDynamic,adLockOptimistic
							resSosp.addNew()
								resSosp("id_persona") = codici(k)
								resSosp("id_impresa") = session("ente")
								resSosp("id_sede") = session("ente")
								resSosp("progressivo") = maxProgressivo + 1
								resSosp("dt_uscita") = data
								resSosp("causale_uscita") = causale
							resSosp.update()
							resSosp.close()
						else
							'l'LSU era gi� uscito, quindi era un dato non certificato e lo stiamo modificando
							query = "SELECT DT_USCITA, CAUSALE_USCITA FROM TMP_USCITA WHERE ID_PERSONA = " & codici(k) & " AND PROGRESSIVO = " & maxProgressivo
							resSosp.open query, connection, adOpenDynamic, adLockOptimistic
								resSosp("dt_uscita") = data
								resSosp("causale_uscita") = causale
							resSosp.update()
							resSosp.close()
							
						end if
						
						'''''''''''''''''''''''''''''
						' aggiornamento dello stato '
						'''''''''''''''''''''''''''''
						res("fl_stato_persona") = "U"
						res("certificato") = "N"
						res.update()
					end if
					res.close()
				end if ' fine operazioni per LSU che pu� essere messo in uscita
			end if
		next
		
		Set res = nothing
		Set resSosp  = nothing
		Set resDate = nothing
  	    connection.close()
		Set connection = nothing
	
	'puliamo la sessione...
	rimuoviAZ()
	rimuoviLettere()
	if(erroreInSalvataggio = "") then
		Server.transfer("Visualizzazione.asp")
	else
		errore = "Non � stato possibile mettere in uscita i seguenti LSU:<br>"
		errore = errore & erroreInSalvataggio & "<br> in quanto sospesi con data di inizio sospensione <br> successiva alla data di uscita specificata"
		session("messaggioErrore") = errore
		Server.transfer("Errore.asp")
	end if
%>
