<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%@ LANGUAGE=VBSCRIPT %>

<%
	'''
	'' ---------------> CONTROLLO VALIDITA' SESSIONE
	'
	if(isNull(Session)) then
		Server.transfer("Redirect.html")
	end if
	
	'''
	'' --------------->   CONTROLLO AUTENTICAZIONE UTENTE
	'
	if (Session("ente") = null or Session("ente") = "") then 
		Response.redirect("Login.html")
		return
	end if
%>
<%
   modo = Request.querystring("mode")
%>
<html>
<style type="text/css">
  td.voceMenu, a {
    font-family: tahoma;
    font-size: 12;
	font-weight: bolder;
	color: white;
	text-align: left;
	text-decoration: none;
	margin: 20;
  }
  td.voceLegenda {
    font-family: tahoma;
    font-size: 11;
	font-weight: bold;
	color: white;
	text-align: center;
	text-decoration: none;
  }
  td.a {
    font-family: tahoma;
    font-size: 12;
	font-weight: bolder;
	color: white;
	text-align: center;
	text-decoration: none;
	margin: 20;
  }
  
  
</style>
<head>
	<title>Untitled</title>
</head>
<%
	Dim mode, mask
	if(session("tipoUtente") = "supervisore") then
		mode = "external"
		select case session("livelloUtente")
			case 5
				mask = "D"
			case 4
				mask = "C"
			case 3
				mask = "B"
			case 2
				mask = "A"
		end select
	else
		mode = "internal"
	end if
%>
<body bgcolor="white" leftmargin="0" rightmargin="0" topmargin="0">
<table border="0" cellpadding="0" cellspacing="0" width="100%" height="100%">
      <tr>
		 <td bgcolor="#003265" align="center" valign="middle">
		    <table align="center" width="98%" height="98%" border="0" cellspacing="0" cellpadding="0">
			  <tr>
			    <td width="20" height="20"><img src="images/bordoUL.jpg" width="20" height="20" border="0"></td>
				<td width="70%" height="20" bgcolor="white" align="center">
                </td>
				<td width="20" height="20"><img src="images/bordoUR.jpg" width="20" height="20" border="0"></td>
			  </tr>
			  <tr>
			    <td width="20" bgcolor="white">&nbsp;</td>
				<td bgcolor="white" width="98%" align="center" valign="middle">
				  <iframe src="Interno.asp?mode=<%=mode%>&mask=<%=mask%>" name="frameTabella" width="100%" height="100%" align="center" frameborder="0" marginwidth="0" marginheight="0"></iframe>
				</td>
				<td width="20" bgcolor="white">&nbsp;</td>
			  </tr>
			  <tr>
			    <td width="20" height="20"><img src="images/bordoBL.jpg" width="20" height="20" border="0"></td>
				<td width="70%" height="20" bgcolor="white" align="center">
				
				</td>
				<td width="20" height="20"><img src="images/bordoBR.jpg" width="20" height="20" border="0"></td>
			  </tr>
			</table>
		 </td>
		 <td width="1" bgcolor="white"></td>
      </tr>
</table>
</body>
</html>
