<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%@ LANGUAGE=VBSCRIPT %>

<% response.expires = -1500 %>

<%
	'''
	'' ---------------> CONTROLLO VALIDITA' SESSIONE
	'
	if(isNull(Session)) then
		Server.transfer("Redirect.html")
	end if
	
	'''
	'' --------------->   CONTROLLO AUTENTICAZIONE UTENTE
	'
	if (Session("ente") = null or Session("ente") = "") then
		Response.redirect("Login.html")
		return
	end if
%>

<%
	offset = 30
	Dim query
	Set connection = Server.createObject("ADODB.Connection")
	connection.open(session("connectString"))

	query = "SELECT I.RAG_SOC FROM IMPRESA I WHERE I.ID_IMPRESA = " & Session("ente")
'PL-SQL * T-SQL  
QUERY = TransformPLSQLToTSQL (QUERY) 
	Set outer = connection.execute(query)
	ragione_sociale = outer("rag_soc")
	outer.close
	sub sottoAlbero(id_impresa, livello)
		query = "SELECT I.ID_IMPRESA, I.RAG_SOC FROM IMPRESA I, ASSOCIAZIONI A WHERE A.PADRE = " & id_impresa & " AND I.ID_IMPRESA = A.FIGLIO"
'PL-SQL * T-SQL  
QUERY = TransformPLSQLToTSQL (QUERY) 
		Set res = connection.execute(query)
		Do while NOT res.eof
			figlio = res("id_impresa")
			nomefi = res("rag_soc")
%>
			<tr>
				<td class="lista" style="padding: 0; padding-left: <%=livello%>">
				<%=nomefi%>
				</td>
<%
			query = "SELECT MIN(DT_ELABORAZIONE) VALIDAZIONE, MAX(DT_ELABORAZIONE) CERTIFICAZIONE " & _
		            "FROM CERTIFICAZIONE " & _
			        "WHERE ID_IMPRESA = " & figlio
'PL-SQL * T-SQL  
QUERY = TransformPLSQLToTSQL (QUERY) 
			Set inner = connection.execute(query)
			data_validazione = inner("validazione")
			ultima_certificazione = inner("certificazione")
			inner.close

			query = "SELECT COUNT(ID_PERSONA) ATTIVI " & _
	                "FROM PERS_ENTE " & _
					"WHERE FL_STATO_PERSONA = 'A' AND ID_IMPRESA = " & figlio & " AND CERTIFICATO = 'S'"
'PL-SQL * T-SQL  
QUERY = TransformPLSQLToTSQL (QUERY) 
			Set inner = connection.execute(query)
			lsu_attivi = inner("attivi")
			inner.close

			query = "SELECT COUNT(ID_PERSONA) SOSPESI " & _
	                "FROM PERS_ENTE " & _
					"WHERE FL_STATO_PERSONA = 'S' AND ID_IMPRESA = " & figlio & " AND CERTIFICATO = 'S'"
'PL-SQL * T-SQL  
QUERY = TransformPLSQLToTSQL (QUERY) 
			Set inner = connection.execute(query)
			lsu_sospesi = inner("sospesi")
			inner.close

			query = "SELECT COUNT(ID_PERSONA) USCITI " & _
	                "FROM PERS_ENTE " & _
					"WHERE FL_STATO_PERSONA = 'U' AND ID_IMPRESA = " & figlio & " AND CERTIFICATO = 'S'"
'PL-SQL * T-SQL  
QUERY = TransformPLSQLToTSQL (QUERY) 
			Set inner = connection.execute(query)
			lsu_usciti = inner("usciti")
			inner.close
%>
				<td class="lista" width="170" align="left">
					<%=data_validazione%>
				</td>
				<td class="lista" width="170" align="left">
					<%=ultima_certificazione%>
				</td>
				<td class="lista" width="60" align="right">
<%
				if (lsu_attivi > 0) then
%>
					<a href="javascript:lista('<%=figlio%>','attivi')"><%=lsu_attivi%></a>
<%
				else
%>
					<%=lsu_attivi%>
<%
				end if
%>
				</td>
				<td class="lista" width="60" align="right">
<%
				if (lsu_sospesi > 0) then
%>
					<a href="ListaLSUEsterni.asp?vedi=<%=figlio%>&filtro=sospesi"><%=lsu_sospesi%></a>
<%
				else
%>
					<%=lsu_sospesi%>
<%
				end if
%>
				</td>
				<td class="lista" width="60" align="right">
<%
				if (lsu_usciti > 0) then
%>
					<a href="ListaLSUEsterni.asp?vedi=<%=figlio%>&filtro=usciti"><%=lsu_usciti%></a>
<%
				else
%>
					<%=lsu_usciti%>
<%
				end if
%>
				</td>
			</tr>
			<tr>
				<td height="1" bgcolor="#AAAAAA" colspan="6">
				</td>
			</tr>
<%
			sottoAlbero figlio, livello + offset
			res.Movenext
		Loop
	res.close()
	end sub
%>
<html>
<head>
	<link rel ="stylesheet" type="text/css" href="stylesheet.css" title="Style">
</head>
<script>
	function lista(ente, tipo) {
		window.open("ListaLSUEsterni.asp?vedi="+ente+"&filtro="+tipo, "", "width=700,height=600,");
	}
</script>
<body style="border: 1 solid #AAAAAA" bgcolor="white" rightmargin="0" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
	<table align="center" border="0" width="100%" cellspacing="0" cellpadding="2">
		<tr>
			<td colspan="6" align="center" height="40" class="entePiccolo">Sotto enti di <%=ragione_sociale%></td>
		</tr>
		<tr>
			<td class="headercolonna" align="center">Ente</td>
			<td class="headercolonna" align="center">Data validazione</td>
			<td class="headercolonna" align="center">Ultima certificazione</td>
			<td class="headercolonna" align="center">LSU Attivi</td>
			<td class="headercolonna" align="center">LSU Sospesi</td>
			<td class="headercolonna" align="center">LSU Usciti</td>
		</tr>
		<tr><td height="1" bgcolor="#AAAAAA" colspan="6"></td></tr>
		<% sottoAlbero Session("ente"), offset %>
	</table>
</body>
</html>
