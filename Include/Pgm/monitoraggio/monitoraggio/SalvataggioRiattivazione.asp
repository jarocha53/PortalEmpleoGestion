<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%@ LANGUAGE=VBSCRIPT %>

<!-- #include file="adovbs.inc" -->
<!-- #include file="db\gestionelsu.asp" -->


<%
	'''
	'' ---------------> CONTROLLO VALIDITA' SESSIONE
	'
	if(isNull(Session)) then
		Server.transfer("Redirect.html")
	end if
	
	'''
	'' --------------->   CONTROLLO AUTENTICAZIONE UTENTE
	'
	if (Session("ente") = null or Session("ente") = "") then 
		Response.redirect("Login.html")
		return
	end if
%>
<% 
	   Set connection = Server.createObject("ADODB.Connection")
   		   connection.open(session("connectString"))
			
	   Set res = Server.createObject("ADODB.Recordset")
	   Set resDate = Server.createObject("ADODB.Recordset")
	   Set resDati = Server.createObject("ADODB.Recordset")
	   Set resSosp = Server.createObject("ADODB.Recordset")
   
	   codici    = split(Request.Form("codice"), ",")
	   lista     = split(Request.Form("lista"), ",")
	   fine     = Request.Form("fineSospensione")

	   Dim erroreInSalvataggio
	   maxProgressivo = 0
	   continue = true

	   for k = Lbound(codici)+1 to Ubound(codici)
	   	if (lista(k) > 0) then
	   		' recupero le informazioni da modificare per l'utente selezionato
	   		' le informazioni sullo stato (fl_stato_persona, certificato) si trovano in pers_ente
	   		' le informazioni sulla modifica e su data/causale uscita vanno salvate in AREA_LAVORO
	   		' e TMP_USCITA
	   		
	   		'query per estrarre la data di inizio sospensione di un LSU in uscita se questo � sospeso
	   		queryDate = "SELECT DT_INIZIO_SOSP FROM PERS_SOSP WHERE ID_PERSONA = " & codici(k) & " AND DT_FINE_SOSP IS NULL " & _
	   		            "UNION " & _
	   					"SELECT DT_INIZIO_SOSP FROM TMP_PERS_SOSP " & _
	   					"	WHERE ID_PERSONA = " & codici(k) & " " & _
	   		            "	AND PROGRESSIVO = (SELECT MAX(PROGRESSIVO) FROM AREA_LAVORO WHERE ID_PERSONA = " & codici(k) & ") "
	   		resDate.open queryDate, connection
	   		if (not resDate.EOF) then
	   			' testiamo se l'LSU pu� essere messo in uscita; questo � possibile solo se la sua data di 
	   			' inizio sospensione � precedente alla data di uscita che vogliamo attribuirgli
	   			dataRiatt = Cdate(resDate("dt_inizio_sosp"))
	   			dataSosp  = Cdate(fine)
	   			if(dataRiatt > dataSosp) then
	   				continue = false
	   				' non possiamo salvare le info richieste per questo LSU
	   				if (isNull(erroreInSalvataggio) or isEmpty(erroreInSalvataggio)) then
	   					erroreInSalvataggio = cognomi(k)&" "&nomi(k)
	   				else
	   					erroreInSalvataggio = erroreInSalvataggio & ", "&cognomi(k)&" "&nomi(k)
	   				end if
	   				response.write("Errore salvataggio: " &erroreInSalvataggio)
	   			else
	   				continue = true
	   			end if
	   		else
	   			continue = true
	   		end if
	   		resDate.close()	
	   		if (continue) then
	   			' info sullo stato
	   			queryStato  = "SELECT FL_STATO_PERSONA, CERTIFICATO " & _
	   				          "FROM PERS_ENTE " & _
	   					      "WHERE ID_PERSONA = " & codici(k)
	   				
	   			res.open queryStato, connection, adOpenDynamic, adLockOptimistic
	   			if (not res.EOF) then
	   				stato = res("fl_stato_persona")
	   				certificato = res("certificato")
	   				
	   				resSosp.open "SELECT MAX(PROGRESSIVO) PROGRESSIVO FROM AREA_LAVORO WHERE ID_PERSONA = "& codici(k), connection
	   				if (not resSosp.EOF) then
	   					if (not isNull(resSosp("progressivo"))) then
	   						maxProgressivo = resSosp("progressivo") 
	   					else
	   						maxProgressivo = 1
	   					end if
	   				end if
	   				resSosp.close()
	   				
	   				''''''''''''''''''''''''''''''''''''''''''''''
	   				' salvataggio della data di fine sospensione '
	   				''''''''''''''''''''''''''''''''''''''''''''''
	   										
	   				if (certificato = "S") then
	   					resSosp.open "SELECT ID_PERSONA, ID_IMPRESA, ID_SEDE, PROGRESSIVO, TIPO_OPERAZIONE FROM AREA_LAVORO", connection, adOpenDynamic,adLockOptimistic
	   					resSosp.addNew()
	   						resSosp("id_persona") = codici(k)
	   						resSosp("id_impresa") = session("ente")
	   						resSosp("id_sede") = session("ente")
	   						resSosp("progressivo") = maxProgressivo + 1
	   						resSosp("tipo_operazione") = "R"
	   					resSosp.update()
	   					resSosp.close()
	   					
						'recupero delle informazioni relative alla sospensione certificata da PERS_SOSP
						Dim inizio
						Dim maternita
						queryDati = "SELECT DT_INIZIO_SOSP, FL_MATERNITA FROM PERS_SOSP WHERE ID_PERSONA = " & codici(k) & " AND DT_FINE_SOSP IS NULL"
						resDati.open queryDati, connection
						if (not resDati.EOF) then
							inizio = resDati("dt_inizio_sosp")
							maternita = resDati("fl_maternita")
						end if
						resDati.close()
						
	   					resSosp.open "SELECT ID_PERSONA, ID_IMPRESA, ID_SEDE, PROGRESSIVO, DT_INIZIO_SOSP, FL_MATERNITA, DT_FINE_SOSP FROM TMP_PERS_SOSP", connection, adOpenDynamic,adLockOptimistic
	   					resSosp.addNew()
	   						resSosp("id_persona") = codici(k)
	   						resSosp("id_impresa") = session("ente")
	   						resSosp("id_sede") = session("ente")
	   						resSosp("progressivo") = maxProgressivo + 1
							resSosp("dt_inizio_sosp") = Cdate(inizio)
	   						resSosp("dt_fine_sosp") = Cdate(fine)
	   						resSosp("fl_maternita") = maternita
	   					resSosp.update()
	   					resSosp.close()
	   				else
	   					'l'LSU era gi� sospeso, quindi era un dato non certificato e lo stiamo modificando
						query = "SELECT TIPO_OPERAZIONE FROM AREA_LAVORO WHERE ID_PERSONA = " & codici(k) & " AND PROGRESSIVO = " & maxProgressivo
	   					resSosp.open query, connection, adOpenDynamic, adLockOptimistic
						if (not resSosp.EOF) then
							resSosp("tipo_operazione") = "R"
							resSosp.update()
						end if
	   					resSosp.close()
						
	   					query = "SELECT DT_FINE_SOSP FROM TMP_PERS_SOSP WHERE ID_PERSONA = " & codici(k) & " AND PROGRESSIVO = " & maxProgressivo
	   					resSosp.open query, connection, adOpenDynamic, adLockOptimistic
						if (not resSosp.EOF) then
							if (not isNull(fine)) then
		   						resSosp("dt_fine_sosp") = fine
							else
								resSosp("dt_fine_sosp") = null
							end if
							resSosp.update()
						end if
	   					resSosp.close()
	   					
	   				end if
	   				
	   				'''''''''''''''''''''''''''''
	   				' aggiornamento dello stato '
	   				'''''''''''''''''''''''''''''
	   				res("fl_stato_persona") = "A"
	   				res("certificato") = "N"
	   				res.update()
	   			end if
	   			res.close()
	   		end if ' fine operazioni per LSU che pu� essere riattivato
	   	end if
	   next
	   
	   Set res = nothing
	   Set resSosp  = nothing
	   Set resDate = nothing
	   Set resDati = nothing
	   
	   connection.close()
	   Set connection = nothing
	
	'puliamo la sessione...
	rimuoviAZ()
	rimuoviLettere()
	
	if(erroreInSalvataggio = "") then
	   Server.transfer("Visualizzazione.asp")
	else
	   errore = "Non � stato possibile riattivare i seguenti LSU:<br>"
	   errore = errore & erroreInSalvataggio & "<br> in quanto sospesi con data di inizio sospensione <br> successiva alla data di fine sospensione specificata"
	   session("messaggioErrore") = errore
	   Server.transfer("Errore.asp")
	end if
	   
%>
