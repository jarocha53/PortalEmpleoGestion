<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%@ LANGUAGE=VBSCRIPT %>

<!-- #INCLUDE FILE="adovbs.inc" -->

<%
	'''
	'' ---------------> CONTROLLO VALIDITA' SESSIONE
	'
	if(isNull(Session)) then
		Server.transfer("Redirect.html")
	end if
	
	'''
	'' --------------->   CONTROLLO AUTENTICAZIONE UTENTE
	'
	if (Session("ente") = null or Session("ente") = "") then 
		Response.redirect("Login.html")
		return
	end if
%>
<% 
       Set connection = Server.createObject("ADODB.Connection")
		   connection.open(session("connectString"))
			
	   Set resPE = Server.createObject("ADODB.Recordset")
	   Set resPS = Server.createObject("ADODB.Recordset")
   
	   codici    = split(Request.Form("codice"), ",")
	   lista     = split(Request.Form("lista"), ",")
	   inizio    = Request.Form("inizioSospensione")
	   fine      = Request.Form("fineSospensione")
	   maternita = Request.Form("maternita")
	   
	  
	   for k = Lbound(codici)+1 to Ubound(codici)
			if (lista(k) > 0) then
				'Aggiornamento di PERS_ENTE
				queryPE = "SELECT FL_STATO_PERSONA, CERTIFICATO FROM PERS_ENTE WHERE ID_PERSONA = " & codici(k) & " AND ID_IMPRESA = " & session("ente")
				resPE.open queryPE, connection, adOpenDynamic, adLockOptimistic
				if (not resPE.EOF) then
					stato = resPE("fl_stato_persona")
					certificato = resPE("certificato")
															
					if (stato = "S") then
						'modifica dei dati di sospensione di una persona gi� sospesa
						queryPS = "SELECT DT_INIZIO_SOSP, DT_FINE_SOSP, FL_MATERNITA FROM TMP_PERS_SOSP WHERE ID_PERSONA = " & codici(k) & _
						          " AND PROGRESSIVO = (SELECT MAX(PROGRESSIVO) FROM TMP_PERS_SOSP WHERE ID_PERSONA = " & codici(k) & ")"
						resPS.open queryPS, connection, adOpenDynamic, adLockOptimistic
						if (not resPS.EOF) then
							resPS("dt_inizio_sosp") = inizio
							if (not isNull(fine) and fine <> "") then
								resPS("dt_fine_sosp") = Cdate(fine)
							else
								resPS("dt_fine_sosp") = null
							end if
							if (maternita = "1") then
								resPS("fl_maternita") = "1"
							else
								resPS("fl_maternita") = "0"
							end if
							resPS.update()
						end if
						resPS.close()
						
						queryPS = "SELECT TIPO_OPERAZIONE FROM AREA_LAVORO WHERE ID_PERSONA = " & codici(k) & " AND PROGRESSIVO = (SELECT MAX(PROGRESSIVO) FROM AREA_LAVORO WHERE ID_PERSONA = " & codici(k) & ")"
						resPS.open queryPS, connection, adOpenDynamic, adLockOptimistic
						if (not resPS.EOF) then
							if (not isNull(fine) and fine <> "") then
								resPS("tipo_operazione") = "R"
							else
								resPS("tipo_operazione") = "S"
							end if
							resPS.update()
						end if
						resPS.close()
					else
						'sospensione di una persona precedentemente attiva e certificata
						maxProgressivo = 0
						resPS.open "SELECT MAX(PROGRESSIVO) PROGRESSIVO FROM AREA_LAVORO WHERE ID_PERSONA = "& codici(k), connection
						if (not resPS.EOF) then
							if (not isNull(resPS("progressivo"))) then
								maxProgressivo = resPS("progressivo") 
							else
								maxProgressivo = 1
							end if
						end if
						resPS.close()
						
						queryPS = "SELECT ID_PERSONA, ID_IMPRESA, ID_SEDE, PROGRESSIVO, TIPO_OPERAZIONE FROM AREA_LAVORO"
						resPS.open queryPS, connection, adOpenDynamic,adLockOptimistic
						resPS.addNew()
							resPS("id_persona") = codici(k)
							resPS("id_impresa") = session("ente")
							resPS("id_sede") = session("ente")
							resPS("progressivo") = maxProgressivo + 1
							resPS("tipo_operazione") = "S"
						resPS.update()
						resPS.close()
						
						queryPS = "SELECT ID_PERSONA, ID_IMPRESA, ID_SEDE, PROGRESSIVO, DT_INIZIO_SOSP, DT_FINE_SOSP, FL_MATERNITA FROM TMP_PERS_SOSP"
						resPS.open queryPS, connection, adOpenDynamic,adLockOptimistic
						resPS.addNew()
							resPS("id_persona") = codici(k)
							resPS("id_impresa") = session("ente")
							resPS("id_sede") = session("ente")
							resPS("progressivo") = maxProgressivo + 1
							resPS("dt_inizio_sosp") = inizio
							if(not isNull("fine") and fine <> "") then
								resPS("dt_fine_sosp") = Cdate(fine)
							else
								resPS("dt_fine_sosp") = null
							end if
							if (maternita = "1") then
								resPS("fl_maternita") = "1"
							else
								resPS("fl_maternita") = "0"
							end if
						resPS.update()
						resPS.close()
						
					end if
					
					if (not isNull(fine) and fine <> "") then
						resPE("fl_stato_persona") = "A"
					else
						resPE("fl_stato_persona") = "S"
					end if
					resPE("certificato") = "N"
					resPE.update()
					
				end if
				resPE.close()
			end if
		next
		Set resPE = nothing
		Set resPS = nothing
	   	connection.close()
		Set connection = nothing

   Server.transfer("Visualizzazione.asp")
%>
