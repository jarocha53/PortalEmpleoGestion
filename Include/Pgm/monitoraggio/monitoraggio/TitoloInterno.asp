<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%@ LANGUAGE=VBSCRIPT %>

<!-- #include file="adovbs.inc" -->

<% response.expires = -1500 %>

<%	
	'''
	'' ---------------> CONTROLLO VALIDITA' SESSIONE
	'
	if(isNull(Session)) then
		Server.transfer("Redirect.html")
	end if
	
	Set connection = Server.createObject("ADODB.Connection")
		connection.open(session("connectString"))
		
%>

<%
	'''
	'' --------------->   CONTROLLO AUTENTICAZIONE UTENTE
	'
	if (Session("ente") = null or Session("ente") = "") then 
		session("ErrorMessage") = "Utente non autenticato"
		Server.transfer("Login.html")
		return
	end if
%>
<%
	Dim ente
		ente = request.querystring("guarda")
	Dim maschera
		maschera = request.queryString("mask")
	Dim guardanull
	guardanull = 0
	if(isNull(ente) or isEmpty(ente) or ente = "") then
		ente = session("ente")
		guardanull = 1
	end if
	
	Dim entiSottoposti
	Dim count
		count = 0
	if (session("livelloUtente") > 1) then
		queryEnti = "SELECT FIGLIO FROM ASSOCIAZIONI WHERE PADRE = " & ente & " " & _
		            "UNION " & _
					"SELECT FIGLIO FROM ASSOCIAZIONI WHERE PADRE IN (SELECT FIGLIO FROM ASSOCIAZIONI WHERE PADRE = " & ente & ")"
'PL-SQL * T-SQL  
QUERYENTI = TransformPLSQLToTSQL (QUERYENTI) 
		Set res = connection.execute(queryEnti)
		do while not(res.EOF)
			codice = res("figlio")
			if (isNull(entiSottoposti) or isEmpty(entiSottoposti)) then
				entiSottoposti = codice
			else
				count = count + 1
				entiSottoposti = entiSottoposti & ", " & codice
			end if
			res.moveNext()
		loop
		res.close()
	end if

	' Selezione degli LSU in carico
	Dim lsuAttivi
		lsuAttivi = 0
	if (session("livelloUtente") = 1 or count = 0 or maschera = "A") then
		queryAttivi = "SELECT COUNT(ID_PERSONA) PERSONE " & _
					  "FROM PERS_ENTE " & _
					  "WHERE FL_STATO_PERSONA IN ('A','S') AND ID_IMPRESA = " & ente
	elseif (maschera = "D") then
		queryAttivi = "SELECT COUNT(ID_PERSONA) PERSONE " & _
					  "FROM PERS_ENTE " & _
					  "WHERE FL_STATO_PERSONA IN ('A','S') AND CERTIFICATO = 'S'"
	else
	 	queryAttivi = "SELECT COUNT(ID_PERSONA) PERSONE " & _
					  "FROM PERS_ENTE " & _
					  "WHERE FL_STATO_PERSONA IN ('A','S') AND CERTIFICATO = 'S' AND ID_IMPRESA IN ( " & entiSottoposti & ")"
	 end if
	
'PL-SQL * T-SQL  
QUERYATTIVI = TransformPLSQLToTSQL (QUERYATTIVI) 
	 Set res = connection.execute(queryAttivi)
	 if (not res.EOF) then
	 	lsuAttivi = res("persone")
	 end if
	 res.close

	 'Selezione degli LSU sospesi
	 Dim lsuSospesi
	 	lsuSospesi = 0
	 if (session("livelloUtente") = 1 or count = 0 or maschera = "A") then
		 querySospesi = "SELECT COUNT(ID_PERSONA) PERSONE " & _
		                "FROM PERS_ENTE " & _
						"WHERE FL_STATO_PERSONA = 'S' AND ID_IMPRESA = " & ente
	elseif (maschera = "D") then
		querySospesi = "SELECT COUNT(ID_PERSONA) PERSONE " & _
		               "FROM PERS_ENTE " & _
					   "WHERE FL_STATO_PERSONA = 'S' AND CERTIFICATO = 'S'"
	else
	 	querySospesi = "SELECT COUNT(ID_PERSONA) PERSONE " & _
		                "FROM PERS_ENTE " & _
						"WHERE FL_STATO_PERSONA = 'S' AND CERTIFICATO = 'S' AND ID_IMPRESA IN ( " & entiSottoposti & ")"
	 end if
	 
'PL-SQL * T-SQL  
QUERYSOSPESI = TransformPLSQLToTSQL (QUERYSOSPESI) 
	 Set res = connection.execute(querySospesi)
	 if (not res.EOF) then
	 	lsuSospesi = res("persone")
	 end if
	 res.close
	 
	 'Selezione degli LSU usciti
	 Dim lsuUsciti
	 	lsuUsciti = 0
	 if (session("livelloUtente") = 1 or count = 0 or maschera = "A") then
		 queryUsciti = "SELECT COUNT(PE.ID_PERSONA) PERSONE " & _
		               "FROM PERS_ENTE PE, USCITA U " & _
					   "WHERE PE.FL_STATO_PERSONA = 'U' AND PE.ID_IMPRESA = " & ente & " " & _
					   "  AND PE.ID_PERSONA = U.ID_PERSONA AND U.DT_USCITA > to_date('1 gen 2002')"
	 elseif (maschera = "D") then
		queryUsciti = "SELECT COUNT(PE.ID_PERSONA) PERSONE " & _
		              "FROM PERS_ENTE PE " & _
					  "WHERE PE.FL_STATO_PERSONA = 'U' AND CERTIFICATO = 'S'"
	 else
	 	queryUsciti = "SELECT COUNT(PE.ID_PERSONA) PERSONE " & _
		              "FROM PERS_ENTE PE, USCITA U " & _
					  "WHERE PE.FL_STATO_PERSONA = 'U' AND PE.CERTIFICATO = 'S' AND PE.ID_IMPRESA IN ( " & entiSottoposti & ")" & " " & _
					  "  AND U.ID_PERSONA = PE.ID_PERSONA AND U.DT_USCITA >= to_date('1 gen 2002')"
	 end if
'PL-SQL * T-SQL  
QUERYUSCITI = TransformPLSQLToTSQL (QUERYUSCITI) 
	 Set res = connection.execute(queryUsciti)
	 if (not res.EOF) then
	 	lsuUsciti = res("persone")
	 end if
	 res.close
	 
	 Dim ultimaCertificazione
	 
	 query = "SELECT MAX(DT_RIFERIMENTO ) DATARIF FROM CERTIFICAZIONE WHERE ID_IMPRESA = " & ente
'PL-SQL * T-SQL  
QUERY = TransformPLSQLToTSQL (QUERY) 
	 Set res = connection.execute(query)
	 if not res.EOF then
	 	ultimaCertificazione = res("datarif")
	end if	  

	if (isNull(ultimaCertificazione) or ultimaCertificazione = "") then
		ultimaCertificazione = "n.a."
	end if
	
	
	res.close()
	
	Dim dataCertificazione
	Dim today
	    today = Date()
	Dim mese
	giorno = day(today)
	if (giorno > 15) then
		mese = month(today)
	else
		mese = month(today) - 1
	end if 
		
	dataCertificazione = "15/"&mese&"/"&year(today)
%>
<html>
<style>
  td.infoEnte {
    font-family: tahoma;
    font-size: 11;
	font-weight: bold;
	color: #1234cc;
  }
  td.ente {
    font-family: tahoma;
    font-size: 14;
	font-weight: bolder;
	color: #6789cc;
	text-align: left;
  }
</style>
<head>
</head>
<body bgcolor="white" alink="#003366" leftmargin="0" link="#003366" rightmargin="0" topmargin="0" vlink="#003366">
<script>
	function guarda(cod) {
		parent.titoloInterno.location = "TitoloInterno.asp?guarda="+cod
		parent.headerInterno.location = "HeaderTabellaA.asp"
		parent.mainInterno.location = "MascheraA.asp?guarda="+cod
	}
</script>
<table width="100%" height="100%" cellspacing="0" cellpadding="0">
  <tr>
  	<% if(maschera = "D" or (maschera = "" and session("livelloUtente") = 5)) then %>
	    <td class="infoEnte">
			SISTEMI DI MONITORAGGIO <br> SITUAZIONE NAZIONALE LSU
		</td>
	<% else %>
		<td class="ente">
		<%
			VariableSQL = "SELECT RAG_SOC FROM IMPRESA WHERE ID_IMPRESA = " & ente
		'PL-SQL * T-SQL
			VariableSQL = TransformPLSQLToTSQL(VariableSQL) 
			Set outer = connection.execute(VariableSQL)
			if (not outer.EOF) then
			%>
				<b style="color:black">ENTE:</b> <%=outer("rag_soc")%>
			<%
			end if
			outer.close
			connection.close()
		%>
		</td>
	<% end if %>
	<% if (session("livelloUtente") = 1 or session("livelloUtente") = 2 or maschera = "A") then %>
		<td  align="right" valign="top">
			<table style="border:1 solid black" cellspacing="0" cellpadding="0">
				<tr>
					<td>
						<table style="border-right:1 solid black">
							<tr>
								<td class="infoEnte">
									DELIBERA N� xxxx
								</td>
								<td class="infoEnte">
									DEL: xx/xx/xxxx
								</td>
							</tr>
							<tr>
								<td class="infoEnte">
									DATA INIZIO: xx/xx/xxxx
								</td>
								<td class="infoEnte">
									DATA FINE: xx/xx/xxxx
								</td>
							</tr>
						</table>
					</td>
					<td>
						<table>
							<tr>
								<td class="infoEnte">
									NUMERO DI LSU IN CARICO: <%= lsuAttivi %> DI CUI SOSPESI: <%= lsuSospesi %>
								</td>
							</tr>
							<tr>
								<td class="infoEnte">
									NUMERO DI LSU USCITI DA 1/1/2002: <%= lsuUsciti %>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	<% else %>
		<td valign="middle" align="center" class="infoEnte">
			Dati al: <%= dataCertificazione %>
		</td>
		<td align="right" valign="top">
			<table style="border:1 solid black">
				<tr>
					<td class="infoEnte">
						NUMERO DI LSU IN CARICO: <%= lsuAttivi %> DI CUI SOSPESI: <%= lsuSospesi %>
					</td>
				</tr>
				<tr>
					<td class="infoEnte">
						NUMERO DI LSU USCITI DA 1/1/2002: <%= lsuUsciti %>
					</td>
				</tr>
			</table>
		</td>
	<% end if %>
  </tr>
</table>
</body>
</html>
