<%@ LANGUAGE=VBSCRIPT %>
<!-- #include file="db/salvataggio.asp" -->
<%
	'''
	'' ---------------> CONTROLLO VALIDITA' SESSIONE
	'
	if(isNull(Session)) then
		Server.transfer("Redirect.html")
	end if
	
	'''
	'' --------------->   CONTROLLO AUTENTICAZIONE UTENTE
	'
	if (Session("ente") = null or Session("ente") = "") then 
		Response.redirect("Login.html")
		return
	end if
%>
<% 
   segnalaInserimenti()
   if Request.Form("ancora") = "si" then
      Server.transfer("ListaInserimento.asp")
   else
      Server.transfer("Visualizzazione.asp")
   end if
%>