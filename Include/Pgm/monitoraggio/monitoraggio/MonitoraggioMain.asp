<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%@ LANGUAGE=VBSCRIPT %>
<!-- #include file="db/connessione.asp" -->
<html>
<%
   	 visualizzazione = Request.querystring("view")
	 if (visualizzazione = "internal") then
	     visualizzazione = "Visualizzazione.asp"
	 elseif (visualizzazione = "external") then
	   session("mode") = "read_only"
	   visualizzazione = "VisualizzazioneEsterni.asp"
	 else
	   visualizzazione = "Visualizzazione.asp"
	 end if
	 
	 Set res = Server.createObject("ADODB.RecordSet")

	 ' Selezione degli LSU in carico
	 queryAttivi = "SELECT COUNT(ID_PERSONA) PERSONE " & _
	                "FROM PERS_ENTE " & _
					"WHERE FL_STATO_PERSONA IN ('A','S') AND ID_IMPRESA = " & session("ente")
				   
'PL-SQL * T-SQL  
QUERYATTIVI = TransformPLSQLToTSQL (QUERYATTIVI) 
	 Set res = connection.execute(queryAttivi)
	 lsuAttivi = 0
	 if (not res.EOF) then
	 	lsuAttivi = res("persone")
	 end if
	 res.close
	 
	 'Selezione degli LSU sospesi
	 querySospesi = "SELECT COUNT(ID_PERSONA) PERSONE " & _
	                "FROM PERS_ENTE " & _
					"WHERE FL_STATO_PERSONA = 'S' AND ID_IMPRESA = " & session("ente")
'PL-SQL * T-SQL  
QUERYSOSPESI = TransformPLSQLToTSQL (QUERYSOSPESI) 
	 Set res = connection.execute(querySospesi)
	 lsuSospesi = 0
	 if (not res.EOF) then
	 	lsuSospesi = res("persone")
	 end if
	 res.close
	 
	 connection.close
	 
	 action = request.QueryString("action")
	 if (action = "sort") then
	 	session("filter") = request.QueryString("filter")
	 else
	 	session("filter") = "cognome"
	 end if
	
%>
	<head>
	<style type="text/css">
	<!--
				 td {
				 		font-family: tahoma;
						font-size: 12;
						font-weight: bold;
				 }
				 td.header {
				 		font-family: tahoma;
						font-size: 12;
						font-weight: bold;
						<%
						  if ombreggia then
						    Response.write("background-color: #969696;")
							else
								Response.write("background-color: #006699;")
							end if
						%>
				 }
				 td.tab {
				 		font-family: tahoma;
						font-size: 18;
						font-weight: bold;
						background-color: #006699;
				 }
	-->
	</style>
	<title>Monitoraggio LSU</title>
	</head>
	<body bgcolor="#6699CC">

		<script language="JavaScript" type="text/javascript" scr="utils/utility.js">
			function certificaDati() {
				if (confirm("Certificare i dati per il mese corrente?")) {
					window.location="SalvataggioCertificazione.asp"	
				}
			}

			function segnalaUscita() {
				options = window.FrmVisualizzazione.document.FormDati.codice
				count = 0;
				for (i = 0; i < options.length; i++)  {
					if (options[i].checked) {
						count++
					}
				}
				if (count > 0)  {
					window.FrmVisualizzazione.document.FormDati.target = "_top"
					window.FrmVisualizzazione.document.FormDati.action = "Uscita.asp"
					window.FrmVisualizzazione.document.FormDati.submit()
				} else {
					alert("Selezionare gli LSU in uscita")
					return
				}
			}

			function segnalaSospensione() {
				options = window.FrmVisualizzazione.document.FormDati.codice
				count = 0;
				for (i = 0; i < options.length; i++)  {
					if (options[i].checked) {
						count++
					}
				}
				if (count > 0)  {
					window.FrmVisualizzazione.document.FormDati.target = "_top"
					window.FrmVisualizzazione.document.FormDati.action = "Sospensione.asp"
					window.FrmVisualizzazione.document.FormDati.submit()
				} else {
					alert("Selezionare gli LSU in sospensione")	
					return
				}
			}

			function segnalaSconosciuto() {
				options = window.FrmVisualizzazione.document.FormDati.codice
				count = 0;
				for (i = 0; i < options.length; i++)  {
					if (options[i].checked) {
						count++
					}
				}
				if (count > 0)  {
					window.FrmVisualizzazione.document.FormDati.target = "_top"
					window.FrmVisualizzazione.document.FormDati.action = "Esclusione.asp"
					window.FrmVisualizzazione.document.FormDati.submit()			
				} else {
					alert("Selezionare gli LSU sconosciuti")	
					return
				}
			}

			function variazioneDati() {
				options = window.FrmVisualizzazione.document.FormDati.codice
				count = 0;
				for (i = 0; i < options.length; i++)  {
					if (options[i].checked) {
						count++
					}
				}
				if (count > 0)  {
					window.FrmVisualizzazione.document.FormDati.target = "_top"
					window.FrmVisualizzazione.document.FormDati.action = "Variazione.asp"
					window.FrmVisualizzazione.document.FormDati.submit()			
				} else {
					alert("Selezionare gli LSU con dati inesatti")	
					return
				}
			}

			function inserimento() {
				righe = window.prompt("Digitare il numero di LSU da inserire", 5)
				if (righe > 0) {
					window.location = "Inserimento.asp?rows="+righe
				}
			}

			function esterni() {
			  window.location = "MonitoraggioMain.asp?view=external"
			}

		  	function interni() {
			  window.location = "MonitoraggioMain.asp?view=internal"
			}

		</script>

		<table border="0" height="100%" width="100%" cellspacing="0" cellpadding="0">
		   <tr height="1%">
			    <td height="1%">
					  <table border="0" cellspacing="0" cellpadding="10" width="100%">
					  	   <tr>
						   	<td colspan="3">
							   	<table width="100%" border="0">
									<tr>
										<td colspan="3">
											<h1>Regione Campania (<%=session("ente")%>)</h1>
										</td>
									</tr>
									<tr>
										<td colspan="2">
											N� di LSU in carico <%=lsuAttivi%>, di cui <%=lsuSospesi%> sospesi
										</td>
										<td align="right">
											Data fine impiego: 31/12/2002
										<td>
									</tr>
								</table>
							</td>
						   </tr>
						   <tr>
							    <td class="tab" style="width:20%; border-left: 1 solid black; border-top: 1 solid black; border-right: 1 solid black">
									<a href="javascript:interni()">
									LSU
									</a>
									</td>
									<td class="tab" style="width:20%; border-left: 1 solid black; border-top: 1 solid black; border-right: 1 solid black">
									<a href="javascript:esterni()">
									Esterni
									</a>
								</td>
								<td>&nbsp;</td>
								
							 </tr>
						</table>
					</td>
			 </tr>
		   <tr height="1%">
			 	 <td height="1%">
					 <table cellpadding="2" width="100%" border="0" style="border: 1 solid black" cellspacing="0">
					 	<tr>
						 <td class="header" width="21" align="left">
							&nbsp;
					   	 </td>
				         <td class="header" width="200" align="left" height="1%">
							Nome
					     </td>
					     <td class="header" width="200" align="left" height="1%">
							<a href="MonitoraggioMain.asp?action=sort&filter=cognome">Cognome</a>
					     </td>
						 <!--
						     <td class="header" width="100" align="left" height="1%">
								Cod. Fiscale
						     </td>
						 -->
					     <td class="header" width="150" align="left" height="1%">
							<a href="MonitoraggioMain.asp?action=sort&filter=cod_settore_lsu">Settore</a>
					     </td>
				   	     <td class="header" width="150" align="left" height="1%">
							<a href="MonitoraggioMain.asp?action=sort&filter=cod_mansione_lsu">Mansione</a>
				   	     </td>
						 <td class="header" width="12" align="left" height="1%">
						 	<a href="MonitoraggioMain.asp?action=sort&filter=status">Stato</a>
						 </td>
						 <td class="header" width="12" align="left" height="1%">
						 
						 </td>
						</tr>
					 </table>
				 </td>
		   </tr>
		   <tr>
		      <td colspan="7">
		         <iframe name="FrmVisualizzazione" scrolling="yes" border="0" 
						         framespacing="0" marginheight="0" marginwidth="0" 
										 frameborder="0" width="100%" height="100%"
										 src="<%=visualizzazione%>">
						 </iframe>
		      </td>
		   </tr>
		   <tr>
		      <td colspan="7" height="1%" bgcolor="#006699">
			     <!-- #include file="Toolbar.asp" -->
			  </td>
		   </tr>
		</table>
	</body>
</html>
