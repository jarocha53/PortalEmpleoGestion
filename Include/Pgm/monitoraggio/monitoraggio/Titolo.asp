<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%@ LANGUAGE=VBSCRIPT %>

<% response.expires = -1500 %>

<%
	'''
	'' ---------------> CONTROLLO VALIDITA' SESSIONE
	'
	if(isNull(Session)) then
		Server.transfer("Redirect.html")
	end if
	
	'''
	'' --------------->   CONTROLLO AUTENTICAZIONE UTENTE
	'
	if (Session("ente") = null or Session("ente") = "") then 
		Response.redirect("Login.html")
		return
	end if
%>
<%
  Dim nomeGiorno, nomeMese
  oggi = date
  giorno = day(date)
  mese = month(date)
  anno = year(date)
  gsett = weekday(date)
  
	select case giorno
		case 1 
			nomeGiorno = "Domenica"
		case 2 
			nomeGiorno = "Luned�"
		case 3 
			nomeGiorno = "Marted�"
		case 4 
			nomeGiorno = "Mercoled�"
		case 5 
			nomeGiorno = "Gioved�"
		case 6 
			nomeGiorno = "Venerd�"
		case 7 
			nomeGiorno = "Sabato"
	end select 
	
	select case mese
		case 1 
			nomeMese = "Gennaio"
		case 2 
			nomeMese = "Febbraio"
		case 3 
			nomeMese = "Marzo"
		case 4 
			nomeMese = "Aprile"
		case 5 
			nomeMese = "Maggio"
		case 6 
			nomeMese = "Giugno"
		case 7 
			nomeMese = "Luglio"
		case 8
			nomeMese = "Agosto"
		case 9 
			nomeMese = "Settembre"
		case 10 
			nomeMese = "Ottobre"
		case 11 
			nomeMese = "Novembre"
		case 12 
			nomeMese = "Dicembre"
	end select 
	
	'questo valore indica se la validazione iniziale dei dati � stata effettuata o meno
	Dim certificazione
	Dim certificazionePerQuestoPeriodo
	
	Set connection = Server.createObject("ADODB.Connection")
		connection.open(session("connectString"))
	
	Set res = Server.createObject("ADODB.Recordset")
	VariableSQL = "SELECT DT_RIFERIMENTO " & _
	                             "FROM CERTIFICAZIONE " & _
								 "WHERE ID_IMPRESA = " & session("ente")
'PL-SQL * T-SQL
	VariableSQL = TransformPLSQLToTSQL(VariableSQL) 
	Set res = connection.execute(VariableSQL)
	if (not res.EOF) then
		' l'ente ha gi� effettuato la certificazione iniziale e quindi il tasto 
		' di certificazione deve essere disabilitato
		certificazione = "disabled"
	else
		certificazione = "enabled"
	end if
	
	session("certificazione") = certificazione

	res.close()
	Set res = nothing
	connection.close()
	Set connection = nothing
%>
<html>
<style>
   a.bottone {
    font-family: tahoma;
    font-size: 11;
	font-weight: bold;
	color: white;
	text-align: center;
	border: 1 solid black;
	background-color: #006796;
	text-decoration: none;
	padding: 4;
	margin: 0;
	vertical-align: middle;
	width: 100;
  }
  a.bottoneDisabilitato {
    font-family: tahoma;
    font-size: 11;
	font-weight: bold;
	color: white;
	text-align: center;
	border: 1 solid black;
	background-color: #666666;
	text-decoration: none;
	padding: 4;
	margin: 0;
	vertical-align: middle;
	width: 100;
  }
  a.bottonePiccolo {
    font-family: tahoma;
    font-size: 9;
	font-weight: bold;
	color: white;
	text-align: center;
	border: 1 solid black;
	background-color: #003162;
	text-decoration: none;
	padding: 2;
	margin: 0;
	vertical-align: middle;
	width: 60;
  }
  
  a.titolo {
    font-family: tahoma;
    font-size: 12;
	font-weight: bold;
	color: white;
	text-decoration: none;
	text-align: center;
	padding: 0;
	margin: 0;
  }
</style>
<head></head>
<body bgcolor="white" alink="#003366" leftmargin="0" link="#003366" rightmargin="0" topmargin="0" vlink="#003366">
	<script language="javascript">
		function livSup(cod) {
			
		}
		<%
			if (Session("tipoUtente") = "operatore") then
		%>
		// FUNZIONI CERTIFICAZIONE
		function certificaDati() {
			window.open("ConfermaCertificazione.asp", "Conferma", "width=700,height=400,noresize,nomaximize,scrollbars=yes");
		}
	
		// FUNZIONI VARIAZIONE DATI
		function variazione() {
						
		}
			
		function confermaVariazione() {
			checks = parent.mainInterno.document.forms[0].escludi
			codici = parent.mainInterno.document.forms[0].codice
			mansioni = parent.mainInterno.document.forms[0].mansione
			settori  = parent.mainInterno.document.forms[0].settore
			
			for (k = 1; k < codici.length; k++) {
				if (checks[k].checked)  { // l'LSU non � stato escluso dall'operazione
					if (isEmpty(mansioni[k].value)) {
						alert("Tutti i campi sono obbligatori")
						mansioni[k].select()
						mansioni[k].focus()
						return
					}
					if (isEmpty(settori[k].value)) {
						alert("Tutti i campi sono obbligatori")
						settori[k].select()
						settori[k].focus()
						return
					}
					} else {
					// escludiamo l'LSU dal salvataggio
					lista[k].value="0";
				}
			}
			parent.headerInterno.location = "HeaderTabella.asp"
	        parent.barraBottoni.location = "Bottoni.asp"
			parent.mainInterno.document.forms[0].submit()
		}
		<%
			else
		%>
			function interni() {
				parent.main.document.location = "Tabella.asp"
			}
			
			function esterni() {
				parent.main.frameTabella.document.location = "Albero.asp"
			}
			
			function storici() {
				
			}
		<%
			end if
		%>
	</script>
		
<table border="0" cellpadding="0" cellspacing="0" width="100%">
      <tr>
         <td width="86"><img border="0" height="35" src="images/titleUL.jpg" width="86"></td>
         <td width="351" bgcolor="#006796" align="center">
		 <table border="0" cellpadding="0" cellspacing="0" width="100%">
		 <tr>
		    <td align="center">
		       <font face="tahoma" size="1" color="white"><b><%=nomeGiorno%> <br> <%=giorno%> &nbsp; <%=nomeMese%> &nbsp; <%=anno%></b></font>
		    </td>
			<td>
		       <a class="bottonePiccolo" href="Tabella.asp" target="main">Home</a>
		    </td>
			<td>
		       <a class="bottonePiccolo" href="">Help</a>
		    </td>
			<td>
		       <a class="bottonePiccolo" href="Logout.asp" target="_top">Exit</a>
		    </td>
		 </tr>
		 </table>
		 </td>
         <td width="62"><img border="0" height="35" src="images/titleUR.jpg" width="62"></td>
		 <td width="433"></td>
      </tr>
      <tr>
         <td width="437" colspan="2"><img border="0" height="62" src="images/titleBL.jpg" width="437"></td>
		 <td bgcolor="#003265" width="70%" align="center" valign="middle" class="titolo">
		 <table width="100%" align="center">
		 	<tr valign="middle">
				<% if(not isNull(request.queryString("livsup")) and not isEmpty(request.queryString("livsup"))) then %>
				<td align="center">
					<a class="bottone" href="javascript:livSup('<%=request.queryString("livsup")%>')">Livello superiore</a>
				</td>
				<% end if %>
				<% if (Session("tipoUtente") = "operatore") then 
					strCertificazione = "Certificazione<br>iniziale"					
				%>
				<!-- ################################################
				     #############  BOTTONI OPERATORE  ##############
					 ################################################ -->
					<% if(certificazione = "enabled") then %>
						<td align="center" valign="middle" style="margin-bottom: 18">
							<a class="bottone" href="javascript:certificaDati()"><%=strCertificazione%></a>
						</td>
					<% else %>
						<td align="center" valign="middle" style="margin-bottom: 18">
							<a class="bottoneDisabilitato" href="javascript:alert('Operazione disponibile solo nella fase di validazione iniziale dei dati')"><%=strCertificazione%></a>
						</td>
					<% end if%>
					<td align="center" valign="middle" style="margin-bottom: 18">
						<a class="bottone" href="javascript:variazione()">Variazione <br>dati consolidati</a>
					</td>
					<td align="center" valign="middle" style="margin-bottom: 18">
						<a class="bottone" href="">Modifica<br>dati delibera</a>
					</td>
				<%
					end if
				%>
			</tr>
		   </table>
		 </td>
		 <td width="55"><img border="0" height="62" src="images/titleBR.jpg" width="55"></td>
      </tr>
</table>
</body>
</html>

