<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #include file="db/connessione.asp" -->

<table bgcolor="#006699" border="0" style="border: 1 solid black" cellpadding="2" width="100%" width="20" cellspacing="0" cellpadding="0">
	<tr>
		<td class="header" align="center">
			<%
			 ' la certificazione � abilitata solo dal 5 al 10 del mese
			  today = Date()
			  if datepart("d", today) >= 2 and datepart("d", today) <= 10 then
			  	certifica = true
			  else
			  	certifica = false
			  end if
			  
			  'la certificazione � possibile solo una volta al mese
			  'come si fa a stabilire se � gi� stata fatta la certificazione per il mese corrente?
			  'IPOTESI: la certificazione � gi� stata fatta se il mese dell'ultima data di 
			  '         certificazione per l'impresa � uguale al mese corrente
			  
			  query = "SELECT MAX(DT_ELABORAZIONE) DT_ELABORAZIONE " & _
			          "FROM CERTIFICAZIONE " & _
					  "WHERE ID_IMPRESA = " & session("ente")
			  
			  Set res = Server.createObject("ADODB.Recordset")
'PL-SQL * T-SQL  
QUERY = TransformPLSQLToTSQL (QUERY) 
			  Set res = connection.execute(query)
			  
			  if not res.EOF then
			  	' � gi� stata fatta una certificazione dall'impresa
				dataCertificazione = res("dt_elaborazione")
				if month(dataCertificazione) = month(Date()) then
					certifica = false
				end if
			  else
			  	' � la prima certificazione in assoluto
				certifica = true
			  end if
			  			  
			  if not ombreggia and certifica then
			    Response.write("<a href=""javascript:certificaDati()"">")
				end if
			%>
			<img src="images/certifica.jpg" border="0" alt="Certifica i dati">
			<%
			  if not ombreggia and certifica then
			    Response.write("</a>")
				end if
			%>
		</td>
		<td class="header" align="center">
		  <%
			  if not ombreggia then
			    Response.write("<a href=""javascript:segnalaUscita()"">")
				end if
			%>
			<img src="images/uscita.jpg" border="0" alt="Segnala l'uscita">
			<%
			  if not ombreggia then
			    Response.write("</a>")
				end if
			%>
		</td>
		<td class="header" align="center">
		  <%
			  if not ombreggia then
			    Response.write("<a href=""javascript:segnalaSospensione()"">")
				end if
			%>
			<img src="images/sospensione.jpg" border="0" alt="Segnala la sospensione">
      <%
			  if not ombreggia then
			    Response.write("</a>")
				end if
			%>
		</td>
		<td class="header" align="center">
		  <%
			  if not ombreggia then
			    Response.write("<a href=""javascript:segnalaSconosciuto()"">")
				end if
			%>
			<img src="images/esclusione.jpg" border="0" alt="LSU Sconosciuto">
			<%
			  if not ombreggia then
			    Response.write("</a>")
				end if
			%>
		</td>
		<td class="header" align="center">
		  <%
			  if not ombreggia then
			    Response.write("<a href=""javascript:variazioneDati()"">")
				end if
			%>
			<img src="images/variazione.jpg" border="0" alt="Variazione dati">
			<%
			  if not ombreggia then
			    Response.write("</a>")
				end if
			%>
		</td>
		<td class="header" align="center">
		  <%
			  if not ombreggia then
			    Response.write("<a href=""javascript:inserimento()"">")
				end if
			%>
			<img src="images/inserimento.jpg" border="0" alt="Inserimento">
			<%
			  if not ombreggia then
			    Response.write("</a>")
				end if
			%>
		</td>
	</tr>
</table>
