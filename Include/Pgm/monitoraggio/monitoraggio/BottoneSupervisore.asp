<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%@ LANGUAGE=VBSCRIPT %>
<% response.expires = -1500 %>
<!-- #include file="adovbs.inc" -->
<%
	'''
	'' ---------------> CONTROLLO VALIDITA' SESSIONE
	'
	if(isNull(Session)) then
		Server.transfer("Redirect.html")
	end if
	
	'''
	'' --------------->   CONTROLLO AUTENTICAZIONE UTENTE
	'
	if (Session("ente") = null or Session("ente") = "") then 
		Response.redirect("Login.html")
		return
	end if
	
	Dim mask
	Dim guarda
	
	mask = request.queryString("mask")
	select case mask:
		case "A":
			guarda = session("mascheraB")
		case "B":
			guarda = session("mascheraC")
		case "C":
			guarda = session("mascheraD")
		case "D":
			guarda = ""
	end select
%>

<html>
<head>
<style>
  a.gen {
    font-family: tahoma;
    font-size: 12;
	font-weight: bold;
	color: white;
	text-align: center;
	border: 1 solid black;
	background-color: #006796;
	text-decoration: none;
	padding: 2;
	margin: 0;
	vertical-align: middle;
	width: 200;
  }
</style>
</head>
<body>
	<script language="javascript">
		function back() {
			<% select case mask:
				case "A": %>
				top.main.frameTabella.location = "Interno.asp?mode=external&mask=B&guarda=<%=guarda%>"
			<%	case "B": %>
				top.main.frameTabella.location = "Interno.asp?mode=external&mask=C&guarda=<%=guarda%>"			
			<%	case "C": %>	
				top.main.frameTabella.location = "Interno.asp?mode=external&mask=D&guarda=<%=guarda%>"
			<%	case "D": %>
				
			<% end select %>
		}
	</script>
	<table width="100%">
		<tr>
			<td align="center">
			
				<%
					if(isNull(mask) or isEmpty(mask) or isNull(guarda) or isEmpty(guarda) or mask = "" or guarda = "") then
						Server.transfer("Blank.asp")
					end if
				%>
					<a class="gen" href="javascript:back()">Livello Superiore</a>
			</td>
		</tr>
	</table>
</body>
</html>
