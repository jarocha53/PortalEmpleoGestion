<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%@ LANGUAGE=VBSCRIPT %>

<%
	'''
	'' --------------->   CONTROLLO AUTENTICAZIONE UTENTE
	'
	if (Session("ente") = null or Session("ente") = "") then 
		Response.redirect("Login.html")
		return
	end if
	if(session("livelloUtente") <> 5) then
		' QUESTO NON DEVE VEDERE QUI !
	end if
%>
<html>
	<head>
		<style type="text/css">
			 td {
			 		font-family: tahoma;
					font-size: 12;
					font-weight: bold;
			 }
			 
			 td.cfis {
			 		font-family: arial;
					font-size: 12;
					font-weight: bold;
			 }
			 
			 a.miniBottone {
			 		font-family: tahoma;
					font-size: 8;
					font-weight: bold;
					text-decoration: none;
					vertical-align: middle;
					border: 1 solid black;
					color: black;
			 }
		</style>
	</head>
	<body>
		<script>
			function guarda(cod) {
				parent.titoloInterno.location = "TitoloInterno.asp?guarda="+cod+"&linkalo=si"
				parent.headerInterno.location = "HeaderTabellaC.asp"
				parent.mainInterno.location = "MascheraC.asp?guarda="+cod
			}
		</script>
		<table border="0" cellpadding="1" style="border-left: 1 solid black; border-right: 1 solid black" width="100%" cellspacing="0">
		<%
			Dim guardato
			guardato = Request.Form("guarda")
			if(isNull(guardato) or isEmpty(guardato) or trim(guardato) = "") then
				guardato = session("ente")
			end if
			Set connection = Server.createObject("ADODB.Connection")
			connection.open(session("connectString"))
			query = "SELECT DISTINCT PE.ID_IMPRESA codice, PE.RAG_SOC nome, " & _
			        "NVL((SELECT to_char(MAX(DT_RIFERIMENTO), 'DD/MM/YYYY') FROM CERTIFICAZIONE WHERE ID_IMPRESA = PE.ID_IMPRESA), '') certificato " & _
					"FROM IMPRESA PE " & _
					"WHERE PE.ID_IMPRESA IN (SELECT FIGLIO FROM ASSOCIAZIONI WHERE PADRE = " & GUARDATO &")"
'PL-SQL * T-SQL  
QUERY = TransformPLSQLToTSQL (QUERY) 
			Set res = connection.execute(query)
			do while not res.eof
				codice = res("codice")
				nome = res("nome")
				certificato = res("certificato")
				
				Dim entiSottoposti
				Dim count
					count = 0
				queryEnti = "SELECT FIGLIO FROM ASSOCIAZIONI WHERE PADRE = " & codice & " " & _
				            "UNION " & _
							"SELECT FIGLIO FROM ASSOCIAZIONI WHERE PADRE IN (SELECT FIGLIO FROM ASSOCIAZIONI WHERE PADRE = " & codice & ")"
'PL-SQL * T-SQL  
'QUERYENTI = TransformPLSQLToTSQL (QUERYENTI) 
				Set resStato = connection.execute(queryEnti)
				do while not(resStato.EOF)
					figlio = resStato("figlio")
					if (isNull(entiSottoposti) or isEmpty(entiSottoposti)) then
						entiSottoposti = figlio
					else
						count = count + 1
						entiSottoposti = entiSottoposti & ", " & figlio
					end if
					resStato.moveNext()
				loop
				resStato.close()
			
				Dim queryAttivi
				if (count = 0) then 
					queryAttivi = "SELECT COUNT(ID_PERSONA) ATTIVI FROM PERS_ENTE WHERE FL_STATO_PERSONA IN ('A', 'S') AND CERTIFICATO = 'S' AND ID_IMPRESA =" & codice
				else
					queryAttivi = "SELECT COUNT(ID_PERSONA) ATTIVI FROM PERS_ENTE WHERE FL_STATO_PERSONA IN ('A', 'S') AND CERTIFICATO = 'S' AND ID_IMPRESA IN ("&entiSottoposti&")"
				end if
				
				Dim inCarico
					inCarico = 0
'PL-SQL * T-SQL  
QUERYATTIVI = TransformPLSQLToTSQL (QUERYATTIVI) 
				Set resStato = connection.execute(queryAttivi)
				if (not resStato.EOF) then
					inCarico = resStato("attivi")
				end if
				resStato.close()
				
				Dim querySospesi
				if (count = 0) then 
					querySospesi = "SELECT COUNT(ID_PERSONA) SOSPESI FROM PERS_ENTE WHERE FL_STATO_PERSONA = 'S' AND CERTIFICATO = 'S' AND ID_IMPRESA =" & codice
				else
					querySospesi = "SELECT COUNT(ID_PERSONA) SOSPESI FROM PERS_ENTE WHERE FL_STATO_PERSONA = 'S' AND CERTIFICATO = 'S' AND ID_IMPRESA IN ("&entiSottoposti&")"
				end if
				
				Dim sospesi
					sospesi = 0
'PL-SQL * T-SQL  
QUERYSOSPESI = TransformPLSQLToTSQL (QUERYSOSPESI) 
				Set resStato = connection.execute(querySospesi)
				if (not resStato.EOF) then
					sospesi = resStato("sospesi")
				end if
				resStato.close()
				
				Dim queryUsciti
				if (count = 0) then 
					queryUsciti = "SELECT COUNT(ID_PERSONA) USCITI FROM PERS_ENTE WHERE FL_STATO_PERSONA = 'U' AND CERTIFICATO = 'S' AND ID_IMPRESA =" & codice
				else
					queryUsciti = "SELECT COUNT(ID_PERSONA) USCITI FROM PERS_ENTE WHERE FL_STATO_PERSONA = 'U' AND CERTIFICATO = 'S' AND ID_IMPRESA IN ("&entiSottoposti&")"
				end if
				
				Dim usciti
					usciti = 0
'PL-SQL * T-SQL  
QUERYUSCITI = TransformPLSQLToTSQL (QUERYUSCITI) 
				Set resStato = connection.execute(queryUsciti)
				if (not resStato.EOF) then
					usciti = resStato("usciti")
				end if
				resStato.close()
		%>
			<tr height="20">
				<td width="200" align="left" bgcolor="<%=colore%>">
				<% if(session("livelloUtente") = 5) then %>
					<a target="_parent" href="Interno.asp?mode=external&guarda=<%=codice%>&mask=C&linkalo=si">
				<% end if %>
				<%=nome%>
				<% if(session("livelloUtente") = 5) then %>
					</a>
				<% end if %>
				</td>
				<td width="50" align="left" bgcolor="<%=colore%>"><%=inCarico%></td>
				<td width="50" align="left" bgcolor="<%=colore%>"><%=sospesi%></td>
				<td width="50" align="left" bgcolor="<%=colore%>"><%=usciti%></td>
			</tr>
			<tr>
			   <td colspan="15" height="1" bgcolor="black"></td>
			</tr>

		<%
			res.movenext
			loop
		%>
		</table>
	</body>
</html>
