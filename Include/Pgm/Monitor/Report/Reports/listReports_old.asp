<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include Virtual = "/strutt_testa2.asp"-->
<!--#include Virtual = "/include/openconn.asp"-->
<!--#include virtual = "/include/ControlDateVb.asp"-->
<!--#include virtual="/util/dbutil.asp"-->
<!--#include virtual="/include/DecComun.asp"-->
<!--#include Virtual = "/include/DecCod.asp"-->
<!--#include virtual = "/include/SelAreaTerrBandi.asp"-->

<script language="javascript" src="Util.js"></script>
<script language="javascript"><!--#include virtual = "/Include/help.inc"--></script>
<% Inizio() %>
<table cellpadding="0" cellspacing="0" width="488" border="0">
	<tr height="18">
		<td class="sfondomenu" height="18" width="45%">
			<span class="tbltext0"><b>REPORTES</b></span>
		</td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
		<td valign="middle" align="right" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1"></td>
	</tr>
	<tr style="height:2px;">
		<td width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif"></td>     
	</tr>
</table>
<table cellpadding="0" cellspacing="0" width="488" border="0">
	<tr width="371">
		<td colspan="3" class="sfondocomm" style="text-align:justify;">
			Es posible consultar un reporte seleccionando uno de los tipos de reportes que se listan a continuaci&oacute;n.
			Una vez seleccionado se visualizar&aacute; el resultado en pantalla.
		</td>
	</tr>
	<tr style="height:2px;">
		<td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
	</tr>
</table>
<%

	sSQL = "SELECT DESCRIZIONE, PRV FROM SEDE_IMPRESA SI, IMPRESA I" &_
			" WHERE SI.ID_IMPRESA = I.ID_IMPRESA AND SI.ID_SEDE = " & CLng(Session("Creator"))

	set rsCPI = CC.Execute(sSQL)

	if not rsCPI.EOF then
		sDescCPI = rsCPI("Descrizione")
		PRV = rsCPI("PRV")
	else
		sDescCPI = " - "
	end if
	rsCPI.Close
	set rsCPI = nothing

	sSQL = "SELECT COGNOME,NOME FROM UTENTE WHERE IDUTENTE = " & CLng(Session("idutente"))
	
	SSQL = TransformPLSQLToTSQL (SSQL) 
	set rsDatiOp = CC.Execute(sSQL)

	if not rsDatiOp.eof then
		sNome	= rsDatiOp("NOME") & "  " & rsDatiOp("COGNOME")
	else
		sNome	= ""
	end if
	rsDatiOp.Close
	set rsDatiOp = nothing
%>
<form name="frmDatiSede" method="post">

	<table cellpadding="2" cellspacing="2" width="488" border="0">
		<tr>
			<td width="30%" class="tbltext1" style:"text-align:left;"><b>Centro de atenci&oacute;n:</b></td>
			<td width="70%" class="textblack" style:"text-align:left;"><b><%=sDescCPI%></b></td>
		</tr>
		<tr>
			<td width="20%" class="tbltext1" style:"text-align:left;"><b>Funcionario:</b></td>
			<td width="70%" class="textblack" style:"text-align:left;"><b><%=sNome%></td>
		</tr>
		<tr height="2">
			<td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
		<tr>
			<input type="hidden" name="CPI" value="<%=sDescCPI%>">
			<input type="hidden" name="txtProvincia" value="<%=PRV%>">
		</tr>
	</table>

	</br>


	<table cellpadding="2" cellspacing="2" width="488" border="0">
	<% 
	 	Sql = "SELECT DESFUNZIONE,WEBPATHFUN, GRUPPOFUNZIONE FROM FUNZIONE "
	 	Sql = Sql & " WHERE GRUPPOFUNZIONE LIKE 'REPORTS%' ORDER by GRUPPOFUNZIONE, DESFUNZIONE"

		 set rsFunStat = CC.Execute(Sql)
		 i=0
		 Titolo = ""
		 if not rsFunStat.EOF then
			do while not rsFunStat.EOF
				if trim(Titolo) <> trim(rsFunStat("GRUPPOFUNZIONE")) then 
					Titolo=rsFunStat("GRUPPOFUNZIONE") %>
					<tr style="text-align:center;">
						<td>
							<b class="tbltext1"></br><%=replace(MkTit(Titolo),"REPORTS", "REPORTES")%></b>
						</td>
					</tr>
				<% end if %>		
					<tr>
						<td style:"text-align:left;" align="left">
							<input type="hidden" name="sNamPag" value="<%=rsFunStat("WEBPATHFUN")%>" />

							<a onmouseover="javascript:status=''; return true" href="Javascript:VisReport(<%=i%>)" ID="imgPunto1" name="imgPunto1" >
								<img border="0" src="<%=Session("Progetto")%>/images/bullet1.gif" />
								<b class="tbltext1"><%=rsFunStat("DESFUNZIONE")%></b>
							</a>
						</td>
					</tr>
				<%  i=i+1
				rsFunStat.MoveNext
			loop
		else %>
			<h2>No se encontraron reportes creados.</h2> 
		<% end if
		rsFunStat.close
		set rsFunStat = nothing 
	%>
	</table>
</form>	
<%sub inizio()%>
  <table border="0" width="520" cellspacing="0" cellpadding="0" height="81">
    <tr>
      <td width="520" background="<%=Session("Progetto")%>/images/titoli/strumenti2b.gif" height="81" valign="bottom" align="right">
            <b class="tbltext1a">REPORTES POR CENTRO</b></br></br>
      </td>
    </tr>
  </table>
<%
end sub

Function MkTit(Tl)
	for ct=1 to 30 - cint(len(Tl)/2): sep = sep & "_" : next
	MkTit = sep & " " & Tl & " " & sep
End Function
%>
<!--#include Virtual ="/Include/closeconn.asp"-->
<!--#include Virtual="/strutt_coda2.asp"-->