<% IF session("progetto") <> "" THEN

dim provdep
if ucase(session("PROGETTO"))="/ARITES" THEN 
	provdep = "Provincia" 
else  
	provdep = "Departamento" 
end if
%>

<!--#include Virtual = "/include/openconn.asp"-->
<!--#include Virtual = "/include/DecCod.asp"-->
<!--#include virtual="/include/ElabRegole.asp" -->
<!--#include virtual="/include/SelezioneProvince.asp" -->
<!--#include virtual = "/include/SelBandi.asp"-->

<html>
	<head>
		<title>Empresas Inscriptas por <%=provdep%> </title>
		<link REL="STYLESHEET" TYPE="text/css" HREF="<%=session("progetto")%>/fogliostile.css">
		<script language="javascript" src="../Util.js"></script>
	</head>
<body topmargin="0" onafterprint="javascript:buttom.style.visibility='visible';" onbeforeprint="javascript:buttom.style.visibility='hidden'">
<%

dim Sql, sSql, sSql1, sConta, sDati
dim RR, rsProv, rsRegio, rsImpresa, rsDatiAz

inizio()

titolo = "Informe distribución de sedes empresas por " & provdep

if Session("Creator") <> 0 then
		sql= "SELECT PRV FROM SEDE_IMPRESA SI, " &_
				"IMPRESA I WHERE I.ID_IMPRESA = SI.ID_IMPRESA " &_
				" AND SI.ID_SEDE=" &_
				Session("Creator") & " AND I.COD_TIMPR IN (select CODICE from TADES WHERE NOME_TABELLA = 'TIMPR' AND VALORE LIKE 'OP%')" 
				
		set RR = server.CreateObject("ADODB.recordset")
		RR.Open Sql,CC,3
				
		if RR.eof then
			bCImpiego = false
			sPRVCPI = ""
		else
			sPRVCPI = RR.Fields("PRV")
			bCImpiego = true
		end if
		RR.close
	else
		bCImpiego = false
	end if

'if bCImpiego then
'
'	
	if trim(Request.QueryString("sAmb"))="" then
		Ambito = FiltroCPI(true)
	else 
		Ambito = Request.QueryString("sAmb")
	end if	
		
	CodProv = Request.QueryString("sProv")
	
'	ValProv = DecodTadesToArray("PROV",date,"CODICE='" & CodProv & "'",0,0)
'	sDescProv = ValProv(0,1,0)
'	Desc_Val=ValProv(0,1,1)
'	sDescRegio = DecCodVal("REGIO",0,date,Desc_Val,0)
'else
	titolo = titolo & " en el Territorio Nacional"
'end if
	select case(Ambito)
		case("")
			titolo = titolo & " "
		case("x")
			titolo = titolo & "<br>(el filtro requerido por " & Request.QueryString("CPI") & " no ha sido utilizado)" 
		case(0)
			titolo = titolo & "(el filtro requerido por " & Request.QueryString("CPI") & " no ha sido utilizado)" 
		case(1)
			'titolo = titolo & " en el Teritorio National "
		case(2)
			titolo = titolo & "(el filtro requerido por departamento no ha sido utilizado)" 
		case(3)
			titolo = titolo & "(el filtro requerido por Region no ha sido utilizado)" 
	end select
	
	if trim(Request.QueryString("sBan"))<>"" then
		titolo = titolo & "<br> - Inscriptas a : " & DecBando(Request.QueryString("sBan"))
	end if
	
	Condizione=""			
	if trim(Request.QueryString("sBan"))<>"" then
		Condizione = Condizione & " AND I.ID_IMPRESA IN (SELECT ID_IMPRESA FROM DOMANDA_ISCR_IMP WHERE ID_BANDO='" & Request.QueryString("sBan") & "') "
	end if 
%>

<center>
<table cellpadding="0" cellspacing="0" width="488" border="0">
	<tr height="18">
		<td class="sfondomenu" height="18" width="65%">  
		<span class="tbltext0"><b>&nbsp;EMPRESAS INSCRIPTAS POR <%=ucase(provdep)%> </b></span></td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
		<td valign="middle" align="right" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1"></td>
	</tr>
	<tr>
		<td width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif"></td>     
	</tr>
	<tr width="371" class="SFONDOCOMM">
		<td colspan="3">
			<%=titolo%>
		</td>
	</tr>
	<tr height="2">
		<td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif">
		</td>
	</tr>
</table>
<br>

<table>
	<tr width="500">
		<td width="500" class="textred" align="center"><b>&nbsp;situación al : <%=date%></strong></td> 
	</tr>
</table>	
<br>

<table WIDTH="400" ALIGN="center" BORDER="0" CELLSPACING="0" CELLPADDING="0">
	
<%

sFileName = Server.MapPath("/") & session("progetto") & "/DocPers/Statistiche/" &_
	Session("IdUtente") & "_AZI.xls"

sFileNameJS = session("progetto") & "/DocPers/Statistiche/" &_
				Session("IdUtente") & "_AZI.xls"

set oFileSystemObject	= Server.CreateObject("Scripting.FileSystemObject")
set oFileObj = oFileSystemObject.CreateTextFile(sFileName)
' ********************************
' Scrivo la intestazione del file.
' ********************************
oFileObj.WriteLine ("SEDES EMPRESAS INSCRIPTAS POR " & ucase(provdep) & chr(9) & "Situacion al : " & chr(9) & date())
oFileObj.WriteLine ("")
oFileObj.WriteLine (titolo)
oFileObj.WriteLine ("")
oFileObj.WriteLine ("")
sReport = "NO"

set rsRegio = Server.CreateObject("ADODB.recordset")
set rsProv = Server.CreateObject("ADODB.recordset")
set rsImpresa = Server.CreateObject("ADODB.recordset")
set rsDatiAz = Server.CreateObject("ADODB.recordset")

	sSQL =" SELECT CODICE, DESCRIZIONE FROM tades WHERE nome_tabella = 'REGIO' ORDER BY DESCRIZIONE"
	
	rsRegio.Open sSql,CC,3
	'oFileObj.WriteLine ("REGIONE" & chr(9) )
	oFileObj.WriteLine ()
    oFileObj.WriteLine ("DEPARTAMENTO" & chr(9) & ucase(provdep) & chr(9) & "NUMERO SEDES" & chr(9) &  "RAGIONE SOCIALE e SEDE IMPRESA" & chr(9) & "FORMA GIURIDICA" & chr(9) & "PART. IVA / COD. FISC.")
	oFileObj.WriteLine ()
	
		if not rsRegio.EOF then
		   		
			sReport = "OK"
			'oFileObj.WriteLine ("REGIONE" & chr(9) & )
		    'oFileObj.WriteLine ()
		    
		end if
	
		do while not rsRegio.EOF
	
			  sDescrRegio = rsRegio("DESCRIZIONE")
			  oFileObj.WriteLine ()
			  oFileObj.WriteLine (rsRegio("DESCRIZIONE") )
				
				somma= 0 'azzerare eventale somma
			  %>
			  <tr>
				<td COLSPAN="2">&nbsp;</td>
			  </tr>

			  <tr>
			  <!--td COLSPAN="2" CLASS="SFONDOCOMM"><b><%=provdep%> : <%=sDescrRegio%></b-->
			  </td>
			  </tr>
			  
			  <tr CLASS="tblsfondo">
				<td CLASS="tbltext1"><b><%=provdep%></b></td>
				<td CLASS="tbltext1" align="center"><b>Numero de Empresas</b></td> 
			  </tr>
			  
			  <%
			  
			  'oFileObj.WriteLine (chr(9) & "Provincia" & chr(9) & chr(9) &  " Numero Aziende")
			  
			  
			  sSql1 =" SELECT CODICE, DESCRIZIONE FROM tades WHERE nome_tabella = 'PROV' " &_
					" AND VALORE= '" & rsRegio("CODICE") &"'" &_
					" ORDER BY DESCRIZIONE"
			
			 rsProv.Open sSql1,CC,3
			 			 
	 		 if not rsProv.EOF then
				do while not rsProv.EOF
					sConta =" SELECT COUNT(ID_IMPRESA) as numImprese FROM IMPRESA I" &_
						  " WHERE COD_TIMPR IN (select CODICE from TADES WHERE NOME_TABELLA = 'TIMPR' AND VALORE LIKE 'AZ%') and " &_
						  " ID_IMPRESA IN (select distinct(SI.ID_IMPRESA) " &_  
						  " FROM SEDE_IMPRESA SI WHERE SI.PRV = '" & rsProv("CODICE") & "')" & Condizione
				
				'Response.Write sSql
				'Response.Write sConta
				rsImpresa.Open sConta,CC,3
				oFileObj.WriteLine ()
				oFileObj.WriteLine (chr(9) & rsProv("DESCRIZIONE") & chr(9) & cint(rsImpresa("numImprese")))
				
			
				if not rsImpresa.EOF then 
			 
					IF cint(rsImpresa("numImprese")) > 0 THEN
				
						 sDati = "SELECT I.RAG_SOC, I.COD_FISC, I.COD_FISC, I.COD_FORMA, " &_
						  		" SI.DESCRIZIONE " &_
						   		" FROM IMPRESA I, SEDE_IMPRESA SI " &_
		 						" WHERE COD_TIMPR IN (select CODICE from TADES WHERE NOME_TABELLA = 'TIMPR' AND VALORE LIKE 'AZ%') AND I.ID_IMPRESA = SI.ID_IMPRESA " &_
		 						" AND  SI.PRV = '" & rsProv("CODICE")  & "' " & Condizione & " ORDER BY RAG_SOC "
								 
					'	 Response.Write sSql
						 rsDatiAz.Open sDati,CC,3
				
						do while not rsDatiAz.EOF
				
							'decodTadesToArrayString(sTabella,dData,sCondizione,nOrder,Isa)
							if trim(rsDatiAz("COD_FORMA")) <> "" then sCondTades = "CODICE = '" & rsDatiAz("COD_FORMA") & "'"
							sApTades = decodTadesToArrayString("FGIUR", Date, sCondTades, 0, "0")
							sAppoDescr = split(sApTades,"|")
							sDescr = sAppoDescr(1)
				
							if trim(rsDatiAz("COD_FISC")) > "" then
								sPIvaCodFisc = rsDatiAz("COD_FISC")
							else 
								if trim(rsDatiAz("cod_fisc")) > "" then
									sPIvaCodFisc = rsDatiAz("cod_fisc")
								end if
							end if		
				
							oFileObj.WriteLine (chr(9) & chr(9) & chr(9) &  rsDatiAz("RAG_SOC") & " - " & rsDatiAz("DESCRIZIONE") & chr(9) & sDescr & chr(9) & sPIvaCodFisc)
							rsDatiAz.MoveNext
						loop
						rsDatiAz.Close
				
					END IF 
			 end if 
			  %>
			  <tr>
				<td CLASS="tbltext1"> <%=rsProv("DESCRIZIONE")%></td>
				<td CLASS="tbltext1" align="center"> <%=rsImpresa("numImprese")%></td>
			  </tr>
			  <%
		
			  somma = somma + cint(rsImpresa("numImprese")) 
		
			rsImpresa.Close 
			rsProv.MoveNext 
		loop
		
		oFileObj.WriteLine ()
  	    oFileObj.WriteLine (chr(9) & chr(9) & "Total por region : " & somma)
	END IF
			rsProv.Close
			%>
			<tr CLASS="tblsfondo">
			  <td CLASS="tbltext1" align="left"><b>Total:&nbsp;&nbsp;&nbsp; </b></td>
			  <td CLASS="tbltext1" align="center"> <%=somma%></td>
			</tr>  
			<%
			
			rsRegio.MoveNext 
		
		LOOP	
	
	rsRegio.Close
	set rsRegio = nothing 
	set rsProv = nothing
	oFileObj.Close
	set oFileObj = nothing
	set oFileSystemObject = nothing
	
	%>
</table>

<%

Fine()

' *************************************************************************************
sub inizio()
%>
  <center>
  <table border="0" width="500" cellspacing="0" cellpadding="0" height="81">
    <tr>
      <td width="500" background="<%=Session("Progetto")%>/images/titoli/strumenti2b.gif" height="81" valign="bottom" align="right">
        <table border="0" background width="260" height="30" cellspacing="0" cellpadding="0">
          <tr>
            <td width="100%" valign="top" nowrap align="right"><b class="tbltext1a">ESTADISTICAS</span></b></td>
          </tr>
        </table>
      <td>
    </tr>
  </table> 
  </center>

<%
end sub
sub Fine()
%>
	<br><br>
	<label id="buttom">
	<table width="460" cellspacing="2" cellpadding="1" border="0" align="center">
		<tr align="center">
			<td align="middle" colspan="2">
				<a href="javascript:self.close()"><img src="<%=Session("progetto")%>/images/chiudi.gif" title="Cerrar la pagina" border="0" align="absBottom"></a>
				<a href="javascript:print()"><input type="image" src="<%=Session("progetto")%>/images/stampa.gif" title="Imprinir la pagina" border="0" align="center" onclick="self.print();" id="stampa" name="stampa"></a>
			</td>
			<td>&nbsp; </td>
		</tr>
		<tr>	
			<td align="middle" colspan="2">
				<a class="textred" href="javascript:Scarica('<%=sFileNameJS%>')"><b>Abrir reporte</b></a>
			</td>
		</tr>
	</table>
	</label>
<%end sub
' ************************************************************************************
%>

</body>
</html>
<!--#include Virtual = "/include/CloseConn.asp"-->

<%ELSE%>
	<script>
		alert("Sessione scaduta")
		self.close()
	</script>
<%END IF%>
