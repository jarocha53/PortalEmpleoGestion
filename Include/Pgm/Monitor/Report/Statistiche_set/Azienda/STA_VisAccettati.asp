<% IF session("progetto") <> "" THEN

dim provdep
%>

<!--#include Virtual = "/include/openconn.asp"-->
<!--#include Virtual = "/include/DecCod.asp"-->
<!--#include virtual="/include/ElabRegole.asp" -->
<!--#include virtual="/include/SelezioneProvince.asp" -->
<!--#include virtual = "/include/SelBandi.asp"-->

<html>
	<head>
		<title>Personas Aceptadas por Empresas que deben actualizar la situaci�n laboral actual </title>
		<link REL="STYLESHEET" TYPE="text/css" HREF="<%=session("progetto")%>/fogliostile.css">
		<script language="javascript" src="../Util.js"></script>
	</head>
<body topmargin="0" onafterprint="javascript:buttom.style.visibility='visible';" onbeforeprint="javascript:buttom.style.visibility='hidden'">
<%

dim Sql, sSql, sSql1, sConta, sDati
dim RR, rsProv, rsRegio, rsImpresa, rsDatiAz

inizio()

titolo = "Lista de las personas aceptadas por empresas que deben actualizar la situaci�n laboral actual "

if Session("Creator") <> 0 then
		sql= "SELECT PRV FROM SEDE_IMPRESA SI, " &_
				"IMPRESA I WHERE I.ID_IMPRESA = SI.ID_IMPRESA " &_
				" AND SI.ID_SEDE=" &_
				Session("Creator") & " AND I.COD_TIMPR IN (select CODICE from TADES WHERE NOME_TABELLA = 'TIMPR' AND VALORE LIKE 'OP%')" 
				
		set RR = server.CreateObject("ADODB.recordset")
		RR.Open Sql,CC,3
				
		if RR.eof then
			bCImpiego = false
			sPRVCPI = ""
		else
			sPRVCPI = RR.Fields("PRV")
			bCImpiego = true
		end if
		RR.close
	else
		bCImpiego = false
	end if

'if bCImpiego then
'
'	
	if trim(Request.QueryString("sAmb"))="" then
		Ambito = FiltroCPI(true)
	else 
		Ambito = Request.QueryString("sAmb")
	end if	
		
	CodProv = Request.QueryString("sProv")
	
	ValProv = DecodTadesToArray("PROV",date,"CODICE='" & CodProv & "'",0,0)
	sDescProv = ValProv(0,1,0)
	Desc_Val=ValProv(0,1,1)
	sDescRegio = DecCodVal("REGIO",0,date,Desc_Val,0)
'else
	'titolo = titolo & " en el Territorio Nacional"
'end if
	select case(Ambito)
		case("")
			titolo = titolo & " "
		case("x")
			titolo = titolo & "a: " & Request.QueryString("CPI") 
		case(0)
			titolo = titolo & "a: " & Request.QueryString("CPI") 
		case(1)
			titolo = titolo & "en el Teritorio Nacional  "
		case(2)
			titolo = titolo & "en el departamento de " & sDescProv
		case(3)
			titolo = titolo & "en la Region de " & sDescRegio
	end select
	
	if trim(Request.QueryString("sBan"))<>"" then
		titolo = titolo & "<br> - Inscriptas a : " & DecBando(Request.QueryString("sBan"))
	end if
	
	Condizione=""			
	if trim(Request.QueryString("sBan"))<>"" then
		Condizione = Condizione & " AND impresa.ID_IMPRESA IN (SELECT ID_IMPRESA FROM DOMANDA_ISCR_IMP WHERE ID_BANDO='" & Request.QueryString("sBan") & "') "
	end if 
	
	select case(ambito)
		case(0)
			'centro inpiego
			Condizione = Condizione & " AND EXISTS (SELECT ID_IMPRESA FROM SEDE_IMPRESA si " & _
						 "WHERE impresa.ID_IMPRESA = si.ID_IMPRESA AND ID_CIMPIEGO=" & session("creator") & ")"
		case(1)
			'territorio
			'Condizione = Condizione & " AND EXISTS (SELECT ID_IMPRESA FROM SEDE_IMPRESA si " & _
			'			 "WHERE I.ID_IMPRESA = si.ID_IMPRESA AND ID_CIMPIEGO=" & session("creator") & ")"
		case(2)
			'provincia
			Condizione = Condizione & " AND EXISTS (SELECT ID_IMPRESA FROM SEDE_IMPRESA si " & _
						 "WHERE impresa.ID_IMPRESA = si.ID_IMPRESA AND SI.PRV='" & sProv & "')"
	   case(3)
			'regione
			Condizione = Condizione & " AND EXISTS (SELECT ID_IMPRESA FROM SEDE_IMPRESA si " & _
						 "WHERE impresa.ID_IMPRESA = si.ID_IMPRESA AND SI.PRV='" & sProv & "')"
	end select		

%>

<center>
<table cellpadding="0" cellspacing="0" width="488" border="0">
	<tr height="18">
		<td class="sfondomenu" height="18" width="65%">  
		<span class="tbltext0"><b>&nbsp;PERSONAS ACEPTADAS POR EMPRESAS QUE NO HAN ACTUALIZADO LA SITUACION LABORAL ACTUAL</b></span></td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
		<td valign="middle" align="right" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1"></td>
	</tr>
	<tr>
		<td width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif"></td>     
	</tr>
	<tr width="371" class="SFONDOCOMM">
		<td colspan="3">
			<%=titolo%>
		</td>
	</tr>
	<tr height="2">
		<td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif">
		</td>
	</tr>
</table>
<br>

<table>
	<tr width="500">
		<td width="500" class="textred" align="center"><b>&nbsp;situaci�n al : <%=date%></strong></td> 
	</tr>
</table>	
<br>
<%

			set	rstable=Server.CreateObject("ADODB.Recordset")

sqltable=	"select distinct p.nome as Nombre, p.cognome as Apellido, p.cod_fisc as CI, convert(nvarchar(10),rc.dt_ins,103) as 'Aceptado el', i.rag_soc as Empresa, si.Descrizione as Sede, rc.id_richiesta as 'Para la Referencia' " &_
			"from persona p inner join stato_occupazionale so on p.id_persona = so.id_persona " &_
			"inner join RICHIESTA_CANDIDATO rc on p.id_persona = rc.id_persona " &_
			" inner join richiesta_sede rs on rc.id_richiesta = rs.id_richiesta " &_
			" inner join sede_impresa si on rs.id_sede = si.id_sede " &_
			" inner join impresa i on si.id_impresa = i.id_impresa " &_
			"where rc.COD_ESITO_SEL = '09' " &_
			"and not exists (select dt_dich_status from stato_occupazionale sso where sso.id_persona = p.id_persona and rc.dt_ins < sso.dt_dich_status ) " &_
			Condizione & _
			" ORDER BY 4, Empresa, Sede, Nombre " 
	
'PL-SQL * T-SQL  
SQLTABLE = TransformPLSQLToTSQL (SQLTABLE) 
			rstable.open sqltable, CC, 3
			n = 1
%>
	<TABLE width="100%" border="0" cellspacing="2" cellpadding="1">			
<%
		If rstable.eof Then
%>
		<tr class="tblsfondo"> 
			<TD>
				<b class="tbltext1">No hay registros presentes</b>
			</TD>
		</tr>
														 
<%		else %>
		<tr class="sfondocomm"> 
<%			For each tupla in rstable.Fields %>
				<td class="tbltext" width="200"> 
					<b><%=tupla.Name %></b>
				</td>
<%			Next %>			
			</tr>
			
<%		Do while not rstable.EOF %>
			 <tr class="tblsfondo"> 
<%				for each tupla in rstable.Fields %>
				<td class="tbltext" width="200">
					<%=rstable(tupla.name)%>
				</td>
<%				next %>
		     </tr>
<%
			rstable.MoveNext
			loop
%>
	</TABLE>
<%
		end if
		  rstable.Close 
' *************************************************************************************
sub inizio()
%>
  <center>
  <table border="0" width="500" cellspacing="0" cellpadding="0" height="81">
    <tr>
      <td width="500" background="<%=Session("Progetto")%>/images/titoli/strumenti2b.gif" height="81" valign="bottom" align="right">
        <table border="0" background width="260" height="30" cellspacing="0" cellpadding="0">
          <tr>
            <td width="100%" valign="top" nowrap align="right"><b class="tbltext1a">ESTADISTICAS</span></b></td>
          </tr>
        </table>
      <td>
    </tr>
  </table> 
  </center>

<%
end sub
sub Fine()
%>
	<br><br>
	<label id="buttom">
	<table width="460" cellspacing="2" cellpadding="1" border="0" align="center">
		<tr align="center">
			<td align="middle" colspan="2">
				<a href="javascript:self.close()"><img src="<%=Session("progetto")%>/images/chiudi.gif" title="Cerrar la pagina" border="0" align="absBottom"></a>
				<a href="javascript:print()"><input type="image" src="<%=Session("progetto")%>/images/stampa.gif" title="Imprinir la pagina" border="0" align="center" onclick="self.print();" id="stampa" name="stampa"></a>
			</td>
			<td>&nbsp; </td>
		</tr>
		<tr>	
			<td align="middle" colspan="2">
				<a class="textred" href="javascript:Scarica('<%=sFileNameJS%>')"><b>Abrir reporte</b></a>
			</td>
		</tr>
	</table>
	</label>
<%end sub
' ************************************************************************************
%>

</body>
</html>
<!--#include Virtual = "/include/CloseConn.asp"-->

<%ELSE%>
	<script>
		alert("Sessione scaduta")
		self.close()
	</script>
<%END IF%>
