<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<% IF session("progetto") <> "" THEN%>

<!--#include Virtual = "/include/openconn.asp"-->
<!--#include Virtual = "/include/DecCod.asp"-->
<!--#include virtual="/include/ElabRegole.asp" -->
<!--#include virtual="/include/SelezioneProvince.asp" -->
<!--#include virtual = "/include/SelBandi.asp"-->

<html>
	<head>
		<title>Empresas Inscriptas en base a la rama de actividad de pertenencia</title>
		<link REL="STYLESHEET" TYPE="text/css" HREF="<%=session("progetto")%>/fogliostile.css">
		<script language="javascript" src="../Util.js"></script>
	</head>
	<body topmargin="0" onafterprint="javascript:buttom.style.visibility='visible';" onbeforeprint="javascript:buttom.style.visibility='hidden'">
<%
dim Sql
dim rsSett

inizio()

titolo =	"Informe distribucion empresas por Rama de actividad " 
	

	if Session("Creator") <> 0 then
			sql= "SELECT PRV FROM SEDE_IMPRESA SI, " &_
					"IMPRESA I WHERE I.ID_IMPRESA = SI.ID_IMPRESA " &_
					" AND SI.ID_SEDE=" &_
					Session("Creator") & "AND I.COD_TIMPR IN (select CODICE from TADES WHERE NOME_TABELLA = 'TIMPR' AND VALORE LIKE 'OP%')" 
					

			set RR = server.CreateObject("ADODB.recordset")
			RR.Open sql,CC,3
					
			if RR.eof then
				bCImpiego = false
				sPRVCPI = ""
			else
				sPRVCPI = RR.Fields("PRV")
				bCImpiego = true
			end if
			RR.close
	else
			bCImpiego = false
	end if

if bCImpiego then
	
	if trim(Request.QueryString("sAmb"))="" then
		Ambito = FiltroCPI(true)
	else 
		Ambito = Request.QueryString("sAmb")
	end if	
	
	sProv = Request.QueryString("sProv")
	
	ValProv = DecodTadesToArray("PROV",date,"CODICE='" & sProv & "'",0,0)
	sDescProv = ValProv(0,1,0)
	Desc_Val=ValProv(0,1,1)
	sDescRegio = DecCodVal("REGIO",0,date,Desc_Val,0)
else
	titolo = titolo & "en el Teritorio Nacional "
end if
	
	select case(Ambito)
		case("")
			titolo = titolo & " "
		case("x")
			titolo = titolo & "a: " & Request.QueryString("CPI") 
		case(0)
			titolo = titolo & "a: " & Request.QueryString("CPI") 
		case(1)
			titolo = titolo & "en el Teritorio Nacional  "
		case(2)
			titolo = titolo & "en el departamento de " & sDescProv
		case(3)
			titolo = titolo & "en la Region de " & sDescRegio
	end select
	
	if trim(Request.QueryString("sBan"))<>"" then
		titolo = titolo & " - Inscriptas a : " & DecBando(Request.QueryString("sBan"))
	end if
%>

<center>
<table cellpadding="0" cellspacing="0" width="488" border="0">
	<tr height="18">
		<td class="sfondomenu" height="18" width="65%">
		<span class="tbltext0"><b>&nbsp;NUMERO DE EMPRESAS POR RAMA DE ACTIVIDAD</b></span></td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
		<td valign="middle" align="right" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1"></td>
	</tr>
	<tr>
		<td width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif"></td>     
	</tr>
	<tr width="371" class="SFONDOCOMM">
		<td colspan="3">
			<%=titolo%>
		</td>
	</tr>
	<tr height="2">
		<td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif">
		</td>
	</tr>
</table>
<br>

<table>
	<tr width="500">
		<td width="500" class="textred" align="center"><b>&nbsp;situaci�n al : <%=date%></strong></td> 
	</tr>
</table>	
<br>

<table border="0" width="488" cellspacing="2" cellpadding="2">

<%
scpi = Request.QueryString("CPI")

sFileName = Server.MapPath("/") & session("progetto") & "/DocPers/Statistiche/" &_
	Session("IdUtente") & "_AZI.xls"

sFileNameJS = session("progetto") & "/DocPers/Statistiche/" &_
				Session("IdUtente") & "_AZI.xls"
	

set oFileSystemObject	= Server.CreateObject("Scripting.FileSystemObject")
set oFileObj = oFileSystemObject.CreateTextFile(sFileName)

' ********************************
' Scrivo la intestazione del file.
' ********************************
oFileObj.WriteLine ("NUMERO DE EMPRESAS POR RAMA DE ACTIVIDAD" & chr(9) & "Situaci�n al : " & chr(9) & date())
oFileObj.WriteLine ("")
oFileObj.WriteLine (titolo)
oFileObj.WriteLine ("")
oFileObj.WriteLine ("")
sReport = "NO"

'17/09/2004 
'
'	Sql =	"SELECT DISTINCT(S.DENOMINAZIONE), COUNT(*) AS NUMAZIENDE " &_
'			"FROM SETTORI S, IMPRESA I " &_
'			"WHERE  I.ID_SETTORE= S.ID_SETTORE" &_
'			" GROUP BY S.DENOMINAZIONE"
					

'	Sql =	"SELECT ID_SETTORE, denominazione " &_
'			" FROM  SETTORI " &_
'			" order BY denominazione " 		

	
		
	Sql =	"SELECT I.ID_SETTORE, S.DENOMINAZIONE, COUNT(*) AS NUMAZIENDE " &_
			"FROM SETTORI S, IMPRESA I " &_
			"WHERE  i.cod_timpr IN (select CODICE from TADES WHERE NOME_TABELLA = 'TIMPR' AND VALORE LIKE 'AZ%') and I.ID_SETTORE= S.ID_SETTORE" 
			
	
	Condizione=""			
	if trim(Request.QueryString("sBan"))<>"" then
		Condizione = Condizione & " AND I.ID_IMPRESA IN (SELECT ID_IMPRESA FROM DOMANDA_ISCR_IMP WHERE ID_BANDO='" & Request.QueryString("sBan") & "') "
	end if 
		

	select case(ambito)
		case(0)
			'centro inpiego
			Condizione = Condizione & " AND EXISTS (SELECT ID_IMPRESA FROM SEDE_IMPRESA si " & _
						 "WHERE I.ID_IMPRESA = si.ID_IMPRESA AND ID_CIMPIEGO=" & session("creator") & ")"
		case(1)
			'territorio
			'Condizione = Condizione & " AND EXISTS (SELECT ID_IMPRESA FROM SEDE_IMPRESA si " & _
			'			 "WHERE I.ID_IMPRESA = si.ID_IMPRESA AND ID_CIMPIEGO=" & session("creator") & ")"
		case(2)
			'provincia
			Condizione = Condizione & " AND EXISTS (SELECT ID_IMPRESA FROM SEDE_IMPRESA si " & _
						 "WHERE I.ID_IMPRESA = si.ID_IMPRESA AND SI.PRV='" & sProv & "')"
	   case(3)
			'regione
			Condizione = Condizione & " AND EXISTS (SELECT ID_IMPRESA FROM SEDE_IMPRESA si " & _
						 "WHERE I.ID_IMPRESA = si.ID_IMPRESA AND SI.PRV='" & sProv & "')"
	end select		
			
			
	SQL = SQL & Condizione & " GROUP BY I.ID_SETTORE, S.DENOMINAZIONE"
	
	'Response.Write sql
				
	set rsSett = server.CreateObject("ADODB.recordset")
	rsSett.Open Sql,cc,3
    						
	'Response.Write "Sql : " & Sql & "<br>********"
	n = 0
	n1= 0
	
	'oFileObj.WriteLine ( "SETTORE" & chr(9) & "NUMERO AZIENDE" & chr(9))
	oFileObj.WriteLine ( "SETTORE" & chr(9) & "RAGIONE SOCIALE" & chr(9) & "FORMA GIURIDICA" & chr(9) & "PART. IVA / COD. FISC" & chr(9) )
	oFileObj.WriteLine ("")
%>
	<tr class="SFONDOCOMM">
		<td width="250" class="tbltext1"><b>Rama de Actividad</b></td>
		<td width="50" class="tbltext1"><b>Numero de empresas</b></td>
	</tr>
<%
	IF not rsSett.eof then
		sReport = "OK"
							
		set rsConta = server.CreateObject("ADODB.recordset")
		do while not rsSett.eof
		
		sConta = "SELECT I.RAG_SOC, I.COD_FISC, I.COD_FISC, I.COD_FORMA " &_
				 "FROM SETTORI S, IMPRESA I " &_
				 "WHERE  I.ID_SETTORE= S.ID_SETTORE AND I.ID_SETTORE = " & rsSett("ID_SETTORE") &_
				 "and cod_timpr IN (select CODICE from TADES WHERE NOME_TABELLA = 'TIMPR' AND VALORE LIKE 'AZ%') " & Condizione


				
			'Response.Write sConta
					
		'	sConta = "SELECT COUNT(*) AS CONTA FROM IMPRESA I " &_
		'			"WHERE I.ID_SETTORE=" & rssett("ID_SETTORE") 
		
				rsConta.Open sConta,cc,3
				
%>
				<tr>
					<td width="250" class="tbltext1">
					<%
						sDenom = rsSett("Denominazione")
						Response.Write sDenom	
					%>
					</td>
					<td width="50" class="tbltext1">
					<%
						'Conta = rsConta("Conta")
						Conta = rsSett("numaziende")
						Response.Write Conta
					%>
					</td>
				</tr>
			<%
			
		'	oFileObj.WriteLine (sDenom & chr(9) & Conta )
		
			IF not rsConta.eof then
				do while not rsConta.eof
				
				'decodTadesToArrayString(sTabella,dData,sCondizione,nOrder,Isa)
				if trim(RsConta("COD_FORMA")) <> "" then sCondTades = "CODICE = '" & RsConta("COD_FORMA") & "'"
				sApTades = decodTadesToArrayString("FGIUR", Date, sCondTades, 0, "0")
				sAppoDescr = split(sApTades,"|")				
				'Response.Write "sCondTades: " & sCondTades & "<br>"
				'Response.Write "sApTades: " & sApTades & "<br>"
				sDescr = sAppoDescr(1)
				
					sRagSoc  = rsConta("rag_soc")
					'sDescr   = rsConta("descrizione")
					if trim(rsConta("COD_FISC")) > "" then
						sPIvaCodFisc = rsConta("COD_FISC")
					else 
						if trim(rsConta("cod_fisc")) > "" then
							sPIvaCodFisc = rsConta("cod_fisc")
						end if
					end if		
						
					oFileObj.WriteLine (sDenom & chr(9) & sRagSoc & chr(9) & sDescr & chr(9) & sPIvaCodFisc)
					
					rsConta.MoveNext
					n1 = n1 + 1
			 loop
			 end if
			 
				rsConta.Close
			
			rsSett.MoveNext
			n = n + 1
			
		loop 
		
	ELSE
		oFileObj.WriteLine (chr(9) & "-" & chr(9) & "0" & chr(9) & "0")
%>
	<!--tr>		<td width="200" class="tbltext1"><b></b></td>		<td width="50" class="tbltext1"><b></b></td>	</tr-->


<%
	END IF
	
	rsSett.Close
	set rsSett = nothing
	set rsConta = nothing

oFileObj.close
set oFileSystemObject = nothing
set oFileObj = nothing
%>

</table>
</center>

<%

Fine()

' *************************************************************************************
sub inizio()
%>
  <center>
  <table border="0" width="500" cellspacing="0" cellpadding="0" height="81">
    <tr>
      <td width="500" background="<%=Session("Progetto")%>/images/titoli/strumenti2b.gif" height="81" valign="bottom" align="right">
        <table border="0" background width="260" height="30" cellspacing="0" cellpadding="0">
          <tr>
            <td width="100%" valign="top" align="right"><b class="tbltext1a">ESTADISTICAS EMPRESAS</span></b></td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  </center>
<%
end sub
' ************************************************************************************

' ************************************************************************************
sub Fine()
%>
	<br><br>
	<label id="buttom">
	<table width="460" cellspacing="2" cellpadding="1" border="0" align="center">
		<tr align="center">
			<td>
				<a href="javascript:self.close()"><img src="<%=Session("progetto")%>/images/chiudi.gif" title="Cierra la pagina" border="0" align="absBottom"></a>
				<a href="javascript:print()"><input type="image" src="<%=Session("progetto")%>/images/stampa.gif" title="Imprime la pagina" border="0" align="center" onclick="self.print();" id="stampa" name="stampa"></a>
			</td>
		</tr>
		<tr><td class="textred">&nbsp; </td></tr>
		<tr align="center">
			<td>
				<a class="textred" href="javascript:Scarica('<%=sFileNameJS%>')"><b>Abrir reporte</b></a>
			</td>
		</tr>
		<tr align="center">
			<td>&nbsp;</td>
		</tr>	
	</table>
	</label>
<%end sub
' ************************************************************************************
%>

</body>
</html>
<!--#include Virtual = "/include/CloseConn.asp"-->

<%ELSE%>
	<script>
		alert("Sessione scaduta")
		self.close()
	</script>
<%END IF%>