<!--#include virtual ="/util/portallib.asp"-->
<!--#include virtual ="/include/openconn.asp"-->
<!--#include virtual ="/Util/DBUtil.asp"-->
<!--#include virtual ="/include/DecCod.asp"-->
<!--#include virtual ="/include/ckProfile.asp"-->

<title>Interrogazione Base Dati</title>
<link REL="STYLESHEET" TYPE="text/css" HREF="<%=Session("Progetto")%>/fogliostile.css">
<script language="Javascript" src="Funtasti.js"></script>
<script language="Javascript" src="ChkQuery.js"></script>
	
<body onload="javascript: document.IForm.txtNome.focus()">
<%

	Sub ImpostaPag()
		Dim sTipo, sSql
		
		sTipo = Request.QueryString("Tipo")
		sSql = Request.QueryString("sql")
		
		Rif = "javascript:Ritorna('"  & sTipo & "')"
		
%>
<br>		
<form method="POST" name="IForm" action="<%=Rif%>" onsubmit="return FormRegistra_Validator(this)">
<table border="0" CELLPADDING="0" CELLSPACING="0" width="450">
	<tr height="17">

		<td class="sfondomenu" width="67%" height="18"><span class="tbltext0">
			<b>&nbsp;SALVATAGGIO QUERY </b>
		</td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">
		</td>
		<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
	</tr>
	<tr>
		<td class="sfondocomm" width="57%" colspan="3">
			Scrivere il nome del file su cui si vuole salvare la query 
				e premere il tasto conferma (indicare il nome 
				senza l'estensione del file) 
		</td>
	</tr>
	<tr height="2">
		<td class="sfondocomm" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif">
		</td>
	</tr>
</table>

<input type="hidden" name="sql" value="<%=trim(sSql)%>">

<br><br>
<table width="450" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td width="150" align="center">
			<span class="tbltext1"><b>Nome file</b> </span>
		</td>
		<td width="300" class="tbltext">
			<input type="text" name="txtNome" border="0" maxlength="20" size="25">
		</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;&nbsp;</td>
	</tr>
</table>
<br><br>
<table width="450px" border="0" cellspacing="0" cellpadding="0">
	<tr align="center">
		<td class="tbltext" align="Right">
			<input name="b" type="image" Title="Conferma" src="<%=Session("Progetto")%>/Images/Conferma.gif">
		</td>
		</form>
		<form name="can" method="post" action="javascript:window.close()">		
		<td align="left">
			<input name="b" type="image" Title="Chiudi" src="<%=Session("Progetto")%>/Images/chiudi.gif">
		</td>
		</form>
	</tr>
</table>
		
<%End Sub

'******************** Main *******************
	Dim strConn
	
	If Not ValidateService(session("idutente"),"SQL_RicReport",cc) Then 
		response.redirect "/util/error_login.asp"
	End If	
	
	ImpostaPag()
%>
	<!--#include virtual ="/include/closeconn.asp"-->
