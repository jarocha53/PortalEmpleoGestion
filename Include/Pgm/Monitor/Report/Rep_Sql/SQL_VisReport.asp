<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include virtual ="/util/portallib.asp"-->
<!--#include Virtual = "/strutt_testa1.asp"-->
<!--#include virtual ="/include/openconn.asp"-->

<script language="Javascript" src="/Include/help.inc"></script>
<script LANGUAGE="JavaScript" src="/Include/FunOpenWindow.js"></script>

<%
	Sub Imposta_Testata(jmax)
		Dim j
		aTestata = Split(STitolo, "," , -1, 1)
		For j = 0 to jmax - 1
%>			<td class="sfondocomm" height="30" align=center width="<%=nWidth%>"> 
				<b><%=aTestata(j)%></b>
			</td>
<%		
		Next
	end Sub
	
Sub Imposta_Pag()
	Dim tabella, sQuery
	Dim iPageSize       'quante righe per pagina si vogliono
	Dim iPageCount      'numero di pagine 
	Dim iPageCurrent    'pagina corrente
	Dim iRecordsShown   'Loop controller
	Dim I               'indice di loop
	
	iPageSize = 8
	
	If Request.QueryString("page") = "" Then
		iPageCurrent = 1
	Else
		iPageCurrent = CInt(Request.QueryString("page"))
	End If
    
    sTabella = Request.Form("cboTabella")
    sTipo = Request.Form("cboTipo")
    If sTipo = "" Then
		sTipo = Request.QueryString("Tipo")
	End If
	    
    sQuery = Request.Form("txtQuery")
    If sQuery = "" then
		sQuery = Request.QueryString("Query")
	End If
	
	If sTabella <> "" or sQuery <> "" Then
		Dim sqlTabDesc
		Dim strConn, sOper, n
		Set rsTabDesc=Server.CreateObject("ADODB.Recordset")
		
		sTabella=uCase(sTabella)		
		
		If sTabella > "" Then
			'prima in oracle era  
			'sqlTabDesc="SELECT DISTINCT TABLE_name, column_name, data_type, data_length, nullable, column_id FROM all_tab_columns WHERE TABLE_name='" & sTabella & "' ORDER BY column_id"
			sqlTabDesc="SELECT DISTINCT TABLE_name, column_name, data_type, character_maximum_length, is_nullable, ordinal_position FROM information_schema.COLUMNS WHERE TABLE_name='" & sTabella & "' ORDER BY ordinal_position"
		Else
			sqlTabDesc= sQuery
			rsTabDesc.PageSize = iPageSize
			rsTabDesc.CacheSize = iPageSize
		End If	
		
		on error resume next	
'PL-SQL * T-SQL  
SQLTABDESC = TransformPLSQLToTSQL (SQLTABDESC) 
		rsTabDesc.open sqlTabDesc, CC, 3
		n = 1
		
		If not Err.number = 0 Then
%>			
			<table BORDER="0" CELLSPACING="2" CELLPADDING="1" align="center" width="745">
				<tr align="center"> 
					<td class="Tbltext3"> 
						<b>Si � verificato un errore durante l'esecuzione della query.
						<br>(<%=Err.description%>)</b>
					</td>
				</tr>
			</table>
			<br><br>
<%			exit sub
		End If

		If rsTabDesc.EOF Then
%>			
			<table BORDER="0" CELLSPACING="2" CELLPADDING="1" align="center" width="745">
				<tr align="center"> 
					<td class="Tbltext3"> 
					  <b>La Tabella richiesta non contiene righe</b>
					</td>
				</tr>
			</table>
			<br><br>											 
<%			exit sub
		End If
			
		If sTipo = "VIDEO" Then
			If sTabella = "" Then
				iPageCount = rsTabDesc.PageCount
			
				If iPageCurrent > iPageCount Then 
					iPageCurrent = iPageCount
				End If
					
				If iPageCurrent < 1 Then 
					iPageCurrent = 1
				End If
			
				rsTabDesc.AbsolutePage = iPageCurrent
			End If
			nColonne = cint(rsTabDesc.Fields.Count)
			nWidth = 745 \ nColonne		
%>			
			<table align="center" cellspacing="2" cellpadding="1" height="2" border="0" width="745">
<%	 			If sTabella = "" Then %>				
				<tr>
					<td class="tbltext" colspan="<%=nColonne%>">
						Pagina <b><%=iPageCurrent%></b> di <b><%=iPageCount%></b>
					</td>
				</tr>
<%				End If	%>				
				<tr align="center"> 
<%					jmax = Request.QueryString("j")
						
					For each tupla in rsTabDesc.Fields
						sTitolo = STitolo &  tupla.Name & ","
						j = j + 1
					Next
						
					If jmax <> "" Then
						j = jmax
					End If
						
					call Imposta_Testata(j)
%>							
				</tr>
			
<%			iRecordsShown = 0
			If sTabella = "" then		
				Do While iRecordsShown < iPageSize And Not rsTabDesc.EOF
%>					
					<tr class="tblsfondo"> 
<%					For each tupla in rsTabDesc.Fields %>						
					<td class="tbltext" width="<%=nWidth%>"><%=rsTabDesc.fields(tupla.name)%></td>
<%					Next  %>				    
					</tr>
<%					
					iRecordsShown = iRecordsShown + 1
					rsTabDesc.MoveNext
				loop
			Else
				Do While not rsTabDesc.EOF
%>					
					<tr class="tblsfondo"> 
<%					For each tupla in rsTabDesc.Fields %>						
						<td class="tbltext" width="<%=nWidth%>"><%=rsTabDesc.fields(tupla.name)%></td>
<%					Next  %>				    
					</tr>
<%					
					iRecordsShown = iRecordsShown + 1
					rsTabDesc.MoveNext
				Loop
			End If
%>			</table>
			<br>
<%			If sTabella = "" Then		'per la struttura delle tabelle non si effettua la paginazione %>
			<table cellspacing="2" cellpadding="1" height="2" align="center" border="0" width="745">
				<tr>
					<td class="tbltext1" align="middle"><b>
<%						If iPageCurrent > 1 Then  %>							
						<a class="tbltext1" href="SQL_VisReport.asp?j=<%=j%>&amp;Tipo=VIDEO&amp;Query=<%=server.URLEncode(sqlTabDesc)%>&amp;page=<%= iPageCurrent - 1 %>" onmouseover="window.status =' '; return true">[&lt;&lt; Prev]</a>
<%						End If
						num = 0
					' paginazione:
						If iPageCount <> 1  then
							num = iPageCurrent \ 10
							num = num * 10
							For I = num + 1 to num + 10
								If I <= iPageCount Then
									If I = iPageCurrent Then
										Response.Write I 
									Else
%>									
									<a class="tbltext1" href="SQL_VisReport.asp?j=<%=j%>&amp;Tipo=VIDEO&amp;Query=<%=server.URLEncode(sqlTabDesc)%>&amp;page=<%= I %>" onmouseover="window.status =' '; return true"><%= I %></a>
<%								
									End If
								End If
							Next 
						End If
	
						If iPageCurrent < iPageCount Then
%>
							<a class="tbltext1" href="SQL_VisReport.asp?j=<%=j%>&amp;Tipo=VIDEO&amp;Query=<%=server.URLEncode(sqlTabDesc)%>&amp;page=<%= iPageCurrent + 1 %>" onmouseover="window.status =' '; return true">[Next &gt;&gt;]</a>
<%						End If %>						
						<br><br>
					</b></td>
				</tr>
			</table>
<%		 	End If
		Else	'tipo = "FILE"
			Response.Flush	
			call CreateFile (sqlTabDesc)				
			sTitolo = ""
			sRec = ""
			For each tupla in rsTabDesc.Fields
			    sTitolo = sTitolo & tupla.Name  & chr(9)
			Next 
			
			WriteRecord(sTitolo)

			Do While Not rsTabDesc.EOF
				For each tupla in rsTabDesc.Fields
					sRec = sRec & rsTabDesc.fields(tupla.name) & chr(9)
				Next
				WriteRecord(sRec)
				sREc = ""
				rsTabDesc.MoveNext
  			Loop
	
		End If
		
		rsTabDesc.Close 
		set rsTabDesc=nothing
		If sTipo = "FILE" Then
			call CloseFile()
		End If
		
	End If
	
End Sub


' creazione file report presenze-assenze e scrittura testata --------------
Sub CreateFile (sqlTabDesc) 
	dim sTesto
	
	sPrj = replace(Session("Progetto"),"/","\")
	sNomeFile = "rptSql_" & Session("idutente") & "_" & day(Now) & month(now())& "_" & Hour(now) & minute(now) & ".xls"
	sPath=Server.MapPath ("\") & sPrj & "\DocPers\Estrattore\"
	on error resume next 
	Set fso = CreateObject("Scripting.FileSystemObject")
	Set fAss = fso.CreateTextFile(sPath & sNomeFile,true)	' creazione file
	If err > 0 Then
		Response.Write "<b class='tbltext'>Non risulta presente la cartella Estrattore </b>"
		Response.End
	End If	
	
'scrittura della testata del report
	fAss.WriteLine "Elaborato il " & day(Now) & "/" & month(now) & "/" & year(now) & " Ore " & hour(now) & ":" & minute(now)
	fAss.WriteLine ""		
	fAss.WriteLine "Report: Interrogazione base dati"
	fAss.WriteLine ""		
	fAss.WriteLine "Interrogazione richiesta: " & ucase(sqlTabDesc)
	fAss.WriteLine ""		
End Sub

' chiusura file e release memoria 
Sub CloseFile () 
	fAss.Close		
	Set fAss = nothing
	Set fso = nothing
End Sub

' sLine = contiene la suddivisione in giorni delle ore di assenza ---------
Sub WriteRecord(Rec)
		fAss.WriteLine Rec 
End Sub
	
Sub Inizio()
%>
	<table border="0" width="743" cellspacing="0" cellpadding="0" align="center" height="70">
		<tr>
			<td width="720" background="<%=session("progetto")%>/images/titoli/servizi1b.gif" height="70" valign="bottom" align="right">
				<table border="0" width="720" height="23" cellspacing="0" cellpadding="0">
					<tr>
						<td width="100%" valign="top" align="right">
							<b CLASS="tbltext1a">Estrattore dati </b>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<br>
	
	<table border="0" CELLPADDING="0" CELLSPACING="0"  align="center" width="743">
	<tr height="17">

		<td class="sfondomenu" width="67%" height="18"><span class="tbltext0">
			<b>&nbsp;ESTRATTORE DATI </b>
		</td>
		<td width="2%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
		<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
	</tr>
	<tr>
		<td class="sfondocomm" width="57%" colspan="3">
			<br>Risultati della ricerca<b></b> 
			<a href="Javascript:Show_Help('/pgm/Help/Monitor/Report/Rep_Sql/SQL_VisReport')">
			<img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0" alt="per maggiori informazioni"></a>
		</td>
	</tr>
	<tr height="2">
		<td class="sfondocomm" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
	</tr>
	</table>
<br>
<%
End sub

'**************  Main **********************

	dim fso			' as object - variabile globale usata in CreateFile
	dim	fAss		' as file - variabile globale usata in CreateFile
	dim sNomeFile	' as string - variabile globale usata in CreateFile
	dim sPath		' as string - variabile globale usata in CreateFile
	Dim sTitolo
	Dim sRec
	Dim nWidth
	
	If Not ValidateService(session("idutente"),"SQL_RicReport",cc) Then 
		response.redirect "/util/error_login.asp"
	End If
	
	Inizio()
	Imposta_Pag()		 
	 
	If sNomeFile <> "" Then
		sPath = Session("Progetto")& "/DocPers/Estrattore/" & sNomeFile
%>		
		<br>
		<table align="center" width="745" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td align="middle" colSpan="3" class="tbltext3">
					<b class="size">Il report � stato creato correttamente.</b>
				</td>
			</tr>
		</table>
		<br><br><br>
		<table align="center" width="745" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td align="middle">
					<a class="textred" href="javascript:ScaricaReport('<%=sPath%>')" onmouseover="window.status =' '; return true"><b>Apri il Report</a></b>&nbsp; 
				</td>
				<td align="middle">
				<a class="textred" href="SQL_RicReport.asp" onmouseover="window.status =' '; return true"><b>Nuova interrogazione</a></b> 
			</td>
			</tr>
		</table>
		<br><br>
<%	 Else %>		
        <table width="745" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td align="middle">
					<a class="textred" href="SQL_RicReport.asp" onmouseover="window.status =' '; return true"><b>Nuova interrogazione</a></b> 
				</td>
			</tr>
		</table>
		<br><br>	
<%	End If %>
<!--#include virtual ="/include/closeconn.asp"-->

