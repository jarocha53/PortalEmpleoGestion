<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%

sFilePath = SERVER.MapPath("\") & session("Progetto") &_
	 "\DocPers\Interrogazioni\" & session("LOGIN") & "_Interrogazioni.txt"

' ***********************************************
' Elimino una riga del file
' ***********************************************

function CancRiga(aFile,sNomeTab,sPrgReg,sCampo)

	if not IsArray (aFile) then
		aFile = ReadFile()
	end if

	set oFileSystemObject = Server.CreateObject("Scripting.FileSystemObject")
	set oFileCurriculum = oFileSystemObject.OpenTextFile(sFilePath,2)

	nRowDel = 0

if isarray(aFile) then
	for t = 0 to UBound(aFile)

		aRiga = Split(aFile(t),"#")

		if ( aRiga(0) = sNomeTab ) and ( aRiga(4) = sPrgReg or aRiga(5) = sPrgReg ) and ( sCampo = "0" or aRiga(1) = sCampo ) then

			nRowDel = nRowDel + 1

		else
			oFileCurriculum.WriteLine( aRiga(0) &_
				"#" & aRiga(1) & "#" & aRiga(2) & "#" &_
				 aRiga(3) & "#" & aRiga(4) & "#" & aRiga(5))

		end if

	next
end if

	oFileCurriculum.close
	set oFileSystemObject = nothing
	set oFileCurriculum = nothing

	CancRiga = nRowDel

end function


' ***********************************************
' Mi deve ritornare il progressivo regola da
' indicare come riferimento.
' ***********************************************

function ProgRegRif(aFile,sNomeTab,sNomeCampo,sValore)

	dim aRiga

	if not IsArray (aFile) then
		aFile = ReadFile()
	end if
	'Response.Write ariga(5)

	for t = 0 to UBound(aFile)
		aRiga = Split(aFile(t),"#")

		'Response.Write "<br> -- " & ubound(aRiga)

		if ( aRiga(0) = sNomeTab ) and ( aRiga(1) = sNomeCampo ) and ( clng(aRiga(5)) = 0 ) and ( sValore = aRiga(3) )then
			ProgRegRif = aRiga(4)
		end if
	next

end function


' ***********************************************
' Mi deve ritornare il progressivo regola pi
' grande per un nuovo record.
' ***********************************************

function MaxProgReg()
	dim aRiga,nMax

	aFile = ReadFile()

	nMax = 0
	if IsArray(aFile) then
		for t = 0 to UBound(aFile)
			aRiga = Split(aFile(t),"#")
			if nMax <= aRiga(4) then
				nMax = aRiga(4)
			end if
		next
	end if
	MaxProgReg = nMax

end function


' ***********************************************
' La funzione controlla se il filtro che si
' vuole aggiungere non sia gi stato inserito
' Ritorna True o False.
' ***********************************************

function ValidFilter(aFile,sNomeTab,sNomeCampo,sValore,sOpLog)
	dim aRiga, t

	ValidFilter = 0
	nContaRighe = 0
	for t = 0 to UBound(aFile)
		aRiga = Split(aFile(t),"#")

		if aRiga(0) = sNomeTab then

			if aRiga(1) = sNomeCampo then

				if (aRiga(2) = sOpLog) or IsNull(sOpLog) then
					if aRiga(3) = sValore then
						nContaRighe = nContaRighe + 1
					end if

				end if

			end if

		end if

	next
	ValidFilter = nContaRighe

end function


' ***********************************************
' Legge il contenuto del file e ritorna un array.
' Ogni elemento dell'Array rappresenta una riga
' di un file.
' *******************************************************************************
' Ogni elemento dell'Array aFile  cos composto
' *******************************************************************************
'    0     #   1   #   2   #   3    #        4          #         5
' [Tabella]#[Campo]#[OpLog]#[Valore]#[Progessivo Regola]#[Progessivo Regola Rif.]
' *******************************************************************************

function ReadFile()
    dim cont
	dim aFile
	dim sFile

	ReadFile = aFile
    cont = 0
'	sFilePath = SERVER.MapPath("\") & session("Progetto") &_
'	 "\Conf\prova.txt"

	set oFileSystemObject = Server.CreateObject("Scripting.FileSystemObject")
	set oFileCurriculum = oFileSystemObject.OpenTextFile(sFilePath,1)

	'Response.Write sFilePath
	Do  While not oFileCurriculum.AtEndOfStream

		sFile = sFile & oFileCurriculum.ReadLine

		if not oFileCurriculum.AtEndOfStream then
			sFile = sFile & "##"
		end if
        cont = 1
	Loop

	aFile = split(sFile,"##")

	oFileCurriculum.close

	set oFileSystemObject = nothing
	set oFileCurriculum = nothing
	if UBound(aFile)  <> -1 then

		ReadFile = aFile

	end if
	'if cont = 1 then
	'  ReadFile = true
	'end if
end function

sub GestFile()
	' Questa funzione deve controllare
	' se gi esiste un file con l'ID utente
	' quindi azzerarlo, oppure crearlo.
	' Il tipo di file deve essere IDUTENTE.TXT.

'	sFilePath = SERVER.MapPath("\") & session("Progetto") &_
'		 "\Conf\prova.txt"

		'RESPONSE.Write sFilePath
		'response.End
	set oFileSystemObject = Server.CreateObject("Scripting.FileSystemObject")

	if oFileSystemObject.FileExists(sFilePath) then
		set oFileCurriculum = oFileSystemObject.OpenTextFile(sFilePath,2)
'			Se il file esiste pulisco il contenuto
	else
		set oFileCurriculum = oFileSystemObject.CreateTextFile(sFilePath,true)
'			Se il file non esiste lo creo vuoto
	end if

	oFileCurriculum.close

	set oFileSystemObject = nothing
	set oFileCurriculum = nothing
End Sub

' ***********************************************
' Legge il contenuto del file e ritorna un array.
' Ogni elemento dell'Array rappresenta una riga
' di un file.
' ***********************************************

function ReadFileWithInputTab(sTab)

	dim aFile
	dim sFile

	ReadFileWithInputTab = false

'	sFilePath = SERVER.MapPath("\") & session("Progetto") &_
'		 "\Conf\prova.txt"
'	Response.Write "<BR>sFilePath = " & sFilePath

	set oFileSystemObject = Server.CreateObject("Scripting.FileSystemObject")
	set oFileCurriculum = oFileSystemObject.OpenTextFile(sFilePath,1)

	Do While not oFileCurriculum.AtEndOfStream

		sFileTemp = oFileCurriculum.ReadLine
		if sFileTemp <> "" then
			aFileTemp = Split(sFileTemp,"#")
		end if

		if isarray(aFileTemp) then
			if aFileTemp(0) = sTab then

				sFile = sFile & sFileTemp

				if not oFileCurriculum.AtEndOfStream then
					sFile = sFile & "##"
				end if

			end if
		end if

	Loop

	if Right (sFile,2) = "##" then
		sFile = left(sFile,Len(sFile)-2)
	end if

	aFile = split(sFile,"##")
	oFileCurriculum.close

	set oFileSystemObject = nothing
	set oFileCurriculum = nothing

	if UBound(aFile)  <> -1 then

		ReadFileWithInputTab = aFile

	end if

end function

function OrderFile(sTab)



	dim aFile
	dim aFileOrder()


	if sTab <> "" then
		aFile = ReadFileWithInputTab(sTab)
	else
		aFile = ReadFile()
	end if

	if 	not IsArray(aFile) then

		aFile = ""

		exit function

	end if

	redim aFileOrder(UBound(aFile))

	sTempTab  = ""
	nCountIns = 0

	for yy = 0 to UBound(aFile)

		aRiga = split(aFile(yy),"#")

		if InStr(sTempTab,aRiga(0)) = 0  then

			sTempTab	= sTempTab & aRiga(0)
			aFileTemp	= ReadFileWithInputTab(aRiga(0))

			do while 0 = 0
				n = -1
				for ee = 0 to UBound(aFileTemp)
					if aFileTemp(ee) = "" then
						n = n + 1
					end if
					if n = UBound(aFileTemp) then
						exit do
					end if
				next

				for ee = 0 to UBound(aFileTemp)



					aTempRiga = split(aFileTemp(ee),"#")
					'Response.Write "bla" & UBound(aTempRiga)
					'response.End

					if ( UBound (aTempRiga) <> -1 ) then

						if ( aTempRiga(5) = 0 ) then

							aFileOrder(nCountIns) = aFileTemp(ee)

	'						Response.Write "<BR>aFileTemp(ee) = " & aFileTemp(ee)

							nCountIns = nCountIns + 1


							for zz = 0 to UBound(aFileTemp)

								aTempRiga2 = split(aFileTemp(zz),"#")
								if ( UBound(aTempRiga2) <> -1 ) then


									if ( aTempRiga2(5) = aTempRiga(4) ) then

										aFileOrder(nCountIns) = aFileTemp(zz)

										aFileTemp(zz) = ""

										nCountIns = nCountIns + 1

									end if

								end if

							next

							aFileTemp(ee) = ""

						end if
					end if
				next

			loop

		end if

	next

	OrderFile = aFileOrder

end function

function DecodIDTab(sIdTab)

	sSQL = "SELECT DESC_TAB FROM DIZ_TAB WHERE ID_TAB = '" & sIdTab & "'"
'PL-SQL * T-SQL
SSQL = TransformPLSQLToTSQL (SSQL)
	set rsDescTab = CC.Execute(sSQL)
	if not rsDescTab.EOF then
		DecodIDTab = rsDescTab("DESC_TAB")
	else
		DecodIDTab = ""
	end if
	rsDescTab.Close

end function



' Funzione che mi costruisce la stringa SQL con le regole da applicare

function CreaCriteriDaFile(CC)
	Dim aOpLog
	Dim memProgr

	aFile = OrderFile("")

	if IsArray(aFile) then

		aOpLog = decodTadesToArray("OPLOG",Date(),"",1,0)
		memProgr = 0
		esCero = ""
		for zz = 0 to ubound(aFile)
			aRiga = Split(aFile(zz),"#")

			'Response.Write "<br>" & ": " &aRiga(3)

			'Agregado para los casos en que el filtro sea experiencia Laboral = 0   RIC
			if (zz) < ubound(aFile) then
				tmpEsCero = Split(aFile(zz + 1 ),"#")
				if tmpEsCero(0) = "ESPRO" and cstr(tmpEsCero(2)) = "01"  and cstr(tmpEsCero(3)) = "0" then
					esCero = "not "
				end if
			end if

			sSQL = "SELECT Tipo_campo FROM DIZ_DATI " &_
				" WHERE ID_CAMPO = '" & aRiga(1) & "' AND" &_
				" ID_TAB = '" & aRiga(0) & "'"

'PL-SQL * T-SQL
'SSQL = TransformPLSQLToTSQL (SSQL)
			set RR1 = CC.Execute (sSQL)
			'Response.Write Ariga(5) & " - " & memprogr

			if clng(aRiga(5)) <> 0 and clng(aRiga(5)) = memProgr then
				if esCero = "not " then
					esCero = ""
				Else
					sql = sql & " and " & aRiga(1) &_
						 " " & searchIntoArray(aRiga(2),aOpLog,1) & Trasforma(aRiga(3), RR1.Fields("Tipo_campo"), searchIntoArray(aRiga(2),aOpLog,1)) &_
						 " And id_persona = a.id_persona "
				end if
			else
				sql = sql & " and " & esCero & " exists(select id_persona from " & aRiga(0) &_
					" Where " & aRiga(1) &_
					" " & searchIntoArray(aRiga(2),aOpLog,1) &_
					Trasforma(aRiga(3), RR1.Fields("Tipo_campo"), searchIntoArray(aRiga(2),aOpLog,1)) &_
					" And id_persona = a.id_persona "
				memProgr = clng(aRiga(4))
			end if

			if (zz) < ubound(aFile) then
				aRiga = Split(aFile(zz + 1 ),"#")
				if clng(aRiga(5)) = 0 then 	sql = sql & ")"
			end if
			RR1.Close
			set rr1 = nothing
		next

		'23/06/2010 AJUSTE DE DAMIAN PARA MOSTRAR TITULARES DE PROGRAMA SOCIAL
		sql = replace(sql,"ESITO_GRAD","COD_ESITO IS NULL AND ESITO_GRAD")
		'FIN 23/06/2010
		
		sql = sql & ")"

		'Response.End
		Erase aOpLog
	end if

	' Response.Write sql

	CreaCriteriDaFile = sql

end function
'**********************************************************************************************************************************************************************************
Function Trasforma(valore,tipo,operatore)

   'Response.Write "1_" & valore
   'Response.Write "2_" &  tipo
   'Response.Write "3_" &  operatore

	if operatore = "IN" or operatore = "NOT IN" then
		if tipo = "A" then
		     Trasforma =  "('" & Replace(Valore,",","','") & "')"
	    else
	         Trasforma =  "(" & Replace(Valore,"$","'") & ")"
	    end if
	else
		if not isnull(tipo) then
			tipo = cstr(tipo)
		else
			tipo = ""
		end if

		select case tipo
			case "D"
				if operatore = "BETWEEN" then
					aVal = Split(valore,",",-1,1)
					    Trasforma = " " & ConvDateToDbS(aVal(0)) & " and " & ConvDateToDbS(aVal(1)) & " "
				else
					Trasforma = ConvDateToDbS(valore)
				end if
			case "A"
			    if operatore = "BETWEEN" then
					aVal = Split(valore,",",-1,1)
					    Trasforma = " '" & aVal(0) & "' and '" & aVal(1) & "' "
				else
				    Trasforma = "'" & valore & "'"
			    end if
			case else
			    if operatore = "BETWEEN" then
					aVal = Split(valore,",",-1,1)
					    Trasforma = " " & aVal(0) & " and " & aVal(1) & " "
				else
				    Trasforma = valore
		        end if
		end select
	end if

End Function
'**********************************************************************************************************************************************************************************

%>
