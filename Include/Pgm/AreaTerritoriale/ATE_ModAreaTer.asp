<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include virtual = "/strutt_testa2.asp"-->
<!--#include virtual ="/include/openconn.asp"-->
<!--#include virtual="/util/portallib.asp"-->
<!--#include virtual = "Include/HTMLEncode.asp"-->
<%
if ValidateService(session("idutente"),"ATE_MODAREATER", CC) <> "true" then 
	response.redirect "/util/error_login.asp"
end if

Dim Record, nIndElem
Dim nActPagina
Dim nTotPagina
Dim nTamPagina		
Dim nTotRecord

'paginazione
nTamPagina=15
If Request("pagina")= "" Then
	nActPagina=1
Else
	nActPagina=Clng(Request("pagina"))
End If

%>

<script LANGUAGE="Javascript">
//include del file per fare i controlli sulla numericit� dei campi
<!--#include virtual = "Include/ControlNum.inc"-->
<!--#include virtual = "Include/ControlString.inc"-->
<!--#include virtual = "Include/help.inc"-->

//paginazione
function AssegnaVal(val){
	document.FrmPagina.pagina.value = val;
	document.FrmPagina.submit()
}

function Elimina()
{
	frmAreaGeoElimina.submit();
}

function InsProv()
{
	//?Area=' + sArea + '&sTxtArea=' + sCampo +'&NomeCampo=' + sNomeCampo +'&ProvinciaCI=' + ProvinciaCI
	windowArea = window.open ('ATE_SelPro.asp','Info','width=425,height=450,Resize=No,Scrollbars=yes')	
}

function ControllaDati2(myForm)
{
	var nPrimoEle 
	var ok

	ok = 0 
	nPrimoEle = 0
	while (myForm.elements[nPrimoEle].value != "Elimina") 
	{
		if (myForm.elements[nPrimoEle].checked){
			ok = 1
		}
		nPrimoEle = nPrimoEle + 1
	}
	if (ok == 0) 
	{
		alert ("Debe seleccionar al menos un Area Geogr�fica")
		return false
	}
	else
		return true
}

function ControllaDati(myFrm){ 
	frmModAreaTer.txtGruppo.value = TRIM(frmModAreaTer.txtGruppo.value)
    
	if (frmModAreaTer.txtGruppo.value == "")
	{
		alert("El Area Territorial es obligatoria")
		frmModAreaTer.txtGruppo.focus()
		return false
	}  				 
return true
}
</script>
</head>
<body>
<table border="0" width="520" cellspacing="0" cellpadding="0" height="81">
   <tr>
     <td width="500" background="<%=Session("Progetto")%>/images/titoli/strumenti2b.gif" height="81" valign="bottom" align="right">
       <table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
         <tr>
           <td width="100%" valign="top" align="right"><b class="tbltext1a">Area Territorial</span></b></td>
         </tr>
       </table>
     </td>
   </tr>
</table>
<br>
<table cellpadding="0" cellspacing="0" width="500" border="0">
		<tr height="18">
			<td class="sfondomenu" height="18" width="60%">
				<span class="tbltext0">
					<b>GESTION AREA TERRITORIAL</b>
				</span>
			</td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">
			</td>
			<td valign="middle" align="right" width="35%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1">
			</td>
		</tr>
		<tr width="371" class="SFONDOCOMM">
			<td colspan="3">
				En la tabla se puede visualizar las areas territoriales presentes en el sistema . 
				<br>Para modificar presionar sobre el <b>Area Territorial</b> de inter�s.
				<br>Para ingresar un area territorial nueva, completar el campo inferior y presionar <b>Agregar</b>.
				<a href="Javascript:Show_Help('/Pgm/help/AreaTerritoriale/ATE_ModAreaTer')" name="prov3" onmouseover="javascript:window.status='' ; return true">
					<img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0">
				</a>
			</td>
		</tr>
		<tr height="2">
			<td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>
<br>
<%
sGruppo=Request.Form("txtDescGruppo")
%>
<br>
<form name="frmModAreaTer" method="post" onsubmit="return ControllaDati(this)" action="ATE_CnfAreaTer.asp">
	<p>
	<table width="500" border="0" cellspacing="2" cellpadding="1">
		<tr> 
			<td align="center">
				<b class="tbltext1">Area Territorial:</b>
				&nbsp;
				<input type="text" maxlength="50" style="TEXT-TRANSFORM:uppercase" name="txtGruppo" class="textblacka">
			</td>
		</tr>
	</table>
	<br>
	<table width="150" border="0">
		<tr> 
			<td align="center">
				<input type="image" src="<% session("Progetto") %>/images/aggiungi.gif" border="0" value="Aggiungi" align="top" name="Aggiungi">
			</td>
		</tr>
	</table>
</form>	

<%
set rsAreaTerr= server.CreateObject("ADODB.Recordset")
sSQL = "SELECT ID_AREATERR,DESC_AREATERR " &_
		"FROM AREA_TERR" ' WHERE DESC_AREATERR ='" & sGruppo & "'"
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
rsAreaTerr.Open sSQL,CC,3

if not rsAreaTerr.Eof then
	rsAreaTerr.PageSize = nTamPagina
	rsAreaTerr.CacheSize = nTamPagina
	nTotPagina = rsAreaTerr.PageCount
	If nActPagina < 1 Then
		nActPagina = 1
	End If
	If nActPagina > nTotPagina Then
		nActPagina = nTotPagina
	End If
	rsAreaTerr.AbsolutePage= nActPagina
	nTotRecord=0
%>
	<table cellspacing="1" cellpadding="1" align="center" width="500" border="0">
		<tr class="sfondocomm" align="center" width="500"> 
			<td align="middle" valign="center">
				<b>Area Territorial</b>
			</td>
		</tr>
	</table>
	<table width="500" bordercolor="#000099" cellpadding="1" cellspacing="1" border="0">
<%
		n= 1
		While rsAreaTerr.EOF <> True And nTotRecord < nTamPagina
		
		ID_AREATERR=rsAreaTerr("ID_AREATERR")
		DESC_AREATERR=strHTMLEncode(rsAreaTerr("DESC_AREATERR"))
		
%>
       <form name="frmAreaProv<%=n%>" method="post" action="ATE_InsProv.asp">
        	<tr class="tblsfondo"> 
				<td align="left">
					<input type="hidden" value="<%=ID_AREATERR%>" name="txtIdArea">
					<input type="hidden" value="<%=DESC_AREATERR%>" name="txtTerr">
					<span class="tbldett">
		     			<a href="javascript:document.frmAreaProv<%=n%>.submit()" class="tblagg" onmouseover="javascript:status='' ; return true"><b><%=DESC_AREATERR%></b></a>
				   </span>
				</td>
			</tr>
		</form>	
<%          
            n= n +1
            nTotRecord=nTotRecord + 1
			rsAreaTerr.MoveNext
		wend	
%>
		
	</table>
	<!--PAGINAZIONE-->
		<form name="FrmPagina" method="post" action="ATE_ModAreaTer.asp">
		<input type="hidden" id="pagina" name="pagina" value="<%=nActPagina%>">
		<table border="0" width="500">
			<tr>
			<%
			if nActPagina > 1 then%>		
				<td align="right" width="480">
					<a href="Javascript:AssegnaVal('<%=(nActPagina - 1)%>')">
						<img src="<%=Session("Progetto")%>/images/precedente.gif" border="0" id="image1" name="image1">
					</a>
				</td>
			<%
			end if
			if nActPagina < nTotPagina then%>
				<td align="right">
					<a href="Javascript:AssegnaVal('<%=(nActPagina + 1)%>')">
						<img src="<%=Session("Progetto")%>/images/successivo.gif" border="0" id="image1" name="image1">
					</a>
				</td>
			<%
			end if%>		
			</tr>
		</table>
		</form>
<%	
		if n = 1 then
%>
			<table border="0" cellspacing="1" cellpadding="1" width="500">
				<tr align="middle">
					<td class="tbltext3">
						No hay definida ning�n Area Territorial
					</td>
				</tr>
			</table>
<%	
		end if
else
%>
	<table border="0" cellspacing="1" cellpadding="1" width="500">
		<tr align="middle">
			<td class="tbltext3">
				No hay definida ning�n Area Territorial
			</td>
		</tr>
	</table>
<%	
end if
rsAreaTerr.Close
set rsAreaTerr = Nothing
%>
<!--#include virtual ="/include/CloseConn.asp"-->
<!--#include virtual = "/strutt_coda2.asp"-->

