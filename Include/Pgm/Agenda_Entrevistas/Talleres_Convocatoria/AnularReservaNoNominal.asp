<!-- #include Virtual="/strutt_testa2.asp" -->
<!-- #include Virtual="/Pgm/Agenda_Entrevistas/ClsInterfaceBD.asp"-->


<%
	if ValidateService(session("idutente"),"AG_ANULA_RESERVA",cc) <> "true" then 
		response.redirect "/util/error_login.asp"
	end if
	
%>

<table border="0" width="530" cellspacing="0" cellpadding="0" height="81">
   <tr>
     <td width="530" background="<%=Session("Progetto")%>/images/titoli/strumenti2g.gif" height="81" valign="bottom" align="right">
       <table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
         <tr>
          <td width="100%" valign="top" align="right">
          		<b class="tbltext1a">Anulaci�n de Reservas no Nominadas</b>
          </td>
         </tr>
       </table>
     </td>
   </tr>
</table>
<br>
<table class="SFONDOCOMM" cellpadding="0" cellspacing="0" width="500" border="0">
	<tr height="18">
		<td class="sfondomenu" height="18" width="67%"> <span class="tbltext0"> 
		<b>Anular Reserva</b>
		</td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
		<td valign="middle" align="right" class="tbltext1" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">
		
		</td>
    </tr>
    <tr>
	<td colspan=3>
		<br><p class="tbltext1">Ingresar la informaci�n correspondiente al taller.</p><br>
	</td>
	<td valign="middle" align="right" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1"></td>
	</tr>
	 
	<tr height="2">
		<td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif">
		</td>
	</tr>
</table>

<!--Inicio del Formulario-->
<script language="javascript">

<!--#include virtual = "/Include/ControlNum.inc"-->

function ControlarDatos()
{
	if ((document.FrmAnularReserva.TxtNroLote.value == "") ||
		(document.FrmAnularReserva.TxtMotivoDesc.value == ""))
	{
		alert("Los campos Nro. de Lote y \nMotivo (Observaciones) son obligatorios");
		return false;
	}
	
	
	if (!IsNum(document.FrmAnularReserva.TxtNroLote.value))
	{
		alert("El campo Nro. de Lote debe ser num�rico");
		return false;
	}

	if (IsNum(document.FrmAnularReserva.TxtMotivoDesc.value))
	{
		alert("El campo Motivo no puede ser num�rico");
		return false;
	}
}
</script>
<%
Dim Inter

Set Inter = New InterfaceBD
%>

<br>
<br>
<div align="center">
	<table border="0" width="500">
		<form name="FrmAnularReserva" action="CnfAnularReserva.asp" method="post" OnSubmit='return ControlarDatos();'>
			<input type="hidden" name="TxtOficina" value=<%=Session("creator")%>>
			<input type="hidden" name="TxtTipoMov" value=4>
			<tr>
				<td class="tbltext1"><b>N�mero de Lote</b></td>
				<td>
													<%

'------------------Pino agosto 2007 per gestire il numero lotto come combo e non doverlo ricordare a memoria (era un campo testo)					
Set RsLote = server.CreateObject("adodb.recordset")					

	SSQL = "select distinct (RefExternaNro ) FrOM AgCitaMovimientos with (nolock) where Oficina = " & session("creator") & " and RefExternaTipo = 1"
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
		Set rsLote = ConnLavoro.Execute(sSQL)		

	Retorna = ""

	If not RsLote.eof then
		Retorna = Retorna & "<select name='TxtNroLote'>"
		do while not RsLote.eof
				Retorna = Retorna & "<option value ='" & RsLote.fields(0) & "'>" & RsLote.fields(0) & "</option>"
			RsLote.movenext
		loop
	end if

	Retorna = Retorna & "</select>"
	Response.Write Retorna
	
	'------------------Pino agosto 2007 fino alla prossima istruzione html remmata	
%>
				<!--input type="text" name="TxtNroLote" class="textblack" style="TEXT-TRANSFORM:uppercase" WIDTH="220px"-->
				</td>
			</tr>
			<% ComboMotivo =Inter.GenerarComboMotivos("CmbMotivo","textblack","","",4)
			if ComboMotivo <> "" then 
			%>
			<tr>
				<td class="tbltext1"><b>Motivo</b></td>
				<td><%=ComboMotivo%></td>
			</tr>
			<%
			end if
			%>
			<tr>
				<td class="tbltext1"><b>Motivo (Observaciones)</b></td>
				<td><input type="text" name="TxtMotivoDesc" class="textblack" style="TEXT-TRANSFORM:uppercase" size=60 maxlength=60></td>
			</tr>
			<tr>
				<td colspan="2" align="right"><br>
					<input type = "submit" class="my" value="Anular Reservas">
				</td>
			</tr>
		</form>
	</table>
</div>
<%
Set Inter = nothing
%>
<!-- #include Virtual="/strutt_coda2.asp" -->