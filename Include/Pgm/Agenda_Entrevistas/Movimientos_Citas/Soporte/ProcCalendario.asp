<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->

<!-- #INCLUDE FILE="Utils.asp" -->

<%

Class CalEndario

'####FUNCION QUE ACTUA COMO INTERFAZ (RESCATA LA INFO NECESARIA DE LA BASE Y
'PASA LOS PARAMETROS CORRESPONDIENTES A LA FUNCION QUE GENERA EL CALEndARIO####

Public Function CrearCalEndario(Mes,Anio)
	CrearCalEndario = ""

	Dim cn
	Dim rs

	Set cn = server.CreateObject("aDodb.connection")
	Set rs = server.CreateObject("aDodb.recordSet")

'PL-SQL * T-SQL  
	cn.open "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=C:\Inetpub\wwwroot\V_PrAg\AgEnda.mdb;Persist Security Info=False"

	Mes = "01"
	Anio = "2006"

	Dim consulta
	Dim cadena

	consulta = "Select * from AGEndA where mes = '" & Mes & "' and anio = '" & Anio & "'"

'PL-SQL * T-SQL  
CONSULTA = TransformPLSQLToTSQL (CONSULTA) 
	rs.Open consulta,CN

	separaDor = "|"
	Atributos = ""
	Saltos = ""

	If Not rs.EOF Then 
		Do While Not rs.EOF 
			Atributos = Atributos & rs("dia") & separaDor & rs("atributo") & separaDor
			Saltos = Saltos & rs("dia") & separaDor & rs("salto") & separaDor
			rs.MoveNext 
		Loop
	End If

	If right(Atributos,1) = "|" Then
		Atributos = left(Atributos,len(Atributos) - 1)
	End If

	If right(Saltos,1) = "|" Then
		Saltos = left(Saltos,len(Saltos) - 1)
	End If

	CrearCalEndario = ProcesarCalEndario (Mes,Anio,Atributos,Saltos) 

	Set rs = Nothing
	Set cn = Nothing
End Function


'####FUNCION QUE GENERA EL CALEndARIO####

Public Function ProcesarCalEndario (MesAMostrar,AnioAMostrar,Atributos,Saltos,Ofi,Mov,Cita,Dur,NroCita,IdMotivo,MotivoDesc,Cuil,Nombre,Viene)
	ProcesarCalEndario = ""

	'#Armo arrays con los parametros #

	ArrAtributos = split(Atributos,"|") '#01/F/05/F#
	ArrSaltos = split(Saltos,"|")		'#20/E/05/M/07/F#

	'#Defino los distintos colores que se van a usar#

	DiaActual = ""						'DiaActual = ""  
	
	DiaNormal = "#33FF99" '"#00CC00" 			'DiaNormal = "#E4EBFC"  
	DiaMuyOcupado = "#FF9900"
	DiaApuntaDo = "#FFFF00" 			'DiaApuntaDo = "#E0D6A9" 
	DiaFeriaDo = "#00CCFF" 				'DiaFeriaDo = "#DEBCBD" 
	DiaCompleto = "#FF0000"			'D�aCompleto
	DiaNoDisponible = "#CCCCCC" 

	Fase = weekday("1/" & MesAMostrar & "/" & AnioAMostrar,2) 'obtengo el primer dia del mes elegiDo

	'#Armo la tabla#
 'bordercolor='#578489'
	ProcesarCalEndario = ProcesarCalEndario & "<table bordercolor='MidnightBlue' border=2 cellpadding=2 cellspacing=4 style='font-family: verdana; font-size: 7pt'>"

	'#Escribo los nombres de los dias de la semana#

	ProcesarCalEndario = ProcesarCalEndario  & "<tr CLASS='sfonDocomm'>"
	ProcesarCalEndario = ProcesarCalEndario & EscribirNombresDias()
	ProcesarCalEndario = ProcesarCalEndario & "</tr>"


	'#Escribo en blanco los casilleros vacios del mes anterior#

	ProcesarCalEndario = ProcesarCalEndario & "<tr>"
	ProcesarCalEndario = ProcesarCalEndario & EscribirVaciosMesAnterior(Fase,"white") 'dia y color
	
	i = 1
	col = Fase-1
	rig = 0

	Do
		ActDate = i & "/" & MesAMostrar & "/" & AnioAMostrar

		If IsDate(ActDate) Then
		
		SysData = Dateserial(AnioAMostrar,MesAMostrar,i)
			col = col + 1
			'#Defino el color#
			
				bg = DiaNormal
				
				'#SabaDos y Domingos# 
				'va a estar condicionaDo a los dias estableciDos en la base como laborables y no laborables..
				
				'If col > 5 Then bg = DiaFeriaDo 
				If SysData = Date  Then
					DiaActual = "style='border: 3; border-style: ridge; border-color: MidnightBlue; font-weight:bolder'"
				Else
					DiaActual = " "
				End If

            
			DiaEncontraDo = EncontrarDia(Atributos,i)
			'response.write i & "-" & DiaEncontraDo & "(" & Atributos & ")" 
			
			Select Case DiaEncontraDo 
				Case "A"
					bg = DiaApuntaDo
				Case "F"
					bg = DiaFeriaDo
				Case "L"
					bg = DiaNormal
				Case "C"
					bg = DiaCompleto
				Case "P"
					bg = DiaNoDisponible
				case "M"
					bg = DiaMuyOcupado
				case else
			End Select
		'	response.write i & "-" & DiaEncontraDo & " "
			Select Case DiaEncontraDo 
				Case "A","L","M"
					ProcesarCalEndario = ProcesarCalEndario & "<td " & DiaActual & " align=center bordercolor='#CC9999' bgcolor='" & bg & "'><a class='EstiloLink' href='" & EncontrarDia(Saltos,i) & "?data=" & ActDate & "&Ofi=" & Ofi & "&Mov=" & Mov & "&Cita=" & Cita & "&Dur=" & Dur & "&NroCita=" & NroCita & "&IdMotivo=" & IdMotivo & "&MotivoDesc=" & MotivoDesc & "&Cuil=" & Cuil & "&Nombre=" & Nombre & "&Viene=" & Viene & "'><u><b>" & i & "</b></u></a></td>" '### Scrivo i giorni
				Case Else	
					ProcesarCalEndario = ProcesarCalEndario & "<td " & DiaActual & " align=center bordercolor='#CC9999' bgcolor='" & bg & "'>" & i & "</td>" '### Scrivo i giorni
			End Select
				
			If weekday(ActDate) = 1 Then
				ProcesarCalEndario = ProcesarCalEndario & "</tr><tr>"
				col = 0
				rig = rig + 1
			End If
		End If
	i = i + 1
	Loop Until (i > 31) or Not IsDate(ActDate)


	'#SabaDos y Domingos#
	For j = col+1 to 7
		ProcesarCalEndario = ProcesarCalEndario & "<td "
		'If j > 5 Then 
			ProcesarCalEndario = ProcesarCalEndario & " bordercolor='#CC9999' bgcolor='white' "
			'ProcesarCalEndario = ProcesarCalEndario & "bgcolor='" & DiaFeriaDo & "' "
		'Else
		'	ProcesarCalEndario = ProcesarCalEndario & "bgcolor='" & bg & "' "
		'	ProcesarCalEndario = ProcesarCalEndario & "bgcolor='" & DiaNormal & "' "
		'End If
		ProcesarCalEndario = ProcesarCalEndario & ">&nbsp;</td>"
	Next

	ProcesarCalEndario = ProcesarCalEndario & "</tr>"

	'#rig marca la ultima fila que completa con espacios si no hay dias que ocupen la fila nro 6#

	If rig < 5 Then
		ProcesarCalEndario = ProcesarCalEndario & "<tr>"
		ProcesarCalEndario = ProcesarCalEndario & EscribirFilaFinalVacia("white")
		'ProcesarCalEndario = ProcesarCalEndario & EscribirFilaFinalVacia(DiaFeriaDo,DiaNormal)
		ProcesarCalEndario = ProcesarCalEndario &  "</tr>"
	End If

	ProcesarCalEndario = ProcesarCalEndario &  "</table>"
End Function
				
Private Function EncontrarDia (A,N)
	'#Busca el d�a y retorna el atributo (si es feriaDo, libre, etc.)#
	
	Arr = split(A,"|") 

	Dim r
	For r = 0 to 9  
		If cint(N) = r Then 
			N = cstr("0" & r) 
		Else
			N = cstr(N)
		End If
	Next

	Dim x
	For x = lbound(Arr) to ubound(Arr)
		If Arr(x) = N Then 
			EncontrarDia = Arr(x+1)
			Exit For
		End If
	Next 
End Function

Public Function ObtenerNombreMes(MesAMostrar)
	'#Obtiene el nombre del mes a mostrar (en castellano)#
	Dim y

	Dim r
	For r = 0 to 9  
		If cint(MesAMostrar) = r Then 
			MesAMostrar = cstr("0" & r) 
		Else
			MesAMostrar = cstr(MesAMostrar)
		End If
	Next

	Meses = "01|Enero|02|Febrero|03|Marzo|04|Abril|05|Mayo|06|Junio|07|Julio|08|Agosto|09|Septiembre|10|Octubre|11|Noviembre|12|Diciembre"

	ArrMeses = split(Meses,"|")

	For y = lbound(ArrMeses) to ubound(ArrMeses)
		If ArrMeses(y) = MesAMostrar Then 
			ObtenerNombreMes = ArrMeses(y+1)
			Exit For
		End If 
	Next
End Function 

Private Function EscribirNombresDias()
	For k = 0 to 6
		EscribirNombresDias = EscribirNombresDias & "<td bgcolor ='#D9D9AE' bordercolor='MidnightBlue' style='font-family: verdana; font-size: 7pt; font-weight:bolder; color:MidnightBlue'>" & DN(k) & "</td>" 
	Next
End Function


Private Function EscribirVaciosMesAnterior(Mes,Color)
	For j = 1 to Mes -1
		EscribirVaciosMesAnterior = EscribirVaciosMesAnterior & "<td bordercolor='#CC9999' bgcolor='" & Color & "'>&nbsp;</td>" 
	Next
End Function


Private Function EscribirFilaFinalVacia(Color)
	For k = 1 to 7 
		EscribirFilaFinalVacia = EscribirFilaFinalVacia & "<td "
		'If k > 5 Then 
			EscribirFilaFinalVacia = EscribirFilaFinalVacia & " bordercolor='#CC9999' bgcolor='" & Color & "' "
			'EscribirFilaFinalVacia = EscribirFilaFinalVacia & "bgcolor='" & DiaFeriaDo & "' "
		'Else
			'EscribirFilaFinalVacia = EscribirFilaFinalVacia & "bgcolor='" & DiaNormal & "' "
		'End If
		EscribirFilaFinalVacia = EscribirFilaFinalVacia & ">&nbsp;</td>"
	Next
End Function


Public Function ObtenerMes(Mes,Tipo)
	Tipo = uCase(Tipo)
	
	Select Case Tipo
		Case "PRE"
			If Mes = 1 Then
				ObtenerMes = 12
				If ObtenerMes<10 Then ObtenerMes= "0" & ObtenerMes
			Else
				ObtenerMes = Mes -1
				If ObtenerMes<10 Then ObtenerMes= "0" & ObtenerMes
			End If

		Case "POS"
			If Mes = 12 Then
				ObtenerMes = 1
				If ObtenerMes<10 Then ObtenerMes= "0" & ObtenerMes
			Else
				ObtenerMes = Mes +1
				If ObtenerMes<10 Then ObtenerMes= "0" & ObtenerMes
			End If
		Case "ACT"
			If Mes<10 Then 
				ObtenerMes="0" & Mes
			Else
				ObtenerMes = Mes
			End If 
	End Select
End Function


Public Function ObtenerAnio(Mes,Anio,Tipo)
	Tipo = uCase(Tipo)
	
	Select Case Tipo
		Case "PRE"
			If Mes = 1 Then
				ObtenerAnio = Anio -1
			Else
				ObtenerAnio = Anio
			End If
		Case "POS"
			If Mes = 12 Then
				ObtenerAnio = Anio +1
			Else
				ObtenerAnio = Anio
			End If
	End Select
End Function
 

Public Function CrearComboAgEnda(Nombre,Clase,Validacion,Tipo,Valor,ValorElegiDo,Duracion)
	Dim retorna
	Dim ArrMeses
	
	ArrMeses = split("Enero,Febrero,Marzo,Abril,Mayo,Junio,Julio,Agosto,Septiembre,Octubre,Noviembre,Diciembre",",")
	
	Tipo = uCase(Tipo)
	
	retorna = ""
	retorna = retorna & "<Select class='" & Clase & "' name='" & Nombre & "' " & Validacion & ">"

	Select Case Tipo
		Case "MESES"
			For I = 1 to 12
				retorna = retorna & "<option value='" & I & "' "
					If ValorElegiDo <> "" Then
						If I = Int(ValorElegiDo) Then 
							retorna = retorna & "Selected"
						End If
					End If
					retorna = retorna & " >" & ArrMeses(I-1) & "</option>"
			Next
		Case "ANIOS"
			For I = year(date)-10 to year(date)+10
				retorna = retorna & "<option value='" & I & "' "
				If ValorElegiDo <> "" Then
					If I = Int(ValorElegiDo) Then 
						retorna = retorna & "Selected"
					End If
				End If
				retorna = retorna & " >" & I & "</option>"
			Next
		Case "HORAS"
		
			ArrValor = Split(Valor,"|")
		
			cantidad = cint(ubound(ArrValor))

			Dim ArrHorasDesde()
			Dim ArrMinutosDesde()
			Dim ArrHorasHasta()
			Dim ArrMinutosHasta()

			ReDim ArrHorasDesde(cantidad)  
			ReDim ArrMinutosDesde(cantidad)
			ReDim ArrHorasHasta(cantidad)  
			ReDim ArrMinutosHasta(cantidad)

			'ej franjas: "750,810|840,1110"
				
			For f = lbound(ArrValor) to ubound(ArrValor) 
				
				ValorPrimario = ConvertirRangoADecimal(ArrValor(f),"I")
				ValorSecundario = ConvertirRangoADecimal(ArrValor(f),"D")

				If instr(1,cstr(ValorPrimario),",") = 0 Then
					HorasDesde = cstr(ValorPrimario)
					MinutosDesde = "00"
				Else
					HorasDesde = ObtenerHoras(ValorPrimario)
					MinutosDesde = ObtenerMinutos(ValorPrimario)
				End If
				
				ValorDesde = HorasDesde & ":" & MinutosDesde
				
				If instr(1,cstr(ValorSecundario),",") = 0 Then
					HorasHasta = cstr(ValorSecundario)
					MinutosHasta = "00"
				Else
					HorasHasta = ObtenerHoras(ValorSecundario)
					MinutosHasta = ObtenerMinutos(ValorSecundario)
				End If
				ValorHasta = HorasHasta & ":" & MinutosHasta
				
				'Response.Write ValorDesde
				'Response.Write ValorHasta
				
				''cargo los arrays con las franjas horarias separadas en horas y minutos
				
				ArrHorasDesde(f) = cint(HorasDesde)
				ArrMinutosDesde(f) = cint(MinutosDesde)
				ArrHorasHasta(f) = cint(HorasHasta)
				ArrMinutosHasta(f)= cint(MinutosHasta)

			Next 

			For y = lbound(ArrHorasDesde) to ubound(ArrHorasDesde)
					HD = ArrHorasDesde(y)
				    HH = ArrHorasHasta(y)
				    MD = ArrMinutosDesde(y)
				    MH = ArrMinutosHasta(y)
				        
					Select Case MD
				        Case 0, 1, 2, 3, 4, 5, 6, 7, 8, 9
				            MD = "0" & CStr(MD)
				        Case Else
				            MD = CStr(MD)
				    End Select
				      
					Select Case MH
				        Case 0, 1, 2, 3, 4, 5, 6, 7, 8, 9
				            MH = "0" & CStr(MH)
				        Case Else
				            MH = CStr(MH)
				    End Select
				       
				    Select Case HD
				        Case 0, 1, 2, 3, 4, 5, 6, 7, 8, 9
				            HD = "0" & CStr(HD)
				        Case Else
				            HD = CStr(HD)
				    End Select
				        
				    Select Case HH
				        Case 0, 1, 2, 3, 4, 5, 6, 7, 8, 9
				            HH = "0" & CStr(HH)
				        Case Else
				            HH = CStr(HH)
				    End Select
				            
					HoraInicial = HD & ":" & MD
					HoraFinal = HH & ":" & MH
					
					minutos = cint(ArrMinutosDesde(y))
					horaact = cint(ArrHorasDesde(y))	
				
				Do Until (HoraFinal = Horario)
					
					Select Case minutos
						Case 0,1,2,3,4,5,6,7,8,9
							ValMinutos = "0" & cstr(minutos)
						Case Else
							ValMinutos = cstr(minutos)
					End Select
									
					Select Case horaact
						Case 0,1,2,3,4,5,6,7,8,9
							ValHoras = "0" & cstr(horaact)
						Case Else
							ValHoras = cstr(horaact)
					End Select
					
					Horario = ValHoras & ":" & ValMinutos
					
					minutos = minutos + (Duracion - 1)
					
                    If minutos > 59 Then
                        Do While minutos >= 60
                            minutos = minutos - 60
                            If horaact > 23 Then
                                horaact = 0
                            Else
                                horaact = horaact + 1
                            End If
                        Loop
                    End If
					
					If ValorElegiDo <> "" and (ValorElegiDo = Horario) Then 
						retorna = retorna & "<option value='" & Horario & "' Selected>" & Horario & "</option>"
					Else
						retorna = retorna & "<option value='" & Horario & "'>" & Horario & "</option>"
					End If
				Loop
			Next
	End Select

	retorna = retorna & "</Select>"
	
	CrearComboAgEnda = retorna
End Function

Public Function ObtenerRangoPosible(HorarioElegiDo,Rangos)
		ArrValor = Split(Rangos,"|")
		cantidad = cint(ubound(ArrValor))

		Dim ArrHorasDesde()
		Dim ArrMinutosDesde()
		Dim ArrHorasHasta()
		Dim ArrMinutosHasta()

		ReDim ArrHorasDesde(cantidad)  
		ReDim ArrMinutosDesde(cantidad)
		ReDim ArrHorasHasta(cantidad)  
		ReDim ArrMinutosHasta(cantidad)

			'ej franjas: "750,810|840,1110"
				
		For f = lbound(ArrValor) to ubound(ArrValor) 
				
			ValorPrimario = ConvertirRangoADecimal(ArrValor(f),"I")
			ValorSecundario = ConvertirRangoADecimal(ArrValor(f),"D")
		
			If instr(1,cstr(ValorPrimario),",") = 0 Then
				HorasDesde = cstr(ValorPrimario)
				MinutosDesde = "00"
			Else
				HorasDesde = ObtenerHoras(ValorPrimario)
				MinutosDesde = ObtenerMinutos(ValorPrimario)
			End If
				
			ValorDesde = HorasDesde & ":" & MinutosDesde
			
			If instr(1,cstr(ValorSecundario),",") = 0 Then
				HorasHasta = cstr(ValorSecundario)
				MinutosHasta = "00"
			Else
				HorasHasta = ObtenerHoras(ValorSecundario)
				MinutosHasta = ObtenerMinutos(ValorSecundario)
			End If
			ValorHasta = HorasHasta & ":" & MinutosHasta
				
			ArrHorasDesde(f) = cint(HorasDesde)
			ArrMinutosDesde(f) = cint(MinutosDesde)
			ArrHorasHasta(f) = cint(HorasHasta)
			ArrMinutosHasta(f)= cint(MinutosHasta)

		Next 
		
		For y = lbound(ArrHorasDesde) to ubound(ArrHorasDesde)				
		            HD = ArrHorasDesde(y)
				    HH = ArrHorasHasta(y)
				    MD = ArrMinutosDesde(y)
				    MH = ArrMinutosHasta(y)
				        
				    If MD = 0 Then
				        MD = "00"
				    ElseIf MD = 5 Then
				        MD = "05"
				    Else
				        MD = CStr(MD)
				    End If
				        
				    If MH = 0 Then
				        MH = "00"
				    ElseIf MH = 5 Then
				        MH = "05"
				    Else
				        MH = CStr(MH)
				    End If
				        
				    Select Case HD
				        Case 0, 1, 2, 3, 4, 5, 6, 7, 8, 9
				            HD = "0" & CStr(HD)
				        Case Else
				            HD = CStr(HD)
				    End Select
				        
				    Select Case HH
				        Case 0, 1, 2, 3, 4, 5, 6, 7, 8, 9
				            HH = "0" & CStr(HH)
				        Case Else
				            HH = CStr(HH)
				    End Select
				            
					HoraInicial = HD & ":" & MD
					HoraFinal = HH & ":" & MH
			
			If HoraInicial = cstr(HorarioElegiDo) Then
				ObtenerRangoPosible = ConvertirHorarioAMinutos(HoraInicial) & "," & ConvertirHorarioAMinutos(HoraFinal)
			ElseIf HoraFinal = cstr(HorarioElegiDo) Then
				ObtenerRangoPosible = ConvertirHorarioAMinutos(HoraInicial) & "," & ConvertirHorarioAMinutos(HoraFinal)
			Else

					
				minutos = cint(ArrMinutosDesde(y))
				horaact = cint(ArrHorasDesde(y))	
				
				Do Until (HoraFinal = Horario)
					
					If minutos=0 Then
						ValMinutos="00"
					ElseIf minutos=5 Then
						ValMinutos = "05"
					Else
						ValMinutos = cstr(minutos)
					End If
					
					Select Case horaact
						Case 0,1,2,3,4,5,6,7,8,9
							ValHoras = "0" & cstr(horaact)
						Case Else
							ValHoras = cstr(horaact)
					End Select
					
					Horario = ValHoras & ":" & ValMinutos
					
					If horaact = 23 and minutos = 55 Then 
						horaact = 0
						minutos = 0 
					Else 
						If minutos = 55 Then
							horaact = horaact + 1
							minutos = 0 
						Else				
							minutos = minutos + 5
						End If
					End If
					
					If Horario = HorarioElegiDo Then 
						ObtenerRangoPosible = ConvertirHorarioAMinutos(HoraInicial) & "," & ConvertirHorarioAMinutos(HoraFinal)
						Exit Do
					End If
				Loop
			End If
		Next
End Function

  
Public Function ConvertirHorarioAMinutos(Horario)
	Dim retorna
	Dim horas
	Dim minutos
	
	retorna = ""
	
	'"12:00"
	
	posicion = instr(1,Horario,":")
	
	If posicion <> 0 Then 
		horas = left(Horario,posicion - 1)
		minutos = right(Horario,len(Horario)-posicion)
		retorna = (cint(Horas) * 60) + cint(minutos)
	End If
	
	ConvertirHorarioAMinutos = retorna
	'response.Write ConvertirHorarioAMinutos
End Function

Public Function ConvertirRangoADecimal(Rango,Posicion)
	Dim retorna
	
	retorna = ""
	
	Posicion = uCase(Posicion)
	
	Select Case Posicion
	
	Case "I"
		retorna = left(Rango,(instr(1,Rango,",")-1)) / 60
	Case "D"
		retorna = right(Rango,(len(Rango)-instr(1,Rango,","))) / 60
	End Select
	
	ConvertirRangoADecimal = retorna
End Function 

Public Function ObtenerMinutosTotales(Valor)
''paso el valor decimal
	Dim retorna
	
	retorna = ""
	If instr(1,cstr(Valor),",") <> 0 Then
		retorna = cstr(cint(round(cdbl(mid(Valor,(instr(1,cstr(Valor),",")))),2)*60))
		retorna = retorna + ((left(Valor,(len(Valor)-(len(Valor) - instr(1,cstr(Valor),",")+1))))*60)
	Else
		retorna = Valor * 60
	End If
	
	ObtenerMinutosTotales = retorna
End Function

Public Function ObtenerMinutos(Valor)
''paso el valor decimal
	Dim retorna
	
	retorna = ""
	If instr(1,cstr(Valor),",") <> 0 Then
		retorna = cstr(cint(round(cdbl(mid(Valor,(instr(1,cstr(Valor),",")))),2)*60))
	Else
		retorna = Valor * 60
	End If
	
	ObtenerMinutos = retorna
End Function

Public Function ObtenerHoras(Valor)
''paso el valor decimal
	Dim retorna
	
	retorna = ""
	
	retorna = cstr(left(Valor,(instr(1,cstr(Valor),",")-1)))
	
	ObtenerHoras = retorna
End Function

Public Function ConvertirRangoAHorario(Rango,Posicion)
	Dim retorna
	Dim iterm
	
	retorna = ""
	interm = ConvertirRangoADecimal(Rango,Posicion)
	
	retorna = ObtenerHoras(interm) & ":" & ObtenerMinutos(interm)
	
	ConvertirRangoAHorario = retorna
End Function

Public Function ConvertirMinutosAHorario(Minutos)
	Dim retorna
	Dim iterm
	
	retorna = ""
	interm = Minutos / 60
	
	'response.Write interm
	
	if instr(1,interm,",") < 1 then 
	    if len(interm) > 1 then 
	        retorna = interm & ":00"
	    else
	        retorna = "0" & interm & ":00"
	    end if
	else
	    retorna = ObtenerHoras(interm) & ":" & ObtenerMinutos(interm)
	end if 
	
	ConvertirMinutosAHorario = retorna
End Function

Public Function EsBetweenIgual(Valor1,Valor2,Valor3)
	If (Valor1 <= Valor2) and (Valor2 <= Valor3) Then 
		EsBetweenIgual = True 
	Else
		EsBetweenIgual = False
	End If
	'Response.Write EsBetweenIgual
End Function

Public Function EsMenorIgual(Valor1,Valor2)
	If Valor1 <= Valor2 Then 
		EsMenorIgual = True 
	Else
		EsMenorIgual = False
	End If
End Function 

Public Function EsMayorIgual(Valor1,Valor2)
	If Valor1 >= Valor2 Then 
		EsMayorIgual = True 
	Else
		EsMayorIgual = False
	End If
End Function 

Public Function ObtenerNombreDia(DiaAMostrar)
	'#Obtiene el nombre del dia a mostrar (en castellano)#
	
	Dim y

	Dim r
	For r = 0 to 7  
		If cint(DiaAMostrar) = r Then 
			DiaAMostrar = cstr("0" & r) 
		Else
			DiaAMostrar = cstr(DiaAMostrar)
		End If
	Next

	Dias = "01|Lunes|02|Martes|03|Miercoles|04|Jueves|05|Viernes|06|Sabado|07|Domingo"

	ArrDias = split(Dias,"|")

	For y = lbound(ArrDias) to ubound(ArrDias)
		If ArrDias(y) = DiaAMostrar Then 
			ObtenerNombreDia = ArrDias(y+1)
			Exit For
		End If 
	Next
End Function 

End Class
%>
