<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #include Virtual="/strutt_testa2.asp" -->
<!-- #include Virtual="include/openconn.asp" -->
<!-- #include Virtual="/Pgm/Agenda_Entrevistas/ClsInterfaceBD.asp"-->

<script language="javascript">
<!--#include Virtual = "/Include/ControlCodFisc.inc"-->
//include del file per fare i controlli sulla validit� delle date
<!--#include virtual = "/Include/ControlDate.inc"-->

//include del file per fare i controlli sulla numericit� dei campi
<!--#include virtual = "/Include/ControlString.inc"-->
<!--#include virtual = "/Include/ControlNum.inc"-->
//SM include controllo Cedula de Identidad
<!--#include Virtual = "/Include/ControlloCI.inc"-->

function RecargarCombos()
{
	var ValorMov
	var ValorCita
	
	ValorMov = document.FrmElegirTipoCita.CmbTipoMov.options[document.FrmElegirTipoCita.CmbTipoMov.selectedIndex].value
	ValorCita = document.FrmElegirTipoCita.CmbTipoCita.options[document.FrmElegirTipoCita.CmbTipoCita.selectedIndex].value
	Agrupacion = document.FrmElegirTipoCita.TxtAgrupacion.value;
	Cuil = document.FrmElegirTipoCita.TxtCuil.value;
	Nombre = document.FrmElegirTipoCita.TxtNom.value;
	Viene = document.FrmElegirTipoCita.TxtViene.value;
	sNroEntrevista = document.FrmElegirTipoCita.TxtNroEntrevista.value;
	sTarea = document.FrmElegirTipoCita.TxtTarea.value;
	
	window.navigate ("ElegirTipoCita.asp?TipoMov="+ValorMov+"&TipoCita="+ValorCita+"&Agrupacion="+Agrupacion+"&Cuil="+Cuil+"&TxtNom="+Nombre+"&Viene="+Viene+"&NroEntrevista="+sNroEntrevista+"&Tarea="+sTarea);

}
function ControlarDatos()
{
	if (document.FrmElegirTipoCita.CmbTipoMov.value == "")
	{
		alert("Debe elegir un tipo de movimiento");
		return false;
	}
	if (document.FrmElegirTipoCita.CmbTipoCita.value == "")
	{
		alert("Debe elegir un tipo de cita");
		return false;
	}
	if (document.FrmElegirTipoCita.CmbMotivo.value == "")
	{
		alert("Debe ingresar un motivo");
		return false;
	}
	if (document.FrmElegirTipoCita.TxtMotivoDesc.value == "")
	{
		alert("Debe ingresar una descripci�n en el campo motivo (observaciones)");
		return false;
	}

	if ((document.FrmElegirTipoCita.CmbTipoMov.value == "6")||(document.FrmElegirTipoCita.CmbTipoMov.value == "10")||(document.FrmElegirTipoCita.CmbTipoMov.value == "11"))
	{
		document.FrmElegirTipoCita.action = "ElegirTipoCita.asp"
		document.FrmElegirTipoCita.submit();
	}else
	{
		document.FrmElegirTipoCita.submit();
	}
}

function ControlarCuil()
{
document.FrmDatos.TxtCuilNH.value=TRIM(document.FrmDatos.TxtCuilNH.value)
	
	/*if (document.FrmDatos.TxtCuilNH.value != "")
	{
		CodFisc = document.FrmDatos.TxtCuilNH.value;
		if (!ControlaCuil(CodFisc))
		{
	        document.FrmDatos.TxtCuilNH.focus()
	        return false;
		}
		else
		{
			document.FrmDatos.TxtCuil.value = CodFisc;
			document.FrmDatos.TxtNom.value = document.FrmDatos.TxtNombre.value;
		}       
	}*/
	
	// SM inizio
	if (document.FrmDatos.TxtCuilNH.value != "")
		{
				blank = " ";
				if (!ChechSingolChar(document.FrmDatos.TxtCuilNH.value,blank))
				{
			
					alert("El campo N�mero de Identificaci�n no puede contener espacios en blanco")
					document.FrmDatos.TxtCuilNH.focus()
					return false;
				}
				
				if ((document.FrmDatos.TxtCuilNH.value.length != 8))
				{
					alert("El campo N�mero de Identificaci�n debe ser de 8 caracteres")
					document.FrmDatos.TxtCuilNH.focus() 
					return false
				}			
				if (!ValidateInputStringWithNumber(document.FrmDatos.TxtCuilNH.value))
				{
					alert("El campo N�mero de Identificaci�n  es err�neo")
					document.FrmDatos.TxtCuilNH.focus() 
					return false
				}
				
				if (!IsNum(document.FrmDatos.TxtCuilNH.value))
				{
					alert("El campo N�mero de Identificaci�n debe ser num�rico")
					document.FrmDatos.TxtCuilNH.focus() 
					return false
				}		
		
				CodFisc = document.FrmDatos.TxtCuilNH.value;
				if (!ControlloCI(eval(document.FrmDatos.TxtCuilNH.value)))
				{
				     document.FrmDatos.TxtCuilNH.focus()
				     return false
				}
				else
				{
					document.FrmDatos.TxtCuil.value = CodFisc;
					document.FrmDatos.TxtNom.value = document.FrmDatos.TxtNombre.value;
				}       
	
				
		}
		
		// SM fine
	
	
}	

function ReprogramarCita(IdMotivo,MotivoDesc,TipoMov,TipoCita,NroCita,Cuil,Nombre)
{

	if (confirm("Seguro desea Reprogramar la cita seleccionada?"))
	{
		if (typeof eval("document.FrmReprog.TxtViene") != 'undefined')
			{Viene = document.FrmReprog.TxtViene.value}
			else
			{Viene = "0"}
		//document.location.href = "CnfReprogramarCita.asp?TipoMov="+TipoMov+"&TipoCita="+TipoCita+"&NroCita="+NroCita+"&Cuil="+Cuil+"&Motivo="+document.FrmDatos.CmbMotivo.value+"&MotivoDesc="+document.FrmDatos.TxtMotivoDesc.value 
		document.location.href = "CnfReprogramarCita.asp?IdMotivo="+IdMotivo+"&MotivoDesc="+MotivoDesc+"&TipoMov="+TipoMov+"&TipoCita="+TipoCita+"&NroCita="+NroCita+"&Cuil="+Cuil+"&Nombre="+Nombre+"&Viene="+Viene
	}
}	
		
</script>

<script src="../FuncionesAgenda.js">
</script>

<table border="0" width="530" cellspacing="0" cellpadding="0" height="81">
   <tr>
     <td width="530" background="<%=Session("Progetto")%>/images/titoli/strumenti2g.gif" height="81" valign="bottom" align="right">
       <table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
         <tr>
          <td width="100%" valign="top" align="right">
          		<b class="tbltext1a">Movimiento de Agenda</b>
          </td>
         </tr>
       </table>
     </td>
   </tr>
</table>
<br>
<table class="SFONDOCOMM" cellpadding="0" cellspacing="0" width="500" border="0">
	<tr height="18">
		<td class="sfondomenu" height="18" width="67%"> <span class="tbltext0"> 
		<b>Nuevo Movimiento</b>
		</td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
		<td valign="middle" align="right" class="tbltext1" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">
		
		</td>
    </tr>
    <tr>
	<td colspan=3>
		<br><p class="tbltext1">Ingresar la informaci�n solicitada.</p><br>
	</td>
	<td valign="middle" align="right" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1"></td>
	</tr>
	 
	<tr height="2">
		<td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif">
		</td>
	</tr>
</table>

<!--Inicio del Formulario-->
<br>
<br>


<%
Dim Inter

Set Inter = New InterfaceBD


''''''''''''''''''
sIdPers = request("IdPers")

sVieneDeDatos = request("Viene")

if sVieneDeDatos = "" then 
	sVieneDeDatos = request("TxtViene")
	if sVieneDeDatos = "" then 
		if sIdPers <> "" then 
			sVieneDeDatos = "1"
		else
			sVieneDeDatos = "0" 
		end if 
	end if
end if

'Response.Write sVieneDeDatos

if sIdPers <> "" then 
	sCuil = ObtenerCuilPersona(sIdPers)
	sNombre = ObtenerNombrePersona(sIdPers) 
else
	sCuil = request("TxtCuil")
	sNombre = request("TxtNom")
end if 
''''''''''''''''''

'Nro de ENtrevista
If len(Session("NroEntrevista")) = 0 then
	Session("NroEntrevista") = request("NroEntrevista")
End If

sNroEntrevista = request("NroEntrevista")
If sNroEntrevista = "" then
	sNroEntrevista = request("TxtNroEntrevista")
End If
	'response.Write " sNroEntrevista" & sNroEntrevista
'Tarea a ejecutar para la entrevista	
If len(Session("TareaEntrevista")) = 0 then
	Session("TareaEntrevista") = request("Tarea")
End If

sTarea = request("Tarea")
If sTarea = "" then
	sTarea = request("TxtTarea")
End If

if sCuil = "" then 
	sCuil = request("Cuil")
end if

sTipoMov = request("TipoMov")
if sTipoMov = "" then 
	sTipoMov = Request("CmbTipoMov")
end if 

sAgrupacion = request("Agrupacion")
if sAgrupacion = "" then
	sAgrupacion = request("TxtAgrupacion")
end if

if sTipoMov <> "" then 
	sTipoCita = Inter.ObtenerCitaParaMovimiento(sTipoMov)
end if 

sOficina = request("TxtOficina")
If sOficina = "" then
	sOficina = session("creator")
End If 

sIdMotivo = request("CmbMotivo")
sMotivo = request("TxtMotivoDesc")

'if sTipoCita = 0 then
'	sTipoCita = 1
'end if 

'response.write "sCuil	   : " & sCuil				& "<br>"
'response.write "sTipoMov   : " & sTipoMov     & "<br>"
'response.write "sAgrupacion: " & sAgrupacion  & "<br>"
'response.write "sTipoCita  : " & sTipoCita    & "<br>"
'response.write "sOficina   : " & sOficina     & "<br>"
'response.write "sIdMotivo  : " & sIdMotivo    & "<br>"
'response.write "sMotivo    : " & sMotivo      & "<br>"

%>

<%IDP = ObtenerIdPersona(SCuil)%>

<% ' Agregado de tabla para que quede igual a los anteriores RJB %>
<table border="0" width="530" cellspacing="0" cellpadding="0" height="81">
   <tr>
     <td width="530" background="<%=Session("Progetto")%>/images/titoli/strumenti2g.gif" height="81" valign="bottom" align="right">
       <table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
         <tr>
          <td width="100%" valign="top" align="right">
				<%if sVieneDeDatos = "1" then %>
					<p CLASS='tbltext1a' align='right'><b><a href='/pgm/Iscr_Utente/UTE_ModDati.asp?Idpers=<%=IDP%>'>Reanudar Entrevista</a></b></p>
				<%end if%>

				<%if sVieneDeDatos = "2" then %>
					<p CLASS='tbltext1a' align='right'><b><a href='/pgm/Entrevistas/EntrevistaAccion.asp?Idpers=<%=IDP%>&sCuil=<%=sCuil%>&sNroEntrevista=<%=Session("NroEntrevista")%>&sTarea=<%=Session("TareaEntrevista")%>'>Reanudar Entrevista</a></b></p>
				<%end if%>
          </td>
         </tr>
       </table>
     </td>
   </tr>
</table>

<div align="center">

<% if sCuil = "" then %>
	
	<table width="70%">
		<form name="FrmDatos" action="ElegirTipoCita.asp" method="post" onSubmit='return ControlarCuil();'>
			<input type="hidden" name="TxtOficina" value="<%=Session("creator")%>">
			<input type="hidden" name="TxtAgrupacion" value="<%=sAgrupacion%>">
			<input type = "hidden" class="textblack" name="TxtNom">
			<% if sNombre <> "" then %>
			<tr>
				<td class="tbltext1"><b>Nombre: </b></td>
				<td class="tbltext1"><%=ucase(replace(sNombre,"_"," "))%></td>
			</tr>
			<%end if %>
			<tr>
				<td class="tbltext1"><b>N�mero de Identificaci�n</b></td>
				<td><input name="TxtCuilNH" type="text" class="textblack" style="TEXT-TRANSFORM:uppercase" maxlength="8" size="20">
				<input name="TxtCuil" type="hidden">
				<a href="Javascript:IrA('../BuscarPostulante.asp')" ID="imgPunto1" name="imgPunto1" onmouseover="javascript:window.status='';return true"><img border="0" src="<%=Session("Progetto")%>/images/bullet1.gif"></a></td>
			</tr>
			<tr>
				<td class="tbltext1"><b>Apellido y Nombre</b></td>
				<td class="tbltext1">
				<input name="TxtNombre" type="text" size="40" disabled class="textblack" style="TEXT-TRANSFORM:uppercase" disabled>
				
				</td>
			</tr>
			<tr>
				<td colspan=2 align = right><br>
					<input type = "submit" class="my" value="Enviar">
				</td>
			</tr>
		</form>
	</table>
	
<%elseif sTipoMov = "" or sTipoCita = "" or sOficina = "" or sCuil = "" or sMotivo = "" then %>

	<%
		if sNombre = "" then 
			NomPostu = Inter.ExistePostulante(cstr(sCuil))
			if NomPostu = "" then
			%>
				<script>
				    alert("El N�mero de Identificaci�n ingresado no corresponde a un postulante v�lido");
					document.location.href="ElegirTipoCita.asp?Agrupacion=<%=sAgrupacion%>";
				</script>
			<%
			else		
				sNombre = NomPostu	
			end if
		end if
	%>	
	<table width="70%">
		<form name="FrmElegirTipoCita" action="MostrarCalendario.asp" method="post" onSubmit='return ControlarDatos();'>
			<input type="hidden" name="TxtOficina" value="<%=Session("creator")%>">
			<input type="hidden" name="TxtAgrupacion" value="<%=sAgrupacion%>">
			<input type = "hidden" class="textblack" name="TxtCuil" value="<%=sCuil%>">
			<input type = "hidden" class="textblack" name="TxtNom" value="<%=sNombre%>">
			<input type="hidden" name="TxtViene" value="<%=sVieneDeDatos%>">
			<input type="hidden" name="TxtNroEntrevista" value="<%=sNroEntrevista%>">
			<input type="hidden" name="TxtTarea" value="<%=sTarea%>">
			
			
			<% if sNombre <> "" then %>
			<tr>
				<td class="tbltext1"><b>N�mero de Identificaci�n: </b></td>
				<td class="tbltext1"><%=sCuil%></td>
			</tr>
			<tr>
				<td class="tbltext1"><b>Nombre: </b></td>
				<td class="tbltext1"><%=ucase(replace(sNombre,"_"," "))%></td>
			</tr>
			<%end if %>
			<tr>
				<td class="tbltext1"><b>Tipo de Movimiento</b></td>
				<td><%=Inter.GenerarComboTipoMovimientosCuil("CmbTipoMov","tbltext1","RecargarCombos();",sTipoMov,sCuil)%></td>
			</tr>
			<tr>
				<td class="tbltext1"><b>Tipo de Cita</b></td>
				<td><%=Inter.GenerarComboTipoCitas("CmbTipoCita","tbltext1","RecargarCombos();",sTipoCita)%></td>
			</tr>
			<tr><%
			
			'response.write "jhamon__" & sTipoMov & "__" & sCuil & "__"
			
			if sTipoCita <> "" and sTipoMov <> "" then 
				Response.Write "<td class='tbltext1'><b>Duraci�n</b></td>"
				Response.Write "<td>" & Inter.GenerarComboDuracionCita("CmbDuracion","tbltext1",sTipoCita,"",15,120) & "</td>"
			end if
				%>
			</tr>
			<%
			ComboMotivo = Inter.GenerarComboMotivos("CmbMotivo","tbltext1","","",sTipoMov)
			
			If ComboMotivo <> "" then 
			%>
			<tr>
				<td class="tbltext1"><b>Motivo</b></td>
				<td><%=ComboMotivo%></td>
			</tr>
			<%else%>
			<input type="hidden" name="CmbMotivo" value="vuoto">
			<%end if%>
			<tr>
				<td class="tbltext1"><b>Motivo (Observaciones)</b></td>
				<td><input type="text" name="TxtMotivoDesc" class="textblack" style="TEXT-TRANSFORM:uppercase" size=60 maxlength=60></td>
			</tr>
			<tr>
				<td colspan=2 align = right><br>
					<input type = "submit" class="my" value="Ver Disponibilidad">
				</td>
			</tr>
		</form>
	</table>
<%
else

	sMotivo = replace(sMotivo," ","_")
	Funcion = "Javascript:ReprogramarCita(" & sIdMotivo & ",'" & sMotivo & "'" 

	Resultado = Inter.GenerarTablaMostrarCita("ElegirTipoCita.asp",sOficina,sTipoMov,sTipoCita,sCuil,sNombre,1,"tbltext1",Funcion,0)

	if Resultado <> "" then 
		%>
		<p class="tbltext1"><b>Seleccione a continuaci�n el n�mero de cita que desea reprogramar.</b></p>
		
			<table>
			<form name="FrmReprog" action="" method="">
				<input type="hidden" name="TxtViene" value="<%=sVieneDeDatos%>">
			<% if sNombre <> "" then %>
			<tr>
				<td align="left" class="tbltext1"><b>N�mero de Identificaci�n: </b></td>
				<td align="left" class="tbltext1"><%=sCuil%></td>
			</tr>
			<tr>
				<td align="left" class="tbltext1"><b>Nombre: </b></td>
				<td align="left" class="tbltext1"><%=ucase(replace(sNombre,"_"," "))%></td>
			</tr>
			<%end if %>
			</form>
			</table>
			
		<br>
		<%
		Response.Write Resultado 
	else%>
		<p class="tbltext1"><b>No se encontraron citas que coincidan con los datos ingresados.</b></p>
		<br>
	<%	
	end if 

end if%>
</div>

<%
Set Inter = Nothing
%>

<!--
<a class="tbltext1" href="ElegirTipoCita.asp">1</a>
<a class="tbltext1" href="MostrarCalendario.asp">2</a>
<a class="tbltext1" href="IngresarDatosCita.asp">3</a>
<a class="tbltext1" href="MostrarCitaProgramada.asp">4</a>
-->

<%

function ObtenerCuilPersona(Id)
	dim rs
	set rs = server.CreateObject("adodb.recordset")
	
    VariableSQL = "select cod_fisc from PERSONA where id_persona = " & Id
    'PL-SQL * T-SQL
    VariableSQL = TransformPLSQLToTSQL(VariableSQL)
    
	set rs = cc.execute(VariableSQL)
	
	if not rs.eof then 
		rs.movefirst
		ObtenerCuilPersona = Rs.fields("cod_fisc")
	end if
	
	set rs =nothing
end function

function obtenerNombrePersona(Id)
	dim rs
	set rs = server.CreateObject("adodb.recordset")
	
    VariableSQL = "select cognome, nome from PERSONA where id_persona = " & Id
    'PL-SQL * T-SQL
    VariableSQL = TransformPLSQLToTSQL(VariableSQL)
 
	set rs = cc.execute(VariableSQL)
	
	if not rs.eof then 
		rs.movefirst
		obtenerNombrePersona = Rs.fields("cognome") & " " & Rs.fields("nome")
	end if
	
	set rs =nothing
end function

function ObtenerIdPersona(Cuil)
	dim rs
	set rs = server.CreateObject("adodb.recordset")
	
    VariableSQL = "select id_persona from PERSONA where cod_fisc = '" & Cuil & "'"
    'PL-SQL * T-SQL
    VariableSQL = TransformPLSQLToTSQL(VariableSQL)
 
	set rs = cc.execute(VariableSQL)
	
	if not rs.eof then 
		rs.movefirst
		ObtenerIdPersona = Rs.fields("id_persona")
	end if
	
	set rs =nothing
end function

%>
<!-- #include Virtual="include/closeconn.asp" -->
<!-- #include Virtual="/strutt_coda2.asp" -->
