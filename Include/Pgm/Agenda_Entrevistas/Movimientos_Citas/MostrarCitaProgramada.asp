<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #include file="Soporte/ProcCalendario.asp" -->
<!-- #include Virtual="/strutt_testa2.asp" -->
<!-- #include Virtual="include/openconn.asp" -->
<!-- #include Virtual="/Pgm/Agenda_Entrevistas/ClsInterfaceBD.asp"-->

<table border="0" width="530" cellspacing="0" cellpadding="0" height="81">
   <tr>
     <td width="530" background="<%=Session("Progetto")%>/images/titoli/strumenti2g.gif" height="81" valign="bottom" align="right">
       <table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
         <tr>
          <td width="100%" valign="top" align="right">
          		<b class="tbltext1a">Listado de datos de la Cita</b>
          </td>
         </tr>
       </table>
     </td>
   </tr>
</table>
<br>
<table cellpadding="0" cellspacing="0" width="500" border="0">
	<tr height="18">
		<td class="sfondomenu" height="18" width="67%"> <span class="tbltext0"> 
		<b>Cita Programada</b>
		</td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
		<td valign="middle" align="right" class="tbltext1" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">
		
		</td>
    </tr>
    <tr>
	<td colspan=3>
		<br><p class="tbltext1">A continuación se muestra la información correspondiente a la cita programada.</p><br>
	</td>
	<td valign="middle" align="right" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1"></td>
	</tr>
	 
	<tr height="2">
		<td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif">
		</td>
	</tr>
</table>

<!--Inicio del Formulario-->
<!--La primera tabla se llenaria con las franjas horarias disponibles q devuelve el sp
para ese dia en particular-->


<%
SFecha 				=  Request("Fecha")
STipoCita 			=  Request("TipoCita")
STipoMov 			=  Request("TipoMov")
SDuracion 			=  Request("Duracion")
SOficina 			=  Request("Oficina")
SHoraInicial 		=  Request("HDesde")
SCuil 				=  Request("TxtCuil")
SMotivo 			=  Request("CmbMotivo")
SObservaciones 		=  Request("TxtObservaciones")
sNombre				=  Request("TxtNombre")

'''si es una reprogramacion tambien cargo estos..

sNroCita  = request("NroCita")

if SMotivo = "" or lcase(SMotivo)="vuoto" then 
	SMotivo = request("IdMotivoReprog")
	if lcase(SMotivo)="vuoto" then 
		SMotivo=""
	end if
end if

if sObservaciones = "" then 
	sObservaciones  = request("MotivoDesc")
end if

if sObservaciones = "" then 
	sObservaciones  = request("MotivoDescReprog")
end if 

if sObservaciones <> "" then 
	if instr(1,sObservaciones,"_") <> 0 then 
		sObservaciones = replace(sObservaciones,"_"," ")
	end if
end if

sVieneDeDatos = request("TxtViene")


'response.write "SFecha 				: " & SFecha 					& "<br>"
'response.write "STipoCita 		: " &   STipoCita 		& "<br>"
'response.write "STipoMov 			: " & STipoMov 			  & "<br>"
'response.write "SDuracion 		: " &   SDuracion 		& "<br>"
'response.write "SOficina 			: " & SOficina 			  & "<br>"
'response.write "SHoraInicial 	: " & SHoraInicial 	  & "<br>"
'response.write "SCuil 				: " &   SCuil 				& "<br>"
'response.write "SMotivo 			: " &   SMotivo 			& "<br>"
'response.write "SObservaciones: " &  SObservaciones & "<br>"
'response.write "sNombre				: " & sNombre				  & "<br>"
'Response.Write "sNroCita: "  & sNroCita & "<br>"
'Response.Write "sVieneDeDatos: " & sVieneDeDatos & "<br>"

%>

<br>

<%IDP = ObtenerIdPersona(SCuil)%>


<%if sVieneDeDatos = "1" then %>
	<p CLASS='tbltext1a' align='right'><b><a href='/pgm/Iscr_Utente/UTE_ModDati.asp?Idpers=<%=IDP%>'>Reanudar Entrevista</a></b></p>
<%end if%>

<%if sVieneDeDatos = "2" then %>
	<p CLASS='tbltext1a' align='right'><b><a href='/pgm/Entrevistas/EntrevistaAccion.asp?Idpers=<%=IDP%>&sCuil=<%=sCuil%>&sNroEntrevista=<%=Session("NroEntrevista")%>&sTarea=<%=Session("TareaEntrevista")%>'>Reanudar Entrevista</a></b></p>
<%end if%>
			
<%
Dim Inter 

Set Inter = New InterfaceBD

'if sTipoMov = 6 then ''''es una reprogramacion
	NroCitaGenerada = Inter.GrabarMovimiento(SOficina,STipoMov,sNroCita,"",STipoCita,SFecha,SHoraInicial,SDuracion,SCuil,"",sMotivo,sObservaciones,"","","",Session("IdUtente"))
'else
	'NroCitaGenerada = Inter.GrabarMovimiento(SOficina,STipoMov,"","",STipoCita,SFecha,SHoraInicial,SDuracion,SCuil,"",SMotivo,SObservaciones,"","","",Session("IdUtente"))
'end if


If Isnumeric(NroCitaGenerada) then 
	%>

	<p><B class=tbltext1>El movimiento se ha llevado a cabo exitosamente.</B></p>

	<br>

	<div align="center">
		<table  border="0" width="250">
			<form name="" action="" method="">
				<tr>
					<td class="tbltext1"><b>Tipo de Movimiento:</b></td>
					<td class="tbltext1"><%=Inter.ObtenerValor("AgCitaMovimientosTipos",STipoMov)%></td>
				</tr>
				<tr>
					<td class="tbltext1"><b>Tipo de Cita:</b></td>
					<td class="tbltext1"><%=Inter.ObtenerValor("AgCitaTipos",STipoCita)%></td>
				</tr>
				<tr>
					<td class="tbltext1"><b>Duración:</b></td>
					<td class="tbltext1"><%=SDuracion%></td>
				</tr>
				<tr>	
					<td class="tbltext1"><b>Nro de Cita:</b></td>
					<td class="tbltext1"><%=NroCitaGenerada%></td>
				</tr>
				<tr>
					<td class="tbltext1"><b>C.I.:</b></td>
					<td class="tbltext1"><%=SCuil%></td>
				</tr>
				<tr>
					<td class="tbltext1"><b>Apellido y Nombre:</b></td>
					<td class="tbltext1"><%=ucase(replace(sNombre,"_"," "))%></td>
				</tr>
				<tr>
					<td class="tbltext1"><b>Fecha:</b></td>
					<td class="tbltext1"><%=SFecha%></td>
				</tr>
				<tr>	
					<td colspan=2><br></td>
				</tr>
				<%if sVieneDeDatos = "0" then %>
				<tr>	
					<td align=right colspan=2><input type="button" value="Volver" onclick="javascript:document.location.href='ElegirTipoCita.asp?Agrupacion=2'" class="my" id=button1 name=button1></td>
				</tr>
				<%end if%>
			</form>
		</table>
	</div>
<%
else
%>
	<p><b class=tbltext1><%=NroCitaGenerada%></b></p>
<%

Set Inter = nothing
end if

function ObtenerIdPersona(Cuil)
	dim rs
	set rs = server.CreateObject("adodb.recordset")
	
    VariableSQL = "select id_persona from PERSONA where cod_fisc = '" & Cuil & "'"
    'PL-SQL * T-SQL
    VariableSQL = TransformPLSQLToTSQL(VariableSQL)

	set rs = cc.execute(VariableSQL)
	
	if not rs.eof then 
		rs.movefirst
		ObtenerIdPersona = Rs.fields("id_persona")
	end if
	
	set rs =nothing
end function
%>
<!-- #include Virtual="include/closeconn.asp" -->

<!-- #include Virtual="/strutt_coda2.asp" -->
