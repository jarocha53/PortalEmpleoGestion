<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #include Virtual="/strutt_testa2.asp" -->
<!-- #include Virtual="/Pgm/Agenda_Entrevistas/ClsInterfaceBD.asp"-->

<table border="0" width="530" cellspacing="0" cellpadding="0" height="81">
   <tr>
     <td width="530" background="<%=Session("Progetto")%>/images/titoli/strumenti2g.gif" height="81" valign="bottom" align="right">
       <table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
         <tr>
          <td width="100%" valign="top" align="right">
          		<b class="tbltext1a">Agenda Diaria</b>
          </td>
         </tr>
       </table>
     </td>
   </tr>
</table>
<br>
<table class="SFONDOCOMM" cellpadding="0" cellspacing="0" width="500" border="0">
	<tr height="18">
		<td class="sfondomenu" height="18" width="67%"> <span class="tbltext0"> 
		<b>Visualizar Agenda Diaria</b>
		</td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
		<td valign="middle" align="right" class="tbltext1" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">
		
		</td>
    </tr>
    <tr>
	<td colspan=3>
		<br><p class="tbltext1">Ingresar la información solicitada.</p><br>
	</td>
	<td valign="middle" align="right" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1"></td>
	</tr>
	 
	<tr height="2">
		<td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif">
		</td>
	</tr>
</table>

<!--Inicio del Formulario-->
<script language="javascript">
<!--#include virtual = "/Include/ControlDate.inc"-->
function RecargarCombos()
{
	var ValorCita
	
	ValorCita = document.FrmDatos.CmbTipoCita.options[document.FrmDatos.CmbTipoCita.selectedIndex].value
	Agrupacion = document.FrmDatos.TxtAgrupacion.value;
	Fecha = document.FrmDatos.TxtFecha.value;	
	window.navigate ("AgendaCitasDia.asp?TipoCita="+ValorCita+"&Agrupacion="+Agrupacion+"&Fecha="+Fecha);

}

function ControlarDatos()
{

	if (document.FrmDatos.TxtFecha.value == "")
	{
		alert("Debe ingresar una Fecha");
		return false;
	}	
	if (!ValidateInputDate(document.FrmDatos.TxtFecha.value))
	{
		return false;
	}
	if (document.FrmDatos.CmbTipoCita.value == "")
	{
		alert("Debe elegir un tipo de cita");
		return false;
	}
}
</script>

<br>
<br>

<%
Dim Inter

Set Inter = New InterfaceBD

sAgrupacion = request("Agrupacion")
sTipoCita = request("TipoCita")
sFecha = request("Fecha")

%>
	<table width="70%">
		<form name="FrmDatos" action="MostrarCitasDia.asp" method="post" OnSubmit= 'return ControlarDatos();'>
			<input type="hidden" name="TxtOficina" value="<%=Session("creator")%>">
			<input type="hidden" name="TxtAgrupacion" value="<%=sAgrupacion%>">
			<tr>
				<td class="tbltext1"><b>Fecha</b></td>
				<td><input type="text" class="tbltext1" value="<%=sFecha%>" maxlength=10 size=14 name="TxtFecha"</td>
			</tr>
	
			<tr>
				<td class="tbltext1"><b>Tipo de Cita</b></td>
				<td><%=Inter.GenerarComboTipoCitas("CmbTipoCita","tbltext1","onchange=RecargarCombos();",sTipoCita)%></td>
			</tr>
	
			<tr>
			<%
				if sTipoCita <> "" then		
					Response.Write "<td class='tbltext1'><b>Duración</b></td>"
					Response.Write "<td>" & Inter.GenerarComboDuracionCita("CmbDuracion","tbltext1",sTipoCita,"",15,120) & "</td>"
				end if
			%>
			</tr>
			<tr>
				<td colspan=2 align = right><br>
					<input type = "submit" class="my" value="Mostrar Agenda">
				</td>
			</tr>
		</form>
	</table>

<%
Set Inter = nothing
%>

<!-- #include Virtual="/strutt_coda2.asp" -->
