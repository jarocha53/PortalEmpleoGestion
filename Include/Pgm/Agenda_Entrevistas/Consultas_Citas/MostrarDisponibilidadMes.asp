<!-- #include Virtual="/Pgm/Agenda_Entrevistas/Movimientos_Citas/Soporte/ProcCalendario.asp" -->
<!-- #include Virtual="/strutt_testa2.asp" -->
<!-- #include Virtual="include/openconn.asp" -->
<!-- #include Virtual="/Pgm/Agenda_Entrevistas/ClsInterfaceBD.asp"-->

<table border="0" width="530"><tr><td>
<table border="0" width="530" cellspacing="0" cellpadding="0" height="81">
   <tr>
     <td width="530" background="<%=Session("Progetto")%>/images/titoli/strumenti2g.gif" height="81" valign="bottom" align="right">
       <table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
         <tr>
          <td width="100%" valign="top" align="right">
          		<b class="tbltext1a">Disponibilidad</b>
          </td>
         </tr>
       </table>
     </td>
   </tr>
</table>
<br>
<table cellpadding="0" cellspacing="0" width="500" border="0">
	<tr height="18">
		<td class="sfondomenu" height="18" width="67%"> <span class="tbltext0"> 
		<b>Visualización de Disponibilidad</b>
		</td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
		<td valign="middle" align="right" class="tbltext1" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">
		
		</td>
    </tr>
    <tr>
	<td colspan=3>
		<br><p class="tbltext1"></p><br>
	</td>
	<td valign="middle" align="right" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1"></td>
	</tr>
	 
	<tr height="2">
		<td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif">
		</td>
	</tr>
</table>

<!--Inicio del Formulario-->
<!--La primera tabla se llenaria con las franjas horarias disponibles q devuelve el sp
para ese dia en particular-->

<% 

SFecha    = Request("data")                       
SDuracion = Request("Dur")

IF sDuracion = "" then 
	sDuracion = request("CmbDuracion")
end if 

if SFecha = "" then 
	sFecha = request("TxtFecha")
end if 

SOficina  = Request("Ofi")
if sOficina = "" then
	sOficina = request("Oficina")
end if 

Dim Cl
Dim Inter

Set Inter = New InterfaceBD
Set Cl = New Calendario

NomDia = Cl.ObtenerNombreDia(cint(weekday(SFecha,2)))
NomMes = Cl.ObtenerNombreMes(cint(month(SFecha)))

FranjasDisponibles = Inter.GenerarStringFranjasDisponibles(SOficina,cstr(SFecha),1,0,SDuracion)

%>

<div align="center">
	<table width="500" border="0">
		<tr>
			<td class="tbltext1" align="right"><b>Duración:</b></td>
			<td class="tbltext1" align="left"><%=sDuracion%> Minutos</td>
		</tr>
	</table>
</div>

<br>
<div align="center">
<table border="0" width="70%"><tr><td>
	<table border="0" width="70%">
		<tr>
			<td>
				<div align="center">
					<table 	bordercolor="MidnightBlue"	border="2" width="350">
						<tr>
							<th align="center" colspan=4 class="tbltext1" bgcolor="#D9D9AE"><b><%=NomDia%>&nbsp;&nbsp;<%=day(SFecha)%>&nbsp;&nbsp;de&nbsp;&nbsp;<%=NomMes%>&nbsp;&nbsp;de&nbsp;&nbsp;<%=year(SFecha)%></b></th>
						</tr>
						<%=Inter.GenerarFilasRangosPosibles("MostrarDisponibilidadMes.asp",SOficina,cstr(SFecha),1,0,SDuracion)%>
					</table>
				</div>
			</td>	
		</tr>
		<tr><td><br></td></tr>
		<tr>
			<td>
				<div align="center">
					<table  border="0" width="100%">
						<form name="FrmDatos" action="DispCitasMes.asp" method="post" onsubmit="return ControlarDatos();">
							<input type = "hidden" name="Fecha" value="<%=SFecha%>">
							<input type = "hidden" name="Duracion" value="<%=SDuracion%>">
							<input type = "hidden" name="Oficina" value="<%=SOficina%>">
							<tr>
								<td colspan=4 align = right><br>
									<input type = "submit" class="my" value="Volver">
								</td>
							</tr>
						</form>
					</table>
				</div>
			</td>
		</tr>
	</table>
</td></tr></table>
</div>

</td></tr></table>
<%
Set Inter = nothing
Set Cl = nothing
%>
<!-- #include Virtual="include/closeconn.asp" -->
<!-- #include Virtual="/strutt_coda2.asp" -->