<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #include Virtual="/Pgm/Agenda_Entrevistas/Movimientos_Citas/Soporte/ProcCalendario.asp" -->
<!-- #include Virtual="/strutt_testa2.asp" -->
<!-- #include Virtual="include/openconn.asp" -->
<!-- #include Virtual="/Pgm/Agenda_Entrevistas/ClsInterfaceBD.asp"-->

<style>
.EstiloComboFecha {
	background-color:LightYellow;
	font-family: Verdana; 
	font-size: 7pt; 
	color: navy;
	font-weight: bold; 
	border-style:ridge;
	text-transform:uppercase;
}
.EstiloPalabra {
	font-family: Verdana; 
	font-size: 7pt; 
	color: navy;
	font-weight: bold; 
	text-transform:uppercase;
}
.EstiloLink:hover{
	color: Black;
	font-weight: bolder;
}
.EstiloLink:visited
{
	color: Black;
	font-weight: bolder;
}
.EstiloLink:link
{
	color: Black;
	font-weight: bolder;
}
.EstiloLink:active
{
	color: Black;
	font-weight: bolder;
}
</style>

<table border="0" width="530" cellspacing="0" cellpadding="0" height="81">
   <tr>
     <td width="530" background="<%=Session("Progetto")%>/images/titoli/strumenti2g.gif" height="81" valign="bottom" align="right">
       <table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
         <tr>
          <td width="100%" valign="top" align="right">
          		<b class="tbltext1a">AGENDA INEFOP <br>Disponibilidad Mensual</b>
          </td>
         </tr>
       </table>
     </td>
   </tr>
</table>
<br>
<table class="SFONDOCOMM" cellpadding="0" cellspacing="0" width="500" border="0">
	<tr height="18">
		<td class="sfondomenu" height="18" width="67%"> <span class="tbltext0"> 
		<b>Visualizar Disponibilidad Mensual</b>
		</td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
		<td valign="middle" align="right" class="tbltext1" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">
		
		</td>
    </tr>
    <tr>
	<td colspan=3>
		<p class="tbltext1">Seleccionar Mes y A�o.
	</td>
	<td valign="middle" align="right" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1"></td>
	</tr>
	 
	<tr height="2">
		<td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif">
		</td>
	</tr>
</table>




<!--Inicio del Formulario-->
<%
	dim Cl
	dim Inter
			
	set Cl = new Calendario
	set Inter = new InterfaceBD
%>

<%


sOficina = session("creator")

sDuracion = 50

%>

<div align="center">
	<table width="500" border="0">
		<tr>
			<td class="tbltext1" align="right"><b>Duraci�n:</b></td>
			<td class="tbltext1" align="left"><%=sDuracion%> Minutos</td>
		</tr>
	</table>
</div>
<br>
	<div align="center">
		<table width="50%" border="0">
			<tr>
				<form name="FrmCalendario" Action="INEFOPAgendaMes.asp" method="POST">
					<input type="hidden" name="TxtOficina" value="<%=sOficina%>">
					<input type="hidden" name="CmbDuracion" value="<%=sDuracion%>">
					
					<td colspan="3" align="center">
						<%
							Mes = Request("Mes")

							If Mes = "" then Mes = Month(date)

							Response.write Cl.CrearComboAgenda("Mes","EstiloComboFecha","OnChange='this.form.submit()'","Meses","",Mes,"")
						%>
						&nbsp;&nbsp;
						<%
							Anio = Request("Anio")
						
							If Anio = "" then Anio = Year(date)
		
							Response.write Cl.CrearComboAgenda("Anio","EstiloComboFecha","OnChange='this.form.submit()'","Anios","",Anio,"")
						%>
					</td>
				</form>
			</tr>
			<tr>
				<td colspan="3" align="center">
					<br>
					<% 
						
						ssAnio = Anio
						ssMes = Cl.ObtenerMes(Mes,"Act")
											
						String1 = Inter.GenerarArrayTipoDia(sOficina,ssAnio,ssMes,sDuracion,"","","")
						String2 = Inter.GenerarArraySaltos(sOficina,ssAnio,ssMes,sDuracion,true,"","","")
						
						'x tipo mov 6, reprogramacion paso mas parametros
						Response.write Cl.ProcesarCalendario (ssMes,ssAnio,String1,String2,sOficina,"","",sDuracion,"","","","","","") 


						'Response.write Cl.ProcesarCalendario (Cl.ObtenerMes(Mes,"Act"),Anio,"01|F|05|L|06|A|16|L|21|A|25|C|27|C","01|MostrarCalendario.asp|05|MostrarCalendario.asp|06|MostrarCalendario.asp|16|MostrarCalendario.asp|21|MostrarCalendario.asp|25|MostrarCalendario.asp|27|MostrarCalendario.asp") 
					%>
				</td>
			</tr>
		</table>
	</div>
<br>
<div align="right">
	<table class="tbltext1" border=1 bordercolor="white">
	
		<tr>
		<td colspan=4 align="right"><b>Referencias</b></td>
		</tr>
		<tr>
		<td colspan=4 align="right"><br></td>
		</tr>
		<tr>
			<td width=10 bgcolor="#CCCCCC"></td>		
			<td colspan=4>No Disponible</td>
		</tr>
		<tr>
			<td width=10 bgcolor="#FF0000"></td>
			<td>Completo</td>
			<td width=10 bgcolor="#FF9900"></td>
			<td>Grado de Ocupaci�n Alto</td>
			<td width=10 bgcolor="#FFFF00"></td>
			<td>Grado de Ocupaci�n Bajo</td>
			<td width=10 bgcolor="#33FF99"></td>
			<td>Libre</td>
		</tr>
		<tr>
			<td width=10 bgcolor="#00CCFF"></td>
			<td colspan=4>Feriado</td>
		</tr>
		
	</table>
</div>
		<%
set Cl = nothing
Set Inter = nothing
		
function ObtenerIdPersona(Cuil)
	dim rs
	set rs = server.CreateObject("adodb.recordset")

    VariableSQL = "select id_persona from PERSONA where cod_fisc = '" & Cuil & "'"
    'PL-SQL * T-SQL
    VariableSQL = TransformPLSQLToTSQL(VariableSQL)

	set rs = cc.execute(VariableSQL)
	
	if not rs.eof then 
		rs.movefirst
		ObtenerIdPersona = Rs.fields("id_persona")
	end if
	
	set rs =nothing
end function
%>
<!-- #include Virtual="include/closeconn.asp" -->
<!-- #include Virtual="/strutt_coda2.asp" -->
