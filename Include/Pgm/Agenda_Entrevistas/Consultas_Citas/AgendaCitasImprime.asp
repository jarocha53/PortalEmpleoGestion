<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%
'===============================================
'	Imprime un comprobante de realizacion de Historia Laboral
'	Imprime un comprobante de Cita proxima
'===============================================

function StringDelMes(iMes)
	select case imes
		case 1
			stringdelmes="Enero" 
		case 2
			stringdelmes="Febrero"
		case 3
			stringdelmes="Marzo"
		case 4
			stringdelmes="Abril"
		case 5
			stringdelmes="Mayo"
		case 6
			stringdelmes="Junio"
		case 7
			stringdelmes="Julio"
		case 8
			stringdelmes="Agosto"
		case 9
			stringdelmes="Septiembre"							 	 				 	 				 	 
		case 10
			stringdelmes="Octubre"
		case 11
			stringdelmes="Noviembre"
		case 12
			stringdelmes="Diciembre"							 	 				 	 				 	 									 	 				 	 				 	 									 	 				 	 				 	 									 	 				 	 				 	 	
	end select
end function


'--------------------------------------------------------------
''''''Impresion de comprobante de historia Laboral
'--------------------------------------------------------------
function ImprimeComprobante(Cuil,oficina,NroEntrevista,sFileNameDic)
	dim Apellido
	dim Nombre
	dim ArticuloSexo
	dim empresa
	dim OficinaLugar
	dim OficinaMunicipio
	dim OficinaNombre
	dim IdPersona
	dim TipoDoc
	dim NroDoc
	dim TipoDocDesc
	dim documento	

	'--------------------------------------------------------------
	' Lee los datos de la persona
	'----------------------------------------------------------------
	Set RsUtente = Server.CreateObject("ADODB.Recordset")
	sSQL = "SELECT COGNOME,NOME,COD_FISC,Id_persona,sesso FROM PERSONA WHERE COD_FISC ='" & CUIL & "'"
	'response.write sSQL
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	set rsUtente = cc.execute (sSQL)
	if not rsUtente.eof then
		Apellido = rsUtente("COGNOME")
		Nombre = rsUtente("NOME")
		IdPersona= rsUtente("Id_persona")
		select case rsUtente("sesso")
		case "M" 
			articulosexo="El Sr "
		case "F" 
			articulosexo="La Sra "	
		case else 
			articulosexo="El/la Sr/Sra "	
		end select
	end if
	rsUtente.close
	set rsUtente = nothing

	ssql="Select  i.ID_IMPRESA "
	ssql= ssql & " From IMPRESA i inner Join Sede_IMPRESA si "
	ssql= ssql & "	On i.ID_IMPRESA = si.ID_IMPRESA "
	ssql= ssql & "	and Id_Sede = " & Oficina


'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	set rsUtente = cc.execute (sSQL)
	if not rsUtente.eof then
		empresa= rsUtente("ID_IMPRESA")
	end if
	rsUtente.close
	set rsUtente = nothing	

	'' Tipo de documento	
	sSQL = "SELECT COD_DOCST,COD_FISC FROM PERS_TIPO_DOC WHERE Id_persona =" & IdPersona
	'response.write sSQL 
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	set rsUtente = cc.execute (sSQL)
	if not rsUtente.eof then
		TipoDoc = rsUtente("COD_DOCST")
		NroDoc = rsUtente("COD_FISC")
	end if
	rsUtente.close
	set rsUtente = nothing	

	'' descripcion de documento	
	sSQL = "SELECT descrizione	from Tades "
	sSQL =ssql & " where NOME_TABELLA = 'DOCST' and CODICE ='" & TipoDoc & "'"

'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	set rsUtente = cc.execute (sSQL)
	if not rsUtente.eof then
		TipoDocDesc = "" & rsUtente("descrizione")
	end if
	rsUtente.close
	set rsUtente = nothing	
	documento="" & tipodocdesc & " " & NroDoc
	'response.write documento
	'response.end

	'' Datos de la Oficina
	sSQL1 = " select  INDIRIZZO,Comune,descrizione from Sede_IMPRESA " 
	sSql1=ssql1 & " where ID_SEDE =" & Oficina
	sSql1=ssql1 & " and ID_IMPRESA =" & EMPRESA

'response.write SSQL1

'PL-SQL * T-SQL  
SSQL1 = TransformPLSQLToTSQL (SSQL1) 
	set rsUtente = cc.execute (sSQL1)
	if not rsUtente.eof then
		OficinaLugar="" & rsUtente("INDIRIZZO")
		OficinaNombre="" & rsUtente("descrizione")
		if isnull(rsUtente("Comune")) then
			OficinaMuni= 0
		else	
			OficinaMuni= rsUtente("Comune")
		end if	
	end if
	rsUtente.close
	set rsUtente = nothing	

	ssql="select DESCOM  from Comune  where CODCOM ='" & OficinaMuni & "'"

'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	set rsUtente = cc.execute (sSQL)
	if not rsUtente.eof then
		
		if isnull(rsUtente("descom")) then
			OficinaMunicipio= "------"
		else	
			OficinaMunicipio= " " & rsUtente("descom")
		end if	
	end if
	rsUtente.close
	set rsUtente = nothing	

	sFileNameDic = CreaDireccion(IdPersona, nombre, Apellido,cuil,OficinaNombre, OficinaLugar,OficinaMunicipio,documento,ArticuloSexo,NroEntrevista)	

	%>
	
	<script language="javascript">
	
		ImprimeTalon = window.open('<%=sFileNameDic%>','', 'menubar=0,width=780,height=500,top=15,left=15,Scrollbars=yes,Resize=yes')	
	
		ImprimeTalon.print();
		
	</script>
	<%

end function


function CreaDireccion(sIdpers, Nombre, Apellido,Cuil,OficinaNombre,OficinaLugar,OficinaMunicipio,documento,ArticuloSexo,NroEntrevista)
'------------------------------------------------------
'	crea el archivo del comprobante de realizacion de historia Laboral
' 
'	sIdpers = idpersona
'	sNome = nombre
'	sCognome = apellido
'	dDataNasc = Fecha de nacimiento
'	sComNasc = Comuna de nacimiento
'
'Devuelve:
'	El nombre del archivo fisico creado con su ubicacion 
'------------------------------------------------------
	dim sName, xName
	dim sFilePath
	dim oFileSystemObject
	dim oFileDichiarazione
	dim sMes
	
	smes=StringdelMes(month(date))
	
	' oficina + Id de persona
	xName = sIdpers
	xName = Cuil
	sName = Session("Creator") & "_" & "InscrHLaboral" & xName & ".htm"
	
	' path del archivo
	sFilePath = SERVER.MapPath("\") & ImpostaProgetto & "/DocPers/Certificazioni/" & sName
	set oFileSystemObject = Server.CreateObject("Scripting.FileSystemObject")
	set oFileDichiarazione = oFileSystemObject.CreateTextFile(sFilePath,true)
	
	' arma las partes de la historia laboral
	FoglioStileTalon(oFileDichiarazione)
	IntDicCertificazione(oFileDichiarazione)
	CuerpoTalonInscripcion oFileDichiarazione, Nombre, Apellido,OficinaNombre,OficinaLugar,OficinaMunicipio,smes,documento,cuil,articuloSexo
	Privacy(oFileDichiarazione)
	TalonInscripcionPie oFileDichiarazione,Nombre,Apellido,oficinamunicipio,NroEntrevista	
	
	oFileDichiarazione.close
	set oFileSystemObject = nothing
	set oFileDichiarazione = nothing
	
	CreaDireccion = ImpostaProgetto & "/DocPers/Certificazioni/" & sName
end function

sub CuerpoTalonInscripcion(fso, Nombre, Apellido,OficinaNombre,OficinaLugar,OficinaMunicipio,smes,documento,cuil,articulosexo)
	fso.writeline("<table border='0' align='center' cellspacing='0' cellpadding='0' width='500' >" )
	fso.writeline("	<tr>" )
	fso.writeline("		<TD>&nbsp;</TD>" )
	fso.writeline("	</TR>" )
	fso.writeline("	<tr>" )
	
	fso.writeline("	<tr>" )
	fso.writeline("		<TD align='center'><B>Comprobante de Inscripci�n Historia Laboral</B></TD>" )
	fso.writeline("	</TR>" )
	
	fso.writeline("	<tr>" )
	fso.writeline("		<TD>&nbsp;</TD>" )
	fso.writeline("	</TR>" )
	fso.writeline("	<tr>" )
	fso.writeline("	<tr>" )
	'AEM edita --- Antigua versi�n
	'fso.writeline("		<TD align='justify'>" & articulosexo & "<I><b>" & Nombre & " " &  Apellido & "</b></i> con <I><b>" & Documento & "</b></i> y C.I. N� <I><b>" & cuil & "</b></i> complet� su Historia Laboral en la oficina de Empleo situada en <b>" & oficinalugar & OficinaMunicipio & " </b></i> comprometi�ndose a actualizar sus datos cuando haya ocurrido alg�n cambio de domicilio,estudios realizados,capacitaciones,etc. como de su situaci�n laboral.  </b> </TD>" )
	fso.writeline("		<TD align='justify'>" & articulosexo & "<I><b>" & Nombre & " " &  Apellido & "</b></i> con <I><b>" & Documento & "</b></i> complet� su Historia Laboral en el Centro de Atenci�n <I><b>" & OficinaNombre & "</b></i> situado en <b>" & oficinalugar & OficinaMunicipio & "</b></i>. Comprometi�ndose a actualizar sus datos cuando haya ocurrido alg�n cambio de domicilio, estudios realizados, capacitaciones, etc., as� como de su situaci�n laboral.  </b> </TD>" )
	fso.writeline("	<tr>" )
	
		
	fso.writeline("	<tr>" )
	fso.writeline("		<TD>&nbsp;</TD>" )
	fso.writeline("	</TR>" )
	
	
	fso.writeline("</table>")
end sub

sub FoglioStileTalon(fso)
	fso.writeline("	<HTML>")
	fso.writeline("	<HEAD>")
	fso.writeline("	<TITLE>COMPROBANTE DE INSCRIPCION</TITLE>")
	fso.writeline("	<LINK REL=STYLESHEET TYPE='text/css' HREF='" & session("progetto") & "/fogliostile.css'>")
	
	
	fso.writeline("	</head>")
	fso.writeline("	<Script language=javascript>")
	fso.writeline("	   function InfoArticolo() {")
	fso.writeline("		  if (document.title != 'Certificaci�n') {")
	fso.writeline("			window.open ('UTE_InfoArt.asp','Info','width=550,height=300,top=15,left=15,Resize=No,Scrollbars=yes,screenX=w,screenY=h')")
	fso.writeline("		  }")
	fso.writeline("	   }")
	fso.writeline("	</Script>")
end sub


sub TalonInscripcionPie(fso, Nombre, Apellido,oficinaMunicipio,NroEntrevista)
'sub TimbroFirma(fso, Nombre, Apellido,OficinaMunicipio,sMes)
	fso.writeline("<table border='0' align='center' cellspacing='0' cellpadding='0' width='500' >" )
	fso.writeline("	<tr>" )
	fso.writeline("		<TD colspan=3>&nbsp;</TD>" )
	fso.writeline("	</TR>" )
	
	fso.writeline("	<tr>" )
	fso.writeline("		<TD  align='left'><font size=2>Nombre y firma del entrevistador</font></TD>" )
	fso.writeline("		<TD  align='left'><font size=2>Firma del inscrito</font></TD>" )
	fso.writeline("		<TD  align='left'><font size=2>Lugar y Fecha</font></TD>" )
	fso.writeline("	</TR>" )
	
	fso.writeline("	<tr>" )
	fso.writeline("		<TD align='justify'><font size=1></TD>" )
	fso.writeline("		<TD align='justify'><font size=1>" & Nombre & " " & Apellido & "</font></TD>" )
	fso.writeline("		<TD align='left' ><font size=1>" & oficinaMunicipio & " " & date & "</font></TD>" )
	fso.writeline("	</TR>" )
	
	fso.writeline("	<tr>" )
	fso.writeline("		<TD colspan=3>&nbsp;</TD>" )
	fso.writeline("	</TR>" )
	
	fso.writeline("	<tr>" )
	
	fso.writeline("		<TD align='justify'><font size=2>N�mero de Entrevista: <b>" & NroEntrevista &  "</b></font></TD>" )
	fso.writeline("		<TD align='left' ><font size=2></font></TD>" )
	fso.writeline("		<TD align='justify'><font size=2></TD>" )
	fso.writeline("	</TR>" )

	fso.writeline("		<TD>&nbsp;</TD>" )
	fso.writeline("		<TD>&nbsp;</TD>" )
	fso.writeline("	</TR>" )
	
	fso.writeline("	<tr>" )
	fso.writeline("		<TD colspan=3>&nbsp;</TD>" )
	fso.writeline("	</TR></table>" )
	
	fso.writeline("</Form>")
end sub


''''''''''''''''''''''''''''''''''''''''''
''''''fin Impresion de comprobante
'''''''''''''''''''''''''''''''''''''''''''


''''''''''''''''''''''''''''''''''''''''''
'''''' Impresion de cita
'''''''''''''''''''''''''''''''''''''''''''

sub TimbroFirma(fso, Nombre, Apellido)
'sub TimbroFirma(fso, Nombre, Apellido,OficinaMunicipio,sMes)
	fso.writeline("<table border='0' align='center' cellspacing='0' cellpadding='0' width='500' >" )
	fso.writeline("	<tr>" )
	fso.writeline("		<TD colspan=2>&nbsp;</TD>" )
	fso.writeline("	</TR>" )
	
	fso.writeline("	<tr>" )
	fso.writeline("		<TD  align='left'><font size=2>Firma y Aclaraci�n del Solicitante</font></TD>" )
	fso.writeline("		<TD  align='left'><font size=2>Sello, firma y aclaraci�n del entrevistador</font></TD>" )
	fso.writeline("	</TR>" )
	
	fso.writeline("	<tr>" )
	fso.writeline("		<TD align='justify'><font size=1>" & Nombre & " " & Apellido & "</font></TD>" )
	fso.writeline("		<TD align='left' ><font size=2>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; de la Oficina de Empleo</font></TD>" )
	fso.writeline("	</TR>" )

	fso.writeline("		<TD>&nbsp;</TD>" )
	fso.writeline("		<TD>&nbsp;</TD>" )
	fso.writeline("	</TR>" )
	
	fso.writeline("	<tr>" )
	fso.writeline("		<TD colspan=2>&nbsp;</TD>" )
	fso.writeline("	</TR></table>" )
	
	fso.writeline("</Form>")
end sub


function ImprimeCita(NroCita,NroEntrevista,sFileNameDic)
	dim obBase
	'''''dim nombre,apellido,cuil,Documento,CitaFechaHora,OficinaLugar,Direccion,OficinaMunicipio,FechaDia
	'set obBase=new InterfaceBD
	dim sFileNameConParametros
	
	dim HoraInicio
	dim Fecha
	dim Cuil
	
	dim Apellido
	dim Nombre
	dim HoraHasta
	
	dim TipoDoc
	dim NroDoc
	dim TipoDocDesc
	dim documento	
	
	dim Oficina
	dim empresa
	dim OficinaLugar
	dim OficinaMunicipio
	dim OficinaNombre
	'-------------------------------------------------------
	' Datos de la persona analizada de la variable sIdpers
	'-------------------------------------------------------
	apellido = " - "
	nombre = " - " 
	documento= " - " 
	cuil = " - " 
	OficinaMunicipio = " - " 


	'--------------------------------------------------------------
	' Lee los datos del convenio emitido de sp agCitaLee
	'----------------------------------------------------------------
	horahasta=""
	
	
'Response.Write  "Mensaje" & Mensaje

	Set Rs = Server.CreateObject("ADODB.Recordset")
	Set cmdCita = Server.CreateObject("adodb.command")
	with cmdCita
		.activeconnection = connLavoro
		.Commandtext = "AgCitaLee"
		.commandtype =4
		.Parameters("@NroCita").Value =NroCita
		.Parameters("@ImprimeCita").Value = 1		' Imprime cita
		.Parameters("@Usuario").Value = usuario
'PL-SQL * T-SQL  
		.Execute()
		Fecha=.Parameters("@Fecha").Value
		cuil=.Parameters("@cuil").Value
		HoraInicio=.Parameters("@HoraInicio").Value
		Oficina=.Parameters("@Oficina").Value
		status=.Parameters("@status").Value
		Mensaje=.Parameters("@Mensaje").Value
	end with	

'Response.Write  "Mensaje" & Mensaje
'Response.End 

	'--------------------------------------------------------------
	' Lee los datos de la persona
	'----------------------------------------------------------------
	sSQL = "SELECT COGNOME,NOME,COD_FISC,Id_persona FROM PERSONA WHERE COD_FISC ='" & CUIL & "'"
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	set rsUtente = cc.execute (sSQL)
	if not rsUtente.eof then
		Apellido = rsUtente("COGNOME")
		Nombre = rsUtente("NOME")
		IdPersona= rsUtente("Id_persona")
		
	end if
	rsUtente.close
	set rsUtente = nothing
	
	'' Tipo de documento	
	sSQL = "SELECT COD_DOCST,COD_FISC FROM PERS_TIPO_DOC WHERE Id_persona =" & IdPersona 
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	set rsUtente = cc.execute (sSQL)
	if not rsUtente.eof then
		TipoDoc = rsUtente("COD_DOCST")
		NroDoc = rsUtente("COD_FISC")
	end if
	rsUtente.close
	set rsUtente = nothing	

	'' descripcion de documento	
	sSQL = "SELECT descrizione	from Tades "
	sSQL =ssql & " where NOME_TABELLA = 'DOCST' and CODICE ='" & TipoDoc & "'"
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	set rsUtente = cc.execute (sSQL)
	if not rsUtente.eof then
		TipoDocDesc = "" & rsUtente("descrizione")
	end if
	rsUtente.close
	set rsUtente = nothing	
	documento="" & tipodocdesc & " " & NroDoc



	ssql="Select  i.ID_IMPRESA "
	ssql= ssql & " From IMPRESA i inner Join Sede_IMPRESA si "
	ssql= ssql & "	On i.ID_IMPRESA = si.ID_IMPRESA "
	ssql= ssql & "	and Id_Sede = " & Oficina


'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	set rsUtente = cc.execute (sSQL)
	if not rsUtente.eof then
		empresa= rsUtente("ID_IMPRESA")
	end if
	rsUtente.close
	set rsUtente = nothing	


	'' Datos de la Oficina
	sSQL1 = " select  INDIRIZZO,Comune,descrizione 	 from Sede_IMPRESA " 
	sSql1=ssql1 & " where ID_SEDE =" & Oficina
	sSql1=ssql1 & " and ID_IMPRESA =" & EMPRESA


'PL-SQL * T-SQL  
SSQL1 = TransformPLSQLToTSQL (SSQL1) 
	set rsUtente = cc.execute (sSQL1)
	if not rsUtente.eof then
		OficinaLugar="" & rsUtente("INDIRIZZO")
		OficinaNombre="" & rsUtente("descrizione")
		if isnull(rsUtente("Comune")) then
			OficinaMuni= 0
		else	
			OficinaMuni= rsUtente("Comune")
		end if	
	end if
	rsUtente.close
	set rsUtente = nothing	
		
	ssql="select DESCOM  from Comune  where CODCOM ='" & OficinaMuni & "'"

'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	set rsUtente = cc.execute (sSQL)
	if not rsUtente.eof then
		if isnull(rsUtente("descom")) then
			OficinaMunicipio= "------"
		else	
			OficinaMunicipio= " " & rsUtente("descom")
		end if	
	end if
	rsUtente.close
	set rsUtente = nothing	
		
		
	''''sFileNameDic = CreaDichiarazione(2, nombre, apellido,documento,scuil, Fecha,HoraDesde,HoraHasta)
	sFileNameDic = CreaDichiarazione(NroCita,2, nombre, Apellido,documento,cuil, Fecha,HoraInicio,HoraHasta,OficinaNombre,OficinaLugar,OficinaMunicipio,NroEntrevista)
	
	'Response.Write sfilenamedic
	' Cerrar todo
	'si pudo grabar el movimiento en agCitasMovimiento
	
	if status=1 then%>
		<script language="javascript">
		var ImprimeCita
		switch (<%=IdPersona%>){
			default:
			ImprimeCita = window.open('<%=sFileNameDic%>','', 'menubar=0,width=780,height=500,top=15,left=15,Scrollbars=yes,Resize=yes')	
			ImprimeCita .print();
		}	
		</script>
		<%
	else
		%>
		<script language="javascript">
		alert('<%=status & "-" & Mensaje%>');
		</script>
		<%	
	end if
end function



function CreaDichiarazione(NroCita,sIdpers, Nombre, Apellido,documento,Cuil,Fecha,HoraDesde,HoraHasta,OficinaNombre,OficinaLugar,OficinaMunicipio,NroEntrevista)
'------------------------------------------------------
'	crea el archivo de la notificacion de proxima Cita 
'	sIdpers = idpersona
'	sNome = nombre
'	sCognome = apellido
'	dDataNasc = Fecha de nacimiento
'	sComNasc = Comuna de nacimiento
'Devuelve:
'	nombre del archivo HTM creado con su ubicacion 
'------------------------------------------------------
	dim sName, xName
	dim sFilePath
	dim oFileSystemObject
	dim oFileDichiarazione
	dim sMes
	
	smes=StringdelMes(month(date))
	
	' oficina + Id de persona
	xName = sIdpers
	xName = CUIL
	sName = Session("Creator") & "_CitaProxima_" & xName & ".htm"
	
	' path del archivo
	sFilePath = SERVER.MapPath("\") & ImpostaProgetto & "/DocPers/Certificazioni/" & sName
	
	''''sfilepath="\\Merlyn\portaldesarrollo\plavoro/DocPers/Certificazioni/" &  sName
	
	'sfilepath="http://PortalDesarrollo\PLAVORO\DocPers\Certificazioni\" & sname
	'''Response.Write sfilepath
	'Response.End
	
	
	set oFileSystemObject = Server.CreateObject("Scripting.FileSystemObject")
	set oFileDichiarazione = oFileSystemObject.CreateTextFile(sFilePath,true)
	' arma las partes del convenio
	FoglioStile(oFileDichiarazione)
	IntDicCertificazione(oFileDichiarazione)
	'''CorpoDicCertificazione oFileDichiarazione,Nombre,Apellido,cuil,cDocumento,CitaFechaHora,OficinaLugar,OficinaMunicipio,dFechadelDia,Direccion,smes
	CorpoDicCertificazione NroCita,oFileDichiarazione,Nombre,Apellido,cuil,documento,Fecha,HoraDesde,HoraHasta,OficinaNombre,OficinaLugar,OficinaMunicipio,smes
	
	
	Privacy(oFileDichiarazione)
	'TimbroFirma oFileDichiarazione,Nombre,Apellido,OficinaLugar,smes
	TimbroFirma oFileDichiarazione,Nombre,Apellido,NroEntrevista
		
	oFileDichiarazione.close
	set oFileSystemObject = nothing
	set oFileDichiarazione = nothing
	
	CreaDichiarazione = ImpostaProgetto & "/DocPers/Certificazioni/" & sName
end function

sub FoglioStile(fso)
	fso.writeline("	<HTML>")
	fso.writeline("	<HEAD>")
	fso.writeline("	<TITLE>CITA</TITLE>")
	fso.writeline("	<LINK REL=STYLESHEET TYPE='text/css' HREF='" & session("progetto") & "/fogliostile.css'>")
	
	
	fso.writeline("	</head>")
	fso.writeline("	<Script language=javascript>")
	fso.writeline("	   function InfoArticolo() {")
	fso.writeline("		  if (document.title != 'Certificaci�n') {")
	fso.writeline("			window.open ('UTE_InfoArt.asp','Info','width=550,height=300,top=15,left=15,Resize=No,Scrollbars=yes,screenX=w,screenY=h')")
	fso.writeline("		  }")
	fso.writeline("	   }")
	fso.writeline("	</Script>")
end sub

sub IntDicCertificazione(fso)
	
	fso.writeline("<img src='"+ ImpostaProgetto +"/images/LogoSE_333x137.png' align=left width='333' height='137'>")
	
	fso.writeline("<table border='0' align='center' cellspacing='0' cellpadding='0' width='500' >" )
	
	fso.writeline("	<tr>" )
	fso.writeline("		<TD>&nbsp;</TD>" )
	fso.writeline("	</TR>" )
	fso.writeline("	<tr>" )
	fso.writeline("		<TD>&nbsp;</TD>" )
	fso.writeline("	</TR>" )
	
	fso.writeline("	<tr>" )
	fso.writeline("		<TD>&nbsp;</TD>" )
	fso.writeline("	</TR>" )
	fso.writeline("	<tr>" )
	fso.writeline("		<TD>&nbsp;</TD>" )
	fso.writeline("	</TR>" )
	
	fso.writeline("	<tr>" )
	fso.writeline("		<TD>&nbsp;</TD>" )
	fso.writeline("	</TR>" )
	fso.writeline("	<tr>" )
	fso.writeline("		<TD>&nbsp;</TD>" )
	fso.writeline("	</TR>" )
	
	fso.writeline("</table>" )	
	fso.writeline("<Form name=formName1><table border='0' align='center' cellspacing='0' cellpadding='0' width='500' >" )
	fso.writeline("	<tr>" )
	fso.writeline("		<TD>&nbsp;</TD>" )
	fso.writeline("	</TR>" )
	fso.writeline("</table>")
end sub


sub CorpoDicCertificazione(NroCita,fso, Nombre, Apellido,cuil,cdocumento,Fecha,HoraDesde,horahasta,OficinaNombre,OficinaLugar,OficinaMunicipio,smes)
	fso.writeline("<table border='0' align='center' cellspacing='0' cellpadding='0' width='500' >" )
	fso.writeline("	<tr>" )
	fso.writeline("		<TD>&nbsp;</TD>" )
	fso.writeline("	</TR>" )
	fso.writeline("	<tr>" )
	fso.writeline("		<TD align='center'><B></B></TD>" )
	fso.writeline("	</TR>" )
	fso.writeline("	<tr>" )
	fso.writeline("		<TD align='center'><B>NRO DE CITA : " & NroCita & "</B></TD>" )
	fso.writeline("	</TR>" )
	
	fso.writeline("	<tr>" )
	fso.writeline("		<TD>&nbsp;</TD>" )
	fso.writeline("	</TR>" )
	fso.writeline("	<tr>" )
	'fso.writeline("	<tr>" )
	fso.writeline("		<TD align='justify'><I><b>" & Nombre & " " & Apellido & "</b></i> con <I><b>" & cDocumento & "</b></i> tiene programada una cita el d�a <b>" & Fecha & "</b> a las <b>" & horadesde & "</b>, en este Centro de Atenci�n (<I><b>" & OficinaNombre & "</b></i>), situado en <b>" & oficinalugar & "</b>.</TD>" )
	'fso.writeline("	<tr>" )
	'fso.writeline("		<TD align='left'>en este Centro de Atenci�n (<I><b>" & OficinaNombre & "</b></i>) situado en <b>" & oficinalugar & "</b>.</TD>" )
	fso.writeline("	</TR>" )
		
	fso.writeline("	<tr>" )
	fso.writeline("		<TD>&nbsp;</TD>" )
	fso.writeline("	</TR>" )
	
	fso.writeline("	<tr>" )
	fso.writeline("		<TD align='left'><b>" & oficinaMunicipio & "</b> el d�a <b>" & day(date) & "</b> del mes de <b>" &  smes & "</b> de <b>" & year(date) & "</b>. Para constancia se firman dos copias. " & "</TD>" )
	fso.writeline("	</TR>" )
	fso.writeline("</table>")
end sub

sub Privacy(fso)
	fso.writeline("<table border='0' align='center' cellspacing='0' cellpadding='0' width='500' >" )
	fso.writeline("	<tr>" )
	fso.writeline("		<TD  colspan=2>&nbsp;" )
	fso.writeline("		</TD>" )
	fso.writeline("	</tr>" )
	fso.writeline("	<tr>" )
	fso.writeline("		<TD  colspan=2>&nbsp;" )
	'fso.writeline("		<!--#include Virtual = ""/TESTI/AUTORIZZAZIONI/Info_Privacy.htm""-->" )
	fso.writeline("	</TD></TR>" )
	fso.writeline("	</Table>" )
end sub

sub TimbroFirma(fso, Nombre, Apellido,NroEntrevista)
'sub TimbroFirma(fso, Nombre, Apellido,OficinaMunicipio,sMes)
	fso.writeline("<table border='0' align='center' cellspacing='0' cellpadding='0' width='500' >" )
	fso.writeline("	<tr>" )
	fso.writeline("		<TD colspan=2>&nbsp;</TD>" )
	fso.writeline("	</TR>" )
	
	fso.writeline("	<tr>" )
	fso.writeline("		<TD  align='left'><font size=2>Firma y Aclaraci�n del Solicitante</font></TD>" )
	fso.writeline("		<TD  align='left'><font size=2>Sello, firma y aclaraci�n del entrevistador</font></TD>" )
	fso.writeline("	</TR>" )
	
	fso.writeline("	<tr>" )
	fso.writeline("		<TD align='justify'><font size=1>" & Nombre & " " & Apellido & "</font></TD>" )
	fso.writeline("		<TD align='left' ><font size=2>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; de la Oficina de Empleo</font></TD>" )
	fso.writeline("	</TR>" )
	
	fso.writeline("	<tr>" )
	fso.writeline("		<TD  colspan=2>&nbsp;" )
	fso.writeline("		</TD>" )
	fso.writeline("	</tr>" )
	
	fso.writeline("	<tr>" )
	fso.writeline("		<TD  align='left'><font size=2>N�mero de Entrevista:<b>" & nroentrevista & "</b></font></TD>" )
	fso.writeline("		<TD  align='left'><font size=2></font></TD>" )
	fso.writeline("	</TR>" )

	fso.writeline("		<TD>&nbsp;</TD>" )
	fso.writeline("		<TD>&nbsp;</TD>" )
	fso.writeline("	</TR>" )
	
	fso.writeline("	<tr>" )
	fso.writeline("		<TD colspan=2>&nbsp;</TD>" )
	fso.writeline("	</TR></table>" )
	
	fso.writeline("</Form>")
end sub

%>

