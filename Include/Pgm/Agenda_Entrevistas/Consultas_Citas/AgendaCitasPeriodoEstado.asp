<!-- #include Virtual="/strutt_testa2.asp" -->
<!-- #include Virtual="/Pgm/Iscr_Utente/clsInterfaseDB.asp"-->

<table border="0" width="530" cellspacing="0" cellpadding="0" height="81">
   <tr>
     <td width="530" background="<%=Session("Progetto")%>/images/titoli/strumenti2g.gif" height="81" valign="bottom" align="right">
       <table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
         <tr>
          <td width="100%" valign="top" align="right">
          		<b class="tbltext1a">Citas</b>
          </td>
         </tr>
       </table>
     </td>
   </tr>
</table>
<br>
<table class="SFONDOCOMM" cellpadding="0" cellspacing="0" width="500" border="0">
	<tr height="18">
		<td class="sfondomenu" height="18" width="67%"> <span class="tbltext0"> 
		<b>Visualizar Reporte de Citas por Estado</b>
		</td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
		<td valign="middle" align="right" class="tbltext1" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">
		
		</td>
    </tr>
    <tr>
	<td colspan=3>
		<p class="tbltext1">Ingresar la información solicitada.</p>
	</td>
	</tr>
    
	<tr>
	<td valign="middle" align="right" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1"></td>
	</tr>
	 
	<tr height="2">
		<td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif">
		</td>
	</tr>
</table>

<!--Inicio del Formulario-->
<script language="javascript">
<!--#include virtual = "/Include/ControlDate.inc"-->

function ControlarDatos()
{

	if (document.FrmDatos.txtFechaIni.value == "")
	{
		alert("Debe ingresar una Fecha Inicial");
		return false;
	}	

	if (document.FrmDatos.txtFechaFin.value == "")
	{
		alert("Debe ingresar una Fecha Final");
		return false;
	}	
	
	if (!ValidateInputDate(document.FrmDatos.txtFechaIni.value))
	{
		return false;
	}
	
	if (!ValidateInputDate(document.FrmDatos.txtFechaFin.value))
	{
		return false;
	}
	
	if (!ValidateRangeDate(document.FrmDatos.txtFechaIni.value,document.FrmDatos.txtFechaFin.value))
	{
		alert("Debe ingresar  Fecha Inicial Menor o Igual que Fecha  Final");
		return false;
	}
	
	
	
	
	
}


function ControlarDatos1()
{

	
	if (document.FrmDatos.CmbEstadoConvenio.value == "")
	{
		alert("Debe elegir un estado");
		return false;
	}

}
</script>

<br>
<br>

<%
Dim Inter

Set Inter = New InterfaceBD

%>
	<table width="70%">
		<form name="FrmDatos" action="MostrarTotalesCitasEstado.asp" method="post" OnSubmit= 'return ControlarDatos();'>
			
			<tr>
				<td class="tbltext1"><b>Fecha Desde</b></td>
				<td><input type="text" class="tbltext1" maxlength=10 size=14 name="txtFechaIni"</td>
			</tr>

			<tr>
				<td class="tbltext1"><b>Fecha Hasta</b></td>
				<td><input type="text" class="tbltext1" maxlength=10 size=14 name="txtFechaFin"</td>
			</tr>
			<tr>
				<td colspan=2 align = right><br>
					<input type = "submit" class="my" value="Mostrar Reporte" id=submit1 name=submit1>
				</td>
			</tr>
		</form>
	</table>

<%
Set Inter = nothing
%>

<!-- #include Virtual="/strutt_coda2.asp" -->