<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #include Virtual="/strutt_testa2.asp" -->
<!-- #include virtual="/include/OpenConn.asp" -->
<!-- #include virtual="/include/DecCod.asp" -->

<!--*************************** INCLUDE BASE 64 FUNCTION ****************** -->
<!-- #include virtual="/include/Base64.asp" -->
<!--*********************************************************************** -->

<!--*********************************************************************** -->
<!--Busca a la persona o grupo que le queremos enviar un mensaje ' RKL-->
<!--***********************************************************************-->
<%
	'@LANGUAGE="VBSCRIPT" CODEPAGE="65001" 
	'Session.Codepage=65001
	'Response.ContentType = "text/HTML" 
	'Response.CodePage = 65001
	'Response.CharSet = "utf-8"


	'****************************************************************************************************
	'Cargando data Grupo Ocupacional
	'sSQLOcupationalGroups = "SELECT ID_AREAPROF, DENOMINAZIONE FROM AREE_PROFESSIONALI ORDER BY DENOMINAZIONE ASC"
	'set rsOcupationalGroups = CC.Execute(sSQLOcupationalGroups)
	'****************************************************************************************************


	sSQLTotal = "SELECT COUNT (COD_FISC) AS TotalEmpresas " &_
				" FROM ( select " &_
				"dbo.PERSONA.COD_FISC,  " &_
				"dbo.PERSONA.COGNOME,  " &_
				"dbo.PERSONA.NOME,  " &_
				"dbo.PERSONA.DT_NASC,  " &_
				"dbo.RICHIESTA_CANDIDATO.ID_RICHIESTA,  " &_
				"dbo.RICHIESTA_SEDE.OFFER_TITLE,  " &_
				"dbo.RICHIESTA_CANDIDATO.COD_ESITO_SEL,  " &_
				"dbo.TADES.DESCRIZIONE  " &_

				"FROM  " &_
				"dbo.RICHIESTA_SEDE WITH(NOLOCK)  " &_
				"INNER JOIN dbo.RICHIESTA_CANDIDATO WITH(NOLOCK) ON dbo.RICHIESTA_CANDIDATO.ID_RICHIESTA = dbo.RICHIESTA_SEDE.ID_RICHIESTA  " &_
				"INNER JOIN dbo.PERSONA WITH(NOLOCK) ON dbo.RICHIESTA_CANDIDATO.ID_PERSONA = dbo.PERSONA.ID_PERSONA  " &_
				"INNER JOIN dbo.TADES WITH(NOLOCK) ON dbo.RICHIESTA_CANDIDATO.COD_ESITO_SEL = dbo.TADES.CODICE  " &_
				"WHERE  " &_
				"dbo.TADES.NOME_TABELLA = 'ESSEL' " 
				if request("tipDoc") <> "" and request("doc_identidad") <> "" then
					sSQLTotal = sSQLTotal & "AND persona.COD_FISC like'%" & request("tipDoc")&request("doc_identidad") & "%' "
				end if
				if request("nombre") <>"" then
					sSQLTotal = sSQLTotal & "AND persona.nome LIKE '%" & request("nombre") & "%' "
				end if
				if request("apellido") <>"" then
					sSQLTotal = sSQLTotal & "AND persona.cognome like '" & request("apellido") & "' "
				end if
				if request("vacante") <>"" then
					sSQLTotal = sSQLTotal & "AND RICHIESTA_CANDIDATO.id_richiesta = " & request("vacante") & " "
				end if
				if request("estado") <>"" then
					sSQLTotal = sSQLTotal & "AND RICHIESTA_CANDIDATO.COD_ESITO_SEL = '" & request("estado") & "' "
				end if
				sSQLTotal = sSQLTotal & " ) as b "
	
	'response.WRITE sSQLTotal
	'RESPONSE.end
	set rsTotal = CC.Execute(sSQLTotal)

	'Consulta Global
	sSQLExist = "SELECT " &_
				"dbo.PERSONA.COD_FISC,  " &_
				"dbo.PERSONA.COGNOME,  " &_
				"dbo.PERSONA.NOME,  " &_
				"dbo.PERSONA.DT_NASC,  " &_
				"dbo.RICHIESTA_CANDIDATO.ID_RICHIESTA,  " &_
				"dbo.RICHIESTA_SEDE.OFFER_TITLE,  " &_
				"dbo.RICHIESTA_CANDIDATO.COD_ESITO_SEL,  " &_
				"dbo.TADES.DESCRIZIONE  " &_

				"FROM  " &_
				"dbo.RICHIESTA_SEDE WITH(NOLOCK)  " &_
				"INNER JOIN dbo.RICHIESTA_CANDIDATO WITH(NOLOCK) ON dbo.RICHIESTA_CANDIDATO.ID_RICHIESTA = dbo.RICHIESTA_SEDE.ID_RICHIESTA  " &_
				"INNER JOIN dbo.PERSONA WITH(NOLOCK) ON dbo.RICHIESTA_CANDIDATO.ID_PERSONA = dbo.PERSONA.ID_PERSONA  " &_
				"INNER JOIN dbo.TADES WITH(NOLOCK) ON dbo.RICHIESTA_CANDIDATO.COD_ESITO_SEL = dbo.TADES.CODICE  " &_
				"WHERE  " &_
				"dbo.TADES.NOME_TABELLA = 'ESSEL' " 
				if request("tipDoc") <> "" and request("doc_identidad") <> "" then
					sSQLExist = sSQLExist & "AND persona.COD_FISC like '%" & request("tipDoc")&request("doc_identidad") & "%' "
				end if
				if request("nombre") <>"" then
					sSQLExist = sSQLExist & "AND persona.nome LIKE '%" & request("nombre") & "%' "
				end if
				if request("apellido") <>"" then
					sSQLExist = sSQLExist & "AND persona.cognome like '" & request("apellido") & "' "
				end if
				if request("vacante") <>"" then
					sSQLExist = sSQLExist & "AND RICHIESTA_CANDIDATO.id_richiesta = " & request("vacante") & " "
				end if
				if request("estado") <>"" then
					sSQLExist = sSQLExist & "AND RICHIESTA_CANDIDATO.COD_ESITO_SEL = '" & request("estado") & "' "
				end if
				
	sSQLExist = sSQLExist & "ORDER BY PERSONA.COGNOME"

	'response.WRITE sSQLExist
	'RESPONSE.end


	'response.Write "*********************************************************"
	'response.Write "NOMBRE EMRESA -- " & request("nombre_empresa") & "</br>"
	'response.Write "SEDE EMPRESA -- " & request("sede_empresa") & "</br>"
	'response.Write "DEPARTAMENTO -- " & request("departamento") & "</br>"
	'response.Write "CIUDAD -- " & request("ciudad") & "</br>"
	'response.Write "FECHA INSCRIPCIÓN FROM -- " & request("fecha_inscripcion_from") & "</br>"
	'response.Write "FECHA INSCRIPCIÓN TO -- " & request("fecha_inscripcion_to") & "</br>"
	'response.Write "*********************************************************"
		
	
	'rsRegisteredPostuldos.CursorType = adOpenstatic 
	set rsRegisteredPostuldos = CC.Execute(sSQLExist)

	if request("search") = "search" then
		session("pagina") = 1
	end if

	rsRegisteredPostuldos.pagesize=10

	if rsRegisteredPostuldos.pagecount <> 0 then
		if session("pagina")>rsRegisteredPostuldos.pagecount then
				session("pagina")=rsRegisteredPostuldos.pagecount 'evitamos el error de reload
		end if

		' controlando avance de pagina
		
		dim direccion 
		'if request("direccion") = "" then		
	''		direccion = "Atras"
	''	else
			direccion = request("direccion")
	''	end if

		
		if session("pagina") = "" then
			session("pagina") = 1
		else

			if direccion = "Adelante" and session("pagina")<rsRegisteredPostuldos.pagecount then
				session("pagina") = session("pagina") + 1
			else 
				if direccion = "Atras" and session("pagina")>1 then
					session("pagina") = session("pagina") - 1
				end if	 
			end if	 
		end if

		if session("pagina")<>"" then
			rsRegisteredPostuldos.absolutepage=session("pagina")
		else 
			rsRegisteredPostuldos.absolutepage=1
		end if

		inicio=1+(session("pagina")-1)*rsRegisteredPostuldos.pagesize
		fin=inicio+rsRegisteredPostuldos.pagesize - 1
		if fin > rsRegisteredPostuldos.recordcount then
			fin =rsRegisteredPostuldos.recordcount
		end if

	end if 

	
	function roundUp(x)
	   If x > Int(x) then
	    roundup = Int(x) + 1
	   Else
	    roundup = x
	   End If
	End Function

%>

<script language="Javascript"> 
$(function(){

	$("#form_next").submit(function(){
		$("#doc_identidad_next").val($("#doc_identidad").val());
		$("#nombre_next").val($("#nombre").val());
		$("#apellido_next").val($("#apellido").val());
		$("#vacante_next").val($("#vacante").val());
		$("#estado_next").val($("#estado").val());
		return true;
	});


	$("#next").click(function(){
		$("#form_next").submit();		
	});

	$("#form_back").submit(function(){
		$("#doc_identidad_back").val($("#doc_identidad").val());
		$("#apellido_back").val($("#apellido").val());
		$("#nombre_back").val($("#nombre").val());
		$("#vacante_back").val($("#vacante").val());
		$("#estado_back").val($("#estado").val());
		return true;
	});
	

	$("#back").click(function(){
		$("#form_back").submit();
	});	


});
</script>




<form name="cerca" id="form_search" action="reportPostuEstad.asp" Method="Post">
	</br>
	<table border="0" width="700" CELLPADDING="0" cellspacing="0">
		<tr>
			<td align="left" class="sfondomenu" width="300">
				<span class="tbltext0">
				<b>&nbsp;REPORTE POSTULADOS Y SUS ESTADOS</b></span>
			</td>
			<td width="25" valign="bottom" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">
				<img border="0" src="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
			<td width="40%" valign="middle" align="right" class="tbltext1" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
		</tr>
	</table>
		
	<table border="0" width="700" CELLPADDING="0" cellspacing="0">
	    <tr>
			<td class="sfondocomm" align="left" class="tbltext1">
			En esta secci&oacute;n se puede efectuar una b&uacute;squeda de los postulados.</br></br>
			<!-- Puede filtrar la b&uacute;squeda ingresando el apellido y nombre del usuario. -->
			</td>
			<td class="sfondocomm">
				<a href="Javascript:Show_Help('/Pgm/Help/ProjectSite/Rubrica/default/')">
				<img src="<%=Session("Progetto")%>/images/Help.gif" border="0" align="right" alt="Help">
			</td> 
	    </tr>

		<tr height="2">
			<td class="sfondocomm" width="100%" colspan="2" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>
	<table width="700" border="0" class="sfondocomm">
		<tr class="sfondocomm">
			<td colspan="2"><b>B&Uacute;SQUEDA DE POSTULADOS</b></td>		
		</tr>
		<tr style="text-align:center;">
			<td class="tbltext1" width="20%"><b>Tipo Documento</b></td>			
			<td class="tbltext1" width="20%"><b>Documento</b></td>
			<td rowspan="4" width="20%">
		   		<input type="image" alt="Cerca" src="<%=Session("Progetto")%>/images/lente.gif" id="search" name="search" value="search">
			</td>
		</tr>
		<tr style="text-align:center;">
			<td class="tbltext1">
			<select name="tipDoc" id="tipDoc" Class="textblack">
			<% sqlTipDoc = "select * from TADES where TADES.NOME_TABELLA = 'DOCST' order by DESCRIZIONE " %>
			<% set rsTipDoc = CC.Execute(sqlTipDoc) %>
			<% contador=0
				
				%><option value=""></option><%
				Do While contador < rsTipDoc.pagesize and NOT rsTipDoc.EOF%>
				  <% if request("tipDoc")<>"" and request("tipDoc")=rsTipDoc("CODICE") then %>
					<option value=<%= rsTipDoc("CODICE")%> selected><%= rsTipDoc("DESCRIZIONE")%></option>
				  <% else %>
				  	<option value=<%= rsTipDoc("CODICE")%> ><%= rsTipDoc("DESCRIZIONE")%></option>
				<% end if
				rsTipDoc.MoveNext
				contador=contador+1
				Loop
			%>
			</select>
			</td>
			<td class="tbltext1">
				<input type="text" Class="textblack" id="doc_identidad" name="doc_identidad" value="<%=request("doc_identidad")%>" maxlength="50" />
			</td>
			<td width="20%" rowspan="2" style="vertical-align: middle; text-align: center; margin-left: auto; margin-right: auto; align: center;">
				<div style="margin:auto; border-radius:10px; padding-top:8px; padding-bottom:8px; width:80%; text-align:center;" class="tbltext0 tblsfondo3"><b>TOTAL POSTULADOS</b> </br> <% response.Write rsTotal("TotalEmpresas") %></div>
			</td>
		</tr>
		<tr style="text-align:center;">
			<td class="tbltext1" width="20%"><b>Nombre</b></td>
			<td class="tbltext1" width="20%"><b>Apellido</b></td>
		</tr>
		<tr style="text-align:center;">
			<td class="tbltext1">
				<input type="text" Class="textblack" id="nombre" name="nombre" value="<%=request("nombre")%>" maxlength="50" />
			</td>
			<td class="tbltext1">
				<input type="text" Class="textblack" id="apellido" name="apellido" value="<%=request("apellido")%>" maxlength="50" />
			</td>
		</tr>
		<tr style="text-align:center;">
			<td class="tbltext1" width="20%"><b>No. vacante</b></td>
			<td class="tbltext1" width="20%"><b>Estado</b></td>
			
		</tr>
		<tr style="text-align:center;">
			<td class="tbltext1">
				<input type="text" Class="textblack" id="vacante" name="vacante" value="<%=request("vacante")%>" maxlength="50" />
			</td>
			<td class="tbltext1">
				<select name="estado" id="estado" Class="textblack">
				<% sqlEstado = "select * from TADES where TADES.NOME_TABELLA = 'ESSEL' order by DESCRIZIONE " %>
				<% set rsEstado = CC.Execute(sqlEstado) %>
				<% contador=0
					
					%><option value=""></option><%
					Do While contador < rsEstado.pagesize and NOT rsEstado.EOF%>
					  <% if request("estado")<>"" and request("estado")=rsEstado("CODICE") then %>
						<option value=<%= rsEstado("CODICE")%> selected><%= rsEstado("DESCRIZIONE")%></option>
					  <% else %>
					  	<option value=<%= rsEstado("CODICE")%> ><%= rsEstado("DESCRIZIONE")%></option>
					<% end if
					rsEstado.MoveNext
					contador=contador+1
					Loop
				%>
				</select>
			</td>
		</tr>
	</table>	
</form>
</br>


<% if not rsRegisteredPostuldos.EOF then %>  
		</br></br>
		<table border="0" width="500">
			<tr class="tbltext0 tblsfondo3" style="text-align:center;">
				<td width="10%" style="border-radius:10px 0 0 0;"><b>Documento</b></td>
				<td width="12%"><b>Apellido</b></td>
				<td width="12%"><b>Nombre</b></td>
				<td width="10%"><b>Fecha de nacimiento</b></td>
				<td width="10%"><b>No. Vacante</b></td>
				<td width="16%"><b>Oferta</b></td>
				<td width="10%"><b>Cod. Estado</b></td>
				<td width="10%" style="border-radius:0 10px 0 0;"><b>Estado</b></td>
			</tr>
		<!-- montamos el bucle para mostrar los registros -->
		<% contador=0
		Do While contador < rsRegisteredPostuldos.pagesize and NOT rsRegisteredPostuldos.EOF%>
			<tr class="tblsfondo">
				<td style="text-align:center;"><%= rsRegisteredPostuldos("COD_FISC")%></td>
				<td><%= rsRegisteredPostuldos("COGNOME") %></td>
				<td><%= rsRegisteredPostuldos("NOME") %></td>
				<td><%= rsRegisteredPostuldos("DT_NASC") %></td>
				<td><%= rsRegisteredPostuldos("ID_RICHIESTA") %></td>
				<td><%= rsRegisteredPostuldos("OFFER_TITLE") %></td>
				<td><%= rsRegisteredPostuldos("COD_ESITO_SEL") %></td>
				<td><%= rsRegisteredPostuldos("DESCRIZIONE") %></td>
			</tr>

		<% rsRegisteredPostuldos.MoveNext
		contador=contador+1
		Loop
		%>

		</table>

	<!-- mostramos los botones de adelante y atras segun proceda -->
	<table border="0" width="100%" class="tbltext0 tblsfondo3" style="margin:auto;">
		<tr>
			<td width="10px" style="text-align:left;">
				<%if session("pagina") <> 1 then %>
					<form method="POST" action="reportPostuEstad.asp" id="form_back">
						<p>
							<a href="#" id="back">
								<img type="image" src="../../images/previous.png" width="24px" height="24px" style="border:0px;">
								<input type="hidden" alt="Atras" value="Atras" src="../../images/previous.png" id="imageBack" name="direccion" >
							</a>
							<!-- input type="image" id="back" alt="Atras" value="Atras" src="../../images/previous.png" id="imageBack" name="direccion" width="24px" height="24px" -->
							<input type="hidden" Class="textblack" id="doc_identidad_back" name="doc_identidad" value="<%=request("doc_indentidad")%>" maxlength="50" />
							<input type="hidden" Class="textblack" id="nombre_back" name="nombre" value="<%=request("nombre")%>" maxlength="50" />
							<input type="hidden" Class="textblack" id="apellido_back" name="apellido" value="<%=request("apellido")%>" maxlength="50" />
							<input type="hidden" Class="textblack" id="vacante_back" name="vacante" value="<%=request("vacante")%>" maxlength="50" />
							<input type="hidden" Class="textblack" id="estado_back" name="estado" value="<%=request("estado")%>" maxlength="50" />
						</p>
					</form>
				<%end if%>
			</td>
			<td width="40%" style="text-align:left;">Registros 
				<span style="color:#FF0000;"><%=inicio%></span> al 
				<span style="color:#FF0000;"><%=fin%></span> de un total de
				<span style="color:#FF0000;"><%=rsRegisteredPostuldos.recordcount%></span>
			</td>
			<td width="40%" style="text-align:right;">P&aacute;gina 
				<span style="color:#FF0000;"><%=session("pagina")%></span> de 
				<span style="color:#FF0000;"><%=rsRegisteredPostuldos.pagecount%></span>
			</td>
			<td width="10%" style="text-align:right;">
				<% if session("pagina") < rsRegisteredPostuldos.pagecount then %>
					<form method="POST" name="reportPostuEstad.asp" id="form_next">
						<p>
							<a href="#" id="next">
								<img type="image" src="../../images/next.png" width="24px" height="24px" style="border:0px;">
								<input type="hidden" alt="Adelante" value="Adelante" src="../../images/next.png" id="imageNext" name="direccion" >
							</a>
							<!-- input type="image" alt="Adelante" value="Adelante" src="../../images/next.png" id="imageNext" name="direccion" width="24px" height="24px" -->

							<input type="hidden" Class="textblack" id="doc_identidad_next" name="doc_identidad" value="<%=request("doc_identidad")%>" maxlength="50" />
							<input type="hidden" Class="textblack" id="nombre_next" name="nombre" value="<%=request("nombre")%>" maxlength="50" />
							<input type="hidden" Class="textblack" id="apellido_next" name="apellido" value="<%=request("apellido")%>" maxlength="50" />							
							<input type="hidden" Class="textblack" id="vacante_next" name="vacante" value="<%=request("vacante")%>" maxlength="50" />							
							<input type="hidden" Class="textblack" id="estado_next" name="estado" value="<%=request("estado")%>" maxlength="50" />							
						</p>
					</form>
				<%end if%>
			</td>
		</tr>
	</table>

	<%
		'******************************************************* EXPORTAR A EXCEL *******************************************************
		Dim encabezadosExcel 
		encabezadosExcel = "Documento,Apellido,Nombre,Fecha de nacimiento,No. Vacante,Oferta,Cod. Estado,Estado"
		encabezadosExcelBase64 = Base64Encode(encabezadosExcel)

		Dim encabezadosQuery 
		encabezadosQuery = "COD_FISC,COGNOME,NOME,DT_NASC,ID_RICHIESTA,OFFER_TITLE,COD_ESITO_SEL,DESCRIZIONE"

		encabezadosQueryBase64 = Base64Encode(encabezadosQuery)

		Dim reportFilterData 
		reportFilterData = "doc_identidad:"&request("TipDoc")&request("doc_identidad")&",nombre:"&request("nombre")&",apellido:"&request("apellido")&",vacante:"&request("vacante")&",estado:"&request("estado")
		reportFilterDataBase64 = Base64Encode(reportFilterData)
		'********************************************************************************************************************************

	%>

	<input type="button" value="Exportar a Excel" onclick="javascript:exporttoExcelGeneric('reportPostuEstad','REPORTE POSTULADOS Y SUS ESTADOS','<%=reportFilterDataBase64%>','<%=encabezadosExcelBase64%>','<%=encabezadosQueryBase64%>')">


<% else%>
	<h1 class="tbltext0 tblsfondo3" style="width:80%; border-radius:10px; font-size:20px;">No se encontraron coincidencias</h1>
<% end if %>

<!-- #include Virtual="/strutt_coda2.asp" -->
<!-- #include virtual="/include/CloseConn.asp" -->



