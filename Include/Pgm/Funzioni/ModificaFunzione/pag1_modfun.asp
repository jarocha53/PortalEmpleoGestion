<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #include Virtual="/strutt_testa2.asp" -->
<!-- #include virtual="/include/openconn.asp" -->
<!-- #include virtual="/util/portallib.asp" -->
<!-- #include virtual="/util/globalsub.asp" -->
<%
If Not ValidateService(Session("IdUtente"),"Gestione Funzioni",cc) Then 
	Response.Redirect "/util/error_login.asp"
End If

If Session("Msg_Error") = "" Then
	Session("ck_idfunzione")=""
	Session("ck_desfunzione")=""
	Session("ck_oldidfunzione")=""
	Session("ck_gruppofunzione")=""
	Session("ck_fungru")=""
	Session("ck_funute")=""
	Session("ck_acronimo")=""
	Session("ck_webpathfun")=""
	Session("ck_modpers")=""
	Session("ck_varinput")=""
	Session("ck_webpathdoc")=""
End If
		
	valfunzione= Request.QueryString("idfun")
   If valfunzione <> "" Then
      Session("ck_idfunzione")= valfunzione 
   End If  
%>
<script>
<!--#include Virtual = "/Include/help.inc"-->

<%
if Session("Msg_Error") <> "" then
		response.write "alert('"&Session("Msg_Error")&"');"
		Session("Msg_Error")=""
  end if
%>
<!--
	function checkField(frm){
			
			if (frm.ndesfunzione.value==""){
				alert("Il campo Descrizione � obbligatorio");
				frm.ndesfunzione.focus();
				return false;
			}else{
				frm.ndesfunzione.focus();
				if (notValidChar(frm.ndesfunzione.value)) return false;
			}
			if (frm.nacronimo.value==""){
				alert("Il campo Nome funzione � obbligatorio");
				frm.nacronimo.focus();
				return false;
			}
			if (frm.ngruppofunzione.value==""){
				alert("Il campo Servizio apparteneza � obbligatorio");
				frm.ngruppofunzione.focus();
				return false;
			}		
			if (frm.nwebpathfun.value==""){
				alert("Il campo Webpath funzione � obbligatorio");
				frm.nwebpathfun.focus();
				return false;
			}			
			if (frm.nwebpathdoc.value==""){
				alert("Il campo Webpath documento � obbligatorio");
				frm.nwebpathdoc.focus();
				return false;
			}
				
			return confirm("Confermi la modifica della funzione?");
		
	}
	function notValidChar(cadena){
		var cars="\\/\"?<>|:*";
		var i, longitud;
		longitud = cadena.length;
		if (longitud==0) return true;
		for (i=0;i<longitud;i++){
			if (cars.indexOf(cadena.charAt(i)) > -1){
				alert ("Il campo Descrizione non pu� contenere i seguenti caratteri : "+cars);
				return true;
			}
		}
		return false;
	}
	
function reload(Form){
	 val= funzione.oldidfunzione.value
	 location.href = "pag1_modfun.asp?idfun=" + val
	}
//-->
</script>
<br>
<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
	<tr height="18">
		<td class="sfondomenu" width="67%" height="18"><span class="tbltext0"><b>&nbsp;Configuraci�n del Sistema - Modificar Funci�n</b></span></td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
		<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">(*) campi obbligatori</td>
	</tr>
<% If Session("ck_idfunzione") <> "" Then %>
	<tr>
		<td class="sfondocomm" width="100%" colspan="3"><br>
		<p>En esta secci�n es posible efectuar la modificaci�n de una o m�s funciones.
		Es posible modificar la descripci�n de la funcion de un Servicio de pertenencia (ej. alfabetizaci�n, formaci�n, etc.), il percorso fisico  
		da dove dovr� essere attivata, le eventuali variabili utilizzate e il percorso dove � posizionato il  documento di help della funzione. </p> 
		Occorre tener presente che i campi Nome Funzione e Descrizione sono univoci e 
		quindi non potr� essere creata una funzione che abbia un nome e/o una descrizione gi� appartenente ad un'altra.
		<a href="Javascript:Show_Help('/Pgm/help/Funzioni/ModificaFunzione/Pag1_ModFun_1')" name onmouseover="javascript:status='' ; return true">
     	     <img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
        </td>
	</tr>
<% End If %>
	<tr height="17">
		<td class="sfondocomm" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
	</tr>
</table>
<br>
<form method="post" action="pag1fun_modfun.asp" name="funzione" onsubmit="return checkField(this);">
<%
If Session("ck_idfunzione") = "" Then
%>
<table border="0" width="500" class="sfondocomm">
	<tr height="30">
		<td width="480" align="left" valign="middle">&nbsp;<b>Busqueda de Funci�n a modificar</b>
		     <a href="Javascript:Show_Help('/Pgm/help/Funzioni/ModificaFunzione/Pag1_ModFun')" name onmouseover="javascript:status='' ; return true">
     	     <img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
        </td>
	</tr>   
</table>
<br>
<table border="0" cellpadding="0" cellspacing="1" width="500">
	 <tr height="20">
		<td align="left" class="tbltext1" colspan="2"><b>Seleccionar una funci�n de las existentes</b></td>
    </tr>
	<tr>
		<td height="25" class="tbltext1" align="left">Funciones existentes*</td>
		<td>
			<select name="oldidfunzione" onchange="reload(this)">
			  <option value="0"></option> 
<%  
				Set rstFunzione = Server.CreateObject("ADODB.RECORDSET")
				SQLFunzione = "SELECT idfunzione, desfunzione FROM funzione where desfunzione <> 'Gestione portale' ORDER BY desfunzione"
				SQLFunzione = UCase(SQLFunzione)
'PL-SQL * T-SQL  
SQLFUNZIONE = TransformPLSQLToTSQL (SQLFUNZIONE) 
				rstFunzione.open SQLFunzione,CC,1,3
				rstFunzione.MoveFirst
 
				While rstFunzione.EOF <> True 
				    response.write "<option value='" & rstFunzione.fields("idfunzione") & "'>" & rstFunzione.fields("desfunzione") & "</option>"
				    rstFunzione.MoveNext
				Wend
				rstFunzione.close
				Set rstFunzione = nothing
%>
			</select>
		</td>
	</tr>
</table>
<br>
<table border="0" cellpadding="2" cellspacing="1" width="500">
	<tr>
	   <td width="400"></td>	
	   <td height="25" class="sfondocomm"><b>Paso 1/2</b></td>
	</tr>
</table>
<%
End If
If Session("ck_idfunzione") <> "" Then
	  Set rstFunzione = Server.CreateObject("ADODB.RECORDSET")
	  SQLFunzione = "SELECT idfunzione,desfunzione,gruppofunzione,acronimo,webpathfun,modpres,varinput,webpathdoc FROM funzione WHERE idfunzione=" & Session("ck_idfunzione") 
      SQLFunzione = UCase(SQLFunzione)
'PL-SQL * T-SQL  
SQLFUNZIONE = TransformPLSQLToTSQL (SQLFUNZIONE) 
	  rstFunzione.open SQLFunzione,CC,1,3
	  rstFunzione.MoveFirst
      
      Session("ck_desfunzione")= rstFunzione("desfunzione")
      Session("ck_gruppofunzione")= rstFunzione("gruppofunzione")
      Session("ck_acronimo")= rstFunzione("acronimo")
      Session("ck_webpathfun")= rstFunzione("webpathfun")
      Session("ck_modpres")= rstFunzione("modpres")
      Session("ck_varinput")= rstFunzione("varinput")
      Session("ck_webpathdoc")= rstFunzione("webpathdoc")
      rstFunzione.close
      Set rstFunzione=nothing      
%>

<table border="0" cellpadding="2" cellspacing="2" width="500">
  <tr>
   <td height="25" class="tbltext1" align="left">&nbsp;Descripci�n*</td>
   <td><input type="text" class="textblack" MAXLENGTH="50" name="ndesfunzione" size="50" Value="<%=Session("ck_desfunzione")%>"></td>
  </tr>
  <tr>
   <td height="25" class="tbltext1" align="left">&nbsp;Nombre de la Funci�n*</td>
   <td><input type="text" class="textblack" name="nacronimo" size="50" Value="<%=Session("ck_acronimo")%>"></td>
  </tr>
  <tr>
   <td height="25" class="tbltext1" align="left">&nbsp;Servicio de Pertenencia*</td>
   <td><input type="text" class="textblack" name="ngruppofunzione" size="50" Value="<%=Session("ck_gruppofunzione")%>"></td>
  </tr>
  <tr>
   <td height="25" class="tbltext1" align="left">&nbsp;Webpath funci�n*</td>
   <td><input type="text" class="textblack" name="nwebpathfun" size="50" Value="<%=lcase(Session("ck_webpathfun"))%>"></td>
  </tr>
  <tr>
   <td height="25" class="tbltext1" align="left">&nbsp;Nombre variable</td>
   <td><input type="text" class="textblack" name="nnomevar" size="50" Value="<%=Session("ck_varinput")%>"></td>
  </tr>
  <tr>
   <td height="25" class="tbltext1" align="left">&nbsp;Webpath Documento*</td>
   <td><input type="text" class="textblack" name="nwebpathdoc" size="50" Value="<%=lcase(Session("ck_webpathdoc"))%>"></td>
  </tr>
  <tr>
	<td>&nbsp;</td>
  </tr>
  <tr>
	<td colspan="2" align="center">
		   <input type="hidden" name="olddesfunzione" value>
		   <input type="image" alt="Invia" src="<%=Session("Progetto")%>/images/conferma.gif" id="image1" name="image1">
	</td>
   </tr>	
  </table>

<table border="0" cellpadding="2" cellspacing="1" width="500">
	<tr>
	   <td width="400"></td>	
	   <td height="25" class="sfondocomm"><b>Paso 2/2</b></td>
	</tr>
</table> 
<%
  end if
%>
</form>
<!-- #include virtual="/include/closeconn.asp" -->
<!-- #include Virtual="/strutt_coda2.asp" -->
