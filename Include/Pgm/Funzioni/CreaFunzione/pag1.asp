<!-- #include Virtual="/strutt_testa2.asp" -->
<!-- #include virtual="/include/openconn.asp" -->
<!-- #include virtual="/util/portallib.asp" -->
<!-- #include virtual="/util/globalsub.asp" -->
<%
If Not ValidateService(Session("IdUtente"),"Gestione Funzioni",cc) Then 
	Response.Redirect "/util/error_login.asp"
End If

If Session("Msg_Error") = "" Then
	Session("ck_idfunzione")=""
	Session("ck_desfunzione")=""
	Session("ck_oldidfunzione")=""
	Session("ck_gruppofunzione")=""
	Session("ck_fungru")=""
	Session("ck_funute")=""
	Session("ck_acronimo")=""
	Session("ck_webpathfun")=""
	Session("ck_modpers")=""
	Session("ck_nomevar")=""
	Session("ck_webpathdoc")=""
End If
%>
<script>
<!--#include Virtual = "/Include/help.inc"-->

<%
if Session("Msg_Error") <> "" then
		response.write "alert('"&Session("Msg_Error")&"');"
		Session("Msg_Error")=""
  end if
%>
<!--
	function checkField(frm){
	
	        if (frm.nacronimo.value==""){
				alert("Il campo Nome funzione � obbligatorio");
				frm.nacronimo.focus();
				return false;
			}
	
			if (frm.ndesfunzione.value==""){
				alert("Il campo Descrizione � obbligatorio");
				frm.ndesfunzione.focus();
				return false;
			}else{
				frm.ndesfunzione.focus();
				if (notValidChar(frm.ndesfunzione.value)) return false;
			}
			if (frm.ngruppofunzione.value==""){
				alert("Il campo Appartiene a � obbligatorio");
				frm.ngruppofunzione.focus();
				return false;
			}		
			if (frm.nwebpathfun.value==""){
				alert("Il campo Webpath funzione � obbligatorio");
				frm.nwebpathfun.focus();
				return false;
			}
			if (frm.nwebpathdoc.value==""){
				alert("Il campo Webpath doc � obbligatorio");
				frm.nwebpathdoc.focus();
				return false;
			}
			return confirm("Confermi la creazione della funzione?");
		
	}
	function notValidChar(cadena){
		var cars="\\/\"?<>|:*";
		var i, longitud;
		longitud = cadena.length;
		if (longitud==0) return true;
		for (i=0;i<longitud;i++){
			if (cars.indexOf(cadena.charAt(i)) > -1){
				alert ("Il campo Descrizione non pu� contenere i seguenti caratteri : "+cars);
				return true;
			}
		}
		return false;
	}
	
//-->
</script>
<body onload="document.funzione.nacronimo.focus();">
<br>
<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
	<tr height="18">
		<td class="sfondomenu" width="67%" height="18"><span class="tbltext0"><b>&nbsp;Configuraci�n del Sistema - Crear Funci�n</b></span></td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
		<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">(*) campo obligatorio</td>
	</tr>
	<tr>
		<td class="sfondocomm" width="57%" colspan="3">
		<br>
		<p>En esta secci�n va associata una descrizione  della funzione oltre al suo Servizio di appartenenza (es. alfabetizzazione, formazione ecc.), il percorso fisico  
		da dove dovr� essere attivata e le eventuali variabili utilizzate.  Il percorso dove � posizionato il  documento di help della funzione 
		dovr� essere immesso in Webpath Documento. </p>
		Occore tener presente che i campi Nome Funzione e Descrizione sono univoci e 
		quindi non potr� essere creata una funzione che abbia un nome e/o una descrizione gi� appartenente ad un'altra.
		     <a href="Javascript:Show_Help('/Pgm/help/Funzioni/CreaFunzione/Pag1')" name onmouseover="javascript:status='' ; return true">
     	     <img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
		</td>
	</tr>
	<tr height="17">
		<td class="sfondocomm" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
	</tr>
</table>
<form method="post" action="pag1fun.asp" name="funzione" onsubmit="return checkField(this);">
<table border="0" cellspacing="0" cellpadding="0" width="500">
	<tr height="30" class="sfondocomm"> 
		<td width="480" align="left" valign="middle"><b>Gestione Funzione</b></td>
	</tr>
</table>

<table border="0" cellpadding="2" cellspacing="2" width="500">
	<tr>
		<td height="25" class="tbltext1" align="left">&nbsp;Nome Funzione*</td>
		<td><input type="text" class="textblack" name="nacronimo" size="50" Value="<%=Session("ck_acronimo")%>"></td>
	</tr>
	<tr>
		<td height="25" class="tbltext1" align="left">&nbsp;Descrizione*</td>
		<td><input type="text" class="textblack" name="ndesfunzione" size="50" Value="<%=Session("ck_desfunzione")%>"></td>
	</tr>
	<tr>
		<td height="25" class="tbltext1" align="left">&nbsp;Servizio appartenenza*</td>
		<td><input type="text" class="textblack" name="ngruppofunzione" size="50" Value="<%=Session("ck_gruppofunzione")%>"></td>
	</tr>
	<tr>
		<td height="25" class="tbltext1" align="left">&nbsp;Webpath funzione*</td>
		<td><input type="text" class="textblack" name="nwebpathfun" size="50" Value="<%=Session("ck_webpathfun")%>"></td>
	</tr>
	<tr>
		<td height="25" class="tbltext1" align="left">&nbsp;Nome variabile</td>
		<td><input type="text" class="textblack" name="nnomevar" size="50" Value="<%=Session("ck_nomevar")%>"></td>
	</tr>
	<tr>
		<td height="25" class="tbltext1" align="left">&nbsp;Webpath Documento*</td>
		<td><input type="text" class="textblack" name="nwebpathdoc" size="50" Value="<%=Session("ck_webpathdoc")%>"></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
    <tr>
	<td colspan="2" align="center">
			<input type="hidden" name="olddesfunzione" value>
			<input type="image" alt="Invia" src="<%=Session("Progetto")%>/images/conferma.gif" name="conferma">
	</td>
   </tr>
  </table>
 <br>

</form>
<!-- #include virtual="/include/closeconn.asp" -->
<!-- #include virtual="/strutt_coda2.asp" -->