<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->

<!-- #include virtual="/util/portallib.asp" -->
<!-- #include virtual="/include/OpenConn.asp" -->
<%
If Not ValidateService(Session("IdUtente"),"Gestione Funzioni",cc) Then 
	Response.Redirect "/util/error_login.asp"
End If

Session("ck_desfunzione") = Request.Form("ndesfunzione")
Session("ck_gruppofunzione") = Request.Form("ngruppofunzione")
Session("ck_acronimo") = Request.Form("nacronimo")
Session("ck_webpathfun") = Request.Form("nwebpathfun")
		
Session("ck_nomevar") = Request.Form("nnomevar")
Session("ck_webpathdoc") = Request.Form("nwebpathdoc")	

sVarFun = Ucase(Request.Form("nacronimo"))
sVarDesc = UCase(Request.Form("ndesfunzione"))

Set rstAcrCount = server.CreateObject("ADODB.RECORDSET")
SQLAcrCount = "SELECT Count(acronimo) FROM funzione WHERE acronimo = '" & sVarFun & "'"
SQLAcrCount = UCase(SQLAcrCount)

'PL-SQL * T-SQL  
SQLACRCOUNT = TransformPLSQLToTSQL (SQLACRCOUNT) 
rstAcrCount.open SQLAcrCount,CC,1,3
If (cint(rstAcrCount.fields(0)) > 0)  Then
  varpres= 1
Else
  varpres= 0
End If 

rstAcrCount.close
Set rstAcrCount = nothing

If varpres=1 Then
  Session("Msg_Error")="Nome Funzione gi� esistente"
  response.redirect "pag1.asp"
  Response.End
End If

Set rstDesCount = server.CreateObject("ADODB.RECORDSET")
SQLDesCount = "SELECT Count(desfunzione) FROM funzione WHERE desfunzione = '" & sVarDesc & "'"
SQLDesCount = UCase(SQLDesCount)
'PL-SQL * T-SQL  
SQLDESCOUNT = TransformPLSQLToTSQL (SQLDESCOUNT) 
rstDesCount.open SQLDesCount,CC,1,3

If (cint(rstDesCount.fields(0)) > 0)  Then
  vardescpres= 1
Else
  vardescpres= 0
End If 
   
rstDesCount.close
Set rstDesCount = nothing

If vardescpres=1 Then
  Session("Msg_Error")="Descrizione gi� esistente"
  response.redirect "pag1.asp"
  Response.End
End If   
     
'Gruppo cancellare
	 		
FieldStr = " (desfunzione, gruppofunzione, acronimo, webpathfun, modpres, varinput, webpathdoc)"
ValueStr = " VALUES ('" & Session("ck_desfunzione") & "','" & Session("ck_gruppofunzione") & "','" & Session("ck_acronimo") & "', '" & Session("ck_webpathfun") & "', 0, '" & Session("ck_nomevar") & "', '" & Session("ck_webpathdoc") & "')"		
 		
SQLInFun = "INSERT INTO funzione" & FieldStr & ValueStr
		
'PL-SQL * T-SQL  
UCASESQLINFUN = TransformPLSQLToTSQL (UCASESQLINFUN) 
Set rstInFun = CC.Execute (ucase(SQLInFun))
   			
%>
	<!-- #include virtual="/include/CloseConn.asp" -->
<%
	
Session("Msg_Error")="Funzione creata correttamente"

Response.Redirect "pag1.asp"
%>
