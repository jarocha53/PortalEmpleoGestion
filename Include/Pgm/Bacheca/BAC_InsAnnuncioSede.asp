<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include Virtual = "/strutt_testa2.asp"-->
<!--#include Virtual = "/include/SysFunction.asp"-->
<!--#include Virtual = "/include/openconn.asp"-->
<!--#include virtual = "/include/DecCod.asp"-->
<!--#include virtual = "/util/portallib.asp"-->
<!--#include virtual = "/include/ControlDateVB.asp"-->

<a name="primo"></a>
<script language="javascript">
//include del file per fare i controlli sulla validit� delle date
<!--#include virtual = "/include/ControlDate.inc"-->

//include del file per fare i controlli sulla numericit� dei campi
<!--#include virtual = "/include/ControlNum.inc"-->
<!--#include virtual = "/include/ControlString.inc"-->
<!--#include virtual = "/include/help.inc"-->

//VANI 30-05-06

function RefreshPagina()
{
	Sede = document.frmBacheca.IdSede.value 
	if (document.frmBacheca.TipoPrestacion.value == "L")
	{
		window.navigate("BAC_InsAnnuncioSede.asp?TEmp="+document.frmBacheca.TEmp.value+"&TipoPrestacion="+document.frmBacheca.TipoPrestacion.value+"&IdSede="+Sede)
	}
	else
	{
		window.navigate("BAC_InsAnnuncioSede.asp?TEmp="+document.frmBacheca.TEmp.value+"&TipoPrestacion="+document.frmBacheca.TipoPrestacion.value+"&IdSede="+Sede)
	}
}


//VANI 30-05-06


//Funzione per i controlli dei campi da inserire 
function Cancella()
{
	document.frmBacheca.reset()
	return false
}

var windowArea
var windowSpecifico

function chiudi()
{   
	if (windowSpecifico != null )
	{
		windowSpecifico.close(); 
	}
	
	if (windowArea != null )
	{
		windowArea .close(); 
	}
	history.back()
}

function ControllaDati()
{		
	if (document.frmBacheca.TipoPrestacion.value == "")
	{
		alert("El campo Tipo de Prestacion es obligatorio!")
		return false
	}
	// Controllo obbligatoriet� Settore
	/*if (frmBacheca.txtSettore.value=="")
	{
		alert("Indicar la rama de actividad!")
		imgPunto1.focus()
		return false
	}	*/
	//if (frmBacheca.txtSettoreHid.value=="")
	/*{
		alert("Indicar la rama de actividad!")
		return false
	}*/	
	// Controllo obbligatoriet� Area
	if (frmBacheca.txtArea.value=="")
	{
		alert("Indicar el Grupo ocupacional / Tipolog�a!")
		imgPunto2.focus()
		return false
	}	
	// Controllo obbligatoriet� Profilo
	if (frmBacheca.txtProfilo.value=="")
	{
		alert("Indicar la Denominaci�n del puesto/ocupaci�n / Subtipolog�a")
		imgPunto3.focus()
		return false
	}	
	if (frmBacheca.PersDisc[0].checked == true)
	{
		if (frmBacheca.cmbDisc.value == "")
		{
			alert("Si eligi� la opci�n SI en \n'El puesto solicitado admite postulantes con alg�n tipo de discapacidad' \ndebe elegir un tipo de discapacidad");
			return false;
		}
	}
	if (frmBacheca.Obs_Disc.value != "")
	{
		if (frmBacheca.Obs_Disc.value.length > 400)
		{
			alert("El campo observaciones no debe superar los 400 caracteres");
			return false;
		}
	}
	if (frmBacheca.txtNumPosti.value=="")
	{
		alert("Indicar la Cantidad de trabajadores demandados para el puesto o vacantes disponibles!")
		frmBacheca.txtNumPosti.focus()
		return false
	}
	if (!ValidateInputNumber(frmBacheca.txtNumPosti.value)){
		alert("Valor no num�rico!")
		frmBacheca.txtNumPosti.focus()
	    return false
	   }
	 if (frmBacheca.txtNumPosti.value == 0){
		alert("Indicar un n�mero mayor a cero!")
		frmBacheca.txtNumPosti.focus()
	    return false
	   }	
	//Controllo se la Provincia e il Comune sono digitati
	//Non si puo selezionare lo Stato Estero
	/*if((frmBacheca.txtRegione.value!="")&&
	   (frmBacheca.txtProvincia.value!="")&&
	   (frmBacheca.txtStatoEstero.value!=""))
	 {
		alert("Presionar Anular y seleccionar el Pais Extranjero o la Regi�n y Provincia!")
		return false
	 }*/
	//Controllo se ho digitato la regione devo digitare
	//anche la provincia
	//		if((frmBacheca.txtRegione.value !="")&&
	//		   (frmBacheca.txtProvincia.value ==""))
	//		{
	//			alert("Selezionare la provincia!")
	//			return false 
	//		}	  		
	//Controllo se non ho digitato ne la provincia ne il comune
	//devo digitare la nazione
	 /*if ((frmBacheca.txtRegione.value == "")&&
		(frmBacheca.txtProvincia.value == "")&&
		(frmBacheca.txtStatoEstero .value == ""))
	 {
		alert("Ingresar la Regi�n o la Regi�n/Provincia o el Pa�s Extranjero!")
		imgPunto4.focus()
		return false		
	 }*/
	 //Controllo se ho digitato Regione e Stato Estero
	 /*if ((frmBacheca.txtRegione.value!= "")&&
	 	 (frmBacheca.txtStatoEstero.value!= ""))
	 { 
	 	alert("Ingresar unicamente el pais extranjero o solo el municipio y provincia!") 	
		imgPunto4.focus()
		return false
	 } 	 	*/
	
	
	// control del ingreso de duracion vacia - >se almacena en 0
	if (frmBacheca.txtDuracion.value == "")
	{
		frmBacheca.txtDuracion.value = 0
	}
	if (frmBacheca.cmbTipoDuracion.value == "")
	{
		frmBacheca.cmbTipoDuracion.value = "M"
	}

	
	
	
	//Controllo della validit� della Data di Scadenza
	DataScadenza = frmBacheca.txtValidita.value
	if (DataScadenza=="")
	{
		alert("Ingresar el Plazo de derivaci�n de postulantes!") 	
		frmBacheca.txtValidita.focus() 
		return false
	}
		
	if (!ValidateInputDate(DataScadenza))
	{
		frmBacheca.txtValidita.focus() 
		return false
	}

	DataOdierna=frmBacheca.txtoggi.value
	if (ValidateRangeDateAvisos(DataScadenza,DataOdierna)==true){
		alert("La fecha de derivaci�n debe ser superior o igual a la fecha de emisi�n!")
		frmBacheca.txtValidita.focus()
	    return false
	   }
	if (frmBacheca.txtDtAvv.value != "")
	{
	       if (!ValidateInputDate(frmBacheca.txtDtAvv.value))
 			   {
				frmBacheca.txtDtAvv.focus();
				return false;
			    }
	        /* if(!ValidateRangeDateAvisos(DataScadenza,frmBacheca.txtDtAvv.value))
		     {	
		       alert("La fecha vencimiento no debe ser superior a la fecha de inicio del empleo!")
		       frmBacheca.txtDtAvv.focus();
		       return false;
		     }	*/		
	}
	else
	{
	         alert("Ingresar Fecha de incorporaci�n/Fecha de Inicio")
	         frmBacheca.txtDtAvv.focus();
	         return false;
	}
		
	//Controllo inserimento testo Attivit�
	if (typeof(frmBacheca.ATTIVITA) != 'undefined')
	{
		frmBacheca.ATTIVITA.value=TRIM(frmBacheca.ATTIVITA.value)
		if (frmBacheca.ATTIVITA.value =="")
		{
		   alert("Ingresar la actividad de la empresa!")
		   frmBacheca.ATTIVITA.focus ()	   
		   return false
		 }  
	} 
	 
	//Controllo inserimento Testo Annuncio
	frmBacheca.TESTO.value=TRIM(frmBacheca.TESTO.value)
	if (frmBacheca.TESTO.value  =="")
	{
	   alert("Ingresar el texto del aviso!")
	   frmBacheca.TESTO.focus ()	   
	   return false
	 }  

	//		else
	/*Controllo inserimento Testo Contatti
	frmBacheca.RIFERIMENTI.value=TRIM(frmBacheca.RIFERIMENTI.value)
	if (frmBacheca.RIFERIMENTI.value =="")
	{
	   alert("Inserire le modalit� di contatto!")
	   frmBacheca.RIFERIMENTI.focus ()	   
	   return false
	 }
     */
	return true
}
function ApriFigProf()
	{
	
	document.frmBacheca.CriterioRicerca.value=TRIM(document.frmBacheca.CriterioRicerca.value)
 			   if (TRIM(document.frmBacheca.CriterioRicerca.value)== "")
 				{
 				alert("Ingresar una funci�n o efectuar la b�squeda por rama de actividad.")
 				document.frmBacheca.CriterioRicerca.focus()
 				return;
 				}
				
			   if (!ValidateInputStringWithNumber(document.frmBacheca.CriterioRicerca.value))
				{
				alert("El criterio de b�squeda en el campo Funci�n recubierto es err�neo")
				document.frmBacheca.CriterioRicerca.focus() 
				return;
				}
				
				
	
		CriterioRicerca = frmBacheca.CriterioRicerca.value
		
		URL = "BAC_RicFigProf2.asp?VISMENU=NO" + "&RICERCA="+ CriterioRicerca 
		opt = "address=no,status=no,width=550,height=280,top=150,left=15"
		window.open (URL,"",opt)
	
	}
</script>
<script>
function EnLoad()
{
	document.frmBacheca.cmbDisc.disabled = true;
}
function HabDes(Valor)
{
	if (Valor == "S")
	{
	document.frmBacheca.cmbDisc.disabled = false;
	}
	else
	{
	document.frmBacheca.cmbDisc.value = "";
	document.frmBacheca.cmbDisc.disabled = true;
	}
}
</script>

<script language="javascript" src="Controllibac.js"></script>
<!--#include file = "BAC_InizioAZ.asp"-->
	<%
	nIdSede = Request("idSede")			   				
	if nidsede <> "" then 
		sSQL ="SELECT SEDE_IMPRESA.ID_SEDE,SEDE_IMPRESA.DESCRIZIONE" ' &_ 
		sSQL = sSQL + " FROM SEDE_IMPRESA WHERE SEDE_IMPRESA.ID_SEDE =" & nIdSede & " "
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
		set rstDescr = cc.Execute(sSQL)
		%>
		<!--#include file = "IMP_Label.asp"-->
		<%
		rstDescr.close
	end if 

	%>

<%
TEmp = request("TEmp")
if (not isnull(TEmp)) and (TEmp <> "") then
	TEmp = ucase(TEmp)
end if 
appoIdSede = request("appoIdSede")
'Response.Write "Tipo de empresa " & TEmp
nIdSede = Request("IdSede")
'Response.Write "IdSede " & Request("IdSede")
If nIdSede = "" Then
	nIdSede = Session("Creator")
End If


if TEmp = "E" then 
	sRol = 1
elseif TEmp = "P" then 
	sRol = 2
end if 

						
TPrestacion = request("TipoPrestacion")
'Response.Write "TPrestacion: " & TPrestacion
if TPrestacion <> "" then
	set RsPrestacion = server.CreateObject("adodb.recordset")
	
	if sRol <> "" then 
		consultasPrest = "select avisotipo, avisoformato from impresa_rolesserviciostipos where rol = " & sRol & " and servicio = " & TPrestacion & _
		"Order by descripcion" 
	else
		consultasPrest = "select avisotipo, avisoformato from impresa_rolesserviciostipos where servicio = " & TPrestacion & _
		 "Order by descripcion" 
	end if 
	
'PL-SQL * T-SQL  
CONSULTASPREST = TransformPLSQLToTSQL (CONSULTASPREST) 
		set RsPrestacion = cc.execute(consultasPrest)
		
		if RsPrestacion("avisotipo") = 1 then 
			TAviso = "L"
		else
			TAviso = "C"
		end if 
		
		if cint(RsPrestacion("avisoformato")) = 1 then 
			FormatoEtiquetas="L"
		else
			FormatoEtiquetas="C"
		end if 
		
		'TAviso = ucase(TAviso)
	
	set RsPrestacion = nothing

	if sRol <> "" then 
		SqlRedireccionar = ""
		SqlRedireccionar = SqlRedireccionar & "select esareaprofesional,esareacorso from PLAVORO.impresa_rolesserviciostipos irst "
		SqlRedireccionar = SqlRedireccionar & "inner join PLAVORO.area_grupos ag on irst.areagrupo = ag.areagrupo " 
		SqlRedireccionar = SqlRedireccionar & "inner join PLAVORO.area_origen ao on ao.areaorigen = ag.areaorigen "
		SqlRedireccionar = SqlRedireccionar & "where irst.rol = " & sRol & " and irst.servicio = " & TPrestacion & _
		"Order by irst.descripcion"
	else
		SqlRedireccionar = ""
		SqlRedireccionar = SqlRedireccionar & "select esareaprofesional,esareacorso from plavoro.impresa_rolesserviciostipos irst "
		SqlRedireccionar = SqlRedireccionar & "inner join plavoro.area_grupos ag on irst.areagrupo = ag.areagrupo " 
		SqlRedireccionar = SqlRedireccionar & "inner join plavoro.area_origen ao on ao.areaorigen = ag.areaorigen "
		SqlRedireccionar = SqlRedireccionar & "where irst.servicio = " & TPrestacion & _
		" Order by irst.descripcion"
	end if 
						
	'Response.Write SqlRedireccionar
					
	set RsRedireccionar = server.CreateObject("adodb.recordset")
			
'PL-SQL * T-SQL  
SQLREDIRECCIONAR = TransformPLSQLToTSQL (SQLREDIRECCIONAR) 
	set  RsRedireccionar = cc.execute(SqlRedireccionar)
	
	if not RsRedireccionar.EOF then 
		if cstr(RsRedireccionar("esareaprofesional")) = "1" then
			Seleccion = "GP" ''grupo ocupacional / puesto
		else
			Seleccion = "TS" ''tipologia / subtipologia
		end if 
	end if 
end if 						

if TEmp = "E" then 
	EtiquetaIII = "Rama de Actividad*"
elseif TEmp = "P" then 
	EtiquetaIII = "Tipo de Prestaci�n*"
end if 
			
if EtiquetaIII = "" then 
	if Seleccion = "" or Seleccion = "GP" then 
		EtiquetaIII = "Rama de Actividad*"
	else
		EtiquetaIII = "Tipo de Prestaci�n*"
	end if 
end if 

'if TAviso = "" or TAviso = "L" then 
'	EtiquetaI = "Grupo Ocupacional *"
'	EtiquetaII = "Denominaci�n del puesto/ocupaci�n *"
'else
'	EtiquetaI = "Tipolog�a *"					
'	EtiquetaII = "Subtipolog�a *"
'end if 			





If (((nIdSede = "") or (nIdSede = "0")) and cstr(session("idgruppo")) <> "28") then
	response.redirect "/util/error_login.asp?msg=Funcion no habilitada"
	Response.End 
	%>
	<br>
	<table border="0" cellpadding="6" cellspacing="0" width="500">
		<tr class="tblsfondo">
		    <td class="tbltext" align="center"> <b>--Funci�n no habilitada--</b> </td>
		</tr>	
	</table>
	<%
Else
	Set rsRif = Server.CreateObject("ADODB.Recordset")
	sSQL ="SELECT REF_SEDE,COD_RUO,NUM_TEL,E_MAIL FROM SEDE_IMPRESA WHERE ID_SEDE= " & nIdSede & ""
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	rsRif.Open sSQL,CC
	If not rsRif.EOF Then
		Dim sReferente
		Dim sRuolo
		Dim sTelefono
		Dim sMail
		sReferente=rsRif("REF_SEDE")
		sTelefono=Trim(rsRif("NUM_TEL"))
		sMail=Trim(rsRif("E_MAIL"))
		sRuolo = DecCodVal("RUOLO", "0", "", rsRif("COD_RUO"), 1)
	end if 
	
	if rsRif.EOF or cstr(session("idgruppo")) = "28" then
		%>
		<form name="frmBacheca" method="POST" action="BAC_CnfIscr.asp">
			<table border="0" cellpadding="0" cellspacing="0" width="500">
				<tr height="18">
					<td class="sfondomenu" height="18" width="67%"> <span class="tbltext0"><b>&nbsp;INGRESAR 
						AVISO </b></span></td>		
				 	<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
				 	<td valign="middle" align="right" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1">(*) 
						campo obligatorio</td>
				</tr>
			    <tr width="371" class="SFONDOCOMM">
					<td colspan="3"> Ingresar los datos relativos al aviso y presionar el 
						bot�n <strong>Enviar</strong>. <a href="Javascript:Show_Help('/pgm/help/Bacheca/BAC_InsAnnuncioSede')"><img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a> 
					</td>
				</tr>
				<tr height="2">
					<td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
				</tr>
			</table>
			<br>
			<table border="0" cellpadding="6" cellspacing="0" width="500">
					
					<input type="hidden" name="TipoAnuncio" value="<%=TAviso%>">
					<tr>
						<td width="130" class="tbltext1"> <b>Prestaci�n</b> </td>
						<td class="tbltext1">
						

							<%				
							''''''''VANI 27/09/06
							dim rstipoanun
							dim misql
							dim arrtipoanun
							dim sarrtipoanun
							set rstipoanun = server.CreateObject("adodb.recordset")
							
							if sRol <> "" then 
								misql = " select IRST.servicio, IRST.descripcion from impresa_rolesserviciostipos IRST "
								misql = misql & " inner join impresa_rolesservicios IRS "
								misql = misql & " on IRST.rol = IRS.rol "
								misql = misql & " inner join sede_impresa SI on IRS.id_impresa = SI.id_impresa "
								misql = misql & " where IRST.servicio = IRS.servicio " 
								misql = misql & " and IRS.rol = " & sRol & " and SI.id_sede = " & nIdSede
								misql = misql & " Order by irst.descripcion"
							else
								misql = " select distinct IRST.servicio, IRST.descripcion from impresa_rolesserviciostipos IRST" 
								misql = misql & " Order by irst.descripcion"
							end if 
							
							'Response.Write misql
							
'PL-SQL * T-SQL  
MISQL = TransformPLSQLToTSQL (MISQL) 
							set rstipoanun = CC.execute(misql)


							%>
							<select class="textblack" name="TipoPrestacion" onchange="RefreshPagina()">
								<option value=""></option>
								<%
								 DO WHILE NOT rstipoanun.EOF 
									Response.Write "<option value='" & rstipoanun.Fields(0) & "' "
									if cstr(TPRESTACION) = cstr(rstipoanun.Fields(0)) then
										Response.Write "selected"
									end if 
									Response.Write ">" & rstipoanun.Fields(1) & "</option>"
									rstipoanun.MoveNext 
								LOOP
								%>
							</select>							
						
						
							<%	
							
							
							rstipoanun.Close 
							Set rstipoanun = nothing
							

							%>
							
						</td>
					</tr>
	
<%
'******************************************************************************************'
'BLOQUE QUE OBTIENE EL AMBITO TERRITORIAL (PROVINCIA U OFICINA) DEPENDIENDO DE LA SELECCION
''''VANI''''
%>
					<tr>
						<td width="130" class="tbltext1"> <b>Ambito Territorial</b> </td>
						<td class="tbltext1">
						<%
							CreateComboDizDati "RICHIESTA_SEDE","TIPOAMBITO","CmbAmbito","tbltext1","",Ambito
						%>
						</td>
					</tr>

<%

					DIM RSAMBITO
					DIM SQLAMBITO

					SET RSAMBITO = SERVER.CreateObject("ADODB.RECORDSET")

					'OBTENGO PROVINCIA
					SQLAMBITO = "select cod_timpr, prv, comune, id_cimpiego from sede_impresa si inner join impresa i on i.id_impresa = si.id_impresa where ID_SEDE = " & nIdSede
					

					'Response.Write sqlambito
					
'PL-SQL * T-SQL  
SQLAMBITO = TransformPLSQLToTSQL (SQLAMBITO) 
					SET RSAMBITO = CC.EXECUTE(SQLAMBITO)

					IF NOT RSAMBITO.EOF THEN 
						IF RSAMBITO("cod_timpr") = "03" THEN
							PROVINCIA =RSAMBITO("PRV")
							OFICINA = nIdSede
						ELSE
							OEMPLEO = RSAMBITO("ID_CIMPIEGO")
							SQLAMBITO = "select cod_sede, prv, comune from sede_impresa si inner join impresa i on i.id_impresa = si.id_impresa where ID_SEDE = " & OEMPLEO
							
'PL-SQL * T-SQL  
SQLAMBITO = TransformPLSQLToTSQL (SQLAMBITO) 
							SET RSAMBITO = CC.EXECUTE(SQLAMBITO)
							PROVINCIA =RSAMBITO("PRV")
							OFICINA = OEMPLEO
						END IF 
					END IF

					'Response.Write PROVINCIA & "<BR>"
					'Response.Write OFICINA


					SET RSAMBITO = NOTHING

%>
				<input type="hidden" name="Provincia" value="<%=PROVINCIA%>">
				<input type="hidden" name="Oficina" value="<%=OFICINA%>">
<%					
'******************************************************************************************'

%>	
			<%
			if Seleccion = "" or Seleccion = "GP" then 
				EtiquetaI = "Grupo Ocupacional:"
				EtiquetaII = "Denominaci�n del puesto/ocupaci�n:"
				'EtiquetaIII = "Rama de Actividad:"

			else
				EtiquetaI = "Tipolog�a:"					
				EtiquetaII = "Subtipolog�a:"
				'EtiquetaIII = "Tipo de Prestaci�n:"

			end if 
			
			if FormatoEtiquetas = "L" then
				EtCant = "Trabajadores demandados para el puesto:"
				EtFecha = "Fecha de Incorporaci�n al puesto de trabajo:"	
				TexAnuncio = "Texto del Aviso"
				duracion = "Duraci�n del trabajo"
			else
				EtCant = "Participantes/Vacantes disponibles:"
				EtFecha = "Fecha de Inicio:"
				TexAnuncio= "Dise�o del Curso/Actividad"			
				duracion = "Duraci�n del Curso/Actividad"
			end if 
			%>
			

			</table>
			<input type="hidden" name="TEmp" value="<%=TEmp%>">

			<%if Seleccion = "" or Seleccion = "GP" then%>
				<br>
				<table border="0" cellpadding="6" cellspacing="0" width="500">
					<tr>
						<td>
							<div align="center">
							<table cellpadding=2 cellspacing=0 width=90% border=1 bordercolor="#006699"><tr><td colspan="2">		
								<tr bordercolor="white">
									<td colspan="3" align="justify" class="textblack">
									<small>Ingresando la <b>Funci�n Requerida</b> o parte de ella se podr�n obtener autom�ticamente
									<!--Tambi�n puede ingresar el <b>C�digo de Ocupaci�n</b> para efectuar la b&uacute;squeda, o bien utilizar simultaneamente ambos criterios de b&uacute;squeda.		-->
									  el grupo ocupacional y el puesto correspondiente presionando el bot�n de <b>B&uacute;squeda</b>.
									</small>
									</td>
								</tr>	
								<tr bordercolor="white">
									<td class="tbltext1"> <b>Funci�n Requerida</b></td>
									<td>
										<input style="TEXT-TRANSFORM: uppercase; width:270px;" type="text" class="textblacka" size="40" maxlength="50" name="CriterioRicerca">
									</td>
									<td align="left" rowspan="2">
									<a href="javascript:ApriFigProf()">
										<img src="<%=Session("Progetto")%>/images/leggi.gif" border="0">
									</a>		
								</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			<% end if %>
		<br>
		<table border="0" cellpadding="6" cellspacing="0" width="500">
			<!--GRUPOS OCUPACIONLES/TIPOLOGIAS Y PUESTOS/SUBTIPOLOGIAS-->		
			<tr height="17">
				<td width="130" class="tbltext1">
					<b><%=EtiquetaI%></b>
				</td>
				<td class="tbltext1">
					<textarea rows="4" class="textblacka" readonly type="text" name="txtArea" style="width=270px"></textarea>
					<input type="hidden" name="txtAreaHid" value>
				</td>
				<td align="left" width="40" class="tbltext1"><b>
					<% if Seleccion = "" or Seleccion = "GP" then %>
						<a href="Javascript:SelSpecifico('AREE_PROFESSIONALI','ID_AREAPROF','txtArea','<%=sRol%>','<%=TPrestacion%>')" ID="imgPunto2" name="imgPunto2"><img border="0" src="<%=Session("Progetto")%>/images/bullet1.gif"></a>
					<% elseif Seleccion = "TS" then%>
						<a href="Javascript:SelSpecifico('AREA_CORSO','ID_AREA','txtArea','<%=sRol%>','<%=TPrestacion%>')" ID="imgPunto2" name="imgPunto2"><img border="0" src="<%=Session("Progetto")%>/images/bullet1.gif"></a>		
					<% end if %>
				</td>
			</tr>
				
			<tr height="17">
				<td width="130" class="tbltext1"> <b><B><%=EtiquetaII%></b> </td>
				<td class="tbltext1">
					<textarea rows="4" class="textblacka" readonly type="text" name="txtProfilo" style="width=270px"></textarea>
					<input type="hidden" name="txtProfiloHid" value>
				</td>
				<td align="left" width="40" class="tbltext1"><b>
					<% if Seleccion = "" or Seleccion = "GP" then %>
						<a href="Javascript:SelSpecifico('FIGUREPROFESSIONALI','ID_FIGPROF','txtProfilo','<%=sRol%>','<%=TPrestacion%>')" ID="imgPunto3" name="imgPunto3"><img border="0" src="<%=Session("Progetto")%>/images/bullet1.gif"></a>
					<% elseif Seleccion = "TS" then%>
						<a href="Javascript:SelSpecifico('AREA_CORSO_SUB','ID_SUBAREA','txtProfilo','<%=sRol%>','<%=TPrestacion%>')" ID="imgPunto3" name="imgPunto3"><img border="0" src="<%=Session("Progetto")%>/images/bullet1.gif"></a>
					<% end if %>
				</td>
			</tr>
		</table>
		<!--FIN GRUPOS OCUPACIONLES/TIPOLOGIAS Y PUESTOS/SUBTIPOLOGIAS-->
			
		<!--INICIO ADAPTACION AL FORMULARIO DE INTERMEDIACION LABORAL-->			
		<table border="0" cellpadding="6" cellspacing="0" width="500">

			<%'''vani 15-06-2006 
				if FormatoEtiquetas="L" then
				'if Seleccion = "" or Seleccion = "GP" then 
			%>
						<tr height="17">
							<td width="130" class="tbltext1"> 
								<b>Personal a Cargo*</b> 
							</td>
							<td class="tbltext1">
								<input type="radio" name="PersCargo" value="S">SI
								<input type="radio" name="PersCargo" value="N" checked>NO
							</td>
						</tr>
			<%'''vani 15-06-2006 
				else
			%>
						<input type="hidden" name="PersCargo" value="">
			<%	
				end if 
			%>
		</table>
		<!--FIN ADAPTACION AL FORMULARIO DE INTERMEDIACION LABORAL-->			
		

		<!--INICIO ADAPTACION AL FORMULARIO DE INTERMEDIACION LABORAL-->	
		<%if FormatoEtiquetas="L" then 'if Seleccion = "" or Seleccion = "GP" then %>	
			<table border="1" cellpadding="6" cellspacing="0" width="500" bordercolor="MidNightBlue">
				<tr height="17"  bordercolor="white">
					<td class="tbltext1" width="130"> 
						<b>El puesto solicitado admite postulantes con alg�n tipo de discapacidad?*</b> 
					</td>
					<td colspan="2" class="tbltext1">
						<input type="radio" name="PersDisc" value="S" onclick="HabDes('S')">SI
						<input type="radio" name="PersDisc" value="N" onclick="HabDes('N')" checked>NO
						<br>
						<b>Tipo de Discapacidad?</b>&nbsp;
						<%				
						ComboDisc = "TCAPI|0|" & date & "||cmbDisc|ORDER BY DESCRIZIONE"			
						CreateCombo(ComboDisc)
						%>
					</td>
				</tr>
				<tr height="17"  bordercolor="white">
					<td class="tbltext1" width="130"> 
						<b>Observaciones</b> 
					</td>
					<td colspan="2" class="tbltext1">
						<textarea name="Obs_Disc" cols=37 rows=7 WRAP="ON"></textarea>
					</td>
				</tr>
			</table>		
		<%else%>
			<input type="hidden" name="PersDisc" value="N">
			<input type="hidden" name="PersDisc" value="N">
			<input type="hidden" name="cmbDisc" value="">
			<input type="hidden" name="Obs_Disc" value="">
		<%end if%>
		<!--<tr height="17">
			<td width="200" class="tbltext1"> 
				<b>Observaciones</b> 
			</td>
			<td colspan="2" class="tbltext1">
				<textarea name="Obs_Disc" cols=45 rows=7 WRAP="ON"></textarea>
			</td>
		</tr>-->
		<!--FIN ADAPTACION AL FORMULARIO DE INTERMEDIACION LABORAL-->	


		<table border="0" cellpadding="6" cellspacing="0" width="500">
			<tr>		
				<td width="130" class="tbltext1"> <b><B><%=EtCant%></b> </td>
				<td  class="tbltext1" colspan="2">
					<input class="textblacka" type="text" name="txtNumPosti" style="width=40px" maxlength="4">
				</td>
			</tr>
		
			<tr>		
				<td width="130" class="tbltext1"> <B>Presentarse en: </B> </td>
				<td  class="tbltext1" colspan="2">
					<input class="textblacka" type="text" name="txtPresentarseEn" style="width=320px" maxlength="250">
				</td>
			</tr>
				
			
			<!--
			<tr height="17">
				<td colspan="3">&nbsp;</td>
			</tr>
			<tr>
				<td height="2" align="left" colspan="4" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
			</tr>
			<tr>
		      <td height="20" align="left" colspan="4" class="textblack"> Indicar la provincia</td>
			</tr>
			<tr>
				<td height="2" align="left" colspan="4" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
			</tr>
			<tr height="2">
				<td width="100%" colspan="3" background></td>
			</tr>
			<tr height="17">
				<td colspan="3">&nbsp;</td>
			</tr>-->
			
			<!--<tr height="17">
				
			<td width="200" class="tbltext1"> <b>Regi&oacute;n</b></td>
				<td width="270" class=140"tbltext1">
					<b><input class="textblacka" readonly type="text" name="txtRegione" style="width=270px">
				</td>
				<td align="left" width="60" class="tbltext1"><b>
					<input type="hidden" name="txtRegioneHid" value>
					<a href="Javascript:SelArea('REGIO','CODICE','txtRegione')" ID="imgPunto4" name="imgPunto4"><img border="0" src="<%=Session("Progetto")%>/images/bullet1.gif"></a>				
				</td>
			</tr>-->
			<!--
			<tr height="17">
				<td width="200" class="tbltext1">
					<b>Provincia</b>
				</td>
				<td width="270" class="tbltext1">
					<b><input class="textblacka" readonly type="text" name="txtProvincia" style="width=270px">
				</td>
				<td align="left" width="60" class="tbltext1">
					<input type="hidden" name="txtProvinciaHid" value>
					<a href="Javascript:SelSpecifico('PROV','CODICE','txtProvincia')" ID="imgPunto5" name="imgPunto5"><img border="0" src="<%=Session("Progetto")%>/images/bullet1.gif"></a>
				</td>
			</tr>-->
			<!--<tr height="17">
	        <td width="200" class="tbltext1"> <b>Pa&iacute;s Extranjero</b> </td>
				<td width="270" class="tbltext1">
					<b><input class="textblacka" readonly type="text" name="txtStatoEstero" style="width=270px"></b>
					</td>
			    <td align="left" width="60" class="tbltext1"><b>
				<input type="hidden" name="txtStatoHid" value>
					<a href="Javascript:SelArea('STATO','CODICE','txtStatoEstero')" ID="imgPunto6" name="imgPunto6"><img border="0" src="<%=Session("Progetto")%>/images/bullet1.gif"></a>
				</td>
		    </tr>
					
			<tr height="17">
				<td colspan="3">&nbsp;</td>
			</tr>
			<tr>
				<td height="2" align="left" colspan="4" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
			</tr>-->
			<!--			<tr>
				
			<td height="20" align="left" colspan="4" class="textblack"> Indicar el plazo de derivaci�n de postulantes</td>
			</tr>
			<tr>
				<td height="2" align="left" colspan="4" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
			</tr>
			
			<tr height="2">
				<td width="100%" colspan="3" background></td>
			</tr>
			<tr height="17">
				<td colspan="3">&nbsp;</td>
			</tr>-->	
			<%
			if TPrestacion <> "1" then 
				%>
				<tr>
					<td class="tbltext1" width="130"><b><B><%=Duracion%>:</B></b></td>
					<td><input type="text" name="txtDuracion" value class="textblack" maxlength="3" size="3">
						&nbsp;&nbsp;&nbsp;<b class="tbltext1">&nbsp;Medida en:</b>&nbsp;&nbsp;&nbsp; <%CreateComboDizDati "FORMAZIONE", "COD_TIPO_DURACION", "cmbTipoDuracion", "textblack", "", ""%>
					</td>
				</tr>
				<%
			else
				%>
				<input type="hidden" name="txtDuracion" value="0">
				<input type="hidden" name="cmbTipoDuracion" value="M">
				<%
			end if
			%>
			<tr height="17">
				<td width="130" class="tbltext1"> <b>Plazo de derivaci�n de postulantes:*</b></td>
				<td  class="tbltext1" colspan="2">
					<input class="textblacka" type="text" name="txtValidita" style="width=72px" maxlength="10">
					<input type="hidden" name="txtoggi" value="<%=ConvDateToString(Date())%>">
					<b class="tbltext1">&nbsp;&nbsp;(dd/mm/aaaa)</b>
				</td>
			</tr>
			<tr>
		        <td class="tbltext1" width="130"> <b><B><%=EtFecha%></B></b></td>
		        <td class="textblack" colspan="2"> 
					<input class="textblacka" type="text" name="txtDtAvv" style="width=72px" maxlength="10">
					<b class="tbltext1">&nbsp;&nbsp;(dd/mm/aaaa)</b> </td>
			</tr>
			
			
<% if FormatoEtiquetas="L" then %>			
			<tr>
				<td width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
			</tr>
			<tr height="2">
				<td width="100%" colspan="3" background></td>
			</tr>

			<tr height="17">
		        <td class="tbltext1" width="130"> <b><B>Actividades principales*</b> </td>
				<td class="tbltext1">
						<textarea NAME="ATTIVITA" ROWS="7" COLS="37" WRAP="ON"></textarea>
				</td>
			</tr>
<%end if%>
		
			<tr height="2">
				<td width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
			</tr>
			<tr height="17">
				<td class="tbltext1" width="130"> <b><%=TexAnuncio%>*</b> </td>
				<td class="tbltext1">
						<textarea NAME="TESTO" ROWS="7" COLS="37" WRAP="ON"></textarea>
				</td>
			</tr>
			<!--tr height="17">			<td width="200" class="tbltext1">				<b>Contattare*</b>			</td>			<td width="270" colspan="2" class="tbltext1">				<textarea NAME="RIFERIMENTI" ROWS="4" COLS="45" WRAP="OFF"><%=sReferente& chr(10)%>				<%=sRuolo & chr(10)%>				<%=sTelefono & chr(10)%>				<%=sMail & chr(10)%></textarea>			</td>			<td align="center" width="30" height="30" class="tbltext1"><b>											</td>		<tr height="2">			<td width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif"></td>		</tr-->
			</tr>
		</table>
		<br>
		<br>
		<table border="0" cellpadding="0" cellspacing="0" width="500" align="center">
			<tr>
				<td align="middle" colspan="2">
					<img onload="EnLoad()" src="<%=session("Progetto")%>/images/indietro.gif" border="0" onclick="return chiudi()">
					<input type="image" name="Invia" src="<%=Session("Progetto")%>/images/conferma.gif" border="0" value="Conferma" onclick="return ControllaDati(this)">
				</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
		   </tr>
		</table>	
		<input type="hidden" name="IdSede" value="<%=nIdSede%>">
		<input type="hidden" name="appoIdSede" value="<%=appoIdSede%>">
		</form>

	<%
	'**********************************************************************************************************************************************************************************	
	   'response.Write valore , tipo
	  ' response.end
	    
		
		Function trasforma(valore,tipo)
			select case tipo
				case "D"
					trasforma = ConvDateToDb(valore)
				case "A"
					trasforma = "'" & Valore & "'"
				case else
					trasforma = valore
			end select
		
		End Function 

	Else
%>
		<br>
		<table>
			<tr class="tblsfondo">
				
    <td colspan="3" width="500" class="tbltext" align="center"> <b>--No existe 
      el aviso--</b> </td>
			</tr>	
		</table>	
	<% End if %>
<%end if%>
<!--#include Virtual ="/include/CloseConn.asp"-->
<!--#include Virtual="/strutt_coda2.asp"-->		
