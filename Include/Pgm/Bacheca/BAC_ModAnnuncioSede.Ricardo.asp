<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include Virtual = "/strutt_testa2.asp"-->
<!--#include Virtual = "/include/SysFunction.asp"-->
<!--#include Virtual = "/include/openconn.asp"-->
<!--#include virtual = "/include/DecCod.asp"-->
<!--#include virtual = "/util/portallib.asp"-->
<!--#include virtual = "/include/ControlDateVB.asp"-->
<!--#include virtual = "/include/ImpostaProgetto.asp"-->


<a name="primo"></a>
<script language="javascript">

//include del file per fare i controlli sulla validit� delle date
<!--#include virtual = "/include/ControlDate.inc"-->

//include del file per fare i controlli sulla numericit� dei campi
<!--#include virtual = "/include/ControlNum.inc"-->
<!--#include virtual = "/include/ControlString.inc"-->
<!--#include virtual = "/include/help.inc"-->

function VisCandidati()
{
	document.frmVisCand.submit(); 
}	

//Funzione per i controlli dei campi da inserire 
function Cancella()
{
	document.frmBacheca.reset()
	return false
}
	

function ControllaDati()
{
	if (frmBacheca.txtAreaHid.value == "")
	{
		alert("Debe seleccionar un Grupo Ocupacional / Tipolog�a");
		return false;
	}
	if (frmBacheca.txtProfiloHid.value == "")
	{
		alert("Debe seleccionar un Puesto / Subtipolog�a");
		return false;
	}
	if (frmBacheca.PersDisc[0].checked == true)
	{
		if (frmBacheca.cmbDisc.value == "")
		{
			alert("Si eligi� la opci�n SI en \n'El puesto solicitado admite postulantes con alg�n tipo de discapacidad' \ndebe elegir un tipo de discapacidad");
			return false;
		}
	}
	if (frmBacheca.Obs_Disc.value != "")
	{
		if (frmBacheca.Obs_Disc.value.length > 400)
		{
			alert("El campo observaciones no debe superar los 400 caracteres");
			return false;
		}
	}
		
	if (frmBacheca.txtNumPosti.value=="")
	{
		alert("Indicar la Cantidad de trabajadores demandados para el puesto!")
		frmBacheca.txtNumPosti.focus()
		return false
	}
		
	if (!ValidateInputNumber(frmBacheca.txtNumPosti.value))
	{
		alert("Valor no num�rico!")
		frmBacheca.txtNumPosti.focus()
		return false
	}	
	
	if (frmBacheca.txtNumPosti.value == 0)
	{
		alert("Indicar un n�mero mayor a cero!")
		frmBacheca.txtNumPosti.focus()
		return false
	}	
	//alert(frmBacheca.txtValidita.value)
	
	DataScadenza = TRIM(frmBacheca.txtValidita.value)

	if (DataScadenza =="")
	{
	   alert("Indicar la validez!")
	   frmBacheca.txtValidita.focus ()	   
	   return false
	}  
	
	if (!ValidateInputDate(DataScadenza))
	{
		frmBacheca.txtValidita.focus() 
		return false
	}
		
	if (ValidateRangeDateAvisos(DataScadenza,frmBacheca.txtoggi.value)==true)
	{
		alert("La fecha de derivacion debe superior o igual a la fecha de emisi�n!")
		frmBacheca.txtValidita.focus()
		return false
	}
		   
	if (frmBacheca.txtDtAvv.value != "")
	{
		if (!ValidateInputDate(frmBacheca.txtDtAvv.value))
 		{
			frmBacheca.txtDtAvv.focus();
			return false;
		}
		/*if(!ValidateRangeDateAvisos(DataScadenza,frmBacheca.txtDtAvv.value))
		  {	
		  	  alert("La fecha vencimiento no debe ser superior a la fecha de inicio del empleo!")
			  frmBacheca.txtDtAvv.focus();
			  return false;
		  }	  */		
	}
	else
	{
		alert("Ingresar Fecha de incorporaci�n/Fecha de Inicio")
		frmBacheca.txtDtAvv.focus();
		return false;
	}
			
	//Controllo inserimento testo Attivit�
	frmBacheca.ATTIVITA.value=TRIM(frmBacheca.ATTIVITA.value)
	if (frmBacheca.ATTIVITA.value =="")
	{
	   alert("Ingresar la actividad de la empresa")
	   frmBacheca.ATTIVITA.focus ()	   
	   return false
	}  
	
	//Controllo inserimento Testo Annuncio
	frmBacheca.TESTO.value=TRIM(frmBacheca.TESTO.value)
	if (frmBacheca.TESTO.value  =="")
	{
	   alert("Ingresar el texto del aviso!")
	   frmBacheca.TESTO.focus ()	   
	   return false
	 }  
	 
	 return true
}		
		
</script>
<script language="javascript" src="Controllibac.js">
</script>

<%

dim sIdAnnuncio
dim sSettoreDesc
dim sAreaProfDesc
dim sFigProfDesc
dim sRegioneDesc
dim sProvDesc
dim sStatoDesc
dim sValidita
dim sDtTmst
dim sPubblica
dim nNumPosti
'INIZIO 5/12/2003
dim appoIdSede
appoIdSede = request("appoIdSede")
'Response.Write "appoIdSede " & appoIdSede
'FINE 5/12/2003 
sIdAnnuncio = Request("Codice")

' datos del proyecto
'-------------------------------------------------
importacionNro = Request.QueryString ("importacionNro")
item		 = Request.QueryString ("Item")
Proyecto	 = Request.QueryString ("Proyecto")

if Proyecto = "" then 
	Proyecto = request("Proyecto")
end if 

gecal        = Request.QueryString ("Gecal")
Programa     = Request.QueryString ("Programa")
tituloBoton  = Request.QueryString ("tituloBoton")
opcion		 = Request.QueryString ("Opcion")
funcion		 = Request.QueryString ("funcion")
numero       = Request.QueryString ("numero")
ID_Busqueda0 = Request.QueryString ("ID_Busqueda0")
ID_Busqueda1 = Request.QueryString ("ID_Busqueda1")
IdSede       = Request.QueryString ("IdSede")


'Response.Write Proyecto

' son las variables para poder volver a Proyectos
'-------------------------------------------------
'if IdSede > 0 then 'si la sede esta entonces seteo las variables de SESSION
if importacionNro <> "" then
	Session("importacionNro") = importacionNro
end if
if item <> "" then 
	Session("item") = item
end if 
if Proyecto <> "" then 
	Session("Proyecto") = Proyecto
end if 
if gecal <> "" then 
	Session("gecal") = gecal
end if 
if Programa <> "" then 
	Session("Programa") = Programa
end if 
if tituloBoton <> "" then 
	Session("tituloBoton") = tituloBoton
end if 
if opcion <> "" then 
	Session("opcion") = opcion
end if 
if funcion <> "" then 
	Session("funcion") = funcion
end if 
if Numero <> "" then 
	Session("Numero") = Numero 
end if 
if ID_Busqueda0 <> "" then 
	Session("ID_Busqueda0") = ID_Busqueda0
end if 
if ID_Busqueda1 <> "" then 
	Session("ID_Busqueda1") = ID_Busqueda1
end if
if IdSede <> "" then 
	Session("IdSede") = IdSede
end if 
'End if



'Response.Write "codice" & sIdAnnuncio

'Response.Write "idsede" & Request.Form("IdSede")
'Response.Write "Pippo = " & sIdAnnuncio


	set rsRif = Server.CreateObject("ADODB.Recordset")
	if sIdAnnuncio = "" or isnull(sIdAnnuncio) then 
		sIdAnnuncio = request("IDRic")
	end if
	
''''VANI 30-05-06''''		
	ssql = ssql & "SELECT DISTINCT RICHIESTA_SEDE.NRO_VACANTES, RICHIESTA_SEDE.ID_RICHIESTA, RICHIESTA_SEDE.ID_SETTORE, RICHIESTA_SEDE.ID_AREAPROF,RICHIESTA_SEDE.FL_PUBBLICATO, " 
	ssql = ssql & "RICHIESTA_SEDE.ID_FIGPROF, " 
	ssql = ssql & "RICHIESTA_SEDE.STAT_LAV, RICHIESTA_SEDE.DT_FINPUBLICACION, " 
	ssql = ssql & "RICHIESTA_SEDE.DT_TMST, RICHIESTA_SEDE.TIPOANUNCIO, RICHIESTA_SEDE.DT_INCORPORACION, RICHIESTA_SEDE.ID_SEDE, RICHIESTA_SEDE.STAT_LAV, SETTORI.DENOMINAZIONE AS SETTORE_DESC, RICHIESTA_SEDE.GECAL, RICHIESTA_SEDE.PROGRAMA, RICHIESTA_SEDE.PROYECTO, RICHIESTA_SEDE.DIRECCION, RICHIESTA_SEDE.DURACION, RICHIESTA_SEDE.DURACIONUMEDIDA, " 
	ssql = ssql & "PERS_CARGO, PERS_DISC, PERS_TIPO_DISC, PERS_DISC_OBS " 
	ssql = ssql & "FROM TADES, RICHIESTA_SEDE, SETTORI WHERE " 
	ssql = ssql & "RICHIESTA_SEDE.ID_SETTORE = SETTORI.ID_SETTORE " 
	ssql = ssql & "AND ID_RICHIESTA =" & sIdAnnuncio
	
	'Response.Write ssql

'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	rsRif.Open sSQL,CC

	dim rsempr 
	set rsempr = server.CreateObject("ADODB.recordset")	
	IF UCASE(rsRif("TIPOANUNCIO")) = "L" THEN 
			SQLEMPR = ""
			SQLEMPR = SQLEMPR & "SELECT ACSE.DENOMINAZIONE AREE_PROF_DESC, AC.DENOMINAZIONE FIG_PROF_DESC "
			SQLEMPR = SQLEMPR & "FROM RICHIESTA_SEDE ASE, "
			SQLEMPR = SQLEMPR & "AREE_PROFESSIONALI ACSE, "
			SQLEMPR = SQLEMPR & "FIGUREPROFESSIONALI AC "
			SQLEMPR = SQLEMPR & "where ACSE.id_areaprof = ASE.ID_AREAPROF "
			SQLEMPR = SQLEMPR & "and AC.id_figprof = ASE.ID_FIGPROF "
			SQLEMPR = SQLEMPR & "and ACSE.ID_AREAPROF = AC.ID_AREAPROF "
			SQLEMPR = SQLEMPR & "and ASE.id_RICHIESTA =" & sIdAnnuncio
	ELSEIF UCASE(rsRif("TIPOANUNCIO")) = "C" THEN 
			SQLEMPR = ""
			SQLEMPR = SQLEMPR & "SELECT DENOMINAZIONE AREE_PROF_DESC, DESCRIPCION FIG_PROF_DESC "
			SQLEMPR = SQLEMPR & "FROM RICHIESTA_SEDE ASE,      "
			SQLEMPR = SQLEMPR & "AREA_CORSO_SUB ACSE,         "
			SQLEMPR = SQLEMPR & "AREA_CORSO AC                "
			SQLEMPR = SQLEMPR & "where ASE.id_areaprof = AC.id_area   "
			SQLEMPR = SQLEMPR & "and ASE.id_figprof = ACSE.id_subarea "
			SQLEMPR = SQLEMPR & "and ACSE.id_area = AC.id_area        "
			SQLEMPR = SQLEMPR & "and ASE.id_RICHIESTA =" & sIdAnnuncio
	END IF 

	'Response.Write sqlempr

'PL-SQL * T-SQL  
SQLEMPR = TransformPLSQLToTSQL (SQLEMPR) 
	set rsempr = cc.execute(sqlempr)
					
	if not rsempr.EOF then 
		sAreaID = rsRif("ID_AREAPROF")
		sFiguraID = rsRif("ID_FIGPROF")
		sAreaProfDesc = rsempr("AREE_PROF_DESC")
		sFigProfDesc = rsempr("FIG_PROF_DESC")
	end if
	
	''''VANI 30-05-06''''
			
	'Response.Write rsRif("COD_REGIONE")

	sStatoDesc = DecCodVal("STATO", "0", "", rsRif("STAT_LAV"), 1)
	sStatoDesc = UCase(sStatoDesc)
	sIdAnnuncio = rsRif("ID_RICHIESTA")
	nIdAreaProf	= rsRif("ID_AREAPROF")
	nIdFigProf	= rsRif("ID_FIGPROF")
	nNumPosti	= rsRif("NRO_VACANTES")
	sSettoreDesc = rsRif("SETTORE_DESC")
	sSettoreDesc = UCase(sSettoreDesc)
	'sAreaProfDesc = rsRif("AREE_PROF_DESC")
	sAreaProfDesc = UCase(sAreaProfDesc)
	'sFigProfDesc = rsRif("FIG_PROF_DESC")
	sFigProfDesc = UCase(sFigProfDesc)
	sPubblica = rsRif("FL_PUBBLICATO")
	sPubblica = UCase(sPubblica)
	sValidita = ConvDateToString(rsRif("DT_FINPUBLICACION"))
	sDtAvv = ConvDateToString(rsRif("DT_INCORPORACION"))
	''''VANI
	sSede = rsRif("ID_SEDE")
	sNomProj = rsRif("STAT_LAV")
	''''VANI
	sDtTmst = rsRif("DT_TMST")

	if sDtTmst = "" then
		sDtTmst = request("sDtTmst")
	end if 


	'_____________________________________

	sPersCargo = rsRif("Pers_Cargo")
	sPersDisc = rsRif("Pers_Disc")
	sPersTipoDisc = rsRif("Pers_Tipo_Disc")
	sDiscObs = rsRif("Pers_Disc_Obs")
	'_____________________________________

%>


	<!--#include file = "BAC_inizioAZ.asp"-->
	<br>	
	<table cellpadding="0" cellspacing="0" width="500" border="0">
		<tr height="18">
			
      <td class="sfondomenu" height="18" width="67%"> <span class="tbltext0"><b>&nbsp;MODIFICAR 
        AVISO </b></span> </td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
			
      <td valign="middle" align="right" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1">(*) 
        campo obligatorio</td>
		</tr>
		<tr width="371" class="SFONDOCOMMAZ">
			
      <td colspan="3"> Para modificar o ingresar detalles del aviso, ingresar en cada uno de los detalles
        y presionar el bot�n <strong>Enviar</strong> para guardar los mismos.
        <br>
        Para mostrar los eventuales postulantes relativos a &eacute;sta ofrecimiento 
        presionar el bot�n <strong>Visualizar Postulantes</strong><a href="Javascript:Show_Help('/pgm/help/Bacheca/BAC_ModAnnuncioSede')"><img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a> 
        y para visualizar los detalles ingresados a este ofrecimiento,presionar<strong> Detalle del Ofrecimiento.</strong>
      </td>
		</tr>
		<tr height="2">
			<td colspan="3" class="SFONDOCOMMAZ" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>
	<br>
	<!--#include file="IncludeMenuCriterios.asp"-->
	<form name="frmBacheca" method="POST" action="BAC_CnfAnnuncioSede.asp" onSubmit="return ControllaDati(this)">
	<br>
	<table border="0" cellpadding="6" cellspacing="0" width="500">
		<tr>
			<td width="130" class="tbltext1"> <b>Tipo de Aviso:</b> </td>
			<td class="textblack">
				<b>
				<%
					dim rstipoanun
					dim misql
					dim arrtipoanun
					dim sarrtipoanun

					set rstipoanun = server.CreateObject("adodb.recordset")
					misql = "Select ID_CAMPO_DESC from DIZ_DATI where ID_TAB = 'RICHIESTA_SEDE' AND ID_CAMPO = 'TIPOANUNCIO'"
'PL-SQL * T-SQL  
MISQL = TransformPLSQLToTSQL (MISQL) 
					set rstipoanun = CC.execute(misql)
					arrtipoanun = rstipoanun("ID_CAMPO_DESC")
					sarrtipoanun = split(arrtipoanun,"|")
					rstipoanun.Close 
					Set rstipoanun = nothing
					for i = lbound(sarrtipoanun) to ubound(sarrtipoanun)
						if sarrtipoanun(i) = rsRif("TIPOANUNCIO") then
							valor = ucase(sarrtipoanun(i + 1))
							Response.Write valor
							response.write "<input type='hidden' value='" & cstr(valor) & "' name='txttipoanuncio'>"
							exit for
						end if 
					next
				%>
				</b>
			</td>
		</tr>
		<tr>
			<td width="130" class="tbltext1">
				<b>Nro de Referencia:</b>
			</td>
			<td class="textblack">
				<b><%=sIdAnnuncio%></b>
				<!--<input type="hidden" name="txtgrupoocup" value="<%=sAreaProfDesc%>">-->
			</td>
		</tr>
		<!--#include virtual="/include/VerificaProyectos.asp"-->	
	</table>		
	<table border="0" cellpadding="6" cellspacing="0" width="500">
		<tr>
			<td width="100%" colspan="8" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
		</tr>					
	</table>
	<br>
	<table border="0" cellpadding="6" cellspacing="0" width="500">
	<!--	<tr>		<td width=130> &nbsp;</td>	</tr>					<tr>		<td width="100%" colspan=2 background="<%=Session("Progetto")%>/images/separazione.gif"></td>	</tr>	<tr>		<td width=130> &nbsp;</td>	</tr>				-->	
	<%
		set rsbuscosede = server.CreateObject("adodb.recordset")	
		sqlbuscosede = "select * from sede_impresa si, richiesta_sede rs, impresa i where i.id_impresa = si.id_impresa and rs.id_sede = si.id_sede and rs.id_richiesta = " & sidannuncio
'PL-SQL * T-SQL  
SQLBUSCOSEDE = TransformPLSQLToTSQL (SQLBUSCOSEDE) 
		set rsbuscosede = cc.execute(sqlbuscosede)
		if not rsbuscosede.eof then 
			sForma= DecCodVal("FGIUR", "0", "", rsbuscosede("COD_FORMA"), 1)
			%>
				<tr>
				  <td class="tbltext1" width="130"><b>Datos del Solicitante:</b></td>
				  <td></td>
				  <td></td>
				</tr>
				<tr>
				  <td class="tbltext1" width="130"></td>
				  	<td class="tbltext1"><b>Sede:</b></td>
				  <td class="textblack"><b><%=Ucase(rsbuscosede("descrizione"))%></b></td>
				</tr>
				<tr>
					<td class="tbltext1" width="130"></td>
				  	<td class="tbltext1"><b>Empresa:</b></td>
				    <td class="textblack"><b><%=trim(rsbuscosede("RAG_SOC") & " (" & sForma & ")")%></b></td>
				</tr>	
				<tr>
					<td class="tbltext1" width="130"></td>
				  	<td class="tbltext1"><b>Domicilio:</b></td>
				    <td class="textblack"><b><%=Ucase(rsbuscosede("INDIRIZZO"))%></b></td>
				</tr>
				<tr>
					<td class="tbltext1" width="130"></td>
				  	<td class="tbltext1"><b></b></td>
					<%
						Set rstComune = Server.CreateObject("ADODB.Recordset")
						SQLcomune = " SELECT DESCOM FROM COMUNE WHERE CODCOM = '" & rsbuscosede("COMUNE") & "'"
'PL-SQL * T-SQL  
SQLCOMUNE = TransformPLSQLToTSQL (SQLCOMUNE) 
						rstComune.open SQLcomune, CC, 1, 3
						if not isnull(rstComune("DESCOM")) then
							DescripcionComune = ucase(rstComune("DESCOM"))
						else
							DescripcionComune = ""
						end if
						set rstComune = nothing
					%>
					
					<td class="textblack"><b>(<%=trim(rsbuscosede("CAP") & ") " & DescripcionComune)%> </b></td>
				</tr>
			<!--	<tr>		<td class="tbltext1"><b>Provincia:</b></TD>		<td class="tbltext1"> &nbsp;</TD>		<td class="textblack">( <%=Ucase(sProvDesc)%> )</td>	</tr>-->
			<%
		end if
		set rsbuscosede = nothing
	%>
	</table>
	<br>

<!--DATOS CONTRAPARTE Y PROYECTO-->

	<%
	if not isnull(rsRif("PROYECTO")) then
		set rsbuscocontraparte = server.CreateObject("adodb.recordset")	

		sqlbuscocontraparte = "select ica_raso as Nombre_Contraparte, Nombre as Nombre_Proyecto, DescMunicipio as Municipio, " 
		sqlbuscocontraparte = sqlbuscocontraparte & "DescDepto as Departamento, DescLocalidad as Localidad "
		sqlbuscocontraparte = sqlbuscocontraparte & "from visProyectos where Gecal = " & rsRif("GECAL") & " and Programa = " & rsRif("PROGRAMA") 
		sqlbuscocontraparte = sqlbuscocontraparte & " and Proyecto = " & rsRif("PROYECTO")

		'Response.Write sqlbuscocontraparte
'PL-SQL * T-SQL  
SQLBUSCOCONTRAPARTE = TransformPLSQLToTSQL (SQLBUSCOCONTRAPARTE) 
		set rsbuscocontraparte = connProyectos.execute(sqlbuscocontraparte)
		if not rsbuscocontraparte.eof then 
			%>
			<table border="0" cellpadding="6" cellspacing="0" width="500">
				<tr>
					<td width="100%" colspan="8" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
				</tr>					
			</table>	
			<br>
			<table border="0" cellpadding="6" cellspacing="0" width="500">
				<tr>
					<td class="tbltext1" width="130"><b>Datos del Proyecto:</b></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td class="tbltext1" width="130"></td>
				  	<td class="tbltext1"><b>Nombre:</b>	</td>
					<td class="textblack"><b><%=Ucase(rsbuscocontraparte("Nombre_Proyecto"))%></b></td>
				</tr>		
				<!--<tr>
					<td class="tbltext1" width="130">&nbsp; </td>
				  <td class="textblack"><b>Nombre: <%=Ucase(rsbuscocontraparte("Nombre_Proyecto"))%></b></td>
				</tr>-->
				<tr>
					<td class="tbltext1" width="130"> </td>
					<td class="tbltext1"><b>Organismo Responsable:</b>	</td>
					<td class="textblack"><b><%=trim(rsbuscocontraparte("Nombre_Contraparte"))%></b></td>
				</tr>	
				<tr>
					<td class="tbltext1" width="130"> </td>
					<td class="tbltext1"><b>Municipio:</b>	</td>
					<td class="textblack"><b><%=Ucase(rsbuscocontraparte("Municipio"))%></b></td>
				</tr>	
				<tr>
					<td class="tbltext1" width="130"></td>
					<td class="tbltext1"><b>Departamento:</b>	</td>
					<td class="textblack"><b><%=Ucase(rsbuscocontraparte("Departamento"))%></b></td>
				</tr>
				<tr>
					<td class="tbltext1" width="130"></td>
				  	<td class="tbltext1"><b>Localidad:</b>	</td>
					<td class="textblack"><b><%=Ucase(rsbuscocontraparte("Localidad"))%></b></td>
				</tr>
				<!--<tr>
				<%
					sDuracion = rsRif("DURACION")
					sTipoDuracion = rsRif("DURACIONUMEDIDA")
				%>
				<td class="tbltext1" width="130">&nbsp; </td>
				<td class="textblack">
				<br>
				<b>Duraci�n: <input type="text" name="txtDuracion" value="<%=sDuracion%>" class="textblack" maxlength="3" size="3">
				&nbsp;&nbsp;Medida en: <%CreateComboDizDati "FORMAZIONE", "COD_TIPO_DURACION", "cmbTipoDuracion", "textblack", "", sTipoDuracion%>
				</b>
				</td>
				</tr>-->
			</table>
			<%
		end if
		set rsbuscocontraparte = nothing
	end if 
	%>



	<!--FIN DATOS CONTRAPARTE Y PROYECTO-->
	<table border="0" cellpadding="6" cellspacing="0" width="500">
		<tr>
			<td width="100%" colspan="8" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
		</tr>					
	</table>	
	<br>
	

	<!--#include virtual="/include/VerificaProyectos.asp"-->	
		
	<%	
		VieneProy = VieneDeProyectos (sIdAnnuncio)
	%>
	
	<!--
		<tr height="23">			
			<td width="200" class="tbltext1">
			<b>Nombre del Proyecto Original:</b>
			</td>
			<td class="textblack">
				<b><%=sNomProj%></b>
				<input type="hidden" name="txtnomproj" value="<%=sNomProj%>">
			</td>
		</tr>
		
	<%
		''''VANI 30-05-06''''

		SQLEMPR = ""
		SQLEMPR = SQLEMPR & "SELECT LC.DESCRIPCION DESCLOC,DP.DESCRIPCION DESCDEPTO " 
		SQLEMPR = SQLEMPR & "FROM PLAVORO.SEDE_IMPRESA SI "
		SQLEMPR = SQLEMPR & "INNER JOIN PLAVORO.LOCALIDAD LC "
		SQLEMPR = SQLEMPR & "ON LC.CODLOC = SI.LOC "
		SQLEMPR = SQLEMPR & "INNER JOIN PLAVORO.DEPARTAMENTO DP "
		SQLEMPR = SQLEMPR & "ON LC.DEPTO = DP.CODDEPT "
		SQLEMPR = SQLEMPR & "WHERE DP.PROVINCIA = SI.PRV "
		SQLEMPR = SQLEMPR & "AND SI.ID_SEDE = " & sSede       
		
		
'PL-SQL * T-SQL  
SQLEMPR = TransformPLSQLToTSQL (SQLEMPR) 
		set rsempr = cc.execute(sqlempr)
		
		if not rsempr.EOF then 
			sDepto = rsempr("DESCDEPTO")
			sLocalidad = rsempr("DESCLOC")
		end if 
		
		SQLEMPR = ""
		SQLEMPR = SQLEMPR & "SELECT DESCOM FROM COMUNE CO INNER JOIN SEDE_IMPRESA SI "
		SQLEMPR = SQLEMPR & "ON CO.CODCOM = SI.COMUNE WHERE "
		SQLEMPR = SQLEMPR & "CO.PRV = SI.PRV AND SI.ID_SEDE =" & sSede

'PL-SQL * T-SQL  
SQLEMPR = TransformPLSQLToTSQL (SQLEMPR) 
		set rsempr = cc.execute(sqlempr)
		
		if not rsempr.EOF then 
			sMunicipio = rsempr("DESCOM")
		end if

		
	%>
		<tr height="23">			
			<td width="200" class="tbltext1">
			<b>Municipio:</b>
			</td>
			<td class="textblack">
				<b><%=sMunicipio%></b>
				<input type="hidden" name="txtmunicipio" value="<%=sMunicipio%>">
			</td>
		</tr>
		
		<tr height="23">			
			<td width="200" class="tbltext1">
			<b>Departamento:</b>
			</td>
			<td class="textblack">
				<b><%=sDepto%></b>
				<input type="hidden" name="txtdepto" value="<%=sDepto%>">
			</td>
		</tr>
		
		<tr height="23">			
			<td width="200" class="tbltext1">
			<b>Localidad:</b>
			</td>
			<td class="textblack">
				<b><%=sLocalidad%></b>
				<input type="hidden" name="txtlocalidad" value="<%=sLocalidad%>">
			</td>
		</tr>-->

<% 
	TAviso = ucase(rsRif("TIPOANUNCIO"))
	if TAviso = "" or TAviso = "L" then 
		EtiquetaI = "Grupo Ocupacional:"
		EtiquetaII = "Denominaci�n del puesto/ocupaci�n:"
		EtiquetaIII = "Rama de Actividad:"
		EtCant = "Trabajadores demandados para el puesto:"
		EtFecha = "Fecha de Incorporaci�n al puesto de trabajo:"
	else
		EtiquetaI = "Tipolog�a:"					
		EtiquetaII = "Subtipolog�a:"
		EtiquetaIII = "Tipo de Prestaci�n:"
		EtCant = "Participantes/Vacantes disponibles:"
		EtFecha = "Fecha de Inicio:"
	end if 
	Direccion=Ucase(rsRif("DIRECCION"))
	if IsNull(rsRif.Fields("direccion"))then 
		Direccion = "(Informaci�n no suministrada)"
	end if 
%>
	<!--<tr height="23">			
    <td width="130" class="tbltext1"><b><%=EtiquetaIII%></b></td>
	<td align="left" class="textblack">
		<b><%=sSettoreDesc%></b>
		<input type="hidden" name="txtramaact" value="<%=sSettoreDesc%>">
		</td>
		</tr>-->
	<!--<tr><td colspan="2">	
	<table>-->	   


<%If sPubblica = "S" then%>
	<table border="0" cellpadding="6" cellspacing="0" width="500">

		<!--GRUPOS OCUPACIONALES/TIPOLOGIAS Y PUESTOS/SUBTIPOLOGIAS VANI-->
		<tr>
			<td class="tbltext1" width="130"><b><%=EtiquetaI%></b></td>
			<td class="textblack"><b><%=sAreaProfDesc%></b>
			<input type="hidden" name="txtAreaHid" value="<%=sAreaID%>"></td>
		</tr>
		<tr>
			<td class="tbltext1" width="130"> <b><B><%=EtiquetaII%></b></td>
			<td class="textblack"><b><%=sFigProfDesc%></b>
			<input type="hidden" name="txtProfiloHid" value="<%=sFiguraID%>"></td>
		</tr>
		<!--FIN GRUPOS OCUPACIONALES/TIPOLOGIAS Y PUESTOS/SUBTIPOLOGIAS-->
	</table>
<%else%>
	<table border="0" cellpadding="6" cellspacing="0" width="500">
		<!--GRUPOS OCUPACIONLES/TIPOLOGIAS Y PUESTOS/SUBTIPOLOGIAS VANI-->		
		<tr>
			<td class="tbltext1" width="130"><b><%=EtiquetaI%></b></td>
			<td class="tbltext1">
				<textarea rows="4" class="textblacka" readonly type="text" name="txtArea" style="width=270px"><%=sAreaProfDesc%></textarea>
				<input type="hidden" name="txtAreaHid" value="<%=sAreaID%>">
				<!--</td>
				<td align="left" class="tbltext1"><b>-->
				<% if TAviso = "" or TAviso = "L" then %>
					<a href="Javascript:SelSpecifico('AREE_PROFESSIONALI','ID_AREAPROF','txtArea')" ID="imgPunto2" name="imgPunto2"><img border="0" src="<%=Session("Progetto")%>/images/bullet1.gif"></a>
				<% elseif TAviso = "C" then%>
					<a href="Javascript:SelSpecifico('AREA_CORSO','ID_AREA','txtArea')" ID="imgPunto2" name="imgPunto2"><img border="0" src="<%=Session("Progetto")%>/images/bullet1.gif"></a>		
				<% end if %>
			</td>
		</tr>
		<tr>
			<td class="tbltext1" width="130"><b><%=EtiquetaII%></b></td>
			<td class="tbltext1" align="right">
				<textarea rows="4" class="textblacka"  readonly type="text" name="txtProfilo" style="width=270px"><%=sFigProfDesc%></textarea>
				<input type="hidden" name="txtProfiloHid" value="<%=sFiguraID%>">
				<!--</td>
				<td align="left" class="tbltext1"><b>-->
				<% if TAviso = "" or TAviso = "L" then %>
					<a href="Javascript:SelSpecifico('FIGUREPROFESSIONALI','ID_FIGPROF','txtProfilo')" ID="imgPunto3" name="imgPunto3"><img border="0" src="<%=Session("Progetto")%>/images/bullet1.gif"></a>
				<% elseif TAviso = "C" then%>
					<a href="Javascript:SelSpecifico('AREA_CORSO_SUB','ID_SUBAREA','txtProfilo')" ID="imgPunto3" name="imgPunto3"><img border="0" src="<%=Session("Progetto")%>/images/bullet1.gif"></a>
				<% end if %>
			</td>
		</tr>
		<!--FIN GRUPOS OCUPACIONLES/TIPOLOGIAS Y PUESTOS/SUBTIPOLOGIAS-->
	</table>
<%End if%>	

<!--</table></td></tr>-->
		
<script>
function EnLoad()
{

	if (document.frmBacheca.PersDisc.options[0].checked)
	{
	document.frmBacheca.cmbDisc.disabled = false;
	}
	else
	{
	document.frmBacheca.cmbDisc.value = "";
	document.frmBacheca.cmbDisc.disabled = true;
	}
	
}
function HabDes(Valor)
{
	if (Valor == "S")
	{
	document.frmBacheca.cmbDisc.disabled = false;
	}
	else
	{
	document.frmBacheca.cmbDisc.value = "";
	document.frmBacheca.cmbDisc.disabled = true;
	}
}
</script>

	<!--INICIO ADAPTACION AL FORMULARIO DE INTERMEDIACION LABORAL-->
<%
	if TAviso = "L" or TAviso = "" then   ''''vani 15-06-2006
%>			
	<table border="0" cellpadding="6" cellspacing="0" width="500">
			<tr>
				<td class="tbltext1" width="130"> 
					<b>Personal a Cargo</b> 
				</td>
				<td class="tbltext1">
					<input type="radio" name="PersCargo" value="S" <%if sPersCargo = "S" then Response.Write "checked"%>>SI
					<input type="radio" name="PersCargo" value="N" <%if sPersCargo = "N" then Response.Write "checked"%>>NO
				</td>
				<td align="left" width="60" class="tbltext1">
				</td>
			</tr>
	</table>
	<br>
<%
	else
%>	
		<input type="hidden" name="PersCargo" value="">
<%
	end if   ''''vani 15-06-2006
%>

<!--FIN ADAPTACION AL FORMULARIO DE INTERMEDIACION LABORAL-->			
					
<!--INICIO ADAPTACION AL FORMULARIO DE INTERMEDIACION LABORAL-->	

<%if TAviso = "L" or TAviso = "" then %>	
	<table border="1" cellpadding="6" cellspacing="0" bordercolor="MidNightBlue" width="500">
		<tr bordercolor="white">
			<td width="130" class="tbltext1"> 
				<b>El puesto solicitado admite postulantes con alg�n tipo de discapacidad?</b> 
			</td>
			<td class="tbltext1">
					<input type="radio" name="PersDisc" value="S" onclick="HabDes('S')" <%if sPersDisc = "S" then Response.Write "checked"%>>SI
					<input type="radio" name="PersDisc" value="N" onclick="HabDes('N')" <%if sPersDisc = "N" then Response.Write "checked"%>>NO
					<br>
					&nbsp;<b>Tipo de Discapacidad?</b>&nbsp;
					<%				
					ComboDisc = "TCAPI|0|" & date & "|" & sPersTipoDisc & "|cmbDisc|ORDER BY DESCRIZIONE"			
					CreateCombo(ComboDisc)
					%>
			</td>
		</tr>
		<tr bordercolor="white">
			<td width="130" class="tbltext1"> 
				<b>Observaciones</b> 
			</td>
			<td class="tbltext1">
				<textarea name="Obs_Disc" rows=7 cols=35 WRAP="ON"><%=trim(sDiscObs)%></textarea>
			</td>
		</tr>
	</table>
	<br>
<%else%>
	<input type="hidden" name="PersDisc" value="N">
	<input type="hidden" name="PersDisc" value="N">
	<input type="hidden" name="cmbDisc" value="">
	<input type="hidden" name="Obs_Disc" value="">
<%end if%>
	<!--<tr height="17" bordercolor="white">
		<td width="200" class="tbltext1"> 
			<b>Observaciones</b> 
		</td>
		<td colspan="2" class="tbltext1">
			<textarea name="Obs_Disc" rows=7 cols=45 WRAP="ON"><%=trim(sDiscObs)%></textarea>
		</td>
	</tr>-->

<!--FIN ADAPTACION AL FORMULARIO DE INTERMEDIACION LABORAL-->	

<table border="0" cellpadding="6" cellspacing="0" width="500">
			
	<%if VieneProy = "N" then %>
		<tr>      
			<td width="130" class="tbltext1"> <b><B><%=EtCant%></b> </td>
			<td class="tbltext1">
				<input class="textblacka" type="text" name="txtNumPosti" style="width=40" maxlength="4" VALUE="<%=nNumPosti%>" size="20">
			</td>
		</tr>
	<%else%>
		<tr>      
			<td width="130" class="tbltext1"> <b><B><%=EtCant%></b> </td>
			<td class="tbltext1">
				<input class="textblacka" type="text" readonly name="txtNumPosti" style="width=40" maxlength="4" VALUE="<%=nNumPosti%>" size="20">
			</td>
		</tr>
	<%end if%>
	<tr>
		<td>
			<input type="hidden" name="txtIdAnnuncio" value="<%=rsRif("ID_RICHIESTA")%>"> 
			<input type="hidden" name="txtSettoreHid" value="<%=rsRif("ID_SETTORE")%>"> 
			<!--<input type="hidden" name="txtAreaHid" value="<%=rsRif("ID_AREAPROF")%>">
			<input type="hidden" name="txtProfiloHid" value="<%=rsRif("ID_FIGPROF")%>">-->
		</td>
	</tr>
	<tr>
		<td class="tbltext1" width="130"><b>Presentarse en: </b></td>
		<td class="textblack"><b> <%=Direccion%></b></td>
	</tr>
	<%
	sDuracion = rsRif("DURACION")
	sTipoDuracion = rsRif("DURACIONUMEDIDA")
	%>
	<tr>
		<td class="tbltext1"><b>Duraci�n:</b></td>
		<td class="tbltext1">
		<b><input type="text" name="txtDuracion" value="<%=sDuracion%>" class="textblack" maxlength="3" size="3"> <!--<input type="text" name="txtDuracion" value="<%=sDuracion%>" class="textblack" readonly maxlength="3" size="3">-->
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Medida en: <%CreateComboDizDati "FORMAZIONE", "COD_TIPO_DURACION", "cmbTipoDuracion", "textblack", "", sTipoDuracion%>
		<%'CreateComboDizDati "FORMAZIONE", "COD_TIPO_DURACION", "cmbTipoDuracion", "textblack", "", sTipoDuracion%>
		</b>
		</td>
	</tr>
	
	<%
	dim visualizza
	dim visualizzaVal
	dim etichetta
	'Visualizzo nella pagina solamente una TEXT BOX con 
	'l'indicazione geografica pi� specifica presente nel DB.
	'3 casi: 1� Se nel DB ho REGIONE e PROV visualizzo PROV
	'			2� Ae nel DB ho solo REGIONE visualizzo REGIONE
	'			3� Se nel DB ho lo STATO visualizzo STATO
	'-------------------------------------------------------

	'if sRegioneDesc <> "" then
		if sProvDesc <> "" then
			visualizza = sProvDesc
			visualizzaVal = sProv
			etichetta = "Provincia:"
		'else
		'	visualizza = sRegioneDesc
		'	visualizzaVal = sRegione
		'	etichetta = "Region:"		
		end if
	'else
	'	visualizza = sStatoDesc
	'	visualizzaVal = sStato
	'	etichetta = "Pais Extranjero:"		
	'end if
	%>		
	<tr>
		<td width="130" class="tbltext1">
			<b><%=etichetta%></b>
		</td>
		<td class="textblack">
			<b><%=visualizza%></b>
		</td>
	</tr>
		
	<tr>
		<td>
			<input type="hidden" name="txtValoreHid" value="<%=visualizzaVal%>">
			<input type="hidden" name="txtValiditaHid" style="width=270px" value="<%=sValidita%>">
			<input type="hidden" name="txtoggi" value="<%=ConvDateToString(Date())%>">
			<input type="hidden" name="txtDtTmst" value="<%=sDtTmst%>">
		</td>
	</tr>
		

	<%
	if VieneProy = "N" then 
	%>	
		<tr>
			<td width="130" class="tbltext1"> <b><B>Plazo de derivaci�n de postulantes:</b></td>
			<td class="tbltext1">
				<input class="textblacka" type="text" name="txtValidita" style="width=71px" value="<%=sValidita%>" maxlength="10" size="20">
			<b class="tbltext1">&nbsp;&nbsp;(dd/mm/aaaa)</b> </td>
		</tr>
	<%
	else
	%>
		<tr>
			<td width="130" class="tbltext1"> <b><B>Plazo de derivaci�n de postulantes:</b></td>
			<td class="tbltext1">
				<input class="textblacka" type="text" name="txtValidita" readonly style="width=71px" value="<%=sValidita%>" maxlength="10" size="20">
			<b class="tbltext1">&nbsp;&nbsp;(dd/mm/aaaa)</b> </td>
		</tr>
	<%
	end if
	%>
		
	<%if VieneProy = "N" then %>
		<tr>
			<td class="tbltext1" width="130"><b><%=EtFecha%></b></td>
	        <td class="textblack"> 
			<input class="textblacka" type="text" name="txtDtAvv" size=20 style="width=71px" maxlength="10" value="<%=sDtAvv%>">
			<b class="tbltext1">&nbsp;&nbsp;(dd/mm/aaaa)</b> </td>
		</tr>
			
	<%else%>
		<tr>
		    <td class="tbltext1" width="130"><b><%=EtFecha%></b></td>
	        <td class="textblack"> 
			<input class="textblacka" type="text" readonly name="txtDtAvv" size=20 style="width=71px" maxlength="10" value="<%=sDtAvv%>">
	        <b class="tbltext1">&nbsp;&nbsp;(dd/mm/aaaa)</b> </td>
		</tr>
	<%end if%>

	</tr>
	<tr height="2">
		<td width="100%" colspan="2" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
	</tr>				
	<%
	sPrj = replace(Session("Progetto"),"/","\")
	On error Resume Next
		
	if sPubblica = "S" then 
'	   		pathname = server.mappath("\") & Session("Progetto") & "\Testi\sistdoc\Bacheca\" & sIdAnnuncio & ".txt"
				''pathname = server.mappath("\") & ImpostaProgetto & "\Testi\sistdoc\Bacheca\" & sIdAnnuncio & ".txt"
			pathname = ""
	else
'			pathname = server.mappath("\") & Session("Progetto") & "\Testi\sistdoc\Bacheca\" & "P" & "" & sIdAnnuncio & ".txt"
	 		''pathname = server.mappath("\") & ImpostaProgetto & "\Testi\sistdoc\Bacheca\" & "P" & "" & sIdAnnuncio & ".txt"
		
		 	pathname = ""
	end if
	
	if pathname = "" then 
		dim nsql
		if rsRif.State = 1 then 
			rsRif.Close 
		end if 
		
		nsql = "Select ACTIVIDADEMPRESA, TEXTOANUNCIO from RICHIESTA_SEDE where id_RICHIESTA = " & sIdAnnuncio
		
'PL-SQL * T-SQL  
NSQL = TransformPLSQLToTSQL (NSQL) 
		rsRif.Open nsql,CC
		
		if not rsRif.EOF then 
			ATTIVITA = rsRif("ACTIVIDADEMPRESA")
			TESTO = rsRif("TEXTOANUNCIO")
			'Response.Write "pase"
			
			'Response.Write ATTIVITA
			'Response.Write TESTO
		end if
	else
		pathname =replace(pathname,"/","\")
		'Response.Write "pathname " & pathname
	
		Set fso = CreateObject("Scripting.FileSystemObject")
		Set f = fso.OpenTextFile(pathname)
		
		'Response.write pathname
		If NOT f.AtEndOfStream Then ATTIVITA = f.readline
		If NOT f.AtEndOfStream Then TESTO = f.readline		
		If NOT f.AtEndOfStream Then RIFERIMENTI = f.readline

		f.close 				
		On error goto 0
	end if 
	%>
	<tr>
		<td width="130" class="tbltext1"> <b><B>Actividad Principal:</b> </td>
			<td class="tbltext1">
				<textarea NAME="ATTIVITA" ROWS="7" COLS="35"><%=ATTIVITA%></textarea>
			</td>
			<td align="center" width="30" height="30" class="tbltext1"><b>
			</td>
		</tr>	
	<tr>
		<td width="100%" colspan="2" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
	</tr>
	<tr>
		<td width="130" class="tbltext1"> <b>Texto del Aviso:</b> </td>
			<td class="tbltext1">
					<textarea NAME="TESTO" ROWS="7" COLS="35"><%=TESTO%></textarea>
		</td>
		<td align="center" width="30" height="30" class="tbltext1"><b>
		</td>
	</tr>
		
	<tr>
		<td width="100%" colspan="2" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
	</tr>
</table>


<%
' ----------------------------------------------------------------------------------
' Integracion de los filtros en la misma pagina del detalle del aviso  RIC 09/2006
' ----------------------------------------------------------------------------------
%>

<br><br>
<table border="0" cellspacing="1" cellpadding="1" width="500" align="center">
<%
	nIdRic = sIdAnnuncio
	'Response.Write "nIdRic: " & nIdRic

	sSQL = "SELECT a.ID_TAB,a.ID_CAMPO,a.COD_OPLOG,a.VALORE,a.PRG_REGOLA,a.PRG_REGOLA_RIF, b.DESC_TAB" &_
			" FROM RICHIESTA_REGOLE a, DIZ_TAB b " &_ 
			" WHERE ID_RICHIESTA=" & nIdRic &_
			" AND a.ID_TAB = b.ID_TAB " &_
			" ORDER BY ID_TAB,PRG_REGOLA,PRG_REGOLA_RIF"
	'Response.Write sSQL
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	set rsCriteri = CC.Execute(sSQL)
	if not rsCriteri.EOF then
		z = 0
		aOpLog = decodTadesToArray("OPLOG",date,"",1,0)
		sDescrTab= rsCriteri("DESC_TAB")
		memId =  rsCriteri.Fields("ID_TAB")











		do while not rsCriteri.EOF
			if 	rsCriteri.Fields("ID_TAB") <> memId then
				sDescrTab= rsCriteri("DESC_TAB")
				memId =  rsCriteri.Fields("ID_TAB")
				%>
					<tr>
						<td colspan="3">
							<table border="0" CELLPADDING="0" CELLSPACING="0" width="500" align="center">
								<tr height="18">
									<td class="sfondomenu" width="350" height="18"><span class="tbltext0"><b>&nbsp;<%=sDescrTab%>   </b></span></td>
									<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
									<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
								</tr>
							</table>
						</td>
					</tr>
				<%
			end if
			%>
			<tr class="tblsfondo"> 
				<td class="textblack" align="left" valign="center" width="140">
					<%
						sSQL = "SELECT ID_DESC,ID_CAMPO,DESC_CAMPO,ID_CAMPO_RIF, ID_TAB_RIF,TIPO_CAMPO,ID_CAMPO_DESC " &_ 
							" FROM DIZ_DATI " &_ 
							" WHERE ID_TAB='" & rsCriteri("ID_TAB")	& "'" &_ 
							" AND ID_CAMPO='" & rsCriteri("ID_CAMPO") & "'" 
						'Response.Write "sSQL: " & sSQL
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
						set rsDecod = CC.Execute(sSQL)
						Response.Write  ucase(rsDecod("DESC_CAMPO"))
						sDecNomTab		= rsDecod("ID_TAB_RIF")
						sTipoCampo		= rsDecod("Tipo_campo")
						sDescNomeCampo  = rsDecod("ID_CAMPO_DESC")
						sDenominazione  = rsDecod("ID_DESC")
					if isnull(rsDecod("ID_CAMPO_rif")) then
						sDecNomCampo	= rsDecod("ID_CAMPO")
					else
						sDecNomCampo	= rsDecod("ID_CAMPO_RIF")
					end if
					rsDecod.Close
					set rsDecod = Nothing
					'Response.Write "sDecNomTab:" & sDecNomTab
					%>
				</td>
				<td class="textblack" align="left" valign="center" width="80">
					<%=(UCASE(searchIntoArray(rsCriteri("COD_OPLOG"),aOpLog,0)))%>
				</td>
				<td class="textblack" align="left" valign="center" width="220">
					<% 
					if sDecNomTab <> "" then
						select case mid(sDecNomTab,1,4)
						case "TAB_"
							aValore = Split(rsCriteri("Valore"),",",-1,1)
							lung = Ubound(aValore)
							sTab = ""
							for i = 0 to lung 
								sTab = sTab & ucase(DecCodVal(mid(sDecNomTab,5), 0, date, replace(aValore(i),"$",""),0)) & ","
							next
							Response.Write left(sTab,len(sTab) - 1)
						case "COMU"
							aValore = Split(rsCriteri("Valore"),",",-1,1)
							lung = Ubound(aValore)
							sCom=""
							if lung > 0 then 
								for i = 0 to lung 
									sSQL = "SELECT DESCOM FROM " & sDecNomTab & " WHERE " &_
									sDecNomCampo & " = " & Replace(aValore(i),"$","'")
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
									set rsDecod = CC.Execute(sSQL)
									sCom = sCom  &  ucase(rsDecod("DESCOM")) & ","
									rsDecod.Close
									set rsDecod = Nothing
								next
								Response.Write left(sCom,len(sCom) - 1)
							else
								sSQL = "SELECT DESCOM FROM " & sDecNomTab & " WHERE " &_
								sDecNomCampo & " ='" & aValore(0) & "'"
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
								set rsDecod = CC.Execute(sSQL)
								Response.write ucase(rsDecod("DESCOM")) 
								rsDecod.Close
								set rsDecod = Nothing
							end if
						case else
							aValore = Split(rsCriteri("Valore"),",",-1,1)
							lung = Ubound(aValore)
							Denom = ""
							for i = 0 to lung 
								sSQL = "SELECT " & sDenominazione & " FROM " & sDecNomTab & " WHERE " &_
								sDecNomCampo & " = " & Trasforma(replace(aValore(i),"$",""),sTipoCampo)
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
								set rsDecod = CC.Execute(sSQL)
								Denom = Denom & ucase(rsDecod(0)) & ","
								rsDecod.Close
								set rsDecod = Nothing
							next
							Response.Write left(Denom,len(Denom) - 1)
						end select
					else
						if sDescNomeCampo <> "" then
							aVal = split(sDescNomeCampo,"|")
							for i=0 to ubound(aVal) step 2
								if aVal(i) = replace(rsCriteri("VALORE"),"$","") then
									Response.Write server.HTMLEncode(UCASE(aVal(i+1)))
								end if
							next
						else
							Response.Write server.HTMLEncode(replace(rsCriteri("VALORE"),"$",""))
						end if
					end if
					%>
				</td>
			</tr>
			<%rsCriteri.MoveNext%>
			<%z=z+1%>
		<%loop%>
			<%Erase aOpLog
				'Response.Write "</table>"
				Indietro1()	
				


	%>













		<%
	Else
		%>
			<tr height="17"> 
			    <td colspan="4" class="tbltext3" align="middle" valign="center"> <strong>No 
					se registraron filtros para la b�squeda seleccionada</strong><b><br>
					<%=sDescTab%></b>
				</td>
			</tr>
		<%
	End if
	rsCriteri.close
	set rsCriteri = nothing		
	
			





























%>
	<tr>
		<td width="100%" colspan="2" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
	</tr>
</table>


<br><br>



<table border="0" cellpadding="0" cellspacing="0" width="500" align="center">
	<tr align="center">
		    
    <td><a class="textred" href="javascript:FormOfferte.submit()" onmouseover="javascript: window.status= ' '; return true"><b>Detalle 
      del Ofrecimiento </b></a>
    <td><b>
    <% if Session("Proyecto") = "" then %>
    <a class="textred" onmouseover="window.status=' '; return true" href="javascript:VisCandidati()">Visualizar 
      Postulantes</b> </a>
    <% end if %>
      
      </td>
	   </tr>
		<tr>
			<td>&nbsp;</td>
	   </tr>
	   
		<tr>
			<td align="middle" colspan="2">
			<%if Session("Proyecto") = "" then%>
				<a href="javascript:frmIndietro.submit();"><img src="<%=session("Progetto")%>/images/indietro.gif" border="0"></a>
			<%end if%>
				<input type="image" name="Invia" src="<%=Session("Progetto")%>/images/conferma.gif" border="0" value="Conferma">
				<!-- <td align=middle colspan="2">				  <input type="image" name="Invia" src="<%=Session("Progetto")%>/images/conferma.gif" border="0" value="Conferma" onclick="">				  &nbsp;&nbsp; -->
				<input type="hidden" name="idSede" value="<%=Request("IdSede")%>">
				<input type="hidden" name="appoIdSede" value="<%=appoIdSede%>">
				<input type="hidden" name="Proyecto" value="<%=Proyecto%>">
				<!-- <input type="image" name="Annulla" src="<%=Session("Progetto")%>/images/annulla.gif" border="0" value="Annulla" onclick="return Cancella()"> -->
			</td>
	   </tr>
	   
		</form>
	
	
		<tr>
			<td colspan="2"><br><br></td>
		</tr>

		<tr align="right">
				<form method="post" name="FormElenco" action="RIC_ElencoCriteri.asp">
					<input type="hidden" name="dtTmst" value="<%=sDt_Tmst%>">
					<input type="hidden" name="idRic" value="<%=sIdAnnuncio%>">
					<input type="hidden" name="cmbTipoRicerca" value="<%=nTipoRicerca%>">
					<input type="hidden" name="idProfilo" value="<%=nTipoProf%>">
					<input type="hidden" name="idArea" value="<%=nTipoArea%>">
					<input type="hidden" name="cmbTipoArea" value="<%=sAreaProfDesc%>">
					<input type="hidden" name="cmbTipoProf" value="<%=sFigProfDesc%>">
					<input type="hidden" name="txtDtDal" value="<%=sDtDal%>">
					<input type="hidden" name="txtDtAl" value="<%=sDtAl%>">
					<input type="hidden" name="txtNPosti" value="<%=nNumPosti%>">
					<input type="hidden" name="txtDtAvv" value="<%=sDtAvv%>">
					<input type="hidden" name="cmbTipoRic" value="<%=nTipoRic%>">
					<input type="hidden" name="Idsede" value="<%=sIdsede%>">									
					<input type="hidden" name="IdTipoAnuncio" value="<%=TAviso%>">
					<input type="hidden" name="Proyecto" value="<%=Proyecto%>">
					
					<td colspan="2"> 
						<a class="textred" href="javascript:FormElenco.submit()" onmouseover="javascript: window.status= ' '; return true">
							<b>Visualizar Filtros</b>
						</a> 
					</td>
				</form>	
		</tr>	
		
		
		
		
		
		<tr>
			<td>&nbsp;</td>
	   </tr>
	
	   <!--form name="FormOfferte" method="POST" action="..\Richieste\Ric_ElencoOfferte.asp"-->
	     <form name="FormOfferte" method="POST" action="/pgm/Richieste/Ric_ElencoOfferte.asp">
			<!--------------------------------------------------------->
					<input type="hidden" name="CodTipoSel" value="0">
					<input type="hidden" name="cmbTipoRicerca" value="<%=TAviso%>">
					<!---->
					<input type="hidden" name="dtTmst" value="<%=sDtTmst%>">
					<input type="hidden" name="idAnnuncio" value="<%=sIdAnnuncio%>">
										
					<input type="hidden" name="DtFinVal" value="<%=sValidita%>">
					<input type="hidden" name="IdSettore" value="<%=rsRif("ID_SETTORE")%>">
					<input type="hidden" name="Dsettore" value="<%=sSettoreDesc%>">
					<!------area----------->
					<input type="hidden" name="idArea" value="<%=nIdAreaProf%>">
					<input type="hidden" name="cmbTipoArea" value="<%=sAreaProfDesc%>">
					<!------profilo----------->
					<input type="hidden" name="idProfilo" value="<%=nIdFigProf%>">
					<input type="hidden" name="cmbTipoProf" value="<%=sFigProfDesc%>">
					<!--------------->
					<input type="hidden" name="txtNPosti" value="<%=nNumPosti%>">
					<input type="hidden" name="Idsede" value="<%=Request("IdSede")%>">			
			<!--------------------------------------------------------->
			<input type="hidden" name="etichetta" value="<%=etichetta%>">	
			<input type="hidden" name="visualizza" value="<%=visualizza%>">	
			<input type="hidden" name="appoIdSede" value="<%=appoIdSede%>">
			<input type="hidden" name="Proyecto" value="<%=Proyecto%>">
	   </form>
	   
	   <form name="frmVisCand" method="POST" action="BAC_ElencoCandidature.asp">
			<input type="hidden" name="cmbTipoRicerca" value="<%=TAviso%>">
			<input type="hidden" name="idAnnuncio" value="<%=sIdAnnuncio%>">	
			<input type="hidden" name="idAreaProf" value="<%=nIdAreaProf%>">	
			<input type="hidden" name="idFigProf" value="<%=nIdFigProf%>">	

			<input type="hidden" name="settore" value="<%=sSettoreDesc%>">	
			<input type="hidden" name="area" value="<%=sAreaProfDesc%>">	
			<input type="hidden" name="figura" value="<%=sFigProfDesc%>">	
			<input type="hidden" name="NumPosti" value="<%=nNumPosti%>">	
			<input type="hidden" name="etichetta" value="<%=etichetta%>">	
			<input type="hidden" name="visualizza" value="<%=visualizza%>">	

			<input type="hidden" name="idSede" value="<%=Request("IdSede")%>">
			<input type="hidden" name="appoIdSede" value="<%=appoIdSede%>">
			<input type="hidden" name="Proyecto" value="<%=Proyecto%>">
	   </form>

	   <form name="frmIndietro" action="BAC_RicAnnuncioSede.asp" method="POST">
			<input type="hidden" name="idSede" value="<%=Request("IdSede")%>">
			<input type="hidden" name="appoIdSede" value="<%=appoIdSede%>">
			<input type="hidden" name="Proyecto" value="<%=Proyecto%>">
	   </form>
</table>	

<%

rsRif.Close

Set rsRif = Nothing


if Request.Form("ESSEL") <> "" then
	Rifiuta()
end if 
%>
<!--#include virtual="/include/ObtieneSecuencia.asp"-->

<%
function Rifiuta()

		'Response.Write Request.Form("Id_Ric")
		
		nCandidato		= clng(Request.Form("Candidato")) 
		sCodRifiuto		= Request.Form("ESSEL") 
		nIdRichiesta	= clng(Request.Form("Codice"))
		nIdSedeImpresa	= clng(Request.Form("Idsede"))

		sSQL = "UPDATE RICHIESTA_CANDIDATO SET COD_ESITO_SEL='" & sCodRifiuto &_
			"' WHERE ID_PERSONA = " & nCandidato &_
			" AND ID_RICHIESTA = " & nIdRichiesta 
			'Response.Write sSQL
			
		Rifiuta = EseguiNoC(sSQL,CC)


		Secuencia = ObtenerProximaSecuencia(nIdRichiesta,nCandidato)

		sSQL = "INSERT INTO RICHIESTA_CANDIDATO_HIST (SECUENCIA,ID_RICHIESTA,ID_PERSONA,DT_INS,DT_TMST,COD_TIPO_INS,COD_TIPO_SEL,COD_ESITO_SEL) " &_
			"VALUES (" & Secuencia & "," & nIdRichiesta & "," & nCandidato & ",SYSDATE,SYSDATE,'1','0'," & sCodRifiuto & ")"
			
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
		cc.execute sSQL
		
end function
%>
<!--#include Virtual ="/include/CloseConn.asp"-->
<!--#include Virtual="/strutt_coda2.asp"-->
