<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include virtual= "/util/portallib.asp"-->
<!--#include Virtual = "/strutt_testa1.asp"-->
<!--#include Virtual = "/include/SysFunction.asp"-->
<!--#include Virtual = "/include/DecComun.asp"-->
<!--#include Virtual = "/include/DecCod.asp"-->
<!--#include Virtual = "/Include/openconn.asp"-->
<!--#include Virtual = "/include/ElabRegole.asp"-->
<!--#include Virtual = "/include/ImpostaProgetto.asp"-->
<!--#include Virtual = "/include/GestSettori.asp"-->

<script LANGUAGE="Javascript">
<!--#include virtual = "/Include/help.inc"-->
var windowQuadro

function Chiudi(){
	if (windowQuadro != null ) {
		windowQuadro.close(); 
	}
	return true
}

function Occupazione(pth) {	
	windowOcc = window.open (pth,'StatoOccupazionale', 'width=560, height=420, scrollbars=yes,directories=0')	
}	

//function DatiPersonali(Rif)
function DatiPersonali(Rif,FlAna) {	
	windowPersona = window.showModelessDialog('LST_DatiPersonali.asp?IdPers=' + Rif +'&FlAna=' + FlAna ,"InfoPersona","dialogWidth: 780px; dialogHeight: 500px; center: Yes; help: No; resizable: No; status: No");
//	windowPersona = window.showModelessDialog('RIC_DatiPersonali.asp?IdPers=' + Rif ,"InfoPersona","dialogWidth: 780px; dialogHeight: 500px; center: Yes; help: No; resizable: No; status: No");
}

function VisualizzaCV(p){
//	windowC = window.open (p,'Curriculum','width=765, height=500,scrollbars=yes,directories=0,resizable=yes')
	windowC = window.open (p,'Curriculum','scrollbars=yes,directories=0')
}

function Prossimita(n,descr,idp) {	
	document.frmProssimita.Denominazione.value = descr
	document.frmProssimita.Id_figprof.value = n
	document.frmProssimita.IdPers.value = idp
	document.frmProssimita.submit();
}

function Certificazione(nIdPer) {
	windowPersona = window.open("/pgm/richieste/RIC_VisCertificazione.asp?idPer=" +
					nIdPer , "InfoPersona","width=550, height=420; center: Yes; help: No; resizable: No; status: No");
}

function Assumi(nIdPer){
	windowArea = window.open ('/Pgm/Richieste/RIC_Assunzione.asp?IdPer=' + 
				 nIdPer,'Info','width=500,height=250,Resize=No,Scrollbars=yes')
}

</script>
<a name="inizio"></a>
<%	
'********************************
'*********** MAIN ***************
	FLana = 0

	Dim nIdRic , sCittadinanza
	Dim nIdArea, nIdProfilo, sDescArea, sDescProfilo 
	dim appoggio, vettApp, appopers
	dim sDisp_26, sDisp_41, sDisp_42
	DIM sDenom, voto, sSettori
	dim idsett
	
	appoggio   = Request("cmbTipoArea")
	
	'Response.Write ("cmbTipoArea -->" & appoggio)
	
	if appoggio > "" then
		vettApp = split(appoggio,"|")
		nIdArea	  = vettApp(0)
		sDescArea = vettApp(1)
	else
		nIdArea	  = ""
		sDescArea = "-"
	end if

	appoggio   = Request("cmbTipoProf")
	
	if appoggio > "" then
		vettApp = split(appoggio,"|")
		nIdProfilo	 = vettApp(0)
		sDescProfilo = vettApp(1)
	else
		nIdProfilo	 = ""
		sDescProfilo = "-"
	end if

	sCittadinanza = Request("cmbCittadinanza")
	'inizio am
	dim sPriori
	sPriori= Request("chkPriori")
	' fine am
	
'	sIdsede		 = Request ("Idsede")

	Inizio()
	Testata()
	Imposta_Pag()	
	Indietro()

'**********************************************************************************************************************************************************************************	
Sub Imposta_Pag()
	
	Dim sql, sql2, sql3, RR	   'recordset delle persone che soddisfano i parametri
	Dim RRT, sqlT, rs2, rs3  'recordset per titolo di studio
	dim sApProgetto, codmans, appomans

	on error resume next
		
	set RR = server.CreateObject("ADODB.recordset")
		
	on error goto 0
	'inizio am 25/11/2004
'	
' Estrazione dei Prioritari
'	
	if sPriori = "S" then
	'fine 25/11/04
		sql = "select nvl(a.id_sede,0) as Cert,FLOOR(MONTHS_BETWEEN(SYSDATE,A.DT_NASC)/12) As Eta, a.id_persona, a.stat_cit As nazionalita, a.sesso, a.com_res, a.prv_res, " &_
			  " c.cod_stdis  As categoria, c.Id_CImpiego As ID_Impiego, C.DT_TMST, NVL(FLOOR(MONTHS_BETWEEN(SYSDATE,a.DT_ULT_RIL)),'1') As Rilascio, " &_
			  " '1' as DPREL" &_
			  " from PERSONA a, STATO_OCCUPAZIONALE c " 
			
		if nIdProfilo > "" then
			sql = sql & ", PERS_FIGPROF e"
		else
			if nIdArea > "" then
				sql = sql & ", PERS_AREAPROF e"
			end if
		end if
			
		sql = sql & " where a.id_persona = c.id_persona(+)" &_
					" And c.ind_status(+) = 0" 
										
		if nIdProfilo > "" then
			sql = sql & " And e.id_persona = a.id_persona " &_
						" And e.id_figprof = " & nIdProfilo
		else
			if nIdArea > "" then
				sql = sql & " And e.id_persona = a.id_persona " &_
							" And e.id_areaprof = " & nIdArea
			end if
		end if

		sql = sql & " AND EXISTS(select * from formazione where COD_TIPO='33' and cod_status_corso='0' and id_persona=a.id_persona) " 

		If sCittadinanza > "" then
			sql = sql & " AND A.STAT_CIT = '" & sCittadinanza & "'"
		end if
	'INIZIO am 26/11
	else
'	
' Estrazione dei Origine Italiana
'
	if sPriori = "I" then
		sql = "select nvl(a.id_sede,0) as Cert,FLOOR(MONTHS_BETWEEN(SYSDATE,A.DT_NASC)/12) As Eta, a.id_persona, a.stat_cit As nazionalita, a.sesso, a.com_res, a.prv_res, " &_
			  " c.cod_stdis  As categoria, c.Id_CImpiego As ID_Impiego, C.DT_TMST, NVL(FLOOR(MONTHS_BETWEEN(SYSDATE,a.DT_ULT_RIL)),'1') As Rilascio," &_
	  		  " '0' as DPREL" &_
			  " from PERSONA a, STATO_OCCUPAZIONALE c, PERS_PRGMIGRATORIO p " 
			
		if nIdProfilo > "" then
			sql = sql & ", PERS_FIGPROF e"
		else
			if nIdArea > "" then
				sql = sql & ", PERS_AREAPROF e"
			end if
		end if

		sql = sql & " where a.id_persona = p.id_persona" &_
					" and nvl(fl_grdpar,'N')='S'"
					
		sql = sql & " And a.id_persona = c.id_persona(+)" &_
					" And c.ind_status(+) = 0 "
					
		if nIdProfilo > "" then
			sql = sql & " And e.id_persona = a.id_persona " &_
						" And e.id_figprof = " & nIdProfilo
		else
			if nIdArea > "" then
				sql = sql & " And e.id_persona = a.id_persona " &_
							" And e.id_areaprof = " & nIdArea
			end if
		end if

		If sCittadinanza > "" then
			sql = sql & " AND A.STAT_CIT = '" & sCittadinanza & "'"
		end if
	else
	
'	
' Estrazione dei Disponibili
'
		sql = "select nvl(a.id_sede,0) as Cert,FLOOR(MONTHS_BETWEEN(SYSDATE,A.DT_NASC)/12) As Eta, a.id_persona, a.stat_cit As nazionalita, a.sesso, a.com_res, a.prv_res, " &_
			  " c.cod_stdis  As categoria, c.Id_CImpiego As ID_Impiego, C.DT_TMST, NVL(FLOOR(MONTHS_BETWEEN(SYSDATE,a.DT_ULT_RIL)),'1') As Rilascio," &_
	  		  " '0' as DPREL" &_
			  " from PERSONA a, STATO_OCCUPAZIONALE c " 
			
		if nIdProfilo > "" then
			sql = sql & ", PERS_FIGPROF e"
		else
			if nIdArea > "" then
				sql = sql & ", PERS_AREAPROF e"
			end if
		end if
			
		sql = sql & " where a.id_persona = c.id_persona(+)" &_
					" And c.ind_status(+) = 0 " &_
					" And not exists (SELECT * FROM FORMAZIONE WHERE COD_TIPO='33' and cod_status_corso='0' and id_persona=a.id_persona)" 
					
		if nIdProfilo > "" then
			sql = sql & " And e.id_persona = a.id_persona " &_
						" And e.id_figprof = " & nIdProfilo
		else
			if nIdArea > "" then
				sql = sql & " And e.id_persona = a.id_persona " &_
							" And e.id_areaprof = " & nIdArea
			end if
		end if

		If sCittadinanza > "" then
			sql = sql & " AND A.STAT_CIT = '" & sCittadinanza & "'"
		end if
	end if
	end if

	'sql = sql & " order by eta, a.id_persona"
	sql = sql & " order by a.id_persona"	
	
'	Response.Write sql
	
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
	RR.Open sql,CC,1
	
	'fine am	
	if RR.EOF then
		Messaggio(" Nessuna persona risponde ai criteri di ricerca specificati.")
		Err.number = 0
		exit sub
	end if%>
	<table WIDTH="743" BORDER="0" CELLPADDING="2" CELLSPACING="2" align="center">
		<tr>
	        <td class="tbltext1" width="200">
				<b>Area Professionale</b>
			</td>
	        <td class="textblack"> 
				<b><%=sDescArea%></b>
			</td>
		</tr>
		<tr>
	        <td class="tbltext1" width="200">
				<b>Profilo professionale</b>
			</td>
	        <td class="textblack"> 
				<b><%=sDescProfilo%></b>
			</td>
		</tr>
		<tr>
	        <td class="tbltext1" width="200">
				<b>Cittadinanza</b>
			</td>
	        <td class="textblack">
				<b><%=LEFT(DecCodVal("STATO",0,"",sCittadinanza,""),15)%></b>
			</td>
		</tr>
	</table>
	<!--Inizio Am -->
	<table WIDTH="300" BORDER="0">
	<tr>
	<td align="center" class="textred"><font size="2">
				<%
					Select Case sPriori
						case "S"%>
							<b> - Lista Prioritari - </b>
					<%	case "I"%>
							<b> - Lista Disponibili di Origine Italiana - </b>
					<%  case else%>
							<b> - Lista Disponibili - </b>
					<%
					end Select
				%>
				</font>		
			</td>
	</tr>		
	</table>
	<!--Fine am -->
	<br>
	<table WIDTH="743" BORDER="0" CELLPADDING="2" CELLSPACING="2" align="center">
		<tr align="left" height="15px">
			<td valign="top" rowspan="5" class="tbltext1" WIDTH="10">
				<b>N.B.:</b>
			</td>
			<td class="tbltext1">
				Disponibilit� al lavoro: <b>Lavoro Stagionale</b> (L.S.), <b>Tempo Determinato</b> (T.D.), <b>Tempo Indeterminato</b> (T.I.).
			</td>
		</tr>
		<tr>
			<td class="tbltext1">
					L'eventuale riferimento <span class="textred">rosso</span>
					� relativo ai candidati 
					che non hanno aggiornato i 
					dati negli ultimi 12 mesi.
			</td>
		</tr>
		<%if session("LOGIN") > "" then%>
			<tr height="15">					
				<td class="tbltext1">
					Premendo sul riferimento del candidato � possibile accedere ai suoi dati personali.
				</td>
			</tr>
		<%end if%>
	</table>
	<br>
	<input type="hidden" name="idRic" value="<%=nIdRic%>">
	<input type="hidden" name="Idsede" value="<%=sIdsede%>">		
	<table width="700" border="0" cellpadding="1" cellspacing="2" align="center">
		<tr height="35" class="sfondocommaz"> 
			<td align="center" width="100">
				<b class="tbltext1"></b>
			</td>
			<td align="center" width="30">
				<b class="tbltext1">Et�</b>
			</td>
			<td align="center" width="30">
				<b class="tbltext1">Sesso</b>
			</td>
			<td align="center" width="170"> 
				<b class="tbltext1">Titolo di studio</b>
			</td>
			<td align="center" width="200"> 
				<b class="tbltext1">Residenza</b>
			</td>
			<!--td align="center" width="200"> 				<b class="tbltext1">Cittadinanza</b>			</td-->
			<td align="center" width="200"> 
				<b class="tbltext1">Ultima mansione svolta</b>
			</td>
			<td align="center" width="200"> 
				<b class="tbltext1">Disponibilit� di lavoro</b>
			</td>
			<td align="center" width="200"> 
				<b class="tbltext1">Lingue Conosciute</b>
			</td>
			<% if sPriori = "S" then%>
			<td align="center" width="200"> 
				<b class="tbltext1">Settore</b>
			</td>
			<%end if %>
		</tr>	
		<%		
		n = 1 ' contatore: ogni 20 richieste si mette un link all'inizio della pag.
		p = 1 ' contatore: vengono visualizzate solo le prime 40 persone per prossimit� ai criteri di ricerca
		
		sApProgetto = ImpostaProgetto()

		do until RR.Eof or p > 40
		
		'inizio am 24/11/2004
		LeggiDispon(clng(RR.fields("id_persona")))
		' fine am
			sqlT = "Select cod_liv_stud As titolo From TISTUD " &_ 
					" where id_persona (+) =" & clng(RR.fields("id_persona")) &_ 
					" And cod_stat_stud (+)= 0 " &_
					" Order by AA_stud desc, dt_tmst desc"
'PL-SQL * T-SQL  
SQLT = TransformPLSQLToTSQL (SQLT) 
			Set RRT = CC.Execute(sqlT)
			sql2 = "select COD_MANSIONE from ESPRO " &_
			       "where ID_PERSONA = " & clng(RR.fields("id_persona")) &_
			       " order by nvl(DT_FIN_ESP_PRO, sysdate) DESC"
'PL-SQL * T-SQL  
SQL2 = TransformPLSQLToTSQL (SQL2) 
			Set rs2 = CC.Execute(sql2)
			if rs2.Eof then
			   appomans = "-"
			else
			   codmans = rs2("COD_MANSIONE")
			   sql3 = "select MANSIONE from CODICI_ISTAT where CODICE = '" & codmans & "'"
'PL-SQL * T-SQL  
SQL3 = TransformPLSQLToTSQL (SQL3) 
			   Set rs3 = CC.Execute(sql3)
			   appomans = rs3("MANSIONE")
			   rs3.Close
			   set rs3 = nothing
			end if
			if RRT.Eof then
				sTitolo = "-"
			else
				RRT.Movefirst
				sTitolo = RRT.fields("titolo")
			end if
			RRT.Close
			set RRT = nothing
			sqlT = ""
			if n = 21 then
				n = 1
				Response.Write	"<tr> " &_
									"<td align=right colspan=5> " &_
										"<a href='#inizio' title='Inizio pagina'><IMG align=right src='/images/sopra.gif'  border=0></a>" &_	
									"</td>" &_
								"</tr>"
			end if
			'inizio am 26/11

			IF sPriori = "S" then
			   if cint(RR.fields("dprel"))>0 then
			'fine am 26/11
			%>				
			<tr height="35" class="tblsfondo">
				<td width="100" align="middle" class="TBLTEXT1">
					<table width="60" border="0">
						<tr>
							<%bBlanck = True
							if session("LOGIN") > "" then
								FLAna = 1
							end if%>
							<td width="30" align="center">
								<%'if cint(RR.fields("dprel"))>0 then%>
									<!--a onmouseover="javascript: window.status=' '; return true">									<img border="0" src="/images/Coccarda7.gif" WIDTH="19" HEIGHT="30">									</a-->				
								<%'end if%>
							</td>
							<td valign="center" width="30" class="tbltext1">
								<%if clng(RR.Fields("Rilascio")) > 12 then
									ColoreTesto = "textred" 
								else
									ColoreTesto = "TBLTEXT1" 
								end if
								if session("LOGIN") > "" then%>
									<b>
										<a class="<%=ColoreTesto%>" href="javascript:DatiPersonali(<%=clng(RR.fields("id_persona"))%>,<%=FLAna%>)" onmouseover="javascript: window.status=' '; 
											return true"><%=clng(RR.fields("id_persona"))%>
										</a>
									</b>
								<%else%>
									<b>
										<span class="<%=ColoreTesto%>">
											<%=clng(RR.fields("id_persona"))%>
										</span>
									</b>
								<%end if%>	
							</td>
						</tr>
					</table>
				</td>
				<td width="30" align="left" class="textblack">
					&nbsp;<%=RR.Fields("Eta")%>
				</td>
				<!-- Inizio am-->
				<td width="30" align="center" class="textblack">
					&nbsp;<%=RR.Fields("Sesso")%>
				</td>
				<!-- Fine am-->
				<td width="170" align="center" class="textblack">
					&nbsp;
					<%if sTitolo <> "-" then
						sTitolo = lcase(DecCodVal("LSTUD",0,"",sTitolo,""))
					end if
					Response.Write sTitolo
					%>
				</td>
				<td width="200" align="center" class="textblack">&nbsp;
					<%sComune = DescrComune(RR.fields("com_res"))
					if  sComune <> "0" then
						if mid(RR.fields("com_res"),1,1) <> "Z" then
							Response.Write sComune & " - (" &_
							 RR.fields("prv_res") & ")"
						else
							Response.Write sComune
						end if
					else
						Response.Write "-"
					end if%> 
				</td> 
				<!--td width="200" align="center" class="textblack">					&nbsp;<%=LEFT(DecCodVal("STATO",0,"",RR.fields("nazionalita"),""),15)%>				</td-->
				<% if appomans = "-" then %>
					<td width="200" align="center" class="textblack">
						<%=appomans%>
					</td>
				<% else %>
					<td width="200" align="left" class="textblack">
						<%=appomans%>
					</td>
				<% end if %>
				<!-- Inizio am-->
				<td width="200" align="center" class="textblack">
					<%=sDisp_26%>
					<%=sDisp_42%>
					<%=sDisp_41%>
				</td>
				<%
				LeggiLingue(clng(RR.fields("id_persona")))
				%>
				<td width="200" align="center" class="textblack">
					&nbsp;<%=sDenom%>
				</td>
				<%LeggiSettore(clng(RR.fields("id_persona")))%>
				<td width="200" align="center" class="textblack">
					&nbsp;<%=sSettori%>
				</td>
				<!-- Fine am-->
		</tr>
		<%
		'inizio am 26/11
			end if 
		else
			'if sPriori= "N" then%>
				<tr height="35" class="tblsfondo">
				<td width="100" align="middle" class="TBLTEXT1">
					<table width="60">
						<tr>
							<%bBlanck = True
							if session("LOGIN") > "" then
								FLAna = 1
							end if%>
							<td width="30" align="center">
								&nbsp;
							</td>
							<td valign="center" width="30" class="tbltext1">
								<%if clng(RR.Fields("Rilascio")) > 12 then
									ColoreTesto = "textred" 
								else
									ColoreTesto = "TBLTEXT1" 
								end if
								if session("LOGIN") > "" then%>
									<b>
										<a class="<%=ColoreTesto%>" href="javascript:DatiPersonali(<%=clng(RR.fields("id_persona"))%>,<%=FLAna%>)" onmouseover="javascript: window.status=' '; 
											return true"><%=clng(RR.fields("id_persona"))%>
										</a>
									</b>
								<%else%>
									<b>
										<span class="<%=ColoreTesto%>">
											<%=clng(RR.fields("id_persona"))%>
										</span>
									</b> 
								<%end if%>	
							</td>
						</tr>
					</table>
				</td>
				<td width="30" align="center" class="textblack">
					&nbsp;<%=RR.Fields("Eta")%>
				</td>
				<!-- Inizio am-->
				<td width="30" align="center" class="textblack">
					&nbsp;<%=RR.Fields("Sesso")%>
				</td>
				<!-- Fine am-->
				<td width="170" align="center" class="textblack">
					&nbsp;
					<%if sTitolo <> "-" then
						sTitolo = lcase(DecCodVal("LSTUD",0,"",sTitolo,""))
					end if
					Response.Write sTitolo
					%>
				</td>
				<td width="200" align="center" class="textblack">&nbsp;
					<%sComune = DescrComune(RR.fields("com_res"))
					if  sComune <> "0" then
						if mid(RR.fields("com_res"),1,1) <> "Z" then
							Response.Write sComune & " - (" &_
							 RR.fields("prv_res") & ")"
						else
							Response.Write sComune
						end if
					else
						Response.Write "-"
					end if%> 
				</td>
				<!--td width="200" align="center" class="textblack">					&nbsp;<%=LEFT(DecCodVal("STATO",0,"",RR.fields("nazionalita"),""),15)%>				</td-->
				<% if appomans = "-" then %>
					<td width="200" align="center" class="textblack">
						<%=appomans%>
					</td>
				<% else %>
					<td width="200" align="left" class="textblack">
						<%=appomans%>
					</td>
				<% end if %>
				<!-- Inizio am-->
				<td width="200" align="center" class="textblack">
					<%=sDisp_26%>
					<%=sDisp_42%>
					<%=sDisp_41%>
				</td>
				<%
				LeggiLingue(clng(RR.fields("id_persona")))
				%>
				<td width="200" align="center" class="textblack">
					&nbsp;<%=sDenom%>
				</td>
				
				<!-- Fine am-->
		</tr>
			<%'end if 
		end if 
		'fine am 26/11
		%>
		<%RR.Movenext
		n = n + 1
		p = p + 1
		rs2.Close
		set rs2 = nothing
		
	loop
			
	RR.Close
	set RR = nothing
	%>				
</table>
<br>
<form name="frmQuadro" action="/pgm/BilancioCompetenze/Quadro/Default.asp" method="post" target="_new">
	<input type="hidden" name="IdPers" value>
	<input type="hidden" name="Vismenu" value="NO">
	<input type="hidden" name="VisAnag" value="NO">
</form>
<form name="frmProssimita" action="/pgm/BilancioCompetenze/Bilancio_Pross/reportfigprof.asp" method="post" target="_new">
	<input type="hidden" name="Id_figprof" value>
	<input type="hidden" name="IdPers" value>
	<input type="hidden" name="Denominazione" value>
	<input type="hidden" name="Vismenu" value="NO">
	<input type="hidden" name="VisAnag" value="NO">
</form>
<%	
End Sub 
'**********************************************************************************************************************************************************************************	
'inizio Am 24/11/2004
' ************************************************************************************************
	sub LeggiDispon(id_persona)
	Dim RRD, sqlD ' recordset per disponibilit�

	set RRD = server.CreateObject("ADODB.recordset")

			sqlD="Select id_dispon from PERS_DISPON" &_
				 " where id_persona =" & id_persona &_ 
				 " and ind_status='0'" &_
				 " and id_dispon in (26,41,42)"
			
'PL-SQL * T-SQL  
SQLD = TransformPLSQLToTSQL (SQLD) 
			set RRD = CC.execute(sqlD)
			sDisp_26=""
			sDisp_41=""
			sDisp_42=""

			do until RRD.Eof
				select case RRD.fields("id_dispon")
					case "26"
						sDisp_26="L.S." & "<BR>"
					case "41"
						sDisp_41="T.I."	& "<BR>"				
					case "42"
						sDisp_42="T.D."	& "<BR>"				
				end select
				RRD.MoveNext	
			loop
			RRD.Close
			set RRD = nothing
			sqlD=""

	end sub
' ************************************************************************************************	
'02/12/2004
sub LeggiLingue(id_persona)
Dim RC, appo, SW
on error resume next
		
	set RC = server.CreateObject("ADODB.recordset")
	sSQLConosc= "SELECT PC.ID_CONOSCENZA,PC.COD_GRADO_CONOSC,C.DENOMINAZIONE " &_
		  " FROM PERS_CONOSC PC,CONOSCENZE C WHERE " &_
		  " PC.ID_CONOSCENZA=C.ID_CONOSCENZA " &_
		  " AND IND_STATUS= '0' " &_
		  " AND C.ID_AREACONOSCENZA = 23 "  &_
		  " AND ID_PERSONA = " & id_persona &_
		  " ORDER BY DENOMINAZIONE " 
'PL-SQL * T-SQL  
SSQLCONOSC = TransformPLSQLToTSQL (SSQLCONOSC) 
	set RC = CC.execute(sSQLConosc)
	'**********
	'Valore presi dal Db di Produzione di plavoro
	'Lingua: Inglese = 208
	'Lingua: Inglese Tecnico = 591
	'Lingua: Inglese Commerciale = 1645
	'Lingua: Inglese base = 1000226
	'Lingua: Francese = 1221
	'Lingua: Italiano = 1000090 
	'**********
	
	sDenom=""
	SW= 0
	
	DO while not RC.EOF	
		select case  RC.fields("COD_GRADO_CONOSC")
			case "1"
				 voto= "Sufficiente"
			case "2"
				 voto= "Discreto"
			case "3"
				 voto= "Buono"
			case "4"
				 voto= "Ottimo"
		end select
		
		if RC("ID_CONOSCENZA") = "208" or RC("ID_CONOSCENZA") = "591" or RC("ID_CONOSCENZA") = "1645" or RC("ID_CONOSCENZA") = "1000226" or RC("ID_CONOSCENZA") = "1221" or RC("ID_CONOSCENZA") = "1000090"  then 
   		   sDenom = sDenom & RC.Fields("Denominazione") & " (" & voto &")" & "<BR>"
		else
			SW= 1
		end if  	
	Rc.MoveNext 
	Loop
	
	IF SW= 1 THEN
		sDenom = sDenom & "Altre Lingue " & "<BR>"
	END IF
	
	RC.Close
	set RC = nothing
	sSQLConosc=""				  
end sub

' ************************************************************************************************	
sub LeggiSettore(id_persona)
Dim Rs, ap1 
on error resume next
		
	set Rs = server.CreateObject("ADODB.recordset")
	sSQLSett= " SELECT F.ID_PERSONA, F.ID_SETTORE FROM FORMAZIONE f" &_ 
			  " WHERE F.ID_PERSONA = " & id_persona &_
			  " AND COD_TIPO='33' AND cod_status_corso ='0'"  
'PL-SQL * T-SQL  
SSQLSETT = TransformPLSQLToTSQL (SSQLSETT) 
	set Rs = CC.execute(sSQLSett)
	sSettori =""
	
	DO while not Rs.EOF	
		'sSett = sSett & Rs.Fields("") & "<BR><BR>"
		'inizio am
		idsett = clng(rs("ID_SETTORE"))
		ap= DecCodSettori (idsett)

		ap1=split(ap,"|")
		sSettori= sSettori & DecCodAteco(ap1(1)) & "<BR><BR>"
		'fine am
	Rs.MoveNext 
	Loop
	Rs.Close
	set Rs = nothing
	sSQLSett=""				  
end sub

' Fine am


Sub Indietro()
%>
	<table WIDTH="743" BORDER="0" CELLPADDING="0" CELLSPACING="0" align="center">
		<tr align="center">
			<td>
				<a href="javascript:history.back()">
					<img src="<%=Session("progetto")%>/images/Indietro.gif" title="Pagina precedente" border="0" align="absBottom">
				</a>
			</td>
		</tr>
	</table>
<%
End Sub 
'**********************************************************************************************************************************************************************************	
Sub Inizio()
%>
	<table border="0" width="743" cellspacing="0" cellpadding="0" height="70" align="center">
		<tr>
			<td width="700" background="<%=session("progetto")%>/images/titoli/servizi1g.gif" height="70" valign="bottom" align="right">
				<table border="0" background width="700" height="23" cellspacing="0" cellpadding="0">
					<tr>
						<td width="100%" valign="top" align="right"><b CLASS="tbltext1a">Elenco candidati
						  </b></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<br>
	<br>
<%
End Sub
'**********************************************************************************************************************************************************************************
Sub Testata()
%>	
	<!-- Lingetta superiore -->
	<table border="0" CELLPADDING="0" CELLSPACING="0" width="743" align="center">
		<tr height="18">
			<td class="sfondomenu" width="67%" height="18"><span class="tbltext0"><b>&nbsp;ELENCO CANDIDATI</b></span></td>
			<td width="2%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
			<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
		</tr>
	</table>
	<!-- Commento -->
	<table border="0" CELLPADDING="0" CELLSPACING="0" width="743" align="center">
		<tr>
			<td align="left" class="sfondocommaz">
				Elenco dei candidati corrispondenti ai criteri selezionati
			<a href="Javascript:Show_Help('/pgm/help/Liste/LST_ElencoCandidati')"><img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
			</td>
		</tr>
		<tr height="2">
			<td class="sfondocommaz" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>
	<br>
<%
End Sub
'**********************************************************************************************************************************************************************************
Sub Messaggio(Testo)
%>		
	<br>
	<br>
	<table width="743" align="center" border="0" cellspacing="2" cellpadding="1">
		<tr height="20"> 
		  	<td class="tbltext3" align="middle" width="743">
				<b><%=Testo%></b>
			</td>
		</tr>
	</table>
	<br>
	<br>
<%
End Sub
%>	
<!--#include Virtual ="/Include/closeconn.asp"-->
<!--#include Virtual="/strutt_coda1.asp"-->		
