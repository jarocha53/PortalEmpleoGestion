<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include virtual="/include/openconn.asp"-->
<!--#include virtual="/include/DecCod.asp"-->
<!-- #include Virtual="/Pgm/Agenda_Entrevistas/Movimientos_Citas/Soporte/ProcCalendario.asp"-->  
<%
Dim C

Set C = New Calendario
%>
<HTML>
<HEAD>
<TITLE><%=Session("TIT")%> [ <%=Session("login")%> ] </TITLE>
<LINK REL=STYLESHEET TYPE="text/css" HREF='<%=session("progetto")%>/fogliostile.css'>

</head>
<body class=sfondocentro topmargin="2" leftmargin="0">
<script language="javascript">

<!--#include virtual="/Include/ControlNum.inc"-->

function ControlarDatos()
 {
	if(document.frmFeriado.txtFecha.value == "")
	{
		alert("Debe ingresar una fecha");
		return false;
	}
	if(document.frmFeriado.cmbTipoFeriado.value == "")
	{
		alert("Debe seleccionar un tipo de feriado");
		return false;
	}
	if(document.frmFeriado.cmbHoraDesde.value == "")
	{
		alert("Debe seleccionar una hora inicial");
		return false;
	}
	if(document.frmFeriado.cmbHoraHasta.value == "")
	{
		alert("Debe seleccionar una hora final");
		return false;
	}			
	if(document.frmFeriado.txtMotivo.value == "")
	{
		alert("Debe ingresar un motivo");
		return false;
	}
 }
</script>

<br>
<br>

<div align="center">
<table width = "500" border=0>
<tr><td>

<%


anioactual = year(date())

sCodImp=request("CodOfi")
sCodSede = request("CodSede")
sRol = request("Rol")

'Response.Write srol
dim rsFeriados

set rsFeriados = server.CreateObject("adodb.recordset")

if isobject(Session("ObjFeriados")) then
	set ObjFeriados = Session("ObjFeriados")
else
	set ObjFeriados = server.CreateObject("Scripting.Dictionary")
end if  

if sCodSede <> 0 and sCodSede<>"" and Session("IngresoFeriados") = False then
'if Session("IngresoFeriados") = False then 
	Session("IngresoFeriados") = True
	sql = "select * from empleolavoro..agdiasexcepciones where oficina = " & sCodSede 
   
	set rsFeriados = CC.execute(sql)
	
	if not rsFeriados.EOF then 
		if isobject(ObjFeriados) then 
			'ObjFeriados.RemoveAll()
		else
			'set ObjFeriados = server.CreateObject("Scripting.Dictionary")
		end if 
		
		Do while not rsFeriados.EOF 
			clavebase = rsFeriados("Fecha") 
			infobase  = rsFeriados("Tipo") & "|" & C.ConvertirMinutosAHorario(rsFeriados("HoraDesde")) & "|" & C.ConvertirMinutosAHorario(rsFeriados("HoraHasta")) & "|" & rsFeriados("Motivo")
			
			if ObjFeriados.Exists(clavebase) = false then
				ObjFeriados.Add clavebase,infobase
			end if
			rsFeriados.MoveNext 
		loop
		
		Set Session("ObjFeriados") = ObjFeriados
	end if 
'end if  
end if 

if isobject(Session("ObjFeriados")) then
	set ObjFeriados = Session("ObjFeriados")
end if


%>
		<form name="frmFeriado" method="post" action="CnfAgregarFeriado.asp" onsubmit='return ControlarDatos();'>
		<center>
		<input type="hidden" name="CodSede" value="<%=sCodSede%>" />
		<table width=500 border=0>
		
		<tr height="10">
		    <td class="sfondomenu" colspan="3"><span class="tbltext0"> 
		    <b>Feriados</b>
		    </td>
		</tr>
      
		<tr height="10" class="sfondocomm">
				<td colspan="3" class="tbltext1">
				
					Los Feriados que se ingrese/n en esta pantalla 
					no seran guardadas hasta que se completen todos los datos de la Razon Social/Organismo/Institucion
					que se esta ingresando y se presione el bot�n <strong>enviar</strong>, que se encuentra al finalizar el mismo. Aceptando asi los 
					datos ingresados. 
				
				
				</td>		
		</tr>
			<tr height="10">
				<td width="150" class="tbltext1"><br /><br /> 
					<b>Fecha *</b>
				</td>
				<td width="150" class="tbltext1" align="left" colspan="2"><br /><br /> 
					<input type="text" name="txtFecha" class="tbltext1" value maxlength="10"/>
					<input type="hidden" name="Rol" value=<%=sRol%>>
				    <input type="hidden" name="CodImp" value="<%=sCodImp%>">
				</td>
				
			</tr>
			<tr height="10">
				<td width="150" class="tbltext1">
					<b>Tipo Feriado *</b>
				</td>
				<td width="150" class="tbltext1" align="left" colspan="2">
					<%
					sqlTipoFeriado = "select tipo,descripcion from empleolavoro.dbo.AGDIASEXCEPCIONESTIPOS where laborable=0"
					CreateComboDesdeSQL sqlTipoFeriado,"cmbTipoFeriado","tbltext1",sTipoOficina,"","270",connlavoro
					%>
				</td>
			</tr>			

			<tr height="10">
				<td width="150" class="tbltext1">
					<b>Hora Desde *</b>
				</td>
				<td width="150" class="tbltext1" align="left" colspan="2">
                    <%=C.CrearComboAgEnda("cmbHoraDesde","tbltext1","","HORAS","0,1440",sHoraDesde,61)%>
				</td>
			</tr>	
			
			<tr height="10">
				<td width="150" class="tbltext1">
					<b>Hora Hasta *</b>
				</td>
				<td width="150" class="tbltext1" align="left" colspan="2">
				    <%=C.CrearComboAgEnda("cmbHoraHasta","tbltext1","","HORAS","0,1440",sHoraHasta,61)%>
				</td>
			</tr>	

			<tr height="10">
				<td width="150" class="tbltext1">
					<b>Motivo *</b>
				</td>
				<td width="150" class="tbltext1" align="left" colspan="2">
				    <input type="text" name="txtMotivo" class="tbltext1" size="50" maxlength="100" value />
				</td>
			</tr>
												
			<tr><td colspan="3"><br><br></td></tr>
			
			<tr height="10">
				<td align="right" colspan="6" class="tbltext1">
					<input type="submit" value="Agregar >>" class="my" id=submit1 name=submit1>
				</td>
			</tr>
		</table>
		</form>

	       
<%

if isobject(Session("ObjFeriados")) then
	set ObjFeriados = Session("ObjFeriados")
	
		if ObjFeriados.count > 0 then 
		%>	
		<br><br>
		<div align="center">
		<table width=600 border=0 cellpadding="2">		
		<tr>
		<td height="2" align="middle" colspan="6" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
		</tr>
		<%
		Response.Write "<tr height='17' class='tbltext1'>"
		Response.Write "<td width='100' align='left'><b>Fecha</b></td>"
		Response.Write "<td width='100' align='left'><b>Tipo</b></td>"
		Response.Write "<td width='100' align='left'><b>Hora Desde</b></td>"
		Response.Write "<td width='100' align='left'><b>Hora Hasta</b></td>"
		Response.Write "<td width='100' align='left'><b>Motivo</b></td>"		
		
		Response.Write "<td width='25'  align='left'><b>Borrar Feriado</b></td>"
		Response.Write "</tr>"
			
		for each cl in ObjFeriados
			Response.Write "<tr height='17' class='tbltext1'>"
			Response.Write "<td width='100' align='left'>" & cl & "<br></td>"
			
			dim ArrayInfo
            
            ArrayInfo = split(ObjFeriados.item(cl),"|")
            
            for x = 0 to ubound(Arrayinfo)
            	Response.Write "<td width='100' align='left'>" 
            	if x = 0 then 
            	    response.Write ObtenerNombreTipoFeriado(ArrayInfo(x))
            	else
            	    response.Write ArrayInfo(x)
            	end if
            	response.Write "</td>"
            next
            
		
			Response.Write "<td width='25'  align='center'><img src=" & Session("Progetto") & "/images/cestino.gif onclick=javascript:document.location.href='BorrarFeriado.asp?CodImp=" & sCodImp & "&Rol=" & sRol & "&Clave=" & cl & "' style='cursor:hand;' title='Borrar Prestaci�n'></td>"
			if sCodImp = "" then 
				sCodImp = 0
			end if
			'Response.Write "<td width='25'  align='center'><a href='javascript:AbrirDetalle(" & sCodImp & "," & sRol & "," & cl & ")'><img border='0' src='" & Session("Progetto") & "/images/bullet1.gif' title='Ingresar Detalle'></a></td>" 'le paso el rol y el Feriado

			Response.Write "</tr>"	
		next
		%>
	
		<tr height='17' class='tbltext1'>
			<td colspan="2" align='right'><br></td>
		<tr>
		
		<tr height='17' class='tbltext1'>
			<td align='right' colspan='5'><input class="my" type = "button" onclick="javascript:document.location.href='BorrarFeriado.asp?CodImp=<%=sCodImp%>&Rol=<%=sRol%>'"; value="Borrar Todo"></td>
			<td align='right'><input class="my" type = "button" onclick= "javascript:self.close();" value="Enviar"></td>
		<tr>
		<tr>
		<td height="2" colspan="6" align="middle" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
		</tr>	
		</table>
		</div>

		<%
	end if
end if

%>		
</td></tr>

<tr><td>
<br>
<br>
</td></tr>


<tr><td align="center">
<input class="my" type="button" onclick= "javascript:self.close();" value="Cerrar">
</td></tr>

</table>
</div>

</body>
</html>


<%

function ObtenerNombreTipoFeriado(Id)
	Dim rs
	dim sql 
	
	set rs = server.CreateObject("Adodb.recordset")
	
	sql = ""
		
    sql = "select descripcion from AGDIASEXCEPCIONESTIPOS where tipo = " & cint(Id)
	
    'PL-SQL * T-SQL  
    SQL = TransformPLSQLToTSQL (SQL) 
	
	set rs = connLavoro.execute (sql)
	
	if not rs.EOF then 
		ObtenerNombreTipoFeriado = LCASE(rs("descripcion"))
	end if 	
	
	set rs = nothing
end function
%>

<!--#include virtual="/include/closeconn.asp"-->
<% set C = nothing %>