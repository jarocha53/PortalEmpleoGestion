<!--#include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include virtual="/strutt_testa2.asp"-->
<!--#include virtual="/util/dbutil.asp"-->
<!--#include virtual="/include/ControlDateVB.asp"-->
<!--#include virtual="/include/SysFunction.asp"-->
<!--#include virtual="/include/openconn.asp"-->
<!--#include virtual="/include/DecCod.asp"-->
<!--#include Virtual="/Pgm/Agenda_Entrevistas/Movimientos_Citas/Soporte/ProcCalendario.asp"-->  
<%
Dim C

Set C = New Calendario
%>
<script language="javascript">
<!--#include virtual = "/include/SelComune.js"-->
<!--#include virtual = "/include/ControlDate.inc"-->
function CambiarDatos()
{
    document.frmIngreso.action = "OFI_ModOficina.asp"
    document.frmIngreso.submit()
}
function MuestroDependencia()
{
    
    var texto;
    var sOficinaPadre;   
    var sDescOficinaPadre;
    
    if (typeof(document.frmIngreso.codofipadreopcional) != 'undefined')
    {
        sOficinaPadre  = document.frmIngreso.codofipadreopcional.value
        sDescOficinaPadre = document.frmIngreso.descofipadreopcional.value
    }
    
    
    texto = ""
    texto = texto + "<table width='500' class='tbltext1'>"
    texto = texto + "<tr>"
    texto = texto + "<td width='40%'><b>La OE depende de la siguiente OE</b></td>"
    texto = texto + "<td width='60%'>"
    texto = texto + "&nbsp;&nbsp;&nbsp;"
    texto = texto + "<input type='hidden' name='hidOficinaPadre' id='hidOficinaPadre' value='"+sOficinaPadre+"'/>"
    texto = texto + "<input type='text' name='txtOficinaPadre' id='txtOficinaPadre' class='tbltext1' size='50' readonly value='"+sDescOficinaPadre+"'/>"
    texto = texto + "&nbsp;&nbsp;"
    /*texto = texto + "<a href='javascript:SeleccionarOficinaPadre();'>"
    texto = texto + "<img border='0' src='<%=Session("Progetto")%>/images/bullet1.gif'>"
    texto = texto + "</a>"*/
    texto = texto + "</td>"
    texto = texto + "</tr>" 
    texto = texto + "</table>"
    
    lblDepende.innerHTML = texto
}
function OcultoDependencia()
{
    lblDepende.innerHTML = ""
   
}
function SeleccionarOficinaPadre()
{
    var prov
    var muni
    
    prov = document.frmIngreso.cmbProvincia.value
    muni = document.frmIngreso.txtMunicipioHid.value
    
    if (prov != "")
    {
	   windowArea = window.open ('/include/SelOficinaPadre.asp?Provincia='+prov+'&Municipio='+muni+'&NomeForm=frmIngreso'+'&Nome=txtOficinaPadre'+'&NomeHid=hidOficinaPadre','Info','width=422,height=450,Resize=No,Scrollbars=yes,status=yes')	
    }else{
        alert("Debe seleccionar al menos el departamento");
    }
    
}

function AbrirForm(CodImp,Rol,Formulario)
{	
    if (Formulario == "Feriados")
    {
        windowSpecifico = window.open ('FormFeriados.asp?CodSede=' + CodImp + '&Rol=' + Rol, 'Info','width=540,height=450,Resize=No,status=yes,Scrollbars=yes')   
    }
    
    if (Formulario == "Servicios")
    {
        windowSpecifico = window.open ('FormServicios.asp?CodImp=' + CodImp + '&Rol=' + Rol, 'Info','width=540,height=450,Resize=No,status=yes,Scrollbars=yes')
    }
}

function ValidacionFinal()
{
    var IngresoHorario;
    
	if (document.frmIngreso.txtOE.value == "")
		{
		alert("El campo Nombre de la OE es obligatorio");
		return false;
		} 
	
	if (document.frmIngreso.txtEmail.value == "")
		{
		alert("El campo E-mail de la OE es obligatorio");
		return false;
		} 
	else
	{
	    if (document.frmIngreso.txtEmail.value.match("@") != "@")
	    {
	        alert("La direcci�n de email ingresada es incorrecta.")
	        return false;
	    }
	    else
	    {
            if (
            (document.frmIngreso.txtEmail.value.match(".com") != ".com")
            &&
            (document.frmIngreso.txtEmail.value.match(".gov") != ".gov")
            &&
            (document.frmIngreso.txtEmail.value.match(".org") != ".org")       
            &&
            (document.frmIngreso.txtEmail.value.match(".mil") != ".mil")         
            &&
            (document.frmIngreso.txtEmail.value.match(".edu") != ".edu") 
            )
            {
                alert("La direcci�n de email ingresada es incorrecta");
                return false;
            }
            else
            {
                if (document.frmIngreso.txtEmail.value.indexOf("@") == 0)
                    {
                        alert("La direcci�n de correo ingresada es incorrecta");
                        return false;
                    }
                if (document.frmIngreso.txtEmail.value.match("@.") == "@.")
                    {
                        alert("La direcci�n de correo ingresada es incorrecta");
                        return false;
                    }
            }
	    }
	}
	
	if (document.frmIngreso.txtDireccion.value == "")
		{
		alert("El campo Direcci�n de la OE es obligatorio");
		return false;
		} 
    
    if (document.frmIngreso.txtDireccion.value.length < 7)
		{
		alert("El campo Direcci�n de la OE de ser de al menos 7 caracteres");
		return false;
		} 
		
	if (document.frmIngreso.cmbProvincia.value == "")
		{
		alert("El campo Departamento es obligatorio");
		return false;
		} 
	
	if (document.frmIngreso.txtMunicipioHid.value == "")
		{
		alert("El campo Municipio es obligatorio");
		return false;
		} 
	
	/*if (document.frmIngreso.txtCPNH.value == "")
		{
			alert("El campo C�digo Postal es obligatorio");
			return false;
		} 
	else
		{
			if (isNaN(document.frmIngreso.txtCPNH.value) == true)
				{
					alert("El C�digo Postal debe ser num�rico")
					return false;
				}
		}
	
	if (document.frmIngreso.txtCP.value == "")
		{
			alert("El campo C�digo Postal es obligatorio");
			return false;
		} 
	else
		{
			if (isNaN(document.frmIngreso.txtCP.value) == true)
				{
					alert("El C�digo Postal debe ser num�rico")
					return false;
				}
		}*/

   
	if (document.frmIngreso.txtTelContacto.value == "")
		{
			alert("El campo Tel�fono de Contacto es obligatorio");
			return false;
			} 
		else
		{
			if (isNaN(document.frmIngreso.txtTelContacto.value) == true)
				{
					alert("El Tel�fono de Contacto debe ser num�rico")
					return false;
				}
				else
				{
				    if (document.frmIngreso.txtTelContacto.value.length < 8)
				    {
    				    alert("La longitud m�nima permitida para el campo tel�fono de contacto es de 8 n�meros");
    				    return false;
				    }
				}
		}
		
	if (document.frmIngreso.txtTelAlternativo.value == "")
		{
			//alert("El campo Tel�fono Alternativo es obligatorio");
			//return false;
			} 
		else
		{
			if (isNaN(document.frmIngreso.txtTelAlternativo.value) == true)
				{
					alert("El Tel�fono Alternativo debe ser num�rico")
					return false;
				}
				else
				{
				    if (document.frmIngreso.txtTelAlternativo.value.length < 8)
				    {
    				    alert("La longitud m�nima permitida para el campo tel�fono alternativo es de 8 n�meros");
    				    return false;
				    }					
				}
		}
			
	if (document.frmIngreso.cmbTipoOficina.value == "")
		{
		alert("El campo Tipo de Oficina es obligatorio");
		return false;
		} 
			
	if ((document.frmIngreso.optEsSupervisora[0].checked == false) && (document.frmIngreso.optEsSupervisora[1].checked == false))
		{
		alert("El campo Es Supervisora es obligatorio");
		return false;
		} 
		
	if (document.frmIngreso.optEsSupervisora[1].checked == true)
	{
	    if (document.frmIngreso.hidOficinaPadre.value == "")
	    {
	        alert("Si seleccion� la opci�n 'No Es Supervisora' debe seleccionar la OE a la cual se encuentra asociada");
	        return false; 
	    }
	    
	}
	
	if (document.frmIngreso.txtFechaDesde.value == "")
		{
		alert("El campo Fecha Desde es obligatorio");
		return false;
		} 
	else
	    {
	    	if (!ValidateInputDate(document.frmIngreso.txtFechaDesde.value))
            {
	            document.frmIngreso.txtFechaDesde.focus() 
	            return false
            }
	    }
	
	if (document.frmIngreso.txtFechaHasta.value == "")
		{
		alert("El campo Fecha Hasta es obligatorio");
		return false;
		} 
	else
	    {
	    	if (!ValidateInputDate(document.frmIngreso.txtFechaHasta.value))
            {
	            document.frmIngreso.txtFechaHasta.focus() 
	            return false
            }
	    }

	if (ValidateRangeDate(document.frmIngreso.txtFechaDesde.value,document.frmIngreso.txtFechaHasta.value)==false){
		alert("La fecha inicial no debe ser mayor a la fecha final!")
	    return false;
	}

	/*if (ValidateRangeDate(document.frmIngreso.txtFechaActual.value,document.frmIngreso.txtFechaDesde.value)==false){
		alert("La fecha actual no debe ser mayor a la fecha inicial!")
	    return false;
	}*/	
		    	
	if (document.frmIngreso.txtPuestos.value == "")
		{
		alert("El campo Cantidad de Puestos es obligatorio");
		return false;
		} 
	else
		{
			if (isNaN(document.frmIngreso.txtPuestos.value) == true)
				{
					alert("La Cantidad de Puestos debe ser num�rica")
					return false;
				}
			else
			{
			    if (parseInt(document.frmIngreso.txtPuestos.value) < parseInt(document.frmIngreso.txtPuestosBD.value))
	            {
	                alert("Solo puede aumentar la cantidad de puestos");
	                return false;
	            }
			}
		}
		
	if (document.frmIngreso.cmbLunesInicio.value == "")
		{
		alert("El campo Lunes Hora Inicio es obligatorio");
		return false;
		} 
	
	if (document.frmIngreso.cmbLunesFin.value == "")
		{
		alert("El campo Lunes Hora Fin es obligatorio");
		return false;
		} 
	
	if (document.frmIngreso.cmbMartesInicio.value == "")
		{
		alert("El campo Martes Hora Inicio es obligatorio");
		return false;
		} 
	
	if (document.frmIngreso.cmbMartesFin.value == "")
		{
		alert("El campo Martes Hora Fin es obligatorio");
		return false;
		} 
	
	if (document.frmIngreso.cmbMiercolesInicio.value == "")
		{
		alert("El campo Miercoles Hora Inicio es obligatorio");
		return false;
		} 
	
	if (document.frmIngreso.cmbMiercolesFin.value == "")
		{
		alert("El campo Miercoles Hora Fin es obligatorio");
		return false;
		} 
	
	if (document.frmIngreso.cmbJuevesInicio.value == "")
		{
		alert("El campo Jueves Hora Inicio es obligatorio");
		return false;
		} 
	
	if (document.frmIngreso.cmbJuevesFin.value == "")
		{
		alert("El campo Jueves Hora Fin es obligatorio");
		return false;
		} 
	
	if (document.frmIngreso.cmbViernesInicio.value == "")
		{
		alert("El campo Viernes Hora Inicio es obligatorio");
		return false;
		} 
	
	if (document.frmIngreso.cmbViernesFin.value == "")
		{
		alert("El campo Viernes Hora Fin es obligatorio");
		return false;
		} 
	
	if (document.frmIngreso.cmbSabadoInicio.value == "")
		{
		alert("El campo S�bado Hora Inicio es obligatorio");
		return false;
		} 
	
	if (document.frmIngreso.cmbSabadoFin.value == "")
		{
		alert("El campo S�bado Hora Fin es obligatorio");
		return false;
		} 
	
	if (document.frmIngreso.cmbDomingoInicio.value == "")
		{
		alert("El campo Domingo Hora Inicio es obligatorio");
		return false;
		} 
	
	if (document.frmIngreso.cmbDomingoFin.value == "")
		{
		alert("El campo Domingo Hora Fin es obligatorio");
		return false;
		} 
		
	IngresoHorario = 0
	
    if (parseInt(document.frmIngreso.cmbLunesInicio.value) > parseInt(document.frmIngreso.cmbLunesFin.value))
    {
        alert("El horario inicial de atenci�n debe ser menor al final");
        return false;
    }else
    {
        if (parseInt(document.frmIngreso.cmbLunesInicio.value) < parseInt(document.frmIngreso.cmbLunesFin.value))
        {
            IngresoHorario = 1
        }
    }

    if (parseInt(document.frmIngreso.cmbMartesInicio.value) > parseInt(document.frmIngreso.cmbMartesFin.value))
    {
        alert("El horario inicial de atenci�n debe ser menor al final");
        return false;
    }else
    {
        if (parseInt(document.frmIngreso.cmbMartesInicio.value) < parseInt(document.frmIngreso.cmbMartesFin.value))
        {
            IngresoHorario = 1
        }        
    }    

    if (parseInt(document.frmIngreso.cmbMiercolesInicio.value) > parseInt(document.frmIngreso.cmbMiercolesFin.value))
    {
        alert("El horario inicial de atenci�n debe ser menor al final");
        return false;
    }else
    {
        if (parseInt(document.frmIngreso.cmbMiercolesInicio.value) < parseInt(document.frmIngreso.cmbMiercolesFin.value))
        {
            IngresoHorario = 1
        }  
    }
        
    if (parseInt(document.frmIngreso.cmbJuevesInicio.value) > parseInt(document.frmIngreso.cmbJuevesFin.value))
    {
        alert("El horario inicial de atenci�n debe ser menor al final");
        return false;
    }else
    {
        if (parseInt(document.frmIngreso.cmbJuevesInicio.value) < parseInt(document.frmIngreso.cmbJuevesFin.value))
        {
            IngresoHorario = 1
        }  
    }
    
    if (parseInt(document.frmIngreso.cmbViernesInicio.value) > parseInt(document.frmIngreso.cmbViernesFin.value))
    {
        alert("El horario inicial de atenci�n debe ser menor al final");
        return false;
    }else
    {
        if (parseInt(document.frmIngreso.cmbViernesInicio.value) < parseInt(document.frmIngreso.cmbViernesFin.value))
        {
            IngresoHorario = 1
        }         
    }

    if (parseInt(document.frmIngreso.cmbSabadoInicio.value) > parseInt(document.frmIngreso.cmbSabadoFin.value))
    {
        alert("El horario inicial de atenci�n debe ser menor al final");
        return false;
    }else
    {
        if (parseInt(document.frmIngreso.cmbSabadoInicio.value) < parseInt(document.frmIngreso.cmbSabadoFin.value))
        {
            IngresoHorario = 1
        }          
    }

    if (parseInt(document.frmIngreso.cmbDomingoInicio.value) > parseInt(document.frmIngreso.cmbDomingoFin.value))
    {
        alert("El horario inicial de atenci�n debe ser menor al final");
        return false;
    }else
    {
        if (parseInt(document.frmIngreso.cmbDomingoInicio.value) < parseInt(document.frmIngreso.cmbDomingoFin.value))
        {
            IngresoHorario = 1
        }           
    }	

    if (IngresoHorario == 0)
    {
        alert("Debe seleccionar el horario de atenci�n al menos para un d�a de la semana");
        return false;
    }
    
    	
	 return true;
}
</script>

<%
if (FaltaCarga <> "S") then
	if isobject(Session("ObjServiciosOE")) then
		set ObjServicios = Session("ObjServiciosOE")
		
		ObjServicios.RemoveAll()
		set Session("ObjServiciosOE")  = ObjServicios
	end if
	
	if isobject(Session("ObjFeriados")) then
		set ObjPerfiles = Session("ObjFeriados")
	
		ObjPerfiles.RemoveAll()
		set Session("ObjFeriados")  = ObjPerfiles
	end if
end if
 
    '''vany 10042007

    sRol = request("Rol")
    sCodImp = request("CodImp") '211043 '211044 '211038 '
    sCodSede = request("CodSede") '138644 '138645 '138638 '
    
    if sRol = "" then 
        sRol = 3
    end if 
    
    if sCodImp = "" then
        sCodImp = 0
    end if 

    if sCodSede = "" then   
        sCodSede = 0 
    end if 
    
    Session("IngresoServiciosOE") = false
    Session("IngresoFeriados") = false


set SpOE = server.CreateObject("adodb.command")

	with SpOE
		.activeconnection = cc
		.Commandtext = "ObtenerDatosOficina"
		.commandtype = adCmdStoredProc
	    For Each Parameter In .Parameters
			If Parameter.Direction And adParamInput Then
				Select case ucase(Mid(Parameter.Name, 2))
                    case "IDEMPRESA"
                        Parameter.value = sCodImp
                    case "IDSEDE"
                        Parameter.value = sCodSede
           			case else
				end select
			End If
		Next
		
		'--------------------------------------------
		' Ejecuta
		'--------------------------------------------

		.Execute XXX, , adexecutenorecords

		'--------------------------------------------
		' Devuelve parametros
		'--------------------------------------------
		
		For Each Parameter In .Parameters
			If Parameter.Direction And (adParamOutput Or adParamReturnValue) Then
				Select case ucase(Mid(Parameter.Name, 2))
					case "STATUS"
					    Status = Parameter.value
					case "MENSAJE"
					    Mensaje = Parameter.value
					case "TIPOOFICINA"
					    sTipoOficina = Parameter.value
					case "DESCRIPCIONSEDE"
					    sOE = Parameter.value					
					case "EMAILOFICINA"
					    sEmail = Parameter.value
					case "DIRECCION"
					    sDireccion = Parameter.value					
					case "MUNICIPIO"
					    sMunicipio = Parameter.value					
					case "CODIGOPOSTAL"
					    sCodPost = Parameter.value					
					case "PROVINCIA"
					    sProvincia = Parameter.value
					case "TELEFONOCONTACTO"
					    sTelContacto = Parameter.value
					case "TELEFONOALTERNATIVO"
					    sTelAlternativo = Parameter.value
					case "ESSUPERVISORA"
					    sEsSupervisora = Parameter.value
					case "IDOFICINAPADRE"
					    sOficinaPadre = Parameter.value
					case "FECHADESDE"
					    sFechaDesde = Parameter.value
					case "FECHAHASTA"
					    sFechaHasta = Parameter.value
					case "CANTIDADDEPUESTOS"
					    sPuestos = Parameter.value
					    sPuestosBD = Parameter.value
					case "LUHI"
					    if isnull(Parameter.value) then 
					    	sLunesI = "1260" '1260
					        sLunesInicio = "07:00"
					    else
					        sLunesI = Parameter.value
					        sLunesInicio = C.ConvertirMinutosAHorario(Parameter.value)
					    end if
					case "LUHF"
						if isnull(Parameter.value) then 
					    	sLunesF = "420" '1260
					        sLunesFin = "07:00"
					    else
					        sLunesF = Parameter.value
					        sLunesFin = C.ConvertirMinutosAHorario(Parameter.value)
					    end if
					case "MAHI"
						if isnull(Parameter.value) then 
					    	sMartesI = "1260" '1260
					        sMartesInicio = "07:00"
					    else
					        sMartesI = Parameter.value
					        sMartesInicio = C.ConvertirMinutosAHorario(Parameter.value)
					    end if
					case "MAHF"
						if isnull(Parameter.value) then 
					    	sMartesF = "420" '1260
					        sMartesFin = "07:00"
					    else
					        sMartesF = Parameter.value
					        sMartesFin = C.ConvertirMinutosAHorario(Parameter.value)
					    end if
					case "MIHI"
						if isnull(Parameter.value) then 
					    	sMiercolesI = "1260" '1260
					        sMiercolesInicio = "07:00"
					    else				
					        sMiercolesI = Parameter.value
					        sMiercolesInicio = C.ConvertirMinutosAHorario(Parameter.value)
					    end if
					case "MIHF"
						if isnull(Parameter.value) then 
					    	sMiercolesF = "420" '1260
					        sMiercolesFin = "07:00"
					    else		
					        sMiercolesF = Parameter.value
					        sMiercolesFin = C.ConvertirMinutosAHorario(Parameter.value)
					    end if
					case "JUHI"
						if isnull(Parameter.value) then 
					    	sJuevesI = "1260" '1260
					        sJuevesInicio = "07:00"
					    else	
					        sJuevesI = Parameter.value
					        sJuevesInicio = C.ConvertirMinutosAHorario(Parameter.value)
					    end if
					case "JUHF"
						if isnull(Parameter.value) then 
					    	sJuevesF = "420" '1260
					        sJuevesFin = "07:00"
					    else	
					        sJuevesF = Parameter.value
					        sJuevesFin = C.ConvertirMinutosAHorario(Parameter.value)
					    end if
					case "VIHI"
						if isnull(Parameter.value) then 
					    	sViernesI = "1260" '1260
					        sViernesInicio = "07:00"
					    else	
					        sViernesI = Parameter.value
					        sViernesInicio = C.ConvertirMinutosAHorario(Parameter.value)
					    end if
					case "VIHF"
						if isnull(Parameter.value) then 
					    	sViernesF = "420" '1260
					        sViernesFin = "07:00"
					    else	
					        sViernesF = Parameter.value
					        sViernesFin = C.ConvertirMinutosAHorario(Parameter.value)
					    end if
					case "SAHI"
						if isnull(Parameter.value) then 
					    	sSabadoI = "1260" '1260
					        sSabadoInicio = "07:00"
					    else	
					        sSabadoI = Parameter.value
					        sSabadoInicio = C.ConvertirMinutosAHorario(Parameter.value)
					    end if
					case "SAHF"
						if isnull(Parameter.value) then 
					    	sSabadoF = "420" '1260
					        sSabadoFin = "07:00"
					    else	
					        sSabadoF = Parameter.value
					        sSabadoFin = C.ConvertirMinutosAHorario(Parameter.value)
					    end if
					case "DOHI"
						if isnull(Parameter.value) then 
					    	sDomingoI = "1260" '1260
					        sDomingoInicio = "07:00"
					    else	
					        sDomingoI = Parameter.value
					        sDomingoInicio = C.ConvertirMinutosAHorario(Parameter.value)
					    end if
					case "DOHF"
						if isnull(Parameter.value) then 
					    	sDomingoF = "420" '1260
					        sDomingoFin = "07:00"
					    else	
					        sDomingoF = Parameter.value
					        sDomingoFin = C.ConvertirMinutosAHorario(Parameter.value)
					    end if
					case else
				end select
			end if
		next
	end with    
       
    ''Genero las franjas disponibles para agrandar el horario de antencion unicamente. 
    ''Segun los datos ingresados con anterioridad.
    'response.Write slunesi
    FranjaLunesInicio = "420," & sLunesI
    FranjaLunesFin = sLunesF & ",1260"
  
    FranjaMartesInicio = "420," & sMartesI
    FranjaMartesFin = sMartesF & ",1260"
    
    FranjaMiercolesInicio = "420," & sMiercolesI
    FranjaMiercolesFin = sMiercolesF & ",1260"
    
    FranjaJuevesInicio = "420," & sJuevesI
    FranjaJuevesFin = sJuevesF & ",1260"

    FranjaViernesInicio = "420," & sViernesI
    FranjaViernesFin = sViernesF & ",1260"

    FranjaSabadoInicio = "420," & sSabadoI
    FranjaSabadoFin = sSabadoF & ",1260"

    FranjaDomingoInicio = "420," & sDomingoI
    FranjaDomingoFin = sDomingoF & ",1260"

    
         
    '****Datos Basicos****' 
    if sOE = "" then
        sOE = request("txtOE")
    end if
    if sEmail = "" then
        sEmail = request("txtEmail")
    end if
    if sDireccion = "" then
        sDireccion = request("txtDireccion")
    end if
    if sTelContacto = "" then
        sTelContacto = request("txtTelContacto")
    end if
    if sTelAlternativo = "" then
        sTelAlternativo = request("txtTelAlternativo")
    end if 
    if sProvincia = "" then
        sProvincia = request("cmbProvincia")
    end if 
    if sMunicipio = "" then
        sMunicipio = request("txtMunicipioHid")
    end if
    if sCodPost = "" then
        sCodPost = request("txtCPNH")
    end if 
    if sTipoOficina = "" then
        sTipoOficina = request("cmbTipoOficina")
    end if
 
    '****Supervisora****'
    if sEsSupervisora = "" then
        sEsSupervisora = request("optEsSupervisora")
    end if
    if sOficinaPadre = "" then 
        sOficinaPadre = request("hidOficinaPadre")
    end if 
    
    'response.Write sEsSupervisora
    if sEsSupervisora = "S" then
        sOficinaPadre = 0
    end if
    
    'response.Write sEsSupervisora
    
    '****Puestos****'
    if sFechaDesde = "" or isnull(sFechaDesde) then
        sFechaDesde = request("txtFechaDesde")    
    else
        sFechaDesde = cdate(sFechaDesde)
        
        dia = day(sFechaDesde)
        mes = month(sFechaDesde)
                
        select case dia
            case 1,2,3,4,5,6,7,8,9
                dia = "0" & dia
            case else
                dia = dia
        end select 
        
        select case mes
            case 1,2,3,4,5,6,7,8,9
                mes = "0" & mes
            case else
                mes = mes
        end select 

        anio = year(sFechaDesde)
        
        sFechaDesde = cstr( dia & "/" & mes & "/" & anio)
    end if
    
    if sFechaHasta = "" or isnull(sFechaHasta) then 
        sFechaHasta = request("txtFechaHasta")
    else
        sFechaHasta = cdate(sFechaHasta)

        dia = day(sFechaHasta)
        mes = month(sFechaHasta)
                
        select case dia
            case 1,2,3,4,5,6,7,8,9
                dia = "0" & dia
            case else
                dia = dia
        end select 
        
        select case mes
            case 1,2,3,4,5,6,7,8,9
                mes = "0" & mes
            case else
                mes = mes
        end select 

        anio = year(sFechaHasta)
                
        sFechaHasta = cstr( dia & "/" & mes & "/" & anio)
    end if
     
    if sPuestos = "" then 
        sPuestos = request("txtPuestos")
    end if 
    
    '****Configuracion de Agenda****' 
    if sLunesIncicio = "" and sLunesFin = "" then  
        sLunesInicio = request("cmbLunesInicio")
        sLunesFin = request("cmbLunesFin")
    end if 
    
    if sMartesInicio = "" and sMartesFin = "" then 
        sMartesInicio = request("cmbMartesInicio")
        sMartesFin = request("cmbMartesFin")
    end if 
    
    if sMiercolesInicio = "" and sMiercolesFin = "" then
        sMiercolesInicio = request("cmbMiercolesInicio")
        sMiercolesFin = request("cmbMiercolesFin")
    end if 
    
    if sJuevesInicio= "" and sJuevesFin= "" then 
        sJuevesInicio = request("cmbJuevesInicio")
        sJuevesFin = request("cmbJuevesFin")
    end if
    
    if sViernesInicio = "" and sViernesFin = "" then 
        sViernesInicio = request("cmbViernesInicio")
        sViernesFin = request("cmbViernesFin")
    end if 
    
    if sSabadoInicio = "" and sSabadoFin = "" then 
        sSabadoInicio = request("cmbSabadoInicio")
        sSabadoFin = request("cmbSabadoFin")               
    end if 
    
    if sDomingoInicio = "" and sDomingoFin = "" then 
        sDomingoInicio = request("cmbDomingoInicio")
        sDomingoFin = request("cmbDomingoFin")
    end if    
      
%>
<br />
<br />

<%FechaActual = ConvDateToString(Date())%>
<form name="frmIngreso" action="OFI_CnfModOficina.asp" method="post" onsubmit="return ValidacionFinal();">
<input type="hidden" value="<%=FechaActual%>" name="txtFechaActual" />
<table width="500" border="0" class="tbltext1">

    <tr>
        <td colspan="2">
            <b>Ingrese a continuaci�n los datos correspondientes a la OE que desea crear.</b><br /><br />
            <b>
            Los siguientes datos no podr�n ser modificados o contendr�n validaciones necesarias para evitar 
            superponer fechas y horarios de atenci�n con las citas activas.</b>
            <ul>
                <li>Departamento</li>
                <li>Municipio</li>
                <li>C.P.</li>
                <li>Tipo de Oficina</li>
                <li>Prestaciones de la Oficina</li>
                <li>La oficina es Supervisora?</li>
                <li>La OE depende de la siguiente OE</li>
                <li>Cantidad de Puestos</li>
                <li>Franja Horaria para Entrevistas</li>
                <li>Feriados de la Oficina</li>
            </ul>
            
            <input type="hidden" value="<%=sRol%>" name="Rol" />
            <input type="hidden" value="<%=sCodImp%>" name="CodImp" />
            <input type="hidden" value="<%=sCodSede%>" name="CodSede" />
            <br />
        </td>
    </tr>
    <tr>
        <td colspan="2"><hr /></td>
    </tr>
    <tr>
        <td colspan="2"><b><u>Datos B�sicos de la Oficina</u></b><br /><br /></td>
    </tr>    
    <tr>
        <td width='40%'><b>Nombre de La OE *</b></td>
        <td width='60%'><input type="text" name="txtOE" value="<%=sOE%>" size="50" maxlength="100" class="tbltext1"/></td>
    </tr>
    <tr>
        <td><b>E-mail de la OE *</b></td>
        <td><input type="text" name="txtEmail" value="<%=sEmail%>" size="50" maxlength="100" class="tbltext1"/></td>
    </tr>
    <tr>
        <td><b>Direcci�n *</b></td>
        <td><input type="text" name="txtDireccion" value="<%=sDireccion%>" size="50" maxlength="100" class="tbltext1"/></td>
    </tr>
    <tr>
        <td><b>Departamento *</b></td>
        <td>
            <%'sInt = "PROV|0|" & date & "|" & sProvincia & "|cmbProvincia' class='tbltext1' onchange='CambiarDatos()|ORDER BY DESCRIZIONE"%>			
		    <%'CreateCombo(sInt)%>   
		    <input type="hidden" name="cmbProvincia" value="<%=sProvincia%>"/>
		    <input type="Text" style="TEXT-TRANSFORM: uppercase; WIDTH: 270px" name="txtProvincia" value="<%=ObtieneDescripcion(sProvincia,"PROVINCIA")%>" readonly class="tbltext1"/>
	    </td>
    </tr>
    <tr>
		<td>
			<b>Municipio *</b>
		</td>
		<td>
			<input type="text" name="txtMunicipio" style="TEXT-TRANSFORM: uppercase; WIDTH: 270px" class="tbltext1" size="10" readonly value="<%=ObtieneDescripcion(sMunicipio,"MUNICIPIO")%>">
			<input type="hidden" id="txtMunicipioHid" name="txtMunicipioHid" value="<%=sMunicipio%>">

<%
				NomeForm="frmIngreso"
				CodiceProvincia="cmbProvincia"
				NomeComune="txtMunicipio"
				CodiceComune="txtMunicipioHid"
				Cap="txtCP"
%>
			<!--<a href="Javascript:SelComune('<%=NomeForm%>','<%=CodiceProvincia%>','<%=NomeComune%>','<%=CodiceComune%>','<%=Cap%>')" ID="imgPunto1" name="imgPunto1" onmouseover="javascript:window.status='';return true">
			
			<img border="0" src="<%=Session("Progetto")%>/images/bullet1.gif"></a>-->				
		</td>
	</tr>
	<tr>
	    <td>
	        <b>C.P. </b>
	    </td>
	    <td>
	        <input type="hidden" name="txtCPNH" value="<%=ObtieneDescripcion(sMunicipio,"CODIGOPOSTAL")%>">
			<input type="text" class="tbltext1" name="txtCP" readonly value="<%=ObtieneDescripcion(sMunicipio,"CODIGOPOSTAL")%>" size="10" maxlength="8">
	    </td>
	</tr>
    <tr>
        <td><b>Tel�fono de Contacto: *</b></td>
        <td><input type="text" name="txtTelContacto" value="<%=sTelContacto%>" size="20" maxlength="100" class="tbltext1"/></td>
    </tr>
    <tr>
        <td><b>Tel�fono Alternativo: </b></td>
        <td><input type="text" name="txtTelAlternativo" value="<%=sTelAlternativo%>" size="20" maxlength="100" class="tbltext1"/></td>
    </tr>
    <tr>
        <td><b>Tipo de Oficina:</b></td>
        <td>
        <% 
        'sqlTipoOficina = "select codice, descrizione from tades where nome_tabella = 'FGIUR' and valore='03'"
        'CreateComboDesdeSQL sqlTipoOficina,"cmbTipoOficina","tbltext1",sTipoOficina,"","270",cc
        %> 
        <input type="hidden" name="cmbTipoOficina" value="<%=sTipoOficina%>"/>
        <input type="Text" style="TEXT-TRANSFORM: uppercase; WIDTH: 270px" name="txtTipoOficina" value="<%=ObtieneDescripcion(sTipoOficina,"TIPOOFICINA")%>" readonly class="tbltext1"/>
        </td>
    </tr>   
    
    <tr>
        <td colspan="2"><hr /></td>
    </tr>
            <% 
        	sql = "Select distinct irst.servicio, irst.areagrupo, irst.descripcion from IMPRESA_ROLESServicios irs, impresa_rolesServiciosTipos irst where irs.ID_IMPRESA = " & sCodImp & "AND irs.rol = " & sRol
	        sql = sql & " and irs.rol = irst.rol and irs.servicio = irst.servicio"

            set rsServiciosOE = server.CreateObject("adodb.recordset")   
	        set rsServiciosOE = CC.execute(sql)  
	        if not rsserviciosoe.EOF then 
	            Id = ""
	            do while not rsserviciosoe.EOF
	                if cstr(rsServiciosOE("Servicio")) = "11" then 
	                    Id = rsServiciosOE("Servicio")
	                end if
	                rsserviciosoe.MoveNext 
	            loop
	        end if 
	        %>       
    <tr>
        <td colspan="2"><br />
            <%if cstr(Id) <> "11" then %>
            <a class="textred" href="javascript:AbrirForm(<%=sCodImp%>,<%=sRol%>,'Servicios');">
                <b>Prestaciones de la Oficina &nbsp;&nbsp;&nbsp;&nbsp;>></b>
            </a>
            <%else%>
            <b><u>Prestaciones de la Oficina</u></b><br />
            <%
            
            response.Write "<ul>"
            rsserviciosoe.MoveFirst
            do while not rsserviciosoe.EOF 
                response.Write "<li>" & rsserviciosoe("descripcion") & "</li>"
                rsserviciosoe.MoveNext 
            loop
            response.Write "</ul>"
            set rsServiciosOE = nothing
            end if
            %>   
        </td>
    </tr> 
 
    <tr>
        <td colspan="2"><hr /></td>
    </tr>
       
    <tr>
        <td><br /><b>La oficina es Supervisora? *</b></td>
        <td>
        <b>SI</b>
        <%
          if sEsSupervisora = "S" then 
            SeleccionadoS = "Checked"
            SeleccionadoN = ""
          else
            SeleccionadoS = ""
            SeleccionadoN = "Checked"
          end if 
          
          if SeleccionadoN <> "" then 
             sDescOficinaPadre = ObtieneDescripcion(sOficinaPadre,"OFICINA")
          %>
            <input type="hidden" name="codofipadreopcional" value="<%=sOficinaPadre%>" />
            <input type="hidden" name="descofipadreopcional" value="<%=sDescOficinaPadre%>" />
                        
            <img src="/BA/images/footer.gif" width="1" height="1" onload="MuestroDependencia()"></img>
          <% 
          else
          %>
            <input type="hidden" name="hidOficinaPadre" value="<%=sOficinaPadre%>" />
          <%
          end if
          %>
        <input  type="radio" disabled name="optEsSupervisoraDis" id="optEsSupervisora" value="S" class="tbltext1" <%=SeleccionadoS%> onclick="OcultoDependencia()"/>
        <b>NO</b>
        <input  type="radio" disabled name="optEsSupervisoraDis" id="optEsSupervisora" value="N" class="tbltext1" <%=SeleccionadoN%> onclick="MuestroDependencia()"/>
        <input type="hidden" name="optEsSupervisora" value="<%=sEsSupervisora%>" />
        </td>
    </tr>
</table>

 <label name="lblDepende" id="lblDepende">
 </label>  
 
 <table width="500" class="tbltext1">
    <tr>
        <td colspan="2"><hr /></td>
    </tr>
    <tr>
        <td colspan="2"><b><u>Puestos</u></b><br /><br /></td>
    </tr>
    <tr>
       <td width="40%">
            <b>Fecha Desde</b>
       </td>
       <td width="60%"> 
            <b><input type="text" name="txtFechaDesde" value="<%=sFechaDesde%>" class="tbltext1" size="15" maxlength="10"/></b>
       </td>
    </tr>     
    <tr>
       <td>
            <b>Fecha Hasta</b>
       </td>
       <td>
            <b><input type="text" name="txtFechaHasta" value="<%=sFechaHasta%>" class="tbltext1" size="15" maxlength="10"/></b>
       </td>
    </tr> 
    <tr>
       <td>
            <b>Cantidad de Puestos</b>
       </td>
       <td>
            <b><input type="text" name="txtPuestos" value="<%=sPuestos%>" class="tbltext1" size="4" maxlength="3"/></b>
             <input type="hidden" name="txtPuestosBD" value="<%=sPuestosBD%>"/>

       </td>   
    </tr>        
    <tr>
        <td colspan="2"><hr /></td>
    </tr>
    <tr>
        <td colspan="2"><b><u>Franja Horaria para Entrevistas</u></b><br /><br /></td>
    </tr>
    <tr>
       <td>
            <b>Lunes</b>&nbsp;&nbsp;&nbsp;&nbsp;
       </td>
       <td>
            <b>H. Inicial</b>&nbsp;&nbsp;<%=C.CrearComboAgEnda("cmbLunesInicio","tbltext1","","HORAS",FranjaLunesInicio,sLunesInicio,61)%>
            &nbsp;&nbsp;&nbsp;&nbsp;<b>H. Final</b>&nbsp;&nbsp;<%=C.CrearComboAgEnda("cmbLunesFin","tbltext1","","HORAS",FranjaLunesFin,sLunesFin,61)%>
       </td>
    </tr>    

    <tr>
       <td>
            <b>Martes</b>&nbsp;&nbsp;&nbsp;&nbsp;
       </td>
       <td>
            <b>H. Inicial</b>&nbsp;&nbsp;<%=C.CrearComboAgEnda("cmbMartesInicio","tbltext1","","HORAS",FranjaMartesInicio,sMartesInicio,61)%>
            &nbsp;&nbsp;&nbsp;&nbsp;<b>H. Final</b>&nbsp;&nbsp;<%=C.CrearComboAgEnda("cmbMartesFin","tbltext1","","HORAS",FranjaMartesFin,sMartesFin,61)%>
       </td>
    </tr>

    <tr>
       <td>
            <b>Mi�rcoles</b>&nbsp;&nbsp;&nbsp;&nbsp;
       </td>
       <td>
            <b>H. Inicial</b>&nbsp;&nbsp;<%=C.CrearComboAgEnda("cmbMiercolesInicio","tbltext1","","HORAS",FranjaMiercolesInicio,sMiercolesInicio,61)%>
            &nbsp;&nbsp;&nbsp;&nbsp;<b>H. Final</b>&nbsp;&nbsp;<%=C.CrearComboAgEnda("cmbMiercolesFin","tbltext1","","HORAS",FranjaMiercolesFin,sMiercolesFin,61)%>
       </td>
    </tr> 
    
    <tr>
       <td>
            <b>Jueves</b>&nbsp;&nbsp;&nbsp;&nbsp;
       </td>
       <td>
            <b>H. Inicial</b>&nbsp;&nbsp;<%=C.CrearComboAgEnda("cmbJuevesInicio","tbltext1","","HORAS",FranjaJuevesInicio,sJuevesInicio,61)%>
            &nbsp;&nbsp;&nbsp;&nbsp;<b>H. Final</b>&nbsp;&nbsp;<%=C.CrearComboAgEnda("cmbJuevesFin","tbltext1","","HORAS",FranjaJuevesFin,sJuevesFin,61)%>
       </td>
    </tr>           
 
    <tr>
       <td>
            <b>Viernes</b>&nbsp;&nbsp;&nbsp;&nbsp;
       </td>
       <td>
            <b>H. Inicial</b>&nbsp;&nbsp;<%=C.CrearComboAgEnda("cmbViernesInicio","tbltext1","","HORAS",FranjaViernesInicio,sViernesInicio,61)%>
            &nbsp;&nbsp;&nbsp;&nbsp;<b>H. Final</b>&nbsp;&nbsp;<%=C.CrearComboAgEnda("cmbViernesFin","tbltext1","","HORAS",FranjaViernesFin,sViernesFin,61)%>
       </td>
    </tr> 
  
    <tr>
       <td>
            <b>S�bado</b>&nbsp;&nbsp;&nbsp;&nbsp;
       </td>
       <td>
            <b>H. Inicial</b>&nbsp;&nbsp;<%=C.CrearComboAgEnda("cmbSabadoInicio","tbltext1","","HORAS",FranjaSabadoInicio,sSabadoInicio,61)%>
            &nbsp;&nbsp;&nbsp;&nbsp;<b>H. Final</b>&nbsp;&nbsp;<%=C.CrearComboAgEnda("cmbSabadoFin","tbltext1","","HORAS",FranjaSabadoFin,sSabadoFin,61)%>
       </td>
    </tr> 
    
    <tr>
       <td>
            <b>Domingo</b>&nbsp;&nbsp;&nbsp;&nbsp;
       </td>
       <td>
            <b>H. Inicial</b>&nbsp;&nbsp;<%=C.CrearComboAgEnda("cmbDomingoInicio","tbltext1","","HORAS",FranjaDomingoInicio,sDomingoInicio,61)%>
            &nbsp;&nbsp;&nbsp;&nbsp;<b>H. Final</b>&nbsp;&nbsp;<%=C.CrearComboAgEnda("cmbDomingoFin","tbltext1","","HORAS",FranjaDomingoFin,sDomingoFin,61)%>
       </td>
    </tr> 

    <tr>
        <td colspan="2"><hr /></td>
    </tr>    
    
    <tr>
        <td colspan="2"><br />
            <a class="textred" href="javascript:AbrirForm(<%=sCodSede%>,<%=sRol%>,'Feriados');">
                <b>Feriados de la Oficina &nbsp;&nbsp;&nbsp;&nbsp;>></b>
            </a>   
        </td>
    </tr>            

    <tr>
        <td colspan="2"><hr /></td>
    </tr>
    
     <tr>
        <td colspan="2" align="right"><br />
            <input type="submit" name="Enviar" value="Enviar >>" class="my"/>
        </td>
    </tr>     
    
 </table>

 </form>  

<%
public function ObtieneDescripcion(Id,Tipo)
    set rsobtiene = server.CreateObject("adodb.recordset")
    
    select case ucase(Tipo)
        case "OFICINA"
            consultaobtienedescripcion = "select descrizione from sede_impresa where id_sede = " & Id
        case "MUNICIPIO"
            consultaobtienedescripcion = "select descom from comune where codcom = " & Id
        case "CODIGOPOSTAL"
            consultaobtienedescripcion = "select cap from comune where codcom = " & Id
        case "PROVINCIA"
            consultaobtienedescripcion = "select descrizione from tades where nome_tabella = 'PROV' and codice = " & Id
        case "TIPOOFICINA"
            consultaobtienedescripcion = "select descrizione from tades where nome_tabella = 'FGIUR' and valore='03' and codice = " & Id
        case else
   
    end select
    
    set rsobtiene = cc.execute(consultaobtienedescripcion)
    
    if not rsobtiene.EOF then
        rsobtiene.MoveFirst
        ObtieneDescripcion = rsobtiene.fields(0)
    end if 
    
    set rsobtiene = nothing    
end function 

%>






















    
		    
    <!--<tr>
        <td><b>Departamento:</b></td>
        <td> 
        <% 
        sDepartamento = request("cmbDepartamento")
        sqlDepartamento = "select distinct coddept,descripcion from Departamento where provincia = '" & sProvincia & "'"
        CreateComboDesdeSQL sqlDepartamento,"cmbDepartamento","tbltext1",sDepartamento,"onchange=CambiarDatos()","",cc   
        %>
        </td>
    </tr>
    <tr>
        <td><b>Localidad:</b></td>
        <td>
        <% 
        sLocalidad = request("cmbLocalidad")
        sqlLocalidad = "select distinct codloc,descripcion from Localidad where provincia = '" & sProvincia & "' and depto ='" & sDepartamento & "'"
        CreateComboDesdeSQL sqlLocalidad,"cmbLocalidad","tbltext1",sLocalidad,"onchange=CambiarDatos()","",cc   
        %>                 
        </td>
    </tr>
    <tr>
        <td><b>La oficina participa del seguro? *</b></td>
        <td>
        <b>SI</b><input type="radio" name="optParticipaSeguro" value="S" class="tbltext1"/>
        <b>NO</b><input type="radio" name="optParticipaSeguro" value="N" class="tbltext1"/>
        </td>
    </tr>
    <tr>
        <td><b>La oficina es una GECAL? *</b></td>
        <td>
        <b>SI</b><input type="radio" name="optEsGecal" value="S" class="tbltext1"/>
        <b>NO</b><input type="radio" name="optEsGecal" value="N" class="tbltext1"/>
        </td>
    </tr>
    <tr>
        <td><b>La oficina es una ONG? *</b></td>
        <td>
        <b>SI</b><input type="radio" name="optEsOng" value="S" class="tbltext1"/>
        <b>NO</b><input type="radio" name="optEsOng" value="N" class="tbltext1"/>
        </td>
    </tr>-->
   
<!--#include virtual="/strutt_coda2.asp"-->
<%
Set C = Nothing
%>

