<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #INCLUDE FILE="../Utils.asp" -->
<%

Set Conn = server.CreateObject ("ADODB.Connection")
Conn.open strConn


Sql = ""
Sql = Sql & "SELECT ID_PERSONA, NON_VOGLIO_FARE, NON_POSSO_FARE, DT_ELAB "
Sql = Sql & "FROM PERS_VINCOLI "
Sql = Sql & "WHERE ID_PERSONA = " & IDP

'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
Set Rs = Conn.Execute (Sql)
if not rs.eof then VV = Rs.getrows()
Conn.Close
Set Conn = Nothing
%>
<html>

<head>
		<LINK REL=STYLESHEET TYPE="text/css" HREF="../fogliostile.css">
		<title>E-job - Orientamento</title>
</head>

<body bgcolor="#FFFFFF" style="font-family: Verdana; font-size: 10pt">
<table border=0 cellspacing=1 cellpadding=0 width='570'>
<tr><td background='../imgs/sfmnsmall.jpg' align=center><B>I miei Vincoli </B></td></tr>
<tr>
<td align=center><br>
<form name='Vincoli' Action='SalvaVincoli.asp' Method='POST'>
<input type='hidden' Name='DT_ELAB' Value='<%= MettiVal(VV,IDP,0,3)%>'>
<table border=0 cellspacing=1 cellpadding=0 style="font-family: Verdana; font-size: 10pt; color:'#ffffff'">
<tr>
	<td  bgcolor='#3163a0'><!img src='../imgs/angolosin.gif'><b>Che cosa non voglio assolutamente fare</b>
	<a href="javascript:Show_Help('../../help/orientamento/obiettivi/Obiettivi_Vincoli.htm')">
	<IMG src='../imgs/imghelp.gif' border=0 ></a><br></td>
</tr>
	<tr><td>
	<TEXTAREA Name="NON_VOGLIO_FARE" cols="55" rows="5"><%= MettiVal(VV,IDP,0,1)%></TEXTAREA>
	</td>
</tr>

<tr><td bgcolor='#f1f3f3'> &nbsp;</td></tr>
<tr>
	<td  bgcolor='#3163a0'><!img src='../imgs/angolosin.gif'><b>Che cosa non posso fare</b><br></td>
</tr>
	<tr><td>
	<TEXTAREA Name="NON_POSSO_FARE" cols="55" rows="5"><%= MettiVal(VV,IDP,0,2)%></TEXTAREA>
	</td>
</tr>
<tr>
<td align=right  bgcolor='#f1f3f3'><input type='Submit' value='Salva' class='My'></td>
</tr>
</table>
</form>
</td></tr>
</table>
<!-- #INCLUDE FILE="../Footer.asp" -->

</body>
</html>
