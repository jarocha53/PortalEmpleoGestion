<!-- #INCLUDE FILE="../Utils.asp" -->
<html>

<head>
	<LINK REL=STYLESHEET TYPE="text/css" HREF="../fogliostile.css">
	<title>E-job - Orientamento</title>
</head>

<body bgcolor="#ffffff" text="#000000">
<TABLE BORDER=0 WIDTH='570'>
<tr>
<TD bgcolor='#f1f1f3' align="middle">
<A href="javascript:Show_Help('../../help/orientamento/Obiettivi/Obiettivi.htm')">
<IMG src="../imgs/imghelp.gif" border=0 ></a>
</TD>
</tr>
<TR>
	<TD bgcolor='#f1f1f3'><font face='verdana' size='3'><br><FONT size=2>
	  Costruire il proprio futuro professionale richiede anche di mettere in chiaro quali siano limiti e disponibilitÓ 
	  soggettive e derivanti dal proprio ambiente (territoriale, familiare, ecc.). 
	  <br><br>Aver chiare le condizioni da cui partiamo Ŕ un buono spunto anche per valutare la possibilitÓ di modificarle, 
	  se possibile.<br><br>  
	  Seguendo le istruzioni delle schermate che seguono inserirai le informazioni sulle tue prioritÓ, 
	  le condizioni, gli obiettivi, i vincoli che delimitano orientano il tuo rapporto con la ricerca
	  di lavoro.<br></FONT><br></font></TD>
</TR>
</TABLE><!-- #INCLUDE FILE="../Footer.asp" -->
</body>

</html>
