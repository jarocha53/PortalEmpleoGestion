<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #INCLUDE FILE="../Utils.asp" -->
<%
Set Conn = server.CreateObject ("ADODB.Connection")
Conn.open strConn
%>
<html>

<head>
	<LINK REL=STYLESHEET TYPE="text/css" HREF="../fogliostile.css">
	<title>E-job - Orientamento</title>

<script language='Javascript'>
<!--
function valida(Obj)
{
for (x = 1; x<=3; x++)
{
	for (j = 1; j<=3; j++)
	{
		A = eval("Obj.Set" + x + ".options[Obj.Set" + x + ".selectedIndex].value")
		B = eval("Obj.Set" + j + ".options[Obj.Set" + j + ".selectedIndex].value")
		if ((A == B) && (x != j) && (B!= ""))
		{
			alert("Impossibile inserire due volte lo stesso Settore")
			return false
		}
		A = eval("Obj.Area" + x + ".options[Obj.Area" + x + ".selectedIndex].value")
		B = eval("Obj.Area" + j + ".options[Obj.Area" + j + ".selectedIndex].value")
		if ((A == B) && (x != j) && (B!= ""))
		{
			alert("Impossibile inserire due volte la stessa Area")
			return false
		}
	}
}
}
//-->
</script>
</head>
<body bgcolor="#FFFFFF" style="font-family: Verdana; font-size: 10pt">
<form name='Target' Action='SalvaTarget.asp' Method='POST' OnSubmit="return valida(this)">
<table border=0 cellspacing=1 cellpadding=0 width='570'>
<tr><td background='../imgs/sfmnsmall.jpg' align=center><B>I miei Obiettivi Professionali </B></td></tr>
<tr>
<td align=center><br>

	<table border=0 cellspacing=1 cellpadding=0 style="font-family: Verdana; font-size: 10pt; color='#ffffff'">
	<% if int(0 & session("idgruppo"))<>66 then 
		'Il gruppo 66 � quello degli utenti Occupati dell'abruzzo.
		'????? 
	response.write "<tr>"
		response.write "<td valign=top bgcolor='#3163a0'><!img src='../imgs/angolosin.gif'></td>"
		response.write "<td bgcolor='#3163a0'><b>Settore&nbsp;&nbsp;&nbsp;&nbsp;</b>"
		%>
		<a href="javascript:Show_Help('../../help/orientamento/obiettivi/Obiettivi_Prof.htm')">
		<%
		Response.Write "----- " & session("idgruppo")
		response.write "<IMG src='../imgs/imghelp.gif' border=0 ></a></td>"
	response.write "</tr>"
	response.write "<tr><td bgcolor='#f1f3f3' colspan=3><font color='#000000'>Indica al massimo tre settori nei quali vorresti lavorare</font></td></tr>"

	Sql = ""
	Sql = Sql & "SELECT ID_PERSONA, ID_SETTORE, DT_ELAB, ROWNUM "
	Sql = Sql & "FROM PERS_SETTORI "
	Sql = Sql & "WHERE ID_PERSONA = " & IDP & " AND "
	Sql = Sql & "ID_FASE = 2"
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
	Set Rs = Conn.Execute (Sql)
	if not rs.eof then VV = Rs.getrows()
'PL-SQL * T-SQL  
	Set Rs = Conn.Execute ("SELECT ID_SETTORE, DENOMINAZIONE FROM SETTORI")
	if not rs.eof then SETT = Rs.getrows()
		for i = 1 to 3
			Pos = Int("0" & MettiVal(VV,i,3,1))
			Response.Write "<tr>"
				Response.Write "<td bgcolor='#3163a0'> " & i & " </td>"
				Response.Write "<td bgcolor='#f1f3f3'>"
					if IsArray(SETT) then
						Response.Write "<select name='Set" & i & "'>"
						Response.Write "<option value=''></option>"
						for x = 0 to ubound(SETT,2)
							Response.write "<option "
								if Int("0" & SETT(0,x)) = Pos then Response.Write "	SELECTED "
							Response.write " value='" & SETT(0,x) & "'>" & left(SETT(1,x),60)
								if len(SETT(1,x)) > 60 then Response.Write "..."
							Response.Write "</option>"
						next
						Response.Write "</select>"
					end if
				Response.Write "</td>"
			Response.Write "</tr>"
		next
		Set VV = Nothing
	Set SETT = Nothing

	response.write "<tr>"
	response.write "<td colspan=2 height=18>&nbsp;</td>"
	response.write "</tr>"
end if
%>
<tr>
	<td valign=top bgcolor='#3163a0'><!img src='../imgs/angolosin.gif'></td>
	<td bgcolor='#3163a0'><b>Area Professionale</b></td>
</tr>
<tr><td bgcolor='#f1f3f3' colspan=3><font color='#000000'>Indica al massimo tre Aree Professionali nelle quali vorresti lavorare</font></td></tr>
<%
Sql = ""
Sql = Sql & "SELECT ID_PERSONA, ID_AREAPROF, DT_ELAB, ROWNUM "
Sql = Sql & "FROM PERS_AREAPROF "
Sql = Sql & "WHERE PERS_AREAPROF.ID_PERSONA = " & IDP & " AND "
Sql = Sql & "ID_FASE = 2"

'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
Set Rs = Conn.Execute (Sql)
if not rs.eof then VV2 = Rs.getrows()

'PL-SQL * T-SQL  
Set Rs = Conn.Execute ("SELECT ID_AREAPROF, DENOMINAZIONE FROM AREE_PROFESSIONALI")
if not rs.eof then AREEPROF = Rs.getrows()

Conn.Close
Set Conn = Nothing



for i = 1 to 3
Pos = Int("0" & MettiVal(VV2,i,3,1))
Response.Write "<tr>"
	Response.Write "<td bgcolor='#3163a0'> " & i & " </td>"
	Response.Write "<td bgcolor='#f1f3f3'>"
		if IsArray(AREEPROF) then
			Response.Write "<select name='Area" & i & "'>"
			Response.Write "<option value=''></option>"
			for x = 0 to ubound(AREEPROF,2)
				Response.Write "<option "
					if Int("0" & AREEPROF(0,x)) = Pos then Response.Write " SELECTED "
				Response.write " value='" & AREEPROF(0,x) & "'>" & left(AREEPROF(1,x),60)
					if len((AREEPROF(1,x))) > 60 then Response.Write "..."
				Response.Write "</option>"
			next
			Response.Write "</select>"
		end if

	Response.Write "</td>"
Response.Write "</tr>"
next
%>
<tr>
<td align=right colspan=2><input type='Submit' value='Salva' class='My'></td>
</tr>
</table>
</form>
</td></tr>
</table>
<!-- #INCLUDE FILE="../Footer.asp" -->
</body>
</html>
