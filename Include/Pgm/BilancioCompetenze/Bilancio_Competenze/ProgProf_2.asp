<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #include Virtual="/strutt_testa2.asp" -->
<!-- #INCLUDE FILE="../Utils.asp" -->


<%
Back = "../ProgettoProf/Dett_ProgettoProf_2.asp"

ID_FIGPROF = request("ID_FIGPROF")
ID_PROG_PROF = Request("ID_PROG_PROF")

Set Conn = server.CreateObject ("ADODB.Connection")
Conn.open strConn

	Sql = ""
	Sql = Sql & "SELECT "
	Sql = Sql & "COMPETENZE.DENOMINAZIONE, COMPETENZE.ID_COMPETENZA, COMPET_FP.GRADO_FP_COMP "
	Sql = Sql & "FROM "
	Sql = Sql & "COMPETENZE, COMPET_FP "
	Sql = Sql & "WHERE "
	Sql = Sql & "COMPETENZE.ID_COMPETENZA = COMPET_FP.ID_COMPETENZA "
	Sql = Sql & "AND "
	Sql = Sql & "COMPET_FP.ID_FIGPROF = " & ID_FIGPROF

'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
	Set Rs = Conn.Execute (Sql)
	if not rs.eof then COMPETENZE = Rs.getrows()

	Sql = ""
	Sql = Sql & "SELECT "
	Sql = Sql & "CONOSCENZE.DENOMINAZIONE, COMPET_CONOSC.ID_COMPETENZA, COMPET_CONOSC.GRADO_COMP_CON, "
	Sql = Sql & "	(SELECT PERS_CONOSC.COD_GRADO_CONOSC FROM PERS_CONOSC "
	Sql = Sql & "	WHERE "
	Sql = Sql & "	PERS_CONOSC.ID_CONOSCENZA = CONOSCENZE.ID_CONOSCENZA AND "
	Sql = Sql & "	PERS_CONOSC.ID_PERSONA = " & IDP & ") GRADO_PERS,  CONOSCENZE.ID_CONOSCENZA "
	Sql = Sql & "FROM "
	Sql = Sql & "CONOSCENZE, COMPET_CONOSC, COMPET_FP "
	Sql = Sql & "WHERE "
	Sql = Sql & "CONOSCENZE.ID_CONOSCENZA = COMPET_CONOSC.ID_CONOSCENZA "
	Sql = Sql & "AND "
	Sql = Sql & "COMPET_CONOSC.ID_COMPETENZA = COMPET_FP.ID_COMPETENZA "
	Sql = Sql & "AND "
	Sql = Sql & "COMPET_FP.ID_FIGPROF = " & request("ID_FIGPROF")

'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
	Set Rs = Conn.Execute (Sql)
	if not rs.eof then CONOSCENZE = Rs.getrows()

	Sql = ""
	Sql = Sql & "SELECT "
	Sql = Sql & "CAPACITA.DENOMINAZIONE, COMPET_CAPAC.ID_COMPETENZA, COMPET_CAPAC.GRADO_COMP_CAP, "
	Sql = Sql & "	(SELECT PERS_CAPAC.GRADO_CAPACITA FROM PERS_CAPAC "
	Sql = Sql & "	WHERE "
	Sql = Sql & "	PERS_CAPAC.ID_CAPACITA = CAPACITA.ID_CAPACITA AND "
	Sql = Sql & "	PERS_CAPAC.ID_PERSONA = " & IDP & ") GRADO_PERS, CAPACITA.ID_CAPACITA "
	Sql = Sql & "FROM "
	Sql = Sql & "CAPACITA, COMPET_CAPAC, COMPET_FP "
	Sql = Sql & "WHERE "
	Sql = Sql & "CAPACITA.ID_CAPACITA = COMPET_CAPAC.ID_CAPACITA "
	Sql = Sql & "AND "
	Sql = Sql & "COMPET_CAPAC.ID_COMPETENZA = COMPET_FP.ID_COMPETENZA "
	Sql = Sql & "AND "
	Sql = Sql & "COMPET_FP.ID_FIGPROF = " & request("ID_FIGPROF")

'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
	Set Rs = Conn.Execute (Sql)
	if not rs.eof then CAPACITA = Rs.getrows()

	Sql = ""
	Sql = Sql & "SELECT "
	Sql = Sql & "SETTORI.DENOMINAZIONE, SETTORI.ID_SETTORE, "
	Sql = Sql & "VALIDAZIONE.ID_VALID, VALIDAZIONE.FL_VALID, VALIDAZIONE.ID_PERS_INS "
	Sql = Sql & "FROM "
	Sql = Sql & "SETTORI, SETTORI_FIGPROF, VALIDAZIONE "
	Sql = Sql & "WHERE "
	Sql = Sql & "SETTORI_FIGPROF.ID_VALID = VALIDAZIONE.ID_VALID "
	Sql = Sql & "AND "
	Sql = Sql & "SETTORI.ID_SETTORE = SETTORI_FIGPROF.ID_SETTORE "
	Sql = Sql & "AND "
	Sql = Sql & "SETTORI_FIGPROF.ID_FIGPROF = " & request("ID_FIGPROF")

'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
	Set Rs = Conn.Execute (Sql)
	if not rs.eof then SETTORI = Rs.getrows()


	Sql = ""
	Sql = Sql & "SELECT "
	Sql = Sql & "COMPORTAMENTI.DENOMINAZIONE, COMPORTAMENTI.ID_COMPORTAMENTO, "
	Sql = Sql & "VALIDAZIONE.ID_VALID, VALIDAZIONE.FL_VALID, VALIDAZIONE.ID_PERS_INS, COMPOR_FP.GRADO_FP_COMPOR, "
	Sql = Sql & "	(SELECT PERS_COMPOR.GRADO_PERS_COMPOR FROM PERS_COMPOR "
	Sql = Sql & "	WHERE "
	Sql = Sql & "	PERS_COMPOR.ID_COMPORTAMENTO = COMPORTAMENTI.ID_COMPORTAMENTO AND "
	Sql = Sql & "	PERS_COMPOR.ID_PERSONA = " & IDP & ") GRADO_PERS "
	Sql = Sql & "FROM "
	Sql = Sql & "COMPORTAMENTI, COMPOR_FP, VALIDAZIONE "
	Sql = Sql & "WHERE "
	Sql = Sql & "COMPOR_FP.ID_VALID = VALIDAZIONE.ID_VALID "
	Sql = Sql & "AND "
	Sql = Sql & "COMPORTAMENTI.ID_COMPORTAMENTO = COMPOR_FP.ID_COMPORTAMENTO "
	Sql = Sql & "AND "
	Sql = Sql & "COMPOR_FP.ID_FIGPROF = " & request("ID_FIGPROF")

'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
	Set Rs = Conn.Execute (Sql)
	if not rs.eof then COMPORT = Rs.getrows()

	Sql = ""
	Sql = Sql &  " SELECT "
	Sql = Sql &  "   ARE.DENOMINAZIONE, FIG.DENOMINAZIONE"
	Sql = Sql &  " FROM "
	Sql = Sql &  "   AREE_PROFESSIONALI ARE, "
	Sql = Sql &  "   FIGUREPROFESSIONALI FIG, "
	Sql = Sql &  "   VALIDAZIONE VAL"
	Sql = Sql &  " WHERE "
	Sql = Sql &  "   ( FIG.ID_AREAPROF=ARE.ID_AREAPROF ) AND "
	Sql = Sql &  "   ( FIG.ID_VALID=VAL.ID_VALID ) AND"
	Sql = Sql &  "   ( VAL.FL_VALID=1 ) AND "
	Sql = Sql &  "   ( FIG.ID_FIGPROF=" & request("ID_FIGPROF") & ")"
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
	Set Rs = Conn.Execute (Sql)

	if not rs.eof then
		DenArea = Rs(0)
		DenFig  = Rs(1)
	end if

set Rs = Nothing
Conn.Close
Set conn = Nothing

%>

<SCRIPT LANGUAGE="JavaScript">
function Progetto(ID_EL,COD_EL)
{
	goToPage("../ProgettoProf/ProgettoProf_2.asp?ID_FIGPROF=<%=ID_FIGPROF%>&ID_ELEMENTO=" + ID_EL + "&COD_ELEMENTO=" + COD_EL + "&back=<%=request.servervariables("URL")%>")
}

</SCRIPT>

<table border=0 width='500' CELLPADDING=0 cellspacing=0>
	<tr class='tblcomm'>
		<td  align=left  class=sfondomenu>
			<span class="tbltext0"><b>PROYECTO PROFESIONAL</b></span>
		</td>
		<td width="25" valign='bottom' background="<%=Session("Progetto")%>/images/imgConsiel/sfondo_linguetta.gif" >
		<img border="0" src="<%=Session("Progetto")%>/images/imgConsiel/tondo_linguetta.gif"></td>
		<td width="278" valign='bottom' background="<%=Session("Progetto")%>/images/imgConsiel/sfondo_linguetta.gif">&nbsp;</td>
	</tr>
    <tr>
		<td class=sfondocomm  align="left" colspan="2" class="tbltext1">
		Definici�n de un Proyecto Profesional</td> 
		<td class=sfondocomm>
		<a href="Javascript:Show_Help('<%=Session("Progetto")%>/HelpBdc/Bilancio_Competenze/ProgProf_2.htm')">
		 <img src="<%=Session("Progetto")%>/images/Help.gif" border='0'  align=right alt='Help'>
		</a>
		</td>
    </tr>
    <tr>

		<td colspan=3 bgcolor='#3399CC'></td>
    </tr>
    <tr>

		<td colspan=3 height='15'></td>
    </tr>

</table>
</center>


<center>
<br>
<%

	Response.Write "<table border=0 cellspacing=1 width='500'>"
		Response.Write "<tr class='sfondocomm'>"
			Response.Write "<td align='center' colspan=3><b>DEFINICION DE MI PROYECTO PROFESIONAL </b></td>"
		Response.Write "</tr>"
		Response.Write "<tr class='sfondocomm'>"
			Response.Write "<td width='30%'><b>Figura Profesional: </b></td>"
			Response.Write "<td > " & UCase(DenFig)& "</td>"

				Response.Write "<td>"
					Response.Write "<input type='button' VALUE='Proyecto' class='my' OnClick='Progetto("""",""FPR"")'>"
				Response.Write "</td>"
		Response.Write "</tr>"
		Response.Write "<tr class='sfondocomm'>"
			Response.Write "<td width='30%'><b>Area Profesional : </b></td>"
			Response.Write "<td colspan=2> " & DenArea & "</td>"

		Response.Write "</tr>"
	Response.Write "</table>"

	Response.Write "<br>"

	if IsArray(SETTORI) then
		Response.Write "<table border=0 width='500' cellspacing=1 cellpadding=0 >"
		Response.write "<tr>"
		Response.Write "<td>"

		Response.Write "<table border=0 cellspacing=1 width='100%'>"
		Response.Write "<tr CLASS='tblsfondo'>"
			Response.Write "<td class='tbltext1'><b><small>Settori Merceologici</small></b></td>"
		Response.Write "</tr>"
			for I = lbound(SETTORI,2) to Ubound(SETTORI,2)
				Response.Write "<tr>"
					Response.Write "<td class='tbltext1'><small>" & SETTORI(0,I) & "</small></td>"
				Response.Write "</tr>"
			next
			Response.Write "</table>"
		Response.Write "</td>"
		Response.Write "</tr>"
		Response.Write "</table>"
	end if
%>
<br>
<%
	if IsArray(COMPETENZE) then
	Response.Write "<table border=0 width='500' cellspacing=1 cellpadding=0>"
	Response.write "<tr>"
	Response.Write "<td>"

	Response.Write "<table border=0 cellspacing=1 width=100% >"
	Response.Write "<tr CLASS='tblsfondo'>"
		Response.Write "<td class='tbltext1' colspan='2'><b><small>Competenze</small></b></td>"
		Response.Write "<td class='tbltext1' align='center'><small><b>Atteso</small></b></td>"
		Response.Write "<td class='tbltext1' align='center'><small><b>Dich.</small></b></td>"
		Response.Write "<td class='tbltext1' align='center'>&nbsp;</td>"
	Response.Write "</tr>"
	for i = lbound(COMPETENZE,2) to Ubound(COMPETENZE,2)
		Response.Write "<tr>"
			Response.Write "<td class='tbltext1' colspan='2'><b>&nbsp;&nbsp;<small>"
			Response.Write "<a href=""javascript:Progetto(" & COMPETENZE(1,I) & ",'COM')"">" & COMPETENZE(0,I) & "</small></b></a></td>"
			Response.Write "<td class='tbltext1' colspan='3'><b>&nbsp;&nbsp;" & COMPETENZE(2,I) & "</b></td>"
		Response.Write "</TR>"
		Response.Write "<tr CLASS='tblsfondo'>"
			Response.Write "<td CLASS='tbltext1' WIDTH='12'>&nbsp;</td>"
			Response.Write "<td CLASS='tbltext1' colspan=4><b><small>Conoscenze</small></b></td>"
		Response.Write "</tr>"
		if isarray(CONOSCENZE) THEN
			for jj = lbound(CONOSCENZE,2) to Ubound(CONOSCENZE,2)
			if int(0 & COMPETENZE(1,I)) = int(0 & CONOSCENZE(1,jj)) then
			 if Int(0 & CONOSCENZE(3,jj)) < Int(0 & CONOSCENZE(2,jj)) then '### Escludo dalla visualizzazione gli elementi per i quali sono gi� Ok!
				Response.Write "<tr >"
					Response.Write "<td CLASS='tbltext1'>&nbsp;</td>"
					Response.Write "<td CLASS='tbltext1'><small>"
					Response.Write "<a href=""javascript:Progetto(" & CONOSCENZE(4,jj) & ",'CON')"">"  & CONOSCENZE(0,jj) & "</small></a></td>"
					Response.Write "<td CLASS='tbltext1'align='center'><small>" & CONOSCENZE(2,jj) & "</small></td>"
					if isnull(CONOSCENZE(3,jj)) then
						testo= "--- "
					else
						testo= CONOSCENZE(3,jj)
					end if

					if Int(0 & CONOSCENZE(3,jj)) >= Int(0 & CONOSCENZE(2,jj)) then
						img = "<img src='SI.gif'>"
					else
						img = "<img src='NO.gif'>"
					end if
					Response.Write "<td CLASS='tbltext1' align='center'>&nbsp;&nbsp;<small>" & testo & "</small></td>"
					Response.Write "<td CLASS='tbltext1' align='center'>&nbsp;&nbsp;" & img & "</td>"
				Response.Write "</tr>"
			 end if
			end if
			next
		END IF

		Response.Write "<tr CLASS='tblsfondo'>"
		Response.Write "<td class='tbltext1' WIDTH='30'>&nbsp;</td>"
		Response.Write "<td class='tbltext1' colspan=4><b><small>Capacit�<small></b></td>"
		Response.Write "</tr>"

		IF ISARRAY(CAPACITA) THEN
			for jj = lbound(CAPACITA,2) to Ubound(CAPACITA,2)
			if int(0 & COMPETENZE(1,I)) = int(0 & CAPACITA(1,jj)) then
			 if Int(0 & CAPACITA(3,jj)) < Int(0 & CAPACITA(2,jj)) then
				Response.Write "<tr>"
					Response.Write "<td class='tbltext1'>&nbsp;</td>"
					Response.Write "<td class='tbltext1'><small>"
					Response.Write "<a href=""javascript:Progetto(" & CAPACITA(4,jj) & ",'CAP')"">"  & CAPACITA(0,jj) & "</small></A></td>"
					Response.Write "<td class='tbltext1' align='center'><small>" & CAPACITA(2,jj) & "</small></td>"
					if isnull(CAPACITA(3,jj)) then
						testo= "--- "
					else
						testo= CAPACITA(3,jj)
					end if
					if Int(0 & CAPACITA(3,jj)) >= Int(0 & CAPACITA(2,jj)) then
						img = "<img src='SI.gif'>"
					else
						img = "<img src='NO.gif'>"
					end if

					Response.Write "<td align='center' class='tbltext1'>&nbsp;&nbsp;<small>" & Testo & "</small></td>"
					Response.Write "<td align='center' class='tbltext1'>&nbsp;&nbsp;" & img & "</td>"
				Response.Write "</tr>"
			 end if
			end if
			next
		END IF
	next
	Response.Write "</table>"
	Response.Write "</td>"
	Response.Write "</tr>"
	Response.Write "</table>"
	end if
%>
<br>
<%
	'#### Escludo dalla visualizzazione i Comportamenti (nel caso si vuole visualizzarli togliere l'if e decommentare l'altro)
	if false then
	'if IsArray(COMPORT) then
	Response.Write "<table border=0 width='500' cellspacing=1 cellpadding=0>"
	Response.write "<tr>"
	Response.Write "<td>"

	Response.Write "<table border=0 cellspacing=1 width='100%'>"
	Response.Write "<tr CLASS='tblsfondo'>"
		Response.Write "<td class='tbltext1'><b><small>Comportamenti</small></b></td>"
		Response.Write "<td class='tbltext1' align='center'><b><small>Atteso</small></b></td>"
		Response.Write "<td class='tbltext1' align='center'><b><small>Dich.</small></b></td>"
		Response.Write "<td class='tbltext1' align='center'>&nbsp;</td>"
	Response.Write "</tr>"
		for I = lbound(COMPORT,2) to Ubound(COMPORT,2)
			Response.Write "<tr>"
				Response.Write "<td class='tbltext1'>&nbsp;&nbsp;<small>" & COMPORT(0,I) & "</small></td>"
				Response.Write "<td class='tbltext1'>&nbsp;&nbsp;<small>" & COMPORT(5,I) & "</small></td>"

				if isnull(COMPORT(6,I)) then
					Testo = " -- "
				else
					Testo = Int(0 & COMPORT(6,I))
				End if

				if Int(0 & COMPORT(6,I)) >= Int(0 & COMPORT(5,I)) then
					img = "<img src='SI.gif'>"
				else
					img = "<img src='NO.gif'>"
				End if

				Response.Write "<td class='tbltext1' align='center'><b>&nbsp;&nbsp;<small>" & Testo & "</small></b></td>"
				Response.Write "<td class='tbltext1' align='center'><b>&nbsp;&nbsp;" & img & "</b></td>"
			Response.Write "</tr>"
		next
		Response.Write "</table>"
	Response.Write "</td>"
	Response.Write "</tr>"
	Response.Write "</table>"
	end if

%>
<br>
<table border=0 width='500' cellspacing=1 cellpadding=0>
	<tr>
		<td align='left'>
			<input type='button' value='Volver' OnClick = "goToPage('<%=Back%>?ID_FIGPROF=<%=Request("ID_FIGPROF")%>')" CLASS='My'>
		</td>
	</tr>
<tr>
<td colspan=5 bgcolor='#3399CC'></td>
</tr>
</table>
</center>
<!-- #include Virtual="/strutt_coda2.asp" -->
