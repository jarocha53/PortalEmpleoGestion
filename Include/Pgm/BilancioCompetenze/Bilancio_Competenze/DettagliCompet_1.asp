<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->

<!-- #include Virtual="/strutt_testa2.asp" -->
<!-- #INCLUDE FILE="../Utils.asp" -->
<form name='CONFERMA_ELEMENTI' Method='POST' action='ConfermaElem.asp'>
<%
Tipo=Request("TIPO")

Select case Tipo

	case "CAPAC"
		VIs="CAPACITA'"

		Sql = ""
		Sql = Sql & " SELECT  CAPACITA.DENOMINAZIONE, PERS_CAPAC.GRADO_CAPACITA, "
		Sql = Sql & " AREA_CAPACITA.DENOMINAZIONE,PERS_CAPAC.ID_CAPACITA, AREA_CAPACITA.ID_AREACAPACITA"
		Sql = Sql & " FROM PERS_CAPAC, CAPACITA, AREA_CAPACITA"
		Sql = Sql & " WHERE "
		Sql = Sql & " CAPACITA.ID_AREACAPACITA = AREA_CAPACITA.ID_AREACAPACITA AND "
		Sql = Sql & " PERS_CAPAC.ID_CAPACITA = CAPACITA.ID_CAPACITA AND "
		Sql = Sql & " PERS_CAPAC.ID_PERSONA = " & IDP
		Sql = Sql & " ORDER BY AREA_CAPACITA.DENOMINAZIONE "

	case "CONOSC"
		VIs="CONOSCENZE"

		Sql = ""
		Sql = Sql & " SELECT CONOSCENZE.DENOMINAZIONE, PERS_CONOSC.COD_GRADO_CONOSC, AREA_CONOSCENZA.DENOMINAZIONE, "
		Sql = Sql & " PERS_CONOSC.ID_CONOSCENZA,AREA_CONOSCENZA.ID_AREACONOSCENZA  "
		Sql = Sql & " FROM PERS_CONOSC, CONOSCENZE,AREA_CONOSCENZA "
		Sql = Sql & " WHERE "
		Sql = Sql & " CONOSCENZE.ID_AREACONOSCENZA = AREA_CONOSCENZA.ID_AREACONOSCENZA AND "
		Sql = Sql & " PERS_CONOSC.ID_CONOSCENZA = CONOSCENZE.ID_CONOSCENZA AND "
		Sql = Sql & " PERS_CONOSC.ID_PERSONA = " & IDP
		Sql = Sql & " ORDER BY AREA_CONOSCENZA.DENOMINAZIONE "

	case "COMPOR"
		VIs="COMPORTAMENTI"

		Sql = ""
		Sql = Sql & " SELECT COMPORTAMENTI.DENOMINAZIONE,PERS_COMPOR.GRADO_PERS_COMPOR, "
		Sql = Sql & " AREA_COMPORTAMENTI.DENOMINAZIONE,PERS_COMPOR.ID_COMPORTAMENTO, AREA_COMPORTAMENTI.ID_AREACOMPORTAMENTO "
		Sql = Sql & " FROM PERS_COMPOR, COMPORTAMENTI, AREA_COMPORTAMENTI "
		Sql = Sql & " WHERE "
		Sql = Sql & " COMPORTAMENTI.ID_AREACOMPORTAMENTO = AREA_COMPORTAMENTI.ID_AREACOMPORTAMENTO AND "
		Sql = Sql & " PERS_COMPOR.ID_COMPORTAMENTO = COMPORTAMENTI.ID_COMPORTAMENTO AND "
		Sql = Sql & " PERS_COMPOR.ID_PERSONA = " & IDP
		Sql = Sql & " ORDER BY AREA_COMPORTAMENTI.DENOMINAZIONE "

	case "UTENTE"
		VIs="INSERITI DALL'UTENTE"
		Sql = ""
		Sql = Sql & " SELECT DENOMINAZIONE, POSSESSO,ID_ELEMENTO  "
		Sql = Sql & " FROM ELEMENTI_USR "
		Sql = Sql & " WHERE  ID_PERSONA = " & IDP
end select

Set Conn = server.CreateObject ("ADODB.Connection")
Conn.open strConn
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
Set Rs = Conn.Execute (Sql)
if not rs.eof then
	LST_USR = Rs.getrows()
end if
Set Rs = Nothing
Set conn = Nothing


Response.Write "<table border=0 cellspacing=1 cellpadding=0 width='500'>"
Response.Write "<tr><td background='../imgs/sfmnsmall.jpg' align=center><font color='#000000'>"
Response.Write "<B>Conferma Elementi Inseriti</B></td></tr>"
Response.Write "</tr></table><br>"

Response.Write "<b>" & Vis & "</b><BR>"
Response.Write "<table border=0 width=500 cellspacing=1 cellpadding=0 style='font-family: Verdana; font-size: 8pt;'>"


IF Tipo<>"UTENTE" then
	'CAPACITA, CONOSCENZA, COMPORTAMENTO Codificate nel dizionario dati
	IF ISARRAY(LST_USR) THEN
		Response.Write "<TR><TD bgcolor='#E8F6F6'><b>Area di Riferimento/Denominazione</b></td>"
		Response.Write "<td bgcolor='#E8F6F6' align=center><b>Confermi<br>Si / No</b></td></tr>"
		VAL=""

		For I = lbound(LST_USR,2) to Ubound(LST_USR,2)

			if VAL = LST_USR(2,I) then
				VAL = ""
			else
				response.write "<tr><td COLSPAN=2 bgcolor='#f1f3f3'><B><I>"
				response.write "<A HREF='javascript: goToPage(""AnalisiCompetenze.asp?IdArea=" & LST_USR(4,I) & """)'>"& LST_USR(2,I) & "</a>"
				Response.Write "</I></B></td></TR>"
			end if

			response.write "<td>&nbsp;&nbsp;&nbsp;" & LST_USR(0,I) & "</td>"
			response.write "<td align='center'>"
			Response.Write "<input type='radio' checked name='COD" & LST_USR(3,I) & "' value='0'>"
			Response.Write "<input type='radio' name='COD" & LST_USR(3,I) & "' value='1'>"
			Response.Write "<input type='hidden' name='CODICE' value='" & LST_USR(3,I) & "'>"
			Response.Write "</td></tr>"

			VAL = LST_USR(2,I)
		Next
	END IF
	Response.Write "</table>"
ELSE
	'Definite dall'utente .......
	IF ISARRAY(LST_USR) THEN
		Response.Write "<TR><TD bgcolor='#E8F6F6'><b>Area di Riferimento/Denominazione</b></td>"
		Response.Write "<td bgcolor='#E8F6F6' align=center><b>Confermi<br>Si / No</b></td></tr>"
		FOR I=0 TO UBOUND(LST_USR,2)
			response.write "<tr><td>" & LST_USR(0,I) & "</td>"
			response.write "<td align='center'>"
			Response.Write "<input type='radio' checked name='COD" & LST_USR(2,I) & "' value='0'>"
			Response.Write "<input type='radio' name='COD" & LST_USR(2,I) & "' value='1'>"
			Response.Write "<input type='hidden' name='CODICE' value='" & LST_USR(2,I) & "'>"
			response.write "</td></tr>"
		NEXT
	END IF
	Response.Write "</table>"
END IF

Response.Write "<input type='hidden' name='TIPO' value='" & Tipo & "'>"

Response.Write "<input type='hidden' name='IdPers' value='" & request("IdPers") & "'>"
Response.Write "<input type='hidden' name='IND_FASE' value='" & request("IND_FASE") & "'>"
%>

<BR><HR width='530' align='left'>
<br>
<center>
<div id="aa">
<input type="button" value="Volver" onClick="javascript: goToPage('DettagliCompet.asp')" class='My'>
<input type="submit" value="Confirmar" name='conferma' class='My'>
</div>
</center>
</form>
<!-- #include Virtual="/strutt_coda2.asp" -->
