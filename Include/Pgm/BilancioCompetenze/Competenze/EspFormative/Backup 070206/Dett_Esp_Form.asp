<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #Include Virtual="/strutt_testa2.asp" -->
<!-- #include Virtual = "/Include/DecCod.asp" -->
<!-- #Include Virtual="/Pgm/BilancioCompetenze/Utils.asp" -->
<!-- #include Virtual = "/include/OpenConn.asp" -->
<!-- #Include Virtual="/Pgm/BilancioCompetenze/ValidaDtNascita.asp" -->
<!-- #Include Virtual="/Include/ControlDateVB.asp" -->


<script language="javascript">
	<!--
<!-- #Include Virtual=/Include/ControlString.inc -->
<!-- #Include Virtual="/Include/ControlNum.inc" -->

function valida(Obj)
{	

	Obj.COD_TCORSO.value=TRIM(Obj.COD_TCORSO.value)
	Obj.DESC_ENTE.value=TRIM(Obj.DESC_ENTE.value)
	Obj.AA_CORSO.value=TRIM(Obj.AA_CORSO.value)
	Obj.AA_CORSO_UltAnno.value=TRIM(Obj.AA_CORSO_UltAnno.value)
	Obj.ORE_DURATA.value=TRIM(Obj.ORE_DURATA.value)  
	Obj.DESC_CERT.value=TRIM(Obj.DESC_CERT.value)
	
	
	if (Obj.COD_TCORSO.value == "")
	{
		alert("El campo T�tulo de Estudio es obligatorio")
		Obj.COD_TCORSO.focus() 
		return false
	}

	if (!ValidateInputStringWithNumber(Obj.COD_TCORSO.value))
	{
		alert("El T�tulo de Estudio es erroneo")
		Obj.COD_TCORSO.focus() 
		return false;
	}

	if (Obj.ID_AREA.options[Obj.ID_AREA.selectedIndex].value == "")
	{
		alert("El campo Area de Referencia es obligatorio")
		Obj.ID_AREA.focus() 
		return false;
	}
	
//		if (Obj.COD_STATUS_CORSO.value == "")
//	{
//		alert("Il campo:\n'Conseguimento' \n � obbligatorio")
//		Obj.COD_STATUS_CORSO.focus() 
//		return false;
//	}

	
	
	   if (!ValidateInputStringWithNumber(Obj.DESC_ENTE.value))
	{
		alert("El campo Tema es erroneo")
		Obj.DESC_ENTE.focus() 
		return false;
	}
	
	

	if ((Obj.AA_CORSO.value == "") && (Obj.AA_CORSO_UltAnno.value == "")) 
	{
		alert("Debe ingresar el Ultimo a�o cursado si el curso no ha finalizado \n o bien el A�o de finalizaci�n si el curso ha finalizado")
		Obj.AA_CORSO.focus() 
		return false
	}	
	
	
	if ((!Obj.AA_CORSO.value == "") && (!Obj.AA_CORSO_UltAnno.value == "")) 
	{
		alert("El campo A�o se refiere al ultimo A�o cursado")
		
	}	
	
		
	if (!Obj.AA_CORSO.value == "") 
	{
		if (!isNum(Obj.AA_CORSO.value))
			{
				alert("El campo A�o debe ser num�rico")
				Obj.AA_CORSO.focus() 
				return false
			}
		if (Obj.AA_CORSO.value  > <%=year(Date)%>)
			{
			alert("El A�o de Finalizaci�n no puede ser superior al a�o en curso")
			Obj.AA_CORSO.focus() 
			return false;	
			}
	if (ValidaAnnoNascita(Obj.AA_CORSO.value))
		{ 
		alert("El A�o de Finalizaci�n no puede ser inferior al a�o de nacimiento") 
		Obj.AA_CORSO.focus() 
		return false; 
		} 

	if ((Obj.COD_STATUS_CORSO.value == "1") || (Obj.COD_STATUS_CORSO.value == "2"))
		{
			alert("El A�o de finalizaci�n debe ingresarse solo en el caso que se haya finalizado el curso")
			Obj.AA_CORSO.focus() 
			return false;
		}


	}	
	 

	 if (!Obj.AA_CORSO_UltAnno.value == "") 
	{
		if (!isNum(Obj.AA_CORSO_UltAnno.value))
		{
			alert("El Ultimo A�o Cursado debe ser num�rico")
			Obj.AA_CORSO_UltAnno.focus() 
			return false;
		}
		if (Obj.AA_CORSO_UltAnno.value  > <%=year(Date)%>)
		{
			alert("El Ultimo A�o Cursado no puede ser superior al a�o en curso")
			Obj.AA_CORSO_UltAnno.focus() 
			return false;	
		}
		if (ValidaAnnoNascita(Obj.AA_CORSO_UltAnno.value))
		{ 
		alert("El Ultimo A�o Cursado no puede ser inferior al a�o de nacimiento") 
		Obj.AA_CORSO_UltAnno.focus() 
		return false; 
		}
		if (!((Obj.COD_STATUS_CORSO.value == "1") || (Obj.COD_STATUS_CORSO.value == "2")))
			{
				alert("El Ultimo A�o Cursado debe ser ingresado unicamente si el curso no fue finalizado")
				Obj.AA_CORSO.focus() 
				return false;
			}
		 
	  }	
  
		
	if (!(Obj.ORE_DURATA.value=="" ))
		{
			if (!isNum(Obj.ORE_DURATA.value))
			{
			alert("El campo Duraci�n en Horas debe ser numerico")
			Obj.ORE_DURATA.focus() 
			return false;
			}
	    }


	    if (Obj.CmbCOD_STAT_FREQ.value == "")
	    {
		alert("El campo Cursado en... es obligatorio")
		Obj.CmbCOD_STAT_FREQ.focus() 
		return false;
	    }
	    
/*	    LColuccia/RGuidotti:
		Commentato controllo = Rimossa obbligatoriet� del campo "Riconosciuto in".
		if (Obj.CmbCOD_STAT_RIC.value == "")
	    {
		alert("Il campo Riconosciuto in � obbligatorio")
		Obj.CmbCOD_STAT_RIC.focus()
		return false;
	    }
*/
	    if (!ValidateInputStringWithNumber(Obj.DESC_CERT.value))
		{
		alert("El campo Descripci�n de la Certificaci�n es obligatorio")
		Obj.DESC_CERT.focus() 
		return false;
		}
	    
}
//-->
</script>

<%


ID_FORMAZIONE =  request("Id_Formazione")


		Sql = " SELECT "
		Sql = Sql &  " FORMAZIONE.COD_TCORSO, "
		Sql = Sql &  " AREA_CORSO.DENOMINAZIONE, "
		Sql = Sql &  " FORMAZIONE.ID_AREA, "
		Sql = Sql &  " FORMAZIONE.COD_TIPO, "
		'Sql = Sql &  " FORMAZIONE.COD_IST_ER, "
		Sql = Sql &  " FORMAZIONE.DESC_ENTE, "
		Sql = Sql &  " FORMAZIONE.AA_CORSO,"
		Sql = Sql &  " FORMAZIONE.COD_STATUS_CORSO, "
		'Sql = Sql &  " FORMAZIONE.FL_CERTIFICAZIONE, "
		Sql = Sql &  " FORMAZIONE.ORE_DURATA, "
		Sql = Sql &  " FORMAZIONE.COD_ATT_FINALE, "
		Sql = Sql &  " FORMAZIONE.DESC_CERT, "
		Sql = Sql &  " FORMAZIONE.COD_STAT_FREQ, "
		Sql = Sql &  " FORMAZIONE.COD_STAT_RIC, "
		Sql = Sql &  " FORMAZIONE.ID_SETTORE, "
		Sql = Sql &  " FORMAZIONE.DT_TMST, COD_TIPO_DURACION "
		Sql = Sql &  " FROM FORMAZIONE, AREA_CORSO " 
		Sql = Sql &  " WHERE "
		Sql = Sql &  " (AREA_CORSO.ID_AREA=FORMAZIONE.ID_AREA) AND "
		Sql = Sql & "  FORMAZIONE.ID_PERSONA= " & IDP & " AND FORMAZIONE.ID_FORMAZIONE=" & ID_FORMAZIONE


'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
	Set rs = cc.execute(sql)
if not rs.eof then
	COD_TCORSO			= RS("COD_TCORSO") 'Denominazione Corso
	ID_AREA  			= RS("ID_AREA") 'Area di riferimento
	COD_TIPO			= RS("COD_TIPO")'Tipologia corso
	COD_STATUS_CORSO	= RS("COD_STATUS_CORSO")	'Status Conseguimento
	DESC_ENTE	        = RS("DESC_ENTE")	'Status Conseguimento	
'	COD_IST_ER			= RS("COD_IST_ER") 'Descrizione Istituto erogatore
	COD_ATT_FINALE		= RS("COD_ATT_FINALE") 'Codice attestato finale
	ORE_DURATA			= RS("ORE_DURATA") 'Durata in ORE
	AA_CORSO			= RS("AA_CORSO") 'Anno Corso
'	FL_STAGE			= RS("FL_STAGE") 'Stage
'	FL_CERTIFICAZIONE	= RS("FL_CERTIFICAZIONE") 'Certificazione
	DESC_CERT 	        = RS("DESC_CERT") 'Certificazione
	COD_STAT_FREQ		= RS("COD_STAT_FREQ")'Stato frequenza corso
	COD_STAT_RIC		= RS("COD_STAT_RIC")'Stato in cui � riconosciuto
	ID_SETTORE		    = RS("ID_SETTORE") 'Settore Merceologico
	DT_TMST				= RS("DT_TMST") 'Data di stampa
	ID_TIPO_DURACION    = RS("COD_TIPO_DURACION")

END IF



	if trim(COD_STATUS_CORSO) = "1" or COD_STATUS_CORSO = "2" then 
		AA_CORSO_UltAnno = AA_CORSO
	else
		AA_CORSO_concluso = AA_CORSO
	end if	
	


	if not isnull(COD_TCORSO) then
		COD_TCORSO= REPLACE(COD_TCORSO,"'","`")
	end if

  	sub CreaComboCOD_STATUS_CORSO(Codice)
	dim stringa_frequenza,aFrequenza

		sSql="SELECT ID_CAMPO_DESC " &_
			"FROM DIZ_DATI " &_
			"WHERE " &_
			"ID_TAB='FORMAZIONE' AND ID_CAMPO='COD_STATUS_CORSO'"

'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
		set Rst=CC.Execute(sSql)
		stringa_frequenza=Rst("ID_CAMPO_DESC")
		aFrequenza=split(stringa_frequenza,"|")
	%>
		<select name="COD_STATUS_CORSO" class="textblack" id="COD_STATUS_CORSO">

	<%	for i=0 to Ubound(aFrequenza) step 2
			Response.Write "<option value='" & aFrequenza(i) & "'"
			if aFrequenza(i)=Codice then Response.Write " selected"
			Response.Write ">" & Ucase((aFrequenza(i+1))) & "</option>"
		next
	%>	
	</select>
	<%	Rst.Close
		set Rst=nothing
	end sub

	sub CreaComboCOD_ATT_FINALE(Codice)
	dim stringa_frequenza,aFrequenza

		sSql="SELECT ID_CAMPO_DESC " &_
			"FROM DIZ_DATI " &_
			"WHERE " &_
			"ID_TAB='FORMAZIONE' AND ID_CAMPO='COD_ATT_FINALE'"

'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
		set Rst=CC.Execute(sSql)
		stringa_frequenza=Rst("ID_CAMPO_DESC")
		aFrequenza=split(stringa_frequenza,"|")
	%>
		<select name="COD_ATT_FINALE" class="textblack" id="COD_ATT_FINALE">

	<%	for i=0 to Ubound(aFrequenza) step 2
			Response.Write "<option value='" & aFrequenza(i) & "'"
			if aFrequenza(i)=Codice then Response.Write " selected"
			Response.Write ">" & Ucase((aFrequenza(i+1))) & "</option>"
		next
	%>	
	</select>
	<%	Rst.Close
		set Rst=nothing
	end sub


%>
<center>
	<form method="post" name="Mod_Esp_Formative" action="Cnf_Esp_Formative.asp" OnSubmit="return valida(this)">
	
	<input type="hidden" value="<%=Request("IdPers")%>" name="IdPers">
	<input type="hidden" value="<%=Request("LivMenu")%>" name="LivMenu">
	<input type="hidden" value="<%=Request("IND_FASE")%>" name="IND_FASE">
	<input type="hidden" value="<%=DT_TMST%>" name="DT_TMST">
	<input type="hidden" value="<%=ID_FORMAZIONE%>" name="ID_FORMAZIONE">
	

	<table border="0" width="500" CELLPADDING="0" cellspacing="0">
		<tr class="tblcomm">
			<td align="left" class="sfondomenu">
				<span class="tbltext0"><b>CURSO DE FORMACION/ESPECIALIZACION</b></span>
			</td>
			<td width="25" valign="bottom" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">
			<img border="0" src="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
			<td width="140" valign="bottom" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">&nbsp;
			</td>
		</tr>
</table>
		
		<table border="0" width="500" CELLPADDING="0" cellspacing="0">
    <tr>
		<td class="sfondocomm" align="left" class="tbltext1">
			Para modificar el curso de formaci�n realizado  <br>Seleccionar una opci&oacute;n y presionar <b>Enviar</b>
			para guardar la modificaci�n. 

		
		</td> 
		<td class="sfondocomm">
		
		
		<a onmouseover="javascript: window.status= ' '; return true;" href="Javascript:Show_Help('/Pgm/Help/BilancioCompetenze/Competenze/EspFormative/Dett_Esp_Form')">
		<img src="<%=Session("Progetto")%>/images/Help.gif" border="0" align="right" alt="Help">
		</a>
		</td>
    </tr>
		<tr height="2">
		<td class="sfondocomm" width="100%" colspan="2" background="<%=Session("Progetto")%>/images/separazione.gif">
	</td>
	</tr>
</table>	

<br>

	<table border="0" width="500" cellspacing="1" cellpadding="2">
		
		<tr>
			
        <td class="tbltext1"><b>Nombre del Curso*</td>
			<td class="tbltext1" width="60%">
				<input style="TEXT-TRANSFORM: uppercase; width:300px;" class="textblacka" maxlength="60" name="COD_TCORSO" value="<%=COD_TCORSO%>">
			</td>
		</tr>
		<tr>
			
        <td class="tbltext1"><b>Area de Formaci�n*&nbsp;</td>
			<td class="tbltext1" width="60%">

	<%
		Sql= ""
		Sql= Sql & " SELECT ID_AREA, DENOMINAZIONE FROM AREA_CORSO ORDER BY DENOMINAZIONE"
    
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
		Set Rs = Cc.Execute (Sql)

		Response.Write "<SELECT  class='textblack' name='ID_AREA'>"
			if not rs.eof then

				DO

					Response.Write "<option value='" & RS(0) & "'" 
				if trim(ID_AREA) = trim(rs(0)) then 
					Response.Write "SELECTED"
				end if
					Response.Write ">"
					Response.Write replace(RS(1),"�","'",1)
					Response.Write  "</option>"
					RS.MOVENEXT
				LOOP UNTIL RS.EOF
			END IF
		Set Rs = Nothing
		Response.Write "</SELECT>"
			Response.Write "</td>"
		Response.Write "</tr>"
	%>
		
		<tr>
			
        <td class="tbltext1"> <b>Tipolog�a del Curso&nbsp; </td>
			<td class="tbltext1" width="60%">
				<%
				'LColuccia/RGuidotti:
				'Creata combo per Tipologia Corso.
				UtCod_Tipo= "TCORS|0|" & date & "|" & COD_TIPO & "|CmbCOD_TIPO|ORDER BY DESCRIZIONE"			
				CreateCombo(UtCod_Tipo)
				'FINE LColuccia/RGuidotti
				%>
			</td>
		</tr>
		
		<tr>
			
        <td class="tbltext1"><b>Estado*</b></td>
			<td class="tbltext1" width="60%">
			<%CreaComboCOD_STATUS_CORSO(COD_STATUS_CORSO)%>
			</td>
		</tr>
	
		<tr height="20">
			
        <td align="left" class="tbltext1"> <b>Instituci�n que dict� el curso:</b></td>
			
			<td class="tbltext1">
				<input style="TEXT-TRANSFORM: uppercase; width:300px;" class="textblacka" maxlength="50" name="DESC_ENTE" value="<%=DESC_ENTE%>">
			</td>
		</tr>

	<tr height="15">
		<td colspan="2">
		</td>
	</tr>
	<tr>
	<td colspan="2" height="2" bgcolor="#3399cc"><td>
	</tr>
	
	<tr>
		<td colspan="2"> <span class="textblack"> Si el curso fue completado ingresar 
          el siguiente dato:</span> </td>
	</tr>
		
			<tr height="20">
			
        <td class="tbltext1" align="left"> <b>A&ntilde;o de Finalizaci�n*</b><br>
          (aaaa)
			</td>
			<td align="left" class="tbltext1" width="60%">
				<input class="textblacka" size="11" maxlength="4" name="AA_CORSO" value="<%=AA_CORSO_concluso%>">
			</td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
	<tr>
		<td colspan="2"> <span class="textblack"> Si fue abandonado o esta en curso, completar 
          el siguiente dato</span> </td>
		</tr>
			
    <tr height="20">
		<td class="tbltext1" align="left"> <b>�ltimo a&ntilde;o Cursado*</b><br>
          (aaaa)
		</td>
		<td align="left" class="tbltext1" width="60%">
				<input class="textblacka" size="11" maxlength="4" name="AA_CORSO_UltAnno" value="<%=AA_CORSO_UltAnno%>">
		</td>
    </tr>		   

	<tr>
		<td colspan="2" height="2" bgcolor="#3399cc"><td>
	</tr>
	<tr height="15">
		<td colspan="2"></td>
	</tr>


	<tr height="20">
		<td align="left" colspan="1" class="tbltext1"> <b>Duraci�n:</b></td>
		<td align="left" colspan="1" width="">
				<input class="textblacka" size="11" maxlength="4" name="ORE_DURATA" VALUE="<%=ORE_DURATA%>">
				 <span class="tbltext1"><b>&nbsp;&nbsp;&nbsp;&nbsp; Medida en:</b></span>
				 &nbsp;&nbsp;&nbsp;&nbsp;
			<%	
			CreateComboDizDati "FORMAZIONE","COD_TIPO_DURACION","COD_TIPO_DURACION","textblack","",ID_TIPO_DURACION
			%>
		</td>
    </tr>


		<tr>
			
        <td class="tbltext1"><b>Tipo de Certificado</b> </td>
			<td class="tbltext1" width="60%">
				<% CreaComboCOD_ATT_FINALE(COD_ATT_FINALE) %>
			</td>
		</tr>
		<tr height="20">
			
        <td align="left" class="tbltext1"> <b>Instituci�n Certificadora</b> 
          <br>
          (Ej: IRAM, ISO, etc.)
			</td>
			<td align="left" width="60%">
				<input style="TEXT-TRANSFORM: uppercase; width:300px;" class="textblacka" maxlength="50" name="DESC_CERT" VALUE="<%=DESC_CERT%>">
			</td>
		</tr>
		<tr>
			
        <td class="tbltext1"><b>Cursado en*</b> </td>
			<td class="tbltext1" width="60%">
		
		<%	
			UtCod_stat_Freq= "STATO|0|" & date & "|" & COD_STAT_FREQ & "|CmbCOD_STAT_FREQ|ORDER BY DESCRIZIONE"			
				CreateCombo(UtCod_stat_Freq)
		%>
	
			</td>
		</tr>
		<tr>
			
        <td class="tbltext1"> <b>Cerfiticado en</b></td>
			<td class="tbltext1" width="60%">
				<%
				UtCod_Stat_Ric= "STATO|0|" & date & "|" & COD_STAT_RIC & "|CmbCOD_STAT_RIC|ORDER BY DESCRIZIONE"			
				CreateCombo(UtCod_Stat_Ric)
				%>
			</td>
		</tr>
			
		<tr>
			
        <td class="tbltext1"><b>Sector&nbsp;</b> </td>
			<td class="tbltext1" width="60%">
	
	
		<%
		Sql= ""
		Sql= Sql & " SELECT ID_SETTORE,DENOMINAZIONE FROM SETTORI ORDER BY DENOMINAZIONE"

'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
		Set Rs = Cc.Execute (Sql)
		Response.Write "<SELECT  class='textblack' name='ID_SETTORE'>"
			if not rs.eof then
					Response.Write "<option value='NULL'></option>"		
			DO
					Response.Write "<option value='" & Rs(0) & "'" 
					
						if   trim(ID_SETTORE) = trim(Rs(0)) then 
							Response.Write "SELECTED"
						end if
					
					Response.Write ">" 
					Response.Write  LEFT(RS(1),50) 
					Response.Write 	"</option>"					
					RS.MOVENEXT
				LOOP UNTIL RS.EOF
			end if
		Set Rs = Nothing
		Response.Write "</SELECT>"

		%>
	
		</td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>	
	</table>
	
	<table border="0" width="500" CELLPADDING="0" cellspacing="0">
				
		<tr>
			<td align="middle">
				<a href="javascript:goToPage('Esp_formative.asp')"><img alt="Indietro" border="0" src="<%=Session("Progetto")%>/images/indietro.gif"></a>
				<input type="image" src="<%=Session("Progetto")%>/images/conferma.gif" border="0" value="Salva" id="image1" name="image1">
			</td>
		</tr>
	</table>
	</form>
</center>
<!--#include Virtual = "/include/closeconn.asp"-->
<!-- #include Virtual="/strutt_coda2.asp" -->
