<!-- #Include Virtual="/strutt_testa2.asp" -->
<!-- #include Virtual = "include/DecCod.asp" -->
<!-- #Include Virtual="/Pgm/BilancioCompetenze/Utils.asp" -->
<!-- #include Virtual = "include/OpenConn.asp" -->
<!-- #Include Virtual="/Pgm/BilancioCompetenze/ValidaDtNascita.asp" -->
<!-- #Include Virtual="/Include/ControlDateVB.asp" -->




<script language="javascript">
<!--

<!-- #Include Virtual=/Include/ControlString.inc -->
<!-- #Include Virtual="/Include/ControlNum.inc" -->

function valida(Obj)
{	
	Obj.COD_TCORSO.value=TRIM(Obj.COD_TCORSO.value)
	Obj.DESC_ENTE.value=TRIM(Obj.DESC_ENTE.value)
	Obj.COD_STATUS_CORSO.value=TRIM(Obj.COD_STATUS_CORSO.value)
	Obj.DESC_CERT.value=TRIM(Obj.DESC_CERT.value)
	Obj.AA_CORSO.value=TRIM(Obj.AA_CORSO.value)
	Obj.AA_CORSO_UltAnno.value=TRIM(Obj.AA_CORSO_UltAnno.value)
	Obj.ORE_DURATA.value=TRIM(Obj.ORE_DURATA.value)  
	
	if (Obj.COD_TCORSO.value == "")
	{
		alert("Il campo Titolo corso � obbligatorio")
		Obj.COD_TCORSO.focus() 
		return false;
	}
	
if (!ValidateInputStringWithNumber(Obj.COD_TCORSO.value))
	{
		alert("Il campo Titolo corso  � formalmente errato")
		Obj.COD_TCORSO.focus() 
		return false;
	}

	if (Obj.ID_AREA.options[Obj.ID_AREA.selectedIndex].value == "")
	{
		alert("Il campo Area di riferimento  � obbligatorio")
		Obj.ID_AREA.focus() 
		return false;
	}
	
	if (Obj.COD_STATUS_CORSO.value == "")
	{
		alert("Il campo Conseguimento  � obbligatorio")
		Obj.COD_STATUS_CORSO.focus() 
		return false;
	}
	
	
	   if (!ValidateInputStringWithNumber(Obj.DESC_ENTE.value))
	{
		alert("Il campo Presso  � formalmente errato")
		Obj.DESC_ENTE.focus() 
		return false;
	}
		
	

	if ((Obj.AA_CORSO.value == "") && (Obj.AA_CORSO_UltAnno.value == "")) 
	{
		alert("Il campo Conclusa nel oppure  Ultimo anno frequentato � obbligatorio")
		Obj.AA_CORSO.focus() 
		return false;
	}	
	
	if ((!Obj.AA_CORSO.value == "") && (!Obj.AA_CORSO_UltAnno.value == "")) 
	{
		alert("L'anno si pu� inserire solo nel campo Conclusa nel  o solo nel campo Ultimo Anno di Frequenza ")
		Obj.AA_CORSO.focus() 
		return false;
	}	
		
	
	if (!Obj.AA_CORSO.value == "") 
	{
		if (!isNum(Obj.AA_CORSO.value))
		{
			alert("Il campo Conclusa nel deve essere numerico")
			Obj.AA_CORSO.focus() 
			return false;
		}
	if (Obj.AA_CORSO.value  > <%=year(Date)%>)
		{
		alert("Il campo Conclusa nel non pu� essere superiore all'anno corrente")
		Obj.AA_CORSO.focus() 
		return false;	
		}
	if (ValidaAnnoNascita(Obj.AA_CORSO.value))
		{ 
		alert("L'anno del campo Conclusa nel deve essere  successivo alla data di nascita") 
		Obj.AA_CORSO.focus() 
		return false; 
		} 	
		if ((Obj.COD_STATUS_CORSO.value == "1") || (Obj.COD_STATUS_CORSO.value == "2"))
		{
			alert("L'anno del campo Conclusa nel deve essere indicato  solo se l'esperienza � conclusa")
			Obj.AA_CORSO.focus() 
			return false;
		}
	}	
	 

	 if (!Obj.AA_CORSO_UltAnno.value == "") 
	{
		if (!isNum(Obj.AA_CORSO_UltAnno.value))
		{
			alert("Il campo Ultimo anno frequentato Deve essere numerico")
			Obj.AA_CORSO_UltAnno.focus() 
			return false;
		}
		if (Obj.AA_CORSO_UltAnno.value  > <%=year(Date)%>)
		{
			alert("L'anno del campo Ultimo anno frequentato  non pu� essere superiore all'anno corrente")
			Obj.AA_CORSO_UltAnno.focus() 
			return false;	
		}
		if (ValidaAnnoNascita(Obj.AA_CORSO_UltAnno.value))
		{ 
		alert("L'anno del campo Ultimo anno frequentato deve essere successiva alla data di nascita") 
		Obj.AA_CORSO_UltAnno.focus() 
		return false; 
		} 
		if (!((Obj.COD_STATUS_CORSO.value == 1) || (Obj.COD_STATUS_CORSO.value == 2)))
		{
		
		alert("L'anno del campo Ultimo anno frequentato  deve essere indicato solo se l'esperienza non � conclusa")
		Obj.AA_CORSO_UltAnno.focus() 
		return false;
		}
	 }	
	  
	
		
	if (!(Obj.ORE_DURATA.value=="" ))
		{
			if (!isNum(Obj.ORE_DURATA.value))
			{
			alert("Il campo Durata Ore deve essere in formato numerico")
			Obj.ORE_DURATA.focus() 
			return false;
			}
	    }
	    
	    
	    if (Obj.CmbCOD_STAT_FREQ.options[Obj.CmbCOD_STAT_FREQ.selectedIndex].value == "")
	    {
		alert("Il campo Frequentato in  � obbligatorio")
		Obj.CmbCOD_STAT_FREQ.focus()
		return false;
	    }
	    
	    if (Obj.CmbCOD_STAT_RIC.options[Obj.CmbCOD_STAT_RIC.selectedIndex].value == "")
	    {
		alert("Il campo Riconosciuto in � obbligatorio")
		Obj.CmbCOD_STAT_RIC.focus()
		return false;
	    }
	    
	    if (!ValidateInputStringWithNumber(Obj.DESC_CERT.value))
		{
		alert("Il campo Descrizione della Certificazione  � formalmente errato")
		Obj.DESC_CERT.focus() 
		return false;
		}
	    
	 


}
//-->
</script>


  <%
	sub CreaComboCOD_STATUS_CORSO(Codice)
	dim stringa_frequenza,aFrequenza

		sSql="SELECT ID_CAMPO_DESC " &_
			"FROM DIZ_DATI " &_
			"WHERE " &_
			"ID_TAB='FORMAZIONE' AND ID_CAMPO='COD_STATUS_CORSO'"

		set Rst=CC.Execute(sSql)
		stringa_frequenza=Rst("ID_CAMPO_DESC")
		aFrequenza=split(stringa_frequenza,"|")
	%>
		<select name="COD_STATUS_CORSO" class="textblack" id="COD_STATUS_CORSO">

	<%	for i=0 to Ubound(aFrequenza) step 2
			Response.Write "<option value='" & aFrequenza(i) & "'"
			if aFrequenza(i)=Codice then Response.Write " selected"
			Response.Write ">" & Ucase((aFrequenza(i+1))) & "</option>"
		next
	%>	
	</select>
	<%	Rst.Close
		set Rst=nothing
	end sub

	sub CreaComboCOD_ATT_FINALE(Codice)
	dim stringa_frequenza,aFrequenza

		sSql="SELECT ID_CAMPO_DESC " &_
			"FROM DIZ_DATI " &_
			"WHERE " &_
			"ID_TAB='FORMAZIONE' AND ID_CAMPO='COD_ATT_FINALE'"

		set Rst=CC.Execute(sSql)
		stringa_frequenza=Rst("ID_CAMPO_DESC")
		aFrequenza=split(stringa_frequenza,"|")
	%>
		<select name="COD_ATT_FINALE" class="textblack" id="COD_ATT_FINALE">

	<%	for i=0 to Ubound(aFrequenza) step 2
			Response.Write "<option value='" & aFrequenza(i) & "'"
			if aFrequenza(i)=Codice then Response.Write " selected"
			Response.Write ">" & Ucase((aFrequenza(i+1))) & "</option>"
		next
	%>	
	</select>
	<%	Rst.Close
		set Rst=nothing
	end sub

%>



<center>
		<form method="post" name="Esp_Formative" action="Salva_Esp_Formative.asp" OnSubmit="return valida(this)">
		<input type="hidden" value="<%=Request("IdPers")%>" name="IdPers">
		<input type="hidden" value="<%=Request("LivMenu")%>" name="LivMenu">
		<input type="hidden" value="<%=Request("IND_FASE")%>" name="IND_FASE">
		
		<input type="hidden" value="<%=Request("Backpage")%>" name="Backpage">
		<input type="hidden" size="50" name="txtoggi" value="<%=ConvDateToString(Now())%>">

	<table border="0" width="500" CELLPADDING="0" cellspacing="0">
		<tr class="tblcomm">
			<td align="left" class="sfondomenu">
				<span class="tbltext0"><b>CORSI DI FORMAZIONE/SPECIALIZZAZIONE</b></span>
			</td>
			<td width="25" valign="bottom" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">
			<img border="0" src="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
			<td width="140" valign="bottom" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">&nbsp;
			</td>
		</tr>
	</table>
	
<table border="0" width="500" CELLPADDING="0" cellspacing="0">
    <tr>
		<td class="sfondocomm" align="left" class="tbltext1">
		Inserimento corsi di formazione. <br>
			Indica di seguito le informazioni relative agli eventuali corsi di formazione a cui hai partecipato.
			<br>Compila i campi sottostanti e premi <b>Invia</b> per salvare le modifiche. 
			</td> 
		<td class="sfondocomm"><a onmouseover="javascript: window.status = ' '; return true; " href="Javascript:Show_Help('/Pgm/Help/BilancioCompetenze/Competenze/EspFormative/Add_Esp_Formative')">
			<img src="<%=Session("Progetto")%>/images/Help.gif" border="0" align="right" alt="Help"></a>
		</td>
    </tr>
 	<tr height="2">
		<td class="sfondocomm" width="100%" colspan="2" background="<%=Session("Progetto")%>/images/separazione.gif">
		</td>
	</tr>
</table>	

<table border="0" cellpadding="0" cellspacing="1" width="500">
	
		<tr>
		<td colspan="2">&nbsp;</td>
		<tr>
		
	<tr height="20">
        <td align="left" colspan="2" class="tbltext1">
	<b>Titolo Corso * &nbsp;</b>
        </td>
        <td align="left" colspan="2" width="60%">
			<input style="TEXT-TRANSFORM: uppercase;width:300px; " class="textblacka" maxlength="60" name="COD_TCORSO">
        </td>
	</tr>

	<tr height="20">
        <td align="left" colspan="2" class="tbltext1">
			<p align="left"><b>Area di <br>Riferimento*&nbsp;</b>
			</p>
        </td>
        <td align="left" colspan="2" width="60%" bgcolor="#FFFFFF">
			<p align="left">

			<%
		
			Sql= ""
			Sql= Sql & " SELECT ID_AREA, DENOMINAZIONE FROM AREA_CORSO ORDER BY DENOMINAZIONE"

			Set Rs = CC.Execute (Sql)

			Response.Write "<SELECT  class='textblack' name='ID_AREA'>"
			Response.Write "<option value=''></option>"
				if not rs.eof then
					DO
						Response.Write "<option value='" & RS(0) & "'>"
						Response.Write replace(RS(1),"�","'",1)
						Response.Write  "</option>"
						RS.MOVENEXT
					LOOP UNTIL RS.EOF
				END IF
			Set Rs = Nothing
			Response.Write "</SELECT>"
			Response.Write "</td>"
			%>
    </tr>

    
    <tr height="20">
		<td align="left" colspan="2">
			<p align="left" class="tbltext1"><strong>Conseguimento*</strong>
		</td>
		<td>
		<%CreaComboCOD_STATUS_CORSO("0")%>

		</td>
    </tr>
    
    <tr height="20">
        <td align="left" colspan="2" class="tbltext1">
			<p align="left"><b>Presso &nbsp;</b>
        </td>
        <td align="left" colspan="2" width="60%">
			<input style="TEXT-TRANSFORM: uppercase; width:300px; " class="textblacka" maxlength="50" name="DESC_ENTE">
        </td>
	</tr>


<tr height="15">
			<td colspan="4">
				</td>
			</tr>

			<tr>
				<td colspan="4" height="2" bgcolor="#3399cc"><td>
			</tr>
	
			<tr>
				<td colspan="4">
					<span class="textblack">
				Se L'Esperienza di Formazione � gi� stata conseguita, inserire nel
				 campo <b>Conclusa nel </b><br>l'anno di conseguimento.
					</span>
				</td>
			</tr>
		

	<tr height="20">
		<td class="tbltext1" align="left" colspan="2">
			<b>Conclusa nel*</b><br>(aaaa)
		</td>
		<td align="left" class="tbltext1" colspan="2" width="60%">
				<input class="textblacka" size="11" maxlength="4" name="AA_CORSO">
				&nbsp;
		</td>
    </tr>
    <tr>
		<td colspan="4">&nbsp;</td>
	</tr>		   
    <tr>
	    <td>
          					

			<tr>
				<td colspan="4">
					<span class="textblack">
						 Se L'Esperienza di Formazione non � completata o � ancora in  frequenza, inserire nel
						 campo <b>Ultimo anno frequentato </b> l'ultimo anno utile.
					</span>
				</td>
			</tr>
		
			
		</td>
    </tr>
    <tr height="20">
		<td class="tbltext1" align="left" colspan="2">
			<b>Ultimo anno frequentato*</b><br>(aaaa)
		</td>
		<td align="left" class="tbltext1" colspan="2" width="60%">
				<input class="textblacka" size="11" maxlength="4" name="AA_CORSO_UltAnno">
				&nbsp;
		</td>
    </tr>
    
    <tr height="15">
			<td colspan="4">
			</td>
	</tr>
	<tr>
		<td colspan="4" height="2" bgcolor="#3399cc"><td>
	</tr>
	<tr height="15">
		<td colspan="4">
		</td>
	</tr>
	<tr height="20">
		<td align="left" colspan="2" class="tbltext1">
			<b>Durata in Ore</b>
		</td>
		<td align="left" colspan="2" width="60%">
				<input class="textblacka" size="11" maxlength="4" name="ORE_DURATA">
		</td>
    </tr>
	<tr height="20">
		<td align="left" colspan="2">
			<p align="left" class="tbltext1">
				<strong>Attestato Finale</strong>
		</td>
		<td colspan="2">
		<%CreaComboCOD_ATT_FINALE("0")%>
		
			
		</td>
	</tr>
	  <tr height="20">
        <td align="left" colspan="2" class="tbltext1">
			<p align="left"><b>Descrizione della Certificazione &nbsp;</b>
					<br>(es. MOUS, CISCO, ECDL...)
        </td>
        <td align="left" colspan="2" width="60%">
			<input style="TEXT-TRANSFORM: uppercase; width:300px; " class="textblacka" maxlength="50" name="DESC_CERT">
        </td>
	</tr>
	
	<tr height="20">
		<td class="tbltext1" align="left" colspan="2">
			<p align="left"><b>Frequentato in*</b></td>
		<td colspan="2">
			<%
			UtCod_stat_Freq= "STATO|0|" & date & "|IT|CmbCOD_STAT_FREQ|ORDER BY DESCRIZIONE"			
			CreateCombo(UtCod_stat_Freq)
			%>
		</td>
	</tr>
	<tr height="20">
		<td class="tbltext1" align="left" colspan="2">
			<p align="left"><b>Riconosciuto in*</b></td>
		<td colspan="2">
			<%
			UtCod_Stat_Ric= "STATO|0|" & date & "|IT|CmbCOD_STAT_RIC|ORDER BY DESCRIZIONE"			
			CreateCombo(UtCod_Stat_Ric)
			%>
		</td>
	</tr>

	<tr height="20">
		<td class="tbltext1" align="left" colspan="2">
			<p align="left"><b>Settore <br>Merceologico</b></td>
		<td colspan="2">
			<%
			Sql= ""
			Sql= Sql & " SELECT ID_SETTORE,DENOMINAZIONE FROM SETTORI ORDER BY DENOMINAZIONE"

			Set Rs = CC.Execute (Sql)
				Response.Write "<SELECT  class='textblack' name='ID_SETTORE'>"
				Response.Write "<option value=''></option>"
				if not rs.eof then
					DO
						Response.Write "<option value='" & Rs(0) & "'>" & LEFT(RS(1),48) & "</option>"
						RS.MOVENEXT
					LOOP UNTIL RS.EOF
				end if
			Set Rs = Nothing
			Response.Write "</SELECT>"
			%>
				</td>
			</tr>

		<tr>
			<td colspan="4">&nbsp;
			</td>
		</tr>
			
	<tr>
		<td align="middle" colspan="3">
			<a href="javascript:goToPage('Esp_formative.asp')"><img alt="Indietro" border="0" src="<%=Session("Progetto")%>/images/indietro.gif"></a>
			<input type="image" src="<%=Session("Progetto")%>/images/conferma.gif" border="0" value="Salva" id="image1" name="image1">
		</td>
	</tr>
</table>

</form>
</center>
<!--#include Virtual = "/include/closeconn.asp"-->
<!-- #include Virtual="/strutt_coda2.asp" -->