<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #Include Virtual="/strutt_testa2.asp" -->
<!-- #include Virtual = "include/DecCod.asp" -->
<!-- #Include Virtual="/Pgm/BilancioCompetenze/Utils.asp" -->
<!-- #include Virtual = "include/OpenConn.asp" -->
<!-- #Include Virtual="/Pgm/BilancioCompetenze/ValidaDtNascita.asp" -->
<!-- #Include Virtual="/Include/ControlDateVB.asp" -->




<script language="javascript">
<!--

<!-- #Include Virtual=/Include/ControlString.inc -->
<!-- #Include Virtual="/Include/ControlNum.inc" -->

function valida(Obj)
{	
	Obj.COD_TCORSO.value=TRIM(Obj.COD_TCORSO.value)
	Obj.DESC_ENTE.value=TRIM(Obj.DESC_ENTE.value)
	Obj.COD_STATUS_CORSO.value=TRIM(Obj.COD_STATUS_CORSO.value)
	Obj.DESC_CERT.value=TRIM(Obj.DESC_CERT.value)
	Obj.AA_CORSO.value=TRIM(Obj.AA_CORSO.value)
	Obj.AA_CORSO_UltAnno.value=TRIM(Obj.AA_CORSO_UltAnno.value)
	Obj.ORE_DURATA.value=TRIM(Obj.ORE_DURATA.value)  
	
	if (Obj.COD_TCORSO.value == "")
	{
		alert("El campo titulo del curso es obligatorio")
		Obj.COD_TCORSO.focus() 
		return false;
	}
	
if (!ValidateInputStringWithNumber(Obj.COD_TCORSO.value))
	{
		alert("El campo titulo del curso es err�neo")
		Obj.COD_TCORSO.focus() 
		return false;
	}

	if (Obj.ID_AREA.options[Obj.ID_AREA.selectedIndex].value == "")
	{
		alert("El campo �rea de formaci�n es obligatorio")
		Obj.ID_AREA.focus() 
		return false;
	}
	
	if (Obj.COD_STATUS_CORSO.value == "")
	{
		alert("El campo estado es obligatorio")
		Obj.COD_STATUS_CORSO.focus() 
		return false;
	}
	
	
	   if (!ValidateInputStringWithNumber(Obj.DESC_ENTE.value))
	{
		alert("El campo acerca de es err�neo")
		Obj.DESC_ENTE.focus() 
		return false;
	}
		
	

	if ((Obj.AA_CORSO.value == "") && (Obj.AA_CORSO_UltAnno.value == "")) 
	{
		alert("El campo A�o de finalizaci�n o bien �ltimo a�o cursado es obligatorio")
		Obj.AA_CORSO.focus() 
		return false;
	}	
	
	if ((!Obj.AA_CORSO.value == "") && (!Obj.AA_CORSO_UltAnno.value == "")) 
	{
		alert("El a�o solo puede ingresarse en el campo A�o de finalizaci�n o bien en �ltimo a�o cursado ")
		Obj.AA_CORSO.focus() 
		return false;
	}	
		
	
	if (!Obj.AA_CORSO.value == "") 
	{
		if (!isNum(Obj.AA_CORSO.value))
		{
			alert("El campo A�o de finalizaci�n debe ser num�rico")
			Obj.AA_CORSO.focus() 
			return false;
		}
	if (Obj.AA_CORSO.value  > <%=year(Date)%>)
		{
		alert("El campo A�o de finalizaci�n no puede ser superior al a�o actual")
		Obj.AA_CORSO.focus() 
		return false;	
		}
	if (ValidaAnnoNascita(Obj.AA_CORSO.value))
		{ 
		alert("El campo A�o de finalizaci�n debe ser superior al a�o de nacimiento") 
		Obj.AA_CORSO.focus() 
		return false; 
		} 	
		if ((Obj.COD_STATUS_CORSO.value == "1") || (Obj.COD_STATUS_CORSO.value == "2"))
		{
			alert("El campo A�o de finalizaci�n debe ser indicado solo si la experiencia fue concluida")
			Obj.AA_CORSO.focus() 
			return false;
		}
	}	
	 

	 if (!Obj.AA_CORSO_UltAnno.value == "") 
	{
		if (!isNum(Obj.AA_CORSO_UltAnno.value))
		{
			alert("El campo Ultimo a�o cursado debe ser num�rico")
			Obj.AA_CORSO_UltAnno.focus() 
			return false;
		}
		if (Obj.AA_CORSO_UltAnno.value  > <%=year(Date)%>)
		{
			alert("El campo Ultimo a�o cursado no puede ser superior al a�o actual")
			Obj.AA_CORSO_UltAnno.focus() 
			return false;	
		}
		if (ValidaAnnoNascita(Obj.AA_CORSO_UltAnno.value))
		{ 
		alert("El campo Ultimo a�o cursado debe ser superior al a�o de nacimiento") 
		Obj.AA_CORSO_UltAnno.focus() 
		return false; 
		} 
		if (!((Obj.COD_STATUS_CORSO.value == 1) || (Obj.COD_STATUS_CORSO.value == 2)))
		{
		
		alert("El campo Ultimo a�o cursado debe ser indicado solo si la experiencia no fue concluida")
		Obj.AA_CORSO_UltAnno.focus() 
		return false;
		}
	 }	
	  
	
		
	if (!(Obj.ORE_DURATA.value=="" ))
		{
			if (!isNum(Obj.ORE_DURATA.value))
			{
			alert("El campo duraci�n debe ser num�rico")
			Obj.ORE_DURATA.focus() 
			return false;
			}
	    }
	    
	    
	    if (Obj.CmbCOD_STAT_FREQ.options[Obj.CmbCOD_STAT_FREQ.selectedIndex].value == "")
	    {
		alert("El campo Cursado en es obligatorio")
		Obj.CmbCOD_STAT_FREQ.focus()
		return false;
	    }
	    
/*	LColuccia/RGuidotti:
	Commentato controllo = Rimossa obbligatoriet� del campo "Riconosciuto in".
	
		if (Obj.CmbCOD_STAT_RIC.options[Obj.<EM>CmbCOD_STAT_RIC</EM>.selectedIndex].value == "")
	    {
		alert("Il campo Riconosciuto in � obbligatorio")
		Obj.CmbCOD_STAT_RIC.focus()
		return false;
	    }
*/
	    
	    if (!ValidateInputStringWithNumber(Obj.DESC_CERT.value))
		{
		alert("El campo Descripci�n de la Certificaci�n es err�neo")
		Obj.DESC_CERT.focus() 
		return false;
		}

}
//-->
</script>


  <%
	sub CreaComboCOD_STATUS_CORSO(Codice)
	dim stringa_frequenza,aFrequenza

		sSql="SELECT ID_CAMPO_DESC " &_
			"FROM DIZ_DATI " &_
			"WHERE " &_
			"ID_TAB='FORMAZIONE' AND ID_CAMPO='COD_STATUS_CORSO'"

'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
		set Rst=CC.Execute(sSql)
		stringa_frequenza=Rst("ID_CAMPO_DESC")
		aFrequenza=split(stringa_frequenza,"|")
	%>
		<select name="COD_STATUS_CORSO" class="textblack" id="COD_STATUS_CORSO">

	<%	for i=0 to Ubound(aFrequenza) step 2
			Response.Write "<option value='" & aFrequenza(i) & "'"
			if aFrequenza(i)=Codice then Response.Write " selected"
			Response.Write ">" & Ucase((aFrequenza(i+1))) & "</option>"
		next
	%>	
	</select>
	<%	Rst.Close
		set Rst=nothing
	end sub

	sub CreaComboCOD_ATT_FINALE(Codice)
	dim stringa_frequenza,aFrequenza

		sSql="SELECT ID_CAMPO_DESC " &_
			"FROM DIZ_DATI " &_
			"WHERE " &_
			"ID_TAB='FORMAZIONE' AND ID_CAMPO='COD_ATT_FINALE'"

'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
		set Rst=CC.Execute(sSql)
		stringa_frequenza=Rst("ID_CAMPO_DESC")
		aFrequenza=split(stringa_frequenza,"|")
	%>
		<select name="COD_ATT_FINALE" class="textblack" id="COD_ATT_FINALE">

	<%	for i=0 to Ubound(aFrequenza) step 2
			Response.Write "<option value='" & aFrequenza(i) & "'"
			if aFrequenza(i)=Codice then Response.Write " selected"
			Response.Write ">" & Ucase((aFrequenza(i+1))) & "</option>"
		next
	%>	
	</select>
	<%	Rst.Close
		set Rst=nothing
	end sub

%>



<center>
		<form method="post" name="Esp_Formative" action="Salva_Esp_Formative.asp" OnSubmit="return valida(this)">
		<input type="hidden" value="<%=Request("IdPers")%>" name="IdPers">
		<input type="hidden" value="<%=Request("LivMenu")%>" name="LivMenu">
		<input type="hidden" value="<%=Request("IND_FASE")%>" name="IND_FASE">
		
		<input type="hidden" value="<%=Request("Backpage")%>" name="Backpage">
		<input type="hidden" size="50" name="txtoggi" value="<%=ConvDateToString(Now())%>">

	<table border="0" width="500" CELLPADDING="0" cellspacing="0">
		<tr class="tblcomm">
			
        <td align="left" class="sfondomenu"> <span class="tbltext0"><b>CURSOS 
          DE FORMACION/ESPECIALIZACION</b></span></td>
			<td width="25" valign="bottom" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">
			<img border="0" src="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
			<td width="140" valign="bottom" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">&nbsp;
			</td>
		</tr>
	</table>
	
<table border="0" width="500" CELLPADDING="0" cellspacing="0">
    <tr>
		<td class="sfondocomm" align="left" class="tbltext1">
		Ingresar el Curso de Formaci�n. <br>
			Indicar la informaci�n relativa a los cursos de formaci�n en los cuales participo.
			<br>Completar los campos y presionar <b>Enviar</b> para guardar la modificaci�n. 
			</td> 
		<td class="sfondocomm"><a onmouseover="javascript: window.status = ' '; return true; " href="Javascript:Show_Help('/Pgm/Help/BilancioCompetenze/Competenze/EspFormative/Add_Esp_Formative/Default.htm')">
			<img src="<%=Session("Progetto")%>/images/Help.gif" border="0" align="right" alt="Help"></a>
		</td>
    </tr>
 	<tr height="2">
		<td class="sfondocomm" width="100%" colspan="2" background="<%=Session("Progetto")%>/images/separazione.gif">
		</td>
	</tr>
</table>	

<table border="0" cellpadding="0" cellspacing="1" width="500">
	<tr>
		<td colspan="2">&nbsp;</td>
	<tr>
		
	<tr height="20">
        <td align="left" colspan="2" class="tbltext1"> <b>Nombre del Curso* &nbsp;</b> 
        </td>
        <td align="left" colspan="2" width="60%">
			<input style="TEXT-TRANSFORM: uppercase;width:300px; " class="textblacka" maxlength="60" name="COD_TCORSO">
        </td>
	</tr>
	
	<tr height="20">
        <td align="left" colspan="2" class="tbltext1">
			<p align="left"><b>Area de Formaci&oacute;n*&nbsp;</b> </p>
        </td>
        <td align="left" colspan="2" width="60%" bgcolor="#FFFFFF">
			<p align="left">

			<%
		
			Sql= ""
			Sql= Sql & " SELECT ID_AREA, DENOMINAZIONE FROM AREA_CORSO ORDER BY DENOMINAZIONE"

'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
			Set Rs = CC.Execute (Sql)

			Response.Write "<SELECT  class='textblack' name='ID_AREA'>"
			Response.Write "<option value=''></option>"
				if not rs.eof then
					DO
						Response.Write "<option value='" & RS(0) & "'>"
						Response.Write replace(RS(1),"�","'",1)
						Response.Write  "</option>"
						RS.MOVENEXT
					LOOP UNTIL RS.EOF
				END IF
			Set Rs = Nothing
			Response.Write "</SELECT>"
			Response.Write "</td>"
			%>
    </tr>
    
    <tr height="20">
		<td class="tbltext1" align="left" colspan="2"> <b>Tipolog&iacute;a del 
          Curso </b></td>
		<td colspan="2">
			<%
			'LColuccia/RGuidotti:
			'Creata combo per Tipologia Corso.
			UtCod_Tipo= "TCORS|0|" & date & "||CmbCOD_TIPO|ORDER BY DESCRIZIONE"			
			CreateCombo(UtCod_Tipo)
			'FINE LColuccia/RGuidotti
			%>
		</td>
	</tr>
	
    <tr height="20">
		<td align="left" colspan="2">
			<p align="left" class="tbltext1"><strong>Estado*</strong> </td>
		<td>
		<%CreaComboCOD_STATUS_CORSO("0")%>

		</td>
    </tr>
    
    <tr height="20">
        <td align="left" colspan="2" class="tbltext1">
			<p align="left"><b>Instituci�n que dict� el curso:</b></td>
        <td align="left" colspan="2" width="60%">
			<input style="TEXT-TRANSFORM: uppercase; width:300px; " class="textblacka" maxlength="50" name="DESC_ENTE">
        </td>
	</tr>


	<tr height="15">
		<td colspan="4">
		</td>
	</tr>

	<tr>
		<td colspan="4" height="2" bgcolor="#3399cc"><td>
	</tr>
	
	<tr>
		<td colspan="4"> <p><span class="textblack"> Si el curso fue completado 
            ingresar el siguiente dato:</span> </p>
          </td>
	</tr>
		
	<tr height="20">
		<td class="tbltext1" align="left" colspan="2"> <b>A�o de finalizaci�n*</b><br>
          (aaaa)
		</td>
		<td align="left" class="tbltext1" colspan="2" width="60%">
				<input class="textblacka" size="11" maxlength="4" name="AA_CORSO">
				&nbsp;
		</td>
    </tr>
    
    <tr>
		<td colspan="4">&nbsp;</td>
	</tr>		   
    
    <tr>
	    <td>
    		<tr>
				
        <td colspan="4"> <span class="textblack"> Si fue abandonado o esta en curso completar 
          el siguiente dato:</span> </td>
			</tr>			
		</td>
    </tr>
    
    <tr height="20">
		<td class="tbltext1" align="left" colspan="2"> <b>�ltimo a&ntilde;o cursado*</b><br>
          (aaaa)
		</td>
		<td align="left" class="tbltext1" colspan="2" width="60%">
				<input class="textblacka" size="11" maxlength="4" name="AA_CORSO_UltAnno">
				&nbsp;
		</td>
    </tr>
    
    <tr height="15">
			<td colspan="4">
			</td>
	</tr>
	
	<tr>
		<td colspan="4" height="2" bgcolor="#3399cc"><td>
	</tr>
	
	<tr height="15">
		<td colspan="4">
		</td>
	</tr>
	
	<tr height="20">
		<td align="left" colspan="2" class="tbltext1"> <b>Total de horas:</b></td>
		<td align="left" colspan="2" width="60%">
				<input class="textblacka" size="11" maxlength="4" name="ORE_DURATA">
		</td>
    </tr>
	
	<tr height="20">
		<td align="left" colspan="2">
			<p align="left" class="tbltext1"> <strong>Tipo de Certificado</strong></td>
		<td colspan="2">
			<%CreaComboCOD_ATT_FINALE("0")%>
		</td>
	</tr>
	
	<tr height="20">
		<td align="left" colspan="2" class="tbltext1">
			<p align="left"><b>Instituci�n Certificadora</b><br>
            (Ej: IRAM, ISO, etc.)
        </td>
        <td align="left" colspan="2" width="60%">
			<input style="TEXT-TRANSFORM: uppercase; width:300px; " class="textblacka" maxlength="50" name="DESC_CERT">
        </td>
	</tr>
	
	<tr height="20">
		<td class="tbltext1" align="left" colspan="2">
			<p align="left"><b>Cursado en *</b></td>
		<td colspan="2">
			<%
			UtCod_stat_Freq= "STATO|0|" & date & "|IT|CmbCOD_STAT_FREQ|ORDER BY DESCRIZIONE"			
			CreateCombo(UtCod_stat_Freq)
			%>
		</td>
	</tr>	
	
	<tr height="20">
		<td class="tbltext1" align="left" colspan="2">
			<p align="left"><b>Certificado en</b></td>
		<td colspan="2">
			<%
			'LColuccia/RGuidotti:
			'Modificata CreateCombo, in modo da mostrare per default riga vuota.
			UtCod_Stat_Ric= "STATO|0|" & date & "||CmbCOD_STAT_RIC|ORDER BY DESCRIZIONE"			
			CreateCombo(UtCod_Stat_Ric)
			'FINE LColuccia/RGuidotti
			%>
		</td>
	</tr>
	
	<tr height="20">
		<td class="tbltext1" align="left" colspan="2">
			<p align="left"><b>Sector</b></td>
		<td colspan="2">
			<%
			Sql= ""
			Sql= Sql & " SELECT ID_SETTORE,DENOMINAZIONE FROM SETTORI ORDER BY DENOMINAZIONE"

'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
			Set Rs = CC.Execute (Sql)
				Response.Write "<SELECT  class='textblack' name='ID_SETTORE'>"
				Response.Write "<option value=''></option>"
				if not rs.eof then
					DO
						Response.Write "<option value='" & Rs(0) & "'>" & LEFT(RS(1),48) & "</option>"
						RS.MOVENEXT
					LOOP UNTIL RS.EOF
				end if
			Set Rs = Nothing
			Response.Write "</SELECT>"
			%>
		</td>
	</tr>
	
	<tr>
		<td colspan="4">&nbsp;
		</td>
	</tr>			
	
	<tr>
		<td align="middle" colspan="3">
			<a href="javascript:goToPage('Esp_formative.asp')"><img alt="Indietro" border="0" src="<%=Session("Progetto")%>/images/indietro.gif"></a>
			<input type="image" src="<%=Session("Progetto")%>/images/conferma.gif" border="0" value="Salva" id="image1" name="image1">
		</td>
	</tr>
</table>

</form>
</center>
<!--#include Virtual = "/include/closeconn.asp"-->
<!-- #include Virtual="/strutt_coda2.asp" -->
