<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->

<%if session("progetto")<>"" then%>
<!--#include Virtual = "include/OpenConn.asp"-->
<!--#include Virtual = "include/DecCod.asp"-->

<html>
<head>

<script language="javascript">
function chiudi(){
	self.close() 
}

function Invia(){
	if (document.frmRagione.cmbSede.value==""){
		alert("Debe Seleccionar Todos los Campos")
		return false
	}else{
		var appo
		var valori
		var appoSede
		var valSede
		appo = document.frmRagione.cmbCod.value
		valori = appo.split("|")
		appoSede = document.frmRagione.cmbSede.value
		valSede = appoSede.split("|")
		opener.document.AddEspro.Rag_Soc.value=valori[0] + " - " + valSede[1]
		opener.document.AddEspro.Settori.value=valori[1]
		opener.document.AddEspro.CmbCOD_FORMA.value=valori[2]
		opener.document.AddEspro.CmbDimaz.value=valori[3]
		opener.document.AddEspro.txtIdSede.value=valSede[0]
		self.close()
	}
}

function InviaMod(){
	if (document.frmRagione.cmbSede.value==""){
		alert("Debe seleccionar todos los Campos")
		return false
	}else{
		var appoMod
		var valoriMod
		var appoSede
		var valSede
		appoMod = document.frmRagione.cmbCod.value
		valoriMod = appoMod.split("|")
		appoSede = document.frmRagione.cmbSede.value
		valSede = appoSede.split("|")
		opener.document.Dett_Espro.Rag_Soc.value=valoriMod[0] + " - " + valSede[1]
		opener.document.Dett_Espro.Settori.value=valoriMod[1]
		opener.document.Dett_Espro.CmbCOD_FORMA.value=valoriMod[2]
		opener.document.Dett_Espro.CmbDimaz.value=valoriMod[3]
		opener.document.Dett_Espro.txtIdSede.value=valSede[0]
		self.close()
	}
}
</script>

<title>EMPRESA</title>
<link REL="STYLESHEET" TYPE="text/css" HREF="<%=session("progetto")%>/fogliostile.css">
</head>
<body>
<center>


<%
dim RICERCA,sForm,sProvSel,sAzienda,sidImp

RICERCA= ucase(request("RAG"))
RICERCA=replace(RICERCA,"'","''")
sForm = Request("nomeForm")
sProvSel = request("cmbProv")
sAzienda = request("cmbCod") 

if sAzienda <> "" then
	aDati = split(sAzienda,"|")
	sidImp = aDati(4)
end if
%>
<table border="0" width="500" CELLPADDING="0" cellspacing="0">
	<tr class="tblcomm">
		
      <td align="left" class="sfondomenu"> <span class="tbltext0"><b>Empresa</b></span></td>
		<td width="25" valign="bottom" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">
			<img border="0" src="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
		<td width="200" valign="middle" align="right" class="tbltext1" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
	</tr>
</table>

<table border="0" width="500" CELLPADDING="0" cellspacing="0">
    <tr>
		
      <td class="sfondocomm" align="left"> En esta pantalla se puede seleccionar 
        la Razon Social de la Empresa referida.<br>
        
		</td>
    </tr>
    <tr height="2">
		<td class="sfondocomm" width="100%" colspan="2" background="<%=Session("Progetto")%>/images/separazione.gif">
		</td>
	</tr>
</table>	
<p>
<form name="frmRagione" action="RagSociale.asp"> 
<input type="hidden" name="RAG" value="<%=RICERCA%>">
<input type="hidden" name="nomeForm" value="<%=sForm%>">
<table border="0" cellpadding="0" cellspacing="1" width="400">
<%

sSql = "Select Rag_soc,i.id_impresa,Cod_forma,Id_settore" &_
		" from impresa i" &_
		" where Rag_soc Like '" & RICERCA & "%'" &_
        " order by rag_soc"

'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
Set RS = cc.Execute (sSql)
 
if not RS.EOF then		%>
			
		<tr>
			
        <td class="tbltext1" align="left"> <b>Razon Social</b> </td>
		
		<td align="left">
		<select class="textblacka" name="cmbCod" onchange="javascript:frmRagione.submit()">
			<option value></option>						
<%
				Set RSDim = Server.CreateObject("ADODB.Recordset")
		
				do while not RS.EOF
		
						sSqlDim= "Select dim_impresa from val_impresa" &_
								 " where id_impresa=" & clng(RS("ID_IMPRESA")) &_
								 " order by dt_ril desc"
								 				
'PL-SQL * T-SQL  
SSQLDIM = TransformPLSQLToTSQL (SSQLDIM) 
						RSDim.Open sSqlDim,cc
						
						if RSDim.EOF then
							sDimAz=""
						else
							sDimAz=RSDim("DIM_IMPRESA")
						end if
						RSDim.Close ()
						
						if clng(RS("ID_IMPRESA")) =clng(sidImp) then 
						%>
						<option value="<%=RS("RAG_SOC")%>|<%=RS("ID_SETTORE")%>|<%=RS("COD_FORMA")%>|<%=sDimAz%>|<%=RS("ID_IMPRESA")%>" selected><%=RS("RAG_SOC")%></option>
						<%else%>
						<option value="<%=RS("RAG_SOC")%>|<%=RS("ID_SETTORE")%>|<%=RS("COD_FORMA")%>|<%=sDimAz%>|<%=RS("ID_IMPRESA")%>"><%=RS("RAG_SOC")%></option>
						<%end if
						RS.MoveNext()
				loop
		
				set RSDim=nothing
		
			%>
		</select>
	<%		
				RS.close
				Set RS=Nothing
	%>
				</td>
				</tr>
				<tr>
			<td class="tbltext1" align="left">
				<b>Departamento&nbsp;&nbsp;</b>					
			</td>
		
			<td align="left">
				<select class="textblacka" name="cmbProv" <%if sAzienda= "" then Response.Write "disabled" end if%> onchange="javascript:frmRagione.submit()">
							<option value></option>	
							<% if sAzienda <> "" then
								sqlProv = "SELECT DISTINCT(PRV) FROM SEDE_IMPRESA WHERE ID_IMPRESA =" & sidImp
'PL-SQL * T-SQL  
SQLPROV = TransformPLSQLToTSQL (SQLPROV) 
							    SET rsProv = cc.execute(sqlProv)
							    do while not rsProv.eof
							        appoDesc = DecCodVal("PROV", 0, date(), rsProv("PRV"),0)
									if rsProv("PRV") = sProvSel then%>
										<option value="<%=rsProv("PRV")%>" selected><%=appoDesc%></option>
								  <%else%>
										<option value="<%=rsProv("PRV")%>"><%=appoDesc%></option>
										
								<%	end if
									rsProv.movenext
							    loop
							    set rsProv = nothing
							   end if
							   
							%>
							
				</select>
			</td>
		</tr>	
			
        <td class="tbltext1" align="left"> <b>Sede</b></td>
		
		<td align="left">
			<select class="textblacka" name="cmbSede" <%if sProvSel= "" then Response.Write "disabled" end if%>>
						<option value></option>	
						<%if sProvSel <> "" then
							sqlsede="SELECT ID_SEDE,DESCRIZIONE " &_
									 "FROM SEDE_IMPRESA " &_
									 " WHERE ID_IMPRESA =" & sidImp &_
									 " AND PRV='" & sProvSel & "'"
'PL-SQL * T-SQL  
SQLSEDE = TransformPLSQLToTSQL (SQLSEDE) 
							SET rsSede = cc.execute(sqlsede)
							if not rsSede.eof then
								do while not rsSede.eof
									%>
										<option value="<%=rsSede("ID_SEDE")%>|<%=rsSede("DESCRIZIONE")%>"><%=rsSede("DESCRIZIONE")%></option>	
									<%
									rsSede.movenext
								loop
								
							end if
							set rsSede = nothing
						  end if	
						%>
			</select>
		</td>
		</tr>			
				</table>
<br><br>	
<table border="0" cellpadding="0" cellspacing="1" width="500">
	
<tr height="10">
		<td align="center">
				<input type="image" src="<%=Session("Progetto")%>/images/chiudi.gif" id="image2" name="image2" onclick="javascript:chiudi()">
				<%if sForm="AddEspro" then %>
					<input type="image" src="<%=Session("Progetto")%>/images/conferma.gif" id="image3" name="image3" onclick="javascript:Invia()">
		        <%else%>
					<input type="image" src="<%=Session("Progetto")%>/images/conferma.gif" id="image3" name="image3" onclick="javascript:InviaMod()">
		        <%end if%>
		</td>
</tr>
	
</table>
<%else%>
				<tr>
					
      <td class="tbltext3" align="center">No se encontraron resultados para el 
        criterio de busqueda ingresado.</td>
				</tr>
				</table>
<br><br>	
<table border="0" cellpadding="0" cellspacing="1" width="500">
	
<tr height="10">
		<td align="center">
				<input type="image" src="<%=Session("Progetto")%>/images/chiudi.gif" id="image2" name="image2" onclick="javascript:chiudi()">
				
		</td>
</tr>
	
</table>
					
<%end if%>


</form>
</center>
<%else%>
	<script>
		alert("Sessione scaduta")
		self.close()
	</script>
<%end if%>
</body>
</html>

<!--#include virtual = "/include/CloseConn.asp"-->
