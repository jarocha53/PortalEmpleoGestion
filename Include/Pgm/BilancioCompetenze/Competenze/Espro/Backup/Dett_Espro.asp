<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #Include Virtual="/strutt_testa2.asp" -->
<!-- #include Virtual="/Include/DecCod.asp"-->
<!-- #Include Virtual="/Pgm/BilancioCompetenze/Utils.asp" -->
<!-- #include Virtual="/Include/OpenConn.asp"-->
<!-- #Include Virtual="/Pgm/BilancioCompetenze/ValidaDtNascita.asp" -->
<!-- #Include Virtual="/Include/ControlDateVB.asp" -->
<!-- #Include Virtual="/Include/ControlVall.asp" -->

<script language="javascript">
	<!--
		//include del file per fare i controlli sulla validit� delle date
		<!--#include Virtual = "/Include/ControlNum.inc"-->

		//include del file per fare i controlli sulla validit� delle date
		<!--#include Virtual = "/Include/ControlDate.inc"-->
		//include del file per fare i controlli sulle stringhe
		<!-- #Include Virtual="/Include/ControlString.inc" -->

	function RicercaSede(){

	    sPresso = document.Dett_Espro.Rag_Soc.value
	    if(sPresso==""){
			alert("Debe completar primero el campo descripci�n")
	    }else{
			URL = "RagSociale.asp?RAG=" + sPresso + "&nomeForm=Dett_Espro"  
			opt = "address=no,status=no,width=550,height=280,top=150,left=15"
			window.open (URL,"",opt)
		}
	}
	
	function valida() {
		
		if (document.Dett_Espro.txtIdSede.value == "") {
			document.Dett_Espro.Rag_Soc.value=TRIM(document.Dett_Espro.Rag_Soc.value)
		}
		
		if (document.Dett_Espro.txtDataInizio.value ==""){
			alert("La fecha de inicio es obligatoria")
			document.Dett_Espro.txtDataInizio.focus()
			return false;				
		} 	
		
		DataInizio=document.Dett_Espro.txtDataInizio.value
		DataFine=document.Dett_Espro.txtDataFine.value
		DataOggi=document.Dett_Espro.txtoggi.value
		DataNascita=document.Dett_Espro.DataNascita.value

		if (!ValidateInputDate(DataInizio))	{
			document.Dett_Espro.txtDataInizio.focus()
			return false
		}

		if (ValidateRangeDate(DataInizio,DataOggi)==false){
			alert("La fecha de inicio no debe ser superior a la fecha actual")
			document.Dett_Espro.txtDataInizio.focus()
			return false;
		}
		
		//DataNascita 
		if (ValidateRangeDate(DataNascita,DataInizio)==false) {
			alert("La fecha de inicio debe ser superior a la fecha de nacimiento") 
			document.Dett_Espro.txtDataInizio.focus()
			return false 
		}

		if (!document.Dett_Espro.txtDataFine.value =="") {
			if (!ValidateInputDate(DataFine)) {
				document.Dett_Espro.txtDataFine.focus()
				return false
			}
			if (ValidateRangeDate(DataInizio,DataFine)==false)	{
				alert("La fecha de finalizaci�n debe ser superior a la fecha de inicio") 
				document.Dett_Espro.txtDataFine.focus()
				return false; 
			}
			if (ValidateRangeDate(DataFine,DataOggi)==false){
				alert("La fecha de finalizaci�n no debe ser superior a la fecha actual")
				document.Dett_Espro.txtDataFine.focus()
				return false;
			}
			if ((document.Dett_Espro.Mcess.value == "")) {
				alert("El campo motivo de finalizaci�n es obligatorio si se completa el campo 'Finalizado el'")
				document.Dett_Espro.Mcess.focus()
				return false;
			}
		}
		
		if ((!document.Dett_Espro.Mcess.value == "")) {
			if (document.Dett_Espro.txtDataFine.value =="") {
				alert("El campo motivo de finalizaci�n solo se completa si se ha ingresado la fecha en el campo 'Finalizado el'")
				document.Dett_Espro.Mcess.focus()
				return false;
			}
		
		}
				
		if (document.Dett_Espro.txtIdSede.value == "") {				
			if ((document.Dett_Espro.Settori.value == "")) {
				alert("El campo sector de la actividad es obligatorio")
				document.Dett_Espro.Settori.focus()
				return false;
			}
		}
		
		if ((document.Dett_Espro.CmbCOD_ESPERIENZA.value == "")) {
			alert("El campo tipologia de experiencia es obligatorio")
			document.Dett_Espro.CmbCOD_ESPERIENZA.focus()
			return false;
		}

		if (document.Dett_Espro.txtIdSede.value == "") {
			if  (!((document.Dett_Espro.CmbCOD_ESPERIENZA.value == "01"))) {	
				if 	((document.Dett_Espro.Rag_Soc.value == ""))	{
					alert("El campo descripci�n es obligatorio para la tipolog�a de experiencia seleccionada")
					document.Dett_Espro.Rag_Soc.focus() 
					return false;
				}
			}
			if ((!document.Dett_Espro.Rag_Soc.value == "")) {
				if  ((document.Dett_Espro.CmbCOD_FORMA.value == "")) {	
					alert("El campo forma jur�dica es obligatorio cuando se especifica el campo descripci�n")
					document.Dett_Espro.CmbCOD_FORMA.focus()
					return false
				}
				if  ((document.Dett_Espro.CmbDimaz.value == "")) {	
					alert("El campo dimensi�n de la empresa es obligatorio cuando se especifica el campo descripci�n")
					document.Dett_Espro.CmbDimaz.focus()
					return false;
				}
			}
			if ((!document.Dett_Espro.CmbCOD_FORMA.value == "")) {
				if  ((document.Dett_Espro.Rag_Soc.value == "")) {	
					alert("El campo descripci�n es obligatorio cuando se especifica el campo forma jur�dica")
					document.Dett_Espro.Rag_Soc.focus()
					return false
				}
			}
			if ((!document.Dett_Espro.CmbDimaz.value == "")) {
				if  ((document.Dett_Espro.Rag_Soc.value == ""))	{	
					alert("El campo descripci�n es obligatorio cuando se especif�ca el campo dimensi�n de la empresa")
					document.Dett_Espro.Rag_Soc.focus()
					return false
				}
			}
		}
	}


function ActivInput(valore)
	{

	if ((valore != "01"))
			{
			document.Dett_Espro.Rag_Soc.disabled = false;
			document.Dett_Espro.CmbTicli.value= "";
			document.Dett_Espro.CmbTicli.options.disabled = true;
			document.Dett_Espro.CmbCOD_FORMA.options.disabled = false;
			document.Dett_Espro.CmbDimaz.options.disabled = false;
			}
			else 
			{
			document.Dett_Espro.CmbTicli.options.value ="";
			document.Dett_Espro.CmbTicli.options.disabled = false;
			document.Dett_Espro.Rag_Soc.disabled = true;
			document.Dett_Espro.Rag_Soc.value = "";
			document.Dett_Espro.CmbCOD_FORMA.options.value = "";
			document.Dett_Espro.CmbCOD_FORMA.options.disabled = true;
			document.Dett_Espro.CmbDimaz.options.value = "";
			document.Dett_Espro.CmbDimaz.options.disabled = true;
			
			}
	}

	function HideVariables(sUrl,idForm)
	{
		document.forms.item(idForm).action = sUrl		
		document.forms.item(idForm).IdPers.value = document.valori.IdPers.value	
		document.forms.item(idForm).IND_FASE.value = document.valori.IND_FASE.value
		document.forms.item(idForm).LivMenu.value = document.valori.LivMenu.value
		document.forms.item(idForm).submit();
	}

	function Pulisci() {
		if (document.Dett_Espro.Settori.value != "") {
			document.Dett_Espro.Settori.value = "";
			document.Dett_Espro.CmbCOD_FORMA.value = "";
			document.Dett_Espro.CmbDimaz.value = "";
			document.Dett_Espro.txtIdSede.value = "";
		} 
	}

	function elimina() {
		document.Dett_Espro.action = "Del_Espro.asp";
		document.Dett_Espro.onsubmit = "";
		document.Dett_Espro.submit();
	}
	//-->
</script>

<%

Id_Espro = Request("Id_Espro")
	
	Sql = " SELECT  "
	Sql = Sql &  " ESPRO.DT_INI_ESP_PRO,"
	Sql = Sql &  " ESPRO.DT_FIN_ESP_PRO,"
	Sql = Sql &  " ESPRO.ID_SETTORE, "
	Sql = Sql &  " ESPRO.COD_ESPERIENZA, "
	Sql = Sql &  " ESPRO.RAG_SOC, "
	Sql = Sql &  " ESPRO.COD_FORMA, "
	sql = sql &  " ESPRO.COD_MANSIONE,"
	Sql = Sql &  "ESPRO.DIM_IMPRESA, "
	Sql = Sql &  "CODICI_ISTAT.MANSIONE, "
	'Sql = Sql &  " (SELECT FIGUREPROFESSIONALI.DENOMINAZIONE FROM  FIGUREPROFESSIONALI,ESPRO"
	'Sql = Sql &  " WHERE FIGUREPROFESSIONALI.COD_EJ = ESPRO.COD_MANSIONE  "
	'Sql = Sql &  " AND ESPRO.ID_ESPRO = " & Id_Espro & " AND ESPRO.ID_PERSONA = " & IDP &  ") MANSIONE,"
	Sql = Sql &  " ESPRO.COD_MOT_CESS, "
	Sql = Sql &  " ESPRO.COD_CCNL, "
	Sql = Sql &  " ESPRO.COD_LIVELLO, "
	'Sql = Sql &  " ESPRO.NUM_ORE_SETT, "
	Sql = Sql &  "ESPRO.COD_TIPO_CLIENTE, "
	Sql = Sql &  " ESPRO.FL_RESP_PERSONE, "
	'Sql = Sql &  " ESPRO.DT_DL5687, "
	Sql = Sql &  " ESPRO.DT_TMST, "
	Sql = Sql &  " ESPRO.ID_SEDE "	
	Sql = Sql &  " FROM ESPRO, CODICI_ISTAT"
	Sql = Sql &  " WHERE CODICI_ISTAT.CODICE = ESPRO.COD_MANSIONE "
	Sql = Sql &  "  AND  ESPRO.ID_ESPRO = " & Id_Espro  

'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
Set Rs = Cc.Execute (Sql)

if not rs.eof then
	
	DATAINIZIO = RS("DT_INI_ESP_PRO") 
	if DATAINIZIO <> "" then
		DataInizio =  ConvDateToString(DATAINIZIO) 
	end if

	DataFine = RS("DT_FIN_ESP_PRO") 

	if DataFine <> "" then
		DataFine =  ConvDateToString(DataFine) 
	end if
	
	ID_SETTORE			= RS("ID_SETTORE") 
	COD_ESPERIENZA		= RS("COD_ESPERIENZA")
	RAGIONESOCIALE		= RS("RAG_SOC") 
	COD_FORMA			= RS("COD_FORMA") 
	DIMENSIONE			= RS("DIM_IMPRESA")	
	MANSIONE			= RS("MANSIONE")'7 'Mansione
	MOT_CESS			= RS("COD_MOT_CESS") 
	CONTRATTONAZIONALE	= RS("COD_CCNL") 
	CONTRATTOLIVELLO	= RS("COD_LIVELLO")
	'ORESETTIMANALI		= RS(11)
	TIPOCLIENTE 		= RS("COD_TIPO_CLIENTE")
	RESPONPERS	 		= RS("FL_RESP_PERSONE")
	sCod_Istat			= rs("COD_MANSIONE")	
	'DATADL5687    		= RS(14)
	DT_TMST				= RS("DT_TMST")

	if RS("ID_SEDE") <> "" then
		IdSede = RS("ID_SEDE")
	else
		IdSede = ""
	end if

	if  COD_ESPERIENZA = "01" then 
		DisabilitaRAGIONESOCIALE = "disabled"
		DisabilitaCmbCOD_FORMA ="disabled"		
		DisabilitaCmbDimaz = "disabled"
		DisabilitaCmbTicli= ""
	else
		DisabilitaCmbTicli= "disabled"
	end if
	
End if


rs.close
set rs=nothing

SqlSettori = " SELECT ID_SETTORE,DENOMINAZIONE FROM SETTORI ORDER BY DENOMINAZIONE"

'PL-SQL * T-SQL  
SQLSETTORI = TransformPLSQLToTSQL (SQLSETTORI) 
Set Rs = cc.Execute (SqlSettori)
	if not rs.eof then Settori = Rs.getrows()
Set rs=Nothing

TIPOLOGIA = request("TIPOLOGIA")

			Select Case TIPOLOGIA
				case "ESPRO"
				CondizTespr = " AND substr(VALORE,2,1) = 'N'" 
				titolo = "Experiencia Laboral"
				sottotitolo= "Modificaci�n de las experiencias laborales ya registradas. <br>Seleccionar una para " & _
				"modificar la informaci�n relacionada y presionar <b>Enviar</b> para guardar la modificaci�n. "
				case "STAGE"
				CondizTespr = " AND substr(VALORE,2,1) = 'S'" 
				titolo = "Formaci�n, Pr�cticas, Aprendizaje"
				sottotitolo= "Modificaci�n de la formaci�n, pr�cticas y per�odos de aprendizaje ya registrados. <br>Seleccionar uno para " & _
				"modificar la informaci�n relacionada y presionar <b>Enviar</b> para guardar la modificaci�n. "
				case else
				titolo = "Experiencia Profesional"
				sottotitolo= "Modificaci�n de la experiencia profesional ya registrada. <br>Seleccionar una para " & _
				"modificar la informaci�n relacionada y presionar <b>Enviar</b> para guardar la modificaci�n. "
			End select
%>
<center>
<table border="0" width="500" CELLPADDING="0" cellspacing="0">
	<tr class="tblcomm">
		<td align="left" class="sfondomenu">
			<span class="tbltext0"><b><%=Ucase(titolo)%></b></span>
		</td>
		<td width="25" valign="bottom" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">
		<img border="0" src="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
		<td width="200" valign="bottom" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">&nbsp;</td>
	</tr>
	</table>
<table border="0" width="500" CELLPADDING="0" cellspacing="0">
	
    <tr>
		<td class="sfondocomm" align="left" class="tbltext1">
			<%=sottotitolo%>
			</td>
		<td class="sfondocomm">
			<%select case TIPOLOGIA
				case "ESPRO"
					Response.Write "<a onmouseover=""javascript:window.status=' '; return true;"" href=""Javascript:Show_Help('/Pgm/Help/BilancioCompetenze/Competenze/Espro/Dett_Espro')"">"
					Response.Write "<img src='" & Session("Progetto")& "/images/Help.gif' border='0'  align=right alt='Help'>"
					Response.Write "</a>"
				case "STAGE"
					Response.Write "<a onmouseover=""javascript:window.status=' '; return true;"" href=""Javascript:Show_Help('/Pgm/Help/BilancioCompetenze/Competenze/Espro/Dett_Espro')"">"
					Response.Write "<img src='" & Session("Progetto")& "/images/Help.gif' border='0'  align=right alt='Help'>"
					Response.Write "</a>"
				case else
					Response.Write "<a onmouseover=""javascript:window.status=' '; return true;"" href=""Javascript:Show_Help('/Pgm/Help/BilancioCompetenze/Competenze/Espro/Dett_Espro')"">"
					Response.Write "<img src='" & Session("Progetto")& "/images/Help.gif' border='0'  align=right alt='Help'>"
					Response.Write "</a>"
			end select%>
		</td> 
    </tr>

	<tr height="2">
		<td class="sfondocomm" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif">
		</td>
	</tr>
</table>
<center>
	<form name="Dett_Espro" Action="Cnf_Espro.asp" Method="post" OnSubmit="return valida()">
		<input type="hidden" value="<%=Request("Id_Espro")%>" name="Id_Espro">
		<input type="hidden" name="IND_FASE" value="<%=Request("IND_FASE")%>">
		<input type="hidden" name="LivMenu" value="<%=Request("LivMenu")%>">	
		<input type="hidden" name="TIPOLOGIA" value="<%=Request("TIPOLOGIA")%>">
		<input type="hidden" value="<%=Request("back")%>" name="back">
		<input type="hidden" name="IdPers" value="<%=sIdPers%>">
		<input type="hidden" size="50" name="txtoggi" value="<%=ConvDateToString(Date())%>">
		<input type="hidden" size="50" name="DT_TMST" value="<%=DT_TMST%>">
		<input type="hidden" size="50" name="DataNascita" value="<%=DT_NASC%>">
		<input type="hidden" name="txtIdSede" value="<%=idSede%>">
		<table border="0" width="500" cellspacing="1" cellpadding="2">
		<%
		
		Response.Write "<tr height='20'>"
		Response.Write "<td class='tbltext1'><b>Iniciado el*</b>(dd/mm/aaaa)&nbsp;</td>"
		Response.Write "<td>"

	'Response.Write "<input type='text' style='TEXT-TRANSFORM: uppercase' class='textblacka' name='txtDataInizio' size='10' value='" & ConvDateToString(DataInizio) &  "' maxlength='10'>"
	Response.Write "<input type='text' style='TEXT-TRANSFORM: uppercase' class='textblacka' name='txtDataInizio' size='10' value='" & DataInizio &  "' maxlength='10'>"

	Response.Write "</td>"
	Response.Write "</tr>"
	
	
		Response.Write "<tr height=8>"
			Response.Write "<td>"
			Response.Write "</td>"
		Response.Write "</tr>"

		Response.Write "<tr>"
			Response.Write "<td colspan='2' height='2' bgcolor='#3399cc'><td>"
		Response.Write "</tr>"
	
		Response.Write "<tr height='15'>"
			Response.Write "<td colspan='2'><td>"
		Response.Write "</tr>"
	
		Response.Write "<tr>"
			Response.Write "<td colspan='2'>"
			Response.Write "<span class='textblack'>"
			Response.Write "En el caso que la experiencia este en curso no completar el campo <b>Finalizado el</b><br>y&nbsp;<b>Motivo de Finalizaci�n</b>."
			Response.Write "&nbsp;De otra manera completar obligatoriamente ambos."
			Response.Write "</span>"
			Response.Write "</td>"
		Response.Write "</tr>"
		
		Response.Write "<tr heigth='15'>"
			Response.Write "<td>"
			Response.Write "</td>"
		Response.Write "</tr>"
		Response.Write "<tr>"
			Response.Write "<td class='tbltext1' ><b>Finalizado el</b><br>(dd/mm/aaaa)</td>"
			Response.Write "<td align=left>"
				
			Response.Write "<input type='text' style='TEXT-TRANSFORM: uppercase' class='textblacka' name='txtDataFine' size='10' maxlength='10' value='" & DataFine & "'>"

		'	Response.Write "<input type='text' style='TEXT-TRANSFORM: uppercase' class='textblacka' name='txtDataFine' size='10' maxlength='10' value='" 
		'	if trim(replace(ConvDateToString(DataFine),"/","")) <> "" then
		'		Response.Write trim(ConvDateToString(DataFine))
		'		'Response.Write ConvDateToString(DataFine)
		'		end if
			
		'	Response.Write  "'>"
			Response.Write "</td>"
		Response.Write "</tr>"


		Response.Write "<tr>"
			Response.Write "<td  class='tbltext1'><b>Motivo de Finalizaci�n"
			Response.Write "</td>"
			Response.Write "<td>"
					EMotivoCess = "MCESS|0|" & date & "|" & MOT_CESS & "|Mcess|ORDER BY DESCRIZIONE"			
					CreateCombo(EMotivoCess)
			Response.Write "</td>"
		Response.Write "</tr>"

			Response.Write "<tr height='15'>"
				Response.Write "<td>"
				Response.Write "</td>"
			Response.Write "</tr>"
		
			Response.Write "<tr>"
				Response.Write "<td colspan='2' height='2' bgcolor='#3399cc'><td>"
			Response.Write "</tr>"
			Response.Write "<tr height='15'>"
				Response.Write "<td colspan='2'><td>"
			Response.Write "</tr>"
			
	
	Response.Write "<tr>"
	Response.Write "<td colspan='3'>"
	Response.Write "<span class='textblack'>"

	if IdSede = "" then
		Response.Write "Completando el campo <b>Descripci�n</b> y presionando el boton <b>Busqueda</b><br>"
		Response.Write "se puede acceder a la empresa deseada"
	end if
	
	Response.Write "</span>"
	Response.Write "</td>"
	Response.Write "</tr>"
	
	Response.Write "<tr>"
	Response.Write "<td class=tbltext1><b>Tipolog�a de Experiencia* &nbsp;</b></td>"
	Response.Write "<td class=tbltext1>"
	
	EEsperienza = "TESPR|0|" & date & "|" & COD_ESPERIENZA & "|CmbCOD_ESPERIENZA' onChange='return ActivInput(this.value);'|"& CondizTespr &" ORDER BY DESCRIZIONE"			
					CreateCombo(EEsperienza)
	
	Response.Write "</td>"
	Response.Write "</tr>"
	
	
	Response.Write "<tr>"
	Response.Write "<td class=tbltext1><b><br><br><br>Descripci�n &nbsp;</b></td>"
	if IdSede = "" then
		Response.Write "<td valign='top'>"
		Response.Write "<input type=text style='text-transform: uppercase; width:300px;'  class='textblacka'  value='" & RAGIONESOCIALE & "' name='Rag_Soc' maxlength='50' " & DisabilitaRAGIONESOCIALE & " onkeypress='javascript:Pulisci()'>"
		
		%>
		<!--<td align="left">-->
			<a href="javascript:RicercaSede()">
				&nbsp;&nbsp;&nbsp;<img src="<%=Session("Progetto")%>/images/lente.gif" border="0">
			</a>		
		<!--</td> -->
		<%
		
		'''' estaba arriba del tag a!
		Response.Write "</td>"
		
	else
		Response.Write "<td class=tbltext><b>"
		Response.Write "<input type=hidden  value='" & RAGIONESOCIALE & "' name='Rag_Soc'>"
		Response.write Ucase(RAGIONESOCIALE)
		Response.Write "</b></td>"
	end if	
	
	Response.Write "</tr>"
	
	IF IsArray(settori) THEN
		Response.Write "<tr  height='20'>"
		Response.Write "<td class='tbltext1'><b>Sector de la Actividad*</td>"
		if IdSede = "" then
			Response.Write "<td>"
			Response.Write "<SELECT class='textblack' name='Settori'>"
			Response.Write "<option value=''></option>"
			for x = 0 to ubound(settori,2)
				Response.Write "<option value='" & settori(0,x) & "'" 
				if trim(ID_SETTORE) = trim(settori(0,x)) then Response.Write "SELECTED"
				Response.Write ">" & left(settori(1,x),50) & "</option>"
			next
			Response.Write "</SELECT>"
			Response.Write "</td>"
			Response.Write "</tr>"
		else
			Response.Write "<td class=tbltext><b>"
			for x = 0 to ubound(settori,2)
				if trim(ID_SETTORE) = trim(settori(0,x)) then
					Response.Write Ucase(left(settori(1,x),50))
					Response.Write "<input type=hidden  value='" & trim(settori(0,x)) & "' name='Settori'>"
				end if
			next
			Response.Write "</b></td>"
		end if
	END IF

	Response.Write "<tr>"
	Response.Write "<td class=tbltext1><b>Forma Jur�dica &nbsp;</b></td>"
	if IdSede = "" then
		Response.Write "<td class=tbltext1>"
		EFormaGiuridica = "FGIUR|0|" & date & "|" & COD_FORMA & "|CmbCOD_FORMA'"& DisabilitaCmbCOD_FORMA &" width='10 |ORDER BY DESCRIZIONE"
		CreateCombo(EFormaGiuridica)
		Response.Write "</td>"
	else
		Response.Write "<td class=tbltext><b>"
		Response.Write DecCodVal("FGIUR","0","",COD_FORMA,"")
		Response.Write "<input type=hidden  value='" & COD_FORMA & "' name='cmbCOD_FORMA'>"					
		Response.Write "</b></td>"		
	end if
	Response.Write "</tr>"
	
	Response.Write "<tr>"
	Response.Write "<td class=tbltext1><b>Dimensi�n de la Empresa &nbsp;</b></td>"
	
	if IdSede = "" then
		Response.Write "<td class=tbltext1>"
		EDimensioneImpresa = "DIMAZ|0|" & date & "|" & DIMENSIONE & "|CmbDimaz'"& DisabilitaCmbDimaz &" width='10|ORDER BY CODICE"			
		CreateCombo(EDimensioneImpresa)
		Response.Write "</td>"
	else
		Response.Write "<td class=tbltext><b>"
		Response.Write DecCodVal("DIMAZ","0","",DIMENSIONE,"")
		Response.Write "<input type=hidden  value='" & DIMENSIONE & "' name='cmbDimaz'>"
		Response.Write "</b></td>"		
	end if
	
	Response.Write "</tr>"

	Response.Write "<tr>"
	Response.Write "<td class='tbltext1' align='LEFT'><b>C�digo Istat&nbsp;</b></td>"
	Response.Write "<td colspan='2' class=tbltext><B>"
	Response.Write sCod_Istat 
'	Response.Write "<input type='text' class='MyTextBox' maxlength='15' size='15' name='Mansione' readonly value=" & sCod_Istat & ">"
	Response.Write "</B></td>"
	Response.Write "</tr>"
	
		Response.Write "<tr>"
			Response.Write "<td class=tbltext1><b>Funci�n </b></td>"
			Response.Write "<td class=tbltext><b>"
			Response.write Ucase(replace(MANSIONE,"�","'",1))
			Response.Write "</b></td>"
		Response.Write "</tr>"
	
		Response.Write "<tr  height='20'>"
			Response.Write "<td class='tbltext1'><b>Indicar si tiene personas a su cargo</td>"
			Response.Write "<td colspan='2'>"
			Response.Write "<input "
			if RESPONPERS = "S" then
				Response.Write " checked "
			end if
			Response.write "Name='resp_pers' type='checkbox' value='S'>"
			Response.Write "</td>"
		Response.Write "</tr>"
		
		Response.Write "<tr height='15'>"
			Response.Write "<td colspan='2'><td>"
		Response.Write "</tr>"

		Response.Write "<tr>"
			Response.Write "<td colspan='2' height='2' bgcolor='#3399cc'><td>"
		Response.Write "</tr>"
	
		Response.Write "<tr height='15'>"
			Response.Write "<td colspan='2'><td>"
		Response.Write "</tr>"
		
		Response.Write "<tr>"
			Response.Write "<td colspan='2'>"
			Response.Write "<span class='textblack'>"
			Response.Write "SI se conoce, indicar el Contrato Colectivo Nacional Aplicado"
			Response.Write " y el nivel de aplicaci�n."
			Response.Write "</span>"
			Response.Write "</td>"
		Response.Write "</tr>"
		
		Response.Write "<tr heigth='15'>"
			Response.Write "<td>"
			Response.Write "</td>"
		Response.Write "</tr>"	


		Response.Write "<tr>"
			Response.Write "<td class=tbltext1><b>Contrato Nacional &nbsp;</b></td>"
			Response.Write "<td class=tbltext1>"	

		EContrattoNazionale = "TCCNL|0|" & date & "|" & CONTRATTONAZIONALE  & "|CmbTccnl|ORDER BY DESCRIZIONE"			
					CreateCombo(EContrattoNazionale)
			'Response.write replace(CONTRATTONAZIONALE,"�","'",1)
			Response.Write "</td>"
		Response.Write "</tr>"
		Response.Write "<tr>"
			Response.Write "<td class=tbltext1><b>Encuadramiento &nbsp;</b></td>"
			Response.Write "<td class=tbltext1>"	
		EContrattoLivello = "LCCNL|0|" & date & "|" & CONTRATTOLIVELLO  & "|CmbLccnl|ORDER BY DESCRIZIONE"			
		CreateCombo(EContrattoLivello)	
	
		'Response.write replace(CONTRATTOLIVELLO,"�","'",1)
			Response.Write "</td>"
		Response.Write "</tr>"
		
			Response.Write "<tr>"
				Response.Write "<td colspan='2' height='2' bgcolor='#3399cc'><td>"
			Response.Write "</tr>"
		
		Response.Write "<tr height='15'>"
			Response.Write "<td colspan='2'><td>"
		Response.Write "</tr>"
		
		Response.Write "<tr>"
			Response.Write "<td colspan='2'>"
			Response.Write "<span class='textblack'>"
			Response.Write "Solo en el caso de un trabajador aut�nomo o profesional independiente indicar la Tipolog�a Principal de usuario."
			Response.Write "</span>"
			Response.Write "</td>"
		Response.Write "</tr>"
		
		Response.Write "<tr heigth='15'>"
			Response.Write "<td colspan='2'>"
			Response.Write "</td>"
		Response.Write "</tr>"
		

		Response.Write "<tr>"
			Response.Write "<td class=tbltext1><b>Tipolog�a Cliente &nbsp;	</b></td>"
			Response.Write "<td class=tbltext1>"	

			ETipoCLiente = "TICLI|0|" & date & "|" & TIPOCLIENTE  & "|CmbTicli'"& DisabilitaCmbTicli &" width='10|ORDER BY DESCRIZIONE"			
					CreateCombo(ETipoCLiente)
			Response.Write "</td>"
		Response.Write "</tr>"
		
		if TIPOLOGIA = "ESPRO" then
			Response.Write "<tr  height='20'>"
			Response.Write "<td class='tbltext1'><b>Responsabilidad de la Persona</td>"
			Response.Write "<td bgcolor='#ffffff'>"
			Response.Write "<input Name='resp_pers' type='checkbox' value='S' " 
			if  RESPONPERS ="S" then Response.Write "CHECKED"
			Response.Write ">"
			Response.Write "</td>"
			Response.Write "</tr>"
		end if 
%>
<tr>
	<td colspan="2">&nbsp;</td>
<tr>
	
<tr>		
	<td align="middle" colspan="2">
		<a href="javascript:HideVariables('Espro.asp','Indietro')" onmouseover="javascript: window.status=' '; return true"><img alt="Indietro" border="0" src="<%=Session("Progetto")%>/images/indietro.gif"></a>
		 <% '
			' Verifica che l'utente sia stato certificato
			'
			bControllo =Controlla(sIdpers)
			
	    	if (Session("tipopers")="P" and clng(Session("Creator"))=clng(sIdpers)) then
			    if bControllo = false then%>
					<input type="image" src="<%=Session("Progetto")%>/images/conferma.gif" border="0" value="Salva">
					<a onmouseover="window.status =' '; return true" title="Elimina" href="javascript:elimina()"><img border="0" src="<%=Session("Progetto")%>/images/elimina.gif"></a>
		 <%     end if
		    else %>
		        <input type="image" src="<%=Session("Progetto")%>/images/conferma.gif" border="0" value="Salva" id="image1" name="image1">
				<a onmouseover="window.status =' '; return true" title="Elimina" href="javascript:elimina()"><img border="0" src="<%=Session("Progetto")%>/images/elimina.gif"></a>
		 <% end if %>   
	</td>
</tr>

	
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
</table>
</center>
</form>
<form name="Indietro" Action="Espro.asp" Method="post">
	<input type="hidden" name="IdPers" value="<%=sIdPers%>">
	<input type="hidden" value="<%=Request("IND_FASE")%>" name="IND_FASE">
	<input type="hidden" value="<%=Request("LivMenu")%>" name="LivMenu">
	<input type="hidden" name="TIPOLOGIA" Value="<%=Request("TIPOLOGIA")%>">
</form>


<!--#include Virtual = "/include/closeconn.asp"-->
<!-- #include Virtual="/strutt_coda2.asp" -->
