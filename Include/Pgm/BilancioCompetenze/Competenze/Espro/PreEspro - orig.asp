<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #Include Virtual="/strutt_testa2.asp" -->
<!-- #include Virtual = "/Include/DecCod.asp" -->
<!-- #include Virtual = "/Include/ControlDateVb.asp" -->
<!-- #Include Virtual="/Pgm/BilancioCompetenze/Utils.asp" -->
<!--#include Virtual = "/Include/OpenConn.asp"-->
<!-- #include Virtual = "/Include/ControlVall.asp" -->



<script language="Javascript">

	function Enviar(persona)
	{
		//alert(persona);
		if ((document.FrmPreEspro.RespEspro.value == "S") || (document.FrmPreEspro.RespEspro[0].checked))
		{
			//alert("Pasa por aca 1")
			FrmPreEspro.Idpers.value = persona;
			FrmPreEspro.Respuesta.value = 'S';
			FrmPreEspro.submit()
		}
		else 
		{
			if (document.FrmPreEspro.RespEspro[1].checked)
			{
			    //alert("Pasa por aca 2")
				FrmPreEspro.action = "/Pgm/BilancioCompetenze/Competenze/Espro/Cnf_PreEspro.asp";
				FrmPreEspro.Idpers.value = persona;
				FrmPreEspro.Respuesta.value = 'N';
				FrmPreEspro.submit()
			}
			else
			{
				alert("Debe responder esta pregunta")
			}
		}
		//alert(result)
	}
</script>


<%
TIPOLOGIA = request("TIPOLOGIA")
INTEST = "Experiencia Laboral"
sIdpers = request("Idpers")

' Introduced by AEM
if sIdpers="" then
   sIdpers=Session("Creator")
end if

'Response.Write "sIdpers: " & sIdpers

sqlEspro = "SELECT antecedente_laboral FROM datos_obligatorios WHERE antecedente_laboral IS NOT NULL AND id_persona = " & sIdpers

set RSEsp = Server.CreateObject("ADODB.Recordset")
RSEsp.Open sqlEspro,CC,1,2
sRespuesta = ""
sTipoOp = "INS"
if not (RSEsp.EOF or RSEsp.BOF) then
	sTipoOp = "MOD"
	sRespuesta = RSEsp.Fields(0).Value 
end if
RSEsp.Close 
set RSEsp = nothing


if sRespuesta = "S" then
	'Response.Write " PASA POR ACA 1"
	%>
	<form name="FrmPreEspro" action="/Pgm/BilancioCompetenze/Competenze/Espro/Espro.asp" method="post">
		<input type="hidden" name="IND_FASE" value="0">
		<input type="hidden" name="TIPOLOGIA" Value="<%=Request("TIPOLOGIA")%>">
		<input type="hidden" name="TipoOp" Value="<%=sTipoOp%>">
		<input type="hidden" name="Idpers" value>
		<input type="hidden" name="Respuesta" value>
		<input type="hidden" name="RespEspro" value="S">
		<img border="0" onload="javascript:Enviar(<%=sIdpers%>)" src="<%=Session("Progetto")%>/images/spacer.gif">

		<table border="0" width="500" CELLPADDING="0" cellspacing="0">
			<tr>
				<td align="left" class="sfondomenu">
					<span class="tbltext0"><b>&nbsp;<%=ucase(INTEST)%></b></span>
				</td>
				<td width="25" valign="bottom" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">
					<img border="0" src="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
				<td width="200" valign="bottom" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">&nbsp;</td>
			</tr>
		</table>
		<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
	</form>	
	<%
    else
	'Response.Write " PASA POR ACA 2"
	%>

	<form name="FrmPreEspro" action="/Pgm/BilancioCompetenze/Competenze/Espro/Espro.asp" method="post">
		<input type="hidden" name="IND_FASE" value="0">
		<input type="hidden" name="TIPOLOGIA" Value="<%=Request("TIPOLOGIA")%>">
		<input type="hidden" name="TipoOp" Value="<%=sTipoOp%>">
		<input type="hidden" name="Idpers" value>
		<input type="hidden" name="Respuesta" value>
		<table border="0" width="500" CELLPADDING="0" cellspacing="0">
			<tr>
				<td align="left" class="sfondomenu">
					<span class="tbltext0"><b>&nbsp;<%=ucase(INTEST)%></b></span>
				</td>
				<td width="25" valign="bottom" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">
					<img border="0" src="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
				<td width="200" valign="bottom" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">&nbsp;</td>
			</tr>
		</table>

		<table border="0" width="500" CELLPADDING="0" cellspacing="0">
			<tr>
				<td class="sfondocomm" align="left" class="tbltext1">
					<b>¿Posee experiencia laboral en empresas, instituciones, programas de empleo, trabajo dom&eacute;stico, trabajo voluntario?</b>	

   					<!--NUEVO AGREGADO POR F.BASANTA -->
					<br>
					<img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0" onclick="Javascript:window.open('ayuda_antecedenteslaborales.htm','Ayuda','width=500,height=350,Resize=No,Scrollbars=yes')">

						
				</td> 
			</tr>
			<tr height="2">
				<td class="sfondocomm" width="100%" colspan="2" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
			</tr>
		</table>		
		<br>

		<table border="0" width="500" CELLPADDING="0" cellspacing="0">
			<tr>
				<td width="210" class="tbltext1">
					<b>¿Posee Experiencia Laboral? *</b>
				</td>
				<td class="textblack">
					<input type="radio" name="RespEspro" value="S" <%if sRespuesta = "S" then Response.Write "checked"%>>SI
					<br>
					<input type="radio" name="RespEspro" value="N" <%if sRespuesta = "N" then Response.Write "checked"%>>NO
				</td>
			</tr>
		</table>		
		<br>
		<table width="500" cellspacing="0" cellpadding="0" border="0" align="center">
			<tr>
				<td  align="center">
					<a href="javascript:Enviar(<%=sIdpers%>);"><img src="<%=Session("Progetto")%>/images/conferma.gif" border="0" align="absBottom"></a> 
				</td>
			</tr>
		</table>
	</form>

	<%
end if
%>




<!--#include Virtual = "/include/CloseConn.asp"-->
<!--#include Virtual = "/strutt_coda2.asp"-->
