<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #INCLUDE FILE="../../Utils.asp" -->
<%

Set Conn = server.CreateObject ("ADODB.Connection")
Conn.open strConn
TIPOLOGIA = REQUEST("TIPOLOGIA")

OraDate = ConvDateToDB(Now())

'##### SALVA IL CASO CAPACITA'

if request("Tipo") = "CAPACITA" then
Conn.BeginTrans
for x = Int(request("Ini")) to Int(request("Limit"))
	LstCampi = "ID_PERSONA,GRADO_CAPACITA,ID_CAPACITA,DT_TMST,IND_STATUS"
 
	LstVal = ""
	LstVal = LstVal & IDP & "|"
	LstVal = LstVal & request("Grado" & x) & "|"
	LstVal = LstVal & request("ID" & x) & "|"
	LstVal = LstVal & OraDate 
	LstVal = LstVal & "|'0'"
	Sql = ""
	if request("exist" & x) = "True" then
		Sql = QryUpd("PERS_CAPAC",LstCampi,LstVal)
		Sql = Sql & " WHERE "
		Sql = Sql & "PERS_CAPAC.ID_PERSONA = " & IDP & " AND "
		Sql = Sql & "PERS_CAPAC.ID_CAPACITA = " & request("ID" & x)
	else
		Sql = QryIns("PERS_CAPAC",LstCampi,LstVal)
	end if
	
	if request("Grado" & x) <> "" then

'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
		Conn.Execute Sql
	else
		Sql = ""
		Sql = Sql & "DELETE PERS_CAPAC "
		Sql = Sql & " WHERE "
		Sql = Sql & " PERS_CAPAC.ID_PERSONA = " & IDP & " AND "
		Sql = Sql & " PERS_CAPAC.ID_CAPACITA = " & request("ID" & x)

'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
		Conn.Execute Sql		
	end if
next
Conn.CommitTrans
end if
'##### END CASO CAPACITA'


'##### SALVA IL CASO COMPORTAMENTI
if request("Tipo") = "COMPORTAMENTI" then
Conn.BeginTrans
for x = Int(request("Ini")) to Int(request("Limit"))
	LstCampi = "ID_PERSONA,GRADO_PERS_COMPOR,ID_COMPORTAMENTO,DT_TMST,IND_STATUS"

	LstVal = ""
	LstVal = LstVal & IDP & "|"
	LstVal = LstVal & request("Grado" & x) & "|"
	LstVal = LstVal & request("ID" & x) & "|"
	LstVal = LstVal & OraDate 
	LstVal = LstVal & "|'0'"

	Sql = ""
	if request("exist" & x) = "True" then
		Sql = QryUpd("PERS_COMPOR",LstCampi,LstVal)
		Sql = Sql & " WHERE "
		Sql = Sql & " PERS_COMPOR.ID_PERSONA = " & IDP & " AND "
		Sql = Sql & " PERS_COMPOR.ID_COMPORTAMENTO = " & request("ID" & x)
	else
		Sql = QryIns("PERS_COMPOR",LstCampi,LstVal)
	end if
	
	if request("Grado" & x) <> "" then
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
		Conn.Execute Sql
	else
		Sql = ""
		Sql = Sql & "DELETE PERS_COMPOR "
		Sql = Sql & " WHERE "
		Sql = Sql & " PERS_COMPOR.ID_PERSONA = " & IDP & " AND "
		Sql = Sql & " PERS_COMPOR.ID_COMPORTAMENTO = " & request("ID" & x)
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
		Conn.Execute Sql		
	end if
next
Conn.CommitTrans
end if
'##### END CASO COMPORTAMENTI

'##### SALVA IL CASO CONOSCENZE
if request("Tipo") = "CONOSCENZE" then
Conn.BeginTrans
for x = Int(request("Ini")) to Int(request("Limit"))
	LstCampi = "ID_PERSONA, COD_GRADO_CONOSC, DT_TMST, DT_DICH_CONOSC, ID_CONOSCENZA, IND_STATUS"

	LstVal = ""
	LstVal = LstVal & IDP & "|"
	LstVal = LstVal & request("Grado" & x) & "|"
	LstVal = LstVal & OraDate & "|"
	LstVal = LstVal & OraDate & "|"
	LstVal = LstVal & request("ID" & x) & "|'0'"

	Sql = ""
	if request("exist" & x) = "True" then
		Sql = QryUpd("PERS_CONOSC",LstCampi,LstVal)
		Sql = Sql & " WHERE "
		Sql = Sql & "PERS_CONOSC.ID_PERSONA = " & IDP & " AND "
		Sql = Sql & "PERS_CONOSC.ID_CONOSCENZA = " & request("ID" & x)
	else
		Sql = QryIns("PERS_CONOSC",LstCampi,LstVal)
	end if
	
	if request("Grado" & x) <> "" then
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
		Conn.Execute Sql
	
	else
		Sql = ""
		Sql = Sql & "DELETE PERS_CONOSC "
		Sql = Sql & " WHERE "
		Sql = Sql & "PERS_CONOSC.ID_PERSONA = " & IDP & " AND "
		Sql = Sql & "PERS_CONOSC.ID_CONOSCENZA = " & request("ID" & x)
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
		Conn.Execute Sql		
	end if
next
Conn.CommitTrans
end if
'##### END CASO CONOSCENZE

Conn.Close
Set conn = Nothing

URL = request("back") & "?TIPOLOGIA=" & request("TIPOLOGIA") & "&Backpage=" & Request("Backpage")

%>

<form  name="Move" action="<%=request("back")%>" method="post">
<input type='hidden' name='Limit' Value='<%=Request("Limit")%>'>
<input type='hidden' name='Ini' Value="<%=Request("Ini")%>">
<input type="hidden" name="Back" value="<%=Request("Back")%>">
<input type=hidden name="TIPOLOGIA" value="<%=TIPOLOGIA%>" > 
<input type=hidden name="RICERCA" value="<%=RICERCA%>" > 
<input type=hidden name="tipo" value="<%=Tipo%>" > 
<input type=hidden name="CONOSCENZE" value="<%=request("CONOSCENZE")%>" > 
<input type=hidden name="AREA_CONOSCENZA" value="<%=request("AREA_CONOSCENZA")%>" > 
<input type=hidden name="CAPACITA" value="<%=request("CAPACITA")%>" > 
<input type=hidden name="COMPORTAMENTI" value="<%=request("COMPORTAMENTI")%>" > 
<input type=hidden name="AREA_COMPORTAMENTI" value="<%=request("AREA_COMPORTAMENTI")%>" > 
<input type=hidden name="AREA_CAPACITA" value="<%=request("AREA_CAPACITA")%>" > 
<input type=hidden name="AREE_PROFESSIONALI" value="<%=request("AREE_PROFESSIONALI")%>" > 
<input type="hidden" name="IdPers" value="<%=sIdPers%>">
<input type="hidden" name="IND_FASE" value="<%=IND_FASE%>">
<input type="hidden" name="LivMenu" value="<%=LivMenu%>">
</form>

<%

'if request("Avanti") <> "" then
	'URL = request("DETTAGLI") & "?"
	'URL = URL & "Limit=" & request("Limit") & "&"
	'URL = URL & "Back=" & request("Back") & "&"
	'URL = URL & "TIPOLOGIA=" & request("TIPOLOGIA") & "&"
	'URL = URL & "RICERCA=" & request("RICERCA") & "&"
	'URL = URL & "CONOSCENZE=" & request("CONOSCENZE") & "&"
	'URL = URL & "AREA_CONOSCENZA=" & request("AREA_CONOSCENZA") & "&"
	'URL = URL & "CAPACITA=" & request("CAPACITA") & "&"
	'URL = URL & "AREA_CAPACITA=" & request("AREA_CAPACITA") & "&"
'	URL = URL & "COMPORTAMENTI=" & request("COMPORTAMENTI") & "&"
'	URL = URL & "AREA_COMPORTAMENTI=" & request("AREA_COMPORTAMENTI")& "&"
'	URL = URL & "AREE_PROFESSIONALI=" & Request("AREE_PROFESSIONALI")& "&"
'	URL = URL & "IdPers=" & request("IdPers")  & "&"
'	URL = URL & "LivMenu=" & request("LivMenu")  & "&"
'	URL = URL & "IND_FASE=" & Request("IND_FASE")  & "&"
'	URL = URL & "Backpage=" & Request("Backpage")

'End if

'if request("Indietro") <> "" then
'	URL = request("DETTAGLI") & "?"
'	URL = URL & "Limit=" & Request("Ini")-10 & "&"
'	URL = URL & "Back=" & request("Back") & "&"
'	URL = URL & "TIPOLOGIA=" & request("TIPOLOGIA") & "&"
'	URL = URL & "RICERCA=" & request("RICERCA") & "&"
'	URL = URL & "CONOSCENZE=" & request("CONOSCENZE") & "&"
'	URL = URL & "AREA_CONOSCENZA=" & request("AREA_CONOSCENZA") & "&"
'	URL = URL & "CAPACITA=" & request("CAPACITA") & "&"
'	URL = URL & "AREA_CAPACITA=" & request("AREA_CAPACITA") & "&"
'	URL = URL & "COMPORTAMENTI=" & request("COMPORTAMENTI") & "&"
'	URL = URL & "AREA_COMPORTAMENTI=" & request("AREA_COMPORTAMENTI") & "&"
'	URL = URL & "AREE_PROFESSIONALI=" & Request("AREE_PROFESSIONALI") & "&"
'	URL = URL & "IdPers=" & request("IdPers")  & "&"
'	URL = URL & "LivMenu=" & request("LivMenu")  & "&"
'	URL = URL & "IND_FASE=" & Request("IND_FASE")  & "&"
'	URL = URL & "Backpage=" & Request("Backpage")
'End if

%>

<script language=javascript>
<!--
// La pagina di salvataggio � richiamata da pi� applicazioni: 
// Pgm/BilancioCompetenze/Competenze/Analisi/Dettagli_Lav_Prof.asp
// Pgm/BilancioCompetenze/Competenze/Analisi/Dettagli.asp 
// pertanto l'url della pagina di ritorno
// viene determinata rispetto ai seguenti parametri
// MMagnani - aggiustamenti supplementari in occasione richiesta intervento giugno/03

Move.action = "<%=request("back")%>"	
<%
	if request("Indietro")<> "" then 
	valLimit = Request("Ini")-10
	%>
	Move.action = "<%=request("DETTAGLI")%>"
	Move.Limit.value = <%=valLimit%>
	<%
	end if
	
	if request("Avanti") <> "" then
	valLimit = Request("Limit")+1
	%>
	Move.action = "<%=request("DETTAGLI")%>"
	Move.Limit.value = <%=valLimit%>
	<%
	end if
	
	if request("Salva") <> "" then
	%>	
	Move.action = "<%=Request("Back")%>"
	<%
	end if 
%>

Move.submit()	
//-->
</script>

