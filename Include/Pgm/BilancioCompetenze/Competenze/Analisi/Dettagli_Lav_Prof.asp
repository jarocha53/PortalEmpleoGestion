<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #Include Virtual="/strutt_testa2.asp" -->
<!-- #INCLUDE FILE="../../Utils.asp" -->

<%

DIM V2


TIPOLOGIA = request("tipologia")
Tipo = ""
Set Conn = server.CreateObject ("ADODB.Connection")
Conn.open strConn
'Elenco delle Figure Professionali Censite con la Medesima Classificaizone ISTAT
Sql=""
'Sql = Sql & "SELECT FIGUREPROFESSIONALI.ID_FIGPROF FROM FIGUREPROFESSIONALI"
'Sql = Sql & " WHERE COD_EJ ='" & request("ID_FIGPROF") & "' and IND_MAPPA='S'"
Sql = Sql & "SELECT FIGUREPROFESSIONALI.ID_FIGPROF FROM FIGUREPROFESSIONALI"
Sql = Sql & " WHERE ID_FIGPROF ='" & request("ID_FIGPROF") & "' and IND_MAPPA='S'"

'Response.Write SQL
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
Set NrFP = conn.Execute (Sql)

if not NrFP.eof then 
	FPAss= NrFP.GetString(,,,",")
end if 
FPAss= Left(FpAss,len(FPass)-1)
'=================================================================================

if request("CONOSCENZE") <> "" then
	Sql = ""
	Sql = Sql & "SELECT PERS_CONOSC.ID_CONOSCENZA, PERS_CONOSC.COD_GRADO_CONOSC "
	Sql = Sql & "FROM PERS_CONOSC "
	Sql = Sql & "WHERE PERS_CONOSC.ID_PERSONA = " & IDP
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
	Set Rs = Conn.Execute (Sql)
	if not rs.eof then MM = Rs.getrows()
	Set Rs = Nothing

	Tipo = "CONOSCENZE"
	V2 = "CONOCIMIENTO"
	
	Sql = ""
	Sql = Sql & " SELECT CONOSCENZE.ID_CONOSCENZA, CONOSCENZE.DENOMINAZIONE "
	Sql = Sql & " FROM "
	Sql = Sql & " CONOSCENZE, FIGUREPROFESSIONALI, "
	Sql = Sql & " COMPET_FP, COMPETENZE, COMPET_CONOSC "
	Sql = Sql & " WHERE "
	Sql = Sql & " ((COMPET_FP.ID_FIGPROF=FIGUREPROFESSIONALI.ID_FIGPROF) "
	Sql = Sql & " AND (COMPET_FP.ID_COMPETENZA=COMPETENZE.ID_COMPETENZA) "
	Sql = Sql & " AND (COMPET_CONOSC.ID_COMPETENZA=COMPETENZE.ID_COMPETENZA) "
	Sql = Sql & " AND (COMPET_CONOSC.ID_CONOSCENZA=CONOSCENZE.ID_CONOSCENZA)) "
	Sql = Sql & " AND FIGUREPROFESSIONALI.ID_FIGPROF IN (" & FPAss & ")"

	Sql = Sql & " GROUP BY "
	Sql = Sql & "  CONOSCENZE.DENOMINAZIONE, CONOSCENZE.ID_CONOSCENZA"

end if

if request("CAPACITA") <> "" then
	Sql = ""
	Sql = Sql & "SELECT PERS_CAPAC.ID_CAPACITA, PERS_CAPAC.GRADO_CAPACITA "
	Sql = Sql & "FROM PERS_CAPAC "
	Sql = Sql & "WHERE PERS_CAPAC.ID_PERSONA = " & IDP
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
	Set Rs = Conn.Execute (Sql)
	if not rs.eof then MM = Rs.getrows()
	Set Rs = Nothing

	Tipo = "CAPACITA"
	V2 = "CAPACIDAD"
	
	Sql = ""
	Sql = Sql & " SELECT CAPACITA.ID_CAPACITA, CAPACITA.DENOMINAZIONE "
	Sql = Sql & " FROM "
	Sql = Sql & " CAPACITA, FIGUREPROFESSIONALI, "
	Sql = Sql & " COMPET_CAPAC, COMPETENZE, COMPET_FP "
	Sql = Sql & " WHERE "
	Sql = Sql & " ((COMPET_FP.ID_FIGPROF=FIGUREPROFESSIONALI.ID_FIGPROF) "
	Sql = Sql & " AND (COMPET_FP.ID_COMPETENZA=COMPETENZE.ID_COMPETENZA) "
	Sql = Sql & " AND (COMPET_CAPAC.ID_COMPETENZA=COMPETENZE.ID_COMPETENZA) "
	Sql = Sql & " AND (COMPET_CAPAC.ID_CAPACITA=CAPACITA.ID_CAPACITA)) "
	Sql = Sql & " AND FIGUREPROFESSIONALI.ID_FIGPROF IN (" & FPAss & ")"
	Sql = Sql & " GROUP BY "
	Sql = Sql & " CAPACITA.DENOMINAZIONE, CAPACITA.ID_CAPACITA "
end if

if request("COMPORTAMENTI") <> "" then
	Sql = ""
	Sql = Sql & "SELECT PERS_COMPOR.ID_COMPORTAMENTO, PERS_COMPOR.GRADO_PERS_COMPOR "
	Sql = Sql & "FROM PERS_COMPOR "
	Sql = Sql & "WHERE PERS_COMPOR.ID_PERSONA = " & IDP
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
	Set Rs = Conn.Execute (Sql)
	if not rs.eof then MM = Rs.getrows()
	Set Rs = Nothing

	Tipo = "COMPORTAMENTI"
	V2 = "COMPORTAMIENTO"
	
	Sql = ""
	Sql = Sql & " SELECT COMPORTAMENTI.ID_COMPORTAMENTO, COMPORTAMENTI.DENOMINAZIONE "
	Sql = Sql & " FROM "
	Sql = Sql & " COMPORTAMENTI, COMPOR_FP, FIGUREPROFESSIONALI "
	Sql = Sql & " WHERE "
	Sql = Sql & " ((COMPORTAMENTI.ID_COMPORTAMENTO = COMPOR_FP.ID_COMPORTAMENTO) AND "
	Sql = Sql & " (COMPOR_FP.ID_FIGPROF = FIGUREPROFESSIONALI.ID_FIGPROF)) "
	Sql = Sql & " AND FIGUREPROFESSIONALI.ID_FIGPROF IN (" & FPAss & ")"
	Sql = Sql & " GROUP BY "
	Sql = Sql & " COMPORTAMENTI.DENOMINAZIONE, COMPORTAMENTI.ID_COMPORTAMENTO "
end if

'Response.Write SQL 

SinDatos = false

if Sql<>"" then
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
	Set Rs = Conn.Execute (Sql)
	if not rs.eof then 
		VV = Rs.getrows()	
	else 
		SinDatos = true 
	end if 
	
	Set Rs = Nothing
	Set Conn = Nothing

	Response.Write "<form name='Salva' action='SalvaDettagli.asp' Method=POST>"
	Response.Write "<input type='hidden' name='IdPers' value='" & Request("IdPers")&  "'>"
	Response.Write "<input type='hidden' name='LivMenu' value='" & Request("LivMenu")&  "'>"
	Response.Write "<input type='hidden' name='IND_FASE' value='" & Request("IND_FASE") & "'>"
	Response.Write "<input type='hidden' name='TIPOLOGIA' value='" & TIPOLOGIA & "'>"
	Response.Write "<input type='hidden' name='VISMENU' value='NO'>"

	Response.Write "<input type='hidden' name='back' value='" & Request("back") & "'>"
	Response.Write "<input type='hidden' name='tipo' value='" & tipo & "'>"
	Response.Write "<input type='hidden' name='ID_AREACONOSCENZA' value='" & request("AREA_CONOSCENZA") & "'>"


Response.Write "<table border=0 width='500' CELLPADDING=0 cellspacing=0>"
		Response.Write "<tr class='tblcomm'>"
	Response.Write "<td  align=left  class=sfondomenu>"
		Select case TIPOLOGIA
			case "ESPRO"
			INTEST = "EXPERIENCIA PROFESIONAL"
			case "STAGE"
			INTEST = "APRENDIZAJES, PASANTIAS"
			case "HOBBY"
			INTEST ="PASATIEMPOS"
			case else
			INTEST = "EXPERIENCIA LABORAL"
		end select 

	
	Response.Write "<span class='tbltext0'><b>" & INTEST & "</b></span>"
	Response.Write "</td>"
	Response.Write "<td width='25' valign='bottom' background='" & Session("Progetto")& "/images/imgConsiel/sfondo_linguetta.gif' >"
	Response.Write "<img border='0' src='" & Session("Progetto") & "/images/imgConsiel/tondo_linguetta.gif'></td>"
	Response.Write "<td width='200' valign='bottom' background='" & Session("Progetto") & "/images/imgConsiel/sfondo_linguetta.gif'>&nbsp;</td>"
		Response.Write "</tr>"
Response.Write "</table>"

Response.Write "<table width='500' CELLPADDING=0 cellspacing=0>"
		Response.Write "<tr>"
	Response.Write "<td class=sfondocomm  align='left' colspan='2' class='tbltext1'>"
	Response.write  replace(Request("FIGPROF"),"'","`") & "&nbsp;"  &  V2 & " - An�lisis del Detalle</b></font>"
	Response.Write "<td class=sfondocomm>"
	
	select case tipo
		
			case "COMPORTAMENTI"
				Response.Write " <a href=""Javascript:Show_Help('/Pgm/Help/BilancioCompetenze/competenze/Analisi/Dettagli_Lav_Prof3.htm')"">"
				Response.Write " <img src='" & Session("Progetto")& "/images/Help.gif' border='0'  align=right alt='Help'>"
				Response.Write " </a>"
			
			case "CAPACITA"
				Response.Write " <a href=""Javascript:Show_Help('/Pgm/Help/BilancioCompetenze/competenze/Analisi/Dettagli_Lav_Prof2.htm')"">"
				Response.Write " <img src='" & Session("Progetto")& "/images/Help.gif' border='0'  align=right alt='Help'>"
				Response.Write " </a>"
			
			case "CONOSCENZE"
				Response.Write " <a href=""Javascript:Show_Help('/Pgm/Help/BilancioCompetenze/competenze/Analisi/Dettagli_Lav_Prof1.htm')"">"
				Response.Write " <img src='" & Session("Progetto")& "/images/Help.gif' border='0'  align=right alt='Help'>"
				Response.Write " </a>"
			case else
		
	end select
		
	Response.Write "</td>"
		Response.Write " </tr>"
		Response.Write "<tr>"
	Response.Write "<td colspan=3 bgcolor='#3399CC'></td>"
	Response.Write "</tr>"
		Response.Write "<tr>"
	Response.Write "<td colspan=3 height='15'></td>"
	Response.Write "</tr>"
Response.Write "</table>"

	
if IsArray(VV) then
	Response.Write "<input type='hidden' value='" & ubound(VV,2) & "' name='Limit'>"
	Response.Write "<table cellspacing=0 cellpadding=0 width='500'>"
	 	Response.Write "<TR class='sfondocomm'>"
		Response.Write "<TD><b>Descripci�n</b></TD>"
		Response.Write "<td align=center ><b>Posesi�n<br> 0 - 1 - 2 - 3 - 4</b></font></td>"
		Response.Write "</tr>"
		
	for x = 0 to ubound(VV,2)
		if (x mod 2)= 0 then 
			sfondo="class='tblsfondo'" 
		else
			sfondo="class='tbltext1'"
		end if


		Response.Write chr(13) & chr(10) & "<tr " & sfondo & ">"
		
			Response.Write "<td class='tbltext1'> &nbsp;" & Ucase(VV(1,x)) & "</td>"
			Response.Write "<td  class='tbltext1' align='center'>"
			Response.write "<input type=hidden name='Id" & x & "' value='" & VV(0,x) & "'>"
			Response.write "<input type=hidden name='exist" & x & "' value='" & (Mettival(MM,VV(0,x),0,0) <> "") & "'>"

				Response.Write "<input type='radio' value='' name='Grado" & x & "'"
					if Int("0" & Mettival(MM,VV(0,x),0,1)) = 0 then Response.Write " CHECKED "
				Response.Write " >"
				Response.Write "<input type='radio' value=1 name='Grado" & x & "'"
					if Int("0" & Mettival(MM,VV(0,x),0,1)) = 1 then Response.Write " CHECKED "
				Response.Write " >"
				Response.Write "<input type='radio' value=2 name='Grado" & x & "'"
					if Int("0" & Mettival(MM,VV(0,x),0,1)) = 2 then Response.Write " CHECKED "
				Response.Write " >"
				Response.Write "<input type='radio' value=3 name='Grado" & x & "'"
					if Int("0" & Mettival(MM,VV(0,x),0,1)) = 3 then Response.Write " CHECKED "
				Response.Write " >"
				Response.Write "<input type='radio' value=4 name='Grado" & x & "'"
					if Int("0" & Mettival(MM,VV(0,x),0,1)) = 4 then Response.Write " CHECKED "
				Response.Write " >"
			Response.Write "</td>"
		Response.Write "</tr>"
	next
	end if
	Response.Write "</table>"
end if

	
Response.Write "<table cellspacing=0 cellpadding=0 width='500'>"
Response.Write "<tr>"
Response.Write "<td colspan=2>"
Response.write "<br>&nbsp;"
	URL = "Espro.asp?" 
	URL = URL & "TIPOLOGIA=" 
	URL = URL & Request("TIPOLOGIA")
Response.Write "<input type='button' value='Volver' OnClick='goToPage(""" & URL & """)' class='My'>"
Response.Write "</td>"
 
	Response.Write "<td align='right'>"
	Response.write "<br>"
	if SinDatos = false then	
	Response.Write "<input type='submit' value='Guardar' class='My'>"
	end if 	
Response.Write "</td>"

Response.Write "</tr>"
Response.write "<tr>"
Response.write "<td colspan=3 bgcolor='#3399CC'></td>"
Response.write "</tr>"
Response.Write "</table>"

Response.Write "</center>"
	
Response.Write "</form>"
%>
<!-- #Include Virtual="/strutt_coda2.asp" -->
