<!-- #INCLUDE FILE="../Utils.asp"-->

<html>
<head>
<title>Competenze</title>
<LINK REL=STYLESHEET TYPE="text/css" HREF="../fogliostile.css">
</head>
<center>
<body bgcolor="#ffffff" text="#000000">

<table border=0 cellspacing=0 cellpadding=0 width='570'>
	<tr>
<TD bgcolor='#f1f3f3' align="middle">
      <P>
<STRONG>
COMPETENZE  </STRONG></P>
</TD>
</tr>
</table>

<table border=0 cellspacing=0 cellpadding=0 width='570' height="100" >

<TR>
	<TD bgcolor='#f1f1f3'><font face='verdana' size='3'><br><FONT size=2>
	  Nelle pagine che seguono ti sar� richiesto di analizzare le tue esperienze lavorative e non.<br><br>
	  Ognuno di noi costruisce un proprio bagaglio professionale nelle occasioni in cui ricopre un posto di 
      lavoro, in quelle in cui frequenta uno stage, in quelle in cui si dedica 
      ad attivit� di impegno personale, ricreativo, hobbistico.<br><br>
      Seguendo le istruzioni che ti saranno proposte, ti chiederemo di analizzare questo 
      �bagaglio� e di trasformarlo in informazioni analitiche, cio� in Elementi 
      di competenze.<br><br>
      In questo il tuo �Portafoglio personale� sar� incrociabile con altre informazioni inserite nel Data Base.<br><br>
      Potrai ottenere indicazioni su quali siano le aree 
      professionali pi� vicine, e, a conclusione del percorso, realizzare un 
      vero e proprio Bilancio di Competenze.</FONT>
         
                          </font></TD>
</TR>
</TABLE>

<!-- #INCLUDE FILE="../Footer.asp" -->
</center>
</body>

</html>
