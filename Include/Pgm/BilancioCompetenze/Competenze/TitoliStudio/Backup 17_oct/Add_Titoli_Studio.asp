<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #Include Virtual="/strutt_testa2.asp" -->
<!--#include Virtual = "/include/DecCod.asp"-->
<!-- #Include Virtual="/Pgm/BilancioCompetenze/Utils.asp" -->
<!--#include Virtual = "/Include/OpenConn.asp"-->
<!-- #Include Virtual="/Pgm/BilancioCompetenze/ValidaDtNascita.asp" -->
<!-- #Include Virtual="/Include/ControlDateVB.asp" -->


<script language="javascript">
<!--#include Virtual = "/Include/ControlNum.inc"-->
<!-- #Include Virtual="/Include/ControlString.inc" -->

function valida(Obj)
{

if (Obj.CmbCOD_LIV_STUD.value != "NES")
{
				Obj.DESC_ISTITUTO.value=TRIM(Obj.DESC_ISTITUTO.value)
				Obj.VOTO_STUD.value=TRIM(Obj.VOTO_STUD.value)
				Obj.AA_STUD.value=TRIM(Obj.AA_STUD.value)
				Obj.AA_STUD_UltAnno.value.value=TRIM(Obj.AA_STUD_UltAnno.value)
				Obj.NUM_AA_FREQ.value=TRIM(Obj.NUM_AA_FREQ.value)
				Obj.NUM_ESAMI.value=TRIM(Obj.NUM_ESAMI.value)      

			    
			if (Obj.CmbCOD_LIV_STUD.options[Obj.CmbCOD_LIV_STUD.selectedIndex].value == "")
			{
				alert("El campo Nivel es obligatorio")
				Obj.CmbCOD_LIV_STUD.focus()
				return false;
			}
	
			if (Obj.txtSpecifico.value == "") 
			{
				alert("El campo t�tulo es obligatorio ")
				return false;
			}	
	

			if (Obj.CmbCOD_STAT_FREQ.options[Obj.CmbCOD_STAT_FREQ.selectedIndex].value == "")
			{
			    alert("El campo �ltimo a�o aprobado es obligatorio si el t�tulo no se consigui�")
			    Obj.CmbCOD_STAT_FREQ.focus()
				return false;
			 }
	
			if (!ValidateInputStringWithNumber(Obj.DESC_ISTITUTO.value))
			{
				alert("El campo tema es incorrecto")
				Obj.DESC_ISTITUTO.focus() 
				return false;
			}

	
			if (Obj.COD_STAT_STUD.value == "0")
			{
				if (Obj.AA_STUD.value == "")
				{
					alert("El campo a�o es obligatorio si se consigui� el t�tulo")
					Obj.AA_STUD.focus() 
					return false;
				}
			}	
	
			if (!Obj.AA_STUD.value == "") 
				{
					if (!isNum(Obj.AA_STUD.value))
					{
						alert("El campo a�o debe ser num�rico")
						Obj.AA_STUD.focus() 
						return false;
					}
					if (Obj.AA_STUD.value < 1900)
					{
						alert("El campo a�o debe ser superior al 1900")
						return false;
					}
					if (Obj.AA_STUD.value  > <%=year(Date)%>)
						{
						alert("El campo a�o no puede ser superior al actual")
						Obj.AA_STUD.focus() 
						return false;	
						}
					if (ValidaAnnoNascita(Obj.AA_STUD.value))
						{ 
						alert("El campo a�o debe ser superior al a�o de nacimiento") 
						Obj.AA_STUD.focus() 
						return false; 
						} 	
						
					if ((Obj.COD_STAT_STUD.value == "1") || (Obj.COD_STAT_STUD.value == "2"))
						{
							alert("El campo a�o debe ser indicado solo si se obtubo el t�tulo de estudio")
							Obj.AA_STUD.focus() 
						return false;
						}
					
				}
	
	
			 /*if (Obj.VOTO_STUD.value == "") 
				{
					if (Obj.COD_STAT_STUD.value == "0")
						{
							alert("El campo promedio es obligatorio cuando el t�tulo fue obtenido")
							Obj.VOTO_STUD.focus() 
							return false;
						}
				}*/


	
			 if (!Obj.VOTO_STUD.value == "") 
				{
				 	if ((Obj.COD_STAT_STUD.value == "1") || (Obj.COD_STAT_STUD.value == "2"))
						{
						alert("El campo promedio no debe ser ingresado si el titulo no fue obtenido")
						Obj.VOTO_STUD.focus() 
						return false;
						}
				}	
	
	
	
			if ((Obj.COD_STAT_STUD.value == "1") || (Obj.COD_STAT_STUD.value == "2"))
					{
						if (Obj.FL_LAUDE.checked == true)
						{
						alert("El campo con honores no debe ingresarse si el titulo no fue obtenido")
						Obj.FL_LAUDE.focus() 
						return false;
						}
					}
	
			if (Obj.CmbCOD_STAT_RIC.options[Obj.CmbCOD_STAT_RIC.selectedIndex].value == "")
					{
					 alert("El campo reconocido en es obligatorio")
					 Obj.CmbCOD_STAT_RIC.focus()
					return false;
					}	
	
	
	
			if ((Obj.COD_STAT_STUD.value == "1") || (Obj.COD_STAT_STUD.value == "2"))
			{
				if (Obj.AA_STUD_UltAnno.value == "")
				{
				alert("El campo �ltimo a�o aprobado es obligatorio si el t�tulo no se ha obtenido")
				Obj.AA_STUD_UltAnno.focus()
				return false;
				}
			}
	
			 if (!Obj.AA_STUD_UltAnno.value == "") 
			{
				if (!isNum(Obj.AA_STUD_UltAnno.value))
					{
						alert("El campo ultimo a�o aprobado debe ser numerico")
						Obj.AA_STUD_UltAnno.focus() 
						return false;
					}
					
					if	(Obj.AA_STUD_UltAnno.value < 1900)
					{
						alert("EL campo ultimo a�o aprobado debe ser superior al 1900")
						Obj.AA_STUD_UltAnno.focus() 
						return false;
					}	
						
				if (Obj.AA_STUD_UltAnno.value  > <%=year(Date)%>)
					{
						alert("El campo ultimo a�o aprobado debe ser superior al actual")
						Obj.AA_STUD_UltAnno.focus() 
						return false;	
					}
				if (ValidaAnnoNascita(Obj.AA_STUD_UltAnno.value))
					{ 
					alert("El campo ultimo a�o aprobado debe ser posterior al de nacimiento") 
					Obj.AA_STUD_UltAnno.focus() 
					return false; 
					} 
				if (!((Obj.COD_STAT_STUD.value == "1") || (Obj.COD_STAT_STUD.value == "2")))
					{
					alert("EL ultimo a�o aprobado debe ser completado solo si el titulo no fue conseguido")
					Obj.AA_STUD_UltAnno.focus() 
					return false;
					}
			 }

					
					
					if (!Obj.NUM_AA_FREQ.value == "")
					{			
						if (!isNum(Obj.NUM_AA_FREQ.value))
							{
								alert("EL campo a�os cursados debe ser num�rico")
								Obj.NUM_AA_FREQ.focus()
								return false;
							}
					}

					
					
					if (!Obj.NUM_ESAMI.value == "")
					{
						if (!isNum(Obj.NUM_ESAMI.value))
							{
								alert("El campo Numero de Ex�menes debe ser num�rico")
								Obj.NUM_ESAMI.focus()
								return false;
							}
						// il numero esami deve essere specificato solo se il titolo non � conseguito
						if (!((Obj.COD_STAT_STUD.value == "1") || (Obj.COD_STAT_STUD.value == "2")))
							{
							alert("El campo Numero ex�menes s�lo debe ser indicado si el t�tulo de estudio a�n no fue conseguido")
							Obj.NUM_ESAMI.focus() 
							return false;
							}				
						// il numero esami deve essere specificato solo se il titolo � una laurea o un diploma post universitario
						if (!((Obj.CmbCOD_LIV_STUD.options[Obj.CmbCOD_LIV_STUD.selectedIndex].value == "LAU") ||
						     (Obj.CmbCOD_LIV_STUD.options[Obj.CmbCOD_LIV_STUD.selectedIndex].value=='LSP') ||
						     (Obj.CmbCOD_LIV_STUD.options[Obj.CmbCOD_LIV_STUD.selectedIndex].value=='DLA') ||
						     (Obj.CmbCOD_LIV_STUD.options[Obj.CmbCOD_LIV_STUD.selectedIndex].value == "QPD")))
							{
							alert("El campo Numero de ex�menes debe ser indicado solo si el titulo de estudio no conseguido es una licenciatura o un diploma post-universitario")
							Obj.NUM_ESAMI.focus() 
							return false;
							}				
					
					}				
		
	}
}


function ActivInputLivello(valore)
	{
	var PrecNes
	
	document.Titoli_Studio_2.txtSpecifico.value = "";
	


			if (valore == "NES")
			{
				document.Titoli_Studio_2.FL_LAUDE.disabled = true;	
				document.Titoli_Studio_2.CmbCOD_STAT_FREQ.value ="";
				document.Titoli_Studio_2.CmbCOD_STAT_FREQ.disabled = true;
				document.Titoli_Studio_2.DESC_ISTITUTO.value = "";
				document.Titoli_Studio_2.DESC_ISTITUTO.disabled = true;
				document.Titoli_Studio_2.COD_STAT_STUD.value ="";
				document.Titoli_Studio_2.COD_STAT_STUD.disabled =true;
				document.Titoli_Studio_2.AA_STUD.value = "";
				document.Titoli_Studio_2.AA_STUD.disabled = true;
				document.Titoli_Studio_2.VOTO_STUD.value = "";
				document.Titoli_Studio_2.VOTO_STUD.disabled = true;
				document.Titoli_Studio_2.CmbCOD_STAT_RIC.value ="";
				document.Titoli_Studio_2.CmbCOD_STAT_RIC.disabled = true;
				document.Titoli_Studio_2.AA_STUD_UltAnno.value = "";
				document.Titoli_Studio_2.AA_STUD_UltAnno.disabled = true;
				document.Titoli_Studio_2.NUM_AA_FREQ.value = "";
				document.Titoli_Studio_2.NUM_AA_FREQ.disabled = true;
				document.Titoli_Studio_2.NUM_ESAMI.value = "";
				document.Titoli_Studio_2.NUM_ESAMI.disabled = true;
				document.Titoli_Studio_2.PrecNES.value = "Yes";
				SelSpecifico()
			}
			if ((valore != "NES") && (document.Titoli_Studio_2.PrecNES.value == "Yes"))
			{
				document.Titoli_Studio_2.CmbCOD_STAT_FREQ.value ="IT";
				document.Titoli_Studio_2.CmbCOD_STAT_FREQ.disabled = false;	
				document.Titoli_Studio_2.DESC_ISTITUTO.disabled = false;
				document.Titoli_Studio_2.COD_STAT_STUD.value =0;
				document.Titoli_Studio_2.COD_STAT_STUD.disabled =false;			
				document.Titoli_Studio_2.AA_STUD.disabled = false;
				document.Titoli_Studio_2.VOTO_STUD.disabled = false;
				document.Titoli_Studio_2.CmbCOD_STAT_RIC.value ="IT";
				document.Titoli_Studio_2.CmbCOD_STAT_RIC.disabled = false;
				document.Titoli_Studio_2.PrecNES.value = "No";
			}	
	
	
	//******************************************************************************************
			// gestione checkbox 'Cum Laude'
			if ((document.Titoli_Studio_2.COD_STAT_STUD.value == "0") && (valore == "LAU" || valore == "LSP" || valore == "DLA"))
				{
				document.Titoli_Studio_2.FL_LAUDE.disabled = false;	
				}
				else
				{
				document.Titoli_Studio_2.FL_LAUDE.checked = false;	
				document.Titoli_Studio_2.FL_LAUDE.disabled = true;	
				}
				
				
				
			if ((document.Titoli_Studio_2.COD_STAT_STUD.value == "1") || (document.Titoli_Studio_2.COD_STAT_STUD.value == "2"))
				{
				if ((valore == "LAU") || (valore == "QPD") || (valore == "LSP") || (valore == "DLA"))
						{
						document.Titoli_Studio_2.NUM_ESAMI.disabled = false;	
						}
					else 
						{
						document.Titoli_Studio_2.NUM_ESAMI.value = "";	
						document.Titoli_Studio_2.NUM_ESAMI.disabled = true;	
						}	
			
			   }
			
			

		}
		
	
function ActivInputFields(valore)
{
//alert(document.Titoli_Studio_2.CmbCOD_LIV_STUD.value)
	if ((valore == "1") || (valore == "2"))
		{
		// Abilitazione campi relativi a status 'non conseguito/in frequenza'
		document.Titoli_Studio_2.NUM_AA_FREQ.disabled = false;
		document.Titoli_Studio_2.AA_STUD_UltAnno.disabled = false;
			if ((document.Titoli_Studio_2.CmbCOD_LIV_STUD.value == "LAU") || (document.Titoli_Studio_2.CmbCOD_LIV_STUD.value == "QPD") || (document.Titoli_Studio_2.CmbCOD_LIV_STUD.value == "LSP") || (document.Titoli_Studio_2.CmbCOD_LIV_STUD.value == "DLA"))
			{
			document.Titoli_Studio_2.NUM_ESAMI.disabled = false;		
			document.Titoli_Studio_2.FL_LAUDE.disabled = true;
			document.Titoli_Studio_2.FL_LAUDE.checked = false;
			}
	// Disabilitazione campi relativi a status 'conseguito'
		document.Titoli_Studio_2.AA_STUD.disabled = true;
		document.Titoli_Studio_2.AA_STUD.value = "";
		document.Titoli_Studio_2.VOTO_STUD.disabled = true;
		document.Titoli_Studio_2.VOTO_STUD.value = "";
		}
	else
		{
		// Abilitazione campi relativi a status 'conseguito'
		
		document.Titoli_Studio_2.AA_STUD.disabled = false;
		document.Titoli_Studio_2.VOTO_STUD.disabled = false;
			
		// Disabilitazione campi relativi a status 'non conseguito/in frequenza'
		document.Titoli_Studio_2.NUM_AA_FREQ.disabled = true;
		document.Titoli_Studio_2.NUM_AA_FREQ.value = "";
		document.Titoli_Studio_2.AA_STUD_UltAnno.disabled = true;
		document.Titoli_Studio_2.AA_STUD_UltAnno.value = "";
		document.Titoli_Studio_2.NUM_ESAMI.disabled = true;	
		document.Titoli_Studio_2.NUM_ESAMI.value = "";	
		
			if 	((document.Titoli_Studio_2.CmbCOD_LIV_STUD.value == "LAU") || (document.Titoli_Studio_2.CmbCOD_LIV_STUD.value == "LSP") || (document.Titoli_Studio_2.CmbCOD_LIV_STUD.value == "DLA"))
			{
			document.Titoli_Studio_2.FL_LAUDE.disabled = false;
			}
		}

}


// In caso di errore restituito dalla pagina di salvataggio mantiene attivi/disattivi i corretti campi
// La funzione viene attivata dall'EventHandler onLoad collocato sulla primo tag <IMG SRC="immagine.gif" ..
function CheckDisabledFields()
{
	if ((document.Titoli_Studio_2.COD_STAT_STUD.value == "1") || (document.Titoli_Studio_2.COD_STAT_STUD.value == "2"))
		{
		// Abilitazione campi relativi a status 'non conseguito/in frequenza'
		document.Titoli_Studio_2.NUM_AA_FREQ.disabled = false;
		document.Titoli_Studio_2.AA_STUD_UltAnno.disabled = false;
			if ((document.Titoli_Studio_2.CmbCOD_LIV_STUD.value == "LAU") || (document.Titoli_Studio_2.CmbCOD_LIV_STUD.value == "QPD") || (document.Titoli_Studio_2.CmbCOD_LIV_STUD.value == "LSP") || (document.Titoli_Studio_2.CmbCOD_LIV_STUD.value == "DLA"))
			{
			document.Titoli_Studio_2.NUM_ESAMI.disabled = false;		
			document.Titoli_Studio_2.FL_LAUDE.disabled = true;
			document.Titoli_Studio_2.FL_LAUDE.checked = false;
			}
	// Disabilitazione campi relativi a status 'conseguito'
		document.Titoli_Studio_2.AA_STUD.disabled = true;
		document.Titoli_Studio_2.AA_STUD.value = "";
		document.Titoli_Studio_2.VOTO_STUD.disabled = true;
		document.Titoli_Studio_2.VOTO_STUD.value = "";
		}
	else
		{
		// Abilitazione campi relativi a status 'conseguito'
		
		document.Titoli_Studio_2.AA_STUD.disabled = false;
		document.Titoli_Studio_2.VOTO_STUD.disabled = false;
		
		
		if (document.Titoli_Studio_2.NUM_AA_FREQ)
		{	
		// Disabilitazione campi relativi a status 'non conseguito/in frequenza'
		document.Titoli_Studio_2.NUM_AA_FREQ.disabled = true;
		document.Titoli_Studio_2.NUM_AA_FREQ.value = "";
		document.Titoli_Studio_2.AA_STUD_UltAnno.disabled = true;
		document.Titoli_Studio_2.AA_STUD_UltAnno.value = "";
		document.Titoli_Studio_2.NUM_ESAMI.disabled = true;	
		document.Titoli_Studio_2.NUM_ESAMI.value = "";	
		}
			if 	((document.Titoli_Studio_2.CmbCOD_LIV_STUD.value == "LAU") || (document.Titoli_Studio_2.CmbCOD_LIV_STUD.value == "LSP") || (document.Titoli_Studio_2.CmbCOD_LIV_STUD.value == "DLA"))
			{
			document.Titoli_Studio_2.FL_LAUDE.disabled = false;
			}
		}

}


//-->
</script>
<script language="javascript" src="Controlli.js">
</script>


<%
	sub CreaComboCOD_STAT_STUD(Codice)
	dim stringa_frequenza,aFrequenza

		sSql="SELECT ID_CAMPO_DESC " &_
			"FROM DIZ_DATI " &_
			"WHERE " &_
			"ID_TAB='TISTUD' AND ID_CAMPO='COD_STAT_STUD'"

'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
		set Rst=CC.Execute(sSql)
		stringa_frequenza=Rst("ID_CAMPO_DESC")
		aFrequenza=split(stringa_frequenza,"|")
	%>
		<select name="COD_STAT_STUD" class="textblack" id="COD_STAT_STUD" onChange="return ActivInputFields(this.value);">

	<%	for i=0 to Ubound(aFrequenza) step 2
			Response.Write "<option value='" & aFrequenza(i) & "'"
			if aFrequenza(i)=Codice then Response.Write " selected"
			Response.Write ">" & Ucase((aFrequenza(i+1))) & "</option>"
		next
	%>	
	</select>
	<%	Rst.Close
		set Rst=nothing
	end sub
%>


<center>
<form name="Titoli_Studio_2" Action="Salva_Tit_Stud.asp" Method="post" OnSubmit="return valida(this)">
<input type="hidden" name="PrecNES" value>
<input type="hidden" value="<%=Request("IdPers")%>" name="IdPers">
<input type="hidden" value="<%=Request("LivMenu")%>" name="LivMenu">
<input type="hidden" size="50" name="txtoggi" value="<%=ConvDateToString(Now())%>">
<input type="hidden" value="<%=Request("IND_FASE")%>" name="IND_FASE">	
<input type="hidden" value="<%=Request.ServerVariables("URL")%>" name="back">
<input type="hidden" value="<%=Request("BackPage")%>" name="BackPage">

<table border="0" width="500" CELLPADDING="0" cellspacing="0">
	<tr>
		<td align="left" class="sfondomenu" width="199"><b><span class="tbltext0"> 
          <b>&nbsp;TITULO DE ESTUDIO</b></span></td>

		<td width="25" valign="bottom" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">
			<img border="0" src="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
		<td width="30%" valign="middle" align="right" class="tbltext1" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">(*) 
          campo obligatorio</td>
	</tr>
	</table>
	
	<table border="0" width="500" CELLPADDING="0" cellspacing="0">
    <tr>
		<td class="sfondocomm" align="left" class="tbltext1">
		Ingreso del t�tulo de estudio. <br>
			Indicar a continuaci�n la informaci�n relativa a titulos de estudio cursados u obtenidos.
			<br>Completar los campos inferiores y presionar <b>Enviar</b> para guardar la modificaci�n. 
			</td> 
		<td class="sfondocomm"><a onmouseover="javascript: window.status=' '; return true; " href="Javascript:Show_Help('/Pgm/Help/BilancioCompetenze/Competenze/TitoliStudio/Add_Titoli_Studio')">
			<img src="<%=Session("Progetto")%>/images/Help.gif" border="0" align="right" alt="Help">
			</a>
		</td>
    </tr>
 	<tr height="2">
		<td class="sfondocomm" width="100%" colspan="2" background="<%=Session("Progetto")%>/images/separazione.gif">
		</td>
	</tr>
</table>	


<table border="0" cellpadding="0" cellspacing="1" width="500" style="font-family: Verdana; font-size: 8pt; color:#ffffff">
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr>	
			<td align="left" class="tbltext1">
				<p align="left">
				<strong>Nivel*</strong>
				</p>
			</td>
			<td align="left" width="60%">
				<p align="left">
			
<%
Sql = " SELECT COD_LIV_STUD"
Sql = Sql &  " FROM TISTUD"
Sql = Sql &  " WHERE "
'COD_LIV_STUD = 'NES' AND"
Sql = Sql &  "  ID_PERSONA = " & IDP

'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
	Set Rs = cc.Execute (Sql)
		if not rs.eof then		 
			 'condizione per CreateCombo(UtCod_TIT_STUD) 
			 'nel caso sono gi� stati registrati titoli di studio
				Condiz = " AND CODICE <> 'NES' " 
			rs.close	
		end if
	set rs = Nothing


			UtCod_TIT_STUD= "LSTUD|0|" & date() & "||CmbCOD_LIV_STUD' onChange='return ActivInputLivello(this.value);'|  " & Condiz & "ORDER BY VALORE"			
			CreateCombo(UtCod_TIT_STUD)
%>
			</td>
		</tr>

		<tr>
			<td align="left" colspan="1" class="tbltext1">
	 			<b>Titulo*</b>
	 		</td>
	 		<td class="tbltext1">
				<input class="textblacka" readonly type="text" name="txtSpecifico" style="width=250px">
				<input type="hidden" name="txtTipoSpecifico">
				<a href="Javascript:SelSpecifico()" ID="imgPunto2" name="imgPunto2"><img border="0" src="<%=Session("Progetto")%>/images/bullet1.gif"></a>
			</td>
		</tr>
		<tr height="20">
			<td class="tbltext1" align="left" colspan="1">
				<p align="left"><b>Obtenido en*</b>
			</td>
			<td colspan="2">
			<%
			UtCod_stat_Freq= "STATO|0|" & date() & "|AR|CmbCOD_STAT_FREQ|ORDER BY DESCRIZIONE"			
			CreateCombo(UtCod_stat_Freq)
			%>
			</td>
		</tr>
		 <tr height="20">
		    <td align="left" colspan="1" class="tbltext1">
				<p align="left"><b>Tema &nbsp;</b>
		    </td>
		    <td align="left" width="60%">
				<input style="TEXT-TRANSFORM: uppercase;width:300px; " class="textblacka" maxlength="50" name="DESC_ISTITUTO">
		    </td>
		</tr>
	<tr height="20">
		<td align="left" colspan="1">
			<p align="left" class="tbltext1"><strong>Estado*</strong>
		</td>
		<td>
		<%CreaComboCOD_STAT_STUD("0")%>
			
		</td>
    </tr>
	<tr height="15">
		<td colspan="2">&nbsp;
		</td>
	</tr>
	<tr>
		<td colspan="2" height="2" bgcolor="#3399cc">
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<span class="textblack">
			Si el titulo ya fue conseguido indicar
			 el <b>A�o </b> en que se obtuvo.
			</span>
		</td>
	</tr>
	<tr height="15">
		<td colspan="2">
		</td>
	</tr>
	<tr height="20">
		<td class="tbltext1" align="left" colspan="1">
			<b>A�o*&nbsp;</b>(aaaa)
		</td>
		<td align="left" class="tbltext1" colspan="2" width="60%">
				<input class="textblacka" size="11" maxlength="4" name="AA_STUD">
				&nbsp;
		</td>
    </tr>

    <tr>
		<td align="left" colspan="1" class="tbltext1">
			<b>Promedio*</b>
		</td>
		<td> <input class="MyTextBox" maxlength="12" name="VOTO_STUD">
		</td>
	</tr>

	<tr>
		<td align="left" class="tbltext1">
			<b>Con honores</b>
		</td>
		<td><input name="FL_LAUDE" type="checkbox" VALUE="S" disabled>		
		</td>
	</tr>
	<tr height="20">
		<td class="tbltext1" align="left" colspan="1">
			<p align="left"><b>Reconocido en*</b></td>
		<td colspan="2">
			<%
			UtCod_Stat_Ric= "STATO|0|" & date & "|AR|CmbCOD_STAT_RIC|ORDER BY DESCRIZIONE"			
			CreateCombo(UtCod_Stat_Ric)
			%>
		</td>
	</tr>
	<tr height="15">
		<td colspan="2">
		</td>
	</tr>
	<tr>
		<td colspan="2" height="2" bgcolor="#3399cc"><td>
	</tr>
	<tr>
		<td colspan="2">
			<span class="textblack">
			 Indicar si corresponde el <b>Ultimo a�o aprobado</b> y
			 el total de <b>A�os aprobados</b>. 
			</span>
		</td>
	</tr>
	<tr height="15">
		<td colspan="2">
		</td>
	</tr>
	 <tr height="20">
		<td class="tbltext1" align="left" colspan="1">
			<b>Ultimo a�o aprobado*</b><br>(aaaa)
		</td>
		
		
		<td align="left" class="tbltext1" colspan="2" width="60%">
				<input class="textblacka" size="11" maxlength="4" name="AA_STUD_UltAnno" disabled>
				&nbsp;
		</td>
    </tr>
    
    <tr>
		<td align="left" colspan="1" class="tbltext1"><b>
			Total de a�os aprobados&nbsp;</b></font>
		</td>
		<td>
		
			<input class="MyTextBox" maxlength="2" name="NUM_AA_FREQ" disabled>
		</td>
	</tr>
	<tr>
		<td align="left" colspan="1" class="tbltext1"> <b>Nro. Examenes Aprobados&nbsp;<b></td>
		<td><input class="MyTextBox" maxlength="3" name="NUM_ESAMI" disabled> </td>
	</tr>
    
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
		<td align="middle" colspan="3">
			<a href="javascript:goToPage('Titoli_Studio.asp')"><img alt="Indietro" border="0" src="<%=Session("Progetto")%>/images/indietro.gif"></a>
			<input type="image" src="<%=Session("Progetto")%>/images/conferma.gif" border="0" value="Salva" onLoad="CheckDisabledFields()">
		</td>
	</tr>
	
</table>

</form>
</center>	
<!--#include virtual="/include/Closeconn.asp"-->
<!-- #include Virtual="/strutt_coda2.asp" -->
