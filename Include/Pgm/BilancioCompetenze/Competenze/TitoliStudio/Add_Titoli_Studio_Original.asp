<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #Include Virtual="/strutt_testa2.asp" -->
<!--#include Virtual = "/include/DecCod.asp"-->
<!-- #Include Virtual="/Pgm/BilancioCompetenze/Utils.asp" -->
<!--#include Virtual = "/Include/OpenConn.asp"-->
<!-- #Include Virtual="/Pgm/BilancioCompetenze/ValidaDtNascita.asp" -->
<!-- #Include Virtual="/Include/ControlDateVB.asp" -->


<script language="javascript">
<!--#include Virtual = "/Include/ControlNum.inc"-->
<!--#Include Virtual="/Include/ControlString.inc" -->
<!--#Include Virtual="/Include/ControlDate.inc" -->

function valida(Obj)
{


var today = new Date();                       
var day   = today.getDate();                  
var month = today.getMonth();                 
var year  = today.getYear();                  
var fechadehoy;                               

fechadehoy = document.Titoli_Studio_2.txtOggi.value 

//alert(fechadehoy);
		
//VALIDACION A REALIZAR SI NO ES NTS (NINGUN TITULO DE ESTUDIO)
if ((Obj.CmbCOD_LIV_STUD.value != "NLE") && (Obj.CmbCOD_LIV_STUD.value != "SLE"))
{
	if (Obj.COD_STAT_STUD.value == "4")
	{
		alert("El valor no corresponde en el estado del estudio solo puede ser indicado si no asisti� a la escuela");
		return false;
	}
}

if ((Obj.CmbCOD_LIV_STUD.value == "NLE") || (Obj.CmbCOD_LIV_STUD.value == "SLE"))
{
	if (Obj.COD_STAT_STUD.value != "4")
	{
		alert("El valor no corresponde es el �nico aceptado para el estado si no asisti� a la escuela");
		return false;
	}
}


if (Obj.CmbCOD_LIV_STUD.value != "NTS")
{
	Obj.DESC_ISTITUTO.value=TRIM(Obj.DESC_ISTITUTO.value)
	Obj.VOTO_STUD.value=TRIM(Obj.VOTO_STUD.value)
	Obj.AA_STUD.value=TRIM(Obj.AA_STUD.value)
	Obj.AA_STUD_UltAnno.value.value=TRIM(Obj.AA_STUD_UltAnno.value)
   

			    
	if (Obj.CmbCOD_LIV_STUD.options[Obj.CmbCOD_LIV_STUD.selectedIndex].value == "")
	{
		alert("El campo Nivel es obligatorio")
		Obj.CmbCOD_LIV_STUD.focus()
		return false;
	}
	
	if (Obj.txtSpecifico.value == "") 
	{
		alert("El campo t�tulo es obligatorio ")
		return false;
	}	
	
	if (!ValidateInputStringWithNumber(Obj.DESC_ISTITUTO.value))
	{
		alert("El campo tema es incorrecto")
		Obj.DESC_ISTITUTO.focus() 
		return false;
	}

	
	if (Obj.COD_STAT_STUD.value == "0")
	{
		if (Obj.AA_STUD.value == "")
		{
			alert("El campo a�o es obligatorio")
			Obj.AA_STUD.focus() 
			return false;
		}
	}	
	
	if (!Obj.AA_STUD.value == "") 
	{
			if (!isNum(Obj.AA_STUD.value))
			{
				alert("El campo a�o debe ser num�rico")
				Obj.AA_STUD.focus() 
				return false;
			}
			
			if (Obj.AA_STUD.value < 1900)
			{
				alert("El campo a�o debe ser superior al 1900")
				return false;
			}
			
			if (Obj.AA_STUD.value  > <%=year(Date)%>)
			{
				alert("El campo a�o no puede ser superior al actual")
				Obj.AA_STUD.focus() 
				return false;	
			}
			
			if (ValidaAnnoNascita(Obj.AA_STUD.value))
			{ 
				alert("El campo a�o debe ser superior al a�o de nacimiento") 
				Obj.AA_STUD.focus() 
				return false; 
			} 	
	}

	if (Obj.CmbCOD_STAT_RIC.options[Obj.CmbCOD_STAT_RIC.selectedIndex].value == "")
	{
		if (Obj.COD_STAT_STUD.value == "0")
		{
			alert("El campo reconocido en es obligatorio")
			Obj.CmbCOD_STAT_RIC.focus()
			return false;
		}
	}	
	
	if (Obj.COD_STAT_STUD.value == "1")
	{
		if (Obj.AA_STUD_UltAnno.value == "")
		{
			alert("El campo Ultimo a�o aprobado es obligatorio");
			Obj.AA_STUD_UltAnno.focus();
			return false;
		}
	}
	
	if (!Obj.AA_STUD_UltAnno.value == "") 
	{
		if (!isNum(Obj.AA_STUD_UltAnno.value))
		{
			alert("El campo ultimo a�o aprobado debe ser numerico")
			Obj.AA_STUD_UltAnno.focus() 
			return false;
		}
			
		/*if	(Obj.AA_STUD_UltAnno.value < 1900)
		{
			alert("EL campo ultimo a�o aprobado debe ser superior al 1900")
			Obj.AA_STUD_UltAnno.focus() 
			return false;
		}	
				
		if (Obj.AA_STUD_UltAnno.value  > <%=year(Date)%>)
		{
			alert("El campo ultimo a�o aprobado no debe ser superior al actual")
			Obj.AA_STUD_UltAnno.focus() 
			return false;	
		}
			
		if (ValidaAnnoNascita(Obj.AA_STUD_UltAnno.value))
		{ 
			alert("El campo ultimo a�o aprobado debe ser posterior al de nacimiento") 
			Obj.AA_STUD_UltAnno.focus() 
			return false; 
		}  */
	}

				//agregadooooooo
		if ((Obj.TXTPORCENTAJEAPROBADO.disabled == false) && (Obj.TXTPORCENTAJEAPROBADO.value == ""))
		{
			alert("El campo Porcentaje Aprobado es obligatorio");
			return false;
		}
		
		if ((Obj.TXTPORCENTAJECURSADO.disabled == false) && (Obj.TXTPORCENTAJECURSADO.value == ""))
		{
			alert("El campo Porcentaje Cursado es obligatorio");
			return false;
		}
		
		if (!Obj.TXTPORCENTAJEAPROBADO.value == "")
		{
			if (!isNum(Obj.TXTPORCENTAJEAPROBADO.value))
			{
				alert("El campo porcentaje cursado debe ser numerico")
				Obj.TXTPORCENTAJEAPROBADO.focus() 
				return false;
			}
			
			if ((Obj.TXTPORCENTAJEAPROBADO.value == "0") || (Obj.TXTPORCENTAJEAPROBADO.value < 0))
			{
				alert("El valor debe ser mayor a cero");
				return false;
		    }

		}
		

			
		if (Obj.TXTFECHAABANDONO.value == "")
		{
			if (Obj.COD_STAT_STUD.value == "1")
			{
			alert("El campo Fecha de Abandono es obligatorio");
			return false;
			}
		}
			
		if (Obj.TXTFECHAABANDONO.value != "")
		{
		
			if (ValidateInputDate(Obj.TXTFECHAABANDONO.value) == false)
			{
				return false;
			}

			if (ValidateRangeDate(Obj.TXTFECHAABANDONO.value,fechadehoy) == false)			
			{
				alert ("La fecha de abandono no debe ser superior a la actual");
				return false;	
			}
			
		}
		
		if (Obj.TXTA�OENCURSO.value == "") 
		{
			if (Obj.COD_STAT_STUD.value == "2")
			{
				alert("El campo a�o en curso es obligatorio");
				return false;
			}
		}
			
		if (Obj.TXTA�OENCURSO.value != "")
		{
			if (!isNum(Obj.TXTA�OENCURSO.value))
			{
				alert("El campo a�o que esta cursando debe ser numerico")
				Obj.TXTA�OENCURSO.focus() 
				return false;
			}

				
			if	(Obj.TXTA�OENCURSO.value < 0)
			{
				alert("El campo a�o que esta cursando debe ser mayor a cero")
				Obj.TXTA�OENCURSO.focus() 
				return false;
			}	
		}	
							
		if (!Obj.TXTPORCENTAJECURSADO.value == "")
		{
				if (!isNum(Obj.TXTPORCENTAJECURSADO.value))
				{
					alert("El campo porcentaje cursado debe ser numerico");
					Obj.TXTPORCENTAJECURSADO.focus();
					return false;
				}
				
				if ((Obj.TXTPORCENTAJECURSADO.value == "0") || (Obj.TXTPORCENTAJECURSADO.value < 0))
				{
					alert("El valor debe ser mayor a cero");
					return false;
				}
		}					
						
  }
}

function ActivInputLivello(valore)
	{
	var PrecNes
	
	document.Titoli_Studio_2.txtSpecifico.value = "";
	


			if (valore == "NTS")
			{
				document.Titoli_Studio_2.FL_LAUDE.disabled = true;	
				document.Titoli_Studio_2.CmbCOD_STAT_FREQ.value ="";
				document.Titoli_Studio_2.CmbCOD_STAT_FREQ.disabled = true;
				document.Titoli_Studio_2.DESC_ISTITUTO.value = "";
				document.Titoli_Studio_2.DESC_ISTITUTO.disabled = true;
				document.Titoli_Studio_2.COD_STAT_STUD.value ="";
				document.Titoli_Studio_2.COD_STAT_STUD.disabled =true;
				document.Titoli_Studio_2.AA_STUD.value = "";
				document.Titoli_Studio_2.AA_STUD.disabled = true;
				document.Titoli_Studio_2.VOTO_STUD.value = "";
				document.Titoli_Studio_2.VOTO_STUD.disabled = true;
				document.Titoli_Studio_2.CmbCOD_STAT_RIC.value ="";
				document.Titoli_Studio_2.CmbCOD_STAT_RIC.disabled = true;
				document.Titoli_Studio_2.AA_STUD_UltAnno.value = "";
				document.Titoli_Studio_2.AA_STUD_UltAnno.disabled = true;
				document.Titoli_Studio_2.TXTFECHAABANDONO.value = "";
				document.Titoli_Studio_2.TXTFECHAABANDONO.disabled = true;
				document.Titoli_Studio_2.TXTPORCENTAJEAPROBADO.value = "";
				document.Titoli_Studio_2.TXTPORCENTAJEAPROBADO.disabled = true;
				document.Titoli_Studio_2.TXTA�OENCURSO.value = "";
				document.Titoli_Studio_2.TXTA�OENCURSO.disabled = true;
				document.Titoli_Studio_2.TXTPORCENTAJECURSADO.value = "";
				document.Titoli_Studio_2.TXTPORCENTAJECURSADO.disabled = true;				
				document.Titoli_Studio_2.PrecNES.value = "Yes";
				SelSpecifico()
			}
			if (valore != "NTS") // && (document.Titoli_Studio_2.PrecNES.value == "Yes"))
			{
				document.Titoli_Studio_2.CmbCOD_STAT_FREQ.value ="UY";
				document.Titoli_Studio_2.CmbCOD_STAT_FREQ.disabled = false;	
				document.Titoli_Studio_2.DESC_ISTITUTO.value = "";
				document.Titoli_Studio_2.DESC_ISTITUTO.disabled = false;
				document.Titoli_Studio_2.COD_STAT_STUD.value =0;
				document.Titoli_Studio_2.COD_STAT_STUD.disabled =false;			
				document.Titoli_Studio_2.AA_STUD.value = "";
				document.Titoli_Studio_2.AA_STUD.disabled = false;
				document.Titoli_Studio_2.VOTO_STUD.value = "";
				document.Titoli_Studio_2.VOTO_STUD.disabled = false;
				document.Titoli_Studio_2.CmbCOD_STAT_RIC.value ="UY";
				document.Titoli_Studio_2.CmbCOD_STAT_RIC.disabled = false;
				document.Titoli_Studio_2.PrecNES.value = "No";
				//document.Titoli_Studio_2.TXTPORCENTAJEAPROBADO.disabled = true;	
				//document.Titoli_Studio_2.TXTPORCENTAJECURSADO.disabled = true;	
			}	
	
	
	//******************************************************************************************
			// gestione checkbox 'Cum Laude'
			if ((document.Titoli_Studio_2.COD_STAT_STUD.value == "0") && (valore == "TER" || valore == "UNI"))
				{
				document.Titoli_Studio_2.FL_LAUDE.disabled = false;	
				}
				else
				{
				document.Titoli_Studio_2.FL_LAUDE.checked = false;	
				document.Titoli_Studio_2.FL_LAUDE.disabled = true;	
				}
			
		}
		
	
function ActivInputFields(valore)
{
//alert(document.Titoli_Studio_2.CmbCOD_LIV_STUD.value)

	
	//habilitacion campos completo
	if (valore == "0")
	{
		document.Titoli_Studio_2.AA_STUD.disabled = false;
		document.Titoli_Studio_2.VOTO_STUD.disabled = false;
		document.Titoli_Studio_2.CmbCOD_STAT_RIC.disabled = false;
		
		// Disabilitazion otros campos
		document.Titoli_Studio_2.AA_STUD_UltAnno.value = "";
		document.Titoli_Studio_2.AA_STUD_UltAnno.disabled = true;
		document.Titoli_Studio_2.TXTPORCENTAJEAPROBADO.value = "";
		document.Titoli_Studio_2.TXTPORCENTAJEAPROBADO.disabled = true;
		document.Titoli_Studio_2.TXTFECHAABANDONO.value = "";
		document.Titoli_Studio_2.TXTFECHAABANDONO.disabled = true;
		document.Titoli_Studio_2.TXTA�OENCURSO.value = "";
		document.Titoli_Studio_2.TXTA�OENCURSO.disabled = true;
		document.Titoli_Studio_2.TXTPORCENTAJECURSADO.value = "";
		document.Titoli_Studio_2.TXTPORCENTAJECURSADO.disabled = true;
		
		
			if 	((document.Titoli_Studio_2.CmbCOD_LIV_STUD.value == "TER") || (document.Titoli_Studio_2.CmbCOD_LIV_STUD.value == "UNI"))
			{
			document.Titoli_Studio_2.FL_LAUDE.disabled = false;
			}
		
		}
	
	//habilitacion campos abandono
	if (valore == "1")
	{
		document.Titoli_Studio_2.AA_STUD.value = "";
		document.Titoli_Studio_2.AA_STUD.disabled = true;
		document.Titoli_Studio_2.VOTO_STUD.value = "";
		document.Titoli_Studio_2.VOTO_STUD.disabled = true;
		document.Titoli_Studio_2.CmbCOD_STAT_RIC.value ="";
		document.Titoli_Studio_2.CmbCOD_STAT_RIC.disabled = true;
		document.Titoli_Studio_2.FL_LAUDE.checked = false;
		document.Titoli_Studio_2.FL_LAUDE.disabled = true;
		document.Titoli_Studio_2.TXTA�OENCURSO.value = "";
		document.Titoli_Studio_2.TXTA�OENCURSO.disabled = true;
		document.Titoli_Studio_2.TXTPORCENTAJECURSADO.value = "";
		document.Titoli_Studio_2.TXTPORCENTAJECURSADO.disabled = true;
	
		
		document.Titoli_Studio_2.AA_STUD_UltAnno.value = "";
		document.Titoli_Studio_2.AA_STUD_UltAnno.disabled = false;
		document.Titoli_Studio_2.TXTFECHAABANDONO.value = "";
		document.Titoli_Studio_2.TXTFECHAABANDONO.disabled = false;
		document.Titoli_Studio_2.TXTPORCENTAJEAPROBADO.value = "";
		document.Titoli_Studio_2.TXTPORCENTAJEAPROBADO.disabled = false;
		
		

		if ((document.Titoli_Studio_2.CmbCOD_LIV_STUD.value == "TER") || (document.Titoli_Studio_2.CmbCOD_LIV_STUD.value == "UNI") || (document.Titoli_Studio_2.CmbCOD_LIV_STUD.value == "POS"))
			{
				document.Titoli_Studio_2.TXTPORCENTAJEAPROBADO.value = "";
				document.Titoli_Studio_2.TXTPORCENTAJEAPROBADO.disabled = false;
			}
		else
			{
				document.Titoli_Studio_2.TXTPORCENTAJEAPROBADO.value = "";
				document.Titoli_Studio_2.TXTPORCENTAJEAPROBADO.disabled = true;
			}
		}
	
	//habilitacion campos en curso
	if (valore == "2")
	{
		document.Titoli_Studio_2.AA_STUD.value = "";
		document.Titoli_Studio_2.AA_STUD.disabled = true;
		document.Titoli_Studio_2.VOTO_STUD.value = "";
		document.Titoli_Studio_2.VOTO_STUD.disabled = true;
		document.Titoli_Studio_2.CmbCOD_STAT_RIC.value ="";
		document.Titoli_Studio_2.CmbCOD_STAT_RIC.disabled = true;
		document.Titoli_Studio_2.FL_LAUDE.checked = false;
		document.Titoli_Studio_2.FL_LAUDE.disabled = true;
		document.Titoli_Studio_2.AA_STUD_UltAnno.value = "";
		document.Titoli_Studio_2.AA_STUD_UltAnno.disabled = true;
		document.Titoli_Studio_2.TXTFECHAABANDONO.value = "";
		document.Titoli_Studio_2.TXTFECHAABANDONO.disabled = true;
		document.Titoli_Studio_2.TXTPORCENTAJEAPROBADO.value = "";
		document.Titoli_Studio_2.TXTPORCENTAJEAPROBADO.disabled = true;
		
		document.Titoli_Studio_2.TXTA�OENCURSO.value = "";
		document.Titoli_Studio_2.TXTA�OENCURSO.disabled = false;
		document.Titoli_Studio_2.TXTPORCENTAJECURSADO.value = "";
		document.Titoli_Studio_2.TXTPORCENTAJECURSADO.disabled = false;
			
		
		if ((document.Titoli_Studio_2.CmbCOD_LIV_STUD.value == "TER") ||  (document.Titoli_Studio_2.CmbCOD_LIV_STUD.value == "UNI") ||  (document.Titoli_Studio_2.CmbCOD_LIV_STUD.value == "POS"))
			{
			document.Titoli_Studio_2.TXTPORCENTAJECURSADO.value = "";
			document.Titoli_Studio_2.TXTPORCENTAJECURSADO.disabled = false;
			}
		else
			{
			document.Titoli_Studio_2.TXTPORCENTAJECURSADO.value = "";
			document.Titoli_Studio_2.TXTPORCENTAJECURSADO.disabled = true;			
			}
		}


	if (valore == "4")
	{
		document.Titoli_Studio_2.AA_STUD.value = "";
		document.Titoli_Studio_2.AA_STUD.disabled = true;
		document.Titoli_Studio_2.VOTO_STUD.value = "";
		document.Titoli_Studio_2.VOTO_STUD.disabled = true;
		document.Titoli_Studio_2.CmbCOD_STAT_RIC.value ="";
		document.Titoli_Studio_2.CmbCOD_STAT_RIC.disabled = true;
		document.Titoli_Studio_2.FL_LAUDE.checked = false;
		document.Titoli_Studio_2.FL_LAUDE.disabled = true;
		document.Titoli_Studio_2.AA_STUD_UltAnno.value = "";
		document.Titoli_Studio_2.AA_STUD_UltAnno.disabled = true;
		document.Titoli_Studio_2.TXTFECHAABANDONO.value = "";
		document.Titoli_Studio_2.TXTFECHAABANDONO.disabled = true;
		document.Titoli_Studio_2.TXTPORCENTAJEAPROBADO.value = "";
		document.Titoli_Studio_2.TXTPORCENTAJEAPROBADO.disabled = true;
		
		document.Titoli_Studio_2.TXTA�OENCURSO.value = "";
		document.Titoli_Studio_2.TXTA�OENCURSO.disabled = true;
		document.Titoli_Studio_2.TXTPORCENTAJECURSADO.value = "";
		document.Titoli_Studio_2.TXTPORCENTAJECURSADO.disabled = true;
			
		
		if ((document.Titoli_Studio_2.CmbCOD_LIV_STUD.value == "TER") ||  (document.Titoli_Studio_2.CmbCOD_LIV_STUD.value == "UNI") ||  (document.Titoli_Studio_2.CmbCOD_LIV_STUD.value == "POS"))
			{
			document.Titoli_Studio_2.TXTPORCENTAJECURSADO.value = "";
			document.Titoli_Studio_2.TXTPORCENTAJECURSADO.disabled = true;
			}
		else
			{
			document.Titoli_Studio_2.TXTPORCENTAJECURSADO.value = "";
			document.Titoli_Studio_2.TXTPORCENTAJECURSADO.disabled = true;			
			}
		}
		
		
}


// In caso di errore restituito dalla pagina di salvataggio mantiene attivi/disattivi i corretti campi
// La funzione viene attivata dall'EventHandler onLoad collocato sulla primo tag <IMG SRC="immagine.gif" ..
function CheckDisabledFields()
{
	if (document.Titoli_Studio_2.COD_STAT_STUD.value == "1") 
	{
		// habilitacion campos abandono'
		document.Titoli_Studio_2.TXTFECHAABANDONO.value = "";
		document.Titoli_Studio_2.TXTFECHAABANDONO.disabled = false;
		document.Titoli_Studio_2.TXTPORCENTAJEAPROBADO.value = "";
		document.Titoli_Studio_2.TXTPORCENTAJEAPROBADO.disabled = false;
		document.Titoli_Studio_2.AA_STUD_UltAnno.value = "";
		document.Titoli_Studio_2.AA_STUD_UltAnno.disabled = false;
		
		document.Titoli_Studio_2.TXTA�OENCURSO.disabled = true;
		document.Titoli_Studio_2.TXTPORCENTAJECURSADO.disabled = true;
		
		if ((document.Titoli_Studio_2.CmbCOD_LIV_STUD.value == "POL") || (document.Titoli_Studio_2.CmbCOD_LIV_STUD.value == "POS") || (document.Titoli_Studio_2.CmbCOD_LIV_STUD.value == "PRI") || (document.Titoli_Studio_2.CmbCOD_LIV_STUD.value == "SEC"))
		{		
		
			document.Titoli_Studio_2.FL_LAUDE.checked = false;	
			document.Titoli_Studio_2.FL_LAUDE.disabled = true;
	
		}
		
		document.Titoli_Studio_2.AA_STUD.value = "";
		document.Titoli_Studio_2.AA_STUD.disabled = true;
		document.Titoli_Studio_2.VOTO_STUD.value = "";
		document.Titoli_Studio_2.VOTO_STUD.disabled = true;
		document.Titoli_Studio_2.FL_LAUDE.value = "";
		document.Titoli_Studio_2.FL_LAUDE.disabled = true;
		//document.Titoli_Studio_2.CmbCOD_STAT_RIC.value = "";
		document.Titoli_Studio_2.CmbCOD_STAT_RIC.disabled = true;
		
	}
	if (document.Titoli_Studio_2.COD_STAT_STUD.value == "2")
		{
		// habilitacion campos en curso'
	
		document.Titoli_Studio_2.TXTA�OENCURSO.disabled = false;
		document.Titoli_Studio_2.TXTPORCENTAJECURSADO.disabled = false;
		
		if ((document.Titoli_Studio_2.CmbCOD_LIV_STUD.value == "POL") || (document.Titoli_Studio_2.CmbCOD_LIV_STUD.value == "POS") || (document.Titoli_Studio_2.CmbCOD_LIV_STUD.value == "PRI") || (document.Titoli_Studio_2.CmbCOD_LIV_STUD.value == "SEC"))
		{	
			document.Titoli_Studio_2.FL_LAUDE.checked = false;
			document.Titoli_Studio_2.FL_LAUDE.disabled = true;
			
		}
	// Deshabilitacion campos relativos a conseguido
		document.Titoli_Studio_2.AA_STUD.value = "";
		document.Titoli_Studio_2.AA_STUD.disabled = true;
		document.Titoli_Studio_2.VOTO_STUD.value = "";
		document.Titoli_Studio_2.VOTO_STUD.disabled = true;
		document.Titoli_Studio_2.FL_LAUDE.value = "";
		document.Titoli_Studio_2.FL_LAUDE.disabled = true;
		document.Titoli_Studio_2.CmbCOD_STAT_RIC.disabled = true;
		//document.Titoli_Studio_2.CmbCOD_STAT_RIC.value = "";
		
		document.Titoli_Studio_2.TXTFECHAABANDONO.disabled = true;
		document.Titoli_Studio_2.TXTPORCENTAJEAPROBADO.disabled = true;
		document.Titoli_Studio_2.AA_STUD_UltAnno.disabled = true;
		
		}
		
	if (document.Titoli_Studio_2.COD_STAT_STUD.value == "0")
		{
		// Abilitazione campi relativi a status 'conseguito'
		
		document.Titoli_Studio_2.AA_STUD.disabled = false;
		document.Titoli_Studio_2.VOTO_STUD.disabled = false;
		document.Titoli_Studio_2.FL_LAUDE.checked = false;
		//document.Titoli_Studio_2.FL_LAUDE.disabled = false;
		
		document.Titoli_Studio_2.CmbCOD_STAT_RIC.disabled = false;
		//document.Titoli_Studio_2.CmbCOD_STAT_RIC.value = "";
		
		// Disabilitazione campi relativi a status 'non conseguito/in frequenza'
		document.Titoli_Studio_2.TXTFECHAABANDONO.value = "";
		document.Titoli_Studio_2.TXTFECHAABANDONO.disabled = true;
		document.Titoli_Studio_2.TXTPORCENTAJEAPROBADO.value = "";
		document.Titoli_Studio_2.TXTPORCENTAJEAPROBADO.disabled = true;
		document.Titoli_Studio_2.AA_STUD_UltAnno.value = "";
		document.Titoli_Studio_2.AA_STUD_UltAnno.disabled = true;
		document.Titoli_Studio_2.TXTA�OENCURSO.value = "";
		document.Titoli_Studio_2.TXTA�OENCURSO.disabled = true;
		document.Titoli_Studio_2.TXTPORCENTAJECURSADO.value = "";
		document.Titoli_Studio_2.TXTPORCENTAJECURSADO.disabled = true;
		
		
		

		if ((document.Titoli_Studio_2.CmbCOD_LIV_STUD.value == "POL") || (document.Titoli_Studio_2.CmbCOD_LIV_STUD.value == "POS") || (document.Titoli_Studio_2.CmbCOD_LIV_STUD.value == "PRI") || (document.Titoli_Studio_2.CmbCOD_LIV_STUD.value == "SEC"))
			{
			document.Titoli_Studio_2.FL_LAUDE.disabled = false;
			}
		}
		
		
	if (document.Titoli_Studio_2.COD_STAT_STUD.value == "4")
		{
		// habilitacion campos en curso'
	
		document.Titoli_Studio_2.TXTA�OENCURSO.disabled = true;
		document.Titoli_Studio_2.TXTPORCENTAJECURSADO.disabled = true;
		
		if ((document.Titoli_Studio_2.CmbCOD_LIV_STUD.value == "POL") || (document.Titoli_Studio_2.CmbCOD_LIV_STUD.value == "POS") || (document.Titoli_Studio_2.CmbCOD_LIV_STUD.value == "PRI") || (document.Titoli_Studio_2.CmbCOD_LIV_STUD.value == "SEC"))
		{	
			document.Titoli_Studio_2.FL_LAUDE.checked = false;
			document.Titoli_Studio_2.FL_LAUDE.disabled = true;
			
		}
	// Deshabilitacion campos relativos a conseguido
		document.Titoli_Studio_2.AA_STUD.value = "";
		document.Titoli_Studio_2.AA_STUD.disabled = true;
		document.Titoli_Studio_2.VOTO_STUD.value = "";
		document.Titoli_Studio_2.VOTO_STUD.disabled = true;
		document.Titoli_Studio_2.FL_LAUDE.value = "";
		document.Titoli_Studio_2.FL_LAUDE.disabled = true;
		document.Titoli_Studio_2.CmbCOD_STAT_RIC.disabled = true;
		//document.Titoli_Studio_2.CmbCOD_STAT_RIC.value = "";
		
		document.Titoli_Studio_2.TXTFECHAABANDONO.disabled = true;
		document.Titoli_Studio_2.TXTPORCENTAJEAPROBADO.disabled = true;
		document.Titoli_Studio_2.AA_STUD_UltAnno.disabled = true;
		
		}

}
</script>


<script language="javascript" src="Controlli.js">
</script>


<%
	sub CreaComboCOD_STAT_STUD(Codice)
	dim stringa_frequenza,aFrequenza

		sSql="SELECT ID_CAMPO_DESC " &_
			"FROM DIZ_DATI " &_
			"WHERE " &_
			"ID_TAB='TISTUD' AND ID_CAMPO='COD_STAT_STUD'"

'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
		set Rst=CC.Execute(sSql)
		stringa_frequenza=Rst("ID_CAMPO_DESC")
		aFrequenza=split(stringa_frequenza,"|")
	%>
		<select name="COD_STAT_STUD" class="textblack" id="COD_STAT_STUD" onChange="return ActivInputFields(this.value);">

	<%	for i=0 to Ubound(aFrequenza) step 2
			if aFrequenza(i) <> "3" then
				Response.Write "<option value='" & aFrequenza(i) & "'"
				if aFrequenza(i)=Codice then Response.Write " selected"
				Response.Write ">" & Ucase((aFrequenza(i+1))) & "</option>"
			end if
		next
	%>	
	</select>
	<%	Rst.Close
		set Rst=nothing
	end sub
%>


<center>
<form name="Titoli_Studio_2" Action="Salva_Tit_Stud.asp" Method="post" OnSubmit="return valida(this)">
<input type="hidden" name="PrecNES" value>
<input type="hidden" value="<%=Request("IdPers")%>" name="IdPers">
<input type="hidden" value="<%=Request("LivMenu")%>" name="LivMenu">
<input type="hidden" size="50" name="txtoggi" value="<%=ConvDateToString(Now())%>">
<input type="hidden" value="<%=Request("IND_FASE")%>" name="IND_FASE">	
<input type="hidden" value="<%=Request.ServerVariables("URL")%>" name="back">
<input type="hidden" value="<%=Request("BackPage")%>" name="BackPage">
<input type="hidden" name="txtOggi" value="<%=ConvDateToString(Date())%>">

<table border="0" width="500" CELLPADDING="0" cellspacing="0">
	<tr>
		<td align="left" class="sfondomenu" width="199"><b><span class="tbltext0"> 
          <b>&nbsp;NIVEL EDUCATIVO</b></span></td>

		<td width="25" valign="bottom" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">
			<img border="0" src="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
		<td width="30%" valign="middle" align="right" class="tbltext1" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">(*) 
          campo obligatorio</td>
	</tr>
	</table>
	
	<table border="0" width="500" CELLPADDING="0" cellspacing="0">
    <tr>
		<td class="sfondocomm" align="left" class="tbltext1">
		Ingreso del t�tulo de estudio. <br>
			Indicar a continuaci�n la informaci�n relativa a titulos de estudio cursados u obtenidos.
			<br>Completar los campos inferiores y presionar <b>Enviar</b> para guardar la modificaci�n. 
			</td> 
		<td class="sfondocomm"><a onmouseover="javascript: window.status=' '; return true; " href="Javascript:Show_Help('/Pgm/Help/BilancioCompetenze/Competenze/TitoliStudio/Add_Titoli_Studio')">
			<img src="<%=Session("Progetto")%>/images/Help.gif" border="0" align="right" alt="Help">
			</a>
		</td>
    </tr>
 	<tr height="2">
		<td class="sfondocomm" width="100%" colspan="2" background="<%=Session("Progetto")%>/images/separazione.gif">
		</td>
	</tr>
</table>	


<table border="0" cellpadding="0" cellspacing="1" width="500" style="font-family: Verdana; font-size: 8pt; color:#ffffff">
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr>	
			<td align="left" class="tbltext1">
				<p align="left">
				<strong>Nivel*</strong>
				</p>
			</td>
			<td align="left" width="60%">
				<p align="left">
			
<%
Sql = " SELECT COD_LIV_STUD"
Sql = Sql &  " FROM TISTUD"
Sql = Sql &  " WHERE "
'COD_LIV_STUD = 'NES' AND"
Sql = Sql &  "  ID_PERSONA = " & IDP

'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
	Set Rs = cc.Execute (Sql)
		if not rs.eof then		 
			 'condizione per CreateCombo(UtCod_TIT_STUD) 
			 'nel caso sono gi� stati registrati titoli di studio
				Condiz = " AND CODICE <> 'NES' " 
			rs.close	
		end if
	set rs = Nothing


			UtCod_TIT_STUD= "LSTUD|0|" & date() & "||CmbCOD_LIV_STUD' onChange='return ActivInputLivello(this.value);'|  " & Condiz & "ORDER BY VALORE"			
			CreateCombo(UtCod_TIT_STUD)
%>
			</td>
		</tr>
	
				<input class="textblacka" type="hidden" name="txtcodigo" style="width=250px" disabled>

				
		<tr>
			<td align="left" colspan="1" class="tbltext1">
	 			<b>Titulo*</b>
	 		</td>
	 		<td class="tbltext1">
				<input class="textblacka" readonly type="text" name="txtSpecifico" style="width=250px">
				<input type="hidden" name="txtTipoSpecifico">
				<a href="Javascript:SelSpecifico()" ID="imgPunto2" name="imgPunto2"><img border="0" src="<%=Session("Progetto")%>/images/bullet1.gif"></a>
			</td>
		</tr>
		 <tr height="20">
		    <td align="left" colspan="1" class="tbltext1">
				<p align="left"><b>Orientaci�n <br>(indicar solo para <br>Secundario T�cnico) &nbsp;</b>
		    </td>
		    <td align="left" width="60%">
				<input style="TEXT-TRANSFORM: uppercase;width:300px; " class="textblacka" maxlength="50" name="DESC_ISTITUTO">
		    </td>
		</tr>
		<tr height="20">
			<td class="tbltext1" align="left" colspan="1">
				<p align="left"><b>Pa�s donde <br>curso los estudios</b>
			</td>
			<td colspan="2">
			<%
			UtCod_stat_Freq= "STATO|0|" & date() & "|UY|CmbCOD_STAT_FREQ|ORDER BY DESCRIZIONE"			
			CreateCombo(UtCod_stat_Freq)
			%>
			</td>
		<tr>
	<tr height="20">
		<td align="left" colspan="1">
			<p align="left" class="tbltext1"><strong>Estado del Estudio</strong>
		</td>
		<td>
		<%CreaComboCOD_STAT_STUD("0")%>
			
		</td>
    </tr>
	<tr height="15">
		<td colspan="2">&nbsp;
		</td>
	</tr>
	<tr>
		<td colspan="2" height="2" bgcolor="#3399cc">
		</td>
	</tr>
	
	
	<!-- SI APROBO ............. -->	
	<tr>
		<td colspan="2">
			<span class="textblack">
			 Si complet� sus estudios llene estos campos. 
			</span>
		</td>
	</tr>
	<tr height="15">
		<td colspan="2">
		</td>
	</tr>


	<tr height="20">
		<td class="tbltext1" align="left" colspan="1">
			<b>A�o Finalizaci�n&nbsp;*</b> (aaaa)
		</td>
		<td align="left" class="tbltext1" colspan="2" width="60%">
				<input class="textblacka" size="4" maxlength="4" name="AA_STUD">
				&nbsp;
		</td>
    </tr>

    <tr>
		<td align="left" colspan="1" class="tbltext1">
			<b>Promedio</b>
		</td>
		<td> <input class="MyTextBox" size="4" maxlength="4" name="VOTO_STUD">
		</td>
	</tr>

	<tr>
		<td align="left" class="tbltext1">
			<b>Con honores</b>
		</td>
		<td><input name="FL_LAUDE" type="checkbox" VALUE="S">		
		</td>
	</tr>
	<tr height="20">
		<td class="tbltext1" align="left" colspan="1">
			<p align="left"><b>Reconocido en</b></td>
		<td colspan="2">
			<%
			UtCod_Stat_Ric= "STATO|0|" & date & "|UY|CmbCOD_STAT_RIC|ORDER BY DESCRIZIONE"			
			CreateCombo(UtCod_Stat_Ric)
			%>
		</td>
	</tr>
	<tr height="15">
		<td colspan="2">
		</td>
	</tr>
	<tr>
		<td colspan="2" height="2" bgcolor="#3399cc"><td>
	</tr>
	
	
	<!-- SI ABANDONO ............. -->	
	
	<tr>
		<td colspan="2">
			<span class="textblack">
			 Si abandon� sus estudios complete estos campos. 
			</span>
		</td>
	</tr>
	<tr height="15">
		<td colspan="2">
		</td>
	</tr>
		
	 <tr height="20">
		<td class="tbltext1" align="left" colspan="1">
			<b>Ultimo a�o aprobado</b><br>
		</td>

		<td align="left" class="tbltext1" colspan="2" width="60%">
				<input class="textblacka" size="2" maxlength="1" name="AA_STUD_UltAnno">
				&nbsp;
		</td>
    </tr>
    
    <tr>
		<td align="left" colspan="1" class="tbltext1"><b>
			Fecha de abandono&nbsp;</b><br>(dd/mm/aaaa)</font>
		</td>
		<td>
			<input class="textblacka" maxlength="10" size="11" name="TXTFECHAABANDONO">
		</td>
	</tr>
    
    
    <tr>
		<td align="left" colspan="1" class="tbltext1"><b>
			Porcentaje de carrera aprobado&nbsp;%<br>(Disponible para niveles superiores a secundario)</b></font>
		</td>
		<td>
			<input class="textblacka" maxlength="2" name="TXTPORCENTAJEAPROBADO" disabled size="4">
		</td>
	</tr>
	
	
<tr>
	<td colspan="2">&nbsp;</td>
</tr>


	<tr>
		<td colspan="2" height="2" bgcolor="#3399cc"><td>
	</tr>

	<!-- SI ESTA EN CURSO ............. -->	
	
	<tr>
		<td colspan="2">
			<span class="textblack">
			 Si a�n sigue cursando complete estos campos. 
			</span>
		</td>
	</tr>
	<tr height="15">
		<td colspan="2">
		</td>
	</tr>
		
	 <tr height="20">
		<td class="tbltext1" align="left" colspan="1">
			<b>A�o que esta cursando</b><br>
		</td>

		<td align="left" class="tbltext1" colspan="2" width="60%">
				<input class="textblacka" size="2" maxlength="1" name="TXTA�OENCURSO" disabled>
				&nbsp;
		</td>
    </tr>
    
    <tr>
		<td align="left" colspan="1" class="tbltext1"><b>
			Porcentaje de carrera cursado&nbsp;</b></font>%
		</td>
		<td>
			<input class="textblacka" maxlength="2" name="TXTPORCENTAJECURSADO" size="4" disabled>
		</td>
	</tr>
    
<tr>
	<td colspan="2">&nbsp;</td>
</tr>

	<tr>
		<td colspan="2" height="2" bgcolor="#3399cc"><td>
	</tr>
	
<tr>
	<td class="tbltext1"><b>&nbsp;�Piensa seguir estudiando?</b></td>
	<td align="left" class="tbltext1">&nbsp;<input type="radio" name="estud" value = "S">SI&nbsp;<input type="radio" name="estud" value = "N" checked>NO
	</td>
</tr>

<tr>
	<td><br><br></td>
	<td><br><br></td>
	
</tr>

<tr>
		<td align="middle" colspan="3">
			<a href="javascript:goToPage('Titoli_Studio.asp')"><img alt="Indietro" border="0" src="<%=Session("Progetto")%>/images/indietro.gif"></a>
			<input type="image" src="<%=Session("Progetto")%>/images/conferma.gif" border="0" value="Salva" onLoad="CheckDisabledFields()">
		</td>
	</tr>
	
</table>

</form>
</center>	
<!--#include virtual="/include/Closeconn.asp"-->
<!-- #include Virtual="/strutt_coda2.asp" -->
