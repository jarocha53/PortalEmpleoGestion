<!-- #Include Virtual="/strutt_testa2.asp" -->
<!--#include Virtual = "/include/DecCod.asp"-->
<!-- #Include Virtual="/Pgm/BilancioCompetenze/Utils.asp" -->
<!--#include Virtual = "/Include/OpenConn.asp"-->
<!-- #Include Virtual="/Pgm/BilancioCompetenze/ValidaDtNascita.asp" -->
<!-- #Include Virtual="/Include/ControlDateVB.asp" -->


<script language="javascript">
<!--
<!--#include Virtual = "/Include/ControlNum.inc"-->
<!-- #Include Virtual="/Include/ControlString.inc" -->

function valida(Obj)
{

if (Obj.CmbCOD_LIV_STUD.value != "NES")
{
				Obj.DESC_ISTITUTO.value=TRIM(Obj.DESC_ISTITUTO.value)
				Obj.VOTO_STUD.value=TRIM(Obj.VOTO_STUD.value)
				Obj.AA_STUD.value=TRIM(Obj.AA_STUD.value)
				Obj.AA_STUD_UltAnno.value.value=TRIM(Obj.AA_STUD_UltAnno.value)
				Obj.NUM_AA_FREQ.value=TRIM(Obj.NUM_AA_FREQ.value)
				Obj.NUM_ESAMI.value=TRIM(Obj.NUM_ESAMI.value)      

			    
			if (Obj.CmbCOD_LIV_STUD.options[Obj.CmbCOD_LIV_STUD.selectedIndex].value == "")
			{
				alert("Il campo Livello � obbligatorio")
				return false;
			}
	
			if (Obj.txtSpecifico.value == "") 
			{
				alert("Il campo Titolo � obbligatorio ")
				return false;
			}	
	

			if (Obj.CmbCOD_STAT_FREQ.options[Obj.CmbCOD_STAT_FREQ.selectedIndex].value == "")
			{
			    alert("Il campo Frequentato in � obbligatorio")
			    Obj.CmbCOD_STAT_FREQ.focus()
				return false;
			 }
	
			if (!ValidateInputStringWithNumber(Obj.DESC_ISTITUTO.value))
			{
				alert("Il campo Presso � formalmente errato")
				Obj.DESC_ISTITUTO.focus() 
				return false;
			}

	
			if (Obj.COD_STAT_STUD.value == "0")
			{
				if (Obj.AA_STUD.value == "")
				{
					alert("Il campo Anno � obbligatorio se il titolo � conseguito")
					Obj.AA_STUD.focus() 
					return false;
				}
			}	
	
			if (!Obj.AA_STUD.value == "") 
				{
					if (!isNum(Obj.AA_STUD.value))
					{
						alert("Il campo Anno deve essere numerico")
						Obj.AA_STUD.focus() 
						return false;
					}
					if (Obj.AA_STUD.value < 1900)
					{
						alert("Il campo Anno della deve essere superiore al 1900")
						return false;
					}
					if (Obj.AA_STUD.value  > <%=year(Date)%>)
						{
						alert("Il campo Anno non pu� essere superiore all'anno corrente")
						Obj.AA_STUD.focus() 
						return false;	
						}
					if (ValidaAnnoNascita(Obj.AA_STUD.value))
						{ 
						alert("Il campo Anno deve essere successivo alla data di nascita") 
						Obj.AA_STUD.focus() 
						return false; 
						} 	
						
					if ((Obj.COD_STAT_STUD.value == "1") || (Obj.COD_STAT_STUD.value == "2"))
						{
							alert("Il campo Anno deve essere indicato solo se il titolo di studio � conseguito")
							Obj.AA_STUD.focus() 
						return false;
						}
					
				}
	
	
			 if (Obj.VOTO_STUD.value == "") 
				{
					if (Obj.COD_STAT_STUD.value == "0")
						{
							alert("Il campo Voto � obbligatorio quando il titolo � conseguito")
							Obj.VOTO_STUD.focus() 
							return false;
						}
				}	


	
			 if (!Obj.VOTO_STUD.value == "") 
				{
				 	if ((Obj.COD_STAT_STUD.value == "1") || (Obj.COD_STAT_STUD.value == "2"))
						{
						alert("Il campo Voto non deve essere inserito se il titolo non � conseguito")
						Obj.VOTO_STUD.focus() 
						return false;
						}
				}	
	
	
	
			if ((Obj.COD_STAT_STUD.value == "1") || (Obj.COD_STAT_STUD.value == "2"))
					{
						if (Obj.FL_LAUDE.checked == true)
						{
						alert("Il campo Cum Laude pu� essere contrassegnato solo se la laurea � stata conseguita")
						Obj.FL_LAUDE.focus() 
						return false;
						}
					}
	
			if (Obj.CmbCOD_STAT_RIC.options[Obj.CmbCOD_STAT_RIC.selectedIndex].value == "")
					{
					 alert("Il campo Riconosciuto in � obbligatorio")
					 Obj.CmbCOD_STAT_RIC.focus()
					return false;
					}	
	
	
	
			if ((Obj.COD_STAT_STUD.value == "1") || (Obj.COD_STAT_STUD.value == "2"))
			{
				if (Obj.AA_STUD_UltAnno.value == "")
				{
				alert("Il campo Ultimo anno frequentato  � obbligatorio se il titolo non � conseguito")
				Obj.AA_STUD_UltAnno.focus()
				return false;
				}
			}
	
			 if (!Obj.AA_STUD_UltAnno.value == "") 
			{
				if (!isNum(Obj.AA_STUD_UltAnno.value))
					{
						alert("Il campo Ultimo anno frequentato deve essere numerico")
						Obj.AA_STUD_UltAnno.focus() 
						return false;
					}
					
					if	(Obj.AA_STUD_UltAnno.value < 1900)
					{
						alert("Il campo Ultimo anno frequentato della deve essere superiore al 1900")
						Obj.AA_STUD_UltAnno.focus() 
						return false;
					}	
						
				if (Obj.AA_STUD_UltAnno.value  > <%=year(Date)%>)
					{
						alert("Il campo Ultimo anno frequentato non pu� essere superiore all'anno corrente")
						Obj.AA_STUD_UltAnno.focus() 
						return false;	
					}
				if (ValidaAnnoNascita(Obj.AA_STUD_UltAnno.value))
					{ 
					alert("L'anno del campo Ultimo anno frequentato deve essere successivo a quello di nascita") 
					Obj.AA_STUD_UltAnno.focus() 
					return false; 
					} 
				if (!((Obj.COD_STAT_STUD.value == "1") || (Obj.COD_STAT_STUD.value == "2")))
					{
					alert("L'anno del campo Ultimo anno frequentato deve essere indicato solo se il titolo di studio non � conseguito")
					Obj.AA_STUD_UltAnno.focus() 
					return false;
					}
			 }

					
					
					if (!Obj.NUM_AA_FREQ.value == "")
					{			
						if (!isNum(Obj.NUM_AA_FREQ.value))
							{
								alert("Il campo Anni frequenza deve essere numerico")
								Obj.NUM_AA_FREQ.focus()
								return false;
							}
					}

					
					
					if (!Obj.NUM_ESAMI.value == "")
					{
						if (!isNum(Obj.NUM_ESAMI.value))
							{
								alert("Il campo Numero Esami deve essere numerico")
								Obj.NUM_ESAMI.focus()
								return false;
							}
						// il mumero esami deve essere speicificato solo se il titolo non � conseguito
						if (!((Obj.COD_STAT_STUD.value == "1") || (Obj.COD_STAT_STUD.value == "2")))
							{
							alert("L'anno del campo Numero esami deve essere indicato solo se il titolo di studio non � conseguito")
							Obj.NUM_ESAMI.focus() 
							return false;
							}				
						// il mumero esami deve essere speicificato solo se il titolo � una laurea o un diploma post universitario
						if (!((Obj.CmbCOD_LIV_STUD.options[Obj.CmbCOD_LIV_STUD.selectedIndex].value == "LAU") || (Obj.CmbCOD_LIV_STUD.options[Obj.CmbCOD_LIV_STUD.selectedIndex].value == "QPD")))
							{
							alert("L'anno del campo Numero esami deve essere indicato solo se il titolo di studio non conseguito � una laurea o un diploma post-universitario")
							Obj.NUM_ESAMI.focus() 
							return false;
							}				
					
					}				
		
	}
}


function ActivInputLivello(valore)
	{
	var PrecNes
	
	document.Titoli_Studio_2.txtSpecifico.value = "";
	


			if (valore == "NES")
			{
				document.Titoli_Studio_2.FL_LAUDE.disabled = true;	
				document.Titoli_Studio_2.CmbCOD_STAT_FREQ.value ="";
				document.Titoli_Studio_2.CmbCOD_STAT_FREQ.disabled = true;
				document.Titoli_Studio_2.DESC_ISTITUTO.value = "";
				document.Titoli_Studio_2.DESC_ISTITUTO.disabled = true;
				document.Titoli_Studio_2.COD_STAT_STUD.value ="";
				document.Titoli_Studio_2.COD_STAT_STUD.disabled =true;
				document.Titoli_Studio_2.AA_STUD.value = "";
				document.Titoli_Studio_2.AA_STUD.disabled = true;
				document.Titoli_Studio_2.VOTO_STUD.value = "";
				document.Titoli_Studio_2.VOTO_STUD.disabled = true;
				document.Titoli_Studio_2.CmbCOD_STAT_RIC.value ="";
				document.Titoli_Studio_2.CmbCOD_STAT_RIC.disabled = true;
				document.Titoli_Studio_2.AA_STUD_UltAnno.value = "";
				document.Titoli_Studio_2.AA_STUD_UltAnno.disabled = true;
				document.Titoli_Studio_2.NUM_AA_FREQ.value = "";
				document.Titoli_Studio_2.NUM_AA_FREQ.disabled = true;
				document.Titoli_Studio_2.NUM_ESAMI.value = "";
				document.Titoli_Studio_2.NUM_ESAMI.disabled = true;
				document.Titoli_Studio_2.PrecNES.value = "Yes";
				SelSpecifico()
			}
			if ((valore != "NES") && (document.Titoli_Studio_2.PrecNES.value == "Yes"))
			{
				document.Titoli_Studio_2.CmbCOD_STAT_FREQ.value ="IT";
				document.Titoli_Studio_2.CmbCOD_STAT_FREQ.disabled = false;	
				document.Titoli_Studio_2.DESC_ISTITUTO.disabled = false;
				document.Titoli_Studio_2.COD_STAT_STUD.value =0;
				document.Titoli_Studio_2.COD_STAT_STUD.disabled =false;			
				document.Titoli_Studio_2.AA_STUD.disabled = false;
				document.Titoli_Studio_2.VOTO_STUD.disabled = false;
				document.Titoli_Studio_2.CmbCOD_STAT_RIC.value ="IT";
				document.Titoli_Studio_2.CmbCOD_STAT_RIC.disabled = false;
				document.Titoli_Studio_2.PrecNES.value = "No";
			}	
	
	
	//******************************************************************************************
			// gestione checkbox 'Cum Laude'
			if ((document.Titoli_Studio_2.COD_STAT_STUD.value == "0") && (valore == "LAU"))
				{
				document.Titoli_Studio_2.FL_LAUDE.disabled = false;	
				}
				else
				{
				document.Titoli_Studio_2.FL_LAUDE.checked = false;	
				document.Titoli_Studio_2.FL_LAUDE.disabled = true;	
				}
				
				
				
			if ((document.Titoli_Studio_2.COD_STAT_STUD.value == "1") || (document.Titoli_Studio_2.COD_STAT_STUD.value == "2"))
				{
				if ((valore == "LAU") || (valore == "QPD"))
						{
						document.Titoli_Studio_2.NUM_ESAMI.disabled = false;	
						}
					else 
						{
						document.Titoli_Studio_2.NUM_ESAMI.value = "";	
						document.Titoli_Studio_2.NUM_ESAMI.disabled = true;	
						}	
			
			   }
			
			

		}
		
	
function ActivInputFields(valore)
{
//alert(document.Titoli_Studio_2.CmbCOD_LIV_STUD.value)
	if ((valore == "1") || (valore == "2"))
		{
		// Abilitazione campi relativi a status 'non conseguito/in frequenza'
		document.Titoli_Studio_2.NUM_AA_FREQ.disabled = false;
		document.Titoli_Studio_2.AA_STUD_UltAnno.disabled = false;
			if ((document.Titoli_Studio_2.CmbCOD_LIV_STUD.value == "LAU") || (document.Titoli_Studio_2.CmbCOD_LIV_STUD.value == "QPD"))
			{
			document.Titoli_Studio_2.NUM_ESAMI.disabled = false;		
			document.Titoli_Studio_2.FL_LAUDE.disabled = true;
			document.Titoli_Studio_2.FL_LAUDE.checked = false;
			}
	// Disabilitazione campi relativi a status 'conseguito'
		document.Titoli_Studio_2.AA_STUD.disabled = true;
		document.Titoli_Studio_2.AA_STUD.value = "";
		document.Titoli_Studio_2.VOTO_STUD.disabled = true;
		document.Titoli_Studio_2.VOTO_STUD.value = "";
		}
	else
		{
		// Abilitazione campi relativi a status 'conseguito'
		
		document.Titoli_Studio_2.AA_STUD.disabled = false;
		document.Titoli_Studio_2.VOTO_STUD.disabled = false;
			
		// Disabilitazione campi relativi a status 'non conseguito/in frequenza'
		document.Titoli_Studio_2.NUM_AA_FREQ.disabled = true;
		document.Titoli_Studio_2.NUM_AA_FREQ.value = "";
		document.Titoli_Studio_2.AA_STUD_UltAnno.disabled = true;
		document.Titoli_Studio_2.AA_STUD_UltAnno.value = "";
		document.Titoli_Studio_2.NUM_ESAMI.disabled = true;	
		document.Titoli_Studio_2.NUM_ESAMI.value = "";	
		
			if 	(document.Titoli_Studio_2.CmbCOD_LIV_STUD.value == "LAU")
			{
			document.Titoli_Studio_2.FL_LAUDE.disabled = false;
			}
		}

}


// In caso di errore restituito dalla pagina di salvataggio mantiene attivi/disattivi i corretti campi
// La funzione viene attivata dall'EventHandler onLoad collocato sulla primo tag <IMG SRC="immagine.gif" ..
function CheckDisabledFields()
{
	if ((document.Titoli_Studio_2.COD_STAT_STUD.value == "1") || (document.Titoli_Studio_2.COD_STAT_STUD.value == "2"))
		{
		// Abilitazione campi relativi a status 'non conseguito/in frequenza'
		document.Titoli_Studio_2.NUM_AA_FREQ.disabled = false;
		document.Titoli_Studio_2.AA_STUD_UltAnno.disabled = false;
			if ((document.Titoli_Studio_2.CmbCOD_LIV_STUD.value == "LAU") || (document.Titoli_Studio_2.CmbCOD_LIV_STUD.value == "QPD"))
			{
			document.Titoli_Studio_2.NUM_ESAMI.disabled = false;		
			document.Titoli_Studio_2.FL_LAUDE.disabled = true;
			document.Titoli_Studio_2.FL_LAUDE.checked = false;
			}
	// Disabilitazione campi relativi a status 'conseguito'
		document.Titoli_Studio_2.AA_STUD.disabled = true;
		document.Titoli_Studio_2.AA_STUD.value = "";
		document.Titoli_Studio_2.VOTO_STUD.disabled = true;
		document.Titoli_Studio_2.VOTO_STUD.value = "";
		}
	else
		{
		// Abilitazione campi relativi a status 'conseguito'
		
		document.Titoli_Studio_2.AA_STUD.disabled = false;
		document.Titoli_Studio_2.VOTO_STUD.disabled = false;
		
		
		if (document.Titoli_Studio_2.NUM_AA_FREQ)
		{	
		// Disabilitazione campi relativi a status 'non conseguito/in frequenza'
		document.Titoli_Studio_2.NUM_AA_FREQ.disabled = true;
		document.Titoli_Studio_2.NUM_AA_FREQ.value = "";
		document.Titoli_Studio_2.AA_STUD_UltAnno.disabled = true;
		document.Titoli_Studio_2.AA_STUD_UltAnno.value = "";
		document.Titoli_Studio_2.NUM_ESAMI.disabled = true;	
		document.Titoli_Studio_2.NUM_ESAMI.value = "";	
		}
			if 	(document.Titoli_Studio_2.CmbCOD_LIV_STUD.value == "LAU")
			{
			document.Titoli_Studio_2.FL_LAUDE.disabled = false;
			}
		}

}


//-->
</script>
<script language="javascript" src="Controlli.js">
</script>


<%
	sub CreaComboCOD_STAT_STUD(Codice)
	dim stringa_frequenza,aFrequenza

		sSql="SELECT ID_CAMPO_DESC " &_
			"FROM DIZ_DATI " &_
			"WHERE " &_
			"ID_TAB='TISTUD' AND ID_CAMPO='COD_STAT_STUD'"

		set Rst=CC.Execute(sSql)
		stringa_frequenza=Rst("ID_CAMPO_DESC")
		aFrequenza=split(stringa_frequenza,"|")
	%>
		<select name="COD_STAT_STUD" class="textblack" id="COD_STAT_STUD" onChange="return ActivInputFields(this.value);">

	<%	for i=0 to Ubound(aFrequenza) step 2
			Response.Write "<option value='" & aFrequenza(i) & "'"
			if aFrequenza(i)=Codice then Response.Write " selected"
			Response.Write ">" & Ucase((aFrequenza(i+1))) & "</option>"
		next
	%>	
	</select>
	<%	Rst.Close
		set Rst=nothing
	end sub
%>


<center>
<form name="Titoli_Studio_2" Action="Salva_Tit_Stud.asp" Method="post" OnSubmit="return valida(this)">
<input type="hidden" name="PrecNES" value>
<input type="hidden" value="<%=Request("IdPers")%>" name="IdPers">
<input type="hidden" value="<%=Request("LivMenu")%>" name="LivMenu">
<input type="hidden" size="50" name="txtoggi" value="<%=ConvDateToString(Now())%>">
<input type="hidden" value="<%=Request("IND_FASE")%>" name="IND_FASE">	
<input type="hidden" value="<%=Request.ServerVariables("URL")%>" name="back">
<input type="hidden" value="<%=Request("BackPage")%>" name="BackPage">

<table border="0" width="500" CELLPADDING="0" cellspacing="0">
	<tr>
		<td align="left" class="sfondomenu" width="199"><b><span class="tbltext0">
			<b>&nbsp;TITOLI DI STUDIO</b></span>
		</td>

		<td width="25" valign="bottom" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">
			<img border="0" src="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
		<td width="30%" valign="middle" align="right" class="tbltext1" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">(*) campi obbligatori</td>
	</tr>
	</table>
	
	<table border="0" width="500" CELLPADDING="0" cellspacing="0">
    <tr>
		<td class="sfondocomm" align="left" class="tbltext1">
		Inserimento titoli di studio. <br>
			Indica di seguito le informazioni relative agli eventuali titoli di studio a cui hai partecipato.
			<br>Compila i campi sottostanti e premi <b>Invia</b> per salvare le modifiche. 
			</td> 
		<td class="sfondocomm"><a href="Javascript:Show_Help('/Pgm/Help/BilancioCompetenze/Competenze/TitoliStudio/Add_Titoli_Studio')">
			<img src="<%=Session("Progetto")%>/images/Help.gif" border="0" align="right" alt="Help">
			</a>
		</td>
    </tr>
 	<tr height="2">
		<td class="sfondocomm" width="100%" colspan="2" background="<%=Session("Progetto")%>/images/separazione.gif">
		</td>
	</tr>
</table>	


<table border="0" cellpadding="0" cellspacing="1" width="500" style="font-family: Verdana; font-size: 8pt; color:#ffffff">
		<tr>
			<td>&nbsp;</td><td>&nbsp;
			</td>
		</tr>
		<tr>	
			<td align="left" class="tbltext1">
				<p align="left">
				<strong>Livello*</strong>
				</font></p>
			</td>
			<td align="left" width="60%">
				<p align="left">
			
<%
Sql = " SELECT COD_LIV_STUD"
Sql = Sql &  " FROM TISTUD"
Sql = Sql &  " WHERE "
'COD_LIV_STUD = 'NES' AND"
Sql = Sql &  "  ID_PERSONA = " & IDP

	Set Rs = cc.Execute (Sql)
		if not rs.eof then		 
			 'condizione per CreateCombo(UtCod_TIT_STUD) 
			 'nel caso sono gi� stati registrati titoli di studio
				Condiz = " AND CODICE <> 'NES' " 
			rs.close	
		end if
	set rs = Nothing


			UtCod_TIT_STUD= "LSTUD|0|" & date & "||CmbCOD_LIV_STUD' onChange='return ActivInputLivello(this.value);'|  " & Condiz & "ORDER BY VALORE"			
			CreateCombo(UtCod_TIT_STUD)
%>
			</td>
		</tr>

		<tr>
			<td align="left" colspan="1" class="tbltext1">
	 			<b>Titolo*</b>
	 		</td>
	 		<td class="tbltext1">
				<input class="textblacka" readonly type="text" name="txtSpecifico" style="width=250px">
				<input type="hidden" name="txtTipoSpecifico">
				<a href="Javascript:SelSpecifico()" ID="imgPunto2" name="imgPunto2"><img border="0" src="<%=Session("Progetto")%>/images/bullet1.gif"></a>
			</td>
		</tr>
		<tr height="20">
		<td class="tbltext1" align="left" colspan="1">
			<p align="left"><b>Frequentato in*</b></td>
		<td colspan="2">
			<%
			UtCod_stat_Freq= "STATO|0|" & date & "|IT|CmbCOD_STAT_FREQ|ORDER BY DESCRIZIONE"			
			CreateCombo(UtCod_stat_Freq)
			%>
		</td>
	</tr>
	 <tr height="20">
        <td align="left" colspan="1" class="tbltext1">
			<p align="left"><b>Presso &nbsp;</b>
        </td>
        <td align="left" colspan="2" width="60%">
			<input style="TEXT-TRANSFORM: uppercase;width:300px; " class="textblacka" maxlength="50" name="DESC_ISTITUTO">
        </td>
	</tr>
	<tr height="20">
		<td align="left" colspan="1">
			<p align="left" class="tbltext1"><strong>Status*</strong>
		</td>
		<td>
		<%CreaComboCOD_STAT_STUD("0")%>
			
		</td>
    </tr>
	<tr height="15">
		<td colspan="2">&nbsp;
		</td>
	</tr>
	<tr>
		<td colspan="2" height="2" bgcolor="#3399cc">
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<span class="textblack">
			 Se il Titolo di Studio � stato conseguito indica
			 l'<b>Anno </b>e la valutazione riportata.
			</span>
		</td>
	</tr>
	<tr height="20">
		<td class="tbltext1" align="left" colspan="1">
			<b>Anno*&nbsp;</b>(aaaa)
		</td>
		<td align="left" class="tbltext1" colspan="2" width="60%">
				<input class="textblacka" size="11" maxlength="4" name="AA_STUD">
				&nbsp;
		</td>
    </tr>

    <tr>
		<td align="left" colspan="1" class="tbltext1">
			<b>Voto*</b>
		</td>
		<td> <input class="MyTextBox" maxlength="12" name="VOTO_STUD">
		</td>
	</tr>

	<tr>
		<td align="left" class="tbltext1">
			<b>Cum Laude</b></td>
			<td><input name="FL_LAUDE" type="checkbox" VALUE="S" disabled>		
			</td>
	</tr>
	<tr height="20">
		<td class="tbltext1" align="left" colspan="1">
			<p align="left"><b>Riconosciuto in*</b></td>
		<td colspan="2">
			<%
			UtCod_Stat_Ric= "STATO|0|" & date & "|IT|CmbCOD_STAT_RIC|ORDER BY DESCRIZIONE"			
			CreateCombo(UtCod_Stat_Ric)
			%>
		</td>
	</tr>
	<tr height="15">
		<td colspan="2">
		</td>
	</tr>
	<tr>
		<td colspan="2" height="2" bgcolor="#3399cc"><td>
	</tr>
	<tr height="15">
		<td colspan="2">
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<span class="textblack">
			 Altrimenti indica l'ultimo anno frequentato e 
			 indica gli anni di frequenza. 
			</span>
		</td>
	</tr>
	
	 <tr height="20">
		<td class="tbltext1" align="left" colspan="1">
			<b>Ultimo anno frequentato*</b><br>(aaaa)
		</td>
		
		
		<td align="left" class="tbltext1" colspan="2" width="60%">
				<input class="textblacka" size="11" maxlength="4" name="AA_STUD_UltAnno" disabled>
				&nbsp;
		</td>
    </tr>
    
    <tr>
		<td align="left" colspan="1" class="tbltext1"><b>
			Anni Frequenza&nbsp;</b></font>
		</td>
		<td>
		
			<input class="MyTextBox" maxlength="2" name="NUM_AA_FREQ" disabled>
		</td>
	</tr>
	<tr>
		<td align="left" colspan="1" class="tbltext1">
			<b>Nr. Esami sostenuti&nbsp;<b></td>
		<td><input class="MyTextBox" maxlength="3" name="NUM_ESAMI" disabled> </td>
	</tr>
    
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
		<td align="middle" colspan="3">
			<a href="javascript:goToPage('Titoli_Studio.asp')"><img alt="Indietro" border="0" src="<%=Session("Progetto")%>/images/indietro.gif"></a>
			<input type="image" src="<%=Session("Progetto")%>/images/conferma.gif" border="0" value="Salva" onLoad="CheckDisabledFields()">
		</td>
	</tr>
	
</table>

</form>
</center>	
<!--#include virtual="/include/Closeconn.asp"-->
<!-- #include Virtual="/strutt_coda2.asp" -->
