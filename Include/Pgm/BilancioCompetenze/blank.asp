<!-- #include virtual='/util/portallib.asp' -->
<!-- #include file='utils.asp' -->

<html>

<head>
	<LINK REL=STYLESHEET TYPE="text/css" HREF="fogliostile.css">
	<title>E-job - Orientamento</title>
</head>

<body bgcolor="#ffffff" text="#000000">
<TABLE BORDER=0 cellspacing=0 cellpadding=1 WIDTH='570'>
<TR>
	<TD valign?="middle'" align='right'>
		<font FACE="Verdana" size="2"><b>
		Benvenuto nel percorso di orientamento&nbsp;</b></font>
	</TD>
	<td align=left>
		&nbsp;  
    </td>
</TR>
<tr>
	<td colspan=2> 
      <font FACE="Verdana" size="2">
      Seguendo le schermate ti sar� richiesto di inserire
      informazioni che ti aiuteranno a:
      <ul>
        <b>
        <li>Costruire il tuo curriculum analitico;</b> 
      </LI>
      </ul>
      <ul>
        <b>
        <li>Individuare il tuo obiettivo professionale;</b> 
        </LI>
      </ul>
      <ul>
        <b>
        <li>Mettere a punto il tuo progetto professionale;</b> </LI>
      </ul>
      <br>Ti consigliamo di procedere seguendo l�ordine di compilazione
      previsto poich� alcune pagine si generano dinamicamente, cio� si
      completano in base a informazioni inserite in passi antecedenti del
      percorso.<br><br>
      Per iniziare il percorso
      fai clic sulla prima voce visualizzata nell'elenco a sinistra e successivamente
      segui&nbsp;le voci di navigazione presenti all'interno delle schermate.
      <br><br>
      Cliccando su questo simbolo <IMG src="imgs/imghelp.gif">  potrai in qualsiasi
      momento trovare un aiuto utile a comprendere cosa ti viene richiesto, le
      modalit� di inserimento e logica di base della richiesta di informazioni.<br><br>
      </font>
    </td>
</tr>
</TABLE>
<!-- #include file='footer.asp' -->
</body>
</html>
