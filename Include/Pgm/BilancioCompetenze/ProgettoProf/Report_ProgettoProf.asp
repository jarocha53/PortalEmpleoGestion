<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #INCLUDE virtual="pgm/BilancioCompetenze/StringConn.asp" -->
<html>
<head>
<title> Imprimir el Detalle de la Orientaci�n Laboral</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="<%=session("progetto")%>/fogliostile.css" type="text/css">
</head>
<body   bgcolor="#ffffff">
<%

IDP = request("IDP")			
id_figprof = Request("ID_FiGPROF")
Set Conn = server.CreateObject ("ADODB.Connection")
Conn.open strConn

		Sql = " SELECT PROGETTO.ID_PROG_PROF, PROGETTO.COD_ELEMENTO, PROGETTO.ID_FIGPROF, AZ.COD_AZIONE,"
		Sql = Sql &  " (SELECT TADES.DESCRIZIONE FROM TADES WHERE TADES.ISA='0' AND UPPER(TADES.NOME_TABELLA)='CL_AZ' "
		Sql = Sql &  " AND rtrim(TADES.CODICE) LIKE rtrim(COD_AZIONE)) DESCR_AZIONE, "
		Sql = Sql &  " FIG.DENOMINAZIONE FIGPROF, AZ.DESCRIZIONE, AZ.COMPLETATO, PROGETTO.ID_ELEMENTO,AZ.OBIETTIVO,"
		Sql = Sql &  " AZ.OSSERVAZIONI,AZ.DURATA	   		"
		Sql = Sql &  " FROM FIGUREPROFESSIONALI FIG, PERS_AZ_PROF AZ, PERS_PROG_PROF PROGETTO "
		Sql = Sql &  " WHERE "
		Sql = Sql &  " ( PROGETTO.ID_PROG_PROF=AZ.ID_PROG_PROF ) AND ( PROGETTO.ID_FIGPROF=FIG.ID_FIGPROF ) "
		Sql = Sql &  " AND PROGETTO.ID_FIGPROF= " & Request("ID_FIGPROF")
		Sql = Sql &  " AND PROGETTO.ID_PERSONA= " & Request("IDP")
		Sql = Sql &  " ORDER BY AZ.COMPLETATO DESC, PROGETTO.COD_ELEMENTO DESC"

'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
	Set Rs = Conn.Execute (Sql)

	if not rs.eof then
		VV = Rs.getrows()
		ID_PROG_PROF = VV(0,0)
		ID_FIGPROF = VV(2,0)
		DESC_FIGPROF = VV(5,0)
	end if

rs.close
set rs=nothing

IDP=request("IDP")
Set Conn = server.CreateObject ("ADODB.Connection")
Conn.open strConn
Sql= "SELECT NOME,COGNOME FROM PERSONA WHERE ID_PERSONA = " & IDP
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
set rs=Conn.Execute (Sql)
if not rs.eof then NOMINATIVO = Rs(0) & " - " & Rs(1)

%>

<SCRIPT LANGUAGE="JavaScript">
	function stp()
	{
	bb=(document.layers)?document.layers['aa']:(document.getElementById)?document.getElementById('aa').style:document.all['aa'].style;
	
		bb.visibility="hidden"
		self.print();
		bb.visibility="visible"
		
	}
</SCRIPT>

<p align=left>
</p>
<CENTER>
<table border=0 cellspacing=1 cellpadding=0 width='570'>
<tr><td align=left class='sfondocomm'>Orientaci�n Laboral de &nbsp;<B><%=NOMINATIVO%>&nbsp;</small></tr>
<tr><td align=left class='sfondocomm'><small>Para el c�digo ocupacional de<br><B><%=ucase(DESC_FIGPROF)%> </B></td></tr>
</table>
<br>
<%
Response.Write "<table border=0 cellspacing=0 cellpadding=0 width='570'>"
If IsArray(VV) then
	for x = 0 to ubound(VV,2)
		CODICE_ELEMENTO = trim(VV(1,x))

		Select case CODICE_ELEMENTO
			case "FPR"
				ELEMENTO = "<b>Acciones de todo el C�digo Ocupacional</b>" 
			case "CON"
				Sql = ""
				Sql = Sql & "SELECT DENOMINAZIONE FROM CONOSCENZE WHERE ID_CONOSCENZA = " & VV(8,x)
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
				Set Rs = Conn.Execute (Sql)
				ELEMENTO = "<b>Acciones del elemento de Conocimiento:&nbsp;</b>" & Rs(0)  
			case "CAP"
				Sql = ""
				Sql = Sql & "SELECT DENOMINAZIONE FROM CAPACITA WHERE ID_CAPACITA = " & VV(8,x)
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
				Set Rs = Conn.Execute (Sql)
				ELEMENTO = "<b>Acciones del elemento de Capacidad:&nbsp;</b>" & Rs(0) 
			case "COM"
				Sql = ""
				Sql = Sql & "SELECT DENOMINAZIONE FROM COMPETENZE WHERE ID_COMPETENZA = " & VV(8,x)
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
				Set Rs = Conn.Execute (Sql)
				ELEMENTO = "<b>Acciones del elemento de Compentencia:&nbsp;</b>" & Rs(0) 
		End select

		if (x mod 2)= 0 then
				sfondo="class='tblsfondo'"
			else
				sfondo="class='tbltext1'"
		end if
		
		Response.write "<tr><td colspan='4' class='tbltext1'>&nbsp;</td></tr>"		
		
		Response.write "<tr " & sfondo & ">"
			Response.Write "<td colspan='4' class='tbltext1'>"
				Response.Write "&nbsp;" & ELEMENTO 	
			Response.Write "</td>"
		Response.Write "</tr>"

		Response.Write "<tr class='sfondomenu' >"
		Response.Write "<td colspan=4 style='font-size: 2px'>&nbsp;</td>"
		Response.Write "</tr>"
		
		Response.write "<tr " & sfondo & ">"
			Response.Write "<td>&nbsp;</td>"
			Response.Write "<td class='tbltext1'>"
				Response.Write "<b>Acci�n</b>&nbsp;" 
			Response.Write "</td>"
			Response.Write "<td class='tbltext1'>"
			Response.Write  VV(4,x) 	
		Response.Write "</td>"
		Response.Write "</tr>"

		Response.write "<tr " & sfondo & ">"
			Response.Write "<td width='2%'>&nbsp;</td>"
				Response.Write "<td class='tbltext1' width='16%'><b>Desc. Azione</b>&nbsp;</td>"
				Response.Write "<td colspan='2' class='tbltext1'>"  & VV(6,x) 	
			Response.Write "</td>"
		Response.Write "</tr>"

		Response.write "<tr " & sfondo & ">"
			Response.Write "<td>&nbsp;</td>"
			Response.Write "<td class='tbltext1'>"
				Response.Write "<b>Objetivo</b>&nbsp;</td>" 
				Response.Write "<td colspan='2' class='tbltext1'>" 
				Response.Write VV(9,x) 	
			Response.Write "</td>"
		Response.Write "</tr>"

		Response.write "<tr " & sfondo & ">"
			Response.Write "<td>&nbsp;</td>"
			Response.Write "<td  class='tbltext1'>"
			Response.Write "<b>Observaci�n</b>&nbsp;" 
			Response.Write "</td>"
			Response.Write "<td colspan='2' class='tbltext1'>"
			Response.Write VV(10,x) 	
		Response.Write "</td>"
		Response.Write "</tr>"


		Response.write "<tr " & sfondo & ">"
			Response.Write "<td>&nbsp;</td>"
			Response.Write "<td class='tbltext1'>"
				Response.Write "<b>Duraci�n</b>&nbsp;" 
			Response.Write "</td>"
			Response.Write "<td colspan='2' class='tbltext1'>"
			Response.Write  VV(11,x) 	
			Response.Write "</td>"
		Response.Write "</tr>"

		Response.write "<tr " & sfondo & ">"
			Response.Write "<td>&nbsp;</td>"
		
			If VV(7,x) = "S" then 
				Completato = "<font color='#008000' size=1><b>SI</b></font>"
				else 
				Completato = "<font  color='#FF0000' size=1><b>NO</b></font>"
			End if
		Response.Write "<td class='tbltext1'>"
				Response.Write "<b>Completado</b>&nbsp;" 
				
			Response.Write "</td>"
		Response.Write "<td class='tbltext1' colspan='2'>"
		Response.Write Completato
			Response.Write "</td>"
		Response.Write "</tr>"
		
		Response.write "<tr " & sfondo & ">"
			Response.Write "<td colspan='4' class='tbltext1'>"
				Response.Write "&nbsp;" 
		Response.Write "</td>"
		Response.Write "</tr>"
	next
else
		Response.Write "<tr>"
		Response.Write "<td class='tbltext3'></td>"
			Response.Write "<td class='tbltext3'><b>No se han ingresado datos</b></td>"
		Response.Write "</tr>"
end if

Conn.Close
Set conn = Nothing
%>

		<tr>
		<td colspan='4'>&nbsp</td>
		</tr>
	
<tr>
<td colspan=4>&nbsp;</td>
</tr>


</CENTER>
<tr>
<td colspan=4 align=center>
<div id="aa">
<input type='button' value='Imprimir' OnClick='stp()' CLASS='My'>&nbsp; &nbsp;
<input type='button' value='Cancelar' OnClick='top.close()' CLASS='My'>
</div>
</td>
</tr>
</table>
</body>
</html>
