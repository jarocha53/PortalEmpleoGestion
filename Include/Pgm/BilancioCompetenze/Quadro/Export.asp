<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #INCLUDE FILE="../Utils.asp" -->

<%
response.ContentType = "application/vnd.ms-word"
response.AddHeader "Content-Disposition","attachment; filename=QuadroDiSintesi.doc"

	if session("UsrOrientamento")=false then 
		response.redirect "../ErroreAbilitazione.asp"
		response.end
	end if
%>
<html>

<head>
<LINK REL=STYLESHEET TYPE="text/css" HREF="../fogliostile.css">
	<title>E-job - Orientamento</title>
</head>


<body bgcolor="#FFFFFF" style="font-family: Verdana; font-size: 10pt">
<form name='QUADRO' Method='POST'>
<%

Set Conn = server.CreateObject ("ADODB.Connection")
Conn.open strConn

NOMINATIVO= SESSION("nome") & " - " & SESSION("cognome")

'===================
' TITOLI DI STUDIO
'====================
Sql = ""
Sql = Sql & " select tades.descrizione,tistud.aa_stud from tades,tistud "
Sql = Sql & " where "
Sql = Sql & " tistud.cod_tit_stud=tades.codice and tades.nome_tabella='TSTUD' "
Sql = Sql & " and tistud.id_persona=" & IDP
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
Set Rs = Conn.Execute (Sql)
if not rs.eof then
	TISTUD = Rs.getrows()
end if
Set Rs = Nothing

'================================================
' STAGE, TIROCINI, APPRENDISTATO
'================================================
Sql = ""
Sql = Sql & " SELECT ATTIVITA, OUTPUT, CLIENTE, "
Sql = Sql & " DT_INI_ESP_PRO, DT_FIN_ESP_PRO,FL_TIPO_ESP "
Sql = Sql & " FROM PERS_ATT_NON_PROFESS "
Sql = Sql & " WHERE "
Sql = Sql & " PERS_ATT_NON_PROFESS.ID_PERSONA = " & IDP
Sql = Sql & " AND PERS_ATT_NON_PROFESS.FL_ATT ='LA' "
Sql = Sql & " AND FL_TIPO_ESP <>' ' "
Sql = Sql & " ORDER BY FL_ATT "
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
Set Rs = Conn.Execute (Sql)
if not rs.eof then
	STAGE_TIROCINI = Rs.getrows()
end if
Set Rs = Nothing

'================================================
'CORSI DI FORMAZIONE
'================================================

Sql = ""
Sql = Sql & " SELECT FORMAZIONE.COD_TCORSO,"
Sql = Sql & " FORMAZIONE.COD_ATT_FINALE, FORMAZIONE.ORE_DURATA, FORMAZIONE.AA_CORSO"
Sql = Sql & " FROM FORMAZIONE, TADES, BDC_AREA_CORSO "
Sql = Sql & " WHERE "
Sql = Sql & " (TADES.NOME_TABELLA='TISER' AND TADES.CODICE=FORMAZIONE.COD_IST_ER AND TADES.ISA='0') AND "
Sql = Sql & " (BDC_AREA_CORSO.ID_AREA=FORMAZIONE.COD_AREA) AND  "
Sql = Sql & " FORMAZIONE.ID_PERSONA= " & IDP 

'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
Set Rs = Conn.Execute (Sql)
if not rs.eof then
	CORSI_FORM = Rs.getrows()
end if
Set Rs = Nothing

'================================================
' ESPERIENZE LAVORATIVE PROFESSIONALI
'================================================
Sql = ""
Sql = Sql & " SELECT DICHIARAZ.DESC_MANSIONE, ESPRO.DT_INI_ESP_PRO, ESPRO.DT_FIN_ESP_PRO "
Sql = Sql & " FROM ESPRO, DICHIARAZ "
Sql = Sql & " WHERE "
Sql = Sql & " ESPRO.ID_PERSONA = DICHIARAZ.ID_PERSONA AND " 
Sql = Sql & " ESPRO.ID_PERSONA = " & IDP
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
Set Rs = Conn.Execute (Sql)
if not rs.eof then
	ESP_PROF_1 = Rs.getrows()
end if
Set Rs = Nothing

Sql = ""
Sql = Sql & " SELECT FIGUREPROFESSIONALI.DENOMINAZIONE, PERS_ATT_PROFESS.RAG_SOC, "
Sql = Sql & " PERS_ATT_PROFESS.DT_INI_ESP_PRO, PERS_ATT_PROFESS.DT_FIN_ESP_PRO  "
Sql = Sql & " FROM  FIGUREPROFESSIONALI, PERS_ATT_PROFESS "
Sql = Sql & " WHERE "
Sql = Sql & " PERS_ATT_PROFESS.ID_FIGPROF=FIGUREPROFESSIONALI.ID_FIGPROF AND "
Sql = Sql & " PERS_ATT_PROFESS.ID_PERSONA = " & IDP
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
Set Rs = Conn.Execute (Sql)
if not rs.eof then
	ESP_PROF_2 = Rs.getrows()
end if
Set Rs = Nothing

'================================================
' ESPERIENZE LAVORATIVE NON PROFESSIONALI (EXTRALAVORATIVE E LAVORATIVE)
'================================================
Sql = ""
Sql = Sql & " SELECT ATTIVITA, OUTPUT, CLIENTE, "
Sql = Sql & " DT_INI_ESP_PRO, DT_FIN_ESP_PRO  "
Sql = Sql & " FROM PERS_ATT_NON_PROFESS "
Sql = Sql & " WHERE "
Sql = Sql & " PERS_ATT_NON_PROFESS.ID_PERSONA = " & IDP
Sql = Sql & " AND (FL_TIPO_ESP ='' or FL_TIPO_ESP is null) "
Sql = Sql & " ORDER BY FL_ATT "
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
Set Rs = Conn.Execute (Sql)
if not rs.eof then
	ESP_NON_PROF = Rs.getrows()
end if
Set Rs = Nothing

'========================
' CAPACITA
'========================
Sql = ""
Sql = Sql & " SELECT  CAPACITA.DENOMINAZIONE, PERS_CAPAC.GRADO_CAPACITA "
Sql = Sql & " FROM PERS_CAPAC,CAPACITA "
Sql = Sql & " WHERE "
Sql = Sql & " PERS_CAPAC.ID_CAPACITA = CAPACITA.ID_CAPACITA AND "
Sql = Sql & " PERS_CAPAC.ID_PERSONA = " & IDP
Sql = Sql & " ORDER BY PERS_CAPAC.GRADO_CAPACITA DESC "
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
Set Rs = Conn.Execute (Sql)
if not rs.eof then
	LST_CAPACITA = Rs.getrows()
end if
Set Rs = Nothing

Sql = ""
Sql = Sql & " SELECT  DENOMINAZIONE, POSSESSO  "
Sql = Sql & " FROM ELEMENTI_USR "
Sql = Sql & " WHERE FL_TIPO='CAPACITA' AND ID_PERSONA = " & IDP
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
Set Rs = Conn.Execute (Sql)
if not rs.eof then
	LST_CAPACITA_USR = Rs.getrows()
end if
Set Rs = Nothing

'========================
'CONOSCENZE
'========================
Sql = ""
Sql = Sql & " SELECT CONOSCENZE.DENOMINAZIONE, PERS_CONOSC.COD_GRADO_CONOSC"
Sql = Sql & " FROM PERS_CONOSC, CONOSCENZE "
Sql = Sql & " WHERE "
Sql = Sql & " PERS_CONOSC.ID_CONOSCENZA = CONOSCENZE.ID_CONOSCENZA AND "
Sql = Sql & " PERS_CONOSC.ID_PERSONA = " & IDP & " AND "
Sql = Sql & " PERS_CONOSC.COD_GRADO_CONOSC >0  ORDER BY PERS_CONOSC.COD_GRADO_CONOSC DESC "
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
Set Rs = Conn.Execute (Sql)
if not rs.eof then
	LST_CONOSC = Rs.getrows()
end if
Set Rs = Nothing

Sql = ""
Sql = Sql & " SELECT  DENOMINAZIONE, POSSESSO  "
Sql = Sql & " FROM ELEMENTI_USR "
Sql = Sql & " WHERE FL_TIPO='CONOSCENZE' AND ID_PERSONA = " & IDP
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
Set Rs = Conn.Execute (Sql)
if not rs.eof then
	LST_CONOSC_USR = Rs.getrows()
end if
Set Rs = Nothing

'========================
' COMPORTAMENTI
'=======================
Sql = ""
Sql = Sql & " SELECT COMPORTAMENTI.DENOMINAZIONE,PERS_COMPOR.GRADO_PERS_COMPOR "
Sql = Sql & " FROM PERS_COMPOR, COMPORTAMENTI "
Sql = Sql & " WHERE "
Sql = Sql & " PERS_COMPOR.ID_COMPORTAMENTO = COMPORTAMENTI.ID_COMPORTAMENTO AND "
Sql = Sql & " PERS_COMPOR.ID_PERSONA = " & IDP
Sql = Sql & " ORDER BY PERS_COMPOR.GRADO_PERS_COMPOR "
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
Set Rs = Conn.Execute (Sql)
if not rs.eof then
	LST_COMPORTAMENTI = Rs.getrows()
end if

Sql = ""
Sql = Sql & " SELECT  DENOMINAZIONE, POSSESSO  "
Sql = Sql & " FROM ELEMENTI_USR "
Sql = Sql & " WHERE FL_TIPO='COMPORTAMENTI' AND ID_PERSONA = " & IDP
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
Set Rs = Conn.Execute (Sql)
if not rs.eof then
	LST_COMPOR_USR = Rs.getrows()
end if
Set Rs = Nothing


'========================
' DISPONIBILITA
'=======================
Sql = ""
Sql = Sql & " SELECT TIPO_DISP.DESCR_TIPO_DISP, DISPONIBILITA.DESCR_DISPON, PERS_DISP.ID_DISPONIBILITA"
Sql = Sql & " FROM PERS_DISP, DISPONIBILITA, TIPO_DISP "
Sql = Sql & " WHERE "
Sql = Sql & " PERS_DISP.ID_DISPONIBILITA = DISPONIBILITA.ID_DISPONIBILITA AND "
Sql = Sql & " DISPONIBILITA.ID_TIPO_DISP = TIPO_DISP.ID_TIPO_DISP AND "
Sql = Sql & " PERS_DISP.ID_PERSONA = " & IDP

'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
Set Rs = Conn.Execute (Sql)
if not rs.eof then
	LST_DISPON = Rs.getrows()
END IF
Set Rs = Nothing

Conn.Close
Set conn = Nothing

response.write "<center>"
Response.Write NOMINATIVO
Response.Write "<BR><br>"

Response.Write "<font face=verdana size=1 color='#000000'>"

Response.Write "<b>TITOLI DI STUDIO CONSEGUITI</b><BR>"
IF ISARRAY(TISTUD) THEN
	Response.Write "<table border=0 width=500 cellspacing=1 cellpadding=0 style='font-family: Verdana; font-size: 8pt;'>"
	Response.Write "<TR><TD><b>Titolo</b></td><td><b>Anno</b></td></tr>"
	FOR I=0 TO UBOUND(TISTUD,2)
		response.write "<tr><td>" & TISTUD(0,I) & "</td>"
		response.write "<td>" & TISTUD(1,I) & "</td></tr>"
	NEXT
	Response.Write "</table>"
END IF
Response.Write "<BR><br>"

Response.Write "<b>ESPERIENZE FORMATIVE</b><BR>"
IF ISARRAY(CORSI_FORM) THEN
	Response.Write "<table border=0 width=500 cellspacing=1 cellpadding=0 style='font-family: Verdana; font-size: 8pt;'>"
	Response.Write "<TR><TD><b>Corso</b></td><td><b>Attestato</b></td><td><b>Durata</b></td><td><b>Anno</b></td></tr>"
	FOR I=0 TO UBOUND(CORSI_FORM,2)
		response.write "<tr><td>" & CORSI_FORM(0,I) & "</td>"
		response.write "<td>"
			select case CORSI_FORM(1,I)
				case 0
					response.write "NESSUNO"
				case 1
					response.write "FREQUENZA"
				case 2
					response.write "QUALIFICA PROFESSIONALE"
				case 3
					response.write "CERTIFICAZIONE"
			end select
		response.write "</td>"
		response.write "<td>" & CORSI_FORM(2,I) & "</td>"
		response.write "<td>" & CORSI_FORM(3,I) & "</td>"

	NEXT
	Response.Write "</table>"
END IF
Response.Write "<BR><br>"

Response.Write "<b>STAGE, TIROCINI, APPRENDISTATO</b><BR>"
IF ISARRAY(STAGE_TIROCINI) THEN
	Response.Write "<table border=0 width=500 cellspacing=1 cellpadding=0 style='font-family: Verdana; font-size: 8pt;'>"
	Response.Write "<TR><TD><b>Attivit�</b></td><td><b>Output</b></td><td><b>Cliente</b></td><td><b>Dal</b></td><td><b>Al</b></td><td><b>Tipo</b></td></tr>"
	FOR I=0 TO UBOUND(STAGE_TIROCINI,2)
		response.write "<tr><td>" & STAGE_TIROCINI(0,I) & "</td>"
		response.write "<td>" & STAGE_TIROCINI(1,I) & "</td>"
		response.write "<td>" & STAGE_TIROCINI(2,I) & "</td>"
		response.write "<td>" & STAGE_TIROCINI(3,I) & "</td>"
		response.write "<td>" & STAGE_TIROCINI(4,I) & "&nbsp;</td>"
		response.write "<td>" & STAGE_TIROCINI(5,I) & "</td></tr>"
	NEXT
	Response.Write "</table>"
END IF
Response.Write "<BR><br>"



Response.Write "<b>ESPERIENZE LAVORATIVE PROFESSIONALI</b><BR>"

IF ISARRAY(ESP_PROF_1) THEN
	Response.Write "<table border=0 width=500 cellspacing=1 cellpadding=0 style='font-family: Verdana; font-size: 8pt;'>"
	Response.Write "<TR><TD><b>Denominazione</b></td><td><b>Dal</b></td><td><b>Al</b></td></tr>"
	FOR I=0 TO UBOUND(ESP_PROF_1,2)
		response.write "<tr><td>" & ESP_PROF_1(0,I) & "</td>"
		response.write "<td>" & ESP_PROF_1(1,I) & "&nbsp;</td>"
		response.write "<td>" & ESP_PROF_1(2,I) & "</td>"
	NEXT
	Response.Write "</table>"
END IF
	Response.Write "<BR>"

IF ISARRAY(ESP_PROF_2) THEN
	Response.Write "<table border=0 width=500 cellspacing=1 cellpadding=0 style='font-family: Verdana; font-size: 8pt;'>"
		Response.Write "<TR><TD><b>Denominazione</b></td><td><b>Cliente</b></td><td><b>Dal<b></td><td><b>Al</b></td></tr>"
	FOR I=0 TO UBOUND(ESP_PROF_2,2)
		response.write "<tr><td>" & ESP_PROF_2(0,I) & "</td>"
		response.write "<td>" & ESP_PROF_2(1,I) & "</td>"
		response.write "<td>" & ESP_PROF_2(2,I) & "&nbsp;</td>"
		response.write "<td>" & ESP_PROF_2(3,I) & "&nbsp;</td></tr>"
	NEXT
	Response.Write "</table>"
END IF
Response.Write "<BR><br>"

Response.Write "<b>ESPERIENZE LAVORATIVE NON PROFESSIONALI</b> <BR>"
IF ISARRAY(ESP_NON_PROF) THEN
	Response.Write "<table border=0 width=500 cellspacing=1 cellpadding=0 style='font-family: Verdana; font-size: 8pt;'>"
	Response.Write "<TR><TD><b>Attivit�</b></td><td><b>Output</b></td><td><b>Cliente</b></td><td><b>Dal</b></td><td><b>Al</b></td></tr>"
	FOR I=0 TO UBOUND(ESP_NON_PROF,2)
		response.write "<tr><td>" & ESP_NON_PROF(0,I) & "</td>"
		response.write "<td>" & ESP_NON_PROF(1,I) & "</td>"
		response.write "<td>" & ESP_NON_PROF(2,I) & "</td>"
		response.write "<td>" & ESP_NON_PROF(3,I) & "&nbsp;</td>"
		response.write "<td>" & ESP_NON_PROF(4,I) & "&nbsp;</td></tr>"
	NEXT
	Response.Write "</table>"
END IF
Response.Write "<BR><br>"

Response.Write "<b>LE CAPACITA</b><BR>"
IF ISARRAY(LST_CAPACITA) THEN
	'Codificate nel dizionario dati
	Response.Write "<table border=0 width=500 cellspacing=1 cellpadding=0 style='font-family: Verdana; font-size: 8pt;'>"
		Response.Write "<TR><TD><b>Denominazione</b></td><td><b>Possesso</b></td></tr>"
	FOR I=0 TO UBOUND(LST_CAPACITA,2)
		response.write "<tr><td>" & LST_CAPACITA(0,I) & "</td>"
		response.write "<td align='center'>" & LST_CAPACITA(1,I) & "</td></tr>"
	NEXT
	'Definite dall'utente .......
	IF ISARRAY(LST_CAPACITA_USR) THEN
		FOR I=0 TO UBOUND(LST_CAPACITA_USR,2)
			response.write "<tr><td>" & LST_CAPACITA_USR(0,I) & "</td>"
			response.write "<td align='center'>" & LST_CAPACITA_USR(1,I) & "</td></tr>"
		NEXT
	END IF
	Response.Write "</table>"
END IF

Response.Write "<BR><br>"

Response.Write "<b>LE CONOSCENZE</b><BR>"
IF ISARRAY(LST_CONOSC) THEN
	Response.Write "<table border=0 width=500 cellspacing=1 cellpadding=0 style='font-family: Verdana; font-size: 8pt;'>"
		Response.Write "<TR><TD><b>Denominazione</b></td><td><b>Possesso</b></td></tr>"
	FOR I=0 TO UBOUND(LST_CONOSC,2)
		response.write "<tr><td>" & LST_CONOSC(0,I) & "</td>"
		response.write "<td align='center'>" & LST_CONOSC(1,I) & "</td></tr>"
	NEXT
	'Definite dall'Utente
	IF ISARRAY(LST_CONOSC_USR) THEN
		FOR I=0 TO UBOUND(LST_CONOSC_USR,2)
			response.write "<tr><td>" & LST_CONOSC_USR(0,I) & "</td>"
			response.write "<td align='center'>" & LST_CONOSC_USR(1,I) & "</td></tr>"
		NEXT
	END IF
	Response.Write "</table>"
END IF

Response.Write "<BR><br>"

Response.Write "<b>I COMPORTAMENTI</b><BR>"
IF ISARRAY(LST_COMPORTAMENTI) THEN
	Response.Write "<table border=0 width=500 cellspacing=1 cellpadding=0 style='font-family: Verdana; font-size: 8pt;'>"
	Response.Write "<TR><TD><b>Denominazione</b></td><td><b>Possesso</b></td></tr>"
	FOR I=0 TO UBOUND(LST_COMPORTAMENTI,2)
		response.write "<tr><td>" & LST_COMPORTAMENTI(0,I) & "</td>"
		response.write "<td align='center'>" & LST_COMPORTAMENTI(1,I) & "</td></tr>"
	NEXT
	'Definiti dall'Utente
	IF ISARRAY(LST_COMPOR_USR) THEN
		FOR I=0 TO UBOUND(LST_COMPOR_USR,2)
			response.write "<tr><td>" & LST_COMPOR_USR(0,I) & "</td>"
			response.write "<td align='center'>" & LST_COMPOR_USR(1,I) & "</td></tr>"
		NEXT
	END IF
	Response.Write "</table>"
END IF
Response.Write "<BR><br>"


Response.Write "<b>CONDIZIONI DI DISPONIBILITA</b><BR>"
VAL=""
IF ISARRAY(LST_DISPON) THEN
	Response.Write "<table border=0 width=500 cellspacing=1 cellpadding=0 style='font-family: Verdana; font-size: 8pt;'>"
	Response.Write "<TR><TD><b>Tipo</b></td><td><b>Descrizione</b></td></tr>"
	I=0
	do while I < UBOUND(LST_DISPON,2)
		if VAL=LST_DISPON(0,I) then
			VAL=""
		else
			VAL=LST_DISPON(0,I)
		end if

		response.write "<tr><td><i>" & VAL & "</i></td>"
		response.write "<td align='left'>" & LST_DISPON(1,I) & "</td></tr>"
		VAL=LST_DISPON(0,I)
		I=I+1
	loop
	Response.Write "</table>"
END IF
Response.Write "</FONT>"
Response.Write "<BR>"

%>

</form>
<br>

<table width=570 border=0 cellspacing=0 cellpadding=0>
	<tr>
		<td align='left'><font face=verdana size='2'>Ai sensi della legge 675/96, esprimo il consenso al trattamento ed alla comunicazione dei 
		miei dati personali da parte di Italia Lavoro S.p.A nei limiti di cui alla stessa.
		<br><br>&nbsp;<br>
		<hr width='50%'>
		</font>
		</td>
	</tr>
</table>

<center>
<div id="aa"><form><input type="button" value="Stampa" onClick="stp()"></form></div>
</center>

<br>
<table width=570 border=0 cellspacing=0 cellpadding=0 align=center>
	<tr>
		<td class="copyright">Copyright � 2001-2003 Consorzio
		<span class="ejob"><b>ejob-pl@ce</b></span>
		Tutti i diritti riservati</td>
	</tr>
</table>
</center>
</body>
</html>
