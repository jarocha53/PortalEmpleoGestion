<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #INCLUDE FILE="../Utils.asp" -->
<html>
<head>
<title>Descripci&oacute;n del Perfil Profesional</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="<%=Session("Progetto")%>/fogliostile.css" type="text/css">

<script LANGUAGE="JavaScript">
function stp()
{
		bb=(document.layers)?document.layers['aa']:(document.getElementById)?document.getElementById('aa').style:document.all['aa'].style;
		gg=(document.layers)?document.layers['ff']:(document.getElementById)?document.getElementById('ff').style:document.all['ff'].style;
		bb.visibility="hidden"
		gg.visibility="hidden"
		self.print();
		bb.visibility="visible"
		gg.visibility="visible"

}
</script>


</head>
<body bgcolor="#ffffff">
<p align="left">
<center>
<div ID="ff">
<input type="button" value="Imprimir" OnClick="stp()" CLASS="My">&nbsp; &nbsp;
<input type="button" value="Cancelar" OnClick="top.close()" CLASS="My">
</div>
</center>
</p>
<%

Set Conn = server.CreateObject ("ADODB.Connection")
Conn.open strConn

TAviso = request("TipoDeAviso")

if TAviso = "C" then 

	Sql = ""
	Sql = Sql & "SELECT "
	Sql = Sql & "area_corso_sub.descripcion, area_corso.DENOMINAZIONE "
	Sql = Sql & "FROM "
	Sql = Sql & "area_corso, area_corso_sub "
	Sql = Sql & "WHERE "
	Sql = Sql & "area_corso_sub.ID_AREA = area_corso.ID_AREA "
	Sql = Sql & "AND "
	Sql = Sql & "area_corso_sub.id_subarea = " & request("ID_FIGPROF")
	
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
	set Rs = conn.Execute(sql)
	
	Des_AreaProf = Rs("denominazione")
	Des_FigProf = Rs("descripcion")
	
	
%>
<body bgcolor="FFFFFF">
<table border="0" cellspacing="1" cellpadding="0" width="500">
<tr class="sfondocomm"><td><b><small><center>DETALLE DE LA TIPOLOGIA<center></small></b>
<a href="Javascript:Show_Help('<%=Session("Progetto")%>/HelpBdc/DettagliArea/FocusFigProf.htm')">
 <img src="<%=Session("Progetto")%>/images/Help.gif" border="0" align="right" alt="Help">
</a>
</td></tr>
<tr class="sfondocomm"><td><b><small>Tipología:&nbsp;<%=lcase(Des_AreaProf)%> </small></b></td></tr>
<tr class="sfondocomm"><td><b><small>Subtipología:&nbsp;<%=lcase(Des_FigProf)%> </small></b></td></tr>
<tr class="sfondocomm"><td align="center"><b><small><%=Session("Nome")%> -<%=Session("Cognome")%> </small></b></td>
</tr>
</table>
</body>
</html>
<%
else


	Sql = ""
	Sql = Sql & "SELECT "
	Sql = Sql & "COMPETENZE.DENOMINAZIONE, COMPETENZE.ID_COMPETENZA, COMPET_FP.GRADO_FP_COMP "
	Sql = Sql & "FROM "
	Sql = Sql & "COMPETENZE, COMPET_FP "
	Sql = Sql & "WHERE "
	Sql = Sql & "COMPETENZE.ID_COMPETENZA = COMPET_FP.ID_COMPETENZA "
	Sql = Sql & "AND "
	Sql = Sql & "COMPET_FP.ID_FIGPROF = " & request("ID_FIGPROF")

'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
	Set Rs = Conn.Execute (Sql)
	if not rs.eof then COMPETENZE = Rs.getrows()

	Sql = ""
	Sql = Sql & "SELECT "
	Sql = Sql & "CONOSCENZE.DENOMINAZIONE, COMPET_CONOSC.ID_COMPETENZA, COMPET_CONOSC.GRADO_COMP_CON "
	Sql = Sql & "FROM "
	Sql = Sql & "CONOSCENZE, COMPET_CONOSC, COMPET_FP "
	Sql = Sql & "WHERE "
	Sql = Sql & "CONOSCENZE.ID_CONOSCENZA = COMPET_CONOSC.ID_CONOSCENZA "
	Sql = Sql & "AND "
	Sql = Sql & "COMPET_CONOSC.ID_COMPETENZA = COMPET_FP.ID_COMPETENZA "
	Sql = Sql & "AND "
	Sql = Sql & "COMPET_FP.ID_FIGPROF = " & request("ID_FIGPROF")

'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
	Set Rs = Conn.Execute (Sql)
	if not rs.eof then CONOSCENZE = Rs.getrows()

	Sql = ""
	Sql = Sql & "SELECT "
	Sql = Sql & "CAPACITA.DENOMINAZIONE, COMPET_CAPAC.ID_COMPETENZA, COMPET_CAPAC.GRADO_COMP_CAP "
	Sql = Sql & "FROM "
	Sql = Sql & "CAPACITA, COMPET_CAPAC, COMPET_FP "
	Sql = Sql & "WHERE "
	Sql = Sql & "CAPACITA.ID_CAPACITA = COMPET_CAPAC.ID_CAPACITA "
	Sql = Sql & "AND "
	Sql = Sql & "COMPET_CAPAC.ID_COMPETENZA = COMPET_FP.ID_COMPETENZA "
	Sql = Sql & "AND "
	Sql = Sql & "COMPET_FP.ID_FIGPROF = " & request("ID_FIGPROF")

'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
	Set Rs = Conn.Execute (Sql)
	if not rs.eof then CAPACITA = Rs.getrows()

	Sql = ""
	Sql = Sql & "SELECT "
	Sql = Sql & "SETTORI.DENOMINAZIONE, SETTORI.ID_SETTORE, "
	Sql = Sql & "VALIDAZIONE.ID_VALID, VALIDAZIONE.FL_VALID, VALIDAZIONE.ID_PERS_INS "
	Sql = Sql & "FROM "
	Sql = Sql & "SETTORI, SETTORI_FIGPROF, VALIDAZIONE "
	Sql = Sql & "WHERE "
	Sql = Sql & "SETTORI_FIGPROF.ID_VALID = VALIDAZIONE.ID_VALID "
	Sql = Sql & "AND "
	Sql = Sql & "SETTORI.ID_SETTORE = SETTORI_FIGPROF.ID_SETTORE "
	Sql = Sql & "AND "
	Sql = Sql & "SETTORI_FIGPROF.ID_FIGPROF = " & request("ID_FIGPROF")

'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
	Set Rs = Conn.Execute (Sql)
	if not rs.eof then SETTORI = Rs.getrows()


	Sql = ""
	Sql = Sql & "SELECT "
	Sql = Sql & "COMPORTAMENTI.DENOMINAZIONE, COMPORTAMENTI.ID_COMPORTAMENTO, "
	Sql = Sql & "VALIDAZIONE.ID_VALID, VALIDAZIONE.FL_VALID, VALIDAZIONE.ID_PERS_INS, COMPOR_FP.GRADO_FP_COMPOR "
	Sql = Sql & "FROM "
	Sql = Sql & "COMPORTAMENTI, COMPOR_FP, VALIDAZIONE "
	Sql = Sql & "WHERE "
	Sql = Sql & "COMPOR_FP.ID_VALID = VALIDAZIONE.ID_VALID "
	Sql = Sql & "AND "
	Sql = Sql & "COMPORTAMENTI.ID_COMPORTAMENTO = COMPOR_FP.ID_COMPORTAMENTO "
	Sql = Sql & "AND "
	Sql = Sql & "COMPOR_FP.ID_FIGPROF = " & request("ID_FIGPROF")

'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
	Set Rs = Conn.Execute (Sql)
	if not rs.eof then COMPORT = Rs.getrows()

	Sql = ""
	Sql = Sql & "SELECT "
	Sql = Sql & "FIGUREPROFESSIONALI.DENOMINAZIONE, AREE_PROFESSIONALI.DENOMINAZIONE,descrizione,breve_desc "
	Sql = Sql & "FROM "
	Sql = Sql & "FIGUREPROFESSIONALI, AREE_PROFESSIONALI "
	Sql = Sql & "WHERE "
	Sql = Sql & "FIGUREPROFESSIONALI.ID_AREAPROF = AREE_PROFESSIONALI.ID_AREAPROF "
	Sql = Sql & "AND "
	Sql = Sql & "FIGUREPROFESSIONALI.ID_FIGPROF = " & request("ID_FIGPROF")

'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
	Set Rs = Conn.Execute (Sql)

	if not rs.eof then
		Des_FigProf=Rs(0)
		Des_AreaProf=Rs(1)
'Leonetti 23/12/2003
'		Des_FigDesc=Rs(2)
		Des_FigBreveDesc=Rs(3)
'Leonetti  23/12/2003				
	END IF

set Rs = Nothing
Conn.Close
Set conn = Nothing

%>


<body bgcolor="FFFFFF">
<table border="0" cellspacing="1" cellpadding="0" width="500">
<tr class="sfondocomm"><td><b><small><center>DESCRIPCI&Oacute;ON DEL PERFIL PROFESIONAL<center></small></b>
<!-- <a href="Javascript:Show_Help('<%=Session("Progetto")%>/HelpBdc/DettagliArea/FocusFigProf.htm')">
 <img src="<%=Session("Progetto")%>/images/Help.gif" border="0" align="right" alt="Help">
</a>
 --></td></tr>

<tr class="sfondocomm"><td><b><small>Puesto:&nbsp;<%=lcase(Des_FigProf)%> </small></b></td></tr>
<tr class="sfondocomm"><td><small><%=Des_FigDesc%> </small></td></tr>
<tr class="sfondocomm"><td><small><%=Des_FigBreveDesc%> </small></td></tr>
<tr class="sfondocomm"><td align="center"><b><small><%=Session("Nome")%> -<%=Session("Cognome")%> </small></b></td>
</tr>
</table>
<br>
<%
	if IsArray(SETTORI) then
	Response.Write "<table border=0 width='500' cellspacing=1 cellpadding=0>"
		Response.write "<tr>"
		Response.Write "<td>"

			Response.Write "<table border=0 cellspacing=1 width='500'>"
				Response.Write "<tr class='sfondocomm'>"
					Response.Write "<td><b><small>Sector Merceológico</small></b></td>"
				Response.Write "</tr>"
					for I = lbound(SETTORI,2) to Ubound(SETTORI,2)
						Response.Write "<tr>"
							Response.Write "<td class='tbltext1'><small>" & SETTORI(0,I) & "</small></td>"
						Response.Write "</tr>"
					next
			Response.Write "</table>"
		Response.Write "</td>"
		Response.Write "</tr>"
	Response.Write "</table>"
	end if

	Response.Write "<table border=0 width='500' cellspacing=1 cellpadding=0 >"
		Response.write "<tr>"
			Response.Write "<td>"
				Response.Write "<table border=0 cellspacing=1 width='500'>"
					Response.Write "<tr class='sfondocomm'>"
						Response.Write "<td><b>Grupo Ocupacional</b></td>"
					Response.Write "</tr>"
					Response.Write "<tr>"
						Response.Write "<td class='tbltext1'>" & Ucase(Des_AreaProf) & "</td>"
					Response.Write "</tr>"
				Response.Write "</table>"
			Response.Write "</td>"
		Response.Write "</tr>"
	Response.Write "</table>"
%>
<br>
<%
	if IsArray(COMPETENZE) then
	Response.Write "<table border=0 width='500' cellspacing=1 cellpadding=0>"
	Response.write "<tr>"
	Response.Write "<td>"

	Response.Write "<table border=0 cellspacing=1 width='500'>"
	Response.Write "<tr class='sfondocomm'>"
		Response.Write "<td class='tbltext1' colspan='2'><b><small>Competencia</small></b></td>"
		Response.Write "<td class='tbltext1' NOWRAP width='73'><b><small>Nivel Requerido</small></b></td>"
	Response.Write "</tr>"
	for i = lbound(COMPETENZE,2) to Ubound(COMPETENZE,2)
		Response.Write "<tr class='sfondocomm'>"
			Response.Write "<td class='tbltext1' colspan='2'><b>&nbsp;&nbsp;<small>" & Ucase(COMPETENZE(0,I)) & "</small></b></td>"
			Response.Write "<td class='tbltext1' align='center'><b>&nbsp;&nbsp;<small>" & COMPETENZE(2,I) & "</small></b></td>"
		Response.Write "</tr>"

		Response.Write "<tr>"
		Response.Write "<td WIDTH='3%'>&nbsp;</td>"
		Response.Write "<td class='tbltext1'><b><small>Conocimiento</small></b></td>"
		Response.Write "</tr>"
		if isarray(CONOSCENZE) THEN
			for jj = lbound(CONOSCENZE,2) to Ubound(CONOSCENZE,2)
			if int(0 & COMPETENZE(1,I)) = int(0 & CONOSCENZE(1,jj)) then
				Response.Write "<tr>"
					Response.Write "<td>&nbsp;</td>"
					Response.Write "<td class='tbltext1'><small>" & Ucase(CONOSCENZE(0,jj)) & "</small></td>"
					Response.Write "<td class='tbltext1' align='center'><b>&nbsp;&nbsp;<small>" & CONOSCENZE(2,jj) & "</small></b></td>"
				Response.Write "</tr>"
			end if
			next
		END IF

		Response.Write "<tr>"
		Response.Write "<td WIDTH='3%'>&nbsp;</td>"
		Response.Write "<td class='tbltext1'><b><small>Capacidad</small></b></td>"
		Response.Write "</tr>"
		IF ISARRAY(CAPACITA) THEN
			for jj = lbound(CAPACITA,2) to Ubound(CAPACITA,2)
			if int(0 & COMPETENZE(1,I)) = int(0 & CAPACITA(1,jj)) then
				Response.Write "<tr>"
					Response.Write "<td>&nbsp;</td>"
					Response.Write "<td class='tbltext1'><small>" & Ucase(CAPACITA(0,jj)) & "</small></td>"
					Response.Write "<td class='tbltext1' align='center'><b>&nbsp;&nbsp;<small>" & CAPACITA(2,jj) & "</small></b></td>"
				Response.Write "</tr>"
			end if
			next
		END IF
	next
	Response.Write "</table>"
	end if
			Response.Write "</td>"
		Response.Write "</tr>"
	Response.Write "</table>"
%>
<br>
<%
	if IsArray(COMPORT) then
	Response.Write "<table border=0 width='500' cellspacing=1 cellpadding=0 >"
	Response.write "<tr>"
	Response.Write "<td>"

		Response.Write "<table border=0 cellspacing=1 width='500'>"
		Response.Write "<tr class='sfondocomm'>"
			Response.Write "<td><b><small>Comportamiento</small></b></td>"
			Response.Write "<td NOWRAP width='73'><b><small>Nivel Requerido</small></b></td>"
		Response.Write "</tr>"
			for I = lbound(COMPORT,2) to Ubound(COMPORT,2)
				Response.Write "<tr>"
					Response.Write "<td class='tbltext1'>&nbsp;&nbsp;<small>" & Ucase(COMPORT(0,I)) & "</small></td>"
					Response.Write "<td class='tbltext1' align='center'><b>&nbsp;&nbsp;<small>" & COMPORT(5,I) & "</small></b></td>"
				Response.Write "</tr>"
			next
		Response.Write "</table>"
	Response.Write "</td>"
	Response.Write "</tr>"
	Response.Write "</table>"
	end if

%>
<br>
<center>
<div id="aa">
<input type="button" value="Imprimir" OnClick="stp()" CLASS="My">&nbsp; &nbsp;
<input type="button" value="Cancelar" OnClick="top.close()" CLASS="My">
</div>
</center>
</body>
</html>

<%end if%>
