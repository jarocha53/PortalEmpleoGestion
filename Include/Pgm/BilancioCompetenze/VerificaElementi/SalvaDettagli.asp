<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!-- #Include Virtual="/strutt_testa2.asp" -->
<!-- #Include file="../Utils.asp" -->
<%


Set Conn = server.CreateObject ("ADODB.Connection")
Conn.open strConn
OraDate = ConvDateToDB(Now())

'##### SALVA IL CASO CAPACITA'

if request("Tipo") = "CAPACITA" then
	Conn.BeginTrans
		for x = Int(request("Ini")) to Int(request("Limit"))
	LstCampi = "ID_PERSONA,GRADO_CAPACITA,ID_CAPACITA,DT_TMST,IND_STATUS"
 
	LstVal = ""
	LstVal = LstVal & IDP & "|"
	LstVal = LstVal & request("Grado" & x) & "|"
	LstVal = LstVal & request("ID" & x) & "|"
	LstVal = LstVal & OraDate 
	LstVal = LstVal & "|'0'"
	Sql = ""
	if request("exist" & x) = "True" then
		Sql = QryUpd("PERS_CAPAC",LstCampi,LstVal)
		Sql = Sql & " WHERE "
		Sql = Sql & "PERS_CAPAC.ID_PERSONA = " & IDP & " AND "
		Sql = Sql & "PERS_CAPAC.ID_CAPACITA = " & request("ID" & x)
	else
		Sql = QryIns("PERS_CAPAC",LstCampi,LstVal)
	end if
	
	if request("Grado" & x) <> "" then
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
		Conn.Execute Sql
	else
		Sql = ""
		Sql = Sql & "DELETE PERS_CAPAC "
		Sql = Sql & " WHERE "
		Sql = Sql & " PERS_CAPAC.ID_PERSONA = " & IDP & " AND "
		Sql = Sql & " PERS_CAPAC.ID_CAPACITA = " & request("ID" & x)

'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
		Conn.Execute Sql		
	end if
next
	Conn.CommitTrans
end if
'##### END CASO CAPACITA'


'##### SALVA IL CASO COMPORTAMENTI
if request("Tipo") = "COMPORTAMENTI" then
	Conn.BeginTrans
		for x = Int(request("Ini")) to Int(request("Limit"))
			LstCampi = "ID_PERSONA,GRADO_PERS_COMPOR,ID_COMPORTAMENTO,DT_TMST,IND_STATUS"

			LstVal = ""
			LstVal = LstVal & IDP & "|"
			LstVal = LstVal & request("Grado" & x) & "|"
			LstVal = LstVal & request("ID" & x) & "|"
			LstVal = LstVal & OraDate 
			LstVal = LstVal & "|'0'"

			Sql = ""
			if request("exist" & x) = "True" then
				Sql = QryUpd("PERS_COMPOR",LstCampi,LstVal)
				Sql = Sql & " WHERE "
				Sql = Sql & " PERS_COMPOR.ID_PERSONA = " & IDP & " AND "
				Sql = Sql & " PERS_COMPOR.ID_COMPORTAMENTO = " & request("ID" & x)
			else
				Sql = QryIns("PERS_COMPOR",LstCampi,LstVal)
			end if
			
			if request("Grado" & x) <> "" then
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
				Conn.Execute Sql
			else
				Sql = ""
				Sql = Sql & "DELETE PERS_COMPOR "
				Sql = Sql & " WHERE "
				Sql = Sql & " PERS_COMPOR.ID_PERSONA = " & IDP & " AND "
				Sql = Sql & " PERS_COMPOR.ID_COMPORTAMENTO = " & request("ID" & x)
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
				Conn.Execute Sql		
			end if
		next
	Conn.CommitTrans
end if
'##### END CASO COMPORTAMENTI

'##### SALVA IL CASO CONOSCENZE
if request("Tipo") = "CONOSCENZE" then
	Conn.BeginTrans
		for x = Int(request("Ini")) to Int(request("Limit"))
			LstCampi = "ID_PERSONA, COD_GRADO_CONOSC, DT_TMST, DT_DICH_CONOSC, ID_CONOSCENZA, IND_STATUS"

			LstVal = ""
			LstVal = LstVal & IDP & "|"
			LstVal = LstVal & request("Grado" & x) & "|"
			LstVal = LstVal & OraDate & "|"
			LstVal = LstVal & OraDate & "|"
			LstVal = LstVal & request("ID" & x) & "|'0'"

			Sql = ""
			if request("exist" & x) = "True" then
				Sql = QryUpd("PERS_CONOSC",LstCampi,LstVal)
				Sql = Sql & " WHERE "
				Sql = Sql & "PERS_CONOSC.ID_PERSONA = " & IDP & " AND "
				Sql = Sql & "PERS_CONOSC.ID_CONOSCENZA = " & request("ID" & x)
			else
				Sql = QryIns("PERS_CONOSC",LstCampi,LstVal)
			end if
			
			if request("Grado" & x) <> "" then
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
				Conn.Execute Sql
			else
				Sql = ""
				Sql = Sql & "DELETE PERS_CONOSC "
				Sql = Sql & " WHERE "
				Sql = Sql & "PERS_CONOSC.ID_PERSONA = " & IDP & " AND "
				Sql = Sql & "PERS_CONOSC.ID_CONOSCENZA = " & request("ID" & x)
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
				Conn.Execute Sql		
			end if
		next
	Conn.CommitTrans
end if
'##### END CASO CONOSCENZE

Conn.Close
Set conn = Nothing

if request("Avanti") <> "" then
	URL = request("DETTAGLI") & "?"
	URL = URL & "Limit=" & request("Limit") & "&"
	URL = URL & "Back=" & request("Back") & "&"
	URL = URL & "Tipo=" & request("Tipo") & "&"
	URL = URL & "CONOSCENZE=" & request("CONOSCENZE") & "&"
	URL = URL & "AREA_CONOSCENZA=" & request("AREA_CONOSCENZA") & "&"
	URL = URL & "CAPACITA=" & request("CAPACITA") & "&"
	URL = URL & "AREA_CAPACITA=" & request("AREA_CAPACITA") & "&"
	URL = URL & "COMPORTAMENTI=" & request("COMPORTAMENTI") & "&"
	URL = URL & "AREA_COMPORTAMENTI=" & request("AREA_COMPORTAMENTI")
End if

if request("Indietro") <> "" then
	URL = request("DETTAGLI") & "?"
	URL = URL & "Limit=" & Request("Ini")-10 & "&"
	URL = URL & "Back=" & request("Back") & "&"
	URL = URL & "Tipo=" & request("Tipo") & "&"
	URL = URL & "CONOSCENZE=" & request("CONOSCENZE") & "&"
	URL = URL & "AREA_CONOSCENZA=" & request("AREA_CONOSCENZA") & "&"
	URL = URL & "CAPACITA=" & request("CAPACITA") & "&"
	URL = URL & "AREA_CAPACITA=" & request("AREA_CAPACITA") & "&"
	URL = URL & "COMPORTAMENTI=" & request("COMPORTAMENTI") & "&"
	URL = URL & "AREA_COMPORTAMENTI=" & request("AREA_COMPORTAMENTI")
End if

if URL ="" then 
	URL = request("back")& "?Tipo=" & request("Tipo")
end if 
%>
<script>
	<!--
		goToPage('<%=URL%>')
	//-->
</script>
<!-- #include Virtual="/strutt_coda2.asp" -->
