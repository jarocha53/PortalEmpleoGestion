<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<center>
	<table width="745" border="0" CELLPADDING="4" CELLSPACING="2" align=middle>

<%	
	Dim sProv
	
	'Presenza di una occorrenza nella tabella PERS_MIGRAZIONE
	sSQL = "SELECT DT_INGRESSO,COM_INGRESSO," &_
			"PRV_INGRESSO,COD_MINGR FROM PERS_MIGRAZIONE" &_
			" WHERE ID_PERSONA=" & sUtente 
	
'Response.Write(sSQL) & "<br>"

'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	set rsImmigrati = CC.Execute(sSQL)
	
	if not rsImmigrati.EOF then
		DT_INGRESSO=rsImmigrati("DT_INGRESSO")
		COM_INGRESSO=rsImmigrati("COM_INGRESSO")
		PRV_INGRESSO=rsImmigrati("PRV_INGRESSO")
		COD_MINGR=rsImmigrati("COD_MINGR")
		if COD_MINGR <> "" then
			descImmigrati = DecCodVal("MINGR",0,"",COD_MINGR,"")
		end if
		if COM_INGRESSO <> "" then			
			descComune=DescrComune(COM_INGRESSO)
		end if
%>
		<tr>
				
			<td align="left" class="tbltext1" width="245" nowrap>
				<b>Fecha de ingreso al pa�s&nbsp;:</b>
			</td>
			<td align="left" class="textblack" width="500">
				<b><%=ConvDateToString(DT_INGRESSO)%></b>
			</td>
		</tr>
		<tr>
				
			<td align="left" class="tbltext1" nowrap>
				<b>Municipio/Departamento de ingreso&nbsp;:</b>
			</td>
			<td align="left" class="textblack">
				<b><%=ucase(descComune)%>&nbsp;&nbsp;(<%=PRV_INGRESSO%>)</b>
			</td>
		</tr>
		<tr>
			<td align="left" class="tbltext1">
				<b>Motivo&nbsp;:</b>
			</td>	
			<td align="left" class="textblack">
				<b><%=ucase(descImmigrati)%></b>
			</td>
		</tr>
<%	end if
	
	rsImmigrati.Close()
	set rsImmigrati = Nothing



	'Presenza di una occorrenza nella tabella PERS_DOC
	sSQL =	"SELECT * FROM PERS_DOC WHERE ID_PERSONA =" & sUtente &_
			" AND COD_DOCST='04'"

'Response.Write sSql & "<br>"

'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	set Rst = CC.Execute(sSQL)
	
	if not Rst.EOF then
		'descStatus = rsStatus("COD_STATUS_DOC")
%>		<tr>
			<td align="left" class="tbltext1" nowrap>
				<b>Permiso.&nbsp;:</b>
			</td>	
			<td align="left" class="textblack">
				<b><%=Ucase(Rst("ID_DOC"))%></b>
			</td>
		</tr>
		<tr>
			<td align="left" class="tbltext1" nowrap>
				<b>Rilasciato dallo sportello Unico<BR>della provincia di&nbsp;:</b>
			</td>
			<%	sProv=DecCodVal("PROV","0","",Rst("PRV_RIL"),"1")				
%>			<td align="left" class="textblack">
				<b><%=Ucase(sProv)%></b>
			</td>
		</tr>
		<tr>
			<td align="left" class="tbltext1" >
				<b>Fecha de Emisi�n&nbsp;:</b>
			</td>
			<td align="left" class="textblack">
				<b><%=ConvDateToString(Rst("DT_RIL_DOC"))%></b>
			</td>
		</tr>
		<tr>
			<td align="left" class="tbltext1">
				<b>Valido hasta&nbsp;:</b>
			</td>
			<td align="left" class="textblack">
				<b><%=ConvDateToString(Rst("DT_FIN_VAL"))%></b>
			</td>
		</tr>
<%	end if
	
	Rst.Close
	set Rst = nothing

%>	</table>
</center>
