<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include Virtual = "/strutt_testa2.asp"-->
<!--#include Virtual = "/include/OpenConn.asp"-->
<!--#include Virtual = "/include/ControlDateVB.asp"-->

<script LANGUAGE="Javascript">
//include del file per fare i controlli sulla validit� delle date
<!--#include Virtual = "/Include/ControlDate.inc"-->

//include del file per fare i controlli sulla numericit� dei campi
<!--#include Virtual = "/Include/ControlNum.inc"-->

//include del file per fare i controlli sulla validit� del CF
<!--#include Virtual = "/Include/ControlCodFisc.inc"-->

//include del file per fare i controlli sulle stringhe
<!--#include Virtual = "/Include/ControlString.inc"-->
<!--#include Virtual = "/Include/help.inc"-->

function ControllaDati(frmMod){
	//-------- Controlli di OBBLIGATORIETA' --------
	//-------- Controlli FORMALI            --------
	//-------- Controlli di RELAZIONE       --------
	
		if (frmInsHandicap.cmbTipoHand.value == ""){
			alert("El campo tipo de discapacidad es obligatorio!");
			frmInsHandicap.cmbTipoHand.focus() ;
			return false;
		}
		
		//Controllo della validit� della Data di Rilascio
		DataIni = frmInsHandicap.txtDataIni.value
		
		if (DataIni == ""){
			alert("El campo fecha de inicio es obligatorio!")
			frmInsHandicap.txtDataIni.focus() 
			return false
		}
		
		if (!ValidateInputDate(DataIni)){
			frmInsHandicap.txtDataIni.focus() 
			return false
		}	
		
		DataOdierna=frmInsHandicap.txtoggi.value
		if (ValidateRangeDate(DataOdierna,DataIni)==true){
			alert("La fecha de inicio debe ser anterior a la fecha actual!")
			frmInsHandicap.txtDataIni.focus()
		    return false
		}
		
		DataNascita=frmInsHandicap.TXT_DT_Nasc.value
		if (ValidateRangeDate(DataIni,DataNascita)==true){
			alert("La fecha de inicio debe ser superior a la fecha de nacimiento!")
			frmInsHandicap.txtDataIni.focus()
		    return false
		}
		
		sDiagnosi=frmInsHandicap.txtDiagnosi.value
		if (ValidateInputStringWithOutNumber(sDiagnosi)==false){
			alert("Diagn�stico no valido")
			frmInsHandicap.txtDiagnosi.focus()		    
		    return false			
		}

}
</script>

<!--#include Virtual = "/include/DecCod.asp"-->
<!--#include Virtual="/util/dbUtil.asp"-->

<%
Parametro_S=Request.QueryString("S")
sIdpers = Request.Form("ID_Persona")
sCodThand=Request.Form("COD_THAND")
sDtIniHand=Request.Form("DT_INI_HANDICAP")
sDiagnosi=cstr(Request.Form("DIAGNOSI"))
DecHand=Request.Form("DecHand")
sDtIniHand=trim(ConvDateToString(sDtIniHand))

sSQL="SELECT DT_NASC FROM PERSONA WHERE ID_PERSONA=" & sIdpers
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
Set rstLista = CC.execute(sSQL)

%>
<br>	
	
<!--#include file="menu.asp"-->

<br>

	<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
		<tr height="18">
			<td class="sfondomenu" width="67%" height="18"><span class="tbltext0"><b>&nbsp;HANDICAP</b></span></td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
			<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">(*) campo obligatorio</td>
		</tr>
	</table>
	<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
		<tr>
		<%if Parametro_S="1" then%>
			<td align="left" class="sfondocomm">
				Ingreso de discapacidad. 
				<br>
				Complete los campos y presione <b>Enviar</b> 
				para registrar la informaci�n.
			<a href="Javascript:Show_Help('/Pgm/help/Iscr_Utente/UTE_InsHandicap_1')" name="prov3" onmouseover="javascript:window.status='' ; return true">
			<img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
			</td>
		<%else%>
			<td align="left" class="sfondocomm">
				Modificaci�n de discapacidad. 
				<br>
				Complete los campos y presione <b>Enviar</b> 
				para registrar la informaci�n.
			<a href="Javascript:Show_Help('/Pgm/help/Iscr_Utente/UTE_InsHandicap_2')" name="prov3" onmouseover="javascript:window.status='' ; return true">
			<img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
			</td>
		<%end if%>
		</tr>
		<tr height="2">
			<td class="sfondocomm" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr> 		  			
   		<form method="post" name="frmInsHandicap" onsubmit="return ControllaDati(this)" action="UTE_CnfInsHandicap.asp?s=<%=Request.QueryString("s")%>">
		<input type="hidden" id="TXT_Idpers" name="TXT_Idpers" value="<%=sIdpers%>">
		<input type="hidden" id="TXT_DT_Nasc" name="TXT_DT_Nasc" value="<%=ConvDateToString(rstLista("DT_NASC"))%>">
		<input type="hidden" id="DT_TMST" name="DT_TMST" value="<%=Request.Form("DT_TMST")%>">
		<input type="hidden" id="CodThandOld" name="CodThandOld" value="<%=Request.Form("COD_THAND")%>">
		<table border="0" cellpadding="0" cellspacing="1" width="500">
	   		<br>
		    <tr>
				<td align="middle" colspan="2" nowrap class="tbltext1" width="120">
					<p align="left">
						<strong>&nbsp;</strong>
						<strong>Tipo de discapacidad*</strong>
						<strong>&nbsp;</strong>
					</p>
				</td>
				<td align="left" colspan="2" width="60%" width="400">
					<%
					sInt = "THAND|0|" & date & "|" & sCodThand & "|cmbTipoHand|ORDER BY DESCRIZIONE"			
					if Request.QueryString("s") = 1 then
						CreateCombo(sInt)				
					else
					%>
						<font class="textblack"><b><%=DecHand%></b></font>
						<input type="hidden" name="cmbTipoHand" value="<%=sCodThand%>">
					<%end if%>

				</td>
		    </tr>
			<tr>     
				<td align="middle" colspan="2" nowrap class="tbltext1" width="120">
					<p align="left">
						<strong>&nbsp;</strong>
						<strong>&nbsp;&nbsp;Fecha de inicio*</strong><font size="1">&nbsp;(gg/mm/aaaa)</font>
						<strong>&nbsp;</strong>
					</p>
				</td>	
				<td align="left" colspan="2" width="60%" width="400">
					<input name="txtDataIni" type="text" maxlength="10" size="10" class="textblacka" value="<%=sDtIniHand%>">
					<input name="txtoggi" type="hidden" value="<%=ConvDateToString(Date())%>">
				</td>
		    </tr>
			<tr>
				<td align="middle" colspan="2" nowrap class="tbltext1" width="120">
					<p align="left">
						<strong>&nbsp;</strong>
						<strong>Diagn�stico</strong>
						<strong>&nbsp;</strong>
					</p>
				</td>
				<td align="left" colspan="2" width="60%" width="400">
					<span class="textblacka">
						<input name="txtDiagnosi" style="width:300px" type="text" id="text1" class="textblacka" style="TEXT-TRANSFORM: uppercase" maxlength="50" value="<%=sDiagnosi%>"> 
					</span>
				</td>
		    </tr>
		</table>				    
		<table WIDTH="500">
		    <tr>
		        <td align="left" colspan="2">&nbsp;</td>
		        <td align="left" colspan="2" width="60%">&nbsp;</td>
		    </tr>
		</table>
		<br>
		<table border="0" cellpadding="1" cellspacing="1" width="500">
			<tr>
			    <td align="middle" colspan="2">
			    <input type="image" src="<%=Session("Progetto")%>/images/conferma.gif" border="0" value="Inserisci" onclick="return ControllaDati(this)" id="image1" name="image1">
			    </td>
			</tr>
		</table>
		</form>
<!--#include virtual = "/include/CloseConn.asp"-->
<!--#include Virtual = "/strutt_coda2.asp"-->

