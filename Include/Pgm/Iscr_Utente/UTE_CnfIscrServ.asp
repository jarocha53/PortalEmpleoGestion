<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%
 Response.ExpiresAbsolute = Now() - 1 
 Response.AddHeader "pragma","no-cache"
 Response.AddHeader "cache-control","private"
 Response.CacheControl = "no-cache"
%>

<!--#include Virtual = "/include/controlDateVB.asp"-->
<!--#include Virtual = "/util/portallib.asp"-->
<!--#include Virtual="/strutt_testa2.asp"-->
<!--#include Virtual = "/include/OpenConn.asp"-->
<!--#include Virtual = "/include/ControlDateVB.asp"-->
<!--#include Virtual= "/include/SysFunction.asp"-->
<!--#include Virtual = "/util/dbutil.asp"-->
<!--#include virtual = "/include/DecComun.asp"-->

<%
'--------------------------------------------------------------------------------------------------------------------------------------------------------

sub Inizio()

	%>
	<table border="0" width="520" cellspacing="0" cellpadding="0" height="81">
		<tr>
			<td width="520" background="<%=Session("Progetto")%>/images/titoli/strumenti2b.gif" height="81" valign="bottom" align="right">
				<table border="0" background width="260" height="30" cellspacing="0" cellpadding="0">
					<tr>
						<td width="100%" valign="top" align="right"><b class="tbltext1a">NUEVO NOMBRE &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>	
	<br>	
	<%

end sub

'--------------------------------------------------------------------------------------------------------------------------------------------------------

sub ImpVariabili()
	sAutorizz=Request.Form("chkAutorizz")

	'-----------------------------------------------------
	'sCognome=Session("Cognome")
	sCognomeUtente = Replace(Session("Cognome"),"'","''")

	'sNome=Session("Nome")
	sNomeUtente = Replace(Session("Nome"),"'","''")
	'-----------------------------------------------------
	
	sDataNascita = UCase(Request.Form ("txtDataNascita"))
	sDataNascita = Replace(server.HTMLEncode(sDataNascita), "'", "''")

	sComuneNascita = UCase(Request.Form ("txtComNas"))

	sProvNascita = UCase(Request.Form ("cmbProvinciaNascita"))
	sNazNascita = UCase(Request.Form ("cmbNazioneNascita"))

	sSesso = UCase(Request.Form ("cmbSesso"))

	sCodFisc = UCase(Request.Form ("txtCodFisc"))
	sCodFisc = Replace(server.HTMLEncode(sCodFisc), "'", "''")

	'residenza
	sProvResidenza = UCase(Request.Form("cmbProvRes"))
	sCodComRedidenza = UCase(Request.Form("txtComRes"))

	select case UCase(Request.Form ("cmbNazioneNascita"))
		case ""
			sCittad = "IT"
		case else
			sCittad = UCase(Request.Form ("cmbNazioneNascita")) 
	end select

	select case Request.Form ("chkAutorizz")
		case "S"
			sAutorizz = "S"
		case else
			sAutorizz = "N"
	end select

end sub

'--------------------------------------------------------------------------------------------------------------------------------------------------------

function InsPersona()

	InsPersona = "0"
	
	sSQLPersona = "INSERT INTO PERSONA " & _
					"(COGNOME," &_
					"NOME," &_
					"DT_NASC," &_
					"COM_NASC," &_
					"PRV_NASC," &_
					"STAT_NASC," &_
					"SESSO," &_
					"COD_FISC," &_
					"PRV_RES," &_
					"COM_RES," &_
					"DT_ULT_RIL," &_
					"DT_TMST) VALUES " &_	
					"('" & sCognomeUtente  & _
					"','" & sNomeUtente & _
					"'," & ConvDateToDbs(ConvStringToDate(sDataNascita)) & _
					",'" & sComuneNascita & _
					"','" & sProvNascita & _
					"','" & sNazNascita & _
					"','" & sSesso &  _
					"','" & sCodFisc &  _
					"','" & sProvResidenza &  _
					"','" & sCodComRedidenza &  _
					"'," & ConvDateToDb(Now) & _
					", " & ConvDateToDb(Now) & ")"
						
	Errore = EseguiNoC(sSQLPersona, CC)			
	
	if Errore <> "0" then
		InsPersona = Errore
	end if
			 
end function

'--------------------------------------------------------------------------------------------------------------------------------------------------------

function EseguiInsert()

	EseguiInsert = "0"
	
	'Controllo se esiste gi� il codice fiscale su persona (persona gi� iscritta)
	set rsPers = Server.CreateObject("ADODB.Recordset")					  
	sSQL = "SELECT ID_PERSONA FROM PERSONA WHERE COD_FISC= '" & sCodFisc & "'"
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	rsPers.Open sSQL, CC, 3

	'se la persona non era iscritta la inserisco
	If rsPers.EOF Then
		
		Errore = InsPersona()
	
		If Errore <> "0" Then
			EseguiInsert = Errore
			exit function
		End If

		rsPers.Close 
		
		'prendo l'Id_Persona inserito per fare la INSERT in Dichiaraz e UPDATE in Utente
		sSQL = "SELECT ID_PERSONA FROM PERSONA WHERE COD_FISC= '" & sCodFisc & "'"
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
		rsPers.Open sSQL, CC, 3	
		
		sAutoIncrocio = "S"
		If Session("progetto") = "/ITALIALAVORO" Then
			sAutoIncrocio = "N"
		End If
										
		sSQL = "INSERT INTO DICHIARAZ " &_
				"(ID_PERSONA," &_
				"AUT_TRAT_DATI," &_
				"AUT_INCROCIO," &_
				"DT_TMST) VALUES " &_
				"(" & sPersona & _
				", '" & sAutorizz & _
				"', '" & sAutoIncrocio & _
				"', " & ConvDateToDb(Now) &")"
												
		Errore = EseguiNoC(sSQL, CC)	
	End if
		sPersona = CLng(rsPers("ID_PERSONA"))
	
		sSQL = "UPDATE UTENTE SET TIPO_PERS = 'P', CREATOR = " & CLng(sPersona) & _
				", DT_TMST = " & ConvDateToDb(Now) & _
				" WHERE LOGIN = '" & Session("Login") & "'"
			
		Errore = EseguiNoC(sSQL, CC)
'	End If
	
	session("creator")=CLng(sPersona)
	session("tipopers")="P"
	
	If Errore <> "0" then
	    'Errore = "Non e' stato possibile accedere al server. Riprovare piu' tardi."
		EseguiInsert = Errore
		exit function
	End If
				    				  
	rsPers.Close 
	set rsPers = nothing

End function

'--------------------------------------------------------------------------------------------------------------------------------------------------------

sub ImpostaPag()
		
	CC.BeginTrans

	Errore = EseguiInsert()

	if Errore = "0" then 
		CC.CommitTrans
		%>
		<br>
		<table border="0" cellspacing="2" cellpadding="1" width="500">
			<tr align="middle">
				<td class="tbltext3">
					Ingreso correctamente efectuado.
				</td>
			</tr>
		</table>
		<%	 	
		If session("progetto") = "/ITALIALAVORO" then
			%><meta HTTP-EQUIV="Refresh" CONTENT="1; URL='/ITALIALAVORO/home.asp'"><%
		Else 
			%>	
			<form name="frm1" action="/Pgm/Iscr_Utente/UTE_ModDati.asp" method="post">
			    <input type="hidden" name="Idpers" value="<%=sPersona%>">
			</form>    
			<meta HTTP-EQUIV="Refresh" CONTENT="1; URL='javascript:frm1.submit();'"><%
		End if	 
			
	else 
		CC.RollbackTrans
		%>
		<table border="0" cellspacing="2" cellpadding="1" width="500">
			<tr align="middle">
				<td class="tbltext3" width="500" align="center">
					El ingreso no puede realizarse.
				</td>
			</tr>
			<tr align="center">
				<td class="tbltext3">
					<%=Errore%> 
				</td>
			</tr>
		</table>
		<br>
		<table WIDTH="500" BORDER="0" CELLPADDING="0" CELLSPACING="0">
			<tr align="center">
				<td>
					<a href="javascript:history.go(-1)"><img src="<%=Session("progetto")%>/images/Indietro.gif" border="0"></a>
				</td>
			</tr>
		</table>
		<%
		
	end if

end sub

'------------------------------------ MAIN ---------------------------------------------------------------------------------------------------------------

'dim sCognome
'dim sNome
dim sDataNascita
dim sComuneNascita
dim sProvNascita
dim sNazNascita
dim sCodFisc
dim sCittad
dim sComune
dim sProv
dim sStato
dim sSesso
dim sAutorizz
dim sSQL
dim sPersona
dim Errore
dim DescEsito
dim sNomeUtente
dim sCognomeUtente

	Inizio()
	
	ImpVariabili()
	
	ImpostaPag()

%>
<!--#include VIRTUAL="/include/CloseConn.asp"-->
<!--#include Virtual="/strutt_Coda2.asp"-->

