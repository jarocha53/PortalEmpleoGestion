<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--META HTTP-EQUIV="Pragma" CONTENT="no-cache"><META HTTP-EQUIV="Cache-Control" CONTENT="no-cache"-->
<%
 Response.ExpiresAbsolute = Now() - 1 
 Response.AddHeader "pragma","no-cache"
 Response.AddHeader "cache-control","private"
 Response.CacheControl = "no-cache"
%>

<!--#include Virtual = "/include/controlDateVB.asp"-->
<!--#include Virtual = "/util/portallib.asp"-->
<!--#include Virtual = "/strutt_testa2.asp"-->
<!--#include Virtual = "/include/OpenConn.asp"-->
<!--#include Virtual = "/include/SysFunction.asp"-->
<!--#include Virtual = "/util/dbutil.asp"-->
<!--#include Virtual = "/include/DecComun.asp"-->
<!--#include Virtual = "/include/ValInfPers.asp"-->

<script LANGUAGE="javascript">
function Esci(){
	open.document.frmDomIscri.reset
}

</script>
<form name="Refresh" method="post" action="UTE_VisAssistenza.asp">
 			
<%

Dim sIdPers
Dim sDtIniAss
Dim sDtFinAss
Dim sCodEnte
Dim sDtTmst

Dim sDecorrenza
Dim sScadenza

Dim Errore

Dim sCond

Dim sSqlCond

Dim sFlag

Dim chiave

sIdPers = Request.Form("txtIdPers")
sDtIniAss = Request.Form("txtDtIni")
sDtFinAss = Request.Form("txtDtFin")
sCodEnte = Request.Form("cmbEnteAss")
sDtTmst = Request.Form("txtTMST")

sFlag = Request.Form("txtFlag")

sDecorrenza = ConvDateToDbS(ConvStringToDate(sDtIniAss))

if sDtFinAss <> "" then
		
	sScadenza = ConvDateToDbS(ConvStringToDate(sDtFinAss))

else

	sScadenza = "''"

end if

'MODIFICA --> UPDATE
CC.BeginTrans

chiave = sIdpers & "|" & sDtIniAss


if sFlag = "Mod" then

	Dim rsCnd

	sCond = " ((DT_INI_ASS BETWEEN " & sDecorrenza &_
			" AND nvl(" & sScadenza & ",sysdate)) OR " &_
			" (DT_FIN_ASS BETWEEN " & sDecorrenza &_
			" AND nvl(" & sScadenza & ",sysdate)) OR " &_
			" (" & sDecorrenza & " BETWEEN DT_INI_ASS AND DT_FIN_ASS) OR " &_
			" ( nvl(" & sScadenza & ",sysdate) BETWEEN DT_INI_ASS AND DT_FIN_ASS)" &_
			")" 


	sSqlCnd = "SELECT ID_PERSONA, DT_INI_ASS, DT_FIN_ASS, COD_ENTE_ASS, " &_
				"DT_TMST FROM PERS_ASSDIS " &_
				"WHERE ID_PERSONA = " & sIdPers & " AND " & sCond & " AND DT_INI_ASS <> " & sDecorrenza


'PL-SQL * T-SQL  
SSQLCND = TransformPLSQLToTSQL (SSQLCND) 
	set rsCnd = CC.Execute(sSqlCnd)

	if rsCnd.eof then
		
		Dim sSqlUp
			
		Dim sValInf

		sSqlUp = "UPDATE PERS_ASSDIS SET DT_FIN_ASS = " & sScadenza & ", " &_
					"COD_ENTE_ASS = '" & sCodEnte & "', DT_TMST = " & ConvDateToDb(NOW()) & " " &_
					"WHERE ID_PERSONA = " & sIdPers & " AND DT_INI_ASS = " & ConvDateToDbS(ConvStringToDate(sDtIniAss)) & " "
		 				
		Errore = EseguiNoCTrace(UCase(Mid(Session("progetto"),2)), chiave, "PERS_ASSDIS", Session("idutente"), sIdpers, "P", "MOD", sSqlUp, 1, sDtTmst, cc)	
			
		if Errore = "0" then
			
			sValInf = ValInfPers(sIdpers,cc)
			Errore = sValInf	
		   
			if Errore = "0" then
			
				CC.CommitTrans
					
				%>
				<br>
				<!--#include file="menu.asp"-->
				<br>			
				<br>
				
				<table border="0" cellspacing="2" cellpadding="1" width="500">
				 	<tr>
				 	<td align="center"> 
										
				 	</td>
				 </tr>
				</table>
				
				<script>
					alert("Modificación efectuada");
					Refresh.submit()
				</script>	
								
				<%
					
			else
		
				CC.RollbackTrans
				
				%>	
				<br>
				<!--#include file="menu.asp"-->
				<br>
				<table border="0" cellspacing="2" cellpadding="1" width="500">
					<tr align="middle">
						<td class="tbltext3" width="500">
							Modificación no efectuada.
						</td>
					</tr>
					<tr align="middle">
						<td class="tbltext3">
							<%Response.Write (Errore)%> 
						</td>
					</tr>
				</table>
				<br>
				<table WIDTH="500" BORDER="0" CELLPADDING="0" CELLSPACING="0">
					<tr align="center">
						<td>
							<a href="javascript:history.back()"><img src="<%=Session("progetto")%>/images/Indietro.gif" border="0"></a>
						</td>
					</tr>
				</table>			

				<%
							
			end if

		else
				
			CC.RollbackTrans
				
			%>	
			<br>
			<!--#include file="menu.asp"-->
			<br>
			<table border="0" cellspacing="2" cellpadding="1" width="500">
				<tr align="middle">
					<td class="tbltext3" width="500">
						Modificación de datos no efectuada. 
					</td>
				</tr>
				<tr align="middle">
					<td class="tbltext3">
						<%Response.Write (Errore)%> 
					</td>
				</tr>
			</table>
			<br>
			<table WIDTH="500" BORDER="0" CELLPADDING="0" CELLSPACING="0">
				<tr align="center">
					<td>
						<a href="javascript:history.back()"><img src="<%=Session("progetto")%>/images/Indietro.gif" border="0"></a>
					</td>
				</tr>
			</table>			

			<%
						
		end if
		
	else
		CC.RollbackTrans		
		%>	
		<br>
		<!--#include file="menu.asp"-->
		<br>
		<table border="0" cellspacing="2" cellpadding="1" width="500">
			<tr align="middle">
				<td class="tbltext3" width="500">
					Modificación no efectuada.<br> 
					Pruebe posteriormente.
				</td>
			</tr>
			<tr align="middle">
				<td class="tbltext3">
					<%Response.Write (Errore)%> 
				</td>
			</tr>
		</table>
		<br>
		<table WIDTH="500" BORDER="0" CELLPADDING="0" CELLSPACING="0">
			<tr align="center">
				<td>
					<a href="javascript:history.back()"><img src="<%=Session("progetto")%>/images/Indietro.gif" border="0"></a>
				</td>
			</tr>
		</table>			

		<%
						
	end if		
		
		

'INSERIMENTO --> INSERT
else

	Dim rsCond

	sCond = " ((DT_INI_ASS BETWEEN " & sDecorrenza &_
			" AND nvl(" & sScadenza & ",sysdate)) OR " &_
			" (DT_FIN_ASS BETWEEN " & sDecorrenza &_
			" AND nvl(" & sScadenza & ",sysdate)) OR " &_
			" (" & sDecorrenza & " BETWEEN DT_INI_ASS AND DT_FIN_ASS) OR " &_
			" ( nvl(" & sScadenza & ",sysdate) BETWEEN DT_INI_ASS AND DT_FIN_ASS)" &_
			")" 

	sSqlCond = "SELECT ID_PERSONA, DT_INI_ASS, DT_FIN_ASS, COD_ENTE_ASS, " &_
				"DT_TMST FROM PERS_ASSDIS " &_
				"WHERE ID_PERSONA = " & sIdPers & " AND " & sCond

'PL-SQL * T-SQL  
SSQLCOND = TransformPLSQLToTSQL (SSQLCOND) 
	set rsCond = CC.Execute(sSqlCond)

	if rsCond.eof then

		Dim sSqlIns

		sSqlIns = "INSERT INTO PERS_ASSDIS (ID_PERSONA, DT_INI_ASS, DT_FIN_ASS, COD_ENTE_ASS, " &_
					"DT_TMST) VALUES (" & sIdpers & ", " & ConvDateToDbS(ConvStringToDate(sDtIniAss)) & ", " & sScadenza & ", " &_
					" '" & sCodEnte & "', " & ConvDateToDb(NOW()) & ")" 
	
		Errore = EseguiNoCTrace(UCase(Mid(Session("progetto"),2)), chiave, "PERS_ASSDIS", Session("idutente"), sIdpers, "P", "INS", sSqlIns, 1, ConvDateToDb(NOW()), cc)

		if Errore = "0" then
					
			sValInf = ValInfPers(sIdpers,cc)
			Errore = sValInf	
				   
			if Errore="0" then
					
				CC.CommitTrans
		
				%>
				<br>
				<!--#include file="menu.asp"-->
				<br>			
				<br>
				<table border="0" cellspacing="2" cellpadding="1" width="500">
				 	<tr>
				 	<td align="center"> 
										
				 	</td>
				 </tr>
				</table>
				
				<script>
					alert("Ingreso correctamente efectuado.");
					Refresh.submit()
				</script>					
				<%	
					
			else	
		
				CC.RollbackTrans
					
				%>	
				<br>
				<!--#include file="menu.asp"-->
				<br>
				<table border="0" cellspacing="2" cellpadding="1" width="500">
					<tr align="middle">
						<td class="tbltext3" width="500">
							Ingreso correctamente efectuado.
						</td>
					</tr>
					<tr align="middle">
						<td class="tbltext3">
							<%Response.Write (Errore)%> 
						</td>
					</tr>
				</table>
				<br>
				<table WIDTH="500" BORDER="0" CELLPADDING="0" CELLSPACING="0">
					<tr align="center">
						<td>
							<a href="javascript:history.back()"><img src="<%=Session("progetto")%>/images/Indietro.gif" border="0"></a>
						</td>
					</tr>
				</table>			

				<%	
						
			end if				

		else

			%>

			CC.RollbackTrans
			
			<br>
			<!--#include file="menu.asp"-->
			<br>
			<table border="0" cellspacing="2" cellpadding="1" width="500">
				<tr align="middle">
					<td class="tbltext3" width="500">
						Ingreso de datos no efectuado. 
					</td>
				</tr>
				<tr align="middle">
					<td class="tbltext3">
						<%Response.Write (Errore)%> 
					</td>
				</tr>
			</table>
			<br>
			<table WIDTH="500" BORDER="0" CELLPADDING="0" CELLSPACING="0">
				<tr align="center">
					<td>
						<a href="javascript:history.back()"><img src="<%=Session("progetto")%>/images/Indietro.gif" border="0"></a>
					</td>
				</tr>
			</table>

			<%

		end if
		
	else
		CC.RollbackTrans
		%>
		
		<br>
		<!--#include file="menu.asp"-->
		<br>
		<table border="0" cellspacing="2" cellpadding="1" width="500">
			<tr align="middle">
				<td class="tbltext3" width="500">
					Ingreso de datos no efectuado.<br>
					Periodo Superpuesto.
				</td>
			</tr>
			<tr align="middle">
				<td class="tbltext3">
					<%Response.Write (Errore)%> 
				</td>
			</tr>
		</table>
		<br>
		<table WIDTH="500" BORDER="0" CELLPADDING="0" CELLSPACING="0">
			<tr align="center">
				<td>
					<a href="javascript:history.back()"><img src="<%=Session("progetto")%>/images/Indietro.gif" border="0"></a>
				</td>
			</tr>
		</table>		
		
		<%
	end if
	
end if
%>

</form>	
<!--#include VIRTUAL="/include/CloseConn.asp"-->
<!--#include Virtual="/strutt_Coda2.asp"-->
