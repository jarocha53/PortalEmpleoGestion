<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include Virtual = "/strutt_testa2.asp"-->
<!--#include Virtual = "/include/OpenConn.asp"-->
<!--#include Virtual = "/include/DecCod.asp"-->


<script LANGUAGE="Javascript">

/********************************************************
*   @autor: IPTECHONOLOGIES                             +
********************************************************/


$(function(){
	change_cmbStatoEstero()
	change_cmbNazioneNascita()	
})
change_cmbStatoEstero= function(){
	var cod_country_selected = $("#cmbStatoEstero").val()
    var cmbProvRes = $("#cmbProvRes") 
    var imgPunto1  = $("#imgPunto2")
    var txtComRes = $("#txtComuneRes")



    cmbProvRes.html("");
    if(cod_country_selected != "CO"){
    	 cmbProvRes.attr("disabled","disabled")
    	 imgPunto1.hide()
    	 txtComRes.parent().children().filter("input").val("")
    	 $("#txtComuneRes").val("")
    }else{
    	cmbProvRes.removeAttr("disabled")
    	imgPunto1.show()
    }

    $.get("../GestProgetti/Alfabeta/Selezione/Utente/AJAX_load_states_by_country_code.asp?countrycode="+cod_country_selected,function(data){
         cmbProvRes.html(data);
    })
}
change_cmbNazioneNascita = function(){
  	var cod_country_selected = $("#cmbNazioneNascita").val()
    var cmbProvRes = $("#cmbProvinciaNascita") 
    var imgPunto1  = $("#imgPunto1")
    var txtComRes = $("#txtComRes")



    cmbProvRes.html("");
    if(cod_country_selected != "CO"){
    	 cmbProvRes.attr("disabled","disabled")
    	 imgPunto1.hide()
    	 txtComRes.parent().children().filter("input").val("")
    	 $("#txtComuneNascita").val("")
    }else{
    	cmbProvRes.removeAttr("disabled")
    	imgPunto1.show()
    }

    $.get("../GestProgetti/Alfabeta/Selezione/Utente/AJAX_load_states_by_country_code.asp?countrycode="+cod_country_selected,function(data){
         cmbProvRes.html(data);
    })
}
/********************************************************/
//include del file per fare i controlli sulla validit� delle date
<!--#include virtual = "/include/SelComune.js"-->
<!--#include virtual = "/include/SelDepto.js"-->
<!--#include virtual = "/include/SelLoc.js"-->
// funcion que valida la provincia



//include del file per fare i controlli sulla validit� delle date
<!--#include Virtual = "/Include/ControlDate.inc"-->
// valida fechas


//include del file per fare i controlli sulla numericit� dei campi
<!--#include Virtual = "/Include/ControlNum.inc"-->
// tiene una funcion que controla valores numericos

//SM include controllo Cedula de Identidad
<!--#include Virtual = "/Include/ControlloCI.inc"-->

//include del file per fare i controlli sulla validit� delle date
<!--#include Virtual = "/Include/ControlCodFisc.inc"-->

//include del file per fare i controlli sulla stringa
<!--#include Virtual = "/Include/ControlString.inc"-->
<!--#include Virtual = "/Include/help.inc"-->



function PulisciCom()
{
	document.frmDomIscri.txtComuneNascita.value = ""
	document.frmDomIscri.txtComune.value = ""
	// SM INIZIO 13092007
	//document.frmDomIscri.txtDeptoNacimiento.value = ""
	//document.frmDomIscri.txtDepto.value = ""
	//document.frmDomIscri.txtLocNacimiento.value = "" 
	//document.frmDomIscri.txtLoc.value = ""
	// SM FINE 13092007
}

function PulisciRes()
{
	document.frmDomIscri.txtComuneRes.value = ""
	document.frmDomIscri.txtComRes.value = ""
	// SM INIZIO 13092007
	//document.frmDomIscri.txtDtoResidencia.value = ""
	//document.frmDomIscri.txtDto.value = ""
	//document.frmDomIscri.txtLcResidencia.value = ""
	//document.frmDomIscri.txtLc.value = ""
	// SM FINE 13092007
}

function esmenor (dateofborn)
{
	//Set the two dates
	today=new Date()
	var borndate=new Date(dateofborn)
	//Set 1 day in milliseconds
	var one_day=1000*60*60*24

	//Calculate difference btw the two dates, and convert to days
	var days = Math.ceil((today.getTime()-borndate.getTime())/(one_day))
	return days		
}


//Funzione per i controlli dei campi da inserire 
	function ControllaDati()
{

		//alert(frmDomIscri.txtCodFisc.value.substr(2,1));
		tipodoc = document.frmDomIscri.cmbTipoDoc.value
        if(tipodoc == "")
        {
            alert("Debe ingresar el Tipo de Documento")
			document.frmDomIscri.cmbTipoDoc.focus() 
			return false
        }

		
		document.frmDomIscri.txtCognome.value=TRIM(document.frmDomIscri.txtCognome.value)
		document.frmDomIscri.txtCodFisc.value = TRIM(document.frmDomIscri.txtCodFisc.value)
		
		if (document.frmDomIscri.txtCodFisc.value == "")
		{
			alert("Debe ingresar un N�mero de Identificaci�n");
			document.frmDomIscri.txtCodFisc.focus()
			return false;
		}
		
		// SM inizio- controllo N�mero de Identificaci�n
		
		document.frmDomIscri.txtCodFisc.value=TRIM(document.frmDomIscri.txtCodFisc.value)
		if (document.frmDomIscri.txtCodFisc.value != "")
		{
				blank = " ";
				if (!ChechSingolChar(document.frmDomIscri.txtCodFisc.value,blank))
				{
			
					alert("El campo N�mero de Identificaci�n no puede contener espacios en blanco")
					document.frmDomIscri.txtCodFisc.focus()
					return false;
				}
				
				// if ((document.frmDomIscri.txtCodFisc.value.length > 10))
				// {
				// 	alert("El campo N�mero de Identificaci�n debe ser menor de 10 caracteres")
				// 	document.frmDomIscri.txtCodFisc.focus() 
				// 	return false
				// }			
				if (!ValidateInputStringWithNumber(document.frmDomIscri.txtCodFisc.value))
				{
					alert("El campo N�mero de Identificaci�n  es err�neo")
					document.frmDomIscri.txtCodFisc.focus() 
					return false
				}
				
				if (!IsNum(document.frmDomIscri.txtCodFisc.value))
				{
					alert("El campo N�mero de Identificaci�n debe ser num�rico")
					document.frmDomIscri.txtCodFisc.focus() 
					return false
				}	
		
				// if (!ControlloCI(eval(document.frmDomIscri.txtCodFisc.value)))
				// {
				//      document.frmDomIscri.txtCodFisc.focus()
				//      return false
				// }
				
		}
		
		// SM fine
				
		//Cognome
		if (document.frmDomIscri.txtCognome.value == "")
		{
			alert("El campo Primer Apellido es obligatorio!")
			document.frmDomIscri.txtCognome.focus() 
			return false
		}
		
		//sCognome=ValidateInputStringWithOutNumber(document.frmDomIscri.txtCognome.value)
		
		//if  (sCognome==false){
		//	alert("Cognome formalmente errato.")
		//	document.frmDomIscri.txtCognome.focus() 
		//	return false
		//}
		
		//Nome
		document.frmDomIscri.txtNome.value=TRIM(document.frmDomIscri.txtNome.value)
		
		if (document.frmDomIscri.txtNome.value == "")
		{	
			alert("El campo Primer Nombre es obligatorio!")
			document.frmDomIscri.txtNome.focus() 
			return false
		}
		
		document.frmDomIscri.txt_secondo_nome.value=TRIM(document.frmDomIscri.txt_secondo_nome.value)
		//SECONDO_COGNOME
		document.frmDomIscri.txt_secondo_cognome.value=TRIM(document.frmDomIscri.txt_secondo_cognome.value)
		
		
		sNome=ValidateInputStringWithOutNumber(document.frmDomIscri.txtNome.value)	
		if  (sNome==false){
			alert("EL Primer Nombre es erroneo.")
			document.frmDomIscri.txtNome.focus() 
			return false
		}

		
		
	  	//Data di Nascita
		if (document.frmDomIscri.txtDataNascita.value == "")
		{
			alert("El campo fecha de nacimiento es obligatorio!")
			document.frmDomIscri.txtDataNascita.focus() 
			return false
		}

		//Controla el registro de los menores de 15 a�os
		if (esmenor(document.frmDomIscri.txtDataNascita.value) < 5479)
		{
			alert("No se acepta el registro de menores de 15 a�os!")
			document.frmDomIscri.txtDataNascita.focus() 
			return false
		}
		
		//Controllo della validit� della Data di Nascita
		DataNascita = document.frmDomIscri.txtDataNascita.value
		DataOdierna = frmDomIscri.txtOggi.value
		
		if (!ValidateInputDate(DataNascita))
		{
			document.frmDomIscri.txtDataNascita.focus() 
			return false
		}
		
		if (ValidateRangeDate(DataNascita,DataOdierna)==false){
			alert("La fecha de nacimiento debe ser inferior a la fecha actual!")
			frmDomIscri.txtDataNascita.focus()
			return false
		}		
				
		//Comune di Nascita e Provincia di Nascita obbligatori
		//e in alternativa con Nazione di Nascita
		document.frmDomIscri.txtComuneNascita.value=TRIM(document.frmDomIscri.txtComuneNascita.value)
		/*if ((document.frmDomIscri.txtComuneNascita.value == "")||
			(document.frmDomIscri.txtComuneNascita.value == " ")){ 
			if (document.frmDomIscri.cmbProvinciaNascita.value == ""){
				if (document.frmDomIscri.cmbNazioneNascita.value == ""){
					alert("El campo Municipio y Provincia de Origen o la Nacionalidad son obligatorios!")
					document.frmDomIscri.txtComuneNascita.focus() 
					return false
				}
			}	
		}*/
		
		/*if (document.frmDomIscri.txtComuneNascita.value != ""){ 
			if (document.frmDomIscri.cmbProvinciaNascita.value != ""){
				if(document.frmDomIscri.cmbNazioneNascita.value != ""){
					alert("Indicar el Departamento y Municipio de Origen o unicamente la nacionalidad de no ser uruguayo.")
					document.frmDomIscri.txtComuneNascita.focus() 
					return false
				}
			}	
		}
		*/
		//Se Comune di Nascita � digitato, la Provincia � obbligatoria
		/*if (document.frmDomIscri.txtComuneNascita.value != ""){
			if (document.frmDomIscri.cmbProvinciaNascita.value == ""){
				alert("El campo provincia de origen es obligatorio!")
				document.frmDomIscri.cmbProvinciaNascita.focus() 
				return false
			}
		}*/
		
	    //Se la Provincia di Nascita � digitata, il Comune � obbligatorio
		/*if ((document.frmDomIscri.txtComuneNascita.value == "")||
			(document.frmDomIscri.txtComuneNascita.value == " "))
		{
			if (document.frmDomIscri.cmbProvinciaNascita.value != "")
			{
				alert("El campo municipio de origen es obligatorio!")
				document.frmDomIscri.txtComuneNascita.focus() 
				return false
			}
		}*/


	    //Se la Provincia di Nascita � digitata, il depto � obbligatorio
		/*if ((document.frmDomIscri.txtDeptoNacimiento.value == "")||
			(document.frmDomIscri.txtDeptoNacimiento.value == " "))
		{
			if (document.frmDomIscri.cmbProvinciaNascita.value != "")
			{
				alert("El campo Departamento de nacimiento es obligatorio!")
				document.frmDomIscri.txtDeptoNacimiento.focus() 
				return false
			}
		}*/
	
	    //Se la Provincia di Nascita � digitata, il localidad � obbligatorio
		/*if ((TRIM(document.frmDomIscri.txtDeptoNacimiento.value) != "Sin Departamento") &&
		(document.frmDomIscri.txtLocNacimiento.value == ""))
		{
			if (document.frmDomIscri.cmbProvinciaNascita.value != "")
			{
				alert("El campo Localidad de nacimiento es obligatorio!")
				document.frmDomIscri.txtLocNacimiento.focus() 
				return false
			}
		}*/	
		
	// SM inizio - 11102007
	
	
	// SM fine - 11102007
	
	    //Se la Provincia di Nascita � digitata, il depto � obbligatorio
		/*if ((document.frmDomIscri.txtDtoResidencia.value == "")||
			(document.frmDomIscri.txtDtoResidencia.value == " "))
		{
			if (document.frmDomIscri.cmbProvRes.value != "")
			{
				alert("El campo Departamento de residencia es obligatorio!")
				document.frmDomIscri.txtDtoResidencia.focus() 
				return false
			}
		}*/
	
	    //Se la Provincia di Nascita � digitata, il localidad � obbligatorio
		/*if ((TRIM(document.frmDomIscri.txtLcResidencia.value) != "Sin Departamento") &&
		(document.frmDomIscri.txtLcResidencia.value == ""))
		{
			if (document.frmDomIscri.cmbProvRes.value != "")
			{
				alert("El campo Localidad de residencia es obligatorio!")
				document.frmDomIscri.txtLcResidencia.focus() 
				return false
			}
		}*/	
		
	   //Sesso
	   Sesso=document.frmDomIscri.cmbSesso.value
		if (Sesso == "")
		{
			alert("El campo sexo es obligatorio!")
			document.frmDomIscri.cmbSesso.focus() 
			return false
		}	
 
 		//el pais de nacimiento
		paisnacimiento = document.frmDomIscri.cmbNazioneNascita.value;
		//if (( paisnacimiento == "")||(paisnacimiento == " ")) 
		if ( paisnacimiento == "")
		{
				alert("El Pa�s de Nacimiento es obligatorio!")
				document.frmDomIscri.cmbNazioneNascita.focus() 
				return false
		}
		 
		 
		 if (paisnacimiento == "CO")
		{
		
		
		//Se la departemento di nacimiento � vuota, il Comune � obbligatorio
		    if ((document.frmDomIscri.cmbProvinciaNascita.value == "")||
			    (document.frmDomIscri.cmbProvinciaNascita.value == " ")) {
				    alert("El Departamento de Nacimiento es obligatorio!")
				    document.frmDomIscri.cmbProvinciaNascita.focus() 
				    return false
		    }		
    		
		    //Se Comune di Residenza � vuoto, il Comune � obbligatorio
		    if ((document.frmDomIscri.txtComuneNascita.value == "")||
			    (document.frmDomIscri.txtComuneNascita.value == " ")) {
				    alert("El Municipio de Nacimiento es obligatorio!")
				    document.frmDomIscri.txtComuneNascita.focus() 
				    return false
    			
		    }	
		    //Se Comune di Residenza � digitato, la Provincia � obbligatoria
		    if (document.frmDomIscri.txtComuneNascita.value != ""){
			    if (document.frmDomIscri.cmbProvinciaNascita.value == ""){
				    //alert("Se viene selezionato il Comune di Residenza, anche il campo Provincia di Residenza � obbligatorio!")
				    alert("Si ha seleccionado el campo municipio, tambi�n el campo Departamento de Nacimiento es obligatorio!")
				    document.frmDomIscri.cmbProvinciaNascita.focus() 
				    return false
			    }
		    }
	        //Se la Provincia di Residenza � digitata, il Comune � obbligatorio
		    if ((document.frmDomIscri.txtComuneNascita.value == "")||
			    (document.frmDomIscri.txtComuneNascita.value == " ")) {
			    if (document.frmDomIscri.cmbProvinciaNascita.value != ""){
				    //alert("Se viene selezionata la Provincia di Residenza, il campo Comune di Residenza � obbligatorio!")
				    alert("Si ha seleccionado el Departamento de Nacimiento, el campo Municipio de Nacimiento es obligatorio!")
				    document.frmDomIscri.txtComuneNascita.focus() 
				    return false
			    }
		    }
		
		
		}    
 
 //  	if(document.frmDomIscri.cmbStatoEstero.value == ""){	
	// 		if (document.frmDomIscri.cmbProvRes.value == "-"){
	// 			alert("Ingresar el departamento de residencia")
 // 				document.frmDomIscri.cmbProvRes.focus() 
 // 			    return false
	// 		} 
		
	// 		if (document.frmDomIscri.txtComuneRes.value == ""){
	// 			alert("Ingresar el Municipio de residencia")
 // 				document.frmDomIscri.txtComuneRes.focus() 
 // 			    return false
	// 		}
	// } 

	   document.frmDomIscri.cmbCittadinanza1.value=TRIM(document.frmDomIscri.cmbCittadinanza1.value)
		if (document.frmDomIscri.cmbCittadinanza1.value == "")
		{
			alert("EL campo Nacionalidad I es obligatorio!")
			document.frmDomIscri.cmbCittadinanza1.focus() 
			return false
		}
		
		if (document.frmDomIscri.cmbCittadinanza1.value != "")
		{
			if (frmDomIscri.cmbCittadinanza1.value == frmDomIscri.cmbCittadinanza2.value){
				alert("Las nacionalidades ingresadas deben ser distintas.")
				frmDomIscri.cmbCittadinanza1.focus() 
				return false
			}   
		}   
	
		//El pais de residencia 
		paisresidencia = document.frmDomIscri.cmbStatoEstero.value;
		if (( paisresidencia == "")||(paisresidencia == " ")) 
		{
				alert("El Pa�s de Residencia es obligatorio!")
				document.frmDomIscri.cmbStatoEstero.focus() 
				return false
		}
				
		if (paisresidencia == "CO")
		{
    						
		    //Se la Provincia di Residenza � vuota, il Comune � obbligatorio
		    if ((document.frmDomIscri.cmbProvRes.value == "")||
			    (document.frmDomIscri.cmbProvRes.value == " ")) {
				    alert("El Departamento de Residencia es obligatorio!")
				    document.frmDomIscri.cmbProvRes.focus() 
				    return false
		    }		
    		
		    //Se Comune di Residenza � vuoto, il Comune � obbligatorio
		    if ((document.frmDomIscri.txtComuneRes.value == "")||
			    (document.frmDomIscri.txtComuneRes.value == " ")) {
				    alert("El municipio de Residencia es obligatorio!")
				    document.frmDomIscri.txtComuneRes.focus() 
				    return false
    			
		    }	
		    //Se Comune di Residenza � digitato, la Provincia � obbligatoria
		    if (document.frmDomIscri.txtComuneRes.value != ""){
			    if (document.frmDomIscri.cmbProvRes.value == ""){
				    //alert("Se viene selezionato il Comune di Residenza, anche il campo Provincia di Residenza � obbligatorio!")
				    alert("Si ha seleccionado el campo municipio, tambi�n el campo Departamento de Residencia es obligatorio!")
				    document.frmDomIscri.cmbProvRes.focus() 
				    return false
			    }
		    }
	        //Se la Provincia di Residenza � digitata, il Comune � obbligatorio
		    if ((document.frmDomIscri.txtComuneRes.value == "")||
			    (document.frmDomIscri.txtComuneRes.value == " ")) {
			    if (document.frmDomIscri.cmbProvRes.value != ""){
				    //alert("Se viene selezionata la Provincia di Residenza, il campo Comune di Residenza � obbligatorio!")
				    alert("Si ha seleccionado el Departamento de Residencia, el campo Municipio de Residencia es obligatorio!")
				    document.frmDomIscri.txtComuneRes.focus() 
				    return false
			    }
		    }
		}
		
		
	// SM inizio - 11102007
	
	/*	if (document.frmDomIscri.txtComuneRes.value != ""){ 
			if (document.frmDomIscri.cmbProvRes.value != ""){
				if(document.frmDomIscri.cmbStatoEstero.value != ""){
					alert("Indicar el Departamento y Municipio de residencia o unicamente la nacionalidad de no ser uruguayo.")
					document.frmDomIscri.txtComuneNascita.focus() 
					return false
				}
			}	
		}
	*/	
	// SM fine - 11102007
		
		//alert(document.frmDomIscri.cmbTipoDoc.value)//jhamonxxx
		//if (document.frmDomIscri.txtCodFisc.value == ""){
 /* 30/10/2007 DAMIAN QUITA OBLIGATORIEDAD DE CAMPOS Tipo Doc y Nro Doc
                
		        if (document.frmDomIscri.cmbTipoDoc.value == "")
		            {
			        alert("El campo tipo de documento es obligatorio!")
			        document.frmDomIscri.cmbTipoDoc.focus() 
			        return false
		        }
		        if (TRIM(document.frmDomIscri.txtNumDoc.value) == "")
		            {
			        alert("El campo n�mero es obligatorio!")
			        document.frmDomIscri.txtNumDoc.focus() 
			        return false
		        }
 FIN 30/10/2007 DAMIAN OBLIGATORIEDAD DE CAMPOS 		   */
		        // SM inizio 19092007
		        /*
		        else
		        {
					if (document.frmDomIscri.cmbTipoDoc.value == 1)
					{
					
						if (frmDomIscri.txtCodFisc.value.substr(2,1) == "0")
						{			
							if (frmDomIscri.txtCodFisc.value.substr(2,2) == "00")
							{
								if (document.frmDomIscri.txtNumDoc.value != frmDomIscri.txtCodFisc.value.substr(4,6))
								{
									//alert(frmDomIscri.txtCodFisc.value.substr(3,7));
									alert("El n�mero de DNI ingresado no corresponde con el ingresado el en cuil");
									return false;
								} 
							}	
							else
							{
								if (document.frmDomIscri.txtNumDoc.value != frmDomIscri.txtCodFisc.value.substr(3,7))
								{
									//alert(frmDomIscri.txtCodFisc.value.substr(3,7));
									alert("El n�mero de DNI ingresado no corresponde con el ingresado el en cuil");
									return false;
								}
							} 										
						}
						else
						{
							if (document.frmDomIscri.txtNumDoc.value != frmDomIscri.txtCodFisc.value.substr(2,8))
							{
								alert("El n�mero de DNI ingresado no corresponde con el ingresado el en cuil");
								return false;
							}    
						}
					}  
		        }
		        // SM fine 19092007 */  
		        
	
	   //Email
	   if (document.frmDomIscri.txtEmail.value != ""){
		   pippo=ValidateEmail(document.frmDomIscri.txtEmail.value)
		
		   if  (pippo==false)
		   {
			   alert("El formato del email es erroneo.")
			   document.frmDomIscri.txtEmail.focus() 
			   return false
		   }
		}
		//Se Usuario � digitato, la password � obbligatoria
		if (document.frmDomIscri.txtPassword.value != ""){
			if (document.frmDomIscri.txtLogin.value == ""){
				alert("El campo Usuario es obligatorio si se digit� la contrase�a!")
				document.frmDomIscri.txtLogin.focus() 
				return false
			}
		}
				//Se Usuario � digitato, la password � obbligatoria
		if (document.frmDomIscri.txtLogin.value != ""){
			if (document.frmDomIscri.txtPassword.value == ""){
				alert("El campo contrase�a es obligatorio si se digit� el Usuario!")
				document.frmDomIscri.txtPassword.focus() 
				return false
			}
		}

		/********************************************************
		*   @autor: IPTECHONOLOGIES                             +
		********************************************************/  
		// Password Confirmation 
		var password = $("#txtPassword").val()
		var password_confirmation = $("#txtPasswordConfirmation").val()

		if(password != password_confirmation){
			alert("Los campos de Contrase�a y Confirmaci�n de Contrase�a no coinciden!")
			$("#txtPassword").val("")
			$("#txtPasswordConfirmation").val("")
			$("#txtPassword").focus() 
			return false
		}
		/*********************************************************/


		
		
		return true
}
</script>

<!--#include Virtual = "/include/ControlDateVB.asp"-->
<!--#include Virtual = "/util/dbutil.asp"-->
<table border="0" width="520" cellspacing="0" cellpadding="0" height="81">
  <tr>
    <td width="520" background="<%=Session("Progetto")%>/images/titoli/strumenti2b.gif" height="81" valign="bottom" align="right">
      <table border="0" background width="260" height="30" cellspacing="0" cellpadding="0">
        <tr>
          <td width="100%" valign="top" align="right"><b class="tbltext1a">Nuevo 
           Buscador de Empleo </b></td>
        </tr>
      </table>
    </td>
  </tr>
</table>

<form action="UTE_CnfIscrUtente.asp" method="post" name="frmDomIscri" id="frmDomIscri">
<br>
<table border="0" cellpadding="0" cellspacing="0" width="515">
    <tr height="18">
		
      <td class="sfondomenu" width="67%" height="18"> <span class="tbltext0"><b>&nbsp;NUEVO 
        BUSCADOR DE EMPLEO </b></span></td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
		
      <td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">(*) 
        campo obligatorio</td>
		
    </tr>
    <tr height="2">
			<td width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif">	</td>     
	</tr>
    <tr>
       <td class="sfondocomm" width="57%" colspan="3">
       <!--<br>Per accedere ai servizi del Portale � necessario essere iscritti.<br> Compila la Scheda Anagrafica con i tuoi dati personali e premi <b>Invia</b>.-->
       <br>
        Completar los datos personales y presionar el boton Enviar para guardar. 
	  
	  <!--NUEVO AGREGADO POR F.BASANTA -->
      <br>
      Ingresar los datos personales de manera de contar con la informaci�n 
      necesaria para identificar a la persona de la mejor forma posible      
      <img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0" onclick="Javascript:window.open('ayuda_datospersonales.htm','Ayuda','width=500,height=350,Resize=No,Scrollbars=yes')">



        <!--<a href="Javascript:Show_Help('/Pgm/help/Iscr_Utente/UTE_IscrUtente/')" name="prv" onmouseover="javascript:status='' ; return true"> 
        <img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a> -->
      </td> 
    </tr>
    <tr height="2">
			<td width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif">	</td>     
	</tr>   
    
 </table>
 <br>

<table border="0" cellpadding="2" cellspacing="2" width="150" style="WIDTH:220px">
<tr>
          <td align="left" class="tbltipdoc"><strong></strong> <span class="tbltext1"><strong>Tipo Documento*</strong> </span></td>
	      <td align="left" colspan="2">
	            <%  
	                sInt = "DOCST|0|" & date & "| |cmbTipoDoc| and valore in('01','03') ORDER BY DESCRIZIONE"			
				    CreateCombo(sInt)
	                
				%>
		</td>
      </tr>   
  <tr>
    <td align="left" class="tbltext1"><span class="tbltext1"> <strong>N�mero*</strong></span> </td>
    <td align="left" colspan="2"><input class="textblacka" style="TEXT-TRANSFORM: uppercase;" size="14" maxlength="14" name="txtCodFisc">    </td>
  </tr>
   <tr>
		
      <td align="left" nowrap class="tbltext1"> <span class="tbltext1"> 
        <strong>Primer Apellido*</strong></span> </td>
		<td align="left" colspan="2">
				<input class="textblacka" style="TEXT-TRANSFORM: uppercase;"
					  size="35" maxlength="50" name="txtCognome">		</td>
    </tr>
    <tr>
		
      <td align="left" nowrap class="tbltext1"> <span class="tbltext1"> 
        <strong>Segundo Apellido</strong></span> </td>
		<td align="left" colspan="2" width="60%">
			<input class="textblacka" style="TEXT-TRANSFORM: uppercase; WIDTH: 220px;" size="35" maxlength="50" name="txt_secondo_cognome">		</td>
    </tr>
    <tr>
		
      <td align="left" nowrap class="tbltext1"> <span class="tbltext1"> 
        <strong>Primer Nombre*</strong></span> </td>
		<td align="left" colspan="2" width="60%">
			<input class="textblacka" style="TEXT-TRANSFORM: uppercase; WIDTH: 220px;" size="35" maxlength="50" name="txtNome">		</td>
    </tr>
     <tr>
		
      	<td align="left" nowrap class="tbltext1"> <span class="tbltext1"> 
        	<strong>Segundo Nombre</strong></span> 
    	</td>
		<td align="left" colspan="2" width="60%">
			<input class="textblacka" style="TEXT-TRANSFORM: uppercase; WIDTH: 220px;" size="35" maxlength="50" name="txt_secondo_nome">		
		</td>
    </tr>
    <tr>
		
      <td align="left" nowrap class="tbltext1"><strong>Fecha de Nacimiento* 
        </strong>(dd/mm/aaaa) </td>	
		<td align="left" colspan="2" width="60%">
				<input class="textblacka fecha" style="TEXT-TRANSFORM: uppercase; WIDTH: 82px; HEIGHT: 22px" size="50"  maxlength="10" name="txtDataNascita">			
		         <input type="hidden" name="txtOggi" value="<%=ConvDateToString(Date())%>">		</td>
    </tr>
	 <tr>
          <td align="left" colspan="2" class="tbltext1">&nbsp;</td>
	      <td align="left" colspan="2">&nbsp;</td>
      </tr>    
    <tr>
		<td height="2" align="left" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
    </tr>

    <tr>
      <td align="middle" nowrap="nowrap" class="tbltext1"><p align="left"> <strong>Sexo*</strong> <strong>&nbsp;</strong> </p></td>
      <td align="left" colspan="2"><% select case sesso
			case "M"%>
          <select class="textblacka" id="cmbSesso" name="cmbSesso">
            <option value="F" selected="selected">FEMENINO </option>
            <option value="M">MASCULINO </option>
          </select>
        <%
			case "F"%>
          <select class="textblacka" id="cmbSesso" name="cmbSesso">
            <option selected="selected" value="F">FEMENINO </option>
            <option value="M">MASCULINO </option>
          </select>
        <%
			case else%>
          <select class="textblacka" id="cmbSesso" name="cmbSesso">
            <option selected="selected"> </option>
            <option value="F">FEMENINO </option>
            <option value="M">MASCULINO </option>
          </select>
        <%
			end select%>      </td>
    </tr>
    
    <!-- 07/11/2007 DAMIAN QUITA CAMPOS -->
    <!--
    <tr>
      <td align="left" class="tbltext1"><span class="tbltext1"> <strong>Tipo Documento*</strong> </span></td>
      <td align="left" colspan="2"><%
				sInt = "DOCST|0|" & date & "| |cmbTipoDoc|AND valore ='' ORDER BY DESCRIZIONE"			
				CreateCombo(sInt)
				%>      </td>
    </tr>
    <tr>
      <td align="left" class="tbltext1"><b>N&uacute;mero de Documento*</b> </td>
      <td align="left" colspan="2"><input size="34" class="textblacka" maxlength="15" name="txtNumDoc" style="TEXT-TRANSFORM: uppercase;" />      </td>
    </tr> FIN 07/11/2007 DAMIAN -->
    
    <!--<tr>
      <td align="left" nowrap="nowrap" class="tbltext1"><strong>Valido hasta 
        el (dd/mm/aaaa)</strong></td>
      <td align="left" colspan="2"><input class="textblacka" style="TEXT-TRANSFORM: uppercase; WIDTH: 80px; HEIGHT: 22px" size="35" maxlength="10" name="txtValidita" />      </td>
    </tr>-->
	  <tr>
          <td align="left" colspan="2" class="tbltext1">&nbsp;</td>
	      <td align="left" colspan="2">&nbsp;</td>
      </tr>    
	   <tr>
		<td height="2" align="left" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
    </tr>
 
	   <!--jhamon <tr>
         <td height="20" align="left" colspan="3" class="textblack">Para los extranjeros especificar el pa&iacute;s de nacimiento</td>
    </tr> jhamon-->
    <tr>
      <td align="left" class="tbltext1"><span class="tbltext1"> <strong>Pa�s 
        de Nacimiento</strong></span></td>
      <td align="left" colspan="2"><%
					set rsComune = Server.CreateObject("ADODB.Recordset")

					sSQL = "SELECT CODICE as CODCOM, DESCRIZIONE as DESCOM FROM TADES where NOME_TABELLA = 'STATO' ORDER BY DESCRIZIONE"
					'sSQL = "SELECT CODCOM,DESCOM from COMUNE WHERE CODCOM like 'Z%' " &_
					'	"ORDER BY DESCOM"
						
						
    
					rsComune.Open sSQL, CC
			%>
					<SELECT id="cmbNazioneNascita" class="textblacka" name="cmbNazioneNascita" onchange="change_cmbNazioneNascita()">
							<OPTION value=></OPTION>
						<%	
						do while not rsComune.EOF

							value_codcom =  rsComune("CODCOM")
							descom = rsComune("DESCOM") %>
							<OPTION  value = "<%=value_codcom%>"> <%=descom%></OPTION>
							<% rsComune.MoveNext 
						loop %>
					</SELECT>
	<%
					rsComune.Close	
				%>      </td>
    </tr>
    <!--jhamon <tr>
		
      <td height="20" align="left" colspan="3" class="textblack">Para los nacidos en Uruguay ingresar 
      el Departamento y la Localidad de nacimiento</td>
    </tr> jhamon-->
    <tr>
        
      <td align="left" class="tbltext1"> <span class="tbltext1"> <strong>Departamento 
        de Nacimiento</strong></span> </td>
	    <td align="left" colspan="2" width="60%">
				<%
				sInt = "PROV|0|" & date & "| |cmbProvinciaNascita' onchange='PulisciCom()|ORDER BY DESCRIZIONE"			
				CreateCombo(sInt)
				%>         </td>
    </tr>
     
     
     <tr>		
			
      <td align="left" nowrap class="tbltext1"> <b>Municipio de Nacimiento</b></td>
			<td nowrap>
				<span class="tbltext">
				<input type="text" name="txtComuneNascita" id="txtComuneNascita" style="TEXT-TRANSFORM: uppercase;" class="textblacka" size="35" readonly value="<%=DescrComuneResid%>">
				<input type="hidden" name="txtComune" value="<%=COM_RES%>">
				
<%
				NomeForm="frmDomIscri"
				CodiceProvincia="cmbProvinciaNascita"
				NomeComune="txtComuneNascita"
				CodiceComune="txtComune"
				Cap="NO"
%>
				<a href="Javascript:SelComune('<%=NomeForm%>','<%=CodiceProvincia%>','<%=NomeComune%>','<%=CodiceComune%>','<%=Cap%>')" ID="imgPunto1" name="imgPunto1" onmouseover="javascript:window.status='';return true"><img border="0" src="<%=Session("Progetto")%>/images/bullet1.gif"></a>				</span>			</td>
		</tr>	
<!-- SM INIZIO 13092007
<tr>
      <td align="left" nowrap class="tbltext1"> <b>Departamento de Nacimiento</b></td>
			<td nowrap>
				<span class="tbltext">
				<input type="text" name="txtDeptoNacimiento" style="TEXT-TRANSFORM: uppercase;" class="textblacka" size="35" readonly value="">
				<input type="hidden" name="txtDepto" value="">
				
<%		

		NomeForm="frmDomIscri"
		CodiceProvincia="cmbProvinciaNascita"
		NomeDepto="txtDeptoNacimiento"
		CodiceDepto="txtDepto"
		CodigoDepto = ""
		CasillaLocNac ="txtLocNacimiento"
		CasillaCodLocNac ="txtLoc"
		
		%>
				<a href="Javascript:SelDepto('<%=NomeForm%>','<%=CodiceProvincia%>','<%=NomeDepto%>','<%=CodiceDepto%>','<%=CodigoDepto%>','<%=CasillaLocNac%>','<%=CasillaCodLocNac%>')" ID="imgPunto1" name="imgPunto1" onmouseover="javascript:window.status='';return true"><img border="0" src="<%=Session("Progetto")%>/images/bullet1.gif"></a>				</span>			</td>

		</tr>	
		
<tr>
      <td align="left" nowrap class="tbltext1"> <b>Localidad de Nacimiento</b></td>
			<td nowrap>
				<span class="tbltext">
				<input type="text" name="txtLocNacimiento" style="TEXT-TRANSFORM: uppercase;" class="textblacka" size="35" readonly value="">
				<input type="hidden" name="txtLoc" value="">
				
<%
				NomeForm="frmDomIscri"
				CodiceProvincia="cmbProvinciaNascita"
				NomeLoc="txtLocNacimiento"
				CodiceLoc="txtLoc"
				CodigoDepto="txtDepto"
%>
				<a href="Javascript:SelLoc('<%=NomeForm%>','<%=CodiceProvincia%>','<%=NomeLoc%>','<%=CodiceLoc%>','<%=CodigoDepto%>')" ID="imgPunto1" name="imgPunto1" onmouseover="javascript:window.status='';return true"><img border="0" src="<%=Session("Progetto")%>/images/bullet1.gif"></a>				</span>			</td>
		</tr>	
      
      
      <tr>
          <td align="left" colspan="2" class="tbltext1"></td>
	      <td align="left" colspan="2">&nbsp;</td>
      </tr>
   SM FINE 13092007-->
    <tr>
		<td height="2" align="left" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
    </tr>
 
     <tr>
        
      <td align="left" class="tbltext1"> <span class="tbltext1"> <strong>Nacionalidad 
        I*</strong> </span></td>
        <td align="left" colspan="2" width="60%">
			<%	sInt = "STATO|0|" & date & "| |cmbCittadinanza1|ORDER BY DESCRIZIONE"
				CreateCombo(sInt)	%>         </td>
    </tr>
    <tr>
        
      <td align="left" class="tbltext1"> <span class="tbltext1"> <strong>Nacionalidad 
        II</strong> </span></td>
        <td align="left" colspan="2" width="60%">
			<%  sInt = "STATO|0|" & date & "| |cmbCittadinanza2|ORDER BY DESCRIZIONE"
				CreateCombo(sInt) %>         </td>
    </tr>
	    <tr>
          <td align="left" colspan="2" class="tbltext1">&nbsp;</td>
	      <td align="left" colspan="2">&nbsp;</td>
      </tr>    
    <tr>
    <td height="2" align="middle" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
    </tr>

    <!--jhamon <tr>
      <td height="9" align="left" colspan="3" class="textblack">Seleccione el pa�s extranjero donde reside</td>
    </tr> jhamon-->
    <tr>
      <td align="left" class="tbltext1"><span class="tbltext1"> <strong>Pa�s de Residencia*</strong></span></td>
      <td align="left" colspan="2"><%
					set rsComune = Server.CreateObject("ADODB.Recordset")

					'sSQL = "SELECT CODCOM,DESCOM from COMUNE WHERE CODCOM like 'Z%' " &_
					'	"ORDER BY DESCOM"
					sSQL = "SELECT CODICE as CODCOM, DESCRIZIONE as DESCOM FROM TADES where NOME_TABELLA = 'STATO' ORDER BY DESCRIZIONE"

					rsComune.Open sSQL, CC
			
					Response.Write "<SELECT class=textblacka ID=cmbStatoEstero name=cmbStatoEstero onchange='change_cmbStatoEstero()'><OPTION value=></OPTION>"

					do while not rsComune.EOF 
						Response.Write "<OPTION "

						Response.write "value ='" & rsComune("CODCOM") & _
							"'> " & rsComune("DESCOM")  & "</OPTION>"
							rsComune.MoveNext 
					loop
	
					Response.Write "</SELECT>"

					rsComune.Close	
				%>      </td>
    </tr>
    <tr>
		
      <!--<td height="20" align="left" colspan="3" class="textblack"> Si es residente 
        en Uruguay seleccione el departamento y la municipio de residencia</td>
    </tr>-->
	<tr>
        
      <td align="left" class="tbltext1"> <span class="tbltext1"> <strong>Departamento 
        de Residencia*</strong></span></td>
        <td align="left" colspan="2" width="60%">
				<%
				sInt = "PROV|0|" & date & "| |cmbProvRes' onchange='PulisciRes()|ORDER BY DESCRIZIONE"			
				CreateCombo(sInt)
				%>         </td>
    </tr>
      <tr>		
			
      <td align="left" nowrap class="tbltext1"> <b>Municipio de Residencia*</b> </td>
			<td nowrap>
		
				<span class="tbltext">
				<input type="text" name="txtComuneRes" id="txtComuneRes" style="TEXT-TRANSFORM: uppercase;" class="textblacka" size="35" readonly value="<%=DescrComuneResid%>">
				<input type="hidden" id="txtComRes" name="txtComRes" value="<%=COM_RES%>">
				
<%
				NomeForm="frmDomIscri"
				CodiceProvincia="cmbProvRes"
				NomeComune="txtComuneRes"
				CodiceComune="txtComRes"
				Cap="NO"
%>
				<a href="Javascript:SelComune('<%=NomeForm%>','<%=CodiceProvincia%>','<%=NomeComune%>','<%=CodiceComune%>','<%=Cap%>')" ID="imgPunto2" name="imgPunto1" onmouseover="javascript:window.status='';return true"><img border="0" src="<%=Session("Progetto")%>/images/bullet1.gif"></a>				</span>			</td>
		</tr>
<!-- SM INIZIO 13092007
<tr>
      <td align="left" nowrap class="tbltext1"> <b>Departamento de Residencia</b></td>
			<td nowrap>
				<span class="tbltext">
				<input type="text" name="txtDtoResidencia" style="TEXT-TRANSFORM: uppercase;" class="textblacka" size="35" readonly value="">
				<input type="hidden" name="txtDto" value="">
				
<%		

		NomeForm="frmDomIscri"
		CodiceProvincia="cmbProvRes"
		NomeDepto="txtDtoResidencia"
		CodiceDepto="txtDto"
		CodigoDepto = ""
		CasillaLocRes ="txtLcResidencia"
		CasillaCodLocRes ="txtLc"
		%>
				<a href="Javascript:SelDepto('<%=NomeForm%>','<%=CodiceProvincia%>','<%=NomeDepto%>','<%=CodiceDepto%>','<%=CodigoDepto%>','<%=CasillaLocRes%>','<%=CasillaCodLocRes%>')" ID="imgPunto1" name="imgPunto1" onmouseover="javascript:window.status='';return true"><img border="0" src="<%=Session("Progetto")%>/images/bullet1.gif"></a>				</span>			</td>

		</tr>	
		
<tr>
      <td align="left" nowrap class="tbltext1"> <b>Localidad de Residencia</b></td>
			<td nowrap>
				<span class="tbltext">
				<input type="text" name="txtLcResidencia" style="TEXT-TRANSFORM: uppercase;" class="textblacka" size="35" readonly value="">
				<input type="hidden" name="txtLc" value="">
				
<%
				NomeForm="frmDomIscri"
				CodiceProvincia="cmbProvRes"
				NomeLoc="txtLcResidencia"
				CodiceLoc="txtLc"
				CodigoDepto="txtDto"
%>
				<a href="Javascript:SelLoc('<%=NomeForm%>','<%=CodiceProvincia%>','<%=NomeLoc%>','<%=CodiceLoc%>','<%=CodigoDepto%>')" ID="imgPunto1" name="imgPunto1" onmouseover="javascript:window.status='';return true"><img border="0" src="<%=Session("Progetto")%>/images/bullet1.gif"></a>				</span>			</td>
		</tr>	
	  
SM FINE 13092007 -->	  
	  
	  
	  <tr>
          <td align="left" colspan="2" class="tbltext1">&nbsp;</td>
	      <td align="left" colspan="2">&nbsp;</td>
      </tr>    
	  <tr>
		<td height="2" align="left" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
	  </tr>

	  <tr>
        <td align="left" class="tbltext1"><span class="tbltext1"> <strong>Correo Electr&oacute;nico</strong></span> </td>
	    <td align="left" colspan="2"><input class="textblacka" style="WIDTH:167px; " size="50" maxlength="100" name="txtEmail" />        </td>
    </tr>
    
    <tr>
          <td align="left" class="tbltext1"><p align="left"><span class="tbltext1"><strong>&nbsp;Estado Civil</strong></span></p></td>
		  <td align="left" colspan="2"><p align="left">
              <%
				sInt = "STCIV|0|" & date & "|01|cmbStatoCiv|ORDER BY DESCRIZIONE"			
				CreateCombo(sInt)
   			  %>
          </p></td>
	  </tr>
	  <tr>
		<td height="2" align="left" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
	  </tr>
	  <tr>
		<td align="left" colspan="3" class="sfondocomm"><span class="tbltext1">Esta secci&oacute;n permite entregar credenciales (Usuario y Contrase�a) al buscador de empleo, para facilitar su acceso al sistema v&iacute;a Web</span> </td>	 
	  </tr>
	  <tr>
		<td align="left" colspan="3" class="tbltext1"><span class="tbltext1"> <strong>&nbsp</strong></span> </td>	 
	  </tr>
	  <tr>
		<td align="left" class="tbltext1"><span class="tbltext1"> <strong>Usuario</strong></span> </td>
		<td align="left" colspan="2">
				<input class="textblacka" style="TEXT-TRANSFORM: uppercase; WIDTH: 167px; size='35' maxlength='50'" name="txtLogin">
		</td>
    </tr>
    <tr>
		<td align="left" class="tbltext1"><span class="tbltext1"> <strong>Contrase�a</strong></span> </td>
		<td align="left" colspan="2">
				<input class="textblacka" style="TEXT-TRANSFORM: uppercase; WIDTH: 220px;
					 HEIGHT: 22px" size="35" maxlength="50" id="txtPassword" name="txtPassword" type="password">
		</td>
    </tr>
        <tr>
		<td align="left" class="tbltext1"><span class="tbltext1"> <strong>Confirmar Contrase�a</strong></span> </td>
		<td align="left" colspan="2">
				<input class="textblacka" style="TEXT-TRANSFORM: uppercase; WIDTH: 220px;
					 HEIGHT: 22px" size="35" maxlength="50" id="txtPasswordConfirmation" name="txtPasswordConfirmation" type="password">
		</td>
    </tr>
</table>
<p>
<table border="0" cellpadding="1" cellspacing="1" width="370" align="center">
	 <tr>
        <td align="middle" colspan="2">
        <input type="image" name="Invia" src="<%=Session("Progetto")%>/images/conferma.gif" border="0" value="Conferma" onclick="return ControllaDati(this)"> 
        </td>
     </tr>
</table>
</form>
<!--#include virtual="/strutt_coda2.asp"-->
<!--#include Virtual = "/include/CloseConn.asp"-->
