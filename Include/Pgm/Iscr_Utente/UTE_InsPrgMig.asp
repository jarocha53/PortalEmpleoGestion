<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include Virtual = "/strutt_testa2.asp"-->
<!--#include Virtual = "/include/OpenConn.asp"-->
<!--#include Virtual = "/include/ControlDateVB.asp"-->
<!--#include Virtual = "/include/DecCod.asp"-->

<script language="javascript" src="ControlliBac.js">
</script>

<script LANGUAGE="Javascript">
//include del file per fare i controlli sulla validit� delle date
<!--#include Virtual = "/Include/ControlDate.inc"-->

//include del file per fare i controlli sulla numericit� dei campi
<!--#include Virtual = "/Include/ControlNum.inc"-->

//include del file per fare i controlli sulle stringhe
<!--#include Virtual = "/Include/ControlString.inc"-->
<!--#include Virtual = "/Include/help.inc"-->

function ControllaDati()
{
	if (frmInsPrgMig.cmbMingr.value=='')
	{
		alert('El campo de disponibilidad en Italia es obligatorio')
		frmInsPrgMig.cmbMingr.focus()
		return false;
	}
	
	if (frmInsPrgMig.cmbRegione.value=='')
	{
		alert('La regi�n de preferencia es obligatoria')
		frmInsPrgMig.cmbRegione.focus()
		return false;
	}
	
	if (frmInsPrgMig.cmbScelte.value=='')
	{
		alert('El motivo de la elecci�n precedente es obligatorio')
		frmInsPrgMig.cmbScelte.focus()
		return false;
	}
	
	if (frmInsPrgMig.cmbColog.value=='')
	{
		alert('Il campo Condizione abitativa � obbligatorio')
		frmInsPrgMig.cmbColog.focus()
		return false;
	}
}
</script>

<%

dim sIDPersona
'sIdpers=Request.QueryString("Idpers")
sIdpers=Request("Idpers")

IF sIdpers ="" then
   sIdpers=Session("creator")
end if

sSQL="SELECT FL_GRDPAR,COD_MINGR,COD_REGIO," & _
	 "COD_MTSEL,COD_COLOG,DT_TMST " & _
	 "FROM PERS_PRGMIGRATORIO WHERE ID_PERSONA=" & sIdpers
	 
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
Set rsPers = CC.Execute(sSQL)

if not rsPers.eof then
	FL_GRDPAR=rsPers("FL_GRDPAR")
	COD_MINGR=rsPers("COD_MINGR")
	COD_REGIO=rsPers("COD_REGIO")
	COD_MTSEL=rsPers("COD_MTSEL")
	COD_COLOG=rsPers("COD_COLOG")
	DT_TMST=rsPers("DT_TMST")
	
	sData=ConvDateToDb(now())
	REGIONE=DecCodVal("REGIO",0,vData,COD_REGIO,0)

end if
%>

<br>
<!--#include file="menu.asp"-->
	<br>
	<br>
	
	<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
		<tr height="18">
			<td class="sfondomenu" width="67%" height="18"><span class="tbltext0"><b>&nbsp;PROGETTO MIGRATORIO</b></span></td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
			<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">(*) campi obbligatori</td>
		</tr>
	</table>
	<!-- Commento -->
	<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
		<tr>
			<td align="left" class="sfondocomm">
				Seleccione una palabra del men� para modificar los datos relativos. <br>
				Presione <b>Enviar</b> para guardar la modificaci�n.
				<a href="Javascript:Show_Help('/Pgm/help/Iscr_Utente/UTE_InsPrgMig')" name onmouseover="javascript:status='' ; return true">
				<img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
			</td>
		</tr>
		<tr height="2">
			<td class="sfondocomm" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>
	
<%

dim rsPers

sSQL = "SELECT ID_PERSONA,COGNOME,NOME,DT_NASC,COM_NASC,PRV_NASC,STAT_NASC,SESSO,POS_MIL,COD_FISC,STAT_CIV,STAT_CIT,STAT_CIT2,IND_RES,FRAZIONE_RES,COM_RES,PRV_RES,CAP_RES,NUM_TEL,E_MAIL,DT_TMST from PERSONA WHERE ID_PERSONA=" & clng(sIdpers) 

'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
set rsPers = CC.Execute(sSQL)

if rsPers.EOF then
%>
	
	<table border="0" cellspacing="2" cellpadding="1" width="500" align="center">
		<tr align="middle">
			<td class="tbltext3">
				<b>No ha sido posible acceder a la informaci�n de la persona.</b>
			</td>
		</tr>
	</table>
	
<%
else
   	sIDPersona = clng(rsPers("ID_PERSONA"))
	'Response.Write sIDPersona
%>
   	<form method="post" name="frmInsPrgMig" onsubmit="return ControllaDati()" action="UTE_CnfPrgMig.asp">
	<input type="hidden" name="txtPersona" value="<%=sIdpers%>">
	<input type="hidden" name="txtDT_TMST" value="<%=DT_TMST%>">
	<table WIDTH="98%" ALIGN="center" BORDER="0" CELLSPACING="5" CELLPADDING="1">
		<tr>
			<td height="20" align="left" colspan="4" class="textblack">
				La legislazione Italiana (Legge 189/02 art.17) riserva delle quote di ingresso
				ai figli,<br>nipoti, a pronipoti di Italiani residenti in paesi non comunitari.
			</td>
		</tr>
		<tr>
			<td align="middle" colspan="2" nowrap class="tbltext1">
				<p align="left">
				<b>Hai questo grado di parentela?*</b>
				</p>
			</td>
			<td align="left" colspan="2" width="60%">
<%			select case FL_GRDPAR
				case "N"
%>
				<input type="radio" name="optLegge" value="S"><span CLASS="TBLTEXT">SI</span>				
				<input type="radio" name="optLegge" checked value="N"><span CLASS="TBLTEXT">NO</span>				
<%
				case "S"
%>
			    <input type="radio" name="optLegge" checked value="S"><span CLASS="TBLTEXT">SI</span>
				<input type="radio" name="optLegge" value="N"><span CLASS="TBLTEXT">NO</span>
<%				
				case else
%>
			    <input type="radio" name="optLegge" value="S"><span CLASS="TBLTEXT">SI</span>
				<input type="radio" name="optLegge" checked value="N"><span CLASS="TBLTEXT">NO</span>
<%
			end select
%>    			
			</td>
		</tr>
		<tr>
			<td width="100%" height="2" colspan="4" background="<%=Session("Progetto")%>/images/separazione.gif">	
			</td>     
		</tr>
		<tr>
			<td align="middle" colspan="2" nowrap class="tbltext1">
				<p align="left">
				<b>La disponibilidad en Argentina es para*</b>
				</p>
			</td>
			<td align="left" colspan="2" width="60%">
<%
				sInt = "MINGR|0|" & date & "|" & COD_MINGR & "|cmbMingr|ORDER BY DESCRIZIONE"			
				CreateCombo(sInt)
%>
			</td>
		</tr>
		<tr>
			<td align="middle" colspan="2" nowrap class="tbltext1">
				<p align="left">
				<b>Declara la preferencia <br>a un trabajo en la regi�n*</b>
				</p>
			</td>			
			<td align="left" colspan="2" width="60%">
				<!--input type="hidden" name="txtRegioneHid" value="< %=COD_REGIO% >">				<input type="text" name="txtRegione" class="textblacka" readonly size="30" value="< %=REGIONE% >">				<a href="Javascript:SelRegio()" ID="imgPunto1" name="imgPunto1">				<img border="0" src="< %=Session("Progetto")% >/images/bullet1.gif"></a-->				
<%
				sInt = "REGIO|0|" & date & "|" & COD_REGIO & "|cmbRegione|ORDER BY DESCRIZIONE"			
				CreateCombo(sInt)
%>
			</td>
		</tr>
		<tr>
			<td align="middle" colspan="2" nowrap class="tbltext1">
				<p align="left">
				<b>Indicar el motivo <br>de la elecci�n precedente*</b>
				</p>
			</td>
			<td align="left" colspan="2" width="60%">
<%
				sInt = "MTSEL|0|" & date & "|" & COD_MTSEL & "|cmbScelte|ORDER BY DESCRIZIONE"			
				CreateCombo(sInt)
%>
			</td>
		</tr>
		<tr>
			<td align="middle" colspan="2" nowrap class="tbltext1">
				<p align="left">
				<b>Indica la condizione abitativa*</b>
				</p>
			</td>
			<td align="left" colspan="2" width="60%">
<%
				sInt = "COLOG|0|" & date & "|" & COD_COLOG & "|cmbColog|ORDER BY CODICE"			
				CreateCombo(sInt)
%>
			</td>
		</tr>
	</table>

	<br>
		
	<table border="0" cellpadding="1" cellspacing="1" width="500">
		<tr>
	        <td align="middle" colspan="2">
				<input type="image" src="<%=Session("Progetto")%>/images/conferma.gif" border="0" value="Modifica" name="image1">			
	        </td>
	    </tr>
	</table>
	</form>
	
<%
end if
rsPers.Close
set rsPers = nothing
%>
<!--#include virtual = "/include/CloseConn.asp"-->
<!--#include Virtual = "/strutt_coda2.asp"-->
