<!--#include Virtual = "/util/portallib.asp"-->
<!--#include Virtual = "/strutt_testa2.asp"-->
<!--#include virtual = "/include/SysFunction.asp"-->
<!--#include virtual = "/include/OpenConn.asp"-->
<!--#include virtual = "/util/dbUtil.asp"-->
<!--#include virtual = "/include/ControlDateVB.asp"-->
<!--#include Virtual = "include/DecCod.asp"-->

<script LANGUAGE="Javascript">

//include del file per fare i controlli sulla validit� delle date
<!--#include Virtual = "/Include/ControlDate.inc"-->

//include del file per fare i controlli sulla numericit� dei campi
<!--#include Virtual = "/Include/ControlNum.inc"-->

//include del file per fare i controlli sulla stringa
<!--#include Virtual = "/Include/ControlString.inc"-->
<!--#include Virtual = "/Include/help.inc"-->


/*
function Chiudi(){	
	if (ControllaDati() == true){
		if (confirm("Sei sicuro di voler chiudere questa Categoria Protetta alla data di oggi?")){
			condizione = "si";
			document.frmCatProt.txtChiudi.value = condizione;
			document.frmCatProt.submit()
		}		
	}
}			
*/


//Controllo Validit� dei campi	
function ControllaDati(){

	//Cognome
	frmCancella.txtCognome.value = TRIM(frmCancella.txtCognome.value)
	if (frmCancella.txtCognome.value == ""){
		alert("El campo apellido es obligatorio!")
		frmCancella.txtCognome.focus() 
		return false
	}
	
	sCognome = ValidateInputStringWithOutNumber(frmCancella.txtCognome.value)
	if (sCognome == false){
		alert("Apellido erroneo.")
		frmCancella.txtCognome.focus() 
		return false
	}

	//Nome
	frmCancella.txtNome.value = TRIM(frmCancella.txtNome.value)
	if (frmCancella.txtNome.value == ""){
		alert("El campo nombre es obligatorio!")
		frmCancella.txtNome.focus() 
		return false
	}
	
	sNome = ValidateInputStringWithOutNumber(frmCancella.txtNome.value)
	if (sNome == false){
		alert("Nombre erroneo.")
		frmCancella.txtNome.focus() 
		return false
	}
		
	//Login
	frmCancella.txtLogin.value = TRIM(frmCancella.txtLogin.value)
	if (frmCancella.txtLogin.value == ""){
		alert("El campo usuario es obligatorio!")
		frmCancella.txtLogin.focus() 
		return false
	}
	
	/*
	sLogin = ValidateInputStringWithOutNumber(frmCancella.txtLogin.value)
	if (sLogin==false){
		alert("Nome formalmente errato.")
		frmCancella.txtLogin.focus() 
		return false
	}	
	*/
	
	//Password
	frmCancella.txtPassword.value = TRIM(frmCancella.txtPassword.value)
	if (frmCancella.txtPassword.value == ""){
		alert("El campo contrase�a es obligatorio!")
		frmCancella.txtPassword.focus() 
		return false
	}	
		
	return true
	
}

</script>

<div align="center">
<%

dim sIDPersona

sIdpers=Request("Idpers")

if sIdpers = "" then
   sIdpers = Session("creator")
end if

'Response.Write sIdPers
'Response.End 

%>
<table border="0" width="520" cellspacing="0" cellpadding="0" height="80">
   <tr>
		<td width="520" background="<%=Session("Progetto")%>/images/titoli/strumenti2g.gif" height="70" align="right" valign="bottom">
			<table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
  				<tr>
					<td width="100%" valign="baseline" align="right"><span class="tbltext1a"><b>Cancelar usuario&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></span></td>
				</tr>
			</table>
		</td>
   </tr>
</table>

	<br>
<br>
<table cellpadding="0" cellspacing="0" width="488" border="0">
	<tr height="18">
		<td class="sfondomenu" height="18" width="45%">
			<span class="tbltext0"><b>&nbsp;Cancelaci�n de Usuario</b></span>
		</td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
		<td valign="middle" align="right" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1">(*) campo obligatorio</td>
	</tr>
	<tr height="2">
		<td width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif"></td>     
	</tr>
	<tr width="371" class="SFONDOCOMMAZ">
		<td colspan="3">
			Para cancelar el usuario confirme la solicitud <b>Apellido</b>, <b>NOmbre</b>, <b>Usuario</b> y <b>Contrase�a</b>.
			<a href="Javascript:Show_Help('/Pgm/help/Iscr_Utente/UTE_Cancella')" name="prov3" onmouseover="javascript:window.status='' ; return true">
			  <img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0">
			</a>
		</td>
	</tr>
	<tr height="2">
		<td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif">
		</td>
	</tr>
</table>
<br>

<form method="post" name="frmCancella" onsubmit="return ControllaDati(this)" action="UTE_CnfCancella.asp">
	<input type="hidden" id="text1" name="txtIdPers" value="<%=sIdpers%>">
	<table border="0" cellpadding="0" cellspacing="1" width="500">
		<tr>
			<td align="middle" colspan="2" nowrap class="tbltext1">
				<p align="left">
					<strong>&nbsp;</strong>
					<strong>Apellido *</strong>
					<strong>&nbsp;</strong>
				</p>
			</td>
			<td align="left" colspan="2" width="60%">
				<input style="TEXT-TRANSFORM: uppercase;" class="textblacka" type="text" maxlength="50" name="txtCognome" value size="35">						
			</td>
	    </tr>
	    <tr>
			<td align="middle" colspan="2" nowrap class="tbltext1">
				<p align="left">
					<strong>&nbsp;</strong>
					<strong>Nombre *</strong>
					<strong>&nbsp;</strong>
				</p>
			</td>
			<td align="left" colspan="2" width="60%">
				<input style="TEXT-TRANSFORM: uppercase" class="textblacka" type="text" maxlength="50" name="txtNome" value size="35">						
			</td>
	   </tr>
	   <tr>
			<td align="middle" colspan="2" nowrap class="tbltext1">
				<p align="left">
					<strong>&nbsp;</strong>
					<strong>Usuario *</strong>
					<strong>&nbsp;</strong>
				</p>
			</td>	
			<td align="left" colspan="2" width="60%">
				<input type="text" style="TEXT-TRANSFORM: uppercase" class="textblacka" name="txtLogin" value size="30">
			</td>
	   </tr>
	   <tr>
			<td align="middle" colspan="2" nowrap class="tbltext1">
				<p align="left">
					<strong>&nbsp;</strong>
					<strong>Contrase�a *</strong>
					<strong>&nbsp;</strong>
				</p>
			</td>	
	  		<td align="left" colspan="2" width="60%">
	  			<span class="tbltext">
				<input type="text" style="TEXT-TRANSFORM: uppercase" class="textblacka" name="txtPassword" value size="30">
				</span>
			</td>
	   </tr>
	</table>
	<br>
	<br>
	<table border="0" cellpadding="1" cellspacing="1" width="500">
		<tr>
			<td align="middle" colspan="2">
				<input type="image" src="<%=Session("Progetto")%>/images/conferma.gif" border="0" value="Modifica" onclick="return ControllaDati(this)" id="image1" name="image1">
			</td>
		</tr>
	</table>	
</form>	

</div>	
<!--#include Virtual = "/include/CloseConn.asp"-->	
<!--#include Virtual="/strutt_Coda2.asp"-->
