<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include Virtual = "/strutt_testa2.asp"-->
<!--#include Virtual = "/include/OpenConn.asp"-->
<!--#include Virtual = "/include/DecCod.asp"-->
<!--#include virtual = "/include/ControlDateVb.asp"-->
<!--#include virtual = "/include/DecComun.asp"-->



<script LANGUAGE="Javascript">
//include del file per fare i controlli sulla validit� dei comuni
<!--#include virtual = "/include/SelComune.js"-->


//include del file per fare i controlli sulla validit� delle date
<!--#include Virtual = "/Include/ControlDate.inc"-->

//include del file per fare i controlli sulla numericit� dei campi
<!--#include Virtual = "/Include/ControlNum.inc"-->

//include del file per fare i controlli sulla stringa
<!--#include Virtual = "/Include/ControlString.inc"-->
<!--#include Virtual = "/Include/help.inc"-->

function PulisciComIng()
{
	document.frmImmigrati.txtComuneIngresso.value = ""
	document.frmImmigrati.txtComuneIngrHid.value = ""
}

function PulisciComInt()
{
	document.frmImmigrati.txtCmnInt.value = ""
	document.frmImmigrati.txtComuneIntHid.value = ""
}

//Funzione per i controlli dei campi da inserire 
	function ControllaDati()
{
		
		//Data di ingresso
		if (document.frmImmigrati.txtDTingresso.value == "")
			
		{
			alert("Data di Ingresso obbligatoria")
			document.frmImmigrati.txtDTingresso.focus() 
			return false
		}
		DataIngresso = frmImmigrati.txtDTingresso.value
		DataOdierna=frmImmigrati.txtOggi.value
		//Controllo che la data di ingresso non sia magiore della data di nascita
		DataNascita=frmImmigrati.txtdtn.value
		DataIngresso = document.frmImmigrati.txtDTingresso.value
		if (ValidateRangeDate(DataNascita,DataIngresso)==false)
		{
			alert("La data di Ingresso deve essere superiore alla data di nascita!")
			frmImmigrati.txtDTingresso.focus()
		    return false
		}
		//Controllo della validit� della Data di Ingresso
		DataIngresso = document.frmImmigrati.txtDTingresso.value
		if (!ValidateInputDate(DataIngresso))
		{
			document.frmImmigrati.txtDTingresso.focus() 
			return false
		}
		
		
		if (ValidateRangeDate(DataOdierna,DataIngresso)==true)
		{
			alert("La data di Ingresso deve essere inferiore alla data odierna!")
			frmImmigrati.txtDTingresso.focus()
		    return false
		}
	
		//Comune di Ingresso e Provincia obbligatori
		document.frmImmigrati.txtComuneIngresso.value=TRIM(document.frmImmigrati.txtComuneIngresso.value)
		if (document.frmImmigrati.txtComuneIngresso.value == "")
		  {
			alert("Comune di Ingresso obbligatorio")
			document.frmImmigrati.txtComuneIngresso.focus() 
			return false
		  }
		// Provincia di Ingresso
		if(document.frmImmigrati.cmbProvinciaIngresso.value == "")
		 {	
			alert("Departamento di Ingresso obbligatoria")
			document.frmImmigrati.txtComuneIngresso.focus() 
			return false
		  }
		
		//Motivo di Ingresso
		if (document.frmImmigrati.cmbMotivo.value=="")
		{
		    alert("Indicare il Motivo di ingresso")
			document.frmImmigrati.cmbMotivo.focus() 
			return false
		  }
		
		//Se � digitato lo Stato della U.E campo Mesi Obligatorio
		if((document.frmImmigrati.cmbStato_UE.value !="")&&
		  (document.frmImmigrati.txtMesi.value ==""))
		{
		  alert("Inserire il numero di Mesi trascorsi nello Stato Europeo da lei selezionato")   	 	
		  return false
		}
		if((document.frmImmigrati.cmbStato_UE.value =="")&&
		  (document.frmImmigrati.txtMesi.value !=""))
		{
		  alert("Indicare lo Stato dell'U.E.")   	 	
		  return false
		}  
		//Mesi deve essere numerico
		 
		 if(document.frmImmigrati.txtMesi.value!="")
		 {
		 if(!IsNum(document.frmImmigrati.txtMesi.value))
		 {
		  alert("Inserire solo numeri per indicare i Mesi")  
		  document.frmImmigrati.txtMesi.focus() 	 	
		  return false
		 }
	     }		 	 
		//CONTROLLO Coerenza dati
		
		if ((document.frmImmigrati.fl2[1].checked == true)&&
			(document.frmImmigrati.fl3[1].checked == true))
		{
		alert("Le affermazioni riguardanti,la volonta di cambiare Departamento o Regione,da lei rilasciate sono in conflitto ")
			return false
		}	
		if ((document.frmImmigrati.fl2[0].checked == true)&&
			(document.frmImmigrati.fl3[0].checked == true))
		{
		alert("Le affermazioni riguardanti,la volonta di cambiare Departamento o Regione,da lei rilasciate sono in conflitto")
			return false
		}
		
		if ((document.frmImmigrati.fl1[0].checked == true)&&
			(document.frmImmigrati.fl3[0].checked == true))
		{
		alert("Le affermazioni riguardanti,la volonta di cambiare Departamento o Regione,da lei rilasciate sono in conflitto")
			return false
		}
		//Se il check altra provincia � N pulisco se pieni, i campi di Comune E Provincia 
		if ((document.frmImmigrati.fl3[0].checked == false)&&
			(document.frmImmigrati.txtCmnInt.value != "")&&
			(document.frmImmigrati.cmbPrvInter.value != ""))
		{
			alert("Dichiarando che non intendi cambiare la Regione,togliere il riferimento di Comune e Departamento di interesse") 
			document.frmImmigrati.cmbPrvInter.focus();
				return false
		}		
		//Se il check altra provincia � S,Comune e Provincia obligatoria
		if ((document.frmImmigrati.fl3[0].checked == true)&&
			(document.frmImmigrati.txtCmnInt.value == ""))
		
		{
			alert("Se si desidera cambiare Departamento,indicare la Regione e Departamento desiderata ")
			return false
		}	
		//Se Comune di Interesse � digitato, la Provincia � obbligatoria
		document.frmImmigrati.txtCmnInt.value=TRIM(document.frmImmigrati.txtCmnInt.value) 
		if (document.frmImmigrati.txtCmnInt.value != "")
		{
			if (document.frmImmigrati.cmbPrvInter.value == "")
			{
				alert("Departamento di Interesse obbligatoria")
				document.frmImmigrati.cmbPrvInter.focus() 
				return false
			}
		}
	    //Se la Provincia di Interesse � digitata, il Comune � obbligatorio
		if ((document.frmImmigrati.cmbPrvInter.value != "")&&
            (document.frmImmigrati.txtCmnInt.value == ""))  		
			{
				alert("Comune di Interesse obbligatorio")
				document.frmImmigrati.txtCmnInt.focus() 
				return false
			}
		
	 
		
		
			  //Rilasciato da
			  if(frmImmigrati.cmbEnteRilascio.value =="")
			  {
				alert("Indicare l'Ente di rilascio del documento")
				frmImmigrati.cmbEnteRilascio.focus()
				return false
			  }
			  //Controllo della validit� della Data di Rilascio
				DataRil = frmImmigrati.txtDataRil.value
				
				if (frmImmigrati.txtDataRil.value == "")
				{
					alert("Data di Rilascio obbligatoria")
					frmImmigrati.txtDataRil.focus() 
					return false
				}
				
				if (!ValidateInputDate(DataRil))
				{
					frmImmigrati.txtDataRil.focus() 
					return false
				}	
				DataRilascio = frmImmigrati.txtDataRil.value
				DataOdierna=frmImmigrati.txtOggi.value
				if (ValidateRangeDate(DataOdierna,DataRilascio)==true)
				{
					
						alert("La data di rilascio deve essere inferiore alla data odierna!")
						frmImmigrati.txtDataRil.focus()
						return false
					
				}

			//Controllo della validit� della Data di Scadenza
				DataScad = frmImmigrati.txtDataScad.value
				if (frmImmigrati.txtDataScad.value == "")
				{
					alert("Data di Scadenza obbligatoria")
					frmImmigrati.txtDataScad.focus() 
					return false
				}			
				
				if (!ValidateInputDate(DataScad))
				{
					frmImmigrati.txtDataScad.focus() 
					return false
				}
				
				DataScadenza = frmImmigrati.txtDataScad.value
				if (ValidateRangeDate(DataScadenza,DataOdierna)==true)
				{
					
					alert("La data di scadenza deve essere maggiore della data odierna!")
					frmImmigrati.txtDataScad.focus()			
				    return false
				}
				
				DataRilascio = frmImmigrati.txtDataRil.value
				DataIngresso= frmImmigrati.txtDTingresso.value
				if (ValidateRangeDate(DataRilascio,DataIngresso)==true)
				{
					if (DataRilascio != DataIngresso) {
						alert("La data di Rilascio deve essere maggiore della data Ingresso!")
						frmImmigrati.txtDataRil.focus()	
						return false		
					}
					
				}
				//Controllo Numero documento
				document.frmImmigrati.txtNumeroDoc.value=TRIM(document.frmImmigrati.txtNumeroDoc.value)
				if (document.frmImmigrati.txtNumeroDoc.value=="")
				{
					alert("Numero del Documento Obbligatorio")
					frmImmigrati.txtNumeroDoc.focus()			
				    return false
				}
				
	
	
		 return true
		
}

</script>

<!--#include Virtual = "/include/ControlDateVB.asp"-->
<!--#include Virtual = "/util/dbutil.asp"-->

<% 
dim data_nascita
dim sesso
dim sOcc
dim rsD
dim Rec

sOcc=S
sesso=M
sIdpers=request.form("IdPers")

set rsD = Server.CreateObject("ADODB.Recordset")
dim sSqld
sSqld="SELECT DT_NASC FROM PERSONA WHERE ID_PERSONA='" & sIdPers & "'"
'PL-SQL * T-SQL  
SSQLD = TransformPLSQLToTSQL (SSQLD) 
rsD.Open sSqld,CC,3

data_nascita=rsD("DT_NASC")

set Rec = Server.CreateObject("ADODB.Recordset")

dim sSql
'Al momento apiccizzato perch� � una select sulla tabella PERS_DOC, che faccio poi sotto
	
'sSql="select PERS_MIGRAZIONE.DT_INGRESSO,PERS_MIGRAZIONE.COM_INGRESSO,PERS_MIGRAZIONE.PRV_INGRESSO," &_
'	"PERS_MIGRAZIONE.COD_MINGR,PERS_MIGRAZIONE.COD_STATO_UE,PERS_MIGRAZIONE.NUM_MESI_UE," &_
'	"PERS_MIGRAZIONE.FL_ALTROCOMUNE,PERS_MIGRAZIONE.FL_ALTRAPROV,PERS_MIGRAZIONE.FL_ALTRAREG," &_
'	"PERS_MIGRAZIONE.COM_MIGRAZIONE,PERS_MIGRAZIONE.PRV_MIGRAZIONE,PERS_MIGRAZIONE.FL_RITORNO,PERS_MIGRAZIONE.FL_PAESE_UE," &_
'	 "FROM PERS_MIGRAZIONE,PERS_DOC " &_
'	"WHERE PERS_MIGRAZIONE.ID_PERSONA=" & sIdpers & " AND PERS_DOC.ID_PERSONA=" & sIdpers & ""

'parte riguardante il permesso di soggiorno, ovvero ,DATA INGRESSO ,PROVINCIA INGRESSO, COMUNE INGRESSO, MOTIVO INGRESSO ECC....
sSql="select DT_INGRESSO,COM_INGRESSO,PRV_INGRESSO," &_
	"COD_MINGR,COD_STATO_UE,NUM_MESI_UE," &_
	"FL_ALTROCOMUNE,FL_ALTRAPROV,FL_ALTRAREG," &_
	"COM_MIGRAZIONE,PRV_MIGRAZIONE,FL_RITORNO,FL_PAESE_UE,DT_TMST " &_
	 "FROM PERS_MIGRAZIONE " &_
	"WHERE ID_PERSONA=" & sIdpers & ""

'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
Rec.Open sSql,CC,3


'flag che mi permette di vedere se sto 
'inserendo o modificando nella tabella PERS_MIGRAZIONE
dim sInserModMIGR
if Rec.EOF then
	'inserimento
	sInserModMIGR=1
	
else
	' modifica 
	sInserModMIGR=0

end if


dim sDataIng
dim sComuneIng
dim sProvIng
dim sMotivoIng
dim sStatoUE
dim sMesiTras
dim sflQprv
dim sflAprv
dim sflAreg
dim sComInt
dim sPrvInt
dim sflTornarePaese
dim sflApaese
dim sTipoDoc
dim sRilascDa
dim sDataRilasc
dim sDataFineVal
dim sIdDoc
dim sTms_Migrazione
dim sTms_MIGR

if Rec.EOF then

	sDataIng=""
	sComuneIng=""
	sProvIng=""
	sMotivoIng=""
	sStatoUE=""
	sMesiTras=""
	sflQprv=""
	sflAprv=""
	sflAreg=""
	sComInt=""
	sPrvInt=""
	sflTornarePaese=""
	sflApaese=""
	sTipoDoc=""
	sRilascDa=""
	sDataRilasc=""
	sDataFineVal=""
	'sCartaSogg=""
	sIdDoc=""
	sTms_Migrazione=""
    sTms_MIGR=""

else

	sDataIng=Rec("DT_INGRESSO")
	sComuneIng=Rec("COM_INGRESSO")
	sProvIng=Rec("PRV_INGRESSO")
	sMotivoIng=Rec("COD_MINGR")
	sStatoUE=Rec("COD_STATO_UE")
	sMesiTras=Rec("NUM_MESI_UE")
	sflQprv=Rec("FL_ALTROCOMUNE")
	sflAprv=Rec("FL_ALTRAPROV")
	sflAreg=Rec("FL_ALTRAREG")
	sComInt=Rec("COM_MIGRAZIONE")
	sPrvInt=Rec("PRV_MIGRAZIONE")
	sflTornarePaese=Rec("FL_RITORNO")
	sflApaese=Rec("FL_PAESE_UE")
	sTms_MIGR=Rec("DT_TMST")
	
'	sTipoDoc=Rec("COD_DOCST")
'	sRilascDa=Rec("COD_ENTE_RIL")
'	sDataRilasc=Rec("DT_RIL_DOC")
'	sDataFineVal=Rec("DT_FIN_VAL")
'	'sCartaSogg=Rec("COD_STATUS_DOC")
'	sIdDoc=rec("ID_DOC")
'	sTms_Migrazione=Rec("TM_MIGRAZIONE")
 '   sTms_MIGR=Rec("TM_DOC")
    
	dim CodComIng
	CodComIng=sComuneIng
	sComuneIng=LCASE(DescrComune(sComuneIng))
	
	
	if sComuneIng="0" then
		sComuneIng=""
	end if
	
	dim CodComInt
	CodComInt=sComInt
	sComInt=LCASE (DescrComune(sComInt))
	if sComInt="0" then
		sComInt=""
	end if
	
	sDataIng=LCASE (ConvDateToString(sDataIng))
'	sDataRilasc=LCASE (ConvDateToString(sDataRilasc))
'	sDataFineVal=LCASE (ConvDateToString(sDataFineVal))
	
end if


 
 
   
%>

<br>
<!--#include file="menu.asp"-->
<form action="UTE_CnfVisIm.asp" method="post" name="frmImmigrati" id="frmImmigrati">
<input type="hidden" id="text0" name="txtdtn" value="<%=ConvDateToString(data_nascita)%>">
<input type="hidden" id="text1" name="txtIdPers" value="<%=sIdpers%>">
<input type="hidden" id="text2" name="txtInserModMIGR" value="<%=sInserModMIGR%>">
<input type="hidden" id="text3" name="txtTms_Migrazione" value="<%=sTms_Migrazione%>">
<input type="hidden" id="text4" name="txtTms_Migr" value="<%=sTms_MIGR%>">

<table border="0" cellpadding="0" cellspacing="0" width="500">
    <tr height="18">
		<td class="sfondomenu" width="67%" height="18">
			<span class="tbltext0"><b>&nbsp;LAVORATORE IMMIGRATO</b></span></td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
		<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">(*) campi obbligatori</td>
		
    </tr>
    <tr height="2">
			<td width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif">	</td>     
	</tr>
    <tr>
       <td class="sfondocomm" width="57%" colspan="3">
       <br>Compila i campi sottostanti e premi <b>Invia</b> per confermare l'inserimento.
       <a href="Javascript:Show_Help('/Pgm/help/Iscr_Utente/UTE_VisImprg')" name="prv" onmouseover="javascript:status='' ; return true">
			<img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
       </td> 
    </tr>
    <tr height="2">
			<td width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif">	</td>     
	</tr>   
    
 </table>
 <br>

<table border="0" cellpadding="2" cellspacing="2" width="500" style="WIDTH:220px">
   <tr>
		<td align="middle" colspan="2" nowrap class="tbltext1" width="250">
					<p align="left">
						
						<strong>Data di Ingresso *</strong><font size="1"> (gg/mm/aaaa)</font>
						
					</p>
				</td>	
		
		
		<td align="left" width="250" colspan="2">
				<input class="textblacka" style="TEXT-TRANSFORM: uppercase; WIDTH: 85px;
					 HEIGHT: 22px" size="15" maxlength="10" name="txtDTingresso" value="<%=sDataIng%>">
				<input type="hidden" name="txtOggi" value="<%=ConvDateToString(Date())%>">
		</td>
		
    </tr>
    <tr>
        <td align="left" colspan="2" class="tbltext1">
			<span class="tbltext1">
				<strong>Departamento di Ingresso*</strong>
			 </span>
		</td>
	    <td align="left" colspan="2" width="60%">
				<%
				
				sInt = "PROV|0|" & date & "|" & sProvIng & "|cmbProvinciaIngresso' onchange='PulisciComIng()|ORDER BY DESCRIZIONE"			
				CreateCombo(sInt)
				
				%>
         </td>
    </tr>
    <tr>
		<td align="left" colspan="2" nowrap class="tbltext1">
				<span class="tbltext1">			
				<strong>Comune di Ingresso*</strong></span>
		</td>	
  		<td align="left" colspan="2" width="60%">
			<input class="textblacka" style="TEXT-TRANSFORM: uppercase; WIDTH: 162px; 
					HEIGHT: 22px" size="35" maxlength="35" name="txtComuneIngresso" VALUE="<%=sComuneIng%>" readonly>
		
		
				<input type="hidden" name="txtComuneIngrHid" value="<%=CodComIng%>">
<%
				NomeForm="frmImmigrati"
				CodiceProvincia="cmbProvinciaIngresso"
				NomeComune="txtComuneIngresso"
				CodiceComune="txtComuneIngrHid"
				Cap="NO"
%>
				<a href="Javascript:SelComune('<%=NomeForm%>','<%=CodiceProvincia%>','<%=NomeComune%>','<%=CodiceComune%>','<%=Cap%>')" ID="imgPunto1" name="imgPunto1" onmouseover="javascript:window.status='';return true"><img border="0" src="<%=Session("Progetto")%>/images/bullet1.gif"></a>

		
		
		</td>
    </tr>
    
      <tr>
        <td align="left" colspan="2" class="tbltext1">
			<span class="tbltext1">
				<strong>Motivo di Ingresso*</strong>
			 </span>
		</td>
	    <td align="left" colspan="2" width="60%">
				<%
				
				sInt = "MINGR|0|" & date & "|" & sMotivoIng & "|cmbMotivo|ORDER BY DESCRIZIONE"			
				CreateCombo(sInt)
				%>
         </td>
    </tr>
    <p>
    <tr>
		<td height="2" align="left" colspan="4" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
    </tr>

    <tr>
		 <tr>
        <td align="left" colspan="2" class="tbltext1">
			<span class="tbltext1">
				<strong>Stato delle U.E precedente<br>all'ingresso in Italia</strong>
			 </span>
		</td>
	    <td align="left" colspan="2" width="60%">
				<%
				sInt = "STATO|0|" & date & "|" & sStatoUE & "|cmbStato_UE|AND VALORE='EE' ORDER BY DESCRIZIONE"	
				CreateCombo(sInt)
				%>
         </td>
    </tr>
    

    <tr>
		<td align="left" colspan="2" nowrap class="tbltext1">
			<span class="tbltext1">
				<strong>Mesi trascorsi</strong></span>
		</td>
		<td align="left" colspan="2" width="30%">
			<input class="textblacka" style="TEXT-TRANSFORM: uppercase; WIDTH: 30px; 
					HEIGHT: 22px" size="35" maxlength="4" name="txtMesi" VALUE="<%=sMesiTras%>">
		</td>
    </tr>
    <tr>
		<td height="2" align="left" colspan="4" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
    </tr>
    <p>
    <tr>
        <td align="left" colspan="2" class="tbltext1">
				<span class="tbltext1">
				<strong>Intendi rimanere in questa Departamento</strong>
				</span>
        <td align="left" colspan="2" width="30%">
        <%
			select case sflQprv
			   case "N"%>
			   	<input type="radio" id="fl1" name="fl1" value="S"><span CLASS="TBLTEXT">SI</span>				
			   	<input type="radio" id="fl1" name="fl1" checked value="N"><span CLASS="TBLTEXT">NO</span>				
			 <%case "S"%>
			    <input type="radio" id="fl1" name="fl1" checked value="S"><span CLASS="TBLTEXT">SI</span>
			   	<input type="radio" id="fl1" name="fl1" value="N"><span CLASS="TBLTEXT">NO</span>
			<%case else%>
			    <input type="radio" id="fl1" name="fl1" checked value="S"><span CLASS="TBLTEXT">SI</span>
			   	<input type="radio" id="fl1" name="fl1" value="N"><span CLASS="TBLTEXT">NO</span>
			<%end select %>
		 </td>
	</tr>
    <tr>
        <td align="left" colspan="2" class="tbltext1">
				<span class="tbltext1">
				<strong>Intendi rimanere nella Regione</strong>
				</span>
        <td align="left" colspan="2" width="60%">
         <%
			select case sflAprv
			   case "N"%>
			   	<input type="radio" id="fl2" name="fl2" value="S"><span CLASS="TBLTEXT">SI</span>				
			   	<input type="radio" id="fl2" name="fl2" checked value="N"><span CLASS="TBLTEXT">NO</span>				
			 <%case "S"%>
			    <input type="radio" id="fl2" name="fl2" checked value="S"><span CLASS="TBLTEXT">SI</span>
			   	<input type="radio" id="fl2" name="fl2" value="N"><span CLASS="TBLTEXT">NO</span>
			<%case else%>
			    <input type="radio" id="fl2" name="fl2" checked value="S"><span CLASS="TBLTEXT">SI</span>
			   	<input type="radio" id="fl2" name="fl2" value="N"><span CLASS="TBLTEXT">NO</span>
			<%end select %>
		 </td>
   </tr>
        </tr>
     <!-- <tr>		<td height="2" align="left" colspan="4" background="<%=Session("Progetto")%>/images/separazione.gif"></td>    </tr>-->
		 <tr>
       <tr>
        <td align="left" colspan="2" class="tbltext1">
				<span class="tbltext1">
				<strong>Intendi trasferirti in altra Regione</strong>
				</span>
       <td align="left" colspan="2" width="60%">
         <%
			select case sflAreg
			   case "N"%>
			   	<input type="radio" id="fl3" name="fl3" value="S"><span CLASS="TBLTEXT">SI</span>				
			   	<input type="radio" id="fl3" name="fl3" checked value="N"><span CLASS="TBLTEXT">NO</span>				
			 <%case "S"%>
			    <input type="radio" id="fl3" name="fl3" checked value="S"><span CLASS="TBLTEXT">SI</span>
			   	<input type="radio" id="fl3" name="fl3" value="N"><span CLASS="TBLTEXT">NO</span>
			<%case else%>
			    <input type="radio" id="fl3" name="fl3" value="S"><span CLASS="TBLTEXT">SI</span>
			   	<input type="radio" id="fl3" name="fl3" checked value="N"><span CLASS="TBLTEXT">NO</span>
			<%end select %>
		 </td>
    </tr>
     <tr>
        <td align="left" colspan="2" class="tbltext1">
			<span class="tbltext1">
				Departamento di Interesse
			 </span>
		</td>
	    <td align="left" colspan="2" width="60%">
				<%
				sInt = "PROV|0|" & date & "|" & sPrvInt & "|cmbPrvInter' onchange='PulisciComInt()|ORDER BY DESCRIZIONE"			
				CreateCombo(sInt)
				%>
         </td>
    </tr>
		 <tr>
        <td align="left" colspan="2" class="tbltext1">
			<span class="tbltext1">
				Comune di Interesse
			 </span>
		</td>
	    <td align="left" colspan="2" width="60%">
			<input class="textblacka" style="TEXT-TRANSFORM: uppercase; WIDTH: 162px; 
					HEIGHT: 22px" size="35" maxlength="35" name="txtCmnInt" VALUE="<%=sComInt%>" readonly>
		
				<input type="text" name="txtComuneIntHid" value="<%=CodComInt%>">
<%
				NomeForm="frmImmigrati"
				CodiceProvincia="cmbPrvInter"
				NomeComune="txtCmnInt"
				CodiceComune="txtComuneIntHid"
				Cap="NO"
%>
				<a href="Javascript:SelComune('<%=NomeForm%>','<%=CodiceProvincia%>','<%=NomeComune%>','<%=CodiceComune%>','<%=Cap%>')" ID="imgPunto1" name="imgPunto1" onmouseover="javascript:window.status='';return true"><img border="0" src="<%=Session("Progetto")%>/images/bullet1.gif"></a>

		
		</td>
    </tr>
		 
    <tr>
		<td height="2" align="left" colspan="4" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
    </tr>
      <tr>
        <td align="left" colspan="2" class="tbltext1">
				<span class="tbltext1">
				<strong>Intendi tornare nel Paese di Origine</strong>				
				</span>
        <td align="left" colspan="2" width="60%">
        
        <%
			select case sflTornarePaese
			   case "N"%>
			   	<input type="radio" id="fl4" name="fl4" value="S"><span CLASS="TBLTEXT">SI</span>				
			   	<input type="radio" id="fl4" name="fl4" checked value="N"><span CLASS="TBLTEXT">NO</span>				
			 <%case "S"%>
			    <input type="radio" id="fl4" name="fl4" checked value="S"><span CLASS="TBLTEXT">SI</span>
			   	<input type="radio" id="fl4" name="fl4" value="N"><span CLASS="TBLTEXT">NO</span>
			<%case else%>
			    <input type="radio" id="fl4" name="fl4" value="S"><span CLASS="TBLTEXT">SI</span>
			   	<input type="radio" id="fl4" name="fl4" checked value="N"><span CLASS="TBLTEXT">NO</span>
			<%end select %>
		 </td>
    </tr>
	  <tr>
        <td align="left" colspan="2" class="tbltext1">
				<span class="tbltext1">
				<strong>Intendi Trasferirti in altro paese U.E</strong>				
				</span>
        <td align="left" colspan="2" width="60%">
        <%
			select case sflApaese
			   case "N"%>
			   	<input type="radio" id="fl5" name="fl5" value="S"><span CLASS="TBLTEXT">SI</span>				
			   	<input type="radio" id="fl5" name="fl5" checked value="N"><span CLASS="TBLTEXT">NO</span>				
			 <%case "S"%>
			    <input type="radio" id="fl5" name="fl5" checked value="S"><span CLASS="TBLTEXT">SI</span>
			   	<input type="radio" id="fl5" name="fl5" value="N"><span CLASS="TBLTEXT">NO</span>
			<%case else%>
			    <input type="radio" id="fl5" name="fl5" value="S"><span CLASS="TBLTEXT">SI</span>
			   	<input type="radio" id="fl5" name="fl5" checked value="N"><span CLASS="TBLTEXT">NO</span>
			<%end select %>	 
		 </td>
    </tr>
    <tr>
		<td height="2" align="left" colspan="4" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
    </tr>
     <tr>
		<td align="left" colspan="4">
			<span class="textblack">	
			Riporta qu� di seguito i dati relativi al <b>Permesso di Soggiorno</b>
			</span>
			<br>	<br>
		</td>
    </tr>
    
    <%'parte riguardante il permesso di soggiorno, ovvero ,:
    'TIPO DOCUMENTO; RILASCIATO DALLO SPORTELLO UNICO;
    ' DATA DI RILASCIO; VALIDO FINO AL; 
    'NUMERO DEL DOCUMENTO.*****************
    set Rec2 = Server.CreateObject("ADODB.Recordset")
	dim sSqlP
    
		sSqlP="select COD_DOCST,PRV_RIL,DT_RIL_DOC,DT_FIN_VAL,ID_DOC" &_
     	" FROM PERS_DOC WHERE ID_PERSONA=" & sIdpers &_
     	" and cod_docst='04'"
   
	'Response.Write sSqlP
	'Response.End 
	
'PL-SQL * T-SQL  
SSQLP = TransformPLSQLToTSQL (SSQLP) 
    Rec2.Open sSqlP,CC,3
    dim sEsisteNonesiste
    
    
    if Rec2.EOF then
   
		'non esiste
		
		sTipoDoc=""
		sRilascDa=""
		sDataRilasc=""
		sDataFineVal=""
		sIdDoc=""
		sTm_Doc=""
    else
    
		'esiste
		do until Rec2.EOF
			sTipoDoc=Rec2("COD_DOCST")
			sRilascDa=Rec2("PRV_RIL")
			sDataRilasc=Rec2("DT_RIL_DOC")
			sDataFineVal=Rec2("DT_FIN_VAL")
			sIdDoc=Rec2("ID_DOC")
		Rec2.MoveNext
		loop
		
		sDataRilasc=LCASE (ConvDateToString(sDataRilasc))
		sDataFineVal=LCASE (ConvDateToString(sDataFineVal))
		
		
    end if
    'rec2.Close
    'set rec2=nothing
    %>
  <tr>
		    <td align="middle" colspan="2" nowrap class="tbltext1" width="250">
					<p align="left">
						
						<strong>Numero del Documento*</strong>
						<strong>&nbsp;</strong>
					</p>
				</td>	
				<td align="left" colspan="2" width="60%" width="400">
					<input type="text" style="TEXT-TRANSFORM: uppercase" maxlength="10" name="txtNumeroDoc" value="<%=sIdDoc%>" size="29" class="textblacka">
				</td>
		    
  
	</tr>	       
 				<%
					DIM sCodDocOcco
					
					if sTipoDoc <> "04"  then
						
						if sTipoDoc="" then
						
							sCodDocOcco="VUOTA"
							sTipoDoc="04"
						else
							sCodDocOcco=sTipoDoc
							sTipoDoc="04"
						end if
					else
					
						
						sCodDocOcco="PS"
					end if
					
					
				%>
					<input type="hidden" id="cmbTipoDoc" name="cmbTipoDoc" value="<%=sTipoDoc%>">

					<input type="hidden" id="txtTipoOcco" name="txtTipoOcco" value="<%=sCodDocOcco%>">
					<%
					'sInt = "DOCST|0|" & date & "|" & sTipoDoc & "|cmbTipoDoc|ORDER BY DESCRIZIONE"			
					'CreateCombo(sInt)	
					%>
					
			
			<tr>
			 	<td align="left" colspan="2" nowrap class="tbltext1">
					
					
						<strong>Rilasciato dallo sportello Unico<br>
						 del Departamento di*</strong>
						
					
				</td>
				<td align="left" colspan="2" width="60%" width="400">
					<%
					sInt = "PROV|0|" & date & "|" & sRilascDa & "|cmbEnteRilascio|ORDER BY DESCRIZIONE"			
					CreateCombo(sInt)				
					%>
				</td>
		    </tr>
		    </tr>
			<tr>     
					<td align="middle" colspan="2" nowrap class="tbltext1" width="250">
					<p align="left">
						
						<strong>Data di Rilascio *</strong><font size="1"> (gg/mm/aaaa)</font>
						
					</p>
				</td>	
				<td align="left" colspan="2" width="60%" width="400">
					<input type="text" style="TEXT-TRANSFORM: uppercase" maxlength="10" name="txtDataRil" value="<%=sDataRilasc%>" size="10" class="textblacka">
					
					<input type="hidden" name="txtoggi" value="<%=ConvDateToString(sDataRilasc)%>">
				</td>
		    </tr>
		    <tr>     
				<td align="middle" colspan="2" nowrap class="tbltext1" width="250">
					<p align="left">
						
						<strong>Valido fino al*</strong><font size="1"> (gg/mm/aaaa)</font>
						
					
				</td>	
				<td align="left" colspan="2" width="60%" width="400">
					<input type="text" style="TEXT-TRANSFORM: uppercase" maxlength="10" name="txtDataScad" size="10" class="textblacka" VALUE="<%=sDataFineVal%>">
				</td>
		    </tr>
		   
</table>    
 <%
 'Rec.Close
 'rec=nothing
 %>   
  
   <br>
<table border="0" cellpadding="1" cellspacing="1" width="370" align="center">
	 <tr>
        <td align="middle" colspan="2">
        <input type="image" name="Invia" src="<%=Session("Progetto")%>/images/conferma.gif" border="0" value="Conferma" onclick="return ControllaDati()"> 
        </td>
     </tr>
</table>
</form>
<!--#include virtual="/strutt_coda2.asp"-->
<!--#include Virtual = "/include/CloseConn.asp"-->

