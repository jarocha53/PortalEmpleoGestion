<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include Virtual = "/strutt_testa2.asp"-->
<!--#include Virtual = "/include/OpenConn.asp"-->
<!--#include Virtual = "/include/ControlDateVB.asp"-->
<!--#include Virtual = "/include/DecCod.asp"-->
<!--#include Virtual = "/include/SysFunction.asp"-->
<!--#include virtual = "/util/portallib.asp"-->
<!--#include Virtual="/util/dbUtil.asp"-->
<!--#include Virtual = "/include/ControlVall.asp"-->

<script language="javascript" src="ControlliBac.js"></script>

<script LANGUAGE="Javascript">
//include del file per fare i controlli sulla validit� delle date
<!--#include Virtual = "/Include/ControlDate.inc"-->

//include del file per fare i controlli sulla numericit� dei campi
<!--#include Virtual = "/Include/ControlNum.inc"-->

//include del file per fare i controlli sulle stringhe
<!--#include Virtual = "/include/ControlString.inc"-->
<!--#include Virtual = "/include/help.inc"-->


function ControlarDatos(){


		if (document.frmModStatOcc.CmbTipoOcup.value == "")
		{
			alert("Debe elegir una situaci�n laboral");
			return false;
		}
		
		if (document.frmModStatOcc.COD_STDIS.value =="N12")
		{
			if (document.frmModStatOcc.CmbOcup.value == "")
			{
				alert("El campo Tipo de ocupaci�n es obligatorio");
				return false;
			}
		}

		if (document.frmModStatOcc.COD_STDIS.value =="N03")
		{
			if ((document.frmModStatOcc.CmbTipoHoras.value == "") || (document.frmModStatOcc.TxtCantHoras.value == "")) 
			{
				alert("Los campos referidos a la duraci�n son obligatorios");
				return false;
			}
			if (document.frmModStatOcc.TxtCantHoras.value != "")
			{
				if (!IsNum(document.frmModStatOcc.TxtCantHoras.value))
				{
					alert ("La duraci�n debe ser una expresi�n num�rica");
					return false;
				}
			}
		}
			 
		if (document.frmModStatOcc.COD_STDIS.value =="I01")
		{
			if (document.frmModStatOcc.txtDecDis.value == "")
			{
				alert("El campo Hasta cuando trabaj� es obligatorio");
				return false;
			}
			if (document.frmModStatOcc.txtDecDis.value != "")
			{
				if (!ValidateInputDate(document.frmModStatOcc.txtDecDis.value))
				{
					return false;
				}
			}
		}	

}

function Cargar()
{
	if (document.frmModStatOcc.COD_STDIS.value != "") 
	{
		document.frmModStatOcc.CmbTipoOcup.value = document.frmModStatOcc.COD_STDIS.value
		if (document.frmModStatOcc.stato.value == "2")
		{
			 document.frmModStatOcc.CmbTipoOcup.disabled = true;
		}
	}
}

function ControllaIndet()
{
	
	if (document.frmModStatOcc.CmbTipoOcup.value != "") 
	{
		document.frmModStatOcc.action = "UTE_ModStatOcc.asp"
		document.frmModStatOcc.submit();
	}
}

	
</script>



<%
dim TipoOcupElegido

TipoOcupElegido = request("CmbTipoOcup")

dim sIDPersona
dim ProvinciaCI
dim sSql
dim PRV_RES
dim PRV_DOM
dim COM_RES
dim COM_DOM

sIdpers = Request("ID_Persona")

if sIdPers = "" then 
	sIdPers = request("txtIdPers")
end if

s = Request("Stato")
%>
<br>
<%
sSinMenu = request("SinMenu")
if sSinMenu <> "S" then
%>
<!--#include file="menu.asp"-->
<%
end if 
%>
	
	<br>
<% 
   
	sSQL="SELECT PRV_RES,COM_RES,PRV_DOM,COM_DOM FROM PERSONA WHERE ID_PERSONA=" & sIdPers
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
	set rsNominativo =CC.execute(sSQL)
	NessunDato=false
	if not rsNominativo.eof then
		PRV_RES=rsNominativo("PRV_RES")
		PRV_DOM=rsNominativo("PRV_DOM")
		COM_RES=rsNominativo("COM_RES")
		COM_DOM=rsNominativo("COM_DOM")
	end if 
	

	NessunDato=true

	if not IsNull(COM_RES) and IsNull(COM_DOM) and mid(COM_RES,1,1) <> "Z" then
		NessunDato=false
	end if

	if not IsNull(COM_RES) and not IsNull(COM_DOM) and mid(COM_RES,1,1) <> "Z" then
		NessunDato=false
	end if

	if not IsNull(COM_DOM) and mid(COM_RES,1,1) = "Z" then
		NessunDato=false
	end if

	set rsNominativo = nothing
	if PRV_DOM <> "" then
		ProvinciaCI = PRV_DOM
	else
		ProvinciaCI = PRV_RES
	end if

%>   

		
	<table border="0" CELLPADDING="0" CELLSPACING="0" width="500" ID="Table1">
		<tr height="18">
			
    <td class="sfondomenu" width="67%" height="18"><span class="tbltext0"><b>&nbsp;SITUACION LABORAL ACTUAL </b></span></td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
			<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">(*) campo obligatorio</td>
		</tr>
	</table>
	<table border="0" CELLPADDING="0" CELLSPACING="0" width="500" ID="Table2">
		<tr>
			
    <td align="left" class="sfondocomm"> Esta p�gina permite modificar los datos relacionados con su situaci�n laboral actual. <br>Se podr�n modificar todos los datos menos el que indica la situaci�n laboral actual, si esta cambi�, deber� ingresar una nueva situaci�n laboral.<br>
      Presionar Enviar para guardar los datos modificados.<br>			

   	  <!--NUEVO AGREGADO POR F.BASANTA -->
	  <br>
	  Al momento de la entrevista se ingresa la situaci�n laboral actual, es decir en referencia
	  a la fecha de la entrevista. 
	  <br>
	  Si se realiza una nueva entrevista y la situaci�n laboral es otra, se la ingresa sin 
	  modificar la anterior.
	  <br>
      <img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0" onclick="Javascript:window.open('ayuda_situacionlaboral.htm','Ayuda','width=500,height=350,Resize=No,Scrollbars=yes')">


<!--
			<a href="Javascript:Show_Help('/Pgm/help/Iscr_Utente/UTE_ModStatOcc')" onmouseover="javascript:window.status='';return true">
			<img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
-->
			</td>
		</tr>
		<tr height="2">
			<td class="sfondocomm" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>
	
<%

dim rsPers

if s = 2 then 
sSQL="SELECT ID_PERSONA,IND_STATUS," &_
	 "COD_STDIS,DT_DICH_STATUS," &_
	 "DT_FIN_TEMPODET,COD_ST181," &_
	 "ID_CIMPIEGO,DT_DEC_ISCR," &_
	 "ID_SEDE,DT_TMST,DT_DEC_DIS, TIPOOCUPADO, SUBHORAS, SUBTIPOHORAS " &_
	 "FROM STATO_OCCUPAZIONALE " &_
	 "WHERE ID_PERSONA = " & sIdPers &_
	 " AND IND_STATUS=0"
	 
 'Response.Write sSQL
 
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
set rstLista = CC.Execute(sSQL)

if not rstLista.EOF then
   	
	CodiceStato=rstLista("COD_STDIS")
	Stato=mid(rstLista("COD_STDIS"),1,1)
	
	if TipoOcupElegido = "" then
		TipoOcupElegido = CodiceStato
	end if
	
	STIPOOCUPADO = rstlista("TIPOOCUPADO")
	SSUBHORAS = rstlista("SUBHORAS")
	SSUBTIPOHORAS = rstlista("SUBTIPOHORAS")
	
	sCategoria=rstLista("COD_ST181")
	IdSede=rstLista("ID_SEDE")
	IdCimpiego=rstLista("ID_CIMPIEGO")
	vData=rstLista("DT_TMST")
	DT_DICH_STATUS=rstLista("DT_DICH_STATUS")
	
	if rstLista("DT_FIN_TEMPODET") <>"" then
		sSCadenza=ConvDateToString(rstLista("DT_FIN_TEMPODET"))
	else
		sSCadenza=""
	end if

	if rstLista("DT_DEC_ISCR")<>"" then
		sDatImp=ConvDateToString(rstLista("DT_DEC_ISCR"))
	else
		sDatImp=""
	end if
	
	if rstLista("DT_DEC_DIS")<>"" then
		sDatDis=ConvDateToString(rstLista("DT_DEC_DIS"))
	else
		sDatDis=""
	end if	
	
	if IdCimpiego <> "" then
		sSQL="SELECT DESCRIZIONE " &_
			 "FROM SEDE_IMPRESA " &_
			 "WHERE ID_SEDE = " & IdCimpiego
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
		Set rstCimpiego = CC.execute(sSQL)
		DescCimpiego=rstCimpiego("DESCRIZIONE")
		rstCimpiego.Close()
	else
		DescCimpiego=""
	end if

	if IdSede <> "" then
		sSQL="SELECT A.DESCRIZIONE,B.RAG_SOC,B.COD_TIMPR " &_
			 "FROM SEDE_IMPRESA A,IMPRESA B " &_
			 "WHERE A.ID_IMPRESA=B.ID_IMPRESA AND ID_SEDE = " & IdSede
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
		Set rstSede = CC.execute(sSQL)
		DescSede=rstSede("DESCRIZIONE")
		DescRagione=rstSede("RAG_SOC")
		COD_TIMPR=rstSede("COD_TIMPR")
		rstSede.Close()
	else
		DescSede=""
	end if
	
end if
rstLista.Close

end if

set rstLista = nothing

if sID_SEDE <> "" and sDESCRIZIONE <> "" then
	DescCimpiego = sDESCRIZIONE
	idCimpiego = sID_SEDE
end if

%>
	<form method="post" name="frmModStatOcc" action="UTE_CnfModStatOcc.asp" onsubmit="return ControlarDatos();">
	
	<input type="hidden" name="txtIdPers" value="<%=sIdpers%>">
	<input type="hidden" name="DT_TMST" value="<%=vData%>">
	<input type="hidden" name="COD_STDIS" value="<%=TipoOcupElegido%>">
	<input type="hidden" name="txtoggi" value="<%=ConvDateToString(Date())%>">
	<input type="hidden" name="txtDtDichStat" value="<%=ConvDateToString(DT_DICH_STATUS)%>">
	<input type="hidden" name="stato" value="<%=s%>">
	<input type="hidden" name="SinMenu" value="<%=sSinMenu%>">


	<table border="0" cellpadding="0" cellspacing="1" width="400" ID="Table3">

		 		    		    
	    <tr>
			<td align="middle" width=48% class="tbltext1">
				<p align="left"><strong>Cual es su situaci�n laboral <br>en este momento? *</strong> </p>
			</td>
			<td align="left" width=70%>
			<%				
				sInt = "STDIS|0|" & date & "|" & TipoOcupElegido & "|CmbTipoOcup' onchange='ControllaIndet()|ORDER BY VALORE ASC"			
				CreateCombo(sInt)
			%>				
			</td>
		</tr>
		
		<tr><td colspan=2>&nbsp;</td></tr>
	
		<tr><td colspan="2">
		<table border=0 width=100%>
		
		<%
		'Response.WRITE STIPOOCUPADO & "<BR>"
		'Response.WRITE SSUBHORAS & "<BR>"
		'Response.WRITE SDATDIS & "<BR>"
		%>
			<%if TipoOcupElegido = "N12" then ''es ocupado%>
			<tr>
				<td class="tbltext1" width=40%>
					<b> Elija un tipo de ocupaci�n</b>
				</td>
				<td align=left> 
				<%
					sInt = "TOCUP|0|" & date & "|" & UCASE(STIPOOCUPADO) & "|CmbOcup' onchange='|ORDER BY VALORE ASC"			
					CreateCombo(sInt)
				%>
				</td>
			</tr>
			
			<%elseif TipoOcupElegido = "N03" then ''es subocupado%>
			<tr>
				<td class="tbltext1" width=40%> 
					<b>Trabaja</b>
					
				</td>
				
				<td  class="tbltext1"><input class="textblack" type="text" name="TxtCantHoras" maxlength="3" size="3" VALUE="<%=SSUBHORAS%>"> Hs.&nbsp;&nbsp; 
					<%
					sInt = "THORA|0|" & date & "|" & UCase(SSUBTIPOHORAS) & "|CmbTipoHoras' onchange='| AND VALORE='SUB' ORDER BY VALORE ASC"			
					CreateCombo(sInt)
					%>
				</td>
			</tr>
				
			<%elseif TipoOcupElegido = "I01" then ''es desocupado%>
			<tr>
				<td class="tbltext1" width=40%> 
					<b>Hasta cuando trabaj�?* <br>(dd/mm/aaaa)</b>
				</td>
				<td>
					<input class="textblack" type="text" id="txtDecDis" name="txtDecDis" MAXLENGTH=10 size=14 value="<%=sDatDis%>" maxlength="10" class="textblacka" size="10">
				</td>
			</tr>
			
			<%else ''es nunca trabajo%>

			<%end if%>
			
		</td></tr>
	
		
		<tr>
			<td>&nbsp;</td>
		</tr>
		<tr height="2">
			<td width="100%" colspan="2" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
		</tr>	    
		<tr>
			<td>&nbsp;</td>
		</tr>
		</table>	
  	    	    

		<br>
		<table border="0" cellpadding="1" cellspacing="1" width="500" ID="Table4">
		<tr>
		    <td align="middle" colspan="2">
				<a href="javascript:document.FrmVolver.submit();"><img onload=Cargar() alt="Indietro" border="0" src="<%=Session("Progetto")%>/images/indietro.gif"></a>
			  <%'bControllo =Controlla(sIdpers)
				'INIZIO 28/11/2003
				'if Session("Progetto")="/PLAVORO" then
				'if (Session("tipopers")="P" and clng(Session("Creator"))=clng(sIdpers)) then
				 '   if bControllo = false then%> 
				         <!--<input type="image" src="<%=Session("Progetto")%>/images/conferma.gif" border="0" value="Modifica" onclick="return ControllaDati(this)" id="image1" name="image1">-->
			<%    '  end if
			    'else   %>
			        <input type="image" src="<%=Session("Progetto")%>/images/conferma.gif"  value="Modifica" name="image1">
			<%  'end if %>         
			</td>
		</tr>
		</table>
 	   </form>
 	   
	<form name="FrmVolver" action="UTE_VisStatOcc.asp" method=post>
	<input type="hidden" name="IdPers" value="<%=sIdPers%>">
	<input type="hidden" name="SinMenu" value="<%=sSinMenu%>">
	</form>
	</td>
	</tr>
</table>

<!--#include virtual = "/include/CloseConn.asp"-->
<!--#include Virtual = "/strutt_coda2.asp"-->
