<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include Virtual = "/strutt_testa2.asp"-->
<!--#include Virtual = "/include/OpenConn.asp"-->
<!--#include virtual = "/include/ControlDateVb.asp"-->
<!--#include virtual = "/util/dbutil.asp"-->
<!--#include virtual = "/include/DecComun.asp"-->
<!--#include Virtual = "include/DecCod.asp"-->

<!--JAVASCRIPT-->			


<script language="javascript" src="/Include/ControlString.inc">
</script>

<script LANGUAGE="Javascript">

//include del file per fare i controlli sulla validit� delle date
<!--#include Virtual = "/Include/ControlDate.inc"-->

//include del file per fare i controlli sulla numericit� dei campi
<!--#include Virtual = "/Include/ControlNum.inc"-->

//include del file per fare i controlli sulla stringa
<!--#include Virtual = "/Include/ControlString.inc"-->
<!--#include Virtual = "/Include/help.inc"-->


function recarga()
{
	document.frmRecarga.cmbopderiv.value=TRIM(document.frmDeriv.cmbopderiv.value)
	document.frmRecarga.txtotro.value=TRIM(document.frmDeriv.txtotro.value)
	document.frmRecarga.txtfechaderiv.value=TRIM(document.frmDeriv.txtfechaderiv.value)
	document.frmRecarga.cmbResDeriv.value=TRIM(document.frmDeriv.cmbResDeriv.value)
	document.frmRecarga.submit()
	//window.navigate("UTE_InsCatProt.asp?CategProt="+document.frmCatProt.cmbCatProt.value)
}


//Controllo Validit� dei campi	
function ControlarDatos(){
    
       
    if ((frmDeriv.cmbopderiv.value == "") && (frmDeriv.txtotro.value == "")) 
    
    {
		alert("Debe elegir una opci�n de direccionamiento o bien completar el campo otra")
		return false;
    }
    
    if ((frmDeriv.cmbopderiv.value != "") && (frmDeriv.txtotro.value != "")) 
    
    {
		alert("Los campos opciones de direccionamiento y otra, son alternativos, no puede completar ambos")
		return false;
    }
    
    //agregado
    if (document.frmDeriv.txtotro.value != "")
    {
		if (document.frmDeriv.txtotro.value.indexOf("'") != -1)
		{
			alert("No esta permitido el ingreso del caracter '");
			return false;
		}
	}
	//fin agregado
			
    if (frmDeriv.txtfechaderiv.value == "")
    
    {
		alert("El campo fecha de direccionamiento es obligatorio");
		frmDeriv.txtfechaderiv.focus();
		return false;
    }
    
    
    /*if (frmDeriv.txtfecharesult.value == "")
    
    {
		alert("El campo fecha de derivaci�n es obligatorio");
		frmDeriv.txtfecharesult.focus();
		return false;
    }*/
    
    //Controllo della validit� della Data Inizio Categoria Protetta
	if (!ValidateInputDate(frmDeriv.txtfechaderiv.value)){
		return false;
		frmDeriv.txtfechaderiv.focus() 
		
	}
	
	if (ValidateRangeDate(frmDeriv.txtfechaderiv.value,frmDeriv.txtfechaactual.value)==false){
		alert("La fecha de direccionamiento no debe ser mayor a la fecha actual!")
		frmDeriv.txtfechaderiv.focus()
	   return false
	}
		
	/*if (ValidateRangeDate(DataIniCat,DataNascita)==true){
		alert("La fecha de inicio de la categor�a protegida debe ser mayor a la fecha de nacimiento!")
		frmCatProt.txtDtIniCat.focus()
	   return false
	}		*/

	
	//Data Inizio Certificazione
	if (frmDeriv.txtfechaderiv.value == ""){
		alert("El campo fecha de direccionamiento es obligatorio!")
		frmDeriv.txtfechaderiv.focus() 
		return false;
	}

    
	if (ValidateRangeDate(frmDeriv.txtfechaderiv.value,frmDeriv.txtfechanac.value)==true){
		alert("La fecha de direccionamiento no puede ser inferior a la fecha de nacimiento!")
		frmDeriv.txtfechaderiv.focus()
	   return false
	}
    
	if (frmDeriv.txtfecharesult.value != "")
	{
		if (ValidateRangeDate(frmDeriv.txtfecharesult.value,frmDeriv.txtfechanac.value)==true){
			alert("La fecha de resultado de derivaci�n no puede ser inferior a la fecha de nacimiento!")
			frmDeriv.txtfecharesult.focus()
		   return false
		  
		}
		//Controllo della validit� della Data Inizio Certificazione
	    if (frmDeriv.txtfecharesult.value != "")
	    {
		    if (!ValidateInputDate(frmDeriv.txtfecharesult.value)){
			    frmDeriv.txtfecharesult.focus() 
			    return false;
		    }
	    }
		 	//Data Inizio Certificazione non successiva data odierna -- 
        if (ValidateRangeDate(frmDeriv.txtfecharesult.value, frmDeriv.txtfechaactual.value)==false){
            //alert("DataIniCert " + DataIniCert + "DataOdierna " + DataOdierna)
	        alert("La fecha de resultado de la derivaci�n no debe ser mayor a la fecha actual!")
	        frmDeriv.txtfecharesult.focus()
           return false
        }
	}
	
	if (frmDeriv.txtfecharesult.value != ""){
		//Controllo della validit� della Data Fine Certificazione
		if (!ValidateInputDate(frmDeriv.txtfecharesult.value)){
			frmDeriv.txtfecharesult.focus() 
			return false
			}
		}

//controllo tra Data Inizio e Fine Certificazione
	if (frmDeriv.txtfecharesult.value != "")
	{ 
		if (frmDeriv.txtfecharesult.value != frmDeriv.txtfechaderiv.value) 
			{
			if (ValidateRangeDate(frmDeriv.txtfecharesult.value,frmDeriv.txtfechaderiv.value)==true)
			{
			alert("La fecha de resultado de la derivaci�n no puede ser inferior a la fecha de derivaci�n!")
			frmDeriv.txtfecharesult.focus()
		   return false
			}
		}
	}
	
	
//controllo tra Data Fine Categoria Protetta e Data inizio Certificazione
//var DataFinCatProtetta;
//DataFinCatProtetta= document.frmCatProt.txtFinCatOld.value
/*
if (DataFinCatOld != "") { 
	//alert("DataFinCatOld " +  DataFinCatOld);
	//alert("DataIniCat " + DataIniCat);
	//alert(ValidateRangeDate(DataFinCatOld,DataIniCat));
	if (!ValidateRangeDate(DataFinCatOld,DataIniCat)){
		alert("Non si pu� inserire una data di Inizio Categoria Protetta inferiore alla data di Fine Certificazione dell'occorrenza esistente!")
		frmCatProt.DataIniCat.focus()
		return false
	}	
}
*/	
	/*if (IsNum(document.frmCatProt.txtEnteCert.value)){
		alert("El valor no debe ser num�rico!")
		document.frmCatProt.txtEnteCert.focus() 
		return false
		}	*/	

}

</script>




<%
dim sIdPers
dim sqlderiv
dim rsderiv

sIdPers = Request("IdPers")
sDerivacion = Request("cmbopderiv")
ValorOtro = Request("txtotro")
sFechaDeriv = Request("txtfechaderiv")
sResDeriv = Request("cmbResDeriv")


%>
<br>
<!--#include file="menu.asp"-->

<br>
<table cellpadding="0" cellspacing="0" width="500" border="0">
	<tr height="18">
		<td class="sfondomenu" height="18" width="45%">
			<span class="tbltext0"><b>&nbsp;DIRECCIONAMIENTOS</b></span></td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
		<td valign="middle" align="right" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1">(*) campo obligatorio</td>
	</tr>
	<tr height="2">
		<td width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif">	</td>     
	</tr>
	<tr width="371" >
		<td colspan="3" class="sfondocomm" align="left">
		Para ingresar los datos completar los siguientes campos. <br>
		Presionar <b>enviar</b> para guardar la informaci�n. 
		<!--<%if sCondizione = "INS" then%>
			<a href="Javascript:Show_Help('/Pgm/help/Iscr_Utente/UTE_InsCatProt')" name="prov3" onmouseover="javascript:window.status='' ; return true"><img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
		<%else%>
			<a href="Javascript:Show_Help('/Pgm/help/Iscr_Utente/UTE_InsCatProt_1')" name="prov3" onmouseover="javascript:window.status='' ; return true"><img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
		<%end if%>-->
		</td>
	</tr>
	<tr height="2">
		<td colspan="3" class="SFONDOCOMM" background="<%=Session("Progetto")%>/images/separazione.gif">
		</td>
	</tr>
</table>

<br><br><br>

<form name="frmRecarga" method="post" action="UTE_InsDeriv.asp">
	<input type="hidden" name="IdPers" value="<%=sIdPers%>">
	<input type="hidden" name="cmbopderiv" value="">
	<input type="hidden" name="txtotro" value="">
	<input type="hidden" name="txtfechaderiv" value="">
	<input type="hidden" name="cmbResDeriv" value="">
</form>

<form name="frmDeriv" method="post" action="UTE_CnfDeriv.asp" onsubmit="return ControlarDatos()">
	<table border="0" cellpadding="6" cellspacing="1" width="500">

<%' Opciones de derivaci�n  -------------------------------------------------------------------------- %>  
		<tr>
			<td align="left" colspan="2" nowrap="nowrap" class="tbltext1">
				<strong>&nbsp;&nbsp;Opciones de <br> Direccionamiento</strong>
			</td>
			<td align="left" colspan="2" width="60%">
				<%'DerivCombo = "DERIV|0|" & date & "|" & "|cmbopderiv|ORDER BY DESCRIZIONE"			
				DerivCombo = "DERIV|0|" & date & "|" & sDerivacion & "|cmbopderiv|ORDER BY DESCRIZIONE"			
				CreateCombo(DerivCombo)%>
			</td>
		</tr>

<%' Otra --------------------------------------------------------------------------------------------- %>  
		  
		<tr>
			<td align="left" colspan="2" nowrap="nowrap" class="tbltext1">
				<strong>&nbsp;&nbsp;Otra (especificar)</strong>
			</td>
			    
			<td align="left" colspan="2" width="60%">
				<input type="text" name="txtotro" style="TEXT-TRANSFORM: uppercase;" class="textblack" maxlength="400" size="50" value="<%=ValorOtro%>">
			</td>
		</tr>

<%' Fecha de Derivaci�n ------------------------------------------------------------------------------ %>  
  
		<tr>
			<td align="left" colspan="2" class="tbltext1">
				<strong>&nbsp;&nbsp;Fecha de Direccionamiento*</strong>
			</td>
			    
			<td align="left" colspan="2" width="60%" class="tbltext1">
				<input type="text" name="txtfechaderiv" style="TEXT-TRANSFORM: uppercase;" span="span" class="textblack" size="12" maxlength="10" value="<%=sFechaDeriv%>">
				<a>&nbsp;&nbsp;(dd/mm/aaaa)</a>
			</td>
		</tr>


<%' Resultado de la Derivaci�n ----------------------------------------------------------------------- %>  

		<tr>
			<td align="left" colspan="2" nowrap="nowrap" class="tbltext1">
				<strong>&nbsp;&nbsp;Resultado del Direccionamiento*</strong>
			</td>
			<td align="left" colspan="2" width="60%">
				<% 
				ResDerivCombo = "DREST|0|" & date & "|" & sResDeriv & "|cmbResDeriv' onChange=" & chr(34) & "javascript: recarga()" & chr(34) & "|ORDER BY DESCRIZIONE"			
				%>
				<input type="hidden" name="txtResDeriv" value="<%=sResDeriv%>">
				<%
				CreateCombo(ResDerivCombo)
				%>
			</td>
		</tr>

<%' Fecha de Resultado ------------------------------------------------------------------------------ %>  

		<%
		'if sResDeriv = "01" or sResDeriv = "03" then
		if sResDeriv = "01" then
			'if sResDeriv = "01" then
				EtiqDTfin = "Fecha Resultado Direccionamiento"
			'else
			'	EtiqDTfin = "Fecha Plazo Nueva"
			'end if
			
			%>
			<tr>
				<input type="hidden" name="OpDeriv" value="<%=sDerivacion%>">
				<td align="left" colspan="2" nowrap="nowrap" class="tbltext1">
					<strong>&nbsp;&nbsp;<%=EtiqDTfin%></strong> 
				</td>
				<td align="left" colspan="2" width="60%" class="tbltext1">
					<input type="text" name="txtfecharesult" style="TEXT-TRANSFORM: uppercase;" span="span" class="textblack" size="12" maxlength="10" value="<%=sFechaResult%>">
					<a>&nbsp;&nbsp;(dd/mm/aaaa)</a>
				</td>
			</tr>
			<% ELSE %>
			
					<input type="HIDDEN" name="txtfecharesult" style="TEXT-TRANSFORM: uppercase;" span="span" class="textblack" size="12" maxlength="10" value="">

		<%End if	
		%>
    
    
<%' Datos de la Persona ------------------------------------------------------------------------------ %>  
		  
		<%
		dim sql 
		dim rs
		sql = ""
		set rs = server.CreateObject("adodb.Recordset")
		sql = "Select * from PERSONA where ID_PERSONA = " & sIdPers
'PL-SQL * T-SQL  
'SQL = TransformPLSQLToTSQL (SQL) 
		set rs = CC.execute(sql)
		%>
		<input type="hidden" id="text1" name="txtfechaactual" value="<%=ConvDateToString(Date())%>">
		<input type="hidden" id="text1" name="txtfechanac" value="<%=ConvDateToString(rs("DT_NASC"))%>">
		<input type="hidden" id="text1" name="txtIdPers" value="<%=Request.Form("IdPers")%>">
		<% set rs = nothing %>

<%' Porque ? ----------------------------------------------------------------------------------------- %>  
		
		<%
		if sResDeriv = "02" then
			%>
			<tr>
				<td align="left" colspan="2" nowrap="nowrap" class="tbltext1">
					<strong>&nbsp;&nbsp;Por Que?:</strong>
				</td>
					    
				<td align="left" colspan="2" width="60%" >
					<input type="text" name="txtporque" style="TEXT-TRANSFORM: uppercase;" class="textblack" maxlength="400" size="50" value="<%=sPorQue%>">
				</td>
			</tr>
			<%
		End if	
		%>

<% 'Observaciones ----------------------------------------------------------------------------------- %>
		
		<tr>
			<td align="left" colspan="2" nowrap="nowrap" class="tbltext1"><b>
				&nbsp;&nbsp;Observaciones </b>		
			</td>
			
			<td align="left">
						
				<textarea OnKeyUp="JavaScript:CheckLenTxArea(document.frmDeriv.txtObservaciones,document.frmDeriv.textNumCaratteri,4000);" name="txtObservaciones" cols="30" rows="7"><%=sObservaciones%></textarea>
						
				<!--input type="text" name="txtobservaciones" size="3" value ="4000" readonly>-->
				<%
				if sObservaciones <> "" then
					largo = 4000-len(sObservaciones)
				else
					largo = 4000
				end if
				%>		 
				<input type="text" name="textNumCaratteri" value="<%=cstr(largo)%>" size="3" readonly>
			</td>
		</tr>  



<%' ENTERIOR   Y   ENVIAR ---------------------------------------------------------------------------- %>  









</table>

<br><br><br>

<table WIDTH="500" BORDER="0" CELLPADDING="0" CELLSPACING="0">
	<tr align="center">
		<td>
			<a href="javascript:document.FrmVolver.submit();"><img src="<%=Session("progetto")%>/images/Indietro.gif" border="0"></a>
			<input type="image" name="Invia" src="<%=Session("Progetto")%>/images/conferma.gif" border="0" value="Conferma"> 				
			<!--<a href="javascript:Chiudi()" id="cmdCancella"><img src="<%=session("Progetto")%>/images/chiudivalidita.gif" value="chiudi" border="0"></a>-->
		</td>	
	</tr>
</table>

</form>

<form name="FrmVolver" action="UTE_VisDeriv.asp" method=post>
<input type="hidden" name="IdPers" value="<%=sIdPers%>">
</form>








<!--#include Virtual ="/Include/closeconn.asp"-->
<!--#include Virtual="/strutt_coda2.asp"-->
