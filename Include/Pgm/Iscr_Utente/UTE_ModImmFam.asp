<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include Virtual = "/strutt_testa2.asp"-->
<!--#include Virtual = "/include/OpenConn.asp"-->
<!--#include Virtual = "/include/ControlDateVB.asp"-->
<!--#include Virtual = "/include/DecCod.asp"-->
<!--#include Virtual="/util/dbUtil.asp"-->

<script LANGUAGE="Javascript">
//include del file per fare i controlli sulla validit� delle date
<!--#include Virtual = "/Include/ControlDate.inc"-->

//include del file per fare i controlli sulla numericit� dei campi
<!--#include Virtual = "/Include/ControlNum.inc"-->

//include del file per fare i controlli sulle stringhe
<!--#include Virtual = "/include/ControlString.inc"-->
<!--#include Virtual = "/include/help.inc"-->


function ControllaDati(frmMod){
	
	//Cognome
	frmModDatiIscri.txtCognome.value=TRIM(frmModDatiIscri.txtCognome.value)
	if ((frmModDatiIscri.txtCognome.value == "")||
		(frmModDatiIscri.txtCognome.value == " ")){
		alert("El apellido es obligatorio")
		frmModDatiIscri.txtCognome.focus() 
		return false
	}
	
	sCognome=ValidateInputStringWithOutNumber(frmModDatiIscri.txtCognome.value)
	if (sCognome==false){
		alert("Apellido erroneo")
		frmModDatiIscri.txtCognome.focus() 
		return false
	}	
				
	//Nome
	frmModDatiIscri.txtNome.value=TRIM(frmModDatiIscri.txtNome.value)
	if ((frmModDatiIscri.txtNome.value == "")||
		(frmModDatiIscri.txtNome.value == " ")){
		alert("Nombre obligatorio")
		frmModDatiIscri.txtNome.focus() 
		return false
	}
		
	sNome=ValidateInputStringWithOutNumber(frmModDatiIscri.txtNome.value)
	if (sNome==false){
		alert("Nombre erroneo")
		frmModDatiIscri.txtNome.focus() 
		return false
	}	

	//Data di Nascita
	if (frmModDatiIscri.txtNascita.value == ""){
		alert("Fecha de Nacimiento obligatoria")
		frmModDatiIscri.txtNascita.focus() 
		return false
	}
	//Controllo della validit� della Data di Nascita
	frmModDatiIscri.txtNascita.value=TRIM(frmModDatiIscri.txtNascita.value)
	DataNascita = frmModDatiIscri.txtNascita.value
	if (!ValidateInputDate(DataNascita)){
		frmModDatiIscri.txtNascita.focus() 
		return false
	}

	DataOdierna=frmModDatiIscri.txtoggi.value
	if (ValidateRangeDate(DataOdierna,DataNascita)==true){
	  	alert("La fecha de nacimiento debe ser inferior a la actual")
		frmModDatiIscri.txtNascita.focus()
		return false
	}	
	
	CognomeIns = frmModDatiIscri.txtCognome.value.toUpperCase()
	NomeIns = frmModDatiIscri.txtNome.value.toUpperCase()
	DtNascIns = frmModDatiIscri.txtNascita.value.toUpperCase()
	SessoIns = frmModDatiIscri.cmbSesso.value.toUpperCase()
		
	PersCognome = frmModDatiIscri.txtPersCognome.value.toUpperCase()
	PersNome = frmModDatiIscri.txtPersNome.value.toUpperCase()
	PersDtNasc = frmModDatiIscri.txtPersDataNasc.value.toUpperCase()
	PersSesso = frmModDatiIscri.txtPersSesso.value.toUpperCase()
		
	if ((CognomeIns == PersCognome)&&(NomeIns == PersNome)&&(DtNascIns == PersDtNasc)&&(SessoIns == PersSesso))
	{
		alert("No puede reingresar el mismo familiar!")			
		return false
	}
	
	if (frmModDatiIscri.cmbStatoNasc.value == ""){
		alert("Lugar de nacimiento obligatorio")
		frmModDatiIscri.cmbStatoNasc.focus() 
		return false
	}	
	
	if (frmModDatiIscri.cmbCittadinanza.value == ""){
		alert("La ciudadania es obligatoria")
		frmModDatiIscri.cmbCittadinanza.focus() 
		return false
	}
	
	if (frmModDatiIscri.cmbGradoParent.value == ""){
		alert("El parentesco es obligatorio")
		frmModDatiIscri.cmbGradoParent.focus() 
		return false
	}	
	
  
	if (frmModDatiIscri.cmbLivelloTitolo.value ==""){
		    alert("Nivel educativo obligatorio!")
		    frmModDatiIscri.cmbLivelloTitolo.focus()
		    return false
	}
	    
	return true
}

/** +++++++++++++++++++++++++++++++++++++++++++++++++
+	@autor: IPTECHONOLOGIES							*
++++++++++++++++++++++++++++++++++++++++++++++++++++*/

	function elimina() {
		var parnerName = frmModDatiIscri.txtNome.value + " " + frmModDatiIscri.txtCognome.value;
		if(confirm("Est� seguro que desea borrar el Familiar [ "+ parnerName +" ]?")){
			$.get("Del_ImmFam.asp",$("#frmModDatiIscri").serializeArray())
			.done(
				function(data){
					alert("Familiar eliminado con exito.")
						document.FrmVisImmFam.submit();
				}
			)
			.fail(function(data){
				alert("No fue posible eliminar el Familiar.")
			})
		}
	}

/** +++++++++++++++++++++++++++++++++++++++++++++++++*/

</script>
<%
PERS_COGNOME = Request.Form("txtPersCognome")
PERS_NOME = Request.Form("txtPersNome")
PERS_DT_NASC = Request.Form("txtPersDataNasc")
PERS_SESSO = Request.Form("txtPersSesso")

sIdpers = Request.Form("txtIdPersona")		
sIdFam = Request.Form("txtIdFam")

%>
<br>
<!--#include file="menu.asp"-->
<br>
<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
	<tr height="18">
		<td class="sfondomenu" width="67%" height="18"><span class="tbltext0"><b>&nbsp;FAMILIARES </b></span></td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
		<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">(*) campo obligatorio</td>
	</tr>
</table>

<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
	<tr>
		<td align="left" class="sfondocomm">
			<!--Modifica dei dati personali dichiarati al momento della iscrizione-->
			Modifica los datos personales del familiar. <br>
			Seleccione una opci�n del menu para modificar los datos relacionados. <br>
			Presione <b>Enviar</b> para guardar la modificaci�n. <br> 

   	  <!--NUEVO AGREGADO POR F.BASANTA -->
  	  En este m�dulo se deber� ingresar los datos correspondientes a las personas que conforman el  
	  <b>n�cleo familiar</b>
	  <br>
      <img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0" onclick="Javascript:window.open('ayuda_Familiares.htm','Ayuda','width=500,height=350,Resize=No,Scrollbars=yes')">


<!--		<a href="Javascript:Show_Help('/Pgm/help/Iscr_Utente/UTE_ModImmFam')">
		<img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
-->		
		</td>
	</tr>
	<tr height="2">
		<td class="sfondocomm" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif">
		</td>
	</tr>
</table>
	
<%
dim rsPers
sSQL = "SELECT ID_FAMILIARI,ID_PERSONA,COGNOME,NOME," &_
	   "SESSO,DT_NASC,STAT_NASC,STAT_CIT,COD_GRPAR," &_
	   "COD_STFAM,COD_LIV_STUD,FL_CARICO,DT_TMST,FL_DISC " &_
	   "FROM PERS_FAMILIARI WHERE ID_FAMILIARI = " & sIDFam
	   

'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
set rsPers = CC.Execute(sSQL)
if rsPers.EOF then
%>
	
	<table border="0" cellspacing="2" cellpadding="1" width="500">
		<tr align="middle">
			<td class="tbltext3">
				<b>No es posible acceder a la informaci�n de la persona.</b>
			</td>
		</tr>
	</table>
	
<%else%>
   	
	<form method="post" name="frmModDatiIscri" id="frmModDatiIscri" action="UTE_CnfModImmFam.asp">

	<input type="hidden" name="txtPersCognome" value="<%=PERS_COGNOME%>">
	<input type="hidden" name="txtPersNome" value="<%=PERS_NOME%>">
	<input type="hidden" name="txtPersDataNasc" value="<%=PERS_DT_NASC%>">
	<input type="hidden" name="txtPersSesso" value="<%=PERS_SESSO%>">
	<input type="hidden" name="txtIdFam" value="<%=sIDFam%>">
	<input type="hidden" name="txtIdPers" value="<%=rsPers("ID_PERSONA")%>">
	<input type="hidden" name="txtTMST" value="<%=rsPers("DT_TMST")%>">
	<input type="hidden" name="txtoggi" value="<%=ConvDateToString(Date())%>">
	<input type="hidden" name="txtFlDisc" value="<%=rsPers("Fl_Disc")%>">
	
	<table border="0" cellpadding="1" cellspacing="2" width="500">
	    <tr>
			<td align="middle" colspan="2" nowrap class="tbltext1">
				<p align="left">
					<strong>Apellido*</strong>
				</p>
			</td>
			<td align="left" colspan="2" width="60%">
				<input type="text" name="txtCognome" value="<%=UCase(rsPers("COGNOME"))%>" style="TEXT-TRANSFORM: uppercase;" class="textblacka" maxlength="50" size="35">
			</td>
	    </tr>
	    <tr>
			<td align="middle" colspan="2" nowrap class="tbltext1">
				<p align="left">
					<strong>Nombre*</strong>
				</p>
			</td>
			<td align="left" colspan="2" width="60%">
				<span class="tbltext">
					<input type="text" name="txtNome" value="<%=UCase(rsPers("NOME"))%>" style="TEXT-TRANSFORM: uppercase" class="textblacka" maxlength="50" size="35">
				</span>
			</td>
	    </tr>
		 <tr>
			<td align="middle" colspan="2" nowrap class="tbltext1">
				<p align="left">
					<strong>Sexo*</strong>
				</p>
			</td>	
			<td align="left" colspan="2" width="60%">
				<% select case rsPers("SESSO")
					case "M"%>
					    <select id="cmbSesso" name="cmbSesso" class="textblack">
							<option value="F">FEMENINO
							<option selected value="M">MASCULINO
							</option>
						</select><%
					case "F"%>
					    <select id="cmbSesso" name="cmbSesso" class="textblack">
							<option selected value="F">FEMENINO
							<option value="M">,MASCULINO
							</option>
						</select><%
					case else%>
					    <select id="cmbSesso" name="cmbSesso" class="textblack">
							<option selected>
							<option value="F">FEMENINO
							<option value="M">MASCULINO
							</option>
						</select><%
				end select%>
			</td>
	    </tr>	    	  
	    <tr>
			<td align="middle" colspan="2" nowrap class="tbltext1">
				<p align="left">
					<strong>Fecha de Nacimiento *</strong><font size="1"> <br>(dd/mm/aaaa)</font>
				</p>
			</td>	
			<td align="left" colspan="2" width="60%">
				<input type="text" name="txtNascita" readonly value="<%=ConvDateToString(rsPers("DT_NASC"))%>" style="TEXT-TRANSFORM: uppercase" class="textblacka fecha" size="10" maxlength="10">
			</td>
	    </tr>
	    <tr>
			<td align="middle" colspan="2" nowrap class="tbltext1">
				<p align="left">
					<strong>Pa�s de Nacimiento *</strong>
				</p>
			</td>	
	  		<td align="left" colspan="2" width="60%">
	  			<span class="tbltext">
				<%
				sStatoNasc = rsPers("STAT_NASC")
				if sStatoNasc <> "" then			
					'Richiamo funzione che popola la combo TIPO SEDE
					StatoNasc = "STATO|0|" & date & "|" & sStatoNasc & "|cmbStatoNasc|ORDER BY DESCRIZIONE"			
					CreateCombo(StatoNasc)
				else
					StatoNasc = "STATO|0|" & date & "|" & "|cmbStatoNasc|ORDER BY DESCRIZIONE"			
					CreateCombo(StatoNasc)				
				end if
				%>	
				</span>
			</td>
	    </tr>
		<tr>	    
	    <tr>
			<td align="middle" colspan="2" nowrap class="tbltext1">
				<p align="left">
					<strong>Ciudadania*</strong>
				</p>
			</td>	
	  		<td align="left" colspan="2" width="60%">
	  			<span class="tbltext">
				<%
				sCittadinanza = rsPers("STAT_CIT")
				if sCittadinanza <> "" then			
					'Richiamo funzione che popola la combo TIPO SEDE
					Cittadinanza = "STATO|0|" & date & "|" & sCittadinanza & "|cmbCittadinanza|ORDER BY DESCRIZIONE"			
					CreateCombo(Cittadinanza)
				else
					Cittadinanza = "STATO|0|" & date & "|" & "|cmbCittadinanza|ORDER BY DESCRIZIONE"			
					CreateCombo(Cittadinanza)				
				end if
				%>	
				</span>
			</td>
	    </tr>
	   <tr>
			<td>&nbsp;</td>
		</tr>
		<tr height="2">
			<td width="100%" colspan="4" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
		</tr>	    
		<tr>
			<td>&nbsp;</td>
	   </tr>
	    <tr>
			<td align="middle" colspan="2" nowrap class="tbltext1">
				<p align="left">
					<strong>Parentesco*</strong>
				</p>
			</td>	
	  		<td align="left" colspan="2" width="60%">
	  			<span class="tbltext">
				<%
				'INIZIO AM 02/12/04
		
				set Rs = server.CreateObject("ADODB.recordset")
		
				sSql= "select stat_cit, stat_cit2 from persona where id_persona =" & sIdpers
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
				Rs.Open sSql,CC,1
				sGradoParent = rsPers("COD_GRPAR")

				IF Rs("stat_cit") = "IT" or  Rs("stat_cit2") = "IT"  THEN
				'FINE AM
					'if sGradoParent <> "" then			
						GradoParent = "GRPAR|0|" & date & "|" & sGradoParent & "|cmbGradoParent|ORDER BY DESCRIZIONE"			
					'	CreateCombo(GradoParent)
					'else
					'	GradoParent = "GRPAR|0|" & date & "|" & "|cmbGradoParent|ORDER BY DESCRIZIONE"			
					'	CreateCombo(GradoParent)			
					'end if
				'INIZIO AM
				ELSE 
					GradoParent = "GRPAR|0|" & date & "|" & sGradoParent & "|cmbGradoParent||ORDER BY DESCRIZIONE"
				END IF 
				rs.close	
				set rs = nothing
				CreateCombo(GradoParent)	
				'FINE AM	
				%>	
				</span>
			</td>
	    </tr>
	    <tr>
			<td align="middle" colspan="2" nowrap class="tbltext1">
				<p align="left">
					<strong>Situaci�n Laboral</strong>
				</p>
			</td>	
	  		<td align="left" colspan="2" width="60%">
	  			<span class="tbltext">
				<%
				'sStatoFamil = rsPers("COD_STFAM")
				'if sStatoFamil <> "" then			
				'	'Richiamo funzione che popola la combo TIPO SEDE
				'	StatoFamil = "STFAM|0|" & date & "|" & sStatoFamil & "|cmbStatoFamil|ORDER BY DESCRIZIONE"			
				'	CreateCombo(StatoFamil)
				'else
				'	StatoFamil = "STFAM|0|" & date & "|" & "|cmbStatoFamil|ORDER BY DESCRIZIONE"			
				'	CreateCombo(StatoFamil)				
				'end if
				
				
				
				sStatoFamil = rsPers("COD_STFAM")
				if sStatoFamil <> "" then			
					'Richiamo funzione che popola la combo TIPO SEDE
					StatoFamil = "STDIS|0|" & date & "|" & sStatoFamil & "|cmbStatoFamil|ORDER BY VALORE ASC"			
					CreateCombo(StatoFamil)
				else
					StatoFamil = "STDIS|0|" & date & "|" & "|cmbStatoFamil|ORDER BY VALORE ASC"			
					CreateCombo(StatoFamil)				
				end if
					 
				%>	
				</span>
			</td>
	    </tr>
	    <tr>
			<td align="middle" colspan="2" nowrap class="tbltext1">
				<p align="left">
					<strong>Nivel Educativo*</strong>
				</p>
			</td>	
	  		<td align="left" colspan="2" width="60%">
	  			<span class="tbltext">
	  			
				<%
				
				sLivelloTitolo = rsPers("COD_LIV_STUD")
				if sLivelloTitolo <> "" then			
					'Richiamo funzione che popola la combo TIPO SEDE
					LivelloTitolo = "LSFAM|0|" & date & "|" & sLivelloTitolo & "|cmbLivelloTitolo' style='WIDTH: 320px'|ORDER BY VALORE"			
					CreateCombo(LivelloTitolo)
					

				else
					LivelloTitolo = "LSFAM|0|" & date & "|" & "|cmbLivelloTitolo' style='WIDTH: 320px'|ORDER BY DESCRIZIONE"			
					CreateCombo(LivelloTitolo)
				end if
				%>	
				</span>
			</td>
	    </tr>
	    <tr>
			<td align="middle" colspan="2" nowrap class="tbltext1">
				<p align="left">
					<strong>El familiar esta a su Cargo?*</strong>
				</p>
			</td>
			<td align="left" colspan="2" width="60%">
				<% select case rsPers("FL_CARICO")
				case "S"%>
					<input type="radio" id="OpbCarico" name="OpbCarico" checked value="S"><span CLASS="TBLTEXT">SI</span>
					<input type="radio" id="OpbCarico" name="OpbCarico" value="N"><span CLASS="TBLTEXT">NO</span>
				<%case "N"%>
				    <input type="radio" id="OpbCarico" name="OpbCarico" value="S"><span CLASS="TBLTEXT">SI</span>
					<input type="radio" id="OpbCarico" name="OpbCarico" checked value="N"><span CLASS="TBLTEXT">NO</span>
				<%case else%>
				    <input type="radio" id="OpbCarico" name="OpbCarico" value="S"><span CLASS="TBLTEXT">SI</span>
					<input type="radio" id="OpbCarico" name="OpbCarico" value="N"><span CLASS="TBLTEXT">NO</span>
			   <%end select%>    
			</td>
	    </tr>
	    
	    <tr>
			<td align="middle" colspan="2" nowrap class="tbltext1">
				<p align="left">
				<strong>El familiar tiene alg�n tipo de <br>Discapacidad?*</strong> 
				</p>
			</td>
			<td align="left" colspan="2" width="60%">
				<% select case rsPers("FL_DISC")
				case "S"%>
					<input type="radio" id="OpDisc" name="OpDisc" checked value="S"><span CLASS="TBLTEXT">SI</span>
					<input type="radio" id="OpDisc" name="OpDisc" value="N"><span CLASS="TBLTEXT">NO</span>
				<%case "N"%>
				    <input type="radio" id="OpDisc" name="OpDisc" value="S"><span CLASS="TBLTEXT">SI</span>
					<input type="radio" id="OpDisc" name="OpDisc" checked value="N"><span CLASS="TBLTEXT">NO</span>
				<%case else%>
				    <input type="radio" id="OpDisc" name="OpDisc" value="S"><span CLASS="TBLTEXT">SI</span>
					<input type="radio" id="OpDisc" name="OpDisc" value="N"><span CLASS="TBLTEXT">NO</span>
			   <%end select%>    
			</td>
	    </tr>
	</table>	    	    	    	    
	<table WIDTH="500">
	    <tr>
	        <td align="left" colspan="2">&nbsp;</td>
	        <td align="left" colspan="2" width="60%">&nbsp;</td>
	    </tr>
	</table>
	<br>
	<table border="0" cellpadding="1" cellspacing="1" width="500">
	<tr>
        <td align="middle" colspan="2">
			<a HREF="javascript:document.FRMIDPERSONA.submit();"><img SRC="<%=Session("Progetto")%>/images/indietro.gif" border="0"></a>
			<input type="image" src="<%=Session("Progetto")%>/images/conferma.gif" border="0" value="Modifica" onclick="return ControllaDati(this)">
        </td>
		<td>
			<a onmouseover="window.status =' '; return true" title="Elimina" href="javascript:elimina()">
						<img border="0" src="<%=Session("Progetto")%>/images/elimina.gif">
			</a>
		</td>
    </tr>
	</table>
	</form>
	<Form name="FRMIDPERSONA" method="post" action="UTE_VisImmFam.asp">
		<input type="hidden" name="IdPers" value="<%=sIdpers%>">
	</form>
<%
end if
rsPers.Close
set rsPers = nothing
%>
<!--#include virtual = "/include/CloseConn.asp"-->
<!--#include Virtual = "/strutt_coda2.asp"-->
