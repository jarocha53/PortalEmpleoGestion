<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include Virtual = "/strutt_testa2.asp"-->
<!--#include Virtual = "/include/OpenConn.asp"-->
<!--#include Virtual = "/include/DecCod.asp"-->
<!--#include Virtual = "/include/ControlDateVB.asp"-->
<%
if ValidateService(session("idutente"),"UTE_ISCRSERV", CC) <> "true" then 
	response.redirect "/util/error_login.asp"
end if
%>
<script LANGUAGE="Javascript">
//include del file per fare i controlli sulla validit� delle date
<!--#include virtual = "/include/SelComune.js"-->

//include del file per fare i controlli sulla validit� delle date
<!--#include Virtual = "/Include/ControlDate.inc"-->

//include del file per fare i controlli sulla numericit� dei campi
<!--#include Virtual = "/Include/ControlNum.inc"-->

//include del file per fare i controlli sulla validit� delle date
<!--#include Virtual = "/Include/ControlCodFisc.inc"-->

//include del file per fare i controlli sulla stringa
<!--#include Virtual = "/Include/ControlString.inc"-->
<!--#include Virtual = "/Include/help.inc"-->

function PulisciCom()
{
	document.frmIscri.txtComuneNascita.value = ""
	document.frmIscri.txtComNas.value = ""
}
function PulisciRes()
{
	document.frmIscri.txtComuneRes.value = ""
	document.frmIscri.txtComRes.value = ""
}

//Funzione per i controlli dei campi da inserire 
	function ControllaDati()
{
		

		if (document.frmIscri.txtDataNascita.value == "")
		{
			alert("El campo fecha de nacimiento es obligatorio!")
			document.frmIscri.txtDataNascita.focus() 
			return false
		}

		//Controllo della validit� della Data di Nascita
		DataNascita = document.frmIscri.txtDataNascita.value
		if (!ValidateInputDate(DataNascita))
		{
			frmIscri.txtDataNascita.focus() 
			return false
		}
		
		//Controllo della validit� della Data di Nascita
		DataNascita = frmIscri.txtDataNascita.value
		if (ValidateRangeDate(DataNascita,frmIscri.txtoggi.value)==false){
			alert("La fecha de nacimiento debe ser anterior a la fecha actual!")
			frmIscri.txtDataNascita.focus()
			return false
		}	
//***************************************************************************************		
		//Comune di Nascita e Provincia di Nascita obbligatori
		//e in alternativa con Nazione di Nascita
		document.frmIscri.txtComuneNascita.value=TRIM(document.frmIscri.txtComuneNascita.value)
		if ((document.frmIscri.txtComuneNascita.value == "")||
			(document.frmIscri.txtComuneNascita.value == " ")){ 
			if (document.frmIscri.cmbProvinciaNascita.value == ""){
				if (document.frmIscri.cmbNazioneNascita.value == ""){
					alert("El campo municipio y departamento de nacimiento o naci�n de nacimiento es obligatorio!")
					document.frmIscri.txtComuneNascita.focus() 
					return false
				}
			}	
		}
		if (document.frmIscri.txtComuneNascita.value != ""){ 
			if (document.frmIscri.cmbProvinciaNascita.value != ""){
				if(document.frmIscri.cmbNazioneNascita.value != ""){
					alert("Indicar solo el departamento y el municipio de nacimiento o bien la naci�n de nacimiento.")
					document.frmIscri.txtComuneNascita.focus() 
					return false
				}
			}	
		}
		//Se Comune di Nascita � digitato, la Provincia � obbligatoria
		if (document.frmIscri.txtComuneNascita.value != ""){
			if (document.frmIscri.cmbProvinciaNascita.value == ""){
				alert("El campo departamento de nacimiento es obligatorio!")
				document.frmIscri.cmbProvinciaNascita.focus() 
				return false
			}
		}
			//Se la Provincia di Nascita � digitata, il Comune � obbligatorio
		if ((document.frmIscri.txtComuneNascita.value == "")||
			(document.frmIscri.txtComuneNascita.value == " ")){
			if (document.frmIscri.cmbProvinciaNascita.value != ""){
				alert("El campo municipio de nacimiento es obligatorio!")
				document.frmIscri.txtComuneNascita.focus() 
				return false
			}
		}
			

		
//***************************************************************************************		
		//Sesso
	   Sesso=document.frmIscri.cmbSesso.value
		if (Sesso == "")
		{
			alert("El campo sexo es obligatorio!")
			document.frmIscri.cmbSesso.focus() 
			return false
		}	

		document.frmIscri.txtCodFisc.value=TRIM(document.frmIscri.txtCodFisc.value)
		if (document.frmIscri.txtCodFisc.value == "")
		{
			alert("El campo c�digo fiscal es obligatorio!")
			document.frmIscri.txtCodFisc.focus() 
			return false
		}
		
		//Codice Fiscale deve essere di 16 caratteri
		if (document.frmIscri.txtCodFisc.value.length != 16)
		{
			alert("Formato de c�digo fiscal err�neo!")
			document.frmIscri.txtCodFisc.focus() 
			return false
		}

	    //Contollo la validit� del Codice Fiscale
		var contrCom=""
		      Sesso = frmIscri.cmbSesso.value
		      DataNascita = frmIscri.txtDataNascita.value
		      CodFisc = frmIscri.txtCodFisc.value.toUpperCase()
		       if (document.frmIscri.txtComuneNascita.value != ""){
		           contrCom =document.frmIscri.txtComNas.value   
		         }
		      else{
		          contrCom = document.frmIscri.cmbNazioneNascita.value
		      }  
		      if (!ControllCodFisc(DataNascita,CodFisc,Sesso,contrCom))
		         {
			      alert("C�digo fiscal err�neo!")
			      document.frmIscri.txtCodFisc.focus()
			      return false
	          }
	          
	     //controllo Comune di Residenza e Provincia di residenza sono obbligatori
	     		
		document.frmIscri.txtComuneRes.value=TRIM(document.frmIscri.txtComuneRes.value)
		if ((document.frmIscri.txtComuneRes.value == "")||
			(document.frmIscri.txtComuneRes.value == " ")){ 
			if (document.frmIscri.txtComuneRes.value == ""){
				if (document.frmIscri.txtComuneRes.value == ""){
					alert("Los campos departamento y municipio de residencia son obligatorios!")
					document.frmIscri.txtComuneRes.focus() 
					return false
				}
			}	
		}
 	  
	return true	     				
}

</script>


<form action="UTE_CnfIscrServ.asp" method="post" name="frmIscri" id="frmIscri">

<table border="0" width="500" cellspacing="0" cellpadding="0" height="81">
  <tr>
    <td width="520" background="<%=Session("Progetto")%>/images/titoli/strumenti2b.gif" height="81" valign="bottom" align="right">
      <table border="0" background width="260" height="30" cellspacing="0" cellpadding="0">
        <tr>
          <td width="100%" valign="top" align="right"><b class="tbltext1a">M�dulo de Inscripci�n &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></td>
        </tr>
      </table>
    </td>
  </tr>
</table>


<table border="0" cellpadding="0" cellspacing="0" width="500">
    <tr height="18">
		<td class="sfondomenu" width="67%" height="18">
			<span class="tbltext0"><b>&nbsp;CANDIDATO</b></span></td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
		<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">(*) campi obbligatori</td>
    </tr>
    <tr>
       <td class="sfondocomm" width="100%" colspan="3"><br>
			Para acceder a los servicios del portal es necesario estar inscripto.<br> Completa la ficha Anagr�fica con tus datos personales y presiona <b>Enviar</b>.			<a href="Javascript:Show_Help('/Pgm/help/Iscr_Utente/UTE_IscrServ')">			<img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>

       </td> 
    </tr>   
    <tr height="17">
		<td colspan="3" class="sfondocomm" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">
			</td>
		</td>
    </tr>
</table>
<br>

<table border="0" cellpadding="2" cellspacing="2" width="500">
   <tr>
		<td align="left" colspan="1" nowrap class="tbltext1">
			<span class="tbltext1">
				<strong>Apellido*</strong></span>
		</td>
		<td align="left" colspan="1">
				<td class="textblacka"><b><%=session("Cognome")%></b>
				<input type="hidden" name="txtoggi" value="<%=ConvDateToString(Date())%>">
		</td>
    </tr>
    <tr align="left">
		<td align="left" colspan="1" nowrap class="tbltext1">
			<span class="tbltext1">
				<strong>Nombre*</strong></span>
			</td>
			<td align="left" colspan="1" width="30%"></td>
				<td class="textblacka"><b><%=session("Nome")%></b>
			</td>
    </tr>
    <tr>
		<td align="left" colspan="2" nowrap class="tbltext1">	
				<span class="tbltext1">					
				<strong>Fecha de nacimiento*</strong>(gg/mm/aaaa)								
		</td>	
		<td align="left" colspan="2" width="60%">
				<input class="textblacka" style="TEXT-TRANSFORM: uppercase; WIDTH: 80px; HEIGHT: 22px" size="35" maxlength="10" name="txtDataNascita">			
		</td>
    </tr>
    <tr>
		<td height="2" align="left" colspan="4" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
    </tr>
    
    <tr>
			<td height="20" align="left" colspan="4" class="textblack">
			Ingrese aqu� el municipio y el departamento de nacimiento si es nacido en Argentina
			</font>
			</td>
	    </tr>
	    
	   <tr>
	        <td align="left" colspan="2" class="tbltext1">
				<span class="tbltext1">
					<strong>Departamento de nacimiento</strong>
			 </span></td>
	        <td align="left" colspan="2" width="60%">
				<%	sInt = "PROV|0|" & date & "| |cmbProvinciaNascita' onchange='PulisciCom()|ORDER BY DESCRIZIONE"			
					CreateCombo(sInt)  	%>
	         </td>
	    </tr> 
	  <tr>		
				<td align="left" colspan="2" nowrap class="tbltext1">
						<b>Municipio de nacimiento</b>
						
				</td>
				<td nowrap>
					<span class="tbltext">
					<input type="text" name="txtComuneNascita" style="TEXT-TRANSFORM: uppercase;" class="textblacka" size="35" readonly>
					<input type="hidden" id="txtComNas" name="txtComNas">
					
	<%
					NomeForm="frmIscri"
					CodiceProvincia="cmbProvinciaNascita"
					NomeComune="txtComuneNascita"
					CodiceComune="txtComNas"
					Cap="NO"
	%>
					<a href="Javascript:SelComune('<%=NomeForm%>','<%=CodiceProvincia%>','<%=NomeComune%>','<%=CodiceComune%>','<%=Cap%>')" ID="imgPunto1" name="imgPunto1" onmouseover="javascript:window.status='';return true"><img border="0" src="<%=Session("Progetto")%>/images/bullet1.gif"></a>
					</span>				
				</td>
			</tr>	
	    
	    <tr>
	    <td height="2" align="middle" colspan="4" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
	    </tr>
	    <tr>
			<td height="20" align="left" colspan="4" class="textblack">
			Si en cambio es nacido en un pa�s extranjero seleccione la denominaci�n  
			</font>
			</td>
	    </tr>

	    <tr>
	        <td align="left" colspan="2" class="tbltext1">
					<span class="tbltext1">
					<strong>Naci�n de nacimiento</strong></span>
	        </td>
	       
	        <td align="left" colspan="2" width="60%">
	        <%			set rsComune = Server.CreateObject("ADODB.Recordset")

						sSQL = "SELECT CODCOM,DESCOM from COMUNE WHERE CODCOM like 'Z%' " &_
							"ORDER BY DESCOM"

'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
						rsComune.Open sSQL, CC
				
						Response.Write "<SELECT class=textblacka ID=cmbNazioneNascita name=cmbNazioneNascita><OPTION value=></OPTION>"

						do while not rsComune.EOF 
							Response.Write "<OPTION "

							Response.write "value ='" & rsComune("CODCOM") & _
								"'> " & rsComune("DESCOM")  & "</OPTION>"
								rsComune.MoveNext 
						loop
		
						Response.Write "</SELECT>"

						rsComune.Close			%>
	        </td>
		</tr>
    
    
    
    
	<tr>
		<td align="middle" colspan="2" nowrap class="tbltext1">
			<p align="left">		
				<strong>Sesso*</strong>
				<strong>&nbsp;</strong>
			</p>
		</td>
		<td align="left" colspan="2" width="60%">
		<% select case sesso
			case "M"%>
			    <select class="textblacka" id="cmbSesso" name="cmbSesso">
					<option value="F">FEMMINILE
					<option selected value="M">MASCHILE
					</option>
				</select><%
			case "F"%>
			    <select class="textblacka" id="cmbSesso" name="cmbSesso">
					<option selected value="F">FEMMINILE
					<option value="M">MASCHILE
					</option>
				</select><%
			case else%>
			    <select class="textblacka" id="cmbSesso" name="cmbSesso">
					<option selected>
					<option value="F">FEMMINILE
					<option value="M">MASCHILE
					</option>
				</select><%
			end select%>    
		</td>
	</tr>
	<tr>
	    <td align="left" colspan="2" class="tbltext1">
	  		<span class="tbltext1">
	  		<strong>C�digo fiscal*</strong>
	  		</span>
	    </td>
	    <td align="left" colspan="2" width="60%">
	  		<input class="textblacka" style="TEXT-TRANSFORM: uppercase; WIDTH:   167px; HEIGHT: 22px" size="11" maxlength="16" name="txtCodFisc">
	    </td>
	</tr>
<!--29/05/2003-->
	<tr>
		<td height="2" align="left" colspan="4" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
	</tr>	
	<tr>
		<td height="20" align="left" colspan="4" class="tbltext1">
			<b>Residencia</b>   
		</td>
    </tr>    
	<tr>
		<td align="left" colspan="2" class="tbltext1">
			<span class="tbltext1">
				<strong>Departamento de residencia*</strong>
			</span>
		</td>
        <td align="left" colspan="2" width="60%">
				<%
				sInt = "PROV|0|" & date & "| |cmbProvRes' onchange='PulisciRes()|ORDER BY DESCRIZIONE"			
				CreateCombo(sInt)
				%>
        </td>
    </tr>
     <tr>		
		<td align="left" colspan="2" nowrap class="tbltext1">
			<b>Municipio de residencia*</b>
		</td>
		<td nowrap>
			<span class="tbltext">
				<input type="text" name="txtComuneRes" style="TEXT-TRANSFORM: uppercase;" class="textblacka" size="35" readonly value="<%=DescrComuneResid%>">
				<input type="hidden" id="txtComRes" name="txtComRes" value="<%=COM_RES%>">
				
<%
				NomeForm="frmIscri"
				CodiceProvincia="cmbProvRes"
				NomeComune="txtComuneRes"
				CodiceComune="txtComRes"
				Cap="NO"
%>
				<a href="Javascript:SelComune('<%=NomeForm%>','<%=CodiceProvincia%>','<%=NomeComune%>','<%=CodiceComune%>','<%=Cap%>')" ID="imgPunto1" name="imgPunto1" onmouseover="javascript:window.status='';return true"><img border="0" src="<%=Session("Progetto")%>/images/bullet1.gif"></a>
			</span>				
		</td>
	</tr>
	<tr>
		<td height="2" align="left" colspan="4" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
	</tr>	
</table>   
<br>
<br>
<table border="0" cellpadding="1" cellspacing="1" width="500" align="center">
	 <tr>
        <td align="middle" colspan="2">
        <input type="image" name="Invia" src="<%=Session("Progetto")%>/images/conferma.gif" border="0" value="Conferma" onclick="return ControllaDati(this)">
        </td>
     </tr>
</table>
</form>

<!--#include Virtual = "/include/closeconn.asp"-->
<!--#include Virtual="/strutt_coda2.asp"-->
