<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include Virtual = "/strutt_testa2.asp"-->
<!--#include Virtual = "/include/OpenConn.asp"-->
<!--#include Virtual = "/include/ControlDateVB.asp"-->
<!--#include Virtual = "/include/DecComun.asp"-->

<script LANGUAGE="Javascript">

<!--#include virtual = "/include/SelComune.js"-->

//include del file per fare i controlli sulla validit� delle date
<!--#include Virtual = "/Include/ControlDate.inc"-->

//include del file per fare i controlli sulla numericit� dei campi
<!--#include Virtual = "/Include/ControlNum.inc"-->

//include del file per fare i controlli sulle stringhe
<!--#include Virtual = "/Include/ControlString.inc"-->
<!--#include Virtual = "/Include/help.inc"-->

function ControllaDati(frmMod){
	//-------- Controlli di OBBLIGATORIETA' --------
	//-------- Controlli FORMALI            --------
	//-------- Controlli di RELAZIONE       --------
	frmModDatiDocPers.txtNDoc.value=TRIM(frmModDatiDocPers.txtNDoc.value)
	DataRil = frmModDatiDocPers.txtDataRil.value
	DataOdierna=frmModDatiDocPers.txtoggi.value
	DataNasc=frmModDatiDocPers.txtDtNasc.value
	
	if (frmModDatiDocPers.txtNDoc.value=="")
	{
		alert ("Numero del documento Obbligatorio")
		frmModDatiDocPers.txtNDoc.focus() 
		return false
	}
	
	if (DataRil == "") 
	{
		alert ("Indicare la data di Rilascio")
		frmModDatiDocPers.txtDataRil.focus() 
		return false
	}

	if (!ValidateInputDate(DataRil)){
		frmModDatiDocPers.txtDataRil.focus() 
		return false
	}	
		
	if (ValidateRangeDate(DataRil,DataOdierna)==false){
		alert("La data di rilascio non puo' essere successiva alla data odierna!")
		frmModDatiDocPers.txtDataRil.focus()
	    return false
	}
		
	if (ValidateRangeDate(DataRil,DataNasc)==true){
		alert("La data di rilascio deve essere successiva alla data di nascita!")
		frmModDatiDocPers.txtDataRil.focus()
	    return false
	}

	
	DataScad = frmModDatiDocPers.txtDataScad.value
	if (DataScad == "") 
	{
		alert ("Indicare la data di Scadenza")
		frmModDatiDocPers.txtDataScad.focus() 
		return false
	}

	if (!ValidateInputDate(DataScad)){
		frmModDatiDocPers.txtDataScad.focus() 
		return false
	}
			
	if (ValidateRangeDate(DataOdierna,DataScad)==false){
	    alert("La data di scadenza non pu� essere inferiore alla data odierna!")
		frmModDatiDocPers.txtDataScad.focus()			
	    return false
	   }

	frmModDatiDocPers.txtComune.value=TRIM(frmModDatiDocPers.txtComune.value)
	
	if ((frmModDatiDocPers.cmbProvRil.value == "") &&
		(frmModDatiDocPers.cmbNazione.value == "")){
		alert("Indicare il luogo di Rilascio del documento")
		frmModDatiDocPers.cmbProvRil.focus() 
		return false
	}
			
	if ((frmModDatiDocPers.txtComune.value == "")||
		(frmModDatiDocPers.txtComune.value == " ")){ 
		if (frmModDatiDocPers.cmbProvRil.value == ""){
			if (frmModDatiDocPers.cmbNazione.value == ""){
				alert("I campi Comune e Departamento di Rilascio o Nazione di Rilascio sono obbligatori!")
				frmModDatiDocPers.txtComune.focus() 
				return false
			}
		}	
	}
	if (frmModDatiDocPers.txtComune.value != ""){ 
		if (frmModDatiDocPers.cmbProvRil.value != ""){
			if(frmModDatiDocPers.cmbNazione.value != ""){
				alert("Indicare solo il Comune e la Departamento di Rilascio o solo la Nazione di Rilascio.")
				frmModDatiDocPers.txtComune.focus() 
				return false
			}
		}	
	}
	//Se Comune di Nascita � digitato, la Provincia � obbligatoria
	if (frmModDatiDocPers.txtComune.value != ""){
		if (frmModDatiDocPers.cmbProvRil.value == ""){
			alert("Il campo Departamento di Rilascio � obbligatorio!")
			frmModDatiDocPers.cmbProvRil.focus() 
			return false
		}
	}
	//Se la Provincia di Nascita � digitata, il Comune � obbligatorio
	if ((frmModDatiDocPers.txtComune.value == "")||
		(frmModDatiDocPers.txtComune.value == " ")){
		if (frmModDatiDocPers.cmbProvRil.value != ""){
			alert("Il campo Comune di Rilascio � obbligatorio!")
			frmModDatiDocPers.txtComune.focus() 
			return false
		}
	}
}
function Pulisci()
{
	frmModDatiDocPers.cmbNazione.selectedIndex=0;
	frmModDatiDocPers.txtComune.value = "";
	frmModDatiDocPers.txtComuneHid.value = ""
}		


function PulEstero()
{
  if (frmModDatiDocPers.cmbNazione.value!="") {
	frmModDatiDocPers.cmbProvRil.selectedIndex=0;
	frmModDatiDocPers.txtComune.value = "";
	frmModDatiDocPers.txtComuneHid.value = ""
	}
}		
</script>

<!--#include Virtual = "/include/DecCod.asp"-->
<!--#include Virtual="/util/dbUtil.asp"-->

<%

dim sIDPersona
dim sCodPers 
dim sIdpers
sCodPers =Request("Codice")
sIdpers=request("txt_idpers")
%>
<br>	
<!--#include file="menu.asp"-->
<br>
<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
	<tr height="18">
		<td class="sfondomenu" width="67%" height="18"><span class="tbltext0"><b>&nbsp;DOCUMENTI PERSONALI</b></span></td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
		<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif">(*) campi obbligatori</td>
	</tr>
</table>
<!-- Commento -->
<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
	<tr>
		<td align="left" class="sfondocomm">
			Modifica delle informazioni relative al documento personale. <br>
			Premere <b>Invia</b> per salvare le modifiche. 
			<a href="Javascript:Show_Help('/Pgm/help/Iscr_Utente/UTE_ModDocPers')">
			<img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
		</td>
	</tr>
	<tr height="2">
		<td class="sfondocomm" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif">
		</td>
	</tr>
</table>	
<%
sSQL = "SELECT A.COM_RIL, A.ID_PERSONA, A.COD_DOCST, A.ID_DOC, A.COD_ENTE_RIL, A.DT_RIL_DOC," &_
       " A.DT_FIN_VAL, A.COD_STATO_RIL, A.COM_RIL, A.PRV_RIL, A.DT_TMST, P.DT_NASC " &_
       " FROM PERS_DOC A, PERSONA P  WHERE A.ID_PERSONA=P.ID_PERSONA  AND A.ID_PERSONA=" & sIdpers &_ 
       " AND COD_DOCST =" & sCodPers
 
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
set rsDocPers = CC.Execute(sSQL)

if rsDocPers.EOF then
%>	
	<table border="0" cellspacing="2" cellpadding="1" width="500" align="center">
		<tr align="middle">
			<td class="tbltext3">
				<b>Non � stato possibile accedere alle informazioni richieste</b>
			</td>
		</tr>
	</table>
<%else
	
	sIDPersona = clng(rsDocPers("ID_PERSONA"))
   	dtNasc = convdatetostring(rsDocPers("DT_NASC"))
   	sAppo = DecCodValidita("DOCST",0,rsDocPers("DT_TMST"),rsDocPers("COD_DOCST"),0)
	'Response.Write sValore
	sAppo1=split(sAppo,"|")
	sValore=sAppo1(0)
	sValidita=sAppo1(1)
%>
<form method="post" name="frmModDatiDocPers" onsubmit="return ControllaDati(this)" action="UTE_CnfModDocPers.asp">
	<input type="hidden" name="txtIdPers" value="<%=sIDPersona%>">
	<input type="hidden" name="txtTMST" value="<%=rsDocPers("DT_TMST")%>">
	<input type="hidden" name="txtDtNasc" value="<%=dtNasc%>">
	<input type="hidden" id="text1" name="txtTipoDoc" value="<%=rsDocPers("COD_DOCST")%>">
	<input type="hidden" id="text1" name="txtNDocOld" value="<%=UCase(rsDocPers("ID_DOC"))%>">	

	<table border="0" cellpadding="0" cellspacing="1" width="500">
		<tr>
			<td align="middle" colspan="2" nowrap class="tbltext1">
				<p align="left">
					<strong>&nbsp;</strong>
					<strong>Tipo Documento </strong>
					<strong>&nbsp;</strong>
				</p>
			</td>
			<td align="left" colspan="2" width="60%">
				<span class="textblack"><b>
					<%=sValore%>
					</b>
				</span>
			</td>
		</tr>    
		<tr>
			<td align="middle" colspan="2" nowrap class="tbltext1">
				<p align="left">
					<strong>&nbsp;</strong>
					<strong>N� Documento *</strong>
					<strong>&nbsp;</strong>
				</p>
			</td>
			<td align="left" colspan="2" width="60%">
				<span class="tbltext">
					<input class="textblacka" style="TEXT-TRANSFORM: uppercase" maxlength="20" type="text" name="txtNDoc" value="<%=UCase(rsDocPers("ID_DOC"))%>" size="25">
				</span>
			</td>
	    </tr>
		<tr>     
			<td align="middle" colspan="2" nowrap class="tbltext1">
				<p align="left">
					<strong>&nbsp;</strong>
					<strong>Rilasciato il *</strong><font size="1"> <br>&nbsp;&nbsp;(gg/mm/aaaa)</font>
					<strong>&nbsp;</strong>
				</p>
			</td>	
			<td align="left" colspan="2" width="60%">
				<input class="textblacka" type="text" style="TEXT-TRANSFORM: uppercase" name="txtDataRil" value="<%=ConvDateToString(rsDocPers("DT_RIL_DOC"))%>" size="10">
				<input type="hidden" name="txtoggi" value="<%=ConvDateToString(Date())%>">
			</td>
	    </tr>
	    <tr>     
			<td align="middle" colspan="2" nowrap class="tbltext1">
				<p align="left">
					<strong>&nbsp;</strong>
					<strong>Valido fino al *</strong><font size="1"> <br>&nbsp;&nbsp;(gg/mm/aaaa)</font>
					<strong>&nbsp;</strong>
				</p>
			</td>	
			<td align="left" colspan="2" width="60%">
				<input type="text" class="textblacka" style="TEXT-TRANSFORM: uppercase" name="txtDataScad" value="<%=ConvDateToString(rsDocPers("DT_FIN_VAL"))%>" size="10">
			</td>
		<tr>
			<td colspan="4">
				&nbsp;
			 </td>
		</tr>		    
		<tr>
			<td height="2" align="left" colspan="4" background="<%=Session("Progetto")%>/images/separazione.gif">
		</tr>
		<tr>
			<td colspan="4">
				&nbsp;
			</td>
		</tr>
		<tr>
			<td colspan="4">
				<p align="left">
					<span class="textblack">
						<strong>&nbsp;</strong>
						<strong>Rilasciato da:</strong>
						<strong>&nbsp;</strong>
						<br>
						<br>
					</span>
				</p>
			 </td>
		</tr>			
	    <tr>
			<td align="middle" colspan="2" nowrap class="tbltext1">
				<p align="left">
					<strong>&nbsp;</strong>
					<strong>Ente</strong>
					<strong>&nbsp;</strong>
				</p>
			</td>
	        <td align="left" colspan="2" width="60%">
				<!--input class="textblacka" type="text" style="TEXT-TRANSFORM: uppercase;" value="<%=rsDocPers("COD_ENTE_RIL")%>" name="idente" maxlength="50">	        </td-->
<%
					sInt = "ENRIL|0|" & date & "|" & rsDocPers("COD_ENTE_RIL") & "|cmbEnteRil|ORDER BY DESCRIZIONE"	
					CreateCombo(sInt)				
%>					
			</td>
		</tr>
		<tr>
			<td colspan="4">
				<span class="textblack"><br>
					&nbsp;&nbsp;Indicare di seguito il luogo di Rilascio del documento.<br>
					&nbsp;&nbsp;<br>
					&nbsp;&nbsp;Se rilasciato da <b>Autorit� Nazionali</b> indicare:<br><br>
				</span>
			</td>
		</tr>
		<tr>
			<td align="middle" colspan="2" class="tbltext1">
				<p align="left">
					<strong>&nbsp;</strong>
					<strong>Departamento di</strong>
					<strong>&nbsp;</strong>
				</p>
			</td>	
			<td align="left" colspan="2" width="60%">
	  			<span class="tbltext">
<%
					dim sProv
					sProv = rsDocPers("PRV_RIL")
					
					sInt = "PROV|0|" & date & "|" & sProv & "|cmbProvRil' onchange='javascript:Pulisci()|ORDER BY DESCRIZIONE"	
				
					CreateCombo(sInt)
				
					DescrComuneResid = DescrComune(rsDocPers("COM_RIL"))					
					if  DescrComuneResid ="0" then
						DescrComuneResid = ""				
					end if 
%>					
				</span>
			</td>
		</tr>
		<tr>
			<td align="left" colspan="2" class="tbltext1">
				<p align="left">
					<strong>&nbsp;</strong>
					<strong>Comune di</strong>
					<strong>&nbsp;</strong>
				</p>
	        </td>
	        <td align="left" colspan="2" width="60%" nowrap>
				<span class="tbltext">
					<input type="text" name="txtComune" style="TEXT-TRANSFORM: uppercase;" class="textblacka" size="35" value="<%=DescrComuneResid%>">
					<input type="hidden" id="txtComuneHid" name="txtComuneHid" value="<%=rsDocPers("COM_RIL")%>">
<%
					NomeForm="frmModDatiDocPers"
					CodiceProvincia="cmbProvRil"
					NomeComune="txtComune"
					CodiceComune="txtComuneHid"
					Cap="NO"					
%>
					<a href="Javascript:SelComune('<%=NomeForm%>','<%=CodiceProvincia%>','<%=NomeComune%>','<%=CodiceComune%>','<%=Cap%>')" ID="imgPunto1" name="imgPunto1" onmouseover="javascript:window.status='';return true"><img border="0" src="<%=Session("Progetto")%>/images/bullet1.gif"></a>
				</span>								
			</td>
		</tr>
		<tr>
			<td colspan="4">
				<span class="textblack"><br>&nbsp;&nbsp;o in alternativa:<br><br>
			</td>
		</tr>		
		<tr>
			<td align="left" colspan="2" class="tbltext1">
				<p align="left">
					<strong>&nbsp;</strong>
					<strong>Nazione</strong>
					<strong>&nbsp;</strong>
				</p>
			</td>   
	        <td align="left" colspan="2" width="60%">
				<span class="tbltext" nowrap>
<%
					dim rsNazione
					dim descNazione
					sNazione = rsDocPers("COD_STATO_RIL")
				
					sInt = "STATO|0|" & date & "|" & sNazione & "|cmbNazione' onchange='javascript:PulEstero()|AND CODICE <> 'XX' ORDER BY DESCRIZIONE"			
					CreateCombo(sInt)				
%>	
				</span>
			</td>
		</tr>
		<tr>
			<td colspan="4">
				&nbsp;
			</td>
		</tr>
		<tr>
			<td colspan="4">
				&nbsp;
			</td>
		</tr>		  
	</table>

	<table border="0" cellpadding="1" cellspacing="1">
		<tr>
			<td align="middle">
				<a href="javascript:document.indietro.submit()"><img src="<%=Session("Progetto")%>/images/indietro.gif" border="0">
				</a>
				<%if sValidita="0" then%>
				<input type="image" src="<%=Session("Progetto")%>/images/conferma.gif" border="0" value="Modifica" onclick="return ControllaDati(this)">
				<%end if%>
		    </td>
		</tr>
	</table>
</form>
<form name="indietro" action="UTE_VisDocPers.asp" method="post">
	<input type="hidden" name="IdPers" value="<%=sIdpers%>">			
</form>
 <%
 
end if
rsDocPers.Close
set rsDocPers = nothing
 %>
<!--#include virtual = "/include/CloseConn.asp"-->
<!--#include Virtual = "/strutt_coda2.asp"-->
			  
