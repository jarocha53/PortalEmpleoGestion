<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include Virtual = "/strutt_testa2.asp"-->
<!--#include Virtual = "/include/OpenConn.asp"-->
<!--#include Virtual = "/include/DecComun.asp"-->
<!--#include Virtual = "/include/ControlDateVB.asp"-->
<!--#INCLUDE VIRTUAL= "IncludeMtss/adovbs.asp"-->
<!--#include Virtual = "/include/VerificaHistoriaLaboralDatos.asp"-->
<%
'------------------------
' Variables usadas
'------------------------
	dim bCondicionJefes
	dim rsOperatore
	dim sOpeCognome
	dim sOpeNome
	dim sCognome
	dim sNome
	dim dDataNascita
	dim sCodFisc
	dim	sComNasc 
	dim	sPrvNasc 
	dim	sStatoNasc
	dim sLuogoCPI

	'set obDB=new InterfaceBD

	dim Tarea
	dim OficinaId
	dim UsuarioId
	Dim Status
	dim Mensaje
	dim sEstadoDesc
	dim fFirma
	dim fFin

	dim HayDatosdeProximaCita
	dim Cierra

'=============================================================================
'	Lee Datos externos (de paginas llamadoras)
'=============================================================================
' Identificador de persona analizada
sIdpers = CLng(Request("IdPers"))

' Otros datos de control de tareas
Cierra  = (Request("Cierra"))
Tarea  = (Request("Tarea"))
formaction= (Request("formaction"))
' variables de session
Oficina = session("creator")
Usuario = Session("idutente")

Idpersona=sIdpers
FlAna=0	'????

if len(Tarea)= 0 then Tarea = "0"
If len(Cierra) = 0 then Cierra = "0"

'=============================================================================
' Lee datos necesarios 
'=============================================================================
'-------------------------------------------------------
' Datos de la oficina de trabajo 
'-------------------------------------------------------
sLuogoCPI = ""

sSQL = "SELECT COMUNE,PRV FROM SEDE_IMPRESA WHERE ID_SEDE = " & session("creator")
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
set rsLuogoCPI = cc.execute(sSQL)
if not rsLuogoCPI.eof then
	sLuogoCPI = rsLuogoCPI("COMUNE")
	if sLuogoCPI <> "" then
		sLuogoCPI = DescrComune(sLuogoCPI) 
	end if
end if
rsLuogoCPI.close
set rsLuogoCPI= nothing

'-------------------------------------------------------
' Datos del usuario actual  de Session("idutente")
'-------------------------------------------------------
sOpeCognome = " - "
sOpeNome = " - " 

sSQL = "SELECT COGNOME,NOME FROM UTENTE WHERE IDUTENTE = " & Session("idutente")
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
set rsOperatore = cc.execute (sSQL)
if not rsOperatore.eof then
	sOpeCognome = rsOperatore("COGNOME")
	sOpeNome = rsOperatore("NOME")
end if
rsOperatore.close
set rsOperatore = nothing 

'-------------------------------------------------------
' Datos de la persona analizada de la variable sIdpers
'-------------------------------------------------------
sCognome = " - "
sNome = " - " 
dDataNascita = " - " 
sCodFisc = " - " 
sComNasc = " - " 
sPrvNasc = " - " 

sSQL = "SELECT COGNOME,NOME,DT_NASC,COM_NASC,PRV_NASC,STAT_NASC,COD_FISC FROM PERSONA WHERE ID_PERSONA = " & sIdpers
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
set rsUtente = cc.execute (sSQL)
if not rsUtente.eof then
	sCognome = rsUtente("COGNOME")
	sNome = rsUtente("NOME")
	dDataNascita = ConvDateToString(rsUtente("DT_NASC"))
	sCodFisc = rsUtente("COD_FISC")
	sComNasc = rsUtente("COM_NASC")
	sPrvNasc = rsUtente("PRV_NASC")
	sStatoNasc = rsUtente("STAT_NASC")
end if
rsUtente.close
set rsUtente = nothing
Response.write "Tarea -Cierra  ....  "  & btnImprimeCita  & tarea & " !! " & cierra & "... " & formaction


 

'Falta el control generico Fabio

'si la tarea es 1 busco los datos de 
'la proxima que quiero imprimir
if cint(Tarea) = cint(1)  then
	
	hayDatosDeProximaCita=0
	Set Rs = Server.CreateObject("ADODB.Recordset")
	Set cmdProximaCita = Server.CreateObject("adodb.command")
	
	with cmdProximaCita
		.activeconnection = connLavoro
		.Commandtext = "AgCitaProxima"
		.commandtype = adCmdStoredProc 
		'--------------------------------------------
		' Carga parametros
		'--------------------------------------------
		For Each Parameter In .Parameters
			If Parameter.Direction And adParamInput Then
				Select case ucase(Mid(Parameter.Name, 2))
					CASE "cuil"
						Parameter.value = scodfisc
					CASE ELSE
						' ????
				END SELECT
			End If
		Next
		
		
		'--------------------------------------------
		' Ejecuta
		'--------------------------------------------
'PL-SQL * T-SQL  
RECORDCOUNT = TransformPLSQLToTSQL (RECORDCOUNT) 
		Set Rs = .Execute(recordcount) 
	end with
		
	'--------------------------------------------
	' Devuelve parametros
	'--------------------------------------------
	IF	rs.RecordCount >0 then
		HayDatosDeProximaCita = "1"
  	    ''''sEstadoConvenio = rs("ConvenioEstado")
  		if status=1 then
  			NroCita=rs("NroCita")
  	
  		end if    
    end if	    

end if


If Cierra = "1" then
	Tarea = "2"
	sCuil = sCodFisc
	sOficina = session("creator")
	sUsuario = session("IdUtente")
	NroEntrevista = Session("NroEntrevista")
	
	Set cmdEntrevista = Server.CreateObject("ADODB.Command")
	With cmdEntrevista
	    .ActiveConnection = ConnLavoro
		.CommandTimeout = 1200
		.CommandText = "AgEntrevistaTareaAplica"
		.CommandType = 4
		'Parametros de Input Obligatorios
		.Parameters("@Oficina").Value = sOficina
		.Parameters("@Tarea").Value = Tarea
		.Parameters("@CUIL").Value = sCuil
			
		if len(NroEntrevista) > 0 then
		 .Parameters("@NroEntrevista").Value = NroEntrevista
		End if
		'Parametros de Input Opcionales
		.Parameters("@Usuario").Value = sUsuario
		
		'--------------------------------------------
		' Carga parametros
		'--------------------------------------------
'		For Each Parameter In .Parameters
'			response.write Parameter.Name & "=" & Parameter.value
			'If Parameter.Direction And adParamInput Then
			'	Select case ucase(Mid(Parameter.Name, 2))
			'		CASE "NROCONVENIO"
			'			Parameter.value =  Nroconvenio
			'		CASE ELSE
			'			' ????
			'	END SELECT
			'End If
'		Next	
		
'PL-SQL * T-SQL  
		.Execute()
		
		'Parametros de Output
		NroEntrevista = .Parameters("@NroEntrevista")		
		Secuencia = .Parameters("@Secuencia")			
		Status = .Parameters("@Status").value
		Mensaje = .Parameters("@Mensaje").value
		CitaNro = .Parameters("@CitaNro").value
		
		'P�ra pruebas Hernan Muestro las variables de Ouput
		'--------------------------------------------------------------
		response.write "NroEntrevista: " & NroEntrevista  & "<br>"  
		response.write "Secuencia: " & Secuencia  & "<br>"
		response.write "Status: " & Status  & "<br>"
		response.write "Mensaje: " & Mensaje  & "<br>"
		Response.End
		'--------------------------------------------------------------
	End With
	'Proceso Ok
	Select Case Status
		Case 1 
	
		DirURL = "/BA/home.asp"
		Response.Redirect DirURL
		
		'Redirecciono a pagina de Error
		case else
			Msg = Status & "<br>" & Mensaje
			DirURL = "/include/error.asp?error=" & Msg
			Response.Redirect DirURL
				
	End Select
end if




'======================================================
'	Arma la Pagina a mostrar por partes 
'======================================================
%>
<!--#include virtual= "/pgm/fascicolo/FAS_ImpostaFlag.asp"-->
<br><!--#include file="menu.asp"--><br>

<%
'--------------------------------------------------
' Arma encabezados y ayuda
'--------------------------------------------------
Inizio()
%>

<script language="vbscript">
'===============================================================================
'	Funciones del cliente
'===============================================================================
'-------------------------------------------------------------------------------
' Desabilita el reloj de arena
'-------------------------------------------------------------------------------
sub  DeshabilitarReloj()
	document.body.style.cursor="none"
end sub


function EntrevistaControl(xTarea,xIdPers,xNroEntrevista,xformAction)
'-------------------------------------------------------------------------------
'	Controla el estado de un convenio
'	La situacion de la persona
' Necesita: Tarea a realizar
'-------------------------------------------------------------------------------
	dim Status, Mensaje, NroConvenio, I, cuil, Nrotarea, Msg
	
	NroConvenio = 0
	cuil = ""
	
	
	' arma mensaje
	select case xTarea
		case 1:	Msg = "Impresion"	' Imprimir
		case 2: Msg = "Cierre"		' firmar
		case 3:	Msg = "Desitir"		' desiste
		case else ' ???
			Msgbox "Tarea No definida "
			exit function
	end select

	' Muestra mensaje y pregunta de confirmacion
	
	select case msgbox ("Confirma " & Msg & " de Entrevista?", 4, "Entrevista a Postulante") 
		' Dijo Aceptar --> ejecuta la tarea solicitada
		
		case 6	' vbYes 
			' Seteo la tarea que quiero y Me llamo a mi mismo
				
			document.all.formaction.value= xformaction
			document.all.EntrevistaTarea.value = xTarea ' Setea la Tarea
			
			document.all.Idpers.value = xIdPers ' Setea el Id_Persona
			document.all.NroEntrevista.value = xNroEntrevista ' Setea el Id_Persona    
			document.all.Tarea.value = xTarea ' Setea la Tarea
			msgbox "entro caso 6 " & xformaction
			
			
			'document.formConvenio.f
			' reloj de arena 
			document.body.style.cursor="wait"
			
			' se llama de nuevo a si mismo
			if xtarea=2 then
				
				'document.formConvenio.action="UTE_ModEntrevistaCerrada.asp"
				document.formConvenio.submit
			end if
			if xtarea=1 then
				
				'document.formConvenio.action="UTE_ModEntrevistaCierra.asp"
				document.formConvenio.submit
			end if
	    case else
	end select
end function
</script>


<%
'==========================================================================================
'	Presenta datos de prerequisitos para Cerrar la Entrevista
'==========================================================================================
	'-------------------------------------------------------
	' Cumple con los prerequisitos para cerrar
	'-------------------------------------------------------
	Resultado = VerificaEntrevistaACerrar(sIdpers, sCodFisc, Oficina, Usuario, Status, Mensaje)
	
	iStatus = cstr(Status)
	if Resultado = 1 then
		iStatus = "OK"
		Mensaje = "La Persona Cumple con los Requisitos del Seguro"
	end if

	%>
	<table border="0" width="500">
	    <td colspan="4"> <span class="tbltext1"><b>Requisitos para Cerrar la Entrevista</b></span>
			<br><br></td>
		</tr>           
			
		<tr class="sfondocomm">
			<td colspan="2" class="tbltext1"><b>Sitaci�n</b></td>
			<td align="left" colspan="2"  class="tbltext1"><%=iStatus%></td>
						
		</tr>
		<tr class="sfondocomm">
			<td colspan="2" class="tbltext1"><b>Descripci�n</b></td>
			<td align="left" class="tbltext1"><%=Mensaje%></td>
		</tr>

	    <tr>
			<td colspan="4"><br></td>
		</tr>           
	</table>
	<%
		
	' si no tiene los requisitos necesarios --> Termina aca la pantalla
	if cint(Status)  <> 1 then
		Response.end		
	end if
	%>

	<table border="0" width="500">
	    <tr>
			<td height="2" align="left" colspan="4" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
		 </tr>
	    <td colspan="4"> <span class="tbltext1"><b>Datos de la Persona para cerrar la entrevista</b></span>
			<br><br></td>
		</tr>           
			
		<tr class="sfondocomm">
			<td colspan="2" class="tbltext1"><b>C.I.</b></td>
			<td colspan="2" align="left"  class="tbltext1"><%=sCodFisc%></td>		
				
		</tr>
		<tr class="sfondocomm">
			<td colspan="2" class="tbltext1"><b>Apellido</b></td>
			<td colspan="2" align="left"  class="tbltext1"><%=sCognome%></td>
			
		</tr>
		<tr class="sfondocomm">
			<td colspan="2" class="tbltext1"><b>Nombre</b></td>
			<td colspan="2" align="left" class="tbltext1"><%=sNome%></td>
		</tr>
		
	    <tr>
			<td colspan="4"><br></td>
		</tr>           
	</table>
	<%
	
'==========================================================================================
'	Arma form de opciones para el Usuario
'========================================================================================== 
'''<FORM method="post" action='UTE_ModEntrevistaCerrada.asp' id=formConvenio name=formConvenio onsubmit='' language="javascript">
%>	
<FORM method="post" action='<%=formaction%>'  id=formConvenio name=formConvenio onsubmit='' language="javascript">
<table border="0" width="520">
	<tr>
		<!-- Botones del usuario -->  	
		<table border="0" width="520">
			<tr>
				<td colspan="3"><br>.</td>
				
					<td><input type="submit" class="my" value="Cerrar Entrevista" name="cierra" onclick="EntrevistaControl(2,<%=sIdPers%>,<%=session("NroEntrevista")%>,'UTE_ModEntrevistaCerrada.asp')" ></td>
					<td colspan="2"><input type="submit" class="my" value="Imprimir una Cita" name="btnImprimeCita" onclick="EntrevistaControl(1,<%=sIdPers%>,<%=session("NroEntrevista")%>,'UTE_ModEntrevistaCierra.asp')" ></td>
			</tr>
			<%	
				'========================================
				'	Emite los mensajes de errores
				'========================================	
			if len(Status)>0 and Status <> "1" then
			%>
			<tr>
				<table border="0" width="520">
					<tr>
						<td colspan="3">
							<br>
							<p class="tbltext1"><b>Resultados de las Operaciones:</b><br>
							Status: <%=Status%> - <%=Mensaje%></p>
						</td>
					</tr>
				</table>
			</tr>
			<%End if%>
		</table>
	</tr>
</table>

	<input type=hidden name="IdPers" value="<%=sIdpers%>">
	<input type="hidden" value="<%=EntrevistaTarea%>" name="EntrevistaTarea">
	<input type="hidden" value="<%=NroEntrevista%>" name="NroEntrevista">
	
	<input type="hidden" value="<%=Tarea%>" name="Tarea">
	<input type="hidden" value="<%=FormAction%>" name="FormAction">
</form>
</form>

<!--#include Virtual = "/include/CloseConn.asp"-->
<!--#include Virtual = "/strutt_coda2.asp"-->

<%

'===================================================
' Funciones del Servidor
'===================================================
'-----------------------------------------
' Arma primera parte de la pagina
'	Encabezados, ayudas
'-----------------------------------------
Sub Inizio()
%>	

	<table border="0" CELLPADDING="0" CELLSPACING="0" width="500" align="center">
		<tr height="18">
			<td class="sfondomenu" width="87%" height="18"><span class="tbltext0"><b>&nbsp;CIERRE DE ENTREVISTAS</b></span></td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
			<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
		</tr>
	</table>
	<!-- Commento -->
	
	<table border="0" CELLPADDING="0" CELLSPACING="0" width="500" align="center">
		<tr>
			<td align="left" class="sfondocomm">
				Aqui se verifica si se han cargado los elementos b�sicos necesarios de la entrevista personal realizada.
				Si falta alguno se presentan los modulos que restan cargar.<br>    
				Luego Presionando - Cierre de Entrevista -  se procede a cerrar esta entrevista a la Persona.<br>
				<a href="Javascript:Show_Help('/Pgm/help/Iscr_Utente/UTE_InsCertificazione')"><img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
			</td>
		</tr>
		<tr height="2">
			<td class="sfondocommaz" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>
	<br>
	
<%End Sub%>







