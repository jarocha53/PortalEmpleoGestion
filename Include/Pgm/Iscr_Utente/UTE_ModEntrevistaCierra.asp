<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include Virtual = "/strutt_testa2.asp"-->
<!--#include Virtual = "/include/OpenConn.asp"-->
<!--#include Virtual = "/include/DecComun.asp"-->
<!--#include Virtual = "/include/ControlDateVB.asp"-->
<!--#include Virtual = "/include/ClsInterfaceBD.asp"-->
<!--#include Virtual = "/include/ImpostaProgetto.asp"-->
<!--#include Virtual = "/pgm/Agenda_Entrevistas/Consultas_Citas/AgendaCitasImprime.asp"-->
<!--#include Virtual = "/include/VerificaHistoriaLaboralDatos.asp"-->



<%
'------------------------
' Variables usadas
'------------------------
	dim bCondicionJefes
	dim rsOperatore
	dim sOpeCognome
	dim sOpeNome
	dim sCognome
	dim sNome
	dim dDataNascita
	dim sCodFisc
	dim	sComNasc 
	dim	sPrvNasc 
	dim	sStatoNasc
	dim sLuogoCPI

	'set obDB=new InterfaceBD

	dim Tarea
	
	dim OficinaId
	dim UsuarioId
	Dim Status
	dim Mensaje
	dim sEstadoDesc
	dim fFirma
	dim fFin

	dim HayDatosdeProximaCita
	dim NroCita
	
	dim Cierra
	
	dim MuestraBoton
	
	dim estado
	dim DescriEstado

' Datos para el cierre de la entrevista
EntrevistaTarea = 2		' Cerrar Entrevista
NroEntrevista = session("NroEntrevista")

'=============================================================================
'	Lee Datos externos (de paginas llamadoras)
'=============================================================================
' Identificador de persona analizada
sIdpers = CLng(Request("IdPers"))

' Otros datos de control de tareas
Cierra  = (Request("Cierra"))
Tarea  = (Request("Tarea"))

'Response.Write "Tarea = " & tarea

'MuestraBoton="0"
'''FormAction  = (Request("FormAction"))
' variables de session
Oficina = session("creator")
Usuario = Session("idutente")

Idpersona=sIdpers
FlAna=0	'????

if len(Tarea)= 0 then Tarea = "0"
If len(Cierra) = 0 then Cierra = "0"
If len(MuestraBoton) = 0 then MuestraBoton = "0"

'=============================================================================
' Lee datos necesarios 
'=============================================================================
'-------------------------------------------------------
' Datos de la oficina de trabajo 
'-------------------------------------------------------
sLuogoCPI = ""

sSQL = "SELECT COMUNE,PRV FROM SEDE_IMPRESA WHERE ID_SEDE = " & session("creator")
'PL-SQL * T-SQL  
'SSQL = TransformPLSQLToTSQL (SSQL) 
set rsLuogoCPI = cc.execute(sSQL)
if not rsLuogoCPI.eof then
	sLuogoCPI = rsLuogoCPI("COMUNE")
	if sLuogoCPI <> "" then
		sLuogoCPI = DescrComune(sLuogoCPI) 
	end if
end if
rsLuogoCPI.close
set rsLuogoCPI= nothing

'-------------------------------------------------------
' Datos del usuario actual  de Session("idutente")
'-------------------------------------------------------
sOpeCognome = " - "
sOpeNome = " - " 

sSQL = "SELECT COGNOME,NOME FROM UTENTE WHERE IDUTENTE = " & Session("idutente")
'PL-SQL * T-SQL  
'SSQL = TransformPLSQLToTSQL (SSQL) 
set rsOperatore = cc.execute (sSQL)
if not rsOperatore.eof then
	sOpeCognome = rsOperatore("COGNOME")
	sOpeNome = rsOperatore("NOME")
end if
rsOperatore.close
set rsOperatore = nothing 

'-------------------------------------------------------
' Datos de la persona analizada de la variable sIdpers
'-------------------------------------------------------
sCognome = " - "
sNome = " - " 
dDataNascita = " - " 
sCodFisc = " - " 
sComNasc = " - " 
sPrvNasc = " - " 
sCod_Docst= " - "

sSQL = "SELECT COGNOME,NOME,DT_NASC,COM_NASC,PRV_NASC,STAT_NASC,COD_FISC FROM PERSONA WHERE ID_PERSONA = " & sIdpers
'PL-SQL * T-SQL  
'SSQL = TransformPLSQLToTSQL (SSQL) 
set rsUtente = cc.execute (sSQL)
if not rsUtente.eof then
	sCognome = rsUtente("COGNOME")
	sNome = rsUtente("NOME")
	dDataNascita = ConvDateToString(rsUtente("DT_NASC"))
	sCodFisc = rsUtente("COD_FISC")
	sComNasc = rsUtente("COM_NASC")
	sPrvNasc = rsUtente("PRV_NASC")
	sStatoNasc = rsUtente("STAT_NASC")
end if
rsUtente.close
set rsUtente = nothing

sSQL = "SELECT COD_DOCST FROM PERS_TIPO_DOC WHERE ID_PERSONA = " & sIdpers
set rsUtente = cc.execute (sSQL)
if not rsUtente.eof then
	sCod_Docst = rsUtente("COD_DOCST")
end if
rsUtente.close
set rsUtente = nothing

'======================================================
'	Arma la Pagina a mostrar por partes 
'======================================================
%>
<!--#include virtual= "/pgm/fascicolo/FAS_ImpostaFlag.asp"-->
<br><!--#include file="menu.asp"--><br>

<%
'--------------------------------------------------
' Arma encabezados y ayuda
'--------------------------------------------------
Inizio()

%>

<script language="vbscript">
'===============================================================================
'	Funciones del cliente
'===============================================================================
'-------------------------------------------------------------------------------
' Desabilita el reloj de arena
'-------------------------------------------------------------------------------
sub  DeshabilitarReloj()
	document.body.style.cursor="none"
end sub




function EntrevistaControl(xTarea)
'-------------------------------------------------------------------------------
'	Controla el estado de las Tareas
' Necesita: Tarea a realizar
'-------------------------------------------------------------------------------
	dim Status, Mensaje, NroConvenio, I, cuil, Nrotarea, Msg
	
	
	' arma mensaje
	select case xTarea
		case 1:	
			Msg = "Impresion"	' Imprimir
			' Muestra mensaje y pregunta de confirmacion
			Alex
			select case msgbox ("Confirma " & Msg & " de la Cita?", 4, "Entrevista a Postulante") 
				' Dijo Aceptar --> ejecuta la tarea solicitada
				case 6	' vbYes 
					' Seteo la tarea que quiero y Me llamo a mi mismo
					'document.write(xtarea)
				
					document.formImpre.tarea.value = cint(xTarea)
					' reloj de arena 
					document.body.style.cursor="wait"
					'..document.formImpre.submit
					
			    case else
					EntrevistaControl = false
					exit function
			end select
			
		case 2: Msg = "el Cierre"		' firmar
				' Muestra mensaje y pregunta de confirmacion
				select case msgbox ("Confirma " & Msg & " de la Entrevista?", 4, "Entrevista a Postulante") 
				' Dijo Aceptar --> ejecuta la tarea solicitada
				case 6	' vbYes 
					' Seteo la tarea que quiero y Me llamo a mi mismo
					document.frmCerra.tarea.value = cint(xTarea)
					' reloj de arena 
					document.body.style.cursor="wait"
					'..document.FormEntrevista.submit
			    case else
					EntrevistaControl = false
					exit function
			end select
			
		case 3: Msg = "Impresion de Comprobante"		' firmar
				' Muestra mensaje y pregunta de confirmacion
			select case msgbox ("Confirma " & Msg & " de Inscripci�n?", 4, "Entrevista a Postulante") 
				' Dijo Aceptar --> ejecuta la tarea solicitada
				case 6	' vbYes 
					' Seteo la tarea que quiero y Me llamo a mi mismo
					document.formImpreTalon.tarea.value = cint(xTarea)
					' reloj de arena 
					document.body.style.cursor="wait"
					'..document.FormEntrevista.submit
			    case else
					EntrevistaControl = false
					exit function
			end select
		' cuando no tiene cita, muestra mensaje de aviso,NO imprime la cita	
		case 4: Msg = "Impresion de Cita"		' firmar
				' Muestra mensaje y pregunta de confirmacion
				document.formImpre.tarea.value = cint(xTarea)
					Msgbox "No existe Cita Programada para este postulante"
					EntrevistaControl = false
					exit function
				
			

		case else
			Msgbox "Tarea No definida "
			exit function
	end select
end function

</script>

<script language="javascript">
function EntrevistaControl2(xxTarea)
{
	var MSG
	MSG = 'Cierre'
	var MSG2 
	MSG2 = 'Entrevista a Postulante \n Confirma Cierre de la Entrevista?';
	if (confirm(MSG2))
		{
		document.frmCerra.tarea.value = xxTarea;
		document.frmCerra.submit()
		//document.frmCerra.EntrevistaTarea.value = EntrevistaTarea;
		//document.FormEntrevista.submit()
		}
		document.frmCerra.submit(0)
}

</script>
<%


'==========================================================================================
'	Presenta datos de prerequisitos para Cerrar la Entrevista
'==========================================================================================
	'-------------------------------------------------------
	' Cumple con los prerequisitos para cerrar
	'-------------------------------------------------------
	'..Resultado = VerificaEntrevistaACerrar(sIdpers, sCodFisc, Oficina, Usuario, Status, Mensaje)
	TipoValidacion = 1	' Validaciones de Entrevistas
	
	If ValidateService(session("idutente"),"ADHESION_SEGURO",cc) <> "true" Then
		TipoValidacion = 999 ''vani
	end if
	
	'Response.Write "EntrevistaTarea" & EntrevistaTarea & "<BR>"
	'Response.Write "NROENTREVISTA" & NROENTREVISTA
	'Response.end
	
	Resultado = VerificaEntrevistaValidacionesAplica (Oficina, TipoValidacion,  NroEntrevista, sIdpers, sCodFisc, EntrevistaTarea, Usuario, Status, Mensaje)

	iStatus = cstr(Status)
	if Resultado = 1 then
		iStatus = "OK"
		Mensaje = "La Persona Cumple con los Requisitos para finalizar la entrevista"
	end if

	%>
	<table border="0" width="500">
		<tr>
	    <td colspan="4"> <span class="tbltext1"><b>Requisitos para Cerrar la Entrevista</b></span>
			<br><br></td>
		</tr>           
			
		<tr class="sfondocomm">
			<td colspan="2" class="tbltext1"><b>Situaci�n</b></td>
			<td align="left" colspan="2"  class="tbltext1"><%=iStatus%></td>
						
		</tr>
		<tr class="sfondocomm">
			<td colspan="2" class="tbltext1"><b>Descripci�n</b></td>
			<td align="left" class="tbltext1"><%=Mensaje%></td>
		</tr>

	    <tr>
			<td colspan="4"><br></td>
		</tr>           
		
	<%
		
	 'si no tiene los requisitos necesarios --> Pide Cierre Abrupto
	if cint(Status)  <> 1 then
	%>
	<tr>
			<td colspan="4" align="center">
	<%
		Response.Write "<b class='tbltext1'>NO se cumplen la condiciones para cerrar la entrevista</b>"
	%>
	</td>
		</tr>           
	</table>
	<%	'<!--#include Virtual = "/strutt_coda2.asp"-->	
	
	'Response.end		
	end if
	%>
	<!--vani-->
	<!--</table>-->
	<table border="0" width="500">
	    <tr>
			<td height="2" align="left" colspan="4" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
		 </tr>
		 <tr>
	    <td colspan="4"> 
	    <span class="tbltext1"><b>Datos de la Persona para cerrar la entrevista</b></span>
			<br><br>
			</td>
		</tr>           
			
		<tr class="sfondocomm">
			<td colspan="2" class="tbltext1"><b>Tipo Documento</b></td>
			<%
			if sCod_Docst="CC" then
				sCod_Docst= "C�DULA DE CIUDADAN�A"
			elseif sCod_Docst="PA" then
				sCod_Docst= "PASAPORTE"
			elseif sCod_Docst="TI" then
				sCod_Docst= "TARJETA DE IDENTIDAD"
			else
				sCod_Docst= "C�DULA DE EXTRANJERIA"
			end if
			%>
			<td colspan="2" align="left"  class="tbltext1"><%=sCod_Docst%></td>		
				
		</tr>
		<tr class="sfondocomm">
			<td colspan="2" class="tbltext1"><b>N&uacute;mero</b></td>
			<td colspan="2" align="left"  class="tbltext1"><%=sCodFisc%></td>		
				
		</tr>
		<tr class="sfondocomm">
			<td colspan="2" class="tbltext1"><b>Apellido</b></td>
			<td colspan="2" align="left"  class="tbltext1"><%=sCognome%></td>
			
		</tr>
		<tr class="sfondocomm">
			<td colspan="2" class="tbltext1"><b>Nombre</b></td>
			<td colspan="2" align="left" class="tbltext1"><%=sNome%></td>
		</tr>
		
	    <tr>
			<td colspan="4"><br></td>
		</tr>           
	</table>
	<%

'--------------------------------------------
' Controles Impresion de Cita
'--------------------------------------------
'if cint(Tarea) = 1 then
'	ImprimeCita NroCita,NroEntrevista, sFileNameDic
'end if	
'if cint(Tarea) = 1 then
'	hayDatosDeProximaCita=0
'	Set Rs = Server.CreateObject("ADODB.Recordset")
'	Set cmdProximaCitaCuil = Server.CreateObject("adodb.command")
	
'	with cmdProximaCitaCuil
'		.activeconnection = connLavoro
'		.Commandtext = "AgCitaCuilBuscaproxima"
'		.commandtype = adCmdStoredProc 
'		'--------------------------------------------
'		' Carga parametros
'		'--------------------------------------------
'		.Parameters("@cuil").Value= scodfisc
'		'--------------------------------------------
'		' Ejecuta
'		'--------------------------------------------
'		 .Execute() 
'		'rescato parametros de salida
'	    NroCita = .parameters("@NroCita")
'		status =.parameters("@status")
'		Mensaje =.parameters("@Mensaje")
'	end with	
'	set cmdProximacitaCuil=nothing
	'si 

'	select case status
'		case 1
'			HayDatosDeProximaCita = "1"
'  	    	ImprimeCita NroCita,NroEntrevista, sFileNameDic
		
'		case else	' Emite mensaje de error o que no se pude imprimir
'			HayDatosDeProximaCita = "0"
'	end select	
'end if


'--------------------------------------------
' Controles Impresion Comprobante Inscripcion
'--------------------------------------------

'Response.Write tarea
'Response.End 
if cint(Tarea) = 3 then
	
	ImprimeComprobante sCodFisc,Oficina,NroEntrevista, sFileNameDic

end if

function ControlCita()
	hayDatosDeProximaCita=0
	Set Rs = Server.CreateObject("ADODB.Recordset")
	Set cmdProximaCitaCuil = Server.CreateObject("adodb.command")
	
			
	with cmdProximaCitaCuil
		.activeconnection = connLavoro
		.Commandtext = "AgCitaCuilBuscaproxima"
		.commandtype = adCmdStoredProc 
		'--------------------------------------------
		' Carga parametros
		'--------------------------------------------
		.Parameters("@cuil").Value= scodfisc
		'--------------------------------------------
		' Ejecuta
		'--------------------------------------------
		'PL-SQL * T-SQL  
		 .Execute() 
		'rescato parametros de salida
	    NroCita = .parameters("@NroCita")
		status =.parameters("@status")
		Mensaje =.parameters("@Mensaje")
	end with	
	set cmdProximacitaCuil=nothing
	'si 

	select case status
		case 1
			
			HayDatosDeProximaCita = "1"
			ControlCita="1"
			if cint(Tarea) = 1 then
				'Response.Write Tarea
				'Response.End 	
				'Response.write session("Creator")
				'Response.write "<br>"
				'Response.write "15"
				'Response.write "<br>"
				'Response.write NroCita
				'Response.write "<br>"
				'Response.write NroEntrevista
				'Response.write "<br>"
				'Response.write session("IdUtente")
				'Response.write "<br>"
				'Response.write sCodFisc
				
				'Dim Inter 

				'Set Inter = New InterfaceBD
				
				'Response.write Inter.GrabarMovimiento (clng(session("Creator")), cint(16), clng(NroCita), clng(NroCita), 0, "", "", "", cstr(sCodFisc), clng(NroEntrevista), "", "", "", "", "", session("IdUtente"))
				'Response.End 
				
				
				ImprimeCita NroCita,NroEntrevista, sFileNameDic
				
				'Response.write request("NoEjecutar")
				'	if request("NoEjecutar") <> "S" then 
					%>
					<script language="javascript">
					//	document.location.href = "UTE_ModEntrevistaCierra.asp?IdPers=<%=sIdPers%>&Cierra=<%=Cierra%>&Tarea=<%=Tarea%>&NoEjecutar=S"
					</script>
					<%
				'	end if
			end if	
  	    	''''ImprimeCita NroCita,NroEntrevista, sFileNameDic
		
		case else	' Emite mensaje de error o que no se pude imprimir
			HayDatosDeProximaCita = "0"
			ControlCita="0"
	end select	
end function

'javascript:manda	
'<input type="submit" class="my" value="Cerrar Entrevista" name="cierra" onclick="EntrevistaControl(2)" >
'==========================================================================================
'	Arma form de opciones para el Usuario
'========================================================================================== 
'''''este es valido
'''<FORM method="post" action='UTE_ModEntrevistaCerrada.asp' id=formConvenio name=formConvenio onsubmit='' language="javascript">
'''<FORM method="post" action='<%=formaction  id=formConvenio name=formConvenio onsubmit='' language="javascript">
%>	


<table border="0" width="500">
	<tr>
	<td>
		<!-- Botones del usuario -->  	
		<table  border="0" width="100%">
			<tr>
				
					<%if cint(Status)  = 1 then%>
					<td colspan="2">
					<form method="post" action='UTE_ModEntrevistaCerrada.asp' id=frmCerra name=frmCerra onsubmit='return EntrevistaControl(2)' language="javascript">
						
						<input type="submit" class="my" value="Cerrar Entrevista" name="cierra">
						
						<input type="hidden" name="Tarea" value="<%=Tarea%>" >
						<input type=hidden name="IdPers" value="<%=sIdpers%>">
						<input type="hidden" value="<%=EntrevistaTarea%>" name="EntrevistaTarea">
						<input type="hidden" value="<%=NroEntrevista%>" name="NroEntrevista">
					</form>
					</td>
					<%end if%>
					<%
					'controla si tiene cita
					'si TieneCita=0,emite mensaje que no tiene cita (tarea 4 en entrevistaControl)
					'si TieneCita es uno,imprime la cita cita (tarea 1 en funcio entrevista control)
					TieneCita=ControlCita()
					
					'Response.Write "tienecita" & tienecita
					
					
					'if TieneCita=0 then
					%>
					<!--								
					<td colspan="2">
					<form method="post" action='UTE_ModEntrevistaCierra.asp' id=formImpre name=formImpre onsubmit='return EntrevistaControl(4)' >
						<input type="submit" class="my" value="Imprimir Comprobante de Cita" name="btnImprimeCita" >

						<input type="hidden"  name="Tarea" value="<%=Tarea%>">
						<input type=hidden name="IdPers" value="<%=sIdpers%>">
						<input type="hidden" value="<%=EntrevistaTarea%>" name="EntrevistaTarea">
						<input type="hidden" value="<%=NroEntrevista%>" name="NroEntrevista">
					</form>
					</td>
					<% 
					'end if
					'if TieneCita=1 then
					%>
					<td colspan="2">
					<form method="post" action='UTE_ModEntrevistaCierra.asp' id=formImpre name=formImpre onsubmit='return EntrevistaControl(1)' language="javascript">
						<input type="submit" class="my" value="Imprimir Comprobante de Cita" name="btnImprimeCita" >
						
						<input type="hidden"  name="Tarea" value="<%=Tarea%>">
						<input type=hidden name="IdPers" value="<%=sIdpers%>">
						<input type="hidden" value="<%=EntrevistaTarea%>" name="EntrevistaTarea">
						<input type="hidden" value="<%=NroEntrevista%>" name="NroEntrevista">
					</form>
					</td>-->
					<%'end if%>
					
			</tr>
			<tr>
					<td colspan="2"></td>
					<td colspan="2">
					<form method="post" action='UTE_ModEntrevistaCierra.asp' id=formImpreTalon name=formImpreTalon onsubmit='return EntrevistaControl(3)' language="javascript">
						<input type="submit" class="my" value="Imprimir Comprobante de Historia Laboral" name="btnImprimeComprobante" >

						<input type="hidden"  name="Tarea" value="<%=Tarea%>">
						<input type=hidden name="IdPers" value="<%=sIdpers%>">
						<input type="hidden" value="<%=EntrevistaTarea%>" name="EntrevistaTarea">
						<input type="hidden" value="<%=NroEntrevista%>" name="NroEntrevista">
					</form>
					</td>
			</tr>
			
			<%	
			
				'========================================
				'	Emite los mensajes de errores
				'========================================	
				
			if (len(Status)>0 and Status <> "1")  then
			%>
			<tr>
				<td colspan="4">
				<table border="0" width="100%">
					<tr>
						<td colspan="3">
							<br>
							<p class="tbltext1"><b>Resultados de las Operaciones:</b><br>
							Status: <%=Status%> - <%=Mensaje%></p>
						</td>
					</tr>
				</table>
				</td>
			</tr>
			<%End if%>
		</table>
		</td>
	</tr>
</table>

<!--#include Virtual = "/strutt_coda2.asp"-->

<%

'===================================================
' Funciones del Servidor
'===================================================
'-----------------------------------------
' Arma primera parte de la pagina
'	Encabezados, ayudas
'-----------------------------------------
Sub Inizio()
%>	

	<table border="0" CELLPADDING="0" CELLSPACING="0" width="500" align="center">
		<tr height="18">
			<td class="sfondomenu" width="87%" height="18"><span class="tbltext0"><b>&nbsp;CIERRE DE ENTREVISTAS</b></span></td>
			<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
			<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
		</tr>
	</table>
	<!-- Commento -->
	
	<table border="0" CELLPADDING="0" CELLSPACING="0" width="500" align="center">
		<tr>
			<td align="left" class="sfondocomm">
				Aqui se verifica si se han cargado los elementos b�sicos necesarios de la entrevista personal realizada.
				Si falta alguno se presentan los modulos que restan cargar.<br>    
				Luego Presionando - Cierre de Entrevista -  se procede a cerrar esta entrevista a la Persona.<br>
				<a href="Javascript:Show_Help('/Pgm/help/Iscr_Utente/UTE_InsCertificazione')"><img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
			</td>
		</tr>
		<tr height="2">
			<td class="sfondocommaz" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif">
			</td>
		</tr>
	</table>
	<br>
	
<%End Sub%>







