<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include virtual="/strutt_testa2.asp"-->
<!--#include virtual="/include/DecComun.asp"-->
<!--#include virtual="/include/ControlDateVB.asp"-->
<!--#include virtual="/include/SysFunction.asp"-->
<!--#include Virtual = "/util/portallib.asp"-->
<!--#include virtual = "/include/OpenConn.asp"-->
<%

dim sSqlUpEsForm

sACT = request("ACT")
nIdPers			= request.form("IdPers")
sCatProt		= request.form("cmbCatProt")
sSubCatProt		= request.form("cmbSubCatProt")
sSituacion		= request.form("cmbSituacion")
sOrigenDisc		= request.form("cmbOrigenDisc")
sEnteCert		= request.form("txtEnteCert")
dInicioCert		= request.form("txtInicioCert")
dFinCert		= request.form("txtFinCert")
sDiagnostico	= request.form("txtDiagnostico")
sRehabilitacion	= request.form("txtRehabilitacion")
sMedicacion		= request.form("txtMedicacion")
sFrecuenMedic	= request.form("cmbFrecuencia")
sEmpleabilidad	= request.form("txtEmpleabilidad")
sAutohigiene	= request.form("optAutohigiene")
sTraslado		= request.form("cmbTraslado")
sMovilidad		= request.form("cmbMovilidad")
sTratamiento	= request.form("txtTratamiento")
sSSoc			= request.form("optSSoc")
sSSocEspec		= request.form("txtSSocEspec")
sSubsid			= request.form("optSubsid")
sSubsidEspec	= request.form("txtSubsidEspec")
sobservaciones  = request.form("txtobservaciones")

if sSubCatProt = "" then
	sSubCatProt	= "99"
end if


srtVer = "SELECT * FROM pers_catprot  WHERE id_Persona = '" & nIdPers & "'" &_
		 " AND cod_catprot = '" & sCatProt & "'" 
		 
		 ''' MODIFICATO 100907 SG  ''''
		 '" AND cod_sub_catprot = '" & sSubCatProt & "'"
		 ''' MODIFICATO 100907 SG  ''''
		 

set mirs = server.CreateObject("adodb.recordset")

set mirs = CC.execute(srtVer)
					
if not mirs.EOF and sACT = "INS" then 
	%>		
	<table border="0" cellspacing="1" cellpadding="1" width="500">
		<tr align="middle">
			<td><br></td>
		</tr>
		<tr align="middle">
			<td class="tbltext3">Ya existe una Discapacidad del tipo especificado.<br>
			</td>
		</tr>
	</table>
	<br>				
	<table border="0" cellpadding="0" cellspacing="1" width="500">
		<tr>
			<td align="middle" colspan="2" width="60%"><b>
				<a HREF="javascript:history.back()"><img SRC="<%=Session("Progetto")%>/images/indietro.gif" border="0">
			</td>
		</tr>
	</table>	
	<%
else
	sSoporte = ""
	for x = 0 to 30
		sVal = cstr(x)
		if len(sVal) = 1 then
			sNum = "0" & sVal 
		else
			sNum = sVal
		end if
		'Response.write "sNum : " &  sNum & "<br>"
		if request.form("chkAct" & sNum ) <> "" then
			sSoporte = sSoporte & sNum & "|"
		end if
		sNum = ""
	next

	CC.BeginTrans	
	Errore = "0"
	chiave =  nIdPers & "|" & sCatProt
	sDtTmst = ""

	if sACT = "MOD" then
		sSqlUpEsForm =  "UPDATE PERS_CATPROT " &_
						"SET " & _
						"COD_SUB_CATPROT" & " = '" & sSubCatProt & "', " & _  
						"SITUACION" & " = '" & sSituacion & "', " & _
						"ORIGEN_DISC" & " = '" & sOrigenDisc & "', "   & _ 
						"COD_ENTE_CERT" & " = '" & sEnteCert & "', " & _   
						"DT_INI_CERT" & " = " & ConvDateToDb(dInicioCert) & ", "   & _ 
						"DT_FIN_CERT" & " = " & ConvDateToDb(dFinCert) & ", "   & _ 
						"DIAGNOSTICO" & " = '" & sDiagnostico & "', "   & _ 
						"REHABILITACION" & " = '" & sRehabilitacion & "', "   & _ 
						"SOPORTE" & " = '" & sSoporte & "', "   & _ 
						"MEDICACION" & " = '" & 	sMedicacion & "', "   & _ 
						"MED_FRECUENCIA" & " = '" & 	sFrecuenMedic & "', "   & _ 
						"NIV_EMPLEABILIDAD" & " = '" & 	sEmpleabilidad & "', "   & _ 
						"AUTOHIGIENE" & " = '" & 	sAutohigiene & "', "  & _  
						"COD_TRASLADO" & " = '" & 	sTraslado & "', "   & _ 
						"MOVILIDAD" & " = '" & 	sMovilidad & "', "   & _ 
						"TRATAM_MEDIC" & " = '" & 	sTratamiento & "', "   & _ 
						"SEG_SOC" & " = '" & 	sSSoc & "', "   & _ 
						"SEG_SOC_ESPEC" & " = '" & 	sSSocEspec & "', "   & _ 
						"OTR_SUBSID" & " = '" & 	sSubsid & "', "   & _ 
						"OTR_SUBSID_ESPEC" & " = '" & 	sSubsidEspec & "', "    & _ 
						"OBSERVACIONES" & " = '" & 	sObservaciones & "', "    & _ 
						"DT_TMST" & " = " & ConvDateToDb(now) & " "   & _ 
						"WHERE ID_PERSONA = " & nIdPers & " AND COD_CATPROT = '" & sCatProt & "'"
	elseif sACT = "DEL" then
		sSqlUpEsForm =	"DELETE PERS_CATPROT " &_
						"WHERE ID_PERSONA = " & nIdPers & " AND COD_CATPROT = '" & sCatProt & "'"
	else
		sSqlUpEsForm =	"INSERT INTO PERS_CATPROT " &_
						"(ID_PERSONA, COD_CATPROT, COD_SUB_CATPROT, SITUACION, ORIGEN_DISC, COD_ENTE_CERT, DT_INI_CERT, DT_FIN_CERT, "   & _ 
						"DIAGNOSTICO, REHABILITACION, SOPORTE, MEDICACION, MED_FRECUENCIA, "   & _ 
						"NIV_EMPLEABILIDAD, AUTOHIGIENE, COD_TRASLADO, MOVILIDAD, TRATAM_MEDIC, "   & _ 
						"SEG_SOC, SEG_SOC_ESPEC, OTR_SUBSID, OTR_SUBSID_ESPEC, OBSERVACIONES, DT_TMST) " &_
						" VALUES (" & _  
						nIdPers & ", " & _  
						"'" & sCatProt & "', " & _ 
						"'" & sSubCatProt & "', " & _  
						"'" & sSituacion & "', " & _  
						"'" & sOrigenDisc & "', " & _ 
						"'" & sEnteCert & "', " & _   
						ConvDateToDb(dInicioCert) & ", "   & _ 
						ConvDateToDb(dFinCert) & ", "   & _ 
						"'" & sDiagnostico & "', " & _ 
						"'" & sRehabilitacion & "', " & _ 
						"'" & sSoporte & "', " & _ 
						"'" & sMedicacion & "', " & _ 
						"'" & sFrecuenMedic & "', " & _ 
						"'" & sEmpleabilidad & "', " & _ 
						"'" & sAutohigiene & "', " & _  
						"'" & sTraslado & "', " & _ 
						"'" & sMovilidad & "', " & _ 
						"'" & sTratamiento & "', " & _ 
						"'" & sSSoc & "', " & _ 
						"'" & sSSocEspec & "', " & _ 
						"'" & sSubsid & "', " & _ 
						"'" & sSubsidEspec & "', " & _ 
						"'" & sObservaciones & "', " & _ 
						ConvDateToDb(now) & _ 
						")" 
	end if

	errore = EseguiNoCTrace(UCase(Mid(Session("progetto"),2)), chiave, "PERS_CATPROT", Session("idutente"), nID_PERSONA, "P", sACT, sSqlUpEsForm, 1, sDtTmst, cc)
	Set sSqlUpEsForm = Nothing
		Response.Write "Errore: " & Errore & "<br>"
		'Response.End 

	if not isnull(errore) then errore = cstr(errore)

	if Errore = "0" then
		CC.CommitTrans
		%>
		<form name="frmIndietro" method="post" action="UTE_VisCatProt.asp">
			<input type="hidden" name="IdPers" value="<%=nIdPers%>">
			<table border="0" CELLPADDING="0" CELLSPACING="0" width="500" align="center">
				<tr>
					<td align="center"> 
								
					</td>
				</tr>
			</table>
		</form>
		<%
		if sACT = "MOD" then
			%>
			<script>
				alert("Modificación Correctamente Efectuada");
				frmIndietro.submit()
			</script>
			<%
		elseif sACT = "DEL" then
			%>
			<script>
				alert("Discapacidad Eliminada");
				frmIndietro.submit()
			</script>
			<%
		else
			%>
			<script>
				alert("Ingreso Correctamente Efectuado");
				frmIndietro.submit()
			</script>
			<%
		end if	
	else
		CC.RollbackTrans
		%>	
		<br>
		<table border="0" cellspacing="1" cellpadding="1" width="500">
			<tr align="middle">
				<td class="tbltext3"> Modificaci&oacute;n de Datos no efectuada.<br>
					<%Response.Write (Errore)%> 
				</td>
			</tr>
		</table>
		<br>				
		<table border="0" cellpadding="0" cellspacing="1" width="500">
			<tr>
				<td align="middle" colspan="2" width="60%"><b>
					<a HREF="javascript:history.back()"><img SRC="<%=Session("Progetto")%>/images/indietro.gif" border="0">
				</td>
			</tr>
		</table>	
		<% 
	end if
end if
set mirs = nothing

%>
<!--#include virtual="/include/CloseConn.asp"-->
<!--#include virtual="/strutt_coda2.asp"-->

