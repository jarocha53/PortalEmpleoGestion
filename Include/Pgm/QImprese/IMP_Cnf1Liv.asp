<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include Virtual = "strutt_testa2.asp"-->
<!--#include Virtual = "/include/OpenConn.asp"-->
<!--#include Virtual = "/util/portallib.asp"-->
<!--#include Virtual = "/include/DecCod.asp"-->

<%
Dim TipologiaImpVal
Dim TipologiaImpDesc
Dim FormaGiuridicaDesc
Dim SettoreDesc
Dim TipoSedeDesc
Dim ProvinciaDesc
Dim CodiceRuoloDesc
Dim rstSettore
Dim SqlSettore


'----- Recupero valori dalla pagina precedente -----
RagioneSociale = UCase(Request.Form("txtRagSoc"))
RagioneSociale = Replace(Server.HTMLEncode(RagioneSociale), "'", "''")

FormaGiuridicaVal = Request.Form("cmbCodForm")

SettoreVal = Request.Form("cmbCodSet")

TipologiaImpVal = Request.Form("cmbCodTimpr")

Attivita = UCase(Request.Form("txtDescAtt"))
Attivita = Replace(Server.HTMLEncode(Attivita), "'", "''")

PartitaIva = Request.Form("txtPartIva")

CodiceFiscale = UCase(Request.Form("txtCodFis"))

SitoWeb = UCase(Request.Form("txtSitoWeb"))
SitoWeb = Replace(Server.HTMLEncode(SitoWeb), "'", "''")

DescrizioneSede = UCase(Request.Form("txtDescrizione"))
DescrizioneSede = Replace(Server.HTMLEncode(DescrizioneSede), "'", "''")

TipoSedeVal = Request.Form("cmbCodSede")

Indirizzo = UCase(Request.Form("txtIndirizzo"))
Indirizzo = Replace(Server.HTMLEncode(Indirizzo), "'", "''")

CAP = Request.Form("txtCap")

Comune = UCase(Request.Form("txtComune"))
Comune = Replace(Server.HTMLEncode(Comune), "'", "''")

CodCom = UCase(Request.Form("txtComuneHid"))
CodCom = Replace(Server.HTMLEncode(CodCom), "'", "''")



ProvinciaVal = Request.Form("cmbProvRes")

Telefono = Request.Form("txtSedeTel")

Fax = Request.Form("txtSedeFax")

Email = Request.Form("txtMail")

Cognome = UCase(Request.Form("txtCognome"))
Cognome = Replace(Server.HTMLEncode(Cognome), "'", "''")

Nome = UCase(Request.Form("txtNome"))
Nome = Replace(Server.HTMLEncode(Nome), "'", "''")

CodiceRuoloVal = Request.Form("cmbRefRuo")

Login = Ucase(Request.Form("txtLogin"))
Login = Replace(Server.HTMLEncode(Login), "'", "")

CentroImpiego = UCase(Request.Form("txtCentroImpiego"))

'----- Recupero le descrizioni per le combo tramite funzione -----
TipologiaImpDesc = DecCodVal("TIMPR", "0", "", TipologiaImpVal, 1)
formaGiuridicaDesc = DecCodVal("FGIUR", "0", "", FormaGiuridicaVal, 1)
TipoSedeDesc = DecCodVal("TSEDE", "0", "", TipoSedeVal, 1)
ProvinciaDesc = DecCodVal("PROV", "0", "", ProvinciaVal, 1)
CodiceRuoloDesc = DecCodVal("RUOLO", "0", "", CodiceRuoloVal, 1)


'----- Recupero la descrizione del settore a partire dal suo codice -----
Set rstSettore = server.CreateObject("ADODB.Connection")
SqlSettore = "SELECT DENOMINAZIONE FROM SETTORI WHERE ID_SETTORE = '" & SettoreVal & "'"
'PL-SQL * T-SQL  
SQLSETTORE = TransformPLSQLToTSQL (SQLSETTORE) 
Set rstSettore = cc.Execute(SqlSettore)
SettoreDesc = rstSettore.fields(0).value


'----- Routine Controllo Esistenza LOGIN ------
Dim rstAziende
Dim sql,rr
Dim inserimento
Dim tipo
Dim numErrore
Dim appo
Dim nIdGruppo

Set rstAziende = Server.CreateObject("ADODB.Recordset")
sqlControllo = "SELECT LOGIN FROM UTENTE WHERE LOGIN ='" & Login & "'"
'PL-SQL * T-SQL  
SQLCONTROLLO = TransformPLSQLToTSQL (SQLCONTROLLO) 
Set rstAziende = cc.Execute(sqlControllo)

'Controllo l'esistenza della LOGIN inserita dal referente:
'se non presente, il numero dei record presente nel recordset
'sar� < 1 e quindi vado avanti; 
'------------------------------------------------------
If rstAziende.eof Then
%>	
<!--#include virtual = "include/DecComun.asp"-->
<%
'
	'Controllo la corrispondenza del comune scritto con la 
	'provincia selezionata nella combo tramite funzione
	'-----------------------------------------------------
	Errore="0"

'	sComDecod = CtlComune(Comune,ProvinciaVal)
'	if sComDecod <> "0" then
		
		'Richiamo la funzione che fa l'inserimento nel DB; se non
		'presente crea automaticamente la password
		'--------------------------------------------------------
		tipo = "S"
		cc.Begintrans
		
		sql = "select idgruppo from gruppo where cod_rorga = 'AZ'"
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
		set rstGruppo = CC.execute(sql)
		
		If not rstGruppo.eof Then
			nIdGruppo = rstGruppo("idgruppo")
		Else
			Response.Write "<b class='tbltext'><br>Inserimento al momento non possibile"
			Response.Write "<p>Contattare il Gruppo Assistenza Portale Italia Lavoro<br>"
			Response.Write "all'indirizzo <a href='mailto:po-assistenza@italialavoro.it'>po-assistenza@italialavoro.it</a></p></b>"					   
		End If
			rstGruppo.close
		set rstGruppo = nothing
		
		inserimento = AddUserGroup(Nome,Cognome,tipo,Login,Password,Email,Creator,nIdGruppo,mid(session("progetto"),2),,cc)		
		
		If inserimento=true Then 
			cc.CommitTrans
			%>	
			<table border="0" width="520" cellspacing="0" cellpadding="0" height="81">
			   <tr>
				 <td width="520" background="<%=Session("Progetto")%>/images/titoli/strumenti2g.gif" height="81" valign="bottom" align="right">
			       <table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
			         <tr>
			           <td width="100%" valign="top" align="right"><b class="tbltext1a">Registrazione Azienda &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></b></td>
			         </tr>
			       </table>
			     </td>
			   </tr>
			</table><br>
			<table cellpadding="0" cellspacing="0" width="340" border="0">
				<tr>
					<td width="30%"></td>
					<td width="3%"></td>
					<td width="50%"></td>
				</tr>
			   <tr>
			      <td width="57%" colspan="3" height="18" align="center"><br>
					<span class="tbltext3">
						Ingreso correctamente efectuado.<br>
						<br>
						A la brevedad recibir� un correo a la direcci�n indicada.
					</span><br>
				  </td> 
			   </tr>   
				<tr height="18">
					<td colspan="3"></td>
				</tr>
			</table>
			<table border="0" width="520" cellspacing="0" cellpadding="0" height="81">
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td align="center"><a HREF="<%=Session("Progetto")%>/home.asp"><img SRC="<%=Session("Progetto")%>/images/indietro.gif" border="0"></a></td>
				</tr>
			</table>
			<br>
			<br>
			
				<%
				Dim objMailOperator
				Dim objMailRefer
				Dim HtmlOperat	
				Dim HtmlRefer

							
					'----- Mail per Operatore Italia Lavoro -----
					Set objMailOperator = Server.CreateObject("CDONTS.NewMail")
					HtmlOperat = "<!DOCTYPE HTML PUBLIC ""-//IETF//DTD HTML//IT"">" 
					HtmlOperat = HtmlOperat & "<HtmlOperat>"
					HtmlOperat = HtmlOperat & "<BODY bgColor=#f0f8ff>"
					HtmlOperat = HtmlOperat & "<div=center>"
					HtmlOperat = HtmlOperat & "<table border=0 width=95% align=center cellpadding=1 bgColor=#f0f8ff cellspacing=2><tr><td colspan=4>"
					HtmlOperat = HtmlOperat & "<font size='3' color='#003466' face='Verdana'><br><i><b>Il Portale di ItaliaLavoro</b></i></font><br><br>"
					HtmlOperat = HtmlOperat & "<IMG SRC='http://www.italialavoro.it/ITALIALAVORO/images/logo_italia_lavoro.gif'><br><br>"
					HtmlOperat = HtmlOperat & "<font size='2' color='#003466' face='Verdana'><br>Si richiede l'abilitazione all'utilizzo dei servizi messi a disposizione dal Portale di Italia Lavoro. Si riportano, a tal proposito, i dati relativi all'Azienda:"
	'				HtmlOperat = HtmlOperat & " per esserti registrato/a al Portale di ItaliaLavoro."
	'				HtmlOperat = HtmlOperat & "Ti inviamo la Login e la Password da utilizzare per entrare "
	'				HtmlOperat = HtmlOperat & "nel Portale ed usufruire dei suoi  servizi."
					HtmlOperat = HtmlOperat & "<br><br><br>"
					HtmlOperat = HtmlOperat & "<tr><td width='25%'></td><td><font color=#003466 size='1' face=Verdana><b>Tipologia Impresa</b></font></td><td><font color=#003466 size='1' face=Verdana><b>" & TipologiaImpDesc & "</b></font></td><td width='10%'></td></tr>"
					HtmlOperat = HtmlOperat & "<tr><td></td><td><font color=#003466 size='1' face=Verdana><b>Ragione Sociale</b></font></td><td><font color=#003466 size='1' face=Verdana><b>" & RagioneSociale & "</b></font></td><td></td></tr>"
					HtmlOperat = HtmlOperat & "<tr><td></td><td><font color=#003466 size='1' face=Verdana><b>Forma Giuridica</b></font></td><td><font color=#003466 size='1' face=Verdana><b>" & FormaGiuridicaDesc & "</b></font></td><td></td></tr>"
					HtmlOperat = HtmlOperat & "<tr><td></td><td><font color=#003466 size='1' face=Verdana><b>Settore</b></font></td><td width='40%'><font color=#003466 size='1' face=Verdana><b>" & SettoreDesc & "</b></font></td><td></td></tr>"
					HtmlOperat = HtmlOperat & "<tr><td></td><td><font color=#003466 size='1' face=Verdana><b>Attivit�</b></font></td><td><font color=#003466 size='1' face=Verdana><b>" & Attivita & "</b></font></td><td></td></tr>"
					HtmlOperat = HtmlOperat & "<tr><td></td><td><font color=#003466 size='1' face=Verdana><b>Partita Iva</b></font></td><td><font color=#003466 size='1' face=Verdana><b>" & PartitaIva & "</b></font></td><td></td></tr>"
					HtmlOperat = HtmlOperat & "<tr><td></td><td><font color=#003466 size='1' face=Verdana><b>Codice Fiscale</b></font></td><td><font color=#003466 size='1' face=Verdana><b>" & CodiceFiscale & "</b></font></td><td></td></tr>"
					HtmlOperat = HtmlOperat & "<tr><td></td><td><font color=#003466 size='1' face=Verdana><b>Sito Web</b></font></td><td><font color=#003466 size='1' face=Verdana><b>" & SitoWeb & "</b></font></td><td></td></tr>"
					HtmlOperat = HtmlOperat & "<tr><td></td><td><font color=#003466 size='1' face=Verdana><b>Descrizione Sede</b></font></td><td><font color=#003466 size='1' face=Verdana><b>" & DescrizioneSede & "</b></td><td></td></tr>"
					HtmlOperat = HtmlOperat & "<tr><td></td><td><font color=#003466 size='1' face=Verdana><b>Tipo Sede</b></font></td><td><font color=#003466 size='1' face=Verdana><b>" & TipoSedeDesc & "</b></font></td><td></td></tr>"
					HtmlOperat = HtmlOperat & "<tr><td></td><td><font color=#003466 size='1' face=Verdana><b>Indirizzo</b></font></td><td><font color=#003466 size='1' face=Verdana><b>" & Indirizzo & "</b></font></td><td></td></tr>"
					HtmlOperat = HtmlOperat & "<tr><td></td><td><font color=#003466 size='1' face=Verdana><b>CAP</b></font></td><td><font color=#003466 size='1' face=Verdana><b>" & CAP & "</b></font></td><td></td></tr>"
					HtmlOperat = HtmlOperat & "<tr><td></td><td><font color=#003466 size='1' face=Verdana><b>Comune</b></font></td><td><font color=#003466 size='1' face=Verdana><b>" & Comune & "</b></font></td><td></td></tr>"
					HtmlOperat = HtmlOperat & "<tr><td></td><td><font color=#003466 size='1' face=Verdana><b>Provincia</b></font></td><td><font color=#003466 size='1' face=Verdana><b>" & ProvinciaDesc & "</b></font></td><td></td></tr>"
					HtmlOperat = HtmlOperat & "<tr><td></td><td><font color=#003466 size='1' face=Verdana><b>Centro Impiego</b></font></td><td><font color=#003466 size='1' face=Verdana><b>" & CentroImpiego & "</b></font></td><td></td></tr>"
					HtmlOperat = HtmlOperat & "<tr><td></td><td><font color=#003466 size='1' face=Verdana><b>Telefono</b></font></td><td><font color=#003466 size='1' face=Verdana><b>" & Telefono & "</b></font></td><td></td><td></td></tr>"
					HtmlOperat = HtmlOperat & "<tr><td></td><td><font color=#003466 size='1' face=Verdana><b>Fax</b></font></td><td><font color=#003466 size='1' face=Verdana><b>" & Fax & "</b></font></td><td></td></tr>"
					HtmlOperat = HtmlOperat & "<tr><td></td><td><font color=#003466 size='1' face=Verdana><b>Email</b></font></td><td><font color=#003466 size='1' face=Verdana><b>" & Email & "</b></font></td><td></td></tr>"
					HtmlOperat = HtmlOperat & "<tr><td></td><td><font color=#003466 size='1' face=Verdana><b>Cognome</b></font></td><td><font color=#003466 size='1' face=Verdana><b>" & Cognome & "</b></font></td><td></td></tr>"
					HtmlOperat = HtmlOperat & "<tr><td></td><td><font color=#003466 size='1' face=Verdana><b>Nome</b></font></td><td><font color=#003466 size='1' face=Verdana><b>" & Nome & "</b></font></td><td></td></tr>"	
					HtmlOperat = HtmlOperat & "<tr><td></td><td><font color=#003466 size='1' face=Verdana><b>Ruolo</b></font></td><td><font color=#003466 size='1' face=Verdana><b>" & CodiceRuoloDesc & "</b></font></td><td></td></tr>"
					HtmlOperat = HtmlOperat & "<tr><td></td><td><font color=#003466 size='1' face=Verdana><b>Login</b></font></td><td><font color=#003466 size='1' face=Verdana><b>" & Login & "</b></font></td><td></td></tr>"
					HtmlOperat = HtmlOperat & "</table></div></body>"
					HtmlOperat = HtmlOperat & "</html>"
					'Response.Write(HtmlOperat)
					objMailOperator.BodyFormat = cdoBodyFormatHTML 
					objMailOperator.MailFormat = cdoMailFormatMIME
					objMailOperator.Body = HtmlOperat 
					objMailOperator.Send "ItaliaLavoro<Infoaziende@italialavoro.it>", "ItaliaLavoro<Infoaziende@italialavoro.it>", "Richiesta Adesione Azienda Portale Lavoro", HtmlOperat
					
					'----- Mail per Referenze Azienda ITALIA LAVORO------
					Set objMailRefer = Server.CreateObject("CDONTS.NewMail")
					HtmlRefer = HtmlRefer & "<HTML>"
					HtmlRefer = HtmlRefer & "<BODY bgColor=#f0f8ff>"
					HtmlRefer = HtmlRefer & "<div=center>"
					HtmlRefer = HtmlRefer & "<table border=0 width=95% align=center cellpadding=1 bgColor=#f0f8ff cellspacing=2 ><tr><td>"
					HtmlRefer = HtmlRefer & "<font size='3' color='#000080' face='Verdana'><br><i><b>Il Portale di ItaliaLavoro</b></i></font><br><br>"
					HtmlRefer = HtmlRefer & "<IMG SRC='http://www.italialavoro.it/ITALIALAVORO/images/logo_italia_lavoro.gif'><br><br>"
					HtmlRefer = HtmlRefer & "<font size='2' color='#000080' face='Verdana'><br>Gentile " & Cognome & " " & Nome & ",<br><br>ti ringraziamo"
					HtmlRefer = HtmlRefer & " per esserti registrato/a al Portale di ItaliaLavoro.<br>"
					HtmlRefer = HtmlRefer & "Ti inviamo la Login e la Password, al momento per� puoi utilizzare solo i servizi base.<br>"
					HtmlRefer = HtmlRefer & "Per usufruire di tutti i servizi, dovrai aspettare che siano convalidate le informazioni "
					HtmlRefer = HtmlRefer & "riguardanti la tua azienda.<br>Ti invieremo un'email di notifica quando il servizio sar� attivo"
					HtmlRefer = HtmlRefer & "<br><br><br>"
					HtmlRefer = HtmlRefer & "<b>LOGIN: " & Login & "</b><BR><b>PASSWORD: " & Password & "</b>"
					HtmlRefer = HtmlRefer & "<br><br>A presto</br>"
					HtmlRefer = HtmlRefer & "<br><i>La Redazione di ItaliaLavoro</i> ( <a href=http://www.ItaliaLavoro.it>www.ItaliaLavoro.it</a> )<br><br></br></td></tr></font>"
					HtmlRefer = HtmlRefer & "</table></div></BODY></HTML>"
					'Response.Write(HtmlRefer)
					objMailRefer.BodyFormat = cdoBodyFormatHTML 
					objMailRefer.MailFormat = cdoMailFormatMIME
					objMailRefer.Body = HtmlRefer 
					objMailRefer.Send "ItaliaLavoro<InfoAziende@italialavoro.it>", Email, "Adesione Portale Lavoro", HtmlRefer

		Else 
		
				%>
				<table border="0" width="520" cellspacing="0" cellpadding="0" height="81">
				   <tr>
				     <td width="520" background="<%=Session("Progetto")%>/images/titoli/zstrumenti_grigio.gif" height="81" valign="bottom" align="right">
				       <table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
				         <tr>
				           <td width="100%" valign="top" align="right"><b class="tbltext1a">Registrazione Azienda &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></b></td>
				         </tr>
				       </table>
				     </td>
				   </tr>
				</table>
				<br>
				<table cellpadding="0" cellspacing="0" width="350" border="0">
					<tr>
						<td width="30%"></td>
						<td width="3%"></td>
						<td width="50%"></td>
					</tr>
				   <tr>
				      <td width="57%" colspan="3" align="center"><br><span class="tbltext3">Registrazione non correttamente effettuata.<br></span>
						<br></td> 
				   </tr>   
					
				</table>
				<table WIDTH="370" ALIGN="center" BORDER="0" CELLSPACING="1" CELLPADDING="1">
					<tr>
						<td></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td align="center"><a HREF="javascript:history.back()"><img SRC="<%=Session("Progetto")%>/images/indietro.gif" border="0"></a></td>
					</tr>
				</table>
				<%
				cc.RollbackTrans

		End If
	
	'	else
			%>
			<!--table border="0" width="520" cellspacing="0" cellpadding="0" height="81">			   <tr>			     <td width="520" background="<%=Session("Progetto")%>/images/titoli/zstrumenti_grigio.gif" height="81" valign="bottom" align="right">			       <table border="0" width="260" height="30" cellspacing="0" cellpadding="0">			         <tr>			           <td width="100%" valign="top" align="right"><b class="tbltext1a">Registrazione Azienda &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></b></td>			         </tr>			       </table>			     </td>			   </tr>			</table>			<br>			<table cellpadding="0" cellspacing="0" width="350" border="0">				<tr>					<td width="30%"></td>					<td width="3%"></td>					<td width="50%"></td>				</tr>			   <tr>			      <td width="57%" colspan="3" height="18" align="center"><br><span class="tbltext3">Controllare il comune <%=Comune%> e la provincia <%=ProvinciaDesc%>.</span><br></td> 			   </tr>				<tr height="18">					<td colspan="3"></td>				</tr>			</table>			<table WIDTH="370" ALIGN="center" BORDER="0" CELLSPACING="1" CELLPADDING="1">				<tr>					<td></td>				</tr>				<tr>					<td>&nbsp;</td>				</tr>				<tr>					<td align="center"><a HREF="javascript:history.back()"><img SRC="<%=Session("Progetto")%>/images/indietro.gif" border="0"></a></td>				</tr>			</table-->
			<%
	'	end if
	
Else
		
		%>
		<table border="0" width="520" cellspacing="0" cellpadding="0" height="81">
		   <tr>
		     <td width="520" background="<%=Session("Progetto")%>/images/titoli/zstrumenti_grigio.gif" height="81" valign="bottom" align="right">
		       <table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
		         <tr>
		           <td width="100%" valign="top" align="right"><b class="tbltext1a">Registrazione Azienda &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></b></td>
		         </tr>
		       </table>
		     </td>
		   </tr>
		</table>
		<br>
		<table cellpadding="0" cellspacing="0" width="350" border="0">
		   <tr>
				<td width="30%"></td>
				<td width="3%"></td>
				<td width="50%"></td>
		   </tr>
		   <tr>
		      <td width="57%" colspan="3" height="18" align="center"><br><span class="tbltext3">Login gi� presente.<br>Prego inserirne un'altra.</span><br></td> 
		   </tr>
			<tr height="18">
				<td colspan="3"></td>
			</tr>
		</table>
		<table WIDTH="370" ALIGN="center" BORDER="0" CELLSPACING="1" CELLPADDING="1">
			<tr>
				<td></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td align="center"><a HREF="javascript:history.back()"><img SRC="<%=Session("Progetto")%>/images/indietro.gif" border="0"></a></td>
			</tr>
		</table>
		<%
		
End if
rstAziende.Close 
rstSettore.Close

Set rstSettore = Nothing
Set rstAziende = Nothing

%>

<!--#include Virtual = "/include/CloseConn.asp"-->
<!--#include Virtual="strutt_coda2.asp"-->
