<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include Virtual = "strutt_testa2.asp"-->
<!--#include Virtual = "include/DecCod.asp"-->
<!--#include Virtual = "include/OpenConn.asp"-->
<!--#include Virtual = "/include/ControlDateVB.asp"-->
<!--#include Virtual = "/util/dbutil.asp"-->

<script LANGUAGE="Javascript">


//include del file per fare i controlli sulla numericit� dei campi
<!--#include Virtual = "/Include/ControlNum.inc"-->

//include del file per fare i controlli sulla stringa
<!--#include Virtual = "/Include/ControlString.inc"-->

<!--#include virtual ="/include/help.inc"-->
	
	//Funzione per i controlli dei campi 
	function ControllaDati(){
// -------- CONTROLLO VALORI INSERITI -------------------------		


		if (frmIscri.elements[4].selectedIndex == 0) {
			alert ("Il campo Ruolo � obbligatorio!")
			frmIscri.elements[4].focus()
			return false
		}
		
		document.frmIscri.txtRefEmail.value=TRIM(document.frmIscri.txtRefEmail.value)
		if (document.frmIscri.txtRefEmail.value ==""){
			alert("Il campo Email � obbligatorio!")
			document.frmIscri.txtRefEmail.focus() 
			return false
		}	

		pluto=ValidateEmail(document.frmIscri.txtRefEmail.value)
		if (pluto==false){
			alert("Indirizzo Email formalmente errato.")
			document.frmIscri.txtRefEmail.focus() 
			return false
		}		
		
		document.frmIscri.txtDescAtt.value=TRIM(document.frmIscri.txtDescAtt.value)
		if (frmIscri.txtDescAtt.value == ""){
			alert("Il campo Attivit� � obbligatorio!")
			frmIscri.txtDescAtt.focus() 
			return false
		}
		
		document.frmIscri.txtSitoWeb.value=TRIM(document.frmIscri.txtSitoWeb.value)

		document.frmIscri.txtIndirizzo.value=TRIM(document.frmIscri.txtIndirizzo.value)
		if (frmIscri.txtIndirizzo.value == ""){
			alert("Il campo Indirizzo � obbligatorio!")
			frmIscri.txtIndirizzo.focus() 
			return false
		}
		
		document.frmIscri.txtCap.value=TRIM(document.frmIscri.txtCap.value)
		if (frmIscri.txtCap.value == ""){
			alert("Il campo CAP � obbligatorio!")
			frmIscri.txtCap.focus() 
			return false
		}		
		
		if (!IsNum(frmIscri.txtCap.value)){
			alert("Il valore deve essere numerico!")
			frmIscri.txtCap.focus() 
			return false
		}
		
		//CAP deve essere diverso da zero
		if (document.frmIscri.txtCap.value != ""){
			if (eval(document.frmIscri.txtCap.value) == 0){
				alert("Il CAP non pu� essere zero!")
				document.frmIscri.txtCap.focus()
				return false
			}
		}
		
		//CAP deve essere di 5 caratteri
		if (document.frmIscri.txtCap.value != ""){
			if (document.frmIscri.txtCap.value.length != 5){
				alert("Formato del CAP errato.")
				document.frmIscri.txtCap.focus() 
				return false
			}
		}				
			
		document.frmIscri.txtSedeTel.value = TRIM(document.frmIscri.txtSedeTel.value)
		if (!IsNum(frmIscri.txtSedeTel.value)){
			alert("Il valore deve essere numerico!")
			frmIscri.txtSedeTel.focus() 
			return false
		}
		
		if (document.frmIscri.txtSedeTel.value != ""){
			if (eval(document.frmIscri.txtSedeTel.value) == 0){
				alert("Il numero di telefono non pu� essere zero!")
				document.frmIscri.txtSedeTel.focus()
				return false
			}
		}		
				
		document.frmIscri.txtSedeFax.value=TRIM(document.frmIscri.txtSedeFax.value)				
		if (!IsNum(frmIscri.txtSedeFax.value)){
			alert("Il valore deve essere numerico!")
			frmIscri.txtSedeFax.focus() 
			return false
		}	
		
		if (document.frmIscri.txtSedeFax.value != ""){
			if (eval(document.frmIscri.txtSedeFax.value) == 0){
				alert("Il numero di fax non pu� essere zero!")
				document.frmIscri.txtSedeFax.focus()
				return false
			}
		}			
		
		//Email
/*		
		document.frmIscri.txtSedeEmail.value=TRIM(document.frmIscri.txtSedeEmail.value)
		if (document.frmIscri.txtSedeEmail.value == ""){
			alert("Il campo Email � obbligatorio!")
			document.frmIscri.txtSedeEmail.focus() 
			return false
		}
*/		
		document.frmIscri.txtSedeEmail.value=TRIM(document.frmIscri.txtSedeEmail.value)
		if (document.frmIscri.txtSedeEmail.value == ""){
		}
		else{ 
			appo=ValidateEmail(document.frmIscri.txtSedeEmail.value)
			if (appo == false){
				alert("Indirizzo Email formalmente errato.")
				document.frmIscri.txtSedeEmail.focus() 
				return false
			}		
		}
/*		
		pippo=ValidateEmail(document.frmIscri.txtSedeEmail.value)
		if (pippo==false){
			alert("Indirizzo Email formalmente errato.")
			document.frmIscri.txtSedeEmail.focus() 
			return false
		}
*/		
		return true
	}	

</script>

<%
dim rsLoadDati

dim TipoImp
dim TipoImpVal
dim RagSoc
dim FormGiurVal
dim CodSettoreVal
dim Attivita
dim PIva
dim CodFis
dim SitoWeb

dim sDescSede
dim sTipoSedeVal
dim sIndirizzo
dim sCap
dim sComune
dim sProvinciaVal
dim sTel
dim sFax
dim sEmailSede

dim sRef
dim sRefSplit
dim sRefCognome
dim sRefNome
dim CodRuoVal
dim sRefEmail

dim sqlLoadDati

dim sIdSede
dim idUte
dim rsIdSede
dim sIdImpresa

if Session("CREATOR") <> 0 then

	sqlLoadDati = "SELECT IMPRESA.ID_IMPRESA, IMPRESA.COD_TIMPR, IMPRESA.RAG_SOC, IMPRESA.COD_FORMA, " &_
			"IMPRESA.ID_SETTORE, IMPRESA.DESC_ATT, IMPRESA.PART_IVA, IMPRESA.COD_FISC, " &_
			"IMPRESA.SITOWEB, SEDE_IMPRESA.DESCRIZIONE, SEDE_IMPRESA.ID_SEDE, " &_
			"SEDE_IMPRESA.COD_SEDE, SEDE_IMPRESA.INDIRIZZO, SEDE_IMPRESA.CAP, " &_
			"SEDE_IMPRESA.COMUNE, SEDE_IMPRESA.PRV, SEDE_IMPRESA.NUM_TEL_SEDE, SEDE_IMPRESA.E_MAIL, " &_
			"SEDE_IMPRESA.FAX_SEDE, SEDE_IMPRESA.E_MAIL_SEDE, SEDE_IMPRESA.DT_TMST AS TmstSEDE," &_
			"SEDE_IMPRESA.REF_SEDE, SEDE_IMPRESA.COD_RUO, SEDE_IMPRESA.ID_CIMPIEGO, IMPRESA.DT_TMST AS TmstIMP FROM IMPRESA, " &_
			"UTENTE, SEDE_IMPRESA WHERE UTENTE.CREATOR = ID_SEDE AND IMPRESA.ID_IMPRESA = " &_
			"SEDE_IMPRESA.ID_IMPRESA AND TIPO_PERS = 'S' AND UTENTE.IND_ABIL = 'S' AND SEDE_IMPRESA.ID_SEDE = " & Session("CREATOR")

'Response.Write sqlLoadDati
'Response.End 

'PL-SQL * T-SQL  
SQLLOADDATI = TransformPLSQLToTSQL (SQLLOADDATI) 
	Set rsLoadDati = cc.execute(sqlLoadDati)


	TipoImpVal = rsLoadDati("COD_TIMPR")
	RagSoc = rsLoadDati("RAG_SOC")
	FormGiurVal = rsLoadDati("COD_FORMA")
	CodSettoreVal = rsLoadDati("ID_SETTORE")
	Attivita = rsLoadDati("DESC_ATT")
	PIva = rsLoadDati("PART_IVA")
	CodFis = rsLoadDati("COD_FISC")
	SitoWeb = rsLoadDati("SITOWEB")
	sDescSede = rsLoadDati("DESCRIZIONE")
	sTipoSedeVal = rsLoadDati("COD_SEDE")
	sIndirizzo = rsLoadDati("INDIRIZZO")
	sCap = rsLoadDati("CAP")
	sComune = rsLoadDati("COMUNE")
	sProvinciaVal = rsLoadDati("PRV")
	sTel = rsLoadDati("NUM_TEL_SEDE")
	sFax = rsLoadDati("FAX_SEDE")
	sEmailSede = rsLoadDati("E_MAIL_SEDE")
	sRefEmail = rsLoadDati("E_MAIL")
	sRef = rsLoadDati("REF_SEDE")
	CodRuoVal = rsLoadDati("COD_RUO")
	sIdImpresa = rsLoadDati("ID_IMPRESA")
	
	sTmstIMP = rsLoadDati("TmstIMP")
	sTmstSEDE = rsLoadDati("TmstSEDE")

	IdCimpiego = rsLoadDati("ID_CIMPIEGO")

	if IdCimpiego <> "" then
		sSQL="SELECT DESCRIZIONE " &_
			 "FROM SEDE_IMPRESA " &_
			 "WHERE ID_SEDE = " & IdCimpiego
'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
		Set rstCimpiego = CC.execute(sSQL)
		DescCimpiego=rstCimpiego("DESCRIZIONE")
		rstCimpiego.Close()
	else
		DescCimpiego=""
	end if

	'Splittare il nome e il cognome del referente dato che nel DB
	'sono memorizzati in un unico campo NOME COGNOME
	'------------------------------------------------------------
	sRefSplit = Split(sRef)

	if sRefSplit(0) = "" then
		sRefSplit(0) = ProvaNOME
	end if

	if sRefSplit(1) = "" then
		sRefSplit(1) = ProvaCOGNOME
	end if

	sRefNome = sRefSplit(0)
	sRefCognome = sRefSplit(1)
	'------------------------------------------------------------
	%>
	
	<div align="center">
	  <center>
	<table border="0" width="525" cellspacing="0" cellpadding="0" height="81">
	   <tr>
	     <td width="525" background="<%=Session("Progetto")%>/images/titoli/strumenti2g.gif" height="81" valign="bottom" align="right">
	       <table border="0" width="260" height="30" cellspacing="0" cellpadding="0">
	         <tr>
	           <td width="100%" valign="top" align="right"><span class="tbltext1a"><b>Dati Aziendali &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></span></td>
	         </tr>
	       </table>
	     </td>
	   </tr>
	</table><br>
	<form action="IMP_CnfDatiImpresa.asp?idImpresa=<%=sIdImpresa%>&amp;idSede=<%=Session("CREATOR")%>" method="post" name="frmIscri" id="frmIscri">
	<input type="hidden" id="text2" size="50" name="txtTmstIMP" value="<%=sTmstIMP%>">
	<input type="hidden" id="text2" size="50" name="txtTmstSEDE" value="<%=sTmstSEDE%>">
	
<table cellpadding="0" cellspacing="0" width="500" border="0">
   <tr height="18">
		<td class="sfondomenu" height="18" width="67%">
		<span class="tbltext0"><b>&nbsp;AZIENDA</b></span></td>
		
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif">&nbsp;</td>
		<td valign="middle" align="right" width="50%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif" class="tbltext1"></td>
   </tr>
   <tr width="371" class="SFONDOCOMMAZ">
	<td colspan="3">Modifica dei dati personali del referente dichiarati al momento dell'iscrizione. <br>
				Premere <b>Invia</b> per salvare le modifiche.
			<a href="Javascript:Show_Help('/Pgm/help/QImprese/Imp_ModDatiImpresa')"><img align="right" src="<%=Session("Progetto")%>/images/help.gif" border="0"></a>
	</td>
	</tr>
	<tr height="2">
		<td colspan="3" class="SFONDOCOMMAZ" background="<%=Session("Progetto")%>/images/separazione.gif">
		</td>
	</tr>
</table>
	
	
	
	
	
	<br>
	<!---------- Inizio Tabella Inserimento ---------->
	<table border="0" width="500" colspan="0" cellpadding="0" cellspacing="0">   
	   <tr>
			<td class="sfondocommaz" COLSPAN="2" width="500">
				<span class="tbltext1a"><b>Referente</b></span>
			</td>
		</tr>
		<tr>
			<td colspan="2" height="7"></td>
		</tr>     
		<tr>
			<td align="left" class="tbltext1" nowrap width="50"><b>
				&nbsp;Cognome </b>	
			</td>
			<td align="left">
				<input style="TEXT-TRANSFORM: uppercase;" size="58" class="textblack" maxlength="50" name="txtCognome" value="<%=sRefCognome%>" readonly>
			</td>
	   </tr> 
		<tr>
			<td align="left" class="tbltext1" nowrap><b>
				&nbsp;Nome </b>		
			</td>
			<td align="left">
				<input style="TEXT-TRANSFORM: uppercase;" size="58" class="textblack" maxlength="50" name="txtNome" value="<%=sRefNome%>" readonly>
			</td>
	   </tr> 	
		<tr>
			<td align="left" class="tbltext1" nowrap><b>
				&nbsp;Ruolo *</b>	
			</td>
			<td align="left">
				<%
					'Richiamo funzione che popola la combo TIPO SEDE
					'-----------------------------------------------
					sCodRuo = "RUOLO|0|" & date & "|" & CodRuoVal & "|cmbCodRuo|ORDER BY DESCRIZIONE"			
					CreateCombo(sCodRuo)
					'-----------------------------------------------
				%>	
			</td>
	   </tr>
	   <tr>
			<td align="left" class="tbltext1" nowrap><b>
				&nbsp;E-mail *</b>		
			</td>
			<td align="left">
				<input size="49" class="textblack" maxlength="50" name="txtRefEmail" value="<%=sRefEmail%>">
				<input type="hidden" size="49" class="textblack" maxlength="50" name="txtRefEmailOri" value="<%=sRefEmail%>">
			</td>
	   </tr>    
	   <tr>
			<td>&nbsp;</td>
	   </tr>
	   <tr>
			<td colspan="2" class="sfondocommaz" width="500">
				<span class="tbltext1a"><b>Impresa</b></span>
			</td>
		</tr>
		<tr>
			<td colspan="2" height="7"></td>
		</tr> 
		<tr>
			<td align="left" class="tbltext1" nowrap><b>
				&nbsp;Tipologia Impresa </b>
			</td>
			<td align="left">
		  		<span class="tbltext">
				<%
					dim TipoImpresa
			
					TipoImpresa = DecCodVal("TIMPR", IsaVal, dt_decorr, TipoImpVal, 1)
				%>	
				<input style="TEXT-TRANSFORM: uppercase;" size="58" class="textblack" maxlength="50" name="txtTipoImp" value="<%=TipoImpresa%>" readonly>
				</span>
			</td>
		</tr>				
		<tr>
			<td align="left" class="tbltext1" nowrap><b>
				&nbsp;Ragione Sociale</b> 	
			</td>
			<td align="left">
				<input style="TEXT-TRANSFORM: uppercase;" size="58" class="textblack" maxlength="50" name="txtRagSoc" value="<%=RagSoc%>" readonly>
			</td>
	   </tr>
	   <tr>
			<td align="left" class="tbltext1" nowrap><b>
				&nbsp;Tipo </b>
			</td>
			<td align="left" colspan="2" width="60%">
		  		<span class="tbltext">
				<%
				dim CodForm
			
				CodForm = DecCodVal("FGIUR", IsaVal, dt_decorr, FormGiurVal, 1)
				%>	
				<input style="TEXT-TRANSFORM: uppercase;" size="58" class="textblack" maxlength="50" name="txtCodForm" value="<%=CodForm%>" readonly>
				</span>
			</td>
		</tr>
	   <tr>
			<td align="left" class="tbltext1" nowrap><b>
				&nbsp;Settore </b>
	      </td>
			<td align="left">
				<%
					'Effettuo una SELECT per popolare la combo SETTORE
					'-------------------------------------------------
					Dim rsRec
					Dim Id
					Dim Name
					
					Id = cmbCodSet
					Name = cmbCodSet
					
					'Lancia una select SQL 
					sSQL = "SELECT id_settore, denominazione FROM SETTORI " &_
							"WHERE id_settore = " & clng(CodSettoreVal)

'PL-SQL * T-SQL  
SSQL = TransformPLSQLToTSQL (SSQL) 
					set rsRec = CC.Execute(sSQL)
				%>
				
				<input style="TEXT-TRANSFORM: uppercase;" size="58" class="textblack" maxlength="50" name="txtSettore" value="<%=rsRec("DENOMINAZIONE")%>" readonly>
					
				<%
					rsRec.Close
					
					set rsRec = Nothing
				%>
			</td>
		</tr>
	   <tr>
	      <td align="left" class="tbltext1" nowrap><b>
				&nbsp;Attivit� *</b>
	      </td>
	      <td align="left">
	          <input size="36" class="textblack" maxlength="34" name="txtDescAtt" style="TEXT-TRANSFORM: uppercase;" value="<%=Attivita%>">
	      </td>
	   </tr>
		<tr>
			<td align="left" class="tbltext1" nowrap><b>
				&nbsp;P.IVA </b>
			</td>
			<td align="left">
				<input style="TEXT-TRANSFORM: uppercase;" class="textblack" size="15" maxlength="11" name="txtPartIva" value="<%=PIva%>" readonly>
			</td>
	   </tr>
		<tr>
			<td align="left" class="tbltext1"><b>
				&nbsp;Codice fiscale<b>
			</td>
			<td align="left">
				<input style="TEXT-TRANSFORM: uppercase;" size="36" class="textblack" maxlength="16" name="txtCodFis" value="<%=CodFis%>" readonly>
			</td>
	   </tr>
		<tr>
			<td align="left" class="tbltext1" nowrap><b>
				&nbsp;Sito Web</b>
			</td>
			<td align="left">
				<input style="TEXT-TRANSFORM: uppercase;" size="58" class="textblack" maxlength="50" name="txtSitoWeb" value="<%=SitoWeb%>">
			</td>
	   </tr>
	   <tr>
			<td align="middle" colspan="2">&nbsp;</td>
	   </tr>
	   <tr>
			<td class="sfondocommaz" COLSPAN="2" width="510">
				<span class="tbltext1a"><b>Sede</b></span>
			</td>
		</tr>
		<tr>
			<td colspan="2" height="7"></td>
		</tr>  		   
		<tr>
			<td align="left" class="tbltext1" nowrap><b>
				&nbsp;Descrizione Sede </b>	
			</td>
			<td align="left">
				<input style="TEXT-TRANSFORM: uppercase;" size="58" class="textblack" maxlength="50" name="txtDescrizione" value="<%=sDescSede%>" readonly>
			</td>
	   </tr>
		<tr>
			<td align="left" class="tbltext1" nowrap><b>
				&nbsp;Tipo Sede </b>		
			</td>
			<td align="left" colspan="2" width="60%">
		  		<span class="tbltext">
				<%
					dim CodSede
			
					CodSede = DecCodVal("TSEDE", IsaVal, dt_decorr, sTipoSedeVal, 1)
				%>	
				<input style="TEXT-TRANSFORM: uppercase;" size="58" class="textblack" maxlength="50" name="txtSede" value="<%=CodSede%>" readonly>
				</span>
			</td>
		</tr>
		<tr>
			<td align="left" class="tbltext1" nowrap><b>
				&nbsp;Indirizzo *</b>		
			</td>
			<td align="left">
				<input style="TEXT-TRANSFORM: uppercase;" size="58" class="textblack" maxlength="50" name="txtIndirizzo" value="<%=sIndirizzo%>">
			</td>
	   </tr>
		<tr>
			<td align="left" class="tbltext1" nowrap><b>
				&nbsp;CAP *</b>
			</td>
			<td align="left">
				<input style="TEXT-TRANSFORM: uppercase;" size="15" class="textblack" maxlength="5" name="txtCap" value="<%=sCap%>">
			</td>
	   </tr>
		<tr>
			<td align="left" class="tbltext1" nowrap><b>
				&nbsp;Comune </b>		
			</td>
			<td align="left">
					<%
					dim rsComune
					dim descComune
					
					dim SqlComune
					
					if sComune <> "" then			
						SqlComune = "SELECT DESCOM from COMUNE WHERE CODCOM = '" & sComune & "'"
				        
'PL-SQL * T-SQL  
SQLCOMUNE = TransformPLSQLToTSQL (SQLCOMUNE) 
				      set rsComune = CC.Execute(SqlComune)
						   
						descComune = rsComune("DESCOM") 
					
						rsComune.Close
						
					   Set rsComune = Nothing
					end if
					%>
				<input style="TEXT-TRANSFORM: uppercase;" size="49" class="textblack" maxlength="50" name="txtComune" value="<%=descComune%>" readonly>
			</td>
	   </tr>
		<tr>
			<td align="left" class="tbltext1" nowrap><b>
				&nbsp;Provincia </b>	
			</td>
			<td align="left" colspan="2" width="60%">
		  		<span class="tbltext">
				<%
				dim CodProv
			
				CodProv = DecCodVal("PROV", IsaVal, dt_decorr, sProvinciaVal, 1)
				%>	
				<input style="TEXT-TRANSFORM: uppercase;" size="49" class="textblack" maxlength="50" name="txtProv" value="<%=CodProv%>" readonly>
				</span>
			</td>
	   </tr>  
		<tr>
			<td align="left" class="tbltext1" nowrap><b>
				&nbsp;Telefono </b>	
			</td>
			<td align="left">
				<input style="TEXT-TRANSFORM: uppercase;" size="28" class="textblack" maxlength="20" name="txtSedeTel" value="<%=sTel%>">
			</td>
	   </tr>  
		<tr>
			<td align="left" class="tbltext1" nowrap><b>
				&nbsp;Fax		
			</b></td>
			<td align="left">
				<input style="TEXT-TRANSFORM: uppercase;" size="28" class="textblack" maxlength="20" name="txtSedeFax" value="<%=sFax%>">
			</td>
	   </tr>  
		<tr>
			<td align="left" class="tbltext1" nowrap><b>
				&nbsp;E-mail</b>		
			</td>
			<td align="left">
				<input size="49" class="textblack" maxlength="50" name="txtSedeEmail" value="<%=sEmailSede%>">
			</td>
	    </tr>
<%
	if TipoImpVal <> "03" then
%>	      
		<tr>
			<td align="left" class="tbltext1" nowrap><b>
				&nbsp;Centro Impiego</b>		
			</td>
			<td align="left">
				<input size="49" class="textblack" maxlength="50" name="txtCentroImpiego" value="<%=DescCimpiego%>" readonly>
			</td>
	   </tr>  
<%
	end if
%>
	</table>

	<!---------- Fine Tabella Inserimento ---------->

	<br>     
	<table border="0" cellpadding="0" cellspacing="0" width="510" align="center">
		<tr>
			<td align="middle" colspan="2"><input type="image" name="Invia" src="<%=Session("Progetto")%>/images/conferma.gif" border="0" value="Conferma" onclick="return ControllaDati()"></td>
	   </tr>
		<tr>
			<td>&nbsp;</td>
	   </tr>
		<tr>
			<td height="2" background="<%=Session("Progetto")%>/images/separazione.gif"></td>
	    </tr>
	</table>
	</form>
	</center>
	</div>
	<%

rsLoadDati.Close	

Set rsLoadDati = Nothing
	
else 

	%>
	<table border="0" width="525" cellspacing="0" cellpadding="0" height="81">
	   <tr>
	     <td width="525" background="<%=Session("Progetto")%>/images/titoli/strumenti2g.gif" height="81" valign="bottom">
	       <table border="0" width="510" height="30" cellspacing="0" cellpadding="0">
	         <tr>
	           <td width="100%" valign="top" align="right"><span class="tbltext1a"><b>Adesione Azienda&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></span></td>
	         </tr>
	       </table>
	     </td>
	   </tr>
	</table>
	<br>
	<table cellpadding="0" cellspacing="0" width="510" border="0">
		<tr>
			<td width="30%"></td>
			<td width="3%"></td>
			<td width="50%"></td>
		</tr>
	   <tr>
			<td width="57%" colspan="3" align="center"><br><span class="tbltext3">
				Dati non presenti.<br>
				Impossibile effettuare la modifica</span>
				<br>
			</td> 
	   </tr>   
	</table>
	<table WIDTH="510" ALIGN="center" BORDER="0" CELLSPACING="0" CELLPADDING="0">
		<tr>
			<td height="40"></td>
		</tr>
		<tr>
			<td align="center"><a HREF="javascript:history.back()"><img SRC="<%=Session("Progetto")%>/images/indietro.gif" border="0"></a></td>
		</tr>
	</table>
	<%

end if

%>
<!--#include Virtual = "/include/closeconn.asp"-->
<!--#include Virtual="/strutt_coda2.asp"-->
