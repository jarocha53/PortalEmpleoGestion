<!--#include Virtual = "/strutt_testa1.asp"-->
<!-- #include virtual="/include/OpenConn.asp" -->
<!--#include file = "BDD_Str1Menu.asp"-->

<script LANGUAGE="JavaScript">
function descCom()
{
	w=(screen.width-600)/2;	
	h=(screen.height-400)/2;
	fin=window.open("BDD_Suggerimenti.asp","","minimize=0,maximize=0,toolbar=0,location=0,directories=0,status=0,menubar=0,scrollbars=yes,resizable=0,copyhistory=0,width=550,height=380,screenX=100,screenY100");	
}
</script>

<%
SP = Session("Progetto")  
If Session("Login") & "*" = "*" then
	Session("mask") = "111" & mid(SP,2)
else
	If Not ValidateService(Session("IdUtente"),"BDD_Ricerca",cc) Then 
		response.redirect "/util/error_login.asp"
	End If
End If
%>  
<script language="Javascript" src="Controlli.js"></script>
<script language="Javascript">

		function viewlink(URL,CAMPO,DESCR,TAB,VAL)
		{
			var sValue
			sValue = document.all.item(CAMPO).innerText
			URL = URL + "?descr=" + DESCR + "&tab=" + TAB + "&val=" + VAL + "&campo=" + CAMPO + "&id=" + sValue  
			window.open ( URL ,"Rubrica","Status=no,scrollbars=yes,toolbar=no,width=550,height=500,top=20,left=100")
		}


	  function invia()
	  {
	  	if ((window.document.forma.parola.value == "")||
	  	(window.document.forma.parola.value == " "))
	  	{
	  	alert("Il campo parola chiave � obbligatorio!")
	  	window.document.forma.parola.focus() 
	  	return false
		}		

	  	if (window.document.forma.parola.value.length < 3 )
	  	{
	  	alert("La parola deve essere composta da almeno 3 caratteri")
	  	window.document.forma.parola.focus() 
	  	return false
		}		
	  	
	      return true
	  }


	function inizio()
	{
		window.document.forma.imgprov.style.display = "none";
		window.document.forma.imgreg.style.display = "none";
		window.document.forma.imgeur.style.display = "none";
	}

	//-->
	</script>

<!--- TITOLO PAGINA ----><br>
<table border="0" width="510" cellspacing="0" cellpadding="0" height="30">
	<tr>
		<td width="510" background="<%=Session("Progetto")%>/images/titoli/barracerca.jpg" valign="bottom" align="right">
			<table border="0" width="90%" height="30" cellspacing="0" cellpadding="0">
				<tr>
					<td width="100%" valign="middle" align="right"><b class="tbltext1a">DATA SPINN - Ricerca semplice per parola&nbsp;&nbsp;</span></b></td>
				</tr>
			</table>
		</td>
	</tr>
</table><br>

<table cellSpacing="4" cellPadding="4" width="500" align="center" border="0">
	<tr>
		<% if mid(Session("mask"),2,1) = "0"  then%>
		<td align="right"><a href="BDD_Contatore.asp" class="textreda"><b>Contatori</b></a></td>
		<%end if %>	
	</tr>  
</table>

<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
	<tr height="15">
		<td width="40%" align="center"><a class="textred" HREF="BDD_Ricerca_Av.asp"><b>Ricerca avanzata</b></a><% if mid(Session("mask"),2,1) = "0"  then%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="textred" HREF="BDD_RisPubblicaLast.asp"><b>Documenti da Pubblicare</b></a><%end if%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="textred" HREF="BDD_RisRicercaLast.asp"><b>Ultime pubblicazioni</b></a>
</td>
	</tr>  
</table><br>

<form method="post" name="forma" action="BDD_RisRicerca.asp" onsubmit="return invia()">
<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
	<tr height="15">
		<td width="55%"><strong><a class="tbltext" href="javascript:descCom();"> <u>Singole parole da cercare : </u></a></strong>
			<input type="text" id="parola" name="parola" size="53">
		</td>
		<td width="45%" class="tbltext">
			&nbsp;
		</td>
	</tr>  
    <tr><td colspan="2" class="tbltext">
		<b>Attenzione:</b><br>La ricerca semplice prevede la ricerca di parole e non di frasi. 
		<br>Esempio: Se si vuole cercare la frase &quot;notizie del giorno&quot; bisogna digitare <i>notizie giorno</i> omettendo articoli e preposizioni, oppure effettuare la ricerca avanzata digitando l'intera frase in <i>frase da cercare</i>. 
	    <br><br>
    </td></tr>
</table>
<br>

<!-- TABELLA CON TUTTI GLI ARGOMENTI NEI QUALI E' POSSIBILE EFFETTUARE LA RICERCA -->


<!--- TUTTE LE NORMATIVE ---->
<table cellSpacing="1" cellPadding="1" width="500" align="center" border="0">
	<tr>
		<td colspan="2" class="tbltext"><strong>Argomenti di ricerca (*)</strong></td>
	</tr>
	<tr>
		<td colspan="4" class="tbltext">Di seguito selezionare i parametri con i quali cercare il documento. E' possibile indicare tutti gli argomenti di una singola categoria, selezionando il relativo check <input type="checkbox" checked readonly disabled id="checkbox1" name="checkbox1"> che troverai accanto ad ogni voce, oppure scegliere nel dettaglio premendo il pulsante <img alt src="/images/bulletagg.gif" border="0" WIDTH="70" HEIGHT="14">.<br>Gli argomenti individuati compariranno automaticamente sotto la voce selezionata.<br><br></td>
	</tr>  
	<tr>
		<td width="2%"><input class="tbltext" type="checkbox" id="chknorma" value="chknorma" name="chknorma" onclick="chknormatutte()"></td>
		<td width="98%" colspan="3" class="tbltext"><b>TUTTE LE NORMATIVE</b> (e relative sottocategorie)</td>
	</tr>
</table>	
<!--- FINE TUTTE LE NORMATIVE ---->



<!--- NORMATIVE ---->
<table cellSpacing="1" cellPadding="1" width="500" align="center" border="0">
<%	Dim aDescrNorm

	aDescrNorm = decodTadesToArray("BDNOR",DATE,"valore='1'",1,"0")

	nInd = 1
	If Not IsNull(aDescrNorm(0,0,0)) Then 
		For yy=0 To Ubound(aDescrNorm)-1
%> 
			<tr>
				<td width="20">&nbsp;</td>
				<td width="20"><input type="checkbox" id="chknorma<%=nInd%>" name="chknorma<%=nInd%>" onclick="chknorma<%=nInd%>tutte()" value="<%=Lcase(aDescrNorm(yy,yy+1,yy))%>"></td>
				<td width="360" class="tbltext"><b><%=aDescrNorm(yy,yy+1,yy)%></b>
				<td width="100" class="tbltext"><a class="tbltext" href="javascript:viewlink('BDD_Sottocategorie.asp','norma<%=nInd%>','<%=aDescrNorm(yy,yy+1,yy)%>','BDNRM','<%=nInd%>')"><img alt src="/images/bulletagg.gif" border="0" WIDTH="70" HEIGHT="14"></a></td>

			</tr>
			<tr class="textred">
				<td colspan="2"></td>
				<td colspan="4" valign="top">
			  		<label class="textred" id="norma<%=nInd%>" name="norma<%=nInd%>"></label>
			  		<input type="hidden" id="norma<%=nInd%>a" name="norma<%=nInd%>a">
			  	</td>
			</tr>
<%		
		nInd=nInd+1
		Next
	End if
	Erase aDescrNorm
%>
</table>
<!--- FINE NORMATIVE ---->

<!--- BANDI E AVVISI ---->
<table cellSpacing="1" cellPadding="1" width="500" align="center" border="0">
	<tr>
		<td width="20"><input type="checkbox" id="chkbandi1" name="chkbandi1" onclick="chkbanditutte()" value="BANDI E AVVISI"></td>
		<td width="380" class="tbltext"><b>BANDI E AVVISI</b>
		<td width="100" class="tbltext"><a class="tbltext" href="javascript:viewlink('BDD_Sottocategorie.asp','bandi1','bandi e avvisi','BDBDA','1')"><img alt src="/images/bulletagg.gif" border="0" WIDTH="70" HEIGHT="14"></a></td>
	</tr>
	<tr class="textred">
		<td></td>
		<td colspan="2" valign="top">
	  		<label class="textred" id="bandi1" name="bandi1&gt;"></label>
	  		<input type="hidden" id="bandi1a" name="bandi1a">
	  	</td>
	</tr>
</table>
<!--- FINE BANDI E AVVISI ---->


<!--- STRUMENTI ---->
<table cellSpacing="1" cellPadding="1" width="500" align="center" border="0">
	<tr>
		<td width="20"><input type="checkbox" id="chkstrum1" name="chkstrum1" onclick="chkstrumtutte()" value="STRUMENTI"></td>
		<td width="380" class="tbltext"><b>STRUMENTI</b>
		<td width="100" class="tbltext"><a class="tbltext" href="javascript:viewlink('BDD_Sottocategorie.asp','strum1','strumenti','BDSTM','1')"><img alt src="/images/bulletagg.gif" border="0" WIDTH="70" HEIGHT="14"></a></td>
	</tr>
	<tr class="textred">
		<td></td>
		<td colspan="2" valign="top">
	  		<label class="textred" id="strum1" name="strum1"></label>
	  		<input type="hidden" id="strum1a" name="strum1a">
	  	</td>
	</tr>
</table>
<!--- FINE STRUMENTI ---->


<!--- BENCHMARKING ---->
<table cellSpacing="1" cellPadding="1" width="500" align="center" border="0">
	<tr>
		<td width="20"><input type="checkbox" id="chkbench1" name="chkbench1" onclick="chkbenchtutte()" value="BENCHMARKING"></td>
		<td width="380" class="tbltext"><b>BENCHMARKING</b>
		<td width="100" class="tbltext"><a class="tbltext" href="javascript:viewlink('BDD_Sottocategorie.asp','bench1','benchmarking','BDBCH','1')"><img alt src="/images/bulletagg.gif" border="0" WIDTH="70" HEIGHT="14"></a></td>
	</tr>
	<tr class="textred">
		<td></td>
		<td colspan="2" valign="top">
	  		<label class="textred" id="bench1" name="bench1"></label>
	  		<input type="hidden" id="bench1a" name="bench1a">
	  	</td>
	</tr>
</table>
<!--- FINE BENCHMARKING ---->


<!--- MATERIALI ITALIA LAVORO ---->
<table cellSpacing="1" cellPadding="1" width="500" align="center" border="0">
	<tr>
		<td width="20"><input type="checkbox" id="chkmater1" name="chkmater1" onclick="chkmatertutte()" value="MATERIALI ITALIA LAVORO"></td>
		<td width="380" class="tbltext"><b>MATERIALI ITALIA LAVORO</b>
		<td width="100" class="tbltext"><a class="tbltext" href="javascript:viewlink('BDD_Sottocategorie.asp','mater1','materiali italia lavoro','BDMTR','1')"><img alt src="/images/bulletagg.gif" border="0" WIDTH="70" HEIGHT="14"></a></td>

	</tr>
	<tr class="textred">
		<td></td>
		<td colspan="2" valign="top">
	  		<label class="textred" id="mater1" name="mater1"></label>
	  		<input type="hidden" id="mater1a" name="mater1a">
	  	</td>
	</tr>
</table>
<!--- FINE MATERIALI ITALIA LAVORO ---->


<!--- TUTTE LE DOCUMENTAZIONI ---->
<table cellSpacing="1" cellPadding="1" width="500" align="center" border="0">
	<tr>
		<td width="2%"><input class="tbltext" type="checkbox" id="chkdocum" value="chkdocum" name="chkdocum" onclick="chkdocumtutte()"></td>
		<td width="98%" colspan="3" class="tbltext"><b>TUTTE LE DOCUMENTAZIONI</b> (e relative sottocategorie)</td>
	</tr>
</table>	
<!--- FINE TUTTE LE DOCUMENTAZIONI ---->



<!--- DOCUMENTAZIONE ---->
<table cellSpacing="1" cellPadding="1" width="500" align="center" border="0">
<%	Dim aDescrDocum

	aDescrDocum = decodTadesToArray("BDDOC",DATE,"valore='1'",1,"0")
	nInd = 1
	If Not IsNull(aDescrDocum(0,0,0)) Then 
		For yy=0 To Ubound(aDescrDocum)-1
%> 
			<tr>
				<td width="20">&nbsp;</td>
				<td width="20"><input type="checkbox" id="chkdocum<%=nInd%>" name="chkdocum<%=nInd%>" onclick="chkdocum<%=nInd%>tutte()" value="<%=Lcase(aDescrDocum(yy,yy+1,yy))%>"></td>
				<td width="360" class="tbltext"><b><%=aDescrDocum(yy,yy+1,yy)%></b>
				<td width="100" class="tbltext"><a class="tbltext" href="javascript:viewlink('BDD_Sottocategorie.asp','docum<%=nInd%>','<%=Lcase(aDescrDocum(yy,yy+1,yy))%>','BDDCM','<%=nInd%>')"><img alt src="/images/bulletagg.gif" border="0" WIDTH="70" HEIGHT="14"></a></td>
			</tr>
			<tr class="textred">
				<td colspan="2"></td>
				<td colspan="2" valign="top">
			  		<label class="textred" id="docum<%=nInd%>" name="docum<%=nInd%>"></label>
			  		<input type="hidden" id="docum<%=nInd%>a" name="docum<%=nInd%>a">
			  	</td>
			</tr>
<%		
		nInd=nInd+1
		Next
	End if
	Erase aDescrDocum
%>
</table>
<!--- FINE DOCUMENTAZIONE ---->

<!--- SCHEDE E RECENSIONI ---->
<table cellSpacing="1" cellPadding="1" width="500" align="center" border="0">
	<tr>
		<td width="2%"><input class="tbltext" type="checkbox" id="chkschede1" value="chkschede1" name="chkschede1" onclick="chkschedetutte()"></td>
		<td width="98%" colspan="3" class="tbltext"><b>SCHEDE E RECENSIONI</b></td>
	</tr>
	<tr class="textred">
		<td></td>
		<td colspan="2" valign="top">
	  		<label class="textred" id="schede1" name="schede1"></label>
	  		<input type="hidden" id="schede1a" name="schede1a">
	  	</td>
	</tr>	
</table>	
<!--- FINE SCHEDE E RECENSIONI ---->

<!--- SETTORE ---->
<table cellSpacing="1" cellPadding="1" width="500" align="center" border="0">
	<tr>
		<td width="20"><input type="checkbox" id="chksettore1" name="chksettore1" onclick="chksetttutte()" value="SETTORE"></td>
		<td width="380" class="tbltext"><b>SETTORE</b>
		<td width="100" class="tbltext"><a class="tbltext" href="javascript:viewlink('BDD_Sottocategorie.asp','settore1','settore','BDSTT','1')"><img alt src="/images/bulletagg.gif" border="0" WIDTH="70" HEIGHT="14"></a></td>
	</tr>
	<tr class="textred">
		<td></td>
		<td colspan="2" valign="top">
	  		<label class="textred" id="settore1" name="settore1"></label>
	  		<input type="hidden" id="settore1a" name="settore1a">
	  	</td>
	</tr>
</table>
<!--- FINE SETTORE ---->

<!--- AZIONI ---->
<table cellSpacing="1" cellPadding="1" width="500" align="center" border="0">
	<tr>
		<td width="20"><input type="checkbox" id="chkazioni1" name="chkazioni1" onclick="chkazionitutte()" value="AZIONI"></td>
		<td width="380" class="tbltext"><b>AZIONI</b>
		<td width="100" class="tbltext"><a class="tbltext" href="javascript:viewlink('BDD_Sottocategorie.asp','azioni1','azioni','BDAZN','1')"><img alt src="/images/bulletagg.gif" border="0" WIDTH="70" HEIGHT="14"></a></td>

	</tr>
	<tr class="textred">
		<td></td>
		<td colspan="2" valign="top">
	  		<label class="textred" id="azioni1" name="azioni1"></label>
	  		<input type="hidden" id="azioni1a" name="azioni1a">
	  	</td>
	</tr>
</table>
<!--- FINE AZIONI ---->

<!--- TERRITORIO ---->

<table cellSpacing="1" cellPadding="1" width="500" align="center" border="0">
	<tr>
		<td width="20"><input type="checkbox" id="chkarea1" name="chkarea1" onclick="chkareatutte()" value="AREA"></td>
		<td width="380" class="tbltext"><b>TERRITORIO</b>
		<td width="100" class="tbltext"><a class="tbltext" href="javascript:viewlink('BDD_Territori.asp','area1','territorio','BDARE','1')"><img alt src="/images/bulletagg.gif" border="0" WIDTH="70" HEIGHT="14"></a></td>
	</tr>
	<tr class="textred">
		<td></td>
		<td colspan="2" valign="top">
	  		<label class="textred" id="area1" name="area1"></label>
	  		<input type="hidden" id="area1a" name="area1a">
	  	</td>
	</tr>
</table>
	
<!--- UNIONE EUROPEA ---->
<table cellSpacing="1" cellPadding="1" width="500" align="center" border="0">
	<tr>
		<td width="20">&nbsp;</td>
		<td width="20"><input type="checkbox" id="chkeuro1" name="chkeuro1" disabled onclick="chkeurotutte()" value="UNIONE EUROPEA"></td>
		<td width="360" class="tbltext"><b>UNIONE EUROPEA</b>
		<td width="100" class="tbltext"><a class="tbltext" href="javascript:viewlink('BDD_Sottocategorie.asp','euro1','unione europea','BDEUR','1')"><img alt src="/images/bulletagg.gif" border="0" id="imgeur" WIDTH="70" HEIGHT="14"></a></td>
	</tr>
	<tr class="textred">
		<td colspan="2"></td>
		<td colspan="2" valign="top">
	  		<label class="textred" id="euro1" name="euro1"></label>
	  		<input type="hidden" id="euro1a" name="euro1a">
	  	</td>
	</tr>
<!--- REGIONE ---->
	<tr>
		<td width="20">&nbsp;</td>
		<td width="20"><input type="checkbox" id="chkregio1" name="chkregio1" disabled onclick="chkregiotutte()" value="REGIONE"></td>
		<td width="360" class="tbltext"><b>REGIONI ITALIANE</b>
		<td width="100" class="tbltext"><a class="tbltext" href="javascript:viewlink('BDD_Regione.asp','regio1','regione','REGIO','')"><img alt src="/images/bulletagg.gif" border="0" id="imgreg" WIDTH="70" HEIGHT="14"></a></td>
	</tr>
	<tr class="textred">
		<td colspan="2"></td>
		<td colspan="2" valign="top">
	  		<label class="textred" id="regio1" name="regio1"></label>
	  		<input type="hidden" id="regio1a" name="regio1a">
	  	</td>
	</tr>
<!--- PROVINCIA ---->
	<tr>
		<td width="20">&nbsp;</td>
		<td width="20"><input type="checkbox" id="chkprov1" name="chkprov1" disabled onclick="chkprovtutte()" value="PROVINCIA"></td>
		<td width="360" class="tbltext"><b>PROVINCE ITALIANE</b>
		<td width="100" class="tbltext"><a class="tbltext" href="javascript:viewlink('BDD_Provincia.asp','regio1','regione','PROV','')"><img alt src="/images/bulletagg.gif" border="0" id="imgprov" WIDTH="70" HEIGHT="14"></a></td>
	</tr>
	<tr class="textred">
		<td colspan="2"></td>
		<td colspan="2" valign="top">
	  		<label class="textred" id="prov1" name="prov1"></label>
	  		<input type="hidden" id="prov1a" name="prov1a">
	  	</td>
	</tr>
</table> 
<br>
<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
	<tr>
		<td align="middle" colspan="3">
			<input type="image" src="/images/lente.gif" border="0" id="image1" name="image1">
		</td>
	</tr>
</table>

</form>

<script language="javascript">
	inizio()
</script>

<!-- tabella descrizione degli argomenti -->
<!-- include file = "BDD_IncDescCat.asp" -->
<!-- fine tabella descrizione degli argomenti -->

<br>
<table cellSpacing="4" cellPadding="4" width="500" align="center" border="0">
	<tr>
		<td align="middle"><a href="BDD_Ricerca_av.asp" class="textred"><b>Ricerca avanzata</b></a></td>
		<% if mid(Session("mask"),3,1) = "0"  then%>
		<td align="middle"><a href="BDD_InsDoc.asp" class="textred"><b>Inserimento nuovo documento</b></a></td>
		<%end if %>	
	</tr>  
</table>

<!--#include file = "BDD_Str2Menu.asp"-->

<!-- #include virtual="/include/CloseConn.asp" -->
<!--#include Virtual = "/strutt_coda1.asp"-->