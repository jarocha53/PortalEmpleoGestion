<script language="javascript" src="/include/help.inc"></script>
<script language="Javascript">
<!--
	function addItem(CAMPO,DESCR)
	{
		sel = ""
		nsel = 0
		numcheck = parseInt(formcheck.CampiCheck.value,10)
	
		for (i=0; i<numcheck; i++) {
					
			if (document.formcheck.elements[i].checked) 	
			{
				sel = sel + document.formcheck.elements[i].value + ", "
				nsel++;
			}
						
		}
			
		if (nsel == numcheck)
		{
			sel = "TUTTE"
			window.opener.document.all.item("chk" + CAMPO).checked = true
			window.opener.document.all.item(CAMPO + "a").value = DESCR
		}
		else
		{
			sel = sel.substring (0,(sel.length-2))
			window.opener.document.all.item("chk" + CAMPO).checked = false	
			window.opener.document.all.item(CAMPO + "a").value = DESCR + "," + sel
		}
			
		window.opener.document.all.item(CAMPO).innerHTML = sel
		window.close();
		
	}
		
//-->
function nuovo(){
	
	FRMINS.submit()
}
</script>

<html>
<head>
	<link REL="STYLESHEET" TYPE="text/css" HREF="<%=session("progetto")%>/fogliostile.css">
	<title>DOCUMENTA</title>
</head>
<body>
<center><table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
	<tr height="18">
		<td class="sfondomenu" width="67%" height="18"><span class="tbltext0"><b>&nbsp;SELEZIONE DELLE SOTTOCATEGORIE </b></span></td>
		<td width="3%" background="<%=Session("Progetto")%>/images/tondo_linguetta.gif"></td>
		<td valign="middle" align="right" class="tbltext1" width="30%" background="<%=Session("Progetto")%>/images/sfondo_linguetta.gif"></td>
	</tr>
</table>
<!-- Commento -->
<table border="0" CELLPADDING="0" CELLSPACING="0" width="500">
	<tr>
		<td align="left" class="sfondocomm">Utilizza questa form per selezionare delle sottocategorie da inserire nei parametri di ricerca del documento.  
			<a href="Javascript:Show_Help('/Pgm/Help/BancaDati/BDD_Bandi')"><img align="right" src="/images/help.gif" border="0"></a>
		</td>
	</tr>
	<tr height="2">
		<td class="sfondocomm" width="100%" colspan="3" background="<%=Session("Progetto")%>/images/separazione.gif">
		</td>
	</tr>
</table>
<%
Dim aDescr, s, tbl, val 
'	tbl = UCase(Request.QueryString("tab"))
	s  = UCase(Request("id")) 
	tbl = UCase(Request("tab"))
	val = UCase(Request("val"))
%>
<form method="post" NAME="FRMINS" action="BDD_InsEle.asp">
	<input type="hidden" id="id" name="id" value="<%=s%>">
	<input type="hidden" id="tab" name="tab" value="<%=tbl%>">
	<input type="hidden" id="val" name="val" value="<%=val%>">
</form>
<form method="post" name="formcheck">
<table WIDTH="400" ALIGN="center" BORDER="0" CELLSPACING="0" CELLPADDING="0">
	<tr>
		<td colspan="2"><p align="center" class="textreda"><b><%=Ucase(Request.QueryString("descr"))%></b></p></td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
<!--#include Virtual="/include/OpenConn.asp"-->
<!--#include Virtual="/include/DecCod.asp"-->
<%
	If s  = "TUTTE" then
		bAll  = "checked"
	Else
		bAll  = ""
	End If
	
	if val <> "" then
		aDescr  = decodTadesToArray(tbl,DATE,"valore=" &val,0,"0")
	else
		aDescr  = decodTadesToArray(tbl,DATE,"",0,"0")
	end if

	nInd = 0
	If Not IsNull(aDescr (0,0,0)) Then 
		For yy=0 To Ubound(aDescr )-1
%> 
			<tr>
				<td>
					<input type="checkbox" id="checkbox<%=nInd%>" <%=bAll %> name="checkbox<%=nInd%>" <% If instr(s,aDescr (yy,yy+1,yy)) Then Response.Write "checked" %> value="<%=Lcase(aDescr (yy,yy+1,yy))%>">
				</td>
				<td class="tbltext"><b><%=aDescr (yy,yy+1,yy)%></b></td>
			</tr>
<%		
		nInd=nInd+1
		Next
	End if
	Erase aDescr 
%>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<!--Inizio 29/03/04 -->
	<tr>
		<%'if mid(Session("mask"),3,1) = "0"  then 
		if mid(Session("mask"),2,1) = "0"  then 
		%>
			<td align="center" colspan="2"><a href="javascript:nuovo()" class="textred"><b>Inserimento nuovo elemento</b></a></td>
		<%end if %>	
	</tr>
	<tr><td>&nbsp;</tr></td>
	<!--Fine 29/03/04 -->
	<tr>
		<td width="400" colspan="2">
			<table width="400" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td align="right" valign="top" width="200"><a href="javascript:addItem('<%=Request.QueryString("campo")%>','<%=Request.QueryString("descr")%>');"><img src="/images/aggiungi.gif" border="0"></a></td>
					<td width="200" valign="top"><a href="javascript:window.close()"><img src="/images/chiudi.gif" border="0"></a></td>
				</tr>
			</table>
		</td>
	</tr>
	<input type="hidden" name="CampiCheck" value="<%=nInd%>"> 
</table>
</form>
</body>
</html>
