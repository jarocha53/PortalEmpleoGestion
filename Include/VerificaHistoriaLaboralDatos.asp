<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%
'====================================================================
'	**** Verificaciones Varias ****
'
'	Verificaciones sobre la Historia Laboral
'					 pertenencia a Jefes de Hogar
'					 Prerequisitos para Convenios de Adhesion
'====================================================================

public function VerificaEntrevistaACerrar(IdPersona, CUIL, OficinaID, UsuarioID, Status, Mensaje)
'=====================================================================
'	Verifica si la persona cumple para cerrar un entrevista
'
'	Si no cumple algun item se indica en los mensajes de salida
'	
'Necesita:
'	IdPersona	ID de la persona en la Tabla Persona
'	CUIL		o Nro de CUIL que esta Persona.Cod_Fis
'	
'
'Devuelve:
'	Status		Codigo de Retorno 1: OK
'	Mensaje		Mensaje de Error	
'	Funcion     1: Esta todo OK 
'=====================================================================

	Response.Write "<br> La funcion que esta usando es OBSOLETA: Includes/xVerificaHistoriaLaboralDatos.asp/VerificaEntrevistaACerrar <br>"
	Response.Write "<br> Debe usar VerificaEntrevistaACerrarVerifica <br>"
	Response.End
	
end function

public function VerificaEntrevistaACerrarVerifica (Oficina, NroEntrevista, IdPersona, CUIL, EntrevistaTarea, UsuarioID, Status, Mensaje)
'=====================================================================
'	Verifica si la persona cumple para cerrar un entrevista
'
'	Si no cumple algun item se indica en los mensajes de salida
'	
'Necesita:
'	Oficina			Oficina de trabajo
'	NroEntrevista	Nro de Entrevista que se esta realizando
'	IdPersona		ID de la persona en la Tabla Persona
'	CUIL			CUIL del entrevistado
'	EntrevistaTarea	Tarea de entrevista que se esta realizando para controlar
'	UsuarioID			Codigo de Usuario
'
'Devuelve:
'	Status		Codigo de Retorno 1: OK
'	Mensaje		Mensaje de Error	
'	Funcion     1: Esta todo OK 
'=====================================================================
	dim Resultado
	
	VerificaEntrevistaACerrarVerifica = 0
	
	Resultado = 0
	
	' Historia Laboral datos basicos (En Oracle)
	Resultado = VerificaHistoriaLaboralDatosBAsicos(IdPersona, CUIL, Oficina, Usuario, Status, Mensaje)
	
	' Verificaciones segun validaciones de Entrevistas
	if Resultado = 1 then
		TipoValidacion = 1	' Validaciones de Entrevistas
		Resultado = VerificaEntrevistaValidacionesAplicaStore(Oficina, TipoValidacion, NroEntrevista, CUIL, EntrevistaTarea, Status, Mensaje)
	end if
	
	'--------------------------------------------
	' Salida
	'--------------------------------------------
	if Resultado = 1 then
		VerificaEntrevistaACerrarVerifica = 1	
	end if
	STATUS = STATUS
	Mensaje = Mensaje

end function


public function VerificaHistoriaLaboralDatosBAsicos(IdPersona, CUIL, OficinaID, UsuarioID, Status, Mensaje)
'=====================================================================
'	Verifica si estan cargados los datos Basicos de una Historia Laboral
'=====================================================================
'	Si no cumple algun item se indica en los mensajes de salida
'
'Necesita:
'	IdPersona	ID de la persona en la Tabla Persona
'	CUIL		o Nro de CUIL que esta Persona.Cod_Fis
'	
'
'Devuelve:
'	Status		Codigo de Retorno 1: OK
'	Mensaje		Mensaje de Error	
'	Funcion     1: Esta todo OK 
'=====================================================================
	dim sSQL, sCUIL, sDireccion, sxIdPersona, sEstOcupacional, sNivelEstudio, sPostulaciones
		
	VerificaHistoriaLaboralDatosBAsicos = 0
	
	sCUIL = "": sDireccion = "": sxIdPersona = 0: sEstOcupacional = 0: sNivelEstudio = 0: sPostulaciones = 0
	Set rsBusca = Server.CreateObject("adodb.recordset")

	
	'OficinaID = ObtenerOficina(OficinaID)

	Status = 1	
	'-------------------------------------------
	'	Datos personales necesarios - ORACLE
	'-------------------------------------------
	if Status = 1 then
		' Cuil y Direccion base
		sSQL = "SELECT COD_FISC, Ind_Res FROM Persona WHERE ID_PErsona =" & IdPersona
		set rsBusca = cc.execute (sSQL)
		if not rsBusca.eof then
			sCUIL = rsBusca("COD_FISC")
			sDireccion = rsBusca("Ind_Res")
		end if
		rsBusca.close
		
		if sCUIL = "" then
			Status = 200
			Mensaje	= "Persona NO valida"
		elseif sDireccion = "" then
			Status = 210
			Mensaje	= "La direcci&oacute;n de la Persona NO est&aacute; definida"
		end if
	end if
	
	'-------------------------------------------
	'	Estado ocupacional
	'-------------------------------------------
	if Status = 1 then
		sxIdPersona = 0
		sSQL = "SELECT	Id_Persona	FROM Stato_Occupazionale WHERE ID_Persona = " & IdPersona & " and Ind_Status = 0"
		set rsBusca = cc.execute (sSQL)
		if not rsBusca.eof then
			sxIdPersona = rsBusca.Fields(0).Value
		end if
		rsBusca.close
		
		sxIdPersona = clng(sxIdPersona) + 0
		if clng(sxIdPersona) = 0 then
			Status = 220
			Mensaje	= "La persona NO tiene Situaci&oacute;n Laboral definida"
		end if
	end if
	
	'-------------------------------------------
	'	Nivel de Estudio
	'-------------------------------------------
	if Status = 1 then
		sEstOcupacional = 0
		sSQL = "SELECT	Count(*) FROM TISTUD WHERE ID_Persona = " & IdPersona 
		'PL-SQL * T-SQL  
		SSQL = TransformPLSQLToTSQL (SSQL) 
		set rsBusca = cc.execute (sSQL)
		if not rsBusca.eof then
			sEstOcupacional = rsBusca.Fields(0).Value
		end if
		rsBusca.close

		sEstOcupacional = cint(sEstOcupacional) + 0
		if cint(sEstOcupacional) = 0 then
			Status = 230
			Mensaje	= "La persona NO tiene Nivel de Estudios definidos"
		end if
	end if
	
	'-------------------------------------------
	'	Postulaciones Laborales
	'-------------------------------------------
	if Status = 1 then
		sPostulaciones = 0
		sSQL = "SELECT Count(*) FROM Pers_figProf WHERE ID_Persona = " & IdPersona & " and INCROCIO='S' and IND_STATUS= 0"
		'PL-SQL * T-SQL  
		SSQL = TransformPLSQLToTSQL (SSQL) 
		set rsBusca = cc.execute (sSQL)
		if not rsBusca.eof then
			sPostulaciones = rsBusca.Fields(0).Value
		end if
		rsBusca.close
		
		sPostulaciones = cint(sPostulaciones) + 0
		if cint(sPostulaciones) = 0 then
			Status = 240
			Mensaje	= "La persona NO tiene Intereses Ocupacionales ingresados"
		end if
	end if
	
	'-------------------------------------------
	'	Postulaciones Capacitacion
	'-------------------------------------------
	'if Status = 1 then
	''	sPostulaciones = 0
	''	sSQL = "SELECT Count(*) FROM pers_subtipologia WHERE ID_Persona = " & IdPersona & " and INCROCIO='S' and IND_STATUS= 0"
		'Response.Write ssql
		'PL-SQL * T-SQL  
	''	SSQL = TransformPLSQLToTSQL (SSQL) 
	''	set rsBusca = cc.execute (sSQL)
	''	if not rsBusca.eof then
	''		sPostulaciones = rsBusca.Fields(0).Value
	''	end if
	''	rsBusca.close
		
	''	sPostulaciones = cint(sPostulaciones) + 0
	''	if cint(sPostulaciones) = 0 then
	''		Status = 250
	''		Mensaje	= "La persona NO tiene &Aacute;reas de Inter&eacute;s ingresadas"
	''	end if
	'end if
	
	'-------------------------------------------
	'	Pregunta sobre Entecedentes Laborales 
	'-------------------------------------------
	if Status = 1 then
		sPostulaciones = 0
		sSQL = "SELECT Count(*) FROM datos_obligatorios WHERE antecedente_laboral IS NOT NULL AND ID_Persona = " & IdPersona
		set rsBusca = cc.execute (sSQL)
		if not rsBusca.eof then
			sPostulaciones = rsBusca.Fields(0).Value
		end if
		rsBusca.close
		
		sPostulaciones = cint(sPostulaciones) + 0
		if cint(sPostulaciones) = 0 then
			Status = 260
			Mensaje	= "La persona NO respondi&oacute; en el item Experiencia Laboral"
		end if
	end if

	'Response.Write spostulaciones
	set rsBusca = nothing 
	'-------------------------------------------
	'	Pregunta sobre Acciones de Busqueda
	'-------------------------------------------
	if Status = 1 then
		sPostulaciones = 0
		sSQL = "SELECT Count(*) FROM datos_obligatorios WHERE acciones IS NOT NULL AND ID_Persona = " & IdPersona
		set rsBusca = cc.execute (sSQL)
		if not rsBusca.eof then
			sPostulaciones = rsBusca.Fields(0).Value
		end if
		rsBusca.close
		
		sPostulaciones = cint(sPostulaciones) + 0
		if cint(sPostulaciones) = 0 then
			Status = 270
			Mensaje	= "La persona NO respondi&oacute; en el item Acciones de B&uacute;squeda de trabajo"
		end if
	end if

	'Response.Write spostulaciones
	set rsBusca = nothing 

	'--------------------------------------------
	' Salida
	'--------------------------------------------
	
	if STATUS = 1 then
		VerificaHistoriaLaboralDatosBAsicos = 1	
	end if
	STATUS = STATUS
	Mensaje = Mensaje

end function

		

public function VerificaEntrevistaValidacionesAplica (Oficina, TipoValidacion, NroEntrevista, IdPersona, CUIL, EntrevistaTarea, UsuarioID, Status, Mensaje)
'=====================================================================
'	Verifica si la persona cumple con las validaciones solicitadas
'		Aplica las validaciones las Oracle y las de SQL
'
'	Si no cumple algun item se indica en los mensajes de salida
'	
'Necesita:
'	Oficina			Oficina de trabajo
'	TipoValidacion	Tipo de Validacion a aplicar
'	NroEntrevista	Nro de Entrevista que se esta realizando
'	IdPersona		ID de la persona en la Tabla Persona
'	CUIL			CUIL del entrevistado
'	EntrevistaTarea	Tarea de entrevista que se esta realizando para controlar
'	UsuarioID			Codigo de Usuario
'
'Devuelve:
'	Status		Codigo de Retorno 1: OK
'	Mensaje		Mensaje de Error	
'	Funcion     1: Esta todo OK 
'=====================================================================
	dim Resultado
	
	VerificaEntrevistaValidacionesAplica = 0
	
	Resultado = 0
	
	' Historia Laboral datos basicos (En Oracle)
	Resultado = VerificaHistoriaLaboralDatosBAsicos(IdPersona, CUIL, Oficina, Usuario, Status, Mensaje)
	
	
	if Resultado = 1 and TipoValidacion = 999 then
		VerificaEntrevistaValidacionesAplica = 1
		exit function
	end if
	
	' Verificaciones segun validaciones de Entrevistas
	if Resultado = 1 then
		Resultado = VerificaEntrevistaValidacionesAplicaStore(Oficina, TipoValidacion, NroEntrevista, CUIL, EntrevistaTarea, Status, Mensaje)
	end if
	
	'--------------------------------------------
	' Salida
	'--------------------------------------------
	if Resultado = 1 then
		VerificaEntrevistaValidacionesAplica = 1	
	end if 
	STATUS = STATUS
	Mensaje = Mensaje

end function


public function VerificaEntrevistaValidacionesAplicaStore(Oficina, TipoValidacion, NroEntrevista, CUIL, EntrevistaTarea, Status, Mensaje)
'=====================================================================
'	Verifica las validaciones  a Aplicar
'	LLama directametne el Store de VErificacion de Validaciones
'
'Necesita:
'	Oficina			Oficina de trabajo
'	TipoValidacion	Tipo de Validacion a Aplicar (ver Store)
'	NroEntrevista	Nro de Entrevista que se esta realizando
'	CUIL			CUIL del entrevistado
'	EntrevistaTarea	Tarea de entrevista que se esta realizando para controlar
'	Usuario			Codigo de Usuario
'
'Devuelve:
'	Status		Codigo de Retorno 1: OK
'	Mensaje		Mensaje de Error	
'	Funcion     1: Esta todo OK 
'=====================================================================

'Oficina = ObtenerOficina(Oficina)
	'Response.Write "Oficina " & Oficina
	'Response.Write "<br>"
	'Response.Write "TipoValidacion " & TipoValidacion
	'Response.Write "<br>"
	'Response.Write "NroEntrevista " & NroEntrevista
	'Response.Write "<br>"
	'Response.Write "CUIL " & CUIL
	'Response.Write "<br>"
	'Response.Write "EntrevistaTarea " & EntrevistaTarea
	'Response.Write "<br>"
	'Response.Write "Usuario " & Session("IdUtente")
	'Response.Write "<br>"
	
	'Response.End 

'.......................PINO 06/08/2007, verifica asteriscata
'
'	
'	VerificaEntrevistaValidacionesAplicaStore = 0
	Status = 1
'
'	'-------------------------------------------
'	'	Es Beneficiario de Jefes de Hogar - SQL
'	'	Cita proxima conveniada	- SQL
'	'-------------------------------------------
'	Status = 0
'	Set cmdVerificar = Server.CreateObject("adodb.command")
'	with cmdVerificar
'		.activeconnection = connlavoro
'		.Commandtext = "AgEntrevistaValidacionesAplica"
'		.commandtype = adCmdStoredProc 
'		'--------------------------------------------
'		' CArga parametros
'		'--------------------------------------------
'		For Each Parameter In .Parameters
'			If Parameter.Direction And adParamInput Then
'				Select case ucase(Mid(Parameter.Name, 2))
'					CASE "OFICINA"
'						Parameter.value = OFICINA
'					CASE "TIPOVALIDACION"
'						Parameter.value = TipoValidacion
'					CASE "NROENTREVISTA"
'						Parameter.value = NroEntrevista
'					CASE "CUIL"
'						Parameter.value = CUIL
'					CASE "TAREA"
'						Parameter.value = EntrevistaTarea
'					CASE "USUARIO"
'						Parameter.value = USUARIO
'					CASE "STATUS"
'						if len(Status) = 0 then 
'							Status = 0
'						end if
'						Parameter.value = Status
'					CASE "MENSAJE"
'						Parameter.value = "" & Mensaje	
'					CASE ELSE
'						' ????
'				END SELECT
'			End If
'		Next
'		
'		
'		
'		
'		' Ejecuta
''PL-SQL * T-SQL  
'		.Execute 0, , adexecutenorecords
'		'--------------------------------------------
'		' Devuelve parametros
'		'--------------------------------------------
'		For Each Parameter In .Parameters
'			If Parameter.Direction And (adParamOutput Or adParamReturnValue) Then
'				Select case ucase(Mid(Parameter.Name, 2))
'					case "STATUS"
'						STATUS = trim(Parameter.value)
'					case "MENSAJE"
'						MENSAJE = trim(Parameter.value)
'					case else
'						' ???
'				end select
'			End If
'		Next
'	end with
'	' cierra objetos	
'	set cmdVerificar = nothing
'	
'	'--------------------------------------------
'	' Salida
'	'--------------------------------------------
'
'	
	if STATUS = 1 then
		VerificaEntrevistaValidacionesAplicaStore = 1	
	end if
	STATUS = STATUS
	Mensaje = Mensaje

end function

'public function ObtenerOficina(Oficina)
'	dim rs
'	dim rsdoc
'	dim sqldoc

'	set rsdoc = server.CreateObject("adodb.recordset")
'	set rs = Server.CreateObject("ADODB.Recordset")
			
'	dim rsofiausar
'	set rsofiausar = server.CreateObject("ADODB.Recordset")

'VariableSQL = "select * from OficinasRelacion where oficinahija =" & Oficina
'PL-SQL * T-SQL
'VariableSQL = TransformPLSQLToTSQL(VariableSQL)

'	set rsofiausar = cc.execute(VariableSQL)
'
'	if not rsofiausar.EOF then 
'		rsofiausar.MoveFirst 
'		ObtenerOficina = rsofiausar("oficinapadre")
''	else
'		ObtenerOficina = Oficina
'	end if 

'	set rsofiausar = nothing
'end function 


'public function VerificaPersonaEstado(CUIL, LIQUIPLAN, Status, Mensaje)
''=====================================================================
''	Verifica el estado de la persona En EmpleoPersonas y cita proxima 
''
''	Si no cumple algun item se indica en los mensajes de salida
''
''Necesita:
''	CUIL		Nro de CUIL que esta Persona.Cod_Fis
''	LIQUIPLAN	Nro de liquipan a observar 
''
''Devuelve:
''	Status		Codigo de Retorno 1: OK
''	Mensaje		Mensaje de Error	
''	Funcion     1: Esta todo OK 
''====================================================================='
'	VerificaPersonaEstado = 0'
'	Status = 0
'
'	'-------------------------------------------
'	'	Es Beneficiario de Jefes de Hogar - SQL
'	'	Cita proxima conveniada	- SQL
'	'-------------------------------------------
'	Status = 0
'	Set cmdVerificar = Server.CreateObject("adodb.command")
'	with cmdVerificar
'		.activeconnection = connlavoro
'		.Commandtext = "SeguroConvenioControlRequisitos"
'		.commandtype = adCmdStoredProc 
'		'--------------------------------------------
'		' CArga parametros
'		'--------------------------------------------
'		For Each Parameter In .Parameters
'			If Parameter.Direction And adParamInput Then
'				Select case ucase(Mid(Parameter.Name, 2))
'					CASE "CUIL"
'						Parameter.value = CUIL
'					CASE "LIQUIPLAN"
'						Parameter.value = LIQUIPLAN
'							
'					CASE "STATUS"
'						if len(Status) = 0 then 
'							Status = 0
'						end if
'						Parameter.value = Status
'					CASE "MENSAJE"
'						Parameter.value = "" & Mensaje	
'					CASE ELSE
'						' ????
'				END SELECT
'			End If
'		Next
'		' Ejecuta
'		.Execute 0, , adexecutenorecords
'		'--------------------------------------------
'		' Devuelve parametros
'		'--------------------------------------------
'		For Each Parameter In .Parameters
'			If Parameter.Direction And (adParamOutput Or adParamReturnValue) Then
'				Select case ucase(Mid(Parameter.Name, 2))
'					case "STATUS"
'						STATUS = trim(Parameter.value)
'					case "MENSAJE"
'						MENSAJE = trim(Parameter.value)
'					case else
'						' ???
'				end select
'			End If
'		Next
'	end with
'	' cierra objetos	
'	set cmdVerificar = nothing
'	
'	'--------------------------------------------
'	' Salida
'	'--------------------------------------------
'	if STATUS = 1 then
'		VerificaPersonaEstado = 1	
'	end if
'	STATUS = STATUS
'	Mensaje = Mensaje
'
'end function
		

		
%>
