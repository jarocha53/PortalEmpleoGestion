<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<script language="javascript">
<!--

function statusmsg(msg) { window.status = msg ; return true }

function FiltroNumerico(num)
{
	ret = true
	for (i = 0; i < num.length; i++)
		if (num[i] == window.event.srcElement.name)
			ret = (String("0123456789").indexOf(String.fromCharCode(window.event.keyCode)) != -1)
	return ret;
}

//	para cuando se corta el servicio
function openWindow(url, name) {
  popupWin = window.open(url, name, 'scrollbars,resizable,width=400,height=250')
}

-->
</script>
<script language="vbscript">
<!--
	public sub AbrirSugerenacia()
		features = "scrollbars,toolbar=no,location=no,width=600,height=400,top=20,left=20"
		url = "../ayuda/popUpSugerencia.asp"
		window.open url, null, features
	end sub
-->
</script>
<%
Private Function ArchivoNombre(Path, Prefijo, Extension)
Dim oFS
Dim Archivo

Set oFS = Server.CreateObject("Scripting.FileSystemObject")
	
If Mid(Path, Len(Path)) <> "\" Then Path = Path & "\"

'Archivo = Prefijo & Year(Now()) & Month(Now()) & Day(Now()) & Hour(Now()) & Minute(Now()) & Second(Now()) & "." + Extension
Archivo = Prefijo & Hour(Now()) & Minute(Now()) & Second(Now()) & "." + Extension
	
While oFS.FileExists(Path & Archivo)
'	Archivo = Prefijo & Year(Now()) & Month(Now()) & Day(Now()) & Hour(Now()) & Minute(Now()) & Second(Now()) & "." + Extension
	Archivo = Prefijo & Hour(Now()) & Minute(Now()) & Second(Now()) & "." + Extension
Wend

ArchivoNombre = Archivo
	
End Function

Private Function ArchivoNombreCorto(Archivo)
Dim oFO, oF

Set oFO = Server.CreateObject("Scripting.FileSystemObject")

	Set oF = oFO.GetFile(Archivo)
	
	ArchivoNombreCorto = oF.ShortName
	
End Function


	Private function edadMenor(FechaNac,EdadMe)
		dim anios
		if IsDate(FechaNac) then
				anios = cint(DateDiff("yyyy", FechaNac, Now()))
				if anios < EdadMe then ' or anios > 65 then
					edadMenor = 0		'Tiene menor a la fecha indicada
					exit function
				else
					edadMenor = 1		'Tiene la edad correcta mayor a la indicada
				end if
		else
			edadMenor=0
		end if
	end function

	Private Function edadMayor1(FechaNac,EdadMa)
	dim anios
		if IsDate(FechaNac) then		
				anios = cint(DateDiff("yyyy", FechaNac, Now()))
				if anios > EdadMa then ' or anios > 65 then
					edadMayor = 0		'Tiene a�omayor a la edad indicada
					exit function
				else
					edadMayor = 1		'Tiene la edad menor de lo indicado
					exit function
				end if
		else
			edadMayor=0
		end if
	End function
	
	Function edadMayor(FechaNac, EdadMa)
	Dim anios
	Dim mes1
	Dim mes2
	Dim dia1
	Dim dia2
	If IsDate(FechaNac) Then
		anios = CInt(DateDiff("yyyy", FechaNac, Now()))
		mes1 = CInt(Mid(FechaNac, 4, 2))
		mes2 = CInt(Mid(Date, 4, 2))
		dia1 = CInt(Left(FechaNac, 2))
		dia2 = CInt(Left(Date, 2))      
		If anios > EdadMa Then
			edadMayor = 0 'paso los a�os estimados
			Exit Function
		Else 'ya que la fecha esta dentro de los limites verificar el dia y mes
			If mes1 < mes2 Then 'Es menor en meses
				edadMayor = 1
				Exit Function
			Else                'El mes mayor
			If dia1 <= dia2 Then 'El dia es menor
                edadMayor = 1
                Exit Function
            Else
                edadMayor = 0
                Exit Function
            End If
        End If
    End If
Else
        edadMayor = 0 ' No es fecha
        Exit Function
End If   
End Function

Private Function edadTope(FechaNac, EdadMinima, EdadMaxima)
        Dim anios
        anios = CInt(DateDiff("yyyy", FechaNac, Now()))
        If IsDate(FechaNac) Then
                If anios <= EdadMinima Then
                    edadTope = 0 'Tiene menor a 18 a�os
                    Exit Function
                ElseIf anios => EdadMaxima Then
                    edadTope = 1  'Tiene mayor a 65 a�os
                    Exit Function
                End If
         Else
            edadTope = 2 'Error No es una fecha Valida
         End If
End Function

Private Function CheckCUIL(cuil)
	'Function que chequea el CUIL
    Dim auxi, acumulo, i
    Dim digitos, tab_cuil(12)    
    CheckCUIL = True
    If Len(cuil) < 11 Then
        CheckCUIL = False
        Exit Function
    End If
       
    digitos = Left(cuil, 2)    

    For i = 1 To 11    
        If InStr("0123456789", Mid(cuil, i, 1)) = 0 Then
            CheckCUIL = False            
            Exit Function
        End If
    Next

	'Cuil
    If digitos = 20 Or digitos = 23 Or digitos = 24 Or digitos = 27  then 'Or digitos = 30 Or digitos = 33 Or digitos = 34 Then
        acumulo = 0
        For i = 1 To 11            
            tab_cuil(i) = Mid(cuil, 12 - i, 1)            
        Next
        For i = 1 To 7
            acumulo = acumulo + (tab_cuil(i) * i)
        Next
        For i = 8 To 11
            acumulo = acumulo + (tab_cuil(i) * (i - 6))
        Next
        If acumulo Mod 11 <> 0 Then
            'CheckCUIT ="El CUIT Ingresado es Incorrecto" 'False
            CheckCUIL = False
            Exit Function
        Else
            'CheckCUIT ="BIEN" 'False
            CheckCUIL = True
            Exit Function
        End If
    Else
            'CheckCUIT ="El CUIT Ingresado es Incorrecto" 'False
            CheckCUIL = False
            Exit Function
    End If
End Function

Private Function CheckCUIT(cuit)
    Dim auxi, acumulo, i
    Dim digitos, tab_cuit(12)    
    CheckCUIT = True    
    If Len(cuit) < 11 Then
        CheckCUIT = False
        Exit Function
    End If
    'Esta parte sirve par cuit's con guiones
    'If Len(cuit) < 13 Or Not Mid(cuit, 3, 1) = "-" Or Not Mid(cuit, 12, 1) = "-" Then
        'CheckCUIT ="El CUIT Ingresado es Incorrecto" 'False
    '    CheckCUIT = False
    '    Exit Function
    'End If
    'auxi = Left(cuit, 2) + Mid(cuit, 4, 8) + Right(cuit, 1)
    'digitos = Left(auxi, 2)
    digitos = Left(cuit, 2)    

    For i = 1 To 11
        'If InStr("0123456789", Mid(auxi, i, 1)) = 0 Then
        If InStr("0123456789", Mid(cuit, i, 1)) = 0 Then
            CheckCUIT = False
            'CheckCUIT ="El CUIT Ingresado es Incorrecto" 'False
            Exit Function
        End If
    Next

    If digitos = 30 Or digitos = 33 Or digitos = 34 Then
        acumulo = 0
        For i = 1 To 11
            'tab_cuit(i) = Mid(auxi, 12 - i, 1)
            tab_cuit(i) = Mid(cuit, 12 - i, 1)            
        Next
        For i = 1 To 7
            acumulo = acumulo + (tab_cuit(i) * i)
        Next
        For i = 8 To 11
            acumulo = acumulo + (tab_cuit(i) * (i - 6))
        Next
        If acumulo Mod 11 <> 0 Then
            'CheckCUIT ="El CUIT Ingresado es Incorrecto" 'False
            CheckCUIT = False
            Exit Function
        Else
            'CheckCUIT ="BIEN" 'False
            CheckCUIT = True
            Exit Function
        End If
    Else
            'CheckCUIT ="El CUIT Ingresado es Incorrecto" 'False
            CheckCUIT = False
            Exit Function
    End If
End Function





Private Function MesNombre(M)
	Select Case M
	Case "1":  MesNombre = "enero"
	Case "2":  MesNombre = "febrero"
	Case "3":  MesNombre = "marzo"
	Case "4":  MesNombre = "abril"
	Case "5":  MesNombre = "mayo"
	Case "6":  MesNombre = "junio"
	Case "7":  MesNombre = "julio"
	Case "8":  MesNombre = "agosto"
	Case "9":  MesNombre = "septiembre"
	Case "10": MesNombre = "octubre"
	Case "11": MesNombre = "noviembre"
	Case "12": MesNombre = "diciembre"
	End Select
End Function

Private Function Fecha(F)

	If IsDate(F) then
		Fecha = Right("00" & Day(F), 2) & "/" _
			& Right("00" & Month(F), 2) & "/" _
			& Right("0000" & Year(F), 4)
	End If

End Function

Private Function FormatParameter(Parameter)

	Select Case Parameter.Type
	Case 135
		FormatParameter = Fecha(Parameter.Value)
'	Case 129
'		FormatParameter = RTrim(Parameter.Value)
	Case Else
		FormatParameter = RTrim(Parameter.Value)
	End Select

End Function

Private Function NullString2Null(S)

	If Len(S) Then
		NullString2Null = S
	Else
		NullString2Null = Null
	End If

End Function

Private Function Null2StringNull(S)

	If len(S)=0 Then
		Null2StringNull = "NULL"
	Else
		Null2StringNull = S
	End If

End Function


'Private Function Formatear(V, F)
'
'	Select Case F
'	Case "T"
'		Formatear = "'" & Replace(V, "'", "''") & "'"
'	Case "N"
'		Formatear = V
'	Case "D"	
'		If IsDate(V) Then
'			Formatear = "'" & Right("0000" & Year(V), 4) _
'				& Right("00" & Month(V), 2) _
'				& Right("00" & Day(V), 2) & "'"
'		End If
'	End Select
'
'End Function


Function Depurar(strtexto)
    Dim txtDatos 
    Dim textolimpio
    '/*Declarar afuera */
    Dim signo(36)
    Dim acento(15)
    Dim reemp(15)
    Dim inti 
    Dim intj 
    
    signo(1) = ","
    signo(2) = ";"
    signo(3) = "_"
    signo(4) = "*"
    signo(5) = "'"
    signo(6) = "/"
    signo(7) = "="
    signo(8) = "�"
    signo(9) = "�"
    signo(10) = "?"
    signo(11) = "`"
    signo(12) = ">"
    signo(13) = "<"
    signo(14) = "."
    signo(15) = "@"
    signo(16) = "&"
    signo(17) = "]"
    signo(18) = "["
    signo(19) = "+"
    signo(20) = "�"
    signo(21) = "^"
    signo(22) = "{"
    signo(23) = "}"
    signo(25) = "!"
    signo(26) = "("
    signo(27) = ")"
    signo(28) = "$"
    signo(29) = "%"
    signo(30) = "�"
    signo(31) = "�"
    signo(32) = "~"
    signo(33) = "�"
    signo(34) = "�"
    signo(35) = "�"
    signo(36) = "#"
    
    acento(1) = "�"
    reemp(1) = "a"
    acento(2) = "�"
    reemp(2) = "o"
    acento(3) = "�"
    reemp(3) = "i"
    acento(4) = "�"
    reemp(4) = "e"
    acento(5) = "�"
    reemp(5) = "u"
    
    acento(6) = "�"
    reemp(6) = "a"
    
    acento(7) = "�"
    reemp(7) = "e"
    
    acento(8) = "�"
    reemp(8) = "i"
    
    acento(9) = "�"
    reemp(9) = "o"
    
    acento(10) = "�"
    reemp(10) = "u"
        
    acento(11) ="�"
    reemp(11) = "a"
    
    acento(12) ="�" 
    reemp(12) = "e"
    
    acento(13) ="�" 
    reemp(13) = "i"
    
    acento(14) ="�" 
    reemp(14) = "o"
    
    acento(15) ="�"
    reemp(15) = "u"
          
    txtDatos = Trim(strtexto)
    
    For inti = 1 To UBound(acento)
        txtDatos = Trim(Replace(Trim(txtDatos), acento(inti), reemp(inti)))
    Next
    
    
    For inti = 1 To UBound(signo)
        txtDatos = Trim(Replace(Trim(txtDatos), signo(inti), " "))
    Next
    Depurar = UCase(txtDatos)
    
End Function

%>
