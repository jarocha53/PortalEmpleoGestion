<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#include VIRTUAL="/IncludeMTSS/fields.asp"-->
<%

'Constantes
PageSize = 20				' Tama�o de pagina de registros
MaxPageCountFino = 30			' Cantidad de paginas, detalle fino   
MaxPageCountGrueso= 30		' Cantidad de paginas, detalle grueso
NombreMes = Array("enero", "febrero", "marzo", "abril", "mayo", "junio", "julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre")


'---------------------------------------------------------
'	Ejecuta un comando a la Base de datos
'---------------------------------------------------------
' Necesita Nombre del Comando SP y la conexion a usar
sub EjecutaComandoSQL (NombreSP, xConn)
	'Ejecutar stored procedure
	Set CmdBuscaZZ1 = Server.CreateObject("ADODB.Command")
	With CmdBuscaZZ1
		.ActiveConnection = xConn
		.CursorLocation = 3
		.CommandTimeout = 240
		.CommandText = 	NombreSP
		.CommandType = adCmdStoredProc
		FieldsSaveParameters .Parameters
'PL-SQL * T-SQL  
RECORDCOUNT = TransformPLSQLToTSQL (RECORDCOUNT) 
		.Execute(RecordCount)
		FieldsLoadParameters .Parameters
		
'		For Each P In .Parameters
'			Response.Write P.Name & "=" & P.Value & "<br />"
'		Next
'		Response.Write  "nombre del SP " &  NombreSP
		
	End With
	set CmdBuscaZZ1 = nothing
end sub

'---------------------------------------------------------
'	Ejecuta un comando a la Base de datos
'---------------------------------------------------------
' Necesita Nombre del Comando SP y la conexion a usar
' Devuelve un Recordset 
sub EjecutaComandoSQLRS (NombreSP, RS , xConn)
	'Ejecutar stored procedure
	
	Set CmdBuscaZZ2 = Server.CreateObject("ADODB.Command")
	With CmdBuscaZZ2
		.ActiveConnection = xConn
		.CursorLocation = 3	
		.CommandTimeout = 240
		.CommandText = 	NombreSP
		.CommandType = adCmdStoredProc
		FieldsSaveParameters .Parameters
'PL-SQL * T-SQL  
		Set Rs = .Execute
		FieldsLoadParameters .Parameters
		
		
'		For Each P In .Parameters
'			Response.Write P.Name & "=" & P.Value & "<br />"
'		Next
'		Response.Write NombreSP
		
	End With
	set CmdBuscaZZ2 = nothing
end sub

'------------------------------------------------------------------------
' ???
'------------------------------------------------------------------------
Private Function CodDescrip(Cod, Descrip)

	temp = Label(Descrip)
	If Len(Fields(Cod)) Then
		temp = temp & " <SMALL>[" & Label(Cod) & "]</SMALL>"
	End If
	CodDescrip = temp

End Function

'------------------------------------------------------------------------
' ???
'------------------------------------------------------------------------
Private Function Label(varName)

	Label = Fields(varName) & Hidden(varName)

End Function

'---------------------------------------------
' Combo comun
'---------------------------------------------
Private Function Combo(varName, SQL, refresh, Ancho, xConn)
	Dim varTemp, Rec, i, text, value
	
	varTemp = varTemp & "<SELECT id='" & varName & "' name='" & varName & "'"
	if Fields("frmEsConsulta")= "1" then
		varTemp = varTemp & " disabled " 
	end if
	If refresh Then
		varTemp = varTemp & " onchange='Reload(this.form)' language=javascript"
	End If
	varTemp = varTemp & ">" & vbCrLf _
		& "			<OPTION value=''>" & Replace(String(Ancho * 3, " "), " ", "&nbsp;") & "</OPTION>" & vbCrLf
		
	If Len(SQL) Then
		Set Rec = Server.CreateObject("ADODB.Recordset")
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
		Rec.Open SQL, xConn, adOpenForwardOnly
		Do Until Rec.EOF
			value = trim(CStr(Rec(1)))
			varTemp = varTemp & "			<OPTION value='" & value & "'"
			If value = Fields(varName) Then
				varTemp = varTemp & " selected"
			End If
			varTemp = varTemp & ">" & RTrim(Rec(0)) & " [" & value & "]</OPTION>" & vbCrLf
			Rec.MoveNext
		Loop
		Rec.Close
		Set Rec = Nothing
	End If
	
	varTemp = varTemp & "		</SELECT>"
	Combo = varTemp
		
End Function

'---------------------------------------------------
' Combo comun Por Store Procedure o un Select comun
'---------------------------------------------------
Private Function ComboSP(varName, SQL, refresh, Ancho, xConn)
	Dim varTemp, Rec, i, text, value, Search
	varTemp = varTemp & "<SELECT id='" & varName & "' name='" & varName & "'"
	
	if Fields("frmEsConsulta")= "1" then
		varTemp = varTemp & " disabled " 
	end if
	If refresh Then
		varTemp = varTemp & " onchange='Reload(this.form)' language=javascript"
	End If
	varTemp = varTemp & ">" & vbCrLf _
		& "			<OPTION value=''>" & Replace(String(Ancho * 3, " "), " ", "&nbsp;") & "</OPTION>" & vbCrLf
	
	If Len(SQL) Then
    	Set cmdComboSP = Server.CreateObject("ADODB.Command")
		set RecComboSP = Server.CreateObject("ADODB.Recordset")
		'-------------------------------------------------------------------------------
		' Analizo la InstruccionSQL para saber si el String contiene una instrucci�n
		' Select o una llamada a un store Procedure
		'-------------------------------------------------------------------------------
		Search = "Select "
		if inStr(1,ucase(left(SQL, len(Search))), Ucase(Search)) = 0  or inStr(1, rtrim(SQL), " " ) = 0  then
			'------------------------------
			' es un store procedure
			'------------------------------	
			xConn.CursorLocation = 3
			With cmdComboSP
				.ActiveConnection = xConn
				.CommandText = SQL 
				.CommandType = adCmdStoredProc
				.CommandTimeout = 120
				'on error resume next
				
				FieldsSaveParameters .Parameters
				
				'For Each P In .Parameters
				'	Response.Write P.Name & "=" & fields(P.Name) & "<br />"
				'Next
				'Response.Write SQL
				'Response.End
				'on error goto 0
				
'PL-SQL * T-SQL  
RECORDCOUNT = TransformPLSQLToTSQL (RECORDCOUNT) 
				Set RecComboSP = .Execute(RecordCount)
				'..FieldsLoadParameters .Parameters
				'on error goto 0
				'For Each P In .Parameters
				 '  Response.Write P.Name & "=" & P.Value & "<br />"
				'Next
			End With
		else
			'------------------------------------
			' es una Instruccion Select
			'------------------------------------	
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
			RecComboSP.Open SQL, xConn, adOpenForwardOnly
		end if 
		
		'------------------------------------
		'Recorro los registro
		'------------------------------------ 
  	    Do Until RecComboSP.EOF
			value = trim(CStr(RecComboSP(1)))
			varTemp = varTemp & "			<OPTION value='" & value & "'"
			If value = Fields(varName) Then
				varTemp = varTemp & " selected"
			End If
			varTemp = varTemp & ">" & RTrim(RecComboSP(0)) & " [" & value & "]</OPTION>" & vbCrLf
			RecComboSP.MoveNext
		Loop
		RecComboSP.Close

		Set RecComboSP = Nothing
    	Set cmdComboSP = Nothing
	end if	
	' salida
	varTemp = varTemp & "		</SELECT>"
	ComboSP = varTemp
End Function

'=======================================================================================================
' Combo con connexion variable
'=======================================================================================================

private function ComboConn(varName, SQL, refresh, Ancho, xConn)
	dim varTemp, Rec, i, text, value
	varTemp = varTemp & "<SELECT id='" & varName & "' class=texblack width='" & Ancho & "px' name='" & varName & "'"
	
	if refresh then
		varTemp = varTemp & " onchange='Reload(this.form)' language=javascript"
	End If
	varTemp = varTemp & ">" & vbCrLf _
		& "			<OPTION value=''>" & Replace(String(Ancho * 3, " "), " ", "&nbsp;") & "</OPTION>" & vbCrLf
 	If Len(SQL) then
 	
			
		Set Rec = Server.CreateObject("ADODB.Recordset")
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
		Rec.Open SQL, xConn, adOpenForwardOnly
		Do Until Rec.EOF
			value = CStr(Rec(0))
			varTemp = varTemp & "			<OPTION value='" & value & "'"
			If value = varName then
				varTemp = varTemp & " selected"
			End If
			varTemp = varTemp & ">" & RTrim(Rec(1)) & "</OPTION>" & vbCrLf
			Rec.MoveNext
		Loop
		Rec.Close
		Set Rec = Nothing
	End If
	varTemp = varTemp & "		</SELECT>"
	ComboConn = varTemp
end function

'---------------------------------------------
' Combo especial 
' Si tiene un solo item -> Lo presenta seleccionado
' Si tiene mas de uno arma -> un combo comun 
'---------------------------------------------
Private Function ComboExt(varName, SQL, refresh, Ancho)
	Dim varTemp, i, text, value
	' cuantos items va a tener
	CantItems=0
	
	If Len(SQL) Then
		Set Rec = Server.CreateObject("ADODB.Recordset")
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
		Rec.Open SQL, ConnEmpleoPersonas, adOpenForwardOnly
		Do Until Rec.EOF 'and CantItems<=2
			CantItems=CantItems+1
			Rec.MoveNext
		Loop
		Rec.Close
	End If
	
	' Arma Combo
	if CantItems>1 then
		ComboExt= Combo(varName, SQL, refresh, Ancho)
	else 
		'Arma combo con un solo Item
		varTemp = varTemp & "<SELECT" & " id='" & varName & "' name='" & varName & "'"
		if Fields("frmEsConsulta")= "1" then
			varTemp = varTemp & " disabled " 
		end if
		If refresh = 1 Then
			varTemp = varTemp & " onchange='Reload()'"
		End If
		varTemp = varTemp & ">" & vbCrLf 
			
		If Len(SQL) Then
'PL-SQL * T-SQL  
SQL = TransformPLSQLToTSQL (SQL) 
			Rec.Open SQL, Con, adOpenForwardOnly
			if Rec.EOF=false then
				value = trim(CStr(Rec(1).value))
				varTemp = varTemp & "			<OPTION value='" & value & "'"
				' siempre seleccionado
				varTemp = varTemp & " selected"
				varTemp = varTemp & ">" & RTrim(Rec(0)) & "</OPTION>" & vbCrLf
			end if
			Rec.Close
		End If
  		varTemp = varTemp & "		</SELECT>"
		ComboExt = varTemp
  	end if
		
End Function

'---------------------------------------------------
' Combo de opciones
'
' Los datos estan en arreglo codificados de la sieguiente forma:
'	descripcion^codigo1_descripcion2^codigo2_descripcion3^codigo3' 
'---------------------------------------------------
Private Function ComboOpcion(varName, Arreglo, refresh, Ancho)
	Dim varTemp, Rec, i, text, value
	
	Renglones = Split(Arreglo, "_")
	RenglonesTot = Ubound(Renglones)

	varTemp = varTemp & "<SELECT id='" & varName & "' name='" & varName & "'"
	' solo de consultas
	if Fields("frmEsConsulta")= "1" then
		varTemp = varTemp & " disabled " 
	end if
	' redisplay
	If refresh = true or cstr(refresh) = 1 Then
		varTemp = varTemp & " onchange='Reload(this.form)' language=javascript"
	End If
	' linea an blanco
	varTemp = varTemp & ">" & vbCrLf _
		& "			<OPTION value=''>" & Replace(String(Ancho * 3, " "), " ", "&nbsp;") & "</OPTION>" & vbCrLf
		
	' Arma opciones
	for i = 0 to cint(RenglonesTot)	-2 
		xDatos = Split(Renglones(i), "^")
		' codigo
		value = xDatos(1)
		varTemp = varTemp & "			<OPTION value='" & value & "'"
		If value = Fields(varName) Then
			varTemp = varTemp & " selected"
		End If
		varTemp = varTemp & ">" & RTrim(xDatos(2)) & " [" & value & "]</OPTION>" & vbCrLf
	next
	varTemp = varTemp & "		</SELECT>"
	ComboOpcion = varTemp
		
End Function

'---------------------------------------------------
' Check button
'---------------------------------------------------
Private Function Check(varName, value)
	dim varTemp	
	If Fields(varName) = CStr(value) Then
		varTemp = "<INPUT type=checkbox CHECKED value='" & value & "' id=" & varName & " name=" & varName 
	Else
		varTemp = "<INPUT type=checkbox value='" & value & "' id=" & varName & " name=" & varName 
	End If

	if Fields("frmEsConsulta")= "1" then
		varTemp = varTemp & " disabled " 
	end if
	varTemp = varTemp & ">"
	
	Check =	varTemp
End Function


'------------------------------------------------------------------------
' check con control de Habilitado o no
'------------------------------------------------------------------------
Private Function CheckEnabled(varName, value, Enabled)
	' check habilitado
	if Enabled= true then
		CheckEnabled =Check(varName, value)
	else
		' check deshabilitado
		If Fields(varName) = CStr(value) Then
			CheckEnabled = "<INPUT type=checkbox CHECKED disabled value='" & value & "' id=" & varName & " name=" & varName & ">"
		Else
			CheckEnabled = "<INPUT type=checkbox disabled value='" & value & "' id=" & varName & " name=" & varName & ">"
		End If
	end if

End Function

'------------------------------------------------------------------------
' ???
'------------------------------------------------------------------------
Private Function CheckExt(varName, value, estado)
	dim varTemp
	If estado = "1" Then
		varTemp = "<INPUT type=checkbox CHECKED value='" & value & "' id=" & varName & " name=" & varName
	Else
		varTemp = "<INPUT type=checkbox value='" & value & "' id=" & varName & " name=" & varName 
	End If
	if Fields("frmEsConsulta")= "1" then
		varTemp = varTemp & " disabled " 
	end if
	varTemp = varTemp & ">"
	
	CheckExt = varTemp
End Function

'------------------------------------------------------------------------
' Campo de Ingreso de Texto Comun
'------------------------------------------------------------------------
Private Function Text(varName, maxLength)
	Text = TextRefresh(varName, maxLength, 0)
End Function

'------------------------------------------------------------------------
' Campo de Ingreso de Texto con recarga cuando hay algun cambio
'------------------------------------------------------------------------
Private Function TextRefresh(varName, maxLength, refresh)
	dim varTemp, Zize
	Size = maxLength + 3
	varTemp = "<input type=""text"" class=""textblack"" id=""" & varName & """ name=""" & varName & """ maxLength=""" & maxLength & """ size=""" & Size & """ value=""" & Fields(varName) & """"
	
	if Fields("frmEsConsulta")= "1" then
		varTemp = varTemp & " readonly " 
	end if
	If refresh Then
		varTemp = varTemp & " onchange='Reload(this.form)' language=javascript"
	End If
	varTemp = varTemp & ">"
	TextRefresh = varTemp
End Function

'------------------------------------------------------------------------
' Campo de Ingreso tipo Area
'------------------------------------------------------------------------
Private Function TextArea(varName, Cols, Rows, Size)
	dim varTemp
	varTemp = "<TextArea id=""" & varName & """ name=""" & varName & """ rows=""" & rows & """ cols=""" & cols & """ maxLength=""" & Size & """ size=""" & Size & """ value=""" & Fields(varName) & """"
	if Fields("frmEsConsulta")= "1" then
		'varTemp = varTemp & " disabled " Esto estaba hasta el 21/09/2004
		varTemp = varTemp & " readonly " 
	end if
	varTemp = varTemp & ">" & Fields(varName) & "</TextArea>"
	TextArea = varTemp
	'..<TEXTAREA id=TEXTAREA1 name=TEXTAREA1 rows=4 cols=50></TEXTAREA>

End Function

'------------------------------------------------------------------------
' ???
'------------------------------------------------------------------------
Private Function Hidden(varName)

	Hidden = "<INPUT type=hidden name='" & varName & "' id='" & varName & "' value='" & Fields(varName) & "' size=0>"

End Function

'------------------------------------------------------------------------
' ???
'------------------------------------------------------------------------
Private Function PageURL(n)
	Fields("p") = n
	PageURL = Request.ServerVariables("SCRIPT_NAME") _
		& "?" & ToQueryString()

End Function

'------------------------------------------------------------------------
' En desuso
'------------------------------------------------------------------------
Private Function oldPeriodo(varName)
	temp = temp _
		& "<SELECT id='" & varName & "Mes' name='" & varName & "Mes'>" & vbCrLf _
		& "			<OPTION></OPTION>" & vbCrLf
	For i = 1 To 12
		If CStr(i) = Fields(varName & "Mes") Then
			temp = temp _
				& "			<OPTION value=" & i & " selected>" & NombreMes(i - 1) & "</OPTION>" & vbCrLf
		Else
			temp = temp _
				& "			<OPTION value=" & i & ">" & NombreMes(i - 1) & "</OPTION>" & vbCrLf
		End If
	Next
	temp = temp _
		& "		</SELECT><SELECT id='" & varName & "Ano' name='" & varName & "Ano'>" & vbCrLf _
		& "			<OPTION></OPTION>" & vbCrLf
	For i = 1997 To 2005
		If CStr(i) = Fields(varName & "Ano") Then
			temp = temp _
				& "			<OPTION value=" & i & " selected>" & i & "</OPTION>" & vbCrLf
		Else
			temp = temp _
				& "			<OPTION value=" & i & ">" & i & "</OPTION>" & vbCrLf
		End If
	Next
	temp = temp _
		& "		</SELECT>"
	oldPeriodo = temp

End Function

'------------------------------------------------------------------------
' En desuso
'------------------------------------------------------------------------
Private Function oldPeriodo1(varName, Alta)
	If IsDate(Alta) Then
		d0 = CDate(Alta)
	Else
		d0 = Now
	End If
	
	temp = temp _
		& "<SELECT id=" & varName & " name=" & varName & ">" & vbCrLf _
		& "			<OPTION></OPTION>" & vbCrLf
	For i = 0 to 12
		d = DateAdd("m", i, d0)
		v = Right("00" & Month(d), 2) & Year(d)
		t = NombreMes(Month(d) - 1) & "&nbsp;" & Year(d)
		If v = Fields(varName) Then
			temp = temp _
				& "			<OPTION value=" & v & " selected>" & t & "</OPTION>" & vbCrLf
		Else
			temp = temp _
				& "			<OPTION value=" & v & ">" & t & "</OPTION>" & vbCrLf
		End If
	Next
	temp = temp _
		& "		</SELECT>"
		
	oldPeriodo1 = temp

End Function

'-----------------------------------------------------------------------------
' Presenta la lista de periodos mensuales a partir del PeriodoAlta indicado
' Presenta doce periodos
'-----------------------------------------------------------------------------
Private Function Periodo(varName, PeriodoAlta)
	dim mes, ano
	
	if len(PeriodoAlta)=0 then
		mes=Month(now)
		ano=Year(now)
	else
		mes=int(left(PeriodoAlta,2))
		ano=int(right(PeriodoAlta,4))
	end if
	
	' arma el combo
	temp = temp _
		& "<SELECT id=" & varName & " name=" & varName & ">" & vbCrLf _
		& "			<OPTION></OPTION>" & vbCrLf

	For i = 0 to 12
		' mes actual
		periodoValor=Right("00" & cstr(mes), 2) & Right("0000" & cstr(ano), 4)
		periodoNombre=NombreMes(mes - 1) & "&nbsp;" & ano
		
		' arma item del combo
		If periodoValor = Fields(varName) Then
			temp = temp _
				& "			<OPTION value=" & periodoValor & " selected>" & periodoNombre & "</OPTION>" & vbCrLf
		Else
			temp = temp _
				& "			<OPTION value=" & periodoValor & ">" & periodoNombre & "</OPTION>" & vbCrLf
		End If
		
		' siguiente mes
		mes=mes+1
		if mes>12 then
			ano=ano+1
			mes=1
		end if

	Next
	temp = temp _
		& "		</SELECT>"
		
	Periodo = temp

End Function

'-----------------------------------------------------------------------------
' Presenta la lista de periodos mensuales desde Abril 2001
' hasta 12 meses posteriores a la fecha actual
'-----------------------------------------------------------------------------
Private Function PeriodoLibre(varName)
	dim i,j, mes, ano, InicioMes, InicioAno, ActualMes, ActualAno, temp
	
	' Periodo inicial a presentar
	InicioMes = 4
	InicioAno = 2001
	
	' Periodo actual
	ActualMes = Month(now)
	ActualAno = Year(now)
	
	' Valor Default
	If len(Fields(varName))=0 Then
		Fields(varName)=Right("00" & cstr(ActualMes), 2) & Right("0000" & cstr(ActualAno), 4)
	end if

	' Arma el combo
	temp = temp _
		& "<SELECT id=" & varName & " name=" & varName & ">" & vbCrLf _
		& "			<OPTION></OPTION>" & vbCrLf

	' arma items del combo	
	J=0: i=0
	mes = InicioMes
	ano = InicioAno
	do
		' mes actual
		periodoValor=Right("00" & cstr(mes), 2) & Right("0000" & cstr(ano), 4)
		periodoNombre=NombreMes(mes - 1) & "&nbsp;" & ano
		
		' arma item del combo
		If periodoValor = Fields(varName) Then
			temp = temp _
				& "			<OPTION value=" & periodoValor & " selected>" & periodoNombre & "</OPTION>" & vbCrLf
			J = 1				
		Else
			temp = temp _
				& "			<OPTION value=" & periodoValor & ">" & periodoNombre & "</OPTION>" & vbCrLf
		End If

		' siguiente mes
		mes = mes + 1
		if mes > 12 then
			ano = ano + 1
			mes = 1
		end if
		
		' Limites
		i = i + 1
		if j > 0 then
			J = J + 1 
		end if
	loop until J => 12 or I >=60
	
	' cierra el combo
	temp = temp _
		& "		</SELECT>"
		
	PeriodoLibre = temp

End Function

'-------------------------------------------------------
' Barra de paginacion de registros para consultas  
'-------------------------------------------------------
Private Function NavBar(Rec, Page)
	If Rec.RecordCount Then
		' registros que se pueden mostrar

		MaxRecordCount = MaxPageCountGrueso * MaxPageCountFino * PageSize

		temp = temp _
			& "<TABLE class=""empresas"">" & vbCrLf _
			& "	<TR>" & vbCrLf _
			& "		<TD width=""30%"" align=left>"

		' Cantidad de registros encontrados
		If Rec.RecordCount <= MaxRecordCount Then
			temp = temp _
				& "<B>" & Rec.RecordCount & "</B> registros encontrados"
		Else
			temp = temp _
				& "<B>+" & MaxRecordCount & "</B> registros encontrados"
		End If
			
		'-----------------------------------
		' Barras de paginacion
		'-----------------------------------
		temp = temp _
			& "</TD>" & vbCrLf _
			& "		<TD width=""70%"" align=right>"

		' Cantidad total de paginas 
		n = Rec.PageCount
		If n > MaxPageCountGrueso * MaxPageCountFino Then 
			n = MaxPageCountGrueso * MaxPageCountFino	'Limitar cant. de p�ginas
		end if
					
		If n > 1 Then			'S�lo si hay varias p�ginas
			'-----------------------------------
			' Barra de paginacion gruesa
			'-----------------------------------
			PageGruesaActual=1
			if n  > MaxPageCountFino  then
				' Pagina gruesa actual
				PageGruesaActual = int((page-1)/MaxPageCountFino) * MaxPageCountFino + 1
				
				' P�gina anterior
				If Page > MaxPageCountFino Then	
					temp = temp _
						& "<A href=" & PageURL(PageGruesaActual - MaxPageCountFino) & ">&lt;&lt;</a>"
				Else
					temp = temp _
						& "&lt;&lt;"
				End If
				
				' Paginas 
				For i = 1 To n step MaxPageCountFino
					temp = temp & " "  ' "&nbsp;"
					If i =< Page and Page < (i + MaxPageCountFino) Then
						temp = temp & "<b>" & i & "</b>"
					Else
						temp = temp & "<a href=" & PageURL(i) & ">" & i & "</a>"
					End If
					MaxPageGruesa=i
				Next
				temp = temp & " "  ' "&nbsp;"
				
				' P�gina siguiente	
				If Page < MaxPageGruesa Then	
					temp = temp _
						& "<a href=" & PageURL(PageGruesaActual + MaxPageCountFino) & ">&gt;&gt;</a>"
				Else
					temp = temp _
						& "&gt;&gt;"
				End If

				' prepara otra celda					
				temp = temp _
					& "<BR>" & vbCrLf 
			end if

			'-----------------------------------
			' Barra de paginacion fina
			'-----------------------------------
			'P�gina anterior
			If Page > 1 Then	
				temp = temp _
					& "<A href=" & PageURL(Page - 1) & ">&lt;&lt;</a>"
			Else
				temp = temp _
					& "&lt;&lt;"
			End If
			
			' Paginas 
			m = PageGruesaActual + MaxPageCountFino - 1
			if m > n then
				m = n
			end if
			For i = PageGruesaActual To m		'P�gina N
				temp = temp _
					& " "  ' "&nbsp;"
				If i <> Page Then
					temp = temp _
						& "<a href=" & PageURL(i) & ">" & i & "</a>"
				Else
					temp = temp _
						& "<b>" & i & "</b>"
				End If
			Next
			temp = temp _
				& " "  ' "&nbsp;"
			
			' P�gina siguiente	
			If Page < n Then	
				temp = temp _
					& "<a href=" & PageURL(Page + 1) & ">&gt;&gt;</a>"
			Else
				temp = temp _
					& "&gt;&gt;"
			End If
			
		End If
		
		temp = temp _
			& "</TD>" & vbCrLf _
			& "	</TR>" & vbCrLf _
			& "</TABLE>" & vbCrLf
	
	End If
		
	NavBar = temp

End Function

Private Function OptionButton(varName, Item, value)
'------------------------------------------------------------------------
'	Arma un control del tipo Option button
'Necesita: 
'	VarName	Nombre del grupo de opciones
'	Item	Numero de item de la opcion dentro del grupo
'	Value	Valor de la opcion
'------------------------------------------------------------------------
	dim varTemp
	
	If Fields(varName) = CStr(value) Then
		varTemp = "<INPUT type=radio CHECKED value='" & value & "' id=" & varName & item & " name=" & varName
	Else
		varTemp = "<INPUT type=radio value='" & value & "' id=" & varName & item & " name=" & varName 
	End If
	if Fields("frmEsConsulta")= "1" then
		varTemp = varTemp & " disabled " 
	end if
	varTemp = varTemp & ">"	
	OptionButton = varTemp
End Function

%>
