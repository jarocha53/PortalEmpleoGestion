<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->

<!--#include virtual="/IncludeMTSS/stringfunctions.asp"-->

<%
'=================================================================
'	Funciones Comunes de los ABMs
'=================================================================

'-----------------------------------------------------------------
'	Prepara campos Field especiales
'-----------------------------------------------------------------
	' Arma fields a usar de campos de tabla
	cn = """dummy0"",""dummy1""" 
	cf = cn: co = Cn : cp = cn : cc= cn: ct = cn 
	for i = 1 to DbCampos(0,0)
		if Len(DbCampos(i,0)) > 0 then
			' Campos Numericos
			if left(ucase(DbCampos(i,2)),1) = "N" then
				if cn <> "" then cn = cn & ","
				cn =cn & """" & DbCampos(i,0) & """"
			end if 
			' Campos Fecha
			if ucase(DbCampos(i,2)) = "F" then
				if cf <> "" then cf = cf & ","
				cf =cf & """" & DbCampos(i,0) & """"
			end if 
			' Campos Obligatorio
			if ucase(DbCampos(i,6)) = "R" then
				if co <> "" then co = co & ","
				co = co & """" & DbCampos(i,0) & """"
			end if 
			' Campos de la pantalla
			if cp <> "" then cp = cp & ","
			cp = cp & """" & DbCampos(i,0) & """"
			' Tipos de datos y requerido
			if ct <> "" then ct = ct & ","
			ct = ct & """" & ucase(DbCampos(i,2)) & """"
		end if
	next	
	' Campos clave
	'for i =1 to DbCampoClave(0)
	'	if cc <> "" then cc = cc & ","
	'	cc = cc & """" & DbCampoClave(i) & """"
	'next
	Fields("frmnumericos")= cn		' campos numericos
	Fields("frmrequeridos")= co		' campos obligatorios
	Fields("frmClaves")= cc			' campos claves
	Fields("frmCampos")= cp			' campos en pantalla
	Fields("frmCamposTipos")= ct	' Tipos de datos
	Fields("frmlimpiar")= cp		' campos a limpiar

'=====================================================
'	Funciones generales del Servidor
'=====================================================
'-----------------------------------------------------
' Formatea un campo de archivo
'-----------------------------------------------------
Private Function Formatear(V, F)
	Select Case F
	Case "T"
		Formatear = "'" & Replace(V, "'", "''") & "'"
	Case "N"
		Formatear = V
	Case "D"	
		If IsDate(V) Then
			Formatear = "'" & Right("0000" & Year(V), 4) _
				& Right("00" & Month(V), 2) _
				& Right("00" & Day(V), 2) & "'"
		End If
	End Select
End Function

'-----------------------------------------------------
' Determina el tipo de formato de un campo de archivo
'-----------------------------------------------------
Private Function FieldType(FieldName)
	dim F
	
	For Each F In Rec.Fields
		If UCase(F.Name) = UCase(FieldName) Then
			Select Case F.Type
			Case 135:		FieldType = "D" 'Fecha
			Case 129,200:	FieldType = "T" 'Texto
			Case Else:		FieldType = "N" '(Numerico)
			End Select
			Exit For
		End If
	Next

End Function

'----------------------------------------------
' Limpia campos de ingreso de datos
'----------------------------------------------
Private Function ABMClearCampos
	dim Arr, I, AA
	
	Arr = split(fields("frmlimpiar"),",")
	for i = 0 to ubound(Arr)
		AA = Arr(i)
		AA = mid(left(AA,len(AA)-1),2)
		fields(AA)= ""
	next
End Function

'==============================================
'	Funcionamiento Basico del Formulario
'==============================================
'----------------------------------------------
'	Funcionamiento Basico del ABM
'----------------------------------------------
Private Function ABMbasicoFuncionamiento
	'----------------------------------------------
	'Funcionamiento del form segun su estado
	'----------------------------------------------
	' Valores por Default
	Fields("frmbotones")=0				' Cantidad de Botones
	Fields("frmestadotit")=""			' titulo del estado
	frmClearCampos = False
	Fields("frmEsConsulta") = "0"		' No es Consulta
	' Estado por Default --> Filtro
	If Len(Fields("frmestado"))= 0 Then
		Fields("frmestado")= "filtro"
	end if

	' Definiciones del form segun su estado 
	select case ucase(Fields("frmestado"))	' Estado actual del formulario
		'----------------------------------------------------
		' *** Actua como filtro de datos para busquedas ***
		'----------------------------------------------------
		case "FILTRO"	
			Fields("frmestadotit") = "Filtros de B�squeda"
			FormAction = "buscar.asp"	' Pagina que llama
			FormMethod = "get"			
			SubmitValue = "Buscar"		' Boton principal	
			FormOnSubmit = ""
			Fields("frmload") = 0			' No Carga datos iniciales 
			Fields("frmLocked") = """dummy0"",""dummy1"""  
			' *** Botones adicionales ***
			Fields("frmbotones") = 2
			' Nuevo registro
			Fields("frmboton1") = "<INPUT type=""button"" class=""button"" value=""Nuevo"" id=button1 name=button1 onclick='ReloadForm(this.form,""nuevo"")' language=javascript >&nbsp;" 
			' Limpiar Campos
			Fields("frmboton2") = "<INPUT type=""button"" class=""button"" value=""Limpiar Campos"" id=btnClear name=btnClear onclick='FormClear(Array(" & Fields("frmlimpiar") & "))' language=javascript >&nbsp;"
		'----------------------------------------------------
		' *** Ingreso de nuevo registro ***
		'----------------------------------------------------
		case "NUEVO"	
			Fields("frmestadotit") = "Nuevo Registro"
			FormAction = "grabar.asp"
			FormMethod = "post"
			SubmitValue = "Grabar"
			'..FormOnSubmit = "frmtarea.value=1;return FormComplete(this, Array(" & Fields("frmrequeridos") & "))"
			FormOnSubmit = "frmtarea.value=1;return FormCompleto(Array(" & Fields("frmcampos") & "), Array(" & Fields("frmcampostipos") & "), Array(" & Fields("frmrequeridos") & "))"	
			Fields("frmload") = 0
			if Fields("frmestado")<> Fields("frmestadoant") then
				frmClearCampos = true
			end if
			Fields("frmLocked") = """dummy0"",""dummy1"""  
			' *** Botones adicionales ***
			Fields("frmbotones") = 2
			' Cancelar Operacion
			Fields("frmboton1") = "<INPUT type=""button"" class=""button"" value=""Cancelar"" id=button1 name=button1 onclick='ReloadForm(this.form,""filtro"")' language=javascript >&nbsp;" 
			' Limpiar Campos
			Fields("frmboton2") = "<INPUT type=""button"" class=""button"" value=""Limpiar Campos"" id=btnClear name=btnClear onclick='FormClear(Array(" & Fields("frmlimpiar") & "))' language=javascript >&nbsp;"
		'----------------------------------------------------
		' *** Modificacion de registro ***
		'----------------------------------------------------
		case "MODIF"	
			Fields("frmestadotit") = "Modificar Registro Actual"
			FormAction = "Grabar.asp"
			FormMethod = "post"
			SubmitValue = "Grabar"
			'..FormOnSubmit = "return FormComplete(this, Array(" & Fields("frmrequeridos") & "))"
			'..FormOnSubmit = "frmtarea.value=2;return FormComplete(this, Array(" & Fields("frmrequeridos") & "))"
			FormOnSubmit = "frmtarea.value=2;return FormCompleto(Array(" & Fields("frmcampos") & "), Array(" & Fields("frmcampostipos") & "), Array(" & Fields("frmrequeridos") & "))"	
			Fields("frmload") = 1			' Carga datos iniciales 
			Fields("frmLocked") = Fields("frmClaves")
			' *** Botones adicionales ***
			Fields("frmbotones") = 2
			' Eliminar Registro 
			Fields("frmboton1") = "<INPUT type=""button"" class=""button"" value=""Eliminar"" id=button1 name=button1 onclick='frmtarea.value=3;action=""" & FormAction & """;method=""POST"";submit()' language=javascript >&nbsp;" 
			' Cancelar Operacion
			Fields("frmboton2") = "<INPUT type=""button"" class=""button"" value=""Cancelar"" id=button2 name=button2 onclick='ReloadForm(this.form,""filtro"")' language=javascript >&nbsp;" 
		'----------------------------------------------------
		' *** Consulta de Registro ***
		'----------------------------------------------------
		case "CONSULTA"
			Fields("frmestadotit") = "Consulta"
			FormAction = ""
			FormMethod = "post"
			SubmitValue = ""
			'..FormOnSubmit = "return FormComplete(this, Array(" & Fields("frmrequeridos") & "))"
			'..FormOnSubmit = "frmtarea.value=2;return FormComplete(this, Array(" & Fields("frmrequeridos") & "))"
			FormOnSubmit = ""
			Fields("frmEsConsulta") = "1"	' Es Consulta
			Fields("frmload") = 1			' Carga datos iniciales 
			' *** Botones adicionales ***
			Fields("frmbotones") = 1
			' Cancelar Operacion
			Fields("frmboton1") = "<INPUT type=""button"" class=""button"" value=""Cancelar"" id=button2 name=button2 onclick='ReloadForm(this.form,""filtro"")' language=javascript >&nbsp;" 
		case "PLAN"	
			Fields("frmestadotit") = "Modificar Registro Actual"
			FormAction = "Grabar.asp"
			FormMethod = "post"
			SubmitValue = "Grabar"
			'..FormOnSubmit = "return FormComplete(this, Array(" & Fields("frmrequeridos") & "))"
			'..FormOnSubmit = "frmtarea.value=2;return FormComplete(this, Array(" & Fields("frmrequeridos") & "))"
			FormOnSubmit = "frmtarea.value=2;return FormCompleto(Array(" & Fields("frmcampos") & "), Array(" & Fields("frmcampostipos") & "), Array(" & Fields("frmrequeridos") & "))"	
			Fields("frmload") = 1			' Carga datos iniciales 
			Fields("frmLocked") = Fields("frmClaves")
			' *** Botones adicionales ***
			Fields("frmbotones") = 2
			' Eliminar Registro 
			Fields("frmboton1") = "<INPUT type=""button"" class=""button"" value=""Eliminar"" id=button1 name=button1 onclick='frmtarea.value=3;action=""" & FormAction & """;method=""POST"";submit()' language=javascript >&nbsp;" 
			' Cancelar Operacion
			Fields("frmboton2") = "<INPUT type=""button"" class=""button"" value=""Cancelar"" id=button2 name=button2 onclick='ReloadForm(this.form,""filtro"")' language=javascript >&nbsp;" 
		' *** Casos no definidos ***
		case else		
	end select
	Fields("frmestadoant") = Fields("frmestado")
End Function

'----------------------------------------------
'	Lee un registro
'----------------------------------------------
Private Function ABMLeeRegistro 
	dim i, Where, value, A, SQL
	
	' Arma Sentencia de SQL para traer el registro 
	Set Rec = Server.CreateObject("ADODB.Recordset")
'PL-SQL * T-SQL  
DBTABLA = TransformPLSQLToTSQL (DBTABLA) 
	Rec.Open DbTabla, ConnEmpleoPersonas,,, adCmdTable
	Where = ""
	for i = 1 to DbCampoClave(0)
		value = Fields(DbCampoClave(i)) ' valor del campo clave en pantalla
		If Len(value) Then
			A = FieldType(DbCampoClave(i))
			If Len(A) then
				if Where <> "" then Where = Where & " AND "
				Where = Where & DbCampoClave(i) & "=" & Formatear(value, A)
			End if
		end if
	next
	Rec.Close
	set Rec = nothing
	SQL = "Select * From " & DbTabla & " Where " & Where
	
	' Trae el registro
	Set cmdBusca = Server.CreateObject("ADODB.Command")
	Set Rec = Server.CreateObject("ADODB.Recordset")
	'Conn.CursorLocation = 3
	With cmdBusca
		.ActiveConnection = ConnEmpleoPersonas
		.CommandText = SQL			'SPnameBuscar
		.CommandType = adCmdText	' adCmdStoredProc
'PL-SQL * T-SQL  
		Set Rec = .Execute
		if rec.BOF = False and rec.EOF = False then
			FieldsLoadDbFields(rec.Fields)
		end if
		Rec.close
		set rec = nothing
	End With
	Set cmdBusca = Nothing
End Function

%>
