<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<!--#INCLUDE VIRTUAL= "/IncludeMtss/adovbs.asp"-->
<!--#INCLUDE VIRTUAL= "/Util/dbutil.asp"-->
<%
class InterfaceGUI


Public Function ArmaTablaGenerico(Stored,Clase)
	
	
	'*************************************************************************************************
	'Escribe una tabla html conteniendo los datos de la consulta ejecutada
	'La tabla se contiene en una cadena de strings como resultado de la funci�n
	'*************************************************************************************************

	Dim ClsADB 'Variable para la clase de acceso a datos
	Dim xxHiddens 'Variable para Hiddens
	
	'Instancio la clase de Acceso a datos
	Set ClsADB = New InterfaceAccessDB
	
	Retorna = ""
	xxHiddens = ""
	
	Set Rec = Server.CreateObject("ADODB.Recordset")

	ClsADB.EjecutaComandoSQLRST Stored, Rec, xxHiddens
	
	
	
		If not Rec.eof then
			Arr = GetRows(Rec)
		Else 
			ArmaTablaGenerico = ""
			Exit Function
		End if
		
		If IsArray(Arr) Then
		
			RecordCount = UBound(Arr, 2)
			' Titulos
				
			Retorna = Retorna & "<table Border=""2"" class=""consultas"" cellspacing=""1"">"
			
			Retorna = Retorna & "<tr class=""trconsultas"" >"
			
			ExisteColCuil = 0
			For FieldIndex = 1 To Rec.Fields.Count - 1
					esSalto = false
					colSpan = 1
					'if FieldIndex <= Rec.Fields.Count - 2 then 
						'.................................................
						' si la siguiente columna a la actual es una referencia para salto 
						' uso el salto definido sino uso el href por default
						'.................................................
					'	If instr(saltosDefinidos,ucase("*" & Rec.Fields(FieldIndex + 1).Name & "*")) > 0 then 
					'		colSpan = 2
					'	end if 
					'end if
					'.....................................................................................
					' si la columna actual es una referencia para salto 
					' uso el salto definido y pongo un icono para ver detalle
					'.....................................................................................
					If instr(saltosDefinidos,ucase("*" & Rec.Fields(FieldIndex).Name & "*")) > 0 then 
						esSalto = true 
					end if 
					if not esSalto then 
						Retorna = Retorna & "<td align=""center"" colspan=" & Colspan & ">"
						'''''''''inizio SM
						if trim(ucase(Rec.Fields(FieldIndex).Name))="CUIL" then
							Retorna = Retorna & " C.I. " 
							Retorna = Retorna & "</td>"
						else
							Retorna = Retorna & Server.HTMLEncode(Rec.Fields(FieldIndex).Name)
							Retorna = Retorna & "</td>"
						end if
						'''''''''fine SM
						if trim(ucase(Rec.Fields(FieldIndex).Name))="CUIL" THEN
							ExisteColCuil=1
						END IF	
					Else
						Retorna = Retorna
					end if 
									
					'Retorna = Retorna & "<td align=""center"">"
					'Retorna = Retorna & Server.HTMLEncode(Rec.Fields(FieldIndex).Name)
					
					
								
			Next
			'Existe Cuil agrego una Columna para poner el Nombre y Apellido de la persona
			if ExisteColCuil=1 then
				retorna=retorna & "<td nowrap width = '25%' align='center' class='tbltext1' bgcolor='#D9D9AE'><b>Apellido &nbsp; y  &nbsp; Nombre</b></td>"
			End If	
			Retorna = Retorna & "</tr>"
			' Lineas de Datos
			For RecordIndex = 0 To RecordCount
				Retorna = Retorna & "<tr class=""Tdconsultas"""
				Retorna = Retorna & RecordIndex Mod 2
				Retorna = Retorna & """>"
				' Referencia para el salto
				salto =""
				if len(SaltoHref)>0  and len(Arr(0, RecordIndex))>0 then
					salto = SaltoHref & Arr(0, RecordIndex)
				end if 
				
				' Emite celdas
				For FieldIndex = 1 To Ubound(Arr, 1)
					esSalto = false
					colSpan = 1
					SaltoXi = ""
					
					'************************************************************************************************************************************************
					' Habria que redise�ar el armado de filas, mirar otros constructores de tablas que usen los saltos multiples
					' Ya que esta forma aparenta tener fallas 
					'************************************************************************************************************************************************
					if (FieldIndex <= Rec.Fields.Count - 2) then 
					'.................................................
					' si la siguiente columna a la actual es una referencia para salto 
					' uso el salto definido sino uso el href por default
					'.................................................
						If instr(saltosDefinidos,ucase("*" & Rec.Fields(FieldIndex + 1).Name & "*")) > 0 then 
							'colSpan = 2
							SaltoXi = Fields("saltoHref" & Rec.Fields(FieldIndex + 1).Name)  &  Cstr(Arr(FieldIndex + 1, RecordIndex))
						end if 
					end if
					
					'.....................................................................................
					' si la columna actual es una referencia para salto 
					' uso el salto definido y pongo un icono para ver detalle
					'.....................................................................................
					If instr(saltosDefinidos,ucase("*" & Rec.Fields(FieldIndex).Name & "*")) > 0 then 
						esSalto = true 
					'	SaltoXi = Fields("saltoHref" & Rec.Fields(FieldIndex).Name) &  Cstr(Arr(FieldIndex, RecordIndex))
					'	AyudaXi = Fields("Ayuda" &  Rec.Fields(FieldIndex).Name)    
					end if 
										
					'Esto no se que es ????????			
					Select Case Rec.Fields(FieldIndex).Type
						Case 3, 6: 
							if not esSalto then 
								Retorna = Retorna & "<td nowrap colspan=" & colSpan & ">"
							end if
								
						Case Else:
							if not esSalto then  
								Retorna = Retorna & "<td nowrap colspan=" & colSpan & ">"
							end if
					End Select
					
					' Datos del salto href Xi
					if len(SaltoXi) > 0 then 
							Retorna = Retorna & "<a href=""" + SaltoXi + """>"
					else
						'Salto por default
						if len(salto) > 0 then
							Retorna = Retorna & "<a href=""" + salto + """>"
						'Else 
						'	Retorna = Retorna & "<td nowrap colspan=" & colSpan & ">"
						'	Retorna = Retorna & Server.HTMLEncode(""  & Arr(FieldIndex, RecordIndex))
						'	Retorna = Retorna & "</td>"	
						end if
					end if 	
					
					'No tiene salto multiple definido
					if not esSalto then 
					'Escribo la celda
						Retorna = Retorna & Server.HTMLEncode(""  & Arr(FieldIndex, RecordIndex))
					end if	
					
					Retorna = Retorna & "</td>"
					
					'************************************************************************************************************************************************
					' Fila del Apellido y Nombre de la Persona
					' No se donde puedo poner esto
					'************************************************************************************************************************************************
					'if ExisteColCuil=1 then
					'	retorna=retorna & "<td align='center' class='tbltext1' bordercolor='LightYellow'>" & ExistePostulante(Arr(FieldIndex, RecordIndex)) & "</td>"
					'End If
										
					'Cierro el href si estan definidos
					If Len(Fields("Excel")) = 0 Then 
						If len(salto) > 0 or len(SaltoXi) > 0 then
							Retorna = Retorna & "</a>"
						End if
					end if	
				Next
				For FieldIndex = 1 To Ubound(Arr, 1)
					'************************************************************************************************************************************************
					' Fila del Apellido y Nombre de la Persona
					' No se donde puedo poner esto
					'************************************************************************************************************************************************
					if ExisteColCuil=1 and Ucase(Rec.Fields(FieldIndex).Name) = "CUIL" then
						retorna=retorna & "<td align='center' class='tbltext1' bordercolor='LightYellow'>" & ExistePostulante(Arr(FieldIndex, RecordIndex)) & "</td>"
					End If
								
				Next
				
				Retorna = Retorna & "</tr>"
			Next
			Retorna = Retorna & "</table><BR>"
		End If
			Retorna = Retorna & xxHiddens
			ArmaTablaGenerico = Retorna
		
		Set ClsADB = nothing
		Rec.close
		Set Rec = nothing
End Function

Public Function DBCamposConstructor(EsCabecera)

'=====================================================================================
' Constructor de campos en pantalla
'=====================================================================================
	' ..(n,0) = Nombre del Parametro o fields
	' ..(n,1) = Descripcion del campo en pantalla (va a la izquierda del campo)
	' ..(n,2) = Tipo de Dato 
	'			T :Texto F :Fecha
	'			N		numerico real
	'			NE		numerico entero
	'			NEP		numerico entero positivo
	'			NEP0	numerico entero positivo sin cero
	'			NRP		numerico real positivo
	'			NRP0	numerico real positivo sin cero
	' ..(n,3) = Tipo de Control (1: Text 2:Combo)
	' ..(n,4) = Longitud para textos (>1) 
	' ..(n,5) = Refresh para combos (0:No 1: con refresco)
	' ..(n,6) = Obligatorio (R)
	' ..(n,7) = Base a la que se conecta 0:Oracle 1:SQL EmpleoLaboro
	' ..(n,8) = ComboSql StoreProcedure para la lista del combo o por parametro busca la descripcion de un dato(codigo)
	
	
	dim Valor, strCampos, strSQL
	
	strCampos = ""	
	' habre la tabla
	strCampos = strCampos & "<table width=""80%"">"
	for I = 1  to cint(DbCampos(0,0))
		' Inicio de la fila
		strCampos = strCampos & "<tr>"
		' Descripcion
		strCampos = strCampos & "<td class=""label"">" & DbCampos(i,1) & "</td>"

		' Segun tipo de campo
		if EsCabecera = "0" then
			' campo de datos
			'strCampos = strCampos & Fields(DbCampos(i,0))
			'--------------------------------------------
			' Descripcion del campo
			'--------------------------------------------
			strCampos = strCampos  & "<td class=""tbltext1"">"
			if len(DbCampos(i,8)) >0 then
				' busco la descripcion en SQL
				'*********************************************************************************************
				' Esto se daria en el caso de ser una sentencia SQL
				' Ver mas adelante 
				' Modificacion realizada el 04/05/2006
				'*********************************************************************************************
				'...' 1) Armar la sentencia SQL del dato --> reemplazar las condiciones *1 x fields(DbCampos(i,0))
				'...' preparo el dato 
				'...select case DbCampos(i,2)
					'...case "F", "T"
						'...valor = "'" & fields(DbCampos(i,0)) & "'"
					'...case else
						'...valor = "" & fields(DbCampos(i,0))
				'...end select
				' reemplazo la sentencia
				'...strSQL = StringReemplaza (DbCampos(1,9), "*1", valor)
				'...'2) ejecuto la sentencia
				'...set rsDesc = server.CreateObject("adobd.recordset")
				
				
				'Instancio la clase de Acceso a datos
					Set ClsAcDB = New InterfaceAccessDB
				' segun coneccion a usar

				select case cint(DbCampos(i,7))

					case 0:	' Oracle
						'ClsADB.EjecutaComandoSQLConn DbCampos(i,8),cc
						set rsDesc = cc.open(strSQL)
						'3) presento la descripcion y emito la descripcion
						strCampos = strCampos  & "[" & Fields(DbCampos(i,0)) & "]"
						strCampos = strCampos  & RsDesc.Field(0) 
					case 1	' SQL
						ClsAcDB.EjecutaComandoSQLConn DbCampos(i,8), connLavoro
						'3) presento la descripcion y emito la descripcion
						strCampos = strCampos  & "[" & Fields(DbCampos(i,0)) & "]"
						strCampos = strCampos  & Fields("Descripcion") 
						'set rsDesc = connLavoro.Open(strSQL)
					case else
						strCampos = strCampos & "Constructor de campos - Base de datos no definida "
						strCampos = strCampos & "Response.end"
				end select

				Set ClsADB = nothing
				strCampos = strCampos & "</td>"
			Else 
				strCampos = strCampos  & Fields(DbCampos(i,0)) 
				strCampos = strCampos & "</td>"
			end if
		else
			strCampos = strCampos & "<td>" 
			'======================================================
			' campos de ingreso de datos
			'======================================================
			select case cint(DbCampos(i,3))
				case 1	' texto 
					strCampos = strCampos & text(DbCampos(i,0), DbCampos(i,4))
				case 2  ' Texto con form de busqueda
					strCampos = strCampos & text(DbCampos(i,0), DbCampos(i,4))
					strCampos = strCampos & "<a href=""Javascript:IrA('../BuscarPostulanteConFields.asp')"" ID=""imgPunto1"" name=""imgPunto1"" onmouseover=""javascript:window.status='';return true""><img border=""0"" src=" & Session("Progetto") & "/images/bullet1.gif></a>"
				case 3	' combo
					'ComboSP(varName, SQL, refresh, Ancho,Conn)
					select case cint(DbCampos(i,7))
						case 0:	' Oracle
							strCampos = strCampos & ComboSP(DbCampos(i,0),DbCampos(i,8) ,DbCampos(i,5),DbCampos(i,4),CC)
						case 1	' SQL
							strCampos = strCampos & ComboSP(DbCampos(i,0),DbCampos(i,8) ,DbCampos(i,5),DbCampos(i,4),connLavoro)
						case else
							strCampos = strCampos & "Constructor de campos - Base de datos no definida "
							'Response.end
						end select
				case else
			end select
			strCampos = strCampos & "</td>" 
		end if
		' separador de filas 
		strCampos = strCampos & "</tr>"
	next 
	' cierra la tabla
	strCampos = strCampos & "</table>"
	
	DBCamposConstructor = strCampos
	
End Function


Public function FormularioArma(Proceso, AnchoPregunta,AnchoRespuesta, TextAreaAltoDef,TextAreaAnchoDef) 
'==============================================================
'	Arma el Formulario en pantalla
'==============================================================
	Dim i 
	FormularioArma = 0
	xx = "" 
	i = 0
	SPnameParametrosSP = "ProcesoParametrosLee" 
	
	'----------------------------------------------
	' Lee los elementos del Formulario
	'----------------------------------------------
	Set cmdParametros = Server.CreateObject("ADODB.Command")
	Set RecElem = Server.CreateObject("ADODB.Recordset")
	With cmdParametros
		.ActiveConnection = connLavoro
		.CursorLocation = 3
		.CommandTimeout = 240
		.CommandText = SPNameParametrosSp
		.CommandType = adCmdStoredProc
		FieldsSaveParameters .Parameters		
'		For Each P In .Parameters
'			Response.Write P.Name & "=" & P.Value & "<br />"
'		Next
'		Response.End
'PL-SQL * T-SQL  
RECORDCOUNT = TransformPLSQLToTSQL (RECORDCOUNT) 
		Set RecParametros = .Execute(Recordcount)
		FieldsLoadParameters .Parameters
'		For Each P In .Parameters
'			Response.Write P.Name & "=" & P.Value & "<br />"
'		Next
'		Response.End
		'Redimensiona el DbCampos segun cantidad total de parametros del Proceso	
	'	redim DbCampos(Fields("TotalParametros"),3) 
	
		' Recorre los elementos del formulario
		Response.Write "<table align=""center"">"

		Do While recParametros.BOF = false and recParametros.EOF = false 
			i = i + 1
			Size = 0
			'.........................................................
			'..Armo el DbCampos de Definicion 
			'.........................................................
			DbCampos(i,0) = "Param" & i  
			DbCampos(i,1) = recParametros("descripcion")
			DbCampos(i,2) = RecParametros("CodigoWeb")
			if RecParametros("Obligatorio") = "1"  then 
				DbCampos(i,3) = "R"
			else 
				DbCampos(i,3) = ""
			end if 	
			
			'.........................................................
			'.. Arma el control en pantalla segun el tipo de elemento
			'.........................................................
			if recParametros("NoMostrar") = 0 then 
				'si no es combo arma text o textarea dependiendo del size 
				if len(trim(recParametros("ComboSelect"))) = 0 then 
					if recParametros("SizeDato") = 0 then
						Select case recParametros("TipoDato")
							case 3,4,5,6,7,8 'numericos
								Size = 10
							case 1,13  'texto 
								Size = 20 
							case 2 'fecha
								Size = 10
							case 10,11
								Size = 70 'Arcchivo
							'case 12
							
							case else 
							'	
						end select	
					else 
						Size = recParametros("SizeDato")	
					end if  
					
					if Size < 60 then 'TextBox y CheckBox
					Select Case recParametros("TipoDato")
						Case 13 'CheckBox
							xx = "<tr><td align=""Right"" class=""label"">" & recParametros("descripcion")
							xx = xx & ": </td>"
							xx = xx & "<td colspan=""3"">"
							xx = xx & Check("Param" & Cstr(i) , 1)
							xx = xx & "<input type=hidden name=" & "Nombre" & Cstr(i) & " value=" & """" & recParametros("nombre") & """" & ">"
							xx = xx & "</td></tr>"
						case else 'TextBox
							xx = "<tr><td align=""Right"" class=""label"">" & recParametros("descripcion")
							xx = xx & ": </td>"
							xx = xx & "<td colspan=""3"">"
							xx = xx & Text("Param" & Cstr(i) , Size)
							xx = xx & "<input type=hidden name=" & "Nombre" & Cstr(i) & " value=" & """" & recParametros("nombre") & """" & ">"
							xx = xx & "</td></tr>"
					End Select
					else
					Select Case recParametros("TipoDato")
					case 11,12 'Archivo
								xx = "<tr>"
								xx = xx & "<td align=""Right"" class=""label"">" & recParametros("descripcion")
								xx = xx & ": </td>"	
								xx = xx & "<td>"
								xx = xx & CuadroDiag("Param" & Cstr(i) , Size)
								'xx = xx & "<input type=file size=" & Size & " name=" & "Param" & Cstr(i) & " id=" & recParametros("nombre") & " MAXLENGTH=""1000000""></td>"
								xx = xx & "<input type=hidden name=" & "Nombre" & Cstr(i) & " value=" & """" & recParametros("nombre") & """" & ">"
								xx = xx & "</tr>"							
					case else			
								'textarea
								xx = "<tr><td align=""Right"" class=""label"">" & recParametros("descripcion")
								xx = xx & ": </td>"
								xx = xx & "<td colspan=""3"">"
								'xx = xx & TextArea("Param" & Cstr(i) ,AnchoFila, AnchoColumna,recParametros("SizeDato"))
								xx = xx & TextArea("Param" & Cstr(i) ,AnchoFila, AnchoColumna, Size)
								xx = xx & "<input type=hidden name=" & "Nombre" & Cstr(i) & " value=" & """" & recParametros("nombre") & """" & ">"
								xx = xx & "</td></tr>"
					end select		
					end if 	
				else 
				'combo 	
					xx = "<tr><td align=""Right"" class=""label"">" & recParametros("descripcion")
					xx = xx & ": </td>"
					xx = xx & "<td>" & Combo("Param" & Cstr(i), Cstr(recParametros("ComboSelect")), false, 20)
					xx = xx & "<input type=hidden name=" & "Nombre" & Cstr(i) & " value=" & """" & recParametros("nombre") & """" & ">"
					xx = xx & "</td></tr>"
				end if 
			else 
				'armo el oculto 
				'Response.write recParametros("VariableExterna")
				xx = "<input type=hidden name=" & "Param" & Cstr(i) & " value=" & Fields(trim(recParametros("VariableExterna"))) & ">"
				xx = xx & "<input type=hidden name=" & "Nombre" & Cstr(i) & " value=" & """" & recParametros("nombre") & """" & ">"
			end  if 	
			Response.Write xx
			' siguiente parametro del proceso
			recParametros.movenext
		loop 
		Response.Write "</table>"
		recParametros.close
		Set recParametros = nothing
	End With
	Set cmdParametros = nothing

	'----------------------------------------------------------------------------
	'	Conexiones Entre campos Externos y Preguntas del formulario
	'----------------------------------------------------------------------------
	' Mete todos los fields de campos externos como Hidden
	if xxHidden <> "" then
		Response.Write xxHidden
	end if

	

	'-----------------------
	' Salida OK
	'-----------------------
	FormularioArma = 1
end function

Public function ExistePostulante(Cuil)
'=========================================================================================
' VERIFICA LA EXISTENCIA DEL POSTULANTE A TRAVES DEL CUIL Y DEVUELVE EL NOMBRE Y APELLIDO
'=========================================================================================
	Dim RsBusca
	Dim SqlBusca

	ExistePostulante = ""
	
	'Response.Write Cuil
	'Response.End
	set RsBusca = server.CreateObject("Adodb.recordset")

	SqlBusca = "Select cognome,nome from Persona where cod_fisc='" & cstr(Cuil) & "'"
	
'PL-SQL * T-SQL  
SQLBUSCA = TransformPLSQLToTSQL (SQLBUSCA) 
	set RsBusca = CC.execute(SqlBusca)
	
	if not RsBusca.EOF then
		RsBusca.MoveFirst 
		ExistePostulante = RsBusca.Fields(0) & " " & RsBusca.fields(1)
	else
		ExistePostulante = ""
	end if
	
	set RsBusca = nothing
End function

Public Function ExportaExcel(SpName,Titulo,Subtitulo)

	Dim ClsADB 'Variable para la clase de acceso a datos
	Dim xxHiddens
	'Instancio la clase de Acceso a datos
	Set ClsADB = New InterfaceAccessDB
	

	sFilePath = Server.MapPath("/") & session("progetto") & "/DocPers/Statistiche/" &_
	        Session("IdUtente") & "_Export.xls"

	sFileNameJS = session("progetto") & "/DocPers/Statistiche/" &_
	   Session("IdUtente") & "_Export.xls"
	
	'sFileNameJS = Session("Server") & Session("IdUtente") & "_Export.xls"
	
	set oFileSystemObject = Server.CreateObject("Scripting.FileSystemObject")
	
	If oFileSystemObject.FileExists(sFilePath) then
		oFileSystemObject.DeleteFile sFilePath,true 
	End If

	set oFileObj = oFileSystemObject.CreateTextFile(sFilePath,true)


	' ********************************
	' Scrivo la intestazione del file.
	' ********************************
	oFileObj.WriteLine (Titulo)
	oFileObj.WriteLine ("")
	oFileObj.WriteLine (Subtitulo)
	oFileObj.WriteLine ("")

	Set Rec = Server.CreateObject("ADODB.Recordset")

	ClsADB.EjecutaComandoSQLRST SpName, Rec, xxHiddens
	
	RetornaCab = ""
		If not Rec.eof then
			Arr = GetRowsSinPaginado(Rec) ''''modificado por vany para que grabe todos los registros.
		Else 
			ExportaExcel = ""
			Exit Function
		End if
		
		If IsArray(Arr) Then
		
			RecordCount = UBound(Arr, 2)
			
			'RetornaCab = RetornaCab & chr(9)
			' Titulos
			ExisteColCuil = 0
			For FieldIndex = 1 To Rec.Fields.Count - 1
					esSalto = false
					colSpan = 1
					If instr(saltosDefinidos,ucase("*" & Rec.Fields(FieldIndex).Name & "*")) > 0 then 
						esSalto = true 
					end if 
					
					if not esSalto then 
						RetornaCab = RetornaCab & Server.HTMLEncode(Rec.Fields(FieldIndex).Name)
						RetornaCab = RetornaCab & chr(9)
					end if 
			Next
			
			oFileObj.WriteLine (RetornaCab)
			
			RetornaDetalle = ""
			
			For RecordIndex = 0 To RecordCount
				'RetornaDetalle = RetornaDetalle 
				'RetornaDetalle = RetornaDetalle & RecordIndex Mod 2 & chr(9)
				
				' Emite celdas
				For FieldIndex = 1 To Ubound(Arr, 1)
					esSalto = false
					colSpan = 1
					SaltoXi = ""
					'.....................................................................................
					' si la columna actual es una referencia para salto 
					' uso el salto definido y pongo un icono para ver detalle
					'.....................................................................................
					If instr(saltosDefinidos,ucase("*" & Rec.Fields(FieldIndex).Name & "*")) > 0 then 
						esSalto = true 
					'	SaltoXi = Fields("saltoHref" & Rec.Fields(FieldIndex).Name) &  Cstr(Arr(FieldIndex, RecordIndex))
					'	AyudaXi = Fields("Ayuda" &  Rec.Fields(FieldIndex).Name)    
					end if 
										
					'No tiene salto multiple definido
					if not esSalto then 
					'Escribo la celda
						RetornaDetalle = RetornaDetalle & Server.HTMLEncode(""  & Arr(FieldIndex, RecordIndex))
						RetornaDetalle = RetornaDetalle & chr(9)
					Else 
						RetornaDetalle = RetornaDetalle
					end if	
										
				Next
				RetornaDetalle = RetornaDetalle & chr(13)
			Next
			oFileObj.WriteLine (RetornaDetalle)
		End if
			
				ExportaExcel = sFileNameJS
				
				Rec.close
				set Rec = nothing 
				oFileObj.CLose
				set oFileObj = nothing
				set oFileSystemObject = nothing
				Set ClsADB = Nothing

	
End Function
End Class
%>
