<%
'=================================================================
'	Persistencia en ASP del lado del cliente 
'=================================================================
'=================================================================
'	Funciones Necesarias del lado del servidor
'=================================================================

'-------------------------------------------------------
' Manejo de hidden entre llamados de paginas
'-------------------------------------------------------
dim PersistenciaRenglones, PersistenciaDatos 

if request("PersistenciaRenglones") = "" then
	PersistenciaRenglones = ""
else
	PersistenciaRenglones = request("PersistenciaRenglones")
end if

if request("PersistenciaDatos") = "" then
	PersistenciaDatos = ""
else
	PersistenciaDatos = request("PersistenciaDatos")
end if

function RegistrosSeleccionadosAgregaItem (NroItem, ItemDatos)
'-------------------------------------------------------
' Agrega un item seleccionado a los vectores de persistencia
' Se usa en la primer lectura de los registros de la base
'
'Necesita:	
'	Pagina		nro pagina de datos a mostrar
'	NroItem		numero del item del registro
'	ItemDatos	item de datos formateados
'
'Devuelve:
'	Arma PersistenciaRenglones y PersistenciaDatos
'-----------------------------------------------------
	dim Separador
	Separador = "*"
	
	if len(PersistenciaRenglones) = 0 then
		PersistenciaRenglones = "0" & Separador & "0" 
		PersistenciaDatos = "x" & Separador & "x" 
	end if
	
	PersistenciaRenglones = PersistenciaRenglones & Separador & NroItem
	PersistenciaDatos = PersistenciaDatos & Separador & ItemDatos
	
end function

function RegistrosSeleccionadosBusca (xItemDatos)
'-------------------------------------------------------
' Busca si existe un item de datos en el vector de items seleccionados
'
'Necesita:	
'	ItemDatos	item de datos formateados
'
'Devuelve:
'	Funcion = True	--> Encontro
'-----------------------------------------------------
	dim Separador, Encontro, ItemDatos
	Separador = "*"
	
	Encontro = false
	ItemDatos = trim(cstr(xItemDatos))
	
	' si tiene un dato valido
	if len(trim(ItemDatos))> 0 then
		' busca si existe con los dos separadores
		DatosSel = Separador & ucase(ItemDatos) & Separador
		if instr( ucase(PersistenciaDatos), DatosSel)> clng(0) then
			Encontro = true
		else
			' busca si existe en el primer elemento
			DatosSel = ltrim(ucase(ItemDatos) & Separador)
			if left(ltrim(ucase(PersistenciaDatos)), len(DatosSel)) = DatosSel then
				Encontro = true
			else
				' busca si existe en el ultimo elemento
				DatosSel = Separador & ucase(ItemDatos)
				if right(rtrim(ucase(PersistenciaDatos)), len(DatosSel)) = DatosSel then
					Encontro = true
				end if
			end if
		end if
	end if

'..	Response.Write "itemdatos:" & ItemDatos & " Encontro:" & Encontro & " PersistenciaDatos:" & PersistenciaDatos
	
	' Salida
	RegistrosSeleccionadosBusca = Encontro
	
end function	
	
	

%>


<SCRIPT LANGUAGE=VBScript>
<!--
'=================================================================
'	Persistencia en ASP del lado del cliente 
'
'	Funciones Especiales del lado del Cliente
'=================================================================


'==========================================================
'	Manejo de Selecciones multipaginas
'==========================================================
'----------------------------------------------------------
'	Actualiza los items seleccionados de la pagina actual
'----------------------------------------------------------

function RegistrosSeleccionadosRefreshForm2 (PrimerRenglonPaginaActual, FormName)
'--------------------------------------------------------------------
'	Actualiza la lista de registros seleccionados en el formulario
'
'Llama a la funcion principal xRegistrosSeleccionadosRefresh
'
'Necesita:	
'	PrimerRenglonPaginaActual	Primer renglon de la pgina actual
'	FormName		Formulario donde estan las variables hidden necesarias
'Devuelve
'	Funcion = 1 --> todo bien
'--------------------------------------------------------------------
	dim PaginaCantReg, PaginaActual
	
	PaginaCantReg = document.forms(FormName).PersistenciaPaginadoCantRenglones.value
	PaginaActual = clng(PrimerRenglonPaginaActual / PaginaCantReg)  + 1 
	
	RegistrosSeleccionadosRefreshForm2 = RegistrosSeleccionadosRefreshForm (PaginaActual, FormName)	
	
end function


function RegistrosSeleccionadosRefreshForm (PaginaActual, FormName)
'--------------------------------------------------------------------
'	Actualiza la lista de registros seleccionados en el formulario
'
'Llama a la funcion principal xRegistrosSeleccionadosRefresh
'
'Necesita:	
'	PaginaActual	pagina actual de registros
'	FormName		Formulario donde estan las variables hidden necesarias
'Devuelve
'	Funcion = 1 --> todo bien
'--------------------------------------------------------------------
	dim PaginaCantReg, TotRegistros, xVectorSelRenglon, xVectorSelDatos
	
	' recupera datos del form	
	PaginaCantReg = document.forms(FormName).PersistenciaPaginadoCantRenglones.value 
	TotRegistros = document.forms(FormName).PersistenciaTotalRegistros.value 
	xVectorSelRenglon = document.forms(FormName).PersistenciaRenglones.value 
	xVectorSelDatos = document.forms(FormName).PersistenciaDatos.value 

'..msgbox  "Inicio:" & chr(13) & "xVectorSelRenglon=" & xVectorSelRenglon & chr(13) & "xVectorSelDatos=" & xVectorSelDatos

	' ejecuta la funcion
  	RegistrosSeleccionadosRefreshForm = xRegistrosSeleccionadosRefresh (PaginaActual, PaginaCantReg, TotRegistros, xVectorSelRenglon, xVectorSelDatos)
	
	' registra los resultados en el form
	document.forms(FormName).PersistenciaRenglones.value = xVectorSelRenglon  
	document.forms(FormName).PersistenciaDatos.value = xVectorSelDatos  
  	
'..msgbox  "Resultados:" & chr(13) & "xVectorSelRenglon=" & xVectorSelRenglon & chr(13) & "xVectorSelDatos=" & xVectorSelDatos & chr(13) & "xVectorSelRenglon:"  & document.forms(FormName).PersistenciaRenglones.value & "xVectorSelDatos:" & document.forms(FormName).PersistenciaDatos.value
  	
end function


function xRegistrosSeleccionadosRefresh (PaginaActual, PaginaCantReg, TotRegistros, xVectorSelRenglon, xVectorSelDatos)
'--------------------------------------------------------------------
'	Funcion Principal
'--------------------------------------------------------------------
'	Actualiza la lista de registros seleccionados en la pagina actual
'	en el array persistente
'
'Necesita:	
'	PaginaActual	pagina actual de registros
'	PaginaCantReg	Cantidad de registros por pagina
'	TotRegistros	Total de registros en archivo
'
'	xVectorSelRenglon	Lista de reglones seleccionados		
'	xVectorSelDatos		Lista de datos de items seleccionados
'	xVectorSelPaginas	Lista de paginas visitadas
'Devuelve: 
'	funcion			=	1 -->	Todo OK
'					<>	1		Error
'--------------------------------------------------------------------
	dim MaxSelRenglones, i, j, RenglonInicial, RenglonFinal, ItemInicial, ItemFinal
	dim NameElemento, Indice, Separador, EstaSeleccionado, ItemDatos
	
	RegistrosSeleccionadosRefresh = 0
	Separador = "*"

	'--------------------------------------------------
	'	Recupera datos de los arrays 
	'--------------------------------------------------
	' datos vacios
	if len(xVectorSElRenglon) = 0 then
		xVectorSElRenglon = "0" & Separador & "0" 
		xVectorSelDatos = "x" & Separador & "x" 
	end if
	
	' Reglones seleccionados (todos)
	VectorSelRenglon = split(xVectorSElRenglon, Separador)
	MaxSelRenglones = Ubound(VectorSelRenglon)
	
	' Datos de los reglones seleccionados
	VectorSelDatos = split(xVectorSelDatos, Separador)
	
	'--------------------------------------------------
	'	Datos de la pagina Actual
	'--------------------------------------------------
	' items mostrados en la pagina actual
	ItemInicial = 1
	ItemFinal = clng(PaginaCantReg) 

	' Rango de Reglones de la pagina actual
	RenglonInicial = 1 + ((clng(PaginaActual)- 1)  * clng(PaginaCantReg))
	RenglonFinal = clng(RenglonInicial)  + clng(PaginaCantReg) - 1

	
	' Hay menos renglones en la pagina ?
	' --> se ajusta a la cantidad de registros mostrados y en archivo
	if clng(RenglonFinal) > clng(TotRegistros ) then
		' ajusta renglones de archivo
		RenglonFinal = clng(TotRegistros ) 
		' ajusta items en pantalla
		ItemFinal = clng(RenglonFinal) - clng(RenglonInicial) +1
	end if

	'--------------------------------------------------
	'	Borra las selecciones anteriores en VectorSelRenglones
	'--------------------------------------------------
	' recorre los reglones seleccionados
	' si hay selecciones de esta pagina les pone pagina cero
	for i = 1 to clng(MaxSelRenglones)
		' es un elemento seleccionado de la pagina actual ?
		if len(VectorSelRenglon (i)) = clng(0) then
			' elemento nulo --> lo pone en cero
			VectorSelRenglon (i) = clng(0)
		elseif clng("" & VectorSelRenglon (i)) >=  clng(RenglonInicial) and clng("" & VectorSelRenglon (i)) <=  clng(RenglonFinal) then
			' lo borra
			VectorSelRenglon (i) = clng(0)
		end if
	Next

	'--------------------------------------------------
	'	Asigna selecciones actuales
	'--------------------------------------------------
	' Recorre elementos de la pagina y los agrega
	for i = clng(ItemInicial) to clng(ItemFinal)	
		' elemento de la pagina para evaluar
		ItemDatos = ""
		EstaSeleccionado = RegistrosSeleccionadosVerificaSeleccion (i, ItemDatos)

		' esta seleccionado el item ?
		if EstaSeleccionado = true then
			'------------------------------------------------------
			' asigna el dato seleccionado al array persistente
			'------------------------------------------------------
			' busca elemento no usado en el array
			Indice = clng(-1)
			for j = 1 to (clng(MaxSelRenglones)-1) ' tiene que ser desde uno
				' es un elemento seleccionado de la pagina actual ?
				if clng(VectorSelRenglon (j))  = clng(0) then
					Indice = j
					exit for					
				end if
			Next
			
			' No hay libre --> agrega item
			if clng(Indice) < clng(0) then
				' No Hay espacio en el array actual?
				Indice = clng(MaxSelRenglones)
				MaxSelRenglones = clng(MaxSelRenglones) + clng(PaginaCantReg)
				' agranda vectores
				redim preserve VectorSelRenglon (MaxSelRenglones)
				redim preserve VectorSelDatos (MaxSelRenglones)
			end if
			
			' Asigna el elemento seleccionado al array persistente				
			' nro de renglon o registro de archivo
			VectorSelRenglon (Indice) = clng(RenglonInicial) + clng(i) - 1
			' datos de la seleccion realizada
			VectorSelDatos(Indice) = ItemDatos
		end if
	next
	
	for I = 0 to MaxSelRenglones
		xxx = xxx & "-" & VectorSelDatos(I)
	next

	'--------------------------------------------------
	'	Rearma las listas de datos 
	' pasa los vectores a strings formateados
	'--------------------------------------------------
	xVectorSelRenglon = ""
	xVectorSelRenglon = StringFormateadoArma (VectorSelRenglon, Separador, "0")
	xVectorSelDatos= ""
	xVectorSelDatos = StringFormateadoArma (VectorSelDatos, Separador, "x")

	'--------------------------------------------------
	'	Salida
	'--------------------------------------------------
	RegistrosSeleccionadosRefresh = 1
	
end function

function StringFormateadoArma (Vector(), Separador, ElementoVacio)
'--------------------------------------------------------------------
'	Arma un string a partir de un vector con el separador indicado
'
'Necesita:	
'	Vector()		Vector con datos
'	Separador		Separados a usar entre elementos
'	ElementoVacio	Elemento Vacio por defecto para que no pinche el split 
'						Ej. 0 o x
'Devuelve: 
'	funcion			String Formateado
'--------------------------------------------------------------------
	dim Max, i, j, StrSalida
	
	' elementos validos del vector
	Max = Ubound(Vector)

	' arma vector 
	StrSalida = ""
	for i = 0 to clng(Max)
		if len(StrSalida)  > cint(0) then
			StrSalida = StrSalida & Separador 
		end if 
		StrSalida = StrSalida & Vector(i)
	next

	' verificaciones para ser vector
	if Max <= 2  and len(ElementoVacio) > cint(0) then
		StrSalida = ElementoVacio & Separador & ElementoVacio & Separador & StrSalida
	end if
	
	' Salida
	StringFormateadoArma = StrSalida
		
end function

-->
</SCRIPT>

