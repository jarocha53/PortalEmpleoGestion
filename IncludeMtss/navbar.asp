<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%

Private CurrentPage, PageCount, RecordCount

Public Function GetRows(Recordset)
	With Recordset
		If Not .EOF Then
			RecordCount = .RecordCount
			If Len(Fields("excel")) = 0 Then
				If ((CurrentPage - 1) * PageSize) <= RecordCount Then
					.Move ((CurrentPage - 1) * PageSize)
				Else
					CurrentPage = 1
				End If
				GetRows = .GetRows(PageSize)
				PageCount = -Int(-RecordCount / PageSize)
			Else
				GetRows = .GetRows()
				PageCount = 1
			End If
		End If
	End With
End Function
	
Public Function GetRowsSinPaginado(Recordset)

''''funcion que permite que grabe todos los registros sin paginar 
''''vany

	With Recordset
		If Not .EOF Then
			If Len(Fields("excel")) = 0 Then
				GetRowsSinPaginado = .GetRows()
			End If
		End If
	End With
End Function

Public Sub WriteRecordsPerPage2()
	Response.Write "<select id=""rpp"" name=""rpp"" class=""Paging"" onchange=""Reload(this.form)"" language=""JavaScript"" style=""font-family: Arial; color: #00000; font-weight: normal;font-size: 12px"">"
	For i = 10 To 100 Step 10
		Response.Write "<option "
		If CStr(i) = CStr(PageSize) Then
			Response.Write "selected=""selected"" "
		End If
		Response.Write "value="""
		Response.Write i
		Response.Write """>"
		Response.Write i
		Response.Write "</option>"
	Next
	Response.Write "</select>"
End Sub

Public Sub WriteNavBar2()
	BaseURL = "results.asp?"
		Response.Write "<span class=""Paging"">"
		Response.Write "<select id=""p"" name=""p"" onchange=""Reload(this.form)"" language=""JavaScript""  class=""Paging""  style=""font-family: Arial; color: #00000; font-weight: normal;font-size: 12px"" >"
		For i = 1 To PageCount
			Response.Write "<option "
			If CStr(i) = CStr(CurrentPage) Then
				Response.Write "selected=""selected"" "
			End If
			Response.Write "value="""
			Response.Write i
			Response.Write """>"
			Response.Write i
			Response.Write "</option>"
		Next
		Response.Write "</select> de "
		Response.Write PageCount
		Response.Write "</span>"
End Sub

Public Function WriteRecordsPerPage()

	Retorna = ""
	Retorna = Retorna &"<select id=""rpp"" name=""rpp"" class=""Paging"" onchange=""submit()"">"
	For i = 10 To 100 Step 10
		Retorna = Retorna & "<option "
		If CStr(i) = CStr(PageSize) Then
			Retorna = Retorna & "selected=""selected"" "
		End If
		Retorna = Retorna & "value="""
		Retorna = Retorna & i
		Retorna = Retorna & """>"
		Retorna = Retorna & i
		Retorna = Retorna & "</option>"
		
	Next
	Retorna = Retorna & "</select>"
	WriteRecordsPerPage = Retorna
End Function

Public Function WriteNavBar()
		Retorna = ""
		BaseURL = "results.asp?"
		'Retorna = Retorna & "<td width=""30%"" align=""center"" class=""tbltext1"">"
		Retorna = Retorna & "<span class=""Paging"">"
		Retorna = Retorna & "<select id=""p"" name=""p"" onchange=""submit()"" class=""Paging"">"
		For i = 1 To PageCount
			Retorna = Retorna & "<option "
			If CStr(i) = CStr(CurrentPage) Then
				Retorna = Retorna & "selected=""selected"" "
			End If
			Retorna = Retorna & "value="""
			Retorna = Retorna & i
			Retorna = Retorna & """>"
			Retorna = Retorna & i
			Retorna = Retorna & "</option>"
		Next
		Retorna = Retorna & "</select> de "
		Retorna = Retorna & PageCount 
		Retorna = Retorna & "</span>" '& "</TD>"
		WriteNavBar = Retorna
		
End Function

Public Function WriteNavBarList()
			Dim Pagina
			Dim Retorna
			Pagina = ""
			Retorna = ""
			'...Inicio la tabla
			Retorna = Retorna & "<table class=""Footer"" width=""100%"">"
			Retorna = Retorna & "<tr class=""Paging"">"
			'...Boton de avance hacia atras
			'if CurrentPage > 1 then
			'	Pagina = CurrentPage - 1
			'Retorna = Retorna & "<td align='left' width='480'>"
			'	Retorna = Retorna & "<input type=""submit"" class=""my"" value=""Atras"" name=""Atras"" onsubmmit=""WriteNavBarListPaginadoFino(2)"">"
				'Retorna = Retorna & "<a href=""javascript:Reload(this.form)""><img src='" & Session("Progetto") & "/images/precedente.gif' border='0' id='image1' name='image1'>"
				'Retorna = Retorna & "</td>"				
				'Response.Write "&nbsp;&nbsp;-&nbsp;&nbsp;"
			'Else
			Retorna = Retorna & "<td width=""20%"" align=""center"" class=""tbltext1"">p&aacute;gina "
			'Retorna = Retorna & "</td>"
			'end if
			'..Listado de paginas (combo)
			'Retorna = Retorna & "&nbsp;p&aacute;gina "
			Retorna = Retorna & WriteNavBar
			'Retorna = Retorna & "<td align=""center"" class=""tbltext1"">"
			Retorna = Retorna & "&nbsp;&nbsp;-&nbsp;&nbsp; "
			
			'..Listado de registros por pagina
			Retorna = Retorna & WriteRecordsPerPage
			Retorna = Retorna & " registros por p&aacute;gina"
			'...Boton de avance hacia adelante
			'if CurrentPage < PageCount then
			'	Pagina = CurrentPage + 1
			'	Retorna = Retorna & "<td align='center'>"
			'Retorna = Retorna & "<input type=""submit"" class=""my"" value=""Adelante"" name=""Adelante"" onsubmmit=""WriteNavBarListPaginadoFino(1)"">"
			'	Retorna = Retorna & "</td>"
			'end if
			'...Exportar a Excel
			Retorna = Retorna & "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
			'Retorna = Retorna & "<a href="""
			'Retorna = Retorna & Request.ServerVariables("SCRIPT_NAME")
			'Retorna = Retorna & "?"
			'Retorna = Retorna & ToQueryString()
			'Retorna = Retorna & "&excel=1><img alt=""Exportar a Microsoft Excel&copy;"" border=""0"" src=""/BA/images/xls.jpg"" width=""16px"" height=""16px"">"
			'Retorna = Retorna & "<a class=""textred"" href=Javascript:Scarica(""=siFileNameJS"")><img alt=""Exportar a Microsoft Excel&copy;"" border=""0"" src=""/BA/images/xls.jpg"" width=""16px"" height=""16px"">"
			'Retorna = Retorna & "<a class=""textred"" href=siFileNameJS><img alt=""Exportar a Microsoft Excel&copy;"" border=""0"" src=""/BA/images/xls.jpg"" width=""16px"" height=""16px"">"
				
			Retorna = Retorna & "</td>"
			Retorna = Retorna & "</tr>"
			Retorna = Retorna & "</table>"
			
			WriteNavBarList = Retorna
		
End Function
%>
