<%
'=================================================================
'	Funciones Especiales de Manejos de Strings
'=================================================================

'-----------------------------------------------------------------
' Reemplaza valores de un String
'-----------------------------------------------------------------
Private Function StringReemplaza (strTexto, Pattern, Valores)
'-----------------------------------------------------------------
'Necesita: 
'	strTexto	string a reemplazar el pattern por los valores
'	Pattern		patron a reemplazar
'	Valores()	valores a reemplazar. 
'	Valores(0)	cantidad de valores validos
'Devuelve:
'	funcion		string con los reemplazos
'-----------------------------------------------------------------
	dim i, strAux
	
	strAux = strTexto
	
	' Busca el pattern en el string
	redim Posit(10) 
	call InstrPosit(strTexto , Pattern , Posit)
	
	' Reemplaza el pattern por los valores 
	' Empieza de atras para adelante
	for I = Posit(0) to 1 step -1
		if i <= Valores(0) then
			strAux = left(strAux, Posit(i) - 1) & Valores(i) & mid(strAux, Posit(i) + 1)
		end if
	next	
	StringReemplaza = strAux	
End Function

Private Function StringReemplaza1(strTexto, Pattern, NewValor)
'-----------------------------------------------------------------
' Reemplaza valores de un String
'-----------------------------------------------------------------
'Necesita:
'   strTexto    string a reemplazar el pattern por el valor
'   Pattern     patron a reemplazar
'   NewValor     valor a reemplazar.
'Devuelve:
'   funcion     string con los reemplazos
'-----------------------------------------------------------------
    Dim i, strAux
    
    strAux = strTexto
    
    ' Busca el pattern en el string
    ReDim Posit(10)
    Call InstrPosit(strTexto, Pattern, Posit)
    
    ' Reemplaza el pattern por los valores
    ' Empieza de atras para adelante
    For i = Posit(0) To 1 Step -1
        strAux = Left(strAux, Posit(i) - 1) & NewValor & Mid(strAux, Posit(i) + 1)
    Next
    StringReemplaza1 = strAux

End Function

private Sub InstrPosit(Texto , Pattern , Posit() )
'-----------------------------------------------------------------
' Dado un texto, busca la existencia del Pattern y
' devuelve la ocurrencia del mismo y su posicion
'Necesita:  Texto    Donde buscar
'           Pattern  Que se busca
'Devuelve:  Posit(0) Candidad de patterns encontrados
'           Posit()  Posicion de los patterns
'-----------------------------------------------------------------
    Dim MaxPosit, i, j
    MaxPosit = UBound(Posit)
    
    If MaxPosit < 5 Then
       MaxPosit = 5
       ReDim Posit(MaxPosit)
    End If

    ' Posit(0)  Cantidad de patterns encontrados
    ' Posit()   tiene la posicion de inicio de cada pattern
    Posit(0) = 0
    j = 1
    Do
        j = InStr(j, Texto, Pattern)
        If j > 0 Then
           If Posit(0) >= MaxPosit Then
              MaxPosit = MaxPosit + 10
              ReDim Preserve Posit(MaxPosit)
          End If
          Posit(0) = Posit(0) + 1
          Posit(Posit(0)) = j
          j = j + Len(Pattern)
        End If
    Loop While j > 0

End Sub

private Sub TagArma(TagTexto, TagDatos())
' ----------------------------------------------------
' Usada para Empaquetar Tag,
'   codificados de la forma: |xxxx|xxxx|xxxxx|
'Necesita: TagDatos()  vector con los datos a empaquetar
'          TagDatos(0) = cantidad de elementos validos
'Devuelve: TagTexto    string empaquetado
' ----------------------------------------------------
    Dim i, Max
    
    Max = UBound(TagDatos)
    If Val(TagDatos(0)) > 0 Then
       Max = Val(TagDatos(0))
    End If
    
    TagTexto = "|"
    For i = 1 To Max
        TagTexto = TagTexto + TagDatos(i) + "|"
    Next

End Sub

private Sub TagDesarma(TagTexto, TagDatos())
'-------------------------------------------------------------------
' Usada para des-empaquetar Tag,
'   codificados de la forma: |xxxx|xxxx|xxxxx|
'
'Necesita: TagTexto    string del Tag a Desempaquetar
'Devuelve: TagDatos()  vector con los datos Desempaquetados
'          TagDatos(0) = cantidad de elementos validos
'                       = -1 Error
'-------------------------------------------------------------------
    Dim i, Max
    Max = UBound(TagDatos)
    ReDim Posit(Max) 
    
    ' borra datos de TagDatos
    For i = 0 To Max
        TagDatos(i) = ""
    Next

    ' Desarma el tag
    Call InstrPosit(TagTexto, "|", Posit)
    
    ' Desarma datos
    If Posit(0) > 1 Then
        i = 1
        Do While Posit(0) > i And Posit(i) <> 0
           TagDatos(i) = ""
           If Posit(i + 1) - Posit(i) > 1 Then
              TagDatos(i) = Mid(TagTexto, Posit(i) + 1, Posit(i + 1) - Posit(i) - 1)
           End If
           i = i + 1
        Loop
        TagDatos(0) = cStr(Posit(0) - 1)
    End If
    
End Sub

private function ParteLeftTexto (Texto, largo, TextoSobra)
'---------------------------------------------------------------
' Entrega la parte izquierda de un texto dato hasta el largo
' y lo que sobra del texto
'---------------------------------------------------------------
	ParteLeftTexto = left(Texto, largo)

	TextoSobra = ""
	if len(Texto) > largo then
		TextoSobra = mid(Texto, largo + 1)
	end if	
	
end function

private function VeyArreglaTexto (Texto)
'---------------------------------------------------------------
' Ve y arregla texto, busca y rempleza caracteres que joden
' en el texto
'---------------------------------------------------------------
	Texto = StringReemplaza1(Texto, """", "'")
	Texto = StringReemplaza1(Texto, "<", "-")
	Texto = StringReemplaza1(Texto, ">", "-")
	VeyArreglaTexto = texto	
end function

%>
