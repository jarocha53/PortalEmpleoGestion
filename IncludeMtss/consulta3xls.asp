<!-- #include VIRTUAL="M3Migracion/migracion.asp" -->
<%@ Language=VBScript %>
<%Response.ContentType = "application/vnd.ms-excel"
Response.AddHeader "Content-Disposition", "attachment; filename=export.xls"
		
%>
<!--#include virtual="/IncludeMTSS/controls.asp"-->
<!--#include virtual="/IncludeMTSS/OpenConn.asp"-->
<%
'<!--#include file=inc/showreq.asp-->
SetLocale "es-ar"
Set Rec = Server.CreateObject("ADODB.Recordset")

'Bajar campos
FieldsInit
FieldsLoadRequest

Select Case Fields("consultaid")
Case 1: SPName = "qryProyectosPresentados"			'Proyectos Presentados
Case 2: SPName = "qryProyectosXEstado"				'Proyectos por Etapa
'Case 3: SPName = "qryTotalProyectosXEtapa_p2"		'Total de Proyectos x Etapa
Case 3: SPName = "qryTotalProyectosXEtapa"			'Total de Proyectos x Etapa
Case 4: SPName = "qryProyectosPriorizados"			'Proyectos Priorizados
Case 5: SPName = "qryTotalProyectosPriorizados"		'Total de Proyectos Priorizados
Case 6: SPName = ""		
End Select

Function Formatear(F)

	Select Case F.Type
	Case 3, 6: Formatear = "		<TD align=right>" & RTrim(F) & "</TD>" & vbCrLf
	Case Else: Formatear = "		<TD> " & RTrim(F) & " </TD>" & vbCrLf
	End Select

End Function

Set Rec = Server.CreateObject("ADODB.Recordset")
		
'Ejecutar stored procedure
Fields("Total") = 0
Con.CursorLocation = 3
Set cmdGrabar = Server.CreateObject("ADODB.Command")
With cmdGrabar
	.ActiveConnection = Con
	.CommandText = SPName
	.CommandType = adCmdStoredProc
	.CommandTimeOut = 300 
	FieldsSaveParameters .Parameters
'PL-SQL * T-SQL  
LNGRECS = TransformPLSQLToTSQL (LNGRECS) 
	Set Rec = .Execute(lngRecs)

	' El Store Tiene Total ?
	For Each P In .Parameters
		If P.Name = "@Total" Then
			TieneTotal = True
			Exit For
		End If
	Next

End With%>

<TABLE>

<TR>
	<TD colSpan="<%=Rec.Fields.Count%>" align="center"><B><%=Fields("Titulo")%></B></TD>
</TR>


<TR>
</TR>

<%If Len(Fields("Gecal")) Then%>
	<TR>
		<TH>Gecal:</TH>
		<TD><%=CodDescrip("Gecal", "DescGecal")%></TD>
	</TR>
<%End If
If Len(Fields("Programa")) Then%>
	<TR>
		<TH>Programa:</TH>
		<TD><%=CodDescrip("Programa", "DescPrograma")%></TD>
	</TR>
<%End If
If Len(Fields("TipoPrograma")) Then%>
	<TR>
		<TH>Tipo Programa:</TH>
		<TD><%=CodDescrip("TipoPrograma", "DescTipoPrograma")%></TD>
	</TR>
<%End If
If Len(Fields("PeriodoInicio")) Then%>
	<TR>
		<TH>Inicio:</TH>
		<TD><%=Fields("InicioDesc")%></TD>
	</TR>
<%End If
If Len(Fields("Resultado")) Then%>
	<TR>
		<TH>Estado:</TH>
		<TD><%=CodDescrip("Resultado", "DescResultado")%></TD>
	</TR>
<%End If%>

<TR>
</TR>

<TR>
<%For Each Field In Rec.Fields%>
	<TH><%=Field.Name%></TH>
<%Next%>
</TR>

<%If Rec.RecordCount Then
	i = 0
	Do Until Rec.EOF%>
		<TR>
		<%For Each Field In Rec.Fields%>
			<%=Formatear(Field)%>
		<%Next%>
		</TR>
		<%i = i + 1
		Rec.MoveNext
	Loop%>

	<%If TieneTotal Then%>
		<TR>
		<%Fields("Total") = 1
		Set cmdGrabar = Server.CreateObject("ADODB.Command")
		With cmdGrabar
			.ActiveConnection = Con
			.CommandText = SPName
			.CommandType = adCmdStoredProc
			.CommandTimeOut = 300
			FieldsSaveParameters .Parameters
'PL-SQL * T-SQL  
LNGRECS = TransformPLSQLToTSQL (LNGRECS) 
			Set Rec2 = .Execute(lngRecs)
		End With
		For Each Field In Rec2.Fields%>
			<%=Formatear(Field)%>
		<%Next%>
		</TR>
	<%End If
End If%>
</TABLE>
