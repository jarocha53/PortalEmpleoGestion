<!--#include virtual="/IncludeMTSS/common.asp"-->
<%
' Comentado para comprobar un error de nombre duplicado de constantes RIC 27-02-07
'<!--#include virtual="/IncludeMTSS/adovbs.asp"--> 


dim Fields
Private Sub FieldsInit()


	Set Fields = Server.CreateObject("Scripting.Dictionary")
	Fields.CompareMode  = vbTextCompare
End Sub

Private Sub FieldsSaveParameters2(Parameters)
'-----------------------------------------------------------
'	Carga ADO.parameters con los valores de Fields
'-----------------------------------------------------------
	For Each Parameter In Parameters
		If Parameter.Direction And adParamInput Then
			'Response.Write Parameter.Name & " = " & NullString2Null(Fields(Mid(Parameter.Name, 2))) & "<br>"
			Parameter.Value = NullString2Null(Fields(Mid(Parameter.Name, 2)))
		End If
	Next
End Sub

Private Sub FieldsSaveParameters(Parameters)
'-----------------------------------------------------------
'	Carga ADO.parameters con los valores de Fields
'-----------------------------------------------------------
	For Each Parameter In Parameters
		If Parameter.Direction And 1 Then
			'Response.Write Parameter.Name & " = " & NullString2Null(Fields(Mid(Parameter.Name, 2))) & "<br>"
			Parameter.Value = NullString2Null(Fields(Mid(Parameter.Name, 2)))
		End If
	Next
End Sub

'Private Sub FieldsSaveParameters(Parameters)
'	
'	For Each Parameter In Parameters
		'If Parameter.Direction And adParamInput Then
'	
'		'Response.End
'		If Parameter.Direction And 1 Then
'		'	Response.Write Parameter.Name & " = " & NullString2Null(Fields(Mid(Parameter.Name, 2))) & "<br>"
'			Parameter.Value = NullString2Null(Fields(Mid(Parameter.Name, 2)))
'		End If
'		
'	Next
'End Sub

Private Sub FieldsLoadParameters(Parameters)
	For Each Parameter In Parameters
		'If Parameter.Direction And (adParamOutput Or adParamReturnValue) Then
		If Parameter.Direction And (2 Or 4) Then
			'Response.Write "Fields(" & Mid(Parameter.Name, 2) & ") = " & FormatParameter(Parameter) & "<BR>"					
			Fields(Mid(Parameter.Name, 2)) = FormatParameter(Parameter)
		End If
		
	Next
End Sub

Private Sub FieldsLoadRequest()
	If Len(Request.QueryString) Then
		FieldsLoadRequestQueryString
	Else
		FieldsLoadRequestForm
	End If
End Sub

Private Sub FieldsLoadRequestQueryString()
	For Each Item In Request.QueryString
'		Fields.Add Item, Request.QueryString(Item)
'		Response.Write item & "=" & Request.QueryString(Item) & "<BR>"
		Fields(Item) = Request.QueryString(Item)
	Next
End Sub

Private Sub FieldsLoadRequestForm()
	For Each Item In Request.Form
	'	Fields.Add Item, Request.Form(Item)
	'	Response.Write item & "=" & Request.Form(Item) & "<BR>"
		Fields(Item) = Request.Form(Item)
	Next
End Sub

Private Function ToQueryString()
	For Each Item In Fields
		value = Fields(Item)
		If Len(value) Then
			varTemp = varTemp _
				& "&" & Item & "=" & Server.URLEncode(value)
		End If
	Next
	ToQueryString = Mid(varTemp, 2)
End Function
%>