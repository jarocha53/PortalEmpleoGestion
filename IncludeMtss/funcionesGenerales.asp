<%
Public Function EmitirReporteEnPaginaASP(TituloConsulta,subtitulo,ASPActual, ComandoStore, AspVarQueryString,ASPLink)  
'=========================================================================================
' Emite Un reporte en una pagina ASP con paginado
'
'Necesitan:
'	ASPactual		ASP que llamo a la funcion 
'	ComandoStore	String con la lina de comando SQL armada, 
'					puede ser un Store con todos sus parametros armados
'	AspVarQueryString	Varialbes para el query String que necesita mantener el ASPactual
'						Armadas de la forma "&Var1=Valor1&Var2=Valor2"
'   AspLink pagina a la que se puede redireccionar desde un campo de la consulta
'Devuelve:
'	Armar la Tabla con datos en la pagina ASPactual
'	Maneja la paginacion de los datos
'=========================================================================================
	Dim SP, Rs, sDato
	Dim Retorna 

	dim ExisteColCuil
	Retorna = ""
	existeColCuil=0
	'----------------------------------------
	' Si se llama a la pagina por el paginado de registros
	' recupero el comando SQL del Query string
	'----------------------------------------
	'..Response.Write "x" & ComandoStore & "x<br>"

	sDato = ""
	sDato = Request.QueryString("xCmdSql")
	if len(sDato)> 0 then
		ComandoStore = sDato
		'..Response.Write "*" & sDato & "*<br>"
		'..Response.end
	end if
	
	'''Response.Write " ccc " & comandostore
	
	'----------------------------------------
	' Muestra la consulta
	'----------------------------------------
	Set Rs = Server.CreateObject("adodb.recordset")
	Set Sp = Server.CreateObject("adodb.command")
	
	Sp.ActiveConnection = connLavoro 'InicioBD()
	Sp.CommandText = ComandoStore
	Sp.CommandType = 4 
	
	Set Rs = Sp.Execute()
	Retorna = ""

	
	
	

'''paginacion
	if rs.state = 1 then		
		if not rs.eof then	
			nActPagina	=Request("txtPagNum")
			Record=0	
			nTamPagina=7	' Cantidad de registos a mostrar
			
			If nActPagina = "" Then
				nActPagina=1
			Else
				nActPagina=Clng(Request("txtPagNum"))
			End If
	
			rs.PageSize = nTamPagina
			rs.CacheSize = nTamPagina
			nTotPagina = rs.PageCount
			If nActPagina < 1 Then
				nActPagina = 1
			End If
			If nActPagina > nTotPagina Then
				nActPagina = nTotPagina
			End If

			rs.AbsolutePage=nActPagina
			nTotRecord=0
			'calculo colspan para los titulos
			dim iCol
			iCol=0
			for i=1 to Rs.Fields.count -1
				icol=icol+1
				if trim(ucase(Rs.Fields(i).Name))="CUIL" THEN
					ExisteColCuil=1
					icol=icol+1
				END IF	
			next	
			
		'''paginacion	
			While rs.EOF <> True And nTotRecord < nTamPagina
				' Titulos
		  		if Record = 0 then
		  			'titulo central de la tabla	
		  			if len(trim(TituloConsulta)) >0 then
		  				
		  				
						Retorna = Retorna & "<th colspan=" & icol & " align='center' class='tbltext1' bgcolor='#D9D9AE'><b>" & tituloConsulta & "</b></th>"  			
						
					end if	
					if len(trim(Subtitulo)) >0 then
		  				retorna=retorna & "<tr>"
						Retorna = Retorna & "<th colspan=" & icol & " align='center' class='tbltext1' bgcolor='#D9D9AE'><b>" & subtitulo & "</b></td>"  			
						Retorna = Retorna & "</tr>"
					end if	
					retorna=retorna & "<tr>"
					Retorna = Retorna & "<td colspan=" & icol & " align='center' class='tbltext1' bgcolor='#D9D9AE'><b> Cantidad Total de registros " & Rs.recordcount & "</b></td>"  			
					Retorna = Retorna & "</tr>"
					retorna=retorna & "<tr>"
					for i=1 to Rs.Fields.count -1
						if trim(ucase(Rs.Fields(i).Name))="CUIL" THEN
							ExisteColCuil=1
						END IF	
							Retorna = Retorna & "<td nowrap width = '25%' align='center' class='tbltext1' bgcolor='#D9D9AE'><b>" & Rs.Fields(i).Name & "</b></td>"
						
					next	
					if existeColCuil=1 then
						retorna=retorna & "<td nowrap width = '25%' align='center' class='tbltext1' bgcolor='#D9D9AE'><b>Apellido &nbsp; y  &nbsp; Nombre</b></td>"
					end if	
					Retorna = Retorna & "</tr>"	
					record = 1  
				end if	
				
				' Datos 
				Retorna = Retorna & "<tr bordercolor='lightyellow' bgcolor='#EAEAD0'>"
				if instr(Rs.Fields(0).Value,"|")<>0 then
					a=Split(Rs.Fields(0).Value,"|")(0) & Split(Rs.Fields(0).Value,"|")(1) & Split(Rs.Fields(0).Value,"|")(2)& Split(Rs.Fields(0).Value,"|")(3)
				
				else
				
				end if
				for i=1 to  Rs.Fields.count-1
					'si los campos no tienen un link a otra pagina
					if aspLink=""  then
						Retorna = Retorna & "<td align='center' class='tbltext1' bordercolor='LightYellow'>" & Rs.Fields(i) & "</td>"
					'si los campos tienen un link a otra pagina: aspLink distinto de blanco	
					else	
						'si existe una pagina a la cual redirecciono haciendo click
						' en los campos de la consulta (se contempla que 
						' algunos o bien todos los campos de la consulta
						' tengan vinculo : 
						' caso 1: todos o algunos de los campos se direccionan
						'a una pagina con los mismos parametros
						' caso 2: todos o algunos de los campos se direccionan
						'a una pagina, cada uno con parametros particulares 
						'a dicho campo.
						
						'si tengo un link distinto para cada campo, uso split,dado que
						'cada parametro para cada campo esta separado por "|"
						
						'si tengo el mismo link para todos los campos, no uso split,dado que
						'en el campo x no voy a tener un vector de parametros,salvo que 
						'se requiera que algunos campos, y NO todos tengan un vinculo,aun cuando
						'usen los mismos parametros
						
						'Split(Rs.Fields(0).Value,"|")(i) es blanco o <> de blanco
						'si instr(Rs.Fields(0).Value,"|")<>0 significa que cada campo
						'tiene especificado un link particular para dicho campo
						
						'si instr(Rs.Fields(0).Value,"|")=0 significa que todos
						'los campos hacen link a la misma pagina con los mismos
						'parametros,por lo tanto no necesito usar el split
						
						VolverAPagina=ASPactual & "?txtPagNum=" & nActPagina & AspVarQueryString & "&xCmdSql=" & ComandoStore 
						''Response.Write VolveraPagina
						
						
						if instr(Rs.Fields(0).Value,"|")<>0 then
							if Split(Rs.Fields(0).Value,"|")(i)="" then
								Retorna = Retorna & "<td align='center' class='tbltext1' bordercolor='LightYellow'>" & Rs.Fields(i) & "</td>"
							else
								
								
								session("VolverAPagina")=VolverAPagina
							
								Retorna = Retorna & "<td align='center' bordercolor='LightYellow'><b><a class='tbltext1' href='" & ASPLink & "?" & Split(Rs.Fields(0).Value,"|")(i)  &  "'>" & Rs.Fields(i) & "</a></b></td>"
							end if
						else
							'puedo tener pagina a linkear, sin parametros
							if Rs.Fields(0).Value="" then
								Retorna = Retorna & "<td align='center' bordercolor='LightYellow'><b><a class='tbltext1' href='" & ASPLink & "'>" & Rs.Fields(i) & "</a></b></td>"
							end if
						end if	
					end if	
					
				next
				if ExisteColCuil=1 then
					if asplink="" then
					retorna=retorna & "<td align='center' class='tbltext1' bordercolor='LightYellow'>" & ExistePostulante(Rs.Fields("cuil")) & "</td>"
					else
						if instr(Rs.Fields(0).Value,"|")<>0 then
							if Split(Rs.Fields(0).Value,"|")(i)="" then
								Retorna = Retorna & "<td align='center' class='tbltext1' bordercolor='LightYellow'>" & ExistePostulante(Rs.Fields("cuil")) & "</td>"
							else
								
								
								session("VolverAPagina")=VolverAPagina
							
								Retorna = Retorna & "<td align='center' bordercolor='LightYellow'><b><a class='tbltext1' href='" & ASPLink & "?" & Split(Rs.Fields(0).Value,"|")(i)  &  "'>" & ExistePostulante(Rs.Fields("cuil")) & "</a></b></td>"
							end if
						else
							'puedo tener pagina a linkear, sin parametros
							if Rs.Fields(0).Value="" then
								Retorna = Retorna & "<td align='center' bordercolor='LightYellow'><b><a class='tbltext1' href='" & ASPLink & "'>" & ExistePostulante(Rs.Fields("cuil")) & "</a></b></td>"
							end if
						end if	
					
					
					
					
					''''''retorna=retorna & "<td align='center' bordercolor='LightYellow'><a class='tbltext1' href='" & ASPLink & "'>" & ExistePostulante(Rs.Fields("cuil")) & "</a></td>"
					end if
				end if	
				Retorna = Retorna & "</tr>"	
			
				nTotRecord=nTotRecord + 1	
				rs.MoveNext
			wend
				
			Retorna = Retorna & "<table border=0 width=480>"
			Retorna = Retorna & "<tr>"
			
			' paginacion
			' paginacion
			if nActPagina > 1 then
				Retorna = Retorna & "<td align='right' width='480'>"
				Retorna = Retorna & "<a href=" & ASPActual & "?txtPagNum=" & (nActPagina - 1) & AspVarQueryString & "&xCmdSql=" & ComandoStore & "><img src='" & Session("Progetto") & "/images/precedente.gif' border='0' id='image1' name='image1'>"
				Retorna = Retorna & "</td>"				
			end if
			if nActPagina < nTotPagina then
				Retorna = Retorna & "<td align='right'>"
				Retorna = Retorna & "<a href=" & ASPActual & "?txtPagNum=" & (nActPagina + 1) & AspVarQueryString & "&xCmdSql=" & ComandoStore & "><img src='" & Session("Progetto") & "/images/successivo.gif' border='0' id='image1' name='image1'>"
				Retorna = Retorna & "</td>"
			end if		
			Retorna = Retorna & "</tr>"
			Retorna = Retorna & "</table>"
				
		end if 
	end if 
		
	' cierre todo 
	'Rs.close
	set Rs = nothing
	set Sp = nothing
	
	'---------------------------
	' salida 	
	'---------------------------
	EmitirReporteEnPaginaASP = Retorna 
	
end function




%>