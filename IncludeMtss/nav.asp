<%
'========================================
'	Control de Navegacion entre paginas
'========================================
' Inicializa variable de control
Sub NavInit(Id)
	Session(Id) = 1
End Sub

' Resetea Variable de Control
Sub NavReset(Id)
	Session(Id) = ""
End Sub

' Ckequea variable de control
Sub NavCheck(Id)
	If Len(Session(Id)) = 0 Then
		Response.Redirect "/jefes/inc/mensaje.asp?m=" _
			& Server.URLEncode("Los datos ya fueron<BR>grabados anteriormente<BR><BR>...para modificarlos, h�galo a trav�s del men�.")
	End If
End Sub

%>
